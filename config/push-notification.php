<?php

return array(

    'wycIos'     => array(
        'environment' =>'production',
        'certificate' =>'ios/visa_app_cert.pem',
        'passPhrase'  =>'qwerty123',
        'service'     =>'apns'
    ),
    'wycAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyA7ev628GjNpVE5WMy7mhARgvFur_JZ-zg',
        'service'     =>'gcm'
    )

);