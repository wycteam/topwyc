/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 261);
/******/ })
/************************************************************************/
/******/ ({

/***/ 139:
/***/ (function(module, exports) {

var currentTable = null;
var currentCategory = null;

var CategoryApp = new Vue({
	el: '#category-root',

	data: {
		categories: [],
		filterType: 'all',
		filterStatus: '',

		catId: '',
		catName: '',
		catParent: '',
		catActivated: false,
		catFeatured: false
	},

	watch: {
		categories: function categories() {
			$('.dt-category').DataTable().destroy();

			setTimeout(function () {
				currentTable = $('.dt-category').DataTable({
					pageLength: 10,
					responsive: true
				});
			}, 500);
		},
		filterType: function filterType() {
			$('.dt-category').DataTable().destroy();

			setTimeout(function () {
				currentTable = $('.dt-category').DataTable({
					pageLength: 10,
					responsive: true
				});
			}, 500);
		},
		filterStatus: function filterStatus() {
			$('.dt-category').DataTable().destroy();

			setTimeout(function () {
				currentTable = $('.dt-category').DataTable({
					pageLength: 10,
					responsive: true
				});
			}, 500);
		}
	},

	methods: {
		default: function _default() {
			this.catName = '';
			this.catParent = '';
			this.catActivated = false;
			this.catFeatured = false;
		},
		showModal: function showModal(id) {
			var category = this.categories.filter(function (obj) {
				return obj.id == id;
			});
			this.default();
			if (category.length) {
				category = category[0];

				this.catId = id;
				this.catName = category.name;
				this.catParent = category.parent_id;
				this.catActivated = parseInt(category.active);
				this.catFeatured = parseInt(category.featured);
			}
			$('#categoryModal').modal('toggle');
		},
		getType: function getType(treelevel) {
			if (treelevel.indexOf('-') > -1) {
				return 'Child';
			} else if (treelevel == '') {
				return 'Grandparent';
			} else {
				return 'Parent';
			}
		},
		updateCategory: function updateCategory() {
			var _this = this;

			if (this.catName != '') {
				if (this.catId == 0) {
					axiosAPIv1.post('/categories', {
						name: this.catName,
						parent_id: this.catParent,
						active: this.catActivated ? 1 : 0,
						featured: this.catFeatured ? 1 : 0
					}).then(function (response) {
						if (response.status == 201) {

							$('.dt-category').DataTable().destroy();
							_this.categories.push(response.data.data);
							toastr.success('Category Added!');

							setTimeout(function () {
								currentTable = $('.dt-category').DataTable({
									pageLength: 10,
									responsive: true
								});
							}, 500);

							$('#categoryModal').modal('toggle');
						}
					});
				} else {
					axiosAPIv1.put('/categories/' + this.catId, {
						name: this.catName,
						parent_id: this.catParent,
						active: this.catActivated ? 1 : 0,
						featured: this.catFeatured ? 1 : 0
					}).then(function (response) {
						if (response.status == 200) {
							_this.categories.filter(function (obj) {
								if (obj.id == _this.catId) {
									obj.name = response.data.data.name;
									obj.parent_id = response.data.data.parent_id;
									obj.active = response.data.data.active;
									obj.featured = response.data.data.featured;
									toastr.success('Category Updated!');

									$('.dt-category').DataTable().destroy();

									setTimeout(function () {
										currentTable = $('.dt-category').DataTable({
											pageLength: 10,
											responsive: true
										});
									}, 500);

									$('#categoryModal').modal('toggle');
								}
							});
						}
					});
				}
			} else {
				toastr.error('', 'Category Name is required.');
			}
		},
		statusColor: function statusColor(status) {
			switch (status) {
				case 'Pending':
					return 'label-warning';break;
				case 'Processing':
					return 'label-default';break;
				case 'Completed':
					return 'label-primary';break;
				case 'Cancelled':
					return 'label-danger';break;
			}
		},
		showFilterStatus: function showFilterStatus(status) {
			// if(this.filterStatus != 'all')
			// {
			// 	return this.filterStatus == status.toLowerCase();
			// }
			// return true;
		},
		showFilterType: function showFilterType(type) {
			if (this.filterType != 'all') {
				return this.filterType.toLowerCase() == type.toLowerCase();
			}
			return true;
		},
		textColor: function textColor(status) {
			switch (status) {
				case 'Pending':
					return 'row-pending';break;
				case 'Completed':
					return 'row-completed';break;
				case 'Processing':
					return 'row-processing';break;
				case 'Cancelled':
					return 'row-cancelled';break;
			}
		}
	},

	created: function created() {
		var _this2 = this;

		axiosAPIv1.get('/category').then(function (response) {
			if (response.status == 200) {
				_this2.categories = response.data.data.cats;
			}
		});
	},
	mounted: function mounted() {}
});

/***/ }),

/***/ 261:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(139);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDE/NTk0YSoqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9wYWdlcy9jYXRlZ29yeS5qcyJdLCJuYW1lcyI6WyJjdXJyZW50VGFibGUiLCJjdXJyZW50Q2F0ZWdvcnkiLCJDYXRlZ29yeUFwcCIsIlZ1ZSIsImVsIiwiZGF0YSIsImNhdGVnb3JpZXMiLCJmaWx0ZXJUeXBlIiwiZmlsdGVyU3RhdHVzIiwiY2F0SWQiLCJjYXROYW1lIiwiY2F0UGFyZW50IiwiY2F0QWN0aXZhdGVkIiwiY2F0RmVhdHVyZWQiLCJ3YXRjaCIsIiQiLCJEYXRhVGFibGUiLCJkZXN0cm95Iiwic2V0VGltZW91dCIsInBhZ2VMZW5ndGgiLCJyZXNwb25zaXZlIiwibWV0aG9kcyIsImRlZmF1bHQiLCJzaG93TW9kYWwiLCJpZCIsImNhdGVnb3J5IiwiZmlsdGVyIiwib2JqIiwibGVuZ3RoIiwibmFtZSIsInBhcmVudF9pZCIsInBhcnNlSW50IiwiYWN0aXZlIiwiZmVhdHVyZWQiLCJtb2RhbCIsImdldFR5cGUiLCJ0cmVlbGV2ZWwiLCJpbmRleE9mIiwidXBkYXRlQ2F0ZWdvcnkiLCJheGlvc0FQSXYxIiwicG9zdCIsInRoZW4iLCJyZXNwb25zZSIsInN0YXR1cyIsInB1c2giLCJ0b2FzdHIiLCJzdWNjZXNzIiwicHV0IiwiZXJyb3IiLCJzdGF0dXNDb2xvciIsInNob3dGaWx0ZXJTdGF0dXMiLCJzaG93RmlsdGVyVHlwZSIsInR5cGUiLCJ0b0xvd2VyQ2FzZSIsInRleHRDb2xvciIsImNyZWF0ZWQiLCJnZXQiLCJjYXRzIiwibW91bnRlZCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQSxJQUFJQSxlQUFhLElBQWpCO0FBQ0EsSUFBSUMsa0JBQWdCLElBQXBCOztBQUVBLElBQUlDLGNBQWMsSUFBSUMsR0FBSixDQUFRO0FBQ3pCQyxLQUFJLGdCQURxQjs7QUFHekJDLE9BQUs7QUFDSkMsY0FBVyxFQURQO0FBRUpDLGNBQVcsS0FGUDtBQUdKQyxnQkFBYSxFQUhUOztBQUtKQyxTQUFNLEVBTEY7QUFNSkMsV0FBUSxFQU5KO0FBT0pDLGFBQVUsRUFQTjtBQVFKQyxnQkFBYSxLQVJUO0FBU0pDLGVBQVk7QUFUUixFQUhvQjs7QUFlekJDLFFBQU07QUFDTFIsY0FBVyxzQkFBVTtBQUNsQlMsS0FBRSxjQUFGLEVBQWtCQyxTQUFsQixHQUE4QkMsT0FBOUI7O0FBRUZDLGNBQVcsWUFBVTtBQUNwQmxCLG1CQUFlZSxFQUFFLGNBQUYsRUFBa0JDLFNBQWxCLENBQTRCO0FBQ3BDRyxpQkFBWSxFQUR3QjtBQUVwQ0MsaUJBQVk7QUFGd0IsS0FBNUIsQ0FBZjtBQUlBLElBTEQsRUFLRSxHQUxGO0FBTUEsR0FWSTtBQVdMYixjQUFXLHNCQUFVO0FBQ2xCUSxLQUFFLGNBQUYsRUFBa0JDLFNBQWxCLEdBQThCQyxPQUE5Qjs7QUFFRkMsY0FBVyxZQUFVO0FBQ3BCbEIsbUJBQWVlLEVBQUUsY0FBRixFQUFrQkMsU0FBbEIsQ0FBNEI7QUFDcENHLGlCQUFZLEVBRHdCO0FBRXBDQyxpQkFBWTtBQUZ3QixLQUE1QixDQUFmO0FBSUEsSUFMRCxFQUtFLEdBTEY7QUFNQSxHQXBCSTtBQXFCTFosZ0JBQWEsd0JBQVU7QUFDcEJPLEtBQUUsY0FBRixFQUFrQkMsU0FBbEIsR0FBOEJDLE9BQTlCOztBQUVGQyxjQUFXLFlBQVU7QUFDcEJsQixtQkFBZWUsRUFBRSxjQUFGLEVBQWtCQyxTQUFsQixDQUE0QjtBQUNwQ0csaUJBQVksRUFEd0I7QUFFcENDLGlCQUFZO0FBRndCLEtBQTVCLENBQWY7QUFJQSxJQUxELEVBS0UsR0FMRjtBQU1BO0FBOUJJLEVBZm1COztBQWdEekJDLFVBQVE7QUFDUEMsU0FETyxzQkFDRTtBQUNSLFFBQUtaLE9BQUwsR0FBZSxFQUFmO0FBQ0EsUUFBS0MsU0FBTCxHQUFpQixFQUFqQjtBQUNBLFFBQUtDLFlBQUwsR0FBb0IsS0FBcEI7QUFDQSxRQUFLQyxXQUFMLEdBQW1CLEtBQW5CO0FBQ0EsR0FOTTtBQVFQVSxXQVJPLHFCQVFHQyxFQVJILEVBUU07QUFDWixPQUFJQyxXQUFXLEtBQUtuQixVQUFMLENBQWdCb0IsTUFBaEIsQ0FBdUIsZUFBTztBQUM1QyxXQUFPQyxJQUFJSCxFQUFKLElBQVVBLEVBQWpCO0FBQ0EsSUFGYyxDQUFmO0FBR0EsUUFBS0YsT0FBTDtBQUNBLE9BQUdHLFNBQVNHLE1BQVosRUFDQTtBQUNDSCxlQUFXQSxTQUFTLENBQVQsQ0FBWDs7QUFFQSxTQUFLaEIsS0FBTCxHQUFhZSxFQUFiO0FBQ0EsU0FBS2QsT0FBTCxHQUFlZSxTQUFTSSxJQUF4QjtBQUNBLFNBQUtsQixTQUFMLEdBQWlCYyxTQUFTSyxTQUExQjtBQUNBLFNBQUtsQixZQUFMLEdBQW9CbUIsU0FBU04sU0FBU08sTUFBbEIsQ0FBcEI7QUFDQSxTQUFLbkIsV0FBTCxHQUFtQmtCLFNBQVNOLFNBQVNRLFFBQWxCLENBQW5CO0FBRUE7QUFDQWxCLEtBQUUsZ0JBQUYsRUFBb0JtQixLQUFwQixDQUEwQixRQUExQjtBQUNELEdBekJNO0FBMkJQQyxTQTNCTyxtQkEyQkNDLFNBM0JELEVBMkJXO0FBQ2pCLE9BQUdBLFVBQVVDLE9BQVYsQ0FBa0IsR0FBbEIsSUFBeUIsQ0FBQyxDQUE3QixFQUNBO0FBQ0MsV0FBTyxPQUFQO0FBQ0EsSUFIRCxNQUdNLElBQUdELGFBQWEsRUFBaEIsRUFDTjtBQUNDLFdBQU8sYUFBUDtBQUNBLElBSEssTUFJTjtBQUNDLFdBQU8sUUFBUDtBQUNBO0FBQ0QsR0F0Q007QUF3Q1BFLGdCQXhDTyw0QkF3Q1M7QUFBQTs7QUFDZixPQUFHLEtBQUs1QixPQUFMLElBQWdCLEVBQW5CLEVBQ0E7QUFDQyxRQUFHLEtBQUtELEtBQUwsSUFBYyxDQUFqQixFQUNBO0FBQ0M4QixnQkFBV0MsSUFBWCxDQUFnQixhQUFoQixFQUE4QjtBQUM3QlgsWUFBUSxLQUFLbkIsT0FEZ0I7QUFFN0JvQixpQkFBVyxLQUFLbkIsU0FGYTtBQUc3QnFCLGNBQVUsS0FBS3BCLFlBQUwsR0FBa0IsQ0FBbEIsR0FBb0IsQ0FIRDtBQUk3QnFCLGdCQUFXLEtBQUtwQixXQUFMLEdBQWlCLENBQWpCLEdBQW1CO0FBSkQsTUFBOUIsRUFNQzRCLElBTkQsQ0FNTSxvQkFBWTtBQUNqQixVQUFHQyxTQUFTQyxNQUFULElBQW1CLEdBQXRCLEVBQ0E7O0FBRUM1QixTQUFFLGNBQUYsRUFBa0JDLFNBQWxCLEdBQThCQyxPQUE5QjtBQUNBLGFBQUtYLFVBQUwsQ0FBZ0JzQyxJQUFoQixDQUFxQkYsU0FBU3JDLElBQVQsQ0FBY0EsSUFBbkM7QUFDQXdDLGNBQU9DLE9BQVAsQ0FBZSxpQkFBZjs7QUFHQTVCLGtCQUFXLFlBQVU7QUFDcEJsQix1QkFBZWUsRUFBRSxjQUFGLEVBQWtCQyxTQUFsQixDQUE0QjtBQUNwQ0cscUJBQVksRUFEd0I7QUFFcENDLHFCQUFZO0FBRndCLFNBQTVCLENBQWY7QUFJQSxRQUxELEVBS0UsR0FMRjs7QUFPQUwsU0FBRSxnQkFBRixFQUFvQm1CLEtBQXBCLENBQTBCLFFBQTFCO0FBRUE7QUFFRCxNQTFCRDtBQTJCQSxLQTdCRCxNQThCQTtBQUNDSyxnQkFBV1EsR0FBWCxDQUFlLGlCQUFlLEtBQUt0QyxLQUFuQyxFQUF5QztBQUN4Q29CLFlBQVEsS0FBS25CLE9BRDJCO0FBRXhDb0IsaUJBQVcsS0FBS25CLFNBRndCO0FBR3hDcUIsY0FBVSxLQUFLcEIsWUFBTCxHQUFrQixDQUFsQixHQUFvQixDQUhVO0FBSXhDcUIsZ0JBQVcsS0FBS3BCLFdBQUwsR0FBaUIsQ0FBakIsR0FBbUI7QUFKVSxNQUF6QyxFQU1DNEIsSUFORCxDQU1NLG9CQUFZO0FBQ2pCLFVBQUdDLFNBQVNDLE1BQVQsSUFBbUIsR0FBdEIsRUFDQTtBQUNDLGFBQUtyQyxVQUFMLENBQWdCb0IsTUFBaEIsQ0FBdUIsZUFBTztBQUM3QixZQUFHQyxJQUFJSCxFQUFKLElBQVUsTUFBS2YsS0FBbEIsRUFDQTtBQUNDa0IsYUFBSUUsSUFBSixHQUFhYSxTQUFTckMsSUFBVCxDQUFjQSxJQUFkLENBQW1Cd0IsSUFBaEM7QUFDQUYsYUFBSUcsU0FBSixHQUFnQlksU0FBU3JDLElBQVQsQ0FBY0EsSUFBZCxDQUFtQnlCLFNBQW5DO0FBQ0FILGFBQUlLLE1BQUosR0FBZVUsU0FBU3JDLElBQVQsQ0FBY0EsSUFBZCxDQUFtQjJCLE1BQWxDO0FBQ0FMLGFBQUlNLFFBQUosR0FBZ0JTLFNBQVNyQyxJQUFULENBQWNBLElBQWQsQ0FBbUI0QixRQUFuQztBQUNBWSxnQkFBT0MsT0FBUCxDQUFlLG1CQUFmOztBQUVBL0IsV0FBRSxjQUFGLEVBQWtCQyxTQUFsQixHQUE4QkMsT0FBOUI7O0FBRUFDLG9CQUFXLFlBQVU7QUFDcEJsQix5QkFBZWUsRUFBRSxjQUFGLEVBQWtCQyxTQUFsQixDQUE0QjtBQUNwQ0csdUJBQVksRUFEd0I7QUFFcENDLHVCQUFZO0FBRndCLFdBQTVCLENBQWY7QUFJQSxVQUxELEVBS0UsR0FMRjs7QUFPQUwsV0FBRSxnQkFBRixFQUFvQm1CLEtBQXBCLENBQTBCLFFBQTFCO0FBRUE7QUFDRCxRQXJCRDtBQXNCQTtBQUNELE1BaENEO0FBaUNBO0FBQ0QsSUFuRUQsTUFtRUs7QUFDSlcsV0FBT0csS0FBUCxDQUFhLEVBQWIsRUFBZ0IsNEJBQWhCO0FBQ0E7QUFDRCxHQS9HTTtBQWlIUEMsYUFqSE8sdUJBaUhLTixNQWpITCxFQWlIWTtBQUNsQixXQUFPQSxNQUFQO0FBQ0MsU0FBSyxTQUFMO0FBQWdCLFlBQU8sZUFBUCxDQUF3QjtBQUN4QyxTQUFLLFlBQUw7QUFBbUIsWUFBTyxlQUFQLENBQXdCO0FBQzNDLFNBQUssV0FBTDtBQUFrQixZQUFPLGVBQVAsQ0FBd0I7QUFDMUMsU0FBSyxXQUFMO0FBQWtCLFlBQU8sY0FBUCxDQUF1QjtBQUoxQztBQU1BLEdBeEhNO0FBMEhQTyxrQkExSE8sNEJBMEhVUCxNQTFIVixFQTBIaUI7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBaElNO0FBa0lQUSxnQkFsSU8sMEJBa0lRQyxJQWxJUixFQWtJYTtBQUNuQixPQUFHLEtBQUs3QyxVQUFMLElBQW1CLEtBQXRCLEVBQ0E7QUFDQyxXQUFPLEtBQUtBLFVBQUwsQ0FBZ0I4QyxXQUFoQixNQUFpQ0QsS0FBS0MsV0FBTCxFQUF4QztBQUNBO0FBQ0QsVUFBTyxJQUFQO0FBQ0EsR0F4SU07QUEySVBDLFdBM0lPLHFCQTJJR1gsTUEzSUgsRUEySVU7QUFDaEIsV0FBT0EsTUFBUDtBQUVDLFNBQUssU0FBTDtBQUFnQixZQUFPLGFBQVAsQ0FBc0I7QUFDdEMsU0FBSyxXQUFMO0FBQWtCLFlBQU8sZUFBUCxDQUF3QjtBQUMxQyxTQUFLLFlBQUw7QUFBbUIsWUFBTyxnQkFBUCxDQUF5QjtBQUM1QyxTQUFLLFdBQUw7QUFBa0IsWUFBTyxlQUFQLENBQXdCO0FBTDNDO0FBT0E7QUFuSk0sRUFoRGlCOztBQXVNekJZLFFBdk15QixxQkF1TWhCO0FBQUE7O0FBQ1JoQixhQUFXaUIsR0FBWCxDQUFlLFdBQWYsRUFDRWYsSUFERixDQUNPLG9CQUFZO0FBQ2pCLE9BQUdDLFNBQVNDLE1BQVQsSUFBbUIsR0FBdEIsRUFDQTtBQUNDLFdBQUtyQyxVQUFMLEdBQWtCb0MsU0FBU3JDLElBQVQsQ0FBY0EsSUFBZCxDQUFtQm9ELElBQXJDO0FBQ0E7QUFDRCxHQU5GO0FBT0EsRUEvTXdCO0FBaU56QkMsUUFqTnlCLHFCQWlOaEIsQ0FFUjtBQW5Od0IsQ0FBUixDQUFsQixDIiwiZmlsZSI6ImpzL2NhdGVnb3J5LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAyNjEpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGIyMGEyYTBiMjk1NmE5ZDZjMGQxIiwidmFyIGN1cnJlbnRUYWJsZT1udWxsO1xyXG52YXIgY3VycmVudENhdGVnb3J5PW51bGw7XHJcblxyXG52YXIgQ2F0ZWdvcnlBcHAgPSBuZXcgVnVlKHtcclxuXHRlbDogJyNjYXRlZ29yeS1yb290JyxcclxuXHRcclxuXHRkYXRhOntcclxuXHRcdGNhdGVnb3JpZXM6W10sXHJcblx0XHRmaWx0ZXJUeXBlOidhbGwnLFxyXG5cdFx0ZmlsdGVyU3RhdHVzOicnLFxyXG5cclxuXHRcdGNhdElkOicnLFxyXG5cdFx0Y2F0TmFtZTonJyxcclxuXHRcdGNhdFBhcmVudDonJyxcclxuXHRcdGNhdEFjdGl2YXRlZDpmYWxzZSxcclxuXHRcdGNhdEZlYXR1cmVkOmZhbHNlLFxyXG5cdH0sXHJcblxyXG5cdHdhdGNoOntcclxuXHRcdGNhdGVnb3JpZXM6ZnVuY3Rpb24oKXtcclxuXHQgIFx0XHQkKCcuZHQtY2F0ZWdvcnknKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XHJcblxyXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcblx0XHRcdFx0Y3VycmVudFRhYmxlID0gJCgnLmR0LWNhdGVnb3J5JykuRGF0YVRhYmxlKHtcclxuXHRcdFx0ICAgICAgICBwYWdlTGVuZ3RoOiAxMCxcclxuXHRcdFx0ICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxyXG5cdFx0XHQgICAgfSk7XHJcblx0XHRcdH0sNTAwKTtcclxuXHRcdH0sXHJcblx0XHRmaWx0ZXJUeXBlOmZ1bmN0aW9uKCl7XHJcblx0ICBcdFx0JCgnLmR0LWNhdGVnb3J5JykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xyXG5cclxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xyXG5cdFx0XHRcdGN1cnJlbnRUYWJsZSA9ICQoJy5kdC1jYXRlZ29yeScpLkRhdGFUYWJsZSh7XHJcblx0XHRcdCAgICAgICAgcGFnZUxlbmd0aDogMTAsXHJcblx0XHRcdCAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcclxuXHRcdFx0ICAgIH0pO1xyXG5cdFx0XHR9LDUwMCk7XHJcblx0XHR9LFxyXG5cdFx0ZmlsdGVyU3RhdHVzOmZ1bmN0aW9uKCl7XHJcblx0ICBcdFx0JCgnLmR0LWNhdGVnb3J5JykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xyXG5cclxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xyXG5cdFx0XHRcdGN1cnJlbnRUYWJsZSA9ICQoJy5kdC1jYXRlZ29yeScpLkRhdGFUYWJsZSh7XHJcblx0XHRcdCAgICAgICAgcGFnZUxlbmd0aDogMTAsXHJcblx0XHRcdCAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcclxuXHRcdFx0ICAgIH0pO1xyXG5cdFx0XHR9LDUwMCk7XHJcblx0XHR9XHJcblx0fSxcclxuXHJcblx0bWV0aG9kczp7XHJcblx0XHRkZWZhdWx0KCl7XHJcblx0XHRcdHRoaXMuY2F0TmFtZSA9ICcnO1xyXG5cdFx0XHR0aGlzLmNhdFBhcmVudCA9ICcnO1xyXG5cdFx0XHR0aGlzLmNhdEFjdGl2YXRlZCA9IGZhbHNlO1xyXG5cdFx0XHR0aGlzLmNhdEZlYXR1cmVkID0gZmFsc2U7XHJcblx0XHR9LFxyXG5cclxuXHRcdHNob3dNb2RhbChpZCl7XHJcblx0XHRcdHZhciBjYXRlZ29yeSA9IHRoaXMuY2F0ZWdvcmllcy5maWx0ZXIob2JqID0+IHtcclxuXHRcdFx0XHRyZXR1cm4gb2JqLmlkID09IGlkO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHR0aGlzLmRlZmF1bHQoKTtcclxuXHRcdFx0aWYoY2F0ZWdvcnkubGVuZ3RoKVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0Y2F0ZWdvcnkgPSBjYXRlZ29yeVswXTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHR0aGlzLmNhdElkID0gaWQ7XHJcblx0XHRcdFx0dGhpcy5jYXROYW1lID0gY2F0ZWdvcnkubmFtZTtcclxuXHRcdFx0XHR0aGlzLmNhdFBhcmVudCA9IGNhdGVnb3J5LnBhcmVudF9pZDtcclxuXHRcdFx0XHR0aGlzLmNhdEFjdGl2YXRlZCA9IHBhcnNlSW50KGNhdGVnb3J5LmFjdGl2ZSk7XHJcblx0XHRcdFx0dGhpcy5jYXRGZWF0dXJlZCA9IHBhcnNlSW50KGNhdGVnb3J5LmZlYXR1cmVkKTtcclxuXHJcblx0XHRcdH1cclxuXHRcdFx0XHQkKCcjY2F0ZWdvcnlNb2RhbCcpLm1vZGFsKCd0b2dnbGUnKTtcclxuXHRcdH0sXHJcblxyXG5cdFx0Z2V0VHlwZSh0cmVlbGV2ZWwpe1xyXG5cdFx0XHRpZih0cmVlbGV2ZWwuaW5kZXhPZignLScpID4gLTEpXHJcblx0XHRcdHtcclxuXHRcdFx0XHRyZXR1cm4gJ0NoaWxkJztcclxuXHRcdFx0fWVsc2UgaWYodHJlZWxldmVsID09ICcnKVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0cmV0dXJuICdHcmFuZHBhcmVudCc7XHJcblx0XHRcdH1lbHNlXHJcblx0XHRcdHtcclxuXHRcdFx0XHRyZXR1cm4gJ1BhcmVudCc7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblxyXG5cdFx0dXBkYXRlQ2F0ZWdvcnkoKXtcclxuXHRcdFx0aWYodGhpcy5jYXROYW1lICE9ICcnKVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0aWYodGhpcy5jYXRJZCA9PSAwKVxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdGF4aW9zQVBJdjEucG9zdCgnL2NhdGVnb3JpZXMnLHtcclxuXHRcdFx0XHRcdFx0bmFtZSBcdFx0OnRoaXMuY2F0TmFtZSxcclxuXHRcdFx0XHRcdFx0cGFyZW50X2lkXHQ6dGhpcy5jYXRQYXJlbnQsXHJcblx0XHRcdFx0XHRcdGFjdGl2ZSBcdFx0OnRoaXMuY2F0QWN0aXZhdGVkPzE6MCxcclxuXHRcdFx0XHRcdFx0ZmVhdHVyZWQgXHQ6dGhpcy5jYXRGZWF0dXJlZD8xOjAsXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdFx0XHRpZihyZXNwb25zZS5zdGF0dXMgPT0gMjAxKVxyXG5cdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0JCgnLmR0LWNhdGVnb3J5JykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xyXG5cdFx0XHRcdFx0XHRcdHRoaXMuY2F0ZWdvcmllcy5wdXNoKHJlc3BvbnNlLmRhdGEuZGF0YSk7XHJcblx0XHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MoJ0NhdGVnb3J5IEFkZGVkIScpO1xyXG5cdFx0XHRcdFx0XHRcdFxyXG5cclxuXHRcdFx0XHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcblx0XHRcdFx0XHRcdFx0XHRjdXJyZW50VGFibGUgPSAkKCcuZHQtY2F0ZWdvcnknKS5EYXRhVGFibGUoe1xyXG5cdFx0XHRcdFx0XHRcdCAgICAgICAgcGFnZUxlbmd0aDogMTAsXHJcblx0XHRcdFx0XHRcdFx0ICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdCAgICB9KTtcclxuXHRcdFx0XHRcdFx0XHR9LDUwMCk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdCQoJyNjYXRlZ29yeU1vZGFsJykubW9kYWwoJ3RvZ2dsZScpO1xyXG5cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0fSlcclxuXHRcdFx0XHR9ZWxzZVxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdGF4aW9zQVBJdjEucHV0KCcvY2F0ZWdvcmllcy8nK3RoaXMuY2F0SWQse1xyXG5cdFx0XHRcdFx0XHRuYW1lIFx0XHQ6dGhpcy5jYXROYW1lLFxyXG5cdFx0XHRcdFx0XHRwYXJlbnRfaWRcdDp0aGlzLmNhdFBhcmVudCxcclxuXHRcdFx0XHRcdFx0YWN0aXZlIFx0XHQ6dGhpcy5jYXRBY3RpdmF0ZWQ/MTowLFxyXG5cdFx0XHRcdFx0XHRmZWF0dXJlZCBcdDp0aGlzLmNhdEZlYXR1cmVkPzE6MCxcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XHJcblx0XHRcdFx0XHRcdGlmKHJlc3BvbnNlLnN0YXR1cyA9PSAyMDApXHJcblx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmNhdGVnb3JpZXMuZmlsdGVyKG9iaiA9PiB7XHJcblx0XHRcdFx0XHRcdFx0XHRpZihvYmouaWQgPT0gdGhpcy5jYXRJZClcclxuXHRcdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0b2JqLm5hbWUgXHRcdD0gcmVzcG9uc2UuZGF0YS5kYXRhLm5hbWU7XHJcblx0XHRcdFx0XHRcdFx0XHRcdG9iai5wYXJlbnRfaWRcdD0gcmVzcG9uc2UuZGF0YS5kYXRhLnBhcmVudF9pZDtcclxuXHRcdFx0XHRcdFx0XHRcdFx0b2JqLmFjdGl2ZSBcdFx0PSByZXNwb25zZS5kYXRhLmRhdGEuYWN0aXZlO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRvYmouZmVhdHVyZWQgXHQ9IHJlc3BvbnNlLmRhdGEuZGF0YS5mZWF0dXJlZDtcclxuXHRcdFx0XHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MoJ0NhdGVnb3J5IFVwZGF0ZWQhJyk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRcdFx0XHQkKCcuZHQtY2F0ZWdvcnknKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0Y3VycmVudFRhYmxlID0gJCgnLmR0LWNhdGVnb3J5JykuRGF0YVRhYmxlKHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0ICAgICAgICBwYWdlTGVuZ3RoOiAxMCxcclxuXHRcdFx0XHRcdFx0XHRcdFx0ICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHQgICAgfSk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdH0sNTAwKTtcclxuXHJcblx0XHRcdFx0XHRcdFx0XHRcdCQoJyNjYXRlZ29yeU1vZGFsJykubW9kYWwoJ3RvZ2dsZScpO1xyXG5cclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fWVsc2V7XHJcblx0XHRcdFx0dG9hc3RyLmVycm9yKCcnLCdDYXRlZ29yeSBOYW1lIGlzIHJlcXVpcmVkLicpO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cclxuXHRcdHN0YXR1c0NvbG9yKHN0YXR1cyl7XHJcblx0XHRcdHN3aXRjaChzdGF0dXMpe1xyXG5cdFx0XHRcdGNhc2UgJ1BlbmRpbmcnOiByZXR1cm4gJ2xhYmVsLXdhcm5pbmcnOyBicmVhaztcclxuXHRcdFx0XHRjYXNlICdQcm9jZXNzaW5nJzogcmV0dXJuICdsYWJlbC1kZWZhdWx0JzsgYnJlYWs7XHJcblx0XHRcdFx0Y2FzZSAnQ29tcGxldGVkJzogcmV0dXJuICdsYWJlbC1wcmltYXJ5JzsgYnJlYWs7XHJcblx0XHRcdFx0Y2FzZSAnQ2FuY2VsbGVkJzogcmV0dXJuICdsYWJlbC1kYW5nZXInOyBicmVhaztcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHRzaG93RmlsdGVyU3RhdHVzKHN0YXR1cyl7XHJcblx0XHRcdC8vIGlmKHRoaXMuZmlsdGVyU3RhdHVzICE9ICdhbGwnKVxyXG5cdFx0XHQvLyB7XHJcblx0XHRcdC8vIFx0cmV0dXJuIHRoaXMuZmlsdGVyU3RhdHVzID09IHN0YXR1cy50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0XHQvLyB9XHJcblx0XHRcdC8vIHJldHVybiB0cnVlO1xyXG5cdFx0fSxcclxuXHJcblx0XHRzaG93RmlsdGVyVHlwZSh0eXBlKXtcclxuXHRcdFx0aWYodGhpcy5maWx0ZXJUeXBlICE9ICdhbGwnKVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMuZmlsdGVyVHlwZS50b0xvd2VyQ2FzZSgpID09IHR5cGUudG9Mb3dlckNhc2UoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH0sXHJcblxyXG5cclxuXHRcdHRleHRDb2xvcihzdGF0dXMpe1xyXG5cdFx0XHRzd2l0Y2goc3RhdHVzKVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0Y2FzZSAnUGVuZGluZyc6IHJldHVybiAncm93LXBlbmRpbmcnOyBicmVhaztcclxuXHRcdFx0XHRjYXNlICdDb21wbGV0ZWQnOiByZXR1cm4gJ3Jvdy1jb21wbGV0ZWQnOyBicmVhaztcclxuXHRcdFx0XHRjYXNlICdQcm9jZXNzaW5nJzogcmV0dXJuICdyb3ctcHJvY2Vzc2luZyc7IGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgJ0NhbmNlbGxlZCc6IHJldHVybiAncm93LWNhbmNlbGxlZCc7IGJyZWFrO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cclxuXHR9LFxyXG5cclxuXHRjcmVhdGVkKCl7XHJcblx0XHRheGlvc0FQSXYxLmdldCgnL2NhdGVnb3J5JylcclxuXHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdGlmKHJlc3BvbnNlLnN0YXR1cyA9PSAyMDApXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0dGhpcy5jYXRlZ29yaWVzID0gcmVzcG9uc2UuZGF0YS5kYXRhLmNhdHM7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdH0sXHJcblxyXG5cdG1vdW50ZWQoKXtcclxuXHJcblx0fSxcclxuXHJcbn0pXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvcGFnZXMvY2F0ZWdvcnkuanMiXSwic291cmNlUm9vdCI6IiJ9