!function(t){function r(o){if(e[o])return e[o].exports;var n=e[o]={i:o,l:!1,exports:{}};return t[o].call(n.exports,n,n.exports,r),n.l=!0,n.exports}var e={};r.m=t,r.c=e,r.i=function(t){return t},r.d=function(t,e,o){r.o(t,e)||Object.defineProperty(t,e,{configurable:!1,enumerable:!0,get:o})},r.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return r.d(e,"a",e),e},r.o=function(t,r){return Object.prototype.hasOwnProperty.call(t,r)},r.p="",r(r.s=15)}({"0+lg":function(t,r,e){"use strict";/*!
 * set-value <https://github.com/jonschlinkert/set-value>
 *
 * Copyright (c) 2014-2015, 2017, Jon Schlinkert.
 * Released under the MIT License.
 */
var o=e("ICQ4"),n=e("hWo9"),i=e("4OB8"),a=e("TLSz");t.exports=function(t,r,e){if(!a(t))return t;if(Array.isArray(r)&&(r=o(r)),"string"!=typeof r)return t;for(var s,c=r.split("."),u=c.length,l=-1,f=t;++l<u;){for(var p=c[l];"\\"===p[p.length-1];)p=p.slice(0,-1)+"."+c[++l];if(l===u-1){s=p;break}a(t[p])||(t[p]={}),t=t[p]}return t.hasOwnProperty(s)&&a(t[s])&&i(e)?n(t[s],e):t[s]=e,f}},15:function(t,r,e){t.exports=e("QD6i")},"3DW9":function(t,r,e){"use strict";/*!
 * for-in <https://github.com/jonschlinkert/for-in>
 *
 * Copyright (c) 2014-2017, Jon Schlinkert.
 * Released under the MIT License.
 */
t.exports=function(t,r,e){for(var o in t)if(!1===r.call(e,t[o],o,t))break}},"4OB8":function(t,r,e){"use strict";function o(t){return!0===n(t)&&"[object Object]"===Object.prototype.toString.call(t)}/*!
 * is-plain-object <https://github.com/jonschlinkert/is-plain-object>
 *
 * Copyright (c) 2014-2017, Jon Schlinkert.
 * Released under the MIT License.
 */
var n=e("cz0o");t.exports=function(t){var r,e;return!1!==o(t)&&("function"==typeof(r=t.constructor)&&(e=r.prototype,!1!==o(e)&&!1!==e.hasOwnProperty("isPrototypeOf")))}},BzBK:function(t,r,e){"use strict";function o(t,r,e){var n=t.indexOf(r,e);return"\\"===t.charAt(n-1)?o(t,r,n+1):n}/*!
 * split-string <https://github.com/jonschlinkert/split-string>
 *
 * Copyright (c) 2015, 2017, Jon Schlinkert.
 * Released under the MIT License.
 */
var n=e("hWo9");t.exports=function(t,r){if("string"!=typeof t)throw new TypeError("expected a string");"string"==typeof r&&(r={sep:r});for(var e,i=n({sep:"."},r),a=[""],s=t.length,c=-1;++c<s;){var u=t[c],l=t[c+1];if("\\"!==u){if('"'===u){if(-1===(e=o(t,'"',c+1))){if(!1!==i.strict)throw new Error("unclosed double quote: "+t);e=c}u=!0===i.keepDoubleQuotes?t.slice(c,e+1):t.slice(c+1,e),c=e}if("'"===u){if(-1===(e=o(t,"'",c+1))){if(!1!==i.strict)throw new Error("unclosed single quote: "+t);e=c}u=!0===i.keepSingleQuotes?t.slice(c,e+1):t.slice(c+1,e),c=e}u===i.sep?a.push(""):a[a.length-1]+=u}else{var f=!0===i.keepEscaping?u+l:l;a[a.length-1]+=f,c++}}return a}},GPbB:function(t,r,e){"use strict";t.exports=function(t){if(!Array.isArray(t))throw new TypeError("arr-union expects the first argument to be an array.");for(var r=arguments.length,e=0;++e<r;){var o=arguments[e];if(o){Array.isArray(o)||(o=[o]);for(var n=0;n<o.length;n++){var i=o[n];t.indexOf(i)>=0||t.push(i)}}}return t}},GrPg:function(t,r,e){"use strict";function o(t){return null===t||void 0===t?[]:Array.isArray(t)?t:[t]}var n=e("TLSz"),i=e("GPbB"),a=e("UI3N"),s=e("0+lg");t.exports=function(t,r,e){if(!n(t))throw new TypeError("union-value expects the first argument to be an object.");if("string"!=typeof r)throw new TypeError("union-value expects `prop` to be a string.");var c=o(a(t,r));return s(t,r,i(c,o(e))),t}},ICQ4:function(t,r,e){"use strict";function o(t){for(var r=t.length,e=-1,i=[];++e<r;){var a=t[e];"arguments"===n(a)||Array.isArray(a)?i.push.apply(i,o(a)):"string"==typeof a&&i.push(a)}return i}/*!
 * to-object-path <https://github.com/jonschlinkert/to-object-path>
 *
 * Copyright (c) 2015, Jon Schlinkert.
 * Licensed under the MIT License.
 */
var n=e("o7hK");t.exports=function(t){return"arguments"!==n(t)&&(t=arguments),o(t).join(".")}},QD6i:function(t,r,e){var o=window.Laravel.rates;new Vue({el:"#orders-content",data:{sales:[],orderDetails:[],base_url:window.Laravel.base_url,user_id:"",hasDiscount:!1,orderProducts:[],listProd:[],totalsub:0,totaldiscount:0,priceAfterDiscount:0,idProds:[]},created:function(){this.getsales()},methods:{btnReturnBuyerEwallet:function(t){var r=this;swal({title:"Confirm...",html:"Return Ewallet to the Buyer?",type:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Yes"}).then(function(){axiosAPIv1.post("order-details/receive-returned-item",{id:t}).then(function(t){toastr.success("Ewallet Amount returned to the Buyer"),r.getsales(),$("#orderdetails-modal").modal("toggle")}).catch(function(t){toastr.error("Updated Failed.")})}).catch(function(){toastr.success("Please try again.")})},openModal:function(t){var r=this;$(".ibox").children(".ibox-content").toggleClass("sk-loading"),axiosAPIv1.get("/getOrderDetails/"+t).then(function(t){r.orderDetails=t.data.data,r.orderProducts=t.data.data.order.details,axiosAPIv1.get("/user/sales-report/checkIfSameStore/"+t.data.data.product_id+"/"+t.data.data.order_id).then(function(t){r.listProd=t,r.totalShippingFee}),r.user_id=t.data.data.order.user_id,0!=t.data.data.discount?r.hasDiscount=!0:r.hasDiscount=!1,$("#orderdetails-modal").modal("toggle"),$(".ibox").children(".ibox-content").toggleClass("sk-loading")})},showVideoEvidenceModal:function(t,r,e){t.preventDefault(),$("#"+e).modal("hide"),setTimeout(function(){$("a#video-evidence-"+r).ekkoLightbox({wrapping:!1,alwaysShowClose:!0,onHidden:function(t,r){$("#"+e).modal("show")}})},1e3)},getsales:function(){var t=this;axios.all([function(){return $(".ibox").children(".ibox-content").toggleClass("sk-loading"),axiosAPIv1.get("/getOrders")}()]).then(axios.spread(function(r){t.sales=r.data.data})).catch($.noop),this.callDataTables()},callDataTables:function(){setTimeout(function(){$(".dt-sales").DataTable({pageLength:10,responsive:!0,dom:'<"top"lf>r<"clear">t<"bottom"p>',scrollY:"500px"})},500)}},watch:{sales:function(){$(".dt-sales").DataTable().destroy(),this.callDataTables()}},updated:function(){$(".ibox").children(".ibox-content").toggleClass("sk-loading")},computed:{discountedPrice:function(){var t=this.orderDetails.discount/100,r=this.orderDetails.price_per_item,e=t*r,o=r-e;return o=o.toFixed(2)},price_per_item:function(){return 1==this.orderDetails.fee_included?parseFloat(this.orderDetails.price_per_item)+parseFloat(this.orderDetails.shipping_fee)+parseFloat(this.orderDetails.charge)+parseFloat(this.orderDetails.vat):this.orderDetails.price_per_item},discountedAmount:function(){if(null!=this.orderDetails.discount||0!=this.orderDetails.discount)var t=this.orderDetails.sale_price;else var t=0;return t=parseFloat(t).toFixed(2)},discountPercentage:function(){if(null!=this.orderDetails.discount){if(1==this.orderDetails.fee_included){return(100-(parseFloat(this.orderDetails.price_per_item)+parseFloat(this.orderDetails.shipping_fee)+parseFloat(this.orderDetails.charge)+parseFloat(this.orderDetails.vat)-parseFloat(this.orderDetails.discount))/(parseFloat(this.orderDetails.price_per_item)+parseFloat(this.orderDetails.shipping_fee)+parseFloat(this.orderDetails.charge)+parseFloat(this.orderDetails.vat))*100).toFixed(2)}return this.orderDetails.discount}return 0},now:function(){return this.price_per_item-this.discountedAmount},totalAmount:function(){var t=parseFloat(this.orderDetails.subtotal)+parseFloat(this.orderDetails.shipping_fee);return t=t.toFixed(2)},shipping:function(){if(1!=this.orderDetails.fee_included){var t=parseFloat(this.orderDetails.shipping_fee)+parseFloat(this.orderDetails.charge)+parseFloat(this.orderDetails.vat);t=t.toFixed(2)}else var t=0;return t},totalShippingFee:function(){var t=e("p1YS"),r=t(this.listProd.data,"product.store.id"),n=0,i=0,a=0,s=0,c=0,u=0,l=0,f=0,p=0,d=0,h=0,g=0;$.each(r,function(t,r){if(r.length>0){a=0,s=0,c=0;var e=0;d=0;for(var y=0;y<r.length;y++)if("Cancelled"!=r[y].status){n=r[y].product.weight,i=r[y].product.length*r[y].product.width*r[y].product.height/3500,n>i&&(l=n),i>n&&(l=i),a+=parseFloat(l)*parseFloat(r[y].quantity),s+=(r[y].sale_price>0?parseFloat(r[y].sale_price):parseFloat(r[y].price_per_item))*parseFloat(r[y].quantity);var v=r[y].subtotal;c+=parseFloat(v.replace(",",""))}u=1.2*a;var b=0,x=0,D=0,m=0;$.each(o,function(t,r){"SAMM"==o[t].rate_category&&(b=o[t].halfkg,x=o[t].fkg,D=o[t].excess)}),u<=.5?m=b:u>.5&&u<=1?m=x:(u-=1,m=u*parseFloat(D)+parseFloat(x)),m=parseFloat(m)+20;var A=.01*s,w=0;w=s>0&&s<=1e3?.12*(m-1+A):.12*(m-10+A);var F=Math.ceil(m)+Math.ceil(w)+Math.ceil(A);f=s+F,g+=f,e+=c,p+=c,d=e-f,h+=d}}),this.totalsub=p,this.totaldiscount=h,this.priceAfterDiscount=p-h;return this.priceAfterDiscount}}})},Re3r:function(t,r){function e(t){return!!t.constructor&&"function"==typeof t.constructor.isBuffer&&t.constructor.isBuffer(t)}function o(t){return"function"==typeof t.readFloatLE&&"function"==typeof t.slice&&e(t.slice(0,0))}/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
 * @license  MIT
 */
t.exports=function(t){return null!=t&&(e(t)||o(t)||!!t._isBuffer)}},Sg0g:function(t,r,e){"use strict";/*!
 * for-own <https://github.com/jonschlinkert/for-own>
 *
 * Copyright (c) 2014-2017, Jon Schlinkert.
 * Released under the MIT License.
 */
var o=e("3DW9"),n=Object.prototype.hasOwnProperty;t.exports=function(t,r,e){o(t,function(o,i){if(n.call(t,i))return r.call(e,t[i],i,t)})}},TLSz:function(t,r,e){"use strict";/*!
 * is-extendable <https://github.com/jonschlinkert/is-extendable>
 *
 * Copyright (c) 2015, Jon Schlinkert.
 * Licensed under the MIT License.
 */
t.exports=function(t){return void 0!==t&&null!==t&&("object"==typeof t||"function"==typeof t)}},UI3N:function(t,r){function e(t){return null!==t&&("object"==typeof t||"function"==typeof t)}function o(t){return t?Array.isArray(t)?t.join("."):t:""}/*!
 * get-value <https://github.com/jonschlinkert/get-value>
 *
 * Copyright (c) 2014-2015, Jon Schlinkert.
 * Licensed under the MIT License.
 */
t.exports=function(t,r,n,i,a){if(!e(t)||!r)return t;if(r=o(r),n&&(r+="."+o(n)),i&&(r+="."+o(i)),a&&(r+="."+o(a)),r in t)return t[r];for(var s=r.split("."),c=s.length,u=-1;t&&++u<c;){for(var l=s[u];"\\"===l[l.length-1];)l=l.slice(0,-1)+"."+s[++u];t=t[l]}return t}},cz0o:function(t,r,e){"use strict";/*!
 * isobject <https://github.com/jonschlinkert/isobject>
 *
 * Copyright (c) 2014-2015, Jon Schlinkert.
 * Licensed under the MIT License.
 */
t.exports=function(t){return null!=t&&"object"==typeof t&&!1===Array.isArray(t)}},hWo9:function(t,r,e){"use strict";function o(t,r){for(var e in r)n(r,e)&&(t[e]=r[e])}function n(t,r){return Object.prototype.hasOwnProperty.call(t,r)}var i=e("TLSz");t.exports=function(t){i(t)||(t={});for(var r=arguments.length,e=1;e<r;e++){var n=arguments[e];i(n)&&o(t,n)}return t}},lmZO:function(t,r,e){"use strict";function o(t,r){for(var e=t.length,n=-1;++n<e;){var i=t[n];Array.isArray(i)?o(i,r):r.push(i)}return r}/*!
 * arr-flatten <https://github.com/jonschlinkert/arr-flatten>
 *
 * Copyright (c) 2014-2015, 2017, Jon Schlinkert.
 * Released under the MIT License.
 */
t.exports=function(t){return o(t,[])}},o7hK:function(t,r,e){var o=e("Re3r"),n=Object.prototype.toString;t.exports=function(t){if(void 0===t)return"undefined";if(null===t)return"null";if(!0===t||!1===t||t instanceof Boolean)return"boolean";if("string"==typeof t||t instanceof String)return"string";if("number"==typeof t||t instanceof Number)return"number";if("function"==typeof t||t instanceof Function)return"function";if(void 0!==Array.isArray&&Array.isArray(t))return"array";if(t instanceof RegExp)return"regexp";if(t instanceof Date)return"date";var r=n.call(t);return"[object RegExp]"===r?"regexp":"[object Date]"===r?"date":"[object Arguments]"===r?"arguments":"[object Error]"===r?"error":o(t)?"buffer":"[object Set]"===r?"set":"[object WeakSet]"===r?"weakset":"[object Map]"===r?"map":"[object WeakMap]"===r?"weakmap":"[object Symbol]"===r?"symbol":"[object Int8Array]"===r?"int8array":"[object Uint8Array]"===r?"uint8array":"[object Uint8ClampedArray]"===r?"uint8clampedarray":"[object Int16Array]"===r?"int16array":"[object Uint16Array]"===r?"uint16array":"[object Int32Array]"===r?"int32array":"[object Uint32Array]"===r?"uint32array":"[object Float32Array]"===r?"float32array":"[object Float64Array]"===r?"float64array":"object"}},p1YS:function(t,r,e){"use strict";function o(t,r){if(null==t)return[];if(!Array.isArray(t))throw new TypeError("group-array expects an array.");if(1===arguments.length)return t;for(var e=u([].slice.call(arguments,1)),o=n(t,e[0]),i=1;i<e.length;i++)a(o,e[i]);return o}function n(t,r,e){for(var o={},n=0;n<t.length;n++){var a,c=t[n];switch(a="function"==typeof r?r.call(o,c,e):d(c,r),p(a)){case"undefined":break;case"string":case"number":case"boolean":l(o,s(String(a)),c);break;case"object":case"array":i(o,c,a);break;case"function":throw new Error("invalid argument type: "+e)}}return o}function i(t,r,e){Array.isArray(e)?e.forEach(function(e){l(t,s(e),r)}):f(e,function(e,o){l(t,s(o),r)})}function a(t,r){return f(t,function(e,o){Array.isArray(e)?t[o]=n(e,r,o):t[o]=a(e,r,o)}),t}function s(t){var r={strict:!1,keepEscaping:!0,keepDoubleQuotes:!0,keepSingleQuotes:!0};try{return c(t,r).join("\\.")}catch(r){return t}}/*!
 * group-array <https://github.com/doowb/group-array>
 *
 * Copyright (c) 2015, 2017, Brian Woodward.
 * Released under the MIT License.
 */
var c=e("BzBK"),u=e("lmZO"),l=e("GrPg"),f=e("Sg0g"),p=e("o7hK"),d=e("UI3N");t.exports=o}});