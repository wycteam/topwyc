/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 467);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuejs_paginate__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuejs_paginate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vuejs_paginate__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_WycIssue_vue__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_WycIssue_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_WycIssue_vue__);



new Vue({
    el: '#customer-service',

    data: {
        issues: [],
        page: 1,
        pageCount: 1,
        search: null,
        timeout: null
    },

    watch: {
        search: function search(val) {
            clearTimeout(this.timeout);

            this.page = 1;
            this.timeout = setTimeout(this.getIssues, 200);
        }
    },

    methods: {
        getIssues: function getIssues() {
            var _this = this;

            return axiosAPIv1.get('/issues', {
                params: {
                    page: this.page,
                    search: this.search
                }
            }).then(function (result) {
                _this.issues = result.data.data;
                _this.page = result.data.current_page;
                _this.pageCount = result.data.last_page;
            });
        },
        clickCallback: function clickCallback(page) {
            this.page = page;

            this.getIssues();
        }
    },

    components: {
        Paginate: __WEBPACK_IMPORTED_MODULE_0_vuejs_paginate___default.a,
        WycIssue: __WEBPACK_IMPORTED_MODULE_1__components_WycIssue_vue___default.a
    },

    created: function created() {
        this.getIssues();
    }
});

/***/ }),

/***/ 261:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['issue'],

    methods: {},

    computed: {
        statusClass: function statusClass() {
            return this.issue.status == 'open' ? 'label-primary' : 'label-danger';
        },
        status: function status() {
            return this.issue.status.charAt(0).toUpperCase() + this.issue.status.slice(1);
        },
        href: function href() {
            return '/customer-service/' + this.issue.id;
        },
        createdOn: function createdOn() {
            return moment.utc(this.issue.created_at).format('MMMM DD, YYYY - h:mm:ss A');
        },
        users: function users() {
            if (this.issue.chat_room) {
                return this.issue.chat_room.users;
            }

            return [];
        }
    },

    created: function created() {
        //
    },
    mounted: function mounted() {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    }
});

/***/ }),

/***/ 354:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(261),
  /* template */
  __webpack_require__(403),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/WycIssue.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] WycIssue.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-63948e2c", Component.options)
  } else {
    hotAPI.reload("data-v-63948e2c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 403:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('tr', [_c('td', [_vm._v(_vm._s((_vm.issue.chat_room != null) ? _vm.issue.chat_room.id : ''))]), _vm._v(" "), _c('td', {
    staticClass: "project-status"
  }, [_c('span', {
    staticClass: "label",
    class: _vm.statusClass
  }, [_vm._v(_vm._s(_vm.status))])]), _vm._v(" "), _c('td', {
    staticClass: "project-title"
  }, [_c('a', {
    attrs: {
      "href": _vm.href
    }
  }, [_vm._v(_vm._s(_vm.issue.subject))]), _vm._v(" "), _c('br'), _vm._v(" "), _c('small', [_vm._v("Created by: "), _c('strong', [_vm._v(_vm._s(_vm.issue.user.full_name))]), _vm._v(" on " + _vm._s(_vm.createdOn))])]), _vm._v(" "), _c('td', {
    staticClass: "project-people"
  }, [_vm._l((_vm.users), function(user) {
    return _c('img', {
      staticClass: "img-circle",
      attrs: {
        "src": user.avatar,
        "data-toggle": "tooltip",
        "title": user.full_name
      }
    })
  }), _vm._v(" "), (!_vm.users.length) ? _c('span', {
    staticClass: "pull-right"
  }, [_vm._v("No assigned support user")]) : _vm._e()], 2), _vm._v(" "), _c('td', {
    staticClass: "project-actions"
  }, [_c('a', {
    staticClass: "btn btn-white btn-sm",
    attrs: {
      "href": _vm.href
    }
  }, [_c('i', {
    staticClass: "fa fa-eye"
  }), _vm._v(" View")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-63948e2c", module.exports)
  }
}

/***/ }),

/***/ 455:
/***/ (function(module, exports, __webpack_require__) {

!function(e,t){ true?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports.VuejsPaginate=t():e.VuejsPaginate=t()}(this,function(){return function(e){function t(s){if(n[s])return n[s].exports;var a=n[s]={exports:{},id:s,loaded:!1};return e[s].call(a.exports,a,a.exports,t),a.loaded=!0,a.exports}var n={};return t.m=e,t.c=n,t.p="",t(0)}([function(e,t,n){"use strict";function s(e){return e&&e.__esModule?e:{default:e}}var a=n(1),i=s(a);e.exports=i.default},function(e,t,n){n(2);var s=n(6)(n(7),n(8),"data-v-82963a40",null);e.exports=s.exports},function(e,t,n){var s=n(3);"string"==typeof s&&(s=[[e.id,s,""]]);n(5)(s,{});s.locals&&(e.exports=s.locals)},function(e,t,n){t=e.exports=n(4)(),t.push([e.id,"a[data-v-82963a40]{cursor:pointer}",""])},function(e,t){e.exports=function(){var e=[];return e.toString=function(){for(var e=[],t=0;t<this.length;t++){var n=this[t];n[2]?e.push("@media "+n[2]+"{"+n[1]+"}"):e.push(n[1])}return e.join("")},e.i=function(t,n){"string"==typeof t&&(t=[[null,t,""]]);for(var s={},a=0;a<this.length;a++){var i=this[a][0];"number"==typeof i&&(s[i]=!0)}for(a=0;a<t.length;a++){var r=t[a];"number"==typeof r[0]&&s[r[0]]||(n&&!r[2]?r[2]=n:n&&(r[2]="("+r[2]+") and ("+n+")"),e.push(r))}},e}},function(e,t,n){function s(e,t){for(var n=0;n<e.length;n++){var s=e[n],a=u[s.id];if(a){a.refs++;for(var i=0;i<a.parts.length;i++)a.parts[i](s.parts[i]);for(;i<s.parts.length;i++)a.parts.push(o(s.parts[i],t))}else{for(var r=[],i=0;i<s.parts.length;i++)r.push(o(s.parts[i],t));u[s.id]={id:s.id,refs:1,parts:r}}}}function a(e){for(var t=[],n={},s=0;s<e.length;s++){var a=e[s],i=a[0],r=a[1],l=a[2],o=a[3],d={css:r,media:l,sourceMap:o};n[i]?n[i].parts.push(d):t.push(n[i]={id:i,parts:[d]})}return t}function i(e,t){var n=g(),s=C[C.length-1];if("top"===e.insertAt)s?s.nextSibling?n.insertBefore(t,s.nextSibling):n.appendChild(t):n.insertBefore(t,n.firstChild),C.push(t);else{if("bottom"!==e.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");n.appendChild(t)}}function r(e){e.parentNode.removeChild(e);var t=C.indexOf(e);t>=0&&C.splice(t,1)}function l(e){var t=document.createElement("style");return t.type="text/css",i(e,t),t}function o(e,t){var n,s,a;if(t.singleton){var i=v++;n=h||(h=l(t)),s=d.bind(null,n,i,!1),a=d.bind(null,n,i,!0)}else n=l(t),s=c.bind(null,n),a=function(){r(n)};return s(e),function(t){if(t){if(t.css===e.css&&t.media===e.media&&t.sourceMap===e.sourceMap)return;s(e=t)}else a()}}function d(e,t,n,s){var a=n?"":s.css;if(e.styleSheet)e.styleSheet.cssText=b(t,a);else{var i=document.createTextNode(a),r=e.childNodes;r[t]&&e.removeChild(r[t]),r.length?e.insertBefore(i,r[t]):e.appendChild(i)}}function c(e,t){var n=t.css,s=t.media,a=t.sourceMap;if(s&&e.setAttribute("media",s),a&&(n+="\n/*# sourceURL="+a.sources[0]+" */",n+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(a))))+" */"),e.styleSheet)e.styleSheet.cssText=n;else{for(;e.firstChild;)e.removeChild(e.firstChild);e.appendChild(document.createTextNode(n))}}var u={},p=function(e){var t;return function(){return"undefined"==typeof t&&(t=e.apply(this,arguments)),t}},f=p(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),g=p(function(){return document.head||document.getElementsByTagName("head")[0]}),h=null,v=0,C=[];e.exports=function(e,t){t=t||{},"undefined"==typeof t.singleton&&(t.singleton=f()),"undefined"==typeof t.insertAt&&(t.insertAt="bottom");var n=a(e);return s(n,t),function(e){for(var i=[],r=0;r<n.length;r++){var l=n[r],o=u[l.id];o.refs--,i.push(o)}if(e){var d=a(e);s(d,t)}for(var r=0;r<i.length;r++){var o=i[r];if(0===o.refs){for(var c=0;c<o.parts.length;c++)o.parts[c]();delete u[o.id]}}}};var b=function(){var e=[];return function(t,n){return e[t]=n,e.filter(Boolean).join("\n")}}()},function(e,t){e.exports=function(e,t,n,s){var a,i=e=e||{},r=typeof e.default;"object"!==r&&"function"!==r||(a=e,i=e.default);var l="function"==typeof i?i.options:i;if(t&&(l.render=t.render,l.staticRenderFns=t.staticRenderFns),n&&(l._scopeId=n),s){var o=l.computed||(l.computed={});Object.keys(s).forEach(function(e){var t=s[e];o[e]=function(){return t}})}return{esModule:a,exports:i,options:l}}},function(e,t){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default={props:{pageCount:{type:Number,required:!0},initialPage:{type:Number,default:0},forcePage:{type:Number},clickHandler:{type:Function,default:function(){}},pageRange:{type:Number,default:3},marginPages:{type:Number,default:1},prevText:{type:String,default:"Prev"},nextText:{type:String,default:"Next"},breakViewText:{type:String,default:"…"},containerClass:{type:String},pageClass:{type:String},pageLinkClass:{type:String},prevClass:{type:String},prevLinkClass:{type:String},nextClass:{type:String},nextLinkClass:{type:String},breakViewClass:{type:String},breakViewLinkClass:{type:String},activeClass:{type:String,default:"active"},disabledClass:{type:String,default:"disabled"},noLiSurround:{type:Boolean,default:!1},firstLastButton:{type:Boolean,default:!1},firstButtonText:{type:String,default:"First"},lastButtonText:{type:String,default:"Last"},hidePrevNext:{type:Boolean,default:!1}},data:function(){return{selected:this.initialPage}},beforeUpdate:function(){void 0!==this.forcePage&&this.forcePage!==this.selected&&(this.selected=this.forcePage)},computed:{pages:function(){var e=this,t={};if(this.pageCount<=this.pageRange)for(var n=0;n<this.pageCount;n++){var s={index:n,content:n+1,selected:n===this.selected};t[n]=s}else{for(var a=Math.floor(this.pageRange/2),i=function(n){var s={index:n,content:n+1,selected:n===e.selected};t[n]=s},r=function(e){var n={disabled:!0,breakView:!0};t[e]=n},l=0;l<this.marginPages;l++)i(l);var o=0;this.selected-a>0&&(o=this.selected-a);var d=o+this.pageRange-1;d>=this.pageCount&&(d=this.pageCount-1,o=d-this.pageRange+1);for(var c=o;c<=d&&c<=this.pageCount-1;c++)i(c);o>this.marginPages&&r(o-1),d+1<this.pageCount-this.marginPages&&r(d+1);for(var u=this.pageCount-1;u>=this.pageCount-this.marginPages;u--)i(u)}return t}},methods:{handlePageSelected:function(e){this.selected!==e&&(this.selected=e,this.clickHandler(this.selected+1))},prevPage:function(){this.selected<=0||(this.selected--,this.clickHandler(this.selected+1))},nextPage:function(){this.selected>=this.pageCount-1||(this.selected++,this.clickHandler(this.selected+1))},firstPageSelected:function(){return 0===this.selected},lastPageSelected:function(){return this.selected===this.pageCount-1||0===this.pageCount},selectFirstPage:function(){this.selected<=0||(this.selected=0,this.clickHandler(this.selected+1))},selectLastPage:function(){this.selected>=this.pageCount-1||(this.selected=this.pageCount-1,this.clickHandler(this.selected+1))}}}},function(e,t){e.exports={render:function(){var e=this,t=e.$createElement,n=e._self._c||t;return e.noLiSurround?n("div",{class:e.containerClass},[e.firstLastButton?n("a",{class:[e.pageLinkClass,e.firstPageSelected()?e.disabledClass:""],attrs:{tabindex:"0"},domProps:{innerHTML:e._s(e.firstButtonText)},on:{click:function(t){e.selectFirstPage()},keyup:function(t){return"button"in t||!e._k(t.keyCode,"enter",13)?void e.selectFirstPage():null}}}):e._e(),e._v(" "),e.firstPageSelected()&&e.hidePrevNext?e._e():n("a",{class:[e.prevLinkClass,e.firstPageSelected()?e.disabledClass:""],attrs:{tabindex:"0"},domProps:{innerHTML:e._s(e.prevText)},on:{click:function(t){e.prevPage()},keyup:function(t){return"button"in t||!e._k(t.keyCode,"enter",13)?void e.prevPage():null}}}),e._v(" "),e._l(e.pages,function(t){return[t.breakView?n("a",{class:[e.pageLinkClass,e.breakViewLinkClass,t.disabled?e.disabledClass:""],attrs:{tabindex:"0"}},[e._t("breakViewContent",[e._v(e._s(e.breakViewText))])],2):t.disabled?n("a",{class:[e.pageLinkClass,t.selected?e.activeClass:"",e.disabledClass],attrs:{tabindex:"0"}},[e._v(e._s(t.content))]):n("a",{class:[e.pageLinkClass,t.selected?e.activeClass:""],attrs:{tabindex:"0"},on:{click:function(n){e.handlePageSelected(t.index)},keyup:function(n){return"button"in n||!e._k(n.keyCode,"enter",13)?void e.handlePageSelected(t.index):null}}},[e._v(e._s(t.content))])]}),e._v(" "),e.lastPageSelected()&&e.hidePrevNext?e._e():n("a",{class:[e.nextLinkClass,e.lastPageSelected()?e.disabledClass:""],attrs:{tabindex:"0"},domProps:{innerHTML:e._s(e.nextText)},on:{click:function(t){e.nextPage()},keyup:function(t){return"button"in t||!e._k(t.keyCode,"enter",13)?void e.nextPage():null}}}),e._v(" "),e.firstLastButton?n("a",{class:[e.pageLinkClass,e.lastPageSelected()?e.disabledClass:""],attrs:{tabindex:"0"},domProps:{innerHTML:e._s(e.lastButtonText)},on:{click:function(t){e.selectLastPage()},keyup:function(t){return"button"in t||!e._k(t.keyCode,"enter",13)?void e.selectLastPage():null}}}):e._e()],2):n("ul",{class:e.containerClass},[e.firstLastButton?n("li",{class:[e.pageClass,e.firstPageSelected()?e.disabledClass:""]},[n("a",{class:e.pageLinkClass,attrs:{tabindex:e.firstPageSelected()?-1:0},domProps:{innerHTML:e._s(e.firstButtonText)},on:{click:function(t){e.selectFirstPage()},keyup:function(t){return"button"in t||!e._k(t.keyCode,"enter",13)?void e.selectFirstPage():null}}})]):e._e(),e._v(" "),e.firstPageSelected()&&e.hidePrevNext?e._e():n("li",{class:[e.prevClass,e.firstPageSelected()?e.disabledClass:""]},[n("a",{class:e.prevLinkClass,attrs:{tabindex:e.firstPageSelected()?-1:0},domProps:{innerHTML:e._s(e.prevText)},on:{click:function(t){e.prevPage()},keyup:function(t){return"button"in t||!e._k(t.keyCode,"enter",13)?void e.prevPage():null}}})]),e._v(" "),e._l(e.pages,function(t){return n("li",{class:[e.pageClass,t.selected?e.activeClass:"",t.disabled?e.disabledClass:"",t.breakView?e.breakViewClass:""]},[t.breakView?n("a",{class:[e.pageLinkClass,e.breakViewLinkClass],attrs:{tabindex:"0"}},[e._t("breakViewContent",[e._v(e._s(e.breakViewText))])],2):t.disabled?n("a",{class:e.pageLinkClass,attrs:{tabindex:"0"}},[e._v(e._s(t.content))]):n("a",{class:e.pageLinkClass,attrs:{tabindex:"0"},on:{click:function(n){e.handlePageSelected(t.index)},keyup:function(n){return"button"in n||!e._k(n.keyCode,"enter",13)?void e.handlePageSelected(t.index):null}}},[e._v(e._s(t.content))])])}),e._v(" "),e.lastPageSelected()&&e.hidePrevNext?e._e():n("li",{class:[e.nextClass,e.lastPageSelected()?e.disabledClass:""]},[n("a",{class:e.nextLinkClass,attrs:{tabindex:e.lastPageSelected()?-1:0},domProps:{innerHTML:e._s(e.nextText)},on:{click:function(t){e.nextPage()},keyup:function(t){return"button"in t||!e._k(t.keyCode,"enter",13)?void e.nextPage():null}}})]),e._v(" "),e.firstLastButton?n("li",{class:[e.pageClass,e.lastPageSelected()?e.disabledClass:""]},[n("a",{class:e.pageLinkClass,attrs:{tabindex:e.lastPageSelected()?-1:0},domProps:{innerHTML:e._s(e.lastButtonText)},on:{click:function(t){e.selectLastPage()},keyup:function(t){return"button"in t||!e._k(t.keyCode,"enter",13)?void e.selectLastPage():null}}})]):e._e()],2)},staticRenderFns:[]}}])});

/***/ }),

/***/ 467:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(177);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjMiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvcGFnZXMvY3VzdG9tZXItc2VydmljZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vV3ljSXNzdWUudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljSXNzdWUudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljSXNzdWUudnVlP2NmNWQiLCJ3ZWJwYWNrOi8vLy4vfi92dWVqcy1wYWdpbmF0ZS9kaXN0L2luZGV4LmpzIl0sIm5hbWVzIjpbIlZ1ZSIsImVsIiwiZGF0YSIsImlzc3VlcyIsInBhZ2UiLCJwYWdlQ291bnQiLCJzZWFyY2giLCJ0aW1lb3V0Iiwid2F0Y2giLCJ2YWwiLCJjbGVhclRpbWVvdXQiLCJzZXRUaW1lb3V0IiwiZ2V0SXNzdWVzIiwibWV0aG9kcyIsImF4aW9zQVBJdjEiLCJnZXQiLCJwYXJhbXMiLCJ0aGVuIiwicmVzdWx0IiwiY3VycmVudF9wYWdlIiwibGFzdF9wYWdlIiwiY2xpY2tDYWxsYmFjayIsImNvbXBvbmVudHMiLCJQYWdpbmF0ZSIsIld5Y0lzc3VlIiwiY3JlYXRlZCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQ2xEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7QUFFQSxJQUFJQSxHQUFKLENBQVE7QUFDSkMsUUFBSSxtQkFEQTs7QUFHSkMsVUFBTTtBQUNGQyxnQkFBUSxFQUROO0FBRUZDLGNBQU0sQ0FGSjtBQUdGQyxtQkFBVyxDQUhUO0FBSUZDLGdCQUFRLElBSk47QUFLRkMsaUJBQVM7QUFMUCxLQUhGOztBQVdKQyxXQUFPO0FBQ0hGLGNBREcsa0JBQ0lHLEdBREosRUFDUztBQUNSQyx5QkFBYSxLQUFLSCxPQUFsQjs7QUFFQSxpQkFBS0gsSUFBTCxHQUFZLENBQVo7QUFDQSxpQkFBS0csT0FBTCxHQUFlSSxXQUFXLEtBQUtDLFNBQWhCLEVBQTJCLEdBQTNCLENBQWY7QUFDSDtBQU5FLEtBWEg7O0FBb0JKQyxhQUFTO0FBQ0xELGlCQURLLHVCQUNPO0FBQUE7O0FBQ1IsbUJBQU9FLFdBQVdDLEdBQVgsQ0FBZSxTQUFmLEVBQTBCO0FBQzdCQyx3QkFBUTtBQUNKWiwwQkFBTSxLQUFLQSxJQURQO0FBRUpFLDRCQUFRLEtBQUtBO0FBRlQ7QUFEcUIsYUFBMUIsRUFLSlcsSUFMSSxDQUtDLGtCQUFVO0FBQ2Qsc0JBQUtkLE1BQUwsR0FBY2UsT0FBT2hCLElBQVAsQ0FBWUEsSUFBMUI7QUFDQSxzQkFBS0UsSUFBTCxHQUFZYyxPQUFPaEIsSUFBUCxDQUFZaUIsWUFBeEI7QUFDQSxzQkFBS2QsU0FBTCxHQUFpQmEsT0FBT2hCLElBQVAsQ0FBWWtCLFNBQTdCO0FBQ0gsYUFUTSxDQUFQO0FBVUgsU0FaSTtBQWNMQyxxQkFkSyx5QkFjU2pCLElBZFQsRUFjZTtBQUNoQixpQkFBS0EsSUFBTCxHQUFZQSxJQUFaOztBQUVBLGlCQUFLUSxTQUFMO0FBQ0g7QUFsQkksS0FwQkw7O0FBeUNKVSxnQkFBWTtBQUNSQyx3RUFEUTtBQUVSQyxrRkFBUUE7QUFGQSxLQXpDUjs7QUE4Q0pDLFdBOUNJLHFCQThDTTtBQUNOLGFBQUtiLFNBQUw7QUFDSDtBQWhERyxDQUFSLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNtQkE7QUFDQSxvQkFEQTs7QUFHQSxlQUhBOztBQU9BO0FBQ0EsbUJBREEseUJBQ0E7QUFDQSxpREFDQSxlQURBLEdBRUEsY0FGQTtBQUdBLFNBTEE7QUFPQSxjQVBBLG9CQU9BO0FBQ0E7QUFDQSxTQVRBO0FBV0EsWUFYQSxrQkFXQTtBQUNBO0FBQ0EsU0FiQTtBQWVBLGlCQWZBLHVCQWVBO0FBQ0E7QUFDQSxTQWpCQTtBQW1CQSxhQW5CQSxtQkFtQkE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQXpCQSxLQVBBOztBQW1DQSxXQW5DQSxxQkFtQ0E7QUFDQTtBQUNBLEtBckNBO0FBdUNBLFdBdkNBLHFCQXVDQTtBQUNBO0FBQ0E7QUFDQSxTQUZBO0FBR0E7QUEzQ0EsRzs7Ozs7OztBQ3RCQSxnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFrRTtBQUMxRjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFtTztBQUM3TztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUE0TDtBQUN0TTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMxQ0EsZUFBZSxLQUFpRCw4SUFBOEksaUJBQWlCLG1CQUFtQixjQUFjLDRCQUE0QixZQUFZLFVBQVUsaUJBQWlCLGdFQUFnRSxTQUFTLCtCQUErQixrQkFBa0IsYUFBYSxjQUFjLDBCQUEwQixXQUFXLGtCQUFrQixvQkFBb0IsaUJBQWlCLEtBQUssNkNBQTZDLG9CQUFvQixpQkFBaUIsV0FBVyxzQ0FBc0MsU0FBUyxFQUFFLCtCQUErQixpQkFBaUIsb0RBQW9ELGVBQWUsT0FBTyxlQUFlLHFCQUFxQixTQUFTLDZCQUE2QixpQkFBaUIsY0FBYyxLQUFLLGNBQWMsNkJBQTZCLFNBQVMsZ0JBQWdCLGtCQUFrQixtQkFBbUIsc0NBQXNDLFlBQVksS0FBSyxjQUFjLEtBQUssaUJBQWlCLDhCQUE4QixRQUFRLFdBQVcsS0FBSyxXQUFXLGdHQUFnRyxJQUFJLGlCQUFpQixnQkFBZ0IsWUFBWSxXQUFXLEtBQUsscUJBQXFCLE1BQU0sU0FBUyxZQUFZLGlCQUFpQiwyQkFBMkIsS0FBSyxpQkFBaUIsa0NBQWtDLEtBQUssaUJBQWlCLGlCQUFpQiw0QkFBNEIsU0FBUywwQkFBMEIsY0FBYyxpQkFBaUIsS0FBSyxXQUFXLEtBQUssMENBQTBDLDJCQUEyQixxQ0FBcUMsZUFBZSxFQUFFLFNBQVMsZ0JBQWdCLDBCQUEwQixnSUFBZ0ksS0FBSywrR0FBK0csa0JBQWtCLGNBQWMsNEJBQTRCLG1CQUFtQixvQkFBb0IsY0FBYyxzQ0FBc0Msa0NBQWtDLGdCQUFnQixVQUFVLGdCQUFnQixVQUFVLDBEQUEwRCwwQ0FBMEMsTUFBTSx3QkFBd0IsTUFBTSxzRUFBc0UsT0FBTyxVQUFVLG9CQUFvQixpQkFBaUIsNENBQTRDLEtBQUssZ0RBQWdELDRFQUE0RSxnQkFBZ0Isb0NBQW9DLDhIQUE4SCwwR0FBMEcsS0FBSyxLQUFLLGFBQWEsNkJBQTZCLDJDQUEyQyxRQUFRLGVBQWUsTUFBTSxrQkFBa0IsNERBQTRELGdCQUFnQixvRUFBb0UsaUJBQWlCLCtEQUErRCxrQkFBa0Isd0JBQXdCLE9BQU8sMEdBQTBHLFdBQVcsMEJBQTBCLGlCQUFpQixXQUFXLEtBQUsscUJBQXFCLG1CQUFtQixNQUFNLFdBQVcsT0FBTyxZQUFZLFdBQVcsS0FBSyxXQUFXLGVBQWUsWUFBWSxpQkFBaUIsaUJBQWlCLG1CQUFtQixpQkFBaUIsU0FBUyxxQkFBcUIsNENBQTRDLEdBQUcsZUFBZSw0QkFBNEIsZUFBZSxvQkFBb0IsZ0RBQWdELHVDQUF1QyxtRkFBbUYsZ0NBQWdDLEVBQUUsbUNBQW1DLFdBQVcsZ0JBQWdCLFVBQVUsRUFBRSxPQUFPLGlDQUFpQyxlQUFlLGFBQWEsc0NBQXNDLFNBQVMsYUFBYSxPQUFPLFdBQVcsd0JBQXdCLGNBQWMsc0JBQXNCLFlBQVksWUFBWSxlQUFlLG1DQUFtQyxZQUFZLHNCQUFzQixjQUFjLHNCQUFzQixXQUFXLDJCQUEyQixXQUFXLDJCQUEyQixnQkFBZ0Isd0JBQXdCLGlCQUFpQixZQUFZLFlBQVksWUFBWSxnQkFBZ0IsWUFBWSxZQUFZLFlBQVksZ0JBQWdCLFlBQVksWUFBWSxZQUFZLGdCQUFnQixZQUFZLGlCQUFpQixZQUFZLHFCQUFxQixZQUFZLGNBQWMsNkJBQTZCLGdCQUFnQiwrQkFBK0IsZUFBZSx3QkFBd0Isa0JBQWtCLHdCQUF3QixrQkFBa0IsNEJBQTRCLGlCQUFpQiwyQkFBMkIsZUFBZSx5QkFBeUIsaUJBQWlCLE9BQU8sMkJBQTJCLHlCQUF5Qix3RkFBd0YsV0FBVyxpQkFBaUIsZ0JBQWdCLDhDQUE4QyxpQkFBaUIsS0FBSyxPQUFPLGdEQUFnRCxPQUFPLEtBQUsscURBQXFELE9BQU8sNkNBQTZDLE9BQU8sZUFBZSxPQUFPLDBCQUEwQixPQUFPLEtBQUssbUJBQW1CLFNBQVMsUUFBUSx1Q0FBdUMseUJBQXlCLDZEQUE2RCxZQUFZLDBCQUEwQixTQUFTLHVFQUF1RSwyQkFBMkIsbUNBQW1DLFNBQVMsVUFBVSxVQUFVLCtCQUErQix3RUFBd0UscUJBQXFCLHVFQUF1RSxxQkFBcUIsc0ZBQXNGLDhCQUE4Qix5QkFBeUIsNkJBQTZCLDREQUE0RCw0QkFBNEIsdUVBQXVFLDJCQUEyQix3R0FBd0csZUFBZSxXQUFXLGtCQUFrQiw4Q0FBOEMsK0JBQStCLHVCQUF1QiwyQkFBMkIsd0VBQXdFLGFBQWEsV0FBVyxrQ0FBa0MsS0FBSyxrQkFBa0Isb0JBQW9CLG1CQUFtQixnRkFBZ0YsdUVBQXVFLHdFQUF3RSxhQUFhLFdBQVcsMkJBQTJCLEtBQUssa0JBQWtCLGFBQWEsbUJBQW1CLHlFQUF5RSxxQ0FBcUMsMEJBQTBCLGtGQUFrRixjQUFjLCtFQUErRSwyRUFBMkUsY0FBYyxpQ0FBaUMsMkRBQTJELGFBQWEsS0FBSyxrQkFBa0IsOEJBQThCLG1CQUFtQiwwRkFBMEYsMkJBQTJCLCtEQUErRCx1RUFBdUUsYUFBYSxXQUFXLDJCQUEyQixLQUFLLGtCQUFrQixhQUFhLG1CQUFtQix5RUFBeUUscUNBQXFDLHVFQUF1RSxhQUFhLFdBQVcsaUNBQWlDLEtBQUssa0JBQWtCLG1CQUFtQixtQkFBbUIsK0VBQStFLHFCQUFxQix1QkFBdUIsNEJBQTRCLDZEQUE2RCxTQUFTLDZCQUE2QixvQ0FBb0MsV0FBVyxrQ0FBa0MsS0FBSyxrQkFBa0Isb0JBQW9CLG1CQUFtQixnRkFBZ0YsMEVBQTBFLDZEQUE2RCxTQUFTLDZCQUE2QixvQ0FBb0MsV0FBVywyQkFBMkIsS0FBSyxrQkFBa0IsYUFBYSxtQkFBbUIseUVBQXlFLHVDQUF1QyxlQUFlLDhHQUE4RyxxQkFBcUIsb0RBQW9ELGNBQWMsK0VBQStFLDZCQUE2QixjQUFjLGlDQUFpQyw2QkFBNkIsYUFBYSxLQUFLLGtCQUFrQiw4QkFBOEIsbUJBQW1CLDBGQUEwRiw0QkFBNEIsZ0VBQWdFLDREQUE0RCxTQUFTLDZCQUE2QixtQ0FBbUMsV0FBVywyQkFBMkIsS0FBSyxrQkFBa0IsYUFBYSxtQkFBbUIseUVBQXlFLHdDQUF3Qyw0REFBNEQsU0FBUyw2QkFBNkIsbUNBQW1DLFdBQVcsaUNBQWlDLEtBQUssa0JBQWtCLG1CQUFtQixtQkFBbUIsK0VBQStFLGVBQWUscUJBQXFCLEdBQUcsRSIsImZpbGUiOiJqcy9wYWdlcy9jdXN0b21lci1zZXJ2aWNlL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0NjcpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGYxMTNlZDM2YTVhNTZiN2RjZjYzIiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUiLCJpbXBvcnQgUGFnaW5hdGUgZnJvbSAndnVlanMtcGFnaW5hdGUnXG5pbXBvcnQgV3ljSXNzdWUgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9XeWNJc3N1ZS52dWUnXG5cbm5ldyBWdWUoe1xuICAgIGVsOiAnI2N1c3RvbWVyLXNlcnZpY2UnLFxuXG4gICAgZGF0YToge1xuICAgICAgICBpc3N1ZXM6IFtdLFxuICAgICAgICBwYWdlOiAxLFxuICAgICAgICBwYWdlQ291bnQ6IDEsXG4gICAgICAgIHNlYXJjaDogbnVsbCxcbiAgICAgICAgdGltZW91dDogbnVsbFxuICAgIH0sXG5cbiAgICB3YXRjaDoge1xuICAgICAgICBzZWFyY2godmFsKSB7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lb3V0KTtcblxuICAgICAgICAgICAgdGhpcy5wYWdlID0gMTtcbiAgICAgICAgICAgIHRoaXMudGltZW91dCA9IHNldFRpbWVvdXQodGhpcy5nZXRJc3N1ZXMsIDIwMCk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgbWV0aG9kczoge1xuICAgICAgICBnZXRJc3N1ZXMoKSB7XG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9pc3N1ZXMnLCB7XG4gICAgICAgICAgICAgICAgcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgIHBhZ2U6IHRoaXMucGFnZSxcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoOiB0aGlzLnNlYXJjaFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmlzc3VlcyA9IHJlc3VsdC5kYXRhLmRhdGE7XG4gICAgICAgICAgICAgICAgdGhpcy5wYWdlID0gcmVzdWx0LmRhdGEuY3VycmVudF9wYWdlO1xuICAgICAgICAgICAgICAgIHRoaXMucGFnZUNvdW50ID0gcmVzdWx0LmRhdGEubGFzdF9wYWdlO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2xpY2tDYWxsYmFjayhwYWdlKSB7XG4gICAgICAgICAgICB0aGlzLnBhZ2UgPSBwYWdlO1xuXG4gICAgICAgICAgICB0aGlzLmdldElzc3VlcygpO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIGNvbXBvbmVudHM6IHtcbiAgICAgICAgUGFnaW5hdGUsXG4gICAgICAgIFd5Y0lzc3VlXG4gICAgfSxcblxuICAgIGNyZWF0ZWQoKSB7XG4gICAgICAgIHRoaXMuZ2V0SXNzdWVzKCk7XG4gICAgfVxufSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9wYWdlcy9jdXN0b21lci1zZXJ2aWNlL2luZGV4LmpzIiwiPHRlbXBsYXRlPlxuPHRyPlxuICAgIDx0ZD57eyAoaXNzdWUuY2hhdF9yb29tIT1udWxsKSA/IGlzc3VlLmNoYXRfcm9vbS5pZCA6ICcnIH19PC90ZD5cbiAgICA8dGQgY2xhc3M9XCJwcm9qZWN0LXN0YXR1c1wiPlxuICAgICAgICA8c3BhbiBjbGFzcz1cImxhYmVsXCIgOmNsYXNzPVwic3RhdHVzQ2xhc3NcIj57eyBzdGF0dXMgfX08L3NwYW4+XG4gICAgPC90ZD5cbiAgICA8dGQgY2xhc3M9XCJwcm9qZWN0LXRpdGxlXCI+XG4gICAgICAgIDxhIDpocmVmPVwiaHJlZlwiPnt7IGlzc3VlLnN1YmplY3QgfX08L2E+XG4gICAgICAgIDxici8+XG4gICAgICAgIDxzbWFsbD5DcmVhdGVkIGJ5OiA8c3Ryb25nPnt7IGlzc3VlLnVzZXIuZnVsbF9uYW1lIH19PC9zdHJvbmc+IG9uIHt7IGNyZWF0ZWRPbiB9fTwvc21hbGw+XG4gICAgPC90ZD5cbiAgICA8dGQgY2xhc3M9XCJwcm9qZWN0LXBlb3BsZVwiPlxuICAgICAgICA8aW1nIHYtZm9yPVwidXNlciBpbiB1c2Vyc1wiIDpzcmM9XCJ1c2VyLmF2YXRhclwiIGNsYXNzPVwiaW1nLWNpcmNsZVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIDp0aXRsZT1cInVzZXIuZnVsbF9uYW1lXCI+XG4gICAgICAgIDxzcGFuIHYtaWY9XCIhIHVzZXJzLmxlbmd0aFwiIGNsYXNzPVwicHVsbC1yaWdodFwiPk5vIGFzc2lnbmVkIHN1cHBvcnQgdXNlcjwvc3Bhbj5cbiAgICA8L3RkPlxuICAgIDx0ZCBjbGFzcz1cInByb2plY3QtYWN0aW9uc1wiPlxuICAgICAgICA8YSA6aHJlZj1cImhyZWZcIiBjbGFzcz1cImJ0biBidG4td2hpdGUgYnRuLXNtXCI+PGkgY2xhc3M9XCJmYSBmYS1leWVcIj48L2k+IFZpZXc8L2E+XG4gICAgPC90ZD5cbjwvdHI+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICAgIHByb3BzOiBbJ2lzc3VlJ10sXG5cbiAgICBtZXRob2RzOiB7XG5cbiAgICB9LFxuXG4gICAgY29tcHV0ZWQ6IHtcbiAgICAgICAgc3RhdHVzQ2xhc3MoKSB7XG4gICAgICAgICAgICByZXR1cm4gKHRoaXMuaXNzdWUuc3RhdHVzID09ICdvcGVuJylcbiAgICAgICAgICAgICAgICA/ICdsYWJlbC1wcmltYXJ5J1xuICAgICAgICAgICAgICAgIDogJ2xhYmVsLWRhbmdlcic7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc3RhdHVzKCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuaXNzdWUuc3RhdHVzLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgdGhpcy5pc3N1ZS5zdGF0dXMuc2xpY2UoMSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaHJlZigpIHtcbiAgICAgICAgICAgIHJldHVybiAnL2N1c3RvbWVyLXNlcnZpY2UvJyArIHRoaXMuaXNzdWUuaWQ7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlZE9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIG1vbWVudC51dGModGhpcy5pc3N1ZS5jcmVhdGVkX2F0KS5mb3JtYXQoJ01NTU0gREQsIFlZWVkgLSBoOm1tOnNzIEEnKTtcbiAgICAgICAgfSxcblxuICAgICAgICB1c2VycygpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmlzc3VlLmNoYXRfcm9vbSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmlzc3VlLmNoYXRfcm9vbS51c2VycztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIFtdO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIGNyZWF0ZWQoKSB7XG4gICAgICAgIC8vXG4gICAgfSxcblxuICAgIG1vdW50ZWQoKSB7XG4gICAgICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFd5Y0lzc3VlLnZ1ZT82NDNiYzcxNiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1d5Y0lzc3VlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjM5NDhlMmNcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vV3ljSXNzdWUudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y0lzc3VlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFd5Y0lzc3VlLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi02Mzk0OGUyY1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTYzOTQ4ZTJjXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljSXNzdWUudnVlXG4vLyBtb2R1bGUgaWQgPSAzNTRcbi8vIG1vZHVsZSBjaHVua3MgPSAxNiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygndHInLCBbX2MoJ3RkJywgW192bS5fdihfdm0uX3MoKF92bS5pc3N1ZS5jaGF0X3Jvb20gIT0gbnVsbCkgPyBfdm0uaXNzdWUuY2hhdF9yb29tLmlkIDogJycpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHJvamVjdC1zdGF0dXNcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWxcIixcbiAgICBjbGFzczogX3ZtLnN0YXR1c0NsYXNzXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5zdGF0dXMpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwcm9qZWN0LXRpdGxlXCJcbiAgfSwgW19jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogX3ZtLmhyZWZcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5pc3N1ZS5zdWJqZWN0KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2JyJyksIF92bS5fdihcIiBcIiksIF9jKCdzbWFsbCcsIFtfdm0uX3YoXCJDcmVhdGVkIGJ5OiBcIiksIF9jKCdzdHJvbmcnLCBbX3ZtLl92KF92bS5fcyhfdm0uaXNzdWUudXNlci5mdWxsX25hbWUpKV0pLCBfdm0uX3YoXCIgb24gXCIgKyBfdm0uX3MoX3ZtLmNyZWF0ZWRPbikpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInByb2plY3QtcGVvcGxlXCJcbiAgfSwgW192bS5fbCgoX3ZtLnVzZXJzKSwgZnVuY3Rpb24odXNlcikge1xuICAgIHJldHVybiBfYygnaW1nJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLWNpcmNsZVwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJzcmNcIjogdXNlci5hdmF0YXIsXG4gICAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICAgIFwidGl0bGVcIjogdXNlci5mdWxsX25hbWVcbiAgICAgIH1cbiAgICB9KVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgKCFfdm0udXNlcnMubGVuZ3RoKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInB1bGwtcmlnaHRcIlxuICB9LCBbX3ZtLl92KFwiTm8gYXNzaWduZWQgc3VwcG9ydCB1c2VyXCIpXSkgOiBfdm0uX2UoKV0sIDIpLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHJvamVjdC1hY3Rpb25zXCJcbiAgfSwgW19jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4td2hpdGUgYnRuLXNtXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBfdm0uaHJlZlxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWV5ZVwiXG4gIH0pLCBfdm0uX3YoXCIgVmlld1wiKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi02Mzk0OGUyY1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTYzOTQ4ZTJjXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y0lzc3VlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMTYiLCIhZnVuY3Rpb24oZSx0KXtcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cyYmXCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZT9tb2R1bGUuZXhwb3J0cz10KCk6XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXSx0KTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9leHBvcnRzLlZ1ZWpzUGFnaW5hdGU9dCgpOmUuVnVlanNQYWdpbmF0ZT10KCl9KHRoaXMsZnVuY3Rpb24oKXtyZXR1cm4gZnVuY3Rpb24oZSl7ZnVuY3Rpb24gdChzKXtpZihuW3NdKXJldHVybiBuW3NdLmV4cG9ydHM7dmFyIGE9bltzXT17ZXhwb3J0czp7fSxpZDpzLGxvYWRlZDohMX07cmV0dXJuIGVbc10uY2FsbChhLmV4cG9ydHMsYSxhLmV4cG9ydHMsdCksYS5sb2FkZWQ9ITAsYS5leHBvcnRzfXZhciBuPXt9O3JldHVybiB0Lm09ZSx0LmM9bix0LnA9XCJcIix0KDApfShbZnVuY3Rpb24oZSx0LG4pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHMoZSl7cmV0dXJuIGUmJmUuX19lc01vZHVsZT9lOntkZWZhdWx0OmV9fXZhciBhPW4oMSksaT1zKGEpO2UuZXhwb3J0cz1pLmRlZmF1bHR9LGZ1bmN0aW9uKGUsdCxuKXtuKDIpO3ZhciBzPW4oNikobig3KSxuKDgpLFwiZGF0YS12LTgyOTYzYTQwXCIsbnVsbCk7ZS5leHBvcnRzPXMuZXhwb3J0c30sZnVuY3Rpb24oZSx0LG4pe3ZhciBzPW4oMyk7XCJzdHJpbmdcIj09dHlwZW9mIHMmJihzPVtbZS5pZCxzLFwiXCJdXSk7big1KShzLHt9KTtzLmxvY2FscyYmKGUuZXhwb3J0cz1zLmxvY2Fscyl9LGZ1bmN0aW9uKGUsdCxuKXt0PWUuZXhwb3J0cz1uKDQpKCksdC5wdXNoKFtlLmlkLFwiYVtkYXRhLXYtODI5NjNhNDBde2N1cnNvcjpwb2ludGVyfVwiLFwiXCJdKX0sZnVuY3Rpb24oZSx0KXtlLmV4cG9ydHM9ZnVuY3Rpb24oKXt2YXIgZT1bXTtyZXR1cm4gZS50b1N0cmluZz1mdW5jdGlvbigpe2Zvcih2YXIgZT1bXSx0PTA7dDx0aGlzLmxlbmd0aDt0Kyspe3ZhciBuPXRoaXNbdF07blsyXT9lLnB1c2goXCJAbWVkaWEgXCIrblsyXStcIntcIituWzFdK1wifVwiKTplLnB1c2goblsxXSl9cmV0dXJuIGUuam9pbihcIlwiKX0sZS5pPWZ1bmN0aW9uKHQsbil7XCJzdHJpbmdcIj09dHlwZW9mIHQmJih0PVtbbnVsbCx0LFwiXCJdXSk7Zm9yKHZhciBzPXt9LGE9MDthPHRoaXMubGVuZ3RoO2ErKyl7dmFyIGk9dGhpc1thXVswXTtcIm51bWJlclwiPT10eXBlb2YgaSYmKHNbaV09ITApfWZvcihhPTA7YTx0Lmxlbmd0aDthKyspe3ZhciByPXRbYV07XCJudW1iZXJcIj09dHlwZW9mIHJbMF0mJnNbclswXV18fChuJiYhclsyXT9yWzJdPW46biYmKHJbMl09XCIoXCIrclsyXStcIikgYW5kIChcIituK1wiKVwiKSxlLnB1c2gocikpfX0sZX19LGZ1bmN0aW9uKGUsdCxuKXtmdW5jdGlvbiBzKGUsdCl7Zm9yKHZhciBuPTA7bjxlLmxlbmd0aDtuKyspe3ZhciBzPWVbbl0sYT11W3MuaWRdO2lmKGEpe2EucmVmcysrO2Zvcih2YXIgaT0wO2k8YS5wYXJ0cy5sZW5ndGg7aSsrKWEucGFydHNbaV0ocy5wYXJ0c1tpXSk7Zm9yKDtpPHMucGFydHMubGVuZ3RoO2krKylhLnBhcnRzLnB1c2gobyhzLnBhcnRzW2ldLHQpKX1lbHNle2Zvcih2YXIgcj1bXSxpPTA7aTxzLnBhcnRzLmxlbmd0aDtpKyspci5wdXNoKG8ocy5wYXJ0c1tpXSx0KSk7dVtzLmlkXT17aWQ6cy5pZCxyZWZzOjEscGFydHM6cn19fX1mdW5jdGlvbiBhKGUpe2Zvcih2YXIgdD1bXSxuPXt9LHM9MDtzPGUubGVuZ3RoO3MrKyl7dmFyIGE9ZVtzXSxpPWFbMF0scj1hWzFdLGw9YVsyXSxvPWFbM10sZD17Y3NzOnIsbWVkaWE6bCxzb3VyY2VNYXA6b307bltpXT9uW2ldLnBhcnRzLnB1c2goZCk6dC5wdXNoKG5baV09e2lkOmkscGFydHM6W2RdfSl9cmV0dXJuIHR9ZnVuY3Rpb24gaShlLHQpe3ZhciBuPWcoKSxzPUNbQy5sZW5ndGgtMV07aWYoXCJ0b3BcIj09PWUuaW5zZXJ0QXQpcz9zLm5leHRTaWJsaW5nP24uaW5zZXJ0QmVmb3JlKHQscy5uZXh0U2libGluZyk6bi5hcHBlbmRDaGlsZCh0KTpuLmluc2VydEJlZm9yZSh0LG4uZmlyc3RDaGlsZCksQy5wdXNoKHQpO2Vsc2V7aWYoXCJib3R0b21cIiE9PWUuaW5zZXJ0QXQpdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCB2YWx1ZSBmb3IgcGFyYW1ldGVyICdpbnNlcnRBdCcuIE11c3QgYmUgJ3RvcCcgb3IgJ2JvdHRvbScuXCIpO24uYXBwZW5kQ2hpbGQodCl9fWZ1bmN0aW9uIHIoZSl7ZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGUpO3ZhciB0PUMuaW5kZXhPZihlKTt0Pj0wJiZDLnNwbGljZSh0LDEpfWZ1bmN0aW9uIGwoZSl7dmFyIHQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInN0eWxlXCIpO3JldHVybiB0LnR5cGU9XCJ0ZXh0L2Nzc1wiLGkoZSx0KSx0fWZ1bmN0aW9uIG8oZSx0KXt2YXIgbixzLGE7aWYodC5zaW5nbGV0b24pe3ZhciBpPXYrKztuPWh8fChoPWwodCkpLHM9ZC5iaW5kKG51bGwsbixpLCExKSxhPWQuYmluZChudWxsLG4saSwhMCl9ZWxzZSBuPWwodCkscz1jLmJpbmQobnVsbCxuKSxhPWZ1bmN0aW9uKCl7cihuKX07cmV0dXJuIHMoZSksZnVuY3Rpb24odCl7aWYodCl7aWYodC5jc3M9PT1lLmNzcyYmdC5tZWRpYT09PWUubWVkaWEmJnQuc291cmNlTWFwPT09ZS5zb3VyY2VNYXApcmV0dXJuO3MoZT10KX1lbHNlIGEoKX19ZnVuY3Rpb24gZChlLHQsbixzKXt2YXIgYT1uP1wiXCI6cy5jc3M7aWYoZS5zdHlsZVNoZWV0KWUuc3R5bGVTaGVldC5jc3NUZXh0PWIodCxhKTtlbHNle3ZhciBpPWRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGEpLHI9ZS5jaGlsZE5vZGVzO3JbdF0mJmUucmVtb3ZlQ2hpbGQoclt0XSksci5sZW5ndGg/ZS5pbnNlcnRCZWZvcmUoaSxyW3RdKTplLmFwcGVuZENoaWxkKGkpfX1mdW5jdGlvbiBjKGUsdCl7dmFyIG49dC5jc3Mscz10Lm1lZGlhLGE9dC5zb3VyY2VNYXA7aWYocyYmZS5zZXRBdHRyaWJ1dGUoXCJtZWRpYVwiLHMpLGEmJihuKz1cIlxcbi8qIyBzb3VyY2VVUkw9XCIrYS5zb3VyY2VzWzBdK1wiICovXCIsbis9XCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiK2J0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KGEpKSkpK1wiICovXCIpLGUuc3R5bGVTaGVldCllLnN0eWxlU2hlZXQuY3NzVGV4dD1uO2Vsc2V7Zm9yKDtlLmZpcnN0Q2hpbGQ7KWUucmVtb3ZlQ2hpbGQoZS5maXJzdENoaWxkKTtlLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKG4pKX19dmFyIHU9e30scD1mdW5jdGlvbihlKXt2YXIgdDtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm5cInVuZGVmaW5lZFwiPT10eXBlb2YgdCYmKHQ9ZS5hcHBseSh0aGlzLGFyZ3VtZW50cykpLHR9fSxmPXAoZnVuY3Rpb24oKXtyZXR1cm4vbXNpZSBbNi05XVxcYi8udGVzdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpKX0pLGc9cChmdW5jdGlvbigpe3JldHVybiBkb2N1bWVudC5oZWFkfHxkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImhlYWRcIilbMF19KSxoPW51bGwsdj0wLEM9W107ZS5leHBvcnRzPWZ1bmN0aW9uKGUsdCl7dD10fHx7fSxcInVuZGVmaW5lZFwiPT10eXBlb2YgdC5zaW5nbGV0b24mJih0LnNpbmdsZXRvbj1mKCkpLFwidW5kZWZpbmVkXCI9PXR5cGVvZiB0Lmluc2VydEF0JiYodC5pbnNlcnRBdD1cImJvdHRvbVwiKTt2YXIgbj1hKGUpO3JldHVybiBzKG4sdCksZnVuY3Rpb24oZSl7Zm9yKHZhciBpPVtdLHI9MDtyPG4ubGVuZ3RoO3IrKyl7dmFyIGw9bltyXSxvPXVbbC5pZF07by5yZWZzLS0saS5wdXNoKG8pfWlmKGUpe3ZhciBkPWEoZSk7cyhkLHQpfWZvcih2YXIgcj0wO3I8aS5sZW5ndGg7cisrKXt2YXIgbz1pW3JdO2lmKDA9PT1vLnJlZnMpe2Zvcih2YXIgYz0wO2M8by5wYXJ0cy5sZW5ndGg7YysrKW8ucGFydHNbY10oKTtkZWxldGUgdVtvLmlkXX19fX07dmFyIGI9ZnVuY3Rpb24oKXt2YXIgZT1bXTtyZXR1cm4gZnVuY3Rpb24odCxuKXtyZXR1cm4gZVt0XT1uLGUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oXCJcXG5cIil9fSgpfSxmdW5jdGlvbihlLHQpe2UuZXhwb3J0cz1mdW5jdGlvbihlLHQsbixzKXt2YXIgYSxpPWU9ZXx8e30scj10eXBlb2YgZS5kZWZhdWx0O1wib2JqZWN0XCIhPT1yJiZcImZ1bmN0aW9uXCIhPT1yfHwoYT1lLGk9ZS5kZWZhdWx0KTt2YXIgbD1cImZ1bmN0aW9uXCI9PXR5cGVvZiBpP2kub3B0aW9uczppO2lmKHQmJihsLnJlbmRlcj10LnJlbmRlcixsLnN0YXRpY1JlbmRlckZucz10LnN0YXRpY1JlbmRlckZucyksbiYmKGwuX3Njb3BlSWQ9bikscyl7dmFyIG89bC5jb21wdXRlZHx8KGwuY29tcHV0ZWQ9e30pO09iamVjdC5rZXlzKHMpLmZvckVhY2goZnVuY3Rpb24oZSl7dmFyIHQ9c1tlXTtvW2VdPWZ1bmN0aW9uKCl7cmV0dXJuIHR9fSl9cmV0dXJue2VzTW9kdWxlOmEsZXhwb3J0czppLG9wdGlvbnM6bH19fSxmdW5jdGlvbihlLHQpe1widXNlIHN0cmljdFwiO09iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pLHQuZGVmYXVsdD17cHJvcHM6e3BhZ2VDb3VudDp7dHlwZTpOdW1iZXIscmVxdWlyZWQ6ITB9LGluaXRpYWxQYWdlOnt0eXBlOk51bWJlcixkZWZhdWx0OjB9LGZvcmNlUGFnZTp7dHlwZTpOdW1iZXJ9LGNsaWNrSGFuZGxlcjp7dHlwZTpGdW5jdGlvbixkZWZhdWx0OmZ1bmN0aW9uKCl7fX0scGFnZVJhbmdlOnt0eXBlOk51bWJlcixkZWZhdWx0OjN9LG1hcmdpblBhZ2VzOnt0eXBlOk51bWJlcixkZWZhdWx0OjF9LHByZXZUZXh0Ont0eXBlOlN0cmluZyxkZWZhdWx0OlwiUHJldlwifSxuZXh0VGV4dDp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIk5leHRcIn0sYnJlYWtWaWV3VGV4dDp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIuKAplwifSxjb250YWluZXJDbGFzczp7dHlwZTpTdHJpbmd9LHBhZ2VDbGFzczp7dHlwZTpTdHJpbmd9LHBhZ2VMaW5rQ2xhc3M6e3R5cGU6U3RyaW5nfSxwcmV2Q2xhc3M6e3R5cGU6U3RyaW5nfSxwcmV2TGlua0NsYXNzOnt0eXBlOlN0cmluZ30sbmV4dENsYXNzOnt0eXBlOlN0cmluZ30sbmV4dExpbmtDbGFzczp7dHlwZTpTdHJpbmd9LGJyZWFrVmlld0NsYXNzOnt0eXBlOlN0cmluZ30sYnJlYWtWaWV3TGlua0NsYXNzOnt0eXBlOlN0cmluZ30sYWN0aXZlQ2xhc3M6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJhY3RpdmVcIn0sZGlzYWJsZWRDbGFzczp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcImRpc2FibGVkXCJ9LG5vTGlTdXJyb3VuZDp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITF9LGZpcnN0TGFzdEJ1dHRvbjp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITF9LGZpcnN0QnV0dG9uVGV4dDp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIkZpcnN0XCJ9LGxhc3RCdXR0b25UZXh0Ont0eXBlOlN0cmluZyxkZWZhdWx0OlwiTGFzdFwifSxoaWRlUHJldk5leHQ6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfX0sZGF0YTpmdW5jdGlvbigpe3JldHVybntzZWxlY3RlZDp0aGlzLmluaXRpYWxQYWdlfX0sYmVmb3JlVXBkYXRlOmZ1bmN0aW9uKCl7dm9pZCAwIT09dGhpcy5mb3JjZVBhZ2UmJnRoaXMuZm9yY2VQYWdlIT09dGhpcy5zZWxlY3RlZCYmKHRoaXMuc2VsZWN0ZWQ9dGhpcy5mb3JjZVBhZ2UpfSxjb21wdXRlZDp7cGFnZXM6ZnVuY3Rpb24oKXt2YXIgZT10aGlzLHQ9e307aWYodGhpcy5wYWdlQ291bnQ8PXRoaXMucGFnZVJhbmdlKWZvcih2YXIgbj0wO248dGhpcy5wYWdlQ291bnQ7bisrKXt2YXIgcz17aW5kZXg6bixjb250ZW50Om4rMSxzZWxlY3RlZDpuPT09dGhpcy5zZWxlY3RlZH07dFtuXT1zfWVsc2V7Zm9yKHZhciBhPU1hdGguZmxvb3IodGhpcy5wYWdlUmFuZ2UvMiksaT1mdW5jdGlvbihuKXt2YXIgcz17aW5kZXg6bixjb250ZW50Om4rMSxzZWxlY3RlZDpuPT09ZS5zZWxlY3RlZH07dFtuXT1zfSxyPWZ1bmN0aW9uKGUpe3ZhciBuPXtkaXNhYmxlZDohMCxicmVha1ZpZXc6ITB9O3RbZV09bn0sbD0wO2w8dGhpcy5tYXJnaW5QYWdlcztsKyspaShsKTt2YXIgbz0wO3RoaXMuc2VsZWN0ZWQtYT4wJiYobz10aGlzLnNlbGVjdGVkLWEpO3ZhciBkPW8rdGhpcy5wYWdlUmFuZ2UtMTtkPj10aGlzLnBhZ2VDb3VudCYmKGQ9dGhpcy5wYWdlQ291bnQtMSxvPWQtdGhpcy5wYWdlUmFuZ2UrMSk7Zm9yKHZhciBjPW87Yzw9ZCYmYzw9dGhpcy5wYWdlQ291bnQtMTtjKyspaShjKTtvPnRoaXMubWFyZ2luUGFnZXMmJnIoby0xKSxkKzE8dGhpcy5wYWdlQ291bnQtdGhpcy5tYXJnaW5QYWdlcyYmcihkKzEpO2Zvcih2YXIgdT10aGlzLnBhZ2VDb3VudC0xO3U+PXRoaXMucGFnZUNvdW50LXRoaXMubWFyZ2luUGFnZXM7dS0tKWkodSl9cmV0dXJuIHR9fSxtZXRob2RzOntoYW5kbGVQYWdlU2VsZWN0ZWQ6ZnVuY3Rpb24oZSl7dGhpcy5zZWxlY3RlZCE9PWUmJih0aGlzLnNlbGVjdGVkPWUsdGhpcy5jbGlja0hhbmRsZXIodGhpcy5zZWxlY3RlZCsxKSl9LHByZXZQYWdlOmZ1bmN0aW9uKCl7dGhpcy5zZWxlY3RlZDw9MHx8KHRoaXMuc2VsZWN0ZWQtLSx0aGlzLmNsaWNrSGFuZGxlcih0aGlzLnNlbGVjdGVkKzEpKX0sbmV4dFBhZ2U6ZnVuY3Rpb24oKXt0aGlzLnNlbGVjdGVkPj10aGlzLnBhZ2VDb3VudC0xfHwodGhpcy5zZWxlY3RlZCsrLHRoaXMuY2xpY2tIYW5kbGVyKHRoaXMuc2VsZWN0ZWQrMSkpfSxmaXJzdFBhZ2VTZWxlY3RlZDpmdW5jdGlvbigpe3JldHVybiAwPT09dGhpcy5zZWxlY3RlZH0sbGFzdFBhZ2VTZWxlY3RlZDpmdW5jdGlvbigpe3JldHVybiB0aGlzLnNlbGVjdGVkPT09dGhpcy5wYWdlQ291bnQtMXx8MD09PXRoaXMucGFnZUNvdW50fSxzZWxlY3RGaXJzdFBhZ2U6ZnVuY3Rpb24oKXt0aGlzLnNlbGVjdGVkPD0wfHwodGhpcy5zZWxlY3RlZD0wLHRoaXMuY2xpY2tIYW5kbGVyKHRoaXMuc2VsZWN0ZWQrMSkpfSxzZWxlY3RMYXN0UGFnZTpmdW5jdGlvbigpe3RoaXMuc2VsZWN0ZWQ+PXRoaXMucGFnZUNvdW50LTF8fCh0aGlzLnNlbGVjdGVkPXRoaXMucGFnZUNvdW50LTEsdGhpcy5jbGlja0hhbmRsZXIodGhpcy5zZWxlY3RlZCsxKSl9fX19LGZ1bmN0aW9uKGUsdCl7ZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24oKXt2YXIgZT10aGlzLHQ9ZS4kY3JlYXRlRWxlbWVudCxuPWUuX3NlbGYuX2N8fHQ7cmV0dXJuIGUubm9MaVN1cnJvdW5kP24oXCJkaXZcIix7Y2xhc3M6ZS5jb250YWluZXJDbGFzc30sW2UuZmlyc3RMYXN0QnV0dG9uP24oXCJhXCIse2NsYXNzOltlLnBhZ2VMaW5rQ2xhc3MsZS5maXJzdFBhZ2VTZWxlY3RlZCgpP2UuZGlzYWJsZWRDbGFzczpcIlwiXSxhdHRyczp7dGFiaW5kZXg6XCIwXCJ9LGRvbVByb3BzOntpbm5lckhUTUw6ZS5fcyhlLmZpcnN0QnV0dG9uVGV4dCl9LG9uOntjbGljazpmdW5jdGlvbih0KXtlLnNlbGVjdEZpcnN0UGFnZSgpfSxrZXl1cDpmdW5jdGlvbih0KXtyZXR1cm5cImJ1dHRvblwiaW4gdHx8IWUuX2sodC5rZXlDb2RlLFwiZW50ZXJcIiwxMyk/dm9pZCBlLnNlbGVjdEZpcnN0UGFnZSgpOm51bGx9fX0pOmUuX2UoKSxlLl92KFwiIFwiKSxlLmZpcnN0UGFnZVNlbGVjdGVkKCkmJmUuaGlkZVByZXZOZXh0P2UuX2UoKTpuKFwiYVwiLHtjbGFzczpbZS5wcmV2TGlua0NsYXNzLGUuZmlyc3RQYWdlU2VsZWN0ZWQoKT9lLmRpc2FibGVkQ2xhc3M6XCJcIl0sYXR0cnM6e3RhYmluZGV4OlwiMFwifSxkb21Qcm9wczp7aW5uZXJIVE1MOmUuX3MoZS5wcmV2VGV4dCl9LG9uOntjbGljazpmdW5jdGlvbih0KXtlLnByZXZQYWdlKCl9LGtleXVwOmZ1bmN0aW9uKHQpe3JldHVyblwiYnV0dG9uXCJpbiB0fHwhZS5fayh0LmtleUNvZGUsXCJlbnRlclwiLDEzKT92b2lkIGUucHJldlBhZ2UoKTpudWxsfX19KSxlLl92KFwiIFwiKSxlLl9sKGUucGFnZXMsZnVuY3Rpb24odCl7cmV0dXJuW3QuYnJlYWtWaWV3P24oXCJhXCIse2NsYXNzOltlLnBhZ2VMaW5rQ2xhc3MsZS5icmVha1ZpZXdMaW5rQ2xhc3MsdC5kaXNhYmxlZD9lLmRpc2FibGVkQ2xhc3M6XCJcIl0sYXR0cnM6e3RhYmluZGV4OlwiMFwifX0sW2UuX3QoXCJicmVha1ZpZXdDb250ZW50XCIsW2UuX3YoZS5fcyhlLmJyZWFrVmlld1RleHQpKV0pXSwyKTp0LmRpc2FibGVkP24oXCJhXCIse2NsYXNzOltlLnBhZ2VMaW5rQ2xhc3MsdC5zZWxlY3RlZD9lLmFjdGl2ZUNsYXNzOlwiXCIsZS5kaXNhYmxlZENsYXNzXSxhdHRyczp7dGFiaW5kZXg6XCIwXCJ9fSxbZS5fdihlLl9zKHQuY29udGVudCkpXSk6bihcImFcIix7Y2xhc3M6W2UucGFnZUxpbmtDbGFzcyx0LnNlbGVjdGVkP2UuYWN0aXZlQ2xhc3M6XCJcIl0sYXR0cnM6e3RhYmluZGV4OlwiMFwifSxvbjp7Y2xpY2s6ZnVuY3Rpb24obil7ZS5oYW5kbGVQYWdlU2VsZWN0ZWQodC5pbmRleCl9LGtleXVwOmZ1bmN0aW9uKG4pe3JldHVyblwiYnV0dG9uXCJpbiBufHwhZS5fayhuLmtleUNvZGUsXCJlbnRlclwiLDEzKT92b2lkIGUuaGFuZGxlUGFnZVNlbGVjdGVkKHQuaW5kZXgpOm51bGx9fX0sW2UuX3YoZS5fcyh0LmNvbnRlbnQpKV0pXX0pLGUuX3YoXCIgXCIpLGUubGFzdFBhZ2VTZWxlY3RlZCgpJiZlLmhpZGVQcmV2TmV4dD9lLl9lKCk6bihcImFcIix7Y2xhc3M6W2UubmV4dExpbmtDbGFzcyxlLmxhc3RQYWdlU2VsZWN0ZWQoKT9lLmRpc2FibGVkQ2xhc3M6XCJcIl0sYXR0cnM6e3RhYmluZGV4OlwiMFwifSxkb21Qcm9wczp7aW5uZXJIVE1MOmUuX3MoZS5uZXh0VGV4dCl9LG9uOntjbGljazpmdW5jdGlvbih0KXtlLm5leHRQYWdlKCl9LGtleXVwOmZ1bmN0aW9uKHQpe3JldHVyblwiYnV0dG9uXCJpbiB0fHwhZS5fayh0LmtleUNvZGUsXCJlbnRlclwiLDEzKT92b2lkIGUubmV4dFBhZ2UoKTpudWxsfX19KSxlLl92KFwiIFwiKSxlLmZpcnN0TGFzdEJ1dHRvbj9uKFwiYVwiLHtjbGFzczpbZS5wYWdlTGlua0NsYXNzLGUubGFzdFBhZ2VTZWxlY3RlZCgpP2UuZGlzYWJsZWRDbGFzczpcIlwiXSxhdHRyczp7dGFiaW5kZXg6XCIwXCJ9LGRvbVByb3BzOntpbm5lckhUTUw6ZS5fcyhlLmxhc3RCdXR0b25UZXh0KX0sb246e2NsaWNrOmZ1bmN0aW9uKHQpe2Uuc2VsZWN0TGFzdFBhZ2UoKX0sa2V5dXA6ZnVuY3Rpb24odCl7cmV0dXJuXCJidXR0b25cImluIHR8fCFlLl9rKHQua2V5Q29kZSxcImVudGVyXCIsMTMpP3ZvaWQgZS5zZWxlY3RMYXN0UGFnZSgpOm51bGx9fX0pOmUuX2UoKV0sMik6bihcInVsXCIse2NsYXNzOmUuY29udGFpbmVyQ2xhc3N9LFtlLmZpcnN0TGFzdEJ1dHRvbj9uKFwibGlcIix7Y2xhc3M6W2UucGFnZUNsYXNzLGUuZmlyc3RQYWdlU2VsZWN0ZWQoKT9lLmRpc2FibGVkQ2xhc3M6XCJcIl19LFtuKFwiYVwiLHtjbGFzczplLnBhZ2VMaW5rQ2xhc3MsYXR0cnM6e3RhYmluZGV4OmUuZmlyc3RQYWdlU2VsZWN0ZWQoKT8tMTowfSxkb21Qcm9wczp7aW5uZXJIVE1MOmUuX3MoZS5maXJzdEJ1dHRvblRleHQpfSxvbjp7Y2xpY2s6ZnVuY3Rpb24odCl7ZS5zZWxlY3RGaXJzdFBhZ2UoKX0sa2V5dXA6ZnVuY3Rpb24odCl7cmV0dXJuXCJidXR0b25cImluIHR8fCFlLl9rKHQua2V5Q29kZSxcImVudGVyXCIsMTMpP3ZvaWQgZS5zZWxlY3RGaXJzdFBhZ2UoKTpudWxsfX19KV0pOmUuX2UoKSxlLl92KFwiIFwiKSxlLmZpcnN0UGFnZVNlbGVjdGVkKCkmJmUuaGlkZVByZXZOZXh0P2UuX2UoKTpuKFwibGlcIix7Y2xhc3M6W2UucHJldkNsYXNzLGUuZmlyc3RQYWdlU2VsZWN0ZWQoKT9lLmRpc2FibGVkQ2xhc3M6XCJcIl19LFtuKFwiYVwiLHtjbGFzczplLnByZXZMaW5rQ2xhc3MsYXR0cnM6e3RhYmluZGV4OmUuZmlyc3RQYWdlU2VsZWN0ZWQoKT8tMTowfSxkb21Qcm9wczp7aW5uZXJIVE1MOmUuX3MoZS5wcmV2VGV4dCl9LG9uOntjbGljazpmdW5jdGlvbih0KXtlLnByZXZQYWdlKCl9LGtleXVwOmZ1bmN0aW9uKHQpe3JldHVyblwiYnV0dG9uXCJpbiB0fHwhZS5fayh0LmtleUNvZGUsXCJlbnRlclwiLDEzKT92b2lkIGUucHJldlBhZ2UoKTpudWxsfX19KV0pLGUuX3YoXCIgXCIpLGUuX2woZS5wYWdlcyxmdW5jdGlvbih0KXtyZXR1cm4gbihcImxpXCIse2NsYXNzOltlLnBhZ2VDbGFzcyx0LnNlbGVjdGVkP2UuYWN0aXZlQ2xhc3M6XCJcIix0LmRpc2FibGVkP2UuZGlzYWJsZWRDbGFzczpcIlwiLHQuYnJlYWtWaWV3P2UuYnJlYWtWaWV3Q2xhc3M6XCJcIl19LFt0LmJyZWFrVmlldz9uKFwiYVwiLHtjbGFzczpbZS5wYWdlTGlua0NsYXNzLGUuYnJlYWtWaWV3TGlua0NsYXNzXSxhdHRyczp7dGFiaW5kZXg6XCIwXCJ9fSxbZS5fdChcImJyZWFrVmlld0NvbnRlbnRcIixbZS5fdihlLl9zKGUuYnJlYWtWaWV3VGV4dCkpXSldLDIpOnQuZGlzYWJsZWQ/bihcImFcIix7Y2xhc3M6ZS5wYWdlTGlua0NsYXNzLGF0dHJzOnt0YWJpbmRleDpcIjBcIn19LFtlLl92KGUuX3ModC5jb250ZW50KSldKTpuKFwiYVwiLHtjbGFzczplLnBhZ2VMaW5rQ2xhc3MsYXR0cnM6e3RhYmluZGV4OlwiMFwifSxvbjp7Y2xpY2s6ZnVuY3Rpb24obil7ZS5oYW5kbGVQYWdlU2VsZWN0ZWQodC5pbmRleCl9LGtleXVwOmZ1bmN0aW9uKG4pe3JldHVyblwiYnV0dG9uXCJpbiBufHwhZS5fayhuLmtleUNvZGUsXCJlbnRlclwiLDEzKT92b2lkIGUuaGFuZGxlUGFnZVNlbGVjdGVkKHQuaW5kZXgpOm51bGx9fX0sW2UuX3YoZS5fcyh0LmNvbnRlbnQpKV0pXSl9KSxlLl92KFwiIFwiKSxlLmxhc3RQYWdlU2VsZWN0ZWQoKSYmZS5oaWRlUHJldk5leHQ/ZS5fZSgpOm4oXCJsaVwiLHtjbGFzczpbZS5uZXh0Q2xhc3MsZS5sYXN0UGFnZVNlbGVjdGVkKCk/ZS5kaXNhYmxlZENsYXNzOlwiXCJdfSxbbihcImFcIix7Y2xhc3M6ZS5uZXh0TGlua0NsYXNzLGF0dHJzOnt0YWJpbmRleDplLmxhc3RQYWdlU2VsZWN0ZWQoKT8tMTowfSxkb21Qcm9wczp7aW5uZXJIVE1MOmUuX3MoZS5uZXh0VGV4dCl9LG9uOntjbGljazpmdW5jdGlvbih0KXtlLm5leHRQYWdlKCl9LGtleXVwOmZ1bmN0aW9uKHQpe3JldHVyblwiYnV0dG9uXCJpbiB0fHwhZS5fayh0LmtleUNvZGUsXCJlbnRlclwiLDEzKT92b2lkIGUubmV4dFBhZ2UoKTpudWxsfX19KV0pLGUuX3YoXCIgXCIpLGUuZmlyc3RMYXN0QnV0dG9uP24oXCJsaVwiLHtjbGFzczpbZS5wYWdlQ2xhc3MsZS5sYXN0UGFnZVNlbGVjdGVkKCk/ZS5kaXNhYmxlZENsYXNzOlwiXCJdfSxbbihcImFcIix7Y2xhc3M6ZS5wYWdlTGlua0NsYXNzLGF0dHJzOnt0YWJpbmRleDplLmxhc3RQYWdlU2VsZWN0ZWQoKT8tMTowfSxkb21Qcm9wczp7aW5uZXJIVE1MOmUuX3MoZS5sYXN0QnV0dG9uVGV4dCl9LG9uOntjbGljazpmdW5jdGlvbih0KXtlLnNlbGVjdExhc3RQYWdlKCl9LGtleXVwOmZ1bmN0aW9uKHQpe3JldHVyblwiYnV0dG9uXCJpbiB0fHwhZS5fayh0LmtleUNvZGUsXCJlbnRlclwiLDEzKT92b2lkIGUuc2VsZWN0TGFzdFBhZ2UoKTpudWxsfX19KV0pOmUuX2UoKV0sMil9LHN0YXRpY1JlbmRlckZuczpbXX19XSl9KTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlanMtcGFnaW5hdGUvZGlzdC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gNDU1XG4vLyBtb2R1bGUgY2h1bmtzID0gMTYiXSwic291cmNlUm9vdCI6IiJ9