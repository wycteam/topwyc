/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 267);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 130:
/***/ (function(module, exports, __webpack_require__) {

!function(p,x){ true?module.exports=x():"function"==typeof define&&define.amd?define([],x):"object"==typeof exports?exports.VueInfiniteLoading=x():p.VueInfiniteLoading=x()}(this,function(){return function(p){function x(a){if(t[a])return t[a].exports;var e=t[a]={exports:{},id:a,loaded:!1};return p[a].call(e.exports,e,e.exports,x),e.loaded=!0,e.exports}var t={};return x.m=p,x.c=t,x.p="/",x(0)}([function(p,x,t){"use strict";function a(p){return p&&p.__esModule?p:{default:p}}Object.defineProperty(x,"__esModule",{value:!0});var e=t(4),o=a(e);x.default=o.default,"undefined"!=typeof window&&window.Vue&&window.Vue.component("infinite-loading",o.default)},function(p,x){"use strict";function t(p){return"BODY"===p.tagName?window:["scroll","auto"].indexOf(getComputedStyle(p).overflowY)>-1?p:p.hasAttribute("infinite-wrapper")||p.hasAttribute("data-infinite-wrapper")?p:t(p.parentNode)}function a(p,x,t){var a=void 0;if("top"===t)a=isNaN(p.scrollTop)?p.pageYOffset:p.scrollTop;else{var e=x.getBoundingClientRect().top,o=p===window?window.innerHeight:p.getBoundingClientRect().bottom;a=e-o}return a}Object.defineProperty(x,"__esModule",{value:!0});var e={bubbles:"loading-bubbles",circles:"loading-circles",default:"loading-default",spiral:"loading-spiral",waveDots:"loading-wave-dots"};x.default={data:function(){return{scrollParent:null,scrollHandler:null,isLoading:!1,isComplete:!1,isFirstLoad:!0}},computed:{spinnerType:function(){return e[this.spinner]||e.default}},props:{distance:{type:Number,default:100},onInfinite:Function,spinner:String,direction:{type:String,default:"bottom"}},mounted:function(){var p=this;this.scrollParent=t(this.$el),this.scrollHandler=function(){this.isLoading||this.attemptLoad()}.bind(this),setTimeout(this.scrollHandler,1),this.scrollParent.addEventListener("scroll",this.scrollHandler),this.$on("$InfiniteLoading:loaded",function(){p.isFirstLoad=!1,p.isLoading&&p.$nextTick(p.attemptLoad)}),this.$on("$InfiniteLoading:complete",function(){p.isLoading=!1,p.isComplete=!0,p.scrollParent.removeEventListener("scroll",p.scrollHandler)}),this.$on("$InfiniteLoading:reset",function(){p.isLoading=!1,p.isComplete=!1,p.isFirstLoad=!0,p.scrollParent.addEventListener("scroll",p.scrollHandler),setTimeout(p.scrollHandler,1)})},deactivated:function(){this.isLoading=!1,this.scrollParent.removeEventListener("scroll",this.scrollHandler)},activated:function(){this.scrollParent.addEventListener("scroll",this.scrollHandler)},methods:{attemptLoad:function(){var p=a(this.scrollParent,this.$el,this.direction);!this.isComplete&&p<=this.distance?(this.isLoading=!0,this.onInfinite.call()):this.isLoading=!1}},destroyed:function(){this.isComplete||this.scrollParent.removeEventListener("scroll",this.scrollHandler)}}},function(p,x,t){x=p.exports=t(3)(),x.push([p.id,'.loading-wave-dots[data-v-50793f02]{position:relative}.loading-wave-dots[data-v-50793f02]:before{content:"";position:absolute;top:50%;left:50%;margin-left:-4px;margin-top:-4px;width:8px;height:8px;background-color:#bbb;border-radius:50%;-webkit-animation:linear loading-wave-dots 2.8s infinite;animation:linear loading-wave-dots 2.8s infinite}@-webkit-keyframes loading-wave-dots{0%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}5%{box-shadow:-32px -4px 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}10%{box-shadow:-32px -6px 0 #999,-16px -4px 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}15%{box-shadow:-32px 2px 0 #bbb,-16px -2px 0 #999,16px 4px 0 #bbb,32px 4px 0 #bbb;-webkit-transform:translateY(-4px);transform:translateY(-4px);background-color:#bbb}20%{box-shadow:-32px 6px 0 #bbb,-16px 4px 0 #bbb,16px 2px 0 #bbb,32px 6px 0 #bbb;-webkit-transform:translateY(-6px);transform:translateY(-6px);background-color:#999}25%{box-shadow:-32px 2px 0 #bbb,-16px 2px 0 #bbb,16px -4px 0 #999,32px -2px 0 #bbb;-webkit-transform:translateY(-2px);transform:translateY(-2px);background-color:#bbb}30%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px -2px 0 #bbb,32px -6px 0 #999;-webkit-transform:translateY(0);transform:translateY(0)}35%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px -2px 0 #bbb}40%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}to{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}}@keyframes loading-wave-dots{0%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}5%{box-shadow:-32px -4px 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}10%{box-shadow:-32px -6px 0 #999,-16px -4px 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}15%{box-shadow:-32px 2px 0 #bbb,-16px -2px 0 #999,16px 4px 0 #bbb,32px 4px 0 #bbb;-webkit-transform:translateY(-4px);transform:translateY(-4px);background-color:#bbb}20%{box-shadow:-32px 6px 0 #bbb,-16px 4px 0 #bbb,16px 2px 0 #bbb,32px 6px 0 #bbb;-webkit-transform:translateY(-6px);transform:translateY(-6px);background-color:#999}25%{box-shadow:-32px 2px 0 #bbb,-16px 2px 0 #bbb,16px -4px 0 #999,32px -2px 0 #bbb;-webkit-transform:translateY(-2px);transform:translateY(-2px);background-color:#bbb}30%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px -2px 0 #bbb,32px -6px 0 #999;-webkit-transform:translateY(0);transform:translateY(0)}35%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px -2px 0 #bbb}40%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}to{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}}.loading-circles[data-v-50793f02]{position:relative}.loading-circles[data-v-50793f02]:before{content:"";position:absolute;left:50%;top:50%;margin-top:-2.5px;margin-left:-2.5px;width:5px;height:5px;border-radius:50%;-webkit-animation:linear loading-circles .75s infinite;animation:linear loading-circles .75s infinite}@-webkit-keyframes loading-circles{0%{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}12.5%{box-shadow:0 -12px 0 #dfdfdf,8.52px -8.52px 0 #505050,12px 0 0 #646464,8.52px 8.52px 0 #797979,0 12px 0 #8d8d8d,-8.52px 8.52px 0 #a2a2a2,-12px 0 0 #b6b6b6,-8.52px -8.52px 0 #cacaca}25%{box-shadow:0 -12px 0 #cacaca,8.52px -8.52px 0 #dfdfdf,12px 0 0 #505050,8.52px 8.52px 0 #646464,0 12px 0 #797979,-8.52px 8.52px 0 #8d8d8d,-12px 0 0 #a2a2a2,-8.52px -8.52px 0 #b6b6b6}37.5%{box-shadow:0 -12px 0 #b6b6b6,8.52px -8.52px 0 #cacaca,12px 0 0 #dfdfdf,8.52px 8.52px 0 #505050,0 12px 0 #646464,-8.52px 8.52px 0 #797979,-12px 0 0 #8d8d8d,-8.52px -8.52px 0 #a2a2a2}50%{box-shadow:0 -12px 0 #a2a2a2,8.52px -8.52px 0 #b6b6b6,12px 0 0 #cacaca,8.52px 8.52px 0 #dfdfdf,0 12px 0 #505050,-8.52px 8.52px 0 #646464,-12px 0 0 #797979,-8.52px -8.52px 0 #8d8d8d}62.5%{box-shadow:0 -12px 0 #8d8d8d,8.52px -8.52px 0 #a2a2a2,12px 0 0 #b6b6b6,8.52px 8.52px 0 #cacaca,0 12px 0 #dfdfdf,-8.52px 8.52px 0 #505050,-12px 0 0 #646464,-8.52px -8.52px 0 #797979}75%{box-shadow:0 -12px 0 #797979,8.52px -8.52px 0 #8d8d8d,12px 0 0 #a2a2a2,8.52px 8.52px 0 #b6b6b6,0 12px 0 #cacaca,-8.52px 8.52px 0 #dfdfdf,-12px 0 0 #505050,-8.52px -8.52px 0 #646464}87.5%{box-shadow:0 -12px 0 #646464,8.52px -8.52px 0 #797979,12px 0 0 #8d8d8d,8.52px 8.52px 0 #a2a2a2,0 12px 0 #b6b6b6,-8.52px 8.52px 0 #cacaca,-12px 0 0 #dfdfdf,-8.52px -8.52px 0 #505050}to{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}}@keyframes loading-circles{0%{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}12.5%{box-shadow:0 -12px 0 #dfdfdf,8.52px -8.52px 0 #505050,12px 0 0 #646464,8.52px 8.52px 0 #797979,0 12px 0 #8d8d8d,-8.52px 8.52px 0 #a2a2a2,-12px 0 0 #b6b6b6,-8.52px -8.52px 0 #cacaca}25%{box-shadow:0 -12px 0 #cacaca,8.52px -8.52px 0 #dfdfdf,12px 0 0 #505050,8.52px 8.52px 0 #646464,0 12px 0 #797979,-8.52px 8.52px 0 #8d8d8d,-12px 0 0 #a2a2a2,-8.52px -8.52px 0 #b6b6b6}37.5%{box-shadow:0 -12px 0 #b6b6b6,8.52px -8.52px 0 #cacaca,12px 0 0 #dfdfdf,8.52px 8.52px 0 #505050,0 12px 0 #646464,-8.52px 8.52px 0 #797979,-12px 0 0 #8d8d8d,-8.52px -8.52px 0 #a2a2a2}50%{box-shadow:0 -12px 0 #a2a2a2,8.52px -8.52px 0 #b6b6b6,12px 0 0 #cacaca,8.52px 8.52px 0 #dfdfdf,0 12px 0 #505050,-8.52px 8.52px 0 #646464,-12px 0 0 #797979,-8.52px -8.52px 0 #8d8d8d}62.5%{box-shadow:0 -12px 0 #8d8d8d,8.52px -8.52px 0 #a2a2a2,12px 0 0 #b6b6b6,8.52px 8.52px 0 #cacaca,0 12px 0 #dfdfdf,-8.52px 8.52px 0 #505050,-12px 0 0 #646464,-8.52px -8.52px 0 #797979}75%{box-shadow:0 -12px 0 #797979,8.52px -8.52px 0 #8d8d8d,12px 0 0 #a2a2a2,8.52px 8.52px 0 #b6b6b6,0 12px 0 #cacaca,-8.52px 8.52px 0 #dfdfdf,-12px 0 0 #505050,-8.52px -8.52px 0 #646464}87.5%{box-shadow:0 -12px 0 #646464,8.52px -8.52px 0 #797979,12px 0 0 #8d8d8d,8.52px 8.52px 0 #a2a2a2,0 12px 0 #b6b6b6,-8.52px 8.52px 0 #cacaca,-12px 0 0 #dfdfdf,-8.52px -8.52px 0 #505050}to{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}}.loading-bubbles[data-v-50793f02]{position:relative}.loading-bubbles[data-v-50793f02]:before{content:"";position:absolute;left:50%;top:50%;margin-top:-.5px;margin-left:-.5px;width:1px;height:1px;border-radius:50%;-webkit-animation:linear loading-bubbles .85s infinite;animation:linear loading-bubbles .85s infinite}@-webkit-keyframes loading-bubbles{0%{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}12.5%{box-shadow:0 -12px 0 3.2px #666,8.52px -8.52px 0 .4px #666,12px 0 0 .8px #666,8.52px 8.52px 0 1.2px #666,0 12px 0 1.6px #666,-8.52px 8.52px 0 2px #666,-12px 0 0 2.4px #666,-8.52px -8.52px 0 2.8px #666}25%{box-shadow:0 -12px 0 2.8px #666,8.52px -8.52px 0 3.2px #666,12px 0 0 .4px #666,8.52px 8.52px 0 .8px #666,0 12px 0 1.2px #666,-8.52px 8.52px 0 1.6px #666,-12px 0 0 2px #666,-8.52px -8.52px 0 2.4px #666}37.5%{box-shadow:0 -12px 0 2.4px #666,8.52px -8.52px 0 2.8px #666,12px 0 0 3.2px #666,8.52px 8.52px 0 .4px #666,0 12px 0 .8px #666,-8.52px 8.52px 0 1.2px #666,-12px 0 0 1.6px #666,-8.52px -8.52px 0 2px #666}50%{box-shadow:0 -12px 0 2px #666,8.52px -8.52px 0 2.4px #666,12px 0 0 2.8px #666,8.52px 8.52px 0 3.2px #666,0 12px 0 .4px #666,-8.52px 8.52px 0 .8px #666,-12px 0 0 1.2px #666,-8.52px -8.52px 0 1.6px #666}62.5%{box-shadow:0 -12px 0 1.6px #666,8.52px -8.52px 0 2px #666,12px 0 0 2.4px #666,8.52px 8.52px 0 2.8px #666,0 12px 0 3.2px #666,-8.52px 8.52px 0 .4px #666,-12px 0 0 .8px #666,-8.52px -8.52px 0 1.2px #666}75%{box-shadow:0 -12px 0 1.2px #666,8.52px -8.52px 0 1.6px #666,12px 0 0 2px #666,8.52px 8.52px 0 2.4px #666,0 12px 0 2.8px #666,-8.52px 8.52px 0 3.2px #666,-12px 0 0 .4px #666,-8.52px -8.52px 0 .8px #666}87.5%{box-shadow:0 -12px 0 .8px #666,8.52px -8.52px 0 1.2px #666,12px 0 0 1.6px #666,8.52px 8.52px 0 2px #666,0 12px 0 2.4px #666,-8.52px 8.52px 0 2.8px #666,-12px 0 0 3.2px #666,-8.52px -8.52px 0 .4px #666}to{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}}@keyframes loading-bubbles{0%{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}12.5%{box-shadow:0 -12px 0 3.2px #666,8.52px -8.52px 0 .4px #666,12px 0 0 .8px #666,8.52px 8.52px 0 1.2px #666,0 12px 0 1.6px #666,-8.52px 8.52px 0 2px #666,-12px 0 0 2.4px #666,-8.52px -8.52px 0 2.8px #666}25%{box-shadow:0 -12px 0 2.8px #666,8.52px -8.52px 0 3.2px #666,12px 0 0 .4px #666,8.52px 8.52px 0 .8px #666,0 12px 0 1.2px #666,-8.52px 8.52px 0 1.6px #666,-12px 0 0 2px #666,-8.52px -8.52px 0 2.4px #666}37.5%{box-shadow:0 -12px 0 2.4px #666,8.52px -8.52px 0 2.8px #666,12px 0 0 3.2px #666,8.52px 8.52px 0 .4px #666,0 12px 0 .8px #666,-8.52px 8.52px 0 1.2px #666,-12px 0 0 1.6px #666,-8.52px -8.52px 0 2px #666}50%{box-shadow:0 -12px 0 2px #666,8.52px -8.52px 0 2.4px #666,12px 0 0 2.8px #666,8.52px 8.52px 0 3.2px #666,0 12px 0 .4px #666,-8.52px 8.52px 0 .8px #666,-12px 0 0 1.2px #666,-8.52px -8.52px 0 1.6px #666}62.5%{box-shadow:0 -12px 0 1.6px #666,8.52px -8.52px 0 2px #666,12px 0 0 2.4px #666,8.52px 8.52px 0 2.8px #666,0 12px 0 3.2px #666,-8.52px 8.52px 0 .4px #666,-12px 0 0 .8px #666,-8.52px -8.52px 0 1.2px #666}75%{box-shadow:0 -12px 0 1.2px #666,8.52px -8.52px 0 1.6px #666,12px 0 0 2px #666,8.52px 8.52px 0 2.4px #666,0 12px 0 2.8px #666,-8.52px 8.52px 0 3.2px #666,-12px 0 0 .4px #666,-8.52px -8.52px 0 .8px #666}87.5%{box-shadow:0 -12px 0 .8px #666,8.52px -8.52px 0 1.2px #666,12px 0 0 1.6px #666,8.52px 8.52px 0 2px #666,0 12px 0 2.4px #666,-8.52px 8.52px 0 2.8px #666,-12px 0 0 3.2px #666,-8.52px -8.52px 0 .4px #666}to{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}}.loading-default[data-v-50793f02]{position:relative;border:1px solid #999;-webkit-animation:ease loading-rotating 1.5s infinite;animation:ease loading-rotating 1.5s infinite}.loading-default[data-v-50793f02]:before{content:"";position:absolute;display:block;top:0;left:50%;margin-top:-3px;margin-left:-3px;width:6px;height:6px;background-color:#999;border-radius:50%}.loading-spiral[data-v-50793f02]{border:2px solid #777;border-right-color:transparent;-webkit-animation:linear loading-rotating .85s infinite;animation:linear loading-rotating .85s infinite}@-webkit-keyframes loading-rotating{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes loading-rotating{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}.infinite-loading-container[data-v-50793f02]{clear:both;text-align:center}.infinite-loading-container [class^=loading-][data-v-50793f02]{display:inline-block;margin:15px 0;width:28px;height:28px;font-size:28px;line-height:28px;border-radius:50%}.infinite-status-prompt[data-v-50793f02]{color:#666;font-size:14px;text-align:center;padding:10px 0}',""])},function(p,x){p.exports=function(){var p=[];return p.toString=function(){for(var p=[],x=0;x<this.length;x++){var t=this[x];t[2]?p.push("@media "+t[2]+"{"+t[1]+"}"):p.push(t[1])}return p.join("")},p.i=function(x,t){"string"==typeof x&&(x=[[null,x,""]]);for(var a={},e=0;e<this.length;e++){var o=this[e][0];"number"==typeof o&&(a[o]=!0)}for(e=0;e<x.length;e++){var n=x[e];"number"==typeof n[0]&&a[n[0]]||(t&&!n[2]?n[2]=t:t&&(n[2]="("+n[2]+") and ("+t+")"),p.push(n))}},p}},function(p,x,t){var a,e;t(7),a=t(1);var o=t(5);e=a=a||{},"object"!=typeof a.default&&"function"!=typeof a.default||(e=a=a.default),"function"==typeof e&&(e=e.options),e.render=o.render,e.staticRenderFns=o.staticRenderFns,e._scopeId="data-v-50793f02",p.exports=a},function(p,x){p.exports={render:function(){var p=this,x=p.$createElement,t=p._self._c||x;return t("div",{staticClass:"infinite-loading-container"},[t("div",{directives:[{name:"show",rawName:"v-show",value:p.isLoading,expression:"isLoading"}]},[p._t("spinner",[t("i",{class:p.spinnerType})])],2),p._v(" "),t("div",{directives:[{name:"show",rawName:"v-show",value:!p.isLoading&&p.isComplete&&p.isFirstLoad,expression:"!isLoading && isComplete && isFirstLoad"}],staticClass:"infinite-status-prompt"},[p._t("no-results",[p._v("No results :(")])],2),p._v(" "),t("div",{directives:[{name:"show",rawName:"v-show",value:!p.isLoading&&p.isComplete&&!p.isFirstLoad,expression:"!isLoading && isComplete && !isFirstLoad"}],staticClass:"infinite-status-prompt"},[p._t("no-more",[p._v("No more data :)")])],2)])},staticRenderFns:[]}},function(p,x,t){function a(p,x){for(var t=0;t<p.length;t++){var a=p[t],e=d[a.id];if(e){e.refs++;for(var o=0;o<e.parts.length;o++)e.parts[o](a.parts[o]);for(;o<a.parts.length;o++)e.parts.push(r(a.parts[o],x))}else{for(var n=[],o=0;o<a.parts.length;o++)n.push(r(a.parts[o],x));d[a.id]={id:a.id,refs:1,parts:n}}}}function e(p){for(var x=[],t={},a=0;a<p.length;a++){var e=p[a],o=e[0],n=e[1],i=e[2],r=e[3],s={css:n,media:i,sourceMap:r};t[o]?t[o].parts.push(s):x.push(t[o]={id:o,parts:[s]})}return x}function o(p,x){var t=c(),a=m[m.length-1];if("top"===p.insertAt)a?a.nextSibling?t.insertBefore(x,a.nextSibling):t.appendChild(x):t.insertBefore(x,t.firstChild),m.push(x);else{if("bottom"!==p.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");t.appendChild(x)}}function n(p){p.parentNode.removeChild(p);var x=m.indexOf(p);x>=0&&m.splice(x,1)}function i(p){var x=document.createElement("style");return x.type="text/css",o(p,x),x}function r(p,x){var t,a,e;if(x.singleton){var o=h++;t=u||(u=i(x)),a=s.bind(null,t,o,!1),e=s.bind(null,t,o,!0)}else t=i(x),a=b.bind(null,t),e=function(){n(t)};return a(p),function(x){if(x){if(x.css===p.css&&x.media===p.media&&x.sourceMap===p.sourceMap)return;a(p=x)}else e()}}function s(p,x,t,a){var e=t?"":a.css;if(p.styleSheet)p.styleSheet.cssText=g(x,e);else{var o=document.createTextNode(e),n=p.childNodes;n[x]&&p.removeChild(n[x]),n.length?p.insertBefore(o,n[x]):p.appendChild(o)}}function b(p,x){var t=x.css,a=x.media,e=x.sourceMap;if(a&&p.setAttribute("media",a),e&&(t+="\n/*# sourceURL="+e.sources[0]+" */",t+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(e))))+" */"),p.styleSheet)p.styleSheet.cssText=t;else{for(;p.firstChild;)p.removeChild(p.firstChild);p.appendChild(document.createTextNode(t))}}var d={},l=function(p){var x;return function(){return"undefined"==typeof x&&(x=p.apply(this,arguments)),x}},f=l(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),c=l(function(){return document.head||document.getElementsByTagName("head")[0]}),u=null,h=0,m=[];p.exports=function(p,x){x=x||{},"undefined"==typeof x.singleton&&(x.singleton=f()),"undefined"==typeof x.insertAt&&(x.insertAt="bottom");var t=e(p);return a(t,x),function(p){for(var o=[],n=0;n<t.length;n++){var i=t[n],r=d[i.id];r.refs--,o.push(r)}if(p){var s=e(p);a(s,x)}for(var n=0;n<o.length;n++){var r=o[n];if(0===r.refs){for(var b=0;b<r.parts.length;b++)r.parts[b]();delete d[r.id]}}}};var g=function(){var p=[];return function(x,t){return p[x]=t,p.filter(Boolean).join("\n")}}()},function(p,x,t){var a=t(2);"string"==typeof a&&(a=[[p.id,a,""]]);t(6)(a,{});a.locals&&(p.exports=a.locals)}])});

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WycMessage_vue__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WycMessage_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_WycMessage_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_WycUserSearch_vue__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_WycUserSearch_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_WycUserSearch_vue__);



new Vue({
    el: '#chatbox',

    data: {
        userLatestMessages: [],
        selected: null
    },

    router: new VueRouter({
        routes: [{
            path: '/u/:id',
            name: 'user',
            component: __webpack_require__(221)
        }, {
            path: '/cr/:id',
            name: 'chat-room',
            component: __webpack_require__(222)
        }]
    }),

    watch: {
        userLatestMessages: function userLatestMessages(val) {
            setTimeout(function () {
                $('#user-message').slimScroll({
                    height: '100%'
                });
            }, 1);
        }
    },

    methods: {
        getLatestMessages: function getLatestMessages() {
            var _this = this;

            axiosAPIv1.get('/messages').then(function (result) {
                console.log(result);
                _this.userLatestMessages = result.data;
            });
        },
        loadSelectedUser: function loadSelectedUser(data) {
            this.selected = data;

            this.$router.push({
                name: 'user',
                params: {
                    id: data.id
                }
            });
        },
        loadSelectedChatRoom: function loadSelectedChatRoom(data) {
            this.selected = data;

            this.$router.push({
                name: 'chat-room',
                params: {
                    id: data.id
                }
            });
        },
        newMessage: function newMessage() {
            Event.fire('user-search.show');
        }
    },

    components: {
        WycMessage: __WEBPACK_IMPORTED_MODULE_0__components_WycMessage_vue___default.a,
        WycUserSearch: __WEBPACK_IMPORTED_MODULE_1__components_WycUserSearch_vue___default.a
    },

    created: function created() {
        this.getLatestMessages();

        Event.listen('user-search.selected', this.loadSelectedUser);
        Event.listen('chat-room.selected', this.loadSelectedChatRoom);
        Event.listen('chat-box.send', this.getLatestMessages);
        Event.listen('chat-box.marked-as-read', this.getLatestMessages);
        Event.listen('new-chat-message', this.getLatestMessages);
    }
});

/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            user: null,
            messages: [],
            newMessage: null,
            currentPage: 0,
            chatRoomId: null,
            $chatBox: null,
            $conversation: null
        };
    },


    watch: {
        $route: function $route(to, from) {
            Event.unlisten('chat-room-' + this.chatRoomId);

            this.user = null;
            this.messages = [];
            this.newMessage = null;
            this.currentPage = 0;
            this.chatRoomId = null;
            this.$chatBox = null;
            this.$conversation = null;

            this.getUser();
        }
    },

    methods: {
        onInfinite: function onInfinite() {
            this.getMessages();
        },
        getUser: function getUser() {
            var _this = this;

            return axiosAPIv1.get('/users/' + this.$route.params.id + '?include=relationship').then(function (result) {
                _this.user = result.data;

                setTimeout(function () {
                    var $el = $(this.$el);

                    this.$chatBox = $el.find('.chat-box');
                    this.$conversation = $el.find('.panel-body');
                    this.$conversation.slimScroll({
                        height: '440px'
                    });
                }.bind(_this), 1);

                _this.markAllAsRead();

                Event.fire('user-search.selected', result.data);
            });
        },
        getMessages: function getMessages() {
            var _this2 = this;

            var prevScrollHeight = this.$conversation.prop('scrollHeight');

            return axiosAPIv1.get('/messages/user/' + this.user.id, {
                params: {
                    page: ++this.currentPage,
                    limit: 999
                }
            }).then(function (result) {
                var data = result.data.data;

                if (data && data.length) {
                    data.sort(function (a, b) {
                        return a.id - b.id;
                    });

                    _this2.messages = data.concat(_this2.messages);
                    _this2.currentPage = result.data.current_page;

                    if (!_this2.chatRoomId) {
                        _this2.chatRoomId = data[0].chat_room_id;
                        _this2.listen();
                    }

                    setTimeout(function () {
                        if (this.currentPage === 1) {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
                        } else {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight') - prevScrollHeight);
                        }

                        this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
                    }.bind(_this2), 1);
                } else {
                    _this2.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                }
            });
        },
        markAllAsRead: function markAllAsRead() {
            return axiosAPIv1.post('/messages/mark-all-as-read', {
                to_user: this.user.id
            }).then(function (result) {
                Event.fire('chat-box.marked-as-read');
            });
        },
        send: function send() {
            var _this3 = this;

            if (!this.newMessage || this.isBlocked) {
                return;
            }

            this.messages.push({
                user_id: window.Laravel.user.id,
                body: this.newMessage
            });

            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));

                this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
            }.bind(this), 1);

            axiosAPIv1.post('/messages', {
                to_user: this.user.id,
                body: this.newMessage
            }).then(function (result) {
                if (!_this3.chatRoomId) {
                    _this3.chatRoomId = result.data.data.chat_room_id;
                    _this3.listen();
                }

                Event.fire('chat-box.send');
            });

            this.newMessage = null;
        },
        close: function close() {
            Event.fire('chat-box.close', this.user);
        },
        isMe: function isMe(message) {
            return window.Laravel.user.id == message.user_id;
        },
        scrollToBottom: function scrollToBottom() {
            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
            }.bind(this), 1);
        },
        listen: function listen() {
            Event.listen('chat-room-' + this.chatRoomId, function (message) {
                if (message.chat_room_id == this.chatRoomId) {
                    this.messages.push(message);
                    this.scrollToBottom();
                }
            }.bind(this));
        },
        request: function request() {
            var _this4 = this;

            return axiosAPIv1.post('/friends', {
                user_id2: this.user.id
            }).then(function (result) {
                return _this4.getUser();
            });
        },
        confirm: function confirm() {
            var _this5 = this;

            return axiosAPIv1.post('/friends/' + this.user.friend_id + '/confirm').then(function (result) {
                return _this5.getUser();
            });
        },
        cancel: function cancel() {
            return this.deleteRequest();
        },
        unfriend: function unfriend() {
            return this.deleteRequest();
        },
        block: function block() {
            var _this6 = this;

            return axiosAPIv1.post('/friends/block', {
                user_id2: this.user.id
            }).then(function (result) {
                return _this6.getUser();
            });
        },
        unblock: function unblock() {
            return this.deleteRequest();
        },
        deleteRequest: function deleteRequest() {
            var _this7 = this;

            return axiosAPIv1.delete('/friends/' + this.user.friend_id).then(function (result) {
                return _this7.getUser();
            });
        }
    },

    computed: {
        fullName: function fullName() {
            return this.user.first_name + ' ' + this.user.last_name;
        },
        isBlocked: function isBlocked() {
            return this.user.friend_status === 'Blocked';
        },
        showSettings: function showSettings() {
            if (this.user.friend_user_id == window.Laravel.user.id) {
                return true;
            } else if (this.user.friend_status === 'Blocked') {
                return false;
            }

            return true;
        }
    },

    components: {
        InfiniteLoading: __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default.a
    },

    created: function created() {
        this.getUser();
    },
    mounted: function mounted() {
        $.contextMenu({
            selector: '#chat-box-settings',
            trigger: 'left',

            callback: function (key, options) {
                this[key].apply(this, arguments);
            }.bind(this),

            build: function ($triggerElement, e) {
                var userId = this.user.friend_user_id,
                    status = this.user.friend_status,
                    items = {};

                if (status === 'Request' && userId != window.Laravel.user.id) {
                    items['confirm'] = { name: 'Confirm', icon: 'fa-check' };
                    items['deleteRequest'] = { name: 'Delete Request', icon: 'fa-times' };
                } else if (status === 'Request') {
                    items['cancel'] = { name: 'Cancel Request', icon: 'fa-times' };
                } else if (status === 'Friend') {
                    items['unfriend'] = { name: 'Unfriend', icon: 'fa-times' };
                } else {
                    items['request'] = { name: 'Send Request', icon: 'fa-plus' };
                }

                if (status === 'Blocked') {
                    items = {
                        unblock: { name: 'Unblock', icon: 'fa-check' }
                    };
                } else {
                    items['block'] = { name: 'Block', icon: 'fa-ban' };
                }

                return { items: items };
            }.bind(this)
        });
    }
});

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            chatRoom: null,
            messages: [],
            newMessage: null,
            currentPage: 0,
            $chatBox: null,
            $conversation: null
        };
    },


    watch: {
        $route: function $route(to, from) {
            Event.unlisten( true ? this.chatRoom.id : null);

            this.chatRoom = null;
            this.messages = [];
            this.newMessage = null;
            this.currentPage = 0;
            this.$chatBox = null;
            this.$conversation = null;

            this.getChatRoom();
        }
    },

    methods: {
        onInfinite: function onInfinite() {
            this.getMessages();
        },
        getChatRoom: function getChatRoom() {
            var _this = this;

            axiosAPIv1.get('/chat-rooms/' + this.$route.params.id).then(function (result) {
                _this.chatRoom = result.data;

                setTimeout(function () {
                    var $el = $(this.$el);

                    this.$chatBox = $el.find('.chat-box');
                    this.$conversation = $el.find('.panel-body');
                    this.$conversation.slimScroll({
                        height: '479px'
                    });
                }.bind(_this), 1);

                _this.markAllAsRead();

                _this.listen();

                Event.fire('chat-room.selected', result.data);
            });
        },
        getMessages: function getMessages() {
            var _this2 = this;

            var prevScrollHeight = this.$conversation.prop('scrollHeight');

            return axiosAPIv1.get('/messages/chat-room/' + this.chatRoom.id, {
                params: {
                    page: ++this.currentPage,
                    limit: 999
                }
            }).then(function (result) {
                var data = result.data.data;

                if (data && data.length) {
                    data.sort(function (a, b) {
                        return a.id - b.id;
                    });

                    _this2.messages = data.concat(_this2.messages);
                    _this2.currentPage = result.data.current_page;

                    setTimeout(function () {
                        if (this.currentPage === 1) {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
                        } else {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight') - prevScrollHeight);
                        }

                        this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
                    }.bind(_this2), 1);
                } else {
                    _this2.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                }
            });
        },
        markAllAsRead: function markAllAsRead() {
            return axiosAPIv1.post('/messages/mark-all-as-read', {
                to_chat_room: this.chatRoom.id
            }).then(function (result) {
                Event.fire('chat-box.marked-as-read');
            });
        },
        send: function send() {
            if (!this.newMessage) {
                return;
            }

            this.messages.push({
                user_id: window.Laravel.user.id,
                body: this.newMessage
            });

            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));

                this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
            }.bind(this), 1);

            axiosAPIv1.post('/messages', {
                to_chat_room: this.chatRoom.id,
                body: this.newMessage
            }).then(function (result) {
                return Event.fire('chat-box.send');
            });

            this.newMessage = null;
        },
        close: function close() {
            Event.fire('chat-box.close', this.chatRoom);
        },
        isMe: function isMe(message) {
            return window.Laravel.user.id == message.user_id;
        },
        scrollToBottom: function scrollToBottom() {
            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
            }.bind(this), 1);
        },
        listen: function listen() {
            Event.listen('chat-room-' + this.chatRoom.id, function (message) {
                if (message.chat_room_id == this.chatRoom.id) {
                    this.messages.push(message);
                    this.scrollToBottom();
                }
            }.bind(this));
        }
    },

    components: {
        InfiniteLoading: __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default.a
    },

    created: function created() {
        this.getChatRoom();
    }
});

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['message'],

    computed: {
        routeTo: function routeTo() {
            if (this.message.chat_room.name) {
                return {
                    name: 'chat-room',
                    params: {
                        id: this.message.chat_room.id
                    }
                };
            }

            return {
                name: 'user',
                params: {
                    id: this.message.chat_room.users[0].id
                }
            };
        },
        notRead: function notRead() {
            if (this.message.user_id == window.Laravel.user.id) {
                return false;
            }

            return !this.message.read_at;
        },
        avatar: function avatar() {
            return this.message.chat_room.users[0].avatar;
        },
        fullName: function fullName() {
            if (this.message.chat_room.name) {
                return this.message.chat_room.users[0].full_name + ' - ' + this.message.chat_room.name;
            }

            return this.message.chat_room.name || this.message.chat_room.users[0].full_name;
        },
        fromNow: function fromNow() {
            return moment(this.message.created_at).fromNow();
        }
    }
});

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['target', 'panelClass'],

    data: function data() {
        return {
            targetEl: null,
            search: '',
            users: [],
            usersContainer: null,
            timeout: null
        };
    },


    watch: {
        search: function search(val) {
            if (val != null && val.length === 0) {
                this.users = [];
            }
        },
        users: function users(val) {
            setTimeout(function () {
                this.usersContainer.slimScroll();
            }.bind(this), 1);
        }
    },

    methods: {
        searchUser: function searchUser(e) {
            if ((e.which <= 90 && e.which >= 48 || e.which === 8) && this.search && this.search.length >= 2) {
                clearTimeout(this.timeout);

                this.timeout = setTimeout(function () {
                    var _this = this;

                    axiosAPIv1.get('/messages/users?search=' + this.search).then(function (result) {
                        if (_this.search && _this.search.length >= 2) {
                            _this.users = result.data;
                        }
                    });
                }.bind(this), 200);
            }
        },
        select: function select(user) {
            Event.fire('user-search.selected', user);

            this._reset();
        },
        avatar: function avatar(user) {
            return user.avatar;
        },
        _positionEl: function _positionEl() {
            var offset = this.targetEl.offset(),
                teWidth = this.targetEl.outerWidth(),
                teHeight = this.targetEl.outerHeight(),
                wWidth = window.innerWidth,
                position = {
                top: offset.top + teHeight + 10,
                left: offset.left
            };

            var $el = $(this.$el),
                $elWidth = $el.outerWidth() || $el.innerWidth();

            if (offset.left + $elWidth > wWidth) {
                position.left = offset.left - $elWidth + teWidth;
            }

            $el.offset(position);
        },
        _reset: function _reset() {
            var $el = $(this.$el),
                $input = $el.find('input'),
                $event = $.Event('keyup', { which: 8 });

            this.search = null;
            this.users = [];

            $input.trigger($event);
            $input.blur();

            $el.hide();
        },
        _registerEventListeners: function _registerEventListeners() {
            var _this2 = this;

            $(window).resize(this._positionEl);

            Event.listen('user-search.show', function () {
                var $el = $(_this2.$el);

                $el.show();
                $el.find('input').focus();

                _this2._positionEl();
            });

            // Remove/Hide floating panels on outside click
            $(document).mouseup(function (e) {
                var $el = $(this.$el);

                // if the target of the click isn't the panel nor a descendant of the panel
                if (!$el.is(e.target) && $el.has(e.target).length === 0) {
                    this._reset();
                }
            }.bind(this));
        }
    },

    created: function created() {
        this._registerEventListeners();
    },
    mounted: function mounted() {
        this.targetEl = $(this.target);
        this.usersContainer = $('.users-container');
        // this.usersContainer.perfectScrollbar();

        this.usersContainer.slimScroll({
            size: '4px',
            height: ''
        });
    }
});

/***/ }),

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(185),
  /* template */
  __webpack_require__(234),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\cpanel\\components\\WycChatbox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] WycChatbox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2fc21a52", Component.options)
  } else {
    hotAPI.reload("data-v-2fc21a52", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 222:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(186),
  /* template */
  __webpack_require__(232),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\cpanel\\components\\WycChatboxRoom.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] WycChatboxRoom.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-26d4bad2", Component.options)
  } else {
    hotAPI.reload("data-v-26d4bad2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 225:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(189),
  /* template */
  __webpack_require__(244),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\cpanel\\components\\WycMessage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] WycMessage.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c4f1052a", Component.options)
  } else {
    hotAPI.reload("data-v-c4f1052a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 227:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(191),
  /* template */
  __webpack_require__(242),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\cpanel\\components\\WycUserSearch.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] WycUserSearch.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-89171ec2", Component.options)
  } else {
    hotAPI.reload("data-v-89171ec2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 232:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.chatRoom) ? _c('div', {
    staticClass: "panel panel-chat-box"
  }, [_c('div', {
    staticClass: "chat-box"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('infinite-loading', {
    ref: "infiniteLoading",
    attrs: {
      "on-infinite": _vm.onInfinite,
      "direction": "top",
      "distance": 0
    }
  }), _vm._v(" "), _c('ul', _vm._l((_vm.messages), function(message) {
    return _c('li', {
      class: {
        me: _vm.isMe(message)
      }
    }, [_c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "col-xs-12"
    }, [_c('div', {
      staticClass: "message",
      staticStyle: {
        "max-width": "100%",
        "word-wrap": "break-word"
      }
    }, [_vm._v(_vm._s(message.body))])])])])
  }))], 1), _vm._v(" "), _c('div', {
    staticClass: "panel-footer"
  }, [(_vm.chatRoom.is_active) ? _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newMessage),
      expression: "newMessage"
    }],
    attrs: {
      "placeholder": "Write something..."
    },
    domProps: {
      "value": (_vm.newMessage)
    },
    on: {
      "keydown": function($event) {
        if (!('button' in $event) && _vm._k($event.keyCode, "enter", 13)) { return null; }
        $event.preventDefault();
        _vm.send($event)
      },
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.newMessage = $event.target.value
      }
    }
  }) : _c('textarea', {
    attrs: {
      "disabled": ""
    }
  }, [_vm._v("This issue is already closed")])])])]) : _vm._e()
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-26d4bad2", module.exports)
  }
}

/***/ }),

/***/ 234:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.user) ? _c('div', {
    staticClass: "panel panel-chat-box"
  }, [_c('div', {
    staticClass: "panel-heading"
  }, [_vm._v("\r\n        " + _vm._s(_vm.fullName) + "\r\n        "), (_vm.showSettings) ? _c('a', {
    staticClass: "pull-right",
    attrs: {
      "href": "javascript:void(0)",
      "id": "chat-box-settings"
    }
  }, [_c('i', {
    staticClass: "fa fa-cog"
  })]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "chat-box"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('infinite-loading', {
    ref: "infiniteLoading",
    attrs: {
      "on-infinite": _vm.onInfinite,
      "direction": "top",
      "distance": 0
    }
  }), _vm._v(" "), _c('ul', _vm._l((_vm.messages), function(message) {
    return _c('li', {
      class: {
        me: _vm.isMe(message)
      }
    }, [_c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "col-xs-12"
    }, [_c('div', {
      staticClass: "message",
      staticStyle: {
        "max-width": "100%",
        "word-wrap": "break-word"
      }
    }, [_vm._v(_vm._s(message.body))])])])])
  }))], 1), _vm._v(" "), _c('div', {
    staticClass: "panel-footer"
  }, [_c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newMessage),
      expression: "newMessage"
    }],
    attrs: {
      "disabled": _vm.isBlocked,
      "placeholder": "Write something..."
    },
    domProps: {
      "value": (_vm.newMessage)
    },
    on: {
      "keydown": function($event) {
        if (!('button' in $event) && _vm._k($event.keyCode, "enter", 13)) { return null; }
        if (!$event.shiftKey) { return null; }
        $event.preventDefault();
        _vm.send($event)
      },
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.newMessage = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    staticClass: "btn-send-message",
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": _vm.send
    }
  }, [_c('i', {
    staticClass: "fa fa-send"
  })])])])]) : _vm._e()
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-2fc21a52", module.exports)
  }
}

/***/ }),

/***/ 242:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "panel panel-default panel-float",
    class: _vm.panelClass
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.search),
      expression: "search"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "user-search",
      "placeholder": "Search..."
    },
    domProps: {
      "value": (_vm.search)
    },
    on: {
      "keyup": _vm.searchUser,
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.search = $event.target.value
      }
    }
  }), _vm._v(" "), _c('ul', {
    staticClass: "users-container"
  }, _vm._l((_vm.users), function(user) {
    return _c('li', {
      on: {
        "click": function($event) {
          _vm.select(user)
        }
      }
    }, [_c('img', {
      staticClass: "img-circle",
      attrs: {
        "src": user.avatar
      }
    }), _vm._v(" "), _c('span', [_vm._v(_vm._s(user.full_name))])])
  }))])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-89171ec2", module.exports)
  }
}

/***/ }),

/***/ 244:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('router-link', {
    staticClass: "row",
    class: {
      'not-read': _vm.notRead
    },
    attrs: {
      "to": _vm.routeTo
    }
  }, [_c('div', {
    staticClass: "col-xs-2 message-user-avatar"
  }, [_c('img', {
    staticClass: "img-circle",
    attrs: {
      "src": _vm.avatar
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-7 message-content"
  }, [_c('div', {
    staticClass: "ellipsis"
  }, [_c('strong', [_vm._v(_vm._s(_vm.fullName))])]), _vm._v(" "), _c('div', {
    staticClass: "ellipsis"
  }, [_vm._v(_vm._s(_vm.message.body))])]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-3 message-date"
  }, [_c('div', {
    staticClass: "text-right"
  }, [_c('small', [_vm._v(_vm._s(_vm.fromNow))])])])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-c4f1052a", module.exports)
  }
}

/***/ }),

/***/ 267:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(145);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDE/NTk0YSoqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqIiwid2VicGFjazovLy8uL34vdnVlLWluZmluaXRlLWxvYWRpbmcvZGlzdC92dWUtaW5maW5pdGUtbG9hZGluZy5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9wYWdlcy9tZXNzYWdlcy5qcyIsIndlYnBhY2s6Ly8vV3ljQ2hhdGJveC52dWUiLCJ3ZWJwYWNrOi8vL1d5Y0NoYXRib3hSb29tLnZ1ZSIsIndlYnBhY2s6Ly8vV3ljTWVzc2FnZS52dWUiLCJ3ZWJwYWNrOi8vL1d5Y1VzZXJTZWFyY2gudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljQ2hhdGJveC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9XeWNDaGF0Ym94Um9vbS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9XeWNNZXNzYWdlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y1VzZXJTZWFyY2gudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljQ2hhdGJveFJvb20udnVlP2Q3MDgiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9XeWNDaGF0Ym94LnZ1ZT80N2Y3Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljVXNlclNlYXJjaC52dWU/NTk3OCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y01lc3NhZ2UudnVlP2Y5ODciXSwibmFtZXMiOlsiVnVlIiwiZWwiLCJkYXRhIiwidXNlckxhdGVzdE1lc3NhZ2VzIiwic2VsZWN0ZWQiLCJyb3V0ZXIiLCJWdWVSb3V0ZXIiLCJyb3V0ZXMiLCJwYXRoIiwibmFtZSIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJ3YXRjaCIsInZhbCIsInNldFRpbWVvdXQiLCIkIiwic2xpbVNjcm9sbCIsImhlaWdodCIsIm1ldGhvZHMiLCJnZXRMYXRlc3RNZXNzYWdlcyIsImF4aW9zQVBJdjEiLCJnZXQiLCJ0aGVuIiwiY29uc29sZSIsImxvZyIsInJlc3VsdCIsImxvYWRTZWxlY3RlZFVzZXIiLCIkcm91dGVyIiwicHVzaCIsInBhcmFtcyIsImlkIiwibG9hZFNlbGVjdGVkQ2hhdFJvb20iLCJuZXdNZXNzYWdlIiwiRXZlbnQiLCJmaXJlIiwiY29tcG9uZW50cyIsIld5Y01lc3NhZ2UiLCJXeWNVc2VyU2VhcmNoIiwiY3JlYXRlZCIsImxpc3RlbiJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBLGVBQWUsNkpBQXlNLGlCQUFpQixtQkFBbUIsY0FBYyw0QkFBNEIsWUFBWSxVQUFVLGlCQUFpQixnRUFBZ0UsU0FBUyxnQ0FBZ0Msa0JBQWtCLGFBQWEsY0FBYywwQkFBMEIsV0FBVyxzQ0FBc0MsU0FBUyxFQUFFLGtCQUFrQiwrR0FBK0csZUFBZSxhQUFhLGNBQWMsNExBQTRMLGtCQUFrQixhQUFhLDREQUE0RCxLQUFLLHFHQUFxRyxNQUFNLFNBQVMsc0NBQXNDLFNBQVMsRUFBRSxPQUFPLG9JQUFvSSxXQUFXLGdCQUFnQixPQUFPLGdGQUFnRixXQUFXLHVCQUF1QixtQ0FBbUMsUUFBUSxVQUFVLHdCQUF3QiwrQ0FBK0MsOEJBQThCLG9CQUFvQixXQUFXLDREQUE0RCxtQ0FBbUMsMkpBQTJKLHlEQUF5RCxrREFBa0QsNEZBQTRGLCtDQUErQyx3SUFBd0ksRUFBRSx3QkFBd0IscUZBQXFGLHNCQUFzQixnRUFBZ0UsVUFBVSx1QkFBdUIsbURBQW1ELGlHQUFpRyxzQkFBc0Isc0ZBQXNGLGlCQUFpQixxRUFBcUUsa0JBQWtCLDJDQUEyQyxXQUFXLGtCQUFrQixRQUFRLFNBQVMsaUJBQWlCLGdCQUFnQixVQUFVLFdBQVcsc0JBQXNCLGtCQUFrQix5REFBeUQsaURBQWlELHFDQUFxQyxHQUFHLHFFQUFxRSxHQUFHLHdFQUF3RSxnQ0FBZ0Msd0JBQXdCLElBQUksMkVBQTJFLGdDQUFnQyx3QkFBd0IsSUFBSSw4RUFBOEUsbUNBQW1DLDJCQUEyQixzQkFBc0IsSUFBSSw2RUFBNkUsbUNBQW1DLDJCQUEyQixzQkFBc0IsSUFBSSwrRUFBK0UsbUNBQW1DLDJCQUEyQixzQkFBc0IsSUFBSSwyRUFBMkUsZ0NBQWdDLHdCQUF3QixJQUFJLHdFQUF3RSxJQUFJLHFFQUFxRSxHQUFHLHNFQUFzRSw2QkFBNkIsR0FBRyxxRUFBcUUsR0FBRyx3RUFBd0UsZ0NBQWdDLHdCQUF3QixJQUFJLDJFQUEyRSxnQ0FBZ0Msd0JBQXdCLElBQUksOEVBQThFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksNkVBQTZFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksK0VBQStFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksMkVBQTJFLGdDQUFnQyx3QkFBd0IsSUFBSSx3RUFBd0UsSUFBSSxxRUFBcUUsR0FBRyxzRUFBc0Usa0NBQWtDLGtCQUFrQix5Q0FBeUMsV0FBVyxrQkFBa0IsU0FBUyxRQUFRLGtCQUFrQixtQkFBbUIsVUFBVSxXQUFXLGtCQUFrQix1REFBdUQsK0NBQStDLG1DQUFtQyxHQUFHLHFMQUFxTCxNQUFNLHFMQUFxTCxJQUFJLHFMQUFxTCxNQUFNLHFMQUFxTCxJQUFJLHFMQUFxTCxNQUFNLHFMQUFxTCxJQUFJLHFMQUFxTCxNQUFNLHFMQUFxTCxHQUFHLHNMQUFzTCwyQkFBMkIsR0FBRyxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsR0FBRyxzTEFBc0wsa0NBQWtDLGtCQUFrQix5Q0FBeUMsV0FBVyxrQkFBa0IsU0FBUyxRQUFRLGlCQUFpQixrQkFBa0IsVUFBVSxXQUFXLGtCQUFrQix1REFBdUQsK0NBQStDLG1DQUFtQyxHQUFHLHlNQUF5TSxNQUFNLHlNQUF5TSxJQUFJLHlNQUF5TSxNQUFNLHlNQUF5TSxJQUFJLHlNQUF5TSxNQUFNLHlNQUF5TSxJQUFJLHlNQUF5TSxNQUFNLHlNQUF5TSxHQUFHLDBNQUEwTSwyQkFBMkIsR0FBRyx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sR0FBRywwTUFBME0sa0NBQWtDLGtCQUFrQixzQkFBc0Isc0RBQXNELDhDQUE4Qyx5Q0FBeUMsV0FBVyxrQkFBa0IsY0FBYyxNQUFNLFNBQVMsZ0JBQWdCLGlCQUFpQixVQUFVLFdBQVcsc0JBQXNCLGtCQUFrQixpQ0FBaUMsc0JBQXNCLCtCQUErQix3REFBd0QsZ0RBQWdELG9DQUFvQyxHQUFHLDRCQUE0QixvQkFBb0IsR0FBRyxnQ0FBZ0MseUJBQXlCLDRCQUE0QixHQUFHLDRCQUE0QixvQkFBb0IsR0FBRyxnQ0FBZ0MseUJBQXlCLDZDQUE2QyxXQUFXLGtCQUFrQiwrREFBK0QscUJBQXFCLGNBQWMsV0FBVyxZQUFZLGVBQWUsaUJBQWlCLGtCQUFrQix5Q0FBeUMsV0FBVyxlQUFlLGtCQUFrQixlQUFlLE9BQU8sZUFBZSxxQkFBcUIsU0FBUyw2QkFBNkIsaUJBQWlCLGNBQWMsS0FBSyxjQUFjLDZCQUE2QixTQUFTLGdCQUFnQixrQkFBa0IsbUJBQW1CLHNDQUFzQyxZQUFZLEtBQUssY0FBYyxLQUFLLGlCQUFpQiw4QkFBOEIsUUFBUSxXQUFXLEtBQUssV0FBVyxnR0FBZ0csSUFBSSxpQkFBaUIsUUFBUSxZQUFZLFdBQVcsU0FBUyw4TUFBOE0sZUFBZSxXQUFXLGtCQUFrQiw4Q0FBOEMsZ0JBQWdCLHlDQUF5QyxXQUFXLGFBQWEsc0VBQXNFLEVBQUUseUJBQXlCLG9CQUFvQiwyQkFBMkIsYUFBYSxrSUFBa0ksdUNBQXVDLG9FQUFvRSxhQUFhLG9JQUFvSSx1Q0FBdUMsa0RBQWtELHFCQUFxQixpQkFBaUIsZ0JBQWdCLFlBQVksV0FBVyxLQUFLLHFCQUFxQixNQUFNLFNBQVMsWUFBWSxpQkFBaUIsMkJBQTJCLEtBQUssaUJBQWlCLGtDQUFrQyxLQUFLLGlCQUFpQixpQkFBaUIsNEJBQTRCLFNBQVMsMEJBQTBCLGNBQWMsaUJBQWlCLEtBQUssV0FBVyxLQUFLLDBDQUEwQywyQkFBMkIscUNBQXFDLGVBQWUsRUFBRSxTQUFTLGdCQUFnQiwwQkFBMEIsZ0lBQWdJLEtBQUssK0dBQStHLGtCQUFrQixjQUFjLDRCQUE0QixtQkFBbUIsb0JBQW9CLGNBQWMsc0NBQXNDLGtDQUFrQyxnQkFBZ0IsVUFBVSxnQkFBZ0IsVUFBVSwwREFBMEQsMENBQTBDLE1BQU0sd0JBQXdCLE1BQU0sc0VBQXNFLE9BQU8sVUFBVSxvQkFBb0IsaUJBQWlCLDRDQUE0QyxLQUFLLGdEQUFnRCw0RUFBNEUsZ0JBQWdCLG9DQUFvQyw4SEFBOEgsMEdBQTBHLEtBQUssS0FBSyxhQUFhLDZCQUE2QiwyQ0FBMkMsUUFBUSxlQUFlLE1BQU0sa0JBQWtCLDREQUE0RCxnQkFBZ0Isb0VBQW9FLGlCQUFpQiwrREFBK0Qsa0JBQWtCLHdCQUF3QixPQUFPLDBHQUEwRyxXQUFXLDBCQUEwQixpQkFBaUIsV0FBVyxLQUFLLHFCQUFxQixtQkFBbUIsTUFBTSxXQUFXLE9BQU8sWUFBWSxXQUFXLEtBQUssV0FBVyxlQUFlLFlBQVksaUJBQWlCLGlCQUFpQixtQkFBbUIsaUJBQWlCLFNBQVMscUJBQXFCLDRDQUE0QyxHQUFHLGlCQUFpQixXQUFXLHNDQUFzQyxTQUFTLEVBQUUsK0JBQStCLEdBQUcsRTs7Ozs7Ozs7Ozs7OztBQ0EzOGtCO0FBQ0E7O0FBRUEsSUFBSUEsR0FBSixDQUFRO0FBQ0pDLFFBQUksVUFEQTs7QUFHSkMsVUFBTTtBQUNGQyw0QkFBb0IsRUFEbEI7QUFFRkMsa0JBQVU7QUFGUixLQUhGOztBQVFKQyxZQUFRLElBQUlDLFNBQUosQ0FBYztBQUNsQkMsZ0JBQVEsQ0FDSjtBQUNJQyxrQkFBTSxRQURWO0FBRUlDLGtCQUFNLE1BRlY7QUFHSUMsdUJBQVcsbUJBQUFDLENBQVEsR0FBUjtBQUhmLFNBREksRUFNSjtBQUNJSCxrQkFBTSxTQURWO0FBRUlDLGtCQUFNLFdBRlY7QUFHSUMsdUJBQVcsbUJBQUFDLENBQVEsR0FBUjtBQUhmLFNBTkk7QUFEVSxLQUFkLENBUko7O0FBdUJKQyxXQUFPO0FBQ0hULDBCQURHLDhCQUNnQlUsR0FEaEIsRUFDcUI7QUFDcEJDLHVCQUFXLFlBQU07QUFDYkMsa0JBQUUsZUFBRixFQUFtQkMsVUFBbkIsQ0FBOEI7QUFDMUJDLDRCQUFRO0FBRGtCLGlCQUE5QjtBQUdILGFBSkQsRUFJRyxDQUpIO0FBS0g7QUFQRSxLQXZCSDs7QUFpQ0pDLGFBQVM7QUFDTEMseUJBREssK0JBQ2U7QUFBQTs7QUFDaEJDLHVCQUFXQyxHQUFYLENBQWUsV0FBZixFQUNLQyxJQURMLENBQ1Usa0JBQVU7QUFDWkMsd0JBQVFDLEdBQVIsQ0FBWUMsTUFBWjtBQUNBLHNCQUFLdEIsa0JBQUwsR0FBMEJzQixPQUFPdkIsSUFBakM7QUFDSCxhQUpMO0FBS0gsU0FQSTtBQVNMd0Isd0JBVEssNEJBU1l4QixJQVRaLEVBU2tCO0FBQ25CLGlCQUFLRSxRQUFMLEdBQWdCRixJQUFoQjs7QUFFQSxpQkFBS3lCLE9BQUwsQ0FBYUMsSUFBYixDQUFrQjtBQUNkbkIsc0JBQU0sTUFEUTtBQUVkb0Isd0JBQVE7QUFDSkMsd0JBQUk1QixLQUFLNEI7QUFETDtBQUZNLGFBQWxCO0FBTUgsU0FsQkk7QUFvQkxDLDRCQXBCSyxnQ0FvQmdCN0IsSUFwQmhCLEVBb0JzQjtBQUN2QixpQkFBS0UsUUFBTCxHQUFnQkYsSUFBaEI7O0FBRUEsaUJBQUt5QixPQUFMLENBQWFDLElBQWIsQ0FBa0I7QUFDZG5CLHNCQUFNLFdBRFE7QUFFZG9CLHdCQUFRO0FBQ0pDLHdCQUFJNUIsS0FBSzRCO0FBREw7QUFGTSxhQUFsQjtBQU1ILFNBN0JJO0FBK0JMRSxrQkEvQkssd0JBK0JRO0FBQ1RDLGtCQUFNQyxJQUFOLENBQVcsa0JBQVg7QUFDSDtBQWpDSSxLQWpDTDs7QUFxRUpDLGdCQUFZO0FBQ1JDLG9CQUFBLGtFQURRO0FBRVJDLHVCQUFBLHFFQUFBQTtBQUZRLEtBckVSOztBQTBFSkMsV0ExRUkscUJBMEVNO0FBQ04sYUFBS25CLGlCQUFMOztBQUVBYyxjQUFNTSxNQUFOLENBQWEsc0JBQWIsRUFBcUMsS0FBS2IsZ0JBQTFDO0FBQ0FPLGNBQU1NLE1BQU4sQ0FBYSxvQkFBYixFQUFtQyxLQUFLUixvQkFBeEM7QUFDQUUsY0FBTU0sTUFBTixDQUFhLGVBQWIsRUFBOEIsS0FBS3BCLGlCQUFuQztBQUNBYyxjQUFNTSxNQUFOLENBQWEseUJBQWIsRUFBd0MsS0FBS3BCLGlCQUE3QztBQUNBYyxjQUFNTSxNQUFOLENBQWEsa0JBQWIsRUFBaUMsS0FBS3BCLGlCQUF0QztBQUNIO0FBbEZHLENBQVIsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzZCQTs7QUFFQTswQkFFQTs7a0JBRUE7c0JBQ0E7d0JBQ0E7eUJBQ0E7d0JBQ0E7c0JBQ0E7MkJBRUE7QUFSQTtBQVVBOzs7OzBDQUVBOytDQUVBOzt3QkFDQTs0QkFDQTs4QkFDQTsrQkFDQTs4QkFDQTs0QkFDQTtpQ0FFQTs7aUJBQ0E7QUFHQTtBQWZBOzs7MENBaUJBO2lCQUNBO0FBRUE7O0FBQ0E7O3NFQUNBLGdEQUNBO29DQUVBOzt1Q0FDQTtxQ0FFQTs7NkNBQ0E7a0RBQ0E7O2dDQUdBO0FBRkE7K0JBSUE7O3NCQUVBOzswREFDQTtBQUNBO0FBRUE7O0FBQ0E7OzJEQUVBOzs7O2lDQUdBOzJCQUVBO0FBSEE7QUFEQSxzQ0FLQTt1Q0FFQTs7eUNBQ0E7OENBQ0E7d0NBQ0E7QUFFQTs7eURBQ0E7cURBRUE7OzRDQUNBO29EQUNBOytCQUNBO0FBRUE7OzJDQUNBO29EQUNBO2lGQUNBOytCQUNBO21HQUNBO0FBRUE7O3lEQUNBO29DQUNBO3VCQUNBO3VEQUNBO0FBQ0E7QUFDQTtBQUVBO2dEQUNBOzttQ0FFQTtBQURBLHNDQUVBOzJCQUNBO0FBQ0E7QUFFQTs7QUFDQTs7b0RBQ0E7QUFDQTtBQUVBOzs7NkNBRUE7MkJBR0E7QUFKQTs7bUNBS0E7cUVBRUE7O2lEQUNBOzBCQUVBOzs7bUNBRUE7MkJBQ0E7QUFGQSxzQ0FHQTt3Q0FDQTt5REFDQTsyQkFDQTtBQUVBOzsyQkFDQTtBQUVBOzs4QkFDQTtBQUVBO2dDQUNBOzhDQUNBO0FBRUE7cUNBQ0E7cURBQ0E7QUFFQTtrREFDQTttQ0FDQTtxRUFDQTswQkFDQTtBQUVBO2tDQUNBOzRFQUNBOzZEQUNBO3VDQUNBO3lCQUNBO0FBQ0E7bUJBQ0E7QUFFQTs7QUFDQTs7O29DQUVBO0FBREE7OEJBRUE7O0FBRUE7O0FBQ0E7O3VFQUNBOzhCQUNBOztBQUVBO2tDQUNBO3dCQUNBO0FBRUE7c0NBQ0E7d0JBQ0E7QUFFQTs7QUFDQTs7O29DQUVBO0FBREE7OEJBRUE7O0FBRUE7b0NBQ0E7d0JBQ0E7QUFFQTs7QUFDQTs7NkRBQ0E7OEJBQ0E7O0FBR0E7QUFqS0E7OztzQ0FtS0E7MERBQ0E7QUFFQTt3Q0FDQTsrQ0FDQTtBQUVBOzhDQUNBO29FQUNBO3VCQUNBOzhEQUNBO3VCQUNBO0FBRUE7O21CQUNBO0FBR0E7QUFuQkE7OztBQXVCQTtBQUhBOztnQ0FJQTthQUNBO0FBRUE7Z0NBQ0E7O3NCQUVBO3FCQUVBOzs4Q0FDQTtzQ0FDQTttQkFFQTs7aURBQ0E7dUNBQ0E7dUNBQ0E7NEJBRUE7OzhFQUNBO2dFQUNBOzZFQUNBO2lEQUNBO3NFQUNBO2dEQUNBO2tFQUNBO3VCQUNBO3FFQUNBO0FBRUE7OzBDQUNBOzswREFHQTtBQUZBO3VCQUdBOzREQUNBO0FBRUE7O3lCQUNBO21CQUVBO0FBbENBO0FBbUNBO0FBL1BBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWEE7O0FBRUE7MEJBRUE7O3NCQUVBO3NCQUNBO3dCQUNBO3lCQUNBO3NCQUNBOzJCQUVBO0FBUEE7QUFTQTs7OzswQ0FFQTtzREFFQTs7NEJBQ0E7NEJBQ0E7OEJBQ0E7K0JBQ0E7NEJBQ0E7aUNBRUE7O2lCQUNBO0FBR0E7QUFkQTs7OzBDQWdCQTtpQkFDQTtBQUVBOztBQUNBOzsrREFDQSwyQkFDQTt3Q0FFQTs7dUNBQ0E7cUNBRUE7OzZDQUNBO2tEQUNBOztnQ0FHQTtBQUZBOytCQUlBOztzQkFFQTs7c0JBRUE7O3dEQUNBO0FBQ0E7QUFFQTs7QUFDQTs7MkRBRUE7Ozs7aUNBR0E7MkJBRUE7QUFIQTtBQURBLHNDQUtBO3VDQUVBOzt5Q0FDQTs4Q0FDQTt3Q0FDQTtBQUVBOzt5REFDQTtxREFFQTs7MkNBQ0E7b0RBQ0E7aUZBQ0E7K0JBQ0E7bUdBQ0E7QUFFQTs7eURBQ0E7b0NBQ0E7dUJBQ0E7dURBQ0E7QUFDQTtBQUNBO0FBRUE7Z0RBQ0E7OzRDQUVBO0FBREEsc0NBRUE7MkJBQ0E7QUFDQTtBQUVBOzhCQUNBO2tDQUNBO0FBQ0E7QUFFQTs7OzZDQUVBOzJCQUdBO0FBSkE7O21DQUtBO3FFQUVBOztpREFDQTswQkFFQTs7OzRDQUVBOzJCQUNBO0FBRkE7a0NBSUE7Ozs4QkFDQTtBQUVBO2dDQUNBOzhDQUNBO0FBRUE7cUNBQ0E7cURBQ0E7QUFFQTtrREFDQTttQ0FDQTtxRUFDQTswQkFDQTtBQUVBO2tDQUNBOzZFQUNBOzhEQUNBO3VDQUNBO3lCQUNBO0FBQ0E7bUJBQ0E7QUFHQTtBQXJIQTs7O0FBeUhBO0FBSEE7O2dDQUlBO2FBQ0E7QUFDQTtBQXZKQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1BBO1lBR0E7OztvQ0FFQTs2Q0FDQTs7MEJBRUE7O21EQUlBO0FBSEE7QUFGQTtBQU9BOzs7c0JBRUE7O3dEQUlBO0FBSEE7QUFGQTtBQU9BO29DQUNBO2dFQUNBO3VCQUNBO0FBRUE7O2lDQUNBO0FBRUE7a0NBQ0E7bURBQ0E7QUFFQTtzQ0FDQTs2Q0FDQTtrR0FDQTtBQUVBOztrRkFDQTtBQUVBO29DQUNBO21EQUNBO0FBRUE7QUExQ0E7QUFIQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTEE7c0JBR0E7OzBCQUNBOztzQkFFQTtvQkFDQTttQkFDQTs0QkFDQTtxQkFFQTtBQU5BO0FBUUE7Ozs7cUNBRUE7aURBQ0E7NkJBQ0E7QUFDQTtBQUVBO21DQUNBO21DQUNBO29DQUNBOzBCQUNBO0FBR0E7QUFiQTs7OzJDQWVBOzZHQUNBO2tDQUVBOzs7QUFDQTs7b0VBQ0EsK0JBQ0E7c0VBQ0E7aURBQ0E7QUFDQTtBQUNBOzhCQUNBO0FBQ0E7QUFFQTtzQ0FDQTsrQ0FFQTs7aUJBQ0E7QUFFQTtzQ0FDQTt3QkFDQTtBQUVBOzRDQUNBO3VDQUNBO3dDQUNBO3lDQUNBO2dDQUNBOzs2Q0FFQTs2QkFHQTtBQUpBOzs2QkFLQTttREFFQTs7aURBQ0E7eURBQ0E7QUFFQTs7dUJBQ0E7QUFFQTtrQ0FDQTs2QkFDQTtrQ0FDQTttREFFQTs7MEJBQ0E7eUJBRUE7OzJCQUNBO21CQUVBOztnQkFDQTtBQUVBOztBQUNBOztrQ0FFQTs7eURBQ0E7bUNBRUE7O29CQUNBO2tDQUVBOzt1QkFDQTtBQUVBOztBQUNBOzZDQUNBO2lDQUVBOztBQUNBO3lFQUNBO3lCQUNBO0FBQ0E7bUJBQ0E7QUFHQTtBQW5GQTs7Z0NBb0ZBO2FBQ0E7QUFFQTtnQ0FDQTsrQkFDQTtnQ0FDQTtBQUVBOzs7a0JBRUE7b0JBRUE7QUFIQTtBQUlBO0FBNUhBLEc7Ozs7Ozs7QUNqQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUE0RztBQUM1RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUE0RztBQUM1RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUE0RztBQUM1RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUE0RztBQUM1RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsMkVBQTJFLGFBQWE7QUFDeEY7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUNwRUEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLDJFQUEyRSxhQUFhO0FBQ3hGLCtCQUErQixhQUFhO0FBQzVDO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3RGQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3BEQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9wYWdlcy9tZXNzYWdlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMjY3KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBiMjBhMmEwYjI5NTZhOWQ2YzBkMSIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNSA2IDcgOCA5IDEwIDExIDEyIiwiIWZ1bmN0aW9uKHAseCl7XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHMmJlwib2JqZWN0XCI9PXR5cGVvZiBtb2R1bGU/bW9kdWxlLmV4cG9ydHM9eCgpOlwiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoW10seCk6XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHM/ZXhwb3J0cy5WdWVJbmZpbml0ZUxvYWRpbmc9eCgpOnAuVnVlSW5maW5pdGVMb2FkaW5nPXgoKX0odGhpcyxmdW5jdGlvbigpe3JldHVybiBmdW5jdGlvbihwKXtmdW5jdGlvbiB4KGEpe2lmKHRbYV0pcmV0dXJuIHRbYV0uZXhwb3J0czt2YXIgZT10W2FdPXtleHBvcnRzOnt9LGlkOmEsbG9hZGVkOiExfTtyZXR1cm4gcFthXS5jYWxsKGUuZXhwb3J0cyxlLGUuZXhwb3J0cyx4KSxlLmxvYWRlZD0hMCxlLmV4cG9ydHN9dmFyIHQ9e307cmV0dXJuIHgubT1wLHguYz10LHgucD1cIi9cIix4KDApfShbZnVuY3Rpb24ocCx4LHQpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGEocCl7cmV0dXJuIHAmJnAuX19lc01vZHVsZT9wOntkZWZhdWx0OnB9fU9iamVjdC5kZWZpbmVQcm9wZXJ0eSh4LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pO3ZhciBlPXQoNCksbz1hKGUpO3guZGVmYXVsdD1vLmRlZmF1bHQsXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHdpbmRvdyYmd2luZG93LlZ1ZSYmd2luZG93LlZ1ZS5jb21wb25lbnQoXCJpbmZpbml0ZS1sb2FkaW5nXCIsby5kZWZhdWx0KX0sZnVuY3Rpb24ocCx4KXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiB0KHApe3JldHVyblwiQk9EWVwiPT09cC50YWdOYW1lP3dpbmRvdzpbXCJzY3JvbGxcIixcImF1dG9cIl0uaW5kZXhPZihnZXRDb21wdXRlZFN0eWxlKHApLm92ZXJmbG93WSk+LTE/cDpwLmhhc0F0dHJpYnV0ZShcImluZmluaXRlLXdyYXBwZXJcIil8fHAuaGFzQXR0cmlidXRlKFwiZGF0YS1pbmZpbml0ZS13cmFwcGVyXCIpP3A6dChwLnBhcmVudE5vZGUpfWZ1bmN0aW9uIGEocCx4LHQpe3ZhciBhPXZvaWQgMDtpZihcInRvcFwiPT09dClhPWlzTmFOKHAuc2Nyb2xsVG9wKT9wLnBhZ2VZT2Zmc2V0OnAuc2Nyb2xsVG9wO2Vsc2V7dmFyIGU9eC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3Asbz1wPT09d2luZG93P3dpbmRvdy5pbm5lckhlaWdodDpwLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmJvdHRvbTthPWUtb31yZXR1cm4gYX1PYmplY3QuZGVmaW5lUHJvcGVydHkoeCxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KTt2YXIgZT17YnViYmxlczpcImxvYWRpbmctYnViYmxlc1wiLGNpcmNsZXM6XCJsb2FkaW5nLWNpcmNsZXNcIixkZWZhdWx0OlwibG9hZGluZy1kZWZhdWx0XCIsc3BpcmFsOlwibG9hZGluZy1zcGlyYWxcIix3YXZlRG90czpcImxvYWRpbmctd2F2ZS1kb3RzXCJ9O3guZGVmYXVsdD17ZGF0YTpmdW5jdGlvbigpe3JldHVybntzY3JvbGxQYXJlbnQ6bnVsbCxzY3JvbGxIYW5kbGVyOm51bGwsaXNMb2FkaW5nOiExLGlzQ29tcGxldGU6ITEsaXNGaXJzdExvYWQ6ITB9fSxjb21wdXRlZDp7c3Bpbm5lclR5cGU6ZnVuY3Rpb24oKXtyZXR1cm4gZVt0aGlzLnNwaW5uZXJdfHxlLmRlZmF1bHR9fSxwcm9wczp7ZGlzdGFuY2U6e3R5cGU6TnVtYmVyLGRlZmF1bHQ6MTAwfSxvbkluZmluaXRlOkZ1bmN0aW9uLHNwaW5uZXI6U3RyaW5nLGRpcmVjdGlvbjp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcImJvdHRvbVwifX0sbW91bnRlZDpmdW5jdGlvbigpe3ZhciBwPXRoaXM7dGhpcy5zY3JvbGxQYXJlbnQ9dCh0aGlzLiRlbCksdGhpcy5zY3JvbGxIYW5kbGVyPWZ1bmN0aW9uKCl7dGhpcy5pc0xvYWRpbmd8fHRoaXMuYXR0ZW1wdExvYWQoKX0uYmluZCh0aGlzKSxzZXRUaW1lb3V0KHRoaXMuc2Nyb2xsSGFuZGxlciwxKSx0aGlzLnNjcm9sbFBhcmVudC5hZGRFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsdGhpcy5zY3JvbGxIYW5kbGVyKSx0aGlzLiRvbihcIiRJbmZpbml0ZUxvYWRpbmc6bG9hZGVkXCIsZnVuY3Rpb24oKXtwLmlzRmlyc3RMb2FkPSExLHAuaXNMb2FkaW5nJiZwLiRuZXh0VGljayhwLmF0dGVtcHRMb2FkKX0pLHRoaXMuJG9uKFwiJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZVwiLGZ1bmN0aW9uKCl7cC5pc0xvYWRpbmc9ITEscC5pc0NvbXBsZXRlPSEwLHAuc2Nyb2xsUGFyZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIixwLnNjcm9sbEhhbmRsZXIpfSksdGhpcy4kb24oXCIkSW5maW5pdGVMb2FkaW5nOnJlc2V0XCIsZnVuY3Rpb24oKXtwLmlzTG9hZGluZz0hMSxwLmlzQ29tcGxldGU9ITEscC5pc0ZpcnN0TG9hZD0hMCxwLnNjcm9sbFBhcmVudC5hZGRFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIscC5zY3JvbGxIYW5kbGVyKSxzZXRUaW1lb3V0KHAuc2Nyb2xsSGFuZGxlciwxKX0pfSxkZWFjdGl2YXRlZDpmdW5jdGlvbigpe3RoaXMuaXNMb2FkaW5nPSExLHRoaXMuc2Nyb2xsUGFyZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIix0aGlzLnNjcm9sbEhhbmRsZXIpfSxhY3RpdmF0ZWQ6ZnVuY3Rpb24oKXt0aGlzLnNjcm9sbFBhcmVudC5hZGRFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsdGhpcy5zY3JvbGxIYW5kbGVyKX0sbWV0aG9kczp7YXR0ZW1wdExvYWQ6ZnVuY3Rpb24oKXt2YXIgcD1hKHRoaXMuc2Nyb2xsUGFyZW50LHRoaXMuJGVsLHRoaXMuZGlyZWN0aW9uKTshdGhpcy5pc0NvbXBsZXRlJiZwPD10aGlzLmRpc3RhbmNlPyh0aGlzLmlzTG9hZGluZz0hMCx0aGlzLm9uSW5maW5pdGUuY2FsbCgpKTp0aGlzLmlzTG9hZGluZz0hMX19LGRlc3Ryb3llZDpmdW5jdGlvbigpe3RoaXMuaXNDb21wbGV0ZXx8dGhpcy5zY3JvbGxQYXJlbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHRoaXMuc2Nyb2xsSGFuZGxlcil9fX0sZnVuY3Rpb24ocCx4LHQpe3g9cC5leHBvcnRzPXQoMykoKSx4LnB1c2goW3AuaWQsJy5sb2FkaW5nLXdhdmUtZG90c1tkYXRhLXYtNTA3OTNmMDJde3Bvc2l0aW9uOnJlbGF0aXZlfS5sb2FkaW5nLXdhdmUtZG90c1tkYXRhLXYtNTA3OTNmMDJdOmJlZm9yZXtjb250ZW50OlwiXCI7cG9zaXRpb246YWJzb2x1dGU7dG9wOjUwJTtsZWZ0OjUwJTttYXJnaW4tbGVmdDotNHB4O21hcmdpbi10b3A6LTRweDt3aWR0aDo4cHg7aGVpZ2h0OjhweDtiYWNrZ3JvdW5kLWNvbG9yOiNiYmI7Ym9yZGVyLXJhZGl1czo1MCU7LXdlYmtpdC1hbmltYXRpb246bGluZWFyIGxvYWRpbmctd2F2ZS1kb3RzIDIuOHMgaW5maW5pdGU7YW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLXdhdmUtZG90cyAyLjhzIGluZmluaXRlfUAtd2Via2l0LWtleWZyYW1lcyBsb2FkaW5nLXdhdmUtZG90c3swJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn01JXtib3gtc2hhZG93Oi0zMnB4IC00cHggMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTEwJXtib3gtc2hhZG93Oi0zMnB4IC02cHggMCAjOTk5LC0xNnB4IC00cHggMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTE1JXtib3gtc2hhZG93Oi0zMnB4IDJweCAwICNiYmIsLTE2cHggLTJweCAwICM5OTksMTZweCA0cHggMCAjYmJiLDMycHggNHB4IDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC00cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC00cHgpO2JhY2tncm91bmQtY29sb3I6I2JiYn0yMCV7Ym94LXNoYWRvdzotMzJweCA2cHggMCAjYmJiLC0xNnB4IDRweCAwICNiYmIsMTZweCAycHggMCAjYmJiLDMycHggNnB4IDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC02cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC02cHgpO2JhY2tncm91bmQtY29sb3I6Izk5OX0yNSV7Ym94LXNoYWRvdzotMzJweCAycHggMCAjYmJiLC0xNnB4IDJweCAwICNiYmIsMTZweCAtNHB4IDAgIzk5OSwzMnB4IC0ycHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTJweCk7YmFja2dyb3VuZC1jb2xvcjojYmJifTMwJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggLTJweCAwICNiYmIsMzJweCAtNnB4IDAgIzk5OTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTM1JXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAtMnB4IDAgI2JiYn00MCV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmJ9dG97Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmJ9fUBrZXlmcmFtZXMgbG9hZGluZy13YXZlLWRvdHN7MCV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmJ9NSV7Ym94LXNoYWRvdzotMzJweCAtNHB4IDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgwKX0xMCV7Ym94LXNoYWRvdzotMzJweCAtNnB4IDAgIzk5OSwtMTZweCAtNHB4IDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgwKX0xNSV7Ym94LXNoYWRvdzotMzJweCAycHggMCAjYmJiLC0xNnB4IC0ycHggMCAjOTk5LDE2cHggNHB4IDAgI2JiYiwzMnB4IDRweCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtNHB4KTtiYWNrZ3JvdW5kLWNvbG9yOiNiYmJ9MjAle2JveC1zaGFkb3c6LTMycHggNnB4IDAgI2JiYiwtMTZweCA0cHggMCAjYmJiLDE2cHggMnB4IDAgI2JiYiwzMnB4IDZweCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtNnB4KTtiYWNrZ3JvdW5kLWNvbG9yOiM5OTl9MjUle2JveC1zaGFkb3c6LTMycHggMnB4IDAgI2JiYiwtMTZweCAycHggMCAjYmJiLDE2cHggLTRweCAwICM5OTksMzJweCAtMnB4IDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC0ycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC0ycHgpO2JhY2tncm91bmQtY29sb3I6I2JiYn0zMCV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IC0ycHggMCAjYmJiLDMycHggLTZweCAwICM5OTk7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgwKX0zNSV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggLTJweCAwICNiYmJ9NDAle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJifXRve2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJifX0ubG9hZGluZy1jaXJjbGVzW2RhdGEtdi01MDc5M2YwMl17cG9zaXRpb246cmVsYXRpdmV9LmxvYWRpbmctY2lyY2xlc1tkYXRhLXYtNTA3OTNmMDJdOmJlZm9yZXtjb250ZW50OlwiXCI7cG9zaXRpb246YWJzb2x1dGU7bGVmdDo1MCU7dG9wOjUwJTttYXJnaW4tdG9wOi0yLjVweDttYXJnaW4tbGVmdDotMi41cHg7d2lkdGg6NXB4O2hlaWdodDo1cHg7Ym9yZGVyLXJhZGl1czo1MCU7LXdlYmtpdC1hbmltYXRpb246bGluZWFyIGxvYWRpbmctY2lyY2xlcyAuNzVzIGluZmluaXRlO2FuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1jaXJjbGVzIC43NXMgaW5maW5pdGV9QC13ZWJraXQta2V5ZnJhbWVzIGxvYWRpbmctY2lyY2xlc3swJXtib3gtc2hhZG93OjAgLTEycHggMCAjNTA1MDUwLDguNTJweCAtOC41MnB4IDAgIzY0NjQ2NCwxMnB4IDAgMCAjNzk3OTc5LDguNTJweCA4LjUycHggMCAjOGQ4ZDhkLDAgMTJweCAwICNhMmEyYTIsLTguNTJweCA4LjUycHggMCAjYjZiNmI2LC0xMnB4IDAgMCAjY2FjYWNhLC04LjUycHggLTguNTJweCAwICNkZmRmZGZ9MTIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2RmZGZkZiw4LjUycHggLTguNTJweCAwICM1MDUwNTAsMTJweCAwIDAgIzY0NjQ2NCw4LjUycHggOC41MnB4IDAgIzc5Nzk3OSwwIDEycHggMCAjOGQ4ZDhkLC04LjUycHggOC41MnB4IDAgI2EyYTJhMiwtMTJweCAwIDAgI2I2YjZiNiwtOC41MnB4IC04LjUycHggMCAjY2FjYWNhfTI1JXtib3gtc2hhZG93OjAgLTEycHggMCAjY2FjYWNhLDguNTJweCAtOC41MnB4IDAgI2RmZGZkZiwxMnB4IDAgMCAjNTA1MDUwLDguNTJweCA4LjUycHggMCAjNjQ2NDY0LDAgMTJweCAwICM3OTc5NzksLTguNTJweCA4LjUycHggMCAjOGQ4ZDhkLC0xMnB4IDAgMCAjYTJhMmEyLC04LjUycHggLTguNTJweCAwICNiNmI2YjZ9MzcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2I2YjZiNiw4LjUycHggLTguNTJweCAwICNjYWNhY2EsMTJweCAwIDAgI2RmZGZkZiw4LjUycHggOC41MnB4IDAgIzUwNTA1MCwwIDEycHggMCAjNjQ2NDY0LC04LjUycHggOC41MnB4IDAgIzc5Nzk3OSwtMTJweCAwIDAgIzhkOGQ4ZCwtOC41MnB4IC04LjUycHggMCAjYTJhMmEyfTUwJXtib3gtc2hhZG93OjAgLTEycHggMCAjYTJhMmEyLDguNTJweCAtOC41MnB4IDAgI2I2YjZiNiwxMnB4IDAgMCAjY2FjYWNhLDguNTJweCA4LjUycHggMCAjZGZkZmRmLDAgMTJweCAwICM1MDUwNTAsLTguNTJweCA4LjUycHggMCAjNjQ2NDY0LC0xMnB4IDAgMCAjNzk3OTc5LC04LjUycHggLTguNTJweCAwICM4ZDhkOGR9NjIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzhkOGQ4ZCw4LjUycHggLTguNTJweCAwICNhMmEyYTIsMTJweCAwIDAgI2I2YjZiNiw4LjUycHggOC41MnB4IDAgI2NhY2FjYSwwIDEycHggMCAjZGZkZmRmLC04LjUycHggOC41MnB4IDAgIzUwNTA1MCwtMTJweCAwIDAgIzY0NjQ2NCwtOC41MnB4IC04LjUycHggMCAjNzk3OTc5fTc1JXtib3gtc2hhZG93OjAgLTEycHggMCAjNzk3OTc5LDguNTJweCAtOC41MnB4IDAgIzhkOGQ4ZCwxMnB4IDAgMCAjYTJhMmEyLDguNTJweCA4LjUycHggMCAjYjZiNmI2LDAgMTJweCAwICNjYWNhY2EsLTguNTJweCA4LjUycHggMCAjZGZkZmRmLC0xMnB4IDAgMCAjNTA1MDUwLC04LjUycHggLTguNTJweCAwICM2NDY0NjR9ODcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzY0NjQ2NCw4LjUycHggLTguNTJweCAwICM3OTc5NzksMTJweCAwIDAgIzhkOGQ4ZCw4LjUycHggOC41MnB4IDAgI2EyYTJhMiwwIDEycHggMCAjYjZiNmI2LC04LjUycHggOC41MnB4IDAgI2NhY2FjYSwtMTJweCAwIDAgI2RmZGZkZiwtOC41MnB4IC04LjUycHggMCAjNTA1MDUwfXRve2JveC1zaGFkb3c6MCAtMTJweCAwICM1MDUwNTAsOC41MnB4IC04LjUycHggMCAjNjQ2NDY0LDEycHggMCAwICM3OTc5NzksOC41MnB4IDguNTJweCAwICM4ZDhkOGQsMCAxMnB4IDAgI2EyYTJhMiwtOC41MnB4IDguNTJweCAwICNiNmI2YjYsLTEycHggMCAwICNjYWNhY2EsLTguNTJweCAtOC41MnB4IDAgI2RmZGZkZn19QGtleWZyYW1lcyBsb2FkaW5nLWNpcmNsZXN7MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzUwNTA1MCw4LjUycHggLTguNTJweCAwICM2NDY0NjQsMTJweCAwIDAgIzc5Nzk3OSw4LjUycHggOC41MnB4IDAgIzhkOGQ4ZCwwIDEycHggMCAjYTJhMmEyLC04LjUycHggOC41MnB4IDAgI2I2YjZiNiwtMTJweCAwIDAgI2NhY2FjYSwtOC41MnB4IC04LjUycHggMCAjZGZkZmRmfTEyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwICNkZmRmZGYsOC41MnB4IC04LjUycHggMCAjNTA1MDUwLDEycHggMCAwICM2NDY0NjQsOC41MnB4IDguNTJweCAwICM3OTc5NzksMCAxMnB4IDAgIzhkOGQ4ZCwtOC41MnB4IDguNTJweCAwICNhMmEyYTIsLTEycHggMCAwICNiNmI2YjYsLTguNTJweCAtOC41MnB4IDAgI2NhY2FjYX0yNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2NhY2FjYSw4LjUycHggLTguNTJweCAwICNkZmRmZGYsMTJweCAwIDAgIzUwNTA1MCw4LjUycHggOC41MnB4IDAgIzY0NjQ2NCwwIDEycHggMCAjNzk3OTc5LC04LjUycHggOC41MnB4IDAgIzhkOGQ4ZCwtMTJweCAwIDAgI2EyYTJhMiwtOC41MnB4IC04LjUycHggMCAjYjZiNmI2fTM3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwICNiNmI2YjYsOC41MnB4IC04LjUycHggMCAjY2FjYWNhLDEycHggMCAwICNkZmRmZGYsOC41MnB4IDguNTJweCAwICM1MDUwNTAsMCAxMnB4IDAgIzY0NjQ2NCwtOC41MnB4IDguNTJweCAwICM3OTc5NzksLTEycHggMCAwICM4ZDhkOGQsLTguNTJweCAtOC41MnB4IDAgI2EyYTJhMn01MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2EyYTJhMiw4LjUycHggLTguNTJweCAwICNiNmI2YjYsMTJweCAwIDAgI2NhY2FjYSw4LjUycHggOC41MnB4IDAgI2RmZGZkZiwwIDEycHggMCAjNTA1MDUwLC04LjUycHggOC41MnB4IDAgIzY0NjQ2NCwtMTJweCAwIDAgIzc5Nzk3OSwtOC41MnB4IC04LjUycHggMCAjOGQ4ZDhkfTYyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwICM4ZDhkOGQsOC41MnB4IC04LjUycHggMCAjYTJhMmEyLDEycHggMCAwICNiNmI2YjYsOC41MnB4IDguNTJweCAwICNjYWNhY2EsMCAxMnB4IDAgI2RmZGZkZiwtOC41MnB4IDguNTJweCAwICM1MDUwNTAsLTEycHggMCAwICM2NDY0NjQsLTguNTJweCAtOC41MnB4IDAgIzc5Nzk3OX03NSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzc5Nzk3OSw4LjUycHggLTguNTJweCAwICM4ZDhkOGQsMTJweCAwIDAgI2EyYTJhMiw4LjUycHggOC41MnB4IDAgI2I2YjZiNiwwIDEycHggMCAjY2FjYWNhLC04LjUycHggOC41MnB4IDAgI2RmZGZkZiwtMTJweCAwIDAgIzUwNTA1MCwtOC41MnB4IC04LjUycHggMCAjNjQ2NDY0fTg3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwICM2NDY0NjQsOC41MnB4IC04LjUycHggMCAjNzk3OTc5LDEycHggMCAwICM4ZDhkOGQsOC41MnB4IDguNTJweCAwICNhMmEyYTIsMCAxMnB4IDAgI2I2YjZiNiwtOC41MnB4IDguNTJweCAwICNjYWNhY2EsLTEycHggMCAwICNkZmRmZGYsLTguNTJweCAtOC41MnB4IDAgIzUwNTA1MH10b3tib3gtc2hhZG93OjAgLTEycHggMCAjNTA1MDUwLDguNTJweCAtOC41MnB4IDAgIzY0NjQ2NCwxMnB4IDAgMCAjNzk3OTc5LDguNTJweCA4LjUycHggMCAjOGQ4ZDhkLDAgMTJweCAwICNhMmEyYTIsLTguNTJweCA4LjUycHggMCAjYjZiNmI2LC0xMnB4IDAgMCAjY2FjYWNhLC04LjUycHggLTguNTJweCAwICNkZmRmZGZ9fS5sb2FkaW5nLWJ1YmJsZXNbZGF0YS12LTUwNzkzZjAyXXtwb3NpdGlvbjpyZWxhdGl2ZX0ubG9hZGluZy1idWJibGVzW2RhdGEtdi01MDc5M2YwMl06YmVmb3Jle2NvbnRlbnQ6XCJcIjtwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjUwJTt0b3A6NTAlO21hcmdpbi10b3A6LS41cHg7bWFyZ2luLWxlZnQ6LS41cHg7d2lkdGg6MXB4O2hlaWdodDoxcHg7Ym9yZGVyLXJhZGl1czo1MCU7LXdlYmtpdC1hbmltYXRpb246bGluZWFyIGxvYWRpbmctYnViYmxlcyAuODVzIGluZmluaXRlO2FuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1idWJibGVzIC44NXMgaW5maW5pdGV9QC13ZWJraXQta2V5ZnJhbWVzIGxvYWRpbmctYnViYmxlc3swJXtib3gtc2hhZG93OjAgLTEycHggMCAuNHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuOHB4ICM2NjYsMTJweCAwIDAgMS4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwwIDEycHggMCAycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuNHB4ICM2NjYsLTEycHggMCAwIDIuOHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2Nn0xMi41JXtib3gtc2hhZG93OjAgLTEycHggMCAzLjJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgLjRweCAjNjY2LDEycHggMCAwIC44cHggIzY2Niw4LjUycHggOC41MnB4IDAgMS4ycHggIzY2NiwwIDEycHggMCAxLjZweCAjNjY2LC04LjUycHggOC41MnB4IDAgMnB4ICM2NjYsLTEycHggMCAwIDIuNHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMi44cHggIzY2Nn0yNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMi44cHggIzY2Niw4LjUycHggLTguNTJweCAwIDMuMnB4ICM2NjYsMTJweCAwIDAgLjRweCAjNjY2LDguNTJweCA4LjUycHggMCAuOHB4ICM2NjYsMCAxMnB4IDAgMS4ycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDEuNnB4ICM2NjYsLTEycHggMCAwIDJweCAjNjY2LC04LjUycHggLTguNTJweCAwIDIuNHB4ICM2NjZ9MzcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMi40cHggIzY2Niw4LjUycHggLTguNTJweCAwIDIuOHB4ICM2NjYsMTJweCAwIDAgMy4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgLjRweCAjNjY2LDAgMTJweCAwIC44cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDEuMnB4ICM2NjYsLTEycHggMCAwIDEuNnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMnB4ICM2NjZ9NTAle2JveC1zaGFkb3c6MCAtMTJweCAwIDJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMi40cHggIzY2NiwxMnB4IDAgMCAyLjhweCAjNjY2LDguNTJweCA4LjUycHggMCAzLjJweCAjNjY2LDAgMTJweCAwIC40cHggIzY2NiwtOC41MnB4IDguNTJweCAwIC44cHggIzY2NiwtMTJweCAwIDAgMS4ycHggIzY2NiwtOC41MnB4IC04LjUycHggMCAxLjZweCAjNjY2fTYyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDEuNnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAycHggIzY2NiwxMnB4IDAgMCAyLjRweCAjNjY2LDguNTJweCA4LjUycHggMCAyLjhweCAjNjY2LDAgMTJweCAwIDMuMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAuNHB4ICM2NjYsLTEycHggMCAwIC44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAxLjJweCAjNjY2fTc1JXtib3gtc2hhZG93OjAgLTEycHggMCAxLjJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMS42cHggIzY2NiwxMnB4IDAgMCAycHggIzY2Niw4LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwwIDEycHggMCAyLjhweCAjNjY2LC04LjUycHggOC41MnB4IDAgMy4ycHggIzY2NiwtMTJweCAwIDAgLjRweCAjNjY2LC04LjUycHggLTguNTJweCAwIC44cHggIzY2Nn04Ny41JXtib3gtc2hhZG93OjAgLTEycHggMCAuOHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAxLjJweCAjNjY2LDEycHggMCAwIDEuNnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDJweCAjNjY2LDAgMTJweCAwIDIuNHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAyLjhweCAjNjY2LC0xMnB4IDAgMCAzLjJweCAjNjY2LC04LjUycHggLTguNTJweCAwIC40cHggIzY2Nn10b3tib3gtc2hhZG93OjAgLTEycHggMCAuNHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuOHB4ICM2NjYsMTJweCAwIDAgMS4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwwIDEycHggMCAycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuNHB4ICM2NjYsLTEycHggMCAwIDIuOHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2Nn19QGtleWZyYW1lcyBsb2FkaW5nLWJ1YmJsZXN7MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgLjRweCAjNjY2LDguNTJweCAtOC41MnB4IDAgLjhweCAjNjY2LDEycHggMCAwIDEuMnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDEuNnB4ICM2NjYsMCAxMnB4IDAgMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAyLjRweCAjNjY2LC0xMnB4IDAgMCAyLjhweCAjNjY2LC04LjUycHggLTguNTJweCAwIDMuMnB4ICM2NjZ9MTIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMy4ycHggIzY2Niw4LjUycHggLTguNTJweCAwIC40cHggIzY2NiwxMnB4IDAgMCAuOHB4ICM2NjYsOC41MnB4IDguNTJweCAwIDEuMnB4ICM2NjYsMCAxMnB4IDAgMS42cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDJweCAjNjY2LC0xMnB4IDAgMCAyLjRweCAjNjY2LC04LjUycHggLTguNTJweCAwIDIuOHB4ICM2NjZ9MjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDIuOHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAzLjJweCAjNjY2LDEycHggMCAwIC40cHggIzY2Niw4LjUycHggOC41MnB4IDAgLjhweCAjNjY2LDAgMTJweCAwIDEuMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAxLjZweCAjNjY2LC0xMnB4IDAgMCAycHggIzY2NiwtOC41MnB4IC04LjUycHggMCAyLjRweCAjNjY2fTM3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDIuNHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAyLjhweCAjNjY2LDEycHggMCAwIDMuMnB4ICM2NjYsOC41MnB4IDguNTJweCAwIC40cHggIzY2NiwwIDEycHggMCAuOHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAxLjJweCAjNjY2LC0xMnB4IDAgMCAxLjZweCAjNjY2LC04LjUycHggLTguNTJweCAwIDJweCAjNjY2fTUwJXtib3gtc2hhZG93OjAgLTEycHggMCAycHggIzY2Niw4LjUycHggLTguNTJweCAwIDIuNHB4ICM2NjYsMTJweCAwIDAgMi44cHggIzY2Niw4LjUycHggOC41MnB4IDAgMy4ycHggIzY2NiwwIDEycHggMCAuNHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAuOHB4ICM2NjYsLTEycHggMCAwIDEuMnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMS42cHggIzY2Nn02Mi41JXtib3gtc2hhZG93OjAgLTEycHggMCAxLjZweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMnB4ICM2NjYsMTJweCAwIDAgMi40cHggIzY2Niw4LjUycHggOC41MnB4IDAgMi44cHggIzY2NiwwIDEycHggMCAzLjJweCAjNjY2LC04LjUycHggOC41MnB4IDAgLjRweCAjNjY2LC0xMnB4IDAgMCAuOHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMS4ycHggIzY2Nn03NSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMS4ycHggIzY2Niw4LjUycHggLTguNTJweCAwIDEuNnB4ICM2NjYsMTJweCAwIDAgMnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDIuNHB4ICM2NjYsMCAxMnB4IDAgMi44cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDMuMnB4ICM2NjYsLTEycHggMCAwIC40cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAuOHB4ICM2NjZ9ODcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgLjhweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMS4ycHggIzY2NiwxMnB4IDAgMCAxLjZweCAjNjY2LDguNTJweCA4LjUycHggMCAycHggIzY2NiwwIDEycHggMCAyLjRweCAjNjY2LC04LjUycHggOC41MnB4IDAgMi44cHggIzY2NiwtMTJweCAwIDAgMy4ycHggIzY2NiwtOC41MnB4IC04LjUycHggMCAuNHB4ICM2NjZ9dG97Ym94LXNoYWRvdzowIC0xMnB4IDAgLjRweCAjNjY2LDguNTJweCAtOC41MnB4IDAgLjhweCAjNjY2LDEycHggMCAwIDEuMnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDEuNnB4ICM2NjYsMCAxMnB4IDAgMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAyLjRweCAjNjY2LC0xMnB4IDAgMCAyLjhweCAjNjY2LC04LjUycHggLTguNTJweCAwIDMuMnB4ICM2NjZ9fS5sb2FkaW5nLWRlZmF1bHRbZGF0YS12LTUwNzkzZjAyXXtwb3NpdGlvbjpyZWxhdGl2ZTtib3JkZXI6MXB4IHNvbGlkICM5OTk7LXdlYmtpdC1hbmltYXRpb246ZWFzZSBsb2FkaW5nLXJvdGF0aW5nIDEuNXMgaW5maW5pdGU7YW5pbWF0aW9uOmVhc2UgbG9hZGluZy1yb3RhdGluZyAxLjVzIGluZmluaXRlfS5sb2FkaW5nLWRlZmF1bHRbZGF0YS12LTUwNzkzZjAyXTpiZWZvcmV7Y29udGVudDpcIlwiO3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6YmxvY2s7dG9wOjA7bGVmdDo1MCU7bWFyZ2luLXRvcDotM3B4O21hcmdpbi1sZWZ0Oi0zcHg7d2lkdGg6NnB4O2hlaWdodDo2cHg7YmFja2dyb3VuZC1jb2xvcjojOTk5O2JvcmRlci1yYWRpdXM6NTAlfS5sb2FkaW5nLXNwaXJhbFtkYXRhLXYtNTA3OTNmMDJde2JvcmRlcjoycHggc29saWQgIzc3Nztib3JkZXItcmlnaHQtY29sb3I6dHJhbnNwYXJlbnQ7LXdlYmtpdC1hbmltYXRpb246bGluZWFyIGxvYWRpbmctcm90YXRpbmcgLjg1cyBpbmZpbml0ZTthbmltYXRpb246bGluZWFyIGxvYWRpbmctcm90YXRpbmcgLjg1cyBpbmZpbml0ZX1ALXdlYmtpdC1rZXlmcmFtZXMgbG9hZGluZy1yb3RhdGluZ3swJXstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMCk7dHJhbnNmb3JtOnJvdGF0ZSgwKX10b3std2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMXR1cm4pO3RyYW5zZm9ybTpyb3RhdGUoMXR1cm4pfX1Aa2V5ZnJhbWVzIGxvYWRpbmctcm90YXRpbmd7MCV7LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDApO3RyYW5zZm9ybTpyb3RhdGUoMCl9dG97LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDF0dXJuKTt0cmFuc2Zvcm06cm90YXRlKDF0dXJuKX19LmluZmluaXRlLWxvYWRpbmctY29udGFpbmVyW2RhdGEtdi01MDc5M2YwMl17Y2xlYXI6Ym90aDt0ZXh0LWFsaWduOmNlbnRlcn0uaW5maW5pdGUtbG9hZGluZy1jb250YWluZXIgW2NsYXNzXj1sb2FkaW5nLV1bZGF0YS12LTUwNzkzZjAyXXtkaXNwbGF5OmlubGluZS1ibG9jazttYXJnaW46MTVweCAwO3dpZHRoOjI4cHg7aGVpZ2h0OjI4cHg7Zm9udC1zaXplOjI4cHg7bGluZS1oZWlnaHQ6MjhweDtib3JkZXItcmFkaXVzOjUwJX0uaW5maW5pdGUtc3RhdHVzLXByb21wdFtkYXRhLXYtNTA3OTNmMDJde2NvbG9yOiM2NjY7Zm9udC1zaXplOjE0cHg7dGV4dC1hbGlnbjpjZW50ZXI7cGFkZGluZzoxMHB4IDB9JyxcIlwiXSl9LGZ1bmN0aW9uKHAseCl7cC5leHBvcnRzPWZ1bmN0aW9uKCl7dmFyIHA9W107cmV0dXJuIHAudG9TdHJpbmc9ZnVuY3Rpb24oKXtmb3IodmFyIHA9W10seD0wO3g8dGhpcy5sZW5ndGg7eCsrKXt2YXIgdD10aGlzW3hdO3RbMl0/cC5wdXNoKFwiQG1lZGlhIFwiK3RbMl0rXCJ7XCIrdFsxXStcIn1cIik6cC5wdXNoKHRbMV0pfXJldHVybiBwLmpvaW4oXCJcIil9LHAuaT1mdW5jdGlvbih4LHQpe1wic3RyaW5nXCI9PXR5cGVvZiB4JiYoeD1bW251bGwseCxcIlwiXV0pO2Zvcih2YXIgYT17fSxlPTA7ZTx0aGlzLmxlbmd0aDtlKyspe3ZhciBvPXRoaXNbZV1bMF07XCJudW1iZXJcIj09dHlwZW9mIG8mJihhW29dPSEwKX1mb3IoZT0wO2U8eC5sZW5ndGg7ZSsrKXt2YXIgbj14W2VdO1wibnVtYmVyXCI9PXR5cGVvZiBuWzBdJiZhW25bMF1dfHwodCYmIW5bMl0/blsyXT10OnQmJihuWzJdPVwiKFwiK25bMl0rXCIpIGFuZCAoXCIrdCtcIilcIikscC5wdXNoKG4pKX19LHB9fSxmdW5jdGlvbihwLHgsdCl7dmFyIGEsZTt0KDcpLGE9dCgxKTt2YXIgbz10KDUpO2U9YT1hfHx7fSxcIm9iamVjdFwiIT10eXBlb2YgYS5kZWZhdWx0JiZcImZ1bmN0aW9uXCIhPXR5cGVvZiBhLmRlZmF1bHR8fChlPWE9YS5kZWZhdWx0KSxcImZ1bmN0aW9uXCI9PXR5cGVvZiBlJiYoZT1lLm9wdGlvbnMpLGUucmVuZGVyPW8ucmVuZGVyLGUuc3RhdGljUmVuZGVyRm5zPW8uc3RhdGljUmVuZGVyRm5zLGUuX3Njb3BlSWQ9XCJkYXRhLXYtNTA3OTNmMDJcIixwLmV4cG9ydHM9YX0sZnVuY3Rpb24ocCx4KXtwLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbigpe3ZhciBwPXRoaXMseD1wLiRjcmVhdGVFbGVtZW50LHQ9cC5fc2VsZi5fY3x8eDtyZXR1cm4gdChcImRpdlwiLHtzdGF0aWNDbGFzczpcImluZmluaXRlLWxvYWRpbmctY29udGFpbmVyXCJ9LFt0KFwiZGl2XCIse2RpcmVjdGl2ZXM6W3tuYW1lOlwic2hvd1wiLHJhd05hbWU6XCJ2LXNob3dcIix2YWx1ZTpwLmlzTG9hZGluZyxleHByZXNzaW9uOlwiaXNMb2FkaW5nXCJ9XX0sW3AuX3QoXCJzcGlubmVyXCIsW3QoXCJpXCIse2NsYXNzOnAuc3Bpbm5lclR5cGV9KV0pXSwyKSxwLl92KFwiIFwiKSx0KFwiZGl2XCIse2RpcmVjdGl2ZXM6W3tuYW1lOlwic2hvd1wiLHJhd05hbWU6XCJ2LXNob3dcIix2YWx1ZTohcC5pc0xvYWRpbmcmJnAuaXNDb21wbGV0ZSYmcC5pc0ZpcnN0TG9hZCxleHByZXNzaW9uOlwiIWlzTG9hZGluZyAmJiBpc0NvbXBsZXRlICYmIGlzRmlyc3RMb2FkXCJ9XSxzdGF0aWNDbGFzczpcImluZmluaXRlLXN0YXR1cy1wcm9tcHRcIn0sW3AuX3QoXCJuby1yZXN1bHRzXCIsW3AuX3YoXCJObyByZXN1bHRzIDooXCIpXSldLDIpLHAuX3YoXCIgXCIpLHQoXCJkaXZcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJzaG93XCIscmF3TmFtZTpcInYtc2hvd1wiLHZhbHVlOiFwLmlzTG9hZGluZyYmcC5pc0NvbXBsZXRlJiYhcC5pc0ZpcnN0TG9hZCxleHByZXNzaW9uOlwiIWlzTG9hZGluZyAmJiBpc0NvbXBsZXRlICYmICFpc0ZpcnN0TG9hZFwifV0sc3RhdGljQ2xhc3M6XCJpbmZpbml0ZS1zdGF0dXMtcHJvbXB0XCJ9LFtwLl90KFwibm8tbW9yZVwiLFtwLl92KFwiTm8gbW9yZSBkYXRhIDopXCIpXSldLDIpXSl9LHN0YXRpY1JlbmRlckZuczpbXX19LGZ1bmN0aW9uKHAseCx0KXtmdW5jdGlvbiBhKHAseCl7Zm9yKHZhciB0PTA7dDxwLmxlbmd0aDt0Kyspe3ZhciBhPXBbdF0sZT1kW2EuaWRdO2lmKGUpe2UucmVmcysrO2Zvcih2YXIgbz0wO288ZS5wYXJ0cy5sZW5ndGg7bysrKWUucGFydHNbb10oYS5wYXJ0c1tvXSk7Zm9yKDtvPGEucGFydHMubGVuZ3RoO28rKyllLnBhcnRzLnB1c2gocihhLnBhcnRzW29dLHgpKX1lbHNle2Zvcih2YXIgbj1bXSxvPTA7bzxhLnBhcnRzLmxlbmd0aDtvKyspbi5wdXNoKHIoYS5wYXJ0c1tvXSx4KSk7ZFthLmlkXT17aWQ6YS5pZCxyZWZzOjEscGFydHM6bn19fX1mdW5jdGlvbiBlKHApe2Zvcih2YXIgeD1bXSx0PXt9LGE9MDthPHAubGVuZ3RoO2ErKyl7dmFyIGU9cFthXSxvPWVbMF0sbj1lWzFdLGk9ZVsyXSxyPWVbM10scz17Y3NzOm4sbWVkaWE6aSxzb3VyY2VNYXA6cn07dFtvXT90W29dLnBhcnRzLnB1c2gocyk6eC5wdXNoKHRbb109e2lkOm8scGFydHM6W3NdfSl9cmV0dXJuIHh9ZnVuY3Rpb24gbyhwLHgpe3ZhciB0PWMoKSxhPW1bbS5sZW5ndGgtMV07aWYoXCJ0b3BcIj09PXAuaW5zZXJ0QXQpYT9hLm5leHRTaWJsaW5nP3QuaW5zZXJ0QmVmb3JlKHgsYS5uZXh0U2libGluZyk6dC5hcHBlbmRDaGlsZCh4KTp0Lmluc2VydEJlZm9yZSh4LHQuZmlyc3RDaGlsZCksbS5wdXNoKHgpO2Vsc2V7aWYoXCJib3R0b21cIiE9PXAuaW5zZXJ0QXQpdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCB2YWx1ZSBmb3IgcGFyYW1ldGVyICdpbnNlcnRBdCcuIE11c3QgYmUgJ3RvcCcgb3IgJ2JvdHRvbScuXCIpO3QuYXBwZW5kQ2hpbGQoeCl9fWZ1bmN0aW9uIG4ocCl7cC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHApO3ZhciB4PW0uaW5kZXhPZihwKTt4Pj0wJiZtLnNwbGljZSh4LDEpfWZ1bmN0aW9uIGkocCl7dmFyIHg9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInN0eWxlXCIpO3JldHVybiB4LnR5cGU9XCJ0ZXh0L2Nzc1wiLG8ocCx4KSx4fWZ1bmN0aW9uIHIocCx4KXt2YXIgdCxhLGU7aWYoeC5zaW5nbGV0b24pe3ZhciBvPWgrKzt0PXV8fCh1PWkoeCkpLGE9cy5iaW5kKG51bGwsdCxvLCExKSxlPXMuYmluZChudWxsLHQsbywhMCl9ZWxzZSB0PWkoeCksYT1iLmJpbmQobnVsbCx0KSxlPWZ1bmN0aW9uKCl7bih0KX07cmV0dXJuIGEocCksZnVuY3Rpb24oeCl7aWYoeCl7aWYoeC5jc3M9PT1wLmNzcyYmeC5tZWRpYT09PXAubWVkaWEmJnguc291cmNlTWFwPT09cC5zb3VyY2VNYXApcmV0dXJuO2EocD14KX1lbHNlIGUoKX19ZnVuY3Rpb24gcyhwLHgsdCxhKXt2YXIgZT10P1wiXCI6YS5jc3M7aWYocC5zdHlsZVNoZWV0KXAuc3R5bGVTaGVldC5jc3NUZXh0PWcoeCxlKTtlbHNle3ZhciBvPWRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGUpLG49cC5jaGlsZE5vZGVzO25beF0mJnAucmVtb3ZlQ2hpbGQoblt4XSksbi5sZW5ndGg/cC5pbnNlcnRCZWZvcmUobyxuW3hdKTpwLmFwcGVuZENoaWxkKG8pfX1mdW5jdGlvbiBiKHAseCl7dmFyIHQ9eC5jc3MsYT14Lm1lZGlhLGU9eC5zb3VyY2VNYXA7aWYoYSYmcC5zZXRBdHRyaWJ1dGUoXCJtZWRpYVwiLGEpLGUmJih0Kz1cIlxcbi8qIyBzb3VyY2VVUkw9XCIrZS5zb3VyY2VzWzBdK1wiICovXCIsdCs9XCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiK2J0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KGUpKSkpK1wiICovXCIpLHAuc3R5bGVTaGVldClwLnN0eWxlU2hlZXQuY3NzVGV4dD10O2Vsc2V7Zm9yKDtwLmZpcnN0Q2hpbGQ7KXAucmVtb3ZlQ2hpbGQocC5maXJzdENoaWxkKTtwLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKHQpKX19dmFyIGQ9e30sbD1mdW5jdGlvbihwKXt2YXIgeDtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm5cInVuZGVmaW5lZFwiPT10eXBlb2YgeCYmKHg9cC5hcHBseSh0aGlzLGFyZ3VtZW50cykpLHh9fSxmPWwoZnVuY3Rpb24oKXtyZXR1cm4vbXNpZSBbNi05XVxcYi8udGVzdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpKX0pLGM9bChmdW5jdGlvbigpe3JldHVybiBkb2N1bWVudC5oZWFkfHxkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImhlYWRcIilbMF19KSx1PW51bGwsaD0wLG09W107cC5leHBvcnRzPWZ1bmN0aW9uKHAseCl7eD14fHx7fSxcInVuZGVmaW5lZFwiPT10eXBlb2YgeC5zaW5nbGV0b24mJih4LnNpbmdsZXRvbj1mKCkpLFwidW5kZWZpbmVkXCI9PXR5cGVvZiB4Lmluc2VydEF0JiYoeC5pbnNlcnRBdD1cImJvdHRvbVwiKTt2YXIgdD1lKHApO3JldHVybiBhKHQseCksZnVuY3Rpb24ocCl7Zm9yKHZhciBvPVtdLG49MDtuPHQubGVuZ3RoO24rKyl7dmFyIGk9dFtuXSxyPWRbaS5pZF07ci5yZWZzLS0sby5wdXNoKHIpfWlmKHApe3ZhciBzPWUocCk7YShzLHgpfWZvcih2YXIgbj0wO248by5sZW5ndGg7bisrKXt2YXIgcj1vW25dO2lmKDA9PT1yLnJlZnMpe2Zvcih2YXIgYj0wO2I8ci5wYXJ0cy5sZW5ndGg7YisrKXIucGFydHNbYl0oKTtkZWxldGUgZFtyLmlkXX19fX07dmFyIGc9ZnVuY3Rpb24oKXt2YXIgcD1bXTtyZXR1cm4gZnVuY3Rpb24oeCx0KXtyZXR1cm4gcFt4XT10LHAuZmlsdGVyKEJvb2xlYW4pLmpvaW4oXCJcXG5cIil9fSgpfSxmdW5jdGlvbihwLHgsdCl7dmFyIGE9dCgyKTtcInN0cmluZ1wiPT10eXBlb2YgYSYmKGE9W1twLmlkLGEsXCJcIl1dKTt0KDYpKGEse30pO2EubG9jYWxzJiYocC5leHBvcnRzPWEubG9jYWxzKX1dKX0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtaW5maW5pdGUtbG9hZGluZy9kaXN0L3Z1ZS1pbmZpbml0ZS1sb2FkaW5nLmpzXG4vLyBtb2R1bGUgaWQgPSAxMzBcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwiaW1wb3J0IFd5Y01lc3NhZ2UgZnJvbSAnLi4vY29tcG9uZW50cy9XeWNNZXNzYWdlLnZ1ZSdcclxuaW1wb3J0IFd5Y1VzZXJTZWFyY2ggZnJvbSAnLi4vY29tcG9uZW50cy9XeWNVc2VyU2VhcmNoLnZ1ZSdcclxuXHJcbm5ldyBWdWUoe1xyXG4gICAgZWw6ICcjY2hhdGJveCcsXHJcblxyXG4gICAgZGF0YToge1xyXG4gICAgICAgIHVzZXJMYXRlc3RNZXNzYWdlczogW10sXHJcbiAgICAgICAgc2VsZWN0ZWQ6IG51bGxcclxuICAgIH0sXHJcblxyXG4gICAgcm91dGVyOiBuZXcgVnVlUm91dGVyKHtcclxuICAgICAgICByb3V0ZXM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcGF0aDogJy91LzppZCcsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAndXNlcicsXHJcbiAgICAgICAgICAgICAgICBjb21wb25lbnQ6IHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvV3ljQ2hhdGJveC52dWUnKVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBwYXRoOiAnL2NyLzppZCcsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnY2hhdC1yb29tJyxcclxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogcmVxdWlyZSgnLi4vY29tcG9uZW50cy9XeWNDaGF0Ym94Um9vbS52dWUnKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSksXHJcblxyXG4gICAgd2F0Y2g6IHtcclxuICAgICAgICB1c2VyTGF0ZXN0TWVzc2FnZXModmFsKSB7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgJCgnI3VzZXItbWVzc2FnZScpLnNsaW1TY3JvbGwoe1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogJzEwMCUnXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgZ2V0TGF0ZXN0TWVzc2FnZXMoKSB7XHJcbiAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvbWVzc2FnZXMnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXNlckxhdGVzdE1lc3NhZ2VzID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBsb2FkU2VsZWN0ZWRVc2VyKGRhdGEpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZCA9IGRhdGE7XHJcblxyXG4gICAgICAgICAgICB0aGlzLiRyb3V0ZXIucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAndXNlcicsXHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogZGF0YS5pZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBsb2FkU2VsZWN0ZWRDaGF0Um9vbShkYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWQgPSBkYXRhO1xyXG5cclxuICAgICAgICAgICAgdGhpcy4kcm91dGVyLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogJ2NoYXQtcm9vbScsXHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogZGF0YS5pZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBuZXdNZXNzYWdlKCkge1xyXG4gICAgICAgICAgICBFdmVudC5maXJlKCd1c2VyLXNlYXJjaC5zaG93Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgV3ljTWVzc2FnZSxcclxuICAgICAgICBXeWNVc2VyU2VhcmNoXHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5nZXRMYXRlc3RNZXNzYWdlcygpO1xyXG5cclxuICAgICAgICBFdmVudC5saXN0ZW4oJ3VzZXItc2VhcmNoLnNlbGVjdGVkJywgdGhpcy5sb2FkU2VsZWN0ZWRVc2VyKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ2NoYXQtcm9vbS5zZWxlY3RlZCcsIHRoaXMubG9hZFNlbGVjdGVkQ2hhdFJvb20pO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignY2hhdC1ib3guc2VuZCcsIHRoaXMuZ2V0TGF0ZXN0TWVzc2FnZXMpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignY2hhdC1ib3gubWFya2VkLWFzLXJlYWQnLCB0aGlzLmdldExhdGVzdE1lc3NhZ2VzKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ25ldy1jaGF0LW1lc3NhZ2UnLCB0aGlzLmdldExhdGVzdE1lc3NhZ2VzKTtcclxuICAgIH1cclxufSk7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3BhZ2VzL21lc3NhZ2VzLmpzIiwiPHRlbXBsYXRlPlxyXG48ZGl2IHYtaWY9XCJ1c2VyXCIgY2xhc3M9XCJwYW5lbCBwYW5lbC1jaGF0LWJveFwiPlxyXG4gICAgPGRpdiBjbGFzcz1cInBhbmVsLWhlYWRpbmdcIj5cclxuICAgICAgICB7eyBmdWxsTmFtZSB9fVxyXG4gICAgICAgIDxhIHYtaWY9XCJzaG93U2V0dGluZ3NcIiBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgaWQ9XCJjaGF0LWJveC1zZXR0aW5nc1wiIGNsYXNzPVwicHVsbC1yaWdodFwiPlxyXG4gICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLWNvZ1wiPjwvaT5cclxuICAgICAgICA8L2E+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgY2xhc3M9XCJjaGF0LWJveFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJwYW5lbC1ib2R5XCI+XHJcbiAgICAgICAgICAgIDxpbmZpbml0ZS1sb2FkaW5nIDpvbi1pbmZpbml0ZT1cIm9uSW5maW5pdGVcIiByZWY9XCJpbmZpbml0ZUxvYWRpbmdcIiBkaXJlY3Rpb249XCJ0b3BcIiA6ZGlzdGFuY2U9XCIwXCI+PC9pbmZpbml0ZS1sb2FkaW5nPlxyXG4gICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICA8bGkgdi1mb3I9XCJtZXNzYWdlIGluIG1lc3NhZ2VzXCIgOmNsYXNzPVwieyBtZTogaXNNZShtZXNzYWdlKSB9XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibWVzc2FnZVwiIHN0eWxlPVwibWF4LXdpZHRoOjEwMCU7IHdvcmQtd3JhcDpicmVhay13b3JkO1wiPnt7IG1lc3NhZ2UuYm9keSB9fTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWZvb3RlclwiPlxyXG4gICAgICAgICAgICA8dGV4dGFyZWEgdi1tb2RlbD1cIm5ld01lc3NhZ2VcIiBAa2V5ZG93bi5zaGlmdC5lbnRlci5wcmV2ZW50PVwic2VuZFwiIDpkaXNhYmxlZD1cImlzQmxvY2tlZFwiIHBsYWNlaG9sZGVyPVwiV3JpdGUgc29tZXRoaW5nLi4uXCI+PC90ZXh0YXJlYT5cclxuICAgICAgICAgICAgPGEgQGNsaWNrPVwic2VuZFwiIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cImJ0bi1zZW5kLW1lc3NhZ2VcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtc2VuZFwiPjwvaT5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbjwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuaW1wb3J0IEluZmluaXRlTG9hZGluZyBmcm9tICd2dWUtaW5maW5pdGUtbG9hZGluZydcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgdXNlcjogbnVsbCxcclxuICAgICAgICAgICAgbWVzc2FnZXM6IFtdLFxyXG4gICAgICAgICAgICBuZXdNZXNzYWdlOiBudWxsLFxyXG4gICAgICAgICAgICBjdXJyZW50UGFnZTogMCxcclxuICAgICAgICAgICAgY2hhdFJvb21JZDogbnVsbCxcclxuICAgICAgICAgICAgJGNoYXRCb3g6IG51bGwsXHJcbiAgICAgICAgICAgICRjb252ZXJzYXRpb246IG51bGwsXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgICRyb3V0ZSh0bywgZnJvbSkge1xyXG4gICAgICAgICAgICBFdmVudC51bmxpc3RlbignY2hhdC1yb29tLScgKyB0aGlzLmNoYXRSb29tSWQpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy51c2VyID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlcyA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLm5ld01lc3NhZ2UgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRQYWdlID0gMDtcclxuICAgICAgICAgICAgdGhpcy5jaGF0Um9vbUlkID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy4kY2hhdEJveCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbiA9IG51bGw7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmdldFVzZXIoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBvbkluZmluaXRlKCkge1xyXG4gICAgICAgICAgICB0aGlzLmdldE1lc3NhZ2VzKCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0VXNlcigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvdXNlcnMvJyArIHRoaXMuJHJvdXRlLnBhcmFtcy5pZCArICc/aW5jbHVkZT1yZWxhdGlvbnNoaXAnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXIgPSByZXN1bHQuZGF0YTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkZWwgPSAkKHRoaXMuJGVsKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNoYXRCb3ggPSAkZWwuZmluZCgnLmNoYXQtYm94Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbiA9ICRlbC5maW5kKCcucGFuZWwtYm9keScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2xpbVNjcm9sbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6ICc0NDBweCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXJrQWxsQXNSZWFkKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIEV2ZW50LmZpcmUoJ3VzZXItc2VhcmNoLnNlbGVjdGVkJywgcmVzdWx0LmRhdGEpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0TWVzc2FnZXMoKSB7XHJcbiAgICAgICAgICAgIHZhciBwcmV2U2Nyb2xsSGVpZ2h0ID0gdGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvbWVzc2FnZXMvdXNlci8nICsgdGhpcy51c2VyLmlkLCB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBwYWdlOiArK3RoaXMuY3VycmVudFBhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgbGltaXQ6IDk5OVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IHJlc3VsdC5kYXRhLmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRhLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGEuaWQgLSBiLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1lc3NhZ2VzID0gZGF0YS5jb25jYXQodGhpcy5tZXNzYWdlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZSA9IHJlc3VsdC5kYXRhLmN1cnJlbnRfcGFnZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEgdGhpcy5jaGF0Um9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhdFJvb21JZCA9IGRhdGFbMF0uY2hhdF9yb29tX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RlbigpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRQYWdlID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSAtIHByZXZTY3JvbGxIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpsb2FkZWQnKTtcclxuICAgICAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBtYXJrQWxsQXNSZWFkKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvbWVzc2FnZXMvbWFyay1hbGwtYXMtcmVhZCcsIHtcclxuICAgICAgICAgICAgICAgIHRvX3VzZXI6IHRoaXMudXNlci5pZFxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LWJveC5tYXJrZWQtYXMtcmVhZCcpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzZW5kKCkge1xyXG4gICAgICAgICAgICBpZiAoISB0aGlzLm5ld01lc3NhZ2UgfHwgdGhpcy5pc0Jsb2NrZWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIHVzZXJfaWQ6IHdpbmRvdy5MYXJhdmVsLnVzZXIuaWQsXHJcbiAgICAgICAgICAgICAgICBib2R5OiB0aGlzLm5ld01lc3NhZ2VcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbi5zY3JvbGxUb3AodGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpsb2FkZWQnKTtcclxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuXHJcbiAgICAgICAgICAgIGF4aW9zQVBJdjEucG9zdCgnL21lc3NhZ2VzJywge1xyXG4gICAgICAgICAgICAgICAgdG9fdXNlcjogdGhpcy51c2VyLmlkLFxyXG4gICAgICAgICAgICAgICAgYm9keTogdGhpcy5uZXdNZXNzYWdlXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghIHRoaXMuY2hhdFJvb21JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhdFJvb21JZCA9IHJlc3VsdC5kYXRhLmRhdGEuY2hhdF9yb29tX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdGVuKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgRXZlbnQuZmlyZSgnY2hhdC1ib3guc2VuZCcpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubmV3TWVzc2FnZSA9IG51bGw7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY2xvc2UoKSB7XHJcbiAgICAgICAgICAgIEV2ZW50LmZpcmUoJ2NoYXQtYm94LmNsb3NlJywgdGhpcy51c2VyKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBpc01lKG1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHdpbmRvdy5MYXJhdmVsLnVzZXIuaWQgPT0gbWVzc2FnZS51c2VyX2lkO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHNjcm9sbFRvQm90dG9tKCkge1xyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbi5zY3JvbGxUb3AodGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpKTtcclxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBsaXN0ZW4oKSB7XHJcbiAgICAgICAgICAgIEV2ZW50Lmxpc3RlbignY2hhdC1yb29tLScgKyB0aGlzLmNoYXRSb29tSWQsIGZ1bmN0aW9uIChtZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAobWVzc2FnZS5jaGF0X3Jvb21faWQgPT0gdGhpcy5jaGF0Um9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZXNzYWdlcy5wdXNoKG1lc3NhZ2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2Nyb2xsVG9Cb3R0b20oKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZXF1ZXN0KCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvZnJpZW5kcycsIHtcclxuICAgICAgICAgICAgICAgIHVzZXJfaWQyOiB0aGlzLnVzZXIuaWQsXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHRoaXMuZ2V0VXNlcigpKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjb25maXJtKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvZnJpZW5kcy8nICsgdGhpcy51c2VyLmZyaWVuZF9pZCArICcvY29uZmlybScpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4gdGhpcy5nZXRVc2VyKCkpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNhbmNlbCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGVsZXRlUmVxdWVzdCgpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHVuZnJpZW5kKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kZWxldGVSZXF1ZXN0KCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgYmxvY2soKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLnBvc3QoJy9mcmllbmRzL2Jsb2NrJywge1xyXG4gICAgICAgICAgICAgICAgdXNlcl9pZDI6IHRoaXMudXNlci5pZCxcclxuICAgICAgICAgICAgfSkudGhlbihyZXN1bHQgPT4gdGhpcy5nZXRVc2VyKCkpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHVuYmxvY2soKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRlbGV0ZVJlcXVlc3QoKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBkZWxldGVSZXF1ZXN0KCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5kZWxldGUoJy9mcmllbmRzLycgKyB0aGlzLnVzZXIuZnJpZW5kX2lkKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHRoaXMuZ2V0VXNlcigpKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgZnVsbE5hbWUoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnVzZXIuZmlyc3RfbmFtZSArICcgJyArIHRoaXMudXNlci5sYXN0X25hbWU7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaXNCbG9ja2VkKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy51c2VyLmZyaWVuZF9zdGF0dXMgPT09ICdCbG9ja2VkJztcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzaG93U2V0dGluZ3MoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVzZXIuZnJpZW5kX3VzZXJfaWQgPT0gd2luZG93LkxhcmF2ZWwudXNlci5pZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy51c2VyLmZyaWVuZF9zdGF0dXMgPT09ICdCbG9ja2VkJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgICBJbmZpbml0ZUxvYWRpbmdcclxuICAgIH0sXHJcblxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgICB0aGlzLmdldFVzZXIoKTtcclxuICAgIH0sXHJcblxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICAkLmNvbnRleHRNZW51KHtcclxuICAgICAgICAgICAgc2VsZWN0b3I6ICcjY2hhdC1ib3gtc2V0dGluZ3MnLFxyXG4gICAgICAgICAgICB0cmlnZ2VyOiAnbGVmdCcsXHJcblxyXG4gICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24gKGtleSwgb3B0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgdGhpc1trZXldLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSxcclxuXHJcbiAgICAgICAgICAgIGJ1aWxkOiBmdW5jdGlvbiAoJHRyaWdnZXJFbGVtZW50LCBlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgdXNlcklkID0gdGhpcy51c2VyLmZyaWVuZF91c2VyX2lkLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1cyA9IHRoaXMudXNlci5mcmllbmRfc3RhdHVzLFxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zID0ge307XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKChzdGF0dXMgPT09ICdSZXF1ZXN0JykgJiYgKHVzZXJJZCAhPSB3aW5kb3cuTGFyYXZlbC51c2VyLmlkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zWydjb25maXJtJ10gPSB7IG5hbWU6ICdDb25maXJtJywgaWNvbjogJ2ZhLWNoZWNrJyB9O1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zWydkZWxldGVSZXF1ZXN0J10gPSB7IG5hbWU6ICdEZWxldGUgUmVxdWVzdCcsIGljb246ICdmYS10aW1lcycgfTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzID09PSAnUmVxdWVzdCcpIHtcclxuICAgICAgICAgICAgICAgICAgICBpdGVtc1snY2FuY2VsJ10gPSB7IG5hbWU6ICdDYW5jZWwgUmVxdWVzdCcsIGljb246ICdmYS10aW1lcycgfTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzID09PSAnRnJpZW5kJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zWyd1bmZyaWVuZCddID0geyBuYW1lOiAnVW5mcmllbmQnLCBpY29uOiAnZmEtdGltZXMnIH07XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zWydyZXF1ZXN0J10gPSB7IG5hbWU6ICdTZW5kIFJlcXVlc3QnLCBpY29uOiAnZmEtcGx1cycgfTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoc3RhdHVzID09PSAnQmxvY2tlZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICBpdGVtcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdW5ibG9jazogeyBuYW1lOiAnVW5ibG9jaycsIGljb246ICdmYS1jaGVjaycgfVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zWydibG9jayddID0geyBuYW1lOiAnQmxvY2snLCBpY29uOiAnZmEtYmFuJyB9O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiB7IGl0ZW1zIH07XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbjwvc2NyaXB0PlxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gV3ljQ2hhdGJveC52dWU/YTViZGMxYzgiLCI8dGVtcGxhdGU+XHJcbjxkaXYgdi1pZj1cImNoYXRSb29tXCIgY2xhc3M9XCJwYW5lbCBwYW5lbC1jaGF0LWJveFwiPlxyXG4gICAgPGRpdiBjbGFzcz1cImNoYXQtYm94XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cclxuICAgICAgICAgICAgPGluZmluaXRlLWxvYWRpbmcgOm9uLWluZmluaXRlPVwib25JbmZpbml0ZVwiIHJlZj1cImluZmluaXRlTG9hZGluZ1wiIGRpcmVjdGlvbj1cInRvcFwiIDpkaXN0YW5jZT1cIjBcIj48L2luZmluaXRlLWxvYWRpbmc+XHJcbiAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgIDxsaSB2LWZvcj1cIm1lc3NhZ2UgaW4gbWVzc2FnZXNcIiA6Y2xhc3M9XCJ7IG1lOiBpc01lKG1lc3NhZ2UpIH1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMTJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtZXNzYWdlXCIgc3R5bGU9XCJtYXgtd2lkdGg6MTAwJTsgd29yZC13cmFwOmJyZWFrLXdvcmQ7XCI+e3sgbWVzc2FnZS5ib2R5IH19PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgPC91bD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwicGFuZWwtZm9vdGVyXCI+XHJcbiAgICAgICAgICAgIDx0ZXh0YXJlYSB2LWlmPVwiY2hhdFJvb20uaXNfYWN0aXZlXCIgdi1tb2RlbD1cIm5ld01lc3NhZ2VcIiBAa2V5ZG93bi5lbnRlci5wcmV2ZW50PVwic2VuZFwiIHBsYWNlaG9sZGVyPVwiV3JpdGUgc29tZXRoaW5nLi4uXCI+PC90ZXh0YXJlYT5cclxuICAgICAgICAgICAgPHRleHRhcmVhIHYtZWxzZSBkaXNhYmxlZD5UaGlzIGlzc3VlIGlzIGFscmVhZHkgY2xvc2VkPC90ZXh0YXJlYT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG48L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmltcG9ydCBJbmZpbml0ZUxvYWRpbmcgZnJvbSAndnVlLWluZmluaXRlLWxvYWRpbmcnXHJcblxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNoYXRSb29tOiBudWxsLFxyXG4gICAgICAgICAgICBtZXNzYWdlczogW10sXHJcbiAgICAgICAgICAgIG5ld01lc3NhZ2U6IG51bGwsXHJcbiAgICAgICAgICAgIGN1cnJlbnRQYWdlOiAwLFxyXG4gICAgICAgICAgICAkY2hhdEJveDogbnVsbCxcclxuICAgICAgICAgICAgJGNvbnZlcnNhdGlvbjogbnVsbCxcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHdhdGNoOiB7XHJcbiAgICAgICAgJHJvdXRlKHRvLCBmcm9tKSB7XHJcbiAgICAgICAgICAgIEV2ZW50LnVubGlzdGVuKCdjaGF0LXJvb20tJyArIHRoaXMuY2hhdFJvb20gPyB0aGlzLmNoYXRSb29tLmlkIDogbnVsbCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmNoYXRSb29tID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlcyA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLm5ld01lc3NhZ2UgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRQYWdlID0gMDtcclxuICAgICAgICAgICAgdGhpcy4kY2hhdEJveCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbiA9IG51bGw7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmdldENoYXRSb29tKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgb25JbmZpbml0ZSgpIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRNZXNzYWdlcygpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdldENoYXRSb29tKCkge1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2NoYXQtcm9vbXMvJyArIHRoaXMuJHJvdXRlLnBhcmFtcy5pZClcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGF0Um9vbSA9IHJlc3VsdC5kYXRhO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRlbCA9ICQodGhpcy4kZWwpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2hhdEJveCA9ICRlbC5maW5kKCcuY2hhdC1ib3gnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uID0gJGVsLmZpbmQoJy5wYW5lbC1ib2R5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbi5zbGltU2Nyb2xsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogJzQ3OXB4J1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1hcmtBbGxBc1JlYWQoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0ZW4oKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgRXZlbnQuZmlyZSgnY2hhdC1yb29tLnNlbGVjdGVkJywgcmVzdWx0LmRhdGEpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0TWVzc2FnZXMoKSB7XHJcbiAgICAgICAgICAgIHZhciBwcmV2U2Nyb2xsSGVpZ2h0ID0gdGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvbWVzc2FnZXMvY2hhdC1yb29tLycgKyB0aGlzLmNoYXRSb29tLmlkLCB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBwYWdlOiArK3RoaXMuY3VycmVudFBhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgbGltaXQ6IDk5OVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IHJlc3VsdC5kYXRhLmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRhLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGEuaWQgLSBiLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1lc3NhZ2VzID0gZGF0YS5jb25jYXQodGhpcy5tZXNzYWdlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZSA9IHJlc3VsdC5kYXRhLmN1cnJlbnRfcGFnZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRQYWdlID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSAtIHByZXZTY3JvbGxIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpsb2FkZWQnKTtcclxuICAgICAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBtYXJrQWxsQXNSZWFkKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvbWVzc2FnZXMvbWFyay1hbGwtYXMtcmVhZCcsIHtcclxuICAgICAgICAgICAgICAgIHRvX2NoYXRfcm9vbTogdGhpcy5jaGF0Um9vbS5pZFxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LWJveC5tYXJrZWQtYXMtcmVhZCcpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzZW5kKCkge1xyXG4gICAgICAgICAgICBpZiAoISB0aGlzLm5ld01lc3NhZ2UpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIHVzZXJfaWQ6IHdpbmRvdy5MYXJhdmVsLnVzZXIuaWQsXHJcbiAgICAgICAgICAgICAgICBib2R5OiB0aGlzLm5ld01lc3NhZ2VcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbi5zY3JvbGxUb3AodGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpsb2FkZWQnKTtcclxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuXHJcbiAgICAgICAgICAgIGF4aW9zQVBJdjEucG9zdCgnL21lc3NhZ2VzJywge1xyXG4gICAgICAgICAgICAgICAgdG9fY2hhdF9yb29tOiB0aGlzLmNoYXRSb29tLmlkLFxyXG4gICAgICAgICAgICAgICAgYm9keTogdGhpcy5uZXdNZXNzYWdlXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IEV2ZW50LmZpcmUoJ2NoYXQtYm94LnNlbmQnKSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm5ld01lc3NhZ2UgPSBudWxsO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNsb3NlKCkge1xyXG4gICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LWJveC5jbG9zZScsIHRoaXMuY2hhdFJvb20pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGlzTWUobWVzc2FnZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gd2luZG93LkxhcmF2ZWwudXNlci5pZCA9PSBtZXNzYWdlLnVzZXJfaWQ7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2Nyb2xsVG9Cb3R0b20oKSB7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uLnNjcm9sbFRvcCh0aGlzLiRjb252ZXJzYXRpb24ucHJvcCgnc2Nyb2xsSGVpZ2h0JykpO1xyXG4gICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGxpc3RlbigpIHtcclxuICAgICAgICAgICAgRXZlbnQubGlzdGVuKCdjaGF0LXJvb20tJyArIHRoaXMuY2hhdFJvb20uaWQsIGZ1bmN0aW9uIChtZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAobWVzc2FnZS5jaGF0X3Jvb21faWQgPT0gdGhpcy5jaGF0Um9vbS5pZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWVzc2FnZXMucHVzaChtZXNzYWdlKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbFRvQm90dG9tKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgSW5maW5pdGVMb2FkaW5nXHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5nZXRDaGF0Um9vbSgpO1xyXG4gICAgfVxyXG59XHJcbjwvc2NyaXB0PlxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gV3ljQ2hhdGJveFJvb20udnVlPzUwMDliN2Q1IiwiPHRlbXBsYXRlPlxyXG48bGk+XHJcbiAgICA8cm91dGVyLWxpbmsgOnRvPVwicm91dGVUb1wiIGNsYXNzPVwicm93XCIgOmNsYXNzPVwieyAnbm90LXJlYWQnOiBub3RSZWFkIH1cIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgbWVzc2FnZS11c2VyLWF2YXRhclwiPlxyXG4gICAgICAgICAgICA8aW1nIDpzcmM9XCJhdmF0YXJcIiBjbGFzcz1cImltZy1jaXJjbGVcIj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTcgbWVzc2FnZS1jb250ZW50XCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJlbGxpcHNpc1wiPjxzdHJvbmc+e3sgZnVsbE5hbWUgfX08L3N0cm9uZz48L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImVsbGlwc2lzXCI+e3sgbWVzc2FnZS5ib2R5IH19PC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0zIG1lc3NhZ2UtZGF0ZVwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1yaWdodFwiPlxyXG4gICAgICAgICAgICAgICAgPHNtYWxsPnt7IGZyb21Ob3cgfX08L3NtYWxsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvcm91dGVyLWxpbms+XHJcbjwvbGk+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczogWydtZXNzYWdlJ10sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICByb3V0ZVRvKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5tZXNzYWdlLmNoYXRfcm9vbS5uYW1lKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICdjaGF0LXJvb20nLFxyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogdGhpcy5tZXNzYWdlLmNoYXRfcm9vbS5pZFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAndXNlcicsXHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogdGhpcy5tZXNzYWdlLmNoYXRfcm9vbS51c2Vyc1swXS5pZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG5vdFJlYWQoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm1lc3NhZ2UudXNlcl9pZCA9PSB3aW5kb3cuTGFyYXZlbC51c2VyLmlkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiAhIHRoaXMubWVzc2FnZS5yZWFkX2F0O1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGF2YXRhcigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubWVzc2FnZS5jaGF0X3Jvb20udXNlcnNbMF0uYXZhdGFyO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGZ1bGxOYW1lKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5tZXNzYWdlLmNoYXRfcm9vbS5uYW1lKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5tZXNzYWdlLmNoYXRfcm9vbS51c2Vyc1swXS5mdWxsX25hbWUgKyAnIC0gJyArIHRoaXMubWVzc2FnZS5jaGF0X3Jvb20ubmFtZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubWVzc2FnZS5jaGF0X3Jvb20ubmFtZSB8fCB0aGlzLm1lc3NhZ2UuY2hhdF9yb29tLnVzZXJzWzBdLmZ1bGxfbmFtZTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBmcm9tTm93KCkge1xyXG4gICAgICAgICAgICByZXR1cm4gbW9tZW50KHRoaXMubWVzc2FnZS5jcmVhdGVkX2F0KS5mcm9tTm93KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbjwvc2NyaXB0PlxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gV3ljTWVzc2FnZS52dWU/Y2ZmMzg0NGMiLCI8dGVtcGxhdGU+XHJcbjxkaXYgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0IHBhbmVsLWZsb2F0XCIgOmNsYXNzPVwicGFuZWxDbGFzc1wiPlxyXG4gICAgPGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cclxuICAgICAgICA8aW5wdXQgdi1tb2RlbD1cInNlYXJjaFwiIEBrZXl1cD1cInNlYXJjaFVzZXJcIiB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJ1c2VyLXNlYXJjaFwiIHBsYWNlaG9sZGVyPVwiU2VhcmNoLi4uXCI+XHJcblxyXG4gICAgICAgIDx1bCBjbGFzcz1cInVzZXJzLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICA8bGkgdi1mb3I9XCJ1c2VyIGluIHVzZXJzXCIgQGNsaWNrPVwic2VsZWN0KHVzZXIpXCI+XHJcbiAgICAgICAgICAgICAgICA8aW1nIDpzcmM9XCJ1c2VyLmF2YXRhclwiIGNsYXNzPVwiaW1nLWNpcmNsZVwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4+e3sgdXNlci5mdWxsX25hbWUgfX08L3NwYW4+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgPC91bD5cclxuICAgIDwvZGl2PlxyXG48L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICAgIHByb3BzOiBbJ3RhcmdldCcsICdwYW5lbENsYXNzJ10sXHJcblxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB0YXJnZXRFbDogbnVsbCxcclxuICAgICAgICAgICAgc2VhcmNoOiAnJyxcclxuICAgICAgICAgICAgdXNlcnM6IFtdLFxyXG4gICAgICAgICAgICB1c2Vyc0NvbnRhaW5lcjogbnVsbCxcclxuICAgICAgICAgICAgdGltZW91dDogbnVsbFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgd2F0Y2g6IHtcclxuICAgICAgICBzZWFyY2godmFsKSB7XHJcbiAgICAgICAgICAgIGlmICh2YWwgIT0gbnVsbCAmJiB2YWwubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJzID0gW107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB1c2Vycyh2YWwpIHtcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJzQ29udGFpbmVyLnNsaW1TY3JvbGwoKTtcclxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBzZWFyY2hVc2VyKGUpIHtcclxuICAgICAgICAgICAgaWYgKCgoKGUud2hpY2ggPD0gOTApICYmIChlLndoaWNoID49IDQ4KSkgfHwgZS53aGljaCA9PT0gOCkgJiYgdGhpcy5zZWFyY2ggJiYgKHRoaXMuc2VhcmNoLmxlbmd0aCA+PSAyKSkge1xyXG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMudGltZW91dCk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy50aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9tZXNzYWdlcy91c2Vycz9zZWFyY2g9JyArIHRoaXMuc2VhcmNoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VhcmNoICYmICh0aGlzLnNlYXJjaC5sZW5ndGggPj0gMikpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJzID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAyMDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2VsZWN0KHVzZXIpIHtcclxuICAgICAgICAgICAgRXZlbnQuZmlyZSgndXNlci1zZWFyY2guc2VsZWN0ZWQnLCB1c2VyKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX3Jlc2V0KCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgYXZhdGFyKHVzZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHVzZXIuYXZhdGFyO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIF9wb3NpdGlvbkVsKCkge1xyXG4gICAgICAgICAgICB2YXIgb2Zmc2V0ID0gdGhpcy50YXJnZXRFbC5vZmZzZXQoKSxcclxuICAgICAgICAgICAgICAgIHRlV2lkdGggPSB0aGlzLnRhcmdldEVsLm91dGVyV2lkdGgoKSxcclxuICAgICAgICAgICAgICAgIHRlSGVpZ2h0ID0gdGhpcy50YXJnZXRFbC5vdXRlckhlaWdodCgpLFxyXG4gICAgICAgICAgICAgICAgd1dpZHRoID0gd2luZG93LmlubmVyV2lkdGgsXHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbiA9IHtcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IG9mZnNldC50b3AgKyB0ZUhlaWdodCArIDEwLFxyXG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IG9mZnNldC5sZWZ0XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgdmFyICRlbCA9ICQodGhpcy4kZWwpLFxyXG4gICAgICAgICAgICAgICAgJGVsV2lkdGggPSAkZWwub3V0ZXJXaWR0aCgpIHx8ICRlbC5pbm5lcldpZHRoKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoKG9mZnNldC5sZWZ0ICsgJGVsV2lkdGgpID4gd1dpZHRoKSB7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbi5sZWZ0ID0gKG9mZnNldC5sZWZ0IC0gJGVsV2lkdGggKyB0ZVdpZHRoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgJGVsLm9mZnNldChwb3NpdGlvbik7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgX3Jlc2V0KCkge1xyXG4gICAgICAgICAgICB2YXIgJGVsID0gJCh0aGlzLiRlbCksXHJcbiAgICAgICAgICAgICAgICAkaW5wdXQgPSAkZWwuZmluZCgnaW5wdXQnKSxcclxuICAgICAgICAgICAgICAgICRldmVudCA9ICQuRXZlbnQoJ2tleXVwJywge3doaWNoOiA4fSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMudXNlcnMgPSBbXTtcclxuXHJcbiAgICAgICAgICAgICRpbnB1dC50cmlnZ2VyKCRldmVudCk7XHJcbiAgICAgICAgICAgICRpbnB1dC5ibHVyKCk7XHJcblxyXG4gICAgICAgICAgICAkZWwuaGlkZSgpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIF9yZWdpc3RlckV2ZW50TGlzdGVuZXJzKCkge1xyXG4gICAgICAgICAgICAkKHdpbmRvdykucmVzaXplKHRoaXMuX3Bvc2l0aW9uRWwpO1xyXG5cclxuICAgICAgICAgICAgRXZlbnQubGlzdGVuKCd1c2VyLXNlYXJjaC5zaG93JywgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdmFyICRlbCA9ICQodGhpcy4kZWwpO1xyXG5cclxuICAgICAgICAgICAgICAgICRlbC5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAkZWwuZmluZCgnaW5wdXQnKS5mb2N1cygpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuX3Bvc2l0aW9uRWwoKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBSZW1vdmUvSGlkZSBmbG9hdGluZyBwYW5lbHMgb24gb3V0c2lkZSBjbGlja1xyXG4gICAgICAgICAgICAkKGRvY3VtZW50KS5tb3VzZXVwKGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgJGVsID0gJCh0aGlzLiRlbCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gaWYgdGhlIHRhcmdldCBvZiB0aGUgY2xpY2sgaXNuJ3QgdGhlIHBhbmVsIG5vciBhIGRlc2NlbmRhbnQgb2YgdGhlIHBhbmVsXHJcbiAgICAgICAgICAgICAgICBpZiAoISAkZWwuaXMoZS50YXJnZXQpICYmICRlbC5oYXMoZS50YXJnZXQpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3Jlc2V0KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuX3JlZ2lzdGVyRXZlbnRMaXN0ZW5lcnMoKTtcclxuICAgIH0sXHJcblxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICB0aGlzLnRhcmdldEVsID0gJCh0aGlzLnRhcmdldCk7XHJcbiAgICAgICAgdGhpcy51c2Vyc0NvbnRhaW5lciA9ICQoJy51c2Vycy1jb250YWluZXInKTtcclxuICAgICAgICAvLyB0aGlzLnVzZXJzQ29udGFpbmVyLnBlcmZlY3RTY3JvbGxiYXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy51c2Vyc0NvbnRhaW5lci5zbGltU2Nyb2xsKHtcclxuICAgICAgICAgICAgc2l6ZTogJzRweCcsXHJcbiAgICAgICAgICAgIGhlaWdodDogJydcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFd5Y1VzZXJTZWFyY2gudnVlPzYwMWE3MGUwIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vV3ljQ2hhdGJveC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTJmYzIxYTUyXFxcIn0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1d5Y0NoYXRib3gudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxjcGFuZWxcXFxcY29tcG9uZW50c1xcXFxXeWNDaGF0Ym94LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFd5Y0NoYXRib3gudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTJmYzIxYTUyXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMmZjMjFhNTJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9XeWNDaGF0Ym94LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjIxXG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1d5Y0NoYXRib3hSb29tLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMjZkNGJhZDJcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vV3ljQ2hhdGJveFJvb20udnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxjcGFuZWxcXFxcY29tcG9uZW50c1xcXFxXeWNDaGF0Ym94Um9vbS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBXeWNDaGF0Ym94Um9vbS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMjZkNGJhZDJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0yNmQ0YmFkMlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y0NoYXRib3hSb29tLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjIyXG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1d5Y01lc3NhZ2UudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1jNGYxMDUyYVxcXCJ9IS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9XeWNNZXNzYWdlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcY3BhbmVsXFxcXGNvbXBvbmVudHNcXFxcV3ljTWVzc2FnZS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBXeWNNZXNzYWdlLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1jNGYxMDUyYVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWM0ZjEwNTJhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljTWVzc2FnZS52dWVcbi8vIG1vZHVsZSBpZCA9IDIyNVxuLy8gbW9kdWxlIGNodW5rcyA9IDIiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9XeWNVc2VyU2VhcmNoLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtODkxNzFlYzJcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vV3ljVXNlclNlYXJjaC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXGNwYW5lbFxcXFxjb21wb25lbnRzXFxcXFd5Y1VzZXJTZWFyY2gudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gV3ljVXNlclNlYXJjaC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtODkxNzFlYzJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi04OTE3MWVjMlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y1VzZXJTZWFyY2gudnVlXG4vLyBtb2R1bGUgaWQgPSAyMjdcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIChfdm0uY2hhdFJvb20pID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1jaGF0LWJveFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNoYXQtYm94XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiXG4gIH0sIFtfYygnaW5maW5pdGUtbG9hZGluZycsIHtcbiAgICByZWY6IFwiaW5maW5pdGVMb2FkaW5nXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwib24taW5maW5pdGVcIjogX3ZtLm9uSW5maW5pdGUsXG4gICAgICBcImRpcmVjdGlvblwiOiBcInRvcFwiLFxuICAgICAgXCJkaXN0YW5jZVwiOiAwXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3VsJywgX3ZtLl9sKChfdm0ubWVzc2FnZXMpLCBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgcmV0dXJuIF9jKCdsaScsIHtcbiAgICAgIGNsYXNzOiB7XG4gICAgICAgIG1lOiBfdm0uaXNNZShtZXNzYWdlKVxuICAgICAgfVxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMlwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJtZXNzYWdlXCIsXG4gICAgICBzdGF0aWNTdHlsZToge1xuICAgICAgICBcIm1heC13aWR0aFwiOiBcIjEwMCVcIixcbiAgICAgICAgXCJ3b3JkLXdyYXBcIjogXCJicmVhay13b3JkXCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KF92bS5fcyhtZXNzYWdlLmJvZHkpKV0pXSldKV0pXG4gIH0pKV0sIDEpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWZvb3RlclwiXG4gIH0sIFsoX3ZtLmNoYXRSb29tLmlzX2FjdGl2ZSkgPyBfYygndGV4dGFyZWEnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubmV3TWVzc2FnZSksXG4gICAgICBleHByZXNzaW9uOiBcIm5ld01lc3NhZ2VcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInBsYWNlaG9sZGVyXCI6IFwiV3JpdGUgc29tZXRoaW5nLi4uXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0ubmV3TWVzc2FnZSlcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImtleWRvd25cIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICghKCdidXR0b24nIGluICRldmVudCkgJiYgX3ZtLl9rKCRldmVudC5rZXlDb2RlLCBcImVudGVyXCIsIDEzKSkgeyByZXR1cm4gbnVsbDsgfVxuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgX3ZtLnNlbmQoJGV2ZW50KVxuICAgICAgfSxcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLm5ld01lc3NhZ2UgPSAkZXZlbnQudGFyZ2V0LnZhbHVlXG4gICAgICB9XG4gICAgfVxuICB9KSA6IF9jKCd0ZXh0YXJlYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJkaXNhYmxlZFwiOiBcIlwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiVGhpcyBpc3N1ZSBpcyBhbHJlYWR5IGNsb3NlZFwiKV0pXSldKV0pIDogX3ZtLl9lKClcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMjZkNGJhZDJcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0yNmQ0YmFkMlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9XeWNDaGF0Ym94Um9vbS52dWVcbi8vIG1vZHVsZSBpZCA9IDIzMlxuLy8gbW9kdWxlIGNodW5rcyA9IDIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gKF92bS51c2VyKSA/IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwgcGFuZWwtY2hhdC1ib3hcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1oZWFkaW5nXCJcbiAgfSwgW192bS5fdihcIlxcclxcbiAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmZ1bGxOYW1lKSArIFwiXFxyXFxuICAgICAgICBcIiksIChfdm0uc2hvd1NldHRpbmdzKSA/IF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImlkXCI6IFwiY2hhdC1ib3gtc2V0dGluZ3NcIlxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNvZ1wiXG4gIH0pXSkgOiBfdm0uX2UoKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNoYXQtYm94XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiXG4gIH0sIFtfYygnaW5maW5pdGUtbG9hZGluZycsIHtcbiAgICByZWY6IFwiaW5maW5pdGVMb2FkaW5nXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwib24taW5maW5pdGVcIjogX3ZtLm9uSW5maW5pdGUsXG4gICAgICBcImRpcmVjdGlvblwiOiBcInRvcFwiLFxuICAgICAgXCJkaXN0YW5jZVwiOiAwXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3VsJywgX3ZtLl9sKChfdm0ubWVzc2FnZXMpLCBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgcmV0dXJuIF9jKCdsaScsIHtcbiAgICAgIGNsYXNzOiB7XG4gICAgICAgIG1lOiBfdm0uaXNNZShtZXNzYWdlKVxuICAgICAgfVxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMlwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJtZXNzYWdlXCIsXG4gICAgICBzdGF0aWNTdHlsZToge1xuICAgICAgICBcIm1heC13aWR0aFwiOiBcIjEwMCVcIixcbiAgICAgICAgXCJ3b3JkLXdyYXBcIjogXCJicmVhay13b3JkXCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KF92bS5fcyhtZXNzYWdlLmJvZHkpKV0pXSldKV0pXG4gIH0pKV0sIDEpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWZvb3RlclwiXG4gIH0sIFtfYygndGV4dGFyZWEnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubmV3TWVzc2FnZSksXG4gICAgICBleHByZXNzaW9uOiBcIm5ld01lc3NhZ2VcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRpc2FibGVkXCI6IF92bS5pc0Jsb2NrZWQsXG4gICAgICBcInBsYWNlaG9sZGVyXCI6IFwiV3JpdGUgc29tZXRoaW5nLi4uXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0ubmV3TWVzc2FnZSlcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImtleWRvd25cIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICghKCdidXR0b24nIGluICRldmVudCkgJiYgX3ZtLl9rKCRldmVudC5rZXlDb2RlLCBcImVudGVyXCIsIDEzKSkgeyByZXR1cm4gbnVsbDsgfVxuICAgICAgICBpZiAoISRldmVudC5zaGlmdEtleSkgeyByZXR1cm4gbnVsbDsgfVxuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgX3ZtLnNlbmQoJGV2ZW50KVxuICAgICAgfSxcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLm5ld01lc3NhZ2UgPSAkZXZlbnQudGFyZ2V0LnZhbHVlXG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuLXNlbmQtbWVzc2FnZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLnNlbmRcbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1zZW5kXCJcbiAgfSldKV0pXSldKSA6IF92bS5fZSgpXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTJmYzIxYTUyXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMmZjMjFhNTJcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljQ2hhdGJveC52dWVcbi8vIG1vZHVsZSBpZCA9IDIzNFxuLy8gbW9kdWxlIGNodW5rcyA9IDIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0IHBhbmVsLWZsb2F0XCIsXG4gICAgY2xhc3M6IF92bS5wYW5lbENsYXNzXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLnNlYXJjaCksXG4gICAgICBleHByZXNzaW9uOiBcInNlYXJjaFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJ1c2VyLXNlYXJjaFwiLFxuICAgICAgXCJwbGFjZWhvbGRlclwiOiBcIlNlYXJjaC4uLlwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLnNlYXJjaClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImtleXVwXCI6IF92bS5zZWFyY2hVc2VyLFxuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uc2VhcmNoID0gJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCd1bCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ1c2Vycy1jb250YWluZXJcIlxuICB9LCBfdm0uX2woKF92bS51c2VycyksIGZ1bmN0aW9uKHVzZXIpIHtcbiAgICByZXR1cm4gX2MoJ2xpJywge1xuICAgICAgb246IHtcbiAgICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBfdm0uc2VsZWN0KHVzZXIpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX2MoJ2ltZycsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1jaXJjbGVcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwic3JjXCI6IHVzZXIuYXZhdGFyXG4gICAgICB9XG4gICAgfSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywgW192bS5fdihfdm0uX3ModXNlci5mdWxsX25hbWUpKV0pXSlcbiAgfSkpXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi04OTE3MWVjMlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTg5MTcxZWMyXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y1VzZXJTZWFyY2gudnVlXG4vLyBtb2R1bGUgaWQgPSAyNDJcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsaScsIFtfYygncm91dGVyLWxpbmsnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdub3QtcmVhZCc6IF92bS5ub3RSZWFkXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0b1wiOiBfdm0ucm91dGVUb1xuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTIgbWVzc2FnZS11c2VyLWF2YXRhclwiXG4gIH0sIFtfYygnaW1nJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImltZy1jaXJjbGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJzcmNcIjogX3ZtLmF2YXRhclxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtNyBtZXNzYWdlLWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJlbGxpcHNpc1wiXG4gIH0sIFtfYygnc3Ryb25nJywgW192bS5fdihfdm0uX3MoX3ZtLmZ1bGxOYW1lKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImVsbGlwc2lzXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm1lc3NhZ2UuYm9keSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMyBtZXNzYWdlLWRhdGVcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LXJpZ2h0XCJcbiAgfSwgW19jKCdzbWFsbCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5mcm9tTm93KSldKV0pXSldKV0sIDEpXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWM0ZjEwNTJhXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYzRmMTA1MmFcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljTWVzc2FnZS52dWVcbi8vIG1vZHVsZSBpZCA9IDI0NFxuLy8gbW9kdWxlIGNodW5rcyA9IDIiXSwic291cmNlUm9vdCI6IiJ9