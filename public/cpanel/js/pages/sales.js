/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 269);
/******/ })
/************************************************************************/
/******/ ({

/***/ 147:
/***/ (function(module, exports) {

var app = new Vue({

    el: '#orders-content',
    data: {
        sales: [],
        orderDetails: [],
        base_url: window.Laravel.base_url,
        user_id: '',
        hasDiscount: false
    },
    created: function created() {
        this.getsales();
    },

    methods: {
        btnReturnBuyerEwallet: function btnReturnBuyerEwallet(id) {
            var _this = this;

            swal({
                title: 'Confirm...',
                html: 'Return Ewallet to the Buyer?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                axiosAPIv1.post('order-details/receive-returned-item', {
                    id: id
                }).then(function (result) {
                    toastr.success('Ewallet Amount returned to the Buyer');
                    _this.getsales();
                    $("#orderdetails-modal").modal('toggle');
                }).catch(function (error) {
                    toastr.error('Updated Failed.');
                });
            }).catch(function () {
                toastr.success('Please try again.');
            });
        },
        openModal: function openModal(id) {
            var _this2 = this;

            $('.ibox').children('.ibox-content').toggleClass('sk-loading');
            axiosAPIv1.get('/getOrderDetails/' + id).then(function (result) {
                _this2.orderDetails = result.data.data;
                _this2.user_id = result.data.data.order.user_id;
                if (result.data.data.discount != 0) {
                    _this2.hasDiscount = true;
                } else {
                    _this2.hasDiscount = false;
                }

                $("#orderdetails-modal").modal();
            });
        },


        //get all sales
        getsales: function getsales() {
            var _this3 = this;

            function getAllsales() {
                $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                return axiosAPIv1.get('/getOrders');
            }
            axios.all([getAllsales()]).then(axios.spread(function (sales) {
                _this3.sales = sales.data.data;
            })).catch($.noop);
            this.callDataTables();
        },
        callDataTables: function callDataTables() {
            setTimeout(function () {
                $('.dt-sales').DataTable({
                    pageLength: 10,
                    responsive: true,
                    dom: '<"top"lf>r<"clear">t<"bottom"p>'
                });
            }, 500);
        }
    },
    watch: {
        sales: function sales() {
            $('.dt-sales').DataTable().destroy();
            this.callDataTables();
        }
    },
    updated: function updated() {
        $('.ibox').children('.ibox-content').toggleClass('sk-loading');
    },

    computed: {
        discountedPrice: function discountedPrice() {
            var $discountDecimal = this.orderDetails.discount / 100;
            var $originalPrice = this.orderDetails.price_per_item;
            var $discountAmount = $discountDecimal * $originalPrice;
            var $discountedPrice = $originalPrice - $discountAmount;
            $discountedPrice = $discountedPrice.toFixed(2);
            return $discountedPrice;
        },
        price_per_item: function price_per_item() {
            if (this.orderDetails.fee_included == 1) return parseFloat(this.orderDetails.price_per_item) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);else return this.orderDetails.price_per_item;
        },
        discountedAmount: function discountedAmount() {
            if (this.orderDetails.discount != null || this.orderDetails.discount != 0) var $amount = this.orderDetails.sale_price;else var $amount = 0;
            $amount = $amount.toFixed(2);
            return $amount;
        },
        discountPercentage: function discountPercentage() {
            if (this.orderDetails.discount != null) {
                if (this.orderDetails.fee_included == 1) {
                    var $amount = 100 - (parseFloat(this.orderDetails.price_per_item) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat) - parseFloat(this.orderDetails.discount)) / (parseFloat(this.orderDetails.price_per_item) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat)) * 100;
                    return $amount.toFixed(2);
                } else return this.orderDetails.discount;
            } else return 0;
        },
        now: function now() {
            return this.price_per_item - this.discountedAmount;
        },
        totalAmount: function totalAmount() {
            var total = parseFloat(this.orderDetails.subtotal) + parseFloat(this.orderDetails.shipping_fee);
            total = total.toFixed(2);
            return total;
        },
        shipping: function shipping() {
            if (this.orderDetails.fee_included != 1) {
                var total = parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
                total = total.toFixed(2);
            } else var total = 0.00;

            return total;
        }
    }

});

/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(147);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDE/NTk0YSoqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvcGFnZXMvc2FsZXMuanMiXSwibmFtZXMiOlsiYXBwIiwiVnVlIiwiZWwiLCJkYXRhIiwic2FsZXMiLCJvcmRlckRldGFpbHMiLCJiYXNlX3VybCIsIndpbmRvdyIsIkxhcmF2ZWwiLCJ1c2VyX2lkIiwiaGFzRGlzY291bnQiLCJjcmVhdGVkIiwiZ2V0c2FsZXMiLCJtZXRob2RzIiwiYnRuUmV0dXJuQnV5ZXJFd2FsbGV0IiwiaWQiLCJzd2FsIiwidGl0bGUiLCJodG1sIiwidHlwZSIsInNob3dDYW5jZWxCdXR0b24iLCJjb25maXJtQnV0dG9uQ29sb3IiLCJjYW5jZWxCdXR0b25Db2xvciIsImNvbmZpcm1CdXR0b25UZXh0IiwidGhlbiIsImF4aW9zQVBJdjEiLCJwb3N0IiwidG9hc3RyIiwic3VjY2VzcyIsIiQiLCJtb2RhbCIsImNhdGNoIiwiZXJyb3IiLCJvcGVuTW9kYWwiLCJjaGlsZHJlbiIsInRvZ2dsZUNsYXNzIiwiZ2V0IiwicmVzdWx0Iiwib3JkZXIiLCJkaXNjb3VudCIsImdldEFsbHNhbGVzIiwiYXhpb3MiLCJhbGwiLCJzcHJlYWQiLCJub29wIiwiY2FsbERhdGFUYWJsZXMiLCJzZXRUaW1lb3V0IiwiRGF0YVRhYmxlIiwicGFnZUxlbmd0aCIsInJlc3BvbnNpdmUiLCJkb20iLCJ3YXRjaCIsImRlc3Ryb3kiLCJ1cGRhdGVkIiwiY29tcHV0ZWQiLCJkaXNjb3VudGVkUHJpY2UiLCIkZGlzY291bnREZWNpbWFsIiwiJG9yaWdpbmFsUHJpY2UiLCJwcmljZV9wZXJfaXRlbSIsIiRkaXNjb3VudEFtb3VudCIsIiRkaXNjb3VudGVkUHJpY2UiLCJ0b0ZpeGVkIiwiZmVlX2luY2x1ZGVkIiwicGFyc2VGbG9hdCIsInNoaXBwaW5nX2ZlZSIsImNoYXJnZSIsInZhdCIsImRpc2NvdW50ZWRBbW91bnQiLCIkYW1vdW50Iiwic2FsZV9wcmljZSIsImRpc2NvdW50UGVyY2VudGFnZSIsIm5vdyIsInRvdGFsQW1vdW50IiwidG90YWwiLCJzdWJ0b3RhbCIsInNoaXBwaW5nIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBLElBQUlBLE1BQU0sSUFBSUMsR0FBSixDQUFROztBQUVkQyxRQUFJLGlCQUZVO0FBR2RDLFVBQU07QUFDRkMsZUFBTSxFQURKO0FBRUZDLHNCQUFhLEVBRlg7QUFHRkMsa0JBQVNDLE9BQU9DLE9BQVAsQ0FBZUYsUUFIdEI7QUFJRkcsaUJBQVEsRUFKTjtBQUtGQyxxQkFBYTtBQUxYLEtBSFE7QUFVZEMsV0FWYyxxQkFVTDtBQUNMLGFBQUtDLFFBQUw7QUFDSCxLQVphOztBQWFkQyxhQUFRO0FBQ0pDLDZCQURJLGlDQUNrQkMsRUFEbEIsRUFDcUI7QUFBQTs7QUFDckJDLGlCQUFLO0FBQ0RDLHVCQUFPLFlBRE47QUFFREMsc0JBQU0sOEJBRkw7QUFHREMsc0JBQU0sU0FITDtBQUlEQyxrQ0FBa0IsSUFKakI7QUFLREMsb0NBQW9CLFNBTG5CO0FBTURDLG1DQUFtQixNQU5sQjtBQU9EQyxtQ0FBbUI7QUFQbEIsYUFBTCxFQVFHQyxJQVJILENBUVEsWUFBTTtBQUNWQywyQkFBV0MsSUFBWCxDQUFnQixxQ0FBaEIsRUFBd0Q7QUFDcERYLHdCQUFHQTtBQURpRCxpQkFBeEQsRUFHQ1MsSUFIRCxDQUdNLGtCQUFVO0FBQ1pHLDJCQUFPQyxPQUFQLENBQWUsc0NBQWY7QUFDQSwwQkFBS2hCLFFBQUw7QUFDQWlCLHNCQUFFLHFCQUFGLEVBQXlCQyxLQUF6QixDQUErQixRQUEvQjtBQUNILGlCQVBELEVBUUNDLEtBUkQsQ0FRTyxpQkFBUztBQUNaSiwyQkFBT0ssS0FBUCxDQUFhLGlCQUFiO0FBQ0gsaUJBVkQ7QUFZSCxhQXJCRCxFQXFCR0QsS0FyQkgsQ0FxQlMsWUFBTTtBQUNYSix1QkFBT0MsT0FBUCxDQUFlLG1CQUFmO0FBQ0gsYUF2QkQ7QUF3QkgsU0ExQkc7QUE0QkpLLGlCQTVCSSxxQkE0Qk1sQixFQTVCTixFQTRCUztBQUFBOztBQUNUYyxjQUFFLE9BQUYsRUFBV0ssUUFBWCxDQUFvQixlQUFwQixFQUFxQ0MsV0FBckMsQ0FBaUQsWUFBakQ7QUFDQVYsdUJBQVdXLEdBQVgsQ0FBZSxzQkFBc0JyQixFQUFyQyxFQUNDUyxJQURELENBQ00sa0JBQVU7QUFDWix1QkFBS25CLFlBQUwsR0FBb0JnQyxPQUFPbEMsSUFBUCxDQUFZQSxJQUFoQztBQUNBLHVCQUFLTSxPQUFMLEdBQWU0QixPQUFPbEMsSUFBUCxDQUFZQSxJQUFaLENBQWlCbUMsS0FBakIsQ0FBdUI3QixPQUF0QztBQUNBLG9CQUFHNEIsT0FBT2xDLElBQVAsQ0FBWUEsSUFBWixDQUFpQm9DLFFBQWpCLElBQTZCLENBQWhDLEVBQWtDO0FBQzlCLDJCQUFLN0IsV0FBTCxHQUFtQixJQUFuQjtBQUNILGlCQUZELE1BRUs7QUFDRCwyQkFBS0EsV0FBTCxHQUFtQixLQUFuQjtBQUNIOztBQUVEbUIsa0JBQUUscUJBQUYsRUFBeUJDLEtBQXpCO0FBQ0gsYUFYRDtBQVlILFNBMUNHOzs7QUE0Q0o7QUFDQWxCLGdCQTdDSSxzQkE2Q007QUFBQTs7QUFDTixxQkFBUzRCLFdBQVQsR0FBdUI7QUFDbkJYLGtCQUFFLE9BQUYsRUFBV0ssUUFBWCxDQUFvQixlQUFwQixFQUFxQ0MsV0FBckMsQ0FBaUQsWUFBakQ7QUFDQSx1QkFBT1YsV0FBV1csR0FBWCxDQUFlLFlBQWYsQ0FBUDtBQUNIO0FBQ0RLLGtCQUFNQyxHQUFOLENBQVUsQ0FDTkYsYUFETSxDQUFWLEVBRUdoQixJQUZILENBRVFpQixNQUFNRSxNQUFOLENBQ0osVUFDS3ZDLEtBREwsRUFFSztBQUNMLHVCQUFLQSxLQUFMLEdBQWFBLE1BQU1ELElBQU4sQ0FBV0EsSUFBeEI7QUFDSCxhQUxPLENBRlIsRUFRQzRCLEtBUkQsQ0FRT0YsRUFBRWUsSUFSVDtBQVNBLGlCQUFLQyxjQUFMO0FBRUgsU0E3REc7QUErREpBLHNCQS9ESSw0QkErRFk7QUFDWkMsdUJBQVcsWUFBVTtBQUNqQmpCLGtCQUFFLFdBQUYsRUFBZWtCLFNBQWYsQ0FBeUI7QUFDckJDLGdDQUFZLEVBRFM7QUFFckJDLGdDQUFZLElBRlM7QUFHckJDLHlCQUFLO0FBSGdCLGlCQUF6QjtBQUtILGFBTkQsRUFNRSxHQU5GO0FBUUg7QUF4RUcsS0FiTTtBQXVGZEMsV0FBTTtBQUNGL0MsZUFBTSxpQkFBVTtBQUNaeUIsY0FBRSxXQUFGLEVBQWVrQixTQUFmLEdBQTJCSyxPQUEzQjtBQUNBLGlCQUFLUCxjQUFMO0FBQ0g7QUFKQyxLQXZGUTtBQTZGZFEsV0E3RmMscUJBNkZKO0FBQ054QixVQUFFLE9BQUYsRUFBV0ssUUFBWCxDQUFvQixlQUFwQixFQUFxQ0MsV0FBckMsQ0FBaUQsWUFBakQ7QUFDSCxLQS9GYTs7QUFnR2RtQixjQUFTO0FBQ0xDLHVCQURLLDZCQUNZO0FBQ2IsZ0JBQUlDLG1CQUFtQixLQUFLbkQsWUFBTCxDQUFrQmtDLFFBQWxCLEdBQTZCLEdBQXBEO0FBQ0EsZ0JBQUlrQixpQkFBaUIsS0FBS3BELFlBQUwsQ0FBa0JxRCxjQUF2QztBQUNBLGdCQUFJQyxrQkFBa0JILG1CQUFtQkMsY0FBekM7QUFDQSxnQkFBSUcsbUJBQW1CSCxpQkFBaUJFLGVBQXhDO0FBQ0FDLCtCQUFtQkEsaUJBQWlCQyxPQUFqQixDQUF5QixDQUF6QixDQUFuQjtBQUNBLG1CQUFRRCxnQkFBUjtBQUNILFNBUkk7QUFVTEYsc0JBVkssNEJBVVc7QUFDWixnQkFBRyxLQUFLckQsWUFBTCxDQUFrQnlELFlBQWxCLElBQWdDLENBQW5DLEVBQ0ksT0FBT0MsV0FBVyxLQUFLMUQsWUFBTCxDQUFrQnFELGNBQTdCLElBQStDSyxXQUFXLEtBQUsxRCxZQUFMLENBQWtCMkQsWUFBN0IsQ0FBL0MsR0FBNEZELFdBQVcsS0FBSzFELFlBQUwsQ0FBa0I0RCxNQUE3QixDQUE1RixHQUFtSUYsV0FBVyxLQUFLMUQsWUFBTCxDQUFrQjZELEdBQTdCLENBQTFJLENBREosS0FHSSxPQUFPLEtBQUs3RCxZQUFMLENBQWtCcUQsY0FBekI7QUFDUCxTQWZJO0FBaUJMUyx3QkFqQkssOEJBaUJhO0FBQ2QsZ0JBQUcsS0FBSzlELFlBQUwsQ0FBa0JrQyxRQUFsQixJQUE0QixJQUE1QixJQUFvQyxLQUFLbEMsWUFBTCxDQUFrQmtDLFFBQWxCLElBQTRCLENBQW5FLEVBQ0ksSUFBSTZCLFVBQVUsS0FBSy9ELFlBQUwsQ0FBa0JnRSxVQUFoQyxDQURKLEtBR0ksSUFBSUQsVUFBVSxDQUFkO0FBQ0pBLHNCQUFVQSxRQUFRUCxPQUFSLENBQWdCLENBQWhCLENBQVY7QUFDQSxtQkFBT08sT0FBUDtBQUNILFNBeEJJO0FBMEJMRSwwQkExQkssZ0NBMEJlO0FBQ2hCLGdCQUFHLEtBQUtqRSxZQUFMLENBQWtCa0MsUUFBbEIsSUFBNEIsSUFBL0IsRUFBb0M7QUFDaEMsb0JBQUcsS0FBS2xDLFlBQUwsQ0FBa0J5RCxZQUFsQixJQUFnQyxDQUFuQyxFQUFxQztBQUNoQyx3QkFBSU0sVUFBVSxNQUFLLENBQUNMLFdBQVcsS0FBSzFELFlBQUwsQ0FBa0JxRCxjQUE3QixJQUErQ0ssV0FBVyxLQUFLMUQsWUFBTCxDQUFrQjJELFlBQTdCLENBQS9DLEdBQTRGRCxXQUFXLEtBQUsxRCxZQUFMLENBQWtCNEQsTUFBN0IsQ0FBNUYsR0FBbUlGLFdBQVcsS0FBSzFELFlBQUwsQ0FBa0I2RCxHQUE3QixDQUFuSSxHQUF1S0gsV0FBVyxLQUFLMUQsWUFBTCxDQUFrQmtDLFFBQTdCLENBQXhLLEtBQWlOd0IsV0FBVyxLQUFLMUQsWUFBTCxDQUFrQnFELGNBQTdCLElBQStDSyxXQUFXLEtBQUsxRCxZQUFMLENBQWtCMkQsWUFBN0IsQ0FBL0MsR0FBNEZELFdBQVcsS0FBSzFELFlBQUwsQ0FBa0I0RCxNQUE3QixDQUE1RixHQUFtSUYsV0FBVyxLQUFLMUQsWUFBTCxDQUFrQjZELEdBQTdCLENBQXBWLENBQUQsR0FBeVgsR0FBM1k7QUFDRCwyQkFBT0UsUUFBUVAsT0FBUixDQUFnQixDQUFoQixDQUFQO0FBQ0gsaUJBSEQsTUFJSSxPQUFPLEtBQUt4RCxZQUFMLENBQWtCa0MsUUFBekI7QUFDUCxhQU5ELE1BT0ksT0FBTyxDQUFQO0FBQ1AsU0FuQ0k7QUFxQ0xnQyxXQXJDSyxpQkFxQ0E7QUFDRCxtQkFBTyxLQUFLYixjQUFMLEdBQW9CLEtBQUtTLGdCQUFoQztBQUNILFNBdkNJO0FBeUNMSyxtQkF6Q0sseUJBeUNRO0FBQ1QsZ0JBQUlDLFFBQVFWLFdBQVcsS0FBSzFELFlBQUwsQ0FBa0JxRSxRQUE3QixJQUF5Q1gsV0FBVyxLQUFLMUQsWUFBTCxDQUFrQjJELFlBQTdCLENBQXJEO0FBQ0FTLG9CQUFRQSxNQUFNWixPQUFOLENBQWMsQ0FBZCxDQUFSO0FBQ0EsbUJBQU9ZLEtBQVA7QUFDSCxTQTdDSTtBQStDTEUsZ0JBL0NLLHNCQStDSztBQUNOLGdCQUFHLEtBQUt0RSxZQUFMLENBQWtCeUQsWUFBbEIsSUFBZ0MsQ0FBbkMsRUFBcUM7QUFDakMsb0JBQUlXLFFBQVFWLFdBQVcsS0FBSzFELFlBQUwsQ0FBa0IyRCxZQUE3QixJQUE2Q0QsV0FBVyxLQUFLMUQsWUFBTCxDQUFrQjRELE1BQTdCLENBQTdDLEdBQW9GRixXQUFXLEtBQUsxRCxZQUFMLENBQWtCNkQsR0FBN0IsQ0FBaEc7QUFDQU8sd0JBQVFBLE1BQU1aLE9BQU4sQ0FBYyxDQUFkLENBQVI7QUFDSCxhQUhELE1BSUksSUFBSVksUUFBUSxJQUFaOztBQUVKLG1CQUFPQSxLQUFQO0FBQ0g7QUF2REk7O0FBaEdLLENBQVIsQ0FBVixDIiwiZmlsZSI6ImpzL3BhZ2VzL3NhbGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAyNjkpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGIyMGEyYTBiMjk1NmE5ZDZjMGQxIiwidmFyIGFwcCA9IG5ldyBWdWUoe1xyXG5cclxuICAgIGVsOiAnI29yZGVycy1jb250ZW50JyxcclxuICAgIGRhdGE6IHtcclxuICAgICAgICBzYWxlczpbXSxcclxuICAgICAgICBvcmRlckRldGFpbHM6W10sXHJcbiAgICAgICAgYmFzZV91cmw6d2luZG93LkxhcmF2ZWwuYmFzZV91cmwsXHJcbiAgICAgICAgdXNlcl9pZDonJyxcclxuICAgICAgICBoYXNEaXNjb3VudDogZmFsc2VcclxuICAgIH0sXHJcbiAgICBjcmVhdGVkKCl7XHJcbiAgICAgICAgdGhpcy5nZXRzYWxlcygpO1xyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6e1xyXG4gICAgICAgIGJ0blJldHVybkJ1eWVyRXdhbGxldChpZCl7XHJcbiAgICAgICAgICAgIHN3YWwoe1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6ICdDb25maXJtLi4uJyxcclxuICAgICAgICAgICAgICAgIGh0bWw6ICdSZXR1cm4gRXdhbGxldCB0byB0aGUgQnV5ZXI/JyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd3YXJuaW5nJyxcclxuICAgICAgICAgICAgICAgIHNob3dDYW5jZWxCdXR0b246IHRydWUsXHJcbiAgICAgICAgICAgICAgICBjb25maXJtQnV0dG9uQ29sb3I6ICcjMzA4NWQ2JyxcclxuICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbkNvbG9yOiAnI2QzMycsXHJcbiAgICAgICAgICAgICAgICBjb25maXJtQnV0dG9uVGV4dDogJ1llcydcclxuICAgICAgICAgICAgfSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLnBvc3QoJ29yZGVyLWRldGFpbHMvcmVjZWl2ZS1yZXR1cm5lZC1pdGVtJyAsIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDppZFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ0V3YWxsZXQgQW1vdW50IHJldHVybmVkIHRvIHRoZSBCdXllcicpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0c2FsZXMoKTtcclxuICAgICAgICAgICAgICAgICAgICAkKFwiI29yZGVyZGV0YWlscy1tb2RhbFwiKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoJ1VwZGF0ZWQgRmFpbGVkLicpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9KS5jYXRjaCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnUGxlYXNlIHRyeSBhZ2Fpbi4nKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgb3Blbk1vZGFsKGlkKXtcclxuICAgICAgICAgICAgJCgnLmlib3gnKS5jaGlsZHJlbignLmlib3gtY29udGVudCcpLnRvZ2dsZUNsYXNzKCdzay1sb2FkaW5nJyk7XHJcbiAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvZ2V0T3JkZXJEZXRhaWxzLycgKyBpZClcclxuICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMub3JkZXJEZXRhaWxzID0gcmVzdWx0LmRhdGEuZGF0YTtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlcl9pZCA9IHJlc3VsdC5kYXRhLmRhdGEub3JkZXIudXNlcl9pZDtcclxuICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLmRhdGEuZGlzY291bnQgIT0gMCl7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5oYXNEaXNjb3VudCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc0Rpc2NvdW50ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgJChcIiNvcmRlcmRldGFpbHMtbW9kYWxcIikubW9kYWwoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgLy9nZXQgYWxsIHNhbGVzXHJcbiAgICAgICAgZ2V0c2FsZXMoKXtcclxuICAgICAgICAgICAgZnVuY3Rpb24gZ2V0QWxsc2FsZXMoKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcuaWJveCcpLmNoaWxkcmVuKCcuaWJveC1jb250ZW50JykudG9nZ2xlQ2xhc3MoJ3NrLWxvYWRpbmcnKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL2dldE9yZGVycycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGF4aW9zLmFsbChbXHJcbiAgICAgICAgICAgICAgICBnZXRBbGxzYWxlcygpXHJcbiAgICAgICAgICAgIF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG4gICAgICAgICAgICAgICAgKFxyXG4gICAgICAgICAgICAgICAgICAgICBzYWxlc1xyXG4gICAgICAgICAgICAgICAgKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNhbGVzID0gc2FsZXMuZGF0YS5kYXRhO1xyXG4gICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2FsbERhdGFUYWJsZXMoKTtcclxuXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY2FsbERhdGFUYWJsZXMoKXtcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgJCgnLmR0LXNhbGVzJykuRGF0YVRhYmxlKHtcclxuICAgICAgICAgICAgICAgICAgICBwYWdlTGVuZ3RoOiAxMCxcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGRvbTogJzxcInRvcFwibGY+cjxcImNsZWFyXCI+dDxcImJvdHRvbVwicD4nXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSw1MDApO1xyXG5cclxuICAgICAgICB9LFxyXG4gICAgfSxcclxuICAgIHdhdGNoOntcclxuICAgICAgICBzYWxlczpmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAkKCcuZHQtc2FsZXMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2FsbERhdGFUYWJsZXMoKTtcclxuICAgICAgICB9LFxyXG4gICAgfSxcclxuICAgIHVwZGF0ZWQoKSB7XHJcbiAgICAgICAgJCgnLmlib3gnKS5jaGlsZHJlbignLmlib3gtY29udGVudCcpLnRvZ2dsZUNsYXNzKCdzay1sb2FkaW5nJyk7XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6e1xyXG4gICAgICAgIGRpc2NvdW50ZWRQcmljZSgpe1xyXG4gICAgICAgICAgICB2YXIgJGRpc2NvdW50RGVjaW1hbCA9IHRoaXMub3JkZXJEZXRhaWxzLmRpc2NvdW50IC8gMTAwO1xyXG4gICAgICAgICAgICB2YXIgJG9yaWdpbmFsUHJpY2UgPSB0aGlzLm9yZGVyRGV0YWlscy5wcmljZV9wZXJfaXRlbTtcclxuICAgICAgICAgICAgdmFyICRkaXNjb3VudEFtb3VudCA9ICRkaXNjb3VudERlY2ltYWwgKiAkb3JpZ2luYWxQcmljZTtcclxuICAgICAgICAgICAgdmFyICRkaXNjb3VudGVkUHJpY2UgPSAkb3JpZ2luYWxQcmljZSAtICRkaXNjb3VudEFtb3VudDtcclxuICAgICAgICAgICAgJGRpc2NvdW50ZWRQcmljZSA9ICRkaXNjb3VudGVkUHJpY2UudG9GaXhlZCgyKTtcclxuICAgICAgICAgICAgcmV0dXJuICgkZGlzY291bnRlZFByaWNlKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBwcmljZV9wZXJfaXRlbSgpe1xyXG4gICAgICAgICAgICBpZih0aGlzLm9yZGVyRGV0YWlscy5mZWVfaW5jbHVkZWQ9PTEpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcGFyc2VGbG9hdCh0aGlzLm9yZGVyRGV0YWlscy5wcmljZV9wZXJfaXRlbSkgKyBwYXJzZUZsb2F0KHRoaXMub3JkZXJEZXRhaWxzLnNoaXBwaW5nX2ZlZSkgKyBwYXJzZUZsb2F0KHRoaXMub3JkZXJEZXRhaWxzLmNoYXJnZSkgKyBwYXJzZUZsb2F0KHRoaXMub3JkZXJEZXRhaWxzLnZhdCk7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLm9yZGVyRGV0YWlscy5wcmljZV9wZXJfaXRlbTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIFxyXG4gICAgICAgIGRpc2NvdW50ZWRBbW91bnQoKXtcclxuICAgICAgICAgICAgaWYodGhpcy5vcmRlckRldGFpbHMuZGlzY291bnQhPW51bGwgfHwgdGhpcy5vcmRlckRldGFpbHMuZGlzY291bnQhPTApXHJcbiAgICAgICAgICAgICAgICB2YXIgJGFtb3VudCA9IHRoaXMub3JkZXJEZXRhaWxzLnNhbGVfcHJpY2U7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHZhciAkYW1vdW50ID0gMDtcclxuICAgICAgICAgICAgJGFtb3VudCA9ICRhbW91bnQudG9GaXhlZCgyKTtcclxuICAgICAgICAgICAgcmV0dXJuICRhbW91bnQ7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZGlzY291bnRQZXJjZW50YWdlKCl7XHJcbiAgICAgICAgICAgIGlmKHRoaXMub3JkZXJEZXRhaWxzLmRpc2NvdW50IT1udWxsKXtcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMub3JkZXJEZXRhaWxzLmZlZV9pbmNsdWRlZD09MSl7XHJcbiAgICAgICAgICAgICAgICAgICAgIHZhciAkYW1vdW50ID0gMTAwLSgocGFyc2VGbG9hdCh0aGlzLm9yZGVyRGV0YWlscy5wcmljZV9wZXJfaXRlbSkgKyBwYXJzZUZsb2F0KHRoaXMub3JkZXJEZXRhaWxzLnNoaXBwaW5nX2ZlZSkgKyBwYXJzZUZsb2F0KHRoaXMub3JkZXJEZXRhaWxzLmNoYXJnZSkgKyBwYXJzZUZsb2F0KHRoaXMub3JkZXJEZXRhaWxzLnZhdCkgLSBwYXJzZUZsb2F0KHRoaXMub3JkZXJEZXRhaWxzLmRpc2NvdW50KSkvKHBhcnNlRmxvYXQodGhpcy5vcmRlckRldGFpbHMucHJpY2VfcGVyX2l0ZW0pICsgcGFyc2VGbG9hdCh0aGlzLm9yZGVyRGV0YWlscy5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdCh0aGlzLm9yZGVyRGV0YWlscy5jaGFyZ2UpICsgcGFyc2VGbG9hdCh0aGlzLm9yZGVyRGV0YWlscy52YXQpKSkqMTAwO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAkYW1vdW50LnRvRml4ZWQoMik7ICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9IGVsc2VcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5vcmRlckRldGFpbHMuZGlzY291bnQ7XHJcbiAgICAgICAgICAgIH0gZWxzZVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDA7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgbm93KCl7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnByaWNlX3Blcl9pdGVtLXRoaXMuZGlzY291bnRlZEFtb3VudDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB0b3RhbEFtb3VudCgpe1xyXG4gICAgICAgICAgICB2YXIgdG90YWwgPSBwYXJzZUZsb2F0KHRoaXMub3JkZXJEZXRhaWxzLnN1YnRvdGFsKSArIHBhcnNlRmxvYXQodGhpcy5vcmRlckRldGFpbHMuc2hpcHBpbmdfZmVlKTtcclxuICAgICAgICAgICAgdG90YWwgPSB0b3RhbC50b0ZpeGVkKDIpO1xyXG4gICAgICAgICAgICByZXR1cm4gdG90YWw7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2hpcHBpbmcoKXtcclxuICAgICAgICAgICAgaWYodGhpcy5vcmRlckRldGFpbHMuZmVlX2luY2x1ZGVkIT0xKXtcclxuICAgICAgICAgICAgICAgIHZhciB0b3RhbCA9IHBhcnNlRmxvYXQodGhpcy5vcmRlckRldGFpbHMuc2hpcHBpbmdfZmVlKSArIHBhcnNlRmxvYXQodGhpcy5vcmRlckRldGFpbHMuY2hhcmdlKSArIHBhcnNlRmxvYXQodGhpcy5vcmRlckRldGFpbHMudmF0KTtcclxuICAgICAgICAgICAgICAgIHRvdGFsID0gdG90YWwudG9GaXhlZCgyKTtcclxuICAgICAgICAgICAgfSBlbHNlIFxyXG4gICAgICAgICAgICAgICAgdmFyIHRvdGFsID0gMC4wMDtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0b3RhbDsgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufSk7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3BhZ2VzL3NhbGVzLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==