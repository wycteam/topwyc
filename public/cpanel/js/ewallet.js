/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 265);
/******/ })
/************************************************************************/
/******/ ({

/***/ 143:
/***/ (function(module, exports) {

var currentTable = null;
var currentTransaction = null;
var EwalletApp = new Vue({
	el: '#ewallet-root',

	data: {
		transactions: [],
		filterType: 'all',
		filterStatus: 'all',
		fileDepositSlip: '',
		filename: '',
		status: '',
		reason: '',
		newStatus: '',

		baseHeight: 0,
		baseWidth: 0,
		baseZoom: 1,
		showZoomable: false,

		pending_count: 0,
		processing_count: 0,
		cancelled_count: 0,
		completed_count: 0,
		status_seen: 'Pending'
	},

	watch: {
		transactions: function transactions() {
			$('.dt-ewallet').DataTable().destroy();

			setTimeout(function () {
				currentTable = $('.dt-ewallet').DataTable({
					pageLength: 10,
					responsive: true
				});
			}, 500);
		},

		filterType: function filterType() {
			$('.dt-ewallet').DataTable().destroy();

			setTimeout(function () {
				currentTable = $('.dt-ewallet').DataTable({
					pageLength: 10,
					responsive: true
				});
			}, 500);
		},

		baseZoom: function baseZoom(newValue) {
			var changedHeightSize = this.baseHeight * newValue;
			var changedWidthSize = this.baseWidth * newValue;
			$('#doc-container').find('img').css('height', changedHeightSize);
			$('#doc-container').find('img').css('width', changedWidthSize);
			$('#doc-container').find('img').removeClass('img-responsive');
		}
		// current_transaction_image:function(){
		// 	this.baseZoom = 1;
		// },
	},

	methods: {
		changeStatus: function changeStatus() {
			var _this = this;

			if (this.newStatus == 'Cancelled') {
				if (this.reason == '' || this.reason == null) {
					toastr.error('', 'Reason is required for cancelling Ewallet Requests');
					return;
				}
			} else {
				this.reason = '';
			}
			axiosAPIv1.put('/ewallet-transaction/' + currentTransaction, {
				status: this.newStatus,
				reason: this.reason
			}).then(function (response) {
				if (response.status == 200) {
					_this.transactions.filter(function (obj) {
						if (obj.id == currentTransaction) {
							obj.status = _this.newStatus;
							obj.updated_at = response.data.data.updated_at;
							modal = obj.type == 'Deposit' ? 'depositModal' : 'decisionModal';
							toastr.success('Status Updated!');

							$('.dt-ewallet').DataTable().destroy();

							_this.showTransactionByStatus(_this.status_seen);

							setTimeout(function () {
								currentTable = $('.dt-ewallet').DataTable({
									pageLength: 10,
									responsive: true
								});
							}, 500);

							$('#' + modal).modal('toggle');
						}
					});
				}
			});
		},
		statusColor: function statusColor(status) {
			switch (status) {
				case 'Pending':
					return 'label-warning';break;
				case 'Processing':
					return 'label-default';break;
				case 'Completed':
					return 'label-primary';break;
				case 'Cancelled':
					return 'label-danger';break;
			}
		},
		showFilterStatus: function showFilterStatus(status) {
			if (this.filterStatus != 'all') {
				return this.filterStatus == status.toLowerCase();
			}
			return true;
		},
		showFilterType: function showFilterType(type) {
			if (this.filterType != 'all') {
				return this.filterType === type.toLowerCase();
			}
			return true;
		},
		showModal: function showModal(id) {
			currentTransaction = id;
			var transaction = this.transactions.filter(function (obj) {
				return obj.id == id;
			});

			if (transaction.length) {
				transaction = transaction[0];
				// if(transaction.status == 'Completed' || transaction.status == 'Cancelled' || transaction.type == 'Transfer')
				// {
				// 	return true;
				// }
				this.reason = transaction.reason;
				modal = transaction.type == 'Deposit' ? 'depositModal' : 'decisionModal';
				if (transaction.deposit_slip) {
					this.filename = transaction.deposit_slip;
				}
				this.newStatus = transaction.status;

				$('#' + modal).modal('toggle');
			}
		},
		textColor: function textColor(status) {
			switch (status) {
				case 'Pending':
					return 'row-pending';break;
				case 'Completed':
					return 'row-completed';break;
				case 'Processing':
					return 'row-processing';break;
				case 'Cancelled':
					return 'row-cancelled';break;
			}
		},
		getTransactionsByStatus: function getTransactionsByStatus(status) {
			var _this2 = this;

			axiosAPIv1.get('/ewallet-transaction').then(function (response) {
				if (response.status == 200) {
					if (status != 'Processing') {
						_this2.transactions = response.data.filter(function (obj) {
							return obj.status == status;
						});
					} else {
						_this2.transactions = response.data.filter(function (obj) {
							return obj.status == 'Processing' && obj.processor_id == Laravel.user.id;
						});
					}

					_this2.pending_count = response.data.filter(function (obj) {
						return obj.status == 'Pending';
					}).length;
					_this2.processing_count = response.data.filter(function (obj) {
						return obj.status == 'Processing' && obj.processor_id == Laravel.user.id;
					}).length;
					_this2.cancelled_count = response.data.filter(function (obj) {
						return obj.status == 'Cancelled';
					}).length;
					_this2.completed_count = response.data.filter(function (obj) {
						return obj.status == 'Completed';
					}).length;
				}
			});
		},
		showTransactionByStatus: function showTransactionByStatus(status) {
			this.status_seen = status;
			// $('.dt-ewallet').DataTable().destroy();
			this.getTransactionsByStatus(status);
		}
	},

	created: function created() {
		this.getTransactionsByStatus('Pending');
	},
	mounted: function mounted() {
		var _this3 = this;

		$('#depositModal').on('shown.bs.modal', function () {
			_this3.baseZoom = 1;
			img = $('<img id="shittyimage" class="center-block img-responsive" src="' + _this3.imgDS + '">').on('load', function () {
				var $modalImg = $('<img src="' + _this3.imgDS + '" class="center-block img-responsive" >');
				$modalImg.appendTo('#doc-container');
				_this3.baseHeight = $modalImg.height();
				_this3.baseWidth = $modalImg.width();

				$modalImg.draggable();
			});
		});

		$('#depositModal').on('hide.bs.modal', function () {
			$('#doc-container').empty();
			_this3.showZoomable = false;
			_this3.filename = false;
		});
	},


	computed: {
		imgDS: function imgDS() {
			if (this.filename) {
				this.showZoomable = true;
				return '/shopping/images/deposit-slip/' + this.filename;
			}
			this.showZoomable = false;
			return false;
		}
	}

});

/***/ }),

/***/ 265:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(143);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDE/NTk0YSoqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3BhZ2VzL2V3YWxsZXQuanMiXSwibmFtZXMiOlsiY3VycmVudFRhYmxlIiwiY3VycmVudFRyYW5zYWN0aW9uIiwiRXdhbGxldEFwcCIsIlZ1ZSIsImVsIiwiZGF0YSIsInRyYW5zYWN0aW9ucyIsImZpbHRlclR5cGUiLCJmaWx0ZXJTdGF0dXMiLCJmaWxlRGVwb3NpdFNsaXAiLCJmaWxlbmFtZSIsInN0YXR1cyIsInJlYXNvbiIsIm5ld1N0YXR1cyIsImJhc2VIZWlnaHQiLCJiYXNlV2lkdGgiLCJiYXNlWm9vbSIsInNob3dab29tYWJsZSIsInBlbmRpbmdfY291bnQiLCJwcm9jZXNzaW5nX2NvdW50IiwiY2FuY2VsbGVkX2NvdW50IiwiY29tcGxldGVkX2NvdW50Iiwic3RhdHVzX3NlZW4iLCJ3YXRjaCIsIiQiLCJEYXRhVGFibGUiLCJkZXN0cm95Iiwic2V0VGltZW91dCIsInBhZ2VMZW5ndGgiLCJyZXNwb25zaXZlIiwibmV3VmFsdWUiLCJjaGFuZ2VkSGVpZ2h0U2l6ZSIsImNoYW5nZWRXaWR0aFNpemUiLCJmaW5kIiwiY3NzIiwicmVtb3ZlQ2xhc3MiLCJtZXRob2RzIiwiY2hhbmdlU3RhdHVzIiwidG9hc3RyIiwiZXJyb3IiLCJheGlvc0FQSXYxIiwicHV0IiwidGhlbiIsInJlc3BvbnNlIiwiZmlsdGVyIiwib2JqIiwiaWQiLCJ1cGRhdGVkX2F0IiwibW9kYWwiLCJ0eXBlIiwic3VjY2VzcyIsInNob3dUcmFuc2FjdGlvbkJ5U3RhdHVzIiwic3RhdHVzQ29sb3IiLCJzaG93RmlsdGVyU3RhdHVzIiwidG9Mb3dlckNhc2UiLCJzaG93RmlsdGVyVHlwZSIsInNob3dNb2RhbCIsInRyYW5zYWN0aW9uIiwibGVuZ3RoIiwiZGVwb3NpdF9zbGlwIiwidGV4dENvbG9yIiwiZ2V0VHJhbnNhY3Rpb25zQnlTdGF0dXMiLCJnZXQiLCJwcm9jZXNzb3JfaWQiLCJMYXJhdmVsIiwidXNlciIsImNyZWF0ZWQiLCJtb3VudGVkIiwib24iLCJpbWciLCJpbWdEUyIsIiRtb2RhbEltZyIsImFwcGVuZFRvIiwiaGVpZ2h0Iiwid2lkdGgiLCJkcmFnZ2FibGUiLCJlbXB0eSIsImNvbXB1dGVkIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBLElBQUlBLGVBQWUsSUFBbkI7QUFDQSxJQUFJQyxxQkFBcUIsSUFBekI7QUFDQSxJQUFJQyxhQUFhLElBQUlDLEdBQUosQ0FBUTtBQUN4QkMsS0FBSSxlQURvQjs7QUFHeEJDLE9BQUs7QUFDSkMsZ0JBQWEsRUFEVDtBQUVKQyxjQUFXLEtBRlA7QUFHSkMsZ0JBQWEsS0FIVDtBQUlKQyxtQkFBZ0IsRUFKWjtBQUtKQyxZQUFTLEVBTEw7QUFNSkMsVUFBTyxFQU5IO0FBT0pDLFVBQU8sRUFQSDtBQVFKQyxhQUFVLEVBUk47O0FBVUpDLGNBQVksQ0FWUjtBQVdEQyxhQUFXLENBWFY7QUFZREMsWUFBVyxDQVpWO0FBYUpDLGdCQUFlLEtBYlg7O0FBZUpDLGlCQUFjLENBZlY7QUFnQkpDLG9CQUFpQixDQWhCYjtBQWlCSkMsbUJBQWdCLENBakJaO0FBa0JKQyxtQkFBZ0IsQ0FsQlo7QUFtQkpDLGVBQVk7QUFuQlIsRUFIbUI7O0FBeUJ4QkMsUUFBTTtBQUNMakIsZ0JBQWEsd0JBQVU7QUFDcEJrQixLQUFFLGFBQUYsRUFBaUJDLFNBQWpCLEdBQTZCQyxPQUE3Qjs7QUFFRkMsY0FBVyxZQUFVO0FBQ3BCM0IsbUJBQWV3QixFQUFFLGFBQUYsRUFBaUJDLFNBQWpCLENBQTJCO0FBQ25DRyxpQkFBWSxFQUR1QjtBQUVuQ0MsaUJBQVk7QUFGdUIsS0FBM0IsQ0FBZjtBQUlBLElBTEQsRUFLRSxHQUxGO0FBTUEsR0FWSTs7QUFZTHRCLGNBQVksc0JBQVU7QUFDckJpQixLQUFFLGFBQUYsRUFBaUJDLFNBQWpCLEdBQTZCQyxPQUE3Qjs7QUFFQUMsY0FBVyxZQUFVO0FBQ3BCM0IsbUJBQWV3QixFQUFFLGFBQUYsRUFBaUJDLFNBQWpCLENBQTJCO0FBQ25DRyxpQkFBWSxFQUR1QjtBQUVuQ0MsaUJBQVk7QUFGdUIsS0FBM0IsQ0FBZjtBQUlBLElBTEQsRUFLRSxHQUxGO0FBTUEsR0FyQkk7O0FBdUJMYixZQUFTLGtCQUFTYyxRQUFULEVBQWtCO0FBQzFCLE9BQUlDLG9CQUFxQixLQUFLakIsVUFBTCxHQUFrQmdCLFFBQTNDO0FBQ0EsT0FBSUUsbUJBQW1CLEtBQUtqQixTQUFMLEdBQWlCZSxRQUF4QztBQUNBTixLQUFFLGdCQUFGLEVBQW9CUyxJQUFwQixDQUF5QixLQUF6QixFQUFnQ0MsR0FBaEMsQ0FBb0MsUUFBcEMsRUFBNkNILGlCQUE3QztBQUNBUCxLQUFFLGdCQUFGLEVBQW9CUyxJQUFwQixDQUF5QixLQUF6QixFQUFnQ0MsR0FBaEMsQ0FBb0MsT0FBcEMsRUFBNENGLGdCQUE1QztBQUNBUixLQUFFLGdCQUFGLEVBQW9CUyxJQUFwQixDQUF5QixLQUF6QixFQUFnQ0UsV0FBaEMsQ0FBNEMsZ0JBQTVDO0FBQ0E7QUFDRDtBQUNBO0FBQ0E7QUFoQ0ssRUF6QmtCOztBQThEeEJDLFVBQVE7QUFFUEMsY0FGTywwQkFFTztBQUFBOztBQUNiLE9BQUcsS0FBS3hCLFNBQUwsSUFBa0IsV0FBckIsRUFDQTtBQUNDLFFBQUcsS0FBS0QsTUFBTCxJQUFlLEVBQWYsSUFBcUIsS0FBS0EsTUFBTCxJQUFlLElBQXZDLEVBQ0E7QUFDQzBCLFlBQU9DLEtBQVAsQ0FBYSxFQUFiLEVBQWdCLG9EQUFoQjtBQUNBO0FBQ0E7QUFDRCxJQVBELE1BUUE7QUFDQyxTQUFLM0IsTUFBTCxHQUFjLEVBQWQ7QUFDQTtBQUNENEIsY0FDRUMsR0FERixDQUNNLDBCQUF3QnhDLGtCQUQ5QixFQUNpRDtBQUMvQ1UsWUFBTyxLQUFLRSxTQURtQztBQUUvQ0QsWUFBTyxLQUFLQTtBQUZtQyxJQURqRCxFQUtFOEIsSUFMRixDQUtPLG9CQUFZO0FBQ2pCLFFBQUdDLFNBQVNoQyxNQUFULElBQW1CLEdBQXRCLEVBQ0E7QUFDQyxXQUFLTCxZQUFMLENBQWtCc0MsTUFBbEIsQ0FBeUIsZUFBTztBQUMvQixVQUFHQyxJQUFJQyxFQUFKLElBQVU3QyxrQkFBYixFQUNBO0FBQ0M0QyxXQUFJbEMsTUFBSixHQUFhLE1BQUtFLFNBQWxCO0FBQ0FnQyxXQUFJRSxVQUFKLEdBQWlCSixTQUFTdEMsSUFBVCxDQUFjQSxJQUFkLENBQW1CMEMsVUFBcEM7QUFDQUMsZUFBTUgsSUFBSUksSUFBSixJQUFVLFNBQVYsR0FBb0IsY0FBcEIsR0FBbUMsZUFBekM7QUFDQVgsY0FBT1ksT0FBUCxDQUFlLGlCQUFmOztBQUVBMUIsU0FBRSxhQUFGLEVBQWlCQyxTQUFqQixHQUE2QkMsT0FBN0I7O0FBRUEsYUFBS3lCLHVCQUFMLENBQTZCLE1BQUs3QixXQUFsQzs7QUFFQUssa0JBQVcsWUFBVTtBQUNwQjNCLHVCQUFld0IsRUFBRSxhQUFGLEVBQWlCQyxTQUFqQixDQUEyQjtBQUNuQ0cscUJBQVksRUFEdUI7QUFFbkNDLHFCQUFZO0FBRnVCLFNBQTNCLENBQWY7QUFJQSxRQUxELEVBS0UsR0FMRjs7QUFPQUwsU0FBRSxNQUFJd0IsS0FBTixFQUFhQSxLQUFiLENBQW1CLFFBQW5CO0FBRUE7QUFDRCxNQXRCRDtBQXVCQTtBQUNELElBaENGO0FBaUNBLEdBL0NNO0FBaURQSSxhQWpETyx1QkFpREt6QyxNQWpETCxFQWlEWTtBQUNsQixXQUFPQSxNQUFQO0FBQ0MsU0FBSyxTQUFMO0FBQWdCLFlBQU8sZUFBUCxDQUF3QjtBQUN4QyxTQUFLLFlBQUw7QUFBbUIsWUFBTyxlQUFQLENBQXdCO0FBQzNDLFNBQUssV0FBTDtBQUFrQixZQUFPLGVBQVAsQ0FBd0I7QUFDMUMsU0FBSyxXQUFMO0FBQWtCLFlBQU8sY0FBUCxDQUF1QjtBQUoxQztBQU1BLEdBeERNO0FBMERQMEMsa0JBMURPLDRCQTBEVTFDLE1BMURWLEVBMERpQjtBQUN2QixPQUFHLEtBQUtILFlBQUwsSUFBcUIsS0FBeEIsRUFDQTtBQUNDLFdBQU8sS0FBS0EsWUFBTCxJQUFxQkcsT0FBTzJDLFdBQVAsRUFBNUI7QUFDQTtBQUNELFVBQU8sSUFBUDtBQUNBLEdBaEVNO0FBa0VQQyxnQkFsRU8sMEJBa0VRTixJQWxFUixFQWtFYTtBQUNuQixPQUFHLEtBQUsxQyxVQUFMLElBQW1CLEtBQXRCLEVBQ0E7QUFDQyxXQUFPLEtBQUtBLFVBQUwsS0FBb0IwQyxLQUFLSyxXQUFMLEVBQTNCO0FBQ0E7QUFDRCxVQUFPLElBQVA7QUFDQSxHQXhFTTtBQTBFUEUsV0ExRU8scUJBMEVHVixFQTFFSCxFQTBFTTtBQUNaN0Msd0JBQXFCNkMsRUFBckI7QUFDQSxPQUFJVyxjQUFjLEtBQUtuRCxZQUFMLENBQWtCc0MsTUFBbEIsQ0FBeUIsZUFBTztBQUNqRCxXQUFPQyxJQUFJQyxFQUFKLElBQVVBLEVBQWpCO0FBQ0EsSUFGaUIsQ0FBbEI7O0FBSUEsT0FBR1csWUFBWUMsTUFBZixFQUNBO0FBQ0NELGtCQUFjQSxZQUFZLENBQVosQ0FBZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBSzdDLE1BQUwsR0FBYzZDLFlBQVk3QyxNQUExQjtBQUNBb0MsWUFBTVMsWUFBWVIsSUFBWixJQUFrQixTQUFsQixHQUE0QixjQUE1QixHQUEyQyxlQUFqRDtBQUNBLFFBQUdRLFlBQVlFLFlBQWYsRUFDQTtBQUNDLFVBQUtqRCxRQUFMLEdBQWdCK0MsWUFBWUUsWUFBNUI7QUFDQTtBQUNELFNBQUs5QyxTQUFMLEdBQWlCNEMsWUFBWTlDLE1BQTdCOztBQUVBYSxNQUFFLE1BQUl3QixLQUFOLEVBQWFBLEtBQWIsQ0FBbUIsUUFBbkI7QUFDQTtBQUNELEdBakdNO0FBbUdQWSxXQW5HTyxxQkFtR0dqRCxNQW5HSCxFQW1HVTtBQUNoQixXQUFPQSxNQUFQO0FBRUMsU0FBSyxTQUFMO0FBQWdCLFlBQU8sYUFBUCxDQUFzQjtBQUN0QyxTQUFLLFdBQUw7QUFBa0IsWUFBTyxlQUFQLENBQXdCO0FBQzFDLFNBQUssWUFBTDtBQUFtQixZQUFPLGdCQUFQLENBQXlCO0FBQzVDLFNBQUssV0FBTDtBQUFrQixZQUFPLGVBQVAsQ0FBd0I7QUFMM0M7QUFPQSxHQTNHTTtBQTZHUGtELHlCQTdHTyxtQ0E2R2lCbEQsTUE3R2pCLEVBOEdQO0FBQUE7O0FBQ0M2QixjQUFXc0IsR0FBWCxDQUFlLHNCQUFmLEVBQ0NwQixJQURELENBQ00sb0JBQVk7QUFDakIsUUFBR0MsU0FBU2hDLE1BQVQsSUFBbUIsR0FBdEIsRUFDQTtBQUNDLFNBQUdBLFVBQVUsWUFBYixFQUNBO0FBQ0MsYUFBS0wsWUFBTCxHQUFvQnFDLFNBQVN0QyxJQUFULENBQWN1QyxNQUFkLENBQXFCLGVBQU87QUFBQyxjQUFPQyxJQUFJbEMsTUFBSixJQUFjQSxNQUFyQjtBQUE0QixPQUF6RCxDQUFwQjtBQUNBLE1BSEQsTUFJQTtBQUNDLGFBQUtMLFlBQUwsR0FBb0JxQyxTQUFTdEMsSUFBVCxDQUFjdUMsTUFBZCxDQUFxQixlQUFPO0FBQUMsY0FBT0MsSUFBSWxDLE1BQUosSUFBYyxZQUFkLElBQThCa0MsSUFBSWtCLFlBQUosSUFBb0JDLFFBQVFDLElBQVIsQ0FBYW5CLEVBQXRFO0FBQXlFLE9BQXRHLENBQXBCO0FBQ0E7O0FBRUQsWUFBSzVCLGFBQUwsR0FBcUJ5QixTQUFTdEMsSUFBVCxDQUFjdUMsTUFBZCxDQUFxQixlQUFPO0FBQUMsYUFBT0MsSUFBSWxDLE1BQUosSUFBYyxTQUFyQjtBQUErQixNQUE1RCxFQUE4RCtDLE1BQW5GO0FBQ0EsWUFBS3ZDLGdCQUFMLEdBQXdCd0IsU0FBU3RDLElBQVQsQ0FBY3VDLE1BQWQsQ0FBcUIsZUFBTztBQUFDLGFBQU9DLElBQUlsQyxNQUFKLElBQWMsWUFBZCxJQUErQmtDLElBQUlrQixZQUFKLElBQW9CQyxRQUFRQyxJQUFSLENBQWFuQixFQUF2RTtBQUEwRSxNQUF2RyxFQUF5R1ksTUFBakk7QUFDQSxZQUFLdEMsZUFBTCxHQUF1QnVCLFNBQVN0QyxJQUFULENBQWN1QyxNQUFkLENBQXFCLGVBQU87QUFBQyxhQUFPQyxJQUFJbEMsTUFBSixJQUFjLFdBQXJCO0FBQWlDLE1BQTlELEVBQWdFK0MsTUFBdkY7QUFDQSxZQUFLckMsZUFBTCxHQUF1QnNCLFNBQVN0QyxJQUFULENBQWN1QyxNQUFkLENBQXFCLGVBQU87QUFBQyxhQUFPQyxJQUFJbEMsTUFBSixJQUFjLFdBQXJCO0FBQWlDLE1BQTlELEVBQWdFK0MsTUFBdkY7QUFHQTtBQUNELElBbkJEO0FBb0JBLEdBbklNO0FBcUlQUCx5QkFySU8sbUNBcUlpQnhDLE1BcklqQixFQXNJUDtBQUNDLFFBQUtXLFdBQUwsR0FBbUJYLE1BQW5CO0FBQ0E7QUFDQSxRQUFLa0QsdUJBQUwsQ0FBNkJsRCxNQUE3QjtBQUVBO0FBM0lNLEVBOURnQjs7QUErTXhCdUQsUUEvTXdCLHFCQStNZjtBQUNSLE9BQUtMLHVCQUFMLENBQTZCLFNBQTdCO0FBQ0EsRUFqTnVCO0FBbU54Qk0sUUFuTndCLHFCQW1OZjtBQUFBOztBQUNQM0MsSUFBRSxlQUFGLEVBQW1CNEMsRUFBbkIsQ0FBc0IsZ0JBQXRCLEVBQXdDLFlBQU07QUFDeEMsVUFBS3BELFFBQUwsR0FBZ0IsQ0FBaEI7QUFDQXFELFNBQU03QyxFQUFFLG9FQUFtRSxPQUFLOEMsS0FBeEUsR0FBK0UsSUFBakYsRUFBdUZGLEVBQXZGLENBQTBGLE1BQTFGLEVBQWtHLFlBQU07QUFDNUcsUUFBSUcsWUFBWS9DLEVBQUUsZUFBYyxPQUFLOEMsS0FBbkIsR0FBMEIseUNBQTVCLENBQWhCO0FBQ0FDLGNBQVVDLFFBQVYsQ0FBbUIsZ0JBQW5CO0FBQ0EsV0FBSzFELFVBQUwsR0FBa0J5RCxVQUFVRSxNQUFWLEVBQWxCO0FBQ0EsV0FBSzFELFNBQUwsR0FBaUJ3RCxVQUFVRyxLQUFWLEVBQWpCOztBQUVBSCxjQUFVSSxTQUFWO0FBRUQsSUFSSyxDQUFOO0FBU0gsR0FYSDs7QUFhRW5ELElBQUUsZUFBRixFQUFtQjRDLEVBQW5CLENBQXNCLGVBQXRCLEVBQXVDLFlBQU07QUFDekM1QyxLQUFFLGdCQUFGLEVBQW9Cb0QsS0FBcEI7QUFDQSxVQUFLM0QsWUFBTCxHQUFvQixLQUFwQjtBQUNBLFVBQUtQLFFBQUwsR0FBZ0IsS0FBaEI7QUFDSCxHQUpEO0FBS0gsRUF0T3VCOzs7QUF3T3hCbUUsV0FBUztBQUNSUCxPQURRLG1CQUNBO0FBQ1AsT0FBSSxLQUFLNUQsUUFBVCxFQUFtQjtBQUNsQixTQUFLTyxZQUFMLEdBQW9CLElBQXBCO0FBQ0EsV0FBTyxtQ0FBbUMsS0FBS1AsUUFBL0M7QUFDQTtBQUNELFFBQUtPLFlBQUwsR0FBb0IsS0FBcEI7QUFDQSxVQUFPLEtBQVA7QUFDQTtBQVJPOztBQXhPZSxDQUFSLENBQWpCLEMiLCJmaWxlIjoianMvZXdhbGxldC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMjY1KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBiMjBhMmEwYjI5NTZhOWQ2YzBkMSIsInZhciBjdXJyZW50VGFibGUgPSBudWxsO1xyXG52YXIgY3VycmVudFRyYW5zYWN0aW9uID0gbnVsbDtcclxudmFyIEV3YWxsZXRBcHAgPSBuZXcgVnVlKHtcclxuXHRlbDogJyNld2FsbGV0LXJvb3QnLFxyXG5cdFxyXG5cdGRhdGE6e1xyXG5cdFx0dHJhbnNhY3Rpb25zOltdLFxyXG5cdFx0ZmlsdGVyVHlwZTonYWxsJyxcclxuXHRcdGZpbHRlclN0YXR1czonYWxsJyxcclxuXHRcdGZpbGVEZXBvc2l0U2xpcDonJyxcclxuXHRcdGZpbGVuYW1lOicnLFxyXG5cdFx0c3RhdHVzOicnLFxyXG5cdFx0cmVhc29uOicnLFxyXG5cdFx0bmV3U3RhdHVzOicnLFxyXG5cclxuXHRcdGJhc2VIZWlnaHQgOjAsXHJcblx0ICAgIGJhc2VXaWR0aCA6MCxcclxuXHQgICAgYmFzZVpvb20gOiAxLFxyXG5cdFx0c2hvd1pvb21hYmxlIDogZmFsc2UsXHJcblxyXG5cdFx0cGVuZGluZ19jb3VudDowLFxyXG5cdFx0cHJvY2Vzc2luZ19jb3VudDowLFxyXG5cdFx0Y2FuY2VsbGVkX2NvdW50OjAsXHJcblx0XHRjb21wbGV0ZWRfY291bnQ6MCxcclxuXHRcdHN0YXR1c19zZWVuOidQZW5kaW5nJ1xyXG5cdH0sXHJcblxyXG5cdHdhdGNoOntcclxuXHRcdHRyYW5zYWN0aW9uczpmdW5jdGlvbigpe1xyXG5cdCAgXHRcdCQoJy5kdC1ld2FsbGV0JykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xyXG5cclxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xyXG5cdFx0XHRcdGN1cnJlbnRUYWJsZSA9ICQoJy5kdC1ld2FsbGV0JykuRGF0YVRhYmxlKHtcclxuXHRcdFx0ICAgICAgICBwYWdlTGVuZ3RoOiAxMCxcclxuXHRcdFx0ICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxyXG5cdFx0XHQgICAgfSk7XHJcblx0XHRcdH0sNTAwKTtcclxuXHRcdH0sXHJcblxyXG5cdFx0ZmlsdGVyVHlwZTogZnVuY3Rpb24oKXtcclxuXHRcdFx0JCgnLmR0LWV3YWxsZXQnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XHJcblxyXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcblx0XHRcdFx0Y3VycmVudFRhYmxlID0gJCgnLmR0LWV3YWxsZXQnKS5EYXRhVGFibGUoe1xyXG5cdFx0XHQgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxyXG5cdFx0XHQgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXHJcblx0XHRcdCAgICB9KTtcclxuXHRcdFx0fSw1MDApO1xyXG5cdFx0fSxcclxuXHRcdFxyXG5cdFx0YmFzZVpvb206ZnVuY3Rpb24obmV3VmFsdWUpe1xyXG5cdFx0XHR2YXIgY2hhbmdlZEhlaWdodFNpemUgPSAgdGhpcy5iYXNlSGVpZ2h0ICogbmV3VmFsdWU7XHJcblx0XHRcdHZhciBjaGFuZ2VkV2lkdGhTaXplID0gdGhpcy5iYXNlV2lkdGggKiBuZXdWYWx1ZTtcclxuXHRcdFx0JCgnI2RvYy1jb250YWluZXInKS5maW5kKCdpbWcnKS5jc3MoJ2hlaWdodCcsY2hhbmdlZEhlaWdodFNpemUpO1xyXG5cdFx0XHQkKCcjZG9jLWNvbnRhaW5lcicpLmZpbmQoJ2ltZycpLmNzcygnd2lkdGgnLGNoYW5nZWRXaWR0aFNpemUpO1xyXG5cdFx0XHQkKCcjZG9jLWNvbnRhaW5lcicpLmZpbmQoJ2ltZycpLnJlbW92ZUNsYXNzKCdpbWctcmVzcG9uc2l2ZScpO1xyXG5cdFx0fSxcclxuXHRcdC8vIGN1cnJlbnRfdHJhbnNhY3Rpb25faW1hZ2U6ZnVuY3Rpb24oKXtcclxuXHRcdC8vIFx0dGhpcy5iYXNlWm9vbSA9IDE7XHJcblx0XHQvLyB9LFxyXG5cdH0sXHJcblxyXG5cclxuXHJcblx0bWV0aG9kczp7XHJcblx0XHRcclxuXHRcdGNoYW5nZVN0YXR1cygpe1xyXG5cdFx0XHRpZih0aGlzLm5ld1N0YXR1cyA9PSAnQ2FuY2VsbGVkJylcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGlmKHRoaXMucmVhc29uID09ICcnIHx8IHRoaXMucmVhc29uID09IG51bGwpXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0dG9hc3RyLmVycm9yKCcnLCdSZWFzb24gaXMgcmVxdWlyZWQgZm9yIGNhbmNlbGxpbmcgRXdhbGxldCBSZXF1ZXN0cycpO1xyXG5cdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fWVsc2VcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRoaXMucmVhc29uID0gJyc7XHJcblx0XHRcdH1cclxuXHRcdFx0YXhpb3NBUEl2MVxyXG5cdFx0XHRcdC5wdXQoJy9ld2FsbGV0LXRyYW5zYWN0aW9uLycrY3VycmVudFRyYW5zYWN0aW9uLHtcclxuXHRcdFx0XHRcdHN0YXR1czp0aGlzLm5ld1N0YXR1cyxcclxuXHRcdFx0XHRcdHJlYXNvbjp0aGlzLnJlYXNvblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdFx0aWYocmVzcG9uc2Uuc3RhdHVzID09IDIwMClcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGhpcy50cmFuc2FjdGlvbnMuZmlsdGVyKG9iaiA9PiB7XHJcblx0XHRcdFx0XHRcdFx0aWYob2JqLmlkID09IGN1cnJlbnRUcmFuc2FjdGlvbilcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRvYmouc3RhdHVzID0gdGhpcy5uZXdTdGF0dXM7XHJcblx0XHRcdFx0XHRcdFx0XHRvYmoudXBkYXRlZF9hdCA9IHJlc3BvbnNlLmRhdGEuZGF0YS51cGRhdGVkX2F0O1xyXG5cdFx0XHRcdFx0XHRcdFx0bW9kYWw9b2JqLnR5cGU9PSdEZXBvc2l0Jz8nZGVwb3NpdE1vZGFsJzonZGVjaXNpb25Nb2RhbCc7XHJcblx0XHRcdFx0XHRcdFx0XHR0b2FzdHIuc3VjY2VzcygnU3RhdHVzIFVwZGF0ZWQhJyk7XHJcblx0XHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHRcdCQoJy5kdC1ld2FsbGV0JykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xyXG5cclxuXHRcdFx0XHRcdFx0XHRcdHRoaXMuc2hvd1RyYW5zYWN0aW9uQnlTdGF0dXModGhpcy5zdGF0dXNfc2Vlbik7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRjdXJyZW50VGFibGUgPSAkKCcuZHQtZXdhbGxldCcpLkRhdGFUYWJsZSh7XHJcblx0XHRcdFx0XHRcdFx0XHQgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxyXG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdFx0ICAgIH0pO1xyXG5cdFx0XHRcdFx0XHRcdFx0fSw1MDApO1xyXG5cclxuXHRcdFx0XHRcdFx0XHRcdCQoJyMnK21vZGFsKS5tb2RhbCgndG9nZ2xlJyk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KVxyXG5cdFx0fSxcclxuXHJcblx0XHRzdGF0dXNDb2xvcihzdGF0dXMpe1xyXG5cdFx0XHRzd2l0Y2goc3RhdHVzKXtcclxuXHRcdFx0XHRjYXNlICdQZW5kaW5nJzogcmV0dXJuICdsYWJlbC13YXJuaW5nJzsgYnJlYWs7XHJcblx0XHRcdFx0Y2FzZSAnUHJvY2Vzc2luZyc6IHJldHVybiAnbGFiZWwtZGVmYXVsdCc7IGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgJ0NvbXBsZXRlZCc6IHJldHVybiAnbGFiZWwtcHJpbWFyeSc7IGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgJ0NhbmNlbGxlZCc6IHJldHVybiAnbGFiZWwtZGFuZ2VyJzsgYnJlYWs7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblxyXG5cdFx0c2hvd0ZpbHRlclN0YXR1cyhzdGF0dXMpe1xyXG5cdFx0XHRpZih0aGlzLmZpbHRlclN0YXR1cyAhPSAnYWxsJylcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHJldHVybiB0aGlzLmZpbHRlclN0YXR1cyA9PSBzdGF0dXMudG9Mb3dlckNhc2UoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH0sXHJcblxyXG5cdFx0c2hvd0ZpbHRlclR5cGUodHlwZSl7XHJcblx0XHRcdGlmKHRoaXMuZmlsdGVyVHlwZSAhPSAnYWxsJylcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHJldHVybiB0aGlzLmZpbHRlclR5cGUgPT09IHR5cGUudG9Mb3dlckNhc2UoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH0sXHJcblxyXG5cdFx0c2hvd01vZGFsKGlkKXtcclxuXHRcdFx0Y3VycmVudFRyYW5zYWN0aW9uID0gaWQ7XHJcblx0XHRcdHZhciB0cmFuc2FjdGlvbiA9IHRoaXMudHJhbnNhY3Rpb25zLmZpbHRlcihvYmogPT4ge1xyXG5cdFx0XHRcdHJldHVybiBvYmouaWQgPT0gaWQ7XHJcblx0XHRcdH0pXHJcblxyXG5cdFx0XHRpZih0cmFuc2FjdGlvbi5sZW5ndGgpXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0cmFuc2FjdGlvbiA9IHRyYW5zYWN0aW9uWzBdO1xyXG5cdFx0XHRcdC8vIGlmKHRyYW5zYWN0aW9uLnN0YXR1cyA9PSAnQ29tcGxldGVkJyB8fCB0cmFuc2FjdGlvbi5zdGF0dXMgPT0gJ0NhbmNlbGxlZCcgfHwgdHJhbnNhY3Rpb24udHlwZSA9PSAnVHJhbnNmZXInKVxyXG5cdFx0XHRcdC8vIHtcclxuXHRcdFx0XHQvLyBcdHJldHVybiB0cnVlO1xyXG5cdFx0XHRcdC8vIH1cclxuXHRcdFx0XHR0aGlzLnJlYXNvbiA9IHRyYW5zYWN0aW9uLnJlYXNvbjtcclxuXHRcdFx0XHRtb2RhbD10cmFuc2FjdGlvbi50eXBlPT0nRGVwb3NpdCc/J2RlcG9zaXRNb2RhbCc6J2RlY2lzaW9uTW9kYWwnO1xyXG5cdFx0XHRcdGlmKHRyYW5zYWN0aW9uLmRlcG9zaXRfc2xpcClcclxuXHRcdFx0XHR7XHJcblx0XHRcdFx0XHR0aGlzLmZpbGVuYW1lID0gdHJhbnNhY3Rpb24uZGVwb3NpdF9zbGlwO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHR0aGlzLm5ld1N0YXR1cyA9IHRyYW5zYWN0aW9uLnN0YXR1cztcclxuXHJcblx0XHRcdFx0JCgnIycrbW9kYWwpLm1vZGFsKCd0b2dnbGUnKTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHR0ZXh0Q29sb3Ioc3RhdHVzKXtcclxuXHRcdFx0c3dpdGNoKHN0YXR1cylcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGNhc2UgJ1BlbmRpbmcnOiByZXR1cm4gJ3Jvdy1wZW5kaW5nJzsgYnJlYWs7XHJcblx0XHRcdFx0Y2FzZSAnQ29tcGxldGVkJzogcmV0dXJuICdyb3ctY29tcGxldGVkJzsgYnJlYWs7XHJcblx0XHRcdFx0Y2FzZSAnUHJvY2Vzc2luZyc6IHJldHVybiAncm93LXByb2Nlc3NpbmcnOyBicmVhaztcclxuXHRcdFx0XHRjYXNlICdDYW5jZWxsZWQnOiByZXR1cm4gJ3Jvdy1jYW5jZWxsZWQnOyBicmVhaztcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHRnZXRUcmFuc2FjdGlvbnNCeVN0YXR1cyhzdGF0dXMpXHJcblx0XHR7XHJcblx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvZXdhbGxldC10cmFuc2FjdGlvbicpXHJcblx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcclxuXHRcdFx0XHRpZihyZXNwb25zZS5zdGF0dXMgPT0gMjAwKVxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdGlmKHN0YXR1cyAhPSAnUHJvY2Vzc2luZycpXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRoaXMudHJhbnNhY3Rpb25zID0gcmVzcG9uc2UuZGF0YS5maWx0ZXIob2JqID0+IHtyZXR1cm4gb2JqLnN0YXR1cyA9PSBzdGF0dXN9KTtcclxuXHRcdFx0XHRcdH1lbHNlXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRoaXMudHJhbnNhY3Rpb25zID0gcmVzcG9uc2UuZGF0YS5maWx0ZXIob2JqID0+IHtyZXR1cm4gb2JqLnN0YXR1cyA9PSAnUHJvY2Vzc2luZycgJiYgb2JqLnByb2Nlc3Nvcl9pZCA9PSBMYXJhdmVsLnVzZXIuaWR9KTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHR0aGlzLnBlbmRpbmdfY291bnQgPSByZXNwb25zZS5kYXRhLmZpbHRlcihvYmogPT4ge3JldHVybiBvYmouc3RhdHVzID09ICdQZW5kaW5nJ30pLmxlbmd0aDtcclxuXHRcdFx0XHRcdHRoaXMucHJvY2Vzc2luZ19jb3VudCA9IHJlc3BvbnNlLmRhdGEuZmlsdGVyKG9iaiA9PiB7cmV0dXJuIG9iai5zdGF0dXMgPT0gJ1Byb2Nlc3NpbmcnICAmJiBvYmoucHJvY2Vzc29yX2lkID09IExhcmF2ZWwudXNlci5pZH0pLmxlbmd0aDtcclxuXHRcdFx0XHRcdHRoaXMuY2FuY2VsbGVkX2NvdW50ID0gcmVzcG9uc2UuZGF0YS5maWx0ZXIob2JqID0+IHtyZXR1cm4gb2JqLnN0YXR1cyA9PSAnQ2FuY2VsbGVkJ30pLmxlbmd0aDtcclxuXHRcdFx0XHRcdHRoaXMuY29tcGxldGVkX2NvdW50ID0gcmVzcG9uc2UuZGF0YS5maWx0ZXIob2JqID0+IHtyZXR1cm4gb2JqLnN0YXR1cyA9PSAnQ29tcGxldGVkJ30pLmxlbmd0aDtcclxuXHJcblxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSlcclxuXHRcdH0sXHJcblxyXG5cdFx0c2hvd1RyYW5zYWN0aW9uQnlTdGF0dXMoc3RhdHVzKVxyXG5cdFx0e1xyXG5cdFx0XHR0aGlzLnN0YXR1c19zZWVuID0gc3RhdHVzO1xyXG5cdFx0XHQvLyAkKCcuZHQtZXdhbGxldCcpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcclxuXHRcdFx0dGhpcy5nZXRUcmFuc2FjdGlvbnNCeVN0YXR1cyhzdGF0dXMpO1xyXG5cclxuXHRcdH0sXHJcblxyXG5cclxuXHJcblx0fSxcclxuXHJcblx0Y3JlYXRlZCgpe1xyXG5cdFx0dGhpcy5nZXRUcmFuc2FjdGlvbnNCeVN0YXR1cygnUGVuZGluZycpO1xyXG5cdH0sXHJcblxyXG5cdG1vdW50ZWQoKXtcclxuXHRcdCAkKCcjZGVwb3NpdE1vZGFsJykub24oJ3Nob3duLmJzLm1vZGFsJywgKCkgPT4ge1xyXG5cdCAgICAgICAgdGhpcy5iYXNlWm9vbSA9IDE7XHJcblx0ICAgICAgICBpbWcgPSAkKCc8aW1nIGlkPVwic2hpdHR5aW1hZ2VcIiBjbGFzcz1cImNlbnRlci1ibG9jayBpbWctcmVzcG9uc2l2ZVwiIHNyYz1cIicrIHRoaXMuaW1nRFMgKydcIj4nKS5vbignbG9hZCcsICgpID0+IHtcclxuXHQgICAgICAgICAgdmFyICRtb2RhbEltZyA9ICQoJzxpbWcgc3JjPVwiJysgdGhpcy5pbWdEUyArJ1wiIGNsYXNzPVwiY2VudGVyLWJsb2NrIGltZy1yZXNwb25zaXZlXCIgPicpO1xyXG5cdCAgICAgICAgICAkbW9kYWxJbWcuYXBwZW5kVG8oJyNkb2MtY29udGFpbmVyJyk7XHJcblx0ICAgICAgICAgIHRoaXMuYmFzZUhlaWdodCA9ICRtb2RhbEltZy5oZWlnaHQoKTtcclxuXHQgICAgICAgICAgdGhpcy5iYXNlV2lkdGggPSAkbW9kYWxJbWcud2lkdGgoKTtcclxuXHJcblx0ICAgICAgICAgICRtb2RhbEltZy5kcmFnZ2FibGUoKTtcclxuXHQgICAgICAgICAgXHJcblx0ICAgICAgICB9KTtcclxuXHQgICAgfSk7XHJcblxyXG5cdCAgICAkKCcjZGVwb3NpdE1vZGFsJykub24oJ2hpZGUuYnMubW9kYWwnLCAoKSA9PiB7XHJcblx0ICAgICAgICAkKCcjZG9jLWNvbnRhaW5lcicpLmVtcHR5KCk7XHJcblx0ICAgICAgICB0aGlzLnNob3dab29tYWJsZSA9IGZhbHNlO1xyXG5cdCAgICAgICAgdGhpcy5maWxlbmFtZSA9IGZhbHNlO1xyXG5cdCAgICB9KTtcclxuXHR9LFxyXG5cclxuXHRjb21wdXRlZDp7XHJcblx0XHRpbWdEUygpIHtcclxuXHRcdFx0aWYgKHRoaXMuZmlsZW5hbWUpIHtcclxuXHRcdFx0XHR0aGlzLnNob3dab29tYWJsZSA9IHRydWU7XHJcblx0XHRcdFx0cmV0dXJuICcvc2hvcHBpbmcvaW1hZ2VzL2RlcG9zaXQtc2xpcC8nICsgdGhpcy5maWxlbmFtZTtcclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLnNob3dab29tYWJsZSA9IGZhbHNlO1xyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHRcclxuXHRcdH0sXHJcblx0fSxcclxuXHJcblx0XHJcbn0pXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvcGFnZXMvZXdhbGxldC5qcyJdLCJzb3VyY2VSb290IjoiIn0=