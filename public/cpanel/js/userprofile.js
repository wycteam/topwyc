/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 471);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['groupreport', 'groupid', 'clientsid', 'clientservices', 'trackings', 'iswritereportpage'],

	data: function data() {
		return {

			docs: [],
			serviceDocs: [],

			multipleQuickReportForm: new Form({
				distinctClientServices: [],

				groupReport: null,
				groupId: null,

				clientsId: [],
				clientServicesId: [],
				trackings: [],

				actionId: [],

				categoryId: [],
				categoryName: [],

				date1: [],
				date2: [],
				date3: [],
				dateTimeArray: [],

				extraDetails: []
			}, { baseURL: axiosAPIv1.defaults.baseURL })

		};
	},


	methods: {
		fetchDocs: function fetchDocs(id) {
			var _this = this;

			var ids = '';
			$.each(id, function (i, item) {
				ids += id[i].docs_needed + ',' + id[i].docs_optional + ',';
			});

			var uniqueList = ids.split(',').filter(function (allItems, i, a) {
				return i == a.indexOf(allItems);
			}).join(',');

			axios.get('/visa/group/' + uniqueList + '/service-docs').then(function (result) {
				_this.docs = result.data;
			});

			setTimeout(function () {
				$('.chosen-select-for-docs').trigger('chosen:updated');
			}, 5000);
		},
		getServiceDocs: function getServiceDocs() {
			var _this2 = this;

			axios.get('/visa/group/service-docs-index').then(function (result) {
				_this2.serviceDocs = result.data;
			});
		},
		resetMultipleQuickReportForm: function resetMultipleQuickReportForm() {
			var _this3 = this;

			// MultipleQuickReportForm.vue
			this.distinctClientServices.forEach(function (distinctClientService, index) {
				var ref = 'form-' + index;

				_this3.$refs[ref][0].multipleQuickReportform = {
					actionId: 1,

					categoryId: null,
					categoryName: null,

					date1: null,
					date2: null,
					date3: null,
					dateTimeArray: [],

					extraDetails: []
				};

				_this3.$refs[ref][0].$refs.date2.value = '';
				_this3.$refs[ref][0].$refs.date3.value = '';
				_this3.$refs[ref][0].$refs.date4.value = '';
				_this3.$refs[ref][0].$refs.date5.value = '';
				_this3.$refs[ref][0].$refs.date6.value = '';
				_this3.$refs[ref][0].$refs.date7.value = '';
				_this3.$refs[ref][0].$refs.date8.value = '';
				_this3.$refs[ref][0].$refs.date9.value = '';
				_this3.$refs[ref][0].$refs.date10.value = '';
				_this3.$refs[ref][0].$refs.date11.value = '';
				_this3.$refs[ref][0].$refs.date12.value = '';
				_this3.$refs[ref][0].$refs.date13.value = '';
				_this3.$refs[ref][0].$refs.date14.value = '';
				_this3.$refs[ref][0].$refs.date15.value = '';
				_this3.$refs[ref][0].$refs.date16.value = '';
				_this3.$refs[ref][0].$refs.categoryName.value = '';

				_this3.$refs[ref][0].customCategory = false;
			});

			// MultipleQuickReport.vue
			this.multipleQuickReportForm = new Form({
				distinctClientServices: [],

				actionId: [],

				categoryId: [],
				categoryName: [],

				date1: [],
				date2: [],
				date3: [],
				dateTimeArray: [],

				extraDetails: []
			}, { baseURL: axiosAPIv1.defaults.baseURL });
		},
		validate: function validate() {
			var _this4 = this;

			var isValid = true;

			this.distinctClientServices.forEach(function (distinctClientService, index) {
				var ref = 'form-' + index;

				if (!_this4.$refs[ref][0].validate(distinctClientService)) {
					isValid = false;
				}
			});

			return isValid;
		},
		addMultipleQuickReport: function addMultipleQuickReport() {
			var _this5 = this;

			if (this.validate()) {
				this.multipleQuickReportForm.groupReport = this.groupreport;
				this.multipleQuickReportForm.groupId = this.groupid;
				this.multipleQuickReportForm.clientsId = this.clientsid;
				this.multipleQuickReportForm.clientServicesId = this.clientservices;
				this.multipleQuickReportForm.trackings = this.trackings;
				this.multipleQuickReportForm.distinctClientServices = this.distinctClientServices;

				this.distinctClientServices.forEach(function (distinctClientService, index) {
					var ref = 'form-' + index;

					var _actionId = _this5.$refs[ref][0].multipleQuickReportform.actionId;
					var _categoryId = _this5.$refs[ref][0].multipleQuickReportform.categoryId;
					var _categoryName = _this5.$refs[ref][0].multipleQuickReportform.categoryName;
					var _date1 = _this5.$refs[ref][0].multipleQuickReportform.date1;
					var _date2 = _this5.$refs[ref][0].multipleQuickReportform.date2;
					var _date3 = _this5.$refs[ref][0].multipleQuickReportform.date3;
					var _dateTimeArray = _this5.$refs[ref][0].multipleQuickReportform.dateTimeArray;
					var _extraDetails = _this5.$refs[ref][0].multipleQuickReportform.extraDetails;

					_this5.multipleQuickReportForm.actionId.push(_actionId);
					_this5.multipleQuickReportForm.categoryId.push(_categoryId);
					_this5.multipleQuickReportForm.categoryName.push(_categoryName);
					_this5.multipleQuickReportForm.date1.push(_date1);
					_this5.multipleQuickReportForm.date2.push(_date2);
					_this5.multipleQuickReportForm.date3.push(_date3);
					_this5.multipleQuickReportForm.dateTimeArray.push(_dateTimeArray);
					_this5.multipleQuickReportForm.extraDetails.push(_extraDetails);
				});

				this.multipleQuickReportForm.submit('post', '/visa/quick-report').then(function (response) {
					if (response.success) {
						_this5.resetMultipleQuickReportForm();

						$('#add-quick-report-modal').modal('hide');

						toastr.success(response.message);

						if (_this5.iswritereportpage) {
							$('#add-quick-report-modal').on('hidden.bs.modal', function (e) {
								window.location.href = '/visa/report/write-report';
							});
						} else {
							setTimeout(function () {
								location.reload();
							}, 1000);
						}
					}
				});
			}
		}
	},

	created: function created() {
		this.getServiceDocs();
	},


	computed: {
		_isWriteReportPage: function _isWriteReportPage() {
			return this.iswritereportpage || false;
		},
		distinctClientServices: function distinctClientServices() {
			var distinctClientServices = [];

			this.clientservices.forEach(function (clientService) {
				if (distinctClientServices.indexOf(clientService.detail) == -1) {
					distinctClientServices.push(clientService.detail);
				}
			});

			return distinctClientServices;
		}
	},

	components: {
		'multiple-quick-report-form': __webpack_require__(15)
	}

});

/***/ }),

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['index', 'docs', 'servicedocs'],

	data: function data() {
		return {

			actions: [],
			categories: [],

			companyCouriers: [],

			customCategory: false,

			multipleQuickReportform: {
				actionId: 1,

				categoryId: null,
				categoryName: null,

				date1: null,
				date2: null,
				date3: null,
				dateTimeArray: [],

				extraDetails: []
			}

		};
	},


	methods: {
		getActions: function getActions() {
			var _this = this;

			axios.get('/visa/actions').then(function (response) {
				_this.actions = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		getCompanyCouriers: function getCompanyCouriers() {
			var _this2 = this;

			var role = 'company-courier';

			axios.get('/visa/users-by-role/' + role).then(function (response) {
				_this2.companyCouriers = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		getCurrentDate: function getCurrentDate() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1;
			var yyyy = today.getFullYear();

			if (dd < 10) {
				dd = '0' + dd;
			}

			if (mm < 10) {
				mm = '0' + mm;
			}

			return mm + '/' + dd + '/' + yyyy;
		},
		changeActionHandler: function changeActionHandler(actionId) {
			// Reset
			this.multipleQuickReportform.date1 = null;
			this.multipleQuickReportform.date2 = null;
			this.multipleQuickReportform.date3 = null;
			this.multipleQuickReportform.dateTimeArray = [];
			this.multipleQuickReportform.extraDetails = [];

			// For auto capture date
			if (actionId == 1 || actionId == 3 || actionId == 4 || actionId == 7 || actionId == 11 || actionId == 15 || actionId == 16) {
				this.multipleQuickReportform.date1 = this.getCurrentDate();
			}
		},
		getCategories: function getCategories() {
			var _this3 = this;

			var actionId = this.multipleQuickReportform.actionId;

			this.changeActionHandler(actionId);

			this.categories = [];

			axios.get('/visa/categories-by-action-id', {
				params: {
					actionId: actionId
				}
			}).then(function (response) {
				_this3.categories = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		changeCategoryHandler: function changeCategoryHandler() {
			if (this.multipleQuickReportform.actionId == 1 && this.multipleQuickReportform.categoryId == 1) {
				this.addDateTimePickerArray();
			}
		},
		addDateTimePickerArray: function addDateTimePickerArray() {
			this.multipleQuickReportform.dateTimeArray.push({ value: null });

			setTimeout(function () {
				$('.datetimepickerarray').inputmask("datetime", {
					mask: "y-2-1 h:s:s",
					placeholder: "yyyy-mm-dd hh:mm:ss",
					leapday: "-02-29",
					separator: "-",
					alias: "yyyy-mm-dd"
				});
			}, 500);
		},
		removeDateTimePickerArray: function removeDateTimePickerArray(index) {
			this.multipleQuickReportform.dateTimeArray.splice(index, 1);
		},
		validate: function validate(distinctClientService) {
			var isValid = true;

			if (this.multipleQuickReportform.actionId == 1) {
				var date2 = this.$refs.date2.value;
				var date3 = this.$refs.date3.value ? this.$refs.date3.value : '';

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date2 == '') {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					var dateTimeArray = [];
					$('input[name^="dateTimeArray"]').each(function (e) {
						if (this.value != '') {
							dateTimeArray.push(this.value);
						}
					});

					this.multipleQuickReportform.date2 = date2;
					if (this.multipleQuickReportform.categoryId == 6 || this.multipleQuickReportform.categoryId == 7 || this.multipleQuickReportform.categoryId == 10 || this.multipleQuickReportform.categoryId == 14 || this.multipleQuickReportform.categoryId == 15) {
						this.multipleQuickReportform.date3 = date3;
					}

					this.multipleQuickReportform.dateTimeArray = dateTimeArray;
				}
			} else if (this.multipleQuickReportform.actionId == 2 || this.multipleQuickReportform.actionId == 3) {
				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				}
			} else if (this.multipleQuickReportform.actionId == 4) {
				var docs = [];
				$('#chosen-' + this.index + ' option:selected').each(function (e) {
					docs.push($(this).val());
				});

				if (docs.length == 0) {
					toastr.error('Please select documents.', distinctClientService);
					isValid = false;
				} else {
					$('#chosen-' + this.index).val('').trigger('chosen:updated');

					this.multipleQuickReportform.extraDetails = docs;
				}
			} else if (this.multipleQuickReportform.actionId == 6) {
				var date4 = this.$refs.date4.value;

				if (date4 == '') {
					toastr.error('Please fill up submission date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date4;
				}
			} else if (this.multipleQuickReportform.actionId == 7) {
				var date5 = this.$refs.date5.value;

				if (date5 == '') {
					toastr.error('Please fill up estimated time of finishing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date5;
				}
			} else if (this.multipleQuickReportform.actionId == 8) {
				var date6 = this.$refs.date6.value;
				var date7 = this.$refs.date7.value;
				var date8 = this.$refs.date8.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date6 == '' || date7 == '') {
					toastr.error('Please fill up affected date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date6;
					this.multipleQuickReportform.date2 = date7;
					this.multipleQuickReportform.date3 = date8;
				}
			} else if (this.multipleQuickReportform.actionId == 9) {
				if (this.customCategory) {
					var categoryName = this.$refs.categoryName.value;

					if (categoryName == '') {
						toastr.error('Please select category.', distinctClientService);
						isValid = false;
					} else {
						this.multipleQuickReportform.categoryId = 0;
						this.multipleQuickReportform.categoryName = categoryName;
					}
				}

				var date9 = this.$refs.date9.value;
				var date10 = this.$refs.date10.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date9 == '' || date10 == '') {
					toastr.error('Please fill up date range.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date9;
					this.multipleQuickReportform.date2 = date10;
				}
			} else if (this.multipleQuickReportform.actionId == 10) {
				var date11 = this.$refs.date11.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date11 == '') {
					toastr.error('Please fill up extended period for processing.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date11;
				}
			} else if (this.multipleQuickReportform.actionId == 11) {
				var date12 = this.$refs.date12.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date12 == '') {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date12;
				}
			} else if (this.multipleQuickReportform.actionId == 12) {
				var date13 = this.$refs.date13.value;

				if (date13 == '') {
					toastr.error('Please fill up date and time pick up.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date13;
				}
			} else if (this.multipleQuickReportform.actionId == 13) {
				var date14 = this.$refs.date14.value;

				if (date14 == '') {
					toastr.error('Please fill up date and time deliver.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date14;
				}
			} else if (this.multipleQuickReportform.actionId == 14) {
				var date15 = this.$refs.date15.value;
				var companyCourier = this.$refs.company_courier.value;
				var trackingNumber = this.$refs.tracking_number.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (this.multipleQuickReportform.categoryId == 128 && companyCourier == '') {
					toastr.error('Please select company courier.', distinctClientService);
					isValid = false;
				} else if (this.multipleQuickReportform.categoryId != 128 && trackingNumber == '') {
					toastr.error('Please fill up tracking number.', distinctClientService);
					isValid = false;
				} else if (date15 == '') {
					toastr.error('Please fill up date and time delivered.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date15;

					var extraDetail = this.multipleQuickReportform.categoryId == 128 ? companyCourier : trackingNumber;
					this.multipleQuickReportform.extraDetails = [extraDetail];
				}
			} else if (this.multipleQuickReportform.actionId == 15) {
				var date16 = this.$refs.date16.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date16 == '' && (this.multipleQuickReportform.categoryId == 76 || this.multipleQuickReportform.categoryId == 77 || this.multipleQuickReportform.categoryId == 78 || this.multipleQuickReportform.categoryId == 79 || this.multipleQuickReportform.categoryId == 80 || this.multipleQuickReportform.categoryId == 81 || this.multipleQuickReportform.categoryId == 82 || this.multipleQuickReportform.categoryId == 83)) {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date16;
				}
			} else if (this.multipleQuickReportform.actionId == 16) {
				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				}
			}

			return isValid;
		}
	},

	created: function created() {
		this.getActions();

		this.getCategories();

		this.getCompanyCouriers();
	},
	mounted: function mounted() {

		// Chosen
		$('.chosen-select-for-docs, .chosen-select-for-docs-2').chosen({
			width: "100%",
			no_results_text: "No result/s found.",
			search_contains: true
		});

		// DatePicker
		$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			autoclose: true,
			format: "mm/dd/yyyy",
			startDate: new Date()
		});

		// DateTime Picker
		$('.datetimepicker').inputmask("datetime", {
			mask: "y-2-1 h:s:s",
			placeholder: "yyyy-mm-dd hh:mm:ss",
			leapday: "-02-29",
			separator: "-",
			alias: "yyyy-mm-dd"
		});
	}
});

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-0f2f2b77] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-0f2f2b77] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["MultipleQuickReport.vue?35f996b9"],"names":[],"mappings":";AA4PA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"MultipleQuickReport.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"addMultipleQuickReport\">\n\n\t\t<div class=\"ios-scroll\">\n\t\t\t<div v-for=\"(distinctClientService, index) in distinctClientServices.length\">\n\n\t\t\t\t<div class=\"panel panel-default\">\n\t                <div class=\"panel-heading\">\n\t                    {{ distinctClientServices[index] }}\n\t                </div>\n\t                <div class=\"panel-body\">\n\t                    \n\t                \t<multiple-quick-report-form :key=\"index\" :ref=\"'form-' + index\" :docs=\"docs\" :servicedocs=\"serviceDocs\" :index=\"index\"></multiple-quick-report-form>\n\n\t                </div> <!-- panel-body -->\n\t            </div> <!-- panel -->\n\n\t\t\t</div> <!-- loop -->\n\t\t</div> <!-- ios-scroll -->\n\t\t\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Create Report</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\t</form>\n\n</template>\n\n<script>\n\n\texport default {\n\n\t\tprops: ['groupreport', 'groupid', 'clientsid', 'clientservices', 'trackings', 'iswritereportpage'],\n\n\t\tdata() {\n\t\t\treturn {\n\t\t\n\t\t\t\tdocs: [],\n\t\t\t\tserviceDocs: [],\n\n        \t\tmultipleQuickReportForm : new Form({\n        \t\t\tdistinctClientServices:[],\n\n        \t\t\tgroupReport: null,\n        \t\t\tgroupId: null,\n\n        \t\t\tclientsId: [],\n        \t\t\tclientServicesId: [],\n        \t\t\ttrackings: [],\n\n        \t\t\tactionId: [],\n\n        \t\t\tcategoryId: [],\n        \t\t\tcategoryName: [],\n\n        \t\t\tdate1: [],\n        \t\t\tdate2: [],\n        \t\t\tdate3: [],\n        \t\t\tdateTimeArray: [],\n\n        \t\t\textraDetails: []\n        \t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\n\t\t\tfetchDocs(id) {\n\t\t\t\tvar ids = '';\n\t\t\t\t$.each(id, function(i, item) {\n\t\t\t\t    ids += id[i].docs_needed + ',' + id[i].docs_optional + ',';\n\t\t\t\t});\n\n\t\t\t\tvar uniqueList=ids.split(',').filter(function(allItems,i,a){\n\t\t\t\t    return i==a.indexOf(allItems);\n\t\t\t\t}).join(',');\n\t        \t\n\t\t\t \taxios.get('/visa/group/'+uniqueList+'/service-docs')\n\t\t\t \t  .then(result => {\n\t\t            this.docs = result.data;\n\t\t        });\n\n\t        \tsetTimeout(() => {\n\t        \t\t$('.chosen-select-for-docs').trigger('chosen:updated');\t\n\t        \t}, 5000);\n\t\t\t},\n\n\t\t\tgetServiceDocs() {\n        \t\taxios.get('/visa/group/service-docs-index')\n\t\t\t \t  .then(result => {\n\t\t            this.serviceDocs = result.data;\n\t\t        });\n        \t},\n\n\t\t\tresetMultipleQuickReportForm() {\n\t\t\t\t// MultipleQuickReportForm.vue\n\t\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\t\tthis.$refs[ref][0].multipleQuickReportform = {\n\t\t        \t\t\tactionId: 1,\n\n\t\t        \t\t\tcategoryId: null,\n\t\t        \t\t\tcategoryName: null,\n\n\t\t        \t\t\tdate1: null,\n\t\t        \t\t\tdate2: null,\n\t\t        \t\t\tdate3: null,\n\t\t        \t\t\tdateTimeArray: [],\n\n\t\t        \t\t\textraDetails: []\n\t\t        \t\t}\n\n\t\t        \t\tthis.$refs[ref][0].$refs.date2.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date3.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date4.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date5.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date6.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date7.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date8.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date9.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date10.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date11.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date12.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date13.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date14.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date15.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date16.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.categoryName.value = '';\n\n\t\t        \t\tthis.$refs[ref][0].customCategory = false;\n\t\t\t\t\t});\n\n        \t\t// MultipleQuickReport.vue\n\t        \t\tthis.multipleQuickReportForm = new Form({\n\t        \t\t\tdistinctClientServices:[],\n\n\t        \t\t\tactionId: [],\n\n\t        \t\t\tcategoryId: [],\n\t        \t\t\tcategoryName: [],\n\n\t        \t\t\tdate1: [],\n\t        \t\t\tdate2: [],\n\t        \t\t\tdate3: [],\n\t        \t\t\tdateTimeArray: [],\n\n\t        \t\t\textraDetails: []\n\t        \t\t}, { baseURL: axiosAPIv1.defaults.baseURL });\n\t\t\t},\n\n\t\t\tvalidate() {\n\t\t\t\tvar isValid = true;\n\n\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\tif( ! this.$refs[ref][0].validate(distinctClientService) ) {\n\t\t\t\t\t\tisValid = false;\n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\treturn isValid;\n\t\t\t},\n\n\t\t\taddMultipleQuickReport() {\n\t\t\t\tif(this.validate()) {\n\t\t\t\t\tthis.multipleQuickReportForm.groupReport = this.groupreport;\n\t\t\t\t\tthis.multipleQuickReportForm.groupId = this.groupid;\n\t\t\t\t\tthis.multipleQuickReportForm.clientsId = this.clientsid;\n\t\t\t\t\tthis.multipleQuickReportForm.clientServicesId = this.clientservices;\n\t\t\t\t\tthis.multipleQuickReportForm.trackings = this.trackings;\n\t\t\t\t\tthis.multipleQuickReportForm.distinctClientServices = this.distinctClientServices;\n\n\t\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\t\tlet _actionId = this.$refs[ref][0].multipleQuickReportform.actionId;\n\t\t\t\t\t\tlet _categoryId = this.$refs[ref][0].multipleQuickReportform.categoryId;\n\t\t\t\t\t\tlet _categoryName = this.$refs[ref][0].multipleQuickReportform.categoryName;\n\t\t\t\t\t\tlet _date1 = this.$refs[ref][0].multipleQuickReportform.date1;\n\t\t\t\t\t\tlet _date2 = this.$refs[ref][0].multipleQuickReportform.date2;\n\t\t\t\t\t\tlet _date3 = this.$refs[ref][0].multipleQuickReportform.date3;\n\t\t\t\t\t\tlet _dateTimeArray = this.$refs[ref][0].multipleQuickReportform.dateTimeArray;\n\t\t\t\t\t\tlet _extraDetails = this.$refs[ref][0].multipleQuickReportform.extraDetails;\n\n\t\t\t\t\t\tthis.multipleQuickReportForm.actionId.push(_actionId);\n\t\t\t\t\t\tthis.multipleQuickReportForm.categoryId.push(_categoryId);\n\t        \t\t\tthis.multipleQuickReportForm.categoryName.push(_categoryName);\n\t        \t\t\tthis.multipleQuickReportForm.date1.push(_date1);\n\t        \t\t\tthis.multipleQuickReportForm.date2.push(_date2);\n\t        \t\t\tthis.multipleQuickReportForm.date3.push(_date3);\n\t        \t\t\tthis.multipleQuickReportForm.dateTimeArray.push(_dateTimeArray);\n\t        \t\t\tthis.multipleQuickReportForm.extraDetails.push(_extraDetails);\n\t\t\t\t\t});\n\n\t\t\t\t\tthis.multipleQuickReportForm.submit('post', '/visa/quick-report')\n\t\t\t            .then(response => {\n\t\t\t            \tif(response.success) {\n\t\t\t            \t\tthis.resetMultipleQuickReportForm();\n\t\t\t            \t\t\n\t\t\t            \t\t$('#add-quick-report-modal').modal('hide');\n\n\t\t\t            \t\ttoastr.success(response.message);\n\n\t\t\t            \t\tif(this.iswritereportpage) {\n\t\t\t            \t\t\t$('#add-quick-report-modal').on('hidden.bs.modal', function (e) {\n\t\t\t\t\t\t\t\t\t  \twindow.location.href = '/visa/report/write-report';\n\t\t\t\t\t\t\t\t\t});\n\t\t\t            \t\t} else {\n\t\t\t            \t\t\tsetTimeout(() => {\n\t\t\t\t            \t\t\tlocation.reload();\n\t\t\t\t            \t\t}, 1000);\n\t\t\t            \t\t}\n\t\t\t            \t}\n\t\t\t            });\n\t\t\t\t}\n\t\t\t}\n\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.getServiceDocs();\n\t\t},\n\n\t\tcomputed: {\n\t\t\t_isWriteReportPage() {\n\t\t      \treturn this.iswritereportpage || false;\n\t\t    },\n\n\t\t\tdistinctClientServices() {\n\t\t\t\tlet distinctClientServices = [];\n\n\t\t\t\tthis.clientservices.forEach(clientService => {\n\t\t\t\t\tif(distinctClientServices.indexOf(clientService.detail) == -1) {\n\t\t\t\t\t\tdistinctClientServices.push(clientService.detail);\n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\treturn distinctClientServices;\n\t\t\t}\n\t\t},\n\n\t\tcomponents: {\n\t        'multiple-quick-report-form': require('./MultipleQuickReportForm.vue')\n\t    }\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(9),
  /* template */
  __webpack_require__(17),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6634763e", Component.options)
  } else {
    hotAPI.reload("data-v-6634763e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(20)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(10),
  /* template */
  __webpack_require__(16),
  /* scopeId */
  "data-v-0f2f2b77",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/MultipleQuickReport.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] MultipleQuickReport.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f2f2b77", Component.options)
  } else {
    hotAPI.reload("data-v-0f2f2b77", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(11),
  /* template */
  __webpack_require__(18),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/MultipleQuickReportForm.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] MultipleQuickReportForm.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-eb2fda4a", Component.options)
  } else {
    hotAPI.reload("data-v-eb2fda4a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addMultipleQuickReport($event)
      }
    }
  }, [_c('div', {
    staticClass: "ios-scroll"
  }, _vm._l((_vm.distinctClientServices.length), function(distinctClientService, index) {
    return _c('div', [_c('div', {
      staticClass: "panel panel-default"
    }, [_c('div', {
      staticClass: "panel-heading"
    }, [_vm._v("\n                    " + _vm._s(_vm.distinctClientServices[index]) + "\n                ")]), _vm._v(" "), _c('div', {
      staticClass: "panel-body"
    }, [_c('multiple-quick-report-form', {
      key: index,
      ref: 'form-' + index,
      refInFor: true,
      attrs: {
        "docs": _vm.docs,
        "servicedocs": _vm.serviceDocs,
        "index": index
      }
    })], 1)])])
  }), 0), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Create Report")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0f2f2b77", module.exports)
  }
}

/***/ }),

/***/ 17:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6634763e", module.exports)
  }
}

/***/ }),

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.actionId),
      expression: "multipleQuickReportform.actionId"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "action"
    },
    on: {
      "change": [function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.multipleQuickReportform, "actionId", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }, _vm.getCategories]
    }
  }, _vm._l((_vm.actions), function(action) {
    return _c('option', {
      domProps: {
        "value": action.id
      }
    }, [_vm._v("\n\t\t           \t\t" + _vm._s(action.name) + "\n\t\t            ")])
  }), 0)])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 1 || _vm.multipleQuickReportform.actionId == 2 || _vm.multipleQuickReportform.actionId == 3 || _vm.multipleQuickReportform.actionId == 8 || _vm.multipleQuickReportform.actionId == 9 || _vm.multipleQuickReportform.actionId == 10 || _vm.multipleQuickReportform.actionId == 11 || _vm.multipleQuickReportform.actionId == 14 || _vm.multipleQuickReportform.actionId == 15 || _vm.multipleQuickReportform.actionId == 16),
      expression: "multipleQuickReportform.actionId == 1 || multipleQuickReportform.actionId == 2 || multipleQuickReportform.actionId == 3 || multipleQuickReportform.actionId == 8 || multipleQuickReportform.actionId == 9 || multipleQuickReportform.actionId == 10 || multipleQuickReportform.actionId == 11 || multipleQuickReportform.actionId == 14 || multipleQuickReportform.actionId == 15 || multipleQuickReportform.actionId == 16"
    }],
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.categoryId),
      expression: "multipleQuickReportform.categoryId"
    }, {
      name: "show",
      rawName: "v-show",
      value: (!_vm.customCategory),
      expression: "!customCategory"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "category"
    },
    on: {
      "change": [function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.multipleQuickReportform, "categoryId", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }, _vm.changeCategoryHandler]
    }
  }, _vm._l((_vm.categories), function(category) {
    return _c('option', {
      domProps: {
        "value": category.id
      }
    }, [_vm._v("\n\t\t            \t" + _vm._s(category.name) + "\n\t\t            ")])
  }), 0), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 9),
      expression: "multipleQuickReportform.actionId == 9"
    }]
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.categoryName),
      expression: "multipleQuickReportform.categoryName"
    }, {
      name: "show",
      rawName: "v-show",
      value: (_vm.customCategory),
      expression: "customCategory"
    }],
    ref: "categoryName",
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "placeholder": "Enter category"
    },
    domProps: {
      "value": (_vm.multipleQuickReportform.categoryName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.multipleQuickReportform, "categoryName", $event.target.value)
      }
    }
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-sm btn-primary pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.customCategory = !_vm.customCategory;
        _vm.multipleQuickReportform.categoryId = null
      }
    }
  }, [_c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (!_vm.customCategory),
      expression: "!customCategory"
    }]
  }, [_vm._v("Use Custom Category")]), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.customCategory),
      expression: "customCategory"
    }]
  }, [_vm._v("Use Predefined Category")])])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 1),
      expression: "multipleQuickReportform.actionId == 1"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(3), _vm._v(" "), _c('input', {
    ref: "date2",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date2",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 1),
      expression: "multipleQuickReportform.categoryId == 1"
    }],
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tScheduled Hearing Date and Time:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_vm._l((_vm.multipleQuickReportform.dateTimeArray), function(dateTimeElement, index) {
    return _c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "col-md-11"
    }, [_c('div', {
      staticClass: "input-group date",
      staticStyle: {
        "margin-bottom": "10px"
      }
    }, [_vm._m(4, true), _vm._v(" "), _c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dateTimeElement.value),
        expression: "dateTimeElement.value"
      }],
      staticClass: "form-control datetimepickerarray",
      attrs: {
        "type": "text",
        "name": "dateTimeArray[]",
        "placeholder": "yyyy-mm-dd hh:mm:ss",
        "autocomplete": "off"
      },
      domProps: {
        "value": (dateTimeElement.value)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(dateTimeElement, "value", $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "col-md-1 text-center"
    }, [_c('button', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (index != 0),
        expression: "index != 0"
      }],
      staticClass: "btn btn-danger btn-sm",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.removeDateTimePickerArray(index)
        }
      }
    }, [_c('i', {
      staticClass: "fa fa-times"
    })])])])
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-sm pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.addDateTimePickerArray
    }
  }, [_c('i', {
    staticClass: "fa fa-plus"
  }), _vm._v(" Add\n                    ")])], 2)]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 6 || _vm.multipleQuickReportform.categoryId == 7 || _vm.multipleQuickReportform.categoryId == 10 || _vm.multipleQuickReportform.categoryId == 14 || _vm.multipleQuickReportform.categoryId == 15),
      expression: "multipleQuickReportform.categoryId == 6 || multipleQuickReportform.categoryId == 7 || multipleQuickReportform.categoryId == 10 || multipleQuickReportform.categoryId == 14 || multipleQuickReportform.categoryId == 15"
    }],
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tScheduled Appointment Date and Time:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(5), _vm._v(" "), _c('input', {
    ref: "date3",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date3",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 2),
      expression: "multipleQuickReportform.actionId == 2"
    }]
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 3),
      expression: "multipleQuickReportform.actionId == 3"
    }]
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 4),
      expression: "multipleQuickReportform.actionId == 4"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-docs-2",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "id": 'chosen-' + _vm.index,
      "data-placeholder": "Select Docs",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.servicedocs), function(servicedoc) {
    return _c('option', {
      domProps: {
        "value": servicedoc.title
      }
    }, [_vm._v("\n\t\t                \t" + _vm._s((servicedoc.title).trim()) + "\n\t\t                ")])
  }), 0)])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 6),
      expression: "multipleQuickReportform.actionId == 6"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(8), _vm._v(" "), _c('input', {
    ref: "date4",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date4",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tConsequence of non submission:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Additional cost maybe charged."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Additional cost maybe charged.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Additional cost maybe charged.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tAdditional cost maybe charged.\n\t\t\t    \t\t")])]), _vm._v(" "), _c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Application maybe forfeited and has to re-apply."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Application maybe forfeited and has to re-apply.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Application maybe forfeited and has to re-apply.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tApplication maybe forfeited and has to re-apply.\n\t\t\t    \t\t")])]), _vm._v(" "), _c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Initial deposit maybe forfeited."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Initial deposit maybe forfeited.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Initial deposit maybe forfeited.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tInitial deposit maybe forfeited.\n\t\t\t    \t\t")])])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 7),
      expression: "multipleQuickReportform.actionId == 7"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(10), _vm._v(" "), _c('input', {
    ref: "date5",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date5",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 8),
      expression: "multipleQuickReportform.actionId == 8"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(11), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-daterange input-group"
  }, [_c('input', {
    ref: "date6",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date6",
      "autocomplete": "off"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("to")]), _vm._v(" "), _c('input', {
    ref: "date7",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date7",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tTarget Filling Date:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(12), _vm._v(" "), _c('input', {
    ref: "date8",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date8",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 9),
      expression: "multipleQuickReportform.actionId == 9"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(13), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-daterange input-group"
  }, [_c('input', {
    ref: "date9",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date9",
      "autocomplete": "off"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("to")]), _vm._v(" "), _c('input', {
    ref: "date10",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date10",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 10),
      expression: "multipleQuickReportform.actionId == 10"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(14), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(15), _vm._v(" "), _c('input', {
    ref: "date11",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date11",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 11),
      expression: "multipleQuickReportform.actionId == 11"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(16), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(17), _vm._v(" "), _c('input', {
    ref: "date12",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date12",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 12),
      expression: "multipleQuickReportform.actionId == 12"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(18), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(19), _vm._v(" "), _c('input', {
    ref: "date13",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date13",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 13),
      expression: "multipleQuickReportform.actionId == 13"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(20), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(21), _vm._v(" "), _c('input', {
    ref: "date14",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date14",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 14),
      expression: "multipleQuickReportform.actionId == 14"
    }]
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 128),
      expression: "multipleQuickReportform.categoryId == 128"
    }],
    staticClass: "form-group"
  }, [_vm._m(22), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    ref: "company_courier",
    staticClass: "form-control",
    attrs: {
      "id": "company_courier"
    }
  }, _vm._l((_vm.companyCouriers), function(companyCourier) {
    return _c('option', {
      domProps: {
        "value": companyCourier.address.contact_number
      }
    }, [_vm._v("\n\t\t\t        \t\t" + _vm._s(companyCourier.first_name + ' ' + companyCourier.last_name) + " \n\t\t\t        \t\t(" + _vm._s(companyCourier.address.contact_number) + ")\n\t\t\t        \t")])
  }), 0)])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId != 128),
      expression: "multipleQuickReportform.categoryId != 128"
    }],
    staticClass: "form-group"
  }, [_vm._m(23), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    ref: "tracking_number",
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "tracking_number"
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(24), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(25), _vm._v(" "), _c('input', {
    ref: "date15",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date15",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 15),
      expression: "multipleQuickReportform.actionId == 15"
    }]
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 76 || _vm.multipleQuickReportform.categoryId == 77 || _vm.multipleQuickReportform.categoryId == 78 || _vm.multipleQuickReportform.categoryId == 79 || _vm.multipleQuickReportform.categoryId == 80 || _vm.multipleQuickReportform.categoryId == 81 || _vm.multipleQuickReportform.categoryId == 82 || _vm.multipleQuickReportform.categoryId == 83),
      expression: "multipleQuickReportform.categoryId == 76 || multipleQuickReportform.categoryId == 77 || multipleQuickReportform.categoryId == 78 || multipleQuickReportform.categoryId == 79 || multipleQuickReportform.categoryId == 80 || multipleQuickReportform.categoryId == 81 || multipleQuickReportform.categoryId == 82 || multipleQuickReportform.categoryId == 83"
    }],
    staticClass: "form-group"
  }, [_vm._m(26), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(27), _vm._v(" "), _c('input', {
    ref: "date16",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date16",
      "autocomplete": "off"
    }
  })])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Action: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Category: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDocuments: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tSubmission Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Time of Finishing: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tAffected Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate Range: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tExtended period for processing: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Pick Up: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Deliver: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tCompany Courier: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tTracking Number: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Delivered: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-eb2fda4a", module.exports)
  }
}

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_multiselect__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_multiselect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_multiselect__);


Vue.component('modals', __webpack_require__(332));
Vue.component('dialog-modal', __webpack_require__(13));
// Vue.component('row-document',  require('../components/Documents/DocumentContainer.vue'));
Vue.component('package-item', __webpack_require__(363));
Vue.component('service-item', __webpack_require__(364));
Vue.component('action-logs', __webpack_require__(29));
Vue.component('transaction-logs', __webpack_require__(30));
Vue.component('add-new-service', __webpack_require__(361));
Vue.component('edit-service', __webpack_require__(362));

var data = window.Laravel.data;
var add = data.address != null ? data.address.address : null;
var city = data.address != null ? data.address.city : null;
if (city != null) {
	add = add + "," + city;
}
var province = data.address != null ? data.address.province : null;
if (province != null) {
	add = add + "," + province;
}
var numb = data.address != null ? data.address.contact_number : null;
var client_id = data.id;
var user = window.Laravel.user;
var ClientApp = new Vue({
	el: '#userprofile',
	data: {
		clients: [],
		processor: user,
		is_auth: false,
		authorizers: [],
		usr_id: data.id,
		rcvs: data.document_receive,
		fullname: data.full_name,
		first_name: data.first_name,
		last_name: data.last_name,
		avatar: data.avatar,
		ewallet_balance: 0,
		email: data.email,
		bday: data.birth_date,
		gender: data.gender,
		cstatus: data.civil_status,
		passport: data.passport,
		height: data.height,
		weight: data.weight,
		unread_notif: data.unread_notif,
		group_binded: data.group_binded,
		binded: data.binded,
		address: data.addresses,
		add: add,
		number: numb,
		is_verified: data.is_verified,
		is_docs_verified: data.is_docs_verified,
		service_cost: null,
		deposit: null,
		payment: null,
		refund: null,
		discount: null,
		balance: null,
		transfer: null,
		packages: [],
		services: [],
		package_date: null,
		package_cost: null,
		package_deposit: null,
		package_payment: null,
		package_refund: null,
		package_discount: null,
		package_balance: null,
		tracking_selected: null,
		group_package: false,
		tracking_status: null,

		actionlogs: [],
		current_year: null,
		current_month: null,
		current_day: null,
		c_year: null,
		c_month: null,
		c_day: null,
		s_count: 1,
		log_count: 0,

		transactionlogs: [],
		current_year2: null,
		current_month2: null,
		current_day2: null,
		c_year2: null,
		c_month2: null,
		c_day2: null,
		s_count2: 1,

		products: [],
		activeprods: [],
		stores: [],
		activestores: [],
		orders: [],
		comporders: [],
		order_prods: [],
		etransactions: [],
		documents: [],
		document_logs: [],
		sales: [],

		or_num: '',
		rcvr_name: '',
		rcvr_contact: '',
		rcvr_orderDate: '',
		total_price: '0',
		tax: '0',
		shipping_fee: '0',
		subtotal: '0',

		currentShoppingUser: '',

		countSold: 0,

		delPack: new Form({
			'reason': "",
			'tracking': ""
		}, { baseURL: '/visa/client/' }),

		fundPack: new Form({
			'type': "",
			'reason': "",
			'amount': "",
			'tracking': "",
			'client_id': "",
			'selected_client': "",
			'authorizer': "",
			'auth_name': ""
		}, { baseURL: '/visa/client/' }),

		editServ: new Form({
			'cost': 0,
			'tip': 0,
			'status': "",
			'active': 0,
			'discount': 0,
			'reason': "",
			'note': "",
			'id': null,
			'reporting': '',
			'extend': '',
			'rcv_docs': ''
		}, { baseURL: '/visa/client/' }),

		cpanel_url: window.Laravel.base_url,
		date_now: new Date(),
		doctype: '',

		validity: new Form({
			'expires_at': "",
			'status': "",
			'id': "",
			'month': "",
			'day': ""
		}, { baseURL: '/visa/client' }),

		quickReportClientsId: [],
		quickReportClientServicesId: [],
		quickReportTrackings: [],

		dt: '',

		authorizerFrm: new Form({
			'authorizer': '',
			'password': ''
		}, { baseURL: '/visa/client/' }),

		setting: user.access_control[0].setting,
		permissions: user.permissions,
		selpermissions: [{
			edit: 0,
			actionLogs: 0,
			transactionLogs: 0,
			documents: 0,
			accessControl: 0,
			addService: 0,
			addFunds: 0,
			deleteThisPackage: 0,
			editService: 0,
			addQuickReport: 0
		}],

		files: []
		//docrcv: [],

	},

	created: function created() {
		var _this = this;

		//console.log(this.is_auth);

		var user_id = data.id; // change to session data
		this.currentShoppingUser = user_id;

		function getServiceCost() {
			return axios.get('/visa/client/clientServiceCost/' + user_id);
		}

		function getDeposit() {
			return axios.get('/visa/client/clientDeposit/' + user_id);
		}

		function getPayment() {
			return axios.get('/visa/client/clientPayment/' + user_id);
		}

		function getRefund() {
			return axios.get('/visa/client/clientRefund/' + user_id);
		}

		function getDiscount() {
			return axios.get('/visa/client/clientDiscount/' + user_id);
		}

		function getBalance() {
			return axios.get('/visa/client/clientBalance/' + user_id);
		}

		function getTransfer() {
			return axios.get('/visa/client/clientTransfer/' + user_id);
		}

		function getPackages() {
			return axios.get('/visa/client/clientPackages/' + user_id);
		}

		function getServices(tracking) {
			return axios.get('/visa/client/clientServices/' + user_id + '/' + tracking);
		}

		function getActionLogs() {
			return axios.get('/visa/client/clientActionLogs/' + user_id);
		}

		function getTransactionLogs() {
			return axios.get('/visa/client/clientTransactionLogs/' + user_id);
		}

		function getDocuments() {
			return axiosAPIv1.get('/users/' + user_id + '/documents');
		}

		function getDocumentLogs() {
			return axiosAPIv1.get('/doclog?user=' + user_id);
		}

		// function getAuthorizers(){
		// 	return axios.get('/visa/client/get-authorizers');
		// }

		function getFiles() {
			var clientId = window.Laravel.data.id;
			return axios.get('/visa/service-manager/client-documents/client-id/' + clientId);
		}

		// function getDocumentReceive(){
		// 	let clientId = window.Laravel.data.id;
		// 	return axios.get('/visa/service-manager/client-documents/received/'+ clientId);
		// }

		axios.all([getServiceCost(), getDeposit(), getPayment(), getRefund(), getDiscount(), getBalance(), getTransfer(), getPackages(), getServices(0), getActionLogs(), getTransactionLogs(), getDocuments(), getDocumentLogs()
		// ,getAuthorizers()
		, getFiles()
		// ,getDocumentReceive()
		]).then(axios.spread(function (service_cost, deposit, payment, refund, discount, balance, transfer, packages, services, actionlogs, transactionlogs, docs, doclogs,
		// authors,
		clientfiles)
		//docrcv,
		{
			_this.service_cost = service_cost.data;
			_this.deposit = deposit.data;
			_this.payment = payment.data;
			_this.refund = refund.data;
			_this.discount = discount.data;
			_this.balance = balance.data;
			_this.transfer = transfer.data;
			_this.packages = packages.data;
			_this.services = services.data.services;
			_this.package_date = services.data.package_date;
			_this.package_cost = services.data.package_cost;
			_this.package_deposit = services.data.package_deposit;
			_this.package_payment = services.data.package_payment;
			_this.package_refund = services.data.package_refund;
			_this.package_discount = services.data.package_discount;
			_this.package_balance = services.data.package_balance;
			_this.actionlogs = actionlogs.data;
			_this.transactionlogs = transactionlogs.data;
			_this.documents = docs.data;
			_this.document_logs = doclogs.data;
			// this.authorizers 	= authors.data;
			_this.files = clientfiles.data;
			//this.docrcv 	= docrcv.data;
		})).catch($.noop);
		//this.getFiles();
	},

	methods: {
		// getAuthorizers(){
		// 	axios.get('/visa/client/get-authorizers')
		// 		.then(response => {
		// 			this.authorizers = response.data;
		// 		})
		// },

		fetchClients: function fetchClients(q) {
			var _this2 = this;

			if (q == data.id) {
				$('.popover #fundset').addClass('has-error');
				toastr.error("Cannot transfer to same client id.");
			}
			axiosAPIv1.get('/visa/client/get-visa-clients', {
				params: {
					q: q
				}
			}).then(function (response) {
				if (response.data.length > 0) {
					_this2.clients.push({
						id: response.data[0].id,
						name: response.data[0].name });
				}

				setTimeout(function () {
					var my_val = $(".chosen-select-for-clients").val();
					$(".chosen-select-for-clients").val(my_val).trigger("chosen:updated");
				}, 1000);
			});
		},
		getFiles: function getFiles() {
			var _this3 = this;

			var clientId = window.Laravel.data.id;

			axios.get('/visa/service-manager/client-documents/client-id/' + clientId).then(function (response) {
				_this3.files = response.data;
			});
		},
		saveAuthorizer: function saveAuthorizer() {
			var _this4 = this;

			this.authorizerFrm.submit('post', '/saveAuthorizer').then(function (result) {
				if (result.status == 'failed') {
					toastr.error('Wrong Password');
				} else if (result.status == 'success') {
					_this4.fundPack.authorizer = _this4.authorizerFrm.authorizer.id;
					_this4.fundPack.auth_name = _this4.authorizerFrm.authorizer.fullname;
					_this4.fundPack.submit('post', '/clientAddFunds').then(function (result) {
						_this4.reloadComputation();
						_this4.showServicesUnder(data.id, _this4.tracking_selected);
						toastr.success('Successfully saved.');
					}).catch(function (error) {
						toastr.error('Failed');
					});
					$('#authorizerModal').modal('toggle');
				}
			}).catch(function (error) {
				toastr.error('Failed');
			});
		},
		getDate: function getDate() {
			this.dt = $('form#daily-form input[name=date]').val();
			this.editServ.extend = this.dt;
		},
		showServicesUnder: function showServicesUnder(client_id, tracking) {
			var _this5 = this;

			axios.get('/visa/client/clientServices/' + client_id + '/' + tracking).then(function (response) {
				_this5.services = response.data.services;
				_this5.package_date = response.data.package_date;
				_this5.package_cost = response.data.package_cost;
				_this5.package_deposit = response.data.package_deposit;
				_this5.package_payment = response.data.package_payment;
				_this5.package_refund = response.data.package_refund;
				_this5.package_discount = response.data.package_discount;
				_this5.package_balance = response.data.package_balance;
				_this5.tracking_selected = tracking;
				_this5.group_package = false;
				if (tracking.slice(0, 1) == 'G') {
					_this5.group_package = true;
				}
				_this5.tracking_status = response.data.status;
				_this5.popOver();
			});
		},
		showAllServices: function showAllServices() {
			var _this6 = this;

			var client_id = data.id;
			axios.get('/visa/client/clientServices/' + client_id + '/' + 0).then(function (response) {
				_this6.services = response.data.services;
				_this6.package_date = response.data.package_date;
				_this6.package_cost = response.data.package_cost;
				_this6.package_deposit = response.data.package_deposit;
				_this6.package_payment = response.data.package_payment;
				_this6.package_refund = response.data.package_refund;
				_this6.package_discount = response.data.package_discount;
				_this6.package_balance = response.data.package_balance;
				_this6.tracking_selected = null;
				_this6.group_package = false;
				_this6.tracking_status = null;
			});
		},
		updateTrackingList: function updateTrackingList(client_id) {
			var _this7 = this;

			axios.get('/visa/client/clientPackages/' + client_id).then(function (response) {
				_this7.packages = null;
				_this7.packages = response.data;
			});
		},
		addNewPackage: function addNewPackage() {
			var _this8 = this;

			var client_id = data.id;
			axios.get('/visa/client/clientAddPackage/' + client_id).then(function (response) {
				var track = response.data.tracking;
				_this8.showServicesUnder(client_id, track);
				_this8.updateTrackingList(client_id);
				var owl = $("#owl-demo");
				owl.data('owlCarousel').destroy();
			});
		},
		deletePackage: function deletePackage(reason) {
			var _this9 = this;

			var client_id = data.id;
			$(".delete-package").popover('hide');
			this.delPack.tracking = this.tracking_selected;
			if (reason == '') {
				$('.popover #validset').addClass('has-error');
				toastr.error("Please indicate reason to delete.");
			} else {
				this.delPack.reason = reason;
				this.delPack.submit('post', '/clientDeletePackage').then(function (response) {
					if (response.status == 'success') {
						toastr.success('Successfully saved.');
						_this9.showAllServices();
						_this9.updateTrackingList(client_id);
						var owl = $("#owl-demo");
						owl.data('owlCarousel').destroy();
					} else {
						toastr.error(response.log);
					}
				}).catch(function (error) {
					if (error.reason) {
						$('.popover #validset').addClass('has-error');
						toastr.error("Please indicate reason to delete.");
					} else {
						toastr.error('Update failed.');
					}
				});
			}
		},
		loadDeposit: function loadDeposit() {
			var _this10 = this;

			var client_id = data.id;
			axios.get('/visa/client/clientDeposit/' + client_id).then(function (response) {
				_this10.deposit = response.data;
			});
		},
		loadRefund: function loadRefund() {
			var _this11 = this;

			var client_id = data.id;
			axios.get('/visa/client/clientRefund/' + client_id).then(function (response) {
				_this11.refund = response.data;
			});
		},
		loadPayment: function loadPayment() {
			var _this12 = this;

			var client_id = data.id;
			axios.get('/visa/client/clientPayment/' + client_id).then(function (response) {
				_this12.payment = response.data;
			});
		},
		loadDiscount: function loadDiscount() {
			var _this13 = this;

			var client_id = data.id;
			axios.get('/visa/client/clientDiscount/' + client_id).then(function (response) {
				_this13.discount = response.data;
			});
		},
		loadBalance: function loadBalance() {
			var _this14 = this;

			var client_id = data.id;
			axios.get('/visa/client/clientBalance/' + client_id).then(function (response) {
				_this14.balance = response.data;
			});
		},
		loadTransfer: function loadTransfer() {
			var _this15 = this;

			var client_id = data.id;
			axios.get('/visa/client/clientTransfer/' + client_id).then(function (response) {
				_this15.transfer = response.data;
			});
		},
		editService: function editService(service_id) {
			var _this16 = this;

			this.editServ.id = service_id;
			axios.get('/visa/client/clientGetService/' + service_id).then(function (response) {
				_this16.editServ.cost = response.data.cost;
				_this16.editServ.tip = response.data.tip;
				if (response.data.deleted_at != null) {
					_this16.editServ.discount = '';
					_this16.editServ.reason = '';
				} else {
					_this16.editServ.discount = response.data.discount_amount;
					_this16.editServ.reason = response.data.reason;
				}
				_this16.editServ.status = response.data.status;
				_this16.editServ.active = response.data.active;
				_this16.editServ.extend = response.data.extend;
				// this.editServ.extend = response.data.remarks;
			});
			$('#edit-service-modal').modal('show');
		},
		addFunds: function addFunds(type, amount, reason) {
			var _this17 = this;

			// this.authorization();
			// console.log(this.is_auth);

			if ((type == 'refund' || type == 'discount' || type == 'transfer') && reason.trim() == '') {
				$('.popover #fundset').addClass('has-error');
				toastr.error("Please input reason.");
			} else if (amount == '') {
				$('.popover #fundset').addClass('has-error');
				toastr.error("Please input amount to add.");
			} else if (type == 'transfer' && this.fundPack.selected_client == '') {
				$('.popover #fundset').addClass('has-error');
				toastr.error("Please select client to proceed transfer.");
			} else {
				this.fundPack.type = type;
				this.fundPack.amount = amount;
				this.fundPack.tracking = this.tracking_selected;
				this.fundPack.client_id = data.id;
				this.fundPack.reason = reason;
				// this.fundPack.selected_client = selected_client;

				$(".add-funds").popover('hide');
				this.fundPack.submit('post', '/clientAddFunds').then(function (result) {
					_this17.reloadComputation();

					_this17.showServicesUnder(data.id, _this17.tracking_selected);

					toastr.success('Successfully saved.');
				}).catch(function (error) {});

				//WITH AUTHORIZER

				// //discount type transaction needs an authorizer to proceed.
				// if(type == 'discount'){
				// 	// function to check if user is an authorizer or not
				// 	if(this.is_auth){
				// 		this.fundPack.authorizer = this.processor.id;
				// 		this.fundPack.authorizer = this.processor.fullname;

				// 		$(".add-funds").popover('hide');
				//        		this.fundPack.submit('post','/clientAddFunds')
				//        			.then(result => {
				//        				this.reloadComputation();

				//        				this.showServicesUnder(data.id, this.tracking_selected);

				//               toastr.success('Successfully saved.');
				//           })
				//           .catch(error =>{});
				// 	}else{
				// 		//show pop up for non authorizers
				// 		$('#authorizerModal').modal('show');
				// 	}
				// }else{
				// 	$(".add-funds").popover('hide');
				//      		this.fundPack.submit('post','/clientAddFunds')
				//      			.then(result => {
				//      				this.reloadComputation();

				//      				this.showServicesUnder(data.id, this.tracking_selected);

				//             toastr.success('Successfully saved.');
				//         })
				//         .catch(error =>{});
				// }
			}
		},
		authorization: function authorization() {
			var roles = this.processor.roles;
			var is_auth = false;
			roles.map(function (e) {
				console.log('ID:' + e.id);
				console.log('NAME:' + e.name);
				if (e.name == 'authorizer') {
					is_auth = true;
				}
			});
			this.is_auth = is_auth;
			console.log(this.is_auth);
		},
		reloadComputation: function reloadComputation() {
			this.loadDeposit();
			this.loadDiscount();
			this.loadRefund();
			this.loadPayment();
			this.loadBalance();
			this.loadTransfer();
		},
		popOver: function popOver() {
			$(".delete-package").popover({
				html: true,
				container: 'body',
				content: function content() {
					return $('#deletePackagePop').html();
				}
			});

			var $fundsPopOver = $(".add-funds").popover({
				html: true,
				container: 'body',
				content: function content() {
					return $('#addFundsPop').html();
				}
			});

			// on show popover
			$fundsPopOver.on("shown.bs.popover", function (e) {
				$("body").tooltip({ selector: '[data-toggle=tooltip]' });
				$('input.typeahead').typeahead({
					source: function source(query, process) {
						if (query == data.id) {
							$('.popover #fundset').addClass('has-error');
							toastr.error("Cannot transfer to same client id.");
						} else {
							return $.get('/visa/client/search/transfer-balance', { query: query }, function (data) {
								data = $.parseJSON(data);
								return process(data);
							});
						}
					},
					minLength: 2,
					fitToElement: true,
					matcher: function matcher(item) {
						return true;
					},
					updater: function updater(item) {
						ClientApp.fundPack.selected_client = item.id;
						return item;
					}
				});
				//   	$('.chosen-select-for-clients').chosen({
				// 	width: "100%",
				// 	no_results_text: "Searching Client # : ",
				// 	search_contains: true,
				// 	// max_selected_options: 1
				// });

				//   	// when using chosen inside popover, call first popover class
				// $('body').on('keyup', '.chosen-container input[type=text]', (e) => {
				// 	let q = $('.popover .chosen-container input[type=text]').val();
				// 	if( q.length >= 4 ){
				// 		ClientApp.fetchClients(q);
				// 	}
				// });
			});

			$fundsPopOver.on('hidden.bs.popover', function () {
				$('.chosen-select-for-clients').chosen('destroy');
			});

			$(".add-verification").popover({
				html: true,
				container: 'body',
				content: function content() {
					ClientApp.validity.id = $(this).attr('id');
					ClientApp.doctype = $(this).attr('data-docs');
					return $('#frmValidUntil').html();
				}
			});
		},
		saveValidity: function saveValidity(dateSel) {
			var _this18 = this;

			$(".verify").popover('hide');
			this.validity.status = "Verified";
			this.validity.expires_at = dateSel;
			var month = parseInt(dateSel.substr(5, 2));
			var day = parseInt(dateSel.substr(8, 2));

			if (month != 0) {
				if (day != 0) {
					this.validity.submit('post', '/verify-docs').then(function (result) {

						if (_this18.doctype == '9gICard') {
							_this18.ninegicard_expiry = dateSel;
							var d = new Date(_this18.ninegicard_expiry);
							_this18.ninegicard_compare = d < _this18.date_now;
						}

						if (_this18.doctype == '9gOrder') {
							_this18.ninegorder_expiry = dateSel;
							var d = new Date(_this18.ninegorder_expiry);
							_this18.ninegorder_compare = d < _this18.date_now;
						}

						if (_this18.doctype == 'prvCard') {
							_this18.prvcard_expiry = dateSel;
							var d = new Date(_this18.prvcard_expiry);
							_this18.prvcard_compare = d < _this18.date_now;
						}

						if (_this18.doctype == 'prvOrder') {
							_this18.prvorder_expiry = dateSel;
							var d = new Date(_this18.prvorder_expiry);
							_this18.prvorder_compare = d < _this18.date_now;
						}

						if (_this18.doctype == 'srrvCard') {
							_this18.srrvcard_expiry = dateSel;
							var d = new Date(_this18.srrvcard_expiry);
							_this18.srrvcard_compare = d < _this18.date_now;
						}

						if (_this18.doctype == 'srrvVisa') {
							_this18.srrvvisa_expiry = dateSel;
							var d = new Date(_this18.srrvvisa_expiry);
							_this18.srrvvisa_compare = d < _this18.date_now;
						}

						if (_this18.doctype == 'aep') {
							_this18.aep_expiry = dateSel;
							var d = new Date(_this18.aep_expiry);
							_this18.aep_compare = d < _this18.date_now;
						}

						if (_this18.doctype == 'nbi') {
							_this18.nbi_expiry = dateSel;
							var d = new Date(_this18.nbi_expiry);
							_this18.nbi_compare = d < _this18.date_now;
						}

						$("#" + _this18.validity.id).popover('hide');
						toastr.success('Successfully saved.');
					}).catch(function (error) {
						if (error.expires_at) {
							$('.popover #validset').addClass('has-error');
							toastr.error("Please use valid date of expiry.");
						} else {
							toastr.error('Update failed.');
						}
						$("#" + _this18.validity.id).popover('show');
					});
				} else {
					toastr.error('Please use valid expiration date');
				}
			} else {
				toastr.error('Please use valid expiration date');
			}
		},
		fetchDocs: function fetchDocs(id) {
			this.$refs.multiplequickreportref.fetchDocs(id);
		},
		fetchDocs2: function fetchDocs2(id) {
			this.$refs.editserviceref.fetchDocs(id);
		},
		fetchDocs3: function fetchDocs3(service_id, needed, optional) {
			this.$refs.addnewserviceref.fetchDocs(service_id, needed, optional);
		}
	},
	mounted: function mounted() {
		var vm = this;

		$('body').popover({ // Ok
			html: true,
			trigger: 'hover',
			selector: '[data-toggle="popover"]'
		});

		// $('.chosen-select-for-clients').chosen({
		// 	width: "100%",
		// 	no_results_text: "Searching Client # : ",
		// 	search_contains: true
		// });

		$('.chosen-select-for-clients').on('change', function () {
			var clientIds = $('.chosen-select-for-clients').val();

			//this.addNewMemberForm.clientIds = clientIds;
		});
	},

	computed: {
		pack_date: function pack_date() {
			var c = moment(String('2017-01-01')).format("Y-m-d");
			var d = moment(String(this.package_date)).format("Y-m-d");
			return c < d ? true : false;
		}
	},
	watch: {
		// whenever bday changes, this function will run
		bday: function bday(newVal) {
			this.upStore(newVal, 'bday');
		}
	},
	updated: function updated() {
		this.popOver();

		// Fired every second, should always be true
		var owl = $(".trackings");
		owl.owlCarousel({
			items: 4,
			lazyLoad: true,
			navigation: false,
			pagination: false,
			rewindNav: false
		});
		$(".next").click(function () {
			owl.trigger('owl.next');
		});
		$(".prev").click(function () {
			owl.trigger('owl.prev');
		});
		//    $('#lists').DataTable({
		//     responsive: true,
		//     "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		//     "iDisplayLength": 25
		// });
	},

	directives: {
		datepicker: {
			bind: function bind(el, binding, vnode) {
				$(el).datepicker({
					format: 'yyyy-mm-dd'
				}).on('changeDate', function (e) {
					ClientApp.$set(ClientApp.validity, 'expires_at', e.format('yyyy-mm-dd'));
				});
			}
		}
	},
	components: {
		Multiselect: __WEBPACK_IMPORTED_MODULE_0_vue_multiselect___default.a,
		'multiple-quick-report': __webpack_require__(14),
		'client-documents': __webpack_require__(339)
	}

});

$(document).ready(function () {
	document.title = ClientApp.fullname;

	$('#birth_date').datepicker();
	$('.underlined').hide();
	$('.freason').hide();
	$('.transferTo').hide();
	$('.dual_select').bootstrapDualListbox({
		selectorMinimalHeight: 160
	});

	$(".se-pre-con2").fadeOut("slow");

	$(".delete-package").popover({
		html: true,
		container: 'body',
		content: function content() {
			return $('#deletePackagePop').html();
		}
	});

	$(".add-funds").popover({
		html: true,
		container: 'body',
		content: function content() {
			return $('#addFundsPop').html();
		}
	});
});

$(document).on("click", ".del-pack", function () {
	var reason = $('.popover #reason').val();
	ClientApp.deletePackage(reason);
});

$(document).on("change", "#fund_type", function () {
	var type = $('.popover #fund_type').val();
	if (type == "discount" || type == "refund") {
		$('.freason').show();
		$('.transferTo').hide();
	} else if (type == "transfer") {
		$('.freason').show();
		$('.transferTo').show();
	} else {
		$('.freason').hide();
		$('.transferTo').hide();
		$('.popover #fund_reason').val('');
	}
});

$(document).on("click", ".funds-add", function () {
	var reason = $('.popover #fund_reason').val();
	var amount = $('.popover #fund_amount').val();
	var type = $('.popover #fund_type').val();
	// alert(reason+" "+amount+" "+type);
	ClientApp.addFunds(type, amount, reason);
});

$(document).on("click", "#fullBalance", function () {
	$('.popover #fund_amount').val(Math.abs(ClientApp.balance));
});

$(document).on("click", ".save-verification", function () {
	var dateSel = $('.popover #expires_at').val();
	ClientApp.saveValidity(dateSel);
});

$(document).on('click', function (e) {
	$('[data-toggle="popover"],[data-original-title]').each(function () {
		if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
			(($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false; // fix for BS 3.3.6
		}
	});
});

$(document).on('focus.autoExpand', 'textarea.autoExpand', function () {
	var savedValue = this.value;
	this.value = '';
	this.baseScrollHeight = this.scrollHeight;
	this.value = savedValue;
}).on('input.autoExpand', 'textarea.autoExpand', function () {
	var minRows = this.getAttribute('data-min-rows') | 0,
	    rows;
	this.rows = minRows;
	rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 17);
	this.rows = minRows + rows;
});

$(document).on('click', '[data-toggle="lightbox"]', function (event) {
	event.preventDefault();
	$(this).ekkoLightbox();
});

/***/ }),

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

!function(t,e){ true?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.VueMultiselect=e():t.VueMultiselect=e()}(this,function(){return function(t){function e(i){if(n[i])return n[i].exports;var r=n[i]={i:i,l:!1,exports:{}};return t[i].call(r.exports,r,r.exports,e),r.l=!0,r.exports}var n={};return e.m=t,e.c=n,e.i=function(t){return t},e.d=function(t,n,i){e.o(t,n)||Object.defineProperty(t,n,{configurable:!1,enumerable:!0,get:i})},e.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(n,"a",n),n},e.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},e.p="/",e(e.s=60)}([function(t,e){var n=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=n)},function(t,e,n){var i=n(49)("wks"),r=n(30),o=n(0).Symbol,s="function"==typeof o;(t.exports=function(t){return i[t]||(i[t]=s&&o[t]||(s?o:r)("Symbol."+t))}).store=i},function(t,e,n){var i=n(5);t.exports=function(t){if(!i(t))throw TypeError(t+" is not an object!");return t}},function(t,e,n){var i=n(0),r=n(10),o=n(8),s=n(6),u=n(11),a=function(t,e,n){var l,c,f,p,h=t&a.F,d=t&a.G,v=t&a.S,g=t&a.P,m=t&a.B,y=d?i:v?i[e]||(i[e]={}):(i[e]||{}).prototype,b=d?r:r[e]||(r[e]={}),_=b.prototype||(b.prototype={});d&&(n=e);for(l in n)c=!h&&y&&void 0!==y[l],f=(c?y:n)[l],p=m&&c?u(f,i):g&&"function"==typeof f?u(Function.call,f):f,y&&s(y,l,f,t&a.U),b[l]!=f&&o(b,l,p),g&&_[l]!=f&&(_[l]=f)};i.core=r,a.F=1,a.G=2,a.S=4,a.P=8,a.B=16,a.W=32,a.U=64,a.R=128,t.exports=a},function(t,e,n){t.exports=!n(7)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,e){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,e,n){var i=n(0),r=n(8),o=n(12),s=n(30)("src"),u=Function.toString,a=(""+u).split("toString");n(10).inspectSource=function(t){return u.call(t)},(t.exports=function(t,e,n,u){var l="function"==typeof n;l&&(o(n,"name")||r(n,"name",e)),t[e]!==n&&(l&&(o(n,s)||r(n,s,t[e]?""+t[e]:a.join(String(e)))),t===i?t[e]=n:u?t[e]?t[e]=n:r(t,e,n):(delete t[e],r(t,e,n)))})(Function.prototype,"toString",function(){return"function"==typeof this&&this[s]||u.call(this)})},function(t,e){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,e,n){var i=n(13),r=n(25);t.exports=n(4)?function(t,e,n){return i.f(t,e,r(1,n))}:function(t,e,n){return t[e]=n,t}},function(t,e){var n={}.toString;t.exports=function(t){return n.call(t).slice(8,-1)}},function(t,e){var n=t.exports={version:"2.5.7"};"number"==typeof __e&&(__e=n)},function(t,e,n){var i=n(14);t.exports=function(t,e,n){if(i(t),void 0===e)return t;switch(n){case 1:return function(n){return t.call(e,n)};case 2:return function(n,i){return t.call(e,n,i)};case 3:return function(n,i,r){return t.call(e,n,i,r)}}return function(){return t.apply(e,arguments)}}},function(t,e){var n={}.hasOwnProperty;t.exports=function(t,e){return n.call(t,e)}},function(t,e,n){var i=n(2),r=n(41),o=n(29),s=Object.defineProperty;e.f=n(4)?Object.defineProperty:function(t,e,n){if(i(t),e=o(e,!0),i(n),r)try{return s(t,e,n)}catch(t){}if("get"in n||"set"in n)throw TypeError("Accessors not supported!");return"value"in n&&(t[e]=n.value),t}},function(t,e){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,e){t.exports={}},function(t,e){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,e,n){"use strict";var i=n(7);t.exports=function(t,e){return!!t&&i(function(){e?t.call(null,function(){},1):t.call(null)})}},function(t,e,n){var i=n(23),r=n(16);t.exports=function(t){return i(r(t))}},function(t,e,n){var i=n(53),r=Math.min;t.exports=function(t){return t>0?r(i(t),9007199254740991):0}},function(t,e,n){var i=n(11),r=n(23),o=n(28),s=n(19),u=n(64);t.exports=function(t,e){var n=1==t,a=2==t,l=3==t,c=4==t,f=6==t,p=5==t||f,h=e||u;return function(e,u,d){for(var v,g,m=o(e),y=r(m),b=i(u,d,3),_=s(y.length),x=0,w=n?h(e,_):a?h(e,0):void 0;_>x;x++)if((p||x in y)&&(v=y[x],g=b(v,x,m),t))if(n)w[x]=g;else if(g)switch(t){case 3:return!0;case 5:return v;case 6:return x;case 2:w.push(v)}else if(c)return!1;return f?-1:l||c?c:w}}},function(t,e,n){var i=n(5),r=n(0).document,o=i(r)&&i(r.createElement);t.exports=function(t){return o?r.createElement(t):{}}},function(t,e){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,e,n){var i=n(9);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==i(t)?t.split(""):Object(t)}},function(t,e){t.exports=!1},function(t,e){t.exports=function(t,e){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:e}}},function(t,e,n){var i=n(13).f,r=n(12),o=n(1)("toStringTag");t.exports=function(t,e,n){t&&!r(t=n?t:t.prototype,o)&&i(t,o,{configurable:!0,value:e})}},function(t,e,n){var i=n(49)("keys"),r=n(30);t.exports=function(t){return i[t]||(i[t]=r(t))}},function(t,e,n){var i=n(16);t.exports=function(t){return Object(i(t))}},function(t,e,n){var i=n(5);t.exports=function(t,e){if(!i(t))return t;var n,r;if(e&&"function"==typeof(n=t.toString)&&!i(r=n.call(t)))return r;if("function"==typeof(n=t.valueOf)&&!i(r=n.call(t)))return r;if(!e&&"function"==typeof(n=t.toString)&&!i(r=n.call(t)))return r;throw TypeError("Can't convert object to primitive value")}},function(t,e){var n=0,i=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++n+i).toString(36))}},function(t,e,n){"use strict";var i=n(0),r=n(12),o=n(9),s=n(67),u=n(29),a=n(7),l=n(77).f,c=n(45).f,f=n(13).f,p=n(51).trim,h=i.Number,d=h,v=h.prototype,g="Number"==o(n(44)(v)),m="trim"in String.prototype,y=function(t){var e=u(t,!1);if("string"==typeof e&&e.length>2){e=m?e.trim():p(e,3);var n,i,r,o=e.charCodeAt(0);if(43===o||45===o){if(88===(n=e.charCodeAt(2))||120===n)return NaN}else if(48===o){switch(e.charCodeAt(1)){case 66:case 98:i=2,r=49;break;case 79:case 111:i=8,r=55;break;default:return+e}for(var s,a=e.slice(2),l=0,c=a.length;l<c;l++)if((s=a.charCodeAt(l))<48||s>r)return NaN;return parseInt(a,i)}}return+e};if(!h(" 0o1")||!h("0b1")||h("+0x1")){h=function(t){var e=arguments.length<1?0:t,n=this;return n instanceof h&&(g?a(function(){v.valueOf.call(n)}):"Number"!=o(n))?s(new d(y(e)),n,h):y(e)};for(var b,_=n(4)?l(d):"MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","),x=0;_.length>x;x++)r(d,b=_[x])&&!r(h,b)&&f(h,b,c(d,b));h.prototype=v,v.constructor=h,n(6)(i,"Number",h)}},function(t,e,n){"use strict";function i(t){return 0!==t&&(!(!Array.isArray(t)||0!==t.length)||!t)}function r(t){return function(){return!t.apply(void 0,arguments)}}function o(t,e){return void 0===t&&(t="undefined"),null===t&&(t="null"),!1===t&&(t="false"),-1!==t.toString().toLowerCase().indexOf(e.trim())}function s(t,e,n,i){return t.filter(function(t){return o(i(t,n),e)})}function u(t){return t.filter(function(t){return!t.$isLabel})}function a(t,e){return function(n){return n.reduce(function(n,i){return i[t]&&i[t].length?(n.push({$groupLabel:i[e],$isLabel:!0}),n.concat(i[t])):n},[])}}function l(t,e,i,r,o){return function(u){return u.map(function(u){var a;if(!u[i])return console.warn("Options passed to vue-multiselect do not contain groups, despite the config."),[];var l=s(u[i],t,e,o);return l.length?(a={},n.i(d.a)(a,r,u[r]),n.i(d.a)(a,i,l),a):[]})}}var c=n(59),f=n(54),p=(n.n(f),n(95)),h=(n.n(p),n(31)),d=(n.n(h),n(58)),v=n(91),g=(n.n(v),n(98)),m=(n.n(g),n(92)),y=(n.n(m),n(88)),b=(n.n(y),n(97)),_=(n.n(b),n(89)),x=(n.n(_),n(96)),w=(n.n(x),n(93)),S=(n.n(w),n(90)),O=(n.n(S),function(){for(var t=arguments.length,e=new Array(t),n=0;n<t;n++)e[n]=arguments[n];return function(t){return e.reduce(function(t,e){return e(t)},t)}});e.a={data:function(){return{search:"",isOpen:!1,prefferedOpenDirection:"below",optimizedHeight:this.maxHeight}},props:{internalSearch:{type:Boolean,default:!0},options:{type:Array,required:!0},multiple:{type:Boolean,default:!1},value:{type:null,default:function(){return[]}},trackBy:{type:String},label:{type:String},searchable:{type:Boolean,default:!0},clearOnSelect:{type:Boolean,default:!0},hideSelected:{type:Boolean,default:!1},placeholder:{type:String,default:"Select option"},allowEmpty:{type:Boolean,default:!0},resetAfter:{type:Boolean,default:!1},closeOnSelect:{type:Boolean,default:!0},customLabel:{type:Function,default:function(t,e){return i(t)?"":e?t[e]:t}},taggable:{type:Boolean,default:!1},tagPlaceholder:{type:String,default:"Press enter to create a tag"},tagPosition:{type:String,default:"top"},max:{type:[Number,Boolean],default:!1},id:{default:null},optionsLimit:{type:Number,default:1e3},groupValues:{type:String},groupLabel:{type:String},groupSelect:{type:Boolean,default:!1},blockKeys:{type:Array,default:function(){return[]}},preserveSearch:{type:Boolean,default:!1},preselectFirst:{type:Boolean,default:!1}},mounted:function(){this.multiple||this.clearOnSelect||console.warn("[Vue-Multiselect warn]: ClearOnSelect and Multiple props can’t be both set to false."),!this.multiple&&this.max&&console.warn("[Vue-Multiselect warn]: Max prop should not be used when prop Multiple equals false."),this.preselectFirst&&!this.internalValue.length&&this.options.length&&this.select(this.filteredOptions[0])},computed:{internalValue:function(){return this.value||0===this.value?Array.isArray(this.value)?this.value:[this.value]:[]},filteredOptions:function(){var t=this.search||"",e=t.toLowerCase().trim(),n=this.options.concat();return n=this.internalSearch?this.groupValues?this.filterAndFlat(n,e,this.label):s(n,e,this.label,this.customLabel):this.groupValues?a(this.groupValues,this.groupLabel)(n):n,n=this.hideSelected?n.filter(r(this.isSelected)):n,this.taggable&&e.length&&!this.isExistingOption(e)&&("bottom"===this.tagPosition?n.push({isTag:!0,label:t}):n.unshift({isTag:!0,label:t})),n.slice(0,this.optionsLimit)},valueKeys:function(){var t=this;return this.trackBy?this.internalValue.map(function(e){return e[t.trackBy]}):this.internalValue},optionKeys:function(){var t=this;return(this.groupValues?this.flatAndStrip(this.options):this.options).map(function(e){return t.customLabel(e,t.label).toString().toLowerCase()})},currentOptionLabel:function(){return this.multiple?this.searchable?"":this.placeholder:this.internalValue.length?this.getOptionLabel(this.internalValue[0]):this.searchable?"":this.placeholder}},watch:{internalValue:function(){this.resetAfter&&this.internalValue.length&&(this.search="",this.$emit("input",this.multiple?[]:null))},search:function(){this.$emit("search-change",this.search,this.id)}},methods:{getValue:function(){return this.multiple?this.internalValue:0===this.internalValue.length?null:this.internalValue[0]},filterAndFlat:function(t,e,n){return O(l(e,n,this.groupValues,this.groupLabel,this.customLabel),a(this.groupValues,this.groupLabel))(t)},flatAndStrip:function(t){return O(a(this.groupValues,this.groupLabel),u)(t)},updateSearch:function(t){this.search=t},isExistingOption:function(t){return!!this.options&&this.optionKeys.indexOf(t)>-1},isSelected:function(t){var e=this.trackBy?t[this.trackBy]:t;return this.valueKeys.indexOf(e)>-1},getOptionLabel:function(t){if(i(t))return"";if(t.isTag)return t.label;if(t.$isLabel)return t.$groupLabel;var e=this.customLabel(t,this.label);return i(e)?"":e},select:function(t,e){if(t.$isLabel&&this.groupSelect)return void this.selectGroup(t);if(!(-1!==this.blockKeys.indexOf(e)||this.disabled||t.$isDisabled||t.$isLabel)&&(!this.max||!this.multiple||this.internalValue.length!==this.max)&&("Tab"!==e||this.pointerDirty)){if(t.isTag)this.$emit("tag",t.label,this.id),this.search="",this.closeOnSelect&&!this.multiple&&this.deactivate();else{if(this.isSelected(t))return void("Tab"!==e&&this.removeElement(t));this.$emit("select",t,this.id),this.multiple?this.$emit("input",this.internalValue.concat([t]),this.id):this.$emit("input",t,this.id),this.clearOnSelect&&(this.search="")}this.closeOnSelect&&this.deactivate()}},selectGroup:function(t){var e=this,n=this.options.find(function(n){return n[e.groupLabel]===t.$groupLabel});if(n)if(this.wholeGroupSelected(n)){this.$emit("remove",n[this.groupValues],this.id);var i=this.internalValue.filter(function(t){return-1===n[e.groupValues].indexOf(t)});this.$emit("input",i,this.id)}else{var o=n[this.groupValues].filter(r(this.isSelected));this.$emit("select",o,this.id),this.$emit("input",this.internalValue.concat(o),this.id)}},wholeGroupSelected:function(t){return t[this.groupValues].every(this.isSelected)},removeElement:function(t){var e=!(arguments.length>1&&void 0!==arguments[1])||arguments[1];if(!this.disabled){if(!this.allowEmpty&&this.internalValue.length<=1)return void this.deactivate();var i="object"===n.i(c.a)(t)?this.valueKeys.indexOf(t[this.trackBy]):this.valueKeys.indexOf(t);if(this.$emit("remove",t,this.id),this.multiple){var r=this.internalValue.slice(0,i).concat(this.internalValue.slice(i+1));this.$emit("input",r,this.id)}else this.$emit("input",null,this.id);this.closeOnSelect&&e&&this.deactivate()}},removeLastElement:function(){-1===this.blockKeys.indexOf("Delete")&&0===this.search.length&&Array.isArray(this.internalValue)&&this.removeElement(this.internalValue[this.internalValue.length-1],!1)},activate:function(){var t=this;this.isOpen||this.disabled||(this.adjustPosition(),this.groupValues&&0===this.pointer&&this.filteredOptions.length&&(this.pointer=1),this.isOpen=!0,this.searchable?(this.preserveSearch||(this.search=""),this.$nextTick(function(){return t.$refs.search.focus()})):this.$el.focus(),this.$emit("open",this.id))},deactivate:function(){this.isOpen&&(this.isOpen=!1,this.searchable?this.$refs.search.blur():this.$el.blur(),this.preserveSearch||(this.search=""),this.$emit("close",this.getValue(),this.id))},toggle:function(){this.isOpen?this.deactivate():this.activate()},adjustPosition:function(){if("undefined"!=typeof window){var t=this.$el.getBoundingClientRect().top,e=window.innerHeight-this.$el.getBoundingClientRect().bottom;e>this.maxHeight||e>t||"below"===this.openDirection||"bottom"===this.openDirection?(this.prefferedOpenDirection="below",this.optimizedHeight=Math.min(e-40,this.maxHeight)):(this.prefferedOpenDirection="above",this.optimizedHeight=Math.min(t-40,this.maxHeight))}}}}},function(t,e,n){"use strict";var i=n(54),r=(n.n(i),n(31));n.n(r);e.a={data:function(){return{pointer:0,pointerDirty:!1}},props:{showPointer:{type:Boolean,default:!0},optionHeight:{type:Number,default:40}},computed:{pointerPosition:function(){return this.pointer*this.optionHeight},visibleElements:function(){return this.optimizedHeight/this.optionHeight}},watch:{filteredOptions:function(){this.pointerAdjust()},isOpen:function(){this.pointerDirty=!1}},methods:{optionHighlight:function(t,e){return{"multiselect__option--highlight":t===this.pointer&&this.showPointer,"multiselect__option--selected":this.isSelected(e)}},groupHighlight:function(t,e){var n=this;if(!this.groupSelect)return["multiselect__option--group","multiselect__option--disabled"];var i=this.options.find(function(t){return t[n.groupLabel]===e.$groupLabel});return["multiselect__option--group",{"multiselect__option--highlight":t===this.pointer&&this.showPointer},{"multiselect__option--group-selected":this.wholeGroupSelected(i)}]},addPointerElement:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"Enter",e=t.key;this.filteredOptions.length>0&&this.select(this.filteredOptions[this.pointer],e),this.pointerReset()},pointerForward:function(){this.pointer<this.filteredOptions.length-1&&(this.pointer++,this.$refs.list.scrollTop<=this.pointerPosition-(this.visibleElements-1)*this.optionHeight&&(this.$refs.list.scrollTop=this.pointerPosition-(this.visibleElements-1)*this.optionHeight),this.filteredOptions[this.pointer]&&this.filteredOptions[this.pointer].$isLabel&&!this.groupSelect&&this.pointerForward()),this.pointerDirty=!0},pointerBackward:function(){this.pointer>0?(this.pointer--,this.$refs.list.scrollTop>=this.pointerPosition&&(this.$refs.list.scrollTop=this.pointerPosition),this.filteredOptions[this.pointer]&&this.filteredOptions[this.pointer].$isLabel&&!this.groupSelect&&this.pointerBackward()):this.filteredOptions[this.pointer]&&this.filteredOptions[0].$isLabel&&!this.groupSelect&&this.pointerForward(),this.pointerDirty=!0},pointerReset:function(){this.closeOnSelect&&(this.pointer=0,this.$refs.list&&(this.$refs.list.scrollTop=0))},pointerAdjust:function(){this.pointer>=this.filteredOptions.length-1&&(this.pointer=this.filteredOptions.length?this.filteredOptions.length-1:0),this.filteredOptions.length>0&&this.filteredOptions[this.pointer].$isLabel&&!this.groupSelect&&this.pointerForward()},pointerSet:function(t){this.pointer=t,this.pointerDirty=!0}}}},function(t,e,n){"use strict";var i=n(36),r=n(74),o=n(15),s=n(18);t.exports=n(72)(Array,"Array",function(t,e){this._t=s(t),this._i=0,this._k=e},function(){var t=this._t,e=this._k,n=this._i++;return!t||n>=t.length?(this._t=void 0,r(1)):"keys"==e?r(0,n):"values"==e?r(0,t[n]):r(0,[n,t[n]])},"values"),o.Arguments=o.Array,i("keys"),i("values"),i("entries")},function(t,e,n){"use strict";var i=n(31),r=(n.n(i),n(32)),o=n(33);e.a={name:"vue-multiselect",mixins:[r.a,o.a],props:{name:{type:String,default:""},selectLabel:{type:String,default:"Press enter to select"},selectGroupLabel:{type:String,default:"Press enter to select group"},selectedLabel:{type:String,default:"Selected"},deselectLabel:{type:String,default:"Press enter to remove"},deselectGroupLabel:{type:String,default:"Press enter to deselect group"},showLabels:{type:Boolean,default:!0},limit:{type:Number,default:99999},maxHeight:{type:Number,default:300},limitText:{type:Function,default:function(t){return"and ".concat(t," more")}},loading:{type:Boolean,default:!1},disabled:{type:Boolean,default:!1},openDirection:{type:String,default:""},showNoOptions:{type:Boolean,default:!0},showNoResults:{type:Boolean,default:!0},tabindex:{type:Number,default:0}},computed:{isSingleLabelVisible:function(){return this.singleValue&&(!this.isOpen||!this.searchable)&&!this.visibleValues.length},isPlaceholderVisible:function(){return!(this.internalValue.length||this.searchable&&this.isOpen)},visibleValues:function(){return this.multiple?this.internalValue.slice(0,this.limit):[]},singleValue:function(){return this.internalValue[0]},deselectLabelText:function(){return this.showLabels?this.deselectLabel:""},deselectGroupLabelText:function(){return this.showLabels?this.deselectGroupLabel:""},selectLabelText:function(){return this.showLabels?this.selectLabel:""},selectGroupLabelText:function(){return this.showLabels?this.selectGroupLabel:""},selectedLabelText:function(){return this.showLabels?this.selectedLabel:""},inputStyle:function(){if(this.searchable||this.multiple&&this.value&&this.value.length)return this.isOpen?{width:"auto"}:{width:"0",position:"absolute",padding:"0"}},contentStyle:function(){return this.options.length?{display:"inline-block"}:{display:"block"}},isAbove:function(){return"above"===this.openDirection||"top"===this.openDirection||"below"!==this.openDirection&&"bottom"!==this.openDirection&&"above"===this.prefferedOpenDirection},showSearchInput:function(){return this.searchable&&(!this.hasSingleSelectedSlot||!this.visibleSingleValue&&0!==this.visibleSingleValue||this.isOpen)}}}},function(t,e,n){var i=n(1)("unscopables"),r=Array.prototype;void 0==r[i]&&n(8)(r,i,{}),t.exports=function(t){r[i][t]=!0}},function(t,e,n){var i=n(18),r=n(19),o=n(85);t.exports=function(t){return function(e,n,s){var u,a=i(e),l=r(a.length),c=o(s,l);if(t&&n!=n){for(;l>c;)if((u=a[c++])!=u)return!0}else for(;l>c;c++)if((t||c in a)&&a[c]===n)return t||c||0;return!t&&-1}}},function(t,e,n){var i=n(9),r=n(1)("toStringTag"),o="Arguments"==i(function(){return arguments}()),s=function(t,e){try{return t[e]}catch(t){}};t.exports=function(t){var e,n,u;return void 0===t?"Undefined":null===t?"Null":"string"==typeof(n=s(e=Object(t),r))?n:o?i(e):"Object"==(u=i(e))&&"function"==typeof e.callee?"Arguments":u}},function(t,e,n){"use strict";var i=n(2);t.exports=function(){var t=i(this),e="";return t.global&&(e+="g"),t.ignoreCase&&(e+="i"),t.multiline&&(e+="m"),t.unicode&&(e+="u"),t.sticky&&(e+="y"),e}},function(t,e,n){var i=n(0).document;t.exports=i&&i.documentElement},function(t,e,n){t.exports=!n(4)&&!n(7)(function(){return 7!=Object.defineProperty(n(21)("div"),"a",{get:function(){return 7}}).a})},function(t,e,n){var i=n(9);t.exports=Array.isArray||function(t){return"Array"==i(t)}},function(t,e,n){"use strict";function i(t){var e,n;this.promise=new t(function(t,i){if(void 0!==e||void 0!==n)throw TypeError("Bad Promise constructor");e=t,n=i}),this.resolve=r(e),this.reject=r(n)}var r=n(14);t.exports.f=function(t){return new i(t)}},function(t,e,n){var i=n(2),r=n(76),o=n(22),s=n(27)("IE_PROTO"),u=function(){},a=function(){var t,e=n(21)("iframe"),i=o.length;for(e.style.display="none",n(40).appendChild(e),e.src="javascript:",t=e.contentWindow.document,t.open(),t.write("<script>document.F=Object<\/script>"),t.close(),a=t.F;i--;)delete a.prototype[o[i]];return a()};t.exports=Object.create||function(t,e){var n;return null!==t?(u.prototype=i(t),n=new u,u.prototype=null,n[s]=t):n=a(),void 0===e?n:r(n,e)}},function(t,e,n){var i=n(79),r=n(25),o=n(18),s=n(29),u=n(12),a=n(41),l=Object.getOwnPropertyDescriptor;e.f=n(4)?l:function(t,e){if(t=o(t),e=s(e,!0),a)try{return l(t,e)}catch(t){}if(u(t,e))return r(!i.f.call(t,e),t[e])}},function(t,e,n){var i=n(12),r=n(18),o=n(37)(!1),s=n(27)("IE_PROTO");t.exports=function(t,e){var n,u=r(t),a=0,l=[];for(n in u)n!=s&&i(u,n)&&l.push(n);for(;e.length>a;)i(u,n=e[a++])&&(~o(l,n)||l.push(n));return l}},function(t,e,n){var i=n(46),r=n(22);t.exports=Object.keys||function(t){return i(t,r)}},function(t,e,n){var i=n(2),r=n(5),o=n(43);t.exports=function(t,e){if(i(t),r(e)&&e.constructor===t)return e;var n=o.f(t);return(0,n.resolve)(e),n.promise}},function(t,e,n){var i=n(10),r=n(0),o=r["__core-js_shared__"]||(r["__core-js_shared__"]={});(t.exports=function(t,e){return o[t]||(o[t]=void 0!==e?e:{})})("versions",[]).push({version:i.version,mode:n(24)?"pure":"global",copyright:"© 2018 Denis Pushkarev (zloirock.ru)"})},function(t,e,n){var i=n(2),r=n(14),o=n(1)("species");t.exports=function(t,e){var n,s=i(t).constructor;return void 0===s||void 0==(n=i(s)[o])?e:r(n)}},function(t,e,n){var i=n(3),r=n(16),o=n(7),s=n(84),u="["+s+"]",a="​",l=RegExp("^"+u+u+"*"),c=RegExp(u+u+"*$"),f=function(t,e,n){var r={},u=o(function(){return!!s[t]()||a[t]()!=a}),l=r[t]=u?e(p):s[t];n&&(r[n]=l),i(i.P+i.F*u,"String",r)},p=f.trim=function(t,e){return t=String(r(t)),1&e&&(t=t.replace(l,"")),2&e&&(t=t.replace(c,"")),t};t.exports=f},function(t,e,n){var i,r,o,s=n(11),u=n(68),a=n(40),l=n(21),c=n(0),f=c.process,p=c.setImmediate,h=c.clearImmediate,d=c.MessageChannel,v=c.Dispatch,g=0,m={},y=function(){var t=+this;if(m.hasOwnProperty(t)){var e=m[t];delete m[t],e()}},b=function(t){y.call(t.data)};p&&h||(p=function(t){for(var e=[],n=1;arguments.length>n;)e.push(arguments[n++]);return m[++g]=function(){u("function"==typeof t?t:Function(t),e)},i(g),g},h=function(t){delete m[t]},"process"==n(9)(f)?i=function(t){f.nextTick(s(y,t,1))}:v&&v.now?i=function(t){v.now(s(y,t,1))}:d?(r=new d,o=r.port2,r.port1.onmessage=b,i=s(o.postMessage,o,1)):c.addEventListener&&"function"==typeof postMessage&&!c.importScripts?(i=function(t){c.postMessage(t+"","*")},c.addEventListener("message",b,!1)):i="onreadystatechange"in l("script")?function(t){a.appendChild(l("script")).onreadystatechange=function(){a.removeChild(this),y.call(t)}}:function(t){setTimeout(s(y,t,1),0)}),t.exports={set:p,clear:h}},function(t,e){var n=Math.ceil,i=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?i:n)(t)}},function(t,e,n){"use strict";var i=n(3),r=n(20)(5),o=!0;"find"in[]&&Array(1).find(function(){o=!1}),i(i.P+i.F*o,"Array",{find:function(t){return r(this,t,arguments.length>1?arguments[1]:void 0)}}),n(36)("find")},function(t,e,n){"use strict";var i,r,o,s,u=n(24),a=n(0),l=n(11),c=n(38),f=n(3),p=n(5),h=n(14),d=n(61),v=n(66),g=n(50),m=n(52).set,y=n(75)(),b=n(43),_=n(80),x=n(86),w=n(48),S=a.TypeError,O=a.process,L=O&&O.versions,P=L&&L.v8||"",k=a.Promise,T="process"==c(O),E=function(){},V=r=b.f,A=!!function(){try{var t=k.resolve(1),e=(t.constructor={})[n(1)("species")]=function(t){t(E,E)};return(T||"function"==typeof PromiseRejectionEvent)&&t.then(E)instanceof e&&0!==P.indexOf("6.6")&&-1===x.indexOf("Chrome/66")}catch(t){}}(),C=function(t){var e;return!(!p(t)||"function"!=typeof(e=t.then))&&e},j=function(t,e){if(!t._n){t._n=!0;var n=t._c;y(function(){for(var i=t._v,r=1==t._s,o=0;n.length>o;)!function(e){var n,o,s,u=r?e.ok:e.fail,a=e.resolve,l=e.reject,c=e.domain;try{u?(r||(2==t._h&&$(t),t._h=1),!0===u?n=i:(c&&c.enter(),n=u(i),c&&(c.exit(),s=!0)),n===e.promise?l(S("Promise-chain cycle")):(o=C(n))?o.call(n,a,l):a(n)):l(i)}catch(t){c&&!s&&c.exit(),l(t)}}(n[o++]);t._c=[],t._n=!1,e&&!t._h&&N(t)})}},N=function(t){m.call(a,function(){var e,n,i,r=t._v,o=D(t);if(o&&(e=_(function(){T?O.emit("unhandledRejection",r,t):(n=a.onunhandledrejection)?n({promise:t,reason:r}):(i=a.console)&&i.error&&i.error("Unhandled promise rejection",r)}),t._h=T||D(t)?2:1),t._a=void 0,o&&e.e)throw e.v})},D=function(t){return 1!==t._h&&0===(t._a||t._c).length},$=function(t){m.call(a,function(){var e;T?O.emit("rejectionHandled",t):(e=a.onrejectionhandled)&&e({promise:t,reason:t._v})})},M=function(t){var e=this;e._d||(e._d=!0,e=e._w||e,e._v=t,e._s=2,e._a||(e._a=e._c.slice()),j(e,!0))},F=function(t){var e,n=this;if(!n._d){n._d=!0,n=n._w||n;try{if(n===t)throw S("Promise can't be resolved itself");(e=C(t))?y(function(){var i={_w:n,_d:!1};try{e.call(t,l(F,i,1),l(M,i,1))}catch(t){M.call(i,t)}}):(n._v=t,n._s=1,j(n,!1))}catch(t){M.call({_w:n,_d:!1},t)}}};A||(k=function(t){d(this,k,"Promise","_h"),h(t),i.call(this);try{t(l(F,this,1),l(M,this,1))}catch(t){M.call(this,t)}},i=function(t){this._c=[],this._a=void 0,this._s=0,this._d=!1,this._v=void 0,this._h=0,this._n=!1},i.prototype=n(81)(k.prototype,{then:function(t,e){var n=V(g(this,k));return n.ok="function"!=typeof t||t,n.fail="function"==typeof e&&e,n.domain=T?O.domain:void 0,this._c.push(n),this._a&&this._a.push(n),this._s&&j(this,!1),n.promise},catch:function(t){return this.then(void 0,t)}}),o=function(){var t=new i;this.promise=t,this.resolve=l(F,t,1),this.reject=l(M,t,1)},b.f=V=function(t){return t===k||t===s?new o(t):r(t)}),f(f.G+f.W+f.F*!A,{Promise:k}),n(26)(k,"Promise"),n(83)("Promise"),s=n(10).Promise,f(f.S+f.F*!A,"Promise",{reject:function(t){var e=V(this);return(0,e.reject)(t),e.promise}}),f(f.S+f.F*(u||!A),"Promise",{resolve:function(t){return w(u&&this===s?k:this,t)}}),f(f.S+f.F*!(A&&n(73)(function(t){k.all(t).catch(E)})),"Promise",{all:function(t){var e=this,n=V(e),i=n.resolve,r=n.reject,o=_(function(){var n=[],o=0,s=1;v(t,!1,function(t){var u=o++,a=!1;n.push(void 0),s++,e.resolve(t).then(function(t){a||(a=!0,n[u]=t,--s||i(n))},r)}),--s||i(n)});return o.e&&r(o.v),n.promise},race:function(t){var e=this,n=V(e),i=n.reject,r=_(function(){v(t,!1,function(t){e.resolve(t).then(n.resolve,i)})});return r.e&&i(r.v),n.promise}})},function(t,e,n){"use strict";var i=n(3),r=n(10),o=n(0),s=n(50),u=n(48);i(i.P+i.R,"Promise",{finally:function(t){var e=s(this,r.Promise||o.Promise),n="function"==typeof t;return this.then(n?function(n){return u(e,t()).then(function(){return n})}:t,n?function(n){return u(e,t()).then(function(){throw n})}:t)}})},function(t,e,n){"use strict";function i(t){n(99)}var r=n(35),o=n(101),s=n(100),u=i,a=s(r.a,o.a,!1,u,null,null);e.a=a.exports},function(t,e,n){"use strict";function i(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}e.a=i},function(t,e,n){"use strict";function i(t){return(i="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(t)}function r(t){return(r="function"==typeof Symbol&&"symbol"===i(Symbol.iterator)?function(t){return i(t)}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":i(t)})(t)}e.a=r},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var i=n(34),r=(n.n(i),n(55)),o=(n.n(r),n(56)),s=(n.n(o),n(57)),u=n(32),a=n(33);n.d(e,"Multiselect",function(){return s.a}),n.d(e,"multiselectMixin",function(){return u.a}),n.d(e,"pointerMixin",function(){return a.a}),e.default=s.a},function(t,e){t.exports=function(t,e,n,i){if(!(t instanceof e)||void 0!==i&&i in t)throw TypeError(n+": incorrect invocation!");return t}},function(t,e,n){var i=n(14),r=n(28),o=n(23),s=n(19);t.exports=function(t,e,n,u,a){i(e);var l=r(t),c=o(l),f=s(l.length),p=a?f-1:0,h=a?-1:1;if(n<2)for(;;){if(p in c){u=c[p],p+=h;break}if(p+=h,a?p<0:f<=p)throw TypeError("Reduce of empty array with no initial value")}for(;a?p>=0:f>p;p+=h)p in c&&(u=e(u,c[p],p,l));return u}},function(t,e,n){var i=n(5),r=n(42),o=n(1)("species");t.exports=function(t){var e;return r(t)&&(e=t.constructor,"function"!=typeof e||e!==Array&&!r(e.prototype)||(e=void 0),i(e)&&null===(e=e[o])&&(e=void 0)),void 0===e?Array:e}},function(t,e,n){var i=n(63);t.exports=function(t,e){return new(i(t))(e)}},function(t,e,n){"use strict";var i=n(8),r=n(6),o=n(7),s=n(16),u=n(1);t.exports=function(t,e,n){var a=u(t),l=n(s,a,""[t]),c=l[0],f=l[1];o(function(){var e={};return e[a]=function(){return 7},7!=""[t](e)})&&(r(String.prototype,t,c),i(RegExp.prototype,a,2==e?function(t,e){return f.call(t,this,e)}:function(t){return f.call(t,this)}))}},function(t,e,n){var i=n(11),r=n(70),o=n(69),s=n(2),u=n(19),a=n(87),l={},c={},e=t.exports=function(t,e,n,f,p){var h,d,v,g,m=p?function(){return t}:a(t),y=i(n,f,e?2:1),b=0;if("function"!=typeof m)throw TypeError(t+" is not iterable!");if(o(m)){for(h=u(t.length);h>b;b++)if((g=e?y(s(d=t[b])[0],d[1]):y(t[b]))===l||g===c)return g}else for(v=m.call(t);!(d=v.next()).done;)if((g=r(v,y,d.value,e))===l||g===c)return g};e.BREAK=l,e.RETURN=c},function(t,e,n){var i=n(5),r=n(82).set;t.exports=function(t,e,n){var o,s=e.constructor;return s!==n&&"function"==typeof s&&(o=s.prototype)!==n.prototype&&i(o)&&r&&r(t,o),t}},function(t,e){t.exports=function(t,e,n){var i=void 0===n;switch(e.length){case 0:return i?t():t.call(n);case 1:return i?t(e[0]):t.call(n,e[0]);case 2:return i?t(e[0],e[1]):t.call(n,e[0],e[1]);case 3:return i?t(e[0],e[1],e[2]):t.call(n,e[0],e[1],e[2]);case 4:return i?t(e[0],e[1],e[2],e[3]):t.call(n,e[0],e[1],e[2],e[3])}return t.apply(n,e)}},function(t,e,n){var i=n(15),r=n(1)("iterator"),o=Array.prototype;t.exports=function(t){return void 0!==t&&(i.Array===t||o[r]===t)}},function(t,e,n){var i=n(2);t.exports=function(t,e,n,r){try{return r?e(i(n)[0],n[1]):e(n)}catch(e){var o=t.return;throw void 0!==o&&i(o.call(t)),e}}},function(t,e,n){"use strict";var i=n(44),r=n(25),o=n(26),s={};n(8)(s,n(1)("iterator"),function(){return this}),t.exports=function(t,e,n){t.prototype=i(s,{next:r(1,n)}),o(t,e+" Iterator")}},function(t,e,n){"use strict";var i=n(24),r=n(3),o=n(6),s=n(8),u=n(15),a=n(71),l=n(26),c=n(78),f=n(1)("iterator"),p=!([].keys&&"next"in[].keys()),h=function(){return this};t.exports=function(t,e,n,d,v,g,m){a(n,e,d);var y,b,_,x=function(t){if(!p&&t in L)return L[t];switch(t){case"keys":case"values":return function(){return new n(this,t)}}return function(){return new n(this,t)}},w=e+" Iterator",S="values"==v,O=!1,L=t.prototype,P=L[f]||L["@@iterator"]||v&&L[v],k=P||x(v),T=v?S?x("entries"):k:void 0,E="Array"==e?L.entries||P:P;if(E&&(_=c(E.call(new t)))!==Object.prototype&&_.next&&(l(_,w,!0),i||"function"==typeof _[f]||s(_,f,h)),S&&P&&"values"!==P.name&&(O=!0,k=function(){return P.call(this)}),i&&!m||!p&&!O&&L[f]||s(L,f,k),u[e]=k,u[w]=h,v)if(y={values:S?k:x("values"),keys:g?k:x("keys"),entries:T},m)for(b in y)b in L||o(L,b,y[b]);else r(r.P+r.F*(p||O),e,y);return y}},function(t,e,n){var i=n(1)("iterator"),r=!1;try{var o=[7][i]();o.return=function(){r=!0},Array.from(o,function(){throw 2})}catch(t){}t.exports=function(t,e){if(!e&&!r)return!1;var n=!1;try{var o=[7],s=o[i]();s.next=function(){return{done:n=!0}},o[i]=function(){return s},t(o)}catch(t){}return n}},function(t,e){t.exports=function(t,e){return{value:e,done:!!t}}},function(t,e,n){var i=n(0),r=n(52).set,o=i.MutationObserver||i.WebKitMutationObserver,s=i.process,u=i.Promise,a="process"==n(9)(s);t.exports=function(){var t,e,n,l=function(){var i,r;for(a&&(i=s.domain)&&i.exit();t;){r=t.fn,t=t.next;try{r()}catch(i){throw t?n():e=void 0,i}}e=void 0,i&&i.enter()};if(a)n=function(){s.nextTick(l)};else if(!o||i.navigator&&i.navigator.standalone)if(u&&u.resolve){var c=u.resolve(void 0);n=function(){c.then(l)}}else n=function(){r.call(i,l)};else{var f=!0,p=document.createTextNode("");new o(l).observe(p,{characterData:!0}),n=function(){p.data=f=!f}}return function(i){var r={fn:i,next:void 0};e&&(e.next=r),t||(t=r,n()),e=r}}},function(t,e,n){var i=n(13),r=n(2),o=n(47);t.exports=n(4)?Object.defineProperties:function(t,e){r(t);for(var n,s=o(e),u=s.length,a=0;u>a;)i.f(t,n=s[a++],e[n]);return t}},function(t,e,n){var i=n(46),r=n(22).concat("length","prototype");e.f=Object.getOwnPropertyNames||function(t){return i(t,r)}},function(t,e,n){var i=n(12),r=n(28),o=n(27)("IE_PROTO"),s=Object.prototype;t.exports=Object.getPrototypeOf||function(t){return t=r(t),i(t,o)?t[o]:"function"==typeof t.constructor&&t instanceof t.constructor?t.constructor.prototype:t instanceof Object?s:null}},function(t,e){e.f={}.propertyIsEnumerable},function(t,e){t.exports=function(t){try{return{e:!1,v:t()}}catch(t){return{e:!0,v:t}}}},function(t,e,n){var i=n(6);t.exports=function(t,e,n){for(var r in e)i(t,r,e[r],n);return t}},function(t,e,n){var i=n(5),r=n(2),o=function(t,e){if(r(t),!i(e)&&null!==e)throw TypeError(e+": can't set as prototype!")};t.exports={set:Object.setPrototypeOf||("__proto__"in{}?function(t,e,i){try{i=n(11)(Function.call,n(45).f(Object.prototype,"__proto__").set,2),i(t,[]),e=!(t instanceof Array)}catch(t){e=!0}return function(t,n){return o(t,n),e?t.__proto__=n:i(t,n),t}}({},!1):void 0),check:o}},function(t,e,n){"use strict";var i=n(0),r=n(13),o=n(4),s=n(1)("species");t.exports=function(t){var e=i[t];o&&e&&!e[s]&&r.f(e,s,{configurable:!0,get:function(){return this}})}},function(t,e){t.exports="\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff"},function(t,e,n){var i=n(53),r=Math.max,o=Math.min;t.exports=function(t,e){return t=i(t),t<0?r(t+e,0):o(t,e)}},function(t,e,n){var i=n(0),r=i.navigator;t.exports=r&&r.userAgent||""},function(t,e,n){var i=n(38),r=n(1)("iterator"),o=n(15);t.exports=n(10).getIteratorMethod=function(t){if(void 0!=t)return t[r]||t["@@iterator"]||o[i(t)]}},function(t,e,n){"use strict";var i=n(3),r=n(20)(2);i(i.P+i.F*!n(17)([].filter,!0),"Array",{filter:function(t){return r(this,t,arguments[1])}})},function(t,e,n){"use strict";var i=n(3),r=n(37)(!1),o=[].indexOf,s=!!o&&1/[1].indexOf(1,-0)<0;i(i.P+i.F*(s||!n(17)(o)),"Array",{indexOf:function(t){return s?o.apply(this,arguments)||0:r(this,t,arguments[1])}})},function(t,e,n){var i=n(3);i(i.S,"Array",{isArray:n(42)})},function(t,e,n){"use strict";var i=n(3),r=n(20)(1);i(i.P+i.F*!n(17)([].map,!0),"Array",{map:function(t){return r(this,t,arguments[1])}})},function(t,e,n){"use strict";var i=n(3),r=n(62);i(i.P+i.F*!n(17)([].reduce,!0),"Array",{reduce:function(t){return r(this,t,arguments.length,arguments[1],!1)}})},function(t,e,n){var i=Date.prototype,r=i.toString,o=i.getTime;new Date(NaN)+""!="Invalid Date"&&n(6)(i,"toString",function(){var t=o.call(this);return t===t?r.call(this):"Invalid Date"})},function(t,e,n){n(4)&&"g"!=/./g.flags&&n(13).f(RegExp.prototype,"flags",{configurable:!0,get:n(39)})},function(t,e,n){n(65)("search",1,function(t,e,n){return[function(n){"use strict";var i=t(this),r=void 0==n?void 0:n[e];return void 0!==r?r.call(n,i):new RegExp(n)[e](String(i))},n]})},function(t,e,n){"use strict";n(94);var i=n(2),r=n(39),o=n(4),s=/./.toString,u=function(t){n(6)(RegExp.prototype,"toString",t,!0)};n(7)(function(){return"/a/b"!=s.call({source:"a",flags:"b"})})?u(function(){var t=i(this);return"/".concat(t.source,"/","flags"in t?t.flags:!o&&t instanceof RegExp?r.call(t):void 0)}):"toString"!=s.name&&u(function(){return s.call(this)})},function(t,e,n){"use strict";n(51)("trim",function(t){return function(){return t(this,3)}})},function(t,e,n){for(var i=n(34),r=n(47),o=n(6),s=n(0),u=n(8),a=n(15),l=n(1),c=l("iterator"),f=l("toStringTag"),p=a.Array,h={CSSRuleList:!0,CSSStyleDeclaration:!1,CSSValueList:!1,ClientRectList:!1,DOMRectList:!1,DOMStringList:!1,DOMTokenList:!0,DataTransferItemList:!1,FileList:!1,HTMLAllCollection:!1,HTMLCollection:!1,HTMLFormElement:!1,HTMLSelectElement:!1,MediaList:!0,MimeTypeArray:!1,NamedNodeMap:!1,NodeList:!0,PaintRequestList:!1,Plugin:!1,PluginArray:!1,SVGLengthList:!1,SVGNumberList:!1,SVGPathSegList:!1,SVGPointList:!1,SVGStringList:!1,SVGTransformList:!1,SourceBufferList:!1,StyleSheetList:!0,TextTrackCueList:!1,TextTrackList:!1,TouchList:!1},d=r(h),v=0;v<d.length;v++){var g,m=d[v],y=h[m],b=s[m],_=b&&b.prototype;if(_&&(_[c]||u(_,c,p),_[f]||u(_,f,m),a[m]=p,y))for(g in i)_[g]||o(_,g,i[g],!0)}},function(t,e){},function(t,e){t.exports=function(t,e,n,i,r,o){var s,u=t=t||{},a=typeof t.default;"object"!==a&&"function"!==a||(s=t,u=t.default);var l="function"==typeof u?u.options:u;e&&(l.render=e.render,l.staticRenderFns=e.staticRenderFns,l._compiled=!0),n&&(l.functional=!0),r&&(l._scopeId=r);var c;if(o?(c=function(t){t=t||this.$vnode&&this.$vnode.ssrContext||this.parent&&this.parent.$vnode&&this.parent.$vnode.ssrContext,t||"undefined"==typeof __VUE_SSR_CONTEXT__||(t=__VUE_SSR_CONTEXT__),i&&i.call(this,t),t&&t._registeredComponents&&t._registeredComponents.add(o)},l._ssrRegister=c):i&&(c=i),c){var f=l.functional,p=f?l.render:l.beforeCreate;f?(l._injectStyles=c,l.render=function(t,e){return c.call(e),p(t,e)}):l.beforeCreate=p?[].concat(p,c):[c]}return{esModule:s,exports:u,options:l}}},function(t,e,n){"use strict";var i=function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"multiselect",class:{"multiselect--active":t.isOpen,"multiselect--disabled":t.disabled,"multiselect--above":t.isAbove},attrs:{tabindex:t.searchable?-1:t.tabindex},on:{focus:function(e){t.activate()},blur:function(e){!t.searchable&&t.deactivate()},keydown:[function(e){return"button"in e||!t._k(e.keyCode,"down",40,e.key,["Down","ArrowDown"])?e.target!==e.currentTarget?null:(e.preventDefault(),void t.pointerForward()):null},function(e){return"button"in e||!t._k(e.keyCode,"up",38,e.key,["Up","ArrowUp"])?e.target!==e.currentTarget?null:(e.preventDefault(),void t.pointerBackward()):null},function(e){return"button"in e||!t._k(e.keyCode,"enter",13,e.key,"Enter")||!t._k(e.keyCode,"tab",9,e.key,"Tab")?(e.stopPropagation(),e.target!==e.currentTarget?null:void t.addPointerElement(e)):null}],keyup:function(e){if(!("button"in e)&&t._k(e.keyCode,"esc",27,e.key,"Escape"))return null;t.deactivate()}}},[t._t("caret",[n("div",{staticClass:"multiselect__select",on:{mousedown:function(e){e.preventDefault(),e.stopPropagation(),t.toggle()}}})],{toggle:t.toggle}),t._v(" "),t._t("clear",null,{search:t.search}),t._v(" "),n("div",{ref:"tags",staticClass:"multiselect__tags"},[t._t("selection",[n("div",{directives:[{name:"show",rawName:"v-show",value:t.visibleValues.length>0,expression:"visibleValues.length > 0"}],staticClass:"multiselect__tags-wrap"},[t._l(t.visibleValues,function(e,i){return[t._t("tag",[n("span",{key:i,staticClass:"multiselect__tag"},[n("span",{domProps:{textContent:t._s(t.getOptionLabel(e))}}),t._v(" "),n("i",{staticClass:"multiselect__tag-icon",attrs:{"aria-hidden":"true",tabindex:"1"},on:{keydown:function(n){if(!("button"in n)&&t._k(n.keyCode,"enter",13,n.key,"Enter"))return null;n.preventDefault(),t.removeElement(e)},mousedown:function(n){n.preventDefault(),t.removeElement(e)}}})])],{option:e,search:t.search,remove:t.removeElement})]})],2),t._v(" "),t.internalValue&&t.internalValue.length>t.limit?[t._t("limit",[n("strong",{staticClass:"multiselect__strong",domProps:{textContent:t._s(t.limitText(t.internalValue.length-t.limit))}})])]:t._e()],{search:t.search,remove:t.removeElement,values:t.visibleValues,isOpen:t.isOpen}),t._v(" "),n("transition",{attrs:{name:"multiselect__loading"}},[t._t("loading",[n("div",{directives:[{name:"show",rawName:"v-show",value:t.loading,expression:"loading"}],staticClass:"multiselect__spinner"})])],2),t._v(" "),t.searchable?n("input",{ref:"search",staticClass:"multiselect__input",style:t.inputStyle,attrs:{name:t.name,id:t.id,type:"text",autocomplete:"off",placeholder:t.placeholder,disabled:t.disabled,tabindex:t.tabindex},domProps:{value:t.search},on:{input:function(e){t.updateSearch(e.target.value)},focus:function(e){e.preventDefault(),t.activate()},blur:function(e){e.preventDefault(),t.deactivate()},keyup:function(e){if(!("button"in e)&&t._k(e.keyCode,"esc",27,e.key,"Escape"))return null;t.deactivate()},keydown:[function(e){if(!("button"in e)&&t._k(e.keyCode,"down",40,e.key,["Down","ArrowDown"]))return null;e.preventDefault(),t.pointerForward()},function(e){if(!("button"in e)&&t._k(e.keyCode,"up",38,e.key,["Up","ArrowUp"]))return null;e.preventDefault(),t.pointerBackward()},function(e){return"button"in e||!t._k(e.keyCode,"enter",13,e.key,"Enter")?(e.preventDefault(),e.stopPropagation(),e.target!==e.currentTarget?null:void t.addPointerElement(e)):null},function(e){if(!("button"in e)&&t._k(e.keyCode,"delete",[8,46],e.key,["Backspace","Delete"]))return null;e.stopPropagation(),t.removeLastElement()}]}}):t._e(),t._v(" "),t.isSingleLabelVisible?n("span",{staticClass:"multiselect__single",on:{mousedown:function(e){return e.preventDefault(),t.toggle(e)}}},[t._t("singleLabel",[[t._v(t._s(t.currentOptionLabel))]],{option:t.singleValue})],2):t._e(),t._v(" "),t.isPlaceholderVisible?n("span",{staticClass:"multiselect__placeholder",on:{mousedown:function(e){return e.preventDefault(),t.toggle(e)}}},[t._t("placeholder",[t._v("\n            "+t._s(t.placeholder)+"\n        ")])],2):t._e()],2),t._v(" "),n("transition",{attrs:{name:"multiselect"}},[n("div",{directives:[{name:"show",rawName:"v-show",value:t.isOpen,expression:"isOpen"}],ref:"list",staticClass:"multiselect__content-wrapper",style:{maxHeight:t.optimizedHeight+"px"},attrs:{tabindex:"-1"},on:{focus:t.activate,mousedown:function(t){t.preventDefault()}}},[n("ul",{staticClass:"multiselect__content",style:t.contentStyle},[t._t("beforeList"),t._v(" "),t.multiple&&t.max===t.internalValue.length?n("li",[n("span",{staticClass:"multiselect__option"},[t._t("maxElements",[t._v("Maximum of "+t._s(t.max)+" options selected. First remove a selected option to select another.")])],2)]):t._e(),t._v(" "),!t.max||t.internalValue.length<t.max?t._l(t.filteredOptions,function(e,i){return n("li",{key:i,staticClass:"multiselect__element"},[e&&(e.$isLabel||e.$isDisabled)?t._e():n("span",{staticClass:"multiselect__option",class:t.optionHighlight(i,e),attrs:{"data-select":e&&e.isTag?t.tagPlaceholder:t.selectLabelText,"data-selected":t.selectedLabelText,"data-deselect":t.deselectLabelText},on:{click:function(n){n.stopPropagation(),t.select(e)},mouseenter:function(e){if(e.target!==e.currentTarget)return null;t.pointerSet(i)}}},[t._t("option",[n("span",[t._v(t._s(t.getOptionLabel(e)))])],{option:e,search:t.search})],2),t._v(" "),e&&(e.$isLabel||e.$isDisabled)?n("span",{staticClass:"multiselect__option",class:t.groupHighlight(i,e),attrs:{"data-select":t.groupSelect&&t.selectGroupLabelText,"data-deselect":t.groupSelect&&t.deselectGroupLabelText},on:{mouseenter:function(e){if(e.target!==e.currentTarget)return null;t.groupSelect&&t.pointerSet(i)},mousedown:function(n){n.preventDefault(),t.selectGroup(e)}}},[t._t("option",[n("span",[t._v(t._s(t.getOptionLabel(e)))])],{option:e,search:t.search})],2):t._e()])}):t._e(),t._v(" "),n("li",{directives:[{name:"show",rawName:"v-show",value:t.showNoResults&&0===t.filteredOptions.length&&t.search&&!t.loading,expression:"showNoResults && (filteredOptions.length === 0 && search && !loading)"}]},[n("span",{staticClass:"multiselect__option"},[t._t("noResult",[t._v("No elements found. Consider changing the search query.")])],2)]),t._v(" "),n("li",{directives:[{name:"show",rawName:"v-show",value:t.showNoOptions&&0===t.options.length&&!t.search&&!t.loading,expression:"showNoOptions && (options.length === 0 && !search && !loading)"}]},[n("span",{staticClass:"multiselect__option"},[t._t("noOptions",[t._v("List is empty.")])],2)]),t._v(" "),t._t("afterList")],2)])])],2)},r=[],o={render:i,staticRenderFns:r};e.a=o}])});

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 20:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(12);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("01398020", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-0f2f2b77\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MultipleQuickReport.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-0f2f2b77\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MultipleQuickReport.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'size': { default: 'modal-lg' },
        'id': { required: true },
        'contentSize': { default: '12' }

    }
});

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['log', 'logcount'],
  methods: {
    // select(id){
    //this.$parent.selectItem(id)
    // },
  }
});

/***/ }),

/***/ 246:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			defaultDocumentTypes: [],

			images: [],
			files: [],
			documentTypes: [],
			issuedAt: [],
			expiredAt: []
		};
	},


	methods: {
		imagePreview: function imagePreview(input) {
			var _this = this;

			if (input.files) {
				var validMimeType = ['data:image/jpeg', 'data:image/jpg', 'data:image/png'];
				var filesAmount = input.files.length;

				for (var i = 0; i < filesAmount; i++) {
					var reader = new FileReader();

					reader.onload = function (event) {
						var img = event.target.result;
						var lastCharIndex = img.indexOf(';');
						var mimeType = img.substr(0, lastCharIndex);
						if (!validMimeType.includes(mimeType)) {
							toastr.error('You can\'t upload files of this type.');
						} else {
							_this.images.push(event.target.result);
							_this.documentTypes.push(null);
							_this.issuedAt.push(null);
							_this.expiredAt.push(null);
						}
					};

					reader.readAsDataURL(input.files[i]);
				}

				setTimeout(function () {
					$('.zoom').magnify();
				}, 1000);
			}
		},
		handleFilesUpload: function handleFilesUpload() {
			this.files.push(this.$refs.files.files);
		},
		remove: function remove(index) {
			this.images.splice(index, 1);
			this.files.splice(index, 1);
			this.documentTypes.splice(index, 1);
			this.issuedAt.splice(index, 1);
			this.expiredAt.splice(index, 1);

			setTimeout(function () {
				$('.zoom').magnify();
			}, 500);
		},
		getDocumentTypes: function getDocumentTypes() {
			var _this2 = this;

			axios.get('/visa/service-manager/client-documents').then(function (response) {
				_this2.defaultDocumentTypes = response.data;
			});
		},
		validate: function validate() {
			var success = true;
			var length = this.images.length;

			if (length == 0) {
				success = false;
				toastr.error('Document/s is required.');
			} else {
				for (var i = 0; i < length; i++) {
					if (this.documentTypes[i] == null) {
						success = false;
						toastr.error('[' + (i + 1) + ']' + ' The document type field is required.');
					}

					if (this.issuedAt[i] == null) {
						success = false;
						toastr.error('[' + (i + 1) + ']' + ' The issued at field is required.');
					} else if (!moment(this.issuedAt[i], 'YYYY-MM-DD').isValid()) {
						success = false;
						toastr.error('[' + (i + 1) + ']' + ' Incorrect issued at field format.');
					}

					if (this.expiredAt[i] == null) {
						success = false;
						toastr.error('[' + (i + 1) + ']' + ' The expired at field is required.');
					} else if (!moment(this.expiredAt[i], 'YYYY-MM-DD').isValid()) {
						success = false;
						toastr.error('[' + (i + 1) + ']' + ' Incorrect expired at field format.');
					}
				}
			}

			return success;
		},
		addDocuments: function addDocuments() {
			var _this3 = this;

			if (this.validate()) {
				var formData = new FormData();

				for (var i = 0; i < this.files.length; i++) {
					var file = this.files[i][0];
					formData.append('attachments[' + i + ']', file);
				}

				formData.append('userId', window.Laravel.data.id);

				for (var _i = 0; _i < this.images.length; _i++) {
					formData.append('images[' + _i + ']', this.images[_i]);
					formData.append('documentTypes[' + _i + ']', this.documentTypes[_i]);
					formData.append('issuedAt[' + _i + ']', this.issuedAt[_i]);
					formData.append('expiredAt[' + _i + ']', this.expiredAt[_i]);
				}

				axios.post('/visa/service-manager/client-documents/upload-documents-v2', formData, {
					headers: {
						'Content-Type': 'multipart/form-data'
					}
				}).then(function (response) {
					if (response.data.success) {
						toastr.success('Successfully saved.');

						_this3.images = [];
						_this3.files = [];
						_this3.documentTypes = [];
						_this3.issuedAt = [];
						_this3.expiredAt = [];

						_this3.$parent.$parent.getFiles();

						$('#image-upload').val(null);

						$('#addDocumentsModal').modal('hide');
					}
				});
			}
		}
	},

	created: function created() {
		this.getDocumentTypes();
	},
	mounted: function mounted() {
		var vm = this;

		$('#image-upload').on('change', function () {
			vm.imagePreview(this);
		});
	}
});

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['log2'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    }
  },
  computed: {
    detail: function detail() {
      var res = this.log2.data.title;
      return res.replace("</br>", " ");
    }
  }
});

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['tracking', 'client_id'],

	data: function data() {
		return {
			loading: false,

			servicesList: [],

			cost: '',

			clients: [],

			detailArray: [],
			requiredDocs: [],
			optionalDocs: [],

			showCheckboxAll: false,

			addNewServiceForm: new Form({
				services: [],
				note: '',
				discount: '',
				reason: '',
				client_id: this.$parent.$parent.usr_id,
				track: this.$parent.$parent.tracking_selected,
				serviceid: '',
				docs: '',
				req_docs: ''
			}, { baseURL: '/visa/client/' })
		};
	},


	methods: {
		fetchServiceList: function fetchServiceList() {
			var _this = this;

			axios.get('/visa/service-manager/show').then(function (response) {
				_this.servicesList = response.data.filter(function (r) {
					return r.parent_id != 0;
				});
				setTimeout(function () {
					$(".chosen-select-for-service").trigger("chosen:updated");
				}, 1000);
			}).catch(function (error) {
				return console.log(error);
			});
		},
		resetAddNewServiceForm: function resetAddNewServiceForm() {
			this.addNewServiceForm.services = [];
			this.addNewServiceForm.note = '';
			this.addNewServiceForm.discount = '';
			this.addNewServiceForm.reason = '';

			this.cost = '';
			// Deselect All
			$('.chosen-select-for-service, .chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');

			this.detailArray = [];
			this.requiredDocs = [];
			this.optionalDocs = [];
		},
		addNewService: function addNewService() {
			var _this2 = this;

			this.loading = true;

			this.addNewServiceForm.track = this.$parent.$parent.tracking_selected;

			var docsArray = [];
			var requiredDocs = '';
			var optionalDocs = '';
			var hasRequiredDocsErrors = [];
			for (var i = 0; i < this.detailArray.length; i++) {
				$('.chosen-select-for-required-docs-' + i + ' option:selected').each(function (e) {
					requiredDocs += $(this).val() + ',';
				});

				var requiredOptions = $('.chosen-select-for-required-docs-' + i + ' option').length;
				var requiredCount = $('.chosen-select-for-required-docs-' + i + ' option:selected').length;
				if (requiredCount == 0 && requiredOptions > 0) {
					hasRequiredDocsErrors.push(this.detailArray[i]);
				}

				$('.chosen-select-for-optional-docs-' + i + ' option:selected').each(function (e) {
					optionalDocs += $(this).val() + ',';
				});

				docsArray.push((requiredDocs + optionalDocs).slice(0, -1));
				requiredDocs = '';
				optionalDocs = '';
			}
			this.addNewServiceForm.docs = docsArray;

			if (this.addNewServiceForm.discount > 0 && this.addNewServiceForm.reason == '') {
				toastr.error('Please input reason.');
				this.loading = false;
			} else if (hasRequiredDocsErrors.length > 0) {
				for (var _i = 0; _i < hasRequiredDocsErrors.length; _i++) {
					toastr.error('Please select from required documents.', hasRequiredDocsErrors[_i]);
				}
				this.loading = false;
			} else if (this.addNewServiceForm.services.length > 0) {
				this.addNewServiceForm.submit('post', '/clientAddService').then(function (response) {
					if (response.success) {
						// this.$parent.$parent.quickReportClientsId = response.clientsId;
						// this.$parent.$parent.quickReportClientServicesId = response.clientServicesId;
						// this.$parent.$parent.quickReportTrackings = response.trackings;

						_this2.resetAddNewServiceForm();
						var client_id = _this2.$parent.$parent.usr_id;
						var track = _this2.$parent.$parent.tracking_selected;
						_this2.$parent.$parent.showServicesUnder(client_id, track);
						_this2.$parent.$parent.updateTrackingList(client_id);
						_this2.$parent.$parent.reloadComputation();
						var owl = $("#owl-demo");
						owl.data('owlCarousel').destroy();

						// this.$parent.$parent.$refs.multiplequickreportref.fetchDocs(response.clientServicesId);

						$('#add-new-service-modal').modal('hide');

						toastr.success(response.message);
					}
					_this2.loading = false;
				}).catch(function (error) {
					for (var key in error) {
						if (error.hasOwnProperty(key)) toastr.error(error[key][0]);
					}
					_this2.loading = false;
				});
			} else {
				toastr.error('Please select service category.');
				this.loading = false;
			}
		},
		fetchDocs: function fetchDocs(service_id, needed, optional) {
			var _this3 = this;

			$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');
			this.addNewServiceForm.serviceid = service_id;

			if (this.addNewServiceForm.services.length == 1) {
				if (needed == '' || needed === undefined) {
					this.requiredDocs = [];
				} else {
					axios.get('/visa/group/' + needed + '/service-docs').then(function (result) {
						_this3.requiredDocs = result.data;
					});
				}

				if (optional == '' || optional === undefined) {
					this.optionalDocs = [];
				} else {
					axios.get('/visa/group/' + optional + '/service-docs').then(function (result) {
						_this3.optionalDocs = result.data;
					});
				}
			} else {
				this.requiredDocs = [];
				this.optionalDocs = [];
			}

			setTimeout(function () {
				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');
			}, 2000);
		},
		getDocs: function getDocs(servicesId) {
			var _this4 = this;

			axios.get('/visa/get-docs', {
				params: {
					servicesId: servicesId
				}
			}).then(function (result) {
				_this4.detailArray = result.data.detailArray;
				_this4.requiredDocs = result.data.docsNeededArray;
				_this4.optionalDocs = result.data.docsOptionalArray;

				setTimeout(function () {
					$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen('destroy');
					$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({
						width: "100%",
						no_results_text: "No result/s found.",
						search_contains: true
					});

					$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');
				}, 1000);
			});
		},
		initChosenSelect: function initChosenSelect() {
			$('.chosen-select-for-service').chosen({
				width: "100%",
				no_results_text: "No result/s found.",
				search_contains: true
			});

			var vm = this;

			$('.chosen-select-for-service').on('change', function () {
				var services = $(this).val();

				vm.showCheckboxAll = services.length > 0 ? true : false;

				vm.getDocs(services);

				vm.addNewServiceForm.services = services;

				// Set cost input
				if (services.length == 1) {
					vm.servicesList.map(function (s) {
						if (s.id == services[0]) {
							vm.cost = s.cost;
						}
					});
				} else {
					vm.cost = '';
				}
			});
		},
		callDataTable: function callDataTable() {
			$('.add-new-service-datatable').DataTable().destroy();

			setTimeout(function () {
				$('.add-new-service-datatable').DataTable({
					pageLength: 10,
					responsive: true,
					dom: '<"top"lf>rt<"bottom"ip><"clear">'
				});
			}, 1000);
		},
		checkAll: function checkAll(e) {
			var checkbox = e.target.className;

			if (checkbox == 'checkbox-all') {
				if ($('.' + checkbox).is(':checked')) $('.chosen-select-for-required-docs option').prop('selected', true);else $('.chosen-select-for-required-docs option').prop('selected', false);

				$('.chosen-select-for-required-docs option').trigger('chosen:updated');
			} else {
				var chosen = e.target.dataset.chosenClass;

				if ($('.' + checkbox).is(':checked')) $('.' + chosen + ' option').prop('selected', true);else $('.' + chosen + ' option').prop('selected', false);

				$('.' + chosen + ' option').trigger('chosen:updated');
			}
		}
	},

	created: function created() {
		this.fetchServiceList();
	},
	mounted: function mounted() {
		this.initChosenSelect();
	}
});

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['tracking', 'client_id'],

	data: function data() {
		return {

			servicesList: [],

			cost: '',

			clients: [],

			requiredDocs: [],

			optionalDocs: []

		};
	},

	computed: {},
	methods: {
		resetEditServiceForm: function resetEditServiceForm() {
			this.$parent.$parent.editServ.cost = 0;
			this.$parent.$parent.editServ.tip = 0;
			this.$parent.$parent.editServ.status = "";
			this.$parent.$parent.editServ.active = 0;
			this.$parent.$parent.editServ.discount = 0;
			this.$parent.$parent.editServ.reason = "";
			this.$parent.$parent.editServ.note = "";
			this.$parent.$parent.editServ.id = null;
			this.$parent.$parent.editServ.reporting = '';
			this.$parent.$parent.editServ.extend = "";
			this.$parent.$parent.editServ.rcv_docs = "";
		},
		editClientService: function editClientService() {
			var _this = this;

			var requiredDocs = '';
			$('.chosen-select-for-required-docs option:selected').each(function (e) {
				requiredDocs += $(this).val() + ',';
			});

			var optionalDocs = '';
			$('.chosen-select-for-optional-docs option:selected').each(function (e) {
				optionalDocs += $(this).val() + ',';
			});

			var docs = requiredDocs + optionalDocs;
			this.$parent.$parent.editServ.rcv_docs = docs.slice(0, -1);

			if (this.$parent.$parent.editServ.discount > 0 && this.$parent.$parent.editServ.reason == null) {
				toastr.error('Please input reason.');
			} else if (requiredDocs == '' && this.requiredDocs.length != 0) {
				toastr.error('Please select Required Documents.');
			} else {
				this.$parent.$parent.editServ.submit('post', '/clientEditService').then(function (response) {
					if (response.success) {
						_this.resetEditServiceForm();
						var client_id = _this.$parent.$parent.usr_id;
						var track = _this.$parent.$parent.tracking_selected;
						_this.$parent.$parent.showServicesUnder(client_id, track);
						_this.$parent.$parent.updateTrackingList(client_id);
						_this.$parent.$parent.reloadComputation();
						var owl = $("#owl-demo");
						owl.data('owlCarousel').destroy();
						$(".chosen-select-for-required-docs, .chosen-select-for-optional-docs").val('').trigger('chosen:updated');
						$('#edit-service-modal').modal('hide');
						toastr.success('Successfully Updated!');
					}
				}).catch(function (error) {
					for (var key in error) {
						if (error.hasOwnProperty(key)) toastr.error(error[key][0]);
					}
				});
			}
		},
		fetchDocs: function fetchDocs(id) {
			var _this2 = this;

			$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');

			if (this.$parent.$parent.editServ.rcv_docs == '') var rcv = id[0].rcv != null ? id[0].rcv.split(",") : '';else var rcv = this.$parent.$parent.editServ.rcv_docs.split(",");

			axios.get('/visa/group/' + id[0].docs_needed + '/service-docs').then(function (result) {
				_this2.requiredDocs = result.data;
			});

			axios.get('/visa/group/' + id[0].docs_optional + '/service-docs').then(function (result) {
				_this2.optionalDocs = result.data;
			});

			setTimeout(function () {
				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen('destroy');
				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({
					width: "100%",
					no_results_text: "No result/s found.",
					search_contains: true
				});

				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val(rcv).trigger('chosen:updated');
				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');
			}, 2000);
		},
		callDataTable: function callDataTable() {
			$('.add-new-service-datatable').DataTable().destroy();

			setTimeout(function () {
				$('.add-new-service-datatable').DataTable({
					pageLength: 10,
					responsive: true,
					dom: '<"top"lf>rt<"bottom"ip><"clear">'
				});
			}, 1000);
		}
	},

	created: function created() {},
	mounted: function mounted() {
		// DatePicker
		$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			autoclose: true,
			format: "yyyy-mm-dd"
		}).on('changeDate', function (e) {
			// Bootstrap Datepicker onChange
			var id = e.target.id;
			var date = e.target.value;

			console.log('ID : ' + id);
			console.log('DATE : ' + date);
			this.$parent.$parent.editServ.extend = date;
		}.bind(this)).on('change', function (e) {
			// native onChange
			var id = e.target.id;
			var date = e.target.value;

			console.log('ID : ' + id);
			console.log('DATE : ' + date);
			this.$parent.$parent.editServ.extend = date;
		}.bind(this));
	}
});

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['pack'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    },
    showServicesUnder: function showServicesUnder() {
      this.$parent.showServicesUnder(this.pack.client_id, this.pack.tracking);
    }
  },
  computed: {
    status: function status() {
      switch (this.pack.status) {
        case '0':
          return 'Empty';
          break;
        case '1':
          return 'On Process';
          break;
        case '2':
          return 'Pending';
          break;
        case '4':
          return 'Complete';
          break;
        default:
          return 'Complete';
      }
    },
    bg: function bg() {
      switch (this.pack.status) {
        case '0':
          return ' bg-red';
          break;
        case '1':
          return ' bg-green';
          break;
        case '2':
          return ' bg-yellow';
          break;
        case '4':
          return ' bg-aqua';
          break;
        default:
          return 'bg-aqua';
      }
    }
  }
});

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['serv'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    },
    showServicesUnder: function showServicesUnder() {
      this.$parent.showServicesUnder(this.serv.client_id, this.serv.tracking);
    },
    editService: function editService(service) {
      this.$parent.editService(this.serv.id);
      this.$parent.fetchDocs2([service]);
    },
    writeReport: function writeReport(service) {
      this.$parent.fetchDocs([service]);

      this.$parent.quickReportClientsId = [service.client_id];

      this.$parent.quickReportClientServicesId = [service];

      this.$parent.quickReportTrackings = [service.tracking];

      $('#add-quick-report-modal').modal('show');
    },
    addDocs: function addDocs(service) {
      this.$parent.fetchDocs2([service]);
      $('#docs-modal').modal('show');
    }
  },
  computed: {
    isDisabled: function isDisabled() {
      return this.serv.active > 0 ? "" : "text-decoration: line-through;";
    },
    bg: function bg() {
      return;
    }
  }
});

/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(24),
  /* template */
  __webpack_require__(32),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/clients/ActionLogs.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ActionLogs.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-72b755b5", Component.options)
  } else {
    hotAPI.reload("data-v-72b755b5", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(7)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(25),
  /* template */
  __webpack_require__(31),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/clients/TransactionLogs.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] TransactionLogs.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6aefbfa6", Component.options)
  } else {
    hotAPI.reload("data-v-6aefbfa6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\ndiv.add-documents-section[data-v-722ae35a] {\n\t\theight: 300px;\n\t\tposition: relative;\n\t\toverflow: hidden;\n\t\tbackground-color: #ffffff;\n\t\tcolor: #ecf0f1;\n}\ndiv.add-documents-section input[data-v-722ae35a] {\n\t  \tline-height: 200px;\n\t  \tfont-size: 200px;\n\t  \tposition: absolute;\n\t  \topacity: 0;\n\t  \tz-index: 10;\n}\ndiv.add-documents-section label[data-v-722ae35a] {\n  \t\tposition: absolute;\n  \t\tz-index: 5;\n\t\tcursor: pointer;\n\t\tbackground-color: #4ABC96;\n\t\twidth: 200px;\n\t\theight: 50px;\n\t\tfont-size: 20px;\n\t\tline-height: 50px;\n\t\ttext-transform: uppercase;\n\t\ttop: 0;\n\t\tleft: 0;\n\t\tright: 0;\n\t\tbottom: 0;\n\t\tmargin: auto;\n\t\ttext-align: center;\n}\ndiv.form-section[data-v-722ae35a] {\n\t\theight: auto;\n}\ndiv.form-section img.document-image[data-v-722ae35a] {\n\t\theight: 200px;\n}\nform[data-v-722ae35a] {\n\t\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-722ae35a] {\n\t\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["ClientDocuments.vue?030cce68"],"names":[],"mappings":";AA+OA;EACA,cAAA;EACA,mBAAA;EACA,iBAAA;EACA,0BAAA;EACA,eAAA;CACA;AACA;IACA,mBAAA;IACA,iBAAA;IACA,mBAAA;IACA,WAAA;IACA,YAAA;CACA;AACA;IACA,mBAAA;IACA,WAAA;EACA,gBAAA;EACA,0BAAA;EACA,aAAA;EACA,aAAA;EACA,gBAAA;EACA,kBAAA;EACA,0BAAA;EACA,OAAA;EACA,QAAA;EACA,SAAA;EACA,UAAA;EACA,aAAA;EACA,mBAAA;CACA;AACA;EACA,aAAA;CACA;AACA;EACA,cAAA;CACA;AACA;EACA,oBAAA;CACA;AACA;EACA,mBAAA;CACA","file":"ClientDocuments.vue","sourcesContent":["<template>\n\t\n\t<div class=\"row\">\n\n\t\t<form @submit.prevent=\"addDocuments\">\n\n\t\t\t<div class=\"col-md-4\" v-for=\"(image, index) in images\">\n\t\t\t\t<div class=\"panel panel-default form-section\">\n\t                <div class=\"panel-body\">\n\t                \t<center>\n\t                \t\t<img :src=\"image\" class=\"img-responsive img-thumbnail document-image zoom\" :data-magnify-src=\"image\">\n\t                \t</center>\n\n\t                \t<div style=\"height:10px;clear:both;\"></div>\n\n\t                \t<p>Document Type:</p>\n\t                \t<div class=\"form-group\">\n\t                \t\t<select class=\"form-control\" v-model=\"documentTypes[index]\">\n\t                \t\t\t<option v-for=\"documentType in defaultDocumentTypes\" :value=\"documentType.id\">\n\t                \t\t\t\t{{ documentType.name }}\n\t                \t\t\t</option>\n\t                \t\t</select>\n\t                \t</div>\n\n\t                \t<p>Issued At:</p>\n\t                \t<div class=\"form-group\">\n\t\t\t                <div class=\"input-group date\">\n\t\t\t\t                <span class=\"input-group-addon\">\n\t\t\t\t                    <i class=\"fa fa-calendar\"></i>\n\t\t\t\t                </span>\n\t\t\t\t                <input type=\"date\" class=\"form-control\" v-model=\"issuedAt[index]\">\n\t\t\t\t            </div>\n\t\t\t\t        </div>\n\n\t\t\t            <p>Expired At:</p>\n\t\t\t            <div class=\"form-group\">\n\t\t\t                <div class=\"input-group date\">\n\t\t\t\t                <span class=\"input-group-addon\">\n\t\t\t\t                    <i class=\"fa fa-calendar\"></i>\n\t\t\t\t                </span>\n\t\t\t\t                <input type=\"date\" class=\"form-control\" v-model=\"expiredAt[index]\">\n\t\t\t\t            </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"form-group\">\n\t\t\t\t        \t<button type=\"button\" class=\"btn btn-danger btn-xs pull-right\" @click=\"remove(index)\">Remove</button>\n\t\t\t\t        </div>\n\n\t                </div>\n\t            </div>\n\t        </div>\n\n\t        <div class=\"col-md-4\">\n\t\t\t\t<div class=\"panel panel-default add-documents-section\">\n\t                <div class=\"panel-body\">\n\t                    <label for=\"image-upload\" id=\"image-label\"><i class=\"fa fa-plus\"></i> Choose File</label>\n\t  \t\t\t\t\t<input type=\"file\" name=\"images\" ref=\"files\" @change=\"handleFilesUpload\" id=\"image-upload\" multiple />\n\t                </div>\n\t            </div>\n\t\t\t</div>\n\n\t\t\t<div style=\"height:1px;clear:both;\"></div>\n\n\t\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Add Document/s</button>\n\t    \t<button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t\t</form>\n\n    </div>\n\n</template>\n\n<script>\n\n\texport default {\n\t\tdata() {\n\t\t\treturn {\n\t\t\t\tdefaultDocumentTypes: [],\n\n\t\t\t\timages: [],\n\t\t\t\tfiles: [],\n\t\t\t\tdocumentTypes: [],\n\t\t\t\tissuedAt: [],\n\t\t\t\texpiredAt: []\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\t\t\timagePreview(input) {\n\t\t\t\tif(input.files) {\n\t\t\t\t\tvar validMimeType = ['data:image/jpeg', 'data:image/jpg', 'data:image/png'];\n\t\t            var filesAmount = input.files.length;\n\n\t\t            for(let i = 0; i < filesAmount; i++) {\n\t\t                var reader = new FileReader();\n\n\t\t                reader.onload = (event) => {\n\t\t                \tlet img = event.target.result;\n\t\t\t\t\t\t\tlet lastCharIndex = img.indexOf(';');\n\t\t\t\t\t\t\tlet mimeType = img.substr(0, lastCharIndex);\n\t\t\t\t\t\t\tif(!validMimeType.includes(mimeType)) {\n\t\t\t\t\t\t\t\ttoastr.error('You can\\'t upload files of this type.');\n\t\t\t\t\t\t\t} else {\n\t\t\t\t\t\t\t\tthis.images.push(event.target.result);\n\t\t\t\t\t\t\t\tthis.documentTypes.push(null);\n\t\t\t\t\t\t\t\tthis.issuedAt.push(null);\n\t\t\t\t\t\t\t\tthis.expiredAt.push(null);\n\t\t\t\t\t\t\t}\n\t\t                }\n\n\t\t                reader.readAsDataURL(input.files[i]);\n\t\t            }\n\n\t\t            setTimeout(() => {\n\t\t            \t$('.zoom').magnify();\n\t\t            }, 1000);\n\t\t        }\n\t\t\t},\n\n\t\t\thandleFilesUpload() {\n\t\t\t\tthis.files.push(this.$refs.files.files);\n\t\t\t},\n\n\t\t\tremove(index) {\n\t\t\t\tthis.images.splice(index, 1);\n\t\t\t\tthis.files.splice(index, 1);\n\t\t\t\tthis.documentTypes.splice(index, 1);\n\t\t\t\tthis.issuedAt.splice(index, 1);\n\t\t\t\tthis.expiredAt.splice(index, 1);\n\n\t\t\t\tsetTimeout(() => {\n\t\t            $('.zoom').magnify();\n\t\t        }, 500);\n\t\t\t},\n\n\t\t\tgetDocumentTypes() {\n\t\t\t\taxios.get('/visa/service-manager/client-documents')\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tthis.defaultDocumentTypes = response.data;\n\t\t\t\t\t});\n\t\t\t},\n\n\t  \t\tvalidate() {\n\t  \t\t\tvar success = true;\n\t  \t\t\tlet length = this.images.length;\n\n\t  \t\t\tif(length == 0) {\n\t  \t\t\t\tsuccess = false;\n\t  \t\t\t\ttoastr.error('Document/s is required.');\n\t  \t\t\t} else {\n\t\t  \t\t\tfor(let i=0; i<length; i++) {\n\t\t  \t\t\t\tif(this.documentTypes[i] == null) {\n\t\t  \t\t\t\t\tsuccess = false;\n\t\t  \t\t\t\t\ttoastr.error('['+(i+1)+']' + ' The document type field is required.');\n\t\t  \t\t\t\t}\n\n\t\t  \t\t\t\tif(this.issuedAt[i] == null) {\n\t\t  \t\t\t\t\tsuccess = false;\n\t\t  \t\t\t\t\ttoastr.error('['+(i+1)+']' + ' The issued at field is required.');\n\t\t  \t\t\t\t} else if(!moment(this.issuedAt[i], 'YYYY-MM-DD').isValid()) {\n\t\t  \t\t\t\t\tsuccess = false;\n\t\t  \t\t\t\t\ttoastr.error('['+(i+1)+']' + ' Incorrect issued at field format.');\n\t\t  \t\t\t\t}\n\n\t\t  \t\t\t\tif(this.expiredAt[i] == null) {\n\t\t  \t\t\t\t\tsuccess = false;\n\t\t  \t\t\t\t\ttoastr.error('['+(i+1)+']' + ' The expired at field is required.');\n\t\t  \t\t\t\t} else if(!moment(this.expiredAt[i], 'YYYY-MM-DD').isValid()) {\n\t\t  \t\t\t\t\tsuccess = false;\n\t\t  \t\t\t\t\ttoastr.error('['+(i+1)+']' + ' Incorrect expired at field format.');\n\t\t  \t\t\t\t}\n\t\t  \t\t\t}\n\t  \t\t\t}\n\n\t  \t\t\treturn success;\n\t  \t\t},\n\n\t  \t\taddDocuments() {\n\t  \t\t\tif(this.validate()) {\n\t  \t\t\t\tlet formData = new FormData();\n\n\t\t\t\t\tfor(let i=0; i<this.files.length; i++) {\n\t\t\t\t\t    let file = this.files[i][0];\n\t\t\t\t\t    formData.append('attachments['+i+']', file);\n\t\t\t\t\t}\n\n\t\t\t\t\tformData.append('userId', window.Laravel.data.id);\n\n\t\t\t\t\tfor(let i=0; i<this.images.length; i++) {\n\t\t\t\t\t\tformData.append('images['+i+']', this.images[i]);\n\t\t\t\t\t\tformData.append('documentTypes['+i+']', this.documentTypes[i]);\n\t\t\t\t\t\tformData.append('issuedAt['+i+']', this.issuedAt[i]);\n\t\t\t\t\t\tformData.append('expiredAt['+i+']', this.expiredAt[i]);\n\t\t\t\t\t}\n\n\t\t\t\t\taxios.post('/visa/service-manager/client-documents/upload-documents-v2',\n\t\t\t\t\t\tformData,\n\t\t\t\t\t\t{\n\t\t\t\t\t\t    headers: {\n\t\t\t\t\t\t        'Content-Type': 'multipart/form-data'\n\t\t\t\t\t\t    }\n\t\t\t\t\t\t}\n\t\t\t\t\t)\n\t\t            .then((response) => {\n\t\t               \tif(response.data.success) {\n\t\t                \ttoastr.success('Successfully saved.');\n\n\t\t\t\t\t\t\tthis.images = [];\n\t\t\t\t\t\t\tthis.files = [];\n\t\t\t\t\t\t\tthis.documentTypes = [];\n\t\t\t\t\t\t\tthis.issuedAt = [];\n\t\t\t\t\t\t\tthis.expiredAt = [];\n\n\t\t\t\t\t\t\tthis.$parent.$parent.getFiles();\n\n\t\t\t\t\t\t\t$('#image-upload').val(null);\n\n\t\t\t\t\t\t\t$('#addDocumentsModal').modal('hide');\n\t\t\t\t\t\t}\n\t\t            });\n\t  \t\t\t}\n\t  \t\t}\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.getDocumentTypes();\n\t\t},\n\n\t\tmounted () {\n\t\t\tlet vm = this;\n\n\t\t\t$('#image-upload').on('change', function() {\n\t\t        vm.imagePreview(this);\n\t\t    });\n\t\t}\n\t}\n</script>\n\n<style scoped>\n\tdiv.add-documents-section {\n\t\theight: 300px;\n\t\tposition: relative;\n\t\toverflow: hidden;\n\t\tbackground-color: #ffffff;\n\t\tcolor: #ecf0f1;\n\t}\n\tdiv.add-documents-section input {\n\t  \tline-height: 200px;\n\t  \tfont-size: 200px;\n\t  \tposition: absolute;\n\t  \topacity: 0;\n\t  \tz-index: 10;\n\t}\n\tdiv.add-documents-section label {\n  \t\tposition: absolute;\n  \t\tz-index: 5;\n\t\tcursor: pointer;\n\t\tbackground-color: #4ABC96;\n\t\twidth: 200px;\n\t\theight: 50px;\n\t\tfont-size: 20px;\n\t\tline-height: 50px;\n\t\ttext-transform: uppercase;\n\t\ttop: 0;\n\t\tleft: 0;\n\t\tright: 0;\n\t\tbottom: 0;\n\t\tmargin: auto;\n\t\ttext-align: center;\n\t}\n\tdiv.form-section {\n\t\theight: auto;\n\t}\n\tdiv.form-section img.document-image {\n\t\theight: 200px;\n\t}\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 31:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.log2.year != null) ? _c('div', {
    staticClass: "row timeline-movement timeline-movement-top no-dashed no-side-margin"
  }, [_c('div', {
    staticClass: "timeline-badge timeline-filter-movement"
  }, [_c('a', {
    attrs: {
      "href": "#"
    }
  }, [_c('span', [_vm._v(_vm._s(_vm.log2.year))])])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "row timeline-movement  no-side-margin"
  }, [(_vm.log2.month != null) ? _c('div', {
    staticClass: "timeline-badge"
  }, [_c('span', {
    staticClass: "timeline-balloon-date-day"
  }, [_vm._v(_vm._s(_vm.log2.day))]), _vm._v(" "), _c('span', {
    staticClass: "timeline-balloon-date-month"
  }, [_vm._v(_vm._s(_vm.log2.month))])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "col-sm-12  timeline-item"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-2 col-sm-offset-1"
  }, [_c('div', {
    staticClass: "timeline-panel debits"
  }, [_c('ul', {
    staticClass: "timeline-panel-ul"
  }, [_c('li', [_c('span', {
    staticClass: "causale"
  }, [_vm._v("Balance: "), _c('b', [_vm._v(_vm._s(_vm._f("currency")(_vm.log2.data.balance)))])])]), _vm._v(" "), _c('li', [_c('p', [_c('small', {
    staticClass: "text-muted"
  }, [_vm._v("Amount : "), _c('b', [_vm._v(_vm._s(_vm._f("currency")(_vm.log2.data.amount)))])])])]), _vm._v(" "), _c('li', [_c('p', [_c('small', {
    staticClass: "text-muted"
  }, [_vm._v("Type : "), _c('b', [_vm._v(_vm._s(_vm.log2.data.type))])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-9"
  }, [_c('div', {
    staticClass: "timeline-panel debits"
  }, [_c('ul', {
    staticClass: "timeline-panel-ul"
  }, [_c('li', [_c('span', {
    staticClass: "importo",
    domProps: {
      "innerHTML": _vm._s(this.detail)
    }
  })]), _vm._v(" "), _c('li', [_c('span', {
    staticClass: "causale"
  }, [_vm._v(_vm._s(_vm.log2.data.processor))])]), _vm._v(" "), _c('li', [_c('p', [_c('small', {
    staticClass: "text-muted"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-time"
  }), _vm._v(_vm._s(_vm.log2.data.date))])])])])])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6aefbfa6", module.exports)
  }
}

/***/ }),

/***/ 310:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-b2c16ff8] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-b2c16ff8] {\n\tmargin-right: 10px;\n}\n.checkbox-inline[data-v-b2c16ff8] {\n\tmargin-top: -7px;\n}\n", "", {"version":3,"sources":["AddService.vue?38ce6376"],"names":[],"mappings":";AAiaA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA;AACA;CACA,iBAAA;CACA","file":"AddService.vue","sourcesContent":["<template>\n\t<form class=\"form-horizontal\" @submit.prevent=\"addNewService\">\n\n\t\t<div class=\"ios-scroll\">\n\t    \t<div class=\"form-group\" :class=\"{ 'has-error': addNewServiceForm.errors.has('services') }\">\n\t\t        <label class=\"col-md-2 control-label\">\n\t\t            Service:\n\t\t        </label>\n\n\t\t        <div class=\"col-md-10\">\n\t\t            <select data-placeholder=\"Select Service\" class=\"chosen-select-for-service\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t                <option v-for=\"service in servicesList\" :value=\"service.id\" :data-docs-needed=\"service.docs_needed\" :data-docs-optional=\"service.docs_optional\">\n\t\t                \t{{ (service.detail).trim() }}\n\t\t                </option>\n\t\t            </select>\n\n\t\t            <span class=\"help-block\" v-if=\"addNewServiceForm.errors.has('services')\" v-text=\"addNewServiceForm.errors.get('services')\"></span>\n\n\t\t            <div class=\"spacer-10\"></div>\n\n\t\t            <p>\n\t\t            \tIf you want to view all services at once, please \n\t\t            \t<a href=\"/visa/service/view-all\" target=\"_blank\">click here</a>\n\t\t            </p>\n\t\t        </div>\n\t\t    </div>\n\n\t\t    <div class=\"form-group\">\n\t\t    \t<label class=\"col-md-2 control-label\">\n\t\t            Note:\n\t\t        </label>\n\t\t        <div class=\"col-md-10\">\n\t\t        \t<input type=\"text\" class=\"form-control\" name=\"note\" v-model=\"addNewServiceForm.note\" placeholder=\"for internal use only, only employees can see.\">\n\t\t        </div>\n\t\t    </div>\n\n\t\t    <div v-show=\"addNewServiceForm.services.length <= 1\">\n\t\t    \t<div classs=\"col-md-12\">\n\t\t\t\t    <div class=\"form-group col-md-6\">\n\t\t\t\t    \t<label class=\"col-md-4 control-label \">\n\t\t\t\t            Cost:\n\t\t\t\t        </label>\n\t\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t        \t<input type=\"text\" class=\"form-control\" name=\"cost\" :value=\"cost\" readonly style=\"width : 175px;\">\n\t\t\t\t        </div>\n\t\t\t\t    </div>\n\n\t\t\t\t    <div class=\"form-group  col-md-6 m-l-2\" :class=\"{ 'has-error': addNewServiceForm.errors.has('discount') }\">\n\t\t\t\t    \t<label class=\"col-md-4 control-label \">\n\t\t\t\t            Discount:\n\t\t\t\t        </label>\n\t\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t        \t<input type=\"number\" class=\"form-control\" name=\"discount\" v-model=\"addNewServiceForm.discount\" min=\"0\" style=\"width : 175px;\">\n\t\t\t\t        \t<span class=\"help-block\" v-if=\"addNewServiceForm.errors.has('discount')\" v-text=\"addNewServiceForm.errors.get('discount')\"></span>\n\t\t\t\t        </div>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\n\n\t\t\t    <div class=\"form-group\" v-if=\"addNewServiceForm.discount > 0\">\n\t\t\t    \t<label class=\"col-md-2 control-label\">\n\t\t\t            Reason:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-10\">\n\t\t\t        \t<input type=\"text\" class=\"form-control\" name=\"reason\" v-model=\"addNewServiceForm.reason\">\n\t\t\t        </div>\n\t\t\t    </div>\n\t\t    </div>\n\n\t\t    <div v-show=\"showCheckboxAll\">\n\t\t\t    <div style=\"height:20px;clear:both;\"></div> <!-- spacer -->\n\n\t\t\t    <label class=\"checkbox-inline\">\n\t    \t\t\t<input type=\"checkbox\" @click=\"checkAll\" class=\"checkbox-all\"> Check all required documents\n\t    \t\t</label>\n    \t\t</div>\n\n\t\t    <div style=\"height:20px;clear:both;\"></div> <!-- spacer -->\n\n            <div v-for=\"(detail, index) in detailArray\" class=\"panel panel-default\">\n\t            <div class=\"panel-heading\">\n\t                {{ detail }}\n\n\t                <label class=\"checkbox-inline pull-right\">\n\t\t\t            <input type=\"checkbox\" @click=\"checkAll\" :class=\"'checkbox-' + index\" :data-chosen-class=\"'chosen-select-for-required-docs-' + index\"> Check all required documents\n\t\t\t        </label>\n\t            </div>\n\t            <div class=\"panel-body\">\n\n\t                <div class=\"form-group\">\n\t\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t\t       \tRequired Documents:\n\t\t\t\t\t    </label>\n\n\t\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t            <select data-placeholder=\"Select Docs\" class=\"chosen-select-for-required-docs\" :class=\"'chosen-select-for-required-docs-' + index\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t\t\t                <option v-for=\"doc in requiredDocs[index]\" :value=\"doc.id\">\n\t\t\t\t                \t{{ doc.title }}\n\t\t\t\t                </option>\n\t\t\t\t            </select>\n\t\t\t\t\t    </div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t\t       \tOptional Documents:\n\t\t\t\t\t    </label>\n\n\t\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t            <select data-placeholder=\"Select Docs\" class=\"chosen-select-for-optional-docs\" :class=\"'chosen-select-for-optional-docs-' + index\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t\t\t                <option v-for=\"doc in optionalDocs[index]\" :value=\"doc.id\">\n\t\t\t\t                \t{{ doc.title }}\n\t\t\t\t                </option>\n\t\t\t\t            </select>\n\t\t\t\t\t    </div>\n\t\t\t\t\t</div>\n\n\t            </div>\n\t        </div>\n\n    \t</div>\n\n    \t<button v-if=\"!loading\" type=\"submit\" class=\"btn btn-primary pull-right\">Save</button>\n    \t<button v-if=\"loading\" type=\"button\" class=\"btn btn-default pull-right\">\n            <div class=\"sk-spinner sk-spinner-wave\" style=\"width:40px; height:20px;\">\n                <div class=\"sk-rect1\"></div>\n                <div class=\"sk-rect2\"></div>\n                <div class=\"sk-rect3\"></div>\n                <div class=\"sk-rect4\"></div>\n                <div class=\"sk-rect5\"></div>\n             </div>\n        </button>\n\t    <a class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</a>\n    </form>\n\n</template>\n\n<script>\n    export default{\n    \tprops: ['tracking','client_id'],\n\n        data() {\n        \treturn {\n        \t\tloading: false,\n\n        \t\tservicesList: [],\n\n        \t\tcost: '',\n\n        \t\tclients: [],\n\n        \t\tdetailArray: [],\n        \t\trequiredDocs: [],\n        \t\toptionalDocs: [],\n\n        \t\tshowCheckboxAll: false,\n\n        \t\taddNewServiceForm: new Form({\n\t\t\t\t\tservices: [],\n\t\t\t\t\tnote: '',\n\t\t\t\t\tdiscount: '',\n\t\t\t\t\treason: '',\n\t\t\t\t\tclient_id: this.$parent.$parent.usr_id,\n\t\t\t\t\ttrack: this.$parent.$parent.tracking_selected,\n\t\t\t\t\tserviceid: '',\n        \t\t\tdocs: '',\n        \t\t\treq_docs:''\t\t\t\t\t\n\t\t\t\t},{ baseURL  : '/visa/client/'}),\t\n        \t}\n        },\n\n        methods: {\n\n        \tfetchServiceList() {\n        \t\taxios.get('/visa/service-manager/show')\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tthis.servicesList = response.data.filter(r => r.parent_id != 0);\n\t\t\t\t\t\tsetTimeout(() => {\n\t\t\t\t\t\t\t$(\".chosen-select-for-service\").trigger(\"chosen:updated\");\n\t\t\t\t\t\t}, 1000);\n\t\t\t\t\t\t\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => console.log(error) );\n        \t},\n\n        \tresetAddNewServiceForm() {\n        \t\tthis.addNewServiceForm.services = [];\n        \t\tthis.addNewServiceForm.note = '';\n        \t\tthis.addNewServiceForm.discount = '';\n        \t\tthis.addNewServiceForm.reason = '';\n\n        \t\tthis.cost = '';\n        \t\t// Deselect All\n\t\t\t\t$('.chosen-select-for-service, .chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');\n\t\t\t\t\n\t\t\t\tthis.detailArray = [];\n\t\t\t\tthis.requiredDocs = [];\n\t\t\t\tthis.optionalDocs = [];\n        \t},\n\n        \taddNewService() {\n        \t\tthis.loading = true;\n\n        \t\tthis.addNewServiceForm.track = this.$parent.$parent.tracking_selected;\n\n        \t\tvar docsArray = [];\n        \t\tvar requiredDocs = '';\n        \t\tvar optionalDocs = '';\n        \t\tvar hasRequiredDocsErrors = [];\n        \t\tfor(let i=0; i<this.detailArray.length; i++) {\n\t        \t\t$('.chosen-select-for-required-docs-' + i +' option:selected').each( function(e) {\n\t\t\t\t        requiredDocs += $(this).val() + ',';\n\t\t\t\t    });\n\n\t        \t\tlet requiredOptions = $('.chosen-select-for-required-docs-' + i +' option').length;\n\t        \t\tlet requiredCount = $('.chosen-select-for-required-docs-' + i +' option:selected').length;\n\t        \t\tif(requiredCount == 0 && requiredOptions > 0) {\n\t        \t\t\thasRequiredDocsErrors.push(this.detailArray[i]);\n\t        \t\t}\n\n\t\t\t\t    $('.chosen-select-for-optional-docs-' + i + ' option:selected').each( function(e) {\n\t\t\t\t        optionalDocs += $(this).val() + ',';\n\t\t\t\t    });\n\n\t\t\t\t    docsArray.push((requiredDocs + optionalDocs).slice(0, -1));\n\t\t\t\t    requiredDocs = '';\n\t\t\t\t    optionalDocs = '';\n\t        \t}\n        \t\tthis.addNewServiceForm.docs = docsArray;\n\n        \t\tif(this.addNewServiceForm.discount > 0 && this.addNewServiceForm.reason == ''){\n\t\t\t        toastr.error('Please input reason.');\n\t\t\t        this.loading = false;\n        \t\t}\n        \t\telse if(hasRequiredDocsErrors.length > 0){\n        \t\t\tfor(let i=0; i<hasRequiredDocsErrors.length; i++) {\n        \t\t\t\ttoastr.error('Please select from required documents.', hasRequiredDocsErrors[i]);\n        \t\t\t}\n        \t\t\tthis.loading = false;\n        \t\t}\n        \t\telse if((this.addNewServiceForm.services).length > 0){\n        \t\t\tthis.addNewServiceForm.submit('post','/clientAddService')\n\t            \t.then(response => {\n\t\t                if(response.success) {\n\t\t                \t\t// this.$parent.$parent.quickReportClientsId = response.clientsId;\n\t\t\t                \t// this.$parent.$parent.quickReportClientServicesId = response.clientServicesId;\n\t\t\t                \t// this.$parent.$parent.quickReportTrackings = response.trackings;\n\n\t\t\t                \tthis.resetAddNewServiceForm();\n\t\t\t                \tvar client_id = this.$parent.$parent.usr_id;\n\t\t\t                \tvar track = this.$parent.$parent.tracking_selected;\n\t\t\t\t\t\t\t\tthis.$parent.$parent.showServicesUnder(client_id,track);\n\t\t\t\t\t\t\t\tthis.$parent.$parent.updateTrackingList(client_id);\n\t\t\t\t\t\t\t\tthis.$parent.$parent.reloadComputation();\n\t\t\t\t\t\t\t\tvar owl = $(\"#owl-demo\");\n\t\t\t\t\t\t\t\towl.data('owlCarousel').destroy();\n\n\t\t\t\t\t\t\t\t// this.$parent.$parent.$refs.multiplequickreportref.fetchDocs(response.clientServicesId);\n\n\t\t\t                \t$('#add-new-service-modal').modal('hide');\n\n\t\t\t                \ttoastr.success(response.message);\n\t\t\t                }\n\t\t\t            this.loading = false;\n\t\t            })\n\t\t            .catch(error =>{\n\t\t                for(var key in error) {\n\t\t\t            \t\tif(error.hasOwnProperty(key)) toastr.error(error[key][0])\n\t\t\t            \t}\n\t\t\t            this.loading = false;\n\t\t            });\n        \t\t}\n        \t\telse{\n\t\t\t        toastr.error('Please select service category.');\n\t\t\t        this.loading = false;\n        \t\t}\n        \t\t\n\t\t\t},\n\n\t\t\tfetchDocs(service_id,needed,optional){\n\t\t\t\t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');\n\t\t\t\tthis.addNewServiceForm.serviceid = service_id;\n\n\t\t\t\tif(this.addNewServiceForm.services.length == 1){\n\t\t\t\t\tif(needed=='' || needed===undefined){\n\t\t\t\t\t\tthis.requiredDocs = [];\n\t\t\t\t\t} else {\n\t\t\t\t\t \taxios.get('/visa/group/'+needed+'/service-docs')\n\t\t\t\t\t \t  .then(result => {\n\t\t\t\t            this.requiredDocs = result.data;\n\t\t\t\t        });\n\t\t\t\t\t} \n\n\t\t        \tif(optional=='' || optional===undefined){\n\t\t        \t\tthis.optionalDocs = [];\n\t\t        \t} else {\n\t\t\t\t\t \taxios.get('/visa/group/'+optional+'/service-docs')\n\t\t\t\t\t \t  .then(result => {\n\t\t\t\t            this.optionalDocs = result.data;\n\t\t\t\t        });\n\t\t        \t}\n\t\t\t\t} else {\n\t\t\t\t\tthis.requiredDocs = [];\n\t\t\t\t\tthis.optionalDocs = [];\n\t\t\t\t}\n\n\n\n\t        \tsetTimeout(() => {\n\t        \t\t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');\n\t        \t}, 2000);\n\t\t\t},\n\n\t\t\tgetDocs(servicesId) {\n\t\t\t\taxios.get('/visa/get-docs', {\n\t\t\t\t\t\tparams: {\n\t\t\t\t\t      \tservicesId: servicesId\n\t\t\t\t\t    }\n\t\t\t\t\t})\n\t\t\t\t\t.then(result => {\n\t\t\t\t\t\tthis.detailArray = result.data.detailArray;\n\t\t\t\t        this.requiredDocs = result.data.docsNeededArray;\n\t\t\t\t        this.optionalDocs = result.data.docsOptionalArray;\n\n\t\t\t\t        setTimeout(() => {\n\t\t\t\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen('destroy');\n\t\t\t\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({\n\t\t\t\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\t\t\t\tsearch_contains: true\n\t\t\t\t\t\t\t});\n\n\t\t\t        \t\t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');\n\t\t\t        \t}, 1000);\n\t\t\t\t\t});\n\t\t\t},\n\n\n\t\t\tinitChosenSelect() {\n\t\t\t\t$('.chosen-select-for-service').chosen({\n\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\tsearch_contains: true\n\t\t\t\t});\n\n\t\t\t\tlet vm = this;\n\n\t\t\t\t$('.chosen-select-for-service').on('change', function() {\n\t\t\t\t\tlet services = $(this).val();\n\n\t\t\t\t\tvm.showCheckboxAll = (services.length > 0) ? true : false;\n\t\t\t\t\t\n\t\t\t\t\tvm.getDocs(services);\n\n\t\t\t\t\tvm.addNewServiceForm.services = services;\n\n\t\t\t\t\t// Set cost input\n\t\t\t\t\tif(services.length == 1) {\n\t\t\t\t\t\tvm.servicesList.map(s => {\n\t\t\t\t\t\t\tif(s.id == services[0]) {\n\t\t\t\t\t\t\t\tvm.cost = s.cost;\n\t\t\t\t\t\t\t}\n\t\t\t\t\t\t});\n\t\t\t\t\t} else {\n\t\t\t\t\t\tvm.cost = '';\n\t\t\t\t\t}\n\t\t\t\t});\n\n\n\t\t\t},\n\n\t\t\tcallDataTable() {\n\t\t\t\t$('.add-new-service-datatable').DataTable().destroy();\n\n\t\t\t\tsetTimeout(function() {\n\t\t\t\t\t$('.add-new-service-datatable').DataTable({\n\t\t\t            pageLength: 10,\n\t\t\t            responsive: true,\n\t\t\t            dom: '<\"top\"lf>rt<\"bottom\"ip><\"clear\">'\n\t\t\t        });\n\t\t\t\t}, 1000);\n\t\t\t},\n\n\t\t\tcheckAll(e) {\n\t\t\t\tlet checkbox = e.target.className;\n\n\t\t\t\tif(checkbox == 'checkbox-all') {\n\t\t\t\t\tif($('.' + checkbox).is(':checked'))\n\t\t\t\t\t\t$('.chosen-select-for-required-docs option').prop('selected', true);\n\t\t\t\t\telse\n\t\t\t\t\t\t$('.chosen-select-for-required-docs option').prop('selected', false);\n\n\t\t\t\t\t$('.chosen-select-for-required-docs option').trigger('chosen:updated');\n\t\t\t\t} else {\n\t\t\t\t\tlet chosen = e.target.dataset.chosenClass;\n\n\t\t\t\t\tif($('.' + checkbox).is(':checked'))\n\t\t\t\t\t\t$('.' + chosen + ' option').prop('selected', true);\n\t\t\t\t\telse\n\t\t\t\t\t\t$('.' + chosen + ' option').prop('selected', false);\n\n\t\t\t\t\t$('.' + chosen + ' option').trigger('chosen:updated');\n\t\t\t\t}\n \t\t\t}\n        },\n\n        created() {\n        \tthis.fetchServiceList();\n        },\n\n        mounted() {\n        \tthis.initChosenSelect();\n        }\n    }\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n\t.checkbox-inline {\n\t\tmargin-top: -7px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 315:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-e4c5ce6a] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-e4c5ce6a] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["EditService.vue?2fc72771"],"names":[],"mappings":";AA2TA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"EditService.vue","sourcesContent":["<template>\n\t<form class=\"form-horizontal\" @submit.prevent=\"editClientService\">\n\n\t\t<div>\n\n\n\t\t    \t<div classs=\"col-md-12\">\n\t\t\t\t    <div class=\"form-group col-md-6\">\n\t\t\t\t    \t<label class=\"col-md-4 control-label \">\n\t\t\t\t            Tip:\n\t\t\t\t        </label>\n\t\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t        \t<input type=\"number\" min=\"0\" class=\"form-control\" name=\"tip\"  v-model=\"$parent.$parent.editServ.tip\" style=\"width : 175px;\">\n\t\t\t\t        </div>\n\t\t\t\t    </div>\n\n\t\t\t\t    <div class=\"form-group  col-md-6 m-l-2\">\n\t\t\t\t    \t<label class=\"col-md-4 control-label \">\n\t\t\t\t            Status:\n\t\t\t\t        </label>\n\t\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t        \t<select class=\"form-control\" name=\"status\" id=\"status\" style=\"width : 175px;\" v-model=\"$parent.$parent.editServ.status\">\n\t                            <option value=\"complete\">Complete</option>\n\t                            <option value=\"on process\">On Process</option>\n\t                            <option value=\"pending\">Pending</option>\n\t                        </select>\n\t\t\t\t        </div>\n\t\t\t\t    </div>\n\n\t\t\t\t</div>\n\n\t\t    \t<div classs=\"col-md-12\">\n\t\t\t\t    <div class=\"form-group col-md-6\">\n\t\t\t\t    \t<label class=\"col-md-4 control-label \">\n\t\t\t\t            Cost:\n\t\t\t\t        </label>\n\t\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t        \t<input type=\"number\" min=\"0\" class=\"form-control\" name=\"cost\" v-model=\"$parent.$parent.editServ.cost\" style=\"width : 175px;\">\n\t\t\t\t        </div>\n\t\t\t\t    </div>\n\n\t\t\t\t    <div class=\"form-group  col-md-6 m-l-2\">\n\t\t\t\t    \t<label class=\"col-md-4 control-label \">\n\t\t\t\t            Discount:\n\t\t\t\t        </label>\n\t\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t        \t<input type=\"number\" class=\"form-control\" name=\"discount\" v-model=\"$parent.$parent.editServ.discount\" min=\"0\" style=\"width : 175px;\">\n\t\t\t\t        </div>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\n\t\t\t    <div class=\"form-group\" v-if=\"$parent.$parent.editServ.discount > 0\">\n\t\t\t    \t<label class=\"col-md-2 control-label\" >\n\t\t\t            Reason:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-10\">\n\t\t\t        \t<input type=\"text\" class=\"form-control\" name=\"reason\" v-model=\"$parent.$parent.editServ.reason\">\n\t\t\t        </div>\n\t\t\t    </div>\n\t\t\t\n\t\t\t    <div class=\"form-group\">\n\t\t\t    \t<label class=\"col-md-2 control-label\">\n\t\t\t            Note:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-10\">\n\t\t\t        \t<input type=\"text\" class=\"form-control\" name=\"note\" v-model=\"$parent.$parent.editServ.note\" placeholder=\"for internal use only, only employees can see.\">\n\t\t\t        </div>\n\t\t\t    </div>\n\n\t\t\t<div classs=\"col-md-12\">\n\t\t\t\t<div class=\"form-group col-md-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-md-2 control-label\">\n\t\t\t\t\t        Status:\n\t\t\t\t\t    </label>\n\t\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t\t    \t<div style=\"height:5px;clear:both;\"></div>\n\t\t\t\t\t        <label class=\"\"> <input type=\"radio\" value=\"0\" name=\"active\" v-model=\"$parent.$parent.editServ.active\"> <i></i> Disabled </label>\n\n\t\t                    <label class=\"m-l-2\"> <input type=\"radio\" value=\"1\" name=\"active\" v-model=\"$parent.$parent.editServ.active\"> <i></i> Enabled </label>\n\t\t                </div>\n\t\t\t\t    </div>\n\t\t\t    </div>\n\t\t\t\t<div class=\"form-group col-md-6\">\n\t\t\t\t\t<label class=\"col-md-4 control-label \">\n\t\t\t\t        Extend To:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-6\">\n\t\t\t\t    \t<form id=\"daily-form\" class=\"form-inline\">\n\t                        <div class=\"form-group\">\n\t                            <div class=\"input-group date\">\n\t                                <span class=\"input-group-addon\">\n\t                                    <i class=\"fa fa-calendar\"></i>\n\t                                </span>\n\t                                <input type=\"text\" id=\"date\" v-model=\"$parent.$parent.editServ.extend\" class=\"form-control datepicker\" autocomplete=\"off\" name=\"date\">\n\t                            </div>\n\t                        </div>\n\t                    </form>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t       \tRequired Documents: <span class=\"text-danger\">*</span>\n\t\t\t\t    </label>\n\n\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t            <select data-placeholder=\"Select Docs\" class=\"chosen-select-for-required-docs\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t\t                <option v-for=\"doc in requiredDocs\" :value=\"doc.id\">\n\t\t\t                \t{{ (doc.title).trim() }}\n\t\t\t                </option>\n\t\t\t            </select>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t       \tOptional Documents:\n\t\t\t\t    </label>\n\n\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t            <select data-placeholder=\"Select Docs\" class=\"chosen-select-for-optional-docs\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t\t                <option v-for=\"doc in optionalDocs\" :value=\"doc.id\">\n\t\t\t                \t{{ (doc.title).trim() }}\n\t\t\t                </option>\n\t\t\t            </select>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\n\t\t\t</div>\n\t\t</div>\n\n <!--\t\t\t<div class=\"col-md-12\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t        Reporting:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t        <input type=\"text\" class=\"form-control\" name=\"reporting\" v-model=\"$parent.$parent.editServ.reporting\">\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t</div> -->\n    \t</div>\n    \t<div class=\"col-md-12\">\n\t\t\t<a class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</a>\n\t\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Save</button>\n\t    </div>\n    </form>\n\n</template>\n\n<script>\n    export default{\n    \tprops: ['tracking','client_id'],\n\n        data() {\n        \treturn {\n\n        \t\tservicesList: [],\n\n        \t\tcost: '',\n\n        \t\tclients: [],\n\n        \t\trequiredDocs: [],\n\n        \t\toptionalDocs: []\n        \t\t\t\t\n        \t}\n        },\n\t\tcomputed: {\n\n\t\t},\n        methods: {\n\n        \tresetEditServiceForm() {\n        \t\tthis.$parent.$parent.editServ.cost = 0;\n        \t\tthis.$parent.$parent.editServ.tip = 0;\n        \t\tthis.$parent.$parent.editServ.status = \"\";\n        \t\tthis.$parent.$parent.editServ.active = 0;\n        \t\tthis.$parent.$parent.editServ.discount = 0;\n        \t\tthis.$parent.$parent.editServ.reason = \"\";\n        \t\tthis.$parent.$parent.editServ.note = \"\";\n        \t\tthis.$parent.$parent.editServ.id = null;\n        \t\tthis.$parent.$parent.editServ.reporting = '';\n        \t\tthis.$parent.$parent.editServ.extend = \"\";\n        \t\tthis.$parent.$parent.editServ.rcv_docs = \"\";\n        \t},\n\n        \teditClientService() {\n\n        \t\tvar requiredDocs = '';\n        \t\t$('.chosen-select-for-required-docs option:selected').each( function(e) {\n\t\t\t        requiredDocs += $(this).val() + ',';\n\t\t\t    });\n\n        \t\tvar optionalDocs = '';\n        \t\t$('.chosen-select-for-optional-docs option:selected').each( function(e) {\n\t\t\t        optionalDocs += $(this).val() + ',';\n\t\t\t    });\n\n\t\t\t    var docs = requiredDocs + optionalDocs;\n\t\t\t\tthis.$parent.$parent.editServ.rcv_docs = docs.slice(0, -1);\n\n        \t\tif(this.$parent.$parent.editServ.discount > 0 && this.$parent.$parent.editServ.reason == null){\n\t\t\t        toastr.error('Please input reason.');\n        \t\t}\n        \t\telse if(requiredDocs == '' && this.requiredDocs.length!=0){\n        \t\t\ttoastr.error('Please select Required Documents.');\n        \t\t}\n      \t\t\telse{\n\t        \t\tthis.$parent.$parent.editServ.submit('post','/clientEditService')\n\t            \t.then(response => {\n\t\t                if(response.success) {\n\t\t\t     \t\t\tthis.resetEditServiceForm();\n\t\t\t     \t\t\tvar client_id = this.$parent.$parent.usr_id;\n\t\t\t     \t\t\tvar track = this.$parent.$parent.tracking_selected;\n\t\t\t\t\t\t\tthis.$parent.$parent.showServicesUnder(client_id,track);\n\t\t\t\t\t\t\tthis.$parent.$parent.updateTrackingList(client_id);\n\t\t\t\t\t\t\tthis.$parent.$parent.reloadComputation();\n\t\t\t\t\t\t\tvar owl = $(\"#owl-demo\");\n\t\t\t\t\t\t\towl.data('owlCarousel').destroy();\n\t\t\t\t\t\t\t$(\".chosen-select-for-required-docs, .chosen-select-for-optional-docs\").val('').trigger('chosen:updated');\n\t\t\t                $('#edit-service-modal').modal('hide');\n\t\t\t\t\t\t\ttoastr.success('Successfully Updated!');\n\t\t\t                }\n\t\t            })\n\t\t            .catch(error =>{\n\t\t                for(var key in error) {\n\t\t\t            \t\tif(error.hasOwnProperty(key)) toastr.error(error[key][0])\n\t\t\t            \t}\n\t\t            });\t\n      \t\t\t}\n\t\t\t},\n\n\t\t\tfetchDocs(id){\n\n\t\t\t\t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');\n\n\t\t\t\tif(this.$parent.$parent.editServ.rcv_docs == '')\n\t\t\t\t\tvar rcv = ( id[0].rcv!=null ? id[0].rcv.split(\",\") : '');\n\t\t\t\telse\n\t\t\t\t\tvar rcv = this.$parent.$parent.editServ.rcv_docs.split(\",\");\n\n\t\t\t \taxios.get('/visa/group/'+id[0].docs_needed+'/service-docs')\n\t\t\t \t  .then(result => {\n\t\t            this.requiredDocs = result.data;\n\t\t        });\n\t        \t\n\t\t\t \taxios.get('/visa/group/'+id[0].docs_optional+'/service-docs')\n\t\t\t \t  .then(result => {\n\t\t            this.optionalDocs = result.data;\n\t\t        });\n\n\t\t        setTimeout(() => {\n\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen('destroy');\n\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({\n\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\tsearch_contains: true\n\t\t\t\t});\n\n\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val(rcv).trigger('chosen:updated');\n        \t\t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');\n        \t}, 2000);\n\n\t\t\t},\n\n\n\t\t\tcallDataTable() {\n\t\t\t\t$('.add-new-service-datatable').DataTable().destroy();\n\n\t\t\t\tsetTimeout(function() {\n\t\t\t\t\t$('.add-new-service-datatable').DataTable({\n\t\t\t            pageLength: 10,\n\t\t\t            responsive: true,\n\t\t\t            dom: '<\"top\"lf>rt<\"bottom\"ip><\"clear\">'\n\t\t\t        });\n\t\t\t\t}, 1000);\n\t\t\t},\n        },\n\n        created() {\n\n        },\n\n        mounted() {\n        \t// DatePicker\n\t        \t$('.datepicker').datepicker({\n\t                todayBtn: \"linked\",\n\t                keyboardNavigation: false,\n\t                forceParse: false,\n\t                calendarWeeks: true,\n\t                autoclose: true,\n\t                format: \"yyyy-mm-dd\",\n\t            })\n\t            .on('changeDate', function(e) { // Bootstrap Datepicker onChange\n\t            \tlet id = e.target.id;\n\t\t\t        let date = e.target.value;\n\n\t\t\t        console.log('ID : ' + id);\n\t\t\t        console.log('DATE : ' + date);\n\t\t\t        this.$parent.$parent.editServ.extend = date;\n\t\t\t    }.bind(this))\n\t\t\t    .on('change', function(e) { // native onChange\n\t\t\t    \tlet id = e.target.id;\n\t\t\t    \tlet date = e.target.value;\n\n\t\t\t    \tconsole.log('ID : ' + id);\n\t\t\t        console.log('DATE : ' + date);\n\t\t\t        this.$parent.$parent.editServ.extend = date;\n\t\t\t    }.bind(this));\n        }\n    }\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 32:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.log.year != null) ? _c('div', {
    staticClass: "row timeline-movement timeline-movement-top no-dashed no-side-margin"
  }, [_c('div', {
    staticClass: "timeline-badge timeline-filter-movement"
  }, [_c('a', {
    attrs: {
      "href": "#"
    }
  }, [_c('span', [_vm._v(_vm._s(_vm.log.year))])])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "row timeline-movement  no-side-margin"
  }, [(_vm.log.month != null) ? _c('div', {
    staticClass: "timeline-badge"
  }, [_c('span', {
    staticClass: "timeline-balloon-date-day"
  }, [_vm._v(_vm._s(_vm.log.day))]), _vm._v(" "), _c('span', {
    staticClass: "timeline-balloon-date-month"
  }, [_vm._v(_vm._s(_vm.log.month))])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "col-sm-12  timeline-item"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-offset-1 col-sm-11"
  }, [_c('div', {
    staticClass: "timeline-panel debits"
  }, [_c('ul', {
    staticClass: "timeline-panel-ul"
  }, [_c('li', [_c('span', {
    staticClass: "importo",
    domProps: {
      "innerHTML": _vm._s(_vm.log.data.title)
    }
  })]), _vm._v(" "), _c('li', [_c('span', {
    staticClass: "causale"
  }, [_vm._v(_vm._s(_vm.log.data.processor))])]), _vm._v(" "), _c('li', [_c('p', [_c('small', {
    staticClass: "text-muted"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-time"
  }), _vm._v(_vm._s(_vm.log.data.date))])])])])])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-72b755b5", module.exports)
  }
}

/***/ }),

/***/ 332:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(239),
  /* template */
  __webpack_require__(421),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Modal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Modal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d013376e", Component.options)
  } else {
    hotAPI.reload("data-v-d013376e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 339:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(443)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(246),
  /* template */
  __webpack_require__(406),
  /* scopeId */
  "data-v-722ae35a",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/ClientDocuments.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ClientDocuments.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-722ae35a", Component.options)
  } else {
    hotAPI.reload("data-v-722ae35a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 361:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(448)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(268),
  /* template */
  __webpack_require__(414),
  /* scopeId */
  "data-v-b2c16ff8",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/clients/AddService.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AddService.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b2c16ff8", Component.options)
  } else {
    hotAPI.reload("data-v-b2c16ff8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 362:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(453)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(269),
  /* template */
  __webpack_require__(424),
  /* scopeId */
  "data-v-e4c5ce6a",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/clients/EditService.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] EditService.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e4c5ce6a", Component.options)
  } else {
    hotAPI.reload("data-v-e4c5ce6a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 363:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(270),
  /* template */
  __webpack_require__(411),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/clients/Package.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Package.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7ddada46", Component.options)
  } else {
    hotAPI.reload("data-v-7ddada46", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 364:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(271),
  /* template */
  __webpack_require__(396),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/clients/Service.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Service.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5686c596", Component.options)
  } else {
    hotAPI.reload("data-v-5686c596", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 396:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('tr', [_c('td', {
    staticClass: "text-center"
  }, [_c('span', {
    style: (_vm.isDisabled)
  }, [_vm._v(_vm._s(_vm._f("commonDate")(_vm.serv.service_date)))])]), _vm._v(" "), _c('td', {
    staticClass: "text-center"
  }, [(_vm.serv.remarks) ? _c('span', {
    style: (_vm.isDisabled)
  }, [_c('a', {
    attrs: {
      "href": "#",
      "data-toggle": "popover",
      "data-placement": "right",
      "data-container": "body",
      "data-content": _vm.serv.remarks,
      "data-trigger": "hover"
    }
  }, [_vm._v("\n          " + _vm._s(_vm.serv.detail) + "\n        ")])]) : _c('span', {
    style: (_vm.isDisabled)
  }, [_vm._v("\n          " + _vm._s(_vm.serv.detail) + "\n      ")])]), _vm._v(" "), _c('td', {
    staticClass: "text-center"
  }, [_c('span', {
    style: (_vm.isDisabled)
  }, [_vm._v(_vm._s(_vm._f("currency")(_vm.serv.cost)))])]), _vm._v(" "), _c('td', {
    staticClass: "text-center"
  }, [_c('span', {
    style: (_vm.isDisabled)
  }, [_vm._v(_vm._s(_vm._f("currency")(_vm.serv.charge)))])]), _vm._v(" "), _c('td', {
    staticClass: "text-center"
  }, [_c('span', {
    style: (_vm.isDisabled)
  }, [_vm._v(_vm._s(_vm._f("currency")(_vm.serv.tip)))])]), _vm._v(" "), _c('td', {
    staticClass: "text-center"
  }, [(_vm.serv.reason) ? _c('span', {
    style: (_vm.isDisabled)
  }, [_c('a', {
    attrs: {
      "href": "#",
      "data-toggle": "popover",
      "data-placement": "right",
      "data-container": "body",
      "data-content": _vm.serv.reason,
      "data-trigger": "hover"
    }
  }, [_vm._v("\n          " + _vm._s(_vm._f("currency")(_vm.serv.discount_amount)) + "\n        ")])]) : _c('span', {
    style: (_vm.isDisabled)
  }, [_vm._v("\n          " + _vm._s(_vm._f("currency")(_vm.serv.discount_amount)) + "\n      ")])]), _vm._v(" "), _c('td', {
    staticClass: "text-center ucfirst"
  }, [_c('span', {
    style: (_vm.isDisabled)
  }, [_vm._v(_vm._s(_vm.serv.status))])]), _vm._v(" "), _c('td', {
    staticClass: "text-center"
  }, [_c('span', {
    style: (_vm.isDisabled)
  }, [_vm._v(_vm._s(_vm.serv.tracking))])]), _vm._v(" "), _c('td', {
    staticClass: "text-center"
  }, [(_vm.serv.group_id > 0) ? _c('span', {
    style: (_vm.isDisabled)
  }, [_c('a', {
    attrs: {
      "href": "/visa/group/" + _vm.serv.group_id,
      "target": "_blank"
    }
  }, [_vm._v(_vm._s(_vm.serv.name))])]) : _vm._e()]), _vm._v(" "), _c('td', {
    staticClass: "text-center"
  }, [_c('span', [_c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.$parent.selpermissions.editService == 1 || _vm.$parent.setting == 1),
      expression: "$parent.selpermissions.editService==1 || $parent.setting == 1"
    }],
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "popover",
      "data-placement": "top",
      "data-container": "body",
      "data-content": "Edit Service",
      "data-trigger": "hover"
    },
    on: {
      "click": function($event) {
        _vm.editService(_vm.serv)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-pencil-square-o"
  })]), _vm._v("\n         \n        "), _c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.$parent.selpermissions.addQuickReport == 1 || _vm.$parent.setting == 1),
      expression: "$parent.selpermissions.addQuickReport==1 || $parent.setting == 1"
    }],
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "popover",
      "data-placement": "top",
      "data-container": "body",
      "data-content": "Add Quick Report",
      "data-trigger": "hover"
    },
    on: {
      "click": function($event) {
        _vm.writeReport(_vm.serv)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-list-alt"
  })]), _vm._v("\n         \n        "), _c('a', {
    staticClass: "hide",
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "popover",
      "data-placement": "top",
      "data-container": "body",
      "data-content": "Add Documents",
      "data-trigger": "hover"
    },
    on: {
      "click": function($event) {
        _vm.addDocs(_vm.serv)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-file"
  })])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5686c596", module.exports)
  }
}

/***/ }),

/***/ 406:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('form', {
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addDocuments($event)
      }
    }
  }, [_vm._l((_vm.images), function(image, index) {
    return _c('div', {
      staticClass: "col-md-4"
    }, [_c('div', {
      staticClass: "panel panel-default form-section"
    }, [_c('div', {
      staticClass: "panel-body"
    }, [_c('center', [_c('img', {
      staticClass: "img-responsive img-thumbnail document-image zoom",
      attrs: {
        "src": image,
        "data-magnify-src": image
      }
    })]), _vm._v(" "), _c('div', {
      staticStyle: {
        "height": "10px",
        "clear": "both"
      }
    }), _vm._v(" "), _c('p', [_vm._v("Document Type:")]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('select', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.documentTypes[index]),
        expression: "documentTypes[index]"
      }],
      staticClass: "form-control",
      on: {
        "change": function($event) {
          var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
            return o.selected
          }).map(function(o) {
            var val = "_value" in o ? o._value : o.value;
            return val
          });
          _vm.$set(_vm.documentTypes, index, $event.target.multiple ? $$selectedVal : $$selectedVal[0])
        }
      }
    }, _vm._l((_vm.defaultDocumentTypes), function(documentType) {
      return _c('option', {
        domProps: {
          "value": documentType.id
        }
      }, [_vm._v("\n\t                \t\t\t\t" + _vm._s(documentType.name) + "\n\t                \t\t\t")])
    }), 0)]), _vm._v(" "), _c('p', [_vm._v("Issued At:")]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('div', {
      staticClass: "input-group date"
    }, [_vm._m(0, true), _vm._v(" "), _c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.issuedAt[index]),
        expression: "issuedAt[index]"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "date"
      },
      domProps: {
        "value": (_vm.issuedAt[index])
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(_vm.issuedAt, index, $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('p', [_vm._v("Expired At:")]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('div', {
      staticClass: "input-group date"
    }, [_vm._m(1, true), _vm._v(" "), _c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.expiredAt[index]),
        expression: "expiredAt[index]"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "date"
      },
      domProps: {
        "value": (_vm.expiredAt[index])
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(_vm.expiredAt, index, $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('button', {
      staticClass: "btn btn-danger btn-xs pull-right",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.remove(index)
        }
      }
    }, [_vm._v("Remove")])])], 1)])])
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-4"
  }, [_c('div', {
    staticClass: "panel panel-default add-documents-section"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_vm._m(2), _vm._v(" "), _c('input', {
    ref: "files",
    attrs: {
      "type": "file",
      "name": "images",
      "id": "image-upload",
      "multiple": ""
    },
    on: {
      "change": _vm.handleFilesUpload
    }
  })])])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "1px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Add Document/s")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])], 2)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    attrs: {
      "for": "image-upload",
      "id": "image-label"
    }
  }, [_c('i', {
    staticClass: "fa fa-plus"
  }), _vm._v(" Choose File")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-722ae35a", module.exports)
  }
}

/***/ }),

/***/ 411:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "item item-fix-width"
  }, [_c('a', {
    staticClass: "item-clickable",
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.showServicesUnder()
      }
    }
  }, [_c('div', {
    class: 'info-box' + _vm.bg
  }, [_c('div', {
    staticClass: "arrow-package"
  }, [_vm._v(_vm._s(_vm.status))]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "info-box-content"
  }, [_c('span', {
    staticClass: "info-box-text"
  }, [_vm._v("Package")]), _vm._v(" "), _c('span', {
    staticClass: "info-box-number"
  }, [_vm._v(_vm._s(_vm.pack.tracking))]), _vm._v(" "), _c('span', {
    staticClass: "progress-description"
  }, [_vm._v("\n                    " + _vm._s(_vm._f("commonDate")(_vm.pack.log_date)) + "\n                ")])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "info-box-icon newbox"
  }, [_c('i', {
    staticClass: "fa fa-dropbox box-position"
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7ddada46", module.exports)
  }
}

/***/ }),

/***/ 414:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addNewService($event)
      }
    }
  }, [_c('div', {
    staticClass: "ios-scroll"
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addNewServiceForm.errors.has('services')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t            Service:\n\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-service",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "data-placeholder": "Select Service",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.servicesList), function(service) {
    return _c('option', {
      attrs: {
        "data-docs-needed": service.docs_needed,
        "data-docs-optional": service.docs_optional
      },
      domProps: {
        "value": service.id
      }
    }, [_vm._v("\n\t\t                \t" + _vm._s((service.detail).trim()) + "\n\t\t                ")])
  }), 0), _vm._v(" "), (_vm.addNewServiceForm.errors.has('services')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addNewServiceForm.errors.get('services'))
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "spacer-10"
  }), _vm._v(" "), _vm._m(0)])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t            Note:\n\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addNewServiceForm.note),
      expression: "addNewServiceForm.note"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "note",
      "placeholder": "for internal use only, only employees can see."
    },
    domProps: {
      "value": (_vm.addNewServiceForm.note)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addNewServiceForm, "note", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.addNewServiceForm.services.length <= 1),
      expression: "addNewServiceForm.services.length <= 1"
    }]
  }, [_c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t            Cost:\n\t\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('input', {
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "type": "text",
      "name": "cost",
      "readonly": ""
    },
    domProps: {
      "value": _vm.cost
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group  col-md-6 m-l-2",
    class: {
      'has-error': _vm.addNewServiceForm.errors.has('discount')
    }
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t            Discount:\n\t\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addNewServiceForm.discount),
      expression: "addNewServiceForm.discount"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "type": "number",
      "name": "discount",
      "min": "0"
    },
    domProps: {
      "value": (_vm.addNewServiceForm.discount)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addNewServiceForm, "discount", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addNewServiceForm.errors.has('discount')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addNewServiceForm.errors.get('discount'))
    }
  }) : _vm._e()])])]), _vm._v(" "), (_vm.addNewServiceForm.discount > 0) ? _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t            Reason:\n\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addNewServiceForm.reason),
      expression: "addNewServiceForm.reason"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "reason"
    },
    domProps: {
      "value": (_vm.addNewServiceForm.reason)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addNewServiceForm, "reason", $event.target.value)
      }
    }
  })])]) : _vm._e()]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showCheckboxAll),
      expression: "showCheckboxAll"
    }]
  }, [_c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('label', {
    staticClass: "checkbox-inline"
  }, [_c('input', {
    staticClass: "checkbox-all",
    attrs: {
      "type": "checkbox"
    },
    on: {
      "click": _vm.checkAll
    }
  }), _vm._v(" Check all required documents\n\t    \t\t")])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _vm._l((_vm.detailArray), function(detail, index) {
    return _c('div', {
      staticClass: "panel panel-default"
    }, [_c('div', {
      staticClass: "panel-heading"
    }, [_vm._v("\n\t                " + _vm._s(detail) + "\n\n\t                "), _c('label', {
      staticClass: "checkbox-inline pull-right"
    }, [_c('input', {
      class: 'checkbox-' + index,
      attrs: {
        "type": "checkbox",
        "data-chosen-class": 'chosen-select-for-required-docs-' + index
      },
      on: {
        "click": _vm.checkAll
      }
    }), _vm._v(" Check all required documents\n\t\t\t        ")])]), _vm._v(" "), _c('div', {
      staticClass: "panel-body"
    }, [_c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-md-2 control-label"
    }, [_vm._v("\n\t\t\t\t\t       \tRequired Documents:\n\t\t\t\t\t    ")]), _vm._v(" "), _c('div', {
      staticClass: "col-md-10"
    }, [_c('select', {
      staticClass: "chosen-select-for-required-docs",
      class: 'chosen-select-for-required-docs-' + index,
      staticStyle: {
        "width": "350px"
      },
      attrs: {
        "data-placeholder": "Select Docs",
        "multiple": "",
        "tabindex": "4"
      }
    }, _vm._l((_vm.requiredDocs[index]), function(doc) {
      return _c('option', {
        domProps: {
          "value": doc.id
        }
      }, [_vm._v("\n\t\t\t\t                \t" + _vm._s(doc.title) + "\n\t\t\t\t                ")])
    }), 0)])]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-md-2 control-label"
    }, [_vm._v("\n\t\t\t\t\t       \tOptional Documents:\n\t\t\t\t\t    ")]), _vm._v(" "), _c('div', {
      staticClass: "col-md-10"
    }, [_c('select', {
      staticClass: "chosen-select-for-optional-docs",
      class: 'chosen-select-for-optional-docs-' + index,
      staticStyle: {
        "width": "350px"
      },
      attrs: {
        "data-placeholder": "Select Docs",
        "multiple": "",
        "tabindex": "4"
      }
    }, _vm._l((_vm.optionalDocs[index]), function(doc) {
      return _c('option', {
        domProps: {
          "value": doc.id
        }
      }, [_vm._v("\n\t\t\t\t                \t" + _vm._s(doc.title) + "\n\t\t\t\t                ")])
    }), 0)])])])])
  })], 2), _vm._v(" "), (!_vm.loading) ? _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Save")]) : _vm._e(), _vm._v(" "), (_vm.loading) ? _c('button', {
    staticClass: "btn btn-default pull-right",
    attrs: {
      "type": "button"
    }
  }, [_vm._m(1)]) : _vm._e(), _vm._v(" "), _c('a', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("\n\t\t            \tIf you want to view all services at once, please \n\t\t            \t"), _c('a', {
    attrs: {
      "href": "/visa/service/view-all",
      "target": "_blank"
    }
  }, [_vm._v("click here")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "sk-spinner sk-spinner-wave",
    staticStyle: {
      "width": "40px",
      "height": "20px"
    }
  }, [_c('div', {
    staticClass: "sk-rect1"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect2"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect3"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect4"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect5"
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-b2c16ff8", module.exports)
  }
}

/***/ }),

/***/ 421:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal inmodal",
    attrs: {
      "id": _vm.id,
      "tabindex": "1",
      "role": "dialog",
      "aria-labelledby": "modal-label",
      "aria-hidden": "true"
    }
  }, [_c('div', {
    staticClass: "modal-dialog",
    class: _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_vm._m(0), _vm._v(" "), _c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-flat",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "data-dismiss": "modal",
      "aria-label": "Close"
    }
  }, [_c('span', {
    attrs: {
      "aria-hidden": "true"
    }
  }, [_vm._v("×")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-d013376e", module.exports)
  }
}

/***/ }),

/***/ 424:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.editClientService($event)
      }
    }
  }, [_c('div', [_c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t            Tip:\n\t\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.tip),
      expression: "$parent.$parent.editServ.tip"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "type": "number",
      "min": "0",
      "name": "tip"
    },
    domProps: {
      "value": (_vm.$parent.$parent.editServ.tip)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.$parent.$parent.editServ, "tip", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group  col-md-6 m-l-2"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t            Status:\n\t\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.status),
      expression: "$parent.$parent.editServ.status"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "name": "status",
      "id": "status"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.$parent.$parent.editServ, "status", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "complete"
    }
  }, [_vm._v("Complete")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "on process"
    }
  }, [_vm._v("On Process")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "pending"
    }
  }, [_vm._v("Pending")])])])])]), _vm._v(" "), _c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t            Cost:\n\t\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.cost),
      expression: "$parent.$parent.editServ.cost"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "type": "number",
      "min": "0",
      "name": "cost"
    },
    domProps: {
      "value": (_vm.$parent.$parent.editServ.cost)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.$parent.$parent.editServ, "cost", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group  col-md-6 m-l-2"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t            Discount:\n\t\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.discount),
      expression: "$parent.$parent.editServ.discount"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "type": "number",
      "name": "discount",
      "min": "0"
    },
    domProps: {
      "value": (_vm.$parent.$parent.editServ.discount)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.$parent.$parent.editServ, "discount", $event.target.value)
      }
    }
  })])])]), _vm._v(" "), (_vm.$parent.$parent.editServ.discount > 0) ? _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t            Reason:\n\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.reason),
      expression: "$parent.$parent.editServ.reason"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "reason"
    },
    domProps: {
      "value": (_vm.$parent.$parent.editServ.reason)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.$parent.$parent.editServ, "reason", $event.target.value)
      }
    }
  })])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t            Note:\n\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.note),
      expression: "$parent.$parent.editServ.note"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "note",
      "placeholder": "for internal use only, only employees can see."
    },
    domProps: {
      "value": (_vm.$parent.$parent.editServ.note)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.$parent.$parent.editServ, "note", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t\t\t        Status:\n\t\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticStyle: {
      "height": "5px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('label', {}, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.active),
      expression: "$parent.$parent.editServ.active"
    }],
    attrs: {
      "type": "radio",
      "value": "0",
      "name": "active"
    },
    domProps: {
      "checked": _vm._q(_vm.$parent.$parent.editServ.active, "0")
    },
    on: {
      "change": function($event) {
        _vm.$set(_vm.$parent.$parent.editServ, "active", "0")
      }
    }
  }), _vm._v(" "), _c('i'), _vm._v(" Disabled ")]), _vm._v(" "), _c('label', {
    staticClass: "m-l-2"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.active),
      expression: "$parent.$parent.editServ.active"
    }],
    attrs: {
      "type": "radio",
      "value": "1",
      "name": "active"
    },
    domProps: {
      "checked": _vm._q(_vm.$parent.$parent.editServ.active, "1")
    },
    on: {
      "change": function($event) {
        _vm.$set(_vm.$parent.$parent.editServ, "active", "1")
      }
    }
  }), _vm._v(" "), _c('i'), _vm._v(" Enabled ")])])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t        Extend To:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('form', {
    staticClass: "form-inline",
    attrs: {
      "id": "daily-form"
    }
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(0), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.$parent.$parent.editServ.extend),
      expression: "$parent.$parent.editServ.extend"
    }],
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date",
      "autocomplete": "off",
      "name": "date"
    },
    domProps: {
      "value": (_vm.$parent.$parent.editServ.extend)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.$parent.$parent.editServ, "extend", $event.target.value)
      }
    }
  })])])])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-required-docs",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "data-placeholder": "Select Docs",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.requiredDocs), function(doc) {
    return _c('option', {
      domProps: {
        "value": doc.id
      }
    }, [_vm._v("\n\t\t\t                \t" + _vm._s((doc.title).trim()) + "\n\t\t\t                ")])
  }), 0)])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t\t       \tOptional Documents:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-optional-docs",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "data-placeholder": "Select Docs",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.optionalDocs), function(doc) {
    return _c('option', {
      domProps: {
        "value": doc.id
      }
    }, [_vm._v("\n\t\t\t                \t" + _vm._s((doc.title).trim()) + "\n\t\t\t                ")])
  }), 0)])])])]), _vm._v(" "), _vm._m(2)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t\t       \tRequired Documents: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-12"
  }, [_c('a', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Save")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-e4c5ce6a", module.exports)
  }
}

/***/ }),

/***/ 443:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(305);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("5655feba", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-722ae35a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ClientDocuments.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-722ae35a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ClientDocuments.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 448:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(310);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("cc1d0756", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-b2c16ff8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddService.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-b2c16ff8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddService.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 453:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(315);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("32c13032", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-e4c5ce6a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EditService.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-e4c5ce6a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EditService.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 471:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(181);


/***/ }),

/***/ 7:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNDJjZTBjNmM2ZTJiM2UyMjc4MzU/YzkyZCIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMiLCJ3ZWJwYWNrOi8vL011bHRpcGxlUXVpY2tSZXBvcnQudnVlPzQ0ODMiLCJ3ZWJwYWNrOi8vL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZT80YzljIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZT80NzA0KiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZT8yYjNjIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZT85Yzc4KiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT82NGQ0Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWU/YzM3ZCoiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvcGFnZXMvdXNlcnByb2ZpbGUuanMiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbXVsdGlzZWxlY3QvZGlzdC92dWUtbXVsdGlzZWxlY3QubWluLmpzIiwid2VicGFjazovLy8uL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanM/ZGEwNCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWU/Yjg1MyoiLCJ3ZWJwYWNrOi8vL01vZGFsLnZ1ZSIsIndlYnBhY2s6Ly8vQWN0aW9uTG9ncy52dWU/YzE2OSIsIndlYnBhY2s6Ly8vQ2xpZW50RG9jdW1lbnRzLnZ1ZSIsIndlYnBhY2s6Ly8vVHJhbnNhY3Rpb25Mb2dzLnZ1ZT84ZTczIiwid2VicGFjazovLy9BZGRTZXJ2aWNlLnZ1ZSIsIndlYnBhY2s6Ly8vRWRpdFNlcnZpY2UudnVlPzFhYmEiLCJ3ZWJwYWNrOi8vL1BhY2thZ2UudnVlIiwid2VicGFjazovLy9TZXJ2aWNlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvQWN0aW9uTG9ncy52dWU/NzY4OCIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qcz82YjJiIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9UcmFuc2FjdGlvbkxvZ3MudnVlPzJmNTEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0NsaWVudERvY3VtZW50cy52dWU/NjY4NSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvVHJhbnNhY3Rpb25Mb2dzLnZ1ZT9hYTllKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvQWRkU2VydmljZS52dWU/ZWI1MCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvRWRpdFNlcnZpY2UudnVlP2RiZDIiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL0FjdGlvbkxvZ3MudnVlP2ZjNzcqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvTW9kYWwudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9DbGllbnREb2N1bWVudHMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9BZGRTZXJ2aWNlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvRWRpdFNlcnZpY2UudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9QYWNrYWdlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvU2VydmljZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL1NlcnZpY2UudnVlPzY3ZTciLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0NsaWVudERvY3VtZW50cy52dWU/NTk4ZiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvUGFja2FnZS52dWU/NmYyMSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvQWRkU2VydmljZS52dWU/ZDViZiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL01vZGFsLnZ1ZT9lNTllIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9FZGl0U2VydmljZS52dWU/OWJjZiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQ2xpZW50RG9jdW1lbnRzLnZ1ZT9kOGFkIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9BZGRTZXJ2aWNlLnZ1ZT82YzQyIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9FZGl0U2VydmljZS52dWU/MTE3ZSIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qcz9lNmFjIiwid2VicGFjazovLy9EaWFsb2dNb2RhbC52dWUiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImRhdGEiLCJ3aW5kb3ciLCJMYXJhdmVsIiwiYWRkIiwiYWRkcmVzcyIsImNpdHkiLCJwcm92aW5jZSIsIm51bWIiLCJjb250YWN0X251bWJlciIsImNsaWVudF9pZCIsImlkIiwidXNlciIsIkNsaWVudEFwcCIsImVsIiwiY2xpZW50cyIsInByb2Nlc3NvciIsImlzX2F1dGgiLCJhdXRob3JpemVycyIsInVzcl9pZCIsInJjdnMiLCJkb2N1bWVudF9yZWNlaXZlIiwiZnVsbG5hbWUiLCJmdWxsX25hbWUiLCJmaXJzdF9uYW1lIiwibGFzdF9uYW1lIiwiYXZhdGFyIiwiZXdhbGxldF9iYWxhbmNlIiwiZW1haWwiLCJiZGF5IiwiYmlydGhfZGF0ZSIsImdlbmRlciIsImNzdGF0dXMiLCJjaXZpbF9zdGF0dXMiLCJwYXNzcG9ydCIsImhlaWdodCIsIndlaWdodCIsInVucmVhZF9ub3RpZiIsImdyb3VwX2JpbmRlZCIsImJpbmRlZCIsImFkZHJlc3NlcyIsIm51bWJlciIsImlzX3ZlcmlmaWVkIiwiaXNfZG9jc192ZXJpZmllZCIsInNlcnZpY2VfY29zdCIsImRlcG9zaXQiLCJwYXltZW50IiwicmVmdW5kIiwiZGlzY291bnQiLCJiYWxhbmNlIiwidHJhbnNmZXIiLCJwYWNrYWdlcyIsInNlcnZpY2VzIiwicGFja2FnZV9kYXRlIiwicGFja2FnZV9jb3N0IiwicGFja2FnZV9kZXBvc2l0IiwicGFja2FnZV9wYXltZW50IiwicGFja2FnZV9yZWZ1bmQiLCJwYWNrYWdlX2Rpc2NvdW50IiwicGFja2FnZV9iYWxhbmNlIiwidHJhY2tpbmdfc2VsZWN0ZWQiLCJncm91cF9wYWNrYWdlIiwidHJhY2tpbmdfc3RhdHVzIiwiYWN0aW9ubG9ncyIsImN1cnJlbnRfeWVhciIsImN1cnJlbnRfbW9udGgiLCJjdXJyZW50X2RheSIsImNfeWVhciIsImNfbW9udGgiLCJjX2RheSIsInNfY291bnQiLCJsb2dfY291bnQiLCJ0cmFuc2FjdGlvbmxvZ3MiLCJjdXJyZW50X3llYXIyIiwiY3VycmVudF9tb250aDIiLCJjdXJyZW50X2RheTIiLCJjX3llYXIyIiwiY19tb250aDIiLCJjX2RheTIiLCJzX2NvdW50MiIsInByb2R1Y3RzIiwiYWN0aXZlcHJvZHMiLCJzdG9yZXMiLCJhY3RpdmVzdG9yZXMiLCJvcmRlcnMiLCJjb21wb3JkZXJzIiwib3JkZXJfcHJvZHMiLCJldHJhbnNhY3Rpb25zIiwiZG9jdW1lbnRzIiwiZG9jdW1lbnRfbG9ncyIsInNhbGVzIiwib3JfbnVtIiwicmN2cl9uYW1lIiwicmN2cl9jb250YWN0IiwicmN2cl9vcmRlckRhdGUiLCJ0b3RhbF9wcmljZSIsInRheCIsInNoaXBwaW5nX2ZlZSIsInN1YnRvdGFsIiwiY3VycmVudFNob3BwaW5nVXNlciIsImNvdW50U29sZCIsImRlbFBhY2siLCJGb3JtIiwiYmFzZVVSTCIsImZ1bmRQYWNrIiwiZWRpdFNlcnYiLCJjcGFuZWxfdXJsIiwiYmFzZV91cmwiLCJkYXRlX25vdyIsIkRhdGUiLCJkb2N0eXBlIiwidmFsaWRpdHkiLCJxdWlja1JlcG9ydENsaWVudHNJZCIsInF1aWNrUmVwb3J0Q2xpZW50U2VydmljZXNJZCIsInF1aWNrUmVwb3J0VHJhY2tpbmdzIiwiZHQiLCJhdXRob3JpemVyRnJtIiwic2V0dGluZyIsImFjY2Vzc19jb250cm9sIiwicGVybWlzc2lvbnMiLCJzZWxwZXJtaXNzaW9ucyIsImVkaXQiLCJhY3Rpb25Mb2dzIiwidHJhbnNhY3Rpb25Mb2dzIiwiYWNjZXNzQ29udHJvbCIsImFkZFNlcnZpY2UiLCJhZGRGdW5kcyIsImRlbGV0ZVRoaXNQYWNrYWdlIiwiZWRpdFNlcnZpY2UiLCJhZGRRdWlja1JlcG9ydCIsImZpbGVzIiwiY3JlYXRlZCIsInVzZXJfaWQiLCJnZXRTZXJ2aWNlQ29zdCIsImF4aW9zIiwiZ2V0IiwiZ2V0RGVwb3NpdCIsImdldFBheW1lbnQiLCJnZXRSZWZ1bmQiLCJnZXREaXNjb3VudCIsImdldEJhbGFuY2UiLCJnZXRUcmFuc2ZlciIsImdldFBhY2thZ2VzIiwiZ2V0U2VydmljZXMiLCJ0cmFja2luZyIsImdldEFjdGlvbkxvZ3MiLCJnZXRUcmFuc2FjdGlvbkxvZ3MiLCJnZXREb2N1bWVudHMiLCJheGlvc0FQSXYxIiwiZ2V0RG9jdW1lbnRMb2dzIiwiZ2V0RmlsZXMiLCJjbGllbnRJZCIsImFsbCIsInRoZW4iLCJzcHJlYWQiLCJkb2NzIiwiZG9jbG9ncyIsImNsaWVudGZpbGVzIiwiY2F0Y2giLCIkIiwibm9vcCIsIm1ldGhvZHMiLCJmZXRjaENsaWVudHMiLCJxIiwiYWRkQ2xhc3MiLCJ0b2FzdHIiLCJlcnJvciIsInBhcmFtcyIsInJlc3BvbnNlIiwibGVuZ3RoIiwicHVzaCIsIm5hbWUiLCJzZXRUaW1lb3V0IiwibXlfdmFsIiwidmFsIiwidHJpZ2dlciIsInNhdmVBdXRob3JpemVyIiwic3VibWl0IiwicmVzdWx0Iiwic3RhdHVzIiwiYXV0aG9yaXplciIsImF1dGhfbmFtZSIsInJlbG9hZENvbXB1dGF0aW9uIiwic2hvd1NlcnZpY2VzVW5kZXIiLCJzdWNjZXNzIiwibW9kYWwiLCJnZXREYXRlIiwiZXh0ZW5kIiwic2xpY2UiLCJwb3BPdmVyIiwic2hvd0FsbFNlcnZpY2VzIiwidXBkYXRlVHJhY2tpbmdMaXN0IiwiYWRkTmV3UGFja2FnZSIsInRyYWNrIiwib3dsIiwiZGVzdHJveSIsImRlbGV0ZVBhY2thZ2UiLCJyZWFzb24iLCJwb3BvdmVyIiwibG9nIiwibG9hZERlcG9zaXQiLCJsb2FkUmVmdW5kIiwibG9hZFBheW1lbnQiLCJsb2FkRGlzY291bnQiLCJsb2FkQmFsYW5jZSIsImxvYWRUcmFuc2ZlciIsInNlcnZpY2VfaWQiLCJjb3N0IiwidGlwIiwiZGVsZXRlZF9hdCIsImRpc2NvdW50X2Ftb3VudCIsImFjdGl2ZSIsInR5cGUiLCJhbW91bnQiLCJ0cmltIiwic2VsZWN0ZWRfY2xpZW50IiwiYXV0aG9yaXphdGlvbiIsInJvbGVzIiwibWFwIiwiZSIsImNvbnNvbGUiLCJodG1sIiwiY29udGFpbmVyIiwiY29udGVudCIsIiRmdW5kc1BvcE92ZXIiLCJvbiIsInRvb2x0aXAiLCJzZWxlY3RvciIsInR5cGVhaGVhZCIsInNvdXJjZSIsInF1ZXJ5IiwicHJvY2VzcyIsInBhcnNlSlNPTiIsIm1pbkxlbmd0aCIsImZpdFRvRWxlbWVudCIsIm1hdGNoZXIiLCJpdGVtIiwidXBkYXRlciIsImNob3NlbiIsImF0dHIiLCJzYXZlVmFsaWRpdHkiLCJkYXRlU2VsIiwiZXhwaXJlc19hdCIsIm1vbnRoIiwicGFyc2VJbnQiLCJzdWJzdHIiLCJkYXkiLCJuaW5lZ2ljYXJkX2V4cGlyeSIsImQiLCJuaW5lZ2ljYXJkX2NvbXBhcmUiLCJuaW5lZ29yZGVyX2V4cGlyeSIsIm5pbmVnb3JkZXJfY29tcGFyZSIsInBydmNhcmRfZXhwaXJ5IiwicHJ2Y2FyZF9jb21wYXJlIiwicHJ2b3JkZXJfZXhwaXJ5IiwicHJ2b3JkZXJfY29tcGFyZSIsInNycnZjYXJkX2V4cGlyeSIsInNycnZjYXJkX2NvbXBhcmUiLCJzcnJ2dmlzYV9leHBpcnkiLCJzcnJ2dmlzYV9jb21wYXJlIiwiYWVwX2V4cGlyeSIsImFlcF9jb21wYXJlIiwibmJpX2V4cGlyeSIsIm5iaV9jb21wYXJlIiwiZmV0Y2hEb2NzIiwiJHJlZnMiLCJtdWx0aXBsZXF1aWNrcmVwb3J0cmVmIiwiZmV0Y2hEb2NzMiIsImVkaXRzZXJ2aWNlcmVmIiwiZmV0Y2hEb2NzMyIsIm5lZWRlZCIsIm9wdGlvbmFsIiwiYWRkbmV3c2VydmljZXJlZiIsIm1vdW50ZWQiLCJ2bSIsImNsaWVudElkcyIsImNvbXB1dGVkIiwicGFja19kYXRlIiwiYyIsIm1vbWVudCIsIlN0cmluZyIsImZvcm1hdCIsIndhdGNoIiwibmV3VmFsIiwidXBTdG9yZSIsInVwZGF0ZWQiLCJvd2xDYXJvdXNlbCIsIml0ZW1zIiwibGF6eUxvYWQiLCJuYXZpZ2F0aW9uIiwicGFnaW5hdGlvbiIsInJld2luZE5hdiIsImNsaWNrIiwiZGlyZWN0aXZlcyIsImRhdGVwaWNrZXIiLCJiaW5kIiwiYmluZGluZyIsInZub2RlIiwiJHNldCIsImNvbXBvbmVudHMiLCJNdWx0aXNlbGVjdCIsImRvY3VtZW50IiwicmVhZHkiLCJ0aXRsZSIsImhpZGUiLCJib290c3RyYXBEdWFsTGlzdGJveCIsInNlbGVjdG9yTWluaW1hbEhlaWdodCIsImZhZGVPdXQiLCJzaG93IiwiTWF0aCIsImFicyIsImVhY2giLCJpcyIsInRhcmdldCIsImhhcyIsImluU3RhdGUiLCJzYXZlZFZhbHVlIiwidmFsdWUiLCJiYXNlU2Nyb2xsSGVpZ2h0Iiwic2Nyb2xsSGVpZ2h0IiwibWluUm93cyIsImdldEF0dHJpYnV0ZSIsInJvd3MiLCJjZWlsIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsImVra29MaWdodGJveCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3JCQTs7QUFFQSxtR0FGQTs7QUFJQSxLQUpBLGtCQUlBO0FBQ0E7O0FBRUEsV0FGQTtBQUdBLGtCQUhBOztBQUtBO0FBQ0EsOEJBREE7O0FBR0EscUJBSEE7QUFJQSxpQkFKQTs7QUFNQSxpQkFOQTtBQU9BLHdCQVBBO0FBUUEsaUJBUkE7O0FBVUEsZ0JBVkE7O0FBWUEsa0JBWkE7QUFhQSxvQkFiQTs7QUFlQSxhQWZBO0FBZ0JBLGFBaEJBO0FBaUJBLGFBakJBO0FBa0JBLHFCQWxCQTs7QUFvQkE7QUFwQkEsTUFxQkEsd0NBckJBOztBQUxBO0FBNkJBLEVBbENBOzs7QUFvQ0E7QUFFQSxXQUZBLHFCQUVBLEVBRkEsRUFFQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBLElBRkE7O0FBSUE7QUFDQTtBQUNBLElBRkEsRUFFQSxJQUZBLENBRUEsR0FGQTs7QUFJQSw0REFDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEE7O0FBS0E7QUFDQTtBQUNBLElBRkEsRUFFQSxJQUZBO0FBR0EsR0FwQkE7QUFzQkEsZ0JBdEJBLDRCQXNCQTtBQUFBOztBQUNBLCtDQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0EsSUFIQTtBQUlBLEdBM0JBO0FBNkJBLDhCQTdCQSwwQ0E2QkE7QUFBQTs7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxnQkFEQTs7QUFHQSxxQkFIQTtBQUlBLHVCQUpBOztBQU1BLGdCQU5BO0FBT0EsZ0JBUEE7QUFRQSxnQkFSQTtBQVNBLHNCQVRBOztBQVdBO0FBWEE7O0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxJQW5DQTs7QUFxQ0E7QUFDQTtBQUNBLDhCQURBOztBQUdBLGdCQUhBOztBQUtBLGtCQUxBO0FBTUEsb0JBTkE7O0FBUUEsYUFSQTtBQVNBLGFBVEE7QUFVQSxhQVZBO0FBV0EscUJBWEE7O0FBYUE7QUFiQSxNQWNBLHdDQWRBO0FBZUEsR0FwRkE7QUFzRkEsVUF0RkEsc0JBc0ZBO0FBQUE7O0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQU5BOztBQVFBO0FBQ0EsR0FsR0E7QUFvR0Esd0JBcEdBLG9DQW9HQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FwQkE7O0FBc0JBLHNFQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsUUFGQTtBQUdBLE9BSkEsTUFJQTtBQUNBO0FBQ0E7QUFDQSxRQUZBLEVBRUEsSUFGQTtBQUdBO0FBQ0E7QUFDQSxLQW5CQTtBQW9CQTtBQUNBO0FBeEpBLEVBcENBOztBQWdNQSxRQWhNQSxxQkFnTUE7QUFDQTtBQUNBLEVBbE1BOzs7QUFvTUE7QUFDQSxvQkFEQSxnQ0FDQTtBQUNBO0FBQ0EsR0FIQTtBQUtBLHdCQUxBLG9DQUtBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUpBOztBQU1BO0FBQ0E7QUFmQSxFQXBNQTs7QUFzTkE7QUFDQTtBQURBOztBQXROQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN1VkE7O0FBRUEsd0NBRkE7O0FBSUEsS0FKQSxrQkFJQTtBQUNBOztBQUVBLGNBRkE7QUFHQSxpQkFIQTs7QUFLQSxzQkFMQTs7QUFPQSx3QkFQQTs7QUFTQTtBQUNBLGVBREE7O0FBR0Esb0JBSEE7QUFJQSxzQkFKQTs7QUFNQSxlQU5BO0FBT0EsZUFQQTtBQVFBLGVBUkE7QUFTQSxxQkFUQTs7QUFXQTtBQVhBOztBQVRBO0FBd0JBLEVBN0JBOzs7QUErQkE7QUFFQSxZQUZBLHdCQUVBO0FBQUE7O0FBQ0EsOEJBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQSxJQUhBLEVBSUEsS0FKQSxDQUlBO0FBQUE7QUFBQSxJQUpBO0FBS0EsR0FSQTtBQVVBLG9CQVZBLGdDQVVBO0FBQUE7O0FBQ0E7O0FBRUEsNENBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQSxJQUhBLEVBSUEsS0FKQSxDQUlBO0FBQUE7QUFBQSxJQUpBO0FBS0EsR0FsQkE7QUFvQkEsZ0JBcEJBLDRCQW9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxHQW5DQTtBQXFDQSxxQkFyQ0EsK0JBcUNBLFFBckNBLEVBcUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FqREE7QUFtREEsZUFuREEsMkJBbURBO0FBQUE7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFEQSxNQUtBLElBTEEsQ0FLQTtBQUNBO0FBQ0EsSUFQQSxFQVFBLEtBUkEsQ0FRQTtBQUFBO0FBQUEsSUFSQTtBQVNBLEdBbkVBO0FBcUVBLHVCQXJFQSxtQ0FxRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQXpFQTtBQTJFQSx3QkEzRUEsb0NBMkVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHdCQURBO0FBRUEsdUNBRkE7QUFHQSxzQkFIQTtBQUlBLG1CQUpBO0FBS0E7QUFMQTtBQU9BLElBUkEsRUFRQSxHQVJBO0FBU0EsR0F2RkE7QUF5RkEsMkJBekZBLHFDQXlGQSxLQXpGQSxFQXlGQTtBQUNBO0FBQ0EsR0EzRkE7QUE2RkEsVUE3RkEsb0JBNkZBLHFCQTdGQSxFQTZGQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBLE1BRkE7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLElBdkJBLE1BdUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUxBLE1BS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUZBOztBQUlBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQWRBLE1BY0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsSUFUQSxNQVNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLElBVEEsTUFTQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQWhCQSxNQWdCQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsTUFIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUExQkEsTUEwQkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLElBWkEsTUFZQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsSUFaQSxNQVlBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLElBVEEsTUFTQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxJQVRBLE1BU0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsSUF2QkEsTUF1QkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLElBWkEsTUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQTNSQSxFQS9CQTs7QUE4VEEsUUE5VEEscUJBOFRBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxFQXBVQTtBQXNVQSxRQXRVQSxxQkFzVUE7O0FBRUE7QUFDQTtBQUNBLGdCQURBO0FBRUEsd0NBRkE7QUFHQTtBQUhBOztBQU1BO0FBQ0E7QUFDQSxxQkFEQTtBQUVBLDRCQUZBO0FBR0Esb0JBSEE7QUFJQSxzQkFKQTtBQUtBLGtCQUxBO0FBTUEsdUJBTkE7QUFPQTtBQVBBOztBQVVBO0FBQ0E7QUFDQSxzQkFEQTtBQUVBLHFDQUZBO0FBR0Esb0JBSEE7QUFJQSxpQkFKQTtBQUtBO0FBTEE7QUFRQTtBQW5XQSxHOzs7Ozs7O0FDcFhBLDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsbUZBQW1GLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVywwWUFBMFksaUNBQWlDLG1yQkFBbXJCLDJIQUEySCxnQkFBZ0IsdUdBQXVHLG1kQUFtZCxHQUFHLHVDQUF1QyxZQUFZLE9BQU8sbUJBQW1CLHlCQUF5Qix1QkFBdUIsd0NBQXdDLHlFQUF5RSxXQUFXLEVBQUUsd0VBQXdFLDRDQUE0QyxXQUFXLFlBQVksMkdBQTJHLDBDQUEwQyxlQUFlLEVBQUUsa0NBQWtDLHVFQUF1RSxpQkFBaUIsUUFBUSxTQUFTLDZCQUE2Qix1RkFBdUYsaURBQWlELGVBQWUsRUFBRSxhQUFhLDJDQUEyQywySEFBMkgsd0NBQXdDLDhEQUE4RCwrU0FBK1MsOERBQThELDREQUE0RCw0REFBNEQsNERBQTRELDREQUE0RCw0REFBNEQsNERBQTRELDREQUE0RCw2REFBNkQsNkRBQTZELDZEQUE2RCw2REFBNkQsNkRBQTZELDZEQUE2RCw2REFBNkQsbUVBQW1FLDhEQUE4RCxhQUFhLEVBQUUsbUdBQW1HLGtVQUFrVSxHQUFHLHVDQUF1QyxFQUFFLFNBQVMsdUJBQXVCLDZCQUE2QixtRkFBbUYsc0NBQXNDLDBFQUEwRSw4QkFBOEIsYUFBYSxXQUFXLEVBQUUsMkJBQTJCLFNBQVMscUNBQXFDLCtCQUErQix3RUFBd0UsZ0VBQWdFLG9FQUFvRSxnRkFBZ0Ysb0VBQW9FLDhGQUE4RixxRkFBcUYsd0NBQXdDLG9GQUFvRixzRkFBc0YsMEZBQTBGLDRFQUE0RSw0RUFBNEUsNEVBQTRFLDRGQUE0RiwwRkFBMEYsc0VBQXNFLHdFQUF3RSxnRkFBZ0Ysa0VBQWtFLGtFQUFrRSxrRUFBa0Usa0ZBQWtGLGdGQUFnRixhQUFhLEVBQUUsc0hBQXNILDRDQUE0Qyw0REFBNEQsMkZBQTJGLDJEQUEyRCxzREFBc0QsMkZBQTJGLDJFQUEyRSxxQkFBcUIsRUFBRSx5QkFBeUIsT0FBTyw0Q0FBNEMsOENBQThDLDJCQUEyQixRQUFRLHlCQUF5Qix1QkFBdUIscUJBQXFCLEVBQUUsV0FBVyxTQUFTLFNBQVMsb0JBQW9CLDhCQUE4QixPQUFPLG9CQUFvQiw4QkFBOEIscURBQXFELFdBQVcscUNBQXFDLDBDQUEwQywwREFBMEQsNEVBQTRFLGdFQUFnRSxhQUFhLFdBQVcsRUFBRSwwQ0FBMEMsU0FBUyxPQUFPLHNCQUFzQiwyRkFBMkYsT0FBTyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEbHhSLGdCQUFnQixtQkFBTyxDQUFDLENBQWtFO0FBQzFGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLENBQXNPO0FBQ2hQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEVBQStMO0FBQ3pNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7QUMxQkE7QUFDQSxtQkFBTyxDQUFDLEVBQXVSOztBQUUvUixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxFQUFpUDtBQUMzUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxFQUE2TTtBQUN2TjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMvQkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBcVA7QUFDL1A7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBaU47QUFDM047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDL0NBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pDQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLE9BQU87QUFDUDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLHdDQUF3QyxRQUFRO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7OztBQzMyQkE7QUFBQTtBQUFBO0FBQUE7O0FBRUFBLElBQUlDLFNBQUosQ0FBYyxRQUFkLEVBQXlCQyxtQkFBT0EsQ0FBQyxHQUFSLENBQXpCO0FBQ0FGLElBQUlDLFNBQUosQ0FBYyxjQUFkLEVBQStCQyxtQkFBT0EsQ0FBQyxFQUFSLENBQS9CO0FBQ0E7QUFDQUYsSUFBSUMsU0FBSixDQUFjLGNBQWQsRUFBK0JDLG1CQUFPQSxDQUFDLEdBQVIsQ0FBL0I7QUFDQUYsSUFBSUMsU0FBSixDQUFjLGNBQWQsRUFBK0JDLG1CQUFPQSxDQUFDLEdBQVIsQ0FBL0I7QUFDQUYsSUFBSUMsU0FBSixDQUFjLGFBQWQsRUFBOEJDLG1CQUFPQSxDQUFDLEVBQVIsQ0FBOUI7QUFDQUYsSUFBSUMsU0FBSixDQUFjLGtCQUFkLEVBQW1DQyxtQkFBT0EsQ0FBQyxFQUFSLENBQW5DO0FBQ0FGLElBQUlDLFNBQUosQ0FBYyxpQkFBZCxFQUFrQ0MsbUJBQU9BLENBQUMsR0FBUixDQUFsQztBQUNBRixJQUFJQyxTQUFKLENBQWMsY0FBZCxFQUErQkMsbUJBQU9BLENBQUMsR0FBUixDQUEvQjs7QUFFQSxJQUFJQyxPQUFPQyxPQUFPQyxPQUFQLENBQWVGLElBQTFCO0FBQ0EsSUFBSUcsTUFBTUgsS0FBS0ksT0FBTCxJQUFlLElBQWYsR0FBcUJKLEtBQUtJLE9BQUwsQ0FBYUEsT0FBbEMsR0FBNEMsSUFBdEQ7QUFDQSxJQUFJQyxPQUFPTCxLQUFLSSxPQUFMLElBQWUsSUFBZixHQUFxQkosS0FBS0ksT0FBTCxDQUFhQyxJQUFsQyxHQUF5QyxJQUFwRDtBQUNBLElBQUdBLFFBQU0sSUFBVCxFQUFjO0FBQ2JGLE9BQUlBLE1BQUksR0FBSixHQUFRRSxJQUFaO0FBQ0E7QUFDRCxJQUFJQyxXQUFXTixLQUFLSSxPQUFMLElBQWUsSUFBZixHQUFxQkosS0FBS0ksT0FBTCxDQUFhRSxRQUFsQyxHQUE2QyxJQUE1RDtBQUNBLElBQUdBLFlBQVUsSUFBYixFQUFrQjtBQUNqQkgsT0FBSUEsTUFBSSxHQUFKLEdBQVFHLFFBQVo7QUFDQTtBQUNELElBQUlDLE9BQU9QLEtBQUtJLE9BQUwsSUFBZSxJQUFmLEdBQXFCSixLQUFLSSxPQUFMLENBQWFJLGNBQWxDLEdBQW1ELElBQTlEO0FBQ0EsSUFBSUMsWUFBWVQsS0FBS1UsRUFBckI7QUFDQSxJQUFJQyxPQUFPVixPQUFPQyxPQUFQLENBQWVTLElBQTFCO0FBQ0EsSUFBSUMsWUFBWSxJQUFJZixHQUFKLENBQVE7QUFDdkJnQixLQUFHLGNBRG9CO0FBRXZCYixPQUFLO0FBQ0pjLFdBQVMsRUFETDtBQUVKQyxhQUFXSixJQUZQO0FBR0pLLFdBQVMsS0FITDtBQUlKQyxlQUFZLEVBSlI7QUFLSkMsVUFBUWxCLEtBQUtVLEVBTFQ7QUFNSlMsUUFBTW5CLEtBQUtvQixnQkFOUDtBQU9KQyxZQUFVckIsS0FBS3NCLFNBUFg7QUFRSkMsY0FBWXZCLEtBQUt1QixVQVJiO0FBU0pDLGFBQVd4QixLQUFLd0IsU0FUWjtBQVVKQyxVQUFRekIsS0FBS3lCLE1BVlQ7QUFXSkMsbUJBQWdCLENBWFo7QUFZSkMsU0FBTzNCLEtBQUsyQixLQVpSO0FBYUpDLFFBQU01QixLQUFLNkIsVUFiUDtBQWNKQyxVQUFROUIsS0FBSzhCLE1BZFQ7QUFlSkMsV0FBUy9CLEtBQUtnQyxZQWZWO0FBZ0JKQyxZQUFVakMsS0FBS2lDLFFBaEJYO0FBaUJKQyxVQUFRbEMsS0FBS2tDLE1BakJUO0FBa0JKQyxVQUFRbkMsS0FBS21DLE1BbEJUO0FBbUJKQyxnQkFBY3BDLEtBQUtvQyxZQW5CZjtBQW9CSkMsZ0JBQWNyQyxLQUFLcUMsWUFwQmY7QUFxQkpDLFVBQVF0QyxLQUFLc0MsTUFyQlQ7QUFzQkpsQyxXQUFTSixLQUFLdUMsU0F0QlY7QUF1QkpwQyxPQUFLQSxHQXZCRDtBQXdCSnFDLFVBQVFqQyxJQXhCSjtBQXlCSmtDLGVBQWF6QyxLQUFLeUMsV0F6QmQ7QUEwQkpDLG9CQUFrQjFDLEtBQUswQyxnQkExQm5CO0FBMkJKQyxnQkFBYyxJQTNCVjtBQTRCSkMsV0FBUyxJQTVCTDtBQTZCSkMsV0FBUyxJQTdCTDtBQThCSkMsVUFBUSxJQTlCSjtBQStCSkMsWUFBVSxJQS9CTjtBQWdDSkMsV0FBUyxJQWhDTDtBQWlDSkMsWUFBVSxJQWpDTjtBQWtDSkMsWUFBVSxFQWxDTjtBQW1DSkMsWUFBVSxFQW5DTjtBQW9DSkMsZ0JBQWMsSUFwQ1Y7QUFxQ0pDLGdCQUFjLElBckNWO0FBc0NKQyxtQkFBaUIsSUF0Q2I7QUF1Q0pDLG1CQUFpQixJQXZDYjtBQXdDSkMsa0JBQWdCLElBeENaO0FBeUNKQyxvQkFBa0IsSUF6Q2Q7QUEwQ0pDLG1CQUFpQixJQTFDYjtBQTJDSkMscUJBQWtCLElBM0NkO0FBNENKQyxpQkFBYyxLQTVDVjtBQTZDSkMsbUJBQWdCLElBN0NaOztBQStDSkMsY0FBWSxFQS9DUjtBQWdESkMsZ0JBQWMsSUFoRFY7QUFpREpDLGlCQUFlLElBakRYO0FBa0RKQyxlQUFhLElBbERUO0FBbURKQyxVQUFRLElBbkRKO0FBb0RKQyxXQUFTLElBcERMO0FBcURKQyxTQUFPLElBckRIO0FBc0RKQyxXQUFTLENBdERMO0FBdURKQyxhQUFXLENBdkRQOztBQXlESkMsbUJBQWlCLEVBekRiO0FBMERKQyxpQkFBZSxJQTFEWDtBQTJESkMsa0JBQWdCLElBM0RaO0FBNERKQyxnQkFBYyxJQTVEVjtBQTZESkMsV0FBUyxJQTdETDtBQThESkMsWUFBVSxJQTlETjtBQStESkMsVUFBUSxJQS9ESjtBQWdFSkMsWUFBVSxDQWhFTjs7QUFrRUpDLFlBQVUsRUFsRU47QUFtRUpDLGVBQWEsRUFuRVQ7QUFvRUpDLFVBQVEsRUFwRUo7QUFxRUpDLGdCQUFjLEVBckVWO0FBc0VKQyxVQUFRLEVBdEVKO0FBdUVKQyxjQUFXLEVBdkVQO0FBd0VKQyxlQUFhLEVBeEVUO0FBeUVKQyxpQkFBZSxFQXpFWDtBQTBFSkMsYUFBVyxFQTFFUDtBQTJFSkMsaUJBQWMsRUEzRVY7QUE0RUpDLFNBQU8sRUE1RUg7O0FBOEVKQyxVQUFRLEVBOUVKO0FBK0VKQyxhQUFXLEVBL0VQO0FBZ0ZKQyxnQkFBYyxFQWhGVjtBQWlGSkMsa0JBQWdCLEVBakZaO0FBa0ZKQyxlQUFjLEdBbEZWO0FBbUZKQyxPQUFNLEdBbkZGO0FBb0ZKQyxnQkFBYyxHQXBGVjtBQXFGSkMsWUFBVSxHQXJGTjs7QUF1RkpDLHVCQUFvQixFQXZGaEI7O0FBeUZKQyxhQUFVLENBekZOOztBQTJGSkMsV0FBUyxJQUFJQyxJQUFKLENBQVM7QUFDUixhQUFVLEVBREY7QUFFUixlQUFXO0FBRkgsR0FBVCxFQUdELEVBQUVDLFNBQVcsZUFBYixFQUhDLENBM0ZMOztBQWdHRUMsWUFBVSxJQUFJRixJQUFKLENBQVM7QUFDZixXQUFRLEVBRE87QUFFZixhQUFVLEVBRks7QUFHZixhQUFTLEVBSE07QUFJZixlQUFXLEVBSkk7QUFLZixnQkFBWSxFQUxHO0FBTWYsc0JBQWtCLEVBTkg7QUFPZixpQkFBYSxFQVBFO0FBUWYsZ0JBQVk7QUFSRyxHQUFULEVBU1IsRUFBRUMsU0FBVyxlQUFiLEVBVFEsQ0FoR1o7O0FBMkdKRSxZQUFVLElBQUlILElBQUosQ0FBUztBQUNULFdBQVEsQ0FEQztBQUVULFVBQU8sQ0FGRTtBQUdULGFBQVMsRUFIQTtBQUlULGFBQVMsQ0FKQTtBQUtULGVBQVcsQ0FMRjtBQU1ULGFBQVUsRUFORDtBQU9ULFdBQVEsRUFQQztBQVFULFNBQU8sSUFSRTtBQVNULGdCQUFjLEVBVEw7QUFVVCxhQUFTLEVBVkE7QUFXVCxlQUFXO0FBWEYsR0FBVCxFQVlGLEVBQUVDLFNBQVcsZUFBYixFQVpFLENBM0dOOztBQXlIREcsY0FBYXhHLE9BQU9DLE9BQVAsQ0FBZXdHLFFBekgzQjtBQTBIREMsWUFBVSxJQUFJQyxJQUFKLEVBMUhUO0FBMkhEQyxXQUFRLEVBM0hQOztBQTZIRUMsWUFBVSxJQUFJVCxJQUFKLENBQVM7QUFDZixpQkFBZSxFQURBO0FBRWYsYUFBUyxFQUZNO0FBR2QsU0FBSyxFQUhTO0FBSWQsWUFBUSxFQUpNO0FBS2YsVUFBTTtBQUxTLEdBQVQsRUFNUixFQUFFQyxTQUFXLGNBQWIsRUFOUSxDQTdIWjs7QUFxSUVTLHdCQUFzQixFQXJJeEI7QUFzSUpDLCtCQUE2QixFQXRJekI7QUF1SUpDLHdCQUFzQixFQXZJbEI7O0FBeUlKQyxNQUFHLEVBeklDOztBQTJJSkMsaUJBQWUsSUFBSWQsSUFBSixDQUFTO0FBQ2QsaUJBQWMsRUFEQTtBQUVkLGVBQVk7QUFGRSxHQUFULEVBR1AsRUFBRUMsU0FBVyxlQUFiLEVBSE8sQ0EzSVg7O0FBZ0pFYyxXQUFTekcsS0FBSzBHLGNBQUwsQ0FBb0IsQ0FBcEIsRUFBdUJELE9BaEpsQztBQWlKRUUsZUFBYTNHLEtBQUsyRyxXQWpKcEI7QUFrSkVDLGtCQUFnQixDQUFDO0FBQ2JDLFNBQUssQ0FEUTtBQUV0QkMsZUFBVyxDQUZXO0FBR3RCQyxvQkFBZ0IsQ0FITTtBQUl0Qm5DLGNBQVUsQ0FKWTtBQUt0Qm9DLGtCQUFjLENBTFE7QUFNdEJDLGVBQVcsQ0FOVztBQU90QkMsYUFBUyxDQVBhO0FBUXRCQyxzQkFBa0IsQ0FSSTtBQVN0QkMsZ0JBQVksQ0FUVTtBQVV0QkMsbUJBQWU7QUFWTyxHQUFELENBbEpsQjs7QUErSkVDLFNBQU87QUFDUDs7QUFoS0YsRUFGa0I7O0FBc0t2QkMsUUF0S3VCLHFCQXNLZDtBQUFBOztBQUNSOztBQUVBLE1BQUlDLFVBQVVuSSxLQUFLVSxFQUFuQixDQUhRLENBR2U7QUFDdkIsT0FBS3dGLG1CQUFMLEdBQTJCaUMsT0FBM0I7O0FBR0EsV0FBU0MsY0FBVCxHQUEwQjtBQUN6QixVQUFPQyxNQUFNQyxHQUFOLENBQVUsb0NBQWtDSCxPQUE1QyxDQUFQO0FBQ0E7O0FBRUQsV0FBU0ksVUFBVCxHQUFzQjtBQUNyQixVQUFPRixNQUFNQyxHQUFOLENBQVUsZ0NBQThCSCxPQUF4QyxDQUFQO0FBQ0E7O0FBRUQsV0FBU0ssVUFBVCxHQUFzQjtBQUNyQixVQUFPSCxNQUFNQyxHQUFOLENBQVUsZ0NBQThCSCxPQUF4QyxDQUFQO0FBQ0E7O0FBRUQsV0FBU00sU0FBVCxHQUFxQjtBQUNwQixVQUFPSixNQUFNQyxHQUFOLENBQVUsK0JBQTZCSCxPQUF2QyxDQUFQO0FBQ0E7O0FBRUQsV0FBU08sV0FBVCxHQUF1QjtBQUN0QixVQUFPTCxNQUFNQyxHQUFOLENBQVUsaUNBQStCSCxPQUF6QyxDQUFQO0FBQ0E7O0FBRUQsV0FBU1EsVUFBVCxHQUFzQjtBQUNyQixVQUFPTixNQUFNQyxHQUFOLENBQVUsZ0NBQThCSCxPQUF4QyxDQUFQO0FBQ0E7O0FBRUQsV0FBU1MsV0FBVCxHQUF1QjtBQUN0QixVQUFPUCxNQUFNQyxHQUFOLENBQVUsaUNBQStCSCxPQUF6QyxDQUFQO0FBQ0E7O0FBRUQsV0FBU1UsV0FBVCxHQUF1QjtBQUN0QixVQUFPUixNQUFNQyxHQUFOLENBQVUsaUNBQStCSCxPQUF6QyxDQUFQO0FBQ0E7O0FBRUQsV0FBU1csV0FBVCxDQUFxQkMsUUFBckIsRUFBK0I7QUFDOUIsVUFBT1YsTUFBTUMsR0FBTixDQUFVLGlDQUErQkgsT0FBL0IsR0FBdUMsR0FBdkMsR0FBMkNZLFFBQXJELENBQVA7QUFDQTs7QUFFRCxXQUFTQyxhQUFULEdBQXlCO0FBQ3hCLFVBQU9YLE1BQU1DLEdBQU4sQ0FBVSxtQ0FBaUNILE9BQTNDLENBQVA7QUFDQTs7QUFFRCxXQUFTYyxrQkFBVCxHQUE4QjtBQUM3QixVQUFPWixNQUFNQyxHQUFOLENBQVUsd0NBQXNDSCxPQUFoRCxDQUFQO0FBQ0E7O0FBRUQsV0FBU2UsWUFBVCxHQUF1QjtBQUN0QixVQUFPQyxXQUFXYixHQUFYLENBQWUsWUFBVUgsT0FBVixHQUFrQixZQUFqQyxDQUFQO0FBQ0E7O0FBRUQsV0FBU2lCLGVBQVQsR0FBMEI7QUFDekIsVUFBT0QsV0FBV2IsR0FBWCxDQUFlLGtCQUFnQkgsT0FBL0IsQ0FBUDtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQSxXQUFTa0IsUUFBVCxHQUFtQjtBQUNsQixPQUFJQyxXQUFXckosT0FBT0MsT0FBUCxDQUFlRixJQUFmLENBQW9CVSxFQUFuQztBQUNBLFVBQU8ySCxNQUFNQyxHQUFOLENBQVUsc0RBQXFEZ0IsUUFBL0QsQ0FBUDtBQUNBOztBQUVEO0FBQ0E7QUFDQTtBQUNBOztBQUVHakIsUUFBTWtCLEdBQU4sQ0FBVSxDQUNabkIsZ0JBRFksRUFFUkcsWUFGUSxFQUdSQyxZQUhRLEVBSVJDLFdBSlEsRUFLUkMsYUFMUSxFQU1SQyxZQU5RLEVBT1JDLGFBUFEsRUFRUkMsYUFSUSxFQVNSQyxZQUFZLENBQVosQ0FUUSxFQVVSRSxlQVZRLEVBV1JDLG9CQVhRLEVBWVJDLGNBWlEsRUFhUkU7QUFDRDtBQWRTLElBZVJDO0FBQ0Y7QUFoQlUsR0FBVixFQWlCQUcsSUFqQkEsQ0FpQktuQixNQUFNb0IsTUFBTixDQUNQLFVBQ0M5RyxZQURELEVBRUNDLE9BRkQsRUFHQ0MsT0FIRCxFQUlDQyxNQUpELEVBS0NDLFFBTEQsRUFNQ0MsT0FORCxFQU9DQyxRQVBELEVBUUNDLFFBUkQsRUFTQ0MsUUFURCxFQVVDVyxVQVZELEVBV0NTLGVBWEQsRUFZQ21GLElBWkQsRUFhQ0MsT0FiRDtBQWNDO0FBQ0FDLGFBZkQ7QUFnQkM7QUFDSTtBQUNMLFNBQUtqSCxZQUFMLEdBQXFCQSxhQUFhM0MsSUFBbEM7QUFDQSxTQUFLNEMsT0FBTCxHQUFpQkEsUUFBUTVDLElBQXpCO0FBQ0EsU0FBSzZDLE9BQUwsR0FBaUJBLFFBQVE3QyxJQUF6QjtBQUNBLFNBQUs4QyxNQUFMLEdBQWdCQSxPQUFPOUMsSUFBdkI7QUFDQSxTQUFLK0MsUUFBTCxHQUFrQkEsU0FBUy9DLElBQTNCO0FBQ0EsU0FBS2dELE9BQUwsR0FBaUJBLFFBQVFoRCxJQUF6QjtBQUNBLFNBQUtpRCxRQUFMLEdBQWtCQSxTQUFTakQsSUFBM0I7QUFDQSxTQUFLa0QsUUFBTCxHQUFrQkEsU0FBU2xELElBQTNCO0FBQ0EsU0FBS21ELFFBQUwsR0FBa0JBLFNBQVNuRCxJQUFULENBQWNtRCxRQUFoQztBQUNBLFNBQUtDLFlBQUwsR0FBc0JELFNBQVNuRCxJQUFULENBQWNvRCxZQUFwQztBQUNBLFNBQUtDLFlBQUwsR0FBc0JGLFNBQVNuRCxJQUFULENBQWNxRCxZQUFwQztBQUNBLFNBQUtDLGVBQUwsR0FBeUJILFNBQVNuRCxJQUFULENBQWNzRCxlQUF2QztBQUNBLFNBQUtDLGVBQUwsR0FBeUJKLFNBQVNuRCxJQUFULENBQWN1RCxlQUF2QztBQUNBLFNBQUtDLGNBQUwsR0FBd0JMLFNBQVNuRCxJQUFULENBQWN3RCxjQUF0QztBQUNBLFNBQUtDLGdCQUFMLEdBQTBCTixTQUFTbkQsSUFBVCxDQUFjeUQsZ0JBQXhDO0FBQ0EsU0FBS0MsZUFBTCxHQUF5QlAsU0FBU25ELElBQVQsQ0FBYzBELGVBQXZDO0FBQ0EsU0FBS0ksVUFBTCxHQUFtQkEsV0FBVzlELElBQTlCO0FBQ0EsU0FBS3VFLGVBQUwsR0FBdUJBLGdCQUFnQnZFLElBQXZDO0FBQ0EsU0FBS3VGLFNBQUwsR0FBbUJtRSxLQUFLMUosSUFBeEI7QUFDQSxTQUFLd0YsYUFBTCxHQUFzQm1FLFFBQVEzSixJQUE5QjtBQUNBO0FBQ0EsU0FBS2lJLEtBQUwsR0FBYzJCLFlBQVk1SixJQUExQjtBQUNBO0FBRUEsR0EzQ08sQ0FqQkwsRUE2REY2SixLQTdERSxDQTZESUMsRUFBRUMsSUE3RE47QUE4REc7QUFFTixFQS9Tc0I7O0FBZ1R2QkMsVUFBUTtBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQUMsY0FSTyx3QkFRTUMsQ0FSTixFQVFTO0FBQUE7O0FBQ2YsT0FBR0EsS0FBS2xLLEtBQUtVLEVBQWIsRUFBZ0I7QUFDZm9KLE1BQUUsbUJBQUYsRUFBdUJLLFFBQXZCLENBQWdDLFdBQWhDO0FBQ1NDLFdBQU9DLEtBQVAsQ0FBYSxvQ0FBYjtBQUNUO0FBQ0VsQixjQUFXYixHQUFYLENBQWUsK0JBQWYsRUFBZ0Q7QUFDakRnQyxZQUFRO0FBQ1BKLFFBQUdBO0FBREk7QUFEeUMsSUFBaEQsRUFLRlYsSUFMRSxDQUtHLG9CQUFZO0FBQ2pCLFFBQUdlLFNBQVN2SyxJQUFULENBQWN3SyxNQUFkLEdBQXVCLENBQTFCLEVBQTRCO0FBQzNCLFlBQUsxSixPQUFMLENBQWEySixJQUFiLENBQWtCO0FBQ04vSixVQUFHNkosU0FBU3ZLLElBQVQsQ0FBYyxDQUFkLEVBQWlCVSxFQURkO0FBRU5nSyxZQUFLSCxTQUFTdkssSUFBVCxDQUFjLENBQWQsRUFBaUIwSyxJQUZoQixFQUFsQjtBQUdBOztBQUVBQyxlQUFXLFlBQU07QUFDakIsU0FBSUMsU0FBU2QsRUFBRSw0QkFBRixFQUFnQ2UsR0FBaEMsRUFBYjtBQUNBZixPQUFFLDRCQUFGLEVBQWdDZSxHQUFoQyxDQUFvQ0QsTUFBcEMsRUFBNENFLE9BQTVDLENBQW9ELGdCQUFwRDtBQUNBLEtBSEEsRUFHRSxJQUhGO0FBSUQsSUFoQkU7QUFpQkgsR0E5Qk07QUFnQ1B6QixVQWhDTyxzQkFnQ0k7QUFBQTs7QUFDVixPQUFJQyxXQUFXckosT0FBT0MsT0FBUCxDQUFlRixJQUFmLENBQW9CVSxFQUFuQzs7QUFFQTJILFNBQU1DLEdBQU4sQ0FBVSxzREFBcURnQixRQUEvRCxFQUNFRSxJQURGLENBQ08sb0JBQVk7QUFDakIsV0FBS3ZCLEtBQUwsR0FBYXNDLFNBQVN2SyxJQUF0QjtBQUNBLElBSEY7QUFJQSxHQXZDTTtBQXlDUCtLLGdCQXpDTyw0QkF5Q1M7QUFBQTs7QUFDZixRQUFLNUQsYUFBTCxDQUFtQjZELE1BQW5CLENBQTBCLE1BQTFCLEVBQWlDLGlCQUFqQyxFQUNDeEIsSUFERCxDQUNNLGtCQUFVO0FBQ2YsUUFBR3lCLE9BQU9DLE1BQVAsSUFBaUIsUUFBcEIsRUFBNkI7QUFDNUJkLFlBQU9DLEtBQVAsQ0FBYSxnQkFBYjtBQUNBLEtBRkQsTUFFTSxJQUFHWSxPQUFPQyxNQUFQLElBQWlCLFNBQXBCLEVBQThCO0FBQ25DLFlBQUszRSxRQUFMLENBQWM0RSxVQUFkLEdBQTJCLE9BQUtoRSxhQUFMLENBQW1CZ0UsVUFBbkIsQ0FBOEJ6SyxFQUF6RDtBQUNBLFlBQUs2RixRQUFMLENBQWM2RSxTQUFkLEdBQTBCLE9BQUtqRSxhQUFMLENBQW1CZ0UsVUFBbkIsQ0FBOEI5SixRQUF4RDtBQUNBLFlBQUtrRixRQUFMLENBQWN5RSxNQUFkLENBQXFCLE1BQXJCLEVBQTRCLGlCQUE1QixFQUNJeEIsSUFESixDQUNTLGtCQUFVO0FBQ2YsYUFBSzZCLGlCQUFMO0FBQ0EsYUFBS0MsaUJBQUwsQ0FBdUJ0TCxLQUFLVSxFQUE1QixFQUFnQyxPQUFLaUQsaUJBQXJDO0FBQ1N5RyxhQUFPbUIsT0FBUCxDQUFlLHFCQUFmO0FBQ0gsTUFMVixFQU1VMUIsS0FOVixDQU1nQixpQkFBUTtBQUNkTyxhQUFPQyxLQUFQLENBQWEsUUFBYjtBQUNBLE1BUlY7QUFTR1AsT0FBRSxrQkFBRixFQUFzQjBCLEtBQXRCLENBQTRCLFFBQTVCO0FBQ0Y7QUFDTyxJQWxCVixFQW1CVTNCLEtBbkJWLENBbUJnQixpQkFBUTtBQUNkTyxXQUFPQyxLQUFQLENBQWEsUUFBYjtBQUNBLElBckJWO0FBc0JBLEdBaEVNO0FBaUVQb0IsU0FqRU8scUJBaUVFO0FBQ0wsUUFBS3ZFLEVBQUwsR0FBVTRDLEVBQUUsa0NBQUYsRUFBc0NlLEdBQXRDLEVBQVY7QUFDQSxRQUFLckUsUUFBTCxDQUFja0YsTUFBZCxHQUF1QixLQUFLeEUsRUFBNUI7QUFDSCxHQXBFTTtBQXFFUG9FLG1CQXJFTyw2QkFxRVc3SyxTQXJFWCxFQXFFcUJzSSxRQXJFckIsRUFxRThCO0FBQUE7O0FBQ3BDVixTQUFNQyxHQUFOLENBQVUsaUNBQStCN0gsU0FBL0IsR0FBeUMsR0FBekMsR0FBNkNzSSxRQUF2RCxFQUNFUyxJQURGLENBQ08sb0JBQVk7QUFDakIsV0FBS3JHLFFBQUwsR0FBZ0JvSCxTQUFTdkssSUFBVCxDQUFjbUQsUUFBOUI7QUFDQSxXQUFLQyxZQUFMLEdBQXNCbUgsU0FBU3ZLLElBQVQsQ0FBY29ELFlBQXBDO0FBQ0EsV0FBS0MsWUFBTCxHQUFzQmtILFNBQVN2SyxJQUFULENBQWNxRCxZQUFwQztBQUNBLFdBQUtDLGVBQUwsR0FBeUJpSCxTQUFTdkssSUFBVCxDQUFjc0QsZUFBdkM7QUFDQSxXQUFLQyxlQUFMLEdBQXlCZ0gsU0FBU3ZLLElBQVQsQ0FBY3VELGVBQXZDO0FBQ0EsV0FBS0MsY0FBTCxHQUF3QitHLFNBQVN2SyxJQUFULENBQWN3RCxjQUF0QztBQUNBLFdBQUtDLGdCQUFMLEdBQTBCOEcsU0FBU3ZLLElBQVQsQ0FBY3lELGdCQUF4QztBQUNBLFdBQUtDLGVBQUwsR0FBeUI2RyxTQUFTdkssSUFBVCxDQUFjMEQsZUFBdkM7QUFDQSxXQUFLQyxpQkFBTCxHQUF5Qm9GLFFBQXpCO0FBQ0EsV0FBS25GLGFBQUwsR0FBcUIsS0FBckI7QUFDQSxRQUFHbUYsU0FBUzRDLEtBQVQsQ0FBZSxDQUFmLEVBQWlCLENBQWpCLEtBQXVCLEdBQTFCLEVBQThCO0FBQzdCLFlBQUsvSCxhQUFMLEdBQXFCLElBQXJCO0FBQ0E7QUFDRCxXQUFLQyxlQUFMLEdBQXlCMEcsU0FBU3ZLLElBQVQsQ0FBY2tMLE1BQXZDO0FBQ0EsV0FBS1UsT0FBTDtBQUNBLElBakJGO0FBa0JNLEdBeEZBO0FBMEZGQyxpQkExRkUsNkJBMEZlO0FBQUE7O0FBQ2YsT0FBSXBMLFlBQVlULEtBQUtVLEVBQXJCO0FBQ04ySCxTQUFNQyxHQUFOLENBQVUsaUNBQStCN0gsU0FBL0IsR0FBeUMsR0FBekMsR0FBNkMsQ0FBdkQsRUFDRStJLElBREYsQ0FDTyxvQkFBWTtBQUNqQixXQUFLckcsUUFBTCxHQUFnQm9ILFNBQVN2SyxJQUFULENBQWNtRCxRQUE5QjtBQUNBLFdBQUtDLFlBQUwsR0FBNEJtSCxTQUFTdkssSUFBVCxDQUFjb0QsWUFBMUM7QUFDZSxXQUFLQyxZQUFMLEdBQTRCa0gsU0FBU3ZLLElBQVQsQ0FBY3FELFlBQTFDO0FBQ0EsV0FBS0MsZUFBTCxHQUErQmlILFNBQVN2SyxJQUFULENBQWNzRCxlQUE3QztBQUNmLFdBQUtDLGVBQUwsR0FBeUJnSCxTQUFTdkssSUFBVCxDQUFjdUQsZUFBdkM7QUFDQSxXQUFLQyxjQUFMLEdBQXdCK0csU0FBU3ZLLElBQVQsQ0FBY3dELGNBQXRDO0FBQ0EsV0FBS0MsZ0JBQUwsR0FBMEI4RyxTQUFTdkssSUFBVCxDQUFjeUQsZ0JBQXhDO0FBQ0EsV0FBS0MsZUFBTCxHQUF5QjZHLFNBQVN2SyxJQUFULENBQWMwRCxlQUF2QztBQUNBLFdBQUtDLGlCQUFMLEdBQXlCLElBQXpCO0FBQ0EsV0FBS0MsYUFBTCxHQUFxQixLQUFyQjtBQUNBLFdBQUtDLGVBQUwsR0FBdUIsSUFBdkI7QUFDQSxJQWJGO0FBY00sR0ExR0E7QUE0R0RpSSxvQkE1R0MsOEJBNEdrQnJMLFNBNUdsQixFQTRHNEI7QUFBQTs7QUFDbEM0SCxTQUFNQyxHQUFOLENBQVUsaUNBQStCN0gsU0FBekMsRUFDRStJLElBREYsQ0FDTyxvQkFBWTtBQUNqQixXQUFLdEcsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUtBLFFBQUwsR0FBZ0JxSCxTQUFTdkssSUFBekI7QUFDQSxJQUpGO0FBS00sR0FsSEE7QUFvSEQrTCxlQXBIQywyQkFvSGM7QUFBQTs7QUFDZCxPQUFJdEwsWUFBWVQsS0FBS1UsRUFBckI7QUFDTjJILFNBQU1DLEdBQU4sQ0FBVSxtQ0FBaUM3SCxTQUEzQyxFQUNFK0ksSUFERixDQUNPLG9CQUFZO0FBQ2pCLFFBQUl3QyxRQUFRekIsU0FBU3ZLLElBQVQsQ0FBYytJLFFBQTFCO0FBQ0EsV0FBS3VDLGlCQUFMLENBQXVCN0ssU0FBdkIsRUFBaUN1TCxLQUFqQztBQUNBLFdBQUtGLGtCQUFMLENBQXdCckwsU0FBeEI7QUFDQSxRQUFJd0wsTUFBTW5DLEVBQUUsV0FBRixDQUFWO0FBQ0FtQyxRQUFJak0sSUFBSixDQUFTLGFBQVQsRUFBd0JrTSxPQUF4QjtBQUNBLElBUEY7QUFRTSxHQTlIQTtBQWdJREMsZUFoSUMseUJBZ0lhQyxNQWhJYixFQWdJb0I7QUFBQTs7QUFDMUIsT0FBSTNMLFlBQVlULEtBQUtVLEVBQXJCO0FBQ1NvSixLQUFFLGlCQUFGLEVBQXFCdUMsT0FBckIsQ0FBNkIsTUFBN0I7QUFDQSxRQUFLakcsT0FBTCxDQUFhMkMsUUFBYixHQUF3QixLQUFLcEYsaUJBQTdCO0FBQ0EsT0FBR3lJLFVBQVUsRUFBYixFQUFnQjtBQUNmdEMsTUFBRSxvQkFBRixFQUF3QkssUUFBeEIsQ0FBaUMsV0FBakM7QUFDR0MsV0FBT0MsS0FBUCxDQUFhLG1DQUFiO0FBQ0gsSUFIRCxNQUlJO0FBQ0gsU0FBS2pFLE9BQUwsQ0FBYWdHLE1BQWIsR0FBc0JBLE1BQXRCO0FBQ0EsU0FBS2hHLE9BQUwsQ0FBYTRFLE1BQWIsQ0FBb0IsTUFBcEIsRUFBMkIsc0JBQTNCLEVBQ0N4QixJQURELENBQ00sb0JBQVk7QUFDakIsU0FBR2UsU0FBU1csTUFBVCxJQUFtQixTQUF0QixFQUFpQztBQUM3QmQsYUFBT21CLE9BQVAsQ0FBZSxxQkFBZjtBQUNBLGFBQUtNLGVBQUw7QUFDQSxhQUFLQyxrQkFBTCxDQUF3QnJMLFNBQXhCO0FBQ0EsVUFBSXdMLE1BQU1uQyxFQUFFLFdBQUYsQ0FBVjtBQUNabUMsVUFBSWpNLElBQUosQ0FBUyxhQUFULEVBQXdCa00sT0FBeEI7QUFDQSxNQU5RLE1BT0w7QUFDUzlCLGFBQU9DLEtBQVAsQ0FBYUUsU0FBUytCLEdBQXRCO0FBQ1o7QUFDUSxLQVpELEVBYUN6QyxLQWJELENBYU8saUJBQVE7QUFDWCxTQUFHUSxNQUFNK0IsTUFBVCxFQUFnQjtBQUNadEMsUUFBRSxvQkFBRixFQUF3QkssUUFBeEIsQ0FBaUMsV0FBakM7QUFDQUMsYUFBT0MsS0FBUCxDQUFhLG1DQUFiO0FBQ0gsTUFIRCxNQUdLO0FBQ0ZELGFBQU9DLEtBQVAsQ0FBYSxnQkFBYjtBQUNGO0FBQ0osS0FwQkQ7QUFxQkE7QUFDSixHQWhLQTtBQWtLRGtDLGFBbEtDLHlCQWtLYTtBQUFBOztBQUNuQixPQUFJOUwsWUFBWVQsS0FBS1UsRUFBckI7QUFDQTJILFNBQU1DLEdBQU4sQ0FBVSxnQ0FBOEI3SCxTQUF4QyxFQUNFK0ksSUFERixDQUNPLG9CQUFZO0FBQ2pCLFlBQUs1RyxPQUFMLEdBQWUySCxTQUFTdkssSUFBeEI7QUFDQSxJQUhGO0FBSUEsR0F4S007QUEwS1B3TSxZQTFLTyx3QkEwS007QUFBQTs7QUFDWixPQUFJL0wsWUFBWVQsS0FBS1UsRUFBckI7QUFDQTJILFNBQU1DLEdBQU4sQ0FBVSwrQkFBNkI3SCxTQUF2QyxFQUNFK0ksSUFERixDQUNPLG9CQUFZO0FBQ2pCLFlBQUsxRyxNQUFMLEdBQWN5SCxTQUFTdkssSUFBdkI7QUFDQSxJQUhGO0FBSUEsR0FoTE07QUFrTFB5TSxhQWxMTyx5QkFrTE87QUFBQTs7QUFDYixPQUFJaE0sWUFBWVQsS0FBS1UsRUFBckI7QUFDQTJILFNBQU1DLEdBQU4sQ0FBVSxnQ0FBOEI3SCxTQUF4QyxFQUNFK0ksSUFERixDQUNPLG9CQUFZO0FBQ2pCLFlBQUszRyxPQUFMLEdBQWUwSCxTQUFTdkssSUFBeEI7QUFDQSxJQUhGO0FBSUEsR0F4TE07QUEwTFAwTSxjQTFMTywwQkEwTFE7QUFBQTs7QUFDZCxPQUFJak0sWUFBWVQsS0FBS1UsRUFBckI7QUFDQTJILFNBQU1DLEdBQU4sQ0FBVSxpQ0FBK0I3SCxTQUF6QyxFQUNFK0ksSUFERixDQUNPLG9CQUFZO0FBQ2pCLFlBQUt6RyxRQUFMLEdBQWdCd0gsU0FBU3ZLLElBQXpCO0FBRUEsSUFKRjtBQUtBLEdBak1NO0FBbU1QMk0sYUFuTU8seUJBbU1PO0FBQUE7O0FBQ2IsT0FBSWxNLFlBQVlULEtBQUtVLEVBQXJCO0FBQ0EySCxTQUFNQyxHQUFOLENBQVUsZ0NBQThCN0gsU0FBeEMsRUFDRStJLElBREYsQ0FDTyxvQkFBWTtBQUNqQixZQUFLeEcsT0FBTCxHQUFldUgsU0FBU3ZLLElBQXhCO0FBQ0EsSUFIRjtBQUlBLEdBek1NO0FBMk1QNE0sY0EzTU8sMEJBMk1RO0FBQUE7O0FBQ2QsT0FBSW5NLFlBQVlULEtBQUtVLEVBQXJCO0FBQ0EySCxTQUFNQyxHQUFOLENBQVUsaUNBQStCN0gsU0FBekMsRUFDRStJLElBREYsQ0FDTyxvQkFBWTtBQUNqQixZQUFLdkcsUUFBTCxHQUFnQnNILFNBQVN2SyxJQUF6QjtBQUNBLElBSEY7QUFJQSxHQWpOTTtBQW1OUCtILGFBbk5PLHVCQW1OSzhFLFVBbk5MLEVBbU5pQjtBQUFBOztBQUN2QixRQUFLckcsUUFBTCxDQUFjOUYsRUFBZCxHQUFtQm1NLFVBQW5CO0FBQ0F4RSxTQUFNQyxHQUFOLENBQVUsbUNBQWlDdUUsVUFBM0MsRUFDRXJELElBREYsQ0FDTyxvQkFBWTtBQUNqQixZQUFLaEQsUUFBTCxDQUFjc0csSUFBZCxHQUFxQnZDLFNBQVN2SyxJQUFULENBQWM4TSxJQUFuQztBQUNBLFlBQUt0RyxRQUFMLENBQWN1RyxHQUFkLEdBQW9CeEMsU0FBU3ZLLElBQVQsQ0FBYytNLEdBQWxDO0FBQ0EsUUFBR3hDLFNBQVN2SyxJQUFULENBQWNnTixVQUFkLElBQTRCLElBQS9CLEVBQXFDO0FBQ3BDLGFBQUt4RyxRQUFMLENBQWN6RCxRQUFkLEdBQXlCLEVBQXpCO0FBQ0EsYUFBS3lELFFBQUwsQ0FBYzRGLE1BQWQsR0FBdUIsRUFBdkI7QUFDQSxLQUhELE1BR087QUFDTixhQUFLNUYsUUFBTCxDQUFjekQsUUFBZCxHQUF5QndILFNBQVN2SyxJQUFULENBQWNpTixlQUF2QztBQUNBLGFBQUt6RyxRQUFMLENBQWM0RixNQUFkLEdBQXVCN0IsU0FBU3ZLLElBQVQsQ0FBY29NLE1BQXJDO0FBQ0E7QUFDRCxZQUFLNUYsUUFBTCxDQUFjMEUsTUFBZCxHQUF1QlgsU0FBU3ZLLElBQVQsQ0FBY2tMLE1BQXJDO0FBQ0EsWUFBSzFFLFFBQUwsQ0FBYzBHLE1BQWQsR0FBdUIzQyxTQUFTdkssSUFBVCxDQUFja04sTUFBckM7QUFDQSxZQUFLMUcsUUFBTCxDQUFja0YsTUFBZCxHQUF1Qm5CLFNBQVN2SyxJQUFULENBQWMwTCxNQUFyQztBQUNBO0FBQ0EsSUFmRjtBQWdCRzVCLEtBQUUscUJBQUYsRUFBeUIwQixLQUF6QixDQUErQixNQUEvQjtBQUNILEdBdE9NO0FBd09EM0QsVUF4T0Msb0JBd09Rc0YsSUF4T1IsRUF3T2FDLE1BeE9iLEVBd09vQmhCLE1BeE9wQixFQXdPMkI7QUFBQTs7QUFDakM7QUFDRzs7QUFFTSxPQUFJLENBQUNlLFFBQVEsUUFBUixJQUFvQkEsUUFBUSxVQUE1QixJQUEwQ0EsUUFBUSxVQUFuRCxLQUFpRWYsT0FBT2lCLElBQVAsTUFBaUIsRUFBdEYsRUFBMEY7QUFDakd2RCxNQUFFLG1CQUFGLEVBQXVCSyxRQUF2QixDQUFnQyxXQUFoQztBQUNTQyxXQUFPQyxLQUFQLENBQWEsc0JBQWI7QUFDSixJQUhFLE1BS0UsSUFBRytDLFVBQVUsRUFBYixFQUFnQjtBQUNqQnRELE1BQUUsbUJBQUYsRUFBdUJLLFFBQXZCLENBQWdDLFdBQWhDO0FBQ0dDLFdBQU9DLEtBQVAsQ0FBYSw2QkFBYjtBQUNILElBSEMsTUFJRyxJQUFHOEMsUUFBUSxVQUFSLElBQXNCLEtBQUs1RyxRQUFMLENBQWMrRyxlQUFkLElBQWlDLEVBQTFELEVBQTZEO0FBQ2pFeEQsTUFBRSxtQkFBRixFQUF1QkssUUFBdkIsQ0FBZ0MsV0FBaEM7QUFDR0MsV0FBT0MsS0FBUCxDQUFhLDJDQUFiO0FBQ0gsSUFISSxNQUlEO0FBQ0gsU0FBSzlELFFBQUwsQ0FBYzRHLElBQWQsR0FBcUJBLElBQXJCO0FBQ04sU0FBSzVHLFFBQUwsQ0FBYzZHLE1BQWQsR0FBdUJBLE1BQXZCO0FBQ0EsU0FBSzdHLFFBQUwsQ0FBY3dDLFFBQWQsR0FBeUIsS0FBS3BGLGlCQUE5QjtBQUNBLFNBQUs0QyxRQUFMLENBQWM5RixTQUFkLEdBQTBCVCxLQUFLVSxFQUEvQjtBQUNBLFNBQUs2RixRQUFMLENBQWM2RixNQUFkLEdBQXVCQSxNQUF2QjtBQUNBOztBQUVBdEMsTUFBRSxZQUFGLEVBQWdCdUMsT0FBaEIsQ0FBd0IsTUFBeEI7QUFDTyxTQUFLOUYsUUFBTCxDQUFjeUUsTUFBZCxDQUFxQixNQUFyQixFQUE0QixpQkFBNUIsRUFDRXhCLElBREYsQ0FDTyxrQkFBVTtBQUNmLGFBQUs2QixpQkFBTDs7QUFFQSxhQUFLQyxpQkFBTCxDQUF1QnRMLEtBQUtVLEVBQTVCLEVBQWdDLFFBQUtpRCxpQkFBckM7O0FBRUd5RyxZQUFPbUIsT0FBUCxDQUFlLHFCQUFmO0FBQ0gsS0FQRixFQVFFMUIsS0FSRixDQVFRLGlCQUFRLENBQUUsQ0FSbEI7O0FBVVY7O0FBRUc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVNO0FBQ0osR0FqVEE7QUFtVEQwRCxlQW5UQywyQkFtVGM7QUFDcEIsT0FBSUMsUUFBUSxLQUFLek0sU0FBTCxDQUFleU0sS0FBM0I7QUFDQSxPQUFJeE0sVUFBVSxLQUFkO0FBQ0F3TSxTQUFNQyxHQUFOLENBQVUsVUFBU0MsQ0FBVCxFQUFZO0FBQ3JCQyxZQUFRckIsR0FBUixDQUFZLFFBQU1vQixFQUFFaE4sRUFBcEI7QUFDQWlOLFlBQVFyQixHQUFSLENBQVksVUFBUW9CLEVBQUVoRCxJQUF0QjtBQUNHLFFBQUdnRCxFQUFFaEQsSUFBRixJQUFVLFlBQWIsRUFBMEI7QUFDekIxSixlQUFVLElBQVY7QUFDQTtBQUNKLElBTkQ7QUFPQSxRQUFLQSxPQUFMLEdBQWVBLE9BQWY7QUFDQTJNLFdBQVFyQixHQUFSLENBQVksS0FBS3RMLE9BQWpCO0FBQ00sR0EvVEE7QUFpVURxSyxtQkFqVUMsK0JBaVVrQjtBQUNsQixRQUFLa0IsV0FBTDtBQUNHLFFBQUtHLFlBQUw7QUFDQSxRQUFLRixVQUFMO0FBQ0EsUUFBS0MsV0FBTDtBQUNBLFFBQUtFLFdBQUw7QUFDQSxRQUFLQyxZQUFMO0FBQ0gsR0F4VUE7QUEwVVBoQixTQTFVTyxxQkEwVUU7QUFDTDlCLEtBQUUsaUJBQUYsRUFDT3VDLE9BRFAsQ0FDZTtBQUNQdUIsVUFBSyxJQURFO0FBRVBDLGVBQVcsTUFGSjtBQUdQQyxhQUFTLG1CQUFZO0FBQ25CLFlBQU9oRSxFQUFFLG1CQUFGLEVBQXVCOEQsSUFBdkIsRUFBUDtBQUNEO0FBTE0sSUFEZjs7QUFTTSxPQUFJRyxnQkFBZ0JqRSxFQUFFLFlBQUYsRUFDekJ1QyxPQUR5QixDQUNqQjtBQUNQdUIsVUFBSyxJQURFO0FBRVBDLGVBQVcsTUFGSjtBQUdQQyxhQUFTLG1CQUFZO0FBQ25CLFlBQU9oRSxFQUFFLGNBQUYsRUFBa0I4RCxJQUFsQixFQUFQO0FBQ0Q7QUFMTSxJQURpQixDQUFwQjs7QUFTQTtBQUNSRyxpQkFBY0MsRUFBZCxDQUFpQixrQkFBakIsRUFBcUMsVUFBU04sQ0FBVCxFQUFZO0FBQ2hENUQsTUFBRSxNQUFGLEVBQVVtRSxPQUFWLENBQWtCLEVBQUVDLFVBQVUsdUJBQVosRUFBbEI7QUFDR3BFLE1BQUUsaUJBQUYsRUFBcUJxRSxTQUFyQixDQUErQjtBQUN2QkMsYUFBUyxnQkFBVUMsS0FBVixFQUFpQkMsT0FBakIsRUFBMEI7QUFDbEMsVUFBR0QsU0FBU3JPLEtBQUtVLEVBQWpCLEVBQW9CO0FBQzlCb0osU0FBRSxtQkFBRixFQUF1QkssUUFBdkIsQ0FBZ0MsV0FBaEM7QUFDU0MsY0FBT0MsS0FBUCxDQUFhLG9DQUFiO0FBQ1QsT0FIVSxNQUlQO0FBQ1MsY0FBT1AsRUFBRXhCLEdBQUYsQ0FBTSxzQ0FBTixFQUE4QyxFQUFFK0YsT0FBT0EsS0FBVCxFQUE5QyxFQUFnRSxVQUFVck8sSUFBVixFQUFnQjtBQUMvRUEsZUFBTzhKLEVBQUV5RSxTQUFGLENBQVl2TyxJQUFaLENBQVA7QUFDQSxlQUFPc08sUUFBUXRPLElBQVIsQ0FBUDtBQUNILFFBSEUsQ0FBUDtBQUlDO0FBQ0osTUFadUI7QUFhdkJ3TyxnQkFBVyxDQWJZO0FBY3ZCQyxtQkFBYyxJQWRTO0FBZXZCQyxjQUFTLGlCQUFTQyxJQUFULEVBQWU7QUFDbEIsYUFBTyxJQUFQO0FBQ0wsTUFqQnNCO0FBa0J2QkMsY0FBUSxpQkFBVUQsSUFBVixFQUFnQjtBQUNyQi9OLGdCQUFVMkYsUUFBVixDQUFtQitHLGVBQW5CLEdBQW9DcUIsS0FBS2pPLEVBQXpDO0FBQ0MsYUFBT2lPLElBQVA7QUFDSDtBQXJCc0IsS0FBL0I7QUF1Qkw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUF2Q0M7O0FBeUNGWixpQkFBY0MsRUFBZCxDQUFpQixtQkFBakIsRUFBc0MsWUFBVztBQUM3Q2xFLE1BQUUsNEJBQUYsRUFBZ0MrRSxNQUFoQyxDQUF1QyxTQUF2QztBQUNILElBRkQ7O0FBSUkvRSxLQUFFLG1CQUFGLEVBQ0N1QyxPQURELENBQ1M7QUFDUHVCLFVBQUssSUFERTtBQUVQQyxlQUFXLE1BRko7QUFHUEMsYUFBUyxtQkFBWTtBQUNwQmxOLGVBQVVrRyxRQUFWLENBQW1CcEcsRUFBbkIsR0FBd0JvSixFQUFFLElBQUYsRUFBUWdGLElBQVIsQ0FBYSxJQUFiLENBQXhCO0FBQ0FsTyxlQUFVaUcsT0FBVixHQUFvQmlELEVBQUUsSUFBRixFQUFRZ0YsSUFBUixDQUFhLFdBQWIsQ0FBcEI7QUFDQyxZQUFPaEYsRUFBRSxnQkFBRixFQUFvQjhELElBQXBCLEVBQVA7QUFDRDtBQVBNLElBRFQ7QUFVSCxHQXJaTTtBQXVaUG1CLGNBdlpPLHdCQXVaTUMsT0F2Wk4sRUF1WmM7QUFBQTs7QUFFWGxGLEtBQUUsU0FBRixFQUFhdUMsT0FBYixDQUFxQixNQUFyQjtBQUNBLFFBQUt2RixRQUFMLENBQWNvRSxNQUFkLEdBQXVCLFVBQXZCO0FBQ0EsUUFBS3BFLFFBQUwsQ0FBY21JLFVBQWQsR0FBMkJELE9BQTNCO0FBQ0EsT0FBSUUsUUFBUUMsU0FBU0gsUUFBUUksTUFBUixDQUFlLENBQWYsRUFBaUIsQ0FBakIsQ0FBVCxDQUFaO0FBQ0EsT0FBSUMsTUFBTUYsU0FBU0gsUUFBUUksTUFBUixDQUFlLENBQWYsRUFBaUIsQ0FBakIsQ0FBVCxDQUFWOztBQUVBLE9BQUlGLFNBQVMsQ0FBYixFQUFlO0FBQ1osUUFBR0csT0FBTyxDQUFWLEVBQVk7QUFDUCxVQUFLdkksUUFBTCxDQUFja0UsTUFBZCxDQUFxQixNQUFyQixFQUE2QixjQUE3QixFQUNDeEIsSUFERCxDQUNNLGtCQUFVOztBQUU5QixVQUFHLFFBQUszQyxPQUFMLElBQWMsU0FBakIsRUFBMkI7QUFDMUIsZUFBS3lJLGlCQUFMLEdBQXlCTixPQUF6QjtBQUNBLFdBQUlPLElBQUksSUFBSTNJLElBQUosQ0FBUyxRQUFLMEksaUJBQWQsQ0FBUjtBQUNBLGVBQUtFLGtCQUFMLEdBQTBCRCxJQUFJLFFBQUs1SSxRQUFuQztBQUNBOztBQUVELFVBQUcsUUFBS0UsT0FBTCxJQUFjLFNBQWpCLEVBQTJCO0FBQzFCLGVBQUs0SSxpQkFBTCxHQUF5QlQsT0FBekI7QUFDQSxXQUFJTyxJQUFJLElBQUkzSSxJQUFKLENBQVMsUUFBSzZJLGlCQUFkLENBQVI7QUFDQSxlQUFLQyxrQkFBTCxHQUEwQkgsSUFBSSxRQUFLNUksUUFBbkM7QUFDQTs7QUFFRCxVQUFHLFFBQUtFLE9BQUwsSUFBYyxTQUFqQixFQUEyQjtBQUMxQixlQUFLOEksY0FBTCxHQUFzQlgsT0FBdEI7QUFDQSxXQUFJTyxJQUFJLElBQUkzSSxJQUFKLENBQVMsUUFBSytJLGNBQWQsQ0FBUjtBQUNBLGVBQUtDLGVBQUwsR0FBdUJMLElBQUksUUFBSzVJLFFBQWhDO0FBQ0E7O0FBRUQsVUFBRyxRQUFLRSxPQUFMLElBQWMsVUFBakIsRUFBNEI7QUFDM0IsZUFBS2dKLGVBQUwsR0FBdUJiLE9BQXZCO0FBQ0EsV0FBSU8sSUFBSSxJQUFJM0ksSUFBSixDQUFTLFFBQUtpSixlQUFkLENBQVI7QUFDQSxlQUFLQyxnQkFBTCxHQUF3QlAsSUFBSSxRQUFLNUksUUFBakM7QUFDQTs7QUFFRCxVQUFHLFFBQUtFLE9BQUwsSUFBYyxVQUFqQixFQUE0QjtBQUMzQixlQUFLa0osZUFBTCxHQUF1QmYsT0FBdkI7QUFDQSxXQUFJTyxJQUFJLElBQUkzSSxJQUFKLENBQVMsUUFBS21KLGVBQWQsQ0FBUjtBQUNBLGVBQUtDLGdCQUFMLEdBQXdCVCxJQUFJLFFBQUs1SSxRQUFqQztBQUNBOztBQUVELFVBQUcsUUFBS0UsT0FBTCxJQUFjLFVBQWpCLEVBQTRCO0FBQzNCLGVBQUtvSixlQUFMLEdBQXVCakIsT0FBdkI7QUFDQSxXQUFJTyxJQUFJLElBQUkzSSxJQUFKLENBQVMsUUFBS3FKLGVBQWQsQ0FBUjtBQUNBLGVBQUtDLGdCQUFMLEdBQXdCWCxJQUFJLFFBQUs1SSxRQUFqQztBQUNBOztBQUVELFVBQUcsUUFBS0UsT0FBTCxJQUFjLEtBQWpCLEVBQXVCO0FBQ3RCLGVBQUtzSixVQUFMLEdBQWtCbkIsT0FBbEI7QUFDQSxXQUFJTyxJQUFJLElBQUkzSSxJQUFKLENBQVMsUUFBS3VKLFVBQWQsQ0FBUjtBQUNBLGVBQUtDLFdBQUwsR0FBbUJiLElBQUksUUFBSzVJLFFBQTVCO0FBQ0E7O0FBRUQsVUFBRyxRQUFLRSxPQUFMLElBQWMsS0FBakIsRUFBdUI7QUFDdEIsZUFBS3dKLFVBQUwsR0FBa0JyQixPQUFsQjtBQUNBLFdBQUlPLElBQUksSUFBSTNJLElBQUosQ0FBUyxRQUFLeUosVUFBZCxDQUFSO0FBQ0EsZUFBS0MsV0FBTCxHQUFtQmYsSUFBSSxRQUFLNUksUUFBNUI7QUFDQTs7QUFHY21ELFFBQUUsTUFBSSxRQUFLaEQsUUFBTCxDQUFjcEcsRUFBcEIsRUFBd0IyTCxPQUF4QixDQUFnQyxNQUFoQztBQUNHakMsYUFBT21CLE9BQVAsQ0FBZSxxQkFBZjtBQUNILE1BdERELEVBdURDMUIsS0F2REQsQ0F1RE8saUJBQVM7QUFDWCxVQUFHUSxNQUFNNEUsVUFBVCxFQUFvQjtBQUNqQm5GLFNBQUUsb0JBQUYsRUFBd0JLLFFBQXhCLENBQWlDLFdBQWpDO0FBQ0FDLGNBQU9DLEtBQVAsQ0FBYSxrQ0FBYjtBQUNILE9BSEEsTUFHSTtBQUNGRCxjQUFPQyxLQUFQLENBQWEsZ0JBQWI7QUFDRjtBQUNEUCxRQUFFLE1BQUksUUFBS2hELFFBQUwsQ0FBY3BHLEVBQXBCLEVBQXdCMkwsT0FBeEIsQ0FBZ0MsTUFBaEM7QUFDSCxNQS9ERDtBQWdFSixLQWpFRCxNQWlFSztBQUNBakMsWUFBT0MsS0FBUCxDQUFhLGtDQUFiO0FBQ0o7QUFDSCxJQXJFRCxNQXFFSztBQUNERCxXQUFPQyxLQUFQLENBQWEsa0NBQWI7QUFDSDtBQUNWLEdBdmVNO0FBeWVQa0csV0F6ZU8scUJBeWVHN1AsRUF6ZUgsRUF5ZU87QUFDYixRQUFLOFAsS0FBTCxDQUFXQyxzQkFBWCxDQUFrQ0YsU0FBbEMsQ0FBNEM3UCxFQUE1QztBQUNBLEdBM2VNO0FBNmVQZ1EsWUE3ZU8sc0JBNmVJaFEsRUE3ZUosRUE2ZVE7QUFDZCxRQUFLOFAsS0FBTCxDQUFXRyxjQUFYLENBQTBCSixTQUExQixDQUFvQzdQLEVBQXBDO0FBQ0EsR0EvZU07QUFpZlBrUSxZQWpmTyxzQkFpZkkvRCxVQWpmSixFQWlmZWdFLE1BamZmLEVBaWZzQkMsUUFqZnRCLEVBaWZnQztBQUN0QyxRQUFLTixLQUFMLENBQVdPLGdCQUFYLENBQTRCUixTQUE1QixDQUFzQzFELFVBQXRDLEVBQWlEZ0UsTUFBakQsRUFBd0RDLFFBQXhEO0FBQ0E7QUFuZk0sRUFoVGU7QUFxeUJ0QkUsUUFyeUJzQixxQkFxeUJYO0FBQ1IsTUFBSUMsS0FBSyxJQUFUOztBQUVBbkgsSUFBRSxNQUFGLEVBQVV1QyxPQUFWLENBQWtCLEVBQUU7QUFDbkJ1QixTQUFLLElBRFk7QUFFcEI5QyxZQUFTLE9BRlc7QUFHakJvRCxhQUFVO0FBSE8sR0FBbEI7O0FBTUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFS3BFLElBQUUsNEJBQUYsRUFBZ0NrRSxFQUFoQyxDQUFtQyxRQUFuQyxFQUE2QyxZQUFNO0FBQ3ZELE9BQUlrRCxZQUFZcEgsRUFBRSw0QkFBRixFQUFnQ2UsR0FBaEMsRUFBaEI7O0FBRUE7QUFDQSxHQUpJO0FBS0wsRUF6ekJzQjs7QUEwekJ2QnNHLFdBQVM7QUFDTEMsV0FESyx1QkFDTTtBQUNWLE9BQUlDLElBQUlDLE9BQU9DLE9BQU8sWUFBUCxDQUFQLEVBQTZCQyxNQUE3QixDQUFvQyxPQUFwQyxDQUFSO0FBQ0EsT0FBSWpDLElBQUkrQixPQUFPQyxPQUFPLEtBQUtuTyxZQUFaLENBQVAsRUFBa0NvTyxNQUFsQyxDQUF5QyxPQUF6QyxDQUFSO0FBQ0EsVUFBUUgsSUFBSTlCLENBQUosR0FBUSxJQUFSLEdBQWUsS0FBdkI7QUFDQTtBQUxJLEVBMXpCYztBQWkwQnZCa0MsUUFBTztBQUNIO0FBQ0E3UCxRQUFNLGNBQVU4UCxNQUFWLEVBQWtCO0FBQ3RCLFFBQUtDLE9BQUwsQ0FBYUQsTUFBYixFQUFvQixNQUFwQjtBQUNEO0FBSkUsRUFqMEJnQjtBQXUwQnZCRSxRQXYwQnVCLHFCQXUwQmI7QUFDTixPQUFLaEcsT0FBTDs7QUFFQTtBQUNBLE1BQUlLLE1BQU1uQyxFQUFFLFlBQUYsQ0FBVjtBQUNHbUMsTUFBSTRGLFdBQUosQ0FBZ0I7QUFDWkMsVUFBUSxDQURJO0FBRVpDLGFBQVcsSUFGQztBQUdaQyxlQUFhLEtBSEQ7QUFJWkMsZUFBWSxLQUpBO0FBS1pDLGNBQVU7QUFMRSxHQUFoQjtBQU9BcEksSUFBRSxPQUFGLEVBQVdxSSxLQUFYLENBQWlCLFlBQVU7QUFDdkJsRyxPQUFJbkIsT0FBSixDQUFZLFVBQVo7QUFDSCxHQUZEO0FBR0FoQixJQUFFLE9BQUYsRUFBV3FJLEtBQVgsQ0FBaUIsWUFBVTtBQUN2QmxHLE9BQUluQixPQUFKLENBQVksVUFBWjtBQUNILEdBRkQ7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0gsRUE5MUJzQjs7QUErMUJ2QnNILGFBQVk7QUFDUkMsY0FBWTtBQUNSQyxPQURRLGdCQUNIelIsRUFERyxFQUNDMFIsT0FERCxFQUNVQyxLQURWLEVBQ2lCO0FBQ3JCMUksTUFBRWpKLEVBQUYsRUFBTXdSLFVBQU4sQ0FBaUI7QUFDYmIsYUFBUTtBQURLLEtBQWpCLEVBRUd4RCxFQUZILENBRU0sWUFGTixFQUVvQixVQUFDTixDQUFELEVBQU87QUFDdkI5TSxlQUFVNlIsSUFBVixDQUFlN1IsVUFBVWtHLFFBQXpCLEVBQW1DLFlBQW5DLEVBQWlENEcsRUFBRThELE1BQUYsQ0FBUyxZQUFULENBQWpEO0FBQ0gsS0FKRDtBQUtIO0FBUE87QUFESixFQS8xQlc7QUEwMkJ2QmtCLGFBQVk7QUFDUkMsc0VBRFE7QUFFUCwyQkFBeUI1UyxtQkFBT0EsQ0FBQyxFQUFSLENBRmxCO0FBR1Asc0JBQW9CQSxtQkFBT0EsQ0FBQyxHQUFSO0FBSGI7O0FBMTJCVyxDQUFSLENBQWhCOztBQWszQkErSixFQUFFOEksUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVk7QUFDN0JELFVBQVNFLEtBQVQsR0FBaUJsUyxVQUFVUyxRQUEzQjs7QUFFR3lJLEdBQUUsYUFBRixFQUFpQnVJLFVBQWpCO0FBQ0h2SSxHQUFFLGFBQUYsRUFBaUJpSixJQUFqQjtBQUNBakosR0FBRSxVQUFGLEVBQWNpSixJQUFkO0FBQ0FqSixHQUFFLGFBQUYsRUFBaUJpSixJQUFqQjtBQUNHakosR0FBRSxjQUFGLEVBQWtCa0osb0JBQWxCLENBQXVDO0FBQ25DQyx5QkFBdUI7QUFEWSxFQUF2Qzs7QUFJQW5KLEdBQUUsY0FBRixFQUFrQm9KLE9BQWxCLENBQTBCLE1BQTFCOztBQUVBcEosR0FBRSxpQkFBRixFQUNDdUMsT0FERCxDQUNTO0FBQ1B1QixRQUFLLElBREU7QUFFUEMsYUFBVyxNQUZKO0FBR1BDLFdBQVMsbUJBQVk7QUFDbkIsVUFBT2hFLEVBQUUsbUJBQUYsRUFBdUI4RCxJQUF2QixFQUFQO0FBQ0Q7QUFMTSxFQURUOztBQVNBOUQsR0FBRSxZQUFGLEVBQ0N1QyxPQURELENBQ1M7QUFDUHVCLFFBQUssSUFERTtBQUVQQyxhQUFXLE1BRko7QUFHUEMsV0FBUyxtQkFBWTtBQUNuQixVQUFPaEUsRUFBRSxjQUFGLEVBQWtCOEQsSUFBbEIsRUFBUDtBQUNEO0FBTE0sRUFEVDtBQVVILENBaENEOztBQWtDQTlELEVBQUU4SSxRQUFGLEVBQVk1RSxFQUFaLENBQWUsT0FBZixFQUF3QixXQUF4QixFQUFxQyxZQUFXO0FBQzVDLEtBQUk1QixTQUFTdEMsRUFBRSxrQkFBRixFQUFzQmUsR0FBdEIsRUFBYjtBQUNBakssV0FBVXVMLGFBQVYsQ0FBd0JDLE1BQXhCO0FBQ0gsQ0FIRDs7QUFLQXRDLEVBQUU4SSxRQUFGLEVBQVk1RSxFQUFaLENBQWUsUUFBZixFQUF5QixZQUF6QixFQUF1QyxZQUFXO0FBQzlDLEtBQUliLE9BQU9yRCxFQUFFLHFCQUFGLEVBQXlCZSxHQUF6QixFQUFYO0FBQ0EsS0FBR3NDLFFBQVEsVUFBUixJQUFzQkEsUUFBUSxRQUFqQyxFQUEwQztBQUN6Q3JELElBQUUsVUFBRixFQUFjcUosSUFBZDtBQUNIckosSUFBRSxhQUFGLEVBQWlCaUosSUFBakI7QUFDRyxFQUhELE1BSUssSUFBRzVGLFFBQU8sVUFBVixFQUFxQjtBQUM1QnJELElBQUUsVUFBRixFQUFjcUosSUFBZDtBQUNBckosSUFBRSxhQUFGLEVBQWlCcUosSUFBakI7QUFDRyxFQUhJLE1BSUQ7QUFDSHJKLElBQUUsVUFBRixFQUFjaUosSUFBZDtBQUNIakosSUFBRSxhQUFGLEVBQWlCaUosSUFBakI7QUFDR2pKLElBQUUsdUJBQUYsRUFBMkJlLEdBQTNCLENBQStCLEVBQS9CO0FBQ0E7QUFDSixDQWZEOztBQWlCQWYsRUFBRThJLFFBQUYsRUFBWTVFLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFlBQXhCLEVBQXNDLFlBQVc7QUFDN0MsS0FBSTVCLFNBQVN0QyxFQUFFLHVCQUFGLEVBQTJCZSxHQUEzQixFQUFiO0FBQ0EsS0FBSXVDLFNBQVN0RCxFQUFFLHVCQUFGLEVBQTJCZSxHQUEzQixFQUFiO0FBQ0EsS0FBSXNDLE9BQU9yRCxFQUFFLHFCQUFGLEVBQXlCZSxHQUF6QixFQUFYO0FBQ0E7QUFDQWpLLFdBQVVpSCxRQUFWLENBQW1Cc0YsSUFBbkIsRUFBd0JDLE1BQXhCLEVBQStCaEIsTUFBL0I7QUFDSCxDQU5EOztBQVFBdEMsRUFBRThJLFFBQUYsRUFBWTVFLEVBQVosQ0FBZSxPQUFmLEVBQXdCLGNBQXhCLEVBQXdDLFlBQVc7QUFDL0NsRSxHQUFFLHVCQUFGLEVBQTJCZSxHQUEzQixDQUErQnVJLEtBQUtDLEdBQUwsQ0FBU3pTLFVBQVVvQyxPQUFuQixDQUEvQjtBQUNILENBRkQ7O0FBSUE4RyxFQUFFOEksUUFBRixFQUFZNUUsRUFBWixDQUFlLE9BQWYsRUFBd0Isb0JBQXhCLEVBQThDLFlBQVc7QUFDckQsS0FBSWdCLFVBQVVsRixFQUFFLHNCQUFGLEVBQTBCZSxHQUExQixFQUFkO0FBQ0FqSyxXQUFVbU8sWUFBVixDQUF1QkMsT0FBdkI7QUFDSCxDQUhEOztBQUtBbEYsRUFBRThJLFFBQUYsRUFBWTVFLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFVBQVVOLENBQVYsRUFBYTtBQUNqQzVELEdBQUUsK0NBQUYsRUFBbUR3SixJQUFuRCxDQUF3RCxZQUFZO0FBQ2hFLE1BQUksQ0FBQ3hKLEVBQUUsSUFBRixFQUFReUosRUFBUixDQUFXN0YsRUFBRThGLE1BQWIsQ0FBRCxJQUF5QjFKLEVBQUUsSUFBRixFQUFRMkosR0FBUixDQUFZL0YsRUFBRThGLE1BQWQsRUFBc0JoSixNQUF0QixLQUFpQyxDQUExRCxJQUErRFYsRUFBRSxVQUFGLEVBQWMySixHQUFkLENBQWtCL0YsRUFBRThGLE1BQXBCLEVBQTRCaEosTUFBNUIsS0FBdUMsQ0FBMUcsRUFBNkc7QUFDekcsSUFBQyxDQUFDVixFQUFFLElBQUYsRUFBUXVDLE9BQVIsQ0FBZ0IsTUFBaEIsRUFBd0JyTSxJQUF4QixDQUE2QixZQUE3QixLQUE0QyxFQUE3QyxFQUFpRDBULE9BQWpELElBQTBELEVBQTNELEVBQStEdkIsS0FBL0QsR0FBdUUsS0FBdkUsQ0FEeUcsQ0FDM0I7QUFDakY7QUFDSixFQUpEO0FBS0gsQ0FORDs7QUFRQXJJLEVBQUU4SSxRQUFGLEVBQVk1RSxFQUFaLENBQWUsa0JBQWYsRUFBbUMscUJBQW5DLEVBQTBELFlBQVU7QUFDNUQsS0FBSTJGLGFBQWEsS0FBS0MsS0FBdEI7QUFDQSxNQUFLQSxLQUFMLEdBQWEsRUFBYjtBQUNBLE1BQUtDLGdCQUFMLEdBQXdCLEtBQUtDLFlBQTdCO0FBQ0EsTUFBS0YsS0FBTCxHQUFhRCxVQUFiO0FBQ0gsQ0FMTCxFQU1LM0YsRUFOTCxDQU1RLGtCQU5SLEVBTTRCLHFCQU41QixFQU1tRCxZQUFVO0FBQ3JELEtBQUkrRixVQUFVLEtBQUtDLFlBQUwsQ0FBa0IsZUFBbEIsSUFBbUMsQ0FBakQ7QUFBQSxLQUFvREMsSUFBcEQ7QUFDQSxNQUFLQSxJQUFMLEdBQVlGLE9BQVo7QUFDQUUsUUFBT2IsS0FBS2MsSUFBTCxDQUFVLENBQUMsS0FBS0osWUFBTCxHQUFvQixLQUFLRCxnQkFBMUIsSUFBOEMsRUFBeEQsQ0FBUDtBQUNBLE1BQUtJLElBQUwsR0FBWUYsVUFBVUUsSUFBdEI7QUFDSCxDQVhMOztBQWFBbkssRUFBRThJLFFBQUYsRUFBWTVFLEVBQVosQ0FBZSxPQUFmLEVBQXdCLDBCQUF4QixFQUFvRCxVQUFTbUcsS0FBVCxFQUFnQjtBQUNoRUEsT0FBTUMsY0FBTjtBQUNBdEssR0FBRSxJQUFGLEVBQVF1SyxZQUFSO0FBQ0gsQ0FIRCxFOzs7Ozs7O0FDeitCQSxlQUFlLEtBQWlELGdKQUFnSixpQkFBaUIsbUJBQW1CLGNBQWMsNEJBQTRCLFlBQVkscUJBQXFCLDJEQUEyRCxTQUFTLG1DQUFtQyxTQUFTLHFCQUFxQixxQ0FBcUMsb0NBQW9DLEVBQUUsaUJBQWlCLGlDQUFpQyxpQkFBaUIsWUFBWSxVQUFVLHNCQUFzQixtQkFBbUIsaURBQWlELG1CQUFtQixnQkFBZ0IsOElBQThJLDhCQUE4QixpQkFBaUIsZ0VBQWdFLHVCQUF1QixrREFBa0QsVUFBVSxpQkFBaUIsV0FBVyxzQkFBc0IsaURBQWlELFVBQVUsaUJBQWlCLDJEQUEyRCwwRUFBMEUsV0FBVyxnQ0FBZ0MsZ0NBQWdDLEVBQUUsU0FBUyxvS0FBb0ssMEVBQTBFLGlCQUFpQiwyQkFBMkIsa0NBQWtDLE1BQU0sZUFBZSxVQUFVLElBQUksRUFBRSxlQUFlLHNCQUFzQix3REFBd0QsaUJBQWlCLHdGQUF3RixnQ0FBZ0MsaUJBQWlCLDhCQUE4QiwyQkFBMkIsMEpBQTBKLDJDQUEyQyxxREFBcUQsRUFBRSxlQUFlLHNCQUFzQixJQUFJLFlBQVksU0FBUyxXQUFXLGlCQUFpQixvQkFBb0IsK0JBQStCLHVCQUF1QixpQkFBaUIsaUJBQWlCLGVBQWUsUUFBUSxVQUFVLHNCQUFzQiw4QkFBOEIsZUFBZSxpQkFBaUIsaUJBQWlCLDhCQUE4QixpQkFBaUIsWUFBWSwwQkFBMEIsNEJBQTRCLFVBQVUsMEJBQTBCLG9CQUFvQiw0QkFBNEIsc0JBQXNCLDhCQUE4Qix3QkFBd0Isa0JBQWtCLDhCQUE4QixlQUFlLFFBQVEsZ0JBQWdCLHdCQUF3QixvQkFBb0IsaUJBQWlCLG1EQUFtRCwrQ0FBK0MsNkJBQTZCLGdCQUFnQixVQUFVLG9FQUFvRSxxQ0FBcUMsZUFBZSxzQkFBc0IsaUVBQWlFLFVBQVUsZUFBZSxhQUFhLGVBQWUsc0JBQXNCLHlEQUF5RCxVQUFVLGlCQUFpQixhQUFhLFdBQVcsd0JBQXdCLHdCQUF3QiwwQkFBMEIsaUJBQWlCLEdBQUcsaUJBQWlCLG9CQUFvQixzQkFBc0IsZ0JBQWdCLGlCQUFpQix1QkFBdUIsc0JBQXNCLHVDQUF1QyxpQkFBaUIsNENBQTRDLHdCQUF3Qix3REFBd0QsdUJBQXVCLGtGQUFrRixJQUFJLHNEQUFzRCxvQkFBb0IsZ0JBQWdCLGdCQUFnQixnQkFBZ0IsaUJBQWlCLG1CQUFtQix1QkFBdUIsaUJBQWlCLHNEQUFzRCxzQkFBc0IsZ0NBQWdDLGVBQWUscUhBQXFILGlCQUFpQixXQUFXLGlFQUFpRSw0Q0FBNEMsZUFBZSxhQUFhLGVBQWUsd0JBQXdCLE9BQU8sZ0VBQWdFLGlCQUFpQiw0Q0FBNEMsMEJBQTBCLG1DQUFtQyx3QkFBd0IsR0FBRyxpQkFBaUIsNEJBQTRCLHNCQUFzQiwwQkFBMEIsaUJBQWlCLFlBQVksc0JBQXNCLHFCQUFxQixpQkFBaUIsV0FBVyx3QkFBd0Isa0JBQWtCLFFBQVEsaUVBQWlFLDZEQUE2RCxrRUFBa0UsNERBQTRELGVBQWUsd0JBQXdCLHNCQUFzQixtRUFBbUUsaUJBQWlCLGFBQWEsMkxBQTJMLGNBQWMsbUNBQW1DLG9CQUFvQiw0QkFBNEIsbUJBQW1CLGdEQUFnRCxnQkFBZ0Isd0JBQXdCLHlCQUF5QixNQUFNLDBCQUEwQixNQUFNLGlCQUFpQixzQ0FBc0MsSUFBSSw4Q0FBOEMsc0JBQXNCLFVBQVUscUNBQXFDLGNBQWMsb0NBQW9DLHVDQUF1QyxrQkFBa0IsMkNBQTJDLGtOQUFrTixXQUFXLHdDQUF3QyxrREFBa0QsaUJBQWlCLGFBQWEsY0FBYyx1REFBdUQsY0FBYyxrQkFBa0Isa0NBQWtDLGdCQUFnQiw4SEFBOEgsb0JBQW9CLDRCQUE0QixtQkFBbUIsRUFBRSxjQUFjLDRCQUE0QixrQkFBa0IsRUFBRSxnQkFBZ0IsbUJBQW1CLDhCQUE4QixrQ0FBa0MsNkJBQTZCLG9CQUFvQixNQUFNLHNCQUFzQixtQkFBbUIseUJBQXlCLE1BQU0sZ0hBQWdILG9CQUFvQixxQkFBcUIsMENBQTBDLEdBQUcsNE9BQTRPLDhDQUE4QyxJQUFJLHNCQUFzQixtQkFBbUIsOEJBQThCLFlBQVksS0FBSyxFQUFFLEtBQUssZ0JBQWdCLE9BQU8sbUZBQW1GLFFBQVEsZ0JBQWdCLHdCQUF3QixVQUFVLHVCQUF1QixXQUFXLHdCQUF3QixRQUFRLDZCQUE2QixVQUFVLFVBQVUsWUFBWSxRQUFRLFlBQVksYUFBYSx3QkFBd0IsZ0JBQWdCLHdCQUF3QixlQUFlLHdCQUF3QixjQUFjLG9DQUFvQyxhQUFhLHdCQUF3QixhQUFhLHdCQUF3QixnQkFBZ0Isd0JBQXdCLGNBQWMsb0NBQW9DLHlCQUF5QixXQUFXLHdCQUF3QixpQkFBaUIsa0RBQWtELGNBQWMsMEJBQTBCLE1BQU0saUNBQWlDLEtBQUssYUFBYSxlQUFlLHdCQUF3QixjQUFjLFlBQVksYUFBYSxZQUFZLGNBQWMsd0JBQXdCLFlBQVksOEJBQThCLFVBQVUsaUJBQWlCLHdCQUF3QixpQkFBaUIseUJBQXlCLG9CQUFvQixrWEFBa1gsV0FBVyx5QkFBeUIsdUZBQXVGLDRCQUE0Qix1RUFBdUUsMFRBQTBULGlCQUFpQixhQUFhLGlCQUFpQixnQ0FBZ0Msc0JBQXNCLFdBQVcsdURBQXVELG9CQUFvQixxQkFBcUIsdUJBQXVCLFdBQVcsc0ZBQXNGLHlEQUF5RCxFQUFFLCtCQUErQixtS0FBbUssUUFBUSx5QkFBeUIsdUdBQXVHLG1CQUFtQixpREFBaUQsVUFBVSxvQkFBb0IsaUdBQWlHLCtCQUErQiwwR0FBMEcsMEJBQTBCLG1EQUFtRCwwQkFBMEIsY0FBYyw4QkFBOEIsb0RBQW9ELHdCQUF3QixxQ0FBcUMsb0NBQW9DLDRCQUE0QixpQkFBaUIsMEJBQTBCLG1DQUFtQyxxQ0FBcUMsaUJBQWlCLHNCQUFzQixnRUFBZ0UsbUxBQW1MLGtIQUFrSCxLQUFLLG9FQUFvRSwyS0FBMkssdUNBQXVDLHlCQUF5QiwyQ0FBMkMsdUNBQXVDLEVBQUUsb0NBQW9DLGlEQUFpRCw0Q0FBNEMsdUNBQXVDLEVBQUUsOEJBQThCLEtBQUsscURBQXFELHlGQUF5RixnQ0FBZ0Msa0RBQWtELDJCQUEyQixpRUFBaUUsbUJBQW1CLGdGQUFnRiwrRkFBK0YsaURBQWlELDBFQUEwRSw4QkFBOEIsc0NBQXNDLDBDQUEwQyw4QkFBOEIseUtBQXlLLHFCQUFxQixXQUFXLHFPQUFxTyw4QkFBOEIsZ0RBQWdELHVCQUF1Qix5S0FBeUssbUJBQW1CLDhDQUE4QywyQkFBMkIsK0JBQStCLHdHQUF3Ryx5UUFBeVEsaUJBQWlCLGFBQWEsNkJBQTZCLE9BQU8sS0FBSyxnQkFBZ0IsT0FBTywyQkFBMkIsUUFBUSxhQUFhLHdCQUF3QixlQUFlLHdCQUF3QixXQUFXLDJCQUEyQixzQ0FBc0MsNEJBQTRCLCtDQUErQyxRQUFRLDJCQUEyQixxQkFBcUIsbUJBQW1CLHNCQUFzQixVQUFVLDhCQUE4QixPQUFPLHdIQUF3SCw4QkFBOEIsV0FBVywwRkFBMEYsb0NBQW9DLHVDQUF1QyxFQUFFLHFDQUFxQyxvRUFBb0UsRUFBRSxpRUFBaUUsRUFBRSw4QkFBOEIsNkVBQTZFLHFHQUFxRywyQkFBMkIsb1lBQW9ZLDRCQUE0QixpWUFBaVkseUJBQXlCLG9GQUFvRiwwQkFBMEIsNk9BQTZPLHdCQUF3Qix1Q0FBdUMsaUJBQWlCLGFBQWEsb0NBQW9DLDRDQUE0QyxpQ0FBaUMsWUFBWSxvQ0FBb0MsaUdBQWlHLGtFQUFrRSxpQkFBaUIsYUFBYSxxQ0FBcUMsS0FBSywrQ0FBK0MsTUFBTSx1QkFBdUIsY0FBYyw0Q0FBNEMsbUJBQW1CLGtEQUFrRCxnQkFBZ0IsK0JBQStCLGdCQUFnQiw0Q0FBNEMscUJBQXFCLG9EQUFvRCxhQUFhLHdCQUF3QixRQUFRLDBCQUEwQixZQUFZLHdCQUF3QixZQUFZLGtDQUFrQyxnQ0FBZ0MsVUFBVSx3QkFBd0IsV0FBVyx3QkFBd0IsZ0JBQWdCLHVCQUF1QixnQkFBZ0Isd0JBQXdCLGdCQUFnQix3QkFBd0IsV0FBVyx1QkFBdUIsV0FBVyxnQ0FBZ0Msc0ZBQXNGLGlDQUFpQyxpRUFBaUUsMEJBQTBCLCtEQUErRCx3QkFBd0IsNkJBQTZCLDhCQUE4Qiw2Q0FBNkMsbUNBQW1DLGtEQUFrRCw0QkFBNEIsMkNBQTJDLGlDQUFpQyxnREFBZ0QsOEJBQThCLDZDQUE2Qyx1QkFBdUIscUZBQXFGLGFBQWEsRUFBRSwyQ0FBMkMseUJBQXlCLDRCQUE0Qix1QkFBdUIsRUFBRSxpQkFBaUIsb0JBQW9CLG1LQUFtSyw0QkFBNEIsNkhBQTZILGlCQUFpQiw0Q0FBNEMseUJBQXlCLHdCQUF3QixZQUFZLGlCQUFpQiw0QkFBNEIsc0JBQXNCLHVCQUF1QixvQ0FBb0MsWUFBWSxLQUFLLElBQUksMkJBQTJCLFVBQVUsSUFBSSw0Q0FBNEMsZUFBZSxpQkFBaUIsNkRBQTZELGlCQUFpQixvQkFBb0IsSUFBSSxZQUFZLFlBQVksc0JBQXNCLFVBQVUsMkpBQTJKLGlCQUFpQixhQUFhLFdBQVcscUJBQXFCLG1CQUFtQixpSEFBaUgsaUJBQWlCLG9CQUFvQiwrQkFBK0IsaUJBQWlCLGtDQUFrQyxrREFBa0QsZUFBZSxVQUFVLElBQUksRUFBRSxpQkFBaUIsV0FBVyxxQ0FBcUMscUJBQXFCLGlCQUFpQixhQUFhLGNBQWMsUUFBUSxpQ0FBaUMscUVBQXFFLFFBQVEscUNBQXFDLFlBQVksd0JBQXdCLGlCQUFpQixpQkFBaUIsNkRBQTZELGNBQWMsbUNBQW1DLHVLQUF1SyxJQUFJLDBCQUEwQixZQUFZLHVDQUF1QyxNQUFNLDhGQUE4RixpQkFBaUIsc0ZBQXNGLHlCQUF5QiwwQkFBMEIsY0FBYyxVQUFVLHlDQUF5QyxpQkFBaUIsb0RBQW9ELHdCQUF3QixzQkFBc0IsbUNBQW1DLEtBQUssV0FBVyxxQ0FBcUMsVUFBVSxpQkFBaUIsb0JBQW9CLG1DQUFtQyxlQUFlLGlCQUFpQiwwQkFBMEIsd0JBQXdCLHlDQUF5QyxhQUFhLGtDQUFrQyxpQkFBaUIseUVBQXlFLEVBQUUseUJBQXlCLGtDQUFrQyxFQUFFLHVCQUF1Qiw4RkFBOEYsRUFBRSxpQkFBaUIscUNBQXFDLHdCQUF3Qix5QkFBeUIsK0NBQStDLGlCQUFpQixnSEFBZ0gsUUFBUSxnQkFBZ0IsMEJBQTBCLHFCQUFxQixvQ0FBb0Msd0JBQXdCLDJFQUEyRSxZQUFZLGlCQUFpQix5SUFBeUksY0FBYyxZQUFZLHdCQUF3QixXQUFXLGlCQUFpQixlQUFlLGdCQUFnQixxQkFBcUIsaUJBQWlCLG1CQUFtQix3QkFBd0IseUJBQXlCLHdDQUF3QyxRQUFRLGVBQWUsWUFBWSxrQ0FBa0MscUJBQXFCLHdCQUF3QixnQkFBZ0Isc0pBQXNKLHdCQUF3QixzRkFBc0YseURBQXlELCtCQUErQixhQUFhLHVCQUF1QixhQUFhLGVBQWUsZUFBZSw2QkFBNkIsc0JBQXNCLG1DQUFtQyxpQkFBaUIsYUFBYSwyQkFBMkIscUNBQXFDLEtBQUssdUJBQXVCLGlCQUFpQix5REFBeUQsZ0JBQWdCLGlCQUFpQixhQUFhLG1QQUFtUCx3QkFBd0IsSUFBSSxzQ0FBc0MsK0JBQStCLFFBQVEsOEhBQThILFdBQVcsaUJBQWlCLE1BQU0sZ0RBQWdELGlCQUFpQixVQUFVLFFBQVEsV0FBVyxhQUFhLDZCQUE2QixXQUFXLGNBQWMsNERBQTRELElBQUksNkpBQTZKLFNBQVMsc0JBQXNCLFNBQVMsK0JBQStCLEdBQUcsZUFBZSxvQkFBb0Isd0JBQXdCLHNCQUFzQixpRUFBaUUsbUJBQW1CLG1FQUFtRSxpREFBaUQsRUFBRSxlQUFlLHlDQUF5QyxlQUFlLG9CQUFvQixNQUFNLDREQUE0RCxzQkFBc0IsRUFBRSxFQUFFLGVBQWUsV0FBVywwRUFBMEUsZUFBZSxhQUFhLFVBQVUsa0JBQWtCLElBQUkscURBQXFELHNCQUFzQixPQUFPLFlBQVksSUFBSSw0QkFBNEIsU0FBUyxhQUFhLDBCQUEwQixTQUFTLFFBQVEsV0FBVyxPQUFPLGtCQUFrQiwyQ0FBMkMsSUFBSSwyQkFBMkIsU0FBUyxnQkFBZ0IsZUFBZSxtRkFBbUYsZ0NBQWdDLG1CQUFtQixtQkFBbUIscUtBQXFLLG1CQUFtQiw0QkFBNEIsZUFBZSxZQUFZLDBEQUEwRCxtQkFBbUIsa0NBQWtDLG9CQUFvQixVQUFVLDhFQUE4RSxtQkFBbUIsY0FBYyxpQ0FBaUMsK0JBQStCLG9CQUFvQixnQ0FBZ0MsbUNBQW1DLGtCQUFrQixjQUFjLGdCQUFnQix3REFBd0QsaUJBQWlCLG1CQUFtQixlQUFlLGlEQUFpRCwyQkFBMkIsSUFBSSxZQUFZLEVBQUUsNkJBQTZCLGtCQUFrQiw0Q0FBNEMsbUJBQW1CLCtCQUErQixFQUFFLEVBQUUsOEJBQThCLEVBQUUsaUJBQWlCLGFBQWEsMENBQTBDLHFCQUFxQixvQkFBb0IsMERBQTBELCtCQUErQixnQ0FBZ0MsU0FBUyxFQUFFLGlCQUFpQixnQ0FBZ0MsUUFBUSxFQUFFLEtBQUssRUFBRSxpQkFBaUIsYUFBYSxjQUFjLE1BQU0sOERBQThELGNBQWMsaUJBQWlCLGFBQWEsa0JBQWtCLHlDQUF5QyxrREFBa0QsV0FBVyxNQUFNLGlCQUFpQixhQUFhLGNBQWMsaUZBQWlGLGdCQUFnQixhQUFhLG9HQUFvRyxLQUFLLGNBQWMsOEVBQThFLFlBQVksYUFBYSxnR0FBZ0csS0FBSyxNQUFNLGlCQUFpQixhQUFhLHNDQUFzQyxTQUFTLEVBQUUsK0VBQStFLCtCQUErQixXQUFXLHNDQUFzQyxXQUFXLGtDQUFrQyxXQUFXLGdCQUFnQixlQUFlLDRCQUE0QixzRkFBc0YsVUFBVSxpQkFBaUIsb0NBQW9DLDhCQUE4QixLQUFLLG1EQUFtRCxhQUFhLEVBQUUsV0FBVyxZQUFZLE1BQU0sa0ZBQWtGLEtBQUssV0FBVywrQkFBK0IsVUFBVSxpQkFBaUIscUNBQXFDLHNCQUFzQixNQUFNLGtKQUFrSixpQkFBaUIsWUFBWSx3QkFBd0IscUJBQXFCLGlCQUFpQixhQUFhLHdDQUF3QywwQkFBMEIsd0NBQXdDLGFBQWEsU0FBUyx1QkFBdUIsU0FBUyxhQUFhLG9FQUFvRSx3QkFBd0IsYUFBYSxzQkFBc0IsSUFBSSxpQkFBaUIsdURBQXVELEtBQUssaUNBQWlDLDJCQUEyQixTQUFTLHlCQUF5QiwrREFBK0QsU0FBUyxrQkFBa0IsSUFBSSw4REFBOEQscUJBQXFCLG1CQUFtQiw4Q0FBOEMscUJBQXFCLGlCQUFpQix1QkFBdUIsMEJBQTBCLHNCQUFzQixzRkFBc0YsZUFBZSwwQkFBMEIsaUJBQWlCLGlCQUFpQiw4QkFBOEIsdUNBQXVDLGlEQUFpRCwyREFBMkQscUVBQXFFLHFCQUFxQixpQkFBaUIsaURBQWlELHNCQUFzQiw0Q0FBNEMsaUJBQWlCLFdBQVcsNEJBQTRCLElBQUksOEJBQThCLFNBQVMsZUFBZSxtQ0FBbUMsaUJBQWlCLGFBQWEsaUNBQWlDLG1DQUFtQyxZQUFZLDRCQUE0QixpQkFBaUIsWUFBWSxzQkFBc0IsaUJBQWlCLGFBQWEsaUlBQWlJLGFBQWEsa0NBQWtDLFNBQVMsd0JBQXdCLDBCQUEwQixVQUFVLDBDQUEwQyxzQkFBc0Isa0JBQWtCLHNCQUFzQixxSkFBcUosb0pBQW9KLG9CQUFvQixzREFBc0Qsb0RBQW9ELGtDQUFrQywyQkFBMkIsVUFBVSxpQkFBaUIsNEJBQTRCLElBQUksZUFBZSxvQkFBb0IsS0FBSyx5QkFBeUIsUUFBUSxFQUFFLFVBQVUsd0JBQXdCLG1CQUFtQixTQUFTLElBQUksbUJBQW1CLGtCQUFrQixPQUFPLFdBQVcsaUJBQWlCLFNBQVMsTUFBTSxVQUFVLFVBQVUsZUFBZSx3QkFBd0IsT0FBTyxtQkFBbUIsaUJBQWlCLG1IQUFtSCxxQkFBcUIsdUJBQXVCLFFBQVEsOEJBQThCLEVBQUUsRUFBRSxnQkFBZ0IsSUFBSSxJQUFJLFNBQVMsd0JBQXdCLHVCQUF1QixrQkFBa0IsZUFBZSxpRUFBaUUsd0JBQXdCLGFBQWEsV0FBVyxrQkFBa0IsYUFBYSxLQUFLLHVDQUF1QyxvQkFBb0IsaUJBQWlCLGVBQWUsYUFBYSxtQkFBbUIsT0FBTyxrQkFBa0IsaUNBQWlDLGlCQUFpQiwyQkFBMkIscURBQXFELEtBQUssZ0NBQWdDLElBQUksc0JBQXNCLFVBQVUsaUJBQWlCLGlEQUFpRCw0Q0FBNEMsZUFBZSxpQkFBaUIsMkRBQTJELDZDQUE2QywySUFBMkksZUFBZSxNQUFNLHNCQUFzQixlQUFlLHNCQUFzQixJQUFJLE9BQU8sWUFBWSxTQUFTLE9BQU8sWUFBWSxpQkFBaUIsV0FBVywwQkFBMEIsNkJBQTZCLFVBQVUsaUJBQWlCLGtDQUFrQyx3RUFBd0UsV0FBVywyQ0FBMkMsaUJBQWlCLElBQUksbUdBQW1HLFNBQVMsS0FBSyxxQkFBcUIsd0NBQXdDLEdBQUcsc0JBQXNCLGlCQUFpQixhQUFhLDRDQUE0QyxzQkFBc0IsV0FBVyxzQkFBc0IsK0JBQStCLGFBQWEsR0FBRyxlQUFlLDJEQUEyRCxpQkFBaUIsa0NBQWtDLHdCQUF3QixtQ0FBbUMsaUJBQWlCLHlCQUF5Qiw2QkFBNkIsaUJBQWlCLHVDQUF1Qyw4Q0FBOEMsb0RBQW9ELGlCQUFpQixhQUFhLHNCQUFzQix3Q0FBd0MsbUJBQW1CLCtCQUErQixFQUFFLGlCQUFpQixhQUFhLGlFQUFpRSxrQ0FBa0Msb0JBQW9CLDREQUE0RCxFQUFFLGlCQUFpQixXQUFXLGVBQWUsY0FBYyxFQUFFLGlCQUFpQixhQUFhLHNCQUFzQixxQ0FBcUMsZ0JBQWdCLCtCQUErQixFQUFFLGlCQUFpQixhQUFhLG1CQUFtQix3Q0FBd0MsbUJBQW1CLG1EQUFtRCxFQUFFLGlCQUFpQiw4Q0FBOEMsK0RBQStELG1CQUFtQix5Q0FBeUMsRUFBRSxpQkFBaUIseURBQXlELDBCQUEwQixFQUFFLGlCQUFpQixpQ0FBaUMsbUJBQW1CLGFBQWEsc0NBQXNDLDBEQUEwRCxJQUFJLEVBQUUsaUJBQWlCLGFBQWEsTUFBTSx1REFBdUQsd0NBQXdDLGdCQUFnQixzQkFBc0IscUJBQXFCLEVBQUUsZUFBZSxjQUFjLDRGQUE0RixtQ0FBbUMsb0JBQW9CLEVBQUUsaUJBQWlCLGFBQWEseUJBQXlCLGtCQUFrQixrQkFBa0IsRUFBRSxpQkFBaUIsNEdBQTRHLG1oQkFBbWhCLFlBQVksV0FBVyxLQUFLLDRDQUE0QyxnRkFBZ0YsZ0JBQWdCLGVBQWUsZ0NBQWdDLGVBQWUsb0JBQW9CLGdEQUFnRCx1Q0FBdUMsaUhBQWlILE1BQU0sb0JBQW9CLDBQQUEwUCwrQkFBK0IsK0NBQStDLDRDQUE0Qyx3QkFBd0Isc0NBQXNDLE9BQU8saUNBQWlDLGlCQUFpQixhQUFhLGlCQUFpQiw4Q0FBOEMsZ0JBQWdCLGlDQUFpQyxpR0FBaUcsUUFBUSxvQ0FBb0MsS0FBSyxrQkFBa0IsYUFBYSxrQkFBa0IsOEJBQThCLHNCQUFzQiw0SkFBNEosYUFBYSx1SkFBdUosYUFBYSwyTEFBMkwsb0JBQW9CLHdFQUF3RSxpQkFBaUIseUJBQXlCLHNDQUFzQyxzQkFBc0Isb0RBQW9ELElBQUksZ0JBQWdCLCtCQUErQixnQkFBZ0IscUJBQXFCLDJDQUEyQyw2QkFBNkIsYUFBYSxrR0FBa0csdUNBQXVDLHFDQUFxQyw2QkFBNkIscUNBQXFDLFlBQVksVUFBVSx1Q0FBdUMsbUJBQW1CLDJDQUEyQyxrQ0FBa0MsS0FBSyxvQkFBb0IseUVBQXlFLHNDQUFzQyx1QkFBdUIsd0NBQXdDLE1BQU0sZ0RBQWdELEdBQUcsMkZBQTJGLDRDQUE0QywrREFBK0QsY0FBYyw4RUFBOEUsNEJBQTRCLE9BQU8sNkJBQTZCLDJCQUEyQixhQUFhLGtFQUFrRSxxQ0FBcUMsMENBQTBDLHdFQUF3RSxxSEFBcUgsV0FBVyxlQUFlLEtBQUssa0JBQWtCLCtCQUErQixtQkFBbUIsZ0NBQWdDLGtCQUFrQixrQ0FBa0MsbUJBQW1CLHdFQUF3RSxlQUFlLHNCQUFzQixxRkFBcUYsc0NBQXNDLGFBQWEsK0VBQStFLHVDQUF1QyxhQUFhLHdLQUF3SyxhQUFhLDZGQUE2RiwwQ0FBMEMsR0FBRyxvREFBb0Qsc0NBQXNDLHNCQUFzQix3Q0FBd0MsMkRBQTJELHFCQUFxQix3REFBd0QsMkNBQTJDLHNCQUFzQix3Q0FBd0MseUhBQXlILE9BQU8sb0JBQW9CLFdBQVcsYUFBYSxnRUFBZ0UsK0RBQStELGlDQUFpQyxRQUFRLGNBQWMsS0FBSyx1Q0FBdUMscUJBQXFCLFVBQVUsd0RBQXdELDRGQUE0RixrQ0FBa0MsZ09BQWdPLGVBQWUseUNBQXlDLGtEQUFrRCxzRUFBc0Usb0lBQW9JLEtBQUssa0JBQWtCLGdDQUFnQyx3QkFBd0IsMENBQTBDLGtCQUFrQiwrREFBK0QseUJBQXlCLHlEQUF5RCxxRUFBcUUsNEdBQTRHLEtBQUssdUJBQXVCLDBDQUEwQywrQkFBK0IsdUJBQXVCLHNDQUFzQywrREFBK0QseUJBQXlCLGVBQWUsMkJBQTJCLGFBQWEsMExBQTBMLEVBQUUsWUFBWSxrQ0FBa0MsNEdBQTRHLGFBQWEsNEtBQTRLLEVBQUUsWUFBWSxrQ0FBa0MsMkZBQTJGLFNBQVMsNEJBQTRCLE1BQU0sR0FBRyxFOzs7Ozs7O0FDQS90M0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0Esd0NBQXdDLGdCQUFnQjtBQUN4RCxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksb0JBQW9CO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNqREE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsRUFBb1Q7QUFDMVUsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNJQTtBQUNBO0FBQ0EsdUNBREE7QUFFQSxnQ0FGQTtBQUdBOztBQUhBO0FBREEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNXQTtBQUNBLDRCQURBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUZBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdUNBO0FBQ0EsS0FEQSxrQkFDQTtBQUNBO0FBQ0EsMkJBREE7O0FBR0EsYUFIQTtBQUlBLFlBSkE7QUFLQSxvQkFMQTtBQU1BLGVBTkE7QUFPQTtBQVBBO0FBU0EsRUFYQTs7O0FBYUE7QUFDQSxjQURBLHdCQUNBLEtBREEsRUFDQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BRkEsTUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQVpBOztBQWNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBRkEsRUFFQSxJQUZBO0FBR0E7QUFDQSxHQTlCQTtBQWdDQSxtQkFoQ0EsK0JBZ0NBO0FBQ0E7QUFDQSxHQWxDQTtBQW9DQSxRQXBDQSxrQkFvQ0EsS0FwQ0EsRUFvQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQUZBLEVBRUEsR0FGQTtBQUdBLEdBOUNBO0FBZ0RBLGtCQWhEQSw4QkFnREE7QUFBQTs7QUFDQSx1REFDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEE7QUFJQSxHQXJEQTtBQXVEQSxVQXZEQSxzQkF1REE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsTUFIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxHQXhGQTtBQTBGQSxjQTFGQSwwQkEwRkE7QUFBQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw2RUFDQSxRQURBLEVBRUE7QUFDQTtBQUNBO0FBREE7QUFEQSxLQUZBLEVBUUEsSUFSQSxDQVFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQXhCQTtBQXlCQTtBQUNBO0FBdElBLEVBYkE7O0FBc0pBLFFBdEpBLHFCQXNKQTtBQUNBO0FBQ0EsRUF4SkE7QUEwSkEsUUExSkEscUJBMEpBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBRkE7QUFHQTtBQWhLQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNUJBO0FBQ0EsaUJBREE7QUFFQTtBQUNBLFVBREEsa0JBQ0EsRUFEQSxFQUNBO0FBQ0E7QUFDQTtBQUhBLEdBRkE7QUFPQTtBQUNBLFVBREEsb0JBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVBBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzRGQTtBQUNBLGlDQURBOztBQUdBLEtBSEEsa0JBR0E7QUFDQTtBQUNBLGlCQURBOztBQUdBLG1CQUhBOztBQUtBLFdBTEE7O0FBT0EsY0FQQTs7QUFTQSxrQkFUQTtBQVVBLG1CQVZBO0FBV0EsbUJBWEE7O0FBYUEseUJBYkE7O0FBZUE7QUFDQSxnQkFEQTtBQUVBLFlBRkE7QUFHQSxnQkFIQTtBQUlBLGNBSkE7QUFLQSwwQ0FMQTtBQU1BLGlEQU5BO0FBT0EsaUJBUEE7QUFRQSxZQVJBO0FBU0E7QUFUQSxNQVVBLDRCQVZBO0FBZkE7QUEyQkEsRUEvQkE7OztBQWlDQTtBQUVBLGtCQUZBLDhCQUVBO0FBQUE7O0FBQ0EsMkNBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBLEtBRkEsRUFFQSxJQUZBO0FBSUEsSUFQQSxFQVFBLEtBUkEsQ0FRQTtBQUFBO0FBQUEsSUFSQTtBQVNBLEdBWkE7QUFjQSx3QkFkQSxvQ0FjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQTNCQTtBQTZCQSxlQTdCQSwyQkE2QkE7QUFBQTs7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBRkE7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FGQTs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBSEEsTUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFMQSxNQU1BO0FBQ0EsK0RBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQXZCQSxFQXdCQSxLQXhCQSxDQXdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0E3QkE7QUE4QkEsSUEvQkEsTUFnQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxHQTFHQTtBQTRHQSxXQTVHQSxxQkE0R0EsVUE1R0EsRUE0R0EsTUE1R0EsRUE0R0EsUUE1R0EsRUE0R0E7QUFBQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBRkEsTUFFQTtBQUNBLDBEQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0EsTUFIQTtBQUlBOztBQUVBO0FBQ0E7QUFDQSxLQUZBLE1BRUE7QUFDQSw0REFDQSxJQURBLENBQ0E7QUFDQTtBQUNBLE1BSEE7QUFJQTtBQUNBLElBbEJBLE1Ba0JBO0FBQ0E7QUFDQTtBQUNBOztBQUlBO0FBQ0E7QUFDQSxJQUZBLEVBRUEsSUFGQTtBQUdBLEdBNUlBO0FBOElBLFNBOUlBLG1CQThJQSxVQTlJQSxFQThJQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREEsTUFLQSxJQUxBLENBS0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUJBREE7QUFFQSwyQ0FGQTtBQUdBO0FBSEE7O0FBTUE7QUFDQSxLQVRBLEVBU0EsSUFUQTtBQVVBLElBcEJBO0FBcUJBLEdBcEtBO0FBdUtBLGtCQXZLQSw4QkF1S0E7QUFDQTtBQUNBLGlCQURBO0FBRUEseUNBRkE7QUFHQTtBQUhBOztBQU1BOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFKQTtBQUtBLEtBTkEsTUFNQTtBQUNBO0FBQ0E7QUFDQSxJQW5CQTtBQXNCQSxHQXRNQTtBQXdNQSxlQXhNQSwyQkF3TUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsbUJBREE7QUFFQSxxQkFGQTtBQUdBO0FBSEE7QUFLQSxJQU5BLEVBTUEsSUFOQTtBQU9BLEdBbE5BO0FBb05BLFVBcE5BLG9CQW9OQSxDQXBOQSxFQW9OQTtBQUNBOztBQUVBO0FBQ0EsMENBQ0Esb0VBREEsS0FHQTs7QUFFQTtBQUNBLElBUEEsTUFPQTtBQUNBOztBQUVBLDBDQUNBLG1EQURBLEtBR0E7O0FBRUE7QUFDQTtBQUNBO0FBeE9BLEVBakNBOztBQTRRQSxRQTVRQSxxQkE0UUE7QUFDQTtBQUNBLEVBOVFBO0FBZ1JBLFFBaFJBLHFCQWdSQTtBQUNBO0FBQ0E7QUFsUkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWUE7QUFDQSxpQ0FEQTs7QUFHQSxLQUhBLGtCQUdBO0FBQ0E7O0FBRUEsbUJBRkE7O0FBSUEsV0FKQTs7QUFNQSxjQU5BOztBQVFBLG1CQVJBOztBQVVBOztBQVZBO0FBYUEsRUFqQkE7O0FBa0JBLGFBbEJBO0FBcUJBO0FBRUEsc0JBRkEsa0NBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FkQTtBQWdCQSxtQkFoQkEsK0JBZ0JBO0FBQUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsSUFGQTs7QUFJQTtBQUNBO0FBQ0E7QUFDQSxJQUZBOztBQUlBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLElBRkEsTUFHQTtBQUNBO0FBQ0EsSUFGQSxNQUdBO0FBQ0EsdUVBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQWZBLEVBZ0JBLEtBaEJBLENBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FwQkE7QUFxQkE7QUFDQSxHQTVEQTtBQThEQSxXQTlEQSxxQkE4REEsRUE5REEsRUE4REE7QUFBQTs7QUFFQTs7QUFFQSxxREFDQSx3REFEQSxLQUdBOztBQUVBLG1FQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0EsSUFIQTs7QUFLQSxxRUFDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEE7O0FBS0E7QUFDQTtBQUNBO0FBQ0Esa0JBREE7QUFFQSwwQ0FGQTtBQUdBO0FBSEE7O0FBTUE7QUFDQTtBQUNBLElBVkEsRUFVQSxJQVZBO0FBWUEsR0E3RkE7QUFnR0EsZUFoR0EsMkJBZ0dBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQURBO0FBRUEscUJBRkE7QUFHQTtBQUhBO0FBS0EsSUFOQSxFQU1BLElBTkE7QUFPQTtBQTFHQSxFQXJCQTs7QUFrSUEsUUFsSUEscUJBa0lBLENBRUEsQ0FwSUE7QUFzSUEsUUF0SUEscUJBc0lBO0FBQ0E7QUFDQTtBQUNBLHFCQURBO0FBRUEsNEJBRkE7QUFHQSxvQkFIQTtBQUlBLHNCQUpBO0FBS0Esa0JBTEE7QUFNQTtBQU5BLEtBUUEsRUFSQSxDQVFBLFlBUkEsRUFRQTtBQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQVBBLENBT0EsSUFQQSxDQU9BLElBUEEsQ0FSQSxFQWdCQSxFQWhCQSxDQWdCQSxRQWhCQSxFQWdCQTtBQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQVBBLENBT0EsSUFQQSxDQU9BLElBUEEsQ0FoQkE7QUF3QkE7QUFoS0EsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25JQTtBQUNBLGlCQURBO0FBRUE7QUFDQSxVQURBLGtCQUNBLEVBREEsRUFDQTtBQUNBO0FBQ0EsS0FIQTtBQUlBLHFCQUpBLCtCQUlBO0FBQ0E7QUFDQTtBQU5BLEdBRkE7QUFVQTtBQUNBLFVBREEsb0JBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQWdCQSxLQWxCQTtBQW1CQSxNQW5CQSxnQkFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQWdCQTtBQXBDQTtBQVZBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM4QkE7QUFDQSxpQkFEQTtBQUVBO0FBQ0EsVUFEQSxrQkFDQSxFQURBLEVBQ0E7QUFDQTtBQUNBLEtBSEE7QUFJQSxxQkFKQSwrQkFJQTtBQUNBO0FBQ0EsS0FOQTtBQU9BLGVBUEEsdUJBT0EsT0FQQSxFQU9BO0FBQ0E7QUFDQTtBQUNBLEtBVkE7QUFXQSxlQVhBLHVCQVdBLE9BWEEsRUFXQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0EsS0FyQkE7QUFzQkEsV0F0QkEsbUJBc0JBLE9BdEJBLEVBc0JBO0FBQ0E7QUFDQTtBQUNBO0FBekJBLEdBRkE7QUE2QkE7QUFDQSxjQURBLHdCQUNBO0FBQ0E7QUFDQSxLQUhBO0FBSUEsTUFKQSxnQkFJQTtBQUNBO0FBQ0E7QUFOQTtBQTdCQSxHOzs7Ozs7O0FDakRBLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEVBQXdPO0FBQ2xQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEVBQW9NO0FBQzlNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSxpQkFBaUI7QUFDM0I7QUFDQTs7QUFFQSxtQkFBbUIsbUJBQU8sQ0FBQyxDQUFnQjs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsbUJBQW1CLHNCQUFzQjtBQUN6QztBQUNBO0FBQ0EsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsMkJBQTJCO0FBQ2hEO0FBQ0E7QUFDQSxZQUFZLHVCQUF1QjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EscUJBQXFCLHVCQUF1QjtBQUM1QztBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUN0TkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBNk87QUFDdlA7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBeU07QUFDbk47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyxpREFBaUQsb0JBQW9CLHlCQUF5Qix1QkFBdUIsZ0NBQWdDLHFCQUFxQixHQUFHLG9EQUFvRCwyQkFBMkIseUJBQXlCLDJCQUEyQixtQkFBbUIsb0JBQW9CLEdBQUcsb0RBQW9ELDJCQUEyQixtQkFBbUIsc0JBQXNCLGdDQUFnQyxtQkFBbUIsbUJBQW1CLHNCQUFzQix3QkFBd0IsZ0NBQWdDLGFBQWEsY0FBYyxlQUFlLGdCQUFnQixtQkFBbUIseUJBQXlCLEdBQUcscUNBQXFDLG1CQUFtQixHQUFHLHdEQUF3RCxvQkFBb0IsR0FBRyx5QkFBeUIsMEJBQTBCLEdBQUcsNEJBQTRCLHlCQUF5QixHQUFHLFVBQVUsK0VBQStFLE1BQU0sVUFBVSxXQUFXLFdBQVcsV0FBVyxVQUFVLEtBQUssS0FBSyxXQUFXLFdBQVcsV0FBVyxVQUFVLFVBQVUsS0FBSyxLQUFLLFdBQVcsVUFBVSxXQUFXLFdBQVcsVUFBVSxVQUFVLFdBQVcsV0FBVyxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsVUFBVSxXQUFXLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxXQUFXLHdpQkFBd2lCLFdBQVcsd1VBQXdVLHFCQUFxQix5dERBQXl0RCxXQUFXLHdTQUF3UyxjQUFjLGdCQUFnQixpS0FBaUssT0FBTyxtQkFBbUIsNkJBQTZCLDJCQUEyQix3RkFBd0YsdURBQXVELGtDQUFrQyxpQkFBaUIsT0FBTyxvREFBb0Qsb0RBQW9ELHNEQUFzRCxrREFBa0QsR0FBRyw0REFBNEQsdURBQXVELHlFQUF5RSxpQkFBaUIsT0FBTyx3REFBd0QsZ0RBQWdELDJDQUEyQyw0Q0FBNEMsaUJBQWlCLHVCQUF1Qiw2REFBNkQsbUJBQW1CLHNDQUFzQyx5Q0FBeUMsbUJBQW1CLFFBQVEsZUFBZSxTQUFTLGdDQUFnQyxrREFBa0QsU0FBUywwQkFBMEIsdUNBQXVDLHNDQUFzQyw4Q0FBOEMseUNBQXlDLDBDQUEwQyw4QkFBOEIsdUNBQXVDLGVBQWUsT0FBTyxTQUFTLCtCQUErQiw0RkFBNEYsd0RBQXdELGFBQWEsRUFBRSxTQUFTLHlCQUF5QiwrQkFBK0IsNENBQTRDLCtCQUErQiw4QkFBOEIsc0RBQXNELGFBQWEsT0FBTywwQkFBMEIsVUFBVSxPQUFPLG1EQUFtRCxrQ0FBa0Msd0ZBQXdGLGlCQUFpQixnREFBZ0Qsa0NBQWtDLG9GQUFvRixpQkFBaUIsNkRBQTZELGtDQUFrQyxxRkFBcUYsaUJBQWlCLGlEQUFpRCxrQ0FBa0MscUZBQXFGLGlCQUFpQiw4REFBOEQsa0NBQWtDLHNGQUFzRixpQkFBaUIsZUFBZSxhQUFhLDZCQUE2QixXQUFXLDZCQUE2QixpQ0FBaUMsNENBQTRDLDBCQUEwQixxQkFBcUIsT0FBTyw0Q0FBNEMsNERBQTRELGFBQWEsZ0VBQWdFLDBCQUEwQixzQkFBc0IsT0FBTywrREFBK0QsNkVBQTZFLG1FQUFtRSxxRUFBcUUsYUFBYSw0SEFBNEgsNEJBQTRCLDhFQUE4RSxlQUFlLG9EQUFvRCxrREFBa0QsOERBQThELG1DQUFtQyxnQ0FBZ0Msd0NBQXdDLG1DQUFtQyxvQ0FBb0Msa0RBQWtELCtDQUErQyx3REFBd0QsZUFBZSxtQkFBbUIsRUFBRSxhQUFhLFdBQVcsT0FBTyxvQkFBb0IsZ0NBQWdDLE9BQU8scUJBQXFCLHNCQUFzQixzREFBc0Qsb0NBQW9DLFdBQVcsRUFBRSxPQUFPLEtBQUssNERBQTRELG9CQUFvQix5QkFBeUIsdUJBQXVCLGdDQUFnQyxxQkFBcUIsS0FBSyxxQ0FBcUMsMkJBQTJCLHlCQUF5QiwyQkFBMkIsbUJBQW1CLG9CQUFvQixLQUFLLHFDQUFxQywyQkFBMkIsbUJBQW1CLHNCQUFzQixnQ0FBZ0MsbUJBQW1CLG1CQUFtQixzQkFBc0Isd0JBQXdCLGdDQUFnQyxhQUFhLGNBQWMsZUFBZSxnQkFBZ0IsbUJBQW1CLHlCQUF5QixLQUFLLHNCQUFzQixtQkFBbUIsS0FBSyx5Q0FBeUMsb0JBQW9CLEtBQUssVUFBVSwwQkFBMEIsS0FBSyxhQUFhLHlCQUF5QixLQUFLLGFBQWEsRzs7Ozs7OztBQ0R0a1UsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMxREEsMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcscUNBQXFDLHFCQUFxQixHQUFHLFVBQVUsMEVBQTBFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxpTkFBaU4sd0RBQXdELGlSQUFpUiwwTkFBME4sMkJBQTJCLG15Q0FBbXlDLG1IQUFtSCx3REFBd0QsMFRBQTBULDJyQkFBMnJCLFdBQVcsZ1FBQWdRLFdBQVcsd0xBQXdMLFVBQVUsd3VCQUF3dUIseUlBQXlJLGFBQWEsaWZBQWlmLHlJQUF5SSxhQUFhLDJhQUEyYSxhQUFhLCthQUErYSw0REFBNEQsb0JBQW9CLHFTQUFxUyw4U0FBOFMsRUFBRSw0QkFBNEIsaUJBQWlCLFdBQVcsdUJBQXVCLGtDQUFrQyxvRkFBb0YsOEVBQThFLGdDQUFnQyw4RUFBOEUsZUFBZSxRQUFRLDJCQUEyQixrREFBa0QsYUFBYSx5Q0FBeUMsbURBQW1ELCtDQUErQyxtREFBbUQsaURBQWlELCtCQUErQiw2S0FBNkssMENBQTBDLGlDQUFpQyxpQ0FBaUMsYUFBYSxnQ0FBZ0Msa0NBQWtDLHNGQUFzRixtQ0FBbUMsb0NBQW9DLG9DQUFvQyw2Q0FBNkMsMEJBQTBCLDJCQUEyQixPQUFPLGtHQUFrRyxzREFBc0QsZUFBZSxFQUFFLHFHQUFxRywwR0FBMEcsK0RBQStELGtFQUFrRSxpQkFBaUIsbUdBQW1HLHNEQUFzRCxlQUFlLEVBQUUsMkVBQTJFLGdDQUFnQyxnQ0FBZ0MsZUFBZSxzREFBc0QsK0ZBQStGLHFEQUFxRCxxQ0FBcUMsZUFBZSx3REFBd0QsNEJBQTRCLGdDQUFnQyxPQUFPLG1HQUFtRyxpQkFBaUIscUNBQXFDLGVBQWUsb0VBQW9FLDhHQUE4Ryw0Q0FBNEMsNEZBQTRGLDBHQUEwRyw0RkFBNEYsMERBQTBELHNFQUFzRSw2RUFBNkUsMEVBQTBFLHFFQUFxRSwyREFBMkQsNkNBQTZDLG9EQUFvRCw4R0FBOEcsc0VBQXNFLDZEQUE2RCx5QkFBeUIseUNBQXlDLG1CQUFtQixtQ0FBbUMsNkNBQTZDLHdHQUF3Ryx5Q0FBeUMsbUJBQW1CLEVBQUUsZUFBZSxtQkFBbUIsZ0VBQWdFLHFDQUFxQyxlQUFlLHVCQUF1QixpREFBaUQsb0hBQW9ILHdEQUF3RCw0REFBNEQsaURBQWlELHFDQUFxQyxhQUFhLE9BQU8saUdBQWlHLHNEQUFzRCxtQkFBbUIsRUFBRSxhQUFhLDREQUE0RCx5Q0FBeUMsaUJBQWlCLE9BQU8sbUdBQW1HLHNEQUFzRCxtQkFBbUIsRUFBRSxpQkFBaUIsV0FBVyxPQUFPLG1DQUFtQyxtQ0FBbUMsV0FBVyxzQ0FBc0Msa0hBQWtILGVBQWUsUUFBUSxTQUFTLGdDQUFnQyx1Q0FBdUMsdUJBQXVCLDJEQUEyRCxhQUFhLDhCQUE4Qix5REFBeUQsa0VBQWtFLG9FQUFvRSxzQ0FBc0MsOEdBQThHLG9HQUFvRyxvSkFBb0osRUFBRSx3SEFBd0gsbUJBQW1CLFFBQVEsYUFBYSxFQUFFLFNBQVMsaUNBQWlDLGtEQUFrRCw0SEFBNEgsRUFBRSwwQkFBMEIscUVBQXFFLHlDQUF5Qyx3RUFBd0UsNkNBQTZDLHVEQUF1RCxxRUFBcUUsd0NBQXdDLHlDQUF5QyxtQ0FBbUMsaUJBQWlCLGVBQWUsRUFBRSxhQUFhLE9BQU8sMkJBQTJCLGFBQWEsV0FBVyxFQUFFLGFBQWEsNEJBQTRCLGdFQUFnRSxtQ0FBbUMsdURBQXVELDBKQUEwSixFQUFFLFdBQVcsUUFBUSxTQUFTLHdCQUF3Qiw0Q0FBNEMsNENBQTRDLGtJQUFrSSxtR0FBbUcscUZBQXFGLFdBQVcsT0FBTyxzREFBc0QsbUhBQW1ILGtGQUFrRixvRUFBb0UsV0FBVyxVQUFVLFdBQVcsd0JBQXdCLG9DQUFvQyxXQUFXLHdCQUF3QixvQ0FBb0MsV0FBVyxPQUFPLHVDQUF1QywwQkFBMEIsS0FBSyxhQUFhLHlCQUF5QixLQUFLLHNCQUFzQix1QkFBdUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEaDBmLDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsMkVBQTJFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyw4aEJBQThoQiwwV0FBMFcsbXdCQUFtd0IsdWFBQXVhLHlwQ0FBeXBDLFdBQVcscW1EQUFxbUQsOEhBQThILHNCQUFzQixxYUFBcWEsOEhBQThILHNCQUFzQixrekJBQWt6Qiw0REFBNEQsb0JBQW9CLHNMQUFzTCxXQUFXLGtCQUFrQixTQUFTLHFCQUFxQixzQ0FBc0MscURBQXFELG9EQUFvRCwwREFBMEQsdURBQXVELHlEQUF5RCwwREFBMEQsd0RBQXdELHNEQUFzRCwyREFBMkQsMERBQTBELDREQUE0RCxhQUFhLG9DQUFvQyxzQ0FBc0MsdUZBQXVGLG9EQUFvRCxhQUFhLEVBQUUsc0NBQXNDLHVGQUF1RixvREFBb0QsYUFBYSxFQUFFLHFEQUFxRCxxRUFBcUUsK0dBQStHLHFEQUFxRCxlQUFlLHlFQUF5RSxrRUFBa0UsZUFBZSxtQkFBbUIsc0hBQXNILDRDQUE0QywrQ0FBK0MsK0RBQStELHNFQUFzRSx3RUFBd0UsbUVBQW1FLHlEQUF5RCwyQ0FBMkMsa0RBQWtELDRIQUE0SCwrREFBK0Qsd0RBQXdELHlCQUF5QixtQkFBbUIsbUNBQW1DLDZDQUE2Qyx3R0FBd0csbUJBQW1CLEVBQUUsaUJBQWlCLFNBQVMseUJBQXlCLHNIQUFzSCxtSUFBbUksd0ZBQXdGLHNHQUFzRyxrREFBa0QsZUFBZSxFQUFFLG9IQUFvSCxrREFBa0QsZUFBZSxFQUFFLGtDQUFrQyx3R0FBd0csOEZBQThGLDRIQUE0SCxFQUFFLDJIQUEySCxnSEFBZ0gsYUFBYSxRQUFRLFdBQVcsOEJBQThCLGdFQUFnRSxtQ0FBbUMsdURBQXVELDBKQUEwSixFQUFFLFdBQVcsUUFBUSxTQUFTLFlBQVksd0JBQXdCLGFBQWEsd0JBQXdCLG9FQUFvRSxxUUFBcVEsZ0RBQWdELHdFQUF3RSwwQ0FBMEMsNENBQTRDLDhDQUE4Qyw0REFBNEQsYUFBYSxtREFBbUQsc0RBQXNELHdDQUF3QywwQ0FBMEMsOENBQThDLDREQUE0RCxhQUFhLGFBQWEsV0FBVyxPQUFPLHVDQUF1QywwQkFBMEIsS0FBSyxhQUFhLHlCQUF5QixLQUFLLGFBQWEsRzs7Ozs7OztBQ0RyMlksZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM5Q0EsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBa0U7QUFDMUY7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBZ087QUFDMU87QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBeUw7QUFDbk07QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7OztBQzFCQTtBQUNBLG1CQUFPLENBQUMsR0FBbVI7O0FBRTNSLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTZPO0FBQ3ZQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXlNO0FBQ25OO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7QUM5QkE7QUFDQSxtQkFBTyxDQUFDLEdBQThROztBQUV0UixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUF3TztBQUNsUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFvTTtBQUM5TTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7O0FDOUJBO0FBQ0EsbUJBQU8sQ0FBQyxHQUErUTs7QUFFdlIsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBeU87QUFDblA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBcU07QUFDL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDL0JBLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXFPO0FBQy9PO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQWlNO0FBQzNNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFxTztBQUMvTztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFpTTtBQUMzTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN2SUEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSx3Q0FBd0MsUUFBUTtBQUNoRDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0Esd0NBQXdDLFFBQVE7QUFDaEQ7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUNsTEEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDdkNBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN2VEEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN4REEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRyw4QkFBOEI7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM5WEE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBZ1Q7QUFDdFUsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7O0FDcEJBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEdBQTJTO0FBQ2pVLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxHQUE0UztBQUNsVSw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLENBQXlFO0FBQzlGO0FBQ0EsR0FBRyxLQUFVO0FBQ2I7QUFDQTtBQUNBLDRKQUE0SixvRUFBb0U7QUFDaE8scUtBQXFLLG9FQUFvRTtBQUN6TztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7Ozs7Ozs7Ozs7OztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixpQkFBaUI7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLHdCQUF3QjtBQUMzRCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSEE7QUFDQTtBQUNBLGdDQURBO0FBRUE7QUFGQTtBQURBLEciLCJmaWxlIjoianMvdXNlcnByb2ZpbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ3MSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgNDJjZTBjNmM2ZTJiM2UyMjc4MzUiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSIsIjx0ZW1wbGF0ZT5cblx0XG5cdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCIgQHN1Ym1pdC5wcmV2ZW50PVwiYWRkTXVsdGlwbGVRdWlja1JlcG9ydFwiPlxuXG5cdFx0PGRpdiBjbGFzcz1cImlvcy1zY3JvbGxcIj5cblx0XHRcdDxkaXYgdi1mb3I9XCIoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgaW4gZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5sZW5ndGhcIj5cblxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwicGFuZWwgcGFuZWwtZGVmYXVsdFwiPlxuXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWhlYWRpbmdcIj5cblx0ICAgICAgICAgICAgICAgICAgICB7eyBkaXN0aW5jdENsaWVudFNlcnZpY2VzW2luZGV4XSB9fVxuXHQgICAgICAgICAgICAgICAgPC9kaXY+XG5cdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGFuZWwtYm9keVwiPlxuXHQgICAgICAgICAgICAgICAgICAgIFxuXHQgICAgICAgICAgICAgICAgXHQ8bXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0gOmtleT1cImluZGV4XCIgOnJlZj1cIidmb3JtLScgKyBpbmRleFwiIDpkb2NzPVwiZG9jc1wiIDpzZXJ2aWNlZG9jcz1cInNlcnZpY2VEb2NzXCIgOmluZGV4PVwiaW5kZXhcIj48L211bHRpcGxlLXF1aWNrLXJlcG9ydC1mb3JtPlxuXG5cdCAgICAgICAgICAgICAgICA8L2Rpdj4gPCEtLSBwYW5lbC1ib2R5IC0tPlxuXHQgICAgICAgICAgICA8L2Rpdj4gPCEtLSBwYW5lbCAtLT5cblxuXHRcdFx0PC9kaXY+IDwhLS0gbG9vcCAtLT5cblx0XHQ8L2Rpdj4gPCEtLSBpb3Mtc2Nyb2xsIC0tPlxuXHRcdFxuXHRcdDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIj5DcmVhdGUgUmVwb3J0PC9idXR0b24+XG5cdCAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cblx0PC9mb3JtPlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG5cdGV4cG9ydCBkZWZhdWx0IHtcblxuXHRcdHByb3BzOiBbJ2dyb3VwcmVwb3J0JywgJ2dyb3VwaWQnLCAnY2xpZW50c2lkJywgJ2NsaWVudHNlcnZpY2VzJywgJ3RyYWNraW5ncycsICdpc3dyaXRlcmVwb3J0cGFnZSddLFxuXG5cdFx0ZGF0YSgpIHtcblx0XHRcdHJldHVybiB7XG5cdFx0XG5cdFx0XHRcdGRvY3M6IFtdLFxuXHRcdFx0XHRzZXJ2aWNlRG9jczogW10sXG5cbiAgICAgICAgXHRcdG11bHRpcGxlUXVpY2tSZXBvcnRGb3JtIDogbmV3IEZvcm0oe1xuICAgICAgICBcdFx0XHRkaXN0aW5jdENsaWVudFNlcnZpY2VzOltdLFxuXG4gICAgICAgIFx0XHRcdGdyb3VwUmVwb3J0OiBudWxsLFxuICAgICAgICBcdFx0XHRncm91cElkOiBudWxsLFxuXG4gICAgICAgIFx0XHRcdGNsaWVudHNJZDogW10sXG4gICAgICAgIFx0XHRcdGNsaWVudFNlcnZpY2VzSWQ6IFtdLFxuICAgICAgICBcdFx0XHR0cmFja2luZ3M6IFtdLFxuXG4gICAgICAgIFx0XHRcdGFjdGlvbklkOiBbXSxcblxuICAgICAgICBcdFx0XHRjYXRlZ29yeUlkOiBbXSxcbiAgICAgICAgXHRcdFx0Y2F0ZWdvcnlOYW1lOiBbXSxcblxuICAgICAgICBcdFx0XHRkYXRlMTogW10sXG4gICAgICAgIFx0XHRcdGRhdGUyOiBbXSxcbiAgICAgICAgXHRcdFx0ZGF0ZTM6IFtdLFxuICAgICAgICBcdFx0XHRkYXRlVGltZUFycmF5OiBbXSxcblxuICAgICAgICBcdFx0XHRleHRyYURldGFpbHM6IFtdXG4gICAgICAgIFx0XHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxuXG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdG1ldGhvZHM6IHtcblxuXHRcdFx0ZmV0Y2hEb2NzKGlkKSB7XG5cdFx0XHRcdHZhciBpZHMgPSAnJztcblx0XHRcdFx0JC5lYWNoKGlkLCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cdFx0XHRcdCAgICBpZHMgKz0gaWRbaV0uZG9jc19uZWVkZWQgKyAnLCcgKyBpZFtpXS5kb2NzX29wdGlvbmFsICsgJywnO1xuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHR2YXIgdW5pcXVlTGlzdD1pZHMuc3BsaXQoJywnKS5maWx0ZXIoZnVuY3Rpb24oYWxsSXRlbXMsaSxhKXtcblx0XHRcdFx0ICAgIHJldHVybiBpPT1hLmluZGV4T2YoYWxsSXRlbXMpO1xuXHRcdFx0XHR9KS5qb2luKCcsJyk7XG5cdCAgICAgICAgXHRcblx0XHRcdCBcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJyt1bmlxdWVMaXN0Kycvc2VydmljZS1kb2NzJylcblx0XHRcdCBcdCAgLnRoZW4ocmVzdWx0ID0+IHtcblx0XHQgICAgICAgICAgICB0aGlzLmRvY3MgPSByZXN1bHQuZGF0YTtcblx0XHQgICAgICAgIH0pO1xuXG5cdCAgICAgICAgXHRzZXRUaW1lb3V0KCgpID0+IHtcblx0ICAgICAgICBcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLWRvY3MnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1x0XG5cdCAgICAgICAgXHR9LCA1MDAwKTtcblx0XHRcdH0sXG5cblx0XHRcdGdldFNlcnZpY2VEb2NzKCkge1xuICAgICAgICBcdFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC9zZXJ2aWNlLWRvY3MtaW5kZXgnKVxuXHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdCAgICAgICAgICAgIHRoaXMuc2VydmljZURvY3MgPSByZXN1bHQuZGF0YTtcblx0XHQgICAgICAgIH0pO1xuICAgICAgICBcdH0sXG5cblx0XHRcdHJlc2V0TXVsdGlwbGVRdWlja1JlcG9ydEZvcm0oKSB7XG5cdFx0XHRcdC8vIE11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZVxuXHRcdFx0XHRcdHRoaXMuZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5mb3JFYWNoKChkaXN0aW5jdENsaWVudFNlcnZpY2UsIGluZGV4KSA9PiB7XG5cdFx0XHRcdFx0XHRsZXQgcmVmID0gJ2Zvcm0tJyArIGluZGV4O1xuXG5cdFx0XHRcdFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0gPSB7XG5cdFx0ICAgICAgICBcdFx0XHRhY3Rpb25JZDogMSxcblxuXHRcdCAgICAgICAgXHRcdFx0Y2F0ZWdvcnlJZDogbnVsbCxcblx0XHQgICAgICAgIFx0XHRcdGNhdGVnb3J5TmFtZTogbnVsbCxcblxuXHRcdCAgICAgICAgXHRcdFx0ZGF0ZTE6IG51bGwsXG5cdFx0ICAgICAgICBcdFx0XHRkYXRlMjogbnVsbCxcblx0XHQgICAgICAgIFx0XHRcdGRhdGUzOiBudWxsLFxuXHRcdCAgICAgICAgXHRcdFx0ZGF0ZVRpbWVBcnJheTogW10sXG5cblx0XHQgICAgICAgIFx0XHRcdGV4dHJhRGV0YWlsczogW11cblx0XHQgICAgICAgIFx0XHR9XG5cblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTIudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTMudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTQudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTUudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTYudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTcudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTgudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTkudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTEwLnZhbHVlID0gJyc7XG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxMS52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTIudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTEzLnZhbHVlID0gJyc7XG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxNC52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTUudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTE2LnZhbHVlID0gJyc7XG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmNhdGVnb3J5TmFtZS52YWx1ZSA9ICcnO1xuXG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLmN1c3RvbUNhdGVnb3J5ID0gZmFsc2U7XG5cdFx0XHRcdFx0fSk7XG5cbiAgICAgICAgXHRcdC8vIE11bHRpcGxlUXVpY2tSZXBvcnQudnVlXG5cdCAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0gPSBuZXcgRm9ybSh7XG5cdCAgICAgICAgXHRcdFx0ZGlzdGluY3RDbGllbnRTZXJ2aWNlczpbXSxcblxuXHQgICAgICAgIFx0XHRcdGFjdGlvbklkOiBbXSxcblxuXHQgICAgICAgIFx0XHRcdGNhdGVnb3J5SWQ6IFtdLFxuXHQgICAgICAgIFx0XHRcdGNhdGVnb3J5TmFtZTogW10sXG5cblx0ICAgICAgICBcdFx0XHRkYXRlMTogW10sXG5cdCAgICAgICAgXHRcdFx0ZGF0ZTI6IFtdLFxuXHQgICAgICAgIFx0XHRcdGRhdGUzOiBbXSxcblx0ICAgICAgICBcdFx0XHRkYXRlVGltZUFycmF5OiBbXSxcblxuXHQgICAgICAgIFx0XHRcdGV4dHJhRGV0YWlsczogW11cblx0ICAgICAgICBcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSk7XG5cdFx0XHR9LFxuXG5cdFx0XHR2YWxpZGF0ZSgpIHtcblx0XHRcdFx0dmFyIGlzVmFsaWQgPSB0cnVlO1xuXG5cdFx0XHRcdHRoaXMuZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5mb3JFYWNoKChkaXN0aW5jdENsaWVudFNlcnZpY2UsIGluZGV4KSA9PiB7XG5cdFx0XHRcdFx0bGV0IHJlZiA9ICdmb3JtLScgKyBpbmRleDtcblxuXHRcdFx0XHRcdGlmKCAhIHRoaXMuJHJlZnNbcmVmXVswXS52YWxpZGF0ZShkaXN0aW5jdENsaWVudFNlcnZpY2UpICkge1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0cmV0dXJuIGlzVmFsaWQ7XG5cdFx0XHR9LFxuXG5cdFx0XHRhZGRNdWx0aXBsZVF1aWNrUmVwb3J0KCkge1xuXHRcdFx0XHRpZih0aGlzLnZhbGlkYXRlKCkpIHtcblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmdyb3VwUmVwb3J0ID0gdGhpcy5ncm91cHJlcG9ydDtcblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmdyb3VwSWQgPSB0aGlzLmdyb3VwaWQ7XG5cdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5jbGllbnRzSWQgPSB0aGlzLmNsaWVudHNpZDtcblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmNsaWVudFNlcnZpY2VzSWQgPSB0aGlzLmNsaWVudHNlcnZpY2VzO1xuXHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udHJhY2tpbmdzID0gdGhpcy50cmFja2luZ3M7XG5cdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kaXN0aW5jdENsaWVudFNlcnZpY2VzID0gdGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzO1xuXG5cdFx0XHRcdFx0dGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzLmZvckVhY2goKGRpc3RpbmN0Q2xpZW50U2VydmljZSwgaW5kZXgpID0+IHtcblx0XHRcdFx0XHRcdGxldCByZWYgPSAnZm9ybS0nICsgaW5kZXg7XG5cblx0XHRcdFx0XHRcdGxldCBfYWN0aW9uSWQgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQ7XG5cdFx0XHRcdFx0XHRsZXQgX2NhdGVnb3J5SWQgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZDtcblx0XHRcdFx0XHRcdGxldCBfY2F0ZWdvcnlOYW1lID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZTtcblx0XHRcdFx0XHRcdGxldCBfZGF0ZTEgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTE7XG5cdFx0XHRcdFx0XHRsZXQgX2RhdGUyID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyO1xuXHRcdFx0XHRcdFx0bGV0IF9kYXRlMyA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMztcblx0XHRcdFx0XHRcdGxldCBfZGF0ZVRpbWVBcnJheSA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlVGltZUFycmF5O1xuXHRcdFx0XHRcdFx0bGV0IF9leHRyYURldGFpbHMgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzO1xuXG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmFjdGlvbklkLnB1c2goX2FjdGlvbklkKTtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2F0ZWdvcnlJZC5wdXNoKF9jYXRlZ29yeUlkKTtcblx0ICAgICAgICBcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmNhdGVnb3J5TmFtZS5wdXNoKF9jYXRlZ29yeU5hbWUpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTEucHVzaChfZGF0ZTEpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTIucHVzaChfZGF0ZTIpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTMucHVzaChfZGF0ZTMpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZVRpbWVBcnJheS5wdXNoKF9kYXRlVGltZUFycmF5KTtcblx0ICAgICAgICBcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmV4dHJhRGV0YWlscy5wdXNoKF9leHRyYURldGFpbHMpO1xuXHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3Zpc2EvcXVpY2stcmVwb3J0Jylcblx0XHRcdCAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdCAgICAgICAgICAgIFx0aWYocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0ICAgICAgICAgICAgXHRcdHRoaXMucmVzZXRNdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSgpO1xuXHRcdFx0ICAgICAgICAgICAgXHRcdFxuXHRcdFx0ICAgICAgICAgICAgXHRcdCQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdFx0ICAgICAgICAgICAgXHRcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xuXG5cdFx0XHQgICAgICAgICAgICBcdFx0aWYodGhpcy5pc3dyaXRlcmVwb3J0cGFnZSkge1xuXHRcdFx0ICAgICAgICAgICAgXHRcdFx0JCgnI2FkZC1xdWljay1yZXBvcnQtbW9kYWwnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0XHRcdFx0XHRcdCAgXHR3aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvdmlzYS9yZXBvcnQvd3JpdGUtcmVwb3J0Jztcblx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0ICAgICAgICAgICAgXHRcdH0gZWxzZSB7XG5cdFx0XHQgICAgICAgICAgICBcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0ICAgICAgICAgICAgXHRcdFx0bG9jYXRpb24ucmVsb2FkKCk7XG5cdFx0XHRcdCAgICAgICAgICAgIFx0XHR9LCAxMDAwKTtcblx0XHRcdCAgICAgICAgICAgIFx0XHR9XG5cdFx0XHQgICAgICAgICAgICBcdH1cblx0XHRcdCAgICAgICAgICAgIH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHR9LFxuXG5cdFx0Y3JlYXRlZCgpIHtcblx0XHRcdHRoaXMuZ2V0U2VydmljZURvY3MoKTtcblx0XHR9LFxuXG5cdFx0Y29tcHV0ZWQ6IHtcblx0XHRcdF9pc1dyaXRlUmVwb3J0UGFnZSgpIHtcblx0XHQgICAgICBcdHJldHVybiB0aGlzLmlzd3JpdGVyZXBvcnRwYWdlIHx8IGZhbHNlO1xuXHRcdCAgICB9LFxuXG5cdFx0XHRkaXN0aW5jdENsaWVudFNlcnZpY2VzKCkge1xuXHRcdFx0XHRsZXQgZGlzdGluY3RDbGllbnRTZXJ2aWNlcyA9IFtdO1xuXG5cdFx0XHRcdHRoaXMuY2xpZW50c2VydmljZXMuZm9yRWFjaChjbGllbnRTZXJ2aWNlID0+IHtcblx0XHRcdFx0XHRpZihkaXN0aW5jdENsaWVudFNlcnZpY2VzLmluZGV4T2YoY2xpZW50U2VydmljZS5kZXRhaWwpID09IC0xKSB7XG5cdFx0XHRcdFx0XHRkaXN0aW5jdENsaWVudFNlcnZpY2VzLnB1c2goY2xpZW50U2VydmljZS5kZXRhaWwpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0cmV0dXJuIGRpc3RpbmN0Q2xpZW50U2VydmljZXM7XG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdGNvbXBvbmVudHM6IHtcblx0ICAgICAgICAnbXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0nOiByZXF1aXJlKCcuL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZScpXG5cdCAgICB9XG5cblx0fVxuXG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE11bHRpcGxlUXVpY2tSZXBvcnQudnVlPzM1Zjk5NmI5IiwiPHRlbXBsYXRlPlxuXHRcblx0PGRpdj5cblx0XHRcblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBBY3Rpb246IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwiYWN0aW9uXCIgdi1tb2RlbD1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkXCIgQGNoYW5nZT1cImdldENhdGVnb3JpZXNcIj5cblx0XHQgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwiYWN0aW9uIGluIGFjdGlvbnNcIiA6dmFsdWU9XCJhY3Rpb24uaWRcIj5cblx0XHQgICAgICAgICAgIFx0XHR7eyBhY3Rpb24ubmFtZSB9fVxuXHRcdCAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdCAgICAgICAgPC9zZWxlY3Q+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdDwvZGl2PiA8IS0tIGZvcm0tZ3JvdXAgLS0+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMiB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAzIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDggfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNlwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBDYXRlZ29yeTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0ICAgIDwvbGFiZWw+XG5cblx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICAgICAgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJjYXRlZ29yeVwiIHYtbW9kZWw9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkXCIgQGNoYW5nZT1cImNoYW5nZUNhdGVnb3J5SGFuZGxlclwiIHYtc2hvdz1cIiFjdXN0b21DYXRlZ29yeVwiPlxuXHRcdCAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XCJjYXRlZ29yeSBpbiBjYXRlZ29yaWVzXCIgOnZhbHVlPVwiY2F0ZWdvcnkuaWRcIj5cblx0XHQgICAgICAgICAgICBcdHt7IGNhdGVnb3J5Lm5hbWUgfX1cblx0XHQgICAgICAgICAgICA8L29wdGlvbj5cblx0XHQgICAgICAgIDwvc2VsZWN0PlxuXG5cdFx0ICAgICAgICA8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDlcIj5cblx0XHQgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiByZWY9XCJjYXRlZ29yeU5hbWVcIiBwbGFjZWhvbGRlcj1cIkVudGVyIGNhdGVnb3J5XCIgdi1tb2RlbD1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZVwiIHYtc2hvdz1cImN1c3RvbUNhdGVnb3J5XCI+XG5cblx0XHQgICAgICAgIFx0PGRpdiBzdHlsZT1cImhlaWdodDoxMHB4OyBjbGVhcjpib3RoO1wiPjwvZGl2PlxuXG5cdFx0ICAgICAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zbSBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIgQGNsaWNrPVwiY3VzdG9tQ2F0ZWdvcnkgPSAhY3VzdG9tQ2F0ZWdvcnk7IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPSBudWxsXCI+XG5cdFx0XHQgICAgICAgIFx0PHNwYW4gdi1zaG93PVwiIWN1c3RvbUNhdGVnb3J5XCI+VXNlIEN1c3RvbSBDYXRlZ29yeTwvc3Bhbj5cblx0XHRcdCAgICAgICAgXHQ8c3BhbiB2LXNob3c9XCJjdXN0b21DYXRlZ29yeVwiPlVzZSBQcmVkZWZpbmVkIENhdGVnb3J5PC9zcGFuPlxuXHRcdFx0ICAgICAgICA8L2J1dHRvbj5cblx0XHQgICAgICAgIDwvZGl2PlxuXHRcdCAgICA8L2Rpdj5cblx0XHQ8L2Rpdj4gPCEtLSBmb3JtLWdyb3VwIC0tPlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxXCI+IDwhLS0gRmlsZWQgLS0+XG5cdFx0XHQ8IS0tIERhdGUgcGlja2VyIChkYXRlMSA6IGF1dG8gY2FwdHVyZSA6IHJlcXVpcmVkKSAtLT5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGUyXCIgcmVmPVwiZGF0ZTJcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDFcIj5cblx0XHRcdFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0U2NoZWR1bGVkIEhlYXJpbmcgRGF0ZSBhbmQgVGltZTpcblx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJyb3dcIiB2LWZvcj1cIihkYXRlVGltZUVsZW1lbnQsIGluZGV4KSBpbiBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlVGltZUFycmF5XCI+XG5cdFx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJjb2wtbWQtMTFcIj5cblx0XHRcdFx0XHQgICAgXHQ8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAgZGF0ZVwiIHN0eWxlPVwibWFyZ2luLWJvdHRvbTogMTBweDtcIj5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHQ8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJhcnJheVwiIG5hbWU9XCJkYXRlVGltZUFycmF5W11cIiB2LW1vZGVsPVwiZGF0ZVRpbWVFbGVtZW50LnZhbHVlXCIgcGxhY2Vob2xkZXI9XCJ5eXl5LW1tLWRkIGhoOm1tOnNzXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG5cdFx0ICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0ICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0ICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEgdGV4dC1jZW50ZXJcIj5cblx0ICAgICAgICAgICAgICAgICAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kYW5nZXIgYnRuLXNtXCIgQGNsaWNrPVwicmVtb3ZlRGF0ZVRpbWVQaWNrZXJBcnJheShpbmRleClcIiB2LXNob3c9XCJpbmRleCAhPSAwXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdDxpIGNsYXNzPVwiZmEgZmEtdGltZXNcIj48L2k+XG5cdCAgICAgICAgICAgICAgICAgICAgXHQ8L2J1dHRvbj5cblx0ICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT1cImhlaWdodDoxMHB4O2NsZWFyOmJvdGg7XCI+PC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLXNtIHB1bGwtcmlnaHRcIiBAY2xpY2s9XCJhZGREYXRlVGltZVBpY2tlckFycmF5XCI+XG4gICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1wbHVzXCI+PC9pPiBBZGRcbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDYgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTAgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDE1XCI+XG5cdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdFNjaGVkdWxlZCBBcHBvaW50bWVudCBEYXRlIGFuZCBUaW1lOlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIgaWQ9XCJkYXRlM1wiIHJlZj1cImRhdGUzXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDJcIj4gPCEtLSBSZWxlYXNlZCAtLT5cblxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAzXCI+IDwhLS0gUGFpZCBPUFMgLS0+XG5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNFwiPiA8IS0tIFVuYWJsZSB0byBmaWxlIG5lZWQgYWRkaXRpb25hbCByZXF1aXJlbWVudHMgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdERvY3VtZW50czogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgICAgICAgICA8c2VsZWN0IDppZD1cIidjaG9zZW4tJyArIGluZGV4XCIgZGF0YS1wbGFjZWhvbGRlcj1cIlNlbGVjdCBEb2NzXCIgY2xhc3M9XCJjaG9zZW4tc2VsZWN0LWZvci1kb2NzLTJcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjM1MHB4O1wiIHRhYmluZGV4PVwiNFwiPlxuXHRcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwic2VydmljZWRvYyBpbiBzZXJ2aWNlZG9jc1wiIDp2YWx1ZT1cInNlcnZpY2Vkb2MudGl0bGVcIj5cblx0XHQgICAgICAgICAgICAgICAgXHR7eyAoc2VydmljZWRvYy50aXRsZSkudHJpbSgpIH19XG5cdFx0ICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdCAgICAgICAgICAgIDwvc2VsZWN0PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDZcIj4gPCEtLSBEZWFkbGluZSBvZiBTdWJtaXNzaW9uIC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRTdWJtaXNzaW9uIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGU0XCIgcmVmPVwiZGF0ZTRcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRDb25zZXF1ZW5jZSBvZiBub24gc3VibWlzc2lvbjpcblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgIFx0PGRpdj5cblx0XHRcdCAgICBcdFx0PGxhYmVsPlxuXHRcdFx0ICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiB2LW1vZGVsPVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCIgdmFsdWU9XCJBZGRpdGlvbmFsIGNvc3QgbWF5YmUgY2hhcmdlZC5cIj5cblx0XHRcdCAgICBcdFx0XHRBZGRpdGlvbmFsIGNvc3QgbWF5YmUgY2hhcmdlZC5cblx0XHRcdCAgICBcdFx0PC9sYWJlbD5cblx0XHRcdCAgICBcdDwvZGl2PlxuXHRcdFx0ICAgIFx0PGRpdj5cblx0XHRcdCAgICBcdFx0PGxhYmVsPlxuXHRcdFx0ICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiB2LW1vZGVsPVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCIgdmFsdWU9XCJBcHBsaWNhdGlvbiBtYXliZSBmb3JmZWl0ZWQgYW5kIGhhcyB0byByZS1hcHBseS5cIj5cblx0XHRcdCAgICBcdFx0XHRBcHBsaWNhdGlvbiBtYXliZSBmb3JmZWl0ZWQgYW5kIGhhcyB0byByZS1hcHBseS5cblx0XHRcdCAgICBcdFx0PC9sYWJlbD5cblx0XHRcdCAgICBcdDwvZGl2PlxuXHRcdFx0ICAgIFx0PGRpdj5cblx0XHRcdCAgICBcdFx0PGxhYmVsPlxuXHRcdFx0ICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiB2LW1vZGVsPVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCIgdmFsdWU9XCJJbml0aWFsIGRlcG9zaXQgbWF5YmUgZm9yZmVpdGVkLlwiPlxuXHRcdFx0ICAgIFx0XHRcdEluaXRpYWwgZGVwb3NpdCBtYXliZSBmb3JmZWl0ZWQuXG5cdFx0XHQgICAgXHRcdDwvbGFiZWw+XG5cdFx0XHQgICAgXHQ8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA3XCI+IDwhLS0gRm9yIEltcGxlbWVudGF0aW9uIC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFc3RpbWF0ZWQgVGltZSBvZiBGaW5pc2hpbmc6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIgaWQ9XCJkYXRlNVwiIHJlZj1cImRhdGU1XCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDhcIj4gPCEtLSBVbmFibGUgdG8gZmlsZSBkdWUgdG8gYmFkIHdlYXRoZXIgLS0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdEFmZmVjdGVkIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgXHQ8ZGl2IGNsYXNzPVwiaW5wdXQtZGF0ZXJhbmdlIGlucHV0LWdyb3VwXCI+XG5cdFx0ICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaW5wdXQtc20gZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGU2XCIgcmVmPVwiZGF0ZTZcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPlxuXHRcdCAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+dG88L3NwYW4+XG5cdFx0ICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaW5wdXQtc20gZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGU3XCIgcmVmPVwiZGF0ZTdcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPlxuXHRcdCAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0VGFyZ2V0IEZpbGxpbmcgRGF0ZTpcblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAgZGF0ZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgXHQ8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIgaWQ9XCJkYXRlOFwiIHJlZj1cImRhdGU4XCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDlcIj4gPCEtLSBVbmFibGUgdG8gZmluaXNoIGR1ZSB0byBzdXNwZW5zaW9uIG9mIHdvcmsgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdERhdGUgUmFuZ2U6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1kYXRlcmFuZ2UgaW5wdXQtZ3JvdXBcIj5cblx0XHQgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTlcIiByZWY9XCJkYXRlOVwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG5cdFx0ICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj50bzwvc3Bhbj5cblx0XHQgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTEwXCIgcmVmPVwiZGF0ZTEwXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgLz5cblx0XHQgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMFwiPiA8IS0tIFByb2Nlc3NpbmcgUGVyaW9kIEV4dGVuc2lvbiAtLT5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0RXh0ZW5kZWQgcGVyaW9kIGZvciBwcm9jZXNzaW5nOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAgZGF0ZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgXHQ8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIgaWQ9XCJkYXRlMTFcIiByZWY9XCJkYXRlMTFcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMVwiPiA8IS0tIEZvciBwYXltZW50IC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGUxMlwiIHJlZj1cImRhdGUxMlwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEyXCI+IDwhLS0gUGlja2VkIHVwIGZyb20gY2xpZW50IC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHREYXRlIGFuZCBUaW1lIFBpY2sgVXA6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIgaWQ9XCJkYXRlMTNcIiByZWY9XCJkYXRlMTNcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTNcIj4gPCEtLSBGb3IgZGVsaXZlcnkgbm93IC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHREYXRlIGFuZCBUaW1lIERlbGl2ZXI6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIgaWQ9XCJkYXRlMTRcIiByZWY9XCJkYXRlMTRcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTRcIj4gPCEtLSBEZWxpdmVyZWQgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTI4XCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0Q29tcGFueSBDb3VyaWVyOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJjb21wYW55X2NvdXJpZXJcIiByZWY9XCJjb21wYW55X2NvdXJpZXJcIj5cblx0XHRcdCAgICAgICAgXHQ8b3B0aW9uIHYtZm9yPVwiY29tcGFueUNvdXJpZXIgaW4gY29tcGFueUNvdXJpZXJzXCIgOnZhbHVlPVwiY29tcGFueUNvdXJpZXIuYWRkcmVzcy5jb250YWN0X251bWJlclwiPlxuXHRcdFx0ICAgICAgICBcdFx0e3sgY29tcGFueUNvdXJpZXIuZmlyc3RfbmFtZSArICcgJyArIGNvbXBhbnlDb3VyaWVyLmxhc3RfbmFtZSB9fSBcblx0XHRcdCAgICAgICAgXHRcdCh7eyBjb21wYW55Q291cmllci5hZGRyZXNzLmNvbnRhY3RfbnVtYmVyIH19KVxuXHRcdFx0ICAgICAgICBcdDwvb3B0aW9uPlxuXHRcdFx0ICAgICAgICA8L3NlbGVjdD5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgIT0gMTI4XCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0VHJhY2tpbmcgTnVtYmVyOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIGlkPVwidHJhY2tpbmdfbnVtYmVyXCIgcmVmPVwidHJhY2tpbmdfbnVtYmVyXCI+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHREYXRlIGFuZCBUaW1lIERlbGl2ZXJlZDogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIiBpZD1cImRhdGUxNVwiIHJlZj1cImRhdGUxNVwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNVwiPiA8IS0tIEZvciBwcm9jZXNzIG9mIC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc2IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzcgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3OCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc5IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODAgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgyIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODNcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGUxNlwiIHJlZj1cImRhdGUxNlwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+IFxuXG5cdDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXHRcblx0ZXhwb3J0IGRlZmF1bHQge1xuXG5cdFx0cHJvcHM6IFsnaW5kZXgnLCAnZG9jcycsICdzZXJ2aWNlZG9jcyddLFxuXG5cdFx0ZGF0YSgpIHtcbiAgICAgICAgXHRyZXR1cm4ge1xuXG4gICAgICAgIFx0XHRhY3Rpb25zOiBbXSxcbiAgICAgICAgXHRcdGNhdGVnb3JpZXM6IFtdLFxuXG4gICAgICAgIFx0XHRjb21wYW55Q291cmllcnM6IFtdLFxuXG4gICAgICAgIFx0XHRjdXN0b21DYXRlZ29yeTogZmFsc2UsXG5cbiAgICAgICAgXHRcdG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtOiB7XG4gICAgICAgIFx0XHRcdGFjdGlvbklkOiAxLFxuXG4gICAgICAgIFx0XHRcdGNhdGVnb3J5SWQ6IG51bGwsXG4gICAgICAgIFx0XHRcdGNhdGVnb3J5TmFtZTogbnVsbCxcblxuICAgICAgICBcdFx0XHRkYXRlMTogbnVsbCxcbiAgICAgICAgXHRcdFx0ZGF0ZTI6IG51bGwsXG4gICAgICAgIFx0XHRcdGRhdGUzOiBudWxsLFxuICAgICAgICBcdFx0XHRkYXRlVGltZUFycmF5OiBbXSxcblxuICAgICAgICBcdFx0XHRleHRyYURldGFpbHM6IFtdXG4gICAgICAgIFx0XHR9XG5cbiAgICAgICAgXHR9XG4gICAgICAgIH0sXG5cbiAgICAgICAgbWV0aG9kczoge1xuXG4gICAgICAgIFx0Z2V0QWN0aW9ucygpIHtcbiAgICAgICAgXHRcdGF4aW9zLmdldCgnL3Zpc2EvYWN0aW9ucycpXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5hY3Rpb25zID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgICAgICBcdH0sXG5cbiAgICAgICAgXHRnZXRDb21wYW55Q291cmllcnMoKSB7XG4gICAgICAgIFx0XHRsZXQgcm9sZSA9ICdjb21wYW55LWNvdXJpZXInO1xuXG4gICAgICAgIFx0XHRheGlvcy5nZXQoJy92aXNhL3VzZXJzLWJ5LXJvbGUvJyArIHJvbGUpXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5jb21wYW55Q291cmllcnMgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGdldEN1cnJlbnREYXRlKCkge1xuICAgICAgICBcdFx0bGV0IHRvZGF5ID0gbmV3IERhdGUoKTtcblx0XHRcdFx0bGV0IGRkID0gdG9kYXkuZ2V0RGF0ZSgpO1xuXHRcdFx0XHRsZXQgbW0gPSB0b2RheS5nZXRNb250aCgpICsgMTtcblx0XHRcdFx0bGV0IHl5eXkgPSB0b2RheS5nZXRGdWxsWWVhcigpO1xuXG5cdFx0XHRcdGlmKGRkIDwgMTApIHtcblx0XHRcdFx0ICAgIGRkID0gJzAnICsgZGQ7XG5cdFx0XHRcdH0gXG5cblx0XHRcdFx0aWYobW0gPCAxMCkge1xuXHRcdFx0XHQgICAgbW0gPSAnMCcgKyBtbTtcblx0XHRcdFx0fSBcblxuXHRcdFx0XHRyZXR1cm4gbW0gKyAnLycgKyBkZCArICcvJyArIHl5eXk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGNoYW5nZUFjdGlvbkhhbmRsZXIoYWN0aW9uSWQpIHtcbiAgICAgICAgXHRcdC8vIFJlc2V0XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gbnVsbDtcbiAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTIgPSBudWxsO1xuICAgICAgICBcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMyA9IG51bGw7XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkgPSBbXTtcbiAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzID0gW107XG5cbiAgICAgICAgXHRcdC8vIEZvciBhdXRvIGNhcHR1cmUgZGF0ZVxuICAgICAgICBcdFx0aWYoYWN0aW9uSWQgPT0gMSB8fCBhY3Rpb25JZCA9PSAzIHx8IGFjdGlvbklkID09IDQgfHwgIGFjdGlvbklkID09IDcgfHwgYWN0aW9uSWQgPT0gMTEgfHwgYWN0aW9uSWQgPT0gMTUgfHwgYWN0aW9uSWQgPT0gMTYpIHtcbiAgICAgICAgXHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IHRoaXMuZ2V0Q3VycmVudERhdGUoKTtcbiAgICAgICAgXHRcdH1cbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0Z2V0Q2F0ZWdvcmllcygpIHtcbiAgICAgICAgXHRcdGxldCBhY3Rpb25JZCA9IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQ7XG5cbiAgICAgICAgXHRcdHRoaXMuY2hhbmdlQWN0aW9uSGFuZGxlcihhY3Rpb25JZCk7XG5cbiAgICAgICAgXHRcdHRoaXMuY2F0ZWdvcmllcyA9IFtdO1xuXG4gICAgICAgIFx0XHRheGlvcy5nZXQoJy92aXNhL2NhdGVnb3JpZXMtYnktYWN0aW9uLWlkJywge1xuICAgICAgICBcdFx0XHRcdHBhcmFtczoge1xuXHRcdFx0XHRcdCAgICBcdGFjdGlvbklkOiBhY3Rpb25JZFxuXHRcdFx0XHRcdCAgICB9XG4gICAgICAgIFx0XHRcdH0pXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5jYXRlZ29yaWVzID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgICAgICBcdH0sXG5cbiAgICAgICAgXHRjaGFuZ2VDYXRlZ29yeUhhbmRsZXIoKSB7XG4gICAgICAgIFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEgJiYgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEpIHtcbiAgICAgICAgXHRcdFx0dGhpcy5hZGREYXRlVGltZVBpY2tlckFycmF5KCk7XG4gICAgICAgIFx0XHR9XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGFkZERhdGVUaW1lUGlja2VyQXJyYXkoKSB7XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkucHVzaCh7dmFsdWU6bnVsbH0pO1xuXG4gICAgICAgIFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdCAgICAgICAgJCgnLmRhdGV0aW1lcGlja2VyYXJyYXknKS5pbnB1dG1hc2soXCJkYXRldGltZVwiLCB7XG5cdFx0ICAgICAgICAgICAgICAgIG1hc2s6IFwieS0yLTEgaDpzOnNcIiwgXG5cdFx0ICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcInl5eXktbW0tZGQgaGg6bW06c3NcIiwgXG5cdFx0ICAgICAgICAgICAgICAgIGxlYXBkYXk6IFwiLTAyLTI5XCIsIFxuXHRcdCAgICAgICAgICAgICAgICBzZXBhcmF0b3I6IFwiLVwiLCBcblx0XHQgICAgICAgICAgICAgICAgYWxpYXM6IFwieXl5eS1tbS1kZFwiXG5cdFx0ICAgICAgICAgICAgfSk7XG4gICAgICAgIFx0XHR9LCA1MDApO1xuICAgICAgICBcdH0sXG5cbiAgICAgICAgXHRyZW1vdmVEYXRlVGltZVBpY2tlckFycmF5KGluZGV4KSB7XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0dmFsaWRhdGUoZGlzdGluY3RDbGllbnRTZXJ2aWNlKSB7XG4gICAgICAgIFx0XHR2YXIgaXNWYWxpZCA9IHRydWU7XG5cbiAgICAgICAgXHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMSkge1xuXHRcdFx0XHRcdGxldCBkYXRlMiA9IHRoaXMuJHJlZnMuZGF0ZTIudmFsdWU7XG5cdFx0XHRcdFx0bGV0IGRhdGUzID0gKHRoaXMuJHJlZnMuZGF0ZTMudmFsdWUpID8gdGhpcy4kcmVmcy5kYXRlMy52YWx1ZSA6ICcnO1xuXG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZihkYXRlMiA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBlc3RpbWF0ZWQgcmVsZWFzaW5nIGRhdGUuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0bGV0IGRhdGVUaW1lQXJyYXkgPSBbXTtcblx0XHQgICAgICAgIFx0XHQkKCdpbnB1dFtuYW1lXj1cImRhdGVUaW1lQXJyYXlcIl0nKS5lYWNoKCBmdW5jdGlvbihlKSB7XG5cdFx0ICAgICAgICBcdFx0XHRpZih0aGlzLnZhbHVlICE9ICcnKSB7IGRhdGVUaW1lQXJyYXkucHVzaCh0aGlzLnZhbHVlKTsgfVxuXHRcdFx0XHRcdCAgICB9KTtcblxuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IGRhdGUyO1xuXHRcdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDYgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDcgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEwIHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNCB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTUpIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMyA9IGRhdGUzO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkgPSBkYXRlVGltZUFycmF5O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMiB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDMpIHtcblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA0KSB7XG5cdFx0XHRcdFx0bGV0IGRvY3MgPSBbXTtcblx0ICAgICAgICBcdFx0JCgnI2Nob3Nlbi0nICsgdGhpcy5pbmRleCArJyBvcHRpb246c2VsZWN0ZWQnKS5lYWNoKCBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdCAgICAgICAgZG9jcy5wdXNoKCQodGhpcykudmFsKCkpO1xuXHRcdFx0XHQgICAgfSk7XG5cblx0XHRcdFx0XHRpZihkb2NzLmxlbmd0aCA9PSAwKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgZG9jdW1lbnRzLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdCQoJyNjaG9zZW4tJyArIHRoaXMuaW5kZXgpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblxuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMgPSBkb2NzO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNikge1xuXHRcdFx0XHRcdGxldCBkYXRlNCA9IHRoaXMuJHJlZnMuZGF0ZTQudmFsdWU7XG5cblx0XHRcdFx0XHRpZihkYXRlNCA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBzdWJtaXNzaW9uIGRhdGUuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGU0O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNykge1xuXHRcdFx0XHRcdGxldCBkYXRlNSA9IHRoaXMuJHJlZnMuZGF0ZTUudmFsdWU7XG5cblx0XHRcdFx0XHRpZihkYXRlNSA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBlc3RpbWF0ZWQgdGltZSBvZiBmaW5pc2hpbmcgZGF0ZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyID0gZGF0ZTU7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA4KSB7XG5cdFx0XHRcdFx0bGV0IGRhdGU2ID0gdGhpcy4kcmVmcy5kYXRlNi52YWx1ZTtcblx0XHRcdFx0XHRsZXQgZGF0ZTcgPSB0aGlzLiRyZWZzLmRhdGU3LnZhbHVlO1xuXHRcdFx0XHRcdGxldCBkYXRlOCA9IHRoaXMuJHJlZnMuZGF0ZTgudmFsdWU7XG5cblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKGRhdGU2ID09ICcnIHx8IGRhdGU3ID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGFmZmVjdGVkIGRhdGUuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGU2O1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IGRhdGU3O1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMyA9IGRhdGU4O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOSkge1xuXHRcdFx0XHRcdGlmKHRoaXMuY3VzdG9tQ2F0ZWdvcnkpIHtcblx0XHRcdFx0XHRcdGxldCBjYXRlZ29yeU5hbWUgPSB0aGlzLiRyZWZzLmNhdGVnb3J5TmFtZS52YWx1ZTtcblxuXHRcdFx0XHRcdFx0aWYoY2F0ZWdvcnlOYW1lID09ICcnKSB7XG5cdFx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPSAwO1xuXHRcdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZSA9IGNhdGVnb3J5TmFtZTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRsZXQgZGF0ZTkgPSB0aGlzLiRyZWZzLmRhdGU5LnZhbHVlO1xuXHRcdFx0XHRcdGxldCBkYXRlMTAgPSB0aGlzLiRyZWZzLmRhdGUxMC52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSBudWxsKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY2F0ZWdvcnkuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoZGF0ZTkgPT0gJycgfHwgZGF0ZTEwID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGRhdGUgcmFuZ2UuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGU5O1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IGRhdGUxMDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEwKSB7XG5cdFx0XHRcdFx0bGV0IGRhdGUxMSA9IHRoaXMuJHJlZnMuZGF0ZTExLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZihkYXRlMTEgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgZXh0ZW5kZWQgcGVyaW9kIGZvciBwcm9jZXNzaW5nLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTEgPSBkYXRlMTE7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMSkge1xuXHRcdFx0XHRcdGxldCBkYXRlMTIgPSB0aGlzLiRyZWZzLmRhdGUxMi52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSBudWxsKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY2F0ZWdvcnkuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoZGF0ZTEyID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGVzdGltYXRlZCByZWxlYXNpbmcgZGF0ZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyID0gZGF0ZTEyO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTIpIHtcblx0XHRcdFx0XHRsZXQgZGF0ZTEzID0gdGhpcy4kcmVmcy5kYXRlMTMudmFsdWU7XG5cblx0XHRcdFx0XHRpZihkYXRlMTMgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgZGF0ZSBhbmQgdGltZSBwaWNrIHVwLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTEgPSBkYXRlMTM7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMykge1xuXHRcdFx0XHRcdGxldCBkYXRlMTQgPSB0aGlzLiRyZWZzLmRhdGUxNC52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKGRhdGUxNCA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBkYXRlIGFuZCB0aW1lIGRlbGl2ZXIuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGUxNDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE0KSB7XG5cdFx0XHRcdFx0bGV0IGRhdGUxNSA9IHRoaXMuJHJlZnMuZGF0ZTE1LnZhbHVlO1xuXHRcdFx0XHRcdGxldCBjb21wYW55Q291cmllciA9IHRoaXMuJHJlZnMuY29tcGFueV9jb3VyaWVyLnZhbHVlO1xuXHRcdFx0XHRcdGxldCB0cmFja2luZ051bWJlciA9IHRoaXMuJHJlZnMudHJhY2tpbmdfbnVtYmVyLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTI4ICYmIGNvbXBhbnlDb3VyaWVyID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY29tcGFueSBjb3VyaWVyLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCAhPSAxMjggJiYgdHJhY2tpbmdOdW1iZXIgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgdHJhY2tpbmcgbnVtYmVyLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKGRhdGUxNSA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBkYXRlIGFuZCB0aW1lIGRlbGl2ZXJlZC4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gZGF0ZTE1O1xuXG5cdFx0XHRcdFx0XHRsZXQgZXh0cmFEZXRhaWwgPSAodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEyOCkgPyBjb21wYW55Q291cmllciA6IHRyYWNraW5nTnVtYmVyO1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMgPSBbZXh0cmFEZXRhaWxdO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTUpIHtcblx0XHRcdFx0XHRsZXQgZGF0ZTE2ID0gdGhpcy4kcmVmcy5kYXRlMTYudmFsdWU7XG5cblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKCBkYXRlMTYgPT0gJycgJiYgKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3NiB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzcgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc4IHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3OSB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODAgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgxIHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MiB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODMpICkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBlc3RpbWF0ZWQgcmVsZWFzaW5nIGRhdGUuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IGRhdGUxNjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE2KSB7XG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cdFx0XHRcdHJldHVybiBpc1ZhbGlkO1xuICAgICAgICBcdH1cblxuICAgICAgICB9LFxuXG4gICAgICAgIGNyZWF0ZWQoKSB7XG4gICAgICAgIFx0dGhpcy5nZXRBY3Rpb25zKCk7XG5cbiAgICAgICAgXHR0aGlzLmdldENhdGVnb3JpZXMoKTtcblxuICAgICAgICBcdHRoaXMuZ2V0Q29tcGFueUNvdXJpZXJzKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgbW91bnRlZCgpIHtcblxuICAgICAgICBcdC8vIENob3NlblxuICAgICAgICBcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1kb2NzLTInKS5jaG9zZW4oe1xuXHRcdFx0XHRcdHdpZHRoOiBcIjEwMCVcIixcblx0XHRcdFx0XHRub19yZXN1bHRzX3RleHQ6IFwiTm8gcmVzdWx0L3MgZm91bmQuXCIsXG5cdFx0XHRcdFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXG5cdFx0XHRcdH0pO1xuXG4gICAgICAgIFx0Ly8gRGF0ZVBpY2tlclxuXHQgICAgICAgIFx0JCgnLmRhdGVwaWNrZXInKS5kYXRlcGlja2VyKHtcblx0ICAgICAgICAgICAgICAgIHRvZGF5QnRuOiBcImxpbmtlZFwiLFxuXHQgICAgICAgICAgICAgICAga2V5Ym9hcmROYXZpZ2F0aW9uOiBmYWxzZSxcblx0ICAgICAgICAgICAgICAgIGZvcmNlUGFyc2U6IGZhbHNlLFxuXHQgICAgICAgICAgICAgICAgY2FsZW5kYXJXZWVrczogdHJ1ZSxcblx0ICAgICAgICAgICAgICAgIGF1dG9jbG9zZTogdHJ1ZSxcblx0ICAgICAgICAgICAgICAgIGZvcm1hdDogXCJtbS9kZC95eXl5XCIsXG5cdCAgICAgICAgICAgICAgICBzdGFydERhdGU6IG5ldyBEYXRlKClcblx0ICAgICAgICAgICAgfSk7XG5cblx0XHRcdC8vIERhdGVUaW1lIFBpY2tlclxuXHRcdFx0XHQkKCcuZGF0ZXRpbWVwaWNrZXInKS5pbnB1dG1hc2soXCJkYXRldGltZVwiLCB7XG5cdFx0ICAgICAgICAgICAgbWFzazogXCJ5LTItMSBoOnM6c1wiLCBcblx0XHQgICAgICAgICAgICBwbGFjZWhvbGRlcjogXCJ5eXl5LW1tLWRkIGhoOm1tOnNzXCIsIFxuXHRcdCAgICAgICAgICAgIGxlYXBkYXk6IFwiLTAyLTI5XCIsIFxuXHRcdCAgICAgICAgICAgIHNlcGFyYXRvcjogXCItXCIsIFxuXHRcdCAgICAgICAgICAgIGFsaWFzOiBcInl5eXktbW0tZGRcIlxuXHRcdCAgICAgICAgfSk7XG5cbiAgICAgICAgfVxuXG5cdH1cblxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZT83YTJmODU2NyIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoKTtcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcbmZvcm1bZGF0YS12LTBmMmYyYjc3XSB7XFxuXFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxuLm0tci0xMFtkYXRhLXYtMGYyZjJiNzddIHtcXG5cXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJNdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZT8zNWY5OTZiOVwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBNFBBO0NBQ0Esb0JBQUE7Q0FDQTtBQUNBO0NBQ0EsbUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdFxcblxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwiYWRkTXVsdGlwbGVRdWlja1JlcG9ydFxcXCI+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiaW9zLXNjcm9sbFxcXCI+XFxuXFx0XFx0XFx0PGRpdiB2LWZvcj1cXFwiKGRpc3RpbmN0Q2xpZW50U2VydmljZSwgaW5kZXgpIGluIGRpc3RpbmN0Q2xpZW50U2VydmljZXMubGVuZ3RoXFxcIj5cXG5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJwYW5lbCBwYW5lbC1kZWZhdWx0XFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicGFuZWwtaGVhZGluZ1xcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICB7eyBkaXN0aW5jdENsaWVudFNlcnZpY2VzW2luZGV4XSB9fVxcblxcdCAgICAgICAgICAgICAgICA8L2Rpdj5cXG5cXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicGFuZWwtYm9keVxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXG5cXHQgICAgICAgICAgICAgICAgXFx0PG11bHRpcGxlLXF1aWNrLXJlcG9ydC1mb3JtIDprZXk9XFxcImluZGV4XFxcIiA6cmVmPVxcXCInZm9ybS0nICsgaW5kZXhcXFwiIDpkb2NzPVxcXCJkb2NzXFxcIiA6c2VydmljZWRvY3M9XFxcInNlcnZpY2VEb2NzXFxcIiA6aW5kZXg9XFxcImluZGV4XFxcIj48L211bHRpcGxlLXF1aWNrLXJlcG9ydC1mb3JtPlxcblxcblxcdCAgICAgICAgICAgICAgICA8L2Rpdj4gPCEtLSBwYW5lbC1ib2R5IC0tPlxcblxcdCAgICAgICAgICAgIDwvZGl2PiA8IS0tIHBhbmVsIC0tPlxcblxcblxcdFxcdFxcdDwvZGl2PiA8IS0tIGxvb3AgLS0+XFxuXFx0XFx0PC9kaXY+IDwhLS0gaW9zLXNjcm9sbCAtLT5cXG5cXHRcXHRcXG5cXHRcXHQ8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5DcmVhdGUgUmVwb3J0PC9idXR0b24+XFxuXFx0ICAgIDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBkYXRhLWRpc21pc3M9XFxcIm1vZGFsXFxcIj5DbG9zZTwvYnV0dG9uPlxcblxcdDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFxuXFx0ZXhwb3J0IGRlZmF1bHQge1xcblxcblxcdFxcdHByb3BzOiBbJ2dyb3VwcmVwb3J0JywgJ2dyb3VwaWQnLCAnY2xpZW50c2lkJywgJ2NsaWVudHNlcnZpY2VzJywgJ3RyYWNraW5ncycsICdpc3dyaXRlcmVwb3J0cGFnZSddLFxcblxcblxcdFxcdGRhdGEoKSB7XFxuXFx0XFx0XFx0cmV0dXJuIHtcXG5cXHRcXHRcXG5cXHRcXHRcXHRcXHRkb2NzOiBbXSxcXG5cXHRcXHRcXHRcXHRzZXJ2aWNlRG9jczogW10sXFxuXFxuICAgICAgICBcXHRcXHRtdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSA6IG5ldyBGb3JtKHtcXG4gICAgICAgIFxcdFxcdFxcdGRpc3RpbmN0Q2xpZW50U2VydmljZXM6W10sXFxuXFxuICAgICAgICBcXHRcXHRcXHRncm91cFJlcG9ydDogbnVsbCxcXG4gICAgICAgIFxcdFxcdFxcdGdyb3VwSWQ6IG51bGwsXFxuXFxuICAgICAgICBcXHRcXHRcXHRjbGllbnRzSWQ6IFtdLFxcbiAgICAgICAgXFx0XFx0XFx0Y2xpZW50U2VydmljZXNJZDogW10sXFxuICAgICAgICBcXHRcXHRcXHR0cmFja2luZ3M6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0XFx0YWN0aW9uSWQ6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0XFx0Y2F0ZWdvcnlJZDogW10sXFxuICAgICAgICBcXHRcXHRcXHRjYXRlZ29yeU5hbWU6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0XFx0ZGF0ZTE6IFtdLFxcbiAgICAgICAgXFx0XFx0XFx0ZGF0ZTI6IFtdLFxcbiAgICAgICAgXFx0XFx0XFx0ZGF0ZTM6IFtdLFxcbiAgICAgICAgXFx0XFx0XFx0ZGF0ZVRpbWVBcnJheTogW10sXFxuXFxuICAgICAgICBcXHRcXHRcXHRleHRyYURldGFpbHM6IFtdXFxuICAgICAgICBcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxcblxcblxcdFxcdFxcdH1cXG5cXHRcXHR9LFxcblxcblxcdFxcdG1ldGhvZHM6IHtcXG5cXG5cXHRcXHRcXHRmZXRjaERvY3MoaWQpIHtcXG5cXHRcXHRcXHRcXHR2YXIgaWRzID0gJyc7XFxuXFx0XFx0XFx0XFx0JC5lYWNoKGlkLCBmdW5jdGlvbihpLCBpdGVtKSB7XFxuXFx0XFx0XFx0XFx0ICAgIGlkcyArPSBpZFtpXS5kb2NzX25lZWRlZCArICcsJyArIGlkW2ldLmRvY3Nfb3B0aW9uYWwgKyAnLCc7XFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0XFx0dmFyIHVuaXF1ZUxpc3Q9aWRzLnNwbGl0KCcsJykuZmlsdGVyKGZ1bmN0aW9uKGFsbEl0ZW1zLGksYSl7XFxuXFx0XFx0XFx0XFx0ICAgIHJldHVybiBpPT1hLmluZGV4T2YoYWxsSXRlbXMpO1xcblxcdFxcdFxcdFxcdH0pLmpvaW4oJywnKTtcXG5cXHQgICAgICAgIFxcdFxcblxcdFxcdFxcdCBcXHRheGlvcy5nZXQoJy92aXNhL2dyb3VwLycrdW5pcXVlTGlzdCsnL3NlcnZpY2UtZG9jcycpXFxuXFx0XFx0XFx0IFxcdCAgLnRoZW4ocmVzdWx0ID0+IHtcXG5cXHRcXHQgICAgICAgICAgICB0aGlzLmRvY3MgPSByZXN1bHQuZGF0YTtcXG5cXHRcXHQgICAgICAgIH0pO1xcblxcblxcdCAgICAgICAgXFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0ICAgICAgICBcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItZG9jcycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XFx0XFxuXFx0ICAgICAgICBcXHR9LCA1MDAwKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGdldFNlcnZpY2VEb2NzKCkge1xcbiAgICAgICAgXFx0XFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC9zZXJ2aWNlLWRvY3MtaW5kZXgnKVxcblxcdFxcdFxcdCBcXHQgIC50aGVuKHJlc3VsdCA9PiB7XFxuXFx0XFx0ICAgICAgICAgICAgdGhpcy5zZXJ2aWNlRG9jcyA9IHJlc3VsdC5kYXRhO1xcblxcdFxcdCAgICAgICAgfSk7XFxuICAgICAgICBcXHR9LFxcblxcblxcdFxcdFxcdHJlc2V0TXVsdGlwbGVRdWlja1JlcG9ydEZvcm0oKSB7XFxuXFx0XFx0XFx0XFx0Ly8gTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzLmZvckVhY2goKGRpc3RpbmN0Q2xpZW50U2VydmljZSwgaW5kZXgpID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgcmVmID0gJ2Zvcm0tJyArIGluZGV4O1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSA9IHtcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGFjdGlvbklkOiAxLFxcblxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0Y2F0ZWdvcnlJZDogbnVsbCxcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGNhdGVnb3J5TmFtZTogbnVsbCxcXG5cXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGRhdGUxOiBudWxsLFxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTI6IG51bGwsXFxuXFx0XFx0ICAgICAgICBcXHRcXHRcXHRkYXRlMzogbnVsbCxcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGRhdGVUaW1lQXJyYXk6IFtdLFxcblxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0ZXh0cmFEZXRhaWxzOiBbXVxcblxcdFxcdCAgICAgICAgXFx0XFx0fVxcblxcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUyLnZhbHVlID0gJyc7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTMudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNC52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGU1LnZhbHVlID0gJyc7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTYudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNy52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGU4LnZhbHVlID0gJyc7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTkudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTAudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTEudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTIudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTMudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTQudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTUudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTYudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5jYXRlZ29yeU5hbWUudmFsdWUgPSAnJztcXG5cXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS5jdXN0b21DYXRlZ29yeSA9IGZhbHNlO1xcblxcdFxcdFxcdFxcdFxcdH0pO1xcblxcbiAgICAgICAgXFx0XFx0Ly8gTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcXG5cXHQgICAgICAgIFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0gPSBuZXcgRm9ybSh7XFxuXFx0ICAgICAgICBcXHRcXHRcXHRkaXN0aW5jdENsaWVudFNlcnZpY2VzOltdLFxcblxcblxcdCAgICAgICAgXFx0XFx0XFx0YWN0aW9uSWQ6IFtdLFxcblxcblxcdCAgICAgICAgXFx0XFx0XFx0Y2F0ZWdvcnlJZDogW10sXFxuXFx0ICAgICAgICBcXHRcXHRcXHRjYXRlZ29yeU5hbWU6IFtdLFxcblxcblxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTE6IFtdLFxcblxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTI6IFtdLFxcblxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTM6IFtdLFxcblxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZVRpbWVBcnJheTogW10sXFxuXFxuXFx0ICAgICAgICBcXHRcXHRcXHRleHRyYURldGFpbHM6IFtdXFxuXFx0ICAgICAgICBcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdHZhbGlkYXRlKCkge1xcblxcdFxcdFxcdFxcdHZhciBpc1ZhbGlkID0gdHJ1ZTtcXG5cXG5cXHRcXHRcXHRcXHR0aGlzLmRpc3RpbmN0Q2xpZW50U2VydmljZXMuZm9yRWFjaCgoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdGxldCByZWYgPSAnZm9ybS0nICsgaW5kZXg7XFxuXFxuXFx0XFx0XFx0XFx0XFx0aWYoICEgdGhpcy4kcmVmc1tyZWZdWzBdLnZhbGlkYXRlKGRpc3RpbmN0Q2xpZW50U2VydmljZSkgKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0aXNWYWxpZCA9IGZhbHNlO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHR9KTtcXG5cXG5cXHRcXHRcXHRcXHRyZXR1cm4gaXNWYWxpZDtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGFkZE11bHRpcGxlUXVpY2tSZXBvcnQoKSB7XFxuXFx0XFx0XFx0XFx0aWYodGhpcy52YWxpZGF0ZSgpKSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5ncm91cFJlcG9ydCA9IHRoaXMuZ3JvdXByZXBvcnQ7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5ncm91cElkID0gdGhpcy5ncm91cGlkO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2xpZW50c0lkID0gdGhpcy5jbGllbnRzaWQ7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5jbGllbnRTZXJ2aWNlc0lkID0gdGhpcy5jbGllbnRzZXJ2aWNlcztcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnRyYWNraW5ncyA9IHRoaXMudHJhY2tpbmdzO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGlzdGluY3RDbGllbnRTZXJ2aWNlcyA9IHRoaXMuZGlzdGluY3RDbGllbnRTZXJ2aWNlcztcXG5cXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmRpc3RpbmN0Q2xpZW50U2VydmljZXMuZm9yRWFjaCgoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdGxldCByZWYgPSAnZm9ybS0nICsgaW5kZXg7XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9hY3Rpb25JZCA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZDtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2NhdGVnb3J5SWQgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZDtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2NhdGVnb3J5TmFtZSA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeU5hbWU7XFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9kYXRlMSA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2RhdGUyID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyO1xcblxcdFxcdFxcdFxcdFxcdFxcdGxldCBfZGF0ZTMgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTM7XFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9kYXRlVGltZUFycmF5ID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9leHRyYURldGFpbHMgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzO1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uYWN0aW9uSWQucHVzaChfYWN0aW9uSWQpO1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2F0ZWdvcnlJZC5wdXNoKF9jYXRlZ29yeUlkKTtcXG5cXHQgICAgICAgIFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2F0ZWdvcnlOYW1lLnB1c2goX2NhdGVnb3J5TmFtZSk7XFxuXFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmRhdGUxLnB1c2goX2RhdGUxKTtcXG5cXHQgICAgICAgIFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTIucHVzaChfZGF0ZTIpO1xcblxcdCAgICAgICAgXFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kYXRlMy5wdXNoKF9kYXRlMyk7XFxuXFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmRhdGVUaW1lQXJyYXkucHVzaChfZGF0ZVRpbWVBcnJheSk7XFxuXFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmV4dHJhRGV0YWlscy5wdXNoKF9leHRyYURldGFpbHMpO1xcblxcdFxcdFxcdFxcdFxcdH0pO1xcblxcblxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uc3VibWl0KCdwb3N0JywgJy92aXNhL3F1aWNrLXJlcG9ydCcpXFxuXFx0XFx0XFx0ICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHR0aGlzLnJlc2V0TXVsdGlwbGVRdWlja1JlcG9ydEZvcm0oKTtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHRcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHQkKCcjYWRkLXF1aWNrLXJlcG9ydC1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0aWYodGhpcy5pc3dyaXRlcmVwb3J0cGFnZSkge1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdFxcdCQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykub24oJ2hpZGRlbi5icy5tb2RhbCcsIGZ1bmN0aW9uIChlKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICBcXHR3aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvdmlzYS9yZXBvcnQvd3JpdGUtcmVwb3J0JztcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR9KTtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHR9IGVsc2Uge1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdFxcdHNldFRpbWVvdXQoKCkgPT4ge1xcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdFxcdGxvY2F0aW9uLnJlbG9hZCgpO1xcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdH0sIDEwMDApO1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdH1cXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHR9XFxuXFx0XFx0XFx0ICAgICAgICAgICAgfSk7XFxuXFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdH1cXG5cXG5cXHRcXHR9LFxcblxcblxcdFxcdGNyZWF0ZWQoKSB7XFxuXFx0XFx0XFx0dGhpcy5nZXRTZXJ2aWNlRG9jcygpO1xcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y29tcHV0ZWQ6IHtcXG5cXHRcXHRcXHRfaXNXcml0ZVJlcG9ydFBhZ2UoKSB7XFxuXFx0XFx0ICAgICAgXFx0cmV0dXJuIHRoaXMuaXN3cml0ZXJlcG9ydHBhZ2UgfHwgZmFsc2U7XFxuXFx0XFx0ICAgIH0sXFxuXFxuXFx0XFx0XFx0ZGlzdGluY3RDbGllbnRTZXJ2aWNlcygpIHtcXG5cXHRcXHRcXHRcXHRsZXQgZGlzdGluY3RDbGllbnRTZXJ2aWNlcyA9IFtdO1xcblxcblxcdFxcdFxcdFxcdHRoaXMuY2xpZW50c2VydmljZXMuZm9yRWFjaChjbGllbnRTZXJ2aWNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRpZihkaXN0aW5jdENsaWVudFNlcnZpY2VzLmluZGV4T2YoY2xpZW50U2VydmljZS5kZXRhaWwpID09IC0xKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0ZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5wdXNoKGNsaWVudFNlcnZpY2UuZGV0YWlsKTtcXG5cXHRcXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0XFx0cmV0dXJuIGRpc3RpbmN0Q2xpZW50U2VydmljZXM7XFxuXFx0XFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y29tcG9uZW50czoge1xcblxcdCAgICAgICAgJ211bHRpcGxlLXF1aWNrLXJlcG9ydC1mb3JtJzogcmVxdWlyZSgnLi9NdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWUnKVxcblxcdCAgICB9XFxuXFxuXFx0fVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMGYyZjJiNzdcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTJcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjYzNDc2M2VcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIERpYWxvZ01vZGFsLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi02NjM0NzYzZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTY2MzQ3NjNlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSAxM1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiAyNSIsIlxuLyogc3R5bGVzICovXG5yZXF1aXJlKFwiISF2dWUtc3R5bGUtbG9hZGVyIWNzcy1sb2FkZXI/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTBmMmYyYjc3XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXN0eWxlcyZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTBmMmYyYjc3XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIFwiZGF0YS12LTBmMmYyYjc3XCIsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBNdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0wZjJmMmI3N1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTBmMmYyYjc3XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTRcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZWIyZmRhNGFcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWViMmZkYTRhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtZWIyZmRhNGFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTVcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmFkZE11bHRpcGxlUXVpY2tSZXBvcnQoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW9zLXNjcm9sbFwiXG4gIH0sIF92bS5fbCgoX3ZtLmRpc3RpbmN0Q2xpZW50U2VydmljZXMubGVuZ3RoKSwgZnVuY3Rpb24oZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkge1xuICAgIHJldHVybiBfYygnZGl2JywgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWhlYWRpbmdcIlxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5kaXN0aW5jdENsaWVudFNlcnZpY2VzW2luZGV4XSkgKyBcIlxcbiAgICAgICAgICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIlxuICAgIH0sIFtfYygnbXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0nLCB7XG4gICAgICBrZXk6IGluZGV4LFxuICAgICAgcmVmOiAnZm9ybS0nICsgaW5kZXgsXG4gICAgICByZWZJbkZvcjogdHJ1ZSxcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiZG9jc1wiOiBfdm0uZG9jcyxcbiAgICAgICAgXCJzZXJ2aWNlZG9jc1wiOiBfdm0uc2VydmljZURvY3MsXG4gICAgICAgIFwiaW5kZXhcIjogaW5kZXhcbiAgICAgIH1cbiAgICB9KV0sIDEpXSldKVxuICB9KSwgMCksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDcmVhdGUgUmVwb3J0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTBmMmYyYjc3XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMGYyZjJiNzdcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTZcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsIG1kLW1vZGFsIGZhZGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBfdm0uaWQsXG4gICAgICBcInRhYmluZGV4XCI6IFwiLTFcIixcbiAgICAgIFwicm9sZVwiOiBcImRpYWxvZ1wiLFxuICAgICAgXCJhcmlhLWxhYmVsbGVkYnlcIjogXCJtb2RhbC1sYWJlbFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBjbGFzczogJ21vZGFsLWRpYWxvZyAnICsgX3ZtLnNpemUsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwicm9sZVwiOiBcImRvY3VtZW50XCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1oZWFkZXJcIlxuICB9LCBbX2MoJ2g0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLXRpdGxlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJtb2RhbC1sYWJlbFwiXG4gICAgfVxuICB9LCBbX3ZtLl90KFwibW9kYWwtdGl0bGVcIiwgW192bS5fdihcIk1vZGFsIFRpdGxlXCIpXSldLCAyKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWJvZHlcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtYm9keVwiLCBbX3ZtLl92KFwiTW9kYWwgQm9keVwiKV0pXSwgMiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtZm9vdGVyXCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWZvb3RlclwiLCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnlcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSldLCAyKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi02NjM0NzYzZVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTY2MzQ3NjNlXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMjUiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDApLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnc2VsZWN0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWRcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcIm5hbWVcIjogXCJhY3Rpb25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IFtmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkc2VsZWN0ZWRWYWwgPSBBcnJheS5wcm90b3R5cGUuZmlsdGVyLmNhbGwoJGV2ZW50LnRhcmdldC5vcHRpb25zLCBmdW5jdGlvbihvKSB7XG4gICAgICAgICAgcmV0dXJuIG8uc2VsZWN0ZWRcbiAgICAgICAgfSkubWFwKGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICB2YXIgdmFsID0gXCJfdmFsdWVcIiBpbiBvID8gby5fdmFsdWUgOiBvLnZhbHVlO1xuICAgICAgICAgIHJldHVybiB2YWxcbiAgICAgICAgfSk7XG4gICAgICAgIF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJhY3Rpb25JZFwiLCAkZXZlbnQudGFyZ2V0Lm11bHRpcGxlID8gJCRzZWxlY3RlZFZhbCA6ICQkc2VsZWN0ZWRWYWxbMF0pXG4gICAgICB9LCBfdm0uZ2V0Q2F0ZWdvcmllc11cbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLmFjdGlvbnMpLCBmdW5jdGlvbihhY3Rpb24pIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogYWN0aW9uLmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICAgICAgICAgXFx0XFx0XCIgKyBfdm0uX3MoYWN0aW9uLm5hbWUpICsgXCJcXG5cXHRcXHQgICAgICAgICAgICBcIildKVxuICB9KSwgMCldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAyIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAzIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA4IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA5IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMCB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTEgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE0IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNSB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTYpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDIgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMyB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA4IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDkgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTAgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTEgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTQgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTUgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTZcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDEpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnc2VsZWN0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkXCJcbiAgICB9LCB7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKCFfdm0uY3VzdG9tQ2F0ZWdvcnkpLFxuICAgICAgZXhwcmVzc2lvbjogXCIhY3VzdG9tQ2F0ZWdvcnlcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcIm5hbWVcIjogXCJjYXRlZ29yeVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjaGFuZ2VcIjogW2Z1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRzZWxlY3RlZFZhbCA9IEFycmF5LnByb3RvdHlwZS5maWx0ZXIuY2FsbCgkZXZlbnQudGFyZ2V0Lm9wdGlvbnMsIGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICByZXR1cm4gby5zZWxlY3RlZFxuICAgICAgICB9KS5tYXAoZnVuY3Rpb24obykge1xuICAgICAgICAgIHZhciB2YWwgPSBcIl92YWx1ZVwiIGluIG8gPyBvLl92YWx1ZSA6IG8udmFsdWU7XG4gICAgICAgICAgcmV0dXJuIHZhbFxuICAgICAgICB9KTtcbiAgICAgICAgX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImNhdGVnb3J5SWRcIiwgJGV2ZW50LnRhcmdldC5tdWx0aXBsZSA/ICQkc2VsZWN0ZWRWYWwgOiAkJHNlbGVjdGVkVmFsWzBdKVxuICAgICAgfSwgX3ZtLmNoYW5nZUNhdGVnb3J5SGFuZGxlcl1cbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLmNhdGVnb3JpZXMpLCBmdW5jdGlvbihjYXRlZ29yeSkge1xuICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBjYXRlZ29yeS5pZFxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgICAgICBcXHRcIiArIF92bS5fcyhjYXRlZ29yeS5uYW1lKSArIFwiXFxuXFx0XFx0ICAgICAgICAgICAgXCIpXSlcbiAgfSksIDApLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA5KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOVwiXG4gICAgfV1cbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeU5hbWUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeU5hbWVcIlxuICAgIH0sIHtcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmN1c3RvbUNhdGVnb3J5KSxcbiAgICAgIGV4cHJlc3Npb246IFwiY3VzdG9tQ2F0ZWdvcnlcIlxuICAgIH1dLFxuICAgIHJlZjogXCJjYXRlZ29yeU5hbWVcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJwbGFjZWhvbGRlclwiOiBcIkVudGVyIGNhdGVnb3J5XCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlOYW1lKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImNhdGVnb3J5TmFtZVwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMTBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1zbSBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmN1c3RvbUNhdGVnb3J5ID0gIV92bS5jdXN0b21DYXRlZ29yeTtcbiAgICAgICAgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPSBudWxsXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoIV92bS5jdXN0b21DYXRlZ29yeSksXG4gICAgICBleHByZXNzaW9uOiBcIiFjdXN0b21DYXRlZ29yeVwiXG4gICAgfV1cbiAgfSwgW192bS5fdihcIlVzZSBDdXN0b20gQ2F0ZWdvcnlcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmN1c3RvbUNhdGVnb3J5KSxcbiAgICAgIGV4cHJlc3Npb246IFwiY3VzdG9tQ2F0ZWdvcnlcIlxuICAgIH1dXG4gIH0sIFtfdm0uX3YoXCJVc2UgUHJlZGVmaW5lZCBDYXRlZ29yeVwiKV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgzKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlMlwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlMlwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMSksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0U2NoZWR1bGVkIEhlYXJpbmcgRGF0ZSBhbmQgVGltZTpcXG5cXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW192bS5fbCgoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkpLCBmdW5jdGlvbihkYXRlVGltZUVsZW1lbnQsIGluZGV4KSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTExXCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIixcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwibWFyZ2luLWJvdHRvbVwiOiBcIjEwcHhcIlxuICAgICAgfVxuICAgIH0sIFtfdm0uX20oNCwgdHJ1ZSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgIHZhbHVlOiAoZGF0ZVRpbWVFbGVtZW50LnZhbHVlKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJkYXRlVGltZUVsZW1lbnQudmFsdWVcIlxuICAgICAgfV0sXG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJhcnJheVwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgICBcIm5hbWVcIjogXCJkYXRlVGltZUFycmF5W11cIixcbiAgICAgICAgXCJwbGFjZWhvbGRlclwiOiBcInl5eXktbW0tZGQgaGg6bW06c3NcIixcbiAgICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgICAgfSxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogKGRhdGVUaW1lRWxlbWVudC52YWx1ZSlcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgICBfdm0uJHNldChkYXRlVGltZUVsZW1lbnQsIFwidmFsdWVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xIHRleHQtY2VudGVyXCJcbiAgICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgICB2YWx1ZTogKGluZGV4ICE9IDApLFxuICAgICAgICBleHByZXNzaW9uOiBcImluZGV4ICE9IDBcIlxuICAgICAgfV0sXG4gICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRhbmdlciBidG4tc21cIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBfdm0ucmVtb3ZlRGF0ZVRpbWVQaWNrZXJBcnJheShpbmRleClcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIFtfYygnaScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXRpbWVzXCJcbiAgICB9KV0pXSldKVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCIxMHB4XCIsXG4gICAgICBcImNsZWFyXCI6IFwiYm90aFwiXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgYnRuLXNtIHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS5hZGREYXRlVGltZVBpY2tlckFycmF5XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtcGx1c1wiXG4gIH0pLCBfdm0uX3YoXCIgQWRkXFxuICAgICAgICAgICAgICAgICAgICBcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNiB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEwIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDE0IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDE1KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA2IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNyB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEwIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTQgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0U2NoZWR1bGVkIEFwcG9pbnRtZW50IERhdGUgYW5kIFRpbWU6XFxuXFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDUpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUzXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlM1wiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMiksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDJcIlxuICAgIH1dXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAzKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gM1wiXG4gICAgfV1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDQpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA0XCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSg2KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ3NlbGVjdCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjaG9zZW4tc2VsZWN0LWZvci1kb2NzLTJcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjM1MHB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6ICdjaG9zZW4tJyArIF92bS5pbmRleCxcbiAgICAgIFwiZGF0YS1wbGFjZWhvbGRlclwiOiBcIlNlbGVjdCBEb2NzXCIsXG4gICAgICBcIm11bHRpcGxlXCI6IFwiXCIsXG4gICAgICBcInRhYmluZGV4XCI6IFwiNFwiXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5zZXJ2aWNlZG9jcyksIGZ1bmN0aW9uKHNlcnZpY2Vkb2MpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogc2VydmljZWRvYy50aXRsZVxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0XCIgKyBfdm0uX3MoKHNlcnZpY2Vkb2MudGl0bGUpLnRyaW0oKSkgKyBcIlxcblxcdFxcdCAgICAgICAgICAgICAgICBcIildKVxuICB9KSwgMCldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDYpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA2XCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSg3KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSg4KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlNFwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlNFwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0Q29uc2VxdWVuY2Ugb2Ygbm9uIHN1Ym1pc3Npb246XFxuXFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2JywgW19jKCdsYWJlbCcsIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiY2hlY2tib3hcIixcbiAgICAgIFwidmFsdWVcIjogXCJBZGRpdGlvbmFsIGNvc3QgbWF5YmUgY2hhcmdlZC5cIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBBcnJheS5pc0FycmF5KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpID8gX3ZtLl9pKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMsIFwiQWRkaXRpb25hbCBjb3N0IG1heWJlIGNoYXJnZWQuXCIpID4gLTEgOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscylcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkYSA9IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMsXG4gICAgICAgICAgJCRlbCA9ICRldmVudC50YXJnZXQsXG4gICAgICAgICAgJCRjID0gJCRlbC5jaGVja2VkID8gKHRydWUpIDogKGZhbHNlKTtcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgIHZhciAkJHYgPSBcIkFkZGl0aW9uYWwgY29zdCBtYXliZSBjaGFyZ2VkLlwiLFxuICAgICAgICAgICAgJCRpID0gX3ZtLl9pKCQkYSwgJCR2KTtcbiAgICAgICAgICBpZiAoJCRlbC5jaGVja2VkKSB7XG4gICAgICAgICAgICAkJGkgPCAwICYmIChfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYS5jb25jYXQoWyQkdl0pKSlcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCRpID4gLTEgJiYgKF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRhLnNsaWNlKDAsICQkaSkuY29uY2F0KCQkYS5zbGljZSgkJGkgKyAxKSkpKVxuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYylcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIlxcblxcdFxcdFxcdCAgICBcXHRcXHRcXHRBZGRpdGlvbmFsIGNvc3QgbWF5YmUgY2hhcmdlZC5cXG5cXHRcXHRcXHQgICAgXFx0XFx0XCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIFtfYygnbGFiZWwnLCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscyksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlsc1wiXG4gICAgfV0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImNoZWNrYm94XCIsXG4gICAgICBcInZhbHVlXCI6IFwiQXBwbGljYXRpb24gbWF5YmUgZm9yZmVpdGVkIGFuZCBoYXMgdG8gcmUtYXBwbHkuXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcImNoZWNrZWRcIjogQXJyYXkuaXNBcnJheShfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKSA/IF92bS5faShfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzLCBcIkFwcGxpY2F0aW9uIG1heWJlIGZvcmZlaXRlZCBhbmQgaGFzIHRvIHJlLWFwcGx5LlwiKSA+IC0xIDogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIHZhciAkJGEgPSBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzLFxuICAgICAgICAgICQkZWwgPSAkZXZlbnQudGFyZ2V0LFxuICAgICAgICAgICQkYyA9ICQkZWwuY2hlY2tlZCA/ICh0cnVlKSA6IChmYWxzZSk7XG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KCQkYSkpIHtcbiAgICAgICAgICB2YXIgJCR2ID0gXCJBcHBsaWNhdGlvbiBtYXliZSBmb3JmZWl0ZWQgYW5kIGhhcyB0byByZS1hcHBseS5cIixcbiAgICAgICAgICAgICQkaSA9IF92bS5faSgkJGEsICQkdik7XG4gICAgICAgICAgaWYgKCQkZWwuY2hlY2tlZCkge1xuICAgICAgICAgICAgJCRpIDwgMCAmJiAoX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGEuY29uY2F0KFskJHZdKSkpXG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQkaSA+IC0xICYmIChfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYS5zbGljZSgwLCAkJGkpLmNvbmNhdCgkJGEuc2xpY2UoJCRpICsgMSkpKSlcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGMpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgXFx0XFx0XFx0QXBwbGljYXRpb24gbWF5YmUgZm9yZmVpdGVkIGFuZCBoYXMgdG8gcmUtYXBwbHkuXFxuXFx0XFx0XFx0ICAgIFxcdFxcdFwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCBbX2MoJ2xhYmVsJywgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHNcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiLFxuICAgICAgXCJ2YWx1ZVwiOiBcIkluaXRpYWwgZGVwb3NpdCBtYXliZSBmb3JmZWl0ZWQuXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcImNoZWNrZWRcIjogQXJyYXkuaXNBcnJheShfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKSA/IF92bS5faShfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzLCBcIkluaXRpYWwgZGVwb3NpdCBtYXliZSBmb3JmZWl0ZWQuXCIpID4gLTEgOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscylcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkYSA9IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMsXG4gICAgICAgICAgJCRlbCA9ICRldmVudC50YXJnZXQsXG4gICAgICAgICAgJCRjID0gJCRlbC5jaGVja2VkID8gKHRydWUpIDogKGZhbHNlKTtcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgIHZhciAkJHYgPSBcIkluaXRpYWwgZGVwb3NpdCBtYXliZSBmb3JmZWl0ZWQuXCIsXG4gICAgICAgICAgICAkJGkgPSBfdm0uX2koJCRhLCAkJHYpO1xuICAgICAgICAgIGlmICgkJGVsLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICQkaSA8IDAgJiYgKF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRhLmNvbmNhdChbJCR2XSkpKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkJGkgPiAtMSAmJiAoX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGEuc2xpY2UoMCwgJCRpKS5jb25jYXQoJCRhLnNsaWNlKCQkaSArIDEpKSkpXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRjKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgIFxcdFxcdFxcdEluaXRpYWwgZGVwb3NpdCBtYXliZSBmb3JmZWl0ZWQuXFxuXFx0XFx0XFx0ICAgIFxcdFxcdFwiKV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDcpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA3XCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSg5KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgxMCksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTVcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGU1XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA4KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOFwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMTEpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWRhdGVyYW5nZSBpbnB1dC1ncm91cFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGU2XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtc20gZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGU2XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX3ZtLl92KFwidG9cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlN1wiLFxuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LXNtIGZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlN1wiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0VGFyZ2V0IEZpbGxpbmcgRGF0ZTpcXG5cXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMTIpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGU4XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGU4XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA5KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMTMpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWRhdGVyYW5nZSBpbnB1dC1ncm91cFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGU5XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtc20gZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGU5XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX3ZtLl92KFwidG9cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlMTBcIixcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTEwXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMCksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEwXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgxNCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMTUpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUxMVwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlMTFcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDExKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTFcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDE2KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgxNyksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTEyXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUxMlwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMlwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMTgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDE5KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlMTNcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUxM1wiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTMpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxM1wiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMjApLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDIxKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlMTRcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUxNFwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTQpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNFwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTI4KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxMjhcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDIyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ3NlbGVjdCcsIHtcbiAgICByZWY6IFwiY29tcGFueV9jb3VyaWVyXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJjb21wYW55X2NvdXJpZXJcIlxuICAgIH1cbiAgfSwgX3ZtLl9sKChfdm0uY29tcGFueUNvdXJpZXJzKSwgZnVuY3Rpb24oY29tcGFueUNvdXJpZXIpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogY29tcGFueUNvdXJpZXIuYWRkcmVzcy5jb250YWN0X251bWJlclxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgIFxcdFxcdFwiICsgX3ZtLl9zKGNvbXBhbnlDb3VyaWVyLmZpcnN0X25hbWUgKyAnICcgKyBjb21wYW55Q291cmllci5sYXN0X25hbWUpICsgXCIgXFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHQoXCIgKyBfdm0uX3MoY29tcGFueUNvdXJpZXIuYWRkcmVzcy5jb250YWN0X251bWJlcikgKyBcIilcXG5cXHRcXHRcXHQgICAgICAgIFxcdFwiKV0pXG4gIH0pLCAwKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgIT0gMTI4KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCAhPSAxMjhcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDIzKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJ0cmFja2luZ19udW1iZXJcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcInRyYWNraW5nX251bWJlclwiXG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMjQpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDI1KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlMTVcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUxNVwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzYgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzcgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzggfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzkgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODAgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODEgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODIgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODMpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc2IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzcgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3OCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc5IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODAgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgyIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODNcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDI2KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgyNyksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTE2XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUxNlwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICBBY3Rpb246IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgIENhdGVnb3J5OiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdEVzdGltYXRlZCBSZWxlYXNpbmcgRGF0ZTogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHREb2N1bWVudHM6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0U3VibWlzc2lvbiBEYXRlOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdEVzdGltYXRlZCBUaW1lIG9mIEZpbmlzaGluZzogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRBZmZlY3RlZCBEYXRlOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdERhdGUgUmFuZ2U6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RXh0ZW5kZWQgcGVyaW9kIGZvciBwcm9jZXNzaW5nOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdEVzdGltYXRlZCBSZWxlYXNpbmcgRGF0ZTogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHREYXRlIGFuZCBUaW1lIFBpY2sgVXA6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RGF0ZSBhbmQgVGltZSBEZWxpdmVyOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdENvbXBhbnkgQ291cmllcjogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRUcmFja2luZyBOdW1iZXI6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RGF0ZSBhbmQgVGltZSBEZWxpdmVyZWQ6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RXN0aW1hdGVkIFJlbGVhc2luZyBEYXRlOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59XX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtZWIyZmRhNGFcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1lYjJmZGE0YVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMThcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsImltcG9ydCBNdWx0aXNlbGVjdCBmcm9tICd2dWUtbXVsdGlzZWxlY3QnXG5cblZ1ZS5jb21wb25lbnQoJ21vZGFscycsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL01vZGFsLnZ1ZScpKTtcblZ1ZS5jb21wb25lbnQoJ2RpYWxvZy1tb2RhbCcsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZScpKTtcbi8vIFZ1ZS5jb21wb25lbnQoJ3Jvdy1kb2N1bWVudCcsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL0RvY3VtZW50cy9Eb2N1bWVudENvbnRhaW5lci52dWUnKSk7XG5WdWUuY29tcG9uZW50KCdwYWNrYWdlLWl0ZW0nLCAgcmVxdWlyZSgnLi4vdmlzYS9jbGllbnRzL1BhY2thZ2UudnVlJykpO1xuVnVlLmNvbXBvbmVudCgnc2VydmljZS1pdGVtJywgIHJlcXVpcmUoJy4uL3Zpc2EvY2xpZW50cy9TZXJ2aWNlLnZ1ZScpKTtcblZ1ZS5jb21wb25lbnQoJ2FjdGlvbi1sb2dzJywgIHJlcXVpcmUoJy4uL3Zpc2EvY2xpZW50cy9BY3Rpb25Mb2dzLnZ1ZScpKTtcblZ1ZS5jb21wb25lbnQoJ3RyYW5zYWN0aW9uLWxvZ3MnLCAgcmVxdWlyZSgnLi4vdmlzYS9jbGllbnRzL1RyYW5zYWN0aW9uTG9ncy52dWUnKSk7XG5WdWUuY29tcG9uZW50KCdhZGQtbmV3LXNlcnZpY2UnLCAgcmVxdWlyZSgnLi4vdmlzYS9jbGllbnRzL0FkZFNlcnZpY2UudnVlJykpO1xuVnVlLmNvbXBvbmVudCgnZWRpdC1zZXJ2aWNlJywgIHJlcXVpcmUoJy4uL3Zpc2EvY2xpZW50cy9FZGl0U2VydmljZS52dWUnKSk7XG5cbmxldCBkYXRhID0gd2luZG93LkxhcmF2ZWwuZGF0YTtcbmxldCBhZGQgPSBkYXRhLmFkZHJlc3MgIT1udWxsPyBkYXRhLmFkZHJlc3MuYWRkcmVzcyA6IG51bGw7XG5sZXQgY2l0eSA9IGRhdGEuYWRkcmVzcyAhPW51bGw/IGRhdGEuYWRkcmVzcy5jaXR5IDogbnVsbDtcbmlmKGNpdHkhPW51bGwpe1xuXHRhZGQ9YWRkK1wiLFwiK2NpdHk7XG59XG5sZXQgcHJvdmluY2UgPSBkYXRhLmFkZHJlc3MgIT1udWxsPyBkYXRhLmFkZHJlc3MucHJvdmluY2UgOiBudWxsO1xuaWYocHJvdmluY2UhPW51bGwpe1xuXHRhZGQ9YWRkK1wiLFwiK3Byb3ZpbmNlO1xufVxubGV0IG51bWIgPSBkYXRhLmFkZHJlc3MgIT1udWxsPyBkYXRhLmFkZHJlc3MuY29udGFjdF9udW1iZXIgOiBudWxsO1xudmFyIGNsaWVudF9pZCA9IGRhdGEuaWQ7XG52YXIgdXNlciA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XG52YXIgQ2xpZW50QXBwID0gbmV3IFZ1ZSh7XG5cdGVsOicjdXNlcnByb2ZpbGUnLFxuXHRkYXRhOntcblx0XHRjbGllbnRzOiBbXSxcblx0XHRwcm9jZXNzb3I6IHVzZXIsXG5cdFx0aXNfYXV0aDogZmFsc2UsXG5cdFx0YXV0aG9yaXplcnM6W10sXG5cdFx0dXNyX2lkOiBkYXRhLmlkLFxuXHRcdHJjdnM6IGRhdGEuZG9jdW1lbnRfcmVjZWl2ZSxcblx0XHRmdWxsbmFtZTogZGF0YS5mdWxsX25hbWUsXG5cdFx0Zmlyc3RfbmFtZTogZGF0YS5maXJzdF9uYW1lLFxuXHRcdGxhc3RfbmFtZTogZGF0YS5sYXN0X25hbWUsXG5cdFx0YXZhdGFyOiBkYXRhLmF2YXRhcixcblx0XHRld2FsbGV0X2JhbGFuY2U6MCxcblx0XHRlbWFpbDogZGF0YS5lbWFpbCxcblx0XHRiZGF5OiBkYXRhLmJpcnRoX2RhdGUsXG5cdFx0Z2VuZGVyOiBkYXRhLmdlbmRlcixcblx0XHRjc3RhdHVzOiBkYXRhLmNpdmlsX3N0YXR1cyxcblx0XHRwYXNzcG9ydDogZGF0YS5wYXNzcG9ydCxcblx0XHRoZWlnaHQ6IGRhdGEuaGVpZ2h0LFxuXHRcdHdlaWdodDogZGF0YS53ZWlnaHQsXG5cdFx0dW5yZWFkX25vdGlmOiBkYXRhLnVucmVhZF9ub3RpZixcblx0XHRncm91cF9iaW5kZWQ6IGRhdGEuZ3JvdXBfYmluZGVkLFxuXHRcdGJpbmRlZDogZGF0YS5iaW5kZWQsXG5cdFx0YWRkcmVzczogZGF0YS5hZGRyZXNzZXMsXG5cdFx0YWRkOiBhZGQsXG5cdFx0bnVtYmVyOiBudW1iLFxuXHRcdGlzX3ZlcmlmaWVkOiBkYXRhLmlzX3ZlcmlmaWVkLFxuXHRcdGlzX2RvY3NfdmVyaWZpZWQ6IGRhdGEuaXNfZG9jc192ZXJpZmllZCxcblx0XHRzZXJ2aWNlX2Nvc3Q6IG51bGwsXG5cdFx0ZGVwb3NpdDogbnVsbCxcblx0XHRwYXltZW50OiBudWxsLFxuXHRcdHJlZnVuZDogbnVsbCxcblx0XHRkaXNjb3VudDogbnVsbCxcblx0XHRiYWxhbmNlOiBudWxsLFxuXHRcdHRyYW5zZmVyOiBudWxsLFxuXHRcdHBhY2thZ2VzOiBbXSxcblx0XHRzZXJ2aWNlczogW10sXG5cdFx0cGFja2FnZV9kYXRlOiBudWxsLFxuXHRcdHBhY2thZ2VfY29zdDogbnVsbCxcblx0XHRwYWNrYWdlX2RlcG9zaXQ6IG51bGwsXG5cdFx0cGFja2FnZV9wYXltZW50OiBudWxsLFxuXHRcdHBhY2thZ2VfcmVmdW5kOiBudWxsLFxuXHRcdHBhY2thZ2VfZGlzY291bnQ6IG51bGwsXG5cdFx0cGFja2FnZV9iYWxhbmNlOiBudWxsLFxuXHRcdHRyYWNraW5nX3NlbGVjdGVkOm51bGwsXG5cdFx0Z3JvdXBfcGFja2FnZTpmYWxzZSxcblx0XHR0cmFja2luZ19zdGF0dXM6bnVsbCxcblxuXHRcdGFjdGlvbmxvZ3M6IFtdLFxuXHRcdGN1cnJlbnRfeWVhcjogbnVsbCxcblx0XHRjdXJyZW50X21vbnRoOiBudWxsLFxuXHRcdGN1cnJlbnRfZGF5OiBudWxsLFxuXHRcdGNfeWVhcjogbnVsbCxcblx0XHRjX21vbnRoOiBudWxsLFxuXHRcdGNfZGF5OiBudWxsLFxuXHRcdHNfY291bnQ6IDEsXG5cdFx0bG9nX2NvdW50OiAwLFxuXG5cdFx0dHJhbnNhY3Rpb25sb2dzOiBbXSxcblx0XHRjdXJyZW50X3llYXIyOiBudWxsLFxuXHRcdGN1cnJlbnRfbW9udGgyOiBudWxsLFxuXHRcdGN1cnJlbnRfZGF5MjogbnVsbCxcblx0XHRjX3llYXIyOiBudWxsLFxuXHRcdGNfbW9udGgyOiBudWxsLFxuXHRcdGNfZGF5MjogbnVsbCxcblx0XHRzX2NvdW50MjogMSxcblxuXHRcdHByb2R1Y3RzOiBbXSxcblx0XHRhY3RpdmVwcm9kczogW10sXG5cdFx0c3RvcmVzOiBbXSxcblx0XHRhY3RpdmVzdG9yZXM6IFtdLFxuXHRcdG9yZGVyczogW10sXG5cdFx0Y29tcG9yZGVyczpbXSxcblx0XHRvcmRlcl9wcm9kczogW10sXG5cdFx0ZXRyYW5zYWN0aW9uczogW10sXG5cdFx0ZG9jdW1lbnRzOiBbXSxcblx0XHRkb2N1bWVudF9sb2dzOltdLFxuXHRcdHNhbGVzOiBbXSxcblxuXHRcdG9yX251bTogJycsXG5cdFx0cmN2cl9uYW1lOiAnJyxcblx0XHRyY3ZyX2NvbnRhY3Q6ICcnLFxuXHRcdHJjdnJfb3JkZXJEYXRlOiAnJyxcblx0XHR0b3RhbF9wcmljZSA6ICcwJyxcblx0XHR0YXggOiAnMCcsXG5cdFx0c2hpcHBpbmdfZmVlOiAnMCcsXG5cdFx0c3VidG90YWw6ICcwJyxcblxuXHRcdGN1cnJlbnRTaG9wcGluZ1VzZXI6JycsXG5cblx0XHRjb3VudFNvbGQ6MCxcblxuXHRcdGRlbFBhY2s6IG5ldyBGb3JtKHtcbiAgICAgICAgICAgICdyZWFzb24nOiBcIlwiLFxuICAgICAgICAgICAgJ3RyYWNraW5nJzpcIlwiXG4gICAgICAgIH0seyBiYXNlVVJMICA6ICcvdmlzYS9jbGllbnQvJ30pLFxuXG4gICAgICAgIGZ1bmRQYWNrOiBuZXcgRm9ybSh7XG4gICAgICAgICAgICAndHlwZSc6IFwiXCIsXG4gICAgICAgICAgICAncmVhc29uJzogXCJcIixcbiAgICAgICAgICAgICdhbW91bnQnOlwiXCIsXG4gICAgICAgICAgICAndHJhY2tpbmcnOlwiXCIsXG4gICAgICAgICAgICAnY2xpZW50X2lkJzpcIlwiLFxuICAgICAgICAgICAgJ3NlbGVjdGVkX2NsaWVudCc6XCJcIixcbiAgICAgICAgICAgICdhdXRob3JpemVyJzpcIlwiLFxuICAgICAgICAgICAgJ2F1dGhfbmFtZSc6XCJcIlxuICAgICAgICB9LHsgYmFzZVVSTCAgOiAnL3Zpc2EvY2xpZW50Lyd9KSxcblxuXHRcdGVkaXRTZXJ2OiBuZXcgRm9ybSh7XG4gICAgICAgICAgICAnY29zdCc6IDAsXG4gICAgICAgICAgICAndGlwJzogMCxcbiAgICAgICAgICAgICdzdGF0dXMnOlwiXCIsXG4gICAgICAgICAgICAnYWN0aXZlJzowLFxuICAgICAgICAgICAgJ2Rpc2NvdW50JzowLFxuICAgICAgICAgICAgJ3JlYXNvbic6IFwiXCIsXG4gICAgICAgICAgICAnbm90ZSc6IFwiXCIsXG4gICAgICAgICAgICAnaWQnIDogbnVsbCxcbiAgICAgICAgICAgICdyZXBvcnRpbmcnIDogJycsXG4gICAgICAgICAgICAnZXh0ZW5kJzonJyxcbiAgICAgICAgICAgICdyY3ZfZG9jcyc6JydcbiAgICAgICAgfSx7IGJhc2VVUkwgIDogJy92aXNhL2NsaWVudC8nfSksXG5cblx0ICAgIGNwYW5lbF91cmwgOiB3aW5kb3cuTGFyYXZlbC5iYXNlX3VybCxcblx0ICAgIGRhdGVfbm93OiBuZXcgRGF0ZSgpLFxuXHQgICAgZG9jdHlwZTonJyxcblxuICAgICAgICB2YWxpZGl0eTogbmV3IEZvcm0oe1xuICAgICAgICAgICAgJ2V4cGlyZXNfYXQnIDogXCJcIixcbiAgICAgICAgICAgICdzdGF0dXMnOlwiXCIsXG5cdCAgICAgICAgICAgICdpZCc6XCJcIixcblx0ICAgICAgICAgICAgJ21vbnRoJzpcIlwiLFxuICAgICAgICAgICAgJ2RheSc6XCJcIlxuICAgICAgICB9LHsgYmFzZVVSTCAgOiAnL3Zpc2EvY2xpZW50J30pLFxuXG4gICAgICAgIHF1aWNrUmVwb3J0Q2xpZW50c0lkOiBbXSxcblx0XHRxdWlja1JlcG9ydENsaWVudFNlcnZpY2VzSWQ6IFtdLFxuXHRcdHF1aWNrUmVwb3J0VHJhY2tpbmdzOiBbXSxcblxuXHRcdGR0OicnLFxuXG5cdFx0YXV0aG9yaXplckZybTogbmV3IEZvcm0oe1xuICAgICAgICAgICAgJ2F1dGhvcml6ZXInOiAnJyxcbiAgICAgICAgICAgICdwYXNzd29yZCc6ICcnLFxuICAgICAgICB9LHsgYmFzZVVSTCAgOiAnL3Zpc2EvY2xpZW50Lyd9KSxcblxuICAgICAgICBzZXR0aW5nOiB1c2VyLmFjY2Vzc19jb250cm9sWzBdLnNldHRpbmcsXG4gICAgICAgIHBlcm1pc3Npb25zOiB1c2VyLnBlcm1pc3Npb25zLFxuICAgICAgICBzZWxwZXJtaXNzaW9uczogW3tcbiAgICAgICAgICAgIGVkaXQ6MCxcblx0XHRcdGFjdGlvbkxvZ3M6MCxcblx0XHRcdHRyYW5zYWN0aW9uTG9nczowLFxuXHRcdFx0ZG9jdW1lbnRzOjAsXG5cdFx0XHRhY2Nlc3NDb250cm9sOjAsXG5cdFx0XHRhZGRTZXJ2aWNlOjAsXG5cdFx0XHRhZGRGdW5kczowLFxuXHRcdFx0ZGVsZXRlVGhpc1BhY2thZ2U6MCxcblx0XHRcdGVkaXRTZXJ2aWNlOjAsXG5cdFx0XHRhZGRRdWlja1JlcG9ydDowXG4gICAgICAgIH1dLFxuXG4gICAgICAgIGZpbGVzOiBbXSxcbiAgICAgICAgLy9kb2NyY3Y6IFtdLFxuXG5cdH0sXG5cblx0Y3JlYXRlZCgpe1xuXHRcdC8vY29uc29sZS5sb2codGhpcy5pc19hdXRoKTtcblx0XHRcblx0XHRsZXQgdXNlcl9pZCA9IGRhdGEuaWQ7IC8vIGNoYW5nZSB0byBzZXNzaW9uIGRhdGFcblx0XHR0aGlzLmN1cnJlbnRTaG9wcGluZ1VzZXIgPSB1c2VyX2lkO1xuXG5cdFx0XG5cdFx0ZnVuY3Rpb24gZ2V0U2VydmljZUNvc3QoKSB7XG5cdFx0XHRyZXR1cm4gYXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvY2xpZW50U2VydmljZUNvc3QvJyt1c2VyX2lkKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXREZXBvc2l0KCkge1xuXHRcdFx0cmV0dXJuIGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudERlcG9zaXQvJyt1c2VyX2lkKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRQYXltZW50KCkge1xuXHRcdFx0cmV0dXJuIGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudFBheW1lbnQvJyt1c2VyX2lkKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRSZWZ1bmQoKSB7XG5cdFx0XHRyZXR1cm4gYXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvY2xpZW50UmVmdW5kLycrdXNlcl9pZCk7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gZ2V0RGlzY291bnQoKSB7XG5cdFx0XHRyZXR1cm4gYXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvY2xpZW50RGlzY291bnQvJyt1c2VyX2lkKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRCYWxhbmNlKCkge1xuXHRcdFx0cmV0dXJuIGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudEJhbGFuY2UvJyt1c2VyX2lkKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRUcmFuc2ZlcigpIHtcblx0XHRcdHJldHVybiBheGlvcy5nZXQoJy92aXNhL2NsaWVudC9jbGllbnRUcmFuc2Zlci8nK3VzZXJfaWQpO1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIGdldFBhY2thZ2VzKCkge1xuXHRcdFx0cmV0dXJuIGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudFBhY2thZ2VzLycrdXNlcl9pZCk7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gZ2V0U2VydmljZXModHJhY2tpbmcpIHtcblx0XHRcdHJldHVybiBheGlvcy5nZXQoJy92aXNhL2NsaWVudC9jbGllbnRTZXJ2aWNlcy8nK3VzZXJfaWQrJy8nK3RyYWNraW5nKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRBY3Rpb25Mb2dzKCkge1xuXHRcdFx0cmV0dXJuIGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudEFjdGlvbkxvZ3MvJyt1c2VyX2lkKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXRUcmFuc2FjdGlvbkxvZ3MoKSB7XG5cdFx0XHRyZXR1cm4gYXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvY2xpZW50VHJhbnNhY3Rpb25Mb2dzLycrdXNlcl9pZCk7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gZ2V0RG9jdW1lbnRzKCl7XG5cdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy91c2Vycy8nK3VzZXJfaWQrJy9kb2N1bWVudHMnKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBnZXREb2N1bWVudExvZ3MoKXtcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2RvY2xvZz91c2VyPScrdXNlcl9pZCk7XG5cdFx0fVxuXG5cdFx0Ly8gZnVuY3Rpb24gZ2V0QXV0aG9yaXplcnMoKXtcblx0XHQvLyBcdHJldHVybiBheGlvcy5nZXQoJy92aXNhL2NsaWVudC9nZXQtYXV0aG9yaXplcnMnKTtcblx0XHQvLyB9XG5cblx0XHRmdW5jdGlvbiBnZXRGaWxlcygpe1xuXHRcdFx0bGV0IGNsaWVudElkID0gd2luZG93LkxhcmF2ZWwuZGF0YS5pZDtcblx0XHRcdHJldHVybiBheGlvcy5nZXQoJy92aXNhL3NlcnZpY2UtbWFuYWdlci9jbGllbnQtZG9jdW1lbnRzL2NsaWVudC1pZC8nKyBjbGllbnRJZCk7XG5cdFx0fVxuXG5cdFx0Ly8gZnVuY3Rpb24gZ2V0RG9jdW1lbnRSZWNlaXZlKCl7XG5cdFx0Ly8gXHRsZXQgY2xpZW50SWQgPSB3aW5kb3cuTGFyYXZlbC5kYXRhLmlkO1xuXHRcdC8vIFx0cmV0dXJuIGF4aW9zLmdldCgnL3Zpc2Evc2VydmljZS1tYW5hZ2VyL2NsaWVudC1kb2N1bWVudHMvcmVjZWl2ZWQvJysgY2xpZW50SWQpO1xuXHRcdC8vIH1cblxuICAgIFx0YXhpb3MuYWxsKFtcblx0XHRcdGdldFNlcnZpY2VDb3N0KClcblx0XHQgICAgLGdldERlcG9zaXQoKVxuXHRcdCAgICAsZ2V0UGF5bWVudCgpXG5cdFx0ICAgICxnZXRSZWZ1bmQoKVxuXHRcdCAgICAsZ2V0RGlzY291bnQoKVxuXHRcdCAgICAsZ2V0QmFsYW5jZSgpXG5cdFx0ICAgICxnZXRUcmFuc2ZlcigpXG5cdFx0ICAgICxnZXRQYWNrYWdlcygpXG5cdFx0ICAgICxnZXRTZXJ2aWNlcygwKVxuXHRcdCAgICAsZ2V0QWN0aW9uTG9ncygpXG5cdFx0ICAgICxnZXRUcmFuc2FjdGlvbkxvZ3MoKVxuXHRcdCAgICAsZ2V0RG9jdW1lbnRzKClcblx0XHQgICAgLGdldERvY3VtZW50TG9ncygpXG5cdFx0ICAgIC8vICxnZXRBdXRob3JpemVycygpXG5cdFx0ICAgICxnZXRGaWxlcygpXG5cdFx0ICAgLy8gLGdldERvY3VtZW50UmVjZWl2ZSgpXG5cdFx0XSkudGhlbihheGlvcy5zcHJlYWQoXG5cdFx0XHQoXG5cdFx0XHRcdHNlcnZpY2VfY29zdCxcblx0XHRcdFx0ZGVwb3NpdCxcblx0XHRcdFx0cGF5bWVudCxcblx0XHRcdFx0cmVmdW5kLFxuXHRcdFx0XHRkaXNjb3VudCxcblx0XHRcdFx0YmFsYW5jZSxcblx0XHRcdFx0dHJhbnNmZXIsXG5cdFx0XHRcdHBhY2thZ2VzLFxuXHRcdFx0XHRzZXJ2aWNlcyxcblx0XHRcdFx0YWN0aW9ubG9ncyxcblx0XHRcdFx0dHJhbnNhY3Rpb25sb2dzLFxuXHRcdFx0XHRkb2NzLFxuXHRcdFx0XHRkb2Nsb2dzLFxuXHRcdFx0XHQvLyBhdXRob3JzLFxuXHRcdFx0XHRjbGllbnRmaWxlcyxcblx0XHRcdFx0Ly9kb2NyY3YsXG5cdFx0XHQpID0+IHtcblx0XHRcdHRoaXMuc2VydmljZV9jb3N0IFx0PSBzZXJ2aWNlX2Nvc3QuZGF0YTtcblx0XHRcdHRoaXMuZGVwb3NpdCBcdFx0PSBkZXBvc2l0LmRhdGE7XG5cdFx0XHR0aGlzLnBheW1lbnQgXHRcdD0gcGF5bWVudC5kYXRhO1xuXHRcdFx0dGhpcy5yZWZ1bmQgXHRcdD0gcmVmdW5kLmRhdGE7XG5cdFx0XHR0aGlzLmRpc2NvdW50IFx0XHQ9IGRpc2NvdW50LmRhdGE7XG5cdFx0XHR0aGlzLmJhbGFuY2UgXHRcdD0gYmFsYW5jZS5kYXRhO1xuXHRcdFx0dGhpcy50cmFuc2ZlciBcdFx0PSB0cmFuc2Zlci5kYXRhO1xuXHRcdFx0dGhpcy5wYWNrYWdlcyBcdFx0PSBwYWNrYWdlcy5kYXRhO1xuXHRcdFx0dGhpcy5zZXJ2aWNlcyBcdFx0PSBzZXJ2aWNlcy5kYXRhLnNlcnZpY2VzO1xuXHRcdFx0dGhpcy5wYWNrYWdlX2RhdGUgXHRcdD0gc2VydmljZXMuZGF0YS5wYWNrYWdlX2RhdGU7XG5cdFx0XHR0aGlzLnBhY2thZ2VfY29zdCBcdFx0PSBzZXJ2aWNlcy5kYXRhLnBhY2thZ2VfY29zdDtcblx0XHRcdHRoaXMucGFja2FnZV9kZXBvc2l0IFx0XHQ9IHNlcnZpY2VzLmRhdGEucGFja2FnZV9kZXBvc2l0O1xuXHRcdFx0dGhpcy5wYWNrYWdlX3BheW1lbnQgXHRcdD0gc2VydmljZXMuZGF0YS5wYWNrYWdlX3BheW1lbnQ7XG5cdFx0XHR0aGlzLnBhY2thZ2VfcmVmdW5kIFx0XHQ9IHNlcnZpY2VzLmRhdGEucGFja2FnZV9yZWZ1bmQ7XG5cdFx0XHR0aGlzLnBhY2thZ2VfZGlzY291bnQgXHRcdD0gc2VydmljZXMuZGF0YS5wYWNrYWdlX2Rpc2NvdW50O1xuXHRcdFx0dGhpcy5wYWNrYWdlX2JhbGFuY2UgXHRcdD0gc2VydmljZXMuZGF0YS5wYWNrYWdlX2JhbGFuY2U7XG5cdFx0XHR0aGlzLmFjdGlvbmxvZ3MgXHQ9IGFjdGlvbmxvZ3MuZGF0YTtcblx0XHRcdHRoaXMudHJhbnNhY3Rpb25sb2dzID0gdHJhbnNhY3Rpb25sb2dzLmRhdGE7XG5cdFx0XHR0aGlzLmRvY3VtZW50cyBcdFx0PSBkb2NzLmRhdGE7XG5cdFx0XHR0aGlzLmRvY3VtZW50X2xvZ3MgXHQ9IGRvY2xvZ3MuZGF0YTtcblx0XHRcdC8vIHRoaXMuYXV0aG9yaXplcnMgXHQ9IGF1dGhvcnMuZGF0YTtcblx0XHRcdHRoaXMuZmlsZXMgXHQ9IGNsaWVudGZpbGVzLmRhdGE7XG5cdFx0XHQvL3RoaXMuZG9jcmN2IFx0PSBkb2NyY3YuZGF0YTtcblxuXHRcdH0pKVxuXHRcdC5jYXRjaCgkLm5vb3ApO1xuICAgICAgICAvL3RoaXMuZ2V0RmlsZXMoKTtcblxuXHR9LFxuXHRtZXRob2RzOntcblx0XHQvLyBnZXRBdXRob3JpemVycygpe1xuXHRcdC8vIFx0YXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvZ2V0LWF1dGhvcml6ZXJzJylcblx0XHQvLyBcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdC8vIFx0XHRcdHRoaXMuYXV0aG9yaXplcnMgPSByZXNwb25zZS5kYXRhO1xuXHRcdC8vIFx0XHR9KVxuXHRcdC8vIH0sXG5cblx0XHRmZXRjaENsaWVudHMocSkge1xuXHRcdFx0aWYocSA9PSBkYXRhLmlkKXtcblx0XHRcdFx0JCgnLnBvcG92ZXIgI2Z1bmRzZXQnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7XG4gICAgICAgICAgICBcdHRvYXN0ci5lcnJvcihcIkNhbm5vdCB0cmFuc2ZlciB0byBzYW1lIGNsaWVudCBpZC5cIik7XG5cdFx0XHR9XG4gICAgXHRcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9jbGllbnQvZ2V0LXZpc2EtY2xpZW50cycsIHtcblx0XHRcdCBcdHBhcmFtczoge1xuXHRcdFx0IFx0XHRxOiBxXG5cdFx0XHQgXHR9XG5cdFx0XHR9KVxuXHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRpZihyZXNwb25zZS5kYXRhLmxlbmd0aCA+IDApe1xuXHRcdFx0XHRcdHRoaXMuY2xpZW50cy5wdXNoKHtcblx0XHQgICAgICAgICAgICAgICBpZDpyZXNwb25zZS5kYXRhWzBdLmlkLFxuXHRcdCAgICAgICAgICAgICAgIG5hbWU6cmVzcG9uc2UuZGF0YVswXS5uYW1lIH0pO1xuXHRcdFx0XHR9XG5cblx0XHQgXHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdHZhciBteV92YWwgPSAkKFwiLmNob3Nlbi1zZWxlY3QtZm9yLWNsaWVudHNcIikudmFsKCk7XG5cdFx0XHRcdFx0JChcIi5jaG9zZW4tc2VsZWN0LWZvci1jbGllbnRzXCIpLnZhbChteV92YWwpLnRyaWdnZXIoXCJjaG9zZW46dXBkYXRlZFwiKTtcblx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHR9KTtcblx0XHR9LFxuXG5cdFx0Z2V0RmlsZXMoKSB7XG5cdFx0XHRsZXQgY2xpZW50SWQgPSB3aW5kb3cuTGFyYXZlbC5kYXRhLmlkO1xuXG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL3NlcnZpY2UtbWFuYWdlci9jbGllbnQtZG9jdW1lbnRzL2NsaWVudC1pZC8nKyBjbGllbnRJZClcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHRoaXMuZmlsZXMgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHR9KTtcblx0XHR9LFxuXG5cdFx0c2F2ZUF1dGhvcml6ZXIoKXtcblx0XHRcdHRoaXMuYXV0aG9yaXplckZybS5zdWJtaXQoJ3Bvc3QnLCcvc2F2ZUF1dGhvcml6ZXInKVx0XG5cdFx0XHQudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0XHRpZihyZXN1bHQuc3RhdHVzID09ICdmYWlsZWQnKXtcblx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1dyb25nIFBhc3N3b3JkJyk7XG5cdFx0XHRcdH1lbHNlIGlmKHJlc3VsdC5zdGF0dXMgPT0gJ3N1Y2Nlc3MnKXtcblx0XHRcdFx0XHR0aGlzLmZ1bmRQYWNrLmF1dGhvcml6ZXIgPSB0aGlzLmF1dGhvcml6ZXJGcm0uYXV0aG9yaXplci5pZDtcblx0XHRcdFx0XHR0aGlzLmZ1bmRQYWNrLmF1dGhfbmFtZSA9IHRoaXMuYXV0aG9yaXplckZybS5hdXRob3JpemVyLmZ1bGxuYW1lO1xuXHRcdFx0XHRcdHRoaXMuZnVuZFBhY2suc3VibWl0KCdwb3N0JywnL2NsaWVudEFkZEZ1bmRzJylcblx0ICAgIFx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICBcdFx0XHRcdHRoaXMucmVsb2FkQ29tcHV0YXRpb24oKTtcblx0ICAgIFx0XHRcdFx0dGhpcy5zaG93U2VydmljZXNVbmRlcihkYXRhLmlkLCB0aGlzLnRyYWNraW5nX3NlbGVjdGVkKTtcblx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBzYXZlZC4nKTtcblx0XHQgICAgICAgICAgICB9KVxuXHRcdCAgICAgICAgICAgIC5jYXRjaChlcnJvciA9Pntcblx0XHQgICAgICAgICAgICBcdHRvYXN0ci5lcnJvcignRmFpbGVkJyk7XG5cdFx0ICAgICAgICAgICAgfSk7XG5cdFx0XHQgICAgXHQkKCcjYXV0aG9yaXplck1vZGFsJykubW9kYWwoJ3RvZ2dsZScpO1xuIFx0XHRcdFx0fVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PntcbiAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKCdGYWlsZWQnKTtcbiAgICAgICAgICAgIH0pO1xuXHRcdH0sXG5cdFx0Z2V0RGF0ZSgpe1xuXHQgICAgXHR0aGlzLmR0ID0gJCgnZm9ybSNkYWlseS1mb3JtIGlucHV0W25hbWU9ZGF0ZV0nKS52YWwoKTtcblx0ICAgIFx0dGhpcy5lZGl0U2Vydi5leHRlbmQgPSB0aGlzLmR0O1xuXHRcdH0sXG5cdFx0c2hvd1NlcnZpY2VzVW5kZXIoY2xpZW50X2lkLHRyYWNraW5nKXtcblx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudFNlcnZpY2VzLycrY2xpZW50X2lkKycvJyt0cmFja2luZylcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHRoaXMuc2VydmljZXMgPSByZXNwb25zZS5kYXRhLnNlcnZpY2VzO1xuXHRcdFx0XHRcdHRoaXMucGFja2FnZV9kYXRlIFx0XHQ9IHJlc3BvbnNlLmRhdGEucGFja2FnZV9kYXRlO1xuXHRcdFx0XHRcdHRoaXMucGFja2FnZV9jb3N0IFx0XHQ9IHJlc3BvbnNlLmRhdGEucGFja2FnZV9jb3N0O1xuXHRcdFx0XHRcdHRoaXMucGFja2FnZV9kZXBvc2l0IFx0XHQ9IHJlc3BvbnNlLmRhdGEucGFja2FnZV9kZXBvc2l0O1xuXHRcdFx0XHRcdHRoaXMucGFja2FnZV9wYXltZW50IFx0XHQ9IHJlc3BvbnNlLmRhdGEucGFja2FnZV9wYXltZW50O1xuXHRcdFx0XHRcdHRoaXMucGFja2FnZV9yZWZ1bmQgXHRcdD0gcmVzcG9uc2UuZGF0YS5wYWNrYWdlX3JlZnVuZDtcblx0XHRcdFx0XHR0aGlzLnBhY2thZ2VfZGlzY291bnQgXHRcdD0gcmVzcG9uc2UuZGF0YS5wYWNrYWdlX2Rpc2NvdW50O1xuXHRcdFx0XHRcdHRoaXMucGFja2FnZV9iYWxhbmNlIFx0XHQ9IHJlc3BvbnNlLmRhdGEucGFja2FnZV9iYWxhbmNlO1xuXHRcdFx0XHRcdHRoaXMudHJhY2tpbmdfc2VsZWN0ZWQgPSB0cmFja2luZztcblx0XHRcdFx0XHR0aGlzLmdyb3VwX3BhY2thZ2UgPSBmYWxzZTtcblx0XHRcdFx0XHRpZih0cmFja2luZy5zbGljZSgwLDEpID09ICdHJyl7XG5cdFx0XHRcdFx0XHR0aGlzLmdyb3VwX3BhY2thZ2UgPSB0cnVlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR0aGlzLnRyYWNraW5nX3N0YXR1cyAgID0gcmVzcG9uc2UuZGF0YS5zdGF0dXM7XG5cdFx0XHRcdFx0dGhpcy5wb3BPdmVyKCk7XG5cdFx0XHRcdH0pXG4gICAgICAgIH0sXG5cbiAgICAgICBzaG93QWxsU2VydmljZXMoKXtcbiAgICAgICBcdFx0dmFyIGNsaWVudF9pZCA9IGRhdGEuaWQ7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2NsaWVudC9jbGllbnRTZXJ2aWNlcy8nK2NsaWVudF9pZCsnLycrMClcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHRoaXMuc2VydmljZXMgPSByZXNwb25zZS5kYXRhLnNlcnZpY2VzO1xuXHRcdFx0XHRcdHRoaXMucGFja2FnZV9kYXRlICAgICAgICAgPSByZXNwb25zZS5kYXRhLnBhY2thZ2VfZGF0ZTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wYWNrYWdlX2Nvc3QgICAgICAgICA9IHJlc3BvbnNlLmRhdGEucGFja2FnZV9jb3N0O1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnBhY2thZ2VfZGVwb3NpdCAgICAgICAgID0gcmVzcG9uc2UuZGF0YS5wYWNrYWdlX2RlcG9zaXQ7XG5cdFx0XHRcdFx0dGhpcy5wYWNrYWdlX3BheW1lbnQgXHRcdD0gcmVzcG9uc2UuZGF0YS5wYWNrYWdlX3BheW1lbnQ7XG5cdFx0XHRcdFx0dGhpcy5wYWNrYWdlX3JlZnVuZCBcdFx0PSByZXNwb25zZS5kYXRhLnBhY2thZ2VfcmVmdW5kO1xuXHRcdFx0XHRcdHRoaXMucGFja2FnZV9kaXNjb3VudCBcdFx0PSByZXNwb25zZS5kYXRhLnBhY2thZ2VfZGlzY291bnQ7XG5cdFx0XHRcdFx0dGhpcy5wYWNrYWdlX2JhbGFuY2UgXHRcdD0gcmVzcG9uc2UuZGF0YS5wYWNrYWdlX2JhbGFuY2U7XG5cdFx0XHRcdFx0dGhpcy50cmFja2luZ19zZWxlY3RlZCA9IG51bGw7XG5cdFx0XHRcdFx0dGhpcy5ncm91cF9wYWNrYWdlID0gZmFsc2U7XG5cdFx0XHRcdFx0dGhpcy50cmFja2luZ19zdGF0dXMgPSBudWxsO1xuXHRcdFx0XHR9KVxuICAgICAgICB9LFxuXG4gICAgICAgIHVwZGF0ZVRyYWNraW5nTGlzdChjbGllbnRfaWQpe1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvY2xpZW50UGFja2FnZXMvJytjbGllbnRfaWQpXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHR0aGlzLnBhY2thZ2VzID0gbnVsbDtcblx0XHRcdFx0XHR0aGlzLnBhY2thZ2VzID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0fSlcbiAgICAgICAgfSxcblxuICAgICAgICBhZGROZXdQYWNrYWdlKCl7XG4gICAgICAgXHRcdHZhciBjbGllbnRfaWQgPSBkYXRhLmlkO1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvY2xpZW50QWRkUGFja2FnZS8nK2NsaWVudF9pZClcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHZhciB0cmFjayA9IHJlc3BvbnNlLmRhdGEudHJhY2tpbmc7XG5cdFx0XHRcdFx0dGhpcy5zaG93U2VydmljZXNVbmRlcihjbGllbnRfaWQsdHJhY2spO1xuXHRcdFx0XHRcdHRoaXMudXBkYXRlVHJhY2tpbmdMaXN0KGNsaWVudF9pZCk7XG5cdFx0XHRcdFx0dmFyIG93bCA9ICQoXCIjb3dsLWRlbW9cIik7XG5cdFx0XHRcdFx0b3dsLmRhdGEoJ293bENhcm91c2VsJykuZGVzdHJveSgpO1xuXHRcdFx0XHR9KVxuICAgICAgICB9LFxuXG4gICAgICAgIGRlbGV0ZVBhY2thZ2UocmVhc29uKXtcblx0XHRcdGxldCBjbGllbnRfaWQgPSBkYXRhLmlkOyBcbiAgICAgICAgICAgICQoXCIuZGVsZXRlLXBhY2thZ2VcIikucG9wb3ZlcignaGlkZScpO1xuICAgICAgICAgICAgdGhpcy5kZWxQYWNrLnRyYWNraW5nID0gdGhpcy50cmFja2luZ19zZWxlY3RlZDtcbiAgICAgICAgICAgIGlmKHJlYXNvbiA9PSAnJyl7XG4gICAgICAgICAgICBcdCQoJy5wb3BvdmVyICN2YWxpZHNldCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoXCJQbGVhc2UgaW5kaWNhdGUgcmVhc29uIHRvIGRlbGV0ZS5cIik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgXHR0aGlzLmRlbFBhY2sucmVhc29uID0gcmVhc29uO1xuICAgICAgICAgICAgXHR0aGlzLmRlbFBhY2suc3VibWl0KCdwb3N0JywnL2NsaWVudERlbGV0ZVBhY2thZ2UnKVxuICAgICAgICAgICAgXHQudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICBcdFx0aWYocmVzcG9uc2Uuc3RhdHVzID09ICdzdWNjZXNzJykge1xuXHRcdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xuXHRcdCAgICAgICAgICAgICAgICB0aGlzLnNob3dBbGxTZXJ2aWNlcygpO1xuXHRcdCAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVRyYWNraW5nTGlzdChjbGllbnRfaWQpO1xuXHRcdCAgICAgICAgICAgICAgICB2YXIgb3dsID0gJChcIiNvd2wtZGVtb1wiKTtcblx0XHRcdFx0XHRcdG93bC5kYXRhKCdvd2xDYXJvdXNlbCcpLmRlc3Ryb3koKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZXtcblx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHJlc3BvbnNlLmxvZyk7XG5cdFx0XHRcdFx0fVxuXHQgICAgICAgICAgICB9KVxuXHQgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT57XG5cdCAgICAgICAgICAgICAgICBpZihlcnJvci5yZWFzb24pe1xuXHQgICAgICAgICAgICAgICAgICAgICQoJy5wb3BvdmVyICN2YWxpZHNldCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcblx0ICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoXCJQbGVhc2UgaW5kaWNhdGUgcmVhc29uIHRvIGRlbGV0ZS5cIik7XG5cdCAgICAgICAgICAgICAgICB9ZWxzZXtcblx0ICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignVXBkYXRlIGZhaWxlZC4nKTtcblx0ICAgICAgICAgICAgICAgIH1cblx0ICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgbG9hZERlcG9zaXQoKSB7XG5cdFx0XHRsZXQgY2xpZW50X2lkID0gZGF0YS5pZDsgXG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2NsaWVudC9jbGllbnREZXBvc2l0LycrY2xpZW50X2lkKVxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0dGhpcy5kZXBvc2l0ID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0fSlcblx0XHR9LFxuXG5cdFx0bG9hZFJlZnVuZCgpIHtcblx0XHRcdGxldCBjbGllbnRfaWQgPSBkYXRhLmlkOyBcblx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudFJlZnVuZC8nK2NsaWVudF9pZClcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHRoaXMucmVmdW5kID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0fSlcblx0XHR9LFxuXG5cdFx0bG9hZFBheW1lbnQoKSB7XG5cdFx0XHRsZXQgY2xpZW50X2lkID0gZGF0YS5pZDsgXG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2NsaWVudC9jbGllbnRQYXltZW50LycrY2xpZW50X2lkKVxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0dGhpcy5wYXltZW50ID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0fSlcblx0XHR9LFxuXG5cdFx0bG9hZERpc2NvdW50KCkge1xuXHRcdFx0bGV0IGNsaWVudF9pZCA9IGRhdGEuaWQ7IFxuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvY2xpZW50RGlzY291bnQvJytjbGllbnRfaWQpXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHR0aGlzLmRpc2NvdW50ID0gcmVzcG9uc2UuZGF0YTtcblxuXHRcdFx0XHR9KVxuXHRcdH0sXG5cblx0XHRsb2FkQmFsYW5jZSgpIHtcblx0XHRcdGxldCBjbGllbnRfaWQgPSBkYXRhLmlkOyBcblx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudEJhbGFuY2UvJytjbGllbnRfaWQpXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHR0aGlzLmJhbGFuY2UgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHR9KVxuXHRcdH0sXG5cblx0XHRsb2FkVHJhbnNmZXIoKSB7XG5cdFx0XHRsZXQgY2xpZW50X2lkID0gZGF0YS5pZDsgXG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2NsaWVudC9jbGllbnRUcmFuc2Zlci8nK2NsaWVudF9pZClcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHRoaXMudHJhbnNmZXIgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHR9KVxuXHRcdH0sXG5cblx0XHRlZGl0U2VydmljZShzZXJ2aWNlX2lkKSB7XG5cdFx0XHR0aGlzLmVkaXRTZXJ2LmlkID0gc2VydmljZV9pZDtcblx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L2NsaWVudEdldFNlcnZpY2UvJytzZXJ2aWNlX2lkKVxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2Vydi5jb3N0ID0gcmVzcG9uc2UuZGF0YS5jb3N0O1xuXHRcdFx0XHRcdHRoaXMuZWRpdFNlcnYudGlwID0gcmVzcG9uc2UuZGF0YS50aXA7XG5cdFx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YS5kZWxldGVkX2F0ICE9IG51bGwpIHtcblx0XHRcdFx0XHRcdHRoaXMuZWRpdFNlcnYuZGlzY291bnQgPSAnJztcblx0XHRcdFx0XHRcdHRoaXMuZWRpdFNlcnYucmVhc29uID0gJyc7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMuZWRpdFNlcnYuZGlzY291bnQgPSByZXNwb25zZS5kYXRhLmRpc2NvdW50X2Ftb3VudDtcblx0XHRcdFx0XHRcdHRoaXMuZWRpdFNlcnYucmVhc29uID0gcmVzcG9uc2UuZGF0YS5yZWFzb247XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHRoaXMuZWRpdFNlcnYuc3RhdHVzID0gcmVzcG9uc2UuZGF0YS5zdGF0dXM7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2Vydi5hY3RpdmUgPSByZXNwb25zZS5kYXRhLmFjdGl2ZTtcblx0XHRcdFx0XHR0aGlzLmVkaXRTZXJ2LmV4dGVuZCA9IHJlc3BvbnNlLmRhdGEuZXh0ZW5kO1xuXHRcdFx0XHRcdC8vIHRoaXMuZWRpdFNlcnYuZXh0ZW5kID0gcmVzcG9uc2UuZGF0YS5yZW1hcmtzO1xuXHRcdFx0XHR9KTtcblx0XHQgICAgJCgnI2VkaXQtc2VydmljZS1tb2RhbCcpLm1vZGFsKCdzaG93Jyk7XG5cdFx0fSxcblxuICAgICAgICBhZGRGdW5kcyh0eXBlLGFtb3VudCxyZWFzb24pe1xuXHRcdFx0Ly8gdGhpcy5hdXRob3JpemF0aW9uKCk7XG5cdFx0ICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuaXNfYXV0aCk7XG5cbiAgICAgICAgICAgIGlmICgodHlwZSA9PSAncmVmdW5kJyB8fCB0eXBlID09ICdkaXNjb3VudCcgfHwgdHlwZSA9PSAndHJhbnNmZXInKSYmIHJlYXNvbi50cmltKCkgPT0gJycpIHtcblx0XHRcdFx0XHQkKCcucG9wb3ZlciAjZnVuZHNldCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcbiAgICAgICAgICAgIFx0XHR0b2FzdHIuZXJyb3IoXCJQbGVhc2UgaW5wdXQgcmVhc29uLlwiKTtcbiAgICAgICAgXHR9XG5cbiAgICAgICAgXHRlbHNlIGlmKGFtb3VudCA9PSAnJyl7XG4gICAgICAgICAgICBcdCQoJy5wb3BvdmVyICNmdW5kc2V0JykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpO1xuICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcihcIlBsZWFzZSBpbnB1dCBhbW91bnQgdG8gYWRkLlwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYodHlwZSA9PSAndHJhbnNmZXInICYmIHRoaXMuZnVuZFBhY2suc2VsZWN0ZWRfY2xpZW50ID09ICcnKXtcbiAgICAgICAgICAgIFx0JCgnLnBvcG92ZXIgI2Z1bmRzZXQnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7XG4gICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKFwiUGxlYXNlIHNlbGVjdCBjbGllbnQgdG8gcHJvY2VlZCB0cmFuc2Zlci5cIik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgXHR0aGlzLmZ1bmRQYWNrLnR5cGUgPSB0eXBlO1xuXHRcdFx0ICAgIHRoaXMuZnVuZFBhY2suYW1vdW50ID0gYW1vdW50O1xuXHRcdFx0ICAgIHRoaXMuZnVuZFBhY2sudHJhY2tpbmcgPSB0aGlzLnRyYWNraW5nX3NlbGVjdGVkO1xuXHRcdFx0ICAgIHRoaXMuZnVuZFBhY2suY2xpZW50X2lkID0gZGF0YS5pZDtcblx0XHQgICAgXHR0aGlzLmZ1bmRQYWNrLnJlYXNvbiA9IHJlYXNvbjtcblx0XHQgICAgXHQvLyB0aGlzLmZ1bmRQYWNrLnNlbGVjdGVkX2NsaWVudCA9IHNlbGVjdGVkX2NsaWVudDtcblxuXHRcdCAgICBcdCQoXCIuYWRkLWZ1bmRzXCIpLnBvcG92ZXIoJ2hpZGUnKTtcbiAgICAgICAgICAgIFx0XHR0aGlzLmZ1bmRQYWNrLnN1Ym1pdCgncG9zdCcsJy9jbGllbnRBZGRGdW5kcycpXG4gICAgICAgICAgICBcdFx0XHQudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgXHRcdFx0XHR0aGlzLnJlbG9hZENvbXB1dGF0aW9uKCk7XG5cbiAgICAgICAgICAgIFx0XHRcdFx0dGhpcy5zaG93U2VydmljZXNVbmRlcihkYXRhLmlkLCB0aGlzLnRyYWNraW5nX3NlbGVjdGVkKTtcbiAgICAgICAgICAgIFx0XHRcdFx0XG5cdFx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBzYXZlZC4nKTtcblx0XHRcdCAgICAgICAgICAgIH0pXG5cdFx0XHQgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT57fSk7XG5cblx0XHRcdFx0Ly9XSVRIIEFVVEhPUklaRVJcblxuXHRcdCAgICBcdC8vIC8vZGlzY291bnQgdHlwZSB0cmFuc2FjdGlvbiBuZWVkcyBhbiBhdXRob3JpemVyIHRvIHByb2NlZWQuXG5cdFx0ICAgIFx0Ly8gaWYodHlwZSA9PSAnZGlzY291bnQnKXtcblx0XHQgICAgXHQvLyBcdC8vIGZ1bmN0aW9uIHRvIGNoZWNrIGlmIHVzZXIgaXMgYW4gYXV0aG9yaXplciBvciBub3Rcblx0XHRcdCAgICAvLyBcdGlmKHRoaXMuaXNfYXV0aCl7XG5cdFx0XHQgICAgLy8gXHRcdHRoaXMuZnVuZFBhY2suYXV0aG9yaXplciA9IHRoaXMucHJvY2Vzc29yLmlkO1xuXHRcdFx0ICAgIC8vIFx0XHR0aGlzLmZ1bmRQYWNrLmF1dGhvcml6ZXIgPSB0aGlzLnByb2Nlc3Nvci5mdWxsbmFtZTtcblxuXHRcdFx0ICAgIC8vIFx0XHQkKFwiLmFkZC1mdW5kc1wiKS5wb3BvdmVyKCdoaWRlJyk7XG5cdFx0ICAgICAvLyAgICAgICAgXHRcdHRoaXMuZnVuZFBhY2suc3VibWl0KCdwb3N0JywnL2NsaWVudEFkZEZ1bmRzJylcblx0XHQgICAgIC8vICAgICAgICBcdFx0XHQudGhlbihyZXN1bHQgPT4ge1xuXHRcdCAgICAgLy8gICAgICAgIFx0XHRcdFx0dGhpcy5yZWxvYWRDb21wdXRhdGlvbigpO1xuXG5cdFx0ICAgICAvLyAgICAgICAgXHRcdFx0XHR0aGlzLnNob3dTZXJ2aWNlc1VuZGVyKGRhdGEuaWQsIHRoaXMudHJhY2tpbmdfc2VsZWN0ZWQpO1xuXHRcdCAgICAgICAgICAgIFx0XHRcdFx0XG5cdFx0XHRcdFx0ICAvLyAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKCdTdWNjZXNzZnVsbHkgc2F2ZWQuJyk7XG5cdFx0XHRcdFx0ICAvLyAgICAgICAgICAgfSlcblx0XHRcdFx0XHQgIC8vICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT57fSk7XG5cdFx0XHQgICAgLy8gXHR9ZWxzZXtcblx0XHRcdCAgICAvLyBcdFx0Ly9zaG93IHBvcCB1cCBmb3Igbm9uIGF1dGhvcml6ZXJzXG5cdFx0XHQgICAgLy8gXHRcdCQoJyNhdXRob3JpemVyTW9kYWwnKS5tb2RhbCgnc2hvdycpO1xuXHRcdFx0ICAgIC8vIFx0fVxuXHRcdCAgICBcdC8vIH1lbHNle1xuXHRcdCAgICBcdC8vIFx0JChcIi5hZGQtZnVuZHNcIikucG9wb3ZlcignaGlkZScpO1xuICAgICAgIC8vICAgICAgXHRcdHRoaXMuZnVuZFBhY2suc3VibWl0KCdwb3N0JywnL2NsaWVudEFkZEZ1bmRzJylcbiAgICAgICAvLyAgICAgIFx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgLy8gICAgICBcdFx0XHRcdHRoaXMucmVsb2FkQ29tcHV0YXRpb24oKTtcblxuICAgICAgIC8vICAgICAgXHRcdFx0XHR0aGlzLnNob3dTZXJ2aWNlc1VuZGVyKGRhdGEuaWQsIHRoaXMudHJhY2tpbmdfc2VsZWN0ZWQpO1xuICAgICAgICAgICAgXHRcdFx0XHRcblx0XHRcdCAgICAvLyAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xuXHRcdFx0ICAgIC8vICAgICAgICAgfSlcblx0XHRcdCAgICAvLyAgICAgICAgIC5jYXRjaChlcnJvciA9Pnt9KTtcblx0XHQgICAgXHQvLyB9XG5cbiAgICAgICAgICAgIH0gICAgICAgICAgICBcbiAgICAgICAgfSxcblxuICAgICAgICBhdXRob3JpemF0aW9uKCl7XG5cdFx0XHR2YXIgcm9sZXMgPSB0aGlzLnByb2Nlc3Nvci5yb2xlcztcblx0XHRcdHZhciBpc19hdXRoID0gZmFsc2U7XG5cdFx0XHRyb2xlcy5tYXAoZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRjb25zb2xlLmxvZygnSUQ6JytlLmlkKTtcblx0XHRcdFx0Y29uc29sZS5sb2coJ05BTUU6JytlLm5hbWUpO1xuXHRcdFx0ICAgIGlmKGUubmFtZSA9PSAnYXV0aG9yaXplcicpe1xuXHRcdFx0ICAgIFx0aXNfYXV0aCA9IHRydWU7XG5cdFx0XHQgICAgfVxuXHRcdFx0fSk7XG5cdFx0XHR0aGlzLmlzX2F1dGggPSBpc19hdXRoO1xuXHRcdFx0Y29uc29sZS5sb2codGhpcy5pc19hdXRoKTtcbiAgICAgICAgfSxcblxuICAgICAgICByZWxvYWRDb21wdXRhdGlvbigpe1xuICAgICAgICBcdHRoaXMubG9hZERlcG9zaXQoKTtcbiAgICAgICAgICAgIHRoaXMubG9hZERpc2NvdW50KCk7XG4gICAgICAgICAgICB0aGlzLmxvYWRSZWZ1bmQoKTtcbiAgICAgICAgICAgIHRoaXMubG9hZFBheW1lbnQoKTtcbiAgICAgICAgICAgIHRoaXMubG9hZEJhbGFuY2UoKTtcbiAgICAgICAgICAgIHRoaXMubG9hZFRyYW5zZmVyKCk7XG4gICAgICAgIH0sXG5cblx0XHRwb3BPdmVyKCl7XG5cdFx0ICAgICQoXCIuZGVsZXRlLXBhY2thZ2VcIilcbiAgICAgICAgICAgIC5wb3BvdmVyKHtcbiAgICAgICAgICAgICAgaHRtbDp0cnVlLFxuICAgICAgICAgICAgICBjb250YWluZXI6ICdib2R5JyxcbiAgICAgICAgICAgICAgY29udGVudDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiAkKCcjZGVsZXRlUGFja2FnZVBvcCcpLmh0bWwoKTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB2YXIgJGZ1bmRzUG9wT3ZlciA9ICQoXCIuYWRkLWZ1bmRzXCIpXG5cdFx0ICAgIC5wb3BvdmVyKHtcblx0XHQgICAgICBodG1sOnRydWUsXG5cdFx0ICAgICAgY29udGFpbmVyOiAnYm9keScsXG5cdFx0ICAgICAgY29udGVudDogZnVuY3Rpb24gKCkge1xuXHRcdCAgICAgICAgcmV0dXJuICQoJyNhZGRGdW5kc1BvcCcpLmh0bWwoKTtcblx0XHQgICAgICB9LFxuXHRcdCAgICB9KTtcblxuXHRcdCAgICAgICAgICAvLyBvbiBzaG93IHBvcG92ZXJcblx0IFx0XHQkZnVuZHNQb3BPdmVyLm9uKFwic2hvd24uYnMucG9wb3ZlclwiLCBmdW5jdGlvbihlKSB7XG5cdCBcdFx0XHQkKFwiYm9keVwiKS50b29sdGlwKHsgc2VsZWN0b3I6ICdbZGF0YS10b2dnbGU9dG9vbHRpcF0nIH0pO1xuXHQgXHRcdCAgICAkKCdpbnB1dC50eXBlYWhlYWQnKS50eXBlYWhlYWQoe1xuXHRcdCAgICAgICAgICAgICAgc291cmNlOiAgZnVuY3Rpb24gKHF1ZXJ5LCBwcm9jZXNzKSB7XG5cdFx0ICAgICAgICAgICAgICBcdGlmKHF1ZXJ5ID09IGRhdGEuaWQpe1xuXHRcdFx0XHRcdFx0XHQkKCcucG9wb3ZlciAjZnVuZHNldCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcblx0XHRcdCAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKFwiQ2Fubm90IHRyYW5zZmVyIHRvIHNhbWUgY2xpZW50IGlkLlwiKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGVsc2V7XG5cdFx0XHQgICAgICAgICAgICAgICAgcmV0dXJuICQuZ2V0KCcvdmlzYS9jbGllbnQvc2VhcmNoL3RyYW5zZmVyLWJhbGFuY2UnLCB7IHF1ZXJ5OiBxdWVyeSB9LCBmdW5jdGlvbiAoZGF0YSkge1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgZGF0YSA9ICQucGFyc2VKU09OKGRhdGEpO1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHByb2Nlc3MoZGF0YSk7XG5cdFx0XHQgICAgICAgICAgICAgICAgICAgIH0pO1xuXHRcdFx0ICAgICAgICAgICAgICAgIH1cblx0XHRcdCAgICAgICAgICAgIH0sXG5cdFx0ICAgICAgICAgICAgICBtaW5MZW5ndGg6IDIsXG5cdFx0ICAgICAgICAgICAgICBmaXRUb0VsZW1lbnQ6IHRydWUsXG5cdFx0ICAgICAgICAgICAgICBtYXRjaGVyOiBmdW5jdGlvbihpdGVtKSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcblx0XHQgICAgICAgICAgICAgIH0sXG5cdFx0ICAgICAgICAgICAgICB1cGRhdGVyOmZ1bmN0aW9uIChpdGVtKSB7XG5cdFx0ICAgICAgICAgICAgICBcdCAgQ2xpZW50QXBwLmZ1bmRQYWNrLnNlbGVjdGVkX2NsaWVudCA9aXRlbS5pZDtcblx0XHQgICAgICAgICAgICAgICAgICByZXR1cm4gaXRlbTtcblx0XHQgICAgICAgICAgICAgIH1cblx0XHRcdCAgICB9KTtcblx0ICAvLyAgIFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLWNsaWVudHMnKS5jaG9zZW4oe1xuXHRcdFx0Ly8gXHR3aWR0aDogXCIxMDAlXCIsXG5cdFx0XHQvLyBcdG5vX3Jlc3VsdHNfdGV4dDogXCJTZWFyY2hpbmcgQ2xpZW50ICMgOiBcIixcblx0XHRcdC8vIFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlLFxuXHRcdFx0Ly8gXHQvLyBtYXhfc2VsZWN0ZWRfb3B0aW9uczogMVxuXHRcdFx0Ly8gfSk7XG5cblx0ICAvLyAgIFx0Ly8gd2hlbiB1c2luZyBjaG9zZW4gaW5zaWRlIHBvcG92ZXIsIGNhbGwgZmlyc3QgcG9wb3ZlciBjbGFzc1xuXHRcdFx0Ly8gJCgnYm9keScpLm9uKCdrZXl1cCcsICcuY2hvc2VuLWNvbnRhaW5lciBpbnB1dFt0eXBlPXRleHRdJywgKGUpID0+IHtcblx0XHRcdC8vIFx0bGV0IHEgPSAkKCcucG9wb3ZlciAuY2hvc2VuLWNvbnRhaW5lciBpbnB1dFt0eXBlPXRleHRdJykudmFsKCk7XG5cdFx0XHQvLyBcdGlmKCBxLmxlbmd0aCA+PSA0ICl7XG5cdFx0XHQvLyBcdFx0Q2xpZW50QXBwLmZldGNoQ2xpZW50cyhxKTtcblx0XHRcdC8vIFx0fVxuXHRcdFx0Ly8gfSk7XG5cdFx0fSk7XG5cblx0XHQkZnVuZHNQb3BPdmVyLm9uKCdoaWRkZW4uYnMucG9wb3ZlcicsIGZ1bmN0aW9uKCkge1xuXHRcdCAgICAkKCcuY2hvc2VuLXNlbGVjdC1mb3ItY2xpZW50cycpLmNob3NlbignZGVzdHJveScpO1xuXHRcdH0pO1xuXG5cdFx0ICAgICQoXCIuYWRkLXZlcmlmaWNhdGlvblwiKVxuXHRcdCAgICAucG9wb3Zlcih7XG5cdFx0ICAgICAgaHRtbDp0cnVlLFxuXHRcdCAgICAgIGNvbnRhaW5lcjogJ2JvZHknLFxuXHRcdCAgICAgIGNvbnRlbnQ6IGZ1bmN0aW9uICgpIHtcblx0XHQgICAgICBcdENsaWVudEFwcC52YWxpZGl0eS5pZCA9ICQodGhpcykuYXR0cignaWQnKTtcblx0XHQgICAgICBcdENsaWVudEFwcC5kb2N0eXBlID0gJCh0aGlzKS5hdHRyKCdkYXRhLWRvY3MnKTtcblx0XHQgICAgICAgIHJldHVybiAkKCcjZnJtVmFsaWRVbnRpbCcpLmh0bWwoKTtcblx0XHQgICAgICB9LFxuXHRcdCAgICB9KTtcblx0XHR9LFxuXG5cdFx0c2F2ZVZhbGlkaXR5KGRhdGVTZWwpe1xuXG4gICAgICAgICAgICAkKFwiLnZlcmlmeVwiKS5wb3BvdmVyKCdoaWRlJyk7XG4gICAgICAgICAgICB0aGlzLnZhbGlkaXR5LnN0YXR1cyA9IFwiVmVyaWZpZWRcIjtcbiAgICAgICAgICAgIHRoaXMudmFsaWRpdHkuZXhwaXJlc19hdCA9IGRhdGVTZWw7XG4gICAgICAgICAgICB2YXIgbW9udGggPSBwYXJzZUludChkYXRlU2VsLnN1YnN0cig1LDIpKTtcbiAgICAgICAgICAgIHZhciBkYXkgPSBwYXJzZUludChkYXRlU2VsLnN1YnN0cig4LDIpKTtcblxuICAgICAgICAgICAgaWYgKG1vbnRoICE9IDApe1xuICAgICAgICAgICAgICAgaWYoZGF5ICE9IDApe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbGlkaXR5LnN1Ym1pdCgncG9zdCcsICcvdmVyaWZ5LWRvY3MnKVxuICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXG5cdFx0XHRcdFx0XHRpZih0aGlzLmRvY3R5cGU9PSc5Z0lDYXJkJyl7XG5cdFx0XHRcdFx0XHRcdHRoaXMubmluZWdpY2FyZF9leHBpcnkgPSBkYXRlU2VsO1xuXHRcdFx0XHRcdFx0XHR2YXIgZCA9IG5ldyBEYXRlKHRoaXMubmluZWdpY2FyZF9leHBpcnkpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLm5pbmVnaWNhcmRfY29tcGFyZSA9IGQgPCB0aGlzLmRhdGVfbm93O1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRpZih0aGlzLmRvY3R5cGU9PSc5Z09yZGVyJyl7XG5cdFx0XHRcdFx0XHRcdHRoaXMubmluZWdvcmRlcl9leHBpcnkgPSBkYXRlU2VsO1xuXHRcdFx0XHRcdFx0XHR2YXIgZCA9IG5ldyBEYXRlKHRoaXMubmluZWdvcmRlcl9leHBpcnkpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLm5pbmVnb3JkZXJfY29tcGFyZSA9IGQgPCB0aGlzLmRhdGVfbm93O1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRpZih0aGlzLmRvY3R5cGU9PSdwcnZDYXJkJyl7XG5cdFx0XHRcdFx0XHRcdHRoaXMucHJ2Y2FyZF9leHBpcnkgPSBkYXRlU2VsO1xuXHRcdFx0XHRcdFx0XHR2YXIgZCA9IG5ldyBEYXRlKHRoaXMucHJ2Y2FyZF9leHBpcnkpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLnBydmNhcmRfY29tcGFyZSA9IGQgPCB0aGlzLmRhdGVfbm93O1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRpZih0aGlzLmRvY3R5cGU9PSdwcnZPcmRlcicpe1xuXHRcdFx0XHRcdFx0XHR0aGlzLnBydm9yZGVyX2V4cGlyeSA9IGRhdGVTZWw7XG5cdFx0XHRcdFx0XHRcdHZhciBkID0gbmV3IERhdGUodGhpcy5wcnZvcmRlcl9leHBpcnkpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLnBydm9yZGVyX2NvbXBhcmUgPSBkIDwgdGhpcy5kYXRlX25vdztcblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0aWYodGhpcy5kb2N0eXBlPT0nc3JydkNhcmQnKXtcblx0XHRcdFx0XHRcdFx0dGhpcy5zcnJ2Y2FyZF9leHBpcnkgPSBkYXRlU2VsO1xuXHRcdFx0XHRcdFx0XHR2YXIgZCA9IG5ldyBEYXRlKHRoaXMuc3JydmNhcmRfZXhwaXJ5KTtcblx0XHRcdFx0XHRcdFx0dGhpcy5zcnJ2Y2FyZF9jb21wYXJlID0gZCA8IHRoaXMuZGF0ZV9ub3c7XG5cdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdGlmKHRoaXMuZG9jdHlwZT09J3NycnZWaXNhJyl7XG5cdFx0XHRcdFx0XHRcdHRoaXMuc3JydnZpc2FfZXhwaXJ5ID0gZGF0ZVNlbDtcblx0XHRcdFx0XHRcdFx0dmFyIGQgPSBuZXcgRGF0ZSh0aGlzLnNycnZ2aXNhX2V4cGlyeSk7XG5cdFx0XHRcdFx0XHRcdHRoaXMuc3JydnZpc2FfY29tcGFyZSA9IGQgPCB0aGlzLmRhdGVfbm93O1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRpZih0aGlzLmRvY3R5cGU9PSdhZXAnKXtcblx0XHRcdFx0XHRcdFx0dGhpcy5hZXBfZXhwaXJ5ID0gZGF0ZVNlbDtcblx0XHRcdFx0XHRcdFx0dmFyIGQgPSBuZXcgRGF0ZSh0aGlzLmFlcF9leHBpcnkpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLmFlcF9jb21wYXJlID0gZCA8IHRoaXMuZGF0ZV9ub3c7XG5cdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdGlmKHRoaXMuZG9jdHlwZT09J25iaScpe1xuXHRcdFx0XHRcdFx0XHR0aGlzLm5iaV9leHBpcnkgPSBkYXRlU2VsO1xuXHRcdFx0XHRcdFx0XHR2YXIgZCA9IG5ldyBEYXRlKHRoaXMubmJpX2V4cGlyeSk7XG5cdFx0XHRcdFx0XHRcdHRoaXMubmJpX2NvbXBhcmUgPSBkIDwgdGhpcy5kYXRlX25vdztcblx0XHRcdFx0XHRcdH1cblxuXG4gICAgICAgICAgICAgICAgICAgIFx0JChcIiNcIit0aGlzLnZhbGlkaXR5LmlkKS5wb3BvdmVyKCdoaWRlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGVycm9yLmV4cGlyZXNfYXQpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJy5wb3BvdmVyICN2YWxpZHNldCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoXCJQbGVhc2UgdXNlIHZhbGlkIGRhdGUgb2YgZXhwaXJ5LlwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKCdVcGRhdGUgZmFpbGVkLicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNcIit0aGlzLnZhbGlkaXR5LmlkKS5wb3BvdmVyKCdzaG93Jyk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignUGxlYXNlIHVzZSB2YWxpZCBleHBpcmF0aW9uIGRhdGUnKTtcbiAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignUGxlYXNlIHVzZSB2YWxpZCBleHBpcmF0aW9uIGRhdGUnKTtcbiAgICAgICAgICAgIH1cblx0XHR9LFxuXG5cdFx0ZmV0Y2hEb2NzKGlkKSB7XG5cdFx0XHR0aGlzLiRyZWZzLm11bHRpcGxlcXVpY2tyZXBvcnRyZWYuZmV0Y2hEb2NzKGlkKTtcblx0XHR9LFxuXG5cdFx0ZmV0Y2hEb2NzMihpZCkge1xuXHRcdFx0dGhpcy4kcmVmcy5lZGl0c2VydmljZXJlZi5mZXRjaERvY3MoaWQpO1xuXHRcdH0sXG5cblx0XHRmZXRjaERvY3MzKHNlcnZpY2VfaWQsbmVlZGVkLG9wdGlvbmFsKSB7XG5cdFx0XHR0aGlzLiRyZWZzLmFkZG5ld3NlcnZpY2VyZWYuZmV0Y2hEb2NzKHNlcnZpY2VfaWQsbmVlZGVkLG9wdGlvbmFsKTtcblx0XHR9XG5cdH0sXG5cdCBtb3VudGVkICgpIHtcblx0ICAgIHZhciB2bSA9IHRoaXM7XG5cblx0ICAgICQoJ2JvZHknKS5wb3BvdmVyKHsgLy8gT2tcblx0XHQgICAgaHRtbDp0cnVlLFxuXHRcdFx0dHJpZ2dlcjogJ2hvdmVyJyxcblx0XHQgICAgc2VsZWN0b3I6ICdbZGF0YS10b2dnbGU9XCJwb3BvdmVyXCJdJ1xuXHRcdH0pO1xuXG5cdFx0Ly8gJCgnLmNob3Nlbi1zZWxlY3QtZm9yLWNsaWVudHMnKS5jaG9zZW4oe1xuXHRcdC8vIFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdC8vIFx0bm9fcmVzdWx0c190ZXh0OiBcIlNlYXJjaGluZyBDbGllbnQgIyA6IFwiLFxuXHRcdC8vIFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXG5cdFx0Ly8gfSk7XG5cblx0XHRcdFx0XHRcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLWNsaWVudHMnKS5vbignY2hhbmdlJywgKCkgPT4ge1xuXHRcdFx0bGV0IGNsaWVudElkcyA9ICQoJy5jaG9zZW4tc2VsZWN0LWZvci1jbGllbnRzJykudmFsKCk7XG5cblx0XHRcdC8vdGhpcy5hZGROZXdNZW1iZXJGb3JtLmNsaWVudElkcyA9IGNsaWVudElkcztcblx0XHR9KTtcblx0fSxcblx0Y29tcHV0ZWQ6e1xuICAgIFx0cGFja19kYXRlKCl7XG4gICAgXHRcdHZhciBjID0gbW9tZW50KFN0cmluZygnMjAxNy0wMS0wMScpKS5mb3JtYXQoXCJZLW0tZFwiKTtcbiAgICBcdFx0dmFyIGQgPSBtb21lbnQoU3RyaW5nKHRoaXMucGFja2FnZV9kYXRlKSkuZm9ybWF0KFwiWS1tLWRcIik7XG4gICAgXHRcdHJldHVybiAoYyA8IGQgPyB0cnVlIDogZmFsc2UpO1xuICAgIFx0fVxuICAgIH0sXG5cdHdhdGNoOiB7XG5cdCAgICAvLyB3aGVuZXZlciBiZGF5IGNoYW5nZXMsIHRoaXMgZnVuY3Rpb24gd2lsbCBydW5cblx0ICAgIGJkYXk6IGZ1bmN0aW9uIChuZXdWYWwpIHtcblx0ICAgICAgdGhpcy51cFN0b3JlKG5ld1ZhbCwnYmRheScpO1xuXHQgICAgfVxuXHR9LFxuXHR1cGRhdGVkKCkge1xuXHQgICAgdGhpcy5wb3BPdmVyKCk7XG5cblx0ICAgIC8vIEZpcmVkIGV2ZXJ5IHNlY29uZCwgc2hvdWxkIGFsd2F5cyBiZSB0cnVlXG5cdCAgICB2YXIgb3dsID0gJChcIi50cmFja2luZ3NcIik7XG4gICAgICAgIG93bC5vd2xDYXJvdXNlbCh7XG4gICAgICAgICAgICBpdGVtcyA6IDQsXG4gICAgICAgICAgICBsYXp5TG9hZCA6IHRydWUsXG4gICAgICAgICAgICBuYXZpZ2F0aW9uIDogZmFsc2UsXG4gICAgICAgICAgICBwYWdpbmF0aW9uIDpmYWxzZSxcbiAgICAgICAgICAgIHJld2luZE5hdjpmYWxzZVxuICAgICAgICB9KTtcbiAgICAgICAgJChcIi5uZXh0XCIpLmNsaWNrKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBvd2wudHJpZ2dlcignb3dsLm5leHQnKTtcbiAgICAgICAgfSk7XG4gICAgICAgICQoXCIucHJldlwiKS5jbGljayhmdW5jdGlvbigpe1xuICAgICAgICAgICAgb3dsLnRyaWdnZXIoJ293bC5wcmV2Jyk7XG4gICAgICAgIH0pO1xuICAgICAvLyAgICAkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuXHQgICAgLy8gICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdCAgICAvLyAgICAgXCJsZW5ndGhNZW51XCI6IFtbMTAsIDI1LCA1MCwgLTFdLCBbMTAsIDI1LCA1MCwgXCJBbGxcIl1dLFxuXHQgICAgLy8gICAgIFwiaURpc3BsYXlMZW5ndGhcIjogMjVcblx0ICAgIC8vIH0pO1xuXHR9LFxuXHRkaXJlY3RpdmVzOiB7XG5cdCAgICBkYXRlcGlja2VyOiB7XG5cdCAgICAgICAgYmluZChlbCwgYmluZGluZywgdm5vZGUpIHtcblx0ICAgICAgICAgICAgJChlbCkuZGF0ZXBpY2tlcih7XG5cdCAgICAgICAgICAgICAgICBmb3JtYXQ6ICd5eXl5LW1tLWRkJ1xuXHQgICAgICAgICAgICB9KS5vbignY2hhbmdlRGF0ZScsIChlKSA9PiB7XG5cdCAgICAgICAgICAgICAgICBDbGllbnRBcHAuJHNldChDbGllbnRBcHAudmFsaWRpdHksICdleHBpcmVzX2F0JywgZS5mb3JtYXQoJ3l5eXktbW0tZGQnKSk7XG5cdCAgICAgICAgICAgIH0pO1xuXHQgICAgICAgIH1cblx0ICAgIH1cblx0fSxcblx0Y29tcG9uZW50czoge1xuXHQgICAgTXVsdGlzZWxlY3Rcblx0ICAgICwnbXVsdGlwbGUtcXVpY2stcmVwb3J0JzogcmVxdWlyZSgnLi4vY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnQudnVlJylcblx0ICAgICwnY2xpZW50LWRvY3VtZW50cyc6IHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvVmlzYS9DbGllbnREb2N1bWVudHMudnVlJylcblx0fVxuXG59KTtcblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuXHRkb2N1bWVudC50aXRsZSA9IENsaWVudEFwcC5mdWxsbmFtZTsgXG5cbiAgICAkKCcjYmlydGhfZGF0ZScpLmRhdGVwaWNrZXIoKTtcblx0JCgnLnVuZGVybGluZWQnKS5oaWRlKCk7XG5cdCQoJy5mcmVhc29uJykuaGlkZSgpO1xuXHQkKCcudHJhbnNmZXJUbycpLmhpZGUoKTtcbiAgICAkKCcuZHVhbF9zZWxlY3QnKS5ib290c3RyYXBEdWFsTGlzdGJveCh7XG4gICAgICAgIHNlbGVjdG9yTWluaW1hbEhlaWdodDogMTYwLFxuICAgIH0pO1xuXG4gICAgJChcIi5zZS1wcmUtY29uMlwiKS5mYWRlT3V0KFwic2xvd1wiKTtcblxuICAgICQoXCIuZGVsZXRlLXBhY2thZ2VcIilcbiAgICAucG9wb3Zlcih7XG4gICAgICBodG1sOnRydWUsXG4gICAgICBjb250YWluZXI6ICdib2R5JyxcbiAgICAgIGNvbnRlbnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuICQoJyNkZWxldGVQYWNrYWdlUG9wJykuaHRtbCgpO1xuICAgICAgfSxcbiAgICB9KTtcblxuICAgICQoXCIuYWRkLWZ1bmRzXCIpXG4gICAgLnBvcG92ZXIoe1xuICAgICAgaHRtbDp0cnVlLFxuICAgICAgY29udGFpbmVyOiAnYm9keScsXG4gICAgICBjb250ZW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAkKCcjYWRkRnVuZHNQb3AnKS5odG1sKCk7XG4gICAgICB9LFxuICAgIH0pO1xuXG5cbn0pO1xuXG4kKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiLmRlbC1wYWNrXCIsIGZ1bmN0aW9uKCkge1xuICAgIHZhciByZWFzb24gPSAkKCcucG9wb3ZlciAjcmVhc29uJykudmFsKCk7XG4gICAgQ2xpZW50QXBwLmRlbGV0ZVBhY2thZ2UocmVhc29uKTtcbn0pO1xuXG4kKGRvY3VtZW50KS5vbihcImNoYW5nZVwiLCBcIiNmdW5kX3R5cGVcIiwgZnVuY3Rpb24oKSB7XG4gICAgdmFyIHR5cGUgPSAkKCcucG9wb3ZlciAjZnVuZF90eXBlJykudmFsKCk7XG4gICAgaWYodHlwZSA9PSBcImRpc2NvdW50XCIgfHwgdHlwZSA9PSBcInJlZnVuZFwiKXtcbiAgICBcdCQoJy5mcmVhc29uJykuc2hvdygpO1xuXHRcdCQoJy50cmFuc2ZlclRvJykuaGlkZSgpO1xuICAgIH1cbiAgICBlbHNlIGlmKHR5cGU9PSBcInRyYW5zZmVyXCIpe1xuXHRcdCQoJy5mcmVhc29uJykuc2hvdygpO1xuXHRcdCQoJy50cmFuc2ZlclRvJykuc2hvdygpO1xuICAgIH1cbiAgICBlbHNle1xuICAgIFx0JCgnLmZyZWFzb24nKS5oaWRlKCk7XG5cdFx0JCgnLnRyYW5zZmVyVG8nKS5oaWRlKCk7XG4gICAgXHQkKCcucG9wb3ZlciAjZnVuZF9yZWFzb24nKS52YWwoJycpO1xuICAgIH1cbn0pO1xuXG4kKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiLmZ1bmRzLWFkZFwiLCBmdW5jdGlvbigpIHtcbiAgICB2YXIgcmVhc29uID0gJCgnLnBvcG92ZXIgI2Z1bmRfcmVhc29uJykudmFsKCk7XG4gICAgdmFyIGFtb3VudCA9ICQoJy5wb3BvdmVyICNmdW5kX2Ftb3VudCcpLnZhbCgpO1xuICAgIHZhciB0eXBlID0gJCgnLnBvcG92ZXIgI2Z1bmRfdHlwZScpLnZhbCgpO1xuICAgIC8vIGFsZXJ0KHJlYXNvbitcIiBcIithbW91bnQrXCIgXCIrdHlwZSk7XG4gICAgQ2xpZW50QXBwLmFkZEZ1bmRzKHR5cGUsYW1vdW50LHJlYXNvbik7XG59KTtcblxuJChkb2N1bWVudCkub24oXCJjbGlja1wiLCBcIiNmdWxsQmFsYW5jZVwiLCBmdW5jdGlvbigpIHtcbiAgICAkKCcucG9wb3ZlciAjZnVuZF9hbW91bnQnKS52YWwoTWF0aC5hYnMoQ2xpZW50QXBwLmJhbGFuY2UpKTtcbn0pO1xuXG4kKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiLnNhdmUtdmVyaWZpY2F0aW9uXCIsIGZ1bmN0aW9uKCkge1xuICAgIHZhciBkYXRlU2VsID0gJCgnLnBvcG92ZXIgI2V4cGlyZXNfYXQnKS52YWwoKTtcbiAgICBDbGllbnRBcHAuc2F2ZVZhbGlkaXR5KGRhdGVTZWwpO1xufSk7XG5cbiQoZG9jdW1lbnQpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgJCgnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXSxbZGF0YS1vcmlnaW5hbC10aXRsZV0nKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKCEkKHRoaXMpLmlzKGUudGFyZ2V0KSAmJiAkKHRoaXMpLmhhcyhlLnRhcmdldCkubGVuZ3RoID09PSAwICYmICQoJy5wb3BvdmVyJykuaGFzKGUudGFyZ2V0KS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICgoJCh0aGlzKS5wb3BvdmVyKCdoaWRlJykuZGF0YSgnYnMucG9wb3ZlcicpfHx7fSkuaW5TdGF0ZXx8e30pLmNsaWNrID0gZmFsc2UgIC8vIGZpeCBmb3IgQlMgMy4zLjZcbiAgICAgICAgfVxuICAgIH0pO1xufSk7XG5cbiQoZG9jdW1lbnQpLm9uKCdmb2N1cy5hdXRvRXhwYW5kJywgJ3RleHRhcmVhLmF1dG9FeHBhbmQnLCBmdW5jdGlvbigpe1xuICAgICAgICB2YXIgc2F2ZWRWYWx1ZSA9IHRoaXMudmFsdWU7XG4gICAgICAgIHRoaXMudmFsdWUgPSAnJztcbiAgICAgICAgdGhpcy5iYXNlU2Nyb2xsSGVpZ2h0ID0gdGhpcy5zY3JvbGxIZWlnaHQ7XG4gICAgICAgIHRoaXMudmFsdWUgPSBzYXZlZFZhbHVlO1xuICAgIH0pXG4gICAgLm9uKCdpbnB1dC5hdXRvRXhwYW5kJywgJ3RleHRhcmVhLmF1dG9FeHBhbmQnLCBmdW5jdGlvbigpe1xuICAgICAgICB2YXIgbWluUm93cyA9IHRoaXMuZ2V0QXR0cmlidXRlKCdkYXRhLW1pbi1yb3dzJyl8MCwgcm93cztcbiAgICAgICAgdGhpcy5yb3dzID0gbWluUm93cztcbiAgICAgICAgcm93cyA9IE1hdGguY2VpbCgodGhpcy5zY3JvbGxIZWlnaHQgLSB0aGlzLmJhc2VTY3JvbGxIZWlnaHQpIC8gMTcpO1xuICAgICAgICB0aGlzLnJvd3MgPSBtaW5Sb3dzICsgcm93cztcbiAgICB9KTtcblxuJChkb2N1bWVudCkub24oJ2NsaWNrJywgJ1tkYXRhLXRvZ2dsZT1cImxpZ2h0Ym94XCJdJywgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICQodGhpcykuZWtrb0xpZ2h0Ym94KCk7XG59KTtcblxuXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9wYWdlcy91c2VycHJvZmlsZS5qcyIsIiFmdW5jdGlvbih0LGUpe1wib2JqZWN0XCI9PXR5cGVvZiBleHBvcnRzJiZcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlP21vZHVsZS5leHBvcnRzPWUoKTpcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFtdLGUpOlwib2JqZWN0XCI9PXR5cGVvZiBleHBvcnRzP2V4cG9ydHMuVnVlTXVsdGlzZWxlY3Q9ZSgpOnQuVnVlTXVsdGlzZWxlY3Q9ZSgpfSh0aGlzLGZ1bmN0aW9uKCl7cmV0dXJuIGZ1bmN0aW9uKHQpe2Z1bmN0aW9uIGUoaSl7aWYobltpXSlyZXR1cm4gbltpXS5leHBvcnRzO3ZhciByPW5baV09e2k6aSxsOiExLGV4cG9ydHM6e319O3JldHVybiB0W2ldLmNhbGwoci5leHBvcnRzLHIsci5leHBvcnRzLGUpLHIubD0hMCxyLmV4cG9ydHN9dmFyIG49e307cmV0dXJuIGUubT10LGUuYz1uLGUuaT1mdW5jdGlvbih0KXtyZXR1cm4gdH0sZS5kPWZ1bmN0aW9uKHQsbixpKXtlLm8odCxuKXx8T2JqZWN0LmRlZmluZVByb3BlcnR5KHQsbix7Y29uZmlndXJhYmxlOiExLGVudW1lcmFibGU6ITAsZ2V0Oml9KX0sZS5uPWZ1bmN0aW9uKHQpe3ZhciBuPXQmJnQuX19lc01vZHVsZT9mdW5jdGlvbigpe3JldHVybiB0LmRlZmF1bHR9OmZ1bmN0aW9uKCl7cmV0dXJuIHR9O3JldHVybiBlLmQobixcImFcIixuKSxufSxlLm89ZnVuY3Rpb24odCxlKXtyZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHQsZSl9LGUucD1cIi9cIixlKGUucz02MCl9KFtmdW5jdGlvbih0LGUpe3ZhciBuPXQuZXhwb3J0cz1cInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93JiZ3aW5kb3cuTWF0aD09TWF0aD93aW5kb3c6XCJ1bmRlZmluZWRcIiE9dHlwZW9mIHNlbGYmJnNlbGYuTWF0aD09TWF0aD9zZWxmOkZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKTtcIm51bWJlclwiPT10eXBlb2YgX19nJiYoX19nPW4pfSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big0OSkoXCJ3a3NcIikscj1uKDMwKSxvPW4oMCkuU3ltYm9sLHM9XCJmdW5jdGlvblwiPT10eXBlb2YgbzsodC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBpW3RdfHwoaVt0XT1zJiZvW3RdfHwocz9vOnIpKFwiU3ltYm9sLlwiK3QpKX0pLnN0b3JlPWl9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDUpO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtpZighaSh0KSl0aHJvdyBUeXBlRXJyb3IodCtcIiBpcyBub3QgYW4gb2JqZWN0IVwiKTtyZXR1cm4gdH19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDApLHI9bigxMCksbz1uKDgpLHM9big2KSx1PW4oMTEpLGE9ZnVuY3Rpb24odCxlLG4pe3ZhciBsLGMsZixwLGg9dCZhLkYsZD10JmEuRyx2PXQmYS5TLGc9dCZhLlAsbT10JmEuQix5PWQ/aTp2P2lbZV18fChpW2VdPXt9KTooaVtlXXx8e30pLnByb3RvdHlwZSxiPWQ/cjpyW2VdfHwocltlXT17fSksXz1iLnByb3RvdHlwZXx8KGIucHJvdG90eXBlPXt9KTtkJiYobj1lKTtmb3IobCBpbiBuKWM9IWgmJnkmJnZvaWQgMCE9PXlbbF0sZj0oYz95Om4pW2xdLHA9bSYmYz91KGYsaSk6ZyYmXCJmdW5jdGlvblwiPT10eXBlb2YgZj91KEZ1bmN0aW9uLmNhbGwsZik6Zix5JiZzKHksbCxmLHQmYS5VKSxiW2xdIT1mJiZvKGIsbCxwKSxnJiZfW2xdIT1mJiYoX1tsXT1mKX07aS5jb3JlPXIsYS5GPTEsYS5HPTIsYS5TPTQsYS5QPTgsYS5CPTE2LGEuVz0zMixhLlU9NjQsYS5SPTEyOCx0LmV4cG9ydHM9YX0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz0hbig3KShmdW5jdGlvbigpe3JldHVybiA3IT1PYmplY3QuZGVmaW5lUHJvcGVydHkoe30sXCJhXCIse2dldDpmdW5jdGlvbigpe3JldHVybiA3fX0pLmF9KX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuXCJvYmplY3RcIj09dHlwZW9mIHQ/bnVsbCE9PXQ6XCJmdW5jdGlvblwiPT10eXBlb2YgdH19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDApLHI9big4KSxvPW4oMTIpLHM9bigzMCkoXCJzcmNcIiksdT1GdW5jdGlvbi50b1N0cmluZyxhPShcIlwiK3UpLnNwbGl0KFwidG9TdHJpbmdcIik7bigxMCkuaW5zcGVjdFNvdXJjZT1mdW5jdGlvbih0KXtyZXR1cm4gdS5jYWxsKHQpfSwodC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuLHUpe3ZhciBsPVwiZnVuY3Rpb25cIj09dHlwZW9mIG47bCYmKG8obixcIm5hbWVcIil8fHIobixcIm5hbWVcIixlKSksdFtlXSE9PW4mJihsJiYobyhuLHMpfHxyKG4scyx0W2VdP1wiXCIrdFtlXTphLmpvaW4oU3RyaW5nKGUpKSkpLHQ9PT1pP3RbZV09bjp1P3RbZV0/dFtlXT1uOnIodCxlLG4pOihkZWxldGUgdFtlXSxyKHQsZSxuKSkpfSkoRnVuY3Rpb24ucHJvdG90eXBlLFwidG9TdHJpbmdcIixmdW5jdGlvbigpe3JldHVyblwiZnVuY3Rpb25cIj09dHlwZW9mIHRoaXMmJnRoaXNbc118fHUuY2FsbCh0aGlzKX0pfSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0KXt0cnl7cmV0dXJuISF0KCl9Y2F0Y2godCl7cmV0dXJuITB9fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTMpLHI9bigyNSk7dC5leHBvcnRzPW4oNCk/ZnVuY3Rpb24odCxlLG4pe3JldHVybiBpLmYodCxlLHIoMSxuKSl9OmZ1bmN0aW9uKHQsZSxuKXtyZXR1cm4gdFtlXT1uLHR9fSxmdW5jdGlvbih0LGUpe3ZhciBuPXt9LnRvU3RyaW5nO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gbi5jYWxsKHQpLnNsaWNlKDgsLTEpfX0sZnVuY3Rpb24odCxlKXt2YXIgbj10LmV4cG9ydHM9e3ZlcnNpb246XCIyLjUuN1wifTtcIm51bWJlclwiPT10eXBlb2YgX19lJiYoX19lPW4pfSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxNCk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuKXtpZihpKHQpLHZvaWQgMD09PWUpcmV0dXJuIHQ7c3dpdGNoKG4pe2Nhc2UgMTpyZXR1cm4gZnVuY3Rpb24obil7cmV0dXJuIHQuY2FsbChlLG4pfTtjYXNlIDI6cmV0dXJuIGZ1bmN0aW9uKG4saSl7cmV0dXJuIHQuY2FsbChlLG4saSl9O2Nhc2UgMzpyZXR1cm4gZnVuY3Rpb24obixpLHIpe3JldHVybiB0LmNhbGwoZSxuLGkscil9fXJldHVybiBmdW5jdGlvbigpe3JldHVybiB0LmFwcGx5KGUsYXJndW1lbnRzKX19fSxmdW5jdGlvbih0LGUpe3ZhciBuPXt9Lmhhc093blByb3BlcnR5O3QuZXhwb3J0cz1mdW5jdGlvbih0LGUpe3JldHVybiBuLmNhbGwodCxlKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDIpLHI9big0MSksbz1uKDI5KSxzPU9iamVjdC5kZWZpbmVQcm9wZXJ0eTtlLmY9big0KT9PYmplY3QuZGVmaW5lUHJvcGVydHk6ZnVuY3Rpb24odCxlLG4pe2lmKGkodCksZT1vKGUsITApLGkobikscil0cnl7cmV0dXJuIHModCxlLG4pfWNhdGNoKHQpe31pZihcImdldFwiaW4gbnx8XCJzZXRcImluIG4pdGhyb3cgVHlwZUVycm9yKFwiQWNjZXNzb3JzIG5vdCBzdXBwb3J0ZWQhXCIpO3JldHVyblwidmFsdWVcImluIG4mJih0W2VdPW4udmFsdWUpLHR9fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0KXtpZihcImZ1bmN0aW9uXCIhPXR5cGVvZiB0KXRocm93IFR5cGVFcnJvcih0K1wiIGlzIG5vdCBhIGZ1bmN0aW9uIVwiKTtyZXR1cm4gdH19LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPXt9fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0KXtpZih2b2lkIDA9PXQpdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY2FsbCBtZXRob2Qgb24gIFwiK3QpO3JldHVybiB0fX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oNyk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7cmV0dXJuISF0JiZpKGZ1bmN0aW9uKCl7ZT90LmNhbGwobnVsbCxmdW5jdGlvbigpe30sMSk6dC5jYWxsKG51bGwpfSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigyMykscj1uKDE2KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIGkocih0KSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big1Mykscj1NYXRoLm1pbjt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIHQ+MD9yKGkodCksOTAwNzE5OTI1NDc0MDk5MSk6MH19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDExKSxyPW4oMjMpLG89bigyOCkscz1uKDE5KSx1PW4oNjQpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUpe3ZhciBuPTE9PXQsYT0yPT10LGw9Mz09dCxjPTQ9PXQsZj02PT10LHA9NT09dHx8ZixoPWV8fHU7cmV0dXJuIGZ1bmN0aW9uKGUsdSxkKXtmb3IodmFyIHYsZyxtPW8oZSkseT1yKG0pLGI9aSh1LGQsMyksXz1zKHkubGVuZ3RoKSx4PTAsdz1uP2goZSxfKTphP2goZSwwKTp2b2lkIDA7Xz54O3grKylpZigocHx8eCBpbiB5KSYmKHY9eVt4XSxnPWIodix4LG0pLHQpKWlmKG4pd1t4XT1nO2Vsc2UgaWYoZylzd2l0Y2godCl7Y2FzZSAzOnJldHVybiEwO2Nhc2UgNTpyZXR1cm4gdjtjYXNlIDY6cmV0dXJuIHg7Y2FzZSAyOncucHVzaCh2KX1lbHNlIGlmKGMpcmV0dXJuITE7cmV0dXJuIGY/LTE6bHx8Yz9jOnd9fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNSkscj1uKDApLmRvY3VtZW50LG89aShyKSYmaShyLmNyZWF0ZUVsZW1lbnQpO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gbz9yLmNyZWF0ZUVsZW1lbnQodCk6e319fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1cImNvbnN0cnVjdG9yLGhhc093blByb3BlcnR5LGlzUHJvdG90eXBlT2YscHJvcGVydHlJc0VudW1lcmFibGUsdG9Mb2NhbGVTdHJpbmcsdG9TdHJpbmcsdmFsdWVPZlwiLnNwbGl0KFwiLFwiKX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oOSk7dC5leHBvcnRzPU9iamVjdChcInpcIikucHJvcGVydHlJc0VudW1lcmFibGUoMCk/T2JqZWN0OmZ1bmN0aW9uKHQpe3JldHVyblwiU3RyaW5nXCI9PWkodCk/dC5zcGxpdChcIlwiKTpPYmplY3QodCl9fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz0hMX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtyZXR1cm57ZW51bWVyYWJsZTohKDEmdCksY29uZmlndXJhYmxlOiEoMiZ0KSx3cml0YWJsZTohKDQmdCksdmFsdWU6ZX19fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxMykuZixyPW4oMTIpLG89bigxKShcInRvU3RyaW5nVGFnXCIpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbil7dCYmIXIodD1uP3Q6dC5wcm90b3R5cGUsbykmJmkodCxvLHtjb25maWd1cmFibGU6ITAsdmFsdWU6ZX0pfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNDkpKFwia2V5c1wiKSxyPW4oMzApO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gaVt0XXx8KGlbdF09cih0KSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxNik7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBPYmplY3QoaSh0KSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big1KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtpZighaSh0KSlyZXR1cm4gdDt2YXIgbixyO2lmKGUmJlwiZnVuY3Rpb25cIj09dHlwZW9mKG49dC50b1N0cmluZykmJiFpKHI9bi5jYWxsKHQpKSlyZXR1cm4gcjtpZihcImZ1bmN0aW9uXCI9PXR5cGVvZihuPXQudmFsdWVPZikmJiFpKHI9bi5jYWxsKHQpKSlyZXR1cm4gcjtpZighZSYmXCJmdW5jdGlvblwiPT10eXBlb2Yobj10LnRvU3RyaW5nKSYmIWkocj1uLmNhbGwodCkpKXJldHVybiByO3Rocm93IFR5cGVFcnJvcihcIkNhbid0IGNvbnZlcnQgb2JqZWN0IHRvIHByaW1pdGl2ZSB2YWx1ZVwiKX19LGZ1bmN0aW9uKHQsZSl7dmFyIG49MCxpPU1hdGgucmFuZG9tKCk7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVyblwiU3ltYm9sKFwiLmNvbmNhdCh2b2lkIDA9PT10P1wiXCI6dCxcIilfXCIsKCsrbitpKS50b1N0cmluZygzNikpfX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMCkscj1uKDEyKSxvPW4oOSkscz1uKDY3KSx1PW4oMjkpLGE9big3KSxsPW4oNzcpLmYsYz1uKDQ1KS5mLGY9bigxMykuZixwPW4oNTEpLnRyaW0saD1pLk51bWJlcixkPWgsdj1oLnByb3RvdHlwZSxnPVwiTnVtYmVyXCI9PW8obig0NCkodikpLG09XCJ0cmltXCJpbiBTdHJpbmcucHJvdG90eXBlLHk9ZnVuY3Rpb24odCl7dmFyIGU9dSh0LCExKTtpZihcInN0cmluZ1wiPT10eXBlb2YgZSYmZS5sZW5ndGg+Mil7ZT1tP2UudHJpbSgpOnAoZSwzKTt2YXIgbixpLHIsbz1lLmNoYXJDb2RlQXQoMCk7aWYoNDM9PT1vfHw0NT09PW8pe2lmKDg4PT09KG49ZS5jaGFyQ29kZUF0KDIpKXx8MTIwPT09bilyZXR1cm4gTmFOfWVsc2UgaWYoNDg9PT1vKXtzd2l0Y2goZS5jaGFyQ29kZUF0KDEpKXtjYXNlIDY2OmNhc2UgOTg6aT0yLHI9NDk7YnJlYWs7Y2FzZSA3OTpjYXNlIDExMTppPTgscj01NTticmVhaztkZWZhdWx0OnJldHVybitlfWZvcih2YXIgcyxhPWUuc2xpY2UoMiksbD0wLGM9YS5sZW5ndGg7bDxjO2wrKylpZigocz1hLmNoYXJDb2RlQXQobCkpPDQ4fHxzPnIpcmV0dXJuIE5hTjtyZXR1cm4gcGFyc2VJbnQoYSxpKX19cmV0dXJuK2V9O2lmKCFoKFwiIDBvMVwiKXx8IWgoXCIwYjFcIil8fGgoXCIrMHgxXCIpKXtoPWZ1bmN0aW9uKHQpe3ZhciBlPWFyZ3VtZW50cy5sZW5ndGg8MT8wOnQsbj10aGlzO3JldHVybiBuIGluc3RhbmNlb2YgaCYmKGc/YShmdW5jdGlvbigpe3YudmFsdWVPZi5jYWxsKG4pfSk6XCJOdW1iZXJcIiE9byhuKSk/cyhuZXcgZCh5KGUpKSxuLGgpOnkoZSl9O2Zvcih2YXIgYixfPW4oNCk/bChkKTpcIk1BWF9WQUxVRSxNSU5fVkFMVUUsTmFOLE5FR0FUSVZFX0lORklOSVRZLFBPU0lUSVZFX0lORklOSVRZLEVQU0lMT04saXNGaW5pdGUsaXNJbnRlZ2VyLGlzTmFOLGlzU2FmZUludGVnZXIsTUFYX1NBRkVfSU5URUdFUixNSU5fU0FGRV9JTlRFR0VSLHBhcnNlRmxvYXQscGFyc2VJbnQsaXNJbnRlZ2VyXCIuc3BsaXQoXCIsXCIpLHg9MDtfLmxlbmd0aD54O3grKylyKGQsYj1fW3hdKSYmIXIoaCxiKSYmZihoLGIsYyhkLGIpKTtoLnByb3RvdHlwZT12LHYuY29uc3RydWN0b3I9aCxuKDYpKGksXCJOdW1iZXJcIixoKX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBpKHQpe3JldHVybiAwIT09dCYmKCEoIUFycmF5LmlzQXJyYXkodCl8fDAhPT10Lmxlbmd0aCl8fCF0KX1mdW5jdGlvbiByKHQpe3JldHVybiBmdW5jdGlvbigpe3JldHVybiF0LmFwcGx5KHZvaWQgMCxhcmd1bWVudHMpfX1mdW5jdGlvbiBvKHQsZSl7cmV0dXJuIHZvaWQgMD09PXQmJih0PVwidW5kZWZpbmVkXCIpLG51bGw9PT10JiYodD1cIm51bGxcIiksITE9PT10JiYodD1cImZhbHNlXCIpLC0xIT09dC50b1N0cmluZygpLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihlLnRyaW0oKSl9ZnVuY3Rpb24gcyh0LGUsbixpKXtyZXR1cm4gdC5maWx0ZXIoZnVuY3Rpb24odCl7cmV0dXJuIG8oaSh0LG4pLGUpfSl9ZnVuY3Rpb24gdSh0KXtyZXR1cm4gdC5maWx0ZXIoZnVuY3Rpb24odCl7cmV0dXJuIXQuJGlzTGFiZWx9KX1mdW5jdGlvbiBhKHQsZSl7cmV0dXJuIGZ1bmN0aW9uKG4pe3JldHVybiBuLnJlZHVjZShmdW5jdGlvbihuLGkpe3JldHVybiBpW3RdJiZpW3RdLmxlbmd0aD8obi5wdXNoKHskZ3JvdXBMYWJlbDppW2VdLCRpc0xhYmVsOiEwfSksbi5jb25jYXQoaVt0XSkpOm59LFtdKX19ZnVuY3Rpb24gbCh0LGUsaSxyLG8pe3JldHVybiBmdW5jdGlvbih1KXtyZXR1cm4gdS5tYXAoZnVuY3Rpb24odSl7dmFyIGE7aWYoIXVbaV0pcmV0dXJuIGNvbnNvbGUud2FybihcIk9wdGlvbnMgcGFzc2VkIHRvIHZ1ZS1tdWx0aXNlbGVjdCBkbyBub3QgY29udGFpbiBncm91cHMsIGRlc3BpdGUgdGhlIGNvbmZpZy5cIiksW107dmFyIGw9cyh1W2ldLHQsZSxvKTtyZXR1cm4gbC5sZW5ndGg/KGE9e30sbi5pKGQuYSkoYSxyLHVbcl0pLG4uaShkLmEpKGEsaSxsKSxhKTpbXX0pfX12YXIgYz1uKDU5KSxmPW4oNTQpLHA9KG4ubihmKSxuKDk1KSksaD0obi5uKHApLG4oMzEpKSxkPShuLm4oaCksbig1OCkpLHY9big5MSksZz0obi5uKHYpLG4oOTgpKSxtPShuLm4oZyksbig5MikpLHk9KG4ubihtKSxuKDg4KSksYj0obi5uKHkpLG4oOTcpKSxfPShuLm4oYiksbig4OSkpLHg9KG4ubihfKSxuKDk2KSksdz0obi5uKHgpLG4oOTMpKSxTPShuLm4odyksbig5MCkpLE89KG4ubihTKSxmdW5jdGlvbigpe2Zvcih2YXIgdD1hcmd1bWVudHMubGVuZ3RoLGU9bmV3IEFycmF5KHQpLG49MDtuPHQ7bisrKWVbbl09YXJndW1lbnRzW25dO3JldHVybiBmdW5jdGlvbih0KXtyZXR1cm4gZS5yZWR1Y2UoZnVuY3Rpb24odCxlKXtyZXR1cm4gZSh0KX0sdCl9fSk7ZS5hPXtkYXRhOmZ1bmN0aW9uKCl7cmV0dXJue3NlYXJjaDpcIlwiLGlzT3BlbjohMSxwcmVmZmVyZWRPcGVuRGlyZWN0aW9uOlwiYmVsb3dcIixvcHRpbWl6ZWRIZWlnaHQ6dGhpcy5tYXhIZWlnaHR9fSxwcm9wczp7aW50ZXJuYWxTZWFyY2g6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiEwfSxvcHRpb25zOnt0eXBlOkFycmF5LHJlcXVpcmVkOiEwfSxtdWx0aXBsZTp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITF9LHZhbHVlOnt0eXBlOm51bGwsZGVmYXVsdDpmdW5jdGlvbigpe3JldHVybltdfX0sdHJhY2tCeTp7dHlwZTpTdHJpbmd9LGxhYmVsOnt0eXBlOlN0cmluZ30sc2VhcmNoYWJsZTp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITB9LGNsZWFyT25TZWxlY3Q6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiEwfSxoaWRlU2VsZWN0ZWQ6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxwbGFjZWhvbGRlcjp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIlNlbGVjdCBvcHRpb25cIn0sYWxsb3dFbXB0eTp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITB9LHJlc2V0QWZ0ZXI6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxjbG9zZU9uU2VsZWN0Ont0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0sY3VzdG9tTGFiZWw6e3R5cGU6RnVuY3Rpb24sZGVmYXVsdDpmdW5jdGlvbih0LGUpe3JldHVybiBpKHQpP1wiXCI6ZT90W2VdOnR9fSx0YWdnYWJsZTp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITF9LHRhZ1BsYWNlaG9sZGVyOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiUHJlc3MgZW50ZXIgdG8gY3JlYXRlIGEgdGFnXCJ9LHRhZ1Bvc2l0aW9uOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwidG9wXCJ9LG1heDp7dHlwZTpbTnVtYmVyLEJvb2xlYW5dLGRlZmF1bHQ6ITF9LGlkOntkZWZhdWx0Om51bGx9LG9wdGlvbnNMaW1pdDp7dHlwZTpOdW1iZXIsZGVmYXVsdDoxZTN9LGdyb3VwVmFsdWVzOnt0eXBlOlN0cmluZ30sZ3JvdXBMYWJlbDp7dHlwZTpTdHJpbmd9LGdyb3VwU2VsZWN0Ont0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0sYmxvY2tLZXlzOnt0eXBlOkFycmF5LGRlZmF1bHQ6ZnVuY3Rpb24oKXtyZXR1cm5bXX19LHByZXNlcnZlU2VhcmNoOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0scHJlc2VsZWN0Rmlyc3Q6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfX0sbW91bnRlZDpmdW5jdGlvbigpe3RoaXMubXVsdGlwbGV8fHRoaXMuY2xlYXJPblNlbGVjdHx8Y29uc29sZS53YXJuKFwiW1Z1ZS1NdWx0aXNlbGVjdCB3YXJuXTogQ2xlYXJPblNlbGVjdCBhbmQgTXVsdGlwbGUgcHJvcHMgY2Fu4oCZdCBiZSBib3RoIHNldCB0byBmYWxzZS5cIiksIXRoaXMubXVsdGlwbGUmJnRoaXMubWF4JiZjb25zb2xlLndhcm4oXCJbVnVlLU11bHRpc2VsZWN0IHdhcm5dOiBNYXggcHJvcCBzaG91bGQgbm90IGJlIHVzZWQgd2hlbiBwcm9wIE11bHRpcGxlIGVxdWFscyBmYWxzZS5cIiksdGhpcy5wcmVzZWxlY3RGaXJzdCYmIXRoaXMuaW50ZXJuYWxWYWx1ZS5sZW5ndGgmJnRoaXMub3B0aW9ucy5sZW5ndGgmJnRoaXMuc2VsZWN0KHRoaXMuZmlsdGVyZWRPcHRpb25zWzBdKX0sY29tcHV0ZWQ6e2ludGVybmFsVmFsdWU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy52YWx1ZXx8MD09PXRoaXMudmFsdWU/QXJyYXkuaXNBcnJheSh0aGlzLnZhbHVlKT90aGlzLnZhbHVlOlt0aGlzLnZhbHVlXTpbXX0sZmlsdGVyZWRPcHRpb25zOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5zZWFyY2h8fFwiXCIsZT10LnRvTG93ZXJDYXNlKCkudHJpbSgpLG49dGhpcy5vcHRpb25zLmNvbmNhdCgpO3JldHVybiBuPXRoaXMuaW50ZXJuYWxTZWFyY2g/dGhpcy5ncm91cFZhbHVlcz90aGlzLmZpbHRlckFuZEZsYXQobixlLHRoaXMubGFiZWwpOnMobixlLHRoaXMubGFiZWwsdGhpcy5jdXN0b21MYWJlbCk6dGhpcy5ncm91cFZhbHVlcz9hKHRoaXMuZ3JvdXBWYWx1ZXMsdGhpcy5ncm91cExhYmVsKShuKTpuLG49dGhpcy5oaWRlU2VsZWN0ZWQ/bi5maWx0ZXIocih0aGlzLmlzU2VsZWN0ZWQpKTpuLHRoaXMudGFnZ2FibGUmJmUubGVuZ3RoJiYhdGhpcy5pc0V4aXN0aW5nT3B0aW9uKGUpJiYoXCJib3R0b21cIj09PXRoaXMudGFnUG9zaXRpb24/bi5wdXNoKHtpc1RhZzohMCxsYWJlbDp0fSk6bi51bnNoaWZ0KHtpc1RhZzohMCxsYWJlbDp0fSkpLG4uc2xpY2UoMCx0aGlzLm9wdGlvbnNMaW1pdCl9LHZhbHVlS2V5czpmdW5jdGlvbigpe3ZhciB0PXRoaXM7cmV0dXJuIHRoaXMudHJhY2tCeT90aGlzLmludGVybmFsVmFsdWUubWFwKGZ1bmN0aW9uKGUpe3JldHVybiBlW3QudHJhY2tCeV19KTp0aGlzLmludGVybmFsVmFsdWV9LG9wdGlvbktleXM6ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3JldHVybih0aGlzLmdyb3VwVmFsdWVzP3RoaXMuZmxhdEFuZFN0cmlwKHRoaXMub3B0aW9ucyk6dGhpcy5vcHRpb25zKS5tYXAoZnVuY3Rpb24oZSl7cmV0dXJuIHQuY3VzdG9tTGFiZWwoZSx0LmxhYmVsKS50b1N0cmluZygpLnRvTG93ZXJDYXNlKCl9KX0sY3VycmVudE9wdGlvbkxhYmVsOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMubXVsdGlwbGU/dGhpcy5zZWFyY2hhYmxlP1wiXCI6dGhpcy5wbGFjZWhvbGRlcjp0aGlzLmludGVybmFsVmFsdWUubGVuZ3RoP3RoaXMuZ2V0T3B0aW9uTGFiZWwodGhpcy5pbnRlcm5hbFZhbHVlWzBdKTp0aGlzLnNlYXJjaGFibGU/XCJcIjp0aGlzLnBsYWNlaG9sZGVyfX0sd2F0Y2g6e2ludGVybmFsVmFsdWU6ZnVuY3Rpb24oKXt0aGlzLnJlc2V0QWZ0ZXImJnRoaXMuaW50ZXJuYWxWYWx1ZS5sZW5ndGgmJih0aGlzLnNlYXJjaD1cIlwiLHRoaXMuJGVtaXQoXCJpbnB1dFwiLHRoaXMubXVsdGlwbGU/W106bnVsbCkpfSxzZWFyY2g6ZnVuY3Rpb24oKXt0aGlzLiRlbWl0KFwic2VhcmNoLWNoYW5nZVwiLHRoaXMuc2VhcmNoLHRoaXMuaWQpfX0sbWV0aG9kczp7Z2V0VmFsdWU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5tdWx0aXBsZT90aGlzLmludGVybmFsVmFsdWU6MD09PXRoaXMuaW50ZXJuYWxWYWx1ZS5sZW5ndGg/bnVsbDp0aGlzLmludGVybmFsVmFsdWVbMF19LGZpbHRlckFuZEZsYXQ6ZnVuY3Rpb24odCxlLG4pe3JldHVybiBPKGwoZSxuLHRoaXMuZ3JvdXBWYWx1ZXMsdGhpcy5ncm91cExhYmVsLHRoaXMuY3VzdG9tTGFiZWwpLGEodGhpcy5ncm91cFZhbHVlcyx0aGlzLmdyb3VwTGFiZWwpKSh0KX0sZmxhdEFuZFN0cmlwOmZ1bmN0aW9uKHQpe3JldHVybiBPKGEodGhpcy5ncm91cFZhbHVlcyx0aGlzLmdyb3VwTGFiZWwpLHUpKHQpfSx1cGRhdGVTZWFyY2g6ZnVuY3Rpb24odCl7dGhpcy5zZWFyY2g9dH0saXNFeGlzdGluZ09wdGlvbjpmdW5jdGlvbih0KXtyZXR1cm4hIXRoaXMub3B0aW9ucyYmdGhpcy5vcHRpb25LZXlzLmluZGV4T2YodCk+LTF9LGlzU2VsZWN0ZWQ6ZnVuY3Rpb24odCl7dmFyIGU9dGhpcy50cmFja0J5P3RbdGhpcy50cmFja0J5XTp0O3JldHVybiB0aGlzLnZhbHVlS2V5cy5pbmRleE9mKGUpPi0xfSxnZXRPcHRpb25MYWJlbDpmdW5jdGlvbih0KXtpZihpKHQpKXJldHVyblwiXCI7aWYodC5pc1RhZylyZXR1cm4gdC5sYWJlbDtpZih0LiRpc0xhYmVsKXJldHVybiB0LiRncm91cExhYmVsO3ZhciBlPXRoaXMuY3VzdG9tTGFiZWwodCx0aGlzLmxhYmVsKTtyZXR1cm4gaShlKT9cIlwiOmV9LHNlbGVjdDpmdW5jdGlvbih0LGUpe2lmKHQuJGlzTGFiZWwmJnRoaXMuZ3JvdXBTZWxlY3QpcmV0dXJuIHZvaWQgdGhpcy5zZWxlY3RHcm91cCh0KTtpZighKC0xIT09dGhpcy5ibG9ja0tleXMuaW5kZXhPZihlKXx8dGhpcy5kaXNhYmxlZHx8dC4kaXNEaXNhYmxlZHx8dC4kaXNMYWJlbCkmJighdGhpcy5tYXh8fCF0aGlzLm11bHRpcGxlfHx0aGlzLmludGVybmFsVmFsdWUubGVuZ3RoIT09dGhpcy5tYXgpJiYoXCJUYWJcIiE9PWV8fHRoaXMucG9pbnRlckRpcnR5KSl7aWYodC5pc1RhZyl0aGlzLiRlbWl0KFwidGFnXCIsdC5sYWJlbCx0aGlzLmlkKSx0aGlzLnNlYXJjaD1cIlwiLHRoaXMuY2xvc2VPblNlbGVjdCYmIXRoaXMubXVsdGlwbGUmJnRoaXMuZGVhY3RpdmF0ZSgpO2Vsc2V7aWYodGhpcy5pc1NlbGVjdGVkKHQpKXJldHVybiB2b2lkKFwiVGFiXCIhPT1lJiZ0aGlzLnJlbW92ZUVsZW1lbnQodCkpO3RoaXMuJGVtaXQoXCJzZWxlY3RcIix0LHRoaXMuaWQpLHRoaXMubXVsdGlwbGU/dGhpcy4kZW1pdChcImlucHV0XCIsdGhpcy5pbnRlcm5hbFZhbHVlLmNvbmNhdChbdF0pLHRoaXMuaWQpOnRoaXMuJGVtaXQoXCJpbnB1dFwiLHQsdGhpcy5pZCksdGhpcy5jbGVhck9uU2VsZWN0JiYodGhpcy5zZWFyY2g9XCJcIil9dGhpcy5jbG9zZU9uU2VsZWN0JiZ0aGlzLmRlYWN0aXZhdGUoKX19LHNlbGVjdEdyb3VwOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMsbj10aGlzLm9wdGlvbnMuZmluZChmdW5jdGlvbihuKXtyZXR1cm4gbltlLmdyb3VwTGFiZWxdPT09dC4kZ3JvdXBMYWJlbH0pO2lmKG4paWYodGhpcy53aG9sZUdyb3VwU2VsZWN0ZWQobikpe3RoaXMuJGVtaXQoXCJyZW1vdmVcIixuW3RoaXMuZ3JvdXBWYWx1ZXNdLHRoaXMuaWQpO3ZhciBpPXRoaXMuaW50ZXJuYWxWYWx1ZS5maWx0ZXIoZnVuY3Rpb24odCl7cmV0dXJuLTE9PT1uW2UuZ3JvdXBWYWx1ZXNdLmluZGV4T2YodCl9KTt0aGlzLiRlbWl0KFwiaW5wdXRcIixpLHRoaXMuaWQpfWVsc2V7dmFyIG89blt0aGlzLmdyb3VwVmFsdWVzXS5maWx0ZXIocih0aGlzLmlzU2VsZWN0ZWQpKTt0aGlzLiRlbWl0KFwic2VsZWN0XCIsbyx0aGlzLmlkKSx0aGlzLiRlbWl0KFwiaW5wdXRcIix0aGlzLmludGVybmFsVmFsdWUuY29uY2F0KG8pLHRoaXMuaWQpfX0sd2hvbGVHcm91cFNlbGVjdGVkOmZ1bmN0aW9uKHQpe3JldHVybiB0W3RoaXMuZ3JvdXBWYWx1ZXNdLmV2ZXJ5KHRoaXMuaXNTZWxlY3RlZCl9LHJlbW92ZUVsZW1lbnQ6ZnVuY3Rpb24odCl7dmFyIGU9IShhcmd1bWVudHMubGVuZ3RoPjEmJnZvaWQgMCE9PWFyZ3VtZW50c1sxXSl8fGFyZ3VtZW50c1sxXTtpZighdGhpcy5kaXNhYmxlZCl7aWYoIXRoaXMuYWxsb3dFbXB0eSYmdGhpcy5pbnRlcm5hbFZhbHVlLmxlbmd0aDw9MSlyZXR1cm4gdm9pZCB0aGlzLmRlYWN0aXZhdGUoKTt2YXIgaT1cIm9iamVjdFwiPT09bi5pKGMuYSkodCk/dGhpcy52YWx1ZUtleXMuaW5kZXhPZih0W3RoaXMudHJhY2tCeV0pOnRoaXMudmFsdWVLZXlzLmluZGV4T2YodCk7aWYodGhpcy4kZW1pdChcInJlbW92ZVwiLHQsdGhpcy5pZCksdGhpcy5tdWx0aXBsZSl7dmFyIHI9dGhpcy5pbnRlcm5hbFZhbHVlLnNsaWNlKDAsaSkuY29uY2F0KHRoaXMuaW50ZXJuYWxWYWx1ZS5zbGljZShpKzEpKTt0aGlzLiRlbWl0KFwiaW5wdXRcIixyLHRoaXMuaWQpfWVsc2UgdGhpcy4kZW1pdChcImlucHV0XCIsbnVsbCx0aGlzLmlkKTt0aGlzLmNsb3NlT25TZWxlY3QmJmUmJnRoaXMuZGVhY3RpdmF0ZSgpfX0scmVtb3ZlTGFzdEVsZW1lbnQ6ZnVuY3Rpb24oKXstMT09PXRoaXMuYmxvY2tLZXlzLmluZGV4T2YoXCJEZWxldGVcIikmJjA9PT10aGlzLnNlYXJjaC5sZW5ndGgmJkFycmF5LmlzQXJyYXkodGhpcy5pbnRlcm5hbFZhbHVlKSYmdGhpcy5yZW1vdmVFbGVtZW50KHRoaXMuaW50ZXJuYWxWYWx1ZVt0aGlzLmludGVybmFsVmFsdWUubGVuZ3RoLTFdLCExKX0sYWN0aXZhdGU6ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3RoaXMuaXNPcGVufHx0aGlzLmRpc2FibGVkfHwodGhpcy5hZGp1c3RQb3NpdGlvbigpLHRoaXMuZ3JvdXBWYWx1ZXMmJjA9PT10aGlzLnBvaW50ZXImJnRoaXMuZmlsdGVyZWRPcHRpb25zLmxlbmd0aCYmKHRoaXMucG9pbnRlcj0xKSx0aGlzLmlzT3Blbj0hMCx0aGlzLnNlYXJjaGFibGU/KHRoaXMucHJlc2VydmVTZWFyY2h8fCh0aGlzLnNlYXJjaD1cIlwiKSx0aGlzLiRuZXh0VGljayhmdW5jdGlvbigpe3JldHVybiB0LiRyZWZzLnNlYXJjaC5mb2N1cygpfSkpOnRoaXMuJGVsLmZvY3VzKCksdGhpcy4kZW1pdChcIm9wZW5cIix0aGlzLmlkKSl9LGRlYWN0aXZhdGU6ZnVuY3Rpb24oKXt0aGlzLmlzT3BlbiYmKHRoaXMuaXNPcGVuPSExLHRoaXMuc2VhcmNoYWJsZT90aGlzLiRyZWZzLnNlYXJjaC5ibHVyKCk6dGhpcy4kZWwuYmx1cigpLHRoaXMucHJlc2VydmVTZWFyY2h8fCh0aGlzLnNlYXJjaD1cIlwiKSx0aGlzLiRlbWl0KFwiY2xvc2VcIix0aGlzLmdldFZhbHVlKCksdGhpcy5pZCkpfSx0b2dnbGU6ZnVuY3Rpb24oKXt0aGlzLmlzT3Blbj90aGlzLmRlYWN0aXZhdGUoKTp0aGlzLmFjdGl2YXRlKCl9LGFkanVzdFBvc2l0aW9uOmZ1bmN0aW9uKCl7aWYoXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHdpbmRvdyl7dmFyIHQ9dGhpcy4kZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wLGU9d2luZG93LmlubmVySGVpZ2h0LXRoaXMuJGVsLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmJvdHRvbTtlPnRoaXMubWF4SGVpZ2h0fHxlPnR8fFwiYmVsb3dcIj09PXRoaXMub3BlbkRpcmVjdGlvbnx8XCJib3R0b21cIj09PXRoaXMub3BlbkRpcmVjdGlvbj8odGhpcy5wcmVmZmVyZWRPcGVuRGlyZWN0aW9uPVwiYmVsb3dcIix0aGlzLm9wdGltaXplZEhlaWdodD1NYXRoLm1pbihlLTQwLHRoaXMubWF4SGVpZ2h0KSk6KHRoaXMucHJlZmZlcmVkT3BlbkRpcmVjdGlvbj1cImFib3ZlXCIsdGhpcy5vcHRpbWl6ZWRIZWlnaHQ9TWF0aC5taW4odC00MCx0aGlzLm1heEhlaWdodCkpfX19fX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oNTQpLHI9KG4ubihpKSxuKDMxKSk7bi5uKHIpO2UuYT17ZGF0YTpmdW5jdGlvbigpe3JldHVybntwb2ludGVyOjAscG9pbnRlckRpcnR5OiExfX0scHJvcHM6e3Nob3dQb2ludGVyOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0sb3B0aW9uSGVpZ2h0Ont0eXBlOk51bWJlcixkZWZhdWx0OjQwfX0sY29tcHV0ZWQ6e3BvaW50ZXJQb3NpdGlvbjpmdW5jdGlvbigpe3JldHVybiB0aGlzLnBvaW50ZXIqdGhpcy5vcHRpb25IZWlnaHR9LHZpc2libGVFbGVtZW50czpmdW5jdGlvbigpe3JldHVybiB0aGlzLm9wdGltaXplZEhlaWdodC90aGlzLm9wdGlvbkhlaWdodH19LHdhdGNoOntmaWx0ZXJlZE9wdGlvbnM6ZnVuY3Rpb24oKXt0aGlzLnBvaW50ZXJBZGp1c3QoKX0saXNPcGVuOmZ1bmN0aW9uKCl7dGhpcy5wb2ludGVyRGlydHk9ITF9fSxtZXRob2RzOntvcHRpb25IaWdobGlnaHQ6ZnVuY3Rpb24odCxlKXtyZXR1cm57XCJtdWx0aXNlbGVjdF9fb3B0aW9uLS1oaWdobGlnaHRcIjp0PT09dGhpcy5wb2ludGVyJiZ0aGlzLnNob3dQb2ludGVyLFwibXVsdGlzZWxlY3RfX29wdGlvbi0tc2VsZWN0ZWRcIjp0aGlzLmlzU2VsZWN0ZWQoZSl9fSxncm91cEhpZ2hsaWdodDpmdW5jdGlvbih0LGUpe3ZhciBuPXRoaXM7aWYoIXRoaXMuZ3JvdXBTZWxlY3QpcmV0dXJuW1wibXVsdGlzZWxlY3RfX29wdGlvbi0tZ3JvdXBcIixcIm11bHRpc2VsZWN0X19vcHRpb24tLWRpc2FibGVkXCJdO3ZhciBpPXRoaXMub3B0aW9ucy5maW5kKGZ1bmN0aW9uKHQpe3JldHVybiB0W24uZ3JvdXBMYWJlbF09PT1lLiRncm91cExhYmVsfSk7cmV0dXJuW1wibXVsdGlzZWxlY3RfX29wdGlvbi0tZ3JvdXBcIix7XCJtdWx0aXNlbGVjdF9fb3B0aW9uLS1oaWdobGlnaHRcIjp0PT09dGhpcy5wb2ludGVyJiZ0aGlzLnNob3dQb2ludGVyfSx7XCJtdWx0aXNlbGVjdF9fb3B0aW9uLS1ncm91cC1zZWxlY3RlZFwiOnRoaXMud2hvbGVHcm91cFNlbGVjdGVkKGkpfV19LGFkZFBvaW50ZXJFbGVtZW50OmZ1bmN0aW9uKCl7dmFyIHQ9YXJndW1lbnRzLmxlbmd0aD4wJiZ2b2lkIDAhPT1hcmd1bWVudHNbMF0/YXJndW1lbnRzWzBdOlwiRW50ZXJcIixlPXQua2V5O3RoaXMuZmlsdGVyZWRPcHRpb25zLmxlbmd0aD4wJiZ0aGlzLnNlbGVjdCh0aGlzLmZpbHRlcmVkT3B0aW9uc1t0aGlzLnBvaW50ZXJdLGUpLHRoaXMucG9pbnRlclJlc2V0KCl9LHBvaW50ZXJGb3J3YXJkOmZ1bmN0aW9uKCl7dGhpcy5wb2ludGVyPHRoaXMuZmlsdGVyZWRPcHRpb25zLmxlbmd0aC0xJiYodGhpcy5wb2ludGVyKyssdGhpcy4kcmVmcy5saXN0LnNjcm9sbFRvcDw9dGhpcy5wb2ludGVyUG9zaXRpb24tKHRoaXMudmlzaWJsZUVsZW1lbnRzLTEpKnRoaXMub3B0aW9uSGVpZ2h0JiYodGhpcy4kcmVmcy5saXN0LnNjcm9sbFRvcD10aGlzLnBvaW50ZXJQb3NpdGlvbi0odGhpcy52aXNpYmxlRWxlbWVudHMtMSkqdGhpcy5vcHRpb25IZWlnaHQpLHRoaXMuZmlsdGVyZWRPcHRpb25zW3RoaXMucG9pbnRlcl0mJnRoaXMuZmlsdGVyZWRPcHRpb25zW3RoaXMucG9pbnRlcl0uJGlzTGFiZWwmJiF0aGlzLmdyb3VwU2VsZWN0JiZ0aGlzLnBvaW50ZXJGb3J3YXJkKCkpLHRoaXMucG9pbnRlckRpcnR5PSEwfSxwb2ludGVyQmFja3dhcmQ6ZnVuY3Rpb24oKXt0aGlzLnBvaW50ZXI+MD8odGhpcy5wb2ludGVyLS0sdGhpcy4kcmVmcy5saXN0LnNjcm9sbFRvcD49dGhpcy5wb2ludGVyUG9zaXRpb24mJih0aGlzLiRyZWZzLmxpc3Quc2Nyb2xsVG9wPXRoaXMucG9pbnRlclBvc2l0aW9uKSx0aGlzLmZpbHRlcmVkT3B0aW9uc1t0aGlzLnBvaW50ZXJdJiZ0aGlzLmZpbHRlcmVkT3B0aW9uc1t0aGlzLnBvaW50ZXJdLiRpc0xhYmVsJiYhdGhpcy5ncm91cFNlbGVjdCYmdGhpcy5wb2ludGVyQmFja3dhcmQoKSk6dGhpcy5maWx0ZXJlZE9wdGlvbnNbdGhpcy5wb2ludGVyXSYmdGhpcy5maWx0ZXJlZE9wdGlvbnNbMF0uJGlzTGFiZWwmJiF0aGlzLmdyb3VwU2VsZWN0JiZ0aGlzLnBvaW50ZXJGb3J3YXJkKCksdGhpcy5wb2ludGVyRGlydHk9ITB9LHBvaW50ZXJSZXNldDpmdW5jdGlvbigpe3RoaXMuY2xvc2VPblNlbGVjdCYmKHRoaXMucG9pbnRlcj0wLHRoaXMuJHJlZnMubGlzdCYmKHRoaXMuJHJlZnMubGlzdC5zY3JvbGxUb3A9MCkpfSxwb2ludGVyQWRqdXN0OmZ1bmN0aW9uKCl7dGhpcy5wb2ludGVyPj10aGlzLmZpbHRlcmVkT3B0aW9ucy5sZW5ndGgtMSYmKHRoaXMucG9pbnRlcj10aGlzLmZpbHRlcmVkT3B0aW9ucy5sZW5ndGg/dGhpcy5maWx0ZXJlZE9wdGlvbnMubGVuZ3RoLTE6MCksdGhpcy5maWx0ZXJlZE9wdGlvbnMubGVuZ3RoPjAmJnRoaXMuZmlsdGVyZWRPcHRpb25zW3RoaXMucG9pbnRlcl0uJGlzTGFiZWwmJiF0aGlzLmdyb3VwU2VsZWN0JiZ0aGlzLnBvaW50ZXJGb3J3YXJkKCl9LHBvaW50ZXJTZXQ6ZnVuY3Rpb24odCl7dGhpcy5wb2ludGVyPXQsdGhpcy5wb2ludGVyRGlydHk9ITB9fX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDM2KSxyPW4oNzQpLG89bigxNSkscz1uKDE4KTt0LmV4cG9ydHM9big3MikoQXJyYXksXCJBcnJheVwiLGZ1bmN0aW9uKHQsZSl7dGhpcy5fdD1zKHQpLHRoaXMuX2k9MCx0aGlzLl9rPWV9LGZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5fdCxlPXRoaXMuX2ssbj10aGlzLl9pKys7cmV0dXJuIXR8fG4+PXQubGVuZ3RoPyh0aGlzLl90PXZvaWQgMCxyKDEpKTpcImtleXNcIj09ZT9yKDAsbik6XCJ2YWx1ZXNcIj09ZT9yKDAsdFtuXSk6cigwLFtuLHRbbl1dKX0sXCJ2YWx1ZXNcIiksby5Bcmd1bWVudHM9by5BcnJheSxpKFwia2V5c1wiKSxpKFwidmFsdWVzXCIpLGkoXCJlbnRyaWVzXCIpfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGk9bigzMSkscj0obi5uKGkpLG4oMzIpKSxvPW4oMzMpO2UuYT17bmFtZTpcInZ1ZS1tdWx0aXNlbGVjdFwiLG1peGluczpbci5hLG8uYV0scHJvcHM6e25hbWU6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJcIn0sc2VsZWN0TGFiZWw6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJQcmVzcyBlbnRlciB0byBzZWxlY3RcIn0sc2VsZWN0R3JvdXBMYWJlbDp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIlByZXNzIGVudGVyIHRvIHNlbGVjdCBncm91cFwifSxzZWxlY3RlZExhYmVsOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiU2VsZWN0ZWRcIn0sZGVzZWxlY3RMYWJlbDp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIlByZXNzIGVudGVyIHRvIHJlbW92ZVwifSxkZXNlbGVjdEdyb3VwTGFiZWw6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJQcmVzcyBlbnRlciB0byBkZXNlbGVjdCBncm91cFwifSxzaG93TGFiZWxzOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0sbGltaXQ6e3R5cGU6TnVtYmVyLGRlZmF1bHQ6OTk5OTl9LG1heEhlaWdodDp7dHlwZTpOdW1iZXIsZGVmYXVsdDozMDB9LGxpbWl0VGV4dDp7dHlwZTpGdW5jdGlvbixkZWZhdWx0OmZ1bmN0aW9uKHQpe3JldHVyblwiYW5kIFwiLmNvbmNhdCh0LFwiIG1vcmVcIil9fSxsb2FkaW5nOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0sZGlzYWJsZWQ6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxvcGVuRGlyZWN0aW9uOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiXCJ9LHNob3dOb09wdGlvbnM6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiEwfSxzaG93Tm9SZXN1bHRzOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0sdGFiaW5kZXg6e3R5cGU6TnVtYmVyLGRlZmF1bHQ6MH19LGNvbXB1dGVkOntpc1NpbmdsZUxhYmVsVmlzaWJsZTpmdW5jdGlvbigpe3JldHVybiB0aGlzLnNpbmdsZVZhbHVlJiYoIXRoaXMuaXNPcGVufHwhdGhpcy5zZWFyY2hhYmxlKSYmIXRoaXMudmlzaWJsZVZhbHVlcy5sZW5ndGh9LGlzUGxhY2Vob2xkZXJWaXNpYmxlOmZ1bmN0aW9uKCl7cmV0dXJuISh0aGlzLmludGVybmFsVmFsdWUubGVuZ3RofHx0aGlzLnNlYXJjaGFibGUmJnRoaXMuaXNPcGVuKX0sdmlzaWJsZVZhbHVlczpmdW5jdGlvbigpe3JldHVybiB0aGlzLm11bHRpcGxlP3RoaXMuaW50ZXJuYWxWYWx1ZS5zbGljZSgwLHRoaXMubGltaXQpOltdfSxzaW5nbGVWYWx1ZTpmdW5jdGlvbigpe3JldHVybiB0aGlzLmludGVybmFsVmFsdWVbMF19LGRlc2VsZWN0TGFiZWxUZXh0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuc2hvd0xhYmVscz90aGlzLmRlc2VsZWN0TGFiZWw6XCJcIn0sZGVzZWxlY3RHcm91cExhYmVsVGV4dDpmdW5jdGlvbigpe3JldHVybiB0aGlzLnNob3dMYWJlbHM/dGhpcy5kZXNlbGVjdEdyb3VwTGFiZWw6XCJcIn0sc2VsZWN0TGFiZWxUZXh0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuc2hvd0xhYmVscz90aGlzLnNlbGVjdExhYmVsOlwiXCJ9LHNlbGVjdEdyb3VwTGFiZWxUZXh0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuc2hvd0xhYmVscz90aGlzLnNlbGVjdEdyb3VwTGFiZWw6XCJcIn0sc2VsZWN0ZWRMYWJlbFRleHQ6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5zaG93TGFiZWxzP3RoaXMuc2VsZWN0ZWRMYWJlbDpcIlwifSxpbnB1dFN0eWxlOmZ1bmN0aW9uKCl7aWYodGhpcy5zZWFyY2hhYmxlfHx0aGlzLm11bHRpcGxlJiZ0aGlzLnZhbHVlJiZ0aGlzLnZhbHVlLmxlbmd0aClyZXR1cm4gdGhpcy5pc09wZW4/e3dpZHRoOlwiYXV0b1wifTp7d2lkdGg6XCIwXCIscG9zaXRpb246XCJhYnNvbHV0ZVwiLHBhZGRpbmc6XCIwXCJ9fSxjb250ZW50U3R5bGU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5vcHRpb25zLmxlbmd0aD97ZGlzcGxheTpcImlubGluZS1ibG9ja1wifTp7ZGlzcGxheTpcImJsb2NrXCJ9fSxpc0Fib3ZlOmZ1bmN0aW9uKCl7cmV0dXJuXCJhYm92ZVwiPT09dGhpcy5vcGVuRGlyZWN0aW9ufHxcInRvcFwiPT09dGhpcy5vcGVuRGlyZWN0aW9ufHxcImJlbG93XCIhPT10aGlzLm9wZW5EaXJlY3Rpb24mJlwiYm90dG9tXCIhPT10aGlzLm9wZW5EaXJlY3Rpb24mJlwiYWJvdmVcIj09PXRoaXMucHJlZmZlcmVkT3BlbkRpcmVjdGlvbn0sc2hvd1NlYXJjaElucHV0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuc2VhcmNoYWJsZSYmKCF0aGlzLmhhc1NpbmdsZVNlbGVjdGVkU2xvdHx8IXRoaXMudmlzaWJsZVNpbmdsZVZhbHVlJiYwIT09dGhpcy52aXNpYmxlU2luZ2xlVmFsdWV8fHRoaXMuaXNPcGVuKX19fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMSkoXCJ1bnNjb3BhYmxlc1wiKSxyPUFycmF5LnByb3RvdHlwZTt2b2lkIDA9PXJbaV0mJm4oOCkocixpLHt9KSx0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cltpXVt0XT0hMH19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDE4KSxyPW4oMTkpLG89big4NSk7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbihlLG4scyl7dmFyIHUsYT1pKGUpLGw9cihhLmxlbmd0aCksYz1vKHMsbCk7aWYodCYmbiE9bil7Zm9yKDtsPmM7KWlmKCh1PWFbYysrXSkhPXUpcmV0dXJuITB9ZWxzZSBmb3IoO2w+YztjKyspaWYoKHR8fGMgaW4gYSkmJmFbY109PT1uKXJldHVybiB0fHxjfHwwO3JldHVybiF0JiYtMX19fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big5KSxyPW4oMSkoXCJ0b1N0cmluZ1RhZ1wiKSxvPVwiQXJndW1lbnRzXCI9PWkoZnVuY3Rpb24oKXtyZXR1cm4gYXJndW1lbnRzfSgpKSxzPWZ1bmN0aW9uKHQsZSl7dHJ5e3JldHVybiB0W2VdfWNhdGNoKHQpe319O3QuZXhwb3J0cz1mdW5jdGlvbih0KXt2YXIgZSxuLHU7cmV0dXJuIHZvaWQgMD09PXQ/XCJVbmRlZmluZWRcIjpudWxsPT09dD9cIk51bGxcIjpcInN0cmluZ1wiPT10eXBlb2Yobj1zKGU9T2JqZWN0KHQpLHIpKT9uOm8/aShlKTpcIk9iamVjdFwiPT0odT1pKGUpKSYmXCJmdW5jdGlvblwiPT10eXBlb2YgZS5jYWxsZWU/XCJBcmd1bWVudHNcIjp1fX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMik7dC5leHBvcnRzPWZ1bmN0aW9uKCl7dmFyIHQ9aSh0aGlzKSxlPVwiXCI7cmV0dXJuIHQuZ2xvYmFsJiYoZSs9XCJnXCIpLHQuaWdub3JlQ2FzZSYmKGUrPVwiaVwiKSx0Lm11bHRpbGluZSYmKGUrPVwibVwiKSx0LnVuaWNvZGUmJihlKz1cInVcIiksdC5zdGlja3kmJihlKz1cInlcIiksZX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDApLmRvY3VtZW50O3QuZXhwb3J0cz1pJiZpLmRvY3VtZW50RWxlbWVudH0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz0hbig0KSYmIW4oNykoZnVuY3Rpb24oKXtyZXR1cm4gNyE9T2JqZWN0LmRlZmluZVByb3BlcnR5KG4oMjEpKFwiZGl2XCIpLFwiYVwiLHtnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gN319KS5hfSl9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDkpO3QuZXhwb3J0cz1BcnJheS5pc0FycmF5fHxmdW5jdGlvbih0KXtyZXR1cm5cIkFycmF5XCI9PWkodCl9fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gaSh0KXt2YXIgZSxuO3RoaXMucHJvbWlzZT1uZXcgdChmdW5jdGlvbih0LGkpe2lmKHZvaWQgMCE9PWV8fHZvaWQgMCE9PW4pdGhyb3cgVHlwZUVycm9yKFwiQmFkIFByb21pc2UgY29uc3RydWN0b3JcIik7ZT10LG49aX0pLHRoaXMucmVzb2x2ZT1yKGUpLHRoaXMucmVqZWN0PXIobil9dmFyIHI9bigxNCk7dC5leHBvcnRzLmY9ZnVuY3Rpb24odCl7cmV0dXJuIG5ldyBpKHQpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMikscj1uKDc2KSxvPW4oMjIpLHM9bigyNykoXCJJRV9QUk9UT1wiKSx1PWZ1bmN0aW9uKCl7fSxhPWZ1bmN0aW9uKCl7dmFyIHQsZT1uKDIxKShcImlmcmFtZVwiKSxpPW8ubGVuZ3RoO2ZvcihlLnN0eWxlLmRpc3BsYXk9XCJub25lXCIsbig0MCkuYXBwZW5kQ2hpbGQoZSksZS5zcmM9XCJqYXZhc2NyaXB0OlwiLHQ9ZS5jb250ZW50V2luZG93LmRvY3VtZW50LHQub3BlbigpLHQud3JpdGUoXCI8c2NyaXB0PmRvY3VtZW50LkY9T2JqZWN0PFxcL3NjcmlwdD5cIiksdC5jbG9zZSgpLGE9dC5GO2ktLTspZGVsZXRlIGEucHJvdG90eXBlW29baV1dO3JldHVybiBhKCl9O3QuZXhwb3J0cz1PYmplY3QuY3JlYXRlfHxmdW5jdGlvbih0LGUpe3ZhciBuO3JldHVybiBudWxsIT09dD8odS5wcm90b3R5cGU9aSh0KSxuPW5ldyB1LHUucHJvdG90eXBlPW51bGwsbltzXT10KTpuPWEoKSx2b2lkIDA9PT1lP246cihuLGUpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNzkpLHI9bigyNSksbz1uKDE4KSxzPW4oMjkpLHU9bigxMiksYT1uKDQxKSxsPU9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3I7ZS5mPW4oNCk/bDpmdW5jdGlvbih0LGUpe2lmKHQ9byh0KSxlPXMoZSwhMCksYSl0cnl7cmV0dXJuIGwodCxlKX1jYXRjaCh0KXt9aWYodSh0LGUpKXJldHVybiByKCFpLmYuY2FsbCh0LGUpLHRbZV0pfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTIpLHI9bigxOCksbz1uKDM3KSghMSkscz1uKDI3KShcIklFX1BST1RPXCIpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUpe3ZhciBuLHU9cih0KSxhPTAsbD1bXTtmb3IobiBpbiB1KW4hPXMmJmkodSxuKSYmbC5wdXNoKG4pO2Zvcig7ZS5sZW5ndGg+YTspaSh1LG49ZVthKytdKSYmKH5vKGwsbil8fGwucHVzaChuKSk7cmV0dXJuIGx9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big0Nikscj1uKDIyKTt0LmV4cG9ydHM9T2JqZWN0LmtleXN8fGZ1bmN0aW9uKHQpe3JldHVybiBpKHQscil9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigyKSxyPW4oNSksbz1uKDQzKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtpZihpKHQpLHIoZSkmJmUuY29uc3RydWN0b3I9PT10KXJldHVybiBlO3ZhciBuPW8uZih0KTtyZXR1cm4oMCxuLnJlc29sdmUpKGUpLG4ucHJvbWlzZX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEwKSxyPW4oMCksbz1yW1wiX19jb3JlLWpzX3NoYXJlZF9fXCJdfHwocltcIl9fY29yZS1qc19zaGFyZWRfX1wiXT17fSk7KHQuZXhwb3J0cz1mdW5jdGlvbih0LGUpe3JldHVybiBvW3RdfHwob1t0XT12b2lkIDAhPT1lP2U6e30pfSkoXCJ2ZXJzaW9uc1wiLFtdKS5wdXNoKHt2ZXJzaW9uOmkudmVyc2lvbixtb2RlOm4oMjQpP1wicHVyZVwiOlwiZ2xvYmFsXCIsY29weXJpZ2h0OlwiwqkgMjAxOCBEZW5pcyBQdXNoa2FyZXYgKHpsb2lyb2NrLnJ1KVwifSl9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDIpLHI9bigxNCksbz1uKDEpKFwic3BlY2llc1wiKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXt2YXIgbixzPWkodCkuY29uc3RydWN0b3I7cmV0dXJuIHZvaWQgMD09PXN8fHZvaWQgMD09KG49aShzKVtvXSk/ZTpyKG4pfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMykscj1uKDE2KSxvPW4oNykscz1uKDg0KSx1PVwiW1wiK3MrXCJdXCIsYT1cIuKAi8KFXCIsbD1SZWdFeHAoXCJeXCIrdSt1K1wiKlwiKSxjPVJlZ0V4cCh1K3UrXCIqJFwiKSxmPWZ1bmN0aW9uKHQsZSxuKXt2YXIgcj17fSx1PW8oZnVuY3Rpb24oKXtyZXR1cm4hIXNbdF0oKXx8YVt0XSgpIT1hfSksbD1yW3RdPXU/ZShwKTpzW3RdO24mJihyW25dPWwpLGkoaS5QK2kuRip1LFwiU3RyaW5nXCIscil9LHA9Zi50cmltPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIHQ9U3RyaW5nKHIodCkpLDEmZSYmKHQ9dC5yZXBsYWNlKGwsXCJcIikpLDImZSYmKHQ9dC5yZXBsYWNlKGMsXCJcIikpLHR9O3QuZXhwb3J0cz1mfSxmdW5jdGlvbih0LGUsbil7dmFyIGkscixvLHM9bigxMSksdT1uKDY4KSxhPW4oNDApLGw9bigyMSksYz1uKDApLGY9Yy5wcm9jZXNzLHA9Yy5zZXRJbW1lZGlhdGUsaD1jLmNsZWFySW1tZWRpYXRlLGQ9Yy5NZXNzYWdlQ2hhbm5lbCx2PWMuRGlzcGF0Y2gsZz0wLG09e30seT1mdW5jdGlvbigpe3ZhciB0PSt0aGlzO2lmKG0uaGFzT3duUHJvcGVydHkodCkpe3ZhciBlPW1bdF07ZGVsZXRlIG1bdF0sZSgpfX0sYj1mdW5jdGlvbih0KXt5LmNhbGwodC5kYXRhKX07cCYmaHx8KHA9ZnVuY3Rpb24odCl7Zm9yKHZhciBlPVtdLG49MTthcmd1bWVudHMubGVuZ3RoPm47KWUucHVzaChhcmd1bWVudHNbbisrXSk7cmV0dXJuIG1bKytnXT1mdW5jdGlvbigpe3UoXCJmdW5jdGlvblwiPT10eXBlb2YgdD90OkZ1bmN0aW9uKHQpLGUpfSxpKGcpLGd9LGg9ZnVuY3Rpb24odCl7ZGVsZXRlIG1bdF19LFwicHJvY2Vzc1wiPT1uKDkpKGYpP2k9ZnVuY3Rpb24odCl7Zi5uZXh0VGljayhzKHksdCwxKSl9OnYmJnYubm93P2k9ZnVuY3Rpb24odCl7di5ub3cocyh5LHQsMSkpfTpkPyhyPW5ldyBkLG89ci5wb3J0MixyLnBvcnQxLm9ubWVzc2FnZT1iLGk9cyhvLnBvc3RNZXNzYWdlLG8sMSkpOmMuYWRkRXZlbnRMaXN0ZW5lciYmXCJmdW5jdGlvblwiPT10eXBlb2YgcG9zdE1lc3NhZ2UmJiFjLmltcG9ydFNjcmlwdHM/KGk9ZnVuY3Rpb24odCl7Yy5wb3N0TWVzc2FnZSh0K1wiXCIsXCIqXCIpfSxjLmFkZEV2ZW50TGlzdGVuZXIoXCJtZXNzYWdlXCIsYiwhMSkpOmk9XCJvbnJlYWR5c3RhdGVjaGFuZ2VcImluIGwoXCJzY3JpcHRcIik/ZnVuY3Rpb24odCl7YS5hcHBlbmRDaGlsZChsKFwic2NyaXB0XCIpKS5vbnJlYWR5c3RhdGVjaGFuZ2U9ZnVuY3Rpb24oKXthLnJlbW92ZUNoaWxkKHRoaXMpLHkuY2FsbCh0KX19OmZ1bmN0aW9uKHQpe3NldFRpbWVvdXQocyh5LHQsMSksMCl9KSx0LmV4cG9ydHM9e3NldDpwLGNsZWFyOmh9fSxmdW5jdGlvbih0LGUpe3ZhciBuPU1hdGguY2VpbCxpPU1hdGguZmxvb3I7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBpc05hTih0PSt0KT8wOih0PjA/aTpuKSh0KX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDMpLHI9bigyMCkoNSksbz0hMDtcImZpbmRcImluW10mJkFycmF5KDEpLmZpbmQoZnVuY3Rpb24oKXtvPSExfSksaShpLlAraS5GKm8sXCJBcnJheVwiLHtmaW5kOmZ1bmN0aW9uKHQpe3JldHVybiByKHRoaXMsdCxhcmd1bWVudHMubGVuZ3RoPjE/YXJndW1lbnRzWzFdOnZvaWQgMCl9fSksbigzNikoXCJmaW5kXCIpfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGkscixvLHMsdT1uKDI0KSxhPW4oMCksbD1uKDExKSxjPW4oMzgpLGY9bigzKSxwPW4oNSksaD1uKDE0KSxkPW4oNjEpLHY9big2NiksZz1uKDUwKSxtPW4oNTIpLnNldCx5PW4oNzUpKCksYj1uKDQzKSxfPW4oODApLHg9big4Niksdz1uKDQ4KSxTPWEuVHlwZUVycm9yLE89YS5wcm9jZXNzLEw9TyYmTy52ZXJzaW9ucyxQPUwmJkwudjh8fFwiXCIsaz1hLlByb21pc2UsVD1cInByb2Nlc3NcIj09YyhPKSxFPWZ1bmN0aW9uKCl7fSxWPXI9Yi5mLEE9ISFmdW5jdGlvbigpe3RyeXt2YXIgdD1rLnJlc29sdmUoMSksZT0odC5jb25zdHJ1Y3Rvcj17fSlbbigxKShcInNwZWNpZXNcIildPWZ1bmN0aW9uKHQpe3QoRSxFKX07cmV0dXJuKFR8fFwiZnVuY3Rpb25cIj09dHlwZW9mIFByb21pc2VSZWplY3Rpb25FdmVudCkmJnQudGhlbihFKWluc3RhbmNlb2YgZSYmMCE9PVAuaW5kZXhPZihcIjYuNlwiKSYmLTE9PT14LmluZGV4T2YoXCJDaHJvbWUvNjZcIil9Y2F0Y2godCl7fX0oKSxDPWZ1bmN0aW9uKHQpe3ZhciBlO3JldHVybiEoIXAodCl8fFwiZnVuY3Rpb25cIiE9dHlwZW9mKGU9dC50aGVuKSkmJmV9LGo9ZnVuY3Rpb24odCxlKXtpZighdC5fbil7dC5fbj0hMDt2YXIgbj10Ll9jO3koZnVuY3Rpb24oKXtmb3IodmFyIGk9dC5fdixyPTE9PXQuX3Msbz0wO24ubGVuZ3RoPm87KSFmdW5jdGlvbihlKXt2YXIgbixvLHMsdT1yP2Uub2s6ZS5mYWlsLGE9ZS5yZXNvbHZlLGw9ZS5yZWplY3QsYz1lLmRvbWFpbjt0cnl7dT8ocnx8KDI9PXQuX2gmJiQodCksdC5faD0xKSwhMD09PXU/bj1pOihjJiZjLmVudGVyKCksbj11KGkpLGMmJihjLmV4aXQoKSxzPSEwKSksbj09PWUucHJvbWlzZT9sKFMoXCJQcm9taXNlLWNoYWluIGN5Y2xlXCIpKToobz1DKG4pKT9vLmNhbGwobixhLGwpOmEobikpOmwoaSl9Y2F0Y2godCl7YyYmIXMmJmMuZXhpdCgpLGwodCl9fShuW28rK10pO3QuX2M9W10sdC5fbj0hMSxlJiYhdC5faCYmTih0KX0pfX0sTj1mdW5jdGlvbih0KXttLmNhbGwoYSxmdW5jdGlvbigpe3ZhciBlLG4saSxyPXQuX3Ysbz1EKHQpO2lmKG8mJihlPV8oZnVuY3Rpb24oKXtUP08uZW1pdChcInVuaGFuZGxlZFJlamVjdGlvblwiLHIsdCk6KG49YS5vbnVuaGFuZGxlZHJlamVjdGlvbik/bih7cHJvbWlzZTp0LHJlYXNvbjpyfSk6KGk9YS5jb25zb2xlKSYmaS5lcnJvciYmaS5lcnJvcihcIlVuaGFuZGxlZCBwcm9taXNlIHJlamVjdGlvblwiLHIpfSksdC5faD1UfHxEKHQpPzI6MSksdC5fYT12b2lkIDAsbyYmZS5lKXRocm93IGUudn0pfSxEPWZ1bmN0aW9uKHQpe3JldHVybiAxIT09dC5faCYmMD09PSh0Ll9hfHx0Ll9jKS5sZW5ndGh9LCQ9ZnVuY3Rpb24odCl7bS5jYWxsKGEsZnVuY3Rpb24oKXt2YXIgZTtUP08uZW1pdChcInJlamVjdGlvbkhhbmRsZWRcIix0KTooZT1hLm9ucmVqZWN0aW9uaGFuZGxlZCkmJmUoe3Byb21pc2U6dCxyZWFzb246dC5fdn0pfSl9LE09ZnVuY3Rpb24odCl7dmFyIGU9dGhpcztlLl9kfHwoZS5fZD0hMCxlPWUuX3d8fGUsZS5fdj10LGUuX3M9MixlLl9hfHwoZS5fYT1lLl9jLnNsaWNlKCkpLGooZSwhMCkpfSxGPWZ1bmN0aW9uKHQpe3ZhciBlLG49dGhpcztpZighbi5fZCl7bi5fZD0hMCxuPW4uX3d8fG47dHJ5e2lmKG49PT10KXRocm93IFMoXCJQcm9taXNlIGNhbid0IGJlIHJlc29sdmVkIGl0c2VsZlwiKTsoZT1DKHQpKT95KGZ1bmN0aW9uKCl7dmFyIGk9e193Om4sX2Q6ITF9O3RyeXtlLmNhbGwodCxsKEYsaSwxKSxsKE0saSwxKSl9Y2F0Y2godCl7TS5jYWxsKGksdCl9fSk6KG4uX3Y9dCxuLl9zPTEsaihuLCExKSl9Y2F0Y2godCl7TS5jYWxsKHtfdzpuLF9kOiExfSx0KX19fTtBfHwoaz1mdW5jdGlvbih0KXtkKHRoaXMsayxcIlByb21pc2VcIixcIl9oXCIpLGgodCksaS5jYWxsKHRoaXMpO3RyeXt0KGwoRix0aGlzLDEpLGwoTSx0aGlzLDEpKX1jYXRjaCh0KXtNLmNhbGwodGhpcyx0KX19LGk9ZnVuY3Rpb24odCl7dGhpcy5fYz1bXSx0aGlzLl9hPXZvaWQgMCx0aGlzLl9zPTAsdGhpcy5fZD0hMSx0aGlzLl92PXZvaWQgMCx0aGlzLl9oPTAsdGhpcy5fbj0hMX0saS5wcm90b3R5cGU9big4MSkoay5wcm90b3R5cGUse3RoZW46ZnVuY3Rpb24odCxlKXt2YXIgbj1WKGcodGhpcyxrKSk7cmV0dXJuIG4ub2s9XCJmdW5jdGlvblwiIT10eXBlb2YgdHx8dCxuLmZhaWw9XCJmdW5jdGlvblwiPT10eXBlb2YgZSYmZSxuLmRvbWFpbj1UP08uZG9tYWluOnZvaWQgMCx0aGlzLl9jLnB1c2gobiksdGhpcy5fYSYmdGhpcy5fYS5wdXNoKG4pLHRoaXMuX3MmJmoodGhpcywhMSksbi5wcm9taXNlfSxjYXRjaDpmdW5jdGlvbih0KXtyZXR1cm4gdGhpcy50aGVuKHZvaWQgMCx0KX19KSxvPWZ1bmN0aW9uKCl7dmFyIHQ9bmV3IGk7dGhpcy5wcm9taXNlPXQsdGhpcy5yZXNvbHZlPWwoRix0LDEpLHRoaXMucmVqZWN0PWwoTSx0LDEpfSxiLmY9Vj1mdW5jdGlvbih0KXtyZXR1cm4gdD09PWt8fHQ9PT1zP25ldyBvKHQpOnIodCl9KSxmKGYuRytmLlcrZi5GKiFBLHtQcm9taXNlOmt9KSxuKDI2KShrLFwiUHJvbWlzZVwiKSxuKDgzKShcIlByb21pc2VcIikscz1uKDEwKS5Qcm9taXNlLGYoZi5TK2YuRiohQSxcIlByb21pc2VcIix7cmVqZWN0OmZ1bmN0aW9uKHQpe3ZhciBlPVYodGhpcyk7cmV0dXJuKDAsZS5yZWplY3QpKHQpLGUucHJvbWlzZX19KSxmKGYuUytmLkYqKHV8fCFBKSxcIlByb21pc2VcIix7cmVzb2x2ZTpmdW5jdGlvbih0KXtyZXR1cm4gdyh1JiZ0aGlzPT09cz9rOnRoaXMsdCl9fSksZihmLlMrZi5GKiEoQSYmbig3MykoZnVuY3Rpb24odCl7ay5hbGwodCkuY2F0Y2goRSl9KSksXCJQcm9taXNlXCIse2FsbDpmdW5jdGlvbih0KXt2YXIgZT10aGlzLG49VihlKSxpPW4ucmVzb2x2ZSxyPW4ucmVqZWN0LG89XyhmdW5jdGlvbigpe3ZhciBuPVtdLG89MCxzPTE7dih0LCExLGZ1bmN0aW9uKHQpe3ZhciB1PW8rKyxhPSExO24ucHVzaCh2b2lkIDApLHMrKyxlLnJlc29sdmUodCkudGhlbihmdW5jdGlvbih0KXthfHwoYT0hMCxuW3VdPXQsLS1zfHxpKG4pKX0scil9KSwtLXN8fGkobil9KTtyZXR1cm4gby5lJiZyKG8udiksbi5wcm9taXNlfSxyYWNlOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMsbj1WKGUpLGk9bi5yZWplY3Qscj1fKGZ1bmN0aW9uKCl7dih0LCExLGZ1bmN0aW9uKHQpe2UucmVzb2x2ZSh0KS50aGVuKG4ucmVzb2x2ZSxpKX0pfSk7cmV0dXJuIHIuZSYmaShyLnYpLG4ucHJvbWlzZX19KX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMykscj1uKDEwKSxvPW4oMCkscz1uKDUwKSx1PW4oNDgpO2koaS5QK2kuUixcIlByb21pc2VcIix7ZmluYWxseTpmdW5jdGlvbih0KXt2YXIgZT1zKHRoaXMsci5Qcm9taXNlfHxvLlByb21pc2UpLG49XCJmdW5jdGlvblwiPT10eXBlb2YgdDtyZXR1cm4gdGhpcy50aGVuKG4/ZnVuY3Rpb24obil7cmV0dXJuIHUoZSx0KCkpLnRoZW4oZnVuY3Rpb24oKXtyZXR1cm4gbn0pfTp0LG4/ZnVuY3Rpb24obil7cmV0dXJuIHUoZSx0KCkpLnRoZW4oZnVuY3Rpb24oKXt0aHJvdyBufSl9OnQpfX0pfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gaSh0KXtuKDk5KX12YXIgcj1uKDM1KSxvPW4oMTAxKSxzPW4oMTAwKSx1PWksYT1zKHIuYSxvLmEsITEsdSxudWxsLG51bGwpO2UuYT1hLmV4cG9ydHN9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBpKHQsZSxuKXtyZXR1cm4gZSBpbiB0P09iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LGUse3ZhbHVlOm4sZW51bWVyYWJsZTohMCxjb25maWd1cmFibGU6ITAsd3JpdGFibGU6ITB9KTp0W2VdPW4sdH1lLmE9aX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGkodCl7cmV0dXJuKGk9XCJmdW5jdGlvblwiPT10eXBlb2YgU3ltYm9sJiZcInN5bWJvbFwiPT10eXBlb2YgU3ltYm9sLml0ZXJhdG9yP2Z1bmN0aW9uKHQpe3JldHVybiB0eXBlb2YgdH06ZnVuY3Rpb24odCl7cmV0dXJuIHQmJlwiZnVuY3Rpb25cIj09dHlwZW9mIFN5bWJvbCYmdC5jb25zdHJ1Y3Rvcj09PVN5bWJvbCYmdCE9PVN5bWJvbC5wcm90b3R5cGU/XCJzeW1ib2xcIjp0eXBlb2YgdH0pKHQpfWZ1bmN0aW9uIHIodCl7cmV0dXJuKHI9XCJmdW5jdGlvblwiPT10eXBlb2YgU3ltYm9sJiZcInN5bWJvbFwiPT09aShTeW1ib2wuaXRlcmF0b3IpP2Z1bmN0aW9uKHQpe3JldHVybiBpKHQpfTpmdW5jdGlvbih0KXtyZXR1cm4gdCYmXCJmdW5jdGlvblwiPT10eXBlb2YgU3ltYm9sJiZ0LmNvbnN0cnVjdG9yPT09U3ltYm9sJiZ0IT09U3ltYm9sLnByb3RvdHlwZT9cInN5bWJvbFwiOmkodCl9KSh0KX1lLmE9cn0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO09iamVjdC5kZWZpbmVQcm9wZXJ0eShlLFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pO3ZhciBpPW4oMzQpLHI9KG4ubihpKSxuKDU1KSksbz0obi5uKHIpLG4oNTYpKSxzPShuLm4obyksbig1NykpLHU9bigzMiksYT1uKDMzKTtuLmQoZSxcIk11bHRpc2VsZWN0XCIsZnVuY3Rpb24oKXtyZXR1cm4gcy5hfSksbi5kKGUsXCJtdWx0aXNlbGVjdE1peGluXCIsZnVuY3Rpb24oKXtyZXR1cm4gdS5hfSksbi5kKGUsXCJwb2ludGVyTWl4aW5cIixmdW5jdGlvbigpe3JldHVybiBhLmF9KSxlLmRlZmF1bHQ9cy5hfSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbixpKXtpZighKHQgaW5zdGFuY2VvZiBlKXx8dm9pZCAwIT09aSYmaSBpbiB0KXRocm93IFR5cGVFcnJvcihuK1wiOiBpbmNvcnJlY3QgaW52b2NhdGlvbiFcIik7cmV0dXJuIHR9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxNCkscj1uKDI4KSxvPW4oMjMpLHM9bigxOSk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuLHUsYSl7aShlKTt2YXIgbD1yKHQpLGM9byhsKSxmPXMobC5sZW5ndGgpLHA9YT9mLTE6MCxoPWE/LTE6MTtpZihuPDIpZm9yKDs7KXtpZihwIGluIGMpe3U9Y1twXSxwKz1oO2JyZWFrfWlmKHArPWgsYT9wPDA6Zjw9cCl0aHJvdyBUeXBlRXJyb3IoXCJSZWR1Y2Ugb2YgZW1wdHkgYXJyYXkgd2l0aCBubyBpbml0aWFsIHZhbHVlXCIpfWZvcig7YT9wPj0wOmY+cDtwKz1oKXAgaW4gYyYmKHU9ZSh1LGNbcF0scCxsKSk7cmV0dXJuIHV9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big1KSxyPW4oNDIpLG89bigxKShcInNwZWNpZXNcIik7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3ZhciBlO3JldHVybiByKHQpJiYoZT10LmNvbnN0cnVjdG9yLFwiZnVuY3Rpb25cIiE9dHlwZW9mIGV8fGUhPT1BcnJheSYmIXIoZS5wcm90b3R5cGUpfHwoZT12b2lkIDApLGkoZSkmJm51bGw9PT0oZT1lW29dKSYmKGU9dm9pZCAwKSksdm9pZCAwPT09ZT9BcnJheTplfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNjMpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUpe3JldHVybiBuZXcoaSh0KSkoZSl9fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGk9big4KSxyPW4oNiksbz1uKDcpLHM9bigxNiksdT1uKDEpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbil7dmFyIGE9dSh0KSxsPW4ocyxhLFwiXCJbdF0pLGM9bFswXSxmPWxbMV07byhmdW5jdGlvbigpe3ZhciBlPXt9O3JldHVybiBlW2FdPWZ1bmN0aW9uKCl7cmV0dXJuIDd9LDchPVwiXCJbdF0oZSl9KSYmKHIoU3RyaW5nLnByb3RvdHlwZSx0LGMpLGkoUmVnRXhwLnByb3RvdHlwZSxhLDI9PWU/ZnVuY3Rpb24odCxlKXtyZXR1cm4gZi5jYWxsKHQsdGhpcyxlKX06ZnVuY3Rpb24odCl7cmV0dXJuIGYuY2FsbCh0LHRoaXMpfSkpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTEpLHI9big3MCksbz1uKDY5KSxzPW4oMiksdT1uKDE5KSxhPW4oODcpLGw9e30sYz17fSxlPXQuZXhwb3J0cz1mdW5jdGlvbih0LGUsbixmLHApe3ZhciBoLGQsdixnLG09cD9mdW5jdGlvbigpe3JldHVybiB0fTphKHQpLHk9aShuLGYsZT8yOjEpLGI9MDtpZihcImZ1bmN0aW9uXCIhPXR5cGVvZiBtKXRocm93IFR5cGVFcnJvcih0K1wiIGlzIG5vdCBpdGVyYWJsZSFcIik7aWYobyhtKSl7Zm9yKGg9dSh0Lmxlbmd0aCk7aD5iO2IrKylpZigoZz1lP3kocyhkPXRbYl0pWzBdLGRbMV0pOnkodFtiXSkpPT09bHx8Zz09PWMpcmV0dXJuIGd9ZWxzZSBmb3Iodj1tLmNhbGwodCk7IShkPXYubmV4dCgpKS5kb25lOylpZigoZz1yKHYseSxkLnZhbHVlLGUpKT09PWx8fGc9PT1jKXJldHVybiBnfTtlLkJSRUFLPWwsZS5SRVRVUk49Y30sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNSkscj1uKDgyKS5zZXQ7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuKXt2YXIgbyxzPWUuY29uc3RydWN0b3I7cmV0dXJuIHMhPT1uJiZcImZ1bmN0aW9uXCI9PXR5cGVvZiBzJiYobz1zLnByb3RvdHlwZSkhPT1uLnByb3RvdHlwZSYmaShvKSYmciYmcih0LG8pLHR9fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbil7dmFyIGk9dm9pZCAwPT09bjtzd2l0Y2goZS5sZW5ndGgpe2Nhc2UgMDpyZXR1cm4gaT90KCk6dC5jYWxsKG4pO2Nhc2UgMTpyZXR1cm4gaT90KGVbMF0pOnQuY2FsbChuLGVbMF0pO2Nhc2UgMjpyZXR1cm4gaT90KGVbMF0sZVsxXSk6dC5jYWxsKG4sZVswXSxlWzFdKTtjYXNlIDM6cmV0dXJuIGk/dChlWzBdLGVbMV0sZVsyXSk6dC5jYWxsKG4sZVswXSxlWzFdLGVbMl0pO2Nhc2UgNDpyZXR1cm4gaT90KGVbMF0sZVsxXSxlWzJdLGVbM10pOnQuY2FsbChuLGVbMF0sZVsxXSxlWzJdLGVbM10pfXJldHVybiB0LmFwcGx5KG4sZSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxNSkscj1uKDEpKFwiaXRlcmF0b3JcIiksbz1BcnJheS5wcm90b3R5cGU7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiB2b2lkIDAhPT10JiYoaS5BcnJheT09PXR8fG9bcl09PT10KX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDIpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbixyKXt0cnl7cmV0dXJuIHI/ZShpKG4pWzBdLG5bMV0pOmUobil9Y2F0Y2goZSl7dmFyIG89dC5yZXR1cm47dGhyb3cgdm9pZCAwIT09byYmaShvLmNhbGwodCkpLGV9fX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oNDQpLHI9bigyNSksbz1uKDI2KSxzPXt9O24oOCkocyxuKDEpKFwiaXRlcmF0b3JcIiksZnVuY3Rpb24oKXtyZXR1cm4gdGhpc30pLHQuZXhwb3J0cz1mdW5jdGlvbih0LGUsbil7dC5wcm90b3R5cGU9aShzLHtuZXh0OnIoMSxuKX0pLG8odCxlK1wiIEl0ZXJhdG9yXCIpfX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMjQpLHI9bigzKSxvPW4oNikscz1uKDgpLHU9bigxNSksYT1uKDcxKSxsPW4oMjYpLGM9big3OCksZj1uKDEpKFwiaXRlcmF0b3JcIikscD0hKFtdLmtleXMmJlwibmV4dFwiaW5bXS5rZXlzKCkpLGg9ZnVuY3Rpb24oKXtyZXR1cm4gdGhpc307dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuLGQsdixnLG0pe2EobixlLGQpO3ZhciB5LGIsXyx4PWZ1bmN0aW9uKHQpe2lmKCFwJiZ0IGluIEwpcmV0dXJuIExbdF07c3dpdGNoKHQpe2Nhc2VcImtleXNcIjpjYXNlXCJ2YWx1ZXNcIjpyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4gbmV3IG4odGhpcyx0KX19cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuIG5ldyBuKHRoaXMsdCl9fSx3PWUrXCIgSXRlcmF0b3JcIixTPVwidmFsdWVzXCI9PXYsTz0hMSxMPXQucHJvdG90eXBlLFA9TFtmXXx8TFtcIkBAaXRlcmF0b3JcIl18fHYmJkxbdl0saz1QfHx4KHYpLFQ9dj9TP3goXCJlbnRyaWVzXCIpOms6dm9pZCAwLEU9XCJBcnJheVwiPT1lP0wuZW50cmllc3x8UDpQO2lmKEUmJihfPWMoRS5jYWxsKG5ldyB0KSkpIT09T2JqZWN0LnByb3RvdHlwZSYmXy5uZXh0JiYobChfLHcsITApLGl8fFwiZnVuY3Rpb25cIj09dHlwZW9mIF9bZl18fHMoXyxmLGgpKSxTJiZQJiZcInZhbHVlc1wiIT09UC5uYW1lJiYoTz0hMCxrPWZ1bmN0aW9uKCl7cmV0dXJuIFAuY2FsbCh0aGlzKX0pLGkmJiFtfHwhcCYmIU8mJkxbZl18fHMoTCxmLGspLHVbZV09ayx1W3ddPWgsdilpZih5PXt2YWx1ZXM6Uz9rOngoXCJ2YWx1ZXNcIiksa2V5czpnP2s6eChcImtleXNcIiksZW50cmllczpUfSxtKWZvcihiIGluIHkpYiBpbiBMfHxvKEwsYix5W2JdKTtlbHNlIHIoci5QK3IuRioocHx8TyksZSx5KTtyZXR1cm4geX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEpKFwiaXRlcmF0b3JcIikscj0hMTt0cnl7dmFyIG89WzddW2ldKCk7by5yZXR1cm49ZnVuY3Rpb24oKXtyPSEwfSxBcnJheS5mcm9tKG8sZnVuY3Rpb24oKXt0aHJvdyAyfSl9Y2F0Y2godCl7fXQuZXhwb3J0cz1mdW5jdGlvbih0LGUpe2lmKCFlJiYhcilyZXR1cm4hMTt2YXIgbj0hMTt0cnl7dmFyIG89WzddLHM9b1tpXSgpO3MubmV4dD1mdW5jdGlvbigpe3JldHVybntkb25lOm49ITB9fSxvW2ldPWZ1bmN0aW9uKCl7cmV0dXJuIHN9LHQobyl9Y2F0Y2godCl7fXJldHVybiBufX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtyZXR1cm57dmFsdWU6ZSxkb25lOiEhdH19fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigwKSxyPW4oNTIpLnNldCxvPWkuTXV0YXRpb25PYnNlcnZlcnx8aS5XZWJLaXRNdXRhdGlvbk9ic2VydmVyLHM9aS5wcm9jZXNzLHU9aS5Qcm9taXNlLGE9XCJwcm9jZXNzXCI9PW4oOSkocyk7dC5leHBvcnRzPWZ1bmN0aW9uKCl7dmFyIHQsZSxuLGw9ZnVuY3Rpb24oKXt2YXIgaSxyO2ZvcihhJiYoaT1zLmRvbWFpbikmJmkuZXhpdCgpO3Q7KXtyPXQuZm4sdD10Lm5leHQ7dHJ5e3IoKX1jYXRjaChpKXt0aHJvdyB0P24oKTplPXZvaWQgMCxpfX1lPXZvaWQgMCxpJiZpLmVudGVyKCl9O2lmKGEpbj1mdW5jdGlvbigpe3MubmV4dFRpY2sobCl9O2Vsc2UgaWYoIW98fGkubmF2aWdhdG9yJiZpLm5hdmlnYXRvci5zdGFuZGFsb25lKWlmKHUmJnUucmVzb2x2ZSl7dmFyIGM9dS5yZXNvbHZlKHZvaWQgMCk7bj1mdW5jdGlvbigpe2MudGhlbihsKX19ZWxzZSBuPWZ1bmN0aW9uKCl7ci5jYWxsKGksbCl9O2Vsc2V7dmFyIGY9ITAscD1kb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShcIlwiKTtuZXcgbyhsKS5vYnNlcnZlKHAse2NoYXJhY3RlckRhdGE6ITB9KSxuPWZ1bmN0aW9uKCl7cC5kYXRhPWY9IWZ9fXJldHVybiBmdW5jdGlvbihpKXt2YXIgcj17Zm46aSxuZXh0OnZvaWQgMH07ZSYmKGUubmV4dD1yKSx0fHwodD1yLG4oKSksZT1yfX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEzKSxyPW4oMiksbz1uKDQ3KTt0LmV4cG9ydHM9big0KT9PYmplY3QuZGVmaW5lUHJvcGVydGllczpmdW5jdGlvbih0LGUpe3IodCk7Zm9yKHZhciBuLHM9byhlKSx1PXMubGVuZ3RoLGE9MDt1PmE7KWkuZih0LG49c1thKytdLGVbbl0pO3JldHVybiB0fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNDYpLHI9bigyMikuY29uY2F0KFwibGVuZ3RoXCIsXCJwcm90b3R5cGVcIik7ZS5mPU9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzfHxmdW5jdGlvbih0KXtyZXR1cm4gaSh0LHIpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTIpLHI9bigyOCksbz1uKDI3KShcIklFX1BST1RPXCIpLHM9T2JqZWN0LnByb3RvdHlwZTt0LmV4cG9ydHM9T2JqZWN0LmdldFByb3RvdHlwZU9mfHxmdW5jdGlvbih0KXtyZXR1cm4gdD1yKHQpLGkodCxvKT90W29dOlwiZnVuY3Rpb25cIj09dHlwZW9mIHQuY29uc3RydWN0b3ImJnQgaW5zdGFuY2VvZiB0LmNvbnN0cnVjdG9yP3QuY29uc3RydWN0b3IucHJvdG90eXBlOnQgaW5zdGFuY2VvZiBPYmplY3Q/czpudWxsfX0sZnVuY3Rpb24odCxlKXtlLmY9e30ucHJvcGVydHlJc0VudW1lcmFibGV9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3RyeXtyZXR1cm57ZTohMSx2OnQoKX19Y2F0Y2godCl7cmV0dXJue2U6ITAsdjp0fX19fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big2KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4pe2Zvcih2YXIgciBpbiBlKWkodCxyLGVbcl0sbik7cmV0dXJuIHR9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big1KSxyPW4oMiksbz1mdW5jdGlvbih0LGUpe2lmKHIodCksIWkoZSkmJm51bGwhPT1lKXRocm93IFR5cGVFcnJvcihlK1wiOiBjYW4ndCBzZXQgYXMgcHJvdG90eXBlIVwiKX07dC5leHBvcnRzPXtzZXQ6T2JqZWN0LnNldFByb3RvdHlwZU9mfHwoXCJfX3Byb3RvX19cImlue30/ZnVuY3Rpb24odCxlLGkpe3RyeXtpPW4oMTEpKEZ1bmN0aW9uLmNhbGwsbig0NSkuZihPYmplY3QucHJvdG90eXBlLFwiX19wcm90b19fXCIpLnNldCwyKSxpKHQsW10pLGU9ISh0IGluc3RhbmNlb2YgQXJyYXkpfWNhdGNoKHQpe2U9ITB9cmV0dXJuIGZ1bmN0aW9uKHQsbil7cmV0dXJuIG8odCxuKSxlP3QuX19wcm90b19fPW46aSh0LG4pLHR9fSh7fSwhMSk6dm9pZCAwKSxjaGVjazpvfX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMCkscj1uKDEzKSxvPW4oNCkscz1uKDEpKFwic3BlY2llc1wiKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7dmFyIGU9aVt0XTtvJiZlJiYhZVtzXSYmci5mKGUscyx7Y29uZmlndXJhYmxlOiEwLGdldDpmdW5jdGlvbigpe3JldHVybiB0aGlzfX0pfX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9XCJcXHRcXG5cXHZcXGZcXHIgwqDhmoDhoI7igIDigIHigILigIPigITigIXigIbigIfigIjigInigIrigK/igZ/jgIBcXHUyMDI4XFx1MjAyOVxcdWZlZmZcIn0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNTMpLHI9TWF0aC5tYXgsbz1NYXRoLm1pbjt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtyZXR1cm4gdD1pKHQpLHQ8MD9yKHQrZSwwKTpvKHQsZSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigwKSxyPWkubmF2aWdhdG9yO3QuZXhwb3J0cz1yJiZyLnVzZXJBZ2VudHx8XCJcIn0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMzgpLHI9bigxKShcIml0ZXJhdG9yXCIpLG89bigxNSk7dC5leHBvcnRzPW4oMTApLmdldEl0ZXJhdG9yTWV0aG9kPWZ1bmN0aW9uKHQpe2lmKHZvaWQgMCE9dClyZXR1cm4gdFtyXXx8dFtcIkBAaXRlcmF0b3JcIl18fG9baSh0KV19fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGk9bigzKSxyPW4oMjApKDIpO2koaS5QK2kuRiohbigxNykoW10uZmlsdGVyLCEwKSxcIkFycmF5XCIse2ZpbHRlcjpmdW5jdGlvbih0KXtyZXR1cm4gcih0aGlzLHQsYXJndW1lbnRzWzFdKX19KX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMykscj1uKDM3KSghMSksbz1bXS5pbmRleE9mLHM9ISFvJiYxL1sxXS5pbmRleE9mKDEsLTApPDA7aShpLlAraS5GKihzfHwhbigxNykobykpLFwiQXJyYXlcIix7aW5kZXhPZjpmdW5jdGlvbih0KXtyZXR1cm4gcz9vLmFwcGx5KHRoaXMsYXJndW1lbnRzKXx8MDpyKHRoaXMsdCxhcmd1bWVudHNbMV0pfX0pfSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigzKTtpKGkuUyxcIkFycmF5XCIse2lzQXJyYXk6big0Mil9KX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMykscj1uKDIwKSgxKTtpKGkuUCtpLkYqIW4oMTcpKFtdLm1hcCwhMCksXCJBcnJheVwiLHttYXA6ZnVuY3Rpb24odCl7cmV0dXJuIHIodGhpcyx0LGFyZ3VtZW50c1sxXSl9fSl9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDMpLHI9big2Mik7aShpLlAraS5GKiFuKDE3KShbXS5yZWR1Y2UsITApLFwiQXJyYXlcIix7cmVkdWNlOmZ1bmN0aW9uKHQpe3JldHVybiByKHRoaXMsdCxhcmd1bWVudHMubGVuZ3RoLGFyZ3VtZW50c1sxXSwhMSl9fSl9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1EYXRlLnByb3RvdHlwZSxyPWkudG9TdHJpbmcsbz1pLmdldFRpbWU7bmV3IERhdGUoTmFOKStcIlwiIT1cIkludmFsaWQgRGF0ZVwiJiZuKDYpKGksXCJ0b1N0cmluZ1wiLGZ1bmN0aW9uKCl7dmFyIHQ9by5jYWxsKHRoaXMpO3JldHVybiB0PT09dD9yLmNhbGwodGhpcyk6XCJJbnZhbGlkIERhdGVcIn0pfSxmdW5jdGlvbih0LGUsbil7big0KSYmXCJnXCIhPS8uL2cuZmxhZ3MmJm4oMTMpLmYoUmVnRXhwLnByb3RvdHlwZSxcImZsYWdzXCIse2NvbmZpZ3VyYWJsZTohMCxnZXQ6bigzOSl9KX0sZnVuY3Rpb24odCxlLG4pe24oNjUpKFwic2VhcmNoXCIsMSxmdW5jdGlvbih0LGUsbil7cmV0dXJuW2Z1bmN0aW9uKG4pe1widXNlIHN0cmljdFwiO3ZhciBpPXQodGhpcykscj12b2lkIDA9PW4/dm9pZCAwOm5bZV07cmV0dXJuIHZvaWQgMCE9PXI/ci5jYWxsKG4saSk6bmV3IFJlZ0V4cChuKVtlXShTdHJpbmcoaSkpfSxuXX0pfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7big5NCk7dmFyIGk9bigyKSxyPW4oMzkpLG89big0KSxzPS8uLy50b1N0cmluZyx1PWZ1bmN0aW9uKHQpe24oNikoUmVnRXhwLnByb3RvdHlwZSxcInRvU3RyaW5nXCIsdCwhMCl9O24oNykoZnVuY3Rpb24oKXtyZXR1cm5cIi9hL2JcIiE9cy5jYWxsKHtzb3VyY2U6XCJhXCIsZmxhZ3M6XCJiXCJ9KX0pP3UoZnVuY3Rpb24oKXt2YXIgdD1pKHRoaXMpO3JldHVyblwiL1wiLmNvbmNhdCh0LnNvdXJjZSxcIi9cIixcImZsYWdzXCJpbiB0P3QuZmxhZ3M6IW8mJnQgaW5zdGFuY2VvZiBSZWdFeHA/ci5jYWxsKHQpOnZvaWQgMCl9KTpcInRvU3RyaW5nXCIhPXMubmFtZSYmdShmdW5jdGlvbigpe3JldHVybiBzLmNhbGwodGhpcyl9KX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO24oNTEpKFwidHJpbVwiLGZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbigpe3JldHVybiB0KHRoaXMsMyl9fSl9LGZ1bmN0aW9uKHQsZSxuKXtmb3IodmFyIGk9bigzNCkscj1uKDQ3KSxvPW4oNikscz1uKDApLHU9big4KSxhPW4oMTUpLGw9bigxKSxjPWwoXCJpdGVyYXRvclwiKSxmPWwoXCJ0b1N0cmluZ1RhZ1wiKSxwPWEuQXJyYXksaD17Q1NTUnVsZUxpc3Q6ITAsQ1NTU3R5bGVEZWNsYXJhdGlvbjohMSxDU1NWYWx1ZUxpc3Q6ITEsQ2xpZW50UmVjdExpc3Q6ITEsRE9NUmVjdExpc3Q6ITEsRE9NU3RyaW5nTGlzdDohMSxET01Ub2tlbkxpc3Q6ITAsRGF0YVRyYW5zZmVySXRlbUxpc3Q6ITEsRmlsZUxpc3Q6ITEsSFRNTEFsbENvbGxlY3Rpb246ITEsSFRNTENvbGxlY3Rpb246ITEsSFRNTEZvcm1FbGVtZW50OiExLEhUTUxTZWxlY3RFbGVtZW50OiExLE1lZGlhTGlzdDohMCxNaW1lVHlwZUFycmF5OiExLE5hbWVkTm9kZU1hcDohMSxOb2RlTGlzdDohMCxQYWludFJlcXVlc3RMaXN0OiExLFBsdWdpbjohMSxQbHVnaW5BcnJheTohMSxTVkdMZW5ndGhMaXN0OiExLFNWR051bWJlckxpc3Q6ITEsU1ZHUGF0aFNlZ0xpc3Q6ITEsU1ZHUG9pbnRMaXN0OiExLFNWR1N0cmluZ0xpc3Q6ITEsU1ZHVHJhbnNmb3JtTGlzdDohMSxTb3VyY2VCdWZmZXJMaXN0OiExLFN0eWxlU2hlZXRMaXN0OiEwLFRleHRUcmFja0N1ZUxpc3Q6ITEsVGV4dFRyYWNrTGlzdDohMSxUb3VjaExpc3Q6ITF9LGQ9cihoKSx2PTA7djxkLmxlbmd0aDt2Kyspe3ZhciBnLG09ZFt2XSx5PWhbbV0sYj1zW21dLF89YiYmYi5wcm90b3R5cGU7aWYoXyYmKF9bY118fHUoXyxjLHApLF9bZl18fHUoXyxmLG0pLGFbbV09cCx5KSlmb3IoZyBpbiBpKV9bZ118fG8oXyxnLGlbZ10sITApfX0sZnVuY3Rpb24odCxlKXt9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuLGkscixvKXt2YXIgcyx1PXQ9dHx8e30sYT10eXBlb2YgdC5kZWZhdWx0O1wib2JqZWN0XCIhPT1hJiZcImZ1bmN0aW9uXCIhPT1hfHwocz10LHU9dC5kZWZhdWx0KTt2YXIgbD1cImZ1bmN0aW9uXCI9PXR5cGVvZiB1P3Uub3B0aW9uczp1O2UmJihsLnJlbmRlcj1lLnJlbmRlcixsLnN0YXRpY1JlbmRlckZucz1lLnN0YXRpY1JlbmRlckZucyxsLl9jb21waWxlZD0hMCksbiYmKGwuZnVuY3Rpb25hbD0hMCksciYmKGwuX3Njb3BlSWQ9cik7dmFyIGM7aWYobz8oYz1mdW5jdGlvbih0KXt0PXR8fHRoaXMuJHZub2RlJiZ0aGlzLiR2bm9kZS5zc3JDb250ZXh0fHx0aGlzLnBhcmVudCYmdGhpcy5wYXJlbnQuJHZub2RlJiZ0aGlzLnBhcmVudC4kdm5vZGUuc3NyQ29udGV4dCx0fHxcInVuZGVmaW5lZFwiPT10eXBlb2YgX19WVUVfU1NSX0NPTlRFWFRfX3x8KHQ9X19WVUVfU1NSX0NPTlRFWFRfXyksaSYmaS5jYWxsKHRoaXMsdCksdCYmdC5fcmVnaXN0ZXJlZENvbXBvbmVudHMmJnQuX3JlZ2lzdGVyZWRDb21wb25lbnRzLmFkZChvKX0sbC5fc3NyUmVnaXN0ZXI9Yyk6aSYmKGM9aSksYyl7dmFyIGY9bC5mdW5jdGlvbmFsLHA9Zj9sLnJlbmRlcjpsLmJlZm9yZUNyZWF0ZTtmPyhsLl9pbmplY3RTdHlsZXM9YyxsLnJlbmRlcj1mdW5jdGlvbih0LGUpe3JldHVybiBjLmNhbGwoZSkscCh0LGUpfSk6bC5iZWZvcmVDcmVhdGU9cD9bXS5jb25jYXQocCxjKTpbY119cmV0dXJue2VzTW9kdWxlOnMsZXhwb3J0czp1LG9wdGlvbnM6bH19fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGk9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLGU9dC4kY3JlYXRlRWxlbWVudCxuPXQuX3NlbGYuX2N8fGU7cmV0dXJuIG4oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdFwiLGNsYXNzOntcIm11bHRpc2VsZWN0LS1hY3RpdmVcIjp0LmlzT3BlbixcIm11bHRpc2VsZWN0LS1kaXNhYmxlZFwiOnQuZGlzYWJsZWQsXCJtdWx0aXNlbGVjdC0tYWJvdmVcIjp0LmlzQWJvdmV9LGF0dHJzOnt0YWJpbmRleDp0LnNlYXJjaGFibGU/LTE6dC50YWJpbmRleH0sb246e2ZvY3VzOmZ1bmN0aW9uKGUpe3QuYWN0aXZhdGUoKX0sYmx1cjpmdW5jdGlvbihlKXshdC5zZWFyY2hhYmxlJiZ0LmRlYWN0aXZhdGUoKX0sa2V5ZG93bjpbZnVuY3Rpb24oZSl7cmV0dXJuXCJidXR0b25cImluIGV8fCF0Ll9rKGUua2V5Q29kZSxcImRvd25cIiw0MCxlLmtleSxbXCJEb3duXCIsXCJBcnJvd0Rvd25cIl0pP2UudGFyZ2V0IT09ZS5jdXJyZW50VGFyZ2V0P251bGw6KGUucHJldmVudERlZmF1bHQoKSx2b2lkIHQucG9pbnRlckZvcndhcmQoKSk6bnVsbH0sZnVuY3Rpb24oZSl7cmV0dXJuXCJidXR0b25cImluIGV8fCF0Ll9rKGUua2V5Q29kZSxcInVwXCIsMzgsZS5rZXksW1wiVXBcIixcIkFycm93VXBcIl0pP2UudGFyZ2V0IT09ZS5jdXJyZW50VGFyZ2V0P251bGw6KGUucHJldmVudERlZmF1bHQoKSx2b2lkIHQucG9pbnRlckJhY2t3YXJkKCkpOm51bGx9LGZ1bmN0aW9uKGUpe3JldHVyblwiYnV0dG9uXCJpbiBlfHwhdC5fayhlLmtleUNvZGUsXCJlbnRlclwiLDEzLGUua2V5LFwiRW50ZXJcIil8fCF0Ll9rKGUua2V5Q29kZSxcInRhYlwiLDksZS5rZXksXCJUYWJcIik/KGUuc3RvcFByb3BhZ2F0aW9uKCksZS50YXJnZXQhPT1lLmN1cnJlbnRUYXJnZXQ/bnVsbDp2b2lkIHQuYWRkUG9pbnRlckVsZW1lbnQoZSkpOm51bGx9XSxrZXl1cDpmdW5jdGlvbihlKXtpZighKFwiYnV0dG9uXCJpbiBlKSYmdC5fayhlLmtleUNvZGUsXCJlc2NcIiwyNyxlLmtleSxcIkVzY2FwZVwiKSlyZXR1cm4gbnVsbDt0LmRlYWN0aXZhdGUoKX19fSxbdC5fdChcImNhcmV0XCIsW24oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fc2VsZWN0XCIsb246e21vdXNlZG93bjpmdW5jdGlvbihlKXtlLnByZXZlbnREZWZhdWx0KCksZS5zdG9wUHJvcGFnYXRpb24oKSx0LnRvZ2dsZSgpfX19KV0se3RvZ2dsZTp0LnRvZ2dsZX0pLHQuX3YoXCIgXCIpLHQuX3QoXCJjbGVhclwiLG51bGwse3NlYXJjaDp0LnNlYXJjaH0pLHQuX3YoXCIgXCIpLG4oXCJkaXZcIix7cmVmOlwidGFnc1wiLHN0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX3RhZ3NcIn0sW3QuX3QoXCJzZWxlY3Rpb25cIixbbihcImRpdlwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6dC52aXNpYmxlVmFsdWVzLmxlbmd0aD4wLGV4cHJlc3Npb246XCJ2aXNpYmxlVmFsdWVzLmxlbmd0aCA+IDBcIn1dLHN0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX3RhZ3Mtd3JhcFwifSxbdC5fbCh0LnZpc2libGVWYWx1ZXMsZnVuY3Rpb24oZSxpKXtyZXR1cm5bdC5fdChcInRhZ1wiLFtuKFwic3BhblwiLHtrZXk6aSxzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X190YWdcIn0sW24oXCJzcGFuXCIse2RvbVByb3BzOnt0ZXh0Q29udGVudDp0Ll9zKHQuZ2V0T3B0aW9uTGFiZWwoZSkpfX0pLHQuX3YoXCIgXCIpLG4oXCJpXCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX3RhZy1pY29uXCIsYXR0cnM6e1wiYXJpYS1oaWRkZW5cIjpcInRydWVcIix0YWJpbmRleDpcIjFcIn0sb246e2tleWRvd246ZnVuY3Rpb24obil7aWYoIShcImJ1dHRvblwiaW4gbikmJnQuX2sobi5rZXlDb2RlLFwiZW50ZXJcIiwxMyxuLmtleSxcIkVudGVyXCIpKXJldHVybiBudWxsO24ucHJldmVudERlZmF1bHQoKSx0LnJlbW92ZUVsZW1lbnQoZSl9LG1vdXNlZG93bjpmdW5jdGlvbihuKXtuLnByZXZlbnREZWZhdWx0KCksdC5yZW1vdmVFbGVtZW50KGUpfX19KV0pXSx7b3B0aW9uOmUsc2VhcmNoOnQuc2VhcmNoLHJlbW92ZTp0LnJlbW92ZUVsZW1lbnR9KV19KV0sMiksdC5fdihcIiBcIiksdC5pbnRlcm5hbFZhbHVlJiZ0LmludGVybmFsVmFsdWUubGVuZ3RoPnQubGltaXQ/W3QuX3QoXCJsaW1pdFwiLFtuKFwic3Ryb25nXCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX3N0cm9uZ1wiLGRvbVByb3BzOnt0ZXh0Q29udGVudDp0Ll9zKHQubGltaXRUZXh0KHQuaW50ZXJuYWxWYWx1ZS5sZW5ndGgtdC5saW1pdCkpfX0pXSldOnQuX2UoKV0se3NlYXJjaDp0LnNlYXJjaCxyZW1vdmU6dC5yZW1vdmVFbGVtZW50LHZhbHVlczp0LnZpc2libGVWYWx1ZXMsaXNPcGVuOnQuaXNPcGVufSksdC5fdihcIiBcIiksbihcInRyYW5zaXRpb25cIix7YXR0cnM6e25hbWU6XCJtdWx0aXNlbGVjdF9fbG9hZGluZ1wifX0sW3QuX3QoXCJsb2FkaW5nXCIsW24oXCJkaXZcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJzaG93XCIscmF3TmFtZTpcInYtc2hvd1wiLHZhbHVlOnQubG9hZGluZyxleHByZXNzaW9uOlwibG9hZGluZ1wifV0sc3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fc3Bpbm5lclwifSldKV0sMiksdC5fdihcIiBcIiksdC5zZWFyY2hhYmxlP24oXCJpbnB1dFwiLHtyZWY6XCJzZWFyY2hcIixzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X19pbnB1dFwiLHN0eWxlOnQuaW5wdXRTdHlsZSxhdHRyczp7bmFtZTp0Lm5hbWUsaWQ6dC5pZCx0eXBlOlwidGV4dFwiLGF1dG9jb21wbGV0ZTpcIm9mZlwiLHBsYWNlaG9sZGVyOnQucGxhY2Vob2xkZXIsZGlzYWJsZWQ6dC5kaXNhYmxlZCx0YWJpbmRleDp0LnRhYmluZGV4fSxkb21Qcm9wczp7dmFsdWU6dC5zZWFyY2h9LG9uOntpbnB1dDpmdW5jdGlvbihlKXt0LnVwZGF0ZVNlYXJjaChlLnRhcmdldC52YWx1ZSl9LGZvY3VzOmZ1bmN0aW9uKGUpe2UucHJldmVudERlZmF1bHQoKSx0LmFjdGl2YXRlKCl9LGJsdXI6ZnVuY3Rpb24oZSl7ZS5wcmV2ZW50RGVmYXVsdCgpLHQuZGVhY3RpdmF0ZSgpfSxrZXl1cDpmdW5jdGlvbihlKXtpZighKFwiYnV0dG9uXCJpbiBlKSYmdC5fayhlLmtleUNvZGUsXCJlc2NcIiwyNyxlLmtleSxcIkVzY2FwZVwiKSlyZXR1cm4gbnVsbDt0LmRlYWN0aXZhdGUoKX0sa2V5ZG93bjpbZnVuY3Rpb24oZSl7aWYoIShcImJ1dHRvblwiaW4gZSkmJnQuX2soZS5rZXlDb2RlLFwiZG93blwiLDQwLGUua2V5LFtcIkRvd25cIixcIkFycm93RG93blwiXSkpcmV0dXJuIG51bGw7ZS5wcmV2ZW50RGVmYXVsdCgpLHQucG9pbnRlckZvcndhcmQoKX0sZnVuY3Rpb24oZSl7aWYoIShcImJ1dHRvblwiaW4gZSkmJnQuX2soZS5rZXlDb2RlLFwidXBcIiwzOCxlLmtleSxbXCJVcFwiLFwiQXJyb3dVcFwiXSkpcmV0dXJuIG51bGw7ZS5wcmV2ZW50RGVmYXVsdCgpLHQucG9pbnRlckJhY2t3YXJkKCl9LGZ1bmN0aW9uKGUpe3JldHVyblwiYnV0dG9uXCJpbiBlfHwhdC5fayhlLmtleUNvZGUsXCJlbnRlclwiLDEzLGUua2V5LFwiRW50ZXJcIik/KGUucHJldmVudERlZmF1bHQoKSxlLnN0b3BQcm9wYWdhdGlvbigpLGUudGFyZ2V0IT09ZS5jdXJyZW50VGFyZ2V0P251bGw6dm9pZCB0LmFkZFBvaW50ZXJFbGVtZW50KGUpKTpudWxsfSxmdW5jdGlvbihlKXtpZighKFwiYnV0dG9uXCJpbiBlKSYmdC5fayhlLmtleUNvZGUsXCJkZWxldGVcIixbOCw0Nl0sZS5rZXksW1wiQmFja3NwYWNlXCIsXCJEZWxldGVcIl0pKXJldHVybiBudWxsO2Uuc3RvcFByb3BhZ2F0aW9uKCksdC5yZW1vdmVMYXN0RWxlbWVudCgpfV19fSk6dC5fZSgpLHQuX3YoXCIgXCIpLHQuaXNTaW5nbGVMYWJlbFZpc2libGU/bihcInNwYW5cIix7c3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fc2luZ2xlXCIsb246e21vdXNlZG93bjpmdW5jdGlvbihlKXtyZXR1cm4gZS5wcmV2ZW50RGVmYXVsdCgpLHQudG9nZ2xlKGUpfX19LFt0Ll90KFwic2luZ2xlTGFiZWxcIixbW3QuX3YodC5fcyh0LmN1cnJlbnRPcHRpb25MYWJlbCkpXV0se29wdGlvbjp0LnNpbmdsZVZhbHVlfSldLDIpOnQuX2UoKSx0Ll92KFwiIFwiKSx0LmlzUGxhY2Vob2xkZXJWaXNpYmxlP24oXCJzcGFuXCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX3BsYWNlaG9sZGVyXCIsb246e21vdXNlZG93bjpmdW5jdGlvbihlKXtyZXR1cm4gZS5wcmV2ZW50RGVmYXVsdCgpLHQudG9nZ2xlKGUpfX19LFt0Ll90KFwicGxhY2Vob2xkZXJcIixbdC5fdihcIlxcbiAgICAgICAgICAgIFwiK3QuX3ModC5wbGFjZWhvbGRlcikrXCJcXG4gICAgICAgIFwiKV0pXSwyKTp0Ll9lKCldLDIpLHQuX3YoXCIgXCIpLG4oXCJ0cmFuc2l0aW9uXCIse2F0dHJzOntuYW1lOlwibXVsdGlzZWxlY3RcIn19LFtuKFwiZGl2XCIse2RpcmVjdGl2ZXM6W3tuYW1lOlwic2hvd1wiLHJhd05hbWU6XCJ2LXNob3dcIix2YWx1ZTp0LmlzT3BlbixleHByZXNzaW9uOlwiaXNPcGVuXCJ9XSxyZWY6XCJsaXN0XCIsc3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fY29udGVudC13cmFwcGVyXCIsc3R5bGU6e21heEhlaWdodDp0Lm9wdGltaXplZEhlaWdodCtcInB4XCJ9LGF0dHJzOnt0YWJpbmRleDpcIi0xXCJ9LG9uOntmb2N1czp0LmFjdGl2YXRlLG1vdXNlZG93bjpmdW5jdGlvbih0KXt0LnByZXZlbnREZWZhdWx0KCl9fX0sW24oXCJ1bFwiLHtzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X19jb250ZW50XCIsc3R5bGU6dC5jb250ZW50U3R5bGV9LFt0Ll90KFwiYmVmb3JlTGlzdFwiKSx0Ll92KFwiIFwiKSx0Lm11bHRpcGxlJiZ0Lm1heD09PXQuaW50ZXJuYWxWYWx1ZS5sZW5ndGg/bihcImxpXCIsW24oXCJzcGFuXCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX29wdGlvblwifSxbdC5fdChcIm1heEVsZW1lbnRzXCIsW3QuX3YoXCJNYXhpbXVtIG9mIFwiK3QuX3ModC5tYXgpK1wiIG9wdGlvbnMgc2VsZWN0ZWQuIEZpcnN0IHJlbW92ZSBhIHNlbGVjdGVkIG9wdGlvbiB0byBzZWxlY3QgYW5vdGhlci5cIildKV0sMildKTp0Ll9lKCksdC5fdihcIiBcIiksIXQubWF4fHx0LmludGVybmFsVmFsdWUubGVuZ3RoPHQubWF4P3QuX2wodC5maWx0ZXJlZE9wdGlvbnMsZnVuY3Rpb24oZSxpKXtyZXR1cm4gbihcImxpXCIse2tleTppLHN0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX2VsZW1lbnRcIn0sW2UmJihlLiRpc0xhYmVsfHxlLiRpc0Rpc2FibGVkKT90Ll9lKCk6bihcInNwYW5cIix7c3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fb3B0aW9uXCIsY2xhc3M6dC5vcHRpb25IaWdobGlnaHQoaSxlKSxhdHRyczp7XCJkYXRhLXNlbGVjdFwiOmUmJmUuaXNUYWc/dC50YWdQbGFjZWhvbGRlcjp0LnNlbGVjdExhYmVsVGV4dCxcImRhdGEtc2VsZWN0ZWRcIjp0LnNlbGVjdGVkTGFiZWxUZXh0LFwiZGF0YS1kZXNlbGVjdFwiOnQuZGVzZWxlY3RMYWJlbFRleHR9LG9uOntjbGljazpmdW5jdGlvbihuKXtuLnN0b3BQcm9wYWdhdGlvbigpLHQuc2VsZWN0KGUpfSxtb3VzZWVudGVyOmZ1bmN0aW9uKGUpe2lmKGUudGFyZ2V0IT09ZS5jdXJyZW50VGFyZ2V0KXJldHVybiBudWxsO3QucG9pbnRlclNldChpKX19fSxbdC5fdChcIm9wdGlvblwiLFtuKFwic3BhblwiLFt0Ll92KHQuX3ModC5nZXRPcHRpb25MYWJlbChlKSkpXSldLHtvcHRpb246ZSxzZWFyY2g6dC5zZWFyY2h9KV0sMiksdC5fdihcIiBcIiksZSYmKGUuJGlzTGFiZWx8fGUuJGlzRGlzYWJsZWQpP24oXCJzcGFuXCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX29wdGlvblwiLGNsYXNzOnQuZ3JvdXBIaWdobGlnaHQoaSxlKSxhdHRyczp7XCJkYXRhLXNlbGVjdFwiOnQuZ3JvdXBTZWxlY3QmJnQuc2VsZWN0R3JvdXBMYWJlbFRleHQsXCJkYXRhLWRlc2VsZWN0XCI6dC5ncm91cFNlbGVjdCYmdC5kZXNlbGVjdEdyb3VwTGFiZWxUZXh0fSxvbjp7bW91c2VlbnRlcjpmdW5jdGlvbihlKXtpZihlLnRhcmdldCE9PWUuY3VycmVudFRhcmdldClyZXR1cm4gbnVsbDt0Lmdyb3VwU2VsZWN0JiZ0LnBvaW50ZXJTZXQoaSl9LG1vdXNlZG93bjpmdW5jdGlvbihuKXtuLnByZXZlbnREZWZhdWx0KCksdC5zZWxlY3RHcm91cChlKX19fSxbdC5fdChcIm9wdGlvblwiLFtuKFwic3BhblwiLFt0Ll92KHQuX3ModC5nZXRPcHRpb25MYWJlbChlKSkpXSldLHtvcHRpb246ZSxzZWFyY2g6dC5zZWFyY2h9KV0sMik6dC5fZSgpXSl9KTp0Ll9lKCksdC5fdihcIiBcIiksbihcImxpXCIse2RpcmVjdGl2ZXM6W3tuYW1lOlwic2hvd1wiLHJhd05hbWU6XCJ2LXNob3dcIix2YWx1ZTp0LnNob3dOb1Jlc3VsdHMmJjA9PT10LmZpbHRlcmVkT3B0aW9ucy5sZW5ndGgmJnQuc2VhcmNoJiYhdC5sb2FkaW5nLGV4cHJlc3Npb246XCJzaG93Tm9SZXN1bHRzICYmIChmaWx0ZXJlZE9wdGlvbnMubGVuZ3RoID09PSAwICYmIHNlYXJjaCAmJiAhbG9hZGluZylcIn1dfSxbbihcInNwYW5cIix7c3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fb3B0aW9uXCJ9LFt0Ll90KFwibm9SZXN1bHRcIixbdC5fdihcIk5vIGVsZW1lbnRzIGZvdW5kLiBDb25zaWRlciBjaGFuZ2luZyB0aGUgc2VhcmNoIHF1ZXJ5LlwiKV0pXSwyKV0pLHQuX3YoXCIgXCIpLG4oXCJsaVwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6dC5zaG93Tm9PcHRpb25zJiYwPT09dC5vcHRpb25zLmxlbmd0aCYmIXQuc2VhcmNoJiYhdC5sb2FkaW5nLGV4cHJlc3Npb246XCJzaG93Tm9PcHRpb25zICYmIChvcHRpb25zLmxlbmd0aCA9PT0gMCAmJiAhc2VhcmNoICYmICFsb2FkaW5nKVwifV19LFtuKFwic3BhblwiLHtzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X19vcHRpb25cIn0sW3QuX3QoXCJub09wdGlvbnNcIixbdC5fdihcIkxpc3QgaXMgZW1wdHkuXCIpXSldLDIpXSksdC5fdihcIiBcIiksdC5fdChcImFmdGVyTGlzdFwiKV0sMildKV0pXSwyKX0scj1bXSxvPXtyZW5kZXI6aSxzdGF0aWNSZW5kZXJGbnM6cn07ZS5hPW99XSl9KTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLW11bHRpc2VsZWN0L2Rpc3QvdnVlLW11bHRpc2VsZWN0Lm1pbi5qc1xuLy8gbW9kdWxlIGlkID0gMTlcbi8vIG1vZHVsZSBjaHVua3MgPSAyIDYgMTciLCIvKlxyXG5cdE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXHJcblx0QXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxyXG4qL1xyXG4vLyBjc3MgYmFzZSBjb2RlLCBpbmplY3RlZCBieSB0aGUgY3NzLWxvYWRlclxyXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKCkge1xyXG5cdHZhciBsaXN0ID0gW107XHJcblxyXG5cdC8vIHJldHVybiB0aGUgbGlzdCBvZiBtb2R1bGVzIGFzIGNzcyBzdHJpbmdcclxuXHRsaXN0LnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gW107XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaXRlbSA9IHRoaXNbaV07XHJcblx0XHRcdGlmKGl0ZW1bMl0pIHtcclxuXHRcdFx0XHRyZXN1bHQucHVzaChcIkBtZWRpYSBcIiArIGl0ZW1bMl0gKyBcIntcIiArIGl0ZW1bMV0gKyBcIn1cIik7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0cmVzdWx0LnB1c2goaXRlbVsxXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiByZXN1bHQuam9pbihcIlwiKTtcclxuXHR9O1xyXG5cclxuXHQvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxyXG5cdGxpc3QuaSA9IGZ1bmN0aW9uKG1vZHVsZXMsIG1lZGlhUXVlcnkpIHtcclxuXHRcdGlmKHR5cGVvZiBtb2R1bGVzID09PSBcInN0cmluZ1wiKVxyXG5cdFx0XHRtb2R1bGVzID0gW1tudWxsLCBtb2R1bGVzLCBcIlwiXV07XHJcblx0XHR2YXIgYWxyZWFkeUltcG9ydGVkTW9kdWxlcyA9IHt9O1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGlkID0gdGhpc1tpXVswXTtcclxuXHRcdFx0aWYodHlwZW9mIGlkID09PSBcIm51bWJlclwiKVxyXG5cdFx0XHRcdGFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaWRdID0gdHJ1ZTtcclxuXHRcdH1cclxuXHRcdGZvcihpID0gMDsgaSA8IG1vZHVsZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGl0ZW0gPSBtb2R1bGVzW2ldO1xyXG5cdFx0XHQvLyBza2lwIGFscmVhZHkgaW1wb3J0ZWQgbW9kdWxlXHJcblx0XHRcdC8vIHRoaXMgaW1wbGVtZW50YXRpb24gaXMgbm90IDEwMCUgcGVyZmVjdCBmb3Igd2VpcmQgbWVkaWEgcXVlcnkgY29tYmluYXRpb25zXHJcblx0XHRcdC8vICB3aGVuIGEgbW9kdWxlIGlzIGltcG9ydGVkIG11bHRpcGxlIHRpbWVzIHdpdGggZGlmZmVyZW50IG1lZGlhIHF1ZXJpZXMuXHJcblx0XHRcdC8vICBJIGhvcGUgdGhpcyB3aWxsIG5ldmVyIG9jY3VyIChIZXkgdGhpcyB3YXkgd2UgaGF2ZSBzbWFsbGVyIGJ1bmRsZXMpXHJcblx0XHRcdGlmKHR5cGVvZiBpdGVtWzBdICE9PSBcIm51bWJlclwiIHx8ICFhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzW2l0ZW1bMF1dKSB7XHJcblx0XHRcdFx0aWYobWVkaWFRdWVyeSAmJiAhaXRlbVsyXSkge1xyXG5cdFx0XHRcdFx0aXRlbVsyXSA9IG1lZGlhUXVlcnk7XHJcblx0XHRcdFx0fSBlbHNlIGlmKG1lZGlhUXVlcnkpIHtcclxuXHRcdFx0XHRcdGl0ZW1bMl0gPSBcIihcIiArIGl0ZW1bMl0gKyBcIikgYW5kIChcIiArIG1lZGlhUXVlcnkgKyBcIilcIjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0bGlzdC5wdXNoKGl0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fTtcclxuXHRyZXR1cm4gbGlzdDtcclxufTtcclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXG4vLyBtb2R1bGUgaWQgPSAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTBmMmYyYjc3XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjAxMzk4MDIwXCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wZjJmMmI3N1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wZjJmMmI3N1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMGYyZjJiNzdcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjBcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsIjx0ZW1wbGF0ZT5cbjxkaXYgY2xhc3M9XCJtb2RhbCBpbm1vZGFsXCIgOmlkPVwiaWRcIiB0YWJpbmRleD1cIjFcIiByb2xlPVwiZGlhbG9nXCIgYXJpYS1sYWJlbGxlZGJ5PVwibW9kYWwtbGFiZWxcIiBhcmlhLWhpZGRlbj1cInRydWVcIj5cbiAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZGlhbG9nXCIgOmNsYXNzPVwic2l6ZVwiIHJvbGU9XCJkb2N1bWVudFwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPlxuICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiIGFyaWEtbGFiZWw9XCJDbG9zZVwiPjxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPiZ0aW1lczs8L3NwYW4+PC9idXR0b24+XG4gICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwibW9kYWwtdGl0bGVcIiBpZD1cIm1vZGFsLWxhYmVsXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC10aXRsZVwiPk1vZGFsIFRpdGxlPC9zbG90PlxuICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+XG4gICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLWJvZHlcIj5Nb2RhbCBCb2R5PC9zbG90PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLWZvb3RlclwiPlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tZmxhdFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG48L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gICAgcHJvcHM6IHtcbiAgICAgICAgICdzaXplJzoge2RlZmF1bHQ6J21vZGFsLWxnJ31cbiAgICAgICAgLCdpZCc6IHtyZXF1aXJlZDp0cnVlfVxuICAgICAgICAsJ2NvbnRlbnRTaXplJzoge2RlZmF1bHQ6JzEyJ31cblxuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE1vZGFsLnZ1ZT8zMTg2ODljMCIsIjx0ZW1wbGF0ZT5cbiAgPGRpdj5cbiAgICAgICAgPGRpdiBjbGFzcz1cInJvdyB0aW1lbGluZS1tb3ZlbWVudCB0aW1lbGluZS1tb3ZlbWVudC10b3Agbm8tZGFzaGVkIG5vLXNpZGUtbWFyZ2luXCIgdi1pZj1cImxvZy55ZWFyICE9IG51bGxcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aW1lbGluZS1iYWRnZSB0aW1lbGluZS1maWx0ZXItbW92ZW1lbnRcIj5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj57e2xvZy55ZWFyfX08L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzPVwicm93IHRpbWVsaW5lLW1vdmVtZW50ICBuby1zaWRlLW1hcmdpblwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpbWVsaW5lLWJhZGdlXCIgdi1pZj1cImxvZy5tb250aCAhPSBudWxsXCI+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0aW1lbGluZS1iYWxsb29uLWRhdGUtZGF5XCI+e3tsb2cuZGF5fX08L3NwYW4+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0aW1lbGluZS1iYWxsb29uLWRhdGUtbW9udGhcIj57e2xvZy5tb250aH19PC9zcGFuPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTEyICB0aW1lbGluZS1pdGVtXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tb2Zmc2V0LTEgY29sLXNtLTExXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpbWVsaW5lLXBhbmVsIGRlYml0c1wiID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwidGltZWxpbmUtcGFuZWwtdWxcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48c3BhbiBjbGFzcz1cImltcG9ydG9cIiB2LWh0bWw9XCJsb2cuZGF0YS50aXRsZVwiPjwvc3Bhbj48L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxzcGFuIGNsYXNzPVwiY2F1c2FsZVwiPnt7bG9nLmRhdGEucHJvY2Vzc29yfX08L3NwYW4+IDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PHA+PHNtYWxsIGNsYXNzPVwidGV4dC1tdXRlZFwiPjxpIGNsYXNzPVwiZ2x5cGhpY29uIGdseXBoaWNvbi10aW1lXCI+PC9pPnt7bG9nLmRhdGEuZGF0ZX19PC9zbWFsbD48L3A+IDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8L2Rpdj5cbiAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICBwcm9wczogWydsb2cnLCdsb2djb3VudCddLFxuICBtZXRob2RzOntcbiAgICAvLyBzZWxlY3QoaWQpe1xuICAgICAgICAvL3RoaXMuJHBhcmVudC5zZWxlY3RJdGVtKGlkKVxuICAgIC8vIH0sXG4gIH0sXG59XG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQWN0aW9uTG9ncy52dWU/NmQ3ODFiZjgiLCI8dGVtcGxhdGU+XG5cdFxuXHQ8ZGl2IGNsYXNzPVwicm93XCI+XG5cblx0XHQ8Zm9ybSBAc3VibWl0LnByZXZlbnQ9XCJhZGREb2N1bWVudHNcIj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC00XCIgdi1mb3I9XCIoaW1hZ2UsIGluZGV4KSBpbiBpbWFnZXNcIj5cblx0XHRcdFx0PGRpdiBjbGFzcz1cInBhbmVsIHBhbmVsLWRlZmF1bHQgZm9ybS1zZWN0aW9uXCI+XG5cdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGFuZWwtYm9keVwiPlxuXHQgICAgICAgICAgICAgICAgXHQ8Y2VudGVyPlxuXHQgICAgICAgICAgICAgICAgXHRcdDxpbWcgOnNyYz1cImltYWdlXCIgY2xhc3M9XCJpbWctcmVzcG9uc2l2ZSBpbWctdGh1bWJuYWlsIGRvY3VtZW50LWltYWdlIHpvb21cIiA6ZGF0YS1tYWduaWZ5LXNyYz1cImltYWdlXCI+XG5cdCAgICAgICAgICAgICAgICBcdDwvY2VudGVyPlxuXG5cdCAgICAgICAgICAgICAgICBcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MTBweDtjbGVhcjpib3RoO1wiPjwvZGl2PlxuXG5cdCAgICAgICAgICAgICAgICBcdDxwPkRvY3VtZW50IFR5cGU6PC9wPlxuXHQgICAgICAgICAgICAgICAgXHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHQgICAgICAgICAgICAgICAgXHRcdDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiB2LW1vZGVsPVwiZG9jdW1lbnRUeXBlc1tpbmRleF1cIj5cblx0ICAgICAgICAgICAgICAgIFx0XHRcdDxvcHRpb24gdi1mb3I9XCJkb2N1bWVudFR5cGUgaW4gZGVmYXVsdERvY3VtZW50VHlwZXNcIiA6dmFsdWU9XCJkb2N1bWVudFR5cGUuaWRcIj5cblx0ICAgICAgICAgICAgICAgIFx0XHRcdFx0e3sgZG9jdW1lbnRUeXBlLm5hbWUgfX1cblx0ICAgICAgICAgICAgICAgIFx0XHRcdDwvb3B0aW9uPlxuXHQgICAgICAgICAgICAgICAgXHRcdDwvc2VsZWN0PlxuXHQgICAgICAgICAgICAgICAgXHQ8L2Rpdj5cblxuXHQgICAgICAgICAgICAgICAgXHQ8cD5Jc3N1ZWQgQXQ6PC9wPlxuXHQgICAgICAgICAgICAgICAgXHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG5cdFx0XHRcdCAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG5cdFx0XHRcdCAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cblx0XHRcdFx0ICAgICAgICAgICAgICAgIDwvc3Bhbj5cblx0XHRcdFx0ICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiZGF0ZVwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgdi1tb2RlbD1cImlzc3VlZEF0W2luZGV4XVwiPlxuXHRcdFx0XHQgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdFx0ICAgICAgICA8L2Rpdj5cblxuXHRcdFx0ICAgICAgICAgICAgPHA+RXhwaXJlZCBBdDo8L3A+XG5cdFx0XHQgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG5cdFx0XHRcdCAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG5cdFx0XHRcdCAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cblx0XHRcdFx0ICAgICAgICAgICAgICAgIDwvc3Bhbj5cblx0XHRcdFx0ICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiZGF0ZVwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgdi1tb2RlbD1cImV4cGlyZWRBdFtpbmRleF1cIj5cblx0XHRcdFx0ICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHRcdCAgICAgICAgPC9kaXY+XG5cblx0XHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHQgICAgICAgIFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRhbmdlciBidG4teHMgcHVsbC1yaWdodFwiIEBjbGljaz1cInJlbW92ZShpbmRleClcIj5SZW1vdmU8L2J1dHRvbj5cblx0XHRcdFx0ICAgICAgICA8L2Rpdj5cblxuXHQgICAgICAgICAgICAgICAgPC9kaXY+XG5cdCAgICAgICAgICAgIDwvZGl2PlxuXHQgICAgICAgIDwvZGl2PlxuXG5cdCAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC00XCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0IGFkZC1kb2N1bWVudHMtc2VjdGlvblwiPlxuXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cblx0ICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiaW1hZ2UtdXBsb2FkXCIgaWQ9XCJpbWFnZS1sYWJlbFwiPjxpIGNsYXNzPVwiZmEgZmEtcGx1c1wiPjwvaT4gQ2hvb3NlIEZpbGU8L2xhYmVsPlxuXHQgIFx0XHRcdFx0XHQ8aW5wdXQgdHlwZT1cImZpbGVcIiBuYW1lPVwiaW1hZ2VzXCIgcmVmPVwiZmlsZXNcIiBAY2hhbmdlPVwiaGFuZGxlRmlsZXNVcGxvYWRcIiBpZD1cImltYWdlLXVwbG9hZFwiIG11bHRpcGxlIC8+XG5cdCAgICAgICAgICAgICAgICA8L2Rpdj5cblx0ICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBzdHlsZT1cImhlaWdodDoxcHg7Y2xlYXI6Ym90aDtcIj48L2Rpdj5cblxuXHRcdFx0PGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPkFkZCBEb2N1bWVudC9zPC9idXR0b24+XG5cdCAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuXG5cdFx0PC9mb3JtPlxuXG4gICAgPC9kaXY+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5cblx0ZXhwb3J0IGRlZmF1bHQge1xuXHRcdGRhdGEoKSB7XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRkZWZhdWx0RG9jdW1lbnRUeXBlczogW10sXG5cblx0XHRcdFx0aW1hZ2VzOiBbXSxcblx0XHRcdFx0ZmlsZXM6IFtdLFxuXHRcdFx0XHRkb2N1bWVudFR5cGVzOiBbXSxcblx0XHRcdFx0aXNzdWVkQXQ6IFtdLFxuXHRcdFx0XHRleHBpcmVkQXQ6IFtdXG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdG1ldGhvZHM6IHtcblx0XHRcdGltYWdlUHJldmlldyhpbnB1dCkge1xuXHRcdFx0XHRpZihpbnB1dC5maWxlcykge1xuXHRcdFx0XHRcdHZhciB2YWxpZE1pbWVUeXBlID0gWydkYXRhOmltYWdlL2pwZWcnLCAnZGF0YTppbWFnZS9qcGcnLCAnZGF0YTppbWFnZS9wbmcnXTtcblx0XHQgICAgICAgICAgICB2YXIgZmlsZXNBbW91bnQgPSBpbnB1dC5maWxlcy5sZW5ndGg7XG5cblx0XHQgICAgICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgZmlsZXNBbW91bnQ7IGkrKykge1xuXHRcdCAgICAgICAgICAgICAgICB2YXIgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcblxuXHRcdCAgICAgICAgICAgICAgICByZWFkZXIub25sb2FkID0gKGV2ZW50KSA9PiB7XG5cdFx0ICAgICAgICAgICAgICAgIFx0bGV0IGltZyA9IGV2ZW50LnRhcmdldC5yZXN1bHQ7XG5cdFx0XHRcdFx0XHRcdGxldCBsYXN0Q2hhckluZGV4ID0gaW1nLmluZGV4T2YoJzsnKTtcblx0XHRcdFx0XHRcdFx0bGV0IG1pbWVUeXBlID0gaW1nLnN1YnN0cigwLCBsYXN0Q2hhckluZGV4KTtcblx0XHRcdFx0XHRcdFx0aWYoIXZhbGlkTWltZVR5cGUuaW5jbHVkZXMobWltZVR5cGUpKSB7XG5cdFx0XHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdZb3UgY2FuXFwndCB1cGxvYWQgZmlsZXMgb2YgdGhpcyB0eXBlLicpO1xuXHRcdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRcdHRoaXMuaW1hZ2VzLnB1c2goZXZlbnQudGFyZ2V0LnJlc3VsdCk7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5kb2N1bWVudFR5cGVzLnB1c2gobnVsbCk7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5pc3N1ZWRBdC5wdXNoKG51bGwpO1xuXHRcdFx0XHRcdFx0XHRcdHRoaXMuZXhwaXJlZEF0LnB1c2gobnVsbCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHQgICAgICAgICAgICAgICAgfVxuXG5cdFx0ICAgICAgICAgICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGlucHV0LmZpbGVzW2ldKTtcblx0XHQgICAgICAgICAgICB9XG5cblx0XHQgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcblx0XHQgICAgICAgICAgICBcdCQoJy56b29tJykubWFnbmlmeSgpO1xuXHRcdCAgICAgICAgICAgIH0sIDEwMDApO1xuXHRcdCAgICAgICAgfVxuXHRcdFx0fSxcblxuXHRcdFx0aGFuZGxlRmlsZXNVcGxvYWQoKSB7XG5cdFx0XHRcdHRoaXMuZmlsZXMucHVzaCh0aGlzLiRyZWZzLmZpbGVzLmZpbGVzKTtcblx0XHRcdH0sXG5cblx0XHRcdHJlbW92ZShpbmRleCkge1xuXHRcdFx0XHR0aGlzLmltYWdlcy5zcGxpY2UoaW5kZXgsIDEpO1xuXHRcdFx0XHR0aGlzLmZpbGVzLnNwbGljZShpbmRleCwgMSk7XG5cdFx0XHRcdHRoaXMuZG9jdW1lbnRUeXBlcy5zcGxpY2UoaW5kZXgsIDEpO1xuXHRcdFx0XHR0aGlzLmlzc3VlZEF0LnNwbGljZShpbmRleCwgMSk7XG5cdFx0XHRcdHRoaXMuZXhwaXJlZEF0LnNwbGljZShpbmRleCwgMSk7XG5cblx0XHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XG5cdFx0ICAgICAgICAgICAgJCgnLnpvb20nKS5tYWduaWZ5KCk7XG5cdFx0ICAgICAgICB9LCA1MDApO1xuXHRcdFx0fSxcblxuXHRcdFx0Z2V0RG9jdW1lbnRUeXBlcygpIHtcblx0XHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvY2xpZW50LWRvY3VtZW50cycpXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5kZWZhdWx0RG9jdW1lbnRUeXBlcyA9IHJlc3BvbnNlLmRhdGE7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHR9LFxuXG5cdCAgXHRcdHZhbGlkYXRlKCkge1xuXHQgIFx0XHRcdHZhciBzdWNjZXNzID0gdHJ1ZTtcblx0ICBcdFx0XHRsZXQgbGVuZ3RoID0gdGhpcy5pbWFnZXMubGVuZ3RoO1xuXG5cdCAgXHRcdFx0aWYobGVuZ3RoID09IDApIHtcblx0ICBcdFx0XHRcdHN1Y2Nlc3MgPSBmYWxzZTtcblx0ICBcdFx0XHRcdHRvYXN0ci5lcnJvcignRG9jdW1lbnQvcyBpcyByZXF1aXJlZC4nKTtcblx0ICBcdFx0XHR9IGVsc2Uge1xuXHRcdCAgXHRcdFx0Zm9yKGxldCBpPTA7IGk8bGVuZ3RoOyBpKyspIHtcblx0XHQgIFx0XHRcdFx0aWYodGhpcy5kb2N1bWVudFR5cGVzW2ldID09IG51bGwpIHtcblx0XHQgIFx0XHRcdFx0XHRzdWNjZXNzID0gZmFsc2U7XG5cdFx0ICBcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdbJysoaSsxKSsnXScgKyAnIFRoZSBkb2N1bWVudCB0eXBlIGZpZWxkIGlzIHJlcXVpcmVkLicpO1xuXHRcdCAgXHRcdFx0XHR9XG5cblx0XHQgIFx0XHRcdFx0aWYodGhpcy5pc3N1ZWRBdFtpXSA9PSBudWxsKSB7XG5cdFx0ICBcdFx0XHRcdFx0c3VjY2VzcyA9IGZhbHNlO1xuXHRcdCAgXHRcdFx0XHRcdHRvYXN0ci5lcnJvcignWycrKGkrMSkrJ10nICsgJyBUaGUgaXNzdWVkIGF0IGZpZWxkIGlzIHJlcXVpcmVkLicpO1xuXHRcdCAgXHRcdFx0XHR9IGVsc2UgaWYoIW1vbWVudCh0aGlzLmlzc3VlZEF0W2ldLCAnWVlZWS1NTS1ERCcpLmlzVmFsaWQoKSkge1xuXHRcdCAgXHRcdFx0XHRcdHN1Y2Nlc3MgPSBmYWxzZTtcblx0XHQgIFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1snKyhpKzEpKyddJyArICcgSW5jb3JyZWN0IGlzc3VlZCBhdCBmaWVsZCBmb3JtYXQuJyk7XG5cdFx0ICBcdFx0XHRcdH1cblxuXHRcdCAgXHRcdFx0XHRpZih0aGlzLmV4cGlyZWRBdFtpXSA9PSBudWxsKSB7XG5cdFx0ICBcdFx0XHRcdFx0c3VjY2VzcyA9IGZhbHNlO1xuXHRcdCAgXHRcdFx0XHRcdHRvYXN0ci5lcnJvcignWycrKGkrMSkrJ10nICsgJyBUaGUgZXhwaXJlZCBhdCBmaWVsZCBpcyByZXF1aXJlZC4nKTtcblx0XHQgIFx0XHRcdFx0fSBlbHNlIGlmKCFtb21lbnQodGhpcy5leHBpcmVkQXRbaV0sICdZWVlZLU1NLUREJykuaXNWYWxpZCgpKSB7XG5cdFx0ICBcdFx0XHRcdFx0c3VjY2VzcyA9IGZhbHNlO1xuXHRcdCAgXHRcdFx0XHRcdHRvYXN0ci5lcnJvcignWycrKGkrMSkrJ10nICsgJyBJbmNvcnJlY3QgZXhwaXJlZCBhdCBmaWVsZCBmb3JtYXQuJyk7XG5cdFx0ICBcdFx0XHRcdH1cblx0XHQgIFx0XHRcdH1cblx0ICBcdFx0XHR9XG5cblx0ICBcdFx0XHRyZXR1cm4gc3VjY2Vzcztcblx0ICBcdFx0fSxcblxuXHQgIFx0XHRhZGREb2N1bWVudHMoKSB7XG5cdCAgXHRcdFx0aWYodGhpcy52YWxpZGF0ZSgpKSB7XG5cdCAgXHRcdFx0XHRsZXQgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcblxuXHRcdFx0XHRcdGZvcihsZXQgaT0wOyBpPHRoaXMuZmlsZXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0XHQgICAgbGV0IGZpbGUgPSB0aGlzLmZpbGVzW2ldWzBdO1xuXHRcdFx0XHRcdCAgICBmb3JtRGF0YS5hcHBlbmQoJ2F0dGFjaG1lbnRzWycraSsnXScsIGZpbGUpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGZvcm1EYXRhLmFwcGVuZCgndXNlcklkJywgd2luZG93LkxhcmF2ZWwuZGF0YS5pZCk7XG5cblx0XHRcdFx0XHRmb3IobGV0IGk9MDsgaTx0aGlzLmltYWdlcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0XHRcdFx0Zm9ybURhdGEuYXBwZW5kKCdpbWFnZXNbJytpKyddJywgdGhpcy5pbWFnZXNbaV0pO1xuXHRcdFx0XHRcdFx0Zm9ybURhdGEuYXBwZW5kKCdkb2N1bWVudFR5cGVzWycraSsnXScsIHRoaXMuZG9jdW1lbnRUeXBlc1tpXSk7XG5cdFx0XHRcdFx0XHRmb3JtRGF0YS5hcHBlbmQoJ2lzc3VlZEF0WycraSsnXScsIHRoaXMuaXNzdWVkQXRbaV0pO1xuXHRcdFx0XHRcdFx0Zm9ybURhdGEuYXBwZW5kKCdleHBpcmVkQXRbJytpKyddJywgdGhpcy5leHBpcmVkQXRbaV0pO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGF4aW9zLnBvc3QoJy92aXNhL3NlcnZpY2UtbWFuYWdlci9jbGllbnQtZG9jdW1lbnRzL3VwbG9hZC1kb2N1bWVudHMtdjInLFxuXHRcdFx0XHRcdFx0Zm9ybURhdGEsXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHQgICAgaGVhZGVyczoge1xuXHRcdFx0XHRcdFx0ICAgICAgICAnQ29udGVudC1UeXBlJzogJ211bHRpcGFydC9mb3JtLWRhdGEnXG5cdFx0XHRcdFx0XHQgICAgfVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdClcblx0XHQgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcblx0XHQgICAgICAgICAgICAgICBcdGlmKHJlc3BvbnNlLmRhdGEuc3VjY2Vzcykge1xuXHRcdCAgICAgICAgICAgICAgICBcdHRvYXN0ci5zdWNjZXNzKCdTdWNjZXNzZnVsbHkgc2F2ZWQuJyk7XG5cblx0XHRcdFx0XHRcdFx0dGhpcy5pbWFnZXMgPSBbXTtcblx0XHRcdFx0XHRcdFx0dGhpcy5maWxlcyA9IFtdO1xuXHRcdFx0XHRcdFx0XHR0aGlzLmRvY3VtZW50VHlwZXMgPSBbXTtcblx0XHRcdFx0XHRcdFx0dGhpcy5pc3N1ZWRBdCA9IFtdO1xuXHRcdFx0XHRcdFx0XHR0aGlzLmV4cGlyZWRBdCA9IFtdO1xuXG5cdFx0XHRcdFx0XHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmdldEZpbGVzKCk7XG5cblx0XHRcdFx0XHRcdFx0JCgnI2ltYWdlLXVwbG9hZCcpLnZhbChudWxsKTtcblxuXHRcdFx0XHRcdFx0XHQkKCcjYWRkRG9jdW1lbnRzTW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdCAgICAgICAgICAgIH0pO1xuXHQgIFx0XHRcdH1cblx0ICBcdFx0fVxuXHRcdH0sXG5cblx0XHRjcmVhdGVkKCkge1xuXHRcdFx0dGhpcy5nZXREb2N1bWVudFR5cGVzKCk7XG5cdFx0fSxcblxuXHRcdG1vdW50ZWQgKCkge1xuXHRcdFx0bGV0IHZtID0gdGhpcztcblxuXHRcdFx0JCgnI2ltYWdlLXVwbG9hZCcpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0XHQgICAgICAgIHZtLmltYWdlUHJldmlldyh0aGlzKTtcblx0XHQgICAgfSk7XG5cdFx0fVxuXHR9XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0ZGl2LmFkZC1kb2N1bWVudHMtc2VjdGlvbiB7XG5cdFx0aGVpZ2h0OiAzMDBweDtcblx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcblx0XHRiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuXHRcdGNvbG9yOiAjZWNmMGYxO1xuXHR9XG5cdGRpdi5hZGQtZG9jdW1lbnRzLXNlY3Rpb24gaW5wdXQge1xuXHQgIFx0bGluZS1oZWlnaHQ6IDIwMHB4O1xuXHQgIFx0Zm9udC1zaXplOiAyMDBweDtcblx0ICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0ICBcdG9wYWNpdHk6IDA7XG5cdCAgXHR6LWluZGV4OiAxMDtcblx0fVxuXHRkaXYuYWRkLWRvY3VtZW50cy1zZWN0aW9uIGxhYmVsIHtcbiAgXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgXHRcdHotaW5kZXg6IDU7XG5cdFx0Y3Vyc29yOiBwb2ludGVyO1xuXHRcdGJhY2tncm91bmQtY29sb3I6ICM0QUJDOTY7XG5cdFx0d2lkdGg6IDIwMHB4O1xuXHRcdGhlaWdodDogNTBweDtcblx0XHRmb250LXNpemU6IDIwcHg7XG5cdFx0bGluZS1oZWlnaHQ6IDUwcHg7XG5cdFx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcblx0XHR0b3A6IDA7XG5cdFx0bGVmdDogMDtcblx0XHRyaWdodDogMDtcblx0XHRib3R0b206IDA7XG5cdFx0bWFyZ2luOiBhdXRvO1xuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcblx0fVxuXHRkaXYuZm9ybS1zZWN0aW9uIHtcblx0XHRoZWlnaHQ6IGF1dG87XG5cdH1cblx0ZGl2LmZvcm0tc2VjdGlvbiBpbWcuZG9jdW1lbnQtaW1hZ2Uge1xuXHRcdGhlaWdodDogMjAwcHg7XG5cdH1cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENsaWVudERvY3VtZW50cy52dWU/MDMwY2NlNjgiLCI8dGVtcGxhdGU+XG4gIDxkaXY+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3cgdGltZWxpbmUtbW92ZW1lbnQgdGltZWxpbmUtbW92ZW1lbnQtdG9wIG5vLWRhc2hlZCBuby1zaWRlLW1hcmdpblwiIHYtaWY9XCJsb2cyLnllYXIgIT0gbnVsbFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpbWVsaW5lLWJhZGdlIHRpbWVsaW5lLWZpbHRlci1tb3ZlbWVudFwiPlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPnt7bG9nMi55ZWFyfX08L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzPVwicm93IHRpbWVsaW5lLW1vdmVtZW50ICBuby1zaWRlLW1hcmdpblwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpbWVsaW5lLWJhZGdlXCIgdi1pZj1cImxvZzIubW9udGggIT0gbnVsbFwiPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwidGltZWxpbmUtYmFsbG9vbi1kYXRlLWRheVwiPnt7bG9nMi5kYXl9fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cInRpbWVsaW5lLWJhbGxvb24tZGF0ZS1tb250aFwiPnt7bG9nMi5tb250aH19PC9zcGFuPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTEyICB0aW1lbGluZS1pdGVtXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMiBjb2wtc20tb2Zmc2V0LTFcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGltZWxpbmUtcGFuZWwgZGViaXRzXCIgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJ0aW1lbGluZS1wYW5lbC11bFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxzcGFuIGNsYXNzPVwiY2F1c2FsZVwiPkJhbGFuY2U6IDxiPnt7bG9nMi5kYXRhLmJhbGFuY2UgfCBjdXJyZW5jeX19PC9iPjwvc3Bhbj4gPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48cD48c21hbGwgY2xhc3M9XCJ0ZXh0LW11dGVkXCI+QW1vdW50IDogPGI+e3tsb2cyLmRhdGEuYW1vdW50ICB8IGN1cnJlbmN5fX08L2I+PC9zbWFsbD48L3A+IDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PHA+PHNtYWxsIGNsYXNzPVwidGV4dC1tdXRlZFwiPlR5cGUgOiA8Yj57e2xvZzIuZGF0YS50eXBlIH19PC9iPjwvc21hbGw+PC9wPiA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS05XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpbWVsaW5lLXBhbmVsIGRlYml0c1wiID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwidGltZWxpbmUtcGFuZWwtdWxcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48c3BhbiBjbGFzcz1cImltcG9ydG9cIiB2LWh0bWw9XCJ0aGlzLmRldGFpbFwiPjwvc3Bhbj48L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxzcGFuIGNsYXNzPVwiY2F1c2FsZVwiPnt7bG9nMi5kYXRhLnByb2Nlc3Nvcn19PC9zcGFuPiA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxwPjxzbWFsbCBjbGFzcz1cInRleHQtbXV0ZWRcIj48aSBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tdGltZVwiPjwvaT57e2xvZzIuZGF0YS5kYXRlfX08L3NtYWxsPjwvcD4gPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDwvZGl2PlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIHByb3BzOiBbJ2xvZzInXSxcbiAgbWV0aG9kczp7XG4gICAgc2VsZWN0KGlkKXtcbiAgICAgICB0aGlzLiRwYXJlbnQuc2VsZWN0SXRlbShpZClcbiAgICB9LFxuICB9LFxuICBjb21wdXRlZDp7XG4gICAgZGV0YWlsKCl7XG4gICAgICB2YXIgcmVzID0gdGhpcy5sb2cyLmRhdGEudGl0bGU7XG4gICAgICByZXR1cm4gcmVzLnJlcGxhY2UoXCI8L2JyPlwiLCBcIiBcIik7XG4gICAgfVxuXG4gIH0sXG59XG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gVHJhbnNhY3Rpb25Mb2dzLnZ1ZT8yZDY5MDA3ZiIsIjx0ZW1wbGF0ZT5cblx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIiBAc3VibWl0LnByZXZlbnQ9XCJhZGROZXdTZXJ2aWNlXCI+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiaW9zLXNjcm9sbFwiPlxuXHQgICAgXHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ3NlcnZpY2VzJykgfVwiPlxuXHRcdCAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdCAgICAgICAgICAgIFNlcnZpY2U6XG5cdFx0ICAgICAgICA8L2xhYmVsPlxuXG5cdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IFNlcnZpY2VcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjM1MHB4O1wiIHRhYmluZGV4PVwiNFwiPlxuXHRcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwic2VydmljZSBpbiBzZXJ2aWNlc0xpc3RcIiA6dmFsdWU9XCJzZXJ2aWNlLmlkXCIgOmRhdGEtZG9jcy1uZWVkZWQ9XCJzZXJ2aWNlLmRvY3NfbmVlZGVkXCIgOmRhdGEtZG9jcy1vcHRpb25hbD1cInNlcnZpY2UuZG9jc19vcHRpb25hbFwiPlxuXHRcdCAgICAgICAgICAgICAgICBcdHt7IChzZXJ2aWNlLmRldGFpbCkudHJpbSgpIH19XG5cdFx0ICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdCAgICAgICAgICAgIDwvc2VsZWN0PlxuXG5cdFx0ICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cImFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ3NlcnZpY2VzJylcIiB2LXRleHQ9XCJhZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdzZXJ2aWNlcycpXCI+PC9zcGFuPlxuXG5cdFx0ICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNwYWNlci0xMFwiPjwvZGl2PlxuXG5cdFx0ICAgICAgICAgICAgPHA+XG5cdFx0ICAgICAgICAgICAgXHRJZiB5b3Ugd2FudCB0byB2aWV3IGFsbCBzZXJ2aWNlcyBhdCBvbmNlLCBwbGVhc2UgXG5cdFx0ICAgICAgICAgICAgXHQ8YSBocmVmPVwiL3Zpc2Evc2VydmljZS92aWV3LWFsbFwiIHRhcmdldD1cIl9ibGFua1wiPmNsaWNrIGhlcmU8L2E+XG5cdFx0ICAgICAgICAgICAgPC9wPlxuXHRcdCAgICAgICAgPC9kaXY+XG5cdFx0ICAgIDwvZGl2PlxuXG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0ICAgIFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdCAgICAgICAgICAgIE5vdGU6XG5cdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJub3RlXCIgdi1tb2RlbD1cImFkZE5ld1NlcnZpY2VGb3JtLm5vdGVcIiBwbGFjZWhvbGRlcj1cImZvciBpbnRlcm5hbCB1c2Ugb25seSwgb25seSBlbXBsb3llZXMgY2FuIHNlZS5cIj5cblx0XHQgICAgICAgIDwvZGl2PlxuXHRcdCAgICA8L2Rpdj5cblxuXHRcdCAgICA8ZGl2IHYtc2hvdz1cImFkZE5ld1NlcnZpY2VGb3JtLnNlcnZpY2VzLmxlbmd0aCA8PSAxXCI+XG5cdFx0ICAgIFx0PGRpdiBjbGFzc3M9XCJjb2wtbWQtMTJcIj5cblx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNvbC1tZC02XCI+XG5cdFx0XHRcdCAgICBcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCI+XG5cdFx0XHRcdCAgICAgICAgICAgIENvc3Q6XG5cdFx0XHRcdCAgICAgICAgPC9sYWJlbD5cblx0XHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLThcIj5cblx0XHRcdFx0ICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cImNvc3RcIiA6dmFsdWU9XCJjb3N0XCIgcmVhZG9ubHkgc3R5bGU9XCJ3aWR0aCA6IDE3NXB4O1wiPlxuXHRcdFx0XHQgICAgICAgIDwvZGl2PlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cblx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwICBjb2wtbWQtNiBtLWwtMlwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2Rpc2NvdW50JykgfVwiPlxuXHRcdFx0XHQgICAgXHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFwiPlxuXHRcdFx0XHQgICAgICAgICAgICBEaXNjb3VudDpcblx0XHRcdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtOFwiPlxuXHRcdFx0XHQgICAgICAgIFx0PGlucHV0IHR5cGU9XCJudW1iZXJcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJkaXNjb3VudFwiIHYtbW9kZWw9XCJhZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudFwiIG1pbj1cIjBcIiBzdHlsZT1cIndpZHRoIDogMTc1cHg7XCI+XG5cdFx0XHRcdCAgICAgICAgXHQ8c3BhbiBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnZGlzY291bnQnKVwiIHYtdGV4dD1cImFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5nZXQoJ2Rpc2NvdW50JylcIj48L3NwYW4+XG5cdFx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdFx0PC9kaXY+XG5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgdi1pZj1cImFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50ID4gMFwiPlxuXHRcdFx0ICAgIFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgICAgICAgUmVhc29uOlxuXHRcdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwicmVhc29uXCIgdi1tb2RlbD1cImFkZE5ld1NlcnZpY2VGb3JtLnJlYXNvblwiPlxuXHRcdFx0ICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHQgICAgPC9kaXY+XG5cblx0XHQgICAgPGRpdiB2LXNob3c9XCJzaG93Q2hlY2tib3hBbGxcIj5cblx0XHRcdCAgICA8ZGl2IHN0eWxlPVwiaGVpZ2h0OjIwcHg7Y2xlYXI6Ym90aDtcIj48L2Rpdj4gPCEtLSBzcGFjZXIgLS0+XG5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjaGVja2JveC1pbmxpbmVcIj5cblx0ICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBAY2xpY2s9XCJjaGVja0FsbFwiIGNsYXNzPVwiY2hlY2tib3gtYWxsXCI+IENoZWNrIGFsbCByZXF1aXJlZCBkb2N1bWVudHNcblx0ICAgIFx0XHQ8L2xhYmVsPlxuICAgIFx0XHQ8L2Rpdj5cblxuXHRcdCAgICA8ZGl2IHN0eWxlPVwiaGVpZ2h0OjIwcHg7Y2xlYXI6Ym90aDtcIj48L2Rpdj4gPCEtLSBzcGFjZXIgLS0+XG5cbiAgICAgICAgICAgIDxkaXYgdi1mb3I9XCIoZGV0YWlsLCBpbmRleCkgaW4gZGV0YWlsQXJyYXlcIiBjbGFzcz1cInBhbmVsIHBhbmVsLWRlZmF1bHRcIj5cblx0ICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWhlYWRpbmdcIj5cblx0ICAgICAgICAgICAgICAgIHt7IGRldGFpbCB9fVxuXG5cdCAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjaGVja2JveC1pbmxpbmUgcHVsbC1yaWdodFwiPlxuXHRcdFx0ICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIEBjbGljaz1cImNoZWNrQWxsXCIgOmNsYXNzPVwiJ2NoZWNrYm94LScgKyBpbmRleFwiIDpkYXRhLWNob3Nlbi1jbGFzcz1cIidjaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpbmRleFwiPiBDaGVjayBhbGwgcmVxdWlyZWQgZG9jdW1lbnRzXG5cdFx0XHQgICAgICAgIDwvbGFiZWw+XG5cdCAgICAgICAgICAgIDwvZGl2PlxuXHQgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGFuZWwtYm9keVwiPlxuXG5cdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHRcdFx0ICAgICAgIFx0UmVxdWlyZWQgRG9jdW1lbnRzOlxuXHRcdFx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IERvY3NcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3NcIiA6Y2xhc3M9XCInY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcy0nICsgaW5kZXhcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjM1MHB4O1wiIHRhYmluZGV4PVwiNFwiPlxuXHRcdFx0XHQgICAgICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cImRvYyBpbiByZXF1aXJlZERvY3NbaW5kZXhdXCIgOnZhbHVlPVwiZG9jLmlkXCI+XG5cdFx0XHRcdCAgICAgICAgICAgICAgICBcdHt7IGRvYy50aXRsZSB9fVxuXHRcdFx0XHQgICAgICAgICAgICAgICAgPC9vcHRpb24+XG5cdFx0XHRcdCAgICAgICAgICAgIDwvc2VsZWN0PlxuXHRcdFx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdFx0XHQgICAgICAgXHRPcHRpb25hbCBEb2N1bWVudHM6XG5cdFx0XHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0XHQgICAgICAgICAgICA8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XCJTZWxlY3QgRG9jc1wiIGNsYXNzPVwiY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jc1wiIDpjbGFzcz1cIidjaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzLScgKyBpbmRleFwiIG11bHRpcGxlIHN0eWxlPVwid2lkdGg6MzUwcHg7XCIgdGFiaW5kZXg9XCI0XCI+XG5cdFx0XHRcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwiZG9jIGluIG9wdGlvbmFsRG9jc1tpbmRleF1cIiA6dmFsdWU9XCJkb2MuaWRcIj5cblx0XHRcdFx0ICAgICAgICAgICAgICAgIFx0e3sgZG9jLnRpdGxlIH19XG5cdFx0XHRcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cblx0XHRcdFx0ICAgICAgICAgICAgPC9zZWxlY3Q+XG5cdFx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHRcdDwvZGl2PlxuXG5cdCAgICAgICAgICAgIDwvZGl2PlxuXHQgICAgICAgIDwvZGl2PlxuXG4gICAgXHQ8L2Rpdj5cblxuICAgIFx0PGJ1dHRvbiB2LWlmPVwiIWxvYWRpbmdcIiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPlNhdmU8L2J1dHRvbj5cbiAgICBcdDxidXR0b24gdi1pZj1cImxvYWRpbmdcIiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNrLXNwaW5uZXIgc2stc3Bpbm5lci13YXZlXCIgc3R5bGU9XCJ3aWR0aDo0MHB4OyBoZWlnaHQ6MjBweDtcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2stcmVjdDFcIj48L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2stcmVjdDJcIj48L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2stcmVjdDNcIj48L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2stcmVjdDRcIj48L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2stcmVjdDVcIj48L2Rpdj5cbiAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9idXR0b24+XG5cdCAgICA8YSBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2E+XG4gICAgPC9mb3JtPlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuICAgIGV4cG9ydCBkZWZhdWx0e1xuICAgIFx0cHJvcHM6IFsndHJhY2tpbmcnLCdjbGllbnRfaWQnXSxcblxuICAgICAgICBkYXRhKCkge1xuICAgICAgICBcdHJldHVybiB7XG4gICAgICAgIFx0XHRsb2FkaW5nOiBmYWxzZSxcblxuICAgICAgICBcdFx0c2VydmljZXNMaXN0OiBbXSxcblxuICAgICAgICBcdFx0Y29zdDogJycsXG5cbiAgICAgICAgXHRcdGNsaWVudHM6IFtdLFxuXG4gICAgICAgIFx0XHRkZXRhaWxBcnJheTogW10sXG4gICAgICAgIFx0XHRyZXF1aXJlZERvY3M6IFtdLFxuICAgICAgICBcdFx0b3B0aW9uYWxEb2NzOiBbXSxcblxuICAgICAgICBcdFx0c2hvd0NoZWNrYm94QWxsOiBmYWxzZSxcblxuICAgICAgICBcdFx0YWRkTmV3U2VydmljZUZvcm06IG5ldyBGb3JtKHtcblx0XHRcdFx0XHRzZXJ2aWNlczogW10sXG5cdFx0XHRcdFx0bm90ZTogJycsXG5cdFx0XHRcdFx0ZGlzY291bnQ6ICcnLFxuXHRcdFx0XHRcdHJlYXNvbjogJycsXG5cdFx0XHRcdFx0Y2xpZW50X2lkOiB0aGlzLiRwYXJlbnQuJHBhcmVudC51c3JfaWQsXG5cdFx0XHRcdFx0dHJhY2s6IHRoaXMuJHBhcmVudC4kcGFyZW50LnRyYWNraW5nX3NlbGVjdGVkLFxuXHRcdFx0XHRcdHNlcnZpY2VpZDogJycsXG4gICAgICAgIFx0XHRcdGRvY3M6ICcnLFxuICAgICAgICBcdFx0XHRyZXFfZG9jczonJ1x0XHRcdFx0XHRcblx0XHRcdFx0fSx7IGJhc2VVUkwgIDogJy92aXNhL2NsaWVudC8nfSksXHRcbiAgICAgICAgXHR9XG4gICAgICAgIH0sXG5cbiAgICAgICAgbWV0aG9kczoge1xuXG4gICAgICAgIFx0ZmV0Y2hTZXJ2aWNlTGlzdCgpIHtcbiAgICAgICAgXHRcdGF4aW9zLmdldCgnL3Zpc2Evc2VydmljZS1tYW5hZ2VyL3Nob3cnKVxuXHRcdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHRcdHRoaXMuc2VydmljZXNMaXN0ID0gcmVzcG9uc2UuZGF0YS5maWx0ZXIociA9PiByLnBhcmVudF9pZCAhPSAwKTtcblx0XHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHQkKFwiLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VcIikudHJpZ2dlcihcImNob3Nlbjp1cGRhdGVkXCIpO1xuXHRcdFx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikgKTtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0cmVzZXRBZGROZXdTZXJ2aWNlRm9ybSgpIHtcbiAgICAgICAgXHRcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMgPSBbXTtcbiAgICAgICAgXHRcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0ubm90ZSA9ICcnO1xuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudCA9ICcnO1xuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5yZWFzb24gPSAnJztcblxuICAgICAgICBcdFx0dGhpcy5jb3N0ID0gJyc7XG4gICAgICAgIFx0XHQvLyBEZXNlbGVjdCBBbGxcblx0XHRcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2UsIC5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblx0XHRcdFx0XG5cdFx0XHRcdHRoaXMuZGV0YWlsQXJyYXkgPSBbXTtcblx0XHRcdFx0dGhpcy5yZXF1aXJlZERvY3MgPSBbXTtcblx0XHRcdFx0dGhpcy5vcHRpb25hbERvY3MgPSBbXTtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0YWRkTmV3U2VydmljZSgpIHtcbiAgICAgICAgXHRcdHRoaXMubG9hZGluZyA9IHRydWU7XG5cbiAgICAgICAgXHRcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0udHJhY2sgPSB0aGlzLiRwYXJlbnQuJHBhcmVudC50cmFja2luZ19zZWxlY3RlZDtcblxuICAgICAgICBcdFx0dmFyIGRvY3NBcnJheSA9IFtdO1xuICAgICAgICBcdFx0dmFyIHJlcXVpcmVkRG9jcyA9ICcnO1xuICAgICAgICBcdFx0dmFyIG9wdGlvbmFsRG9jcyA9ICcnO1xuICAgICAgICBcdFx0dmFyIGhhc1JlcXVpcmVkRG9jc0Vycm9ycyA9IFtdO1xuICAgICAgICBcdFx0Zm9yKGxldCBpPTA7IGk8dGhpcy5kZXRhaWxBcnJheS5sZW5ndGg7IGkrKykge1xuXHQgICAgICAgIFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcy0nICsgaSArJyBvcHRpb246c2VsZWN0ZWQnKS5lYWNoKCBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdCAgICAgICAgcmVxdWlyZWREb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XG5cdFx0XHRcdCAgICB9KTtcblxuXHQgICAgICAgIFx0XHRsZXQgcmVxdWlyZWRPcHRpb25zID0gJCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MtJyArIGkgKycgb3B0aW9uJykubGVuZ3RoO1xuXHQgICAgICAgIFx0XHRsZXQgcmVxdWlyZWRDb3VudCA9ICQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpICsnIG9wdGlvbjpzZWxlY3RlZCcpLmxlbmd0aDtcblx0ICAgICAgICBcdFx0aWYocmVxdWlyZWRDb3VudCA9PSAwICYmIHJlcXVpcmVkT3B0aW9ucyA+IDApIHtcblx0ICAgICAgICBcdFx0XHRoYXNSZXF1aXJlZERvY3NFcnJvcnMucHVzaCh0aGlzLmRldGFpbEFycmF5W2ldKTtcblx0ICAgICAgICBcdFx0fVxuXG5cdFx0XHRcdCAgICAkKCcuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcy0nICsgaSArICcgb3B0aW9uOnNlbGVjdGVkJykuZWFjaCggZnVuY3Rpb24oZSkge1xuXHRcdFx0XHQgICAgICAgIG9wdGlvbmFsRG9jcyArPSAkKHRoaXMpLnZhbCgpICsgJywnO1xuXHRcdFx0XHQgICAgfSk7XG5cblx0XHRcdFx0ICAgIGRvY3NBcnJheS5wdXNoKChyZXF1aXJlZERvY3MgKyBvcHRpb25hbERvY3MpLnNsaWNlKDAsIC0xKSk7XG5cdFx0XHRcdCAgICByZXF1aXJlZERvY3MgPSAnJztcblx0XHRcdFx0ICAgIG9wdGlvbmFsRG9jcyA9ICcnO1xuXHQgICAgICAgIFx0fVxuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5kb2NzID0gZG9jc0FycmF5O1xuXG4gICAgICAgIFx0XHRpZih0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50ID4gMCAmJiB0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnJlYXNvbiA9PSAnJyl7XG5cdFx0XHQgICAgICAgIHRvYXN0ci5lcnJvcignUGxlYXNlIGlucHV0IHJlYXNvbi4nKTtcblx0XHRcdCAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIFx0XHR9XG4gICAgICAgIFx0XHRlbHNlIGlmKGhhc1JlcXVpcmVkRG9jc0Vycm9ycy5sZW5ndGggPiAwKXtcbiAgICAgICAgXHRcdFx0Zm9yKGxldCBpPTA7IGk8aGFzUmVxdWlyZWREb2NzRXJyb3JzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGZyb20gcmVxdWlyZWQgZG9jdW1lbnRzLicsIGhhc1JlcXVpcmVkRG9jc0Vycm9yc1tpXSk7XG4gICAgICAgIFx0XHRcdH1cbiAgICAgICAgXHRcdFx0dGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIFx0XHR9XG4gICAgICAgIFx0XHRlbHNlIGlmKCh0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnNlcnZpY2VzKS5sZW5ndGggPiAwKXtcbiAgICAgICAgXHRcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5zdWJtaXQoJ3Bvc3QnLCcvY2xpZW50QWRkU2VydmljZScpXG5cdCAgICAgICAgICAgIFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdCAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XG5cdFx0ICAgICAgICAgICAgICAgIFx0XHQvLyB0aGlzLiRwYXJlbnQuJHBhcmVudC5xdWlja1JlcG9ydENsaWVudHNJZCA9IHJlc3BvbnNlLmNsaWVudHNJZDtcblx0XHRcdCAgICAgICAgICAgICAgICBcdC8vIHRoaXMuJHBhcmVudC4kcGFyZW50LnF1aWNrUmVwb3J0Q2xpZW50U2VydmljZXNJZCA9IHJlc3BvbnNlLmNsaWVudFNlcnZpY2VzSWQ7XG5cdFx0XHQgICAgICAgICAgICAgICAgXHQvLyB0aGlzLiRwYXJlbnQuJHBhcmVudC5xdWlja1JlcG9ydFRyYWNraW5ncyA9IHJlc3BvbnNlLnRyYWNraW5ncztcblxuXHRcdFx0ICAgICAgICAgICAgICAgIFx0dGhpcy5yZXNldEFkZE5ld1NlcnZpY2VGb3JtKCk7XG5cdFx0XHQgICAgICAgICAgICAgICAgXHR2YXIgY2xpZW50X2lkID0gdGhpcy4kcGFyZW50LiRwYXJlbnQudXNyX2lkO1xuXHRcdFx0ICAgICAgICAgICAgICAgIFx0dmFyIHRyYWNrID0gdGhpcy4kcGFyZW50LiRwYXJlbnQudHJhY2tpbmdfc2VsZWN0ZWQ7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuc2hvd1NlcnZpY2VzVW5kZXIoY2xpZW50X2lkLHRyYWNrKTtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC51cGRhdGVUcmFja2luZ0xpc3QoY2xpZW50X2lkKTtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC5yZWxvYWRDb21wdXRhdGlvbigpO1xuXHRcdFx0XHRcdFx0XHRcdHZhciBvd2wgPSAkKFwiI293bC1kZW1vXCIpO1xuXHRcdFx0XHRcdFx0XHRcdG93bC5kYXRhKCdvd2xDYXJvdXNlbCcpLmRlc3Ryb3koKTtcblxuXHRcdFx0XHRcdFx0XHRcdC8vIHRoaXMuJHBhcmVudC4kcGFyZW50LiRyZWZzLm11bHRpcGxlcXVpY2tyZXBvcnRyZWYuZmV0Y2hEb2NzKHJlc3BvbnNlLmNsaWVudFNlcnZpY2VzSWQpO1xuXG5cdFx0XHQgICAgICAgICAgICAgICAgXHQkKCcjYWRkLW5ldy1zZXJ2aWNlLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdFx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XG5cdFx0XHQgICAgICAgICAgICAgICAgfVxuXHRcdFx0ICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG5cdFx0ICAgICAgICAgICAgfSlcblx0XHQgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT57XG5cdFx0ICAgICAgICAgICAgICAgIGZvcih2YXIga2V5IGluIGVycm9yKSB7XG5cdFx0XHQgICAgICAgICAgICBcdFx0aWYoZXJyb3IuaGFzT3duUHJvcGVydHkoa2V5KSkgdG9hc3RyLmVycm9yKGVycm9yW2tleV1bMF0pXG5cdFx0XHQgICAgICAgICAgICBcdH1cblx0XHRcdCAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuXHRcdCAgICAgICAgICAgIH0pO1xuICAgICAgICBcdFx0fVxuICAgICAgICBcdFx0ZWxzZXtcblx0XHRcdCAgICAgICAgdG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IHNlcnZpY2UgY2F0ZWdvcnkuJyk7XG5cdFx0XHQgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICBcdFx0fVxuICAgICAgICBcdFx0XG5cdFx0XHR9LFxuXG5cdFx0XHRmZXRjaERvY3Moc2VydmljZV9pZCxuZWVkZWQsb3B0aW9uYWwpe1xuXHRcdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS52YWwoJycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cdFx0XHRcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uc2VydmljZWlkID0gc2VydmljZV9pZDtcblxuXHRcdFx0XHRpZih0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnNlcnZpY2VzLmxlbmd0aCA9PSAxKXtcblx0XHRcdFx0XHRpZihuZWVkZWQ9PScnIHx8IG5lZWRlZD09PXVuZGVmaW5lZCl7XG5cdFx0XHRcdFx0XHR0aGlzLnJlcXVpcmVkRG9jcyA9IFtdO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0IFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC8nK25lZWRlZCsnL3NlcnZpY2UtZG9jcycpXG5cdFx0XHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0XHQgICAgICAgICAgICB0aGlzLnJlcXVpcmVkRG9jcyA9IHJlc3VsdC5kYXRhO1xuXHRcdFx0XHQgICAgICAgIH0pO1xuXHRcdFx0XHRcdH0gXG5cblx0XHQgICAgICAgIFx0aWYob3B0aW9uYWw9PScnIHx8IG9wdGlvbmFsPT09dW5kZWZpbmVkKXtcblx0XHQgICAgICAgIFx0XHR0aGlzLm9wdGlvbmFsRG9jcyA9IFtdO1xuXHRcdCAgICAgICAgXHR9IGVsc2Uge1xuXHRcdFx0XHRcdCBcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJytvcHRpb25hbCsnL3NlcnZpY2UtZG9jcycpXG5cdFx0XHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0XHQgICAgICAgICAgICB0aGlzLm9wdGlvbmFsRG9jcyA9IHJlc3VsdC5kYXRhO1xuXHRcdFx0XHQgICAgICAgIH0pO1xuXHRcdCAgICAgICAgXHR9XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0dGhpcy5yZXF1aXJlZERvY3MgPSBbXTtcblx0XHRcdFx0XHR0aGlzLm9wdGlvbmFsRG9jcyA9IFtdO1xuXHRcdFx0XHR9XG5cblxuXG5cdCAgICAgICAgXHRzZXRUaW1lb3V0KCgpID0+IHtcblx0ICAgICAgICBcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblx0ICAgICAgICBcdH0sIDIwMDApO1xuXHRcdFx0fSxcblxuXHRcdFx0Z2V0RG9jcyhzZXJ2aWNlc0lkKSB7XG5cdFx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvZ2V0LWRvY3MnLCB7XG5cdFx0XHRcdFx0XHRwYXJhbXM6IHtcblx0XHRcdFx0XHQgICAgICBcdHNlcnZpY2VzSWQ6IHNlcnZpY2VzSWRcblx0XHRcdFx0XHQgICAgfVxuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcblx0XHRcdFx0XHRcdHRoaXMuZGV0YWlsQXJyYXkgPSByZXN1bHQuZGF0YS5kZXRhaWxBcnJheTtcblx0XHRcdFx0ICAgICAgICB0aGlzLnJlcXVpcmVkRG9jcyA9IHJlc3VsdC5kYXRhLmRvY3NOZWVkZWRBcnJheTtcblx0XHRcdFx0ICAgICAgICB0aGlzLm9wdGlvbmFsRG9jcyA9IHJlc3VsdC5kYXRhLmRvY3NPcHRpb25hbEFycmF5O1xuXG5cdFx0XHRcdCAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG5cdFx0XHRcdCAgICAgICAgXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS5jaG9zZW4oJ2Rlc3Ryb3knKTtcblx0XHRcdFx0ICAgICAgICBcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLmNob3Nlbih7XG5cdFx0XHRcdFx0XHRcdFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdFx0XHRcdFx0XHRcdG5vX3Jlc3VsdHNfdGV4dDogXCJObyByZXN1bHQvcyBmb3VuZC5cIixcblx0XHRcdFx0XHRcdFx0XHRzZWFyY2hfY29udGFpbnM6IHRydWVcblx0XHRcdFx0XHRcdFx0fSk7XG5cblx0XHRcdCAgICAgICAgXHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cdFx0XHQgICAgICAgIFx0fSwgMTAwMCk7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHR9LFxuXG5cblx0XHRcdGluaXRDaG9zZW5TZWxlY3QoKSB7XG5cdFx0XHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlJykuY2hvc2VuKHtcblx0XHRcdFx0XHR3aWR0aDogXCIxMDAlXCIsXG5cdFx0XHRcdFx0bm9fcmVzdWx0c190ZXh0OiBcIk5vIHJlc3VsdC9zIGZvdW5kLlwiLFxuXHRcdFx0XHRcdHNlYXJjaF9jb250YWluczogdHJ1ZVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRsZXQgdm0gPSB0aGlzO1xuXG5cdFx0XHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlJykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdGxldCBzZXJ2aWNlcyA9ICQodGhpcykudmFsKCk7XG5cblx0XHRcdFx0XHR2bS5zaG93Q2hlY2tib3hBbGwgPSAoc2VydmljZXMubGVuZ3RoID4gMCkgPyB0cnVlIDogZmFsc2U7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0dm0uZ2V0RG9jcyhzZXJ2aWNlcyk7XG5cblx0XHRcdFx0XHR2bS5hZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcyA9IHNlcnZpY2VzO1xuXG5cdFx0XHRcdFx0Ly8gU2V0IGNvc3QgaW5wdXRcblx0XHRcdFx0XHRpZihzZXJ2aWNlcy5sZW5ndGggPT0gMSkge1xuXHRcdFx0XHRcdFx0dm0uc2VydmljZXNMaXN0Lm1hcChzID0+IHtcblx0XHRcdFx0XHRcdFx0aWYocy5pZCA9PSBzZXJ2aWNlc1swXSkge1xuXHRcdFx0XHRcdFx0XHRcdHZtLmNvc3QgPSBzLmNvc3Q7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR2bS5jb3N0ID0gJyc7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXG5cdFx0XHR9LFxuXG5cdFx0XHRjYWxsRGF0YVRhYmxlKCkge1xuXHRcdFx0XHQkKCcuYWRkLW5ldy1zZXJ2aWNlLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQoJy5hZGQtbmV3LXNlcnZpY2UtZGF0YXRhYmxlJykuRGF0YVRhYmxlKHtcblx0XHRcdCAgICAgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxuXHRcdFx0ICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHRcdCAgICAgICAgICAgIGRvbTogJzxcInRvcFwibGY+cnQ8XCJib3R0b21cImlwPjxcImNsZWFyXCI+J1xuXHRcdFx0ICAgICAgICB9KTtcblx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHR9LFxuXG5cdFx0XHRjaGVja0FsbChlKSB7XG5cdFx0XHRcdGxldCBjaGVja2JveCA9IGUudGFyZ2V0LmNsYXNzTmFtZTtcblxuXHRcdFx0XHRpZihjaGVja2JveCA9PSAnY2hlY2tib3gtYWxsJykge1xuXHRcdFx0XHRcdGlmKCQoJy4nICsgY2hlY2tib3gpLmlzKCc6Y2hlY2tlZCcpKVxuXHRcdFx0XHRcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3Mgb3B0aW9uJykucHJvcCgnc2VsZWN0ZWQnLCB0cnVlKTtcblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIGZhbHNlKTtcblxuXHRcdFx0XHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzIG9wdGlvbicpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0bGV0IGNob3NlbiA9IGUudGFyZ2V0LmRhdGFzZXQuY2hvc2VuQ2xhc3M7XG5cblx0XHRcdFx0XHRpZigkKCcuJyArIGNoZWNrYm94KS5pcygnOmNoZWNrZWQnKSlcblx0XHRcdFx0XHRcdCQoJy4nICsgY2hvc2VuICsgJyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIHRydWUpO1xuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdCQoJy4nICsgY2hvc2VuICsgJyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIGZhbHNlKTtcblxuXHRcdFx0XHRcdCQoJy4nICsgY2hvc2VuICsgJyBvcHRpb24nKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xuXHRcdFx0XHR9XG4gXHRcdFx0fVxuICAgICAgICB9LFxuXG4gICAgICAgIGNyZWF0ZWQoKSB7XG4gICAgICAgIFx0dGhpcy5mZXRjaFNlcnZpY2VMaXN0KCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgbW91bnRlZCgpIHtcbiAgICAgICAgXHR0aGlzLmluaXRDaG9zZW5TZWxlY3QoKTtcbiAgICAgICAgfVxuICAgIH1cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuXHQuY2hlY2tib3gtaW5saW5lIHtcblx0XHRtYXJnaW4tdG9wOiAtN3B4O1xuXHR9XG48L3N0eWxlPlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBBZGRTZXJ2aWNlLnZ1ZT8zOGNlNjM3NiIsIjx0ZW1wbGF0ZT5cblx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIiBAc3VibWl0LnByZXZlbnQ9XCJlZGl0Q2xpZW50U2VydmljZVwiPlxuXG5cdFx0PGRpdj5cblxuXG5cdFx0ICAgIFx0PGRpdiBjbGFzc3M9XCJjb2wtbWQtMTJcIj5cblx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNvbC1tZC02XCI+XG5cdFx0XHRcdCAgICBcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCI+XG5cdFx0XHRcdCAgICAgICAgICAgIFRpcDpcblx0XHRcdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtOFwiPlxuXHRcdFx0XHQgICAgICAgIFx0PGlucHV0IHR5cGU9XCJudW1iZXJcIiBtaW49XCIwXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwidGlwXCIgIHYtbW9kZWw9XCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYudGlwXCIgc3R5bGU9XCJ3aWR0aCA6IDE3NXB4O1wiPlxuXHRcdFx0XHQgICAgICAgIDwvZGl2PlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cblx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwICBjb2wtbWQtNiBtLWwtMlwiPlxuXHRcdFx0XHQgICAgXHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFwiPlxuXHRcdFx0XHQgICAgICAgICAgICBTdGF0dXM6XG5cdFx0XHRcdCAgICAgICAgPC9sYWJlbD5cblx0XHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLThcIj5cblx0XHRcdFx0ICAgICAgICBcdDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwic3RhdHVzXCIgaWQ9XCJzdGF0dXNcIiBzdHlsZT1cIndpZHRoIDogMTc1cHg7XCIgdi1tb2RlbD1cIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5zdGF0dXNcIj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJjb21wbGV0ZVwiPkNvbXBsZXRlPC9vcHRpb24+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwib24gcHJvY2Vzc1wiPk9uIFByb2Nlc3M8L29wdGlvbj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJwZW5kaW5nXCI+UGVuZGluZzwvb3B0aW9uPlxuXHQgICAgICAgICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cblx0XHRcdFx0ICAgICAgICA8L2Rpdj5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXG5cdFx0XHRcdDwvZGl2PlxuXG5cdFx0ICAgIFx0PGRpdiBjbGFzc3M9XCJjb2wtbWQtMTJcIj5cblx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNvbC1tZC02XCI+XG5cdFx0XHRcdCAgICBcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCI+XG5cdFx0XHRcdCAgICAgICAgICAgIENvc3Q6XG5cdFx0XHRcdCAgICAgICAgPC9sYWJlbD5cblx0XHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLThcIj5cblx0XHRcdFx0ICAgICAgICBcdDxpbnB1dCB0eXBlPVwibnVtYmVyXCIgbWluPVwiMFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cImNvc3RcIiB2LW1vZGVsPVwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmNvc3RcIiBzdHlsZT1cIndpZHRoIDogMTc1cHg7XCI+XG5cdFx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHRcdCAgICA8L2Rpdj5cblxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgIGNvbC1tZC02IG0tbC0yXCI+XG5cdFx0XHRcdCAgICBcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCI+XG5cdFx0XHRcdCAgICAgICAgICAgIERpc2NvdW50OlxuXHRcdFx0XHQgICAgICAgIDwvbGFiZWw+XG5cdFx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC04XCI+XG5cdFx0XHRcdCAgICAgICAgXHQ8aW5wdXQgdHlwZT1cIm51bWJlclwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cImRpc2NvdW50XCIgdi1tb2RlbD1cIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5kaXNjb3VudFwiIG1pbj1cIjBcIiBzdHlsZT1cIndpZHRoIDogMTc1cHg7XCI+XG5cdFx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdFx0PC9kaXY+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtaWY9XCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZGlzY291bnQgPiAwXCI+XG5cdFx0XHQgICAgXHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCIgPlxuXHRcdFx0ICAgICAgICAgICAgUmVhc29uOlxuXHRcdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwicmVhc29uXCIgdi1tb2RlbD1cIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5yZWFzb25cIj5cblx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgICAgICAgTm90ZTpcblx0XHRcdCAgICAgICAgPC9sYWJlbD5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cIm5vdGVcIiB2LW1vZGVsPVwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2Lm5vdGVcIiBwbGFjZWhvbGRlcj1cImZvciBpbnRlcm5hbCB1c2Ugb25seSwgb25seSBlbXBsb3llZXMgY2FuIHNlZS5cIj5cblx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cblx0XHRcdDxkaXYgY2xhc3NzPVwiY29sLW1kLTEyXCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNvbC1tZC02XCI+XG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdFx0XHQgICAgICAgIFN0YXR1czpcblx0XHRcdFx0XHQgICAgPC9sYWJlbD5cblx0XHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0XHRcdCAgICBcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6NXB4O2NsZWFyOmJvdGg7XCI+PC9kaXY+XG5cdFx0XHRcdFx0ICAgICAgICA8bGFiZWwgY2xhc3M9XCJcIj4gPGlucHV0IHR5cGU9XCJyYWRpb1wiIHZhbHVlPVwiMFwiIG5hbWU9XCJhY3RpdmVcIiB2LW1vZGVsPVwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmFjdGl2ZVwiPiA8aT48L2k+IERpc2FibGVkIDwvbGFiZWw+XG5cblx0XHQgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cIm0tbC0yXCI+IDxpbnB1dCB0eXBlPVwicmFkaW9cIiB2YWx1ZT1cIjFcIiBuYW1lPVwiYWN0aXZlXCIgdi1tb2RlbD1cIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5hY3RpdmVcIj4gPGk+PC9pPiBFbmFibGVkIDwvbGFiZWw+XG5cdFx0ICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNvbC1tZC02XCI+XG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTQgY29udHJvbC1sYWJlbCBcIj5cblx0XHRcdFx0ICAgICAgICBFeHRlbmQgVG86XG5cdFx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XG5cdFx0XHRcdCAgICBcdDxmb3JtIGlkPVwiZGFpbHktZm9ybVwiIGNsYXNzPVwiZm9ybS1pbmxpbmVcIj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlxuXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cImRhdGVcIiB2LW1vZGVsPVwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmV4dGVuZFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBhdXRvY29tcGxldGU9XCJvZmZcIiBuYW1lPVwiZGF0ZVwiPlxuXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHQgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdFx0ICAgICAgIFx0UmVxdWlyZWQgRG9jdW1lbnRzOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgICAgIDxzZWxlY3QgZGF0YS1wbGFjZWhvbGRlcj1cIlNlbGVjdCBEb2NzXCIgY2xhc3M9XCJjaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzXCIgbXVsdGlwbGUgc3R5bGU9XCJ3aWR0aDozNTBweDtcIiB0YWJpbmRleD1cIjRcIj5cblx0XHRcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwiZG9jIGluIHJlcXVpcmVkRG9jc1wiIDp2YWx1ZT1cImRvYy5pZFwiPlxuXHRcdFx0ICAgICAgICAgICAgICAgIFx0e3sgKGRvYy50aXRsZSkudHJpbSgpIH19XG5cdFx0XHQgICAgICAgICAgICAgICAgPC9vcHRpb24+XG5cdFx0XHQgICAgICAgICAgICA8L3NlbGVjdD5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdFx0ICAgICAgIFx0T3B0aW9uYWwgRG9jdW1lbnRzOlxuXHRcdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IERvY3NcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3NcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjM1MHB4O1wiIHRhYmluZGV4PVwiNFwiPlxuXHRcdFx0ICAgICAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XCJkb2MgaW4gb3B0aW9uYWxEb2NzXCIgOnZhbHVlPVwiZG9jLmlkXCI+XG5cdFx0XHQgICAgICAgICAgICAgICAgXHR7eyAoZG9jLnRpdGxlKS50cmltKCkgfX1cblx0XHRcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cblx0XHRcdCAgICAgICAgICAgIDwvc2VsZWN0PlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuIDwhLS1cdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTEyXCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHRcdCAgICAgICAgUmVwb3J0aW5nOlxuXHRcdFx0XHQgICAgPC9sYWJlbD5cblx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdFx0ICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJyZXBvcnRpbmdcIiB2LW1vZGVsPVwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlcG9ydGluZ1wiPlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+IC0tPlxuICAgIFx0PC9kaXY+XG4gICAgXHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTEyXCI+XG5cdFx0XHQ8YSBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2E+XG5cdFx0XHQ8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCI+U2F2ZTwvYnV0dG9uPlxuXHQgICAgPC9kaXY+XG4gICAgPC9mb3JtPlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuICAgIGV4cG9ydCBkZWZhdWx0e1xuICAgIFx0cHJvcHM6IFsndHJhY2tpbmcnLCdjbGllbnRfaWQnXSxcblxuICAgICAgICBkYXRhKCkge1xuICAgICAgICBcdHJldHVybiB7XG5cbiAgICAgICAgXHRcdHNlcnZpY2VzTGlzdDogW10sXG5cbiAgICAgICAgXHRcdGNvc3Q6ICcnLFxuXG4gICAgICAgIFx0XHRjbGllbnRzOiBbXSxcblxuICAgICAgICBcdFx0cmVxdWlyZWREb2NzOiBbXSxcblxuICAgICAgICBcdFx0b3B0aW9uYWxEb2NzOiBbXVxuICAgICAgICBcdFx0XHRcdFxuICAgICAgICBcdH1cbiAgICAgICAgfSxcblx0XHRjb21wdXRlZDoge1xuXG5cdFx0fSxcbiAgICAgICAgbWV0aG9kczoge1xuXG4gICAgICAgIFx0cmVzZXRFZGl0U2VydmljZUZvcm0oKSB7XG4gICAgICAgIFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5jb3N0ID0gMDtcbiAgICAgICAgXHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnRpcCA9IDA7XG4gICAgICAgIFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5zdGF0dXMgPSBcIlwiO1xuICAgICAgICBcdFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuYWN0aXZlID0gMDtcbiAgICAgICAgXHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmRpc2NvdW50ID0gMDtcbiAgICAgICAgXHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlYXNvbiA9IFwiXCI7XG4gICAgICAgIFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5ub3RlID0gXCJcIjtcbiAgICAgICAgXHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmlkID0gbnVsbDtcbiAgICAgICAgXHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlcG9ydGluZyA9ICcnO1xuICAgICAgICBcdFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZXh0ZW5kID0gXCJcIjtcbiAgICAgICAgXHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJjdl9kb2NzID0gXCJcIjtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0ZWRpdENsaWVudFNlcnZpY2UoKSB7XG5cbiAgICAgICAgXHRcdHZhciByZXF1aXJlZERvY3MgPSAnJztcbiAgICAgICAgXHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzIG9wdGlvbjpzZWxlY3RlZCcpLmVhY2goIGZ1bmN0aW9uKGUpIHtcblx0XHRcdCAgICAgICAgcmVxdWlyZWREb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XG5cdFx0XHQgICAgfSk7XG5cbiAgICAgICAgXHRcdHZhciBvcHRpb25hbERvY3MgPSAnJztcbiAgICAgICAgXHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzIG9wdGlvbjpzZWxlY3RlZCcpLmVhY2goIGZ1bmN0aW9uKGUpIHtcblx0XHRcdCAgICAgICAgb3B0aW9uYWxEb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XG5cdFx0XHQgICAgfSk7XG5cblx0XHRcdCAgICB2YXIgZG9jcyA9IHJlcXVpcmVkRG9jcyArIG9wdGlvbmFsRG9jcztcblx0XHRcdFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYucmN2X2RvY3MgPSBkb2NzLnNsaWNlKDAsIC0xKTtcblxuICAgICAgICBcdFx0aWYodGhpcy4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZGlzY291bnQgPiAwICYmIHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlYXNvbiA9PSBudWxsKXtcblx0XHRcdCAgICAgICAgdG9hc3RyLmVycm9yKCdQbGVhc2UgaW5wdXQgcmVhc29uLicpO1xuICAgICAgICBcdFx0fVxuICAgICAgICBcdFx0ZWxzZSBpZihyZXF1aXJlZERvY3MgPT0gJycgJiYgdGhpcy5yZXF1aXJlZERvY3MubGVuZ3RoIT0wKXtcbiAgICAgICAgXHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IFJlcXVpcmVkIERvY3VtZW50cy4nKTtcbiAgICAgICAgXHRcdH1cbiAgICAgIFx0XHRcdGVsc2V7XG5cdCAgICAgICAgXHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnN1Ym1pdCgncG9zdCcsJy9jbGllbnRFZGl0U2VydmljZScpXG5cdCAgICAgICAgICAgIFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdCAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XG5cdFx0XHQgICAgIFx0XHRcdHRoaXMucmVzZXRFZGl0U2VydmljZUZvcm0oKTtcblx0XHRcdCAgICAgXHRcdFx0dmFyIGNsaWVudF9pZCA9IHRoaXMuJHBhcmVudC4kcGFyZW50LnVzcl9pZDtcblx0XHRcdCAgICAgXHRcdFx0dmFyIHRyYWNrID0gdGhpcy4kcGFyZW50LiRwYXJlbnQudHJhY2tpbmdfc2VsZWN0ZWQ7XG5cdFx0XHRcdFx0XHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LnNob3dTZXJ2aWNlc1VuZGVyKGNsaWVudF9pZCx0cmFjayk7XG5cdFx0XHRcdFx0XHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LnVwZGF0ZVRyYWNraW5nTGlzdChjbGllbnRfaWQpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC5yZWxvYWRDb21wdXRhdGlvbigpO1xuXHRcdFx0XHRcdFx0XHR2YXIgb3dsID0gJChcIiNvd2wtZGVtb1wiKTtcblx0XHRcdFx0XHRcdFx0b3dsLmRhdGEoJ293bENhcm91c2VsJykuZGVzdHJveSgpO1xuXHRcdFx0XHRcdFx0XHQkKFwiLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzXCIpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblx0XHRcdCAgICAgICAgICAgICAgICAkKCcjZWRpdC1zZXJ2aWNlLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblx0XHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBVcGRhdGVkIScpO1xuXHRcdFx0ICAgICAgICAgICAgICAgIH1cblx0XHQgICAgICAgICAgICB9KVxuXHRcdCAgICAgICAgICAgIC5jYXRjaChlcnJvciA9Pntcblx0XHQgICAgICAgICAgICAgICAgZm9yKHZhciBrZXkgaW4gZXJyb3IpIHtcblx0XHRcdCAgICAgICAgICAgIFx0XHRpZihlcnJvci5oYXNPd25Qcm9wZXJ0eShrZXkpKSB0b2FzdHIuZXJyb3IoZXJyb3Jba2V5XVswXSlcblx0XHRcdCAgICAgICAgICAgIFx0fVxuXHRcdCAgICAgICAgICAgIH0pO1x0XG4gICAgICBcdFx0XHR9XG5cdFx0XHR9LFxuXG5cdFx0XHRmZXRjaERvY3MoaWQpe1xuXG5cdFx0XHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblxuXHRcdFx0XHRpZih0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5yY3ZfZG9jcyA9PSAnJylcblx0XHRcdFx0XHR2YXIgcmN2ID0gKCBpZFswXS5yY3YhPW51bGwgPyBpZFswXS5yY3Yuc3BsaXQoXCIsXCIpIDogJycpO1xuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0dmFyIHJjdiA9IHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJjdl9kb2NzLnNwbGl0KFwiLFwiKTtcblxuXHRcdFx0IFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC8nK2lkWzBdLmRvY3NfbmVlZGVkKycvc2VydmljZS1kb2NzJylcblx0XHRcdCBcdCAgLnRoZW4ocmVzdWx0ID0+IHtcblx0XHQgICAgICAgICAgICB0aGlzLnJlcXVpcmVkRG9jcyA9IHJlc3VsdC5kYXRhO1xuXHRcdCAgICAgICAgfSk7XG5cdCAgICAgICAgXHRcblx0XHRcdCBcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJytpZFswXS5kb2NzX29wdGlvbmFsKycvc2VydmljZS1kb2NzJylcblx0XHRcdCBcdCAgLnRoZW4ocmVzdWx0ID0+IHtcblx0XHQgICAgICAgICAgICB0aGlzLm9wdGlvbmFsRG9jcyA9IHJlc3VsdC5kYXRhO1xuXHRcdCAgICAgICAgfSk7XG5cblx0XHQgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuXHQgICAgICAgIFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykuY2hvc2VuKCdkZXN0cm95Jyk7XG5cdCAgICAgICAgXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS5jaG9zZW4oe1xuXHRcdFx0XHRcdHdpZHRoOiBcIjEwMCVcIixcblx0XHRcdFx0XHRub19yZXN1bHRzX3RleHQ6IFwiTm8gcmVzdWx0L3MgZm91bmQuXCIsXG5cdFx0XHRcdFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXG5cdFx0XHRcdH0pO1xuXG5cdCAgICAgICAgXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS52YWwocmN2KS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xuICAgICAgICBcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcbiAgICAgICAgXHR9LCAyMDAwKTtcblxuXHRcdFx0fSxcblxuXG5cdFx0XHRjYWxsRGF0YVRhYmxlKCkge1xuXHRcdFx0XHQkKCcuYWRkLW5ldy1zZXJ2aWNlLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQoJy5hZGQtbmV3LXNlcnZpY2UtZGF0YXRhYmxlJykuRGF0YVRhYmxlKHtcblx0XHRcdCAgICAgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxuXHRcdFx0ICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHRcdCAgICAgICAgICAgIGRvbTogJzxcInRvcFwibGY+cnQ8XCJib3R0b21cImlwPjxcImNsZWFyXCI+J1xuXHRcdFx0ICAgICAgICB9KTtcblx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHR9LFxuICAgICAgICB9LFxuXG4gICAgICAgIGNyZWF0ZWQoKSB7XG5cbiAgICAgICAgfSxcblxuICAgICAgICBtb3VudGVkKCkge1xuICAgICAgICBcdC8vIERhdGVQaWNrZXJcblx0ICAgICAgICBcdCQoJy5kYXRlcGlja2VyJykuZGF0ZXBpY2tlcih7XG5cdCAgICAgICAgICAgICAgICB0b2RheUJ0bjogXCJsaW5rZWRcIixcblx0ICAgICAgICAgICAgICAgIGtleWJvYXJkTmF2aWdhdGlvbjogZmFsc2UsXG5cdCAgICAgICAgICAgICAgICBmb3JjZVBhcnNlOiBmYWxzZSxcblx0ICAgICAgICAgICAgICAgIGNhbGVuZGFyV2Vla3M6IHRydWUsXG5cdCAgICAgICAgICAgICAgICBhdXRvY2xvc2U6IHRydWUsXG5cdCAgICAgICAgICAgICAgICBmb3JtYXQ6IFwieXl5eS1tbS1kZFwiLFxuXHQgICAgICAgICAgICB9KVxuXHQgICAgICAgICAgICAub24oJ2NoYW5nZURhdGUnLCBmdW5jdGlvbihlKSB7IC8vIEJvb3RzdHJhcCBEYXRlcGlja2VyIG9uQ2hhbmdlXG5cdCAgICAgICAgICAgIFx0bGV0IGlkID0gZS50YXJnZXQuaWQ7XG5cdFx0XHQgICAgICAgIGxldCBkYXRlID0gZS50YXJnZXQudmFsdWU7XG5cblx0XHRcdCAgICAgICAgY29uc29sZS5sb2coJ0lEIDogJyArIGlkKTtcblx0XHRcdCAgICAgICAgY29uc29sZS5sb2coJ0RBVEUgOiAnICsgZGF0ZSk7XG5cdFx0XHQgICAgICAgIHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmV4dGVuZCA9IGRhdGU7XG5cdFx0XHQgICAgfS5iaW5kKHRoaXMpKVxuXHRcdFx0ICAgIC5vbignY2hhbmdlJywgZnVuY3Rpb24oZSkgeyAvLyBuYXRpdmUgb25DaGFuZ2Vcblx0XHRcdCAgICBcdGxldCBpZCA9IGUudGFyZ2V0LmlkO1xuXHRcdFx0ICAgIFx0bGV0IGRhdGUgPSBlLnRhcmdldC52YWx1ZTtcblxuXHRcdFx0ICAgIFx0Y29uc29sZS5sb2coJ0lEIDogJyArIGlkKTtcblx0XHRcdCAgICAgICAgY29uc29sZS5sb2coJ0RBVEUgOiAnICsgZGF0ZSk7XG5cdFx0XHQgICAgICAgIHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmV4dGVuZCA9IGRhdGU7XG5cdFx0XHQgICAgfS5iaW5kKHRoaXMpKTtcbiAgICAgICAgfVxuICAgIH1cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRWRpdFNlcnZpY2UudnVlPzJmYzcyNzcxIiwiPHRlbXBsYXRlPlxuICAgIDxkaXYgY2xhc3M9XCJpdGVtIGl0ZW0tZml4LXdpZHRoXCI+XG4gICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cIml0ZW0tY2xpY2thYmxlXCIgQGNsaWNrPVwic2hvd1NlcnZpY2VzVW5kZXIoKVwiPlxuICAgICAgICAgICAgPGRpdiA6Y2xhc3M9XCInaW5mby1ib3gnICtiZ1wiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhcnJvdy1wYWNrYWdlXCI+e3tzdGF0dXN9fTwvZGl2PlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5mby1ib3gtaWNvbiBuZXdib3hcIj48aSBjbGFzcz1cImZhIGZhLWRyb3Bib3ggYm94LXBvc2l0aW9uXCI+PC9pPjwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5mby1ib3gtY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImluZm8tYm94LXRleHRcIj5QYWNrYWdlPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImluZm8tYm94LW51bWJlclwiPnt7IHBhY2sudHJhY2tpbmcgfX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwicHJvZ3Jlc3MtZGVzY3JpcHRpb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHt7cGFjay5sb2dfZGF0ZSB8IGNvbW1vbkRhdGV9fVxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9hPlxuICAgIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgcHJvcHM6IFsncGFjayddLFxuICBtZXRob2RzOntcbiAgICBzZWxlY3QoaWQpe1xuICAgICAgIHRoaXMuJHBhcmVudC5zZWxlY3RJdGVtKGlkKVxuICAgIH0sXG4gICAgc2hvd1NlcnZpY2VzVW5kZXIoKXtcbiAgICAgICB0aGlzLiRwYXJlbnQuc2hvd1NlcnZpY2VzVW5kZXIodGhpcy5wYWNrLmNsaWVudF9pZCx0aGlzLnBhY2sudHJhY2tpbmcpO1xuICAgIH0sXG4gIH0sXG4gIGNvbXB1dGVkOntcbiAgICBzdGF0dXMoKXtcbiAgICAgIHN3aXRjaCh0aGlzLnBhY2suc3RhdHVzKXtcbiAgICAgICAgY2FzZSAnMCc6XG4gICAgICAgICAgcmV0dXJuICdFbXB0eSc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJzEnOlxuICAgICAgICAgIHJldHVybiAnT24gUHJvY2Vzcyc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJzInOlxuICAgICAgICAgIHJldHVybiAnUGVuZGluZyc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJzQnOlxuICAgICAgICAgIHJldHVybiAnQ29tcGxldGUnO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIHJldHVybiAnQ29tcGxldGUnO1xuICAgICAgfVxuICAgIH0sXG4gICAgYmcoKXtcbiAgICAgIHN3aXRjaCh0aGlzLnBhY2suc3RhdHVzKXtcbiAgICAgICAgY2FzZSAnMCc6XG4gICAgICAgICAgcmV0dXJuICcgYmctcmVkJztcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnMSc6XG4gICAgICAgICAgcmV0dXJuICcgYmctZ3JlZW4nO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICcyJzpcbiAgICAgICAgICByZXR1cm4gJyBiZy15ZWxsb3cnO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICc0JzpcbiAgICAgICAgICByZXR1cm4gJyBiZy1hcXVhJztcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICByZXR1cm4gJ2JnLWFxdWEnO1xuICAgICAgfVxuICAgIH0sXG4gIH0sXG59XG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gUGFja2FnZS52dWU/MzgwNjFlNDAiLCI8dGVtcGxhdGU+XG4gICAgPHRyPlxuICAgICAgICA8dGQgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPjxzcGFuIDpzdHlsZT1cImlzRGlzYWJsZWRcIj57eyBzZXJ2LnNlcnZpY2VfZGF0ZSB8IGNvbW1vbkRhdGUgfX08L3NwYW4+PC90ZD5cbiAgICAgICAgPHRkIGNsYXNzPVwidGV4dC1jZW50ZXJcIj5cbiAgICAgICAgICA8c3BhbiA6c3R5bGU9XCJpc0Rpc2FibGVkXCIgdi1pZj1cInNlcnYucmVtYXJrc1wiPlxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInJpZ2h0XCIgZGF0YS1jb250YWluZXI9XCJib2R5XCIgOmRhdGEtY29udGVudD1cInNlcnYucmVtYXJrc1wiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCI+XG4gICAgICAgICAgICAgIHt7c2Vydi5kZXRhaWx9fVxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8c3BhbiA6c3R5bGU9XCJpc0Rpc2FibGVkXCIgdi1lbHNlPlxuICAgICAgICAgICAgICB7e3NlcnYuZGV0YWlsfX1cbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgIDwvdGQ+XG4gICAgICAgIDx0ZCBjbGFzcz1cInRleHQtY2VudGVyXCI+PHNwYW4gOnN0eWxlPVwiaXNEaXNhYmxlZFwiPnt7IHNlcnYuY29zdCB8IGN1cnJlbmN5IH19PC9zcGFuPjwvdGQ+XG4gICAgICAgIDx0ZCBjbGFzcz1cInRleHQtY2VudGVyXCI+PHNwYW4gOnN0eWxlPVwiaXNEaXNhYmxlZFwiPnt7IHNlcnYuY2hhcmdlIHwgY3VycmVuY3kgfX08L3NwYW4+PC90ZD5cbiAgICAgICAgPHRkIGNsYXNzPVwidGV4dC1jZW50ZXJcIj48c3BhbiA6c3R5bGU9XCJpc0Rpc2FibGVkXCI+e3sgc2Vydi50aXAgfCBjdXJyZW5jeSB9fTwvc3Bhbj48L3RkPlxuICAgICAgICA8dGQgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPlxuICAgICAgICAgIDxzcGFuIDpzdHlsZT1cImlzRGlzYWJsZWRcIiB2LWlmPVwic2Vydi5yZWFzb25cIj5cbiAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgZGF0YS10b2dnbGU9XCJwb3BvdmVyXCIgZGF0YS1wbGFjZW1lbnQ9XCJyaWdodFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIDpkYXRhLWNvbnRlbnQ9XCJzZXJ2LnJlYXNvblwiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCI+XG4gICAgICAgICAgICAgIHt7IHNlcnYuZGlzY291bnRfYW1vdW50IHwgY3VycmVuY3kgfX1cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4gOnN0eWxlPVwiaXNEaXNhYmxlZFwiIHYtZWxzZT5cbiAgICAgICAgICAgICAge3sgc2Vydi5kaXNjb3VudF9hbW91bnQgfCBjdXJyZW5jeSB9fVxuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPC90ZD5cbiAgICAgICAgPHRkIGNsYXNzPVwidGV4dC1jZW50ZXIgdWNmaXJzdFwiPjxzcGFuIDpzdHlsZT1cImlzRGlzYWJsZWRcIj57eyBzZXJ2LnN0YXR1cyB9fTwvc3Bhbj48L3RkPlxuICAgICAgICA8dGQgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPjxzcGFuIDpzdHlsZT1cImlzRGlzYWJsZWRcIj57eyBzZXJ2LnRyYWNraW5nIH19PC9zcGFuPjwvdGQ+XG4gICAgICAgIDx0ZCBjbGFzcz1cInRleHQtY2VudGVyXCI+PHNwYW4gOnN0eWxlPVwiaXNEaXNhYmxlZFwiIHYtaWY9XCJzZXJ2Lmdyb3VwX2lkID4gMFwiPjxhIDpocmVmPSdcIi92aXNhL2dyb3VwL1wiK3NlcnYuZ3JvdXBfaWQnIHRhcmdldD1cIl9ibGFua1wiPnt7IHNlcnYubmFtZSB9fTwvYT48L3NwYW4+PC90ZD5cbiAgICAgICAgPHRkIGNsYXNzPVwidGV4dC1jZW50ZXJcIiA+PHNwYW4+XG4gICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgQGNsaWNrPVwiZWRpdFNlcnZpY2Uoc2VydilcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInRvcFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIGRhdGEtY29udGVudD1cIkVkaXQgU2VydmljZVwiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCIgdi1zaG93PVwiJHBhcmVudC5zZWxwZXJtaXNzaW9ucy5lZGl0U2VydmljZT09MSB8fCAkcGFyZW50LnNldHRpbmcgPT0gMVwiPlxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtcGVuY2lsLXNxdWFyZS1vXCI+PC9pPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgJm5ic3A7XG4gICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgQGNsaWNrPVwid3JpdGVSZXBvcnQoc2VydilcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInRvcFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIGRhdGEtY29udGVudD1cIkFkZCBRdWljayBSZXBvcnRcIiBkYXRhLXRyaWdnZXI9XCJob3ZlclwiIHYtc2hvdz1cIiRwYXJlbnQuc2VscGVybWlzc2lvbnMuYWRkUXVpY2tSZXBvcnQ9PTEgfHwgJHBhcmVudC5zZXR0aW5nID09IDFcIj5cbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLWxpc3QtYWx0XCI+PC9pPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgJm5ic3A7XG4gICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgQGNsaWNrPVwiYWRkRG9jcyhzZXJ2KVwiIGRhdGEtdG9nZ2xlPVwicG9wb3ZlclwiIGRhdGEtcGxhY2VtZW50PVwidG9wXCIgZGF0YS1jb250YWluZXI9XCJib2R5XCIgZGF0YS1jb250ZW50PVwiQWRkIERvY3VtZW50c1wiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCIgY2xhc3M9XCJoaWRlXCI+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1maWxlXCI+PC9pPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgIDwvc3Bhbj4gXG4gICAgICAgIDwvdGQ+XG5cblxuICAgIDwvdHI+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICBwcm9wczogWydzZXJ2J10sXG4gIG1ldGhvZHM6e1xuICAgIHNlbGVjdChpZCl7XG4gICAgICAgdGhpcy4kcGFyZW50LnNlbGVjdEl0ZW0oaWQpXG4gICAgfSxcbiAgICBzaG93U2VydmljZXNVbmRlcigpe1xuICAgICAgIHRoaXMuJHBhcmVudC5zaG93U2VydmljZXNVbmRlcih0aGlzLnNlcnYuY2xpZW50X2lkLHRoaXMuc2Vydi50cmFja2luZylcbiAgICB9LFxuICAgIGVkaXRTZXJ2aWNlKHNlcnZpY2Upe1xuICAgICAgIHRoaXMuJHBhcmVudC5lZGl0U2VydmljZSh0aGlzLnNlcnYuaWQpXG4gICAgICAgdGhpcy4kcGFyZW50LmZldGNoRG9jczIoW3NlcnZpY2VdKTtcbiAgICB9LFxuICAgIHdyaXRlUmVwb3J0KHNlcnZpY2UpIHtcbiAgICAgIHRoaXMuJHBhcmVudC5mZXRjaERvY3MoW3NlcnZpY2VdKTtcblxuICAgICAgdGhpcy4kcGFyZW50LnF1aWNrUmVwb3J0Q2xpZW50c0lkID0gW3NlcnZpY2UuY2xpZW50X2lkXTtcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICB0aGlzLiRwYXJlbnQucXVpY2tSZXBvcnRDbGllbnRTZXJ2aWNlc0lkID0gW3NlcnZpY2VdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICB0aGlzLiRwYXJlbnQucXVpY2tSZXBvcnRUcmFja2luZ3MgPSBbc2VydmljZS50cmFja2luZ107XG5cbiAgICAgICQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykubW9kYWwoJ3Nob3cnKTtcbiAgICB9LFxuICAgIGFkZERvY3Moc2VydmljZSl7XG4gICAgICB0aGlzLiRwYXJlbnQuZmV0Y2hEb2NzMihbc2VydmljZV0pO1xuICAgICAgJCgnI2RvY3MtbW9kYWwnKS5tb2RhbCgnc2hvdycpO1xuICAgIH1cbiAgfSxcbiAgY29tcHV0ZWQ6e1xuICAgIGlzRGlzYWJsZWQoKXtcbiAgICAgIHJldHVybiAodGhpcy5zZXJ2LmFjdGl2ZSA+IDAgPyBcIlwiOlwidGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XCIpO1xuICAgIH0sXG4gICAgYmcoKXtcbiAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgfSxcbn1cbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBTZXJ2aWNlLnZ1ZT82Y2UxZjZjYiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0FjdGlvbkxvZ3MudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03MmI3NTViNVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9BY3Rpb25Mb2dzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL0FjdGlvbkxvZ3MudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQWN0aW9uTG9ncy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNzJiNzU1YjVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi03MmI3NTViNVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvQWN0aW9uTG9ncy52dWVcbi8vIG1vZHVsZSBpZCA9IDI5XG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIiwiLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuICBNb2RpZmllZCBieSBFdmFuIFlvdSBAeXl4OTkwODAzXG4qL1xuXG52YXIgaGFzRG9jdW1lbnQgPSB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG5cbmlmICh0eXBlb2YgREVCVUcgIT09ICd1bmRlZmluZWQnICYmIERFQlVHKSB7XG4gIGlmICghaGFzRG9jdW1lbnQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgJ3Z1ZS1zdHlsZS1sb2FkZXIgY2Fubm90IGJlIHVzZWQgaW4gYSBub24tYnJvd3NlciBlbnZpcm9ubWVudC4gJyArXG4gICAgXCJVc2UgeyB0YXJnZXQ6ICdub2RlJyB9IGluIHlvdXIgV2VicGFjayBjb25maWcgdG8gaW5kaWNhdGUgYSBzZXJ2ZXItcmVuZGVyaW5nIGVudmlyb25tZW50LlwiXG4gICkgfVxufVxuXG52YXIgbGlzdFRvU3R5bGVzID0gcmVxdWlyZSgnLi9saXN0VG9TdHlsZXMnKVxuXG4vKlxudHlwZSBTdHlsZU9iamVjdCA9IHtcbiAgaWQ6IG51bWJlcjtcbiAgcGFydHM6IEFycmF5PFN0eWxlT2JqZWN0UGFydD5cbn1cblxudHlwZSBTdHlsZU9iamVjdFBhcnQgPSB7XG4gIGNzczogc3RyaW5nO1xuICBtZWRpYTogc3RyaW5nO1xuICBzb3VyY2VNYXA6ID9zdHJpbmdcbn1cbiovXG5cbnZhciBzdHlsZXNJbkRvbSA9IHsvKlxuICBbaWQ6IG51bWJlcl06IHtcbiAgICBpZDogbnVtYmVyLFxuICAgIHJlZnM6IG51bWJlcixcbiAgICBwYXJ0czogQXJyYXk8KG9iaj86IFN0eWxlT2JqZWN0UGFydCkgPT4gdm9pZD5cbiAgfVxuKi99XG5cbnZhciBoZWFkID0gaGFzRG9jdW1lbnQgJiYgKGRvY3VtZW50LmhlYWQgfHwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXSlcbnZhciBzaW5nbGV0b25FbGVtZW50ID0gbnVsbFxudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwXG52YXIgaXNQcm9kdWN0aW9uID0gZmFsc2VcbnZhciBub29wID0gZnVuY3Rpb24gKCkge31cblxuLy8gRm9yY2Ugc2luZ2xlLXRhZyBzb2x1dGlvbiBvbiBJRTYtOSwgd2hpY2ggaGFzIGEgaGFyZCBsaW1pdCBvbiB0aGUgIyBvZiA8c3R5bGU+XG4vLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG52YXIgaXNPbGRJRSA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIC9tc2llIFs2LTldXFxiLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSlcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAocGFyZW50SWQsIGxpc3QsIF9pc1Byb2R1Y3Rpb24pIHtcbiAgaXNQcm9kdWN0aW9uID0gX2lzUHJvZHVjdGlvblxuXG4gIHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIGxpc3QpXG4gIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG4gICAgdmFyIG1heVJlbW92ZSA9IFtdXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgICAgZG9tU3R5bGUucmVmcy0tXG4gICAgICBtYXlSZW1vdmUucHVzaChkb21TdHlsZSlcbiAgICB9XG4gICAgaWYgKG5ld0xpc3QpIHtcbiAgICAgIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhwYXJlbnRJZCwgbmV3TGlzdClcbiAgICAgIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVzID0gW11cbiAgICB9XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXVxuICAgICAgaWYgKGRvbVN0eWxlLnJlZnMgPT09IDApIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKClcbiAgICAgICAgfVxuICAgICAgICBkZWxldGUgc3R5bGVzSW5Eb21bZG9tU3R5bGUuaWRdXG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlc1RvRG9tIChzdHlsZXMgLyogQXJyYXk8U3R5bGVPYmplY3Q+ICovKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBzdHlsZXNbaV1cbiAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgZG9tU3R5bGUucmVmcysrXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pXG4gICAgICB9XG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdKSlcbiAgICAgIH1cbiAgICAgIGlmIChkb21TdHlsZS5wYXJ0cy5sZW5ndGggPiBpdGVtLnBhcnRzLmxlbmd0aCkge1xuICAgICAgICBkb21TdHlsZS5wYXJ0cy5sZW5ndGggPSBpdGVtLnBhcnRzLmxlbmd0aFxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgcGFydHMgPSBbXVxuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIHBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBzdHlsZXNJbkRvbVtpdGVtLmlkXSA9IHsgaWQ6IGl0ZW0uaWQsIHJlZnM6IDEsIHBhcnRzOiBwYXJ0cyB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVN0eWxlRWxlbWVudCAoKSB7XG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpXG4gIHN0eWxlRWxlbWVudC50eXBlID0gJ3RleHQvY3NzJ1xuICBoZWFkLmFwcGVuZENoaWxkKHN0eWxlRWxlbWVudClcbiAgcmV0dXJuIHN0eWxlRWxlbWVudFxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZSAob2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICB2YXIgdXBkYXRlLCByZW1vdmVcbiAgdmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3N0eWxlW2RhdGEtdnVlLXNzci1pZH49XCInICsgb2JqLmlkICsgJ1wiXScpXG5cbiAgaWYgKHN0eWxlRWxlbWVudCkge1xuICAgIGlmIChpc1Byb2R1Y3Rpb24pIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGFuZCBpbiBwcm9kdWN0aW9uIG1vZGUuXG4gICAgICAvLyBzaW1wbHkgZG8gbm90aGluZy5cbiAgICAgIHJldHVybiBub29wXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGJ1dCBpbiBkZXYgbW9kZS5cbiAgICAgIC8vIGZvciBzb21lIHJlYXNvbiBDaHJvbWUgY2FuJ3QgaGFuZGxlIHNvdXJjZSBtYXAgaW4gc2VydmVyLXJlbmRlcmVkXG4gICAgICAvLyBzdHlsZSB0YWdzIC0gc291cmNlIG1hcHMgaW4gPHN0eWxlPiBvbmx5IHdvcmtzIGlmIHRoZSBzdHlsZSB0YWcgaXNcbiAgICAgIC8vIGNyZWF0ZWQgYW5kIGluc2VydGVkIGR5bmFtaWNhbGx5LiBTbyB3ZSByZW1vdmUgdGhlIHNlcnZlciByZW5kZXJlZFxuICAgICAgLy8gc3R5bGVzIGFuZCBpbmplY3QgbmV3IG9uZXMuXG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgaWYgKGlzT2xkSUUpIHtcbiAgICAvLyB1c2Ugc2luZ2xldG9uIG1vZGUgZm9yIElFOS5cbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrK1xuICAgIHN0eWxlRWxlbWVudCA9IHNpbmdsZXRvbkVsZW1lbnQgfHwgKHNpbmdsZXRvbkVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKSlcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCBmYWxzZSlcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCB0cnVlKVxuICB9IGVsc2Uge1xuICAgIC8vIHVzZSBtdWx0aS1zdHlsZS10YWcgbW9kZSBpbiBhbGwgb3RoZXIgY2FzZXNcbiAgICBzdHlsZUVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQpXG4gICAgcmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmopXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmogLyogU3R5bGVPYmplY3RQYXJ0ICovKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcbiAgICAgICAgICBuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJlxuICAgICAgICAgIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKVxuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKVxuICAgIH1cbiAgfVxufVxuXG52YXIgcmVwbGFjZVRleHQgPSAoZnVuY3Rpb24gKCkge1xuICB2YXIgdGV4dFN0b3JlID0gW11cblxuICByZXR1cm4gZnVuY3Rpb24gKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudFxuICAgIHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpXG4gIH1cbn0pKClcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyAoc3R5bGVFbGVtZW50LCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcbiAgdmFyIGNzcyA9IHJlbW92ZSA/ICcnIDogb2JqLmNzc1xuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKVxuICB9IGVsc2Uge1xuICAgIHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKVxuICAgIHZhciBjaGlsZE5vZGVzID0gc3R5bGVFbGVtZW50LmNoaWxkTm9kZXNcbiAgICBpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSlcbiAgICBpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChjc3NOb2RlKVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRvVGFnIChzdHlsZUVsZW1lbnQsIG9iaikge1xuICB2YXIgY3NzID0gb2JqLmNzc1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWFcbiAgdmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXBcblxuICBpZiAobWVkaWEpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKCdtZWRpYScsIG1lZGlhKVxuICB9XG5cbiAgaWYgKHNvdXJjZU1hcCkge1xuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLmNocm9tZS5jb20vZGV2dG9vbHMvZG9jcy9qYXZhc2NyaXB0LWRlYnVnZ2luZ1xuICAgIC8vIHRoaXMgbWFrZXMgc291cmNlIG1hcHMgaW5zaWRlIHN0eWxlIHRhZ3Mgd29yayBwcm9wZXJseSBpbiBDaHJvbWVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VVUkw9JyArIHNvdXJjZU1hcC5zb3VyY2VzWzBdICsgJyAqLydcbiAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNjYwMzg3NVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCwnICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArICcgKi8nXG4gIH1cblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzXG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpXG4gICAgfVxuICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKVxuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9UcmFuc2FjdGlvbkxvZ3MudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi02YWVmYmZhNlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9UcmFuc2FjdGlvbkxvZ3MudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvVHJhbnNhY3Rpb25Mb2dzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFRyYW5zYWN0aW9uTG9ncy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNmFlZmJmYTZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi02YWVmYmZhNlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvVHJhbnNhY3Rpb25Mb2dzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzBcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5kaXYuYWRkLWRvY3VtZW50cy1zZWN0aW9uW2RhdGEtdi03MjJhZTM1YV0ge1xcblxcdFxcdGhlaWdodDogMzAwcHg7XFxuXFx0XFx0cG9zaXRpb246IHJlbGF0aXZlO1xcblxcdFxcdG92ZXJmbG93OiBoaWRkZW47XFxuXFx0XFx0YmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcXG5cXHRcXHRjb2xvcjogI2VjZjBmMTtcXG59XFxuZGl2LmFkZC1kb2N1bWVudHMtc2VjdGlvbiBpbnB1dFtkYXRhLXYtNzIyYWUzNWFdIHtcXG5cXHQgIFxcdGxpbmUtaGVpZ2h0OiAyMDBweDtcXG5cXHQgIFxcdGZvbnQtc2l6ZTogMjAwcHg7XFxuXFx0ICBcXHRwb3NpdGlvbjogYWJzb2x1dGU7XFxuXFx0ICBcXHRvcGFjaXR5OiAwO1xcblxcdCAgXFx0ei1pbmRleDogMTA7XFxufVxcbmRpdi5hZGQtZG9jdW1lbnRzLXNlY3Rpb24gbGFiZWxbZGF0YS12LTcyMmFlMzVhXSB7XFxuICBcXHRcXHRwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBcXHRcXHR6LWluZGV4OiA1O1xcblxcdFxcdGN1cnNvcjogcG9pbnRlcjtcXG5cXHRcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjNEFCQzk2O1xcblxcdFxcdHdpZHRoOiAyMDBweDtcXG5cXHRcXHRoZWlnaHQ6IDUwcHg7XFxuXFx0XFx0Zm9udC1zaXplOiAyMHB4O1xcblxcdFxcdGxpbmUtaGVpZ2h0OiA1MHB4O1xcblxcdFxcdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxuXFx0XFx0dG9wOiAwO1xcblxcdFxcdGxlZnQ6IDA7XFxuXFx0XFx0cmlnaHQ6IDA7XFxuXFx0XFx0Ym90dG9tOiAwO1xcblxcdFxcdG1hcmdpbjogYXV0bztcXG5cXHRcXHR0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcbmRpdi5mb3JtLXNlY3Rpb25bZGF0YS12LTcyMmFlMzVhXSB7XFxuXFx0XFx0aGVpZ2h0OiBhdXRvO1xcbn1cXG5kaXYuZm9ybS1zZWN0aW9uIGltZy5kb2N1bWVudC1pbWFnZVtkYXRhLXYtNzIyYWUzNWFdIHtcXG5cXHRcXHRoZWlnaHQ6IDIwMHB4O1xcbn1cXG5mb3JtW2RhdGEtdi03MjJhZTM1YV0ge1xcblxcdFxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTcyMmFlMzVhXSB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiQ2xpZW50RG9jdW1lbnRzLnZ1ZT8wMzBjY2U2OFwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBK09BO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtFQUNBLGVBQUE7Q0FDQTtBQUNBO0lBQ0EsbUJBQUE7SUFDQSxpQkFBQTtJQUNBLG1CQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7Q0FDQTtBQUNBO0lBQ0EsbUJBQUE7SUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtDQUNBO0FBQ0E7RUFDQSxhQUFBO0NBQ0E7QUFDQTtFQUNBLGNBQUE7Q0FDQTtBQUNBO0VBQ0Esb0JBQUE7Q0FDQTtBQUNBO0VBQ0EsbUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiQ2xpZW50RG9jdW1lbnRzLnZ1ZVwiLFwic291cmNlc0NvbnRlbnRcIjpbXCI8dGVtcGxhdGU+XFxuXFx0XFxuXFx0PGRpdiBjbGFzcz1cXFwicm93XFxcIj5cXG5cXG5cXHRcXHQ8Zm9ybSBAc3VibWl0LnByZXZlbnQ9XFxcImFkZERvY3VtZW50c1xcXCI+XFxuXFxuXFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiY29sLW1kLTRcXFwiIHYtZm9yPVxcXCIoaW1hZ2UsIGluZGV4KSBpbiBpbWFnZXNcXFwiPlxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcInBhbmVsIHBhbmVsLWRlZmF1bHQgZm9ybS1zZWN0aW9uXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicGFuZWwtYm9keVxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgIFxcdDxjZW50ZXI+XFxuXFx0ICAgICAgICAgICAgICAgIFxcdFxcdDxpbWcgOnNyYz1cXFwiaW1hZ2VcXFwiIGNsYXNzPVxcXCJpbWctcmVzcG9uc2l2ZSBpbWctdGh1bWJuYWlsIGRvY3VtZW50LWltYWdlIHpvb21cXFwiIDpkYXRhLW1hZ25pZnktc3JjPVxcXCJpbWFnZVxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgIFxcdDwvY2VudGVyPlxcblxcblxcdCAgICAgICAgICAgICAgICBcXHQ8ZGl2IHN0eWxlPVxcXCJoZWlnaHQ6MTBweDtjbGVhcjpib3RoO1xcXCI+PC9kaXY+XFxuXFxuXFx0ICAgICAgICAgICAgICAgIFxcdDxwPkRvY3VtZW50IFR5cGU6PC9wPlxcblxcdCAgICAgICAgICAgICAgICBcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgXFx0XFx0PHNlbGVjdCBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiB2LW1vZGVsPVxcXCJkb2N1bWVudFR5cGVzW2luZGV4XVxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgIFxcdFxcdFxcdDxvcHRpb24gdi1mb3I9XFxcImRvY3VtZW50VHlwZSBpbiBkZWZhdWx0RG9jdW1lbnRUeXBlc1xcXCIgOnZhbHVlPVxcXCJkb2N1bWVudFR5cGUuaWRcXFwiPlxcblxcdCAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHR7eyBkb2N1bWVudFR5cGUubmFtZSB9fVxcblxcdCAgICAgICAgICAgICAgICBcXHRcXHRcXHQ8L29wdGlvbj5cXG5cXHQgICAgICAgICAgICAgICAgXFx0XFx0PC9zZWxlY3Q+XFxuXFx0ICAgICAgICAgICAgICAgIFxcdDwvZGl2PlxcblxcblxcdCAgICAgICAgICAgICAgICBcXHQ8cD5Jc3N1ZWQgQXQ6PC9wPlxcblxcdCAgICAgICAgICAgICAgICBcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiaW5wdXQtZ3JvdXAgZGF0ZVxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJpbnB1dC1ncm91cC1hZGRvblxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cXFwiZmEgZmEtY2FsZW5kYXJcXFwiPjwvaT5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgPC9zcGFuPlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cXFwiZGF0ZVxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgdi1tb2RlbD1cXFwiaXNzdWVkQXRbaW5kZXhdXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDwvZGl2PlxcblxcblxcdFxcdFxcdCAgICAgICAgICAgIDxwPkV4cGlyZWQgQXQ6PC9wPlxcblxcdFxcdFxcdCAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJpbnB1dC1ncm91cCBkYXRlXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcImlucHV0LWdyb3VwLWFkZG9uXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVxcXCJmYSBmYS1jYWxlbmRhclxcXCI+PC9pPlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8L3NwYW4+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVxcXCJkYXRlXFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiB2LW1vZGVsPVxcXCJleHBpcmVkQXRbaW5kZXhdXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDwvZGl2PlxcblxcblxcdFxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHQ8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGFuZ2VyIGJ0bi14cyBwdWxsLXJpZ2h0XFxcIiBAY2xpY2s9XFxcInJlbW92ZShpbmRleClcXFwiPlJlbW92ZTwvYnV0dG9uPlxcblxcdFxcdFxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFxuXFx0ICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcdCAgICAgICAgICAgIDwvZGl2PlxcblxcdCAgICAgICAgPC9kaXY+XFxuXFxuXFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtNFxcXCI+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwicGFuZWwgcGFuZWwtZGVmYXVsdCBhZGQtZG9jdW1lbnRzLXNlY3Rpb25cXFwiPlxcblxcdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwYW5lbC1ib2R5XFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XFxcImltYWdlLXVwbG9hZFxcXCIgaWQ9XFxcImltYWdlLWxhYmVsXFxcIj48aSBjbGFzcz1cXFwiZmEgZmEtcGx1c1xcXCI+PC9pPiBDaG9vc2UgRmlsZTwvbGFiZWw+XFxuXFx0ICBcXHRcXHRcXHRcXHRcXHQ8aW5wdXQgdHlwZT1cXFwiZmlsZVxcXCIgbmFtZT1cXFwiaW1hZ2VzXFxcIiByZWY9XFxcImZpbGVzXFxcIiBAY2hhbmdlPVxcXCJoYW5kbGVGaWxlc1VwbG9hZFxcXCIgaWQ9XFxcImltYWdlLXVwbG9hZFxcXCIgbXVsdGlwbGUgLz5cXG5cXHQgICAgICAgICAgICAgICAgPC9kaXY+XFxuXFx0ICAgICAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0PGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjFweDtjbGVhcjpib3RoO1xcXCI+PC9kaXY+XFxuXFxuXFx0XFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJzdWJtaXRcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCI+QWRkIERvY3VtZW50L3M8L2J1dHRvbj5cXG5cXHQgICAgXFx0PGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFxuXFx0XFx0PC9mb3JtPlxcblxcbiAgICA8L2Rpdj5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFxuXFx0ZXhwb3J0IGRlZmF1bHQge1xcblxcdFxcdGRhdGEoKSB7XFxuXFx0XFx0XFx0cmV0dXJuIHtcXG5cXHRcXHRcXHRcXHRkZWZhdWx0RG9jdW1lbnRUeXBlczogW10sXFxuXFxuXFx0XFx0XFx0XFx0aW1hZ2VzOiBbXSxcXG5cXHRcXHRcXHRcXHRmaWxlczogW10sXFxuXFx0XFx0XFx0XFx0ZG9jdW1lbnRUeXBlczogW10sXFxuXFx0XFx0XFx0XFx0aXNzdWVkQXQ6IFtdLFxcblxcdFxcdFxcdFxcdGV4cGlyZWRBdDogW11cXG5cXHRcXHRcXHR9XFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRtZXRob2RzOiB7XFxuXFx0XFx0XFx0aW1hZ2VQcmV2aWV3KGlucHV0KSB7XFxuXFx0XFx0XFx0XFx0aWYoaW5wdXQuZmlsZXMpIHtcXG5cXHRcXHRcXHRcXHRcXHR2YXIgdmFsaWRNaW1lVHlwZSA9IFsnZGF0YTppbWFnZS9qcGVnJywgJ2RhdGE6aW1hZ2UvanBnJywgJ2RhdGE6aW1hZ2UvcG5nJ107XFxuXFx0XFx0ICAgICAgICAgICAgdmFyIGZpbGVzQW1vdW50ID0gaW5wdXQuZmlsZXMubGVuZ3RoO1xcblxcblxcdFxcdCAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBmaWxlc0Ftb3VudDsgaSsrKSB7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICByZWFkZXIub25sb2FkID0gKGV2ZW50KSA9PiB7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdGxldCBpbWcgPSBldmVudC50YXJnZXQucmVzdWx0O1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdGxldCBsYXN0Q2hhckluZGV4ID0gaW1nLmluZGV4T2YoJzsnKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRsZXQgbWltZVR5cGUgPSBpbWcuc3Vic3RyKDAsIGxhc3RDaGFySW5kZXgpO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdGlmKCF2YWxpZE1pbWVUeXBlLmluY2x1ZGVzKG1pbWVUeXBlKSkge1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdHRvYXN0ci5lcnJvcignWW91IGNhblxcXFwndCB1cGxvYWQgZmlsZXMgb2YgdGhpcyB0eXBlLicpO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5pbWFnZXMucHVzaChldmVudC50YXJnZXQucmVzdWx0KTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmRvY3VtZW50VHlwZXMucHVzaChudWxsKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmlzc3VlZEF0LnB1c2gobnVsbCk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5leHBpcmVkQXQucHVzaChudWxsKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHR9XFxuXFx0XFx0ICAgICAgICAgICAgICAgIH1cXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoaW5wdXQuZmlsZXNbaV0pO1xcblxcdFxcdCAgICAgICAgICAgIH1cXG5cXG5cXHRcXHQgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcXG5cXHRcXHQgICAgICAgICAgICBcXHQkKCcuem9vbScpLm1hZ25pZnkoKTtcXG5cXHRcXHQgICAgICAgICAgICB9LCAxMDAwKTtcXG5cXHRcXHQgICAgICAgIH1cXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGhhbmRsZUZpbGVzVXBsb2FkKCkge1xcblxcdFxcdFxcdFxcdHRoaXMuZmlsZXMucHVzaCh0aGlzLiRyZWZzLmZpbGVzLmZpbGVzKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdHJlbW92ZShpbmRleCkge1xcblxcdFxcdFxcdFxcdHRoaXMuaW1hZ2VzLnNwbGljZShpbmRleCwgMSk7XFxuXFx0XFx0XFx0XFx0dGhpcy5maWxlcy5zcGxpY2UoaW5kZXgsIDEpO1xcblxcdFxcdFxcdFxcdHRoaXMuZG9jdW1lbnRUeXBlcy5zcGxpY2UoaW5kZXgsIDEpO1xcblxcdFxcdFxcdFxcdHRoaXMuaXNzdWVkQXQuc3BsaWNlKGluZGV4LCAxKTtcXG5cXHRcXHRcXHRcXHR0aGlzLmV4cGlyZWRBdC5zcGxpY2UoaW5kZXgsIDEpO1xcblxcblxcdFxcdFxcdFxcdHNldFRpbWVvdXQoKCkgPT4ge1xcblxcdFxcdCAgICAgICAgICAgICQoJy56b29tJykubWFnbmlmeSgpO1xcblxcdFxcdCAgICAgICAgfSwgNTAwKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGdldERvY3VtZW50VHlwZXMoKSB7XFxuXFx0XFx0XFx0XFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvY2xpZW50LWRvY3VtZW50cycpXFxuXFx0XFx0XFx0XFx0XFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuZGVmYXVsdERvY3VtZW50VHlwZXMgPSByZXNwb25zZS5kYXRhO1xcblxcdFxcdFxcdFxcdFxcdH0pO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0ICBcXHRcXHR2YWxpZGF0ZSgpIHtcXG5cXHQgIFxcdFxcdFxcdHZhciBzdWNjZXNzID0gdHJ1ZTtcXG5cXHQgIFxcdFxcdFxcdGxldCBsZW5ndGggPSB0aGlzLmltYWdlcy5sZW5ndGg7XFxuXFxuXFx0ICBcXHRcXHRcXHRpZihsZW5ndGggPT0gMCkge1xcblxcdCAgXFx0XFx0XFx0XFx0c3VjY2VzcyA9IGZhbHNlO1xcblxcdCAgXFx0XFx0XFx0XFx0dG9hc3RyLmVycm9yKCdEb2N1bWVudC9zIGlzIHJlcXVpcmVkLicpO1xcblxcdCAgXFx0XFx0XFx0fSBlbHNlIHtcXG5cXHRcXHQgIFxcdFxcdFxcdGZvcihsZXQgaT0wOyBpPGxlbmd0aDsgaSsrKSB7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHRpZih0aGlzLmRvY3VtZW50VHlwZXNbaV0gPT0gbnVsbCkge1xcblxcdFxcdCAgXFx0XFx0XFx0XFx0XFx0c3VjY2VzcyA9IGZhbHNlO1xcblxcdFxcdCAgXFx0XFx0XFx0XFx0XFx0dG9hc3RyLmVycm9yKCdbJysoaSsxKSsnXScgKyAnIFRoZSBkb2N1bWVudCB0eXBlIGZpZWxkIGlzIHJlcXVpcmVkLicpO1xcblxcdFxcdCAgXFx0XFx0XFx0XFx0fVxcblxcblxcdFxcdCAgXFx0XFx0XFx0XFx0aWYodGhpcy5pc3N1ZWRBdFtpXSA9PSBudWxsKSB7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHRcXHRzdWNjZXNzID0gZmFsc2U7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ1snKyhpKzEpKyddJyArICcgVGhlIGlzc3VlZCBhdCBmaWVsZCBpcyByZXF1aXJlZC4nKTtcXG5cXHRcXHQgIFxcdFxcdFxcdFxcdH0gZWxzZSBpZighbW9tZW50KHRoaXMuaXNzdWVkQXRbaV0sICdZWVlZLU1NLUREJykuaXNWYWxpZCgpKSB7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHRcXHRzdWNjZXNzID0gZmFsc2U7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ1snKyhpKzEpKyddJyArICcgSW5jb3JyZWN0IGlzc3VlZCBhdCBmaWVsZCBmb3JtYXQuJyk7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHR9XFxuXFxuXFx0XFx0ICBcXHRcXHRcXHRcXHRpZih0aGlzLmV4cGlyZWRBdFtpXSA9PSBudWxsKSB7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHRcXHRzdWNjZXNzID0gZmFsc2U7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ1snKyhpKzEpKyddJyArICcgVGhlIGV4cGlyZWQgYXQgZmllbGQgaXMgcmVxdWlyZWQuJyk7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHR9IGVsc2UgaWYoIW1vbWVudCh0aGlzLmV4cGlyZWRBdFtpXSwgJ1lZWVktTU0tREQnKS5pc1ZhbGlkKCkpIHtcXG5cXHRcXHQgIFxcdFxcdFxcdFxcdFxcdHN1Y2Nlc3MgPSBmYWxzZTtcXG5cXHRcXHQgIFxcdFxcdFxcdFxcdFxcdHRvYXN0ci5lcnJvcignWycrKGkrMSkrJ10nICsgJyBJbmNvcnJlY3QgZXhwaXJlZCBhdCBmaWVsZCBmb3JtYXQuJyk7XFxuXFx0XFx0ICBcXHRcXHRcXHRcXHR9XFxuXFx0XFx0ICBcXHRcXHRcXHR9XFxuXFx0ICBcXHRcXHRcXHR9XFxuXFxuXFx0ICBcXHRcXHRcXHRyZXR1cm4gc3VjY2VzcztcXG5cXHQgIFxcdFxcdH0sXFxuXFxuXFx0ICBcXHRcXHRhZGREb2N1bWVudHMoKSB7XFxuXFx0ICBcXHRcXHRcXHRpZih0aGlzLnZhbGlkYXRlKCkpIHtcXG5cXHQgIFxcdFxcdFxcdFxcdGxldCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xcblxcblxcdFxcdFxcdFxcdFxcdGZvcihsZXQgaT0wOyBpPHRoaXMuZmlsZXMubGVuZ3RoOyBpKyspIHtcXG5cXHRcXHRcXHRcXHRcXHQgICAgbGV0IGZpbGUgPSB0aGlzLmZpbGVzW2ldWzBdO1xcblxcdFxcdFxcdFxcdFxcdCAgICBmb3JtRGF0YS5hcHBlbmQoJ2F0dGFjaG1lbnRzWycraSsnXScsIGZpbGUpO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXG5cXHRcXHRcXHRcXHRcXHRmb3JtRGF0YS5hcHBlbmQoJ3VzZXJJZCcsIHdpbmRvdy5MYXJhdmVsLmRhdGEuaWQpO1xcblxcblxcdFxcdFxcdFxcdFxcdGZvcihsZXQgaT0wOyBpPHRoaXMuaW1hZ2VzLmxlbmd0aDsgaSsrKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0Zm9ybURhdGEuYXBwZW5kKCdpbWFnZXNbJytpKyddJywgdGhpcy5pbWFnZXNbaV0pO1xcblxcdFxcdFxcdFxcdFxcdFxcdGZvcm1EYXRhLmFwcGVuZCgnZG9jdW1lbnRUeXBlc1snK2krJ10nLCB0aGlzLmRvY3VtZW50VHlwZXNbaV0pO1xcblxcdFxcdFxcdFxcdFxcdFxcdGZvcm1EYXRhLmFwcGVuZCgnaXNzdWVkQXRbJytpKyddJywgdGhpcy5pc3N1ZWRBdFtpXSk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0Zm9ybURhdGEuYXBwZW5kKCdleHBpcmVkQXRbJytpKyddJywgdGhpcy5leHBpcmVkQXRbaV0pO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXG5cXHRcXHRcXHRcXHRcXHRheGlvcy5wb3N0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvY2xpZW50LWRvY3VtZW50cy91cGxvYWQtZG9jdW1lbnRzLXYyJyxcXG5cXHRcXHRcXHRcXHRcXHRcXHRmb3JtRGF0YSxcXG5cXHRcXHRcXHRcXHRcXHRcXHR7XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIGhlYWRlcnM6IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICdDb250ZW50LVR5cGUnOiAnbXVsdGlwYXJ0L2Zvcm0tZGF0YSdcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgfVxcblxcdFxcdFxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHRcXHQpXFxuXFx0XFx0ICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XFxuXFx0XFx0ICAgICAgICAgICAgICAgXFx0aWYocmVzcG9uc2UuZGF0YS5zdWNjZXNzKSB7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRvYXN0ci5zdWNjZXNzKCdTdWNjZXNzZnVsbHkgc2F2ZWQuJyk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5pbWFnZXMgPSBbXTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmZpbGVzID0gW107XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5kb2N1bWVudFR5cGVzID0gW107XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5pc3N1ZWRBdCA9IFtdO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuZXhwaXJlZEF0ID0gW107XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZ2V0RmlsZXMoKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQkKCcjaW1hZ2UtdXBsb2FkJykudmFsKG51bGwpO1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdCQoJyNhZGREb2N1bWVudHNNb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0fVxcblxcdFxcdCAgICAgICAgICAgIH0pO1xcblxcdCAgXFx0XFx0XFx0fVxcblxcdCAgXFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y3JlYXRlZCgpIHtcXG5cXHRcXHRcXHR0aGlzLmdldERvY3VtZW50VHlwZXMoKTtcXG5cXHRcXHR9LFxcblxcblxcdFxcdG1vdW50ZWQgKCkge1xcblxcdFxcdFxcdGxldCB2bSA9IHRoaXM7XFxuXFxuXFx0XFx0XFx0JCgnI2ltYWdlLXVwbG9hZCcpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcXG5cXHRcXHQgICAgICAgIHZtLmltYWdlUHJldmlldyh0aGlzKTtcXG5cXHRcXHQgICAgfSk7XFxuXFx0XFx0fVxcblxcdH1cXG48L3NjcmlwdD5cXG5cXG48c3R5bGUgc2NvcGVkPlxcblxcdGRpdi5hZGQtZG9jdW1lbnRzLXNlY3Rpb24ge1xcblxcdFxcdGhlaWdodDogMzAwcHg7XFxuXFx0XFx0cG9zaXRpb246IHJlbGF0aXZlO1xcblxcdFxcdG92ZXJmbG93OiBoaWRkZW47XFxuXFx0XFx0YmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcXG5cXHRcXHRjb2xvcjogI2VjZjBmMTtcXG5cXHR9XFxuXFx0ZGl2LmFkZC1kb2N1bWVudHMtc2VjdGlvbiBpbnB1dCB7XFxuXFx0ICBcXHRsaW5lLWhlaWdodDogMjAwcHg7XFxuXFx0ICBcXHRmb250LXNpemU6IDIwMHB4O1xcblxcdCAgXFx0cG9zaXRpb246IGFic29sdXRlO1xcblxcdCAgXFx0b3BhY2l0eTogMDtcXG5cXHQgIFxcdHotaW5kZXg6IDEwO1xcblxcdH1cXG5cXHRkaXYuYWRkLWRvY3VtZW50cy1zZWN0aW9uIGxhYmVsIHtcXG4gIFxcdFxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIFxcdFxcdHotaW5kZXg6IDU7XFxuXFx0XFx0Y3Vyc29yOiBwb2ludGVyO1xcblxcdFxcdGJhY2tncm91bmQtY29sb3I6ICM0QUJDOTY7XFxuXFx0XFx0d2lkdGg6IDIwMHB4O1xcblxcdFxcdGhlaWdodDogNTBweDtcXG5cXHRcXHRmb250LXNpemU6IDIwcHg7XFxuXFx0XFx0bGluZS1oZWlnaHQ6IDUwcHg7XFxuXFx0XFx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcXG5cXHRcXHR0b3A6IDA7XFxuXFx0XFx0bGVmdDogMDtcXG5cXHRcXHRyaWdodDogMDtcXG5cXHRcXHRib3R0b206IDA7XFxuXFx0XFx0bWFyZ2luOiBhdXRvO1xcblxcdFxcdHRleHQtYWxpZ246IGNlbnRlcjtcXG5cXHR9XFxuXFx0ZGl2LmZvcm0tc2VjdGlvbiB7XFxuXFx0XFx0aGVpZ2h0OiBhdXRvO1xcblxcdH1cXG5cXHRkaXYuZm9ybS1zZWN0aW9uIGltZy5kb2N1bWVudC1pbWFnZSB7XFxuXFx0XFx0aGVpZ2h0OiAyMDBweDtcXG5cXHR9XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNzIyYWUzNWFcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9DbGllbnREb2N1bWVudHMudnVlXG4vLyBtb2R1bGUgaWQgPSAzMDVcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbKF92bS5sb2cyLnllYXIgIT0gbnVsbCkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvdyB0aW1lbGluZS1tb3ZlbWVudCB0aW1lbGluZS1tb3ZlbWVudC10b3Agbm8tZGFzaGVkIG5vLXNpZGUtbWFyZ2luXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtYmFkZ2UgdGltZWxpbmUtZmlsdGVyLW1vdmVtZW50XCJcbiAgfSwgW19jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCIjXCJcbiAgICB9XG4gIH0sIFtfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKF92bS5sb2cyLnllYXIpKV0pXSldKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93IHRpbWVsaW5lLW1vdmVtZW50ICBuby1zaWRlLW1hcmdpblwiXG4gIH0sIFsoX3ZtLmxvZzIubW9udGggIT0gbnVsbCkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLWJhZGdlXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLWJhbGxvb24tZGF0ZS1kYXlcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubG9nMi5kYXkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0aW1lbGluZS1iYWxsb29uLWRhdGUtbW9udGhcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubG9nMi5tb250aCkpXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xMiAgdGltZWxpbmUtaXRlbVwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0yIGNvbC1zbS1vZmZzZXQtMVwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLXBhbmVsIGRlYml0c1wiXG4gIH0sIFtfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtcGFuZWwtdWxcIlxuICB9LCBbX2MoJ2xpJywgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhdXNhbGVcIlxuICB9LCBbX3ZtLl92KFwiQmFsYW5jZTogXCIpLCBfYygnYicsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKF92bS5sb2cyLmRhdGEuYmFsYW5jZSkpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywgW19jKCdwJywgW19jKCdzbWFsbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihcIkFtb3VudCA6IFwiKSwgX2MoJ2InLCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShfdm0ubG9nMi5kYXRhLmFtb3VudCkpKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCBbX2MoJ3AnLCBbX2MoJ3NtYWxsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtbXV0ZWRcIlxuICB9LCBbX3ZtLl92KFwiVHlwZSA6IFwiKSwgX2MoJ2InLCBbX3ZtLl92KF92bS5fcyhfdm0ubG9nMi5kYXRhLnR5cGUpKV0pXSldKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS05XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtcGFuZWwgZGViaXRzXCJcbiAgfSwgW19jKCd1bCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0aW1lbGluZS1wYW5lbC11bFwiXG4gIH0sIFtfYygnbGknLCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1wb3J0b1wiLFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcImlubmVySFRNTFwiOiBfdm0uX3ModGhpcy5kZXRhaWwpXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2F1c2FsZVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sb2cyLmRhdGEucHJvY2Vzc29yKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCBbX2MoJ3AnLCBbX2MoJ3NtYWxsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtbXV0ZWRcIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi10aW1lXCJcbiAgfSksIF92bS5fdihfdm0uX3MoX3ZtLmxvZzIuZGF0YS5kYXRlKSldKV0pXSldKV0pXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi02YWVmYmZhNlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTZhZWZiZmE2XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvVHJhbnNhY3Rpb25Mb2dzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzFcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi1iMmMxNmZmOF0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LWIyYzE2ZmY4XSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG4uY2hlY2tib3gtaW5saW5lW2RhdGEtdi1iMmMxNmZmOF0ge1xcblxcdG1hcmdpbi10b3A6IC03cHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJBZGRTZXJ2aWNlLnZ1ZT8zOGNlNjM3NlwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBaWFBO0NBQ0Esb0JBQUE7Q0FDQTtBQUNBO0NBQ0EsbUJBQUE7Q0FDQTtBQUNBO0NBQ0EsaUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiQWRkU2VydmljZS52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwiYWRkTmV3U2VydmljZVxcXCI+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiaW9zLXNjcm9sbFxcXCI+XFxuXFx0ICAgIFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnc2VydmljZXMnKSB9XFxcIj5cXG5cXHRcXHQgICAgICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0ICAgICAgICAgICAgU2VydmljZTpcXG5cXHRcXHQgICAgICAgIDwvbGFiZWw+XFxuXFxuXFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdCAgICAgICAgICAgIDxzZWxlY3QgZGF0YS1wbGFjZWhvbGRlcj1cXFwiU2VsZWN0IFNlcnZpY2VcXFwiIGNsYXNzPVxcXCJjaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlXFxcIiBtdWx0aXBsZSBzdHlsZT1cXFwid2lkdGg6MzUwcHg7XFxcIiB0YWJpbmRleD1cXFwiNFxcXCI+XFxuXFx0XFx0ICAgICAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XFxcInNlcnZpY2UgaW4gc2VydmljZXNMaXN0XFxcIiA6dmFsdWU9XFxcInNlcnZpY2UuaWRcXFwiIDpkYXRhLWRvY3MtbmVlZGVkPVxcXCJzZXJ2aWNlLmRvY3NfbmVlZGVkXFxcIiA6ZGF0YS1kb2NzLW9wdGlvbmFsPVxcXCJzZXJ2aWNlLmRvY3Nfb3B0aW9uYWxcXFwiPlxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHR7eyAoc2VydmljZS5kZXRhaWwpLnRyaW0oKSB9fVxcblxcdFxcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cXG5cXHRcXHQgICAgICAgICAgICA8L3NlbGVjdD5cXG5cXG5cXHRcXHQgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnc2VydmljZXMnKVxcXCIgdi10ZXh0PVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdzZXJ2aWNlcycpXFxcIj48L3NwYW4+XFxuXFxuXFx0XFx0ICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic3BhY2VyLTEwXFxcIj48L2Rpdj5cXG5cXG5cXHRcXHQgICAgICAgICAgICA8cD5cXG5cXHRcXHQgICAgICAgICAgICBcXHRJZiB5b3Ugd2FudCB0byB2aWV3IGFsbCBzZXJ2aWNlcyBhdCBvbmNlLCBwbGVhc2UgXFxuXFx0XFx0ICAgICAgICAgICAgXFx0PGEgaHJlZj1cXFwiL3Zpc2Evc2VydmljZS92aWV3LWFsbFxcXCIgdGFyZ2V0PVxcXCJfYmxhbmtcXFwiPmNsaWNrIGhlcmU8L2E+XFxuXFx0XFx0ICAgICAgICAgICAgPC9wPlxcblxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHQgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHQgICAgICAgICAgICBOb3RlOlxcblxcdFxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHQgICAgICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0ICAgICAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwibm90ZVxcXCIgdi1tb2RlbD1cXFwiYWRkTmV3U2VydmljZUZvcm0ubm90ZVxcXCIgcGxhY2Vob2xkZXI9XFxcImZvciBpbnRlcm5hbCB1c2Ugb25seSwgb25seSBlbXBsb3llZXMgY2FuIHNlZS5cXFwiPlxcblxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcblxcdFxcdCAgICA8ZGl2IHYtc2hvdz1cXFwiYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMubGVuZ3RoIDw9IDFcXFwiPlxcblxcdFxcdCAgICBcXHQ8ZGl2IGNsYXNzcz1cXFwiY29sLW1kLTEyXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cCBjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTQgY29udHJvbC1sYWJlbCBcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIENvc3Q6XFxuXFx0XFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLThcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImNvc3RcXFwiIDp2YWx1ZT1cXFwiY29zdFxcXCIgcmVhZG9ubHkgc3R5bGU9XFxcIndpZHRoIDogMTc1cHg7XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDwvZGl2PlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cCAgY29sLW1kLTYgbS1sLTJcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnZGlzY291bnQnKSB9XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgRGlzY291bnQ6XFxuXFx0XFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLThcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0PGlucHV0IHR5cGU9XFxcIm51bWJlclxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwiZGlzY291bnRcXFwiIHYtbW9kZWw9XFxcImFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50XFxcIiBtaW49XFxcIjBcXFwiIHN0eWxlPVxcXCJ3aWR0aCA6IDE3NXB4O1xcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHQ8c3BhbiBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnZGlzY291bnQnKVxcXCIgdi10ZXh0PVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdkaXNjb3VudCcpXFxcIj48L3NwYW4+XFxuXFx0XFx0XFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0PC9kaXY+XFxuXFxuXFxuXFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIHYtaWY9XFxcImFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50ID4gMFxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgUmVhc29uOlxcblxcdFxcdFxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHQgICAgICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwicmVhc29uXFxcIiB2LW1vZGVsPVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5yZWFzb25cXFwiPlxcblxcdFxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXG5cXHRcXHQgICAgPGRpdiB2LXNob3c9XFxcInNob3dDaGVja2JveEFsbFxcXCI+XFxuXFx0XFx0XFx0ICAgIDxkaXYgc3R5bGU9XFxcImhlaWdodDoyMHB4O2NsZWFyOmJvdGg7XFxcIj48L2Rpdj4gPCEtLSBzcGFjZXIgLS0+XFxuXFxuXFx0XFx0XFx0ICAgIDxsYWJlbCBjbGFzcz1cXFwiY2hlY2tib3gtaW5saW5lXFxcIj5cXG5cXHQgICAgXFx0XFx0XFx0PGlucHV0IHR5cGU9XFxcImNoZWNrYm94XFxcIiBAY2xpY2s9XFxcImNoZWNrQWxsXFxcIiBjbGFzcz1cXFwiY2hlY2tib3gtYWxsXFxcIj4gQ2hlY2sgYWxsIHJlcXVpcmVkIGRvY3VtZW50c1xcblxcdCAgICBcXHRcXHQ8L2xhYmVsPlxcbiAgICBcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQgICAgPGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjIwcHg7Y2xlYXI6Ym90aDtcXFwiPjwvZGl2PiA8IS0tIHNwYWNlciAtLT5cXG5cXG4gICAgICAgICAgICA8ZGl2IHYtZm9yPVxcXCIoZGV0YWlsLCBpbmRleCkgaW4gZGV0YWlsQXJyYXlcXFwiIGNsYXNzPVxcXCJwYW5lbCBwYW5lbC1kZWZhdWx0XFxcIj5cXG5cXHQgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwYW5lbC1oZWFkaW5nXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAge3sgZGV0YWlsIH19XFxuXFxuXFx0ICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cXFwiY2hlY2tib3gtaW5saW5lIHB1bGwtcmlnaHRcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgIDxpbnB1dCB0eXBlPVxcXCJjaGVja2JveFxcXCIgQGNsaWNrPVxcXCJjaGVja0FsbFxcXCIgOmNsYXNzPVxcXCInY2hlY2tib3gtJyArIGluZGV4XFxcIiA6ZGF0YS1jaG9zZW4tY2xhc3M9XFxcIidjaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpbmRleFxcXCI+IENoZWNrIGFsbCByZXF1aXJlZCBkb2N1bWVudHNcXG5cXHRcXHRcXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0ICAgICAgICAgICAgPC9kaXY+XFxuXFx0ICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicGFuZWwtYm9keVxcXCI+XFxuXFxuXFx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICBcXHRSZXF1aXJlZCBEb2N1bWVudHM6XFxuXFx0XFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFxuXFx0XFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVxcXCJTZWxlY3QgRG9jc1xcXCIgY2xhc3M9XFxcImNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3NcXFwiIDpjbGFzcz1cXFwiJ2Nob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MtJyArIGluZGV4XFxcIiBtdWx0aXBsZSBzdHlsZT1cXFwid2lkdGg6MzUwcHg7XFxcIiB0YWJpbmRleD1cXFwiNFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XFxcImRvYyBpbiByZXF1aXJlZERvY3NbaW5kZXhdXFxcIiA6dmFsdWU9XFxcImRvYy5pZFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdHt7IGRvYy50aXRsZSB9fVxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICA8L3NlbGVjdD5cXG5cXHRcXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgIFxcdE9wdGlvbmFsIERvY3VtZW50czpcXG5cXHRcXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXG5cXHRcXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICA8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XFxcIlNlbGVjdCBEb2NzXFxcIiBjbGFzcz1cXFwiY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jc1xcXCIgOmNsYXNzPVxcXCInY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcy0nICsgaW5kZXhcXFwiIG11bHRpcGxlIHN0eWxlPVxcXCJ3aWR0aDozNTBweDtcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cXFwiZG9jIGluIG9wdGlvbmFsRG9jc1tpbmRleF1cXFwiIDp2YWx1ZT1cXFwiZG9jLmlkXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0e3sgZG9jLnRpdGxlIH19XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIDwvc2VsZWN0PlxcblxcdFxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHQgICAgICAgICAgICA8L2Rpdj5cXG5cXHQgICAgICAgIDwvZGl2PlxcblxcbiAgICBcXHQ8L2Rpdj5cXG5cXG4gICAgXFx0PGJ1dHRvbiB2LWlmPVxcXCIhbG9hZGluZ1xcXCIgdHlwZT1cXFwic3VibWl0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcXFwiPlNhdmU8L2J1dHRvbj5cXG4gICAgXFx0PGJ1dHRvbiB2LWlmPVxcXCJsb2FkaW5nXFxcIiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodFxcXCI+XFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic2stc3Bpbm5lciBzay1zcGlubmVyLXdhdmVcXFwiIHN0eWxlPVxcXCJ3aWR0aDo0MHB4OyBoZWlnaHQ6MjBweDtcXFwiPlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJzay1yZWN0MVxcXCI+PC9kaXY+XFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInNrLXJlY3QyXFxcIj48L2Rpdj5cXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic2stcmVjdDNcXFwiPjwvZGl2PlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJzay1yZWN0NFxcXCI+PC9kaXY+XFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInNrLXJlY3Q1XFxcIj48L2Rpdj5cXG4gICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICA8L2J1dHRvbj5cXG5cXHQgICAgPGEgY2xhc3M9XFxcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFxcXCIgZGF0YS1kaXNtaXNzPVxcXCJtb2RhbFxcXCI+Q2xvc2U8L2E+XFxuICAgIDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuICAgIGV4cG9ydCBkZWZhdWx0e1xcbiAgICBcXHRwcm9wczogWyd0cmFja2luZycsJ2NsaWVudF9pZCddLFxcblxcbiAgICAgICAgZGF0YSgpIHtcXG4gICAgICAgIFxcdHJldHVybiB7XFxuICAgICAgICBcXHRcXHRsb2FkaW5nOiBmYWxzZSxcXG5cXG4gICAgICAgIFxcdFxcdHNlcnZpY2VzTGlzdDogW10sXFxuXFxuICAgICAgICBcXHRcXHRjb3N0OiAnJyxcXG5cXG4gICAgICAgIFxcdFxcdGNsaWVudHM6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0ZGV0YWlsQXJyYXk6IFtdLFxcbiAgICAgICAgXFx0XFx0cmVxdWlyZWREb2NzOiBbXSxcXG4gICAgICAgIFxcdFxcdG9wdGlvbmFsRG9jczogW10sXFxuXFxuICAgICAgICBcXHRcXHRzaG93Q2hlY2tib3hBbGw6IGZhbHNlLFxcblxcbiAgICAgICAgXFx0XFx0YWRkTmV3U2VydmljZUZvcm06IG5ldyBGb3JtKHtcXG5cXHRcXHRcXHRcXHRcXHRzZXJ2aWNlczogW10sXFxuXFx0XFx0XFx0XFx0XFx0bm90ZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0ZGlzY291bnQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHJlYXNvbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0Y2xpZW50X2lkOiB0aGlzLiRwYXJlbnQuJHBhcmVudC51c3JfaWQsXFxuXFx0XFx0XFx0XFx0XFx0dHJhY2s6IHRoaXMuJHBhcmVudC4kcGFyZW50LnRyYWNraW5nX3NlbGVjdGVkLFxcblxcdFxcdFxcdFxcdFxcdHNlcnZpY2VpZDogJycsXFxuICAgICAgICBcXHRcXHRcXHRkb2NzOiAnJyxcXG4gICAgICAgIFxcdFxcdFxcdHJlcV9kb2NzOicnXFx0XFx0XFx0XFx0XFx0XFxuXFx0XFx0XFx0XFx0fSx7IGJhc2VVUkwgIDogJy92aXNhL2NsaWVudC8nfSksXFx0XFxuICAgICAgICBcXHR9XFxuICAgICAgICB9LFxcblxcbiAgICAgICAgbWV0aG9kczoge1xcblxcbiAgICAgICAgXFx0ZmV0Y2hTZXJ2aWNlTGlzdCgpIHtcXG4gICAgICAgIFxcdFxcdGF4aW9zLmdldCgnL3Zpc2Evc2VydmljZS1tYW5hZ2VyL3Nob3cnKVxcblxcdFxcdFxcdFxcdFxcdC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLnNlcnZpY2VzTGlzdCA9IHJlc3BvbnNlLmRhdGEuZmlsdGVyKHIgPT4gci5wYXJlbnRfaWQgIT0gMCk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0JChcXFwiLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VcXFwiKS50cmlnZ2VyKFxcXCJjaG9zZW46dXBkYXRlZFxcXCIpO1xcblxcdFxcdFxcdFxcdFxcdFxcdH0sIDEwMDApO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSApO1xcbiAgICAgICAgXFx0fSxcXG5cXG4gICAgICAgIFxcdHJlc2V0QWRkTmV3U2VydmljZUZvcm0oKSB7XFxuICAgICAgICBcXHRcXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnNlcnZpY2VzID0gW107XFxuICAgICAgICBcXHRcXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLm5vdGUgPSAnJztcXG4gICAgICAgIFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uZGlzY291bnQgPSAnJztcXG4gICAgICAgIFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0ucmVhc29uID0gJyc7XFxuXFxuICAgICAgICBcXHRcXHR0aGlzLmNvc3QgPSAnJztcXG4gICAgICAgIFxcdFxcdC8vIERlc2VsZWN0IEFsbFxcblxcdFxcdFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlLCAuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS52YWwoJycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XFxuXFx0XFx0XFx0XFx0XFxuXFx0XFx0XFx0XFx0dGhpcy5kZXRhaWxBcnJheSA9IFtdO1xcblxcdFxcdFxcdFxcdHRoaXMucmVxdWlyZWREb2NzID0gW107XFxuXFx0XFx0XFx0XFx0dGhpcy5vcHRpb25hbERvY3MgPSBbXTtcXG4gICAgICAgIFxcdH0sXFxuXFxuICAgICAgICBcXHRhZGROZXdTZXJ2aWNlKCkge1xcbiAgICAgICAgXFx0XFx0dGhpcy5sb2FkaW5nID0gdHJ1ZTtcXG5cXG4gICAgICAgIFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0udHJhY2sgPSB0aGlzLiRwYXJlbnQuJHBhcmVudC50cmFja2luZ19zZWxlY3RlZDtcXG5cXG4gICAgICAgIFxcdFxcdHZhciBkb2NzQXJyYXkgPSBbXTtcXG4gICAgICAgIFxcdFxcdHZhciByZXF1aXJlZERvY3MgPSAnJztcXG4gICAgICAgIFxcdFxcdHZhciBvcHRpb25hbERvY3MgPSAnJztcXG4gICAgICAgIFxcdFxcdHZhciBoYXNSZXF1aXJlZERvY3NFcnJvcnMgPSBbXTtcXG4gICAgICAgIFxcdFxcdGZvcihsZXQgaT0wOyBpPHRoaXMuZGV0YWlsQXJyYXkubGVuZ3RoOyBpKyspIHtcXG5cXHQgICAgICAgIFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpICsnIG9wdGlvbjpzZWxlY3RlZCcpLmVhY2goIGZ1bmN0aW9uKGUpIHtcXG5cXHRcXHRcXHRcXHQgICAgICAgIHJlcXVpcmVkRG9jcyArPSAkKHRoaXMpLnZhbCgpICsgJywnO1xcblxcdFxcdFxcdFxcdCAgICB9KTtcXG5cXG5cXHQgICAgICAgIFxcdFxcdGxldCByZXF1aXJlZE9wdGlvbnMgPSAkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcy0nICsgaSArJyBvcHRpb24nKS5sZW5ndGg7XFxuXFx0ICAgICAgICBcXHRcXHRsZXQgcmVxdWlyZWRDb3VudCA9ICQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpICsnIG9wdGlvbjpzZWxlY3RlZCcpLmxlbmd0aDtcXG5cXHQgICAgICAgIFxcdFxcdGlmKHJlcXVpcmVkQ291bnQgPT0gMCAmJiByZXF1aXJlZE9wdGlvbnMgPiAwKSB7XFxuXFx0ICAgICAgICBcXHRcXHRcXHRoYXNSZXF1aXJlZERvY3NFcnJvcnMucHVzaCh0aGlzLmRldGFpbEFycmF5W2ldKTtcXG5cXHQgICAgICAgIFxcdFxcdH1cXG5cXG5cXHRcXHRcXHRcXHQgICAgJCgnLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MtJyArIGkgKyAnIG9wdGlvbjpzZWxlY3RlZCcpLmVhY2goIGZ1bmN0aW9uKGUpIHtcXG5cXHRcXHRcXHRcXHQgICAgICAgIG9wdGlvbmFsRG9jcyArPSAkKHRoaXMpLnZhbCgpICsgJywnO1xcblxcdFxcdFxcdFxcdCAgICB9KTtcXG5cXG5cXHRcXHRcXHRcXHQgICAgZG9jc0FycmF5LnB1c2goKHJlcXVpcmVkRG9jcyArIG9wdGlvbmFsRG9jcykuc2xpY2UoMCwgLTEpKTtcXG5cXHRcXHRcXHRcXHQgICAgcmVxdWlyZWREb2NzID0gJyc7XFxuXFx0XFx0XFx0XFx0ICAgIG9wdGlvbmFsRG9jcyA9ICcnO1xcblxcdCAgICAgICAgXFx0fVxcbiAgICAgICAgXFx0XFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5kb2NzID0gZG9jc0FycmF5O1xcblxcbiAgICAgICAgXFx0XFx0aWYodGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudCA+IDAgJiYgdGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5yZWFzb24gPT0gJycpe1xcblxcdFxcdFxcdCAgICAgICAgdG9hc3RyLmVycm9yKCdQbGVhc2UgaW5wdXQgcmVhc29uLicpO1xcblxcdFxcdFxcdCAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XFxuICAgICAgICBcXHRcXHR9XFxuICAgICAgICBcXHRcXHRlbHNlIGlmKGhhc1JlcXVpcmVkRG9jc0Vycm9ycy5sZW5ndGggPiAwKXtcXG4gICAgICAgIFxcdFxcdFxcdGZvcihsZXQgaT0wOyBpPGhhc1JlcXVpcmVkRG9jc0Vycm9ycy5sZW5ndGg7IGkrKykge1xcbiAgICAgICAgXFx0XFx0XFx0XFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGZyb20gcmVxdWlyZWQgZG9jdW1lbnRzLicsIGhhc1JlcXVpcmVkRG9jc0Vycm9yc1tpXSk7XFxuICAgICAgICBcXHRcXHRcXHR9XFxuICAgICAgICBcXHRcXHRcXHR0aGlzLmxvYWRpbmcgPSBmYWxzZTtcXG4gICAgICAgIFxcdFxcdH1cXG4gICAgICAgIFxcdFxcdGVsc2UgaWYoKHRoaXMuYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMpLmxlbmd0aCA+IDApe1xcbiAgICAgICAgXFx0XFx0XFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5zdWJtaXQoJ3Bvc3QnLCcvY2xpZW50QWRkU2VydmljZScpXFxuXFx0ICAgICAgICAgICAgXFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdCAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdFxcdC8vIHRoaXMuJHBhcmVudC4kcGFyZW50LnF1aWNrUmVwb3J0Q2xpZW50c0lkID0gcmVzcG9uc2UuY2xpZW50c0lkO1xcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHQvLyB0aGlzLiRwYXJlbnQuJHBhcmVudC5xdWlja1JlcG9ydENsaWVudFNlcnZpY2VzSWQgPSByZXNwb25zZS5jbGllbnRTZXJ2aWNlc0lkO1xcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHQvLyB0aGlzLiRwYXJlbnQuJHBhcmVudC5xdWlja1JlcG9ydFRyYWNraW5ncyA9IHJlc3BvbnNlLnRyYWNraW5ncztcXG5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0dGhpcy5yZXNldEFkZE5ld1NlcnZpY2VGb3JtKCk7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdHZhciBjbGllbnRfaWQgPSB0aGlzLiRwYXJlbnQuJHBhcmVudC51c3JfaWQ7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdHZhciB0cmFjayA9IHRoaXMuJHBhcmVudC4kcGFyZW50LnRyYWNraW5nX3NlbGVjdGVkO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LnNob3dTZXJ2aWNlc1VuZGVyKGNsaWVudF9pZCx0cmFjayk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQudXBkYXRlVHJhY2tpbmdMaXN0KGNsaWVudF9pZCk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQucmVsb2FkQ29tcHV0YXRpb24oKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR2YXIgb3dsID0gJChcXFwiI293bC1kZW1vXFxcIik7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0b3dsLmRhdGEoJ293bENhcm91c2VsJykuZGVzdHJveSgpO1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdC8vIHRoaXMuJHBhcmVudC4kcGFyZW50LiRyZWZzLm11bHRpcGxlcXVpY2tyZXBvcnRyZWYuZmV0Y2hEb2NzKHJlc3BvbnNlLmNsaWVudFNlcnZpY2VzSWQpO1xcblxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHQkKCcjYWRkLW5ldy1zZXJ2aWNlLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcXG5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIH1cXG5cXHRcXHRcXHQgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcXG5cXHRcXHQgICAgICAgICAgICB9KVxcblxcdFxcdCAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PntcXG5cXHRcXHQgICAgICAgICAgICAgICAgZm9yKHZhciBrZXkgaW4gZXJyb3IpIHtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHRpZihlcnJvci5oYXNPd25Qcm9wZXJ0eShrZXkpKSB0b2FzdHIuZXJyb3IoZXJyb3Jba2V5XVswXSlcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHR9XFxuXFx0XFx0XFx0ICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XFxuXFx0XFx0ICAgICAgICAgICAgfSk7XFxuICAgICAgICBcXHRcXHR9XFxuICAgICAgICBcXHRcXHRlbHNle1xcblxcdFxcdFxcdCAgICAgICAgdG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IHNlcnZpY2UgY2F0ZWdvcnkuJyk7XFxuXFx0XFx0XFx0ICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcXG4gICAgICAgIFxcdFxcdH1cXG4gICAgICAgIFxcdFxcdFxcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0ZmV0Y2hEb2NzKHNlcnZpY2VfaWQsbmVlZGVkLG9wdGlvbmFsKXtcXG5cXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS52YWwoJycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XFxuXFx0XFx0XFx0XFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlaWQgPSBzZXJ2aWNlX2lkO1xcblxcblxcdFxcdFxcdFxcdGlmKHRoaXMuYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMubGVuZ3RoID09IDEpe1xcblxcdFxcdFxcdFxcdFxcdGlmKG5lZWRlZD09JycgfHwgbmVlZGVkPT09dW5kZWZpbmVkKXtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLnJlcXVpcmVkRG9jcyA9IFtdO1xcblxcdFxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0IFxcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJytuZWVkZWQrJy9zZXJ2aWNlLWRvY3MnKVxcblxcdFxcdFxcdFxcdFxcdCBcXHQgIC50aGVuKHJlc3VsdCA9PiB7XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgdGhpcy5yZXF1aXJlZERvY3MgPSByZXN1bHQuZGF0YTtcXG5cXHRcXHRcXHRcXHQgICAgICAgIH0pO1xcblxcdFxcdFxcdFxcdFxcdH0gXFxuXFxuXFx0XFx0ICAgICAgICBcXHRpZihvcHRpb25hbD09JycgfHwgb3B0aW9uYWw9PT11bmRlZmluZWQpe1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy5vcHRpb25hbERvY3MgPSBbXTtcXG5cXHRcXHQgICAgICAgIFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0IFxcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJytvcHRpb25hbCsnL3NlcnZpY2UtZG9jcycpXFxuXFx0XFx0XFx0XFx0XFx0IFxcdCAgLnRoZW4ocmVzdWx0ID0+IHtcXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICB0aGlzLm9wdGlvbmFsRG9jcyA9IHJlc3VsdC5kYXRhO1xcblxcdFxcdFxcdFxcdCAgICAgICAgfSk7XFxuXFx0XFx0ICAgICAgICBcXHR9XFxuXFx0XFx0XFx0XFx0fSBlbHNlIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLnJlcXVpcmVkRG9jcyA9IFtdO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMub3B0aW9uYWxEb2NzID0gW107XFxuXFx0XFx0XFx0XFx0fVxcblxcblxcblxcblxcdCAgICAgICAgXFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0ICAgICAgICBcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcdCAgICAgICAgXFx0fSwgMjAwMCk7XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRnZXREb2NzKHNlcnZpY2VzSWQpIHtcXG5cXHRcXHRcXHRcXHRheGlvcy5nZXQoJy92aXNhL2dldC1kb2NzJywge1xcblxcdFxcdFxcdFxcdFxcdFxcdHBhcmFtczoge1xcblxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdHNlcnZpY2VzSWQ6IHNlcnZpY2VzSWRcXG5cXHRcXHRcXHRcXHRcXHQgICAgfVxcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LnRoZW4ocmVzdWx0ID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmRldGFpbEFycmF5ID0gcmVzdWx0LmRhdGEuZGV0YWlsQXJyYXk7XFxuXFx0XFx0XFx0XFx0ICAgICAgICB0aGlzLnJlcXVpcmVkRG9jcyA9IHJlc3VsdC5kYXRhLmRvY3NOZWVkZWRBcnJheTtcXG5cXHRcXHRcXHRcXHQgICAgICAgIHRoaXMub3B0aW9uYWxEb2NzID0gcmVzdWx0LmRhdGEuZG9jc09wdGlvbmFsQXJyYXk7XFxuXFxuXFx0XFx0XFx0XFx0ICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLmNob3NlbignZGVzdHJveScpO1xcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykuY2hvc2VuKHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR3aWR0aDogXFxcIjEwMCVcXFwiLFxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdG5vX3Jlc3VsdHNfdGV4dDogXFxcIk5vIHJlc3VsdC9zIGZvdW5kLlxcXCIsXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcdFxcdFxcdCAgICAgICAgXFx0fSwgMTAwMCk7XFxuXFx0XFx0XFx0XFx0XFx0fSk7XFxuXFx0XFx0XFx0fSxcXG5cXG5cXG5cXHRcXHRcXHRpbml0Q2hvc2VuU2VsZWN0KCkge1xcblxcdFxcdFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlJykuY2hvc2VuKHtcXG5cXHRcXHRcXHRcXHRcXHR3aWR0aDogXFxcIjEwMCVcXFwiLFxcblxcdFxcdFxcdFxcdFxcdG5vX3Jlc3VsdHNfdGV4dDogXFxcIk5vIHJlc3VsdC9zIGZvdW5kLlxcXCIsXFxuXFx0XFx0XFx0XFx0XFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0XFx0bGV0IHZtID0gdGhpcztcXG5cXG5cXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZScpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcXG5cXHRcXHRcXHRcXHRcXHRsZXQgc2VydmljZXMgPSAkKHRoaXMpLnZhbCgpO1xcblxcblxcdFxcdFxcdFxcdFxcdHZtLnNob3dDaGVja2JveEFsbCA9IChzZXJ2aWNlcy5sZW5ndGggPiAwKSA/IHRydWUgOiBmYWxzZTtcXG5cXHRcXHRcXHRcXHRcXHRcXG5cXHRcXHRcXHRcXHRcXHR2bS5nZXREb2NzKHNlcnZpY2VzKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHR2bS5hZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcyA9IHNlcnZpY2VzO1xcblxcblxcdFxcdFxcdFxcdFxcdC8vIFNldCBjb3N0IGlucHV0XFxuXFx0XFx0XFx0XFx0XFx0aWYoc2VydmljZXMubGVuZ3RoID09IDEpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR2bS5zZXJ2aWNlc0xpc3QubWFwKHMgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdGlmKHMuaWQgPT0gc2VydmljZXNbMF0pIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR2bS5jb3N0ID0gcy5jb3N0O1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHRcXHRcXHR9KTtcXG5cXHRcXHRcXHRcXHRcXHR9IGVsc2Uge1xcblxcdFxcdFxcdFxcdFxcdFxcdHZtLmNvc3QgPSAnJztcXG5cXHRcXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRjYWxsRGF0YVRhYmxlKCkge1xcblxcdFxcdFxcdFxcdCQoJy5hZGQtbmV3LXNlcnZpY2UtZGF0YXRhYmxlJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xcblxcblxcdFxcdFxcdFxcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XFxuXFx0XFx0XFx0XFx0XFx0JCgnLmFkZC1uZXctc2VydmljZS1kYXRhdGFibGUnKS5EYXRhVGFibGUoe1xcblxcdFxcdFxcdCAgICAgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxcblxcdFxcdFxcdCAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXFxuXFx0XFx0XFx0ICAgICAgICAgICAgZG9tOiAnPFxcXCJ0b3BcXFwibGY+cnQ8XFxcImJvdHRvbVxcXCJpcD48XFxcImNsZWFyXFxcIj4nXFxuXFx0XFx0XFx0ICAgICAgICB9KTtcXG5cXHRcXHRcXHRcXHR9LCAxMDAwKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGNoZWNrQWxsKGUpIHtcXG5cXHRcXHRcXHRcXHRsZXQgY2hlY2tib3ggPSBlLnRhcmdldC5jbGFzc05hbWU7XFxuXFxuXFx0XFx0XFx0XFx0aWYoY2hlY2tib3ggPT0gJ2NoZWNrYm94LWFsbCcpIHtcXG5cXHRcXHRcXHRcXHRcXHRpZigkKCcuJyArIGNoZWNrYm94KS5pcygnOmNoZWNrZWQnKSlcXG5cXHRcXHRcXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIHRydWUpO1xcblxcdFxcdFxcdFxcdFxcdGVsc2VcXG5cXHRcXHRcXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIGZhbHNlKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb24nKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0bGV0IGNob3NlbiA9IGUudGFyZ2V0LmRhdGFzZXQuY2hvc2VuQ2xhc3M7XFxuXFxuXFx0XFx0XFx0XFx0XFx0aWYoJCgnLicgKyBjaGVja2JveCkuaXMoJzpjaGVja2VkJykpXFxuXFx0XFx0XFx0XFx0XFx0XFx0JCgnLicgKyBjaG9zZW4gKyAnIG9wdGlvbicpLnByb3AoJ3NlbGVjdGVkJywgdHJ1ZSk7XFxuXFx0XFx0XFx0XFx0XFx0ZWxzZVxcblxcdFxcdFxcdFxcdFxcdFxcdCQoJy4nICsgY2hvc2VuICsgJyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIGZhbHNlKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHQkKCcuJyArIGNob3NlbiArICcgb3B0aW9uJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcXG5cXHRcXHRcXHRcXHR9XFxuIFxcdFxcdFxcdH1cXG4gICAgICAgIH0sXFxuXFxuICAgICAgICBjcmVhdGVkKCkge1xcbiAgICAgICAgXFx0dGhpcy5mZXRjaFNlcnZpY2VMaXN0KCk7XFxuICAgICAgICB9LFxcblxcbiAgICAgICAgbW91bnRlZCgpIHtcXG4gICAgICAgIFxcdHRoaXMuaW5pdENob3NlblNlbGVjdCgpO1xcbiAgICAgICAgfVxcbiAgICB9XFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcblxcdC5jaGVja2JveC1pbmxpbmUge1xcblxcdFxcdG1hcmdpbi10b3A6IC03cHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1iMmMxNmZmOFwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL0FkZFNlcnZpY2UudnVlXG4vLyBtb2R1bGUgaWQgPSAzMTBcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSgpO1xuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuZm9ybVtkYXRhLXYtZTRjNWNlNmFdIHtcXG5cXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcbn1cXG4ubS1yLTEwW2RhdGEtdi1lNGM1Y2U2YV0ge1xcblxcdG1hcmdpbi1yaWdodDogMTBweDtcXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIkVkaXRTZXJ2aWNlLnZ1ZT8yZmM3Mjc3MVwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBMlRBO0NBQ0Esb0JBQUE7Q0FDQTtBQUNBO0NBQ0EsbUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiRWRpdFNlcnZpY2UudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIiBAc3VibWl0LnByZXZlbnQ9XFxcImVkaXRDbGllbnRTZXJ2aWNlXFxcIj5cXG5cXG5cXHRcXHQ8ZGl2PlxcblxcblxcblxcdFxcdCAgICBcXHQ8ZGl2IGNsYXNzcz1cXFwiY29sLW1kLTEyXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cCBjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTQgY29udHJvbC1sYWJlbCBcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIFRpcDpcXG5cXHRcXHRcXHRcXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtOFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHQ8aW5wdXQgdHlwZT1cXFwibnVtYmVyXFxcIiBtaW49XFxcIjBcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcInRpcFxcXCIgIHYtbW9kZWw9XFxcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi50aXBcXFwiIHN0eWxlPVxcXCJ3aWR0aCA6IDE3NXB4O1xcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXAgIGNvbC1tZC02IG0tbC0yXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgU3RhdHVzOlxcblxcdFxcdFxcdFxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC04XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdDxzZWxlY3QgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwic3RhdHVzXFxcIiBpZD1cXFwic3RhdHVzXFxcIiBzdHlsZT1cXFwid2lkdGggOiAxNzVweDtcXFwiIHYtbW9kZWw9XFxcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5zdGF0dXNcXFwiPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVxcXCJjb21wbGV0ZVxcXCI+Q29tcGxldGU8L29wdGlvbj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cXFwib24gcHJvY2Vzc1xcXCI+T24gUHJvY2Vzczwvb3B0aW9uPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVxcXCJwZW5kaW5nXFxcIj5QZW5kaW5nPC9vcHRpb24+XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XFxuXFx0XFx0XFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0ICAgIFxcdDxkaXYgY2xhc3NzPVxcXCJjb2wtbWQtMTJcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwIGNvbC1tZC02XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgQ29zdDpcXG5cXHRcXHRcXHRcXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtOFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHQ8aW5wdXQgdHlwZT1cXFwibnVtYmVyXFxcIiBtaW49XFxcIjBcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImNvc3RcXFwiIHYtbW9kZWw9XFxcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5jb3N0XFxcIiBzdHlsZT1cXFwid2lkdGggOiAxNzVweDtcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcblxcdFxcdFxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwICBjb2wtbWQtNiBtLWwtMlxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTQgY29udHJvbC1sYWJlbCBcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIERpc2NvdW50OlxcblxcdFxcdFxcdFxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC04XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJudW1iZXJcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImRpc2NvdW50XFxcIiB2LW1vZGVsPVxcXCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZGlzY291bnRcXFwiIG1pbj1cXFwiMFxcXCIgc3R5bGU9XFxcIndpZHRoIDogMTc1cHg7XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDwvZGl2PlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgdi1pZj1cXFwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmRpc2NvdW50ID4gMFxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCIgPlxcblxcdFxcdFxcdCAgICAgICAgICAgIFJlYXNvbjpcXG5cXHRcXHRcXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgXFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcInJlYXNvblxcXCIgdi1tb2RlbD1cXFwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlYXNvblxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFxuXFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdCAgICBcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgIE5vdGU6XFxuXFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHQgICAgICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJub3RlXFxcIiB2LW1vZGVsPVxcXCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYubm90ZVxcXCIgcGxhY2Vob2xkZXI9XFxcImZvciBpbnRlcm5hbCB1c2Ugb25seSwgb25seSBlbXBsb3llZXMgY2FuIHNlZS5cXFwiPlxcblxcdFxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcblxcdFxcdFxcdDxkaXYgY2xhc3NzPVxcXCJjb2wtbWQtMTJcXFwiPlxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXAgY29sLW1kLTZcXFwiPlxcblxcdFxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICBTdGF0dXM6XFxuXFx0XFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgIFxcdDxkaXYgc3R5bGU9XFxcImhlaWdodDo1cHg7Y2xlYXI6Ym90aDtcXFwiPjwvZGl2PlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgPGxhYmVsIGNsYXNzPVxcXCJcXFwiPiA8aW5wdXQgdHlwZT1cXFwicmFkaW9cXFwiIHZhbHVlPVxcXCIwXFxcIiBuYW1lPVxcXCJhY3RpdmVcXFwiIHYtbW9kZWw9XFxcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5hY3RpdmVcXFwiPiA8aT48L2k+IERpc2FibGVkIDwvbGFiZWw+XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XFxcIm0tbC0yXFxcIj4gPGlucHV0IHR5cGU9XFxcInJhZGlvXFxcIiB2YWx1ZT1cXFwiMVxcXCIgbmFtZT1cXFwiYWN0aXZlXFxcIiB2LW1vZGVsPVxcXCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuYWN0aXZlXFxcIj4gPGk+PC9pPiBFbmFibGVkIDwvbGFiZWw+XFxuXFx0XFx0ICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cCBjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBFeHRlbmQgVG86XFxuXFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC02XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgXFx0PGZvcm0gaWQ9XFxcImRhaWx5LWZvcm1cXFwiIGNsYXNzPVxcXCJmb3JtLWlubGluZVxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImlucHV0LWdyb3VwIGRhdGVcXFwiPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcImlucHV0LWdyb3VwLWFkZG9uXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cXFwiZmEgZmEtY2FsZW5kYXJcXFwiPjwvaT5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBpZD1cXFwiZGF0ZVxcXCIgdi1tb2RlbD1cXFwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmV4dGVuZFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbCBkYXRlcGlja2VyXFxcIiBhdXRvY29tcGxldGU9XFxcIm9mZlxcXCIgbmFtZT1cXFwiZGF0ZVxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcdCAgICAgICAgICAgICAgICAgICAgPC9mb3JtPlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgXFx0UmVxdWlyZWQgRG9jdW1lbnRzOiA8c3BhbiBjbGFzcz1cXFwidGV4dC1kYW5nZXJcXFwiPio8L3NwYW4+XFxuXFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFxuXFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVxcXCJTZWxlY3QgRG9jc1xcXCIgY2xhc3M9XFxcImNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3NcXFwiIG11bHRpcGxlIHN0eWxlPVxcXCJ3aWR0aDozNTBweDtcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cXFwiZG9jIGluIHJlcXVpcmVkRG9jc1xcXCIgOnZhbHVlPVxcXCJkb2MuaWRcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHR7eyAoZG9jLnRpdGxlKS50cmltKCkgfX1cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgPC9vcHRpb24+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgPC9zZWxlY3Q+XFxuXFx0XFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICBcXHRPcHRpb25hbCBEb2N1bWVudHM6XFxuXFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFxuXFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVxcXCJTZWxlY3QgRG9jc1xcXCIgY2xhc3M9XFxcImNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3NcXFwiIG11bHRpcGxlIHN0eWxlPVxcXCJ3aWR0aDozNTBweDtcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cXFwiZG9jIGluIG9wdGlvbmFsRG9jc1xcXCIgOnZhbHVlPVxcXCJkb2MuaWRcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHR7eyAoZG9jLnRpdGxlKS50cmltKCkgfX1cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgPC9vcHRpb24+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgPC9zZWxlY3Q+XFxuXFx0XFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdDwvZGl2PlxcblxcbiA8IS0tXFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiY29sLW1kLTEyXFxcIj5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIFJlcG9ydGluZzpcXG5cXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJyZXBvcnRpbmdcXFwiIHYtbW9kZWw9XFxcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5yZXBvcnRpbmdcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHQ8L2Rpdj4gLS0+XFxuICAgIFxcdDwvZGl2PlxcbiAgICBcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTJcXFwiPlxcblxcdFxcdFxcdDxhIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9hPlxcblxcdFxcdFxcdDxidXR0b24gdHlwZT1cXFwic3VibWl0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcXFwiPlNhdmU8L2J1dHRvbj5cXG5cXHQgICAgPC9kaXY+XFxuICAgIDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuICAgIGV4cG9ydCBkZWZhdWx0e1xcbiAgICBcXHRwcm9wczogWyd0cmFja2luZycsJ2NsaWVudF9pZCddLFxcblxcbiAgICAgICAgZGF0YSgpIHtcXG4gICAgICAgIFxcdHJldHVybiB7XFxuXFxuICAgICAgICBcXHRcXHRzZXJ2aWNlc0xpc3Q6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0Y29zdDogJycsXFxuXFxuICAgICAgICBcXHRcXHRjbGllbnRzOiBbXSxcXG5cXG4gICAgICAgIFxcdFxcdHJlcXVpcmVkRG9jczogW10sXFxuXFxuICAgICAgICBcXHRcXHRvcHRpb25hbERvY3M6IFtdXFxuICAgICAgICBcXHRcXHRcXHRcXHRcXG4gICAgICAgIFxcdH1cXG4gICAgICAgIH0sXFxuXFx0XFx0Y29tcHV0ZWQ6IHtcXG5cXG5cXHRcXHR9LFxcbiAgICAgICAgbWV0aG9kczoge1xcblxcbiAgICAgICAgXFx0cmVzZXRFZGl0U2VydmljZUZvcm0oKSB7XFxuICAgICAgICBcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5jb3N0ID0gMDtcXG4gICAgICAgIFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnRpcCA9IDA7XFxuICAgICAgICBcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5zdGF0dXMgPSBcXFwiXFxcIjtcXG4gICAgICAgIFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmFjdGl2ZSA9IDA7XFxuICAgICAgICBcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5kaXNjb3VudCA9IDA7XFxuICAgICAgICBcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5yZWFzb24gPSBcXFwiXFxcIjtcXG4gICAgICAgIFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2Lm5vdGUgPSBcXFwiXFxcIjtcXG4gICAgICAgIFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmlkID0gbnVsbDtcXG4gICAgICAgIFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlcG9ydGluZyA9ICcnO1xcbiAgICAgICAgXFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZXh0ZW5kID0gXFxcIlxcXCI7XFxuICAgICAgICBcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5yY3ZfZG9jcyA9IFxcXCJcXFwiO1xcbiAgICAgICAgXFx0fSxcXG5cXG4gICAgICAgIFxcdGVkaXRDbGllbnRTZXJ2aWNlKCkge1xcblxcbiAgICAgICAgXFx0XFx0dmFyIHJlcXVpcmVkRG9jcyA9ICcnO1xcbiAgICAgICAgXFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3Mgb3B0aW9uOnNlbGVjdGVkJykuZWFjaCggZnVuY3Rpb24oZSkge1xcblxcdFxcdFxcdCAgICAgICAgcmVxdWlyZWREb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XFxuXFx0XFx0XFx0ICAgIH0pO1xcblxcbiAgICAgICAgXFx0XFx0dmFyIG9wdGlvbmFsRG9jcyA9ICcnO1xcbiAgICAgICAgXFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3Mgb3B0aW9uOnNlbGVjdGVkJykuZWFjaCggZnVuY3Rpb24oZSkge1xcblxcdFxcdFxcdCAgICAgICAgb3B0aW9uYWxEb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XFxuXFx0XFx0XFx0ICAgIH0pO1xcblxcblxcdFxcdFxcdCAgICB2YXIgZG9jcyA9IHJlcXVpcmVkRG9jcyArIG9wdGlvbmFsRG9jcztcXG5cXHRcXHRcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5yY3ZfZG9jcyA9IGRvY3Muc2xpY2UoMCwgLTEpO1xcblxcbiAgICAgICAgXFx0XFx0aWYodGhpcy4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZGlzY291bnQgPiAwICYmIHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlYXNvbiA9PSBudWxsKXtcXG5cXHRcXHRcXHQgICAgICAgIHRvYXN0ci5lcnJvcignUGxlYXNlIGlucHV0IHJlYXNvbi4nKTtcXG4gICAgICAgIFxcdFxcdH1cXG4gICAgICAgIFxcdFxcdGVsc2UgaWYocmVxdWlyZWREb2NzID09ICcnICYmIHRoaXMucmVxdWlyZWREb2NzLmxlbmd0aCE9MCl7XFxuICAgICAgICBcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgUmVxdWlyZWQgRG9jdW1lbnRzLicpO1xcbiAgICAgICAgXFx0XFx0fVxcbiAgICAgIFxcdFxcdFxcdGVsc2V7XFxuXFx0ICAgICAgICBcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5zdWJtaXQoJ3Bvc3QnLCcvY2xpZW50RWRpdFNlcnZpY2UnKVxcblxcdCAgICAgICAgICAgIFxcdC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHQgICAgICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2Vzcykge1xcblxcdFxcdFxcdCAgICAgXFx0XFx0XFx0dGhpcy5yZXNldEVkaXRTZXJ2aWNlRm9ybSgpO1xcblxcdFxcdFxcdCAgICAgXFx0XFx0XFx0dmFyIGNsaWVudF9pZCA9IHRoaXMuJHBhcmVudC4kcGFyZW50LnVzcl9pZDtcXG5cXHRcXHRcXHQgICAgIFxcdFxcdFxcdHZhciB0cmFjayA9IHRoaXMuJHBhcmVudC4kcGFyZW50LnRyYWNraW5nX3NlbGVjdGVkO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LnNob3dTZXJ2aWNlc1VuZGVyKGNsaWVudF9pZCx0cmFjayk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQudXBkYXRlVHJhY2tpbmdMaXN0KGNsaWVudF9pZCk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQucmVsb2FkQ29tcHV0YXRpb24oKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHR2YXIgb3dsID0gJChcXFwiI293bC1kZW1vXFxcIik7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0b3dsLmRhdGEoJ293bENhcm91c2VsJykuZGVzdHJveSgpO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdCQoXFxcIi5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jc1xcXCIpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgJCgnI2VkaXQtc2VydmljZS1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0dG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBVcGRhdGVkIScpO1xcblxcdFxcdFxcdCAgICAgICAgICAgICAgICB9XFxuXFx0XFx0ICAgICAgICAgICAgfSlcXG5cXHRcXHQgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT57XFxuXFx0XFx0ICAgICAgICAgICAgICAgIGZvcih2YXIga2V5IGluIGVycm9yKSB7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0aWYoZXJyb3IuaGFzT3duUHJvcGVydHkoa2V5KSkgdG9hc3RyLmVycm9yKGVycm9yW2tleV1bMF0pXFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0fVxcblxcdFxcdCAgICAgICAgICAgIH0pO1xcdFxcbiAgICAgIFxcdFxcdFxcdH1cXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGZldGNoRG9jcyhpZCl7XFxuXFxuXFx0XFx0XFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykudmFsKCcnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcblxcdFxcdFxcdFxcdGlmKHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJjdl9kb2NzID09ICcnKVxcblxcdFxcdFxcdFxcdFxcdHZhciByY3YgPSAoIGlkWzBdLnJjdiE9bnVsbCA/IGlkWzBdLnJjdi5zcGxpdChcXFwiLFxcXCIpIDogJycpO1xcblxcdFxcdFxcdFxcdGVsc2VcXG5cXHRcXHRcXHRcXHRcXHR2YXIgcmN2ID0gdGhpcy4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYucmN2X2RvY3Muc3BsaXQoXFxcIixcXFwiKTtcXG5cXG5cXHRcXHRcXHQgXFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC8nK2lkWzBdLmRvY3NfbmVlZGVkKycvc2VydmljZS1kb2NzJylcXG5cXHRcXHRcXHQgXFx0ICAudGhlbihyZXN1bHQgPT4ge1xcblxcdFxcdCAgICAgICAgICAgIHRoaXMucmVxdWlyZWREb2NzID0gcmVzdWx0LmRhdGE7XFxuXFx0XFx0ICAgICAgICB9KTtcXG5cXHQgICAgICAgIFxcdFxcblxcdFxcdFxcdCBcXHRheGlvcy5nZXQoJy92aXNhL2dyb3VwLycraWRbMF0uZG9jc19vcHRpb25hbCsnL3NlcnZpY2UtZG9jcycpXFxuXFx0XFx0XFx0IFxcdCAgLnRoZW4ocmVzdWx0ID0+IHtcXG5cXHRcXHQgICAgICAgICAgICB0aGlzLm9wdGlvbmFsRG9jcyA9IHJlc3VsdC5kYXRhO1xcblxcdFxcdCAgICAgICAgfSk7XFxuXFxuXFx0XFx0ICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcXG5cXHQgICAgICAgIFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLmNob3NlbignZGVzdHJveScpO1xcblxcdCAgICAgICAgXFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykuY2hvc2VuKHtcXG5cXHRcXHRcXHRcXHRcXHR3aWR0aDogXFxcIjEwMCVcXFwiLFxcblxcdFxcdFxcdFxcdFxcdG5vX3Jlc3VsdHNfdGV4dDogXFxcIk5vIHJlc3VsdC9zIGZvdW5kLlxcXCIsXFxuXFx0XFx0XFx0XFx0XFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0ICAgICAgICBcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS52YWwocmN2KS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcbiAgICAgICAgXFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcXG4gICAgICAgIFxcdH0sIDIwMDApO1xcblxcblxcdFxcdFxcdH0sXFxuXFxuXFxuXFx0XFx0XFx0Y2FsbERhdGFUYWJsZSgpIHtcXG5cXHRcXHRcXHRcXHQkKCcuYWRkLW5ldy1zZXJ2aWNlLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcXG5cXG5cXHRcXHRcXHRcXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xcblxcdFxcdFxcdFxcdFxcdCQoJy5hZGQtbmV3LXNlcnZpY2UtZGF0YXRhYmxlJykuRGF0YVRhYmxlKHtcXG5cXHRcXHRcXHQgICAgICAgICAgICBwYWdlTGVuZ3RoOiAxMCxcXG5cXHRcXHRcXHQgICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxcblxcdFxcdFxcdCAgICAgICAgICAgIGRvbTogJzxcXFwidG9wXFxcImxmPnJ0PFxcXCJib3R0b21cXFwiaXA+PFxcXCJjbGVhclxcXCI+J1xcblxcdFxcdFxcdCAgICAgICAgfSk7XFxuXFx0XFx0XFx0XFx0fSwgMTAwMCk7XFxuXFx0XFx0XFx0fSxcXG4gICAgICAgIH0sXFxuXFxuICAgICAgICBjcmVhdGVkKCkge1xcblxcbiAgICAgICAgfSxcXG5cXG4gICAgICAgIG1vdW50ZWQoKSB7XFxuICAgICAgICBcXHQvLyBEYXRlUGlja2VyXFxuXFx0ICAgICAgICBcXHQkKCcuZGF0ZXBpY2tlcicpLmRhdGVwaWNrZXIoe1xcblxcdCAgICAgICAgICAgICAgICB0b2RheUJ0bjogXFxcImxpbmtlZFxcXCIsXFxuXFx0ICAgICAgICAgICAgICAgIGtleWJvYXJkTmF2aWdhdGlvbjogZmFsc2UsXFxuXFx0ICAgICAgICAgICAgICAgIGZvcmNlUGFyc2U6IGZhbHNlLFxcblxcdCAgICAgICAgICAgICAgICBjYWxlbmRhcldlZWtzOiB0cnVlLFxcblxcdCAgICAgICAgICAgICAgICBhdXRvY2xvc2U6IHRydWUsXFxuXFx0ICAgICAgICAgICAgICAgIGZvcm1hdDogXFxcInl5eXktbW0tZGRcXFwiLFxcblxcdCAgICAgICAgICAgIH0pXFxuXFx0ICAgICAgICAgICAgLm9uKCdjaGFuZ2VEYXRlJywgZnVuY3Rpb24oZSkgeyAvLyBCb290c3RyYXAgRGF0ZXBpY2tlciBvbkNoYW5nZVxcblxcdCAgICAgICAgICAgIFxcdGxldCBpZCA9IGUudGFyZ2V0LmlkO1xcblxcdFxcdFxcdCAgICAgICAgbGV0IGRhdGUgPSBlLnRhcmdldC52YWx1ZTtcXG5cXG5cXHRcXHRcXHQgICAgICAgIGNvbnNvbGUubG9nKCdJRCA6ICcgKyBpZCk7XFxuXFx0XFx0XFx0ICAgICAgICBjb25zb2xlLmxvZygnREFURSA6ICcgKyBkYXRlKTtcXG5cXHRcXHRcXHQgICAgICAgIHRoaXMuJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmV4dGVuZCA9IGRhdGU7XFxuXFx0XFx0XFx0ICAgIH0uYmluZCh0aGlzKSlcXG5cXHRcXHRcXHQgICAgLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbihlKSB7IC8vIG5hdGl2ZSBvbkNoYW5nZVxcblxcdFxcdFxcdCAgICBcXHRsZXQgaWQgPSBlLnRhcmdldC5pZDtcXG5cXHRcXHRcXHQgICAgXFx0bGV0IGRhdGUgPSBlLnRhcmdldC52YWx1ZTtcXG5cXG5cXHRcXHRcXHQgICAgXFx0Y29uc29sZS5sb2coJ0lEIDogJyArIGlkKTtcXG5cXHRcXHRcXHQgICAgICAgIGNvbnNvbGUubG9nKCdEQVRFIDogJyArIGRhdGUpO1xcblxcdFxcdFxcdCAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZXh0ZW5kID0gZGF0ZTtcXG5cXHRcXHRcXHQgICAgfS5iaW5kKHRoaXMpKTtcXG4gICAgICAgIH1cXG4gICAgfVxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtZTRjNWNlNmFcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9FZGl0U2VydmljZS52dWVcbi8vIG1vZHVsZSBpZCA9IDMxNVxuLy8gbW9kdWxlIGNodW5rcyA9IDIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFsoX3ZtLmxvZy55ZWFyICE9IG51bGwpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3cgdGltZWxpbmUtbW92ZW1lbnQgdGltZWxpbmUtbW92ZW1lbnQtdG9wIG5vLWRhc2hlZCBuby1zaWRlLW1hcmdpblwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLWJhZGdlIHRpbWVsaW5lLWZpbHRlci1tb3ZlbWVudFwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgfVxuICB9LCBbX2MoJ3NwYW4nLCBbX3ZtLl92KF92bS5fcyhfdm0ubG9nLnllYXIpKV0pXSldKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93IHRpbWVsaW5lLW1vdmVtZW50ICBuby1zaWRlLW1hcmdpblwiXG4gIH0sIFsoX3ZtLmxvZy5tb250aCAhPSBudWxsKSA/IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtYmFkZ2VcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtYmFsbG9vbi1kYXRlLWRheVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sb2cuZGF5KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtYmFsbG9vbi1kYXRlLW1vbnRoXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLmxvZy5tb250aCkpXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xMiAgdGltZWxpbmUtaXRlbVwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS1vZmZzZXQtMSBjb2wtc20tMTFcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0aW1lbGluZS1wYW5lbCBkZWJpdHNcIlxuICB9LCBbX2MoJ3VsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLXBhbmVsLXVsXCJcbiAgfSwgW19jKCdsaScsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbXBvcnRvXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiaW5uZXJIVE1MXCI6IF92bS5fcyhfdm0ubG9nLmRhdGEudGl0bGUpXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2F1c2FsZVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sb2cuZGF0YS5wcm9jZXNzb3IpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIFtfYygncCcsIFtfYygnc21hbGwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1tdXRlZFwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJnbHlwaGljb24gZ2x5cGhpY29uLXRpbWVcIlxuICB9KSwgX3ZtLl92KF92bS5fcyhfdm0ubG9nLmRhdGEuZGF0ZSkpXSldKV0pXSldKV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNzJiNzU1YjVcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi03MmI3NTViNVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL0FjdGlvbkxvZ3MudnVlXG4vLyBtb2R1bGUgaWQgPSAzMlxuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL01vZGFsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZDAxMzM3NmVcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vTW9kYWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL01vZGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIE1vZGFsLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1kMDEzMzc2ZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWQwMTMzNzZlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSAzMzJcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNzIyYWUzNWFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9DbGllbnREb2N1bWVudHMudnVlXCIpXG5cbnZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NsaWVudERvY3VtZW50cy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTcyMmFlMzVhXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0NsaWVudERvY3VtZW50cy52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtNzIyYWUzNWFcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9DbGllbnREb2N1bWVudHMudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQ2xpZW50RG9jdW1lbnRzLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi03MjJhZTM1YVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTcyMmFlMzVhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9DbGllbnREb2N1bWVudHMudnVlXG4vLyBtb2R1bGUgaWQgPSAzMzlcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtYjJjMTZmZjhcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGRTZXJ2aWNlLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9BZGRTZXJ2aWNlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtYjJjMTZmZjhcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQWRkU2VydmljZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtYjJjMTZmZjhcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9BZGRTZXJ2aWNlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEFkZFNlcnZpY2UudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWIyYzE2ZmY4XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtYjJjMTZmZjhcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL0FkZFNlcnZpY2UudnVlXG4vLyBtb2R1bGUgaWQgPSAzNjFcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZTRjNWNlNmFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9FZGl0U2VydmljZS52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRWRpdFNlcnZpY2UudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1lNGM1Y2U2YVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9FZGl0U2VydmljZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtZTRjNWNlNmFcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9FZGl0U2VydmljZS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBFZGl0U2VydmljZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtZTRjNWNlNmFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi1lNGM1Y2U2YVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvRWRpdFNlcnZpY2UudnVlXG4vLyBtb2R1bGUgaWQgPSAzNjJcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vUGFja2FnZS52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTdkZGFkYTQ2XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1BhY2thZ2UudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvUGFja2FnZS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBQYWNrYWdlLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi03ZGRhZGE0NlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTdkZGFkYTQ2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9QYWNrYWdlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzYzXG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1NlcnZpY2UudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi01Njg2YzU5NlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9TZXJ2aWNlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL1NlcnZpY2UudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gU2VydmljZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNTY4NmM1OTZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi01Njg2YzU5NlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvU2VydmljZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM2NFxuLy8gbW9kdWxlIGNodW5rcyA9IDIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3RyJywgW19jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdHlsZTogKF92bS5pc0Rpc2FibGVkKVxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjb21tb25EYXRlXCIpKF92bS5zZXJ2LnNlcnZpY2VfZGF0ZSkpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiXG4gIH0sIFsoX3ZtLnNlcnYucmVtYXJrcykgPyBfYygnc3BhbicsIHtcbiAgICBzdHlsZTogKF92bS5pc0Rpc2FibGVkKVxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIiNcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJwb3BvdmVyXCIsXG4gICAgICBcImRhdGEtcGxhY2VtZW50XCI6IFwicmlnaHRcIixcbiAgICAgIFwiZGF0YS1jb250YWluZXJcIjogXCJib2R5XCIsXG4gICAgICBcImRhdGEtY29udGVudFwiOiBfdm0uc2Vydi5yZW1hcmtzLFxuICAgICAgXCJkYXRhLXRyaWdnZXJcIjogXCJob3ZlclwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5zZXJ2LmRldGFpbCkgKyBcIlxcbiAgICAgICAgXCIpXSldKSA6IF9jKCdzcGFuJywge1xuICAgIHN0eWxlOiAoX3ZtLmlzRGlzYWJsZWQpXG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLnNlcnYuZGV0YWlsKSArIFwiXFxuICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0eWxlOiAoX3ZtLmlzRGlzYWJsZWQpXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKF92bS5zZXJ2LmNvc3QpKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1jZW50ZXJcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3R5bGU6IChfdm0uaXNEaXNhYmxlZClcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikoX3ZtLnNlcnYuY2hhcmdlKSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0eWxlOiAoX3ZtLmlzRGlzYWJsZWQpXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKF92bS5zZXJ2LnRpcCkpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiXG4gIH0sIFsoX3ZtLnNlcnYucmVhc29uKSA/IF9jKCdzcGFuJywge1xuICAgIHN0eWxlOiAoX3ZtLmlzRGlzYWJsZWQpXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiI1wiLFxuICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInBvcG92ZXJcIixcbiAgICAgIFwiZGF0YS1wbGFjZW1lbnRcIjogXCJyaWdodFwiLFxuICAgICAgXCJkYXRhLWNvbnRhaW5lclwiOiBcImJvZHlcIixcbiAgICAgIFwiZGF0YS1jb250ZW50XCI6IF92bS5zZXJ2LnJlYXNvbixcbiAgICAgIFwiZGF0YS10cmlnZ2VyXCI6IFwiaG92ZXJcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICBcIiArIF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShfdm0uc2Vydi5kaXNjb3VudF9hbW91bnQpKSArIFwiXFxuICAgICAgICBcIildKV0pIDogX2MoJ3NwYW4nLCB7XG4gICAgc3R5bGU6IChfdm0uaXNEaXNhYmxlZClcbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICBcIiArIF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShfdm0uc2Vydi5kaXNjb3VudF9hbW91bnQpKSArIFwiXFxuICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyIHVjZmlyc3RcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3R5bGU6IChfdm0uaXNEaXNhYmxlZClcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLnNlcnYuc3RhdHVzKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1jZW50ZXJcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3R5bGU6IChfdm0uaXNEaXNhYmxlZClcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLnNlcnYudHJhY2tpbmcpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiXG4gIH0sIFsoX3ZtLnNlcnYuZ3JvdXBfaWQgPiAwKSA/IF9jKCdzcGFuJywge1xuICAgIHN0eWxlOiAoX3ZtLmlzRGlzYWJsZWQpXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiL3Zpc2EvZ3JvdXAvXCIgKyBfdm0uc2Vydi5ncm91cF9pZCxcbiAgICAgIFwidGFyZ2V0XCI6IFwiX2JsYW5rXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5zZXJ2Lm5hbWUpKV0pXSkgOiBfdm0uX2UoKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1jZW50ZXJcIlxuICB9LCBbX2MoJ3NwYW4nLCBbX2MoJ2EnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLiRwYXJlbnQuc2VscGVybWlzc2lvbnMuZWRpdFNlcnZpY2UgPT0gMSB8fCBfdm0uJHBhcmVudC5zZXR0aW5nID09IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCIkcGFyZW50LnNlbHBlcm1pc3Npb25zLmVkaXRTZXJ2aWNlPT0xIHx8ICRwYXJlbnQuc2V0dGluZyA9PSAxXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwicG9wb3ZlclwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiLFxuICAgICAgXCJkYXRhLWNvbnRhaW5lclwiOiBcImJvZHlcIixcbiAgICAgIFwiZGF0YS1jb250ZW50XCI6IFwiRWRpdCBTZXJ2aWNlXCIsXG4gICAgICBcImRhdGEtdHJpZ2dlclwiOiBcImhvdmVyXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZWRpdFNlcnZpY2UoX3ZtLnNlcnYpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtcGVuY2lsLXNxdWFyZS1vXCJcbiAgfSldKSwgX3ZtLl92KFwiXFxuICAgICAgICDCoFxcbiAgICAgICAgXCIpLCBfYygnYScsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uJHBhcmVudC5zZWxwZXJtaXNzaW9ucy5hZGRRdWlja1JlcG9ydCA9PSAxIHx8IF92bS4kcGFyZW50LnNldHRpbmcgPT0gMSksXG4gICAgICBleHByZXNzaW9uOiBcIiRwYXJlbnQuc2VscGVybWlzc2lvbnMuYWRkUXVpY2tSZXBvcnQ9PTEgfHwgJHBhcmVudC5zZXR0aW5nID09IDFcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJwb3BvdmVyXCIsXG4gICAgICBcImRhdGEtcGxhY2VtZW50XCI6IFwidG9wXCIsXG4gICAgICBcImRhdGEtY29udGFpbmVyXCI6IFwiYm9keVwiLFxuICAgICAgXCJkYXRhLWNvbnRlbnRcIjogXCJBZGQgUXVpY2sgUmVwb3J0XCIsXG4gICAgICBcImRhdGEtdHJpZ2dlclwiOiBcImhvdmVyXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0ud3JpdGVSZXBvcnQoX3ZtLnNlcnYpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtbGlzdC1hbHRcIlxuICB9KV0pLCBfdm0uX3YoXCJcXG4gICAgICAgIMKgXFxuICAgICAgICBcIiksIF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhpZGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwicG9wb3ZlclwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiLFxuICAgICAgXCJkYXRhLWNvbnRhaW5lclwiOiBcImJvZHlcIixcbiAgICAgIFwiZGF0YS1jb250ZW50XCI6IFwiQWRkIERvY3VtZW50c1wiLFxuICAgICAgXCJkYXRhLXRyaWdnZXJcIjogXCJob3ZlclwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmFkZERvY3MoX3ZtLnNlcnYpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtZmlsZVwiXG4gIH0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNTY4NmM1OTZcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi01Njg2YzU5NlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL1NlcnZpY2UudnVlXG4vLyBtb2R1bGUgaWQgPSAzOTZcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdmb3JtJywge1xuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0uYWRkRG9jdW1lbnRzKCRldmVudClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX2woKF92bS5pbWFnZXMpLCBmdW5jdGlvbihpbWFnZSwgaW5kZXgpIHtcbiAgICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC00XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsIHBhbmVsLWRlZmF1bHQgZm9ybS1zZWN0aW9uXCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIlxuICAgIH0sIFtfYygnY2VudGVyJywgW19jKCdpbWcnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZSBpbWctdGh1bWJuYWlsIGRvY3VtZW50LWltYWdlIHpvb21cIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwic3JjXCI6IGltYWdlLFxuICAgICAgICBcImRhdGEtbWFnbmlmeS1zcmNcIjogaW1hZ2VcbiAgICAgIH1cbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJoZWlnaHRcIjogXCIxMHB4XCIsXG4gICAgICAgIFwiY2xlYXJcIjogXCJib3RoXCJcbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3AnLCBbX3ZtLl92KFwiRG9jdW1lbnQgVHlwZTpcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICAgIH0sIFtfYygnc2VsZWN0Jywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgdmFsdWU6IChfdm0uZG9jdW1lbnRUeXBlc1tpbmRleF0pLFxuICAgICAgICBleHByZXNzaW9uOiBcImRvY3VtZW50VHlwZXNbaW5kZXhdXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICBvbjoge1xuICAgICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICB2YXIgJCRzZWxlY3RlZFZhbCA9IEFycmF5LnByb3RvdHlwZS5maWx0ZXIuY2FsbCgkZXZlbnQudGFyZ2V0Lm9wdGlvbnMsIGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICAgIHJldHVybiBvLnNlbGVjdGVkXG4gICAgICAgICAgfSkubWFwKGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICAgIHZhciB2YWwgPSBcIl92YWx1ZVwiIGluIG8gPyBvLl92YWx1ZSA6IG8udmFsdWU7XG4gICAgICAgICAgICByZXR1cm4gdmFsXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgX3ZtLiRzZXQoX3ZtLmRvY3VtZW50VHlwZXMsIGluZGV4LCAkZXZlbnQudGFyZ2V0Lm11bHRpcGxlID8gJCRzZWxlY3RlZFZhbCA6ICQkc2VsZWN0ZWRWYWxbMF0pXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBfdm0uX2woKF92bS5kZWZhdWx0RG9jdW1lbnRUeXBlcyksIGZ1bmN0aW9uKGRvY3VtZW50VHlwZSkge1xuICAgICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgICAgXCJ2YWx1ZVwiOiBkb2N1bWVudFR5cGUuaWRcbiAgICAgICAgfVxuICAgICAgfSwgW192bS5fdihcIlxcblxcdCAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHRcIiArIF92bS5fcyhkb2N1bWVudFR5cGUubmFtZSkgKyBcIlxcblxcdCAgICAgICAgICAgICAgICBcXHRcXHRcXHRcIildKVxuICAgIH0pLCAwKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygncCcsIFtfdm0uX3YoXCJJc3N1ZWQgQXQ6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICAgIH0sIFtfdm0uX20oMCwgdHJ1ZSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgIHZhbHVlOiAoX3ZtLmlzc3VlZEF0W2luZGV4XSksXG4gICAgICAgIGV4cHJlc3Npb246IFwiaXNzdWVkQXRbaW5kZXhdXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJkYXRlXCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IChfdm0uaXNzdWVkQXRbaW5kZXhdKVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICAgIF92bS4kc2V0KF92bS5pc3N1ZWRBdCwgaW5kZXgsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdwJywgW192bS5fdihcIkV4cGlyZWQgQXQ6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICAgIH0sIFtfdm0uX20oMSwgdHJ1ZSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgIHZhbHVlOiAoX3ZtLmV4cGlyZWRBdFtpbmRleF0pLFxuICAgICAgICBleHByZXNzaW9uOiBcImV4cGlyZWRBdFtpbmRleF1cIlxuICAgICAgfV0sXG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidHlwZVwiOiBcImRhdGVcIlxuICAgICAgfSxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogKF92bS5leHBpcmVkQXRbaW5kZXhdKVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICAgIF92bS4kc2V0KF92bS5leHBpcmVkQXQsIGluZGV4LCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gICAgfSwgW19jKCdidXR0b24nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRhbmdlciBidG4teHMgcHVsbC1yaWdodFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS5yZW1vdmUoaW5kZXgpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiUmVtb3ZlXCIpXSldKV0sIDEpXSldKVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsIHBhbmVsLWRlZmF1bHQgYWRkLWRvY3VtZW50cy1zZWN0aW9uXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiXG4gIH0sIFtfdm0uX20oMiksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZmlsZXNcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiZmlsZVwiLFxuICAgICAgXCJuYW1lXCI6IFwiaW1hZ2VzXCIsXG4gICAgICBcImlkXCI6IFwiaW1hZ2UtdXBsb2FkXCIsXG4gICAgICBcIm11bHRpcGxlXCI6IFwiXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBfdm0uaGFuZGxlRmlsZXNVcGxvYWRcbiAgICB9XG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImhlaWdodFwiOiBcIjFweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJBZGQgRG9jdW1lbnQvc1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldLCAyKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImZvclwiOiBcImltYWdlLXVwbG9hZFwiLFxuICAgICAgXCJpZFwiOiBcImltYWdlLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1wbHVzXCJcbiAgfSksIF92bS5fdihcIiBDaG9vc2UgRmlsZVwiKV0pXG59XX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNzIyYWUzNWFcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi03MjJhZTM1YVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0NsaWVudERvY3VtZW50cy52dWVcbi8vIG1vZHVsZSBpZCA9IDQwNlxuLy8gbW9kdWxlIGNodW5rcyA9IDIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpdGVtIGl0ZW0tZml4LXdpZHRoXCJcbiAgfSwgW19jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcIml0ZW0tY2xpY2thYmxlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnNob3dTZXJ2aWNlc1VuZGVyKClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGNsYXNzOiAnaW5mby1ib3gnICsgX3ZtLmJnXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImFycm93LXBhY2thZ2VcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uc3RhdHVzKSldKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl9tKDApLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImluZm8tYm94LWNvbnRlbnRcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5mby1ib3gtdGV4dFwiXG4gIH0sIFtfdm0uX3YoXCJQYWNrYWdlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImluZm8tYm94LW51bWJlclwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5wYWNrLnRyYWNraW5nKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHJvZ3Jlc3MtZGVzY3JpcHRpb25cIlxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uX2YoXCJjb21tb25EYXRlXCIpKF92bS5wYWNrLmxvZ19kYXRlKSkgKyBcIlxcbiAgICAgICAgICAgICAgICBcIildKV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5mby1ib3gtaWNvbiBuZXdib3hcIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtZHJvcGJveCBib3gtcG9zaXRpb25cIlxuICB9KV0pXG59XX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtN2RkYWRhNDZcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi03ZGRhZGE0NlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL1BhY2thZ2UudnVlXG4vLyBtb2R1bGUgaWQgPSA0MTFcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0uYWRkTmV3U2VydmljZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpb3Mtc2Nyb2xsXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnaGFzLWVycm9yJzogX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ3NlcnZpY2VzJylcbiAgICB9XG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgICAgICBTZXJ2aWNlOlxcblxcdFxcdCAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZVwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMzUwcHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1wbGFjZWhvbGRlclwiOiBcIlNlbGVjdCBTZXJ2aWNlXCIsXG4gICAgICBcIm11bHRpcGxlXCI6IFwiXCIsXG4gICAgICBcInRhYmluZGV4XCI6IFwiNFwiXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5zZXJ2aWNlc0xpc3QpLCBmdW5jdGlvbihzZXJ2aWNlKSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcImRhdGEtZG9jcy1uZWVkZWRcIjogc2VydmljZS5kb2NzX25lZWRlZCxcbiAgICAgICAgXCJkYXRhLWRvY3Mtb3B0aW9uYWxcIjogc2VydmljZS5kb2NzX29wdGlvbmFsXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBzZXJ2aWNlLmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHRcIiArIF92bS5fcygoc2VydmljZS5kZXRhaWwpLnRyaW0oKSkgKyBcIlxcblxcdFxcdCAgICAgICAgICAgICAgICBcIildKVxuICB9KSwgMCksIF92bS5fdihcIiBcIiksIChfdm0uYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnc2VydmljZXMnKSkgPyBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdzZXJ2aWNlcycpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzcGFjZXItMTBcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl9tKDApXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICAgICAgICAgIE5vdGU6XFxuXFx0XFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLm5vdGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJhZGROZXdTZXJ2aWNlRm9ybS5ub3RlXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJuYW1lXCI6IFwibm90ZVwiLFxuICAgICAgXCJwbGFjZWhvbGRlclwiOiBcImZvciBpbnRlcm5hbCB1c2Ugb25seSwgb25seSBlbXBsb3llZXMgY2FuIHNlZS5cIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5ub3RlKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLCBcIm5vdGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMubGVuZ3RoIDw9IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCJhZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcy5sZW5ndGggPD0gMVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiY2xhc3NzXCI6IFwiY29sLW1kLTEyXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXAgY29sLW1kLTZcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIENvc3Q6XFxuXFx0XFx0XFx0XFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtOFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwid2lkdGhcIjogXCIxNzVweFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJuYW1lXCI6IFwiY29zdFwiLFxuICAgICAgXCJyZWFkb25seVwiOiBcIlwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiBfdm0uY29zdFxuICAgIH1cbiAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXAgIGNvbC1tZC02IG0tbC0yXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnZGlzY291bnQnKVxuICAgIH1cbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICBEaXNjb3VudDpcXG5cXHRcXHRcXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC04XCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudCksXG4gICAgICBleHByZXNzaW9uOiBcImFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjE3NXB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJudW1iZXJcIixcbiAgICAgIFwibmFtZVwiOiBcImRpc2NvdW50XCIsXG4gICAgICBcIm1pblwiOiBcIjBcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5hZGROZXdTZXJ2aWNlRm9ybSwgXCJkaXNjb3VudFwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIChfdm0uYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnZGlzY291bnQnKSkgPyBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdkaXNjb3VudCcpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudCA+IDApID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgICAgIFJlYXNvbjpcXG5cXHRcXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkTmV3U2VydmljZUZvcm0ucmVhc29uKSxcbiAgICAgIGV4cHJlc3Npb246IFwiYWRkTmV3U2VydmljZUZvcm0ucmVhc29uXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJuYW1lXCI6IFwicmVhc29uXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uYWRkTmV3U2VydmljZUZvcm0ucmVhc29uKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLCBcInJlYXNvblwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSldKV0pIDogX3ZtLl9lKCldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2hvd0NoZWNrYm94QWxsKSxcbiAgICAgIGV4cHJlc3Npb246IFwic2hvd0NoZWNrYm94QWxsXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCIyMHB4XCIsXG4gICAgICBcImNsZWFyXCI6IFwiYm90aFwiXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNoZWNrYm94LWlubGluZVwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hlY2tib3gtYWxsXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImNoZWNrYm94XCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS5jaGVja0FsbFxuICAgIH1cbiAgfSksIF92bS5fdihcIiBDaGVjayBhbGwgcmVxdWlyZWQgZG9jdW1lbnRzXFxuXFx0ICAgIFxcdFxcdFwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMjBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF92bS5fbCgoX3ZtLmRldGFpbEFycmF5KSwgZnVuY3Rpb24oZGV0YWlsLCBpbmRleCkge1xuICAgIHJldHVybiBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicGFuZWwgcGFuZWwtZGVmYXVsdFwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1oZWFkaW5nXCJcbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGRldGFpbCkgKyBcIlxcblxcblxcdCAgICAgICAgICAgICAgICBcIiksIF9jKCdsYWJlbCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNoZWNrYm94LWlubGluZSBwdWxsLXJpZ2h0XCJcbiAgICB9LCBbX2MoJ2lucHV0Jywge1xuICAgICAgY2xhc3M6ICdjaGVja2JveC0nICsgaW5kZXgsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiLFxuICAgICAgICBcImRhdGEtY2hvc2VuLWNsYXNzXCI6ICdjaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpbmRleFxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogX3ZtLmNoZWNrQWxsXG4gICAgICB9XG4gICAgfSksIF92bS5fdihcIiBDaGVjayBhbGwgcmVxdWlyZWQgZG9jdW1lbnRzXFxuXFx0XFx0XFx0ICAgICAgICBcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgICB9LCBbX2MoJ2xhYmVsJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICBcXHRSZXF1aXJlZCBEb2N1bWVudHM6XFxuXFx0XFx0XFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgICB9LCBbX2MoJ3NlbGVjdCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3NcIixcbiAgICAgIGNsYXNzOiAnY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcy0nICsgaW5kZXgsXG4gICAgICBzdGF0aWNTdHlsZToge1xuICAgICAgICBcIndpZHRoXCI6IFwiMzUwcHhcIlxuICAgICAgfSxcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiZGF0YS1wbGFjZWhvbGRlclwiOiBcIlNlbGVjdCBEb2NzXCIsXG4gICAgICAgIFwibXVsdGlwbGVcIjogXCJcIixcbiAgICAgICAgXCJ0YWJpbmRleFwiOiBcIjRcIlxuICAgICAgfVxuICAgIH0sIF92bS5fbCgoX3ZtLnJlcXVpcmVkRG9jc1tpbmRleF0pLCBmdW5jdGlvbihkb2MpIHtcbiAgICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgIFwidmFsdWVcIjogZG9jLmlkXG4gICAgICAgIH1cbiAgICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0XCIgKyBfdm0uX3MoZG9jLnRpdGxlKSArIFwiXFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIFwiKV0pXG4gICAgfSksIDApXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICAgIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgIFxcdE9wdGlvbmFsIERvY3VtZW50czpcXG5cXHRcXHRcXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICAgIH0sIFtfYygnc2VsZWN0Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jc1wiLFxuICAgICAgY2xhc3M6ICdjaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzLScgKyBpbmRleCxcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwid2lkdGhcIjogXCIzNTBweFwiXG4gICAgICB9LFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJkYXRhLXBsYWNlaG9sZGVyXCI6IFwiU2VsZWN0IERvY3NcIixcbiAgICAgICAgXCJtdWx0aXBsZVwiOiBcIlwiLFxuICAgICAgICBcInRhYmluZGV4XCI6IFwiNFwiXG4gICAgICB9XG4gICAgfSwgX3ZtLl9sKChfdm0ub3B0aW9uYWxEb2NzW2luZGV4XSksIGZ1bmN0aW9uKGRvYykge1xuICAgICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgICAgXCJ2YWx1ZVwiOiBkb2MuaWRcbiAgICAgICAgfVxuICAgICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHRcIiArIF92bS5fcyhkb2MudGl0bGUpICsgXCJcXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgXCIpXSlcbiAgICB9KSwgMCldKV0pXSldKVxuICB9KV0sIDIpLCBfdm0uX3YoXCIgXCIpLCAoIV92bS5sb2FkaW5nKSA/IF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJTYXZlXCIpXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5sb2FkaW5nKSA/IF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9XG4gIH0sIFtfdm0uX20oMSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3AnLCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICAgICAgXFx0SWYgeW91IHdhbnQgdG8gdmlldyBhbGwgc2VydmljZXMgYXQgb25jZSwgcGxlYXNlIFxcblxcdFxcdCAgICAgICAgICAgIFxcdFwiKSwgX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIi92aXNhL3NlcnZpY2Uvdmlldy1hbGxcIixcbiAgICAgIFwidGFyZ2V0XCI6IFwiX2JsYW5rXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJjbGljayBoZXJlXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwic2stc3Bpbm5lciBzay1zcGlubmVyLXdhdmVcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjQwcHhcIixcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMjBweFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzay1yZWN0MVwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNrLXJlY3QyXCJcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwic2stcmVjdDNcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzay1yZWN0NFwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNrLXJlY3Q1XCJcbiAgfSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWIyYzE2ZmY4XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYjJjMTZmZjhcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9BZGRTZXJ2aWNlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDE0XG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsIGlubW9kYWxcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBfdm0uaWQsXG4gICAgICBcInRhYmluZGV4XCI6IFwiMVwiLFxuICAgICAgXCJyb2xlXCI6IFwiZGlhbG9nXCIsXG4gICAgICBcImFyaWEtbGFiZWxsZWRieVwiOiBcIm1vZGFsLWxhYmVsXCIsXG4gICAgICBcImFyaWEtaGlkZGVuXCI6IFwidHJ1ZVwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1kaWFsb2dcIixcbiAgICBjbGFzczogX3ZtLnNpemUsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwicm9sZVwiOiBcImRvY3VtZW50XCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1oZWFkZXJcIlxuICB9LCBbX3ZtLl9tKDApLCBfdm0uX3YoXCIgXCIpLCBfYygnaDQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtdGl0bGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiLCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtYm9keVwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1ib2R5XCIsIFtfdm0uX3YoXCJNb2RhbCBCb2R5XCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtZm9vdGVyXCIsIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZmxhdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjbG9zZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIixcbiAgICAgIFwiYXJpYS1sYWJlbFwiOiBcIkNsb3NlXCJcbiAgICB9XG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJhcmlhLWhpZGRlblwiOiBcInRydWVcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIsOXXCIpXSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWQwMTMzNzZlXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtZDAxMzM3NmVcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSA0MjFcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0uZWRpdENsaWVudFNlcnZpY2UoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJjbGFzc3NcIjogXCJjb2wtbWQtMTJcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cCBjb2wtbWQtNlwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTQgY29udHJvbC1sYWJlbCBcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgVGlwOlxcblxcdFxcdFxcdFxcdCAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLThcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi50aXApLFxuICAgICAgZXhwcmVzc2lvbjogXCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYudGlwXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjE3NXB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJudW1iZXJcIixcbiAgICAgIFwibWluXCI6IFwiMFwiLFxuICAgICAgXCJuYW1lXCI6IFwidGlwXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnRpcClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYsIFwidGlwXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cCAgY29sLW1kLTYgbS1sLTJcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIFN0YXR1czpcXG5cXHRcXHRcXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC04XCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnN0YXR1cyksXG4gICAgICBleHByZXNzaW9uOiBcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5zdGF0dXNcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMTc1cHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwibmFtZVwiOiBcInN0YXR1c1wiLFxuICAgICAgXCJpZFwiOiBcInN0YXR1c1wiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIHZhciAkJHNlbGVjdGVkVmFsID0gQXJyYXkucHJvdG90eXBlLmZpbHRlci5jYWxsKCRldmVudC50YXJnZXQub3B0aW9ucywgZnVuY3Rpb24obykge1xuICAgICAgICAgIHJldHVybiBvLnNlbGVjdGVkXG4gICAgICAgIH0pLm1hcChmdW5jdGlvbihvKSB7XG4gICAgICAgICAgdmFyIHZhbCA9IFwiX3ZhbHVlXCIgaW4gbyA/IG8uX3ZhbHVlIDogby52YWx1ZTtcbiAgICAgICAgICByZXR1cm4gdmFsXG4gICAgICAgIH0pO1xuICAgICAgICBfdm0uJHNldChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LCBcInN0YXR1c1wiLCAkZXZlbnQudGFyZ2V0Lm11bHRpcGxlID8gJCRzZWxlY3RlZFZhbCA6ICQkc2VsZWN0ZWRWYWxbMF0pXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ29wdGlvbicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJ2YWx1ZVwiOiBcImNvbXBsZXRlXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDb21wbGV0ZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwib24gcHJvY2Vzc1wiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiT24gUHJvY2Vzc1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwicGVuZGluZ1wiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiUGVuZGluZ1wiKV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiY2xhc3NzXCI6IFwiY29sLW1kLTEyXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXAgY29sLW1kLTZcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIENvc3Q6XFxuXFx0XFx0XFx0XFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtOFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmNvc3QpLFxuICAgICAgZXhwcmVzc2lvbjogXCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuY29zdFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwid2lkdGhcIjogXCIxNzVweFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwibnVtYmVyXCIsXG4gICAgICBcIm1pblwiOiBcIjBcIixcbiAgICAgIFwibmFtZVwiOiBcImNvc3RcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuY29zdClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYsIFwiY29zdFwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXAgIGNvbC1tZC02IG0tbC0yXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICBEaXNjb3VudDpcXG5cXHRcXHRcXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC04XCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZGlzY291bnQpLFxuICAgICAgZXhwcmVzc2lvbjogXCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZGlzY291bnRcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMTc1cHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcIm51bWJlclwiLFxuICAgICAgXCJuYW1lXCI6IFwiZGlzY291bnRcIixcbiAgICAgIFwibWluXCI6IFwiMFwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5kaXNjb3VudClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYsIFwiZGlzY291bnRcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5kaXNjb3VudCA+IDApID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgICAgIFJlYXNvbjpcXG5cXHRcXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlYXNvbiksXG4gICAgICBleHByZXNzaW9uOiBcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5yZWFzb25cIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJyZWFzb25cIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYucmVhc29uKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLiRwYXJlbnQuJHBhcmVudC5lZGl0U2VydiwgXCJyZWFzb25cIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgICAgICAgTm90ZTpcXG5cXHRcXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2Lm5vdGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYubm90ZVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcIm5vdGVcIixcbiAgICAgIFwicGxhY2Vob2xkZXJcIjogXCJmb3IgaW50ZXJuYWwgdXNlIG9ubHksIG9ubHkgZW1wbG95ZWVzIGNhbiBzZWUuXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2Lm5vdGUpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LCBcIm5vdGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJjbGFzc3NcIjogXCJjb2wtbWQtMTJcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cCBjb2wtbWQtNlwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICBTdGF0dXM6XFxuXFx0XFx0XFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImhlaWdodFwiOiBcIjVweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHt9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5hY3RpdmUpLFxuICAgICAgZXhwcmVzc2lvbjogXCIkcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuYWN0aXZlXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwicmFkaW9cIixcbiAgICAgIFwidmFsdWVcIjogXCIwXCIsXG4gICAgICBcIm5hbWVcIjogXCJhY3RpdmVcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBfdm0uX3EoX3ZtLiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5hY3RpdmUsIFwiMFwiKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJHNldChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LCBcImFjdGl2ZVwiLCBcIjBcIilcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaScpLCBfdm0uX3YoXCIgRGlzYWJsZWQgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtLWwtMlwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmFjdGl2ZSksXG4gICAgICBleHByZXNzaW9uOiBcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5hY3RpdmVcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJyYWRpb1wiLFxuICAgICAgXCJ2YWx1ZVwiOiBcIjFcIixcbiAgICAgIFwibmFtZVwiOiBcImFjdGl2ZVwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IF92bS5fcShfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmFjdGl2ZSwgXCIxXCIpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS4kc2V0KF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYsIFwiYWN0aXZlXCIsIFwiMVwiKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdpJyksIF92bS5fdihcIiBFbmFibGVkIFwiKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXAgY29sLW1kLTZcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICAgRXh0ZW5kIFRvOlxcblxcdFxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNlwiXG4gIH0sIFtfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWlubGluZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwiZGFpbHktZm9ybVwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMCksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYuZXh0ZW5kKSxcbiAgICAgIGV4cHJlc3Npb246IFwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmV4dGVuZFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGVcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCIsXG4gICAgICBcIm5hbWVcIjogXCJkYXRlXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LmV4dGVuZClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS4kcGFyZW50LiRwYXJlbnQuZWRpdFNlcnYsIFwiZXh0ZW5kXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jc1wiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMzUwcHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1wbGFjZWhvbGRlclwiOiBcIlNlbGVjdCBEb2NzXCIsXG4gICAgICBcIm11bHRpcGxlXCI6IFwiXCIsXG4gICAgICBcInRhYmluZGV4XCI6IFwiNFwiXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5yZXF1aXJlZERvY3MpLCBmdW5jdGlvbihkb2MpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogZG9jLmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHRcIiArIF92bS5fcygoZG9jLnRpdGxlKS50cmltKCkpICsgXCJcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXCIpXSlcbiAgfSksIDApXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICBcXHRPcHRpb25hbCBEb2N1bWVudHM6XFxuXFx0XFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnc2VsZWN0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3NcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjM1MHB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtcGxhY2Vob2xkZXJcIjogXCJTZWxlY3QgRG9jc1wiLFxuICAgICAgXCJtdWx0aXBsZVwiOiBcIlwiLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIjRcIlxuICAgIH1cbiAgfSwgX3ZtLl9sKChfdm0ub3B0aW9uYWxEb2NzKSwgZnVuY3Rpb24oZG9jKSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IGRvYy5pZFxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0XCIgKyBfdm0uX3MoKGRvYy50aXRsZSkudHJpbSgpKSArIFwiXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFwiKV0pXG4gIH0pLCAwKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX20oMildKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICBcXHRSZXF1aXJlZCBEb2N1bWVudHM6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMlwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJzdWJtaXRcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlNhdmVcIildKV0pXG59XX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtZTRjNWNlNmFcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1lNGM1Y2U2YVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL0VkaXRTZXJ2aWNlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDI0XG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNzIyYWUzNWFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9DbGllbnREb2N1bWVudHMudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCI1NjU1ZmViYVwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNzIyYWUzNWFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9DbGllbnREb2N1bWVudHMudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03MjJhZTM1YVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0NsaWVudERvY3VtZW50cy52dWVcIik7XG4gICAgIGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuICAgICB1cGRhdGUobmV3Q29udGVudCk7XG4gICB9KTtcbiB9XG4gLy8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtc3R5bGUtbG9hZGVyIS4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi03MjJhZTM1YVwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0NsaWVudERvY3VtZW50cy52dWVcbi8vIG1vZHVsZSBpZCA9IDQ0M1xuLy8gbW9kdWxlIGNodW5rcyA9IDIiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LWIyYzE2ZmY4XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vQWRkU2VydmljZS52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcImNjMWQwNzU2XCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1iMmMxNmZmOFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZFNlcnZpY2UudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1iMmMxNmZmOFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZFNlcnZpY2UudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYjJjMTZmZjhcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9BZGRTZXJ2aWNlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDQ4XG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZTRjNWNlNmFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9FZGl0U2VydmljZS52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjMyYzEzMDMyXCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1lNGM1Y2U2YVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0VkaXRTZXJ2aWNlLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZTRjNWNlNmFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9FZGl0U2VydmljZS52dWVcIik7XG4gICAgIGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuICAgICB1cGRhdGUobmV3Q29udGVudCk7XG4gICB9KTtcbiB9XG4gLy8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtc3R5bGUtbG9hZGVyIS4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1lNGM1Y2U2YVwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL0VkaXRTZXJ2aWNlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDUzXG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIi8qKlxuICogVHJhbnNsYXRlcyB0aGUgbGlzdCBmb3JtYXQgcHJvZHVjZWQgYnkgY3NzLWxvYWRlciBpbnRvIHNvbWV0aGluZ1xuICogZWFzaWVyIHRvIG1hbmlwdWxhdGUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbGlzdFRvU3R5bGVzIChwYXJlbnRJZCwgbGlzdCkge1xuICB2YXIgc3R5bGVzID0gW11cbiAgdmFyIG5ld1N0eWxlcyA9IHt9XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGlzdC5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gbGlzdFtpXVxuICAgIHZhciBpZCA9IGl0ZW1bMF1cbiAgICB2YXIgY3NzID0gaXRlbVsxXVxuICAgIHZhciBtZWRpYSA9IGl0ZW1bMl1cbiAgICB2YXIgc291cmNlTWFwID0gaXRlbVszXVxuICAgIHZhciBwYXJ0ID0ge1xuICAgICAgaWQ6IHBhcmVudElkICsgJzonICsgaSxcbiAgICAgIGNzczogY3NzLFxuICAgICAgbWVkaWE6IG1lZGlhLFxuICAgICAgc291cmNlTWFwOiBzb3VyY2VNYXBcbiAgICB9XG4gICAgaWYgKCFuZXdTdHlsZXNbaWRdKSB7XG4gICAgICBzdHlsZXMucHVzaChuZXdTdHlsZXNbaWRdID0geyBpZDogaWQsIHBhcnRzOiBbcGFydF0gfSlcbiAgICB9IGVsc2Uge1xuICAgICAgbmV3U3R5bGVzW2lkXS5wYXJ0cy5wdXNoKHBhcnQpXG4gICAgfVxuICB9XG4gIHJldHVybiBzdHlsZXNcbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtc3R5bGUtbG9hZGVyL2xpYi9saXN0VG9TdHlsZXMuanNcbi8vIG1vZHVsZSBpZCA9IDdcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgMyA0IDUgOSAxMSAxMiAxMyIsIjx0ZW1wbGF0ZT5cbjxkaXYgY2xhc3M9XCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIgOmlkPVwiaWRcIiB0YWJpbmRleD1cIi0xXCIgcm9sZT1cImRpYWxvZ1wiIGFyaWEtbGFiZWxsZWRieT1cIm1vZGFsLWxhYmVsXCI+XG4gICAgPGRpdiA6Y2xhc3M9XCInbW9kYWwtZGlhbG9nICcrc2l6ZVwiIHJvbGU9XCJkb2N1bWVudFwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPlxuICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCIgaWQ9XCJtb2RhbC1sYWJlbFwiPlxuICAgICAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtdGl0bGVcIj5Nb2RhbCBUaXRsZTwvc2xvdD5cbiAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1ib2R5XCI+TW9kYWwgQm9keTwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPlxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9zbG90PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICAgIHByb3BzOiB7XG4gICAgICAgICdpZCc6e3JlcXVpcmVkOnRydWV9XG4gICAgICAgICwnc2l6ZSc6IHtkZWZhdWx0Oidtb2RhbC1tZCd9XG4gICAgfVxufVxuPC9zY3JpcHQ+XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRGlhbG9nTW9kYWwudnVlPzAwM2JkYTg4Il0sInNvdXJjZVJvb3QiOiIifQ==