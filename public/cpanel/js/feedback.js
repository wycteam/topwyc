/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 256);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 134:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('feedback', __webpack_require__(217));

var FeedbackApp = new Vue({
	el: '#feedback-root',

	data: {
		pending: [],
		approved: [],
		rejected: [],
		per_page: 5,
		pending_page: 0,
		approved_page: 0,
		rejected_page: 0,
		pending_count: 0,
		approved_count: 0,
		rejected_count: 0
	},

	methods: {
		showPage: function showPage(approved, skip) {
			var _this = this;

			skip--;
			axiosAPIv1.get('/feedbacks', {
				params: {
					approved: approved,
					skip: skip * this.per_page,
					count: this.per_page
				}
			}).then(function (response) {
				if (response.status == 200) {
					switch (approved) {
						case 0:
							_this.pending = response.data;break;
						case 1:
							_this.approved = response.data;break;
						case 2:
							_this.rejected = response.data;break;
					}
				}
			});
		}
	},

	created: function created() {
		var _this2 = this;

		function getApprovedCount() {
			return axiosAPIv1.get('/feedbacks/count?approved=1');
		}
		function getRejectedCount() {
			return axiosAPIv1.get('/feedbacks/count?approved=2');
		}
		function getPendingCount() {
			return axiosAPIv1.get('/feedbacks/count?approved=0');
		}

		axios.all([getPendingCount(), getApprovedCount(), getRejectedCount()]).then(axios.spread(function (pending, approved, rejected) {

			_this2.pending_count = pending.data;
			_this2.approved_count = approved.data;
			_this2.rejected_count = rejected.data;

			_this2.pending_page = Math.ceil(pending.data / _this2.per_page);
			_this2.approved_page = Math.ceil(approved.data / _this2.per_page);
			_this2.rejected_page = Math.ceil(rejected.data / _this2.per_page);

			if (_this2.pending_page) {
				_this2.showPage(0, 0);
			}
			if (_this2.approved_page) {
				_this2.showPage(1, 0);
			}
			if (_this2.rejected_page) {
				_this2.showPage(2, 0);
			}
		})).catch($.noop);
	}
});

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['feedback'],
    methods: {
        process: function process(id, approve, from, msg) {
            var _this = this;

            axiosAPIv1.put('/feedbacks/' + id, {
                approved: approve,
                processor_id: Laravel.user.id
            }).then(function (result) {
                if (result.status == 200) {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    var fromArray = null;
                    switch (from) {
                        case 'pending':
                            fromArray = _this.$root.pending;
                            _this.$root.pending_count--;
                            break;

                        case 'accepted':
                            fromArray = _this.$root.approved;
                            _this.$root.approved_count--;
                            break;

                        case 'rejected':
                            fromArray = _this.$root.rejected;
                            _this.$root.rejected_count--;
                            break;
                    }
                    //remove
                    var Index = fromArray.findIndex(function (i) {
                        return i.id === id;
                    });
                    fromArray.splice(Index, 1);

                    var toArray = null;
                    var opentab = null;
                    switch (approve) {
                        case 0:
                            toArray = _this.$root.pending;
                            opentab = 'pending';
                            _this.$root.pending_count++;
                            break;
                        case 1:
                            toArray = _this.$root.approved;
                            opentab = 'approved';
                            _this.$root.approved_count++;
                            break;
                        case 2:
                            toArray = _this.$root.rejected;
                            opentab = 'rejected';
                            _this.$root.rejected_count++;
                            break;
                    }

                    toArray.unshift(result.data.data);
                    toastr.success(msg, 'Feedback');

                    //open to tab
                    $('a[href="#fb-' + opentab + '"]').tab('show');

                    //update shit
                }
            });
        }
    },

    computed: {
        toHumanCreatedAt: function toHumanCreatedAt() {
            return moment.utc(this.feedback.created_at).fromNow();
        },
        toHumanApprovedAt: function toHumanApprovedAt() {
            return moment.utc(this.feedback.updated).fromNow();
        },
        toReadableDate: function toReadableDate() {
            var date = this.feedback.approved ? this.feedback.updated_at : this.feedback.created_at;
            return moment.utc(date).format('MMMM Do YYYY, h:mm:ss a');
        },
        processor: function processor() {
            return this.feedback.processor.first_name + ' ' + this.feedback.processor.last_name;
        },
        user: function user() {
            return this.feedback.user.first_name + ' ' + this.feedback.user.last_name;
        },
        processorWithLabel: function processorWithLabel() {
            return this.feedback.processor ? "<b>Processed By: " + this.processor + "</b><br>on " + this.toReadableDate : this.toReadableDate;
        }
    }
});

/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(181),
  /* template */
  __webpack_require__(229),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\cpanel\\components\\FeedbackContainer.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] FeedbackContainer.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-11e42ba7", Component.options)
  } else {
    hotAPI.reload("data-v-11e42ba7", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "feed-element"
  }, [_c('a', {
    staticClass: "pull-left",
    attrs: {
      "href": "#"
    }
  }, [_c('img', {
    staticClass: "img-circle",
    attrs: {
      "alt": "image",
      "src": _vm.feedback.user.avatar
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "media-body "
  }, [_c('small', {
    staticClass: "pull-right"
  }, [_vm._v(_vm._s(_vm.toHumanApprovedAt))]), _vm._v(" "), _c('strong', [_vm._v(_vm._s(_vm.user))]), _vm._v(" posted a "), _c('strong', [_vm._v("feedback")]), _vm._v(" . "), _c('br'), _vm._v(" "), _c('small', {
    staticClass: "text-muted"
  }, [_c('span', {
    domProps: {
      "innerHTML": _vm._s(_vm.processorWithLabel)
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [(_vm.feedback.image) ? _c('div', {
    staticClass: "col-md-3"
  }, [_c('img', {
    staticClass: "img-responsive",
    attrs: {
      "src": _vm.feedback.image_path + "feedback.jpg"
    }
  })]) : _vm._e(), _vm._v(" "), _c('div', {
    class: _vm.feedback.image ? 'col-md-9' : 'col-md-12'
  }, [_c('div', {
    staticClass: "well"
  }, [_vm._v("\n\t\t\t\t\t" + _vm._s(_vm.feedback.content) + "\n\t\t\t\t")])])]), _vm._v(" "), _c('div', {
    staticClass: "pull-right"
  }, [(_vm.feedback.approved == 0) ? _c('a', {
    staticClass: "btn btn-sm btn-danger",
    on: {
      "click": function($event) {
        _vm.process(_vm.feedback.id, 2, "pending", "Rejected!")
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-times"
  }), _vm._v(" Reject")]) : _vm._e(), _vm._v(" "), (_vm.feedback.approved == 0) ? _c('a', {
    staticClass: "btn btn-sm btn-primary",
    on: {
      "click": function($event) {
        _vm.process(_vm.feedback.id, 1, "pending", "Approved!")
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-check"
  }), _vm._v(" Approve")]) : _vm._e(), _vm._v(" "), (_vm.feedback.approved != 0) ? _c('a', {
    staticClass: "btn btn-sm btn-warning",
    on: {
      "click": function($event) {
        _vm.process(_vm.feedback.id, 0, _vm.feedback.approved == 2 ? "rejected" : "accepted", "Restored to Pending!")
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-check"
  }), _vm._v(" Restore")]) : _vm._e()])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-11e42ba7", module.exports)
  }
}

/***/ }),

/***/ 256:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(134);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDEiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9mZWVkYmFjay5qcyIsIndlYnBhY2s6Ly8vRmVlZGJhY2tDb250YWluZXIudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvRmVlZGJhY2tDb250YWluZXIudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvRmVlZGJhY2tDb250YWluZXIudnVlPzhmMDUiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsIkZlZWRiYWNrQXBwIiwiZWwiLCJkYXRhIiwicGVuZGluZyIsImFwcHJvdmVkIiwicmVqZWN0ZWQiLCJwZXJfcGFnZSIsInBlbmRpbmdfcGFnZSIsImFwcHJvdmVkX3BhZ2UiLCJyZWplY3RlZF9wYWdlIiwicGVuZGluZ19jb3VudCIsImFwcHJvdmVkX2NvdW50IiwicmVqZWN0ZWRfY291bnQiLCJtZXRob2RzIiwic2hvd1BhZ2UiLCJza2lwIiwiYXhpb3NBUEl2MSIsImdldCIsInBhcmFtcyIsImNvdW50IiwidGhlbiIsInJlc3BvbnNlIiwic3RhdHVzIiwiY3JlYXRlZCIsImdldEFwcHJvdmVkQ291bnQiLCJnZXRSZWplY3RlZENvdW50IiwiZ2V0UGVuZGluZ0NvdW50IiwiYXhpb3MiLCJhbGwiLCJzcHJlYWQiLCJNYXRoIiwiY2VpbCIsImNhdGNoIiwiJCIsIm5vb3AiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQUEsSUFBSUMsU0FBSixDQUFjLFVBQWQsRUFBMkIsbUJBQUFDLENBQVEsR0FBUixDQUEzQjs7QUFFQSxJQUFJQyxjQUFjLElBQUlILEdBQUosQ0FBUTtBQUN6QkksS0FBRyxnQkFEc0I7O0FBR3pCQyxPQUFLO0FBQ0pDLFdBQVEsRUFESjtBQUVKQyxZQUFTLEVBRkw7QUFHSkMsWUFBUyxFQUhMO0FBSUpDLFlBQVMsQ0FKTDtBQUtKQyxnQkFBYSxDQUxUO0FBTUpDLGlCQUFjLENBTlY7QUFPSkMsaUJBQWMsQ0FQVjtBQVFKQyxpQkFBYyxDQVJWO0FBU0pDLGtCQUFlLENBVFg7QUFVSkMsa0JBQWU7QUFWWCxFQUhvQjs7QUFnQnpCQyxVQUFRO0FBQ1BDLFVBRE8sb0JBQ0VWLFFBREYsRUFDV1csSUFEWCxFQUNnQjtBQUFBOztBQUN0QkE7QUFDQUMsY0FBV0MsR0FBWCxDQUFlLFlBQWYsRUFBNEI7QUFDM0JDLFlBQU87QUFDTmQsZUFBU0EsUUFESDtBQUVOVyxXQUFLQSxPQUFLLEtBQUtULFFBRlQ7QUFHTmEsWUFBTSxLQUFLYjtBQUhMO0FBRG9CLElBQTVCLEVBTUdjLElBTkgsQ0FNUSxvQkFBWTtBQUNuQixRQUFHQyxTQUFTQyxNQUFULElBQW1CLEdBQXRCLEVBQ0E7QUFDQyxhQUFPbEIsUUFBUDtBQUVDLFdBQUssQ0FBTDtBQUFRLGFBQUtELE9BQUwsR0FBZWtCLFNBQVNuQixJQUF4QixDQUE4QjtBQUN0QyxXQUFLLENBQUw7QUFBUSxhQUFLRSxRQUFMLEdBQWdCaUIsU0FBU25CLElBQXpCLENBQStCO0FBQ3ZDLFdBQUssQ0FBTDtBQUFRLGFBQUtHLFFBQUwsR0FBZ0JnQixTQUFTbkIsSUFBekIsQ0FBK0I7QUFKeEM7QUFNQTtBQUNELElBaEJEO0FBaUJBO0FBcEJNLEVBaEJpQjs7QUF1Q3pCcUIsUUF2Q3lCLHFCQXVDaEI7QUFBQTs7QUFFUixXQUFTQyxnQkFBVCxHQUEyQjtBQUMxQixVQUFPUixXQUFXQyxHQUFYLENBQWUsNkJBQWYsQ0FBUDtBQUNBO0FBQ0QsV0FBU1EsZ0JBQVQsR0FBMkI7QUFDMUIsVUFBT1QsV0FBV0MsR0FBWCxDQUFlLDZCQUFmLENBQVA7QUFDQTtBQUNELFdBQVNTLGVBQVQsR0FBMEI7QUFDekIsVUFBT1YsV0FBV0MsR0FBWCxDQUFlLDZCQUFmLENBQVA7QUFDQTs7QUFFRFUsUUFBTUMsR0FBTixDQUFVLENBQ1JGLGlCQURRLEVBRVJGLGtCQUZRLEVBR1JDLGtCQUhRLENBQVYsRUFJR0wsSUFKSCxDQUlRTyxNQUFNRSxNQUFOLENBQ1AsVUFBQzFCLE9BQUQsRUFBU0MsUUFBVCxFQUFrQkMsUUFBbEIsRUFBK0I7O0FBRS9CLFVBQUtLLGFBQUwsR0FBcUJQLFFBQVFELElBQTdCO0FBQ0EsVUFBS1MsY0FBTCxHQUFzQlAsU0FBU0YsSUFBL0I7QUFDQSxVQUFLVSxjQUFMLEdBQXNCUCxTQUFTSCxJQUEvQjs7QUFHQSxVQUFLSyxZQUFMLEdBQW9CdUIsS0FBS0MsSUFBTCxDQUFVNUIsUUFBUUQsSUFBUixHQUFlLE9BQUtJLFFBQTlCLENBQXBCO0FBQ0EsVUFBS0UsYUFBTCxHQUFxQnNCLEtBQUtDLElBQUwsQ0FBVTNCLFNBQVNGLElBQVQsR0FBZ0IsT0FBS0ksUUFBL0IsQ0FBckI7QUFDQSxVQUFLRyxhQUFMLEdBQXFCcUIsS0FBS0MsSUFBTCxDQUFVMUIsU0FBU0gsSUFBVCxHQUFnQixPQUFLSSxRQUEvQixDQUFyQjs7QUFJQSxPQUFHLE9BQUtDLFlBQVIsRUFBcUI7QUFDcEIsV0FBS08sUUFBTCxDQUFjLENBQWQsRUFBZ0IsQ0FBaEI7QUFDQTtBQUNELE9BQUcsT0FBS04sYUFBUixFQUFzQjtBQUNyQixXQUFLTSxRQUFMLENBQWMsQ0FBZCxFQUFnQixDQUFoQjtBQUNBO0FBQ0QsT0FBRyxPQUFLTCxhQUFSLEVBQXNCO0FBQ3JCLFdBQUtLLFFBQUwsQ0FBYyxDQUFkLEVBQWdCLENBQWhCO0FBQ0E7QUFJRCxHQTFCTyxDQUpSLEVBK0JDa0IsS0EvQkQsQ0ErQk9DLEVBQUVDLElBL0JUO0FBaUNBO0FBcEZ3QixDQUFSLENBQWxCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM2QkE7WUFFQTs7O0FBRUE7O0FBQ0E7MEJBRUE7MkNBRUE7QUFIQSxzQ0FJQTtxQ0FDQSxLQUNBOztxQ0FFQTtxQ0FDQTtvQ0FDQTtpQ0FFQTtBQUxBO29DQU1BOzRCQUVBOzZCQUNBO29EQUNBO3dDQUNBO0FBRUE7OzZCQUNBO29EQUNBO3dDQUNBO0FBRUE7OzZCQUNBO29EQUNBO3dDQUNBO0FBRUE7O0FBQ0E7O3dDQUNBOzs0Q0FFQTs7a0NBQ0E7a0NBQ0E7NEJBRUE7NkJBQ0E7a0RBQ0E7c0NBQ0E7d0NBQ0E7QUFDQTs2QkFDQTtrREFDQTtzQ0FDQTt3Q0FDQTtBQUNBOzZCQUNBO2tEQUNBO3NDQUNBO3dDQUNBO0FBR0E7OztnREFDQTt3Q0FFQTs7QUFDQTsyREFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFJQTtBQXhFQTs7O3NEQTBFQTt3REFDQTtBQUVBO3dEQUNBO3FEQUNBO0FBRUE7a0RBQ0E7eUZBQ0E7MkNBQ0E7QUFFQTt3Q0FDQTtzRkFDQTtBQUVBOzhCQUNBOzRFQUNBO0FBRUE7MERBQ0E7K0hBQ0E7QUFFQTtBQXpCQTtBQTNFQSxHOzs7Ozs7O0FDaENBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBNEc7QUFDNUc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9mZWVkYmFjay5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMjU2KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBiMjBhMmEwYjI5NTZhOWQ2YzBkMSIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNSA2IDcgOCA5IDEwIDExIDEyIiwiVnVlLmNvbXBvbmVudCgnZmVlZGJhY2snLCAgcmVxdWlyZSgnLi9jb21wb25lbnRzL0ZlZWRiYWNrQ29udGFpbmVyLnZ1ZScpKTtcclxuXHJcbnZhciBGZWVkYmFja0FwcCA9IG5ldyBWdWUoe1xyXG5cdGVsOicjZmVlZGJhY2stcm9vdCcsXHJcblx0XHJcblx0ZGF0YTp7XHJcblx0XHRwZW5kaW5nOltdLFxyXG5cdFx0YXBwcm92ZWQ6W10sXHJcblx0XHRyZWplY3RlZDpbXSxcclxuXHRcdHBlcl9wYWdlOjUsXHJcblx0XHRwZW5kaW5nX3BhZ2U6MCxcclxuXHRcdGFwcHJvdmVkX3BhZ2U6MCxcclxuXHRcdHJlamVjdGVkX3BhZ2U6MCxcclxuXHRcdHBlbmRpbmdfY291bnQ6MCxcclxuXHRcdGFwcHJvdmVkX2NvdW50OjAsXHJcblx0XHRyZWplY3RlZF9jb3VudDowLFxyXG5cdH0sXHJcblxyXG5cdG1ldGhvZHM6e1xyXG5cdFx0c2hvd1BhZ2UoYXBwcm92ZWQsc2tpcCl7XHJcblx0XHRcdHNraXAtLTtcclxuXHRcdFx0YXhpb3NBUEl2MS5nZXQoJy9mZWVkYmFja3MnLHtcclxuXHRcdFx0XHRwYXJhbXM6e1xyXG5cdFx0XHRcdFx0YXBwcm92ZWQ6YXBwcm92ZWQsXHJcblx0XHRcdFx0XHRza2lwOnNraXAqdGhpcy5wZXJfcGFnZSxcclxuXHRcdFx0XHRcdGNvdW50OnRoaXMucGVyX3BhZ2VcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdGlmKHJlc3BvbnNlLnN0YXR1cyA9PSAyMDApXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0c3dpdGNoKGFwcHJvdmVkKVxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRjYXNlIDA6IHRoaXMucGVuZGluZyA9IHJlc3BvbnNlLmRhdGE7IGJyZWFrO1xyXG5cdFx0XHRcdFx0XHRjYXNlIDE6IHRoaXMuYXBwcm92ZWQgPSByZXNwb25zZS5kYXRhOyBicmVhaztcclxuXHRcdFx0XHRcdFx0Y2FzZSAyOiB0aGlzLnJlamVjdGVkID0gcmVzcG9uc2UuZGF0YTsgYnJlYWs7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdH0sXHJcblx0fSxcclxuXHJcblx0Y3JlYXRlZCgpe1xyXG5cdFx0XHJcblx0XHRmdW5jdGlvbiBnZXRBcHByb3ZlZENvdW50KCl7XHJcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2ZlZWRiYWNrcy9jb3VudD9hcHByb3ZlZD0xJyk7XHJcblx0XHR9XHJcblx0XHRmdW5jdGlvbiBnZXRSZWplY3RlZENvdW50KCl7XHJcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2ZlZWRiYWNrcy9jb3VudD9hcHByb3ZlZD0yJyk7XHJcblx0XHR9XHJcblx0XHRmdW5jdGlvbiBnZXRQZW5kaW5nQ291bnQoKXtcclxuXHRcdFx0cmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvZmVlZGJhY2tzL2NvdW50P2FwcHJvdmVkPTAnKTtcclxuXHRcdH1cclxuXHJcblx0XHRheGlvcy5hbGwoW1xyXG5cdFx0XHQgZ2V0UGVuZGluZ0NvdW50KClcclxuXHRcdFx0LGdldEFwcHJvdmVkQ291bnQoKVxyXG5cdFx0XHQsZ2V0UmVqZWN0ZWRDb3VudCgpXHJcblx0XHRdKS50aGVuKGF4aW9zLnNwcmVhZChcclxuXHRcdFx0KHBlbmRpbmcsYXBwcm92ZWQscmVqZWN0ZWQpID0+IHtcclxuXHRcdFx0XHJcblx0XHRcdHRoaXMucGVuZGluZ19jb3VudCA9IHBlbmRpbmcuZGF0YTtcclxuXHRcdFx0dGhpcy5hcHByb3ZlZF9jb3VudCA9IGFwcHJvdmVkLmRhdGE7XHJcblx0XHRcdHRoaXMucmVqZWN0ZWRfY291bnQgPSByZWplY3RlZC5kYXRhO1xyXG5cdFx0XHRcclxuXHJcblx0XHRcdHRoaXMucGVuZGluZ19wYWdlID0gTWF0aC5jZWlsKHBlbmRpbmcuZGF0YSAvIHRoaXMucGVyX3BhZ2UpO1xyXG5cdFx0XHR0aGlzLmFwcHJvdmVkX3BhZ2UgPSBNYXRoLmNlaWwoYXBwcm92ZWQuZGF0YSAvIHRoaXMucGVyX3BhZ2UpO1xyXG5cdFx0XHR0aGlzLnJlamVjdGVkX3BhZ2UgPSBNYXRoLmNlaWwocmVqZWN0ZWQuZGF0YSAvIHRoaXMucGVyX3BhZ2UpO1xyXG5cdFx0XHRcclxuXHJcblxyXG5cdFx0XHRpZih0aGlzLnBlbmRpbmdfcGFnZSl7XHJcblx0XHRcdFx0dGhpcy5zaG93UGFnZSgwLDApXHJcblx0XHRcdH1cclxuXHRcdFx0aWYodGhpcy5hcHByb3ZlZF9wYWdlKXtcclxuXHRcdFx0XHR0aGlzLnNob3dQYWdlKDEsMClcclxuXHRcdFx0fVxyXG5cdFx0XHRpZih0aGlzLnJlamVjdGVkX3BhZ2Upe1xyXG5cdFx0XHRcdHRoaXMuc2hvd1BhZ2UoMiwwKVxyXG5cdFx0XHR9XHJcblxyXG5cclxuXHJcblx0XHR9KSlcclxuXHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cclxuXHR9LFxyXG5cclxuXHJcbn0pXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZmVlZGJhY2suanMiLCI8dGVtcGxhdGU+XHJcblx0PGRpdiBjbGFzcz1cImZlZWQtZWxlbWVudFwiPlxyXG5cdFx0PGEgaHJlZj1cIiNcIiBjbGFzcz1cInB1bGwtbGVmdFwiPlxyXG5cdFx0XHQ8aW1nIGFsdD1cImltYWdlXCIgY2xhc3M9XCJpbWctY2lyY2xlXCIgOnNyYz1cImZlZWRiYWNrLnVzZXIuYXZhdGFyXCI+XHJcblx0XHQ8L2E+XHJcblx0XHQ8ZGl2IGNsYXNzPVwibWVkaWEtYm9keSBcIj5cclxuXHRcdFx0PHNtYWxsIGNsYXNzPVwicHVsbC1yaWdodFwiPnt7dG9IdW1hbkFwcHJvdmVkQXR9fTwvc21hbGw+XHJcblx0XHRcdDxzdHJvbmc+e3t1c2VyfX08L3N0cm9uZz4gcG9zdGVkIGEgPHN0cm9uZz5mZWVkYmFjazwvc3Ryb25nPiAuIDxicj5cclxuXHRcdFx0PHNtYWxsIGNsYXNzPVwidGV4dC1tdXRlZFwiID5cclxuXHRcdFx0XHQ8c3BhbiB2LWh0bWw9XCJwcm9jZXNzb3JXaXRoTGFiZWxcIj48L3NwYW4+XHJcblx0XHRcdDwvc21hbGw+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTNcIiB2LWlmPSdmZWVkYmFjay5pbWFnZSc+XHJcblx0XHRcdFx0XHQ8aW1nIDpzcmM9J2ZlZWRiYWNrLmltYWdlX3BhdGgrXCJmZWVkYmFjay5qcGdcIicgY2xhc3M9J2ltZy1yZXNwb25zaXZlJz5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8ZGl2IDpjbGFzcz1cImZlZWRiYWNrLmltYWdlPydjb2wtbWQtOSc6J2NvbC1tZC0xMidcIj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJ3ZWxsXCI+XHJcblx0XHRcdFx0XHRcdHt7ZmVlZGJhY2suY29udGVudH19XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCJwdWxsLXJpZ2h0XCI+XHJcblx0XHRcdFx0PGEgdi1pZj0nZmVlZGJhY2suYXBwcm92ZWQ9PTAnIGNsYXNzPVwiYnRuIGJ0bi1zbSBidG4tZGFuZ2VyXCIgQGNsaWNrPSdwcm9jZXNzKGZlZWRiYWNrLmlkLDIsXCJwZW5kaW5nXCIsXCJSZWplY3RlZCFcIiknPjxpIGNsYXNzPVwiZmEgZmEtdGltZXNcIj48L2k+IFJlamVjdDwvYT5cclxuXHRcdFx0XHQ8YSB2LWlmPSdmZWVkYmFjay5hcHByb3ZlZD09MCcgY2xhc3M9XCJidG4gYnRuLXNtIGJ0bi1wcmltYXJ5XCIgQGNsaWNrPSdwcm9jZXNzKGZlZWRiYWNrLmlkLDEsXCJwZW5kaW5nXCIsXCJBcHByb3ZlZCFcIiknPjxpIGNsYXNzPVwiZmEgZmEtY2hlY2tcIj48L2k+IEFwcHJvdmU8L2E+XHJcblx0XHRcdFx0PGEgdi1pZj0nZmVlZGJhY2suYXBwcm92ZWQhPTAnIGNsYXNzPVwiYnRuIGJ0bi1zbSBidG4td2FybmluZ1wiIEBjbGljaz0ncHJvY2VzcyhmZWVkYmFjay5pZCwwLGZlZWRiYWNrLmFwcHJvdmVkPT0yP1wicmVqZWN0ZWRcIjpcImFjY2VwdGVkXCIsXCJSZXN0b3JlZCB0byBQZW5kaW5nIVwiKSc+PGkgY2xhc3M9XCJmYSBmYS1jaGVja1wiPjwvaT4gUmVzdG9yZTwvYT5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQ8L2Rpdj5cclxuXHQ8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRwcm9wczogWydmZWVkYmFjayddLFxyXG4gICAgbWV0aG9kczp7XHJcbiAgICBcdHByb2Nlc3MoaWQsYXBwcm92ZSxmcm9tLG1zZyl7XHJcbiAgICBcdFx0YXhpb3NBUEl2MVxyXG5cdFx0XHRcdC5wdXQoJy9mZWVkYmFja3MvJytpZCx7XHJcblx0XHRcdFx0XHRhcHByb3ZlZDphcHByb3ZlLFxyXG5cdFx0XHRcdFx0cHJvY2Vzc29yX2lkOkxhcmF2ZWwudXNlci5pZFxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRcdGlmKHJlc3VsdC5zdGF0dXMgPT0gMjAwKVxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHR0b2FzdHIub3B0aW9ucyA9IHtcclxuXHRcdCAgICAgICAgICAgICAgICAgICAgY2xvc2VCdXR0b246IHRydWUsXHJcblx0XHQgICAgICAgICAgICAgICAgICAgIHByb2dyZXNzQmFyOiB0cnVlLFxyXG5cdFx0ICAgICAgICAgICAgICAgICAgICBzaG93TWV0aG9kOiAnc2xpZGVEb3duJyxcclxuXHRcdCAgICAgICAgICAgICAgICAgICAgdGltZU91dDogNDAwMFxyXG5cdFx0ICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIFx0XHRcdHZhciBmcm9tQXJyYXkgPSBudWxsO1xyXG5cdFx0ICAgICAgICAgICAgICAgIHN3aXRjaChmcm9tKVxyXG5cdFx0ICAgICAgICAgICAgICAgIHtcclxuXHRcdCAgICAgICAgICAgICAgICBcdGNhc2UgJ3BlbmRpbmcnOlxyXG5cdFx0ICAgICAgICAgICAgXHRcdFx0ZnJvbUFycmF5ID0gdGhpcy4kcm9vdC5wZW5kaW5nO1xyXG5cdFx0ICAgICAgICAgICAgXHRcdFx0dGhpcy4kcm9vdC5wZW5kaW5nX2NvdW50LS07XHJcblx0XHQgICAgICAgICAgICBcdFx0XHRicmVhaztcclxuXHJcblx0XHQgICAgICAgICAgICAgICAgXHRjYXNlICdhY2NlcHRlZCc6XHJcblx0XHQgICAgICAgICAgICBcdFx0XHRmcm9tQXJyYXkgPSB0aGlzLiRyb290LmFwcHJvdmVkO1xyXG5cdFx0ICAgICAgICAgICAgXHRcdFx0dGhpcy4kcm9vdC5hcHByb3ZlZF9jb3VudC0tO1xyXG5cdFx0ICAgICAgICAgICAgXHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0ICAgICAgICAgICAgICAgIFx0Y2FzZSAncmVqZWN0ZWQnOlxyXG5cdFx0ICAgICAgICAgICAgXHRcdFx0ZnJvbUFycmF5ID0gdGhpcy4kcm9vdC5yZWplY3RlZDtcclxuXHRcdCAgICAgICAgICAgIFx0XHRcdHRoaXMuJHJvb3QucmVqZWN0ZWRfY291bnQtLTtcclxuXHRcdCAgICAgICAgICAgIFx0XHRcdGJyZWFrO1xyXG5cdFx0ICAgICAgICAgICAgICAgIH1cclxuXHRcdCAgICAgICAgICAgICAgICAvL3JlbW92ZVxyXG4gICAgICAgICAgICBcdFx0XHR2YXIgSW5kZXggPSBmcm9tQXJyYXkuZmluZEluZGV4KCBpID0+IGkuaWQgPT09IGlkKSBcclxuICAgICAgICAgICAgXHRcdFx0ZnJvbUFycmF5LnNwbGljZShJbmRleCAsMSk7XHJcblxyXG4gICAgICAgICAgICBcdFx0XHR2YXIgdG9BcnJheSA9IG51bGw7XHJcbiAgICAgICAgICAgIFx0XHRcdHZhciBvcGVudGFiID0gbnVsbDtcclxuICAgICAgICAgICAgXHRcdFx0c3dpdGNoKGFwcHJvdmUpXHJcbiAgICAgICAgICAgIFx0XHRcdHtcclxuICAgICAgICAgICAgXHRcdFx0XHRjYXNlIDA6XHJcbiAgICAgICAgICAgIFx0XHRcdFx0XHR0b0FycmF5ID0gdGhpcy4kcm9vdC5wZW5kaW5nO1xyXG4gICAgICAgICAgICBcdFx0XHRcdFx0b3BlbnRhYiA9ICdwZW5kaW5nJztcclxuICAgICAgICAgICAgXHRcdFx0XHRcdHRoaXMuJHJvb3QucGVuZGluZ19jb3VudCsrO1xyXG4gICAgICAgICAgICBcdFx0XHRcdGJyZWFrO1xyXG4gICAgICAgICAgICBcdFx0XHRcdGNhc2UgMTpcclxuICAgICAgICAgICAgXHRcdFx0XHRcdHRvQXJyYXkgPSB0aGlzLiRyb290LmFwcHJvdmVkO1xyXG4gICAgICAgICAgICBcdFx0XHRcdFx0b3BlbnRhYiA9ICdhcHByb3ZlZCc7XHJcblx0XHRcdFx0XHRcdFx0XHR0aGlzLiRyb290LmFwcHJvdmVkX2NvdW50Kys7XHJcbiAgICAgICAgICAgIFx0XHRcdFx0YnJlYWs7XHJcbiAgICAgICAgICAgIFx0XHRcdFx0Y2FzZSAyOlxyXG4gICAgICAgICAgICBcdFx0XHRcdFx0dG9BcnJheSA9IHRoaXMuJHJvb3QucmVqZWN0ZWQ7XHJcbiAgICAgICAgICAgIFx0XHRcdFx0XHRvcGVudGFiID0gJ3JlamVjdGVkJztcclxuXHRcdFx0XHRcdFx0XHRcdHRoaXMuJHJvb3QucmVqZWN0ZWRfY291bnQrKztcclxuICAgICAgICAgICAgXHRcdFx0XHRicmVhaztcclxuICAgICAgICAgICAgXHRcdFx0fVxyXG5cclxuXHRcdCAgICAgICAgICAgICAgICB0b0FycmF5LnVuc2hpZnQocmVzdWx0LmRhdGEuZGF0YSk7XHJcblx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MobXNnLCAnRmVlZGJhY2snKTtcclxuXHJcblx0XHQgICAgICAgICAgICAgICAgLy9vcGVuIHRvIHRhYlxyXG5cdFx0ICAgICAgICAgICAgICAgICQoJ2FbaHJlZj1cIiNmYi0nK29wZW50YWIrJ1wiXScpLnRhYignc2hvdycpO1xyXG5cclxuXHRcdCAgICAgICAgICAgICAgICAvL3VwZGF0ZSBzaGl0XHJcblxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pXHJcbiAgICBcdH1cclxuXHRcdFxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgXHR0b0h1bWFuQ3JlYXRlZEF0KCkge1xyXG4gICAgXHRcdHJldHVybiBtb21lbnQudXRjKHRoaXMuZmVlZGJhY2suY3JlYXRlZF9hdCkuZnJvbU5vdygpO1xyXG4gICAgXHR9LFxyXG5cclxuICAgIFx0dG9IdW1hbkFwcHJvdmVkQXQoKSB7XHJcbiAgICBcdFx0cmV0dXJuIG1vbWVudC51dGModGhpcy5mZWVkYmFjay51cGRhdGVkKS5mcm9tTm93KCk7XHJcbiAgICBcdH0sXHJcblxyXG4gICAgXHR0b1JlYWRhYmxlRGF0ZSgpe1xyXG4gICAgXHRcdHZhciBkYXRlID0gdGhpcy5mZWVkYmFjay5hcHByb3ZlZD90aGlzLmZlZWRiYWNrLnVwZGF0ZWRfYXQ6dGhpcy5mZWVkYmFjay5jcmVhdGVkX2F0O1xyXG4gICAgXHRcdHJldHVybiBtb21lbnQudXRjKGRhdGUpLmZvcm1hdCgnTU1NTSBEbyBZWVlZLCBoOm1tOnNzIGEnKTtcclxuICAgIFx0fSxcclxuXHJcbiAgICBcdHByb2Nlc3Nvcigpe1xyXG4gICAgXHRcdHJldHVybiB0aGlzLmZlZWRiYWNrLnByb2Nlc3Nvci5maXJzdF9uYW1lKycgJyt0aGlzLmZlZWRiYWNrLnByb2Nlc3Nvci5sYXN0X25hbWU7XHJcbiAgICBcdH0sXHJcblxyXG4gICAgXHR1c2VyKCl7XHJcbiAgICBcdFx0cmV0dXJuIHRoaXMuZmVlZGJhY2sudXNlci5maXJzdF9uYW1lKycgJyt0aGlzLmZlZWRiYWNrLnVzZXIubGFzdF9uYW1lO1xyXG4gICAgXHR9LFxyXG5cclxuICAgIFx0cHJvY2Vzc29yV2l0aExhYmVsKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLmZlZWRiYWNrLnByb2Nlc3Nvcj9cIjxiPlByb2Nlc3NlZCBCeTogXCIgK3RoaXMucHJvY2Vzc29yICsgXCI8L2I+PGJyPm9uIFwiICsgdGhpcy50b1JlYWRhYmxlRGF0ZTp0aGlzLnRvUmVhZGFibGVEYXRlO1xyXG4gICAgXHR9XHJcbiAgICB9LFxyXG59XHJcblxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRmVlZGJhY2tDb250YWluZXIudnVlPzdhYmE1MDQ4IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRmVlZGJhY2tDb250YWluZXIudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0xMWU0MmJhN1xcXCJ9IS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9GZWVkYmFja0NvbnRhaW5lci52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXGNwYW5lbFxcXFxjb21wb25lbnRzXFxcXEZlZWRiYWNrQ29udGFpbmVyLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEZlZWRiYWNrQ29udGFpbmVyLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0xMWU0MmJhN1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTExZTQyYmE3XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvRmVlZGJhY2tDb250YWluZXIudnVlXG4vLyBtb2R1bGUgaWQgPSAyMTdcbi8vIG1vZHVsZSBjaHVua3MgPSA5IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmVlZC1lbGVtZW50XCJcbiAgfSwgW19jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInB1bGwtbGVmdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCIjXCJcbiAgICB9XG4gIH0sIFtfYygnaW1nJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImltZy1jaXJjbGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJhbHRcIjogXCJpbWFnZVwiLFxuICAgICAgXCJzcmNcIjogX3ZtLmZlZWRiYWNrLnVzZXIuYXZhdGFyXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1lZGlhLWJvZHkgXCJcbiAgfSwgW19jKCdzbWFsbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwdWxsLXJpZ2h0XCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLnRvSHVtYW5BcHByb3ZlZEF0KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3N0cm9uZycsIFtfdm0uX3YoX3ZtLl9zKF92bS51c2VyKSldKSwgX3ZtLl92KFwiIHBvc3RlZCBhIFwiKSwgX2MoJ3N0cm9uZycsIFtfdm0uX3YoXCJmZWVkYmFja1wiKV0pLCBfdm0uX3YoXCIgLiBcIiksIF9jKCdicicpLCBfdm0uX3YoXCIgXCIpLCBfYygnc21hbGwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1tdXRlZFwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJpbm5lckhUTUxcIjogX3ZtLl9zKF92bS5wcm9jZXNzb3JXaXRoTGFiZWwpXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFsoX3ZtLmZlZWRiYWNrLmltYWdlKSA/IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTNcIlxuICB9LCBbX2MoJ2ltZycsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInNyY1wiOiBfdm0uZmVlZGJhY2suaW1hZ2VfcGF0aCArIFwiZmVlZGJhY2suanBnXCJcbiAgICB9XG4gIH0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBjbGFzczogX3ZtLmZlZWRiYWNrLmltYWdlID8gJ2NvbC1tZC05JyA6ICdjb2wtbWQtMTInXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIndlbGxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0XFx0XCIgKyBfdm0uX3MoX3ZtLmZlZWRiYWNrLmNvbnRlbnQpICsgXCJcXG5cXHRcXHRcXHRcXHRcIildKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHVsbC1yaWdodFwiXG4gIH0sIFsoX3ZtLmZlZWRiYWNrLmFwcHJvdmVkID09IDApID8gX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1zbSBidG4tZGFuZ2VyXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5wcm9jZXNzKF92bS5mZWVkYmFjay5pZCwgMiwgXCJwZW5kaW5nXCIsIFwiUmVqZWN0ZWQhXCIpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtdGltZXNcIlxuICB9KSwgX3ZtLl92KFwiIFJlamVjdFwiKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uZmVlZGJhY2suYXBwcm92ZWQgPT0gMCkgPyBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXNtIGJ0bi1wcmltYXJ5XCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5wcm9jZXNzKF92bS5mZWVkYmFjay5pZCwgMSwgXCJwZW5kaW5nXCIsIFwiQXBwcm92ZWQhXCIpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2hlY2tcIlxuICB9KSwgX3ZtLl92KFwiIEFwcHJvdmVcIildKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmZlZWRiYWNrLmFwcHJvdmVkICE9IDApID8gX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1zbSBidG4td2FybmluZ1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0ucHJvY2Vzcyhfdm0uZmVlZGJhY2suaWQsIDAsIF92bS5mZWVkYmFjay5hcHByb3ZlZCA9PSAyID8gXCJyZWplY3RlZFwiIDogXCJhY2NlcHRlZFwiLCBcIlJlc3RvcmVkIHRvIFBlbmRpbmchXCIpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2hlY2tcIlxuICB9KSwgX3ZtLl92KFwiIFJlc3RvcmVcIildKSA6IF92bS5fZSgpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTExZTQyYmE3XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMTFlNDJiYTdcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvRmVlZGJhY2tDb250YWluZXIudnVlXG4vLyBtb2R1bGUgaWQgPSAyMjlcbi8vIG1vZHVsZSBjaHVua3MgPSA5Il0sInNvdXJjZVJvb3QiOiIifQ==