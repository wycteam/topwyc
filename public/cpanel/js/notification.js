/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 260);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WycNotification_vue__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WycNotification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_WycNotification_vue__);


new Vue({
    el: '#notifications',

    data: {
        notifications: []
    },

    methods: {
        getNotifications: function getNotifications() {
            var _this = this;

            return axiosAPIv1.get('/notifications').then(function (result) {
                _this.notifications = result.data;
            });
        },
        markAllAsSeen: function markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (!notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });

            return axiosAPIv1.post('/notifications/mark-all-as-seen');
        },
        removeNotif: function removeNotif(data) {
            var index = this.notifications.indexOf(data);

            this.notifications.splice(index, 1);
        }
    },

    computed: {
        count: function count() {
            var count = 0;

            if (this.notifications) {
                this.notifications.forEach(function (notif) {
                    if (!notif.seen_at) {
                        ++count;
                    }
                });
            }

            return count || '';
        }
    },

    components: {
        WycNotification: __WEBPACK_IMPORTED_MODULE_0__components_WycNotification_vue___default.a
    },

    created: function created() {
        this.getNotifications();

        Event.listen('new-feedback', this.getNotifications);
        Event.listen('new-issue', this.getNotifications);
        Event.listen('new-ewallet-transaction', this.getNotifications);
        Event.listen('issue-assignment', this.getNotifications);
        Event.listen('new-document', this.getNotifications);
        Event.listen('new-ewallet-transaction', this.getNotifications);
        Event.listen('notification.remove', this.removeNotif);
    },
    mounted: function mounted() {
        $(this.$el).find('.panel-body').slimScroll({
            height: '300px'
        });
    }
});

/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['notification'],

    methods: {
        action: function action() {
            var data = this.notification.data;

            if (!this.notification.read_at) {
                axiosAPIv1.post('/notifications/' + this.notification.id + '/mark-as-read');
            }

            if (this.type === 'new-feedback') {
                window.location.href = '/feedback';
            } else if (this.type === 'issue-assignment') {
                window.location.href = '/customer-service/' + data.issue.id;
            } else if (this.type === 'new-document') {
                window.location.href = '/documents';
            } else if (this.type === 'new-issue') {
                window.location.href = '/customer-service/' + data.issue.id;
            } else if (this.type === 'change-ewallet-transaction-status' || this.type === 'new-ewallet-transaction') {
                window.location.href = '/ewallet';
            }
        },
        feedbackApprove: function feedbackApprove() {
            var _this = this;

            axiosAPIv1.post('/feedbacks/' + this.notification.data.feedback.id + '/approve', {
                notification_id: this.notification.id
            }).then(function (result) {
                Event.fire('notification.remove', _this.notification);
            });

            $(this.$el).slideUp(function () {
                $(this).remove();
            });
        },
        feedbackReject: function feedbackReject() {
            var _this2 = this;

            axiosAPIv1.post('/feedbacks/' + this.notification.data.feedback.id + '/reject', {
                notification_id: this.notification.id
            }).then(function (result) {
                Event.fire('notification.remove', _this2.notification);
            });

            $(this.$el).slideUp(function () {
                $(this).remove();
            });
        }
    },

    computed: {
        notRead: function notRead() {
            return !this.notification.read_at;
        },
        fromNow: function fromNow() {
            return this.notification.human_created_at;
        },
        type: function type() {
            var arrType = this.notification.type.split('\\');
            var type = arrType[arrType.length - 1].replace(/([a-z](?=[A-Z]))/g, '$1-').toLowerCase();

            return type;
        }
    },

    mounted: function mounted() {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    }
});

/***/ }),

/***/ 226:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(190),
  /* template */
  __webpack_require__(239),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\cpanel\\components\\WycNotification.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] WycNotification.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7611b737", Component.options)
  } else {
    hotAPI.reload("data-v-7611b737", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 239:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', {
    staticClass: "clearfix",
    class: {
      'not-read': _vm.notRead
    },
    on: {
      "click": _vm.action
    }
  }, [_c('div', {
    staticClass: "notif-image"
  }, [_c('img', {
    attrs: {
      "src": _vm.notification.data.image
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "notif-content"
  }, [_c('div', {
    staticClass: "ellipsis"
  }, [_c('strong', [_vm._v(_vm._s(_vm.notification.data.title))])]), _vm._v(" "), _c('div', {
    staticClass: "ellipsis",
    attrs: {
      "data-toggle": "tooltip",
      "data-placement": "left",
      "data-container": "body",
      "title": _vm.notification.data.body
    }
  }, [_vm._v(_vm._s(_vm.notification.data.body))]), _vm._v(" "), _c('div', [_c('small', [_vm._v(_vm._s(_vm.fromNow))])])]), _vm._v(" "), (_vm.type === 'new-feedback') ? _c('div', {
    staticClass: "notif-action"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-xs btn-block",
    on: {
      "click": function($event) {
        $event.stopPropagation();
        _vm.feedbackApprove($event)
      }
    }
  }, [_vm._v("Approve")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-xs btn-block",
    on: {
      "click": function($event) {
        $event.stopPropagation();
        _vm.feedbackReject($event)
      }
    }
  }, [_vm._v("Reject")])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7611b737", module.exports)
  }
}

/***/ }),

/***/ 260:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(138);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDE/NTk0YSoqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9ub3RpZmljYXRpb24uanMiLCJ3ZWJwYWNrOi8vL1d5Y05vdGlmaWNhdGlvbi52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9XeWNOb3RpZmljYXRpb24udnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljTm90aWZpY2F0aW9uLnZ1ZT82YWI1Il0sIm5hbWVzIjpbIlZ1ZSIsImVsIiwiZGF0YSIsIm5vdGlmaWNhdGlvbnMiLCJtZXRob2RzIiwiZ2V0Tm90aWZpY2F0aW9ucyIsImF4aW9zQVBJdjEiLCJnZXQiLCJ0aGVuIiwicmVzdWx0IiwibWFya0FsbEFzU2VlbiIsImZvckVhY2giLCJub3RpZiIsInNlZW5fYXQiLCJEYXRlIiwicG9zdCIsInJlbW92ZU5vdGlmIiwiaW5kZXgiLCJpbmRleE9mIiwic3BsaWNlIiwiY29tcHV0ZWQiLCJjb3VudCIsImNvbXBvbmVudHMiLCJXeWNOb3RpZmljYXRpb24iLCJjcmVhdGVkIiwiRXZlbnQiLCJsaXN0ZW4iLCJtb3VudGVkIiwiJCIsIiRlbCIsImZpbmQiLCJzbGltU2Nyb2xsIiwiaGVpZ2h0Il0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDbERBOztBQUVBLElBQUlBLEdBQUosQ0FBUTtBQUNKQyxRQUFJLGdCQURBOztBQUdKQyxVQUFNO0FBQ0ZDLHVCQUFlO0FBRGIsS0FIRjs7QUFPSkMsYUFBUztBQUNMQyx3QkFESyw4QkFDYztBQUFBOztBQUNmLG1CQUFPQyxXQUFXQyxHQUFYLENBQWUsZ0JBQWYsRUFDRkMsSUFERSxDQUNHLGtCQUFVO0FBQ1osc0JBQUtMLGFBQUwsR0FBcUJNLE9BQU9QLElBQTVCO0FBQ0gsYUFIRSxDQUFQO0FBSUgsU0FOSTtBQVFMUSxxQkFSSywyQkFRVztBQUNaLGlCQUFLUCxhQUFMLENBQW1CUSxPQUFuQixDQUEyQixVQUFVQyxLQUFWLEVBQWlCO0FBQ3hDLG9CQUFJLENBQUVBLE1BQU1DLE9BQVosRUFBcUI7QUFDakJELDBCQUFNQyxPQUFOLEdBQWdCLElBQUlDLElBQUosRUFBaEI7QUFDSDtBQUNKLGFBSkQ7O0FBTUEsbUJBQU9SLFdBQVdTLElBQVgsQ0FBZ0IsaUNBQWhCLENBQVA7QUFDSCxTQWhCSTtBQWtCTEMsbUJBbEJLLHVCQWtCT2QsSUFsQlAsRUFrQmE7QUFDZCxnQkFBSWUsUUFBUSxLQUFLZCxhQUFMLENBQW1CZSxPQUFuQixDQUEyQmhCLElBQTNCLENBQVo7O0FBRUEsaUJBQUtDLGFBQUwsQ0FBbUJnQixNQUFuQixDQUEwQkYsS0FBMUIsRUFBaUMsQ0FBakM7QUFDSDtBQXRCSSxLQVBMOztBQWdDSkcsY0FBVTtBQUNOQyxhQURNLG1CQUNFO0FBQ0osZ0JBQUlBLFFBQVEsQ0FBWjs7QUFFQSxnQkFBSSxLQUFLbEIsYUFBVCxFQUF3QjtBQUNwQixxQkFBS0EsYUFBTCxDQUFtQlEsT0FBbkIsQ0FBMkIsVUFBVUMsS0FBVixFQUFpQjtBQUN4Qyx3QkFBSSxDQUFFQSxNQUFNQyxPQUFaLEVBQXFCO0FBQ2pCLDBCQUFFUSxLQUFGO0FBQ0g7QUFDSixpQkFKRDtBQUtIOztBQUVELG1CQUFPQSxTQUFTLEVBQWhCO0FBQ0g7QUFiSyxLQWhDTjs7QUFnREpDLGdCQUFZO0FBQ1JDLHlCQUFBLHVFQUFBQTtBQURRLEtBaERSOztBQW9ESkMsV0FwREkscUJBb0RNO0FBQ04sYUFBS25CLGdCQUFMOztBQUVBb0IsY0FBTUMsTUFBTixDQUFhLGNBQWIsRUFBNkIsS0FBS3JCLGdCQUFsQztBQUNBb0IsY0FBTUMsTUFBTixDQUFhLFdBQWIsRUFBMEIsS0FBS3JCLGdCQUEvQjtBQUNBb0IsY0FBTUMsTUFBTixDQUFhLHlCQUFiLEVBQXdDLEtBQUtyQixnQkFBN0M7QUFDQW9CLGNBQU1DLE1BQU4sQ0FBYSxrQkFBYixFQUFpQyxLQUFLckIsZ0JBQXRDO0FBQ0FvQixjQUFNQyxNQUFOLENBQWEsY0FBYixFQUE2QixLQUFLckIsZ0JBQWxDO0FBQ0FvQixjQUFNQyxNQUFOLENBQWEseUJBQWIsRUFBd0MsS0FBS3JCLGdCQUE3QztBQUNBb0IsY0FBTUMsTUFBTixDQUFhLHFCQUFiLEVBQW9DLEtBQUtWLFdBQXpDO0FBQ0gsS0E5REc7QUFnRUpXLFdBaEVJLHFCQWdFTTtBQUNOQyxVQUFFLEtBQUtDLEdBQVAsRUFBWUMsSUFBWixDQUFpQixhQUFqQixFQUNLQyxVQURMLENBQ2dCO0FBQ1JDLG9CQUFRO0FBREEsU0FEaEI7QUFJSDtBQXJFRyxDQUFSLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2dCQTtZQUdBOzs7a0NBRUE7eUNBRUE7OzRDQUNBOzJFQUNBO0FBRUE7OzhDQUNBO3VDQUNBO3lEQUNBO3lFQUNBO3FEQUNBO3VDQUNBO2tEQUNBO3lFQUNBO3FIQUNBO3VDQUNBO0FBQ0E7QUFFQTs7QUFDQTs7O21EQUVBO0FBREEsc0NBRUE7d0RBQ0E7QUFFQTs7NENBQ0E7d0JBQ0E7QUFDQTtBQUVBOztBQUNBOzs7bURBRUE7QUFEQSxzQ0FFQTt5REFDQTtBQUVBOzs0Q0FDQTt3QkFDQTtBQUNBO0FBR0E7QUE3Q0E7OztvQ0ErQ0E7c0NBQ0E7QUFFQTtvQ0FDQTtxQ0FDQTtBQUVBOzhCQUNBO3VEQUNBO3VGQUVBOzttQkFDQTtBQUdBO0FBaEJBOztnQ0FpQkE7c0NBQ0E7eUNBQ0E7QUFDQTtBQUNBO0FBdEVBLEc7Ozs7Ozs7QUNuQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUE0RztBQUM1RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL25vdGlmaWNhdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMjYwKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBiMjBhMmEwYjI5NTZhOWQ2YzBkMSIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNSA2IDcgOCA5IDEwIDExIDEyIiwiaW1wb3J0IFd5Y05vdGlmaWNhdGlvbiBmcm9tICcuL2NvbXBvbmVudHMvV3ljTm90aWZpY2F0aW9uLnZ1ZSdcclxuXHJcbm5ldyBWdWUoe1xyXG4gICAgZWw6ICcjbm90aWZpY2F0aW9ucycsXHJcblxyXG4gICAgZGF0YToge1xyXG4gICAgICAgIG5vdGlmaWNhdGlvbnM6IFtdXHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXROb3RpZmljYXRpb25zKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9ub3RpZmljYXRpb25zJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBtYXJrQWxsQXNTZWVuKCkge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbnMuZm9yRWFjaChmdW5jdGlvbiAobm90aWYpIHtcclxuICAgICAgICAgICAgICAgIGlmICghIG5vdGlmLnNlZW5fYXQpIHtcclxuICAgICAgICAgICAgICAgICAgICBub3RpZi5zZWVuX2F0ID0gbmV3IERhdGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvbm90aWZpY2F0aW9ucy9tYXJrLWFsbC1hcy1zZWVuJyk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgcmVtb3ZlTm90aWYoZGF0YSkge1xyXG4gICAgICAgICAgICB2YXIgaW5kZXggPSB0aGlzLm5vdGlmaWNhdGlvbnMuaW5kZXhPZihkYXRhKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBjb3VudCgpIHtcclxuICAgICAgICAgICAgbGV0IGNvdW50ID0gMDtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLm5vdGlmaWNhdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucy5mb3JFYWNoKGZ1bmN0aW9uIChub3RpZikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghIG5vdGlmLnNlZW5fYXQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgKytjb3VudDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGNvdW50IHx8ICcnO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAgIFd5Y05vdGlmaWNhdGlvblxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucygpO1xyXG5cclxuICAgICAgICBFdmVudC5saXN0ZW4oJ25ldy1mZWVkYmFjaycsIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucyk7XHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCduZXctaXNzdWUnLCB0aGlzLmdldE5vdGlmaWNhdGlvbnMpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignbmV3LWV3YWxsZXQtdHJhbnNhY3Rpb24nLCB0aGlzLmdldE5vdGlmaWNhdGlvbnMpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignaXNzdWUtYXNzaWdubWVudCcsIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucyk7XHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCduZXctZG9jdW1lbnQnLCB0aGlzLmdldE5vdGlmaWNhdGlvbnMpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignbmV3LWV3YWxsZXQtdHJhbnNhY3Rpb24nLCB0aGlzLmdldE5vdGlmaWNhdGlvbnMpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3Rlbignbm90aWZpY2F0aW9uLnJlbW92ZScsIHRoaXMucmVtb3ZlTm90aWYpO1xyXG4gICAgfSxcclxuXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgICQodGhpcy4kZWwpLmZpbmQoJy5wYW5lbC1ib2R5JylcclxuICAgICAgICAgICAgLnNsaW1TY3JvbGwoe1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAnMzAwcHgnXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59KTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvbm90aWZpY2F0aW9uLmpzIiwiPHRlbXBsYXRlPlxyXG48bGkgQGNsaWNrPVwiYWN0aW9uXCIgY2xhc3M9XCJjbGVhcmZpeFwiIDpjbGFzcz1cInsgJ25vdC1yZWFkJzogbm90UmVhZCB9XCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwibm90aWYtaW1hZ2VcIj5cclxuICAgICAgICA8aW1nIDpzcmM9XCJub3RpZmljYXRpb24uZGF0YS5pbWFnZVwiPlxyXG4gICAgPC9kaXY+XHJcbiAgICA8ZGl2IGNsYXNzPVwibm90aWYtY29udGVudFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJlbGxpcHNpc1wiPjxzdHJvbmc+e3sgbm90aWZpY2F0aW9uLmRhdGEudGl0bGUgfX08L3N0cm9uZz48L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiZWxsaXBzaXNcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiBkYXRhLXBsYWNlbWVudD1cImxlZnRcIiBkYXRhLWNvbnRhaW5lcj1cImJvZHlcIiA6dGl0bGU9XCJub3RpZmljYXRpb24uZGF0YS5ib2R5XCI+e3sgbm90aWZpY2F0aW9uLmRhdGEuYm9keSB9fTwvZGl2PlxyXG4gICAgICAgIDxkaXY+PHNtYWxsPnt7IGZyb21Ob3cgfX08L3NtYWxsPjwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgICA8ZGl2IHYtaWY9XCJ0eXBlID09PSAnbmV3LWZlZWRiYWNrJ1wiIGNsYXNzPVwibm90aWYtYWN0aW9uXCI+XHJcbiAgICAgICAgPGJ1dHRvbiBAY2xpY2suc3RvcD1cImZlZWRiYWNrQXBwcm92ZVwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi14cyBidG4tYmxvY2tcIj5BcHByb3ZlPC9idXR0b24+XHJcbiAgICAgICAgPGJ1dHRvbiBAY2xpY2suc3RvcD1cImZlZWRiYWNrUmVqZWN0XCIgY2xhc3M9XCJidG4gYnRuLWRhbmdlciBidG4teHMgYnRuLWJsb2NrXCI+UmVqZWN0PC9idXR0b24+XHJcbiAgICA8L2Rpdj5cclxuPC9saT5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICAgIHByb3BzOiBbJ25vdGlmaWNhdGlvbiddLFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBhY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGxldCBkYXRhID0gdGhpcy5ub3RpZmljYXRpb24uZGF0YTtcclxuXHJcbiAgICAgICAgICAgIGlmICghIHRoaXMubm90aWZpY2F0aW9uLnJlYWRfYXQpIHtcclxuICAgICAgICAgICAgICAgIGF4aW9zQVBJdjEucG9zdCgnL25vdGlmaWNhdGlvbnMvJyArIHRoaXMubm90aWZpY2F0aW9uLmlkICsgJy9tYXJrLWFzLXJlYWQnKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMudHlwZSA9PT0gJ25ldy1mZWVkYmFjaycpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9mZWVkYmFjayc7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy50eXBlID09PSAnaXNzdWUtYXNzaWdubWVudCcpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9jdXN0b21lci1zZXJ2aWNlLycgKyBkYXRhLmlzc3VlLmlkO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudHlwZSA9PT0gJ25ldy1kb2N1bWVudCcpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9kb2N1bWVudHMnO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudHlwZSA9PT0gJ25ldy1pc3N1ZScpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9jdXN0b21lci1zZXJ2aWNlLycgKyBkYXRhLmlzc3VlLmlkO1xyXG4gICAgICAgICAgICB9ZWxzZSBpZiAodGhpcy50eXBlID09PSAnY2hhbmdlLWV3YWxsZXQtdHJhbnNhY3Rpb24tc3RhdHVzJ3x8IHRoaXMudHlwZSA9PT0gJ25ldy1ld2FsbGV0LXRyYW5zYWN0aW9uJykge1xyXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSAnL2V3YWxsZXQnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZmVlZGJhY2tBcHByb3ZlKCkge1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLnBvc3QoJy9mZWVkYmFja3MvJyArIHRoaXMubm90aWZpY2F0aW9uLmRhdGEuZmVlZGJhY2suaWQgKyAnL2FwcHJvdmUnLCB7XHJcbiAgICAgICAgICAgICAgICBub3RpZmljYXRpb25faWQ6IHRoaXMubm90aWZpY2F0aW9uLmlkXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIEV2ZW50LmZpcmUoJ25vdGlmaWNhdGlvbi5yZW1vdmUnLCB0aGlzLm5vdGlmaWNhdGlvbik7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJCh0aGlzLiRlbCkuc2xpZGVVcChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBmZWVkYmFja1JlamVjdCgpIHtcclxuICAgICAgICAgICAgYXhpb3NBUEl2MS5wb3N0KCcvZmVlZGJhY2tzLycgKyB0aGlzLm5vdGlmaWNhdGlvbi5kYXRhLmZlZWRiYWNrLmlkICsgJy9yZWplY3QnLCB7XHJcbiAgICAgICAgICAgICAgICBub3RpZmljYXRpb25faWQ6IHRoaXMubm90aWZpY2F0aW9uLmlkXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIEV2ZW50LmZpcmUoJ25vdGlmaWNhdGlvbi5yZW1vdmUnLCB0aGlzLm5vdGlmaWNhdGlvbik7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJCh0aGlzLiRlbCkuc2xpZGVVcChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgbm90UmVhZCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuICEgdGhpcy5ub3RpZmljYXRpb24ucmVhZF9hdDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBmcm9tTm93KCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpZmljYXRpb24uaHVtYW5fY3JlYXRlZF9hdDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB0eXBlKCkge1xyXG4gICAgICAgICAgICBsZXQgYXJyVHlwZSA9IHRoaXMubm90aWZpY2F0aW9uLnR5cGUuc3BsaXQoJ1xcXFwnKTtcclxuICAgICAgICAgICAgbGV0IHR5cGUgPSBhcnJUeXBlW2FyclR5cGUubGVuZ3RoIC0gMV0ucmVwbGFjZSgvKFthLXpdKD89W0EtWl0pKS9nLCAnJDEtJykudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0eXBlO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuPC9zY3JpcHQ+XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBXeWNOb3RpZmljYXRpb24udnVlPzM1MGEyMTVjIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vV3ljTm90aWZpY2F0aW9uLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNzYxMWI3MzdcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vV3ljTm90aWZpY2F0aW9uLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcY3BhbmVsXFxcXGNvbXBvbmVudHNcXFxcV3ljTm90aWZpY2F0aW9uLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFd5Y05vdGlmaWNhdGlvbi52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNzYxMWI3MzdcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi03NjExYjczN1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y05vdGlmaWNhdGlvbi52dWVcbi8vIG1vZHVsZSBpZCA9IDIyNlxuLy8gbW9kdWxlIGNodW5rcyA9IDciLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNsZWFyZml4XCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdub3QtcmVhZCc6IF92bS5ub3RSZWFkXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0uYWN0aW9uXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJub3RpZi1pbWFnZVwiXG4gIH0sIFtfYygnaW1nJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInNyY1wiOiBfdm0ubm90aWZpY2F0aW9uLmRhdGEuaW1hZ2VcbiAgICB9XG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibm90aWYtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImVsbGlwc2lzXCJcbiAgfSwgW19jKCdzdHJvbmcnLCBbX3ZtLl92KF92bS5fcyhfdm0ubm90aWZpY2F0aW9uLmRhdGEudGl0bGUpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZWxsaXBzaXNcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcbiAgICAgIFwiZGF0YS1wbGFjZW1lbnRcIjogXCJsZWZ0XCIsXG4gICAgICBcImRhdGEtY29udGFpbmVyXCI6IFwiYm9keVwiLFxuICAgICAgXCJ0aXRsZVwiOiBfdm0ubm90aWZpY2F0aW9uLmRhdGEuYm9keVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm5vdGlmaWNhdGlvbi5kYXRhLmJvZHkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2JywgW19jKCdzbWFsbCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5mcm9tTm93KSldKV0pXSksIF92bS5fdihcIiBcIiksIChfdm0udHlwZSA9PT0gJ25ldy1mZWVkYmFjaycpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJub3RpZi1hY3Rpb25cIlxuICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgYnRuLXhzIGJ0bi1ibG9ja1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgIF92bS5mZWVkYmFja0FwcHJvdmUoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihcIkFwcHJvdmVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRhbmdlciBidG4teHMgYnRuLWJsb2NrXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgX3ZtLmZlZWRiYWNrUmVqZWN0KCRldmVudClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJSZWplY3RcIildKV0pIDogX3ZtLl9lKCldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi03NjExYjczN1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTc2MTFiNzM3XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y05vdGlmaWNhdGlvbi52dWVcbi8vIG1vZHVsZSBpZCA9IDIzOVxuLy8gbW9kdWxlIGNodW5rcyA9IDciXSwic291cmNlUm9vdCI6IiJ9