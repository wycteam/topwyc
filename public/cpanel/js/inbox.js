/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 257);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WycInbox_vue__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WycInbox_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_WycInbox_vue__);


new Vue({
    el: '#inbox',

    data: {
        notifications: []
    },

    methods: {
        getNotifications: function getNotifications() {
            var _this = this;

            return axiosAPIv1.get('/messages/notifications2').then(function (result) {
                _this.notifications = result.data;
            });
        },
        markAllAsSeen: function markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (!notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });

            return axiosAPIv1.post('/messages/mark-all-as-seen');
        },
        fireNewChatMessage: function fireNewChatMessage(data) {
            Event.fire('chat-room-' + data.message.chat_room_id, data.message);
        }
    },

    computed: {
        count: function count() {
            var count = 0;

            if (this.notifications) {
                this.notifications.forEach(function (notif) {
                    if (!notif.seen_at) {
                        ++count;
                    }
                });
            }

            return count || '';
        }
    },

    components: {
        WycInbox: __WEBPACK_IMPORTED_MODULE_0__components_WycInbox_vue___default.a
    },

    created: function created() {
        var _this2 = this;

        this.getNotifications();

        Event.listen('new-chat-message', function (data) {
            _this2.getNotifications();
            _this2.fireNewChatMessage(data);
        });
    },
    mounted: function mounted() {
        $(this.$el).find('.panel-body').slimScroll({
            height: '300px'
        });
    }
});

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['notification'],

    methods: {
        action: function action() {
            axiosAPIv1.post('/notifications/' + this.notification.id + '/mark-as-read');

            if (this.notification.data.message.chat_room.name) {
                return window.location.href = '/messages/#/cr/' + this.notification.data.message.chat_room.id;
            }

            window.location.href = '/messages/#/u/' + this.notification.data.message.user_id;
        }
    },

    computed: {
        notRead: function notRead() {
            return !this.notification.read_at;
        },
        fromNow: function fromNow() {
            return moment.utc(this.notification.created_at).fromNow();
        },
        fullName: function fullName() {
            if (this.notification.chat_room.name) return this.notification.chat_room.name;else return this.notification.chat_room.users[0].first_name + ' ' + this.notification.chat_room.users[0].last_name;
        }
    }
});

/***/ }),

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(187),
  /* template */
  __webpack_require__(233),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\cpanel\\components\\WycInbox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] WycInbox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2ba16b6a", Component.options)
  } else {
    hotAPI.reload("data-v-2ba16b6a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 233:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', {
    staticClass: "clearfix",
    class: {
      'not-read': _vm.notRead
    },
    on: {
      "click": _vm.action
    }
  }, [_c('div', {
    staticClass: "notif-image"
  }, [_c('img', {
    attrs: {
      "src": _vm.notification.chat_room.users[0].avatar
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "notif-content"
  }, [_c('div', {
    staticClass: "ellipsis"
  }, [_c('strong', [_vm._v(_vm._s(_vm.fullName))])]), _vm._v(" "), _c('div', {
    staticClass: "ellipsis"
  }, [_vm._v(_vm._s(_vm.notification.body))])]), _vm._v(" "), _c('div', {
    staticClass: "notif-action"
  }, [_c('div', [_c('small', [_vm._v(_vm._s(_vm.fromNow))])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-2ba16b6a", module.exports)
  }
}

/***/ }),

/***/ 257:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(135);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDE/NTk0YSoqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9pbmJveC5qcyIsIndlYnBhY2s6Ly8vV3ljSW5ib3gudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljSW5ib3gudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvV3ljSW5ib3gudnVlPzY5OGMiXSwibmFtZXMiOlsiVnVlIiwiZWwiLCJkYXRhIiwibm90aWZpY2F0aW9ucyIsIm1ldGhvZHMiLCJnZXROb3RpZmljYXRpb25zIiwiYXhpb3NBUEl2MSIsImdldCIsInRoZW4iLCJyZXN1bHQiLCJtYXJrQWxsQXNTZWVuIiwiZm9yRWFjaCIsIm5vdGlmIiwic2Vlbl9hdCIsIkRhdGUiLCJwb3N0IiwiZmlyZU5ld0NoYXRNZXNzYWdlIiwiRXZlbnQiLCJmaXJlIiwibWVzc2FnZSIsImNoYXRfcm9vbV9pZCIsImNvbXB1dGVkIiwiY291bnQiLCJjb21wb25lbnRzIiwiV3ljSW5ib3giLCJjcmVhdGVkIiwibGlzdGVuIiwibW91bnRlZCIsIiQiLCIkZWwiLCJmaW5kIiwic2xpbVNjcm9sbCIsImhlaWdodCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xEQTs7QUFFQSxJQUFJQSxHQUFKLENBQVE7QUFDSkMsUUFBSSxRQURBOztBQUdKQyxVQUFNO0FBQ0ZDLHVCQUFlO0FBRGIsS0FIRjs7QUFPSkMsYUFBUztBQUNMQyx3QkFESyw4QkFDYztBQUFBOztBQUNmLG1CQUFPQyxXQUFXQyxHQUFYLENBQWUsMEJBQWYsRUFDRkMsSUFERSxDQUNHLGtCQUFVO0FBQ1osc0JBQUtMLGFBQUwsR0FBcUJNLE9BQU9QLElBQTVCO0FBQ0gsYUFIRSxDQUFQO0FBSUgsU0FOSTtBQVFMUSxxQkFSSywyQkFRVztBQUNaLGlCQUFLUCxhQUFMLENBQW1CUSxPQUFuQixDQUEyQixVQUFVQyxLQUFWLEVBQWlCO0FBQ3hDLG9CQUFJLENBQUVBLE1BQU1DLE9BQVosRUFBcUI7QUFDakJELDBCQUFNQyxPQUFOLEdBQWdCLElBQUlDLElBQUosRUFBaEI7QUFDSDtBQUNKLGFBSkQ7O0FBTUEsbUJBQU9SLFdBQVdTLElBQVgsQ0FBZ0IsNEJBQWhCLENBQVA7QUFDSCxTQWhCSTtBQWtCTEMsMEJBbEJLLDhCQWtCY2QsSUFsQmQsRUFrQm9CO0FBQ3JCZSxrQkFBTUMsSUFBTixDQUFXLGVBQWVoQixLQUFLaUIsT0FBTCxDQUFhQyxZQUF2QyxFQUFxRGxCLEtBQUtpQixPQUExRDtBQUNIO0FBcEJJLEtBUEw7O0FBOEJKRSxjQUFVO0FBQ05DLGFBRE0sbUJBQ0U7QUFDSixnQkFBSUEsUUFBUSxDQUFaOztBQUVBLGdCQUFJLEtBQUtuQixhQUFULEVBQXdCO0FBQ3BCLHFCQUFLQSxhQUFMLENBQW1CUSxPQUFuQixDQUEyQixVQUFVQyxLQUFWLEVBQWlCO0FBQ3hDLHdCQUFJLENBQUVBLE1BQU1DLE9BQVosRUFBcUI7QUFDakIsMEJBQUVTLEtBQUY7QUFDSDtBQUNKLGlCQUpEO0FBS0g7O0FBRUQsbUJBQU9BLFNBQVMsRUFBaEI7QUFDSDtBQWJLLEtBOUJOOztBQThDSkMsZ0JBQVk7QUFDUkMsa0JBQUEsZ0VBQUFBO0FBRFEsS0E5Q1I7O0FBa0RKQyxXQWxESSxxQkFrRE07QUFBQTs7QUFDTixhQUFLcEIsZ0JBQUw7O0FBRUFZLGNBQU1TLE1BQU4sQ0FBYSxrQkFBYixFQUFpQyxnQkFBUTtBQUNyQyxtQkFBS3JCLGdCQUFMO0FBQ0EsbUJBQUtXLGtCQUFMLENBQXdCZCxJQUF4QjtBQUNILFNBSEQ7QUFJSCxLQXpERztBQTJESnlCLFdBM0RJLHFCQTJETTtBQUNOQyxVQUFFLEtBQUtDLEdBQVAsRUFBWUMsSUFBWixDQUFpQixhQUFqQixFQUNLQyxVQURMLENBQ2dCO0FBQ1JDLG9CQUFRO0FBREEsU0FEaEI7QUFJSDtBQWhFRyxDQUFSLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZUE7WUFHQTs7O2tDQUVBO3VFQUVBOzsrREFDQTsyR0FDQTtBQUVBOztxRkFDQTtBQUdBO0FBWEE7OztvQ0FhQTtzQ0FDQTtBQUVBO29DQUNBOzREQUNBO0FBQ0E7c0NBQ0E7NENBQ0EseUNBRUEsOEdBQ0E7QUFFQTtBQWRBO0FBZkEsRzs7Ozs7OztBQ2xCQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQTRHO0FBQzVHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEMiLCJmaWxlIjoianMvaW5ib3guanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDI1Nyk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDEiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDUgNiA3IDggOSAxMCAxMSAxMiIsImltcG9ydCBXeWNJbmJveCBmcm9tICcuL2NvbXBvbmVudHMvV3ljSW5ib3gudnVlJ1xyXG5cclxubmV3IFZ1ZSh7XHJcbiAgICBlbDogJyNpbmJveCcsXHJcblxyXG4gICAgZGF0YToge1xyXG4gICAgICAgIG5vdGlmaWNhdGlvbnM6IFtdXHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXROb3RpZmljYXRpb25zKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9tZXNzYWdlcy9ub3RpZmljYXRpb25zMicpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgbWFya0FsbEFzU2VlbigpIHtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKG5vdGlmKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoISBub3RpZi5zZWVuX2F0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbm90aWYuc2Vlbl9hdCA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEucG9zdCgnL21lc3NhZ2VzL21hcmstYWxsLWFzLXNlZW4nKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBmaXJlTmV3Q2hhdE1lc3NhZ2UoZGF0YSkge1xyXG4gICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LXJvb20tJyArIGRhdGEubWVzc2FnZS5jaGF0X3Jvb21faWQsIGRhdGEubWVzc2FnZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGNvdW50KCkge1xyXG4gICAgICAgICAgICBsZXQgY291bnQgPSAwO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMubm90aWZpY2F0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKG5vdGlmKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEgbm90aWYuc2Vlbl9hdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICArK2NvdW50O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gY291bnQgfHwgJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgV3ljSW5ib3hcclxuICAgIH0sXHJcblxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgICB0aGlzLmdldE5vdGlmaWNhdGlvbnMoKTtcclxuXHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCduZXctY2hhdC1tZXNzYWdlJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucygpO1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVOZXdDaGF0TWVzc2FnZShkYXRhKTtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICAkKHRoaXMuJGVsKS5maW5kKCcucGFuZWwtYm9keScpXHJcbiAgICAgICAgICAgIC5zbGltU2Nyb2xsKHtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogJzMwMHB4J1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufSk7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2luYm94LmpzIiwiXHJcbjx0ZW1wbGF0ZT5cclxuPGxpIEBjbGljaz1cImFjdGlvblwiIGNsYXNzPVwiY2xlYXJmaXhcIiA6Y2xhc3M9XCJ7ICdub3QtcmVhZCc6IG5vdFJlYWQgfVwiPlxyXG4gICAgPGRpdiBjbGFzcz1cIm5vdGlmLWltYWdlXCI+XHJcbiAgICAgICAgPGltZyA6c3JjPVwibm90aWZpY2F0aW9uLmNoYXRfcm9vbS51c2Vyc1swXS5hdmF0YXJcIj5cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBjbGFzcz1cIm5vdGlmLWNvbnRlbnRcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiZWxsaXBzaXNcIj48c3Ryb25nPnt7IGZ1bGxOYW1lIH19PC9zdHJvbmc+PC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImVsbGlwc2lzXCI+e3sgbm90aWZpY2F0aW9uLmJvZHkgfX08L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBjbGFzcz1cIm5vdGlmLWFjdGlvblwiPlxyXG4gICAgICAgIDxkaXY+PHNtYWxsPnt7IGZyb21Ob3cgfX08L3NtYWxsPjwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbjwvbGk+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczogWydub3RpZmljYXRpb24nXSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgYWN0aW9uKCkge1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLnBvc3QoJy9ub3RpZmljYXRpb25zLycgKyB0aGlzLm5vdGlmaWNhdGlvbi5pZCArICcvbWFyay1hcy1yZWFkJyk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5ub3RpZmljYXRpb24uZGF0YS5tZXNzYWdlLmNoYXRfcm9vbS5uYW1lKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gd2luZG93LmxvY2F0aW9uLmhyZWYgPSAnL21lc3NhZ2VzLyMvY3IvJyArIHRoaXMubm90aWZpY2F0aW9uLmRhdGEubWVzc2FnZS5jaGF0X3Jvb20uaWQ7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9tZXNzYWdlcy8jL3UvJyArIHRoaXMubm90aWZpY2F0aW9uLmRhdGEubWVzc2FnZS51c2VyX2lkO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBub3RSZWFkKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gISB0aGlzLm5vdGlmaWNhdGlvbi5yZWFkX2F0O1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGZyb21Ob3coKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBtb21lbnQudXRjKHRoaXMubm90aWZpY2F0aW9uLmNyZWF0ZWRfYXQpLmZyb21Ob3coKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZ1bGxOYW1lKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5ub3RpZmljYXRpb24uY2hhdF9yb29tLm5hbWUpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpZmljYXRpb24uY2hhdF9yb29tLm5hbWU7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLm5vdGlmaWNhdGlvbi5jaGF0X3Jvb20udXNlcnNbMF0uZmlyc3RfbmFtZSArICcgJyArIHRoaXMubm90aWZpY2F0aW9uLmNoYXRfcm9vbS51c2Vyc1swXS5sYXN0X25hbWU7IFxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFd5Y0luYm94LnZ1ZT80NTQ2YzNmMiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1d5Y0luYm94LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMmJhMTZiNmFcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vV3ljSW5ib3gudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxjcGFuZWxcXFxcY29tcG9uZW50c1xcXFxXeWNJbmJveC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBXeWNJbmJveC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMmJhMTZiNmFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0yYmExNmI2YVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1d5Y0luYm94LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjIzXG4vLyBtb2R1bGUgY2h1bmtzID0gOCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2xlYXJmaXhcIixcbiAgICBjbGFzczoge1xuICAgICAgJ25vdC1yZWFkJzogX3ZtLm5vdFJlYWRcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS5hY3Rpb25cbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm5vdGlmLWltYWdlXCJcbiAgfSwgW19jKCdpbWcnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5ub3RpZmljYXRpb24uY2hhdF9yb29tLnVzZXJzWzBdLmF2YXRhclxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJub3RpZi1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZWxsaXBzaXNcIlxuICB9LCBbX2MoJ3N0cm9uZycsIFtfdm0uX3YoX3ZtLl9zKF92bS5mdWxsTmFtZSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJlbGxpcHNpc1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5ub3RpZmljYXRpb24uYm9keSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJub3RpZi1hY3Rpb25cIlxuICB9LCBbX2MoJ2RpdicsIFtfYygnc21hbGwnLCBbX3ZtLl92KF92bS5fcyhfdm0uZnJvbU5vdykpXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMmJhMTZiNmFcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0yYmExNmI2YVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9XeWNJbmJveC52dWVcbi8vIG1vZHVsZSBpZCA9IDIzM1xuLy8gbW9kdWxlIGNodW5rcyA9IDgiXSwic291cmNlUm9vdCI6IiJ9