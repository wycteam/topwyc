/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 489);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 199:
/***/ (function(module, exports, __webpack_require__) {


Vue.component('dialog-modal', __webpack_require__(5));

var Tracking = new Vue({
    el: '#tracking',
    data: {
        keyword: '',
        check: 0,
        count: 0,
        services: [],
        groups: [],
        reports: [],
        process: '',
        serviceCost: [],
        compute: []
    },
    methods: {
        searchEnter: function searchEnter(e) {
            var _this = this;

            if (this.keyword != "") {
                var key = this.keyword;
                var upper = key.toUpperCase();
                var keyword = upper.substring(0, 2);
                // alert(keyword);
                if (e.keyCode === 13) {
                    if (keyword == 'GL') {
                        this.check = 1;

                        axiosAPIv1.get('/visa/tracking/group/' + this.keyword).then(function (response) {
                            _this.groups = response.data.groups[0];
                            _this.compute = response.data;
                            _this.count = response.data.groups.length;
                            _this.computeServiceCost(_this.groups.groupmember);
                            _this.balance = parseFloat(_this.compute.package_deposit) + parseFloat(_this.compute.package_payment) + parseFloat(_this.compute.package_discount) - parseFloat(_this.compute.package_refund) - parseFloat(_this.compute.total_cost);
                        });
                    } else {
                        this.check = 0;

                        axiosAPIv1.get('/visa/tracking/service/' + this.keyword).then(function (result) {
                            _this.services = result.data;
                            _this.count = _this.services.services.length;
                            _this.balance = parseFloat(_this.services.package_deposit) + parseFloat(_this.services.package_payment) + parseFloat(_this.services.package_discount) - parseFloat(_this.services.package_refund) - parseFloat(_this.services.package_cost);
                        });
                    }
                }
            } else {
                this.count = 0;
            }
        },
        searchBtn: function searchBtn() {
            var _this2 = this;

            if (this.keyword != "") {
                var key = this.keyword;
                var upper = key.toUpperCase();
                var keyword = upper.substring(0, 2);
                if (keyword == 'GL') {
                    this.check = 1;

                    axiosAPIv1.get('/visa/tracking/group/' + this.keyword).then(function (response) {
                        _this2.groups = response.data.groups[0];
                        _this2.compute = response.data;
                        _this2.count = response.data.groups.length;
                        _this2.computeServiceCost(_this2.groups.groupmember);
                        _this2.balance = parseFloat(_this2.compute.package_deposit) + parseFloat(_this2.compute.package_payment) + parseFloat(_this2.compute.package_discount) - parseFloat(_this2.compute.package_refund) - parseFloat(_this2.compute.total_cost);
                    });
                } else {
                    this.check = 0;

                    axiosAPIv1.get('/visa/tracking/service/' + this.keyword).then(function (result) {
                        _this2.services = result.data;
                        _this2.count = _this2.services.services.length;
                        _this2.balance = parseFloat(_this2.services.package_deposit) + parseFloat(_this2.services.package_payment) + parseFloat(_this2.services.package_discount) - parseFloat(_this2.services.package_refund) - parseFloat(_this2.services.package_cost);
                    });
                }
            } else {
                this.count = 0;
            }
        },
        showServices: function showServices(id, e) {
            if (e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#client-' + id).toggleClass('hide');
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },
        computeServiceCost: function computeServiceCost(members) {
            var _this3 = this;

            this.serviceCost = members.map(function (member) {
                var cost = 0;
                var charge = 0;
                var tip = 0;

                member.services.map(function (service) {
                    if (service.active == 1 && service.group_id == _this3.groups.id) {
                        cost += parseFloat(service.cost);
                        charge += parseFloat(service.charge);
                        tip += parseFloat(service.tip);
                    }
                });

                return cost + charge + tip;
            });
        },
        report: function report(id) {
            var _this4 = this;

            axiosAPIv1.get('/visa/tracking/report/' + id).then(function (response) {
                _this4.reports = response.data;
                if (_this4.reports.length != 0) $('#reports').modal('toggle');
            });
        }
    },
    created: function created() {
        this.process = 'process';
        $('body').popover({ // Ok
            html: true,
            trigger: 'hover',
            selector: '[data-toggle="popover"]'
        });
    }
});

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 489:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(199);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS90cmFja2luZy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vRGlhbG9nTW9kYWwudnVlP2U0NzQqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT9mZTFiKioqKioiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsIlRyYWNraW5nIiwiZWwiLCJkYXRhIiwia2V5d29yZCIsImNoZWNrIiwiY291bnQiLCJzZXJ2aWNlcyIsImdyb3VwcyIsInJlcG9ydHMiLCJwcm9jZXNzIiwic2VydmljZUNvc3QiLCJjb21wdXRlIiwibWV0aG9kcyIsInNlYXJjaEVudGVyIiwiZSIsImtleSIsInVwcGVyIiwidG9VcHBlckNhc2UiLCJzdWJzdHJpbmciLCJrZXlDb2RlIiwiYXhpb3NBUEl2MSIsImdldCIsInRoZW4iLCJyZXNwb25zZSIsImxlbmd0aCIsImNvbXB1dGVTZXJ2aWNlQ29zdCIsImdyb3VwbWVtYmVyIiwiYmFsYW5jZSIsInBhcnNlRmxvYXQiLCJwYWNrYWdlX2RlcG9zaXQiLCJwYWNrYWdlX3BheW1lbnQiLCJwYWNrYWdlX2Rpc2NvdW50IiwicGFja2FnZV9yZWZ1bmQiLCJ0b3RhbF9jb3N0IiwicmVzdWx0IiwicGFja2FnZV9jb3N0Iiwic2VhcmNoQnRuIiwic2hvd1NlcnZpY2VzIiwiaWQiLCJ0YXJnZXQiLCJub2RlTmFtZSIsIiQiLCJ0b2dnbGVDbGFzcyIsIm1lbWJlcnMiLCJtYXAiLCJjb3N0IiwiY2hhcmdlIiwidGlwIiwibWVtYmVyIiwic2VydmljZSIsImFjdGl2ZSIsImdyb3VwX2lkIiwicmVwb3J0IiwibW9kYWwiLCJjcmVhdGVkIiwicG9wb3ZlciIsImh0bWwiLCJ0cmlnZ2VyIiwic2VsZWN0b3IiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUNqREFBLElBQUlDLFNBQUosQ0FBYyxjQUFkLEVBQStCQyxtQkFBT0EsQ0FBQyxDQUFSLENBQS9COztBQUVBLElBQUlDLFdBQVcsSUFBSUgsR0FBSixDQUFRO0FBQ25CSSxRQUFHLFdBRGdCO0FBRW5CQyxVQUFLO0FBQ0RDLGlCQUFRLEVBRFA7QUFFREMsZUFBTSxDQUZMO0FBR0RDLGVBQU0sQ0FITDtBQUlEQyxrQkFBUyxFQUpSO0FBS0RDLGdCQUFPLEVBTE47QUFNREMsaUJBQVEsRUFOUDtBQU9EQyxpQkFBUSxFQVBQO0FBUURDLHFCQUFZLEVBUlg7QUFTREMsaUJBQVE7QUFUUCxLQUZjO0FBYW5CQyxhQUFTO0FBQ0xDLG1CQURLLHVCQUNPQyxDQURQLEVBQ1M7QUFBQTs7QUFDVixnQkFBRyxLQUFLWCxPQUFMLElBQWMsRUFBakIsRUFBb0I7QUFDaEIsb0JBQUlZLE1BQU0sS0FBS1osT0FBZjtBQUNBLG9CQUFJYSxRQUFRRCxJQUFJRSxXQUFKLEVBQVo7QUFDQSxvQkFBSWQsVUFBVWEsTUFBTUUsU0FBTixDQUFnQixDQUFoQixFQUFtQixDQUFuQixDQUFkO0FBQ0E7QUFDQSxvQkFBSUosRUFBRUssT0FBRixLQUFjLEVBQWxCLEVBQXNCO0FBQ2xCLHdCQUFHaEIsV0FBUyxJQUFaLEVBQWlCO0FBQ2IsNkJBQUtDLEtBQUwsR0FBYSxDQUFiOztBQUVBZ0IsbUNBQVdDLEdBQVgsQ0FBZSwwQkFBd0IsS0FBS2xCLE9BQTVDLEVBQ0NtQixJQURELENBQ00sb0JBQVk7QUFDZCxrQ0FBS2YsTUFBTCxHQUFjZ0IsU0FBU3JCLElBQVQsQ0FBY0ssTUFBZCxDQUFxQixDQUFyQixDQUFkO0FBQ0Esa0NBQUtJLE9BQUwsR0FBZVksU0FBU3JCLElBQXhCO0FBQ0Esa0NBQUtHLEtBQUwsR0FBYWtCLFNBQVNyQixJQUFULENBQWNLLE1BQWQsQ0FBcUJpQixNQUFsQztBQUNBLGtDQUFLQyxrQkFBTCxDQUF3QixNQUFLbEIsTUFBTCxDQUFZbUIsV0FBcEM7QUFDQSxrQ0FBS0MsT0FBTCxHQUFrQkMsV0FBVyxNQUFLakIsT0FBTCxDQUFha0IsZUFBeEIsSUFBeUNELFdBQVcsTUFBS2pCLE9BQUwsQ0FBYW1CLGVBQXhCLENBQXpDLEdBQWtGRixXQUFXLE1BQUtqQixPQUFMLENBQWFvQixnQkFBeEIsQ0FBbkYsR0FBOEhILFdBQVcsTUFBS2pCLE9BQUwsQ0FBYXFCLGNBQXhCLENBQS9ILEdBQXlLSixXQUFXLE1BQUtqQixPQUFMLENBQWFzQixVQUF4QixDQUF6TDtBQUNILHlCQVBEO0FBUUgscUJBWEQsTUFXTztBQUNILDZCQUFLN0IsS0FBTCxHQUFhLENBQWI7O0FBRUFnQixtQ0FBV0MsR0FBWCxDQUFlLDRCQUEwQixLQUFLbEIsT0FBOUMsRUFDQ21CLElBREQsQ0FDTSxrQkFBVTtBQUNaLGtDQUFLaEIsUUFBTCxHQUFnQjRCLE9BQU9oQyxJQUF2QjtBQUNBLGtDQUFLRyxLQUFMLEdBQWEsTUFBS0MsUUFBTCxDQUFjQSxRQUFkLENBQXVCa0IsTUFBcEM7QUFDQSxrQ0FBS0csT0FBTCxHQUFrQkMsV0FBVyxNQUFLdEIsUUFBTCxDQUFjdUIsZUFBekIsSUFBMENELFdBQVcsTUFBS3RCLFFBQUwsQ0FBY3dCLGVBQXpCLENBQTFDLEdBQW9GRixXQUFXLE1BQUt0QixRQUFMLENBQWN5QixnQkFBekIsQ0FBckYsR0FBaUlILFdBQVcsTUFBS3RCLFFBQUwsQ0FBYzBCLGNBQXpCLENBQWxJLEdBQTZLSixXQUFXLE1BQUt0QixRQUFMLENBQWM2QixZQUF6QixDQUE3TDtBQUNILHlCQUxEO0FBTUg7QUFDSjtBQUNKLGFBNUJELE1BNEJPO0FBQ0gscUJBQUs5QixLQUFMLEdBQWEsQ0FBYjtBQUNIO0FBQ0osU0FqQ0k7QUFtQ0wrQixpQkFuQ0ssdUJBbUNNO0FBQUE7O0FBQ1AsZ0JBQUcsS0FBS2pDLE9BQUwsSUFBYyxFQUFqQixFQUFvQjtBQUNoQixvQkFBSVksTUFBTSxLQUFLWixPQUFmO0FBQ0Esb0JBQUlhLFFBQVFELElBQUlFLFdBQUosRUFBWjtBQUNBLG9CQUFJZCxVQUFVYSxNQUFNRSxTQUFOLENBQWdCLENBQWhCLEVBQW1CLENBQW5CLENBQWQ7QUFDQSxvQkFBR2YsV0FBUyxJQUFaLEVBQWlCO0FBQ2IseUJBQUtDLEtBQUwsR0FBYSxDQUFiOztBQUVBZ0IsK0JBQVdDLEdBQVgsQ0FBZSwwQkFBd0IsS0FBS2xCLE9BQTVDLEVBQ0NtQixJQURELENBQ00sb0JBQVk7QUFDZCwrQkFBS2YsTUFBTCxHQUFjZ0IsU0FBU3JCLElBQVQsQ0FBY0ssTUFBZCxDQUFxQixDQUFyQixDQUFkO0FBQ0EsK0JBQUtJLE9BQUwsR0FBZVksU0FBU3JCLElBQXhCO0FBQ0EsK0JBQUtHLEtBQUwsR0FBYWtCLFNBQVNyQixJQUFULENBQWNLLE1BQWQsQ0FBcUJpQixNQUFsQztBQUNBLCtCQUFLQyxrQkFBTCxDQUF3QixPQUFLbEIsTUFBTCxDQUFZbUIsV0FBcEM7QUFDQSwrQkFBS0MsT0FBTCxHQUFrQkMsV0FBVyxPQUFLakIsT0FBTCxDQUFha0IsZUFBeEIsSUFBeUNELFdBQVcsT0FBS2pCLE9BQUwsQ0FBYW1CLGVBQXhCLENBQXpDLEdBQWtGRixXQUFXLE9BQUtqQixPQUFMLENBQWFvQixnQkFBeEIsQ0FBbkYsR0FBOEhILFdBQVcsT0FBS2pCLE9BQUwsQ0FBYXFCLGNBQXhCLENBQS9ILEdBQXlLSixXQUFXLE9BQUtqQixPQUFMLENBQWFzQixVQUF4QixDQUF6TDtBQUNILHFCQVBEO0FBUUgsaUJBWEQsTUFXTztBQUNILHlCQUFLN0IsS0FBTCxHQUFhLENBQWI7O0FBRUFnQiwrQkFBV0MsR0FBWCxDQUFlLDRCQUEwQixLQUFLbEIsT0FBOUMsRUFDQ21CLElBREQsQ0FDTSxrQkFBVTtBQUNaLCtCQUFLaEIsUUFBTCxHQUFnQjRCLE9BQU9oQyxJQUF2QjtBQUNBLCtCQUFLRyxLQUFMLEdBQWEsT0FBS0MsUUFBTCxDQUFjQSxRQUFkLENBQXVCa0IsTUFBcEM7QUFDQSwrQkFBS0csT0FBTCxHQUFrQkMsV0FBVyxPQUFLdEIsUUFBTCxDQUFjdUIsZUFBekIsSUFBMENELFdBQVcsT0FBS3RCLFFBQUwsQ0FBY3dCLGVBQXpCLENBQTFDLEdBQW9GRixXQUFXLE9BQUt0QixRQUFMLENBQWN5QixnQkFBekIsQ0FBckYsR0FBaUlILFdBQVcsT0FBS3RCLFFBQUwsQ0FBYzBCLGNBQXpCLENBQWxJLEdBQTZLSixXQUFXLE9BQUt0QixRQUFMLENBQWM2QixZQUF6QixDQUE3TDtBQUNILHFCQUxEO0FBTUg7QUFDSixhQXpCRCxNQXlCTztBQUNILHFCQUFLOUIsS0FBTCxHQUFhLENBQWI7QUFDSDtBQUNKLFNBaEVJO0FBa0VMZ0Msb0JBbEVLLHdCQWtFUUMsRUFsRVIsRUFrRVl4QixDQWxFWixFQWtFZTtBQUNoQixnQkFBR0EsRUFBRXlCLE1BQUYsQ0FBU0MsUUFBVCxJQUFxQixRQUFyQixJQUFpQzFCLEVBQUV5QixNQUFGLENBQVNDLFFBQVQsSUFBcUIsTUFBekQsRUFBaUU7QUFDN0RDLGtCQUFFLGFBQWFILEVBQWYsRUFBbUJJLFdBQW5CLENBQStCLE1BQS9CO0FBQ0FELGtCQUFFLFNBQVNILEVBQVgsRUFBZUksV0FBZixDQUEyQixlQUEzQjtBQUNIO0FBQ0osU0F2RUk7QUF5RUxqQiwwQkF6RUssOEJBeUVja0IsT0F6RWQsRUF5RXdCO0FBQUE7O0FBQ3pCLGlCQUFLakMsV0FBTCxHQUFtQmlDLFFBQVFDLEdBQVIsQ0FBWSxrQkFBVTtBQUNyQyxvQkFBSUMsT0FBTyxDQUFYO0FBQ0Esb0JBQUlDLFNBQVMsQ0FBYjtBQUNBLG9CQUFJQyxNQUFNLENBQVY7O0FBRUFDLHVCQUFPMUMsUUFBUCxDQUFnQnNDLEdBQWhCLENBQW9CLG1CQUFXO0FBQzNCLHdCQUFHSyxRQUFRQyxNQUFSLElBQWtCLENBQWxCLElBQXVCRCxRQUFRRSxRQUFSLElBQW9CLE9BQUs1QyxNQUFMLENBQVkrQixFQUExRCxFQUE2RDtBQUN6RE8sZ0NBQVFqQixXQUFXcUIsUUFBUUosSUFBbkIsQ0FBUjtBQUNBQyxrQ0FBVWxCLFdBQVdxQixRQUFRSCxNQUFuQixDQUFWO0FBQ0FDLCtCQUFPbkIsV0FBV3FCLFFBQVFGLEdBQW5CLENBQVA7QUFDSDtBQUNKLGlCQU5EOztBQVFBLHVCQUFRRixPQUFPQyxNQUFQLEdBQWdCQyxHQUF4QjtBQUVILGFBZmtCLENBQW5CO0FBZ0JILFNBMUZJO0FBNEZMSyxjQTVGSyxrQkE0RkVkLEVBNUZGLEVBNEZLO0FBQUE7O0FBQ05sQix1QkFBV0MsR0FBWCxDQUFlLDJCQUF5QmlCLEVBQXhDLEVBQ0NoQixJQURELENBQ00sb0JBQVk7QUFDZCx1QkFBS2QsT0FBTCxHQUFlZSxTQUFTckIsSUFBeEI7QUFDQSxvQkFBRyxPQUFLTSxPQUFMLENBQWFnQixNQUFiLElBQXFCLENBQXhCLEVBQ0lpQixFQUFFLFVBQUYsRUFBY1ksS0FBZCxDQUFvQixRQUFwQjtBQUNQLGFBTEQ7QUFNSDtBQW5HSSxLQWJVO0FBa0huQkMsV0FsSG1CLHFCQWtIVjtBQUNMLGFBQUs3QyxPQUFMLEdBQWUsU0FBZjtBQUNBZ0MsVUFBRSxNQUFGLEVBQVVjLE9BQVYsQ0FBa0IsRUFBRTtBQUNoQkMsa0JBQUssSUFEUztBQUVkQyxxQkFBUyxPQUZLO0FBR2RDLHNCQUFVO0FBSEksU0FBbEI7QUFLSDtBQXpIa0IsQ0FBUixDQUFmLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDb0JBO0FBQ0E7QUFDQSxnQ0FEQTtBQUVBO0FBRkE7QUFEQSxHOzs7Ozs7Ozs7Ozs7Ozs7QUN2QkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBeU87QUFDblA7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBcU07QUFDL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy92aXNhL3RyYWNraW5nL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0ODkpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGYxMTNlZDM2YTVhNTZiN2RjZjYzIiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUiLCJcblZ1ZS5jb21wb25lbnQoJ2RpYWxvZy1tb2RhbCcsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZScpKTtcblxudmFyIFRyYWNraW5nID0gbmV3IFZ1ZSh7XG4gICAgZWw6JyN0cmFja2luZycsXG4gICAgZGF0YTp7XG4gICAgICAgIGtleXdvcmQ6JycsXG4gICAgICAgIGNoZWNrOjAsXG4gICAgICAgIGNvdW50OjAsXG4gICAgICAgIHNlcnZpY2VzOltdLFxuICAgICAgICBncm91cHM6W10sXG4gICAgICAgIHJlcG9ydHM6W10sXG4gICAgICAgIHByb2Nlc3M6JycsXG4gICAgICAgIHNlcnZpY2VDb3N0OltdLFxuICAgICAgICBjb21wdXRlOltdXG4gICAgfSxcbiAgICBtZXRob2RzOiB7XG4gICAgICAgIHNlYXJjaEVudGVyKGUpe1xuICAgICAgICAgICAgaWYodGhpcy5rZXl3b3JkIT1cIlwiKXtcbiAgICAgICAgICAgICAgICB2YXIga2V5ID0gdGhpcy5rZXl3b3JkO1xuICAgICAgICAgICAgICAgIHZhciB1cHBlciA9IGtleS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgICAgIHZhciBrZXl3b3JkID0gdXBwZXIuc3Vic3RyaW5nKDAsIDIpO1xuICAgICAgICAgICAgICAgIC8vIGFsZXJ0KGtleXdvcmQpO1xuICAgICAgICAgICAgICAgIGlmIChlLmtleUNvZGUgPT09IDEzKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKGtleXdvcmQ9PSdHTCcpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVjayA9IDE7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS90cmFja2luZy9ncm91cC8nK3RoaXMua2V5d29yZClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdyb3VwcyA9IHJlc3BvbnNlLmRhdGEuZ3JvdXBzWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29tcHV0ZSA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3VudCA9IHJlc3BvbnNlLmRhdGEuZ3JvdXBzLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbXB1dGVTZXJ2aWNlQ29zdCh0aGlzLmdyb3Vwcy5ncm91cG1lbWJlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iYWxhbmNlID0gKCgocGFyc2VGbG9hdCh0aGlzLmNvbXB1dGUucGFja2FnZV9kZXBvc2l0KStwYXJzZUZsb2F0KHRoaXMuY29tcHV0ZS5wYWNrYWdlX3BheW1lbnQpK3BhcnNlRmxvYXQodGhpcy5jb21wdXRlLnBhY2thZ2VfZGlzY291bnQpKS1wYXJzZUZsb2F0KHRoaXMuY29tcHV0ZS5wYWNrYWdlX3JlZnVuZCkgKS1wYXJzZUZsb2F0KHRoaXMuY29tcHV0ZS50b3RhbF9jb3N0KSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hlY2sgPSAwO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL3Zpc2EvdHJhY2tpbmcvc2VydmljZS8nK3RoaXMua2V5d29yZClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXJ2aWNlcyA9IHJlc3VsdC5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY291bnQgPSB0aGlzLnNlcnZpY2VzLnNlcnZpY2VzLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJhbGFuY2UgPSAoKChwYXJzZUZsb2F0KHRoaXMuc2VydmljZXMucGFja2FnZV9kZXBvc2l0KStwYXJzZUZsb2F0KHRoaXMuc2VydmljZXMucGFja2FnZV9wYXltZW50KStwYXJzZUZsb2F0KHRoaXMuc2VydmljZXMucGFja2FnZV9kaXNjb3VudCkpLXBhcnNlRmxvYXQodGhpcy5zZXJ2aWNlcy5wYWNrYWdlX3JlZnVuZCkgKS1wYXJzZUZsb2F0KHRoaXMuc2VydmljZXMucGFja2FnZV9jb3N0KSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb3VudCA9IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2VhcmNoQnRuKCl7XG4gICAgICAgICAgICBpZih0aGlzLmtleXdvcmQhPVwiXCIpe1xuICAgICAgICAgICAgICAgIHZhciBrZXkgPSB0aGlzLmtleXdvcmQ7XG4gICAgICAgICAgICAgICAgdmFyIHVwcGVyID0ga2V5LnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICAgICAgdmFyIGtleXdvcmQgPSB1cHBlci5zdWJzdHJpbmcoMCwgMik7XG4gICAgICAgICAgICAgICAgaWYoa2V5d29yZD09J0dMJyl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hlY2sgPSAxO1xuXG4gICAgICAgICAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS90cmFja2luZy9ncm91cC8nK3RoaXMua2V5d29yZClcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ncm91cHMgPSByZXNwb25zZS5kYXRhLmdyb3Vwc1swXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29tcHV0ZSA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvdW50ID0gcmVzcG9uc2UuZGF0YS5ncm91cHMubGVuZ3RoO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb21wdXRlU2VydmljZUNvc3QodGhpcy5ncm91cHMuZ3JvdXBtZW1iZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iYWxhbmNlID0gKCgocGFyc2VGbG9hdCh0aGlzLmNvbXB1dGUucGFja2FnZV9kZXBvc2l0KStwYXJzZUZsb2F0KHRoaXMuY29tcHV0ZS5wYWNrYWdlX3BheW1lbnQpK3BhcnNlRmxvYXQodGhpcy5jb21wdXRlLnBhY2thZ2VfZGlzY291bnQpKS1wYXJzZUZsb2F0KHRoaXMuY29tcHV0ZS5wYWNrYWdlX3JlZnVuZCkgKS1wYXJzZUZsb2F0KHRoaXMuY29tcHV0ZS50b3RhbF9jb3N0KSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hlY2sgPSAwO1xuXG4gICAgICAgICAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS90cmFja2luZy9zZXJ2aWNlLycrdGhpcy5rZXl3b3JkKVxuICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXJ2aWNlcyA9IHJlc3VsdC5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3VudCA9IHRoaXMuc2VydmljZXMuc2VydmljZXMubGVuZ3RoO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iYWxhbmNlID0gKCgocGFyc2VGbG9hdCh0aGlzLnNlcnZpY2VzLnBhY2thZ2VfZGVwb3NpdCkrcGFyc2VGbG9hdCh0aGlzLnNlcnZpY2VzLnBhY2thZ2VfcGF5bWVudCkrcGFyc2VGbG9hdCh0aGlzLnNlcnZpY2VzLnBhY2thZ2VfZGlzY291bnQpKS1wYXJzZUZsb2F0KHRoaXMuc2VydmljZXMucGFja2FnZV9yZWZ1bmQpICktcGFyc2VGbG9hdCh0aGlzLnNlcnZpY2VzLnBhY2thZ2VfY29zdCkpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuY291bnQgPSAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNob3dTZXJ2aWNlcyhpZCwgZSkge1xuICAgICAgICAgICAgaWYoZS50YXJnZXQubm9kZU5hbWUgIT0gJ0JVVFRPTicgJiYgZS50YXJnZXQubm9kZU5hbWUgIT0gJ1NQQU4nKSB7XG4gICAgICAgICAgICAgICAgJCgnI2NsaWVudC0nICsgaWQpLnRvZ2dsZUNsYXNzKCdoaWRlJyk7XG4gICAgICAgICAgICAgICAgJCgnI2ZhLScgKyBpZCkudG9nZ2xlQ2xhc3MoJ2ZhLWFycm93LWRvd24nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjb21wdXRlU2VydmljZUNvc3QobWVtYmVycywpIHtcbiAgICAgICAgICAgIHRoaXMuc2VydmljZUNvc3QgPSBtZW1iZXJzLm1hcChtZW1iZXIgPT4ge1xuICAgICAgICAgICAgICAgIGxldCBjb3N0ID0gMDtcbiAgICAgICAgICAgICAgICBsZXQgY2hhcmdlID0gMDtcbiAgICAgICAgICAgICAgICBsZXQgdGlwID0gMDtcblxuICAgICAgICAgICAgICAgIG1lbWJlci5zZXJ2aWNlcy5tYXAoc2VydmljZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmKHNlcnZpY2UuYWN0aXZlID09IDEgJiYgc2VydmljZS5ncm91cF9pZCA9PSB0aGlzLmdyb3Vwcy5pZCl7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb3N0ICs9IHBhcnNlRmxvYXQoc2VydmljZS5jb3N0KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoYXJnZSArPSBwYXJzZUZsb2F0KHNlcnZpY2UuY2hhcmdlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpcCArPSBwYXJzZUZsb2F0KHNlcnZpY2UudGlwKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIChjb3N0ICsgY2hhcmdlICsgdGlwKTtcblxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVwb3J0KGlkKXtcbiAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS90cmFja2luZy9yZXBvcnQvJytpZClcbiAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnJlcG9ydHMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgIGlmKHRoaXMucmVwb3J0cy5sZW5ndGghPTApXG4gICAgICAgICAgICAgICAgICAgICQoJyNyZXBvcnRzJykubW9kYWwoJ3RvZ2dsZScpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIGNyZWF0ZWQoKXtcbiAgICAgICAgdGhpcy5wcm9jZXNzID0gJ3Byb2Nlc3MnO1xuICAgICAgICAkKCdib2R5JykucG9wb3Zlcih7IC8vIE9rXG4gICAgICAgICAgICBodG1sOnRydWUsXG4gICAgICAgICAgICB0cmlnZ2VyOiAnaG92ZXInLFxuICAgICAgICAgICAgc2VsZWN0b3I6ICdbZGF0YS10b2dnbGU9XCJwb3BvdmVyXCJdJ1xuICAgICAgICB9KTtcbiAgICB9XG59KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvdHJhY2tpbmcvaW5kZXguanMiLCI8dGVtcGxhdGU+XG48ZGl2IGNsYXNzPVwibW9kYWwgbWQtbW9kYWwgZmFkZVwiIDppZD1cImlkXCIgdGFiaW5kZXg9XCItMVwiIHJvbGU9XCJkaWFsb2dcIiBhcmlhLWxhYmVsbGVkYnk9XCJtb2RhbC1sYWJlbFwiPlxuICAgIDxkaXYgOmNsYXNzPVwiJ21vZGFsLWRpYWxvZyAnK3NpemVcIiByb2xlPVwiZG9jdW1lbnRcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cbiAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJtb2RhbC10aXRsZVwiIGlkPVwibW9kYWwtbGFiZWxcIj5cbiAgICAgICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLXRpdGxlXCI+TW9kYWwgVGl0bGU8L3Nsb3Q+XG4gICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtYm9keVwiPk1vZGFsIEJvZHk8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5XCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbjwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICBwcm9wczoge1xuICAgICAgICAnaWQnOntyZXF1aXJlZDp0cnVlfVxuICAgICAgICAsJ3NpemUnOiB7ZGVmYXVsdDonbW9kYWwtbWQnfVxuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIERpYWxvZ01vZGFsLnZ1ZT8wMDNiZGE4OCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNGRlNjA2NTVcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRGlhbG9nTW9kYWwudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTRkZTYwNjU1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNGRlNjA2NTVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNVxuLy8gbW9kdWxlIGNodW5rcyA9IDQgNSA2IDkgMTEgMTggMTkgMjAgMjEgMjIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogX3ZtLmlkLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIi0xXCIsXG4gICAgICBcInJvbGVcIjogXCJkaWFsb2dcIixcbiAgICAgIFwiYXJpYS1sYWJlbGxlZGJ5XCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgY2xhc3M6ICdtb2RhbC1kaWFsb2cgJyArIF92bS5zaXplLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInJvbGVcIjogXCJkb2N1bWVudFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtaGVhZGVyXCJcbiAgfSwgW19jKCdoNCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC10aXRsZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW192bS5fdChcIm1vZGFsLXRpdGxlXCIsIFtfdm0uX3YoXCJNb2RhbCBUaXRsZVwiKV0pXSwgMildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1ib2R5XCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWJvZHlcIiwgW192bS5fdihcIk1vZGFsIEJvZHlcIildKV0sIDIpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWZvb3RlclwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1mb290ZXJcIiwgW19jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXSwgMildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNGRlNjA2NTVcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi00ZGU2MDY1NVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNlxuLy8gbW9kdWxlIGNodW5rcyA9IDQgNSA2IDkgMTEgMTggMTkgMjAgMjEgMjIiXSwic291cmNlUm9vdCI6IiJ9