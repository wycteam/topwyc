/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 481);
/******/ })
/************************************************************************/
/******/ ({

/***/ 191:
/***/ (function(module, exports) {

var data = window.Laravel.user;

new Vue({
	el: '#app',

	data: {
		// contacts: [],

		form: new Form({
			groupName: '',
			leaderId: ''
			// contact: '',
			// address: '',
			// barangay: '',
			// city: '',
			// province: '',
			// zipCode: ''
		}, { baseURL: axiosAPIv1.defaults.baseURL }),

		// addressOptions: 'current address',

		leader: '',

		setting: data.access_control[0].setting,
		permissions: data.permissions,
		selpermissions: [{
			addNewGroup: 0,
			balance: 0,
			collectables: 0,
			action: 0
		}]
	},

	methods: {
		loadClients: function loadClients() {

			var x = this.permissions.filter(function (obj) {
				return obj.type === 'Manage Groups';
			});

			var y = x.filter(function (obj) {
				return obj.name === 'Add New Group';
			});

			if (y.length == 0) this.selpermissions.addNewGroup = 0;else this.selpermissions.addNewGroup = 1;

			var y = x.filter(function (obj) {
				return obj.name === 'Balance';
			});

			if (y.length == 0) this.selpermissions.balance = 0;else this.selpermissions.balance = 1;

			var y = x.filter(function (obj) {
				return obj.name === 'Collectables';
			});

			if (y.length == 0) this.selpermissions.collectables = 0;else this.selpermissions.collectables = 1;

			var y = x.filter(function (obj) {
				return obj.name === 'Action';
			});

			if (y.length == 0) this.selpermissions.action = 0;else this.selpermissions.action = 1;

			if (this.selpermissions.balance == 1 && this.selpermissions.collectables == 1 && this.selpermissions.action == 1) {
				this.callDatatable();
			}

			if (this.selpermissions.balance == 0 && this.selpermissions.collectables == 1 && this.selpermissions.action == 1) {
				this.callDatatable1(1);
			}

			if (this.selpermissions.balance == 1 && this.selpermissions.collectables == 0 && this.selpermissions.action == 1) {
				this.callDatatable1(2);
			}

			if (this.selpermissions.balance == 1 && this.selpermissions.collectables == 1 && this.selpermissions.action == 0) {
				this.callDatatable1(6);
			}

			if (this.selpermissions.balance == 0 && this.selpermissions.collectables == 0 && this.selpermissions.action == 1) {
				this.callDatatable2(1, 2);
			}

			if (this.selpermissions.balance == 0 && this.selpermissions.collectables == 1 && this.selpermissions.action == 0) {
				this.callDatatable2(1, 6);
			}

			if (this.selpermissions.balance == 1 && this.selpermissions.collectables == 0 && this.selpermissions.action == 0) {
				this.callDatatable2(2, 6);
			}

			if (this.selpermissions.balance == 0 && this.selpermissions.collectables == 0 && this.selpermissions.action == 0) {
				this.callDatatable3(1, 2, 6);
			}
		},
		callDatatable: function callDatatable() {
			axios.get('/visa/group/load-group-list').then(function (response) {
				$('#lists').DataTable().destroy();

				setTimeout(function () {
					$('#lists').DataTable({
						"ajax": "/storage/data/Group.txt",
						responsive: true,
						"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
						"iDisplayLength": 25,
						columnDefs: [{ type: 'non-empty-string', targets: [4, 5] }]
					});
				}, 1000);
			});
		},
		callDatatable1: function callDatatable1(a) {
			axios.get('/visa/group/load-group-list').then(function (response) {
				$('#lists').DataTable().destroy();

				setTimeout(function () {
					$('#lists').DataTable({
						"ajax": "/storage/data/Group.txt",
						responsive: true,
						"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
						"iDisplayLength": 25,
						columnDefs: [{ type: 'non-empty-string', targets: [4, 5] }, { "targets": [a], "visible": false, "searchable": false }]
					});
				}, 1000);
			});
		},
		callDatatable2: function callDatatable2(a, b) {
			axios.get('/visa/group/load-group-list').then(function (response) {
				$('#lists').DataTable().destroy();

				setTimeout(function () {
					$('#lists').DataTable({
						"ajax": "/storage/data/Group.txt",
						responsive: true,
						"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
						"iDisplayLength": 25,
						columnDefs: [{ type: 'non-empty-string', targets: [4, 5] }, { "targets": [a], "visible": false, "searchable": false }, { "targets": [b], "visible": false, "searchable": false }]
					});
				}, 1000);
			});
		},
		callDatatable3: function callDatatable3(a, b, c) {
			axios.get('/visa/group/load-group-list').then(function (response) {
				$('#lists').DataTable().destroy();

				setTimeout(function () {
					$('#lists').DataTable({
						"ajax": "/storage/data/Group.txt",
						responsive: true,
						"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
						"iDisplayLength": 25,
						columnDefs: [{ type: 'non-empty-string', targets: [4, 5] }, { "targets": [a], "visible": false, "searchable": false }, { "targets": [b], "visible": false, "searchable": false }, { "targets": [c], "visible": false, "searchable": false }]
					});
				}, 1000);
			});
		},
		addNewGroup: function addNewGroup() {
			var _this = this;

			this.form.submit('post', '/visa/group', {
				groupName: this.form.groupName,
				leader: this.form.leaderId
				// contact: this.form.contact,
				// address: this.form.address,
				// barangay: this.form.barangay,
				// city: this.form.city,
				// province: this.form.province,
				// zipCode: this.form.zipCode	
			}).then(function (result) {
				if (result.success) {
					$('#addNewGroupModal').modal('hide');

					toastr.success(result.message);

					_this.form.groupName = '';
					_this.clearLeader();

					_this.loadClients();
				}
			}).catch(function (error) {
				console.log(error);
			});
		},
		clearLeader: function clearLeader() {
			this.form.leaderId = '';

			// this.contacts = [];
			// this.form.contact = '';

			// this.addressOptions = 'current address';
			// 	this.form.address = '';
			// 	this.form.barangay = '';
			// 	this.form.city = '';
			// 	this.form.province = '';
			// 	this.form.zipCode = '';

			this.leader = '';

			$('.leader_typeahead').val('');
		}
	},

	created: function created() {
		this.loadClients();
	},
	mounted: function mounted() {
		//this.loadClients();

		var vm = this;

		$('.leader_typeahead').typeahead({
			source: function source(query, process) {
				axiosAPIv1.get('/visa/client/get-visa-clients?q=' + query).then(function (response) {
					return process(response.data);
				});
			},
			minLength: 4,
			matcher: function matcher(item) {
				return true;
			},
			updater: function updater(item) {
				vm.form.leaderId = item.id;

				// vm.contacts[0] = item.contact1;
				// if(item.contact2 != null) { vm.contacts[1] = item.contact2; }
				// vm.form.contact = vm.contacts[0];

				// vm.form.address = item.address;
				// if(item.barangay != null) { vm.form.barangay = item.barangay; }
				// vm.form.city = item.city;
				// vm.form.province = item.province;
				// if(item.zipCode != null) { vm.form.zipCode = item.zipCode; }

				vm.leader = item.name;
				return item.name;
			}
		});
	}
});

$(document).ready(function () {

	jQuery.extend(jQuery.fn.dataTableExt.oSort, {
		"non-empty-string-asc": function nonEmptyStringAsc(str1, str2) {
			if (str1 == '<span style="display:none;">--</span>') return 1;
			if (str2 == '<span style="display:none;">--</span>') return -1;
			return str1 < str2 ? -1 : str1 > str2 ? 1 : 0;
		},

		"non-empty-string-desc": function nonEmptyStringDesc(str1, str2) {
			if (str1 == '<span style="display:none;">--</span>') return 1;
			if (str2 == '<span style="display:none;">--</span>') return -1;
			return str1 < str2 ? 1 : str1 > str2 ? -1 : 0;
		}
	});

	$(document).on("click", ".exportExcel", function () {
		var grid = $(this).attr("group-id");

		swal({
			title: "Export Group to Excel?",
			type: 'info',
			allowOutsideClick: false,
			showCloseButton: true,
			html: "" + "<br>" + '<a href="/visa/report/get-group-summary/' + grid + '" target="_blank" class="export-english btn btn-success btn-lg">' + '<img class="dd-selected-image" src="/shopping/img/general/flag/en.jpg"> English' + '</a>' + '<a href="/visa/report/get-group-summary-chinese/' + grid + '" target="_blank" class="export-chinese btn btn-danger btn-lg m-l-xs">' + '<img class="dd-selected-image" src="/shopping/img/general/flag/cn.jpg"> Mandarin' + '</a>',
			showCancelButton: false,
			showConfirmButton: false
		});
	});
});

/***/ }),

/***/ 481:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(191);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNDJjZTBjNmM2ZTJiM2UyMjc4MzU/YzkyZCoqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvZ3JvdXBzL2luZGV4LmpzIl0sIm5hbWVzIjpbImRhdGEiLCJ3aW5kb3ciLCJMYXJhdmVsIiwidXNlciIsIlZ1ZSIsImVsIiwiZm9ybSIsIkZvcm0iLCJncm91cE5hbWUiLCJsZWFkZXJJZCIsImJhc2VVUkwiLCJheGlvc0FQSXYxIiwiZGVmYXVsdHMiLCJsZWFkZXIiLCJzZXR0aW5nIiwiYWNjZXNzX2NvbnRyb2wiLCJwZXJtaXNzaW9ucyIsInNlbHBlcm1pc3Npb25zIiwiYWRkTmV3R3JvdXAiLCJiYWxhbmNlIiwiY29sbGVjdGFibGVzIiwiYWN0aW9uIiwibWV0aG9kcyIsImxvYWRDbGllbnRzIiwieCIsImZpbHRlciIsIm9iaiIsInR5cGUiLCJ5IiwibmFtZSIsImxlbmd0aCIsImNhbGxEYXRhdGFibGUiLCJjYWxsRGF0YXRhYmxlMSIsImNhbGxEYXRhdGFibGUyIiwiY2FsbERhdGF0YWJsZTMiLCJheGlvcyIsImdldCIsInRoZW4iLCJyZXNwb25zZSIsIiQiLCJEYXRhVGFibGUiLCJkZXN0cm95Iiwic2V0VGltZW91dCIsInJlc3BvbnNpdmUiLCJjb2x1bW5EZWZzIiwidGFyZ2V0cyIsImEiLCJiIiwiYyIsInN1Ym1pdCIsInJlc3VsdCIsInN1Y2Nlc3MiLCJtb2RhbCIsInRvYXN0ciIsIm1lc3NhZ2UiLCJjbGVhckxlYWRlciIsImNhdGNoIiwiY29uc29sZSIsImxvZyIsImVycm9yIiwidmFsIiwiY3JlYXRlZCIsIm1vdW50ZWQiLCJ2bSIsInR5cGVhaGVhZCIsInNvdXJjZSIsInF1ZXJ5IiwicHJvY2VzcyIsIm1pbkxlbmd0aCIsIm1hdGNoZXIiLCJpdGVtIiwidXBkYXRlciIsImlkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImpRdWVyeSIsImV4dGVuZCIsImZuIiwiZGF0YVRhYmxlRXh0Iiwib1NvcnQiLCJzdHIxIiwic3RyMiIsIm9uIiwiZ3JpZCIsImF0dHIiLCJzd2FsIiwidGl0bGUiLCJhbGxvd091dHNpZGVDbGljayIsInNob3dDbG9zZUJ1dHRvbiIsImh0bWwiLCJzaG93Q2FuY2VsQnV0dG9uIiwic2hvd0NvbmZpcm1CdXR0b24iXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUEsSUFBSUEsT0FBT0MsT0FBT0MsT0FBUCxDQUFlQyxJQUExQjs7QUFFQSxJQUFJQyxHQUFKLENBQVE7QUFDUEMsS0FBSSxNQURHOztBQUdQTCxPQUFNO0FBQ0w7O0FBRUFNLFFBQU0sSUFBSUMsSUFBSixDQUFTO0FBQ2RDLGNBQVcsRUFERztBQUVkQyxhQUFVO0FBQ1Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUmMsR0FBVCxFQVNILEVBQUVDLFNBQVNDLFdBQVdDLFFBQVgsQ0FBb0JGLE9BQS9CLEVBVEcsQ0FIRDs7QUFjTDs7QUFFQUcsVUFBUSxFQWhCSDs7QUFrQkNDLFdBQVNkLEtBQUtlLGNBQUwsQ0FBb0IsQ0FBcEIsRUFBdUJELE9BbEJqQztBQW1CQ0UsZUFBYWhCLEtBQUtnQixXQW5CbkI7QUFvQkNDLGtCQUFnQixDQUFDO0FBQ2JDLGdCQUFZLENBREM7QUFFYkMsWUFBUSxDQUZLO0FBR2JDLGlCQUFhLENBSEE7QUFJYkMsV0FBTztBQUpNLEdBQUQ7QUFwQmpCLEVBSEM7O0FBK0JQQyxVQUFTO0FBQ1JDLGFBRFEseUJBQ007O0FBRVAsT0FBSUMsSUFBSSxLQUFLUixXQUFMLENBQWlCUyxNQUFqQixDQUF3QixlQUFPO0FBQUUsV0FBT0MsSUFBSUMsSUFBSixLQUFhLGVBQXBCO0FBQW9DLElBQXJFLENBQVI7O0FBRUEsT0FBSUMsSUFBSUosRUFBRUMsTUFBRixDQUFTLGVBQU87QUFBRSxXQUFPQyxJQUFJRyxJQUFKLEtBQWEsZUFBcEI7QUFBb0MsSUFBdEQsQ0FBUjs7QUFFQSxPQUFHRCxFQUFFRSxNQUFGLElBQVUsQ0FBYixFQUFnQixLQUFLYixjQUFMLENBQW9CQyxXQUFwQixHQUFrQyxDQUFsQyxDQUFoQixLQUNLLEtBQUtELGNBQUwsQ0FBb0JDLFdBQXBCLEdBQWtDLENBQWxDOztBQUVMLE9BQUlVLElBQUlKLEVBQUVDLE1BQUYsQ0FBUyxlQUFPO0FBQUUsV0FBT0MsSUFBSUcsSUFBSixLQUFhLFNBQXBCO0FBQThCLElBQWhELENBQVI7O0FBRUEsT0FBR0QsRUFBRUUsTUFBRixJQUFVLENBQWIsRUFBZ0IsS0FBS2IsY0FBTCxDQUFvQkUsT0FBcEIsR0FBOEIsQ0FBOUIsQ0FBaEIsS0FDSyxLQUFLRixjQUFMLENBQW9CRSxPQUFwQixHQUE4QixDQUE5Qjs7QUFFTCxPQUFJUyxJQUFJSixFQUFFQyxNQUFGLENBQVMsZUFBTztBQUFFLFdBQU9DLElBQUlHLElBQUosS0FBYSxjQUFwQjtBQUFtQyxJQUFyRCxDQUFSOztBQUVBLE9BQUdELEVBQUVFLE1BQUYsSUFBVSxDQUFiLEVBQWdCLEtBQUtiLGNBQUwsQ0FBb0JHLFlBQXBCLEdBQW1DLENBQW5DLENBQWhCLEtBQ0ssS0FBS0gsY0FBTCxDQUFvQkcsWUFBcEIsR0FBbUMsQ0FBbkM7O0FBRUwsT0FBSVEsSUFBSUosRUFBRUMsTUFBRixDQUFTLGVBQU87QUFBRSxXQUFPQyxJQUFJRyxJQUFKLEtBQWEsUUFBcEI7QUFBNkIsSUFBL0MsQ0FBUjs7QUFFQSxPQUFHRCxFQUFFRSxNQUFGLElBQVUsQ0FBYixFQUFnQixLQUFLYixjQUFMLENBQW9CSSxNQUFwQixHQUE2QixDQUE3QixDQUFoQixLQUNLLEtBQUtKLGNBQUwsQ0FBb0JJLE1BQXBCLEdBQTZCLENBQTdCOztBQUVMLE9BQUcsS0FBS0osY0FBTCxDQUFvQkUsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0YsY0FBTCxDQUFvQkcsWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0gsY0FBTCxDQUFvQkksTUFBcEIsSUFBOEIsQ0FBOUcsRUFBZ0g7QUFDckgsU0FBS1UsYUFBTDtBQUNNOztBQUVELE9BQUcsS0FBS2QsY0FBTCxDQUFvQkUsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0YsY0FBTCxDQUFvQkcsWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0gsY0FBTCxDQUFvQkksTUFBcEIsSUFBOEIsQ0FBOUcsRUFBZ0g7QUFDckgsU0FBS1csY0FBTCxDQUFvQixDQUFwQjtBQUNNOztBQUVELE9BQUcsS0FBS2YsY0FBTCxDQUFvQkUsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0YsY0FBTCxDQUFvQkcsWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0gsY0FBTCxDQUFvQkksTUFBcEIsSUFBOEIsQ0FBOUcsRUFBZ0g7QUFDckgsU0FBS1csY0FBTCxDQUFvQixDQUFwQjtBQUNNOztBQUVELE9BQUcsS0FBS2YsY0FBTCxDQUFvQkUsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0YsY0FBTCxDQUFvQkcsWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0gsY0FBTCxDQUFvQkksTUFBcEIsSUFBOEIsQ0FBOUcsRUFBZ0g7QUFDckgsU0FBS1csY0FBTCxDQUFvQixDQUFwQjtBQUNNOztBQUVELE9BQUcsS0FBS2YsY0FBTCxDQUFvQkUsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0YsY0FBTCxDQUFvQkcsWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0gsY0FBTCxDQUFvQkksTUFBcEIsSUFBOEIsQ0FBOUcsRUFBZ0g7QUFDckgsU0FBS1ksY0FBTCxDQUFvQixDQUFwQixFQUFzQixDQUF0QjtBQUNNOztBQUVELE9BQUcsS0FBS2hCLGNBQUwsQ0FBb0JFLE9BQXBCLElBQStCLENBQS9CLElBQW9DLEtBQUtGLGNBQUwsQ0FBb0JHLFlBQXBCLElBQW9DLENBQXhFLElBQTZFLEtBQUtILGNBQUwsQ0FBb0JJLE1BQXBCLElBQThCLENBQTlHLEVBQWdIO0FBQ3JILFNBQUtZLGNBQUwsQ0FBb0IsQ0FBcEIsRUFBc0IsQ0FBdEI7QUFDTTs7QUFFRCxPQUFHLEtBQUtoQixjQUFMLENBQW9CRSxPQUFwQixJQUErQixDQUEvQixJQUFvQyxLQUFLRixjQUFMLENBQW9CRyxZQUFwQixJQUFvQyxDQUF4RSxJQUE2RSxLQUFLSCxjQUFMLENBQW9CSSxNQUFwQixJQUE4QixDQUE5RyxFQUFnSDtBQUNySCxTQUFLWSxjQUFMLENBQW9CLENBQXBCLEVBQXNCLENBQXRCO0FBQ007O0FBRUQsT0FBRyxLQUFLaEIsY0FBTCxDQUFvQkUsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0YsY0FBTCxDQUFvQkcsWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0gsY0FBTCxDQUFvQkksTUFBcEIsSUFBOEIsQ0FBOUcsRUFBZ0g7QUFDckgsU0FBS2EsY0FBTCxDQUFvQixDQUFwQixFQUFzQixDQUF0QixFQUF3QixDQUF4QjtBQUNNO0FBRVAsR0F6RE87QUEyRFJILGVBM0RRLDJCQTJEUTtBQUNmSSxTQUFNQyxHQUFOLENBQVUsNkJBQVYsRUFDRUMsSUFERixDQUNPLFVBQVVDLFFBQVYsRUFBb0I7QUFDekJDLE1BQUUsUUFBRixFQUFZQyxTQUFaLEdBQXdCQyxPQUF4Qjs7QUFFQUMsZUFBVyxZQUFXO0FBQ3JCSCxPQUFFLFFBQUYsRUFBWUMsU0FBWixDQUFzQjtBQUNsQixjQUFRLHlCQURVO0FBRWxCRyxrQkFBWSxJQUZNO0FBR2xCLG9CQUFjLENBQUMsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsRUFBYSxDQUFDLENBQWQsQ0FBRCxFQUFtQixDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLEtBQWIsQ0FBbkIsQ0FISTtBQUlsQix3QkFBa0IsRUFKQTtBQUtsQkMsa0JBQVksQ0FDWixFQUFDakIsTUFBTSxrQkFBUCxFQUEyQmtCLFNBQVMsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFwQyxFQURZO0FBTE0sTUFBdEI7QUFTQSxLQVZELEVBVUcsSUFWSDtBQVdBLElBZkY7QUFnQkEsR0E1RU87QUE4RVJiLGdCQTlFUSwwQkE4RU9jLENBOUVQLEVBOEVVO0FBQ2pCWCxTQUFNQyxHQUFOLENBQVUsNkJBQVYsRUFDRUMsSUFERixDQUNPLFVBQVVDLFFBQVYsRUFBb0I7QUFDekJDLE1BQUUsUUFBRixFQUFZQyxTQUFaLEdBQXdCQyxPQUF4Qjs7QUFFQUMsZUFBVyxZQUFXO0FBQ3JCSCxPQUFFLFFBQUYsRUFBWUMsU0FBWixDQUFzQjtBQUNsQixjQUFRLHlCQURVO0FBRWxCRyxrQkFBWSxJQUZNO0FBR2xCLG9CQUFjLENBQUMsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsRUFBYSxDQUFDLENBQWQsQ0FBRCxFQUFtQixDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLEtBQWIsQ0FBbkIsQ0FISTtBQUlsQix3QkFBa0IsRUFKQTtBQUtsQkMsa0JBQVksQ0FDWixFQUFDakIsTUFBTSxrQkFBUCxFQUEyQmtCLFNBQVMsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFwQyxFQURZLEVBRVosRUFBQyxXQUFXLENBQUVDLENBQUYsQ0FBWixFQUFrQixXQUFXLEtBQTdCLEVBQW1DLGNBQWMsS0FBakQsRUFGWTtBQUxNLE1BQXRCO0FBVUEsS0FYRCxFQVdHLElBWEg7QUFZQSxJQWhCRjtBQWlCQSxHQWhHTztBQWtHUmIsZ0JBbEdRLDBCQWtHT2EsQ0FsR1AsRUFrR1NDLENBbEdULEVBa0dZO0FBQ25CWixTQUFNQyxHQUFOLENBQVUsNkJBQVYsRUFDRUMsSUFERixDQUNPLFVBQVVDLFFBQVYsRUFBb0I7QUFDekJDLE1BQUUsUUFBRixFQUFZQyxTQUFaLEdBQXdCQyxPQUF4Qjs7QUFFQUMsZUFBVyxZQUFXO0FBQ3JCSCxPQUFFLFFBQUYsRUFBWUMsU0FBWixDQUFzQjtBQUNsQixjQUFRLHlCQURVO0FBRWxCRyxrQkFBWSxJQUZNO0FBR2xCLG9CQUFjLENBQUMsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsRUFBYSxDQUFDLENBQWQsQ0FBRCxFQUFtQixDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLEtBQWIsQ0FBbkIsQ0FISTtBQUlsQix3QkFBa0IsRUFKQTtBQUtsQkMsa0JBQVksQ0FDWixFQUFDakIsTUFBTSxrQkFBUCxFQUEyQmtCLFNBQVMsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFwQyxFQURZLEVBRVosRUFBQyxXQUFXLENBQUVDLENBQUYsQ0FBWixFQUFrQixXQUFXLEtBQTdCLEVBQW1DLGNBQWMsS0FBakQsRUFGWSxFQUdaLEVBQUMsV0FBVyxDQUFFQyxDQUFGLENBQVosRUFBa0IsV0FBVyxLQUE3QixFQUFtQyxjQUFjLEtBQWpELEVBSFk7QUFMTSxNQUF0QjtBQVdBLEtBWkQsRUFZRyxJQVpIO0FBYUEsSUFqQkY7QUFrQkEsR0FySE87QUF1SFJiLGdCQXZIUSwwQkF1SE9ZLENBdkhQLEVBdUhTQyxDQXZIVCxFQXVIV0MsQ0F2SFgsRUF1SGM7QUFDckJiLFNBQU1DLEdBQU4sQ0FBVSw2QkFBVixFQUNFQyxJQURGLENBQ08sVUFBVUMsUUFBVixFQUFvQjtBQUN6QkMsTUFBRSxRQUFGLEVBQVlDLFNBQVosR0FBd0JDLE9BQXhCOztBQUVBQyxlQUFXLFlBQVc7QUFDckJILE9BQUUsUUFBRixFQUFZQyxTQUFaLENBQXNCO0FBQ2xCLGNBQVEseUJBRFU7QUFFbEJHLGtCQUFZLElBRk07QUFHbEIsb0JBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLENBQUMsQ0FBZCxDQUFELEVBQW1CLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULEVBQWEsS0FBYixDQUFuQixDQUhJO0FBSWxCLHdCQUFrQixFQUpBO0FBS2xCQyxrQkFBWSxDQUNaLEVBQUNqQixNQUFNLGtCQUFQLEVBQTJCa0IsU0FBUyxDQUFDLENBQUQsRUFBRyxDQUFILENBQXBDLEVBRFksRUFFWixFQUFDLFdBQVcsQ0FBRUMsQ0FBRixDQUFaLEVBQWtCLFdBQVcsS0FBN0IsRUFBbUMsY0FBYyxLQUFqRCxFQUZZLEVBR1osRUFBQyxXQUFXLENBQUVDLENBQUYsQ0FBWixFQUFrQixXQUFXLEtBQTdCLEVBQW1DLGNBQWMsS0FBakQsRUFIWSxFQUlaLEVBQUMsV0FBVyxDQUFFQyxDQUFGLENBQVosRUFBa0IsV0FBVyxLQUE3QixFQUFtQyxjQUFjLEtBQWpELEVBSlk7QUFMTSxNQUF0QjtBQVlBLEtBYkQsRUFhRyxJQWJIO0FBY0EsSUFsQkY7QUFtQkEsR0EzSU87QUE2SVI5QixhQTdJUSx5QkE2SU07QUFBQTs7QUFDWixRQUFLWixJQUFMLENBQVUyQyxNQUFWLENBQWlCLE1BQWpCLEVBQXlCLGFBQXpCLEVBQXdDO0FBQ3ZDekMsZUFBVyxLQUFLRixJQUFMLENBQVVFLFNBRGtCO0FBRWxDSyxZQUFRLEtBQUtQLElBQUwsQ0FBVUc7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUmtDLElBQXhDLEVBVVM0QixJQVZULENBVWMsa0JBQVU7QUFDWixRQUFHYSxPQUFPQyxPQUFWLEVBQW1CO0FBQ2xCWixPQUFFLG1CQUFGLEVBQXVCYSxLQUF2QixDQUE2QixNQUE3Qjs7QUFFWEMsWUFBT0YsT0FBUCxDQUFlRCxPQUFPSSxPQUF0Qjs7QUFFQSxXQUFLaEQsSUFBTCxDQUFVRSxTQUFWLEdBQXNCLEVBQXRCO0FBQ0EsV0FBSytDLFdBQUw7O0FBRUEsV0FBS2hDLFdBQUw7QUFDRDtBQUNRLElBckJULEVBc0JTaUMsS0F0QlQsQ0FzQmUsaUJBQVM7QUFDWkMsWUFBUUMsR0FBUixDQUFZQyxLQUFaO0FBQ0gsSUF4QlQ7QUF5QkQsR0F2S087QUF5S1JKLGFBektRLHlCQXlLTTtBQUNiLFFBQUtqRCxJQUFMLENBQVVHLFFBQVYsR0FBcUIsRUFBckI7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsUUFBS0ksTUFBTCxHQUFjLEVBQWQ7O0FBRUEwQixLQUFFLG1CQUFGLEVBQXVCcUIsR0FBdkIsQ0FBMkIsRUFBM0I7QUFDQTtBQXpMTyxFQS9CRjs7QUEyTlBDLFFBM05PLHFCQTJORTtBQUNSLE9BQUt0QyxXQUFMO0FBQ0EsRUE3Tk07QUErTlB1QyxRQS9OTyxxQkErTkc7QUFDVDs7QUFFQSxNQUFJQyxLQUFLLElBQVQ7O0FBRUF4QixJQUFFLG1CQUFGLEVBQXVCeUIsU0FBdkIsQ0FBaUM7QUFDdkJDLFdBQVEsZ0JBQVNDLEtBQVQsRUFBZ0JDLE9BQWhCLEVBQXlCO0FBQ3BDeEQsZUFBV3lCLEdBQVgsQ0FBZSxxQ0FBcUM4QixLQUFwRCxFQUNIN0IsSUFERyxDQUNFLG9CQUFZO0FBQ2pCLFlBQU84QixRQUFRN0IsU0FBU3RDLElBQWpCLENBQVA7QUFDQSxLQUhHO0FBSUYsSUFONEI7QUFPdkJvRSxjQUFXLENBUFk7QUFRdkJDLFlBQVMsaUJBQVNDLElBQVQsRUFBZTtBQUNwQixXQUFPLElBQVA7QUFDSCxJQVZzQjtBQVd2QkMsWUFBUyxpQkFBU0QsSUFBVCxFQUFlO0FBQ3ZCUCxPQUFHekQsSUFBSCxDQUFRRyxRQUFSLEdBQW1CNkQsS0FBS0UsRUFBeEI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUFULE9BQUdsRCxNQUFILEdBQVl5RCxLQUFLekMsSUFBakI7QUFDQSxXQUFPeUMsS0FBS3pDLElBQVo7QUFDQTtBQTFCc0IsR0FBakM7QUE0QkE7QUFoUU0sQ0FBUjs7QUFvUUFVLEVBQUVrQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVzs7QUFFNUJDLFFBQU9DLE1BQVAsQ0FBZUQsT0FBT0UsRUFBUCxDQUFVQyxZQUFWLENBQXVCQyxLQUF0QyxFQUE2QztBQUN6QywwQkFBd0IsMkJBQVVDLElBQVYsRUFBZ0JDLElBQWhCLEVBQXNCO0FBQzFDLE9BQUdELFFBQVEsdUNBQVgsRUFDSSxPQUFPLENBQVA7QUFDSixPQUFHQyxRQUFRLHVDQUFYLEVBQ0ksT0FBTyxDQUFDLENBQVI7QUFDSixVQUFTRCxPQUFPQyxJQUFSLEdBQWdCLENBQUMsQ0FBakIsR0FBdUJELE9BQU9DLElBQVIsR0FBZ0IsQ0FBaEIsR0FBb0IsQ0FBbEQ7QUFDSCxHQVB3Qzs7QUFTekMsMkJBQXlCLDRCQUFVRCxJQUFWLEVBQWdCQyxJQUFoQixFQUFzQjtBQUMzQyxPQUFHRCxRQUFRLHVDQUFYLEVBQ0ksT0FBTyxDQUFQO0FBQ0osT0FBR0MsUUFBUSx1Q0FBWCxFQUNJLE9BQU8sQ0FBQyxDQUFSO0FBQ0osVUFBU0QsT0FBT0MsSUFBUixHQUFnQixDQUFoQixHQUFzQkQsT0FBT0MsSUFBUixHQUFnQixDQUFDLENBQWpCLEdBQXFCLENBQWxEO0FBQ0g7QUFmd0MsRUFBN0M7O0FBbUJBMUMsR0FBRWtDLFFBQUYsRUFBWVMsRUFBWixDQUFlLE9BQWYsRUFBdUIsY0FBdkIsRUFBc0MsWUFBVztBQUNoRCxNQUFJQyxPQUFPNUMsRUFBRSxJQUFGLEVBQVE2QyxJQUFSLENBQWEsVUFBYixDQUFYOztBQUVLQyxPQUFLO0FBQ1RDLFVBQU8sd0JBREU7QUFFQTNELFNBQU0sTUFGTjtBQUdBNEQsc0JBQW1CLEtBSG5CO0FBSUFDLG9CQUFpQixJQUpqQjtBQUtUQyxTQUFNLEtBQ0ksTUFESixHQUVJLDBDQUZKLEdBRStDTixJQUYvQyxHQUVvRCxrRUFGcEQsR0FFeUgsaUZBRnpILEdBRTZNLE1BRjdNLEdBR0ksa0RBSEosR0FHdURBLElBSHZELEdBRzRELHdFQUg1RCxHQUd1SSxrRkFIdkksR0FHNE4sTUFSek47QUFTSE8scUJBQWtCLEtBVGY7QUFVSEMsc0JBQW1CO0FBVmhCLEdBQUw7QUFZRixFQWZKO0FBaUJBLENBdENELEUiLCJmaWxlIjoianMvdmlzYS9ncm91cHMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ4MSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgNDJjZTBjNmM2ZTJiM2UyMjc4MzUiLCJsZXQgZGF0YSA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XG5cbm5ldyBWdWUoe1xuXHRlbDogJyNhcHAnLFxuXG5cdGRhdGE6IHtcblx0XHQvLyBjb250YWN0czogW10sXG5cblx0XHRmb3JtOiBuZXcgRm9ybSh7XG5cdFx0XHRncm91cE5hbWU6ICcnLFxuXHRcdFx0bGVhZGVySWQ6ICcnLFxuXHRcdFx0Ly8gY29udGFjdDogJycsXG5cdFx0XHQvLyBhZGRyZXNzOiAnJyxcblx0XHRcdC8vIGJhcmFuZ2F5OiAnJyxcblx0XHRcdC8vIGNpdHk6ICcnLFxuXHRcdFx0Ly8gcHJvdmluY2U6ICcnLFxuXHRcdFx0Ly8gemlwQ29kZTogJydcblx0XHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KSxcblxuXHRcdC8vIGFkZHJlc3NPcHRpb25zOiAnY3VycmVudCBhZGRyZXNzJyxcblxuXHRcdGxlYWRlcjogJycsXG5cbiAgICAgICAgc2V0dGluZzogZGF0YS5hY2Nlc3NfY29udHJvbFswXS5zZXR0aW5nLFxuICAgICAgICBwZXJtaXNzaW9uczogZGF0YS5wZXJtaXNzaW9ucyxcbiAgICAgICAgc2VscGVybWlzc2lvbnM6IFt7XG4gICAgICAgICAgICBhZGROZXdHcm91cDowLFxuICAgICAgICAgICAgYmFsYW5jZTowLFxuICAgICAgICAgICAgY29sbGVjdGFibGVzOjAsXG4gICAgICAgICAgICBhY3Rpb246MFxuICAgICAgICB9XVxuXHR9LFxuXG5cdG1ldGhvZHM6IHtcblx0XHRsb2FkQ2xpZW50cygpIHtcblxuXHQgICAgICAgIHZhciB4ID0gdGhpcy5wZXJtaXNzaW9ucy5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAnTWFuYWdlIEdyb3Vwcyd9KTtcblxuXHQgICAgICAgIHZhciB5ID0geC5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai5uYW1lID09PSAnQWRkIE5ldyBHcm91cCd9KTtcblxuXHQgICAgICAgIGlmKHkubGVuZ3RoPT0wKSB0aGlzLnNlbHBlcm1pc3Npb25zLmFkZE5ld0dyb3VwID0gMDtcblx0ICAgICAgICBlbHNlIHRoaXMuc2VscGVybWlzc2lvbnMuYWRkTmV3R3JvdXAgPSAxO1xuXG5cdCAgICAgICAgdmFyIHkgPSB4LmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLm5hbWUgPT09ICdCYWxhbmNlJ30pO1xuXG5cdCAgICAgICAgaWYoeS5sZW5ndGg9PTApIHRoaXMuc2VscGVybWlzc2lvbnMuYmFsYW5jZSA9IDA7XG5cdCAgICAgICAgZWxzZSB0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPSAxO1xuXG5cdCAgICAgICAgdmFyIHkgPSB4LmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLm5hbWUgPT09ICdDb2xsZWN0YWJsZXMnfSk7XG5cblx0ICAgICAgICBpZih5Lmxlbmd0aD09MCkgdGhpcy5zZWxwZXJtaXNzaW9ucy5jb2xsZWN0YWJsZXMgPSAwO1xuXHQgICAgICAgIGVsc2UgdGhpcy5zZWxwZXJtaXNzaW9ucy5jb2xsZWN0YWJsZXMgPSAxO1xuXG5cdCAgICAgICAgdmFyIHkgPSB4LmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLm5hbWUgPT09ICdBY3Rpb24nfSk7XG5cblx0ICAgICAgICBpZih5Lmxlbmd0aD09MCkgdGhpcy5zZWxwZXJtaXNzaW9ucy5hY3Rpb24gPSAwO1xuXHQgICAgICAgIGVsc2UgdGhpcy5zZWxwZXJtaXNzaW9ucy5hY3Rpb24gPSAxO1xuXG5cdCAgICAgICAgaWYodGhpcy5zZWxwZXJtaXNzaW9ucy5iYWxhbmNlID09IDEgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5jb2xsZWN0YWJsZXMgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmFjdGlvbiA9PSAxKXtcblx0XHRcdFx0dGhpcy5jYWxsRGF0YXRhYmxlKCk7XG5cdCAgICAgICAgfVxuXG5cdCAgICAgICAgaWYodGhpcy5zZWxwZXJtaXNzaW9ucy5iYWxhbmNlID09IDAgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5jb2xsZWN0YWJsZXMgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmFjdGlvbiA9PSAxKXtcblx0XHRcdFx0dGhpcy5jYWxsRGF0YXRhYmxlMSgxKTtcblx0ICAgICAgICB9XG5cblx0ICAgICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbGxlY3RhYmxlcyA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMuYWN0aW9uID09IDEpe1xuXHRcdFx0XHR0aGlzLmNhbGxEYXRhdGFibGUxKDIpO1xuXHQgICAgICAgIH1cblxuXHQgICAgICAgIGlmKHRoaXMuc2VscGVybWlzc2lvbnMuYmFsYW5jZSA9PSAxICYmIHRoaXMuc2VscGVybWlzc2lvbnMuY29sbGVjdGFibGVzID09IDEgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5hY3Rpb24gPT0gMCl7XG5cdFx0XHRcdHRoaXMuY2FsbERhdGF0YWJsZTEoNik7XG5cdCAgICAgICAgfVxuXG5cdCAgICAgICAgaWYodGhpcy5zZWxwZXJtaXNzaW9ucy5iYWxhbmNlID09IDAgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5jb2xsZWN0YWJsZXMgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmFjdGlvbiA9PSAxKXtcblx0XHRcdFx0dGhpcy5jYWxsRGF0YXRhYmxlMigxLDIpO1xuXHQgICAgICAgIH1cblxuXHQgICAgICAgIGlmKHRoaXMuc2VscGVybWlzc2lvbnMuYmFsYW5jZSA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMuY29sbGVjdGFibGVzID09IDEgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5hY3Rpb24gPT0gMCl7XG5cdFx0XHRcdHRoaXMuY2FsbERhdGF0YWJsZTIoMSw2KTtcblx0ICAgICAgICB9XG5cblx0ICAgICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbGxlY3RhYmxlcyA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMuYWN0aW9uID09IDApe1xuXHRcdFx0XHR0aGlzLmNhbGxEYXRhdGFibGUyKDIsNik7XG5cdCAgICAgICAgfVxuXG5cdCAgICAgICAgaWYodGhpcy5zZWxwZXJtaXNzaW9ucy5iYWxhbmNlID09IDAgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5jb2xsZWN0YWJsZXMgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmFjdGlvbiA9PSAwKXtcblx0XHRcdFx0dGhpcy5jYWxsRGF0YXRhYmxlMygxLDIsNik7XG5cdCAgICAgICAgfVxuXG5cdFx0fSxcblxuXHRcdGNhbGxEYXRhdGFibGUoKSB7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2dyb3VwL2xvYWQtZ3JvdXAtbGlzdCcpXG5cdFx0XHRcdC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuXHRcdFx0XHRcdCQoJyNsaXN0cycpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuXHRcdFx0XHRcdFx0ICAgIFwiYWpheFwiOiBcIi9zdG9yYWdlL2RhdGEvR3JvdXAudHh0XCIsXG5cdFx0XHRcdFx0XHQgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHRcdFx0XHRcdCAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwLCAtMV0sIFsxMCwgMjUsIDUwLCBcIkFsbFwiXV0sXG5cdFx0XHRcdFx0XHQgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAyNSxcblx0XHRcdFx0XHRcdCAgICBjb2x1bW5EZWZzOiBbXG5cdFx0XHRcdFx0XHQgICAge3R5cGU6ICdub24tZW1wdHktc3RyaW5nJywgdGFyZ2V0czogWzQsNV19XG5cdFx0XHRcdFx0XHQgICAgXVxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHRcdH0pO1xuXHRcdH0sXG5cblx0XHRjYWxsRGF0YXRhYmxlMShhKSB7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2dyb3VwL2xvYWQtZ3JvdXAtbGlzdCcpXG5cdFx0XHRcdC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuXHRcdFx0XHRcdCQoJyNsaXN0cycpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuXHRcdFx0XHRcdFx0ICAgIFwiYWpheFwiOiBcIi9zdG9yYWdlL2RhdGEvR3JvdXAudHh0XCIsXG5cdFx0XHRcdFx0XHQgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHRcdFx0XHRcdCAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwLCAtMV0sIFsxMCwgMjUsIDUwLCBcIkFsbFwiXV0sXG5cdFx0XHRcdFx0XHQgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAyNSxcblx0XHRcdFx0XHRcdCAgICBjb2x1bW5EZWZzOiBbXG5cdFx0XHRcdFx0XHQgICAge3R5cGU6ICdub24tZW1wdHktc3RyaW5nJywgdGFyZ2V0czogWzQsNV19LFxuXHRcdFx0XHRcdFx0ICAgIHtcInRhcmdldHNcIjogWyBhIF0sXCJ2aXNpYmxlXCI6IGZhbHNlLFwic2VhcmNoYWJsZVwiOiBmYWxzZX1cblx0XHRcdFx0XHRcdCAgICBdXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9LCAxMDAwKTtcblx0XHRcdFx0fSk7XG5cdFx0fSxcblxuXHRcdGNhbGxEYXRhdGFibGUyKGEsYikge1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC9sb2FkLWdyb3VwLWxpc3QnKVxuXHRcdFx0XHQudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcblx0XHRcdFx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0XHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0JCgnI2xpc3RzJykuRGF0YVRhYmxlKHtcblx0XHRcdFx0XHRcdCAgICBcImFqYXhcIjogXCIvc3RvcmFnZS9kYXRhL0dyb3VwLnR4dFwiLFxuXHRcdFx0XHRcdFx0ICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdFx0XHRcdFx0XHQgICAgXCJsZW5ndGhNZW51XCI6IFtbMTAsIDI1LCA1MCwgLTFdLCBbMTAsIDI1LCA1MCwgXCJBbGxcIl1dLFxuXHRcdFx0XHRcdFx0ICAgIFwiaURpc3BsYXlMZW5ndGhcIjogMjUsXG5cdFx0XHRcdFx0XHQgICAgY29sdW1uRGVmczogW1xuXHRcdFx0XHRcdFx0ICAgIHt0eXBlOiAnbm9uLWVtcHR5LXN0cmluZycsIHRhcmdldHM6IFs0LDVdfSxcblx0XHRcdFx0XHRcdCAgICB7XCJ0YXJnZXRzXCI6IFsgYSBdLFwidmlzaWJsZVwiOiBmYWxzZSxcInNlYXJjaGFibGVcIjogZmFsc2V9LFxuXHRcdFx0XHRcdFx0ICAgIHtcInRhcmdldHNcIjogWyBiIF0sXCJ2aXNpYmxlXCI6IGZhbHNlLFwic2VhcmNoYWJsZVwiOiBmYWxzZX1cblx0XHRcdFx0XHRcdCAgICBdXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9LCAxMDAwKTtcblx0XHRcdFx0fSk7XG5cdFx0fSxcblxuXHRcdGNhbGxEYXRhdGFibGUzKGEsYixjKSB7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2dyb3VwL2xvYWQtZ3JvdXAtbGlzdCcpXG5cdFx0XHRcdC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuXHRcdFx0XHRcdCQoJyNsaXN0cycpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuXHRcdFx0XHRcdFx0ICAgIFwiYWpheFwiOiBcIi9zdG9yYWdlL2RhdGEvR3JvdXAudHh0XCIsXG5cdFx0XHRcdFx0XHQgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHRcdFx0XHRcdCAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwLCAtMV0sIFsxMCwgMjUsIDUwLCBcIkFsbFwiXV0sXG5cdFx0XHRcdFx0XHQgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAyNSxcblx0XHRcdFx0XHRcdCAgICBjb2x1bW5EZWZzOiBbXG5cdFx0XHRcdFx0XHQgICAge3R5cGU6ICdub24tZW1wdHktc3RyaW5nJywgdGFyZ2V0czogWzQsNV19LFxuXHRcdFx0XHRcdFx0ICAgIHtcInRhcmdldHNcIjogWyBhIF0sXCJ2aXNpYmxlXCI6IGZhbHNlLFwic2VhcmNoYWJsZVwiOiBmYWxzZX0sXG5cdFx0XHRcdFx0XHQgICAge1widGFyZ2V0c1wiOiBbIGIgXSxcInZpc2libGVcIjogZmFsc2UsXCJzZWFyY2hhYmxlXCI6IGZhbHNlfSxcblx0XHRcdFx0XHRcdCAgICB7XCJ0YXJnZXRzXCI6IFsgYyBdLFwidmlzaWJsZVwiOiBmYWxzZSxcInNlYXJjaGFibGVcIjogZmFsc2V9XG5cdFx0XHRcdFx0XHQgICAgXVxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHRcdH0pO1xuXHRcdH0sXHRcdFxuXG5cdFx0YWRkTmV3R3JvdXAoKSB7XG5cdFx0IFx0dGhpcy5mb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ncm91cCcsIHtcblx0XHQgXHRcdGdyb3VwTmFtZTogdGhpcy5mb3JtLmdyb3VwTmFtZSxcblx0XHQgICAgICAgIGxlYWRlcjogdGhpcy5mb3JtLmxlYWRlcklkLFxuXHRcdCAgICAgICAgLy8gY29udGFjdDogdGhpcy5mb3JtLmNvbnRhY3QsXG5cdFx0ICAgICAgICAvLyBhZGRyZXNzOiB0aGlzLmZvcm0uYWRkcmVzcyxcblx0XHQgICAgICAgIC8vIGJhcmFuZ2F5OiB0aGlzLmZvcm0uYmFyYW5nYXksXG5cdFx0ICAgICAgICAvLyBjaXR5OiB0aGlzLmZvcm0uY2l0eSxcblx0XHQgICAgICAgIC8vIHByb3ZpbmNlOiB0aGlzLmZvcm0ucHJvdmluY2UsXG5cdFx0ICAgICAgICAvLyB6aXBDb2RlOiB0aGlzLmZvcm0uemlwQ29kZVx0XG5cdFx0IFx0fSlcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgICAgICAgICAgaWYocmVzdWx0LnN1Y2Nlc3MpIHtcbiAgICAgICAgICAgICAgICBcdCQoJyNhZGROZXdHcm91cE1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdFx0IFx0XHR0b2FzdHIuc3VjY2VzcyhyZXN1bHQubWVzc2FnZSk7XG5cblx0XHRcdCBcdFx0dGhpcy5mb3JtLmdyb3VwTmFtZSA9ICcnO1xuXHRcdFx0IFx0XHR0aGlzLmNsZWFyTGVhZGVyKCk7XG5cblx0XHRcdCBcdFx0dGhpcy5sb2FkQ2xpZW50cygpO1xuXHRcdFx0XHR9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICB9KTtcblx0XHR9LFxuXG5cdFx0Y2xlYXJMZWFkZXIoKSB7XG5cdFx0XHR0aGlzLmZvcm0ubGVhZGVySWQgPSAnJztcblxuXHRcdFx0Ly8gdGhpcy5jb250YWN0cyA9IFtdO1xuXHRcdFx0Ly8gdGhpcy5mb3JtLmNvbnRhY3QgPSAnJztcblxuXHRcdFx0Ly8gdGhpcy5hZGRyZXNzT3B0aW9ucyA9ICdjdXJyZW50IGFkZHJlc3MnO1xuXHRcdFx0Ly8gXHR0aGlzLmZvcm0uYWRkcmVzcyA9ICcnO1xuXHRcdFx0Ly8gXHR0aGlzLmZvcm0uYmFyYW5nYXkgPSAnJztcblx0XHRcdC8vIFx0dGhpcy5mb3JtLmNpdHkgPSAnJztcblx0XHRcdC8vIFx0dGhpcy5mb3JtLnByb3ZpbmNlID0gJyc7XG5cdFx0XHQvLyBcdHRoaXMuZm9ybS56aXBDb2RlID0gJyc7XG5cblx0XHRcdHRoaXMubGVhZGVyID0gJyc7XG5cblx0XHRcdCQoJy5sZWFkZXJfdHlwZWFoZWFkJykudmFsKCcnKTtcblx0XHR9XG5cdH0sXG5cblx0Y3JlYXRlZCgpe1xuXHRcdHRoaXMubG9hZENsaWVudHMoKTtcblx0fSxcblxuXHRtb3VudGVkKCkge1xuXHRcdC8vdGhpcy5sb2FkQ2xpZW50cygpO1xuXG5cdFx0dmFyIHZtID0gdGhpcztcblxuXHRcdCQoJy5sZWFkZXJfdHlwZWFoZWFkJykudHlwZWFoZWFkKHtcbiAgICAgICAgICAgIHNvdXJjZTogZnVuY3Rpb24ocXVlcnksIHByb2Nlc3MpIHtcblx0XHQgICAgICBcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9jbGllbnQvZ2V0LXZpc2EtY2xpZW50cz9xPScgKyBxdWVyeSlcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHRyZXR1cm4gcHJvY2VzcyhyZXNwb25zZS5kYXRhKTtcblx0XHRcdFx0XHR9KTtcblx0XHQgICAgfSxcbiAgICAgICAgICAgIG1pbkxlbmd0aDogNCxcbiAgICAgICAgICAgIG1hdGNoZXI6IGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB1cGRhdGVyOiBmdW5jdGlvbihpdGVtKSB7XG4gICAgICAgICAgICBcdHZtLmZvcm0ubGVhZGVySWQgPSBpdGVtLmlkO1xuICAgICAgICAgICAgXHRcbiAgICAgICAgICAgIFx0Ly8gdm0uY29udGFjdHNbMF0gPSBpdGVtLmNvbnRhY3QxO1xuICAgICAgICAgICAgXHQvLyBpZihpdGVtLmNvbnRhY3QyICE9IG51bGwpIHsgdm0uY29udGFjdHNbMV0gPSBpdGVtLmNvbnRhY3QyOyB9XG4gICAgICAgICAgICBcdC8vIHZtLmZvcm0uY29udGFjdCA9IHZtLmNvbnRhY3RzWzBdO1xuXG4gICAgICAgICAgICBcdC8vIHZtLmZvcm0uYWRkcmVzcyA9IGl0ZW0uYWRkcmVzcztcbiAgICAgICAgICAgIFx0Ly8gaWYoaXRlbS5iYXJhbmdheSAhPSBudWxsKSB7IHZtLmZvcm0uYmFyYW5nYXkgPSBpdGVtLmJhcmFuZ2F5OyB9XG4gICAgICAgICAgICBcdC8vIHZtLmZvcm0uY2l0eSA9IGl0ZW0uY2l0eTtcbiAgICAgICAgICAgIFx0Ly8gdm0uZm9ybS5wcm92aW5jZSA9IGl0ZW0ucHJvdmluY2U7XG4gICAgICAgICAgICBcdC8vIGlmKGl0ZW0uemlwQ29kZSAhPSBudWxsKSB7IHZtLmZvcm0uemlwQ29kZSA9IGl0ZW0uemlwQ29kZTsgfVxuXG4gICAgICAgICAgICBcdHZtLmxlYWRlciA9IGl0ZW0ubmFtZTtcbiAgICAgICAgICAgIFx0cmV0dXJuIGl0ZW0ubmFtZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cdH1cblxufSk7XG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuXG5cdGpRdWVyeS5leHRlbmQoIGpRdWVyeS5mbi5kYXRhVGFibGVFeHQub1NvcnQsIHtcblx0ICAgIFwibm9uLWVtcHR5LXN0cmluZy1hc2NcIjogZnVuY3Rpb24gKHN0cjEsIHN0cjIpIHtcblx0ICAgICAgICBpZihzdHIxID09ICc8c3BhbiBzdHlsZT1cImRpc3BsYXk6bm9uZTtcIj4tLTwvc3Bhbj4nKVxuXHQgICAgICAgICAgICByZXR1cm4gMTtcblx0ICAgICAgICBpZihzdHIyID09ICc8c3BhbiBzdHlsZT1cImRpc3BsYXk6bm9uZTtcIj4tLTwvc3Bhbj4nKVxuXHQgICAgICAgICAgICByZXR1cm4gLTE7XG5cdCAgICAgICAgcmV0dXJuICgoc3RyMSA8IHN0cjIpID8gLTEgOiAoKHN0cjEgPiBzdHIyKSA/IDEgOiAwKSk7XG5cdCAgICB9LFxuXHQgXG5cdCAgICBcIm5vbi1lbXB0eS1zdHJpbmctZGVzY1wiOiBmdW5jdGlvbiAoc3RyMSwgc3RyMikge1xuXHQgICAgICAgIGlmKHN0cjEgPT0gJzxzcGFuIHN0eWxlPVwiZGlzcGxheTpub25lO1wiPi0tPC9zcGFuPicpXG5cdCAgICAgICAgICAgIHJldHVybiAxO1xuXHQgICAgICAgIGlmKHN0cjIgPT0gJzxzcGFuIHN0eWxlPVwiZGlzcGxheTpub25lO1wiPi0tPC9zcGFuPicpXG5cdCAgICAgICAgICAgIHJldHVybiAtMTtcblx0ICAgICAgICByZXR1cm4gKChzdHIxIDwgc3RyMikgPyAxIDogKChzdHIxID4gc3RyMikgPyAtMSA6IDApKTtcblx0ICAgIH1cblx0fSk7XG5cblxuXHQkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsXCIuZXhwb3J0RXhjZWxcIixmdW5jdGlvbigpIHtcblx0XHR2YXIgZ3JpZCA9ICQodGhpcykuYXR0cihcImdyb3VwLWlkXCIpO1xuXG4gICAgICBcdHN3YWwoe1xuXHRcdFx0dGl0bGU6IFwiRXhwb3J0IEdyb3VwIHRvIEV4Y2VsP1wiLFxuICAgICAgICAgICAgdHlwZTogJ2luZm8nLFxuICAgICAgICAgICAgYWxsb3dPdXRzaWRlQ2xpY2s6IGZhbHNlLFxuICAgICAgICAgICAgc2hvd0Nsb3NlQnV0dG9uOiB0cnVlLFxuXHRcdFx0aHRtbDogXCJcIiArXG5cdCAgICAgICAgICAgIFwiPGJyPlwiICtcblx0ICAgICAgICAgICAgJzxhIGhyZWY9XCIvdmlzYS9yZXBvcnQvZ2V0LWdyb3VwLXN1bW1hcnkvJytncmlkKydcIiB0YXJnZXQ9XCJfYmxhbmtcIiBjbGFzcz1cImV4cG9ydC1lbmdsaXNoIGJ0biBidG4tc3VjY2VzcyBidG4tbGdcIj4nICsgJzxpbWcgY2xhc3M9XCJkZC1zZWxlY3RlZC1pbWFnZVwiIHNyYz1cIi9zaG9wcGluZy9pbWcvZ2VuZXJhbC9mbGFnL2VuLmpwZ1wiPiBFbmdsaXNoJyArICc8L2E+JyArXG5cdCAgICAgICAgICAgICc8YSBocmVmPVwiL3Zpc2EvcmVwb3J0L2dldC1ncm91cC1zdW1tYXJ5LWNoaW5lc2UvJytncmlkKydcIiB0YXJnZXQ9XCJfYmxhbmtcIiBjbGFzcz1cImV4cG9ydC1jaGluZXNlIGJ0biBidG4tZGFuZ2VyIGJ0bi1sZyBtLWwteHNcIj4nICsgJzxpbWcgY2xhc3M9XCJkZC1zZWxlY3RlZC1pbWFnZVwiIHNyYz1cIi9zaG9wcGluZy9pbWcvZ2VuZXJhbC9mbGFnL2NuLmpwZ1wiPiBNYW5kYXJpbicgKyAnPC9hPicsXG5cdCAgICAgICAgc2hvd0NhbmNlbEJ1dHRvbjogZmFsc2UsXG5cdCAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlXG5cdFx0fSk7XG4gICAgfSk7XG5cbn0pO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvZ3JvdXBzL2luZGV4LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==