/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 482);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['groupreport', 'groupid', 'clientsid', 'clientservices', 'trackings', 'iswritereportpage'],

	data: function data() {
		return {

			docs: [],
			serviceDocs: [],

			multipleQuickReportForm: new Form({
				distinctClientServices: [],

				groupReport: null,
				groupId: null,

				clientsId: [],
				clientServicesId: [],
				trackings: [],

				actionId: [],

				categoryId: [],
				categoryName: [],

				date1: [],
				date2: [],
				date3: [],
				dateTimeArray: [],

				extraDetails: []
			}, { baseURL: axiosAPIv1.defaults.baseURL })

		};
	},


	methods: {
		fetchDocs: function fetchDocs(id) {
			var _this = this;

			var ids = '';
			$.each(id, function (i, item) {
				ids += id[i].docs_needed + ',' + id[i].docs_optional + ',';
			});

			var uniqueList = ids.split(',').filter(function (allItems, i, a) {
				return i == a.indexOf(allItems);
			}).join(',');

			axios.get('/visa/group/' + uniqueList + '/service-docs').then(function (result) {
				_this.docs = result.data;
			});

			setTimeout(function () {
				$('.chosen-select-for-docs').trigger('chosen:updated');
			}, 5000);
		},
		getServiceDocs: function getServiceDocs() {
			var _this2 = this;

			axios.get('/visa/group/service-docs-index').then(function (result) {
				_this2.serviceDocs = result.data;
			});
		},
		resetMultipleQuickReportForm: function resetMultipleQuickReportForm() {
			var _this3 = this;

			// MultipleQuickReportForm.vue
			this.distinctClientServices.forEach(function (distinctClientService, index) {
				var ref = 'form-' + index;

				_this3.$refs[ref][0].multipleQuickReportform = {
					actionId: 1,

					categoryId: null,
					categoryName: null,

					date1: null,
					date2: null,
					date3: null,
					dateTimeArray: [],

					extraDetails: []
				};

				_this3.$refs[ref][0].$refs.date2.value = '';
				_this3.$refs[ref][0].$refs.date3.value = '';
				_this3.$refs[ref][0].$refs.date4.value = '';
				_this3.$refs[ref][0].$refs.date5.value = '';
				_this3.$refs[ref][0].$refs.date6.value = '';
				_this3.$refs[ref][0].$refs.date7.value = '';
				_this3.$refs[ref][0].$refs.date8.value = '';
				_this3.$refs[ref][0].$refs.date9.value = '';
				_this3.$refs[ref][0].$refs.date10.value = '';
				_this3.$refs[ref][0].$refs.date11.value = '';
				_this3.$refs[ref][0].$refs.date12.value = '';
				_this3.$refs[ref][0].$refs.date13.value = '';
				_this3.$refs[ref][0].$refs.date14.value = '';
				_this3.$refs[ref][0].$refs.date15.value = '';
				_this3.$refs[ref][0].$refs.date16.value = '';
				_this3.$refs[ref][0].$refs.categoryName.value = '';

				_this3.$refs[ref][0].customCategory = false;
			});

			// MultipleQuickReport.vue
			this.multipleQuickReportForm = new Form({
				distinctClientServices: [],

				actionId: [],

				categoryId: [],
				categoryName: [],

				date1: [],
				date2: [],
				date3: [],
				dateTimeArray: [],

				extraDetails: []
			}, { baseURL: axiosAPIv1.defaults.baseURL });
		},
		validate: function validate() {
			var _this4 = this;

			var isValid = true;

			this.distinctClientServices.forEach(function (distinctClientService, index) {
				var ref = 'form-' + index;

				if (!_this4.$refs[ref][0].validate(distinctClientService)) {
					isValid = false;
				}
			});

			return isValid;
		},
		addMultipleQuickReport: function addMultipleQuickReport() {
			var _this5 = this;

			if (this.validate()) {
				this.multipleQuickReportForm.groupReport = this.groupreport;
				this.multipleQuickReportForm.groupId = this.groupid;
				this.multipleQuickReportForm.clientsId = this.clientsid;
				this.multipleQuickReportForm.clientServicesId = this.clientservices;
				this.multipleQuickReportForm.trackings = this.trackings;
				this.multipleQuickReportForm.distinctClientServices = this.distinctClientServices;

				this.distinctClientServices.forEach(function (distinctClientService, index) {
					var ref = 'form-' + index;

					var _actionId = _this5.$refs[ref][0].multipleQuickReportform.actionId;
					var _categoryId = _this5.$refs[ref][0].multipleQuickReportform.categoryId;
					var _categoryName = _this5.$refs[ref][0].multipleQuickReportform.categoryName;
					var _date1 = _this5.$refs[ref][0].multipleQuickReportform.date1;
					var _date2 = _this5.$refs[ref][0].multipleQuickReportform.date2;
					var _date3 = _this5.$refs[ref][0].multipleQuickReportform.date3;
					var _dateTimeArray = _this5.$refs[ref][0].multipleQuickReportform.dateTimeArray;
					var _extraDetails = _this5.$refs[ref][0].multipleQuickReportform.extraDetails;

					_this5.multipleQuickReportForm.actionId.push(_actionId);
					_this5.multipleQuickReportForm.categoryId.push(_categoryId);
					_this5.multipleQuickReportForm.categoryName.push(_categoryName);
					_this5.multipleQuickReportForm.date1.push(_date1);
					_this5.multipleQuickReportForm.date2.push(_date2);
					_this5.multipleQuickReportForm.date3.push(_date3);
					_this5.multipleQuickReportForm.dateTimeArray.push(_dateTimeArray);
					_this5.multipleQuickReportForm.extraDetails.push(_extraDetails);
				});

				this.multipleQuickReportForm.submit('post', '/visa/quick-report').then(function (response) {
					if (response.success) {
						_this5.resetMultipleQuickReportForm();

						$('#add-quick-report-modal').modal('hide');

						toastr.success(response.message);

						if (_this5.iswritereportpage) {
							$('#add-quick-report-modal').on('hidden.bs.modal', function (e) {
								window.location.href = '/visa/report/write-report';
							});
						} else {
							setTimeout(function () {
								location.reload();
							}, 1000);
						}
					}
				});
			}
		}
	},

	created: function created() {
		this.getServiceDocs();
	},


	computed: {
		_isWriteReportPage: function _isWriteReportPage() {
			return this.iswritereportpage || false;
		},
		distinctClientServices: function distinctClientServices() {
			var distinctClientServices = [];

			this.clientservices.forEach(function (clientService) {
				if (distinctClientServices.indexOf(clientService.detail) == -1) {
					distinctClientServices.push(clientService.detail);
				}
			});

			return distinctClientServices;
		}
	},

	components: {
		'multiple-quick-report-form': __webpack_require__(15)
	}

});

/***/ }),

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['index', 'docs', 'servicedocs'],

	data: function data() {
		return {

			actions: [],
			categories: [],

			companyCouriers: [],

			customCategory: false,

			multipleQuickReportform: {
				actionId: 1,

				categoryId: null,
				categoryName: null,

				date1: null,
				date2: null,
				date3: null,
				dateTimeArray: [],

				extraDetails: []
			}

		};
	},


	methods: {
		getActions: function getActions() {
			var _this = this;

			axios.get('/visa/actions').then(function (response) {
				_this.actions = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		getCompanyCouriers: function getCompanyCouriers() {
			var _this2 = this;

			var role = 'company-courier';

			axios.get('/visa/users-by-role/' + role).then(function (response) {
				_this2.companyCouriers = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		getCurrentDate: function getCurrentDate() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1;
			var yyyy = today.getFullYear();

			if (dd < 10) {
				dd = '0' + dd;
			}

			if (mm < 10) {
				mm = '0' + mm;
			}

			return mm + '/' + dd + '/' + yyyy;
		},
		changeActionHandler: function changeActionHandler(actionId) {
			// Reset
			this.multipleQuickReportform.date1 = null;
			this.multipleQuickReportform.date2 = null;
			this.multipleQuickReportform.date3 = null;
			this.multipleQuickReportform.dateTimeArray = [];
			this.multipleQuickReportform.extraDetails = [];

			// For auto capture date
			if (actionId == 1 || actionId == 3 || actionId == 4 || actionId == 7 || actionId == 11 || actionId == 15 || actionId == 16) {
				this.multipleQuickReportform.date1 = this.getCurrentDate();
			}
		},
		getCategories: function getCategories() {
			var _this3 = this;

			var actionId = this.multipleQuickReportform.actionId;

			this.changeActionHandler(actionId);

			this.categories = [];

			axios.get('/visa/categories-by-action-id', {
				params: {
					actionId: actionId
				}
			}).then(function (response) {
				_this3.categories = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		changeCategoryHandler: function changeCategoryHandler() {
			if (this.multipleQuickReportform.actionId == 1 && this.multipleQuickReportform.categoryId == 1) {
				this.addDateTimePickerArray();
			}
		},
		addDateTimePickerArray: function addDateTimePickerArray() {
			this.multipleQuickReportform.dateTimeArray.push({ value: null });

			setTimeout(function () {
				$('.datetimepickerarray').inputmask("datetime", {
					mask: "y-2-1 h:s:s",
					placeholder: "yyyy-mm-dd hh:mm:ss",
					leapday: "-02-29",
					separator: "-",
					alias: "yyyy-mm-dd"
				});
			}, 500);
		},
		removeDateTimePickerArray: function removeDateTimePickerArray(index) {
			this.multipleQuickReportform.dateTimeArray.splice(index, 1);
		},
		validate: function validate(distinctClientService) {
			var isValid = true;

			if (this.multipleQuickReportform.actionId == 1) {
				var date2 = this.$refs.date2.value;
				var date3 = this.$refs.date3.value ? this.$refs.date3.value : '';

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date2 == '') {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					var dateTimeArray = [];
					$('input[name^="dateTimeArray"]').each(function (e) {
						if (this.value != '') {
							dateTimeArray.push(this.value);
						}
					});

					this.multipleQuickReportform.date2 = date2;
					if (this.multipleQuickReportform.categoryId == 6 || this.multipleQuickReportform.categoryId == 7 || this.multipleQuickReportform.categoryId == 10 || this.multipleQuickReportform.categoryId == 14 || this.multipleQuickReportform.categoryId == 15) {
						this.multipleQuickReportform.date3 = date3;
					}

					this.multipleQuickReportform.dateTimeArray = dateTimeArray;
				}
			} else if (this.multipleQuickReportform.actionId == 2 || this.multipleQuickReportform.actionId == 3) {
				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				}
			} else if (this.multipleQuickReportform.actionId == 4) {
				var docs = [];
				$('#chosen-' + this.index + ' option:selected').each(function (e) {
					docs.push($(this).val());
				});

				if (docs.length == 0) {
					toastr.error('Please select documents.', distinctClientService);
					isValid = false;
				} else {
					$('#chosen-' + this.index).val('').trigger('chosen:updated');

					this.multipleQuickReportform.extraDetails = docs;
				}
			} else if (this.multipleQuickReportform.actionId == 6) {
				var date4 = this.$refs.date4.value;

				if (date4 == '') {
					toastr.error('Please fill up submission date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date4;
				}
			} else if (this.multipleQuickReportform.actionId == 7) {
				var date5 = this.$refs.date5.value;

				if (date5 == '') {
					toastr.error('Please fill up estimated time of finishing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date5;
				}
			} else if (this.multipleQuickReportform.actionId == 8) {
				var date6 = this.$refs.date6.value;
				var date7 = this.$refs.date7.value;
				var date8 = this.$refs.date8.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date6 == '' || date7 == '') {
					toastr.error('Please fill up affected date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date6;
					this.multipleQuickReportform.date2 = date7;
					this.multipleQuickReportform.date3 = date8;
				}
			} else if (this.multipleQuickReportform.actionId == 9) {
				if (this.customCategory) {
					var categoryName = this.$refs.categoryName.value;

					if (categoryName == '') {
						toastr.error('Please select category.', distinctClientService);
						isValid = false;
					} else {
						this.multipleQuickReportform.categoryId = 0;
						this.multipleQuickReportform.categoryName = categoryName;
					}
				}

				var date9 = this.$refs.date9.value;
				var date10 = this.$refs.date10.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date9 == '' || date10 == '') {
					toastr.error('Please fill up date range.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date9;
					this.multipleQuickReportform.date2 = date10;
				}
			} else if (this.multipleQuickReportform.actionId == 10) {
				var date11 = this.$refs.date11.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date11 == '') {
					toastr.error('Please fill up extended period for processing.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date11;
				}
			} else if (this.multipleQuickReportform.actionId == 11) {
				var date12 = this.$refs.date12.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date12 == '') {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date12;
				}
			} else if (this.multipleQuickReportform.actionId == 12) {
				var date13 = this.$refs.date13.value;

				if (date13 == '') {
					toastr.error('Please fill up date and time pick up.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date13;
				}
			} else if (this.multipleQuickReportform.actionId == 13) {
				var date14 = this.$refs.date14.value;

				if (date14 == '') {
					toastr.error('Please fill up date and time deliver.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date14;
				}
			} else if (this.multipleQuickReportform.actionId == 14) {
				var date15 = this.$refs.date15.value;
				var companyCourier = this.$refs.company_courier.value;
				var trackingNumber = this.$refs.tracking_number.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (this.multipleQuickReportform.categoryId == 128 && companyCourier == '') {
					toastr.error('Please select company courier.', distinctClientService);
					isValid = false;
				} else if (this.multipleQuickReportform.categoryId != 128 && trackingNumber == '') {
					toastr.error('Please fill up tracking number.', distinctClientService);
					isValid = false;
				} else if (date15 == '') {
					toastr.error('Please fill up date and time delivered.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date15;

					var extraDetail = this.multipleQuickReportform.categoryId == 128 ? companyCourier : trackingNumber;
					this.multipleQuickReportform.extraDetails = [extraDetail];
				}
			} else if (this.multipleQuickReportform.actionId == 15) {
				var date16 = this.$refs.date16.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date16 == '' && (this.multipleQuickReportform.categoryId == 76 || this.multipleQuickReportform.categoryId == 77 || this.multipleQuickReportform.categoryId == 78 || this.multipleQuickReportform.categoryId == 79 || this.multipleQuickReportform.categoryId == 80 || this.multipleQuickReportform.categoryId == 81 || this.multipleQuickReportform.categoryId == 82 || this.multipleQuickReportform.categoryId == 83)) {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date16;
				}
			} else if (this.multipleQuickReportform.actionId == 16) {
				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				}
			}

			return isValid;
		}
	},

	created: function created() {
		this.getActions();

		this.getCategories();

		this.getCompanyCouriers();
	},
	mounted: function mounted() {

		// Chosen
		$('.chosen-select-for-docs, .chosen-select-for-docs-2').chosen({
			width: "100%",
			no_results_text: "No result/s found.",
			search_contains: true
		});

		// DatePicker
		$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			autoclose: true,
			format: "mm/dd/yyyy",
			startDate: new Date()
		});

		// DateTime Picker
		$('.datetimepicker').inputmask("datetime", {
			mask: "y-2-1 h:s:s",
			placeholder: "yyyy-mm-dd hh:mm:ss",
			leapday: "-02-29",
			separator: "-",
			alias: "yyyy-mm-dd"
		});
	}
});

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-0f2f2b77] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-0f2f2b77] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["MultipleQuickReport.vue?35f996b9"],"names":[],"mappings":";AA4PA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"MultipleQuickReport.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"addMultipleQuickReport\">\n\n\t\t<div class=\"ios-scroll\">\n\t\t\t<div v-for=\"(distinctClientService, index) in distinctClientServices.length\">\n\n\t\t\t\t<div class=\"panel panel-default\">\n\t                <div class=\"panel-heading\">\n\t                    {{ distinctClientServices[index] }}\n\t                </div>\n\t                <div class=\"panel-body\">\n\t                    \n\t                \t<multiple-quick-report-form :key=\"index\" :ref=\"'form-' + index\" :docs=\"docs\" :servicedocs=\"serviceDocs\" :index=\"index\"></multiple-quick-report-form>\n\n\t                </div> <!-- panel-body -->\n\t            </div> <!-- panel -->\n\n\t\t\t</div> <!-- loop -->\n\t\t</div> <!-- ios-scroll -->\n\t\t\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Create Report</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\t</form>\n\n</template>\n\n<script>\n\n\texport default {\n\n\t\tprops: ['groupreport', 'groupid', 'clientsid', 'clientservices', 'trackings', 'iswritereportpage'],\n\n\t\tdata() {\n\t\t\treturn {\n\t\t\n\t\t\t\tdocs: [],\n\t\t\t\tserviceDocs: [],\n\n        \t\tmultipleQuickReportForm : new Form({\n        \t\t\tdistinctClientServices:[],\n\n        \t\t\tgroupReport: null,\n        \t\t\tgroupId: null,\n\n        \t\t\tclientsId: [],\n        \t\t\tclientServicesId: [],\n        \t\t\ttrackings: [],\n\n        \t\t\tactionId: [],\n\n        \t\t\tcategoryId: [],\n        \t\t\tcategoryName: [],\n\n        \t\t\tdate1: [],\n        \t\t\tdate2: [],\n        \t\t\tdate3: [],\n        \t\t\tdateTimeArray: [],\n\n        \t\t\textraDetails: []\n        \t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\n\t\t\tfetchDocs(id) {\n\t\t\t\tvar ids = '';\n\t\t\t\t$.each(id, function(i, item) {\n\t\t\t\t    ids += id[i].docs_needed + ',' + id[i].docs_optional + ',';\n\t\t\t\t});\n\n\t\t\t\tvar uniqueList=ids.split(',').filter(function(allItems,i,a){\n\t\t\t\t    return i==a.indexOf(allItems);\n\t\t\t\t}).join(',');\n\t        \t\n\t\t\t \taxios.get('/visa/group/'+uniqueList+'/service-docs')\n\t\t\t \t  .then(result => {\n\t\t            this.docs = result.data;\n\t\t        });\n\n\t        \tsetTimeout(() => {\n\t        \t\t$('.chosen-select-for-docs').trigger('chosen:updated');\t\n\t        \t}, 5000);\n\t\t\t},\n\n\t\t\tgetServiceDocs() {\n        \t\taxios.get('/visa/group/service-docs-index')\n\t\t\t \t  .then(result => {\n\t\t            this.serviceDocs = result.data;\n\t\t        });\n        \t},\n\n\t\t\tresetMultipleQuickReportForm() {\n\t\t\t\t// MultipleQuickReportForm.vue\n\t\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\t\tthis.$refs[ref][0].multipleQuickReportform = {\n\t\t        \t\t\tactionId: 1,\n\n\t\t        \t\t\tcategoryId: null,\n\t\t        \t\t\tcategoryName: null,\n\n\t\t        \t\t\tdate1: null,\n\t\t        \t\t\tdate2: null,\n\t\t        \t\t\tdate3: null,\n\t\t        \t\t\tdateTimeArray: [],\n\n\t\t        \t\t\textraDetails: []\n\t\t        \t\t}\n\n\t\t        \t\tthis.$refs[ref][0].$refs.date2.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date3.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date4.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date5.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date6.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date7.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date8.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date9.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date10.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date11.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date12.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date13.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date14.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date15.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date16.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.categoryName.value = '';\n\n\t\t        \t\tthis.$refs[ref][0].customCategory = false;\n\t\t\t\t\t});\n\n        \t\t// MultipleQuickReport.vue\n\t        \t\tthis.multipleQuickReportForm = new Form({\n\t        \t\t\tdistinctClientServices:[],\n\n\t        \t\t\tactionId: [],\n\n\t        \t\t\tcategoryId: [],\n\t        \t\t\tcategoryName: [],\n\n\t        \t\t\tdate1: [],\n\t        \t\t\tdate2: [],\n\t        \t\t\tdate3: [],\n\t        \t\t\tdateTimeArray: [],\n\n\t        \t\t\textraDetails: []\n\t        \t\t}, { baseURL: axiosAPIv1.defaults.baseURL });\n\t\t\t},\n\n\t\t\tvalidate() {\n\t\t\t\tvar isValid = true;\n\n\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\tif( ! this.$refs[ref][0].validate(distinctClientService) ) {\n\t\t\t\t\t\tisValid = false;\n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\treturn isValid;\n\t\t\t},\n\n\t\t\taddMultipleQuickReport() {\n\t\t\t\tif(this.validate()) {\n\t\t\t\t\tthis.multipleQuickReportForm.groupReport = this.groupreport;\n\t\t\t\t\tthis.multipleQuickReportForm.groupId = this.groupid;\n\t\t\t\t\tthis.multipleQuickReportForm.clientsId = this.clientsid;\n\t\t\t\t\tthis.multipleQuickReportForm.clientServicesId = this.clientservices;\n\t\t\t\t\tthis.multipleQuickReportForm.trackings = this.trackings;\n\t\t\t\t\tthis.multipleQuickReportForm.distinctClientServices = this.distinctClientServices;\n\n\t\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\t\tlet _actionId = this.$refs[ref][0].multipleQuickReportform.actionId;\n\t\t\t\t\t\tlet _categoryId = this.$refs[ref][0].multipleQuickReportform.categoryId;\n\t\t\t\t\t\tlet _categoryName = this.$refs[ref][0].multipleQuickReportform.categoryName;\n\t\t\t\t\t\tlet _date1 = this.$refs[ref][0].multipleQuickReportform.date1;\n\t\t\t\t\t\tlet _date2 = this.$refs[ref][0].multipleQuickReportform.date2;\n\t\t\t\t\t\tlet _date3 = this.$refs[ref][0].multipleQuickReportform.date3;\n\t\t\t\t\t\tlet _dateTimeArray = this.$refs[ref][0].multipleQuickReportform.dateTimeArray;\n\t\t\t\t\t\tlet _extraDetails = this.$refs[ref][0].multipleQuickReportform.extraDetails;\n\n\t\t\t\t\t\tthis.multipleQuickReportForm.actionId.push(_actionId);\n\t\t\t\t\t\tthis.multipleQuickReportForm.categoryId.push(_categoryId);\n\t        \t\t\tthis.multipleQuickReportForm.categoryName.push(_categoryName);\n\t        \t\t\tthis.multipleQuickReportForm.date1.push(_date1);\n\t        \t\t\tthis.multipleQuickReportForm.date2.push(_date2);\n\t        \t\t\tthis.multipleQuickReportForm.date3.push(_date3);\n\t        \t\t\tthis.multipleQuickReportForm.dateTimeArray.push(_dateTimeArray);\n\t        \t\t\tthis.multipleQuickReportForm.extraDetails.push(_extraDetails);\n\t\t\t\t\t});\n\n\t\t\t\t\tthis.multipleQuickReportForm.submit('post', '/visa/quick-report')\n\t\t\t            .then(response => {\n\t\t\t            \tif(response.success) {\n\t\t\t            \t\tthis.resetMultipleQuickReportForm();\n\t\t\t            \t\t\n\t\t\t            \t\t$('#add-quick-report-modal').modal('hide');\n\n\t\t\t            \t\ttoastr.success(response.message);\n\n\t\t\t            \t\tif(this.iswritereportpage) {\n\t\t\t            \t\t\t$('#add-quick-report-modal').on('hidden.bs.modal', function (e) {\n\t\t\t\t\t\t\t\t\t  \twindow.location.href = '/visa/report/write-report';\n\t\t\t\t\t\t\t\t\t});\n\t\t\t            \t\t} else {\n\t\t\t            \t\t\tsetTimeout(() => {\n\t\t\t\t            \t\t\tlocation.reload();\n\t\t\t\t            \t\t}, 1000);\n\t\t\t            \t\t}\n\t\t\t            \t}\n\t\t\t            });\n\t\t\t\t}\n\t\t\t}\n\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.getServiceDocs();\n\t\t},\n\n\t\tcomputed: {\n\t\t\t_isWriteReportPage() {\n\t\t      \treturn this.iswritereportpage || false;\n\t\t    },\n\n\t\t\tdistinctClientServices() {\n\t\t\t\tlet distinctClientServices = [];\n\n\t\t\t\tthis.clientservices.forEach(clientService => {\n\t\t\t\t\tif(distinctClientServices.indexOf(clientService.detail) == -1) {\n\t\t\t\t\t\tdistinctClientServices.push(clientService.detail);\n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\treturn distinctClientServices;\n\t\t\t}\n\t\t},\n\n\t\tcomponents: {\n\t        'multiple-quick-report-form': require('./MultipleQuickReportForm.vue')\n\t    }\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(20)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(10),
  /* template */
  __webpack_require__(16),
  /* scopeId */
  "data-v-0f2f2b77",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/MultipleQuickReport.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] MultipleQuickReport.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f2f2b77", Component.options)
  } else {
    hotAPI.reload("data-v-0f2f2b77", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(11),
  /* template */
  __webpack_require__(18),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/MultipleQuickReportForm.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] MultipleQuickReportForm.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-eb2fda4a", Component.options)
  } else {
    hotAPI.reload("data-v-eb2fda4a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addMultipleQuickReport($event)
      }
    }
  }, [_c('div', {
    staticClass: "ios-scroll"
  }, _vm._l((_vm.distinctClientServices.length), function(distinctClientService, index) {
    return _c('div', [_c('div', {
      staticClass: "panel panel-default"
    }, [_c('div', {
      staticClass: "panel-heading"
    }, [_vm._v("\n                    " + _vm._s(_vm.distinctClientServices[index]) + "\n                ")]), _vm._v(" "), _c('div', {
      staticClass: "panel-body"
    }, [_c('multiple-quick-report-form', {
      key: index,
      ref: 'form-' + index,
      refInFor: true,
      attrs: {
        "docs": _vm.docs,
        "servicedocs": _vm.serviceDocs,
        "index": index
      }
    })], 1)])])
  }), 0), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Create Report")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0f2f2b77", module.exports)
  }
}

/***/ }),

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.actionId),
      expression: "multipleQuickReportform.actionId"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "action"
    },
    on: {
      "change": [function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.multipleQuickReportform, "actionId", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }, _vm.getCategories]
    }
  }, _vm._l((_vm.actions), function(action) {
    return _c('option', {
      domProps: {
        "value": action.id
      }
    }, [_vm._v("\n\t\t           \t\t" + _vm._s(action.name) + "\n\t\t            ")])
  }), 0)])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 1 || _vm.multipleQuickReportform.actionId == 2 || _vm.multipleQuickReportform.actionId == 3 || _vm.multipleQuickReportform.actionId == 8 || _vm.multipleQuickReportform.actionId == 9 || _vm.multipleQuickReportform.actionId == 10 || _vm.multipleQuickReportform.actionId == 11 || _vm.multipleQuickReportform.actionId == 14 || _vm.multipleQuickReportform.actionId == 15 || _vm.multipleQuickReportform.actionId == 16),
      expression: "multipleQuickReportform.actionId == 1 || multipleQuickReportform.actionId == 2 || multipleQuickReportform.actionId == 3 || multipleQuickReportform.actionId == 8 || multipleQuickReportform.actionId == 9 || multipleQuickReportform.actionId == 10 || multipleQuickReportform.actionId == 11 || multipleQuickReportform.actionId == 14 || multipleQuickReportform.actionId == 15 || multipleQuickReportform.actionId == 16"
    }],
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.categoryId),
      expression: "multipleQuickReportform.categoryId"
    }, {
      name: "show",
      rawName: "v-show",
      value: (!_vm.customCategory),
      expression: "!customCategory"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "category"
    },
    on: {
      "change": [function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.multipleQuickReportform, "categoryId", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }, _vm.changeCategoryHandler]
    }
  }, _vm._l((_vm.categories), function(category) {
    return _c('option', {
      domProps: {
        "value": category.id
      }
    }, [_vm._v("\n\t\t            \t" + _vm._s(category.name) + "\n\t\t            ")])
  }), 0), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 9),
      expression: "multipleQuickReportform.actionId == 9"
    }]
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.categoryName),
      expression: "multipleQuickReportform.categoryName"
    }, {
      name: "show",
      rawName: "v-show",
      value: (_vm.customCategory),
      expression: "customCategory"
    }],
    ref: "categoryName",
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "placeholder": "Enter category"
    },
    domProps: {
      "value": (_vm.multipleQuickReportform.categoryName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.multipleQuickReportform, "categoryName", $event.target.value)
      }
    }
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-sm btn-primary pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.customCategory = !_vm.customCategory;
        _vm.multipleQuickReportform.categoryId = null
      }
    }
  }, [_c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (!_vm.customCategory),
      expression: "!customCategory"
    }]
  }, [_vm._v("Use Custom Category")]), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.customCategory),
      expression: "customCategory"
    }]
  }, [_vm._v("Use Predefined Category")])])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 1),
      expression: "multipleQuickReportform.actionId == 1"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(3), _vm._v(" "), _c('input', {
    ref: "date2",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date2",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 1),
      expression: "multipleQuickReportform.categoryId == 1"
    }],
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tScheduled Hearing Date and Time:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_vm._l((_vm.multipleQuickReportform.dateTimeArray), function(dateTimeElement, index) {
    return _c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "col-md-11"
    }, [_c('div', {
      staticClass: "input-group date",
      staticStyle: {
        "margin-bottom": "10px"
      }
    }, [_vm._m(4, true), _vm._v(" "), _c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dateTimeElement.value),
        expression: "dateTimeElement.value"
      }],
      staticClass: "form-control datetimepickerarray",
      attrs: {
        "type": "text",
        "name": "dateTimeArray[]",
        "placeholder": "yyyy-mm-dd hh:mm:ss",
        "autocomplete": "off"
      },
      domProps: {
        "value": (dateTimeElement.value)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(dateTimeElement, "value", $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "col-md-1 text-center"
    }, [_c('button', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (index != 0),
        expression: "index != 0"
      }],
      staticClass: "btn btn-danger btn-sm",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.removeDateTimePickerArray(index)
        }
      }
    }, [_c('i', {
      staticClass: "fa fa-times"
    })])])])
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-sm pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.addDateTimePickerArray
    }
  }, [_c('i', {
    staticClass: "fa fa-plus"
  }), _vm._v(" Add\n                    ")])], 2)]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 6 || _vm.multipleQuickReportform.categoryId == 7 || _vm.multipleQuickReportform.categoryId == 10 || _vm.multipleQuickReportform.categoryId == 14 || _vm.multipleQuickReportform.categoryId == 15),
      expression: "multipleQuickReportform.categoryId == 6 || multipleQuickReportform.categoryId == 7 || multipleQuickReportform.categoryId == 10 || multipleQuickReportform.categoryId == 14 || multipleQuickReportform.categoryId == 15"
    }],
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tScheduled Appointment Date and Time:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(5), _vm._v(" "), _c('input', {
    ref: "date3",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date3",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 2),
      expression: "multipleQuickReportform.actionId == 2"
    }]
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 3),
      expression: "multipleQuickReportform.actionId == 3"
    }]
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 4),
      expression: "multipleQuickReportform.actionId == 4"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-docs-2",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "id": 'chosen-' + _vm.index,
      "data-placeholder": "Select Docs",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.servicedocs), function(servicedoc) {
    return _c('option', {
      domProps: {
        "value": servicedoc.title
      }
    }, [_vm._v("\n\t\t                \t" + _vm._s((servicedoc.title).trim()) + "\n\t\t                ")])
  }), 0)])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 6),
      expression: "multipleQuickReportform.actionId == 6"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(8), _vm._v(" "), _c('input', {
    ref: "date4",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date4",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tConsequence of non submission:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Additional cost maybe charged."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Additional cost maybe charged.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Additional cost maybe charged.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tAdditional cost maybe charged.\n\t\t\t    \t\t")])]), _vm._v(" "), _c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Application maybe forfeited and has to re-apply."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Application maybe forfeited and has to re-apply.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Application maybe forfeited and has to re-apply.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tApplication maybe forfeited and has to re-apply.\n\t\t\t    \t\t")])]), _vm._v(" "), _c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Initial deposit maybe forfeited."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Initial deposit maybe forfeited.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Initial deposit maybe forfeited.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tInitial deposit maybe forfeited.\n\t\t\t    \t\t")])])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 7),
      expression: "multipleQuickReportform.actionId == 7"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(10), _vm._v(" "), _c('input', {
    ref: "date5",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date5",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 8),
      expression: "multipleQuickReportform.actionId == 8"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(11), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-daterange input-group"
  }, [_c('input', {
    ref: "date6",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date6",
      "autocomplete": "off"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("to")]), _vm._v(" "), _c('input', {
    ref: "date7",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date7",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tTarget Filling Date:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(12), _vm._v(" "), _c('input', {
    ref: "date8",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date8",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 9),
      expression: "multipleQuickReportform.actionId == 9"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(13), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-daterange input-group"
  }, [_c('input', {
    ref: "date9",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date9",
      "autocomplete": "off"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("to")]), _vm._v(" "), _c('input', {
    ref: "date10",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date10",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 10),
      expression: "multipleQuickReportform.actionId == 10"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(14), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(15), _vm._v(" "), _c('input', {
    ref: "date11",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date11",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 11),
      expression: "multipleQuickReportform.actionId == 11"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(16), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(17), _vm._v(" "), _c('input', {
    ref: "date12",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date12",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 12),
      expression: "multipleQuickReportform.actionId == 12"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(18), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(19), _vm._v(" "), _c('input', {
    ref: "date13",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date13",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 13),
      expression: "multipleQuickReportform.actionId == 13"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(20), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(21), _vm._v(" "), _c('input', {
    ref: "date14",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date14",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 14),
      expression: "multipleQuickReportform.actionId == 14"
    }]
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 128),
      expression: "multipleQuickReportform.categoryId == 128"
    }],
    staticClass: "form-group"
  }, [_vm._m(22), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    ref: "company_courier",
    staticClass: "form-control",
    attrs: {
      "id": "company_courier"
    }
  }, _vm._l((_vm.companyCouriers), function(companyCourier) {
    return _c('option', {
      domProps: {
        "value": companyCourier.address.contact_number
      }
    }, [_vm._v("\n\t\t\t        \t\t" + _vm._s(companyCourier.first_name + ' ' + companyCourier.last_name) + " \n\t\t\t        \t\t(" + _vm._s(companyCourier.address.contact_number) + ")\n\t\t\t        \t")])
  }), 0)])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId != 128),
      expression: "multipleQuickReportform.categoryId != 128"
    }],
    staticClass: "form-group"
  }, [_vm._m(23), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    ref: "tracking_number",
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "tracking_number"
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(24), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(25), _vm._v(" "), _c('input', {
    ref: "date15",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date15",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 15),
      expression: "multipleQuickReportform.actionId == 15"
    }]
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 76 || _vm.multipleQuickReportform.categoryId == 77 || _vm.multipleQuickReportform.categoryId == 78 || _vm.multipleQuickReportform.categoryId == 79 || _vm.multipleQuickReportform.categoryId == 80 || _vm.multipleQuickReportform.categoryId == 81 || _vm.multipleQuickReportform.categoryId == 82 || _vm.multipleQuickReportform.categoryId == 83),
      expression: "multipleQuickReportform.categoryId == 76 || multipleQuickReportform.categoryId == 77 || multipleQuickReportform.categoryId == 78 || multipleQuickReportform.categoryId == 79 || multipleQuickReportform.categoryId == 80 || multipleQuickReportform.categoryId == 81 || multipleQuickReportform.categoryId == 82 || multipleQuickReportform.categoryId == 83"
    }],
    staticClass: "form-group"
  }, [_vm._m(26), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(27), _vm._v(" "), _c('input', {
    ref: "date16",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date16",
      "autocomplete": "off"
    }
  })])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Action: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Category: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDocuments: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tSubmission Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Time of Finishing: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tAffected Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate Range: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tExtended period for processing: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Pick Up: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Deliver: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tCompany Courier: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tTracking Number: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Delivered: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-eb2fda4a", module.exports)
  }
}

/***/ }),

/***/ 192:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('action-logs', __webpack_require__(29));
Vue.component('transaction-logs', __webpack_require__(30));

var data = window.Laravel.user;

new Vue({

	el: '#app',

	data: {

		groupId: null, // Ok

		details: null, // Ok

		members: null, // Ok

		serviceCost: [], // Ok

		seLclientId: '',

		assignLeaderForm: new Form({ // Ok
			clientId: '',
			type: ''
		}, { baseURL: axiosAPIv1.defaults.baseURL }),

		quickReportClientsId: [],
		quickReportClientServicesId: [],
		quickReportTrackings: [],

		actionLogs: [],
		current_year: null,
		current_month: null,
		current_day: null,
		c_year: null,
		c_month: null,
		c_day: null,
		s_count: 1,

		transactionLogs: [],
		current_year2: null,
		current_month2: null,
		current_day2: null,
		c_year2: null,
		c_month2: null,
		c_day2: null,
		s_count2: 1,

		deposits: [],
		payments: [],
		refunds: [],
		discounts: [],

		gServices: null,
		gBatch: null,

		docsrcv: null,

		setting: data.access_control[0].setting,
		permissions: data.permissions,
		selpermissions: [{
			editAddress: 0,
			actionLogs: 0,
			transactionLogs: 0,
			deposits: 0,
			payments: 0,
			refunds: 0,
			discounts: 0,
			newMember: 0,
			addNewService: 0,
			addFunds: 0,
			writeReport: 0,
			makeLeader: 0,
			transfer: 0,
			delete: 0,
			editService: 0
		}]

	},

	methods: {
		// Ok
		init: function init() {

			var pathArray = window.location.pathname.split('/');
			this.groupId = pathArray[3];

			this.fetchDetails();
			this.fetchMembers();

			this.fetchGServices();
			this.fetchGBatch();
		},


		// Ok
		fetchDetails: function fetchDetails() {
			var _this = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/summary').then(function (response) {
				_this.details = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},


		// Ok
		fetchMembers: function fetchMembers() {
			var _this2 = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/members').then(function (response) {
				_this2.members = response.data.groupmember;

				_this2.$refs.clientservicesref.createDatatable('membersDatatable');

				_this2.computeServiceCost(_this2.members);

				_this2.createDatatableToggleRow(_this2.members);
			}).catch(function (error) {
				return console.log(error);
			});
		},


		// Ok
		fetchDocs: function fetchDocs(id) {
			this.$refs.multiplequickreportref.fetchDocs(id);
		},


		// Ok
		computeServiceCost: function computeServiceCost(members) {
			var groupId = this.groupId;

			this.serviceCost = members.map(function (member) {
				var cost = 0;
				var charge = 0;
				var tip = 0;

				member.services.map(function (service) {
					if (service.active == 1 && service.group_id == groupId) {
						cost += parseFloat(service.cost);
						charge += parseFloat(service.charge);
						tip += parseFloat(service.tip);
					}
				});

				return cost + charge + tip;
			});
		},


		// Ok
		fetchGServices: function fetchGServices() {
			var _this3 = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/services').then(function (response) {
				//console.log(response.data);
				_this3.gServices = response.data;

				_this3.createDatatableToggleByService(_this3.gServices);
			}).catch(function (error) {
				return console.log(error);
			});
		},
		fetchGBatch: function fetchGBatch() {
			var _this4 = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/batch').then(function (response) {
				//console.log(response.data);
				_this4.gBatch = response.data;

				_this4.createDatatableToggleByBatch(_this4.gBatch);
			}).catch(function (error) {
				return console.log(error);
			});
		},


		// Ok
		assignLeader: function assignLeader(clientId, type) {
			var _this5 = this;

			var groupId = this.groupId;

			this.assignLeaderForm.clientId = clientId;
			this.assignLeaderForm.type = type;

			this.assignLeaderForm.submit('patch', '/visa/group/' + groupId + '/assign-leader').then(function (result) {
				if (result.success) {

					_this5.fetchMembers();
					_this5.fetchDetails();
					$('#vice-leader-modal').modal('hide');
					toastr.success(result.message);
				}
			}).catch(function (error) {
				return console.log(error);
			});

			this.seLclientId = "";
		},


		// Ok
		showMemberRow: function showMemberRow(id, e) {
			if (e.target.nodeName != 'A') {
				setTimeout(function () {
					$('#member-row-' + id).toggleClass('hide').removeAttr('style');
				}, 500);
			}
		},
		createDatatable: function createDatatable(className) {
			$('table.' + className).DataTable().destroy();

			setTimeout(function () {
				$('table.' + className).DataTable({
					pageLength: 10,
					responsive: true,
					"aaSorting": [],
					dom: '<"top"lf>rt<"bottom"ip><"clear">'
				});
			}, 500);
		},
		filterCurrency: function filterCurrency(value) {
			if (value != null) {
				if (value % 1 != 0) {
					return this.numberWithCommas(parseFloat(value).toFixed(2));
				} else {
					return this.numberWithCommas(parseInt(value));
				}
			} else {
				return 0;
			}
		},
		numberWithCommas: function numberWithCommas(value) {
			return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},
		createQuickReport: function createQuickReport(clientId, clientServiceId, tracking) {
			var _this6 = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/client-service/' + clientServiceId).then(function (response) {
				var clientService = response.data;

				_this6.quickReportClientsId = [clientId];
				_this6.quickReportClientServicesId = clientService;
				_this6.quickReportTrackings = [tracking];

				_this6.fetchDocs(clientService);

				$('#add-quick-report-modal').modal('show');
			}).catch(function (error) {
				return console.log(error);
			});
		},
		createDatatableToggleRow: function createDatatableToggleRow(data) {
			var _this7 = this;

			var groupId = this.groupId;

			var serviceCost = data.map(function (member) {
				var cost = 0;
				var charge = 0;
				var tip = 0;

				member.services.map(function (service) {
					if (service.active == 1 && service.group_id == groupId) {
						cost += parseFloat(service.cost);
						charge += parseFloat(service.charge);
						tip += parseFloat(service.tip);
					}
				});

				return _this7.filterCurrency(cost + charge + tip);
			});

			$('table.dt-members').DataTable().destroy();

			var vi = this;

			setTimeout(function () {

				var format = function (d) {
					var cid = null;
					var table = '<table class="table table-bordered table-striped table-hover">' + '<thead>' + '<tr>' + '<th>Package</th>' + '<th>Date Recorded</th>' + '<th>Details</th>' + '<th>Status</th>' + '<th>Cost</th>' + '<th>Service Fee</th>' + '<th>Additional Fee</th>' + '<th>Discount</th>' + '<th class="text-center">Action</th>' + '</tr>' + '</thead>' + '<tbody>';

					for (var i = 0; i < d.services.length; i++) {
						cid = d.client.document_receive;
						var clientServiceId = JSON.parse(JSON.stringify(d.services[i])).id;

						var className = d.services[i].active == 0 ? 'strikethrough' : '';
						var visibility = d.services[i].group_id == groupId ? 'table-row' : 'none';
						if (d.services[i].remarks) {
							var remarks = d.services[i].remarks;
							var details = '<a href="#" data-toggle="popover" data-placement="right" data-container="body" data-content="' + remarks + '" data-trigger="hover">' + d.services[i].detail + '</a>';
						} else {
							var details = d.services[i].detail;
						}
						if (d.services[i].discount2.length > 0) {
							if (d.services[i].discount2[0].reason) {
								var reason = d.services[i].discount2[0].reason;
								var discount = '<a href="#" data-toggle="popover" data-placement="left" data-container="body" data-content="' + reason + '" data-trigger="hover">' + d.services[i].discount2[0].discount_amount + '</a>';
							} else {
								var discount = d.services[i].discount2[0].discount_amount;
							}
						} else {
							var discount = '&nbsp;';
						}
						var _action = '<a href="javascript:void(0)" class="edit-service-action" data-toggle="modal" data-target="#edit-service-modal" data-serviceclientid="' + d.services[i].client_id + '" data-serviceserviceid="' + d.services[i].service_id + '" data-serviceid="' + d.services[i].id + '"><i class="fa fa-pencil-square-o" data-serviceclientid="' + d.services[i].client_id + '" data-serviceserviceid="' + d.services[i].service_id + '" data-serviceid="' + d.services[i].id + '"></i></a> &nbsp; <a href="javascript:void(0)" class="write-report-action" data-clientid="' + d.services[i].client_id + '" data-tracking="' + d.services[i].tracking + '" data-clientserviceid="' + clientServiceId + '"><i class="fa fa-list-alt" data-clientid="' + d.services[i].client_id + '" data-tracking="' + d.services[i].tracking + '" data-clientserviceid="' + clientServiceId + '"></i></a>';

						table += '<tr class="' + className + '" style="display:' + visibility + '">';
						table += '<td>' + d.services[i].tracking + '</td>';
						table += '<td>' + d.services[i].service_date + '</td>';
						table += '<td>' + details + '</td>';
						table += '<td>' + d.services[i].status + '</td>';
						table += '<td>' + d.services[i].cost + '</td>';
						table += '<td>' + d.services[i].charge + '</td>';
						table += '<td>' + d.services[i].tip + '</td>';
						table += '<td>' + discount + '</td>';
						table += '<td class="text-center">' + _action + '</td>';

						table += '</tr>';
					}

					table += '</tbody></table>';

					var rcv = cid;
					var docrcv = '<ul class="tag-list" style="padding: 0">';
					for (var i = 0; i < rcv.length; i++) {
						docrcv = docrcv + '<li><a style="cursor:default;"><i class="fa fa-tag"></i>' + rcv[i] + '</a></li>';
					}
					docrcv = docrcv + '</ul>';

					return docrcv + table;
				}.bind(this);

				//if(this.selpermissions.makeLeader == 1 && this.selpermissions.transfer == 1 && this.selpermissions.delete == 1){
				table = $('table.dt-members').DataTable({
					"data": data,
					"columns": [{
						"width": "5%",
						"className": 'details-control text-center',
						"orderable": false,
						"data": null,
						"defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
					}, {
						"width": "30%",
						data: "client", render: function render(data, type, row) {
							return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>';
						}
					}, {
						"width": "15%",
						data: "client", render: function render(data, type, row) {
							return data.id;
						}
					}, {
						"width": "15%",
						data: null, render: function render(data, type, row, meta) {
							return serviceCost[meta.row];
						}
					}, {
						data: null, render: function render(data, type, row) {
							if (data.leader == 1) {
								return '<span><i class="fa fa-star"></i><i class="fa fa-star"></i> Group Leader</span>';
							} else if (data.leader == 2) {
								return '<span><a href="javascript:void(0)" class="vice-leader-action" data-id="' + data.client.id + '"><i class="fa fa-star"></i> Vice Leader</a></span>';
							} else {
								return '<span><a href="javascript:void(0)" class="make-leader-action" data-id="' + data.client.id + '"><i class="fa fa-star-o"></i> Make Vice----Leader</a></span>';
							}
						}, className: 'text-center'
					}, {
						data: null, render: function render(data, type, row) {
							return '<a href="javascript:void(0)" class="transfer-action" data-toggle="modal" data-target="#transfer-modal" data-id="' + data.client.id + '">Transfer</a>';
						}, className: 'text-center'
					}, {
						data: null, render: function render(data, type, row) {
							return '<a href="javascript:void(0)" class="delete-action" data-toggle="modal" data-target="#delete-member-modal" data-id="' + data.client.id + '">Delete</a>';
						}, className: 'text-center'
					}]
				});
				//}

				//      if(this.selpermissions.makeLeader == 0 && this.selpermissions.transfer == 1 && this.selpermissions.delete == 1){
				// table = $('table.dt-members').DataTable( {
				//        "data": data,
				//        "columns": [
				//            {
				//            	"width": "5%",
				//                "className":      'details-control text-center',
				//                "orderable":      false,
				//                "data":           null,
				//                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				//            },
				//            { 
				//            	"width": "30%",
				//            	data: "client", render: function(data, type, row) {
				//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
				//             }
				//         },
				//         {
				//            	"width": "15%",
				//         	data: "client", render: function(data, type, row) {
				//         		return data.id
				//         	}
				//         },
				//         {
				//            	"width": "15%",
				//         	data: null, render: function(data, type, row, meta) {
				//         		return serviceCost[meta.row]
				//         	}
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//         		return '<a href="javascript:void(0)" class="transfer-action" data-toggle="modal" data-target="#transfer-modal" data-id="'+data.client.id+'">Transfer</a>'
				//             }, className: 'text-center'
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//         		return '<a href="javascript:void(0)" class="delete-action" data-toggle="modal" data-target="#delete-member-modal" data-id="'+data.client.id+'">Delete</a>'
				//             }, className: 'text-center'
				//         }
				//        ]
				//    });
				//      }

				//      if(this.selpermissions.makeLeader == 1 && this.selpermissions.transfer == 0 && this.selpermissions.delete == 1){
				// table = $('table.dt-members').DataTable( {
				//        "data": data,
				//        "columns": [
				//            {
				//            	"width": "5%",
				//                "className":      'details-control text-center',
				//                "orderable":      false,
				//                "data":           null,
				//                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				//            },
				//            { 
				//            	"width": "30%",
				//            	data: "client", render: function(data, type, row) {
				//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
				//             }
				//         },
				//         {
				//            	"width": "15%",
				//         	data: "client", render: function(data, type, row) {
				//         		return data.id
				//         	}
				//         },
				//         {
				//            	"width": "15%",
				//         	data: null, render: function(data, type, row, meta) {
				//         		return serviceCost[meta.row]
				//         	}
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//                 if(data.leader == 1) {
				//                 	return '<span><i class="fa fa-star"></i><i class="fa fa-star"></i> Group Leader</span>'
				//                 }
				//                 else if(data.leader == 2) {
				//            			return '<span><a href="javascript:void(0)" class="vice-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star"></i> Vice Leader</a></span>'
				//                 }
				//                  else {
				//                 	return '<span><a href="javascript:void(0)" class="make-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star-o"></i> Make Vice----Leader</a></span>'
				//                 }
				//             }, className: 'text-center'
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//         		return '<a href="javascript:void(0)" class="delete-action" data-toggle="modal" data-target="#delete-member-modal" data-id="'+data.client.id+'">Delete</a>'
				//             }, className: 'text-center'
				//         }
				//        ]
				//    });
				//      }

				//      if(this.selpermissions.makeLeader == 1 && this.selpermissions.transfer == 1 && this.selpermissions.delete == 0){
				// table = $('table.dt-members').DataTable( {
				//        "data": data,
				//        "columns": [
				//            {
				//            	"width": "5%",
				//                "className":      'details-control text-center',
				//                "orderable":      false,
				//                "data":           null,
				//                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				//            },
				//            { 
				//            	"width": "30%",
				//            	data: "client", render: function(data, type, row) {
				//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
				//             }
				//         },
				//         {
				//            	"width": "15%",
				//         	data: "client", render: function(data, type, row) {
				//         		return data.id
				//         	}
				//         },
				//         {
				//            	"width": "15%",
				//         	data: null, render: function(data, type, row, meta) {
				//         		return serviceCost[meta.row]
				//         	}
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//                 if(data.leader == 1) {
				//                 	return '<span><i class="fa fa-star"></i><i class="fa fa-star"></i> Group Leader</span>'
				//                 }
				//                 else if(data.leader == 2) {
				//            			return '<span><a href="javascript:void(0)" class="vice-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star"></i> Vice Leader</a></span>'
				//                 }
				//                  else {
				//                 	return '<span><a href="javascript:void(0)" class="make-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star-o"></i> Make Vice----Leader</a></span>'
				//                 }
				//             }, className: 'text-center'
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//         		return '<a href="javascript:void(0)" class="transfer-action" data-toggle="modal" data-target="#transfer-modal" data-id="'+data.client.id+'">Transfer</a>'
				//             }, className: 'text-center'
				//         }
				//        ]
				//    });
				//      }

				//      if(this.selpermissions.makeLeader == 0 && this.selpermissions.transfer == 0 && this.selpermissions.delete == 1){
				// table = $('table.dt-members').DataTable( {
				//        "data": data,
				//        "columns": [
				//            {
				//            	"width": "5%",
				//                "className":      'details-control text-center',
				//                "orderable":      false,
				//                "data":           null,
				//                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				//            },
				//            { 
				//            	"width": "30%",
				//            	data: "client", render: function(data, type, row) {
				//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
				//             }
				//         },
				//         {
				//            	"width": "15%",
				//         	data: "client", render: function(data, type, row) {
				//         		return data.id
				//         	}
				//         },
				//         {
				//            	"width": "15%",
				//         	data: null, render: function(data, type, row, meta) {
				//         		return serviceCost[meta.row]
				//         	}
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//         		return '<a href="javascript:void(0)" class="delete-action" data-toggle="modal" data-target="#delete-member-modal" data-id="'+data.client.id+'">Delete</a>'
				//             }, className: 'text-center'
				//         }
				//        ]
				//    });
				//      }

				//      if(this.selpermissions.makeLeader == 0 && this.selpermissions.transfer == 1 && this.selpermissions.delete == 0){
				// table = $('table.dt-members').DataTable( {
				//        "data": data,
				//        "columns": [
				//            {
				//            	"width": "5%",
				//                "className":      'details-control text-center',
				//                "orderable":      false,
				//                "data":           null,
				//                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				//            },
				//            { 
				//            	"width": "30%",
				//            	data: "client", render: function(data, type, row) {
				//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
				//             }
				//         },
				//         {
				//            	"width": "15%",
				//         	data: "client", render: function(data, type, row) {
				//         		return data.id
				//         	}
				//         },
				//         {
				//            	"width": "15%",
				//         	data: null, render: function(data, type, row, meta) {
				//         		return serviceCost[meta.row]
				//         	}
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//         		return '<a href="javascript:void(0)" class="transfer-action" data-toggle="modal" data-target="#transfer-modal" data-id="'+data.client.id+'">Transfer</a>'
				//             }, className: 'text-center'
				//         }
				//        ]
				//    });
				//      }

				//      if(this.selpermissions.makeLeader == 1 && this.selpermissions.transfer == 0 && this.selpermissions.delete == 0){
				// table = $('table.dt-members').DataTable( {
				//        "data": data,
				//        "columns": [
				//            {
				//            	"width": "5%",
				//                "className":      'details-control text-center',
				//                "orderable":      false,
				//                "data":           null,
				//                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				//            },
				//            { 
				//            	"width": "30%",
				//            	data: "client", render: function(data, type, row) {
				//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
				//             }
				//         },
				//         {
				//            	"width": "15%",
				//         	data: "client", render: function(data, type, row) {
				//         		return data.id
				//         	}
				//         },
				//         {
				//            	"width": "15%",
				//         	data: null, render: function(data, type, row, meta) {
				//         		return serviceCost[meta.row]
				//         	}
				//         },
				//         { 
				//         	data: null, render: function(data, type, row) {
				//                 if(data.leader == 1) {
				//                 	return '<span><i class="fa fa-star"></i><i class="fa fa-star"></i> Group Leader</span>'
				//                 }
				//                 else if(data.leader == 2) {
				//            			return '<span><a href="javascript:void(0)" class="vice-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star"></i> Vice Leader</a></span>'
				//                 }
				//                  else {
				//                 	return '<span><a href="javascript:void(0)" class="make-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star-o"></i> Make Vice----Leader</a></span>'
				//                 }
				//             }, className: 'text-center'
				//         }
				//        ]
				//    });
				//      }

				//      if(this.selpermissions.makeLeader == 0 && this.selpermissions.transfer == 0 && this.selpermissions.delete == 0){
				// table = $('table.dt-members').DataTable( {
				//        "data": data,
				//        "columns": [
				//            {
				//            	"width": "5%",
				//                "className":      'details-control text-center',
				//                "orderable":      false,
				//                "data":           null,
				//                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				//            },
				//            { 
				//            	"width": "30%",
				//            	data: "client", render: function(data, type, row) {
				//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
				//             }
				//         },
				//         {
				//            	"width": "15%",
				//         	data: "client", render: function(data, type, row) {
				//         		return data.id
				//         	}
				//         },
				//         {
				//            	"width": "15%",
				//         	data: null, render: function(data, type, row, meta) {
				//         		return serviceCost[meta.row]
				//         	}
				//         }
				//        ]
				//    });
				//      }		        


				$('body').off('click', 'table.dt-members tbody td.details-control');
				setTimeout(function () {
					$('body').on('click', 'table.dt-members tbody td.details-control', function (e) {
						var tr = $(this).closest('tr');
						var row = table.row(tr);

						if (row.child.isShown()) {
							row.child.hide();
							tr.removeClass('shown');
						} else {
							row.child(format(row.data())).show();
							tr.addClass('shown');
						}
					});
				}.bind(this), 500);

				//make vice leader
				$('body').on('click', 'table.dt-members tbody a.make-leader-action', function (e) {
					var clientId = e.target.getAttribute('data-id');

					this.assignLeader(clientId, 'vice');
				}.bind(this));

				//vice leader modal
				$('body').on('click', 'table.dt-members tbody a.vice-leader-action', function (e) {
					$('#vice-leader-modal').modal('show');
					var clientId = e.target.getAttribute('data-id');
					this.seLclientId = clientId;
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody a.transfer-action', function (e) {
					var clientId = e.target.getAttribute('data-id');

					this.$refs.transferref.setMemberId(clientId);
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody a.delete-action', function (e) {
					var clientId = e.target.getAttribute('data-id');

					this.$refs.deletememberref.setMemberId(clientId);
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody .edit-service-action', function (e) {
					var serviceClientId = e.target.getAttribute('data-serviceclientid');
					var serviceServiceid = e.target.getAttribute('data-serviceserviceid');
					var serviceId = e.target.getAttribute('data-serviceid');

					this.$refs.editserviceref.init(serviceClientId, serviceServiceid, serviceId);
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody .write-report-action', function (e) {
					var clientId = e.target.getAttribute('data-clientid');
					var clientServiceId = e.target.getAttribute('data-clientserviceid');
					var tracking = e.target.getAttribute('data-tracking');

					this.createQuickReport(clientId, clientServiceId, tracking);
				}.bind(this));
			}.bind(this), 1000);
		},
		changeMemberType: function changeMemberType(type) {
			var clientId = this.seLclientId;
			this.assignLeader(clientId, type);
		},
		createDatatableToggleByService: function createDatatableToggleByService(serv) {
			var groupId = this.groupId;

			$('table.dt-gservice').DataTable().destroy();

			var vi = this;

			setTimeout(function () {

				var formatByService = function formatByService(d) {
					var div = $('<div/>').addClass('loading').text('Loading...');
					axiosAPIv1.post('/visa/group/' + groupId + '/service-by-detail', {
						service_id: d.service_id
					}).then(function (response) {
						var table = '<table class="table table-bordered table-striped table-hover">' + '<thead>' + '<tr>' + '<th>Package</th>' + '<th>Date Recorded</th>' + '<th>Details</th>' + '<th>Status</th>' + '<th>Cost</th>' + '<th>Service Fee</th>' + '<th>Additional Fee</th>' + '<th>Discount</th>' + '<th class="text-center">Action</th>' + '</tr>' + '</thead>' + '<tbody>';
						for (var a = 0; a < response.data.length; a++) {
							// var className = (response.data.active == 0) ? 'strikethrough' : '';
							// var visibility = (response.data.group_id == groupId) ? 'table-row' : 'none';

							table += '<tr>';
							table += '<td colspan="9" style="background-color: #d2d2d2; text-transform: capitalize"><b>' + response.data[a].first_name + ' ' + response.data[a].last_name + '</b></td>';
							table += '</tr>';

							for (var i = 0; i < response.data[a].services.length; i++) {
								var className = response.data[a].services[i].active == 0 ? 'strikethrough' : '';
								var visibility = response.data[a].services[i].group_id == groupId ? 'table-row' : 'none';
								if (response.data[a].services[i].remarks) {
									var remarks = response.data[a].services[i].remarks;
									var details = '<a href="#" data-toggle="popover" data-placement="right" data-container="body" data-content="' + remarks + '" data-trigger="hover">' + response.data[a].services[i].detail + '</a>';
								} else {
									var details = response.data[a].services[i].detail;
								}
								if (response.data[a].services[i].discount2.length > 0) {
									if (response.data[a].services[i].discount2[0].reason) {
										var reason = response.data[a].services[i].discount2[0].reason;
										var discount = '<a href="#" data-toggle="popover" data-placement="left" data-container="body" data-content="' + reason + '" data-trigger="hover">' + response.data[a].services[i].discount2[0].discount_amount + '</a>';
									} else {
										var discount = response.data[a].services[i].discount2[0].discount_amount;
									}
								} else {
									var discount = '&nbsp;';
								}
								var _action = '<a href="javascript:void(0)" class="edit-service-action" data-toggle="modal" data-target="#edit-service-modal" data-serviceclientid="' + response.data[a].services[i].client_id + '" data-serviceserviceid="' + response.data[a].services[i].service_id + '" data-serviceid="' + response.data[a].services[i].id + '"><i class="fa fa-pencil-square-o" data-serviceclientid="' + response.data[a].services[i].client_id + '" data-serviceserviceid="' + response.data[a].services[i].service_id + '" data-serviceid="' + response.data[a].services[i].id + '"></i></a>';

								table += '<tr class="' + className + '" style="display:' + visibility + '">';
								table += '<td>' + response.data[a].services[i].tracking + '</td>';
								table += '<td>' + response.data[a].services[i].service_date + '</td>';
								table += '<td>' + details + '</td>';
								table += '<td>' + response.data[a].services[i].status + '</td>';
								table += '<td>' + response.data[a].services[i].cost + '</td>';
								table += '<td>' + response.data[a].services[i].charge + '</td>';
								table += '<td>' + response.data[a].services[i].tip + '</td>';
								table += '<td>' + discount + '</td>';
								table += '<td class="text-center">' + _action + '</td>';
								table += '</tr>';
							}
						}
						table += '</tbody></table>';

						//return table;

						div.html(table).removeClass('loading');
					}).catch(function (error) {
						return console.log(error);
					});

					return div;
				};

				table2 = $('table.dt-gservice').DataTable({
					"data": serv,
					"columns": [{
						"width": "5%",
						"className": 'service-control text-center',
						"orderable": false,
						"data": null,
						"defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
					}, {
						"width": "30%",
						data: null, render: function render(serv, type, row) {
							return '<b style="color: #1ab394;">' + serv.detail + '</b>';
						}
					}, {
						"width": "15%",
						data: null, render: function render(serv, type, row) {
							return '<b>' + moment(String(serv.service_date)).format("MMMM D,YYYY") + '</b>';
						}
					}, {
						"width": "15%",
						data: null, render: function render(serv, type, row, meta) {
							return '<b>' + serv.totalcost + '</b>';
						}
					}]
				});

				$('body').on('click', 'table.dt-gservice tbody td.service-control', function (e) {
					var tr2 = $(this).closest('tr');
					var row = table2.row(tr2);

					if (row.child.isShown()) {
						row.child.hide();
						tr2.removeClass('shown');
					} else {
						row.child(formatByService(row.data())).show();
						tr2.addClass('shown');
					}
				});

				$('body').on('click', 'table.dt-gservice tbody .edit-service-action', function (e) {
					var serviceClientId = e.target.getAttribute('data-serviceclientid');
					var serviceServiceid = e.target.getAttribute('data-serviceserviceid');
					var serviceId = e.target.getAttribute('data-serviceid');

					this.$refs.editserviceref.init(serviceClientId, serviceServiceid, serviceId);
				}.bind(this));
			}.bind(this), 1000);
		},
		createDatatableToggleByBatch: function createDatatableToggleByBatch(serv) {
			var groupId = this.groupId;

			$('table.dt-gbatch').DataTable().destroy();

			var vi = this;

			setTimeout(function () {

				var formatByBatch = function formatByBatch(d) {
					var div = $('<div/>').addClass('loading').text('Loading...');
					axiosAPIv1.post('/visa/group/' + groupId + '/service-by-date', {
						date: d.service_date
					}).then(function (response) {
						var table = '<table class="table table-bordered table-striped table-hover">' + '<thead>' + '<tr>' + '<th>Package</th>' + '<th>Date Recorded</th>' + '<th>Details</th>' + '<th>Status</th>' + '<th>Cost</th>' + '<th>Service Fee</th>' + '<th>Additional Fee</th>' + '<th>Discount</th>' + '<th class="text-center">Action</th>' + '</tr>' + '</thead>' + '<tbody>';
						for (var a = 0; a < response.data.length; a++) {
							// var className = (response.data.active == 0) ? 'strikethrough' : '';
							// var visibility = (response.data.group_id == groupId) ? 'table-row' : 'none';

							table += '<tr>';
							table += '<td colspan="9" style="background-color: #d2d2d2; text-transform: capitalize"><b>' + response.data[a].first_name + ' ' + response.data[a].last_name + '</b></td>';
							table += '</tr>';

							for (var i = 0; i < response.data[a].services.length; i++) {
								var className = response.data[a].services[i].active == 0 ? 'strikethrough' : '';
								var visibility = response.data[a].services[i].group_id == groupId ? 'table-row' : 'none';
								if (response.data[a].services[i].remarks) {
									var remarks = response.data[a].services[i].remarks;
									var details = '<a href="#" data-toggle="popover" data-placement="right" data-container="body" data-content="' + remarks + '" data-trigger="hover">' + response.data[a].services[i].detail + '</a>';
								} else {
									var details = response.data[a].services[i].detail;
								}
								if (response.data[a].services[i].discount2.length > 0) {
									if (response.data[a].services[i].discount2[0].reason) {
										var reason = response.data[a].services[i].discount2[0].reason;
										var discount = '<a href="#" data-toggle="popover" data-placement="left" data-container="body" data-content="' + reason + '" data-trigger="hover">' + response.data[a].services[i].discount2[0].discount_amount + '</a>';
									} else {
										var discount = response.data[a].services[i].discount2[0].discount_amount;
									}
								} else {
									var discount = '&nbsp;';
								}
								var _action = '<a href="javascript:void(0)" class="edit-service-action" data-toggle="modal" data-target="#edit-service-modal" data-serviceclientid="' + response.data[a].services[i].client_id + '" data-serviceserviceid="' + response.data[a].services[i].service_id + '" data-serviceid="' + response.data[a].services[i].id + '"><i class="fa fa-pencil-square-o" data-serviceclientid="' + response.data[a].services[i].client_id + '" data-serviceserviceid="' + response.data[a].services[i].service_id + '" data-serviceid="' + response.data[a].services[i].id + '"></i></a>';

								table += '<tr class="' + className + '" style="display:' + visibility + '">';
								table += '<td>' + response.data[a].services[i].tracking + '</td>';
								table += '<td>' + response.data[a].services[i].service_date + '</td>';
								table += '<td>' + details + '</td>';
								table += '<td>' + response.data[a].services[i].status + '</td>';
								table += '<td>' + response.data[a].services[i].cost + '</td>';
								table += '<td>' + response.data[a].services[i].charge + '</td>';
								table += '<td>' + response.data[a].services[i].tip + '</td>';
								table += '<td>' + discount + '</td>';
								table += '<td class="text-center">' + _action + '</td>';
								table += '</tr>';
							}
						}
						table += '</tbody></table>';

						//return table;

						div.html(table).removeClass('loading');
					}).catch(function (error) {
						return console.log(error);
					});

					return div;
				};

				table3 = $('table.dt-gbatch').DataTable({
					"data": serv,
					"columns": [{
						"width": "5%",
						"className": 'batch-control text-center',
						"orderable": false,
						"data": null,
						"defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
					}, {
						"width": "30%",
						data: null, render: function render(serv, type, row) {
							return '<b style="color: #1ab394;">' + moment(String(serv.service_date)).format("MMMM D,YYYY") + '</b>';
						}
					}, {
						"width": "15%",
						data: null, render: function render(serv, type, row, meta) {
							return '<b>' + serv.totalcost + '</b>';
						}
					}]
				});

				$('body').on('click', 'table.dt-gbatch tbody td.batch-control', function (e) {
					var tr3 = $(this).closest('tr');
					var row = table3.row(tr3);

					if (row.child.isShown()) {
						row.child.hide();
						tr3.removeClass('shown');
					} else {
						row.child(formatByBatch(row.data())).show();
						tr3.addClass('shown');
					}
				});

				$('body').on('click', 'table.dt-gbatch tbody .edit-service-action', function (e) {
					var serviceClientId = e.target.getAttribute('data-serviceclientid');
					var serviceServiceid = e.target.getAttribute('data-serviceserviceid');
					var serviceId = e.target.getAttribute('data-serviceid');

					this.$refs.editserviceref.init(serviceClientId, serviceServiceid, serviceId);
				}.bind(this));
			}.bind(this), 1000);
		},
		fetchDeposits: function fetchDeposits() {
			var _this8 = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/deposits').then(function (response) {
				_this8.deposits = response.data;

				_this8.createDatatable('dt-deposits');
			}).catch(function (error) {
				return console.log(error);
			});
		},
		fetchPayments: function fetchPayments() {
			var _this9 = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/payments').then(function (response) {
				_this9.payments = response.data;

				_this9.createDatatable('dt-payments');
			}).catch(function (error) {
				return console.log(error);
			});
		},
		fetchRefunds: function fetchRefunds() {
			var _this10 = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/refunds').then(function (response) {
				_this10.refunds = response.data;

				_this10.createDatatable('dt-refunds');
			}).catch(function (error) {
				return console.log(error);
			});
		},
		fetchDiscounts: function fetchDiscounts() {
			var _this11 = this;

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/discounts').then(function (response) {
				_this11.discounts = response.data;

				_this11.createDatatable('dt-discounts');
			}).catch(function (error) {
				return console.log(error);
			});
		},
		resetActionLogsComponentData: function resetActionLogsComponentData() {
			this.actionLogs = [];
			this.current_year = null;
			this.current_month = null;
			this.current_day = null;
			this.c_year = null;
			this.c_month = null;
			this.c_day = null;
			this.s_count = 1;
		},
		fetchActionLogs: function fetchActionLogs() {
			var _this12 = this;

			this.resetActionLogsComponentData();

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/action-logs').then(function (response) {
				_this12.actionLogs = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		resetTransactionLogsComponentData: function resetTransactionLogsComponentData() {
			this.transactionLogs = [];
			this.current_year2 = null;
			this.current_month2 = null;
			this.current_day2 = null;
			this.c_year2 = null;
			this.c_month2 = null;
			this.c_day2 = null;
			this.s_count2 = 1;
		},
		fetchTransactionLogs: function fetchTransactionLogs() {
			var _this13 = this;

			this.resetTransactionLogsComponentData();

			var groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/transaction-logs').then(function (response) {
				_this13.transactionLogs = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		}
	},

	created: function created() {
		this.init(); // Ok
	},
	mounted: function mounted() {
		// Popover init
		$('body').popover({ // Ok
			html: true,
			trigger: 'hover',
			selector: '[data-toggle="popover"]'
		});
	},


	components: {
		'edit-address': __webpack_require__(344),
		'add-new-member': __webpack_require__(335),
		'add-new-service': __webpack_require__(336),
		'edit-service': __webpack_require__(345),
		'add-fund': __webpack_require__(334),
		'transfer': __webpack_require__(348),
		'member-type': __webpack_require__(347),
		'delete-member': __webpack_require__(342),
		'multiple-quick-report': __webpack_require__(14),
		'client-services': __webpack_require__(340)
	}

});

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 20:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(12);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("01398020", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-0f2f2b77\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MultipleQuickReport.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-0f2f2b77\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MultipleQuickReport.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['log', 'logcount'],
  methods: {
    // select(id){
    //this.$parent.selectItem(id)
    // },
  }
});

/***/ }),

/***/ 241:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

  props: ['groupid'],

  data: function data() {
    return {
      addFundForm: new Form({
        option: 'Deposit',
        amount: '',
        reason: ''
      }, { baseURL: axiosAPIv1.defaults.baseURL })
    };
  },


  methods: {
    resetAddFundForm: function resetAddFundForm() {
      this.addFundForm.option = 'Deposit';
      this.addFundForm.amount = '';
      this.addFundForm.reason = '';
    },
    addFunds: function addFunds() {
      var _this = this;

      this.addFundForm.submit('post', '/visa/group/' + this.groupid + '/add-funds').then(function (response) {
        if (response.success) {
          _this.$parent.$parent.fetchDetails();

          _this.resetAddFundForm();

          $('#add-funds-modal').modal('hide');

          toastr.success(response.message);
        }
      }).catch(function (error) {
        return console.log(error);
      });
    }
  }

});

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['groupid'],

	data: function data() {
		return {
			clients: [],

			addNewMemberForm: new Form({
				clientIds: []
			}, { baseURL: axiosAPIv1.defaults.baseURL })
		};
	},


	methods: {
		fetchClients: function fetchClients(q) {
			var _this = this;

			axiosAPIv1.get('/visa/client/get-available-visa-clients', {
				params: {
					groupId: this.groupid,
					q: q
				}
			}).then(function (response) {
				if (response.data.length > 0) {
					_this.clients.push({
						id: response.data[0].id,
						name: response.data[0].name });
				}

				setTimeout(function () {
					var my_val = $('.chosen-select-for-clients').val();
					//console.log(my_val);
					$(".chosen-select-for-clients").val(my_val).trigger("chosen:updated");
				}, 1000);
			});
		},
		resetAddNewMemberForm: function resetAddNewMemberForm() {
			this.addNewMemberForm = new Form({
				clientIds: []
			}, { baseURL: axiosAPIv1.defaults.baseURL });
		},
		addNewMember: function addNewMember() {
			var _this2 = this;

			if (this.addNewMemberForm.clientIds.length == 0) {
				toastr.error('Clients field is required.');
			} else {
				this.addNewMemberForm.submit('post', '/visa/group/' + this.groupid + '/add-members').then(function (result) {
					if (result.success) {
						_this2.clients = [];
						_this2.resetAddNewMemberForm();
						$(".chosen-select-for-clients").empty().trigger('chosen:updated');

						$('#add-new-member-modal').modal('hide');

						toastr.success(result.message);

						_this2.$parent.$parent.fetchMembers();

						_this2.$parent.$parent.$refs.addnewserviceref.fetchGroupPackagesServicesList();
					}
				}).catch(function (error) {
					return console.log(error);
				});
			}
		}
	},

	mounted: function mounted() {
		var _this3 = this;

		$('.chosen-select-for-clients').chosen({
			width: "100%",
			no_results_text: "Searching Client # : ",
			search_contains: true
		});

		$('.chosen-select-for-clients').on('change', function () {
			var clientIds = $('.chosen-select-for-clients').val();

			_this3.addNewMemberForm.clientIds = clientIds;
		});

		$('body').on('keyup', '.chosen-container input[type=text]', function (e) {
			var q = $('.chosen-container input[type=text]').val();
			if (q.length >= 4) {
				_this3.fetchClients(q);
			}
		});
	}
});

/***/ }),

/***/ 243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['groupid'],

	data: function data() {
		return {
			loading: false,

			screen: 1,

			servicesList: [],

			cost: '',

			clients: [],

			detailArray: [],
			requiredDocs: [],
			optionalDocs: [],

			addNewServiceForm: new Form({
				services: [],
				note: '',
				discount: '',
				reason: '',
				reporting: '',

				clients: [],
				packages: [],
				docs: []
			}, { baseURL: axiosAPIv1.defaults.baseURL })
		};
	},


	methods: {
		fetchServiceList: function fetchServiceList() {
			var _this = this;

			axios.get('/visa/service-manager/show').then(function (response) {
				_this.servicesList = response.data.filter(function (r) {
					return r.parent_id != 0;
				});

				setTimeout(function () {
					$(".chosen-select-for-service").trigger("chosen:updated");
				}, 1000);
			}).catch(function (error) {
				return console.log(error);
			});
		},
		resetAddNewServiceForm: function resetAddNewServiceForm() {
			this.screen = 1;

			this.addNewServiceForm.services = [];
			this.addNewServiceForm.note = '';
			this.addNewServiceForm.discount = '';
			this.addNewServiceForm.reason = '';
			this.addNewServiceForm.reporting = '';
			this.addNewServiceForm.clients = [];
			this.addNewServiceForm.packages = [];
			this.addNewServiceForm.docs = [];

			this.cost = '';
			// Deselect All
			$('.chosen-select-for-service, .chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');

			this.detailArray = [];
			this.requiredDocs = [];
			this.optionalDocs = [];
		},
		addNewService: function addNewService() {
			var _this2 = this;

			this.loading = true;

			this.addNewServiceForm.packages = [];
			var tbl = $('.add-new-service-datatable').DataTable();
			this.addNewServiceForm.clients.forEach(function (c, index) {
				var value = tbl.$('select.package-select-' + c).val();
				_this2.addNewServiceForm.packages.push(value);
			});

			var docsArray = [];
			var requiredDocs = '';
			var optionalDocs = '';
			var hasRequiredDocsErrors = [];

			this.addNewServiceForm.clients.forEach(function (client, index1) {
				_this2.addNewServiceForm.services.forEach(function (service, index2) {
					$('.chosen-select-for-required-docs-' + index1 + '-' + index2 + ' option:selected').each(function () {
						requiredDocs += $(this).val() + ',';
					});

					var requiredOptions = $('.chosen-select-for-required-docs-' + index1 + '-' + index2 + ' option').length;
					var requiredCount = $('.chosen-select-for-required-docs-' + index1 + '-' + index2 + ' option:selected').length;
					if (requiredCount == 0 && requiredOptions > 0) {
						hasRequiredDocsErrors.push({
							client: _this2.getClientName(client),
							service: _this2.getServiceName(service)
						});
					}

					$('.chosen-select-for-optional-docs-' + index1 + '-' + index2 + ' option:selected').each(function () {
						optionalDocs += $(this).val() + ',';
					});

					docsArray.push((requiredDocs + optionalDocs).slice(0, -1));
					requiredDocs = '';
					optionalDocs = '';
				});
			});

			this.addNewServiceForm.docs = docsArray;

			if (hasRequiredDocsErrors.length > 0) {
				for (var i = 0; i < hasRequiredDocsErrors.length; i++) {
					toastr.error('Please select from required documents.', hasRequiredDocsErrors[i].client + ' - ' + hasRequiredDocsErrors[i].service);
				}
				this.loading = false;
			} else {

				this.addNewServiceForm.submit('post', '/visa/group/' + this.groupid + '/add-service').then(function (response) {
					if (response.success) {
						_this2.$parent.$parent.fetchDetails();
						_this2.$parent.$parent.fetchMembers();

						_this2.fetchGroupPackagesServicesList();
						_this2.resetAddNewServiceForm();

						$('#add-new-service-modal').modal('hide');

						toastr.success(response.message);
					}

					_this2.loading = false;
				}).catch(function (error) {
					for (var key in error) {
						if (error.hasOwnProperty(key)) toastr.error(error[key][0]);
					}

					_this2.loading = false;
				});
			}
		},
		getDocs: function getDocs(servicesId) {
			var _this3 = this;

			axios.get('/visa/get-docs', {
				params: {
					servicesId: servicesId
				}
			}).then(function (result) {
				_this3.detailArray = result.data.detailArray;
				_this3.requiredDocs = result.data.docsNeededArray;
				_this3.optionalDocs = result.data.docsOptionalArray;
			});
		},
		getClientName: function getClientName(clientId) {
			var clientName = '';
			this.clients.forEach(function (client) {
				if (client.client.id == clientId) {
					clientName = client.client.first_name + ' ' + client.client.last_name;
				}
			});

			return clientName;
		},
		getServiceName: function getServiceName(serviceId) {
			var serviceName = '';
			this.servicesList.forEach(function (service) {
				if (service.id == serviceId) {
					serviceName = service.detail;
				}
			});

			return serviceName;
		},
		initChosenSelect: function initChosenSelect() {
			$('.chosen-select-for-service').chosen({
				width: "100%",
				no_results_text: "No result/s found.",
				search_contains: true
			});

			var vm = this;

			$('.chosen-select-for-service').on('change', function () {
				var services = $(this).val();

				vm.getDocs(services);

				vm.addNewServiceForm.services = services;

				// Set cost input
				if (services.length == 1) {
					vm.servicesList.map(function (s) {
						if (s.id == services[0]) {
							vm.cost = s.cost;
						}
					});
				} else {
					vm.cost = '';
				}
			});
		},
		changeScreen: function changeScreen(screen) {
			if (screen == 2) {
				if (this.addNewServiceForm.services.length == 0) {
					toastr.error('Please select at least one service');
					return false;
				}
			} else if (screen == 3) {
				if (this.addNewServiceForm.clients.length == 0) {
					toastr.error('Please select at least one member');
					return false;
				}

				setTimeout(function () {
					$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen('destroy');
					$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({
						width: "100%",
						no_results_text: "No result/s found.",
						search_contains: true
					});

					$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');
				}, 1000);
			}

			this.screen = screen;
		},
		fetchGroupPackagesServicesList: function fetchGroupPackagesServicesList() {
			var _this4 = this;

			axios.get('/visa/group/' + this.groupid + '/packages-services').then(function (response) {
				_this4.clients = response.data;

				_this4.callDataTable();
			}).catch(function (error) {
				return console.log(error);
			});
		},
		callDataTable: function callDataTable() {
			$('.add-new-service-datatable').DataTable().destroy();

			setTimeout(function () {
				$('.add-new-service-datatable').DataTable({
					pageLength: 10,
					responsive: true,
					dom: '<"top"lf>rt<"bottom"ip><"clear">'
				});
			}, 1000);
		},
		toggleAllCheckbox: function toggleAllCheckbox(e) {
			var _this5 = this;

			if (e.target.checked == true) {
				this.addNewServiceForm.clients = [];
				this.clients.forEach(function (c, index) {
					_this5.addNewServiceForm.clients.push(c.client.id);
				});
			} else {
				this.addNewServiceForm.clients = [];
			}
		},
		checkAll: function checkAll(e) {
			var checkbox = e.target.className;

			if (checkbox == 'checkbox-all') {
				if ($('.' + checkbox).is(':checked')) $('.chosen-select-for-required-docs option').prop('selected', true);else $('.chosen-select-for-required-docs option').prop('selected', false);

				$('.chosen-select-for-required-docs option').trigger('chosen:updated');
			} else {
				var chosen = e.target.dataset.chosenClass;

				if ($('.' + checkbox).is(':checked')) $('.' + chosen + ' option').prop('selected', true);else $('.' + chosen + ' option').prop('selected', false);

				$('.' + chosen + ' option').trigger('chosen:updated');
			}
		}
	},

	created: function created() {
		this.fetchServiceList();
		this.fetchGroupPackagesServicesList();
	},
	mounted: function mounted() {
		this.initChosenSelect();
	}
});

/***/ }),

/***/ 247:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['members', 'groupid'],

	data: function data() {

		return {
			membersDatatable: null,

			screen: 1,

			selectedClientsId: [],
			selectedClientServicesId: [],
			selectedTrackings: []
		};
	},


	methods: {
		gotoScreen: function gotoScreen(screen) {
			this.setSelectedClientsId();

			if (screen == 2 && this.selectedClientsId.length == 0) {
				toastr.error('Please select client/s.');
			} else {
				this.screen = screen;
			}
		},
		setSelectedClientsId: function setSelectedClientsId() {
			var selectedClientsId = [];

			this.membersDatatable.$("input[name='clientsId[]']:checked").each(function (e) {
				selectedClientsId.push(parseInt($(this).val()));
			});

			this.selectedClientsId = selectedClientsId;
		},
		setSelectedClientServicesAndTrackingsId: function setSelectedClientServicesAndTrackingsId() {
			var _this = this;

			var selectedClientServicesId = [];

			$("input[name='clientServicesId[]']:checked").each(function (e) {
				selectedClientServicesId.push(parseInt($(this).val()));
			});

			if (selectedClientServicesId.length == 0) {
				toastr.error('Please select services/s.');
			} else {
				axiosAPIv1.get('/visa/group/' + this.groupid + '/client-services', {
					params: {
						clientServicesId: selectedClientServicesId
					}
				}).then(function (response) {
					_this.selectedClientServicesId = response.data;

					var selectedTrackings = [];

					_this.selectedClientServicesId.forEach(function (clientService) {
						selectedTrackings.push(clientService.tracking);
					});

					_this.selectedTrackings = selectedTrackings;

					_this.showMultipleQuickReportModal();
				}).catch(function (error) {
					return console.log(error);
				});
			}
		},
		showMultipleQuickReportModal: function showMultipleQuickReportModal() {
			this.$parent.$parent.quickReportClientsId = this.selectedClientsId;
			this.$parent.$parent.quickReportClientServicesId = this.selectedClientServicesId;
			this.$parent.$parent.quickReportTrackings = this.selectedTrackings;

			this.$parent.$parent.fetchDocs(this.selectedClientServicesId);

			$('#client-services-modal').modal('hide');

			setTimeout(function () {
				$('#add-quick-report-modal').modal('show');
			}, 1000);
		},
		uncheckSelectedClientsId: function uncheckSelectedClientsId() {
			this.membersDatatable.$("input[name='clientsId[]']").prop('checked', false);
		},
		uncheckSelectedClientServicesId: function uncheckSelectedClientServicesId() {
			this.membersDatatable.$("input[name='clientServicesId[]']").prop('checked', false);
		},
		createDatatable: function createDatatable(className) {
			this.membersDatatable = $('table.' + className).DataTable().destroy();

			setTimeout(function () {
				this.membersDatatable = $('table.' + className).DataTable({
					pageLength: 10,
					responsive: true,
					"aaSorting": [],
					dom: '<"top"lf>rt<"bottom"ip><"clear">'
				});
			}.bind(this), 1000);
		},
		toggleChildRow: function toggleChildRow(event, index) {
			$('#child-row-' + index).toggleClass('hide');
			$('.arrow-' + index).toggleClass('fa-arrow-right fa-arrow-down');
		}
	},

	mounted: function mounted() {

		$('#client-services-modal').on('hidden.bs.modal', function (e) {

			this.screen = 1;

			this.selectedClientsId = [];
			this.selectedClientServicesId = [];
			this.selectedTrackings = [];

			this.uncheckSelectedClientsId();
			this.uncheckSelectedClientServicesId();
		}.bind(this));
	}
});

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

  props: ['groupid'],

  data: function data() {
    return {
      deleteMemberForm: new Form({
        memberId: null
      }, { baseURL: axiosAPIv1.defaults.baseURL })
    };
  },


  methods: {
    setMemberId: function setMemberId(id) {
      this.deleteMemberForm.memberId = id;
    },
    resetDeleteMemberForm: function resetDeleteMemberForm() {
      this.deleteMemberForm.memberId = null;
    },
    deleteMember: function deleteMember() {
      var _this = this;

      this.deleteMemberForm.submit('post', '/visa/group/' + this.groupid + '/delete-member').then(function (response) {
        _this.resetDeleteMemberForm();

        $('#delete-member-modal').modal('hide');

        if (response.success) {
          _this.$parent.$parent.fetchMembers();

          toastr.success(response.message);
        } else {
          toastr.error(response.message);
        }
      }).catch(function (error) {
        return console.log(error);
      });
    }
  }

});

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['log2'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    }
  },
  computed: {
    detail: function detail() {
      var res = this.log2.data.title;
      return res.replace("</br>", " ");
    }
  }
});

/***/ }),

/***/ 251:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['groupid'],

	data: function data() {
		return {
			editAddressForm: new Form({
				address: '',
				number: '',
				name: ''
			}, { baseURL: axiosAPIv1.defaults.baseURL })
		};
	},


	methods: {
		setGroupAddress: function setGroupAddress() {
			var _this = this;

			axiosAPIv1.get('/visa/group/' + this.groupid).then(function (response) {
				_this.editAddressForm.address = response.data.address;
				_this.editAddressForm.number = response.data.user.address.contact_number;
				_this.editAddressForm.name = response.data.name;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		editAddress: function editAddress() {
			var _this2 = this;

			this.editAddressForm.submit('patch', '/visa/group/' + this.groupid + '/edit-address').then(function (response) {
				if (response.success) {
					_this2.$parent.$parent.fetchDetails();

					$('#edit-address-modal').modal('hide');

					toastr.success(response.message);
				}
			}).catch(function (error) {
				return console.log(error);
			});
		}
	}

});

/***/ }),

/***/ 252:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

  props: ['groupid'],

  data: function data() {
    return {
      screen: 1,

      clientServices: [],

      editServiceForm: new Form({
        clientServicesId: [],
        serviceId: '',
        additional_charge: '',
        cost: '',
        discount: '',
        reason: '',
        status: '',
        note: '',
        active: ''
      }, { baseURL: axiosAPIv1.defaults.baseURL })
    };
  },


  methods: {
    changeScreen: function changeScreen(screen) {
      if (screen == 2) {
        if (this.editServiceForm.clientServicesId.length == 0) {
          toastr.error('Please select at least one client');
          return false;
        }
      }

      this.screen = screen;
    },
    setEditServiceForm: function setEditServiceForm(clientServiceId, serviceId, additionalCharge, cost, discount, reason, status, note, active) {
      this.editServiceForm.clientServicesId = [];
      this.editServiceForm.clientServicesId.push(clientServiceId);

      this.editServiceForm.serviceId = serviceId;
      this.editServiceForm.additional_charge = additionalCharge;
      this.editServiceForm.cost = cost;
      this.editServiceForm.discount = discount;
      this.editServiceForm.reason = reason;
      this.editServiceForm.status = status;
      this.editServiceForm.note = '';
      this.editServiceForm.active = active;
    },
    init: function init(clientId, serviceId, cid) {
      var _this = this;

      this.changeScreen(1);

      var _clientId = clientId;
      var _serviceId = serviceId;
      var _cid = cid;
      var _groupId = this.groupid;
      axios.get('/visa/group/' + _groupId + '/edit-services', {
        params: {
          clientId: _clientId,
          serviceId: _serviceId,
          cid: _cid
        }
      }).then(function (response) {
        _this.clientServices = response.data;
        //console.log(response.data[0].id);
        _this.callDataTable();

        var _response$data$ = response.data[0],
            tip = _response$data$.tip,
            cost = _response$data$.cost,
            status = _response$data$.status,
            remarks = _response$data$.remarks,
            active = _response$data$.active;

        var discount = null;
        var reason = null;

        if (response.data[0].discount) {
          var _discount = response.data[0].discount.length > 0 ? response.data[0].discount[0].discount_amount : '';
          var _reason = response.data[0].discount.length > 0 ? response.data[0].discount[0].reason : '';
        }

        _this.setEditServiceForm(_cid, serviceId, tip, cost, discount, reason, status, remarks, active);
      }).catch(function (error) {
        return console.log(error);
      });
    },
    callDataTable: function callDataTable() {
      $('.clients-datatable').DataTable().destroy();

      setTimeout(function () {
        $('.clients-datatable').DataTable({
          pageLength: 10,
          responsive: true,
          dom: '<"top"lf>rt<"bottom"ip><"clear">'
        });
      }, 1000);
    },
    toggleAllCheckbox: function toggleAllCheckbox(e) {
      var _this2 = this;

      if (e.target.checked == true) {
        this.editServiceForm.clientServicesId = [];
        this.clientServices.forEach(function (c, index) {
          _this2.editServiceForm.clientServicesId.push(c.id);
        });
      } else {
        this.editServiceForm.clientServicesId = [];
      }
    },
    editService: function editService() {
      var _this3 = this;

      this.editServiceForm.submit('patch', '/visa/group/' + this.groupid + '/edit-services').then(function (response) {
        if (response.success) {
          _this3.$parent.$parent.fetchDetails();
          _this3.$parent.$parent.fetchMembers();

          _this3.$parent.$parent.$refs.addnewserviceref.fetchGroupPackagesServicesList();

          $('#edit-service-modal').modal('hide');

          toastr.success(response.message);
        }
      }).catch(function (error) {
        return console.log(error);
      });
    }
  }

});

/***/ }),

/***/ 254:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['groupid'],

	data: function data() {
		return {};
	},


	methods: {
		changeMemberType: function changeMemberType(type) {
			// option = make-group-leader | make-group-member
			var vm = this;
			this.$parent.$parent.changeMemberType(type);
		}
	},

	created: function created() {}
});

/***/ }),

/***/ 255:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['groupid'],

	data: function data() {
		return {
			screen: null,

			services: [],
			packages: [],

			transferForm: new Form({
				groupId: '',
				memberId: '',
				services: [],
				packages: [],
				option: ''
			}, { baseURL: axiosAPIv1.defaults.baseURL })
		};
	},


	methods: {
		setScreen: function setScreen(screen) {
			this.screen = screen;
		},
		setMemberId: function setMemberId(id) {
			this.transferForm.memberId = id;

			this.setScreen(1);
		},
		getDetails: function getDetails(option, screen) {
			var _this = this;

			// option = client-to-group | group-to-client
			this.transferForm.option = option;

			var vm = this;

			function getServices() {
				return axios.get('/visa/client/' + vm.transferForm.memberId + '/services', {
					params: {
						option: option,
						groupId: vm.groupid
					}
				});
			}
			function getPackages() {
				// axios
				return axios.get('/visa/client/' + vm.transferForm.memberId + '/packages', {
					params: {
						option: option,
						groupId: vm.groupid
					}
				});
			}

			axios.all([getServices(), getPackages()]).then(axios.spread(function (services, packages) {

				_this.services = services.data;
				_this.packages = packages.data;
			})).catch(function (error) {
				return console.log(error);
			});

			this.setScreen(screen);
		},
		callDataTable: function callDataTable() {
			$('.transfer-datatable').DataTable().destroy();

			setTimeout(function () {
				$('.transfer-datatable').DataTable({
					pageLength: 10,
					responsive: true,
					dom: '<"top"lf>rt<"bottom"ip><"clear">'
				});
			}, 1000);
		},
		toggleAllCheckbox: function toggleAllCheckbox(e) {
			var _this2 = this;

			if (e.target.checked == true) {
				this.transferForm.services = [];
				this.services.forEach(function (s, index) {
					_this2.transferForm.services.push(s.id);
				});
			} else {
				this.transferForm.services = [];
			}
		},
		resetTransferForm: function resetTransferForm() {
			this.services = [];
			this.packages = [];
		},
		transfer: function transfer() {
			var _this3 = this;

			if (this.transferForm.services.length == 0) {
				toastr.error('Please select at least one service');
				return false;
			} else {

				this.transferForm.packages = [];

				this.transferForm.services.forEach(function (s, index) {
					var value = $('select.transfer-package-select-' + s).val();

					_this3.transferForm.packages.push(value);
				});

				this.transferForm.submit('patch', '/visa/client/' + this.groupid + '/transfer').then(function (response) {
					if (response.success) {
						_this3.$parent.$parent.fetchDetails();
						_this3.$parent.$parent.fetchMembers();

						_this3.$parent.$parent.$refs.addnewserviceref.fetchGroupPackagesServicesList();

						_this3.resetTransferForm();

						$('#transfer-modal').modal('hide');

						toastr.success(response.message);
					}
				}).catch(function (error) {
					return console.log(error);
				});
			}
		}
	},

	created: function created() {
		this.transferForm.groupId = this.groupid;
	}
});

/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(24),
  /* template */
  __webpack_require__(32),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/clients/ActionLogs.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ActionLogs.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-72b755b5", Component.options)
  } else {
    hotAPI.reload("data-v-72b755b5", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 293:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-06d26471] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-06d26471] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["EditService.vue?4d2c8a9b"],"names":[],"mappings":";AAqPA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"EditService.vue","sourcesContent":["<template>\n\n\t<div>\n\t\t<div v-show='screen == 1'>\n\t\t\t<div class=\"table-responsive\">\n\t            <table class=\"table table-striped table-bordered table-hover clients-datatable\">\n\t                <thead>\n\t\t                <tr>\n\t\t                    <th class=\"text-center\">\n\t\t                        <input type=\"checkbox\" @click=\"toggleAllCheckbox\">\n\t\t                    </th>\n\t\t                    <th>Name</th>\n\t\t                    <th>Client ID</th>\n\t\t                    <th>Package</th>\n\t\t                    <th>Date Recorded</th>\n\t\t                    <th>Details</th>\n\t\t                    <th>Status</th>\n\t\t                </tr>\n\t                </thead>\n\t                <tbody>\n\t                    <tr v-for=\"clientService in clientServices\">\n\t                    \t<td class=\"text-center\">\n\t                    \t\t<input type=\"checkbox\" \n\t                    \t\t\t:value=\"clientService.id\" \n\t                    \t\t\tv-model=\"editServiceForm.clientServicesId\">\n\t                    \t</td>\n\t                    \t<td>{{ clientService.user.full_name }}</td>\n\t                    \t<td>{{ clientService.user.id }}</td>\n\t                    \t<td>{{ clientService.tracking }}</td>\n\t                    \t<td>{{ clientService.service_date }}</td>\n\t                    \t<td>{{ clientService.detail }}</td>\n\t                    \t<td>{{ clientService.status }}</td>\n\t                    </tr>\n\t                </tbody>\n\t            </table>\n\t        </div>\n\n\t        <a class=\"btn btn-primary pull-right\" @click=\"changeScreen(2)\">Continue</a>\n\t    \t<a class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</a>\n\n\t    \t<br>\n\t\t</div>\n\n\t\t<div v-show='screen == 2'>\n\t\t\t<form @submit.prevent=\"editService\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': editServiceForm.errors.has('additional_charge') }\">\n\t\t\t\t\t\t    <label for=\"additonal_charge\">Additional Charge</label>\n\t\t\t\t\t\t    <input type=\"text\" class=\"form-control\" id=\"additional_charge\" name=\"additional_charge\" v-model=\"editServiceForm.additional_charge\">\n\t\t\t\t\t\t    <span class=\"help-block\" v-if=\"editServiceForm.errors.has('additional_charge')\" v-text=\"editServiceForm.errors.get('additional_charge')\"></span>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': editServiceForm.errors.has('cost') }\">\n\t\t\t\t\t\t    <label for=\"cost\">Cost</label>\n\t\t\t\t\t\t    <input type=\"text\" class=\"form-control\" id=\"cost\" name=\"cost\" v-model=\"editServiceForm.cost\">\n\t\t\t\t\t\t    <span class=\"help-block\" v-if=\"editServiceForm.errors.has('cost')\" v-text=\"editServiceForm.errors.get('cost')\"></span>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': editServiceForm.errors.has('discount') }\">\n\t\t\t\t\t\t    <label for=\"cost\">Discount</label>\n\t\t\t\t\t\t    <input type=\"text\" class=\"form-control\" id=\"discount\" name=\"discount\" v-model=\"editServiceForm.discount\">\n\t\t\t\t\t\t    <span class=\"help-block\" v-if=\"editServiceForm.errors.has('discount')\" v-text=\"editServiceForm.errors.get('discount')\"></span>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t    <label for=\"status\">Status</label>\n\t\t\t\t\t\t    <select class=\"form-control\" id=\"status\" name=\"status\" v-model=\"editServiceForm.status\">\n\t\t\t\t\t\t    \t<option value=\"complete\">Complete</option>\n\t\t\t\t\t\t    \t<option value=\"on process\">On Process</option>\n\t\t\t\t\t\t    \t<option value=\"pending\">Pending</option>\n\t\t\t\t\t\t    </select>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t    <label for=\"note\">Note</label>\n\t\t\t\t\t\t    <textarea class=\"form-control\" id=\"note\" name=\"note\" v-model=\"editServiceForm.note\" placeholder=\"for internal use only, only employees can see.\"></textarea>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\" style=\"margin-top: 22px;\">\n\t\t\t\t\t\t    <label for=\"active\"></label>\n\t\t\t\t\t\t    <label class=\"radio-inline\">\n\t\t\t\t\t\t\t  <input type=\"radio\" id=\"active\" name=\"active\" value=\"1\" v-model=\"editServiceForm.active\"> Enable\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<label class=\"radio-inline\">\n\t\t\t\t\t\t\t  <input type=\"radio\" id=\"active\" name=\"active\" value=\"0\" v-model=\"editServiceForm.active\"> Disable\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': editServiceForm.errors.has('reason') }\" v-if=\"editServiceForm.discount>0\" style=\"margin-top: 25px;\">\n\t\t\t\t\t\t    <label for=\"cost\">Reason</label>\n\t\t\t\t\t\t    <input type=\"text\" class=\"form-control\" id=\"reason\" name=\"reason\" v-model=\"editServiceForm.reason\">\n\t\t\t\t\t\t    <span class=\"help-block\" v-if=\"editServiceForm.errors.has('reason')\" v-text=\"editServiceForm.errors.get('reason')\"></span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Save</button>\n\t    \t\t<a class=\"btn btn-default pull-right m-r-10\" @click=\"changeScreen(1)\">Back</a>\n\t\t\t</form>\n\t\t</div>\n\t</div>\n\n</template>\n\n<script>\n\n    export default {\n\n    \tprops: ['groupid'],\n\n    \tdata() {\n    \t\treturn {\n    \t\t\tscreen: 1,\n\n    \t\t\tclientServices: [],\n\n    \t\t\teditServiceForm: new Form({\n    \t\t\t\tclientServicesId: [],\n    \t\t\t\tserviceId: '',\n    \t\t\t\tadditional_charge: '',\n    \t\t\t\tcost: '',\n    \t\t\t\tdiscount: '',\n    \t\t\t\treason: '',\n    \t\t\t\tstatus: '',\n    \t\t\t\tnote: '',\n    \t\t\t\tactive: ''\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\t\n    \t\t}\n    \t},\n\n    \tmethods: {\n\n    \t\tchangeScreen(screen) {\n    \t\t\tif(screen == 2) {\n\t\t\t\t\tif(this.editServiceForm.clientServicesId.length == 0) {\n\t\t\t\t\t\ttoastr.error('Please select at least one client');\n\t\t\t\t\t\treturn false;\n\t\t\t\t\t}\n\t\t\t\t}\n\n    \t\t\tthis.screen = screen;\n    \t\t},\n\n    \t\tsetEditServiceForm(clientServiceId, serviceId, additionalCharge, cost, discount, reason, status, note, active) {\n    \t\t\tthis.editServiceForm.clientServicesId = [];\n    \t\t\tthis.editServiceForm.clientServicesId.push(clientServiceId);\n\n    \t\t\tthis.editServiceForm.serviceId = serviceId;\n    \t\t\tthis.editServiceForm.additional_charge = additionalCharge;\n    \t\t\tthis.editServiceForm.cost = cost;\n    \t\t\tthis.editServiceForm.discount = discount;\n    \t\t\tthis.editServiceForm.reason = reason;\n    \t\t\tthis.editServiceForm.status = status;\n    \t\t\tthis.editServiceForm.note = '';\n    \t\t\tthis.editServiceForm.active = active;\n    \t\t},\n\n    \t\tinit(clientId, serviceId,cid) {\n    \t\t\tthis.changeScreen(1);\n\n    \t\t\tlet _clientId = clientId;\n    \t\t\tlet _serviceId = serviceId;\n    \t\t\tlet _cid = cid;\n    \t\t\tlet _groupId = this.groupid;\n    \t\t\taxios.get('/visa/group/'+ _groupId +'/edit-services', {\n    \t\t\t\tparams: {\n    \t\t\t\t\tclientId: _clientId,\n    \t\t\t\t\tserviceId: _serviceId,\n    \t\t\t\t\tcid: _cid\n    \t\t\t\t}\n    \t\t\t})\n    \t\t\t.then(response => {\n    \t\t\t\tthis.clientServices = response.data;\n    \t\t\t\t//console.log(response.data[0].id);\n    \t\t\t\tthis.callDataTable();\n\n    \t\t\t\tlet { tip, cost, status, remarks, active } = response.data[0];\n    \t\t\t\tlet discount = null;\n    \t\t\t\tlet reason = null;\n\n    \t\t\t\tif(response.data[0].discount){\n\t    \t\t\t\tlet discount = (response.data[0].discount.length > 0) \n\t    \t\t\t\t\t? response.data[0].discount[0].discount_amount\n\t    \t\t\t\t\t: '';\n\t    \t\t\t\tlet reason = (response.data[0].discount.length > 0) \n\t    \t\t\t\t\t? response.data[0].discount[0].reason \n\t    \t\t\t\t\t: '';\n    \t\t\t\t}\n\n    \t\t\t\tthis.setEditServiceForm(_cid, serviceId, tip, cost, discount, reason, status, remarks, active);\n    \t\t\t})\n    \t\t\t.catch(error => console.log(error));\n    \t\t},\n\n    \t\tcallDataTable() {\n\t\t\t\t$('.clients-datatable').DataTable().destroy();\n\n\t\t\t\tsetTimeout(function() {\n\t\t\t\t\t$('.clients-datatable').DataTable({\n\t\t\t            pageLength: 10,\n\t\t\t            responsive: true,\n\t\t\t            dom: '<\"top\"lf>rt<\"bottom\"ip><\"clear\">'\n\t\t\t        });\n\t\t\t\t}, 1000);\n\t\t\t},\n\n\t\t\ttoggleAllCheckbox(e) {\n\t\t\t\tif(e.target.checked == true) {\n\t\t\t\t\tthis.editServiceForm.clientServicesId = [];\n\t\t\t\t\tthis.clientServices.forEach((c, index) => {\n\t\t\t\t\t\tthis.editServiceForm.clientServicesId.push(c.id);\n\t\t\t\t\t});\n\t\t\t\t} else {\n\t\t\t\t\tthis.editServiceForm.clientServicesId = [];\n\t\t\t\t}\n\t\t\t},\n\n\t\t\teditService() {\n\t\t\t\tthis.editServiceForm.submit('patch', '/visa/group/' + this.groupid + '/edit-services')\n\t\t        .then(response => {\n\t\t            if(response.success) {\n\t\t                this.$parent.$parent.fetchDetails();\n\t\t                this.$parent.$parent.fetchMembers();\n\n\t\t                this.$parent.$parent.$refs.addnewserviceref.fetchGroupPackagesServicesList();\n\n\t\t                $('#edit-service-modal').modal('hide');\n\n\t\t                toastr.success(response.message);\n\t\t            }\n\t\t        })\n\t\t        .catch(error => console.log(error));\n\t\t\t}\n\n    \t}\n\n    }\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 295:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-168f2e0e] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-168f2e0e] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["GroupMemberType.vue?d7f16a72"],"names":[],"mappings":";AAsDA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"GroupMemberType.vue","sourcesContent":["<template>\n\t<div>\n\t\t<div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary btn-block\" \n\t\t\t\t\t\t@click=\"changeMemberType('leader')\">\n\t\t\t\t\t\t<i class=\"fa fa-star fa-fw\" ></i> <i class=\"fa fa-star fa-fw\" ></i> Make Group Leader\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-info btn-block\" \n\t\t\t\t\t\t@click=\"changeMemberType('member')\">\n\t\t\t\t\t\t<i class=\"fa fa-star-o fa-fw\" ></i> Make Group Member</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n\n</template>\n\n<script>\n\n    export default {\n\n    \tprops: ['groupid'],\n\n    \tdata() {\n    \t\treturn {\n    \t\t}\n    \t},\n\n    \tmethods: {\n\n    \t\tchangeMemberType(type) {\n    \t\t\t// option = make-group-leader | make-group-member\n    \t\t\tvar vm = this;\n    \t\t\tthis.$parent.$parent.changeMemberType(type);\n\n    \t\t},\n\n\n    \t},\n\n    \tcreated() {\n    \t\t\n    \t}\n\n    }\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 297:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-27ab8230] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-27ab8230] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["EditAddress.vue?d70410be"],"names":[],"mappings":";AAgGA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"EditAddress.vue","sourcesContent":["<template>\n\n\t<form class=\"form-horizontal\" @submit.prevent=\"editAddress\">\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': editAddressForm.errors.has('address') }\">\n\t        <label class=\"col-sm-2 control-label\">\n\t            Name\n\t            <span class=\"text-danger\">*</span>\n\t        </label>\n\n\t        <div class=\"col-sm-10\">\n\t            <input type=\"text\" class=\"form-control m-top-0\" name=\"name\" v-model=\"editAddressForm.name\">\n\t            <span class=\"help-block\" v-if=\"editAddressForm.errors.has('name')\" v-text=\"editAddressForm.errors.get('name')\"></span>\n\t        </div>\n\t    </div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': editAddressForm.errors.has('address') }\">\n\t        <label class=\"col-sm-2 control-label\">\n\t            Number\n\t            <span class=\"text-danger\">*</span>\n\t        </label>\n\n\t        <div class=\"col-sm-10\">\n\t            <input type=\"text\" class=\"form-control m-top-0\" name=\"number\" v-model=\"editAddressForm.number\">\n\t            <span class=\"help-block\" v-if=\"editAddressForm.errors.has('number')\" v-text=\"editAddressForm.errors.get('number')\"></span>\n\t        </div>\n\t    </div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': editAddressForm.errors.has('address') }\">\n\t        <label class=\"col-sm-2 control-label\">\n\t            Address\n\t            <span class=\"text-danger\">*</span>\n\t        </label>\n\n\t        <div class=\"col-sm-10\">\n\t            <input type=\"text\" class=\"form-control m-top-0\" name=\"address\" v-model=\"editAddressForm.address\">\n\t            <span class=\"help-block\" v-if=\"editAddressForm.errors.has('address')\" v-text=\"editAddressForm.errors.get('address')\"></span>\n\t        </div>\n\t    </div>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Edit</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\n    export default {\n\n    \tprops: ['groupid'],\n\n    \tdata() {\n    \t\treturn {\n    \t\t\teditAddressForm: new Form({\n\t\t\t\t\taddress: '',\n\t\t\t\t\tnumber: '',\n\t\t\t\t\tname: '',\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n    \t\t}\n    \t},\n\n    \tmethods: {\n\n    \t\tsetGroupAddress() {\n    \t\t\taxiosAPIv1.get('/visa/group/' + this.groupid)\n\t\t\t\t.then(response => {\t\t\t\t\n\t\t\t\t\tthis.editAddressForm.address = response.data.address;\n\t\t\t\t\tthis.editAddressForm.number = response.data.user.address.contact_number;\n\t\t\t\t\tthis.editAddressForm.name = response.data.name;\n\t\t\t\t})\n\t\t\t\t.catch(error => console.log(error));\n    \t\t},\n\n    \t\teditAddress() {\n    \t\t\tthis.editAddressForm.submit('patch', '/visa/group/' + this.groupid + '/edit-address')\n\t\t\t\t.then(response => {\n\t\t\t\t\tif(response.success) {\n\t\t\t\t\t\tthis.$parent.$parent.fetchDetails();\n\n\t\t\t\t\t\t$('#edit-address-modal').modal('hide');\n\n\t\t\t\t\t\ttoastr.success(response.message);\n\t\t\t\t\t}\n\t\t\t\t})\n\t\t\t\t.catch(error => console.log(error));\n    \t\t}\n\n    \t}\n\n    }\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 298:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-3928fa9c] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-3928fa9c] {\n\tmargin-right: 10px;\n}\n.checkbox-inline[data-v-3928fa9c] {\n\tmargin-top: -7px;\n}\n", "", {"version":3,"sources":["AddNewService.vue?a9451898"],"names":[],"mappings":";AA+eA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA;AACA;CACA,iBAAA;CACA","file":"AddNewService.vue","sourcesContent":["<template>\n\t<form class=\"form-horizontal\" @submit.prevent=\"addNewService\">\n\n\t\t<div v-show=\"screen == 1\">\n\t    \t<div class=\"form-group\" :class=\"{ 'has-error': addNewServiceForm.errors.has('services') }\">\n\t\t        <label class=\"col-md-2 control-label\">\n\t\t            Service:\n\t\t        </label>\n\n\t\t        <div class=\"col-md-10\">\n\t\t            <select data-placeholder=\"Select Service\" class=\"chosen-select-for-service\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t                <option v-for=\"service in servicesList\" :value=\"service.id\">\n\t\t                \t{{ (service.detail).trim() }}\n\t\t                </option>\n\t\t            </select>\n\n\t\t            <span class=\"help-block\" v-if=\"addNewServiceForm.errors.has('services')\" v-text=\"addNewServiceForm.errors.get('services')\"></span>\n\n\t\t            <div class=\"spacer-10\"></div>\n\n\t\t            <p>\n\t\t            \tIf you want to view all services at once, please \n\t\t            \t<a href=\"/visa/service/view-all\" target=\"_blank\">click here</a>\n\t\t            </p>\n\t\t        </div>\n\t\t    </div>\n\n\t\t    <div class=\"form-group\">\n\t\t    \t<label class=\"col-md-2 control-label\">\n\t\t            Note:\n\t\t        </label>\n\t\t        <div class=\"col-md-10\">\n\t\t        \t<input type=\"text\" class=\"form-control\" name=\"note\" v-model=\"addNewServiceForm.note\" placeholder=\"for internal use only, only employees can see.\">\n\t\t        </div>\n\t\t    </div>\n\n\t\t    <div v-show=\"addNewServiceForm.services.length <= 1\">\n\t\t\t    <div class=\"form-group\">\n\t\t\t    \t<label class=\"col-md-2 control-label\">\n\t\t\t            Cost:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-10\">\n\t\t\t        \t<input type=\"text\" class=\"form-control\" name=\"cost\" :value=\"cost\" readonly>\n\t\t\t        </div>\n\t\t\t    </div>\n\n<!-- \t\t\t    <div class=\"form-group\">\n\t\t\t    \t<label class=\"col-md-2 control-label\">\n\t\t\t            Report :\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-10\">\n\t\t\t        \t<input type=\"text\" class=\"form-control\" name=\"reporting\" v-model=\"addNewServiceForm.reporting\">\n\t\t\t        </div>\n\t\t\t    </div> -->\n\n\t\t\t    <div class=\"form-group\" :class=\"{ 'has-error': addNewServiceForm.errors.has('discount') }\">\n\t\t\t    \t<label class=\"col-md-2 control-label\">\n\t\t\t            Discount:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-4\">\n\t\t\t        \t<input type=\"text\" class=\"form-control\" name=\"discount\" v-model=\"addNewServiceForm.discount\">\n\t\t\t        \t<span class=\"help-block\" v-if=\"addNewServiceForm.errors.has('discount')\" v-text=\"addNewServiceForm.errors.get('discount')\"></span>\n\t\t\t        </div>\n\t\t\t        <label class=\"col-md-2 control-label\" v-if=\"addNewServiceForm.discount!=''\">\n\t\t\t            Reason:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-4\" v-if=\"addNewServiceForm.discount!=''\">\n\t\t\t        \t<input type=\"text\" class=\"form-control\" name=\"reason\" v-model=\"addNewServiceForm.reason\">\n\t\t\t        </div>\n\t\t\t    </div>\n\t\t    </div>\n\n\t\t    <a class=\"btn btn-primary pull-right\" @click=\"changeScreen(2)\">Continue</a>\n\t    \t<a class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</a>\n    \t</div>\n\n    \t<div v-show=\"screen == 2\">\n    \t\t<div class=\"table-responsive\">\n                <table class=\"table table-striped table-bordered table-hover add-new-service-datatable\">\n                    <thead>\n\t                    <tr>\n\t                        <th class=\"text-center\">\n\t                        \t<input type=\"checkbox\" @click=\"toggleAllCheckbox\">\n\t                        </th>\n\t                        <th>Name</th>\n\t                        <th>Client ID</th>\n\t                        <th>Packages</th>\n\t                    </tr>\n                    </thead>\n                    <tbody>\n                    \t<tr v-for=\"data in clients\">\n                    \t\t<td class=\"text-center\">\n                    \t\t\t<input type=\"checkbox\" :value=\"data.client.id\" v-model=\"addNewServiceForm.clients\">\n                    \t\t</td>\n                    \t\t<td>{{ data.client.full_name }}</td>\n                    \t\t<td>{{ data.client.id }}</td>\n                    \t\t<td>\n                    \t\t\t<select class=\"form-control\" :class=\"'package-select-' + data.client.id\" \n                    \t\t\t:disabled=\"addNewServiceForm.clients.indexOf(data.client.id) == -1\">\n                    \t\t\t\t<option value=\"Generate new package\">Generate new package</option>\n                    \t\t\t\t<option v-for=\"package in data.client.packages\" :value=\"package.tracking\" v-if=\"package.status != 4\">\n                    \t\t\t\t\t{{package.tracking}} \n                    \t\t\t\t\t({{ package.log_date | commonDate }})\n                    \t\t\t\t</option>\n                    \t\t\t</select>\n                    \t\t</td>\n                    \t</tr>\n                    </tbody>\n                </table>\n            </div>\n\n            <a class=\"btn btn-primary pull-right\" @click=\"changeScreen(3)\">Continue</a>\n\t    \t<a class=\"btn btn-default pull-right m-r-10\" @click=\"changeScreen(1)\">Back</a>\n    \t</div>\n\n    \t<div v-show=\"screen == 3\">\n    \t\t<div class=\"ios-scroll\">\n    \t\t\t<label class=\"checkbox-inline\">\n    \t\t\t\t<input type=\"checkbox\" @click=\"checkAll\" class=\"checkbox-all\"> Check all required documents\n    \t\t\t</label>\n\n    \t\t\t<div style=\"height:20px; clear:both;\"></div> <!-- spacer -->\n\n\t    \t\t<div v-for=\"(clientId, index1) in addNewServiceForm.clients\" class=\"panel panel-default\">\n\t               \t<div class=\"panel-heading\">\n\t                    <strong>{{ getClientName(clientId) }}</strong>\n\t                </div>\n\t                <div class=\"panel-body\">\n\t                    \n\t                \t<div v-for=\"(serviceId, index2) in addNewServiceForm.services\" class=\"panel panel-default\">\n\t\t\t               \t<div class=\"panel-heading\">\n\t\t\t                    {{ getServiceName(serviceId) }}\n\t\t\t                    <label class=\"checkbox-inline pull-right\">\n\t\t\t                    \t<input type=\"checkbox\" @click=\"checkAll\" :class=\"'checkbox-' + index1 + '-' + index2\" :data-chosen-class=\"'chosen-select-for-required-docs-' + index1 + '-' + index2\"> Check all required documents\n\t\t\t                    </label>\n\t\t\t                </div>\n\t\t\t                <div class=\"panel-body\">\n\t\t\t                    \n\t\t\t                \t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t\t\t\t\t       \tRequired Documents:\n\t\t\t\t\t\t\t\t    </label>\n\n\t\t\t\t\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t\t\t\t            <select data-placeholder=\"Select Docs\" class=\"chosen-select-for-required-docs\" :class=\"'chosen-select-for-required-docs-' + index1 + '-' + index2\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t\t\t\t\t\t                <option v-for=\"doc in requiredDocs[index2]\" :value=\"doc.id\">\n\t\t\t\t\t\t\t                \t{{ doc.title }}\n\t\t\t\t\t\t\t                </option>\n\t\t\t\t\t\t\t            </select>\n\t\t\t\t\t\t\t\t    </div>\n\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t\t\t\t\t       \tOptional Documents:\n\t\t\t\t\t\t\t\t    </label>\n\n\t\t\t\t\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t\t\t\t            <select data-placeholder=\"Select Docs\" class=\"chosen-select-for-optional-docs\" :class=\"'chosen-select-for-optional-docs-' + index1 + '-' + index2\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t\t\t\t\t\t                <option v-for=\"doc in optionalDocs[index2]\" :value=\"doc.id\">\n\t\t\t\t\t\t\t                \t{{ doc.title }}\n\t\t\t\t\t\t\t                </option>\n\t\t\t\t\t\t\t            </select>\n\t\t\t\t\t\t\t\t    </div>\n\t\t\t\t\t\t\t\t</div>\n\n\t\t\t                </div> <!-- SERVICE panel-body -->\n\t\t\t            </div> <!-- SERVICE panel -->\n\n\t                </div> <!-- CLIENT panel-body -->\n\t            </div> <!-- CLIENT panel -->\n            </div>\n\n    \t\t<button v-if=\"!loading\" type=\"submit\" class=\"btn btn-primary pull-right\">Save</button>\n    \t\t<button v-if=\"loading\" type=\"button\" class=\"btn btn-default pull-right\">\n\t            <div class=\"sk-spinner sk-spinner-wave\" style=\"width:40px; height:20px;\">\n\t                <div class=\"sk-rect1\"></div>\n\t                <div class=\"sk-rect2\"></div>\n\t                <div class=\"sk-rect3\"></div>\n\t                <div class=\"sk-rect4\"></div>\n\t                <div class=\"sk-rect5\"></div>\n\t             </div>\n\t        </button>\n\t    \t<a class=\"btn btn-default pull-right m-r-10\" @click=\"changeScreen(2)\">Back</a>\n    \t</div>\n\n    </form>\n\n</template>\n\n<script>\n    export default{\n    \tprops: ['groupid'],\n\n        data() {\n        \treturn {\n        \t\tloading: false,\n\n        \t\tscreen: 1,\n\n        \t\tservicesList: [],\n\n        \t\tcost: '',\n\n        \t\tclients: [],\n\n        \t\tdetailArray: [],\n        \t\trequiredDocs: [],\n        \t\toptionalDocs: [],\n\n        \t\taddNewServiceForm: new Form({\n\t\t\t\t\tservices: [],\n\t\t\t\t\tnote: '',\n\t\t\t\t\tdiscount: '',\n\t\t\t\t\treason: '',\n\t\t\t\t\treporting: '',\n\n\t\t\t\t\tclients: [],\n\t\t\t\t\tpackages:[],\n\t\t\t\t\tdocs: []\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\t\n        \t}\n        },\n\n        methods: {\n        \tfetchServiceList() {\n        \t\taxios.get('/visa/service-manager/show')\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tthis.servicesList = response.data.filter(r => r.parent_id != 0);\n\n\t\t\t\t\t\tsetTimeout(() => {\n\t\t\t\t\t\t\t$(\".chosen-select-for-service\").trigger(\"chosen:updated\");\n\t\t\t\t\t\t}, 1000);\n\t\t\t\t\t\t\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => console.log(error) );\n        \t},\n\n        \tresetAddNewServiceForm() {\n        \t\tthis.screen = 1;\n\n        \t\tthis.addNewServiceForm.services = [];\n        \t\tthis.addNewServiceForm.note = '';\n        \t\tthis.addNewServiceForm.discount = '';\n        \t\tthis.addNewServiceForm.reason = '';\n        \t\tthis.addNewServiceForm.reporting = '';\n        \t\tthis.addNewServiceForm.clients = [];\n        \t\tthis.addNewServiceForm.packages = [];\n        \t\tthis.addNewServiceForm.docs = [];\n\n        \t\tthis.cost = '';\n        \t\t// Deselect All\n\t\t\t\t$('.chosen-select-for-service, .chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');\n\t\t\t\t\n\t\t\t\tthis.detailArray = [];\n\t\t\t\tthis.requiredDocs = [];\n\t\t\t\tthis.optionalDocs = [];\n        \t},\n\n        \taddNewService() {\n        \t\tthis.loading = true;\n\n        \t\tthis.addNewServiceForm.packages = [];\n\t\t\t\tvar tbl = $('.add-new-service-datatable').DataTable();\n\t        \tthis.addNewServiceForm.clients.forEach((c, index) => {\n\t        \t\tlet value = tbl.$('select.package-select-' + c).val();\n\t        \t\tthis.addNewServiceForm.packages.push(value);\n\t        \t});\n\n\t        \tvar docsArray = [];\n        \t\tvar requiredDocs = '';\n        \t\tvar optionalDocs = '';\n        \t\tvar hasRequiredDocsErrors = [];\n\n\t        \tthis.addNewServiceForm.clients.forEach((client, index1) => {\n\t        \t\tthis.addNewServiceForm.services.forEach((service, index2) => {\n\t        \t\t\t$('.chosen-select-for-required-docs-' + index1 + '-' + index2 +' option:selected').each(function() {\n\t\t        \t\t\trequiredDocs += $(this).val() + ',';\n\t\t\t\t\t    });\n\n\t        \t\t\tlet requiredOptions = $('.chosen-select-for-required-docs-' + index1 + '-' + index2 +' option').length;\n\t\t\t\t\t    let requiredCount = $('.chosen-select-for-required-docs-' + index1 + '-' + index2 +' option:selected').length;\n\t        \t\t\tif(requiredCount == 0 && requiredOptions > 0) {\n\t        \t\t\t\thasRequiredDocsErrors.push({\n\t        \t\t\t\t\tclient: this.getClientName(client),\n\t        \t\t\t\t\tservice: this.getServiceName(service)\n\t        \t\t\t\t});\n\t        \t\t\t}\n\n\t\t\t\t\t    $('.chosen-select-for-optional-docs-' + index1 + '-' + index2 + ' option:selected').each(function() {\n\t\t\t\t\t        optionalDocs += $(this).val() + ',';\n\t\t\t\t\t    });\n\n\t\t\t\t\t    docsArray.push((requiredDocs + optionalDocs).slice(0, -1));\n\t\t\t\t    \trequiredDocs = '';\n\t\t\t\t    \toptionalDocs = '';\n\t        \t\t});\n\t        \t});\n        \t\t\n        \t\tthis.addNewServiceForm.docs = docsArray;\n\n        \t\tif(hasRequiredDocsErrors.length > 0){\n        \t\t\tfor(let i=0; i<hasRequiredDocsErrors.length; i++) {\n        \t\t\t\ttoastr.error('Please select from required documents.', hasRequiredDocsErrors[i].client + ' - ' + hasRequiredDocsErrors[i].service);\n        \t\t\t}\n        \t\t\tthis.loading = false;\n         \t\t} else {\n\n\t\t        \tthis.addNewServiceForm.submit('post', '/visa/group/' + this.groupid + '/add-service')\n\t\t\t            .then(response => {\n\t\t\t                if(response.success) {\n\t\t\t                \tthis.$parent.$parent.fetchDetails();\n\t\t\t                \tthis.$parent.$parent.fetchMembers();\n\n\t\t\t                \tthis.fetchGroupPackagesServicesList();\n\t\t\t                \tthis.resetAddNewServiceForm();\n\n\t\t\t                \t$('#add-new-service-modal').modal('hide');\n\n\t\t\t                \ttoastr.success(response.message);\n\t\t\t                }\n\n\t\t\t                this.loading = false;\n\t\t\t            })\n\t\t\t            .catch(error => {\n\t\t\t            \tfor(var key in error) {\n\t\t\t            \t\tif(error.hasOwnProperty(key)) toastr.error(error[key][0])\n\t\t\t            \t}\n\n\t\t\t            \tthis.loading = false;\n\t\t\t            });\n\t\t\t    }\n\t\t\t},\n\n\t\t\tgetDocs(servicesId) {\n\t\t\t\taxios.get('/visa/get-docs', {\n\t\t\t\t\t\tparams: {\n\t\t\t\t\t      \tservicesId: servicesId\n\t\t\t\t\t    }\n\t\t\t\t\t})\n\t\t\t\t\t.then(result => {\n\t\t\t\t\t\tthis.detailArray = result.data.detailArray;\n\t\t\t\t        this.requiredDocs = result.data.docsNeededArray;\n\t\t\t\t        this.optionalDocs = result.data.docsOptionalArray;\n\t\t\t\t\t});\n\t\t\t},\n\n\t\t\tgetClientName(clientId) {\n\t\t\t\tvar clientName = '';\n\t\t\t\tthis.clients.forEach(client => {\n\t\t\t\t\tif(client.client.id == clientId) {\n\t\t\t\t\t\tclientName = client.client.first_name + ' ' + client.client.last_name; \n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\treturn clientName;\n\t\t\t},\n\n\t\t\tgetServiceName(serviceId) {\n\t\t\t\tvar serviceName = '';\n\t\t\t\tthis.servicesList.forEach(service => {\n\t\t\t\t\tif(service.id == serviceId) {\n\t\t\t\t\t\tserviceName = service.detail; \n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\treturn serviceName;\n\t\t\t},\n\n\t\t\tinitChosenSelect() {\n\t\t\t\t$('.chosen-select-for-service').chosen({\n\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\tsearch_contains: true\n\t\t\t\t});\n\n\t\t\t\tlet vm = this;\n\n\t\t\t\t$('.chosen-select-for-service').on('change', function() {\n\t\t\t\t\tlet services = $(this).val();\n\n\t\t\t\t\tvm.getDocs(services);\n\n\t\t\t\t\tvm.addNewServiceForm.services = services;\n\n\t\t\t\t\t// Set cost input\n\t\t\t\t\tif(services.length == 1) {\n\t\t\t\t\t\tvm.servicesList.map(s => {\n\t\t\t\t\t\t\tif(s.id == services[0]) {\n\t\t\t\t\t\t\t\tvm.cost = s.cost;\n\t\t\t\t\t\t\t}\n\t\t\t\t\t\t});\n\t\t\t\t\t} else {\n\t\t\t\t\t\tvm.cost = '';\n\t\t\t\t\t}\n\n\t\t\t\t});\n\n\t\t\t},\n\n\t\t\tchangeScreen(screen) {\n\t\t\t\tif(screen == 2) {\n\t\t\t\t\tif(this.addNewServiceForm.services.length == 0) {\n\t\t\t\t\t\ttoastr.error('Please select at least one service');\n\t\t\t\t\t\treturn false;\n\t\t\t\t\t}\n\t\t\t\t} else if(screen == 3) {\n\t\t\t\t\tif(this.addNewServiceForm.clients.length == 0) {\n\t        \t\t\ttoastr.error('Please select at least one member');\n\t\t\t\t\t\treturn false;\n\t        \t\t}\n\n\t        \t\tsetTimeout(() => {\n\t\t\t\t        $('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen('destroy');\n\t\t\t\t        $('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({\n\t\t\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\t\t\tsearch_contains: true\n\t\t\t\t\t\t});\n\n\t\t\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');\n\t\t\t        }, 1000);\n\t\t\t\t}\n\n\t\t\t\tthis.screen = screen;\n\t\t\t},\n\n\t\t\tfetchGroupPackagesServicesList() {\n\t\t\t\taxios.get('/visa/group/'+ this.groupid +'/packages-services')\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tthis.clients = response.data;\n\n\t\t\t\t\t\tthis.callDataTable();\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => console.log(error) );\n\t\t\t},\n\n\t\t\tcallDataTable() {\n\t\t\t\t$('.add-new-service-datatable').DataTable().destroy();\n\n\t\t\t\tsetTimeout(function() {\n\t\t\t\t\t$('.add-new-service-datatable').DataTable({\n\t\t\t            pageLength: 10,\n\t\t\t            responsive: true,\n\t\t\t            dom: '<\"top\"lf>rt<\"bottom\"ip><\"clear\">'\n\t\t\t        });\n\t\t\t\t}, 1000);\n\t\t\t},\n\n\t\t\ttoggleAllCheckbox(e) {\n\t\t\t\tif(e.target.checked == true) {\n\t\t\t\t\tthis.addNewServiceForm.clients = [];\n\t\t\t\t\tthis.clients.forEach((c, index) => {\n\t\t\t\t\t\tthis.addNewServiceForm.clients.push(c.client.id);\n\t\t\t\t\t});\n\t\t\t\t} else {\n\t\t\t\t\tthis.addNewServiceForm.clients = [];\n\t\t\t\t}\n\t\t\t},\n\n\t\t\tcheckAll(e) {\n\t\t\t\tlet checkbox = e.target.className;\n\n\t\t\t\tif(checkbox == 'checkbox-all') {\n\t\t\t\t\tif($('.' + checkbox).is(':checked'))\n\t\t\t\t\t\t$('.chosen-select-for-required-docs option').prop('selected', true);\n\t\t\t\t\telse\n\t\t\t\t\t\t$('.chosen-select-for-required-docs option').prop('selected', false);\n\n\t\t\t\t\t$('.chosen-select-for-required-docs option').trigger('chosen:updated');\n\t\t\t\t} else {\n\t\t\t\t\tlet chosen = e.target.dataset.chosenClass;\n\n\t\t\t\t\tif($('.' + checkbox).is(':checked'))\n\t\t\t\t\t\t$('.' + chosen + ' option').prop('selected', true);\n\t\t\t\t\telse\n\t\t\t\t\t\t$('.' + chosen + ' option').prop('selected', false);\n\n\t\t\t\t\t$('.' + chosen + ' option').trigger('chosen:updated');\n\t\t\t\t}\n\t\t\t}\n        },\n\n        created() {\n        \tthis.fetchServiceList();\n        \tthis.fetchGroupPackagesServicesList();\n        },\n\n        mounted() {\n        \tthis.initChosenSelect();\n        }\n    }\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n\t.checkbox-inline {\n\t\tmargin-top: -7px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(7)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(25),
  /* template */
  __webpack_require__(31),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/clients/TransactionLogs.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] TransactionLogs.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6aefbfa6", Component.options)
  } else {
    hotAPI.reload("data-v-6aefbfa6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 301:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-3ca94bef] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-3ca94bef] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["DeleteMember.vue?71ad53b8"],"names":[],"mappings":";AA8DA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"DeleteMember.vue","sourcesContent":["<template>\n\n\t<form class=\"form-horizontal\" @submit.prevent=\"deleteMember\">\n\n\t\t<p>Are you sure you want to delete this member?</p>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Delete</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\n    export default {\n\n    \tprops: ['groupid'],\n\n    \tdata() {\n    \t\treturn {\n    \t\t\tdeleteMemberForm: new Form({\n\t\t\t\t\tmemberId: null\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n    \t\t}\n    \t},\n\n    \tmethods: {\n\n    \t\tsetMemberId(id) {\n    \t\t\tthis.deleteMemberForm.memberId = id;\n    \t\t},\n\n    \t\tresetDeleteMemberForm() {\n    \t\t\tthis.deleteMemberForm.memberId = null;\n    \t\t},\n\n    \t\tdeleteMember() {\n    \t\t\tthis.deleteMemberForm.submit('post', '/visa/group/' + this.groupid + '/delete-member')\n\t\t        .then(response => {\n\t\t        \tthis.resetDeleteMemberForm();\n\n\t\t            $('#delete-member-modal').modal('hide');\n\n\t\t            if(response.success) {\n\t\t                this.$parent.$parent.fetchMembers();\n\t\t                \n\t\t                toastr.success(response.message);\n\t\t            } else {\n\t\t            \ttoastr.error(response.message);\n\t\t            }\n\t\t        })\n\t\t        .catch(error => console.log(error));\n    \t\t}\n\n    \t}\n\n    }\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 302:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-5199a8a8] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-5199a8a8] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["AddFund.vue?3a4970c6"],"names":[],"mappings":";AAgGA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"AddFund.vue","sourcesContent":["<template>\n\n\t<form class=\"form-horizontal\" @submit.prevent=\"addFunds\">\n\n\t\t<div class=\"form-group\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Option:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<select class=\"form-control\" name=\"option\" v-model=\"addFundForm.option\">\n\t\t    \t\t<option value=\"Deposit\">Deposit</option>\n\t\t    \t\t<option value=\"Payment\">Payment</option>\n\t\t    \t\t<option value=\"Discount\">Discount</option>\n\t\t    \t\t<option value=\"Refund\">Refund</option>\n\t\t    \t</select>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': addFundForm.errors.has('amount') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Amount:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"amount\" v-model=\"addFundForm.amount\">\n\n\t\t    \t<span class=\"help-block\" v-if=\"addFundForm.errors.has('amount')\" v-text=\"addFundForm.errors.get('amount')\"></span>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div v-if=\"addFundForm.option == 'Discount' || addFundForm.option == 'Refund'\">\n\t\t\t\n\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': addFundForm.errors.has('reason') }\">\n\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t        Reason:\n\t\t        </label>\n\t\t\t    <div class=\"col-md-10\">\n\t\t\t    \t<input type=\"text\" class=\"form-control\" name=\"reason\" v-model=\"addFundForm.reason\">\n\n\t\t\t    \t<span class=\"help-block\" v-if=\"addFundForm.errors.has('reason')\" v-text=\"addFundForm.errors.get('reason')\"></span>\n\t\t\t    </div>\n\t\t\t</div>\n\n\t\t</div>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Add</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\n    export default {\n\n    \tprops: ['groupid'],\n\n    \tdata() {\n    \t\treturn {\n    \t\t\taddFundForm: new Form({\n\t\t\t\t\toption: 'Deposit',\n\t\t\t\t\tamount: '',\n\t\t\t\t\treason: ''\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n    \t\t}\n    \t},\n\n    \tmethods: {\n    \t\tresetAddFundForm() {\n    \t\t\tthis.addFundForm.option = 'Deposit';\n    \t\t\tthis.addFundForm.amount = '';\n    \t\t\tthis.addFundForm.reason = '';\n    \t\t},\n\n    \t\taddFunds() {\n    \t\t\tthis.addFundForm.submit('post', '/visa/group/' + this.groupid + '/add-funds')\n\t\t        .then(response => {\n\t\t            if(response.success) {\n\t\t                this.$parent.$parent.fetchDetails();\n\n\t\t                this.resetAddFundForm();\n\n\t\t                $('#add-funds-modal').modal('hide');\n\n\t\t                toastr.success(response.message);\n\t\t            }\n\t\t        })\n\t\t        .catch(error => console.log(error));\n    \t\t}\n    \t}\n\n    }\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 307:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-73df72fa] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-73df72fa] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["AddNewMember.vue?5c172394"],"names":[],"mappings":";AAuHA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"AddNewMember.vue","sourcesContent":["<template>\n\t<form class=\"form-horizontal\" @submit.prevent=\"addNewMember\">\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': addNewMemberForm.errors.has('clientId') }\">\n\t        <label class=\"col-sm-2 control-label\">\n\t            Clients\n\t        </label>\n\n\t        <div class=\"col-sm-10\">\n\t            <select data-placeholder=\"Select clients\" class=\"chosen-select-for-clients\" multiple style=\"width:100%;\">\n\t\t            <option v-for=\"client in clients\" :value=\"client.id\">\n\t\t               \t{{ client.name }}\n\t\t            </option>\n\t\t        </select>\n\t        </div>\n\t    </div>\n\n\t    <button type=\"submit\" class=\"btn btn-primary pull-right\">Add</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n</template>\n\n<script>\n\n    export default {\n\n    \tprops: ['groupid'],\n\n    \tdata() {\n    \t\treturn {\n    \t\t\tclients: [],\n\n    \t\t\taddNewMemberForm: new Form({\n\t\t\t\t\tclientIds: []\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n    \t\t}\n    \t},\n\n    \tmethods: {\n    \t\tfetchClients(q) {\n\t    \t\taxiosAPIv1.get('/visa/client/get-available-visa-clients', {\n\t\t\t\t \tparams: {\n\t\t\t\t \t\tgroupId: this.groupid,\n\t\t\t\t \t\tq: q\n\t\t\t\t \t}\n\t\t\t\t})\n\t\t\t\t.then(response => {\n\t\t\t\t\tif(response.data.length > 0){\n\t\t\t\t\t\tthis.clients.push({\n\t\t\t               id:response.data[0].id,\n\t\t\t               name:response.data[0].name });\n\t\t\t\t\t}\n\n\t\t\t \t\tsetTimeout(() => {\n\t\t\t\t\t\tvar my_val = $('.chosen-select-for-clients').val();\n\t\t\t\t\t\t//console.log(my_val);\n\t\t\t\t\t\t$(\".chosen-select-for-clients\").val(my_val).trigger(\"chosen:updated\");\n\t\t\t\t\t}, 1000);\n\t\t\t\t});\n    \t\t},\n\n    \t\tresetAddNewMemberForm() {\n    \t\t\tthis.addNewMemberForm = new Form({\n\t\t\t\t\tclientIds: []\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL });\n    \t\t},\n\n    \t\taddNewMember() {\n    \t\t\tif(this.addNewMemberForm.clientIds.length == 0) {\n    \t\t\t\ttoastr.error('Clients field is required.');\n    \t\t\t} else {\n    \t\t\t\tthis.addNewMemberForm.submit('post', '/visa/group/' + this.groupid + '/add-members')\n\t\t            .then(result => {\n\t\t                if(result.success) {\n\t\t                \tthis.clients = [];\n\t\t                \tthis.resetAddNewMemberForm();\n\t\t                \t$(\".chosen-select-for-clients\").empty().trigger('chosen:updated');\n\n\t\t                \t$('#add-new-member-modal').modal('hide');\n\n\t\t\t\t\t \t\ttoastr.success(result.message);\n\n\t\t\t\t\t \t\tthis.$parent.$parent.fetchMembers();\n\n\t\t\t\t\t \t\tthis.$parent.$parent.$refs.addnewserviceref.fetchGroupPackagesServicesList();\n\t\t\t\t\t\t}\n\t\t            })\n\t\t            .catch(error => console.log(error));\n    \t\t\t}\n    \t\t}\n    \t},\n\n    \tmounted() {\n\t  \t\t$('.chosen-select-for-clients').chosen({\n\t\t\t\twidth: \"100%\",\n\t\t\t\tno_results_text: \"Searching Client # : \",\n\t\t\t\tsearch_contains: true\n\t\t\t});\n\n\t\t\t$('.chosen-select-for-clients').on('change', () => {\n\t\t\t\tlet clientIds = $('.chosen-select-for-clients').val();\n\n\t\t\t\tthis.addNewMemberForm.clientIds = clientIds;\n\t\t\t});\n\n\t\t\t$('body').on('keyup', '.chosen-container input[type=text]', (e) => {\n\t\t\t\tlet q = $('.chosen-container input[type=text]').val();\n\t\t\t\tif( q.length >= 4 ){\n\t\t\t\t\tthis.fetchClients(q);\n\t\t\t\t}\n\t\t\t});\n    \t}\n\n    }\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 31:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.log2.year != null) ? _c('div', {
    staticClass: "row timeline-movement timeline-movement-top no-dashed no-side-margin"
  }, [_c('div', {
    staticClass: "timeline-badge timeline-filter-movement"
  }, [_c('a', {
    attrs: {
      "href": "#"
    }
  }, [_c('span', [_vm._v(_vm._s(_vm.log2.year))])])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "row timeline-movement  no-side-margin"
  }, [(_vm.log2.month != null) ? _c('div', {
    staticClass: "timeline-badge"
  }, [_c('span', {
    staticClass: "timeline-balloon-date-day"
  }, [_vm._v(_vm._s(_vm.log2.day))]), _vm._v(" "), _c('span', {
    staticClass: "timeline-balloon-date-month"
  }, [_vm._v(_vm._s(_vm.log2.month))])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "col-sm-12  timeline-item"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-2 col-sm-offset-1"
  }, [_c('div', {
    staticClass: "timeline-panel debits"
  }, [_c('ul', {
    staticClass: "timeline-panel-ul"
  }, [_c('li', [_c('span', {
    staticClass: "causale"
  }, [_vm._v("Balance: "), _c('b', [_vm._v(_vm._s(_vm._f("currency")(_vm.log2.data.balance)))])])]), _vm._v(" "), _c('li', [_c('p', [_c('small', {
    staticClass: "text-muted"
  }, [_vm._v("Amount : "), _c('b', [_vm._v(_vm._s(_vm._f("currency")(_vm.log2.data.amount)))])])])]), _vm._v(" "), _c('li', [_c('p', [_c('small', {
    staticClass: "text-muted"
  }, [_vm._v("Type : "), _c('b', [_vm._v(_vm._s(_vm.log2.data.type))])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-9"
  }, [_c('div', {
    staticClass: "timeline-panel debits"
  }, [_c('ul', {
    staticClass: "timeline-panel-ul"
  }, [_c('li', [_c('span', {
    staticClass: "importo",
    domProps: {
      "innerHTML": _vm._s(this.detail)
    }
  })]), _vm._v(" "), _c('li', [_c('span', {
    staticClass: "causale"
  }, [_vm._v(_vm._s(_vm.log2.data.processor))])]), _vm._v(" "), _c('li', [_c('p', [_c('small', {
    staticClass: "text-muted"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-time"
  }), _vm._v(_vm._s(_vm.log2.data.date))])])])])])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6aefbfa6", module.exports)
  }
}

/***/ }),

/***/ 311:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-bbd2ec96] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-bbd2ec96] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["Transfer.vue?40b7f7cb"],"names":[],"mappings":";AAsNA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"Transfer.vue","sourcesContent":["<template>\n\n\t<div>\n\n\t\t<div v-show=\"screen == 1\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary btn-block\" \n\t\t\t\t\t\t@click=\"getDetails('client-to-group', 2)\">Client to Group</button>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-info btn-block\" \n\t\t\t\t\t\t@click=\"getDetails('group-to-client', 2)\">Group to Client</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div v-show=\"screen == 2\">\n\t\t\t<form class=\"form-horizontal\" @submit.prevent=\"transfer\">\n\t\t\t\t\n\t\t\t\t<div class=\"table-responsive\">\n\t                <table class=\"table table-striped table-bordered table-hover transfer-datatable\">\n\t                    <thead>\n\t\t                    <tr>\n\t\t                        <th class=\"text-center\">\n\t\t                        \t<input type=\"checkbox\" @click=\"toggleAllCheckbox\">\n\t\t                        </th>\n\t\t                        <th>Detail</th>\n\t\t                        <th>Status</th>\n\t\t                        <th>Tracking</th>\n\t\t                        <th>Packages</th>\n\t\t                    </tr>\n\t                    </thead>\n\t                    <tbody>\n\t                    \t<tr v-for=\"service in services\">\n\t                    \t\t<td class=\"text-center\">\n\t                    \t\t\t<input type=\"checkbox\" :value=\"service.id\" v-model=\"transferForm.services\">\n\t                    \t\t</td>\n\t                    \t\t<td>{{ service.detail }}</td>\n\t                    \t\t<td>{{ service.status }}</td>\n\t                    \t\t<td>{{ service.tracking }}</td>\n\t                    \t\t<td>\n\t                    \t\t\t<select class=\"form-control\" :class=\"'transfer-package-select-' + service.id\" \n\t                    \t\t\t>\n\t                    \t\t\t\t<option value=\"Generate new package\">Generate new package</option>\n\t                    \t\t\t\t<option v-for=\"package in packages\" :value=\"package.tracking\" >\n\t                    \t\t\t\t\tPackage #{{package.tracking}}\n\t                    \t\t\t\t\t({{ package.log_date | commonDate }})\n\t                    \t\t\t\t</option>\n\t                    \t\t\t</select>\n\t                    \t\t</td>\n\t                    \t</tr>\n\t                    </tbody>\n\t                </table>\n\t            </div>\n\n\t\t\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Complete Transfer</button>\n\t\t    \t<button type=\"button\" class=\"btn btn-default pull-right m-r-10\" @click=\"setScreen(1)\">Back</button>\n\t\t    </form>\n\t\t</div>\n\n\t</div>\n\n</template>\n\n<script>\n\n    export default {\n\n    \tprops: ['groupid'],\n\n    \tdata() {\n    \t\treturn {\n    \t\t\tscreen: null,\n\n    \t\t\tservices: [],\n    \t\t\tpackages: [],\n\n    \t\t\ttransferForm: new Form({\n    \t\t\t\tgroupId: '',\n    \t\t\t\tmemberId: '',\n\t\t\t\t\tservices: [],\n\t\t\t\t\tpackages:[],\n\t\t\t\t\toption: ''\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n    \t\t}\n    \t},\n\n    \tmethods: {\n\n    \t\tsetScreen(screen) {\n    \t\t\tthis.screen = screen;\n    \t\t},\n\n    \t\tsetMemberId(id) {\n    \t\t\tthis.transferForm.memberId = id;\n\n    \t\t\tthis.setScreen(1);\n    \t\t},\n\n    \t\tgetDetails(option, screen) {\n    \t\t\t// option = client-to-group | group-to-client\n    \t\t\tthis.transferForm.option = option;\n\n    \t\t\tvar vm = this;\n\n\t\t\t\tfunction getServices() {\n\t                return axios.get('/visa/client/'+ vm.transferForm.memberId +'/services', {\n    \t\t\t\t\tparams: {\n    \t\t\t\t\t\toption: option,\n    \t\t\t\t\t\tgroupId: vm.groupid\n    \t\t\t\t\t}\n    \t\t\t\t});\n\t            }\n\t            function getPackages() {\n\t                // axios\n\t                return axios.get('/visa/client/'+ vm.transferForm.memberId +'/packages', {\n    \t\t\t\t\tparams: {\n    \t\t\t\t\t\toption: option,\n    \t\t\t\t\t\tgroupId: vm.groupid\n    \t\t\t\t\t}\n    \t\t\t\t});\n\t            }\n\n\t            axios.all([\n\t                getServices(),\n\t                getPackages()\n\t            ]).then(axios.spread(\n\t                (\n\t                services,\n\t                packages\n\t                ) => {\n\t                    \n\t                this.services = services.data;\n\t                this.packages = packages.data;\n\t            }))\n\t            .catch(error => console.log(error));\n\n    \t\t\tthis.setScreen(screen);\n    \t\t},\n\n    \t\tcallDataTable() {\n\t\t\t\t$('.transfer-datatable').DataTable().destroy();\n\n\t\t\t\tsetTimeout(function() {\n\t\t\t\t\t$('.transfer-datatable').DataTable({\n\t\t\t            pageLength: 10,\n\t\t\t            responsive: true,\n\t\t\t            dom: '<\"top\"lf>rt<\"bottom\"ip><\"clear\">'\n\t\t\t        });\n\t\t\t\t}, 1000);\n\t\t\t},\n\n\t\t\ttoggleAllCheckbox(e) {\n\t\t\t\tif(e.target.checked == true) {\n\t\t\t\t\tthis.transferForm.services = [];\n\t\t\t\t\tthis.services.forEach((s, index) => {\n\t\t\t\t\t\tthis.transferForm.services.push(s.id);\n\t\t\t\t\t});\n\t\t\t\t} else {\n\t\t\t\t\tthis.transferForm.services = [];\n\t\t\t\t}\n\t\t\t},\n\n\t\t\tresetTransferForm() {\n\t\t\t\tthis.services = [];\n\t\t\t\tthis.packages = [];\n\t\t\t},\n\n    \t\ttransfer() {\n    \t\t\tif(this.transferForm.services.length == 0) {\n        \t\t\ttoastr.error('Please select at least one service');\n\t\t\t\t\treturn false;\n        \t\t} else {\n\n        \t\t\tthis.transferForm.packages = [];\n\n        \t\t\tthis.transferForm.services.forEach((s, index) => {\n\t\t        \t\tlet value = $('select.transfer-package-select-' + s).val();\n\t\t        \t\t\t\n\t\t        \t\tthis.transferForm.packages.push(value);\n\t\t       \t\t});\n\n\t    \t\t\tthis.transferForm.submit('patch', '/visa/client/' + this.groupid + '/transfer')\n\t\t            .then(response => {\n\t\t                if(response.success) {\n\t\t                \tthis.$parent.$parent.fetchDetails();\n\t\t                \tthis.$parent.$parent.fetchMembers();\n\n\t\t                \tthis.$parent.$parent.$refs.addnewserviceref.fetchGroupPackagesServicesList();\n\n\t\t                \tthis.resetTransferForm();\n\n\t\t                \t$('#transfer-modal').modal('hide');\n\n\t\t                \ttoastr.success(response.message);\n\t\t                }\n\t\t            })\n\t\t            .catch(error => console.log(error));\n\n        \t\t}\n    \t\t}\n\n    \t},\n\n    \tcreated() {\n    \t\tthis.transferForm.groupId = this.groupid;\n    \t}\n\n    }\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 312:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-c430dada] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-c430dada] {\n\tmargin-right: 10px;\n}\n.spacer-10[data-v-c430dada] {\n\tclear: both;\n\theight: 10px;\n}\n.cursor[data-v-c430dada] {\n\tcursor: pointer;\n}\n.scrollable[data-v-c430dada] {\n\tmax-height: 450px;\n\toverflow-y: auto;\n}\n.child-row-header[data-v-c430dada] {\n\tcolor: #3c8dbc;\n}\n", "", {"version":3,"sources":["ClientServices.vue?74f4f77b"],"names":[],"mappings":";AAkPA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA;AACA;CACA,YAAA;CACA,aAAA;CACA;AACA;CACA,gBAAA;CACA;AACA;CACA,kBAAA;CACA,iBAAA;CACA;AACA;CACA,eAAA;CACA","file":"ClientServices.vue","sourcesContent":["<template>\n\t\n\t<div>\n\t\t\n\t\t<div v-show=\"screen == 1\">\n\t\t\t<form class=\"form-horizontal\">\n\t\t\t\t<table class=\"table table-bordered table-striped table-hover membersDatatable\">\n\t\t\t        <thead>\n\t\t\t            <tr>\n\t\t\t            \t<th style=\"width:40%;\">CLIENT ID</th>\n\t\t\t                <th style=\"width:40%;\">NAME</th>\n\t\t\t                <th style=\"width:20%;\">ACTION</th>\n\t\t\t            </tr>\n\t\t\t        </thead>\n\t\t\t        <tbody>\n\t\t\t        \t<tr v-for=\"member in members\">\n\t\t\t        \t\t<td>{{ member.client.id }}</td>\n\t\t\t        \t\t<td>{{ member.client.first_name + ' ' + member.client.last_name }}</td>\n\t\t\t        \t\t<td class=\"text-center\">\n\t\t\t        \t\t\t<input type=\"checkbox\" name=\"clientsId[]\" :value=\"member.client.id\">\n\t\t\t        \t\t</td>\n\t\t\t        \t</tr>\n\t\t\t        </tbody>\n\t\t\t    </table>\n\n\t\t\t    <div class=\"spacer-10\"></div>\n\n\t\t\t    <button type=\"button\" class=\"btn btn-primary pull-right\" @click=\"gotoScreen(2)\">Continue</button>\n\t\t\t</form>\n\t\t</div> <!-- screen 1 -->\n\n\t\t<div v-show=\"screen == 2\">\n\n\t\t\t<form class=\"form-horizontal\">\n\n\t\t\t\t<div class=\"scrollable\">\n\t\t\t\t\t<table class=\"table table-bordered table-striped table-hover\">\n\t\t\t\t        <thead>\n\t\t\t\t            <tr>\n\t\t\t\t            \t<th style=\"width:45%;\">CLIENT ID</th>\n\t\t\t\t                <th style=\"width:45%;\">NAME</th>\n\t\t\t\t                <th style=\"width:10%;\" class=\"text-center\">ACTION</th>\n\t\t\t\t            </tr>\n\t\t\t\t        </thead>\n\t\t\t\t        <tbody v-for=\"(member, index) in members\" v-if=\"selectedClientsId.indexOf(member.client.id) != -1\">\n\t\t\t\t        \t\n\t\t\t\t        \t<tr @click=\"toggleChildRow($event, index)\">\n\t\t\t\t\t\t        <td style=\"width:45%;\">{{ member.client.id }}</td>\n\t\t\t\t\t\t        <td style=\"width:45%;\">{{ member.client.first_name + ' ' + member.client.last_name }}</td>\n\t\t\t\t\t\t        <td style=\"width:10%;\" class=\"text-center\">\n\t\t\t\t\t\t           \t<i id=\"fa-1000\" class=\"fa fa-arrow-right cursor\" :class=\"'arrow-' + index\"></i>\n\t\t\t\t\t\t        </td>\n\t\t\t\t\t\t    </tr>\n\n\t\t\t\t\t\t    <tr :id=\"'child-row-'+index\" class=\"hide\">\n\t\t\t\t\t\t    \t<td colspan=\"3\">\n\t\t\t\t\t\t    \t\t<table class=\"table table-bordered table-striped table-hover\">\n\t\t\t\t\t\t\t\t        <thead>\n\t\t\t\t\t\t\t\t            <tr class=\"child-row-header\">\n\t\t\t\t\t\t\t\t            \t<th>Date</th>\n\t\t\t\t\t\t\t\t                <th>Package</th>\n\t\t\t\t\t\t\t\t                <th>Details</th>\n\t\t\t\t\t\t\t\t                <th>Cost</th>\n\t\t\t\t\t\t\t\t                <th>Charge</th>\n\t\t\t\t\t\t\t\t                <th>Tip</th>\n\t\t\t\t\t\t\t\t                <th>Discount</th>\n\t\t\t\t\t\t\t\t                <th>Status</th>\n\t\t\t\t\t\t\t\t                <th class=\"text-center\">\n\t\t\t\t\t\t\t\t                \t\n\t\t\t\t\t\t\t\t                </th>\n\t\t\t\t\t\t\t\t            </tr>\n\t\t\t\t\t\t\t\t        </thead>\n\t\t\t\t\t\t\t\t        <tbody>\n\t\t\t\t\t\t\t\t        \t<tr v-for=\"service in member.services\" v-if=\"service.group_id == groupid && service.active == 1 && service.status != 'Complete'\">\n\t\t\t\t\t\t\t\t        \t\t<td>{{ service.service_date }}</td>\n\t\t\t\t\t\t\t\t        \t\t<td>{{ service.tracking }}</td>\n\t\t\t\t\t\t\t\t        \t\t<td>{{ service.detail }}</td>\n\t\t\t\t\t\t\t\t        \t\t<td>{{ service.cost | currency }}</td>\n\t\t\t\t\t\t\t\t        \t\t<td>{{ service.charge | currency }}</td>\n\t\t\t\t\t\t\t\t        \t\t<td>{{ service.tip | currency }}</td>\n\t\t\t\t\t\t\t\t        \t\t<td>{{ service.discount_amount | currency }}</td>\n\t\t\t\t\t\t\t\t        \t\t<td>{{ service.status }}</td>\n\t\t\t\t\t\t\t\t        \t\t<td class=\"text-center\">\n\t\t\t\t\t\t\t\t        \t\t\t<input type=\"checkbox\" name=\"clientServicesId[]\" :value=\"service.id\">\n\t\t\t\t\t\t\t\t        \t\t</td>\n\t\t\t\t\t\t\t\t        \t</tr>\n\t\t\t\t\t\t\t\t        </tbody>\n\t\t\t\t\t\t\t\t    </table>\n\t\t\t\t\t\t    \t</td>\n\t\t\t\t\t\t    </tr>\n\n\t\t\t\t        </tbody>\n\t\t\t\t    </table>\n\t\t\t    </div>\n\n\t\t\t\t<div class=\"spacer-10\"></div>\n\n\t\t\t\t<button type=\"button\" class=\"btn btn-primary pull-right\" @click=\"setSelectedClientServicesAndTrackingsId\">Continue</button>\n\t\t\t\t<button type=\"button\" class=\"btn btn-default pull-right m-r-10\" @click=\"gotoScreen(1)\">Back</button>\n\t\t\t</form>\n\t\t</div> <!-- screen 2 -->\n\n\t</div>\n\n</template>\n\n<script>\n\texport default {\n\n\t\tprops: ['members', 'groupid'],\n\n        data() {\n\n        \treturn {\n        \t\tmembersDatatable: null,\n\n        \t\tscreen: 1,\n\n        \t\tselectedClientsId: [],\n        \t\tselectedClientServicesId: [],\n        \t\tselectedTrackings: []\n        \t}\n\n        },\n\n        methods: {\n\n        \tgotoScreen(screen) {\n        \t\tthis.setSelectedClientsId();\n\n        \t\tif(screen == 2 && this.selectedClientsId.length == 0) {\n        \t\t\ttoastr.error('Please select client/s.');\n        \t\t} else {\n        \t\t\tthis.screen = screen;\n        \t\t}\n        \t},\n\n        \tsetSelectedClientsId() {\n        \t\tlet selectedClientsId = [];\n\n\t\t\t    this.membersDatatable.$(\"input[name='clientsId[]']:checked\").each(function(e) {\n\t\t\t    \tselectedClientsId.push(parseInt($(this).val()));\n\t\t\t    });\n\n\t\t\t    this.selectedClientsId = selectedClientsId;\n        \t},\n\n        \tsetSelectedClientServicesAndTrackingsId() {\n        \t\tlet selectedClientServicesId = [];\n\n\t\t\t    $(\"input[name='clientServicesId[]']:checked\").each(function(e) {\n\t\t\t    \tselectedClientServicesId.push(parseInt($(this).val()));\n\t\t\t    });\n\n\t\t\t    if(selectedClientServicesId.length == 0) {\n\t\t\t    \ttoastr.error('Please select services/s.');\n\t\t\t    } else {\n\t\t\t    \taxiosAPIv1.get('/visa/group/' + this.groupid + '/client-services', {\n\t\t\t\t\t    params: {\n\t\t\t\t\t      \tclientServicesId: selectedClientServicesId\n\t\t\t\t\t    }\n\t\t\t\t\t})\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tthis.selectedClientServicesId = response.data;\n\n\t\t\t\t\t\tlet selectedTrackings = [];\n\n\t\t        \t\tthis.selectedClientServicesId.forEach(clientService => {\n\t\t        \t\t\tselectedTrackings.push(clientService.tracking);\n\t\t        \t\t});\n\n\t\t        \t\tthis.selectedTrackings = selectedTrackings;\n\n\t\t        \t\tthis.showMultipleQuickReportModal();\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => console.log(error));\n\t\t\t    }\n        \t},\n\n        \tshowMultipleQuickReportModal() {\n        \t\tthis.$parent.$parent.quickReportClientsId = this.selectedClientsId;\n\t\t\t\tthis.$parent.$parent.quickReportClientServicesId = this.selectedClientServicesId;\n\t\t\t\tthis.$parent.$parent.quickReportTrackings = this.selectedTrackings;\n\t\t\t\t\t\n\t\t\t\tthis.$parent.$parent.fetchDocs(this.selectedClientServicesId);\n\n\t\t\t\t$('#client-services-modal').modal('hide');\n\n\t\t\t\tsetTimeout(() => {\n\t\t\t\t\t$('#add-quick-report-modal').modal('show');\n\t\t\t\t}, 1000);\n        \t},\n\n        \tuncheckSelectedClientsId() {\n        \t\tthis.membersDatatable.$(\"input[name='clientsId[]']\").prop('checked', false);\n        \t},\n\n        \tuncheckSelectedClientServicesId() {\n        \t\tthis.membersDatatable.$(\"input[name='clientServicesId[]']\").prop('checked', false);\n        \t},\n\n        \tcreateDatatable(className) {\n        \t\tthis.membersDatatable = $('table.'+className).DataTable().destroy();\n\n\t\t\t\tsetTimeout(function() {\n\t\t\t\t\tthis.membersDatatable = $('table.'+className).DataTable({\n\t\t\t\t        pageLength: 10,\n\t\t\t\t        responsive: true,\n\t\t\t\t        \"aaSorting\": [],\n\t\t\t\t        dom: '<\"top\"lf>rt<\"bottom\"ip><\"clear\">'\n\t\t\t\t    });\n\t\t\t\t}.bind(this), 1000);\n\t\t\t},\n\n\t\t\ttoggleChildRow(event, index) {\n\t\t\t\t$('#child-row-'+index).toggleClass('hide');\n\t\t\t\t$('.arrow-'+index).toggleClass('fa-arrow-right fa-arrow-down');\n\t\t\t}\n\n        },\n\n        mounted() {\n\n        \t$('#client-services-modal').on('hidden.bs.modal', function (e) {\n\t\t\t\t\n\t\t\t\tthis.screen = 1;\n\t\t\t\t\n\t\t\t\tthis.selectedClientsId = [];\n\t\t\t\tthis.selectedClientServicesId = [];\n\t\t\t\tthis.selectedTrackings = [];\n\n\t\t\t\tthis.uncheckSelectedClientsId();\n\t\t\t\tthis.uncheckSelectedClientServicesId();\n\n\t\t\t}.bind(this));\n\n        }\n\n\t}\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n\t.spacer-10 {\n\t\tclear: both;\n\t\theight: 10px;\n\t}\n\t.cursor {\n\t\tcursor: pointer;\n\t}\n\t.scrollable {\n\t\tmax-height: 450px;\n\t\toverflow-y: auto;\n\t}\n\t.child-row-header {\n\t\tcolor: #3c8dbc;\n\t}\n</style>"]}]);

/***/ }),

/***/ 32:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.log.year != null) ? _c('div', {
    staticClass: "row timeline-movement timeline-movement-top no-dashed no-side-margin"
  }, [_c('div', {
    staticClass: "timeline-badge timeline-filter-movement"
  }, [_c('a', {
    attrs: {
      "href": "#"
    }
  }, [_c('span', [_vm._v(_vm._s(_vm.log.year))])])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "row timeline-movement  no-side-margin"
  }, [(_vm.log.month != null) ? _c('div', {
    staticClass: "timeline-badge"
  }, [_c('span', {
    staticClass: "timeline-balloon-date-day"
  }, [_vm._v(_vm._s(_vm.log.day))]), _vm._v(" "), _c('span', {
    staticClass: "timeline-balloon-date-month"
  }, [_vm._v(_vm._s(_vm.log.month))])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "col-sm-12  timeline-item"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-offset-1 col-sm-11"
  }, [_c('div', {
    staticClass: "timeline-panel debits"
  }, [_c('ul', {
    staticClass: "timeline-panel-ul"
  }, [_c('li', [_c('span', {
    staticClass: "importo",
    domProps: {
      "innerHTML": _vm._s(_vm.log.data.title)
    }
  })]), _vm._v(" "), _c('li', [_c('span', {
    staticClass: "causale"
  }, [_vm._v(_vm._s(_vm.log.data.processor))])]), _vm._v(" "), _c('li', [_c('p', [_c('small', {
    staticClass: "text-muted"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-time"
  }), _vm._v(_vm._s(_vm.log.data.date))])])])])])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-72b755b5", module.exports)
  }
}

/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(440)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(241),
  /* template */
  __webpack_require__(395),
  /* scopeId */
  "data-v-5199a8a8",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/AddFund.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AddFund.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5199a8a8", Component.options)
  } else {
    hotAPI.reload("data-v-5199a8a8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(445)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(242),
  /* template */
  __webpack_require__(408),
  /* scopeId */
  "data-v-73df72fa",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/AddNewMember.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AddNewMember.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-73df72fa", Component.options)
  } else {
    hotAPI.reload("data-v-73df72fa", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 336:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(436)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(243),
  /* template */
  __webpack_require__(385),
  /* scopeId */
  "data-v-3928fa9c",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/AddNewService.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AddNewService.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3928fa9c", Component.options)
  } else {
    hotAPI.reload("data-v-3928fa9c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(450)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(247),
  /* template */
  __webpack_require__(419),
  /* scopeId */
  "data-v-c430dada",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/ClientServices.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ClientServices.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c430dada", Component.options)
  } else {
    hotAPI.reload("data-v-c430dada", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(439)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(249),
  /* template */
  __webpack_require__(388),
  /* scopeId */
  "data-v-3ca94bef",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/DeleteMember.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DeleteMember.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ca94bef", Component.options)
  } else {
    hotAPI.reload("data-v-3ca94bef", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(435)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(251),
  /* template */
  __webpack_require__(383),
  /* scopeId */
  "data-v-27ab8230",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/EditAddress.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] EditAddress.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-27ab8230", Component.options)
  } else {
    hotAPI.reload("data-v-27ab8230", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(431)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(252),
  /* template */
  __webpack_require__(378),
  /* scopeId */
  "data-v-06d26471",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/EditService.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] EditService.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-06d26471", Component.options)
  } else {
    hotAPI.reload("data-v-06d26471", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(433)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(254),
  /* template */
  __webpack_require__(380),
  /* scopeId */
  "data-v-168f2e0e",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/GroupMemberType.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] GroupMemberType.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-168f2e0e", Component.options)
  } else {
    hotAPI.reload("data-v-168f2e0e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 348:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(449)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(255),
  /* template */
  __webpack_require__(418),
  /* scopeId */
  "data-v-bbd2ec96",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/Transfer.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Transfer.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bbd2ec96", Component.options)
  } else {
    hotAPI.reload("data-v-bbd2ec96", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 378:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 1),
      expression: "screen == 1"
    }]
  }, [_c('div', {
    staticClass: "table-responsive"
  }, [_c('table', {
    staticClass: "table table-striped table-bordered table-hover clients-datatable"
  }, [_c('thead', [_c('tr', [_c('th', {
    staticClass: "text-center"
  }, [_c('input', {
    attrs: {
      "type": "checkbox"
    },
    on: {
      "click": _vm.toggleAllCheckbox
    }
  })]), _vm._v(" "), _c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("Client ID")]), _vm._v(" "), _c('th', [_vm._v("Package")]), _vm._v(" "), _c('th', [_vm._v("Date Recorded")]), _vm._v(" "), _c('th', [_vm._v("Details")]), _vm._v(" "), _c('th', [_vm._v("Status")])])]), _vm._v(" "), _c('tbody', _vm._l((_vm.clientServices), function(clientService) {
    return _c('tr', [_c('td', {
      staticClass: "text-center"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.editServiceForm.clientServicesId),
        expression: "editServiceForm.clientServicesId"
      }],
      attrs: {
        "type": "checkbox"
      },
      domProps: {
        "value": clientService.id,
        "checked": Array.isArray(_vm.editServiceForm.clientServicesId) ? _vm._i(_vm.editServiceForm.clientServicesId, clientService.id) > -1 : (_vm.editServiceForm.clientServicesId)
      },
      on: {
        "change": function($event) {
          var $$a = _vm.editServiceForm.clientServicesId,
            $$el = $event.target,
            $$c = $$el.checked ? (true) : (false);
          if (Array.isArray($$a)) {
            var $$v = clientService.id,
              $$i = _vm._i($$a, $$v);
            if ($$el.checked) {
              $$i < 0 && (_vm.$set(_vm.editServiceForm, "clientServicesId", $$a.concat([$$v])))
            } else {
              $$i > -1 && (_vm.$set(_vm.editServiceForm, "clientServicesId", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
            }
          } else {
            _vm.$set(_vm.editServiceForm, "clientServicesId", $$c)
          }
        }
      }
    })]), _vm._v(" "), _c('td', [_vm._v(_vm._s(clientService.user.full_name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(clientService.user.id))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(clientService.tracking))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(clientService.service_date))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(clientService.detail))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(clientService.status))])])
  }), 0)])]), _vm._v(" "), _c('a', {
    staticClass: "btn btn-primary pull-right",
    on: {
      "click": function($event) {
        _vm.changeScreen(2)
      }
    }
  }, [_vm._v("Continue")]), _vm._v(" "), _c('a', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")]), _vm._v(" "), _c('br')]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 2),
      expression: "screen == 2"
    }]
  }, [_c('form', {
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.editService($event)
      }
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editServiceForm.errors.has('additional_charge')
    }
  }, [_c('label', {
    attrs: {
      "for": "additonal_charge"
    }
  }, [_vm._v("Additional Charge")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editServiceForm.additional_charge),
      expression: "editServiceForm.additional_charge"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "additional_charge",
      "name": "additional_charge"
    },
    domProps: {
      "value": (_vm.editServiceForm.additional_charge)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editServiceForm, "additional_charge", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.editServiceForm.errors.has('additional_charge')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editServiceForm.errors.get('additional_charge'))
    }
  }) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editServiceForm.errors.has('cost')
    }
  }, [_c('label', {
    attrs: {
      "for": "cost"
    }
  }, [_vm._v("Cost")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editServiceForm.cost),
      expression: "editServiceForm.cost"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "cost",
      "name": "cost"
    },
    domProps: {
      "value": (_vm.editServiceForm.cost)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editServiceForm, "cost", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.editServiceForm.errors.has('cost')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editServiceForm.errors.get('cost'))
    }
  }) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editServiceForm.errors.has('discount')
    }
  }, [_c('label', {
    attrs: {
      "for": "cost"
    }
  }, [_vm._v("Discount")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editServiceForm.discount),
      expression: "editServiceForm.discount"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "discount",
      "name": "discount"
    },
    domProps: {
      "value": (_vm.editServiceForm.discount)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editServiceForm, "discount", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.editServiceForm.errors.has('discount')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editServiceForm.errors.get('discount'))
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    attrs: {
      "for": "status"
    }
  }, [_vm._v("Status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editServiceForm.status),
      expression: "editServiceForm.status"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "status",
      "name": "status"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.editServiceForm, "status", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "complete"
    }
  }, [_vm._v("Complete")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "on process"
    }
  }, [_vm._v("On Process")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "pending"
    }
  }, [_vm._v("Pending")])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    attrs: {
      "for": "note"
    }
  }, [_vm._v("Note")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editServiceForm.note),
      expression: "editServiceForm.note"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "note",
      "name": "note",
      "placeholder": "for internal use only, only employees can see."
    },
    domProps: {
      "value": (_vm.editServiceForm.note)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editServiceForm, "note", $event.target.value)
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    staticStyle: {
      "margin-top": "22px"
    }
  }, [_c('label', {
    attrs: {
      "for": "active"
    }
  }), _vm._v(" "), _c('label', {
    staticClass: "radio-inline"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editServiceForm.active),
      expression: "editServiceForm.active"
    }],
    attrs: {
      "type": "radio",
      "id": "active",
      "name": "active",
      "value": "1"
    },
    domProps: {
      "checked": _vm._q(_vm.editServiceForm.active, "1")
    },
    on: {
      "change": function($event) {
        _vm.$set(_vm.editServiceForm, "active", "1")
      }
    }
  }), _vm._v(" Enable\n\t\t\t\t\t\t")]), _vm._v(" "), _c('label', {
    staticClass: "radio-inline"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editServiceForm.active),
      expression: "editServiceForm.active"
    }],
    attrs: {
      "type": "radio",
      "id": "active",
      "name": "active",
      "value": "0"
    },
    domProps: {
      "checked": _vm._q(_vm.editServiceForm.active, "0")
    },
    on: {
      "change": function($event) {
        _vm.$set(_vm.editServiceForm, "active", "0")
      }
    }
  }), _vm._v(" Disable\n\t\t\t\t\t\t")])]), _vm._v(" "), (_vm.editServiceForm.discount > 0) ? _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editServiceForm.errors.has('reason')
    },
    staticStyle: {
      "margin-top": "25px"
    }
  }, [_c('label', {
    attrs: {
      "for": "cost"
    }
  }, [_vm._v("Reason")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editServiceForm.reason),
      expression: "editServiceForm.reason"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "reason",
      "name": "reason"
    },
    domProps: {
      "value": (_vm.editServiceForm.reason)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editServiceForm, "reason", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.editServiceForm.errors.has('reason')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editServiceForm.errors.get('reason'))
    }
  }) : _vm._e()]) : _vm._e()])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('a', {
    staticClass: "btn btn-default pull-right m-r-10",
    on: {
      "click": function($event) {
        _vm.changeScreen(1)
      }
    }
  }, [_vm._v("Back")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-06d26471", module.exports)
  }
}

/***/ }),

/***/ 380:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-block",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.changeMemberType('leader')
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-star fa-fw"
  }), _vm._v(" "), _c('i', {
    staticClass: "fa fa-star fa-fw"
  }), _vm._v(" Make Group Leader\n\t\t\t\t")])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('button', {
    staticClass: "btn btn-info btn-block",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.changeMemberType('member')
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-star-o fa-fw"
  }), _vm._v(" Make Group Member")])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-168f2e0e", module.exports)
  }
}

/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.editAddress($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editAddressForm.errors.has('address')
    }
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-sm-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editAddressForm.name),
      expression: "editAddressForm.name"
    }],
    staticClass: "form-control m-top-0",
    attrs: {
      "type": "text",
      "name": "name"
    },
    domProps: {
      "value": (_vm.editAddressForm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editAddressForm, "name", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.editAddressForm.errors.has('name')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editAddressForm.errors.get('name'))
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editAddressForm.errors.has('address')
    }
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-sm-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editAddressForm.number),
      expression: "editAddressForm.number"
    }],
    staticClass: "form-control m-top-0",
    attrs: {
      "type": "text",
      "name": "number"
    },
    domProps: {
      "value": (_vm.editAddressForm.number)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editAddressForm, "number", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.editAddressForm.errors.has('number')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editAddressForm.errors.get('number'))
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editAddressForm.errors.has('address')
    }
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-sm-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editAddressForm.address),
      expression: "editAddressForm.address"
    }],
    staticClass: "form-control m-top-0",
    attrs: {
      "type": "text",
      "name": "address"
    },
    domProps: {
      "value": (_vm.editAddressForm.address)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editAddressForm, "address", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.editAddressForm.errors.has('address')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editAddressForm.errors.get('address'))
    }
  }) : _vm._e()])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Edit")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-sm-2 control-label"
  }, [_vm._v("\n            Name\n            "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-sm-2 control-label"
  }, [_vm._v("\n            Number\n            "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-sm-2 control-label"
  }, [_vm._v("\n            Address\n            "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-27ab8230", module.exports)
  }
}

/***/ }),

/***/ 385:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addNewService($event)
      }
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 1),
      expression: "screen == 1"
    }]
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addNewServiceForm.errors.has('services')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t            Service:\n\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-service",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "data-placeholder": "Select Service",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.servicesList), function(service) {
    return _c('option', {
      domProps: {
        "value": service.id
      }
    }, [_vm._v("\n\t\t                \t" + _vm._s((service.detail).trim()) + "\n\t\t                ")])
  }), 0), _vm._v(" "), (_vm.addNewServiceForm.errors.has('services')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addNewServiceForm.errors.get('services'))
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "spacer-10"
  }), _vm._v(" "), _vm._m(0)])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t            Note:\n\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addNewServiceForm.note),
      expression: "addNewServiceForm.note"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "note",
      "placeholder": "for internal use only, only employees can see."
    },
    domProps: {
      "value": (_vm.addNewServiceForm.note)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addNewServiceForm, "note", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.addNewServiceForm.services.length <= 1),
      expression: "addNewServiceForm.services.length <= 1"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t            Cost:\n\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "cost",
      "readonly": ""
    },
    domProps: {
      "value": _vm.cost
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addNewServiceForm.errors.has('discount')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t            Discount:\n\t\t\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-4"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addNewServiceForm.discount),
      expression: "addNewServiceForm.discount"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "discount"
    },
    domProps: {
      "value": (_vm.addNewServiceForm.discount)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addNewServiceForm, "discount", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addNewServiceForm.errors.has('discount')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addNewServiceForm.errors.get('discount'))
    }
  }) : _vm._e()]), _vm._v(" "), (_vm.addNewServiceForm.discount != '') ? _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t            Reason:\n\t\t\t        ")]) : _vm._e(), _vm._v(" "), (_vm.addNewServiceForm.discount != '') ? _c('div', {
    staticClass: "col-md-4"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addNewServiceForm.reason),
      expression: "addNewServiceForm.reason"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "reason"
    },
    domProps: {
      "value": (_vm.addNewServiceForm.reason)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addNewServiceForm, "reason", $event.target.value)
      }
    }
  })]) : _vm._e()])]), _vm._v(" "), _c('a', {
    staticClass: "btn btn-primary pull-right",
    on: {
      "click": function($event) {
        _vm.changeScreen(2)
      }
    }
  }, [_vm._v("Continue")]), _vm._v(" "), _c('a', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 2),
      expression: "screen == 2"
    }]
  }, [_c('div', {
    staticClass: "table-responsive"
  }, [_c('table', {
    staticClass: "table table-striped table-bordered table-hover add-new-service-datatable"
  }, [_c('thead', [_c('tr', [_c('th', {
    staticClass: "text-center"
  }, [_c('input', {
    attrs: {
      "type": "checkbox"
    },
    on: {
      "click": _vm.toggleAllCheckbox
    }
  })]), _vm._v(" "), _c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("Client ID")]), _vm._v(" "), _c('th', [_vm._v("Packages")])])]), _vm._v(" "), _c('tbody', _vm._l((_vm.clients), function(data) {
    return _c('tr', [_c('td', {
      staticClass: "text-center"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.addNewServiceForm.clients),
        expression: "addNewServiceForm.clients"
      }],
      attrs: {
        "type": "checkbox"
      },
      domProps: {
        "value": data.client.id,
        "checked": Array.isArray(_vm.addNewServiceForm.clients) ? _vm._i(_vm.addNewServiceForm.clients, data.client.id) > -1 : (_vm.addNewServiceForm.clients)
      },
      on: {
        "change": function($event) {
          var $$a = _vm.addNewServiceForm.clients,
            $$el = $event.target,
            $$c = $$el.checked ? (true) : (false);
          if (Array.isArray($$a)) {
            var $$v = data.client.id,
              $$i = _vm._i($$a, $$v);
            if ($$el.checked) {
              $$i < 0 && (_vm.$set(_vm.addNewServiceForm, "clients", $$a.concat([$$v])))
            } else {
              $$i > -1 && (_vm.$set(_vm.addNewServiceForm, "clients", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
            }
          } else {
            _vm.$set(_vm.addNewServiceForm, "clients", $$c)
          }
        }
      }
    })]), _vm._v(" "), _c('td', [_vm._v(_vm._s(data.client.full_name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(data.client.id))]), _vm._v(" "), _c('td', [_c('select', {
      staticClass: "form-control",
      class: 'package-select-' + data.client.id,
      attrs: {
        "disabled": _vm.addNewServiceForm.clients.indexOf(data.client.id) == -1
      }
    }, [_c('option', {
      attrs: {
        "value": "Generate new package"
      }
    }, [_vm._v("Generate new package")]), _vm._v(" "), _vm._l((data.client.packages), function(package) {
      return (package.status != 4) ? _c('option', {
        domProps: {
          "value": package.tracking
        }
      }, [_vm._v("\n                    \t\t\t\t\t" + _vm._s(package.tracking) + " \n                    \t\t\t\t\t(" + _vm._s(_vm._f("commonDate")(package.log_date)) + ")\n                    \t\t\t\t")]) : _vm._e()
    })], 2)])])
  }), 0)])]), _vm._v(" "), _c('a', {
    staticClass: "btn btn-primary pull-right",
    on: {
      "click": function($event) {
        _vm.changeScreen(3)
      }
    }
  }, [_vm._v("Continue")]), _vm._v(" "), _c('a', {
    staticClass: "btn btn-default pull-right m-r-10",
    on: {
      "click": function($event) {
        _vm.changeScreen(1)
      }
    }
  }, [_vm._v("Back")])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 3),
      expression: "screen == 3"
    }]
  }, [_c('div', {
    staticClass: "ios-scroll"
  }, [_c('label', {
    staticClass: "checkbox-inline"
  }, [_c('input', {
    staticClass: "checkbox-all",
    attrs: {
      "type": "checkbox"
    },
    on: {
      "click": _vm.checkAll
    }
  }), _vm._v(" Check all required documents\n    \t\t\t")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _vm._l((_vm.addNewServiceForm.clients), function(clientId, index1) {
    return _c('div', {
      staticClass: "panel panel-default"
    }, [_c('div', {
      staticClass: "panel-heading"
    }, [_c('strong', [_vm._v(_vm._s(_vm.getClientName(clientId)))])]), _vm._v(" "), _c('div', {
      staticClass: "panel-body"
    }, _vm._l((_vm.addNewServiceForm.services), function(serviceId, index2) {
      return _c('div', {
        staticClass: "panel panel-default"
      }, [_c('div', {
        staticClass: "panel-heading"
      }, [_vm._v("\n\t\t\t                    " + _vm._s(_vm.getServiceName(serviceId)) + "\n\t\t\t                    "), _c('label', {
        staticClass: "checkbox-inline pull-right"
      }, [_c('input', {
        class: 'checkbox-' + index1 + '-' + index2,
        attrs: {
          "type": "checkbox",
          "data-chosen-class": 'chosen-select-for-required-docs-' + index1 + '-' + index2
        },
        on: {
          "click": _vm.checkAll
        }
      }), _vm._v(" Check all required documents\n\t\t\t                    ")])]), _vm._v(" "), _c('div', {
        staticClass: "panel-body"
      }, [_c('div', {
        staticClass: "form-group"
      }, [_c('label', {
        staticClass: "col-md-2 control-label"
      }, [_vm._v("\n\t\t\t\t\t\t\t\t       \tRequired Documents:\n\t\t\t\t\t\t\t\t    ")]), _vm._v(" "), _c('div', {
        staticClass: "col-md-10"
      }, [_c('select', {
        staticClass: "chosen-select-for-required-docs",
        class: 'chosen-select-for-required-docs-' + index1 + '-' + index2,
        staticStyle: {
          "width": "350px"
        },
        attrs: {
          "data-placeholder": "Select Docs",
          "multiple": "",
          "tabindex": "4"
        }
      }, _vm._l((_vm.requiredDocs[index2]), function(doc) {
        return _c('option', {
          domProps: {
            "value": doc.id
          }
        }, [_vm._v("\n\t\t\t\t\t\t\t                \t" + _vm._s(doc.title) + "\n\t\t\t\t\t\t\t                ")])
      }), 0)])]), _vm._v(" "), _c('div', {
        staticClass: "form-group"
      }, [_c('label', {
        staticClass: "col-md-2 control-label"
      }, [_vm._v("\n\t\t\t\t\t\t\t\t       \tOptional Documents:\n\t\t\t\t\t\t\t\t    ")]), _vm._v(" "), _c('div', {
        staticClass: "col-md-10"
      }, [_c('select', {
        staticClass: "chosen-select-for-optional-docs",
        class: 'chosen-select-for-optional-docs-' + index1 + '-' + index2,
        staticStyle: {
          "width": "350px"
        },
        attrs: {
          "data-placeholder": "Select Docs",
          "multiple": "",
          "tabindex": "4"
        }
      }, _vm._l((_vm.optionalDocs[index2]), function(doc) {
        return _c('option', {
          domProps: {
            "value": doc.id
          }
        }, [_vm._v("\n\t\t\t\t\t\t\t                \t" + _vm._s(doc.title) + "\n\t\t\t\t\t\t\t                ")])
      }), 0)])])])])
    }), 0)])
  })], 2), _vm._v(" "), (!_vm.loading) ? _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Save")]) : _vm._e(), _vm._v(" "), (_vm.loading) ? _c('button', {
    staticClass: "btn btn-default pull-right",
    attrs: {
      "type": "button"
    }
  }, [_vm._m(1)]) : _vm._e(), _vm._v(" "), _c('a', {
    staticClass: "btn btn-default pull-right m-r-10",
    on: {
      "click": function($event) {
        _vm.changeScreen(2)
      }
    }
  }, [_vm._v("Back")])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("\n\t\t            \tIf you want to view all services at once, please \n\t\t            \t"), _c('a', {
    attrs: {
      "href": "/visa/service/view-all",
      "target": "_blank"
    }
  }, [_vm._v("click here")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "sk-spinner sk-spinner-wave",
    staticStyle: {
      "width": "40px",
      "height": "20px"
    }
  }, [_c('div', {
    staticClass: "sk-rect1"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect2"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect3"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect4"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect5"
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3928fa9c", module.exports)
  }
}

/***/ }),

/***/ 388:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.deleteMember($event)
      }
    }
  }, [_c('p', [_vm._v("Are you sure you want to delete this member?")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Delete")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3ca94bef", module.exports)
  }
}

/***/ }),

/***/ 395:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addFunds($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Option:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addFundForm.option),
      expression: "addFundForm.option"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "option"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.addFundForm, "option", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "Deposit"
    }
  }, [_vm._v("Deposit")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "Payment"
    }
  }, [_vm._v("Payment")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "Discount"
    }
  }, [_vm._v("Discount")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "Refund"
    }
  }, [_vm._v("Refund")])])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addFundForm.errors.has('amount')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Amount:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addFundForm.amount),
      expression: "addFundForm.amount"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "amount"
    },
    domProps: {
      "value": (_vm.addFundForm.amount)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addFundForm, "amount", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addFundForm.errors.has('amount')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addFundForm.errors.get('amount'))
    }
  }) : _vm._e()])]), _vm._v(" "), (_vm.addFundForm.option == 'Discount' || _vm.addFundForm.option == 'Refund') ? _c('div', [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addFundForm.errors.has('reason')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Reason:\n\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addFundForm.reason),
      expression: "addFundForm.reason"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "reason"
    },
    domProps: {
      "value": (_vm.addFundForm.reason)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addFundForm, "reason", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addFundForm.errors.has('reason')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addFundForm.errors.get('reason'))
    }
  }) : _vm._e()])])]) : _vm._e(), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Add")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5199a8a8", module.exports)
  }
}

/***/ }),

/***/ 408:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addNewMember($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addNewMemberForm.errors.has('clientId')
    }
  }, [_c('label', {
    staticClass: "col-sm-2 control-label"
  }, [_vm._v("\n            Clients\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-clients",
    staticStyle: {
      "width": "100%"
    },
    attrs: {
      "data-placeholder": "Select clients",
      "multiple": ""
    }
  }, _vm._l((_vm.clients), function(client) {
    return _c('option', {
      domProps: {
        "value": client.id
      }
    }, [_vm._v("\n\t               \t" + _vm._s(client.name) + "\n\t            ")])
  }), 0)])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Add")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-73df72fa", module.exports)
  }
}

/***/ }),

/***/ 418:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 1),
      expression: "screen == 1"
    }]
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-block",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.getDetails('client-to-group', 2)
      }
    }
  }, [_vm._v("Client to Group")])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('button', {
    staticClass: "btn btn-info btn-block",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.getDetails('group-to-client', 2)
      }
    }
  }, [_vm._v("Group to Client")])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 2),
      expression: "screen == 2"
    }]
  }, [_c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.transfer($event)
      }
    }
  }, [_c('div', {
    staticClass: "table-responsive"
  }, [_c('table', {
    staticClass: "table table-striped table-bordered table-hover transfer-datatable"
  }, [_c('thead', [_c('tr', [_c('th', {
    staticClass: "text-center"
  }, [_c('input', {
    attrs: {
      "type": "checkbox"
    },
    on: {
      "click": _vm.toggleAllCheckbox
    }
  })]), _vm._v(" "), _c('th', [_vm._v("Detail")]), _vm._v(" "), _c('th', [_vm._v("Status")]), _vm._v(" "), _c('th', [_vm._v("Tracking")]), _vm._v(" "), _c('th', [_vm._v("Packages")])])]), _vm._v(" "), _c('tbody', _vm._l((_vm.services), function(service) {
    return _c('tr', [_c('td', {
      staticClass: "text-center"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.transferForm.services),
        expression: "transferForm.services"
      }],
      attrs: {
        "type": "checkbox"
      },
      domProps: {
        "value": service.id,
        "checked": Array.isArray(_vm.transferForm.services) ? _vm._i(_vm.transferForm.services, service.id) > -1 : (_vm.transferForm.services)
      },
      on: {
        "change": function($event) {
          var $$a = _vm.transferForm.services,
            $$el = $event.target,
            $$c = $$el.checked ? (true) : (false);
          if (Array.isArray($$a)) {
            var $$v = service.id,
              $$i = _vm._i($$a, $$v);
            if ($$el.checked) {
              $$i < 0 && (_vm.$set(_vm.transferForm, "services", $$a.concat([$$v])))
            } else {
              $$i > -1 && (_vm.$set(_vm.transferForm, "services", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
            }
          } else {
            _vm.$set(_vm.transferForm, "services", $$c)
          }
        }
      }
    })]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.detail))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.status))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.tracking))]), _vm._v(" "), _c('td', [_c('select', {
      staticClass: "form-control",
      class: 'transfer-package-select-' + service.id
    }, [_c('option', {
      attrs: {
        "value": "Generate new package"
      }
    }, [_vm._v("Generate new package")]), _vm._v(" "), _vm._l((_vm.packages), function(package) {
      return _c('option', {
        domProps: {
          "value": package.tracking
        }
      }, [_vm._v("\n                    \t\t\t\t\tPackage #" + _vm._s(package.tracking) + "\n                    \t\t\t\t\t(" + _vm._s(_vm._f("commonDate")(package.log_date)) + ")\n                    \t\t\t\t")])
    })], 2)])])
  }), 0)])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Complete Transfer")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.setScreen(1)
      }
    }
  }, [_vm._v("Back")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-bbd2ec96", module.exports)
  }
}

/***/ }),

/***/ 419:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 1),
      expression: "screen == 1"
    }]
  }, [_c('form', {
    staticClass: "form-horizontal"
  }, [_c('table', {
    staticClass: "table table-bordered table-striped table-hover membersDatatable"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.members), function(member) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(member.client.id))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(member.client.first_name + ' ' + member.client.last_name))]), _vm._v(" "), _c('td', {
      staticClass: "text-center"
    }, [_c('input', {
      attrs: {
        "type": "checkbox",
        "name": "clientsId[]"
      },
      domProps: {
        "value": member.client.id
      }
    })])])
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "spacer-10"
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.gotoScreen(2)
      }
    }
  }, [_vm._v("Continue")])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 2),
      expression: "screen == 2"
    }]
  }, [_c('form', {
    staticClass: "form-horizontal"
  }, [_c('div', {
    staticClass: "scrollable"
  }, [_c('table', {
    staticClass: "table table-bordered table-striped table-hover"
  }, [_vm._m(1), _vm._v(" "), _vm._l((_vm.members), function(member, index) {
    return (_vm.selectedClientsId.indexOf(member.client.id) != -1) ? _c('tbody', [_c('tr', {
      on: {
        "click": function($event) {
          _vm.toggleChildRow($event, index)
        }
      }
    }, [_c('td', {
      staticStyle: {
        "width": "45%"
      }
    }, [_vm._v(_vm._s(member.client.id))]), _vm._v(" "), _c('td', {
      staticStyle: {
        "width": "45%"
      }
    }, [_vm._v(_vm._s(member.client.first_name + ' ' + member.client.last_name))]), _vm._v(" "), _c('td', {
      staticClass: "text-center",
      staticStyle: {
        "width": "10%"
      }
    }, [_c('i', {
      staticClass: "fa fa-arrow-right cursor",
      class: 'arrow-' + index,
      attrs: {
        "id": "fa-1000"
      }
    })])]), _vm._v(" "), _c('tr', {
      staticClass: "hide",
      attrs: {
        "id": 'child-row-' + index
      }
    }, [_c('td', {
      attrs: {
        "colspan": "3"
      }
    }, [_c('table', {
      staticClass: "table table-bordered table-striped table-hover"
    }, [_vm._m(2, true), _vm._v(" "), _c('tbody', _vm._l((member.services), function(service) {
      return (service.group_id == _vm.groupid && service.active == 1 && service.status != 'Complete') ? _c('tr', [_c('td', [_vm._v(_vm._s(service.service_date))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.tracking))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.detail))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(service.cost)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(service.charge)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(service.tip)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(service.discount_amount)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.status))]), _vm._v(" "), _c('td', {
        staticClass: "text-center"
      }, [_c('input', {
        attrs: {
          "type": "checkbox",
          "name": "clientServicesId[]"
        },
        domProps: {
          "value": service.id
        }
      })])]) : _vm._e()
    }), 0)])])])]) : _vm._e()
  })], 2)]), _vm._v(" "), _c('div', {
    staticClass: "spacer-10"
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.setSelectedClientServicesAndTrackingsId
    }
  }, [_vm._v("Continue")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.gotoScreen(1)
      }
    }
  }, [_vm._v("Back")])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', {
    staticStyle: {
      "width": "40%"
    }
  }, [_vm._v("CLIENT ID")]), _vm._v(" "), _c('th', {
    staticStyle: {
      "width": "40%"
    }
  }, [_vm._v("NAME")]), _vm._v(" "), _c('th', {
    staticStyle: {
      "width": "20%"
    }
  }, [_vm._v("ACTION")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', {
    staticStyle: {
      "width": "45%"
    }
  }, [_vm._v("CLIENT ID")]), _vm._v(" "), _c('th', {
    staticStyle: {
      "width": "45%"
    }
  }, [_vm._v("NAME")]), _vm._v(" "), _c('th', {
    staticClass: "text-center",
    staticStyle: {
      "width": "10%"
    }
  }, [_vm._v("ACTION")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', {
    staticClass: "child-row-header"
  }, [_c('th', [_vm._v("Date")]), _vm._v(" "), _c('th', [_vm._v("Package")]), _vm._v(" "), _c('th', [_vm._v("Details")]), _vm._v(" "), _c('th', [_vm._v("Cost")]), _vm._v(" "), _c('th', [_vm._v("Charge")]), _vm._v(" "), _c('th', [_vm._v("Tip")]), _vm._v(" "), _c('th', [_vm._v("Discount")]), _vm._v(" "), _c('th', [_vm._v("Status")]), _vm._v(" "), _c('th', {
    staticClass: "text-center"
  })])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-c430dada", module.exports)
  }
}

/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(293);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("251ac334", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-06d26471\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EditService.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-06d26471\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EditService.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 433:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(295);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("2621287d", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-168f2e0e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./GroupMemberType.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-168f2e0e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./GroupMemberType.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 435:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(297);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("519e0638", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-27ab8230\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EditAddress.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-27ab8230\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EditAddress.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 436:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(298);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("4512acf8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-3928fa9c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddNewService.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-3928fa9c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddNewService.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 439:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(301);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("fd3cd98a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-3ca94bef\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeleteMember.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-3ca94bef\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeleteMember.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 440:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(302);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("3bf69eca", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-5199a8a8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddFund.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-5199a8a8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddFund.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 445:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(307);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("60dcff78", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-73df72fa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddNewMember.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-73df72fa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddNewMember.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 449:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(311);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("5d2ac727", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-bbd2ec96\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Transfer.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-bbd2ec96\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Transfer.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 450:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(312);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("236e7bc8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-c430dada\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ClientServices.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-c430dada\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ClientServices.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 482:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(192);


/***/ }),

/***/ 7:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNDJjZTBjNmM2ZTJiM2UyMjc4MzUiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly8vTXVsdGlwbGVRdWlja1JlcG9ydC52dWUiLCJ3ZWJwYWNrOi8vL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWU/NDcwNCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWU/ZDU2OSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZT85Yzc4Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWU/YzM3ZCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2dyb3Vwcy9zaG93LmpzIiwid2VicGFjazovLy8uL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnQudnVlP2I4NTMiLCJ3ZWJwYWNrOi8vL0FjdGlvbkxvZ3MudnVlIiwid2VicGFjazovLy9BZGRGdW5kLnZ1ZSIsIndlYnBhY2s6Ly8vQWRkTmV3TWVtYmVyLnZ1ZSIsIndlYnBhY2s6Ly8vQWRkTmV3U2VydmljZS52dWUiLCJ3ZWJwYWNrOi8vL0NsaWVudFNlcnZpY2VzLnZ1ZSIsIndlYnBhY2s6Ly8vRGVsZXRlTWVtYmVyLnZ1ZSIsIndlYnBhY2s6Ly8vVHJhbnNhY3Rpb25Mb2dzLnZ1ZSIsIndlYnBhY2s6Ly8vRWRpdEFkZHJlc3MudnVlIiwid2VicGFjazovLy9FZGl0U2VydmljZS52dWUiLCJ3ZWJwYWNrOi8vL0dyb3VwTWVtYmVyVHlwZS52dWUiLCJ3ZWJwYWNrOi8vL1RyYW5zZmVyLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvQWN0aW9uTG9ncy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRTZXJ2aWNlLnZ1ZT81NjYzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9Hcm91cE1lbWJlclR5cGUudnVlP2NiZjEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRBZGRyZXNzLnZ1ZT9lMDVkIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGROZXdTZXJ2aWNlLnZ1ZT81OWEzIiwid2VicGFjazovLy8uL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9UcmFuc2FjdGlvbkxvZ3MudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVNZW1iZXIudnVlPzk2ZGMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZEZ1bmQudnVlP2FjZTIiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZE5ld01lbWJlci52dWU/YzY1ZiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvVHJhbnNhY3Rpb25Mb2dzLnZ1ZT9hYTllIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9UcmFuc2Zlci52dWU/MGJlZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQ2xpZW50U2VydmljZXMudnVlP2RiMmUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL0FjdGlvbkxvZ3MudnVlP2ZjNzciLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZEZ1bmQudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGROZXdNZW1iZXIudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGROZXdTZXJ2aWNlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQ2xpZW50U2VydmljZXMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVNZW1iZXIudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0QWRkcmVzcy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRTZXJ2aWNlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvR3JvdXBNZW1iZXJUeXBlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvVHJhbnNmZXIudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0U2VydmljZS52dWU/M2JiMyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvR3JvdXBNZW1iZXJUeXBlLnZ1ZT82MzBkIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0QWRkcmVzcy52dWU/OTIzYiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkTmV3U2VydmljZS52dWU/ZGQwYSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlTWVtYmVyLnZ1ZT80MzlkIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGRGdW5kLnZ1ZT80NTZhIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGROZXdNZW1iZXIudnVlP2M4NTMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1RyYW5zZmVyLnZ1ZT83Y2ZiIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9DbGllbnRTZXJ2aWNlcy52dWU/MWYzZCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRWRpdFNlcnZpY2UudnVlP2VkYTkiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0dyb3VwTWVtYmVyVHlwZS52dWU/ZTQ3MyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRWRpdEFkZHJlc3MudnVlPzBhNDAiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZE5ld1NlcnZpY2UudnVlP2Y1MzUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0RlbGV0ZU1lbWJlci52dWU/YzJjNSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRnVuZC52dWU/MWM2ZCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkTmV3TWVtYmVyLnZ1ZT9iYWQ3Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9UcmFuc2Zlci52dWU/MjVmMiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQ2xpZW50U2VydmljZXMudnVlPzFhYTgiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtc3R5bGUtbG9hZGVyL2xpYi9saXN0VG9TdHlsZXMuanMiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImRhdGEiLCJ3aW5kb3ciLCJMYXJhdmVsIiwidXNlciIsImVsIiwiZ3JvdXBJZCIsImRldGFpbHMiLCJtZW1iZXJzIiwic2VydmljZUNvc3QiLCJzZUxjbGllbnRJZCIsImFzc2lnbkxlYWRlckZvcm0iLCJGb3JtIiwiY2xpZW50SWQiLCJ0eXBlIiwiYmFzZVVSTCIsImF4aW9zQVBJdjEiLCJkZWZhdWx0cyIsInF1aWNrUmVwb3J0Q2xpZW50c0lkIiwicXVpY2tSZXBvcnRDbGllbnRTZXJ2aWNlc0lkIiwicXVpY2tSZXBvcnRUcmFja2luZ3MiLCJhY3Rpb25Mb2dzIiwiY3VycmVudF95ZWFyIiwiY3VycmVudF9tb250aCIsImN1cnJlbnRfZGF5IiwiY195ZWFyIiwiY19tb250aCIsImNfZGF5Iiwic19jb3VudCIsInRyYW5zYWN0aW9uTG9ncyIsImN1cnJlbnRfeWVhcjIiLCJjdXJyZW50X21vbnRoMiIsImN1cnJlbnRfZGF5MiIsImNfeWVhcjIiLCJjX21vbnRoMiIsImNfZGF5MiIsInNfY291bnQyIiwiZGVwb3NpdHMiLCJwYXltZW50cyIsInJlZnVuZHMiLCJkaXNjb3VudHMiLCJnU2VydmljZXMiLCJnQmF0Y2giLCJkb2NzcmN2Iiwic2V0dGluZyIsImFjY2Vzc19jb250cm9sIiwicGVybWlzc2lvbnMiLCJzZWxwZXJtaXNzaW9ucyIsImVkaXRBZGRyZXNzIiwibmV3TWVtYmVyIiwiYWRkTmV3U2VydmljZSIsImFkZEZ1bmRzIiwid3JpdGVSZXBvcnQiLCJtYWtlTGVhZGVyIiwidHJhbnNmZXIiLCJkZWxldGUiLCJlZGl0U2VydmljZSIsIm1ldGhvZHMiLCJpbml0IiwicGF0aEFycmF5IiwibG9jYXRpb24iLCJwYXRobmFtZSIsInNwbGl0IiwiZmV0Y2hEZXRhaWxzIiwiZmV0Y2hNZW1iZXJzIiwiZmV0Y2hHU2VydmljZXMiLCJmZXRjaEdCYXRjaCIsImdldCIsInRoZW4iLCJyZXNwb25zZSIsImNhdGNoIiwiY29uc29sZSIsImxvZyIsImVycm9yIiwiZ3JvdXBtZW1iZXIiLCIkcmVmcyIsImNsaWVudHNlcnZpY2VzcmVmIiwiY3JlYXRlRGF0YXRhYmxlIiwiY29tcHV0ZVNlcnZpY2VDb3N0IiwiY3JlYXRlRGF0YXRhYmxlVG9nZ2xlUm93IiwiZmV0Y2hEb2NzIiwiaWQiLCJtdWx0aXBsZXF1aWNrcmVwb3J0cmVmIiwibWFwIiwiY29zdCIsImNoYXJnZSIsInRpcCIsIm1lbWJlciIsInNlcnZpY2VzIiwic2VydmljZSIsImFjdGl2ZSIsImdyb3VwX2lkIiwicGFyc2VGbG9hdCIsImNyZWF0ZURhdGF0YWJsZVRvZ2dsZUJ5U2VydmljZSIsImNyZWF0ZURhdGF0YWJsZVRvZ2dsZUJ5QmF0Y2giLCJhc3NpZ25MZWFkZXIiLCJzdWJtaXQiLCJyZXN1bHQiLCJzdWNjZXNzIiwiJCIsIm1vZGFsIiwidG9hc3RyIiwibWVzc2FnZSIsInNob3dNZW1iZXJSb3ciLCJlIiwidGFyZ2V0Iiwibm9kZU5hbWUiLCJzZXRUaW1lb3V0IiwidG9nZ2xlQ2xhc3MiLCJyZW1vdmVBdHRyIiwiY2xhc3NOYW1lIiwiRGF0YVRhYmxlIiwiZGVzdHJveSIsInBhZ2VMZW5ndGgiLCJyZXNwb25zaXZlIiwiZG9tIiwiZmlsdGVyQ3VycmVuY3kiLCJ2YWx1ZSIsIm51bWJlcldpdGhDb21tYXMiLCJ0b0ZpeGVkIiwicGFyc2VJbnQiLCJ0b1N0cmluZyIsInJlcGxhY2UiLCJjcmVhdGVRdWlja1JlcG9ydCIsImNsaWVudFNlcnZpY2VJZCIsInRyYWNraW5nIiwiY2xpZW50U2VydmljZSIsInZpIiwiZm9ybWF0IiwiZCIsImNpZCIsInRhYmxlIiwiaSIsImxlbmd0aCIsImNsaWVudCIsImRvY3VtZW50X3JlY2VpdmUiLCJKU09OIiwicGFyc2UiLCJzdHJpbmdpZnkiLCJ2aXNpYmlsaXR5IiwicmVtYXJrcyIsImRldGFpbCIsImRpc2NvdW50MiIsInJlYXNvbiIsImRpc2NvdW50IiwiZGlzY291bnRfYW1vdW50IiwiX2FjdGlvbiIsImNsaWVudF9pZCIsInNlcnZpY2VfaWQiLCJzZXJ2aWNlX2RhdGUiLCJzdGF0dXMiLCJyY3YiLCJkb2NyY3YiLCJiaW5kIiwicmVuZGVyIiwicm93IiwiZnVsbF9uYW1lIiwibWV0YSIsImxlYWRlciIsIm9mZiIsIm9uIiwidHIiLCJjbG9zZXN0IiwiY2hpbGQiLCJpc1Nob3duIiwiaGlkZSIsInJlbW92ZUNsYXNzIiwic2hvdyIsImFkZENsYXNzIiwiZ2V0QXR0cmlidXRlIiwidHJhbnNmZXJyZWYiLCJzZXRNZW1iZXJJZCIsImRlbGV0ZW1lbWJlcnJlZiIsInNlcnZpY2VDbGllbnRJZCIsInNlcnZpY2VTZXJ2aWNlaWQiLCJzZXJ2aWNlSWQiLCJlZGl0c2VydmljZXJlZiIsImNoYW5nZU1lbWJlclR5cGUiLCJzZXJ2IiwiZm9ybWF0QnlTZXJ2aWNlIiwiZGl2IiwidGV4dCIsInBvc3QiLCJhIiwiZmlyc3RfbmFtZSIsImxhc3RfbmFtZSIsImh0bWwiLCJ0YWJsZTIiLCJtb21lbnQiLCJTdHJpbmciLCJ0b3RhbGNvc3QiLCJ0cjIiLCJmb3JtYXRCeUJhdGNoIiwiZGF0ZSIsInRhYmxlMyIsInRyMyIsImZldGNoRGVwb3NpdHMiLCJmZXRjaFBheW1lbnRzIiwiZmV0Y2hSZWZ1bmRzIiwiZmV0Y2hEaXNjb3VudHMiLCJyZXNldEFjdGlvbkxvZ3NDb21wb25lbnREYXRhIiwiZmV0Y2hBY3Rpb25Mb2dzIiwicmVzZXRUcmFuc2FjdGlvbkxvZ3NDb21wb25lbnREYXRhIiwiZmV0Y2hUcmFuc2FjdGlvbkxvZ3MiLCJjcmVhdGVkIiwibW91bnRlZCIsInBvcG92ZXIiLCJ0cmlnZ2VyIiwic2VsZWN0b3IiLCJjb21wb25lbnRzIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDckJBOztBQUVBLG1HQUZBOztBQUlBLEtBSkEsa0JBSUE7QUFDQTs7QUFFQSxXQUZBO0FBR0Esa0JBSEE7O0FBS0E7QUFDQSw4QkFEQTs7QUFHQSxxQkFIQTtBQUlBLGlCQUpBOztBQU1BLGlCQU5BO0FBT0Esd0JBUEE7QUFRQSxpQkFSQTs7QUFVQSxnQkFWQTs7QUFZQSxrQkFaQTtBQWFBLG9CQWJBOztBQWVBLGFBZkE7QUFnQkEsYUFoQkE7QUFpQkEsYUFqQkE7QUFrQkEscUJBbEJBOztBQW9CQTtBQXBCQSxNQXFCQSx3Q0FyQkE7O0FBTEE7QUE2QkEsRUFsQ0E7OztBQW9DQTtBQUVBLFdBRkEscUJBRUEsRUFGQSxFQUVBO0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFGQTs7QUFJQTtBQUNBO0FBQ0EsSUFGQSxFQUVBLElBRkEsQ0FFQSxHQUZBOztBQUlBLDREQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0EsSUFIQTs7QUFLQTtBQUNBO0FBQ0EsSUFGQSxFQUVBLElBRkE7QUFHQSxHQXBCQTtBQXNCQSxnQkF0QkEsNEJBc0JBO0FBQUE7O0FBQ0EsK0NBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQSxJQUhBO0FBSUEsR0EzQkE7QUE2QkEsOEJBN0JBLDBDQTZCQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGdCQURBOztBQUdBLHFCQUhBO0FBSUEsdUJBSkE7O0FBTUEsZ0JBTkE7QUFPQSxnQkFQQTtBQVFBLGdCQVJBO0FBU0Esc0JBVEE7O0FBV0E7QUFYQTs7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLElBbkNBOztBQXFDQTtBQUNBO0FBQ0EsOEJBREE7O0FBR0EsZ0JBSEE7O0FBS0Esa0JBTEE7QUFNQSxvQkFOQTs7QUFRQSxhQVJBO0FBU0EsYUFUQTtBQVVBLGFBVkE7QUFXQSxxQkFYQTs7QUFhQTtBQWJBLE1BY0Esd0NBZEE7QUFlQSxHQXBGQTtBQXNGQSxVQXRGQSxzQkFzRkE7QUFBQTs7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBTkE7O0FBUUE7QUFDQSxHQWxHQTtBQW9HQSx3QkFwR0Esb0NBb0dBO0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQXBCQTs7QUFzQkEsc0VBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxRQUZBO0FBR0EsT0FKQSxNQUlBO0FBQ0E7QUFDQTtBQUNBLFFBRkEsRUFFQSxJQUZBO0FBR0E7QUFDQTtBQUNBLEtBbkJBO0FBb0JBO0FBQ0E7QUF4SkEsRUFwQ0E7O0FBZ01BLFFBaE1BLHFCQWdNQTtBQUNBO0FBQ0EsRUFsTUE7OztBQW9NQTtBQUNBLG9CQURBLGdDQUNBO0FBQ0E7QUFDQSxHQUhBO0FBS0Esd0JBTEEsb0NBS0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBSkE7O0FBTUE7QUFDQTtBQWZBLEVBcE1BOztBQXNOQTtBQUNBO0FBREE7O0FBdE5BLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3VWQTs7QUFFQSx3Q0FGQTs7QUFJQSxLQUpBLGtCQUlBO0FBQ0E7O0FBRUEsY0FGQTtBQUdBLGlCQUhBOztBQUtBLHNCQUxBOztBQU9BLHdCQVBBOztBQVNBO0FBQ0EsZUFEQTs7QUFHQSxvQkFIQTtBQUlBLHNCQUpBOztBQU1BLGVBTkE7QUFPQSxlQVBBO0FBUUEsZUFSQTtBQVNBLHFCQVRBOztBQVdBO0FBWEE7O0FBVEE7QUF3QkEsRUE3QkE7OztBQStCQTtBQUVBLFlBRkEsd0JBRUE7QUFBQTs7QUFDQSw4QkFDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEEsRUFJQSxLQUpBLENBSUE7QUFBQTtBQUFBLElBSkE7QUFLQSxHQVJBO0FBVUEsb0JBVkEsZ0NBVUE7QUFBQTs7QUFDQTs7QUFFQSw0Q0FDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEEsRUFJQSxLQUpBLENBSUE7QUFBQTtBQUFBLElBSkE7QUFLQSxHQWxCQTtBQW9CQSxnQkFwQkEsNEJBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBbkNBO0FBcUNBLHFCQXJDQSwrQkFxQ0EsUUFyQ0EsRUFxQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQWpEQTtBQW1EQSxlQW5EQSwyQkFtREE7QUFBQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQURBLE1BS0EsSUFMQSxDQUtBO0FBQ0E7QUFDQSxJQVBBLEVBUUEsS0FSQSxDQVFBO0FBQUE7QUFBQSxJQVJBO0FBU0EsR0FuRUE7QUFxRUEsdUJBckVBLG1DQXFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBekVBO0FBMkVBLHdCQTNFQSxvQ0EyRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esd0JBREE7QUFFQSx1Q0FGQTtBQUdBLHNCQUhBO0FBSUEsbUJBSkE7QUFLQTtBQUxBO0FBT0EsSUFSQSxFQVFBLEdBUkE7QUFTQSxHQXZGQTtBQXlGQSwyQkF6RkEscUNBeUZBLEtBekZBLEVBeUZBO0FBQ0E7QUFDQSxHQTNGQTtBQTZGQSxVQTdGQSxvQkE2RkEscUJBN0ZBLEVBNkZBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0EsTUFGQTs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsSUF2QkEsTUF1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBTEEsTUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBRkE7O0FBSUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLElBZEEsTUFjQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxJQVRBLE1BU0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsSUFUQSxNQVNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBaEJBLE1BZ0JBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxNQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQTFCQSxNQTBCQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsSUFaQSxNQVlBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxJQVpBLE1BWUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsSUFUQSxNQVNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLElBVEEsTUFTQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQXZCQSxNQXVCQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsSUFaQSxNQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBM1JBLEVBL0JBOztBQThUQSxRQTlUQSxxQkE4VEE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLEVBcFVBO0FBc1VBLFFBdFVBLHFCQXNVQTs7QUFFQTtBQUNBO0FBQ0EsZ0JBREE7QUFFQSx3Q0FGQTtBQUdBO0FBSEE7O0FBTUE7QUFDQTtBQUNBLHFCQURBO0FBRUEsNEJBRkE7QUFHQSxvQkFIQTtBQUlBLHNCQUpBO0FBS0Esa0JBTEE7QUFNQSx1QkFOQTtBQU9BO0FBUEE7O0FBVUE7QUFDQTtBQUNBLHNCQURBO0FBRUEscUNBRkE7QUFHQSxvQkFIQTtBQUlBLGlCQUpBO0FBS0E7QUFMQTtBQVFBO0FBbldBLEc7Ozs7Ozs7QUNwWEEsMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcsVUFBVSxtRkFBbUYsTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLDBZQUEwWSxpQ0FBaUMsbXJCQUFtckIsMkhBQTJILGdCQUFnQix1R0FBdUcsbWRBQW1kLEdBQUcsdUNBQXVDLFlBQVksT0FBTyxtQkFBbUIseUJBQXlCLHVCQUF1Qix3Q0FBd0MseUVBQXlFLFdBQVcsRUFBRSx3RUFBd0UsNENBQTRDLFdBQVcsWUFBWSwyR0FBMkcsMENBQTBDLGVBQWUsRUFBRSxrQ0FBa0MsdUVBQXVFLGlCQUFpQixRQUFRLFNBQVMsNkJBQTZCLHVGQUF1RixpREFBaUQsZUFBZSxFQUFFLGFBQWEsMkNBQTJDLDJIQUEySCx3Q0FBd0MsOERBQThELCtTQUErUyw4REFBOEQsNERBQTRELDREQUE0RCw0REFBNEQsNERBQTRELDREQUE0RCw0REFBNEQsNERBQTRELDZEQUE2RCw2REFBNkQsNkRBQTZELDZEQUE2RCw2REFBNkQsNkRBQTZELDZEQUE2RCxtRUFBbUUsOERBQThELGFBQWEsRUFBRSxtR0FBbUcsa1VBQWtVLEdBQUcsdUNBQXVDLEVBQUUsU0FBUyx1QkFBdUIsNkJBQTZCLG1GQUFtRixzQ0FBc0MsMEVBQTBFLDhCQUE4QixhQUFhLFdBQVcsRUFBRSwyQkFBMkIsU0FBUyxxQ0FBcUMsK0JBQStCLHdFQUF3RSxnRUFBZ0Usb0VBQW9FLGdGQUFnRixvRUFBb0UsOEZBQThGLHFGQUFxRix3Q0FBd0Msb0ZBQW9GLHNGQUFzRiwwRkFBMEYsNEVBQTRFLDRFQUE0RSw0RUFBNEUsNEZBQTRGLDBGQUEwRixzRUFBc0Usd0VBQXdFLGdGQUFnRixrRUFBa0Usa0VBQWtFLGtFQUFrRSxrRkFBa0YsZ0ZBQWdGLGFBQWEsRUFBRSxzSEFBc0gsNENBQTRDLDREQUE0RCwyRkFBMkYsMkRBQTJELHNEQUFzRCwyRkFBMkYsMkVBQTJFLHFCQUFxQixFQUFFLHlCQUF5QixPQUFPLDRDQUE0Qyw4Q0FBOEMsMkJBQTJCLFFBQVEseUJBQXlCLHVCQUF1QixxQkFBcUIsRUFBRSxXQUFXLFNBQVMsU0FBUyxvQkFBb0IsOEJBQThCLE9BQU8sb0JBQW9CLDhCQUE4QixxREFBcUQsV0FBVyxxQ0FBcUMsMENBQTBDLDBEQUEwRCw0RUFBNEUsZ0VBQWdFLGFBQWEsV0FBVyxFQUFFLDBDQUEwQyxTQUFTLE9BQU8sc0JBQXNCLDJGQUEyRixPQUFPLHlDQUF5QywwQkFBMEIsS0FBSyxhQUFhLHlCQUF5QixLQUFLLGFBQWEsRzs7Ozs7Ozs7QUNBbHhSO0FBQ0EsbUJBQU8sQ0FBQyxFQUF1Ujs7QUFFL1IsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBaVA7QUFDM1A7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBNk07QUFDdk47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDL0JBLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEVBQXFQO0FBQy9QO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEVBQWlOO0FBQzNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQy9DQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLE9BQU87QUFDUDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLHdDQUF3QyxRQUFRO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDMzJCQUEsSUFBSUMsU0FBSixDQUFjLGFBQWQsRUFBOEJDLG1CQUFPQSxDQUFDLEVBQVIsQ0FBOUI7QUFDQUYsSUFBSUMsU0FBSixDQUFjLGtCQUFkLEVBQW1DQyxtQkFBT0EsQ0FBQyxFQUFSLENBQW5DOztBQUVBLElBQUlDLE9BQU9DLE9BQU9DLE9BQVAsQ0FBZUMsSUFBMUI7O0FBRUEsSUFBSU4sR0FBSixDQUFROztBQUVQTyxLQUFJLE1BRkc7O0FBSVBKLE9BQU07O0FBRUxLLFdBQVMsSUFGSixFQUVVOztBQUVmQyxXQUFTLElBSkosRUFJVTs7QUFFZkMsV0FBUyxJQU5KLEVBTVU7O0FBRWZDLGVBQWEsRUFSUixFQVFZOztBQUVqQkMsZUFBYSxFQVZSOztBQVlMQyxvQkFBa0IsSUFBSUMsSUFBSixDQUFTLEVBQUU7QUFDNUJDLGFBQVUsRUFEZ0I7QUFFMUJDLFNBQUs7QUFGcUIsR0FBVCxFQUdmLEVBQUVDLFNBQVNDLFdBQVdDLFFBQVgsQ0FBb0JGLE9BQS9CLEVBSGUsQ0FaYjs7QUFpQkxHLHdCQUFzQixFQWpCakI7QUFrQkxDLCtCQUE2QixFQWxCeEI7QUFtQkxDLHdCQUFzQixFQW5CakI7O0FBcUJMQyxjQUFZLEVBckJQO0FBc0JMQyxnQkFBYyxJQXRCVDtBQXVCTEMsaUJBQWUsSUF2QlY7QUF3QkxDLGVBQWEsSUF4QlI7QUF5QkxDLFVBQVEsSUF6Qkg7QUEwQkxDLFdBQVMsSUExQko7QUEyQkxDLFNBQU8sSUEzQkY7QUE0QkxDLFdBQVMsQ0E1Qko7O0FBOEJMQyxtQkFBaUIsRUE5Qlo7QUErQkxDLGlCQUFlLElBL0JWO0FBZ0NMQyxrQkFBZ0IsSUFoQ1g7QUFpQ0xDLGdCQUFjLElBakNUO0FBa0NMQyxXQUFTLElBbENKO0FBbUNMQyxZQUFVLElBbkNMO0FBb0NMQyxVQUFRLElBcENIO0FBcUNMQyxZQUFVLENBckNMOztBQXVDTEMsWUFBVSxFQXZDTDtBQXdDTEMsWUFBVSxFQXhDTDtBQXlDTEMsV0FBUyxFQXpDSjtBQTBDTEMsYUFBVyxFQTFDTjs7QUE0Q0xDLGFBQVcsSUE1Q047QUE2Q0xDLFVBQVEsSUE3Q0g7O0FBK0NMQyxXQUFTLElBL0NKOztBQWlEQ0MsV0FBUzNDLEtBQUs0QyxjQUFMLENBQW9CLENBQXBCLEVBQXVCRCxPQWpEakM7QUFrRENFLGVBQWE3QyxLQUFLNkMsV0FsRG5CO0FBbURDQyxrQkFBZ0IsQ0FBQztBQUN0QkMsZ0JBQVksQ0FEVTtBQUV0QjNCLGVBQVcsQ0FGVztBQUd0QlEsb0JBQWdCLENBSE07QUFJdEJRLGFBQVMsQ0FKYTtBQUt0QkMsYUFBUyxDQUxhO0FBTXRCQyxZQUFRLENBTmM7QUFPdEJDLGNBQVUsQ0FQWTtBQVF0QlMsY0FBVSxDQVJZO0FBU3RCQyxrQkFBYyxDQVRRO0FBVXRCQyxhQUFTLENBVmE7QUFXdEJDLGdCQUFZLENBWFU7QUFZdEJDLGVBQVcsQ0FaVztBQWF0QkMsYUFBUyxDQWJhO0FBY3RCQyxXQUFPLENBZGU7QUFldEJDLGdCQUFZO0FBZlUsR0FBRDs7QUFuRGpCLEVBSkM7O0FBMkVQQyxVQUFTO0FBQ1I7QUFDQUMsTUFGUSxrQkFFRDs7QUFFTixPQUFJQyxZQUFZekQsT0FBTzBELFFBQVAsQ0FBZ0JDLFFBQWhCLENBQXlCQyxLQUF6QixDQUFnQyxHQUFoQyxDQUFoQjtBQUNBLFFBQUt4RCxPQUFMLEdBQWVxRCxVQUFVLENBQVYsQ0FBZjs7QUFFQSxRQUFLSSxZQUFMO0FBQ0EsUUFBS0MsWUFBTDs7QUFFQSxRQUFLQyxjQUFMO0FBQ0EsUUFBS0MsV0FBTDtBQUNBLEdBWk87OztBQWNSO0FBQ0FILGNBZlEsMEJBZU87QUFBQTs7QUFDZCxPQUFJekQsVUFBVSxLQUFLQSxPQUFuQjs7QUFFQVUsY0FBV21ELEdBQVgsQ0FBZSxpQkFBaUI3RCxPQUFqQixHQUEyQixVQUExQyxFQUNFOEQsSUFERixDQUNPLG9CQUFZO0FBQ2pCLFVBQUs3RCxPQUFMLEdBQWU4RCxTQUFTcEUsSUFBeEI7QUFDQSxJQUhGLEVBSUVxRSxLQUpGLENBSVE7QUFBQSxXQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLElBSlI7QUFLQSxHQXZCTzs7O0FBeUJSO0FBQ0FULGNBMUJRLDBCQTBCTztBQUFBOztBQUNkLE9BQUkxRCxVQUFVLEtBQUtBLE9BQW5COztBQUVBVSxjQUFXbUQsR0FBWCxDQUFlLGlCQUFpQjdELE9BQWpCLEdBQTJCLFVBQTFDLEVBQ0U4RCxJQURGLENBQ08sb0JBQVk7QUFDakIsV0FBSzVELE9BQUwsR0FBZTZELFNBQVNwRSxJQUFULENBQWN5RSxXQUE3Qjs7QUFFQSxXQUFLQyxLQUFMLENBQVdDLGlCQUFYLENBQTZCQyxlQUE3QixDQUE2QyxrQkFBN0M7O0FBRUEsV0FBS0Msa0JBQUwsQ0FBd0IsT0FBS3RFLE9BQTdCOztBQUVBLFdBQUt1RSx3QkFBTCxDQUE4QixPQUFLdkUsT0FBbkM7QUFDQSxJQVRGLEVBVUU4RCxLQVZGLENBVVE7QUFBQSxXQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLElBVlI7QUFXQSxHQXhDTzs7O0FBMENSO0FBQ0FPLFdBM0NRLHFCQTJDRUMsRUEzQ0YsRUEyQ007QUFDYixRQUFLTixLQUFMLENBQVdPLHNCQUFYLENBQWtDRixTQUFsQyxDQUE0Q0MsRUFBNUM7QUFDQSxHQTdDTzs7O0FBK0NSO0FBQ0FILG9CQWhEUSw4QkFnRFd0RSxPQWhEWCxFQWdEb0I7QUFDM0IsT0FBSUYsVUFBVSxLQUFLQSxPQUFuQjs7QUFFQSxRQUFLRyxXQUFMLEdBQW1CRCxRQUFRMkUsR0FBUixDQUFZLGtCQUFVO0FBQ3hDLFFBQUlDLE9BQU8sQ0FBWDtBQUNBLFFBQUlDLFNBQVMsQ0FBYjtBQUNBLFFBQUlDLE1BQU0sQ0FBVjs7QUFFQUMsV0FBT0MsUUFBUCxDQUFnQkwsR0FBaEIsQ0FBb0IsbUJBQVc7QUFDOUIsU0FBR00sUUFBUUMsTUFBUixJQUFrQixDQUFsQixJQUF1QkQsUUFBUUUsUUFBUixJQUFvQnJGLE9BQTlDLEVBQXVEO0FBQ3REOEUsY0FBUVEsV0FBV0gsUUFBUUwsSUFBbkIsQ0FBUjtBQUNBQyxnQkFBVU8sV0FBV0gsUUFBUUosTUFBbkIsQ0FBVjtBQUNBQyxhQUFPTSxXQUFXSCxRQUFRSCxHQUFuQixDQUFQO0FBQ0E7QUFDRCxLQU5EOztBQVFBLFdBQVFGLE9BQU9DLE1BQVAsR0FBZ0JDLEdBQXhCO0FBQ0EsSUFka0IsQ0FBbkI7QUFlQSxHQWxFTzs7O0FBb0VSO0FBQ0FyQixnQkFyRVEsNEJBcUVTO0FBQUE7O0FBQ2hCLE9BQUkzRCxVQUFVLEtBQUtBLE9BQW5COztBQUVBVSxjQUFXbUQsR0FBWCxDQUFlLGlCQUFpQjdELE9BQWpCLEdBQTJCLFdBQTFDLEVBQ0U4RCxJQURGLENBQ08sb0JBQVk7QUFDakI7QUFDQSxXQUFLM0IsU0FBTCxHQUFpQjRCLFNBQVNwRSxJQUExQjs7QUFFQSxXQUFLNEYsOEJBQUwsQ0FBb0MsT0FBS3BELFNBQXpDO0FBRUEsSUFQRixFQVFFNkIsS0FSRixDQVFRO0FBQUEsV0FBU0MsUUFBUUMsR0FBUixDQUFZQyxLQUFaLENBQVQ7QUFBQSxJQVJSO0FBU0EsR0FqRk87QUFtRlJQLGFBbkZRLHlCQW1GTTtBQUFBOztBQUNiLE9BQUk1RCxVQUFVLEtBQUtBLE9BQW5COztBQUVBVSxjQUFXbUQsR0FBWCxDQUFlLGlCQUFpQjdELE9BQWpCLEdBQTJCLFFBQTFDLEVBQ0U4RCxJQURGLENBQ08sb0JBQVk7QUFDakI7QUFDQSxXQUFLMUIsTUFBTCxHQUFjMkIsU0FBU3BFLElBQXZCOztBQUVBLFdBQUs2Riw0QkFBTCxDQUFrQyxPQUFLcEQsTUFBdkM7QUFFQSxJQVBGLEVBUUU0QixLQVJGLENBUVE7QUFBQSxXQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLElBUlI7QUFTQSxHQS9GTzs7O0FBaUdSO0FBQ0FzQixjQWxHUSx3QkFrR0tsRixRQWxHTCxFQWtHZUMsSUFsR2YsRUFrR3FCO0FBQUE7O0FBQzVCLE9BQUlSLFVBQVUsS0FBS0EsT0FBbkI7O0FBRUEsUUFBS0ssZ0JBQUwsQ0FBc0JFLFFBQXRCLEdBQWlDQSxRQUFqQztBQUNBLFFBQUtGLGdCQUFMLENBQXNCRyxJQUF0QixHQUE2QkEsSUFBN0I7O0FBRUEsUUFBS0gsZ0JBQUwsQ0FBc0JxRixNQUF0QixDQUE2QixPQUE3QixFQUFzQyxpQkFBaUIxRixPQUFqQixHQUEyQixnQkFBakUsRUFDVThELElBRFYsQ0FDZSxrQkFBVTtBQUNaLFFBQUc2QixPQUFPQyxPQUFWLEVBQW1COztBQUVsQixZQUFLbEMsWUFBTDtBQUNBLFlBQUtELFlBQUw7QUFDWm9DLE9BQUUsb0JBQUYsRUFBd0JDLEtBQXhCLENBQThCLE1BQTlCO0FBQ0FDLFlBQU9ILE9BQVAsQ0FBZUQsT0FBT0ssT0FBdEI7QUFDQTtBQUNRLElBVFYsRUFVVWhDLEtBVlYsQ0FVZ0I7QUFBQSxXQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLElBVmhCOztBQVlTLFFBQUsvRCxXQUFMLEdBQW1CLEVBQW5CO0FBQ1QsR0FySE87OztBQXVIUjtBQUNBNkYsZUF4SFEseUJBd0hNdEIsRUF4SE4sRUF3SFV1QixDQXhIVixFQXdIYTtBQUNwQixPQUFHQSxFQUFFQyxNQUFGLENBQVNDLFFBQVQsSUFBcUIsR0FBeEIsRUFBNkI7QUFDNUJDLGVBQVcsWUFBVztBQUNyQlIsT0FBRSxpQkFBaUJsQixFQUFuQixFQUF1QjJCLFdBQXZCLENBQW1DLE1BQW5DLEVBQTJDQyxVQUEzQyxDQUFzRCxPQUF0RDtBQUNBLEtBRkQsRUFFRyxHQUZIO0FBR0E7QUFDRCxHQTlITztBQWdJUmhDLGlCQWhJUSwyQkFnSVFpQyxTQWhJUixFQWdJbUI7QUFDMUJYLEtBQUUsV0FBU1csU0FBWCxFQUFzQkMsU0FBdEIsR0FBa0NDLE9BQWxDOztBQUVBTCxjQUFXLFlBQVc7QUFDckJSLE1BQUUsV0FBU1csU0FBWCxFQUFzQkMsU0FBdEIsQ0FBZ0M7QUFDekJFLGlCQUFZLEVBRGE7QUFFekJDLGlCQUFZLElBRmE7QUFHekIsa0JBQWEsRUFIWTtBQUl6QkMsVUFBSztBQUpvQixLQUFoQztBQU1BLElBUEQsRUFPRyxHQVBIO0FBUUEsR0EzSU87QUE2SVJDLGdCQTdJUSwwQkE2SU9DLEtBN0lQLEVBNkljO0FBQ3JCLE9BQUdBLFNBQVMsSUFBWixFQUFpQjtBQUNiLFFBQUdBLFFBQU0sQ0FBTixJQUFXLENBQWQsRUFBZ0I7QUFDYixZQUFRLEtBQUtDLGdCQUFMLENBQXNCMUIsV0FBV3lCLEtBQVgsRUFBa0JFLE9BQWxCLENBQTBCLENBQTFCLENBQXRCLENBQVI7QUFDRixLQUZELE1BRU87QUFDSixZQUFRLEtBQUtELGdCQUFMLENBQXNCRSxTQUFTSCxLQUFULENBQXRCLENBQVI7QUFDRjtBQUNKLElBTkQsTUFNTTtBQUNMLFdBQU8sQ0FBUDtBQUNBO0FBQ0QsR0F2Sk87QUF5SlJDLGtCQXpKUSw0QkF5SlNELEtBekpULEVBeUpnQjtBQUN2QixVQUFPQSxNQUFNSSxRQUFOLEdBQWlCQyxPQUFqQixDQUF5Qix1QkFBekIsRUFBa0QsR0FBbEQsQ0FBUDtBQUNBLEdBM0pPO0FBNkpSQyxtQkE3SlEsNkJBNkpVOUcsUUE3SlYsRUE2Sm9CK0csZUE3SnBCLEVBNkpxQ0MsUUE3SnJDLEVBNkorQztBQUFBOztBQUN0RCxPQUFJdkgsVUFBVSxLQUFLQSxPQUFuQjs7QUFFQVUsY0FBV21ELEdBQVgsQ0FBZSxpQkFBaUI3RCxPQUFqQixHQUEyQixrQkFBM0IsR0FBZ0RzSCxlQUEvRCxFQUNFeEQsSUFERixDQUNPLG9CQUFZO0FBQ2pCLFFBQUkwRCxnQkFBZ0J6RCxTQUFTcEUsSUFBN0I7O0FBRUEsV0FBS2lCLG9CQUFMLEdBQTRCLENBQUNMLFFBQUQsQ0FBNUI7QUFDQSxXQUFLTSwyQkFBTCxHQUFtQzJHLGFBQW5DO0FBQ0EsV0FBSzFHLG9CQUFMLEdBQTRCLENBQUN5RyxRQUFELENBQTVCOztBQUVBLFdBQUs3QyxTQUFMLENBQWU4QyxhQUFmOztBQUVBM0IsTUFBRSx5QkFBRixFQUE2QkMsS0FBN0IsQ0FBbUMsTUFBbkM7QUFDQSxJQVhGLEVBWUU5QixLQVpGLENBWVE7QUFBQSxXQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLElBWlI7QUFhQSxHQTdLTztBQStLUk0sMEJBL0tRLG9DQStLaUI5RSxJQS9LakIsRUErS3VCO0FBQUE7O0FBQzlCLE9BQUlLLFVBQVUsS0FBS0EsT0FBbkI7O0FBRUEsT0FBSUcsY0FBY1IsS0FBS2tGLEdBQUwsQ0FBUyxrQkFBVTtBQUNwQyxRQUFJQyxPQUFPLENBQVg7QUFDQSxRQUFJQyxTQUFTLENBQWI7QUFDQSxRQUFJQyxNQUFNLENBQVY7O0FBRUFDLFdBQU9DLFFBQVAsQ0FBZ0JMLEdBQWhCLENBQW9CLG1CQUFXO0FBQzlCLFNBQUdNLFFBQVFDLE1BQVIsSUFBa0IsQ0FBbEIsSUFBdUJELFFBQVFFLFFBQVIsSUFBb0JyRixPQUE5QyxFQUF1RDtBQUN0RDhFLGNBQVFRLFdBQVdILFFBQVFMLElBQW5CLENBQVI7QUFDQUMsZ0JBQVVPLFdBQVdILFFBQVFKLE1BQW5CLENBQVY7QUFDQUMsYUFBT00sV0FBV0gsUUFBUUgsR0FBbkIsQ0FBUDtBQUNBO0FBQ0QsS0FORDs7QUFRQSxXQUFPLE9BQUs4QixjQUFMLENBQW9CaEMsT0FBT0MsTUFBUCxHQUFnQkMsR0FBcEMsQ0FBUDtBQUNBLElBZGlCLENBQWxCOztBQWdCQWEsS0FBRSxrQkFBRixFQUFzQlksU0FBdEIsR0FBa0NDLE9BQWxDOztBQUVBLE9BQUllLEtBQUssSUFBVDs7QUFFQXBCLGNBQVcsWUFBVzs7QUFFckIsUUFBSXFCLFNBQVMsVUFBU0MsQ0FBVCxFQUFZO0FBQ3hCLFNBQUlDLE1BQU0sSUFBVjtBQUNBLFNBQUlDLFFBQVEsbUVBQ1QsU0FEUyxHQUVhLE1BRmIsR0FHYyxrQkFIZCxHQUljLHdCQUpkLEdBS2Msa0JBTGQsR0FNYyxpQkFOZCxHQU9jLGVBUGQsR0FRYyxzQkFSZCxHQVNjLHlCQVRkLEdBVWMsbUJBVmQsR0FXYyxxQ0FYZCxHQVlZLE9BWlosR0FhUyxVQWJULEdBY1MsU0FkckI7O0FBZ0JxQixVQUFJLElBQUlDLElBQUUsQ0FBVixFQUFhQSxJQUFHSCxFQUFFekMsUUFBSCxDQUFhNkMsTUFBNUIsRUFBb0NELEdBQXBDLEVBQXlDO0FBQ3hDRixZQUFNRCxFQUFFSyxNQUFGLENBQVNDLGdCQUFmO0FBQ0EsVUFBSVgsa0JBQWtCWSxLQUFLQyxLQUFMLENBQVdELEtBQUtFLFNBQUwsQ0FBZVQsRUFBRXpDLFFBQUYsQ0FBVzRDLENBQVgsQ0FBZixDQUFYLEVBQTBDbkQsRUFBaEU7O0FBRUEsVUFBSTZCLFlBQWFtQixFQUFFekMsUUFBRixDQUFXNEMsQ0FBWCxFQUFjMUMsTUFBZCxJQUF3QixDQUF6QixHQUE4QixlQUE5QixHQUFnRCxFQUFoRTtBQUNBLFVBQUlpRCxhQUFjVixFQUFFekMsUUFBRixDQUFXNEMsQ0FBWCxFQUFjekMsUUFBZCxJQUEwQnJGLE9BQTNCLEdBQXNDLFdBQXRDLEdBQW9ELE1BQXJFO0FBQ0EsVUFBRzJILEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNRLE9BQWpCLEVBQTBCO0FBQ3pCLFdBQUlBLFVBQVVYLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNRLE9BQTVCO0FBQ0EsV0FBSXJJLFVBQVUsa0dBQWdHcUksT0FBaEcsR0FBd0cseUJBQXhHLEdBQWtJWCxFQUFFekMsUUFBRixDQUFXNEMsQ0FBWCxFQUFjUyxNQUFoSixHQUF1SixNQUFySztBQUNBLE9BSEQsTUFHTztBQUNOLFdBQUl0SSxVQUFVMEgsRUFBRXpDLFFBQUYsQ0FBVzRDLENBQVgsRUFBY1MsTUFBNUI7QUFDQTtBQUNELFVBQUdaLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNVLFNBQWQsQ0FBd0JULE1BQXhCLEdBQWlDLENBQXBDLEVBQXVDO0FBQ3RDLFdBQUdKLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNVLFNBQWQsQ0FBd0IsQ0FBeEIsRUFBMkJDLE1BQTlCLEVBQXNDO0FBQ3JDLFlBQUlBLFNBQVNkLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNVLFNBQWQsQ0FBd0IsQ0FBeEIsRUFBMkJDLE1BQXhDO0FBQ0EsWUFBSUMsV0FBVyxpR0FBK0ZELE1BQS9GLEdBQXNHLHlCQUF0RyxHQUFnSWQsRUFBRXpDLFFBQUYsQ0FBVzRDLENBQVgsRUFBY1UsU0FBZCxDQUF3QixDQUF4QixFQUEyQkcsZUFBM0osR0FBMkssTUFBMUw7QUFDQSxRQUhELE1BR087QUFDTixZQUFJRCxXQUFXZixFQUFFekMsUUFBRixDQUFXNEMsQ0FBWCxFQUFjVSxTQUFkLENBQXdCLENBQXhCLEVBQTJCRyxlQUExQztBQUNBO0FBQ0QsT0FQRCxNQU9PO0FBQ04sV0FBSUQsV0FBVyxRQUFmO0FBQ0E7QUFDRCxVQUFJRSxVQUFVLDBJQUF3SWpCLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNlLFNBQXRKLEdBQWdLLDJCQUFoSyxHQUE0TGxCLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNnQixVQUExTSxHQUFxTixvQkFBck4sR0FBME9uQixFQUFFekMsUUFBRixDQUFXNEMsQ0FBWCxFQUFjbkQsRUFBeFAsR0FBMlAsMkRBQTNQLEdBQXVUZ0QsRUFBRXpDLFFBQUYsQ0FBVzRDLENBQVgsRUFBY2UsU0FBclUsR0FBK1UsMkJBQS9VLEdBQTJXbEIsRUFBRXpDLFFBQUYsQ0FBVzRDLENBQVgsRUFBY2dCLFVBQXpYLEdBQW9ZLG9CQUFwWSxHQUF5Wm5CLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNuRCxFQUF2YSxHQUEwYSw0RkFBMWEsR0FBdWdCZ0QsRUFBRXpDLFFBQUYsQ0FBVzRDLENBQVgsRUFBY2UsU0FBcmhCLEdBQStoQixtQkFBL2hCLEdBQW1qQmxCLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNQLFFBQWprQixHQUEwa0IsMEJBQTFrQixHQUFxbUJELGVBQXJtQixHQUFxbkIsNkNBQXJuQixHQUFtcUJLLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNlLFNBQWpyQixHQUEyckIsbUJBQTNyQixHQUErc0JsQixFQUFFekMsUUFBRixDQUFXNEMsQ0FBWCxFQUFjUCxRQUE3dEIsR0FBc3VCLDBCQUF0dUIsR0FBaXdCRCxlQUFqd0IsR0FBaXhCLFlBQS94Qjs7QUFFQU8sZUFBUyxnQkFBY3JCLFNBQWQsR0FBd0IsbUJBQXhCLEdBQTRDNkIsVUFBNUMsR0FBdUQsSUFBaEU7QUFDQ1IsZUFBUyxTQUFPRixFQUFFekMsUUFBRixDQUFXNEMsQ0FBWCxFQUFjUCxRQUFyQixHQUE4QixPQUF2QztBQUNBTSxlQUFTLFNBQU9GLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNpQixZQUFyQixHQUFrQyxPQUEzQztBQUNBbEIsZUFBUyxTQUFPNUgsT0FBUCxHQUFlLE9BQXhCO0FBQ0E0SCxlQUFTLFNBQU9GLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWNrQixNQUFyQixHQUE0QixPQUFyQztBQUNBbkIsZUFBUyxTQUFPRixFQUFFekMsUUFBRixDQUFXNEMsQ0FBWCxFQUFjaEQsSUFBckIsR0FBMEIsT0FBbkM7QUFDQStDLGVBQVMsU0FBT0YsRUFBRXpDLFFBQUYsQ0FBVzRDLENBQVgsRUFBYy9DLE1BQXJCLEdBQTRCLE9BQXJDO0FBQ0E4QyxlQUFTLFNBQU9GLEVBQUV6QyxRQUFGLENBQVc0QyxDQUFYLEVBQWM5QyxHQUFyQixHQUF5QixPQUFsQztBQUNBNkMsZUFBUyxTQUFPYSxRQUFQLEdBQWdCLE9BQXpCO0FBQ0FiLGVBQVMsNkJBQTJCZSxPQUEzQixHQUFtQyxPQUE1Qzs7QUFFRGYsZUFBUyxPQUFUO0FBRUE7O0FBRVRBLGNBQVMsa0JBQVQ7O0FBRUosU0FBSW9CLE1BQU1yQixHQUFWO0FBQ1QsU0FBSXNCLFNBQVMsMENBQWI7QUFDUyxVQUFJLElBQUlwQixJQUFFLENBQVYsRUFBYUEsSUFBRW1CLElBQUlsQixNQUFuQixFQUEyQkQsR0FBM0IsRUFBZ0M7QUFDeENvQixlQUFTQSxTQUFRLDBEQUFSLEdBQW1FRCxJQUFJbkIsQ0FBSixDQUFuRSxHQUEwRSxXQUFuRjtBQUNBO0FBQ2NvQixjQUFTQSxTQUFRLE9BQWpCOztBQUVOLFlBQU9BLFNBQU9yQixLQUFkO0FBQ1QsS0FuRVksQ0FtRVhzQixJQW5FVyxDQW1FTixJQW5FTSxDQUFiOztBQXFFTTtBQUNMdEIsWUFBUWhDLEVBQUUsa0JBQUYsRUFBc0JZLFNBQXRCLENBQWlDO0FBQ2xDLGFBQVE5RyxJQUQwQjtBQUVsQyxnQkFBVyxDQUNQO0FBQ0MsZUFBUyxJQURWO0FBRUksbUJBQWtCLDZCQUZ0QjtBQUdJLG1CQUFrQixLQUh0QjtBQUlJLGNBQWtCLElBSnRCO0FBS0ksd0JBQWtCO0FBTHRCLE1BRE8sRUFRUDtBQUNDLGVBQVMsS0FEVjtBQUVDQSxZQUFNLFFBRlAsRUFFaUJ5SixRQUFRLGdCQUFTekosSUFBVCxFQUFlYSxJQUFmLEVBQXFCNkksR0FBckIsRUFBMEI7QUFDOUMsY0FBTywyQkFBMkIxSixLQUFLZ0YsRUFBaEMsR0FBcUMsb0JBQXJDLEdBQTREaEYsS0FBSzJKLFNBQWpFLEdBQTZFLE1BQXBGO0FBQ0g7QUFKRixNQVJPLEVBY1Y7QUFDSSxlQUFTLEtBRGI7QUFFQzNKLFlBQU0sUUFGUCxFQUVpQnlKLFFBQVEsZ0JBQVN6SixJQUFULEVBQWVhLElBQWYsRUFBcUI2SSxHQUFyQixFQUEwQjtBQUNqRCxjQUFPMUosS0FBS2dGLEVBQVo7QUFDQTtBQUpGLE1BZFUsRUFvQlY7QUFDSSxlQUFTLEtBRGI7QUFFQ2hGLFlBQU0sSUFGUCxFQUVheUosUUFBUSxnQkFBU3pKLElBQVQsRUFBZWEsSUFBZixFQUFxQjZJLEdBQXJCLEVBQTBCRSxJQUExQixFQUFnQztBQUNuRCxjQUFPcEosWUFBWW9KLEtBQUtGLEdBQWpCLENBQVA7QUFDQTtBQUpGLE1BcEJVLEVBMEJWO0FBQ0MxSixZQUFNLElBRFAsRUFDYXlKLFFBQVEsZ0JBQVN6SixJQUFULEVBQWVhLElBQWYsRUFBcUI2SSxHQUFyQixFQUEwQjtBQUN2QyxXQUFHMUosS0FBSzZKLE1BQUwsSUFBZSxDQUFsQixFQUFxQjtBQUNwQixlQUFPLGdGQUFQO0FBQ0EsUUFGRCxNQUdLLElBQUc3SixLQUFLNkosTUFBTCxJQUFlLENBQWxCLEVBQXFCO0FBQzVCLGVBQU8sNEVBQTBFN0osS0FBS3FJLE1BQUwsQ0FBWXJELEVBQXRGLEdBQXlGLHFEQUFoRztBQUNHLFFBRkksTUFHQztBQUNMLGVBQU8sNEVBQTBFaEYsS0FBS3FJLE1BQUwsQ0FBWXJELEVBQXRGLEdBQXlGLCtEQUFoRztBQUNBO0FBQ0osT0FYTCxFQVdPNkIsV0FBVztBQVhsQixNQTFCVSxFQXVDVjtBQUNDN0csWUFBTSxJQURQLEVBQ2F5SixRQUFRLGdCQUFTekosSUFBVCxFQUFlYSxJQUFmLEVBQXFCNkksR0FBckIsRUFBMEI7QUFDN0MsY0FBTyxxSEFBbUgxSixLQUFLcUksTUFBTCxDQUFZckQsRUFBL0gsR0FBa0ksZ0JBQXpJO0FBQ0csT0FITCxFQUdPNkIsV0FBVztBQUhsQixNQXZDVSxFQTRDVjtBQUNDN0csWUFBTSxJQURQLEVBQ2F5SixRQUFRLGdCQUFTekosSUFBVCxFQUFlYSxJQUFmLEVBQXFCNkksR0FBckIsRUFBMEI7QUFDN0MsY0FBTyx3SEFBc0gxSixLQUFLcUksTUFBTCxDQUFZckQsRUFBbEksR0FBcUksY0FBNUk7QUFDRyxPQUhMLEVBR082QixXQUFXO0FBSGxCLE1BNUNVO0FBRnVCLEtBQWpDLENBQVI7QUFxREs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBSURYLE1BQUUsTUFBRixFQUFVNEQsR0FBVixDQUFjLE9BQWQsRUFBdUIsMkNBQXZCO0FBQ0FwRCxlQUFXLFlBQVc7QUFDckJSLE9BQUUsTUFBRixFQUFVNkQsRUFBVixDQUFhLE9BQWIsRUFBc0IsMkNBQXRCLEVBQW1FLFVBQVV4RCxDQUFWLEVBQWE7QUFDL0UsVUFBSXlELEtBQUs5RCxFQUFFLElBQUYsRUFBUStELE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBVDtBQUNBLFVBQUlQLE1BQU14QixNQUFNd0IsR0FBTixDQUFXTSxFQUFYLENBQVY7O0FBRUEsVUFBR04sSUFBSVEsS0FBSixDQUFVQyxPQUFWLEVBQUgsRUFBd0I7QUFDdkJULFdBQUlRLEtBQUosQ0FBVUUsSUFBVjtBQUNBSixVQUFHSyxXQUFILENBQWUsT0FBZjtBQUNBLE9BSEQsTUFHTztBQUNOWCxXQUFJUSxLQUFKLENBQVduQyxPQUFPMkIsSUFBSTFKLElBQUosRUFBUCxDQUFYLEVBQWdDc0ssSUFBaEM7QUFDQU4sVUFBR08sUUFBSCxDQUFZLE9BQVo7QUFDQTtBQUNELE1BWEQ7QUFZQSxLQWJVLENBYVRmLElBYlMsQ0FhSixJQWJJLENBQVgsRUFhYyxHQWJkOztBQWVBO0FBQ0F0RCxNQUFFLE1BQUYsRUFBVTZELEVBQVYsQ0FBYSxPQUFiLEVBQXNCLDZDQUF0QixFQUFxRSxVQUFVeEQsQ0FBVixFQUFhO0FBQ2pGLFNBQUkzRixXQUFXMkYsRUFBRUMsTUFBRixDQUFTZ0UsWUFBVCxDQUFzQixTQUF0QixDQUFmOztBQUVBLFVBQUsxRSxZQUFMLENBQWtCbEYsUUFBbEIsRUFBNEIsTUFBNUI7QUFDQSxLQUpvRSxDQUluRTRJLElBSm1FLENBSTlELElBSjhELENBQXJFOztBQU1BO0FBQ0F0RCxNQUFFLE1BQUYsRUFBVTZELEVBQVYsQ0FBYSxPQUFiLEVBQXNCLDZDQUF0QixFQUFxRSxVQUFVeEQsQ0FBVixFQUFhO0FBQ2pGTCxPQUFFLG9CQUFGLEVBQXdCQyxLQUF4QixDQUE4QixNQUE5QjtBQUNBLFNBQUl2RixXQUFXMkYsRUFBRUMsTUFBRixDQUFTZ0UsWUFBVCxDQUFzQixTQUF0QixDQUFmO0FBQ0EsVUFBSy9KLFdBQUwsR0FBbUJHLFFBQW5CO0FBQ0EsS0FKb0UsQ0FJbkU0SSxJQUptRSxDQUk5RCxJQUo4RCxDQUFyRTs7QUFNQXRELE1BQUUsTUFBRixFQUFVNkQsRUFBVixDQUFhLE9BQWIsRUFBc0IsMENBQXRCLEVBQWtFLFVBQVV4RCxDQUFWLEVBQWE7QUFDOUUsU0FBSTNGLFdBQVcyRixFQUFFQyxNQUFGLENBQVNnRSxZQUFULENBQXNCLFNBQXRCLENBQWY7O0FBRUEsVUFBSzlGLEtBQUwsQ0FBVytGLFdBQVgsQ0FBdUJDLFdBQXZCLENBQW1DOUosUUFBbkM7QUFDQSxLQUppRSxDQUloRTRJLElBSmdFLENBSTNELElBSjJELENBQWxFOztBQU1BdEQsTUFBRSxNQUFGLEVBQVU2RCxFQUFWLENBQWEsT0FBYixFQUFzQix3Q0FBdEIsRUFBZ0UsVUFBVXhELENBQVYsRUFBYTtBQUM1RSxTQUFJM0YsV0FBVzJGLEVBQUVDLE1BQUYsQ0FBU2dFLFlBQVQsQ0FBc0IsU0FBdEIsQ0FBZjs7QUFFQSxVQUFLOUYsS0FBTCxDQUFXaUcsZUFBWCxDQUEyQkQsV0FBM0IsQ0FBdUM5SixRQUF2QztBQUNBLEtBSitELENBSTlENEksSUFKOEQsQ0FJekQsSUFKeUQsQ0FBaEU7O0FBTUF0RCxNQUFFLE1BQUYsRUFBVTZELEVBQVYsQ0FBYSxPQUFiLEVBQXNCLDZDQUF0QixFQUFxRSxVQUFVeEQsQ0FBVixFQUFhO0FBQ2pGLFNBQUlxRSxrQkFBa0JyRSxFQUFFQyxNQUFGLENBQVNnRSxZQUFULENBQXNCLHNCQUF0QixDQUF0QjtBQUNBLFNBQUlLLG1CQUFtQnRFLEVBQUVDLE1BQUYsQ0FBU2dFLFlBQVQsQ0FBc0IsdUJBQXRCLENBQXZCO0FBQ0EsU0FBSU0sWUFBWXZFLEVBQUVDLE1BQUYsQ0FBU2dFLFlBQVQsQ0FBc0IsZ0JBQXRCLENBQWhCOztBQUVBLFVBQUs5RixLQUFMLENBQVdxRyxjQUFYLENBQTBCdEgsSUFBMUIsQ0FBK0JtSCxlQUEvQixFQUFnREMsZ0JBQWhELEVBQWtFQyxTQUFsRTtBQUNBLEtBTm9FLENBTW5FdEIsSUFObUUsQ0FNOUQsSUFOOEQsQ0FBckU7O0FBUUF0RCxNQUFFLE1BQUYsRUFBVTZELEVBQVYsQ0FBYSxPQUFiLEVBQXNCLDZDQUF0QixFQUFxRSxVQUFVeEQsQ0FBVixFQUFhO0FBQ2pGLFNBQUkzRixXQUFXMkYsRUFBRUMsTUFBRixDQUFTZ0UsWUFBVCxDQUFzQixlQUF0QixDQUFmO0FBQ0EsU0FBSTdDLGtCQUFrQnBCLEVBQUVDLE1BQUYsQ0FBU2dFLFlBQVQsQ0FBc0Isc0JBQXRCLENBQXRCO0FBQ0EsU0FBSTVDLFdBQVdyQixFQUFFQyxNQUFGLENBQVNnRSxZQUFULENBQXNCLGVBQXRCLENBQWY7O0FBRUEsVUFBSzlDLGlCQUFMLENBQXVCOUcsUUFBdkIsRUFBaUMrRyxlQUFqQyxFQUFrREMsUUFBbEQ7QUFDQSxLQU5vRSxDQU1uRTRCLElBTm1FLENBTTlELElBTjhELENBQXJFO0FBUUEsSUF2ZVUsQ0F1ZVRBLElBdmVTLENBdWVKLElBdmVJLENBQVgsRUF1ZWMsSUF2ZWQ7QUF5ZUEsR0EvcUJPO0FBaXJCUndCLGtCQWpyQlEsNEJBaXJCU25LLElBanJCVCxFQWlyQmM7QUFDckIsT0FBSUQsV0FBVyxLQUFLSCxXQUFwQjtBQUNBLFFBQUtxRixZQUFMLENBQWtCbEYsUUFBbEIsRUFBNEJDLElBQTVCO0FBQ0EsR0FwckJPO0FBc3JCUitFLGdDQXRyQlEsMENBc3JCdUJxRixJQXRyQnZCLEVBc3JCNkI7QUFDcEMsT0FBSTVLLFVBQVUsS0FBS0EsT0FBbkI7O0FBRUE2RixLQUFFLG1CQUFGLEVBQXVCWSxTQUF2QixHQUFtQ0MsT0FBbkM7O0FBRUEsT0FBSWUsS0FBSyxJQUFUOztBQUVBcEIsY0FBVyxZQUFXOztBQUVyQixRQUFJd0Usa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFTbEQsQ0FBVCxFQUFZO0FBQ2pDLFNBQUltRCxNQUFNakYsRUFBRSxRQUFGLEVBQ0ZxRSxRQURFLENBQ1EsU0FEUixFQUVGYSxJQUZFLENBRUksWUFGSixDQUFWO0FBR0RySyxnQkFBV3NLLElBQVgsQ0FBZ0IsaUJBQWlCaEwsT0FBakIsR0FBMkIsb0JBQTNDLEVBQWdFO0FBQy9EOEksa0JBQWFuQixFQUFFbUI7QUFEZ0QsTUFBaEUsRUFHQ2hGLElBSEQsQ0FHTSxvQkFBWTtBQUNaLFVBQUkrRCxRQUFRLG1FQUNkLFNBRGMsR0FFUSxNQUZSLEdBR1Msa0JBSFQsR0FJUyx3QkFKVCxHQUtTLGtCQUxULEdBTVMsaUJBTlQsR0FPUyxlQVBULEdBUVMsc0JBUlQsR0FTUyx5QkFUVCxHQVVTLG1CQVZULEdBV1MscUNBWFQsR0FZTyxPQVpQLEdBYUksVUFiSixHQWNJLFNBZGhCO0FBZWdCLFdBQUksSUFBSW9ELElBQUUsQ0FBVixFQUFhQSxJQUFHbEgsU0FBU3BFLElBQVYsQ0FBZ0JvSSxNQUEvQixFQUF1Q2tELEdBQXZDLEVBQTRDO0FBQzNDO0FBQ0E7O0FBRUFwRCxnQkFBUyxNQUFUO0FBQ0FBLGdCQUFTLHNGQUFvRjlELFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCQyxVQUFyRyxHQUFnSCxHQUFoSCxHQUFvSG5ILFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCRSxTQUFySSxHQUErSSxXQUF4SjtBQUNBdEQsZ0JBQVMsT0FBVDs7QUFFRCxZQUFJLElBQUlDLElBQUUsQ0FBVixFQUFhQSxJQUFHL0QsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFsQixDQUE0QjZDLE1BQTNDLEVBQW1ERCxHQUFuRCxFQUF3RDtBQUN2RCxZQUFJdEIsWUFBYXpDLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QjFDLE1BQTdCLElBQXVDLENBQXhDLEdBQTZDLGVBQTdDLEdBQStELEVBQS9FO0FBQ0EsWUFBSWlELGFBQWN0RSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJ6QyxRQUE3QixJQUF5Q3JGLE9BQTFDLEdBQXFELFdBQXJELEdBQW1FLE1BQXBGO0FBQ0EsWUFBRytELFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QlEsT0FBaEMsRUFBeUM7QUFDeEMsYUFBSUEsVUFBVXZFLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QlEsT0FBM0M7QUFDQSxhQUFJckksVUFBVSxrR0FBZ0dxSSxPQUFoRyxHQUF3Ryx5QkFBeEcsR0FBa0l2RSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJTLE1BQS9KLEdBQXNLLE1BQXBMO0FBQ0EsU0FIRCxNQUdPO0FBQ04sYUFBSXRJLFVBQVU4RCxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJTLE1BQTNDO0FBQ0E7QUFDRCxZQUFHeEUsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFqQixDQUEwQjRDLENBQTFCLEVBQTZCVSxTQUE3QixDQUF1Q1QsTUFBdkMsR0FBZ0QsQ0FBbkQsRUFBc0Q7QUFDckQsYUFBR2hFLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QlUsU0FBN0IsQ0FBdUMsQ0FBdkMsRUFBMENDLE1BQTdDLEVBQXFEO0FBQ3BELGNBQUlBLFNBQVMxRSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJVLFNBQTdCLENBQXVDLENBQXZDLEVBQTBDQyxNQUF2RDtBQUNBLGNBQUlDLFdBQVcsaUdBQStGRCxNQUEvRixHQUFzRyx5QkFBdEcsR0FBZ0kxRSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJVLFNBQTdCLENBQXVDLENBQXZDLEVBQTBDRyxlQUExSyxHQUEwTCxNQUF6TTtBQUNBLFVBSEQsTUFHTztBQUNOLGNBQUlELFdBQVczRSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJVLFNBQTdCLENBQXVDLENBQXZDLEVBQTBDRyxlQUF6RDtBQUNBO0FBQ0QsU0FQRCxNQU9PO0FBQ04sYUFBSUQsV0FBVyxRQUFmO0FBQ0E7QUFDRCxZQUFJRSxVQUFVLDBJQUF3STdFLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QmUsU0FBckssR0FBK0ssMkJBQS9LLEdBQTJNOUUsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFqQixDQUEwQjRDLENBQTFCLEVBQTZCZ0IsVUFBeE8sR0FBbVAsb0JBQW5QLEdBQXdRL0UsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFqQixDQUEwQjRDLENBQTFCLEVBQTZCbkQsRUFBclMsR0FBd1MsMkRBQXhTLEdBQW9XWixTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJlLFNBQWpZLEdBQTJZLDJCQUEzWSxHQUF1YTlFLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QmdCLFVBQXBjLEdBQStjLG9CQUEvYyxHQUFvZS9FLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2Qm5ELEVBQWpnQixHQUFvZ0IsWUFBbGhCOztBQUVBa0QsaUJBQVMsZ0JBQWNyQixTQUFkLEdBQXdCLG1CQUF4QixHQUE0QzZCLFVBQTVDLEdBQXVELElBQWhFO0FBQ0NSLGlCQUFTLFNBQU85RCxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJQLFFBQXBDLEdBQTZDLE9BQXREO0FBQ0FNLGlCQUFTLFNBQU85RCxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJpQixZQUFwQyxHQUFpRCxPQUExRDtBQUNBbEIsaUJBQVMsU0FBTzVILE9BQVAsR0FBZSxPQUF4QjtBQUNBNEgsaUJBQVMsU0FBTzlELFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QmtCLE1BQXBDLEdBQTJDLE9BQXBEO0FBQ0FuQixpQkFBUyxTQUFPOUQsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFqQixDQUEwQjRDLENBQTFCLEVBQTZCaEQsSUFBcEMsR0FBeUMsT0FBbEQ7QUFDQStDLGlCQUFTLFNBQU85RCxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkIvQyxNQUFwQyxHQUEyQyxPQUFwRDtBQUNBOEMsaUJBQVMsU0FBTzlELFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QjlDLEdBQXBDLEdBQXdDLE9BQWpEO0FBQ0E2QyxpQkFBUyxTQUFPYSxRQUFQLEdBQWdCLE9BQXpCO0FBQ0FiLGlCQUFTLDZCQUEyQmUsT0FBM0IsR0FBbUMsT0FBNUM7QUFDRGYsaUJBQVMsT0FBVDtBQUVBO0FBQ0o7QUFDTEEsZUFBUyxrQkFBVDs7QUFFSjs7QUFFVGlELFVBQUlNLElBQUosQ0FBVXZELEtBQVYsRUFBa0JtQyxXQUFsQixDQUErQixTQUEvQjtBQUNBLE1BbkVELEVBb0VDaEcsS0FwRUQsQ0FvRU87QUFBQSxhQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLE1BcEVQOztBQXNFSSxZQUFPMkcsR0FBUDtBQUVILEtBNUVEOztBQThFQU8sYUFBU3hGLEVBQUUsbUJBQUYsRUFBdUJZLFNBQXZCLENBQWtDO0FBQ3BDLGFBQVFtRSxJQUQ0QjtBQUVwQyxnQkFBVyxDQUNQO0FBQ0MsZUFBUyxJQURWO0FBRUksbUJBQWtCLDZCQUZ0QjtBQUdJLG1CQUFrQixLQUh0QjtBQUlJLGNBQWtCLElBSnRCO0FBS0ksd0JBQWtCO0FBTHRCLE1BRE8sRUFRUDtBQUNDLGVBQVMsS0FEVjtBQUVDakwsWUFBTSxJQUZQLEVBRWF5SixRQUFRLGdCQUFTd0IsSUFBVCxFQUFlcEssSUFBZixFQUFxQjZJLEdBQXJCLEVBQTBCO0FBQzFDLGNBQU8sZ0NBQWdDdUIsS0FBS3JDLE1BQXJDLEdBQThDLE1BQXJEO0FBQ0g7QUFKRixNQVJPLEVBY1Y7QUFDSSxlQUFTLEtBRGI7QUFFQzVJLFlBQU0sSUFGUCxFQUVheUosUUFBUSxnQkFBU3dCLElBQVQsRUFBZXBLLElBQWYsRUFBcUI2SSxHQUFyQixFQUEwQjtBQUM3QyxjQUFPLFFBQU1pQyxPQUFPQyxPQUFPWCxLQUFLN0IsWUFBWixDQUFQLEVBQWtDckIsTUFBbEMsQ0FBeUMsYUFBekMsQ0FBTixHQUErRCxNQUF0RTtBQUNBO0FBSkYsTUFkVSxFQW9CVjtBQUNJLGVBQVMsS0FEYjtBQUVDL0gsWUFBTSxJQUZQLEVBRWF5SixRQUFRLGdCQUFTd0IsSUFBVCxFQUFlcEssSUFBZixFQUFxQjZJLEdBQXJCLEVBQTBCRSxJQUExQixFQUFnQztBQUNuRCxjQUFPLFFBQU1xQixLQUFLWSxTQUFYLEdBQXNCLE1BQTdCO0FBQ0E7QUFKRixNQXBCVTtBQUZ5QixLQUFsQyxDQUFUOztBQStCRzNGLE1BQUUsTUFBRixFQUFVNkQsRUFBVixDQUFhLE9BQWIsRUFBc0IsNENBQXRCLEVBQW9FLFVBQVV4RCxDQUFWLEVBQWE7QUFDbkYsU0FBSXVGLE1BQU01RixFQUFFLElBQUYsRUFBUStELE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBVjtBQUNBLFNBQUlQLE1BQU1nQyxPQUFPaEMsR0FBUCxDQUFZb0MsR0FBWixDQUFWOztBQUVBLFNBQUdwQyxJQUFJUSxLQUFKLENBQVVDLE9BQVYsRUFBSCxFQUF3QjtBQUN2QlQsVUFBSVEsS0FBSixDQUFVRSxJQUFWO0FBQ0EwQixVQUFJekIsV0FBSixDQUFnQixPQUFoQjtBQUNBLE1BSEQsTUFHTztBQUNOWCxVQUFJUSxLQUFKLENBQVdnQixnQkFBZ0J4QixJQUFJMUosSUFBSixFQUFoQixDQUFYLEVBQXlDc0ssSUFBekM7QUFDQXdCLFVBQUl2QixRQUFKLENBQWEsT0FBYjtBQUNBO0FBQ0QsS0FYRTs7QUFhSHJFLE1BQUUsTUFBRixFQUFVNkQsRUFBVixDQUFhLE9BQWIsRUFBc0IsOENBQXRCLEVBQXNFLFVBQVV4RCxDQUFWLEVBQWE7QUFDbEYsU0FBSXFFLGtCQUFrQnJFLEVBQUVDLE1BQUYsQ0FBU2dFLFlBQVQsQ0FBc0Isc0JBQXRCLENBQXRCO0FBQ0EsU0FBSUssbUJBQW1CdEUsRUFBRUMsTUFBRixDQUFTZ0UsWUFBVCxDQUFzQix1QkFBdEIsQ0FBdkI7QUFDQSxTQUFJTSxZQUFZdkUsRUFBRUMsTUFBRixDQUFTZ0UsWUFBVCxDQUFzQixnQkFBdEIsQ0FBaEI7O0FBRUEsVUFBSzlGLEtBQUwsQ0FBV3FHLGNBQVgsQ0FBMEJ0SCxJQUExQixDQUErQm1ILGVBQS9CLEVBQWdEQyxnQkFBaEQsRUFBa0VDLFNBQWxFO0FBQ0EsS0FOcUUsQ0FNcEV0QixJQU5vRSxDQU0vRCxJQU4rRCxDQUF0RTtBQVFBLElBcElVLENBb0lUQSxJQXBJUyxDQW9JSixJQXBJSSxDQUFYLEVBb0ljLElBcElkO0FBc0lBLEdBbjBCTztBQXMwQlIzRCw4QkF0MEJRLHdDQXMwQnFCb0YsSUF0MEJyQixFQXMwQjJCO0FBQ2xDLE9BQUk1SyxVQUFVLEtBQUtBLE9BQW5COztBQUVBNkYsS0FBRSxpQkFBRixFQUFxQlksU0FBckIsR0FBaUNDLE9BQWpDOztBQUVBLE9BQUllLEtBQUssSUFBVDs7QUFFQXBCLGNBQVcsWUFBVzs7QUFFckIsUUFBSXFGLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBUy9ELENBQVQsRUFBWTtBQUMvQixTQUFJbUQsTUFBTWpGLEVBQUUsUUFBRixFQUNGcUUsUUFERSxDQUNRLFNBRFIsRUFFRmEsSUFGRSxDQUVJLFlBRkosQ0FBVjtBQUdEckssZ0JBQVdzSyxJQUFYLENBQWdCLGlCQUFpQmhMLE9BQWpCLEdBQTJCLGtCQUEzQyxFQUE4RDtBQUM3RDJMLFlBQU9oRSxFQUFFb0I7QUFEb0QsTUFBOUQsRUFHQ2pGLElBSEQsQ0FHTSxvQkFBWTtBQUNaLFVBQUkrRCxRQUFRLG1FQUNkLFNBRGMsR0FFUSxNQUZSLEdBR1Msa0JBSFQsR0FJUyx3QkFKVCxHQUtTLGtCQUxULEdBTVMsaUJBTlQsR0FPUyxlQVBULEdBUVMsc0JBUlQsR0FTUyx5QkFUVCxHQVVTLG1CQVZULEdBV1MscUNBWFQsR0FZTyxPQVpQLEdBYUksVUFiSixHQWNJLFNBZGhCO0FBZWdCLFdBQUksSUFBSW9ELElBQUUsQ0FBVixFQUFhQSxJQUFHbEgsU0FBU3BFLElBQVYsQ0FBZ0JvSSxNQUEvQixFQUF1Q2tELEdBQXZDLEVBQTRDO0FBQzNDO0FBQ0E7O0FBRUFwRCxnQkFBUyxNQUFUO0FBQ0FBLGdCQUFTLHNGQUFvRjlELFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCQyxVQUFyRyxHQUFnSCxHQUFoSCxHQUFvSG5ILFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCRSxTQUFySSxHQUErSSxXQUF4SjtBQUNBdEQsZ0JBQVMsT0FBVDs7QUFFRCxZQUFJLElBQUlDLElBQUUsQ0FBVixFQUFhQSxJQUFHL0QsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFsQixDQUE0QjZDLE1BQTNDLEVBQW1ERCxHQUFuRCxFQUF3RDtBQUN2RCxZQUFJdEIsWUFBYXpDLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QjFDLE1BQTdCLElBQXVDLENBQXhDLEdBQTZDLGVBQTdDLEdBQStELEVBQS9FO0FBQ0EsWUFBSWlELGFBQWN0RSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJ6QyxRQUE3QixJQUF5Q3JGLE9BQTFDLEdBQXFELFdBQXJELEdBQW1FLE1BQXBGO0FBQ0EsWUFBRytELFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QlEsT0FBaEMsRUFBeUM7QUFDeEMsYUFBSUEsVUFBVXZFLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QlEsT0FBM0M7QUFDQSxhQUFJckksVUFBVSxrR0FBZ0dxSSxPQUFoRyxHQUF3Ryx5QkFBeEcsR0FBa0l2RSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJTLE1BQS9KLEdBQXNLLE1BQXBMO0FBQ0EsU0FIRCxNQUdPO0FBQ04sYUFBSXRJLFVBQVU4RCxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJTLE1BQTNDO0FBQ0E7QUFDRCxZQUFHeEUsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFqQixDQUEwQjRDLENBQTFCLEVBQTZCVSxTQUE3QixDQUF1Q1QsTUFBdkMsR0FBZ0QsQ0FBbkQsRUFBc0Q7QUFDckQsYUFBR2hFLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QlUsU0FBN0IsQ0FBdUMsQ0FBdkMsRUFBMENDLE1BQTdDLEVBQXFEO0FBQ3BELGNBQUlBLFNBQVMxRSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJVLFNBQTdCLENBQXVDLENBQXZDLEVBQTBDQyxNQUF2RDtBQUNBLGNBQUlDLFdBQVcsaUdBQStGRCxNQUEvRixHQUFzRyx5QkFBdEcsR0FBZ0kxRSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJVLFNBQTdCLENBQXVDLENBQXZDLEVBQTBDRyxlQUExSyxHQUEwTCxNQUF6TTtBQUNBLFVBSEQsTUFHTztBQUNOLGNBQUlELFdBQVczRSxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJVLFNBQTdCLENBQXVDLENBQXZDLEVBQTBDRyxlQUF6RDtBQUNBO0FBQ0QsU0FQRCxNQU9PO0FBQ04sYUFBSUQsV0FBVyxRQUFmO0FBQ0E7QUFDRCxZQUFJRSxVQUFVLDBJQUF3STdFLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QmUsU0FBckssR0FBK0ssMkJBQS9LLEdBQTJNOUUsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFqQixDQUEwQjRDLENBQTFCLEVBQTZCZ0IsVUFBeE8sR0FBbVAsb0JBQW5QLEdBQXdRL0UsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFqQixDQUEwQjRDLENBQTFCLEVBQTZCbkQsRUFBclMsR0FBd1MsMkRBQXhTLEdBQW9XWixTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJlLFNBQWpZLEdBQTJZLDJCQUEzWSxHQUF1YTlFLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QmdCLFVBQXBjLEdBQStjLG9CQUEvYyxHQUFvZS9FLFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2Qm5ELEVBQWpnQixHQUFvZ0IsWUFBbGhCOztBQUVBa0QsaUJBQVMsZ0JBQWNyQixTQUFkLEdBQXdCLG1CQUF4QixHQUE0QzZCLFVBQTVDLEdBQXVELElBQWhFO0FBQ0NSLGlCQUFTLFNBQU85RCxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJQLFFBQXBDLEdBQTZDLE9BQXREO0FBQ0FNLGlCQUFTLFNBQU85RCxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkJpQixZQUFwQyxHQUFpRCxPQUExRDtBQUNBbEIsaUJBQVMsU0FBTzVILE9BQVAsR0FBZSxPQUF4QjtBQUNBNEgsaUJBQVMsU0FBTzlELFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QmtCLE1BQXBDLEdBQTJDLE9BQXBEO0FBQ0FuQixpQkFBUyxTQUFPOUQsU0FBU3BFLElBQVQsQ0FBY3NMLENBQWQsRUFBaUIvRixRQUFqQixDQUEwQjRDLENBQTFCLEVBQTZCaEQsSUFBcEMsR0FBeUMsT0FBbEQ7QUFDQStDLGlCQUFTLFNBQU85RCxTQUFTcEUsSUFBVCxDQUFjc0wsQ0FBZCxFQUFpQi9GLFFBQWpCLENBQTBCNEMsQ0FBMUIsRUFBNkIvQyxNQUFwQyxHQUEyQyxPQUFwRDtBQUNBOEMsaUJBQVMsU0FBTzlELFNBQVNwRSxJQUFULENBQWNzTCxDQUFkLEVBQWlCL0YsUUFBakIsQ0FBMEI0QyxDQUExQixFQUE2QjlDLEdBQXBDLEdBQXdDLE9BQWpEO0FBQ0E2QyxpQkFBUyxTQUFPYSxRQUFQLEdBQWdCLE9BQXpCO0FBQ0FiLGlCQUFTLDZCQUEyQmUsT0FBM0IsR0FBbUMsT0FBNUM7QUFDRGYsaUJBQVMsT0FBVDtBQUVBO0FBQ0o7QUFDTEEsZUFBUyxrQkFBVDs7QUFFSjs7QUFFVGlELFVBQUlNLElBQUosQ0FBVXZELEtBQVYsRUFBa0JtQyxXQUFsQixDQUErQixTQUEvQjtBQUNBLE1BbkVELEVBb0VDaEcsS0FwRUQsQ0FvRU87QUFBQSxhQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLE1BcEVQOztBQXNFSSxZQUFPMkcsR0FBUDtBQUVILEtBNUVEOztBQThFQWMsYUFBUy9GLEVBQUUsaUJBQUYsRUFBcUJZLFNBQXJCLENBQWdDO0FBQ2xDLGFBQVFtRSxJQUQwQjtBQUVsQyxnQkFBVyxDQUNQO0FBQ0MsZUFBUyxJQURWO0FBRUksbUJBQWtCLDJCQUZ0QjtBQUdJLG1CQUFrQixLQUh0QjtBQUlJLGNBQWtCLElBSnRCO0FBS0ksd0JBQWtCO0FBTHRCLE1BRE8sRUFRUDtBQUNDLGVBQVMsS0FEVjtBQUVGakwsWUFBTSxJQUZKLEVBRVV5SixRQUFRLGdCQUFTd0IsSUFBVCxFQUFlcEssSUFBZixFQUFxQjZJLEdBQXJCLEVBQTBCO0FBQzdDLGNBQU8sZ0NBQThCaUMsT0FBT0MsT0FBT1gsS0FBSzdCLFlBQVosQ0FBUCxFQUFrQ3JCLE1BQWxDLENBQXlDLGFBQXpDLENBQTlCLEdBQXVGLE1BQTlGO0FBQ0E7QUFKQyxNQVJPLEVBY1Y7QUFDSSxlQUFTLEtBRGI7QUFFQy9ILFlBQU0sSUFGUCxFQUVheUosUUFBUSxnQkFBU3dCLElBQVQsRUFBZXBLLElBQWYsRUFBcUI2SSxHQUFyQixFQUEwQkUsSUFBMUIsRUFBZ0M7QUFDbkQsY0FBTyxRQUFNcUIsS0FBS1ksU0FBWCxHQUFzQixNQUE3QjtBQUNBO0FBSkYsTUFkVTtBQUZ1QixLQUFoQyxDQUFUOztBQXlCRzNGLE1BQUUsTUFBRixFQUFVNkQsRUFBVixDQUFhLE9BQWIsRUFBc0Isd0NBQXRCLEVBQWdFLFVBQVV4RCxDQUFWLEVBQWE7QUFDL0UsU0FBSTJGLE1BQU1oRyxFQUFFLElBQUYsRUFBUStELE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBVjtBQUNBLFNBQUlQLE1BQU11QyxPQUFPdkMsR0FBUCxDQUFZd0MsR0FBWixDQUFWOztBQUVBLFNBQUd4QyxJQUFJUSxLQUFKLENBQVVDLE9BQVYsRUFBSCxFQUF3QjtBQUN2QlQsVUFBSVEsS0FBSixDQUFVRSxJQUFWO0FBQ0E4QixVQUFJN0IsV0FBSixDQUFnQixPQUFoQjtBQUNBLE1BSEQsTUFHTztBQUNOWCxVQUFJUSxLQUFKLENBQVc2QixjQUFjckMsSUFBSTFKLElBQUosRUFBZCxDQUFYLEVBQXVDc0ssSUFBdkM7QUFDQTRCLFVBQUkzQixRQUFKLENBQWEsT0FBYjtBQUNBO0FBQ0QsS0FYRTs7QUFhSHJFLE1BQUUsTUFBRixFQUFVNkQsRUFBVixDQUFhLE9BQWIsRUFBc0IsNENBQXRCLEVBQW9FLFVBQVV4RCxDQUFWLEVBQWE7QUFDaEYsU0FBSXFFLGtCQUFrQnJFLEVBQUVDLE1BQUYsQ0FBU2dFLFlBQVQsQ0FBc0Isc0JBQXRCLENBQXRCO0FBQ0EsU0FBSUssbUJBQW1CdEUsRUFBRUMsTUFBRixDQUFTZ0UsWUFBVCxDQUFzQix1QkFBdEIsQ0FBdkI7QUFDQSxTQUFJTSxZQUFZdkUsRUFBRUMsTUFBRixDQUFTZ0UsWUFBVCxDQUFzQixnQkFBdEIsQ0FBaEI7O0FBRUEsVUFBSzlGLEtBQUwsQ0FBV3FHLGNBQVgsQ0FBMEJ0SCxJQUExQixDQUErQm1ILGVBQS9CLEVBQWdEQyxnQkFBaEQsRUFBa0VDLFNBQWxFO0FBQ0EsS0FObUUsQ0FNbEV0QixJQU5rRSxDQU03RCxJQU42RCxDQUFwRTtBQVFBLElBOUhVLENBOEhUQSxJQTlIUyxDQThISixJQTlISSxDQUFYLEVBOEhjLElBOUhkO0FBZ0lBLEdBNzhCTztBQSs4QlIyQyxlQS84QlEsMkJBKzhCUTtBQUFBOztBQUNmLE9BQUk5TCxVQUFVLEtBQUtBLE9BQW5COztBQUVBVSxjQUFXbUQsR0FBWCxDQUFlLGlCQUFpQjdELE9BQWpCLEdBQTJCLFdBQTFDLEVBQ0U4RCxJQURGLENBQ08sb0JBQVk7QUFDakIsV0FBSy9CLFFBQUwsR0FBZ0JnQyxTQUFTcEUsSUFBekI7O0FBRUEsV0FBSzRFLGVBQUwsQ0FBcUIsYUFBckI7QUFDQSxJQUxGLEVBTUVQLEtBTkYsQ0FNUTtBQUFBLFdBQVNDLFFBQVFDLEdBQVIsQ0FBWUMsS0FBWixDQUFUO0FBQUEsSUFOUjtBQU9BLEdBejlCTztBQTI5QlI0SCxlQTM5QlEsMkJBMjlCUTtBQUFBOztBQUNmLE9BQUkvTCxVQUFVLEtBQUtBLE9BQW5COztBQUVBVSxjQUFXbUQsR0FBWCxDQUFlLGlCQUFpQjdELE9BQWpCLEdBQTJCLFdBQTFDLEVBQ0U4RCxJQURGLENBQ08sb0JBQVk7QUFDakIsV0FBSzlCLFFBQUwsR0FBZ0IrQixTQUFTcEUsSUFBekI7O0FBRUEsV0FBSzRFLGVBQUwsQ0FBcUIsYUFBckI7QUFDQSxJQUxGLEVBTUVQLEtBTkYsQ0FNUTtBQUFBLFdBQVNDLFFBQVFDLEdBQVIsQ0FBWUMsS0FBWixDQUFUO0FBQUEsSUFOUjtBQU9BLEdBcitCTztBQXUrQlI2SCxjQXYrQlEsMEJBdStCTztBQUFBOztBQUNkLE9BQUloTSxVQUFVLEtBQUtBLE9BQW5COztBQUVBVSxjQUFXbUQsR0FBWCxDQUFlLGlCQUFpQjdELE9BQWpCLEdBQTJCLFVBQTFDLEVBQ0U4RCxJQURGLENBQ08sb0JBQVk7QUFDakIsWUFBSzdCLE9BQUwsR0FBZThCLFNBQVNwRSxJQUF4Qjs7QUFFQSxZQUFLNEUsZUFBTCxDQUFxQixZQUFyQjtBQUNBLElBTEYsRUFNRVAsS0FORixDQU1RO0FBQUEsV0FBU0MsUUFBUUMsR0FBUixDQUFZQyxLQUFaLENBQVQ7QUFBQSxJQU5SO0FBT0EsR0FqL0JPO0FBbS9CUjhILGdCQW4vQlEsNEJBbS9CUztBQUFBOztBQUNoQixPQUFJak0sVUFBVSxLQUFLQSxPQUFuQjs7QUFFQVUsY0FBV21ELEdBQVgsQ0FBZSxpQkFBaUI3RCxPQUFqQixHQUEyQixZQUExQyxFQUNFOEQsSUFERixDQUNPLG9CQUFZO0FBQ2pCLFlBQUs1QixTQUFMLEdBQWlCNkIsU0FBU3BFLElBQTFCOztBQUVBLFlBQUs0RSxlQUFMLENBQXFCLGNBQXJCO0FBQ0EsSUFMRixFQU1FUCxLQU5GLENBTVE7QUFBQSxXQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLElBTlI7QUFPQSxHQTcvQk87QUErL0JSK0gsOEJBLy9CUSwwQ0ErL0J1QjtBQUM5QixRQUFLbkwsVUFBTCxHQUFrQixFQUFsQjtBQUNBLFFBQUtDLFlBQUwsR0FBb0IsSUFBcEI7QUFDQSxRQUFLQyxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsUUFBS0MsV0FBTCxHQUFtQixJQUFuQjtBQUNBLFFBQUtDLE1BQUwsR0FBYyxJQUFkO0FBQ0EsUUFBS0MsT0FBTCxHQUFlLElBQWY7QUFDQSxRQUFLQyxLQUFMLEdBQWEsSUFBYjtBQUNBLFFBQUtDLE9BQUwsR0FBZSxDQUFmO0FBQ0EsR0F4Z0NPO0FBMGdDUjZLLGlCQTFnQ1EsNkJBMGdDVTtBQUFBOztBQUNqQixRQUFLRCw0QkFBTDs7QUFFQSxPQUFJbE0sVUFBVSxLQUFLQSxPQUFuQjs7QUFFQVUsY0FBV21ELEdBQVgsQ0FBZSxpQkFBaUI3RCxPQUFqQixHQUEyQixjQUExQyxFQUNFOEQsSUFERixDQUNPLG9CQUFZO0FBQ2pCLFlBQUsvQyxVQUFMLEdBQWtCZ0QsU0FBU3BFLElBQTNCO0FBQ0EsSUFIRixFQUlFcUUsS0FKRixDQUlRO0FBQUEsV0FBU0MsUUFBUUMsR0FBUixDQUFZQyxLQUFaLENBQVQ7QUFBQSxJQUpSO0FBS0EsR0FwaENPO0FBc2hDUmlJLG1DQXRoQ1EsK0NBc2hDNEI7QUFDbkMsUUFBSzdLLGVBQUwsR0FBdUIsRUFBdkI7QUFDQSxRQUFLQyxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsUUFBS0MsY0FBTCxHQUFzQixJQUF0QjtBQUNBLFFBQUtDLFlBQUwsR0FBb0IsSUFBcEI7QUFDQSxRQUFLQyxPQUFMLEdBQWUsSUFBZjtBQUNBLFFBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxRQUFLQyxNQUFMLEdBQWMsSUFBZDtBQUNBLFFBQUtDLFFBQUwsR0FBZ0IsQ0FBaEI7QUFDQSxHQS9oQ087QUFpaUNSdUssc0JBamlDUSxrQ0FpaUNlO0FBQUE7O0FBQ3RCLFFBQUtELGlDQUFMOztBQUVBLE9BQUlwTSxVQUFVLEtBQUtBLE9BQW5COztBQUVBVSxjQUFXbUQsR0FBWCxDQUFlLGlCQUFpQjdELE9BQWpCLEdBQTJCLG1CQUExQyxFQUNFOEQsSUFERixDQUNPLG9CQUFZO0FBQ2pCLFlBQUt2QyxlQUFMLEdBQXVCd0MsU0FBU3BFLElBQWhDO0FBQ0EsSUFIRixFQUlFcUUsS0FKRixDQUlRO0FBQUEsV0FBU0MsUUFBUUMsR0FBUixDQUFZQyxLQUFaLENBQVQ7QUFBQSxJQUpSO0FBS0E7QUEzaUNPLEVBM0VGOztBQTBuQ1BtSSxRQTFuQ08scUJBMG5DRztBQUNULE9BQUtsSixJQUFMLEdBRFMsQ0FDSTtBQUNiLEVBNW5DTTtBQThuQ1BtSixRQTluQ08scUJBOG5DRztBQUNUO0FBQ0ExRyxJQUFFLE1BQUYsRUFBVTJHLE9BQVYsQ0FBa0IsRUFBRTtBQUNoQnBCLFNBQUssSUFEUztBQUVqQnFCLFlBQVMsT0FGUTtBQUdkQyxhQUFVO0FBSEksR0FBbEI7QUFLQSxFQXJvQ007OztBQXVvQ1BDLGFBQVk7QUFDWCxrQkFBZ0JqTixtQkFBT0EsQ0FBQyxHQUFSLENBREw7QUFFWCxvQkFBa0JBLG1CQUFPQSxDQUFDLEdBQVIsQ0FGUDtBQUdSLHFCQUFtQkEsbUJBQU9BLENBQUMsR0FBUixDQUhYO0FBSVIsa0JBQWdCQSxtQkFBT0EsQ0FBQyxHQUFSLENBSlI7QUFLUixjQUFZQSxtQkFBT0EsQ0FBQyxHQUFSLENBTEo7QUFNUixjQUFZQSxtQkFBT0EsQ0FBQyxHQUFSLENBTko7QUFPUixpQkFBZUEsbUJBQU9BLENBQUMsR0FBUixDQVBQO0FBUVIsbUJBQWlCQSxtQkFBT0EsQ0FBQyxHQUFSLENBUlQ7QUFTUiwyQkFBeUJBLG1CQUFPQSxDQUFDLEVBQVIsQ0FUakI7QUFVUixxQkFBbUJBLG1CQUFPQSxDQUFDLEdBQVI7QUFWWDs7QUF2b0NMLENBQVIsRTs7Ozs7OztBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixpQkFBaUI7QUFDakM7QUFDQTtBQUNBLHdDQUF3QyxnQkFBZ0I7QUFDeEQsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixpQkFBaUI7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9CQUFvQjtBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDakRBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEVBQW9UO0FBQzFVLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNlQTtBQUNBLDRCQURBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUZBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDa0JBOztBQUVBLG9CQUZBOztBQUlBLE1BSkEsa0JBSUE7QUFDQTtBQUNBO0FBQ0EseUJBREE7QUFFQSxrQkFGQTtBQUdBO0FBSEEsU0FJQSx3Q0FKQTtBQURBO0FBT0EsR0FaQTs7O0FBY0E7QUFDQSxvQkFEQSw4QkFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBTEE7QUFPQSxZQVBBLHNCQU9BO0FBQUE7O0FBQ0Esb0ZBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsT0FYQSxFQVlBLEtBWkEsQ0FZQTtBQUFBO0FBQUEsT0FaQTtBQWFBO0FBckJBOztBQWRBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1QkE7O0FBRUEsbUJBRkE7O0FBSUEsS0FKQSxrQkFJQTtBQUNBO0FBQ0EsY0FEQTs7QUFHQTtBQUNBO0FBREEsTUFFQSx3Q0FGQTtBQUhBO0FBT0EsRUFaQTs7O0FBY0E7QUFDQSxjQURBLHdCQUNBLENBREEsRUFDQTtBQUFBOztBQUNBO0FBQ0E7QUFDQSwwQkFEQTtBQUVBO0FBRkE7QUFEQSxNQU1BLElBTkEsQ0FNQTtBQUNBO0FBQ0E7QUFDQSw2QkFEQTtBQUVBLGlDQUZBO0FBR0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUpBLEVBSUEsSUFKQTtBQUtBLElBbEJBO0FBbUJBLEdBckJBO0FBdUJBLHVCQXZCQSxtQ0F1QkE7QUFDQTtBQUNBO0FBREEsTUFFQSx3Q0FGQTtBQUdBLEdBM0JBO0FBNkJBLGNBN0JBLDBCQTZCQTtBQUFBOztBQUNBO0FBQ0E7QUFDQSxJQUZBLE1BRUE7QUFDQSx5RkFDQSxJQURBLENBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FmQSxFQWdCQSxLQWhCQSxDQWdCQTtBQUFBO0FBQUEsS0FoQkE7QUFpQkE7QUFDQTtBQW5EQSxFQWRBOztBQW9FQSxRQXBFQSxxQkFvRUE7QUFBQTs7QUFDQTtBQUNBLGdCQURBO0FBRUEsMkNBRkE7QUFHQTtBQUhBOztBQU1BO0FBQ0E7O0FBRUE7QUFDQSxHQUpBOztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUxBO0FBTUE7QUF2RkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNzS0E7QUFDQSxtQkFEQTs7QUFHQSxLQUhBLGtCQUdBO0FBQ0E7QUFDQSxpQkFEQTs7QUFHQSxZQUhBOztBQUtBLG1CQUxBOztBQU9BLFdBUEE7O0FBU0EsY0FUQTs7QUFXQSxrQkFYQTtBQVlBLG1CQVpBO0FBYUEsbUJBYkE7O0FBZUE7QUFDQSxnQkFEQTtBQUVBLFlBRkE7QUFHQSxnQkFIQTtBQUlBLGNBSkE7QUFLQSxpQkFMQTs7QUFPQSxlQVBBO0FBUUEsZ0JBUkE7QUFTQTtBQVRBLE1BVUEsd0NBVkE7QUFmQTtBQTJCQSxFQS9CQTs7O0FBaUNBO0FBQ0Esa0JBREEsOEJBQ0E7QUFBQTs7QUFDQSwyQ0FDQSxJQURBLENBQ0E7QUFDQTtBQUFBO0FBQUE7O0FBRUE7QUFDQTtBQUNBLEtBRkEsRUFFQSxJQUZBO0FBSUEsSUFSQSxFQVNBLEtBVEEsQ0FTQTtBQUFBO0FBQUEsSUFUQTtBQVVBLEdBWkE7QUFjQSx3QkFkQSxvQ0FjQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBakNBO0FBbUNBLGVBbkNBLDJCQW1DQTtBQUFBOztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUhBOztBQUtBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFGQTs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQURBO0FBRUE7QUFGQTtBQUlBOztBQUVBO0FBQ0E7QUFDQSxNQUZBOztBQUlBO0FBQ0E7QUFDQTtBQUNBLEtBckJBO0FBc0JBLElBdkJBOztBQXlCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFMQSxNQUtBOztBQUVBLDBGQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxLQWZBLEVBZ0JBLEtBaEJBLENBZ0JBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0F0QkE7QUF1QkE7QUFDQSxHQTVHQTtBQThHQSxTQTlHQSxtQkE4R0EsVUE5R0EsRUE4R0E7QUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBLE1BS0EsSUFMQSxDQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFUQTtBQVVBLEdBekhBO0FBMkhBLGVBM0hBLHlCQTJIQSxRQTNIQSxFQTJIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUpBOztBQU1BO0FBQ0EsR0FwSUE7QUFzSUEsZ0JBdElBLDBCQXNJQSxTQXRJQSxFQXNJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUpBOztBQU1BO0FBQ0EsR0EvSUE7QUFpSkEsa0JBakpBLDhCQWlKQTtBQUNBO0FBQ0EsaUJBREE7QUFFQSx5Q0FGQTtBQUdBO0FBSEE7O0FBTUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUpBO0FBS0EsS0FOQSxNQU1BO0FBQ0E7QUFDQTtBQUVBLElBbEJBO0FBb0JBLEdBOUtBO0FBZ0xBLGNBaExBLHdCQWdMQSxNQWhMQSxFQWdMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUxBLE1BS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQkFEQTtBQUVBLDJDQUZBO0FBR0E7QUFIQTs7QUFNQTtBQUNBLEtBVEEsRUFTQSxJQVRBO0FBVUE7O0FBRUE7QUFDQSxHQXpNQTtBQTJNQSxnQ0EzTUEsNENBMk1BO0FBQUE7O0FBQ0EsbUVBQ0EsSUFEQSxDQUNBO0FBQ0E7O0FBRUE7QUFDQSxJQUxBLEVBTUEsS0FOQSxDQU1BO0FBQUE7QUFBQSxJQU5BO0FBT0EsR0FuTkE7QUFxTkEsZUFyTkEsMkJBcU5BO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQURBO0FBRUEscUJBRkE7QUFHQTtBQUhBO0FBS0EsSUFOQSxFQU1BLElBTkE7QUFPQSxHQS9OQTtBQWlPQSxtQkFqT0EsNkJBaU9BLENBak9BLEVBaU9BO0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUZBO0FBR0EsSUFMQSxNQUtBO0FBQ0E7QUFDQTtBQUNBLEdBMU9BO0FBNE9BLFVBNU9BLG9CQTRPQSxDQTVPQSxFQTRPQTtBQUNBOztBQUVBO0FBQ0EsMENBQ0Esb0VBREEsS0FHQTs7QUFFQTtBQUNBLElBUEEsTUFPQTtBQUNBOztBQUVBLDBDQUNBLG1EQURBLEtBR0E7O0FBRUE7QUFDQTtBQUNBO0FBaFFBLEVBakNBOztBQW9TQSxRQXBTQSxxQkFvU0E7QUFDQTtBQUNBO0FBQ0EsRUF2U0E7QUF5U0EsUUF6U0EscUJBeVNBO0FBQ0E7QUFDQTtBQTNTQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BGQTs7QUFFQSw4QkFGQTs7QUFJQSxLQUpBLGtCQUlBOztBQUVBO0FBQ0EseUJBREE7O0FBR0EsWUFIQTs7QUFLQSx3QkFMQTtBQU1BLCtCQU5BO0FBT0E7QUFQQTtBQVVBLEVBaEJBOzs7QUFrQkE7QUFFQSxZQUZBLHNCQUVBLE1BRkEsRUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQUZBLE1BRUE7QUFDQTtBQUNBO0FBQ0EsR0FWQTtBQVlBLHNCQVpBLGtDQVlBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLElBRkE7O0FBSUE7QUFDQSxHQXBCQTtBQXNCQSx5Q0F0QkEscURBc0JBO0FBQUE7O0FBQ0E7O0FBRUE7QUFDQTtBQUNBLElBRkE7O0FBSUE7QUFDQTtBQUNBLElBRkEsTUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREEsT0FLQSxJQUxBLENBS0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsTUFGQTs7QUFJQTs7QUFFQTtBQUNBLEtBakJBLEVBa0JBLEtBbEJBLENBa0JBO0FBQUE7QUFBQSxLQWxCQTtBQW1CQTtBQUNBLEdBcERBO0FBc0RBLDhCQXREQSwwQ0FzREE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLElBRkEsRUFFQSxJQUZBO0FBR0EsR0FsRUE7QUFvRUEsMEJBcEVBLHNDQW9FQTtBQUNBO0FBQ0EsR0F0RUE7QUF3RUEsaUNBeEVBLDZDQXdFQTtBQUNBO0FBQ0EsR0ExRUE7QUE0RUEsaUJBNUVBLDJCQTRFQSxTQTVFQSxFQTRFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQkFEQTtBQUVBLHFCQUZBO0FBR0Esb0JBSEE7QUFJQTtBQUpBO0FBTUEsSUFQQSxDQU9BLElBUEEsQ0FPQSxJQVBBLEdBT0EsSUFQQTtBQVFBLEdBdkZBO0FBeUZBLGdCQXpGQSwwQkF5RkEsS0F6RkEsRUF5RkEsS0F6RkEsRUF5RkE7QUFDQTtBQUNBO0FBQ0E7QUE1RkEsRUFsQkE7O0FBa0hBLFFBbEhBLHFCQWtIQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUVBLEdBWEEsQ0FXQSxJQVhBLENBV0EsSUFYQTtBQWFBO0FBaklBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVGQTs7QUFFQSxvQkFGQTs7QUFJQSxNQUpBLGtCQUlBO0FBQ0E7QUFDQTtBQUNBO0FBREEsU0FFQSx3Q0FGQTtBQURBO0FBS0EsR0FWQTs7O0FBWUE7QUFFQSxlQUZBLHVCQUVBLEVBRkEsRUFFQTtBQUNBO0FBQ0EsS0FKQTtBQU1BLHlCQU5BLG1DQU1BO0FBQ0E7QUFDQSxLQVJBO0FBVUEsZ0JBVkEsMEJBVUE7QUFBQTs7QUFDQSw2RkFDQSxJQURBLENBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsU0FKQSxNQUlBO0FBQ0E7QUFDQTtBQUNBLE9BYkEsRUFjQSxLQWRBLENBY0E7QUFBQTtBQUFBLE9BZEE7QUFlQTtBQTFCQTs7QUFaQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDK0JBO0FBQ0EsaUJBREE7QUFFQTtBQUNBLFVBREEsa0JBQ0EsRUFEQSxFQUNBO0FBQ0E7QUFDQTtBQUhBLEdBRkE7QUFPQTtBQUNBLFVBREEsb0JBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVBBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNHQTs7QUFFQSxtQkFGQTs7QUFJQSxLQUpBLGtCQUlBO0FBQ0E7QUFDQTtBQUNBLGVBREE7QUFFQSxjQUZBO0FBR0E7QUFIQSxNQUlBLHdDQUpBO0FBREE7QUFPQSxFQVpBOzs7QUFjQTtBQUVBLGlCQUZBLDZCQUVBO0FBQUE7O0FBQ0EsaURBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFMQSxFQU1BLEtBTkEsQ0FNQTtBQUFBO0FBQUEsSUFOQTtBQU9BLEdBVkE7QUFZQSxhQVpBLHlCQVlBO0FBQUE7O0FBQ0EseUZBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsSUFUQSxFQVVBLEtBVkEsQ0FVQTtBQUFBO0FBQUEsSUFWQTtBQVdBO0FBeEJBOztBQWRBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNkRBOztBQUVBLG9CQUZBOztBQUlBLE1BSkEsa0JBSUE7QUFDQTtBQUNBLGVBREE7O0FBR0Esd0JBSEE7O0FBS0E7QUFDQSw0QkFEQTtBQUVBLHFCQUZBO0FBR0EsNkJBSEE7QUFJQSxnQkFKQTtBQUtBLG9CQUxBO0FBTUEsa0JBTkE7QUFPQSxrQkFQQTtBQVFBLGdCQVJBO0FBU0E7QUFUQSxTQVVBLHdDQVZBO0FBTEE7QUFpQkEsR0F0QkE7OztBQXdCQTtBQUVBLGdCQUZBLHdCQUVBLE1BRkEsRUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBWEE7QUFhQSxzQkFiQSw4QkFhQSxlQWJBLEVBYUEsU0FiQSxFQWFBLGdCQWJBLEVBYUEsSUFiQSxFQWFBLFFBYkEsRUFhQSxNQWJBLEVBYUEsTUFiQSxFQWFBLElBYkEsRUFhQSxNQWJBLEVBYUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQXpCQTtBQTJCQSxRQTNCQSxnQkEyQkEsUUEzQkEsRUEyQkEsU0EzQkEsRUEyQkEsR0EzQkEsRUEyQkE7QUFBQTs7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFEQTtBQUVBLCtCQUZBO0FBR0E7QUFIQTtBQURBLFNBT0EsSUFQQSxDQU9BO0FBQ0E7QUFDQTtBQUNBOztBQUhBLDhCQUtBLGdCQUxBO0FBQUEsWUFLQSxHQUxBLG1CQUtBLEdBTEE7QUFBQSxZQUtBLElBTEEsbUJBS0EsSUFMQTtBQUFBLFlBS0EsTUFMQSxtQkFLQSxNQUxBO0FBQUEsWUFLQSxPQUxBLG1CQUtBLE9BTEE7QUFBQSxZQUtBLE1BTEEsbUJBS0EsTUFMQTs7QUFNQTtBQUNBOztBQUVBO0FBQ0EsaUVBQ0EsNENBREEsR0FFQSxFQUZBO0FBR0EsK0RBQ0EsbUNBREEsR0FFQSxFQUZBO0FBR0E7O0FBRUE7QUFDQSxPQTFCQSxFQTJCQSxLQTNCQSxDQTJCQTtBQUFBO0FBQUEsT0EzQkE7QUE0QkEsS0E5REE7QUFnRUEsaUJBaEVBLDJCQWdFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx3QkFEQTtBQUVBLDBCQUZBO0FBR0E7QUFIQTtBQUtBLE9BTkEsRUFNQSxJQU5BO0FBT0EsS0ExRUE7QUE0RUEscUJBNUVBLDZCQTRFQSxDQTVFQSxFQTRFQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FGQTtBQUdBLE9BTEEsTUFLQTtBQUNBO0FBQ0E7QUFDQSxLQXJGQTtBQXVGQSxlQXZGQSx5QkF1RkE7QUFBQTs7QUFDQSw2RkFDQSxJQURBLENBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLE9BWkEsRUFhQSxLQWJBLENBYUE7QUFBQTtBQUFBLE9BYkE7QUFjQTtBQXRHQTs7QUF4QkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdEZBOztBQUVBLG1CQUZBOztBQUlBLEtBSkEsa0JBSUE7QUFDQTtBQUVBLEVBUEE7OztBQVNBO0FBRUEsa0JBRkEsNEJBRUEsSUFGQSxFQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFQQSxFQVRBOztBQXFCQSxRQXJCQSxxQkFxQkEsQ0FFQTtBQXZCQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMkNBOztBQUVBLG1CQUZBOztBQUlBLEtBSkEsa0JBSUE7QUFDQTtBQUNBLGVBREE7O0FBR0EsZUFIQTtBQUlBLGVBSkE7O0FBTUE7QUFDQSxlQURBO0FBRUEsZ0JBRkE7QUFHQSxnQkFIQTtBQUlBLGdCQUpBO0FBS0E7QUFMQSxNQU1BLHdDQU5BO0FBTkE7QUFjQSxFQW5CQTs7O0FBcUJBO0FBRUEsV0FGQSxxQkFFQSxNQUZBLEVBRUE7QUFDQTtBQUNBLEdBSkE7QUFNQSxhQU5BLHVCQU1BLEVBTkEsRUFNQTtBQUNBOztBQUVBO0FBQ0EsR0FWQTtBQVlBLFlBWkEsc0JBWUEsTUFaQSxFQVlBLE1BWkEsRUFZQTtBQUFBOztBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esb0JBREE7QUFFQTtBQUZBO0FBREE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBREE7QUFFQTtBQUZBO0FBREE7QUFNQTs7QUFFQSxjQUNBLGFBREEsRUFFQSxhQUZBLEdBR0EsSUFIQSxDQUdBLGFBQ0EsVUFDQSxRQURBLEVBRUEsUUFGQSxFQUdBOztBQUVBO0FBQ0E7QUFDQSxJQVJBLENBSEEsRUFZQSxLQVpBLENBWUE7QUFBQTtBQUFBLElBWkE7O0FBY0E7QUFDQSxHQW5EQTtBQXFEQSxlQXJEQSwyQkFxREE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsbUJBREE7QUFFQSxxQkFGQTtBQUdBO0FBSEE7QUFLQSxJQU5BLEVBTUEsSUFOQTtBQU9BLEdBL0RBO0FBaUVBLG1CQWpFQSw2QkFpRUEsQ0FqRUEsRUFpRUE7QUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBRkE7QUFHQSxJQUxBLE1BS0E7QUFDQTtBQUNBO0FBQ0EsR0ExRUE7QUE0RUEsbUJBNUVBLCtCQTRFQTtBQUNBO0FBQ0E7QUFDQSxHQS9FQTtBQWlGQSxVQWpGQSxzQkFpRkE7QUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUhBLE1BR0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLEtBSkE7O0FBTUEsb0ZBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQWRBLEVBZUEsS0FmQSxDQWVBO0FBQUE7QUFBQSxLQWZBO0FBaUJBO0FBQ0E7QUFqSEEsRUFyQkE7O0FBMElBLFFBMUlBLHFCQTBJQTtBQUNBO0FBQ0E7QUE1SUEsRzs7Ozs7OztBQ25FQSxnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxFQUF3TztBQUNsUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxFQUFvTTtBQUM5TTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcsVUFBVSwyRUFBMkUsTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLGtwQ0FBa3BDLGdDQUFnQyxxQ0FBcUMseUJBQXlCLHFDQUFxQywwQkFBMEIscUNBQXFDLDhCQUE4QixxQ0FBcUMsd0JBQXdCLHFDQUFxQyx3QkFBd0IsNmZBQTZmLCtEQUErRCw4ZEFBOGQsa0RBQWtELG9ZQUFvWSxzREFBc0QsOHNDQUE4c0Msa2dCQUFrZ0Isb0RBQW9ELGdFQUFnRSwwb0JBQTBvQiwrQ0FBK0Msa0JBQWtCLGdHQUFnRyxpUUFBaVEsR0FBRyx1Q0FBdUMsY0FBYyxTQUFTLHFCQUFxQixrQ0FBa0MsNkJBQTZCLG1FQUFtRSxnRUFBZ0UsMkJBQTJCLGFBQWEsV0FBVyxtQ0FBbUMsV0FBVyw2SEFBNkgsdURBQXVELHdFQUF3RSx5REFBeUQsc0VBQXNFLDZDQUE2QyxxREFBcUQsaURBQWlELGlEQUFpRCwyQ0FBMkMsaURBQWlELFdBQVcsNENBQTRDLGlDQUFpQyx1Q0FBdUMsdUNBQXVDLDJCQUEyQix3Q0FBd0MsbUVBQW1FLHVCQUF1QixrSEFBa0gsYUFBYSxnQ0FBZ0Msa0RBQWtELGlEQUFpRCxtQ0FBbUMscUJBQXFCLHFDQUFxQyxvQkFBb0Isa0NBQWtDLGdDQUFnQyw4Q0FBOEMsNkpBQTZKLG1KQUFtSixlQUFlLCtHQUErRyxhQUFhLGlEQUFpRCxXQUFXLDhCQUE4Qix3REFBd0QsbUNBQW1DLCtDQUErQywwSkFBMEosRUFBRSxXQUFXLFFBQVEsU0FBUyxpQ0FBaUMsd0NBQXdDLHVEQUF1RCx1REFBdUQsK0RBQStELGFBQWEsRUFBRSxXQUFXLE9BQU8sdURBQXVELFdBQVcsU0FBUywwQkFBMEIsaUlBQWlJLHdDQUF3QywwREFBMEQsMERBQTBELHFHQUFxRywrREFBK0QseURBQXlELG1CQUFtQixlQUFlLG1EQUFtRCxTQUFTLFdBQVcsU0FBUyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEMTFULDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsK0VBQStFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVywydUJBQTJ1QiwrQ0FBK0Msa0JBQWtCLFdBQVcsU0FBUyxxQkFBcUIsb0NBQW9DLHVGQUF1Rix3REFBd0QsYUFBYSxjQUFjLHNCQUFzQixtQkFBbUIsU0FBUyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEeDlDLDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsMkVBQTJFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyw4S0FBOEsscURBQXFELHFnQkFBcWdCLHFEQUFxRCwrZ0JBQStnQixxREFBcUQsMnVCQUEydUIsK0NBQStDLGtCQUFrQix1Q0FBdUMsK0VBQStFLEdBQUcsdUNBQXVDLFlBQVksU0FBUyxxQkFBcUIsK0JBQStCLHNGQUFzRix5RUFBeUUsb0ZBQW9GLDJEQUEyRCxXQUFXLCtDQUErQyxXQUFXLDRCQUE0Qiw4SEFBOEgsa0NBQWtDLGtEQUFrRCx1REFBdUQsaURBQWlELGFBQWEsV0FBVywrQ0FBK0MsV0FBVyxXQUFXLFNBQVMseUNBQXlDLDBCQUEwQixLQUFLLGFBQWEseUJBQXlCLEtBQUssYUFBYSxHOzs7Ozs7O0FDRHBpSCwyQkFBMkIsbUJBQU8sQ0FBQyxDQUEyRDtBQUM5RixjQUFjLFFBQVMsNEJBQTRCLHdCQUF3QixHQUFHLDRCQUE0Qix1QkFBdUIsR0FBRyxxQ0FBcUMscUJBQXFCLEdBQUcsVUFBVSw2RUFBNkUsTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxXQUFXLHNOQUFzTix3REFBd0QsaVJBQWlSLGtJQUFrSSwyQkFBMkIsaXBEQUFpcEQsd0RBQXdELHU2REFBdTZELHlCQUF5QixxQ0FBcUMsa0JBQWtCLDJmQUEyZixrQkFBa0Isb0NBQW9DLGlDQUFpQywrckJBQStyQixZQUFZLHdOQUF3TiwyQkFBMkIsa1RBQWtULDZCQUE2QixrNEJBQWs0QixzSkFBc0osYUFBYSw2akJBQTZqQixzSkFBc0osYUFBYSxtbkJBQW1uQixhQUFhLGtkQUFrZCwrQ0FBK0Msb0JBQW9CLHdSQUF3Uix5TUFBeU0sR0FBRyx1Q0FBdUMsZ0JBQWdCLFdBQVcsdUJBQXVCLGdDQUFnQyxvRkFBb0YsOEVBQThFLGtDQUFrQyw4RUFBOEUsZUFBZSxRQUFRLDJCQUEyQixrREFBa0QsYUFBYSx5Q0FBeUMsOEJBQThCLHFEQUFxRCwrQ0FBK0MsbURBQW1ELGlEQUFpRCxvREFBb0Qsa0RBQWtELG1EQUFtRCwrQ0FBK0MsK0JBQStCLDZLQUE2SywwQ0FBMEMsaUNBQWlDLGlDQUFpQyxhQUFhLGdDQUFnQyxrQ0FBa0MscURBQXFELGdFQUFnRSxvRUFBb0Usc0VBQXNFLDREQUE0RCxlQUFlLEVBQUUsbUNBQW1DLG9DQUFvQyxvQ0FBb0MsNkNBQTZDLDRFQUE0RSw4RUFBOEUsc0hBQXNILHdEQUF3RCxpQkFBaUIsRUFBRSwySEFBMkgsOEhBQThILGlFQUFpRSxnREFBZ0QseUlBQXlJLEVBQUUsbUJBQW1CLHVIQUF1SCx3REFBd0QsaUJBQWlCLEVBQUUsNkVBQTZFLGtDQUFrQyxrQ0FBa0MsaUJBQWlCLEVBQUUsZUFBZSxFQUFFLG9FQUFvRSxxREFBcUQsNEJBQTRCLGdDQUFnQyxPQUFPLHFKQUFxSixpQkFBaUIscUNBQXFDLGdCQUFnQixPQUFPLDhJQUE4SSw4Q0FBOEMsOERBQThELDhEQUE4RCxrRUFBa0Usd0RBQXdELHNFQUFzRSw2REFBNkQseUJBQXlCLCtDQUErQyxxQkFBcUIsc0NBQXNDLDZDQUE2Qyx3R0FBd0csNkNBQTZDLHFCQUFxQixFQUFFLGFBQWEsU0FBUyxnQ0FBZ0MsdUNBQXVDLHVCQUF1QiwyREFBMkQsYUFBYSw4QkFBOEIseURBQXlELGtFQUFrRSxvRUFBb0UsYUFBYSxFQUFFLFNBQVMsb0NBQW9DLDhCQUE4QiwwQ0FBMEMsOENBQThDLG9GQUFvRixjQUFjLFdBQVcsRUFBRSw4QkFBOEIsU0FBUyxzQ0FBc0MsK0JBQStCLGdEQUFnRCx5Q0FBeUMsMkNBQTJDLGNBQWMsV0FBVyxFQUFFLCtCQUErQixTQUFTLCtCQUErQixrREFBa0QsNEhBQTRILEVBQUUsMEJBQTBCLHFFQUFxRSx5Q0FBeUMsbUNBQW1DLHVEQUF1RCxxRUFBcUUsd0NBQXdDLHlDQUF5QyxtQ0FBbUMsaUJBQWlCLGVBQWUsRUFBRSxhQUFhLE9BQU8sMkJBQTJCLGFBQWEsYUFBYSxFQUFFLFdBQVcsaUNBQWlDLDJCQUEyQiw2REFBNkQsaUVBQWlFLDJCQUEyQixhQUFhLFdBQVcsdUJBQXVCLDREQUE0RCxvRUFBb0UsMkJBQTJCLGlCQUFpQixvQ0FBb0MsNEdBQTRHLGtHQUFrRyw0SUFBNEksRUFBRSxzSEFBc0gsaUJBQWlCLFFBQVEsV0FBVyxpQ0FBaUMsU0FBUyw2Q0FBNkMsc0dBQXNHLDJDQUEyQyxxQ0FBcUMsYUFBYSxrREFBa0QsU0FBUyw0QkFBNEIsZ0VBQWdFLG1DQUFtQyx1REFBdUQsMEpBQTBKLEVBQUUsV0FBVyxRQUFRLFNBQVMsaUNBQWlDLHdDQUF3QyxnREFBZ0QsZ0RBQWdELCtEQUErRCxhQUFhLEVBQUUsV0FBVyxPQUFPLGdEQUFnRCxXQUFXLFNBQVMsd0JBQXdCLDRDQUE0Qyw0Q0FBNEMsa0lBQWtJLG1HQUFtRyxxRkFBcUYsV0FBVyxPQUFPLHNEQUFzRCxtSEFBbUgsa0ZBQWtGLG9FQUFvRSxXQUFXLFNBQVMsV0FBVyx3QkFBd0Isb0NBQW9DLGtEQUFrRCxXQUFXLHdCQUF3QixvQ0FBb0MsV0FBVyxPQUFPLHVDQUF1QywwQkFBMEIsS0FBSyxhQUFhLHlCQUF5QixLQUFLLHNCQUFzQix1QkFBdUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEN2ltQjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSxpQkFBaUI7QUFDM0I7QUFDQTs7QUFFQSxtQkFBbUIsbUJBQU8sQ0FBQyxDQUFnQjs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsbUJBQW1CLHNCQUFzQjtBQUN6QztBQUNBO0FBQ0EsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsMkJBQTJCO0FBQ2hEO0FBQ0E7QUFDQSxZQUFZLHVCQUF1QjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EscUJBQXFCLHVCQUF1QjtBQUM1QztBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUN0TkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBNk87QUFDdlA7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBeU07QUFDbk47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsNEVBQTRFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyxvY0FBb2MsK0NBQStDLGtCQUFrQix3Q0FBd0MscUNBQXFDLEdBQUcsdUNBQXVDLFlBQVksU0FBUyxxQkFBcUIsNkJBQTZCLGdEQUFnRCxXQUFXLHNDQUFzQyxrREFBa0QsV0FBVyw2QkFBNkIsbUlBQW1JLDZDQUE2Qyw0REFBNEQsMENBQTBDLDBEQUEwRCw2RUFBNkUsbUJBQW1CLE9BQU8sbURBQW1ELG1CQUFtQixlQUFlLG1EQUFtRCxXQUFXLFdBQVcsU0FBUyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNENTFELDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsdUVBQXVFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyxrckJBQWtyQixnREFBZ0QsdWhCQUF1aEIsZ0RBQWdELDBxQkFBMHFCLCtDQUErQyxrQkFBa0IsbUNBQW1DLHNGQUFzRixHQUFHLHVDQUF1QyxZQUFZLFNBQVMscUJBQXFCLDhCQUE4QixnREFBZ0QseUNBQXlDLHlDQUF5QyxXQUFXLHlCQUF5QiwwSEFBMEgsd0NBQXdDLDBEQUEwRCxnREFBZ0QsNERBQTRELHlEQUF5RCxtQkFBbUIsZUFBZSxtREFBbUQsV0FBVyxTQUFTLFNBQVMseUNBQXlDLDBCQUEwQixLQUFLLGFBQWEseUJBQXlCLEtBQUssYUFBYSxHOzs7Ozs7O0FDRG54RywyQkFBMkIsbUJBQU8sQ0FBQyxDQUEyRDtBQUM5RixjQUFjLFFBQVMsNEJBQTRCLHdCQUF3QixHQUFHLDRCQUE0Qix1QkFBdUIsR0FBRyxVQUFVLDRFQUE0RSxNQUFNLFdBQVcsS0FBSyxLQUFLLFdBQVcsOEtBQThLLHVEQUF1RCxxUUFBcVEsdUdBQXVHLGVBQWUsb1ZBQW9WLCtDQUErQyxrQkFBa0Isa0VBQWtFLG9DQUFvQyxHQUFHLHVDQUF1QyxZQUFZLFNBQVMscUJBQXFCLDJCQUEyQix1RUFBdUUsc0JBQXNCLHNFQUFzRSxXQUFXLDhCQUE4Qix5Q0FBeUMsaUNBQWlDLGlHQUFpRyxFQUFFLGFBQWEsaUNBQWlDLGlFQUFpRSxvQ0FBb0Msd0ZBQXdGLGFBQWEsUUFBUSxXQUFXLEVBQUUsV0FBVyxzQ0FBc0MsOENBQThDLG9DQUFvQyxHQUFHLHVDQUF1QyxFQUFFLFdBQVcsNkJBQTZCLDZEQUE2RCx5REFBeUQsYUFBYSxPQUFPLHFJQUFxSSwwQ0FBMEMsMENBQTBDLHFEQUFxRCw0RkFBNEYsbUVBQW1FLGtEQUFrRCx1REFBdUQsZ0dBQWdHLGVBQWUsbUJBQW1CLHVEQUF1RCxhQUFhLFdBQVcsU0FBUyxzQkFBc0Isa0RBQWtELHVIQUF1SCxFQUFFLDhEQUE4RCxnRUFBZ0Usd0RBQXdELFNBQVMsRUFBRSw4RUFBOEUsZ0VBQWdFLDhCQUE4QixpQ0FBaUMsV0FBVyxTQUFTLEVBQUUsU0FBUyxTQUFTLHlDQUF5QywwQkFBMEIsS0FBSyxhQUFhLHlCQUF5QixLQUFLLGFBQWEsRzs7Ozs7OztBQ0R4NUgsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMxREEsMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcsVUFBVSx3RUFBd0UsTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLDhsREFBOGxELGtCQUFrQix1Q0FBdUMsa0JBQWtCLHVDQUF1QyxvQkFBb0Isd2FBQXdhLGtCQUFrQixxQ0FBcUMsaUNBQWlDLHVoQkFBdWhCLCtDQUErQyxrQkFBa0IsbUhBQW1ILHVJQUF1SSxHQUFHLHVDQUF1QyxZQUFZLFNBQVMscUJBQXFCLCtCQUErQixpQ0FBaUMsV0FBVyw4QkFBOEIsNENBQTRDLGdDQUFnQyxXQUFXLHlDQUF5Qyx1R0FBdUcsNEJBQTRCLG9DQUFvQyw4RkFBOEYseUJBQXlCLHVGQUF1RixlQUFlLEVBQUUsaUJBQWlCLHdDQUF3QywwSEFBMEgseUJBQXlCLHVGQUF1RixlQUFlLEVBQUUsaUJBQWlCLDZPQUE2TywwRUFBMEUsa0RBQWtELGlCQUFpQixzREFBc0QscUNBQXFDLFdBQVcsOEJBQThCLHlEQUF5RCxtQ0FBbUMsZ0RBQWdELDBKQUEwSixFQUFFLFdBQVcsUUFBUSxTQUFTLGlDQUFpQyx3Q0FBd0MsNENBQTRDLGlEQUFpRCxvREFBb0QsYUFBYSxFQUFFLFdBQVcsT0FBTyw0Q0FBNEMsV0FBVyxTQUFTLGdDQUFnQyw2QkFBNkIsNkJBQTZCLFNBQVMseUJBQXlCLHdEQUF3RCxtRUFBbUUseUJBQXlCLGVBQWUsT0FBTyxrREFBa0Qsb0VBQW9FLDZFQUE2RSw2RUFBNkUsa0JBQWtCLEVBQUUsb0lBQW9JLDRDQUE0Qyw0REFBNEQsNERBQTRELHVHQUF1RyxtREFBbUQsNkRBQTZELDJEQUEyRCx1QkFBdUIsbUJBQW1CLHVEQUF1RCxpQkFBaUIsV0FBVyxXQUFXLHNCQUFzQixtREFBbUQsU0FBUyxTQUFTLHlDQUF5QywwQkFBMEIsS0FBSyxhQUFhLHlCQUF5QixLQUFLLGFBQWEsRzs7Ozs7OztBQ0Q5NU4sMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcsK0JBQStCLGdCQUFnQixpQkFBaUIsR0FBRyw0QkFBNEIsb0JBQW9CLEdBQUcsZ0NBQWdDLHNCQUFzQixxQkFBcUIsR0FBRyxzQ0FBc0MsbUJBQW1CLEdBQUcsVUFBVSw4RUFBOEUsTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxVQUFVLFVBQVUsS0FBSyxLQUFLLFdBQVcsS0FBSyxLQUFLLFdBQVcsV0FBVyxLQUFLLEtBQUssVUFBVSxrVkFBa1YsK0RBQStELDBEQUEwRCxrS0FBa0ssb0JBQW9CLCtCQUErQiw0REFBNEQsK3RCQUErdEIsaUVBQWlFLDREQUE0RCxtVkFBbVYsS0FBSyxvQkFBb0IsaURBQWlELEtBQUssNERBQTRELGlEQUFpRCw0d0NBQTR3Qyx3QkFBd0IseUNBQXlDLG9CQUFvQix5Q0FBeUMsa0JBQWtCLHlDQUF5QywyQkFBMkIseUNBQXlDLDZCQUE2Qix5Q0FBeUMsMEJBQTBCLHlDQUF5QyxzQ0FBc0MseUNBQXlDLGtCQUFrQixvekJBQW96QiwwREFBMEQsc0JBQXNCLGdNQUFnTSxhQUFhLHVCQUF1QixrQ0FBa0MsMENBQTBDLHVFQUF1RSx3REFBd0QsZUFBZSxPQUFPLHFDQUFxQyxlQUFlLGFBQWEsdUNBQXVDLHlDQUF5QywrRkFBK0YsOERBQThELGFBQWEsRUFBRSx5REFBeUQsYUFBYSwwREFBMEQsZ0RBQWdELGdGQUFnRixxRUFBcUUsYUFBYSxFQUFFLHdEQUF3RCx3REFBd0QsYUFBYSxPQUFPLGtGQUFrRix5QkFBeUIsK0VBQStFLGFBQWEsZ0NBQWdDLDREQUE0RCwyQ0FBMkMsNEVBQTRFLG1FQUFtRSxtQkFBbUIsRUFBRSwrREFBK0Qsd0RBQXdELGFBQWEsaURBQWlELGFBQWEsYUFBYSwrQ0FBK0MsaUZBQWlGLDJGQUEyRiw2RUFBNkUsb0ZBQW9GLHNEQUFzRCw4QkFBOEIsdURBQXVELFdBQVcsUUFBUSxhQUFhLDJDQUEyQyw0RkFBNEYsYUFBYSxrREFBa0QsbUdBQW1HLGFBQWEsMkNBQTJDLGtGQUFrRixtQ0FBbUMscUVBQXFFLHNMQUFzTCxFQUFFLFdBQVcsbUJBQW1CLFNBQVMseUNBQXlDLHFEQUFxRCx5RUFBeUUsU0FBUyxhQUFhLHdCQUF3Qiw4RUFBOEUsb0NBQW9DLGdEQUFnRCw2Q0FBNkMsc0NBQXNDLDRDQUE0QyxpREFBaUQsV0FBVyxhQUFhLGFBQWEsT0FBTyx1Q0FBdUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxnQkFBZ0Isa0JBQWtCLG1CQUFtQixLQUFLLGFBQWEsc0JBQXNCLEtBQUssaUJBQWlCLHdCQUF3Qix1QkFBdUIsS0FBSyx1QkFBdUIscUJBQXFCLEtBQUssYUFBYSxHOzs7Ozs7O0FDRDU0UyxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7QUM3Q0E7QUFDQSxtQkFBTyxDQUFDLEdBQTJROztBQUVuUixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFxTztBQUMvTztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFpTTtBQUMzTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7O0FDOUJBO0FBQ0EsbUJBQU8sQ0FBQyxHQUFnUjs7QUFFeFIsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBME87QUFDcFA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBc007QUFDaE47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7OztBQzlCQTtBQUNBLG1CQUFPLENBQUMsR0FBaVI7O0FBRXpSLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTJPO0FBQ3JQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXVNO0FBQ2pOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7QUM5QkE7QUFDQSxtQkFBTyxDQUFDLEdBQWtSOztBQUUxUixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUE0TztBQUN0UDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUF3TTtBQUNsTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7O0FDOUJBO0FBQ0EsbUJBQU8sQ0FBQyxHQUFnUjs7QUFFeFIsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBME87QUFDcFA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBc007QUFDaE47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7OztBQzlCQTtBQUNBLG1CQUFPLENBQUMsR0FBK1E7O0FBRXZSLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXlPO0FBQ25QO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXFNO0FBQy9NO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7QUM5QkE7QUFDQSxtQkFBTyxDQUFDLEdBQStROztBQUV2UixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUF5TztBQUNuUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFxTTtBQUMvTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7O0FDOUJBO0FBQ0EsbUJBQU8sQ0FBQyxHQUFtUjs7QUFFM1IsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBNk87QUFDdlA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBeU07QUFDbk47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7OztBQzlCQTtBQUNBLG1CQUFPLENBQUMsR0FBNFE7O0FBRXBSLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXNPO0FBQ2hQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQWtNO0FBQzVNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQy9CQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM1WEEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pDQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUMsYUFBYSxhQUFhLDBCQUEwQjtBQUNyRDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUMsYUFBYSxhQUFhLDBCQUEwQjtBQUNyRDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ2pKQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1AsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3BaQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM1QkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQzlJQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3BEQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN0SUEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUMsYUFBYSxhQUFhLDBCQUEwQjtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDbEtBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEdBQTRTO0FBQ2xVLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxHQUFnVDtBQUN0VSw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLENBQXlFO0FBQzlGO0FBQ0EsR0FBRyxLQUFVO0FBQ2I7QUFDQTtBQUNBLDRKQUE0SixvRUFBb0U7QUFDaE8scUtBQXFLLG9FQUFvRTtBQUN6TztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7Ozs7QUNwQkE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBNFM7QUFDbFUsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7O0FDcEJBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEdBQThTO0FBQ3BVLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxHQUE2UztBQUNuVSw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLENBQXlFO0FBQzlGO0FBQ0EsR0FBRyxLQUFVO0FBQ2I7QUFDQTtBQUNBLDRKQUE0SixvRUFBb0U7QUFDaE8scUtBQXFLLG9FQUFvRTtBQUN6TztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7Ozs7QUNwQkE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBd1M7QUFDOVQsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7O0FDcEJBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEdBQTZTO0FBQ25VLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxHQUF5UztBQUMvVCw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLENBQXlFO0FBQzlGO0FBQ0EsR0FBRyxLQUFVO0FBQ2I7QUFDQTtBQUNBLDRKQUE0SixvRUFBb0U7QUFDaE8scUtBQXFLLG9FQUFvRTtBQUN6TztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7Ozs7QUNwQkE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBK1M7QUFDclUsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7Ozs7Ozs7Ozs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsaUJBQWlCO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQyx3QkFBd0I7QUFDM0QsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoianMvdmlzYS9ncm91cHMvc2hvdy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDgyKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCA0MmNlMGM2YzZlMmIzZTIyNzgzNSIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDE4IDE5IDIwIDIxIDIyIDIzIDI0IDI1IiwiPHRlbXBsYXRlPlxuXHRcblx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIiBAc3VibWl0LnByZXZlbnQ9XCJhZGRNdWx0aXBsZVF1aWNrUmVwb3J0XCI+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiaW9zLXNjcm9sbFwiPlxuXHRcdFx0PGRpdiB2LWZvcj1cIihkaXN0aW5jdENsaWVudFNlcnZpY2UsIGluZGV4KSBpbiBkaXN0aW5jdENsaWVudFNlcnZpY2VzLmxlbmd0aFwiPlxuXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0XCI+XG5cdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGFuZWwtaGVhZGluZ1wiPlxuXHQgICAgICAgICAgICAgICAgICAgIHt7IGRpc3RpbmN0Q2xpZW50U2VydmljZXNbaW5kZXhdIH19XG5cdCAgICAgICAgICAgICAgICA8L2Rpdj5cblx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwYW5lbC1ib2R5XCI+XG5cdCAgICAgICAgICAgICAgICAgICAgXG5cdCAgICAgICAgICAgICAgICBcdDxtdWx0aXBsZS1xdWljay1yZXBvcnQtZm9ybSA6a2V5PVwiaW5kZXhcIiA6cmVmPVwiJ2Zvcm0tJyArIGluZGV4XCIgOmRvY3M9XCJkb2NzXCIgOnNlcnZpY2Vkb2NzPVwic2VydmljZURvY3NcIiA6aW5kZXg9XCJpbmRleFwiPjwvbXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0+XG5cblx0ICAgICAgICAgICAgICAgIDwvZGl2PiA8IS0tIHBhbmVsLWJvZHkgLS0+XG5cdCAgICAgICAgICAgIDwvZGl2PiA8IS0tIHBhbmVsIC0tPlxuXG5cdFx0XHQ8L2Rpdj4gPCEtLSBsb29wIC0tPlxuXHRcdDwvZGl2PiA8IS0tIGlvcy1zY3JvbGwgLS0+XG5cdFx0XG5cdFx0PGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPkNyZWF0ZSBSZXBvcnQ8L2J1dHRvbj5cblx0ICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuXHQ8L2Zvcm0+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5cblx0ZXhwb3J0IGRlZmF1bHQge1xuXG5cdFx0cHJvcHM6IFsnZ3JvdXByZXBvcnQnLCAnZ3JvdXBpZCcsICdjbGllbnRzaWQnLCAnY2xpZW50c2VydmljZXMnLCAndHJhY2tpbmdzJywgJ2lzd3JpdGVyZXBvcnRwYWdlJ10sXG5cblx0XHRkYXRhKCkge1xuXHRcdFx0cmV0dXJuIHtcblx0XHRcblx0XHRcdFx0ZG9jczogW10sXG5cdFx0XHRcdHNlcnZpY2VEb2NzOiBbXSxcblxuICAgICAgICBcdFx0bXVsdGlwbGVRdWlja1JlcG9ydEZvcm0gOiBuZXcgRm9ybSh7XG4gICAgICAgIFx0XHRcdGRpc3RpbmN0Q2xpZW50U2VydmljZXM6W10sXG5cbiAgICAgICAgXHRcdFx0Z3JvdXBSZXBvcnQ6IG51bGwsXG4gICAgICAgIFx0XHRcdGdyb3VwSWQ6IG51bGwsXG5cbiAgICAgICAgXHRcdFx0Y2xpZW50c0lkOiBbXSxcbiAgICAgICAgXHRcdFx0Y2xpZW50U2VydmljZXNJZDogW10sXG4gICAgICAgIFx0XHRcdHRyYWNraW5nczogW10sXG5cbiAgICAgICAgXHRcdFx0YWN0aW9uSWQ6IFtdLFxuXG4gICAgICAgIFx0XHRcdGNhdGVnb3J5SWQ6IFtdLFxuICAgICAgICBcdFx0XHRjYXRlZ29yeU5hbWU6IFtdLFxuXG4gICAgICAgIFx0XHRcdGRhdGUxOiBbXSxcbiAgICAgICAgXHRcdFx0ZGF0ZTI6IFtdLFxuICAgICAgICBcdFx0XHRkYXRlMzogW10sXG4gICAgICAgIFx0XHRcdGRhdGVUaW1lQXJyYXk6IFtdLFxuXG4gICAgICAgIFx0XHRcdGV4dHJhRGV0YWlsczogW11cbiAgICAgICAgXHRcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXG5cblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0bWV0aG9kczoge1xuXG5cdFx0XHRmZXRjaERvY3MoaWQpIHtcblx0XHRcdFx0dmFyIGlkcyA9ICcnO1xuXHRcdFx0XHQkLmVhY2goaWQsIGZ1bmN0aW9uKGksIGl0ZW0pIHtcblx0XHRcdFx0ICAgIGlkcyArPSBpZFtpXS5kb2NzX25lZWRlZCArICcsJyArIGlkW2ldLmRvY3Nfb3B0aW9uYWwgKyAnLCc7XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdHZhciB1bmlxdWVMaXN0PWlkcy5zcGxpdCgnLCcpLmZpbHRlcihmdW5jdGlvbihhbGxJdGVtcyxpLGEpe1xuXHRcdFx0XHQgICAgcmV0dXJuIGk9PWEuaW5kZXhPZihhbGxJdGVtcyk7XG5cdFx0XHRcdH0pLmpvaW4oJywnKTtcblx0ICAgICAgICBcdFxuXHRcdFx0IFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC8nK3VuaXF1ZUxpc3QrJy9zZXJ2aWNlLWRvY3MnKVxuXHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdCAgICAgICAgICAgIHRoaXMuZG9jcyA9IHJlc3VsdC5kYXRhO1xuXHRcdCAgICAgICAgfSk7XG5cblx0ICAgICAgICBcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHQgICAgICAgIFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItZG9jcycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XHRcblx0ICAgICAgICBcdH0sIDUwMDApO1xuXHRcdFx0fSxcblxuXHRcdFx0Z2V0U2VydmljZURvY3MoKSB7XG4gICAgICAgIFx0XHRheGlvcy5nZXQoJy92aXNhL2dyb3VwL3NlcnZpY2UtZG9jcy1pbmRleCcpXG5cdFx0XHQgXHQgIC50aGVuKHJlc3VsdCA9PiB7XG5cdFx0ICAgICAgICAgICAgdGhpcy5zZXJ2aWNlRG9jcyA9IHJlc3VsdC5kYXRhO1xuXHRcdCAgICAgICAgfSk7XG4gICAgICAgIFx0fSxcblxuXHRcdFx0cmVzZXRNdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSgpIHtcblx0XHRcdFx0Ly8gTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXG5cdFx0XHRcdFx0dGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzLmZvckVhY2goKGRpc3RpbmN0Q2xpZW50U2VydmljZSwgaW5kZXgpID0+IHtcblx0XHRcdFx0XHRcdGxldCByZWYgPSAnZm9ybS0nICsgaW5kZXg7XG5cblx0XHRcdFx0XHRcdHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSA9IHtcblx0XHQgICAgICAgIFx0XHRcdGFjdGlvbklkOiAxLFxuXG5cdFx0ICAgICAgICBcdFx0XHRjYXRlZ29yeUlkOiBudWxsLFxuXHRcdCAgICAgICAgXHRcdFx0Y2F0ZWdvcnlOYW1lOiBudWxsLFxuXG5cdFx0ICAgICAgICBcdFx0XHRkYXRlMTogbnVsbCxcblx0XHQgICAgICAgIFx0XHRcdGRhdGUyOiBudWxsLFxuXHRcdCAgICAgICAgXHRcdFx0ZGF0ZTM6IG51bGwsXG5cdFx0ICAgICAgICBcdFx0XHRkYXRlVGltZUFycmF5OiBbXSxcblxuXHRcdCAgICAgICAgXHRcdFx0ZXh0cmFEZXRhaWxzOiBbXVxuXHRcdCAgICAgICAgXHRcdH1cblxuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMi52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMy52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNC52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNS52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNi52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNy52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlOC52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlOS52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTAudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTExLnZhbHVlID0gJyc7XG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxMi52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTMudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTE0LnZhbHVlID0gJyc7XG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxNS52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTYudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuY2F0ZWdvcnlOYW1lLnZhbHVlID0gJyc7XG5cblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uY3VzdG9tQ2F0ZWdvcnkgPSBmYWxzZTtcblx0XHRcdFx0XHR9KTtcblxuICAgICAgICBcdFx0Ly8gTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcblx0ICAgICAgICBcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSA9IG5ldyBGb3JtKHtcblx0ICAgICAgICBcdFx0XHRkaXN0aW5jdENsaWVudFNlcnZpY2VzOltdLFxuXG5cdCAgICAgICAgXHRcdFx0YWN0aW9uSWQ6IFtdLFxuXG5cdCAgICAgICAgXHRcdFx0Y2F0ZWdvcnlJZDogW10sXG5cdCAgICAgICAgXHRcdFx0Y2F0ZWdvcnlOYW1lOiBbXSxcblxuXHQgICAgICAgIFx0XHRcdGRhdGUxOiBbXSxcblx0ICAgICAgICBcdFx0XHRkYXRlMjogW10sXG5cdCAgICAgICAgXHRcdFx0ZGF0ZTM6IFtdLFxuXHQgICAgICAgIFx0XHRcdGRhdGVUaW1lQXJyYXk6IFtdLFxuXG5cdCAgICAgICAgXHRcdFx0ZXh0cmFEZXRhaWxzOiBbXVxuXHQgICAgICAgIFx0XHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KTtcblx0XHRcdH0sXG5cblx0XHRcdHZhbGlkYXRlKCkge1xuXHRcdFx0XHR2YXIgaXNWYWxpZCA9IHRydWU7XG5cblx0XHRcdFx0dGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzLmZvckVhY2goKGRpc3RpbmN0Q2xpZW50U2VydmljZSwgaW5kZXgpID0+IHtcblx0XHRcdFx0XHRsZXQgcmVmID0gJ2Zvcm0tJyArIGluZGV4O1xuXG5cdFx0XHRcdFx0aWYoICEgdGhpcy4kcmVmc1tyZWZdWzBdLnZhbGlkYXRlKGRpc3RpbmN0Q2xpZW50U2VydmljZSkgKSB7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRyZXR1cm4gaXNWYWxpZDtcblx0XHRcdH0sXG5cblx0XHRcdGFkZE11bHRpcGxlUXVpY2tSZXBvcnQoKSB7XG5cdFx0XHRcdGlmKHRoaXMudmFsaWRhdGUoKSkge1xuXHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZ3JvdXBSZXBvcnQgPSB0aGlzLmdyb3VwcmVwb3J0O1xuXHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZ3JvdXBJZCA9IHRoaXMuZ3JvdXBpZDtcblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmNsaWVudHNJZCA9IHRoaXMuY2xpZW50c2lkO1xuXHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2xpZW50U2VydmljZXNJZCA9IHRoaXMuY2xpZW50c2VydmljZXM7XG5cdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS50cmFja2luZ3MgPSB0aGlzLnRyYWNraW5ncztcblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmRpc3RpbmN0Q2xpZW50U2VydmljZXMgPSB0aGlzLmRpc3RpbmN0Q2xpZW50U2VydmljZXM7XG5cblx0XHRcdFx0XHR0aGlzLmRpc3RpbmN0Q2xpZW50U2VydmljZXMuZm9yRWFjaCgoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgPT4ge1xuXHRcdFx0XHRcdFx0bGV0IHJlZiA9ICdmb3JtLScgKyBpbmRleDtcblxuXHRcdFx0XHRcdFx0bGV0IF9hY3Rpb25JZCA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZDtcblx0XHRcdFx0XHRcdGxldCBfY2F0ZWdvcnlJZCA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkO1xuXHRcdFx0XHRcdFx0bGV0IF9jYXRlZ29yeU5hbWUgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlOYW1lO1xuXHRcdFx0XHRcdFx0bGV0IF9kYXRlMSA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMTtcblx0XHRcdFx0XHRcdGxldCBfZGF0ZTIgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTI7XG5cdFx0XHRcdFx0XHRsZXQgX2RhdGUzID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUzO1xuXHRcdFx0XHRcdFx0bGV0IF9kYXRlVGltZUFycmF5ID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXk7XG5cdFx0XHRcdFx0XHRsZXQgX2V4dHJhRGV0YWlscyA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHM7XG5cblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uYWN0aW9uSWQucHVzaChfYWN0aW9uSWQpO1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5jYXRlZ29yeUlkLnB1c2goX2NhdGVnb3J5SWQpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2F0ZWdvcnlOYW1lLnB1c2goX2NhdGVnb3J5TmFtZSk7XG5cdCAgICAgICAgXHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kYXRlMS5wdXNoKF9kYXRlMSk7XG5cdCAgICAgICAgXHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kYXRlMi5wdXNoKF9kYXRlMik7XG5cdCAgICAgICAgXHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kYXRlMy5wdXNoKF9kYXRlMyk7XG5cdCAgICAgICAgXHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kYXRlVGltZUFycmF5LnB1c2goX2RhdGVUaW1lQXJyYXkpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZXh0cmFEZXRhaWxzLnB1c2goX2V4dHJhRGV0YWlscyk7XG5cdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9xdWljay1yZXBvcnQnKVxuXHRcdFx0ICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0ICAgICAgICAgICAgXHRpZihyZXNwb25zZS5zdWNjZXNzKSB7XG5cdFx0XHQgICAgICAgICAgICBcdFx0dGhpcy5yZXNldE11bHRpcGxlUXVpY2tSZXBvcnRGb3JtKCk7XG5cdFx0XHQgICAgICAgICAgICBcdFx0XG5cdFx0XHQgICAgICAgICAgICBcdFx0JCgnI2FkZC1xdWljay1yZXBvcnQtbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXG5cdFx0XHQgICAgICAgICAgICBcdFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XG5cblx0XHRcdCAgICAgICAgICAgIFx0XHRpZih0aGlzLmlzd3JpdGVyZXBvcnRwYWdlKSB7XG5cdFx0XHQgICAgICAgICAgICBcdFx0XHQkKCcjYWRkLXF1aWNrLXJlcG9ydC1tb2RhbCcpLm9uKCdoaWRkZW4uYnMubW9kYWwnLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0ICBcdHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy92aXNhL3JlcG9ydC93cml0ZS1yZXBvcnQnO1xuXHRcdFx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHQgICAgICAgICAgICBcdFx0fSBlbHNlIHtcblx0XHRcdCAgICAgICAgICAgIFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHQgICAgICAgICAgICBcdFx0XHRsb2NhdGlvbi5yZWxvYWQoKTtcblx0XHRcdFx0ICAgICAgICAgICAgXHRcdH0sIDEwMDApO1xuXHRcdFx0ICAgICAgICAgICAgXHRcdH1cblx0XHRcdCAgICAgICAgICAgIFx0fVxuXHRcdFx0ICAgICAgICAgICAgfSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdH0sXG5cblx0XHRjcmVhdGVkKCkge1xuXHRcdFx0dGhpcy5nZXRTZXJ2aWNlRG9jcygpO1xuXHRcdH0sXG5cblx0XHRjb21wdXRlZDoge1xuXHRcdFx0X2lzV3JpdGVSZXBvcnRQYWdlKCkge1xuXHRcdCAgICAgIFx0cmV0dXJuIHRoaXMuaXN3cml0ZXJlcG9ydHBhZ2UgfHwgZmFsc2U7XG5cdFx0ICAgIH0sXG5cblx0XHRcdGRpc3RpbmN0Q2xpZW50U2VydmljZXMoKSB7XG5cdFx0XHRcdGxldCBkaXN0aW5jdENsaWVudFNlcnZpY2VzID0gW107XG5cblx0XHRcdFx0dGhpcy5jbGllbnRzZXJ2aWNlcy5mb3JFYWNoKGNsaWVudFNlcnZpY2UgPT4ge1xuXHRcdFx0XHRcdGlmKGRpc3RpbmN0Q2xpZW50U2VydmljZXMuaW5kZXhPZihjbGllbnRTZXJ2aWNlLmRldGFpbCkgPT0gLTEpIHtcblx0XHRcdFx0XHRcdGRpc3RpbmN0Q2xpZW50U2VydmljZXMucHVzaChjbGllbnRTZXJ2aWNlLmRldGFpbCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRyZXR1cm4gZGlzdGluY3RDbGllbnRTZXJ2aWNlcztcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0Y29tcG9uZW50czoge1xuXHQgICAgICAgICdtdWx0aXBsZS1xdWljay1yZXBvcnQtZm9ybSc6IHJlcXVpcmUoJy4vTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlJylcblx0ICAgIH1cblxuXHR9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gTXVsdGlwbGVRdWlja1JlcG9ydC52dWU/MzVmOTk2YjkiLCI8dGVtcGxhdGU+XG5cdFxuXHQ8ZGl2PlxuXHRcdFxuXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHQgICAgICAgIEFjdGlvbjogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0ICAgIDwvbGFiZWw+XG5cblx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICAgICAgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJhY3Rpb25cIiB2LW1vZGVsPVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWRcIiBAY2hhbmdlPVwiZ2V0Q2F0ZWdvcmllc1wiPlxuXHRcdCAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XCJhY3Rpb24gaW4gYWN0aW9uc1wiIDp2YWx1ZT1cImFjdGlvbi5pZFwiPlxuXHRcdCAgICAgICAgICAgXHRcdHt7IGFjdGlvbi5uYW1lIH19XG5cdFx0ICAgICAgICAgICAgPC9vcHRpb24+XG5cdFx0ICAgICAgICA8L3NlbGVjdD5cblx0XHQgICAgPC9kaXY+XG5cdFx0PC9kaXY+IDwhLS0gZm9ybS1ncm91cCAtLT5cblxuXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAyIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDMgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA5IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEwIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDExIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE0IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE1IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE2XCI+XG5cdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHQgICAgICAgIENhdGVnb3J5OiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHQgICAgPC9sYWJlbD5cblxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cImNhdGVnb3J5XCIgdi1tb2RlbD1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWRcIiBAY2hhbmdlPVwiY2hhbmdlQ2F0ZWdvcnlIYW5kbGVyXCIgdi1zaG93PVwiIWN1c3RvbUNhdGVnb3J5XCI+XG5cdFx0ICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cImNhdGVnb3J5IGluIGNhdGVnb3JpZXNcIiA6dmFsdWU9XCJjYXRlZ29yeS5pZFwiPlxuXHRcdCAgICAgICAgICAgIFx0e3sgY2F0ZWdvcnkubmFtZSB9fVxuXHRcdCAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdCAgICAgICAgPC9zZWxlY3Q+XG5cblx0XHQgICAgICAgIDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOVwiPlxuXHRcdCAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHJlZj1cImNhdGVnb3J5TmFtZVwiIHBsYWNlaG9sZGVyPVwiRW50ZXIgY2F0ZWdvcnlcIiB2LW1vZGVsPVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlOYW1lXCIgdi1zaG93PVwiY3VzdG9tQ2F0ZWdvcnlcIj5cblxuXHRcdCAgICAgICAgXHQ8ZGl2IHN0eWxlPVwiaGVpZ2h0OjEwcHg7IGNsZWFyOmJvdGg7XCI+PC9kaXY+XG5cblx0XHQgICAgICAgIFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXNtIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIiBAY2xpY2s9XCJjdXN0b21DYXRlZ29yeSA9ICFjdXN0b21DYXRlZ29yeTsgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9IG51bGxcIj5cblx0XHRcdCAgICAgICAgXHQ8c3BhbiB2LXNob3c9XCIhY3VzdG9tQ2F0ZWdvcnlcIj5Vc2UgQ3VzdG9tIENhdGVnb3J5PC9zcGFuPlxuXHRcdFx0ICAgICAgICBcdDxzcGFuIHYtc2hvdz1cImN1c3RvbUNhdGVnb3J5XCI+VXNlIFByZWRlZmluZWQgQ2F0ZWdvcnk8L3NwYW4+XG5cdFx0XHQgICAgICAgIDwvYnV0dG9uPlxuXHRcdCAgICAgICAgPC9kaXY+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdDwvZGl2PiA8IS0tIGZvcm0tZ3JvdXAgLS0+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDFcIj4gPCEtLSBGaWxlZCAtLT5cblx0XHRcdDwhLS0gRGF0ZSBwaWNrZXIgKGRhdGUxIDogYXV0byBjYXB0dXJlIDogcmVxdWlyZWQpIC0tPlxuXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdEVzdGltYXRlZCBSZWxlYXNpbmcgRGF0ZTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTJcIiByZWY9XCJkYXRlMlwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMVwiPlxuXHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRTY2hlZHVsZWQgSGVhcmluZyBEYXRlIGFuZCBUaW1lOlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgIFx0PGRpdiBjbGFzcz1cInJvd1wiIHYtZm9yPVwiKGRhdGVUaW1lRWxlbWVudCwgaW5kZXgpIGluIG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXlcIj5cblx0XHRcdFx0ICAgIFx0PGRpdiBjbGFzcz1cImNvbC1tZC0xMVwiPlxuXHRcdFx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCIgc3R5bGU9XCJtYXJnaW4tYm90dG9tOiAxMHB4O1wiPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICBcdDwvc3Bhbj5cblxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBkYXRldGltZXBpY2tlcmFycmF5XCIgbmFtZT1cImRhdGVUaW1lQXJyYXlbXVwiIHYtbW9kZWw9XCJkYXRlVGltZUVsZW1lbnQudmFsdWVcIiBwbGFjZWhvbGRlcj1cInl5eXktbW0tZGQgaGg6bW06c3NcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cblx0XHQgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHQgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHQgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMSB0ZXh0LWNlbnRlclwiPlxuXHQgICAgICAgICAgICAgICAgICAgIFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRhbmdlciBidG4tc21cIiBAY2xpY2s9XCJyZW1vdmVEYXRlVGltZVBpY2tlckFycmF5KGluZGV4KVwiIHYtc2hvdz1cImluZGV4ICE9IDBcIj5cblx0ICAgICAgICAgICAgICAgICAgICBcdFx0PGkgY2xhc3M9XCJmYSBmYS10aW1lc1wiPjwvaT5cblx0ICAgICAgICAgICAgICAgICAgICBcdDwvYnV0dG9uPlxuXHQgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPVwiaGVpZ2h0OjEwcHg7Y2xlYXI6Ym90aDtcIj48L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBidG4tc20gcHVsbC1yaWdodFwiIEBjbGljaz1cImFkZERhdGVUaW1lUGlja2VyQXJyYXlcIj5cbiAgICAgICAgICAgICAgICAgICAgXHQ8aSBjbGFzcz1cImZhIGZhLXBsdXNcIj48L2k+IEFkZFxuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNiB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDcgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxMCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDE0IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTVcIj5cblx0XHRcdFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0U2NoZWR1bGVkIEFwcG9pbnRtZW50IERhdGUgYW5kIFRpbWU6XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIiBpZD1cImRhdGUzXCIgcmVmPVwiZGF0ZTNcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMlwiPiA8IS0tIFJlbGVhc2VkIC0tPlxuXG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDNcIj4gPCEtLSBQYWlkIE9QUyAtLT5cblxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA0XCI+IDwhLS0gVW5hYmxlIHRvIGZpbGUgbmVlZCBhZGRpdGlvbmFsIHJlcXVpcmVtZW50cyAtLT5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0RG9jdW1lbnRzOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICAgICAgICAgIDxzZWxlY3QgOmlkPVwiJ2Nob3Nlbi0nICsgaW5kZXhcIiBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IERvY3NcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLWRvY3MtMlwiIG11bHRpcGxlIHN0eWxlPVwid2lkdGg6MzUwcHg7XCIgdGFiaW5kZXg9XCI0XCI+XG5cdFx0ICAgICAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XCJzZXJ2aWNlZG9jIGluIHNlcnZpY2Vkb2NzXCIgOnZhbHVlPVwic2VydmljZWRvYy50aXRsZVwiPlxuXHRcdCAgICAgICAgICAgICAgICBcdHt7IChzZXJ2aWNlZG9jLnRpdGxlKS50cmltKCkgfX1cblx0XHQgICAgICAgICAgICAgICAgPC9vcHRpb24+XG5cdFx0ICAgICAgICAgICAgPC9zZWxlY3Q+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNlwiPiA8IS0tIERlYWRsaW5lIG9mIFN1Ym1pc3Npb24gLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdFN1Ym1pc3Npb24gRGF0ZTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTRcIiByZWY9XCJkYXRlNFwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdENvbnNlcXVlbmNlIG9mIG5vbiBzdWJtaXNzaW9uOlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgXHQ8ZGl2PlxuXHRcdFx0ICAgIFx0XHQ8bGFiZWw+XG5cdFx0XHQgICAgXHRcdFx0PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIHYtbW9kZWw9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHNcIiB2YWx1ZT1cIkFkZGl0aW9uYWwgY29zdCBtYXliZSBjaGFyZ2VkLlwiPlxuXHRcdFx0ICAgIFx0XHRcdEFkZGl0aW9uYWwgY29zdCBtYXliZSBjaGFyZ2VkLlxuXHRcdFx0ICAgIFx0XHQ8L2xhYmVsPlxuXHRcdFx0ICAgIFx0PC9kaXY+XG5cdFx0XHQgICAgXHQ8ZGl2PlxuXHRcdFx0ICAgIFx0XHQ8bGFiZWw+XG5cdFx0XHQgICAgXHRcdFx0PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIHYtbW9kZWw9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHNcIiB2YWx1ZT1cIkFwcGxpY2F0aW9uIG1heWJlIGZvcmZlaXRlZCBhbmQgaGFzIHRvIHJlLWFwcGx5LlwiPlxuXHRcdFx0ICAgIFx0XHRcdEFwcGxpY2F0aW9uIG1heWJlIGZvcmZlaXRlZCBhbmQgaGFzIHRvIHJlLWFwcGx5LlxuXHRcdFx0ICAgIFx0XHQ8L2xhYmVsPlxuXHRcdFx0ICAgIFx0PC9kaXY+XG5cdFx0XHQgICAgXHQ8ZGl2PlxuXHRcdFx0ICAgIFx0XHQ8bGFiZWw+XG5cdFx0XHQgICAgXHRcdFx0PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIHYtbW9kZWw9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHNcIiB2YWx1ZT1cIkluaXRpYWwgZGVwb3NpdCBtYXliZSBmb3JmZWl0ZWQuXCI+XG5cdFx0XHQgICAgXHRcdFx0SW5pdGlhbCBkZXBvc2l0IG1heWJlIGZvcmZlaXRlZC5cblx0XHRcdCAgICBcdFx0PC9sYWJlbD5cblx0XHRcdCAgICBcdDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDdcIj4gPCEtLSBGb3IgSW1wbGVtZW50YXRpb24gLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdEVzdGltYXRlZCBUaW1lIG9mIEZpbmlzaGluZzogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIiBpZD1cImRhdGU1XCIgcmVmPVwiZGF0ZTVcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOFwiPiA8IS0tIFVuYWJsZSB0byBmaWxlIGR1ZSB0byBiYWQgd2VhdGhlciAtLT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0QWZmZWN0ZWQgRGF0ZTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJpbnB1dC1kYXRlcmFuZ2UgaW5wdXQtZ3JvdXBcIj5cblx0XHQgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTZcIiByZWY9XCJkYXRlNlwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG5cdFx0ICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj50bzwvc3Bhbj5cblx0XHQgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTdcIiByZWY9XCJkYXRlN1wiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG5cdFx0ICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRUYXJnZXQgRmlsbGluZyBEYXRlOlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGU4XCIgcmVmPVwiZGF0ZThcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOVwiPiA8IS0tIFVuYWJsZSB0byBmaW5pc2ggZHVlIHRvIHN1c3BlbnNpb24gb2Ygd29yayAtLT5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0RGF0ZSBSYW5nZTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWRhdGVyYW5nZSBpbnB1dC1ncm91cFwiPlxuXHRcdCAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlucHV0LXNtIGZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIgaWQ9XCJkYXRlOVwiIHJlZj1cImRhdGU5XCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgLz5cblx0XHQgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPnRvPC9zcGFuPlxuXHRcdCAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlucHV0LXNtIGZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIgaWQ9XCJkYXRlMTBcIiByZWY9XCJkYXRlMTBcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPlxuXHRcdCAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEwXCI+IDwhLS0gUHJvY2Vzc2luZyBQZXJpb2QgRXh0ZW5zaW9uIC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFeHRlbmRlZCBwZXJpb2QgZm9yIHByb2Nlc3Npbmc6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGUxMVwiIHJlZj1cImRhdGUxMVwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDExXCI+IDwhLS0gRm9yIHBheW1lbnQgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdEVzdGltYXRlZCBSZWxlYXNpbmcgRGF0ZTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTEyXCIgcmVmPVwiZGF0ZTEyXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTJcIj4gPCEtLSBQaWNrZWQgdXAgZnJvbSBjbGllbnQgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdERhdGUgYW5kIFRpbWUgUGljayBVcDogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIiBpZD1cImRhdGUxM1wiIHJlZj1cImRhdGUxM1wiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxM1wiPiA8IS0tIEZvciBkZWxpdmVyeSBub3cgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdERhdGUgYW5kIFRpbWUgRGVsaXZlcjogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIiBpZD1cImRhdGUxNFwiIHJlZj1cImRhdGUxNFwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNFwiPiA8IS0tIERlbGl2ZXJlZCAtLT5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxMjhcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRDb21wYW55IENvdXJpZXI6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImNvbXBhbnlfY291cmllclwiIHJlZj1cImNvbXBhbnlfY291cmllclwiPlxuXHRcdFx0ICAgICAgICBcdDxvcHRpb24gdi1mb3I9XCJjb21wYW55Q291cmllciBpbiBjb21wYW55Q291cmllcnNcIiA6dmFsdWU9XCJjb21wYW55Q291cmllci5hZGRyZXNzLmNvbnRhY3RfbnVtYmVyXCI+XG5cdFx0XHQgICAgICAgIFx0XHR7eyBjb21wYW55Q291cmllci5maXJzdF9uYW1lICsgJyAnICsgY29tcGFueUNvdXJpZXIubGFzdF9uYW1lIH19IFxuXHRcdFx0ICAgICAgICBcdFx0KHt7IGNvbXBhbnlDb3VyaWVyLmFkZHJlc3MuY29udGFjdF9udW1iZXIgfX0pXG5cdFx0XHQgICAgICAgIFx0PC9vcHRpb24+XG5cdFx0XHQgICAgICAgIDwvc2VsZWN0PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCAhPSAxMjhcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRUcmFja2luZyBOdW1iZXI6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJ0cmFja2luZ19udW1iZXJcIiByZWY9XCJ0cmFja2luZ19udW1iZXJcIj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdERhdGUgYW5kIFRpbWUgRGVsaXZlcmVkOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAgZGF0ZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgXHQ8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBkYXRldGltZXBpY2tlclwiIGlkPVwiZGF0ZTE1XCIgcmVmPVwiZGF0ZTE1XCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE1XCI+IDwhLS0gRm9yIHByb2Nlc3Mgb2YgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzYgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3NyB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc4IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzkgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgxIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODIgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4M1wiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdEVzdGltYXRlZCBSZWxlYXNpbmcgRGF0ZTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTE2XCIgcmVmPVwiZGF0ZTE2XCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj4gXG5cblx0PC9kaXY+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5cdFxuXHRleHBvcnQgZGVmYXVsdCB7XG5cblx0XHRwcm9wczogWydpbmRleCcsICdkb2NzJywgJ3NlcnZpY2Vkb2NzJ10sXG5cblx0XHRkYXRhKCkge1xuICAgICAgICBcdHJldHVybiB7XG5cbiAgICAgICAgXHRcdGFjdGlvbnM6IFtdLFxuICAgICAgICBcdFx0Y2F0ZWdvcmllczogW10sXG5cbiAgICAgICAgXHRcdGNvbXBhbnlDb3VyaWVyczogW10sXG5cbiAgICAgICAgXHRcdGN1c3RvbUNhdGVnb3J5OiBmYWxzZSxcblxuICAgICAgICBcdFx0bXVsdGlwbGVRdWlja1JlcG9ydGZvcm06IHtcbiAgICAgICAgXHRcdFx0YWN0aW9uSWQ6IDEsXG5cbiAgICAgICAgXHRcdFx0Y2F0ZWdvcnlJZDogbnVsbCxcbiAgICAgICAgXHRcdFx0Y2F0ZWdvcnlOYW1lOiBudWxsLFxuXG4gICAgICAgIFx0XHRcdGRhdGUxOiBudWxsLFxuICAgICAgICBcdFx0XHRkYXRlMjogbnVsbCxcbiAgICAgICAgXHRcdFx0ZGF0ZTM6IG51bGwsXG4gICAgICAgIFx0XHRcdGRhdGVUaW1lQXJyYXk6IFtdLFxuXG4gICAgICAgIFx0XHRcdGV4dHJhRGV0YWlsczogW11cbiAgICAgICAgXHRcdH1cblxuICAgICAgICBcdH1cbiAgICAgICAgfSxcblxuICAgICAgICBtZXRob2RzOiB7XG5cbiAgICAgICAgXHRnZXRBY3Rpb25zKCkge1xuICAgICAgICBcdFx0YXhpb3MuZ2V0KCcvdmlzYS9hY3Rpb25zJylcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHR0aGlzLmFjdGlvbnMgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGdldENvbXBhbnlDb3VyaWVycygpIHtcbiAgICAgICAgXHRcdGxldCByb2xlID0gJ2NvbXBhbnktY291cmllcic7XG5cbiAgICAgICAgXHRcdGF4aW9zLmdldCgnL3Zpc2EvdXNlcnMtYnktcm9sZS8nICsgcm9sZSlcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHR0aGlzLmNvbXBhbnlDb3VyaWVycyA9IHJlc3BvbnNlLmRhdGE7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0Z2V0Q3VycmVudERhdGUoKSB7XG4gICAgICAgIFx0XHRsZXQgdG9kYXkgPSBuZXcgRGF0ZSgpO1xuXHRcdFx0XHRsZXQgZGQgPSB0b2RheS5nZXREYXRlKCk7XG5cdFx0XHRcdGxldCBtbSA9IHRvZGF5LmdldE1vbnRoKCkgKyAxO1xuXHRcdFx0XHRsZXQgeXl5eSA9IHRvZGF5LmdldEZ1bGxZZWFyKCk7XG5cblx0XHRcdFx0aWYoZGQgPCAxMCkge1xuXHRcdFx0XHQgICAgZGQgPSAnMCcgKyBkZDtcblx0XHRcdFx0fSBcblxuXHRcdFx0XHRpZihtbSA8IDEwKSB7XG5cdFx0XHRcdCAgICBtbSA9ICcwJyArIG1tO1xuXHRcdFx0XHR9IFxuXG5cdFx0XHRcdHJldHVybiBtbSArICcvJyArIGRkICsgJy8nICsgeXl5eTtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0Y2hhbmdlQWN0aW9uSGFuZGxlcihhY3Rpb25JZCkge1xuICAgICAgICBcdFx0Ly8gUmVzZXRcbiAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTEgPSBudWxsO1xuICAgICAgICBcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IG51bGw7XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUzID0gbnVsbDtcbiAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZVRpbWVBcnJheSA9IFtdO1xuICAgICAgICBcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMgPSBbXTtcblxuICAgICAgICBcdFx0Ly8gRm9yIGF1dG8gY2FwdHVyZSBkYXRlXG4gICAgICAgIFx0XHRpZihhY3Rpb25JZCA9PSAxIHx8IGFjdGlvbklkID09IDMgfHwgYWN0aW9uSWQgPT0gNCB8fCAgYWN0aW9uSWQgPT0gNyB8fCBhY3Rpb25JZCA9PSAxMSB8fCBhY3Rpb25JZCA9PSAxNSB8fCBhY3Rpb25JZCA9PSAxNikge1xuICAgICAgICBcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gdGhpcy5nZXRDdXJyZW50RGF0ZSgpO1xuICAgICAgICBcdFx0fVxuICAgICAgICBcdH0sXG5cbiAgICAgICAgXHRnZXRDYXRlZ29yaWVzKCkge1xuICAgICAgICBcdFx0bGV0IGFjdGlvbklkID0gdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZDtcblxuICAgICAgICBcdFx0dGhpcy5jaGFuZ2VBY3Rpb25IYW5kbGVyKGFjdGlvbklkKTtcblxuICAgICAgICBcdFx0dGhpcy5jYXRlZ29yaWVzID0gW107XG5cbiAgICAgICAgXHRcdGF4aW9zLmdldCgnL3Zpc2EvY2F0ZWdvcmllcy1ieS1hY3Rpb24taWQnLCB7XG4gICAgICAgIFx0XHRcdFx0cGFyYW1zOiB7XG5cdFx0XHRcdFx0ICAgIFx0YWN0aW9uSWQ6IGFjdGlvbklkXG5cdFx0XHRcdFx0ICAgIH1cbiAgICAgICAgXHRcdFx0fSlcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHR0aGlzLmNhdGVnb3JpZXMgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGNoYW5nZUNhdGVnb3J5SGFuZGxlcigpIHtcbiAgICAgICAgXHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMSAmJiB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMSkge1xuICAgICAgICBcdFx0XHR0aGlzLmFkZERhdGVUaW1lUGlja2VyQXJyYXkoKTtcbiAgICAgICAgXHRcdH1cbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0YWRkRGF0ZVRpbWVQaWNrZXJBcnJheSgpIHtcbiAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZVRpbWVBcnJheS5wdXNoKHt2YWx1ZTpudWxsfSk7XG5cbiAgICAgICAgXHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0ICAgICAgICAkKCcuZGF0ZXRpbWVwaWNrZXJhcnJheScpLmlucHV0bWFzayhcImRhdGV0aW1lXCIsIHtcblx0XHQgICAgICAgICAgICAgICAgbWFzazogXCJ5LTItMSBoOnM6c1wiLCBcblx0XHQgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwieXl5eS1tbS1kZCBoaDptbTpzc1wiLCBcblx0XHQgICAgICAgICAgICAgICAgbGVhcGRheTogXCItMDItMjlcIiwgXG5cdFx0ICAgICAgICAgICAgICAgIHNlcGFyYXRvcjogXCItXCIsIFxuXHRcdCAgICAgICAgICAgICAgICBhbGlhczogXCJ5eXl5LW1tLWRkXCJcblx0XHQgICAgICAgICAgICB9KTtcbiAgICAgICAgXHRcdH0sIDUwMCk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdHJlbW92ZURhdGVUaW1lUGlja2VyQXJyYXkoaW5kZXgpIHtcbiAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZVRpbWVBcnJheS5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICBcdH0sXG5cbiAgICAgICAgXHR2YWxpZGF0ZShkaXN0aW5jdENsaWVudFNlcnZpY2UpIHtcbiAgICAgICAgXHRcdHZhciBpc1ZhbGlkID0gdHJ1ZTtcblxuICAgICAgICBcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxKSB7XG5cdFx0XHRcdFx0bGV0IGRhdGUyID0gdGhpcy4kcmVmcy5kYXRlMi52YWx1ZTtcblx0XHRcdFx0XHRsZXQgZGF0ZTMgPSAodGhpcy4kcmVmcy5kYXRlMy52YWx1ZSkgPyB0aGlzLiRyZWZzLmRhdGUzLnZhbHVlIDogJyc7XG5cblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKGRhdGUyID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGVzdGltYXRlZCByZWxlYXNpbmcgZGF0ZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRsZXQgZGF0ZVRpbWVBcnJheSA9IFtdO1xuXHRcdCAgICAgICAgXHRcdCQoJ2lucHV0W25hbWVePVwiZGF0ZVRpbWVBcnJheVwiXScpLmVhY2goIGZ1bmN0aW9uKGUpIHtcblx0XHQgICAgICAgIFx0XHRcdGlmKHRoaXMudmFsdWUgIT0gJycpIHsgZGF0ZVRpbWVBcnJheS5wdXNoKHRoaXMudmFsdWUpOyB9XG5cdFx0XHRcdFx0ICAgIH0pO1xuXG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyID0gZGF0ZTI7XG5cdFx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNiB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNyB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTAgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDE0IHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNSkge1xuXHRcdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUzID0gZGF0ZTM7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZVRpbWVBcnJheSA9IGRhdGVUaW1lQXJyYXk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAyIHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMykge1xuXHRcdFx0XHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSBudWxsKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY2F0ZWdvcnkuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDQpIHtcblx0XHRcdFx0XHRsZXQgZG9jcyA9IFtdO1xuXHQgICAgICAgIFx0XHQkKCcjY2hvc2VuLScgKyB0aGlzLmluZGV4ICsnIG9wdGlvbjpzZWxlY3RlZCcpLmVhY2goIGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0ICAgICAgICBkb2NzLnB1c2goJCh0aGlzKS52YWwoKSk7XG5cdFx0XHRcdCAgICB9KTtcblxuXHRcdFx0XHRcdGlmKGRvY3MubGVuZ3RoID09IDApIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBkb2N1bWVudHMuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0JCgnI2Nob3Nlbi0nICsgdGhpcy5pbmRleCkudmFsKCcnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xuXG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscyA9IGRvY3M7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA2KSB7XG5cdFx0XHRcdFx0bGV0IGRhdGU0ID0gdGhpcy4kcmVmcy5kYXRlNC52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKGRhdGU0ID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIHN1Ym1pc3Npb24gZGF0ZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gZGF0ZTQ7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA3KSB7XG5cdFx0XHRcdFx0bGV0IGRhdGU1ID0gdGhpcy4kcmVmcy5kYXRlNS52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKGRhdGU1ID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGVzdGltYXRlZCB0aW1lIG9mIGZpbmlzaGluZyBkYXRlLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTIgPSBkYXRlNTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDgpIHtcblx0XHRcdFx0XHRsZXQgZGF0ZTYgPSB0aGlzLiRyZWZzLmRhdGU2LnZhbHVlO1xuXHRcdFx0XHRcdGxldCBkYXRlNyA9IHRoaXMuJHJlZnMuZGF0ZTcudmFsdWU7XG5cdFx0XHRcdFx0bGV0IGRhdGU4ID0gdGhpcy4kcmVmcy5kYXRlOC52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSBudWxsKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY2F0ZWdvcnkuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoZGF0ZTYgPT0gJycgfHwgZGF0ZTcgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgYWZmZWN0ZWQgZGF0ZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gZGF0ZTY7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyID0gZGF0ZTc7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUzID0gZGF0ZTg7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA5KSB7XG5cdFx0XHRcdFx0aWYodGhpcy5jdXN0b21DYXRlZ29yeSkge1xuXHRcdFx0XHRcdFx0bGV0IGNhdGVnb3J5TmFtZSA9IHRoaXMuJHJlZnMuY2F0ZWdvcnlOYW1lLnZhbHVlO1xuXG5cdFx0XHRcdFx0XHRpZihjYXRlZ29yeU5hbWUgPT0gJycpIHtcblx0XHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9IDA7XG5cdFx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlOYW1lID0gY2F0ZWdvcnlOYW1lO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGxldCBkYXRlOSA9IHRoaXMuJHJlZnMuZGF0ZTkudmFsdWU7XG5cdFx0XHRcdFx0bGV0IGRhdGUxMCA9IHRoaXMuJHJlZnMuZGF0ZTEwLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZihkYXRlOSA9PSAnJyB8fCBkYXRlMTAgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgZGF0ZSByYW5nZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gZGF0ZTk7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyID0gZGF0ZTEwO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTApIHtcblx0XHRcdFx0XHRsZXQgZGF0ZTExID0gdGhpcy4kcmVmcy5kYXRlMTEudmFsdWU7XG5cblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKGRhdGUxMSA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBleHRlbmRlZCBwZXJpb2QgZm9yIHByb2Nlc3NpbmcuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGUxMTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDExKSB7XG5cdFx0XHRcdFx0bGV0IGRhdGUxMiA9IHRoaXMuJHJlZnMuZGF0ZTEyLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZihkYXRlMTIgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgZXN0aW1hdGVkIHJlbGVhc2luZyBkYXRlLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTIgPSBkYXRlMTI7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMikge1xuXHRcdFx0XHRcdGxldCBkYXRlMTMgPSB0aGlzLiRyZWZzLmRhdGUxMy52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKGRhdGUxMyA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBkYXRlIGFuZCB0aW1lIHBpY2sgdXAuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGUxMztcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEzKSB7XG5cdFx0XHRcdFx0bGV0IGRhdGUxNCA9IHRoaXMuJHJlZnMuZGF0ZTE0LnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYoZGF0ZTE0ID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGRhdGUgYW5kIHRpbWUgZGVsaXZlci4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gZGF0ZTE0O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTQpIHtcblx0XHRcdFx0XHRsZXQgZGF0ZTE1ID0gdGhpcy4kcmVmcy5kYXRlMTUudmFsdWU7XG5cdFx0XHRcdFx0bGV0IGNvbXBhbnlDb3VyaWVyID0gdGhpcy4kcmVmcy5jb21wYW55X2NvdXJpZXIudmFsdWU7XG5cdFx0XHRcdFx0bGV0IHRyYWNraW5nTnVtYmVyID0gdGhpcy4kcmVmcy50cmFja2luZ19udW1iZXIudmFsdWU7XG5cblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxMjggJiYgY29tcGFueUNvdXJpZXIgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjb21wYW55IGNvdXJpZXIuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkICE9IDEyOCAmJiB0cmFja2luZ051bWJlciA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCB0cmFja2luZyBudW1iZXIuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoZGF0ZTE1ID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGRhdGUgYW5kIHRpbWUgZGVsaXZlcmVkLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTEgPSBkYXRlMTU7XG5cblx0XHRcdFx0XHRcdGxldCBleHRyYURldGFpbCA9ICh0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTI4KSA/IGNvbXBhbnlDb3VyaWVyIDogdHJhY2tpbmdOdW1iZXI7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscyA9IFtleHRyYURldGFpbF07XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNSkge1xuXHRcdFx0XHRcdGxldCBkYXRlMTYgPSB0aGlzLiRyZWZzLmRhdGUxNi52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSBudWxsKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY2F0ZWdvcnkuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoIGRhdGUxNiA9PSAnJyAmJiAodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc2IHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3NyB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzggfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc5IHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MCB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODEgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgyIHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MykgKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGVzdGltYXRlZCByZWxlYXNpbmcgZGF0ZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyID0gZGF0ZTE2O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTYpIHtcblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0cmV0dXJuIGlzVmFsaWQ7XG4gICAgICAgIFx0fVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlZCgpIHtcbiAgICAgICAgXHR0aGlzLmdldEFjdGlvbnMoKTtcblxuICAgICAgICBcdHRoaXMuZ2V0Q2F0ZWdvcmllcygpO1xuXG4gICAgICAgIFx0dGhpcy5nZXRDb21wYW55Q291cmllcnMoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBtb3VudGVkKCkge1xuXG4gICAgICAgIFx0Ly8gQ2hvc2VuXG4gICAgICAgIFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLWRvY3MtMicpLmNob3Nlbih7XG5cdFx0XHRcdFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdFx0XHRcdG5vX3Jlc3VsdHNfdGV4dDogXCJObyByZXN1bHQvcyBmb3VuZC5cIixcblx0XHRcdFx0XHRzZWFyY2hfY29udGFpbnM6IHRydWVcblx0XHRcdFx0fSk7XG5cbiAgICAgICAgXHQvLyBEYXRlUGlja2VyXG5cdCAgICAgICAgXHQkKCcuZGF0ZXBpY2tlcicpLmRhdGVwaWNrZXIoe1xuXHQgICAgICAgICAgICAgICAgdG9kYXlCdG46IFwibGlua2VkXCIsXG5cdCAgICAgICAgICAgICAgICBrZXlib2FyZE5hdmlnYXRpb246IGZhbHNlLFxuXHQgICAgICAgICAgICAgICAgZm9yY2VQYXJzZTogZmFsc2UsXG5cdCAgICAgICAgICAgICAgICBjYWxlbmRhcldlZWtzOiB0cnVlLFxuXHQgICAgICAgICAgICAgICAgYXV0b2Nsb3NlOiB0cnVlLFxuXHQgICAgICAgICAgICAgICAgZm9ybWF0OiBcIm1tL2RkL3l5eXlcIixcblx0ICAgICAgICAgICAgICAgIHN0YXJ0RGF0ZTogbmV3IERhdGUoKVxuXHQgICAgICAgICAgICB9KTtcblxuXHRcdFx0Ly8gRGF0ZVRpbWUgUGlja2VyXG5cdFx0XHRcdCQoJy5kYXRldGltZXBpY2tlcicpLmlucHV0bWFzayhcImRhdGV0aW1lXCIsIHtcblx0XHQgICAgICAgICAgICBtYXNrOiBcInktMi0xIGg6czpzXCIsIFxuXHRcdCAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcInl5eXktbW0tZGQgaGg6bW06c3NcIiwgXG5cdFx0ICAgICAgICAgICAgbGVhcGRheTogXCItMDItMjlcIiwgXG5cdFx0ICAgICAgICAgICAgc2VwYXJhdG9yOiBcIi1cIiwgXG5cdFx0ICAgICAgICAgICAgYWxpYXM6IFwieXl5eS1tbS1kZFwiXG5cdFx0ICAgICAgICB9KTtcblxuICAgICAgICB9XG5cblx0fVxuXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlPzdhMmY4NTY3IiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSgpO1xuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuZm9ybVtkYXRhLXYtMGYyZjJiNzddIHtcXG5cXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcbn1cXG4ubS1yLTEwW2RhdGEtdi0wZjJmMmI3N10ge1xcblxcdG1hcmdpbi1yaWdodDogMTBweDtcXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIk11bHRpcGxlUXVpY2tSZXBvcnQudnVlPzM1Zjk5NmI5XCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUE0UEE7Q0FDQSxvQkFBQTtDQUNBO0FBQ0E7Q0FDQSxtQkFBQTtDQUNBXCIsXCJmaWxlXCI6XCJNdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVwiLFwic291cmNlc0NvbnRlbnRcIjpbXCI8dGVtcGxhdGU+XFxuXFx0XFxuXFx0PGZvcm0gY2xhc3M9XFxcImZvcm0taG9yaXpvbnRhbFxcXCIgQHN1Ym1pdC5wcmV2ZW50PVxcXCJhZGRNdWx0aXBsZVF1aWNrUmVwb3J0XFxcIj5cXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJpb3Mtc2Nyb2xsXFxcIj5cXG5cXHRcXHRcXHQ8ZGl2IHYtZm9yPVxcXCIoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgaW4gZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5sZW5ndGhcXFwiPlxcblxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcInBhbmVsIHBhbmVsLWRlZmF1bHRcXFwiPlxcblxcdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwYW5lbC1oZWFkaW5nXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIHt7IGRpc3RpbmN0Q2xpZW50U2VydmljZXNbaW5kZXhdIH19XFxuXFx0ICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwYW5lbC1ib2R5XFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcblxcdCAgICAgICAgICAgICAgICBcXHQ8bXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0gOmtleT1cXFwiaW5kZXhcXFwiIDpyZWY9XFxcIidmb3JtLScgKyBpbmRleFxcXCIgOmRvY3M9XFxcImRvY3NcXFwiIDpzZXJ2aWNlZG9jcz1cXFwic2VydmljZURvY3NcXFwiIDppbmRleD1cXFwiaW5kZXhcXFwiPjwvbXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0+XFxuXFxuXFx0ICAgICAgICAgICAgICAgIDwvZGl2PiA8IS0tIHBhbmVsLWJvZHkgLS0+XFxuXFx0ICAgICAgICAgICAgPC9kaXY+IDwhLS0gcGFuZWwgLS0+XFxuXFxuXFx0XFx0XFx0PC9kaXY+IDwhLS0gbG9vcCAtLT5cXG5cXHRcXHQ8L2Rpdj4gPCEtLSBpb3Mtc2Nyb2xsIC0tPlxcblxcdFxcdFxcblxcdFxcdDxidXR0b24gdHlwZT1cXFwic3VibWl0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcXFwiPkNyZWF0ZSBSZXBvcnQ8L2J1dHRvbj5cXG5cXHQgICAgPGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFx0PC9mb3JtPlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXG5cXHRleHBvcnQgZGVmYXVsdCB7XFxuXFxuXFx0XFx0cHJvcHM6IFsnZ3JvdXByZXBvcnQnLCAnZ3JvdXBpZCcsICdjbGllbnRzaWQnLCAnY2xpZW50c2VydmljZXMnLCAndHJhY2tpbmdzJywgJ2lzd3JpdGVyZXBvcnRwYWdlJ10sXFxuXFxuXFx0XFx0ZGF0YSgpIHtcXG5cXHRcXHRcXHRyZXR1cm4ge1xcblxcdFxcdFxcblxcdFxcdFxcdFxcdGRvY3M6IFtdLFxcblxcdFxcdFxcdFxcdHNlcnZpY2VEb2NzOiBbXSxcXG5cXG4gICAgICAgIFxcdFxcdG11bHRpcGxlUXVpY2tSZXBvcnRGb3JtIDogbmV3IEZvcm0oe1xcbiAgICAgICAgXFx0XFx0XFx0ZGlzdGluY3RDbGllbnRTZXJ2aWNlczpbXSxcXG5cXG4gICAgICAgIFxcdFxcdFxcdGdyb3VwUmVwb3J0OiBudWxsLFxcbiAgICAgICAgXFx0XFx0XFx0Z3JvdXBJZDogbnVsbCxcXG5cXG4gICAgICAgIFxcdFxcdFxcdGNsaWVudHNJZDogW10sXFxuICAgICAgICBcXHRcXHRcXHRjbGllbnRTZXJ2aWNlc0lkOiBbXSxcXG4gICAgICAgIFxcdFxcdFxcdHRyYWNraW5nczogW10sXFxuXFxuICAgICAgICBcXHRcXHRcXHRhY3Rpb25JZDogW10sXFxuXFxuICAgICAgICBcXHRcXHRcXHRjYXRlZ29yeUlkOiBbXSxcXG4gICAgICAgIFxcdFxcdFxcdGNhdGVnb3J5TmFtZTogW10sXFxuXFxuICAgICAgICBcXHRcXHRcXHRkYXRlMTogW10sXFxuICAgICAgICBcXHRcXHRcXHRkYXRlMjogW10sXFxuICAgICAgICBcXHRcXHRcXHRkYXRlMzogW10sXFxuICAgICAgICBcXHRcXHRcXHRkYXRlVGltZUFycmF5OiBbXSxcXG5cXG4gICAgICAgIFxcdFxcdFxcdGV4dHJhRGV0YWlsczogW11cXG4gICAgICAgIFxcdFxcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXFxuXFxuXFx0XFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0bWV0aG9kczoge1xcblxcblxcdFxcdFxcdGZldGNoRG9jcyhpZCkge1xcblxcdFxcdFxcdFxcdHZhciBpZHMgPSAnJztcXG5cXHRcXHRcXHRcXHQkLmVhY2goaWQsIGZ1bmN0aW9uKGksIGl0ZW0pIHtcXG5cXHRcXHRcXHRcXHQgICAgaWRzICs9IGlkW2ldLmRvY3NfbmVlZGVkICsgJywnICsgaWRbaV0uZG9jc19vcHRpb25hbCArICcsJztcXG5cXHRcXHRcXHRcXHR9KTtcXG5cXG5cXHRcXHRcXHRcXHR2YXIgdW5pcXVlTGlzdD1pZHMuc3BsaXQoJywnKS5maWx0ZXIoZnVuY3Rpb24oYWxsSXRlbXMsaSxhKXtcXG5cXHRcXHRcXHRcXHQgICAgcmV0dXJuIGk9PWEuaW5kZXhPZihhbGxJdGVtcyk7XFxuXFx0XFx0XFx0XFx0fSkuam9pbignLCcpO1xcblxcdCAgICAgICAgXFx0XFxuXFx0XFx0XFx0IFxcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJyt1bmlxdWVMaXN0Kycvc2VydmljZS1kb2NzJylcXG5cXHRcXHRcXHQgXFx0ICAudGhlbihyZXN1bHQgPT4ge1xcblxcdFxcdCAgICAgICAgICAgIHRoaXMuZG9jcyA9IHJlc3VsdC5kYXRhO1xcblxcdFxcdCAgICAgICAgfSk7XFxuXFxuXFx0ICAgICAgICBcXHRzZXRUaW1lb3V0KCgpID0+IHtcXG5cXHQgICAgICAgIFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1kb2NzJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcXHRcXG5cXHQgICAgICAgIFxcdH0sIDUwMDApO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0Z2V0U2VydmljZURvY3MoKSB7XFxuICAgICAgICBcXHRcXHRheGlvcy5nZXQoJy92aXNhL2dyb3VwL3NlcnZpY2UtZG9jcy1pbmRleCcpXFxuXFx0XFx0XFx0IFxcdCAgLnRoZW4ocmVzdWx0ID0+IHtcXG5cXHRcXHQgICAgICAgICAgICB0aGlzLnNlcnZpY2VEb2NzID0gcmVzdWx0LmRhdGE7XFxuXFx0XFx0ICAgICAgICB9KTtcXG4gICAgICAgIFxcdH0sXFxuXFxuXFx0XFx0XFx0cmVzZXRNdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSgpIHtcXG5cXHRcXHRcXHRcXHQvLyBNdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWVcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmRpc3RpbmN0Q2xpZW50U2VydmljZXMuZm9yRWFjaCgoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdGxldCByZWYgPSAnZm9ybS0nICsgaW5kZXg7XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtID0ge1xcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0YWN0aW9uSWQ6IDEsXFxuXFxuXFx0XFx0ICAgICAgICBcXHRcXHRcXHRjYXRlZ29yeUlkOiBudWxsLFxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0Y2F0ZWdvcnlOYW1lOiBudWxsLFxcblxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTE6IG51bGwsXFxuXFx0XFx0ICAgICAgICBcXHRcXHRcXHRkYXRlMjogbnVsbCxcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGRhdGUzOiBudWxsLFxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZVRpbWVBcnJheTogW10sXFxuXFxuXFx0XFx0ICAgICAgICBcXHRcXHRcXHRleHRyYURldGFpbHM6IFtdXFxuXFx0XFx0ICAgICAgICBcXHRcXHR9XFxuXFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTIudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMy52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGU0LnZhbHVlID0gJyc7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTUudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNi52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGU3LnZhbHVlID0gJyc7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTgudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlOS52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxMC52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxMS52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxMi52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxMy52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxNC52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxNS52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxNi52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmNhdGVnb3J5TmFtZS52YWx1ZSA9ICcnO1xcblxcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLmN1c3RvbUNhdGVnb3J5ID0gZmFsc2U7XFxuXFx0XFx0XFx0XFx0XFx0fSk7XFxuXFxuICAgICAgICBcXHRcXHQvLyBNdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxcblxcdCAgICAgICAgXFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSA9IG5ldyBGb3JtKHtcXG5cXHQgICAgICAgIFxcdFxcdFxcdGRpc3RpbmN0Q2xpZW50U2VydmljZXM6W10sXFxuXFxuXFx0ICAgICAgICBcXHRcXHRcXHRhY3Rpb25JZDogW10sXFxuXFxuXFx0ICAgICAgICBcXHRcXHRcXHRjYXRlZ29yeUlkOiBbXSxcXG5cXHQgICAgICAgIFxcdFxcdFxcdGNhdGVnb3J5TmFtZTogW10sXFxuXFxuXFx0ICAgICAgICBcXHRcXHRcXHRkYXRlMTogW10sXFxuXFx0ICAgICAgICBcXHRcXHRcXHRkYXRlMjogW10sXFxuXFx0ICAgICAgICBcXHRcXHRcXHRkYXRlMzogW10sXFxuXFx0ICAgICAgICBcXHRcXHRcXHRkYXRlVGltZUFycmF5OiBbXSxcXG5cXG5cXHQgICAgICAgIFxcdFxcdFxcdGV4dHJhRGV0YWlsczogW11cXG5cXHQgICAgICAgIFxcdFxcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0dmFsaWRhdGUoKSB7XFxuXFx0XFx0XFx0XFx0dmFyIGlzVmFsaWQgPSB0cnVlO1xcblxcblxcdFxcdFxcdFxcdHRoaXMuZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5mb3JFYWNoKChkaXN0aW5jdENsaWVudFNlcnZpY2UsIGluZGV4KSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0bGV0IHJlZiA9ICdmb3JtLScgKyBpbmRleDtcXG5cXG5cXHRcXHRcXHRcXHRcXHRpZiggISB0aGlzLiRyZWZzW3JlZl1bMF0udmFsaWRhdGUoZGlzdGluY3RDbGllbnRTZXJ2aWNlKSApIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRpc1ZhbGlkID0gZmFsc2U7XFxuXFx0XFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdFxcdH0pO1xcblxcblxcdFxcdFxcdFxcdHJldHVybiBpc1ZhbGlkO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0YWRkTXVsdGlwbGVRdWlja1JlcG9ydCgpIHtcXG5cXHRcXHRcXHRcXHRpZih0aGlzLnZhbGlkYXRlKCkpIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmdyb3VwUmVwb3J0ID0gdGhpcy5ncm91cHJlcG9ydDtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmdyb3VwSWQgPSB0aGlzLmdyb3VwaWQ7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5jbGllbnRzSWQgPSB0aGlzLmNsaWVudHNpZDtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmNsaWVudFNlcnZpY2VzSWQgPSB0aGlzLmNsaWVudHNlcnZpY2VzO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udHJhY2tpbmdzID0gdGhpcy50cmFja2luZ3M7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kaXN0aW5jdENsaWVudFNlcnZpY2VzID0gdGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzO1xcblxcblxcdFxcdFxcdFxcdFxcdHRoaXMuZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5mb3JFYWNoKChkaXN0aW5jdENsaWVudFNlcnZpY2UsIGluZGV4KSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IHJlZiA9ICdmb3JtLScgKyBpbmRleDtcXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2FjdGlvbklkID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkO1xcblxcdFxcdFxcdFxcdFxcdFxcdGxldCBfY2F0ZWdvcnlJZCA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkO1xcblxcdFxcdFxcdFxcdFxcdFxcdGxldCBfY2F0ZWdvcnlOYW1lID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2RhdGUxID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxO1xcblxcdFxcdFxcdFxcdFxcdFxcdGxldCBfZGF0ZTIgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTI7XFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9kYXRlMyA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMztcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2RhdGVUaW1lQXJyYXkgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZVRpbWVBcnJheTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2V4dHJhRGV0YWlscyA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHM7XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5hY3Rpb25JZC5wdXNoKF9hY3Rpb25JZCk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5jYXRlZ29yeUlkLnB1c2goX2NhdGVnb3J5SWQpO1xcblxcdCAgICAgICAgXFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5jYXRlZ29yeU5hbWUucHVzaChfY2F0ZWdvcnlOYW1lKTtcXG5cXHQgICAgICAgIFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTEucHVzaChfZGF0ZTEpO1xcblxcdCAgICAgICAgXFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kYXRlMi5wdXNoKF9kYXRlMik7XFxuXFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmRhdGUzLnB1c2goX2RhdGUzKTtcXG5cXHQgICAgICAgIFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZVRpbWVBcnJheS5wdXNoKF9kYXRlVGltZUFycmF5KTtcXG5cXHQgICAgICAgIFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZXh0cmFEZXRhaWxzLnB1c2goX2V4dHJhRGV0YWlscyk7XFxuXFx0XFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3Zpc2EvcXVpY2stcmVwb3J0JylcXG5cXHRcXHRcXHQgICAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0aWYocmVzcG9uc2Uuc3VjY2Vzcykge1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdHRoaXMucmVzZXRNdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSgpO1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdFxcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdCQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcXG5cXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHR0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5tZXNzYWdlKTtcXG5cXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHRpZih0aGlzLmlzd3JpdGVyZXBvcnRwYWdlKSB7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0XFx0JCgnI2FkZC1xdWljay1yZXBvcnQtbW9kYWwnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24gKGUpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgIFxcdHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy92aXNhL3JlcG9ydC93cml0ZS1yZXBvcnQnO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdH0pO1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0XFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0XFx0bG9jYXRpb24ucmVsb2FkKCk7XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0fSwgMTAwMCk7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0fVxcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdH1cXG5cXHRcXHRcXHQgICAgICAgICAgICB9KTtcXG5cXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0fVxcblxcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y3JlYXRlZCgpIHtcXG5cXHRcXHRcXHR0aGlzLmdldFNlcnZpY2VEb2NzKCk7XFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRjb21wdXRlZDoge1xcblxcdFxcdFxcdF9pc1dyaXRlUmVwb3J0UGFnZSgpIHtcXG5cXHRcXHQgICAgICBcXHRyZXR1cm4gdGhpcy5pc3dyaXRlcmVwb3J0cGFnZSB8fCBmYWxzZTtcXG5cXHRcXHQgICAgfSxcXG5cXG5cXHRcXHRcXHRkaXN0aW5jdENsaWVudFNlcnZpY2VzKCkge1xcblxcdFxcdFxcdFxcdGxldCBkaXN0aW5jdENsaWVudFNlcnZpY2VzID0gW107XFxuXFxuXFx0XFx0XFx0XFx0dGhpcy5jbGllbnRzZXJ2aWNlcy5mb3JFYWNoKGNsaWVudFNlcnZpY2UgPT4ge1xcblxcdFxcdFxcdFxcdFxcdGlmKGRpc3RpbmN0Q2xpZW50U2VydmljZXMuaW5kZXhPZihjbGllbnRTZXJ2aWNlLmRldGFpbCkgPT0gLTEpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRkaXN0aW5jdENsaWVudFNlcnZpY2VzLnB1c2goY2xpZW50U2VydmljZS5kZXRhaWwpO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHR9KTtcXG5cXG5cXHRcXHRcXHRcXHRyZXR1cm4gZGlzdGluY3RDbGllbnRTZXJ2aWNlcztcXG5cXHRcXHRcXHR9XFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRjb21wb25lbnRzOiB7XFxuXFx0ICAgICAgICAnbXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0nOiByZXF1aXJlKCcuL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZScpXFxuXFx0ICAgIH1cXG5cXG5cXHR9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0wZjJmMmI3N1wiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXG4vLyBtb2R1bGUgaWQgPSAxMlxuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiA5IiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMGYyZjJiNzdcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMGYyZjJiNzdcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtMGYyZjJiNzdcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIE11bHRpcGxlUXVpY2tSZXBvcnQudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTBmMmYyYjc3XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMGYyZjJiNzdcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXG4vLyBtb2R1bGUgaWQgPSAxNFxuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiA5IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1lYjJmZGE0YVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9NdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBNdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtZWIyZmRhNGFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi1lYjJmZGE0YVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXG4vLyBtb2R1bGUgaWQgPSAxNVxuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiA5IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0uYWRkTXVsdGlwbGVRdWlja1JlcG9ydCgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpb3Mtc2Nyb2xsXCJcbiAgfSwgX3ZtLl9sKChfdm0uZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5sZW5ndGgpLCBmdW5jdGlvbihkaXN0aW5jdENsaWVudFNlcnZpY2UsIGluZGV4KSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsIHBhbmVsLWRlZmF1bHRcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtaGVhZGluZ1wiXG4gICAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmRpc3RpbmN0Q2xpZW50U2VydmljZXNbaW5kZXhdKSArIFwiXFxuICAgICAgICAgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiXG4gICAgfSwgW19jKCdtdWx0aXBsZS1xdWljay1yZXBvcnQtZm9ybScsIHtcbiAgICAgIGtleTogaW5kZXgsXG4gICAgICByZWY6ICdmb3JtLScgKyBpbmRleCxcbiAgICAgIHJlZkluRm9yOiB0cnVlLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJkb2NzXCI6IF92bS5kb2NzLFxuICAgICAgICBcInNlcnZpY2Vkb2NzXCI6IF92bS5zZXJ2aWNlRG9jcyxcbiAgICAgICAgXCJpbmRleFwiOiBpbmRleFxuICAgICAgfVxuICAgIH0pXSwgMSldKV0pXG4gIH0pLCAwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJzdWJtaXRcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNyZWF0ZSBSZXBvcnRcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMGYyZjJiNzdcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0wZjJmMmI3N1wifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXG4vLyBtb2R1bGUgaWQgPSAxNlxuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiA5IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ3NlbGVjdCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJuYW1lXCI6IFwiYWN0aW9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBbZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIHZhciAkJHNlbGVjdGVkVmFsID0gQXJyYXkucHJvdG90eXBlLmZpbHRlci5jYWxsKCRldmVudC50YXJnZXQub3B0aW9ucywgZnVuY3Rpb24obykge1xuICAgICAgICAgIHJldHVybiBvLnNlbGVjdGVkXG4gICAgICAgIH0pLm1hcChmdW5jdGlvbihvKSB7XG4gICAgICAgICAgdmFyIHZhbCA9IFwiX3ZhbHVlXCIgaW4gbyA/IG8uX3ZhbHVlIDogby52YWx1ZTtcbiAgICAgICAgICByZXR1cm4gdmFsXG4gICAgICAgIH0pO1xuICAgICAgICBfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiYWN0aW9uSWRcIiwgJGV2ZW50LnRhcmdldC5tdWx0aXBsZSA/ICQkc2VsZWN0ZWRWYWwgOiAkJHNlbGVjdGVkVmFsWzBdKVxuICAgICAgfSwgX3ZtLmdldENhdGVnb3JpZXNdXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5hY3Rpb25zKSwgZnVuY3Rpb24oYWN0aW9uKSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IGFjdGlvbi5pZFxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgICAgIFxcdFxcdFwiICsgX3ZtLl9zKGFjdGlvbi5uYW1lKSArIFwiXFxuXFx0XFx0ICAgICAgICAgICAgXCIpXSlcbiAgfSksIDApXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMSB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMiB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMyB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOCB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOSB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTAgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDExIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNCB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTUgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE2KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAyIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDMgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA5IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEwIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDExIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE0IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE1IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE2XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgxKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ3NlbGVjdCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZFwiXG4gICAgfSwge1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6ICghX3ZtLmN1c3RvbUNhdGVnb3J5KSxcbiAgICAgIGV4cHJlc3Npb246IFwiIWN1c3RvbUNhdGVnb3J5XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJuYW1lXCI6IFwiY2F0ZWdvcnlcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IFtmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkc2VsZWN0ZWRWYWwgPSBBcnJheS5wcm90b3R5cGUuZmlsdGVyLmNhbGwoJGV2ZW50LnRhcmdldC5vcHRpb25zLCBmdW5jdGlvbihvKSB7XG4gICAgICAgICAgcmV0dXJuIG8uc2VsZWN0ZWRcbiAgICAgICAgfSkubWFwKGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICB2YXIgdmFsID0gXCJfdmFsdWVcIiBpbiBvID8gby5fdmFsdWUgOiBvLnZhbHVlO1xuICAgICAgICAgIHJldHVybiB2YWxcbiAgICAgICAgfSk7XG4gICAgICAgIF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJjYXRlZ29yeUlkXCIsICRldmVudC50YXJnZXQubXVsdGlwbGUgPyAkJHNlbGVjdGVkVmFsIDogJCRzZWxlY3RlZFZhbFswXSlcbiAgICAgIH0sIF92bS5jaGFuZ2VDYXRlZ29yeUhhbmRsZXJdXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5jYXRlZ29yaWVzKSwgZnVuY3Rpb24oY2F0ZWdvcnkpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogY2F0ZWdvcnkuaWRcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICAgICAgXFx0XCIgKyBfdm0uX3MoY2F0ZWdvcnkubmFtZSkgKyBcIlxcblxcdFxcdCAgICAgICAgICAgIFwiKV0pXG4gIH0pLCAwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOSksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDlcIlxuICAgIH1dXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlOYW1lKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlOYW1lXCJcbiAgICB9LCB7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5jdXN0b21DYXRlZ29yeSksXG4gICAgICBleHByZXNzaW9uOiBcImN1c3RvbUNhdGVnb3J5XCJcbiAgICB9XSxcbiAgICByZWY6IFwiY2F0ZWdvcnlOYW1lXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwicGxhY2Vob2xkZXJcIjogXCJFbnRlciBjYXRlZ29yeVwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZSlcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJjYXRlZ29yeU5hbWVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImhlaWdodFwiOiBcIjEwcHhcIixcbiAgICAgIFwiY2xlYXJcIjogXCJib3RoXCJcbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tc20gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5jdXN0b21DYXRlZ29yeSA9ICFfdm0uY3VzdG9tQ2F0ZWdvcnk7XG4gICAgICAgIF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID0gbnVsbFxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdzcGFuJywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKCFfdm0uY3VzdG9tQ2F0ZWdvcnkpLFxuICAgICAgZXhwcmVzc2lvbjogXCIhY3VzdG9tQ2F0ZWdvcnlcIlxuICAgIH1dXG4gIH0sIFtfdm0uX3YoXCJVc2UgQ3VzdG9tIENhdGVnb3J5XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5jdXN0b21DYXRlZ29yeSksXG4gICAgICBleHByZXNzaW9uOiBcImN1c3RvbUNhdGVnb3J5XCJcbiAgICB9XVxuICB9LCBbX3ZtLl92KFwiVXNlIFByZWRlZmluZWQgQ2F0ZWdvcnlcIildKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMyksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTJcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTJcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDFcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdFNjaGVkdWxlZCBIZWFyaW5nIERhdGUgYW5kIFRpbWU6XFxuXFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfdm0uX2woKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlVGltZUFycmF5KSwgZnVuY3Rpb24oZGF0ZVRpbWVFbGVtZW50LCBpbmRleCkge1xuICAgIHJldHVybiBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMVwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCIsXG4gICAgICBzdGF0aWNTdHlsZToge1xuICAgICAgICBcIm1hcmdpbi1ib3R0b21cIjogXCIxMHB4XCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl9tKDQsIHRydWUpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGRhdGVUaW1lRWxlbWVudC52YWx1ZSksXG4gICAgICAgIGV4cHJlc3Npb246IFwiZGF0ZVRpbWVFbGVtZW50LnZhbHVlXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyYXJyYXlcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgICAgXCJuYW1lXCI6IFwiZGF0ZVRpbWVBcnJheVtdXCIsXG4gICAgICAgIFwicGxhY2Vob2xkZXJcIjogXCJ5eXl5LW1tLWRkIGhoOm1tOnNzXCIsXG4gICAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IChkYXRlVGltZUVsZW1lbnQudmFsdWUpXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgICAgX3ZtLiRzZXQoZGF0ZVRpbWVFbGVtZW50LCBcInZhbHVlXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMSB0ZXh0LWNlbnRlclwiXG4gICAgfSwgW19jKCdidXR0b24nLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgdmFsdWU6IChpbmRleCAhPSAwKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJpbmRleCAhPSAwXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kYW5nZXIgYnRuLXNtXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLnJlbW92ZURhdGVUaW1lUGlja2VyQXJyYXkoaW5kZXgpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX2MoJ2knLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmYSBmYS10aW1lc1wiXG4gICAgfSldKV0pXSlcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMTBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1zbSBwdWxsLXJpZ2h0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0uYWRkRGF0ZVRpbWVQaWNrZXJBcnJheVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXBsdXNcIlxuICB9KSwgX3ZtLl92KFwiIEFkZFxcbiAgICAgICAgICAgICAgICAgICAgXCIpXSldLCAyKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDYgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNyB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxMCB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNCB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNSksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNiB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDcgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxMCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDE0IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTVcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdFNjaGVkdWxlZCBBcHBvaW50bWVudCBEYXRlIGFuZCBUaW1lOlxcblxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSg1KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlM1wiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRldGltZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTNcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAyXCJcbiAgICB9XVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMyksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDNcIlxuICAgIH1dXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA0KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNFwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oNiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hvc2VuLXNlbGVjdC1mb3ItZG9jcy0yXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwid2lkdGhcIjogXCIzNTBweFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiAnY2hvc2VuLScgKyBfdm0uaW5kZXgsXG4gICAgICBcImRhdGEtcGxhY2Vob2xkZXJcIjogXCJTZWxlY3QgRG9jc1wiLFxuICAgICAgXCJtdWx0aXBsZVwiOiBcIlwiLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIjRcIlxuICAgIH1cbiAgfSwgX3ZtLl9sKChfdm0uc2VydmljZWRvY3MpLCBmdW5jdGlvbihzZXJ2aWNlZG9jKSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IHNlcnZpY2Vkb2MudGl0bGVcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdFwiICsgX3ZtLl9zKChzZXJ2aWNlZG9jLnRpdGxlKS50cmltKCkpICsgXCJcXG5cXHRcXHQgICAgICAgICAgICAgICAgXCIpXSlcbiAgfSksIDApXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA2KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNlwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oNyksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oOCksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTRcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTRcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdENvbnNlcXVlbmNlIG9mIG5vbiBzdWJtaXNzaW9uOlxcblxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIFtfYygnbGFiZWwnLCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscyksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlsc1wiXG4gICAgfV0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImNoZWNrYm94XCIsXG4gICAgICBcInZhbHVlXCI6IFwiQWRkaXRpb25hbCBjb3N0IG1heWJlIGNoYXJnZWQuXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcImNoZWNrZWRcIjogQXJyYXkuaXNBcnJheShfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKSA/IF92bS5faShfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzLCBcIkFkZGl0aW9uYWwgY29zdCBtYXliZSBjaGFyZ2VkLlwiKSA+IC0xIDogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIHZhciAkJGEgPSBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzLFxuICAgICAgICAgICQkZWwgPSAkZXZlbnQudGFyZ2V0LFxuICAgICAgICAgICQkYyA9ICQkZWwuY2hlY2tlZCA/ICh0cnVlKSA6IChmYWxzZSk7XG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KCQkYSkpIHtcbiAgICAgICAgICB2YXIgJCR2ID0gXCJBZGRpdGlvbmFsIGNvc3QgbWF5YmUgY2hhcmdlZC5cIixcbiAgICAgICAgICAgICQkaSA9IF92bS5faSgkJGEsICQkdik7XG4gICAgICAgICAgaWYgKCQkZWwuY2hlY2tlZCkge1xuICAgICAgICAgICAgJCRpIDwgMCAmJiAoX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGEuY29uY2F0KFskJHZdKSkpXG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQkaSA+IC0xICYmIChfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYS5zbGljZSgwLCAkJGkpLmNvbmNhdCgkJGEuc2xpY2UoJCRpICsgMSkpKSlcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGMpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgXFx0XFx0XFx0QWRkaXRpb25hbCBjb3N0IG1heWJlIGNoYXJnZWQuXFxuXFx0XFx0XFx0ICAgIFxcdFxcdFwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCBbX2MoJ2xhYmVsJywgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHNcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiLFxuICAgICAgXCJ2YWx1ZVwiOiBcIkFwcGxpY2F0aW9uIG1heWJlIGZvcmZlaXRlZCBhbmQgaGFzIHRvIHJlLWFwcGx5LlwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IEFycmF5LmlzQXJyYXkoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscykgPyBfdm0uX2koX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscywgXCJBcHBsaWNhdGlvbiBtYXliZSBmb3JmZWl0ZWQgYW5kIGhhcyB0byByZS1hcHBseS5cIikgPiAtMSA6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRhID0gX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscyxcbiAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAkJGMgPSAkJGVsLmNoZWNrZWQgPyAodHJ1ZSkgOiAoZmFsc2UpO1xuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSgkJGEpKSB7XG4gICAgICAgICAgdmFyICQkdiA9IFwiQXBwbGljYXRpb24gbWF5YmUgZm9yZmVpdGVkIGFuZCBoYXMgdG8gcmUtYXBwbHkuXCIsXG4gICAgICAgICAgICAkJGkgPSBfdm0uX2koJCRhLCAkJHYpO1xuICAgICAgICAgIGlmICgkJGVsLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICQkaSA8IDAgJiYgKF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRhLmNvbmNhdChbJCR2XSkpKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkJGkgPiAtMSAmJiAoX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGEuc2xpY2UoMCwgJCRpKS5jb25jYXQoJCRhLnNsaWNlKCQkaSArIDEpKSkpXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRjKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgIFxcdFxcdFxcdEFwcGxpY2F0aW9uIG1heWJlIGZvcmZlaXRlZCBhbmQgaGFzIHRvIHJlLWFwcGx5LlxcblxcdFxcdFxcdCAgICBcXHRcXHRcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2JywgW19jKCdsYWJlbCcsIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiY2hlY2tib3hcIixcbiAgICAgIFwidmFsdWVcIjogXCJJbml0aWFsIGRlcG9zaXQgbWF5YmUgZm9yZmVpdGVkLlwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IEFycmF5LmlzQXJyYXkoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscykgPyBfdm0uX2koX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscywgXCJJbml0aWFsIGRlcG9zaXQgbWF5YmUgZm9yZmVpdGVkLlwiKSA+IC0xIDogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIHZhciAkJGEgPSBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzLFxuICAgICAgICAgICQkZWwgPSAkZXZlbnQudGFyZ2V0LFxuICAgICAgICAgICQkYyA9ICQkZWwuY2hlY2tlZCA/ICh0cnVlKSA6IChmYWxzZSk7XG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KCQkYSkpIHtcbiAgICAgICAgICB2YXIgJCR2ID0gXCJJbml0aWFsIGRlcG9zaXQgbWF5YmUgZm9yZmVpdGVkLlwiLFxuICAgICAgICAgICAgJCRpID0gX3ZtLl9pKCQkYSwgJCR2KTtcbiAgICAgICAgICBpZiAoJCRlbC5jaGVja2VkKSB7XG4gICAgICAgICAgICAkJGkgPCAwICYmIChfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYS5jb25jYXQoWyQkdl0pKSlcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCRpID4gLTEgJiYgKF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRhLnNsaWNlKDAsICQkaSkuY29uY2F0KCQkYS5zbGljZSgkJGkgKyAxKSkpKVxuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYylcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIlxcblxcdFxcdFxcdCAgICBcXHRcXHRcXHRJbml0aWFsIGRlcG9zaXQgbWF5YmUgZm9yZmVpdGVkLlxcblxcdFxcdFxcdCAgICBcXHRcXHRcIildKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA3KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gN1wiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oOSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMTApLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGU1XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlNVwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOCksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDhcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDExKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1kYXRlcmFuZ2UgaW5wdXQtZ3JvdXBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlNlwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LXNtIGZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlNlwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW192bS5fdihcInRvXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTdcIixcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTdcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdFRhcmdldCBGaWxsaW5nIERhdGU6XFxuXFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDEyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlOFwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlOFwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOSksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDlcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDEzKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1kYXRlcmFuZ2UgaW5wdXQtZ3JvdXBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlOVwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LXNtIGZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlOVwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW192bS5fdihcInRvXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTEwXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtc20gZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUxMFwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTApLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMFwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMTQpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDE1KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlMTFcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTExXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMSksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDExXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgxNiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMTcpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUxMlwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlMTJcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEyKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTJcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDE4KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgxOSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTEzXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlMTNcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEzKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTNcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDIwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgyMSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTE0XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlMTRcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE0KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTRcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEyOCksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTI4XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgyMiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgcmVmOiBcImNvbXBhbnlfY291cmllclwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwiY29tcGFueV9jb3VyaWVyXCJcbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLmNvbXBhbnlDb3VyaWVycyksIGZ1bmN0aW9uKGNvbXBhbnlDb3VyaWVyKSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IGNvbXBhbnlDb3VyaWVyLmFkZHJlc3MuY29udGFjdF9udW1iZXJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHRcIiArIF92bS5fcyhjb21wYW55Q291cmllci5maXJzdF9uYW1lICsgJyAnICsgY29tcGFueUNvdXJpZXIubGFzdF9uYW1lKSArIFwiIFxcblxcdFxcdFxcdCAgICAgICAgXFx0XFx0KFwiICsgX3ZtLl9zKGNvbXBhbnlDb3VyaWVyLmFkZHJlc3MuY29udGFjdF9udW1iZXIpICsgXCIpXFxuXFx0XFx0XFx0ICAgICAgICBcXHRcIildKVxuICB9KSwgMCldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkICE9IDEyOCksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgIT0gMTI4XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgyMyksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwidHJhY2tpbmdfbnVtYmVyXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJ0cmFja2luZ19udW1iZXJcIlxuICAgIH1cbiAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDI0KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgyNSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTE1XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlMTVcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE1KSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTVcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc2IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc3IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc4IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc5IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgwIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgxIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgyIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgzKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3NiB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc3IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzggfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3OSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgwIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODEgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MiB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgzXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgyNiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMjcpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUxNlwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlMTZcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICAgICAgQWN0aW9uOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICBDYXRlZ29yeTogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RG9jdW1lbnRzOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdFN1Ym1pc3Npb24gRGF0ZTogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRFc3RpbWF0ZWQgVGltZSBvZiBGaW5pc2hpbmc6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0QWZmZWN0ZWQgRGF0ZTogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHREYXRlIFJhbmdlOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdEV4dGVuZGVkIHBlcmlvZCBmb3IgcHJvY2Vzc2luZzogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RGF0ZSBhbmQgVGltZSBQaWNrIFVwOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdERhdGUgYW5kIFRpbWUgRGVsaXZlcjogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRDb21wYW55IENvdXJpZXI6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0VHJhY2tpbmcgTnVtYmVyOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdERhdGUgYW5kIFRpbWUgRGVsaXZlcmVkOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdEVzdGltYXRlZCBSZWxlYXNpbmcgRGF0ZTogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWViMmZkYTRhXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtZWIyZmRhNGFcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWVcbi8vIG1vZHVsZSBpZCA9IDE4XG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDkiLCJWdWUuY29tcG9uZW50KCdhY3Rpb24tbG9ncycsICByZXF1aXJlKCcuLi9jbGllbnRzL0FjdGlvbkxvZ3MudnVlJykpO1xuVnVlLmNvbXBvbmVudCgndHJhbnNhY3Rpb24tbG9ncycsICByZXF1aXJlKCcuLi9jbGllbnRzL1RyYW5zYWN0aW9uTG9ncy52dWUnKSk7XG5cbmxldCBkYXRhID0gd2luZG93LkxhcmF2ZWwudXNlcjtcblxubmV3IFZ1ZSh7XG5cblx0ZWw6ICcjYXBwJyxcblxuXHRkYXRhOiB7XG5cblx0XHRncm91cElkOiBudWxsLCAvLyBPa1xuXG5cdFx0ZGV0YWlsczogbnVsbCwgLy8gT2tcblxuXHRcdG1lbWJlcnM6IG51bGwsIC8vIE9rXG5cblx0XHRzZXJ2aWNlQ29zdDogW10sIC8vIE9rXG5cblx0XHRzZUxjbGllbnRJZDogJycsXG5cblx0XHRhc3NpZ25MZWFkZXJGb3JtOiBuZXcgRm9ybSh7IC8vIE9rXG5cdFx0XHRjbGllbnRJZDogJycsXG5cdFx0XHR0eXBlOicnXG5cdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSksXG5cblx0XHRxdWlja1JlcG9ydENsaWVudHNJZDogW10sXG5cdFx0cXVpY2tSZXBvcnRDbGllbnRTZXJ2aWNlc0lkOiBbXSxcblx0XHRxdWlja1JlcG9ydFRyYWNraW5nczogW10sXG5cblx0XHRhY3Rpb25Mb2dzOiBbXSxcblx0XHRjdXJyZW50X3llYXI6IG51bGwsXG5cdFx0Y3VycmVudF9tb250aDogbnVsbCxcblx0XHRjdXJyZW50X2RheTogbnVsbCxcblx0XHRjX3llYXI6IG51bGwsXG5cdFx0Y19tb250aDogbnVsbCxcblx0XHRjX2RheTogbnVsbCxcblx0XHRzX2NvdW50OiAxLFxuXG5cdFx0dHJhbnNhY3Rpb25Mb2dzOiBbXSxcblx0XHRjdXJyZW50X3llYXIyOiBudWxsLFxuXHRcdGN1cnJlbnRfbW9udGgyOiBudWxsLFxuXHRcdGN1cnJlbnRfZGF5MjogbnVsbCxcblx0XHRjX3llYXIyOiBudWxsLFxuXHRcdGNfbW9udGgyOiBudWxsLFxuXHRcdGNfZGF5MjogbnVsbCxcblx0XHRzX2NvdW50MjogMSxcblxuXHRcdGRlcG9zaXRzOiBbXSxcblx0XHRwYXltZW50czogW10sXG5cdFx0cmVmdW5kczogW10sXG5cdFx0ZGlzY291bnRzOiBbXSxcblxuXHRcdGdTZXJ2aWNlczogbnVsbCwgXG5cdFx0Z0JhdGNoOiBudWxsLCBcbiBcblx0XHRkb2NzcmN2OiBudWxsLCBcblxuICAgICAgICBzZXR0aW5nOiBkYXRhLmFjY2Vzc19jb250cm9sWzBdLnNldHRpbmcsXG4gICAgICAgIHBlcm1pc3Npb25zOiBkYXRhLnBlcm1pc3Npb25zLFxuICAgICAgICBzZWxwZXJtaXNzaW9uczogW3tcblx0XHRcdGVkaXRBZGRyZXNzOjAsXG5cdFx0XHRhY3Rpb25Mb2dzOjAsXG5cdFx0XHR0cmFuc2FjdGlvbkxvZ3M6MCxcblx0XHRcdGRlcG9zaXRzOjAsXG5cdFx0XHRwYXltZW50czowLFxuXHRcdFx0cmVmdW5kczowLFxuXHRcdFx0ZGlzY291bnRzOjAsXG5cdFx0XHRuZXdNZW1iZXI6MCxcblx0XHRcdGFkZE5ld1NlcnZpY2U6MCxcblx0XHRcdGFkZEZ1bmRzOjAsXG5cdFx0XHR3cml0ZVJlcG9ydDowLFxuXHRcdFx0bWFrZUxlYWRlcjowLFxuXHRcdFx0dHJhbnNmZXI6MCxcblx0XHRcdGRlbGV0ZTowLFxuXHRcdFx0ZWRpdFNlcnZpY2U6MFxuICAgICAgICB9XVxuXG5cdH0sXG5cblx0bWV0aG9kczoge1xuXHRcdC8vIE9rXG5cdFx0aW5pdCgpIHtcblxuXHRcdFx0bGV0IHBhdGhBcnJheSA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZS5zcGxpdCggJy8nICk7XG5cdFx0XHR0aGlzLmdyb3VwSWQgPSBwYXRoQXJyYXlbM107XG5cblx0XHRcdHRoaXMuZmV0Y2hEZXRhaWxzKCk7XG5cdFx0XHR0aGlzLmZldGNoTWVtYmVycygpO1xuXG5cdFx0XHR0aGlzLmZldGNoR1NlcnZpY2VzKCk7XG5cdFx0XHR0aGlzLmZldGNoR0JhdGNoKCk7XG5cdFx0fSxcblxuXHRcdC8vIE9rXG5cdFx0ZmV0Y2hEZXRhaWxzKCkge1xuXHRcdFx0bGV0IGdyb3VwSWQgPSB0aGlzLmdyb3VwSWQ7XG5cblx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9ncm91cC8nICsgZ3JvdXBJZCArICcvc3VtbWFyeScpXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHR0aGlzLmRldGFpbHMgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHR9LFxuXG5cdFx0Ly8gT2tcblx0XHRmZXRjaE1lbWJlcnMoKSB7XG5cdFx0XHRsZXQgZ3JvdXBJZCA9IHRoaXMuZ3JvdXBJZDtcblxuXHRcdFx0YXhpb3NBUEl2MS5nZXQoJy92aXNhL2dyb3VwLycgKyBncm91cElkICsgJy9tZW1iZXJzJylcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHRoaXMubWVtYmVycyA9IHJlc3BvbnNlLmRhdGEuZ3JvdXBtZW1iZXI7XG5cblx0XHRcdFx0XHR0aGlzLiRyZWZzLmNsaWVudHNlcnZpY2VzcmVmLmNyZWF0ZURhdGF0YWJsZSgnbWVtYmVyc0RhdGF0YWJsZScpO1xuXG5cdFx0XHRcdFx0dGhpcy5jb21wdXRlU2VydmljZUNvc3QodGhpcy5tZW1iZXJzKTtcblxuXHRcdFx0XHRcdHRoaXMuY3JlYXRlRGF0YXRhYmxlVG9nZ2xlUm93KHRoaXMubWVtYmVycyk7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuXHRcdH0sXG5cblx0XHQvLyBPa1xuXHRcdGZldGNoRG9jcyhpZCkge1xuXHRcdFx0dGhpcy4kcmVmcy5tdWx0aXBsZXF1aWNrcmVwb3J0cmVmLmZldGNoRG9jcyhpZCk7XG5cdFx0fSxcblxuXHRcdC8vIE9rXG5cdFx0Y29tcHV0ZVNlcnZpY2VDb3N0KG1lbWJlcnMpIHtcblx0XHRcdGxldCBncm91cElkID0gdGhpcy5ncm91cElkO1xuXG5cdFx0XHR0aGlzLnNlcnZpY2VDb3N0ID0gbWVtYmVycy5tYXAobWVtYmVyID0+IHtcblx0XHRcdFx0bGV0IGNvc3QgPSAwO1xuXHRcdFx0XHRsZXQgY2hhcmdlID0gMDtcblx0XHRcdFx0bGV0IHRpcCA9IDA7XG5cblx0XHRcdFx0bWVtYmVyLnNlcnZpY2VzLm1hcChzZXJ2aWNlID0+IHtcblx0XHRcdFx0XHRpZihzZXJ2aWNlLmFjdGl2ZSA9PSAxICYmIHNlcnZpY2UuZ3JvdXBfaWQgPT0gZ3JvdXBJZCkge1xuXHRcdFx0XHRcdFx0Y29zdCArPSBwYXJzZUZsb2F0KHNlcnZpY2UuY29zdCk7XG5cdFx0XHRcdFx0XHRjaGFyZ2UgKz0gcGFyc2VGbG9hdChzZXJ2aWNlLmNoYXJnZSk7XG5cdFx0XHRcdFx0XHR0aXAgKz0gcGFyc2VGbG9hdChzZXJ2aWNlLnRpcCk7XHRcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdHJldHVybiAoY29zdCArIGNoYXJnZSArIHRpcCk7XG5cdFx0XHR9KTtcblx0XHR9LFxuXG5cdFx0Ly8gT2tcblx0XHRmZXRjaEdTZXJ2aWNlcygpIHtcblx0XHRcdGxldCBncm91cElkID0gdGhpcy5ncm91cElkO1xuXG5cdFx0XHRheGlvc0FQSXYxLmdldCgnL3Zpc2EvZ3JvdXAvJyArIGdyb3VwSWQgKyAnL3NlcnZpY2VzJylcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdC8vY29uc29sZS5sb2cocmVzcG9uc2UuZGF0YSk7XG5cdFx0XHRcdFx0dGhpcy5nU2VydmljZXMgPSByZXNwb25zZS5kYXRhO1xuXG5cdFx0XHRcdFx0dGhpcy5jcmVhdGVEYXRhdGFibGVUb2dnbGVCeVNlcnZpY2UodGhpcy5nU2VydmljZXMpO1xuXG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuXHRcdH0sXG5cblx0XHRmZXRjaEdCYXRjaCgpIHtcblx0XHRcdGxldCBncm91cElkID0gdGhpcy5ncm91cElkO1xuXG5cdFx0XHRheGlvc0FQSXYxLmdldCgnL3Zpc2EvZ3JvdXAvJyArIGdyb3VwSWQgKyAnL2JhdGNoJylcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdC8vY29uc29sZS5sb2cocmVzcG9uc2UuZGF0YSk7XG5cdFx0XHRcdFx0dGhpcy5nQmF0Y2ggPSByZXNwb25zZS5kYXRhO1xuXG5cdFx0XHRcdFx0dGhpcy5jcmVhdGVEYXRhdGFibGVUb2dnbGVCeUJhdGNoKHRoaXMuZ0JhdGNoKTtcblxuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHR9LFxuXG5cdFx0Ly8gT2tcblx0XHRhc3NpZ25MZWFkZXIoY2xpZW50SWQsIHR5cGUpIHtcblx0XHRcdGxldCBncm91cElkID0gdGhpcy5ncm91cElkO1xuXG5cdFx0XHR0aGlzLmFzc2lnbkxlYWRlckZvcm0uY2xpZW50SWQgPSBjbGllbnRJZDtcblx0XHRcdHRoaXMuYXNzaWduTGVhZGVyRm9ybS50eXBlID0gdHlwZTtcblxuXHRcdFx0dGhpcy5hc3NpZ25MZWFkZXJGb3JtLnN1Ym1pdCgncGF0Y2gnLCAnL3Zpc2EvZ3JvdXAvJyArIGdyb3VwSWQgKyAnL2Fzc2lnbi1sZWFkZXInKVxuICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICAgICAgICBpZihyZXN1bHQuc3VjY2Vzcykge1xuXG4gICAgICAgICAgICAgICAgXHR0aGlzLmZldGNoTWVtYmVycygpO1xuICAgICAgICAgICAgICAgIFx0dGhpcy5mZXRjaERldGFpbHMoKTtcblx0XHRcdFx0XHQkKCcjdmljZS1sZWFkZXItbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKHJlc3VsdC5tZXNzYWdlKTtcblx0XHRcdFx0fVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuXG4gICAgICAgICAgICB0aGlzLnNlTGNsaWVudElkID0gXCJcIjtcblx0XHR9LFxuXG5cdFx0Ly8gT2tcblx0XHRzaG93TWVtYmVyUm93KGlkLCBlKSB7XG5cdFx0XHRpZihlLnRhcmdldC5ub2RlTmFtZSAhPSAnQScpIHtcblx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHQkKCcjbWVtYmVyLXJvdy0nICsgaWQpLnRvZ2dsZUNsYXNzKCdoaWRlJykucmVtb3ZlQXR0cignc3R5bGUnKTtcblx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0Y3JlYXRlRGF0YXRhYmxlKGNsYXNzTmFtZSkge1xuXHRcdFx0JCgndGFibGUuJytjbGFzc05hbWUpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0JCgndGFibGUuJytjbGFzc05hbWUpLkRhdGFUYWJsZSh7XG5cdFx0XHQgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxuXHRcdFx0ICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxuXHRcdFx0ICAgICAgICBcImFhU29ydGluZ1wiOiBbXSxcblx0XHRcdCAgICAgICAgZG9tOiAnPFwidG9wXCJsZj5ydDxcImJvdHRvbVwiaXA+PFwiY2xlYXJcIj4nXG5cdFx0XHQgICAgfSk7XG5cdFx0XHR9LCA1MDApO1xuXHRcdH0sXG5cblx0XHRmaWx0ZXJDdXJyZW5jeSh2YWx1ZSkge1xuXHRcdFx0aWYodmFsdWUgIT0gbnVsbCl7XG5cdFx0XHQgICAgaWYodmFsdWUlMSAhPSAwKXtcblx0XHRcdCAgICAgICByZXR1cm4gIHRoaXMubnVtYmVyV2l0aENvbW1hcyhwYXJzZUZsb2F0KHZhbHVlKS50b0ZpeGVkKDIpKTtcblx0XHRcdCAgICB9IGVsc2Uge1xuXHRcdFx0ICAgICAgIHJldHVybiAgdGhpcy5udW1iZXJXaXRoQ29tbWFzKHBhcnNlSW50KHZhbHVlKSk7XG5cdFx0XHQgICAgfVxuXHRcdFx0fSBlbHNle1xuXHRcdFx0XHRyZXR1cm4gMDtcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0bnVtYmVyV2l0aENvbW1hcyh2YWx1ZSkge1xuXHRcdFx0cmV0dXJuIHZhbHVlLnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIsXCIpO1xuXHRcdH0sXG5cblx0XHRjcmVhdGVRdWlja1JlcG9ydChjbGllbnRJZCwgY2xpZW50U2VydmljZUlkLCB0cmFja2luZykge1xuXHRcdFx0bGV0IGdyb3VwSWQgPSB0aGlzLmdyb3VwSWQ7XG5cblx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9ncm91cC8nICsgZ3JvdXBJZCArICcvY2xpZW50LXNlcnZpY2UvJyArIGNsaWVudFNlcnZpY2VJZClcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdGxldCBjbGllbnRTZXJ2aWNlID0gcmVzcG9uc2UuZGF0YTtcblxuXHRcdFx0XHRcdHRoaXMucXVpY2tSZXBvcnRDbGllbnRzSWQgPSBbY2xpZW50SWRdO1xuXHRcdFx0XHRcdHRoaXMucXVpY2tSZXBvcnRDbGllbnRTZXJ2aWNlc0lkID0gY2xpZW50U2VydmljZTtcblx0XHRcdFx0XHR0aGlzLnF1aWNrUmVwb3J0VHJhY2tpbmdzID0gW3RyYWNraW5nXTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHR0aGlzLmZldGNoRG9jcyhjbGllbnRTZXJ2aWNlKTtcblxuXHRcdFx0XHRcdCQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykubW9kYWwoJ3Nob3cnKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG5cdFx0fSxcblxuXHRcdGNyZWF0ZURhdGF0YWJsZVRvZ2dsZVJvdyhkYXRhKSB7XG5cdFx0XHR2YXIgZ3JvdXBJZCA9IHRoaXMuZ3JvdXBJZDtcblxuXHRcdFx0bGV0IHNlcnZpY2VDb3N0ID0gZGF0YS5tYXAobWVtYmVyID0+IHtcblx0XHRcdFx0bGV0IGNvc3QgPSAwO1xuXHRcdFx0XHRsZXQgY2hhcmdlID0gMDtcblx0XHRcdFx0bGV0IHRpcCA9IDA7XG5cblx0XHRcdFx0bWVtYmVyLnNlcnZpY2VzLm1hcChzZXJ2aWNlID0+IHtcblx0XHRcdFx0XHRpZihzZXJ2aWNlLmFjdGl2ZSA9PSAxICYmIHNlcnZpY2UuZ3JvdXBfaWQgPT0gZ3JvdXBJZCkge1xuXHRcdFx0XHRcdFx0Y29zdCArPSBwYXJzZUZsb2F0KHNlcnZpY2UuY29zdCk7XG5cdFx0XHRcdFx0XHRjaGFyZ2UgKz0gcGFyc2VGbG9hdChzZXJ2aWNlLmNoYXJnZSk7XG5cdFx0XHRcdFx0XHR0aXAgKz0gcGFyc2VGbG9hdChzZXJ2aWNlLnRpcCk7XHRcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdHJldHVybiB0aGlzLmZpbHRlckN1cnJlbmN5KGNvc3QgKyBjaGFyZ2UgKyB0aXApO1xuXHRcdFx0fSk7XG5cblx0XHRcdCQoJ3RhYmxlLmR0LW1lbWJlcnMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0XHRcdHZhciB2aSA9IHRoaXM7XG5cblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cblx0XHRcdFx0dmFyIGZvcm1hdCA9IGZ1bmN0aW9uKGQpIHtcblx0XHRcdFx0XHRsZXQgY2lkID0gbnVsbDtcblx0XHRcdFx0XHRsZXQgdGFibGUgPSAnPHRhYmxlIGNsYXNzPVwidGFibGUgdGFibGUtYm9yZGVyZWQgdGFibGUtc3RyaXBlZCB0YWJsZS1ob3ZlclwiPicrXG5cdFx0XHRcdFx0XHRcdFx0Jzx0aGVhZD4nK1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRyPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5QYWNrYWdlPC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGg+RGF0ZSBSZWNvcmRlZDwvdGg+Jytcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRoPkRldGFpbHM8L3RoPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5TdGF0dXM8L3RoPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5Db3N0PC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGg+U2VydmljZSBGZWU8L3RoPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5BZGRpdGlvbmFsIEZlZTwvdGg+Jytcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRoPkRpc2NvdW50PC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGggY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPkFjdGlvbjwvdGg+Jytcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC90cj4nK1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgICc8L3RoZWFkPicrXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgJzx0Ym9keT4nO1xuXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgZm9yKHZhciBpPTA7IGk8KGQuc2VydmljZXMpLmxlbmd0aDsgaSsrKSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRjaWQgPSBkLmNsaWVudC5kb2N1bWVudF9yZWNlaXZlO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0bGV0IGNsaWVudFNlcnZpY2VJZCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoZC5zZXJ2aWNlc1tpXSkpLmlkO1xuXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR2YXIgY2xhc3NOYW1lID0gKGQuc2VydmljZXNbaV0uYWN0aXZlID09IDApID8gJ3N0cmlrZXRocm91Z2gnIDogJyc7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR2YXIgdmlzaWJpbGl0eSA9IChkLnNlcnZpY2VzW2ldLmdyb3VwX2lkID09IGdyb3VwSWQpID8gJ3RhYmxlLXJvdycgOiAnbm9uZSc7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRpZihkLnNlcnZpY2VzW2ldLnJlbWFya3MpIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dmFyIHJlbWFya3MgPSBkLnNlcnZpY2VzW2ldLnJlbWFya3M7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHZhciBkZXRhaWxzID0gJzxhIGhyZWY9XCIjXCIgZGF0YS10b2dnbGU9XCJwb3BvdmVyXCIgZGF0YS1wbGFjZW1lbnQ9XCJyaWdodFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIGRhdGEtY29udGVudD1cIicrcmVtYXJrcysnXCIgZGF0YS10cmlnZ2VyPVwiaG92ZXJcIj4nK2Quc2VydmljZXNbaV0uZGV0YWlsKyc8L2E+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdH0gZWxzZSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHZhciBkZXRhaWxzID0gZC5zZXJ2aWNlc1tpXS5kZXRhaWw7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR9XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRpZihkLnNlcnZpY2VzW2ldLmRpc2NvdW50Mi5sZW5ndGggPiAwKSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdGlmKGQuc2VydmljZXNbaV0uZGlzY291bnQyWzBdLnJlYXNvbikge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHRcdHZhciByZWFzb24gPSBkLnNlcnZpY2VzW2ldLmRpc2NvdW50MlswXS5yZWFzb247XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdFx0dmFyIGRpc2NvdW50ID0gJzxhIGhyZWY9XCIjXCIgZGF0YS10b2dnbGU9XCJwb3BvdmVyXCIgZGF0YS1wbGFjZW1lbnQ9XCJsZWZ0XCIgZGF0YS1jb250YWluZXI9XCJib2R5XCIgZGF0YS1jb250ZW50PVwiJytyZWFzb24rJ1wiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCI+JytkLnNlcnZpY2VzW2ldLmRpc2NvdW50MlswXS5kaXNjb3VudF9hbW91bnQrJzwvYT4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR9IGVsc2Uge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHRcdHZhciBkaXNjb3VudCA9IGQuc2VydmljZXNbaV0uZGlzY291bnQyWzBdLmRpc2NvdW50X2Ftb3VudDtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0fVxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0fSBlbHNlIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dmFyIGRpc2NvdW50ID0gJyZuYnNwOyc7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR9XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR2YXIgX2FjdGlvbiA9ICc8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJlZGl0LXNlcnZpY2UtYWN0aW9uXCIgZGF0YS10b2dnbGU9XCJtb2RhbFwiIGRhdGEtdGFyZ2V0PVwiI2VkaXQtc2VydmljZS1tb2RhbFwiIGRhdGEtc2VydmljZWNsaWVudGlkPVwiJytkLnNlcnZpY2VzW2ldLmNsaWVudF9pZCsnXCIgZGF0YS1zZXJ2aWNlc2VydmljZWlkPVwiJytkLnNlcnZpY2VzW2ldLnNlcnZpY2VfaWQrJ1wiIGRhdGEtc2VydmljZWlkPVwiJytkLnNlcnZpY2VzW2ldLmlkKydcIj48aSBjbGFzcz1cImZhIGZhLXBlbmNpbC1zcXVhcmUtb1wiIGRhdGEtc2VydmljZWNsaWVudGlkPVwiJytkLnNlcnZpY2VzW2ldLmNsaWVudF9pZCsnXCIgZGF0YS1zZXJ2aWNlc2VydmljZWlkPVwiJytkLnNlcnZpY2VzW2ldLnNlcnZpY2VfaWQrJ1wiIGRhdGEtc2VydmljZWlkPVwiJytkLnNlcnZpY2VzW2ldLmlkKydcIj48L2k+PC9hPiAmbmJzcDsgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwid3JpdGUtcmVwb3J0LWFjdGlvblwiIGRhdGEtY2xpZW50aWQ9XCInK2Quc2VydmljZXNbaV0uY2xpZW50X2lkKydcIiBkYXRhLXRyYWNraW5nPVwiJytkLnNlcnZpY2VzW2ldLnRyYWNraW5nKydcIiBkYXRhLWNsaWVudHNlcnZpY2VpZD1cIicrY2xpZW50U2VydmljZUlkKydcIj48aSBjbGFzcz1cImZhIGZhLWxpc3QtYWx0XCIgZGF0YS1jbGllbnRpZD1cIicrZC5zZXJ2aWNlc1tpXS5jbGllbnRfaWQrJ1wiIGRhdGEtdHJhY2tpbmc9XCInK2Quc2VydmljZXNbaV0udHJhY2tpbmcrJ1wiIGRhdGEtY2xpZW50c2VydmljZWlkPVwiJytjbGllbnRTZXJ2aWNlSWQrJ1wiPjwvaT48L2E+JztcblxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0dGFibGUgKz0gJzx0ciBjbGFzcz1cIicrY2xhc3NOYW1lKydcIiBzdHlsZT1cImRpc3BsYXk6Jyt2aXNpYmlsaXR5KydcIj4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR0YWJsZSArPSAnPHRkPicrZC5zZXJ2aWNlc1tpXS50cmFja2luZysnPC90ZD4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR0YWJsZSArPSAnPHRkPicrZC5zZXJ2aWNlc1tpXS5zZXJ2aWNlX2RhdGUrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK2RldGFpbHMrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK2Quc2VydmljZXNbaV0uc3RhdHVzKyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHRhYmxlICs9ICc8dGQ+JytkLnNlcnZpY2VzW2ldLmNvc3QrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK2Quc2VydmljZXNbaV0uY2hhcmdlKyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHRhYmxlICs9ICc8dGQ+JytkLnNlcnZpY2VzW2ldLnRpcCsnPC90ZD4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR0YWJsZSArPSAnPHRkPicrZGlzY291bnQrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZCBjbGFzcz1cInRleHQtY2VudGVyXCI+JytfYWN0aW9uKyc8L3RkPic7XG5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdHRhYmxlICs9ICc8L3RyPic7XG5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICB9XG5cblx0XHQgICAgICAgICAgICAgICAgdGFibGUgKz0gJzwvdGJvZHk+PC90YWJsZT4nO1xuXG5cdFx0ICAgICAgICAgICAgbGV0IHJjdiA9IGNpZDtcblx0XHRcdFx0XHRsZXQgZG9jcmN2ID0gJzx1bCBjbGFzcz1cInRhZy1saXN0XCIgc3R5bGU9XCJwYWRkaW5nOiAwXCI+Jztcblx0XHQgICAgICAgICAgICBmb3IodmFyIGk9MDsgaTxyY3YubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0XHRcdGRvY3JjdiA9IGRvY3JjdiArJzxsaT48YSBzdHlsZT1cImN1cnNvcjpkZWZhdWx0O1wiPjxpIGNsYXNzPVwiZmEgZmEtdGFnXCI+PC9pPicrcmN2W2ldKyc8L2E+PC9saT4nO1xuXHRcdFx0XHRcdH1cbiAgICAgICAgICAgICAgICAgICAgZG9jcmN2ID0gZG9jcmN2ICsnPC91bD4nO1xuXG5cdFx0ICAgICAgICAgICAgcmV0dXJuIGRvY3Jjdit0YWJsZTtcblx0XHRcdFx0fS5iaW5kKHRoaXMpO1xuXG5cdFx0ICAgICAgICAvL2lmKHRoaXMuc2VscGVybWlzc2lvbnMubWFrZUxlYWRlciA9PSAxICYmIHRoaXMuc2VscGVybWlzc2lvbnMudHJhbnNmZXIgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmRlbGV0ZSA9PSAxKXtcblx0XHRcdFx0XHR0YWJsZSA9ICQoJ3RhYmxlLmR0LW1lbWJlcnMnKS5EYXRhVGFibGUoIHtcblx0XHRcdFx0ICAgICAgICBcImRhdGFcIjogZGF0YSxcblx0XHRcdFx0ICAgICAgICBcImNvbHVtbnNcIjogW1xuXHRcdFx0XHQgICAgICAgICAgICB7XG5cdFx0XHRcdCAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjUlXCIsXG5cdFx0XHRcdCAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdkZXRhaWxzLWNvbnRyb2wgdGV4dC1jZW50ZXInLFxuXHRcdFx0XHQgICAgICAgICAgICAgICAgXCJvcmRlcmFibGVcIjogICAgICBmYWxzZSxcblx0XHRcdFx0ICAgICAgICAgICAgICAgIFwiZGF0YVwiOiAgICAgICAgICAgbnVsbCxcblx0XHRcdFx0ICAgICAgICAgICAgICAgIFwiZGVmYXVsdENvbnRlbnRcIjogJzxpIGNsYXNzPVwiZmEgZmEtYXJyb3ctcmlnaHRcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiPjwvaT4nXG5cdFx0XHRcdCAgICAgICAgICAgIH0sXG5cdFx0XHRcdCAgICAgICAgICAgIHsgXG5cdFx0XHRcdCAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjMwJVwiLFxuXHRcdFx0XHQgICAgICAgICAgICBcdGRhdGE6IFwiY2xpZW50XCIsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0ICAgICAgICAgICAgICAgIHJldHVybiAnPGEgaHJlZj1cIi92aXNhL2NsaWVudC8nICsgZGF0YS5pZCArICdcIiB0YXJnZXQ9XCJfYmxhbmtcIj4nICsgZGF0YS5mdWxsX25hbWUgKyAnPC9hPidcblx0XHRcdFx0XHQgICAgICAgICAgICB9XG5cdFx0XHRcdFx0ICAgICAgICB9LFxuXHRcdFx0XHRcdCAgICAgICAge1xuXHRcdFx0XHQgICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQgICAgICAgIFx0ZGF0YTogXCJjbGllbnRcIiwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQgICAgICAgIFx0XHRyZXR1cm4gZGF0YS5pZFxuXHRcdFx0XHRcdCAgICAgICAgXHR9XG5cdFx0XHRcdFx0ICAgICAgICB9LFxuXHRcdFx0XHRcdCAgICAgICAge1xuXHRcdFx0XHQgICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHQgICAgICAgIFx0XHRyZXR1cm4gc2VydmljZUNvc3RbbWV0YS5yb3ddXG5cdFx0XHRcdFx0ICAgICAgICBcdH1cblx0XHRcdFx0XHQgICAgICAgIH0sXG5cdFx0XHRcdFx0ICAgICAgICB7IFxuXHRcdFx0XHRcdCAgICAgICAgXHRkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdykge1xuXHRcdFx0XHRcdCAgICAgICAgICAgICAgICBpZihkYXRhLmxlYWRlciA9PSAxKSB7XG5cdFx0XHRcdFx0ICAgICAgICAgICAgICAgIFx0cmV0dXJuICc8c3Bhbj48aSBjbGFzcz1cImZhIGZhLXN0YXJcIj48L2k+PGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPiBHcm91cCBMZWFkZXI8L3NwYW4+J1xuXHRcdFx0XHRcdCAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0ICAgICAgICAgICAgICAgIGVsc2UgaWYoZGF0YS5sZWFkZXIgPT0gMikge1xuXHRcdFx0XHRcdCAgICAgICAgICAgXHRcdFx0cmV0dXJuICc8c3Bhbj48YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJ2aWNlLWxlYWRlci1hY3Rpb25cIiBkYXRhLWlkPVwiJytkYXRhLmNsaWVudC5pZCsnXCI+PGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPiBWaWNlIExlYWRlcjwvYT48L3NwYW4+J1xuXHRcdFx0XHRcdCAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0ICAgICAgICAgICAgICAgICBlbHNlIHtcblx0XHRcdFx0XHQgICAgICAgICAgICAgICAgXHRyZXR1cm4gJzxzcGFuPjxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cIm1ha2UtbGVhZGVyLWFjdGlvblwiIGRhdGEtaWQ9XCInK2RhdGEuY2xpZW50LmlkKydcIj48aSBjbGFzcz1cImZhIGZhLXN0YXItb1wiPjwvaT4gTWFrZSBWaWNlLS0tLUxlYWRlcjwvYT48L3NwYW4+J1xuXHRcdFx0XHRcdCAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0ICAgICAgICAgICAgfSwgY2xhc3NOYW1lOiAndGV4dC1jZW50ZXInXG5cdFx0XHRcdFx0ICAgICAgICB9LFxuXHRcdFx0XHRcdCAgICAgICAgeyBcblx0XHRcdFx0XHQgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQgICAgICAgIFx0XHRyZXR1cm4gJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cInRyYW5zZmVyLWFjdGlvblwiIGRhdGEtdG9nZ2xlPVwibW9kYWxcIiBkYXRhLXRhcmdldD1cIiN0cmFuc2Zlci1tb2RhbFwiIGRhdGEtaWQ9XCInK2RhdGEuY2xpZW50LmlkKydcIj5UcmFuc2ZlcjwvYT4nXG5cdFx0XHRcdFx0ICAgICAgICAgICAgfSwgY2xhc3NOYW1lOiAndGV4dC1jZW50ZXInXG5cdFx0XHRcdFx0ICAgICAgICB9LFxuXHRcdFx0XHRcdCAgICAgICAgeyBcblx0XHRcdFx0XHQgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQgICAgICAgIFx0XHRyZXR1cm4gJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cImRlbGV0ZS1hY3Rpb25cIiBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgZGF0YS10YXJnZXQ9XCIjZGVsZXRlLW1lbWJlci1tb2RhbFwiIGRhdGEtaWQ9XCInK2RhdGEuY2xpZW50LmlkKydcIj5EZWxldGU8L2E+J1xuXHRcdFx0XHRcdCAgICAgICAgICAgIH0sIGNsYXNzTmFtZTogJ3RleHQtY2VudGVyJ1xuXHRcdFx0XHRcdCAgICAgICAgfVxuXHRcdFx0XHQgICAgICAgIF1cblx0XHRcdFx0ICAgIH0pO1xuXHRcdCAgICAgICAgLy99XG5cblx0XHQgICAvLyAgICAgIGlmKHRoaXMuc2VscGVybWlzc2lvbnMubWFrZUxlYWRlciA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMudHJhbnNmZXIgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmRlbGV0ZSA9PSAxKXtcblx0XHRcdFx0XHQvLyB0YWJsZSA9ICQoJ3RhYmxlLmR0LW1lbWJlcnMnKS5EYXRhVGFibGUoIHtcblx0XHRcdFx0IC8vICAgICAgICBcImRhdGFcIjogZGF0YSxcblx0XHRcdFx0IC8vICAgICAgICBcImNvbHVtbnNcIjogW1xuXHRcdFx0XHQgLy8gICAgICAgICAgICB7XG5cdFx0XHRcdCAvLyAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjUlXCIsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdkZXRhaWxzLWNvbnRyb2wgdGV4dC1jZW50ZXInLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICAgICAgXCJvcmRlcmFibGVcIjogICAgICBmYWxzZSxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwiZGF0YVwiOiAgICAgICAgICAgbnVsbCxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwiZGVmYXVsdENvbnRlbnRcIjogJzxpIGNsYXNzPVwiZmEgZmEtYXJyb3ctcmlnaHRcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiPjwvaT4nXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIH0sXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIHsgXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjMwJVwiLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdGRhdGE6IFwiY2xpZW50XCIsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIHJldHVybiAnPGEgaHJlZj1cIi92aXNhL2NsaWVudC8nICsgZGF0YS5pZCArICdcIiB0YXJnZXQ9XCJfYmxhbmtcIj4nICsgZGF0YS5mdWxsX25hbWUgKyAnPC9hPidcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogXCJjbGllbnRcIiwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gZGF0YS5pZFxuXHRcdFx0XHRcdC8vICAgICAgICAgXHR9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gc2VydmljZUNvc3RbbWV0YS5yb3ddXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdH1cblx0XHRcdFx0XHQvLyAgICAgICAgIH0sXG5cdFx0XHRcdFx0Ly8gICAgICAgICB7IFxuXHRcdFx0XHRcdC8vICAgICAgICAgXHRkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdykge1xuXHRcdFx0XHRcdC8vICAgICAgICAgXHRcdHJldHVybiAnPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwidHJhbnNmZXItYWN0aW9uXCIgZGF0YS10b2dnbGU9XCJtb2RhbFwiIGRhdGEtdGFyZ2V0PVwiI3RyYW5zZmVyLW1vZGFsXCIgZGF0YS1pZD1cIicrZGF0YS5jbGllbnQuaWQrJ1wiPlRyYW5zZmVyPC9hPidcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICB9LCBjbGFzc05hbWU6ICd0ZXh0LWNlbnRlcidcblx0XHRcdFx0XHQvLyAgICAgICAgIH0sXG5cdFx0XHRcdFx0Ly8gICAgICAgICB7IFxuXHRcdFx0XHRcdC8vICAgICAgICAgXHRkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdykge1xuXHRcdFx0XHRcdC8vICAgICAgICAgXHRcdHJldHVybiAnPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwiZGVsZXRlLWFjdGlvblwiIGRhdGEtdG9nZ2xlPVwibW9kYWxcIiBkYXRhLXRhcmdldD1cIiNkZWxldGUtbWVtYmVyLW1vZGFsXCIgZGF0YS1pZD1cIicrZGF0YS5jbGllbnQuaWQrJ1wiPkRlbGV0ZTwvYT4nXG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgfSwgY2xhc3NOYW1lOiAndGV4dC1jZW50ZXInXG5cdFx0XHRcdFx0Ly8gICAgICAgICB9XG5cdFx0XHRcdCAvLyAgICAgICAgXVxuXHRcdFx0XHQgLy8gICAgfSk7XG5cdFx0ICAgLy8gICAgICB9XG5cblx0XHQgICAvLyAgICAgIGlmKHRoaXMuc2VscGVybWlzc2lvbnMubWFrZUxlYWRlciA9PSAxICYmIHRoaXMuc2VscGVybWlzc2lvbnMudHJhbnNmZXIgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmRlbGV0ZSA9PSAxKXtcblx0XHRcdFx0XHQvLyB0YWJsZSA9ICQoJ3RhYmxlLmR0LW1lbWJlcnMnKS5EYXRhVGFibGUoIHtcblx0XHRcdFx0IC8vICAgICAgICBcImRhdGFcIjogZGF0YSxcblx0XHRcdFx0IC8vICAgICAgICBcImNvbHVtbnNcIjogW1xuXHRcdFx0XHQgLy8gICAgICAgICAgICB7XG5cdFx0XHRcdCAvLyAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjUlXCIsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdkZXRhaWxzLWNvbnRyb2wgdGV4dC1jZW50ZXInLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICAgICAgXCJvcmRlcmFibGVcIjogICAgICBmYWxzZSxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwiZGF0YVwiOiAgICAgICAgICAgbnVsbCxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwiZGVmYXVsdENvbnRlbnRcIjogJzxpIGNsYXNzPVwiZmEgZmEtYXJyb3ctcmlnaHRcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiPjwvaT4nXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIH0sXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIHsgXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjMwJVwiLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdGRhdGE6IFwiY2xpZW50XCIsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIHJldHVybiAnPGEgaHJlZj1cIi92aXNhL2NsaWVudC8nICsgZGF0YS5pZCArICdcIiB0YXJnZXQ9XCJfYmxhbmtcIj4nICsgZGF0YS5mdWxsX25hbWUgKyAnPC9hPidcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogXCJjbGllbnRcIiwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gZGF0YS5pZFxuXHRcdFx0XHRcdC8vICAgICAgICAgXHR9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gc2VydmljZUNvc3RbbWV0YS5yb3ddXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdH1cblx0XHRcdFx0XHQvLyAgICAgICAgIH0sXG5cdFx0XHRcdFx0Ly8gICAgICAgICB7IFxuXHRcdFx0XHRcdC8vICAgICAgICAgXHRkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdykge1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICBpZihkYXRhLmxlYWRlciA9PSAxKSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIFx0cmV0dXJuICc8c3Bhbj48aSBjbGFzcz1cImZhIGZhLXN0YXJcIj48L2k+PGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPiBHcm91cCBMZWFkZXI8L3NwYW4+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIGVsc2UgaWYoZGF0YS5sZWFkZXIgPT0gMikge1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgXHRcdFx0cmV0dXJuICc8c3Bhbj48YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJ2aWNlLWxlYWRlci1hY3Rpb25cIiBkYXRhLWlkPVwiJytkYXRhLmNsaWVudC5pZCsnXCI+PGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPiBWaWNlIExlYWRlcjwvYT48L3NwYW4+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgICBlbHNlIHtcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICAgICAgXHRyZXR1cm4gJzxzcGFuPjxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cIm1ha2UtbGVhZGVyLWFjdGlvblwiIGRhdGEtaWQ9XCInK2RhdGEuY2xpZW50LmlkKydcIj48aSBjbGFzcz1cImZhIGZhLXN0YXItb1wiPjwvaT4gTWFrZSBWaWNlLS0tLUxlYWRlcjwvYT48L3NwYW4+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgfSwgY2xhc3NOYW1lOiAndGV4dC1jZW50ZXInXG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAgeyBcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cImRlbGV0ZS1hY3Rpb25cIiBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgZGF0YS10YXJnZXQ9XCIjZGVsZXRlLW1lbWJlci1tb2RhbFwiIGRhdGEtaWQ9XCInK2RhdGEuY2xpZW50LmlkKydcIj5EZWxldGU8L2E+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgIH0sIGNsYXNzTmFtZTogJ3RleHQtY2VudGVyJ1xuXHRcdFx0XHRcdC8vICAgICAgICAgfVxuXHRcdFx0XHQgLy8gICAgICAgIF1cblx0XHRcdFx0IC8vICAgIH0pO1xuXHRcdCAgIC8vICAgICAgfVxuXG5cdFx0ICAgLy8gICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLm1ha2VMZWFkZXIgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLnRyYW5zZmVyID09IDEgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5kZWxldGUgPT0gMCl7XG5cdFx0XHRcdFx0Ly8gdGFibGUgPSAkKCd0YWJsZS5kdC1tZW1iZXJzJykuRGF0YVRhYmxlKCB7XG5cdFx0XHRcdCAvLyAgICAgICAgXCJkYXRhXCI6IGRhdGEsXG5cdFx0XHRcdCAvLyAgICAgICAgXCJjb2x1bW5zXCI6IFtcblx0XHRcdFx0IC8vICAgICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCI1JVwiLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICAgICAgXCJjbGFzc05hbWVcIjogICAgICAnZGV0YWlscy1jb250cm9sIHRleHQtY2VudGVyJyxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwib3JkZXJhYmxlXCI6ICAgICAgZmFsc2UsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImRhdGFcIjogICAgICAgICAgIG51bGwsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImRlZmF1bHRDb250ZW50XCI6ICc8aSBjbGFzcz1cImZhIGZhLWFycm93LXJpZ2h0XCIgc3R5bGU9XCJjdXJzb3I6cG9pbnRlcjtcIj48L2k+J1xuXHRcdFx0XHQgLy8gICAgICAgICAgICB9LFxuXHRcdFx0XHQgLy8gICAgICAgICAgICB7IFxuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIzMCVcIixcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRkYXRhOiBcImNsaWVudFwiLCByZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdykge1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICByZXR1cm4gJzxhIGhyZWY9XCIvdmlzYS9jbGllbnQvJyArIGRhdGEuaWQgKyAnXCIgdGFyZ2V0PVwiX2JsYW5rXCI+JyArIGRhdGEuZnVsbF9uYW1lICsgJzwvYT4nXG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgfVxuXHRcdFx0XHRcdC8vICAgICAgICAgfSxcblx0XHRcdFx0XHQvLyAgICAgICAgIHtcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRcIndpZHRoXCI6IFwiMTUlXCIsXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdGRhdGE6IFwiY2xpZW50XCIsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdFx0cmV0dXJuIGRhdGEuaWRcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0fVxuXHRcdFx0XHRcdC8vICAgICAgICAgfSxcblx0XHRcdFx0XHQvLyAgICAgICAgIHtcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRcIndpZHRoXCI6IFwiMTUlXCIsXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93LCBtZXRhKSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdFx0cmV0dXJuIHNlcnZpY2VDb3N0W21ldGEucm93XVxuXHRcdFx0XHRcdC8vICAgICAgICAgXHR9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAgeyBcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICAgICAgaWYoZGF0YS5sZWFkZXIgPT0gMSkge1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICBcdHJldHVybiAnPHNwYW4+PGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPjxpIGNsYXNzPVwiZmEgZmEtc3RhclwiPjwvaT4gR3JvdXAgTGVhZGVyPC9zcGFuPidcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICAgICAgfVxuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICBlbHNlIGlmKGRhdGEubGVhZGVyID09IDIpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgICAgIFx0XHRcdHJldHVybiAnPHNwYW4+PGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwidmljZS1sZWFkZXItYWN0aW9uXCIgZGF0YS1pZD1cIicrZGF0YS5jbGllbnQuaWQrJ1wiPjxpIGNsYXNzPVwiZmEgZmEtc3RhclwiPjwvaT4gVmljZSBMZWFkZXI8L2E+PC9zcGFuPidcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICAgICAgfVxuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICAgZWxzZSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIFx0cmV0dXJuICc8c3Bhbj48YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJtYWtlLWxlYWRlci1hY3Rpb25cIiBkYXRhLWlkPVwiJytkYXRhLmNsaWVudC5pZCsnXCI+PGkgY2xhc3M9XCJmYSBmYS1zdGFyLW9cIj48L2k+IE1ha2UgVmljZS0tLS1MZWFkZXI8L2E+PC9zcGFuPidcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICAgICAgfVxuXHRcdFx0XHRcdC8vICAgICAgICAgICAgIH0sIGNsYXNzTmFtZTogJ3RleHQtY2VudGVyJ1xuXHRcdFx0XHRcdC8vICAgICAgICAgfSxcblx0XHRcdFx0XHQvLyAgICAgICAgIHsgXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdFx0cmV0dXJuICc8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJ0cmFuc2Zlci1hY3Rpb25cIiBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgZGF0YS10YXJnZXQ9XCIjdHJhbnNmZXItbW9kYWxcIiBkYXRhLWlkPVwiJytkYXRhLmNsaWVudC5pZCsnXCI+VHJhbnNmZXI8L2E+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgIH0sIGNsYXNzTmFtZTogJ3RleHQtY2VudGVyJ1xuXHRcdFx0XHRcdC8vICAgICAgICAgfVxuXHRcdFx0XHQgLy8gICAgICAgIF1cblx0XHRcdFx0IC8vICAgIH0pO1xuXHRcdCAgIC8vICAgICAgfVxuXG5cdFx0ICAgLy8gICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLm1ha2VMZWFkZXIgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLnRyYW5zZmVyID09IDAgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5kZWxldGUgPT0gMSl7XG5cdFx0XHRcdFx0Ly8gdGFibGUgPSAkKCd0YWJsZS5kdC1tZW1iZXJzJykuRGF0YVRhYmxlKCB7XG5cdFx0XHRcdCAvLyAgICAgICAgXCJkYXRhXCI6IGRhdGEsXG5cdFx0XHRcdCAvLyAgICAgICAgXCJjb2x1bW5zXCI6IFtcblx0XHRcdFx0IC8vICAgICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCI1JVwiLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICAgICAgXCJjbGFzc05hbWVcIjogICAgICAnZGV0YWlscy1jb250cm9sIHRleHQtY2VudGVyJyxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwib3JkZXJhYmxlXCI6ICAgICAgZmFsc2UsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImRhdGFcIjogICAgICAgICAgIG51bGwsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImRlZmF1bHRDb250ZW50XCI6ICc8aSBjbGFzcz1cImZhIGZhLWFycm93LXJpZ2h0XCIgc3R5bGU9XCJjdXJzb3I6cG9pbnRlcjtcIj48L2k+J1xuXHRcdFx0XHQgLy8gICAgICAgICAgICB9LFxuXHRcdFx0XHQgLy8gICAgICAgICAgICB7IFxuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIzMCVcIixcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRkYXRhOiBcImNsaWVudFwiLCByZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdykge1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICByZXR1cm4gJzxhIGhyZWY9XCIvdmlzYS9jbGllbnQvJyArIGRhdGEuaWQgKyAnXCIgdGFyZ2V0PVwiX2JsYW5rXCI+JyArIGRhdGEuZnVsbF9uYW1lICsgJzwvYT4nXG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgfVxuXHRcdFx0XHRcdC8vICAgICAgICAgfSxcblx0XHRcdFx0XHQvLyAgICAgICAgIHtcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRcIndpZHRoXCI6IFwiMTUlXCIsXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdGRhdGE6IFwiY2xpZW50XCIsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdFx0cmV0dXJuIGRhdGEuaWRcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0fVxuXHRcdFx0XHRcdC8vICAgICAgICAgfSxcblx0XHRcdFx0XHQvLyAgICAgICAgIHtcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRcIndpZHRoXCI6IFwiMTUlXCIsXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93LCBtZXRhKSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdFx0cmV0dXJuIHNlcnZpY2VDb3N0W21ldGEucm93XVxuXHRcdFx0XHRcdC8vICAgICAgICAgXHR9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAgeyBcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cImRlbGV0ZS1hY3Rpb25cIiBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgZGF0YS10YXJnZXQ9XCIjZGVsZXRlLW1lbWJlci1tb2RhbFwiIGRhdGEtaWQ9XCInK2RhdGEuY2xpZW50LmlkKydcIj5EZWxldGU8L2E+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgIH0sIGNsYXNzTmFtZTogJ3RleHQtY2VudGVyJ1xuXHRcdFx0XHRcdC8vICAgICAgICAgfVxuXHRcdFx0XHQgLy8gICAgICAgIF1cblx0XHRcdFx0IC8vICAgIH0pO1xuXHRcdCAgIC8vICAgICAgfVxuXG5cdFx0ICAgLy8gICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLm1ha2VMZWFkZXIgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLnRyYW5zZmVyID09IDEgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5kZWxldGUgPT0gMCl7XG5cdFx0XHRcdFx0Ly8gdGFibGUgPSAkKCd0YWJsZS5kdC1tZW1iZXJzJykuRGF0YVRhYmxlKCB7XG5cdFx0XHRcdCAvLyAgICAgICAgXCJkYXRhXCI6IGRhdGEsXG5cdFx0XHRcdCAvLyAgICAgICAgXCJjb2x1bW5zXCI6IFtcblx0XHRcdFx0IC8vICAgICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCI1JVwiLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICAgICAgXCJjbGFzc05hbWVcIjogICAgICAnZGV0YWlscy1jb250cm9sIHRleHQtY2VudGVyJyxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwib3JkZXJhYmxlXCI6ICAgICAgZmFsc2UsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImRhdGFcIjogICAgICAgICAgIG51bGwsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImRlZmF1bHRDb250ZW50XCI6ICc8aSBjbGFzcz1cImZhIGZhLWFycm93LXJpZ2h0XCIgc3R5bGU9XCJjdXJzb3I6cG9pbnRlcjtcIj48L2k+J1xuXHRcdFx0XHQgLy8gICAgICAgICAgICB9LFxuXHRcdFx0XHQgLy8gICAgICAgICAgICB7IFxuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIzMCVcIixcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRkYXRhOiBcImNsaWVudFwiLCByZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdykge1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICByZXR1cm4gJzxhIGhyZWY9XCIvdmlzYS9jbGllbnQvJyArIGRhdGEuaWQgKyAnXCIgdGFyZ2V0PVwiX2JsYW5rXCI+JyArIGRhdGEuZnVsbF9uYW1lICsgJzwvYT4nXG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgfVxuXHRcdFx0XHRcdC8vICAgICAgICAgfSxcblx0XHRcdFx0XHQvLyAgICAgICAgIHtcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRcIndpZHRoXCI6IFwiMTUlXCIsXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdGRhdGE6IFwiY2xpZW50XCIsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdFx0cmV0dXJuIGRhdGEuaWRcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0fVxuXHRcdFx0XHRcdC8vICAgICAgICAgfSxcblx0XHRcdFx0XHQvLyAgICAgICAgIHtcblx0XHRcdFx0IC8vICAgICAgICAgICAgXHRcIndpZHRoXCI6IFwiMTUlXCIsXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93LCBtZXRhKSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdFx0cmV0dXJuIHNlcnZpY2VDb3N0W21ldGEucm93XVxuXHRcdFx0XHRcdC8vICAgICAgICAgXHR9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAgeyBcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cInRyYW5zZmVyLWFjdGlvblwiIGRhdGEtdG9nZ2xlPVwibW9kYWxcIiBkYXRhLXRhcmdldD1cIiN0cmFuc2Zlci1tb2RhbFwiIGRhdGEtaWQ9XCInK2RhdGEuY2xpZW50LmlkKydcIj5UcmFuc2ZlcjwvYT4nXG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgfSwgY2xhc3NOYW1lOiAndGV4dC1jZW50ZXInXG5cdFx0XHRcdFx0Ly8gICAgICAgICB9XG5cdFx0XHRcdCAvLyAgICAgICAgXVxuXHRcdFx0XHQgLy8gICAgfSk7XG5cdFx0ICAgLy8gICAgICB9XG5cblx0XHQgICAvLyAgICAgIGlmKHRoaXMuc2VscGVybWlzc2lvbnMubWFrZUxlYWRlciA9PSAxICYmIHRoaXMuc2VscGVybWlzc2lvbnMudHJhbnNmZXIgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmRlbGV0ZSA9PSAwKXtcblx0XHRcdFx0XHQvLyB0YWJsZSA9ICQoJ3RhYmxlLmR0LW1lbWJlcnMnKS5EYXRhVGFibGUoIHtcblx0XHRcdFx0IC8vICAgICAgICBcImRhdGFcIjogZGF0YSxcblx0XHRcdFx0IC8vICAgICAgICBcImNvbHVtbnNcIjogW1xuXHRcdFx0XHQgLy8gICAgICAgICAgICB7XG5cdFx0XHRcdCAvLyAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjUlXCIsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdkZXRhaWxzLWNvbnRyb2wgdGV4dC1jZW50ZXInLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICAgICAgXCJvcmRlcmFibGVcIjogICAgICBmYWxzZSxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwiZGF0YVwiOiAgICAgICAgICAgbnVsbCxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwiZGVmYXVsdENvbnRlbnRcIjogJzxpIGNsYXNzPVwiZmEgZmEtYXJyb3ctcmlnaHRcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiPjwvaT4nXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIH0sXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIHsgXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjMwJVwiLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdGRhdGE6IFwiY2xpZW50XCIsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIHJldHVybiAnPGEgaHJlZj1cIi92aXNhL2NsaWVudC8nICsgZGF0YS5pZCArICdcIiB0YXJnZXQ9XCJfYmxhbmtcIj4nICsgZGF0YS5mdWxsX25hbWUgKyAnPC9hPidcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogXCJjbGllbnRcIiwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gZGF0YS5pZFxuXHRcdFx0XHRcdC8vICAgICAgICAgXHR9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gc2VydmljZUNvc3RbbWV0YS5yb3ddXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdH1cblx0XHRcdFx0XHQvLyAgICAgICAgIH0sXG5cdFx0XHRcdFx0Ly8gICAgICAgICB7IFxuXHRcdFx0XHRcdC8vICAgICAgICAgXHRkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKGRhdGEsIHR5cGUsIHJvdykge1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICBpZihkYXRhLmxlYWRlciA9PSAxKSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIFx0cmV0dXJuICc8c3Bhbj48aSBjbGFzcz1cImZhIGZhLXN0YXJcIj48L2k+PGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPiBHcm91cCBMZWFkZXI8L3NwYW4+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIGVsc2UgaWYoZGF0YS5sZWFkZXIgPT0gMikge1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgXHRcdFx0cmV0dXJuICc8c3Bhbj48YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJ2aWNlLWxlYWRlci1hY3Rpb25cIiBkYXRhLWlkPVwiJytkYXRhLmNsaWVudC5pZCsnXCI+PGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPiBWaWNlIExlYWRlcjwvYT48L3NwYW4+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgICBlbHNlIHtcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICAgICAgXHRyZXR1cm4gJzxzcGFuPjxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cIm1ha2UtbGVhZGVyLWFjdGlvblwiIGRhdGEtaWQ9XCInK2RhdGEuY2xpZW50LmlkKydcIj48aSBjbGFzcz1cImZhIGZhLXN0YXItb1wiPjwvaT4gTWFrZSBWaWNlLS0tLUxlYWRlcjwvYT48L3NwYW4+J1xuXHRcdFx0XHRcdC8vICAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgfSwgY2xhc3NOYW1lOiAndGV4dC1jZW50ZXInXG5cdFx0XHRcdFx0Ly8gICAgICAgICB9XG5cdFx0XHRcdCAvLyAgICAgICAgXVxuXHRcdFx0XHQgLy8gICAgfSk7XG5cdFx0ICAgLy8gICAgICB9XG5cblx0XHQgICAvLyAgICAgIGlmKHRoaXMuc2VscGVybWlzc2lvbnMubWFrZUxlYWRlciA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMudHJhbnNmZXIgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmRlbGV0ZSA9PSAwKXtcblx0XHRcdFx0XHQvLyB0YWJsZSA9ICQoJ3RhYmxlLmR0LW1lbWJlcnMnKS5EYXRhVGFibGUoIHtcblx0XHRcdFx0IC8vICAgICAgICBcImRhdGFcIjogZGF0YSxcblx0XHRcdFx0IC8vICAgICAgICBcImNvbHVtbnNcIjogW1xuXHRcdFx0XHQgLy8gICAgICAgICAgICB7XG5cdFx0XHRcdCAvLyAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjUlXCIsXG5cdFx0XHRcdCAvLyAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdkZXRhaWxzLWNvbnRyb2wgdGV4dC1jZW50ZXInLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICAgICAgXCJvcmRlcmFibGVcIjogICAgICBmYWxzZSxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwiZGF0YVwiOiAgICAgICAgICAgbnVsbCxcblx0XHRcdFx0IC8vICAgICAgICAgICAgICAgIFwiZGVmYXVsdENvbnRlbnRcIjogJzxpIGNsYXNzPVwiZmEgZmEtYXJyb3ctcmlnaHRcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiPjwvaT4nXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIH0sXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIHsgXG5cdFx0XHRcdCAvLyAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjMwJVwiLFxuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdGRhdGE6IFwiY2xpZW50XCIsIHJlbmRlcjogZnVuY3Rpb24oZGF0YSwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdFx0Ly8gICAgICAgICAgICAgICAgIHJldHVybiAnPGEgaHJlZj1cIi92aXNhL2NsaWVudC8nICsgZGF0YS5pZCArICdcIiB0YXJnZXQ9XCJfYmxhbmtcIj4nICsgZGF0YS5mdWxsX25hbWUgKyAnPC9hPidcblx0XHRcdFx0XHQvLyAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogXCJjbGllbnRcIiwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3cpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gZGF0YS5pZFxuXHRcdFx0XHRcdC8vICAgICAgICAgXHR9XG5cdFx0XHRcdFx0Ly8gICAgICAgICB9LFxuXHRcdFx0XHRcdC8vICAgICAgICAge1xuXHRcdFx0XHQgLy8gICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihkYXRhLCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0XHQvLyAgICAgICAgIFx0XHRyZXR1cm4gc2VydmljZUNvc3RbbWV0YS5yb3ddXG5cdFx0XHRcdFx0Ly8gICAgICAgICBcdH1cblx0XHRcdFx0XHQvLyAgICAgICAgIH1cblx0XHRcdFx0IC8vICAgICAgICBdXG5cdFx0XHRcdCAvLyAgICB9KTtcblx0XHQgICAvLyAgICAgIH1cdFx0ICAgICAgICBcblx0XHRcdFx0XG5cblxuXHRcdFx0XHQkKCdib2R5Jykub2ZmKCdjbGljaycsICd0YWJsZS5kdC1tZW1iZXJzIHRib2R5IHRkLmRldGFpbHMtY29udHJvbCcpO1xuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQoJ2JvZHknKS5vbignY2xpY2snLCAndGFibGUuZHQtbWVtYmVycyB0Ym9keSB0ZC5kZXRhaWxzLWNvbnRyb2wnLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRcdFx0dmFyIHRyID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpO1xuXHRcdFx0XHRcdFx0dmFyIHJvdyA9IHRhYmxlLnJvdyggdHIgKTtcblx0XHRcdFx0XHRcdFx0IFxuXHRcdFx0XHRcdFx0aWYocm93LmNoaWxkLmlzU2hvd24oKSkge1xuXHRcdFx0XHRcdFx0XHRyb3cuY2hpbGQuaGlkZSgpO1xuXHRcdFx0XHRcdFx0XHR0ci5yZW1vdmVDbGFzcygnc2hvd24nKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdHJvdy5jaGlsZCggZm9ybWF0KHJvdy5kYXRhKCkpICkuc2hvdygpO1xuXHRcdFx0XHRcdFx0XHR0ci5hZGRDbGFzcygnc2hvd24nKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fS5iaW5kKHRoaXMpLCA1MDApO1xuXG5cdFx0XHRcdC8vbWFrZSB2aWNlIGxlYWRlclxuXHRcdFx0XHQkKCdib2R5Jykub24oJ2NsaWNrJywgJ3RhYmxlLmR0LW1lbWJlcnMgdGJvZHkgYS5tYWtlLWxlYWRlci1hY3Rpb24nLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRcdGxldCBjbGllbnRJZCA9IGUudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1pZCcpO1xuXG5cdFx0XHRcdFx0dGhpcy5hc3NpZ25MZWFkZXIoY2xpZW50SWQsICd2aWNlJyk7XG5cdFx0XHRcdH0uYmluZCh0aGlzKSk7XG5cblx0XHRcdFx0Ly92aWNlIGxlYWRlciBtb2RhbFxuXHRcdFx0XHQkKCdib2R5Jykub24oJ2NsaWNrJywgJ3RhYmxlLmR0LW1lbWJlcnMgdGJvZHkgYS52aWNlLWxlYWRlci1hY3Rpb24nLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRcdCQoJyN2aWNlLWxlYWRlci1tb2RhbCcpLm1vZGFsKCdzaG93Jyk7XG5cdFx0XHRcdFx0bGV0IGNsaWVudElkID0gZS50YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLWlkJyk7XG5cdFx0XHRcdFx0dGhpcy5zZUxjbGllbnRJZCA9IGNsaWVudElkO1xuXHRcdFx0XHR9LmJpbmQodGhpcykpO1xuXG5cdFx0XHRcdCQoJ2JvZHknKS5vbignY2xpY2snLCAndGFibGUuZHQtbWVtYmVycyB0Ym9keSBhLnRyYW5zZmVyLWFjdGlvbicsIGZ1bmN0aW9uIChlKSB7XG5cdFx0XHRcdFx0bGV0IGNsaWVudElkID0gZS50YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLWlkJyk7XG5cblx0XHRcdFx0XHR0aGlzLiRyZWZzLnRyYW5zZmVycmVmLnNldE1lbWJlcklkKGNsaWVudElkKTtcblx0XHRcdFx0fS5iaW5kKHRoaXMpKTtcblxuXHRcdFx0XHQkKCdib2R5Jykub24oJ2NsaWNrJywgJ3RhYmxlLmR0LW1lbWJlcnMgdGJvZHkgYS5kZWxldGUtYWN0aW9uJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0XHRsZXQgY2xpZW50SWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKTtcblxuXHRcdFx0XHRcdHRoaXMuJHJlZnMuZGVsZXRlbWVtYmVycmVmLnNldE1lbWJlcklkKGNsaWVudElkKTtcblx0XHRcdFx0fS5iaW5kKHRoaXMpKTtcblxuXHRcdFx0XHQkKCdib2R5Jykub24oJ2NsaWNrJywgJ3RhYmxlLmR0LW1lbWJlcnMgdGJvZHkgLmVkaXQtc2VydmljZS1hY3Rpb24nLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRcdGxldCBzZXJ2aWNlQ2xpZW50SWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2VydmljZWNsaWVudGlkJyk7XG5cdFx0XHRcdFx0bGV0IHNlcnZpY2VTZXJ2aWNlaWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2VydmljZXNlcnZpY2VpZCcpO1xuXHRcdFx0XHRcdGxldCBzZXJ2aWNlSWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2VydmljZWlkJyk7XG5cblx0XHRcdFx0XHR0aGlzLiRyZWZzLmVkaXRzZXJ2aWNlcmVmLmluaXQoc2VydmljZUNsaWVudElkLCBzZXJ2aWNlU2VydmljZWlkLCBzZXJ2aWNlSWQpXG5cdFx0XHRcdH0uYmluZCh0aGlzKSk7XG5cblx0XHRcdFx0JCgnYm9keScpLm9uKCdjbGljaycsICd0YWJsZS5kdC1tZW1iZXJzIHRib2R5IC53cml0ZS1yZXBvcnQtYWN0aW9uJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0XHRsZXQgY2xpZW50SWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY2xpZW50aWQnKTtcblx0XHRcdFx0XHRsZXQgY2xpZW50U2VydmljZUlkID0gZS50YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLWNsaWVudHNlcnZpY2VpZCcpO1xuXHRcdFx0XHRcdGxldCB0cmFja2luZyA9IGUudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS10cmFja2luZycpO1xuXG5cdFx0XHRcdFx0dGhpcy5jcmVhdGVRdWlja1JlcG9ydChjbGllbnRJZCwgY2xpZW50U2VydmljZUlkLCB0cmFja2luZyk7XG5cdFx0XHRcdH0uYmluZCh0aGlzKSk7XG5cblx0XHRcdH0uYmluZCh0aGlzKSwgMTAwMCk7XG5cblx0XHR9LFxuXG5cdFx0Y2hhbmdlTWVtYmVyVHlwZSh0eXBlKXtcblx0XHRcdHZhciBjbGllbnRJZCA9IHRoaXMuc2VMY2xpZW50SWQ7XG5cdFx0XHR0aGlzLmFzc2lnbkxlYWRlcihjbGllbnRJZCwgdHlwZSk7XG5cdFx0fSxcblxuXHRcdGNyZWF0ZURhdGF0YWJsZVRvZ2dsZUJ5U2VydmljZShzZXJ2KSB7XG5cdFx0XHR2YXIgZ3JvdXBJZCA9IHRoaXMuZ3JvdXBJZDtcblxuXHRcdFx0JCgndGFibGUuZHQtZ3NlcnZpY2UnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0XHRcdHZhciB2aSA9IHRoaXM7XG5cblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cblx0XHRcdFx0dmFyIGZvcm1hdEJ5U2VydmljZSA9IGZ1bmN0aW9uKGQpIHtcblx0XHRcdFx0XHR2YXIgZGl2ID0gJCgnPGRpdi8+Jylcblx0XHRcdFx0ICAgICAgICAuYWRkQ2xhc3MoICdsb2FkaW5nJyApXG5cdFx0XHRcdCAgICAgICAgLnRleHQoICdMb2FkaW5nLi4uJyApO1xuXHRcdFx0XHRheGlvc0FQSXYxLnBvc3QoJy92aXNhL2dyb3VwLycgKyBncm91cElkICsgJy9zZXJ2aWNlLWJ5LWRldGFpbCcse1xuXHRcdFx0XHRcdHNlcnZpY2VfaWQgOiBkLnNlcnZpY2VfaWRcblx0XHRcdFx0fSlcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRsZXQgdGFibGUgPSAnPHRhYmxlIGNsYXNzPVwidGFibGUgdGFibGUtYm9yZGVyZWQgdGFibGUtc3RyaXBlZCB0YWJsZS1ob3ZlclwiPicrXG5cdFx0XHRcdFx0XHRcdFx0Jzx0aGVhZD4nK1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRyPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5QYWNrYWdlPC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGg+RGF0ZSBSZWNvcmRlZDwvdGg+Jytcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRoPkRldGFpbHM8L3RoPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5TdGF0dXM8L3RoPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5Db3N0PC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGg+U2VydmljZSBGZWU8L3RoPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5BZGRpdGlvbmFsIEZlZTwvdGg+Jytcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRoPkRpc2NvdW50PC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGggY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPkFjdGlvbjwvdGg+Jytcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC90cj4nK1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgICc8L3RoZWFkPicrXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgJzx0Ym9keT4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIGZvcih2YXIgYT0wOyBhPChyZXNwb25zZS5kYXRhKS5sZW5ndGg7IGErKykge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0Ly8gdmFyIGNsYXNzTmFtZSA9IChyZXNwb25zZS5kYXRhLmFjdGl2ZSA9PSAwKSA/ICdzdHJpa2V0aHJvdWdoJyA6ICcnO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0Ly8gdmFyIHZpc2liaWxpdHkgPSAocmVzcG9uc2UuZGF0YS5ncm91cF9pZCA9PSBncm91cElkKSA/ICd0YWJsZS1yb3cnIDogJ25vbmUnO1xuXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR0YWJsZSArPSAnPHRyPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR0YWJsZSArPSAnPHRkIGNvbHNwYW49XCI5XCIgc3R5bGU9XCJiYWNrZ3JvdW5kLWNvbG9yOiAjZDJkMmQyOyB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZVwiPjxiPicrcmVzcG9uc2UuZGF0YVthXS5maXJzdF9uYW1lKycgJytyZXNwb25zZS5kYXRhW2FdLmxhc3RfbmFtZSsnPC9iPjwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdHRhYmxlICs9ICc8L3RyPic7XG5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGk9MDsgaTwocmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlcykubGVuZ3RoOyBpKyspIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdHZhciBjbGFzc05hbWUgPSAocmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS5hY3RpdmUgPT0gMCkgPyAnc3RyaWtldGhyb3VnaCcgOiAnJztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdHZhciB2aXNpYmlsaXR5ID0gKHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uZ3JvdXBfaWQgPT0gZ3JvdXBJZCkgPyAndGFibGUtcm93JyA6ICdub25lJztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdGlmKHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0ucmVtYXJrcykge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR2YXIgcmVtYXJrcyA9IHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0ucmVtYXJrcztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dmFyIGRldGFpbHMgPSAnPGEgaHJlZj1cIiNcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInJpZ2h0XCIgZGF0YS1jb250YWluZXI9XCJib2R5XCIgZGF0YS1jb250ZW50PVwiJytyZW1hcmtzKydcIiBkYXRhLXRyaWdnZXI9XCJob3ZlclwiPicrcmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS5kZXRhaWwrJzwvYT4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0fSBlbHNlIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dmFyIGRldGFpbHMgPSByZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmRldGFpbDtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdH1cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdGlmKHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uZGlzY291bnQyLmxlbmd0aCA+IDApIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0aWYocmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS5kaXNjb3VudDJbMF0ucmVhc29uKSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdFx0dmFyIHJlYXNvbiA9IHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uZGlzY291bnQyWzBdLnJlYXNvbjtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0XHR2YXIgZGlzY291bnQgPSAnPGEgaHJlZj1cIiNcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cImxlZnRcIiBkYXRhLWNvbnRhaW5lcj1cImJvZHlcIiBkYXRhLWNvbnRlbnQ9XCInK3JlYXNvbisnXCIgZGF0YS10cmlnZ2VyPVwiaG92ZXJcIj4nK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uZGlzY291bnQyWzBdLmRpc2NvdW50X2Ftb3VudCsnPC9hPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdH0gZWxzZSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdFx0dmFyIGRpc2NvdW50ID0gcmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS5kaXNjb3VudDJbMF0uZGlzY291bnRfYW1vdW50O1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR9XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR9IGVsc2Uge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR2YXIgZGlzY291bnQgPSAnJm5ic3A7Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdH1cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdHZhciBfYWN0aW9uID0gJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cImVkaXQtc2VydmljZS1hY3Rpb25cIiBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgZGF0YS10YXJnZXQ9XCIjZWRpdC1zZXJ2aWNlLW1vZGFsXCIgZGF0YS1zZXJ2aWNlY2xpZW50aWQ9XCInK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uY2xpZW50X2lkKydcIiBkYXRhLXNlcnZpY2VzZXJ2aWNlaWQ9XCInK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uc2VydmljZV9pZCsnXCIgZGF0YS1zZXJ2aWNlaWQ9XCInK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uaWQrJ1wiPjxpIGNsYXNzPVwiZmEgZmEtcGVuY2lsLXNxdWFyZS1vXCIgZGF0YS1zZXJ2aWNlY2xpZW50aWQ9XCInK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uY2xpZW50X2lkKydcIiBkYXRhLXNlcnZpY2VzZXJ2aWNlaWQ9XCInK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uc2VydmljZV9pZCsnXCIgZGF0YS1zZXJ2aWNlaWQ9XCInK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uaWQrJ1wiPjwvaT48L2E+JztcblxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0dGFibGUgKz0gJzx0ciBjbGFzcz1cIicrY2xhc3NOYW1lKydcIiBzdHlsZT1cImRpc3BsYXk6Jyt2aXNpYmlsaXR5KydcIj4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR0YWJsZSArPSAnPHRkPicrcmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS50cmFja2luZysnPC90ZD4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR0YWJsZSArPSAnPHRkPicrcmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS5zZXJ2aWNlX2RhdGUrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK2RldGFpbHMrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uc3RhdHVzKyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHRhYmxlICs9ICc8dGQ+JytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmNvc3QrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uY2hhcmdlKyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHRhYmxlICs9ICc8dGQ+JytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLnRpcCsnPC90ZD4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR0YWJsZSArPSAnPHRkPicrZGlzY291bnQrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZCBjbGFzcz1cInRleHQtY2VudGVyXCI+JytfYWN0aW9uKyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR0YWJsZSArPSAnPC90cj4nO1xuXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgfVxuXHRcdCAgICAgICAgICAgICAgICAgICAgfVxuXHRcdCAgICAgICAgICAgICAgICB0YWJsZSArPSAnPC90Ym9keT48L3RhYmxlPic7XG5cblx0XHQgICAgICAgICAgICAvL3JldHVybiB0YWJsZTtcblxuXHRcdFx0XHRcdGRpdi5odG1sKCB0YWJsZSApLnJlbW92ZUNsYXNzKCAnbG9hZGluZycgKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG5cdFx0XHRcdCBcblx0XHRcdFx0ICAgIHJldHVybiBkaXY7XG5cblx0XHRcdFx0fTtcblx0XHRcdFx0XG5cdFx0XHRcdHRhYmxlMiA9ICQoJ3RhYmxlLmR0LWdzZXJ2aWNlJykuRGF0YVRhYmxlKCB7XG5cdFx0XHQgICAgICAgIFwiZGF0YVwiOiBzZXJ2LFxuXHRcdFx0ICAgICAgICBcImNvbHVtbnNcIjogW1xuXHRcdFx0ICAgICAgICAgICAge1xuXHRcdFx0ICAgICAgICAgICAgXHRcIndpZHRoXCI6IFwiNSVcIixcblx0XHRcdCAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdzZXJ2aWNlLWNvbnRyb2wgdGV4dC1jZW50ZXInLFxuXHRcdFx0ICAgICAgICAgICAgICAgIFwib3JkZXJhYmxlXCI6ICAgICAgZmFsc2UsXG5cdFx0XHQgICAgICAgICAgICAgICAgXCJkYXRhXCI6ICAgICAgICAgICBudWxsLFxuXHRcdFx0ICAgICAgICAgICAgICAgIFwiZGVmYXVsdENvbnRlbnRcIjogJzxpIGNsYXNzPVwiZmEgZmEtYXJyb3ctcmlnaHRcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiPjwvaT4nXG5cdFx0XHQgICAgICAgICAgICB9LFxuXHRcdFx0ICAgICAgICAgICAgeyBcblx0XHRcdCAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjMwJVwiLFxuXHRcdFx0ICAgICAgICAgICAgXHRkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKHNlcnYsIHR5cGUsIHJvdykge1xuXHRcdFx0XHQgICAgICAgICAgICAgICAgcmV0dXJuICc8YiBzdHlsZT1cImNvbG9yOiAjMWFiMzk0O1wiPicgKyBzZXJ2LmRldGFpbCArICc8L2I+J1xuXHRcdFx0XHQgICAgICAgICAgICB9XG5cdFx0XHRcdCAgICAgICAgfSxcblx0XHRcdFx0ICAgICAgICB7XG5cdFx0XHQgICAgICAgICAgICBcdFwid2lkdGhcIjogXCIxNSVcIixcblx0XHRcdFx0ICAgICAgICBcdGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24oc2VydiwgdHlwZSwgcm93KSB7XG5cdFx0XHRcdCAgICAgICAgXHRcdHJldHVybiAnPGI+Jyttb21lbnQoU3RyaW5nKHNlcnYuc2VydmljZV9kYXRlKSkuZm9ybWF0KFwiTU1NTSBELFlZWVlcIikrICc8L2I+J1xuXHRcdFx0XHQgICAgICAgIFx0fVxuXHRcdFx0XHQgICAgICAgIH0sXG5cdFx0XHRcdCAgICAgICAge1xuXHRcdFx0ICAgICAgICAgICAgXHRcIndpZHRoXCI6IFwiMTUlXCIsXG5cdFx0XHRcdCAgICAgICAgXHRkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKHNlcnYsIHR5cGUsIHJvdywgbWV0YSkge1xuXHRcdFx0XHQgICAgICAgIFx0XHRyZXR1cm4gJzxiPicrc2Vydi50b3RhbGNvc3QrICc8L2I+J1xuXHRcdFx0XHQgICAgICAgIFx0fVxuXHRcdFx0XHQgICAgICAgIH0sXG5cdFx0XHQgICAgICAgIF1cblx0XHRcdCAgICB9KTtcblxuXHRcdFx0ICAgICQoJ2JvZHknKS5vbignY2xpY2snLCAndGFibGUuZHQtZ3NlcnZpY2UgdGJvZHkgdGQuc2VydmljZS1jb250cm9sJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0XHR2YXIgdHIyID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpO1xuXHRcdFx0XHRcdHZhciByb3cgPSB0YWJsZTIucm93KCB0cjIgKTtcblx0XHRcdFx0XHRcdCBcblx0XHRcdFx0XHRpZihyb3cuY2hpbGQuaXNTaG93bigpKSB7XG5cdFx0XHRcdFx0XHRyb3cuY2hpbGQuaGlkZSgpO1xuXHRcdFx0XHRcdFx0dHIyLnJlbW92ZUNsYXNzKCdzaG93bicpO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRyb3cuY2hpbGQoIGZvcm1hdEJ5U2VydmljZShyb3cuZGF0YSgpKSApLnNob3coKTtcblx0XHRcdFx0XHRcdHRyMi5hZGRDbGFzcygnc2hvd24nKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdCQoJ2JvZHknKS5vbignY2xpY2snLCAndGFibGUuZHQtZ3NlcnZpY2UgdGJvZHkgLmVkaXQtc2VydmljZS1hY3Rpb24nLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRcdGxldCBzZXJ2aWNlQ2xpZW50SWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2VydmljZWNsaWVudGlkJyk7XG5cdFx0XHRcdFx0bGV0IHNlcnZpY2VTZXJ2aWNlaWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2VydmljZXNlcnZpY2VpZCcpO1xuXHRcdFx0XHRcdGxldCBzZXJ2aWNlSWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2VydmljZWlkJyk7XG5cblx0XHRcdFx0XHR0aGlzLiRyZWZzLmVkaXRzZXJ2aWNlcmVmLmluaXQoc2VydmljZUNsaWVudElkLCBzZXJ2aWNlU2VydmljZWlkLCBzZXJ2aWNlSWQpXG5cdFx0XHRcdH0uYmluZCh0aGlzKSk7XG5cblx0XHRcdH0uYmluZCh0aGlzKSwgMTAwMCk7XG5cblx0XHR9LFxuXG5cblx0XHRjcmVhdGVEYXRhdGFibGVUb2dnbGVCeUJhdGNoKHNlcnYpIHtcblx0XHRcdHZhciBncm91cElkID0gdGhpcy5ncm91cElkO1xuXG5cdFx0XHQkKCd0YWJsZS5kdC1nYmF0Y2gnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0XHRcdHZhciB2aSA9IHRoaXM7XG5cblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cblx0XHRcdFx0dmFyIGZvcm1hdEJ5QmF0Y2ggPSBmdW5jdGlvbihkKSB7XG5cdFx0XHRcdFx0dmFyIGRpdiA9ICQoJzxkaXYvPicpXG5cdFx0XHRcdCAgICAgICAgLmFkZENsYXNzKCAnbG9hZGluZycgKVxuXHRcdFx0XHQgICAgICAgIC50ZXh0KCAnTG9hZGluZy4uLicgKTtcblx0XHRcdFx0YXhpb3NBUEl2MS5wb3N0KCcvdmlzYS9ncm91cC8nICsgZ3JvdXBJZCArICcvc2VydmljZS1ieS1kYXRlJyx7XG5cdFx0XHRcdFx0ZGF0ZSA6IGQuc2VydmljZV9kYXRlXG5cdFx0XHRcdH0pXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0bGV0IHRhYmxlID0gJzx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLWJvcmRlcmVkIHRhYmxlLXN0cmlwZWQgdGFibGUtaG92ZXJcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGhlYWQ+Jytcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0cj4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGg+UGFja2FnZTwvdGg+Jytcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRoPkRhdGUgUmVjb3JkZWQ8L3RoPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5EZXRhaWxzPC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGg+U3RhdHVzPC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGg+Q29zdDwvdGg+Jytcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRoPlNlcnZpY2UgRmVlPC90aD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dGg+QWRkaXRpb25hbCBGZWU8L3RoPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0aD5EaXNjb3VudDwvdGg+Jytcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRoIGNsYXNzPVwidGV4dC1jZW50ZXJcIj5BY3Rpb248L3RoPicrXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgJzwvdHI+Jytcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICAnPC90aGVhZD4nK1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgICc8dGJvZHk+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGE9MDsgYTwocmVzcG9uc2UuZGF0YSkubGVuZ3RoOyBhKyspIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdC8vIHZhciBjbGFzc05hbWUgPSAocmVzcG9uc2UuZGF0YS5hY3RpdmUgPT0gMCkgPyAnc3RyaWtldGhyb3VnaCcgOiAnJztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdC8vIHZhciB2aXNpYmlsaXR5ID0gKHJlc3BvbnNlLmRhdGEuZ3JvdXBfaWQgPT0gZ3JvdXBJZCkgPyAndGFibGUtcm93JyA6ICdub25lJztcblxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0dGFibGUgKz0gJzx0cj4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0dGFibGUgKz0gJzx0ZCBjb2xzcGFuPVwiOVwiIHN0eWxlPVwiYmFja2dyb3VuZC1jb2xvcjogI2QyZDJkMjsgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemVcIj48Yj4nK3Jlc3BvbnNlLmRhdGFbYV0uZmlyc3RfbmFtZSsnICcrcmVzcG9uc2UuZGF0YVthXS5sYXN0X25hbWUrJzwvYj48L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR0YWJsZSArPSAnPC90cj4nO1xuXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgZm9yKHZhciBpPTA7IGk8KHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXMpLmxlbmd0aDsgaSsrKSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR2YXIgY2xhc3NOYW1lID0gKHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uYWN0aXZlID09IDApID8gJ3N0cmlrZXRocm91Z2gnIDogJyc7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR2YXIgdmlzaWJpbGl0eSA9IChyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmdyb3VwX2lkID09IGdyb3VwSWQpID8gJ3RhYmxlLXJvdycgOiAnbm9uZSc7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRpZihyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLnJlbWFya3MpIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dmFyIHJlbWFya3MgPSByZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLnJlbWFya3M7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHZhciBkZXRhaWxzID0gJzxhIGhyZWY9XCIjXCIgZGF0YS10b2dnbGU9XCJwb3BvdmVyXCIgZGF0YS1wbGFjZW1lbnQ9XCJyaWdodFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIGRhdGEtY29udGVudD1cIicrcmVtYXJrcysnXCIgZGF0YS10cmlnZ2VyPVwiaG92ZXJcIj4nK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uZGV0YWlsKyc8L2E+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdH0gZWxzZSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHZhciBkZXRhaWxzID0gcmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS5kZXRhaWw7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR9XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRpZihyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmRpc2NvdW50Mi5sZW5ndGggPiAwKSB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdGlmKHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uZGlzY291bnQyWzBdLnJlYXNvbikge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHRcdHZhciByZWFzb24gPSByZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmRpc2NvdW50MlswXS5yZWFzb247XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdFx0dmFyIGRpc2NvdW50ID0gJzxhIGhyZWY9XCIjXCIgZGF0YS10b2dnbGU9XCJwb3BvdmVyXCIgZGF0YS1wbGFjZW1lbnQ9XCJsZWZ0XCIgZGF0YS1jb250YWluZXI9XCJib2R5XCIgZGF0YS1jb250ZW50PVwiJytyZWFzb24rJ1wiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCI+JytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmRpc2NvdW50MlswXS5kaXNjb3VudF9hbW91bnQrJzwvYT4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR9IGVsc2Uge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHRcdHZhciBkaXNjb3VudCA9IHJlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uZGlzY291bnQyWzBdLmRpc2NvdW50X2Ftb3VudDtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0fVxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0fSBlbHNlIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dmFyIGRpc2NvdW50ID0gJyZuYnNwOyc7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR9XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHR2YXIgX2FjdGlvbiA9ICc8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJlZGl0LXNlcnZpY2UtYWN0aW9uXCIgZGF0YS10b2dnbGU9XCJtb2RhbFwiIGRhdGEtdGFyZ2V0PVwiI2VkaXQtc2VydmljZS1tb2RhbFwiIGRhdGEtc2VydmljZWNsaWVudGlkPVwiJytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmNsaWVudF9pZCsnXCIgZGF0YS1zZXJ2aWNlc2VydmljZWlkPVwiJytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLnNlcnZpY2VfaWQrJ1wiIGRhdGEtc2VydmljZWlkPVwiJytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmlkKydcIj48aSBjbGFzcz1cImZhIGZhLXBlbmNpbC1zcXVhcmUtb1wiIGRhdGEtc2VydmljZWNsaWVudGlkPVwiJytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmNsaWVudF9pZCsnXCIgZGF0YS1zZXJ2aWNlc2VydmljZWlkPVwiJytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLnNlcnZpY2VfaWQrJ1wiIGRhdGEtc2VydmljZWlkPVwiJytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmlkKydcIj48L2k+PC9hPic7XG5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdHRhYmxlICs9ICc8dHIgY2xhc3M9XCInK2NsYXNzTmFtZSsnXCIgc3R5bGU9XCJkaXNwbGF5OicrdmlzaWJpbGl0eSsnXCI+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0udHJhY2tpbmcrJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK3Jlc3BvbnNlLmRhdGFbYV0uc2VydmljZXNbaV0uc2VydmljZV9kYXRlKyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHRhYmxlICs9ICc8dGQ+JytkZXRhaWxzKyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHRhYmxlICs9ICc8dGQ+JytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLnN0YXR1cysnPC90ZD4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR0YWJsZSArPSAnPHRkPicrcmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS5jb3N0Kyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHRhYmxlICs9ICc8dGQ+JytyZXNwb25zZS5kYXRhW2FdLnNlcnZpY2VzW2ldLmNoYXJnZSsnPC90ZD4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0XHR0YWJsZSArPSAnPHRkPicrcmVzcG9uc2UuZGF0YVthXS5zZXJ2aWNlc1tpXS50aXArJzwvdGQ+Jztcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcdFx0dGFibGUgKz0gJzx0ZD4nK2Rpc2NvdW50Kyc8L3RkPic7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHRcdHRhYmxlICs9ICc8dGQgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPicrX2FjdGlvbisnPC90ZD4nO1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFx0dGFibGUgKz0gJzwvdHI+JztcblxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIH1cblx0XHQgICAgICAgICAgICAgICAgICAgIH1cblx0XHQgICAgICAgICAgICAgICAgdGFibGUgKz0gJzwvdGJvZHk+PC90YWJsZT4nO1xuXG5cdFx0ICAgICAgICAgICAgLy9yZXR1cm4gdGFibGU7XG5cblx0XHRcdFx0XHRkaXYuaHRtbCggdGFibGUgKS5yZW1vdmVDbGFzcyggJ2xvYWRpbmcnICk7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuXHRcdFx0XHQgXG5cdFx0XHRcdCAgICByZXR1cm4gZGl2O1xuXG5cdFx0XHRcdH07XG5cdFx0XHRcdFxuXHRcdFx0XHR0YWJsZTMgPSAkKCd0YWJsZS5kdC1nYmF0Y2gnKS5EYXRhVGFibGUoIHtcblx0XHRcdCAgICAgICAgXCJkYXRhXCI6IHNlcnYsXG5cdFx0XHQgICAgICAgIFwiY29sdW1uc1wiOiBbXG5cdFx0XHQgICAgICAgICAgICB7XG5cdFx0XHQgICAgICAgICAgICBcdFwid2lkdGhcIjogXCI1JVwiLFxuXHRcdFx0ICAgICAgICAgICAgICAgIFwiY2xhc3NOYW1lXCI6ICAgICAgJ2JhdGNoLWNvbnRyb2wgdGV4dC1jZW50ZXInLFxuXHRcdFx0ICAgICAgICAgICAgICAgIFwib3JkZXJhYmxlXCI6ICAgICAgZmFsc2UsXG5cdFx0XHQgICAgICAgICAgICAgICAgXCJkYXRhXCI6ICAgICAgICAgICBudWxsLFxuXHRcdFx0ICAgICAgICAgICAgICAgIFwiZGVmYXVsdENvbnRlbnRcIjogJzxpIGNsYXNzPVwiZmEgZmEtYXJyb3ctcmlnaHRcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiPjwvaT4nXG5cdFx0XHQgICAgICAgICAgICB9LFxuXHRcdFx0ICAgICAgICAgICAgeyBcblx0XHRcdCAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjMwJVwiLFxuXHRcdFx0XHQgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihzZXJ2LCB0eXBlLCByb3cpIHtcblx0XHRcdFx0ICAgICAgICBcdFx0cmV0dXJuICc8YiBzdHlsZT1cImNvbG9yOiAjMWFiMzk0O1wiPicrbW9tZW50KFN0cmluZyhzZXJ2LnNlcnZpY2VfZGF0ZSkpLmZvcm1hdChcIk1NTU0gRCxZWVlZXCIpKyAnPC9iPidcblx0XHRcdFx0ICAgICAgICBcdH1cblx0XHRcdFx0ICAgICAgICB9LFxuXHRcdFx0XHQgICAgICAgIHtcblx0XHRcdCAgICAgICAgICAgIFx0XCJ3aWR0aFwiOiBcIjE1JVwiLFxuXHRcdFx0XHQgICAgICAgIFx0ZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihzZXJ2LCB0eXBlLCByb3csIG1ldGEpIHtcblx0XHRcdFx0ICAgICAgICBcdFx0cmV0dXJuICc8Yj4nK3NlcnYudG90YWxjb3N0KyAnPC9iPidcblx0XHRcdFx0ICAgICAgICBcdH1cblx0XHRcdFx0ICAgICAgICB9LFxuXHRcdFx0ICAgICAgICBdXG5cdFx0XHQgICAgfSk7XG5cblx0XHRcdCAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJ3RhYmxlLmR0LWdiYXRjaCB0Ym9keSB0ZC5iYXRjaC1jb250cm9sJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0XHR2YXIgdHIzID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpO1xuXHRcdFx0XHRcdHZhciByb3cgPSB0YWJsZTMucm93KCB0cjMgKTtcblx0XHRcdFx0XHRcdCBcblx0XHRcdFx0XHRpZihyb3cuY2hpbGQuaXNTaG93bigpKSB7XG5cdFx0XHRcdFx0XHRyb3cuY2hpbGQuaGlkZSgpO1xuXHRcdFx0XHRcdFx0dHIzLnJlbW92ZUNsYXNzKCdzaG93bicpO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRyb3cuY2hpbGQoIGZvcm1hdEJ5QmF0Y2gocm93LmRhdGEoKSkgKS5zaG93KCk7XG5cdFx0XHRcdFx0XHR0cjMuYWRkQ2xhc3MoJ3Nob3duJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHQkKCdib2R5Jykub24oJ2NsaWNrJywgJ3RhYmxlLmR0LWdiYXRjaCB0Ym9keSAuZWRpdC1zZXJ2aWNlLWFjdGlvbicsIGZ1bmN0aW9uIChlKSB7XG5cdFx0XHRcdFx0bGV0IHNlcnZpY2VDbGllbnRJZCA9IGUudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1zZXJ2aWNlY2xpZW50aWQnKTtcblx0XHRcdFx0XHRsZXQgc2VydmljZVNlcnZpY2VpZCA9IGUudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1zZXJ2aWNlc2VydmljZWlkJyk7XG5cdFx0XHRcdFx0bGV0IHNlcnZpY2VJZCA9IGUudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1zZXJ2aWNlaWQnKTtcblxuXHRcdFx0XHRcdHRoaXMuJHJlZnMuZWRpdHNlcnZpY2VyZWYuaW5pdChzZXJ2aWNlQ2xpZW50SWQsIHNlcnZpY2VTZXJ2aWNlaWQsIHNlcnZpY2VJZClcblx0XHRcdFx0fS5iaW5kKHRoaXMpKTtcblxuXHRcdFx0fS5iaW5kKHRoaXMpLCAxMDAwKTtcblxuXHRcdH0sXG5cblx0XHRmZXRjaERlcG9zaXRzKCkge1xuXHRcdFx0bGV0IGdyb3VwSWQgPSB0aGlzLmdyb3VwSWQ7XG5cblx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9ncm91cC8nICsgZ3JvdXBJZCArICcvZGVwb3NpdHMnKVxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0dGhpcy5kZXBvc2l0cyA9IHJlc3BvbnNlLmRhdGE7XG5cblx0XHRcdFx0XHR0aGlzLmNyZWF0ZURhdGF0YWJsZSgnZHQtZGVwb3NpdHMnKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG5cdFx0fSxcblxuXHRcdGZldGNoUGF5bWVudHMoKSB7XG5cdFx0XHRsZXQgZ3JvdXBJZCA9IHRoaXMuZ3JvdXBJZDtcblxuXHRcdFx0YXhpb3NBUEl2MS5nZXQoJy92aXNhL2dyb3VwLycgKyBncm91cElkICsgJy9wYXltZW50cycpXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHR0aGlzLnBheW1lbnRzID0gcmVzcG9uc2UuZGF0YTtcblxuXHRcdFx0XHRcdHRoaXMuY3JlYXRlRGF0YXRhYmxlKCdkdC1wYXltZW50cycpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHR9LFxuXG5cdFx0ZmV0Y2hSZWZ1bmRzKCkge1xuXHRcdFx0bGV0IGdyb3VwSWQgPSB0aGlzLmdyb3VwSWQ7XG5cblx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9ncm91cC8nICsgZ3JvdXBJZCArICcvcmVmdW5kcycpXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHR0aGlzLnJlZnVuZHMgPSByZXNwb25zZS5kYXRhO1xuXG5cdFx0XHRcdFx0dGhpcy5jcmVhdGVEYXRhdGFibGUoJ2R0LXJlZnVuZHMnKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG5cdFx0fSxcblxuXHRcdGZldGNoRGlzY291bnRzKCkge1xuXHRcdFx0bGV0IGdyb3VwSWQgPSB0aGlzLmdyb3VwSWQ7XG5cblx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9ncm91cC8nICsgZ3JvdXBJZCArICcvZGlzY291bnRzJylcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHRoaXMuZGlzY291bnRzID0gcmVzcG9uc2UuZGF0YTtcblxuXHRcdFx0XHRcdHRoaXMuY3JlYXRlRGF0YXRhYmxlKCdkdC1kaXNjb3VudHMnKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG5cdFx0fSxcblxuXHRcdHJlc2V0QWN0aW9uTG9nc0NvbXBvbmVudERhdGEoKSB7XG5cdFx0XHR0aGlzLmFjdGlvbkxvZ3MgPSBbXTtcblx0XHRcdHRoaXMuY3VycmVudF95ZWFyID0gbnVsbDtcblx0XHRcdHRoaXMuY3VycmVudF9tb250aCA9IG51bGw7XG5cdFx0XHR0aGlzLmN1cnJlbnRfZGF5ID0gbnVsbDtcblx0XHRcdHRoaXMuY195ZWFyID0gbnVsbDtcblx0XHRcdHRoaXMuY19tb250aCA9IG51bGw7XG5cdFx0XHR0aGlzLmNfZGF5ID0gbnVsbDtcblx0XHRcdHRoaXMuc19jb3VudCA9IDE7XG5cdFx0fSxcblxuXHRcdGZldGNoQWN0aW9uTG9ncygpIHtcblx0XHRcdHRoaXMucmVzZXRBY3Rpb25Mb2dzQ29tcG9uZW50RGF0YSgpO1xuXG5cdFx0XHRsZXQgZ3JvdXBJZCA9IHRoaXMuZ3JvdXBJZDtcblxuXHRcdFx0YXhpb3NBUEl2MS5nZXQoJy92aXNhL2dyb3VwLycgKyBncm91cElkICsgJy9hY3Rpb24tbG9ncycpXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHR0aGlzLmFjdGlvbkxvZ3MgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHR9LFxuXG5cdFx0cmVzZXRUcmFuc2FjdGlvbkxvZ3NDb21wb25lbnREYXRhKCkge1xuXHRcdFx0dGhpcy50cmFuc2FjdGlvbkxvZ3MgPSBbXTtcblx0XHRcdHRoaXMuY3VycmVudF95ZWFyMiA9IG51bGw7XG5cdFx0XHR0aGlzLmN1cnJlbnRfbW9udGgyID0gbnVsbDtcblx0XHRcdHRoaXMuY3VycmVudF9kYXkyID0gbnVsbDtcblx0XHRcdHRoaXMuY195ZWFyMiA9IG51bGw7XG5cdFx0XHR0aGlzLmNfbW9udGgyID0gbnVsbDtcblx0XHRcdHRoaXMuY19kYXkyID0gbnVsbDtcblx0XHRcdHRoaXMuc19jb3VudDIgPSAxO1xuXHRcdH0sXG5cblx0XHRmZXRjaFRyYW5zYWN0aW9uTG9ncygpIHtcblx0XHRcdHRoaXMucmVzZXRUcmFuc2FjdGlvbkxvZ3NDb21wb25lbnREYXRhKCk7XG5cdFx0XHRcblx0XHRcdGxldCBncm91cElkID0gdGhpcy5ncm91cElkO1xuXG5cdFx0XHRheGlvc0FQSXYxLmdldCgnL3Zpc2EvZ3JvdXAvJyArIGdyb3VwSWQgKyAnL3RyYW5zYWN0aW9uLWxvZ3MnKVxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0dGhpcy50cmFuc2FjdGlvbkxvZ3MgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHR9XG5cblx0fSxcblxuXHRjcmVhdGVkKCkge1xuXHRcdHRoaXMuaW5pdCgpOyAvLyBPa1xuXHR9LFxuXG5cdG1vdW50ZWQoKSB7XHRcblx0XHQvLyBQb3BvdmVyIGluaXRcblx0XHQkKCdib2R5JykucG9wb3Zlcih7IC8vIE9rXG5cdFx0ICAgIGh0bWw6dHJ1ZSxcblx0XHRcdHRyaWdnZXI6ICdob3ZlcicsXG5cdFx0ICAgIHNlbGVjdG9yOiAnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXSdcblx0XHR9KTtcblx0fSxcblxuXHRjb21wb25lbnRzOiB7XG5cdFx0J2VkaXQtYWRkcmVzcyc6IHJlcXVpcmUoJy4uLy4uL2NvbXBvbmVudHMvVmlzYS9FZGl0QWRkcmVzcy52dWUnKSxcblx0XHQnYWRkLW5ldy1tZW1iZXInOiByZXF1aXJlKCcuLi8uLi9jb21wb25lbnRzL1Zpc2EvQWRkTmV3TWVtYmVyLnZ1ZScpLFxuXHQgICAgJ2FkZC1uZXctc2VydmljZSc6IHJlcXVpcmUoJy4uLy4uL2NvbXBvbmVudHMvVmlzYS9BZGROZXdTZXJ2aWNlLnZ1ZScpLFxuXHQgICAgJ2VkaXQtc2VydmljZSc6IHJlcXVpcmUoJy4uLy4uL2NvbXBvbmVudHMvVmlzYS9FZGl0U2VydmljZS52dWUnKSxcblx0ICAgICdhZGQtZnVuZCc6IHJlcXVpcmUoJy4uLy4uL2NvbXBvbmVudHMvVmlzYS9BZGRGdW5kLnZ1ZScpLFxuXHQgICAgJ3RyYW5zZmVyJzogcmVxdWlyZSgnLi4vLi4vY29tcG9uZW50cy9WaXNhL1RyYW5zZmVyLnZ1ZScpLFxuXHQgICAgJ21lbWJlci10eXBlJzogcmVxdWlyZSgnLi4vLi4vY29tcG9uZW50cy9WaXNhL0dyb3VwTWVtYmVyVHlwZS52dWUnKSxcblx0ICAgICdkZWxldGUtbWVtYmVyJzogcmVxdWlyZSgnLi4vLi4vY29tcG9uZW50cy9WaXNhL0RlbGV0ZU1lbWJlci52dWUnKSxcblx0ICAgICdtdWx0aXBsZS1xdWljay1yZXBvcnQnOiByZXF1aXJlKCcuLi8uLi9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWUnKSxcblx0ICAgICdjbGllbnQtc2VydmljZXMnOiByZXF1aXJlKCcuLi8uLi9jb21wb25lbnRzL1Zpc2EvQ2xpZW50U2VydmljZXMudnVlJylcblx0fVxuXG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2dyb3Vwcy9zaG93LmpzIiwiLypcclxuXHRNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxyXG5cdEF1dGhvciBUb2JpYXMgS29wcGVycyBAc29rcmFcclxuKi9cclxuLy8gY3NzIGJhc2UgY29kZSwgaW5qZWN0ZWQgYnkgdGhlIGNzcy1sb2FkZXJcclxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbigpIHtcclxuXHR2YXIgbGlzdCA9IFtdO1xyXG5cclxuXHQvLyByZXR1cm4gdGhlIGxpc3Qgb2YgbW9kdWxlcyBhcyBjc3Mgc3RyaW5nXHJcblx0bGlzdC50b1N0cmluZyA9IGZ1bmN0aW9uIHRvU3RyaW5nKCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IFtdO1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGl0ZW0gPSB0aGlzW2ldO1xyXG5cdFx0XHRpZihpdGVtWzJdKSB7XHJcblx0XHRcdFx0cmVzdWx0LnB1c2goXCJAbWVkaWEgXCIgKyBpdGVtWzJdICsgXCJ7XCIgKyBpdGVtWzFdICsgXCJ9XCIpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHJlc3VsdC5wdXNoKGl0ZW1bMV0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gcmVzdWx0LmpvaW4oXCJcIik7XHJcblx0fTtcclxuXHJcblx0Ly8gaW1wb3J0IGEgbGlzdCBvZiBtb2R1bGVzIGludG8gdGhlIGxpc3RcclxuXHRsaXN0LmkgPSBmdW5jdGlvbihtb2R1bGVzLCBtZWRpYVF1ZXJ5KSB7XHJcblx0XHRpZih0eXBlb2YgbW9kdWxlcyA9PT0gXCJzdHJpbmdcIilcclxuXHRcdFx0bW9kdWxlcyA9IFtbbnVsbCwgbW9kdWxlcywgXCJcIl1dO1xyXG5cdFx0dmFyIGFscmVhZHlJbXBvcnRlZE1vZHVsZXMgPSB7fTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHZhciBpZCA9IHRoaXNbaV1bMF07XHJcblx0XHRcdGlmKHR5cGVvZiBpZCA9PT0gXCJudW1iZXJcIilcclxuXHRcdFx0XHRhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzW2lkXSA9IHRydWU7XHJcblx0XHR9XHJcblx0XHRmb3IoaSA9IDA7IGkgPCBtb2R1bGVzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHZhciBpdGVtID0gbW9kdWxlc1tpXTtcclxuXHRcdFx0Ly8gc2tpcCBhbHJlYWR5IGltcG9ydGVkIG1vZHVsZVxyXG5cdFx0XHQvLyB0aGlzIGltcGxlbWVudGF0aW9uIGlzIG5vdCAxMDAlIHBlcmZlY3QgZm9yIHdlaXJkIG1lZGlhIHF1ZXJ5IGNvbWJpbmF0aW9uc1xyXG5cdFx0XHQvLyAgd2hlbiBhIG1vZHVsZSBpcyBpbXBvcnRlZCBtdWx0aXBsZSB0aW1lcyB3aXRoIGRpZmZlcmVudCBtZWRpYSBxdWVyaWVzLlxyXG5cdFx0XHQvLyAgSSBob3BlIHRoaXMgd2lsbCBuZXZlciBvY2N1ciAoSGV5IHRoaXMgd2F5IHdlIGhhdmUgc21hbGxlciBidW5kbGVzKVxyXG5cdFx0XHRpZih0eXBlb2YgaXRlbVswXSAhPT0gXCJudW1iZXJcIiB8fCAhYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpdGVtWzBdXSkge1xyXG5cdFx0XHRcdGlmKG1lZGlhUXVlcnkgJiYgIWl0ZW1bMl0pIHtcclxuXHRcdFx0XHRcdGl0ZW1bMl0gPSBtZWRpYVF1ZXJ5O1xyXG5cdFx0XHRcdH0gZWxzZSBpZihtZWRpYVF1ZXJ5KSB7XHJcblx0XHRcdFx0XHRpdGVtWzJdID0gXCIoXCIgKyBpdGVtWzJdICsgXCIpIGFuZCAoXCIgKyBtZWRpYVF1ZXJ5ICsgXCIpXCI7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGxpc3QucHVzaChpdGVtKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH07XHJcblx0cmV0dXJuIGxpc3Q7XHJcbn07XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1xuLy8gbW9kdWxlIGlkID0gMlxuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiAzIDQgNSA5IDExIDEyIDEzIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wZjJmMmI3N1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCIwMTM5ODAyMFwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMGYyZjJiNzdcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMGYyZjJiNzdcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTBmMmYyYjc3XCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcbi8vIG1vZHVsZSBpZCA9IDIwXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDkiLCI8dGVtcGxhdGU+XG4gIDxkaXY+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3cgdGltZWxpbmUtbW92ZW1lbnQgdGltZWxpbmUtbW92ZW1lbnQtdG9wIG5vLWRhc2hlZCBuby1zaWRlLW1hcmdpblwiIHYtaWY9XCJsb2cueWVhciAhPSBudWxsXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGltZWxpbmUtYmFkZ2UgdGltZWxpbmUtZmlsdGVyLW1vdmVtZW50XCI+XG4gICAgICAgICAgICAgICAgPGEgaHJlZj1cIiNcIj5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+e3tsb2cueWVhcn19PC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzcz1cInJvdyB0aW1lbGluZS1tb3ZlbWVudCAgbm8tc2lkZS1tYXJnaW5cIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aW1lbGluZS1iYWRnZVwiIHYtaWY9XCJsb2cubW9udGggIT0gbnVsbFwiPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwidGltZWxpbmUtYmFsbG9vbi1kYXRlLWRheVwiPnt7bG9nLmRheX19PC9zcGFuPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwidGltZWxpbmUtYmFsbG9vbi1kYXRlLW1vbnRoXCI+e3tsb2cubW9udGh9fTwvc3Bhbj5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMiAgdGltZWxpbmUtaXRlbVwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLW9mZnNldC0xIGNvbC1zbS0xMVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aW1lbGluZS1wYW5lbCBkZWJpdHNcIiA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInRpbWVsaW5lLXBhbmVsLXVsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PHNwYW4gY2xhc3M9XCJpbXBvcnRvXCIgdi1odG1sPVwibG9nLmRhdGEudGl0bGVcIj48L3NwYW4+PC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48c3BhbiBjbGFzcz1cImNhdXNhbGVcIj57e2xvZy5kYXRhLnByb2Nlc3Nvcn19PC9zcGFuPiA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxwPjxzbWFsbCBjbGFzcz1cInRleHQtbXV0ZWRcIj48aSBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tdGltZVwiPjwvaT57e2xvZy5kYXRhLmRhdGV9fTwvc21hbGw+PC9wPiA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPC9kaXY+XG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgcHJvcHM6IFsnbG9nJywnbG9nY291bnQnXSxcbiAgbWV0aG9kczp7XG4gICAgLy8gc2VsZWN0KGlkKXtcbiAgICAgICAgLy90aGlzLiRwYXJlbnQuc2VsZWN0SXRlbShpZClcbiAgICAvLyB9LFxuICB9LFxufVxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIEFjdGlvbkxvZ3MudnVlPzZkNzgxYmY4IiwiPHRlbXBsYXRlPlxuXG5cdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCIgQHN1Ym1pdC5wcmV2ZW50PVwiYWRkRnVuZHNcIj5cblxuXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHQgICAgICAgIE9wdGlvbjpcblx0ICAgICAgICA8L2xhYmVsPlxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgIFx0PHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJvcHRpb25cIiB2LW1vZGVsPVwiYWRkRnVuZEZvcm0ub3B0aW9uXCI+XG5cdFx0ICAgIFx0XHQ8b3B0aW9uIHZhbHVlPVwiRGVwb3NpdFwiPkRlcG9zaXQ8L29wdGlvbj5cblx0XHQgICAgXHRcdDxvcHRpb24gdmFsdWU9XCJQYXltZW50XCI+UGF5bWVudDwvb3B0aW9uPlxuXHRcdCAgICBcdFx0PG9wdGlvbiB2YWx1ZT1cIkRpc2NvdW50XCI+RGlzY291bnQ8L29wdGlvbj5cblx0XHQgICAgXHRcdDxvcHRpb24gdmFsdWU9XCJSZWZ1bmRcIj5SZWZ1bmQ8L29wdGlvbj5cblx0XHQgICAgXHQ8L3NlbGVjdD5cblx0XHQgICAgPC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ2Ftb3VudCcpIH1cIj5cblx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdCAgICAgICAgQW1vdW50OlxuXHQgICAgICAgIDwvbGFiZWw+XG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJhbW91bnRcIiB2LW1vZGVsPVwiYWRkRnVuZEZvcm0uYW1vdW50XCI+XG5cblx0XHQgICAgXHQ8c3BhbiBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiYWRkRnVuZEZvcm0uZXJyb3JzLmhhcygnYW1vdW50JylcIiB2LXRleHQ9XCJhZGRGdW5kRm9ybS5lcnJvcnMuZ2V0KCdhbW91bnQnKVwiPjwvc3Bhbj5cblx0XHQgICAgPC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtaWY9XCJhZGRGdW5kRm9ybS5vcHRpb24gPT0gJ0Rpc2NvdW50JyB8fCBhZGRGdW5kRm9ybS5vcHRpb24gPT0gJ1JlZnVuZCdcIj5cblx0XHRcdFxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiBhZGRGdW5kRm9ybS5lcnJvcnMuaGFzKCdyZWFzb24nKSB9XCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgICBSZWFzb246XG5cdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cInJlYXNvblwiIHYtbW9kZWw9XCJhZGRGdW5kRm9ybS5yZWFzb25cIj5cblxuXHRcdFx0ICAgIFx0PHNwYW4gY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cImFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ3JlYXNvbicpXCIgdi10ZXh0PVwiYWRkRnVuZEZvcm0uZXJyb3JzLmdldCgncmVhc29uJylcIj48L3NwYW4+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdDwvZGl2PlxuXG5cdFx0PGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPkFkZDwvYnV0dG9uPlxuXHQgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG5cblx0PC9mb3JtPlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG4gICAgZXhwb3J0IGRlZmF1bHQge1xuXG4gICAgXHRwcm9wczogWydncm91cGlkJ10sXG5cbiAgICBcdGRhdGEoKSB7XG4gICAgXHRcdHJldHVybiB7XG4gICAgXHRcdFx0YWRkRnVuZEZvcm06IG5ldyBGb3JtKHtcblx0XHRcdFx0XHRvcHRpb246ICdEZXBvc2l0Jyxcblx0XHRcdFx0XHRhbW91bnQ6ICcnLFxuXHRcdFx0XHRcdHJlYXNvbjogJydcblx0XHRcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSlcbiAgICBcdFx0fVxuICAgIFx0fSxcblxuICAgIFx0bWV0aG9kczoge1xuICAgIFx0XHRyZXNldEFkZEZ1bmRGb3JtKCkge1xuICAgIFx0XHRcdHRoaXMuYWRkRnVuZEZvcm0ub3B0aW9uID0gJ0RlcG9zaXQnO1xuICAgIFx0XHRcdHRoaXMuYWRkRnVuZEZvcm0uYW1vdW50ID0gJyc7XG4gICAgXHRcdFx0dGhpcy5hZGRGdW5kRm9ybS5yZWFzb24gPSAnJztcbiAgICBcdFx0fSxcblxuICAgIFx0XHRhZGRGdW5kcygpIHtcbiAgICBcdFx0XHR0aGlzLmFkZEZ1bmRGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkICsgJy9hZGQtZnVuZHMnKVxuXHRcdCAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdCAgICAgICAgICAgIGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcblx0XHQgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hEZXRhaWxzKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgdGhpcy5yZXNldEFkZEZ1bmRGb3JtKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgJCgnI2FkZC1mdW5kcy1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG5cblx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XG5cdFx0ICAgICAgICAgICAgfVxuXHRcdCAgICAgICAgfSlcblx0XHQgICAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgIFx0XHR9XG4gICAgXHR9XG5cbiAgICB9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQWRkRnVuZC52dWU/M2E0OTcwYzYiLCI8dGVtcGxhdGU+XG5cdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCIgQHN1Ym1pdC5wcmV2ZW50PVwiYWRkTmV3TWVtYmVyXCI+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGFkZE5ld01lbWJlckZvcm0uZXJyb3JzLmhhcygnY2xpZW50SWQnKSB9XCI+XG5cdCAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiPlxuXHQgICAgICAgICAgICBDbGllbnRzXG5cdCAgICAgICAgPC9sYWJlbD5cblxuXHQgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMTBcIj5cblx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IGNsaWVudHNcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLWNsaWVudHNcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XG5cdFx0ICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cImNsaWVudCBpbiBjbGllbnRzXCIgOnZhbHVlPVwiY2xpZW50LmlkXCI+XG5cdFx0ICAgICAgICAgICAgICAgXHR7eyBjbGllbnQubmFtZSB9fVxuXHRcdCAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdCAgICAgICAgPC9zZWxlY3Q+XG5cdCAgICAgICAgPC9kaXY+XG5cdCAgICA8L2Rpdj5cblxuXHQgICAgPGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPkFkZDwvYnV0dG9uPlxuXHQgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG5cblx0PC9mb3JtPlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblxuICAgIGV4cG9ydCBkZWZhdWx0IHtcblxuICAgIFx0cHJvcHM6IFsnZ3JvdXBpZCddLFxuXG4gICAgXHRkYXRhKCkge1xuICAgIFx0XHRyZXR1cm4ge1xuICAgIFx0XHRcdGNsaWVudHM6IFtdLFxuXG4gICAgXHRcdFx0YWRkTmV3TWVtYmVyRm9ybTogbmV3IEZvcm0oe1xuXHRcdFx0XHRcdGNsaWVudElkczogW11cblx0XHRcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSlcbiAgICBcdFx0fVxuICAgIFx0fSxcblxuICAgIFx0bWV0aG9kczoge1xuICAgIFx0XHRmZXRjaENsaWVudHMocSkge1xuXHQgICAgXHRcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9jbGllbnQvZ2V0LWF2YWlsYWJsZS12aXNhLWNsaWVudHMnLCB7XG5cdFx0XHRcdCBcdHBhcmFtczoge1xuXHRcdFx0XHQgXHRcdGdyb3VwSWQ6IHRoaXMuZ3JvdXBpZCxcblx0XHRcdFx0IFx0XHRxOiBxXG5cdFx0XHRcdCBcdH1cblx0XHRcdFx0fSlcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdGlmKHJlc3BvbnNlLmRhdGEubGVuZ3RoID4gMCl7XG5cdFx0XHRcdFx0XHR0aGlzLmNsaWVudHMucHVzaCh7XG5cdFx0XHQgICAgICAgICAgICAgICBpZDpyZXNwb25zZS5kYXRhWzBdLmlkLFxuXHRcdFx0ICAgICAgICAgICAgICAgbmFtZTpyZXNwb25zZS5kYXRhWzBdLm5hbWUgfSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHQgXHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdFx0dmFyIG15X3ZhbCA9ICQoJy5jaG9zZW4tc2VsZWN0LWZvci1jbGllbnRzJykudmFsKCk7XG5cdFx0XHRcdFx0XHQvL2NvbnNvbGUubG9nKG15X3ZhbCk7XG5cdFx0XHRcdFx0XHQkKFwiLmNob3Nlbi1zZWxlY3QtZm9yLWNsaWVudHNcIikudmFsKG15X3ZhbCkudHJpZ2dlcihcImNob3Nlbjp1cGRhdGVkXCIpO1xuXHRcdFx0XHRcdH0sIDEwMDApO1xuXHRcdFx0XHR9KTtcbiAgICBcdFx0fSxcblxuICAgIFx0XHRyZXNldEFkZE5ld01lbWJlckZvcm0oKSB7XG4gICAgXHRcdFx0dGhpcy5hZGROZXdNZW1iZXJGb3JtID0gbmV3IEZvcm0oe1xuXHRcdFx0XHRcdGNsaWVudElkczogW11cblx0XHRcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSk7XG4gICAgXHRcdH0sXG5cbiAgICBcdFx0YWRkTmV3TWVtYmVyKCkge1xuICAgIFx0XHRcdGlmKHRoaXMuYWRkTmV3TWVtYmVyRm9ybS5jbGllbnRJZHMubGVuZ3RoID09IDApIHtcbiAgICBcdFx0XHRcdHRvYXN0ci5lcnJvcignQ2xpZW50cyBmaWVsZCBpcyByZXF1aXJlZC4nKTtcbiAgICBcdFx0XHR9IGVsc2Uge1xuICAgIFx0XHRcdFx0dGhpcy5hZGROZXdNZW1iZXJGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkICsgJy9hZGQtbWVtYmVycycpXG5cdFx0ICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0XHQgICAgICAgICAgICAgICAgaWYocmVzdWx0LnN1Y2Nlc3MpIHtcblx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLmNsaWVudHMgPSBbXTtcblx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLnJlc2V0QWRkTmV3TWVtYmVyRm9ybSgpO1xuXHRcdCAgICAgICAgICAgICAgICBcdCQoXCIuY2hvc2VuLXNlbGVjdC1mb3ItY2xpZW50c1wiKS5lbXB0eSgpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHQkKCcjYWRkLW5ldy1tZW1iZXItbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXG5cdFx0XHRcdFx0IFx0XHR0b2FzdHIuc3VjY2VzcyhyZXN1bHQubWVzc2FnZSk7XG5cblx0XHRcdFx0XHQgXHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmZldGNoTWVtYmVycygpO1xuXG5cdFx0XHRcdFx0IFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC4kcmVmcy5hZGRuZXdzZXJ2aWNlcmVmLmZldGNoR3JvdXBQYWNrYWdlc1NlcnZpY2VzTGlzdCgpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdCAgICAgICAgICAgIH0pXG5cdFx0ICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG4gICAgXHRcdFx0fVxuICAgIFx0XHR9XG4gICAgXHR9LFxuXG4gICAgXHRtb3VudGVkKCkge1xuXHQgIFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItY2xpZW50cycpLmNob3Nlbih7XG5cdFx0XHRcdHdpZHRoOiBcIjEwMCVcIixcblx0XHRcdFx0bm9fcmVzdWx0c190ZXh0OiBcIlNlYXJjaGluZyBDbGllbnQgIyA6IFwiLFxuXHRcdFx0XHRzZWFyY2hfY29udGFpbnM6IHRydWVcblx0XHRcdH0pO1xuXG5cdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItY2xpZW50cycpLm9uKCdjaGFuZ2UnLCAoKSA9PiB7XG5cdFx0XHRcdGxldCBjbGllbnRJZHMgPSAkKCcuY2hvc2VuLXNlbGVjdC1mb3ItY2xpZW50cycpLnZhbCgpO1xuXG5cdFx0XHRcdHRoaXMuYWRkTmV3TWVtYmVyRm9ybS5jbGllbnRJZHMgPSBjbGllbnRJZHM7XG5cdFx0XHR9KTtcblxuXHRcdFx0JCgnYm9keScpLm9uKCdrZXl1cCcsICcuY2hvc2VuLWNvbnRhaW5lciBpbnB1dFt0eXBlPXRleHRdJywgKGUpID0+IHtcblx0XHRcdFx0bGV0IHEgPSAkKCcuY2hvc2VuLWNvbnRhaW5lciBpbnB1dFt0eXBlPXRleHRdJykudmFsKCk7XG5cdFx0XHRcdGlmKCBxLmxlbmd0aCA+PSA0ICl7XG5cdFx0XHRcdFx0dGhpcy5mZXRjaENsaWVudHMocSk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuICAgIFx0fVxuXG4gICAgfVxuXG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIEFkZE5ld01lbWJlci52dWU/NWMxNzIzOTQiLCI8dGVtcGxhdGU+XG5cdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCIgQHN1Ym1pdC5wcmV2ZW50PVwiYWRkTmV3U2VydmljZVwiPlxuXG5cdFx0PGRpdiB2LXNob3c9XCJzY3JlZW4gPT0gMVwiPlxuXHQgICAgXHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ3NlcnZpY2VzJykgfVwiPlxuXHRcdCAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdCAgICAgICAgICAgIFNlcnZpY2U6XG5cdFx0ICAgICAgICA8L2xhYmVsPlxuXG5cdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IFNlcnZpY2VcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjM1MHB4O1wiIHRhYmluZGV4PVwiNFwiPlxuXHRcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwic2VydmljZSBpbiBzZXJ2aWNlc0xpc3RcIiA6dmFsdWU9XCJzZXJ2aWNlLmlkXCI+XG5cdFx0ICAgICAgICAgICAgICAgIFx0e3sgKHNlcnZpY2UuZGV0YWlsKS50cmltKCkgfX1cblx0XHQgICAgICAgICAgICAgICAgPC9vcHRpb24+XG5cdFx0ICAgICAgICAgICAgPC9zZWxlY3Q+XG5cblx0XHQgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnc2VydmljZXMnKVwiIHYtdGV4dD1cImFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5nZXQoJ3NlcnZpY2VzJylcIj48L3NwYW4+XG5cblx0XHQgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3BhY2VyLTEwXCI+PC9kaXY+XG5cblx0XHQgICAgICAgICAgICA8cD5cblx0XHQgICAgICAgICAgICBcdElmIHlvdSB3YW50IHRvIHZpZXcgYWxsIHNlcnZpY2VzIGF0IG9uY2UsIHBsZWFzZSBcblx0XHQgICAgICAgICAgICBcdDxhIGhyZWY9XCIvdmlzYS9zZXJ2aWNlL3ZpZXctYWxsXCIgdGFyZ2V0PVwiX2JsYW5rXCI+Y2xpY2sgaGVyZTwvYT5cblx0XHQgICAgICAgICAgICA8L3A+XG5cdFx0ICAgICAgICA8L2Rpdj5cblx0XHQgICAgPC9kaXY+XG5cblx0XHQgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHQgICAgXHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICAgICAgTm90ZTpcblx0XHQgICAgICAgIDwvbGFiZWw+XG5cdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cIm5vdGVcIiB2LW1vZGVsPVwiYWRkTmV3U2VydmljZUZvcm0ubm90ZVwiIHBsYWNlaG9sZGVyPVwiZm9yIGludGVybmFsIHVzZSBvbmx5LCBvbmx5IGVtcGxveWVlcyBjYW4gc2VlLlwiPlxuXHRcdCAgICAgICAgPC9kaXY+XG5cdFx0ICAgIDwvZGl2PlxuXG5cdFx0ICAgIDxkaXYgdi1zaG93PVwiYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMubGVuZ3RoIDw9IDFcIj5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgICAgICAgQ29zdDpcblx0XHRcdCAgICAgICAgPC9sYWJlbD5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cImNvc3RcIiA6dmFsdWU9XCJjb3N0XCIgcmVhZG9ubHk+XG5cdFx0XHQgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXG48IS0tIFx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgICAgICAgUmVwb3J0IDpcblx0XHRcdCAgICAgICAgPC9sYWJlbD5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cInJlcG9ydGluZ1wiIHYtbW9kZWw9XCJhZGROZXdTZXJ2aWNlRm9ybS5yZXBvcnRpbmdcIj5cblx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+IC0tPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiBhZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdkaXNjb3VudCcpIH1cIj5cblx0XHRcdCAgICBcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICAgICAgIERpc2NvdW50OlxuXHRcdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTRcIj5cblx0XHRcdCAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJkaXNjb3VudFwiIHYtbW9kZWw9XCJhZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudFwiPlxuXHRcdFx0ICAgICAgICBcdDxzcGFuIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJhZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdkaXNjb3VudCcpXCIgdi10ZXh0PVwiYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmdldCgnZGlzY291bnQnKVwiPjwvc3Bhbj5cblx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIiB2LWlmPVwiYWRkTmV3U2VydmljZUZvcm0uZGlzY291bnQhPScnXCI+XG5cdFx0XHQgICAgICAgICAgICBSZWFzb246XG5cdFx0XHQgICAgICAgIDwvbGFiZWw+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNFwiIHYtaWY9XCJhZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudCE9JydcIj5cblx0XHRcdCAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJyZWFzb25cIiB2LW1vZGVsPVwiYWRkTmV3U2VydmljZUZvcm0ucmVhc29uXCI+XG5cdFx0XHQgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdCAgICA8L2Rpdj5cblxuXHRcdCAgICA8YSBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIgQGNsaWNrPVwiY2hhbmdlU2NyZWVuKDIpXCI+Q29udGludWU8L2E+XG5cdCAgICBcdDxhIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYT5cbiAgICBcdDwvZGl2PlxuXG4gICAgXHQ8ZGl2IHYtc2hvdz1cInNjcmVlbiA9PSAyXCI+XG4gICAgXHRcdDxkaXYgY2xhc3M9XCJ0YWJsZS1yZXNwb25zaXZlXCI+XG4gICAgICAgICAgICAgICAgPHRhYmxlIGNsYXNzPVwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJlZCB0YWJsZS1ob3ZlciBhZGQtbmV3LXNlcnZpY2UtZGF0YXRhYmxlXCI+XG4gICAgICAgICAgICAgICAgICAgIDx0aGVhZD5cblx0ICAgICAgICAgICAgICAgICAgICA8dHI+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aCBjbGFzcz1cInRleHQtY2VudGVyXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIEBjbGljaz1cInRvZ2dsZUFsbENoZWNrYm94XCI+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIDwvdGg+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5OYW1lPC90aD5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgPHRoPkNsaWVudCBJRDwvdGg+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5QYWNrYWdlczwvdGg+XG5cdCAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICAgICAgPC90aGVhZD5cbiAgICAgICAgICAgICAgICAgICAgPHRib2R5PlxuICAgICAgICAgICAgICAgICAgICBcdDx0ciB2LWZvcj1cImRhdGEgaW4gY2xpZW50c1wiPlxuICAgICAgICAgICAgICAgICAgICBcdFx0PHRkIGNsYXNzPVwidGV4dC1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgXHRcdFx0PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIDp2YWx1ZT1cImRhdGEuY2xpZW50LmlkXCIgdi1tb2RlbD1cImFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHNcIj5cbiAgICAgICAgICAgICAgICAgICAgXHRcdDwvdGQ+XG4gICAgICAgICAgICAgICAgICAgIFx0XHQ8dGQ+e3sgZGF0YS5jbGllbnQuZnVsbF9uYW1lIH19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgXHRcdDx0ZD57eyBkYXRhLmNsaWVudC5pZCB9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgIFx0XHQ8dGQ+XG4gICAgICAgICAgICAgICAgICAgIFx0XHRcdDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiA6Y2xhc3M9XCIncGFja2FnZS1zZWxlY3QtJyArIGRhdGEuY2xpZW50LmlkXCIgXG4gICAgICAgICAgICAgICAgICAgIFx0XHRcdDpkaXNhYmxlZD1cImFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHMuaW5kZXhPZihkYXRhLmNsaWVudC5pZCkgPT0gLTFcIj5cbiAgICAgICAgICAgICAgICAgICAgXHRcdFx0XHQ8b3B0aW9uIHZhbHVlPVwiR2VuZXJhdGUgbmV3IHBhY2thZ2VcIj5HZW5lcmF0ZSBuZXcgcGFja2FnZTwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICBcdFx0XHRcdDxvcHRpb24gdi1mb3I9XCJwYWNrYWdlIGluIGRhdGEuY2xpZW50LnBhY2thZ2VzXCIgOnZhbHVlPVwicGFja2FnZS50cmFja2luZ1wiIHYtaWY9XCJwYWNrYWdlLnN0YXR1cyAhPSA0XCI+XG4gICAgICAgICAgICAgICAgICAgIFx0XHRcdFx0XHR7e3BhY2thZ2UudHJhY2tpbmd9fSBcbiAgICAgICAgICAgICAgICAgICAgXHRcdFx0XHRcdCh7eyBwYWNrYWdlLmxvZ19kYXRlIHwgY29tbW9uRGF0ZSB9fSlcbiAgICAgICAgICAgICAgICAgICAgXHRcdFx0XHQ8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgXHRcdFx0PC9zZWxlY3Q+XG4gICAgICAgICAgICAgICAgICAgIFx0XHQ8L3RkPlxuICAgICAgICAgICAgICAgICAgICBcdDwvdHI+XG4gICAgICAgICAgICAgICAgICAgIDwvdGJvZHk+XG4gICAgICAgICAgICAgICAgPC90YWJsZT5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICA8YSBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIgQGNsaWNrPVwiY2hhbmdlU2NyZWVuKDMpXCI+Q29udGludWU8L2E+XG5cdCAgICBcdDxhIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgQGNsaWNrPVwiY2hhbmdlU2NyZWVuKDEpXCI+QmFjazwvYT5cbiAgICBcdDwvZGl2PlxuXG4gICAgXHQ8ZGl2IHYtc2hvdz1cInNjcmVlbiA9PSAzXCI+XG4gICAgXHRcdDxkaXYgY2xhc3M9XCJpb3Mtc2Nyb2xsXCI+XG4gICAgXHRcdFx0PGxhYmVsIGNsYXNzPVwiY2hlY2tib3gtaW5saW5lXCI+XG4gICAgXHRcdFx0XHQ8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgQGNsaWNrPVwiY2hlY2tBbGxcIiBjbGFzcz1cImNoZWNrYm94LWFsbFwiPiBDaGVjayBhbGwgcmVxdWlyZWQgZG9jdW1lbnRzXG4gICAgXHRcdFx0PC9sYWJlbD5cblxuICAgIFx0XHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj4gPCEtLSBzcGFjZXIgLS0+XG5cblx0ICAgIFx0XHQ8ZGl2IHYtZm9yPVwiKGNsaWVudElkLCBpbmRleDEpIGluIGFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHNcIiBjbGFzcz1cInBhbmVsIHBhbmVsLWRlZmF1bHRcIj5cblx0ICAgICAgICAgICAgICAgXHQ8ZGl2IGNsYXNzPVwicGFuZWwtaGVhZGluZ1wiPlxuXHQgICAgICAgICAgICAgICAgICAgIDxzdHJvbmc+e3sgZ2V0Q2xpZW50TmFtZShjbGllbnRJZCkgfX08L3N0cm9uZz5cblx0ICAgICAgICAgICAgICAgIDwvZGl2PlxuXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cblx0ICAgICAgICAgICAgICAgICAgICBcblx0ICAgICAgICAgICAgICAgIFx0PGRpdiB2LWZvcj1cIihzZXJ2aWNlSWQsIGluZGV4MikgaW4gYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXNcIiBjbGFzcz1cInBhbmVsIHBhbmVsLWRlZmF1bHRcIj5cblx0XHRcdCAgICAgICAgICAgICAgIFx0PGRpdiBjbGFzcz1cInBhbmVsLWhlYWRpbmdcIj5cblx0XHRcdCAgICAgICAgICAgICAgICAgICAge3sgZ2V0U2VydmljZU5hbWUoc2VydmljZUlkKSB9fVxuXHRcdFx0ICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjaGVja2JveC1pbmxpbmUgcHVsbC1yaWdodFwiPlxuXHRcdFx0ICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBAY2xpY2s9XCJjaGVja0FsbFwiIDpjbGFzcz1cIidjaGVja2JveC0nICsgaW5kZXgxICsgJy0nICsgaW5kZXgyXCIgOmRhdGEtY2hvc2VuLWNsYXNzPVwiJ2Nob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MtJyArIGluZGV4MSArICctJyArIGluZGV4MlwiPiBDaGVjayBhbGwgcmVxdWlyZWQgZG9jdW1lbnRzXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XG5cdFx0XHQgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cblx0XHRcdCAgICAgICAgICAgICAgICAgICAgXG5cdFx0XHQgICAgICAgICAgICAgICAgXHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHRcdFx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgIFx0UmVxdWlyZWQgRG9jdW1lbnRzOlxuXHRcdFx0XHRcdFx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHRcdFx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IERvY3NcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3NcIiA6Y2xhc3M9XCInY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcy0nICsgaW5kZXgxICsgJy0nICsgaW5kZXgyXCIgbXVsdGlwbGUgc3R5bGU9XCJ3aWR0aDozNTBweDtcIiB0YWJpbmRleD1cIjRcIj5cblx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XCJkb2MgaW4gcmVxdWlyZWREb2NzW2luZGV4Ml1cIiA6dmFsdWU9XCJkb2MuaWRcIj5cblx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgIFx0e3sgZG9jLnRpdGxlIH19XG5cdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cblx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgPC9zZWxlY3Q+XG5cdFx0XHRcdFx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0XHRcdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICBcdE9wdGlvbmFsIERvY3VtZW50czpcblx0XHRcdFx0XHRcdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0XHRcdFx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHRcdFx0XHRcdCAgICAgICAgICAgIDxzZWxlY3QgZGF0YS1wbGFjZWhvbGRlcj1cIlNlbGVjdCBEb2NzXCIgY2xhc3M9XCJjaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzXCIgOmNsYXNzPVwiJ2Nob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MtJyArIGluZGV4MSArICctJyArIGluZGV4MlwiIG11bHRpcGxlIHN0eWxlPVwid2lkdGg6MzUwcHg7XCIgdGFiaW5kZXg9XCI0XCI+XG5cdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwiZG9jIGluIG9wdGlvbmFsRG9jc1tpbmRleDJdXCIgOnZhbHVlPVwiZG9jLmlkXCI+XG5cdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICBcdHt7IGRvYy50aXRsZSB9fVxuXHRcdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgPC9vcHRpb24+XG5cdFx0XHRcdFx0XHRcdCAgICAgICAgICAgIDwvc2VsZWN0PlxuXHRcdFx0XHRcdFx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0ICAgICAgICAgICAgICAgIDwvZGl2PiA8IS0tIFNFUlZJQ0UgcGFuZWwtYm9keSAtLT5cblx0XHRcdCAgICAgICAgICAgIDwvZGl2PiA8IS0tIFNFUlZJQ0UgcGFuZWwgLS0+XG5cblx0ICAgICAgICAgICAgICAgIDwvZGl2PiA8IS0tIENMSUVOVCBwYW5lbC1ib2R5IC0tPlxuXHQgICAgICAgICAgICA8L2Rpdj4gPCEtLSBDTElFTlQgcGFuZWwgLS0+XG4gICAgICAgICAgICA8L2Rpdj5cblxuICAgIFx0XHQ8YnV0dG9uIHYtaWY9XCIhbG9hZGluZ1wiIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCI+U2F2ZTwvYnV0dG9uPlxuICAgIFx0XHQ8YnV0dG9uIHYtaWY9XCJsb2FkaW5nXCIgdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHRcIj5cblx0ICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNrLXNwaW5uZXIgc2stc3Bpbm5lci13YXZlXCIgc3R5bGU9XCJ3aWR0aDo0MHB4OyBoZWlnaHQ6MjBweDtcIj5cblx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzay1yZWN0MVwiPjwvZGl2PlxuXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNrLXJlY3QyXCI+PC9kaXY+XG5cdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2stcmVjdDNcIj48L2Rpdj5cblx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzay1yZWN0NFwiPjwvZGl2PlxuXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNrLXJlY3Q1XCI+PC9kaXY+XG5cdCAgICAgICAgICAgICA8L2Rpdj5cblx0ICAgICAgICA8L2J1dHRvbj5cblx0ICAgIFx0PGEgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIiBAY2xpY2s9XCJjaGFuZ2VTY3JlZW4oMilcIj5CYWNrPC9hPlxuICAgIFx0PC9kaXY+XG5cbiAgICA8L2Zvcm0+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG4gICAgZXhwb3J0IGRlZmF1bHR7XG4gICAgXHRwcm9wczogWydncm91cGlkJ10sXG5cbiAgICAgICAgZGF0YSgpIHtcbiAgICAgICAgXHRyZXR1cm4ge1xuICAgICAgICBcdFx0bG9hZGluZzogZmFsc2UsXG5cbiAgICAgICAgXHRcdHNjcmVlbjogMSxcblxuICAgICAgICBcdFx0c2VydmljZXNMaXN0OiBbXSxcblxuICAgICAgICBcdFx0Y29zdDogJycsXG5cbiAgICAgICAgXHRcdGNsaWVudHM6IFtdLFxuXG4gICAgICAgIFx0XHRkZXRhaWxBcnJheTogW10sXG4gICAgICAgIFx0XHRyZXF1aXJlZERvY3M6IFtdLFxuICAgICAgICBcdFx0b3B0aW9uYWxEb2NzOiBbXSxcblxuICAgICAgICBcdFx0YWRkTmV3U2VydmljZUZvcm06IG5ldyBGb3JtKHtcblx0XHRcdFx0XHRzZXJ2aWNlczogW10sXG5cdFx0XHRcdFx0bm90ZTogJycsXG5cdFx0XHRcdFx0ZGlzY291bnQ6ICcnLFxuXHRcdFx0XHRcdHJlYXNvbjogJycsXG5cdFx0XHRcdFx0cmVwb3J0aW5nOiAnJyxcblxuXHRcdFx0XHRcdGNsaWVudHM6IFtdLFxuXHRcdFx0XHRcdHBhY2thZ2VzOltdLFxuXHRcdFx0XHRcdGRvY3M6IFtdXG5cdFx0XHRcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXHRcbiAgICAgICAgXHR9XG4gICAgICAgIH0sXG5cbiAgICAgICAgbWV0aG9kczoge1xuICAgICAgICBcdGZldGNoU2VydmljZUxpc3QoKSB7XG4gICAgICAgIFx0XHRheGlvcy5nZXQoJy92aXNhL3NlcnZpY2UtbWFuYWdlci9zaG93Jylcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHR0aGlzLnNlcnZpY2VzTGlzdCA9IHJlc3BvbnNlLmRhdGEuZmlsdGVyKHIgPT4gci5wYXJlbnRfaWQgIT0gMCk7XG5cblx0XHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHQkKFwiLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VcIikudHJpZ2dlcihcImNob3Nlbjp1cGRhdGVkXCIpO1xuXHRcdFx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikgKTtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0cmVzZXRBZGROZXdTZXJ2aWNlRm9ybSgpIHtcbiAgICAgICAgXHRcdHRoaXMuc2NyZWVuID0gMTtcblxuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcyA9IFtdO1xuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5ub3RlID0gJyc7XG4gICAgICAgIFx0XHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50ID0gJyc7XG4gICAgICAgIFx0XHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnJlYXNvbiA9ICcnO1xuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5yZXBvcnRpbmcgPSAnJztcbiAgICAgICAgXHRcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cyA9IFtdO1xuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5wYWNrYWdlcyA9IFtdO1xuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5kb2NzID0gW107XG5cbiAgICAgICAgXHRcdHRoaXMuY29zdCA9ICcnO1xuICAgICAgICBcdFx0Ly8gRGVzZWxlY3QgQWxsXG5cdFx0XHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlLCAuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS52YWwoJycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cdFx0XHRcdFxuXHRcdFx0XHR0aGlzLmRldGFpbEFycmF5ID0gW107XG5cdFx0XHRcdHRoaXMucmVxdWlyZWREb2NzID0gW107XG5cdFx0XHRcdHRoaXMub3B0aW9uYWxEb2NzID0gW107XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGFkZE5ld1NlcnZpY2UoKSB7XG4gICAgICAgIFx0XHR0aGlzLmxvYWRpbmcgPSB0cnVlO1xuXG4gICAgICAgIFx0XHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnBhY2thZ2VzID0gW107XG5cdFx0XHRcdHZhciB0YmwgPSAkKCcuYWRkLW5ldy1zZXJ2aWNlLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpO1xuXHQgICAgICAgIFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzLmZvckVhY2goKGMsIGluZGV4KSA9PiB7XG5cdCAgICAgICAgXHRcdGxldCB2YWx1ZSA9IHRibC4kKCdzZWxlY3QucGFja2FnZS1zZWxlY3QtJyArIGMpLnZhbCgpO1xuXHQgICAgICAgIFx0XHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnBhY2thZ2VzLnB1c2godmFsdWUpO1xuXHQgICAgICAgIFx0fSk7XG5cblx0ICAgICAgICBcdHZhciBkb2NzQXJyYXkgPSBbXTtcbiAgICAgICAgXHRcdHZhciByZXF1aXJlZERvY3MgPSAnJztcbiAgICAgICAgXHRcdHZhciBvcHRpb25hbERvY3MgPSAnJztcbiAgICAgICAgXHRcdHZhciBoYXNSZXF1aXJlZERvY3NFcnJvcnMgPSBbXTtcblxuXHQgICAgICAgIFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzLmZvckVhY2goKGNsaWVudCwgaW5kZXgxKSA9PiB7XG5cdCAgICAgICAgXHRcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMuZm9yRWFjaCgoc2VydmljZSwgaW5kZXgyKSA9PiB7XG5cdCAgICAgICAgXHRcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MtJyArIGluZGV4MSArICctJyArIGluZGV4MiArJyBvcHRpb246c2VsZWN0ZWQnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdCAgICAgICAgXHRcdFx0cmVxdWlyZWREb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XG5cdFx0XHRcdFx0ICAgIH0pO1xuXG5cdCAgICAgICAgXHRcdFx0bGV0IHJlcXVpcmVkT3B0aW9ucyA9ICQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpbmRleDEgKyAnLScgKyBpbmRleDIgKycgb3B0aW9uJykubGVuZ3RoO1xuXHRcdFx0XHRcdCAgICBsZXQgcmVxdWlyZWRDb3VudCA9ICQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpbmRleDEgKyAnLScgKyBpbmRleDIgKycgb3B0aW9uOnNlbGVjdGVkJykubGVuZ3RoO1xuXHQgICAgICAgIFx0XHRcdGlmKHJlcXVpcmVkQ291bnQgPT0gMCAmJiByZXF1aXJlZE9wdGlvbnMgPiAwKSB7XG5cdCAgICAgICAgXHRcdFx0XHRoYXNSZXF1aXJlZERvY3NFcnJvcnMucHVzaCh7XG5cdCAgICAgICAgXHRcdFx0XHRcdGNsaWVudDogdGhpcy5nZXRDbGllbnROYW1lKGNsaWVudCksXG5cdCAgICAgICAgXHRcdFx0XHRcdHNlcnZpY2U6IHRoaXMuZ2V0U2VydmljZU5hbWUoc2VydmljZSlcblx0ICAgICAgICBcdFx0XHRcdH0pO1xuXHQgICAgICAgIFx0XHRcdH1cblxuXHRcdFx0XHRcdCAgICAkKCcuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcy0nICsgaW5kZXgxICsgJy0nICsgaW5kZXgyICsgJyBvcHRpb246c2VsZWN0ZWQnKS5lYWNoKGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCAgICAgICAgb3B0aW9uYWxEb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XG5cdFx0XHRcdFx0ICAgIH0pO1xuXG5cdFx0XHRcdFx0ICAgIGRvY3NBcnJheS5wdXNoKChyZXF1aXJlZERvY3MgKyBvcHRpb25hbERvY3MpLnNsaWNlKDAsIC0xKSk7XG5cdFx0XHRcdCAgICBcdHJlcXVpcmVkRG9jcyA9ICcnO1xuXHRcdFx0XHQgICAgXHRvcHRpb25hbERvY3MgPSAnJztcblx0ICAgICAgICBcdFx0fSk7XG5cdCAgICAgICAgXHR9KTtcbiAgICAgICAgXHRcdFxuICAgICAgICBcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5kb2NzID0gZG9jc0FycmF5O1xuXG4gICAgICAgIFx0XHRpZihoYXNSZXF1aXJlZERvY3NFcnJvcnMubGVuZ3RoID4gMCl7XG4gICAgICAgIFx0XHRcdGZvcihsZXQgaT0wOyBpPGhhc1JlcXVpcmVkRG9jc0Vycm9ycy5sZW5ndGg7IGkrKykge1xuICAgICAgICBcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBmcm9tIHJlcXVpcmVkIGRvY3VtZW50cy4nLCBoYXNSZXF1aXJlZERvY3NFcnJvcnNbaV0uY2xpZW50ICsgJyAtICcgKyBoYXNSZXF1aXJlZERvY3NFcnJvcnNbaV0uc2VydmljZSk7XG4gICAgICAgIFx0XHRcdH1cbiAgICAgICAgXHRcdFx0dGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICBcdFx0fSBlbHNlIHtcblxuXHRcdCAgICAgICAgXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkICsgJy9hZGQtc2VydmljZScpXG5cdFx0XHQgICAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHQgICAgICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0ICAgICAgICAgICAgICAgIFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hEZXRhaWxzKCk7XG5cdFx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5mZXRjaE1lbWJlcnMoKTtcblxuXHRcdFx0ICAgICAgICAgICAgICAgIFx0dGhpcy5mZXRjaEdyb3VwUGFja2FnZXNTZXJ2aWNlc0xpc3QoKTtcblx0XHRcdCAgICAgICAgICAgICAgICBcdHRoaXMucmVzZXRBZGROZXdTZXJ2aWNlRm9ybSgpO1xuXG5cdFx0XHQgICAgICAgICAgICAgICAgXHQkKCcjYWRkLW5ldy1zZXJ2aWNlLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdFx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XG5cdFx0XHQgICAgICAgICAgICAgICAgfVxuXG5cdFx0XHQgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG5cdFx0XHQgICAgICAgICAgICB9KVxuXHRcdFx0ICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcblx0XHRcdCAgICAgICAgICAgIFx0Zm9yKHZhciBrZXkgaW4gZXJyb3IpIHtcblx0XHRcdCAgICAgICAgICAgIFx0XHRpZihlcnJvci5oYXNPd25Qcm9wZXJ0eShrZXkpKSB0b2FzdHIuZXJyb3IoZXJyb3Jba2V5XVswXSlcblx0XHRcdCAgICAgICAgICAgIFx0fVxuXG5cdFx0XHQgICAgICAgICAgICBcdHRoaXMubG9hZGluZyA9IGZhbHNlO1xuXHRcdFx0ICAgICAgICAgICAgfSk7XG5cdFx0XHQgICAgfVxuXHRcdFx0fSxcblxuXHRcdFx0Z2V0RG9jcyhzZXJ2aWNlc0lkKSB7XG5cdFx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvZ2V0LWRvY3MnLCB7XG5cdFx0XHRcdFx0XHRwYXJhbXM6IHtcblx0XHRcdFx0XHQgICAgICBcdHNlcnZpY2VzSWQ6IHNlcnZpY2VzSWRcblx0XHRcdFx0XHQgICAgfVxuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcblx0XHRcdFx0XHRcdHRoaXMuZGV0YWlsQXJyYXkgPSByZXN1bHQuZGF0YS5kZXRhaWxBcnJheTtcblx0XHRcdFx0ICAgICAgICB0aGlzLnJlcXVpcmVkRG9jcyA9IHJlc3VsdC5kYXRhLmRvY3NOZWVkZWRBcnJheTtcblx0XHRcdFx0ICAgICAgICB0aGlzLm9wdGlvbmFsRG9jcyA9IHJlc3VsdC5kYXRhLmRvY3NPcHRpb25hbEFycmF5O1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0fSxcblxuXHRcdFx0Z2V0Q2xpZW50TmFtZShjbGllbnRJZCkge1xuXHRcdFx0XHR2YXIgY2xpZW50TmFtZSA9ICcnO1xuXHRcdFx0XHR0aGlzLmNsaWVudHMuZm9yRWFjaChjbGllbnQgPT4ge1xuXHRcdFx0XHRcdGlmKGNsaWVudC5jbGllbnQuaWQgPT0gY2xpZW50SWQpIHtcblx0XHRcdFx0XHRcdGNsaWVudE5hbWUgPSBjbGllbnQuY2xpZW50LmZpcnN0X25hbWUgKyAnICcgKyBjbGllbnQuY2xpZW50Lmxhc3RfbmFtZTsgXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRyZXR1cm4gY2xpZW50TmFtZTtcblx0XHRcdH0sXG5cblx0XHRcdGdldFNlcnZpY2VOYW1lKHNlcnZpY2VJZCkge1xuXHRcdFx0XHR2YXIgc2VydmljZU5hbWUgPSAnJztcblx0XHRcdFx0dGhpcy5zZXJ2aWNlc0xpc3QuZm9yRWFjaChzZXJ2aWNlID0+IHtcblx0XHRcdFx0XHRpZihzZXJ2aWNlLmlkID09IHNlcnZpY2VJZCkge1xuXHRcdFx0XHRcdFx0c2VydmljZU5hbWUgPSBzZXJ2aWNlLmRldGFpbDsgXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRyZXR1cm4gc2VydmljZU5hbWU7XG5cdFx0XHR9LFxuXG5cdFx0XHRpbml0Q2hvc2VuU2VsZWN0KCkge1xuXHRcdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZScpLmNob3Nlbih7XG5cdFx0XHRcdFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdFx0XHRcdG5vX3Jlc3VsdHNfdGV4dDogXCJObyByZXN1bHQvcyBmb3VuZC5cIixcblx0XHRcdFx0XHRzZWFyY2hfY29udGFpbnM6IHRydWVcblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0bGV0IHZtID0gdGhpcztcblxuXHRcdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZScpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRsZXQgc2VydmljZXMgPSAkKHRoaXMpLnZhbCgpO1xuXG5cdFx0XHRcdFx0dm0uZ2V0RG9jcyhzZXJ2aWNlcyk7XG5cblx0XHRcdFx0XHR2bS5hZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcyA9IHNlcnZpY2VzO1xuXG5cdFx0XHRcdFx0Ly8gU2V0IGNvc3QgaW5wdXRcblx0XHRcdFx0XHRpZihzZXJ2aWNlcy5sZW5ndGggPT0gMSkge1xuXHRcdFx0XHRcdFx0dm0uc2VydmljZXNMaXN0Lm1hcChzID0+IHtcblx0XHRcdFx0XHRcdFx0aWYocy5pZCA9PSBzZXJ2aWNlc1swXSkge1xuXHRcdFx0XHRcdFx0XHRcdHZtLmNvc3QgPSBzLmNvc3Q7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR2bS5jb3N0ID0gJyc7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHR9LFxuXG5cdFx0XHRjaGFuZ2VTY3JlZW4oc2NyZWVuKSB7XG5cdFx0XHRcdGlmKHNjcmVlbiA9PSAyKSB7XG5cdFx0XHRcdFx0aWYodGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcy5sZW5ndGggPT0gMCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGF0IGxlYXN0IG9uZSBzZXJ2aWNlJyk7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYoc2NyZWVuID09IDMpIHtcblx0XHRcdFx0XHRpZih0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHMubGVuZ3RoID09IDApIHtcblx0ICAgICAgICBcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgYXQgbGVhc3Qgb25lIG1lbWJlcicpO1xuXHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHQgICAgICAgIFx0XHR9XG5cblx0ICAgICAgICBcdFx0c2V0VGltZW91dCgoKSA9PiB7XG5cdFx0XHRcdCAgICAgICAgJCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykuY2hvc2VuKCdkZXN0cm95Jyk7XG5cdFx0XHRcdCAgICAgICAgJCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykuY2hvc2VuKHtcblx0XHRcdFx0XHRcdFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdFx0XHRcdFx0XHRub19yZXN1bHRzX3RleHQ6IFwiTm8gcmVzdWx0L3MgZm91bmQuXCIsXG5cdFx0XHRcdFx0XHRcdHNlYXJjaF9jb250YWluczogdHJ1ZVxuXHRcdFx0XHRcdFx0fSk7XG5cblx0XHRcdCAgICAgICAgXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xuXHRcdFx0ICAgICAgICB9LCAxMDAwKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdHRoaXMuc2NyZWVuID0gc2NyZWVuO1xuXHRcdFx0fSxcblxuXHRcdFx0ZmV0Y2hHcm91cFBhY2thZ2VzU2VydmljZXNMaXN0KCkge1xuXHRcdFx0XHRheGlvcy5nZXQoJy92aXNhL2dyb3VwLycrIHRoaXMuZ3JvdXBpZCArJy9wYWNrYWdlcy1zZXJ2aWNlcycpXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5jbGllbnRzID0gcmVzcG9uc2UuZGF0YTtcblxuXHRcdFx0XHRcdFx0dGhpcy5jYWxsRGF0YVRhYmxlKCk7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpICk7XG5cdFx0XHR9LFxuXG5cdFx0XHRjYWxsRGF0YVRhYmxlKCkge1xuXHRcdFx0XHQkKCcuYWRkLW5ldy1zZXJ2aWNlLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQoJy5hZGQtbmV3LXNlcnZpY2UtZGF0YXRhYmxlJykuRGF0YVRhYmxlKHtcblx0XHRcdCAgICAgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxuXHRcdFx0ICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHRcdCAgICAgICAgICAgIGRvbTogJzxcInRvcFwibGY+cnQ8XCJib3R0b21cImlwPjxcImNsZWFyXCI+J1xuXHRcdFx0ICAgICAgICB9KTtcblx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHR9LFxuXG5cdFx0XHR0b2dnbGVBbGxDaGVja2JveChlKSB7XG5cdFx0XHRcdGlmKGUudGFyZ2V0LmNoZWNrZWQgPT0gdHJ1ZSkge1xuXHRcdFx0XHRcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cyA9IFtdO1xuXHRcdFx0XHRcdHRoaXMuY2xpZW50cy5mb3JFYWNoKChjLCBpbmRleCkgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzLnB1c2goYy5jbGllbnQuaWQpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cyA9IFtdO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXG5cdFx0XHRjaGVja0FsbChlKSB7XG5cdFx0XHRcdGxldCBjaGVja2JveCA9IGUudGFyZ2V0LmNsYXNzTmFtZTtcblxuXHRcdFx0XHRpZihjaGVja2JveCA9PSAnY2hlY2tib3gtYWxsJykge1xuXHRcdFx0XHRcdGlmKCQoJy4nICsgY2hlY2tib3gpLmlzKCc6Y2hlY2tlZCcpKVxuXHRcdFx0XHRcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3Mgb3B0aW9uJykucHJvcCgnc2VsZWN0ZWQnLCB0cnVlKTtcblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIGZhbHNlKTtcblxuXHRcdFx0XHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzIG9wdGlvbicpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0bGV0IGNob3NlbiA9IGUudGFyZ2V0LmRhdGFzZXQuY2hvc2VuQ2xhc3M7XG5cblx0XHRcdFx0XHRpZigkKCcuJyArIGNoZWNrYm94KS5pcygnOmNoZWNrZWQnKSlcblx0XHRcdFx0XHRcdCQoJy4nICsgY2hvc2VuICsgJyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIHRydWUpO1xuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdCQoJy4nICsgY2hvc2VuICsgJyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIGZhbHNlKTtcblxuXHRcdFx0XHRcdCQoJy4nICsgY2hvc2VuICsgJyBvcHRpb24nKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlZCgpIHtcbiAgICAgICAgXHR0aGlzLmZldGNoU2VydmljZUxpc3QoKTtcbiAgICAgICAgXHR0aGlzLmZldGNoR3JvdXBQYWNrYWdlc1NlcnZpY2VzTGlzdCgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIG1vdW50ZWQoKSB7XG4gICAgICAgIFx0dGhpcy5pbml0Q2hvc2VuU2VsZWN0KCk7XG4gICAgICAgIH1cbiAgICB9XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cblx0LmNoZWNrYm94LWlubGluZSB7XG5cdFx0bWFyZ2luLXRvcDogLTdweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQWRkTmV3U2VydmljZS52dWU/YTk0NTE4OTgiLCI8dGVtcGxhdGU+XG5cdFxuXHQ8ZGl2PlxuXHRcdFxuXHRcdDxkaXYgdi1zaG93PVwic2NyZWVuID09IDFcIj5cblx0XHRcdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCI+XG5cdFx0XHRcdDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLWJvcmRlcmVkIHRhYmxlLXN0cmlwZWQgdGFibGUtaG92ZXIgbWVtYmVyc0RhdGF0YWJsZVwiPlxuXHRcdFx0ICAgICAgICA8dGhlYWQ+XG5cdFx0XHQgICAgICAgICAgICA8dHI+XG5cdFx0XHQgICAgICAgICAgICBcdDx0aCBzdHlsZT1cIndpZHRoOjQwJTtcIj5DTElFTlQgSUQ8L3RoPlxuXHRcdFx0ICAgICAgICAgICAgICAgIDx0aCBzdHlsZT1cIndpZHRoOjQwJTtcIj5OQU1FPC90aD5cblx0XHRcdCAgICAgICAgICAgICAgICA8dGggc3R5bGU9XCJ3aWR0aDoyMCU7XCI+QUNUSU9OPC90aD5cblx0XHRcdCAgICAgICAgICAgIDwvdHI+XG5cdFx0XHQgICAgICAgIDwvdGhlYWQ+XG5cdFx0XHQgICAgICAgIDx0Ym9keT5cblx0XHRcdCAgICAgICAgXHQ8dHIgdi1mb3I9XCJtZW1iZXIgaW4gbWVtYmVyc1wiPlxuXHRcdFx0ICAgICAgICBcdFx0PHRkPnt7IG1lbWJlci5jbGllbnQuaWQgfX08L3RkPlxuXHRcdFx0ICAgICAgICBcdFx0PHRkPnt7IG1lbWJlci5jbGllbnQuZmlyc3RfbmFtZSArICcgJyArIG1lbWJlci5jbGllbnQubGFzdF9uYW1lIH19PC90ZD5cblx0XHRcdCAgICAgICAgXHRcdDx0ZCBjbGFzcz1cInRleHQtY2VudGVyXCI+XG5cdFx0XHQgICAgICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBuYW1lPVwiY2xpZW50c0lkW11cIiA6dmFsdWU9XCJtZW1iZXIuY2xpZW50LmlkXCI+XG5cdFx0XHQgICAgICAgIFx0XHQ8L3RkPlxuXHRcdFx0ICAgICAgICBcdDwvdHI+XG5cdFx0XHQgICAgICAgIDwvdGJvZHk+XG5cdFx0XHQgICAgPC90YWJsZT5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJzcGFjZXItMTBcIj48L2Rpdj5cblxuXHRcdFx0ICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIiBAY2xpY2s9XCJnb3RvU2NyZWVuKDIpXCI+Q29udGludWU8L2J1dHRvbj5cblx0XHRcdDwvZm9ybT5cblx0XHQ8L2Rpdj4gPCEtLSBzY3JlZW4gMSAtLT5cblxuXHRcdDxkaXYgdi1zaG93PVwic2NyZWVuID09IDJcIj5cblxuXHRcdFx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIj5cblxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwic2Nyb2xsYWJsZVwiPlxuXHRcdFx0XHRcdDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLWJvcmRlcmVkIHRhYmxlLXN0cmlwZWQgdGFibGUtaG92ZXJcIj5cblx0XHRcdFx0ICAgICAgICA8dGhlYWQ+XG5cdFx0XHRcdCAgICAgICAgICAgIDx0cj5cblx0XHRcdFx0ICAgICAgICAgICAgXHQ8dGggc3R5bGU9XCJ3aWR0aDo0NSU7XCI+Q0xJRU5UIElEPC90aD5cblx0XHRcdFx0ICAgICAgICAgICAgICAgIDx0aCBzdHlsZT1cIndpZHRoOjQ1JTtcIj5OQU1FPC90aD5cblx0XHRcdFx0ICAgICAgICAgICAgICAgIDx0aCBzdHlsZT1cIndpZHRoOjEwJTtcIiBjbGFzcz1cInRleHQtY2VudGVyXCI+QUNUSU9OPC90aD5cblx0XHRcdFx0ICAgICAgICAgICAgPC90cj5cblx0XHRcdFx0ICAgICAgICA8L3RoZWFkPlxuXHRcdFx0XHQgICAgICAgIDx0Ym9keSB2LWZvcj1cIihtZW1iZXIsIGluZGV4KSBpbiBtZW1iZXJzXCIgdi1pZj1cInNlbGVjdGVkQ2xpZW50c0lkLmluZGV4T2YobWVtYmVyLmNsaWVudC5pZCkgIT0gLTFcIj5cblx0XHRcdFx0ICAgICAgICBcdFxuXHRcdFx0XHQgICAgICAgIFx0PHRyIEBjbGljaz1cInRvZ2dsZUNoaWxkUm93KCRldmVudCwgaW5kZXgpXCI+XG5cdFx0XHRcdFx0XHQgICAgICAgIDx0ZCBzdHlsZT1cIndpZHRoOjQ1JTtcIj57eyBtZW1iZXIuY2xpZW50LmlkIH19PC90ZD5cblx0XHRcdFx0XHRcdCAgICAgICAgPHRkIHN0eWxlPVwid2lkdGg6NDUlO1wiPnt7IG1lbWJlci5jbGllbnQuZmlyc3RfbmFtZSArICcgJyArIG1lbWJlci5jbGllbnQubGFzdF9uYW1lIH19PC90ZD5cblx0XHRcdFx0XHRcdCAgICAgICAgPHRkIHN0eWxlPVwid2lkdGg6MTAlO1wiIGNsYXNzPVwidGV4dC1jZW50ZXJcIj5cblx0XHRcdFx0XHRcdCAgICAgICAgICAgXHQ8aSBpZD1cImZhLTEwMDBcIiBjbGFzcz1cImZhIGZhLWFycm93LXJpZ2h0IGN1cnNvclwiIDpjbGFzcz1cIidhcnJvdy0nICsgaW5kZXhcIj48L2k+XG5cdFx0XHRcdFx0XHQgICAgICAgIDwvdGQ+XG5cdFx0XHRcdFx0XHQgICAgPC90cj5cblxuXHRcdFx0XHRcdFx0ICAgIDx0ciA6aWQ9XCInY2hpbGQtcm93LScraW5kZXhcIiBjbGFzcz1cImhpZGVcIj5cblx0XHRcdFx0XHRcdCAgICBcdDx0ZCBjb2xzcGFuPVwiM1wiPlxuXHRcdFx0XHRcdFx0ICAgIFx0XHQ8dGFibGUgY2xhc3M9XCJ0YWJsZSB0YWJsZS1ib3JkZXJlZCB0YWJsZS1zdHJpcGVkIHRhYmxlLWhvdmVyXCI+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICA8dGhlYWQ+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgPHRyIGNsYXNzPVwiY2hpbGQtcm93LWhlYWRlclwiPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgICAgIFx0PHRoPkRhdGU8L3RoPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICA8dGg+UGFja2FnZTwvdGg+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgIDx0aD5EZXRhaWxzPC90aD5cblx0XHRcdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgPHRoPkNvc3Q8L3RoPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICA8dGg+Q2hhcmdlPC90aD5cblx0XHRcdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgPHRoPlRpcDwvdGg+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgIDx0aD5EaXNjb3VudDwvdGg+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgIDx0aD5TdGF0dXM8L3RoPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICA8dGggY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICBcdFxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICA8L3RoPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgICAgIDwvdHI+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICA8L3RoZWFkPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgPHRib2R5PlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgXHQ8dHIgdi1mb3I9XCJzZXJ2aWNlIGluIG1lbWJlci5zZXJ2aWNlc1wiIHYtaWY9XCJzZXJ2aWNlLmdyb3VwX2lkID09IGdyb3VwaWQgJiYgc2VydmljZS5hY3RpdmUgPT0gMSAmJiBzZXJ2aWNlLnN0YXR1cyAhPSAnQ29tcGxldGUnXCI+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICBcdFx0PHRkPnt7IHNlcnZpY2Uuc2VydmljZV9kYXRlIH19PC90ZD5cblx0XHRcdFx0XHRcdFx0XHQgICAgICAgIFx0XHQ8dGQ+e3sgc2VydmljZS50cmFja2luZyB9fTwvdGQ+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICBcdFx0PHRkPnt7IHNlcnZpY2UuZGV0YWlsIH19PC90ZD5cblx0XHRcdFx0XHRcdFx0XHQgICAgICAgIFx0XHQ8dGQ+e3sgc2VydmljZS5jb3N0IHwgY3VycmVuY3kgfX08L3RkPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgXHRcdDx0ZD57eyBzZXJ2aWNlLmNoYXJnZSB8IGN1cnJlbmN5IH19PC90ZD5cblx0XHRcdFx0XHRcdFx0XHQgICAgICAgIFx0XHQ8dGQ+e3sgc2VydmljZS50aXAgfCBjdXJyZW5jeSB9fTwvdGQ+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICBcdFx0PHRkPnt7IHNlcnZpY2UuZGlzY291bnRfYW1vdW50IHwgY3VycmVuY3kgfX08L3RkPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgXHRcdDx0ZD57eyBzZXJ2aWNlLnN0YXR1cyB9fTwvdGQ+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICBcdFx0PHRkIGNsYXNzPVwidGV4dC1jZW50ZXJcIj5cblx0XHRcdFx0XHRcdFx0XHQgICAgICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBuYW1lPVwiY2xpZW50U2VydmljZXNJZFtdXCIgOnZhbHVlPVwic2VydmljZS5pZFwiPlxuXHRcdFx0XHRcdFx0XHRcdCAgICAgICAgXHRcdDwvdGQ+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICBcdDwvdHI+XG5cdFx0XHRcdFx0XHRcdFx0ICAgICAgICA8L3Rib2R5PlxuXHRcdFx0XHRcdFx0XHRcdCAgICA8L3RhYmxlPlxuXHRcdFx0XHRcdFx0ICAgIFx0PC90ZD5cblx0XHRcdFx0XHRcdCAgICA8L3RyPlxuXG5cdFx0XHRcdCAgICAgICAgPC90Ym9keT5cblx0XHRcdFx0ICAgIDwvdGFibGU+XG5cdFx0XHQgICAgPC9kaXY+XG5cblx0XHRcdFx0PGRpdiBjbGFzcz1cInNwYWNlci0xMFwiPjwvZGl2PlxuXG5cdFx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIiBAY2xpY2s9XCJzZXRTZWxlY3RlZENsaWVudFNlcnZpY2VzQW5kVHJhY2tpbmdzSWRcIj5Db250aW51ZTwvYnV0dG9uPlxuXHRcdFx0XHQ8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIEBjbGljaz1cImdvdG9TY3JlZW4oMSlcIj5CYWNrPC9idXR0b24+XG5cdFx0XHQ8L2Zvcm0+XG5cdFx0PC9kaXY+IDwhLS0gc2NyZWVuIDIgLS0+XG5cblx0PC9kaXY+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5cdGV4cG9ydCBkZWZhdWx0IHtcblxuXHRcdHByb3BzOiBbJ21lbWJlcnMnLCAnZ3JvdXBpZCddLFxuXG4gICAgICAgIGRhdGEoKSB7XG5cbiAgICAgICAgXHRyZXR1cm4ge1xuICAgICAgICBcdFx0bWVtYmVyc0RhdGF0YWJsZTogbnVsbCxcblxuICAgICAgICBcdFx0c2NyZWVuOiAxLFxuXG4gICAgICAgIFx0XHRzZWxlY3RlZENsaWVudHNJZDogW10sXG4gICAgICAgIFx0XHRzZWxlY3RlZENsaWVudFNlcnZpY2VzSWQ6IFtdLFxuICAgICAgICBcdFx0c2VsZWN0ZWRUcmFja2luZ3M6IFtdXG4gICAgICAgIFx0fVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgbWV0aG9kczoge1xuXG4gICAgICAgIFx0Z290b1NjcmVlbihzY3JlZW4pIHtcbiAgICAgICAgXHRcdHRoaXMuc2V0U2VsZWN0ZWRDbGllbnRzSWQoKTtcblxuICAgICAgICBcdFx0aWYoc2NyZWVuID09IDIgJiYgdGhpcy5zZWxlY3RlZENsaWVudHNJZC5sZW5ndGggPT0gMCkge1xuICAgICAgICBcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY2xpZW50L3MuJyk7XG4gICAgICAgIFx0XHR9IGVsc2Uge1xuICAgICAgICBcdFx0XHR0aGlzLnNjcmVlbiA9IHNjcmVlbjtcbiAgICAgICAgXHRcdH1cbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0c2V0U2VsZWN0ZWRDbGllbnRzSWQoKSB7XG4gICAgICAgIFx0XHRsZXQgc2VsZWN0ZWRDbGllbnRzSWQgPSBbXTtcblxuXHRcdFx0ICAgIHRoaXMubWVtYmVyc0RhdGF0YWJsZS4kKFwiaW5wdXRbbmFtZT0nY2xpZW50c0lkW10nXTpjaGVja2VkXCIpLmVhY2goZnVuY3Rpb24oZSkge1xuXHRcdFx0ICAgIFx0c2VsZWN0ZWRDbGllbnRzSWQucHVzaChwYXJzZUludCgkKHRoaXMpLnZhbCgpKSk7XG5cdFx0XHQgICAgfSk7XG5cblx0XHRcdCAgICB0aGlzLnNlbGVjdGVkQ2xpZW50c0lkID0gc2VsZWN0ZWRDbGllbnRzSWQ7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdHNldFNlbGVjdGVkQ2xpZW50U2VydmljZXNBbmRUcmFja2luZ3NJZCgpIHtcbiAgICAgICAgXHRcdGxldCBzZWxlY3RlZENsaWVudFNlcnZpY2VzSWQgPSBbXTtcblxuXHRcdFx0ICAgICQoXCJpbnB1dFtuYW1lPSdjbGllbnRTZXJ2aWNlc0lkW10nXTpjaGVja2VkXCIpLmVhY2goZnVuY3Rpb24oZSkge1xuXHRcdFx0ICAgIFx0c2VsZWN0ZWRDbGllbnRTZXJ2aWNlc0lkLnB1c2gocGFyc2VJbnQoJCh0aGlzKS52YWwoKSkpO1xuXHRcdFx0ICAgIH0pO1xuXG5cdFx0XHQgICAgaWYoc2VsZWN0ZWRDbGllbnRTZXJ2aWNlc0lkLmxlbmd0aCA9PSAwKSB7XG5cdFx0XHQgICAgXHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3Qgc2VydmljZXMvcy4nKTtcblx0XHRcdCAgICB9IGVsc2Uge1xuXHRcdFx0ICAgIFx0YXhpb3NBUEl2MS5nZXQoJy92aXNhL2dyb3VwLycgKyB0aGlzLmdyb3VwaWQgKyAnL2NsaWVudC1zZXJ2aWNlcycsIHtcblx0XHRcdFx0XHQgICAgcGFyYW1zOiB7XG5cdFx0XHRcdFx0ICAgICAgXHRjbGllbnRTZXJ2aWNlc0lkOiBzZWxlY3RlZENsaWVudFNlcnZpY2VzSWRcblx0XHRcdFx0XHQgICAgfVxuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5zZWxlY3RlZENsaWVudFNlcnZpY2VzSWQgPSByZXNwb25zZS5kYXRhO1xuXG5cdFx0XHRcdFx0XHRsZXQgc2VsZWN0ZWRUcmFja2luZ3MgPSBbXTtcblxuXHRcdCAgICAgICAgXHRcdHRoaXMuc2VsZWN0ZWRDbGllbnRTZXJ2aWNlc0lkLmZvckVhY2goY2xpZW50U2VydmljZSA9PiB7XG5cdFx0ICAgICAgICBcdFx0XHRzZWxlY3RlZFRyYWNraW5ncy5wdXNoKGNsaWVudFNlcnZpY2UudHJhY2tpbmcpO1xuXHRcdCAgICAgICAgXHRcdH0pO1xuXG5cdFx0ICAgICAgICBcdFx0dGhpcy5zZWxlY3RlZFRyYWNraW5ncyA9IHNlbGVjdGVkVHJhY2tpbmdzO1xuXG5cdFx0ICAgICAgICBcdFx0dGhpcy5zaG93TXVsdGlwbGVRdWlja1JlcG9ydE1vZGFsKCk7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHRcdCAgICB9XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdHNob3dNdWx0aXBsZVF1aWNrUmVwb3J0TW9kYWwoKSB7XG4gICAgICAgIFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC5xdWlja1JlcG9ydENsaWVudHNJZCA9IHRoaXMuc2VsZWN0ZWRDbGllbnRzSWQ7XG5cdFx0XHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LnF1aWNrUmVwb3J0Q2xpZW50U2VydmljZXNJZCA9IHRoaXMuc2VsZWN0ZWRDbGllbnRTZXJ2aWNlc0lkO1xuXHRcdFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC5xdWlja1JlcG9ydFRyYWNraW5ncyA9IHRoaXMuc2VsZWN0ZWRUcmFja2luZ3M7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmZldGNoRG9jcyh0aGlzLnNlbGVjdGVkQ2xpZW50U2VydmljZXNJZCk7XG5cblx0XHRcdFx0JCgnI2NsaWVudC1zZXJ2aWNlcy1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG5cblx0XHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XG5cdFx0XHRcdFx0JCgnI2FkZC1xdWljay1yZXBvcnQtbW9kYWwnKS5tb2RhbCgnc2hvdycpO1xuXHRcdFx0XHR9LCAxMDAwKTtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0dW5jaGVja1NlbGVjdGVkQ2xpZW50c0lkKCkge1xuICAgICAgICBcdFx0dGhpcy5tZW1iZXJzRGF0YXRhYmxlLiQoXCJpbnB1dFtuYW1lPSdjbGllbnRzSWRbXSddXCIpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdHVuY2hlY2tTZWxlY3RlZENsaWVudFNlcnZpY2VzSWQoKSB7XG4gICAgICAgIFx0XHR0aGlzLm1lbWJlcnNEYXRhdGFibGUuJChcImlucHV0W25hbWU9J2NsaWVudFNlcnZpY2VzSWRbXSddXCIpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGNyZWF0ZURhdGF0YWJsZShjbGFzc05hbWUpIHtcbiAgICAgICAgXHRcdHRoaXMubWVtYmVyc0RhdGF0YWJsZSA9ICQoJ3RhYmxlLicrY2xhc3NOYW1lKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHR0aGlzLm1lbWJlcnNEYXRhdGFibGUgPSAkKCd0YWJsZS4nK2NsYXNzTmFtZSkuRGF0YVRhYmxlKHtcblx0XHRcdFx0ICAgICAgICBwYWdlTGVuZ3RoOiAxMCxcblx0XHRcdFx0ICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxuXHRcdFx0XHQgICAgICAgIFwiYWFTb3J0aW5nXCI6IFtdLFxuXHRcdFx0XHQgICAgICAgIGRvbTogJzxcInRvcFwibGY+cnQ8XCJib3R0b21cImlwPjxcImNsZWFyXCI+J1xuXHRcdFx0XHQgICAgfSk7XG5cdFx0XHRcdH0uYmluZCh0aGlzKSwgMTAwMCk7XG5cdFx0XHR9LFxuXG5cdFx0XHR0b2dnbGVDaGlsZFJvdyhldmVudCwgaW5kZXgpIHtcblx0XHRcdFx0JCgnI2NoaWxkLXJvdy0nK2luZGV4KS50b2dnbGVDbGFzcygnaGlkZScpO1xuXHRcdFx0XHQkKCcuYXJyb3ctJytpbmRleCkudG9nZ2xlQ2xhc3MoJ2ZhLWFycm93LXJpZ2h0IGZhLWFycm93LWRvd24nKTtcblx0XHRcdH1cblxuICAgICAgICB9LFxuXG4gICAgICAgIG1vdW50ZWQoKSB7XG5cbiAgICAgICAgXHQkKCcjY2xpZW50LXNlcnZpY2VzLW1vZGFsJykub24oJ2hpZGRlbi5icy5tb2RhbCcsIGZ1bmN0aW9uIChlKSB7XG5cdFx0XHRcdFxuXHRcdFx0XHR0aGlzLnNjcmVlbiA9IDE7XG5cdFx0XHRcdFxuXHRcdFx0XHR0aGlzLnNlbGVjdGVkQ2xpZW50c0lkID0gW107XG5cdFx0XHRcdHRoaXMuc2VsZWN0ZWRDbGllbnRTZXJ2aWNlc0lkID0gW107XG5cdFx0XHRcdHRoaXMuc2VsZWN0ZWRUcmFja2luZ3MgPSBbXTtcblxuXHRcdFx0XHR0aGlzLnVuY2hlY2tTZWxlY3RlZENsaWVudHNJZCgpO1xuXHRcdFx0XHR0aGlzLnVuY2hlY2tTZWxlY3RlZENsaWVudFNlcnZpY2VzSWQoKTtcblxuXHRcdFx0fS5iaW5kKHRoaXMpKTtcblxuICAgICAgICB9XG5cblx0fVxuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG5cdGZvcm0ge1xuXHRcdG1hcmdpbi1ib3R0b206IDMwcHg7XG5cdH1cblx0Lm0tci0xMCB7XG5cdFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xuXHR9XG5cdC5zcGFjZXItMTAge1xuXHRcdGNsZWFyOiBib3RoO1xuXHRcdGhlaWdodDogMTBweDtcblx0fVxuXHQuY3Vyc29yIHtcblx0XHRjdXJzb3I6IHBvaW50ZXI7XG5cdH1cblx0LnNjcm9sbGFibGUge1xuXHRcdG1heC1oZWlnaHQ6IDQ1MHB4O1xuXHRcdG92ZXJmbG93LXk6IGF1dG87XG5cdH1cblx0LmNoaWxkLXJvdy1oZWFkZXIge1xuXHRcdGNvbG9yOiAjM2M4ZGJjO1xuXHR9XG48L3N0eWxlPlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBDbGllbnRTZXJ2aWNlcy52dWU/NzRmNGY3N2IiLCI8dGVtcGxhdGU+XG5cblx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIiBAc3VibWl0LnByZXZlbnQ9XCJkZWxldGVNZW1iZXJcIj5cblxuXHRcdDxwPkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhpcyBtZW1iZXI/PC9wPlxuXG5cdFx0PGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPkRlbGV0ZTwvYnV0dG9uPlxuXHQgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG5cblx0PC9mb3JtPlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG4gICAgZXhwb3J0IGRlZmF1bHQge1xuXG4gICAgXHRwcm9wczogWydncm91cGlkJ10sXG5cbiAgICBcdGRhdGEoKSB7XG4gICAgXHRcdHJldHVybiB7XG4gICAgXHRcdFx0ZGVsZXRlTWVtYmVyRm9ybTogbmV3IEZvcm0oe1xuXHRcdFx0XHRcdG1lbWJlcklkOiBudWxsXG5cdFx0XHRcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXG4gICAgXHRcdH1cbiAgICBcdH0sXG5cbiAgICBcdG1ldGhvZHM6IHtcblxuICAgIFx0XHRzZXRNZW1iZXJJZChpZCkge1xuICAgIFx0XHRcdHRoaXMuZGVsZXRlTWVtYmVyRm9ybS5tZW1iZXJJZCA9IGlkO1xuICAgIFx0XHR9LFxuXG4gICAgXHRcdHJlc2V0RGVsZXRlTWVtYmVyRm9ybSgpIHtcbiAgICBcdFx0XHR0aGlzLmRlbGV0ZU1lbWJlckZvcm0ubWVtYmVySWQgPSBudWxsO1xuICAgIFx0XHR9LFxuXG4gICAgXHRcdGRlbGV0ZU1lbWJlcigpIHtcbiAgICBcdFx0XHR0aGlzLmRlbGV0ZU1lbWJlckZvcm0uc3VibWl0KCdwb3N0JywgJy92aXNhL2dyb3VwLycgKyB0aGlzLmdyb3VwaWQgKyAnL2RlbGV0ZS1tZW1iZXInKVxuXHRcdCAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdCAgICAgICAgXHR0aGlzLnJlc2V0RGVsZXRlTWVtYmVyRm9ybSgpO1xuXG5cdFx0ICAgICAgICAgICAgJCgnI2RlbGV0ZS1tZW1iZXItbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXG5cdFx0ICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdCAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuJHBhcmVudC5mZXRjaE1lbWJlcnMoKTtcblx0XHQgICAgICAgICAgICAgICAgXG5cdFx0ICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xuXHRcdCAgICAgICAgICAgIH0gZWxzZSB7XG5cdFx0ICAgICAgICAgICAgXHR0b2FzdHIuZXJyb3IocmVzcG9uc2UubWVzc2FnZSk7XG5cdFx0ICAgICAgICAgICAgfVxuXHRcdCAgICAgICAgfSlcblx0XHQgICAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgIFx0XHR9XG5cbiAgICBcdH1cblxuICAgIH1cblxuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG5cdGZvcm0ge1xuXHRcdG1hcmdpbi1ib3R0b206IDMwcHg7XG5cdH1cblx0Lm0tci0xMCB7XG5cdFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xuXHR9XG48L3N0eWxlPlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBEZWxldGVNZW1iZXIudnVlPzcxYWQ1M2I4IiwiPHRlbXBsYXRlPlxuICA8ZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzPVwicm93IHRpbWVsaW5lLW1vdmVtZW50IHRpbWVsaW5lLW1vdmVtZW50LXRvcCBuby1kYXNoZWQgbm8tc2lkZS1tYXJnaW5cIiB2LWlmPVwibG9nMi55ZWFyICE9IG51bGxcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aW1lbGluZS1iYWRnZSB0aW1lbGluZS1maWx0ZXItbW92ZW1lbnRcIj5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj57e2xvZzIueWVhcn19PC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzcz1cInJvdyB0aW1lbGluZS1tb3ZlbWVudCAgbm8tc2lkZS1tYXJnaW5cIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aW1lbGluZS1iYWRnZVwiIHYtaWY9XCJsb2cyLm1vbnRoICE9IG51bGxcIj5cbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cInRpbWVsaW5lLWJhbGxvb24tZGF0ZS1kYXlcIj57e2xvZzIuZGF5fX08L3NwYW4+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0aW1lbGluZS1iYWxsb29uLWRhdGUtbW9udGhcIj57e2xvZzIubW9udGh9fTwvc3Bhbj5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMiAgdGltZWxpbmUtaXRlbVwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTIgY29sLXNtLW9mZnNldC0xXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpbWVsaW5lLXBhbmVsIGRlYml0c1wiID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwidGltZWxpbmUtcGFuZWwtdWxcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48c3BhbiBjbGFzcz1cImNhdXNhbGVcIj5CYWxhbmNlOiA8Yj57e2xvZzIuZGF0YS5iYWxhbmNlIHwgY3VycmVuY3l9fTwvYj48L3NwYW4+IDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PHA+PHNtYWxsIGNsYXNzPVwidGV4dC1tdXRlZFwiPkFtb3VudCA6IDxiPnt7bG9nMi5kYXRhLmFtb3VudCAgfCBjdXJyZW5jeX19PC9iPjwvc21hbGw+PC9wPiA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxwPjxzbWFsbCBjbGFzcz1cInRleHQtbXV0ZWRcIj5UeXBlIDogPGI+e3tsb2cyLmRhdGEudHlwZSB9fTwvYj48L3NtYWxsPjwvcD4gPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tOVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aW1lbGluZS1wYW5lbCBkZWJpdHNcIiA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInRpbWVsaW5lLXBhbmVsLXVsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PHNwYW4gY2xhc3M9XCJpbXBvcnRvXCIgdi1odG1sPVwidGhpcy5kZXRhaWxcIj48L3NwYW4+PC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48c3BhbiBjbGFzcz1cImNhdXNhbGVcIj57e2xvZzIuZGF0YS5wcm9jZXNzb3J9fTwvc3Bhbj4gPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48cD48c21hbGwgY2xhc3M9XCJ0ZXh0LW11dGVkXCI+PGkgY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLXRpbWVcIj48L2k+e3tsb2cyLmRhdGEuZGF0ZX19PC9zbWFsbD48L3A+IDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8L2Rpdj5cbiAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICBwcm9wczogWydsb2cyJ10sXG4gIG1ldGhvZHM6e1xuICAgIHNlbGVjdChpZCl7XG4gICAgICAgdGhpcy4kcGFyZW50LnNlbGVjdEl0ZW0oaWQpXG4gICAgfSxcbiAgfSxcbiAgY29tcHV0ZWQ6e1xuICAgIGRldGFpbCgpe1xuICAgICAgdmFyIHJlcyA9IHRoaXMubG9nMi5kYXRhLnRpdGxlO1xuICAgICAgcmV0dXJuIHJlcy5yZXBsYWNlKFwiPC9icj5cIiwgXCIgXCIpO1xuICAgIH1cblxuICB9LFxufVxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFRyYW5zYWN0aW9uTG9ncy52dWU/MmQ2OTAwN2YiLCI8dGVtcGxhdGU+XG5cblx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIiBAc3VibWl0LnByZXZlbnQ9XCJlZGl0QWRkcmVzc1wiPlxuXG5cdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiBlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnYWRkcmVzcycpIH1cIj5cblx0ICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb250cm9sLWxhYmVsXCI+XG5cdCAgICAgICAgICAgIE5hbWVcblx0ICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdCAgICAgICAgPC9sYWJlbD5cblxuXHQgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMTBcIj5cblx0ICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgbS10b3AtMFwiIG5hbWU9XCJuYW1lXCIgdi1tb2RlbD1cImVkaXRBZGRyZXNzRm9ybS5uYW1lXCI+XG5cdCAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnbmFtZScpXCIgdi10ZXh0PVwiZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5nZXQoJ25hbWUnKVwiPjwvc3Bhbj5cblx0ICAgICAgICA8L2Rpdj5cblx0ICAgIDwvZGl2PlxuXG5cdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiBlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnYWRkcmVzcycpIH1cIj5cblx0ICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb250cm9sLWxhYmVsXCI+XG5cdCAgICAgICAgICAgIE51bWJlclxuXHQgICAgICAgICAgICA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0ICAgICAgICA8L2xhYmVsPlxuXG5cdCAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMFwiPlxuXHQgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBtLXRvcC0wXCIgbmFtZT1cIm51bWJlclwiIHYtbW9kZWw9XCJlZGl0QWRkcmVzc0Zvcm0ubnVtYmVyXCI+XG5cdCAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnbnVtYmVyJylcIiB2LXRleHQ9XCJlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmdldCgnbnVtYmVyJylcIj48L3NwYW4+XG5cdCAgICAgICAgPC9kaXY+XG5cdCAgICA8L2Rpdj5cblxuXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgOmNsYXNzPVwieyAnaGFzLWVycm9yJzogZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5oYXMoJ2FkZHJlc3MnKSB9XCI+XG5cdCAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiPlxuXHQgICAgICAgICAgICBBZGRyZXNzXG5cdCAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHQgICAgICAgIDwvbGFiZWw+XG5cblx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTEwXCI+XG5cdCAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIG0tdG9wLTBcIiBuYW1lPVwiYWRkcmVzc1wiIHYtbW9kZWw9XCJlZGl0QWRkcmVzc0Zvcm0uYWRkcmVzc1wiPlxuXHQgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5oYXMoJ2FkZHJlc3MnKVwiIHYtdGV4dD1cImVkaXRBZGRyZXNzRm9ybS5lcnJvcnMuZ2V0KCdhZGRyZXNzJylcIj48L3NwYW4+XG5cdCAgICAgICAgPC9kaXY+XG5cdCAgICA8L2Rpdj5cblxuXHRcdDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIj5FZGl0PC9idXR0b24+XG5cdCAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cblxuXHQ8L2Zvcm0+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5cbiAgICBleHBvcnQgZGVmYXVsdCB7XG5cbiAgICBcdHByb3BzOiBbJ2dyb3VwaWQnXSxcblxuICAgIFx0ZGF0YSgpIHtcbiAgICBcdFx0cmV0dXJuIHtcbiAgICBcdFx0XHRlZGl0QWRkcmVzc0Zvcm06IG5ldyBGb3JtKHtcblx0XHRcdFx0XHRhZGRyZXNzOiAnJyxcblx0XHRcdFx0XHRudW1iZXI6ICcnLFxuXHRcdFx0XHRcdG5hbWU6ICcnLFxuXHRcdFx0XHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxuICAgIFx0XHR9XG4gICAgXHR9LFxuXG4gICAgXHRtZXRob2RzOiB7XG5cbiAgICBcdFx0c2V0R3JvdXBBZGRyZXNzKCkge1xuICAgIFx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkKVxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XHRcdFx0XHRcblx0XHRcdFx0XHR0aGlzLmVkaXRBZGRyZXNzRm9ybS5hZGRyZXNzID0gcmVzcG9uc2UuZGF0YS5hZGRyZXNzO1xuXHRcdFx0XHRcdHRoaXMuZWRpdEFkZHJlc3NGb3JtLm51bWJlciA9IHJlc3BvbnNlLmRhdGEudXNlci5hZGRyZXNzLmNvbnRhY3RfbnVtYmVyO1xuXHRcdFx0XHRcdHRoaXMuZWRpdEFkZHJlc3NGb3JtLm5hbWUgPSByZXNwb25zZS5kYXRhLm5hbWU7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgIFx0XHR9LFxuXG4gICAgXHRcdGVkaXRBZGRyZXNzKCkge1xuICAgIFx0XHRcdHRoaXMuZWRpdEFkZHJlc3NGb3JtLnN1Ym1pdCgncGF0Y2gnLCAnL3Zpc2EvZ3JvdXAvJyArIHRoaXMuZ3JvdXBpZCArICcvZWRpdC1hZGRyZXNzJylcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcblx0XHRcdFx0XHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LmZldGNoRGV0YWlscygpO1xuXG5cdFx0XHRcdFx0XHQkKCcjZWRpdC1hZGRyZXNzLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcbiAgICBcdFx0fVxuXG4gICAgXHR9XG5cbiAgICB9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRWRpdEFkZHJlc3MudnVlP2Q3MDQxMGJlIiwiPHRlbXBsYXRlPlxuXG5cdDxkaXY+XG5cdFx0PGRpdiB2LXNob3c9J3NjcmVlbiA9PSAxJz5cblx0XHRcdDxkaXYgY2xhc3M9XCJ0YWJsZS1yZXNwb25zaXZlXCI+XG5cdCAgICAgICAgICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVyZWQgdGFibGUtaG92ZXIgY2xpZW50cy1kYXRhdGFibGVcIj5cblx0ICAgICAgICAgICAgICAgIDx0aGVhZD5cblx0XHQgICAgICAgICAgICAgICAgPHRyPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgPHRoIGNsYXNzPVwidGV4dC1jZW50ZXJcIj5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgQGNsaWNrPVwidG9nZ2xlQWxsQ2hlY2tib3hcIj5cblx0XHQgICAgICAgICAgICAgICAgICAgIDwvdGg+XG5cdFx0ICAgICAgICAgICAgICAgICAgICA8dGg+TmFtZTwvdGg+XG5cdFx0ICAgICAgICAgICAgICAgICAgICA8dGg+Q2xpZW50IElEPC90aD5cblx0XHQgICAgICAgICAgICAgICAgICAgIDx0aD5QYWNrYWdlPC90aD5cblx0XHQgICAgICAgICAgICAgICAgICAgIDx0aD5EYXRlIFJlY29yZGVkPC90aD5cblx0XHQgICAgICAgICAgICAgICAgICAgIDx0aD5EZXRhaWxzPC90aD5cblx0XHQgICAgICAgICAgICAgICAgICAgIDx0aD5TdGF0dXM8L3RoPlxuXHRcdCAgICAgICAgICAgICAgICA8L3RyPlxuXHQgICAgICAgICAgICAgICAgPC90aGVhZD5cblx0ICAgICAgICAgICAgICAgIDx0Ym9keT5cblx0ICAgICAgICAgICAgICAgICAgICA8dHIgdi1mb3I9XCJjbGllbnRTZXJ2aWNlIGluIGNsaWVudFNlcnZpY2VzXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgXHQ8dGQgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPlxuXHQgICAgICAgICAgICAgICAgICAgIFx0XHQ8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgXG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdFx0OnZhbHVlPVwiY2xpZW50U2VydmljZS5pZFwiIFxuXHQgICAgICAgICAgICAgICAgICAgIFx0XHRcdHYtbW9kZWw9XCJlZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZFwiPlxuXHQgICAgICAgICAgICAgICAgICAgIFx0PC90ZD5cblx0ICAgICAgICAgICAgICAgICAgICBcdDx0ZD57eyBjbGllbnRTZXJ2aWNlLnVzZXIuZnVsbF9uYW1lIH19PC90ZD5cblx0ICAgICAgICAgICAgICAgICAgICBcdDx0ZD57eyBjbGllbnRTZXJ2aWNlLnVzZXIuaWQgfX08L3RkPlxuXHQgICAgICAgICAgICAgICAgICAgIFx0PHRkPnt7IGNsaWVudFNlcnZpY2UudHJhY2tpbmcgfX08L3RkPlxuXHQgICAgICAgICAgICAgICAgICAgIFx0PHRkPnt7IGNsaWVudFNlcnZpY2Uuc2VydmljZV9kYXRlIH19PC90ZD5cblx0ICAgICAgICAgICAgICAgICAgICBcdDx0ZD57eyBjbGllbnRTZXJ2aWNlLmRldGFpbCB9fTwvdGQ+XG5cdCAgICAgICAgICAgICAgICAgICAgXHQ8dGQ+e3sgY2xpZW50U2VydmljZS5zdGF0dXMgfX08L3RkPlxuXHQgICAgICAgICAgICAgICAgICAgIDwvdHI+XG5cdCAgICAgICAgICAgICAgICA8L3Rib2R5PlxuXHQgICAgICAgICAgICA8L3RhYmxlPlxuXHQgICAgICAgIDwvZGl2PlxuXG5cdCAgICAgICAgPGEgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiIEBjbGljaz1cImNoYW5nZVNjcmVlbigyKVwiPkNvbnRpbnVlPC9hPlxuXHQgICAgXHQ8YSBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2E+XG5cblx0ICAgIFx0PGJyPlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9J3NjcmVlbiA9PSAyJz5cblx0XHRcdDxmb3JtIEBzdWJtaXQucHJldmVudD1cImVkaXRTZXJ2aWNlXCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJyb3dcIj5cblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgOmNsYXNzPVwieyAnaGFzLWVycm9yJzogZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2FkZGl0aW9uYWxfY2hhcmdlJykgfVwiPlxuXHRcdFx0XHRcdFx0ICAgIDxsYWJlbCBmb3I9XCJhZGRpdG9uYWxfY2hhcmdlXCI+QWRkaXRpb25hbCBDaGFyZ2U8L2xhYmVsPlxuXHRcdFx0XHRcdFx0ICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJhZGRpdGlvbmFsX2NoYXJnZVwiIG5hbWU9XCJhZGRpdGlvbmFsX2NoYXJnZVwiIHYtbW9kZWw9XCJlZGl0U2VydmljZUZvcm0uYWRkaXRpb25hbF9jaGFyZ2VcIj5cblx0XHRcdFx0XHRcdCAgICA8c3BhbiBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2FkZGl0aW9uYWxfY2hhcmdlJylcIiB2LXRleHQ9XCJlZGl0U2VydmljZUZvcm0uZXJyb3JzLmdldCgnYWRkaXRpb25hbF9jaGFyZ2UnKVwiPjwvc3Bhbj5cblx0XHRcdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdjb3N0JykgfVwiPlxuXHRcdFx0XHRcdFx0ICAgIDxsYWJlbCBmb3I9XCJjb3N0XCI+Q29zdDwvbGFiZWw+XG5cdFx0XHRcdFx0XHQgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImNvc3RcIiBuYW1lPVwiY29zdFwiIHYtbW9kZWw9XCJlZGl0U2VydmljZUZvcm0uY29zdFwiPlxuXHRcdFx0XHRcdFx0ICAgIDxzcGFuIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJlZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygnY29zdCcpXCIgdi10ZXh0PVwiZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5nZXQoJ2Nvc3QnKVwiPjwvc3Bhbj5cblx0XHRcdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdkaXNjb3VudCcpIH1cIj5cblx0XHRcdFx0XHRcdCAgICA8bGFiZWwgZm9yPVwiY29zdFwiPkRpc2NvdW50PC9sYWJlbD5cblx0XHRcdFx0XHRcdCAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIGlkPVwiZGlzY291bnRcIiBuYW1lPVwiZGlzY291bnRcIiB2LW1vZGVsPVwiZWRpdFNlcnZpY2VGb3JtLmRpc2NvdW50XCI+XG5cdFx0XHRcdFx0XHQgICAgPHNwYW4gY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cImVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdkaXNjb3VudCcpXCIgdi10ZXh0PVwiZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5nZXQoJ2Rpc2NvdW50JylcIj48L3NwYW4+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHRcdFx0ICAgIDxsYWJlbCBmb3I9XCJzdGF0dXNcIj5TdGF0dXM8L2xhYmVsPlxuXHRcdFx0XHRcdFx0ICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cInN0YXR1c1wiIG5hbWU9XCJzdGF0dXNcIiB2LW1vZGVsPVwiZWRpdFNlcnZpY2VGb3JtLnN0YXR1c1wiPlxuXHRcdFx0XHRcdFx0ICAgIFx0PG9wdGlvbiB2YWx1ZT1cImNvbXBsZXRlXCI+Q29tcGxldGU8L29wdGlvbj5cblx0XHRcdFx0XHRcdCAgICBcdDxvcHRpb24gdmFsdWU9XCJvbiBwcm9jZXNzXCI+T24gUHJvY2Vzczwvb3B0aW9uPlxuXHRcdFx0XHRcdFx0ICAgIFx0PG9wdGlvbiB2YWx1ZT1cInBlbmRpbmdcIj5QZW5kaW5nPC9vcHRpb24+XG5cdFx0XHRcdFx0XHQgICAgPC9zZWxlY3Q+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0XHRcdCAgICA8bGFiZWwgZm9yPVwibm90ZVwiPk5vdGU8L2xhYmVsPlxuXHRcdFx0XHRcdFx0ICAgIDx0ZXh0YXJlYSBjbGFzcz1cImZvcm0tY29udHJvbFwiIGlkPVwibm90ZVwiIG5hbWU9XCJub3RlXCIgdi1tb2RlbD1cImVkaXRTZXJ2aWNlRm9ybS5ub3RlXCIgcGxhY2Vob2xkZXI9XCJmb3IgaW50ZXJuYWwgdXNlIG9ubHksIG9ubHkgZW1wbG95ZWVzIGNhbiBzZWUuXCI+PC90ZXh0YXJlYT5cblx0XHRcdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHN0eWxlPVwibWFyZ2luLXRvcDogMjJweDtcIj5cblx0XHRcdFx0XHRcdCAgICA8bGFiZWwgZm9yPVwiYWN0aXZlXCI+PC9sYWJlbD5cblx0XHRcdFx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJyYWRpby1pbmxpbmVcIj5cblx0XHRcdFx0XHRcdFx0ICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgaWQ9XCJhY3RpdmVcIiBuYW1lPVwiYWN0aXZlXCIgdmFsdWU9XCIxXCIgdi1tb2RlbD1cImVkaXRTZXJ2aWNlRm9ybS5hY3RpdmVcIj4gRW5hYmxlXG5cdFx0XHRcdFx0XHRcdDwvbGFiZWw+XG5cdFx0XHRcdFx0XHRcdDxsYWJlbCBjbGFzcz1cInJhZGlvLWlubGluZVwiPlxuXHRcdFx0XHRcdFx0XHQgIDxpbnB1dCB0eXBlPVwicmFkaW9cIiBpZD1cImFjdGl2ZVwiIG5hbWU9XCJhY3RpdmVcIiB2YWx1ZT1cIjBcIiB2LW1vZGVsPVwiZWRpdFNlcnZpY2VGb3JtLmFjdGl2ZVwiPiBEaXNhYmxlXG5cdFx0XHRcdFx0XHRcdDwvbGFiZWw+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiBlZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygncmVhc29uJykgfVwiIHYtaWY9XCJlZGl0U2VydmljZUZvcm0uZGlzY291bnQ+MFwiIHN0eWxlPVwibWFyZ2luLXRvcDogMjVweDtcIj5cblx0XHRcdFx0XHRcdCAgICA8bGFiZWwgZm9yPVwiY29zdFwiPlJlYXNvbjwvbGFiZWw+XG5cdFx0XHRcdFx0XHQgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cInJlYXNvblwiIG5hbWU9XCJyZWFzb25cIiB2LW1vZGVsPVwiZWRpdFNlcnZpY2VGb3JtLnJlYXNvblwiPlxuXHRcdFx0XHRcdFx0ICAgIDxzcGFuIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJlZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygncmVhc29uJylcIiB2LXRleHQ9XCJlZGl0U2VydmljZUZvcm0uZXJyb3JzLmdldCgncmVhc29uJylcIj48L3NwYW4+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0PC9kaXY+XG5cblx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPlNhdmU8L2J1dHRvbj5cblx0ICAgIFx0XHQ8YSBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIEBjbGljaz1cImNoYW5nZVNjcmVlbigxKVwiPkJhY2s8L2E+XG5cdFx0XHQ8L2Zvcm0+XG5cdFx0PC9kaXY+XG5cdDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG4gICAgZXhwb3J0IGRlZmF1bHQge1xuXG4gICAgXHRwcm9wczogWydncm91cGlkJ10sXG5cbiAgICBcdGRhdGEoKSB7XG4gICAgXHRcdHJldHVybiB7XG4gICAgXHRcdFx0c2NyZWVuOiAxLFxuXG4gICAgXHRcdFx0Y2xpZW50U2VydmljZXM6IFtdLFxuXG4gICAgXHRcdFx0ZWRpdFNlcnZpY2VGb3JtOiBuZXcgRm9ybSh7XG4gICAgXHRcdFx0XHRjbGllbnRTZXJ2aWNlc0lkOiBbXSxcbiAgICBcdFx0XHRcdHNlcnZpY2VJZDogJycsXG4gICAgXHRcdFx0XHRhZGRpdGlvbmFsX2NoYXJnZTogJycsXG4gICAgXHRcdFx0XHRjb3N0OiAnJyxcbiAgICBcdFx0XHRcdGRpc2NvdW50OiAnJyxcbiAgICBcdFx0XHRcdHJlYXNvbjogJycsXG4gICAgXHRcdFx0XHRzdGF0dXM6ICcnLFxuICAgIFx0XHRcdFx0bm90ZTogJycsXG4gICAgXHRcdFx0XHRhY3RpdmU6ICcnXG5cdFx0XHRcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXHRcbiAgICBcdFx0fVxuICAgIFx0fSxcblxuICAgIFx0bWV0aG9kczoge1xuXG4gICAgXHRcdGNoYW5nZVNjcmVlbihzY3JlZW4pIHtcbiAgICBcdFx0XHRpZihzY3JlZW4gPT0gMikge1xuXHRcdFx0XHRcdGlmKHRoaXMuZWRpdFNlcnZpY2VGb3JtLmNsaWVudFNlcnZpY2VzSWQubGVuZ3RoID09IDApIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBhdCBsZWFzdCBvbmUgY2xpZW50Jyk7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cbiAgICBcdFx0XHR0aGlzLnNjcmVlbiA9IHNjcmVlbjtcbiAgICBcdFx0fSxcblxuICAgIFx0XHRzZXRFZGl0U2VydmljZUZvcm0oY2xpZW50U2VydmljZUlkLCBzZXJ2aWNlSWQsIGFkZGl0aW9uYWxDaGFyZ2UsIGNvc3QsIGRpc2NvdW50LCByZWFzb24sIHN0YXR1cywgbm90ZSwgYWN0aXZlKSB7XG4gICAgXHRcdFx0dGhpcy5lZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZCA9IFtdO1xuICAgIFx0XHRcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLmNsaWVudFNlcnZpY2VzSWQucHVzaChjbGllbnRTZXJ2aWNlSWQpO1xuXG4gICAgXHRcdFx0dGhpcy5lZGl0U2VydmljZUZvcm0uc2VydmljZUlkID0gc2VydmljZUlkO1xuICAgIFx0XHRcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLmFkZGl0aW9uYWxfY2hhcmdlID0gYWRkaXRpb25hbENoYXJnZTtcbiAgICBcdFx0XHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5jb3N0ID0gY29zdDtcbiAgICBcdFx0XHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5kaXNjb3VudCA9IGRpc2NvdW50O1xuICAgIFx0XHRcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLnJlYXNvbiA9IHJlYXNvbjtcbiAgICBcdFx0XHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5zdGF0dXMgPSBzdGF0dXM7XG4gICAgXHRcdFx0dGhpcy5lZGl0U2VydmljZUZvcm0ubm90ZSA9ICcnO1xuICAgIFx0XHRcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLmFjdGl2ZSA9IGFjdGl2ZTtcbiAgICBcdFx0fSxcblxuICAgIFx0XHRpbml0KGNsaWVudElkLCBzZXJ2aWNlSWQsY2lkKSB7XG4gICAgXHRcdFx0dGhpcy5jaGFuZ2VTY3JlZW4oMSk7XG5cbiAgICBcdFx0XHRsZXQgX2NsaWVudElkID0gY2xpZW50SWQ7XG4gICAgXHRcdFx0bGV0IF9zZXJ2aWNlSWQgPSBzZXJ2aWNlSWQ7XG4gICAgXHRcdFx0bGV0IF9jaWQgPSBjaWQ7XG4gICAgXHRcdFx0bGV0IF9ncm91cElkID0gdGhpcy5ncm91cGlkO1xuICAgIFx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJysgX2dyb3VwSWQgKycvZWRpdC1zZXJ2aWNlcycsIHtcbiAgICBcdFx0XHRcdHBhcmFtczoge1xuICAgIFx0XHRcdFx0XHRjbGllbnRJZDogX2NsaWVudElkLFxuICAgIFx0XHRcdFx0XHRzZXJ2aWNlSWQ6IF9zZXJ2aWNlSWQsXG4gICAgXHRcdFx0XHRcdGNpZDogX2NpZFxuICAgIFx0XHRcdFx0fVxuICAgIFx0XHRcdH0pXG4gICAgXHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgIFx0XHRcdFx0dGhpcy5jbGllbnRTZXJ2aWNlcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgXHRcdFx0XHQvL2NvbnNvbGUubG9nKHJlc3BvbnNlLmRhdGFbMF0uaWQpO1xuICAgIFx0XHRcdFx0dGhpcy5jYWxsRGF0YVRhYmxlKCk7XG5cbiAgICBcdFx0XHRcdGxldCB7IHRpcCwgY29zdCwgc3RhdHVzLCByZW1hcmtzLCBhY3RpdmUgfSA9IHJlc3BvbnNlLmRhdGFbMF07XG4gICAgXHRcdFx0XHRsZXQgZGlzY291bnQgPSBudWxsO1xuICAgIFx0XHRcdFx0bGV0IHJlYXNvbiA9IG51bGw7XG5cbiAgICBcdFx0XHRcdGlmKHJlc3BvbnNlLmRhdGFbMF0uZGlzY291bnQpe1xuXHQgICAgXHRcdFx0XHRsZXQgZGlzY291bnQgPSAocmVzcG9uc2UuZGF0YVswXS5kaXNjb3VudC5sZW5ndGggPiAwKSBcblx0ICAgIFx0XHRcdFx0XHQ/IHJlc3BvbnNlLmRhdGFbMF0uZGlzY291bnRbMF0uZGlzY291bnRfYW1vdW50XG5cdCAgICBcdFx0XHRcdFx0OiAnJztcblx0ICAgIFx0XHRcdFx0bGV0IHJlYXNvbiA9IChyZXNwb25zZS5kYXRhWzBdLmRpc2NvdW50Lmxlbmd0aCA+IDApIFxuXHQgICAgXHRcdFx0XHRcdD8gcmVzcG9uc2UuZGF0YVswXS5kaXNjb3VudFswXS5yZWFzb24gXG5cdCAgICBcdFx0XHRcdFx0OiAnJztcbiAgICBcdFx0XHRcdH1cblxuICAgIFx0XHRcdFx0dGhpcy5zZXRFZGl0U2VydmljZUZvcm0oX2NpZCwgc2VydmljZUlkLCB0aXAsIGNvc3QsIGRpc2NvdW50LCByZWFzb24sIHN0YXR1cywgcmVtYXJrcywgYWN0aXZlKTtcbiAgICBcdFx0XHR9KVxuICAgIFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgIFx0XHR9LFxuXG4gICAgXHRcdGNhbGxEYXRhVGFibGUoKSB7XG5cdFx0XHRcdCQoJy5jbGllbnRzLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdCQoJy5jbGllbnRzLWRhdGF0YWJsZScpLkRhdGFUYWJsZSh7XG5cdFx0XHQgICAgICAgICAgICBwYWdlTGVuZ3RoOiAxMCxcblx0XHRcdCAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdFx0XHQgICAgICAgICAgICBkb206ICc8XCJ0b3BcImxmPnJ0PFwiYm90dG9tXCJpcD48XCJjbGVhclwiPidcblx0XHRcdCAgICAgICAgfSk7XG5cdFx0XHRcdH0sIDEwMDApO1xuXHRcdFx0fSxcblxuXHRcdFx0dG9nZ2xlQWxsQ2hlY2tib3goZSkge1xuXHRcdFx0XHRpZihlLnRhcmdldC5jaGVja2VkID09IHRydWUpIHtcblx0XHRcdFx0XHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5jbGllbnRTZXJ2aWNlc0lkID0gW107XG5cdFx0XHRcdFx0dGhpcy5jbGllbnRTZXJ2aWNlcy5mb3JFYWNoKChjLCBpbmRleCkgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5lZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZC5wdXNoKGMuaWQpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLmNsaWVudFNlcnZpY2VzSWQgPSBbXTtcblx0XHRcdFx0fVxuXHRcdFx0fSxcblxuXHRcdFx0ZWRpdFNlcnZpY2UoKSB7XG5cdFx0XHRcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLnN1Ym1pdCgncGF0Y2gnLCAnL3Zpc2EvZ3JvdXAvJyArIHRoaXMuZ3JvdXBpZCArICcvZWRpdC1zZXJ2aWNlcycpXG5cdFx0ICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0ICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdCAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuJHBhcmVudC5mZXRjaERldGFpbHMoKTtcblx0XHQgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hNZW1iZXJzKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuJHJlZnMuYWRkbmV3c2VydmljZXJlZi5mZXRjaEdyb3VwUGFja2FnZXNTZXJ2aWNlc0xpc3QoKTtcblxuXHRcdCAgICAgICAgICAgICAgICAkKCcjZWRpdC1zZXJ2aWNlLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5tZXNzYWdlKTtcblx0XHQgICAgICAgICAgICB9XG5cdFx0ICAgICAgICB9KVxuXHRcdCAgICAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG5cdFx0XHR9XG5cbiAgICBcdH1cblxuICAgIH1cblxuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG5cdGZvcm0ge1xuXHRcdG1hcmdpbi1ib3R0b206IDMwcHg7XG5cdH1cblx0Lm0tci0xMCB7XG5cdFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xuXHR9XG48L3N0eWxlPlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBFZGl0U2VydmljZS52dWU/NGQyYzhhOWIiLCI8dGVtcGxhdGU+XG5cdDxkaXY+XG5cdFx0PGRpdj5cblx0XHRcdDxkaXYgY2xhc3M9XCJyb3dcIj5cblx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XG5cdFx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLWJsb2NrXCIgXG5cdFx0XHRcdFx0XHRAY2xpY2s9XCJjaGFuZ2VNZW1iZXJUeXBlKCdsZWFkZXInKVwiPlxuXHRcdFx0XHRcdFx0PGkgY2xhc3M9XCJmYSBmYS1zdGFyIGZhLWZ3XCIgPjwvaT4gPGkgY2xhc3M9XCJmYSBmYS1zdGFyIGZhLWZ3XCIgPjwvaT4gTWFrZSBHcm91cCBMZWFkZXJcblx0XHRcdFx0XHQ8L2J1dHRvbj5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxuXHRcdFx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1pbmZvIGJ0bi1ibG9ja1wiIFxuXHRcdFx0XHRcdFx0QGNsaWNrPVwiY2hhbmdlTWVtYmVyVHlwZSgnbWVtYmVyJylcIj5cblx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtc3Rhci1vIGZhLWZ3XCIgPjwvaT4gTWFrZSBHcm91cCBNZW1iZXI8L2J1dHRvbj5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHQ8L2Rpdj5cblxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblxuICAgIGV4cG9ydCBkZWZhdWx0IHtcblxuICAgIFx0cHJvcHM6IFsnZ3JvdXBpZCddLFxuXG4gICAgXHRkYXRhKCkge1xuICAgIFx0XHRyZXR1cm4ge1xuICAgIFx0XHR9XG4gICAgXHR9LFxuXG4gICAgXHRtZXRob2RzOiB7XG5cbiAgICBcdFx0Y2hhbmdlTWVtYmVyVHlwZSh0eXBlKSB7XG4gICAgXHRcdFx0Ly8gb3B0aW9uID0gbWFrZS1ncm91cC1sZWFkZXIgfCBtYWtlLWdyb3VwLW1lbWJlclxuICAgIFx0XHRcdHZhciB2bSA9IHRoaXM7XG4gICAgXHRcdFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuY2hhbmdlTWVtYmVyVHlwZSh0eXBlKTtcblxuICAgIFx0XHR9LFxuXG5cbiAgICBcdH0sXG5cbiAgICBcdGNyZWF0ZWQoKSB7XG4gICAgXHRcdFxuICAgIFx0fVxuXG4gICAgfVxuXG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIEdyb3VwTWVtYmVyVHlwZS52dWU/ZDdmMTZhNzIiLCI8dGVtcGxhdGU+XG5cblx0PGRpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwic2NyZWVuID09IDFcIj5cblx0XHRcdDxkaXYgY2xhc3M9XCJyb3dcIj5cblx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XG5cdFx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLWJsb2NrXCIgXG5cdFx0XHRcdFx0XHRAY2xpY2s9XCJnZXREZXRhaWxzKCdjbGllbnQtdG8tZ3JvdXAnLCAyKVwiPkNsaWVudCB0byBHcm91cDwvYnV0dG9uPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XG5cdFx0XHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWluZm8gYnRuLWJsb2NrXCIgXG5cdFx0XHRcdFx0XHRAY2xpY2s9XCJnZXREZXRhaWxzKCdncm91cC10by1jbGllbnQnLCAyKVwiPkdyb3VwIHRvIENsaWVudDwvYnV0dG9uPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJzY3JlZW4gPT0gMlwiPlxuXHRcdFx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIiBAc3VibWl0LnByZXZlbnQ9XCJ0cmFuc2ZlclwiPlxuXHRcdFx0XHRcblx0XHRcdFx0PGRpdiBjbGFzcz1cInRhYmxlLXJlc3BvbnNpdmVcIj5cblx0ICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVyZWQgdGFibGUtaG92ZXIgdHJhbnNmZXItZGF0YXRhYmxlXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgPHRoZWFkPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgPHRyPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aCBjbGFzcz1cInRleHQtY2VudGVyXCI+XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHQ8aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgQGNsaWNrPVwidG9nZ2xlQWxsQ2hlY2tib3hcIj5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICA8L3RoPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5EZXRhaWw8L3RoPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5TdGF0dXM8L3RoPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5UcmFja2luZzwvdGg+XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgPHRoPlBhY2thZ2VzPC90aD5cblx0XHQgICAgICAgICAgICAgICAgICAgIDwvdHI+XG5cdCAgICAgICAgICAgICAgICAgICAgPC90aGVhZD5cblx0ICAgICAgICAgICAgICAgICAgICA8dGJvZHk+XG5cdCAgICAgICAgICAgICAgICAgICAgXHQ8dHIgdi1mb3I9XCJzZXJ2aWNlIGluIHNlcnZpY2VzXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdDx0ZCBjbGFzcz1cInRleHQtY2VudGVyXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdFx0PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIDp2YWx1ZT1cInNlcnZpY2UuaWRcIiB2LW1vZGVsPVwidHJhbnNmZXJGb3JtLnNlcnZpY2VzXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdDwvdGQ+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdDx0ZD57eyBzZXJ2aWNlLmRldGFpbCB9fTwvdGQ+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdDx0ZD57eyBzZXJ2aWNlLnN0YXR1cyB9fTwvdGQ+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdDx0ZD57eyBzZXJ2aWNlLnRyYWNraW5nIH19PC90ZD5cblx0ICAgICAgICAgICAgICAgICAgICBcdFx0PHRkPlxuXHQgICAgICAgICAgICAgICAgICAgIFx0XHRcdDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiA6Y2xhc3M9XCIndHJhbnNmZXItcGFja2FnZS1zZWxlY3QtJyArIHNlcnZpY2UuaWRcIiBcblx0ICAgICAgICAgICAgICAgICAgICBcdFx0XHQ+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdFx0XHQ8b3B0aW9uIHZhbHVlPVwiR2VuZXJhdGUgbmV3IHBhY2thZ2VcIj5HZW5lcmF0ZSBuZXcgcGFja2FnZTwvb3B0aW9uPlxuXHQgICAgICAgICAgICAgICAgICAgIFx0XHRcdFx0PG9wdGlvbiB2LWZvcj1cInBhY2thZ2UgaW4gcGFja2FnZXNcIiA6dmFsdWU9XCJwYWNrYWdlLnRyYWNraW5nXCIgPlxuXHQgICAgICAgICAgICAgICAgICAgIFx0XHRcdFx0XHRQYWNrYWdlICN7e3BhY2thZ2UudHJhY2tpbmd9fVxuXHQgICAgICAgICAgICAgICAgICAgIFx0XHRcdFx0XHQoe3sgcGFja2FnZS5sb2dfZGF0ZSB8IGNvbW1vbkRhdGUgfX0pXG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdFx0XHQ8L29wdGlvbj5cblx0ICAgICAgICAgICAgICAgICAgICBcdFx0XHQ8L3NlbGVjdD5cblx0ICAgICAgICAgICAgICAgICAgICBcdFx0PC90ZD5cblx0ICAgICAgICAgICAgICAgICAgICBcdDwvdHI+XG5cdCAgICAgICAgICAgICAgICAgICAgPC90Ym9keT5cblx0ICAgICAgICAgICAgICAgIDwvdGFibGU+XG5cdCAgICAgICAgICAgIDwvZGl2PlxuXG5cdFx0XHRcdDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIj5Db21wbGV0ZSBUcmFuc2ZlcjwvYnV0dG9uPlxuXHRcdCAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgQGNsaWNrPVwic2V0U2NyZWVuKDEpXCI+QmFjazwvYnV0dG9uPlxuXHRcdCAgICA8L2Zvcm0+XG5cdFx0PC9kaXY+XG5cblx0PC9kaXY+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5cbiAgICBleHBvcnQgZGVmYXVsdCB7XG5cbiAgICBcdHByb3BzOiBbJ2dyb3VwaWQnXSxcblxuICAgIFx0ZGF0YSgpIHtcbiAgICBcdFx0cmV0dXJuIHtcbiAgICBcdFx0XHRzY3JlZW46IG51bGwsXG5cbiAgICBcdFx0XHRzZXJ2aWNlczogW10sXG4gICAgXHRcdFx0cGFja2FnZXM6IFtdLFxuXG4gICAgXHRcdFx0dHJhbnNmZXJGb3JtOiBuZXcgRm9ybSh7XG4gICAgXHRcdFx0XHRncm91cElkOiAnJyxcbiAgICBcdFx0XHRcdG1lbWJlcklkOiAnJyxcblx0XHRcdFx0XHRzZXJ2aWNlczogW10sXG5cdFx0XHRcdFx0cGFja2FnZXM6W10sXG5cdFx0XHRcdFx0b3B0aW9uOiAnJ1xuXHRcdFx0XHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxuICAgIFx0XHR9XG4gICAgXHR9LFxuXG4gICAgXHRtZXRob2RzOiB7XG5cbiAgICBcdFx0c2V0U2NyZWVuKHNjcmVlbikge1xuICAgIFx0XHRcdHRoaXMuc2NyZWVuID0gc2NyZWVuO1xuICAgIFx0XHR9LFxuXG4gICAgXHRcdHNldE1lbWJlcklkKGlkKSB7XG4gICAgXHRcdFx0dGhpcy50cmFuc2ZlckZvcm0ubWVtYmVySWQgPSBpZDtcblxuICAgIFx0XHRcdHRoaXMuc2V0U2NyZWVuKDEpO1xuICAgIFx0XHR9LFxuXG4gICAgXHRcdGdldERldGFpbHMob3B0aW9uLCBzY3JlZW4pIHtcbiAgICBcdFx0XHQvLyBvcHRpb24gPSBjbGllbnQtdG8tZ3JvdXAgfCBncm91cC10by1jbGllbnRcbiAgICBcdFx0XHR0aGlzLnRyYW5zZmVyRm9ybS5vcHRpb24gPSBvcHRpb247XG5cbiAgICBcdFx0XHR2YXIgdm0gPSB0aGlzO1xuXG5cdFx0XHRcdGZ1bmN0aW9uIGdldFNlcnZpY2VzKCkge1xuXHQgICAgICAgICAgICAgICAgcmV0dXJuIGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50LycrIHZtLnRyYW5zZmVyRm9ybS5tZW1iZXJJZCArJy9zZXJ2aWNlcycsIHtcbiAgICBcdFx0XHRcdFx0cGFyYW1zOiB7XG4gICAgXHRcdFx0XHRcdFx0b3B0aW9uOiBvcHRpb24sXG4gICAgXHRcdFx0XHRcdFx0Z3JvdXBJZDogdm0uZ3JvdXBpZFxuICAgIFx0XHRcdFx0XHR9XG4gICAgXHRcdFx0XHR9KTtcblx0ICAgICAgICAgICAgfVxuXHQgICAgICAgICAgICBmdW5jdGlvbiBnZXRQYWNrYWdlcygpIHtcblx0ICAgICAgICAgICAgICAgIC8vIGF4aW9zXG5cdCAgICAgICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvJysgdm0udHJhbnNmZXJGb3JtLm1lbWJlcklkICsnL3BhY2thZ2VzJywge1xuICAgIFx0XHRcdFx0XHRwYXJhbXM6IHtcbiAgICBcdFx0XHRcdFx0XHRvcHRpb246IG9wdGlvbixcbiAgICBcdFx0XHRcdFx0XHRncm91cElkOiB2bS5ncm91cGlkXG4gICAgXHRcdFx0XHRcdH1cbiAgICBcdFx0XHRcdH0pO1xuXHQgICAgICAgICAgICB9XG5cblx0ICAgICAgICAgICAgYXhpb3MuYWxsKFtcblx0ICAgICAgICAgICAgICAgIGdldFNlcnZpY2VzKCksXG5cdCAgICAgICAgICAgICAgICBnZXRQYWNrYWdlcygpXG5cdCAgICAgICAgICAgIF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxuXHQgICAgICAgICAgICAgICAgKFxuXHQgICAgICAgICAgICAgICAgc2VydmljZXMsXG5cdCAgICAgICAgICAgICAgICBwYWNrYWdlc1xuXHQgICAgICAgICAgICAgICAgKSA9PiB7XG5cdCAgICAgICAgICAgICAgICAgICAgXG5cdCAgICAgICAgICAgICAgICB0aGlzLnNlcnZpY2VzID0gc2VydmljZXMuZGF0YTtcblx0ICAgICAgICAgICAgICAgIHRoaXMucGFja2FnZXMgPSBwYWNrYWdlcy5kYXRhO1xuXHQgICAgICAgICAgICB9KSlcblx0ICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG5cbiAgICBcdFx0XHR0aGlzLnNldFNjcmVlbihzY3JlZW4pO1xuICAgIFx0XHR9LFxuXG4gICAgXHRcdGNhbGxEYXRhVGFibGUoKSB7XG5cdFx0XHRcdCQoJy50cmFuc2Zlci1kYXRhdGFibGUnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHQkKCcudHJhbnNmZXItZGF0YXRhYmxlJykuRGF0YVRhYmxlKHtcblx0XHRcdCAgICAgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxuXHRcdFx0ICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHRcdCAgICAgICAgICAgIGRvbTogJzxcInRvcFwibGY+cnQ8XCJib3R0b21cImlwPjxcImNsZWFyXCI+J1xuXHRcdFx0ICAgICAgICB9KTtcblx0XHRcdFx0fSwgMTAwMCk7XG5cdFx0XHR9LFxuXG5cdFx0XHR0b2dnbGVBbGxDaGVja2JveChlKSB7XG5cdFx0XHRcdGlmKGUudGFyZ2V0LmNoZWNrZWQgPT0gdHJ1ZSkge1xuXHRcdFx0XHRcdHRoaXMudHJhbnNmZXJGb3JtLnNlcnZpY2VzID0gW107XG5cdFx0XHRcdFx0dGhpcy5zZXJ2aWNlcy5mb3JFYWNoKChzLCBpbmRleCkgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy50cmFuc2ZlckZvcm0uc2VydmljZXMucHVzaChzLmlkKTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR0aGlzLnRyYW5zZmVyRm9ybS5zZXJ2aWNlcyA9IFtdO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXG5cdFx0XHRyZXNldFRyYW5zZmVyRm9ybSgpIHtcblx0XHRcdFx0dGhpcy5zZXJ2aWNlcyA9IFtdO1xuXHRcdFx0XHR0aGlzLnBhY2thZ2VzID0gW107XG5cdFx0XHR9LFxuXG4gICAgXHRcdHRyYW5zZmVyKCkge1xuICAgIFx0XHRcdGlmKHRoaXMudHJhbnNmZXJGb3JtLnNlcnZpY2VzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgIFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBhdCBsZWFzdCBvbmUgc2VydmljZScpO1xuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcbiAgICAgICAgXHRcdH0gZWxzZSB7XG5cbiAgICAgICAgXHRcdFx0dGhpcy50cmFuc2ZlckZvcm0ucGFja2FnZXMgPSBbXTtcblxuICAgICAgICBcdFx0XHR0aGlzLnRyYW5zZmVyRm9ybS5zZXJ2aWNlcy5mb3JFYWNoKChzLCBpbmRleCkgPT4ge1xuXHRcdCAgICAgICAgXHRcdGxldCB2YWx1ZSA9ICQoJ3NlbGVjdC50cmFuc2Zlci1wYWNrYWdlLXNlbGVjdC0nICsgcykudmFsKCk7XG5cdFx0ICAgICAgICBcdFx0XHRcblx0XHQgICAgICAgIFx0XHR0aGlzLnRyYW5zZmVyRm9ybS5wYWNrYWdlcy5wdXNoKHZhbHVlKTtcblx0XHQgICAgICAgXHRcdH0pO1xuXG5cdCAgICBcdFx0XHR0aGlzLnRyYW5zZmVyRm9ybS5zdWJtaXQoJ3BhdGNoJywgJy92aXNhL2NsaWVudC8nICsgdGhpcy5ncm91cGlkICsgJy90cmFuc2ZlcicpXG5cdFx0ICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdCAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XG5cdFx0ICAgICAgICAgICAgICAgIFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hEZXRhaWxzKCk7XG5cdFx0ICAgICAgICAgICAgICAgIFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hNZW1iZXJzKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLiRwYXJlbnQuJHBhcmVudC4kcmVmcy5hZGRuZXdzZXJ2aWNlcmVmLmZldGNoR3JvdXBQYWNrYWdlc1NlcnZpY2VzTGlzdCgpO1xuXG5cdFx0ICAgICAgICAgICAgICAgIFx0dGhpcy5yZXNldFRyYW5zZmVyRm9ybSgpO1xuXG5cdFx0ICAgICAgICAgICAgICAgIFx0JCgnI3RyYW5zZmVyLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xuXHRcdCAgICAgICAgICAgICAgICB9XG5cdFx0ICAgICAgICAgICAgfSlcblx0XHQgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblxuICAgICAgICBcdFx0fVxuICAgIFx0XHR9XG5cbiAgICBcdH0sXG5cbiAgICBcdGNyZWF0ZWQoKSB7XG4gICAgXHRcdHRoaXMudHJhbnNmZXJGb3JtLmdyb3VwSWQgPSB0aGlzLmdyb3VwaWQ7XG4gICAgXHR9XG5cbiAgICB9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gVHJhbnNmZXIudnVlPzQwYjdmN2NiIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQWN0aW9uTG9ncy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTcyYjc1NWI1XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0FjdGlvbkxvZ3MudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvQWN0aW9uTG9ncy52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBBY3Rpb25Mb2dzLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi03MmI3NTViNVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTcyYjc1NWI1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9BY3Rpb25Mb2dzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjlcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi0wNmQyNjQ3MV0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTA2ZDI2NDcxXSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiRWRpdFNlcnZpY2UudnVlPzRkMmM4YTliXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUFxUEE7Q0FDQSxvQkFBQTtDQUNBO0FBQ0E7Q0FDQSxtQkFBQTtDQUNBXCIsXCJmaWxlXCI6XCJFZGl0U2VydmljZS52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcblxcdDxkaXY+XFxuXFx0XFx0PGRpdiB2LXNob3c9J3NjcmVlbiA9PSAxJz5cXG5cXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJ0YWJsZS1yZXNwb25zaXZlXFxcIj5cXG5cXHQgICAgICAgICAgICA8dGFibGUgY2xhc3M9XFxcInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVyZWQgdGFibGUtaG92ZXIgY2xpZW50cy1kYXRhdGFibGVcXFwiPlxcblxcdCAgICAgICAgICAgICAgICA8dGhlYWQ+XFxuXFx0XFx0ICAgICAgICAgICAgICAgIDx0cj5cXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgIDx0aCBjbGFzcz1cXFwidGV4dC1jZW50ZXJcXFwiPlxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVxcXCJjaGVja2JveFxcXCIgQGNsaWNrPVxcXCJ0b2dnbGVBbGxDaGVja2JveFxcXCI+XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICA8L3RoPlxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgPHRoPk5hbWU8L3RoPlxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgPHRoPkNsaWVudCBJRDwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICA8dGg+UGFja2FnZTwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICA8dGg+RGF0ZSBSZWNvcmRlZDwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICA8dGg+RGV0YWlsczwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICA8dGg+U3RhdHVzPC90aD5cXG5cXHRcXHQgICAgICAgICAgICAgICAgPC90cj5cXG5cXHQgICAgICAgICAgICAgICAgPC90aGVhZD5cXG5cXHQgICAgICAgICAgICAgICAgPHRib2R5PlxcblxcdCAgICAgICAgICAgICAgICAgICAgPHRyIHYtZm9yPVxcXCJjbGllbnRTZXJ2aWNlIGluIGNsaWVudFNlcnZpY2VzXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdDx0ZCBjbGFzcz1cXFwidGV4dC1jZW50ZXJcXFwiPlxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0XFx0PGlucHV0IHR5cGU9XFxcImNoZWNrYm94XFxcIiBcXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdFxcdFxcdDp2YWx1ZT1cXFwiY2xpZW50U2VydmljZS5pZFxcXCIgXFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHR2LW1vZGVsPVxcXCJlZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZFxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHQ8L3RkPlxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0PHRkPnt7IGNsaWVudFNlcnZpY2UudXNlci5mdWxsX25hbWUgfX08L3RkPlxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0PHRkPnt7IGNsaWVudFNlcnZpY2UudXNlci5pZCB9fTwvdGQ+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHQ8dGQ+e3sgY2xpZW50U2VydmljZS50cmFja2luZyB9fTwvdGQ+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHQ8dGQ+e3sgY2xpZW50U2VydmljZS5zZXJ2aWNlX2RhdGUgfX08L3RkPlxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0PHRkPnt7IGNsaWVudFNlcnZpY2UuZGV0YWlsIH19PC90ZD5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdDx0ZD57eyBjbGllbnRTZXJ2aWNlLnN0YXR1cyB9fTwvdGQ+XFxuXFx0ICAgICAgICAgICAgICAgICAgICA8L3RyPlxcblxcdCAgICAgICAgICAgICAgICA8L3Rib2R5PlxcblxcdCAgICAgICAgICAgIDwvdGFibGU+XFxuXFx0ICAgICAgICA8L2Rpdj5cXG5cXG5cXHQgICAgICAgIDxhIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCIgQGNsaWNrPVxcXCJjaGFuZ2VTY3JlZW4oMilcXFwiPkNvbnRpbnVlPC9hPlxcblxcdCAgICBcXHQ8YSBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBkYXRhLWRpc21pc3M9XFxcIm1vZGFsXFxcIj5DbG9zZTwvYT5cXG5cXG5cXHQgICAgXFx0PGJyPlxcblxcdFxcdDwvZGl2PlxcblxcblxcdFxcdDxkaXYgdi1zaG93PSdzY3JlZW4gPT0gMic+XFxuXFx0XFx0XFx0PGZvcm0gQHN1Ym1pdC5wcmV2ZW50PVxcXCJlZGl0U2VydmljZVxcXCI+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwicm93XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBlZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygnYWRkaXRpb25hbF9jaGFyZ2UnKSB9XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgPGxhYmVsIGZvcj1cXFwiYWRkaXRvbmFsX2NoYXJnZVxcXCI+QWRkaXRpb25hbCBDaGFyZ2U8L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgaWQ9XFxcImFkZGl0aW9uYWxfY2hhcmdlXFxcIiBuYW1lPVxcXCJhZGRpdGlvbmFsX2NoYXJnZVxcXCIgdi1tb2RlbD1cXFwiZWRpdFNlcnZpY2VGb3JtLmFkZGl0aW9uYWxfY2hhcmdlXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgPHNwYW4gY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIHYtaWY9XFxcImVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdhZGRpdGlvbmFsX2NoYXJnZScpXFxcIiB2LXRleHQ9XFxcImVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdhZGRpdGlvbmFsX2NoYXJnZScpXFxcIj48L3NwYW4+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBlZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygnY29zdCcpIH1cXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8bGFiZWwgZm9yPVxcXCJjb3N0XFxcIj5Db3N0PC9sYWJlbD5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgPGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIGlkPVxcXCJjb3N0XFxcIiBuYW1lPVxcXCJjb3N0XFxcIiB2LW1vZGVsPVxcXCJlZGl0U2VydmljZUZvcm0uY29zdFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIDxzcGFuIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJlZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygnY29zdCcpXFxcIiB2LXRleHQ9XFxcImVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdjb3N0JylcXFwiPjwvc3Bhbj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiA6Y2xhc3M9XFxcInsgJ2hhcy1lcnJvcic6IGVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdkaXNjb3VudCcpIH1cXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8bGFiZWwgZm9yPVxcXCJjb3N0XFxcIj5EaXNjb3VudDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBpZD1cXFwiZGlzY291bnRcXFwiIG5hbWU9XFxcImRpc2NvdW50XFxcIiB2LW1vZGVsPVxcXCJlZGl0U2VydmljZUZvcm0uZGlzY291bnRcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8c3BhbiBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2Rpc2NvdW50JylcXFwiIHYtdGV4dD1cXFwiZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5nZXQoJ2Rpc2NvdW50JylcXFwiPjwvc3Bhbj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIDxsYWJlbCBmb3I9XFxcInN0YXR1c1xcXCI+U3RhdHVzPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgPHNlbGVjdCBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBpZD1cXFwic3RhdHVzXFxcIiBuYW1lPVxcXCJzdGF0dXNcXFwiIHYtbW9kZWw9XFxcImVkaXRTZXJ2aWNlRm9ybS5zdGF0dXNcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8b3B0aW9uIHZhbHVlPVxcXCJjb21wbGV0ZVxcXCI+Q29tcGxldGU8L29wdGlvbj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgXFx0PG9wdGlvbiB2YWx1ZT1cXFwib24gcHJvY2Vzc1xcXCI+T24gUHJvY2Vzczwvb3B0aW9uPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8b3B0aW9uIHZhbHVlPVxcXCJwZW5kaW5nXFxcIj5QZW5kaW5nPC9vcHRpb24+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIDwvc2VsZWN0PlxcblxcdFxcdFxcdFxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8bGFiZWwgZm9yPVxcXCJub3RlXFxcIj5Ob3RlPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgPHRleHRhcmVhIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIGlkPVxcXCJub3RlXFxcIiBuYW1lPVxcXCJub3RlXFxcIiB2LW1vZGVsPVxcXCJlZGl0U2VydmljZUZvcm0ubm90ZVxcXCIgcGxhY2Vob2xkZXI9XFxcImZvciBpbnRlcm5hbCB1c2Ugb25seSwgb25seSBlbXBsb3llZXMgY2FuIHNlZS5cXFwiPjwvdGV4dGFyZWE+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgc3R5bGU9XFxcIm1hcmdpbi10b3A6IDIycHg7XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgPGxhYmVsIGZvcj1cXFwiYWN0aXZlXFxcIj48L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcInJhZGlvLWlubGluZVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0ICA8aW5wdXQgdHlwZT1cXFwicmFkaW9cXFwiIGlkPVxcXCJhY3RpdmVcXFwiIG5hbWU9XFxcImFjdGl2ZVxcXCIgdmFsdWU9XFxcIjFcXFwiIHYtbW9kZWw9XFxcImVkaXRTZXJ2aWNlRm9ybS5hY3RpdmVcXFwiPiBFbmFibGVcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQ8L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdDxsYWJlbCBjbGFzcz1cXFwicmFkaW8taW5saW5lXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQgIDxpbnB1dCB0eXBlPVxcXCJyYWRpb1xcXCIgaWQ9XFxcImFjdGl2ZVxcXCIgbmFtZT1cXFwiYWN0aXZlXFxcIiB2YWx1ZT1cXFwiMFxcXCIgdi1tb2RlbD1cXFwiZWRpdFNlcnZpY2VGb3JtLmFjdGl2ZVxcXCI+IERpc2FibGVcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQ8L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ3JlYXNvbicpIH1cXFwiIHYtaWY9XFxcImVkaXRTZXJ2aWNlRm9ybS5kaXNjb3VudD4wXFxcIiBzdHlsZT1cXFwibWFyZ2luLXRvcDogMjVweDtcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8bGFiZWwgZm9yPVxcXCJjb3N0XFxcIj5SZWFzb248L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgaWQ9XFxcInJlYXNvblxcXCIgbmFtZT1cXFwicmVhc29uXFxcIiB2LW1vZGVsPVxcXCJlZGl0U2VydmljZUZvcm0ucmVhc29uXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgPHNwYW4gY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIHYtaWY9XFxcImVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdyZWFzb24nKVxcXCIgdi10ZXh0PVxcXCJlZGl0U2VydmljZUZvcm0uZXJyb3JzLmdldCgncmVhc29uJylcXFwiPjwvc3Bhbj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHQ8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5TYXZlPC9idXR0b24+XFxuXFx0ICAgIFxcdFxcdDxhIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIEBjbGljaz1cXFwiY2hhbmdlU2NyZWVuKDEpXFxcIj5CYWNrPC9hPlxcblxcdFxcdFxcdDwvZm9ybT5cXG5cXHRcXHQ8L2Rpdj5cXG5cXHQ8L2Rpdj5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFxuICAgIGV4cG9ydCBkZWZhdWx0IHtcXG5cXG4gICAgXFx0cHJvcHM6IFsnZ3JvdXBpZCddLFxcblxcbiAgICBcXHRkYXRhKCkge1xcbiAgICBcXHRcXHRyZXR1cm4ge1xcbiAgICBcXHRcXHRcXHRzY3JlZW46IDEsXFxuXFxuICAgIFxcdFxcdFxcdGNsaWVudFNlcnZpY2VzOiBbXSxcXG5cXG4gICAgXFx0XFx0XFx0ZWRpdFNlcnZpY2VGb3JtOiBuZXcgRm9ybSh7XFxuICAgIFxcdFxcdFxcdFxcdGNsaWVudFNlcnZpY2VzSWQ6IFtdLFxcbiAgICBcXHRcXHRcXHRcXHRzZXJ2aWNlSWQ6ICcnLFxcbiAgICBcXHRcXHRcXHRcXHRhZGRpdGlvbmFsX2NoYXJnZTogJycsXFxuICAgIFxcdFxcdFxcdFxcdGNvc3Q6ICcnLFxcbiAgICBcXHRcXHRcXHRcXHRkaXNjb3VudDogJycsXFxuICAgIFxcdFxcdFxcdFxcdHJlYXNvbjogJycsXFxuICAgIFxcdFxcdFxcdFxcdHN0YXR1czogJycsXFxuICAgIFxcdFxcdFxcdFxcdG5vdGU6ICcnLFxcbiAgICBcXHRcXHRcXHRcXHRhY3RpdmU6ICcnXFxuXFx0XFx0XFx0XFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSlcXHRcXG4gICAgXFx0XFx0fVxcbiAgICBcXHR9LFxcblxcbiAgICBcXHRtZXRob2RzOiB7XFxuXFxuICAgIFxcdFxcdGNoYW5nZVNjcmVlbihzY3JlZW4pIHtcXG4gICAgXFx0XFx0XFx0aWYoc2NyZWVuID09IDIpIHtcXG5cXHRcXHRcXHRcXHRcXHRpZih0aGlzLmVkaXRTZXJ2aWNlRm9ybS5jbGllbnRTZXJ2aWNlc0lkLmxlbmd0aCA9PSAwKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGF0IGxlYXN0IG9uZSBjbGllbnQnKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRyZXR1cm4gZmFsc2U7XFxuXFx0XFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdFxcdH1cXG5cXG4gICAgXFx0XFx0XFx0dGhpcy5zY3JlZW4gPSBzY3JlZW47XFxuICAgIFxcdFxcdH0sXFxuXFxuICAgIFxcdFxcdHNldEVkaXRTZXJ2aWNlRm9ybShjbGllbnRTZXJ2aWNlSWQsIHNlcnZpY2VJZCwgYWRkaXRpb25hbENoYXJnZSwgY29zdCwgZGlzY291bnQsIHJlYXNvbiwgc3RhdHVzLCBub3RlLCBhY3RpdmUpIHtcXG4gICAgXFx0XFx0XFx0dGhpcy5lZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZCA9IFtdO1xcbiAgICBcXHRcXHRcXHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5jbGllbnRTZXJ2aWNlc0lkLnB1c2goY2xpZW50U2VydmljZUlkKTtcXG5cXG4gICAgXFx0XFx0XFx0dGhpcy5lZGl0U2VydmljZUZvcm0uc2VydmljZUlkID0gc2VydmljZUlkO1xcbiAgICBcXHRcXHRcXHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5hZGRpdGlvbmFsX2NoYXJnZSA9IGFkZGl0aW9uYWxDaGFyZ2U7XFxuICAgIFxcdFxcdFxcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLmNvc3QgPSBjb3N0O1xcbiAgICBcXHRcXHRcXHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5kaXNjb3VudCA9IGRpc2NvdW50O1xcbiAgICBcXHRcXHRcXHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5yZWFzb24gPSByZWFzb247XFxuICAgIFxcdFxcdFxcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLnN0YXR1cyA9IHN0YXR1cztcXG4gICAgXFx0XFx0XFx0dGhpcy5lZGl0U2VydmljZUZvcm0ubm90ZSA9ICcnO1xcbiAgICBcXHRcXHRcXHR0aGlzLmVkaXRTZXJ2aWNlRm9ybS5hY3RpdmUgPSBhY3RpdmU7XFxuICAgIFxcdFxcdH0sXFxuXFxuICAgIFxcdFxcdGluaXQoY2xpZW50SWQsIHNlcnZpY2VJZCxjaWQpIHtcXG4gICAgXFx0XFx0XFx0dGhpcy5jaGFuZ2VTY3JlZW4oMSk7XFxuXFxuICAgIFxcdFxcdFxcdGxldCBfY2xpZW50SWQgPSBjbGllbnRJZDtcXG4gICAgXFx0XFx0XFx0bGV0IF9zZXJ2aWNlSWQgPSBzZXJ2aWNlSWQ7XFxuICAgIFxcdFxcdFxcdGxldCBfY2lkID0gY2lkO1xcbiAgICBcXHRcXHRcXHRsZXQgX2dyb3VwSWQgPSB0aGlzLmdyb3VwaWQ7XFxuICAgIFxcdFxcdFxcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJysgX2dyb3VwSWQgKycvZWRpdC1zZXJ2aWNlcycsIHtcXG4gICAgXFx0XFx0XFx0XFx0cGFyYW1zOiB7XFxuICAgIFxcdFxcdFxcdFxcdFxcdGNsaWVudElkOiBfY2xpZW50SWQsXFxuICAgIFxcdFxcdFxcdFxcdFxcdHNlcnZpY2VJZDogX3NlcnZpY2VJZCxcXG4gICAgXFx0XFx0XFx0XFx0XFx0Y2lkOiBfY2lkXFxuICAgIFxcdFxcdFxcdFxcdH1cXG4gICAgXFx0XFx0XFx0fSlcXG4gICAgXFx0XFx0XFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xcbiAgICBcXHRcXHRcXHRcXHR0aGlzLmNsaWVudFNlcnZpY2VzID0gcmVzcG9uc2UuZGF0YTtcXG4gICAgXFx0XFx0XFx0XFx0Ly9jb25zb2xlLmxvZyhyZXNwb25zZS5kYXRhWzBdLmlkKTtcXG4gICAgXFx0XFx0XFx0XFx0dGhpcy5jYWxsRGF0YVRhYmxlKCk7XFxuXFxuICAgIFxcdFxcdFxcdFxcdGxldCB7IHRpcCwgY29zdCwgc3RhdHVzLCByZW1hcmtzLCBhY3RpdmUgfSA9IHJlc3BvbnNlLmRhdGFbMF07XFxuICAgIFxcdFxcdFxcdFxcdGxldCBkaXNjb3VudCA9IG51bGw7XFxuICAgIFxcdFxcdFxcdFxcdGxldCByZWFzb24gPSBudWxsO1xcblxcbiAgICBcXHRcXHRcXHRcXHRpZihyZXNwb25zZS5kYXRhWzBdLmRpc2NvdW50KXtcXG5cXHQgICAgXFx0XFx0XFx0XFx0bGV0IGRpc2NvdW50ID0gKHJlc3BvbnNlLmRhdGFbMF0uZGlzY291bnQubGVuZ3RoID4gMCkgXFxuXFx0ICAgIFxcdFxcdFxcdFxcdFxcdD8gcmVzcG9uc2UuZGF0YVswXS5kaXNjb3VudFswXS5kaXNjb3VudF9hbW91bnRcXG5cXHQgICAgXFx0XFx0XFx0XFx0XFx0OiAnJztcXG5cXHQgICAgXFx0XFx0XFx0XFx0bGV0IHJlYXNvbiA9IChyZXNwb25zZS5kYXRhWzBdLmRpc2NvdW50Lmxlbmd0aCA+IDApIFxcblxcdCAgICBcXHRcXHRcXHRcXHRcXHQ/IHJlc3BvbnNlLmRhdGFbMF0uZGlzY291bnRbMF0ucmVhc29uIFxcblxcdCAgICBcXHRcXHRcXHRcXHRcXHQ6ICcnO1xcbiAgICBcXHRcXHRcXHRcXHR9XFxuXFxuICAgIFxcdFxcdFxcdFxcdHRoaXMuc2V0RWRpdFNlcnZpY2VGb3JtKF9jaWQsIHNlcnZpY2VJZCwgdGlwLCBjb3N0LCBkaXNjb3VudCwgcmVhc29uLCBzdGF0dXMsIHJlbWFya3MsIGFjdGl2ZSk7XFxuICAgIFxcdFxcdFxcdH0pXFxuICAgIFxcdFxcdFxcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xcbiAgICBcXHRcXHR9LFxcblxcbiAgICBcXHRcXHRjYWxsRGF0YVRhYmxlKCkge1xcblxcdFxcdFxcdFxcdCQoJy5jbGllbnRzLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcXG5cXG5cXHRcXHRcXHRcXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xcblxcdFxcdFxcdFxcdFxcdCQoJy5jbGllbnRzLWRhdGF0YWJsZScpLkRhdGFUYWJsZSh7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgcGFnZUxlbmd0aDogMTAsXFxuXFx0XFx0XFx0ICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcXG5cXHRcXHRcXHQgICAgICAgICAgICBkb206ICc8XFxcInRvcFxcXCJsZj5ydDxcXFwiYm90dG9tXFxcImlwPjxcXFwiY2xlYXJcXFwiPidcXG5cXHRcXHRcXHQgICAgICAgIH0pO1xcblxcdFxcdFxcdFxcdH0sIDEwMDApO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0dG9nZ2xlQWxsQ2hlY2tib3goZSkge1xcblxcdFxcdFxcdFxcdGlmKGUudGFyZ2V0LmNoZWNrZWQgPT0gdHJ1ZSkge1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLmNsaWVudFNlcnZpY2VzSWQgPSBbXTtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmNsaWVudFNlcnZpY2VzLmZvckVhY2goKGMsIGluZGV4KSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZC5wdXNoKGMuaWQpO1xcblxcdFxcdFxcdFxcdFxcdH0pO1xcblxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZCA9IFtdO1xcblxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGVkaXRTZXJ2aWNlKCkge1xcblxcdFxcdFxcdFxcdHRoaXMuZWRpdFNlcnZpY2VGb3JtLnN1Ym1pdCgncGF0Y2gnLCAnL3Zpc2EvZ3JvdXAvJyArIHRoaXMuZ3JvdXBpZCArICcvZWRpdC1zZXJ2aWNlcycpXFxuXFx0XFx0ICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XFxuXFx0XFx0ICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2Vzcykge1xcblxcdFxcdCAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuJHBhcmVudC5mZXRjaERldGFpbHMoKTtcXG5cXHRcXHQgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hNZW1iZXJzKCk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC4kcGFyZW50LiRyZWZzLmFkZG5ld3NlcnZpY2VyZWYuZmV0Y2hHcm91cFBhY2thZ2VzU2VydmljZXNMaXN0KCk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgICQoJyNlZGl0LXNlcnZpY2UtbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5tZXNzYWdlKTtcXG5cXHRcXHQgICAgICAgICAgICB9XFxuXFx0XFx0ICAgICAgICB9KVxcblxcdFxcdCAgICAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XFxuXFx0XFx0XFx0fVxcblxcbiAgICBcXHR9XFxuXFxuICAgIH1cXG5cXG48L3NjcmlwdD5cXG5cXG48c3R5bGUgc2NvcGVkPlxcblxcdGZvcm0ge1xcblxcdFxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxuXFx0fVxcblxcdC5tLXItMTAge1xcblxcdFxcdG1hcmdpbi1yaWdodDogMTBweDtcXG5cXHR9XFxuPC9zdHlsZT5cIl19XSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTA2ZDI2NDcxXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRWRpdFNlcnZpY2UudnVlXG4vLyBtb2R1bGUgaWQgPSAyOTNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSgpO1xuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuZm9ybVtkYXRhLXYtMTY4ZjJlMGVdIHtcXG5cXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcbn1cXG4ubS1yLTEwW2RhdGEtdi0xNjhmMmUwZV0ge1xcblxcdG1hcmdpbi1yaWdodDogMTBweDtcXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIkdyb3VwTWVtYmVyVHlwZS52dWU/ZDdmMTZhNzJcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQXNEQTtDQUNBLG9CQUFBO0NBQ0E7QUFDQTtDQUNBLG1CQUFBO0NBQ0FcIixcImZpbGVcIjpcIkdyb3VwTWVtYmVyVHlwZS52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdDxkaXY+XFxuXFx0XFx0PGRpdj5cXG5cXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJyb3dcXFwiPlxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImNvbC1tZC02XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQ8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBidG4tYmxvY2tcXFwiIFxcblxcdFxcdFxcdFxcdFxcdFxcdEBjbGljaz1cXFwiY2hhbmdlTWVtYmVyVHlwZSgnbGVhZGVyJylcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdDxpIGNsYXNzPVxcXCJmYSBmYS1zdGFyIGZhLWZ3XFxcIiA+PC9pPiA8aSBjbGFzcz1cXFwiZmEgZmEtc3RhciBmYS1md1xcXCIgPjwvaT4gTWFrZSBHcm91cCBMZWFkZXJcXG5cXHRcXHRcXHRcXHRcXHQ8L2J1dHRvbj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWluZm8gYnRuLWJsb2NrXFxcIiBcXG5cXHRcXHRcXHRcXHRcXHRcXHRAY2xpY2s9XFxcImNoYW5nZU1lbWJlclR5cGUoJ21lbWJlcicpXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQ8aSBjbGFzcz1cXFwiZmEgZmEtc3Rhci1vIGZhLWZ3XFxcIiA+PC9pPiBNYWtlIEdyb3VwIE1lbWJlcjwvYnV0dG9uPlxcblxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdDwvZGl2PlxcblxcblxcdDwvZGl2PlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXG4gICAgZXhwb3J0IGRlZmF1bHQge1xcblxcbiAgICBcXHRwcm9wczogWydncm91cGlkJ10sXFxuXFxuICAgIFxcdGRhdGEoKSB7XFxuICAgIFxcdFxcdHJldHVybiB7XFxuICAgIFxcdFxcdH1cXG4gICAgXFx0fSxcXG5cXG4gICAgXFx0bWV0aG9kczoge1xcblxcbiAgICBcXHRcXHRjaGFuZ2VNZW1iZXJUeXBlKHR5cGUpIHtcXG4gICAgXFx0XFx0XFx0Ly8gb3B0aW9uID0gbWFrZS1ncm91cC1sZWFkZXIgfCBtYWtlLWdyb3VwLW1lbWJlclxcbiAgICBcXHRcXHRcXHR2YXIgdm0gPSB0aGlzO1xcbiAgICBcXHRcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5jaGFuZ2VNZW1iZXJUeXBlKHR5cGUpO1xcblxcbiAgICBcXHRcXHR9LFxcblxcblxcbiAgICBcXHR9LFxcblxcbiAgICBcXHRjcmVhdGVkKCkge1xcbiAgICBcXHRcXHRcXG4gICAgXFx0fVxcblxcbiAgICB9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0xNjhmMmUwZVwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0dyb3VwTWVtYmVyVHlwZS52dWVcbi8vIG1vZHVsZSBpZCA9IDI5NVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi0yN2FiODIzMF0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTI3YWI4MjMwXSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiRWRpdEFkZHJlc3MudnVlP2Q3MDQxMGJlXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUFnR0E7Q0FDQSxvQkFBQTtDQUNBO0FBQ0E7Q0FDQSxtQkFBQTtDQUNBXCIsXCJmaWxlXCI6XCJFZGl0QWRkcmVzcy52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcblxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwiZWRpdEFkZHJlc3NcXFwiPlxcblxcblxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5oYXMoJ2FkZHJlc3MnKSB9XFxcIj5cXG5cXHQgICAgICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLXNtLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0ICAgICAgICAgICAgTmFtZVxcblxcdCAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJ0ZXh0LWRhbmdlclxcXCI+Kjwvc3Bhbj5cXG5cXHQgICAgICAgIDwvbGFiZWw+XFxuXFxuXFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtc20tMTBcXFwiPlxcblxcdCAgICAgICAgICAgIDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sIG0tdG9wLTBcXFwiIG5hbWU9XFxcIm5hbWVcXFwiIHYtbW9kZWw9XFxcImVkaXRBZGRyZXNzRm9ybS5uYW1lXFxcIj5cXG5cXHQgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5oYXMoJ25hbWUnKVxcXCIgdi10ZXh0PVxcXCJlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmdldCgnbmFtZScpXFxcIj48L3NwYW4+XFxuXFx0ICAgICAgICA8L2Rpdj5cXG5cXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnYWRkcmVzcycpIH1cXFwiPlxcblxcdCAgICAgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHQgICAgICAgICAgICBOdW1iZXJcXG5cXHQgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwidGV4dC1kYW5nZXJcXFwiPio8L3NwYW4+XFxuXFx0ICAgICAgICA8L2xhYmVsPlxcblxcblxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLXNtLTEwXFxcIj5cXG5cXHQgICAgICAgICAgICA8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbCBtLXRvcC0wXFxcIiBuYW1lPVxcXCJudW1iZXJcXFwiIHYtbW9kZWw9XFxcImVkaXRBZGRyZXNzRm9ybS5udW1iZXJcXFwiPlxcblxcdCAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnbnVtYmVyJylcXFwiIHYtdGV4dD1cXFwiZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5nZXQoJ251bWJlcicpXFxcIj48L3NwYW4+XFxuXFx0ICAgICAgICA8L2Rpdj5cXG5cXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnYWRkcmVzcycpIH1cXFwiPlxcblxcdCAgICAgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHQgICAgICAgICAgICBBZGRyZXNzXFxuXFx0ICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcInRleHQtZGFuZ2VyXFxcIj4qPC9zcGFuPlxcblxcdCAgICAgICAgPC9sYWJlbD5cXG5cXG5cXHQgICAgICAgIDxkaXYgY2xhc3M9XFxcImNvbC1zbS0xMFxcXCI+XFxuXFx0ICAgICAgICAgICAgPGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2wgbS10b3AtMFxcXCIgbmFtZT1cXFwiYWRkcmVzc1xcXCIgdi1tb2RlbD1cXFwiZWRpdEFkZHJlc3NGb3JtLmFkZHJlc3NcXFwiPlxcblxcdCAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJlZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnYWRkcmVzcycpXFxcIiB2LXRleHQ9XFxcImVkaXRBZGRyZXNzRm9ybS5lcnJvcnMuZ2V0KCdhZGRyZXNzJylcXFwiPjwvc3Bhbj5cXG5cXHQgICAgICAgIDwvZGl2PlxcblxcdCAgICA8L2Rpdj5cXG5cXG5cXHRcXHQ8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5FZGl0PC9idXR0b24+XFxuXFx0ICAgIDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBkYXRhLWRpc21pc3M9XFxcIm1vZGFsXFxcIj5DbG9zZTwvYnV0dG9uPlxcblxcblxcdDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFxuICAgIGV4cG9ydCBkZWZhdWx0IHtcXG5cXG4gICAgXFx0cHJvcHM6IFsnZ3JvdXBpZCddLFxcblxcbiAgICBcXHRkYXRhKCkge1xcbiAgICBcXHRcXHRyZXR1cm4ge1xcbiAgICBcXHRcXHRcXHRlZGl0QWRkcmVzc0Zvcm06IG5ldyBGb3JtKHtcXG5cXHRcXHRcXHRcXHRcXHRhZGRyZXNzOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHRudW1iZXI6ICcnLFxcblxcdFxcdFxcdFxcdFxcdG5hbWU6ICcnLFxcblxcdFxcdFxcdFxcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXFxuICAgIFxcdFxcdH1cXG4gICAgXFx0fSxcXG5cXG4gICAgXFx0bWV0aG9kczoge1xcblxcbiAgICBcXHRcXHRzZXRHcm91cEFkZHJlc3MoKSB7XFxuICAgIFxcdFxcdFxcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkKVxcblxcdFxcdFxcdFxcdC50aGVuKHJlc3BvbnNlID0+IHtcXHRcXHRcXHRcXHRcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRBZGRyZXNzRm9ybS5hZGRyZXNzID0gcmVzcG9uc2UuZGF0YS5hZGRyZXNzO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdEFkZHJlc3NGb3JtLm51bWJlciA9IHJlc3BvbnNlLmRhdGEudXNlci5hZGRyZXNzLmNvbnRhY3RfbnVtYmVyO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdEFkZHJlc3NGb3JtLm5hbWUgPSByZXNwb25zZS5kYXRhLm5hbWU7XFxuXFx0XFx0XFx0XFx0fSlcXG5cXHRcXHRcXHRcXHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcXG4gICAgXFx0XFx0fSxcXG5cXG4gICAgXFx0XFx0ZWRpdEFkZHJlc3MoKSB7XFxuICAgIFxcdFxcdFxcdHRoaXMuZWRpdEFkZHJlc3NGb3JtLnN1Ym1pdCgncGF0Y2gnLCAnL3Zpc2EvZ3JvdXAvJyArIHRoaXMuZ3JvdXBpZCArICcvZWRpdC1hZGRyZXNzJylcXG5cXHRcXHRcXHRcXHQudGhlbihyZXNwb25zZSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0aWYocmVzcG9uc2Uuc3VjY2Vzcykge1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmZldGNoRGV0YWlscygpO1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdCQoJyNlZGl0LWFkZHJlc3MtbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xcbiAgICBcXHRcXHR9XFxuXFxuICAgIFxcdH1cXG5cXG4gICAgfVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMjdhYjgyMzBcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0QWRkcmVzcy52dWVcbi8vIG1vZHVsZSBpZCA9IDI5N1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi0zOTI4ZmE5Y10ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTM5MjhmYTljXSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG4uY2hlY2tib3gtaW5saW5lW2RhdGEtdi0zOTI4ZmE5Y10ge1xcblxcdG1hcmdpbi10b3A6IC03cHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJBZGROZXdTZXJ2aWNlLnZ1ZT9hOTQ1MTg5OFwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBK2VBO0NBQ0Esb0JBQUE7Q0FDQTtBQUNBO0NBQ0EsbUJBQUE7Q0FDQTtBQUNBO0NBQ0EsaUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiQWRkTmV3U2VydmljZS52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwiYWRkTmV3U2VydmljZVxcXCI+XFxuXFxuXFx0XFx0PGRpdiB2LXNob3c9XFxcInNjcmVlbiA9PSAxXFxcIj5cXG5cXHQgICAgXFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBhZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdzZXJ2aWNlcycpIH1cXFwiPlxcblxcdFxcdCAgICAgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHQgICAgICAgICAgICBTZXJ2aWNlOlxcblxcdFxcdCAgICAgICAgPC9sYWJlbD5cXG5cXG5cXHRcXHQgICAgICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVxcXCJTZWxlY3QgU2VydmljZVxcXCIgY2xhc3M9XFxcImNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VcXFwiIG11bHRpcGxlIHN0eWxlPVxcXCJ3aWR0aDozNTBweDtcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj5cXG5cXHRcXHQgICAgICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cXFwic2VydmljZSBpbiBzZXJ2aWNlc0xpc3RcXFwiIDp2YWx1ZT1cXFwic2VydmljZS5pZFxcXCI+XFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHt7IChzZXJ2aWNlLmRldGFpbCkudHJpbSgpIH19XFxuXFx0XFx0ICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxcblxcdFxcdCAgICAgICAgICAgIDwvc2VsZWN0PlxcblxcblxcdFxcdCAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdzZXJ2aWNlcycpXFxcIiB2LXRleHQ9XFxcImFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5nZXQoJ3NlcnZpY2VzJylcXFwiPjwvc3Bhbj5cXG5cXG5cXHRcXHQgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJzcGFjZXItMTBcXFwiPjwvZGl2PlxcblxcblxcdFxcdCAgICAgICAgICAgIDxwPlxcblxcdFxcdCAgICAgICAgICAgIFxcdElmIHlvdSB3YW50IHRvIHZpZXcgYWxsIHNlcnZpY2VzIGF0IG9uY2UsIHBsZWFzZSBcXG5cXHRcXHQgICAgICAgICAgICBcXHQ8YSBocmVmPVxcXCIvdmlzYS9zZXJ2aWNlL3ZpZXctYWxsXFxcIiB0YXJnZXQ9XFxcIl9ibGFua1xcXCI+Y2xpY2sgaGVyZTwvYT5cXG5cXHRcXHQgICAgICAgICAgICA8L3A+XFxuXFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdCAgICBcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgICAgIE5vdGU6XFxuXFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHQgICAgICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJub3RlXFxcIiB2LW1vZGVsPVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5ub3RlXFxcIiBwbGFjZWhvbGRlcj1cXFwiZm9yIGludGVybmFsIHVzZSBvbmx5LCBvbmx5IGVtcGxveWVlcyBjYW4gc2VlLlxcXCI+XFxuXFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0ICAgIDxkaXYgdi1zaG93PVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcy5sZW5ndGggPD0gMVxcXCI+XFxuXFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdCAgICBcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgIENvc3Q6XFxuXFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHQgICAgICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJjb3N0XFxcIiA6dmFsdWU9XFxcImNvc3RcXFwiIHJlYWRvbmx5PlxcblxcdFxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcbjwhLS0gXFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdCAgICBcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgIFJlcG9ydCA6XFxuXFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHQgICAgICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJyZXBvcnRpbmdcXFwiIHYtbW9kZWw9XFxcImFkZE5ld1NlcnZpY2VGb3JtLnJlcG9ydGluZ1xcXCI+XFxuXFx0XFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHQgICAgPC9kaXY+IC0tPlxcblxcblxcdFxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiA6Y2xhc3M9XFxcInsgJ2hhcy1lcnJvcic6IGFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2Rpc2NvdW50JykgfVxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgRGlzY291bnQ6XFxuXFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTRcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgXFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImRpc2NvdW50XFxcIiB2LW1vZGVsPVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICBcXHQ8c3BhbiBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnZGlzY291bnQnKVxcXCIgdi10ZXh0PVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdkaXNjb3VudCcpXFxcIj48L3NwYW4+XFxuXFx0XFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHQgICAgICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCIgdi1pZj1cXFwiYWRkTmV3U2VydmljZUZvcm0uZGlzY291bnQhPScnXFxcIj5cXG5cXHRcXHRcXHQgICAgICAgICAgICBSZWFzb246XFxuXFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTRcXFwiIHYtaWY9XFxcImFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50IT0nJ1xcXCI+XFxuXFx0XFx0XFx0ICAgICAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwicmVhc29uXFxcIiB2LW1vZGVsPVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5yZWFzb25cXFwiPlxcblxcdFxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXG5cXHRcXHQgICAgPGEgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIiBAY2xpY2s9XFxcImNoYW5nZVNjcmVlbigyKVxcXCI+Q29udGludWU8L2E+XFxuXFx0ICAgIFxcdDxhIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9hPlxcbiAgICBcXHQ8L2Rpdj5cXG5cXG4gICAgXFx0PGRpdiB2LXNob3c9XFxcInNjcmVlbiA9PSAyXFxcIj5cXG4gICAgXFx0XFx0PGRpdiBjbGFzcz1cXFwidGFibGUtcmVzcG9uc2l2ZVxcXCI+XFxuICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzcz1cXFwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJlZCB0YWJsZS1ob3ZlciBhZGQtbmV3LXNlcnZpY2UtZGF0YXRhYmxlXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDx0aGVhZD5cXG5cXHQgICAgICAgICAgICAgICAgICAgIDx0cj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICA8dGggY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICBcXHQ8aW5wdXQgdHlwZT1cXFwiY2hlY2tib3hcXFwiIEBjbGljaz1cXFwidG9nZ2xlQWxsQ2hlY2tib3hcXFwiPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgIDwvdGg+XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgPHRoPk5hbWU8L3RoPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5DbGllbnQgSUQ8L3RoPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5QYWNrYWdlczwvdGg+XFxuXFx0ICAgICAgICAgICAgICAgICAgICA8L3RyPlxcbiAgICAgICAgICAgICAgICAgICAgPC90aGVhZD5cXG4gICAgICAgICAgICAgICAgICAgIDx0Ym9keT5cXG4gICAgICAgICAgICAgICAgICAgIFxcdDx0ciB2LWZvcj1cXFwiZGF0YSBpbiBjbGllbnRzXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIFxcdFxcdDx0ZCBjbGFzcz1cXFwidGV4dC1jZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgXFx0XFx0XFx0PGlucHV0IHR5cGU9XFxcImNoZWNrYm94XFxcIiA6dmFsdWU9XFxcImRhdGEuY2xpZW50LmlkXFxcIiB2LW1vZGVsPVxcXCJhZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIFxcdFxcdDwvdGQ+XFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHQ8dGQ+e3sgZGF0YS5jbGllbnQuZnVsbF9uYW1lIH19PC90ZD5cXG4gICAgICAgICAgICAgICAgICAgIFxcdFxcdDx0ZD57eyBkYXRhLmNsaWVudC5pZCB9fTwvdGQ+XFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHQ8dGQ+XFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHQ8c2VsZWN0IGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIDpjbGFzcz1cXFwiJ3BhY2thZ2Utc2VsZWN0LScgKyBkYXRhLmNsaWVudC5pZFxcXCIgXFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHQ6ZGlzYWJsZWQ9XFxcImFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHMuaW5kZXhPZihkYXRhLmNsaWVudC5pZCkgPT0gLTFcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgXFx0XFx0XFx0XFx0PG9wdGlvbiB2YWx1ZT1cXFwiR2VuZXJhdGUgbmV3IHBhY2thZ2VcXFwiPkdlbmVyYXRlIG5ldyBwYWNrYWdlPC9vcHRpb24+XFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHQ8b3B0aW9uIHYtZm9yPVxcXCJwYWNrYWdlIGluIGRhdGEuY2xpZW50LnBhY2thZ2VzXFxcIiA6dmFsdWU9XFxcInBhY2thZ2UudHJhY2tpbmdcXFwiIHYtaWY9XFxcInBhY2thZ2Uuc3RhdHVzICE9IDRcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgXFx0XFx0XFx0XFx0XFx0e3twYWNrYWdlLnRyYWNraW5nfX0gXFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHRcXHQoe3sgcGFja2FnZS5sb2dfZGF0ZSB8IGNvbW1vbkRhdGUgfX0pXFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHQ8L29wdGlvbj5cXG4gICAgICAgICAgICAgICAgICAgIFxcdFxcdFxcdDwvc2VsZWN0PlxcbiAgICAgICAgICAgICAgICAgICAgXFx0XFx0PC90ZD5cXG4gICAgICAgICAgICAgICAgICAgIFxcdDwvdHI+XFxuICAgICAgICAgICAgICAgICAgICA8L3Rib2R5PlxcbiAgICAgICAgICAgICAgICA8L3RhYmxlPlxcbiAgICAgICAgICAgIDwvZGl2PlxcblxcbiAgICAgICAgICAgIDxhIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCIgQGNsaWNrPVxcXCJjaGFuZ2VTY3JlZW4oMylcXFwiPkNvbnRpbnVlPC9hPlxcblxcdCAgICBcXHQ8YSBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBAY2xpY2s9XFxcImNoYW5nZVNjcmVlbigxKVxcXCI+QmFjazwvYT5cXG4gICAgXFx0PC9kaXY+XFxuXFxuICAgIFxcdDxkaXYgdi1zaG93PVxcXCJzY3JlZW4gPT0gM1xcXCI+XFxuICAgIFxcdFxcdDxkaXYgY2xhc3M9XFxcImlvcy1zY3JvbGxcXFwiPlxcbiAgICBcXHRcXHRcXHQ8bGFiZWwgY2xhc3M9XFxcImNoZWNrYm94LWlubGluZVxcXCI+XFxuICAgIFxcdFxcdFxcdFxcdDxpbnB1dCB0eXBlPVxcXCJjaGVja2JveFxcXCIgQGNsaWNrPVxcXCJjaGVja0FsbFxcXCIgY2xhc3M9XFxcImNoZWNrYm94LWFsbFxcXCI+IENoZWNrIGFsbCByZXF1aXJlZCBkb2N1bWVudHNcXG4gICAgXFx0XFx0XFx0PC9sYWJlbD5cXG5cXG4gICAgXFx0XFx0XFx0PGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjIwcHg7IGNsZWFyOmJvdGg7XFxcIj48L2Rpdj4gPCEtLSBzcGFjZXIgLS0+XFxuXFxuXFx0ICAgIFxcdFxcdDxkaXYgdi1mb3I9XFxcIihjbGllbnRJZCwgaW5kZXgxKSBpbiBhZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzXFxcIiBjbGFzcz1cXFwicGFuZWwgcGFuZWwtZGVmYXVsdFxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgXFx0PGRpdiBjbGFzcz1cXFwicGFuZWwtaGVhZGluZ1xcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICA8c3Ryb25nPnt7IGdldENsaWVudE5hbWUoY2xpZW50SWQpIH19PC9zdHJvbmc+XFxuXFx0ICAgICAgICAgICAgICAgIDwvZGl2PlxcblxcdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwYW5lbC1ib2R5XFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcblxcdCAgICAgICAgICAgICAgICBcXHQ8ZGl2IHYtZm9yPVxcXCIoc2VydmljZUlkLCBpbmRleDIpIGluIGFkZE5ld1NlcnZpY2VGb3JtLnNlcnZpY2VzXFxcIiBjbGFzcz1cXFwicGFuZWwgcGFuZWwtZGVmYXVsdFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgXFx0PGRpdiBjbGFzcz1cXFwicGFuZWwtaGVhZGluZ1xcXCI+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgICAgICB7eyBnZXRTZXJ2aWNlTmFtZShzZXJ2aWNlSWQpIH19XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XFxcImNoZWNrYm94LWlubGluZSBwdWxsLXJpZ2h0XFxcIj5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJjaGVja2JveFxcXCIgQGNsaWNrPVxcXCJjaGVja0FsbFxcXCIgOmNsYXNzPVxcXCInY2hlY2tib3gtJyArIGluZGV4MSArICctJyArIGluZGV4MlxcXCIgOmRhdGEtY2hvc2VuLWNsYXNzPVxcXCInY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcy0nICsgaW5kZXgxICsgJy0nICsgaW5kZXgyXFxcIj4gQ2hlY2sgYWxsIHJlcXVpcmVkIGRvY3VtZW50c1xcblxcdFxcdFxcdCAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInBhbmVsLWJvZHlcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICAgICAgXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICBcXHRSZXF1aXJlZCBEb2N1bWVudHM6XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVxcXCJTZWxlY3QgRG9jc1xcXCIgY2xhc3M9XFxcImNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3NcXFwiIDpjbGFzcz1cXFwiJ2Nob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MtJyArIGluZGV4MSArICctJyArIGluZGV4MlxcXCIgbXVsdGlwbGUgc3R5bGU9XFxcIndpZHRoOjM1MHB4O1xcXCIgdGFiaW5kZXg9XFxcIjRcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVxcXCJkb2MgaW4gcmVxdWlyZWREb2NzW2luZGV4Ml1cXFwiIDp2YWx1ZT1cXFwiZG9jLmlkXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0e3sgZG9jLnRpdGxlIH19XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgIDwvc2VsZWN0PlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgXFx0T3B0aW9uYWwgRG9jdW1lbnRzOlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICA8L2xhYmVsPlxcblxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgIDxzZWxlY3QgZGF0YS1wbGFjZWhvbGRlcj1cXFwiU2VsZWN0IERvY3NcXFwiIGNsYXNzPVxcXCJjaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzXFxcIiA6Y2xhc3M9XFxcIidjaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzLScgKyBpbmRleDEgKyAnLScgKyBpbmRleDJcXFwiIG11bHRpcGxlIHN0eWxlPVxcXCJ3aWR0aDozNTBweDtcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cXFwiZG9jIGluIG9wdGlvbmFsRG9jc1tpbmRleDJdXFxcIiA6dmFsdWU9XFxcImRvYy5pZFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdHt7IGRvYy50aXRsZSB9fVxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICA8L3NlbGVjdD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIDwvZGl2PiA8IS0tIFNFUlZJQ0UgcGFuZWwtYm9keSAtLT5cXG5cXHRcXHRcXHQgICAgICAgICAgICA8L2Rpdj4gPCEtLSBTRVJWSUNFIHBhbmVsIC0tPlxcblxcblxcdCAgICAgICAgICAgICAgICA8L2Rpdj4gPCEtLSBDTElFTlQgcGFuZWwtYm9keSAtLT5cXG5cXHQgICAgICAgICAgICA8L2Rpdj4gPCEtLSBDTElFTlQgcGFuZWwgLS0+XFxuICAgICAgICAgICAgPC9kaXY+XFxuXFxuICAgIFxcdFxcdDxidXR0b24gdi1pZj1cXFwiIWxvYWRpbmdcXFwiIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5TYXZlPC9idXR0b24+XFxuICAgIFxcdFxcdDxidXR0b24gdi1pZj1cXFwibG9hZGluZ1xcXCIgdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHRcXFwiPlxcblxcdCAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInNrLXNwaW5uZXIgc2stc3Bpbm5lci13YXZlXFxcIiBzdHlsZT1cXFwid2lkdGg6NDBweDsgaGVpZ2h0OjIwcHg7XFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic2stcmVjdDFcXFwiPjwvZGl2PlxcblxcdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJzay1yZWN0MlxcXCI+PC9kaXY+XFxuXFx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInNrLXJlY3QzXFxcIj48L2Rpdj5cXG5cXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic2stcmVjdDRcXFwiPjwvZGl2PlxcblxcdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJzay1yZWN0NVxcXCI+PC9kaXY+XFxuXFx0ICAgICAgICAgICAgIDwvZGl2PlxcblxcdCAgICAgICAgPC9idXR0b24+XFxuXFx0ICAgIFxcdDxhIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIEBjbGljaz1cXFwiY2hhbmdlU2NyZWVuKDIpXFxcIj5CYWNrPC9hPlxcbiAgICBcXHQ8L2Rpdj5cXG5cXG4gICAgPC9mb3JtPlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG4gICAgZXhwb3J0IGRlZmF1bHR7XFxuICAgIFxcdHByb3BzOiBbJ2dyb3VwaWQnXSxcXG5cXG4gICAgICAgIGRhdGEoKSB7XFxuICAgICAgICBcXHRyZXR1cm4ge1xcbiAgICAgICAgXFx0XFx0bG9hZGluZzogZmFsc2UsXFxuXFxuICAgICAgICBcXHRcXHRzY3JlZW46IDEsXFxuXFxuICAgICAgICBcXHRcXHRzZXJ2aWNlc0xpc3Q6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0Y29zdDogJycsXFxuXFxuICAgICAgICBcXHRcXHRjbGllbnRzOiBbXSxcXG5cXG4gICAgICAgIFxcdFxcdGRldGFpbEFycmF5OiBbXSxcXG4gICAgICAgIFxcdFxcdHJlcXVpcmVkRG9jczogW10sXFxuICAgICAgICBcXHRcXHRvcHRpb25hbERvY3M6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0YWRkTmV3U2VydmljZUZvcm06IG5ldyBGb3JtKHtcXG5cXHRcXHRcXHRcXHRcXHRzZXJ2aWNlczogW10sXFxuXFx0XFx0XFx0XFx0XFx0bm90ZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0ZGlzY291bnQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHJlYXNvbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0cmVwb3J0aW5nOiAnJyxcXG5cXG5cXHRcXHRcXHRcXHRcXHRjbGllbnRzOiBbXSxcXG5cXHRcXHRcXHRcXHRcXHRwYWNrYWdlczpbXSxcXG5cXHRcXHRcXHRcXHRcXHRkb2NzOiBbXVxcblxcdFxcdFxcdFxcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXFx0XFxuICAgICAgICBcXHR9XFxuICAgICAgICB9LFxcblxcbiAgICAgICAgbWV0aG9kczoge1xcbiAgICAgICAgXFx0ZmV0Y2hTZXJ2aWNlTGlzdCgpIHtcXG4gICAgICAgIFxcdFxcdGF4aW9zLmdldCgnL3Zpc2Evc2VydmljZS1tYW5hZ2VyL3Nob3cnKVxcblxcdFxcdFxcdFxcdFxcdC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLnNlcnZpY2VzTGlzdCA9IHJlc3BvbnNlLmRhdGEuZmlsdGVyKHIgPT4gci5wYXJlbnRfaWQgIT0gMCk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0JChcXFwiLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VcXFwiKS50cmlnZ2VyKFxcXCJjaG9zZW46dXBkYXRlZFxcXCIpO1xcblxcdFxcdFxcdFxcdFxcdFxcdH0sIDEwMDApO1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSApO1xcbiAgICAgICAgXFx0fSxcXG5cXG4gICAgICAgIFxcdHJlc2V0QWRkTmV3U2VydmljZUZvcm0oKSB7XFxuICAgICAgICBcXHRcXHR0aGlzLnNjcmVlbiA9IDE7XFxuXFxuICAgICAgICBcXHRcXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnNlcnZpY2VzID0gW107XFxuICAgICAgICBcXHRcXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLm5vdGUgPSAnJztcXG4gICAgICAgIFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uZGlzY291bnQgPSAnJztcXG4gICAgICAgIFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0ucmVhc29uID0gJyc7XFxuICAgICAgICBcXHRcXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnJlcG9ydGluZyA9ICcnO1xcbiAgICAgICAgXFx0XFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzID0gW107XFxuICAgICAgICBcXHRcXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnBhY2thZ2VzID0gW107XFxuICAgICAgICBcXHRcXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLmRvY3MgPSBbXTtcXG5cXG4gICAgICAgIFxcdFxcdHRoaXMuY29zdCA9ICcnO1xcbiAgICAgICAgXFx0XFx0Ly8gRGVzZWxlY3QgQWxsXFxuXFx0XFx0XFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2UsIC5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcXG5cXHRcXHRcXHRcXHRcXG5cXHRcXHRcXHRcXHR0aGlzLmRldGFpbEFycmF5ID0gW107XFxuXFx0XFx0XFx0XFx0dGhpcy5yZXF1aXJlZERvY3MgPSBbXTtcXG5cXHRcXHRcXHRcXHR0aGlzLm9wdGlvbmFsRG9jcyA9IFtdO1xcbiAgICAgICAgXFx0fSxcXG5cXG4gICAgICAgIFxcdGFkZE5ld1NlcnZpY2UoKSB7XFxuICAgICAgICBcXHRcXHR0aGlzLmxvYWRpbmcgPSB0cnVlO1xcblxcbiAgICAgICAgXFx0XFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5wYWNrYWdlcyA9IFtdO1xcblxcdFxcdFxcdFxcdHZhciB0YmwgPSAkKCcuYWRkLW5ldy1zZXJ2aWNlLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpO1xcblxcdCAgICAgICAgXFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzLmZvckVhY2goKGMsIGluZGV4KSA9PiB7XFxuXFx0ICAgICAgICBcXHRcXHRsZXQgdmFsdWUgPSB0YmwuJCgnc2VsZWN0LnBhY2thZ2Utc2VsZWN0LScgKyBjKS52YWwoKTtcXG5cXHQgICAgICAgIFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0ucGFja2FnZXMucHVzaCh2YWx1ZSk7XFxuXFx0ICAgICAgICBcXHR9KTtcXG5cXG5cXHQgICAgICAgIFxcdHZhciBkb2NzQXJyYXkgPSBbXTtcXG4gICAgICAgIFxcdFxcdHZhciByZXF1aXJlZERvY3MgPSAnJztcXG4gICAgICAgIFxcdFxcdHZhciBvcHRpb25hbERvY3MgPSAnJztcXG4gICAgICAgIFxcdFxcdHZhciBoYXNSZXF1aXJlZERvY3NFcnJvcnMgPSBbXTtcXG5cXG5cXHQgICAgICAgIFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cy5mb3JFYWNoKChjbGllbnQsIGluZGV4MSkgPT4ge1xcblxcdCAgICAgICAgXFx0XFx0dGhpcy5hZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcy5mb3JFYWNoKChzZXJ2aWNlLCBpbmRleDIpID0+IHtcXG5cXHQgICAgICAgIFxcdFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpbmRleDEgKyAnLScgKyBpbmRleDIgKycgb3B0aW9uOnNlbGVjdGVkJykuZWFjaChmdW5jdGlvbigpIHtcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdHJlcXVpcmVkRG9jcyArPSAkKHRoaXMpLnZhbCgpICsgJywnO1xcblxcdFxcdFxcdFxcdFxcdCAgICB9KTtcXG5cXG5cXHQgICAgICAgIFxcdFxcdFxcdGxldCByZXF1aXJlZE9wdGlvbnMgPSAkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcy0nICsgaW5kZXgxICsgJy0nICsgaW5kZXgyICsnIG9wdGlvbicpLmxlbmd0aDtcXG5cXHRcXHRcXHRcXHRcXHQgICAgbGV0IHJlcXVpcmVkQ291bnQgPSAkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcy0nICsgaW5kZXgxICsgJy0nICsgaW5kZXgyICsnIG9wdGlvbjpzZWxlY3RlZCcpLmxlbmd0aDtcXG5cXHQgICAgICAgIFxcdFxcdFxcdGlmKHJlcXVpcmVkQ291bnQgPT0gMCAmJiByZXF1aXJlZE9wdGlvbnMgPiAwKSB7XFxuXFx0ICAgICAgICBcXHRcXHRcXHRcXHRoYXNSZXF1aXJlZERvY3NFcnJvcnMucHVzaCh7XFxuXFx0ICAgICAgICBcXHRcXHRcXHRcXHRcXHRjbGllbnQ6IHRoaXMuZ2V0Q2xpZW50TmFtZShjbGllbnQpLFxcblxcdCAgICAgICAgXFx0XFx0XFx0XFx0XFx0c2VydmljZTogdGhpcy5nZXRTZXJ2aWNlTmFtZShzZXJ2aWNlKVxcblxcdCAgICAgICAgXFx0XFx0XFx0XFx0fSk7XFxuXFx0ICAgICAgICBcXHRcXHRcXHR9XFxuXFxuXFx0XFx0XFx0XFx0XFx0ICAgICQoJy5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzLScgKyBpbmRleDEgKyAnLScgKyBpbmRleDIgKyAnIG9wdGlvbjpzZWxlY3RlZCcpLmVhY2goZnVuY3Rpb24oKSB7XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICBvcHRpb25hbERvY3MgKz0gJCh0aGlzKS52YWwoKSArICcsJztcXG5cXHRcXHRcXHRcXHRcXHQgICAgfSk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0ICAgIGRvY3NBcnJheS5wdXNoKChyZXF1aXJlZERvY3MgKyBvcHRpb25hbERvY3MpLnNsaWNlKDAsIC0xKSk7XFxuXFx0XFx0XFx0XFx0ICAgIFxcdHJlcXVpcmVkRG9jcyA9ICcnO1xcblxcdFxcdFxcdFxcdCAgICBcXHRvcHRpb25hbERvY3MgPSAnJztcXG5cXHQgICAgICAgIFxcdFxcdH0pO1xcblxcdCAgICAgICAgXFx0fSk7XFxuICAgICAgICBcXHRcXHRcXG4gICAgICAgIFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uZG9jcyA9IGRvY3NBcnJheTtcXG5cXG4gICAgICAgIFxcdFxcdGlmKGhhc1JlcXVpcmVkRG9jc0Vycm9ycy5sZW5ndGggPiAwKXtcXG4gICAgICAgIFxcdFxcdFxcdGZvcihsZXQgaT0wOyBpPGhhc1JlcXVpcmVkRG9jc0Vycm9ycy5sZW5ndGg7IGkrKykge1xcbiAgICAgICAgXFx0XFx0XFx0XFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGZyb20gcmVxdWlyZWQgZG9jdW1lbnRzLicsIGhhc1JlcXVpcmVkRG9jc0Vycm9yc1tpXS5jbGllbnQgKyAnIC0gJyArIGhhc1JlcXVpcmVkRG9jc0Vycm9yc1tpXS5zZXJ2aWNlKTtcXG4gICAgICAgIFxcdFxcdFxcdH1cXG4gICAgICAgIFxcdFxcdFxcdHRoaXMubG9hZGluZyA9IGZhbHNlO1xcbiAgICAgICAgIFxcdFxcdH0gZWxzZSB7XFxuXFxuXFx0XFx0ICAgICAgICBcXHR0aGlzLmFkZE5ld1NlcnZpY2VGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkICsgJy9hZGQtc2VydmljZScpXFxuXFx0XFx0XFx0ICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmZldGNoRGV0YWlscygpO1xcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5mZXRjaE1lbWJlcnMoKTtcXG5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0dGhpcy5mZXRjaEdyb3VwUGFja2FnZXNTZXJ2aWNlc0xpc3QoKTtcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0dGhpcy5yZXNldEFkZE5ld1NlcnZpY2VGb3JtKCk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdCQoJyNhZGQtbmV3LXNlcnZpY2UtbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xcblxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHR0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5tZXNzYWdlKTtcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgfVxcblxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcXG5cXHRcXHRcXHQgICAgICAgICAgICB9KVxcblxcdFxcdFxcdCAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0Zm9yKHZhciBrZXkgaW4gZXJyb3IpIHtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHRpZihlcnJvci5oYXNPd25Qcm9wZXJ0eShrZXkpKSB0b2FzdHIuZXJyb3IoZXJyb3Jba2V5XVswXSlcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHR9XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0dGhpcy5sb2FkaW5nID0gZmFsc2U7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgfSk7XFxuXFx0XFx0XFx0ICAgIH1cXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGdldERvY3Moc2VydmljZXNJZCkge1xcblxcdFxcdFxcdFxcdGF4aW9zLmdldCgnL3Zpc2EvZ2V0LWRvY3MnLCB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0cGFyYW1zOiB7XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0c2VydmljZXNJZDogc2VydmljZXNJZFxcblxcdFxcdFxcdFxcdFxcdCAgICB9XFxuXFx0XFx0XFx0XFx0XFx0fSlcXG5cXHRcXHRcXHRcXHRcXHQudGhlbihyZXN1bHQgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuZGV0YWlsQXJyYXkgPSByZXN1bHQuZGF0YS5kZXRhaWxBcnJheTtcXG5cXHRcXHRcXHRcXHQgICAgICAgIHRoaXMucmVxdWlyZWREb2NzID0gcmVzdWx0LmRhdGEuZG9jc05lZWRlZEFycmF5O1xcblxcdFxcdFxcdFxcdCAgICAgICAgdGhpcy5vcHRpb25hbERvY3MgPSByZXN1bHQuZGF0YS5kb2NzT3B0aW9uYWxBcnJheTtcXG5cXHRcXHRcXHRcXHRcXHR9KTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGdldENsaWVudE5hbWUoY2xpZW50SWQpIHtcXG5cXHRcXHRcXHRcXHR2YXIgY2xpZW50TmFtZSA9ICcnO1xcblxcdFxcdFxcdFxcdHRoaXMuY2xpZW50cy5mb3JFYWNoKGNsaWVudCA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0aWYoY2xpZW50LmNsaWVudC5pZCA9PSBjbGllbnRJZCkge1xcblxcdFxcdFxcdFxcdFxcdFxcdGNsaWVudE5hbWUgPSBjbGllbnQuY2xpZW50LmZpcnN0X25hbWUgKyAnICcgKyBjbGllbnQuY2xpZW50Lmxhc3RfbmFtZTsgXFxuXFx0XFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdFxcdH0pO1xcblxcblxcdFxcdFxcdFxcdHJldHVybiBjbGllbnROYW1lO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0Z2V0U2VydmljZU5hbWUoc2VydmljZUlkKSB7XFxuXFx0XFx0XFx0XFx0dmFyIHNlcnZpY2VOYW1lID0gJyc7XFxuXFx0XFx0XFx0XFx0dGhpcy5zZXJ2aWNlc0xpc3QuZm9yRWFjaChzZXJ2aWNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRpZihzZXJ2aWNlLmlkID09IHNlcnZpY2VJZCkge1xcblxcdFxcdFxcdFxcdFxcdFxcdHNlcnZpY2VOYW1lID0gc2VydmljZS5kZXRhaWw7IFxcblxcdFxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHR9KTtcXG5cXG5cXHRcXHRcXHRcXHRyZXR1cm4gc2VydmljZU5hbWU7XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRpbml0Q2hvc2VuU2VsZWN0KCkge1xcblxcdFxcdFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlJykuY2hvc2VuKHtcXG5cXHRcXHRcXHRcXHRcXHR3aWR0aDogXFxcIjEwMCVcXFwiLFxcblxcdFxcdFxcdFxcdFxcdG5vX3Jlc3VsdHNfdGV4dDogXFxcIk5vIHJlc3VsdC9zIGZvdW5kLlxcXCIsXFxuXFx0XFx0XFx0XFx0XFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0XFx0bGV0IHZtID0gdGhpcztcXG5cXG5cXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZScpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcXG5cXHRcXHRcXHRcXHRcXHRsZXQgc2VydmljZXMgPSAkKHRoaXMpLnZhbCgpO1xcblxcblxcdFxcdFxcdFxcdFxcdHZtLmdldERvY3Moc2VydmljZXMpO1xcblxcblxcdFxcdFxcdFxcdFxcdHZtLmFkZE5ld1NlcnZpY2VGb3JtLnNlcnZpY2VzID0gc2VydmljZXM7XFxuXFxuXFx0XFx0XFx0XFx0XFx0Ly8gU2V0IGNvc3QgaW5wdXRcXG5cXHRcXHRcXHRcXHRcXHRpZihzZXJ2aWNlcy5sZW5ndGggPT0gMSkge1xcblxcdFxcdFxcdFxcdFxcdFxcdHZtLnNlcnZpY2VzTGlzdC5tYXAocyA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0aWYocy5pZCA9PSBzZXJ2aWNlc1swXSkge1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdHZtLmNvc3QgPSBzLmNvc3Q7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdFxcdFxcdFxcdH0pO1xcblxcdFxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dm0uY29zdCA9ICcnO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXG5cXHRcXHRcXHRcXHR9KTtcXG5cXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGNoYW5nZVNjcmVlbihzY3JlZW4pIHtcXG5cXHRcXHRcXHRcXHRpZihzY3JlZW4gPT0gMikge1xcblxcdFxcdFxcdFxcdFxcdGlmKHRoaXMuYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMubGVuZ3RoID09IDApIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgYXQgbGVhc3Qgb25lIHNlcnZpY2UnKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRyZXR1cm4gZmFsc2U7XFxuXFx0XFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdFxcdH0gZWxzZSBpZihzY3JlZW4gPT0gMykge1xcblxcdFxcdFxcdFxcdFxcdGlmKHRoaXMuYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cy5sZW5ndGggPT0gMCkge1xcblxcdCAgICAgICAgXFx0XFx0XFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGF0IGxlYXN0IG9uZSBtZW1iZXInKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRyZXR1cm4gZmFsc2U7XFxuXFx0ICAgICAgICBcXHRcXHR9XFxuXFxuXFx0ICAgICAgICBcXHRcXHRzZXRUaW1lb3V0KCgpID0+IHtcXG5cXHRcXHRcXHRcXHQgICAgICAgICQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLmNob3NlbignZGVzdHJveScpO1xcblxcdFxcdFxcdFxcdCAgICAgICAgJCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykuY2hvc2VuKHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHR3aWR0aDogXFxcIjEwMCVcXFwiLFxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdG5vX3Jlc3VsdHNfdGV4dDogXFxcIk5vIHJlc3VsdC9zIGZvdW5kLlxcXCIsXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXFxuXFx0XFx0XFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICBcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcdFxcdFxcdCAgICAgICAgfSwgMTAwMCk7XFxuXFx0XFx0XFx0XFx0fVxcblxcblxcdFxcdFxcdFxcdHRoaXMuc2NyZWVuID0gc2NyZWVuO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0ZmV0Y2hHcm91cFBhY2thZ2VzU2VydmljZXNMaXN0KCkge1xcblxcdFxcdFxcdFxcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJysgdGhpcy5ncm91cGlkICsnL3BhY2thZ2VzLXNlcnZpY2VzJylcXG5cXHRcXHRcXHRcXHRcXHQudGhlbihyZXNwb25zZSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5jbGllbnRzID0gcmVzcG9uc2UuZGF0YTtcXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmNhbGxEYXRhVGFibGUoKTtcXG5cXHRcXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdFxcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikgKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGNhbGxEYXRhVGFibGUoKSB7XFxuXFx0XFx0XFx0XFx0JCgnLmFkZC1uZXctc2VydmljZS1kYXRhdGFibGUnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XFxuXFxuXFx0XFx0XFx0XFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcXG5cXHRcXHRcXHRcXHRcXHQkKCcuYWRkLW5ldy1zZXJ2aWNlLWRhdGF0YWJsZScpLkRhdGFUYWJsZSh7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgcGFnZUxlbmd0aDogMTAsXFxuXFx0XFx0XFx0ICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcXG5cXHRcXHRcXHQgICAgICAgICAgICBkb206ICc8XFxcInRvcFxcXCJsZj5ydDxcXFwiYm90dG9tXFxcImlwPjxcXFwiY2xlYXJcXFwiPidcXG5cXHRcXHRcXHQgICAgICAgIH0pO1xcblxcdFxcdFxcdFxcdH0sIDEwMDApO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0dG9nZ2xlQWxsQ2hlY2tib3goZSkge1xcblxcdFxcdFxcdFxcdGlmKGUudGFyZ2V0LmNoZWNrZWQgPT0gdHJ1ZSkge1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cyA9IFtdO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuY2xpZW50cy5mb3JFYWNoKChjLCBpbmRleCkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cy5wdXNoKGMuY2xpZW50LmlkKTtcXG5cXHRcXHRcXHRcXHRcXHR9KTtcXG5cXHRcXHRcXHRcXHR9IGVsc2Uge1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cyA9IFtdO1xcblxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGNoZWNrQWxsKGUpIHtcXG5cXHRcXHRcXHRcXHRsZXQgY2hlY2tib3ggPSBlLnRhcmdldC5jbGFzc05hbWU7XFxuXFxuXFx0XFx0XFx0XFx0aWYoY2hlY2tib3ggPT0gJ2NoZWNrYm94LWFsbCcpIHtcXG5cXHRcXHRcXHRcXHRcXHRpZigkKCcuJyArIGNoZWNrYm94KS5pcygnOmNoZWNrZWQnKSlcXG5cXHRcXHRcXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIHRydWUpO1xcblxcdFxcdFxcdFxcdFxcdGVsc2VcXG5cXHRcXHRcXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIGZhbHNlKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb24nKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0bGV0IGNob3NlbiA9IGUudGFyZ2V0LmRhdGFzZXQuY2hvc2VuQ2xhc3M7XFxuXFxuXFx0XFx0XFx0XFx0XFx0aWYoJCgnLicgKyBjaGVja2JveCkuaXMoJzpjaGVja2VkJykpXFxuXFx0XFx0XFx0XFx0XFx0XFx0JCgnLicgKyBjaG9zZW4gKyAnIG9wdGlvbicpLnByb3AoJ3NlbGVjdGVkJywgdHJ1ZSk7XFxuXFx0XFx0XFx0XFx0XFx0ZWxzZVxcblxcdFxcdFxcdFxcdFxcdFxcdCQoJy4nICsgY2hvc2VuICsgJyBvcHRpb24nKS5wcm9wKCdzZWxlY3RlZCcsIGZhbHNlKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHQkKCcuJyArIGNob3NlbiArICcgb3B0aW9uJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcXG5cXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0fVxcbiAgICAgICAgfSxcXG5cXG4gICAgICAgIGNyZWF0ZWQoKSB7XFxuICAgICAgICBcXHR0aGlzLmZldGNoU2VydmljZUxpc3QoKTtcXG4gICAgICAgIFxcdHRoaXMuZmV0Y2hHcm91cFBhY2thZ2VzU2VydmljZXNMaXN0KCk7XFxuICAgICAgICB9LFxcblxcbiAgICAgICAgbW91bnRlZCgpIHtcXG4gICAgICAgIFxcdHRoaXMuaW5pdENob3NlblNlbGVjdCgpO1xcbiAgICAgICAgfVxcbiAgICB9XFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcblxcdC5jaGVja2JveC1pbmxpbmUge1xcblxcdFxcdG1hcmdpbi10b3A6IC03cHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zOTI4ZmE5Y1wiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZE5ld1NlcnZpY2UudnVlXG4vLyBtb2R1bGUgaWQgPSAyOThcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuICBNb2RpZmllZCBieSBFdmFuIFlvdSBAeXl4OTkwODAzXG4qL1xuXG52YXIgaGFzRG9jdW1lbnQgPSB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG5cbmlmICh0eXBlb2YgREVCVUcgIT09ICd1bmRlZmluZWQnICYmIERFQlVHKSB7XG4gIGlmICghaGFzRG9jdW1lbnQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgJ3Z1ZS1zdHlsZS1sb2FkZXIgY2Fubm90IGJlIHVzZWQgaW4gYSBub24tYnJvd3NlciBlbnZpcm9ubWVudC4gJyArXG4gICAgXCJVc2UgeyB0YXJnZXQ6ICdub2RlJyB9IGluIHlvdXIgV2VicGFjayBjb25maWcgdG8gaW5kaWNhdGUgYSBzZXJ2ZXItcmVuZGVyaW5nIGVudmlyb25tZW50LlwiXG4gICkgfVxufVxuXG52YXIgbGlzdFRvU3R5bGVzID0gcmVxdWlyZSgnLi9saXN0VG9TdHlsZXMnKVxuXG4vKlxudHlwZSBTdHlsZU9iamVjdCA9IHtcbiAgaWQ6IG51bWJlcjtcbiAgcGFydHM6IEFycmF5PFN0eWxlT2JqZWN0UGFydD5cbn1cblxudHlwZSBTdHlsZU9iamVjdFBhcnQgPSB7XG4gIGNzczogc3RyaW5nO1xuICBtZWRpYTogc3RyaW5nO1xuICBzb3VyY2VNYXA6ID9zdHJpbmdcbn1cbiovXG5cbnZhciBzdHlsZXNJbkRvbSA9IHsvKlxuICBbaWQ6IG51bWJlcl06IHtcbiAgICBpZDogbnVtYmVyLFxuICAgIHJlZnM6IG51bWJlcixcbiAgICBwYXJ0czogQXJyYXk8KG9iaj86IFN0eWxlT2JqZWN0UGFydCkgPT4gdm9pZD5cbiAgfVxuKi99XG5cbnZhciBoZWFkID0gaGFzRG9jdW1lbnQgJiYgKGRvY3VtZW50LmhlYWQgfHwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXSlcbnZhciBzaW5nbGV0b25FbGVtZW50ID0gbnVsbFxudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwXG52YXIgaXNQcm9kdWN0aW9uID0gZmFsc2VcbnZhciBub29wID0gZnVuY3Rpb24gKCkge31cblxuLy8gRm9yY2Ugc2luZ2xlLXRhZyBzb2x1dGlvbiBvbiBJRTYtOSwgd2hpY2ggaGFzIGEgaGFyZCBsaW1pdCBvbiB0aGUgIyBvZiA8c3R5bGU+XG4vLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG52YXIgaXNPbGRJRSA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIC9tc2llIFs2LTldXFxiLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSlcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAocGFyZW50SWQsIGxpc3QsIF9pc1Byb2R1Y3Rpb24pIHtcbiAgaXNQcm9kdWN0aW9uID0gX2lzUHJvZHVjdGlvblxuXG4gIHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIGxpc3QpXG4gIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG4gICAgdmFyIG1heVJlbW92ZSA9IFtdXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgICAgZG9tU3R5bGUucmVmcy0tXG4gICAgICBtYXlSZW1vdmUucHVzaChkb21TdHlsZSlcbiAgICB9XG4gICAgaWYgKG5ld0xpc3QpIHtcbiAgICAgIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhwYXJlbnRJZCwgbmV3TGlzdClcbiAgICAgIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVzID0gW11cbiAgICB9XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXVxuICAgICAgaWYgKGRvbVN0eWxlLnJlZnMgPT09IDApIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKClcbiAgICAgICAgfVxuICAgICAgICBkZWxldGUgc3R5bGVzSW5Eb21bZG9tU3R5bGUuaWRdXG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlc1RvRG9tIChzdHlsZXMgLyogQXJyYXk8U3R5bGVPYmplY3Q+ICovKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBzdHlsZXNbaV1cbiAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgZG9tU3R5bGUucmVmcysrXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pXG4gICAgICB9XG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdKSlcbiAgICAgIH1cbiAgICAgIGlmIChkb21TdHlsZS5wYXJ0cy5sZW5ndGggPiBpdGVtLnBhcnRzLmxlbmd0aCkge1xuICAgICAgICBkb21TdHlsZS5wYXJ0cy5sZW5ndGggPSBpdGVtLnBhcnRzLmxlbmd0aFxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgcGFydHMgPSBbXVxuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIHBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBzdHlsZXNJbkRvbVtpdGVtLmlkXSA9IHsgaWQ6IGl0ZW0uaWQsIHJlZnM6IDEsIHBhcnRzOiBwYXJ0cyB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVN0eWxlRWxlbWVudCAoKSB7XG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpXG4gIHN0eWxlRWxlbWVudC50eXBlID0gJ3RleHQvY3NzJ1xuICBoZWFkLmFwcGVuZENoaWxkKHN0eWxlRWxlbWVudClcbiAgcmV0dXJuIHN0eWxlRWxlbWVudFxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZSAob2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICB2YXIgdXBkYXRlLCByZW1vdmVcbiAgdmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3N0eWxlW2RhdGEtdnVlLXNzci1pZH49XCInICsgb2JqLmlkICsgJ1wiXScpXG5cbiAgaWYgKHN0eWxlRWxlbWVudCkge1xuICAgIGlmIChpc1Byb2R1Y3Rpb24pIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGFuZCBpbiBwcm9kdWN0aW9uIG1vZGUuXG4gICAgICAvLyBzaW1wbHkgZG8gbm90aGluZy5cbiAgICAgIHJldHVybiBub29wXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGJ1dCBpbiBkZXYgbW9kZS5cbiAgICAgIC8vIGZvciBzb21lIHJlYXNvbiBDaHJvbWUgY2FuJ3QgaGFuZGxlIHNvdXJjZSBtYXAgaW4gc2VydmVyLXJlbmRlcmVkXG4gICAgICAvLyBzdHlsZSB0YWdzIC0gc291cmNlIG1hcHMgaW4gPHN0eWxlPiBvbmx5IHdvcmtzIGlmIHRoZSBzdHlsZSB0YWcgaXNcbiAgICAgIC8vIGNyZWF0ZWQgYW5kIGluc2VydGVkIGR5bmFtaWNhbGx5LiBTbyB3ZSByZW1vdmUgdGhlIHNlcnZlciByZW5kZXJlZFxuICAgICAgLy8gc3R5bGVzIGFuZCBpbmplY3QgbmV3IG9uZXMuXG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgaWYgKGlzT2xkSUUpIHtcbiAgICAvLyB1c2Ugc2luZ2xldG9uIG1vZGUgZm9yIElFOS5cbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrK1xuICAgIHN0eWxlRWxlbWVudCA9IHNpbmdsZXRvbkVsZW1lbnQgfHwgKHNpbmdsZXRvbkVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKSlcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCBmYWxzZSlcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCB0cnVlKVxuICB9IGVsc2Uge1xuICAgIC8vIHVzZSBtdWx0aS1zdHlsZS10YWcgbW9kZSBpbiBhbGwgb3RoZXIgY2FzZXNcbiAgICBzdHlsZUVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQpXG4gICAgcmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmopXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmogLyogU3R5bGVPYmplY3RQYXJ0ICovKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcbiAgICAgICAgICBuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJlxuICAgICAgICAgIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKVxuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKVxuICAgIH1cbiAgfVxufVxuXG52YXIgcmVwbGFjZVRleHQgPSAoZnVuY3Rpb24gKCkge1xuICB2YXIgdGV4dFN0b3JlID0gW11cblxuICByZXR1cm4gZnVuY3Rpb24gKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudFxuICAgIHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpXG4gIH1cbn0pKClcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyAoc3R5bGVFbGVtZW50LCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcbiAgdmFyIGNzcyA9IHJlbW92ZSA/ICcnIDogb2JqLmNzc1xuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKVxuICB9IGVsc2Uge1xuICAgIHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKVxuICAgIHZhciBjaGlsZE5vZGVzID0gc3R5bGVFbGVtZW50LmNoaWxkTm9kZXNcbiAgICBpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSlcbiAgICBpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChjc3NOb2RlKVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRvVGFnIChzdHlsZUVsZW1lbnQsIG9iaikge1xuICB2YXIgY3NzID0gb2JqLmNzc1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWFcbiAgdmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXBcblxuICBpZiAobWVkaWEpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKCdtZWRpYScsIG1lZGlhKVxuICB9XG5cbiAgaWYgKHNvdXJjZU1hcCkge1xuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLmNocm9tZS5jb20vZGV2dG9vbHMvZG9jcy9qYXZhc2NyaXB0LWRlYnVnZ2luZ1xuICAgIC8vIHRoaXMgbWFrZXMgc291cmNlIG1hcHMgaW5zaWRlIHN0eWxlIHRhZ3Mgd29yayBwcm9wZXJseSBpbiBDaHJvbWVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VVUkw9JyArIHNvdXJjZU1hcC5zb3VyY2VzWzBdICsgJyAqLydcbiAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNjYwMzg3NVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCwnICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArICcgKi8nXG4gIH1cblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzXG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpXG4gICAgfVxuICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKVxuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9UcmFuc2FjdGlvbkxvZ3MudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi02YWVmYmZhNlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9UcmFuc2FjdGlvbkxvZ3MudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvVHJhbnNhY3Rpb25Mb2dzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFRyYW5zYWN0aW9uTG9ncy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNmFlZmJmYTZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi02YWVmYmZhNlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvVHJhbnNhY3Rpb25Mb2dzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzBcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi0zY2E5NGJlZl0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTNjYTk0YmVmXSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiRGVsZXRlTWVtYmVyLnZ1ZT83MWFkNTNiOFwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBOERBO0NBQ0Esb0JBQUE7Q0FDQTtBQUNBO0NBQ0EsbUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiRGVsZXRlTWVtYmVyLnZ1ZVwiLFwic291cmNlc0NvbnRlbnRcIjpbXCI8dGVtcGxhdGU+XFxuXFxuXFx0PGZvcm0gY2xhc3M9XFxcImZvcm0taG9yaXpvbnRhbFxcXCIgQHN1Ym1pdC5wcmV2ZW50PVxcXCJkZWxldGVNZW1iZXJcXFwiPlxcblxcblxcdFxcdDxwPkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhpcyBtZW1iZXI/PC9wPlxcblxcblxcdFxcdDxidXR0b24gdHlwZT1cXFwic3VibWl0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcXFwiPkRlbGV0ZTwvYnV0dG9uPlxcblxcdCAgICA8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFxcXCIgZGF0YS1kaXNtaXNzPVxcXCJtb2RhbFxcXCI+Q2xvc2U8L2J1dHRvbj5cXG5cXG5cXHQ8L2Zvcm0+XFxuXFxuPC90ZW1wbGF0ZT5cXG5cXG48c2NyaXB0PlxcblxcbiAgICBleHBvcnQgZGVmYXVsdCB7XFxuXFxuICAgIFxcdHByb3BzOiBbJ2dyb3VwaWQnXSxcXG5cXG4gICAgXFx0ZGF0YSgpIHtcXG4gICAgXFx0XFx0cmV0dXJuIHtcXG4gICAgXFx0XFx0XFx0ZGVsZXRlTWVtYmVyRm9ybTogbmV3IEZvcm0oe1xcblxcdFxcdFxcdFxcdFxcdG1lbWJlcklkOiBudWxsXFxuXFx0XFx0XFx0XFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSlcXG4gICAgXFx0XFx0fVxcbiAgICBcXHR9LFxcblxcbiAgICBcXHRtZXRob2RzOiB7XFxuXFxuICAgIFxcdFxcdHNldE1lbWJlcklkKGlkKSB7XFxuICAgIFxcdFxcdFxcdHRoaXMuZGVsZXRlTWVtYmVyRm9ybS5tZW1iZXJJZCA9IGlkO1xcbiAgICBcXHRcXHR9LFxcblxcbiAgICBcXHRcXHRyZXNldERlbGV0ZU1lbWJlckZvcm0oKSB7XFxuICAgIFxcdFxcdFxcdHRoaXMuZGVsZXRlTWVtYmVyRm9ybS5tZW1iZXJJZCA9IG51bGw7XFxuICAgIFxcdFxcdH0sXFxuXFxuICAgIFxcdFxcdGRlbGV0ZU1lbWJlcigpIHtcXG4gICAgXFx0XFx0XFx0dGhpcy5kZWxldGVNZW1iZXJGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkICsgJy9kZWxldGUtbWVtYmVyJylcXG5cXHRcXHQgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHQgICAgICAgIFxcdHRoaXMucmVzZXREZWxldGVNZW1iZXJGb3JtKCk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgJCgnI2RlbGV0ZS1tZW1iZXItbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xcblxcblxcdFxcdCAgICAgICAgICAgIGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcXG5cXHRcXHQgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hNZW1iZXJzKCk7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcblxcdFxcdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5tZXNzYWdlKTtcXG5cXHRcXHQgICAgICAgICAgICB9IGVsc2Uge1xcblxcdFxcdCAgICAgICAgICAgIFxcdHRvYXN0ci5lcnJvcihyZXNwb25zZS5tZXNzYWdlKTtcXG5cXHRcXHQgICAgICAgICAgICB9XFxuXFx0XFx0ICAgICAgICB9KVxcblxcdFxcdCAgICAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XFxuICAgIFxcdFxcdH1cXG5cXG4gICAgXFx0fVxcblxcbiAgICB9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zY2E5NGJlZlwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0RlbGV0ZU1lbWJlci52dWVcbi8vIG1vZHVsZSBpZCA9IDMwMVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi01MTk5YThhOF0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTUxOTlhOGE4XSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiQWRkRnVuZC52dWU/M2E0OTcwYzZcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQWdHQTtDQUNBLG9CQUFBO0NBQ0E7QUFDQTtDQUNBLG1CQUFBO0NBQ0FcIixcImZpbGVcIjpcIkFkZEZ1bmQudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXG5cXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIiBAc3VibWl0LnByZXZlbnQ9XFxcImFkZEZ1bmRzXFxcIj5cXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHQgICAgICAgIE9wdGlvbjpcXG5cXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0ICAgIFxcdDxzZWxlY3QgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwib3B0aW9uXFxcIiB2LW1vZGVsPVxcXCJhZGRGdW5kRm9ybS5vcHRpb25cXFwiPlxcblxcdFxcdCAgICBcXHRcXHQ8b3B0aW9uIHZhbHVlPVxcXCJEZXBvc2l0XFxcIj5EZXBvc2l0PC9vcHRpb24+XFxuXFx0XFx0ICAgIFxcdFxcdDxvcHRpb24gdmFsdWU9XFxcIlBheW1lbnRcXFwiPlBheW1lbnQ8L29wdGlvbj5cXG5cXHRcXHQgICAgXFx0XFx0PG9wdGlvbiB2YWx1ZT1cXFwiRGlzY291bnRcXFwiPkRpc2NvdW50PC9vcHRpb24+XFxuXFx0XFx0ICAgIFxcdFxcdDxvcHRpb24gdmFsdWU9XFxcIlJlZnVuZFxcXCI+UmVmdW5kPC9vcHRpb24+XFxuXFx0XFx0ICAgIFxcdDwvc2VsZWN0PlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiA6Y2xhc3M9XFxcInsgJ2hhcy1lcnJvcic6IGFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ2Ftb3VudCcpIH1cXFwiPlxcblxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgQW1vdW50OlxcblxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHQgICAgXFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImFtb3VudFxcXCIgdi1tb2RlbD1cXFwiYWRkRnVuZEZvcm0uYW1vdW50XFxcIj5cXG5cXG5cXHRcXHQgICAgXFx0PHNwYW4gY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIHYtaWY9XFxcImFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ2Ftb3VudCcpXFxcIiB2LXRleHQ9XFxcImFkZEZ1bmRGb3JtLmVycm9ycy5nZXQoJ2Ftb3VudCcpXFxcIj48L3NwYW4+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdDwvZGl2PlxcblxcblxcdFxcdDxkaXYgdi1pZj1cXFwiYWRkRnVuZEZvcm0ub3B0aW9uID09ICdEaXNjb3VudCcgfHwgYWRkRnVuZEZvcm0ub3B0aW9uID09ICdSZWZ1bmQnXFxcIj5cXG5cXHRcXHRcXHRcXG5cXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiA6Y2xhc3M9XFxcInsgJ2hhcy1lcnJvcic6IGFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ3JlYXNvbicpIH1cXFwiPlxcblxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgUmVhc29uOlxcblxcdFxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHQgICAgXFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcInJlYXNvblxcXCIgdi1tb2RlbD1cXFwiYWRkRnVuZEZvcm0ucmVhc29uXFxcIj5cXG5cXG5cXHRcXHRcXHQgICAgXFx0PHNwYW4gY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIHYtaWY9XFxcImFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ3JlYXNvbicpXFxcIiB2LXRleHQ9XFxcImFkZEZ1bmRGb3JtLmVycm9ycy5nZXQoJ3JlYXNvbicpXFxcIj48L3NwYW4+XFxuXFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdDwvZGl2PlxcblxcblxcdFxcdDxidXR0b24gdHlwZT1cXFwic3VibWl0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcXFwiPkFkZDwvYnV0dG9uPlxcblxcdCAgICA8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFxcXCIgZGF0YS1kaXNtaXNzPVxcXCJtb2RhbFxcXCI+Q2xvc2U8L2J1dHRvbj5cXG5cXG5cXHQ8L2Zvcm0+XFxuXFxuPC90ZW1wbGF0ZT5cXG5cXG48c2NyaXB0PlxcblxcbiAgICBleHBvcnQgZGVmYXVsdCB7XFxuXFxuICAgIFxcdHByb3BzOiBbJ2dyb3VwaWQnXSxcXG5cXG4gICAgXFx0ZGF0YSgpIHtcXG4gICAgXFx0XFx0cmV0dXJuIHtcXG4gICAgXFx0XFx0XFx0YWRkRnVuZEZvcm06IG5ldyBGb3JtKHtcXG5cXHRcXHRcXHRcXHRcXHRvcHRpb246ICdEZXBvc2l0JyxcXG5cXHRcXHRcXHRcXHRcXHRhbW91bnQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHJlYXNvbjogJydcXG5cXHRcXHRcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxcbiAgICBcXHRcXHR9XFxuICAgIFxcdH0sXFxuXFxuICAgIFxcdG1ldGhvZHM6IHtcXG4gICAgXFx0XFx0cmVzZXRBZGRGdW5kRm9ybSgpIHtcXG4gICAgXFx0XFx0XFx0dGhpcy5hZGRGdW5kRm9ybS5vcHRpb24gPSAnRGVwb3NpdCc7XFxuICAgIFxcdFxcdFxcdHRoaXMuYWRkRnVuZEZvcm0uYW1vdW50ID0gJyc7XFxuICAgIFxcdFxcdFxcdHRoaXMuYWRkRnVuZEZvcm0ucmVhc29uID0gJyc7XFxuICAgIFxcdFxcdH0sXFxuXFxuICAgIFxcdFxcdGFkZEZ1bmRzKCkge1xcbiAgICBcXHRcXHRcXHR0aGlzLmFkZEZ1bmRGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkICsgJy9hZGQtZnVuZHMnKVxcblxcdFxcdCAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdCAgICAgICAgICAgIGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcXG5cXHRcXHQgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hEZXRhaWxzKCk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIHRoaXMucmVzZXRBZGRGdW5kRm9ybSgpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICAkKCcjYWRkLWZ1bmRzLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XFxuXFx0XFx0ICAgICAgICAgICAgfVxcblxcdFxcdCAgICAgICAgfSlcXG5cXHRcXHQgICAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xcbiAgICBcXHRcXHR9XFxuICAgIFxcdH1cXG5cXG4gICAgfVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNTE5OWE4YThcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGRGdW5kLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoKTtcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcbmZvcm1bZGF0YS12LTczZGY3MmZhXSB7XFxuXFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxuLm0tci0xMFtkYXRhLXYtNzNkZjcyZmFdIHtcXG5cXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJBZGROZXdNZW1iZXIudnVlPzVjMTcyMzk0XCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUF1SEE7Q0FDQSxvQkFBQTtDQUNBO0FBQ0E7Q0FDQSxtQkFBQTtDQUNBXCIsXCJmaWxlXCI6XCJBZGROZXdNZW1iZXIudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIiBAc3VibWl0LnByZXZlbnQ9XFxcImFkZE5ld01lbWJlclxcXCI+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBhZGROZXdNZW1iZXJGb3JtLmVycm9ycy5oYXMoJ2NsaWVudElkJykgfVxcXCI+XFxuXFx0ICAgICAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdCAgICAgICAgICAgIENsaWVudHNcXG5cXHQgICAgICAgIDwvbGFiZWw+XFxuXFxuXFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtc20tMTBcXFwiPlxcblxcdCAgICAgICAgICAgIDxzZWxlY3QgZGF0YS1wbGFjZWhvbGRlcj1cXFwiU2VsZWN0IGNsaWVudHNcXFwiIGNsYXNzPVxcXCJjaG9zZW4tc2VsZWN0LWZvci1jbGllbnRzXFxcIiBtdWx0aXBsZSBzdHlsZT1cXFwid2lkdGg6MTAwJTtcXFwiPlxcblxcdFxcdCAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XFxcImNsaWVudCBpbiBjbGllbnRzXFxcIiA6dmFsdWU9XFxcImNsaWVudC5pZFxcXCI+XFxuXFx0XFx0ICAgICAgICAgICAgICAgXFx0e3sgY2xpZW50Lm5hbWUgfX1cXG5cXHRcXHQgICAgICAgICAgICA8L29wdGlvbj5cXG5cXHRcXHQgICAgICAgIDwvc2VsZWN0PlxcblxcdCAgICAgICAgPC9kaXY+XFxuXFx0ICAgIDwvZGl2PlxcblxcblxcdCAgICA8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5BZGQ8L2J1dHRvbj5cXG5cXHQgICAgPGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFxuXFx0PC9mb3JtPlxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXG4gICAgZXhwb3J0IGRlZmF1bHQge1xcblxcbiAgICBcXHRwcm9wczogWydncm91cGlkJ10sXFxuXFxuICAgIFxcdGRhdGEoKSB7XFxuICAgIFxcdFxcdHJldHVybiB7XFxuICAgIFxcdFxcdFxcdGNsaWVudHM6IFtdLFxcblxcbiAgICBcXHRcXHRcXHRhZGROZXdNZW1iZXJGb3JtOiBuZXcgRm9ybSh7XFxuXFx0XFx0XFx0XFx0XFx0Y2xpZW50SWRzOiBbXVxcblxcdFxcdFxcdFxcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXFxuICAgIFxcdFxcdH1cXG4gICAgXFx0fSxcXG5cXG4gICAgXFx0bWV0aG9kczoge1xcbiAgICBcXHRcXHRmZXRjaENsaWVudHMocSkge1xcblxcdCAgICBcXHRcXHRheGlvc0FQSXYxLmdldCgnL3Zpc2EvY2xpZW50L2dldC1hdmFpbGFibGUtdmlzYS1jbGllbnRzJywge1xcblxcdFxcdFxcdFxcdCBcXHRwYXJhbXM6IHtcXG5cXHRcXHRcXHRcXHQgXFx0XFx0Z3JvdXBJZDogdGhpcy5ncm91cGlkLFxcblxcdFxcdFxcdFxcdCBcXHRcXHRxOiBxXFxuXFx0XFx0XFx0XFx0IFxcdH1cXG5cXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRpZihyZXNwb25zZS5kYXRhLmxlbmd0aCA+IDApe1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuY2xpZW50cy5wdXNoKHtcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICBpZDpyZXNwb25zZS5kYXRhWzBdLmlkLFxcblxcdFxcdFxcdCAgICAgICAgICAgICAgIG5hbWU6cmVzcG9uc2UuZGF0YVswXS5uYW1lIH0pO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXG5cXHRcXHRcXHQgXFx0XFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dmFyIG15X3ZhbCA9ICQoJy5jaG9zZW4tc2VsZWN0LWZvci1jbGllbnRzJykudmFsKCk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0Ly9jb25zb2xlLmxvZyhteV92YWwpO1xcblxcdFxcdFxcdFxcdFxcdFxcdCQoXFxcIi5jaG9zZW4tc2VsZWN0LWZvci1jbGllbnRzXFxcIikudmFsKG15X3ZhbCkudHJpZ2dlcihcXFwiY2hvc2VuOnVwZGF0ZWRcXFwiKTtcXG5cXHRcXHRcXHRcXHRcXHR9LCAxMDAwKTtcXG5cXHRcXHRcXHRcXHR9KTtcXG4gICAgXFx0XFx0fSxcXG5cXG4gICAgXFx0XFx0cmVzZXRBZGROZXdNZW1iZXJGb3JtKCkge1xcbiAgICBcXHRcXHRcXHR0aGlzLmFkZE5ld01lbWJlckZvcm0gPSBuZXcgRm9ybSh7XFxuXFx0XFx0XFx0XFx0XFx0Y2xpZW50SWRzOiBbXVxcblxcdFxcdFxcdFxcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pO1xcbiAgICBcXHRcXHR9LFxcblxcbiAgICBcXHRcXHRhZGROZXdNZW1iZXIoKSB7XFxuICAgIFxcdFxcdFxcdGlmKHRoaXMuYWRkTmV3TWVtYmVyRm9ybS5jbGllbnRJZHMubGVuZ3RoID09IDApIHtcXG4gICAgXFx0XFx0XFx0XFx0dG9hc3RyLmVycm9yKCdDbGllbnRzIGZpZWxkIGlzIHJlcXVpcmVkLicpO1xcbiAgICBcXHRcXHRcXHR9IGVsc2Uge1xcbiAgICBcXHRcXHRcXHRcXHR0aGlzLmFkZE5ld01lbWJlckZvcm0uc3VibWl0KCdwb3N0JywgJy92aXNhL2dyb3VwLycgKyB0aGlzLmdyb3VwaWQgKyAnL2FkZC1tZW1iZXJzJylcXG5cXHRcXHQgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xcblxcdFxcdCAgICAgICAgICAgICAgICBpZihyZXN1bHQuc3VjY2Vzcykge1xcblxcdFxcdCAgICAgICAgICAgICAgICBcXHR0aGlzLmNsaWVudHMgPSBbXTtcXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0dGhpcy5yZXNldEFkZE5ld01lbWJlckZvcm0oKTtcXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0JChcXFwiLmNob3Nlbi1zZWxlY3QtZm9yLWNsaWVudHNcXFwiKS5lbXB0eSgpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdCQoJyNhZGQtbmV3LW1lbWJlci1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0IFxcdFxcdHRvYXN0ci5zdWNjZXNzKHJlc3VsdC5tZXNzYWdlKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHQgXFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZmV0Y2hNZW1iZXJzKCk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0IFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LiRyZWZzLmFkZG5ld3NlcnZpY2VyZWYuZmV0Y2hHcm91cFBhY2thZ2VzU2VydmljZXNMaXN0KCk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0fVxcblxcdFxcdCAgICAgICAgICAgIH0pXFxuXFx0XFx0ICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XFxuICAgIFxcdFxcdFxcdH1cXG4gICAgXFx0XFx0fVxcbiAgICBcXHR9LFxcblxcbiAgICBcXHRtb3VudGVkKCkge1xcblxcdCAgXFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLWNsaWVudHMnKS5jaG9zZW4oe1xcblxcdFxcdFxcdFxcdHdpZHRoOiBcXFwiMTAwJVxcXCIsXFxuXFx0XFx0XFx0XFx0bm9fcmVzdWx0c190ZXh0OiBcXFwiU2VhcmNoaW5nIENsaWVudCAjIDogXFxcIixcXG5cXHRcXHRcXHRcXHRzZWFyY2hfY29udGFpbnM6IHRydWVcXG5cXHRcXHRcXHR9KTtcXG5cXG5cXHRcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItY2xpZW50cycpLm9uKCdjaGFuZ2UnLCAoKSA9PiB7XFxuXFx0XFx0XFx0XFx0bGV0IGNsaWVudElkcyA9ICQoJy5jaG9zZW4tc2VsZWN0LWZvci1jbGllbnRzJykudmFsKCk7XFxuXFxuXFx0XFx0XFx0XFx0dGhpcy5hZGROZXdNZW1iZXJGb3JtLmNsaWVudElkcyA9IGNsaWVudElkcztcXG5cXHRcXHRcXHR9KTtcXG5cXG5cXHRcXHRcXHQkKCdib2R5Jykub24oJ2tleXVwJywgJy5jaG9zZW4tY29udGFpbmVyIGlucHV0W3R5cGU9dGV4dF0nLCAoZSkgPT4ge1xcblxcdFxcdFxcdFxcdGxldCBxID0gJCgnLmNob3Nlbi1jb250YWluZXIgaW5wdXRbdHlwZT10ZXh0XScpLnZhbCgpO1xcblxcdFxcdFxcdFxcdGlmKCBxLmxlbmd0aCA+PSA0ICl7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5mZXRjaENsaWVudHMocSk7XFxuXFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdH0pO1xcbiAgICBcXHR9XFxuXFxuICAgIH1cXG5cXG48L3NjcmlwdD5cXG5cXG48c3R5bGUgc2NvcGVkPlxcblxcdGZvcm0ge1xcblxcdFxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxuXFx0fVxcblxcdC5tLXItMTAge1xcblxcdFxcdG1hcmdpbi1yaWdodDogMTBweDtcXG5cXHR9XFxuPC9zdHlsZT5cIl19XSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTczZGY3MmZhXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkTmV3TWVtYmVyLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzA3XG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2JywgWyhfdm0ubG9nMi55ZWFyICE9IG51bGwpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3cgdGltZWxpbmUtbW92ZW1lbnQgdGltZWxpbmUtbW92ZW1lbnQtdG9wIG5vLWRhc2hlZCBuby1zaWRlLW1hcmdpblwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLWJhZGdlIHRpbWVsaW5lLWZpbHRlci1tb3ZlbWVudFwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgfVxuICB9LCBbX2MoJ3NwYW4nLCBbX3ZtLl92KF92bS5fcyhfdm0ubG9nMi55ZWFyKSldKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvdyB0aW1lbGluZS1tb3ZlbWVudCAgbm8tc2lkZS1tYXJnaW5cIlxuICB9LCBbKF92bS5sb2cyLm1vbnRoICE9IG51bGwpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0aW1lbGluZS1iYWRnZVwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0aW1lbGluZS1iYWxsb29uLWRhdGUtZGF5XCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLmxvZzIuZGF5KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtYmFsbG9vbi1kYXRlLW1vbnRoXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLmxvZzIubW9udGgpKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTIgIHRpbWVsaW5lLWl0ZW1cIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiBjb2wtc20tb2Zmc2V0LTFcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0aW1lbGluZS1wYW5lbCBkZWJpdHNcIlxuICB9LCBbX2MoJ3VsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLXBhbmVsLXVsXCJcbiAgfSwgW19jKCdsaScsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjYXVzYWxlXCJcbiAgfSwgW192bS5fdihcIkJhbGFuY2U6IFwiKSwgX2MoJ2InLCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShfdm0ubG9nMi5kYXRhLmJhbGFuY2UpKSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIFtfYygncCcsIFtfYygnc21hbGwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1tdXRlZFwiXG4gIH0sIFtfdm0uX3YoXCJBbW91bnQgOiBcIiksIF9jKCdiJywgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikoX3ZtLmxvZzIuZGF0YS5hbW91bnQpKSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywgW19jKCdwJywgW19jKCdzbWFsbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihcIlR5cGUgOiBcIiksIF9jKCdiJywgW192bS5fdihfdm0uX3MoX3ZtLmxvZzIuZGF0YS50eXBlKSldKV0pXSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tOVwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLXBhbmVsIGRlYml0c1wiXG4gIH0sIFtfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtcGFuZWwtdWxcIlxuICB9LCBbX2MoJ2xpJywgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImltcG9ydG9cIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJpbm5lckhUTUxcIjogX3ZtLl9zKHRoaXMuZGV0YWlsKVxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhdXNhbGVcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubG9nMi5kYXRhLnByb2Nlc3NvcikpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywgW19jKCdwJywgW19jKCdzbWFsbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImdseXBoaWNvbiBnbHlwaGljb24tdGltZVwiXG4gIH0pLCBfdm0uX3YoX3ZtLl9zKF92bS5sb2cyLmRhdGEuZGF0ZSkpXSldKV0pXSldKV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNmFlZmJmYTZcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi02YWVmYmZhNlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jbGllbnRzL1RyYW5zYWN0aW9uTG9ncy52dWVcbi8vIG1vZHVsZSBpZCA9IDMxXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSgpO1xuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuZm9ybVtkYXRhLXYtYmJkMmVjOTZdIHtcXG5cXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcbn1cXG4ubS1yLTEwW2RhdGEtdi1iYmQyZWM5Nl0ge1xcblxcdG1hcmdpbi1yaWdodDogMTBweDtcXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIlRyYW5zZmVyLnZ1ZT80MGI3ZjdjYlwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBc05BO0NBQ0Esb0JBQUE7Q0FDQTtBQUNBO0NBQ0EsbUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiVHJhbnNmZXIudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXG5cXHQ8ZGl2PlxcblxcblxcdFxcdDxkaXYgdi1zaG93PVxcXCJzY3JlZW4gPT0gMVxcXCI+XFxuXFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwicm93XFxcIj5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgYnRuLWJsb2NrXFxcIiBcXG5cXHRcXHRcXHRcXHRcXHRcXHRAY2xpY2s9XFxcImdldERldGFpbHMoJ2NsaWVudC10by1ncm91cCcsIDIpXFxcIj5DbGllbnQgdG8gR3JvdXA8L2J1dHRvbj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWluZm8gYnRuLWJsb2NrXFxcIiBcXG5cXHRcXHRcXHRcXHRcXHRcXHRAY2xpY2s9XFxcImdldERldGFpbHMoJ2dyb3VwLXRvLWNsaWVudCcsIDIpXFxcIj5Hcm91cCB0byBDbGllbnQ8L2J1dHRvbj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQ8ZGl2IHYtc2hvdz1cXFwic2NyZWVuID09IDJcXFwiPlxcblxcdFxcdFxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwidHJhbnNmZXJcXFwiPlxcblxcdFxcdFxcdFxcdFxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcInRhYmxlLXJlc3BvbnNpdmVcXFwiPlxcblxcdCAgICAgICAgICAgICAgICA8dGFibGUgY2xhc3M9XFxcInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVyZWQgdGFibGUtaG92ZXIgdHJhbnNmZXItZGF0YXRhYmxlXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIDx0aGVhZD5cXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgIDx0cj5cXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICA8dGggY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICBcXHQ8aW5wdXQgdHlwZT1cXFwiY2hlY2tib3hcXFwiIEBjbGljaz1cXFwidG9nZ2xlQWxsQ2hlY2tib3hcXFwiPlxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgICAgIDwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgPHRoPkRldGFpbDwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgPHRoPlN0YXR1czwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgPHRoPlRyYWNraW5nPC90aD5cXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICA8dGg+UGFja2FnZXM8L3RoPlxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgPC90cj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIDwvdGhlYWQ+XFxuXFx0ICAgICAgICAgICAgICAgICAgICA8dGJvZHk+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHQ8dHIgdi1mb3I9XFxcInNlcnZpY2UgaW4gc2VydmljZXNcXFwiPlxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0XFx0PHRkIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHQ8aW5wdXQgdHlwZT1cXFwiY2hlY2tib3hcXFwiIDp2YWx1ZT1cXFwic2VydmljZS5pZFxcXCIgdi1tb2RlbD1cXFwidHJhbnNmZXJGb3JtLnNlcnZpY2VzXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdFxcdDwvdGQ+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHRcXHQ8dGQ+e3sgc2VydmljZS5kZXRhaWwgfX08L3RkPlxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0XFx0PHRkPnt7IHNlcnZpY2Uuc3RhdHVzIH19PC90ZD5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdFxcdDx0ZD57eyBzZXJ2aWNlLnRyYWNraW5nIH19PC90ZD5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdFxcdDx0ZD5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdFxcdFxcdDxzZWxlY3QgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgOmNsYXNzPVxcXCIndHJhbnNmZXItcGFja2FnZS1zZWxlY3QtJyArIHNlcnZpY2UuaWRcXFwiIFxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0XFx0XFx0PlxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0XFx0XFx0XFx0PG9wdGlvbiB2YWx1ZT1cXFwiR2VuZXJhdGUgbmV3IHBhY2thZ2VcXFwiPkdlbmVyYXRlIG5ldyBwYWNrYWdlPC9vcHRpb24+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHQ8b3B0aW9uIHYtZm9yPVxcXCJwYWNrYWdlIGluIHBhY2thZ2VzXFxcIiA6dmFsdWU9XFxcInBhY2thZ2UudHJhY2tpbmdcXFwiID5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdFxcdFxcdFxcdFxcdFBhY2thZ2UgI3t7cGFja2FnZS50cmFja2luZ319XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHRcXHQoe3sgcGFja2FnZS5sb2dfZGF0ZSB8IGNvbW1vbkRhdGUgfX0pXFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHQ8L29wdGlvbj5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdFxcdFxcdDwvc2VsZWN0PlxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0XFx0PC90ZD5cXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdDwvdHI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICA8L3Rib2R5PlxcblxcdCAgICAgICAgICAgICAgICA8L3RhYmxlPlxcblxcdCAgICAgICAgICAgIDwvZGl2PlxcblxcblxcdFxcdFxcdFxcdDxidXR0b24gdHlwZT1cXFwic3VibWl0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcXFwiPkNvbXBsZXRlIFRyYW5zZmVyPC9idXR0b24+XFxuXFx0XFx0ICAgIFxcdDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBAY2xpY2s9XFxcInNldFNjcmVlbigxKVxcXCI+QmFjazwvYnV0dG9uPlxcblxcdFxcdCAgICA8L2Zvcm0+XFxuXFx0XFx0PC9kaXY+XFxuXFxuXFx0PC9kaXY+XFxuXFxuPC90ZW1wbGF0ZT5cXG5cXG48c2NyaXB0PlxcblxcbiAgICBleHBvcnQgZGVmYXVsdCB7XFxuXFxuICAgIFxcdHByb3BzOiBbJ2dyb3VwaWQnXSxcXG5cXG4gICAgXFx0ZGF0YSgpIHtcXG4gICAgXFx0XFx0cmV0dXJuIHtcXG4gICAgXFx0XFx0XFx0c2NyZWVuOiBudWxsLFxcblxcbiAgICBcXHRcXHRcXHRzZXJ2aWNlczogW10sXFxuICAgIFxcdFxcdFxcdHBhY2thZ2VzOiBbXSxcXG5cXG4gICAgXFx0XFx0XFx0dHJhbnNmZXJGb3JtOiBuZXcgRm9ybSh7XFxuICAgIFxcdFxcdFxcdFxcdGdyb3VwSWQ6ICcnLFxcbiAgICBcXHRcXHRcXHRcXHRtZW1iZXJJZDogJycsXFxuXFx0XFx0XFx0XFx0XFx0c2VydmljZXM6IFtdLFxcblxcdFxcdFxcdFxcdFxcdHBhY2thZ2VzOltdLFxcblxcdFxcdFxcdFxcdFxcdG9wdGlvbjogJydcXG5cXHRcXHRcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxcbiAgICBcXHRcXHR9XFxuICAgIFxcdH0sXFxuXFxuICAgIFxcdG1ldGhvZHM6IHtcXG5cXG4gICAgXFx0XFx0c2V0U2NyZWVuKHNjcmVlbikge1xcbiAgICBcXHRcXHRcXHR0aGlzLnNjcmVlbiA9IHNjcmVlbjtcXG4gICAgXFx0XFx0fSxcXG5cXG4gICAgXFx0XFx0c2V0TWVtYmVySWQoaWQpIHtcXG4gICAgXFx0XFx0XFx0dGhpcy50cmFuc2ZlckZvcm0ubWVtYmVySWQgPSBpZDtcXG5cXG4gICAgXFx0XFx0XFx0dGhpcy5zZXRTY3JlZW4oMSk7XFxuICAgIFxcdFxcdH0sXFxuXFxuICAgIFxcdFxcdGdldERldGFpbHMob3B0aW9uLCBzY3JlZW4pIHtcXG4gICAgXFx0XFx0XFx0Ly8gb3B0aW9uID0gY2xpZW50LXRvLWdyb3VwIHwgZ3JvdXAtdG8tY2xpZW50XFxuICAgIFxcdFxcdFxcdHRoaXMudHJhbnNmZXJGb3JtLm9wdGlvbiA9IG9wdGlvbjtcXG5cXG4gICAgXFx0XFx0XFx0dmFyIHZtID0gdGhpcztcXG5cXG5cXHRcXHRcXHRcXHRmdW5jdGlvbiBnZXRTZXJ2aWNlcygpIHtcXG5cXHQgICAgICAgICAgICAgICAgcmV0dXJuIGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50LycrIHZtLnRyYW5zZmVyRm9ybS5tZW1iZXJJZCArJy9zZXJ2aWNlcycsIHtcXG4gICAgXFx0XFx0XFx0XFx0XFx0cGFyYW1zOiB7XFxuICAgIFxcdFxcdFxcdFxcdFxcdFxcdG9wdGlvbjogb3B0aW9uLFxcbiAgICBcXHRcXHRcXHRcXHRcXHRcXHRncm91cElkOiB2bS5ncm91cGlkXFxuICAgIFxcdFxcdFxcdFxcdFxcdH1cXG4gICAgXFx0XFx0XFx0XFx0fSk7XFxuXFx0ICAgICAgICAgICAgfVxcblxcdCAgICAgICAgICAgIGZ1bmN0aW9uIGdldFBhY2thZ2VzKCkge1xcblxcdCAgICAgICAgICAgICAgICAvLyBheGlvc1xcblxcdCAgICAgICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdmlzYS9jbGllbnQvJysgdm0udHJhbnNmZXJGb3JtLm1lbWJlcklkICsnL3BhY2thZ2VzJywge1xcbiAgICBcXHRcXHRcXHRcXHRcXHRwYXJhbXM6IHtcXG4gICAgXFx0XFx0XFx0XFx0XFx0XFx0b3B0aW9uOiBvcHRpb24sXFxuICAgIFxcdFxcdFxcdFxcdFxcdFxcdGdyb3VwSWQ6IHZtLmdyb3VwaWRcXG4gICAgXFx0XFx0XFx0XFx0XFx0fVxcbiAgICBcXHRcXHRcXHRcXHR9KTtcXG5cXHQgICAgICAgICAgICB9XFxuXFxuXFx0ICAgICAgICAgICAgYXhpb3MuYWxsKFtcXG5cXHQgICAgICAgICAgICAgICAgZ2V0U2VydmljZXMoKSxcXG5cXHQgICAgICAgICAgICAgICAgZ2V0UGFja2FnZXMoKVxcblxcdCAgICAgICAgICAgIF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxcblxcdCAgICAgICAgICAgICAgICAoXFxuXFx0ICAgICAgICAgICAgICAgIHNlcnZpY2VzLFxcblxcdCAgICAgICAgICAgICAgICBwYWNrYWdlc1xcblxcdCAgICAgICAgICAgICAgICApID0+IHtcXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcblxcdCAgICAgICAgICAgICAgICB0aGlzLnNlcnZpY2VzID0gc2VydmljZXMuZGF0YTtcXG5cXHQgICAgICAgICAgICAgICAgdGhpcy5wYWNrYWdlcyA9IHBhY2thZ2VzLmRhdGE7XFxuXFx0ICAgICAgICAgICAgfSkpXFxuXFx0ICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XFxuXFxuICAgIFxcdFxcdFxcdHRoaXMuc2V0U2NyZWVuKHNjcmVlbik7XFxuICAgIFxcdFxcdH0sXFxuXFxuICAgIFxcdFxcdGNhbGxEYXRhVGFibGUoKSB7XFxuXFx0XFx0XFx0XFx0JCgnLnRyYW5zZmVyLWRhdGF0YWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcXG5cXG5cXHRcXHRcXHRcXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xcblxcdFxcdFxcdFxcdFxcdCQoJy50cmFuc2Zlci1kYXRhdGFibGUnKS5EYXRhVGFibGUoe1xcblxcdFxcdFxcdCAgICAgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxcblxcdFxcdFxcdCAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXFxuXFx0XFx0XFx0ICAgICAgICAgICAgZG9tOiAnPFxcXCJ0b3BcXFwibGY+cnQ8XFxcImJvdHRvbVxcXCJpcD48XFxcImNsZWFyXFxcIj4nXFxuXFx0XFx0XFx0ICAgICAgICB9KTtcXG5cXHRcXHRcXHRcXHR9LCAxMDAwKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdHRvZ2dsZUFsbENoZWNrYm94KGUpIHtcXG5cXHRcXHRcXHRcXHRpZihlLnRhcmdldC5jaGVja2VkID09IHRydWUpIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLnRyYW5zZmVyRm9ybS5zZXJ2aWNlcyA9IFtdO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuc2VydmljZXMuZm9yRWFjaCgocywgaW5kZXgpID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLnRyYW5zZmVyRm9ybS5zZXJ2aWNlcy5wdXNoKHMuaWQpO1xcblxcdFxcdFxcdFxcdFxcdH0pO1xcblxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy50cmFuc2ZlckZvcm0uc2VydmljZXMgPSBbXTtcXG5cXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRyZXNldFRyYW5zZmVyRm9ybSgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLnNlcnZpY2VzID0gW107XFxuXFx0XFx0XFx0XFx0dGhpcy5wYWNrYWdlcyA9IFtdO1xcblxcdFxcdFxcdH0sXFxuXFxuICAgIFxcdFxcdHRyYW5zZmVyKCkge1xcbiAgICBcXHRcXHRcXHRpZih0aGlzLnRyYW5zZmVyRm9ybS5zZXJ2aWNlcy5sZW5ndGggPT0gMCkge1xcbiAgICAgICAgXFx0XFx0XFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGF0IGxlYXN0IG9uZSBzZXJ2aWNlJyk7XFxuXFx0XFx0XFx0XFx0XFx0cmV0dXJuIGZhbHNlO1xcbiAgICAgICAgXFx0XFx0fSBlbHNlIHtcXG5cXG4gICAgICAgIFxcdFxcdFxcdHRoaXMudHJhbnNmZXJGb3JtLnBhY2thZ2VzID0gW107XFxuXFxuICAgICAgICBcXHRcXHRcXHR0aGlzLnRyYW5zZmVyRm9ybS5zZXJ2aWNlcy5mb3JFYWNoKChzLCBpbmRleCkgPT4ge1xcblxcdFxcdCAgICAgICAgXFx0XFx0bGV0IHZhbHVlID0gJCgnc2VsZWN0LnRyYW5zZmVyLXBhY2thZ2Utc2VsZWN0LScgKyBzKS52YWwoKTtcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdFxcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy50cmFuc2ZlckZvcm0ucGFja2FnZXMucHVzaCh2YWx1ZSk7XFxuXFx0XFx0ICAgICAgIFxcdFxcdH0pO1xcblxcblxcdCAgICBcXHRcXHRcXHR0aGlzLnRyYW5zZmVyRm9ybS5zdWJtaXQoJ3BhdGNoJywgJy92aXNhL2NsaWVudC8nICsgdGhpcy5ncm91cGlkICsgJy90cmFuc2ZlcicpXFxuXFx0XFx0ICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdCAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmZldGNoRGV0YWlscygpO1xcblxcdFxcdCAgICAgICAgICAgICAgICBcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5mZXRjaE1lbWJlcnMoKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuJHJlZnMuYWRkbmV3c2VydmljZXJlZi5mZXRjaEdyb3VwUGFja2FnZXNTZXJ2aWNlc0xpc3QoKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0dGhpcy5yZXNldFRyYW5zZmVyRm9ybSgpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHQkKCcjdHJhbnNmZXItbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHR0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5tZXNzYWdlKTtcXG5cXHRcXHQgICAgICAgICAgICAgICAgfVxcblxcdFxcdCAgICAgICAgICAgIH0pXFxuXFx0XFx0ICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XFxuXFxuICAgICAgICBcXHRcXHR9XFxuICAgIFxcdFxcdH1cXG5cXG4gICAgXFx0fSxcXG5cXG4gICAgXFx0Y3JlYXRlZCgpIHtcXG4gICAgXFx0XFx0dGhpcy50cmFuc2ZlckZvcm0uZ3JvdXBJZCA9IHRoaXMuZ3JvdXBpZDtcXG4gICAgXFx0fVxcblxcbiAgICB9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1iYmQyZWM5NlwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1RyYW5zZmVyLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzExXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoKTtcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcbmZvcm1bZGF0YS12LWM0MzBkYWRhXSB7XFxuXFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxuLm0tci0xMFtkYXRhLXYtYzQzMGRhZGFdIHtcXG5cXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxufVxcbi5zcGFjZXItMTBbZGF0YS12LWM0MzBkYWRhXSB7XFxuXFx0Y2xlYXI6IGJvdGg7XFxuXFx0aGVpZ2h0OiAxMHB4O1xcbn1cXG4uY3Vyc29yW2RhdGEtdi1jNDMwZGFkYV0ge1xcblxcdGN1cnNvcjogcG9pbnRlcjtcXG59XFxuLnNjcm9sbGFibGVbZGF0YS12LWM0MzBkYWRhXSB7XFxuXFx0bWF4LWhlaWdodDogNDUwcHg7XFxuXFx0b3ZlcmZsb3cteTogYXV0bztcXG59XFxuLmNoaWxkLXJvdy1oZWFkZXJbZGF0YS12LWM0MzBkYWRhXSB7XFxuXFx0Y29sb3I6ICMzYzhkYmM7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJDbGllbnRTZXJ2aWNlcy52dWU/NzRmNGY3N2JcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQWtQQTtDQUNBLG9CQUFBO0NBQ0E7QUFDQTtDQUNBLG1CQUFBO0NBQ0E7QUFDQTtDQUNBLFlBQUE7Q0FDQSxhQUFBO0NBQ0E7QUFDQTtDQUNBLGdCQUFBO0NBQ0E7QUFDQTtDQUNBLGtCQUFBO0NBQ0EsaUJBQUE7Q0FDQTtBQUNBO0NBQ0EsZUFBQTtDQUNBXCIsXCJmaWxlXCI6XCJDbGllbnRTZXJ2aWNlcy52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdFxcblxcdDxkaXY+XFxuXFx0XFx0XFxuXFx0XFx0PGRpdiB2LXNob3c9XFxcInNjcmVlbiA9PSAxXFxcIj5cXG5cXHRcXHRcXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIj5cXG5cXHRcXHRcXHRcXHQ8dGFibGUgY2xhc3M9XFxcInRhYmxlIHRhYmxlLWJvcmRlcmVkIHRhYmxlLXN0cmlwZWQgdGFibGUtaG92ZXIgbWVtYmVyc0RhdGF0YWJsZVxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICA8dGhlYWQ+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgPHRyPlxcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdDx0aCBzdHlsZT1cXFwid2lkdGg6NDAlO1xcXCI+Q0xJRU5UIElEPC90aD5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgPHRoIHN0eWxlPVxcXCJ3aWR0aDo0MCU7XFxcIj5OQU1FPC90aD5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgPHRoIHN0eWxlPVxcXCJ3aWR0aDoyMCU7XFxcIj5BQ1RJT048L3RoPlxcblxcdFxcdFxcdCAgICAgICAgICAgIDwvdHI+XFxuXFx0XFx0XFx0ICAgICAgICA8L3RoZWFkPlxcblxcdFxcdFxcdCAgICAgICAgPHRib2R5PlxcblxcdFxcdFxcdCAgICAgICAgXFx0PHRyIHYtZm9yPVxcXCJtZW1iZXIgaW4gbWVtYmVyc1xcXCI+XFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHQ8dGQ+e3sgbWVtYmVyLmNsaWVudC5pZCB9fTwvdGQ+XFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHQ8dGQ+e3sgbWVtYmVyLmNsaWVudC5maXJzdF9uYW1lICsgJyAnICsgbWVtYmVyLmNsaWVudC5sYXN0X25hbWUgfX08L3RkPlxcblxcdFxcdFxcdCAgICAgICAgXFx0XFx0PHRkIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHRcXHQ8aW5wdXQgdHlwZT1cXFwiY2hlY2tib3hcXFwiIG5hbWU9XFxcImNsaWVudHNJZFtdXFxcIiA6dmFsdWU9XFxcIm1lbWJlci5jbGllbnQuaWRcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgXFx0XFx0PC90ZD5cXG5cXHRcXHRcXHQgICAgICAgIFxcdDwvdHI+XFxuXFx0XFx0XFx0ICAgICAgICA8L3Rib2R5PlxcblxcdFxcdFxcdCAgICA8L3RhYmxlPlxcblxcblxcdFxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJzcGFjZXItMTBcXFwiPjwvZGl2PlxcblxcblxcdFxcdFxcdCAgICA8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIiBAY2xpY2s9XFxcImdvdG9TY3JlZW4oMilcXFwiPkNvbnRpbnVlPC9idXR0b24+XFxuXFx0XFx0XFx0PC9mb3JtPlxcblxcdFxcdDwvZGl2PiA8IS0tIHNjcmVlbiAxIC0tPlxcblxcblxcdFxcdDxkaXYgdi1zaG93PVxcXCJzY3JlZW4gPT0gMlxcXCI+XFxuXFxuXFx0XFx0XFx0PGZvcm0gY2xhc3M9XFxcImZvcm0taG9yaXpvbnRhbFxcXCI+XFxuXFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwic2Nyb2xsYWJsZVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0PHRhYmxlIGNsYXNzPVxcXCJ0YWJsZSB0YWJsZS1ib3JkZXJlZCB0YWJsZS1zdHJpcGVkIHRhYmxlLWhvdmVyXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDx0aGVhZD5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICA8dHI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgXFx0PHRoIHN0eWxlPVxcXCJ3aWR0aDo0NSU7XFxcIj5DTElFTlQgSUQ8L3RoPlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8dGggc3R5bGU9XFxcIndpZHRoOjQ1JTtcXFwiPk5BTUU8L3RoPlxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8dGggc3R5bGU9XFxcIndpZHRoOjEwJTtcXFwiIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCI+QUNUSU9OPC90aD5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICA8L3RyPlxcblxcdFxcdFxcdFxcdCAgICAgICAgPC90aGVhZD5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDx0Ym9keSB2LWZvcj1cXFwiKG1lbWJlciwgaW5kZXgpIGluIG1lbWJlcnNcXFwiIHYtaWY9XFxcInNlbGVjdGVkQ2xpZW50c0lkLmluZGV4T2YobWVtYmVyLmNsaWVudC5pZCkgIT0gLTFcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0XFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHQ8dHIgQGNsaWNrPVxcXCJ0b2dnbGVDaGlsZFJvdygkZXZlbnQsIGluZGV4KVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICA8dGQgc3R5bGU9XFxcIndpZHRoOjQ1JTtcXFwiPnt7IG1lbWJlci5jbGllbnQuaWQgfX08L3RkPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgPHRkIHN0eWxlPVxcXCJ3aWR0aDo0NSU7XFxcIj57eyBtZW1iZXIuY2xpZW50LmZpcnN0X25hbWUgKyAnICcgKyBtZW1iZXIuY2xpZW50Lmxhc3RfbmFtZSB9fTwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICA8dGQgc3R5bGU9XFxcIndpZHRoOjEwJTtcXFwiIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICBcXHQ8aSBpZD1cXFwiZmEtMTAwMFxcXCIgY2xhc3M9XFxcImZhIGZhLWFycm93LXJpZ2h0IGN1cnNvclxcXCIgOmNsYXNzPVxcXCInYXJyb3ctJyArIGluZGV4XFxcIj48L2k+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICA8L3RkPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8L3RyPlxcblxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8dHIgOmlkPVxcXCInY2hpbGQtcm93LScraW5kZXhcXFwiIGNsYXNzPVxcXCJoaWRlXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgXFx0PHRkIGNvbHNwYW49XFxcIjNcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHRcXHQ8dGFibGUgY2xhc3M9XFxcInRhYmxlIHRhYmxlLWJvcmRlcmVkIHRhYmxlLXN0cmlwZWQgdGFibGUtaG92ZXJcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgPHRoZWFkPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgIDx0ciBjbGFzcz1cXFwiY2hpbGQtcm93LWhlYWRlclxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgXFx0PHRoPkRhdGU8L3RoPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8dGg+UGFja2FnZTwvdGg+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDx0aD5EZXRhaWxzPC90aD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgPHRoPkNvc3Q8L3RoPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8dGg+Q2hhcmdlPC90aD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgPHRoPlRpcDwvdGg+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDx0aD5EaXNjb3VudDwvdGg+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDx0aD5TdGF0dXM8L3RoPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICA8dGggY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIDwvdGg+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgPC90cj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIDwvdGhlYWQ+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICA8dGJvZHk+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICBcXHQ8dHIgdi1mb3I9XFxcInNlcnZpY2UgaW4gbWVtYmVyLnNlcnZpY2VzXFxcIiB2LWlmPVxcXCJzZXJ2aWNlLmdyb3VwX2lkID09IGdyb3VwaWQgJiYgc2VydmljZS5hY3RpdmUgPT0gMSAmJiBzZXJ2aWNlLnN0YXR1cyAhPSAnQ29tcGxldGUnXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdDx0ZD57eyBzZXJ2aWNlLnNlcnZpY2VfZGF0ZSB9fTwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICBcXHRcXHQ8dGQ+e3sgc2VydmljZS50cmFja2luZyB9fTwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICBcXHRcXHQ8dGQ+e3sgc2VydmljZS5kZXRhaWwgfX08L3RkPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgXFx0XFx0PHRkPnt7IHNlcnZpY2UuY29zdCB8IGN1cnJlbmN5IH19PC90ZD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdDx0ZD57eyBzZXJ2aWNlLmNoYXJnZSB8IGN1cnJlbmN5IH19PC90ZD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdDx0ZD57eyBzZXJ2aWNlLnRpcCB8IGN1cnJlbmN5IH19PC90ZD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdDx0ZD57eyBzZXJ2aWNlLmRpc2NvdW50X2Ftb3VudCB8IGN1cnJlbmN5IH19PC90ZD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdDx0ZD57eyBzZXJ2aWNlLnN0YXR1cyB9fTwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICBcXHRcXHQ8dGQgY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdFxcdDxpbnB1dCB0eXBlPVxcXCJjaGVja2JveFxcXCIgbmFtZT1cXFwiY2xpZW50U2VydmljZXNJZFtdXFxcIiA6dmFsdWU9XFxcInNlcnZpY2UuaWRcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgXFx0XFx0PC90ZD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIFxcdDwvdHI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICA8L3Rib2R5PlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICA8L3RhYmxlPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8L3RkPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8L3RyPlxcblxcblxcdFxcdFxcdFxcdCAgICAgICAgPC90Ym9keT5cXG5cXHRcXHRcXHRcXHQgICAgPC90YWJsZT5cXG5cXHRcXHRcXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwic3BhY2VyLTEwXFxcIj48L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHQ8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIiBAY2xpY2s9XFxcInNldFNlbGVjdGVkQ2xpZW50U2VydmljZXNBbmRUcmFja2luZ3NJZFxcXCI+Q29udGludWU8L2J1dHRvbj5cXG5cXHRcXHRcXHRcXHQ8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFxcXCIgQGNsaWNrPVxcXCJnb3RvU2NyZWVuKDEpXFxcIj5CYWNrPC9idXR0b24+XFxuXFx0XFx0XFx0PC9mb3JtPlxcblxcdFxcdDwvZGl2PiA8IS0tIHNjcmVlbiAyIC0tPlxcblxcblxcdDwvZGl2PlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXHRleHBvcnQgZGVmYXVsdCB7XFxuXFxuXFx0XFx0cHJvcHM6IFsnbWVtYmVycycsICdncm91cGlkJ10sXFxuXFxuICAgICAgICBkYXRhKCkge1xcblxcbiAgICAgICAgXFx0cmV0dXJuIHtcXG4gICAgICAgIFxcdFxcdG1lbWJlcnNEYXRhdGFibGU6IG51bGwsXFxuXFxuICAgICAgICBcXHRcXHRzY3JlZW46IDEsXFxuXFxuICAgICAgICBcXHRcXHRzZWxlY3RlZENsaWVudHNJZDogW10sXFxuICAgICAgICBcXHRcXHRzZWxlY3RlZENsaWVudFNlcnZpY2VzSWQ6IFtdLFxcbiAgICAgICAgXFx0XFx0c2VsZWN0ZWRUcmFja2luZ3M6IFtdXFxuICAgICAgICBcXHR9XFxuXFxuICAgICAgICB9LFxcblxcbiAgICAgICAgbWV0aG9kczoge1xcblxcbiAgICAgICAgXFx0Z290b1NjcmVlbihzY3JlZW4pIHtcXG4gICAgICAgIFxcdFxcdHRoaXMuc2V0U2VsZWN0ZWRDbGllbnRzSWQoKTtcXG5cXG4gICAgICAgIFxcdFxcdGlmKHNjcmVlbiA9PSAyICYmIHRoaXMuc2VsZWN0ZWRDbGllbnRzSWQubGVuZ3RoID09IDApIHtcXG4gICAgICAgIFxcdFxcdFxcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjbGllbnQvcy4nKTtcXG4gICAgICAgIFxcdFxcdH0gZWxzZSB7XFxuICAgICAgICBcXHRcXHRcXHR0aGlzLnNjcmVlbiA9IHNjcmVlbjtcXG4gICAgICAgIFxcdFxcdH1cXG4gICAgICAgIFxcdH0sXFxuXFxuICAgICAgICBcXHRzZXRTZWxlY3RlZENsaWVudHNJZCgpIHtcXG4gICAgICAgIFxcdFxcdGxldCBzZWxlY3RlZENsaWVudHNJZCA9IFtdO1xcblxcblxcdFxcdFxcdCAgICB0aGlzLm1lbWJlcnNEYXRhdGFibGUuJChcXFwiaW5wdXRbbmFtZT0nY2xpZW50c0lkW10nXTpjaGVja2VkXFxcIikuZWFjaChmdW5jdGlvbihlKSB7XFxuXFx0XFx0XFx0ICAgIFxcdHNlbGVjdGVkQ2xpZW50c0lkLnB1c2gocGFyc2VJbnQoJCh0aGlzKS52YWwoKSkpO1xcblxcdFxcdFxcdCAgICB9KTtcXG5cXG5cXHRcXHRcXHQgICAgdGhpcy5zZWxlY3RlZENsaWVudHNJZCA9IHNlbGVjdGVkQ2xpZW50c0lkO1xcbiAgICAgICAgXFx0fSxcXG5cXG4gICAgICAgIFxcdHNldFNlbGVjdGVkQ2xpZW50U2VydmljZXNBbmRUcmFja2luZ3NJZCgpIHtcXG4gICAgICAgIFxcdFxcdGxldCBzZWxlY3RlZENsaWVudFNlcnZpY2VzSWQgPSBbXTtcXG5cXG5cXHRcXHRcXHQgICAgJChcXFwiaW5wdXRbbmFtZT0nY2xpZW50U2VydmljZXNJZFtdJ106Y2hlY2tlZFxcXCIpLmVhY2goZnVuY3Rpb24oZSkge1xcblxcdFxcdFxcdCAgICBcXHRzZWxlY3RlZENsaWVudFNlcnZpY2VzSWQucHVzaChwYXJzZUludCgkKHRoaXMpLnZhbCgpKSk7XFxuXFx0XFx0XFx0ICAgIH0pO1xcblxcblxcdFxcdFxcdCAgICBpZihzZWxlY3RlZENsaWVudFNlcnZpY2VzSWQubGVuZ3RoID09IDApIHtcXG5cXHRcXHRcXHQgICAgXFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IHNlcnZpY2VzL3MuJyk7XFxuXFx0XFx0XFx0ICAgIH0gZWxzZSB7XFxuXFx0XFx0XFx0ICAgIFxcdGF4aW9zQVBJdjEuZ2V0KCcvdmlzYS9ncm91cC8nICsgdGhpcy5ncm91cGlkICsgJy9jbGllbnQtc2VydmljZXMnLCB7XFxuXFx0XFx0XFx0XFx0XFx0ICAgIHBhcmFtczoge1xcblxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdGNsaWVudFNlcnZpY2VzSWQ6IHNlbGVjdGVkQ2xpZW50U2VydmljZXNJZFxcblxcdFxcdFxcdFxcdFxcdCAgICB9XFxuXFx0XFx0XFx0XFx0XFx0fSlcXG5cXHRcXHRcXHRcXHRcXHQudGhlbihyZXNwb25zZSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5zZWxlY3RlZENsaWVudFNlcnZpY2VzSWQgPSByZXNwb25zZS5kYXRhO1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdGxldCBzZWxlY3RlZFRyYWNraW5ncyA9IFtdO1xcblxcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy5zZWxlY3RlZENsaWVudFNlcnZpY2VzSWQuZm9yRWFjaChjbGllbnRTZXJ2aWNlID0+IHtcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdHNlbGVjdGVkVHJhY2tpbmdzLnB1c2goY2xpZW50U2VydmljZS50cmFja2luZyk7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR9KTtcXG5cXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuc2VsZWN0ZWRUcmFja2luZ3MgPSBzZWxlY3RlZFRyYWNraW5ncztcXG5cXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuc2hvd011bHRpcGxlUXVpY2tSZXBvcnRNb2RhbCgpO1xcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XFxuXFx0XFx0XFx0ICAgIH1cXG4gICAgICAgIFxcdH0sXFxuXFxuICAgICAgICBcXHRzaG93TXVsdGlwbGVRdWlja1JlcG9ydE1vZGFsKCkge1xcbiAgICAgICAgXFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQucXVpY2tSZXBvcnRDbGllbnRzSWQgPSB0aGlzLnNlbGVjdGVkQ2xpZW50c0lkO1xcblxcdFxcdFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LnF1aWNrUmVwb3J0Q2xpZW50U2VydmljZXNJZCA9IHRoaXMuc2VsZWN0ZWRDbGllbnRTZXJ2aWNlc0lkO1xcblxcdFxcdFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LnF1aWNrUmVwb3J0VHJhY2tpbmdzID0gdGhpcy5zZWxlY3RlZFRyYWNraW5ncztcXG5cXHRcXHRcXHRcXHRcXHRcXG5cXHRcXHRcXHRcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5mZXRjaERvY3ModGhpcy5zZWxlY3RlZENsaWVudFNlcnZpY2VzSWQpO1xcblxcblxcdFxcdFxcdFxcdCQoJyNjbGllbnQtc2VydmljZXMtbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xcblxcblxcdFxcdFxcdFxcdHNldFRpbWVvdXQoKCkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdCQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykubW9kYWwoJ3Nob3cnKTtcXG5cXHRcXHRcXHRcXHR9LCAxMDAwKTtcXG4gICAgICAgIFxcdH0sXFxuXFxuICAgICAgICBcXHR1bmNoZWNrU2VsZWN0ZWRDbGllbnRzSWQoKSB7XFxuICAgICAgICBcXHRcXHR0aGlzLm1lbWJlcnNEYXRhdGFibGUuJChcXFwiaW5wdXRbbmFtZT0nY2xpZW50c0lkW10nXVxcXCIpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XFxuICAgICAgICBcXHR9LFxcblxcbiAgICAgICAgXFx0dW5jaGVja1NlbGVjdGVkQ2xpZW50U2VydmljZXNJZCgpIHtcXG4gICAgICAgIFxcdFxcdHRoaXMubWVtYmVyc0RhdGF0YWJsZS4kKFxcXCJpbnB1dFtuYW1lPSdjbGllbnRTZXJ2aWNlc0lkW10nXVxcXCIpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XFxuICAgICAgICBcXHR9LFxcblxcbiAgICAgICAgXFx0Y3JlYXRlRGF0YXRhYmxlKGNsYXNzTmFtZSkge1xcbiAgICAgICAgXFx0XFx0dGhpcy5tZW1iZXJzRGF0YXRhYmxlID0gJCgndGFibGUuJytjbGFzc05hbWUpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcXG5cXG5cXHRcXHRcXHRcXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xcblxcdFxcdFxcdFxcdFxcdHRoaXMubWVtYmVyc0RhdGF0YWJsZSA9ICQoJ3RhYmxlLicrY2xhc3NOYW1lKS5EYXRhVGFibGUoe1xcblxcdFxcdFxcdFxcdCAgICAgICAgcGFnZUxlbmd0aDogMTAsXFxuXFx0XFx0XFx0XFx0ICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxcblxcdFxcdFxcdFxcdCAgICAgICAgXFxcImFhU29ydGluZ1xcXCI6IFtdLFxcblxcdFxcdFxcdFxcdCAgICAgICAgZG9tOiAnPFxcXCJ0b3BcXFwibGY+cnQ8XFxcImJvdHRvbVxcXCJpcD48XFxcImNsZWFyXFxcIj4nXFxuXFx0XFx0XFx0XFx0ICAgIH0pO1xcblxcdFxcdFxcdFxcdH0uYmluZCh0aGlzKSwgMTAwMCk7XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHR0b2dnbGVDaGlsZFJvdyhldmVudCwgaW5kZXgpIHtcXG5cXHRcXHRcXHRcXHQkKCcjY2hpbGQtcm93LScraW5kZXgpLnRvZ2dsZUNsYXNzKCdoaWRlJyk7XFxuXFx0XFx0XFx0XFx0JCgnLmFycm93LScraW5kZXgpLnRvZ2dsZUNsYXNzKCdmYS1hcnJvdy1yaWdodCBmYS1hcnJvdy1kb3duJyk7XFxuXFx0XFx0XFx0fVxcblxcbiAgICAgICAgfSxcXG5cXG4gICAgICAgIG1vdW50ZWQoKSB7XFxuXFxuICAgICAgICBcXHQkKCcjY2xpZW50LXNlcnZpY2VzLW1vZGFsJykub24oJ2hpZGRlbi5icy5tb2RhbCcsIGZ1bmN0aW9uIChlKSB7XFxuXFx0XFx0XFx0XFx0XFxuXFx0XFx0XFx0XFx0dGhpcy5zY3JlZW4gPSAxO1xcblxcdFxcdFxcdFxcdFxcblxcdFxcdFxcdFxcdHRoaXMuc2VsZWN0ZWRDbGllbnRzSWQgPSBbXTtcXG5cXHRcXHRcXHRcXHR0aGlzLnNlbGVjdGVkQ2xpZW50U2VydmljZXNJZCA9IFtdO1xcblxcdFxcdFxcdFxcdHRoaXMuc2VsZWN0ZWRUcmFja2luZ3MgPSBbXTtcXG5cXG5cXHRcXHRcXHRcXHR0aGlzLnVuY2hlY2tTZWxlY3RlZENsaWVudHNJZCgpO1xcblxcdFxcdFxcdFxcdHRoaXMudW5jaGVja1NlbGVjdGVkQ2xpZW50U2VydmljZXNJZCgpO1xcblxcblxcdFxcdFxcdH0uYmluZCh0aGlzKSk7XFxuXFxuICAgICAgICB9XFxuXFxuXFx0fVxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG5cXHQuc3BhY2VyLTEwIHtcXG5cXHRcXHRjbGVhcjogYm90aDtcXG5cXHRcXHRoZWlnaHQ6IDEwcHg7XFxuXFx0fVxcblxcdC5jdXJzb3Ige1xcblxcdFxcdGN1cnNvcjogcG9pbnRlcjtcXG5cXHR9XFxuXFx0LnNjcm9sbGFibGUge1xcblxcdFxcdG1heC1oZWlnaHQ6IDQ1MHB4O1xcblxcdFxcdG92ZXJmbG93LXk6IGF1dG87XFxuXFx0fVxcblxcdC5jaGlsZC1yb3ctaGVhZGVyIHtcXG5cXHRcXHRjb2xvcjogIzNjOGRiYztcXG5cXHR9XFxuPC9zdHlsZT5cIl19XSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWM0MzBkYWRhXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQ2xpZW50U2VydmljZXMudnVlXG4vLyBtb2R1bGUgaWQgPSAzMTJcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbKF92bS5sb2cueWVhciAhPSBudWxsKSA/IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93IHRpbWVsaW5lLW1vdmVtZW50IHRpbWVsaW5lLW1vdmVtZW50LXRvcCBuby1kYXNoZWQgbm8tc2lkZS1tYXJnaW5cIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0aW1lbGluZS1iYWRnZSB0aW1lbGluZS1maWx0ZXItbW92ZW1lbnRcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIiNcIlxuICAgIH1cbiAgfSwgW19jKCdzcGFuJywgW192bS5fdihfdm0uX3MoX3ZtLmxvZy55ZWFyKSldKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvdyB0aW1lbGluZS1tb3ZlbWVudCAgbm8tc2lkZS1tYXJnaW5cIlxuICB9LCBbKF92bS5sb2cubW9udGggIT0gbnVsbCkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLWJhZGdlXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLWJhbGxvb24tZGF0ZS1kYXlcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubG9nLmRheSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRpbWVsaW5lLWJhbGxvb24tZGF0ZS1tb250aFwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sb2cubW9udGgpKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTIgIHRpbWVsaW5lLWl0ZW1cIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tb2Zmc2V0LTEgY29sLXNtLTExXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGltZWxpbmUtcGFuZWwgZGViaXRzXCJcbiAgfSwgW19jKCd1bCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0aW1lbGluZS1wYW5lbC11bFwiXG4gIH0sIFtfYygnbGknLCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1wb3J0b1wiLFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcImlubmVySFRNTFwiOiBfdm0uX3MoX3ZtLmxvZy5kYXRhLnRpdGxlKVxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhdXNhbGVcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubG9nLmRhdGEucHJvY2Vzc29yKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCBbX2MoJ3AnLCBbX2MoJ3NtYWxsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtbXV0ZWRcIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi10aW1lXCJcbiAgfSksIF92bS5fdihfdm0uX3MoX3ZtLmxvZy5kYXRhLmRhdGUpKV0pXSldKV0pXSldKV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTcyYjc1NWI1XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNzJiNzU1YjVcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9BY3Rpb25Mb2dzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzJcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi01MTk5YThhOFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZEZ1bmQudnVlXCIpXG5cbnZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0FkZEZ1bmQudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi01MTk5YThhOFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9BZGRGdW5kLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi01MTk5YThhOFwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZEZ1bmQudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQWRkRnVuZC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNTE5OWE4YThcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi01MTk5YThhOFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRnVuZC52dWVcbi8vIG1vZHVsZSBpZCA9IDMzNFxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03M2RmNzJmYVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZE5ld01lbWJlci52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQWRkTmV3TWVtYmVyLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNzNkZjcyZmFcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQWRkTmV3TWVtYmVyLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi03M2RmNzJmYVwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZE5ld01lbWJlci52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBBZGROZXdNZW1iZXIudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTczZGY3MmZhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNzNkZjcyZmFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZE5ld01lbWJlci52dWVcbi8vIG1vZHVsZSBpZCA9IDMzNVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zOTI4ZmE5Y1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZE5ld1NlcnZpY2UudnVlXCIpXG5cbnZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0FkZE5ld1NlcnZpY2UudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zOTI4ZmE5Y1xcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9BZGROZXdTZXJ2aWNlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi0zOTI4ZmE5Y1wiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZE5ld1NlcnZpY2UudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQWRkTmV3U2VydmljZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMzkyOGZhOWNcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0zOTI4ZmE5Y1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkTmV3U2VydmljZS52dWVcbi8vIG1vZHVsZSBpZCA9IDMzNlxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1jNDMwZGFkYVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0NsaWVudFNlcnZpY2VzLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9DbGllbnRTZXJ2aWNlcy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LWM0MzBkYWRhXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0NsaWVudFNlcnZpY2VzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi1jNDMwZGFkYVwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0NsaWVudFNlcnZpY2VzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIENsaWVudFNlcnZpY2VzLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1jNDMwZGFkYVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWM0MzBkYWRhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9DbGllbnRTZXJ2aWNlcy52dWVcbi8vIG1vZHVsZSBpZCA9IDM0MFxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zY2E5NGJlZlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0RlbGV0ZU1lbWJlci52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRGVsZXRlTWVtYmVyLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtM2NhOTRiZWZcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRGVsZXRlTWVtYmVyLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi0zY2E5NGJlZlwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0RlbGV0ZU1lbWJlci52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBEZWxldGVNZW1iZXIudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTNjYTk0YmVmXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtM2NhOTRiZWZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0RlbGV0ZU1lbWJlci52dWVcbi8vIG1vZHVsZSBpZCA9IDM0MlxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0yN2FiODIzMFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0VkaXRBZGRyZXNzLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9FZGl0QWRkcmVzcy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTI3YWI4MjMwXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0VkaXRBZGRyZXNzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi0yN2FiODIzMFwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRBZGRyZXNzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEVkaXRBZGRyZXNzLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0yN2FiODIzMFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTI3YWI4MjMwXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0QWRkcmVzcy52dWVcbi8vIG1vZHVsZSBpZCA9IDM0NFxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wNmQyNjQ3MVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0VkaXRTZXJ2aWNlLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9FZGl0U2VydmljZS52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTA2ZDI2NDcxXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0VkaXRTZXJ2aWNlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi0wNmQyNjQ3MVwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRTZXJ2aWNlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEVkaXRTZXJ2aWNlLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0wNmQyNjQ3MVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTA2ZDI2NDcxXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0U2VydmljZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM0NVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0xNjhmMmUwZVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0dyb3VwTWVtYmVyVHlwZS52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vR3JvdXBNZW1iZXJUeXBlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMTY4ZjJlMGVcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vR3JvdXBNZW1iZXJUeXBlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi0xNjhmMmUwZVwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0dyb3VwTWVtYmVyVHlwZS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBHcm91cE1lbWJlclR5cGUudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTE2OGYyZTBlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMTY4ZjJlMGVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0dyb3VwTWVtYmVyVHlwZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM0N1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1iYmQyZWM5NlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL1RyYW5zZmVyLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9UcmFuc2Zlci52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LWJiZDJlYzk2XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1RyYW5zZmVyLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi1iYmQyZWM5NlwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1RyYW5zZmVyLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFRyYW5zZmVyLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1iYmQyZWM5NlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWJiZDJlYzk2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9UcmFuc2Zlci52dWVcbi8vIG1vZHVsZSBpZCA9IDM0OFxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5zY3JlZW4gPT0gMSksXG4gICAgICBleHByZXNzaW9uOiBcInNjcmVlbiA9PSAxXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZS1yZXNwb25zaXZlXCJcbiAgfSwgW19jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmVkIHRhYmxlLWhvdmVyIGNsaWVudHMtZGF0YXRhYmxlXCJcbiAgfSwgW19jKCd0aGVhZCcsIFtfYygndHInLCBbX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiY2hlY2tib3hcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLnRvZ2dsZUFsbENoZWNrYm94XG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiTmFtZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiQ2xpZW50IElEXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJQYWNrYWdlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJEYXRlIFJlY29yZGVkXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJEZXRhaWxzXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJTdGF0dXNcIildKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0Ym9keScsIF92bS5fbCgoX3ZtLmNsaWVudFNlcnZpY2VzKSwgZnVuY3Rpb24oY2xpZW50U2VydmljZSkge1xuICAgIHJldHVybiBfYygndHInLCBbX2MoJ3RkJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwidGV4dC1jZW50ZXJcIlxuICAgIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKF92bS5lZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZCksXG4gICAgICAgIGV4cHJlc3Npb246IFwiZWRpdFNlcnZpY2VGb3JtLmNsaWVudFNlcnZpY2VzSWRcIlxuICAgICAgfV0sXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBjbGllbnRTZXJ2aWNlLmlkLFxuICAgICAgICBcImNoZWNrZWRcIjogQXJyYXkuaXNBcnJheShfdm0uZWRpdFNlcnZpY2VGb3JtLmNsaWVudFNlcnZpY2VzSWQpID8gX3ZtLl9pKF92bS5lZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZCwgY2xpZW50U2VydmljZS5pZCkgPiAtMSA6IChfdm0uZWRpdFNlcnZpY2VGb3JtLmNsaWVudFNlcnZpY2VzSWQpXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgdmFyICQkYSA9IF92bS5lZGl0U2VydmljZUZvcm0uY2xpZW50U2VydmljZXNJZCxcbiAgICAgICAgICAgICQkZWwgPSAkZXZlbnQudGFyZ2V0LFxuICAgICAgICAgICAgJCRjID0gJCRlbC5jaGVja2VkID8gKHRydWUpIDogKGZhbHNlKTtcbiAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSgkJGEpKSB7XG4gICAgICAgICAgICB2YXIgJCR2ID0gY2xpZW50U2VydmljZS5pZCxcbiAgICAgICAgICAgICAgJCRpID0gX3ZtLl9pKCQkYSwgJCR2KTtcbiAgICAgICAgICAgIGlmICgkJGVsLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICAgJCRpIDwgMCAmJiAoX3ZtLiRzZXQoX3ZtLmVkaXRTZXJ2aWNlRm9ybSwgXCJjbGllbnRTZXJ2aWNlc0lkXCIsICQkYS5jb25jYXQoWyQkdl0pKSlcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICQkaSA+IC0xICYmIChfdm0uJHNldChfdm0uZWRpdFNlcnZpY2VGb3JtLCBcImNsaWVudFNlcnZpY2VzSWRcIiwgJCRhLnNsaWNlKDAsICQkaSkuY29uY2F0KCQkYS5zbGljZSgkJGkgKyAxKSkpKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNlcnZpY2VGb3JtLCBcImNsaWVudFNlcnZpY2VzSWRcIiwgJCRjKVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKGNsaWVudFNlcnZpY2UudXNlci5mdWxsX25hbWUpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhjbGllbnRTZXJ2aWNlLnVzZXIuaWQpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhjbGllbnRTZXJ2aWNlLnRyYWNraW5nKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoY2xpZW50U2VydmljZS5zZXJ2aWNlX2RhdGUpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhjbGllbnRTZXJ2aWNlLmRldGFpbCkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKGNsaWVudFNlcnZpY2Uuc3RhdHVzKSldKV0pXG4gIH0pLCAwKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5jaGFuZ2VTY3JlZW4oMilcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDb250aW51ZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2JyJyldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gMlwiXG4gICAgfV1cbiAgfSwgW19jKCdmb3JtJywge1xuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0uZWRpdFNlcnZpY2UoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTZcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2FkZGl0aW9uYWxfY2hhcmdlJylcbiAgICB9XG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZm9yXCI6IFwiYWRkaXRvbmFsX2NoYXJnZVwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQWRkaXRpb25hbCBDaGFyZ2VcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5hZGRpdGlvbmFsX2NoYXJnZSksXG4gICAgICBleHByZXNzaW9uOiBcImVkaXRTZXJ2aWNlRm9ybS5hZGRpdGlvbmFsX2NoYXJnZVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJhZGRpdGlvbmFsX2NoYXJnZVwiLFxuICAgICAgXCJuYW1lXCI6IFwiYWRkaXRpb25hbF9jaGFyZ2VcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5lZGl0U2VydmljZUZvcm0uYWRkaXRpb25hbF9jaGFyZ2UpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNlcnZpY2VGb3JtLCBcImFkZGl0aW9uYWxfY2hhcmdlXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgKF92bS5lZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygnYWRkaXRpb25hbF9jaGFyZ2UnKSkgPyBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5lZGl0U2VydmljZUZvcm0uZXJyb3JzLmdldCgnYWRkaXRpb25hbF9jaGFyZ2UnKSlcbiAgICB9XG4gIH0pIDogX3ZtLl9lKCldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2Nvc3QnKVxuICAgIH1cbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJmb3JcIjogXCJjb3N0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDb3N0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5lZGl0U2VydmljZUZvcm0uY29zdCksXG4gICAgICBleHByZXNzaW9uOiBcImVkaXRTZXJ2aWNlRm9ybS5jb3N0XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImNvc3RcIixcbiAgICAgIFwibmFtZVwiOiBcImNvc3RcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5lZGl0U2VydmljZUZvcm0uY29zdClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5lZGl0U2VydmljZUZvcm0sIFwiY29zdFwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIChfdm0uZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2Nvc3QnKSkgPyBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5lZGl0U2VydmljZUZvcm0uZXJyb3JzLmdldCgnY29zdCcpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIixcbiAgICBjbGFzczoge1xuICAgICAgJ2hhcy1lcnJvcic6IF92bS5lZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygnZGlzY291bnQnKVxuICAgIH1cbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJmb3JcIjogXCJjb3N0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJEaXNjb3VudFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uZWRpdFNlcnZpY2VGb3JtLmRpc2NvdW50KSxcbiAgICAgIGV4cHJlc3Npb246IFwiZWRpdFNlcnZpY2VGb3JtLmRpc2NvdW50XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRpc2NvdW50XCIsXG4gICAgICBcIm5hbWVcIjogXCJkaXNjb3VudFwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5kaXNjb3VudClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5lZGl0U2VydmljZUZvcm0sIFwiZGlzY291bnRcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdkaXNjb3VudCcpKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdkaXNjb3VudCcpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTZcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJmb3JcIjogXCJzdGF0dXNcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlN0YXR1c1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc2VsZWN0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5zdGF0dXMpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2VydmljZUZvcm0uc3RhdHVzXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcInN0YXR1c1wiLFxuICAgICAgXCJuYW1lXCI6IFwic3RhdHVzXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkc2VsZWN0ZWRWYWwgPSBBcnJheS5wcm90b3R5cGUuZmlsdGVyLmNhbGwoJGV2ZW50LnRhcmdldC5vcHRpb25zLCBmdW5jdGlvbihvKSB7XG4gICAgICAgICAgcmV0dXJuIG8uc2VsZWN0ZWRcbiAgICAgICAgfSkubWFwKGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICB2YXIgdmFsID0gXCJfdmFsdWVcIiBpbiBvID8gby5fdmFsdWUgOiBvLnZhbHVlO1xuICAgICAgICAgIHJldHVybiB2YWxcbiAgICAgICAgfSk7XG4gICAgICAgIF92bS4kc2V0KF92bS5lZGl0U2VydmljZUZvcm0sIFwic3RhdHVzXCIsICRldmVudC50YXJnZXQubXVsdGlwbGUgPyAkJHNlbGVjdGVkVmFsIDogJCRzZWxlY3RlZFZhbFswXSlcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwiY29tcGxldGVcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNvbXBsZXRlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJvbiBwcm9jZXNzXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJPbiBQcm9jZXNzXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJwZW5kaW5nXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJQZW5kaW5nXCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImZvclwiOiBcIm5vdGVcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIk5vdGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RleHRhcmVhJywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5ub3RlKSxcbiAgICAgIGV4cHJlc3Npb246IFwiZWRpdFNlcnZpY2VGb3JtLm5vdGVcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwibm90ZVwiLFxuICAgICAgXCJuYW1lXCI6IFwibm90ZVwiLFxuICAgICAgXCJwbGFjZWhvbGRlclwiOiBcImZvciBpbnRlcm5hbCB1c2Ugb25seSwgb25seSBlbXBsb3llZXMgY2FuIHNlZS5cIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5lZGl0U2VydmljZUZvcm0ubm90ZSlcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5lZGl0U2VydmljZUZvcm0sIFwibm90ZVwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luLXRvcFwiOiBcIjIycHhcIlxuICAgIH1cbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJmb3JcIjogXCJhY3RpdmVcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyYWRpby1pbmxpbmVcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5hY3RpdmUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2VydmljZUZvcm0uYWN0aXZlXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwicmFkaW9cIixcbiAgICAgIFwiaWRcIjogXCJhY3RpdmVcIixcbiAgICAgIFwibmFtZVwiOiBcImFjdGl2ZVwiLFxuICAgICAgXCJ2YWx1ZVwiOiBcIjFcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBfdm0uX3EoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5hY3RpdmUsIFwiMVwiKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNlcnZpY2VGb3JtLCBcImFjdGl2ZVwiLCBcIjFcIilcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgRW5hYmxlXFxuXFx0XFx0XFx0XFx0XFx0XFx0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyYWRpby1pbmxpbmVcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5hY3RpdmUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2VydmljZUZvcm0uYWN0aXZlXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwicmFkaW9cIixcbiAgICAgIFwiaWRcIjogXCJhY3RpdmVcIixcbiAgICAgIFwibmFtZVwiOiBcImFjdGl2ZVwiLFxuICAgICAgXCJ2YWx1ZVwiOiBcIjBcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBfdm0uX3EoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5hY3RpdmUsIFwiMFwiKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNlcnZpY2VGb3JtLCBcImFjdGl2ZVwiLCBcIjBcIilcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgRGlzYWJsZVxcblxcdFxcdFxcdFxcdFxcdFxcdFwiKV0pXSksIF92bS5fdihcIiBcIiksIChfdm0uZWRpdFNlcnZpY2VGb3JtLmRpc2NvdW50ID4gMCkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIixcbiAgICBjbGFzczoge1xuICAgICAgJ2hhcy1lcnJvcic6IF92bS5lZGl0U2VydmljZUZvcm0uZXJyb3JzLmhhcygncmVhc29uJylcbiAgICB9LFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIm1hcmdpbi10b3BcIjogXCIyNXB4XCJcbiAgICB9XG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZm9yXCI6IFwiY29zdFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiUmVhc29uXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5lZGl0U2VydmljZUZvcm0ucmVhc29uKSxcbiAgICAgIGV4cHJlc3Npb246IFwiZWRpdFNlcnZpY2VGb3JtLnJlYXNvblwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJyZWFzb25cIixcbiAgICAgIFwibmFtZVwiOiBcInJlYXNvblwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5yZWFzb24pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNlcnZpY2VGb3JtLCBcInJlYXNvblwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIChfdm0uZWRpdFNlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ3JlYXNvbicpKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLmVkaXRTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdyZWFzb24nKSlcbiAgICB9XG4gIH0pIDogX3ZtLl9lKCldKSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJzdWJtaXRcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlNhdmVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5jaGFuZ2VTY3JlZW4oMSlcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJCYWNrXCIpXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMDZkMjY0NzFcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0wNmQyNjQ3MVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRTZXJ2aWNlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzc4XG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2JywgW19jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNlwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBidG4tYmxvY2tcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uY2hhbmdlTWVtYmVyVHlwZSgnbGVhZGVyJylcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1zdGFyIGZhLWZ3XCJcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXN0YXIgZmEtZndcIlxuICB9KSwgX3ZtLl92KFwiIE1ha2UgR3JvdXAgTGVhZGVyXFxuXFx0XFx0XFx0XFx0XCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNlwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4taW5mbyBidG4tYmxvY2tcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uY2hhbmdlTWVtYmVyVHlwZSgnbWVtYmVyJylcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1zdGFyLW8gZmEtZndcIlxuICB9KSwgX3ZtLl92KFwiIE1ha2UgR3JvdXAgTWVtYmVyXCIpXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0xNjhmMmUwZVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTE2OGYyZTBlXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvR3JvdXBNZW1iZXJUeXBlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzgwXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmVkaXRBZGRyZXNzKCRldmVudClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIixcbiAgICBjbGFzczoge1xuICAgICAgJ2hhcy1lcnJvcic6IF92bS5lZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnYWRkcmVzcycpXG4gICAgfVxuICB9LCBbX3ZtLl9tKDApLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uZWRpdEFkZHJlc3NGb3JtLm5hbWUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0QWRkcmVzc0Zvcm0ubmFtZVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIG0tdG9wLTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJuYW1lXCI6IFwibmFtZVwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmVkaXRBZGRyZXNzRm9ybS5uYW1lKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmVkaXRBZGRyZXNzRm9ybSwgXCJuYW1lXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgKF92bS5lZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmhhcygnbmFtZScpKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLmVkaXRBZGRyZXNzRm9ybS5lcnJvcnMuZ2V0KCduYW1lJykpXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5oYXMoJ2FkZHJlc3MnKVxuICAgIH1cbiAgfSwgW192bS5fbSgxKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRBZGRyZXNzRm9ybS5udW1iZXIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0QWRkcmVzc0Zvcm0ubnVtYmVyXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgbS10b3AtMFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJudW1iZXJcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5lZGl0QWRkcmVzc0Zvcm0ubnVtYmVyKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmVkaXRBZGRyZXNzRm9ybSwgXCJudW1iZXJcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmVkaXRBZGRyZXNzRm9ybS5lcnJvcnMuaGFzKCdudW1iZXInKSkgPyBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5lZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmdldCgnbnVtYmVyJykpXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5oYXMoJ2FkZHJlc3MnKVxuICAgIH1cbiAgfSwgW192bS5fbSgyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRBZGRyZXNzRm9ybS5hZGRyZXNzKSxcbiAgICAgIGV4cHJlc3Npb246IFwiZWRpdEFkZHJlc3NGb3JtLmFkZHJlc3NcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBtLXRvcC0wXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcImFkZHJlc3NcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5lZGl0QWRkcmVzc0Zvcm0uYWRkcmVzcylcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5lZGl0QWRkcmVzc0Zvcm0sIFwiYWRkcmVzc1wiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIChfdm0uZWRpdEFkZHJlc3NGb3JtLmVycm9ycy5oYXMoJ2FkZHJlc3MnKSkgPyBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5lZGl0QWRkcmVzc0Zvcm0uZXJyb3JzLmdldCgnYWRkcmVzcycpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJFZGl0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgTmFtZVxcbiAgICAgICAgICAgIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICBOdW1iZXJcXG4gICAgICAgICAgICBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgQWRkcmVzc1xcbiAgICAgICAgICAgIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0yN2FiODIzMFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTI3YWI4MjMwXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRWRpdEFkZHJlc3MudnVlXG4vLyBtb2R1bGUgaWQgPSAzODNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0uYWRkTmV3U2VydmljZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gMVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnaGFzLWVycm9yJzogX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ3NlcnZpY2VzJylcbiAgICB9XG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgICAgICBTZXJ2aWNlOlxcblxcdFxcdCAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZVwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMzUwcHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1wbGFjZWhvbGRlclwiOiBcIlNlbGVjdCBTZXJ2aWNlXCIsXG4gICAgICBcIm11bHRpcGxlXCI6IFwiXCIsXG4gICAgICBcInRhYmluZGV4XCI6IFwiNFwiXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5zZXJ2aWNlc0xpc3QpLCBmdW5jdGlvbihzZXJ2aWNlKSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IHNlcnZpY2UuaWRcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdFwiICsgX3ZtLl9zKChzZXJ2aWNlLmRldGFpbCkudHJpbSgpKSArIFwiXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFwiKV0pXG4gIH0pLCAwKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuaGFzKCdzZXJ2aWNlcycpKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5nZXQoJ3NlcnZpY2VzJykpXG4gICAgfVxuICB9KSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNwYWNlci0xMFwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX20oMCldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICAgICAgTm90ZTpcXG5cXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkTmV3U2VydmljZUZvcm0ubm90ZSksXG4gICAgICBleHByZXNzaW9uOiBcImFkZE5ld1NlcnZpY2VGb3JtLm5vdGVcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJub3RlXCIsXG4gICAgICBcInBsYWNlaG9sZGVyXCI6IFwiZm9yIGludGVybmFsIHVzZSBvbmx5LCBvbmx5IGVtcGxveWVlcyBjYW4gc2VlLlwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLm5vdGUpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uYWRkTmV3U2VydmljZUZvcm0sIFwibm90ZVwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5zZXJ2aWNlcy5sZW5ndGggPD0gMSksXG4gICAgICBleHByZXNzaW9uOiBcImFkZE5ld1NlcnZpY2VGb3JtLnNlcnZpY2VzLmxlbmd0aCA8PSAxXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgICAgIENvc3Q6XFxuXFx0XFx0XFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJjb3N0XCIsXG4gICAgICBcInJlYWRvbmx5XCI6IFwiXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IF92bS5jb3N0XG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnaGFzLWVycm9yJzogX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmVycm9ycy5oYXMoJ2Rpc2NvdW50JylcbiAgICB9XG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgICAgICBEaXNjb3VudDpcXG5cXHRcXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC00XCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudCksXG4gICAgICBleHByZXNzaW9uOiBcImFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJuYW1lXCI6IFwiZGlzY291bnRcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5kaXNjb3VudClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5hZGROZXdTZXJ2aWNlRm9ybSwgXCJkaXNjb3VudFwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIChfdm0uYWRkTmV3U2VydmljZUZvcm0uZXJyb3JzLmhhcygnZGlzY291bnQnKSkgPyBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5lcnJvcnMuZ2V0KCdkaXNjb3VudCcpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmRpc2NvdW50ICE9ICcnKSA/IF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgICAgIFJlYXNvbjpcXG5cXHRcXHRcXHQgICAgICAgIFwiKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uYWRkTmV3U2VydmljZUZvcm0uZGlzY291bnQgIT0gJycpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkTmV3U2VydmljZUZvcm0ucmVhc29uKSxcbiAgICAgIGV4cHJlc3Npb246IFwiYWRkTmV3U2VydmljZUZvcm0ucmVhc29uXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJuYW1lXCI6IFwicmVhc29uXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uYWRkTmV3U2VydmljZUZvcm0ucmVhc29uKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLCBcInJlYXNvblwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSldKSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmNoYW5nZVNjcmVlbigyKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihcIkNvbnRpbnVlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLnNjcmVlbiA9PSAyKSxcbiAgICAgIGV4cHJlc3Npb246IFwic2NyZWVuID09IDJcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYmxlLXJlc3BvbnNpdmVcIlxuICB9LCBbX2MoJ3RhYmxlJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVyZWQgdGFibGUtaG92ZXIgYWRkLW5ldy1zZXJ2aWNlLWRhdGF0YWJsZVwiXG4gIH0sIFtfYygndGhlYWQnLCBbX2MoJ3RyJywgW19jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImNoZWNrYm94XCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS50b2dnbGVBbGxDaGVja2JveFxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIk5hbWVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkNsaWVudCBJRFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiUGFja2FnZXNcIildKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0Ym9keScsIF92bS5fbCgoX3ZtLmNsaWVudHMpLCBmdW5jdGlvbihkYXRhKSB7XG4gICAgcmV0dXJuIF9jKCd0cicsIFtfYygndGQnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiXG4gICAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgIHZhbHVlOiAoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHMpLFxuICAgICAgICBleHByZXNzaW9uOiBcImFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHNcIlxuICAgICAgfV0sXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBkYXRhLmNsaWVudC5pZCxcbiAgICAgICAgXCJjaGVja2VkXCI6IEFycmF5LmlzQXJyYXkoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHMpID8gX3ZtLl9pKF92bS5hZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzLCBkYXRhLmNsaWVudC5pZCkgPiAtMSA6IChfdm0uYWRkTmV3U2VydmljZUZvcm0uY2xpZW50cylcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICB2YXIgJCRhID0gX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHMsXG4gICAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAgICQkYyA9ICQkZWwuY2hlY2tlZCA/ICh0cnVlKSA6IChmYWxzZSk7XG4gICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgICAgdmFyICQkdiA9IGRhdGEuY2xpZW50LmlkLFxuICAgICAgICAgICAgICAkJGkgPSBfdm0uX2koJCRhLCAkJHYpO1xuICAgICAgICAgICAgaWYgKCQkZWwuY2hlY2tlZCkge1xuICAgICAgICAgICAgICAkJGkgPCAwICYmIChfdm0uJHNldChfdm0uYWRkTmV3U2VydmljZUZvcm0sIFwiY2xpZW50c1wiLCAkJGEuY29uY2F0KFskJHZdKSkpXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAkJGkgPiAtMSAmJiAoX3ZtLiRzZXQoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLCBcImNsaWVudHNcIiwgJCRhLnNsaWNlKDAsICQkaSkuY29uY2F0KCQkYS5zbGljZSgkJGkgKyAxKSkpKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfdm0uJHNldChfdm0uYWRkTmV3U2VydmljZUZvcm0sIFwiY2xpZW50c1wiLCAkJGMpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoZGF0YS5jbGllbnQuZnVsbF9uYW1lKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoZGF0YS5jbGllbnQuaWQpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX2MoJ3NlbGVjdCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgICAgY2xhc3M6ICdwYWNrYWdlLXNlbGVjdC0nICsgZGF0YS5jbGllbnQuaWQsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImRpc2FibGVkXCI6IF92bS5hZGROZXdTZXJ2aWNlRm9ybS5jbGllbnRzLmluZGV4T2YoZGF0YS5jbGllbnQuaWQpID09IC0xXG4gICAgICB9XG4gICAgfSwgW19jKCdvcHRpb24nLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcInZhbHVlXCI6IFwiR2VuZXJhdGUgbmV3IHBhY2thZ2VcIlxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJHZW5lcmF0ZSBuZXcgcGFja2FnZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX2woKGRhdGEuY2xpZW50LnBhY2thZ2VzKSwgZnVuY3Rpb24ocGFja2FnZSkge1xuICAgICAgcmV0dXJuIChwYWNrYWdlLnN0YXR1cyAhPSA0KSA/IF9jKCdvcHRpb24nLCB7XG4gICAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgICAgXCJ2YWx1ZVwiOiBwYWNrYWdlLnRyYWNraW5nXG4gICAgICAgIH1cbiAgICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgIFxcdFxcdFxcdFxcdFxcdFwiICsgX3ZtLl9zKHBhY2thZ2UudHJhY2tpbmcpICsgXCIgXFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHRcXHQoXCIgKyBfdm0uX3MoX3ZtLl9mKFwiY29tbW9uRGF0ZVwiKShwYWNrYWdlLmxvZ19kYXRlKSkgKyBcIilcXG4gICAgICAgICAgICAgICAgICAgIFxcdFxcdFxcdFxcdFwiKV0pIDogX3ZtLl9lKClcbiAgICB9KV0sIDIpXSldKVxuICB9KSwgMCldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uY2hhbmdlU2NyZWVuKDMpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ29udGludWVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5jaGFuZ2VTY3JlZW4oMSlcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJCYWNrXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDMpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gM1wiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW9zLXNjcm9sbFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hlY2tib3gtaW5saW5lXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjaGVja2JveC1hbGxcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiY2hlY2tib3hcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLmNoZWNrQWxsXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIENoZWNrIGFsbCByZXF1aXJlZCBkb2N1bWVudHNcXG4gICAgXFx0XFx0XFx0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMjBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF92bS5fbCgoX3ZtLmFkZE5ld1NlcnZpY2VGb3JtLmNsaWVudHMpLCBmdW5jdGlvbihjbGllbnRJZCwgaW5kZXgxKSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWhlYWRpbmdcIlxuICAgIH0sIFtfYygnc3Ryb25nJywgW192bS5fdihfdm0uX3MoX3ZtLmdldENsaWVudE5hbWUoY2xpZW50SWQpKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiXG4gICAgfSwgX3ZtLl9sKChfdm0uYWRkTmV3U2VydmljZUZvcm0uc2VydmljZXMpLCBmdW5jdGlvbihzZXJ2aWNlSWQsIGluZGV4Mikge1xuICAgICAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsIHBhbmVsLWRlZmF1bHRcIlxuICAgICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWhlYWRpbmdcIlxuICAgICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmdldFNlcnZpY2VOYW1lKHNlcnZpY2VJZCkpICsgXCJcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgICAgIFwiKSwgX2MoJ2xhYmVsJywge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJjaGVja2JveC1pbmxpbmUgcHVsbC1yaWdodFwiXG4gICAgICB9LCBbX2MoJ2lucHV0Jywge1xuICAgICAgICBjbGFzczogJ2NoZWNrYm94LScgKyBpbmRleDEgKyAnLScgKyBpbmRleDIsXG4gICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgXCJ0eXBlXCI6IFwiY2hlY2tib3hcIixcbiAgICAgICAgICBcImRhdGEtY2hvc2VuLWNsYXNzXCI6ICdjaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLScgKyBpbmRleDEgKyAnLScgKyBpbmRleDJcbiAgICAgICAgfSxcbiAgICAgICAgb246IHtcbiAgICAgICAgICBcImNsaWNrXCI6IF92bS5jaGVja0FsbFxuICAgICAgICB9XG4gICAgICB9KSwgX3ZtLl92KFwiIENoZWNrIGFsbCByZXF1aXJlZCBkb2N1bWVudHNcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgICAgIFwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIlxuICAgICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICAgICAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gICAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgIFxcdFJlcXVpcmVkIERvY3VtZW50czpcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gICAgICB9LCBbX2MoJ3NlbGVjdCcsIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6IFwiY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jc1wiLFxuICAgICAgICBjbGFzczogJ2Nob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MtJyArIGluZGV4MSArICctJyArIGluZGV4MixcbiAgICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgICBcIndpZHRoXCI6IFwiMzUwcHhcIlxuICAgICAgICB9LFxuICAgICAgICBhdHRyczoge1xuICAgICAgICAgIFwiZGF0YS1wbGFjZWhvbGRlclwiOiBcIlNlbGVjdCBEb2NzXCIsXG4gICAgICAgICAgXCJtdWx0aXBsZVwiOiBcIlwiLFxuICAgICAgICAgIFwidGFiaW5kZXhcIjogXCI0XCJcbiAgICAgICAgfVxuICAgICAgfSwgX3ZtLl9sKChfdm0ucmVxdWlyZWREb2NzW2luZGV4Ml0pLCBmdW5jdGlvbihkb2MpIHtcbiAgICAgICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgICAgIFwidmFsdWVcIjogZG9jLmlkXG4gICAgICAgICAgfVxuICAgICAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdFwiICsgX3ZtLl9zKGRvYy50aXRsZSkgKyBcIlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICBcIildKVxuICAgICAgfSksIDApXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gICAgICB9LCBbX2MoJ2xhYmVsJywge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgXFx0T3B0aW9uYWwgRG9jdW1lbnRzOlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgICAgIH0sIFtfYygnc2VsZWN0Jywge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJjaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzXCIsXG4gICAgICAgIGNsYXNzOiAnY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcy0nICsgaW5kZXgxICsgJy0nICsgaW5kZXgyLFxuICAgICAgICBzdGF0aWNTdHlsZToge1xuICAgICAgICAgIFwid2lkdGhcIjogXCIzNTBweFwiXG4gICAgICAgIH0sXG4gICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgXCJkYXRhLXBsYWNlaG9sZGVyXCI6IFwiU2VsZWN0IERvY3NcIixcbiAgICAgICAgICBcIm11bHRpcGxlXCI6IFwiXCIsXG4gICAgICAgICAgXCJ0YWJpbmRleFwiOiBcIjRcIlxuICAgICAgICB9XG4gICAgICB9LCBfdm0uX2woKF92bS5vcHRpb25hbERvY3NbaW5kZXgyXSksIGZ1bmN0aW9uKGRvYykge1xuICAgICAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgXCJ2YWx1ZVwiOiBkb2MuaWRcbiAgICAgICAgICB9XG4gICAgICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0XCIgKyBfdm0uX3MoZG9jLnRpdGxlKSArIFwiXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIFwiKV0pXG4gICAgICB9KSwgMCldKV0pXSldKVxuICAgIH0pLCAwKV0pXG4gIH0pXSwgMiksIF92bS5fdihcIiBcIiksICghX3ZtLmxvYWRpbmcpID8gX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJzdWJtaXRcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlNhdmVcIildKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmxvYWRpbmcpID8gX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH1cbiAgfSwgW192bS5fbSgxKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uY2hhbmdlU2NyZWVuKDIpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQmFja1wiKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdwJywgW192bS5fdihcIlxcblxcdFxcdCAgICAgICAgICAgIFxcdElmIHlvdSB3YW50IHRvIHZpZXcgYWxsIHNlcnZpY2VzIGF0IG9uY2UsIHBsZWFzZSBcXG5cXHRcXHQgICAgICAgICAgICBcXHRcIiksIF9jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCIvdmlzYS9zZXJ2aWNlL3ZpZXctYWxsXCIsXG4gICAgICBcInRhcmdldFwiOiBcIl9ibGFua1wiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiY2xpY2sgaGVyZVwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNrLXNwaW5uZXIgc2stc3Bpbm5lci13YXZlXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwid2lkdGhcIjogXCI0MHB4XCIsXG4gICAgICBcImhlaWdodFwiOiBcIjIwcHhcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwic2stcmVjdDFcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzay1yZWN0MlwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNrLXJlY3QzXCJcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwic2stcmVjdDRcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzay1yZWN0NVwiXG4gIH0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0zOTI4ZmE5Y1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTM5MjhmYTljXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkTmV3U2VydmljZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM4NVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2Zvcm0nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ob3Jpem9udGFsXCIsXG4gICAgb246IHtcbiAgICAgIFwic3VibWl0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgcmV0dXJuIF92bS5kZWxldGVNZW1iZXIoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdwJywgW192bS5fdihcIkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhpcyBtZW1iZXI/XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJEZWxldGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtM2NhOTRiZWZcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zY2E5NGJlZlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0RlbGV0ZU1lbWJlci52dWVcbi8vIG1vZHVsZSBpZCA9IDM4OFxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2Zvcm0nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ob3Jpem9udGFsXCIsXG4gICAgb246IHtcbiAgICAgIFwic3VibWl0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgcmV0dXJuIF92bS5hZGRGdW5kcygkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdCAgICAgICAgT3B0aW9uOlxcbiAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkRnVuZEZvcm0ub3B0aW9uKSxcbiAgICAgIGV4cHJlc3Npb246IFwiYWRkRnVuZEZvcm0ub3B0aW9uXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJuYW1lXCI6IFwib3B0aW9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkc2VsZWN0ZWRWYWwgPSBBcnJheS5wcm90b3R5cGUuZmlsdGVyLmNhbGwoJGV2ZW50LnRhcmdldC5vcHRpb25zLCBmdW5jdGlvbihvKSB7XG4gICAgICAgICAgcmV0dXJuIG8uc2VsZWN0ZWRcbiAgICAgICAgfSkubWFwKGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICB2YXIgdmFsID0gXCJfdmFsdWVcIiBpbiBvID8gby5fdmFsdWUgOiBvLnZhbHVlO1xuICAgICAgICAgIHJldHVybiB2YWxcbiAgICAgICAgfSk7XG4gICAgICAgIF92bS4kc2V0KF92bS5hZGRGdW5kRm9ybSwgXCJvcHRpb25cIiwgJGV2ZW50LnRhcmdldC5tdWx0aXBsZSA/ICQkc2VsZWN0ZWRWYWwgOiAkJHNlbGVjdGVkVmFsWzBdKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJEZXBvc2l0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJEZXBvc2l0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJQYXltZW50XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJQYXltZW50XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJEaXNjb3VudFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiRGlzY291bnRcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ29wdGlvbicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJ2YWx1ZVwiOiBcIlJlZnVuZFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiUmVmdW5kXCIpXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnaGFzLWVycm9yJzogX3ZtLmFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ2Ftb3VudCcpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBBbW91bnQ6XFxuICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmFkZEZ1bmRGb3JtLmFtb3VudCksXG4gICAgICBleHByZXNzaW9uOiBcImFkZEZ1bmRGb3JtLmFtb3VudFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcImFtb3VudFwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmFkZEZ1bmRGb3JtLmFtb3VudClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5hZGRGdW5kRm9ybSwgXCJhbW91bnRcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ2Ftb3VudCcpKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLmFkZEZ1bmRGb3JtLmVycm9ycy5nZXQoJ2Ftb3VudCcpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pXSksIF92bS5fdihcIiBcIiksIChfdm0uYWRkRnVuZEZvcm0ub3B0aW9uID09ICdEaXNjb3VudCcgfHwgX3ZtLmFkZEZ1bmRGb3JtLm9wdGlvbiA9PSAnUmVmdW5kJykgPyBfYygnZGl2JywgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnaGFzLWVycm9yJzogX3ZtLmFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ3JlYXNvbicpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICBSZWFzb246XFxuXFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmFkZEZ1bmRGb3JtLnJlYXNvbiksXG4gICAgICBleHByZXNzaW9uOiBcImFkZEZ1bmRGb3JtLnJlYXNvblwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcInJlYXNvblwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmFkZEZ1bmRGb3JtLnJlYXNvbilcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5hZGRGdW5kRm9ybSwgXCJyZWFzb25cIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmFkZEZ1bmRGb3JtLmVycm9ycy5oYXMoJ3JlYXNvbicpKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLmFkZEZ1bmRGb3JtLmVycm9ycy5nZXQoJ3JlYXNvbicpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInN1Ym1pdFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQWRkXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTUxOTlhOGE4XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNTE5OWE4YThcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGRGdW5kLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzk1XG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmFkZE5ld01lbWJlcigkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uYWRkTmV3TWVtYmVyRm9ybS5lcnJvcnMuaGFzKCdjbGllbnRJZCcpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgQ2xpZW50c1xcbiAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hvc2VuLXNlbGVjdC1mb3ItY2xpZW50c1wiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMTAwJVwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLXBsYWNlaG9sZGVyXCI6IFwiU2VsZWN0IGNsaWVudHNcIixcbiAgICAgIFwibXVsdGlwbGVcIjogXCJcIlxuICAgIH1cbiAgfSwgX3ZtLl9sKChfdm0uY2xpZW50cyksIGZ1bmN0aW9uKGNsaWVudCkge1xuICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBjbGllbnQuaWRcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICAgICAgICAgXFx0XCIgKyBfdm0uX3MoY2xpZW50Lm5hbWUpICsgXCJcXG5cXHQgICAgICAgICAgICBcIildKVxuICB9KSwgMCldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInN1Ym1pdFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQWRkXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTczZGY3MmZhXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNzNkZjcyZmFcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGROZXdNZW1iZXIudnVlXG4vLyBtb2R1bGUgaWQgPSA0MDhcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gMVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTZcIlxuICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgYnRuLWJsb2NrXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmdldERldGFpbHMoJ2NsaWVudC10by1ncm91cCcsIDIpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xpZW50IHRvIEdyb3VwXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNlwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4taW5mbyBidG4tYmxvY2tcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZ2V0RGV0YWlscygnZ3JvdXAtdG8tY2xpZW50JywgMilcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJHcm91cCB0byBDbGllbnRcIildKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gMlwiXG4gICAgfV1cbiAgfSwgW19jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0udHJhbnNmZXIoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGFibGUtcmVzcG9uc2l2ZVwiXG4gIH0sIFtfYygndGFibGUnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJlZCB0YWJsZS1ob3ZlciB0cmFuc2Zlci1kYXRhdGFibGVcIlxuICB9LCBbX2MoJ3RoZWFkJywgW19jKCd0cicsIFtfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1jZW50ZXJcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0udG9nZ2xlQWxsQ2hlY2tib3hcbiAgICB9XG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJEZXRhaWxcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIlN0YXR1c1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiVHJhY2tpbmdcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIlBhY2thZ2VzXCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGJvZHknLCBfdm0uX2woKF92bS5zZXJ2aWNlcyksIGZ1bmN0aW9uKHNlcnZpY2UpIHtcbiAgICByZXR1cm4gX2MoJ3RyJywgW19jKCd0ZCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyXCJcbiAgICB9LCBbX2MoJ2lucHV0Jywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgdmFsdWU6IChfdm0udHJhbnNmZXJGb3JtLnNlcnZpY2VzKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJ0cmFuc2ZlckZvcm0uc2VydmljZXNcIlxuICAgICAgfV0sXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBzZXJ2aWNlLmlkLFxuICAgICAgICBcImNoZWNrZWRcIjogQXJyYXkuaXNBcnJheShfdm0udHJhbnNmZXJGb3JtLnNlcnZpY2VzKSA/IF92bS5faShfdm0udHJhbnNmZXJGb3JtLnNlcnZpY2VzLCBzZXJ2aWNlLmlkKSA+IC0xIDogKF92bS50cmFuc2ZlckZvcm0uc2VydmljZXMpXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgdmFyICQkYSA9IF92bS50cmFuc2ZlckZvcm0uc2VydmljZXMsXG4gICAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAgICQkYyA9ICQkZWwuY2hlY2tlZCA/ICh0cnVlKSA6IChmYWxzZSk7XG4gICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgICAgdmFyICQkdiA9IHNlcnZpY2UuaWQsXG4gICAgICAgICAgICAgICQkaSA9IF92bS5faSgkJGEsICQkdik7XG4gICAgICAgICAgICBpZiAoJCRlbC5jaGVja2VkKSB7XG4gICAgICAgICAgICAgICQkaSA8IDAgJiYgKF92bS4kc2V0KF92bS50cmFuc2ZlckZvcm0sIFwic2VydmljZXNcIiwgJCRhLmNvbmNhdChbJCR2XSkpKVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgJCRpID4gLTEgJiYgKF92bS4kc2V0KF92bS50cmFuc2ZlckZvcm0sIFwic2VydmljZXNcIiwgJCRhLnNsaWNlKDAsICQkaSkuY29uY2F0KCQkYS5zbGljZSgkJGkgKyAxKSkpKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfdm0uJHNldChfdm0udHJhbnNmZXJGb3JtLCBcInNlcnZpY2VzXCIsICQkYylcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhzZXJ2aWNlLmRldGFpbCkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKHNlcnZpY2Uuc3RhdHVzKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3Moc2VydmljZS50cmFja2luZykpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnc2VsZWN0Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICBjbGFzczogJ3RyYW5zZmVyLXBhY2thZ2Utc2VsZWN0LScgKyBzZXJ2aWNlLmlkXG4gICAgfSwgW19jKCdvcHRpb24nLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcInZhbHVlXCI6IFwiR2VuZXJhdGUgbmV3IHBhY2thZ2VcIlxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJHZW5lcmF0ZSBuZXcgcGFja2FnZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX2woKF92bS5wYWNrYWdlcyksIGZ1bmN0aW9uKHBhY2thZ2UpIHtcbiAgICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgIFwidmFsdWVcIjogcGFja2FnZS50cmFja2luZ1xuICAgICAgICB9XG4gICAgICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHRcXHRQYWNrYWdlICNcIiArIF92bS5fcyhwYWNrYWdlLnRyYWNraW5nKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICBcXHRcXHRcXHRcXHRcXHQoXCIgKyBfdm0uX3MoX3ZtLl9mKFwiY29tbW9uRGF0ZVwiKShwYWNrYWdlLmxvZ19kYXRlKSkgKyBcIilcXG4gICAgICAgICAgICAgICAgICAgIFxcdFxcdFxcdFxcdFwiKV0pXG4gICAgfSldLCAyKV0pXSlcbiAgfSksIDApXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJzdWJtaXRcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNvbXBsZXRlIFRyYW5zZmVyXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnNldFNjcmVlbigxKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihcIkJhY2tcIildKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1iYmQyZWM5NlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWJiZDJlYzk2XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvVHJhbnNmZXIudnVlXG4vLyBtb2R1bGUgaWQgPSA0MThcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gMVwiXG4gICAgfV1cbiAgfSwgW19jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiXG4gIH0sIFtfYygndGFibGUnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGFibGUgdGFibGUtYm9yZGVyZWQgdGFibGUtc3RyaXBlZCB0YWJsZS1ob3ZlciBtZW1iZXJzRGF0YXRhYmxlXCJcbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0ubWVtYmVycyksIGZ1bmN0aW9uKG1lbWJlcikge1xuICAgIHJldHVybiBfYygndHInLCBbX2MoJ3RkJywgW192bS5fdihfdm0uX3MobWVtYmVyLmNsaWVudC5pZCkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKG1lbWJlci5jbGllbnQuZmlyc3RfbmFtZSArICcgJyArIG1lbWJlci5jbGllbnQubGFzdF9uYW1lKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwidGV4dC1jZW50ZXJcIlxuICAgIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiLFxuICAgICAgICBcIm5hbWVcIjogXCJjbGllbnRzSWRbXVwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBtZW1iZXIuY2xpZW50LmlkXG4gICAgICB9XG4gICAgfSldKV0pXG4gIH0pLCAwKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNwYWNlci0xMFwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmdvdG9TY3JlZW4oMilcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDb250aW51ZVwiKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gMlwiXG4gICAgfV1cbiAgfSwgW19jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNjcm9sbGFibGVcIlxuICB9LCBbX2MoJ3RhYmxlJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYmxlIHRhYmxlLWJvcmRlcmVkIHRhYmxlLXN0cmlwZWQgdGFibGUtaG92ZXJcIlxuICB9LCBbX3ZtLl9tKDEpLCBfdm0uX3YoXCIgXCIpLCBfdm0uX2woKF92bS5tZW1iZXJzKSwgZnVuY3Rpb24obWVtYmVyLCBpbmRleCkge1xuICAgIHJldHVybiAoX3ZtLnNlbGVjdGVkQ2xpZW50c0lkLmluZGV4T2YobWVtYmVyLmNsaWVudC5pZCkgIT0gLTEpID8gX2MoJ3Rib2R5JywgW19jKCd0cicsIHtcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLnRvZ2dsZUNoaWxkUm93KCRldmVudCwgaW5kZXgpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX2MoJ3RkJywge1xuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJ3aWR0aFwiOiBcIjQ1JVwiXG4gICAgICB9XG4gICAgfSwgW192bS5fdihfdm0uX3MobWVtYmVyLmNsaWVudC5pZCkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwid2lkdGhcIjogXCI0NSVcIlxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoX3ZtLl9zKG1lbWJlci5jbGllbnQuZmlyc3RfbmFtZSArICcgJyArIG1lbWJlci5jbGllbnQubGFzdF9uYW1lKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwidGV4dC1jZW50ZXJcIixcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwid2lkdGhcIjogXCIxMCVcIlxuICAgICAgfVxuICAgIH0sIFtfYygnaScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWFycm93LXJpZ2h0IGN1cnNvclwiLFxuICAgICAgY2xhc3M6ICdhcnJvdy0nICsgaW5kZXgsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImlkXCI6IFwiZmEtMTAwMFwiXG4gICAgICB9XG4gICAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndHInLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJoaWRlXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImlkXCI6ICdjaGlsZC1yb3ctJyArIGluZGV4XG4gICAgICB9XG4gICAgfSwgW19jKCd0ZCcsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiY29sc3BhblwiOiBcIjNcIlxuICAgICAgfVxuICAgIH0sIFtfYygndGFibGUnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1ib3JkZXJlZCB0YWJsZS1zdHJpcGVkIHRhYmxlLWhvdmVyXCJcbiAgICB9LCBbX3ZtLl9tKDIsIHRydWUpLCBfdm0uX3YoXCIgXCIpLCBfYygndGJvZHknLCBfdm0uX2woKG1lbWJlci5zZXJ2aWNlcyksIGZ1bmN0aW9uKHNlcnZpY2UpIHtcbiAgICAgIHJldHVybiAoc2VydmljZS5ncm91cF9pZCA9PSBfdm0uZ3JvdXBpZCAmJiBzZXJ2aWNlLmFjdGl2ZSA9PSAxICYmIHNlcnZpY2Uuc3RhdHVzICE9ICdDb21wbGV0ZScpID8gX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKHNlcnZpY2Uuc2VydmljZV9kYXRlKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3Moc2VydmljZS50cmFja2luZykpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKHNlcnZpY2UuZGV0YWlsKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikoc2VydmljZS5jb3N0KSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHNlcnZpY2UuY2hhcmdlKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHNlcnZpY2UudGlwKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHNlcnZpY2UuZGlzY291bnRfYW1vdW50KSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKHNlcnZpY2Uuc3RhdHVzKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiXG4gICAgICB9LCBbX2MoJ2lucHV0Jywge1xuICAgICAgICBhdHRyczoge1xuICAgICAgICAgIFwidHlwZVwiOiBcImNoZWNrYm94XCIsXG4gICAgICAgICAgXCJuYW1lXCI6IFwiY2xpZW50U2VydmljZXNJZFtdXCJcbiAgICAgICAgfSxcbiAgICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgICBcInZhbHVlXCI6IHNlcnZpY2UuaWRcbiAgICAgICAgfVxuICAgICAgfSldKV0pIDogX3ZtLl9lKClcbiAgICB9KSwgMCldKV0pXSldKSA6IF92bS5fZSgpXG4gIH0pXSwgMildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzcGFjZXItMTBcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLnNldFNlbGVjdGVkQ2xpZW50U2VydmljZXNBbmRUcmFja2luZ3NJZFxuICAgIH1cbiAgfSwgW192bS5fdihcIkNvbnRpbnVlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmdvdG9TY3JlZW4oMSlcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJCYWNrXCIpXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygndGhlYWQnLCBbX2MoJ3RyJywgW19jKCd0aCcsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjQwJVwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ0xJRU5UIElEXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjQwJVwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiTkFNRVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwid2lkdGhcIjogXCIyMCVcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkFDVElPTlwiKV0pXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0aGVhZCcsIFtfYygndHInLCBbX2MoJ3RoJywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiNDUlXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDTElFTlQgSURcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiNDUlXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJOQU1FXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMTAlXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJBQ1RJT05cIildKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygndGhlYWQnLCBbX2MoJ3RyJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNoaWxkLXJvdy1oZWFkZXJcIlxuICB9LCBbX2MoJ3RoJywgW192bS5fdihcIkRhdGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIlBhY2thZ2VcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkRldGFpbHNcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkNvc3RcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkNoYXJnZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiVGlwXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJEaXNjb3VudFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiU3RhdHVzXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiXG4gIH0pXSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWM0MzBkYWRhXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYzQzMGRhZGFcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9DbGllbnRTZXJ2aWNlcy52dWVcbi8vIG1vZHVsZSBpZCA9IDQxOVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTA2ZDI2NDcxXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vRWRpdFNlcnZpY2UudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCIyNTFhYzMzNFwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMDZkMjY0NzFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9FZGl0U2VydmljZS52dWVcIiwgZnVuY3Rpb24oKSB7XG4gICAgIHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTA2ZDI2NDcxXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vRWRpdFNlcnZpY2UudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMDZkMjY0NzFcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0U2VydmljZS52dWVcbi8vIG1vZHVsZSBpZCA9IDQzMVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTE2OGYyZTBlXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vR3JvdXBNZW1iZXJUeXBlLnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiMjYyMTI4N2RcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTE2OGYyZTBlXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vR3JvdXBNZW1iZXJUeXBlLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMTY4ZjJlMGVcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9Hcm91cE1lbWJlclR5cGUudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMTY4ZjJlMGVcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9Hcm91cE1lbWJlclR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSA0MzNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0yN2FiODIzMFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0VkaXRBZGRyZXNzLnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiNTE5ZTA2MzhcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTI3YWI4MjMwXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vRWRpdEFkZHJlc3MudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0yN2FiODIzMFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0VkaXRBZGRyZXNzLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTI3YWI4MjMwXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRWRpdEFkZHJlc3MudnVlXG4vLyBtb2R1bGUgaWQgPSA0MzVcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zOTI4ZmE5Y1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZE5ld1NlcnZpY2UudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCI0NTEyYWNmOFwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMzkyOGZhOWNcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGROZXdTZXJ2aWNlLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMzkyOGZhOWNcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGROZXdTZXJ2aWNlLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTM5MjhmYTljXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkTmV3U2VydmljZS52dWVcbi8vIG1vZHVsZSBpZCA9IDQzNlxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTNjYTk0YmVmXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vRGVsZXRlTWVtYmVyLnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiZmQzY2Q5OGFcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTNjYTk0YmVmXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vRGVsZXRlTWVtYmVyLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtM2NhOTRiZWZcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9EZWxldGVNZW1iZXIudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtM2NhOTRiZWZcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVNZW1iZXIudnVlXG4vLyBtb2R1bGUgaWQgPSA0Mzlcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi01MTk5YThhOFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZEZ1bmQudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCIzYmY2OWVjYVwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNTE5OWE4YThcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGRGdW5kLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNTE5OWE4YThcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGRGdW5kLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTUxOTlhOGE4XCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRnVuZC52dWVcbi8vIG1vZHVsZSBpZCA9IDQ0MFxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTczZGY3MmZhXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vQWRkTmV3TWVtYmVyLnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiNjBkY2ZmNzhcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTczZGY3MmZhXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vQWRkTmV3TWVtYmVyLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNzNkZjcyZmFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGROZXdNZW1iZXIudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNzNkZjcyZmFcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGROZXdNZW1iZXIudnVlXG4vLyBtb2R1bGUgaWQgPSA0NDVcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1iYmQyZWM5NlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL1RyYW5zZmVyLnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiNWQyYWM3MjdcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LWJiZDJlYzk2XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vVHJhbnNmZXIudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1iYmQyZWM5NlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL1RyYW5zZmVyLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWJiZDJlYzk2XCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvVHJhbnNmZXIudnVlXG4vLyBtb2R1bGUgaWQgPSA0NDlcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1jNDMwZGFkYVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0NsaWVudFNlcnZpY2VzLnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiMjM2ZTdiYzhcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LWM0MzBkYWRhXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vQ2xpZW50U2VydmljZXMudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1jNDMwZGFkYVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0NsaWVudFNlcnZpY2VzLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWM0MzBkYWRhXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQ2xpZW50U2VydmljZXMudnVlXG4vLyBtb2R1bGUgaWQgPSA0NTBcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiLyoqXG4gKiBUcmFuc2xhdGVzIHRoZSBsaXN0IGZvcm1hdCBwcm9kdWNlZCBieSBjc3MtbG9hZGVyIGludG8gc29tZXRoaW5nXG4gKiBlYXNpZXIgdG8gbWFuaXB1bGF0ZS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBsaXN0VG9TdHlsZXMgKHBhcmVudElkLCBsaXN0KSB7XG4gIHZhciBzdHlsZXMgPSBbXVxuICB2YXIgbmV3U3R5bGVzID0ge31cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldXG4gICAgdmFyIGlkID0gaXRlbVswXVxuICAgIHZhciBjc3MgPSBpdGVtWzFdXG4gICAgdmFyIG1lZGlhID0gaXRlbVsyXVxuICAgIHZhciBzb3VyY2VNYXAgPSBpdGVtWzNdXG4gICAgdmFyIHBhcnQgPSB7XG4gICAgICBpZDogcGFyZW50SWQgKyAnOicgKyBpLFxuICAgICAgY3NzOiBjc3MsXG4gICAgICBtZWRpYTogbWVkaWEsXG4gICAgICBzb3VyY2VNYXA6IHNvdXJjZU1hcFxuICAgIH1cbiAgICBpZiAoIW5ld1N0eWxlc1tpZF0pIHtcbiAgICAgIHN0eWxlcy5wdXNoKG5ld1N0eWxlc1tpZF0gPSB7IGlkOiBpZCwgcGFydHM6IFtwYXJ0XSB9KVxuICAgIH0gZWxzZSB7XG4gICAgICBuZXdTdHlsZXNbaWRdLnBhcnRzLnB1c2gocGFydClcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHN0eWxlc1xufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qc1xuLy8gbW9kdWxlIGlkID0gN1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiAzIDQgNSA5IDExIDEyIDEzIl0sInNvdXJjZVJvb3QiOiIifQ==