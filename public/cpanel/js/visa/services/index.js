/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 488);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 167:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(279),
  /* template */
  __webpack_require__(397),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/services/ChildServices.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ChildServices.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5ca91eee", Component.options)
  } else {
    hotAPI.reload("data-v-5ca91eee", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

!function(t,e){ true?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.VueMultiselect=e():t.VueMultiselect=e()}(this,function(){return function(t){function e(i){if(n[i])return n[i].exports;var r=n[i]={i:i,l:!1,exports:{}};return t[i].call(r.exports,r,r.exports,e),r.l=!0,r.exports}var n={};return e.m=t,e.c=n,e.i=function(t){return t},e.d=function(t,n,i){e.o(t,n)||Object.defineProperty(t,n,{configurable:!1,enumerable:!0,get:i})},e.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(n,"a",n),n},e.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},e.p="/",e(e.s=60)}([function(t,e){var n=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=n)},function(t,e,n){var i=n(49)("wks"),r=n(30),o=n(0).Symbol,s="function"==typeof o;(t.exports=function(t){return i[t]||(i[t]=s&&o[t]||(s?o:r)("Symbol."+t))}).store=i},function(t,e,n){var i=n(5);t.exports=function(t){if(!i(t))throw TypeError(t+" is not an object!");return t}},function(t,e,n){var i=n(0),r=n(10),o=n(8),s=n(6),u=n(11),a=function(t,e,n){var l,c,f,p,h=t&a.F,d=t&a.G,v=t&a.S,g=t&a.P,m=t&a.B,y=d?i:v?i[e]||(i[e]={}):(i[e]||{}).prototype,b=d?r:r[e]||(r[e]={}),_=b.prototype||(b.prototype={});d&&(n=e);for(l in n)c=!h&&y&&void 0!==y[l],f=(c?y:n)[l],p=m&&c?u(f,i):g&&"function"==typeof f?u(Function.call,f):f,y&&s(y,l,f,t&a.U),b[l]!=f&&o(b,l,p),g&&_[l]!=f&&(_[l]=f)};i.core=r,a.F=1,a.G=2,a.S=4,a.P=8,a.B=16,a.W=32,a.U=64,a.R=128,t.exports=a},function(t,e,n){t.exports=!n(7)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,e){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,e,n){var i=n(0),r=n(8),o=n(12),s=n(30)("src"),u=Function.toString,a=(""+u).split("toString");n(10).inspectSource=function(t){return u.call(t)},(t.exports=function(t,e,n,u){var l="function"==typeof n;l&&(o(n,"name")||r(n,"name",e)),t[e]!==n&&(l&&(o(n,s)||r(n,s,t[e]?""+t[e]:a.join(String(e)))),t===i?t[e]=n:u?t[e]?t[e]=n:r(t,e,n):(delete t[e],r(t,e,n)))})(Function.prototype,"toString",function(){return"function"==typeof this&&this[s]||u.call(this)})},function(t,e){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,e,n){var i=n(13),r=n(25);t.exports=n(4)?function(t,e,n){return i.f(t,e,r(1,n))}:function(t,e,n){return t[e]=n,t}},function(t,e){var n={}.toString;t.exports=function(t){return n.call(t).slice(8,-1)}},function(t,e){var n=t.exports={version:"2.5.7"};"number"==typeof __e&&(__e=n)},function(t,e,n){var i=n(14);t.exports=function(t,e,n){if(i(t),void 0===e)return t;switch(n){case 1:return function(n){return t.call(e,n)};case 2:return function(n,i){return t.call(e,n,i)};case 3:return function(n,i,r){return t.call(e,n,i,r)}}return function(){return t.apply(e,arguments)}}},function(t,e){var n={}.hasOwnProperty;t.exports=function(t,e){return n.call(t,e)}},function(t,e,n){var i=n(2),r=n(41),o=n(29),s=Object.defineProperty;e.f=n(4)?Object.defineProperty:function(t,e,n){if(i(t),e=o(e,!0),i(n),r)try{return s(t,e,n)}catch(t){}if("get"in n||"set"in n)throw TypeError("Accessors not supported!");return"value"in n&&(t[e]=n.value),t}},function(t,e){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,e){t.exports={}},function(t,e){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,e,n){"use strict";var i=n(7);t.exports=function(t,e){return!!t&&i(function(){e?t.call(null,function(){},1):t.call(null)})}},function(t,e,n){var i=n(23),r=n(16);t.exports=function(t){return i(r(t))}},function(t,e,n){var i=n(53),r=Math.min;t.exports=function(t){return t>0?r(i(t),9007199254740991):0}},function(t,e,n){var i=n(11),r=n(23),o=n(28),s=n(19),u=n(64);t.exports=function(t,e){var n=1==t,a=2==t,l=3==t,c=4==t,f=6==t,p=5==t||f,h=e||u;return function(e,u,d){for(var v,g,m=o(e),y=r(m),b=i(u,d,3),_=s(y.length),x=0,w=n?h(e,_):a?h(e,0):void 0;_>x;x++)if((p||x in y)&&(v=y[x],g=b(v,x,m),t))if(n)w[x]=g;else if(g)switch(t){case 3:return!0;case 5:return v;case 6:return x;case 2:w.push(v)}else if(c)return!1;return f?-1:l||c?c:w}}},function(t,e,n){var i=n(5),r=n(0).document,o=i(r)&&i(r.createElement);t.exports=function(t){return o?r.createElement(t):{}}},function(t,e){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,e,n){var i=n(9);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==i(t)?t.split(""):Object(t)}},function(t,e){t.exports=!1},function(t,e){t.exports=function(t,e){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:e}}},function(t,e,n){var i=n(13).f,r=n(12),o=n(1)("toStringTag");t.exports=function(t,e,n){t&&!r(t=n?t:t.prototype,o)&&i(t,o,{configurable:!0,value:e})}},function(t,e,n){var i=n(49)("keys"),r=n(30);t.exports=function(t){return i[t]||(i[t]=r(t))}},function(t,e,n){var i=n(16);t.exports=function(t){return Object(i(t))}},function(t,e,n){var i=n(5);t.exports=function(t,e){if(!i(t))return t;var n,r;if(e&&"function"==typeof(n=t.toString)&&!i(r=n.call(t)))return r;if("function"==typeof(n=t.valueOf)&&!i(r=n.call(t)))return r;if(!e&&"function"==typeof(n=t.toString)&&!i(r=n.call(t)))return r;throw TypeError("Can't convert object to primitive value")}},function(t,e){var n=0,i=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++n+i).toString(36))}},function(t,e,n){"use strict";var i=n(0),r=n(12),o=n(9),s=n(67),u=n(29),a=n(7),l=n(77).f,c=n(45).f,f=n(13).f,p=n(51).trim,h=i.Number,d=h,v=h.prototype,g="Number"==o(n(44)(v)),m="trim"in String.prototype,y=function(t){var e=u(t,!1);if("string"==typeof e&&e.length>2){e=m?e.trim():p(e,3);var n,i,r,o=e.charCodeAt(0);if(43===o||45===o){if(88===(n=e.charCodeAt(2))||120===n)return NaN}else if(48===o){switch(e.charCodeAt(1)){case 66:case 98:i=2,r=49;break;case 79:case 111:i=8,r=55;break;default:return+e}for(var s,a=e.slice(2),l=0,c=a.length;l<c;l++)if((s=a.charCodeAt(l))<48||s>r)return NaN;return parseInt(a,i)}}return+e};if(!h(" 0o1")||!h("0b1")||h("+0x1")){h=function(t){var e=arguments.length<1?0:t,n=this;return n instanceof h&&(g?a(function(){v.valueOf.call(n)}):"Number"!=o(n))?s(new d(y(e)),n,h):y(e)};for(var b,_=n(4)?l(d):"MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","),x=0;_.length>x;x++)r(d,b=_[x])&&!r(h,b)&&f(h,b,c(d,b));h.prototype=v,v.constructor=h,n(6)(i,"Number",h)}},function(t,e,n){"use strict";function i(t){return 0!==t&&(!(!Array.isArray(t)||0!==t.length)||!t)}function r(t){return function(){return!t.apply(void 0,arguments)}}function o(t,e){return void 0===t&&(t="undefined"),null===t&&(t="null"),!1===t&&(t="false"),-1!==t.toString().toLowerCase().indexOf(e.trim())}function s(t,e,n,i){return t.filter(function(t){return o(i(t,n),e)})}function u(t){return t.filter(function(t){return!t.$isLabel})}function a(t,e){return function(n){return n.reduce(function(n,i){return i[t]&&i[t].length?(n.push({$groupLabel:i[e],$isLabel:!0}),n.concat(i[t])):n},[])}}function l(t,e,i,r,o){return function(u){return u.map(function(u){var a;if(!u[i])return console.warn("Options passed to vue-multiselect do not contain groups, despite the config."),[];var l=s(u[i],t,e,o);return l.length?(a={},n.i(d.a)(a,r,u[r]),n.i(d.a)(a,i,l),a):[]})}}var c=n(59),f=n(54),p=(n.n(f),n(95)),h=(n.n(p),n(31)),d=(n.n(h),n(58)),v=n(91),g=(n.n(v),n(98)),m=(n.n(g),n(92)),y=(n.n(m),n(88)),b=(n.n(y),n(97)),_=(n.n(b),n(89)),x=(n.n(_),n(96)),w=(n.n(x),n(93)),S=(n.n(w),n(90)),O=(n.n(S),function(){for(var t=arguments.length,e=new Array(t),n=0;n<t;n++)e[n]=arguments[n];return function(t){return e.reduce(function(t,e){return e(t)},t)}});e.a={data:function(){return{search:"",isOpen:!1,prefferedOpenDirection:"below",optimizedHeight:this.maxHeight}},props:{internalSearch:{type:Boolean,default:!0},options:{type:Array,required:!0},multiple:{type:Boolean,default:!1},value:{type:null,default:function(){return[]}},trackBy:{type:String},label:{type:String},searchable:{type:Boolean,default:!0},clearOnSelect:{type:Boolean,default:!0},hideSelected:{type:Boolean,default:!1},placeholder:{type:String,default:"Select option"},allowEmpty:{type:Boolean,default:!0},resetAfter:{type:Boolean,default:!1},closeOnSelect:{type:Boolean,default:!0},customLabel:{type:Function,default:function(t,e){return i(t)?"":e?t[e]:t}},taggable:{type:Boolean,default:!1},tagPlaceholder:{type:String,default:"Press enter to create a tag"},tagPosition:{type:String,default:"top"},max:{type:[Number,Boolean],default:!1},id:{default:null},optionsLimit:{type:Number,default:1e3},groupValues:{type:String},groupLabel:{type:String},groupSelect:{type:Boolean,default:!1},blockKeys:{type:Array,default:function(){return[]}},preserveSearch:{type:Boolean,default:!1},preselectFirst:{type:Boolean,default:!1}},mounted:function(){this.multiple||this.clearOnSelect||console.warn("[Vue-Multiselect warn]: ClearOnSelect and Multiple props can’t be both set to false."),!this.multiple&&this.max&&console.warn("[Vue-Multiselect warn]: Max prop should not be used when prop Multiple equals false."),this.preselectFirst&&!this.internalValue.length&&this.options.length&&this.select(this.filteredOptions[0])},computed:{internalValue:function(){return this.value||0===this.value?Array.isArray(this.value)?this.value:[this.value]:[]},filteredOptions:function(){var t=this.search||"",e=t.toLowerCase().trim(),n=this.options.concat();return n=this.internalSearch?this.groupValues?this.filterAndFlat(n,e,this.label):s(n,e,this.label,this.customLabel):this.groupValues?a(this.groupValues,this.groupLabel)(n):n,n=this.hideSelected?n.filter(r(this.isSelected)):n,this.taggable&&e.length&&!this.isExistingOption(e)&&("bottom"===this.tagPosition?n.push({isTag:!0,label:t}):n.unshift({isTag:!0,label:t})),n.slice(0,this.optionsLimit)},valueKeys:function(){var t=this;return this.trackBy?this.internalValue.map(function(e){return e[t.trackBy]}):this.internalValue},optionKeys:function(){var t=this;return(this.groupValues?this.flatAndStrip(this.options):this.options).map(function(e){return t.customLabel(e,t.label).toString().toLowerCase()})},currentOptionLabel:function(){return this.multiple?this.searchable?"":this.placeholder:this.internalValue.length?this.getOptionLabel(this.internalValue[0]):this.searchable?"":this.placeholder}},watch:{internalValue:function(){this.resetAfter&&this.internalValue.length&&(this.search="",this.$emit("input",this.multiple?[]:null))},search:function(){this.$emit("search-change",this.search,this.id)}},methods:{getValue:function(){return this.multiple?this.internalValue:0===this.internalValue.length?null:this.internalValue[0]},filterAndFlat:function(t,e,n){return O(l(e,n,this.groupValues,this.groupLabel,this.customLabel),a(this.groupValues,this.groupLabel))(t)},flatAndStrip:function(t){return O(a(this.groupValues,this.groupLabel),u)(t)},updateSearch:function(t){this.search=t},isExistingOption:function(t){return!!this.options&&this.optionKeys.indexOf(t)>-1},isSelected:function(t){var e=this.trackBy?t[this.trackBy]:t;return this.valueKeys.indexOf(e)>-1},getOptionLabel:function(t){if(i(t))return"";if(t.isTag)return t.label;if(t.$isLabel)return t.$groupLabel;var e=this.customLabel(t,this.label);return i(e)?"":e},select:function(t,e){if(t.$isLabel&&this.groupSelect)return void this.selectGroup(t);if(!(-1!==this.blockKeys.indexOf(e)||this.disabled||t.$isDisabled||t.$isLabel)&&(!this.max||!this.multiple||this.internalValue.length!==this.max)&&("Tab"!==e||this.pointerDirty)){if(t.isTag)this.$emit("tag",t.label,this.id),this.search="",this.closeOnSelect&&!this.multiple&&this.deactivate();else{if(this.isSelected(t))return void("Tab"!==e&&this.removeElement(t));this.$emit("select",t,this.id),this.multiple?this.$emit("input",this.internalValue.concat([t]),this.id):this.$emit("input",t,this.id),this.clearOnSelect&&(this.search="")}this.closeOnSelect&&this.deactivate()}},selectGroup:function(t){var e=this,n=this.options.find(function(n){return n[e.groupLabel]===t.$groupLabel});if(n)if(this.wholeGroupSelected(n)){this.$emit("remove",n[this.groupValues],this.id);var i=this.internalValue.filter(function(t){return-1===n[e.groupValues].indexOf(t)});this.$emit("input",i,this.id)}else{var o=n[this.groupValues].filter(r(this.isSelected));this.$emit("select",o,this.id),this.$emit("input",this.internalValue.concat(o),this.id)}},wholeGroupSelected:function(t){return t[this.groupValues].every(this.isSelected)},removeElement:function(t){var e=!(arguments.length>1&&void 0!==arguments[1])||arguments[1];if(!this.disabled){if(!this.allowEmpty&&this.internalValue.length<=1)return void this.deactivate();var i="object"===n.i(c.a)(t)?this.valueKeys.indexOf(t[this.trackBy]):this.valueKeys.indexOf(t);if(this.$emit("remove",t,this.id),this.multiple){var r=this.internalValue.slice(0,i).concat(this.internalValue.slice(i+1));this.$emit("input",r,this.id)}else this.$emit("input",null,this.id);this.closeOnSelect&&e&&this.deactivate()}},removeLastElement:function(){-1===this.blockKeys.indexOf("Delete")&&0===this.search.length&&Array.isArray(this.internalValue)&&this.removeElement(this.internalValue[this.internalValue.length-1],!1)},activate:function(){var t=this;this.isOpen||this.disabled||(this.adjustPosition(),this.groupValues&&0===this.pointer&&this.filteredOptions.length&&(this.pointer=1),this.isOpen=!0,this.searchable?(this.preserveSearch||(this.search=""),this.$nextTick(function(){return t.$refs.search.focus()})):this.$el.focus(),this.$emit("open",this.id))},deactivate:function(){this.isOpen&&(this.isOpen=!1,this.searchable?this.$refs.search.blur():this.$el.blur(),this.preserveSearch||(this.search=""),this.$emit("close",this.getValue(),this.id))},toggle:function(){this.isOpen?this.deactivate():this.activate()},adjustPosition:function(){if("undefined"!=typeof window){var t=this.$el.getBoundingClientRect().top,e=window.innerHeight-this.$el.getBoundingClientRect().bottom;e>this.maxHeight||e>t||"below"===this.openDirection||"bottom"===this.openDirection?(this.prefferedOpenDirection="below",this.optimizedHeight=Math.min(e-40,this.maxHeight)):(this.prefferedOpenDirection="above",this.optimizedHeight=Math.min(t-40,this.maxHeight))}}}}},function(t,e,n){"use strict";var i=n(54),r=(n.n(i),n(31));n.n(r);e.a={data:function(){return{pointer:0,pointerDirty:!1}},props:{showPointer:{type:Boolean,default:!0},optionHeight:{type:Number,default:40}},computed:{pointerPosition:function(){return this.pointer*this.optionHeight},visibleElements:function(){return this.optimizedHeight/this.optionHeight}},watch:{filteredOptions:function(){this.pointerAdjust()},isOpen:function(){this.pointerDirty=!1}},methods:{optionHighlight:function(t,e){return{"multiselect__option--highlight":t===this.pointer&&this.showPointer,"multiselect__option--selected":this.isSelected(e)}},groupHighlight:function(t,e){var n=this;if(!this.groupSelect)return["multiselect__option--group","multiselect__option--disabled"];var i=this.options.find(function(t){return t[n.groupLabel]===e.$groupLabel});return["multiselect__option--group",{"multiselect__option--highlight":t===this.pointer&&this.showPointer},{"multiselect__option--group-selected":this.wholeGroupSelected(i)}]},addPointerElement:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"Enter",e=t.key;this.filteredOptions.length>0&&this.select(this.filteredOptions[this.pointer],e),this.pointerReset()},pointerForward:function(){this.pointer<this.filteredOptions.length-1&&(this.pointer++,this.$refs.list.scrollTop<=this.pointerPosition-(this.visibleElements-1)*this.optionHeight&&(this.$refs.list.scrollTop=this.pointerPosition-(this.visibleElements-1)*this.optionHeight),this.filteredOptions[this.pointer]&&this.filteredOptions[this.pointer].$isLabel&&!this.groupSelect&&this.pointerForward()),this.pointerDirty=!0},pointerBackward:function(){this.pointer>0?(this.pointer--,this.$refs.list.scrollTop>=this.pointerPosition&&(this.$refs.list.scrollTop=this.pointerPosition),this.filteredOptions[this.pointer]&&this.filteredOptions[this.pointer].$isLabel&&!this.groupSelect&&this.pointerBackward()):this.filteredOptions[this.pointer]&&this.filteredOptions[0].$isLabel&&!this.groupSelect&&this.pointerForward(),this.pointerDirty=!0},pointerReset:function(){this.closeOnSelect&&(this.pointer=0,this.$refs.list&&(this.$refs.list.scrollTop=0))},pointerAdjust:function(){this.pointer>=this.filteredOptions.length-1&&(this.pointer=this.filteredOptions.length?this.filteredOptions.length-1:0),this.filteredOptions.length>0&&this.filteredOptions[this.pointer].$isLabel&&!this.groupSelect&&this.pointerForward()},pointerSet:function(t){this.pointer=t,this.pointerDirty=!0}}}},function(t,e,n){"use strict";var i=n(36),r=n(74),o=n(15),s=n(18);t.exports=n(72)(Array,"Array",function(t,e){this._t=s(t),this._i=0,this._k=e},function(){var t=this._t,e=this._k,n=this._i++;return!t||n>=t.length?(this._t=void 0,r(1)):"keys"==e?r(0,n):"values"==e?r(0,t[n]):r(0,[n,t[n]])},"values"),o.Arguments=o.Array,i("keys"),i("values"),i("entries")},function(t,e,n){"use strict";var i=n(31),r=(n.n(i),n(32)),o=n(33);e.a={name:"vue-multiselect",mixins:[r.a,o.a],props:{name:{type:String,default:""},selectLabel:{type:String,default:"Press enter to select"},selectGroupLabel:{type:String,default:"Press enter to select group"},selectedLabel:{type:String,default:"Selected"},deselectLabel:{type:String,default:"Press enter to remove"},deselectGroupLabel:{type:String,default:"Press enter to deselect group"},showLabels:{type:Boolean,default:!0},limit:{type:Number,default:99999},maxHeight:{type:Number,default:300},limitText:{type:Function,default:function(t){return"and ".concat(t," more")}},loading:{type:Boolean,default:!1},disabled:{type:Boolean,default:!1},openDirection:{type:String,default:""},showNoOptions:{type:Boolean,default:!0},showNoResults:{type:Boolean,default:!0},tabindex:{type:Number,default:0}},computed:{isSingleLabelVisible:function(){return this.singleValue&&(!this.isOpen||!this.searchable)&&!this.visibleValues.length},isPlaceholderVisible:function(){return!(this.internalValue.length||this.searchable&&this.isOpen)},visibleValues:function(){return this.multiple?this.internalValue.slice(0,this.limit):[]},singleValue:function(){return this.internalValue[0]},deselectLabelText:function(){return this.showLabels?this.deselectLabel:""},deselectGroupLabelText:function(){return this.showLabels?this.deselectGroupLabel:""},selectLabelText:function(){return this.showLabels?this.selectLabel:""},selectGroupLabelText:function(){return this.showLabels?this.selectGroupLabel:""},selectedLabelText:function(){return this.showLabels?this.selectedLabel:""},inputStyle:function(){if(this.searchable||this.multiple&&this.value&&this.value.length)return this.isOpen?{width:"auto"}:{width:"0",position:"absolute",padding:"0"}},contentStyle:function(){return this.options.length?{display:"inline-block"}:{display:"block"}},isAbove:function(){return"above"===this.openDirection||"top"===this.openDirection||"below"!==this.openDirection&&"bottom"!==this.openDirection&&"above"===this.prefferedOpenDirection},showSearchInput:function(){return this.searchable&&(!this.hasSingleSelectedSlot||!this.visibleSingleValue&&0!==this.visibleSingleValue||this.isOpen)}}}},function(t,e,n){var i=n(1)("unscopables"),r=Array.prototype;void 0==r[i]&&n(8)(r,i,{}),t.exports=function(t){r[i][t]=!0}},function(t,e,n){var i=n(18),r=n(19),o=n(85);t.exports=function(t){return function(e,n,s){var u,a=i(e),l=r(a.length),c=o(s,l);if(t&&n!=n){for(;l>c;)if((u=a[c++])!=u)return!0}else for(;l>c;c++)if((t||c in a)&&a[c]===n)return t||c||0;return!t&&-1}}},function(t,e,n){var i=n(9),r=n(1)("toStringTag"),o="Arguments"==i(function(){return arguments}()),s=function(t,e){try{return t[e]}catch(t){}};t.exports=function(t){var e,n,u;return void 0===t?"Undefined":null===t?"Null":"string"==typeof(n=s(e=Object(t),r))?n:o?i(e):"Object"==(u=i(e))&&"function"==typeof e.callee?"Arguments":u}},function(t,e,n){"use strict";var i=n(2);t.exports=function(){var t=i(this),e="";return t.global&&(e+="g"),t.ignoreCase&&(e+="i"),t.multiline&&(e+="m"),t.unicode&&(e+="u"),t.sticky&&(e+="y"),e}},function(t,e,n){var i=n(0).document;t.exports=i&&i.documentElement},function(t,e,n){t.exports=!n(4)&&!n(7)(function(){return 7!=Object.defineProperty(n(21)("div"),"a",{get:function(){return 7}}).a})},function(t,e,n){var i=n(9);t.exports=Array.isArray||function(t){return"Array"==i(t)}},function(t,e,n){"use strict";function i(t){var e,n;this.promise=new t(function(t,i){if(void 0!==e||void 0!==n)throw TypeError("Bad Promise constructor");e=t,n=i}),this.resolve=r(e),this.reject=r(n)}var r=n(14);t.exports.f=function(t){return new i(t)}},function(t,e,n){var i=n(2),r=n(76),o=n(22),s=n(27)("IE_PROTO"),u=function(){},a=function(){var t,e=n(21)("iframe"),i=o.length;for(e.style.display="none",n(40).appendChild(e),e.src="javascript:",t=e.contentWindow.document,t.open(),t.write("<script>document.F=Object<\/script>"),t.close(),a=t.F;i--;)delete a.prototype[o[i]];return a()};t.exports=Object.create||function(t,e){var n;return null!==t?(u.prototype=i(t),n=new u,u.prototype=null,n[s]=t):n=a(),void 0===e?n:r(n,e)}},function(t,e,n){var i=n(79),r=n(25),o=n(18),s=n(29),u=n(12),a=n(41),l=Object.getOwnPropertyDescriptor;e.f=n(4)?l:function(t,e){if(t=o(t),e=s(e,!0),a)try{return l(t,e)}catch(t){}if(u(t,e))return r(!i.f.call(t,e),t[e])}},function(t,e,n){var i=n(12),r=n(18),o=n(37)(!1),s=n(27)("IE_PROTO");t.exports=function(t,e){var n,u=r(t),a=0,l=[];for(n in u)n!=s&&i(u,n)&&l.push(n);for(;e.length>a;)i(u,n=e[a++])&&(~o(l,n)||l.push(n));return l}},function(t,e,n){var i=n(46),r=n(22);t.exports=Object.keys||function(t){return i(t,r)}},function(t,e,n){var i=n(2),r=n(5),o=n(43);t.exports=function(t,e){if(i(t),r(e)&&e.constructor===t)return e;var n=o.f(t);return(0,n.resolve)(e),n.promise}},function(t,e,n){var i=n(10),r=n(0),o=r["__core-js_shared__"]||(r["__core-js_shared__"]={});(t.exports=function(t,e){return o[t]||(o[t]=void 0!==e?e:{})})("versions",[]).push({version:i.version,mode:n(24)?"pure":"global",copyright:"© 2018 Denis Pushkarev (zloirock.ru)"})},function(t,e,n){var i=n(2),r=n(14),o=n(1)("species");t.exports=function(t,e){var n,s=i(t).constructor;return void 0===s||void 0==(n=i(s)[o])?e:r(n)}},function(t,e,n){var i=n(3),r=n(16),o=n(7),s=n(84),u="["+s+"]",a="​",l=RegExp("^"+u+u+"*"),c=RegExp(u+u+"*$"),f=function(t,e,n){var r={},u=o(function(){return!!s[t]()||a[t]()!=a}),l=r[t]=u?e(p):s[t];n&&(r[n]=l),i(i.P+i.F*u,"String",r)},p=f.trim=function(t,e){return t=String(r(t)),1&e&&(t=t.replace(l,"")),2&e&&(t=t.replace(c,"")),t};t.exports=f},function(t,e,n){var i,r,o,s=n(11),u=n(68),a=n(40),l=n(21),c=n(0),f=c.process,p=c.setImmediate,h=c.clearImmediate,d=c.MessageChannel,v=c.Dispatch,g=0,m={},y=function(){var t=+this;if(m.hasOwnProperty(t)){var e=m[t];delete m[t],e()}},b=function(t){y.call(t.data)};p&&h||(p=function(t){for(var e=[],n=1;arguments.length>n;)e.push(arguments[n++]);return m[++g]=function(){u("function"==typeof t?t:Function(t),e)},i(g),g},h=function(t){delete m[t]},"process"==n(9)(f)?i=function(t){f.nextTick(s(y,t,1))}:v&&v.now?i=function(t){v.now(s(y,t,1))}:d?(r=new d,o=r.port2,r.port1.onmessage=b,i=s(o.postMessage,o,1)):c.addEventListener&&"function"==typeof postMessage&&!c.importScripts?(i=function(t){c.postMessage(t+"","*")},c.addEventListener("message",b,!1)):i="onreadystatechange"in l("script")?function(t){a.appendChild(l("script")).onreadystatechange=function(){a.removeChild(this),y.call(t)}}:function(t){setTimeout(s(y,t,1),0)}),t.exports={set:p,clear:h}},function(t,e){var n=Math.ceil,i=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?i:n)(t)}},function(t,e,n){"use strict";var i=n(3),r=n(20)(5),o=!0;"find"in[]&&Array(1).find(function(){o=!1}),i(i.P+i.F*o,"Array",{find:function(t){return r(this,t,arguments.length>1?arguments[1]:void 0)}}),n(36)("find")},function(t,e,n){"use strict";var i,r,o,s,u=n(24),a=n(0),l=n(11),c=n(38),f=n(3),p=n(5),h=n(14),d=n(61),v=n(66),g=n(50),m=n(52).set,y=n(75)(),b=n(43),_=n(80),x=n(86),w=n(48),S=a.TypeError,O=a.process,L=O&&O.versions,P=L&&L.v8||"",k=a.Promise,T="process"==c(O),E=function(){},V=r=b.f,A=!!function(){try{var t=k.resolve(1),e=(t.constructor={})[n(1)("species")]=function(t){t(E,E)};return(T||"function"==typeof PromiseRejectionEvent)&&t.then(E)instanceof e&&0!==P.indexOf("6.6")&&-1===x.indexOf("Chrome/66")}catch(t){}}(),C=function(t){var e;return!(!p(t)||"function"!=typeof(e=t.then))&&e},j=function(t,e){if(!t._n){t._n=!0;var n=t._c;y(function(){for(var i=t._v,r=1==t._s,o=0;n.length>o;)!function(e){var n,o,s,u=r?e.ok:e.fail,a=e.resolve,l=e.reject,c=e.domain;try{u?(r||(2==t._h&&$(t),t._h=1),!0===u?n=i:(c&&c.enter(),n=u(i),c&&(c.exit(),s=!0)),n===e.promise?l(S("Promise-chain cycle")):(o=C(n))?o.call(n,a,l):a(n)):l(i)}catch(t){c&&!s&&c.exit(),l(t)}}(n[o++]);t._c=[],t._n=!1,e&&!t._h&&N(t)})}},N=function(t){m.call(a,function(){var e,n,i,r=t._v,o=D(t);if(o&&(e=_(function(){T?O.emit("unhandledRejection",r,t):(n=a.onunhandledrejection)?n({promise:t,reason:r}):(i=a.console)&&i.error&&i.error("Unhandled promise rejection",r)}),t._h=T||D(t)?2:1),t._a=void 0,o&&e.e)throw e.v})},D=function(t){return 1!==t._h&&0===(t._a||t._c).length},$=function(t){m.call(a,function(){var e;T?O.emit("rejectionHandled",t):(e=a.onrejectionhandled)&&e({promise:t,reason:t._v})})},M=function(t){var e=this;e._d||(e._d=!0,e=e._w||e,e._v=t,e._s=2,e._a||(e._a=e._c.slice()),j(e,!0))},F=function(t){var e,n=this;if(!n._d){n._d=!0,n=n._w||n;try{if(n===t)throw S("Promise can't be resolved itself");(e=C(t))?y(function(){var i={_w:n,_d:!1};try{e.call(t,l(F,i,1),l(M,i,1))}catch(t){M.call(i,t)}}):(n._v=t,n._s=1,j(n,!1))}catch(t){M.call({_w:n,_d:!1},t)}}};A||(k=function(t){d(this,k,"Promise","_h"),h(t),i.call(this);try{t(l(F,this,1),l(M,this,1))}catch(t){M.call(this,t)}},i=function(t){this._c=[],this._a=void 0,this._s=0,this._d=!1,this._v=void 0,this._h=0,this._n=!1},i.prototype=n(81)(k.prototype,{then:function(t,e){var n=V(g(this,k));return n.ok="function"!=typeof t||t,n.fail="function"==typeof e&&e,n.domain=T?O.domain:void 0,this._c.push(n),this._a&&this._a.push(n),this._s&&j(this,!1),n.promise},catch:function(t){return this.then(void 0,t)}}),o=function(){var t=new i;this.promise=t,this.resolve=l(F,t,1),this.reject=l(M,t,1)},b.f=V=function(t){return t===k||t===s?new o(t):r(t)}),f(f.G+f.W+f.F*!A,{Promise:k}),n(26)(k,"Promise"),n(83)("Promise"),s=n(10).Promise,f(f.S+f.F*!A,"Promise",{reject:function(t){var e=V(this);return(0,e.reject)(t),e.promise}}),f(f.S+f.F*(u||!A),"Promise",{resolve:function(t){return w(u&&this===s?k:this,t)}}),f(f.S+f.F*!(A&&n(73)(function(t){k.all(t).catch(E)})),"Promise",{all:function(t){var e=this,n=V(e),i=n.resolve,r=n.reject,o=_(function(){var n=[],o=0,s=1;v(t,!1,function(t){var u=o++,a=!1;n.push(void 0),s++,e.resolve(t).then(function(t){a||(a=!0,n[u]=t,--s||i(n))},r)}),--s||i(n)});return o.e&&r(o.v),n.promise},race:function(t){var e=this,n=V(e),i=n.reject,r=_(function(){v(t,!1,function(t){e.resolve(t).then(n.resolve,i)})});return r.e&&i(r.v),n.promise}})},function(t,e,n){"use strict";var i=n(3),r=n(10),o=n(0),s=n(50),u=n(48);i(i.P+i.R,"Promise",{finally:function(t){var e=s(this,r.Promise||o.Promise),n="function"==typeof t;return this.then(n?function(n){return u(e,t()).then(function(){return n})}:t,n?function(n){return u(e,t()).then(function(){throw n})}:t)}})},function(t,e,n){"use strict";function i(t){n(99)}var r=n(35),o=n(101),s=n(100),u=i,a=s(r.a,o.a,!1,u,null,null);e.a=a.exports},function(t,e,n){"use strict";function i(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}e.a=i},function(t,e,n){"use strict";function i(t){return(i="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(t)}function r(t){return(r="function"==typeof Symbol&&"symbol"===i(Symbol.iterator)?function(t){return i(t)}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":i(t)})(t)}e.a=r},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var i=n(34),r=(n.n(i),n(55)),o=(n.n(r),n(56)),s=(n.n(o),n(57)),u=n(32),a=n(33);n.d(e,"Multiselect",function(){return s.a}),n.d(e,"multiselectMixin",function(){return u.a}),n.d(e,"pointerMixin",function(){return a.a}),e.default=s.a},function(t,e){t.exports=function(t,e,n,i){if(!(t instanceof e)||void 0!==i&&i in t)throw TypeError(n+": incorrect invocation!");return t}},function(t,e,n){var i=n(14),r=n(28),o=n(23),s=n(19);t.exports=function(t,e,n,u,a){i(e);var l=r(t),c=o(l),f=s(l.length),p=a?f-1:0,h=a?-1:1;if(n<2)for(;;){if(p in c){u=c[p],p+=h;break}if(p+=h,a?p<0:f<=p)throw TypeError("Reduce of empty array with no initial value")}for(;a?p>=0:f>p;p+=h)p in c&&(u=e(u,c[p],p,l));return u}},function(t,e,n){var i=n(5),r=n(42),o=n(1)("species");t.exports=function(t){var e;return r(t)&&(e=t.constructor,"function"!=typeof e||e!==Array&&!r(e.prototype)||(e=void 0),i(e)&&null===(e=e[o])&&(e=void 0)),void 0===e?Array:e}},function(t,e,n){var i=n(63);t.exports=function(t,e){return new(i(t))(e)}},function(t,e,n){"use strict";var i=n(8),r=n(6),o=n(7),s=n(16),u=n(1);t.exports=function(t,e,n){var a=u(t),l=n(s,a,""[t]),c=l[0],f=l[1];o(function(){var e={};return e[a]=function(){return 7},7!=""[t](e)})&&(r(String.prototype,t,c),i(RegExp.prototype,a,2==e?function(t,e){return f.call(t,this,e)}:function(t){return f.call(t,this)}))}},function(t,e,n){var i=n(11),r=n(70),o=n(69),s=n(2),u=n(19),a=n(87),l={},c={},e=t.exports=function(t,e,n,f,p){var h,d,v,g,m=p?function(){return t}:a(t),y=i(n,f,e?2:1),b=0;if("function"!=typeof m)throw TypeError(t+" is not iterable!");if(o(m)){for(h=u(t.length);h>b;b++)if((g=e?y(s(d=t[b])[0],d[1]):y(t[b]))===l||g===c)return g}else for(v=m.call(t);!(d=v.next()).done;)if((g=r(v,y,d.value,e))===l||g===c)return g};e.BREAK=l,e.RETURN=c},function(t,e,n){var i=n(5),r=n(82).set;t.exports=function(t,e,n){var o,s=e.constructor;return s!==n&&"function"==typeof s&&(o=s.prototype)!==n.prototype&&i(o)&&r&&r(t,o),t}},function(t,e){t.exports=function(t,e,n){var i=void 0===n;switch(e.length){case 0:return i?t():t.call(n);case 1:return i?t(e[0]):t.call(n,e[0]);case 2:return i?t(e[0],e[1]):t.call(n,e[0],e[1]);case 3:return i?t(e[0],e[1],e[2]):t.call(n,e[0],e[1],e[2]);case 4:return i?t(e[0],e[1],e[2],e[3]):t.call(n,e[0],e[1],e[2],e[3])}return t.apply(n,e)}},function(t,e,n){var i=n(15),r=n(1)("iterator"),o=Array.prototype;t.exports=function(t){return void 0!==t&&(i.Array===t||o[r]===t)}},function(t,e,n){var i=n(2);t.exports=function(t,e,n,r){try{return r?e(i(n)[0],n[1]):e(n)}catch(e){var o=t.return;throw void 0!==o&&i(o.call(t)),e}}},function(t,e,n){"use strict";var i=n(44),r=n(25),o=n(26),s={};n(8)(s,n(1)("iterator"),function(){return this}),t.exports=function(t,e,n){t.prototype=i(s,{next:r(1,n)}),o(t,e+" Iterator")}},function(t,e,n){"use strict";var i=n(24),r=n(3),o=n(6),s=n(8),u=n(15),a=n(71),l=n(26),c=n(78),f=n(1)("iterator"),p=!([].keys&&"next"in[].keys()),h=function(){return this};t.exports=function(t,e,n,d,v,g,m){a(n,e,d);var y,b,_,x=function(t){if(!p&&t in L)return L[t];switch(t){case"keys":case"values":return function(){return new n(this,t)}}return function(){return new n(this,t)}},w=e+" Iterator",S="values"==v,O=!1,L=t.prototype,P=L[f]||L["@@iterator"]||v&&L[v],k=P||x(v),T=v?S?x("entries"):k:void 0,E="Array"==e?L.entries||P:P;if(E&&(_=c(E.call(new t)))!==Object.prototype&&_.next&&(l(_,w,!0),i||"function"==typeof _[f]||s(_,f,h)),S&&P&&"values"!==P.name&&(O=!0,k=function(){return P.call(this)}),i&&!m||!p&&!O&&L[f]||s(L,f,k),u[e]=k,u[w]=h,v)if(y={values:S?k:x("values"),keys:g?k:x("keys"),entries:T},m)for(b in y)b in L||o(L,b,y[b]);else r(r.P+r.F*(p||O),e,y);return y}},function(t,e,n){var i=n(1)("iterator"),r=!1;try{var o=[7][i]();o.return=function(){r=!0},Array.from(o,function(){throw 2})}catch(t){}t.exports=function(t,e){if(!e&&!r)return!1;var n=!1;try{var o=[7],s=o[i]();s.next=function(){return{done:n=!0}},o[i]=function(){return s},t(o)}catch(t){}return n}},function(t,e){t.exports=function(t,e){return{value:e,done:!!t}}},function(t,e,n){var i=n(0),r=n(52).set,o=i.MutationObserver||i.WebKitMutationObserver,s=i.process,u=i.Promise,a="process"==n(9)(s);t.exports=function(){var t,e,n,l=function(){var i,r;for(a&&(i=s.domain)&&i.exit();t;){r=t.fn,t=t.next;try{r()}catch(i){throw t?n():e=void 0,i}}e=void 0,i&&i.enter()};if(a)n=function(){s.nextTick(l)};else if(!o||i.navigator&&i.navigator.standalone)if(u&&u.resolve){var c=u.resolve(void 0);n=function(){c.then(l)}}else n=function(){r.call(i,l)};else{var f=!0,p=document.createTextNode("");new o(l).observe(p,{characterData:!0}),n=function(){p.data=f=!f}}return function(i){var r={fn:i,next:void 0};e&&(e.next=r),t||(t=r,n()),e=r}}},function(t,e,n){var i=n(13),r=n(2),o=n(47);t.exports=n(4)?Object.defineProperties:function(t,e){r(t);for(var n,s=o(e),u=s.length,a=0;u>a;)i.f(t,n=s[a++],e[n]);return t}},function(t,e,n){var i=n(46),r=n(22).concat("length","prototype");e.f=Object.getOwnPropertyNames||function(t){return i(t,r)}},function(t,e,n){var i=n(12),r=n(28),o=n(27)("IE_PROTO"),s=Object.prototype;t.exports=Object.getPrototypeOf||function(t){return t=r(t),i(t,o)?t[o]:"function"==typeof t.constructor&&t instanceof t.constructor?t.constructor.prototype:t instanceof Object?s:null}},function(t,e){e.f={}.propertyIsEnumerable},function(t,e){t.exports=function(t){try{return{e:!1,v:t()}}catch(t){return{e:!0,v:t}}}},function(t,e,n){var i=n(6);t.exports=function(t,e,n){for(var r in e)i(t,r,e[r],n);return t}},function(t,e,n){var i=n(5),r=n(2),o=function(t,e){if(r(t),!i(e)&&null!==e)throw TypeError(e+": can't set as prototype!")};t.exports={set:Object.setPrototypeOf||("__proto__"in{}?function(t,e,i){try{i=n(11)(Function.call,n(45).f(Object.prototype,"__proto__").set,2),i(t,[]),e=!(t instanceof Array)}catch(t){e=!0}return function(t,n){return o(t,n),e?t.__proto__=n:i(t,n),t}}({},!1):void 0),check:o}},function(t,e,n){"use strict";var i=n(0),r=n(13),o=n(4),s=n(1)("species");t.exports=function(t){var e=i[t];o&&e&&!e[s]&&r.f(e,s,{configurable:!0,get:function(){return this}})}},function(t,e){t.exports="\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff"},function(t,e,n){var i=n(53),r=Math.max,o=Math.min;t.exports=function(t,e){return t=i(t),t<0?r(t+e,0):o(t,e)}},function(t,e,n){var i=n(0),r=i.navigator;t.exports=r&&r.userAgent||""},function(t,e,n){var i=n(38),r=n(1)("iterator"),o=n(15);t.exports=n(10).getIteratorMethod=function(t){if(void 0!=t)return t[r]||t["@@iterator"]||o[i(t)]}},function(t,e,n){"use strict";var i=n(3),r=n(20)(2);i(i.P+i.F*!n(17)([].filter,!0),"Array",{filter:function(t){return r(this,t,arguments[1])}})},function(t,e,n){"use strict";var i=n(3),r=n(37)(!1),o=[].indexOf,s=!!o&&1/[1].indexOf(1,-0)<0;i(i.P+i.F*(s||!n(17)(o)),"Array",{indexOf:function(t){return s?o.apply(this,arguments)||0:r(this,t,arguments[1])}})},function(t,e,n){var i=n(3);i(i.S,"Array",{isArray:n(42)})},function(t,e,n){"use strict";var i=n(3),r=n(20)(1);i(i.P+i.F*!n(17)([].map,!0),"Array",{map:function(t){return r(this,t,arguments[1])}})},function(t,e,n){"use strict";var i=n(3),r=n(62);i(i.P+i.F*!n(17)([].reduce,!0),"Array",{reduce:function(t){return r(this,t,arguments.length,arguments[1],!1)}})},function(t,e,n){var i=Date.prototype,r=i.toString,o=i.getTime;new Date(NaN)+""!="Invalid Date"&&n(6)(i,"toString",function(){var t=o.call(this);return t===t?r.call(this):"Invalid Date"})},function(t,e,n){n(4)&&"g"!=/./g.flags&&n(13).f(RegExp.prototype,"flags",{configurable:!0,get:n(39)})},function(t,e,n){n(65)("search",1,function(t,e,n){return[function(n){"use strict";var i=t(this),r=void 0==n?void 0:n[e];return void 0!==r?r.call(n,i):new RegExp(n)[e](String(i))},n]})},function(t,e,n){"use strict";n(94);var i=n(2),r=n(39),o=n(4),s=/./.toString,u=function(t){n(6)(RegExp.prototype,"toString",t,!0)};n(7)(function(){return"/a/b"!=s.call({source:"a",flags:"b"})})?u(function(){var t=i(this);return"/".concat(t.source,"/","flags"in t?t.flags:!o&&t instanceof RegExp?r.call(t):void 0)}):"toString"!=s.name&&u(function(){return s.call(this)})},function(t,e,n){"use strict";n(51)("trim",function(t){return function(){return t(this,3)}})},function(t,e,n){for(var i=n(34),r=n(47),o=n(6),s=n(0),u=n(8),a=n(15),l=n(1),c=l("iterator"),f=l("toStringTag"),p=a.Array,h={CSSRuleList:!0,CSSStyleDeclaration:!1,CSSValueList:!1,ClientRectList:!1,DOMRectList:!1,DOMStringList:!1,DOMTokenList:!0,DataTransferItemList:!1,FileList:!1,HTMLAllCollection:!1,HTMLCollection:!1,HTMLFormElement:!1,HTMLSelectElement:!1,MediaList:!0,MimeTypeArray:!1,NamedNodeMap:!1,NodeList:!0,PaintRequestList:!1,Plugin:!1,PluginArray:!1,SVGLengthList:!1,SVGNumberList:!1,SVGPathSegList:!1,SVGPointList:!1,SVGStringList:!1,SVGTransformList:!1,SourceBufferList:!1,StyleSheetList:!0,TextTrackCueList:!1,TextTrackList:!1,TouchList:!1},d=r(h),v=0;v<d.length;v++){var g,m=d[v],y=h[m],b=s[m],_=b&&b.prototype;if(_&&(_[c]||u(_,c,p),_[f]||u(_,f,m),a[m]=p,y))for(g in i)_[g]||o(_,g,i[g],!0)}},function(t,e){},function(t,e){t.exports=function(t,e,n,i,r,o){var s,u=t=t||{},a=typeof t.default;"object"!==a&&"function"!==a||(s=t,u=t.default);var l="function"==typeof u?u.options:u;e&&(l.render=e.render,l.staticRenderFns=e.staticRenderFns,l._compiled=!0),n&&(l.functional=!0),r&&(l._scopeId=r);var c;if(o?(c=function(t){t=t||this.$vnode&&this.$vnode.ssrContext||this.parent&&this.parent.$vnode&&this.parent.$vnode.ssrContext,t||"undefined"==typeof __VUE_SSR_CONTEXT__||(t=__VUE_SSR_CONTEXT__),i&&i.call(this,t),t&&t._registeredComponents&&t._registeredComponents.add(o)},l._ssrRegister=c):i&&(c=i),c){var f=l.functional,p=f?l.render:l.beforeCreate;f?(l._injectStyles=c,l.render=function(t,e){return c.call(e),p(t,e)}):l.beforeCreate=p?[].concat(p,c):[c]}return{esModule:s,exports:u,options:l}}},function(t,e,n){"use strict";var i=function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"multiselect",class:{"multiselect--active":t.isOpen,"multiselect--disabled":t.disabled,"multiselect--above":t.isAbove},attrs:{tabindex:t.searchable?-1:t.tabindex},on:{focus:function(e){t.activate()},blur:function(e){!t.searchable&&t.deactivate()},keydown:[function(e){return"button"in e||!t._k(e.keyCode,"down",40,e.key,["Down","ArrowDown"])?e.target!==e.currentTarget?null:(e.preventDefault(),void t.pointerForward()):null},function(e){return"button"in e||!t._k(e.keyCode,"up",38,e.key,["Up","ArrowUp"])?e.target!==e.currentTarget?null:(e.preventDefault(),void t.pointerBackward()):null},function(e){return"button"in e||!t._k(e.keyCode,"enter",13,e.key,"Enter")||!t._k(e.keyCode,"tab",9,e.key,"Tab")?(e.stopPropagation(),e.target!==e.currentTarget?null:void t.addPointerElement(e)):null}],keyup:function(e){if(!("button"in e)&&t._k(e.keyCode,"esc",27,e.key,"Escape"))return null;t.deactivate()}}},[t._t("caret",[n("div",{staticClass:"multiselect__select",on:{mousedown:function(e){e.preventDefault(),e.stopPropagation(),t.toggle()}}})],{toggle:t.toggle}),t._v(" "),t._t("clear",null,{search:t.search}),t._v(" "),n("div",{ref:"tags",staticClass:"multiselect__tags"},[t._t("selection",[n("div",{directives:[{name:"show",rawName:"v-show",value:t.visibleValues.length>0,expression:"visibleValues.length > 0"}],staticClass:"multiselect__tags-wrap"},[t._l(t.visibleValues,function(e,i){return[t._t("tag",[n("span",{key:i,staticClass:"multiselect__tag"},[n("span",{domProps:{textContent:t._s(t.getOptionLabel(e))}}),t._v(" "),n("i",{staticClass:"multiselect__tag-icon",attrs:{"aria-hidden":"true",tabindex:"1"},on:{keydown:function(n){if(!("button"in n)&&t._k(n.keyCode,"enter",13,n.key,"Enter"))return null;n.preventDefault(),t.removeElement(e)},mousedown:function(n){n.preventDefault(),t.removeElement(e)}}})])],{option:e,search:t.search,remove:t.removeElement})]})],2),t._v(" "),t.internalValue&&t.internalValue.length>t.limit?[t._t("limit",[n("strong",{staticClass:"multiselect__strong",domProps:{textContent:t._s(t.limitText(t.internalValue.length-t.limit))}})])]:t._e()],{search:t.search,remove:t.removeElement,values:t.visibleValues,isOpen:t.isOpen}),t._v(" "),n("transition",{attrs:{name:"multiselect__loading"}},[t._t("loading",[n("div",{directives:[{name:"show",rawName:"v-show",value:t.loading,expression:"loading"}],staticClass:"multiselect__spinner"})])],2),t._v(" "),t.searchable?n("input",{ref:"search",staticClass:"multiselect__input",style:t.inputStyle,attrs:{name:t.name,id:t.id,type:"text",autocomplete:"off",placeholder:t.placeholder,disabled:t.disabled,tabindex:t.tabindex},domProps:{value:t.search},on:{input:function(e){t.updateSearch(e.target.value)},focus:function(e){e.preventDefault(),t.activate()},blur:function(e){e.preventDefault(),t.deactivate()},keyup:function(e){if(!("button"in e)&&t._k(e.keyCode,"esc",27,e.key,"Escape"))return null;t.deactivate()},keydown:[function(e){if(!("button"in e)&&t._k(e.keyCode,"down",40,e.key,["Down","ArrowDown"]))return null;e.preventDefault(),t.pointerForward()},function(e){if(!("button"in e)&&t._k(e.keyCode,"up",38,e.key,["Up","ArrowUp"]))return null;e.preventDefault(),t.pointerBackward()},function(e){return"button"in e||!t._k(e.keyCode,"enter",13,e.key,"Enter")?(e.preventDefault(),e.stopPropagation(),e.target!==e.currentTarget?null:void t.addPointerElement(e)):null},function(e){if(!("button"in e)&&t._k(e.keyCode,"delete",[8,46],e.key,["Backspace","Delete"]))return null;e.stopPropagation(),t.removeLastElement()}]}}):t._e(),t._v(" "),t.isSingleLabelVisible?n("span",{staticClass:"multiselect__single",on:{mousedown:function(e){return e.preventDefault(),t.toggle(e)}}},[t._t("singleLabel",[[t._v(t._s(t.currentOptionLabel))]],{option:t.singleValue})],2):t._e(),t._v(" "),t.isPlaceholderVisible?n("span",{staticClass:"multiselect__placeholder",on:{mousedown:function(e){return e.preventDefault(),t.toggle(e)}}},[t._t("placeholder",[t._v("\n            "+t._s(t.placeholder)+"\n        ")])],2):t._e()],2),t._v(" "),n("transition",{attrs:{name:"multiselect"}},[n("div",{directives:[{name:"show",rawName:"v-show",value:t.isOpen,expression:"isOpen"}],ref:"list",staticClass:"multiselect__content-wrapper",style:{maxHeight:t.optimizedHeight+"px"},attrs:{tabindex:"-1"},on:{focus:t.activate,mousedown:function(t){t.preventDefault()}}},[n("ul",{staticClass:"multiselect__content",style:t.contentStyle},[t._t("beforeList"),t._v(" "),t.multiple&&t.max===t.internalValue.length?n("li",[n("span",{staticClass:"multiselect__option"},[t._t("maxElements",[t._v("Maximum of "+t._s(t.max)+" options selected. First remove a selected option to select another.")])],2)]):t._e(),t._v(" "),!t.max||t.internalValue.length<t.max?t._l(t.filteredOptions,function(e,i){return n("li",{key:i,staticClass:"multiselect__element"},[e&&(e.$isLabel||e.$isDisabled)?t._e():n("span",{staticClass:"multiselect__option",class:t.optionHighlight(i,e),attrs:{"data-select":e&&e.isTag?t.tagPlaceholder:t.selectLabelText,"data-selected":t.selectedLabelText,"data-deselect":t.deselectLabelText},on:{click:function(n){n.stopPropagation(),t.select(e)},mouseenter:function(e){if(e.target!==e.currentTarget)return null;t.pointerSet(i)}}},[t._t("option",[n("span",[t._v(t._s(t.getOptionLabel(e)))])],{option:e,search:t.search})],2),t._v(" "),e&&(e.$isLabel||e.$isDisabled)?n("span",{staticClass:"multiselect__option",class:t.groupHighlight(i,e),attrs:{"data-select":t.groupSelect&&t.selectGroupLabelText,"data-deselect":t.groupSelect&&t.deselectGroupLabelText},on:{mouseenter:function(e){if(e.target!==e.currentTarget)return null;t.groupSelect&&t.pointerSet(i)},mousedown:function(n){n.preventDefault(),t.selectGroup(e)}}},[t._t("option",[n("span",[t._v(t._s(t.getOptionLabel(e)))])],{option:e,search:t.search})],2):t._e()])}):t._e(),t._v(" "),n("li",{directives:[{name:"show",rawName:"v-show",value:t.showNoResults&&0===t.filteredOptions.length&&t.search&&!t.loading,expression:"showNoResults && (filteredOptions.length === 0 && search && !loading)"}]},[n("span",{staticClass:"multiselect__option"},[t._t("noResult",[t._v("No elements found. Consider changing the search query.")])],2)]),t._v(" "),n("li",{directives:[{name:"show",rawName:"v-show",value:t.showNoOptions&&0===t.options.length&&!t.search&&!t.loading,expression:"showNoOptions && (options.length === 0 && !search && !loading)"}]},[n("span",{staticClass:"multiselect__option"},[t._t("noOptions",[t._v("List is empty.")])],2)]),t._v(" "),t._t("afterList")],2)])])],2)},r=[],o={render:i,staticRenderFns:r};e.a=o}])});

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_multiselect__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_multiselect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_multiselect__);


Vue.component('dialog-modal', __webpack_require__(5));
Vue.component('service-panel', __webpack_require__(374));
Vue.component('service-list', __webpack_require__(375));
Vue.component('docs-list', __webpack_require__(373));

var data = window.Laravel.user;

var app = new Vue({
	el: '#services',
	data: {
		services: [], // list of Existing Services -REMOVE THIS
		serviceParents: [], // list all parents
		servicetype: ['Parent', 'Child'],
		addOrEditModalTitle: '',
		addorEditFunction: '',
		showChildOptions: false,
		activeId: '', //selected ID on the table
		countChild: '',
		serviceDocuments: [], //list of all service documents
		selectedDocsNeeded: [],
		selectedDocsOpt: [],

		setting: data.access_control[0].setting,
		permissions: data.permissions,
		selpermissions: [{
			addNewService: 0,
			editService: 0,
			deleteService: 0
		}],

		// ADD & EDIT SERVICE FORM
		addServFrm: new Form({
			detail: '',
			detail_cn: '',
			description: '',
			description_cn: '',
			type: '', //parent or child
			parent_id: '',
			cost: '',
			id: '',
			is_active: '',
			docs_needed: [],
			docs_optional: [],
			estimated_cost: ''
		}, { baseURL: '/visa/service-manager' })

	},
	components: {
		Multiselect: __WEBPACK_IMPORTED_MODULE_0_vue_multiselect___default.a
	},

	mounted: function mounted() {
		$('#addService').on('hide.bs.modal', function () {
			//this.commonFunctionReload();
		});
		$('#dialogRemoveService').on('hide.bs.modal', function () {
			//this.commonFunctionReload();
		});
	},


	methods: {
		showRemoveServiceDialog: function showRemoveServiceDialog() {
			$('#dialogRemoveService').modal('toggle');
		},
		addOrEdit: function addOrEdit(d) {
			var _this = this;

			if (d == "add") {
				this.addOrEditModalTitle = "Add Service";
				this.addorEditFunction = 'add';
				this.commonFunctionReload();
				$('#addService').modal('toggle');
			} else if (d == "edit") {
				console.log(this.activeId);
				var needed = [];
				var optional = [];
				axios.get('/visa/service-manager/getService/' + this.activeId).then(function (result) {
					console.log(result.data);
					_this.addServFrm.detail = result.data.detail;
					_this.addServFrm.detail_cn = result.data.detail_cn;
					_this.addServFrm.description = result.data.description;
					_this.addServFrm.description_cn = result.data.description_cn;
					_this.addServFrm.parent_id = result.data.parent_id;
					_this.addServFrm.cost = parseFloat(result.data.cost);
					_this.addServFrm.estimated_cost = result.data.estimated_cost ? parseFloat(result.data.estimated_cost) : 0;
					_this.addServFrm.id = result.data.id;

					if (result.data.select_docs_needed) {
						result.data.select_docs_needed.map(function (value, key) {
							needed.push(value[0]);
						});
					}
					if (result.data.select_docs_optional) {
						result.data.select_docs_optional.map(function (value, key) {
							optional.push(value[0]);
						});
					}
					_this.addServFrm.docs_needed = needed, _this.addServFrm.docs_optional = optional;

					if (result.data.parent_id == 0) {
						_this.addServFrm.type = 'Parent';
						_this.showChildOptions = false;
					} else {
						_this.addServFrm.type = 'Child';
						_this.showChildOptions = true;
					}
				});

				this.addOrEditModalTitle = "Edit Service";
				this.addorEditFunction = 'edit';
				$('#addService').modal('toggle');
			}
		},
		changeActiveId: function changeActiveId(id) {
			this.activeId = id;
		},

		saveForm: function saveForm(event) {
			var _this2 = this;

			var neededList = '';
			var optionalList = '';
			this.addServFrm.docs_needed.map(function (e) {
				if (neededList == '') {
					neededList = e.id;
				} else {
					neededList += "," + e.id;
				}
			});

			this.addServFrm.docs_optional.map(function (e) {
				if (optionalList == '') {
					optionalList = e.id;
				} else {
					optionalList += "," + e.id;
				}
			});

			this.addServFrm.docs_needed = neededList;
			this.addServFrm.docs_optional = optionalList;
			if (this.addorEditFunction == "add") {
				// check if "parent" is selected
				if (this.addServFrm.type == "Parent") {
					this.addServFrm.parent_id = 0;
					this.addServFrm.cost = 0;
				}
				this.addServFrm.is_active = 1;
				// save form
				this.addServFrm.submit('post', '/save').then(function (result) {
					_this2.commonFunctionReload();
					toastr.success('New Service Added.');
				}).catch(function (error) {
					toastr.error('Saving Failed.');
				});
				$('#addService').modal('toggle');
			} else if (this.addorEditFunction == "edit") {
				if (this.addServFrm.type == "Parent") {
					this.addServFrm.parent_id = 0;
					this.addServFrm.cost = 0;
				} else if (this.addServFrm.type == "Child") {
					if (this.addServFrm.parent_id == '0') {
						this.addServFrm.parent_id = '';
					}
					if (this.addServFrm.cost == '0.00') {
						this.addServFrm.cost = '';
					}
				}
				this.addServFrm.submit('post', '/edit').then(function (result) {
					_this2.commonFunctionReload();
					toastr.success('Service Updated.');
				}).catch(function (error) {
					toastr.error('Saving Failed.');
				});
				$('#addService').modal('toggle');
			}
		},

		//show child options on modal
		changeServiceType: function changeServiceType() {
			var type = this.addServFrm.type;
			if (type == "Parent") {
				this.showChildOptions = false;
			} else if (type == "Child") {
				this.showChildOptions = true;
			}
		},


		//reset add/edit service form
		emptyform: function emptyform() {
			this.activeId = '';
			this.showChildOptions = false;
			this.selectedDocsNeeded = [];
			this.selectedDocsOpt = [];
			this.addServFrm = new Form({
				detail: '',
				detail_cn: '',
				description: '',
				description_cn: '',
				type: '', //parent or child
				parent_id: '',
				cost: '',
				id: '',
				is_active: '',
				docs_needed: [],
				docs_optional: [],
				estimated_cost: ''
			}, { baseURL: '/visa/service-manager' });
		},
		deleteService: function deleteService() {
			var _this3 = this;

			this.countChildren();
			if (this.countChild == 0) {
				axios.get('/visa/service-manager/delete/' + this.activeId).then(function (result) {
					_this3.commonFunctionReload();
					toastr.success('Service Deleted.');
				}).catch(function (error) {
					_this3.commonFunctionReload();
					toastr.error('Delete Failed.');
				});
			} else {
				toastr.error('Delete Failed. Delete child services first.');
			}
		},
		loadService: function loadService() {
			var _this4 = this;

			axios.get('/visa/service-manager/show').then(function (result) {
				_this4.services = result.data;
			});
		},
		loadParents: function loadParents() {
			var _this5 = this;

			axios.get('/visa/service-manager/showParents').then(function (result) {
				_this5.serviceParents = result.data;
				_this5.callDataTables();
			});
		},
		loadDocs: function loadDocs() {
			var _this6 = this;

			axios.get('/visa/service-manager/service-documents').then(function (result) {
				_this6.serviceDocuments = result.data;
			});
		},
		commonFunctionReload: function commonFunctionReload() {
			this.emptyform();
			this.loadService();
			this.loadParents();
		},
		reloadDataTable: function reloadDataTable() {
			setTimeout(function () {
				$('#lists').DataTable({
					pageLength: 10,
					responsive: true,
					dom: '<"top"lf>rt<"bottom"ip><"clear">'
				});
			}, 500);
		},
		countChildren: function countChildren(id) {
			var _this7 = this;

			axios.get('/visa/service-manager/showChildren/' + id).then(function (result) {
				_this7.countChild = result.data.length;
			});
		},
		callDataTables: function callDataTables() {
			$('#lists').DataTable().destroy();

			var vm = this;

			setTimeout(function () {

				var table = $('#lists').DataTable({
					pageLength: 10,
					responsive: true,
					dom: '<"top"lf>r<"clear">t<"bottom"p>',
					order: [[0, "asc"]],
					scrollY: "500px",
					autoWidth: false
				});

				$('#lists tbody').on('click', '.toggle-row', function () {
					var dis = $(this).attr('id');
					var tr = $(this).closest('tr');
					var row = table.row(tr);

					if (row.child.isShown()) {
						row.child.hide();
						tr.removeClass('shown');
					} else {
						var id = dis.split("_");
						axios.get('/visa/service-manager/showChildren/' + id[1]).then(function (result) {
							var parent = "#parent_" + id[1];
							$(parent).closest("td").closest("tr").remove();
							$(parent).closest("td").remove();

							$(parent).remove();
							$(parent + " tr").remove();
							row.child(vm.formatRow(result.data, id[1])).show();
						});
						tr.addClass('shown');
					}
				});
			}, 1000);
		},
		formatRow: function formatRow(d, parent) {
			if (d.length > 0) {
				var ret = '<table class="table table-bordered table-striped table-hovered" id="parent_' + parent + '">';
				for (var i = 0; i < d.length; i++) {
					var btn = '<center>';
					btn += '<a style="margin-right:8px;" class="editServ" id="serv_' + d[i].id + '" data-toggle="tooltip" title="Edit Service">';
					btn += '<i class="fa fa-pencil fa-1x"></i>';
					btn += '</a>';
					btn += '<a data-toggle="tooltip"  class="deleteServ" id="dserv_' + d[i].id + '" title="Delete">';
					btn += '<i class="fa fa-trash fa-1x"></i>';
					btn += '</a>';
					btn += '</center>';
					ret += '<tr>' + '<td><p style="padding-left: 30px; margin-bottom: 0px;">> ' + d[i].detail + '</p></td>' + '<td>' + d[i].description + '</td>' + '<td>' + d[i].cost + '</td>' + '<td>' + btn + '</td>' + '</tr>';
				}
				ret += '</table>';
				return ret;
			}
		}
	},
	created: function created() {

		var x = this.permissions.filter(function (obj) {
			return obj.type === 'Service Manager';
		});

		var y = x.filter(function (obj) {
			return obj.name === 'Add New Service';
		});

		if (y.length == 0) this.selpermissions.addNewService = 0;else this.selpermissions.addNewService = 1;

		var y = x.filter(function (obj) {
			return obj.name === 'Edit Service';
		});

		if (y.length == 0) this.selpermissions.editService = 0;else this.selpermissions.editService = 1;

		var y = x.filter(function (obj) {
			return obj.name === 'Delete Service';
		});

		if (y.length == 0) this.selpermissions.deleteService = 0;else this.selpermissions.deleteService = 1;

		this.loadService();
		this.loadParents();
		this.loadDocs();
		this.callDataTables();
	}
});

var app2 = new Vue({
	el: '#documents',
	data: {
		documents: [],
		act: '',
		sel_id: '',

		setting: data.access_control[0].setting,
		permissions: data.permissions,
		selpermissions: [{
			addNewDocument: 0,
			editDocument: 0,
			deleteDocument: 0
		}],

		docsFrm: new Form({
			id: '',
			title: '',
			title_cn: ''
		}, { baseURL: '/visa/service-manager/service-documents' })
	},
	methods: {
		loadDocs: function loadDocs() {
			var _this8 = this;

			axios.get('/visa/service-manager/service-documents').then(function (result) {
				_this8.documents = result.data;
			});
		},
		reloadDataTable: function reloadDataTable() {
			setTimeout(function () {
				$('#lists').DataTable({
					responsive: true
				});
			}, 500);
		},
		saveDocs: function saveDocs(e) {
			var _this9 = this;

			if (e == "Add") {
				this.docsFrm.submit('post', '/add').then(function (result) {
					toastr.success('New Service Document Added.');
					$('#dialogAddDocument').modal('toggle');
					_this9.reloadAll();
				}).catch(function (error) {
					toastr.error('Saving Failed.');
				});
			} else if (e == "Edit") {
				this.docsFrm.submit('post', '/edit').then(function (result) {
					toastr.success('Service Document Updated');
					$('#dialogAddDocument').modal('toggle');
					_this9.reloadAll();
				}).catch(function (error) {
					toastr.error('Saving Failed.');
				});
			} else if (e == "Delete") {
				axios.post('/visa/service-manager/service-documents/delete/' + this.sel_id).then(function (result) {
					toastr.success('Service Document Deleted');
					_this9.reloadAll();
				}).catch(function (error) {
					toastr.error('Delete Failed.');
				});
			}
		},
		actionDoc: function actionDoc(e, id) {
			var _this10 = this;

			if (e == "add") {
				$('#dialogAddDocument').modal('toggle');
				this.act = "Add";
				console.log(id);
				console.log(e);
			} else if (e == "edit") {
				this.act = "Edit";
				axios.get('/visa/service-manager/service-documents/info/' + id).then(function (result) {
					_this10.docsFrm.id = result.data.id;
					_this10.docsFrm.title = result.data.title;
					_this10.docsFrm.title_cn = result.data.title_cn;
				});
				$('#dialogAddDocument').modal('toggle');
			} else if (e == "delete") {
				this.act = "Delete";
				this.sel_id = id;
				$('#dialogRemoveDocument').modal('toggle');
			}
		},
		resetDocFrm: function resetDocFrm() {
			this.docsFrm = new Form({
				id: '',
				title: '',
				title_cn: ''
			}, { baseURL: '/visa/service-manager/service-documents' });
		},
		reloadAll: function reloadAll() {
			$('#lists').DataTable().destroy();
			this.resetDocFrm();
			this.loadDocs();
			this.reloadDataTable();
		}
	},
	created: function created() {

		var x = this.permissions.filter(function (obj) {
			return obj.type === 'Service Manager';
		});

		var y = x.filter(function (obj) {
			return obj.name === 'Add New Document';
		});

		if (y.length == 0) this.selpermissions.addNewDocument = 0;else this.selpermissions.addNewDocument = 1;

		var y = x.filter(function (obj) {
			return obj.name === 'Edit Document';
		});

		if (y.length == 0) this.selpermissions.editDocument = 0;else this.selpermissions.editDocument = 1;

		var y = x.filter(function (obj) {
			return obj.name === 'Delete Document';
		});

		if (y.length == 0) this.selpermissions.deleteDocument = 0;else this.selpermissions.deleteDocument = 1;

		this.loadDocs();
		this.reloadDataTable();
	}
});

$(document).ready(function () {
	//for editing of child
	$(document).on("click", ".editServ", function () {
		var id = $(this).attr('id');
		var id = id.split("_");
		app.changeActiveId(id[1]);
		app.addOrEdit('edit');
	});

	//for deleting of child
	$(document).on("click", ".deleteServ", function () {
		var id = $(this).attr('id');
		var id = id.split("_");
		app.changeActiveId(id[1]);
		app.showRemoveServiceDialog();
	});
});

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['chld'],
    methods: {
        ed: function ed(id) {
            this.$parent.$parent.activeId = id;
            this.$parent.$parent.addOrEdit('edit');
        },
        de: function de(id) {
            this.$parent.$parent.activeId = id;
            this.$parent.$parent.showRemoveServiceDialog();
        }
    }
});

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['item']
});

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

Vue.component('childservice', __webpack_require__(167));
/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['item'],
    methods: {
        edit: function edit(id) {
            this.$parent.activeId = id;
            this.$parent.addOrEdit('edit');
        },
        del: function del(id) {
            this.$parent.activeId = id;
            this.$parent.showRemoveServiceDialog();
        }
    }
});

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_services_ChildServices_vue__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_services_ChildServices_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_services_ChildServices_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['item'],
    components: {
        'child-service': __WEBPACK_IMPORTED_MODULE_0__components_services_ChildServices_vue___default.a
    },
    methods: {
        edit: function edit(id) {
            this.$parent.activeId = id;
            this.$parent.addOrEdit('edit');
        },
        del: function del(id) {
            this.$parent.activeId = id;
            this.$parent.showRemoveServiceDialog();
        }
    }
});

/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(281),
  /* template */
  __webpack_require__(391),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/services/DocumentList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DocumentList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4450ba3a", Component.options)
  } else {
    hotAPI.reload("data-v-4450ba3a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 374:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(282),
  /* template */
  __webpack_require__(389),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/services/ServicePanel.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ServicePanel.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ea9c9a0", Component.options)
  } else {
    hotAPI.reload("data-v-3ea9c9a0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 375:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(283),
  /* template */
  __webpack_require__(412),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/services/Services.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Services.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8132c782", Component.options)
  } else {
    hotAPI.reload("data-v-8132c782", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 389:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "ibox collapsed service-panel"
  }, [_c('div', {
    staticClass: "ibox-title"
  }, [_c('h5', [_vm._v(_vm._s(_vm.item.detail) + "\n            "), _c('small', {
    staticClass: "m-l-sm"
  }, [_vm._v(_vm._s(_vm.item.description))])]), _vm._v(" "), _c('div', {
    staticClass: "ibox-tools"
  }, [(_vm.item.children.length != 0) ? _c('a', {
    staticClass: "collapse-link"
  }, [_c('i', {
    staticClass: "fa fa-chevron-up"
  })]) : _vm._e(), _vm._v(" "), _c('a', {
    staticStyle: {
      "margin-right": "8px"
    },
    attrs: {
      "data-toggle": "tooltip",
      "title": "Edit"
    },
    on: {
      "click": function($event) {
        _vm.edit(_vm.item.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-pencil fa-1x"
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "data-toggle": "tooltip",
      "title": "Delete"
    },
    on: {
      "click": function($event) {
        _vm.del(_vm.item.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-trash fa-1x"
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "ibox-content"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "panel-group"
  }, _vm._l((_vm.item.children), function(chlsrv) {
    return _c('childservice', {
      key: chlsrv.id,
      attrs: {
        "chld": chlsrv
      }
    })
  }), 1)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3ea9c9a0", module.exports)
  }
}

/***/ }),

/***/ 391:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('tr', [_c('td', {
    staticClass: "col-lg-5 toggle-row",
    staticStyle: {
      "cursor": "pointer"
    }
  }, [_c('p', [_c('span', [_c('b', [_vm._v(_vm._s(_vm.item.title))])]), _c('span', [_vm._v(" " + _vm._s(_vm.item.title_cn) + " ")])])]), _vm._v(" "), _c('td', {
    staticClass: "col-lg-1"
  }, [_c('center', [_c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.$parent.selpermissions.editDocument == 1 || _vm.$parent.setting == 1),
      expression: "$parent.selpermissions.editDocument==1 || $parent.setting == 1"
    }],
    staticStyle: {
      "margin-right": "8px"
    },
    attrs: {
      "data-toggle": "tooltip",
      "title": "Edit Document"
    },
    on: {
      "click": function($event) {
        _vm.$parent.actionDoc('edit', _vm.item.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-pencil fa-1x"
  })]), _vm._v(" "), _c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.$parent.selpermissions.deleteDocument == 1 || _vm.$parent.setting == 1),
      expression: "$parent.selpermissions.deleteDocument==1 || $parent.setting == 1"
    }],
    attrs: {
      "data-toggle": "tooltip",
      "title": "Delete"
    },
    on: {
      "click": function($event) {
        _vm.$parent.actionDoc('delete', _vm.item.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-trash fa-1x"
  })])])], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4450ba3a", module.exports)
  }
}

/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-heading"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-11"
  }, [_c('h5', {
    staticClass: "panel-title"
  }, [_c('a', [_vm._v(_vm._s(_vm.chld.detail))]), _vm._v(" "), _c('span', {
    staticClass: "label label-info"
  }, [_vm._v(_vm._s(_vm.chld.cost))]), _vm._v(" "), _c('span', [_c('small', {
    staticClass: "m-l-sm"
  }, [_vm._v(_vm._s(_vm.chld.description))])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-1"
  }, [_c('a', {
    staticStyle: {
      "margin-right": "8px"
    },
    attrs: {
      "data-toggle": "tooltip",
      "title": "Edit"
    },
    on: {
      "click": function($event) {
        _vm.ed(_vm.chld.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-pencil fa-1x"
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "data-toggle": "tooltip",
      "title": "Delete"
    },
    on: {
      "click": function($event) {
        _vm.de(_vm.chld.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-trash fa-1x"
  })])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5ca91eee", module.exports)
  }
}

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 412:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('tr', [_c('td', {
    staticClass: "col-lg-5 toggle-row",
    staticStyle: {
      "cursor": "pointer"
    },
    attrs: {
      "id": 'p_' + _vm.item.id
    }
  }, [_c('b', [_vm._v(_vm._s(_vm.item.detail))])]), _vm._v(" "), _c('td', {
    staticClass: "col-lg-5"
  }, [_c('b', [_vm._v(_vm._s(_vm.item.description))])]), _vm._v(" "), _c('td', {
    staticClass: "col-lg-1"
  }, [(_vm.item.cost != '0.00') ? _c('span', [_c('b', [_vm._v(_vm._s(_vm.item.cost))])]) : _vm._e()]), _vm._v(" "), _c('td', {
    staticClass: "col-lg-1"
  }, [_c('center', [_c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.$parent.selpermissions.editService == 1 || _vm.$parent.setting == 1),
      expression: "$parent.selpermissions.editService==1 || $parent.setting == 1"
    }],
    staticStyle: {
      "margin-right": "8px"
    },
    attrs: {
      "data-toggle": "tooltip",
      "title": "Edit Service"
    },
    on: {
      "click": function($event) {
        _vm.edit(_vm.item.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-pencil fa-1x"
  })]), _vm._v(" "), _c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.$parent.selpermissions.deleteService == 1 || _vm.$parent.setting == 1),
      expression: "$parent.selpermissions.deleteService==1 || $parent.setting == 1"
    }],
    attrs: {
      "data-toggle": "tooltip",
      "title": "Delete"
    },
    on: {
      "click": function($event) {
        _vm.del(_vm.item.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-trash fa-1x"
  })])])], 1), _vm._v(" "), _c('td', {
    staticClass: "hide"
  }, [_vm._v("Extra Data 1")]), _vm._v(" "), _c('td', {
    staticClass: "hide"
  }, [_vm._v("Extra Data 2")]), _vm._v(" "), _c('td', {
    staticClass: "hide"
  }, [_vm._v("Extra Data 3")]), _vm._v(" "), _c('td', {
    staticClass: "hide"
  }, [_vm._v("Extra Data 4")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-8132c782", module.exports)
  }
}

/***/ }),

/***/ 488:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(198);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL0NoaWxkU2VydmljZXMudnVlIiwid2VicGFjazovLy8uL34vdnVlLW11bHRpc2VsZWN0L2Rpc3QvdnVlLW11bHRpc2VsZWN0Lm1pbi5qcz84ZGYzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2Evc2VydmljZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vL0NoaWxkU2VydmljZXMudnVlIiwid2VicGFjazovLy9Eb2N1bWVudExpc3QudnVlIiwid2VicGFjazovLy9TZXJ2aWNlUGFuZWwudnVlIiwid2VicGFjazovLy9TZXJ2aWNlcy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL0RvY3VtZW50TGlzdC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL1NlcnZpY2VQYW5lbC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL1NlcnZpY2VzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvc2VydmljZXMvU2VydmljZVBhbmVsLnZ1ZT9kNGQxIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9zZXJ2aWNlcy9Eb2N1bWVudExpc3QudnVlPzQ3ODgiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL0NoaWxkU2VydmljZXMudnVlPzZlZTgiLCJ3ZWJwYWNrOi8vL0RpYWxvZ01vZGFsLnZ1ZT9lNDc0KioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvc2VydmljZXMvU2VydmljZXMudnVlPzU5Y2EiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT8yMDdjKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT9mZTFiKioiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImRhdGEiLCJ3aW5kb3ciLCJMYXJhdmVsIiwidXNlciIsImFwcCIsImVsIiwic2VydmljZXMiLCJzZXJ2aWNlUGFyZW50cyIsInNlcnZpY2V0eXBlIiwiYWRkT3JFZGl0TW9kYWxUaXRsZSIsImFkZG9yRWRpdEZ1bmN0aW9uIiwic2hvd0NoaWxkT3B0aW9ucyIsImFjdGl2ZUlkIiwiY291bnRDaGlsZCIsInNlcnZpY2VEb2N1bWVudHMiLCJzZWxlY3RlZERvY3NOZWVkZWQiLCJzZWxlY3RlZERvY3NPcHQiLCJzZXR0aW5nIiwiYWNjZXNzX2NvbnRyb2wiLCJwZXJtaXNzaW9ucyIsInNlbHBlcm1pc3Npb25zIiwiYWRkTmV3U2VydmljZSIsImVkaXRTZXJ2aWNlIiwiZGVsZXRlU2VydmljZSIsImFkZFNlcnZGcm0iLCJGb3JtIiwiZGV0YWlsIiwiZGV0YWlsX2NuIiwiZGVzY3JpcHRpb24iLCJkZXNjcmlwdGlvbl9jbiIsInR5cGUiLCJwYXJlbnRfaWQiLCJjb3N0IiwiaWQiLCJpc19hY3RpdmUiLCJkb2NzX25lZWRlZCIsImRvY3Nfb3B0aW9uYWwiLCJlc3RpbWF0ZWRfY29zdCIsImJhc2VVUkwiLCJjb21wb25lbnRzIiwiTXVsdGlzZWxlY3QiLCJtb3VudGVkIiwiJCIsIm9uIiwibWV0aG9kcyIsInNob3dSZW1vdmVTZXJ2aWNlRGlhbG9nIiwibW9kYWwiLCJhZGRPckVkaXQiLCJkIiwiY29tbW9uRnVuY3Rpb25SZWxvYWQiLCJjb25zb2xlIiwibG9nIiwibmVlZGVkIiwib3B0aW9uYWwiLCJheGlvcyIsImdldCIsInRoZW4iLCJyZXN1bHQiLCJwYXJzZUZsb2F0Iiwic2VsZWN0X2RvY3NfbmVlZGVkIiwibWFwIiwidmFsdWUiLCJrZXkiLCJwdXNoIiwic2VsZWN0X2RvY3Nfb3B0aW9uYWwiLCJjaGFuZ2VBY3RpdmVJZCIsInNhdmVGb3JtIiwiZXZlbnQiLCJuZWVkZWRMaXN0Iiwib3B0aW9uYWxMaXN0IiwiZSIsInN1Ym1pdCIsInRvYXN0ciIsInN1Y2Nlc3MiLCJjYXRjaCIsImVycm9yIiwiY2hhbmdlU2VydmljZVR5cGUiLCJlbXB0eWZvcm0iLCJjb3VudENoaWxkcmVuIiwibG9hZFNlcnZpY2UiLCJsb2FkUGFyZW50cyIsImNhbGxEYXRhVGFibGVzIiwibG9hZERvY3MiLCJyZWxvYWREYXRhVGFibGUiLCJzZXRUaW1lb3V0IiwiRGF0YVRhYmxlIiwicGFnZUxlbmd0aCIsInJlc3BvbnNpdmUiLCJkb20iLCJsZW5ndGgiLCJkZXN0cm95Iiwidm0iLCJ0YWJsZSIsIm9yZGVyIiwic2Nyb2xsWSIsImF1dG9XaWR0aCIsImRpcyIsImF0dHIiLCJ0ciIsImNsb3Nlc3QiLCJyb3ciLCJjaGlsZCIsImlzU2hvd24iLCJoaWRlIiwicmVtb3ZlQ2xhc3MiLCJzcGxpdCIsInBhcmVudCIsInJlbW92ZSIsImZvcm1hdFJvdyIsInNob3ciLCJhZGRDbGFzcyIsInJldCIsImkiLCJidG4iLCJjcmVhdGVkIiwieCIsImZpbHRlciIsIm9iaiIsInkiLCJuYW1lIiwiYXBwMiIsImRvY3VtZW50cyIsImFjdCIsInNlbF9pZCIsImFkZE5ld0RvY3VtZW50IiwiZWRpdERvY3VtZW50IiwiZGVsZXRlRG9jdW1lbnQiLCJkb2NzRnJtIiwidGl0bGUiLCJ0aXRsZV9jbiIsInNhdmVEb2NzIiwicmVsb2FkQWxsIiwicG9zdCIsImFjdGlvbkRvYyIsInJlc2V0RG9jRnJtIiwiZG9jdW1lbnQiLCJyZWFkeSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBLGdCQUFnQixtQkFBTyxDQUFDLENBQXdFO0FBQ2hHO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQThPO0FBQ3hQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTZNO0FBQ3ZOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxlQUFlLEtBQWlELGdKQUFnSixpQkFBaUIsbUJBQW1CLGNBQWMsNEJBQTRCLFlBQVkscUJBQXFCLDJEQUEyRCxTQUFTLG1DQUFtQyxTQUFTLHFCQUFxQixxQ0FBcUMsb0NBQW9DLEVBQUUsaUJBQWlCLGlDQUFpQyxpQkFBaUIsWUFBWSxVQUFVLHNCQUFzQixtQkFBbUIsaURBQWlELG1CQUFtQixnQkFBZ0IsOElBQThJLDhCQUE4QixpQkFBaUIsZ0VBQWdFLHVCQUF1QixrREFBa0QsVUFBVSxpQkFBaUIsV0FBVyxzQkFBc0IsaURBQWlELFVBQVUsaUJBQWlCLDJEQUEyRCwwRUFBMEUsV0FBVyxnQ0FBZ0MsZ0NBQWdDLEVBQUUsU0FBUyxvS0FBb0ssMEVBQTBFLGlCQUFpQiwyQkFBMkIsa0NBQWtDLE1BQU0sZUFBZSxVQUFVLElBQUksRUFBRSxlQUFlLHNCQUFzQix3REFBd0QsaUJBQWlCLHdGQUF3RixnQ0FBZ0MsaUJBQWlCLDhCQUE4QiwyQkFBMkIsMEpBQTBKLDJDQUEyQyxxREFBcUQsRUFBRSxlQUFlLHNCQUFzQixJQUFJLFlBQVksU0FBUyxXQUFXLGlCQUFpQixvQkFBb0IsK0JBQStCLHVCQUF1QixpQkFBaUIsaUJBQWlCLGVBQWUsUUFBUSxVQUFVLHNCQUFzQiw4QkFBOEIsZUFBZSxpQkFBaUIsaUJBQWlCLDhCQUE4QixpQkFBaUIsWUFBWSwwQkFBMEIsNEJBQTRCLFVBQVUsMEJBQTBCLG9CQUFvQiw0QkFBNEIsc0JBQXNCLDhCQUE4Qix3QkFBd0Isa0JBQWtCLDhCQUE4QixlQUFlLFFBQVEsZ0JBQWdCLHdCQUF3QixvQkFBb0IsaUJBQWlCLG1EQUFtRCwrQ0FBK0MsNkJBQTZCLGdCQUFnQixVQUFVLG9FQUFvRSxxQ0FBcUMsZUFBZSxzQkFBc0IsaUVBQWlFLFVBQVUsZUFBZSxhQUFhLGVBQWUsc0JBQXNCLHlEQUF5RCxVQUFVLGlCQUFpQixhQUFhLFdBQVcsd0JBQXdCLHdCQUF3QiwwQkFBMEIsaUJBQWlCLEdBQUcsaUJBQWlCLG9CQUFvQixzQkFBc0IsZ0JBQWdCLGlCQUFpQix1QkFBdUIsc0JBQXNCLHVDQUF1QyxpQkFBaUIsNENBQTRDLHdCQUF3Qix3REFBd0QsdUJBQXVCLGtGQUFrRixJQUFJLHNEQUFzRCxvQkFBb0IsZ0JBQWdCLGdCQUFnQixnQkFBZ0IsaUJBQWlCLG1CQUFtQix1QkFBdUIsaUJBQWlCLHNEQUFzRCxzQkFBc0IsZ0NBQWdDLGVBQWUscUhBQXFILGlCQUFpQixXQUFXLGlFQUFpRSw0Q0FBNEMsZUFBZSxhQUFhLGVBQWUsd0JBQXdCLE9BQU8sZ0VBQWdFLGlCQUFpQiw0Q0FBNEMsMEJBQTBCLG1DQUFtQyx3QkFBd0IsR0FBRyxpQkFBaUIsNEJBQTRCLHNCQUFzQiwwQkFBMEIsaUJBQWlCLFlBQVksc0JBQXNCLHFCQUFxQixpQkFBaUIsV0FBVyx3QkFBd0Isa0JBQWtCLFFBQVEsaUVBQWlFLDZEQUE2RCxrRUFBa0UsNERBQTRELGVBQWUsd0JBQXdCLHNCQUFzQixtRUFBbUUsaUJBQWlCLGFBQWEsMkxBQTJMLGNBQWMsbUNBQW1DLG9CQUFvQiw0QkFBNEIsbUJBQW1CLGdEQUFnRCxnQkFBZ0Isd0JBQXdCLHlCQUF5QixNQUFNLDBCQUEwQixNQUFNLGlCQUFpQixzQ0FBc0MsSUFBSSw4Q0FBOEMsc0JBQXNCLFVBQVUscUNBQXFDLGNBQWMsb0NBQW9DLHVDQUF1QyxrQkFBa0IsMkNBQTJDLGtOQUFrTixXQUFXLHdDQUF3QyxrREFBa0QsaUJBQWlCLGFBQWEsY0FBYyx1REFBdUQsY0FBYyxrQkFBa0Isa0NBQWtDLGdCQUFnQiw4SEFBOEgsb0JBQW9CLDRCQUE0QixtQkFBbUIsRUFBRSxjQUFjLDRCQUE0QixrQkFBa0IsRUFBRSxnQkFBZ0IsbUJBQW1CLDhCQUE4QixrQ0FBa0MsNkJBQTZCLG9CQUFvQixNQUFNLHNCQUFzQixtQkFBbUIseUJBQXlCLE1BQU0sZ0hBQWdILG9CQUFvQixxQkFBcUIsMENBQTBDLEdBQUcsNE9BQTRPLDhDQUE4QyxJQUFJLHNCQUFzQixtQkFBbUIsOEJBQThCLFlBQVksS0FBSyxFQUFFLEtBQUssZ0JBQWdCLE9BQU8sbUZBQW1GLFFBQVEsZ0JBQWdCLHdCQUF3QixVQUFVLHVCQUF1QixXQUFXLHdCQUF3QixRQUFRLDZCQUE2QixVQUFVLFVBQVUsWUFBWSxRQUFRLFlBQVksYUFBYSx3QkFBd0IsZ0JBQWdCLHdCQUF3QixlQUFlLHdCQUF3QixjQUFjLG9DQUFvQyxhQUFhLHdCQUF3QixhQUFhLHdCQUF3QixnQkFBZ0Isd0JBQXdCLGNBQWMsb0NBQW9DLHlCQUF5QixXQUFXLHdCQUF3QixpQkFBaUIsa0RBQWtELGNBQWMsMEJBQTBCLE1BQU0saUNBQWlDLEtBQUssYUFBYSxlQUFlLHdCQUF3QixjQUFjLFlBQVksYUFBYSxZQUFZLGNBQWMsd0JBQXdCLFlBQVksOEJBQThCLFVBQVUsaUJBQWlCLHdCQUF3QixpQkFBaUIseUJBQXlCLG9CQUFvQixrWEFBa1gsV0FBVyx5QkFBeUIsdUZBQXVGLDRCQUE0Qix1RUFBdUUsMFRBQTBULGlCQUFpQixhQUFhLGlCQUFpQixnQ0FBZ0Msc0JBQXNCLFdBQVcsdURBQXVELG9CQUFvQixxQkFBcUIsdUJBQXVCLFdBQVcsc0ZBQXNGLHlEQUF5RCxFQUFFLCtCQUErQixtS0FBbUssUUFBUSx5QkFBeUIsdUdBQXVHLG1CQUFtQixpREFBaUQsVUFBVSxvQkFBb0IsaUdBQWlHLCtCQUErQiwwR0FBMEcsMEJBQTBCLG1EQUFtRCwwQkFBMEIsY0FBYyw4QkFBOEIsb0RBQW9ELHdCQUF3QixxQ0FBcUMsb0NBQW9DLDRCQUE0QixpQkFBaUIsMEJBQTBCLG1DQUFtQyxxQ0FBcUMsaUJBQWlCLHNCQUFzQixnRUFBZ0UsbUxBQW1MLGtIQUFrSCxLQUFLLG9FQUFvRSwyS0FBMkssdUNBQXVDLHlCQUF5QiwyQ0FBMkMsdUNBQXVDLEVBQUUsb0NBQW9DLGlEQUFpRCw0Q0FBNEMsdUNBQXVDLEVBQUUsOEJBQThCLEtBQUsscURBQXFELHlGQUF5RixnQ0FBZ0Msa0RBQWtELDJCQUEyQixpRUFBaUUsbUJBQW1CLGdGQUFnRiwrRkFBK0YsaURBQWlELDBFQUEwRSw4QkFBOEIsc0NBQXNDLDBDQUEwQyw4QkFBOEIseUtBQXlLLHFCQUFxQixXQUFXLHFPQUFxTyw4QkFBOEIsZ0RBQWdELHVCQUF1Qix5S0FBeUssbUJBQW1CLDhDQUE4QywyQkFBMkIsK0JBQStCLHdHQUF3Ryx5UUFBeVEsaUJBQWlCLGFBQWEsNkJBQTZCLE9BQU8sS0FBSyxnQkFBZ0IsT0FBTywyQkFBMkIsUUFBUSxhQUFhLHdCQUF3QixlQUFlLHdCQUF3QixXQUFXLDJCQUEyQixzQ0FBc0MsNEJBQTRCLCtDQUErQyxRQUFRLDJCQUEyQixxQkFBcUIsbUJBQW1CLHNCQUFzQixVQUFVLDhCQUE4QixPQUFPLHdIQUF3SCw4QkFBOEIsV0FBVywwRkFBMEYsb0NBQW9DLHVDQUF1QyxFQUFFLHFDQUFxQyxvRUFBb0UsRUFBRSxpRUFBaUUsRUFBRSw4QkFBOEIsNkVBQTZFLHFHQUFxRywyQkFBMkIsb1lBQW9ZLDRCQUE0QixpWUFBaVkseUJBQXlCLG9GQUFvRiwwQkFBMEIsNk9BQTZPLHdCQUF3Qix1Q0FBdUMsaUJBQWlCLGFBQWEsb0NBQW9DLDRDQUE0QyxpQ0FBaUMsWUFBWSxvQ0FBb0MsaUdBQWlHLGtFQUFrRSxpQkFBaUIsYUFBYSxxQ0FBcUMsS0FBSywrQ0FBK0MsTUFBTSx1QkFBdUIsY0FBYyw0Q0FBNEMsbUJBQW1CLGtEQUFrRCxnQkFBZ0IsK0JBQStCLGdCQUFnQiw0Q0FBNEMscUJBQXFCLG9EQUFvRCxhQUFhLHdCQUF3QixRQUFRLDBCQUEwQixZQUFZLHdCQUF3QixZQUFZLGtDQUFrQyxnQ0FBZ0MsVUFBVSx3QkFBd0IsV0FBVyx3QkFBd0IsZ0JBQWdCLHVCQUF1QixnQkFBZ0Isd0JBQXdCLGdCQUFnQix3QkFBd0IsV0FBVyx1QkFBdUIsV0FBVyxnQ0FBZ0Msc0ZBQXNGLGlDQUFpQyxpRUFBaUUsMEJBQTBCLCtEQUErRCx3QkFBd0IsNkJBQTZCLDhCQUE4Qiw2Q0FBNkMsbUNBQW1DLGtEQUFrRCw0QkFBNEIsMkNBQTJDLGlDQUFpQyxnREFBZ0QsOEJBQThCLDZDQUE2Qyx1QkFBdUIscUZBQXFGLGFBQWEsRUFBRSwyQ0FBMkMseUJBQXlCLDRCQUE0Qix1QkFBdUIsRUFBRSxpQkFBaUIsb0JBQW9CLG1LQUFtSyw0QkFBNEIsNkhBQTZILGlCQUFpQiw0Q0FBNEMseUJBQXlCLHdCQUF3QixZQUFZLGlCQUFpQiw0QkFBNEIsc0JBQXNCLHVCQUF1QixvQ0FBb0MsWUFBWSxLQUFLLElBQUksMkJBQTJCLFVBQVUsSUFBSSw0Q0FBNEMsZUFBZSxpQkFBaUIsNkRBQTZELGlCQUFpQixvQkFBb0IsSUFBSSxZQUFZLFlBQVksc0JBQXNCLFVBQVUsMkpBQTJKLGlCQUFpQixhQUFhLFdBQVcscUJBQXFCLG1CQUFtQixpSEFBaUgsaUJBQWlCLG9CQUFvQiwrQkFBK0IsaUJBQWlCLGtDQUFrQyxrREFBa0QsZUFBZSxVQUFVLElBQUksRUFBRSxpQkFBaUIsV0FBVyxxQ0FBcUMscUJBQXFCLGlCQUFpQixhQUFhLGNBQWMsUUFBUSxpQ0FBaUMscUVBQXFFLFFBQVEscUNBQXFDLFlBQVksd0JBQXdCLGlCQUFpQixpQkFBaUIsNkRBQTZELGNBQWMsbUNBQW1DLHVLQUF1SyxJQUFJLDBCQUEwQixZQUFZLHVDQUF1QyxNQUFNLDhGQUE4RixpQkFBaUIsc0ZBQXNGLHlCQUF5QiwwQkFBMEIsY0FBYyxVQUFVLHlDQUF5QyxpQkFBaUIsb0RBQW9ELHdCQUF3QixzQkFBc0IsbUNBQW1DLEtBQUssV0FBVyxxQ0FBcUMsVUFBVSxpQkFBaUIsb0JBQW9CLG1DQUFtQyxlQUFlLGlCQUFpQiwwQkFBMEIsd0JBQXdCLHlDQUF5QyxhQUFhLGtDQUFrQyxpQkFBaUIseUVBQXlFLEVBQUUseUJBQXlCLGtDQUFrQyxFQUFFLHVCQUF1Qiw4RkFBOEYsRUFBRSxpQkFBaUIscUNBQXFDLHdCQUF3Qix5QkFBeUIsK0NBQStDLGlCQUFpQixnSEFBZ0gsUUFBUSxnQkFBZ0IsMEJBQTBCLHFCQUFxQixvQ0FBb0Msd0JBQXdCLDJFQUEyRSxZQUFZLGlCQUFpQix5SUFBeUksY0FBYyxZQUFZLHdCQUF3QixXQUFXLGlCQUFpQixlQUFlLGdCQUFnQixxQkFBcUIsaUJBQWlCLG1CQUFtQix3QkFBd0IseUJBQXlCLHdDQUF3QyxRQUFRLGVBQWUsWUFBWSxrQ0FBa0MscUJBQXFCLHdCQUF3QixnQkFBZ0Isc0pBQXNKLHdCQUF3QixzRkFBc0YseURBQXlELCtCQUErQixhQUFhLHVCQUF1QixhQUFhLGVBQWUsZUFBZSw2QkFBNkIsc0JBQXNCLG1DQUFtQyxpQkFBaUIsYUFBYSwyQkFBMkIscUNBQXFDLEtBQUssdUJBQXVCLGlCQUFpQix5REFBeUQsZ0JBQWdCLGlCQUFpQixhQUFhLG1QQUFtUCx3QkFBd0IsSUFBSSxzQ0FBc0MsK0JBQStCLFFBQVEsOEhBQThILFdBQVcsaUJBQWlCLE1BQU0sZ0RBQWdELGlCQUFpQixVQUFVLFFBQVEsV0FBVyxhQUFhLDZCQUE2QixXQUFXLGNBQWMsNERBQTRELElBQUksNkpBQTZKLFNBQVMsc0JBQXNCLFNBQVMsK0JBQStCLEdBQUcsZUFBZSxvQkFBb0Isd0JBQXdCLHNCQUFzQixpRUFBaUUsbUJBQW1CLG1FQUFtRSxpREFBaUQsRUFBRSxlQUFlLHlDQUF5QyxlQUFlLG9CQUFvQixNQUFNLDREQUE0RCxzQkFBc0IsRUFBRSxFQUFFLGVBQWUsV0FBVywwRUFBMEUsZUFBZSxhQUFhLFVBQVUsa0JBQWtCLElBQUkscURBQXFELHNCQUFzQixPQUFPLFlBQVksSUFBSSw0QkFBNEIsU0FBUyxhQUFhLDBCQUEwQixTQUFTLFFBQVEsV0FBVyxPQUFPLGtCQUFrQiwyQ0FBMkMsSUFBSSwyQkFBMkIsU0FBUyxnQkFBZ0IsZUFBZSxtRkFBbUYsZ0NBQWdDLG1CQUFtQixtQkFBbUIscUtBQXFLLG1CQUFtQiw0QkFBNEIsZUFBZSxZQUFZLDBEQUEwRCxtQkFBbUIsa0NBQWtDLG9CQUFvQixVQUFVLDhFQUE4RSxtQkFBbUIsY0FBYyxpQ0FBaUMsK0JBQStCLG9CQUFvQixnQ0FBZ0MsbUNBQW1DLGtCQUFrQixjQUFjLGdCQUFnQix3REFBd0QsaUJBQWlCLG1CQUFtQixlQUFlLGlEQUFpRCwyQkFBMkIsSUFBSSxZQUFZLEVBQUUsNkJBQTZCLGtCQUFrQiw0Q0FBNEMsbUJBQW1CLCtCQUErQixFQUFFLEVBQUUsOEJBQThCLEVBQUUsaUJBQWlCLGFBQWEsMENBQTBDLHFCQUFxQixvQkFBb0IsMERBQTBELCtCQUErQixnQ0FBZ0MsU0FBUyxFQUFFLGlCQUFpQixnQ0FBZ0MsUUFBUSxFQUFFLEtBQUssRUFBRSxpQkFBaUIsYUFBYSxjQUFjLE1BQU0sOERBQThELGNBQWMsaUJBQWlCLGFBQWEsa0JBQWtCLHlDQUF5QyxrREFBa0QsV0FBVyxNQUFNLGlCQUFpQixhQUFhLGNBQWMsaUZBQWlGLGdCQUFnQixhQUFhLG9HQUFvRyxLQUFLLGNBQWMsOEVBQThFLFlBQVksYUFBYSxnR0FBZ0csS0FBSyxNQUFNLGlCQUFpQixhQUFhLHNDQUFzQyxTQUFTLEVBQUUsK0VBQStFLCtCQUErQixXQUFXLHNDQUFzQyxXQUFXLGtDQUFrQyxXQUFXLGdCQUFnQixlQUFlLDRCQUE0QixzRkFBc0YsVUFBVSxpQkFBaUIsb0NBQW9DLDhCQUE4QixLQUFLLG1EQUFtRCxhQUFhLEVBQUUsV0FBVyxZQUFZLE1BQU0sa0ZBQWtGLEtBQUssV0FBVywrQkFBK0IsVUFBVSxpQkFBaUIscUNBQXFDLHNCQUFzQixNQUFNLGtKQUFrSixpQkFBaUIsWUFBWSx3QkFBd0IscUJBQXFCLGlCQUFpQixhQUFhLHdDQUF3QywwQkFBMEIsd0NBQXdDLGFBQWEsU0FBUyx1QkFBdUIsU0FBUyxhQUFhLG9FQUFvRSx3QkFBd0IsYUFBYSxzQkFBc0IsSUFBSSxpQkFBaUIsdURBQXVELEtBQUssaUNBQWlDLDJCQUEyQixTQUFTLHlCQUF5QiwrREFBK0QsU0FBUyxrQkFBa0IsSUFBSSw4REFBOEQscUJBQXFCLG1CQUFtQiw4Q0FBOEMscUJBQXFCLGlCQUFpQix1QkFBdUIsMEJBQTBCLHNCQUFzQixzRkFBc0YsZUFBZSwwQkFBMEIsaUJBQWlCLGlCQUFpQiw4QkFBOEIsdUNBQXVDLGlEQUFpRCwyREFBMkQscUVBQXFFLHFCQUFxQixpQkFBaUIsaURBQWlELHNCQUFzQiw0Q0FBNEMsaUJBQWlCLFdBQVcsNEJBQTRCLElBQUksOEJBQThCLFNBQVMsZUFBZSxtQ0FBbUMsaUJBQWlCLGFBQWEsaUNBQWlDLG1DQUFtQyxZQUFZLDRCQUE0QixpQkFBaUIsWUFBWSxzQkFBc0IsaUJBQWlCLGFBQWEsaUlBQWlJLGFBQWEsa0NBQWtDLFNBQVMsd0JBQXdCLDBCQUEwQixVQUFVLDBDQUEwQyxzQkFBc0Isa0JBQWtCLHNCQUFzQixxSkFBcUosb0pBQW9KLG9CQUFvQixzREFBc0Qsb0RBQW9ELGtDQUFrQywyQkFBMkIsVUFBVSxpQkFBaUIsNEJBQTRCLElBQUksZUFBZSxvQkFBb0IsS0FBSyx5QkFBeUIsUUFBUSxFQUFFLFVBQVUsd0JBQXdCLG1CQUFtQixTQUFTLElBQUksbUJBQW1CLGtCQUFrQixPQUFPLFdBQVcsaUJBQWlCLFNBQVMsTUFBTSxVQUFVLFVBQVUsZUFBZSx3QkFBd0IsT0FBTyxtQkFBbUIsaUJBQWlCLG1IQUFtSCxxQkFBcUIsdUJBQXVCLFFBQVEsOEJBQThCLEVBQUUsRUFBRSxnQkFBZ0IsSUFBSSxJQUFJLFNBQVMsd0JBQXdCLHVCQUF1QixrQkFBa0IsZUFBZSxpRUFBaUUsd0JBQXdCLGFBQWEsV0FBVyxrQkFBa0IsYUFBYSxLQUFLLHVDQUF1QyxvQkFBb0IsaUJBQWlCLGVBQWUsYUFBYSxtQkFBbUIsT0FBTyxrQkFBa0IsaUNBQWlDLGlCQUFpQiwyQkFBMkIscURBQXFELEtBQUssZ0NBQWdDLElBQUksc0JBQXNCLFVBQVUsaUJBQWlCLGlEQUFpRCw0Q0FBNEMsZUFBZSxpQkFBaUIsMkRBQTJELDZDQUE2QywySUFBMkksZUFBZSxNQUFNLHNCQUFzQixlQUFlLHNCQUFzQixJQUFJLE9BQU8sWUFBWSxTQUFTLE9BQU8sWUFBWSxpQkFBaUIsV0FBVywwQkFBMEIsNkJBQTZCLFVBQVUsaUJBQWlCLGtDQUFrQyx3RUFBd0UsV0FBVywyQ0FBMkMsaUJBQWlCLElBQUksbUdBQW1HLFNBQVMsS0FBSyxxQkFBcUIsd0NBQXdDLEdBQUcsc0JBQXNCLGlCQUFpQixhQUFhLDRDQUE0QyxzQkFBc0IsV0FBVyxzQkFBc0IsK0JBQStCLGFBQWEsR0FBRyxlQUFlLDJEQUEyRCxpQkFBaUIsa0NBQWtDLHdCQUF3QixtQ0FBbUMsaUJBQWlCLHlCQUF5Qiw2QkFBNkIsaUJBQWlCLHVDQUF1Qyw4Q0FBOEMsb0RBQW9ELGlCQUFpQixhQUFhLHNCQUFzQix3Q0FBd0MsbUJBQW1CLCtCQUErQixFQUFFLGlCQUFpQixhQUFhLGlFQUFpRSxrQ0FBa0Msb0JBQW9CLDREQUE0RCxFQUFFLGlCQUFpQixXQUFXLGVBQWUsY0FBYyxFQUFFLGlCQUFpQixhQUFhLHNCQUFzQixxQ0FBcUMsZ0JBQWdCLCtCQUErQixFQUFFLGlCQUFpQixhQUFhLG1CQUFtQix3Q0FBd0MsbUJBQW1CLG1EQUFtRCxFQUFFLGlCQUFpQiw4Q0FBOEMsK0RBQStELG1CQUFtQix5Q0FBeUMsRUFBRSxpQkFBaUIseURBQXlELDBCQUEwQixFQUFFLGlCQUFpQixpQ0FBaUMsbUJBQW1CLGFBQWEsc0NBQXNDLDBEQUEwRCxJQUFJLEVBQUUsaUJBQWlCLGFBQWEsTUFBTSx1REFBdUQsd0NBQXdDLGdCQUFnQixzQkFBc0IscUJBQXFCLEVBQUUsZUFBZSxjQUFjLDRGQUE0RixtQ0FBbUMsb0JBQW9CLEVBQUUsaUJBQWlCLGFBQWEseUJBQXlCLGtCQUFrQixrQkFBa0IsRUFBRSxpQkFBaUIsNEdBQTRHLG1oQkFBbWhCLFlBQVksV0FBVyxLQUFLLDRDQUE0QyxnRkFBZ0YsZ0JBQWdCLGVBQWUsZ0NBQWdDLGVBQWUsb0JBQW9CLGdEQUFnRCx1Q0FBdUMsaUhBQWlILE1BQU0sb0JBQW9CLDBQQUEwUCwrQkFBK0IsK0NBQStDLDRDQUE0Qyx3QkFBd0Isc0NBQXNDLE9BQU8saUNBQWlDLGlCQUFpQixhQUFhLGlCQUFpQiw4Q0FBOEMsZ0JBQWdCLGlDQUFpQyxpR0FBaUcsUUFBUSxvQ0FBb0MsS0FBSyxrQkFBa0IsYUFBYSxrQkFBa0IsOEJBQThCLHNCQUFzQiw0SkFBNEosYUFBYSx1SkFBdUosYUFBYSwyTEFBMkwsb0JBQW9CLHdFQUF3RSxpQkFBaUIseUJBQXlCLHNDQUFzQyxzQkFBc0Isb0RBQW9ELElBQUksZ0JBQWdCLCtCQUErQixnQkFBZ0IscUJBQXFCLDJDQUEyQyw2QkFBNkIsYUFBYSxrR0FBa0csdUNBQXVDLHFDQUFxQyw2QkFBNkIscUNBQXFDLFlBQVksVUFBVSx1Q0FBdUMsbUJBQW1CLDJDQUEyQyxrQ0FBa0MsS0FBSyxvQkFBb0IseUVBQXlFLHNDQUFzQyx1QkFBdUIsd0NBQXdDLE1BQU0sZ0RBQWdELEdBQUcsMkZBQTJGLDRDQUE0QywrREFBK0QsY0FBYyw4RUFBOEUsNEJBQTRCLE9BQU8sNkJBQTZCLDJCQUEyQixhQUFhLGtFQUFrRSxxQ0FBcUMsMENBQTBDLHdFQUF3RSxxSEFBcUgsV0FBVyxlQUFlLEtBQUssa0JBQWtCLCtCQUErQixtQkFBbUIsZ0NBQWdDLGtCQUFrQixrQ0FBa0MsbUJBQW1CLHdFQUF3RSxlQUFlLHNCQUFzQixxRkFBcUYsc0NBQXNDLGFBQWEsK0VBQStFLHVDQUF1QyxhQUFhLHdLQUF3SyxhQUFhLDZGQUE2RiwwQ0FBMEMsR0FBRyxvREFBb0Qsc0NBQXNDLHNCQUFzQix3Q0FBd0MsMkRBQTJELHFCQUFxQix3REFBd0QsMkNBQTJDLHNCQUFzQix3Q0FBd0MseUhBQXlILE9BQU8sb0JBQW9CLFdBQVcsYUFBYSxnRUFBZ0UsK0RBQStELGlDQUFpQyxRQUFRLGNBQWMsS0FBSyx1Q0FBdUMscUJBQXFCLFVBQVUsd0RBQXdELDRGQUE0RixrQ0FBa0MsZ09BQWdPLGVBQWUseUNBQXlDLGtEQUFrRCxzRUFBc0Usb0lBQW9JLEtBQUssa0JBQWtCLGdDQUFnQyx3QkFBd0IsMENBQTBDLGtCQUFrQiwrREFBK0QseUJBQXlCLHlEQUF5RCxxRUFBcUUsNEdBQTRHLEtBQUssdUJBQXVCLDBDQUEwQywrQkFBK0IsdUJBQXVCLHNDQUFzQywrREFBK0QseUJBQXlCLGVBQWUsMkJBQTJCLGFBQWEsMExBQTBMLEVBQUUsWUFBWSxrQ0FBa0MsNEdBQTRHLGFBQWEsNEtBQTRLLEVBQUUsWUFBWSxrQ0FBa0MsMkZBQTJGLFNBQVMsNEJBQTRCLE1BQU0sR0FBRyxFOzs7Ozs7OztBQ0EvdDNDO0FBQUE7QUFBQTtBQUFBOztBQUVBQSxJQUFJQyxTQUFKLENBQWMsY0FBZCxFQUErQkMsbUJBQU9BLENBQUMsQ0FBUixDQUEvQjtBQUNBRixJQUFJQyxTQUFKLENBQWMsZUFBZCxFQUFnQ0MsbUJBQU9BLENBQUMsR0FBUixDQUFoQztBQUNBRixJQUFJQyxTQUFKLENBQWMsY0FBZCxFQUErQkMsbUJBQU9BLENBQUMsR0FBUixDQUEvQjtBQUNBRixJQUFJQyxTQUFKLENBQWMsV0FBZCxFQUE0QkMsbUJBQU9BLENBQUMsR0FBUixDQUE1Qjs7QUFFQSxJQUFJQyxPQUFPQyxPQUFPQyxPQUFQLENBQWVDLElBQTFCOztBQUVBLElBQUlDLE1BQU0sSUFBSVAsR0FBSixDQUFRO0FBQ2pCUSxLQUFHLFdBRGM7QUFFakJMLE9BQUs7QUFDSk0sWUFBUyxFQURMLEVBQ1E7QUFDWkMsa0JBQWUsRUFGWCxFQUVjO0FBQ2xCQyxlQUFZLENBQUMsUUFBRCxFQUFXLE9BQVgsQ0FIUjtBQUlKQyx1QkFBb0IsRUFKaEI7QUFLREMscUJBQW1CLEVBTGxCO0FBTURDLG9CQUFrQixLQU5qQjtBQU9EQyxZQUFTLEVBUFIsRUFPWTtBQUNiQyxjQUFXLEVBUlY7QUFTREMsb0JBQWlCLEVBVGhCLEVBU29CO0FBQ3JCQyxzQkFBbUIsRUFWbEI7QUFXREMsbUJBQWdCLEVBWGY7O0FBYUVDLFdBQVNqQixLQUFLa0IsY0FBTCxDQUFvQixDQUFwQixFQUF1QkQsT0FibEM7QUFjRUUsZUFBYW5CLEtBQUttQixXQWRwQjtBQWVFQyxrQkFBZ0IsQ0FBQztBQUNiQyxrQkFBYyxDQUREO0FBRWJDLGdCQUFZLENBRkM7QUFHYkMsa0JBQWM7QUFIRCxHQUFELENBZmxCOztBQXFCSjtBQUNBQyxjQUFZLElBQUlDLElBQUosQ0FBUztBQUNkQyxXQUFZLEVBREU7QUFFZEMsY0FBYSxFQUZDO0FBR2RDLGdCQUFlLEVBSEQ7QUFJZEMsbUJBQWlCLEVBSkg7QUFLZEMsU0FBWSxFQUxFLEVBS0U7QUFDaEJDLGNBQWEsRUFOQztBQU9kQyxTQUFVLEVBUEk7QUFRZEMsT0FBVyxFQVJHO0FBU2RDLGNBQWEsRUFUQztBQVVkQyxnQkFBZSxFQVZEO0FBV2RDLGtCQUFnQixFQVhGO0FBWWRDLG1CQUFpQjtBQVpILEdBQVQsRUFhUCxFQUFFQyxTQUFZLHVCQUFkLEVBYk87O0FBdEJSLEVBRlk7QUF5Q2pCQyxhQUFZO0FBQ1RDLHNFQUFXQTtBQURGLEVBekNLOztBQTZDakJDLFFBN0NpQixxQkE2Q1A7QUFDTkMsSUFBRSxhQUFGLEVBQWlCQyxFQUFqQixDQUFvQixlQUFwQixFQUFxQyxZQUFNO0FBQ3ZDO0FBQ0gsR0FGRDtBQUdBRCxJQUFFLHNCQUFGLEVBQTBCQyxFQUExQixDQUE2QixlQUE3QixFQUE4QyxZQUFNO0FBQ2hEO0FBQ0gsR0FGRDtBQUdILEVBcERnQjs7O0FBc0RqQkMsVUFBUztBQUNSQyx5QkFEUSxxQ0FDa0I7QUFDekJILEtBQUUsc0JBQUYsRUFBMEJJLEtBQTFCLENBQWdDLFFBQWhDO0FBQ0EsR0FITztBQUtSQyxXQUxRLHFCQUtFQyxDQUxGLEVBS0k7QUFBQTs7QUFDWCxPQUFHQSxLQUFLLEtBQVIsRUFBYztBQUNiLFNBQUt2QyxtQkFBTCxHQUEyQixhQUEzQjtBQUNBLFNBQUtDLGlCQUFMLEdBQXlCLEtBQXpCO0FBQ0EsU0FBS3VDLG9CQUFMO0FBQ0FQLE1BQUUsYUFBRixFQUFpQkksS0FBakIsQ0FBdUIsUUFBdkI7QUFDQSxJQUxELE1BS00sSUFBR0UsS0FBSyxNQUFSLEVBQWU7QUFDcEJFLFlBQVFDLEdBQVIsQ0FBWSxLQUFLdkMsUUFBakI7QUFDQSxRQUFJd0MsU0FBUSxFQUFaO0FBQ0EsUUFBSUMsV0FBVyxFQUFmO0FBQ0FDLFVBQU1DLEdBQU4sQ0FBVSxzQ0FBcUMsS0FBSzNDLFFBQXBELEVBQ0U0QyxJQURGLENBQ08sa0JBQVM7QUFDZE4sYUFBUUMsR0FBUixDQUFZTSxPQUFPekQsSUFBbkI7QUFDRSxXQUFLd0IsVUFBTCxDQUFnQkUsTUFBaEIsR0FBeUIrQixPQUFPekQsSUFBUCxDQUFZMEIsTUFBckM7QUFDQSxXQUFLRixVQUFMLENBQWdCRyxTQUFoQixHQUE0QjhCLE9BQU96RCxJQUFQLENBQVkyQixTQUF4QztBQUNBLFdBQUtILFVBQUwsQ0FBZ0JJLFdBQWhCLEdBQThCNkIsT0FBT3pELElBQVAsQ0FBWTRCLFdBQTFDO0FBQ0EsV0FBS0osVUFBTCxDQUFnQkssY0FBaEIsR0FBaUM0QixPQUFPekQsSUFBUCxDQUFZNkIsY0FBN0M7QUFDQSxXQUFLTCxVQUFMLENBQWdCTyxTQUFoQixHQUE0QjBCLE9BQU96RCxJQUFQLENBQVkrQixTQUF4QztBQUNBLFdBQUtQLFVBQUwsQ0FBZ0JRLElBQWhCLEdBQXVCMEIsV0FBV0QsT0FBT3pELElBQVAsQ0FBWWdDLElBQXZCLENBQXZCO0FBQ0EsV0FBS1IsVUFBTCxDQUFnQmEsY0FBaEIsR0FBa0NvQixPQUFPekQsSUFBUCxDQUFZcUMsY0FBYixHQUM5QnFCLFdBQVdELE9BQU96RCxJQUFQLENBQVlxQyxjQUF2QixDQUQ4QixHQUU5QixDQUZIO0FBR0EsV0FBS2IsVUFBTCxDQUFnQlMsRUFBaEIsR0FBcUJ3QixPQUFPekQsSUFBUCxDQUFZaUMsRUFBakM7O0FBRUMsU0FBR3dCLE9BQU96RCxJQUFQLENBQVkyRCxrQkFBZixFQUFrQztBQUNqQ0YsYUFBT3pELElBQVAsQ0FBWTJELGtCQUFaLENBQStCQyxHQUEvQixDQUFtQyxVQUFTQyxLQUFULEVBQWdCQyxHQUFoQixFQUFxQjtBQUMxRFYsY0FBT1csSUFBUCxDQUFZRixNQUFNLENBQU4sQ0FBWjtBQUNBLE9BRkU7QUFHQTtBQUNELFNBQUdKLE9BQU96RCxJQUFQLENBQVlnRSxvQkFBZixFQUFvQztBQUNuQ1AsYUFBT3pELElBQVAsQ0FBWWdFLG9CQUFaLENBQWlDSixHQUFqQyxDQUFxQyxVQUFTQyxLQUFULEVBQWdCQyxHQUFoQixFQUFxQjtBQUM1RFQsZ0JBQVNVLElBQVQsQ0FBY0YsTUFBTSxDQUFOLENBQWQ7QUFDQSxPQUZFO0FBR0M7QUFDRixXQUFLckMsVUFBTCxDQUFnQlcsV0FBaEIsR0FBOEJpQixNQUE5QixFQUNILE1BQUs1QixVQUFMLENBQWdCWSxhQUFoQixHQUFnQ2lCLFFBRDdCOztBQUdELFNBQUdJLE9BQU96RCxJQUFQLENBQVkrQixTQUFaLElBQXlCLENBQTVCLEVBQThCO0FBQzVCLFlBQUtQLFVBQUwsQ0FBZ0JNLElBQWhCLEdBQXVCLFFBQXZCO0FBQ0EsWUFBS25CLGdCQUFMLEdBQXdCLEtBQXhCO0FBQ0QsTUFIRCxNQUdLO0FBQ0gsWUFBS2EsVUFBTCxDQUFnQk0sSUFBaEIsR0FBdUIsT0FBdkI7QUFDQSxZQUFLbkIsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDRDtBQUNKLEtBbENEOztBQW9DQSxTQUFLRixtQkFBTCxHQUEyQixjQUEzQjtBQUNBLFNBQUtDLGlCQUFMLEdBQXlCLE1BQXpCO0FBQ0FnQyxNQUFFLGFBQUYsRUFBaUJJLEtBQWpCLENBQXVCLFFBQXZCO0FBQ0E7QUFDRCxHQXZETztBQXdEUm1CLGdCQXhEUSwwQkF3RE9oQyxFQXhEUCxFQXdEVTtBQUNqQixRQUFLckIsUUFBTCxHQUFnQnFCLEVBQWhCO0FBQ0EsR0ExRE87O0FBMkRSaUMsWUFBUyxrQkFBVUMsS0FBVixFQUFnQjtBQUFBOztBQUN4QixPQUFJQyxhQUFXLEVBQWY7QUFDQSxPQUFJQyxlQUFhLEVBQWpCO0FBQ0EsUUFBSzdDLFVBQUwsQ0FBZ0JXLFdBQWhCLENBQTRCeUIsR0FBNUIsQ0FBZ0MsVUFBU1UsQ0FBVCxFQUFZO0FBQ3hDLFFBQUdGLGNBQWMsRUFBakIsRUFBb0I7QUFDbkJBLGtCQUFhRSxFQUFFckMsRUFBZjtBQUNBLEtBRkQsTUFFSztBQUNKbUMsbUJBQWMsTUFBTUUsRUFBRXJDLEVBQXRCO0FBQ0E7QUFDSixJQU5EOztBQVFBLFFBQUtULFVBQUwsQ0FBZ0JZLGFBQWhCLENBQThCd0IsR0FBOUIsQ0FBa0MsVUFBU1UsQ0FBVCxFQUFZO0FBQzFDLFFBQUdELGdCQUFnQixFQUFuQixFQUFzQjtBQUNyQkEsb0JBQWVDLEVBQUVyQyxFQUFqQjtBQUNBLEtBRkQsTUFFSztBQUNKb0MscUJBQWdCLE1BQU1DLEVBQUVyQyxFQUF4QjtBQUNBO0FBQ0osSUFORDs7QUFRQSxRQUFLVCxVQUFMLENBQWdCVyxXQUFoQixHQUE4QmlDLFVBQTlCO0FBQ0EsUUFBSzVDLFVBQUwsQ0FBZ0JZLGFBQWhCLEdBQWdDaUMsWUFBaEM7QUFDQSxPQUFHLEtBQUszRCxpQkFBTCxJQUEwQixLQUE3QixFQUFtQztBQUNsQztBQUNBLFFBQUcsS0FBS2MsVUFBTCxDQUFnQk0sSUFBaEIsSUFBd0IsUUFBM0IsRUFBb0M7QUFDbkMsVUFBS04sVUFBTCxDQUFnQk8sU0FBaEIsR0FBNEIsQ0FBNUI7QUFDQSxVQUFLUCxVQUFMLENBQWdCUSxJQUFoQixHQUF1QixDQUF2QjtBQUNBO0FBQ0QsU0FBS1IsVUFBTCxDQUFnQlUsU0FBaEIsR0FBNEIsQ0FBNUI7QUFDQTtBQUNBLFNBQUtWLFVBQUwsQ0FBZ0IrQyxNQUFoQixDQUF1QixNQUF2QixFQUErQixPQUEvQixFQUNJZixJQURKLENBQ1Msa0JBQVU7QUFDZixZQUFLUCxvQkFBTDtBQUNHdUIsWUFBT0MsT0FBUCxDQUFlLG9CQUFmO0FBQ0gsS0FKSixFQUtJQyxLQUxKLENBS1UsaUJBQVM7QUFDWkYsWUFBT0csS0FBUCxDQUFhLGdCQUFiO0FBQ0gsS0FQSjtBQVFHakMsTUFBRSxhQUFGLEVBQWlCSSxLQUFqQixDQUF1QixRQUF2QjtBQUNILElBakJELE1BaUJNLElBQUksS0FBS3BDLGlCQUFMLElBQTBCLE1BQTlCLEVBQXFDO0FBQzFDLFFBQUcsS0FBS2MsVUFBTCxDQUFnQk0sSUFBaEIsSUFBd0IsUUFBM0IsRUFBb0M7QUFDbkMsVUFBS04sVUFBTCxDQUFnQk8sU0FBaEIsR0FBNEIsQ0FBNUI7QUFDQSxVQUFLUCxVQUFMLENBQWdCUSxJQUFoQixHQUF1QixDQUF2QjtBQUNBLEtBSEQsTUFHTSxJQUFHLEtBQUtSLFVBQUwsQ0FBZ0JNLElBQWhCLElBQXdCLE9BQTNCLEVBQW1DO0FBQ3hDLFNBQUcsS0FBS04sVUFBTCxDQUFnQk8sU0FBaEIsSUFBNkIsR0FBaEMsRUFBb0M7QUFDbkMsV0FBS1AsVUFBTCxDQUFnQk8sU0FBaEIsR0FBNEIsRUFBNUI7QUFDQTtBQUNELFNBQUcsS0FBS1AsVUFBTCxDQUFnQlEsSUFBaEIsSUFBd0IsTUFBM0IsRUFBa0M7QUFDakMsV0FBS1IsVUFBTCxDQUFnQlEsSUFBaEIsR0FBdUIsRUFBdkI7QUFDQTtBQUNEO0FBQ0QsU0FBS1IsVUFBTCxDQUFnQitDLE1BQWhCLENBQXVCLE1BQXZCLEVBQStCLE9BQS9CLEVBQ0NmLElBREQsQ0FDTSxrQkFBVTtBQUNaLFlBQUtQLG9CQUFMO0FBQ0d1QixZQUFPQyxPQUFQLENBQWUsa0JBQWY7QUFDSCxLQUpKLEVBS0lDLEtBTEosQ0FLVSxpQkFBUztBQUNaRixZQUFPRyxLQUFQLENBQWEsZ0JBQWI7QUFDSCxLQVBKO0FBUUdqQyxNQUFFLGFBQUYsRUFBaUJJLEtBQWpCLENBQXVCLFFBQXZCO0FBQ0g7QUFFRCxHQXhITzs7QUEySFI7QUFDQThCLG1CQTVIUSwrQkE0SFc7QUFDbEIsT0FBSTlDLE9BQU8sS0FBS04sVUFBTCxDQUFnQk0sSUFBM0I7QUFDQSxPQUFHQSxRQUFRLFFBQVgsRUFBb0I7QUFDbkIsU0FBS25CLGdCQUFMLEdBQXdCLEtBQXhCO0FBQ0EsSUFGRCxNQUVNLElBQUltQixRQUFRLE9BQVosRUFBb0I7QUFDekIsU0FBS25CLGdCQUFMLEdBQXdCLElBQXhCO0FBQ0E7QUFDRCxHQW5JTzs7O0FBcUlSO0FBQ0FrRSxXQXRJUSx1QkFzSUc7QUFDVixRQUFLakUsUUFBTCxHQUFnQixFQUFoQjtBQUNBLFFBQUtELGdCQUFMLEdBQXdCLEtBQXhCO0FBQ0EsUUFBS0ksa0JBQUwsR0FBd0IsRUFBeEI7QUFDRyxRQUFLQyxlQUFMLEdBQXFCLEVBQXJCO0FBQ0gsUUFBS1EsVUFBTCxHQUFrQixJQUFJQyxJQUFKLENBQVM7QUFDcEJDLFlBQVksRUFEUTtBQUVwQkMsZUFBYSxFQUZPO0FBR3BCQyxpQkFBZSxFQUhLO0FBSXBCQyxvQkFBaUIsRUFKRztBQUtwQkMsVUFBWSxFQUxRLEVBS0o7QUFDaEJDLGVBQWEsRUFOTztBQU9wQkMsVUFBVSxFQVBVO0FBUXBCQyxRQUFXLEVBUlM7QUFTcEJDLGVBQWEsRUFUTztBQVVwQkMsaUJBQWUsRUFWSztBQVdwQkMsbUJBQWdCLEVBWEk7QUFZcEJDLG9CQUFpQjtBQVpHLElBQVQsRUFhYixFQUFFQyxTQUFZLHVCQUFkLEVBYmEsQ0FBbEI7QUFnQkEsR0EzSk87QUE2SlJmLGVBN0pRLDJCQTZKTztBQUFBOztBQUNkLFFBQUt1RCxhQUFMO0FBQ0EsT0FBSSxLQUFLakUsVUFBTCxJQUFtQixDQUF2QixFQUF5QjtBQUN4QnlDLFVBQU1DLEdBQU4sQ0FBVSxrQ0FBa0MsS0FBSzNDLFFBQWpELEVBQ080QyxJQURQLENBQ1ksa0JBQVU7QUFDZixZQUFLUCxvQkFBTDtBQUNIdUIsWUFBT0MsT0FBUCxDQUFlLGtCQUFmO0FBQ0EsS0FKSixFQUtJQyxLQUxKLENBS1UsaUJBQVM7QUFDZixZQUFLekIsb0JBQUw7QUFDR3VCLFlBQU9HLEtBQVAsQ0FBYSxnQkFBYjtBQUNILEtBUko7QUFTQSxJQVZELE1BVUs7QUFDSEgsV0FBT0csS0FBUCxDQUFhLDZDQUFiO0FBQ0Q7QUFDRCxHQTVLTztBQThLUkksYUE5S1EseUJBOEtLO0FBQUE7O0FBQ1p6QixTQUFNQyxHQUFOLENBQVUsNEJBQVYsRUFDT0MsSUFEUCxDQUNZLGtCQUFVO0FBQ2QsV0FBS2xELFFBQUwsR0FBZ0JtRCxPQUFPekQsSUFBdkI7QUFDSixJQUhKO0FBSUEsR0FuTE87QUFxTFJnRixhQXJMUSx5QkFxTEs7QUFBQTs7QUFDWjFCLFNBQU1DLEdBQU4sQ0FBVSxtQ0FBVixFQUNPQyxJQURQLENBQ1ksa0JBQVU7QUFDZCxXQUFLakQsY0FBTCxHQUFzQmtELE9BQU96RCxJQUE3QjtBQUNOLFdBQUtpRixjQUFMO0FBQ0UsSUFKSjtBQUtBLEdBM0xPO0FBNkxSQyxVQTdMUSxzQkE2TEU7QUFBQTs7QUFDVDVCLFNBQU1DLEdBQU4sQ0FBVSx5Q0FBVixFQUNPQyxJQURQLENBQ1ksa0JBQVU7QUFDZCxXQUFLMUMsZ0JBQUwsR0FBd0IyQyxPQUFPekQsSUFBL0I7QUFDSixJQUhKO0FBSUEsR0FsTU87QUFvTVJpRCxzQkFwTVEsa0NBb01jO0FBQ3JCLFFBQUs0QixTQUFMO0FBQ00sUUFBS0UsV0FBTDtBQUNBLFFBQUtDLFdBQUw7QUFDTixHQXhNTztBQTBNUkcsaUJBMU1RLDZCQTBNUztBQUNoQkMsY0FBVyxZQUFVO0FBQ3BCMUMsTUFBRSxRQUFGLEVBQVkyQyxTQUFaLENBQXNCO0FBQ1pDLGlCQUFZLEVBREE7QUFFWkMsaUJBQVksSUFGQTtBQUdaQyxVQUFLO0FBSE8sS0FBdEI7QUFLQSxJQU5ELEVBTUUsR0FORjtBQU9BLEdBbE5PO0FBb05SVixlQXBOUSx5QkFvTk03QyxFQXBOTixFQW9OUztBQUFBOztBQUNoQnFCLFNBQU1DLEdBQU4sQ0FBVSx3Q0FBd0N0QixFQUFsRCxFQUNPdUIsSUFEUCxDQUNZLGtCQUFVO0FBQ2YsV0FBSzNDLFVBQUwsR0FBa0I0QyxPQUFPekQsSUFBUCxDQUFZeUYsTUFBOUI7QUFDTixJQUhEO0FBSUEsR0F6Tk87QUEyTlJSLGdCQTNOUSw0QkEyTlE7QUFDZnZDLEtBQUUsUUFBRixFQUFZMkMsU0FBWixHQUF3QkssT0FBeEI7O0FBRUEsT0FBSUMsS0FBSyxJQUFUOztBQUVBUCxjQUFXLFlBQVU7O0FBRXBCLFFBQUlRLFFBQVFsRCxFQUFFLFFBQUYsRUFBWTJDLFNBQVosQ0FBc0I7QUFDeEJDLGlCQUFZLEVBRFk7QUFFeEJDLGlCQUFZLElBRlk7QUFHeEJDLFVBQUssaUNBSG1CO0FBSXhCSyxZQUFPLENBQUMsQ0FBRSxDQUFGLEVBQUssS0FBTCxDQUFELENBSmlCO0FBS3hCQyxjQUFTLE9BTGU7QUFNeEJDLGdCQUFXO0FBTmEsS0FBdEIsQ0FBWjs7QUFTTXJELE1BQUUsY0FBRixFQUFrQkMsRUFBbEIsQ0FBcUIsT0FBckIsRUFBOEIsYUFBOUIsRUFBNkMsWUFBWTtBQUN4RCxTQUFJcUQsTUFBTXRELEVBQUUsSUFBRixFQUFRdUQsSUFBUixDQUFhLElBQWIsQ0FBVjtBQUNBLFNBQUlDLEtBQUt4RCxFQUFFLElBQUYsRUFBUXlELE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBVDtBQUNBLFNBQUlDLE1BQU1SLE1BQU1RLEdBQU4sQ0FBV0YsRUFBWCxDQUFWOztBQUVBLFNBQUdFLElBQUlDLEtBQUosQ0FBVUMsT0FBVixFQUFILEVBQXdCO0FBQ3BCRixVQUFJQyxLQUFKLENBQVVFLElBQVY7QUFDQUwsU0FBR00sV0FBSCxDQUFlLE9BQWY7QUFDSCxNQUhELE1BR087QUFDWixVQUFJdkUsS0FBSytELElBQUlTLEtBQUosQ0FBVSxHQUFWLENBQVQ7QUFDTW5ELFlBQU1DLEdBQU4sQ0FBVSx3Q0FBdUN0QixHQUFHLENBQUgsQ0FBakQsRUFDQ3VCLElBREQsQ0FDTSxrQkFBVTtBQUNmLFdBQUlrRCxTQUFTLGFBQVd6RSxHQUFHLENBQUgsQ0FBeEI7QUFDTlMsU0FBRWdFLE1BQUYsRUFBVVAsT0FBVixDQUFrQixJQUFsQixFQUF3QkEsT0FBeEIsQ0FBZ0MsSUFBaEMsRUFBc0NRLE1BQXRDO0FBQ01qRSxTQUFFZ0UsTUFBRixFQUFVUCxPQUFWLENBQWtCLElBQWxCLEVBQXdCUSxNQUF4Qjs7QUFFQWpFLFNBQUVnRSxNQUFGLEVBQVVDLE1BQVY7QUFDQWpFLFNBQUVnRSxTQUFPLEtBQVQsRUFBZ0JDLE1BQWhCO0FBQ0dQLFdBQUlDLEtBQUosQ0FBVVYsR0FBR2lCLFNBQUgsQ0FBYW5ELE9BQU96RCxJQUFwQixFQUF5QmlDLEdBQUcsQ0FBSCxDQUF6QixDQUFWLEVBQTJDNEUsSUFBM0M7QUFDSCxPQVREO0FBVUFYLFNBQUdZLFFBQUgsQ0FBWSxPQUFaO0FBQ0E7QUFDSixLQXRCRTtBQXdCTCxJQW5DRixFQW1DRyxJQW5DSDtBQW9DQSxHQXBRTztBQXNRUkYsV0F0UVEscUJBc1FFNUQsQ0F0UUYsRUFzUUkwRCxNQXRRSixFQXNRWTtBQUNuQixPQUFHMUQsRUFBRXlDLE1BQUYsR0FBVyxDQUFkLEVBQWdCO0FBQ2hCLFFBQUlzQixNQUFPLGdGQUE4RUwsTUFBOUUsR0FBcUYsSUFBaEc7QUFDQSxTQUFJLElBQUlNLElBQUcsQ0FBWCxFQUFjQSxJQUFFaEUsRUFBRXlDLE1BQWxCLEVBQXlCdUIsR0FBekIsRUFBNkI7QUFDNUIsU0FBSUMsTUFBSyxVQUFUO0FBQ1lBLFlBQU8sNERBQTBEakUsRUFBRWdFLENBQUYsRUFBSy9FLEVBQS9ELEdBQWtFLCtDQUF6RTtBQUNBZ0YsWUFBTyxvQ0FBUDtBQUNBQSxZQUFPLE1BQVA7QUFDQUEsWUFBTyw0REFBMERqRSxFQUFFZ0UsQ0FBRixFQUFLL0UsRUFBL0QsR0FBa0UsbUJBQXpFO0FBQ0FnRixZQUFPLG1DQUFQO0FBQ0FBLFlBQU8sTUFBUDtBQUNIQSxZQUFNLFdBQU47QUFDVEYsWUFBTSxTQUNHLDJEQURILEdBQytEL0QsRUFBRWdFLENBQUYsRUFBS3RGLE1BRHBFLEdBQzJFLFdBRDNFLEdBRUcsTUFGSCxHQUVVc0IsRUFBRWdFLENBQUYsRUFBS3BGLFdBRmYsR0FFMkIsT0FGM0IsR0FHRyxNQUhILEdBR1VvQixFQUFFZ0UsQ0FBRixFQUFLaEYsSUFIZixHQUdvQixPQUhwQixHQUlHLE1BSkgsR0FJVWlGLEdBSlYsR0FJYyxPQUpkLEdBS0EsT0FMTjtBQU1NO0FBQ0RGLFdBQUssVUFBTDtBQUNOLFdBQU9BLEdBQVA7QUFDQztBQUNEO0FBNVJPLEVBdERRO0FBb1ZqQkcsUUFwVmlCLHFCQW9WUjs7QUFFRixNQUFJQyxJQUFJLEtBQUtoRyxXQUFMLENBQWlCaUcsTUFBakIsQ0FBd0IsZUFBTztBQUFFLFVBQU9DLElBQUl2RixJQUFKLEtBQWEsaUJBQXBCO0FBQXNDLEdBQXZFLENBQVI7O0FBRUEsTUFBSXdGLElBQUlILEVBQUVDLE1BQUYsQ0FBUyxlQUFPO0FBQUUsVUFBT0MsSUFBSUUsSUFBSixLQUFhLGlCQUFwQjtBQUFzQyxHQUF4RCxDQUFSOztBQUVBLE1BQUdELEVBQUU3QixNQUFGLElBQVUsQ0FBYixFQUFnQixLQUFLckUsY0FBTCxDQUFvQkMsYUFBcEIsR0FBb0MsQ0FBcEMsQ0FBaEIsS0FDSyxLQUFLRCxjQUFMLENBQW9CQyxhQUFwQixHQUFvQyxDQUFwQzs7QUFFTCxNQUFJaUcsSUFBSUgsRUFBRUMsTUFBRixDQUFTLGVBQU87QUFBRSxVQUFPQyxJQUFJRSxJQUFKLEtBQWEsY0FBcEI7QUFBbUMsR0FBckQsQ0FBUjs7QUFFQSxNQUFHRCxFQUFFN0IsTUFBRixJQUFVLENBQWIsRUFBZ0IsS0FBS3JFLGNBQUwsQ0FBb0JFLFdBQXBCLEdBQWtDLENBQWxDLENBQWhCLEtBQ0ssS0FBS0YsY0FBTCxDQUFvQkUsV0FBcEIsR0FBa0MsQ0FBbEM7O0FBRUwsTUFBSWdHLElBQUlILEVBQUVDLE1BQUYsQ0FBUyxlQUFPO0FBQUUsVUFBT0MsSUFBSUUsSUFBSixLQUFhLGdCQUFwQjtBQUFxQyxHQUF2RCxDQUFSOztBQUVBLE1BQUdELEVBQUU3QixNQUFGLElBQVUsQ0FBYixFQUFnQixLQUFLckUsY0FBTCxDQUFvQkcsYUFBcEIsR0FBb0MsQ0FBcEMsQ0FBaEIsS0FDSyxLQUFLSCxjQUFMLENBQW9CRyxhQUFwQixHQUFvQyxDQUFwQzs7QUFFWCxPQUFLd0QsV0FBTDtBQUNBLE9BQUtDLFdBQUw7QUFDQSxPQUFLRSxRQUFMO0FBQ0EsT0FBS0QsY0FBTDtBQUNBO0FBM1dnQixDQUFSLENBQVY7O0FBd1hBLElBQUl1QyxPQUFPLElBQUkzSCxHQUFKLENBQVE7QUFDbEJRLEtBQUcsWUFEZTtBQUVsQkwsT0FBSztBQUNKeUgsYUFBVSxFQUROO0FBRUpDLE9BQUksRUFGQTtBQUdKQyxVQUFPLEVBSEg7O0FBS0UxRyxXQUFTakIsS0FBS2tCLGNBQUwsQ0FBb0IsQ0FBcEIsRUFBdUJELE9BTGxDO0FBTUVFLGVBQWFuQixLQUFLbUIsV0FOcEI7QUFPRUMsa0JBQWdCLENBQUM7QUFDYndHLG1CQUFlLENBREY7QUFFYkMsaUJBQWEsQ0FGQTtBQUdiQyxtQkFBZTtBQUhGLEdBQUQsQ0FQbEI7O0FBYUpDLFdBQVMsSUFBSXRHLElBQUosQ0FBUztBQUNYUSxPQUFPLEVBREk7QUFFZCtGLFVBQVMsRUFGSztBQUdkQyxhQUFVO0FBSEksR0FBVCxFQUlKLEVBQUUzRixTQUFVLHlDQUFaLEVBSkk7QUFiTCxFQUZhO0FBcUJsQk0sVUFBUTtBQUNQc0MsVUFETyxzQkFDRztBQUFBOztBQUNUNUIsU0FBTUMsR0FBTixDQUFVLHlDQUFWLEVBQ09DLElBRFAsQ0FDWSxrQkFBVTtBQUNkLFdBQUtpRSxTQUFMLEdBQWlCaEUsT0FBT3pELElBQXhCO0FBQ0osSUFISjtBQUlBLEdBTk07QUFPUG1GLGlCQVBPLDZCQU9VO0FBQ2hCQyxjQUFXLFlBQVU7QUFDcEIxQyxNQUFFLFFBQUYsRUFBWTJDLFNBQVosQ0FBc0I7QUFDWkUsaUJBQVk7QUFEQSxLQUF0QjtBQUdBLElBSkQsRUFJRSxHQUpGO0FBS0EsR0FiTTtBQWNQMkMsVUFkTyxvQkFjRTVELENBZEYsRUFjSTtBQUFBOztBQUNQLE9BQUdBLEtBQUssS0FBUixFQUFjO0FBQ2hCLFNBQUt5RCxPQUFMLENBQWF4RCxNQUFiLENBQW9CLE1BQXBCLEVBQTRCLE1BQTVCLEVBQ0lmLElBREosQ0FDUyxrQkFBVTtBQUNaZ0IsWUFBT0MsT0FBUCxDQUFlLDZCQUFmO0FBQ0EvQixPQUFFLG9CQUFGLEVBQXdCSSxLQUF4QixDQUE4QixRQUE5QjtBQUNBLFlBQUtxRixTQUFMO0FBQ0gsS0FMSixFQU1JekQsS0FOSixDQU1VLGlCQUFTO0FBQ1pGLFlBQU9HLEtBQVAsQ0FBYSxnQkFBYjtBQUNILEtBUko7QUFTQSxJQVZFLE1BVUcsSUFBSUwsS0FBSyxNQUFULEVBQWdCO0FBQ3JCLFNBQUt5RCxPQUFMLENBQWF4RCxNQUFiLENBQW9CLE1BQXBCLEVBQTRCLE9BQTVCLEVBQ0lmLElBREosQ0FDUyxrQkFBVTtBQUNaZ0IsWUFBT0MsT0FBUCxDQUFlLDBCQUFmO0FBQ0EvQixPQUFFLG9CQUFGLEVBQXdCSSxLQUF4QixDQUE4QixRQUE5QjtBQUNBLFlBQUtxRixTQUFMO0FBQ0gsS0FMSixFQU1JekQsS0FOSixDQU1VLGlCQUFTO0FBQ1pGLFlBQU9HLEtBQVAsQ0FBYSxnQkFBYjtBQUNILEtBUko7QUFTQSxJQVZLLE1BVUEsSUFBSUwsS0FBSyxRQUFULEVBQWtCO0FBQ3ZCaEIsVUFBTThFLElBQU4sQ0FBVyxvREFBbUQsS0FBS1QsTUFBbkUsRUFDSW5FLElBREosQ0FDUyxrQkFBVTtBQUNaZ0IsWUFBT0MsT0FBUCxDQUFlLDBCQUFmO0FBQ0EsWUFBSzBELFNBQUw7QUFDSCxLQUpKLEVBS0l6RCxLQUxKLENBS1UsaUJBQVM7QUFDWkYsWUFBT0csS0FBUCxDQUFhLGdCQUFiO0FBQ0gsS0FQSjtBQVFBO0FBQ0QsR0E3Q007QUErQ1AwRCxXQS9DTyxxQkErQ0cvRCxDQS9DSCxFQStDTXJDLEVBL0NOLEVBK0NTO0FBQUE7O0FBQ2YsT0FBR3FDLEtBQUssS0FBUixFQUFjO0FBQ2I1QixNQUFFLG9CQUFGLEVBQXdCSSxLQUF4QixDQUE4QixRQUE5QjtBQUNBLFNBQUs0RSxHQUFMLEdBQVcsS0FBWDtBQUNBeEUsWUFBUUMsR0FBUixDQUFZbEIsRUFBWjtBQUNBaUIsWUFBUUMsR0FBUixDQUFZbUIsQ0FBWjtBQUNBLElBTEQsTUFLTSxJQUFJQSxLQUFLLE1BQVQsRUFBZ0I7QUFDckIsU0FBS29ELEdBQUwsR0FBVyxNQUFYO0FBQ0FwRSxVQUFNQyxHQUFOLENBQVUsa0RBQWlEdEIsRUFBM0QsRUFDRXVCLElBREYsQ0FDTyxrQkFBUztBQUNmLGFBQUt1RSxPQUFMLENBQWE5RixFQUFiLEdBQWtCd0IsT0FBT3pELElBQVAsQ0FBWWlDLEVBQTlCO0FBQ0EsYUFBSzhGLE9BQUwsQ0FBYUMsS0FBYixHQUFxQnZFLE9BQU96RCxJQUFQLENBQVlnSSxLQUFqQztBQUNBLGFBQUtELE9BQUwsQ0FBYUUsUUFBYixHQUF3QnhFLE9BQU96RCxJQUFQLENBQVlpSSxRQUFwQztBQUNBLEtBTEQ7QUFNQXZGLE1BQUUsb0JBQUYsRUFBd0JJLEtBQXhCLENBQThCLFFBQTlCO0FBQ0EsSUFUSyxNQVNBLElBQUl3QixLQUFLLFFBQVQsRUFBa0I7QUFDdkIsU0FBS29ELEdBQUwsR0FBVyxRQUFYO0FBQ0EsU0FBS0MsTUFBTCxHQUFjMUYsRUFBZDtBQUNBUyxNQUFFLHVCQUFGLEVBQTJCSSxLQUEzQixDQUFpQyxRQUFqQztBQUNBO0FBQ0QsR0FuRU07QUFxRVB3RixhQXJFTyx5QkFxRU07QUFDWixRQUFLUCxPQUFMLEdBQWUsSUFBSXRHLElBQUosQ0FBUztBQUN2QlEsUUFBTyxFQURnQjtBQUVqQitGLFdBQVMsRUFGUTtBQUdqQkMsY0FBVTtBQUhPLElBQVQsRUFJVixFQUFFM0YsU0FBVSx5Q0FBWixFQUpVLENBQWY7QUFLQSxHQTNFTTtBQTRFUDZGLFdBNUVPLHVCQTRFSTtBQUNWekYsS0FBRSxRQUFGLEVBQVkyQyxTQUFaLEdBQXdCSyxPQUF4QjtBQUNBLFFBQUs0QyxXQUFMO0FBQ0EsUUFBS3BELFFBQUw7QUFDQSxRQUFLQyxlQUFMO0FBQ0E7QUFqRk0sRUFyQlU7QUF3R2xCK0IsUUF4R2tCLHFCQXdHVDs7QUFFRixNQUFJQyxJQUFJLEtBQUtoRyxXQUFMLENBQWlCaUcsTUFBakIsQ0FBd0IsZUFBTztBQUFFLFVBQU9DLElBQUl2RixJQUFKLEtBQWEsaUJBQXBCO0FBQXNDLEdBQXZFLENBQVI7O0FBRUEsTUFBSXdGLElBQUlILEVBQUVDLE1BQUYsQ0FBUyxlQUFPO0FBQUUsVUFBT0MsSUFBSUUsSUFBSixLQUFhLGtCQUFwQjtBQUF1QyxHQUF6RCxDQUFSOztBQUVBLE1BQUdELEVBQUU3QixNQUFGLElBQVUsQ0FBYixFQUFnQixLQUFLckUsY0FBTCxDQUFvQndHLGNBQXBCLEdBQXFDLENBQXJDLENBQWhCLEtBQ0ssS0FBS3hHLGNBQUwsQ0FBb0J3RyxjQUFwQixHQUFxQyxDQUFyQzs7QUFFTCxNQUFJTixJQUFJSCxFQUFFQyxNQUFGLENBQVMsZUFBTztBQUFFLFVBQU9DLElBQUlFLElBQUosS0FBYSxlQUFwQjtBQUFvQyxHQUF0RCxDQUFSOztBQUVBLE1BQUdELEVBQUU3QixNQUFGLElBQVUsQ0FBYixFQUFnQixLQUFLckUsY0FBTCxDQUFvQnlHLFlBQXBCLEdBQW1DLENBQW5DLENBQWhCLEtBQ0ssS0FBS3pHLGNBQUwsQ0FBb0J5RyxZQUFwQixHQUFtQyxDQUFuQzs7QUFFTCxNQUFJUCxJQUFJSCxFQUFFQyxNQUFGLENBQVMsZUFBTztBQUFFLFVBQU9DLElBQUlFLElBQUosS0FBYSxpQkFBcEI7QUFBc0MsR0FBeEQsQ0FBUjs7QUFFQSxNQUFHRCxFQUFFN0IsTUFBRixJQUFVLENBQWIsRUFBZ0IsS0FBS3JFLGNBQUwsQ0FBb0IwRyxjQUFwQixHQUFxQyxDQUFyQyxDQUFoQixLQUNLLEtBQUsxRyxjQUFMLENBQW9CMEcsY0FBcEIsR0FBcUMsQ0FBckM7O0FBRVgsT0FBSzVDLFFBQUw7QUFDQSxPQUFLQyxlQUFMO0FBRUE7QUE5SGlCLENBQVIsQ0FBWDs7QUFrSUF6QyxFQUFFNkYsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVc7QUFDNUI7QUFDQTlGLEdBQUU2RixRQUFGLEVBQVk1RixFQUFaLENBQWUsT0FBZixFQUF1QixXQUF2QixFQUFtQyxZQUFXO0FBQzdDLE1BQUlWLEtBQUtTLEVBQUUsSUFBRixFQUFRdUQsSUFBUixDQUFhLElBQWIsQ0FBVDtBQUNBLE1BQUloRSxLQUFLQSxHQUFHd0UsS0FBSCxDQUFTLEdBQVQsQ0FBVDtBQUNNckcsTUFBSTZELGNBQUosQ0FBbUJoQyxHQUFHLENBQUgsQ0FBbkI7QUFDQTdCLE1BQUkyQyxTQUFKLENBQWMsTUFBZDtBQUNILEVBTEo7O0FBT0E7QUFDR0wsR0FBRTZGLFFBQUYsRUFBWTVGLEVBQVosQ0FBZSxPQUFmLEVBQXVCLGFBQXZCLEVBQXFDLFlBQVc7QUFDbEQsTUFBSVYsS0FBS1MsRUFBRSxJQUFGLEVBQVF1RCxJQUFSLENBQWEsSUFBYixDQUFUO0FBQ0EsTUFBSWhFLEtBQUtBLEdBQUd3RSxLQUFILENBQVMsR0FBVCxDQUFUO0FBQ01yRyxNQUFJNkQsY0FBSixDQUFtQmhDLEdBQUcsQ0FBSCxDQUFuQjtBQUNBN0IsTUFBSXlDLHVCQUFKO0FBQ0gsRUFMRDtBQU1ILENBaEJELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDemVBO0FBQ0EsbUJBREE7QUFFQTtBQUNBLFVBREEsY0FDQSxFQURBLEVBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FKQTtBQUtBLFVBTEEsY0FLQSxFQUxBLEVBS0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUZBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1JBO0FBQ0E7QUFEQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2lCQTtBQUNBO0FBQ0EsbUJBREE7QUFFQTtBQUNBLFlBREEsZ0JBQ0EsRUFEQSxFQUNBO0FBQ0E7QUFDQTtBQUNBLFNBSkE7QUFLQSxXQUxBLGVBS0EsRUFMQSxFQUtBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFGQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNYQTtBQUNBO0FBQ0EsbUJBREE7QUFFQTtBQUNBO0FBREEsS0FGQTtBQUtBO0FBQ0EsWUFEQSxnQkFDQSxFQURBLEVBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FKQTtBQUtBLFdBTEEsZUFLQSxFQUxBLEVBS0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUxBLEc7Ozs7Ozs7QUMxQkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBd0U7QUFDaEc7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBNk87QUFDdlA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBNE07QUFDdE47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBTyxDQUFDLENBQXdFO0FBQ2hHO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTZPO0FBQ3ZQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTRNO0FBQ3ROO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQU8sQ0FBQyxDQUF3RTtBQUNoRztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUF5TztBQUNuUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUF3TTtBQUNsTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDN0RBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN2REEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBQ0EsZ0NBREE7QUFFQTtBQUZBO0FBREEsRzs7Ozs7OztBQ3ZCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7Ozs7OztBQ3RFQSxnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxDQUF5TztBQUNuUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxDQUFxTTtBQUMvTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3Zpc2Evc2VydmljZXMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ4OCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjMiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NoaWxkU2VydmljZXMudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi01Y2E5MWVlZVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9DaGlsZFNlcnZpY2VzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL0NoaWxkU2VydmljZXMudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQ2hpbGRTZXJ2aWNlcy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNWNhOTFlZWVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi01Y2E5MWVlZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvc2VydmljZXMvQ2hpbGRTZXJ2aWNlcy52dWVcbi8vIG1vZHVsZSBpZCA9IDE2N1xuLy8gbW9kdWxlIGNodW5rcyA9IDYiLCIhZnVuY3Rpb24odCxlKXtcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cyYmXCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZT9tb2R1bGUuZXhwb3J0cz1lKCk6XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXSxlKTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9leHBvcnRzLlZ1ZU11bHRpc2VsZWN0PWUoKTp0LlZ1ZU11bHRpc2VsZWN0PWUoKX0odGhpcyxmdW5jdGlvbigpe3JldHVybiBmdW5jdGlvbih0KXtmdW5jdGlvbiBlKGkpe2lmKG5baV0pcmV0dXJuIG5baV0uZXhwb3J0czt2YXIgcj1uW2ldPXtpOmksbDohMSxleHBvcnRzOnt9fTtyZXR1cm4gdFtpXS5jYWxsKHIuZXhwb3J0cyxyLHIuZXhwb3J0cyxlKSxyLmw9ITAsci5leHBvcnRzfXZhciBuPXt9O3JldHVybiBlLm09dCxlLmM9bixlLmk9ZnVuY3Rpb24odCl7cmV0dXJuIHR9LGUuZD1mdW5jdGlvbih0LG4saSl7ZS5vKHQsbil8fE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LG4se2NvbmZpZ3VyYWJsZTohMSxlbnVtZXJhYmxlOiEwLGdldDppfSl9LGUubj1mdW5jdGlvbih0KXt2YXIgbj10JiZ0Ll9fZXNNb2R1bGU/ZnVuY3Rpb24oKXtyZXR1cm4gdC5kZWZhdWx0fTpmdW5jdGlvbigpe3JldHVybiB0fTtyZXR1cm4gZS5kKG4sXCJhXCIsbiksbn0sZS5vPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbCh0LGUpfSxlLnA9XCIvXCIsZShlLnM9NjApfShbZnVuY3Rpb24odCxlKXt2YXIgbj10LmV4cG9ydHM9XCJ1bmRlZmluZWRcIiE9dHlwZW9mIHdpbmRvdyYmd2luZG93Lk1hdGg9PU1hdGg/d2luZG93OlwidW5kZWZpbmVkXCIhPXR5cGVvZiBzZWxmJiZzZWxmLk1hdGg9PU1hdGg/c2VsZjpGdW5jdGlvbihcInJldHVybiB0aGlzXCIpKCk7XCJudW1iZXJcIj09dHlwZW9mIF9fZyYmKF9fZz1uKX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNDkpKFwid2tzXCIpLHI9bigzMCksbz1uKDApLlN5bWJvbCxzPVwiZnVuY3Rpb25cIj09dHlwZW9mIG87KHQuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gaVt0XXx8KGlbdF09cyYmb1t0XXx8KHM/bzpyKShcIlN5bWJvbC5cIit0KSl9KS5zdG9yZT1pfSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big1KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7aWYoIWkodCkpdGhyb3cgVHlwZUVycm9yKHQrXCIgaXMgbm90IGFuIG9iamVjdCFcIik7cmV0dXJuIHR9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigwKSxyPW4oMTApLG89big4KSxzPW4oNiksdT1uKDExKSxhPWZ1bmN0aW9uKHQsZSxuKXt2YXIgbCxjLGYscCxoPXQmYS5GLGQ9dCZhLkcsdj10JmEuUyxnPXQmYS5QLG09dCZhLkIseT1kP2k6dj9pW2VdfHwoaVtlXT17fSk6KGlbZV18fHt9KS5wcm90b3R5cGUsYj1kP3I6cltlXXx8KHJbZV09e30pLF89Yi5wcm90b3R5cGV8fChiLnByb3RvdHlwZT17fSk7ZCYmKG49ZSk7Zm9yKGwgaW4gbiljPSFoJiZ5JiZ2b2lkIDAhPT15W2xdLGY9KGM/eTpuKVtsXSxwPW0mJmM/dShmLGkpOmcmJlwiZnVuY3Rpb25cIj09dHlwZW9mIGY/dShGdW5jdGlvbi5jYWxsLGYpOmYseSYmcyh5LGwsZix0JmEuVSksYltsXSE9ZiYmbyhiLGwscCksZyYmX1tsXSE9ZiYmKF9bbF09Zil9O2kuY29yZT1yLGEuRj0xLGEuRz0yLGEuUz00LGEuUD04LGEuQj0xNixhLlc9MzIsYS5VPTY0LGEuUj0xMjgsdC5leHBvcnRzPWF9LGZ1bmN0aW9uKHQsZSxuKXt0LmV4cG9ydHM9IW4oNykoZnVuY3Rpb24oKXtyZXR1cm4gNyE9T2JqZWN0LmRlZmluZVByb3BlcnR5KHt9LFwiYVwiLHtnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gN319KS5hfSl9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVyblwib2JqZWN0XCI9PXR5cGVvZiB0P251bGwhPT10OlwiZnVuY3Rpb25cIj09dHlwZW9mIHR9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigwKSxyPW4oOCksbz1uKDEyKSxzPW4oMzApKFwic3JjXCIpLHU9RnVuY3Rpb24udG9TdHJpbmcsYT0oXCJcIit1KS5zcGxpdChcInRvU3RyaW5nXCIpO24oMTApLmluc3BlY3RTb3VyY2U9ZnVuY3Rpb24odCl7cmV0dXJuIHUuY2FsbCh0KX0sKHQuZXhwb3J0cz1mdW5jdGlvbih0LGUsbix1KXt2YXIgbD1cImZ1bmN0aW9uXCI9PXR5cGVvZiBuO2wmJihvKG4sXCJuYW1lXCIpfHxyKG4sXCJuYW1lXCIsZSkpLHRbZV0hPT1uJiYobCYmKG8obixzKXx8cihuLHMsdFtlXT9cIlwiK3RbZV06YS5qb2luKFN0cmluZyhlKSkpKSx0PT09aT90W2VdPW46dT90W2VdP3RbZV09bjpyKHQsZSxuKTooZGVsZXRlIHRbZV0scih0LGUsbikpKX0pKEZ1bmN0aW9uLnByb3RvdHlwZSxcInRvU3RyaW5nXCIsZnVuY3Rpb24oKXtyZXR1cm5cImZ1bmN0aW9uXCI9PXR5cGVvZiB0aGlzJiZ0aGlzW3NdfHx1LmNhbGwodGhpcyl9KX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7dHJ5e3JldHVybiEhdCgpfWNhdGNoKHQpe3JldHVybiEwfX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEzKSxyPW4oMjUpO3QuZXhwb3J0cz1uKDQpP2Z1bmN0aW9uKHQsZSxuKXtyZXR1cm4gaS5mKHQsZSxyKDEsbikpfTpmdW5jdGlvbih0LGUsbil7cmV0dXJuIHRbZV09bix0fX0sZnVuY3Rpb24odCxlKXt2YXIgbj17fS50b1N0cmluZzt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIG4uY2FsbCh0KS5zbGljZSg4LC0xKX19LGZ1bmN0aW9uKHQsZSl7dmFyIG49dC5leHBvcnRzPXt2ZXJzaW9uOlwiMi41LjdcIn07XCJudW1iZXJcIj09dHlwZW9mIF9fZSYmKF9fZT1uKX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTQpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbil7aWYoaSh0KSx2b2lkIDA9PT1lKXJldHVybiB0O3N3aXRjaChuKXtjYXNlIDE6cmV0dXJuIGZ1bmN0aW9uKG4pe3JldHVybiB0LmNhbGwoZSxuKX07Y2FzZSAyOnJldHVybiBmdW5jdGlvbihuLGkpe3JldHVybiB0LmNhbGwoZSxuLGkpfTtjYXNlIDM6cmV0dXJuIGZ1bmN0aW9uKG4saSxyKXtyZXR1cm4gdC5jYWxsKGUsbixpLHIpfX1yZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4gdC5hcHBseShlLGFyZ3VtZW50cyl9fX0sZnVuY3Rpb24odCxlKXt2YXIgbj17fS5oYXNPd25Qcm9wZXJ0eTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtyZXR1cm4gbi5jYWxsKHQsZSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigyKSxyPW4oNDEpLG89bigyOSkscz1PYmplY3QuZGVmaW5lUHJvcGVydHk7ZS5mPW4oNCk/T2JqZWN0LmRlZmluZVByb3BlcnR5OmZ1bmN0aW9uKHQsZSxuKXtpZihpKHQpLGU9byhlLCEwKSxpKG4pLHIpdHJ5e3JldHVybiBzKHQsZSxuKX1jYXRjaCh0KXt9aWYoXCJnZXRcImluIG58fFwic2V0XCJpbiBuKXRocm93IFR5cGVFcnJvcihcIkFjY2Vzc29ycyBub3Qgc3VwcG9ydGVkIVwiKTtyZXR1cm5cInZhbHVlXCJpbiBuJiYodFtlXT1uLnZhbHVlKSx0fX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7aWYoXCJmdW5jdGlvblwiIT10eXBlb2YgdCl0aHJvdyBUeXBlRXJyb3IodCtcIiBpcyBub3QgYSBmdW5jdGlvbiFcIik7cmV0dXJuIHR9fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz17fX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7aWYodm9pZCAwPT10KXRocm93IFR5cGVFcnJvcihcIkNhbid0IGNhbGwgbWV0aG9kIG9uICBcIit0KTtyZXR1cm4gdH19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDcpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUpe3JldHVybiEhdCYmaShmdW5jdGlvbigpe2U/dC5jYWxsKG51bGwsZnVuY3Rpb24oKXt9LDEpOnQuY2FsbChudWxsKX0pfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMjMpLHI9bigxNik7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBpKHIodCkpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNTMpLHI9TWF0aC5taW47dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiB0PjA/cihpKHQpLDkwMDcxOTkyNTQ3NDA5OTEpOjB9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxMSkscj1uKDIzKSxvPW4oMjgpLHM9bigxOSksdT1uKDY0KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXt2YXIgbj0xPT10LGE9Mj09dCxsPTM9PXQsYz00PT10LGY9Nj09dCxwPTU9PXR8fGYsaD1lfHx1O3JldHVybiBmdW5jdGlvbihlLHUsZCl7Zm9yKHZhciB2LGcsbT1vKGUpLHk9cihtKSxiPWkodSxkLDMpLF89cyh5Lmxlbmd0aCkseD0wLHc9bj9oKGUsXyk6YT9oKGUsMCk6dm9pZCAwO18+eDt4KyspaWYoKHB8fHggaW4geSkmJih2PXlbeF0sZz1iKHYseCxtKSx0KSlpZihuKXdbeF09ZztlbHNlIGlmKGcpc3dpdGNoKHQpe2Nhc2UgMzpyZXR1cm4hMDtjYXNlIDU6cmV0dXJuIHY7Y2FzZSA2OnJldHVybiB4O2Nhc2UgMjp3LnB1c2godil9ZWxzZSBpZihjKXJldHVybiExO3JldHVybiBmPy0xOmx8fGM/Yzp3fX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDUpLHI9bigwKS5kb2N1bWVudCxvPWkocikmJmkoci5jcmVhdGVFbGVtZW50KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIG8/ci5jcmVhdGVFbGVtZW50KHQpOnt9fX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9XCJjb25zdHJ1Y3RvcixoYXNPd25Qcm9wZXJ0eSxpc1Byb3RvdHlwZU9mLHByb3BlcnR5SXNFbnVtZXJhYmxlLHRvTG9jYWxlU3RyaW5nLHRvU3RyaW5nLHZhbHVlT2ZcIi5zcGxpdChcIixcIil9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDkpO3QuZXhwb3J0cz1PYmplY3QoXCJ6XCIpLnByb3BlcnR5SXNFbnVtZXJhYmxlKDApP09iamVjdDpmdW5jdGlvbih0KXtyZXR1cm5cIlN0cmluZ1wiPT1pKHQpP3Quc3BsaXQoXCJcIik6T2JqZWN0KHQpfX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ITF9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7cmV0dXJue2VudW1lcmFibGU6ISgxJnQpLGNvbmZpZ3VyYWJsZTohKDImdCksd3JpdGFibGU6ISg0JnQpLHZhbHVlOmV9fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTMpLmYscj1uKDEyKSxvPW4oMSkoXCJ0b1N0cmluZ1RhZ1wiKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4pe3QmJiFyKHQ9bj90OnQucHJvdG90eXBlLG8pJiZpKHQsbyx7Y29uZmlndXJhYmxlOiEwLHZhbHVlOmV9KX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDQ5KShcImtleXNcIikscj1uKDMwKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIGlbdF18fChpW3RdPXIodCkpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTYpO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gT2JqZWN0KGkodCkpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNSk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7aWYoIWkodCkpcmV0dXJuIHQ7dmFyIG4scjtpZihlJiZcImZ1bmN0aW9uXCI9PXR5cGVvZihuPXQudG9TdHJpbmcpJiYhaShyPW4uY2FsbCh0KSkpcmV0dXJuIHI7aWYoXCJmdW5jdGlvblwiPT10eXBlb2Yobj10LnZhbHVlT2YpJiYhaShyPW4uY2FsbCh0KSkpcmV0dXJuIHI7aWYoIWUmJlwiZnVuY3Rpb25cIj09dHlwZW9mKG49dC50b1N0cmluZykmJiFpKHI9bi5jYWxsKHQpKSlyZXR1cm4gcjt0aHJvdyBUeXBlRXJyb3IoXCJDYW4ndCBjb252ZXJ0IG9iamVjdCB0byBwcmltaXRpdmUgdmFsdWVcIil9fSxmdW5jdGlvbih0LGUpe3ZhciBuPTAsaT1NYXRoLnJhbmRvbSgpO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm5cIlN5bWJvbChcIi5jb25jYXQodm9pZCAwPT09dD9cIlwiOnQsXCIpX1wiLCgrK24raSkudG9TdHJpbmcoMzYpKX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDApLHI9bigxMiksbz1uKDkpLHM9big2NyksdT1uKDI5KSxhPW4oNyksbD1uKDc3KS5mLGM9big0NSkuZixmPW4oMTMpLmYscD1uKDUxKS50cmltLGg9aS5OdW1iZXIsZD1oLHY9aC5wcm90b3R5cGUsZz1cIk51bWJlclwiPT1vKG4oNDQpKHYpKSxtPVwidHJpbVwiaW4gU3RyaW5nLnByb3RvdHlwZSx5PWZ1bmN0aW9uKHQpe3ZhciBlPXUodCwhMSk7aWYoXCJzdHJpbmdcIj09dHlwZW9mIGUmJmUubGVuZ3RoPjIpe2U9bT9lLnRyaW0oKTpwKGUsMyk7dmFyIG4saSxyLG89ZS5jaGFyQ29kZUF0KDApO2lmKDQzPT09b3x8NDU9PT1vKXtpZig4OD09PShuPWUuY2hhckNvZGVBdCgyKSl8fDEyMD09PW4pcmV0dXJuIE5hTn1lbHNlIGlmKDQ4PT09byl7c3dpdGNoKGUuY2hhckNvZGVBdCgxKSl7Y2FzZSA2NjpjYXNlIDk4Omk9MixyPTQ5O2JyZWFrO2Nhc2UgNzk6Y2FzZSAxMTE6aT04LHI9NTU7YnJlYWs7ZGVmYXVsdDpyZXR1cm4rZX1mb3IodmFyIHMsYT1lLnNsaWNlKDIpLGw9MCxjPWEubGVuZ3RoO2w8YztsKyspaWYoKHM9YS5jaGFyQ29kZUF0KGwpKTw0OHx8cz5yKXJldHVybiBOYU47cmV0dXJuIHBhcnNlSW50KGEsaSl9fXJldHVybitlfTtpZighaChcIiAwbzFcIil8fCFoKFwiMGIxXCIpfHxoKFwiKzB4MVwiKSl7aD1mdW5jdGlvbih0KXt2YXIgZT1hcmd1bWVudHMubGVuZ3RoPDE/MDp0LG49dGhpcztyZXR1cm4gbiBpbnN0YW5jZW9mIGgmJihnP2EoZnVuY3Rpb24oKXt2LnZhbHVlT2YuY2FsbChuKX0pOlwiTnVtYmVyXCIhPW8obikpP3MobmV3IGQoeShlKSksbixoKTp5KGUpfTtmb3IodmFyIGIsXz1uKDQpP2woZCk6XCJNQVhfVkFMVUUsTUlOX1ZBTFVFLE5hTixORUdBVElWRV9JTkZJTklUWSxQT1NJVElWRV9JTkZJTklUWSxFUFNJTE9OLGlzRmluaXRlLGlzSW50ZWdlcixpc05hTixpc1NhZmVJbnRlZ2VyLE1BWF9TQUZFX0lOVEVHRVIsTUlOX1NBRkVfSU5URUdFUixwYXJzZUZsb2F0LHBhcnNlSW50LGlzSW50ZWdlclwiLnNwbGl0KFwiLFwiKSx4PTA7Xy5sZW5ndGg+eDt4KyspcihkLGI9X1t4XSkmJiFyKGgsYikmJmYoaCxiLGMoZCxiKSk7aC5wcm90b3R5cGU9dix2LmNvbnN0cnVjdG9yPWgsbig2KShpLFwiTnVtYmVyXCIsaCl9fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gaSh0KXtyZXR1cm4gMCE9PXQmJighKCFBcnJheS5pc0FycmF5KHQpfHwwIT09dC5sZW5ndGgpfHwhdCl9ZnVuY3Rpb24gcih0KXtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4hdC5hcHBseSh2b2lkIDAsYXJndW1lbnRzKX19ZnVuY3Rpb24gbyh0LGUpe3JldHVybiB2b2lkIDA9PT10JiYodD1cInVuZGVmaW5lZFwiKSxudWxsPT09dCYmKHQ9XCJudWxsXCIpLCExPT09dCYmKHQ9XCJmYWxzZVwiKSwtMSE9PXQudG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZS50cmltKCkpfWZ1bmN0aW9uIHModCxlLG4saSl7cmV0dXJuIHQuZmlsdGVyKGZ1bmN0aW9uKHQpe3JldHVybiBvKGkodCxuKSxlKX0pfWZ1bmN0aW9uIHUodCl7cmV0dXJuIHQuZmlsdGVyKGZ1bmN0aW9uKHQpe3JldHVybiF0LiRpc0xhYmVsfSl9ZnVuY3Rpb24gYSh0LGUpe3JldHVybiBmdW5jdGlvbihuKXtyZXR1cm4gbi5yZWR1Y2UoZnVuY3Rpb24obixpKXtyZXR1cm4gaVt0XSYmaVt0XS5sZW5ndGg/KG4ucHVzaCh7JGdyb3VwTGFiZWw6aVtlXSwkaXNMYWJlbDohMH0pLG4uY29uY2F0KGlbdF0pKTpufSxbXSl9fWZ1bmN0aW9uIGwodCxlLGkscixvKXtyZXR1cm4gZnVuY3Rpb24odSl7cmV0dXJuIHUubWFwKGZ1bmN0aW9uKHUpe3ZhciBhO2lmKCF1W2ldKXJldHVybiBjb25zb2xlLndhcm4oXCJPcHRpb25zIHBhc3NlZCB0byB2dWUtbXVsdGlzZWxlY3QgZG8gbm90IGNvbnRhaW4gZ3JvdXBzLCBkZXNwaXRlIHRoZSBjb25maWcuXCIpLFtdO3ZhciBsPXModVtpXSx0LGUsbyk7cmV0dXJuIGwubGVuZ3RoPyhhPXt9LG4uaShkLmEpKGEscix1W3JdKSxuLmkoZC5hKShhLGksbCksYSk6W119KX19dmFyIGM9big1OSksZj1uKDU0KSxwPShuLm4oZiksbig5NSkpLGg9KG4ubihwKSxuKDMxKSksZD0obi5uKGgpLG4oNTgpKSx2PW4oOTEpLGc9KG4ubih2KSxuKDk4KSksbT0obi5uKGcpLG4oOTIpKSx5PShuLm4obSksbig4OCkpLGI9KG4ubih5KSxuKDk3KSksXz0obi5uKGIpLG4oODkpKSx4PShuLm4oXyksbig5NikpLHc9KG4ubih4KSxuKDkzKSksUz0obi5uKHcpLG4oOTApKSxPPShuLm4oUyksZnVuY3Rpb24oKXtmb3IodmFyIHQ9YXJndW1lbnRzLmxlbmd0aCxlPW5ldyBBcnJheSh0KSxuPTA7bjx0O24rKyllW25dPWFyZ3VtZW50c1tuXTtyZXR1cm4gZnVuY3Rpb24odCl7cmV0dXJuIGUucmVkdWNlKGZ1bmN0aW9uKHQsZSl7cmV0dXJuIGUodCl9LHQpfX0pO2UuYT17ZGF0YTpmdW5jdGlvbigpe3JldHVybntzZWFyY2g6XCJcIixpc09wZW46ITEscHJlZmZlcmVkT3BlbkRpcmVjdGlvbjpcImJlbG93XCIsb3B0aW1pemVkSGVpZ2h0OnRoaXMubWF4SGVpZ2h0fX0scHJvcHM6e2ludGVybmFsU2VhcmNoOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0sb3B0aW9uczp7dHlwZTpBcnJheSxyZXF1aXJlZDohMH0sbXVsdGlwbGU6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSx2YWx1ZTp7dHlwZTpudWxsLGRlZmF1bHQ6ZnVuY3Rpb24oKXtyZXR1cm5bXX19LHRyYWNrQnk6e3R5cGU6U3RyaW5nfSxsYWJlbDp7dHlwZTpTdHJpbmd9LHNlYXJjaGFibGU6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiEwfSxjbGVhck9uU2VsZWN0Ont0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0saGlkZVNlbGVjdGVkOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0scGxhY2Vob2xkZXI6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJTZWxlY3Qgb3B0aW9uXCJ9LGFsbG93RW1wdHk6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiEwfSxyZXNldEFmdGVyOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0sY2xvc2VPblNlbGVjdDp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITB9LGN1c3RvbUxhYmVsOnt0eXBlOkZ1bmN0aW9uLGRlZmF1bHQ6ZnVuY3Rpb24odCxlKXtyZXR1cm4gaSh0KT9cIlwiOmU/dFtlXTp0fX0sdGFnZ2FibGU6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSx0YWdQbGFjZWhvbGRlcjp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIlByZXNzIGVudGVyIHRvIGNyZWF0ZSBhIHRhZ1wifSx0YWdQb3NpdGlvbjp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcInRvcFwifSxtYXg6e3R5cGU6W051bWJlcixCb29sZWFuXSxkZWZhdWx0OiExfSxpZDp7ZGVmYXVsdDpudWxsfSxvcHRpb25zTGltaXQ6e3R5cGU6TnVtYmVyLGRlZmF1bHQ6MWUzfSxncm91cFZhbHVlczp7dHlwZTpTdHJpbmd9LGdyb3VwTGFiZWw6e3R5cGU6U3RyaW5nfSxncm91cFNlbGVjdDp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITF9LGJsb2NrS2V5czp7dHlwZTpBcnJheSxkZWZhdWx0OmZ1bmN0aW9uKCl7cmV0dXJuW119fSxwcmVzZXJ2ZVNlYXJjaDp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITF9LHByZXNlbGVjdEZpcnN0Ont0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX19LG1vdW50ZWQ6ZnVuY3Rpb24oKXt0aGlzLm11bHRpcGxlfHx0aGlzLmNsZWFyT25TZWxlY3R8fGNvbnNvbGUud2FybihcIltWdWUtTXVsdGlzZWxlY3Qgd2Fybl06IENsZWFyT25TZWxlY3QgYW5kIE11bHRpcGxlIHByb3BzIGNhbuKAmXQgYmUgYm90aCBzZXQgdG8gZmFsc2UuXCIpLCF0aGlzLm11bHRpcGxlJiZ0aGlzLm1heCYmY29uc29sZS53YXJuKFwiW1Z1ZS1NdWx0aXNlbGVjdCB3YXJuXTogTWF4IHByb3Agc2hvdWxkIG5vdCBiZSB1c2VkIHdoZW4gcHJvcCBNdWx0aXBsZSBlcXVhbHMgZmFsc2UuXCIpLHRoaXMucHJlc2VsZWN0Rmlyc3QmJiF0aGlzLmludGVybmFsVmFsdWUubGVuZ3RoJiZ0aGlzLm9wdGlvbnMubGVuZ3RoJiZ0aGlzLnNlbGVjdCh0aGlzLmZpbHRlcmVkT3B0aW9uc1swXSl9LGNvbXB1dGVkOntpbnRlcm5hbFZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMudmFsdWV8fDA9PT10aGlzLnZhbHVlP0FycmF5LmlzQXJyYXkodGhpcy52YWx1ZSk/dGhpcy52YWx1ZTpbdGhpcy52YWx1ZV06W119LGZpbHRlcmVkT3B0aW9uczpmdW5jdGlvbigpe3ZhciB0PXRoaXMuc2VhcmNofHxcIlwiLGU9dC50b0xvd2VyQ2FzZSgpLnRyaW0oKSxuPXRoaXMub3B0aW9ucy5jb25jYXQoKTtyZXR1cm4gbj10aGlzLmludGVybmFsU2VhcmNoP3RoaXMuZ3JvdXBWYWx1ZXM/dGhpcy5maWx0ZXJBbmRGbGF0KG4sZSx0aGlzLmxhYmVsKTpzKG4sZSx0aGlzLmxhYmVsLHRoaXMuY3VzdG9tTGFiZWwpOnRoaXMuZ3JvdXBWYWx1ZXM/YSh0aGlzLmdyb3VwVmFsdWVzLHRoaXMuZ3JvdXBMYWJlbCkobik6bixuPXRoaXMuaGlkZVNlbGVjdGVkP24uZmlsdGVyKHIodGhpcy5pc1NlbGVjdGVkKSk6bix0aGlzLnRhZ2dhYmxlJiZlLmxlbmd0aCYmIXRoaXMuaXNFeGlzdGluZ09wdGlvbihlKSYmKFwiYm90dG9tXCI9PT10aGlzLnRhZ1Bvc2l0aW9uP24ucHVzaCh7aXNUYWc6ITAsbGFiZWw6dH0pOm4udW5zaGlmdCh7aXNUYWc6ITAsbGFiZWw6dH0pKSxuLnNsaWNlKDAsdGhpcy5vcHRpb25zTGltaXQpfSx2YWx1ZUtleXM6ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3JldHVybiB0aGlzLnRyYWNrQnk/dGhpcy5pbnRlcm5hbFZhbHVlLm1hcChmdW5jdGlvbihlKXtyZXR1cm4gZVt0LnRyYWNrQnldfSk6dGhpcy5pbnRlcm5hbFZhbHVlfSxvcHRpb25LZXlzOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpcztyZXR1cm4odGhpcy5ncm91cFZhbHVlcz90aGlzLmZsYXRBbmRTdHJpcCh0aGlzLm9wdGlvbnMpOnRoaXMub3B0aW9ucykubWFwKGZ1bmN0aW9uKGUpe3JldHVybiB0LmN1c3RvbUxhYmVsKGUsdC5sYWJlbCkudG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpfSl9LGN1cnJlbnRPcHRpb25MYWJlbDpmdW5jdGlvbigpe3JldHVybiB0aGlzLm11bHRpcGxlP3RoaXMuc2VhcmNoYWJsZT9cIlwiOnRoaXMucGxhY2Vob2xkZXI6dGhpcy5pbnRlcm5hbFZhbHVlLmxlbmd0aD90aGlzLmdldE9wdGlvbkxhYmVsKHRoaXMuaW50ZXJuYWxWYWx1ZVswXSk6dGhpcy5zZWFyY2hhYmxlP1wiXCI6dGhpcy5wbGFjZWhvbGRlcn19LHdhdGNoOntpbnRlcm5hbFZhbHVlOmZ1bmN0aW9uKCl7dGhpcy5yZXNldEFmdGVyJiZ0aGlzLmludGVybmFsVmFsdWUubGVuZ3RoJiYodGhpcy5zZWFyY2g9XCJcIix0aGlzLiRlbWl0KFwiaW5wdXRcIix0aGlzLm11bHRpcGxlP1tdOm51bGwpKX0sc2VhcmNoOmZ1bmN0aW9uKCl7dGhpcy4kZW1pdChcInNlYXJjaC1jaGFuZ2VcIix0aGlzLnNlYXJjaCx0aGlzLmlkKX19LG1ldGhvZHM6e2dldFZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMubXVsdGlwbGU/dGhpcy5pbnRlcm5hbFZhbHVlOjA9PT10aGlzLmludGVybmFsVmFsdWUubGVuZ3RoP251bGw6dGhpcy5pbnRlcm5hbFZhbHVlWzBdfSxmaWx0ZXJBbmRGbGF0OmZ1bmN0aW9uKHQsZSxuKXtyZXR1cm4gTyhsKGUsbix0aGlzLmdyb3VwVmFsdWVzLHRoaXMuZ3JvdXBMYWJlbCx0aGlzLmN1c3RvbUxhYmVsKSxhKHRoaXMuZ3JvdXBWYWx1ZXMsdGhpcy5ncm91cExhYmVsKSkodCl9LGZsYXRBbmRTdHJpcDpmdW5jdGlvbih0KXtyZXR1cm4gTyhhKHRoaXMuZ3JvdXBWYWx1ZXMsdGhpcy5ncm91cExhYmVsKSx1KSh0KX0sdXBkYXRlU2VhcmNoOmZ1bmN0aW9uKHQpe3RoaXMuc2VhcmNoPXR9LGlzRXhpc3RpbmdPcHRpb246ZnVuY3Rpb24odCl7cmV0dXJuISF0aGlzLm9wdGlvbnMmJnRoaXMub3B0aW9uS2V5cy5pbmRleE9mKHQpPi0xfSxpc1NlbGVjdGVkOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMudHJhY2tCeT90W3RoaXMudHJhY2tCeV06dDtyZXR1cm4gdGhpcy52YWx1ZUtleXMuaW5kZXhPZihlKT4tMX0sZ2V0T3B0aW9uTGFiZWw6ZnVuY3Rpb24odCl7aWYoaSh0KSlyZXR1cm5cIlwiO2lmKHQuaXNUYWcpcmV0dXJuIHQubGFiZWw7aWYodC4kaXNMYWJlbClyZXR1cm4gdC4kZ3JvdXBMYWJlbDt2YXIgZT10aGlzLmN1c3RvbUxhYmVsKHQsdGhpcy5sYWJlbCk7cmV0dXJuIGkoZSk/XCJcIjplfSxzZWxlY3Q6ZnVuY3Rpb24odCxlKXtpZih0LiRpc0xhYmVsJiZ0aGlzLmdyb3VwU2VsZWN0KXJldHVybiB2b2lkIHRoaXMuc2VsZWN0R3JvdXAodCk7aWYoISgtMSE9PXRoaXMuYmxvY2tLZXlzLmluZGV4T2YoZSl8fHRoaXMuZGlzYWJsZWR8fHQuJGlzRGlzYWJsZWR8fHQuJGlzTGFiZWwpJiYoIXRoaXMubWF4fHwhdGhpcy5tdWx0aXBsZXx8dGhpcy5pbnRlcm5hbFZhbHVlLmxlbmd0aCE9PXRoaXMubWF4KSYmKFwiVGFiXCIhPT1lfHx0aGlzLnBvaW50ZXJEaXJ0eSkpe2lmKHQuaXNUYWcpdGhpcy4kZW1pdChcInRhZ1wiLHQubGFiZWwsdGhpcy5pZCksdGhpcy5zZWFyY2g9XCJcIix0aGlzLmNsb3NlT25TZWxlY3QmJiF0aGlzLm11bHRpcGxlJiZ0aGlzLmRlYWN0aXZhdGUoKTtlbHNle2lmKHRoaXMuaXNTZWxlY3RlZCh0KSlyZXR1cm4gdm9pZChcIlRhYlwiIT09ZSYmdGhpcy5yZW1vdmVFbGVtZW50KHQpKTt0aGlzLiRlbWl0KFwic2VsZWN0XCIsdCx0aGlzLmlkKSx0aGlzLm11bHRpcGxlP3RoaXMuJGVtaXQoXCJpbnB1dFwiLHRoaXMuaW50ZXJuYWxWYWx1ZS5jb25jYXQoW3RdKSx0aGlzLmlkKTp0aGlzLiRlbWl0KFwiaW5wdXRcIix0LHRoaXMuaWQpLHRoaXMuY2xlYXJPblNlbGVjdCYmKHRoaXMuc2VhcmNoPVwiXCIpfXRoaXMuY2xvc2VPblNlbGVjdCYmdGhpcy5kZWFjdGl2YXRlKCl9fSxzZWxlY3RHcm91cDpmdW5jdGlvbih0KXt2YXIgZT10aGlzLG49dGhpcy5vcHRpb25zLmZpbmQoZnVuY3Rpb24obil7cmV0dXJuIG5bZS5ncm91cExhYmVsXT09PXQuJGdyb3VwTGFiZWx9KTtpZihuKWlmKHRoaXMud2hvbGVHcm91cFNlbGVjdGVkKG4pKXt0aGlzLiRlbWl0KFwicmVtb3ZlXCIsblt0aGlzLmdyb3VwVmFsdWVzXSx0aGlzLmlkKTt2YXIgaT10aGlzLmludGVybmFsVmFsdWUuZmlsdGVyKGZ1bmN0aW9uKHQpe3JldHVybi0xPT09bltlLmdyb3VwVmFsdWVzXS5pbmRleE9mKHQpfSk7dGhpcy4kZW1pdChcImlucHV0XCIsaSx0aGlzLmlkKX1lbHNle3ZhciBvPW5bdGhpcy5ncm91cFZhbHVlc10uZmlsdGVyKHIodGhpcy5pc1NlbGVjdGVkKSk7dGhpcy4kZW1pdChcInNlbGVjdFwiLG8sdGhpcy5pZCksdGhpcy4kZW1pdChcImlucHV0XCIsdGhpcy5pbnRlcm5hbFZhbHVlLmNvbmNhdChvKSx0aGlzLmlkKX19LHdob2xlR3JvdXBTZWxlY3RlZDpmdW5jdGlvbih0KXtyZXR1cm4gdFt0aGlzLmdyb3VwVmFsdWVzXS5ldmVyeSh0aGlzLmlzU2VsZWN0ZWQpfSxyZW1vdmVFbGVtZW50OmZ1bmN0aW9uKHQpe3ZhciBlPSEoYXJndW1lbnRzLmxlbmd0aD4xJiZ2b2lkIDAhPT1hcmd1bWVudHNbMV0pfHxhcmd1bWVudHNbMV07aWYoIXRoaXMuZGlzYWJsZWQpe2lmKCF0aGlzLmFsbG93RW1wdHkmJnRoaXMuaW50ZXJuYWxWYWx1ZS5sZW5ndGg8PTEpcmV0dXJuIHZvaWQgdGhpcy5kZWFjdGl2YXRlKCk7dmFyIGk9XCJvYmplY3RcIj09PW4uaShjLmEpKHQpP3RoaXMudmFsdWVLZXlzLmluZGV4T2YodFt0aGlzLnRyYWNrQnldKTp0aGlzLnZhbHVlS2V5cy5pbmRleE9mKHQpO2lmKHRoaXMuJGVtaXQoXCJyZW1vdmVcIix0LHRoaXMuaWQpLHRoaXMubXVsdGlwbGUpe3ZhciByPXRoaXMuaW50ZXJuYWxWYWx1ZS5zbGljZSgwLGkpLmNvbmNhdCh0aGlzLmludGVybmFsVmFsdWUuc2xpY2UoaSsxKSk7dGhpcy4kZW1pdChcImlucHV0XCIscix0aGlzLmlkKX1lbHNlIHRoaXMuJGVtaXQoXCJpbnB1dFwiLG51bGwsdGhpcy5pZCk7dGhpcy5jbG9zZU9uU2VsZWN0JiZlJiZ0aGlzLmRlYWN0aXZhdGUoKX19LHJlbW92ZUxhc3RFbGVtZW50OmZ1bmN0aW9uKCl7LTE9PT10aGlzLmJsb2NrS2V5cy5pbmRleE9mKFwiRGVsZXRlXCIpJiYwPT09dGhpcy5zZWFyY2gubGVuZ3RoJiZBcnJheS5pc0FycmF5KHRoaXMuaW50ZXJuYWxWYWx1ZSkmJnRoaXMucmVtb3ZlRWxlbWVudCh0aGlzLmludGVybmFsVmFsdWVbdGhpcy5pbnRlcm5hbFZhbHVlLmxlbmd0aC0xXSwhMSl9LGFjdGl2YXRlOmZ1bmN0aW9uKCl7dmFyIHQ9dGhpczt0aGlzLmlzT3Blbnx8dGhpcy5kaXNhYmxlZHx8KHRoaXMuYWRqdXN0UG9zaXRpb24oKSx0aGlzLmdyb3VwVmFsdWVzJiYwPT09dGhpcy5wb2ludGVyJiZ0aGlzLmZpbHRlcmVkT3B0aW9ucy5sZW5ndGgmJih0aGlzLnBvaW50ZXI9MSksdGhpcy5pc09wZW49ITAsdGhpcy5zZWFyY2hhYmxlPyh0aGlzLnByZXNlcnZlU2VhcmNofHwodGhpcy5zZWFyY2g9XCJcIiksdGhpcy4kbmV4dFRpY2soZnVuY3Rpb24oKXtyZXR1cm4gdC4kcmVmcy5zZWFyY2guZm9jdXMoKX0pKTp0aGlzLiRlbC5mb2N1cygpLHRoaXMuJGVtaXQoXCJvcGVuXCIsdGhpcy5pZCkpfSxkZWFjdGl2YXRlOmZ1bmN0aW9uKCl7dGhpcy5pc09wZW4mJih0aGlzLmlzT3Blbj0hMSx0aGlzLnNlYXJjaGFibGU/dGhpcy4kcmVmcy5zZWFyY2guYmx1cigpOnRoaXMuJGVsLmJsdXIoKSx0aGlzLnByZXNlcnZlU2VhcmNofHwodGhpcy5zZWFyY2g9XCJcIiksdGhpcy4kZW1pdChcImNsb3NlXCIsdGhpcy5nZXRWYWx1ZSgpLHRoaXMuaWQpKX0sdG9nZ2xlOmZ1bmN0aW9uKCl7dGhpcy5pc09wZW4/dGhpcy5kZWFjdGl2YXRlKCk6dGhpcy5hY3RpdmF0ZSgpfSxhZGp1c3RQb3NpdGlvbjpmdW5jdGlvbigpe2lmKFwidW5kZWZpbmVkXCIhPXR5cGVvZiB3aW5kb3cpe3ZhciB0PXRoaXMuJGVsLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcCxlPXdpbmRvdy5pbm5lckhlaWdodC10aGlzLiRlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5ib3R0b207ZT50aGlzLm1heEhlaWdodHx8ZT50fHxcImJlbG93XCI9PT10aGlzLm9wZW5EaXJlY3Rpb258fFwiYm90dG9tXCI9PT10aGlzLm9wZW5EaXJlY3Rpb24/KHRoaXMucHJlZmZlcmVkT3BlbkRpcmVjdGlvbj1cImJlbG93XCIsdGhpcy5vcHRpbWl6ZWRIZWlnaHQ9TWF0aC5taW4oZS00MCx0aGlzLm1heEhlaWdodCkpOih0aGlzLnByZWZmZXJlZE9wZW5EaXJlY3Rpb249XCJhYm92ZVwiLHRoaXMub3B0aW1pemVkSGVpZ2h0PU1hdGgubWluKHQtNDAsdGhpcy5tYXhIZWlnaHQpKX19fX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDU0KSxyPShuLm4oaSksbigzMSkpO24ubihyKTtlLmE9e2RhdGE6ZnVuY3Rpb24oKXtyZXR1cm57cG9pbnRlcjowLHBvaW50ZXJEaXJ0eTohMX19LHByb3BzOntzaG93UG9pbnRlcjp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITB9LG9wdGlvbkhlaWdodDp7dHlwZTpOdW1iZXIsZGVmYXVsdDo0MH19LGNvbXB1dGVkOntwb2ludGVyUG9zaXRpb246ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5wb2ludGVyKnRoaXMub3B0aW9uSGVpZ2h0fSx2aXNpYmxlRWxlbWVudHM6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5vcHRpbWl6ZWRIZWlnaHQvdGhpcy5vcHRpb25IZWlnaHR9fSx3YXRjaDp7ZmlsdGVyZWRPcHRpb25zOmZ1bmN0aW9uKCl7dGhpcy5wb2ludGVyQWRqdXN0KCl9LGlzT3BlbjpmdW5jdGlvbigpe3RoaXMucG9pbnRlckRpcnR5PSExfX0sbWV0aG9kczp7b3B0aW9uSGlnaGxpZ2h0OmZ1bmN0aW9uKHQsZSl7cmV0dXJue1wibXVsdGlzZWxlY3RfX29wdGlvbi0taGlnaGxpZ2h0XCI6dD09PXRoaXMucG9pbnRlciYmdGhpcy5zaG93UG9pbnRlcixcIm11bHRpc2VsZWN0X19vcHRpb24tLXNlbGVjdGVkXCI6dGhpcy5pc1NlbGVjdGVkKGUpfX0sZ3JvdXBIaWdobGlnaHQ6ZnVuY3Rpb24odCxlKXt2YXIgbj10aGlzO2lmKCF0aGlzLmdyb3VwU2VsZWN0KXJldHVybltcIm11bHRpc2VsZWN0X19vcHRpb24tLWdyb3VwXCIsXCJtdWx0aXNlbGVjdF9fb3B0aW9uLS1kaXNhYmxlZFwiXTt2YXIgaT10aGlzLm9wdGlvbnMuZmluZChmdW5jdGlvbih0KXtyZXR1cm4gdFtuLmdyb3VwTGFiZWxdPT09ZS4kZ3JvdXBMYWJlbH0pO3JldHVybltcIm11bHRpc2VsZWN0X19vcHRpb24tLWdyb3VwXCIse1wibXVsdGlzZWxlY3RfX29wdGlvbi0taGlnaGxpZ2h0XCI6dD09PXRoaXMucG9pbnRlciYmdGhpcy5zaG93UG9pbnRlcn0se1wibXVsdGlzZWxlY3RfX29wdGlvbi0tZ3JvdXAtc2VsZWN0ZWRcIjp0aGlzLndob2xlR3JvdXBTZWxlY3RlZChpKX1dfSxhZGRQb2ludGVyRWxlbWVudDpmdW5jdGlvbigpe3ZhciB0PWFyZ3VtZW50cy5sZW5ndGg+MCYmdm9pZCAwIT09YXJndW1lbnRzWzBdP2FyZ3VtZW50c1swXTpcIkVudGVyXCIsZT10LmtleTt0aGlzLmZpbHRlcmVkT3B0aW9ucy5sZW5ndGg+MCYmdGhpcy5zZWxlY3QodGhpcy5maWx0ZXJlZE9wdGlvbnNbdGhpcy5wb2ludGVyXSxlKSx0aGlzLnBvaW50ZXJSZXNldCgpfSxwb2ludGVyRm9yd2FyZDpmdW5jdGlvbigpe3RoaXMucG9pbnRlcjx0aGlzLmZpbHRlcmVkT3B0aW9ucy5sZW5ndGgtMSYmKHRoaXMucG9pbnRlcisrLHRoaXMuJHJlZnMubGlzdC5zY3JvbGxUb3A8PXRoaXMucG9pbnRlclBvc2l0aW9uLSh0aGlzLnZpc2libGVFbGVtZW50cy0xKSp0aGlzLm9wdGlvbkhlaWdodCYmKHRoaXMuJHJlZnMubGlzdC5zY3JvbGxUb3A9dGhpcy5wb2ludGVyUG9zaXRpb24tKHRoaXMudmlzaWJsZUVsZW1lbnRzLTEpKnRoaXMub3B0aW9uSGVpZ2h0KSx0aGlzLmZpbHRlcmVkT3B0aW9uc1t0aGlzLnBvaW50ZXJdJiZ0aGlzLmZpbHRlcmVkT3B0aW9uc1t0aGlzLnBvaW50ZXJdLiRpc0xhYmVsJiYhdGhpcy5ncm91cFNlbGVjdCYmdGhpcy5wb2ludGVyRm9yd2FyZCgpKSx0aGlzLnBvaW50ZXJEaXJ0eT0hMH0scG9pbnRlckJhY2t3YXJkOmZ1bmN0aW9uKCl7dGhpcy5wb2ludGVyPjA/KHRoaXMucG9pbnRlci0tLHRoaXMuJHJlZnMubGlzdC5zY3JvbGxUb3A+PXRoaXMucG9pbnRlclBvc2l0aW9uJiYodGhpcy4kcmVmcy5saXN0LnNjcm9sbFRvcD10aGlzLnBvaW50ZXJQb3NpdGlvbiksdGhpcy5maWx0ZXJlZE9wdGlvbnNbdGhpcy5wb2ludGVyXSYmdGhpcy5maWx0ZXJlZE9wdGlvbnNbdGhpcy5wb2ludGVyXS4kaXNMYWJlbCYmIXRoaXMuZ3JvdXBTZWxlY3QmJnRoaXMucG9pbnRlckJhY2t3YXJkKCkpOnRoaXMuZmlsdGVyZWRPcHRpb25zW3RoaXMucG9pbnRlcl0mJnRoaXMuZmlsdGVyZWRPcHRpb25zWzBdLiRpc0xhYmVsJiYhdGhpcy5ncm91cFNlbGVjdCYmdGhpcy5wb2ludGVyRm9yd2FyZCgpLHRoaXMucG9pbnRlckRpcnR5PSEwfSxwb2ludGVyUmVzZXQ6ZnVuY3Rpb24oKXt0aGlzLmNsb3NlT25TZWxlY3QmJih0aGlzLnBvaW50ZXI9MCx0aGlzLiRyZWZzLmxpc3QmJih0aGlzLiRyZWZzLmxpc3Quc2Nyb2xsVG9wPTApKX0scG9pbnRlckFkanVzdDpmdW5jdGlvbigpe3RoaXMucG9pbnRlcj49dGhpcy5maWx0ZXJlZE9wdGlvbnMubGVuZ3RoLTEmJih0aGlzLnBvaW50ZXI9dGhpcy5maWx0ZXJlZE9wdGlvbnMubGVuZ3RoP3RoaXMuZmlsdGVyZWRPcHRpb25zLmxlbmd0aC0xOjApLHRoaXMuZmlsdGVyZWRPcHRpb25zLmxlbmd0aD4wJiZ0aGlzLmZpbHRlcmVkT3B0aW9uc1t0aGlzLnBvaW50ZXJdLiRpc0xhYmVsJiYhdGhpcy5ncm91cFNlbGVjdCYmdGhpcy5wb2ludGVyRm9yd2FyZCgpfSxwb2ludGVyU2V0OmZ1bmN0aW9uKHQpe3RoaXMucG9pbnRlcj10LHRoaXMucG9pbnRlckRpcnR5PSEwfX19fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGk9bigzNikscj1uKDc0KSxvPW4oMTUpLHM9bigxOCk7dC5leHBvcnRzPW4oNzIpKEFycmF5LFwiQXJyYXlcIixmdW5jdGlvbih0LGUpe3RoaXMuX3Q9cyh0KSx0aGlzLl9pPTAsdGhpcy5faz1lfSxmdW5jdGlvbigpe3ZhciB0PXRoaXMuX3QsZT10aGlzLl9rLG49dGhpcy5faSsrO3JldHVybiF0fHxuPj10Lmxlbmd0aD8odGhpcy5fdD12b2lkIDAscigxKSk6XCJrZXlzXCI9PWU/cigwLG4pOlwidmFsdWVzXCI9PWU/cigwLHRbbl0pOnIoMCxbbix0W25dXSl9LFwidmFsdWVzXCIpLG8uQXJndW1lbnRzPW8uQXJyYXksaShcImtleXNcIiksaShcInZhbHVlc1wiKSxpKFwiZW50cmllc1wiKX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMzEpLHI9KG4ubihpKSxuKDMyKSksbz1uKDMzKTtlLmE9e25hbWU6XCJ2dWUtbXVsdGlzZWxlY3RcIixtaXhpbnM6W3IuYSxvLmFdLHByb3BzOntuYW1lOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiXCJ9LHNlbGVjdExhYmVsOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiUHJlc3MgZW50ZXIgdG8gc2VsZWN0XCJ9LHNlbGVjdEdyb3VwTGFiZWw6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJQcmVzcyBlbnRlciB0byBzZWxlY3QgZ3JvdXBcIn0sc2VsZWN0ZWRMYWJlbDp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIlNlbGVjdGVkXCJ9LGRlc2VsZWN0TGFiZWw6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJQcmVzcyBlbnRlciB0byByZW1vdmVcIn0sZGVzZWxlY3RHcm91cExhYmVsOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiUHJlc3MgZW50ZXIgdG8gZGVzZWxlY3QgZ3JvdXBcIn0sc2hvd0xhYmVsczp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITB9LGxpbWl0Ont0eXBlOk51bWJlcixkZWZhdWx0Ojk5OTk5fSxtYXhIZWlnaHQ6e3R5cGU6TnVtYmVyLGRlZmF1bHQ6MzAwfSxsaW1pdFRleHQ6e3R5cGU6RnVuY3Rpb24sZGVmYXVsdDpmdW5jdGlvbih0KXtyZXR1cm5cImFuZCBcIi5jb25jYXQodCxcIiBtb3JlXCIpfX0sbG9hZGluZzp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITF9LGRpc2FibGVkOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0sb3BlbkRpcmVjdGlvbjp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIlwifSxzaG93Tm9PcHRpb25zOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0sc2hvd05vUmVzdWx0czp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITB9LHRhYmluZGV4Ont0eXBlOk51bWJlcixkZWZhdWx0OjB9fSxjb21wdXRlZDp7aXNTaW5nbGVMYWJlbFZpc2libGU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5zaW5nbGVWYWx1ZSYmKCF0aGlzLmlzT3Blbnx8IXRoaXMuc2VhcmNoYWJsZSkmJiF0aGlzLnZpc2libGVWYWx1ZXMubGVuZ3RofSxpc1BsYWNlaG9sZGVyVmlzaWJsZTpmdW5jdGlvbigpe3JldHVybiEodGhpcy5pbnRlcm5hbFZhbHVlLmxlbmd0aHx8dGhpcy5zZWFyY2hhYmxlJiZ0aGlzLmlzT3Blbil9LHZpc2libGVWYWx1ZXM6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5tdWx0aXBsZT90aGlzLmludGVybmFsVmFsdWUuc2xpY2UoMCx0aGlzLmxpbWl0KTpbXX0sc2luZ2xlVmFsdWU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5pbnRlcm5hbFZhbHVlWzBdfSxkZXNlbGVjdExhYmVsVGV4dDpmdW5jdGlvbigpe3JldHVybiB0aGlzLnNob3dMYWJlbHM/dGhpcy5kZXNlbGVjdExhYmVsOlwiXCJ9LGRlc2VsZWN0R3JvdXBMYWJlbFRleHQ6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5zaG93TGFiZWxzP3RoaXMuZGVzZWxlY3RHcm91cExhYmVsOlwiXCJ9LHNlbGVjdExhYmVsVGV4dDpmdW5jdGlvbigpe3JldHVybiB0aGlzLnNob3dMYWJlbHM/dGhpcy5zZWxlY3RMYWJlbDpcIlwifSxzZWxlY3RHcm91cExhYmVsVGV4dDpmdW5jdGlvbigpe3JldHVybiB0aGlzLnNob3dMYWJlbHM/dGhpcy5zZWxlY3RHcm91cExhYmVsOlwiXCJ9LHNlbGVjdGVkTGFiZWxUZXh0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuc2hvd0xhYmVscz90aGlzLnNlbGVjdGVkTGFiZWw6XCJcIn0saW5wdXRTdHlsZTpmdW5jdGlvbigpe2lmKHRoaXMuc2VhcmNoYWJsZXx8dGhpcy5tdWx0aXBsZSYmdGhpcy52YWx1ZSYmdGhpcy52YWx1ZS5sZW5ndGgpcmV0dXJuIHRoaXMuaXNPcGVuP3t3aWR0aDpcImF1dG9cIn06e3dpZHRoOlwiMFwiLHBvc2l0aW9uOlwiYWJzb2x1dGVcIixwYWRkaW5nOlwiMFwifX0sY29udGVudFN0eWxlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMub3B0aW9ucy5sZW5ndGg/e2Rpc3BsYXk6XCJpbmxpbmUtYmxvY2tcIn06e2Rpc3BsYXk6XCJibG9ja1wifX0saXNBYm92ZTpmdW5jdGlvbigpe3JldHVyblwiYWJvdmVcIj09PXRoaXMub3BlbkRpcmVjdGlvbnx8XCJ0b3BcIj09PXRoaXMub3BlbkRpcmVjdGlvbnx8XCJiZWxvd1wiIT09dGhpcy5vcGVuRGlyZWN0aW9uJiZcImJvdHRvbVwiIT09dGhpcy5vcGVuRGlyZWN0aW9uJiZcImFib3ZlXCI9PT10aGlzLnByZWZmZXJlZE9wZW5EaXJlY3Rpb259LHNob3dTZWFyY2hJbnB1dDpmdW5jdGlvbigpe3JldHVybiB0aGlzLnNlYXJjaGFibGUmJighdGhpcy5oYXNTaW5nbGVTZWxlY3RlZFNsb3R8fCF0aGlzLnZpc2libGVTaW5nbGVWYWx1ZSYmMCE9PXRoaXMudmlzaWJsZVNpbmdsZVZhbHVlfHx0aGlzLmlzT3Blbil9fX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEpKFwidW5zY29wYWJsZXNcIikscj1BcnJheS5wcm90b3R5cGU7dm9pZCAwPT1yW2ldJiZuKDgpKHIsaSx7fSksdC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JbaV1bdF09ITB9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxOCkscj1uKDE5KSxvPW4oODUpO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gZnVuY3Rpb24oZSxuLHMpe3ZhciB1LGE9aShlKSxsPXIoYS5sZW5ndGgpLGM9byhzLGwpO2lmKHQmJm4hPW4pe2Zvcig7bD5jOylpZigodT1hW2MrK10pIT11KXJldHVybiEwfWVsc2UgZm9yKDtsPmM7YysrKWlmKCh0fHxjIGluIGEpJiZhW2NdPT09bilyZXR1cm4gdHx8Y3x8MDtyZXR1cm4hdCYmLTF9fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oOSkscj1uKDEpKFwidG9TdHJpbmdUYWdcIiksbz1cIkFyZ3VtZW50c1wiPT1pKGZ1bmN0aW9uKCl7cmV0dXJuIGFyZ3VtZW50c30oKSkscz1mdW5jdGlvbih0LGUpe3RyeXtyZXR1cm4gdFtlXX1jYXRjaCh0KXt9fTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7dmFyIGUsbix1O3JldHVybiB2b2lkIDA9PT10P1wiVW5kZWZpbmVkXCI6bnVsbD09PXQ/XCJOdWxsXCI6XCJzdHJpbmdcIj09dHlwZW9mKG49cyhlPU9iamVjdCh0KSxyKSk/bjpvP2koZSk6XCJPYmplY3RcIj09KHU9aShlKSkmJlwiZnVuY3Rpb25cIj09dHlwZW9mIGUuY2FsbGVlP1wiQXJndW1lbnRzXCI6dX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDIpO3QuZXhwb3J0cz1mdW5jdGlvbigpe3ZhciB0PWkodGhpcyksZT1cIlwiO3JldHVybiB0Lmdsb2JhbCYmKGUrPVwiZ1wiKSx0Lmlnbm9yZUNhc2UmJihlKz1cImlcIiksdC5tdWx0aWxpbmUmJihlKz1cIm1cIiksdC51bmljb2RlJiYoZSs9XCJ1XCIpLHQuc3RpY2t5JiYoZSs9XCJ5XCIpLGV9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigwKS5kb2N1bWVudDt0LmV4cG9ydHM9aSYmaS5kb2N1bWVudEVsZW1lbnR9LGZ1bmN0aW9uKHQsZSxuKXt0LmV4cG9ydHM9IW4oNCkmJiFuKDcpKGZ1bmN0aW9uKCl7cmV0dXJuIDchPU9iamVjdC5kZWZpbmVQcm9wZXJ0eShuKDIxKShcImRpdlwiKSxcImFcIix7Z2V0OmZ1bmN0aW9uKCl7cmV0dXJuIDd9fSkuYX0pfSxmdW5jdGlvbih0LGUsbil7dmFyIGk9big5KTt0LmV4cG9ydHM9QXJyYXkuaXNBcnJheXx8ZnVuY3Rpb24odCl7cmV0dXJuXCJBcnJheVwiPT1pKHQpfX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGkodCl7dmFyIGUsbjt0aGlzLnByb21pc2U9bmV3IHQoZnVuY3Rpb24odCxpKXtpZih2b2lkIDAhPT1lfHx2b2lkIDAhPT1uKXRocm93IFR5cGVFcnJvcihcIkJhZCBQcm9taXNlIGNvbnN0cnVjdG9yXCIpO2U9dCxuPWl9KSx0aGlzLnJlc29sdmU9cihlKSx0aGlzLnJlamVjdD1yKG4pfXZhciByPW4oMTQpO3QuZXhwb3J0cy5mPWZ1bmN0aW9uKHQpe3JldHVybiBuZXcgaSh0KX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDIpLHI9big3Niksbz1uKDIyKSxzPW4oMjcpKFwiSUVfUFJPVE9cIiksdT1mdW5jdGlvbigpe30sYT1mdW5jdGlvbigpe3ZhciB0LGU9bigyMSkoXCJpZnJhbWVcIiksaT1vLmxlbmd0aDtmb3IoZS5zdHlsZS5kaXNwbGF5PVwibm9uZVwiLG4oNDApLmFwcGVuZENoaWxkKGUpLGUuc3JjPVwiamF2YXNjcmlwdDpcIix0PWUuY29udGVudFdpbmRvdy5kb2N1bWVudCx0Lm9wZW4oKSx0LndyaXRlKFwiPHNjcmlwdD5kb2N1bWVudC5GPU9iamVjdDxcXC9zY3JpcHQ+XCIpLHQuY2xvc2UoKSxhPXQuRjtpLS07KWRlbGV0ZSBhLnByb3RvdHlwZVtvW2ldXTtyZXR1cm4gYSgpfTt0LmV4cG9ydHM9T2JqZWN0LmNyZWF0ZXx8ZnVuY3Rpb24odCxlKXt2YXIgbjtyZXR1cm4gbnVsbCE9PXQ/KHUucHJvdG90eXBlPWkodCksbj1uZXcgdSx1LnByb3RvdHlwZT1udWxsLG5bc109dCk6bj1hKCksdm9pZCAwPT09ZT9uOnIobixlKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDc5KSxyPW4oMjUpLG89bigxOCkscz1uKDI5KSx1PW4oMTIpLGE9big0MSksbD1PYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yO2UuZj1uKDQpP2w6ZnVuY3Rpb24odCxlKXtpZih0PW8odCksZT1zKGUsITApLGEpdHJ5e3JldHVybiBsKHQsZSl9Y2F0Y2godCl7fWlmKHUodCxlKSlyZXR1cm4gcighaS5mLmNhbGwodCxlKSx0W2VdKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEyKSxyPW4oMTgpLG89bigzNykoITEpLHM9bigyNykoXCJJRV9QUk9UT1wiKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXt2YXIgbix1PXIodCksYT0wLGw9W107Zm9yKG4gaW4gdSluIT1zJiZpKHUsbikmJmwucHVzaChuKTtmb3IoO2UubGVuZ3RoPmE7KWkodSxuPWVbYSsrXSkmJih+byhsLG4pfHxsLnB1c2gobikpO3JldHVybiBsfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNDYpLHI9bigyMik7dC5leHBvcnRzPU9iamVjdC5rZXlzfHxmdW5jdGlvbih0KXtyZXR1cm4gaSh0LHIpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMikscj1uKDUpLG89big0Myk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7aWYoaSh0KSxyKGUpJiZlLmNvbnN0cnVjdG9yPT09dClyZXR1cm4gZTt2YXIgbj1vLmYodCk7cmV0dXJuKDAsbi5yZXNvbHZlKShlKSxuLnByb21pc2V9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxMCkscj1uKDApLG89cltcIl9fY29yZS1qc19zaGFyZWRfX1wiXXx8KHJbXCJfX2NvcmUtanNfc2hhcmVkX19cIl09e30pOyh0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtyZXR1cm4gb1t0XXx8KG9bdF09dm9pZCAwIT09ZT9lOnt9KX0pKFwidmVyc2lvbnNcIixbXSkucHVzaCh7dmVyc2lvbjppLnZlcnNpb24sbW9kZTpuKDI0KT9cInB1cmVcIjpcImdsb2JhbFwiLGNvcHlyaWdodDpcIsKpIDIwMTggRGVuaXMgUHVzaGthcmV2ICh6bG9pcm9jay5ydSlcIn0pfSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigyKSxyPW4oMTQpLG89bigxKShcInNwZWNpZXNcIik7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7dmFyIG4scz1pKHQpLmNvbnN0cnVjdG9yO3JldHVybiB2b2lkIDA9PT1zfHx2b2lkIDA9PShuPWkocylbb10pP2U6cihuKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDMpLHI9bigxNiksbz1uKDcpLHM9big4NCksdT1cIltcIitzK1wiXVwiLGE9XCLigIvChVwiLGw9UmVnRXhwKFwiXlwiK3UrdStcIipcIiksYz1SZWdFeHAodSt1K1wiKiRcIiksZj1mdW5jdGlvbih0LGUsbil7dmFyIHI9e30sdT1vKGZ1bmN0aW9uKCl7cmV0dXJuISFzW3RdKCl8fGFbdF0oKSE9YX0pLGw9clt0XT11P2UocCk6c1t0XTtuJiYocltuXT1sKSxpKGkuUCtpLkYqdSxcIlN0cmluZ1wiLHIpfSxwPWYudHJpbT1mdW5jdGlvbih0LGUpe3JldHVybiB0PVN0cmluZyhyKHQpKSwxJmUmJih0PXQucmVwbGFjZShsLFwiXCIpKSwyJmUmJih0PXQucmVwbGFjZShjLFwiXCIpKSx0fTt0LmV4cG9ydHM9Zn0sZnVuY3Rpb24odCxlLG4pe3ZhciBpLHIsbyxzPW4oMTEpLHU9big2OCksYT1uKDQwKSxsPW4oMjEpLGM9bigwKSxmPWMucHJvY2VzcyxwPWMuc2V0SW1tZWRpYXRlLGg9Yy5jbGVhckltbWVkaWF0ZSxkPWMuTWVzc2FnZUNoYW5uZWwsdj1jLkRpc3BhdGNoLGc9MCxtPXt9LHk9ZnVuY3Rpb24oKXt2YXIgdD0rdGhpcztpZihtLmhhc093blByb3BlcnR5KHQpKXt2YXIgZT1tW3RdO2RlbGV0ZSBtW3RdLGUoKX19LGI9ZnVuY3Rpb24odCl7eS5jYWxsKHQuZGF0YSl9O3AmJmh8fChwPWZ1bmN0aW9uKHQpe2Zvcih2YXIgZT1bXSxuPTE7YXJndW1lbnRzLmxlbmd0aD5uOyllLnB1c2goYXJndW1lbnRzW24rK10pO3JldHVybiBtWysrZ109ZnVuY3Rpb24oKXt1KFwiZnVuY3Rpb25cIj09dHlwZW9mIHQ/dDpGdW5jdGlvbih0KSxlKX0saShnKSxnfSxoPWZ1bmN0aW9uKHQpe2RlbGV0ZSBtW3RdfSxcInByb2Nlc3NcIj09big5KShmKT9pPWZ1bmN0aW9uKHQpe2YubmV4dFRpY2socyh5LHQsMSkpfTp2JiZ2Lm5vdz9pPWZ1bmN0aW9uKHQpe3Yubm93KHMoeSx0LDEpKX06ZD8ocj1uZXcgZCxvPXIucG9ydDIsci5wb3J0MS5vbm1lc3NhZ2U9YixpPXMoby5wb3N0TWVzc2FnZSxvLDEpKTpjLmFkZEV2ZW50TGlzdGVuZXImJlwiZnVuY3Rpb25cIj09dHlwZW9mIHBvc3RNZXNzYWdlJiYhYy5pbXBvcnRTY3JpcHRzPyhpPWZ1bmN0aW9uKHQpe2MucG9zdE1lc3NhZ2UodCtcIlwiLFwiKlwiKX0sYy5hZGRFdmVudExpc3RlbmVyKFwibWVzc2FnZVwiLGIsITEpKTppPVwib25yZWFkeXN0YXRlY2hhbmdlXCJpbiBsKFwic2NyaXB0XCIpP2Z1bmN0aW9uKHQpe2EuYXBwZW5kQ2hpbGQobChcInNjcmlwdFwiKSkub25yZWFkeXN0YXRlY2hhbmdlPWZ1bmN0aW9uKCl7YS5yZW1vdmVDaGlsZCh0aGlzKSx5LmNhbGwodCl9fTpmdW5jdGlvbih0KXtzZXRUaW1lb3V0KHMoeSx0LDEpLDApfSksdC5leHBvcnRzPXtzZXQ6cCxjbGVhcjpofX0sZnVuY3Rpb24odCxlKXt2YXIgbj1NYXRoLmNlaWwsaT1NYXRoLmZsb29yO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gaXNOYU4odD0rdCk/MDoodD4wP2k6bikodCl9fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGk9bigzKSxyPW4oMjApKDUpLG89ITA7XCJmaW5kXCJpbltdJiZBcnJheSgxKS5maW5kKGZ1bmN0aW9uKCl7bz0hMX0pLGkoaS5QK2kuRipvLFwiQXJyYXlcIix7ZmluZDpmdW5jdGlvbih0KXtyZXR1cm4gcih0aGlzLHQsYXJndW1lbnRzLmxlbmd0aD4xP2FyZ3VtZW50c1sxXTp2b2lkIDApfX0pLG4oMzYpKFwiZmluZFwiKX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpLHIsbyxzLHU9bigyNCksYT1uKDApLGw9bigxMSksYz1uKDM4KSxmPW4oMykscD1uKDUpLGg9bigxNCksZD1uKDYxKSx2PW4oNjYpLGc9big1MCksbT1uKDUyKS5zZXQseT1uKDc1KSgpLGI9big0MyksXz1uKDgwKSx4PW4oODYpLHc9big0OCksUz1hLlR5cGVFcnJvcixPPWEucHJvY2VzcyxMPU8mJk8udmVyc2lvbnMsUD1MJiZMLnY4fHxcIlwiLGs9YS5Qcm9taXNlLFQ9XCJwcm9jZXNzXCI9PWMoTyksRT1mdW5jdGlvbigpe30sVj1yPWIuZixBPSEhZnVuY3Rpb24oKXt0cnl7dmFyIHQ9ay5yZXNvbHZlKDEpLGU9KHQuY29uc3RydWN0b3I9e30pW24oMSkoXCJzcGVjaWVzXCIpXT1mdW5jdGlvbih0KXt0KEUsRSl9O3JldHVybihUfHxcImZ1bmN0aW9uXCI9PXR5cGVvZiBQcm9taXNlUmVqZWN0aW9uRXZlbnQpJiZ0LnRoZW4oRSlpbnN0YW5jZW9mIGUmJjAhPT1QLmluZGV4T2YoXCI2LjZcIikmJi0xPT09eC5pbmRleE9mKFwiQ2hyb21lLzY2XCIpfWNhdGNoKHQpe319KCksQz1mdW5jdGlvbih0KXt2YXIgZTtyZXR1cm4hKCFwKHQpfHxcImZ1bmN0aW9uXCIhPXR5cGVvZihlPXQudGhlbikpJiZlfSxqPWZ1bmN0aW9uKHQsZSl7aWYoIXQuX24pe3QuX249ITA7dmFyIG49dC5fYzt5KGZ1bmN0aW9uKCl7Zm9yKHZhciBpPXQuX3Yscj0xPT10Ll9zLG89MDtuLmxlbmd0aD5vOykhZnVuY3Rpb24oZSl7dmFyIG4sbyxzLHU9cj9lLm9rOmUuZmFpbCxhPWUucmVzb2x2ZSxsPWUucmVqZWN0LGM9ZS5kb21haW47dHJ5e3U/KHJ8fCgyPT10Ll9oJiYkKHQpLHQuX2g9MSksITA9PT11P249aTooYyYmYy5lbnRlcigpLG49dShpKSxjJiYoYy5leGl0KCkscz0hMCkpLG49PT1lLnByb21pc2U/bChTKFwiUHJvbWlzZS1jaGFpbiBjeWNsZVwiKSk6KG89QyhuKSk/by5jYWxsKG4sYSxsKTphKG4pKTpsKGkpfWNhdGNoKHQpe2MmJiFzJiZjLmV4aXQoKSxsKHQpfX0obltvKytdKTt0Ll9jPVtdLHQuX249ITEsZSYmIXQuX2gmJk4odCl9KX19LE49ZnVuY3Rpb24odCl7bS5jYWxsKGEsZnVuY3Rpb24oKXt2YXIgZSxuLGkscj10Ll92LG89RCh0KTtpZihvJiYoZT1fKGZ1bmN0aW9uKCl7VD9PLmVtaXQoXCJ1bmhhbmRsZWRSZWplY3Rpb25cIixyLHQpOihuPWEub251bmhhbmRsZWRyZWplY3Rpb24pP24oe3Byb21pc2U6dCxyZWFzb246cn0pOihpPWEuY29uc29sZSkmJmkuZXJyb3ImJmkuZXJyb3IoXCJVbmhhbmRsZWQgcHJvbWlzZSByZWplY3Rpb25cIixyKX0pLHQuX2g9VHx8RCh0KT8yOjEpLHQuX2E9dm9pZCAwLG8mJmUuZSl0aHJvdyBlLnZ9KX0sRD1mdW5jdGlvbih0KXtyZXR1cm4gMSE9PXQuX2gmJjA9PT0odC5fYXx8dC5fYykubGVuZ3RofSwkPWZ1bmN0aW9uKHQpe20uY2FsbChhLGZ1bmN0aW9uKCl7dmFyIGU7VD9PLmVtaXQoXCJyZWplY3Rpb25IYW5kbGVkXCIsdCk6KGU9YS5vbnJlamVjdGlvbmhhbmRsZWQpJiZlKHtwcm9taXNlOnQscmVhc29uOnQuX3Z9KX0pfSxNPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7ZS5fZHx8KGUuX2Q9ITAsZT1lLl93fHxlLGUuX3Y9dCxlLl9zPTIsZS5fYXx8KGUuX2E9ZS5fYy5zbGljZSgpKSxqKGUsITApKX0sRj1mdW5jdGlvbih0KXt2YXIgZSxuPXRoaXM7aWYoIW4uX2Qpe24uX2Q9ITAsbj1uLl93fHxuO3RyeXtpZihuPT09dCl0aHJvdyBTKFwiUHJvbWlzZSBjYW4ndCBiZSByZXNvbHZlZCBpdHNlbGZcIik7KGU9Qyh0KSk/eShmdW5jdGlvbigpe3ZhciBpPXtfdzpuLF9kOiExfTt0cnl7ZS5jYWxsKHQsbChGLGksMSksbChNLGksMSkpfWNhdGNoKHQpe00uY2FsbChpLHQpfX0pOihuLl92PXQsbi5fcz0xLGoobiwhMSkpfWNhdGNoKHQpe00uY2FsbCh7X3c6bixfZDohMX0sdCl9fX07QXx8KGs9ZnVuY3Rpb24odCl7ZCh0aGlzLGssXCJQcm9taXNlXCIsXCJfaFwiKSxoKHQpLGkuY2FsbCh0aGlzKTt0cnl7dChsKEYsdGhpcywxKSxsKE0sdGhpcywxKSl9Y2F0Y2godCl7TS5jYWxsKHRoaXMsdCl9fSxpPWZ1bmN0aW9uKHQpe3RoaXMuX2M9W10sdGhpcy5fYT12b2lkIDAsdGhpcy5fcz0wLHRoaXMuX2Q9ITEsdGhpcy5fdj12b2lkIDAsdGhpcy5faD0wLHRoaXMuX249ITF9LGkucHJvdG90eXBlPW4oODEpKGsucHJvdG90eXBlLHt0aGVuOmZ1bmN0aW9uKHQsZSl7dmFyIG49VihnKHRoaXMsaykpO3JldHVybiBuLm9rPVwiZnVuY3Rpb25cIiE9dHlwZW9mIHR8fHQsbi5mYWlsPVwiZnVuY3Rpb25cIj09dHlwZW9mIGUmJmUsbi5kb21haW49VD9PLmRvbWFpbjp2b2lkIDAsdGhpcy5fYy5wdXNoKG4pLHRoaXMuX2EmJnRoaXMuX2EucHVzaChuKSx0aGlzLl9zJiZqKHRoaXMsITEpLG4ucHJvbWlzZX0sY2F0Y2g6ZnVuY3Rpb24odCl7cmV0dXJuIHRoaXMudGhlbih2b2lkIDAsdCl9fSksbz1mdW5jdGlvbigpe3ZhciB0PW5ldyBpO3RoaXMucHJvbWlzZT10LHRoaXMucmVzb2x2ZT1sKEYsdCwxKSx0aGlzLnJlamVjdD1sKE0sdCwxKX0sYi5mPVY9ZnVuY3Rpb24odCl7cmV0dXJuIHQ9PT1rfHx0PT09cz9uZXcgbyh0KTpyKHQpfSksZihmLkcrZi5XK2YuRiohQSx7UHJvbWlzZTprfSksbigyNikoayxcIlByb21pc2VcIiksbig4MykoXCJQcm9taXNlXCIpLHM9bigxMCkuUHJvbWlzZSxmKGYuUytmLkYqIUEsXCJQcm9taXNlXCIse3JlamVjdDpmdW5jdGlvbih0KXt2YXIgZT1WKHRoaXMpO3JldHVybigwLGUucmVqZWN0KSh0KSxlLnByb21pc2V9fSksZihmLlMrZi5GKih1fHwhQSksXCJQcm9taXNlXCIse3Jlc29sdmU6ZnVuY3Rpb24odCl7cmV0dXJuIHcodSYmdGhpcz09PXM/azp0aGlzLHQpfX0pLGYoZi5TK2YuRiohKEEmJm4oNzMpKGZ1bmN0aW9uKHQpe2suYWxsKHQpLmNhdGNoKEUpfSkpLFwiUHJvbWlzZVwiLHthbGw6ZnVuY3Rpb24odCl7dmFyIGU9dGhpcyxuPVYoZSksaT1uLnJlc29sdmUscj1uLnJlamVjdCxvPV8oZnVuY3Rpb24oKXt2YXIgbj1bXSxvPTAscz0xO3YodCwhMSxmdW5jdGlvbih0KXt2YXIgdT1vKyssYT0hMTtuLnB1c2godm9pZCAwKSxzKyssZS5yZXNvbHZlKHQpLnRoZW4oZnVuY3Rpb24odCl7YXx8KGE9ITAsblt1XT10LC0tc3x8aShuKSl9LHIpfSksLS1zfHxpKG4pfSk7cmV0dXJuIG8uZSYmcihvLnYpLG4ucHJvbWlzZX0scmFjZTpmdW5jdGlvbih0KXt2YXIgZT10aGlzLG49VihlKSxpPW4ucmVqZWN0LHI9XyhmdW5jdGlvbigpe3YodCwhMSxmdW5jdGlvbih0KXtlLnJlc29sdmUodCkudGhlbihuLnJlc29sdmUsaSl9KX0pO3JldHVybiByLmUmJmkoci52KSxuLnByb21pc2V9fSl9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDMpLHI9bigxMCksbz1uKDApLHM9big1MCksdT1uKDQ4KTtpKGkuUCtpLlIsXCJQcm9taXNlXCIse2ZpbmFsbHk6ZnVuY3Rpb24odCl7dmFyIGU9cyh0aGlzLHIuUHJvbWlzZXx8by5Qcm9taXNlKSxuPVwiZnVuY3Rpb25cIj09dHlwZW9mIHQ7cmV0dXJuIHRoaXMudGhlbihuP2Z1bmN0aW9uKG4pe3JldHVybiB1KGUsdCgpKS50aGVuKGZ1bmN0aW9uKCl7cmV0dXJuIG59KX06dCxuP2Z1bmN0aW9uKG4pe3JldHVybiB1KGUsdCgpKS50aGVuKGZ1bmN0aW9uKCl7dGhyb3cgbn0pfTp0KX19KX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGkodCl7big5OSl9dmFyIHI9bigzNSksbz1uKDEwMSkscz1uKDEwMCksdT1pLGE9cyhyLmEsby5hLCExLHUsbnVsbCxudWxsKTtlLmE9YS5leHBvcnRzfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gaSh0LGUsbil7cmV0dXJuIGUgaW4gdD9PYmplY3QuZGVmaW5lUHJvcGVydHkodCxlLHt2YWx1ZTpuLGVudW1lcmFibGU6ITAsY29uZmlndXJhYmxlOiEwLHdyaXRhYmxlOiEwfSk6dFtlXT1uLHR9ZS5hPWl9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBpKHQpe3JldHVybihpPVwiZnVuY3Rpb25cIj09dHlwZW9mIFN5bWJvbCYmXCJzeW1ib2xcIj09dHlwZW9mIFN5bWJvbC5pdGVyYXRvcj9mdW5jdGlvbih0KXtyZXR1cm4gdHlwZW9mIHR9OmZ1bmN0aW9uKHQpe3JldHVybiB0JiZcImZ1bmN0aW9uXCI9PXR5cGVvZiBTeW1ib2wmJnQuY29uc3RydWN0b3I9PT1TeW1ib2wmJnQhPT1TeW1ib2wucHJvdG90eXBlP1wic3ltYm9sXCI6dHlwZW9mIHR9KSh0KX1mdW5jdGlvbiByKHQpe3JldHVybihyPVwiZnVuY3Rpb25cIj09dHlwZW9mIFN5bWJvbCYmXCJzeW1ib2xcIj09PWkoU3ltYm9sLml0ZXJhdG9yKT9mdW5jdGlvbih0KXtyZXR1cm4gaSh0KX06ZnVuY3Rpb24odCl7cmV0dXJuIHQmJlwiZnVuY3Rpb25cIj09dHlwZW9mIFN5bWJvbCYmdC5jb25zdHJ1Y3Rvcj09PVN5bWJvbCYmdCE9PVN5bWJvbC5wcm90b3R5cGU/XCJzeW1ib2xcIjppKHQpfSkodCl9ZS5hPXJ9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjtPYmplY3QuZGVmaW5lUHJvcGVydHkoZSxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KTt2YXIgaT1uKDM0KSxyPShuLm4oaSksbig1NSkpLG89KG4ubihyKSxuKDU2KSkscz0obi5uKG8pLG4oNTcpKSx1PW4oMzIpLGE9bigzMyk7bi5kKGUsXCJNdWx0aXNlbGVjdFwiLGZ1bmN0aW9uKCl7cmV0dXJuIHMuYX0pLG4uZChlLFwibXVsdGlzZWxlY3RNaXhpblwiLGZ1bmN0aW9uKCl7cmV0dXJuIHUuYX0pLG4uZChlLFwicG9pbnRlck1peGluXCIsZnVuY3Rpb24oKXtyZXR1cm4gYS5hfSksZS5kZWZhdWx0PXMuYX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4saSl7aWYoISh0IGluc3RhbmNlb2YgZSl8fHZvaWQgMCE9PWkmJmkgaW4gdCl0aHJvdyBUeXBlRXJyb3IobitcIjogaW5jb3JyZWN0IGludm9jYXRpb24hXCIpO3JldHVybiB0fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTQpLHI9bigyOCksbz1uKDIzKSxzPW4oMTkpO3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbix1LGEpe2koZSk7dmFyIGw9cih0KSxjPW8obCksZj1zKGwubGVuZ3RoKSxwPWE/Zi0xOjAsaD1hPy0xOjE7aWYobjwyKWZvcig7Oyl7aWYocCBpbiBjKXt1PWNbcF0scCs9aDticmVha31pZihwKz1oLGE/cDwwOmY8PXApdGhyb3cgVHlwZUVycm9yKFwiUmVkdWNlIG9mIGVtcHR5IGFycmF5IHdpdGggbm8gaW5pdGlhbCB2YWx1ZVwiKX1mb3IoO2E/cD49MDpmPnA7cCs9aClwIGluIGMmJih1PWUodSxjW3BdLHAsbCkpO3JldHVybiB1fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNSkscj1uKDQyKSxvPW4oMSkoXCJzcGVjaWVzXCIpO3QuZXhwb3J0cz1mdW5jdGlvbih0KXt2YXIgZTtyZXR1cm4gcih0KSYmKGU9dC5jb25zdHJ1Y3RvcixcImZ1bmN0aW9uXCIhPXR5cGVvZiBlfHxlIT09QXJyYXkmJiFyKGUucHJvdG90eXBlKXx8KGU9dm9pZCAwKSxpKGUpJiZudWxsPT09KGU9ZVtvXSkmJihlPXZvaWQgMCkpLHZvaWQgMD09PWU/QXJyYXk6ZX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDYzKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtyZXR1cm4gbmV3KGkodCkpKGUpfX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oOCkscj1uKDYpLG89big3KSxzPW4oMTYpLHU9bigxKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4pe3ZhciBhPXUodCksbD1uKHMsYSxcIlwiW3RdKSxjPWxbMF0sZj1sWzFdO28oZnVuY3Rpb24oKXt2YXIgZT17fTtyZXR1cm4gZVthXT1mdW5jdGlvbigpe3JldHVybiA3fSw3IT1cIlwiW3RdKGUpfSkmJihyKFN0cmluZy5wcm90b3R5cGUsdCxjKSxpKFJlZ0V4cC5wcm90b3R5cGUsYSwyPT1lP2Z1bmN0aW9uKHQsZSl7cmV0dXJuIGYuY2FsbCh0LHRoaXMsZSl9OmZ1bmN0aW9uKHQpe3JldHVybiBmLmNhbGwodCx0aGlzKX0pKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDExKSxyPW4oNzApLG89big2OSkscz1uKDIpLHU9bigxOSksYT1uKDg3KSxsPXt9LGM9e30sZT10LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4sZixwKXt2YXIgaCxkLHYsZyxtPXA/ZnVuY3Rpb24oKXtyZXR1cm4gdH06YSh0KSx5PWkobixmLGU/MjoxKSxiPTA7aWYoXCJmdW5jdGlvblwiIT10eXBlb2YgbSl0aHJvdyBUeXBlRXJyb3IodCtcIiBpcyBub3QgaXRlcmFibGUhXCIpO2lmKG8obSkpe2ZvcihoPXUodC5sZW5ndGgpO2g+YjtiKyspaWYoKGc9ZT95KHMoZD10W2JdKVswXSxkWzFdKTp5KHRbYl0pKT09PWx8fGc9PT1jKXJldHVybiBnfWVsc2UgZm9yKHY9bS5jYWxsKHQpOyEoZD12Lm5leHQoKSkuZG9uZTspaWYoKGc9cih2LHksZC52YWx1ZSxlKSk9PT1sfHxnPT09YylyZXR1cm4gZ307ZS5CUkVBSz1sLGUuUkVUVVJOPWN9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDUpLHI9big4Mikuc2V0O3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbil7dmFyIG8scz1lLmNvbnN0cnVjdG9yO3JldHVybiBzIT09biYmXCJmdW5jdGlvblwiPT10eXBlb2YgcyYmKG89cy5wcm90b3R5cGUpIT09bi5wcm90b3R5cGUmJmkobykmJnImJnIodCxvKSx0fX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4pe3ZhciBpPXZvaWQgMD09PW47c3dpdGNoKGUubGVuZ3RoKXtjYXNlIDA6cmV0dXJuIGk/dCgpOnQuY2FsbChuKTtjYXNlIDE6cmV0dXJuIGk/dChlWzBdKTp0LmNhbGwobixlWzBdKTtjYXNlIDI6cmV0dXJuIGk/dChlWzBdLGVbMV0pOnQuY2FsbChuLGVbMF0sZVsxXSk7Y2FzZSAzOnJldHVybiBpP3QoZVswXSxlWzFdLGVbMl0pOnQuY2FsbChuLGVbMF0sZVsxXSxlWzJdKTtjYXNlIDQ6cmV0dXJuIGk/dChlWzBdLGVbMV0sZVsyXSxlWzNdKTp0LmNhbGwobixlWzBdLGVbMV0sZVsyXSxlWzNdKX1yZXR1cm4gdC5hcHBseShuLGUpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMTUpLHI9bigxKShcIml0ZXJhdG9yXCIpLG89QXJyYXkucHJvdG90eXBlO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gdm9pZCAwIT09dCYmKGkuQXJyYXk9PT10fHxvW3JdPT09dCl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigyKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4scil7dHJ5e3JldHVybiByP2UoaShuKVswXSxuWzFdKTplKG4pfWNhdGNoKGUpe3ZhciBvPXQucmV0dXJuO3Rocm93IHZvaWQgMCE9PW8mJmkoby5jYWxsKHQpKSxlfX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDQ0KSxyPW4oMjUpLG89bigyNikscz17fTtuKDgpKHMsbigxKShcIml0ZXJhdG9yXCIpLGZ1bmN0aW9uKCl7cmV0dXJuIHRoaXN9KSx0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4pe3QucHJvdG90eXBlPWkocyx7bmV4dDpyKDEsbil9KSxvKHQsZStcIiBJdGVyYXRvclwiKX19LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDI0KSxyPW4oMyksbz1uKDYpLHM9big4KSx1PW4oMTUpLGE9big3MSksbD1uKDI2KSxjPW4oNzgpLGY9bigxKShcIml0ZXJhdG9yXCIpLHA9IShbXS5rZXlzJiZcIm5leHRcImluW10ua2V5cygpKSxoPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXN9O3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbixkLHYsZyxtKXthKG4sZSxkKTt2YXIgeSxiLF8seD1mdW5jdGlvbih0KXtpZighcCYmdCBpbiBMKXJldHVybiBMW3RdO3N3aXRjaCh0KXtjYXNlXCJrZXlzXCI6Y2FzZVwidmFsdWVzXCI6cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuIG5ldyBuKHRoaXMsdCl9fXJldHVybiBmdW5jdGlvbigpe3JldHVybiBuZXcgbih0aGlzLHQpfX0sdz1lK1wiIEl0ZXJhdG9yXCIsUz1cInZhbHVlc1wiPT12LE89ITEsTD10LnByb3RvdHlwZSxQPUxbZl18fExbXCJAQGl0ZXJhdG9yXCJdfHx2JiZMW3ZdLGs9UHx8eCh2KSxUPXY/Uz94KFwiZW50cmllc1wiKTprOnZvaWQgMCxFPVwiQXJyYXlcIj09ZT9MLmVudHJpZXN8fFA6UDtpZihFJiYoXz1jKEUuY2FsbChuZXcgdCkpKSE9PU9iamVjdC5wcm90b3R5cGUmJl8ubmV4dCYmKGwoXyx3LCEwKSxpfHxcImZ1bmN0aW9uXCI9PXR5cGVvZiBfW2ZdfHxzKF8sZixoKSksUyYmUCYmXCJ2YWx1ZXNcIiE9PVAubmFtZSYmKE89ITAsaz1mdW5jdGlvbigpe3JldHVybiBQLmNhbGwodGhpcyl9KSxpJiYhbXx8IXAmJiFPJiZMW2ZdfHxzKEwsZixrKSx1W2VdPWssdVt3XT1oLHYpaWYoeT17dmFsdWVzOlM/azp4KFwidmFsdWVzXCIpLGtleXM6Zz9rOngoXCJrZXlzXCIpLGVudHJpZXM6VH0sbSlmb3IoYiBpbiB5KWIgaW4gTHx8byhMLGIseVtiXSk7ZWxzZSByKHIuUCtyLkYqKHB8fE8pLGUseSk7cmV0dXJuIHl9fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxKShcIml0ZXJhdG9yXCIpLHI9ITE7dHJ5e3ZhciBvPVs3XVtpXSgpO28ucmV0dXJuPWZ1bmN0aW9uKCl7cj0hMH0sQXJyYXkuZnJvbShvLGZ1bmN0aW9uKCl7dGhyb3cgMn0pfWNhdGNoKHQpe310LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtpZighZSYmIXIpcmV0dXJuITE7dmFyIG49ITE7dHJ5e3ZhciBvPVs3XSxzPW9baV0oKTtzLm5leHQ9ZnVuY3Rpb24oKXtyZXR1cm57ZG9uZTpuPSEwfX0sb1tpXT1mdW5jdGlvbigpe3JldHVybiBzfSx0KG8pfWNhdGNoKHQpe31yZXR1cm4gbn19LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7cmV0dXJue3ZhbHVlOmUsZG9uZTohIXR9fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMCkscj1uKDUyKS5zZXQsbz1pLk11dGF0aW9uT2JzZXJ2ZXJ8fGkuV2ViS2l0TXV0YXRpb25PYnNlcnZlcixzPWkucHJvY2Vzcyx1PWkuUHJvbWlzZSxhPVwicHJvY2Vzc1wiPT1uKDkpKHMpO3QuZXhwb3J0cz1mdW5jdGlvbigpe3ZhciB0LGUsbixsPWZ1bmN0aW9uKCl7dmFyIGkscjtmb3IoYSYmKGk9cy5kb21haW4pJiZpLmV4aXQoKTt0Oyl7cj10LmZuLHQ9dC5uZXh0O3RyeXtyKCl9Y2F0Y2goaSl7dGhyb3cgdD9uKCk6ZT12b2lkIDAsaX19ZT12b2lkIDAsaSYmaS5lbnRlcigpfTtpZihhKW49ZnVuY3Rpb24oKXtzLm5leHRUaWNrKGwpfTtlbHNlIGlmKCFvfHxpLm5hdmlnYXRvciYmaS5uYXZpZ2F0b3Iuc3RhbmRhbG9uZSlpZih1JiZ1LnJlc29sdmUpe3ZhciBjPXUucmVzb2x2ZSh2b2lkIDApO249ZnVuY3Rpb24oKXtjLnRoZW4obCl9fWVsc2Ugbj1mdW5jdGlvbigpe3IuY2FsbChpLGwpfTtlbHNle3ZhciBmPSEwLHA9ZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoXCJcIik7bmV3IG8obCkub2JzZXJ2ZShwLHtjaGFyYWN0ZXJEYXRhOiEwfSksbj1mdW5jdGlvbigpe3AuZGF0YT1mPSFmfX1yZXR1cm4gZnVuY3Rpb24oaSl7dmFyIHI9e2ZuOmksbmV4dDp2b2lkIDB9O2UmJihlLm5leHQ9ciksdHx8KHQ9cixuKCkpLGU9cn19fSxmdW5jdGlvbih0LGUsbil7dmFyIGk9bigxMykscj1uKDIpLG89big0Nyk7dC5leHBvcnRzPW4oNCk/T2JqZWN0LmRlZmluZVByb3BlcnRpZXM6ZnVuY3Rpb24odCxlKXtyKHQpO2Zvcih2YXIgbixzPW8oZSksdT1zLmxlbmd0aCxhPTA7dT5hOylpLmYodCxuPXNbYSsrXSxlW25dKTtyZXR1cm4gdH19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDQ2KSxyPW4oMjIpLmNvbmNhdChcImxlbmd0aFwiLFwicHJvdG90eXBlXCIpO2UuZj1PYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lc3x8ZnVuY3Rpb24odCl7cmV0dXJuIGkodCxyKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDEyKSxyPW4oMjgpLG89bigyNykoXCJJRV9QUk9UT1wiKSxzPU9iamVjdC5wcm90b3R5cGU7dC5leHBvcnRzPU9iamVjdC5nZXRQcm90b3R5cGVPZnx8ZnVuY3Rpb24odCl7cmV0dXJuIHQ9cih0KSxpKHQsbyk/dFtvXTpcImZ1bmN0aW9uXCI9PXR5cGVvZiB0LmNvbnN0cnVjdG9yJiZ0IGluc3RhbmNlb2YgdC5jb25zdHJ1Y3Rvcj90LmNvbnN0cnVjdG9yLnByb3RvdHlwZTp0IGluc3RhbmNlb2YgT2JqZWN0P3M6bnVsbH19LGZ1bmN0aW9uKHQsZSl7ZS5mPXt9LnByb3BlcnR5SXNFbnVtZXJhYmxlfSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0KXt0cnl7cmV0dXJue2U6ITEsdjp0KCl9fWNhdGNoKHQpe3JldHVybntlOiEwLHY6dH19fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNik7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuKXtmb3IodmFyIHIgaW4gZSlpKHQscixlW3JdLG4pO3JldHVybiB0fX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oNSkscj1uKDIpLG89ZnVuY3Rpb24odCxlKXtpZihyKHQpLCFpKGUpJiZudWxsIT09ZSl0aHJvdyBUeXBlRXJyb3IoZStcIjogY2FuJ3Qgc2V0IGFzIHByb3RvdHlwZSFcIil9O3QuZXhwb3J0cz17c2V0Ok9iamVjdC5zZXRQcm90b3R5cGVPZnx8KFwiX19wcm90b19fXCJpbnt9P2Z1bmN0aW9uKHQsZSxpKXt0cnl7aT1uKDExKShGdW5jdGlvbi5jYWxsLG4oNDUpLmYoT2JqZWN0LnByb3RvdHlwZSxcIl9fcHJvdG9fX1wiKS5zZXQsMiksaSh0LFtdKSxlPSEodCBpbnN0YW5jZW9mIEFycmF5KX1jYXRjaCh0KXtlPSEwfXJldHVybiBmdW5jdGlvbih0LG4pe3JldHVybiBvKHQsbiksZT90Ll9fcHJvdG9fXz1uOmkodCxuKSx0fX0oe30sITEpOnZvaWQgMCksY2hlY2s6b319LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDApLHI9bigxMyksbz1uKDQpLHM9bigxKShcInNwZWNpZXNcIik7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3ZhciBlPWlbdF07byYmZSYmIWVbc10mJnIuZihlLHMse2NvbmZpZ3VyYWJsZTohMCxnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpc319KX19LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPVwiXFx0XFxuXFx2XFxmXFxyIMKg4ZqA4aCO4oCA4oCB4oCC4oCD4oCE4oCF4oCG4oCH4oCI4oCJ4oCK4oCv4oGf44CAXFx1MjAyOFxcdTIwMjlcXHVmZWZmXCJ9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDUzKSxyPU1hdGgubWF4LG89TWF0aC5taW47dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIHQ9aSh0KSx0PDA/cih0K2UsMCk6byh0LGUpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMCkscj1pLm5hdmlnYXRvcjt0LmV4cG9ydHM9ciYmci51c2VyQWdlbnR8fFwiXCJ9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgaT1uKDM4KSxyPW4oMSkoXCJpdGVyYXRvclwiKSxvPW4oMTUpO3QuZXhwb3J0cz1uKDEwKS5nZXRJdGVyYXRvck1ldGhvZD1mdW5jdGlvbih0KXtpZih2b2lkIDAhPXQpcmV0dXJuIHRbcl18fHRbXCJAQGl0ZXJhdG9yXCJdfHxvW2kodCldfX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPW4oMykscj1uKDIwKSgyKTtpKGkuUCtpLkYqIW4oMTcpKFtdLmZpbHRlciwhMCksXCJBcnJheVwiLHtmaWx0ZXI6ZnVuY3Rpb24odCl7cmV0dXJuIHIodGhpcyx0LGFyZ3VtZW50c1sxXSl9fSl9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDMpLHI9bigzNykoITEpLG89W10uaW5kZXhPZixzPSEhbyYmMS9bMV0uaW5kZXhPZigxLC0wKTwwO2koaS5QK2kuRiooc3x8IW4oMTcpKG8pKSxcIkFycmF5XCIse2luZGV4T2Y6ZnVuY3Rpb24odCl7cmV0dXJuIHM/by5hcHBseSh0aGlzLGFyZ3VtZW50cyl8fDA6cih0aGlzLHQsYXJndW1lbnRzWzFdKX19KX0sZnVuY3Rpb24odCxlLG4pe3ZhciBpPW4oMyk7aShpLlMsXCJBcnJheVwiLHtpc0FycmF5Om4oNDIpfSl9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT1uKDMpLHI9bigyMCkoMSk7aShpLlAraS5GKiFuKDE3KShbXS5tYXAsITApLFwiQXJyYXlcIix7bWFwOmZ1bmN0aW9uKHQpe3JldHVybiByKHRoaXMsdCxhcmd1bWVudHNbMV0pfX0pfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIGk9bigzKSxyPW4oNjIpO2koaS5QK2kuRiohbigxNykoW10ucmVkdWNlLCEwKSxcIkFycmF5XCIse3JlZHVjZTpmdW5jdGlvbih0KXtyZXR1cm4gcih0aGlzLHQsYXJndW1lbnRzLmxlbmd0aCxhcmd1bWVudHNbMV0sITEpfX0pfSxmdW5jdGlvbih0LGUsbil7dmFyIGk9RGF0ZS5wcm90b3R5cGUscj1pLnRvU3RyaW5nLG89aS5nZXRUaW1lO25ldyBEYXRlKE5hTikrXCJcIiE9XCJJbnZhbGlkIERhdGVcIiYmbig2KShpLFwidG9TdHJpbmdcIixmdW5jdGlvbigpe3ZhciB0PW8uY2FsbCh0aGlzKTtyZXR1cm4gdD09PXQ/ci5jYWxsKHRoaXMpOlwiSW52YWxpZCBEYXRlXCJ9KX0sZnVuY3Rpb24odCxlLG4pe24oNCkmJlwiZ1wiIT0vLi9nLmZsYWdzJiZuKDEzKS5mKFJlZ0V4cC5wcm90b3R5cGUsXCJmbGFnc1wiLHtjb25maWd1cmFibGU6ITAsZ2V0Om4oMzkpfSl9LGZ1bmN0aW9uKHQsZSxuKXtuKDY1KShcInNlYXJjaFwiLDEsZnVuY3Rpb24odCxlLG4pe3JldHVybltmdW5jdGlvbihuKXtcInVzZSBzdHJpY3RcIjt2YXIgaT10KHRoaXMpLHI9dm9pZCAwPT1uP3ZvaWQgMDpuW2VdO3JldHVybiB2b2lkIDAhPT1yP3IuY2FsbChuLGkpOm5ldyBSZWdFeHAobilbZV0oU3RyaW5nKGkpKX0sbl19KX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO24oOTQpO3ZhciBpPW4oMikscj1uKDM5KSxvPW4oNCkscz0vLi8udG9TdHJpbmcsdT1mdW5jdGlvbih0KXtuKDYpKFJlZ0V4cC5wcm90b3R5cGUsXCJ0b1N0cmluZ1wiLHQsITApfTtuKDcpKGZ1bmN0aW9uKCl7cmV0dXJuXCIvYS9iXCIhPXMuY2FsbCh7c291cmNlOlwiYVwiLGZsYWdzOlwiYlwifSl9KT91KGZ1bmN0aW9uKCl7dmFyIHQ9aSh0aGlzKTtyZXR1cm5cIi9cIi5jb25jYXQodC5zb3VyY2UsXCIvXCIsXCJmbGFnc1wiaW4gdD90LmZsYWdzOiFvJiZ0IGluc3RhbmNlb2YgUmVnRXhwP3IuY2FsbCh0KTp2b2lkIDApfSk6XCJ0b1N0cmluZ1wiIT1zLm5hbWUmJnUoZnVuY3Rpb24oKXtyZXR1cm4gcy5jYWxsKHRoaXMpfSl9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjtuKDUxKShcInRyaW1cIixmdW5jdGlvbih0KXtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4gdCh0aGlzLDMpfX0pfSxmdW5jdGlvbih0LGUsbil7Zm9yKHZhciBpPW4oMzQpLHI9big0Nyksbz1uKDYpLHM9bigwKSx1PW4oOCksYT1uKDE1KSxsPW4oMSksYz1sKFwiaXRlcmF0b3JcIiksZj1sKFwidG9TdHJpbmdUYWdcIikscD1hLkFycmF5LGg9e0NTU1J1bGVMaXN0OiEwLENTU1N0eWxlRGVjbGFyYXRpb246ITEsQ1NTVmFsdWVMaXN0OiExLENsaWVudFJlY3RMaXN0OiExLERPTVJlY3RMaXN0OiExLERPTVN0cmluZ0xpc3Q6ITEsRE9NVG9rZW5MaXN0OiEwLERhdGFUcmFuc2Zlckl0ZW1MaXN0OiExLEZpbGVMaXN0OiExLEhUTUxBbGxDb2xsZWN0aW9uOiExLEhUTUxDb2xsZWN0aW9uOiExLEhUTUxGb3JtRWxlbWVudDohMSxIVE1MU2VsZWN0RWxlbWVudDohMSxNZWRpYUxpc3Q6ITAsTWltZVR5cGVBcnJheTohMSxOYW1lZE5vZGVNYXA6ITEsTm9kZUxpc3Q6ITAsUGFpbnRSZXF1ZXN0TGlzdDohMSxQbHVnaW46ITEsUGx1Z2luQXJyYXk6ITEsU1ZHTGVuZ3RoTGlzdDohMSxTVkdOdW1iZXJMaXN0OiExLFNWR1BhdGhTZWdMaXN0OiExLFNWR1BvaW50TGlzdDohMSxTVkdTdHJpbmdMaXN0OiExLFNWR1RyYW5zZm9ybUxpc3Q6ITEsU291cmNlQnVmZmVyTGlzdDohMSxTdHlsZVNoZWV0TGlzdDohMCxUZXh0VHJhY2tDdWVMaXN0OiExLFRleHRUcmFja0xpc3Q6ITEsVG91Y2hMaXN0OiExfSxkPXIoaCksdj0wO3Y8ZC5sZW5ndGg7disrKXt2YXIgZyxtPWRbdl0seT1oW21dLGI9c1ttXSxfPWImJmIucHJvdG90eXBlO2lmKF8mJihfW2NdfHx1KF8sYyxwKSxfW2ZdfHx1KF8sZixtKSxhW21dPXAseSkpZm9yKGcgaW4gaSlfW2ddfHxvKF8sZyxpW2ddLCEwKX19LGZ1bmN0aW9uKHQsZSl7fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbixpLHIsbyl7dmFyIHMsdT10PXR8fHt9LGE9dHlwZW9mIHQuZGVmYXVsdDtcIm9iamVjdFwiIT09YSYmXCJmdW5jdGlvblwiIT09YXx8KHM9dCx1PXQuZGVmYXVsdCk7dmFyIGw9XCJmdW5jdGlvblwiPT10eXBlb2YgdT91Lm9wdGlvbnM6dTtlJiYobC5yZW5kZXI9ZS5yZW5kZXIsbC5zdGF0aWNSZW5kZXJGbnM9ZS5zdGF0aWNSZW5kZXJGbnMsbC5fY29tcGlsZWQ9ITApLG4mJihsLmZ1bmN0aW9uYWw9ITApLHImJihsLl9zY29wZUlkPXIpO3ZhciBjO2lmKG8/KGM9ZnVuY3Rpb24odCl7dD10fHx0aGlzLiR2bm9kZSYmdGhpcy4kdm5vZGUuc3NyQ29udGV4dHx8dGhpcy5wYXJlbnQmJnRoaXMucGFyZW50LiR2bm9kZSYmdGhpcy5wYXJlbnQuJHZub2RlLnNzckNvbnRleHQsdHx8XCJ1bmRlZmluZWRcIj09dHlwZW9mIF9fVlVFX1NTUl9DT05URVhUX198fCh0PV9fVlVFX1NTUl9DT05URVhUX18pLGkmJmkuY2FsbCh0aGlzLHQpLHQmJnQuX3JlZ2lzdGVyZWRDb21wb25lbnRzJiZ0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cy5hZGQobyl9LGwuX3NzclJlZ2lzdGVyPWMpOmkmJihjPWkpLGMpe3ZhciBmPWwuZnVuY3Rpb25hbCxwPWY/bC5yZW5kZXI6bC5iZWZvcmVDcmVhdGU7Zj8obC5faW5qZWN0U3R5bGVzPWMsbC5yZW5kZXI9ZnVuY3Rpb24odCxlKXtyZXR1cm4gYy5jYWxsKGUpLHAodCxlKX0pOmwuYmVmb3JlQ3JlYXRlPXA/W10uY29uY2F0KHAsYyk6W2NdfXJldHVybntlc01vZHVsZTpzLGV4cG9ydHM6dSxvcHRpb25zOmx9fX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciBpPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcyxlPXQuJGNyZWF0ZUVsZW1lbnQsbj10Ll9zZWxmLl9jfHxlO3JldHVybiBuKFwiZGl2XCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RcIixjbGFzczp7XCJtdWx0aXNlbGVjdC0tYWN0aXZlXCI6dC5pc09wZW4sXCJtdWx0aXNlbGVjdC0tZGlzYWJsZWRcIjp0LmRpc2FibGVkLFwibXVsdGlzZWxlY3QtLWFib3ZlXCI6dC5pc0Fib3ZlfSxhdHRyczp7dGFiaW5kZXg6dC5zZWFyY2hhYmxlPy0xOnQudGFiaW5kZXh9LG9uOntmb2N1czpmdW5jdGlvbihlKXt0LmFjdGl2YXRlKCl9LGJsdXI6ZnVuY3Rpb24oZSl7IXQuc2VhcmNoYWJsZSYmdC5kZWFjdGl2YXRlKCl9LGtleWRvd246W2Z1bmN0aW9uKGUpe3JldHVyblwiYnV0dG9uXCJpbiBlfHwhdC5fayhlLmtleUNvZGUsXCJkb3duXCIsNDAsZS5rZXksW1wiRG93blwiLFwiQXJyb3dEb3duXCJdKT9lLnRhcmdldCE9PWUuY3VycmVudFRhcmdldD9udWxsOihlLnByZXZlbnREZWZhdWx0KCksdm9pZCB0LnBvaW50ZXJGb3J3YXJkKCkpOm51bGx9LGZ1bmN0aW9uKGUpe3JldHVyblwiYnV0dG9uXCJpbiBlfHwhdC5fayhlLmtleUNvZGUsXCJ1cFwiLDM4LGUua2V5LFtcIlVwXCIsXCJBcnJvd1VwXCJdKT9lLnRhcmdldCE9PWUuY3VycmVudFRhcmdldD9udWxsOihlLnByZXZlbnREZWZhdWx0KCksdm9pZCB0LnBvaW50ZXJCYWNrd2FyZCgpKTpudWxsfSxmdW5jdGlvbihlKXtyZXR1cm5cImJ1dHRvblwiaW4gZXx8IXQuX2soZS5rZXlDb2RlLFwiZW50ZXJcIiwxMyxlLmtleSxcIkVudGVyXCIpfHwhdC5fayhlLmtleUNvZGUsXCJ0YWJcIiw5LGUua2V5LFwiVGFiXCIpPyhlLnN0b3BQcm9wYWdhdGlvbigpLGUudGFyZ2V0IT09ZS5jdXJyZW50VGFyZ2V0P251bGw6dm9pZCB0LmFkZFBvaW50ZXJFbGVtZW50KGUpKTpudWxsfV0sa2V5dXA6ZnVuY3Rpb24oZSl7aWYoIShcImJ1dHRvblwiaW4gZSkmJnQuX2soZS5rZXlDb2RlLFwiZXNjXCIsMjcsZS5rZXksXCJFc2NhcGVcIikpcmV0dXJuIG51bGw7dC5kZWFjdGl2YXRlKCl9fX0sW3QuX3QoXCJjYXJldFwiLFtuKFwiZGl2XCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX3NlbGVjdFwiLG9uOnttb3VzZWRvd246ZnVuY3Rpb24oZSl7ZS5wcmV2ZW50RGVmYXVsdCgpLGUuc3RvcFByb3BhZ2F0aW9uKCksdC50b2dnbGUoKX19fSldLHt0b2dnbGU6dC50b2dnbGV9KSx0Ll92KFwiIFwiKSx0Ll90KFwiY2xlYXJcIixudWxsLHtzZWFyY2g6dC5zZWFyY2h9KSx0Ll92KFwiIFwiKSxuKFwiZGl2XCIse3JlZjpcInRhZ3NcIixzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X190YWdzXCJ9LFt0Ll90KFwic2VsZWN0aW9uXCIsW24oXCJkaXZcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJzaG93XCIscmF3TmFtZTpcInYtc2hvd1wiLHZhbHVlOnQudmlzaWJsZVZhbHVlcy5sZW5ndGg+MCxleHByZXNzaW9uOlwidmlzaWJsZVZhbHVlcy5sZW5ndGggPiAwXCJ9XSxzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X190YWdzLXdyYXBcIn0sW3QuX2wodC52aXNpYmxlVmFsdWVzLGZ1bmN0aW9uKGUsaSl7cmV0dXJuW3QuX3QoXCJ0YWdcIixbbihcInNwYW5cIix7a2V5Omksc3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fdGFnXCJ9LFtuKFwic3BhblwiLHtkb21Qcm9wczp7dGV4dENvbnRlbnQ6dC5fcyh0LmdldE9wdGlvbkxhYmVsKGUpKX19KSx0Ll92KFwiIFwiKSxuKFwiaVwiLHtzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X190YWctaWNvblwiLGF0dHJzOntcImFyaWEtaGlkZGVuXCI6XCJ0cnVlXCIsdGFiaW5kZXg6XCIxXCJ9LG9uOntrZXlkb3duOmZ1bmN0aW9uKG4pe2lmKCEoXCJidXR0b25cImluIG4pJiZ0Ll9rKG4ua2V5Q29kZSxcImVudGVyXCIsMTMsbi5rZXksXCJFbnRlclwiKSlyZXR1cm4gbnVsbDtuLnByZXZlbnREZWZhdWx0KCksdC5yZW1vdmVFbGVtZW50KGUpfSxtb3VzZWRvd246ZnVuY3Rpb24obil7bi5wcmV2ZW50RGVmYXVsdCgpLHQucmVtb3ZlRWxlbWVudChlKX19fSldKV0se29wdGlvbjplLHNlYXJjaDp0LnNlYXJjaCxyZW1vdmU6dC5yZW1vdmVFbGVtZW50fSldfSldLDIpLHQuX3YoXCIgXCIpLHQuaW50ZXJuYWxWYWx1ZSYmdC5pbnRlcm5hbFZhbHVlLmxlbmd0aD50LmxpbWl0P1t0Ll90KFwibGltaXRcIixbbihcInN0cm9uZ1wiLHtzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X19zdHJvbmdcIixkb21Qcm9wczp7dGV4dENvbnRlbnQ6dC5fcyh0LmxpbWl0VGV4dCh0LmludGVybmFsVmFsdWUubGVuZ3RoLXQubGltaXQpKX19KV0pXTp0Ll9lKCldLHtzZWFyY2g6dC5zZWFyY2gscmVtb3ZlOnQucmVtb3ZlRWxlbWVudCx2YWx1ZXM6dC52aXNpYmxlVmFsdWVzLGlzT3Blbjp0LmlzT3Blbn0pLHQuX3YoXCIgXCIpLG4oXCJ0cmFuc2l0aW9uXCIse2F0dHJzOntuYW1lOlwibXVsdGlzZWxlY3RfX2xvYWRpbmdcIn19LFt0Ll90KFwibG9hZGluZ1wiLFtuKFwiZGl2XCIse2RpcmVjdGl2ZXM6W3tuYW1lOlwic2hvd1wiLHJhd05hbWU6XCJ2LXNob3dcIix2YWx1ZTp0LmxvYWRpbmcsZXhwcmVzc2lvbjpcImxvYWRpbmdcIn1dLHN0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX3NwaW5uZXJcIn0pXSldLDIpLHQuX3YoXCIgXCIpLHQuc2VhcmNoYWJsZT9uKFwiaW5wdXRcIix7cmVmOlwic2VhcmNoXCIsc3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9faW5wdXRcIixzdHlsZTp0LmlucHV0U3R5bGUsYXR0cnM6e25hbWU6dC5uYW1lLGlkOnQuaWQsdHlwZTpcInRleHRcIixhdXRvY29tcGxldGU6XCJvZmZcIixwbGFjZWhvbGRlcjp0LnBsYWNlaG9sZGVyLGRpc2FibGVkOnQuZGlzYWJsZWQsdGFiaW5kZXg6dC50YWJpbmRleH0sZG9tUHJvcHM6e3ZhbHVlOnQuc2VhcmNofSxvbjp7aW5wdXQ6ZnVuY3Rpb24oZSl7dC51cGRhdGVTZWFyY2goZS50YXJnZXQudmFsdWUpfSxmb2N1czpmdW5jdGlvbihlKXtlLnByZXZlbnREZWZhdWx0KCksdC5hY3RpdmF0ZSgpfSxibHVyOmZ1bmN0aW9uKGUpe2UucHJldmVudERlZmF1bHQoKSx0LmRlYWN0aXZhdGUoKX0sa2V5dXA6ZnVuY3Rpb24oZSl7aWYoIShcImJ1dHRvblwiaW4gZSkmJnQuX2soZS5rZXlDb2RlLFwiZXNjXCIsMjcsZS5rZXksXCJFc2NhcGVcIikpcmV0dXJuIG51bGw7dC5kZWFjdGl2YXRlKCl9LGtleWRvd246W2Z1bmN0aW9uKGUpe2lmKCEoXCJidXR0b25cImluIGUpJiZ0Ll9rKGUua2V5Q29kZSxcImRvd25cIiw0MCxlLmtleSxbXCJEb3duXCIsXCJBcnJvd0Rvd25cIl0pKXJldHVybiBudWxsO2UucHJldmVudERlZmF1bHQoKSx0LnBvaW50ZXJGb3J3YXJkKCl9LGZ1bmN0aW9uKGUpe2lmKCEoXCJidXR0b25cImluIGUpJiZ0Ll9rKGUua2V5Q29kZSxcInVwXCIsMzgsZS5rZXksW1wiVXBcIixcIkFycm93VXBcIl0pKXJldHVybiBudWxsO2UucHJldmVudERlZmF1bHQoKSx0LnBvaW50ZXJCYWNrd2FyZCgpfSxmdW5jdGlvbihlKXtyZXR1cm5cImJ1dHRvblwiaW4gZXx8IXQuX2soZS5rZXlDb2RlLFwiZW50ZXJcIiwxMyxlLmtleSxcIkVudGVyXCIpPyhlLnByZXZlbnREZWZhdWx0KCksZS5zdG9wUHJvcGFnYXRpb24oKSxlLnRhcmdldCE9PWUuY3VycmVudFRhcmdldD9udWxsOnZvaWQgdC5hZGRQb2ludGVyRWxlbWVudChlKSk6bnVsbH0sZnVuY3Rpb24oZSl7aWYoIShcImJ1dHRvblwiaW4gZSkmJnQuX2soZS5rZXlDb2RlLFwiZGVsZXRlXCIsWzgsNDZdLGUua2V5LFtcIkJhY2tzcGFjZVwiLFwiRGVsZXRlXCJdKSlyZXR1cm4gbnVsbDtlLnN0b3BQcm9wYWdhdGlvbigpLHQucmVtb3ZlTGFzdEVsZW1lbnQoKX1dfX0pOnQuX2UoKSx0Ll92KFwiIFwiKSx0LmlzU2luZ2xlTGFiZWxWaXNpYmxlP24oXCJzcGFuXCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX3NpbmdsZVwiLG9uOnttb3VzZWRvd246ZnVuY3Rpb24oZSl7cmV0dXJuIGUucHJldmVudERlZmF1bHQoKSx0LnRvZ2dsZShlKX19fSxbdC5fdChcInNpbmdsZUxhYmVsXCIsW1t0Ll92KHQuX3ModC5jdXJyZW50T3B0aW9uTGFiZWwpKV1dLHtvcHRpb246dC5zaW5nbGVWYWx1ZX0pXSwyKTp0Ll9lKCksdC5fdihcIiBcIiksdC5pc1BsYWNlaG9sZGVyVmlzaWJsZT9uKFwic3BhblwiLHtzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X19wbGFjZWhvbGRlclwiLG9uOnttb3VzZWRvd246ZnVuY3Rpb24oZSl7cmV0dXJuIGUucHJldmVudERlZmF1bHQoKSx0LnRvZ2dsZShlKX19fSxbdC5fdChcInBsYWNlaG9sZGVyXCIsW3QuX3YoXCJcXG4gICAgICAgICAgICBcIit0Ll9zKHQucGxhY2Vob2xkZXIpK1wiXFxuICAgICAgICBcIildKV0sMik6dC5fZSgpXSwyKSx0Ll92KFwiIFwiKSxuKFwidHJhbnNpdGlvblwiLHthdHRyczp7bmFtZTpcIm11bHRpc2VsZWN0XCJ9fSxbbihcImRpdlwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6dC5pc09wZW4sZXhwcmVzc2lvbjpcImlzT3BlblwifV0scmVmOlwibGlzdFwiLHN0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX2NvbnRlbnQtd3JhcHBlclwiLHN0eWxlOnttYXhIZWlnaHQ6dC5vcHRpbWl6ZWRIZWlnaHQrXCJweFwifSxhdHRyczp7dGFiaW5kZXg6XCItMVwifSxvbjp7Zm9jdXM6dC5hY3RpdmF0ZSxtb3VzZWRvd246ZnVuY3Rpb24odCl7dC5wcmV2ZW50RGVmYXVsdCgpfX19LFtuKFwidWxcIix7c3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fY29udGVudFwiLHN0eWxlOnQuY29udGVudFN0eWxlfSxbdC5fdChcImJlZm9yZUxpc3RcIiksdC5fdihcIiBcIiksdC5tdWx0aXBsZSYmdC5tYXg9PT10LmludGVybmFsVmFsdWUubGVuZ3RoP24oXCJsaVwiLFtuKFwic3BhblwiLHtzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X19vcHRpb25cIn0sW3QuX3QoXCJtYXhFbGVtZW50c1wiLFt0Ll92KFwiTWF4aW11bSBvZiBcIit0Ll9zKHQubWF4KStcIiBvcHRpb25zIHNlbGVjdGVkLiBGaXJzdCByZW1vdmUgYSBzZWxlY3RlZCBvcHRpb24gdG8gc2VsZWN0IGFub3RoZXIuXCIpXSldLDIpXSk6dC5fZSgpLHQuX3YoXCIgXCIpLCF0Lm1heHx8dC5pbnRlcm5hbFZhbHVlLmxlbmd0aDx0Lm1heD90Ll9sKHQuZmlsdGVyZWRPcHRpb25zLGZ1bmN0aW9uKGUsaSl7cmV0dXJuIG4oXCJsaVwiLHtrZXk6aSxzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X19lbGVtZW50XCJ9LFtlJiYoZS4kaXNMYWJlbHx8ZS4kaXNEaXNhYmxlZCk/dC5fZSgpOm4oXCJzcGFuXCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX29wdGlvblwiLGNsYXNzOnQub3B0aW9uSGlnaGxpZ2h0KGksZSksYXR0cnM6e1wiZGF0YS1zZWxlY3RcIjplJiZlLmlzVGFnP3QudGFnUGxhY2Vob2xkZXI6dC5zZWxlY3RMYWJlbFRleHQsXCJkYXRhLXNlbGVjdGVkXCI6dC5zZWxlY3RlZExhYmVsVGV4dCxcImRhdGEtZGVzZWxlY3RcIjp0LmRlc2VsZWN0TGFiZWxUZXh0fSxvbjp7Y2xpY2s6ZnVuY3Rpb24obil7bi5zdG9wUHJvcGFnYXRpb24oKSx0LnNlbGVjdChlKX0sbW91c2VlbnRlcjpmdW5jdGlvbihlKXtpZihlLnRhcmdldCE9PWUuY3VycmVudFRhcmdldClyZXR1cm4gbnVsbDt0LnBvaW50ZXJTZXQoaSl9fX0sW3QuX3QoXCJvcHRpb25cIixbbihcInNwYW5cIixbdC5fdih0Ll9zKHQuZ2V0T3B0aW9uTGFiZWwoZSkpKV0pXSx7b3B0aW9uOmUsc2VhcmNoOnQuc2VhcmNofSldLDIpLHQuX3YoXCIgXCIpLGUmJihlLiRpc0xhYmVsfHxlLiRpc0Rpc2FibGVkKT9uKFwic3BhblwiLHtzdGF0aWNDbGFzczpcIm11bHRpc2VsZWN0X19vcHRpb25cIixjbGFzczp0Lmdyb3VwSGlnaGxpZ2h0KGksZSksYXR0cnM6e1wiZGF0YS1zZWxlY3RcIjp0Lmdyb3VwU2VsZWN0JiZ0LnNlbGVjdEdyb3VwTGFiZWxUZXh0LFwiZGF0YS1kZXNlbGVjdFwiOnQuZ3JvdXBTZWxlY3QmJnQuZGVzZWxlY3RHcm91cExhYmVsVGV4dH0sb246e21vdXNlZW50ZXI6ZnVuY3Rpb24oZSl7aWYoZS50YXJnZXQhPT1lLmN1cnJlbnRUYXJnZXQpcmV0dXJuIG51bGw7dC5ncm91cFNlbGVjdCYmdC5wb2ludGVyU2V0KGkpfSxtb3VzZWRvd246ZnVuY3Rpb24obil7bi5wcmV2ZW50RGVmYXVsdCgpLHQuc2VsZWN0R3JvdXAoZSl9fX0sW3QuX3QoXCJvcHRpb25cIixbbihcInNwYW5cIixbdC5fdih0Ll9zKHQuZ2V0T3B0aW9uTGFiZWwoZSkpKV0pXSx7b3B0aW9uOmUsc2VhcmNoOnQuc2VhcmNofSldLDIpOnQuX2UoKV0pfSk6dC5fZSgpLHQuX3YoXCIgXCIpLG4oXCJsaVwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6dC5zaG93Tm9SZXN1bHRzJiYwPT09dC5maWx0ZXJlZE9wdGlvbnMubGVuZ3RoJiZ0LnNlYXJjaCYmIXQubG9hZGluZyxleHByZXNzaW9uOlwic2hvd05vUmVzdWx0cyAmJiAoZmlsdGVyZWRPcHRpb25zLmxlbmd0aCA9PT0gMCAmJiBzZWFyY2ggJiYgIWxvYWRpbmcpXCJ9XX0sW24oXCJzcGFuXCIse3N0YXRpY0NsYXNzOlwibXVsdGlzZWxlY3RfX29wdGlvblwifSxbdC5fdChcIm5vUmVzdWx0XCIsW3QuX3YoXCJObyBlbGVtZW50cyBmb3VuZC4gQ29uc2lkZXIgY2hhbmdpbmcgdGhlIHNlYXJjaCBxdWVyeS5cIildKV0sMildKSx0Ll92KFwiIFwiKSxuKFwibGlcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJzaG93XCIscmF3TmFtZTpcInYtc2hvd1wiLHZhbHVlOnQuc2hvd05vT3B0aW9ucyYmMD09PXQub3B0aW9ucy5sZW5ndGgmJiF0LnNlYXJjaCYmIXQubG9hZGluZyxleHByZXNzaW9uOlwic2hvd05vT3B0aW9ucyAmJiAob3B0aW9ucy5sZW5ndGggPT09IDAgJiYgIXNlYXJjaCAmJiAhbG9hZGluZylcIn1dfSxbbihcInNwYW5cIix7c3RhdGljQ2xhc3M6XCJtdWx0aXNlbGVjdF9fb3B0aW9uXCJ9LFt0Ll90KFwibm9PcHRpb25zXCIsW3QuX3YoXCJMaXN0IGlzIGVtcHR5LlwiKV0pXSwyKV0pLHQuX3YoXCIgXCIpLHQuX3QoXCJhZnRlckxpc3RcIildLDIpXSldKV0sMil9LHI9W10sbz17cmVuZGVyOmksc3RhdGljUmVuZGVyRm5zOnJ9O2UuYT1vfV0pfSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1tdWx0aXNlbGVjdC9kaXN0L3Z1ZS1tdWx0aXNlbGVjdC5taW4uanNcbi8vIG1vZHVsZSBpZCA9IDE5XG4vLyBtb2R1bGUgY2h1bmtzID0gMiA2IDE3IiwiaW1wb3J0IE11bHRpc2VsZWN0IGZyb20gJ3Z1ZS1tdWx0aXNlbGVjdCdcblxuVnVlLmNvbXBvbmVudCgnZGlhbG9nLW1vZGFsJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlJykpO1xuVnVlLmNvbXBvbmVudCgnc2VydmljZS1wYW5lbCcsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL3NlcnZpY2VzL1NlcnZpY2VQYW5lbC52dWUnKSk7XG5WdWUuY29tcG9uZW50KCdzZXJ2aWNlLWxpc3QnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9zZXJ2aWNlcy9TZXJ2aWNlcy52dWUnKSk7XG5WdWUuY29tcG9uZW50KCdkb2NzLWxpc3QnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9zZXJ2aWNlcy9Eb2N1bWVudExpc3QudnVlJykpO1xuXG5sZXQgZGF0YSA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XG5cbnZhciBhcHAgPSBuZXcgVnVlKHtcblx0ZWw6JyNzZXJ2aWNlcycsXG5cdGRhdGE6e1xuXHRcdHNlcnZpY2VzOltdLC8vIGxpc3Qgb2YgRXhpc3RpbmcgU2VydmljZXMgLVJFTU9WRSBUSElTXG5cdFx0c2VydmljZVBhcmVudHM6W10sLy8gbGlzdCBhbGwgcGFyZW50c1xuXHRcdHNlcnZpY2V0eXBlOlsnUGFyZW50JywgJ0NoaWxkJ10sXG5cdFx0YWRkT3JFZGl0TW9kYWxUaXRsZTonJyxcblx0ICAgXHRhZGRvckVkaXRGdW5jdGlvbjogJycsXG5cdCAgIFx0c2hvd0NoaWxkT3B0aW9uczogZmFsc2UsXG5cdCAgIFx0YWN0aXZlSWQ6JycsIC8vc2VsZWN0ZWQgSUQgb24gdGhlIHRhYmxlXG5cdCAgIFx0Y291bnRDaGlsZDonJyxcblx0ICAgXHRzZXJ2aWNlRG9jdW1lbnRzOltdLCAvL2xpc3Qgb2YgYWxsIHNlcnZpY2UgZG9jdW1lbnRzXG5cdCAgIFx0c2VsZWN0ZWREb2NzTmVlZGVkOltdLFxuXHQgICAgc2VsZWN0ZWREb2NzT3B0OltdLFxuXG4gICAgICAgIHNldHRpbmc6IGRhdGEuYWNjZXNzX2NvbnRyb2xbMF0uc2V0dGluZyxcbiAgICAgICAgcGVybWlzc2lvbnM6IGRhdGEucGVybWlzc2lvbnMsXG4gICAgICAgIHNlbHBlcm1pc3Npb25zOiBbe1xuICAgICAgICAgICAgYWRkTmV3U2VydmljZTowLFxuICAgICAgICAgICAgZWRpdFNlcnZpY2U6MCxcbiAgICAgICAgICAgIGRlbGV0ZVNlcnZpY2U6MFxuICAgICAgICB9XSxcblxuXHRcdC8vIEFERCAmIEVESVQgU0VSVklDRSBGT1JNXG5cdFx0YWRkU2VydkZybTogbmV3IEZvcm0oe1xuXHQgICAgICAgIGRldGFpbCBcdFx0XHQ6ICcnLFxuXHQgICAgICAgIGRldGFpbF9jblx0XHQ6ICcnLFxuXHQgICAgICAgIGRlc2NyaXB0aW9uIFx0OiAnJyxcblx0ICAgICAgICBkZXNjcmlwdGlvbl9jblx0OiAnJyxcblx0ICAgICAgICB0eXBlICAgXHRcdFx0OiAnJywgLy9wYXJlbnQgb3IgY2hpbGRcblx0ICAgICAgICBwYXJlbnRfaWRcdFx0OiAnJyxcblx0ICAgICAgICBjb3N0IFx0XHRcdDogJycsXG5cdCAgICAgICAgaWRcdCAgIFx0XHRcdDogJycsXG5cdCAgICAgICAgaXNfYWN0aXZlXHRcdDogJycsXG5cdCAgICAgICAgZG9jc19uZWVkZWRcdFx0OiBbXSxcblx0ICAgICAgICBkb2NzX29wdGlvbmFsXHQ6IFtdLFxuXHQgICAgICAgIGVzdGltYXRlZF9jb3N0XHQ6ICcnXG5cdCAgICB9LHsgYmFzZVVSTFx0XHRcdDogJy92aXNhL3NlcnZpY2UtbWFuYWdlcid9KSxcblxuXG5cdH0sXG5cdGNvbXBvbmVudHM6IHtcbiAgXHRcdE11bHRpc2VsZWN0XG4gIFx0fSxcblxuXHRtb3VudGVkKCkge1xuXHQgICAgJCgnI2FkZFNlcnZpY2UnKS5vbignaGlkZS5icy5tb2RhbCcsICgpID0+IHtcblx0ICAgICAgICAvL3RoaXMuY29tbW9uRnVuY3Rpb25SZWxvYWQoKTtcblx0ICAgIH0pO1xuXHQgICBcdCQoJyNkaWFsb2dSZW1vdmVTZXJ2aWNlJykub24oJ2hpZGUuYnMubW9kYWwnLCAoKSA9PiB7XG5cdCAgICAgICAgLy90aGlzLmNvbW1vbkZ1bmN0aW9uUmVsb2FkKCk7XG5cdCAgICB9KTtcblx0fSxcblxuXHRtZXRob2RzOiB7XG5cdFx0c2hvd1JlbW92ZVNlcnZpY2VEaWFsb2coKSB7XG5cdFx0XHQkKCcjZGlhbG9nUmVtb3ZlU2VydmljZScpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHR9LFxuXG5cdFx0YWRkT3JFZGl0KGQpe1xuXHRcdFx0aWYoZCA9PSBcImFkZFwiKXtcblx0XHRcdFx0dGhpcy5hZGRPckVkaXRNb2RhbFRpdGxlID0gXCJBZGQgU2VydmljZVwiO1xuXHRcdFx0XHR0aGlzLmFkZG9yRWRpdEZ1bmN0aW9uID0gJ2FkZCc7XG5cdFx0XHRcdHRoaXMuY29tbW9uRnVuY3Rpb25SZWxvYWQoKTtcdFx0XHRcblx0XHRcdFx0JCgnI2FkZFNlcnZpY2UnKS5tb2RhbCgndG9nZ2xlJyk7XG5cdFx0XHR9ZWxzZSBpZihkID09IFwiZWRpdFwiKXtcblx0XHRcdFx0Y29uc29sZS5sb2codGhpcy5hY3RpdmVJZCk7XG5cdFx0XHRcdHZhciBuZWVkZWQgPVtdO1xuXHRcdFx0XHR2YXIgb3B0aW9uYWwgPSBbXTtcblx0XHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvZ2V0U2VydmljZS8nKyB0aGlzLmFjdGl2ZUlkKVxuXHRcdFx0XHRcdC50aGVuKHJlc3VsdCA9Pntcblx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKHJlc3VsdC5kYXRhKTtcblx0XHRcdFx0XHQgICB0aGlzLmFkZFNlcnZGcm0uZGV0YWlsID0gcmVzdWx0LmRhdGEuZGV0YWlsO1xuXHRcdFx0XHRcdCAgIHRoaXMuYWRkU2VydkZybS5kZXRhaWxfY24gPSByZXN1bHQuZGF0YS5kZXRhaWxfY247XG5cdFx0XHRcdFx0ICAgdGhpcy5hZGRTZXJ2RnJtLmRlc2NyaXB0aW9uID0gcmVzdWx0LmRhdGEuZGVzY3JpcHRpb247XG5cdFx0XHRcdFx0ICAgdGhpcy5hZGRTZXJ2RnJtLmRlc2NyaXB0aW9uX2NuID0gcmVzdWx0LmRhdGEuZGVzY3JpcHRpb25fY247XG5cdFx0XHRcdFx0ICAgdGhpcy5hZGRTZXJ2RnJtLnBhcmVudF9pZCA9IHJlc3VsdC5kYXRhLnBhcmVudF9pZDtcblx0XHRcdFx0XHQgICB0aGlzLmFkZFNlcnZGcm0uY29zdCA9IHBhcnNlRmxvYXQocmVzdWx0LmRhdGEuY29zdCk7XG5cdFx0XHRcdFx0ICAgdGhpcy5hZGRTZXJ2RnJtLmVzdGltYXRlZF9jb3N0ID0gKHJlc3VsdC5kYXRhLmVzdGltYXRlZF9jb3N0KSBcblx0XHRcdFx0XHQgICBcdD8gcGFyc2VGbG9hdChyZXN1bHQuZGF0YS5lc3RpbWF0ZWRfY29zdClcblx0XHRcdFx0XHQgICBcdDogMDtcblx0XHRcdFx0XHQgICB0aGlzLmFkZFNlcnZGcm0uaWQgPSByZXN1bHQuZGF0YS5pZDtcblxuXHRcdFx0XHRcdCAgICBpZihyZXN1bHQuZGF0YS5zZWxlY3RfZG9jc19uZWVkZWQpe1xuXHRcdFx0XHRcdCAgIFx0XHRyZXN1bHQuZGF0YS5zZWxlY3RfZG9jc19uZWVkZWQubWFwKGZ1bmN0aW9uKHZhbHVlLCBrZXkpIHtcblx0XHRcdFx0XHRcdFx0XHRuZWVkZWQucHVzaCh2YWx1ZVswXSk7XG5cdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdCAgIFx0fVxuXHRcdFx0XHRcdCAgIFx0aWYocmVzdWx0LmRhdGEuc2VsZWN0X2RvY3Nfb3B0aW9uYWwpe1xuXHRcdFx0XHRcdFx0ICAgXHRyZXN1bHQuZGF0YS5zZWxlY3RfZG9jc19vcHRpb25hbC5tYXAoZnVuY3Rpb24odmFsdWUsIGtleSkge1xuXHRcdFx0XHRcdFx0XHRcdG9wdGlvbmFsLnB1c2godmFsdWVbMF0pO1xuXHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHQgICBcdCB9XG5cdFx0XHRcdFx0ICAgIHRoaXMuYWRkU2VydkZybS5kb2NzX25lZWRlZCA9IG5lZWRlZCxcblx0XHRcdFx0XHRcdHRoaXMuYWRkU2VydkZybS5kb2NzX29wdGlvbmFsID0gb3B0aW9uYWxcblxuXHRcdFx0XHRcdCAgIGlmKHJlc3VsdC5kYXRhLnBhcmVudF9pZCA9PSAwKXtcblx0XHRcdFx0XHQgICBcdFx0dGhpcy5hZGRTZXJ2RnJtLnR5cGUgPSAnUGFyZW50Jztcblx0XHRcdFx0XHQgICBcdFx0dGhpcy5zaG93Q2hpbGRPcHRpb25zID0gZmFsc2U7XG5cdFx0XHRcdFx0ICAgfWVsc2V7XG5cdFx0XHRcdFx0ICAgXHRcdHRoaXMuYWRkU2VydkZybS50eXBlID0gJ0NoaWxkJztcblx0XHRcdFx0XHQgICBcdFx0dGhpcy5zaG93Q2hpbGRPcHRpb25zID0gdHJ1ZTtcblx0XHRcdFx0XHQgICB9XG5cdFx0XHRcdH0pXG5cdFx0XHRcdFxuXHRcdFx0XHR0aGlzLmFkZE9yRWRpdE1vZGFsVGl0bGUgPSBcIkVkaXQgU2VydmljZVwiO1xuXHRcdFx0XHR0aGlzLmFkZG9yRWRpdEZ1bmN0aW9uID0gJ2VkaXQnO1xuXHRcdFx0XHQkKCcjYWRkU2VydmljZScpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHRcdH1cblx0XHR9LFxuXHRcdGNoYW5nZUFjdGl2ZUlkKGlkKXtcblx0XHRcdHRoaXMuYWN0aXZlSWQgPSBpZDtcblx0XHR9LFxuXHRcdHNhdmVGb3JtOmZ1bmN0aW9uIChldmVudCl7XG5cdFx0XHR2YXIgbmVlZGVkTGlzdD0nJztcblx0XHRcdHZhciBvcHRpb25hbExpc3Q9Jyc7XG5cdFx0XHR0aGlzLmFkZFNlcnZGcm0uZG9jc19uZWVkZWQubWFwKGZ1bmN0aW9uKGUpIHtcblx0XHRcdCAgICBpZihuZWVkZWRMaXN0ID09ICcnKXtcblx0XHRcdCAgICBcdG5lZWRlZExpc3QgPSBlLmlkXG5cdFx0XHQgICAgfWVsc2V7XG5cdFx0XHQgICAgXHRuZWVkZWRMaXN0ICs9IFwiLFwiICsgZS5pZDtcblx0XHRcdCAgICB9XG5cdFx0XHR9KTtcblxuXHRcdFx0dGhpcy5hZGRTZXJ2RnJtLmRvY3Nfb3B0aW9uYWwubWFwKGZ1bmN0aW9uKGUpIHtcblx0XHRcdCAgICBpZihvcHRpb25hbExpc3QgPT0gJycpe1xuXHRcdFx0ICAgIFx0b3B0aW9uYWxMaXN0ID0gZS5pZFxuXHRcdFx0ICAgIH1lbHNle1xuXHRcdFx0ICAgIFx0b3B0aW9uYWxMaXN0ICs9IFwiLFwiICsgZS5pZDtcblx0XHRcdCAgICB9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0dGhpcy5hZGRTZXJ2RnJtLmRvY3NfbmVlZGVkID0gbmVlZGVkTGlzdDtcblx0XHRcdHRoaXMuYWRkU2VydkZybS5kb2NzX29wdGlvbmFsID0gb3B0aW9uYWxMaXN0O1xuXHRcdFx0aWYodGhpcy5hZGRvckVkaXRGdW5jdGlvbiA9PSBcImFkZFwiKXtcblx0XHRcdFx0Ly8gY2hlY2sgaWYgXCJwYXJlbnRcIiBpcyBzZWxlY3RlZFxuXHRcdFx0XHRpZih0aGlzLmFkZFNlcnZGcm0udHlwZSA9PSBcIlBhcmVudFwiKXtcblx0XHRcdFx0XHR0aGlzLmFkZFNlcnZGcm0ucGFyZW50X2lkID0gMDtcblx0XHRcdFx0XHR0aGlzLmFkZFNlcnZGcm0uY29zdCA9IDA7XG5cdFx0XHRcdH1cblx0XHRcdFx0dGhpcy5hZGRTZXJ2RnJtLmlzX2FjdGl2ZSA9IDE7XG5cdFx0XHRcdC8vIHNhdmUgZm9ybVxuXHRcdFx0XHR0aGlzLmFkZFNlcnZGcm0uc3VibWl0KCdwb3N0JywgJy9zYXZlJylcblx0XHRcdCAgICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0ICAgIFx0dGhpcy5jb21tb25GdW5jdGlvblJlbG9hZCgpO1xuXHRcdFx0ICAgICAgICB0b2FzdHIuc3VjY2VzcygnTmV3IFNlcnZpY2UgQWRkZWQuJyk7XG5cdFx0XHQgICAgfSlcblx0XHRcdCAgICAuY2F0Y2goZXJyb3IgPT4ge1xuXHRcdFx0ICAgICAgICB0b2FzdHIuZXJyb3IoJ1NhdmluZyBGYWlsZWQuJyk7XG5cdFx0XHQgICAgfSk7XG5cdFx0XHQgICAgJCgnI2FkZFNlcnZpY2UnKS5tb2RhbCgndG9nZ2xlJyk7XG5cdFx0XHR9ZWxzZSBpZiAodGhpcy5hZGRvckVkaXRGdW5jdGlvbiA9PSBcImVkaXRcIil7XG5cdFx0XHRcdGlmKHRoaXMuYWRkU2VydkZybS50eXBlID09IFwiUGFyZW50XCIpe1xuXHRcdFx0XHRcdHRoaXMuYWRkU2VydkZybS5wYXJlbnRfaWQgPSAwO1xuXHRcdFx0XHRcdHRoaXMuYWRkU2VydkZybS5jb3N0ID0gMDtcblx0XHRcdFx0fWVsc2UgaWYodGhpcy5hZGRTZXJ2RnJtLnR5cGUgPT0gXCJDaGlsZFwiKXtcblx0XHRcdFx0XHRpZih0aGlzLmFkZFNlcnZGcm0ucGFyZW50X2lkID09ICcwJyl7XG5cdFx0XHRcdFx0XHR0aGlzLmFkZFNlcnZGcm0ucGFyZW50X2lkID0gJyc7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGlmKHRoaXMuYWRkU2VydkZybS5jb3N0ID09ICcwLjAwJyl7XG5cdFx0XHRcdFx0XHR0aGlzLmFkZFNlcnZGcm0uY29zdCA9ICcnO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHR0aGlzLmFkZFNlcnZGcm0uc3VibWl0KCdwb3N0JywgJy9lZGl0Jylcblx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcblx0XHRcdCAgICBcdHRoaXMuY29tbW9uRnVuY3Rpb25SZWxvYWQoKTtcblx0XHRcdCAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1NlcnZpY2UgVXBkYXRlZC4nKTtcblx0XHRcdCAgICB9KVxuXHRcdFx0ICAgIC5jYXRjaChlcnJvciA9PiB7XG5cdFx0XHQgICAgICAgIHRvYXN0ci5lcnJvcignU2F2aW5nIEZhaWxlZC4nKTtcblx0XHRcdCAgICB9KTtcblx0XHRcdCAgICAkKCcjYWRkU2VydmljZScpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHRcdH1cblx0XHRcdFxuXHRcdH0sXG5cblxuXHRcdC8vc2hvdyBjaGlsZCBvcHRpb25zIG9uIG1vZGFsXG5cdFx0Y2hhbmdlU2VydmljZVR5cGUoKXtcblx0XHRcdHZhciB0eXBlID0gdGhpcy5hZGRTZXJ2RnJtLnR5cGU7XG5cdFx0XHRpZih0eXBlID09IFwiUGFyZW50XCIpe1xuXHRcdFx0XHR0aGlzLnNob3dDaGlsZE9wdGlvbnMgPSBmYWxzZTtcblx0XHRcdH1lbHNlIGlmICh0eXBlID09IFwiQ2hpbGRcIil7XG5cdFx0XHRcdHRoaXMuc2hvd0NoaWxkT3B0aW9ucyA9IHRydWU7XG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdC8vcmVzZXQgYWRkL2VkaXQgc2VydmljZSBmb3JtXG5cdFx0ZW1wdHlmb3JtKCl7XG5cdFx0XHR0aGlzLmFjdGl2ZUlkID0gJyc7XG5cdFx0XHR0aGlzLnNob3dDaGlsZE9wdGlvbnMgPSBmYWxzZTtcblx0XHRcdHRoaXMuc2VsZWN0ZWREb2NzTmVlZGVkPVtdO1xuXHQgICAgXHR0aGlzLnNlbGVjdGVkRG9jc09wdD1bXTtcblx0XHRcdHRoaXMuYWRkU2VydkZybSA9IG5ldyBGb3JtKHtcblx0XHQgICAgICAgXHRkZXRhaWwgXHRcdFx0OiAnJyxcblx0XHQgICAgICAgIGRldGFpbF9jblx0XHQ6ICcnLFxuXHRcdCAgICAgICAgZGVzY3JpcHRpb24gXHQ6ICcnLFxuXHRcdCAgICAgICAgZGVzY3JpcHRpb25fY25cdDogJycsXG5cdFx0ICAgICAgICB0eXBlICAgXHRcdFx0OiAnJywgLy9wYXJlbnQgb3IgY2hpbGRcblx0XHQgICAgICAgIHBhcmVudF9pZFx0XHQ6ICcnLFxuXHRcdCAgICAgICAgY29zdCBcdFx0XHQ6ICcnLFxuXHRcdCAgICAgICAgaWRcdCAgIFx0XHRcdDogJycsXG5cdFx0ICAgICAgICBpc19hY3RpdmVcdFx0OiAnJyxcblx0XHQgICAgICAgIGRvY3NfbmVlZGVkXHRcdDogW10sXG5cdCAgICAgICAgXHRkb2NzX29wdGlvbmFsXHQ6IFtdLFxuXHQgICAgICAgIFx0ZXN0aW1hdGVkX2Nvc3RcdDogJydcblx0ICAgIFx0fSx7IGJhc2VVUkxcdFx0XHQ6ICcvdmlzYS9zZXJ2aWNlLW1hbmFnZXInfSlcdFxuXG5cblx0XHR9LFxuXG5cdFx0ZGVsZXRlU2VydmljZSgpe1xuXHRcdFx0dGhpcy5jb3VudENoaWxkcmVuKCk7XG5cdFx0XHRpZiggdGhpcy5jb3VudENoaWxkID09IDApe1xuXHRcdFx0XHRheGlvcy5nZXQoJy92aXNhL3NlcnZpY2UtbWFuYWdlci9kZWxldGUvJyArIHRoaXMuYWN0aXZlSWQpXG5cdFx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdCAgICAgICAgXHR0aGlzLmNvbW1vbkZ1bmN0aW9uUmVsb2FkKCk7XG5cdFx0XHRcdCAgICB0b2FzdHIuc3VjY2VzcygnU2VydmljZSBEZWxldGVkLicpO1xuXHRcdFx0ICAgIH0pXG5cdFx0XHQgICAgLmNhdGNoKGVycm9yID0+IHtcblx0XHRcdCAgICBcdHRoaXMuY29tbW9uRnVuY3Rpb25SZWxvYWQoKTtcblx0XHRcdCAgICAgICAgdG9hc3RyLmVycm9yKCdEZWxldGUgRmFpbGVkLicpO1xuXHRcdFx0ICAgIH0pO1xuXHRcdFx0fWVsc2V7XG5cdFx0XHRcdCB0b2FzdHIuZXJyb3IoJ0RlbGV0ZSBGYWlsZWQuIERlbGV0ZSBjaGlsZCBzZXJ2aWNlcyBmaXJzdC4nKTtcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0bG9hZFNlcnZpY2UoKXtcblx0XHRcdGF4aW9zLmdldCgnL3Zpc2Evc2VydmljZS1tYW5hZ2VyL3Nob3cnKVxuXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICB0aGlzLnNlcnZpY2VzID0gcmVzdWx0LmRhdGE7XG5cdCAgICBcdH0pO1xuXHRcdH0sXG5cblx0XHRsb2FkUGFyZW50cygpe1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvc2hvd1BhcmVudHMnKVxuXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICB0aGlzLnNlcnZpY2VQYXJlbnRzID0gcmVzdWx0LmRhdGE7XG5cdFx0XHQgIHRoaXMuY2FsbERhdGFUYWJsZXMoKTtcblx0ICAgIFx0fSk7XG5cdFx0fSxcblxuXHRcdGxvYWREb2NzKCl7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL3NlcnZpY2UtbWFuYWdlci9zZXJ2aWNlLWRvY3VtZW50cycpXG5cdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICAgIHRoaXMuc2VydmljZURvY3VtZW50cyA9IHJlc3VsdC5kYXRhO1xuXHQgICAgXHR9KTtcblx0XHR9LFxuXG5cdFx0Y29tbW9uRnVuY3Rpb25SZWxvYWQoKXtcblx0XHRcdHRoaXMuZW1wdHlmb3JtKCk7XG5cdCAgICAgICBcdHRoaXMubG9hZFNlcnZpY2UoKTtcblx0ICAgICAgIFx0dGhpcy5sb2FkUGFyZW50cygpO1xuXHRcdH0sXG5cblx0XHRyZWxvYWREYXRhVGFibGUoKXtcblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblx0XHRcdFx0JCgnI2xpc3RzJykuRGF0YVRhYmxlKHtcblx0XHQgICAgICAgICAgICBwYWdlTGVuZ3RoOiAxMCxcblx0XHQgICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxuXHRcdCAgICAgICAgICAgIGRvbTogJzxcInRvcFwibGY+cnQ8XCJib3R0b21cImlwPjxcImNsZWFyXCI+J1xuXHRcdCAgICAgICAgfSk7XG5cdFx0XHR9LDUwMCk7XG5cdFx0fSxcblxuXHRcdGNvdW50Q2hpbGRyZW4oaWQpe1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvc2hvd0NoaWxkcmVuLycgKyBpZClcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHQgICAgICAgIFx0dGhpcy5jb3VudENoaWxkID0gcmVzdWx0LmRhdGEubGVuZ3RoO1xuXHRcdFx0fSkgXG5cdFx0fSxcblxuXHRcdGNhbGxEYXRhVGFibGVzKCl7XG5cdFx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0XHRcdHZhciB2bSA9IHRoaXM7XG5cblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblxuXHRcdFx0XHRsZXQgdGFibGUgPSAkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuXHRcdCAgICAgICAgICAgIHBhZ2VMZW5ndGg6IDEwLFxuXHRcdCAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdFx0ICAgICAgICAgICAgZG9tOiAnPFwidG9wXCJsZj5yPFwiY2xlYXJcIj50PFwiYm90dG9tXCJwPicsXG5cdFx0ICAgICAgICAgICAgb3JkZXI6IFtbIDAsIFwiYXNjXCIgXV0sXG5cdFx0ICAgICAgICAgICAgc2Nyb2xsWTogXCI1MDBweFwiLFxuXHRcdCAgICAgICAgICAgIGF1dG9XaWR0aDogZmFsc2UsXG5cdFx0ICAgICAgICB9KTtcblxuXHRcdCAgICAgICAgJCgnI2xpc3RzIHRib2R5Jykub24oJ2NsaWNrJywgJy50b2dnbGUtcm93JywgZnVuY3Rpb24gKCkge1xuXHRcdCAgICAgICAgXHR2YXIgZGlzID0gJCh0aGlzKS5hdHRyKCdpZCcpO1xuXHRcdFx0ICAgICAgICB2YXIgdHIgPSAkKHRoaXMpLmNsb3Nlc3QoJ3RyJyk7XG5cdFx0XHQgICAgICAgIHZhciByb3cgPSB0YWJsZS5yb3coIHRyICk7XG5cdFx0XHQgXG5cdFx0XHQgICAgICAgIGlmKHJvdy5jaGlsZC5pc1Nob3duKCkpIHtcblx0XHRcdCAgICAgICAgICAgIHJvdy5jaGlsZC5oaWRlKCk7XG5cdFx0XHQgICAgICAgICAgICB0ci5yZW1vdmVDbGFzcygnc2hvd24nKTtcblx0XHRcdCAgICAgICAgfSBlbHNlIHtcblx0XHRcdFx0XHRcdHZhciBpZCA9IGRpcy5zcGxpdChcIl9cIik7XG5cdFx0XHQgICAgICAgIFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvc2hvd0NoaWxkcmVuLycgK2lkWzFdKVxuXHRcdFx0ICAgICAgICBcdC50aGVuKHJlc3VsdCA9PiB7XG5cdFx0XHQgICAgICAgIFx0XHR2YXIgcGFyZW50ID0gXCIjcGFyZW50X1wiK2lkWzFdO1xuXHRcdFx0XHRcdFx0XHQkKHBhcmVudCkuY2xvc2VzdChcInRkXCIpLmNsb3Nlc3QoXCJ0clwiKS5yZW1vdmUoKTtcblx0XHRcdCAgICAgICAgXHRcdCQocGFyZW50KS5jbG9zZXN0KFwidGRcIikucmVtb3ZlKCk7XG5cblx0XHRcdCAgICAgICAgXHRcdCQocGFyZW50KS5yZW1vdmUoKTtcblx0XHRcdCAgICAgICAgXHRcdCQocGFyZW50K1wiIHRyXCIpLnJlbW92ZSgpOyBcblx0XHRcdFx0ICAgICAgICAgICAgcm93LmNoaWxkKHZtLmZvcm1hdFJvdyhyZXN1bHQuZGF0YSxpZFsxXSkpLnNob3coKTtcblx0XHRcdCAgICAgICAgXHR9KVxuXHRcdFx0ICAgICAgICBcdHRyLmFkZENsYXNzKCdzaG93bicpO1xuXHRcdFx0ICAgICAgICB9XG5cdFx0XHQgICAgfSk7XG5cblx0XHRcdCB9LDEwMDApO1xuXHRcdH0sXG5cblx0XHRmb3JtYXRSb3coZCxwYXJlbnQpIHtcblx0XHRcdGlmKGQubGVuZ3RoID4gMCl7XG5cdFx0XHR2YXIgcmV0ID0gICc8dGFibGUgY2xhc3M9XCJ0YWJsZSB0YWJsZS1ib3JkZXJlZCB0YWJsZS1zdHJpcGVkIHRhYmxlLWhvdmVyZWRcIiBpZD1cInBhcmVudF8nK3BhcmVudCsnXCI+Jztcblx0XHRcdGZvcih2YXIgaT0gMDsgaTxkLmxlbmd0aDtpKyspe1xuXHRcdFx0XHR2YXIgYnRuID0nPGNlbnRlcj4nO1xuICAgICAgICAgICAgICAgIGJ0biArPSAnPGEgc3R5bGU9XCJtYXJnaW4tcmlnaHQ6OHB4O1wiIGNsYXNzPVwiZWRpdFNlcnZcIiBpZD1cInNlcnZfJytkW2ldLmlkKydcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiB0aXRsZT1cIkVkaXQgU2VydmljZVwiPic7XG4gICAgICAgICAgICAgICAgYnRuICs9ICc8aSBjbGFzcz1cImZhIGZhLXBlbmNpbCBmYS0xeFwiPjwvaT4nO1xuICAgICAgICAgICAgICAgIGJ0biArPSAnPC9hPic7XG4gICAgICAgICAgICAgICAgYnRuICs9ICc8YSBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiAgY2xhc3M9XCJkZWxldGVTZXJ2XCIgaWQ9XCJkc2Vydl8nK2RbaV0uaWQrJ1wiIHRpdGxlPVwiRGVsZXRlXCI+JztcbiAgICAgICAgICAgICAgICBidG4gKz0gJzxpIGNsYXNzPVwiZmEgZmEtdHJhc2ggZmEtMXhcIj48L2k+JztcbiAgICAgICAgICAgICAgICBidG4gKz0gJzwvYT4nO1xuICAgICAgICAgICAgXHRidG4gKz0nPC9jZW50ZXI+Jztcblx0XHRcdFx0cmV0ICs9Jzx0cj4nK1xuXHQgICAgICAgICAgICAnPHRkPjxwIHN0eWxlPVwicGFkZGluZy1sZWZ0OiAzMHB4OyBtYXJnaW4tYm90dG9tOiAwcHg7XCI+PiAnK2RbaV0uZGV0YWlsKyc8L3A+PC90ZD4nK1xuXHQgICAgICAgICAgICAnPHRkPicrZFtpXS5kZXNjcmlwdGlvbisnPC90ZD4nK1xuXHQgICAgICAgICAgICAnPHRkPicrZFtpXS5jb3N0Kyc8L3RkPicrXG5cdCAgICAgICAgICAgICc8dGQ+JytidG4rJzwvdGQ+Jytcblx0ICAgICAgICBcdCc8L3RyPic7XG5cdCAgICAgICAgfVxuICAgICAgICBcdHJldCs9JzwvdGFibGU+Jztcblx0XHRcdHJldHVybiByZXQ7ICAgICAgICBcblx0XHRcdH1cblx0XHR9XG5cdH0sXG5cdGNyZWF0ZWQoKXtcblxuICAgICAgICB2YXIgeCA9IHRoaXMucGVybWlzc2lvbnMuZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoudHlwZSA9PT0gJ1NlcnZpY2UgTWFuYWdlcid9KTtcblxuICAgICAgICB2YXIgeSA9IHguZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoubmFtZSA9PT0gJ0FkZCBOZXcgU2VydmljZSd9KTtcblxuICAgICAgICBpZih5Lmxlbmd0aD09MCkgdGhpcy5zZWxwZXJtaXNzaW9ucy5hZGROZXdTZXJ2aWNlID0gMDtcbiAgICAgICAgZWxzZSB0aGlzLnNlbHBlcm1pc3Npb25zLmFkZE5ld1NlcnZpY2UgPSAxO1xuXG4gICAgICAgIHZhciB5ID0geC5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai5uYW1lID09PSAnRWRpdCBTZXJ2aWNlJ30pO1xuXG4gICAgICAgIGlmKHkubGVuZ3RoPT0wKSB0aGlzLnNlbHBlcm1pc3Npb25zLmVkaXRTZXJ2aWNlID0gMDtcbiAgICAgICAgZWxzZSB0aGlzLnNlbHBlcm1pc3Npb25zLmVkaXRTZXJ2aWNlID0gMTtcblxuICAgICAgICB2YXIgeSA9IHguZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoubmFtZSA9PT0gJ0RlbGV0ZSBTZXJ2aWNlJ30pO1xuXG4gICAgICAgIGlmKHkubGVuZ3RoPT0wKSB0aGlzLnNlbHBlcm1pc3Npb25zLmRlbGV0ZVNlcnZpY2UgPSAwO1xuICAgICAgICBlbHNlIHRoaXMuc2VscGVybWlzc2lvbnMuZGVsZXRlU2VydmljZSA9IDE7XG5cblx0XHR0aGlzLmxvYWRTZXJ2aWNlKCk7XG5cdFx0dGhpcy5sb2FkUGFyZW50cygpO1xuXHRcdHRoaXMubG9hZERvY3MoKTtcblx0XHR0aGlzLmNhbGxEYXRhVGFibGVzKCk7XG5cdH0sXG5cdC8vIHVwZGF0ZWQoKSB7XG5cdC8vIFx0dGhpcy5jYWxsRGF0YVRhYmxlcygpO1xuXHQvLyB9XG5cdC8vIHdhdGNoOntcblx0Ly8gXHRzZXJ2aWNlOmZ1bmN0aW9uKCl7XG5cdC8vICAgXHRcdCQoJy4jbGlzdHMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cdC8vICAgXHRcdHRoaXMucmVsb2FkRGF0YVRhYmxlKCk7XG5cdC8vIFx0fSxcblxuXHQvLyB9LFxufSk7XG5cbnZhciBhcHAyID0gbmV3IFZ1ZSh7XG5cdGVsOicjZG9jdW1lbnRzJyxcblx0ZGF0YTp7XG5cdFx0ZG9jdW1lbnRzOltdLFxuXHRcdGFjdDonJyxcblx0XHRzZWxfaWQ6JycsXG5cbiAgICAgICAgc2V0dGluZzogZGF0YS5hY2Nlc3NfY29udHJvbFswXS5zZXR0aW5nLFxuICAgICAgICBwZXJtaXNzaW9uczogZGF0YS5wZXJtaXNzaW9ucyxcbiAgICAgICAgc2VscGVybWlzc2lvbnM6IFt7XG4gICAgICAgICAgICBhZGROZXdEb2N1bWVudDowLFxuICAgICAgICAgICAgZWRpdERvY3VtZW50OjAsXG4gICAgICAgICAgICBkZWxldGVEb2N1bWVudDowXG4gICAgICAgIH1dLFxuXG5cdFx0ZG9jc0ZybTogbmV3IEZvcm0oe1xuXHQgICAgICAgXHRpZCBcdFx0OiAnJyxcblx0XHQgICAgdGl0bGUgXHQ6ICcnLFxuXHRcdCAgIFx0dGl0bGVfY246ICcnLFxuXHQgICAgfSx7IGJhc2VVUkxcdDogJy92aXNhL3NlcnZpY2UtbWFuYWdlci9zZXJ2aWNlLWRvY3VtZW50cyd9KVxuXHR9LFxuXHRtZXRob2RzOntcblx0XHRsb2FkRG9jcygpe1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvc2VydmljZS1kb2N1bWVudHMnKVxuXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICB0aGlzLmRvY3VtZW50cyA9IHJlc3VsdC5kYXRhO1xuXHQgICAgXHR9KTtcblx0XHR9LFxuXHRcdHJlbG9hZERhdGFUYWJsZSgpe1xuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xuXHRcdFx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuXHRcdCAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdFx0ICAgICAgICB9KTtcblx0XHRcdH0sNTAwKTtcblx0XHR9LFxuXHRcdHNhdmVEb2NzKGUpe1xuXHRcdCAgIFx0aWYoZSA9PSBcIkFkZFwiKXtcblx0XHRcdFx0dGhpcy5kb2NzRnJtLnN1Ym1pdCgncG9zdCcsICcvYWRkJylcblx0XHRcdCAgICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0ICAgICAgICB0b2FzdHIuc3VjY2VzcygnTmV3IFNlcnZpY2UgRG9jdW1lbnQgQWRkZWQuJyk7XG5cdFx0XHQgICAgICAgICQoJyNkaWFsb2dBZGREb2N1bWVudCcpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHRcdCAgICAgICAgdGhpcy5yZWxvYWRBbGwoKTtcblx0XHRcdCAgICB9KVxuXHRcdFx0ICAgIC5jYXRjaChlcnJvciA9PiB7XG5cdFx0XHQgICAgICAgIHRvYXN0ci5lcnJvcignU2F2aW5nIEZhaWxlZC4nKTtcblx0XHRcdCAgICB9KTtcblx0XHRcdH1lbHNlIGlmIChlID09IFwiRWRpdFwiKXtcblx0XHRcdFx0dGhpcy5kb2NzRnJtLnN1Ym1pdCgncG9zdCcsICcvZWRpdCcpXG5cdFx0XHQgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0XHRcdCAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1NlcnZpY2UgRG9jdW1lbnQgVXBkYXRlZCcpO1xuXHRcdFx0ICAgICAgICAkKCcjZGlhbG9nQWRkRG9jdW1lbnQnKS5tb2RhbCgndG9nZ2xlJyk7XG5cdFx0XHQgICAgICAgIHRoaXMucmVsb2FkQWxsKCk7XG5cdFx0XHQgICAgfSlcblx0XHRcdCAgICAuY2F0Y2goZXJyb3IgPT4ge1xuXHRcdFx0ICAgICAgICB0b2FzdHIuZXJyb3IoJ1NhdmluZyBGYWlsZWQuJyk7XG5cdFx0XHQgICAgfSk7XG5cdFx0XHR9ZWxzZSBpZiAoZSA9PSBcIkRlbGV0ZVwiKXtcblx0XHRcdFx0YXhpb3MucG9zdCgnL3Zpc2Evc2VydmljZS1tYW5hZ2VyL3NlcnZpY2UtZG9jdW1lbnRzL2RlbGV0ZS8nKyB0aGlzLnNlbF9pZClcblx0XHRcdCAgICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0ICAgICAgICB0b2FzdHIuc3VjY2VzcygnU2VydmljZSBEb2N1bWVudCBEZWxldGVkJyk7XG5cdFx0XHQgICAgICAgIHRoaXMucmVsb2FkQWxsKCk7XG5cdFx0XHQgICAgfSlcblx0XHRcdCAgICAuY2F0Y2goZXJyb3IgPT4ge1xuXHRcdFx0ICAgICAgICB0b2FzdHIuZXJyb3IoJ0RlbGV0ZSBGYWlsZWQuJyk7XG5cdFx0XHQgICAgfSk7XG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdGFjdGlvbkRvYyhlLCBpZCl7XG5cdFx0XHRpZihlID09IFwiYWRkXCIpe1xuXHRcdFx0XHQkKCcjZGlhbG9nQWRkRG9jdW1lbnQnKS5tb2RhbCgndG9nZ2xlJyk7XG5cdFx0XHRcdHRoaXMuYWN0ID0gXCJBZGRcIjtcblx0XHRcdFx0Y29uc29sZS5sb2coaWQpO1xuXHRcdFx0XHRjb25zb2xlLmxvZyhlKTtcblx0XHRcdH1lbHNlIGlmIChlID09IFwiZWRpdFwiKXtcblx0XHRcdFx0dGhpcy5hY3QgPSBcIkVkaXRcIjtcblx0XHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvc2VydmljZS1kb2N1bWVudHMvaW5mby8nKyBpZClcblx0XHRcdFx0XHQudGhlbihyZXN1bHQgPT57XG5cdFx0XHRcdFx0dGhpcy5kb2NzRnJtLmlkID0gcmVzdWx0LmRhdGEuaWQ7XG5cdFx0XHRcdFx0dGhpcy5kb2NzRnJtLnRpdGxlID0gcmVzdWx0LmRhdGEudGl0bGU7XG5cdFx0XHRcdFx0dGhpcy5kb2NzRnJtLnRpdGxlX2NuID0gcmVzdWx0LmRhdGEudGl0bGVfY247XG5cdFx0XHRcdH0pXG5cdFx0XHRcdCQoJyNkaWFsb2dBZGREb2N1bWVudCcpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHRcdH1lbHNlIGlmIChlID09IFwiZGVsZXRlXCIpe1xuXHRcdFx0XHR0aGlzLmFjdCA9IFwiRGVsZXRlXCI7XG5cdFx0XHRcdHRoaXMuc2VsX2lkID0gaWQ7XG5cdFx0XHRcdCQoJyNkaWFsb2dSZW1vdmVEb2N1bWVudCcpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0cmVzZXREb2NGcm0oKXtcblx0XHRcdHRoaXMuZG9jc0ZybSA9IG5ldyBGb3JtKHtcblx0XHRcdFx0aWQgXHRcdDogJycsXG5cdFx0ICAgICAgICB0aXRsZSBcdDogJycsXG5cdFx0ICAgICAgICB0aXRsZV9jbjogJycsXG5cdFx0ICAgIH0seyBiYXNlVVJMXHQ6ICcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvc2VydmljZS1kb2N1bWVudHMnfSlcblx0XHR9LFxuXHRcdHJlbG9hZEFsbCgpe1xuXHRcdFx0JCgnI2xpc3RzJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuXHRcdFx0dGhpcy5yZXNldERvY0ZybSgpO1xuXHRcdFx0dGhpcy5sb2FkRG9jcygpO1xuXHRcdFx0dGhpcy5yZWxvYWREYXRhVGFibGUoKTtcblx0XHR9XG5cdH0sXG5cdGNyZWF0ZWQoKXtcblxuICAgICAgICB2YXIgeCA9IHRoaXMucGVybWlzc2lvbnMuZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoudHlwZSA9PT0gJ1NlcnZpY2UgTWFuYWdlcid9KTtcblxuICAgICAgICB2YXIgeSA9IHguZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoubmFtZSA9PT0gJ0FkZCBOZXcgRG9jdW1lbnQnfSk7XG5cbiAgICAgICAgaWYoeS5sZW5ndGg9PTApIHRoaXMuc2VscGVybWlzc2lvbnMuYWRkTmV3RG9jdW1lbnQgPSAwO1xuICAgICAgICBlbHNlIHRoaXMuc2VscGVybWlzc2lvbnMuYWRkTmV3RG9jdW1lbnQgPSAxO1xuXG4gICAgICAgIHZhciB5ID0geC5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai5uYW1lID09PSAnRWRpdCBEb2N1bWVudCd9KTtcblxuICAgICAgICBpZih5Lmxlbmd0aD09MCkgdGhpcy5zZWxwZXJtaXNzaW9ucy5lZGl0RG9jdW1lbnQgPSAwO1xuICAgICAgICBlbHNlIHRoaXMuc2VscGVybWlzc2lvbnMuZWRpdERvY3VtZW50ID0gMTtcblxuICAgICAgICB2YXIgeSA9IHguZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoubmFtZSA9PT0gJ0RlbGV0ZSBEb2N1bWVudCd9KTtcblxuICAgICAgICBpZih5Lmxlbmd0aD09MCkgdGhpcy5zZWxwZXJtaXNzaW9ucy5kZWxldGVEb2N1bWVudCA9IDA7XG4gICAgICAgIGVsc2UgdGhpcy5zZWxwZXJtaXNzaW9ucy5kZWxldGVEb2N1bWVudCA9IDE7XG5cblx0XHR0aGlzLmxvYWREb2NzKCk7XG5cdFx0dGhpcy5yZWxvYWREYXRhVGFibGUoKTtcblxuXHR9LFxufSk7XG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG5cdC8vZm9yIGVkaXRpbmcgb2YgY2hpbGRcblx0JChkb2N1bWVudCkub24oXCJjbGlja1wiLFwiLmVkaXRTZXJ2XCIsZnVuY3Rpb24oKSB7XG5cdFx0dmFyIGlkID0gJCh0aGlzKS5hdHRyKCdpZCcpXG5cdFx0dmFyIGlkID0gaWQuc3BsaXQoXCJfXCIpO1xuICAgICAgICBhcHAuY2hhbmdlQWN0aXZlSWQoaWRbMV0pO1xuICAgICAgICBhcHAuYWRkT3JFZGl0KCdlZGl0Jyk7XG4gICAgfSk7XG5cblx0Ly9mb3IgZGVsZXRpbmcgb2YgY2hpbGRcbiAgICAkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsXCIuZGVsZXRlU2VydlwiLGZ1bmN0aW9uKCkge1xuXHRcdHZhciBpZCA9ICQodGhpcykuYXR0cignaWQnKVxuXHRcdHZhciBpZCA9IGlkLnNwbGl0KFwiX1wiKTtcbiAgICAgICAgYXBwLmNoYW5nZUFjdGl2ZUlkKGlkWzFdKTtcbiAgICAgICAgYXBwLnNob3dSZW1vdmVTZXJ2aWNlRGlhbG9nKCk7XG4gICAgfSk7XG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL3NlcnZpY2VzL2luZGV4LmpzIiwiPHRlbXBsYXRlPlxuICAgIDxkaXYgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0XCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJwYW5lbC1oZWFkaW5nXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1sZy0xMVwiPlxuICAgICAgICAgICAgICAgICAgICA8aDUgY2xhc3M9XCJwYW5lbC10aXRsZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGE+e3tjaGxkLmRldGFpbH19PC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJsYWJlbCBsYWJlbC1pbmZvXCI+e3tjaGxkLmNvc3R9fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPiBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c21hbGwgY2xhc3M9XCJtLWwtc21cIj57e2NobGQuZGVzY3JpcHRpb259fTwvc21hbGw+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvaDU+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1sZy0xXCI+XG4gICAgICAgICAgICAgICAgICAgIDxhIEBjbGljaz1cImVkKGNobGQuaWQpXCIgc3R5bGU9XCJtYXJnaW4tcmlnaHQ6OHB4O1wiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiRWRpdFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1wZW5jaWwgZmEtMXhcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICAgICAgPGEgQGNsaWNrPVwiZGUoY2hsZC5pZClcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiB0aXRsZT1cIkRlbGV0ZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS10cmFzaCBmYS0xeFwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PiBcbjwvdGVtcGxhdGU+XG48c2NyaXB0PlxuICAgIGV4cG9ydCBkZWZhdWx0e1xuICAgICAgICBwcm9wczogWydjaGxkJ10sXG4gICAgICAgIG1ldGhvZHM6e1xuICAgICAgICAgICAgZWQoaWQpe1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC4kcGFyZW50LmFjdGl2ZUlkID0gaWQ7XG4gICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuYWRkT3JFZGl0KCdlZGl0Jyk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZGUoaWQpe1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC4kcGFyZW50LmFjdGl2ZUlkID0gaWQ7XG4gICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LiRwYXJlbnQuc2hvd1JlbW92ZVNlcnZpY2VEaWFsb2coKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgIH0sXG4gICAgfVxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENoaWxkU2VydmljZXMudnVlP2Y3NDAwZDQ0IiwiPHRlbXBsYXRlPlxuICAgIDx0cj5cbiAgICAgICAgPHRkIGNsYXNzPVwiY29sLWxnLTUgdG9nZ2xlLXJvd1wiIHN0eWxlPVwiY3Vyc29yOnBvaW50ZXI7XCI+XG4gICAgICAgICAgICA8cD48c3Bhbj48Yj57e2l0ZW0udGl0bGV9fTwvYj48L3NwYW4+PHNwYW4+IHt7aXRlbS50aXRsZV9jbn19IDwvc3Bhbj48L3A+XG4gICAgICAgIDwvdGQ+XG4gICAgICAgIDx0ZCBjbGFzcz1cImNvbC1sZy0xXCI+XG4gICAgICAgICAgICA8Y2VudGVyPlxuICAgICAgICAgICAgICAgIDxhIHN0eWxlPVwibWFyZ2luLXJpZ2h0OjhweDtcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiB0aXRsZT1cIkVkaXQgRG9jdW1lbnRcIiBAY2xpY2s9XCIkcGFyZW50LmFjdGlvbkRvYygnZWRpdCcsIGl0ZW0uaWQpXCIgdi1zaG93PVwiJHBhcmVudC5zZWxwZXJtaXNzaW9ucy5lZGl0RG9jdW1lbnQ9PTEgfHwgJHBhcmVudC5zZXR0aW5nID09IDFcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1wZW5jaWwgZmEtMXhcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgIDxhIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiRGVsZXRlXCIgQGNsaWNrPVwiJHBhcmVudC5hY3Rpb25Eb2MoJ2RlbGV0ZScsIGl0ZW0uaWQpXCIgdi1zaG93PVwiJHBhcmVudC5zZWxwZXJtaXNzaW9ucy5kZWxldGVEb2N1bWVudD09MSB8fCAkcGFyZW50LnNldHRpbmcgPT0gMVwiPlxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLXRyYXNoIGZhLTF4XCI+PC9pPlxuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvY2VudGVyPlxuICAgICAgICA8L3RkPlxuICAgIDwvdHI+XG48L3RlbXBsYXRlPlxuPHNjcmlwdD5cblx0ZXhwb3J0IGRlZmF1bHR7XG5cdFx0cHJvcHM6IFsnaXRlbSddLFxuXHR9XG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRG9jdW1lbnRMaXN0LnZ1ZT83ODhhOWVlZiIsIjx0ZW1wbGF0ZT5cbiAgICA8ZGl2IGNsYXNzPVwiaWJveCBjb2xsYXBzZWQgc2VydmljZS1wYW5lbFwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwiaWJveC10aXRsZVwiPlxuICAgICAgICAgICAgPGg1Pnt7aXRlbS5kZXRhaWx9fVxuICAgICAgICAgICAgICAgIDxzbWFsbCBjbGFzcz1cIm0tbC1zbVwiPnt7aXRlbS5kZXNjcmlwdGlvbn19PC9zbWFsbD5cbiAgICAgICAgICAgIDwvaDU+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWJveC10b29sc1wiPlxuICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwiY29sbGFwc2UtbGlua1wiIHYtaWY9XCJpdGVtLmNoaWxkcmVuLmxlbmd0aCAhPSAwXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtY2hldnJvbi11cFwiPjwvaT5cbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgPGEgQGNsaWNrPVwiZWRpdChpdGVtLmlkKVwiIHN0eWxlPVwibWFyZ2luLXJpZ2h0OjhweDtcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiB0aXRsZT1cIkVkaXRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1wZW5jaWwgZmEtMXhcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgIDxhIEBjbGljaz1cImRlbChpdGVtLmlkKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiRGVsZXRlXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtdHJhc2ggZmEtMXhcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzPVwiaWJveC1jb250ZW50XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGFuZWwtYm9keVwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwYW5lbC1ncm91cFwiPlxuICAgICAgICAgICAgICAgICAgICA8IS0tIGluc2VydCBjaGlsZHJlbiAtLT5cbiAgICAgICAgICAgICAgICAgICAgPGNoaWxkc2VydmljZSBcbiAgICAgICAgICAgICAgICAgICAgICAgIHYtZm9yPVwiY2hsc3J2IGluIGl0ZW0uY2hpbGRyZW5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgOmNobGQ9XCJjaGxzcnZcIiBcbiAgICAgICAgICAgICAgICAgICAgICAgIDprZXk9XCJjaGxzcnYuaWRcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9jaGlsZHNlcnZpY2U+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+PCEtLWVuZCBvZiBzZXJ2aWNlLXBhbmVsICAtLT5cblxuXG48L3RlbXBsYXRlPlxuPHNjcmlwdD5cbiAgICBWdWUuY29tcG9uZW50KCdjaGlsZHNlcnZpY2UnLCAgcmVxdWlyZSgnLi4vLi4vY29tcG9uZW50cy9zZXJ2aWNlcy9DaGlsZFNlcnZpY2VzLnZ1ZScpKTtcblx0ZXhwb3J0IGRlZmF1bHR7XG5cdFx0cHJvcHM6IFsnaXRlbSddLFxuICAgICAgICBtZXRob2RzOntcbiAgICAgICAgICAgIGVkaXQoaWQpe1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5hY3RpdmVJZCA9IGlkO1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5hZGRPckVkaXQoJ2VkaXQnKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBkZWwoaWQpe1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5hY3RpdmVJZCA9IGlkO1xuICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5zaG93UmVtb3ZlU2VydmljZURpYWxvZygpO1xuICAgICAgICAgICAgfSxcbiAgICAgICBcbiAgICAgICAgfSxcblx0fVxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFNlcnZpY2VQYW5lbC52dWU/NTYxY2M4ZTQiLCI8dGVtcGxhdGU+XG4gICAgPHRyPlxuICAgICAgICA8dGQgY2xhc3M9XCJjb2wtbGctNSB0b2dnbGUtcm93XCIgOmlkPVwiJ3BfJytpdGVtLmlkXCIgc3R5bGU9XCJjdXJzb3I6cG9pbnRlcjtcIj48Yj57e2l0ZW0uZGV0YWlsfX08L2I+PC90ZD5cbiAgICAgICAgPHRkIGNsYXNzPVwiY29sLWxnLTVcIj48Yj57e2l0ZW0uZGVzY3JpcHRpb259fTwvYj48L3RkPlxuICAgICAgICA8dGQgY2xhc3M9XCJjb2wtbGctMVwiPlxuICAgICAgICAgICAgPHNwYW4gdi1pZj1cIml0ZW0uY29zdCAhPSAnMC4wMCdcIj48Yj57e2l0ZW0uY29zdH19PC9iPjwvc3Bhbj5cbiAgICAgICAgPC90ZD5cbiAgICAgICAgPHRkIGNsYXNzPVwiY29sLWxnLTFcIj5cbiAgICAgICAgICAgIDxjZW50ZXI+XG4gICAgICAgICAgICAgICAgPGEgQGNsaWNrPVwiZWRpdChpdGVtLmlkKVwiIHN0eWxlPVwibWFyZ2luLXJpZ2h0OjhweDtcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiB0aXRsZT1cIkVkaXQgU2VydmljZVwiIHYtc2hvdz1cIiRwYXJlbnQuc2VscGVybWlzc2lvbnMuZWRpdFNlcnZpY2U9PTEgfHwgJHBhcmVudC5zZXR0aW5nID09IDFcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1wZW5jaWwgZmEtMXhcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgIDxhIEBjbGljaz1cImRlbChpdGVtLmlkKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiRGVsZXRlXCIgdi1zaG93PVwiJHBhcmVudC5zZWxwZXJtaXNzaW9ucy5kZWxldGVTZXJ2aWNlPT0xIHx8ICRwYXJlbnQuc2V0dGluZyA9PSAxXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtdHJhc2ggZmEtMXhcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9jZW50ZXI+XG4gICAgICAgIDwvdGQ+XG5cbiAgICAgICAgPHRkIGNsYXNzPVwiaGlkZVwiPkV4dHJhIERhdGEgMTwvdGQ+XG4gICAgICAgIDx0ZCBjbGFzcz1cImhpZGVcIj5FeHRyYSBEYXRhIDI8L3RkPlxuICAgICAgICA8dGQgY2xhc3M9XCJoaWRlXCI+RXh0cmEgRGF0YSAzPC90ZD5cbiAgICAgICAgPHRkIGNsYXNzPVwiaGlkZVwiPkV4dHJhIERhdGEgNDwvdGQ+XG4gICAgPC90cj5cbjwvdGVtcGxhdGU+XG48c2NyaXB0PlxuICAgIGltcG9ydCBjaGlsZCBmcm9tICcuLi8uLi9jb21wb25lbnRzL3NlcnZpY2VzL0NoaWxkU2VydmljZXMudnVlJztcblx0ZXhwb3J0IGRlZmF1bHR7XG5cdFx0cHJvcHM6IFsnaXRlbSddLFxuICAgICAgICBjb21wb25lbnRzOiB7XG4gICAgICAgICAgICAnY2hpbGQtc2VydmljZSc6IGNoaWxkXG4gICAgICAgIH0sXG4gICAgICAgIG1ldGhvZHM6e1xuICAgICAgICAgICAgZWRpdChpZCl7XG4gICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LmFjdGl2ZUlkID0gaWQ7XG4gICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LmFkZE9yRWRpdCgnZWRpdCcpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGRlbChpZCl7XG4gICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LmFjdGl2ZUlkID0gaWQ7XG4gICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LnNob3dSZW1vdmVTZXJ2aWNlRGlhbG9nKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgIFxuICAgICAgICB9LFxuXHR9XG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gU2VydmljZXMudnVlPzQxY2UyZmFkIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRG9jdW1lbnRMaXN0LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNDQ1MGJhM2FcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRG9jdW1lbnRMaXN0LnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL0RvY3VtZW50TGlzdC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBEb2N1bWVudExpc3QudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTQ0NTBiYTNhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNDQ1MGJhM2FcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL0RvY3VtZW50TGlzdC52dWVcbi8vIG1vZHVsZSBpZCA9IDM3M1xuLy8gbW9kdWxlIGNodW5rcyA9IDYiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9TZXJ2aWNlUGFuZWwudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zZWE5YzlhMFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9TZXJ2aWNlUGFuZWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvc2VydmljZXMvU2VydmljZVBhbmVsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFNlcnZpY2VQYW5lbC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtM2VhOWM5YTBcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0zZWE5YzlhMFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvc2VydmljZXMvU2VydmljZVBhbmVsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzc0XG4vLyBtb2R1bGUgY2h1bmtzID0gNiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1NlcnZpY2VzLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtODEzMmM3ODJcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vU2VydmljZXMudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvc2VydmljZXMvU2VydmljZXMudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gU2VydmljZXMudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTgxMzJjNzgyXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtODEzMmM3ODJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL1NlcnZpY2VzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzc1XG4vLyBtb2R1bGUgY2h1bmtzID0gNiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlib3ggY29sbGFwc2VkIHNlcnZpY2UtcGFuZWxcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpYm94LXRpdGxlXCJcbiAgfSwgW19jKCdoNScsIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLmRldGFpbCkgKyBcIlxcbiAgICAgICAgICAgIFwiKSwgX2MoJ3NtYWxsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm0tbC1zbVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLmRlc2NyaXB0aW9uKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlib3gtdG9vbHNcIlxuICB9LCBbKF92bS5pdGVtLmNoaWxkcmVuLmxlbmd0aCAhPSAwKSA/IF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbGxhcHNlLWxpbmtcIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2hldnJvbi11cFwiXG4gIH0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luLXJpZ2h0XCI6IFwiOHB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkVkaXRcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5lZGl0KF92bS5pdGVtLmlkKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXBlbmNpbCBmYS0xeFwiXG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkRlbGV0ZVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmRlbChfdm0uaXRlbS5pZClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS10cmFzaCBmYS0xeFwiXG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlib3gtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1ncm91cFwiXG4gIH0sIF92bS5fbCgoX3ZtLml0ZW0uY2hpbGRyZW4pLCBmdW5jdGlvbihjaGxzcnYpIHtcbiAgICByZXR1cm4gX2MoJ2NoaWxkc2VydmljZScsIHtcbiAgICAgIGtleTogY2hsc3J2LmlkLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJjaGxkXCI6IGNobHNydlxuICAgICAgfVxuICAgIH0pXG4gIH0pLCAxKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0zZWE5YzlhMFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTNlYTljOWEwXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvc2VydmljZXMvU2VydmljZVBhbmVsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzg5XG4vLyBtb2R1bGUgY2h1bmtzID0gNiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygndHInLCBbX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy01IHRvZ2dsZS1yb3dcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJjdXJzb3JcIjogXCJwb2ludGVyXCJcbiAgICB9XG4gIH0sIFtfYygncCcsIFtfYygnc3BhbicsIFtfYygnYicsIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLnRpdGxlKSldKV0pLCBfYygnc3BhbicsIFtfdm0uX3YoXCIgXCIgKyBfdm0uX3MoX3ZtLml0ZW0udGl0bGVfY24pICsgXCIgXCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTFcIlxuICB9LCBbX2MoJ2NlbnRlcicsIFtfYygnYScsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uJHBhcmVudC5zZWxwZXJtaXNzaW9ucy5lZGl0RG9jdW1lbnQgPT0gMSB8fCBfdm0uJHBhcmVudC5zZXR0aW5nID09IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCIkcGFyZW50LnNlbHBlcm1pc3Npb25zLmVkaXREb2N1bWVudD09MSB8fCAkcGFyZW50LnNldHRpbmcgPT0gMVwiXG4gICAgfV0sXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luLXJpZ2h0XCI6IFwiOHB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkVkaXQgRG9jdW1lbnRcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS4kcGFyZW50LmFjdGlvbkRvYygnZWRpdCcsIF92bS5pdGVtLmlkKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXBlbmNpbCBmYS0xeFwiXG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS4kcGFyZW50LnNlbHBlcm1pc3Npb25zLmRlbGV0ZURvY3VtZW50ID09IDEgfHwgX3ZtLiRwYXJlbnQuc2V0dGluZyA9PSAxKSxcbiAgICAgIGV4cHJlc3Npb246IFwiJHBhcmVudC5zZWxwZXJtaXNzaW9ucy5kZWxldGVEb2N1bWVudD09MSB8fCAkcGFyZW50LnNldHRpbmcgPT0gMVwiXG4gICAgfV0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiRGVsZXRlXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJHBhcmVudC5hY3Rpb25Eb2MoJ2RlbGV0ZScsIF92bS5pdGVtLmlkKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXRyYXNoIGZhLTF4XCJcbiAgfSldKV0pXSwgMSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi00NDUwYmEzYVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTQ0NTBiYTNhXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvc2VydmljZXMvRG9jdW1lbnRMaXN0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzkxXG4vLyBtb2R1bGUgY2h1bmtzID0gNiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsIHBhbmVsLWRlZmF1bHRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1oZWFkaW5nXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTExXCJcbiAgfSwgW19jKCdoNScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbC10aXRsZVwiXG4gIH0sIFtfYygnYScsIFtfdm0uX3YoX3ZtLl9zKF92bS5jaGxkLmRldGFpbCkpXSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImxhYmVsIGxhYmVsLWluZm9cIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uY2hsZC5jb3N0KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCBbX2MoJ3NtYWxsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm0tbC1zbVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5jaGxkLmRlc2NyaXB0aW9uKSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMVwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJtYXJnaW4tcmlnaHRcIjogXCI4cHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiRWRpdFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmVkKF92bS5jaGxkLmlkKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXBlbmNpbCBmYS0xeFwiXG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkRlbGV0ZVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmRlKF92bS5jaGxkLmlkKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXRyYXNoIGZhLTF4XCJcbiAgfSldKV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTVjYTkxZWVlXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNWNhOTFlZWVcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9zZXJ2aWNlcy9DaGlsZFNlcnZpY2VzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzk3XG4vLyBtb2R1bGUgY2h1bmtzID0gNiIsIjx0ZW1wbGF0ZT5cbjxkaXYgY2xhc3M9XCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIgOmlkPVwiaWRcIiB0YWJpbmRleD1cIi0xXCIgcm9sZT1cImRpYWxvZ1wiIGFyaWEtbGFiZWxsZWRieT1cIm1vZGFsLWxhYmVsXCI+XG4gICAgPGRpdiA6Y2xhc3M9XCInbW9kYWwtZGlhbG9nICcrc2l6ZVwiIHJvbGU9XCJkb2N1bWVudFwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPlxuICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCIgaWQ9XCJtb2RhbC1sYWJlbFwiPlxuICAgICAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtdGl0bGVcIj5Nb2RhbCBUaXRsZTwvc2xvdD5cbiAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1ib2R5XCI+TW9kYWwgQm9keTwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPlxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9zbG90PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICAgIHByb3BzOiB7XG4gICAgICAgICdpZCc6e3JlcXVpcmVkOnRydWV9XG4gICAgICAgICwnc2l6ZSc6IHtkZWZhdWx0Oidtb2RhbC1tZCd9XG4gICAgfVxufVxuPC9zY3JpcHQ+XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRGlhbG9nTW9kYWwudnVlPzAwM2JkYTg4IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0cicsIFtfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTUgdG9nZ2xlLXJvd1wiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImN1cnNvclwiOiBcInBvaW50ZXJcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ3BfJyArIF92bS5pdGVtLmlkXG4gICAgfVxuICB9LCBbX2MoJ2InLCBbX3ZtLl92KF92bS5fcyhfdm0uaXRlbS5kZXRhaWwpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctNVwiXG4gIH0sIFtfYygnYicsIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLmRlc2NyaXB0aW9uKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTFcIlxuICB9LCBbKF92bS5pdGVtLmNvc3QgIT0gJzAuMDAnKSA/IF9jKCdzcGFuJywgW19jKCdiJywgW192bS5fdihfdm0uX3MoX3ZtLml0ZW0uY29zdCkpXSldKSA6IF92bS5fZSgpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMVwiXG4gIH0sIFtfYygnY2VudGVyJywgW19jKCdhJywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS4kcGFyZW50LnNlbHBlcm1pc3Npb25zLmVkaXRTZXJ2aWNlID09IDEgfHwgX3ZtLiRwYXJlbnQuc2V0dGluZyA9PSAxKSxcbiAgICAgIGV4cHJlc3Npb246IFwiJHBhcmVudC5zZWxwZXJtaXNzaW9ucy5lZGl0U2VydmljZT09MSB8fCAkcGFyZW50LnNldHRpbmcgPT0gMVwiXG4gICAgfV0sXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luLXJpZ2h0XCI6IFwiOHB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkVkaXQgU2VydmljZVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmVkaXQoX3ZtLml0ZW0uaWQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtcGVuY2lsIGZhLTF4XCJcbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLiRwYXJlbnQuc2VscGVybWlzc2lvbnMuZGVsZXRlU2VydmljZSA9PSAxIHx8IF92bS4kcGFyZW50LnNldHRpbmcgPT0gMSksXG4gICAgICBleHByZXNzaW9uOiBcIiRwYXJlbnQuc2VscGVybWlzc2lvbnMuZGVsZXRlU2VydmljZT09MSB8fCAkcGFyZW50LnNldHRpbmcgPT0gMVwiXG4gICAgfV0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiRGVsZXRlXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZGVsKF92bS5pdGVtLmlkKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXRyYXNoIGZhLTF4XCJcbiAgfSldKV0pXSwgMSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoaWRlXCJcbiAgfSwgW192bS5fdihcIkV4dHJhIERhdGEgMVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGlkZVwiXG4gIH0sIFtfdm0uX3YoXCJFeHRyYSBEYXRhIDJcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhpZGVcIlxuICB9LCBbX3ZtLl92KFwiRXh0cmEgRGF0YSAzXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoaWRlXCJcbiAgfSwgW192bS5fdihcIkV4dHJhIERhdGEgNFwiKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtODEzMmM3ODJcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi04MTMyYzc4MlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL3NlcnZpY2VzL1NlcnZpY2VzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDEyXG4vLyBtb2R1bGUgY2h1bmtzID0gNiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNGRlNjA2NTVcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRGlhbG9nTW9kYWwudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTRkZTYwNjU1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNGRlNjA2NTVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNVxuLy8gbW9kdWxlIGNodW5rcyA9IDQgNSA2IDkgMTEgMTggMTkgMjAgMjEgMjIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogX3ZtLmlkLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIi0xXCIsXG4gICAgICBcInJvbGVcIjogXCJkaWFsb2dcIixcbiAgICAgIFwiYXJpYS1sYWJlbGxlZGJ5XCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgY2xhc3M6ICdtb2RhbC1kaWFsb2cgJyArIF92bS5zaXplLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInJvbGVcIjogXCJkb2N1bWVudFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtaGVhZGVyXCJcbiAgfSwgW19jKCdoNCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC10aXRsZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW192bS5fdChcIm1vZGFsLXRpdGxlXCIsIFtfdm0uX3YoXCJNb2RhbCBUaXRsZVwiKV0pXSwgMildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1ib2R5XCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWJvZHlcIiwgW192bS5fdihcIk1vZGFsIEJvZHlcIildKV0sIDIpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWZvb3RlclwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1mb290ZXJcIiwgW19jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXSwgMildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNGRlNjA2NTVcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi00ZGU2MDY1NVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNlxuLy8gbW9kdWxlIGNodW5rcyA9IDQgNSA2IDkgMTEgMTggMTkgMjAgMjEgMjIiXSwic291cmNlUm9vdCI6IiJ9