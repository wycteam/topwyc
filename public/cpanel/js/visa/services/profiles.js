/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 517);
/******/ })
/************************************************************************/
/******/ ({

/***/ 212:
/***/ (function(module, exports) {

new Vue({
	el: '#attendance_monitoring',

	data: {
		loading: true,

		search: '',

		current: true,

		selectMonths: [{ value: '1', text: 'January' }, { value: '2', text: 'February' }, { value: '3', text: 'March' }, { value: '4', text: 'April' }, { value: '5', text: 'May' }, { value: '6', text: 'June' }, { value: '7', text: 'July' }, { value: '8', text: 'August' }, { value: '9', text: 'September' }, { value: '10', text: 'October' }, { value: '11', text: 'November' }, { value: '12', text: 'December' }],

		selectYears: ['2018', '2019', '2020', '2021', '2022'],

		filterMonth: new Date().getMonth() + 1,
		filterYear: new Date().getFullYear(),

		numberOfDays: 0,

		services: [],
		profiles: [],
		pcount: 0,
		pid: 0,
		sid: 0
	},

	methods: {
		getServiceProfiles: function getServiceProfiles() {
			var _this = this;

			// this.numberOfDays = new Date(this.filterYear, this.filterMonth, 0).getDate();
			axios.get('/visa/service-manager/servProfiles').then(function (result) {
				_this.profiles = result.data.profiles;
				_this.pcount = result.data.count;
				var count = _this.pcount;
				count = count * 200;
				var w = 600 + count;
				$("#fixTable").css("width", w + "px");
			});
		},
		editProfileCost: function editProfileCost(cost, profile_id, service_id) {
			$('.inputCost').hide();
			$('.strCost').show();
			var pcost = parseInt(cost);
			this.pid = profile_id;
			this.sid = service_id;
			// alert('#pc_'+service_id+'_'+profile_id);
			$('#pc_' + service_id + '_' + profile_id).val(pcost);
			$('#str_' + service_id + '_' + profile_id).hide();
			$('#pc_' + service_id + '_' + profile_id).show();
			$('#pc_' + service_id + '_' + profile_id).focus();
		},
		updateProfileCost: function updateProfileCost() {
			var service_id = this.sid;
			var profile_id = this.pid;
			var pval = $('#pc_' + service_id + '_' + profile_id).val();;

			axios.post('/visa/service-manager/updateProfileCost', {
				service_id: service_id,
				profile_id: profile_id,
				cost: pval
			}).then(function (response) {
				$('#str_' + service_id + '_' + profile_id).text(pval + '.00');
				$('#str_' + service_id + '_' + profile_id).show();
				$('#pc_' + service_id + '_' + profile_id).hide();
				toastr.success('', 'Updated Successfully!');
			}).catch(function (error) {
				return console.log(error);
			});
		},
		refreshTableHeadFixer: function refreshTableHeadFixer() {
			setTimeout(function () {
				$("#fixTable").tableHeadFixer({ "head": false, "left": 1 });
			}, 1000);
		},
		getServiceDetails: function getServiceDetails() {
			var _this2 = this;

			this.loading = true;
			axios.get('/visa/service-manager/sortedServices').then(function (result) {
				_this2.services = result.data;
				setTimeout(function () {
					_this2.loading = false;

					$("#fixTable").tableHeadFixer({ "head": false, "left": 1 });

					var position = 0;
					$("#fixTable-wrapper").animate({ scrollLeft: position }, 100);
				}, 1000);
			});
		},
		filter: function filter() {
			this.getServiceDetails();
		}
	},

	created: function created() {
		this.getServiceProfiles();

		this.getServiceDetails();
		$('.inputCost').hide();
	},


	computed: {
		filteredServices: function filteredServices() {
			var _this3 = this;

			return this.services.filter(function (service) {
				return service.detail.toLowerCase().indexOf(_this3.search.toLowerCase()) > -1;
			});
		}
	}
});

/***/ }),

/***/ 517:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(212);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNDY4YzM1NmNmZTNhZDIyODAxYzEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9zZXJ2aWNlcy9wcm9maWxlcy5qcyJdLCJuYW1lcyI6WyJWdWUiLCJlbCIsImRhdGEiLCJsb2FkaW5nIiwic2VhcmNoIiwiY3VycmVudCIsInNlbGVjdE1vbnRocyIsInZhbHVlIiwidGV4dCIsInNlbGVjdFllYXJzIiwiZmlsdGVyTW9udGgiLCJEYXRlIiwiZ2V0TW9udGgiLCJmaWx0ZXJZZWFyIiwiZ2V0RnVsbFllYXIiLCJudW1iZXJPZkRheXMiLCJzZXJ2aWNlcyIsInByb2ZpbGVzIiwicGNvdW50IiwicGlkIiwic2lkIiwibWV0aG9kcyIsImdldFNlcnZpY2VQcm9maWxlcyIsImF4aW9zIiwiZ2V0IiwidGhlbiIsInJlc3VsdCIsImNvdW50IiwidyIsIiQiLCJjc3MiLCJlZGl0UHJvZmlsZUNvc3QiLCJjb3N0IiwicHJvZmlsZV9pZCIsInNlcnZpY2VfaWQiLCJoaWRlIiwic2hvdyIsInBjb3N0IiwicGFyc2VJbnQiLCJ2YWwiLCJmb2N1cyIsInVwZGF0ZVByb2ZpbGVDb3N0IiwicHZhbCIsInBvc3QiLCJ0b2FzdHIiLCJzdWNjZXNzIiwiY2F0Y2giLCJjb25zb2xlIiwibG9nIiwiZXJyb3IiLCJyZWZyZXNoVGFibGVIZWFkRml4ZXIiLCJzZXRUaW1lb3V0IiwidGFibGVIZWFkRml4ZXIiLCJnZXRTZXJ2aWNlRGV0YWlscyIsInBvc2l0aW9uIiwiYW5pbWF0ZSIsInNjcm9sbExlZnQiLCJmaWx0ZXIiLCJjcmVhdGVkIiwiY29tcHV0ZWQiLCJmaWx0ZXJlZFNlcnZpY2VzIiwic2VydmljZSIsImRldGFpbCIsInRvTG93ZXJDYXNlIiwiaW5kZXhPZiJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQSxJQUFJQSxHQUFKLENBQVE7QUFDUEMsS0FBRyx3QkFESTs7QUFHUEMsT0FBSztBQUNKQyxXQUFTLElBREw7O0FBR0pDLFVBQVEsRUFISjs7QUFLSkMsV0FBUyxJQUxMOztBQU9KQyxnQkFBYyxDQUNiLEVBQUNDLE9BQU8sR0FBUixFQUFhQyxNQUFNLFNBQW5CLEVBRGEsRUFFYixFQUFDRCxPQUFPLEdBQVIsRUFBYUMsTUFBTSxVQUFuQixFQUZhLEVBR2IsRUFBQ0QsT0FBTyxHQUFSLEVBQWFDLE1BQU0sT0FBbkIsRUFIYSxFQUliLEVBQUNELE9BQU8sR0FBUixFQUFhQyxNQUFNLE9BQW5CLEVBSmEsRUFLYixFQUFDRCxPQUFPLEdBQVIsRUFBYUMsTUFBTSxLQUFuQixFQUxhLEVBTWIsRUFBQ0QsT0FBTyxHQUFSLEVBQWFDLE1BQU0sTUFBbkIsRUFOYSxFQU9iLEVBQUNELE9BQU8sR0FBUixFQUFhQyxNQUFNLE1BQW5CLEVBUGEsRUFRYixFQUFDRCxPQUFPLEdBQVIsRUFBYUMsTUFBTSxRQUFuQixFQVJhLEVBU2IsRUFBQ0QsT0FBTyxHQUFSLEVBQWFDLE1BQU0sV0FBbkIsRUFUYSxFQVViLEVBQUNELE9BQU8sSUFBUixFQUFjQyxNQUFNLFNBQXBCLEVBVmEsRUFXYixFQUFDRCxPQUFPLElBQVIsRUFBY0MsTUFBTSxVQUFwQixFQVhhLEVBWWIsRUFBQ0QsT0FBTyxJQUFSLEVBQWNDLE1BQU0sVUFBcEIsRUFaYSxDQVBWOztBQXNCSkMsZUFBYSxDQUFDLE1BQUQsRUFBUyxNQUFULEVBQWlCLE1BQWpCLEVBQXlCLE1BQXpCLEVBQWlDLE1BQWpDLENBdEJUOztBQXdCSkMsZUFBYSxJQUFJQyxJQUFKLEdBQVdDLFFBQVgsS0FBd0IsQ0F4QmpDO0FBeUJKQyxjQUFZLElBQUlGLElBQUosR0FBV0csV0FBWCxFQXpCUjs7QUEyQkpDLGdCQUFjLENBM0JWOztBQTZCSkMsWUFBUyxFQTdCTDtBQThCSkMsWUFBUyxFQTlCTDtBQStCSkMsVUFBTyxDQS9CSDtBQWdDSkMsT0FBSSxDQWhDQTtBQWlDSkMsT0FBSTtBQWpDQSxFQUhFOztBQXVDUEMsVUFBUztBQUNSQyxvQkFEUSxnQ0FDYTtBQUFBOztBQUNwQjtBQUNBQyxTQUFNQyxHQUFOLENBQVUsb0NBQVYsRUFDUUMsSUFEUixDQUNhLGtCQUFVO0FBQ2QsVUFBS1IsUUFBTCxHQUFnQlMsT0FBT3hCLElBQVAsQ0FBWWUsUUFBNUI7QUFDQSxVQUFLQyxNQUFMLEdBQWNRLE9BQU94QixJQUFQLENBQVl5QixLQUExQjtBQUNDLFFBQUlBLFFBQVEsTUFBS1QsTUFBakI7QUFDTFMsWUFBUUEsUUFBUSxHQUFoQjtBQUNBLFFBQUlDLElBQUksTUFBTUQsS0FBZDtBQUNIRSxNQUFFLFdBQUYsRUFBZUMsR0FBZixDQUFtQixPQUFuQixFQUE0QkYsSUFBRSxJQUE5QjtBQUNHLElBUkw7QUFTQSxHQVpPO0FBYVJHLGlCQWJRLDJCQWFRQyxJQWJSLEVBYWFDLFVBYmIsRUFhd0JDLFVBYnhCLEVBYW9DO0FBQzFDTCxLQUFFLFlBQUYsRUFBZ0JNLElBQWhCO0FBQ0FOLEtBQUUsVUFBRixFQUFjTyxJQUFkO0FBQ0EsT0FBSUMsUUFBUUMsU0FBU04sSUFBVCxDQUFaO0FBQ0EsUUFBS2IsR0FBTCxHQUFXYyxVQUFYO0FBQ0EsUUFBS2IsR0FBTCxHQUFXYyxVQUFYO0FBQ0E7QUFDQUwsS0FBRSxTQUFPSyxVQUFQLEdBQWtCLEdBQWxCLEdBQXNCRCxVQUF4QixFQUFvQ00sR0FBcEMsQ0FBd0NGLEtBQXhDO0FBQ0FSLEtBQUUsVUFBUUssVUFBUixHQUFtQixHQUFuQixHQUF1QkQsVUFBekIsRUFBcUNFLElBQXJDO0FBQ0FOLEtBQUUsU0FBT0ssVUFBUCxHQUFrQixHQUFsQixHQUFzQkQsVUFBeEIsRUFBb0NHLElBQXBDO0FBQ0FQLEtBQUUsU0FBT0ssVUFBUCxHQUFrQixHQUFsQixHQUFzQkQsVUFBeEIsRUFBb0NPLEtBQXBDO0FBQ0QsR0F4Qk87QUEwQlJDLG1CQTFCUSwrQkEwQlk7QUFDbEIsT0FBSVAsYUFBYSxLQUFLZCxHQUF0QjtBQUNBLE9BQUlhLGFBQWEsS0FBS2QsR0FBdEI7QUFDQSxPQUFJdUIsT0FBT2IsRUFBRSxTQUFPSyxVQUFQLEdBQWtCLEdBQWxCLEdBQXNCRCxVQUF4QixFQUFvQ00sR0FBcEMsRUFBWCxDQUFxRDs7QUFFckRoQixTQUFNb0IsSUFBTixDQUFXLHlDQUFYLEVBQXFEO0FBQ3BEVCxnQkFBYUEsVUFEdUM7QUFFcERELGdCQUFhQSxVQUZ1QztBQUdwREQsVUFBT1U7QUFINkMsSUFBckQsRUFLQ2pCLElBTEQsQ0FLTSxvQkFBWTtBQUNqQkksTUFBRSxVQUFRSyxVQUFSLEdBQW1CLEdBQW5CLEdBQXVCRCxVQUF6QixFQUFxQ3pCLElBQXJDLENBQTBDa0MsT0FBSyxLQUEvQztBQUNBYixNQUFFLFVBQVFLLFVBQVIsR0FBbUIsR0FBbkIsR0FBdUJELFVBQXpCLEVBQXFDRyxJQUFyQztBQUNBUCxNQUFFLFNBQU9LLFVBQVAsR0FBa0IsR0FBbEIsR0FBc0JELFVBQXhCLEVBQW9DRSxJQUFwQztBQUNBUyxXQUFPQyxPQUFQLENBQWUsRUFBZixFQUFrQix1QkFBbEI7QUFDQSxJQVZELEVBV0NDLEtBWEQsQ0FXTztBQUFBLFdBQVNDLFFBQVFDLEdBQVIsQ0FBWUMsS0FBWixDQUFUO0FBQUEsSUFYUDtBQWFELEdBNUNPO0FBOENSQyx1QkE5Q1EsbUNBOENnQjtBQUN2QkMsY0FBVyxZQUFNO0FBQ2R0QixNQUFFLFdBQUYsRUFBZXVCLGNBQWYsQ0FBOEIsRUFBQyxRQUFTLEtBQVYsRUFBaUIsUUFBUyxDQUExQixFQUE5QjtBQUNBLElBRkgsRUFFSyxJQUZMO0FBR0EsR0FsRE87QUFtRFJDLG1CQW5EUSwrQkFtRFk7QUFBQTs7QUFDbkIsUUFBS2xELE9BQUwsR0FBZSxJQUFmO0FBQ0FvQixTQUFNQyxHQUFOLENBQVUsc0NBQVYsRUFDUUMsSUFEUixDQUNhLGtCQUFVO0FBQ2QsV0FBS1QsUUFBTCxHQUFnQlUsT0FBT3hCLElBQXZCO0FBQ0FpRCxlQUFXLFlBQU07QUFDckIsWUFBS2hELE9BQUwsR0FBZSxLQUFmOztBQUVBMEIsT0FBRSxXQUFGLEVBQWV1QixjQUFmLENBQThCLEVBQUMsUUFBUyxLQUFWLEVBQWlCLFFBQVMsQ0FBMUIsRUFBOUI7O0FBRUEsU0FBSUUsV0FBVyxDQUFmO0FBQ0F6QixPQUFFLG1CQUFGLEVBQXVCMEIsT0FBdkIsQ0FBK0IsRUFBQ0MsWUFBWUYsUUFBYixFQUEvQixFQUF1RCxHQUF2RDtBQUNBLEtBUEksRUFPRixJQVBFO0FBUUosSUFYTDtBQVlBLEdBakVPO0FBa0VSRyxRQWxFUSxvQkFrRUM7QUFDUixRQUFLSixpQkFBTDtBQUNBO0FBcEVPLEVBdkNGOztBQThHUEssUUE5R08scUJBOEdHO0FBQ1QsT0FBS3BDLGtCQUFMOztBQUVBLE9BQUsrQixpQkFBTDtBQUNBeEIsSUFBRSxZQUFGLEVBQWdCTSxJQUFoQjtBQUNBLEVBbkhNOzs7QUFxSFB3QixXQUFVO0FBQ05DLGtCQURNLDhCQUNhO0FBQUE7O0FBQ2hCLFVBQU8sS0FBSzVDLFFBQUwsQ0FBY3lDLE1BQWQsQ0FBcUIsbUJBQVc7QUFDcEMsV0FBUUksUUFBUUMsTUFBUixDQUFlQyxXQUFmLEdBQTZCQyxPQUE3QixDQUFxQyxPQUFLNUQsTUFBTCxDQUFZMkQsV0FBWixFQUFyQyxJQUFrRSxDQUFDLENBQTNFO0FBQ0YsSUFGTSxDQUFQO0FBR0Y7QUFMSztBQXJISCxDQUFSLEUiLCJmaWxlIjoianMvdmlzYS9zZXJ2aWNlcy9wcm9maWxlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNTE3KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCA0NjhjMzU2Y2ZlM2FkMjI4MDFjMSIsIm5ldyBWdWUoe1xyXG5cdGVsOicjYXR0ZW5kYW5jZV9tb25pdG9yaW5nJyxcclxuXHJcblx0ZGF0YTp7XHJcblx0XHRsb2FkaW5nOiB0cnVlLFxyXG5cclxuXHRcdHNlYXJjaDogJycsXHJcblxyXG5cdFx0Y3VycmVudDogdHJ1ZSxcclxuXHJcblx0XHRzZWxlY3RNb250aHM6IFtcclxuXHRcdFx0e3ZhbHVlOiAnMScsIHRleHQ6ICdKYW51YXJ5J30sXHJcblx0XHRcdHt2YWx1ZTogJzInLCB0ZXh0OiAnRmVicnVhcnknfSxcclxuXHRcdFx0e3ZhbHVlOiAnMycsIHRleHQ6ICdNYXJjaCd9LFxyXG5cdFx0XHR7dmFsdWU6ICc0JywgdGV4dDogJ0FwcmlsJ30sXHJcblx0XHRcdHt2YWx1ZTogJzUnLCB0ZXh0OiAnTWF5J30sXHJcblx0XHRcdHt2YWx1ZTogJzYnLCB0ZXh0OiAnSnVuZSd9LFxyXG5cdFx0XHR7dmFsdWU6ICc3JywgdGV4dDogJ0p1bHknfSxcclxuXHRcdFx0e3ZhbHVlOiAnOCcsIHRleHQ6ICdBdWd1c3QnfSxcclxuXHRcdFx0e3ZhbHVlOiAnOScsIHRleHQ6ICdTZXB0ZW1iZXInfSxcclxuXHRcdFx0e3ZhbHVlOiAnMTAnLCB0ZXh0OiAnT2N0b2Jlcid9LFxyXG5cdFx0XHR7dmFsdWU6ICcxMScsIHRleHQ6ICdOb3ZlbWJlcid9LFxyXG5cdFx0XHR7dmFsdWU6ICcxMicsIHRleHQ6ICdEZWNlbWJlcid9XHJcblx0XHRdLFxyXG5cclxuXHRcdHNlbGVjdFllYXJzOiBbJzIwMTgnLCAnMjAxOScsICcyMDIwJywgJzIwMjEnLCAnMjAyMiddLFxyXG5cclxuXHRcdGZpbHRlck1vbnRoOiBuZXcgRGF0ZSgpLmdldE1vbnRoKCkgKyAxLFxyXG5cdFx0ZmlsdGVyWWVhcjogbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpLFxyXG5cclxuXHRcdG51bWJlck9mRGF5czogMCxcclxuXHJcblx0XHRzZXJ2aWNlczpbXSxcclxuXHRcdHByb2ZpbGVzOltdLFxyXG5cdFx0cGNvdW50OjAsXHJcblx0XHRwaWQ6MCxcclxuXHRcdHNpZDowLFxyXG5cdH0sXHJcblxyXG5cdG1ldGhvZHM6IHtcclxuXHRcdGdldFNlcnZpY2VQcm9maWxlcygpIHtcclxuXHRcdFx0Ly8gdGhpcy5udW1iZXJPZkRheXMgPSBuZXcgRGF0ZSh0aGlzLmZpbHRlclllYXIsIHRoaXMuZmlsdGVyTW9udGgsIDApLmdldERhdGUoKTtcclxuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvc2VydlByb2ZpbGVzJylcclxuXHRcdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdCAgICAgICAgICB0aGlzLnByb2ZpbGVzID0gcmVzdWx0LmRhdGEucHJvZmlsZXM7XHJcblx0XHQgICAgICAgICAgdGhpcy5wY291bnQgPSByZXN1bHQuZGF0YS5jb3VudDtcclxuXHRcdCAgICAgICAgICBcdGxldCBjb3VudCA9IHRoaXMucGNvdW50O1xyXG5cdFx0XHRcdCAgICBjb3VudCA9IGNvdW50ICogMjAwO1xyXG5cdFx0XHRcdCAgICBsZXQgdyA9IDYwMCArIGNvdW50O1xyXG5cdFx0XHRcdFx0JChcIiNmaXhUYWJsZVwiKS5jc3MoXCJ3aWR0aFwiLCB3K1wicHhcIik7XHJcblx0XHQgICAgXHR9KTtcclxuXHRcdH0sXHJcblx0XHRlZGl0UHJvZmlsZUNvc3QoY29zdCxwcm9maWxlX2lkLHNlcnZpY2VfaWQpIHtcclxuXHRcdCAgJCgnLmlucHV0Q29zdCcpLmhpZGUoKTtcclxuXHRcdCAgJCgnLnN0ckNvc3QnKS5zaG93KCk7XHJcblx0XHQgIHZhciBwY29zdCA9IHBhcnNlSW50KGNvc3QpO1xyXG5cdFx0ICB0aGlzLnBpZCA9IHByb2ZpbGVfaWQ7XHJcblx0XHQgIHRoaXMuc2lkID0gc2VydmljZV9pZDtcclxuXHRcdCAgLy8gYWxlcnQoJyNwY18nK3NlcnZpY2VfaWQrJ18nK3Byb2ZpbGVfaWQpO1xyXG5cdFx0ICAkKCcjcGNfJytzZXJ2aWNlX2lkKydfJytwcm9maWxlX2lkKS52YWwocGNvc3QpO1xyXG5cdFx0ICAkKCcjc3RyXycrc2VydmljZV9pZCsnXycrcHJvZmlsZV9pZCkuaGlkZSgpO1xyXG5cdFx0ICAkKCcjcGNfJytzZXJ2aWNlX2lkKydfJytwcm9maWxlX2lkKS5zaG93KCk7XHJcblx0XHQgICQoJyNwY18nK3NlcnZpY2VfaWQrJ18nK3Byb2ZpbGVfaWQpLmZvY3VzKCk7XHJcblx0XHR9LFxyXG5cclxuXHRcdHVwZGF0ZVByb2ZpbGVDb3N0KCkge1xyXG5cdFx0ICB2YXIgc2VydmljZV9pZCA9IHRoaXMuc2lkO1xyXG5cdFx0ICB2YXIgcHJvZmlsZV9pZCA9IHRoaXMucGlkO1xyXG5cdFx0ICB2YXIgcHZhbCA9ICQoJyNwY18nK3NlcnZpY2VfaWQrJ18nK3Byb2ZpbGVfaWQpLnZhbCgpOztcclxuXHJcblx0XHQgIGF4aW9zLnBvc3QoJy92aXNhL3NlcnZpY2UtbWFuYWdlci91cGRhdGVQcm9maWxlQ29zdCcse1xyXG5cdFx0XHRcdFx0c2VydmljZV9pZCA6IHNlcnZpY2VfaWQsXHJcblx0XHRcdFx0XHRwcm9maWxlX2lkIDogcHJvZmlsZV9pZCxcclxuXHRcdFx0XHRcdGNvc3QgOiBwdmFsLFxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdFx0JCgnI3N0cl8nK3NlcnZpY2VfaWQrJ18nK3Byb2ZpbGVfaWQpLnRleHQocHZhbCsnLjAwJyk7XHJcblx0XHRcdFx0XHQkKCcjc3RyXycrc2VydmljZV9pZCsnXycrcHJvZmlsZV9pZCkuc2hvdygpO1xyXG5cdFx0XHRcdFx0JCgnI3BjXycrc2VydmljZV9pZCsnXycrcHJvZmlsZV9pZCkuaGlkZSgpO1xyXG5cdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MoJycsJ1VwZGF0ZWQgU3VjY2Vzc2Z1bGx5IScpXHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcclxuXHJcblx0XHR9LFxyXG5cclxuXHRcdHJlZnJlc2hUYWJsZUhlYWRGaXhlcigpIHtcclxuXHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XHJcblx0XHQgIFx0XHQkKFwiI2ZpeFRhYmxlXCIpLnRhYmxlSGVhZEZpeGVyKHtcImhlYWRcIiA6IGZhbHNlLCBcImxlZnRcIiA6IDF9KTtcclxuXHRcdCAgXHR9LCAxMDAwKTtcclxuXHRcdH0sXHJcblx0XHRnZXRTZXJ2aWNlRGV0YWlscygpIHtcclxuXHRcdFx0dGhpcy5sb2FkaW5nID0gdHJ1ZTtcclxuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvc29ydGVkU2VydmljZXMnKVxyXG5cdFx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0ICAgICAgICAgIHRoaXMuc2VydmljZXMgPSByZXN1bHQuZGF0YTtcclxuXHRcdCAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuXHRcdCAgXHRcdFx0XHR0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuXHJcblx0XHQgIFx0XHRcdFx0JChcIiNmaXhUYWJsZVwiKS50YWJsZUhlYWRGaXhlcih7XCJoZWFkXCIgOiBmYWxzZSwgXCJsZWZ0XCIgOiAxfSk7XHJcblxyXG5cdFx0ICBcdFx0XHRcdGxldCBwb3NpdGlvbiA9IDA7XHJcblx0XHQgIFx0XHRcdFx0JChcIiNmaXhUYWJsZS13cmFwcGVyXCIpLmFuaW1hdGUoe3Njcm9sbExlZnQ6IHBvc2l0aW9ufSwgMTAwKTtcclxuXHRcdCAgXHRcdFx0fSwgMTAwMCk7XHJcblx0XHQgICAgXHR9KTtcclxuXHRcdH0sXHJcblx0XHRmaWx0ZXIoKSB7XHJcblx0XHRcdHRoaXMuZ2V0U2VydmljZURldGFpbHMoKTtcclxuXHRcdH1cclxuXHR9LFxyXG5cclxuXHRjcmVhdGVkKCkge1xyXG5cdFx0dGhpcy5nZXRTZXJ2aWNlUHJvZmlsZXMoKTtcclxuXHJcblx0XHR0aGlzLmdldFNlcnZpY2VEZXRhaWxzKCk7XHJcblx0XHQkKCcuaW5wdXRDb3N0JykuaGlkZSgpO1xyXG5cdH0sXHJcblxyXG5cdGNvbXB1dGVkOiB7XHJcblx0ICAgIGZpbHRlcmVkU2VydmljZXMoKSB7XHJcblx0ICAgICAgXHRyZXR1cm4gdGhpcy5zZXJ2aWNlcy5maWx0ZXIoc2VydmljZSA9PiB7XHJcblx0ICAgICAgICAgXHRyZXR1cm4gKHNlcnZpY2UuZGV0YWlsLnRvTG93ZXJDYXNlKCkuaW5kZXhPZih0aGlzLnNlYXJjaC50b0xvd2VyQ2FzZSgpKSA+IC0xKTtcclxuXHQgICAgICBcdH0pO1xyXG5cdCAgICB9XHJcblx0fVxyXG59KTtcclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2Evc2VydmljZXMvcHJvZmlsZXMuanMiXSwic291cmNlUm9vdCI6IiJ9