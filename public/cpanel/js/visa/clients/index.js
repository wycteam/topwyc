/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 477);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 187:
/***/ (function(module, exports, __webpack_require__) {

var data = window.Laravel.user;

var Clients = new Vue({
	el: '#client',
	data: {
		groups: [],

		clients: []
	},
	methods: {
		changeUrl: function changeUrl() {
			var cpanelUrl = window.Laravel.cpanel_url;
			if ($('#email').val() == '') {
				$('#form-client').attr('action', 'http://' + cpanelUrl + '/visa/client/store');
			} else {
				$('#form-client').attr('action', 'http://' + cpanelUrl + '/visa/client/store2');
			}
		}
	},
	created: function created() {
		var _this = this;

		axios.get('/visa/client/showGroup').then(function (result) {
			_this.groups = result.data;

			$(".select2").select2({
				placeholder: "Select group",
				allowClear: true
			});

			$(".select2-selection").css('height', '34px');
		});
	}
});

new Vue({

	el: '#app',

	data: {
		temporaryClientIds: [],

		setting: data.access_control[0].setting,
		permissions: data.permissions,
		selpermissions: [{
			addNewClient: 0,
			addTemporaryClient: 0,
			balance: 0,
			collectables: 0,
			viewAction: 0
		}]
	},

	methods: {
		callDatatable: function callDatatable() {
			$('#lists').DataTable().destroy();

			setTimeout(function () {
				$('#lists').DataTable({
					"ajax": "/storage/data/Clients.txt",
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"iDisplayLength": 25,
					columnDefs: [{ type: 'non-empty-string', targets: [4, 5] }]
				});
			}, 1000);
		},
		callDatatable1: function callDatatable1(a) {
			$('#lists').DataTable().destroy();

			setTimeout(function () {
				$('#lists').DataTable({
					"ajax": "/storage/data/Clients.txt",
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"iDisplayLength": 25,
					columnDefs: [{ type: 'non-empty-string', targets: [4, 5] }, { "targets": [a], "visible": false, "searchable": false }]
				});
			}, 1000);
		},
		callDatatable2: function callDatatable2(a, b) {
			$('#lists').DataTable().destroy();

			setTimeout(function () {
				$('#lists').DataTable({
					"ajax": "/storage/data/Clients.txt",
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"iDisplayLength": 25,
					columnDefs: [{ type: 'non-empty-string', targets: [4, 5] }, { "targets": [a], "visible": false, "searchable": false }, { "targets": [b], "visible": false, "searchable": false }]
				});
			}, 1000);
		},
		callDatatable3: function callDatatable3(a, b, c) {
			$('#lists').DataTable().destroy();

			setTimeout(function () {
				$('#lists').DataTable({
					"ajax": "/storage/data/Clients.txt",
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"iDisplayLength": 25,
					columnDefs: [{ type: 'non-empty-string', targets: [4, 5] }, { "targets": [a], "visible": false, "searchable": false }, { "targets": [b], "visible": false, "searchable": false }, { "targets": [c], "visible": false, "searchable": false }]
				});
			}, 1000);
		}
	},

	created: function created() {
		var x = this.permissions.filter(function (obj) {
			return obj.type === 'Manage Clients';
		});

		var y = x.filter(function (obj) {
			return obj.name === 'Add New Client';
		});

		if (y.length == 0) this.selpermissions.addNewClient = 0;else this.selpermissions.addNewClient = 1;

		var y = x.filter(function (obj) {
			return obj.name === 'Add Temporary Client';
		});

		if (y.length == 0) this.selpermissions.addTemporaryClient = 0;else this.selpermissions.addTemporaryClient = 1;

		var y = x.filter(function (obj) {
			return obj.name === 'Balance';
		});

		if (y.length == 0) this.selpermissions.balance = 0;else this.selpermissions.balance = 1;

		var y = x.filter(function (obj) {
			return obj.name === 'Collectables';
		});

		if (y.length == 0) this.selpermissions.collectables = 0;else this.selpermissions.collectables = 1;

		var y = x.filter(function (obj) {
			return obj.name === 'View Action';
		});

		if (y.length == 0) this.selpermissions.viewAction = 0;else this.selpermissions.viewAction = 1;

		if (this.selpermissions.balance == 1 && this.selpermissions.collectables == 1 && this.selpermissions.viewAction == 1) {
			this.callDatatable();
		}

		if (this.selpermissions.balance == 0 && this.selpermissions.collectables == 1 && this.selpermissions.viewAction == 1) {
			this.callDatatable1(2);
		}

		if (this.selpermissions.balance == 1 && this.selpermissions.collectables == 0 && this.selpermissions.viewAction == 1) {
			this.callDatatable1(3);
		}

		if (this.selpermissions.balance == 1 && this.selpermissions.collectables == 1 && this.selpermissions.viewAction == 0) {
			this.callDatatable1(6);
		}

		if (this.selpermissions.balance == 0 && this.selpermissions.collectables == 0 && this.selpermissions.viewAction == 1) {
			this.callDatatable2(2, 3);
		}

		if (this.selpermissions.balance == 0 && this.selpermissions.collectables == 1 && this.selpermissions.viewAction == 0) {
			this.callDatatable2(2, 6);
		}

		if (this.selpermissions.balance == 1 && this.selpermissions.collectables == 0 && this.selpermissions.viewAction == 0) {
			this.callDatatable2(3, 6);
		}

		if (this.selpermissions.balance == 0 && this.selpermissions.collectables == 0 && this.selpermissions.viewAction == 0) {
			this.callDatatable3(2, 3, 6);
		}
	},


	components: {
		'add-temporary-client': __webpack_require__(338)
	},

	mounted: function mounted() {
		$('#newlyAddedTemporaryClientModal').on('hidden.bs.modal', function (e) {
			toastr.success('Temporary client successfully added.', null, {
				timeOut: 1000,
				onHidden: function onHidden() {
					window.location.reload();
				}
			});
		});
	}
});

$(document).ready(function () {

	jQuery.extend(jQuery.fn.dataTableExt.oSort, {
		"non-empty-string-asc": function nonEmptyStringAsc(str1, str2) {
			if (str1 == '<span style="display:none;">--</span>') return 1;
			if (str2 == '<span style="display:none;">--</span>') return -1;
			return str1 < str2 ? -1 : str1 > str2 ? 1 : 0;
		},

		"non-empty-string-desc": function nonEmptyStringDesc(str1, str2) {
			if (str1 == '<span style="display:none;">--</span>') return 1;
			if (str2 == '<span style="display:none;">--</span>') return -1;
			return str1 < str2 ? 1 : str1 > str2 ? -1 : 0;
		}
	});

	$('#first_name, #middle_name, #last_name').keypress(function (event) {
		var inputValue = event.charCode;
		//alert(inputValue);
		if (!(inputValue > 64 && inputValue < 91 || inputValue > 96 && inputValue < 123 || inputValue == 32 || inputValue == 0)) {
			event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});

	// $('#group_id').on('change', function() {
	//     alert($(this).val());
	// });

	$('form#form-client').on('submit', function (e) {
		var firstName = $('input#first_name').val();
		var lastName = $('input#last_name').val();
		var gender = $('select[name=gender]').val();
		var birthdate = $('input#birth_date').val();
		var contactNumber = $('input#contact_number').val();
		var passport = $('input#passport').val();
		var group_id = $('#group_id').val();

		$.ajax({
			url: 'validationBeforeSubmit',
			type: 'get',
			data: {
				firstName: firstName,
				lastName: lastName,
				gender: gender,
				birthdate: birthdate,
				contactNumber: contactNumber,
				passport: passport,
				group_id: group_id
			},
			async: false,
			success: function success(data) {
				var data = JSON.parse(data);

				Clients.clients = data.users;

				if (data.binded) {
					toastr.error('Contact number is already binded in the visa app.');
					e.preventDefault();
				} else if (data.duplicateEntry) {
					$('#duplicateClientsListModal').modal('show');
					e.preventDefault();
				}
			}
		});
	});
});

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			temporaryClientForm: new Form({
				contact_numbers: [{ country_code: '', value: '', action: '', process: '', first_name: '', last_name: '', passport: '', birthdate: '', gender: '', ifExist: false, ifBinded: false, ifGroupLeader: false }]
			}, { baseURL: axiosAPIv1.defaults.baseURL }),

			screen: 1,

			ifExistArray: [],
			ifBindedArray: [],
			ifGroupLeaderArray: [],
			userArray: []
		};
	},


	methods: {
		resetTemporaryClientForm: function resetTemporaryClientForm() {
			this.temporaryClientForm = new Form({
				contact_numbers: [{ country_code: '', value: '', action: '', process: '', first_name: '', last_name: '', passport: '', birthdate: '', gender: '', ifExist: false, ifBinded: false, ifGroupLeader: false }]
			}, { baseURL: axiosAPIv1.defaults.baseURL });
		},
		resetArray: function resetArray() {
			this.ifExistArray = [];
			this.ifBindedArray = [];
			this.ifGroupLeaderArray = [];
			this.userArray = [];
		},
		validateContactNumbers: function validateContactNumbers() {
			var isValid = true;

			this.temporaryClientForm.contact_numbers.forEach(function (contactNumber, index) {
				if (contactNumber.country_code == '') {
					isValid = false;
					toastr.error('Contact Number ' + (index + 1) + ' country code is invalid.');
				}

				if (contactNumber.value == '') {
					isValid = false;
					toastr.error('Contact Number ' + (index + 1) + ' is invalid.');
				}

				if (contactNumber.value.length != 10) {
					isValid = false;
					toastr.error('Contact Number ' + (index + 1) + ' must be 10 digit number.');
				}
			});

			return isValid;
		},
		addTemporaryClientValidation: function addTemporaryClientValidation() {
			var _this = this;

			var valid = this.validateContactNumbers();

			if (valid) {
				this.temporaryClientForm.submit('post', '/visa/client/add-temporary-validation').then(function (response) {
					if (response.success) {
						_this.screen = 2;

						_this.ifExistArray = response.ifExistArray;
						_this.ifBindedArray = response.ifBindedArray;
						_this.ifGroupLeaderArray = response.ifGroupLeaderArray;
						_this.userArray = response.userArray;

						var count = _this.temporaryClientForm.contact_numbers.length;
						for (var i = 0; i < count; i++) {
							if (_this.ifBindedArray[i] || _this.ifGroupLeaderArray[i]) {
								_this.temporaryClientForm.contact_numbers[i].action = 'use_existing_profile';
							} else {
								_this.temporaryClientForm.contact_numbers[i].action = 'proceed_anyway';
							}

							_this.temporaryClientForm.contact_numbers[i].process = 'complete';

							_this.temporaryClientForm.contact_numbers[i].ifExist = _this.ifExistArray[i];
							_this.temporaryClientForm.contact_numbers[i].ifBinded = _this.ifBindedArray[i];
							_this.temporaryClientForm.contact_numbers[i].ifGroupLeader = _this.ifGroupLeaderArray[i];
						}
					}
				});
			}
		},
		validateInputs: function validateInputs() {
			var isValid = true;

			this.temporaryClientForm.contact_numbers.forEach(function (contactNumber, index) {
				if (contactNumber.action == '') {
					isValid = false;
					toastr.error('The action field is required.', contactNumber.country_code + contactNumber.value);
				}

				if (contactNumber.process == '' && (contactNumber.action == 'proceed_anyway' || !ifExistArray[index])) {
					isValid = false;
					toastr.error('The add details field is required.', contactNumber.country_code + contactNumber.value);
				}

				if (contactNumber.first_name == '' && contactNumber.process == 'detail') {
					isValid = false;
					toastr.error('The first name field is required.', contactNumber.country_code + contactNumber.value);
				}

				if (contactNumber.last_name == '' && contactNumber.process == 'detail') {
					isValid = false;
					toastr.error('The last name field is required.', contactNumber.country_code + contactNumber.value);
				}

				if (contactNumber.passport == '' && contactNumber.process == 'detail') {
					isValid = false;
					toastr.error('The passport field is required.', contactNumber.country_code + contactNumber.value);
				}

				if (contactNumber.birthdate == '' && contactNumber.process == 'detail') {
					isValid = false;
					toastr.error('The birthdate field is required.', contactNumber.country_code + contactNumber.value);
				}

				if (contactNumber.gender == '' && contactNumber.process == 'detail') {
					isValid = false;
					toastr.error('The gender field is required.', contactNumber.country_code + contactNumber.value);
				}
			});

			return isValid;
		},
		addTemporaryClient: function addTemporaryClient() {
			var _this2 = this;

			var valid = this.validateInputs();

			if (valid) {
				$('button#temporaryClientFormSubmitBtn').attr('disabled', 'disabled');

				this.temporaryClientForm.submit('post', '/visa/client/add-temporary').then(function (response) {
					if (response.success) {
						_this2.resetTemporaryClientForm();

						_this2.resetArray();

						_this2.screen = 1;

						$('#addTemporaryClientModal').modal('hide');

						toastr.success(response.message);

						setTimeout(function () {
							location.reload();
						}, 3000);
					}

					$('button#temporaryClientFormSubmitBtn').removeAttr('disabled');
				});
			}
		},
		addContactNumber: function addContactNumber() {
			this.temporaryClientForm.contact_numbers.push({ country_code: '', value: '', action: '', process: '', first_name: '', last_name: '', passport: '', birthdate: '', gender: '', ifExist: false, ifBinded: false, ifGroupLeader: false });
		},
		removeContactNumber: function removeContactNumber(index) {
			this.temporaryClientForm.contact_numbers.splice(index, 1);
		}
	},

	mounted: function mounted() {
		var _this3 = this;

		$('#addTemporaryClientModal').on('hidden.bs.modal', function (e) {
			_this3.resetTemporaryClientForm();

			_this3.resetArray();

			_this3.screen = 1;
		});
	}
});

/***/ }),

/***/ 296:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-1905a9e5] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-1905a9e5] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["AddTemporaryClient.vue?a051bc08"],"names":[],"mappings":";AA2UA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"AddTemporaryClient.vue","sourcesContent":["<template>\n\n\t<form id=\"add-temporary-client-form\" class=\"form-horizontal\" @submit.prevent=\"addTemporaryClient\">\n\t\t        \n\t\t<div v-show=\"screen == 1\">\n\t\t\t<button type=\"button\" class=\"btn pull-left\" @click=\"addContactNumber\">\n\t\t\t\t<i class=\"fa fa-plus\"></i> Add Contact Number\n\t\t\t</button>\n\n\t\t\t<div style=\"height:20px; clear:both;\"></div>\n\n\t\t\t<div v-for=\"(contact_number, index) in temporaryClientForm.contact_numbers\" class=\"form-group\">\n\t\t\t\t<div class=\"col-sm-1\">\n\t\t\t\t\t<button v-if=\"index != 0\" type=\"button\" class=\"btn btn-danger pull-right\" @click=\"removeContactNumber(index)\">\n\t\t\t\t\t\t<i class=\"fa fa-times\"></i>\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t\t<label for=\"contact_number\" class=\"col-sm-3 control-label\">({{ index+1 }}) Contact No.:</label>\n\t\t\t\t<div class=\"col-sm-3\">\n\t\t\t\t\t<select class=\"form-control\" name=\"country_code[]\" v-model=\"contact_number.country_code\">\n\t\t\t\t\t\t<option value=\"+63\">+63</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"contact_number[]\" v-model=\"contact_number.value\">\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<button type=\"button\" class=\"btn btn-primary pull-right\" @click=\"addTemporaryClientValidation\">\n\t\t\t\tContinue\n\t\t\t</button>\n\t\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\t    </div>\n\n\t    <div v-show=\"screen == 2\">\n\t    \t<div class=\"panel panel-default\" v-for=\"(contact_number, index) in temporaryClientForm.contact_numbers\">\n                <div class=\"panel-heading\">\n                    {{ contact_number.country_code }}{{ contact_number.value }}\n                    <a v-if=\"ifExistArray[index]\" :href=\"'/visa/client/' + userArray[index].id\" class=\"pull-right\" target=\"_blank\">\n                    \tView Existing Profile\n                    </a>\n                </div>\n                <div class=\"panel-body\">\n                \t<form class=\"form-horizontal\">\n\n                \t\t<div class=\"form-group\">\n\t\t\t\t\t    \t<label class=\"col-sm-2 control-label\">Status:</label>\n\t\t\t\t\t    \t<div class=\"col-sm-10\">\n\t\t\t\t\t    \t\t<div style=\"height:5px;clear:both;\"></div>\n\n\t\t\t\t\t      \t\t<span class=\"label\" \n\t\t\t\t\t      \t\t\t:class=\"{'label-primary':!ifExistArray[index], 'label-danger':ifExistArray[index]}\">\n\t\t\t\t\t      \t\t\t{{ (ifExistArray[index]) ? 'Existing' : 'Not Existing' }}\n\t\t\t\t\t      \t\t</span>\n\n\t\t\t\t\t      \t\t<span class=\"label\" \n\t\t\t\t\t      \t\t\t:class=\"{'label-primary':!ifBindedArray[index], 'label-danger':ifBindedArray[index]}\"\n\t\t\t\t\t      \t\t\tstyle=\"margin-left:10px;\">\n\t\t\t\t\t      \t\t\t{{ (ifBindedArray[index]) ? 'Binded' : 'Not Binded' }}\n\t\t\t\t\t      \t\t</span>\n\n\t\t\t\t\t      \t\t<span class=\"label\" \n\t\t\t\t\t      \t\t\t:class=\"{'label-primary':!ifGroupLeaderArray[index], 'label-danger':ifGroupLeaderArray[index]}\" style=\"margin-left:10px;\">\n\t\t\t\t\t      \t\t\t{{ (ifGroupLeaderArray[index]) ? 'Group Leader' : 'Not Group Leader' }}\n\t\t\t\t\t      \t\t</span>\n\t\t\t\t\t    \t</div>\n\t\t\t\t\t  \t</div>\n\n\t\t\t\t\t  \t<div v-show=\"ifExistArray[index]\" class=\"form-group\">\n\t\t\t\t\t    \t<label class=\"col-sm-2 control-label\">Action:</label>\n\t\t\t\t\t    \t<div class=\"col-sm-10\">\n\t\t\t\t\t      \t\t<label class=\"radio-inline\">\n\t\t\t\t\t\t\t\t  \t<input type=\"radio\" :name=\"'action-'+index\" value=\"proceed_anyway\" v-model=\"contact_number.action\" :disabled=\"ifBindedArray[index] || ifGroupLeaderArray[index]\" /> Proceed Anyway\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"radio-inline\">\n\t\t\t\t\t\t\t\t  \t<input type=\"radio\" :name=\"'action-'+index\" value=\"use_existing_profile\" v-model=\"contact_number.action\" /> \n\t\t\t\t\t\t\t\t  \tUse Existing Profile\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t    \t</div>\n\t\t\t\t\t  \t</div>\n\n\t\t\t\t\t  \t<div v-show=\"!ifExistArray[index] || contact_number.action == 'proceed_anyway'\" class=\"form-group\">\n\t\t\t\t\t\t    <label class=\"col-sm-2 control-label\">Add Details:</label>\n\t\t\t\t\t\t    <div class=\"col-sm-10\">\n\t\t\t\t\t\t      \t<label class=\"radio-inline\">\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" :name=\"'process-'+index\" value=\"complete\" v-model=\"contact_number.process\" /> No\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<label class=\"radio-inline\">\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" :name=\"'process-'+index\" value=\"detail\" v-model=\"contact_number.process\" /> Yes\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t    </div>\n\t\t\t\t\t  \t</div>\n\n\t\t\t\t\t  \t<div v-show=\"contact_number.process == 'detail'\" class=\"form-group\">\n\t\t\t\t\t  \t\t<div class=\"form-group\">\n\t\t\t\t\t\t    \t<label class=\"col-sm-2 control-label\">First Name</label>\n\t\t\t\t\t\t    \t<div class=\"col-sm-10\">\n\t\t\t\t\t\t      \t\t<input type=\"text\" class=\"form-control\" name=\"first_name\" v-model=\"contact_number.first_name\" placeholder=\"First Name\" autocomplete=\"off\" />\n\t\t\t\t\t\t    \t</div>\n\t\t\t\t\t\t  \t</div>\n\n\t\t\t\t\t\t  \t<div class=\"form-group\">\n\t\t\t\t\t\t    \t<label class=\"col-sm-2 control-label\">Last Name</label>\n\t\t\t\t\t\t    \t<div class=\"col-sm-10\">\n\t\t\t\t\t\t      \t\t<input type=\"text\" class=\"form-control\" name=\"last_name\" v-model=\"contact_number.last_name\" placeholder=\"Last Name\" autocomplete=\"off\" />\n\t\t\t\t\t\t    \t</div>\n\t\t\t\t\t\t  \t</div>\n\n\t\t\t\t\t\t  \t<div class=\"form-group\">\n\t\t\t\t\t\t    \t<label class=\"col-sm-2 control-label\">Passport</label>\n\t\t\t\t\t\t    \t<div class=\"col-sm-10\">\n\t\t\t\t\t\t      \t\t<input type=\"text\" class=\"form-control\" name=\"passport\" v-model=\"contact_number.passport\" placeholder=\"Passport\" autocomplete=\"off\" />\n\t\t\t\t\t\t    \t</div>\n\t\t\t\t\t\t  \t</div>\n\n\t\t\t\t\t\t  \t<div class=\"form-group\">\n\t\t\t\t\t\t    \t<label class=\"col-sm-2 control-label\">Birthdate</label>\n\t\t\t\t\t\t    \t<div class=\"col-sm-10\">\n\t\t\t\t\t\t      \t\t<input type=\"date\" class=\"form-control\" name=\"birthdate\" v-model=\"contact_number.birthdate\" autocomplete=\"off\" />\n\t\t\t\t\t\t    \t</div>\n\t\t\t\t\t\t  \t</div>\n\n\t\t\t\t\t\t  \t<div class=\"form-group\">\n\t\t\t\t\t\t    \t<label class=\"col-sm-2 control-label\">Gender</label>\n\t\t\t\t\t\t    \t<div class=\"col-sm-10\">\n\t\t\t\t\t\t      \t\t<select class=\"form-control\" name=\"gender\" v-model=\"contact_number.gender\">\n\t\t\t\t\t\t      \t\t\t<option value=\"Male\">Male</option>\n\t\t\t\t\t\t      \t\t\t<option value=\"Female\">Female</option>\n\t\t\t\t\t\t      \t\t</select>\n\t\t\t\t\t\t    \t</div>\n\t\t\t\t\t\t  \t</div>\n\t\t\t\t\t  \t</div>\n\n\t\t\t\t\t</form>\n                </div>\n            </div>\n\n\t    \t<button type=\"submit\" id=\"temporaryClientFormSubmitBtn\" class=\"btn btn-primary pull-right\">\n\t\t\t\tSave\n\t\t\t</button>\n\t\t\t<button type=\"button\" class=\"btn btn-default pull-right m-r-10\" @click=\"screen = 1\">Back</button>\n\t\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\t    </div>\n\n\t</form>\n\n</template>\n\n<script>\n\n    export default {\n    \tdata() {\n    \t\treturn {\n    \t\t\ttemporaryClientForm: new Form({\n\t\t\t\t\tcontact_numbers: [{country_code: '', value: '', action: '', process: '', first_name: '', last_name: '', passport: '', birthdate:'', gender: '', ifExist: false, ifBinded: false, ifGroupLeader: false}]\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL }),\n\n\t\t\t\tscreen: 1,\n\n\t\t\t\tifExistArray: [],\n\t\t\t\tifBindedArray: [],\n\t\t\t\tifGroupLeaderArray: [],\n\t\t\t\tuserArray: [],\n    \t\t}\n    \t},\n\n    \tmethods: {\n    \t\tresetTemporaryClientForm() {\n\t    \t\tthis.temporaryClientForm = new Form({\n\t\t\t\t\tcontact_numbers: [{country_code: '', value: '', action: '', process: '', first_name: '', last_name: '', passport: '', birthdate:'', gender: '', ifExist: false, ifBinded: false, ifGroupLeader: false}]\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL });\n\t    \t},\n\n\t    \tresetArray() {\n\t    \t\tthis.ifExistArray = [];\n\t    \t\tthis.ifBindedArray = [];\n\t    \t\tthis.ifGroupLeaderArray = [];\n\t    \t\tthis.userArray = [];\n\t    \t},\n\n\t    \tvalidateContactNumbers() {\n\t    \t\tlet isValid = true;\n\n\t    \t\tthis.temporaryClientForm.contact_numbers.forEach((contactNumber, index) => {\n\t    \t\t\tif(contactNumber.country_code == '') {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('Contact Number ' + (index+1) + ' country code is invalid.');\n\t    \t\t\t}\n\n\t    \t\t\tif(contactNumber.value == '') {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('Contact Number ' + (index+1) + ' is invalid.');\n\t    \t\t\t}\n\n\t    \t\t\tif(contactNumber.value.length != 10) {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('Contact Number ' + (index+1) + ' must be 10 digit number.');\n\t    \t\t\t}\n\t    \t\t});\n\n\t    \t\treturn isValid;\n\t    \t},\n\n\t    \taddTemporaryClientValidation() {\n\t    \t\tlet valid = this.validateContactNumbers();\n\n\t    \t\tif(valid) {\n\t\t\t\t\tthis.temporaryClientForm.submit('post', '/visa/client/add-temporary-validation')\n\t\t\t\t        .then(response => {\n\t\t\t\t        \tif(response.success) {\n\t\t\t\t        \t\tthis.screen = 2;\n\n\t\t\t\t        \t\tthis.ifExistArray = response.ifExistArray;\n\t\t\t\t        \t\tthis.ifBindedArray = response.ifBindedArray;\n\t\t\t\t        \t\tthis.ifGroupLeaderArray = response.ifGroupLeaderArray;\n\t\t\t\t        \t\tthis.userArray = response.userArray;\n\n\t\t\t\t        \t\tlet count = this.temporaryClientForm.contact_numbers.length;\n\t\t\t\t        \t\tfor(let i=0; i<count; i++) {\n\t\t\t\t        \t\t\tif(this.ifBindedArray[i] || this.ifGroupLeaderArray[i]) {\n\t\t\t\t        \t\t\t\tthis.temporaryClientForm.contact_numbers[i].action = 'use_existing_profile';\n\t\t\t\t        \t\t\t} else {\n\t\t\t\t        \t\t\t\tthis.temporaryClientForm.contact_numbers[i].action = 'proceed_anyway';\n\t\t\t\t        \t\t\t}\n\n\t\t\t\t        \t\t\tthis.temporaryClientForm.contact_numbers[i].process = 'complete';\n\n\t\t\t\t        \t\t\tthis.temporaryClientForm.contact_numbers[i].ifExist = this.ifExistArray[i];\n\t\t\t\t        \t\t\tthis.temporaryClientForm.contact_numbers[i].ifBinded = this.ifBindedArray[i];\n\t\t\t\t        \t\t\tthis.temporaryClientForm.contact_numbers[i].ifGroupLeader = this.ifGroupLeaderArray[i];\n\t\t\t\t        \t\t}\n\t\t\t\t        \t}\n\t\t\t\t        });\n\t    \t\t}\n\t    \t},\n\n\t    \tvalidateInputs() {\n\t    \t\tlet isValid = true;\n\n\t    \t\tthis.temporaryClientForm.contact_numbers.forEach((contactNumber, index) => {\n\t    \t\t\tif(contactNumber.action == '') {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('The action field is required.', contactNumber.country_code + contactNumber.value);\n\t    \t\t\t}\n\n\t    \t\t\tif(contactNumber.process == '' && (contactNumber.action == 'proceed_anyway' || !ifExistArray[index]) ) {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('The add details field is required.', contactNumber.country_code + contactNumber.value);\n\t    \t\t\t}\n\n\t    \t\t\tif(contactNumber.first_name == '' && contactNumber.process == 'detail') {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('The first name field is required.', contactNumber.country_code + contactNumber.value);\n\t    \t\t\t}\n\n\t    \t\t\tif(contactNumber.last_name == '' && contactNumber.process == 'detail') {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('The last name field is required.', contactNumber.country_code + contactNumber.value);\n\t    \t\t\t}\n\n\t    \t\t\tif(contactNumber.passport == '' && contactNumber.process == 'detail') {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('The passport field is required.', contactNumber.country_code + contactNumber.value);\n\t    \t\t\t}\n\n\t    \t\t\tif(contactNumber.birthdate == '' && contactNumber.process == 'detail') {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('The birthdate field is required.', contactNumber.country_code + contactNumber.value);\n\t    \t\t\t}\n\n\t    \t\t\tif(contactNumber.gender == '' && contactNumber.process == 'detail') {\n\t    \t\t\t\tisValid = false;\n\t    \t\t\t\ttoastr.error('The gender field is required.', contactNumber.country_code + contactNumber.value);\n\t    \t\t\t}\n\t    \t\t});\n\n\t    \t\treturn isValid;\n\t    \t},\n\n\t    \taddTemporaryClient() {\n\t    \t\tlet valid = this.validateInputs();\n\n\t    \t\tif(valid) {\n\t    \t\t\t$('button#temporaryClientFormSubmitBtn').attr('disabled', 'disabled');\n\n\t    \t\t\tthis.temporaryClientForm.submit('post', '/visa/client/add-temporary')\n\t\t\t\t        .then(response => {\n\t\t\t\t            if(response.success) {\n\t\t\t\t            \tthis.resetTemporaryClientForm();\n\n\t\t\t\t\t\t\t\tthis.resetArray();\n\n\t\t\t\t\t\t\t\tthis.screen = 1;\n\n\t\t\t\t                $('#addTemporaryClientModal').modal('hide');\n\n\t\t\t\t                toastr.success(response.message);\n\n\t\t\t\t                setTimeout(() => {\n\t\t\t\t                \tlocation.reload();\n\t\t\t\t                }, 3000);\n\t\t\t\t            }\n\n\t\t\t\t            $('button#temporaryClientFormSubmitBtn').removeAttr('disabled');\n\t\t\t\t        });\n\t    \t\t}\n\t\t\t},\n\n\t\t\taddContactNumber() {\n\t\t\t\tthis.temporaryClientForm.contact_numbers.push({country_code: '', value: '', action: '', process: '', first_name: '', last_name: '', passport: '', birthdate:'', gender: '', ifExist: false, ifBinded: false, ifGroupLeader: false});\n\t\t\t},\n\n\t\t\tremoveContactNumber(index) {\n\t\t\t\tthis.temporaryClientForm.contact_numbers.splice(index, 1);\n\t\t\t}\n    \t},\n\n    \tmounted() {\n    \t\t$('#addTemporaryClientModal').on('hidden.bs.modal', (e) => {\n\t\t\t\tthis.resetTemporaryClientForm();\n\n\t\t\t\tthis.resetArray();\n\n\t\t\t\tthis.screen = 1;\n\t\t\t});\n    \t}\n    }\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(7)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 338:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(434)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(245),
  /* template */
  __webpack_require__(381),
  /* scopeId */
  "data-v-1905a9e5",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/AddTemporaryClient.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AddTemporaryClient.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1905a9e5", Component.options)
  } else {
    hotAPI.reload("data-v-1905a9e5", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    attrs: {
      "id": "add-temporary-client-form"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addTemporaryClient($event)
      }
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 1),
      expression: "screen == 1"
    }]
  }, [_c('button', {
    staticClass: "btn pull-left",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.addContactNumber
    }
  }, [_c('i', {
    staticClass: "fa fa-plus"
  }), _vm._v(" Add Contact Number\n\t\t\t")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _vm._l((_vm.temporaryClientForm.contact_numbers), function(contact_number, index) {
    return _c('div', {
      staticClass: "form-group"
    }, [_c('div', {
      staticClass: "col-sm-1"
    }, [(index != 0) ? _c('button', {
      staticClass: "btn btn-danger pull-right",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.removeContactNumber(index)
        }
      }
    }, [_c('i', {
      staticClass: "fa fa-times"
    })]) : _vm._e()]), _vm._v(" "), _c('label', {
      staticClass: "col-sm-3 control-label",
      attrs: {
        "for": "contact_number"
      }
    }, [_vm._v("(" + _vm._s(index + 1) + ") Contact No.:")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-3"
    }, [_c('select', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.country_code),
        expression: "contact_number.country_code"
      }],
      staticClass: "form-control",
      attrs: {
        "name": "country_code[]"
      },
      on: {
        "change": function($event) {
          var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
            return o.selected
          }).map(function(o) {
            var val = "_value" in o ? o._value : o.value;
            return val
          });
          _vm.$set(contact_number, "country_code", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
        }
      }
    }, [_c('option', {
      attrs: {
        "value": "+63"
      }
    }, [_vm._v("+63")])])]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-5"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.value),
        expression: "contact_number.value"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "number",
        "name": "contact_number[]"
      },
      domProps: {
        "value": (contact_number.value)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(contact_number, "value", $event.target.value)
        }
      }
    })])])
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.addTemporaryClientValidation
    }
  }, [_vm._v("\n\t\t\t\tContinue\n\t\t\t")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])], 2), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.screen == 2),
      expression: "screen == 2"
    }]
  }, [_vm._l((_vm.temporaryClientForm.contact_numbers), function(contact_number, index) {
    return _c('div', {
      staticClass: "panel panel-default"
    }, [_c('div', {
      staticClass: "panel-heading"
    }, [_vm._v("\n                    " + _vm._s(contact_number.country_code) + _vm._s(contact_number.value) + "\n                    "), (_vm.ifExistArray[index]) ? _c('a', {
      staticClass: "pull-right",
      attrs: {
        "href": '/visa/client/' + _vm.userArray[index].id,
        "target": "_blank"
      }
    }, [_vm._v("\n                    \tView Existing Profile\n                    ")]) : _vm._e()]), _vm._v(" "), _c('div', {
      staticClass: "panel-body"
    }, [_c('form', {
      staticClass: "form-horizontal"
    }, [_c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-sm-2 control-label"
    }, [_vm._v("Status:")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-10"
    }, [_c('div', {
      staticStyle: {
        "height": "5px",
        "clear": "both"
      }
    }), _vm._v(" "), _c('span', {
      staticClass: "label",
      class: {
        'label-primary': !_vm.ifExistArray[index], 'label-danger': _vm.ifExistArray[index]
      }
    }, [_vm._v("\n\t\t\t\t\t      \t\t\t" + _vm._s((_vm.ifExistArray[index]) ? 'Existing' : 'Not Existing') + "\n\t\t\t\t\t      \t\t")]), _vm._v(" "), _c('span', {
      staticClass: "label",
      class: {
        'label-primary': !_vm.ifBindedArray[index], 'label-danger': _vm.ifBindedArray[index]
      },
      staticStyle: {
        "margin-left": "10px"
      }
    }, [_vm._v("\n\t\t\t\t\t      \t\t\t" + _vm._s((_vm.ifBindedArray[index]) ? 'Binded' : 'Not Binded') + "\n\t\t\t\t\t      \t\t")]), _vm._v(" "), _c('span', {
      staticClass: "label",
      class: {
        'label-primary': !_vm.ifGroupLeaderArray[index], 'label-danger': _vm.ifGroupLeaderArray[index]
      },
      staticStyle: {
        "margin-left": "10px"
      }
    }, [_vm._v("\n\t\t\t\t\t      \t\t\t" + _vm._s((_vm.ifGroupLeaderArray[index]) ? 'Group Leader' : 'Not Group Leader') + "\n\t\t\t\t\t      \t\t")])])]), _vm._v(" "), _c('div', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (_vm.ifExistArray[index]),
        expression: "ifExistArray[index]"
      }],
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-sm-2 control-label"
    }, [_vm._v("Action:")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-10"
    }, [_c('label', {
      staticClass: "radio-inline"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.action),
        expression: "contact_number.action"
      }],
      attrs: {
        "type": "radio",
        "name": 'action-' + index,
        "value": "proceed_anyway",
        "disabled": _vm.ifBindedArray[index] || _vm.ifGroupLeaderArray[index]
      },
      domProps: {
        "checked": _vm._q(contact_number.action, "proceed_anyway")
      },
      on: {
        "change": function($event) {
          _vm.$set(contact_number, "action", "proceed_anyway")
        }
      }
    }), _vm._v(" Proceed Anyway\n\t\t\t\t\t\t\t\t")]), _vm._v(" "), _c('label', {
      staticClass: "radio-inline"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.action),
        expression: "contact_number.action"
      }],
      attrs: {
        "type": "radio",
        "name": 'action-' + index,
        "value": "use_existing_profile"
      },
      domProps: {
        "checked": _vm._q(contact_number.action, "use_existing_profile")
      },
      on: {
        "change": function($event) {
          _vm.$set(contact_number, "action", "use_existing_profile")
        }
      }
    }), _vm._v(" \n\t\t\t\t\t\t\t\t  \tUse Existing Profile\n\t\t\t\t\t\t\t\t")])])]), _vm._v(" "), _c('div', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (!_vm.ifExistArray[index] || contact_number.action == 'proceed_anyway'),
        expression: "!ifExistArray[index] || contact_number.action == 'proceed_anyway'"
      }],
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-sm-2 control-label"
    }, [_vm._v("Add Details:")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-10"
    }, [_c('label', {
      staticClass: "radio-inline"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.process),
        expression: "contact_number.process"
      }],
      attrs: {
        "type": "radio",
        "name": 'process-' + index,
        "value": "complete"
      },
      domProps: {
        "checked": _vm._q(contact_number.process, "complete")
      },
      on: {
        "change": function($event) {
          _vm.$set(contact_number, "process", "complete")
        }
      }
    }), _vm._v(" No\n\t\t\t\t\t\t\t\t")]), _vm._v(" "), _c('label', {
      staticClass: "radio-inline"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.process),
        expression: "contact_number.process"
      }],
      attrs: {
        "type": "radio",
        "name": 'process-' + index,
        "value": "detail"
      },
      domProps: {
        "checked": _vm._q(contact_number.process, "detail")
      },
      on: {
        "change": function($event) {
          _vm.$set(contact_number, "process", "detail")
        }
      }
    }), _vm._v(" Yes\n\t\t\t\t\t\t\t\t")])])]), _vm._v(" "), _c('div', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (contact_number.process == 'detail'),
        expression: "contact_number.process == 'detail'"
      }],
      staticClass: "form-group"
    }, [_c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-sm-2 control-label"
    }, [_vm._v("First Name")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-10"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.first_name),
        expression: "contact_number.first_name"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "text",
        "name": "first_name",
        "placeholder": "First Name",
        "autocomplete": "off"
      },
      domProps: {
        "value": (contact_number.first_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(contact_number, "first_name", $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-sm-2 control-label"
    }, [_vm._v("Last Name")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-10"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.last_name),
        expression: "contact_number.last_name"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "text",
        "name": "last_name",
        "placeholder": "Last Name",
        "autocomplete": "off"
      },
      domProps: {
        "value": (contact_number.last_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(contact_number, "last_name", $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-sm-2 control-label"
    }, [_vm._v("Passport")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-10"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.passport),
        expression: "contact_number.passport"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "text",
        "name": "passport",
        "placeholder": "Passport",
        "autocomplete": "off"
      },
      domProps: {
        "value": (contact_number.passport)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(contact_number, "passport", $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-sm-2 control-label"
    }, [_vm._v("Birthdate")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-10"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.birthdate),
        expression: "contact_number.birthdate"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "date",
        "name": "birthdate",
        "autocomplete": "off"
      },
      domProps: {
        "value": (contact_number.birthdate)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(contact_number, "birthdate", $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      staticClass: "col-sm-2 control-label"
    }, [_vm._v("Gender")]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-10"
    }, [_c('select', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (contact_number.gender),
        expression: "contact_number.gender"
      }],
      staticClass: "form-control",
      attrs: {
        "name": "gender"
      },
      on: {
        "change": function($event) {
          var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
            return o.selected
          }).map(function(o) {
            var val = "_value" in o ? o._value : o.value;
            return val
          });
          _vm.$set(contact_number, "gender", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
        }
      }
    }, [_c('option', {
      attrs: {
        "value": "Male"
      }
    }, [_vm._v("Male")]), _vm._v(" "), _c('option', {
      attrs: {
        "value": "Female"
      }
    }, [_vm._v("Female")])])])])])])])])
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit",
      "id": "temporaryClientFormSubmitBtn"
    }
  }, [_vm._v("\n\t\t\t\tSave\n\t\t\t")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.screen = 1
      }
    }
  }, [_vm._v("Back")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-1905a9e5", module.exports)
  }
}

/***/ }),

/***/ 434:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(296);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("26383ec3", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-1905a9e5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddTemporaryClient.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-1905a9e5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddTemporaryClient.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 477:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(187);


/***/ }),

/***/ 7:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vfi9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qcz9kYTA0KioqKioqKiIsIndlYnBhY2s6Ly8vQWRkVGVtcG9yYXJ5Q2xpZW50LnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkVGVtcG9yYXJ5Q2xpZW50LnZ1ZT8zNzFkIiwid2VicGFjazovLy8uL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzPzZiMmIqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGRUZW1wb3JhcnlDbGllbnQudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGRUZW1wb3JhcnlDbGllbnQudnVlP2FjZWMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZFRlbXBvcmFyeUNsaWVudC52dWU/NWQxNCIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qcz9lNmFjKioqKioqKiJdLCJuYW1lcyI6WyJkYXRhIiwid2luZG93IiwiTGFyYXZlbCIsInVzZXIiLCJDbGllbnRzIiwiVnVlIiwiZWwiLCJncm91cHMiLCJjbGllbnRzIiwibWV0aG9kcyIsImNoYW5nZVVybCIsImNwYW5lbFVybCIsImNwYW5lbF91cmwiLCIkIiwidmFsIiwiYXR0ciIsImNyZWF0ZWQiLCJheGlvcyIsImdldCIsInRoZW4iLCJyZXN1bHQiLCJzZWxlY3QyIiwicGxhY2Vob2xkZXIiLCJhbGxvd0NsZWFyIiwiY3NzIiwidGVtcG9yYXJ5Q2xpZW50SWRzIiwic2V0dGluZyIsImFjY2Vzc19jb250cm9sIiwicGVybWlzc2lvbnMiLCJzZWxwZXJtaXNzaW9ucyIsImFkZE5ld0NsaWVudCIsImFkZFRlbXBvcmFyeUNsaWVudCIsImJhbGFuY2UiLCJjb2xsZWN0YWJsZXMiLCJ2aWV3QWN0aW9uIiwiY2FsbERhdGF0YWJsZSIsIkRhdGFUYWJsZSIsImRlc3Ryb3kiLCJzZXRUaW1lb3V0IiwicmVzcG9uc2l2ZSIsImNvbHVtbkRlZnMiLCJ0eXBlIiwidGFyZ2V0cyIsImNhbGxEYXRhdGFibGUxIiwiYSIsImNhbGxEYXRhdGFibGUyIiwiYiIsImNhbGxEYXRhdGFibGUzIiwiYyIsIngiLCJmaWx0ZXIiLCJvYmoiLCJ5IiwibmFtZSIsImxlbmd0aCIsImNvbXBvbmVudHMiLCJyZXF1aXJlIiwibW91bnRlZCIsIm9uIiwiZSIsInRvYXN0ciIsInN1Y2Nlc3MiLCJ0aW1lT3V0Iiwib25IaWRkZW4iLCJsb2NhdGlvbiIsInJlbG9hZCIsImRvY3VtZW50IiwicmVhZHkiLCJqUXVlcnkiLCJleHRlbmQiLCJmbiIsImRhdGFUYWJsZUV4dCIsIm9Tb3J0Iiwic3RyMSIsInN0cjIiLCJrZXlwcmVzcyIsImV2ZW50IiwiaW5wdXRWYWx1ZSIsImNoYXJDb2RlIiwicHJldmVudERlZmF1bHQiLCJ3aGljaCIsImZpcnN0TmFtZSIsImxhc3ROYW1lIiwiZ2VuZGVyIiwiYmlydGhkYXRlIiwiY29udGFjdE51bWJlciIsInBhc3Nwb3J0IiwiZ3JvdXBfaWQiLCJhamF4IiwidXJsIiwiYXN5bmMiLCJKU09OIiwicGFyc2UiLCJ1c2VycyIsImJpbmRlZCIsImVycm9yIiwiZHVwbGljYXRlRW50cnkiLCJtb2RhbCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBLElBQUlBLE9BQU9DLE9BQU9DLE9BQVAsQ0FBZUMsSUFBMUI7O0FBRUEsSUFBSUMsVUFBVSxJQUFJQyxHQUFKLENBQVE7QUFDckJDLEtBQUcsU0FEa0I7QUFFckJOLE9BQUs7QUFDSk8sVUFBTyxFQURIOztBQUdKQyxXQUFTO0FBSEwsRUFGZ0I7QUFPckJDLFVBQVM7QUFDUkMsV0FEUSx1QkFDRztBQUNWLE9BQUlDLFlBQVlWLE9BQU9DLE9BQVAsQ0FBZVUsVUFBL0I7QUFDQSxPQUFHQyxFQUFFLFFBQUYsRUFBWUMsR0FBWixNQUFtQixFQUF0QixFQUF5QjtBQUNyQkQsTUFBRSxjQUFGLEVBQWtCRSxJQUFsQixDQUF1QixRQUF2QixFQUFnQyxZQUFVSixTQUFWLEdBQW9CLG9CQUFwRDtBQUNILElBRkQsTUFFTztBQUNIRSxNQUFFLGNBQUYsRUFBa0JFLElBQWxCLENBQXVCLFFBQXZCLEVBQWdDLFlBQVVKLFNBQVYsR0FBb0IscUJBQXBEO0FBQ0g7QUFDRDtBQVJPLEVBUFk7QUFpQnJCSyxRQWpCcUIscUJBaUJaO0FBQUE7O0FBQ1JDLFFBQU1DLEdBQU4sQ0FBVSx3QkFBVixFQUNPQyxJQURQLENBQ1ksa0JBQVU7QUFDYixTQUFLWixNQUFMLEdBQWNhLE9BQU9wQixJQUFyQjs7QUFFQ2EsS0FBRSxVQUFGLEVBQWNRLE9BQWQsQ0FBc0I7QUFDbEJDLGlCQUFhLGNBREs7QUFFbEJDLGdCQUFZO0FBRk0sSUFBdEI7O0FBS0FWLEtBQUUsb0JBQUYsRUFBd0JXLEdBQXhCLENBQTRCLFFBQTVCLEVBQXFDLE1BQXJDO0FBQ04sR0FWSjtBQVdBO0FBN0JvQixDQUFSLENBQWQ7O0FBZ0NBLElBQUluQixHQUFKLENBQVE7O0FBRVBDLEtBQUksTUFGRzs7QUFJUE4sT0FBTTtBQUNMeUIsc0JBQW9CLEVBRGY7O0FBR0NDLFdBQVMxQixLQUFLMkIsY0FBTCxDQUFvQixDQUFwQixFQUF1QkQsT0FIakM7QUFJQ0UsZUFBYTVCLEtBQUs0QixXQUpuQjtBQUtDQyxrQkFBZ0IsQ0FBQztBQUNiQyxpQkFBYSxDQURBO0FBRWJDLHVCQUFtQixDQUZOO0FBR2JDLFlBQVEsQ0FISztBQUliQyxpQkFBYSxDQUpBO0FBS2JDLGVBQVc7QUFMRSxHQUFEO0FBTGpCLEVBSkM7O0FBa0JQekIsVUFBUztBQUNSMEIsZUFEUSwyQkFDUTtBQUNmdEIsS0FBRSxRQUFGLEVBQVl1QixTQUFaLEdBQXdCQyxPQUF4Qjs7QUFFQUMsY0FBVyxZQUFXO0FBQ3JCekIsTUFBRSxRQUFGLEVBQVl1QixTQUFaLENBQXNCO0FBQ2xCLGFBQVEsMkJBRFU7QUFFbEJHLGlCQUFZLElBRk07QUFHbEIsbUJBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLENBQUMsQ0FBZCxDQUFELEVBQW1CLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULEVBQWEsS0FBYixDQUFuQixDQUhJO0FBSWxCLHVCQUFrQixFQUpBO0FBS2xCQyxpQkFBWSxDQUNaLEVBQUNDLE1BQU0sa0JBQVAsRUFBMkJDLFNBQVMsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFwQyxFQURZO0FBTE0sS0FBdEI7QUFTQSxJQVZELEVBVUcsSUFWSDtBQVdBLEdBZk87QUFnQlJDLGdCQWhCUSwwQkFnQk9DLENBaEJQLEVBZ0JVO0FBQ2pCL0IsS0FBRSxRQUFGLEVBQVl1QixTQUFaLEdBQXdCQyxPQUF4Qjs7QUFFQUMsY0FBVyxZQUFXO0FBQ3JCekIsTUFBRSxRQUFGLEVBQVl1QixTQUFaLENBQXNCO0FBQ2xCLGFBQVEsMkJBRFU7QUFFbEJHLGlCQUFZLElBRk07QUFHbEIsbUJBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLENBQUMsQ0FBZCxDQUFELEVBQW1CLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULEVBQWEsS0FBYixDQUFuQixDQUhJO0FBSWxCLHVCQUFrQixFQUpBO0FBS2xCQyxpQkFBWSxDQUNaLEVBQUNDLE1BQU0sa0JBQVAsRUFBMkJDLFNBQVMsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFwQyxFQURZLEVBRVosRUFBQyxXQUFXLENBQUVFLENBQUYsQ0FBWixFQUFrQixXQUFXLEtBQTdCLEVBQW1DLGNBQWMsS0FBakQsRUFGWTtBQUxNLEtBQXRCO0FBVUEsSUFYRCxFQVdHLElBWEg7QUFZQSxHQS9CTztBQWdDUkMsZ0JBaENRLDBCQWdDT0QsQ0FoQ1AsRUFnQ1NFLENBaENULEVBZ0NZO0FBQ25CakMsS0FBRSxRQUFGLEVBQVl1QixTQUFaLEdBQXdCQyxPQUF4Qjs7QUFFQUMsY0FBVyxZQUFXO0FBQ3JCekIsTUFBRSxRQUFGLEVBQVl1QixTQUFaLENBQXNCO0FBQ2xCLGFBQVEsMkJBRFU7QUFFbEJHLGlCQUFZLElBRk07QUFHbEIsbUJBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLENBQUMsQ0FBZCxDQUFELEVBQW1CLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULEVBQWEsS0FBYixDQUFuQixDQUhJO0FBSWxCLHVCQUFrQixFQUpBO0FBS2xCQyxpQkFBWSxDQUNaLEVBQUNDLE1BQU0sa0JBQVAsRUFBMkJDLFNBQVMsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFwQyxFQURZLEVBRVosRUFBQyxXQUFXLENBQUVFLENBQUYsQ0FBWixFQUFrQixXQUFXLEtBQTdCLEVBQW1DLGNBQWMsS0FBakQsRUFGWSxFQUdaLEVBQUMsV0FBVyxDQUFFRSxDQUFGLENBQVosRUFBa0IsV0FBVyxLQUE3QixFQUFtQyxjQUFjLEtBQWpELEVBSFk7QUFMTSxLQUF0QjtBQVdBLElBWkQsRUFZRyxJQVpIO0FBYUEsR0FoRE87QUFpRFJDLGdCQWpEUSwwQkFpRE9ILENBakRQLEVBaURTRSxDQWpEVCxFQWlEV0UsQ0FqRFgsRUFpRGM7QUFDckJuQyxLQUFFLFFBQUYsRUFBWXVCLFNBQVosR0FBd0JDLE9BQXhCOztBQUVBQyxjQUFXLFlBQVc7QUFDckJ6QixNQUFFLFFBQUYsRUFBWXVCLFNBQVosQ0FBc0I7QUFDbEIsYUFBUSwyQkFEVTtBQUVsQkcsaUJBQVksSUFGTTtBQUdsQixtQkFBYyxDQUFDLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULEVBQWEsQ0FBQyxDQUFkLENBQUQsRUFBbUIsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsRUFBYSxLQUFiLENBQW5CLENBSEk7QUFJbEIsdUJBQWtCLEVBSkE7QUFLbEJDLGlCQUFZLENBQ1osRUFBQ0MsTUFBTSxrQkFBUCxFQUEyQkMsU0FBUyxDQUFDLENBQUQsRUFBRyxDQUFILENBQXBDLEVBRFksRUFFWixFQUFDLFdBQVcsQ0FBRUUsQ0FBRixDQUFaLEVBQWtCLFdBQVcsS0FBN0IsRUFBbUMsY0FBYyxLQUFqRCxFQUZZLEVBR1osRUFBQyxXQUFXLENBQUVFLENBQUYsQ0FBWixFQUFrQixXQUFXLEtBQTdCLEVBQW1DLGNBQWMsS0FBakQsRUFIWSxFQUlaLEVBQUMsV0FBVyxDQUFFRSxDQUFGLENBQVosRUFBa0IsV0FBVyxLQUE3QixFQUFtQyxjQUFjLEtBQWpELEVBSlk7QUFMTSxLQUF0QjtBQVlBLElBYkQsRUFhRyxJQWJIO0FBY0E7QUFsRU8sRUFsQkY7O0FBdUZQaEMsUUF2Rk8scUJBdUZFO0FBQ0YsTUFBSWlDLElBQUksS0FBS3JCLFdBQUwsQ0FBaUJzQixNQUFqQixDQUF3QixlQUFPO0FBQUUsVUFBT0MsSUFBSVYsSUFBSixLQUFhLGdCQUFwQjtBQUFxQyxHQUF0RSxDQUFSOztBQUVBLE1BQUlXLElBQUlILEVBQUVDLE1BQUYsQ0FBUyxlQUFPO0FBQUUsVUFBT0MsSUFBSUUsSUFBSixLQUFhLGdCQUFwQjtBQUFxQyxHQUF2RCxDQUFSOztBQUVBLE1BQUdELEVBQUVFLE1BQUYsSUFBVSxDQUFiLEVBQWdCLEtBQUt6QixjQUFMLENBQW9CQyxZQUFwQixHQUFtQyxDQUFuQyxDQUFoQixLQUNLLEtBQUtELGNBQUwsQ0FBb0JDLFlBQXBCLEdBQW1DLENBQW5DOztBQUVMLE1BQUlzQixJQUFJSCxFQUFFQyxNQUFGLENBQVMsZUFBTztBQUFFLFVBQU9DLElBQUlFLElBQUosS0FBYSxzQkFBcEI7QUFBMkMsR0FBN0QsQ0FBUjs7QUFFQSxNQUFHRCxFQUFFRSxNQUFGLElBQVUsQ0FBYixFQUFnQixLQUFLekIsY0FBTCxDQUFvQkUsa0JBQXBCLEdBQXlDLENBQXpDLENBQWhCLEtBQ0ssS0FBS0YsY0FBTCxDQUFvQkUsa0JBQXBCLEdBQXlDLENBQXpDOztBQUVMLE1BQUlxQixJQUFJSCxFQUFFQyxNQUFGLENBQVMsZUFBTztBQUFFLFVBQU9DLElBQUlFLElBQUosS0FBYSxTQUFwQjtBQUE4QixHQUFoRCxDQUFSOztBQUVBLE1BQUdELEVBQUVFLE1BQUYsSUFBVSxDQUFiLEVBQWdCLEtBQUt6QixjQUFMLENBQW9CRyxPQUFwQixHQUE4QixDQUE5QixDQUFoQixLQUNLLEtBQUtILGNBQUwsQ0FBb0JHLE9BQXBCLEdBQThCLENBQTlCOztBQUVMLE1BQUlvQixJQUFJSCxFQUFFQyxNQUFGLENBQVMsZUFBTztBQUFFLFVBQU9DLElBQUlFLElBQUosS0FBYSxjQUFwQjtBQUFtQyxHQUFyRCxDQUFSOztBQUVBLE1BQUdELEVBQUVFLE1BQUYsSUFBVSxDQUFiLEVBQWdCLEtBQUt6QixjQUFMLENBQW9CSSxZQUFwQixHQUFtQyxDQUFuQyxDQUFoQixLQUNLLEtBQUtKLGNBQUwsQ0FBb0JJLFlBQXBCLEdBQW1DLENBQW5DOztBQUVMLE1BQUltQixJQUFJSCxFQUFFQyxNQUFGLENBQVMsZUFBTztBQUFFLFVBQU9DLElBQUlFLElBQUosS0FBYSxhQUFwQjtBQUFrQyxHQUFwRCxDQUFSOztBQUVBLE1BQUdELEVBQUVFLE1BQUYsSUFBVSxDQUFiLEVBQWdCLEtBQUt6QixjQUFMLENBQW9CSyxVQUFwQixHQUFpQyxDQUFqQyxDQUFoQixLQUNLLEtBQUtMLGNBQUwsQ0FBb0JLLFVBQXBCLEdBQWlDLENBQWpDOztBQUVMLE1BQUcsS0FBS0wsY0FBTCxDQUFvQkcsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0gsY0FBTCxDQUFvQkksWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0osY0FBTCxDQUFvQkssVUFBcEIsSUFBa0MsQ0FBbEgsRUFBb0g7QUFDekgsUUFBS0MsYUFBTDtBQUNNOztBQUVELE1BQUcsS0FBS04sY0FBTCxDQUFvQkcsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0gsY0FBTCxDQUFvQkksWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0osY0FBTCxDQUFvQkssVUFBcEIsSUFBa0MsQ0FBbEgsRUFBb0g7QUFDekgsUUFBS1MsY0FBTCxDQUFvQixDQUFwQjtBQUNNOztBQUVELE1BQUcsS0FBS2QsY0FBTCxDQUFvQkcsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0gsY0FBTCxDQUFvQkksWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0osY0FBTCxDQUFvQkssVUFBcEIsSUFBa0MsQ0FBbEgsRUFBb0g7QUFDekgsUUFBS1MsY0FBTCxDQUFvQixDQUFwQjtBQUNNOztBQUVELE1BQUcsS0FBS2QsY0FBTCxDQUFvQkcsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0gsY0FBTCxDQUFvQkksWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0osY0FBTCxDQUFvQkssVUFBcEIsSUFBa0MsQ0FBbEgsRUFBb0g7QUFDekgsUUFBS1MsY0FBTCxDQUFvQixDQUFwQjtBQUNNOztBQUVELE1BQUcsS0FBS2QsY0FBTCxDQUFvQkcsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0gsY0FBTCxDQUFvQkksWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0osY0FBTCxDQUFvQkssVUFBcEIsSUFBa0MsQ0FBbEgsRUFBb0g7QUFDekgsUUFBS1csY0FBTCxDQUFvQixDQUFwQixFQUFzQixDQUF0QjtBQUNNOztBQUVELE1BQUcsS0FBS2hCLGNBQUwsQ0FBb0JHLE9BQXBCLElBQStCLENBQS9CLElBQW9DLEtBQUtILGNBQUwsQ0FBb0JJLFlBQXBCLElBQW9DLENBQXhFLElBQTZFLEtBQUtKLGNBQUwsQ0FBb0JLLFVBQXBCLElBQWtDLENBQWxILEVBQW9IO0FBQ3pILFFBQUtXLGNBQUwsQ0FBb0IsQ0FBcEIsRUFBc0IsQ0FBdEI7QUFDTTs7QUFFRCxNQUFHLEtBQUtoQixjQUFMLENBQW9CRyxPQUFwQixJQUErQixDQUEvQixJQUFvQyxLQUFLSCxjQUFMLENBQW9CSSxZQUFwQixJQUFvQyxDQUF4RSxJQUE2RSxLQUFLSixjQUFMLENBQW9CSyxVQUFwQixJQUFrQyxDQUFsSCxFQUFvSDtBQUN6SCxRQUFLVyxjQUFMLENBQW9CLENBQXBCLEVBQXNCLENBQXRCO0FBQ007O0FBRUQsTUFBRyxLQUFLaEIsY0FBTCxDQUFvQkcsT0FBcEIsSUFBK0IsQ0FBL0IsSUFBb0MsS0FBS0gsY0FBTCxDQUFvQkksWUFBcEIsSUFBb0MsQ0FBeEUsSUFBNkUsS0FBS0osY0FBTCxDQUFvQkssVUFBcEIsSUFBa0MsQ0FBbEgsRUFBb0g7QUFDekgsUUFBS2EsY0FBTCxDQUFvQixDQUFwQixFQUFzQixDQUF0QixFQUF3QixDQUF4QjtBQUNNO0FBRVAsRUFuSk07OztBQXFKUFEsYUFBWTtBQUNYLDBCQUF3QkMsbUJBQU9BLENBQUMsR0FBUjtBQURiLEVBckpMOztBQXlKUEMsUUF6Sk8scUJBeUpHO0FBQ1Q1QyxJQUFFLGlDQUFGLEVBQXFDNkMsRUFBckMsQ0FBd0MsaUJBQXhDLEVBQTJELFVBQVVDLENBQVYsRUFBYTtBQUN2RUMsVUFBT0MsT0FBUCxDQUFlLHNDQUFmLEVBQXVELElBQXZELEVBQTZEO0FBQ3REQyxhQUFTLElBRDZDO0FBRXREQyxjQUFVLG9CQUFZO0FBQzNCOUQsWUFBTytELFFBQVAsQ0FBZ0JDLE1BQWhCO0FBQ0E7QUFKMkQsSUFBN0Q7QUFNQSxHQVBEO0FBUUE7QUFsS00sQ0FBUjs7QUFzS0FwRCxFQUFFcUQsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVc7O0FBRTdCQyxRQUFPQyxNQUFQLENBQWVELE9BQU9FLEVBQVAsQ0FBVUMsWUFBVixDQUF1QkMsS0FBdEMsRUFBNkM7QUFDekMsMEJBQXdCLDJCQUFVQyxJQUFWLEVBQWdCQyxJQUFoQixFQUFzQjtBQUMxQyxPQUFHRCxRQUFRLHVDQUFYLEVBQ0ksT0FBTyxDQUFQO0FBQ0osT0FBR0MsUUFBUSx1Q0FBWCxFQUNJLE9BQU8sQ0FBQyxDQUFSO0FBQ0osVUFBU0QsT0FBT0MsSUFBUixHQUFnQixDQUFDLENBQWpCLEdBQXVCRCxPQUFPQyxJQUFSLEdBQWdCLENBQWhCLEdBQW9CLENBQWxEO0FBQ0gsR0FQd0M7O0FBU3pDLDJCQUF5Qiw0QkFBVUQsSUFBVixFQUFnQkMsSUFBaEIsRUFBc0I7QUFDM0MsT0FBR0QsUUFBUSx1Q0FBWCxFQUNJLE9BQU8sQ0FBUDtBQUNKLE9BQUdDLFFBQVEsdUNBQVgsRUFDSSxPQUFPLENBQUMsQ0FBUjtBQUNKLFVBQVNELE9BQU9DLElBQVIsR0FBZ0IsQ0FBaEIsR0FBc0JELE9BQU9DLElBQVIsR0FBZ0IsQ0FBQyxDQUFqQixHQUFxQixDQUFsRDtBQUNIO0FBZndDLEVBQTdDOztBQWtCQzdELEdBQUUsdUNBQUYsRUFBMkM4RCxRQUEzQyxDQUFvRCxVQUFTQyxLQUFULEVBQWU7QUFDbEUsTUFBSUMsYUFBYUQsTUFBTUUsUUFBdkI7QUFDQTtBQUNBLE1BQUcsRUFBR0QsYUFBYSxFQUFiLElBQW1CQSxhQUFhLEVBQWpDLElBQXlDQSxhQUFhLEVBQWIsSUFBbUJBLGFBQWEsR0FBekUsSUFBZ0ZBLGNBQVksRUFBNUYsSUFBb0dBLGNBQVksQ0FBbEgsQ0FBSCxFQUF5SDtBQUN6SEQsU0FBTUcsY0FBTjtBQUNDO0FBQ0QsRUFORDs7QUFRQWxFLEdBQUUsdURBQUYsRUFBMkQ4RCxRQUEzRCxDQUFvRSxVQUFVaEIsQ0FBVixFQUFhO0FBQ2hGO0FBQ0csTUFBSUEsRUFBRXFCLEtBQUYsSUFBVyxDQUFYLElBQWdCckIsRUFBRXFCLEtBQUYsSUFBVyxDQUEzQixLQUFpQ3JCLEVBQUVxQixLQUFGLEdBQVUsRUFBVixJQUFnQnJCLEVBQUVxQixLQUFGLEdBQVUsRUFBM0QsQ0FBSixFQUFvRTtBQUNoRSxVQUFPLEtBQVA7QUFDSDtBQUNKLEVBTEQ7O0FBT0E7QUFDQTtBQUNBOztBQUVBbkUsR0FBRSxrQkFBRixFQUFzQjZDLEVBQXRCLENBQXlCLFFBQXpCLEVBQW1DLFVBQVNDLENBQVQsRUFBWTtBQUM5QyxNQUFJc0IsWUFBWXBFLEVBQUUsa0JBQUYsRUFBc0JDLEdBQXRCLEVBQWhCO0FBQ0EsTUFBSW9FLFdBQVdyRSxFQUFFLGlCQUFGLEVBQXFCQyxHQUFyQixFQUFmO0FBQ0EsTUFBSXFFLFNBQVN0RSxFQUFFLHFCQUFGLEVBQXlCQyxHQUF6QixFQUFiO0FBQ0EsTUFBSXNFLFlBQVl2RSxFQUFFLGtCQUFGLEVBQXNCQyxHQUF0QixFQUFoQjtBQUNBLE1BQUl1RSxnQkFBZ0J4RSxFQUFFLHNCQUFGLEVBQTBCQyxHQUExQixFQUFwQjtBQUNBLE1BQUl3RSxXQUFXekUsRUFBRSxnQkFBRixFQUFvQkMsR0FBcEIsRUFBZjtBQUNBLE1BQUl5RSxXQUFXMUUsRUFBRSxXQUFGLEVBQWVDLEdBQWYsRUFBZjs7QUFFQUQsSUFBRTJFLElBQUYsQ0FBTztBQUNOQyxRQUFLLHdCQURDO0FBRUdoRCxTQUFNLEtBRlQ7QUFHR3pDLFNBQU07QUFDTGlGLGVBQVVBLFNBREw7QUFFZEMsY0FBVUEsUUFGSTtBQUdkQyxZQUFRQSxNQUhNO0FBSWRDLGVBQVdBLFNBSkc7QUFLZEMsbUJBQWVBLGFBTEQ7QUFNZEMsY0FBVUEsUUFOSTtBQU9kQyxjQUFVQTtBQVBJLElBSFQ7QUFZR0csVUFBTyxLQVpWO0FBYUc3QixZQUFTLGlCQUFTN0QsSUFBVCxFQUFlO0FBQ3ZCLFFBQUlBLE9BQU8yRixLQUFLQyxLQUFMLENBQVc1RixJQUFYLENBQVg7O0FBRUFJLFlBQVFJLE9BQVIsR0FBa0JSLEtBQUs2RixLQUF2Qjs7QUFFRyxRQUFHN0YsS0FBSzhGLE1BQVIsRUFBZ0I7QUFDZmxDLFlBQU9tQyxLQUFQLENBQWEsbURBQWI7QUFDQXBDLE9BQUVvQixjQUFGO0FBQ0EsS0FIRCxNQUdPLElBQUcvRSxLQUFLZ0csY0FBUixFQUF3QjtBQUMzQm5GLE9BQUUsNEJBQUYsRUFBZ0NvRixLQUFoQyxDQUFzQyxNQUF0QztBQUNBdEMsT0FBRW9CLGNBQUY7QUFDSDtBQUNKO0FBekJKLEdBQVA7QUEyQkEsRUFwQ0Q7QUFzQ0EsQ0E3RUQsRTs7Ozs7OztBQ3hNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsaUJBQWlCO0FBQ2pDO0FBQ0E7QUFDQSx3Q0FBd0MsZ0JBQWdCO0FBQ3hELElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsaUJBQWlCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvQkFBb0I7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcUdBO0FBQ0EsS0FEQSxrQkFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBLE1BRUEsd0NBRkEsQ0FEQTs7QUFLQSxZQUxBOztBQU9BLG1CQVBBO0FBUUEsb0JBUkE7QUFTQSx5QkFUQTtBQVVBO0FBVkE7QUFZQSxFQWRBOzs7QUFnQkE7QUFDQSwwQkFEQSxzQ0FDQTtBQUNBO0FBQ0E7QUFEQSxNQUVBLHdDQUZBO0FBR0EsR0FMQTtBQU9BLFlBUEEsd0JBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBWkE7QUFjQSx3QkFkQSxvQ0FjQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQWZBOztBQWlCQTtBQUNBLEdBbkNBO0FBcUNBLDhCQXJDQSwwQ0FxQ0E7QUFBQTs7QUFDQTs7QUFFQTtBQUNBLHFGQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUZBLE1BRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQXpCQTtBQTBCQTtBQUNBLEdBcEVBO0FBc0VBLGdCQXRFQSw0QkFzRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFuQ0E7O0FBcUNBO0FBQ0EsR0EvR0E7QUFpSEEsb0JBakhBLGdDQWlIQTtBQUFBOztBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMEVBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsT0FGQSxFQUVBLElBRkE7QUFHQTs7QUFFQTtBQUNBLEtBbkJBO0FBb0JBO0FBQ0EsR0E1SUE7QUE4SUEsa0JBOUlBLDhCQThJQTtBQUNBO0FBQ0EsR0FoSkE7QUFrSkEscUJBbEpBLCtCQWtKQSxLQWxKQSxFQWtKQTtBQUNBO0FBQ0E7QUFwSkEsRUFoQkE7O0FBdUtBLFFBdktBLHFCQXVLQTtBQUFBOztBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxHQU5BO0FBT0E7QUEvS0EsRzs7Ozs7OztBQ3RKQSwyQkFBMkIsbUJBQU8sQ0FBQyxDQUEyRDtBQUM5RixjQUFjLFFBQVMsNEJBQTRCLHdCQUF3QixHQUFHLDRCQUE0Qix1QkFBdUIsR0FBRyxVQUFVLGtGQUFrRixNQUFNLFdBQVcsS0FBSyxLQUFLLFdBQVcsa2FBQWthLFlBQVksc2JBQXNiLFdBQVcsbTZCQUFtNkIsaUNBQWlDLHdCQUF3Qiw4aEJBQThoQixXQUFXLHlGQUF5Rix5RUFBeUUsNkJBQTZCLHVEQUF1RCw2R0FBNkcsMkVBQTJFLG1EQUFtRCw2QkFBNkIsb0RBQW9ELDZHQUE2RyxxRkFBcUYsNEJBQTRCLDZCQUE2QixxRUFBcUUsMmxJQUEybEksZ0JBQWdCLGtCQUFrQiwyQ0FBMkMsK0JBQStCLG1MQUFtTCxZQUFZLEdBQUcsdUNBQXVDLHFKQUFxSixTQUFTLHFCQUFxQixzQ0FBc0MsaURBQWlELCtCQUErQixtTEFBbUwsWUFBWSxHQUFHLHVDQUF1QyxFQUFFLFdBQVcsMkJBQTJCLG1DQUFtQyxvQ0FBb0MseUNBQXlDLGdDQUFnQyxXQUFXLHVDQUF1QywrQkFBK0IsMEZBQTBGLG9EQUFvRCxnQ0FBZ0MsMEZBQTBGLGVBQWUsK0NBQStDLGdDQUFnQyw2RUFBNkUsZUFBZSxzREFBc0QsZ0NBQWdDLDBGQUEwRixlQUFlLGFBQWEsRUFBRSw2QkFBNkIsV0FBVyw2Q0FBNkMsc0RBQXNELHlCQUF5QixpSUFBaUksMENBQTBDLHNDQUFzQyxrRUFBa0Usa0VBQWtFLDRFQUE0RSwwREFBMEQsb0ZBQW9GLGtDQUFrQyxTQUFTLE9BQU8saUZBQWlGLHNHQUFzRyx5QkFBeUIsT0FBTyxnR0FBZ0cseUJBQXlCLDJGQUEyRixxR0FBcUcscUdBQXFHLCtHQUErRyx1QkFBdUIscUJBQXFCLG1CQUFtQixFQUFFLGFBQWEsV0FBVywrQkFBK0IsK0JBQStCLDBGQUEwRiw4Q0FBOEMsZ0NBQWdDLGdIQUFnSCxlQUFlLHdIQUF3SCxnQ0FBZ0MscUhBQXFILGVBQWUseUZBQXlGLGdDQUFnQyxvSEFBb0gsZUFBZSx3RkFBd0YsZ0NBQWdDLG1IQUFtSCxlQUFlLHVGQUF1RixnQ0FBZ0Msa0hBQWtILGVBQWUsd0ZBQXdGLGdDQUFnQyxtSEFBbUgsZUFBZSxxRkFBcUYsZ0NBQWdDLGdIQUFnSCxlQUFlLGFBQWEsRUFBRSw2QkFBNkIsV0FBVyxtQ0FBbUMsOENBQThDLHlCQUF5QixvRkFBb0YsMEhBQTBILDRDQUE0Qyx3REFBd0Qsc0NBQXNDLG9DQUFvQyx3RUFBd0UsNkRBQTZELDhDQUE4Qyw4Q0FBOEMsMkJBQTJCLFFBQVEsdUJBQXVCLHdGQUF3RixtQkFBbUIsRUFBRSxhQUFhLFNBQVMsK0JBQStCLHlEQUF5RCxtTEFBbUwsRUFBRSxTQUFTLHVDQUF1QyxvRUFBb0UsU0FBUyxTQUFTLHNCQUFzQixzRUFBc0UsMENBQTBDLDhCQUE4Qiw0QkFBNEIsU0FBUyxFQUFFLFNBQVMsT0FBTyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNELzRjO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLGlCQUFpQjtBQUMzQjtBQUNBOztBQUVBLG1CQUFtQixtQkFBTyxDQUFDLENBQWdCOztBQUUzQztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsbUJBQW1CLG1CQUFtQjtBQUN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxtQkFBbUIsc0JBQXNCO0FBQ3pDO0FBQ0E7QUFDQSx1QkFBdUIsMkJBQTJCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCLG1CQUFtQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiwyQkFBMkI7QUFDaEQ7QUFDQTtBQUNBLFlBQVksdUJBQXVCO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxxQkFBcUIsdUJBQXVCO0FBQzVDO0FBQ0E7QUFDQSw4QkFBOEI7QUFDOUI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseURBQXlEO0FBQ3pEOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUNyTkE7QUFDQSxtQkFBTyxDQUFDLEdBQXNSOztBQUU5UixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFnUDtBQUMxUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUE0TTtBQUN0TjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMvQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLHdDQUF3QyxRQUFRO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSx3Q0FBd0MsUUFBUTtBQUNoRDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0Esd0NBQXdDLFFBQVE7QUFDaEQ7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLHdDQUF3QyxRQUFRO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0Esd0NBQXdDLFFBQVE7QUFDaEQ7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMvZEE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBbVQ7QUFDelUsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7Ozs7Ozs7Ozs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsaUJBQWlCO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQyx3QkFBd0I7QUFDM0QsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoianMvdmlzYS9jbGllbnRzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0NzcpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGYxMTNlZDM2YTVhNTZiN2RjZjYzIiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUiLCJsZXQgZGF0YSA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XG5cbnZhciBDbGllbnRzID0gbmV3IFZ1ZSh7XG5cdGVsOicjY2xpZW50Jyxcblx0ZGF0YTp7XG5cdFx0Z3JvdXBzOltdLFxuXG5cdFx0Y2xpZW50czogW11cblx0fSxcblx0bWV0aG9kczoge1xuXHRcdGNoYW5nZVVybCgpe1xuXHRcdFx0dmFyIGNwYW5lbFVybCA9IHdpbmRvdy5MYXJhdmVsLmNwYW5lbF91cmw7XG5cdFx0XHRpZigkKCcjZW1haWwnKS52YWwoKT09Jycpe1xuXHRcdCAgICBcdCQoJyNmb3JtLWNsaWVudCcpLmF0dHIoJ2FjdGlvbicsJ2h0dHA6Ly8nK2NwYW5lbFVybCsnL3Zpc2EvY2xpZW50L3N0b3JlJyk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdCAgICBcdCQoJyNmb3JtLWNsaWVudCcpLmF0dHIoJ2FjdGlvbicsJ2h0dHA6Ly8nK2NwYW5lbFVybCsnL3Zpc2EvY2xpZW50L3N0b3JlMicpO1x0XHRcdFx0XG5cdFx0XHR9XG5cdFx0fVx0XG5cdH0sXG5cdGNyZWF0ZWQoKXtcblx0XHRheGlvcy5nZXQoJy92aXNhL2NsaWVudC9zaG93R3JvdXAnKVxuICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgIFx0dGhpcy5ncm91cHMgPSByZXN1bHQuZGF0YTtcblxuICAgICAgICAgICAgJChcIi5zZWxlY3QyXCIpLnNlbGVjdDIoe1xuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIlNlbGVjdCBncm91cFwiLFxuICAgICAgICAgICAgICAgIGFsbG93Q2xlYXI6IHRydWVcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkKFwiLnNlbGVjdDItc2VsZWN0aW9uXCIpLmNzcygnaGVpZ2h0JywnMzRweCcpO1xuICAgIFx0fSk7XG5cdH1cbn0pO1xuXG5uZXcgVnVlKHtcblx0XG5cdGVsOiAnI2FwcCcsXG5cblx0ZGF0YToge1xuXHRcdHRlbXBvcmFyeUNsaWVudElkczogW10sXG5cbiAgICAgICAgc2V0dGluZzogZGF0YS5hY2Nlc3NfY29udHJvbFswXS5zZXR0aW5nLFxuICAgICAgICBwZXJtaXNzaW9uczogZGF0YS5wZXJtaXNzaW9ucyxcbiAgICAgICAgc2VscGVybWlzc2lvbnM6IFt7XG4gICAgICAgICAgICBhZGROZXdDbGllbnQ6MCxcbiAgICAgICAgICAgIGFkZFRlbXBvcmFyeUNsaWVudDowLFxuICAgICAgICAgICAgYmFsYW5jZTowLFxuICAgICAgICAgICAgY29sbGVjdGFibGVzOjAsXG4gICAgICAgICAgICB2aWV3QWN0aW9uOjBcbiAgICAgICAgfV1cblx0fSxcblxuXHRtZXRob2RzOiB7XG5cdFx0Y2FsbERhdGF0YWJsZSgpIHtcblx0XHRcdCQoJyNsaXN0cycpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0JCgnI2xpc3RzJykuRGF0YVRhYmxlKHtcblx0XHRcdFx0ICAgIFwiYWpheFwiOiBcIi9zdG9yYWdlL2RhdGEvQ2xpZW50cy50eHRcIixcblx0XHRcdFx0ICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdFx0XHRcdCAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwLCAtMV0sIFsxMCwgMjUsIDUwLCBcIkFsbFwiXV0sXG5cdFx0XHRcdCAgICBcImlEaXNwbGF5TGVuZ3RoXCI6IDI1LFxuXHRcdFx0XHQgICAgY29sdW1uRGVmczogW1xuXHRcdFx0XHQgICAge3R5cGU6ICdub24tZW1wdHktc3RyaW5nJywgdGFyZ2V0czogWzQsNV19XG5cdFx0XHRcdCAgICBdXG5cdFx0XHRcdH0pO1xuXHRcdFx0fSwgMTAwMCk7XG5cdFx0fSxcblx0XHRjYWxsRGF0YXRhYmxlMShhKSB7XG5cdFx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdCQoJyNsaXN0cycpLkRhdGFUYWJsZSh7XG5cdFx0XHRcdCAgICBcImFqYXhcIjogXCIvc3RvcmFnZS9kYXRhL0NsaWVudHMudHh0XCIsXG5cdFx0XHRcdCAgICByZXNwb25zaXZlOiB0cnVlLFxuXHRcdFx0XHQgICAgXCJsZW5ndGhNZW51XCI6IFtbMTAsIDI1LCA1MCwgLTFdLCBbMTAsIDI1LCA1MCwgXCJBbGxcIl1dLFxuXHRcdFx0XHQgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAyNSxcblx0XHRcdFx0ICAgIGNvbHVtbkRlZnM6IFtcblx0XHRcdFx0ICAgIHt0eXBlOiAnbm9uLWVtcHR5LXN0cmluZycsIHRhcmdldHM6IFs0LDVdfSxcblx0XHRcdFx0ICAgIHtcInRhcmdldHNcIjogWyBhIF0sXCJ2aXNpYmxlXCI6IGZhbHNlLFwic2VhcmNoYWJsZVwiOiBmYWxzZX1cblx0XHRcdFx0ICAgIF1cblx0XHRcdFx0fSk7XG5cdFx0XHR9LCAxMDAwKTtcblx0XHR9LFxuXHRcdGNhbGxEYXRhdGFibGUyKGEsYikge1xuXHRcdFx0JCgnI2xpc3RzJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuXHRcdFx0XHQgICAgXCJhamF4XCI6IFwiL3N0b3JhZ2UvZGF0YS9DbGllbnRzLnR4dFwiLFxuXHRcdFx0XHQgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHRcdFx0ICAgIFwibGVuZ3RoTWVudVwiOiBbWzEwLCAyNSwgNTAsIC0xXSwgWzEwLCAyNSwgNTAsIFwiQWxsXCJdXSxcblx0XHRcdFx0ICAgIFwiaURpc3BsYXlMZW5ndGhcIjogMjUsXG5cdFx0XHRcdCAgICBjb2x1bW5EZWZzOiBbXG5cdFx0XHRcdCAgICB7dHlwZTogJ25vbi1lbXB0eS1zdHJpbmcnLCB0YXJnZXRzOiBbNCw1XX0sXG5cdFx0XHRcdCAgICB7XCJ0YXJnZXRzXCI6IFsgYSBdLFwidmlzaWJsZVwiOiBmYWxzZSxcInNlYXJjaGFibGVcIjogZmFsc2V9LFxuXHRcdFx0XHQgICAge1widGFyZ2V0c1wiOiBbIGIgXSxcInZpc2libGVcIjogZmFsc2UsXCJzZWFyY2hhYmxlXCI6IGZhbHNlfVxuXHRcdFx0XHQgICAgXVxuXHRcdFx0XHR9KTtcblx0XHRcdH0sIDEwMDApO1xuXHRcdH0sXG5cdFx0Y2FsbERhdGF0YWJsZTMoYSxiLGMpIHtcblx0XHRcdCQoJyNsaXN0cycpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0JCgnI2xpc3RzJykuRGF0YVRhYmxlKHtcblx0XHRcdFx0ICAgIFwiYWpheFwiOiBcIi9zdG9yYWdlL2RhdGEvQ2xpZW50cy50eHRcIixcblx0XHRcdFx0ICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdFx0XHRcdCAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwLCAtMV0sIFsxMCwgMjUsIDUwLCBcIkFsbFwiXV0sXG5cdFx0XHRcdCAgICBcImlEaXNwbGF5TGVuZ3RoXCI6IDI1LFxuXHRcdFx0XHQgICAgY29sdW1uRGVmczogW1xuXHRcdFx0XHQgICAge3R5cGU6ICdub24tZW1wdHktc3RyaW5nJywgdGFyZ2V0czogWzQsNV19LFxuXHRcdFx0XHQgICAge1widGFyZ2V0c1wiOiBbIGEgXSxcInZpc2libGVcIjogZmFsc2UsXCJzZWFyY2hhYmxlXCI6IGZhbHNlfSxcblx0XHRcdFx0ICAgIHtcInRhcmdldHNcIjogWyBiIF0sXCJ2aXNpYmxlXCI6IGZhbHNlLFwic2VhcmNoYWJsZVwiOiBmYWxzZX0sXG5cdFx0XHRcdCAgICB7XCJ0YXJnZXRzXCI6IFsgYyBdLFwidmlzaWJsZVwiOiBmYWxzZSxcInNlYXJjaGFibGVcIjogZmFsc2V9XG5cdFx0XHRcdCAgICBdXG5cdFx0XHRcdH0pO1xuXHRcdFx0fSwgMTAwMCk7XG5cdFx0fVxuXHR9LFxuXG5cdGNyZWF0ZWQoKXtcbiAgICAgICAgdmFyIHggPSB0aGlzLnBlcm1pc3Npb25zLmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLnR5cGUgPT09ICdNYW5hZ2UgQ2xpZW50cyd9KTtcblxuICAgICAgICB2YXIgeSA9IHguZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoubmFtZSA9PT0gJ0FkZCBOZXcgQ2xpZW50J30pO1xuXG4gICAgICAgIGlmKHkubGVuZ3RoPT0wKSB0aGlzLnNlbHBlcm1pc3Npb25zLmFkZE5ld0NsaWVudCA9IDA7XG4gICAgICAgIGVsc2UgdGhpcy5zZWxwZXJtaXNzaW9ucy5hZGROZXdDbGllbnQgPSAxO1xuXG4gICAgICAgIHZhciB5ID0geC5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai5uYW1lID09PSAnQWRkIFRlbXBvcmFyeSBDbGllbnQnfSk7XG5cbiAgICAgICAgaWYoeS5sZW5ndGg9PTApIHRoaXMuc2VscGVybWlzc2lvbnMuYWRkVGVtcG9yYXJ5Q2xpZW50ID0gMDtcbiAgICAgICAgZWxzZSB0aGlzLnNlbHBlcm1pc3Npb25zLmFkZFRlbXBvcmFyeUNsaWVudCA9IDE7XG5cbiAgICAgICAgdmFyIHkgPSB4LmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLm5hbWUgPT09ICdCYWxhbmNlJ30pO1xuXG4gICAgICAgIGlmKHkubGVuZ3RoPT0wKSB0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPSAwO1xuICAgICAgICBlbHNlIHRoaXMuc2VscGVybWlzc2lvbnMuYmFsYW5jZSA9IDE7XG5cbiAgICAgICAgdmFyIHkgPSB4LmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLm5hbWUgPT09ICdDb2xsZWN0YWJsZXMnfSk7XG5cbiAgICAgICAgaWYoeS5sZW5ndGg9PTApIHRoaXMuc2VscGVybWlzc2lvbnMuY29sbGVjdGFibGVzID0gMDtcbiAgICAgICAgZWxzZSB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbGxlY3RhYmxlcyA9IDE7XG5cbiAgICAgICAgdmFyIHkgPSB4LmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLm5hbWUgPT09ICdWaWV3IEFjdGlvbid9KTtcblxuICAgICAgICBpZih5Lmxlbmd0aD09MCkgdGhpcy5zZWxwZXJtaXNzaW9ucy52aWV3QWN0aW9uID0gMDtcbiAgICAgICAgZWxzZSB0aGlzLnNlbHBlcm1pc3Npb25zLnZpZXdBY3Rpb24gPSAxO1xuXG4gICAgICAgIGlmKHRoaXMuc2VscGVybWlzc2lvbnMuYmFsYW5jZSA9PSAxICYmIHRoaXMuc2VscGVybWlzc2lvbnMuY29sbGVjdGFibGVzID09IDEgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy52aWV3QWN0aW9uID09IDEpe1xuXHRcdFx0dGhpcy5jYWxsRGF0YXRhYmxlKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbGxlY3RhYmxlcyA9PSAxICYmIHRoaXMuc2VscGVybWlzc2lvbnMudmlld0FjdGlvbiA9PSAxKXtcblx0XHRcdHRoaXMuY2FsbERhdGF0YWJsZTEoMik7XG4gICAgICAgIH1cblxuICAgICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbGxlY3RhYmxlcyA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMudmlld0FjdGlvbiA9PSAxKXtcblx0XHRcdHRoaXMuY2FsbERhdGF0YWJsZTEoMyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPT0gMSAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbGxlY3RhYmxlcyA9PSAxICYmIHRoaXMuc2VscGVybWlzc2lvbnMudmlld0FjdGlvbiA9PSAwKXtcblx0XHRcdHRoaXMuY2FsbERhdGF0YWJsZTEoNik7XG4gICAgICAgIH1cblxuICAgICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbGxlY3RhYmxlcyA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMudmlld0FjdGlvbiA9PSAxKXtcblx0XHRcdHRoaXMuY2FsbERhdGF0YWJsZTIoMiwzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKHRoaXMuc2VscGVybWlzc2lvbnMuYmFsYW5jZSA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMuY29sbGVjdGFibGVzID09IDEgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy52aWV3QWN0aW9uID09IDApe1xuXHRcdFx0dGhpcy5jYWxsRGF0YXRhYmxlMigyLDYpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYodGhpcy5zZWxwZXJtaXNzaW9ucy5iYWxhbmNlID09IDEgJiYgdGhpcy5zZWxwZXJtaXNzaW9ucy5jb2xsZWN0YWJsZXMgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLnZpZXdBY3Rpb24gPT0gMCl7XG5cdFx0XHR0aGlzLmNhbGxEYXRhdGFibGUyKDMsNik7XG4gICAgICAgIH1cblxuICAgICAgICBpZih0aGlzLnNlbHBlcm1pc3Npb25zLmJhbGFuY2UgPT0gMCAmJiB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbGxlY3RhYmxlcyA9PSAwICYmIHRoaXMuc2VscGVybWlzc2lvbnMudmlld0FjdGlvbiA9PSAwKXtcblx0XHRcdHRoaXMuY2FsbERhdGF0YWJsZTMoMiwzLDYpO1xuICAgICAgICB9XG5cblx0fSxcblxuXHRjb21wb25lbnRzOiB7XG5cdFx0J2FkZC10ZW1wb3JhcnktY2xpZW50JzogcmVxdWlyZSgnLi4vLi4vY29tcG9uZW50cy9WaXNhL0FkZFRlbXBvcmFyeUNsaWVudC52dWUnKSxcblx0fSxcblxuXHRtb3VudGVkKCkge1xuXHRcdCQoJyNuZXdseUFkZGVkVGVtcG9yYXJ5Q2xpZW50TW9kYWwnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdHRvYXN0ci5zdWNjZXNzKCdUZW1wb3JhcnkgY2xpZW50IHN1Y2Nlc3NmdWxseSBhZGRlZC4nLCBudWxsLCB7XG5cdFx0ICAgICAgICB0aW1lT3V0OiAxMDAwLFxuXHRcdCAgICAgICAgb25IaWRkZW46IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHR3aW5kb3cubG9jYXRpb24ucmVsb2FkKCk7XG5cdFx0XHRcdH1cblx0XHQgICAgfSk7XG5cdFx0fSk7XG5cdH1cblxufSk7XG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuXG5qUXVlcnkuZXh0ZW5kKCBqUXVlcnkuZm4uZGF0YVRhYmxlRXh0Lm9Tb3J0LCB7XG4gICAgXCJub24tZW1wdHktc3RyaW5nLWFzY1wiOiBmdW5jdGlvbiAoc3RyMSwgc3RyMikge1xuICAgICAgICBpZihzdHIxID09ICc8c3BhbiBzdHlsZT1cImRpc3BsYXk6bm9uZTtcIj4tLTwvc3Bhbj4nKVxuICAgICAgICAgICAgcmV0dXJuIDE7XG4gICAgICAgIGlmKHN0cjIgPT0gJzxzcGFuIHN0eWxlPVwiZGlzcGxheTpub25lO1wiPi0tPC9zcGFuPicpXG4gICAgICAgICAgICByZXR1cm4gLTE7XG4gICAgICAgIHJldHVybiAoKHN0cjEgPCBzdHIyKSA/IC0xIDogKChzdHIxID4gc3RyMikgPyAxIDogMCkpO1xuICAgIH0sXG4gXG4gICAgXCJub24tZW1wdHktc3RyaW5nLWRlc2NcIjogZnVuY3Rpb24gKHN0cjEsIHN0cjIpIHtcbiAgICAgICAgaWYoc3RyMSA9PSAnPHNwYW4gc3R5bGU9XCJkaXNwbGF5Om5vbmU7XCI+LS08L3NwYW4+JylcbiAgICAgICAgICAgIHJldHVybiAxO1xuICAgICAgICBpZihzdHIyID09ICc8c3BhbiBzdHlsZT1cImRpc3BsYXk6bm9uZTtcIj4tLTwvc3Bhbj4nKVxuICAgICAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgICByZXR1cm4gKChzdHIxIDwgc3RyMikgPyAxIDogKChzdHIxID4gc3RyMikgPyAtMSA6IDApKTtcbiAgICB9XG59KTtcblxuXHQkKCcjZmlyc3RfbmFtZSwgI21pZGRsZV9uYW1lLCAjbGFzdF9uYW1lJykua2V5cHJlc3MoZnVuY3Rpb24oZXZlbnQpe1xuXHRcdHZhciBpbnB1dFZhbHVlID0gZXZlbnQuY2hhckNvZGU7XG5cdFx0Ly9hbGVydChpbnB1dFZhbHVlKTtcblx0XHRpZighKChpbnB1dFZhbHVlID4gNjQgJiYgaW5wdXRWYWx1ZSA8IDkxKSB8fCAoaW5wdXRWYWx1ZSA+IDk2ICYmIGlucHV0VmFsdWUgPCAxMjMpfHwoaW5wdXRWYWx1ZT09MzIpIHx8IChpbnB1dFZhbHVlPT0wKSkpe1xuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0fVxuXHR9KTtcblxuXHQkKFwiI2NvbnRhY3RfbnVtYmVyLCAjYWx0ZXJuYXRlX2NvbnRhY3RfbnVtYmVyLCAjemlwX2NvZGVcIikua2V5cHJlc3MoZnVuY3Rpb24gKGUpIHtcblx0IC8vaWYgdGhlIGxldHRlciBpcyBub3QgZGlnaXQgdGhlbiBkaXNwbGF5IGVycm9yIGFuZCBkb24ndCB0eXBlIGFueXRoaW5nXG5cdCAgICBpZiAoZS53aGljaCAhPSA4ICYmIGUud2hpY2ggIT0gMCAmJiAoZS53aGljaCA8IDQ4IHx8IGUud2hpY2ggPiA1NykpIHtcblx0ICAgICAgICByZXR1cm4gZmFsc2U7XG5cdCAgICB9XG5cdH0pO1xuXG5cdC8vICQoJyNncm91cF9pZCcpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0Ly8gICAgIGFsZXJ0KCQodGhpcykudmFsKCkpO1xuXHQvLyB9KTtcblxuXHQkKCdmb3JtI2Zvcm0tY2xpZW50Jykub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGUpIHtcblx0XHR2YXIgZmlyc3ROYW1lID0gJCgnaW5wdXQjZmlyc3RfbmFtZScpLnZhbCgpO1xuXHRcdHZhciBsYXN0TmFtZSA9ICQoJ2lucHV0I2xhc3RfbmFtZScpLnZhbCgpO1xuXHRcdHZhciBnZW5kZXIgPSAkKCdzZWxlY3RbbmFtZT1nZW5kZXJdJykudmFsKCk7XG5cdFx0dmFyIGJpcnRoZGF0ZSA9ICQoJ2lucHV0I2JpcnRoX2RhdGUnKS52YWwoKTtcblx0XHR2YXIgY29udGFjdE51bWJlciA9ICQoJ2lucHV0I2NvbnRhY3RfbnVtYmVyJykudmFsKCk7XG5cdFx0dmFyIHBhc3Nwb3J0ID0gJCgnaW5wdXQjcGFzc3BvcnQnKS52YWwoKTtcblx0XHR2YXIgZ3JvdXBfaWQgPSAkKCcjZ3JvdXBfaWQnKS52YWwoKTtcblxuXHRcdCQuYWpheCh7XG5cdFx0XHR1cmw6ICd2YWxpZGF0aW9uQmVmb3JlU3VibWl0JyxcbiAgICAgICAgICAgIHR5cGU6ICdnZXQnLFxuICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgXHRmaXJzdE5hbWU6Zmlyc3ROYW1lLCBcblx0XHRcdFx0bGFzdE5hbWU6IGxhc3ROYW1lLFxuXHRcdFx0XHRnZW5kZXI6IGdlbmRlcixcblx0XHRcdFx0YmlydGhkYXRlOiBiaXJ0aGRhdGUsXG5cdFx0XHRcdGNvbnRhY3ROdW1iZXI6IGNvbnRhY3ROdW1iZXIsXG5cdFx0XHRcdHBhc3Nwb3J0OiBwYXNzcG9ydCxcblx0XHRcdFx0Z3JvdXBfaWQ6IGdyb3VwX2lkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYXN5bmM6IGZhbHNlLFxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAgICAgXHR2YXIgZGF0YSA9IEpTT04ucGFyc2UoZGF0YSk7XG5cbiAgICAgICAgICAgIFx0Q2xpZW50cy5jbGllbnRzID0gZGF0YS51c2VycztcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZihkYXRhLmJpbmRlZCkge1xuICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKCdDb250YWN0IG51bWJlciBpcyBhbHJlYWR5IGJpbmRlZCBpbiB0aGUgdmlzYSBhcHAuJyk7XG4gICAgICAgICAgICAgICAgXHRlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmKGRhdGEuZHVwbGljYXRlRW50cnkpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgnI2R1cGxpY2F0ZUNsaWVudHNMaXN0TW9kYWwnKS5tb2RhbCgnc2hvdycpO1xuICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgfSAgIFxuICAgICAgICAgICAgfVxuXHRcdH0pO1xuXHR9KTtcblxufSApO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY2xpZW50cy9pbmRleC5qcyIsIi8qXHJcblx0TUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcclxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXHJcbiovXHJcbi8vIGNzcyBiYXNlIGNvZGUsIGluamVjdGVkIGJ5IHRoZSBjc3MtbG9hZGVyXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oKSB7XHJcblx0dmFyIGxpc3QgPSBbXTtcclxuXHJcblx0Ly8gcmV0dXJuIHRoZSBsaXN0IG9mIG1vZHVsZXMgYXMgY3NzIHN0cmluZ1xyXG5cdGxpc3QudG9TdHJpbmcgPSBmdW5jdGlvbiB0b1N0cmluZygpIHtcclxuXHRcdHZhciByZXN1bHQgPSBbXTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHZhciBpdGVtID0gdGhpc1tpXTtcclxuXHRcdFx0aWYoaXRlbVsyXSkge1xyXG5cdFx0XHRcdHJlc3VsdC5wdXNoKFwiQG1lZGlhIFwiICsgaXRlbVsyXSArIFwie1wiICsgaXRlbVsxXSArIFwifVwiKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXN1bHQucHVzaChpdGVtWzFdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHJlc3VsdC5qb2luKFwiXCIpO1xyXG5cdH07XHJcblxyXG5cdC8vIGltcG9ydCBhIGxpc3Qgb2YgbW9kdWxlcyBpbnRvIHRoZSBsaXN0XHJcblx0bGlzdC5pID0gZnVuY3Rpb24obW9kdWxlcywgbWVkaWFRdWVyeSkge1xyXG5cdFx0aWYodHlwZW9mIG1vZHVsZXMgPT09IFwic3RyaW5nXCIpXHJcblx0XHRcdG1vZHVsZXMgPSBbW251bGwsIG1vZHVsZXMsIFwiXCJdXTtcclxuXHRcdHZhciBhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzID0ge307XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaWQgPSB0aGlzW2ldWzBdO1xyXG5cdFx0XHRpZih0eXBlb2YgaWQgPT09IFwibnVtYmVyXCIpXHJcblx0XHRcdFx0YWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xyXG5cdFx0fVxyXG5cdFx0Zm9yKGkgPSAwOyBpIDwgbW9kdWxlcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaXRlbSA9IG1vZHVsZXNbaV07XHJcblx0XHRcdC8vIHNraXAgYWxyZWFkeSBpbXBvcnRlZCBtb2R1bGVcclxuXHRcdFx0Ly8gdGhpcyBpbXBsZW1lbnRhdGlvbiBpcyBub3QgMTAwJSBwZXJmZWN0IGZvciB3ZWlyZCBtZWRpYSBxdWVyeSBjb21iaW5hdGlvbnNcclxuXHRcdFx0Ly8gIHdoZW4gYSBtb2R1bGUgaXMgaW1wb3J0ZWQgbXVsdGlwbGUgdGltZXMgd2l0aCBkaWZmZXJlbnQgbWVkaWEgcXVlcmllcy5cclxuXHRcdFx0Ly8gIEkgaG9wZSB0aGlzIHdpbGwgbmV2ZXIgb2NjdXIgKEhleSB0aGlzIHdheSB3ZSBoYXZlIHNtYWxsZXIgYnVuZGxlcylcclxuXHRcdFx0aWYodHlwZW9mIGl0ZW1bMF0gIT09IFwibnVtYmVyXCIgfHwgIWFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaXRlbVswXV0pIHtcclxuXHRcdFx0XHRpZihtZWRpYVF1ZXJ5ICYmICFpdGVtWzJdKSB7XHJcblx0XHRcdFx0XHRpdGVtWzJdID0gbWVkaWFRdWVyeTtcclxuXHRcdFx0XHR9IGVsc2UgaWYobWVkaWFRdWVyeSkge1xyXG5cdFx0XHRcdFx0aXRlbVsyXSA9IFwiKFwiICsgaXRlbVsyXSArIFwiKSBhbmQgKFwiICsgbWVkaWFRdWVyeSArIFwiKVwiO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRsaXN0LnB1c2goaXRlbSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9O1xyXG5cdHJldHVybiBsaXN0O1xyXG59O1xyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcbi8vIG1vZHVsZSBpZCA9IDJcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgMyA0IDUgOSAxMSAxMiAxMyIsIjx0ZW1wbGF0ZT5cblxuXHQ8Zm9ybSBpZD1cImFkZC10ZW1wb3JhcnktY2xpZW50LWZvcm1cIiBjbGFzcz1cImZvcm0taG9yaXpvbnRhbFwiIEBzdWJtaXQucHJldmVudD1cImFkZFRlbXBvcmFyeUNsaWVudFwiPlxuXHRcdCAgICAgICAgXG5cdFx0PGRpdiB2LXNob3c9XCJzY3JlZW4gPT0gMVwiPlxuXHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gcHVsbC1sZWZ0XCIgQGNsaWNrPVwiYWRkQ29udGFjdE51bWJlclwiPlxuXHRcdFx0XHQ8aSBjbGFzcz1cImZhIGZhLXBsdXNcIj48L2k+IEFkZCBDb250YWN0IE51bWJlclxuXHRcdFx0PC9idXR0b24+XG5cblx0XHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cblxuXHRcdFx0PGRpdiB2LWZvcj1cIihjb250YWN0X251bWJlciwgaW5kZXgpIGluIHRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzXCIgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtc20tMVwiPlxuXHRcdFx0XHRcdDxidXR0b24gdi1pZj1cImluZGV4ICE9IDBcIiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRhbmdlciBwdWxsLXJpZ2h0XCIgQGNsaWNrPVwicmVtb3ZlQ29udGFjdE51bWJlcihpbmRleClcIj5cblx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtdGltZXNcIj48L2k+XG5cdFx0XHRcdFx0PC9idXR0b24+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHQ8bGFiZWwgZm9yPVwiY29udGFjdF9udW1iZXJcIiBjbGFzcz1cImNvbC1zbS0zIGNvbnRyb2wtbGFiZWxcIj4oe3sgaW5kZXgrMSB9fSkgQ29udGFjdCBOby46PC9sYWJlbD5cblx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1zbS0zXCI+XG5cdFx0XHRcdFx0PHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJjb3VudHJ5X2NvZGVbXVwiIHYtbW9kZWw9XCJjb250YWN0X251bWJlci5jb3VudHJ5X2NvZGVcIj5cblx0XHRcdFx0XHRcdDxvcHRpb24gdmFsdWU9XCIrNjNcIj4rNjM8L29wdGlvbj5cblx0XHRcdFx0XHQ8L3NlbGVjdD5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtc20tNVwiPlxuXHRcdFx0XHRcdDxpbnB1dCB0eXBlPVwibnVtYmVyXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwiY29udGFjdF9udW1iZXJbXVwiIHYtbW9kZWw9XCJjb250YWN0X251bWJlci52YWx1ZVwiPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIgQGNsaWNrPVwiYWRkVGVtcG9yYXJ5Q2xpZW50VmFsaWRhdGlvblwiPlxuXHRcdFx0XHRDb250aW51ZVxuXHRcdFx0PC9idXR0b24+XG5cdFx0ICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuXHQgICAgPC9kaXY+XG5cblx0ICAgIDxkaXYgdi1zaG93PVwic2NyZWVuID09IDJcIj5cblx0ICAgIFx0PGRpdiBjbGFzcz1cInBhbmVsIHBhbmVsLWRlZmF1bHRcIiB2LWZvcj1cIihjb250YWN0X251bWJlciwgaW5kZXgpIGluIHRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWhlYWRpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAge3sgY29udGFjdF9udW1iZXIuY291bnRyeV9jb2RlIH19e3sgY29udGFjdF9udW1iZXIudmFsdWUgfX1cbiAgICAgICAgICAgICAgICAgICAgPGEgdi1pZj1cImlmRXhpc3RBcnJheVtpbmRleF1cIiA6aHJlZj1cIicvdmlzYS9jbGllbnQvJyArIHVzZXJBcnJheVtpbmRleF0uaWRcIiBjbGFzcz1cInB1bGwtcmlnaHRcIiB0YXJnZXQ9XCJfYmxhbmtcIj5cbiAgICAgICAgICAgICAgICAgICAgXHRWaWV3IEV4aXN0aW5nIFByb2ZpbGVcbiAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwYW5lbC1ib2R5XCI+XG4gICAgICAgICAgICAgICAgXHQ8Zm9ybSBjbGFzcz1cImZvcm0taG9yaXpvbnRhbFwiPlxuXG4gICAgICAgICAgICAgICAgXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0ICAgIFx0PGxhYmVsIGNsYXNzPVwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiPlN0YXR1czo8L2xhYmVsPlxuXHRcdFx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJjb2wtc20tMTBcIj5cblx0XHRcdFx0XHQgICAgXHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6NXB4O2NsZWFyOmJvdGg7XCI+PC9kaXY+XG5cblx0XHRcdFx0XHQgICAgICBcdFx0PHNwYW4gY2xhc3M9XCJsYWJlbFwiIFxuXHRcdFx0XHRcdCAgICAgIFx0XHRcdDpjbGFzcz1cInsnbGFiZWwtcHJpbWFyeSc6IWlmRXhpc3RBcnJheVtpbmRleF0sICdsYWJlbC1kYW5nZXInOmlmRXhpc3RBcnJheVtpbmRleF19XCI+XG5cdFx0XHRcdFx0ICAgICAgXHRcdFx0e3sgKGlmRXhpc3RBcnJheVtpbmRleF0pID8gJ0V4aXN0aW5nJyA6ICdOb3QgRXhpc3RpbmcnIH19XG5cdFx0XHRcdFx0ICAgICAgXHRcdDwvc3Bhbj5cblxuXHRcdFx0XHRcdCAgICAgIFx0XHQ8c3BhbiBjbGFzcz1cImxhYmVsXCIgXG5cdFx0XHRcdFx0ICAgICAgXHRcdFx0OmNsYXNzPVwieydsYWJlbC1wcmltYXJ5JzohaWZCaW5kZWRBcnJheVtpbmRleF0sICdsYWJlbC1kYW5nZXInOmlmQmluZGVkQXJyYXlbaW5kZXhdfVwiXG5cdFx0XHRcdFx0ICAgICAgXHRcdFx0c3R5bGU9XCJtYXJnaW4tbGVmdDoxMHB4O1wiPlxuXHRcdFx0XHRcdCAgICAgIFx0XHRcdHt7IChpZkJpbmRlZEFycmF5W2luZGV4XSkgPyAnQmluZGVkJyA6ICdOb3QgQmluZGVkJyB9fVxuXHRcdFx0XHRcdCAgICAgIFx0XHQ8L3NwYW4+XG5cblx0XHRcdFx0XHQgICAgICBcdFx0PHNwYW4gY2xhc3M9XCJsYWJlbFwiIFxuXHRcdFx0XHRcdCAgICAgIFx0XHRcdDpjbGFzcz1cInsnbGFiZWwtcHJpbWFyeSc6IWlmR3JvdXBMZWFkZXJBcnJheVtpbmRleF0sICdsYWJlbC1kYW5nZXInOmlmR3JvdXBMZWFkZXJBcnJheVtpbmRleF19XCIgc3R5bGU9XCJtYXJnaW4tbGVmdDoxMHB4O1wiPlxuXHRcdFx0XHRcdCAgICAgIFx0XHRcdHt7IChpZkdyb3VwTGVhZGVyQXJyYXlbaW5kZXhdKSA/ICdHcm91cCBMZWFkZXInIDogJ05vdCBHcm91cCBMZWFkZXInIH19XG5cdFx0XHRcdFx0ICAgICAgXHRcdDwvc3Bhbj5cblx0XHRcdFx0XHQgICAgXHQ8L2Rpdj5cblx0XHRcdFx0XHQgIFx0PC9kaXY+XG5cblx0XHRcdFx0XHQgIFx0PGRpdiB2LXNob3c9XCJpZkV4aXN0QXJyYXlbaW5kZXhdXCIgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0ICAgIFx0PGxhYmVsIGNsYXNzPVwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiPkFjdGlvbjo8L2xhYmVsPlxuXHRcdFx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJjb2wtc20tMTBcIj5cblx0XHRcdFx0XHQgICAgICBcdFx0PGxhYmVsIGNsYXNzPVwicmFkaW8taW5saW5lXCI+XG5cdFx0XHRcdFx0XHRcdFx0ICBcdDxpbnB1dCB0eXBlPVwicmFkaW9cIiA6bmFtZT1cIidhY3Rpb24tJytpbmRleFwiIHZhbHVlPVwicHJvY2VlZF9hbnl3YXlcIiB2LW1vZGVsPVwiY29udGFjdF9udW1iZXIuYWN0aW9uXCIgOmRpc2FibGVkPVwiaWZCaW5kZWRBcnJheVtpbmRleF0gfHwgaWZHcm91cExlYWRlckFycmF5W2luZGV4XVwiIC8+IFByb2NlZWQgQW55d2F5XG5cdFx0XHRcdFx0XHRcdFx0PC9sYWJlbD5cblx0XHRcdFx0XHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJyYWRpby1pbmxpbmVcIj5cblx0XHRcdFx0XHRcdFx0XHQgIFx0PGlucHV0IHR5cGU9XCJyYWRpb1wiIDpuYW1lPVwiJ2FjdGlvbi0nK2luZGV4XCIgdmFsdWU9XCJ1c2VfZXhpc3RpbmdfcHJvZmlsZVwiIHYtbW9kZWw9XCJjb250YWN0X251bWJlci5hY3Rpb25cIiAvPiBcblx0XHRcdFx0XHRcdFx0XHQgIFx0VXNlIEV4aXN0aW5nIFByb2ZpbGVcblx0XHRcdFx0XHRcdFx0XHQ8L2xhYmVsPlxuXHRcdFx0XHRcdCAgICBcdDwvZGl2PlxuXHRcdFx0XHRcdCAgXHQ8L2Rpdj5cblxuXHRcdFx0XHRcdCAgXHQ8ZGl2IHYtc2hvdz1cIiFpZkV4aXN0QXJyYXlbaW5kZXhdIHx8IGNvbnRhY3RfbnVtYmVyLmFjdGlvbiA9PSAncHJvY2VlZF9hbnl3YXknXCIgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiPkFkZCBEZXRhaWxzOjwvbGFiZWw+XG5cdFx0XHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMFwiPlxuXHRcdFx0XHRcdFx0ICAgICAgXHQ8bGFiZWwgY2xhc3M9XCJyYWRpby1pbmxpbmVcIj5cblx0XHRcdFx0XHRcdFx0XHRcdDxpbnB1dCB0eXBlPVwicmFkaW9cIiA6bmFtZT1cIidwcm9jZXNzLScraW5kZXhcIiB2YWx1ZT1cImNvbXBsZXRlXCIgdi1tb2RlbD1cImNvbnRhY3RfbnVtYmVyLnByb2Nlc3NcIiAvPiBOb1xuXHRcdFx0XHRcdFx0XHRcdDwvbGFiZWw+XG5cdFx0XHRcdFx0XHRcdFx0PGxhYmVsIGNsYXNzPVwicmFkaW8taW5saW5lXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8aW5wdXQgdHlwZT1cInJhZGlvXCIgOm5hbWU9XCIncHJvY2Vzcy0nK2luZGV4XCIgdmFsdWU9XCJkZXRhaWxcIiB2LW1vZGVsPVwiY29udGFjdF9udW1iZXIucHJvY2Vzc1wiIC8+IFllc1xuXHRcdFx0XHRcdFx0XHRcdDwvbGFiZWw+XG5cdFx0XHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcdFx0ICBcdDwvZGl2PlxuXG5cdFx0XHRcdFx0ICBcdDxkaXYgdi1zaG93PVwiY29udGFjdF9udW1iZXIucHJvY2VzcyA9PSAnZGV0YWlsJ1wiIGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHRcdCAgXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHQgICAgXHQ8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb250cm9sLWxhYmVsXCI+Rmlyc3QgTmFtZTwvbGFiZWw+XG5cdFx0XHRcdFx0XHQgICAgXHQ8ZGl2IGNsYXNzPVwiY29sLXNtLTEwXCI+XG5cdFx0XHRcdFx0XHQgICAgICBcdFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwiZmlyc3RfbmFtZVwiIHYtbW9kZWw9XCJjb250YWN0X251bWJlci5maXJzdF9uYW1lXCIgcGxhY2Vob2xkZXI9XCJGaXJzdCBOYW1lXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgLz5cblx0XHRcdFx0XHRcdCAgICBcdDwvZGl2PlxuXHRcdFx0XHRcdFx0ICBcdDwvZGl2PlxuXG5cdFx0XHRcdFx0XHQgIFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0XHRcdCAgICBcdDxsYWJlbCBjbGFzcz1cImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcIj5MYXN0IE5hbWU8L2xhYmVsPlxuXHRcdFx0XHRcdFx0ICAgIFx0PGRpdiBjbGFzcz1cImNvbC1zbS0xMFwiPlxuXHRcdFx0XHRcdFx0ICAgICAgXHRcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cImxhc3RfbmFtZVwiIHYtbW9kZWw9XCJjb250YWN0X251bWJlci5sYXN0X25hbWVcIiBwbGFjZWhvbGRlcj1cIkxhc3QgTmFtZVwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG5cdFx0XHRcdFx0XHQgICAgXHQ8L2Rpdj5cblx0XHRcdFx0XHRcdCAgXHQ8L2Rpdj5cblxuXHRcdFx0XHRcdFx0ICBcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHQgICAgXHQ8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb250cm9sLWxhYmVsXCI+UGFzc3BvcnQ8L2xhYmVsPlxuXHRcdFx0XHRcdFx0ICAgIFx0PGRpdiBjbGFzcz1cImNvbC1zbS0xMFwiPlxuXHRcdFx0XHRcdFx0ICAgICAgXHRcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cInBhc3Nwb3J0XCIgdi1tb2RlbD1cImNvbnRhY3RfbnVtYmVyLnBhc3Nwb3J0XCIgcGxhY2Vob2xkZXI9XCJQYXNzcG9ydFwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG5cdFx0XHRcdFx0XHQgICAgXHQ8L2Rpdj5cblx0XHRcdFx0XHRcdCAgXHQ8L2Rpdj5cblxuXHRcdFx0XHRcdFx0ICBcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHQgICAgXHQ8bGFiZWwgY2xhc3M9XCJjb2wtc20tMiBjb250cm9sLWxhYmVsXCI+QmlydGhkYXRlPC9sYWJlbD5cblx0XHRcdFx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJjb2wtc20tMTBcIj5cblx0XHRcdFx0XHRcdCAgICAgIFx0XHQ8aW5wdXQgdHlwZT1cImRhdGVcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJiaXJ0aGRhdGVcIiB2LW1vZGVsPVwiY29udGFjdF9udW1iZXIuYmlydGhkYXRlXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgLz5cblx0XHRcdFx0XHRcdCAgICBcdDwvZGl2PlxuXHRcdFx0XHRcdFx0ICBcdDwvZGl2PlxuXG5cdFx0XHRcdFx0XHQgIFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdFx0XHRcdCAgICBcdDxsYWJlbCBjbGFzcz1cImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcIj5HZW5kZXI8L2xhYmVsPlxuXHRcdFx0XHRcdFx0ICAgIFx0PGRpdiBjbGFzcz1cImNvbC1zbS0xMFwiPlxuXHRcdFx0XHRcdFx0ICAgICAgXHRcdDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwiZ2VuZGVyXCIgdi1tb2RlbD1cImNvbnRhY3RfbnVtYmVyLmdlbmRlclwiPlxuXHRcdFx0XHRcdFx0ICAgICAgXHRcdFx0PG9wdGlvbiB2YWx1ZT1cIk1hbGVcIj5NYWxlPC9vcHRpb24+XG5cdFx0XHRcdFx0XHQgICAgICBcdFx0XHQ8b3B0aW9uIHZhbHVlPVwiRmVtYWxlXCI+RmVtYWxlPC9vcHRpb24+XG5cdFx0XHRcdFx0XHQgICAgICBcdFx0PC9zZWxlY3Q+XG5cdFx0XHRcdFx0XHQgICAgXHQ8L2Rpdj5cblx0XHRcdFx0XHRcdCAgXHQ8L2Rpdj5cblx0XHRcdFx0XHQgIFx0PC9kaXY+XG5cblx0XHRcdFx0XHQ8L2Zvcm0+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cblxuXHQgICAgXHQ8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBpZD1cInRlbXBvcmFyeUNsaWVudEZvcm1TdWJtaXRCdG5cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCI+XG5cdFx0XHRcdFNhdmVcblx0XHRcdDwvYnV0dG9uPlxuXHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIiBAY2xpY2s9XCJzY3JlZW4gPSAxXCI+QmFjazwvYnV0dG9uPlxuXHRcdCAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cblx0ICAgIDwvZGl2PlxuXG5cdDwvZm9ybT5cblxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblxuICAgIGV4cG9ydCBkZWZhdWx0IHtcbiAgICBcdGRhdGEoKSB7XG4gICAgXHRcdHJldHVybiB7XG4gICAgXHRcdFx0dGVtcG9yYXJ5Q2xpZW50Rm9ybTogbmV3IEZvcm0oe1xuXHRcdFx0XHRcdGNvbnRhY3RfbnVtYmVyczogW3tjb3VudHJ5X2NvZGU6ICcnLCB2YWx1ZTogJycsIGFjdGlvbjogJycsIHByb2Nlc3M6ICcnLCBmaXJzdF9uYW1lOiAnJywgbGFzdF9uYW1lOiAnJywgcGFzc3BvcnQ6ICcnLCBiaXJ0aGRhdGU6JycsIGdlbmRlcjogJycsIGlmRXhpc3Q6IGZhbHNlLCBpZkJpbmRlZDogZmFsc2UsIGlmR3JvdXBMZWFkZXI6IGZhbHNlfV1cblx0XHRcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSksXG5cblx0XHRcdFx0c2NyZWVuOiAxLFxuXG5cdFx0XHRcdGlmRXhpc3RBcnJheTogW10sXG5cdFx0XHRcdGlmQmluZGVkQXJyYXk6IFtdLFxuXHRcdFx0XHRpZkdyb3VwTGVhZGVyQXJyYXk6IFtdLFxuXHRcdFx0XHR1c2VyQXJyYXk6IFtdLFxuICAgIFx0XHR9XG4gICAgXHR9LFxuXG4gICAgXHRtZXRob2RzOiB7XG4gICAgXHRcdHJlc2V0VGVtcG9yYXJ5Q2xpZW50Rm9ybSgpIHtcblx0ICAgIFx0XHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0gPSBuZXcgRm9ybSh7XG5cdFx0XHRcdFx0Y29udGFjdF9udW1iZXJzOiBbe2NvdW50cnlfY29kZTogJycsIHZhbHVlOiAnJywgYWN0aW9uOiAnJywgcHJvY2VzczogJycsIGZpcnN0X25hbWU6ICcnLCBsYXN0X25hbWU6ICcnLCBwYXNzcG9ydDogJycsIGJpcnRoZGF0ZTonJywgZ2VuZGVyOiAnJywgaWZFeGlzdDogZmFsc2UsIGlmQmluZGVkOiBmYWxzZSwgaWZHcm91cExlYWRlcjogZmFsc2V9XVxuXHRcdFx0XHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KTtcblx0ICAgIFx0fSxcblxuXHQgICAgXHRyZXNldEFycmF5KCkge1xuXHQgICAgXHRcdHRoaXMuaWZFeGlzdEFycmF5ID0gW107XG5cdCAgICBcdFx0dGhpcy5pZkJpbmRlZEFycmF5ID0gW107XG5cdCAgICBcdFx0dGhpcy5pZkdyb3VwTGVhZGVyQXJyYXkgPSBbXTtcblx0ICAgIFx0XHR0aGlzLnVzZXJBcnJheSA9IFtdO1xuXHQgICAgXHR9LFxuXG5cdCAgICBcdHZhbGlkYXRlQ29udGFjdE51bWJlcnMoKSB7XG5cdCAgICBcdFx0bGV0IGlzVmFsaWQgPSB0cnVlO1xuXG5cdCAgICBcdFx0dGhpcy50ZW1wb3JhcnlDbGllbnRGb3JtLmNvbnRhY3RfbnVtYmVycy5mb3JFYWNoKChjb250YWN0TnVtYmVyLCBpbmRleCkgPT4ge1xuXHQgICAgXHRcdFx0aWYoY29udGFjdE51bWJlci5jb3VudHJ5X2NvZGUgPT0gJycpIHtcblx0ICAgIFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHQgICAgXHRcdFx0XHR0b2FzdHIuZXJyb3IoJ0NvbnRhY3QgTnVtYmVyICcgKyAoaW5kZXgrMSkgKyAnIGNvdW50cnkgY29kZSBpcyBpbnZhbGlkLicpO1xuXHQgICAgXHRcdFx0fVxuXG5cdCAgICBcdFx0XHRpZihjb250YWN0TnVtYmVyLnZhbHVlID09ICcnKSB7XG5cdCAgICBcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0ICAgIFx0XHRcdFx0dG9hc3RyLmVycm9yKCdDb250YWN0IE51bWJlciAnICsgKGluZGV4KzEpICsgJyBpcyBpbnZhbGlkLicpO1xuXHQgICAgXHRcdFx0fVxuXG5cdCAgICBcdFx0XHRpZihjb250YWN0TnVtYmVyLnZhbHVlLmxlbmd0aCAhPSAxMCkge1xuXHQgICAgXHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdCAgICBcdFx0XHRcdHRvYXN0ci5lcnJvcignQ29udGFjdCBOdW1iZXIgJyArIChpbmRleCsxKSArICcgbXVzdCBiZSAxMCBkaWdpdCBudW1iZXIuJyk7XG5cdCAgICBcdFx0XHR9XG5cdCAgICBcdFx0fSk7XG5cblx0ICAgIFx0XHRyZXR1cm4gaXNWYWxpZDtcblx0ICAgIFx0fSxcblxuXHQgICAgXHRhZGRUZW1wb3JhcnlDbGllbnRWYWxpZGF0aW9uKCkge1xuXHQgICAgXHRcdGxldCB2YWxpZCA9IHRoaXMudmFsaWRhdGVDb250YWN0TnVtYmVycygpO1xuXG5cdCAgICBcdFx0aWYodmFsaWQpIHtcblx0XHRcdFx0XHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uc3VibWl0KCdwb3N0JywgJy92aXNhL2NsaWVudC9hZGQtdGVtcG9yYXJ5LXZhbGlkYXRpb24nKVxuXHRcdFx0XHQgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0ICAgICAgICBcdGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcblx0XHRcdFx0ICAgICAgICBcdFx0dGhpcy5zY3JlZW4gPSAyO1xuXG5cdFx0XHRcdCAgICAgICAgXHRcdHRoaXMuaWZFeGlzdEFycmF5ID0gcmVzcG9uc2UuaWZFeGlzdEFycmF5O1xuXHRcdFx0XHQgICAgICAgIFx0XHR0aGlzLmlmQmluZGVkQXJyYXkgPSByZXNwb25zZS5pZkJpbmRlZEFycmF5O1xuXHRcdFx0XHQgICAgICAgIFx0XHR0aGlzLmlmR3JvdXBMZWFkZXJBcnJheSA9IHJlc3BvbnNlLmlmR3JvdXBMZWFkZXJBcnJheTtcblx0XHRcdFx0ICAgICAgICBcdFx0dGhpcy51c2VyQXJyYXkgPSByZXNwb25zZS51c2VyQXJyYXk7XG5cblx0XHRcdFx0ICAgICAgICBcdFx0bGV0IGNvdW50ID0gdGhpcy50ZW1wb3JhcnlDbGllbnRGb3JtLmNvbnRhY3RfbnVtYmVycy5sZW5ndGg7XG5cdFx0XHRcdCAgICAgICAgXHRcdGZvcihsZXQgaT0wOyBpPGNvdW50OyBpKyspIHtcblx0XHRcdFx0ICAgICAgICBcdFx0XHRpZih0aGlzLmlmQmluZGVkQXJyYXlbaV0gfHwgdGhpcy5pZkdyb3VwTGVhZGVyQXJyYXlbaV0pIHtcblx0XHRcdFx0ICAgICAgICBcdFx0XHRcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnNbaV0uYWN0aW9uID0gJ3VzZV9leGlzdGluZ19wcm9maWxlJztcblx0XHRcdFx0ICAgICAgICBcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQgICAgICAgIFx0XHRcdFx0dGhpcy50ZW1wb3JhcnlDbGllbnRGb3JtLmNvbnRhY3RfbnVtYmVyc1tpXS5hY3Rpb24gPSAncHJvY2VlZF9hbnl3YXknO1xuXHRcdFx0XHQgICAgICAgIFx0XHRcdH1cblxuXHRcdFx0XHQgICAgICAgIFx0XHRcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnNbaV0ucHJvY2VzcyA9ICdjb21wbGV0ZSc7XG5cblx0XHRcdFx0ICAgICAgICBcdFx0XHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzW2ldLmlmRXhpc3QgPSB0aGlzLmlmRXhpc3RBcnJheVtpXTtcblx0XHRcdFx0ICAgICAgICBcdFx0XHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzW2ldLmlmQmluZGVkID0gdGhpcy5pZkJpbmRlZEFycmF5W2ldO1xuXHRcdFx0XHQgICAgICAgIFx0XHRcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnNbaV0uaWZHcm91cExlYWRlciA9IHRoaXMuaWZHcm91cExlYWRlckFycmF5W2ldO1xuXHRcdFx0XHQgICAgICAgIFx0XHR9XG5cdFx0XHRcdCAgICAgICAgXHR9XG5cdFx0XHRcdCAgICAgICAgfSk7XG5cdCAgICBcdFx0fVxuXHQgICAgXHR9LFxuXG5cdCAgICBcdHZhbGlkYXRlSW5wdXRzKCkge1xuXHQgICAgXHRcdGxldCBpc1ZhbGlkID0gdHJ1ZTtcblxuXHQgICAgXHRcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnMuZm9yRWFjaCgoY29udGFjdE51bWJlciwgaW5kZXgpID0+IHtcblx0ICAgIFx0XHRcdGlmKGNvbnRhY3ROdW1iZXIuYWN0aW9uID09ICcnKSB7XG5cdCAgICBcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0ICAgIFx0XHRcdFx0dG9hc3RyLmVycm9yKCdUaGUgYWN0aW9uIGZpZWxkIGlzIHJlcXVpcmVkLicsIGNvbnRhY3ROdW1iZXIuY291bnRyeV9jb2RlICsgY29udGFjdE51bWJlci52YWx1ZSk7XG5cdCAgICBcdFx0XHR9XG5cblx0ICAgIFx0XHRcdGlmKGNvbnRhY3ROdW1iZXIucHJvY2VzcyA9PSAnJyAmJiAoY29udGFjdE51bWJlci5hY3Rpb24gPT0gJ3Byb2NlZWRfYW55d2F5JyB8fCAhaWZFeGlzdEFycmF5W2luZGV4XSkgKSB7XG5cdCAgICBcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0ICAgIFx0XHRcdFx0dG9hc3RyLmVycm9yKCdUaGUgYWRkIGRldGFpbHMgZmllbGQgaXMgcmVxdWlyZWQuJywgY29udGFjdE51bWJlci5jb3VudHJ5X2NvZGUgKyBjb250YWN0TnVtYmVyLnZhbHVlKTtcblx0ICAgIFx0XHRcdH1cblxuXHQgICAgXHRcdFx0aWYoY29udGFjdE51bWJlci5maXJzdF9uYW1lID09ICcnICYmIGNvbnRhY3ROdW1iZXIucHJvY2VzcyA9PSAnZGV0YWlsJykge1xuXHQgICAgXHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdCAgICBcdFx0XHRcdHRvYXN0ci5lcnJvcignVGhlIGZpcnN0IG5hbWUgZmllbGQgaXMgcmVxdWlyZWQuJywgY29udGFjdE51bWJlci5jb3VudHJ5X2NvZGUgKyBjb250YWN0TnVtYmVyLnZhbHVlKTtcblx0ICAgIFx0XHRcdH1cblxuXHQgICAgXHRcdFx0aWYoY29udGFjdE51bWJlci5sYXN0X25hbWUgPT0gJycgJiYgY29udGFjdE51bWJlci5wcm9jZXNzID09ICdkZXRhaWwnKSB7XG5cdCAgICBcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0ICAgIFx0XHRcdFx0dG9hc3RyLmVycm9yKCdUaGUgbGFzdCBuYW1lIGZpZWxkIGlzIHJlcXVpcmVkLicsIGNvbnRhY3ROdW1iZXIuY291bnRyeV9jb2RlICsgY29udGFjdE51bWJlci52YWx1ZSk7XG5cdCAgICBcdFx0XHR9XG5cblx0ICAgIFx0XHRcdGlmKGNvbnRhY3ROdW1iZXIucGFzc3BvcnQgPT0gJycgJiYgY29udGFjdE51bWJlci5wcm9jZXNzID09ICdkZXRhaWwnKSB7XG5cdCAgICBcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0ICAgIFx0XHRcdFx0dG9hc3RyLmVycm9yKCdUaGUgcGFzc3BvcnQgZmllbGQgaXMgcmVxdWlyZWQuJywgY29udGFjdE51bWJlci5jb3VudHJ5X2NvZGUgKyBjb250YWN0TnVtYmVyLnZhbHVlKTtcblx0ICAgIFx0XHRcdH1cblxuXHQgICAgXHRcdFx0aWYoY29udGFjdE51bWJlci5iaXJ0aGRhdGUgPT0gJycgJiYgY29udGFjdE51bWJlci5wcm9jZXNzID09ICdkZXRhaWwnKSB7XG5cdCAgICBcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0ICAgIFx0XHRcdFx0dG9hc3RyLmVycm9yKCdUaGUgYmlydGhkYXRlIGZpZWxkIGlzIHJlcXVpcmVkLicsIGNvbnRhY3ROdW1iZXIuY291bnRyeV9jb2RlICsgY29udGFjdE51bWJlci52YWx1ZSk7XG5cdCAgICBcdFx0XHR9XG5cblx0ICAgIFx0XHRcdGlmKGNvbnRhY3ROdW1iZXIuZ2VuZGVyID09ICcnICYmIGNvbnRhY3ROdW1iZXIucHJvY2VzcyA9PSAnZGV0YWlsJykge1xuXHQgICAgXHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdCAgICBcdFx0XHRcdHRvYXN0ci5lcnJvcignVGhlIGdlbmRlciBmaWVsZCBpcyByZXF1aXJlZC4nLCBjb250YWN0TnVtYmVyLmNvdW50cnlfY29kZSArIGNvbnRhY3ROdW1iZXIudmFsdWUpO1xuXHQgICAgXHRcdFx0fVxuXHQgICAgXHRcdH0pO1xuXG5cdCAgICBcdFx0cmV0dXJuIGlzVmFsaWQ7XG5cdCAgICBcdH0sXG5cblx0ICAgIFx0YWRkVGVtcG9yYXJ5Q2xpZW50KCkge1xuXHQgICAgXHRcdGxldCB2YWxpZCA9IHRoaXMudmFsaWRhdGVJbnB1dHMoKTtcblxuXHQgICAgXHRcdGlmKHZhbGlkKSB7XG5cdCAgICBcdFx0XHQkKCdidXR0b24jdGVtcG9yYXJ5Q2xpZW50Rm9ybVN1Ym1pdEJ0bicpLmF0dHIoJ2Rpc2FibGVkJywgJ2Rpc2FibGVkJyk7XG5cblx0ICAgIFx0XHRcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3Zpc2EvY2xpZW50L2FkZC10ZW1wb3JhcnknKVxuXHRcdFx0XHQgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0ICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0XHQgICAgICAgICAgICBcdHRoaXMucmVzZXRUZW1wb3JhcnlDbGllbnRGb3JtKCk7XG5cblx0XHRcdFx0XHRcdFx0XHR0aGlzLnJlc2V0QXJyYXkoKTtcblxuXHRcdFx0XHRcdFx0XHRcdHRoaXMuc2NyZWVuID0gMTtcblxuXHRcdFx0XHQgICAgICAgICAgICAgICAgJCgnI2FkZFRlbXBvcmFyeUNsaWVudE1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdFx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XG5cblx0XHRcdFx0ICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHQgICAgICAgICAgICAgICAgXHRsb2NhdGlvbi5yZWxvYWQoKTtcblx0XHRcdFx0ICAgICAgICAgICAgICAgIH0sIDMwMDApO1xuXHRcdFx0XHQgICAgICAgICAgICB9XG5cblx0XHRcdFx0ICAgICAgICAgICAgJCgnYnV0dG9uI3RlbXBvcmFyeUNsaWVudEZvcm1TdWJtaXRCdG4nKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuXHRcdFx0XHQgICAgICAgIH0pO1xuXHQgICAgXHRcdH1cblx0XHRcdH0sXG5cblx0XHRcdGFkZENvbnRhY3ROdW1iZXIoKSB7XG5cdFx0XHRcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnMucHVzaCh7Y291bnRyeV9jb2RlOiAnJywgdmFsdWU6ICcnLCBhY3Rpb246ICcnLCBwcm9jZXNzOiAnJywgZmlyc3RfbmFtZTogJycsIGxhc3RfbmFtZTogJycsIHBhc3Nwb3J0OiAnJywgYmlydGhkYXRlOicnLCBnZW5kZXI6ICcnLCBpZkV4aXN0OiBmYWxzZSwgaWZCaW5kZWQ6IGZhbHNlLCBpZkdyb3VwTGVhZGVyOiBmYWxzZX0pO1xuXHRcdFx0fSxcblxuXHRcdFx0cmVtb3ZlQ29udGFjdE51bWJlcihpbmRleCkge1xuXHRcdFx0XHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzLnNwbGljZShpbmRleCwgMSk7XG5cdFx0XHR9XG4gICAgXHR9LFxuXG4gICAgXHRtb3VudGVkKCkge1xuICAgIFx0XHQkKCcjYWRkVGVtcG9yYXJ5Q2xpZW50TW9kYWwnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgKGUpID0+IHtcblx0XHRcdFx0dGhpcy5yZXNldFRlbXBvcmFyeUNsaWVudEZvcm0oKTtcblxuXHRcdFx0XHR0aGlzLnJlc2V0QXJyYXkoKTtcblxuXHRcdFx0XHR0aGlzLnNjcmVlbiA9IDE7XG5cdFx0XHR9KTtcbiAgICBcdH1cbiAgICB9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQWRkVGVtcG9yYXJ5Q2xpZW50LnZ1ZT9hMDUxYmMwOCIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoKTtcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcbmZvcm1bZGF0YS12LTE5MDVhOWU1XSB7XFxuXFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxuLm0tci0xMFtkYXRhLXYtMTkwNWE5ZTVdIHtcXG5cXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJBZGRUZW1wb3JhcnlDbGllbnQudnVlP2EwNTFiYzA4XCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUEyVUE7Q0FDQSxvQkFBQTtDQUNBO0FBQ0E7Q0FDQSxtQkFBQTtDQUNBXCIsXCJmaWxlXCI6XCJBZGRUZW1wb3JhcnlDbGllbnQudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXG5cXHQ8Zm9ybSBpZD1cXFwiYWRkLXRlbXBvcmFyeS1jbGllbnQtZm9ybVxcXCIgY2xhc3M9XFxcImZvcm0taG9yaXpvbnRhbFxcXCIgQHN1Ym1pdC5wcmV2ZW50PVxcXCJhZGRUZW1wb3JhcnlDbGllbnRcXFwiPlxcblxcdFxcdCAgICAgICAgXFxuXFx0XFx0PGRpdiB2LXNob3c9XFxcInNjcmVlbiA9PSAxXFxcIj5cXG5cXHRcXHRcXHQ8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBwdWxsLWxlZnRcXFwiIEBjbGljaz1cXFwiYWRkQ29udGFjdE51bWJlclxcXCI+XFxuXFx0XFx0XFx0XFx0PGkgY2xhc3M9XFxcImZhIGZhLXBsdXNcXFwiPjwvaT4gQWRkIENvbnRhY3QgTnVtYmVyXFxuXFx0XFx0XFx0PC9idXR0b24+XFxuXFxuXFx0XFx0XFx0PGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjIwcHg7IGNsZWFyOmJvdGg7XFxcIj48L2Rpdj5cXG5cXG5cXHRcXHRcXHQ8ZGl2IHYtZm9yPVxcXCIoY29udGFjdF9udW1iZXIsIGluZGV4KSBpbiB0ZW1wb3JhcnlDbGllbnRGb3JtLmNvbnRhY3RfbnVtYmVyc1xcXCIgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImNvbC1zbS0xXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQ8YnV0dG9uIHYtaWY9XFxcImluZGV4ICE9IDBcXFwiIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGFuZ2VyIHB1bGwtcmlnaHRcXFwiIEBjbGljaz1cXFwicmVtb3ZlQ29udGFjdE51bWJlcihpbmRleClcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdDxpIGNsYXNzPVxcXCJmYSBmYS10aW1lc1xcXCI+PC9pPlxcblxcdFxcdFxcdFxcdFxcdDwvYnV0dG9uPlxcblxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdFxcdDxsYWJlbCBmb3I9XFxcImNvbnRhY3RfbnVtYmVyXFxcIiBjbGFzcz1cXFwiY29sLXNtLTMgY29udHJvbC1sYWJlbFxcXCI+KHt7IGluZGV4KzEgfX0pIENvbnRhY3QgTm8uOjwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiY29sLXNtLTNcXFwiPlxcblxcdFxcdFxcdFxcdFxcdDxzZWxlY3QgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwiY291bnRyeV9jb2RlW11cXFwiIHYtbW9kZWw9XFxcImNvbnRhY3RfbnVtYmVyLmNvdW50cnlfY29kZVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PG9wdGlvbiB2YWx1ZT1cXFwiKzYzXFxcIj4rNjM8L29wdGlvbj5cXG5cXHRcXHRcXHRcXHRcXHQ8L3NlbGVjdD5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtc20tNVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0PGlucHV0IHR5cGU9XFxcIm51bWJlclxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwiY29udGFjdF9udW1iZXJbXVxcXCIgdi1tb2RlbD1cXFwiY29udGFjdF9udW1iZXIudmFsdWVcXFwiPlxcblxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcXFwiIEBjbGljaz1cXFwiYWRkVGVtcG9yYXJ5Q2xpZW50VmFsaWRhdGlvblxcXCI+XFxuXFx0XFx0XFx0XFx0Q29udGludWVcXG5cXHRcXHRcXHQ8L2J1dHRvbj5cXG5cXHRcXHQgICAgPGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFx0ICAgIDwvZGl2PlxcblxcblxcdCAgICA8ZGl2IHYtc2hvdz1cXFwic2NyZWVuID09IDJcXFwiPlxcblxcdCAgICBcXHQ8ZGl2IGNsYXNzPVxcXCJwYW5lbCBwYW5lbC1kZWZhdWx0XFxcIiB2LWZvcj1cXFwiKGNvbnRhY3RfbnVtYmVyLCBpbmRleCkgaW4gdGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnNcXFwiPlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwYW5lbC1oZWFkaW5nXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIHt7IGNvbnRhY3RfbnVtYmVyLmNvdW50cnlfY29kZSB9fXt7IGNvbnRhY3RfbnVtYmVyLnZhbHVlIH19XFxuICAgICAgICAgICAgICAgICAgICA8YSB2LWlmPVxcXCJpZkV4aXN0QXJyYXlbaW5kZXhdXFxcIiA6aHJlZj1cXFwiJy92aXNhL2NsaWVudC8nICsgdXNlckFycmF5W2luZGV4XS5pZFxcXCIgY2xhc3M9XFxcInB1bGwtcmlnaHRcXFwiIHRhcmdldD1cXFwiX2JsYW5rXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIFxcdFZpZXcgRXhpc3RpbmcgUHJvZmlsZVxcbiAgICAgICAgICAgICAgICAgICAgPC9hPlxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicGFuZWwtYm9keVxcXCI+XFxuICAgICAgICAgICAgICAgIFxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiPlxcblxcbiAgICAgICAgICAgICAgICBcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXFxcIj5TdGF0dXM6PC9sYWJlbD5cXG5cXHRcXHRcXHRcXHRcXHQgICAgXFx0PGRpdiBjbGFzcz1cXFwiY29sLXNtLTEwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgXFx0XFx0PGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjVweDtjbGVhcjpib3RoO1xcXCI+PC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0PHNwYW4gY2xhc3M9XFxcImxhYmVsXFxcIiBcXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHRcXHQ6Y2xhc3M9XFxcInsnbGFiZWwtcHJpbWFyeSc6IWlmRXhpc3RBcnJheVtpbmRleF0sICdsYWJlbC1kYW5nZXInOmlmRXhpc3RBcnJheVtpbmRleF19XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHRcXHR7eyAoaWZFeGlzdEFycmF5W2luZGV4XSkgPyAnRXhpc3RpbmcnIDogJ05vdCBFeGlzdGluZycgfX1cXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHQ8L3NwYW4+XFxuXFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0PHNwYW4gY2xhc3M9XFxcImxhYmVsXFxcIiBcXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHRcXHQ6Y2xhc3M9XFxcInsnbGFiZWwtcHJpbWFyeSc6IWlmQmluZGVkQXJyYXlbaW5kZXhdLCAnbGFiZWwtZGFuZ2VyJzppZkJpbmRlZEFycmF5W2luZGV4XX1cXFwiXFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0XFx0c3R5bGU9XFxcIm1hcmdpbi1sZWZ0OjEwcHg7XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHRcXHR7eyAoaWZCaW5kZWRBcnJheVtpbmRleF0pID8gJ0JpbmRlZCcgOiAnTm90IEJpbmRlZCcgfX1cXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHQ8L3NwYW4+XFxuXFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0PHNwYW4gY2xhc3M9XFxcImxhYmVsXFxcIiBcXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHRcXHQ6Y2xhc3M9XFxcInsnbGFiZWwtcHJpbWFyeSc6IWlmR3JvdXBMZWFkZXJBcnJheVtpbmRleF0sICdsYWJlbC1kYW5nZXInOmlmR3JvdXBMZWFkZXJBcnJheVtpbmRleF19XFxcIiBzdHlsZT1cXFwibWFyZ2luLWxlZnQ6MTBweDtcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdFxcdFxcdHt7IChpZkdyb3VwTGVhZGVyQXJyYXlbaW5kZXhdKSA/ICdHcm91cCBMZWFkZXInIDogJ05vdCBHcm91cCBMZWFkZXInIH19XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0PC9zcGFuPlxcblxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8L2Rpdj5cXG5cXHRcXHRcXHRcXHRcXHQgIFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdFxcdFxcdCAgXFx0PGRpdiB2LXNob3c9XFxcImlmRXhpc3RBcnJheVtpbmRleF1cXFwiIGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXFxcIj5BY3Rpb246PC9sYWJlbD5cXG5cXHRcXHRcXHRcXHRcXHQgICAgXFx0PGRpdiBjbGFzcz1cXFwiY29sLXNtLTEwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHQ8bGFiZWwgY2xhc3M9XFxcInJhZGlvLWlubGluZVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICBcXHQ8aW5wdXQgdHlwZT1cXFwicmFkaW9cXFwiIDpuYW1lPVxcXCInYWN0aW9uLScraW5kZXhcXFwiIHZhbHVlPVxcXCJwcm9jZWVkX2FueXdheVxcXCIgdi1tb2RlbD1cXFwiY29udGFjdF9udW1iZXIuYWN0aW9uXFxcIiA6ZGlzYWJsZWQ9XFxcImlmQmluZGVkQXJyYXlbaW5kZXhdIHx8IGlmR3JvdXBMZWFkZXJBcnJheVtpbmRleF1cXFwiIC8+IFByb2NlZWQgQW55d2F5XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0PC9sYWJlbD5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8bGFiZWwgY2xhc3M9XFxcInJhZGlvLWlubGluZVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICBcXHQ8aW5wdXQgdHlwZT1cXFwicmFkaW9cXFwiIDpuYW1lPVxcXCInYWN0aW9uLScraW5kZXhcXFwiIHZhbHVlPVxcXCJ1c2VfZXhpc3RpbmdfcHJvZmlsZVxcXCIgdi1tb2RlbD1cXFwiY29udGFjdF9udW1iZXIuYWN0aW9uXFxcIiAvPiBcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgIFxcdFVzZSBFeGlzdGluZyBQcm9maWxlXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0PC9sYWJlbD5cXG5cXHRcXHRcXHRcXHRcXHQgICAgXFx0PC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0ICBcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHRcXHQgIFxcdDxkaXYgdi1zaG93PVxcXCIhaWZFeGlzdEFycmF5W2luZGV4XSB8fCBjb250YWN0X251bWJlci5hY3Rpb24gPT0gJ3Byb2NlZWRfYW55d2F5J1xcXCIgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcXFwiPkFkZCBEZXRhaWxzOjwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1zbS0xMFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJyYWRpby1pbmxpbmVcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDxpbnB1dCB0eXBlPVxcXCJyYWRpb1xcXCIgOm5hbWU9XFxcIidwcm9jZXNzLScraW5kZXhcXFwiIHZhbHVlPVxcXCJjb21wbGV0ZVxcXCIgdi1tb2RlbD1cXFwiY29udGFjdF9udW1iZXIucHJvY2Vzc1xcXCIgLz4gTm9cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDxsYWJlbCBjbGFzcz1cXFwicmFkaW8taW5saW5lXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8aW5wdXQgdHlwZT1cXFwicmFkaW9cXFwiIDpuYW1lPVxcXCIncHJvY2Vzcy0nK2luZGV4XFxcIiB2YWx1ZT1cXFwiZGV0YWlsXFxcIiB2LW1vZGVsPVxcXCJjb250YWN0X251bWJlci5wcm9jZXNzXFxcIiAvPiBZZXNcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHRcXHQgIFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdFxcdFxcdCAgXFx0PGRpdiB2LXNob3c9XFxcImNvbnRhY3RfbnVtYmVyLnByb2Nlc3MgPT0gJ2RldGFpbCdcXFwiIGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgIFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcXFwiPkZpcnN0IE5hbWU8L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtc20tMTBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJmaXJzdF9uYW1lXFxcIiB2LW1vZGVsPVxcXCJjb250YWN0X251bWJlci5maXJzdF9uYW1lXFxcIiBwbGFjZWhvbGRlcj1cXFwiRmlyc3QgTmFtZVxcXCIgYXV0b2NvbXBsZXRlPVxcXCJvZmZcXFwiIC8+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIFxcdDwvZGl2PlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgXFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0ICBcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXFxcIj5MYXN0IE5hbWU8L2xhYmVsPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtc20tMTBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJsYXN0X25hbWVcXFwiIHYtbW9kZWw9XFxcImNvbnRhY3RfbnVtYmVyLmxhc3RfbmFtZVxcXCIgcGxhY2Vob2xkZXI9XFxcIkxhc3QgTmFtZVxcXCIgYXV0b2NvbXBsZXRlPVxcXCJvZmZcXFwiIC8+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIFxcdDwvZGl2PlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgXFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0ICBcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgXFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXFxcIj5QYXNzcG9ydDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIFxcdDxkaXYgY2xhc3M9XFxcImNvbC1zbS0xMFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcInBhc3Nwb3J0XFxcIiB2LW1vZGVsPVxcXCJjb250YWN0X251bWJlci5wYXNzcG9ydFxcXCIgcGxhY2Vob2xkZXI9XFxcIlBhc3Nwb3J0XFxcIiBhdXRvY29tcGxldGU9XFxcIm9mZlxcXCIgLz5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgXFx0PC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICBcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgIFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcXFwiPkJpcnRoZGF0ZTwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIFxcdDxkaXYgY2xhc3M9XFxcImNvbC1zbS0xMFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0PGlucHV0IHR5cGU9XFxcImRhdGVcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImJpcnRoZGF0ZVxcXCIgdi1tb2RlbD1cXFwiY29udGFjdF9udW1iZXIuYmlydGhkYXRlXFxcIiBhdXRvY29tcGxldGU9XFxcIm9mZlxcXCIgLz5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgXFx0PC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICBcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHQgIFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICBcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcXFwiPkdlbmRlcjwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIFxcdDxkaXYgY2xhc3M9XFxcImNvbC1zbS0xMFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0PHNlbGVjdCBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJnZW5kZXJcXFwiIHYtbW9kZWw9XFxcImNvbnRhY3RfbnVtYmVyLmdlbmRlclxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0XFx0PG9wdGlvbiB2YWx1ZT1cXFwiTWFsZVxcXCI+TWFsZTwvb3B0aW9uPlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdFxcdFxcdDxvcHRpb24gdmFsdWU9XFxcIkZlbWFsZVxcXCI+RmVtYWxlPC9vcHRpb24+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0PC9zZWxlY3Q+XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgIFxcdDwvZGl2PlxcblxcdFxcdFxcdFxcdFxcdFxcdCAgXFx0PC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0ICBcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHRcXHQ8L2Zvcm0+XFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgIDwvZGl2PlxcblxcblxcdCAgICBcXHQ8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgaWQ9XFxcInRlbXBvcmFyeUNsaWVudEZvcm1TdWJtaXRCdG5cXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCI+XFxuXFx0XFx0XFx0XFx0U2F2ZVxcblxcdFxcdFxcdDwvYnV0dG9uPlxcblxcdFxcdFxcdDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBAY2xpY2s9XFxcInNjcmVlbiA9IDFcXFwiPkJhY2s8L2J1dHRvbj5cXG5cXHRcXHQgICAgPGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFx0ICAgIDwvZGl2PlxcblxcblxcdDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFxuICAgIGV4cG9ydCBkZWZhdWx0IHtcXG4gICAgXFx0ZGF0YSgpIHtcXG4gICAgXFx0XFx0cmV0dXJuIHtcXG4gICAgXFx0XFx0XFx0dGVtcG9yYXJ5Q2xpZW50Rm9ybTogbmV3IEZvcm0oe1xcblxcdFxcdFxcdFxcdFxcdGNvbnRhY3RfbnVtYmVyczogW3tjb3VudHJ5X2NvZGU6ICcnLCB2YWx1ZTogJycsIGFjdGlvbjogJycsIHByb2Nlc3M6ICcnLCBmaXJzdF9uYW1lOiAnJywgbGFzdF9uYW1lOiAnJywgcGFzc3BvcnQ6ICcnLCBiaXJ0aGRhdGU6JycsIGdlbmRlcjogJycsIGlmRXhpc3Q6IGZhbHNlLCBpZkJpbmRlZDogZmFsc2UsIGlmR3JvdXBMZWFkZXI6IGZhbHNlfV1cXG5cXHRcXHRcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KSxcXG5cXG5cXHRcXHRcXHRcXHRzY3JlZW46IDEsXFxuXFxuXFx0XFx0XFx0XFx0aWZFeGlzdEFycmF5OiBbXSxcXG5cXHRcXHRcXHRcXHRpZkJpbmRlZEFycmF5OiBbXSxcXG5cXHRcXHRcXHRcXHRpZkdyb3VwTGVhZGVyQXJyYXk6IFtdLFxcblxcdFxcdFxcdFxcdHVzZXJBcnJheTogW10sXFxuICAgIFxcdFxcdH1cXG4gICAgXFx0fSxcXG5cXG4gICAgXFx0bWV0aG9kczoge1xcbiAgICBcXHRcXHRyZXNldFRlbXBvcmFyeUNsaWVudEZvcm0oKSB7XFxuXFx0ICAgIFxcdFxcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybSA9IG5ldyBGb3JtKHtcXG5cXHRcXHRcXHRcXHRcXHRjb250YWN0X251bWJlcnM6IFt7Y291bnRyeV9jb2RlOiAnJywgdmFsdWU6ICcnLCBhY3Rpb246ICcnLCBwcm9jZXNzOiAnJywgZmlyc3RfbmFtZTogJycsIGxhc3RfbmFtZTogJycsIHBhc3Nwb3J0OiAnJywgYmlydGhkYXRlOicnLCBnZW5kZXI6ICcnLCBpZkV4aXN0OiBmYWxzZSwgaWZCaW5kZWQ6IGZhbHNlLCBpZkdyb3VwTGVhZGVyOiBmYWxzZX1dXFxuXFx0XFx0XFx0XFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSk7XFxuXFx0ICAgIFxcdH0sXFxuXFxuXFx0ICAgIFxcdHJlc2V0QXJyYXkoKSB7XFxuXFx0ICAgIFxcdFxcdHRoaXMuaWZFeGlzdEFycmF5ID0gW107XFxuXFx0ICAgIFxcdFxcdHRoaXMuaWZCaW5kZWRBcnJheSA9IFtdO1xcblxcdCAgICBcXHRcXHR0aGlzLmlmR3JvdXBMZWFkZXJBcnJheSA9IFtdO1xcblxcdCAgICBcXHRcXHR0aGlzLnVzZXJBcnJheSA9IFtdO1xcblxcdCAgICBcXHR9LFxcblxcblxcdCAgICBcXHR2YWxpZGF0ZUNvbnRhY3ROdW1iZXJzKCkge1xcblxcdCAgICBcXHRcXHRsZXQgaXNWYWxpZCA9IHRydWU7XFxuXFxuXFx0ICAgIFxcdFxcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnMuZm9yRWFjaCgoY29udGFjdE51bWJlciwgaW5kZXgpID0+IHtcXG5cXHQgICAgXFx0XFx0XFx0aWYoY29udGFjdE51bWJlci5jb3VudHJ5X2NvZGUgPT0gJycpIHtcXG5cXHQgICAgXFx0XFx0XFx0XFx0aXNWYWxpZCA9IGZhbHNlO1xcblxcdCAgICBcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ0NvbnRhY3QgTnVtYmVyICcgKyAoaW5kZXgrMSkgKyAnIGNvdW50cnkgY29kZSBpcyBpbnZhbGlkLicpO1xcblxcdCAgICBcXHRcXHRcXHR9XFxuXFxuXFx0ICAgIFxcdFxcdFxcdGlmKGNvbnRhY3ROdW1iZXIudmFsdWUgPT0gJycpIHtcXG5cXHQgICAgXFx0XFx0XFx0XFx0aXNWYWxpZCA9IGZhbHNlO1xcblxcdCAgICBcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ0NvbnRhY3QgTnVtYmVyICcgKyAoaW5kZXgrMSkgKyAnIGlzIGludmFsaWQuJyk7XFxuXFx0ICAgIFxcdFxcdFxcdH1cXG5cXG5cXHQgICAgXFx0XFx0XFx0aWYoY29udGFjdE51bWJlci52YWx1ZS5sZW5ndGggIT0gMTApIHtcXG5cXHQgICAgXFx0XFx0XFx0XFx0aXNWYWxpZCA9IGZhbHNlO1xcblxcdCAgICBcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ0NvbnRhY3QgTnVtYmVyICcgKyAoaW5kZXgrMSkgKyAnIG11c3QgYmUgMTAgZGlnaXQgbnVtYmVyLicpO1xcblxcdCAgICBcXHRcXHRcXHR9XFxuXFx0ICAgIFxcdFxcdH0pO1xcblxcblxcdCAgICBcXHRcXHRyZXR1cm4gaXNWYWxpZDtcXG5cXHQgICAgXFx0fSxcXG5cXG5cXHQgICAgXFx0YWRkVGVtcG9yYXJ5Q2xpZW50VmFsaWRhdGlvbigpIHtcXG5cXHQgICAgXFx0XFx0bGV0IHZhbGlkID0gdGhpcy52YWxpZGF0ZUNvbnRhY3ROdW1iZXJzKCk7XFxuXFxuXFx0ICAgIFxcdFxcdGlmKHZhbGlkKSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy50ZW1wb3JhcnlDbGllbnRGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9jbGllbnQvYWRkLXRlbXBvcmFyeS12YWxpZGF0aW9uJylcXG5cXHRcXHRcXHRcXHQgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuc2NyZWVuID0gMjtcXG5cXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuaWZFeGlzdEFycmF5ID0gcmVzcG9uc2UuaWZFeGlzdEFycmF5O1xcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy5pZkJpbmRlZEFycmF5ID0gcmVzcG9uc2UuaWZCaW5kZWRBcnJheTtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuaWZHcm91cExlYWRlckFycmF5ID0gcmVzcG9uc2UuaWZHcm91cExlYWRlckFycmF5O1xcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy51c2VyQXJyYXkgPSByZXNwb25zZS51c2VyQXJyYXk7XFxuXFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHRcXHRsZXQgY291bnQgPSB0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzLmxlbmd0aDtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdGZvcihsZXQgaT0wOyBpPGNvdW50OyBpKyspIHtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdFxcdGlmKHRoaXMuaWZCaW5kZWRBcnJheVtpXSB8fCB0aGlzLmlmR3JvdXBMZWFkZXJBcnJheVtpXSkge1xcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0XFx0XFx0XFx0dGhpcy50ZW1wb3JhcnlDbGllbnRGb3JtLmNvbnRhY3RfbnVtYmVyc1tpXS5hY3Rpb24gPSAndXNlX2V4aXN0aW5nX3Byb2ZpbGUnO1xcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0XFx0XFx0fSBlbHNlIHtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdFxcdFxcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnNbaV0uYWN0aW9uID0gJ3Byb2NlZWRfYW55d2F5JztcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdFxcdH1cXG5cXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdFxcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnNbaV0ucHJvY2VzcyA9ICdjb21wbGV0ZSc7XFxuXFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzW2ldLmlmRXhpc3QgPSB0aGlzLmlmRXhpc3RBcnJheVtpXTtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdFxcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnNbaV0uaWZCaW5kZWQgPSB0aGlzLmlmQmluZGVkQXJyYXlbaV07XFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzW2ldLmlmR3JvdXBMZWFkZXIgPSB0aGlzLmlmR3JvdXBMZWFkZXJBcnJheVtpXTtcXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdFxcdH1cXG5cXHRcXHRcXHRcXHQgICAgICAgIFxcdH1cXG5cXHRcXHRcXHRcXHQgICAgICAgIH0pO1xcblxcdCAgICBcXHRcXHR9XFxuXFx0ICAgIFxcdH0sXFxuXFxuXFx0ICAgIFxcdHZhbGlkYXRlSW5wdXRzKCkge1xcblxcdCAgICBcXHRcXHRsZXQgaXNWYWxpZCA9IHRydWU7XFxuXFxuXFx0ICAgIFxcdFxcdHRoaXMudGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnMuZm9yRWFjaCgoY29udGFjdE51bWJlciwgaW5kZXgpID0+IHtcXG5cXHQgICAgXFx0XFx0XFx0aWYoY29udGFjdE51bWJlci5hY3Rpb24gPT0gJycpIHtcXG5cXHQgICAgXFx0XFx0XFx0XFx0aXNWYWxpZCA9IGZhbHNlO1xcblxcdCAgICBcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ1RoZSBhY3Rpb24gZmllbGQgaXMgcmVxdWlyZWQuJywgY29udGFjdE51bWJlci5jb3VudHJ5X2NvZGUgKyBjb250YWN0TnVtYmVyLnZhbHVlKTtcXG5cXHQgICAgXFx0XFx0XFx0fVxcblxcblxcdCAgICBcXHRcXHRcXHRpZihjb250YWN0TnVtYmVyLnByb2Nlc3MgPT0gJycgJiYgKGNvbnRhY3ROdW1iZXIuYWN0aW9uID09ICdwcm9jZWVkX2FueXdheScgfHwgIWlmRXhpc3RBcnJheVtpbmRleF0pICkge1xcblxcdCAgICBcXHRcXHRcXHRcXHRpc1ZhbGlkID0gZmFsc2U7XFxuXFx0ICAgIFxcdFxcdFxcdFxcdHRvYXN0ci5lcnJvcignVGhlIGFkZCBkZXRhaWxzIGZpZWxkIGlzIHJlcXVpcmVkLicsIGNvbnRhY3ROdW1iZXIuY291bnRyeV9jb2RlICsgY29udGFjdE51bWJlci52YWx1ZSk7XFxuXFx0ICAgIFxcdFxcdFxcdH1cXG5cXG5cXHQgICAgXFx0XFx0XFx0aWYoY29udGFjdE51bWJlci5maXJzdF9uYW1lID09ICcnICYmIGNvbnRhY3ROdW1iZXIucHJvY2VzcyA9PSAnZGV0YWlsJykge1xcblxcdCAgICBcXHRcXHRcXHRcXHRpc1ZhbGlkID0gZmFsc2U7XFxuXFx0ICAgIFxcdFxcdFxcdFxcdHRvYXN0ci5lcnJvcignVGhlIGZpcnN0IG5hbWUgZmllbGQgaXMgcmVxdWlyZWQuJywgY29udGFjdE51bWJlci5jb3VudHJ5X2NvZGUgKyBjb250YWN0TnVtYmVyLnZhbHVlKTtcXG5cXHQgICAgXFx0XFx0XFx0fVxcblxcblxcdCAgICBcXHRcXHRcXHRpZihjb250YWN0TnVtYmVyLmxhc3RfbmFtZSA9PSAnJyAmJiBjb250YWN0TnVtYmVyLnByb2Nlc3MgPT0gJ2RldGFpbCcpIHtcXG5cXHQgICAgXFx0XFx0XFx0XFx0aXNWYWxpZCA9IGZhbHNlO1xcblxcdCAgICBcXHRcXHRcXHRcXHR0b2FzdHIuZXJyb3IoJ1RoZSBsYXN0IG5hbWUgZmllbGQgaXMgcmVxdWlyZWQuJywgY29udGFjdE51bWJlci5jb3VudHJ5X2NvZGUgKyBjb250YWN0TnVtYmVyLnZhbHVlKTtcXG5cXHQgICAgXFx0XFx0XFx0fVxcblxcblxcdCAgICBcXHRcXHRcXHRpZihjb250YWN0TnVtYmVyLnBhc3Nwb3J0ID09ICcnICYmIGNvbnRhY3ROdW1iZXIucHJvY2VzcyA9PSAnZGV0YWlsJykge1xcblxcdCAgICBcXHRcXHRcXHRcXHRpc1ZhbGlkID0gZmFsc2U7XFxuXFx0ICAgIFxcdFxcdFxcdFxcdHRvYXN0ci5lcnJvcignVGhlIHBhc3Nwb3J0IGZpZWxkIGlzIHJlcXVpcmVkLicsIGNvbnRhY3ROdW1iZXIuY291bnRyeV9jb2RlICsgY29udGFjdE51bWJlci52YWx1ZSk7XFxuXFx0ICAgIFxcdFxcdFxcdH1cXG5cXG5cXHQgICAgXFx0XFx0XFx0aWYoY29udGFjdE51bWJlci5iaXJ0aGRhdGUgPT0gJycgJiYgY29udGFjdE51bWJlci5wcm9jZXNzID09ICdkZXRhaWwnKSB7XFxuXFx0ICAgIFxcdFxcdFxcdFxcdGlzVmFsaWQgPSBmYWxzZTtcXG5cXHQgICAgXFx0XFx0XFx0XFx0dG9hc3RyLmVycm9yKCdUaGUgYmlydGhkYXRlIGZpZWxkIGlzIHJlcXVpcmVkLicsIGNvbnRhY3ROdW1iZXIuY291bnRyeV9jb2RlICsgY29udGFjdE51bWJlci52YWx1ZSk7XFxuXFx0ICAgIFxcdFxcdFxcdH1cXG5cXG5cXHQgICAgXFx0XFx0XFx0aWYoY29udGFjdE51bWJlci5nZW5kZXIgPT0gJycgJiYgY29udGFjdE51bWJlci5wcm9jZXNzID09ICdkZXRhaWwnKSB7XFxuXFx0ICAgIFxcdFxcdFxcdFxcdGlzVmFsaWQgPSBmYWxzZTtcXG5cXHQgICAgXFx0XFx0XFx0XFx0dG9hc3RyLmVycm9yKCdUaGUgZ2VuZGVyIGZpZWxkIGlzIHJlcXVpcmVkLicsIGNvbnRhY3ROdW1iZXIuY291bnRyeV9jb2RlICsgY29udGFjdE51bWJlci52YWx1ZSk7XFxuXFx0ICAgIFxcdFxcdFxcdH1cXG5cXHQgICAgXFx0XFx0fSk7XFxuXFxuXFx0ICAgIFxcdFxcdHJldHVybiBpc1ZhbGlkO1xcblxcdCAgICBcXHR9LFxcblxcblxcdCAgICBcXHRhZGRUZW1wb3JhcnlDbGllbnQoKSB7XFxuXFx0ICAgIFxcdFxcdGxldCB2YWxpZCA9IHRoaXMudmFsaWRhdGVJbnB1dHMoKTtcXG5cXG5cXHQgICAgXFx0XFx0aWYodmFsaWQpIHtcXG5cXHQgICAgXFx0XFx0XFx0JCgnYnV0dG9uI3RlbXBvcmFyeUNsaWVudEZvcm1TdWJtaXRCdG4nKS5hdHRyKCdkaXNhYmxlZCcsICdkaXNhYmxlZCcpO1xcblxcblxcdCAgICBcXHRcXHRcXHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uc3VibWl0KCdwb3N0JywgJy92aXNhL2NsaWVudC9hZGQtdGVtcG9yYXJ5JylcXG5cXHRcXHRcXHRcXHQgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgXFx0dGhpcy5yZXNldFRlbXBvcmFyeUNsaWVudEZvcm0oKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR0aGlzLnJlc2V0QXJyYXkoKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR0aGlzLnNjcmVlbiA9IDE7XFxuXFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgICQoJyNhZGRUZW1wb3JhcnlDbGllbnRNb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xcblxcblxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0bG9jYXRpb24ucmVsb2FkKCk7XFxuXFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIH0sIDMwMDApO1xcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIH1cXG5cXG5cXHRcXHRcXHRcXHQgICAgICAgICAgICAkKCdidXR0b24jdGVtcG9yYXJ5Q2xpZW50Rm9ybVN1Ym1pdEJ0bicpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XFxuXFx0XFx0XFx0XFx0ICAgICAgICB9KTtcXG5cXHQgICAgXFx0XFx0fVxcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0YWRkQ29udGFjdE51bWJlcigpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzLnB1c2goe2NvdW50cnlfY29kZTogJycsIHZhbHVlOiAnJywgYWN0aW9uOiAnJywgcHJvY2VzczogJycsIGZpcnN0X25hbWU6ICcnLCBsYXN0X25hbWU6ICcnLCBwYXNzcG9ydDogJycsIGJpcnRoZGF0ZTonJywgZ2VuZGVyOiAnJywgaWZFeGlzdDogZmFsc2UsIGlmQmluZGVkOiBmYWxzZSwgaWZHcm91cExlYWRlcjogZmFsc2V9KTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdHJlbW92ZUNvbnRhY3ROdW1iZXIoaW5kZXgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzLnNwbGljZShpbmRleCwgMSk7XFxuXFx0XFx0XFx0fVxcbiAgICBcXHR9LFxcblxcbiAgICBcXHRtb3VudGVkKCkge1xcbiAgICBcXHRcXHQkKCcjYWRkVGVtcG9yYXJ5Q2xpZW50TW9kYWwnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgKGUpID0+IHtcXG5cXHRcXHRcXHRcXHR0aGlzLnJlc2V0VGVtcG9yYXJ5Q2xpZW50Rm9ybSgpO1xcblxcblxcdFxcdFxcdFxcdHRoaXMucmVzZXRBcnJheSgpO1xcblxcblxcdFxcdFxcdFxcdHRoaXMuc2NyZWVuID0gMTtcXG5cXHRcXHRcXHR9KTtcXG4gICAgXFx0fVxcbiAgICB9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0xOTA1YTllNVwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZFRlbXBvcmFyeUNsaWVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDI5NlxuLy8gbW9kdWxlIGNodW5rcyA9IDEzIiwiLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuICBNb2RpZmllZCBieSBFdmFuIFlvdSBAeXl4OTkwODAzXG4qL1xuXG52YXIgaGFzRG9jdW1lbnQgPSB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG5cbmlmICh0eXBlb2YgREVCVUcgIT09ICd1bmRlZmluZWQnICYmIERFQlVHKSB7XG4gIGlmICghaGFzRG9jdW1lbnQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgJ3Z1ZS1zdHlsZS1sb2FkZXIgY2Fubm90IGJlIHVzZWQgaW4gYSBub24tYnJvd3NlciBlbnZpcm9ubWVudC4gJyArXG4gICAgXCJVc2UgeyB0YXJnZXQ6ICdub2RlJyB9IGluIHlvdXIgV2VicGFjayBjb25maWcgdG8gaW5kaWNhdGUgYSBzZXJ2ZXItcmVuZGVyaW5nIGVudmlyb25tZW50LlwiXG4gICkgfVxufVxuXG52YXIgbGlzdFRvU3R5bGVzID0gcmVxdWlyZSgnLi9saXN0VG9TdHlsZXMnKVxuXG4vKlxudHlwZSBTdHlsZU9iamVjdCA9IHtcbiAgaWQ6IG51bWJlcjtcbiAgcGFydHM6IEFycmF5PFN0eWxlT2JqZWN0UGFydD5cbn1cblxudHlwZSBTdHlsZU9iamVjdFBhcnQgPSB7XG4gIGNzczogc3RyaW5nO1xuICBtZWRpYTogc3RyaW5nO1xuICBzb3VyY2VNYXA6ID9zdHJpbmdcbn1cbiovXG5cbnZhciBzdHlsZXNJbkRvbSA9IHsvKlxuICBbaWQ6IG51bWJlcl06IHtcbiAgICBpZDogbnVtYmVyLFxuICAgIHJlZnM6IG51bWJlcixcbiAgICBwYXJ0czogQXJyYXk8KG9iaj86IFN0eWxlT2JqZWN0UGFydCkgPT4gdm9pZD5cbiAgfVxuKi99XG5cbnZhciBoZWFkID0gaGFzRG9jdW1lbnQgJiYgKGRvY3VtZW50LmhlYWQgfHwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXSlcbnZhciBzaW5nbGV0b25FbGVtZW50ID0gbnVsbFxudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwXG52YXIgaXNQcm9kdWN0aW9uID0gZmFsc2VcbnZhciBub29wID0gZnVuY3Rpb24gKCkge31cblxuLy8gRm9yY2Ugc2luZ2xlLXRhZyBzb2x1dGlvbiBvbiBJRTYtOSwgd2hpY2ggaGFzIGEgaGFyZCBsaW1pdCBvbiB0aGUgIyBvZiA8c3R5bGU+XG4vLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG52YXIgaXNPbGRJRSA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIC9tc2llIFs2LTldXFxiLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSlcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAocGFyZW50SWQsIGxpc3QsIF9pc1Byb2R1Y3Rpb24pIHtcbiAgaXNQcm9kdWN0aW9uID0gX2lzUHJvZHVjdGlvblxuXG4gIHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIGxpc3QpXG4gIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG4gICAgdmFyIG1heVJlbW92ZSA9IFtdXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgICAgZG9tU3R5bGUucmVmcy0tXG4gICAgICBtYXlSZW1vdmUucHVzaChkb21TdHlsZSlcbiAgICB9XG4gICAgaWYgKG5ld0xpc3QpIHtcbiAgICAgIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhwYXJlbnRJZCwgbmV3TGlzdClcbiAgICAgIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVzID0gW11cbiAgICB9XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXVxuICAgICAgaWYgKGRvbVN0eWxlLnJlZnMgPT09IDApIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKClcbiAgICAgICAgfVxuICAgICAgICBkZWxldGUgc3R5bGVzSW5Eb21bZG9tU3R5bGUuaWRdXG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlc1RvRG9tIChzdHlsZXMgLyogQXJyYXk8U3R5bGVPYmplY3Q+ICovKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBzdHlsZXNbaV1cbiAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgZG9tU3R5bGUucmVmcysrXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pXG4gICAgICB9XG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdKSlcbiAgICAgIH1cbiAgICAgIGlmIChkb21TdHlsZS5wYXJ0cy5sZW5ndGggPiBpdGVtLnBhcnRzLmxlbmd0aCkge1xuICAgICAgICBkb21TdHlsZS5wYXJ0cy5sZW5ndGggPSBpdGVtLnBhcnRzLmxlbmd0aFxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgcGFydHMgPSBbXVxuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIHBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBzdHlsZXNJbkRvbVtpdGVtLmlkXSA9IHsgaWQ6IGl0ZW0uaWQsIHJlZnM6IDEsIHBhcnRzOiBwYXJ0cyB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVN0eWxlRWxlbWVudCAoKSB7XG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpXG4gIHN0eWxlRWxlbWVudC50eXBlID0gJ3RleHQvY3NzJ1xuICBoZWFkLmFwcGVuZENoaWxkKHN0eWxlRWxlbWVudClcbiAgcmV0dXJuIHN0eWxlRWxlbWVudFxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZSAob2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICB2YXIgdXBkYXRlLCByZW1vdmVcbiAgdmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3N0eWxlW2RhdGEtdnVlLXNzci1pZH49XCInICsgb2JqLmlkICsgJ1wiXScpXG5cbiAgaWYgKHN0eWxlRWxlbWVudCkge1xuICAgIGlmIChpc1Byb2R1Y3Rpb24pIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGFuZCBpbiBwcm9kdWN0aW9uIG1vZGUuXG4gICAgICAvLyBzaW1wbHkgZG8gbm90aGluZy5cbiAgICAgIHJldHVybiBub29wXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGJ1dCBpbiBkZXYgbW9kZS5cbiAgICAgIC8vIGZvciBzb21lIHJlYXNvbiBDaHJvbWUgY2FuJ3QgaGFuZGxlIHNvdXJjZSBtYXAgaW4gc2VydmVyLXJlbmRlcmVkXG4gICAgICAvLyBzdHlsZSB0YWdzIC0gc291cmNlIG1hcHMgaW4gPHN0eWxlPiBvbmx5IHdvcmtzIGlmIHRoZSBzdHlsZSB0YWcgaXNcbiAgICAgIC8vIGNyZWF0ZWQgYW5kIGluc2VydGVkIGR5bmFtaWNhbGx5LiBTbyB3ZSByZW1vdmUgdGhlIHNlcnZlciByZW5kZXJlZFxuICAgICAgLy8gc3R5bGVzIGFuZCBpbmplY3QgbmV3IG9uZXMuXG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgaWYgKGlzT2xkSUUpIHtcbiAgICAvLyB1c2Ugc2luZ2xldG9uIG1vZGUgZm9yIElFOS5cbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrK1xuICAgIHN0eWxlRWxlbWVudCA9IHNpbmdsZXRvbkVsZW1lbnQgfHwgKHNpbmdsZXRvbkVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKSlcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCBmYWxzZSlcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCB0cnVlKVxuICB9IGVsc2Uge1xuICAgIC8vIHVzZSBtdWx0aS1zdHlsZS10YWcgbW9kZSBpbiBhbGwgb3RoZXIgY2FzZXNcbiAgICBzdHlsZUVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQpXG4gICAgcmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmopXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmogLyogU3R5bGVPYmplY3RQYXJ0ICovKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcbiAgICAgICAgICBuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJlxuICAgICAgICAgIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKVxuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKVxuICAgIH1cbiAgfVxufVxuXG52YXIgcmVwbGFjZVRleHQgPSAoZnVuY3Rpb24gKCkge1xuICB2YXIgdGV4dFN0b3JlID0gW11cblxuICByZXR1cm4gZnVuY3Rpb24gKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudFxuICAgIHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpXG4gIH1cbn0pKClcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyAoc3R5bGVFbGVtZW50LCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcbiAgdmFyIGNzcyA9IHJlbW92ZSA/ICcnIDogb2JqLmNzc1xuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKVxuICB9IGVsc2Uge1xuICAgIHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKVxuICAgIHZhciBjaGlsZE5vZGVzID0gc3R5bGVFbGVtZW50LmNoaWxkTm9kZXNcbiAgICBpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSlcbiAgICBpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChjc3NOb2RlKVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRvVGFnIChzdHlsZUVsZW1lbnQsIG9iaikge1xuICB2YXIgY3NzID0gb2JqLmNzc1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWFcbiAgdmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXBcblxuICBpZiAobWVkaWEpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKCdtZWRpYScsIG1lZGlhKVxuICB9XG5cbiAgaWYgKHNvdXJjZU1hcCkge1xuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLmNocm9tZS5jb20vZGV2dG9vbHMvZG9jcy9qYXZhc2NyaXB0LWRlYnVnZ2luZ1xuICAgIC8vIHRoaXMgbWFrZXMgc291cmNlIG1hcHMgaW5zaWRlIHN0eWxlIHRhZ3Mgd29yayBwcm9wZXJseSBpbiBDaHJvbWVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VVUkw9JyArIHNvdXJjZU1hcC5zb3VyY2VzWzBdICsgJyAqLydcbiAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNjYwMzg3NVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCwnICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArICcgKi8nXG4gIH1cblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzXG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpXG4gICAgfVxuICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKVxuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0xOTA1YTllNVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZFRlbXBvcmFyeUNsaWVudC52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQWRkVGVtcG9yYXJ5Q2xpZW50LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMTkwNWE5ZTVcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQWRkVGVtcG9yYXJ5Q2xpZW50LnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi0xOTA1YTllNVwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZFRlbXBvcmFyeUNsaWVudC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBBZGRUZW1wb3JhcnlDbGllbnQudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTE5MDVhOWU1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMTkwNWE5ZTVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZFRlbXBvcmFyeUNsaWVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDMzOFxuLy8gbW9kdWxlIGNodW5rcyA9IDEzIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwiYWRkLXRlbXBvcmFyeS1jbGllbnQtZm9ybVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmFkZFRlbXBvcmFyeUNsaWVudCgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDEpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gMVwiXG4gICAgfV1cbiAgfSwgW19jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIHB1bGwtbGVmdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLmFkZENvbnRhY3ROdW1iZXJcbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1wbHVzXCJcbiAgfSksIF92bS5fdihcIiBBZGQgQ29udGFjdCBOdW1iZXJcXG5cXHRcXHRcXHRcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCIyMHB4XCIsXG4gICAgICBcImNsZWFyXCI6IFwiYm90aFwiXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl9sKChfdm0udGVtcG9yYXJ5Q2xpZW50Rm9ybS5jb250YWN0X251bWJlcnMpLCBmdW5jdGlvbihjb250YWN0X251bWJlciwgaW5kZXgpIHtcbiAgICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTFcIlxuICAgIH0sIFsoaW5kZXggIT0gMCkgPyBfYygnYnV0dG9uJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kYW5nZXIgcHVsbC1yaWdodFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS5yZW1vdmVDb250YWN0TnVtYmVyKGluZGV4KVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW19jKCdpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtdGltZXNcIlxuICAgIH0pXSkgOiBfdm0uX2UoKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMyBjb250cm9sLWxhYmVsXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImZvclwiOiBcImNvbnRhY3RfbnVtYmVyXCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiKFwiICsgX3ZtLl9zKGluZGV4ICsgMSkgKyBcIikgQ29udGFjdCBOby46XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tM1wiXG4gICAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGNvbnRhY3RfbnVtYmVyLmNvdW50cnlfY29kZSksXG4gICAgICAgIGV4cHJlc3Npb246IFwiY29udGFjdF9udW1iZXIuY291bnRyeV9jb2RlXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcIm5hbWVcIjogXCJjb3VudHJ5X2NvZGVbXVwiXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgdmFyICQkc2VsZWN0ZWRWYWwgPSBBcnJheS5wcm90b3R5cGUuZmlsdGVyLmNhbGwoJGV2ZW50LnRhcmdldC5vcHRpb25zLCBmdW5jdGlvbihvKSB7XG4gICAgICAgICAgICByZXR1cm4gby5zZWxlY3RlZFxuICAgICAgICAgIH0pLm1hcChmdW5jdGlvbihvKSB7XG4gICAgICAgICAgICB2YXIgdmFsID0gXCJfdmFsdWVcIiBpbiBvID8gby5fdmFsdWUgOiBvLnZhbHVlO1xuICAgICAgICAgICAgcmV0dXJuIHZhbFxuICAgICAgICAgIH0pO1xuICAgICAgICAgIF92bS4kc2V0KGNvbnRhY3RfbnVtYmVyLCBcImNvdW50cnlfY29kZVwiLCAkZXZlbnQudGFyZ2V0Lm11bHRpcGxlID8gJCRzZWxlY3RlZFZhbCA6ICQkc2VsZWN0ZWRWYWxbMF0pXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX2MoJ29wdGlvbicsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidmFsdWVcIjogXCIrNjNcIlxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCIrNjNcIildKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tNVwiXG4gICAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgIHZhbHVlOiAoY29udGFjdF9udW1iZXIudmFsdWUpLFxuICAgICAgICBleHByZXNzaW9uOiBcImNvbnRhY3RfbnVtYmVyLnZhbHVlXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJudW1iZXJcIixcbiAgICAgICAgXCJuYW1lXCI6IFwiY29udGFjdF9udW1iZXJbXVwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiAoY29udGFjdF9udW1iZXIudmFsdWUpXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgICAgX3ZtLiRzZXQoY29udGFjdF9udW1iZXIsIFwidmFsdWVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXSldKVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLmFkZFRlbXBvcmFyeUNsaWVudFZhbGlkYXRpb25cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHRDb250aW51ZVxcblxcdFxcdFxcdFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uc2NyZWVuID09IDIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzY3JlZW4gPT0gMlwiXG4gICAgfV1cbiAgfSwgW192bS5fbCgoX3ZtLnRlbXBvcmFyeUNsaWVudEZvcm0uY29udGFjdF9udW1iZXJzKSwgZnVuY3Rpb24oY29udGFjdF9udW1iZXIsIGluZGV4KSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWhlYWRpbmdcIlxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGNvbnRhY3RfbnVtYmVyLmNvdW50cnlfY29kZSkgKyBfdm0uX3MoY29udGFjdF9udW1iZXIudmFsdWUpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgIFwiKSwgKF92bS5pZkV4aXN0QXJyYXlbaW5kZXhdKSA/IF9jKCdhJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicHVsbC1yaWdodFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvdmlzYS9jbGllbnQvJyArIF92bS51c2VyQXJyYXlbaW5kZXhdLmlkLFxuICAgICAgICBcInRhcmdldFwiOiBcIl9ibGFua1wiXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgXFx0VmlldyBFeGlzdGluZyBQcm9maWxlXFxuICAgICAgICAgICAgICAgICAgICBcIildKSA6IF92bS5fZSgpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1ib2R5XCJcbiAgICB9LCBbX2MoJ2Zvcm0nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gICAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcIlxuICAgIH0sIFtfdm0uX3YoXCJTdGF0dXM6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTBcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJoZWlnaHRcIjogXCI1cHhcIixcbiAgICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgICAgfVxuICAgIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImxhYmVsXCIsXG4gICAgICBjbGFzczoge1xuICAgICAgICAnbGFiZWwtcHJpbWFyeSc6ICFfdm0uaWZFeGlzdEFycmF5W2luZGV4XSwgJ2xhYmVsLWRhbmdlcic6IF92bS5pZkV4aXN0QXJyYXlbaW5kZXhdXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdFxcdFxcdFwiICsgX3ZtLl9zKChfdm0uaWZFeGlzdEFycmF5W2luZGV4XSkgPyAnRXhpc3RpbmcnIDogJ05vdCBFeGlzdGluZycpICsgXCJcXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHRcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJsYWJlbFwiLFxuICAgICAgY2xhc3M6IHtcbiAgICAgICAgJ2xhYmVsLXByaW1hcnknOiAhX3ZtLmlmQmluZGVkQXJyYXlbaW5kZXhdLCAnbGFiZWwtZGFuZ2VyJzogX3ZtLmlmQmluZGVkQXJyYXlbaW5kZXhdXG4gICAgICB9LFxuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJtYXJnaW4tbGVmdFwiOiBcIjEwcHhcIlxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHRcXHQgICAgICBcXHRcXHRcXHRcIiArIF92bS5fcygoX3ZtLmlmQmluZGVkQXJyYXlbaW5kZXhdKSA/ICdCaW5kZWQnIDogJ05vdCBCaW5kZWQnKSArIFwiXFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgXFx0XFx0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGFiZWxcIixcbiAgICAgIGNsYXNzOiB7XG4gICAgICAgICdsYWJlbC1wcmltYXJ5JzogIV92bS5pZkdyb3VwTGVhZGVyQXJyYXlbaW5kZXhdLCAnbGFiZWwtZGFuZ2VyJzogX3ZtLmlmR3JvdXBMZWFkZXJBcnJheVtpbmRleF1cbiAgICAgIH0sXG4gICAgICBzdGF0aWNTdHlsZToge1xuICAgICAgICBcIm1hcmdpbi1sZWZ0XCI6IFwiMTBweFwiXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdFxcdFxcdFwiICsgX3ZtLl9zKChfdm0uaWZHcm91cExlYWRlckFycmF5W2luZGV4XSkgPyAnR3JvdXAgTGVhZGVyJyA6ICdOb3QgR3JvdXAgTGVhZGVyJykgKyBcIlxcblxcdFxcdFxcdFxcdFxcdCAgICAgIFxcdFxcdFwiKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgICB2YWx1ZTogKF92bS5pZkV4aXN0QXJyYXlbaW5kZXhdKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJpZkV4aXN0QXJyYXlbaW5kZXhdXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gICAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0yIGNvbnRyb2wtbGFiZWxcIlxuICAgIH0sIFtfdm0uX3YoXCJBY3Rpb246XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTBcIlxuICAgIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJyYWRpby1pbmxpbmVcIlxuICAgIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGNvbnRhY3RfbnVtYmVyLmFjdGlvbiksXG4gICAgICAgIGV4cHJlc3Npb246IFwiY29udGFjdF9udW1iZXIuYWN0aW9uXCJcbiAgICAgIH1dLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwicmFkaW9cIixcbiAgICAgICAgXCJuYW1lXCI6ICdhY3Rpb24tJyArIGluZGV4LFxuICAgICAgICBcInZhbHVlXCI6IFwicHJvY2VlZF9hbnl3YXlcIixcbiAgICAgICAgXCJkaXNhYmxlZFwiOiBfdm0uaWZCaW5kZWRBcnJheVtpbmRleF0gfHwgX3ZtLmlmR3JvdXBMZWFkZXJBcnJheVtpbmRleF1cbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcImNoZWNrZWRcIjogX3ZtLl9xKGNvbnRhY3RfbnVtYmVyLmFjdGlvbiwgXCJwcm9jZWVkX2FueXdheVwiKVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS4kc2V0KGNvbnRhY3RfbnVtYmVyLCBcImFjdGlvblwiLCBcInByb2NlZWRfYW55d2F5XCIpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KFwiIFByb2NlZWQgQW55d2F5XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInJhZGlvLWlubGluZVwiXG4gICAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgIHZhbHVlOiAoY29udGFjdF9udW1iZXIuYWN0aW9uKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJjb250YWN0X251bWJlci5hY3Rpb25cIlxuICAgICAgfV0sXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJyYWRpb1wiLFxuICAgICAgICBcIm5hbWVcIjogJ2FjdGlvbi0nICsgaW5kZXgsXG4gICAgICAgIFwidmFsdWVcIjogXCJ1c2VfZXhpc3RpbmdfcHJvZmlsZVwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJjaGVja2VkXCI6IF92bS5fcShjb250YWN0X251bWJlci5hY3Rpb24sIFwidXNlX2V4aXN0aW5nX3Byb2ZpbGVcIilcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBfdm0uJHNldChjb250YWN0X251bWJlciwgXCJhY3Rpb25cIiwgXCJ1c2VfZXhpc3RpbmdfcHJvZmlsZVwiKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSksIF92bS5fdihcIiBcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgIFxcdFVzZSBFeGlzdGluZyBQcm9maWxlXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgIHZhbHVlOiAoIV92bS5pZkV4aXN0QXJyYXlbaW5kZXhdIHx8IGNvbnRhY3RfbnVtYmVyLmFjdGlvbiA9PSAncHJvY2VlZF9hbnl3YXknKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCIhaWZFeGlzdEFycmF5W2luZGV4XSB8fCBjb250YWN0X251bWJlci5hY3Rpb24gPT0gJ3Byb2NlZWRfYW55d2F5J1wiXG4gICAgICB9XSxcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICAgIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXCJcbiAgICB9LCBbX3ZtLl92KFwiQWRkIERldGFpbHM6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTBcIlxuICAgIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJyYWRpby1pbmxpbmVcIlxuICAgIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGNvbnRhY3RfbnVtYmVyLnByb2Nlc3MpLFxuICAgICAgICBleHByZXNzaW9uOiBcImNvbnRhY3RfbnVtYmVyLnByb2Nlc3NcIlxuICAgICAgfV0sXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJyYWRpb1wiLFxuICAgICAgICBcIm5hbWVcIjogJ3Byb2Nlc3MtJyArIGluZGV4LFxuICAgICAgICBcInZhbHVlXCI6IFwiY29tcGxldGVcIlxuICAgICAgfSxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwiY2hlY2tlZFwiOiBfdm0uX3EoY29udGFjdF9udW1iZXIucHJvY2VzcywgXCJjb21wbGV0ZVwiKVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS4kc2V0KGNvbnRhY3RfbnVtYmVyLCBcInByb2Nlc3NcIiwgXCJjb21wbGV0ZVwiKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSksIF92bS5fdihcIiBOb1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJyYWRpby1pbmxpbmVcIlxuICAgIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGNvbnRhY3RfbnVtYmVyLnByb2Nlc3MpLFxuICAgICAgICBleHByZXNzaW9uOiBcImNvbnRhY3RfbnVtYmVyLnByb2Nlc3NcIlxuICAgICAgfV0sXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJyYWRpb1wiLFxuICAgICAgICBcIm5hbWVcIjogJ3Byb2Nlc3MtJyArIGluZGV4LFxuICAgICAgICBcInZhbHVlXCI6IFwiZGV0YWlsXCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcImNoZWNrZWRcIjogX3ZtLl9xKGNvbnRhY3RfbnVtYmVyLnByb2Nlc3MsIFwiZGV0YWlsXCIpXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLiRzZXQoY29udGFjdF9udW1iZXIsIFwicHJvY2Vzc1wiLCBcImRldGFpbFwiKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSksIF92bS5fdihcIiBZZXNcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcIildKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgdmFsdWU6IChjb250YWN0X251bWJlci5wcm9jZXNzID09ICdkZXRhaWwnKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJjb250YWN0X251bWJlci5wcm9jZXNzID09ICdkZXRhaWwnXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgICB9LCBbX2MoJ2xhYmVsJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiXG4gICAgfSwgW192bS5fdihcIkZpcnN0IE5hbWVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xMFwiXG4gICAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgIHZhbHVlOiAoY29udGFjdF9udW1iZXIuZmlyc3RfbmFtZSksXG4gICAgICAgIGV4cHJlc3Npb246IFwiY29udGFjdF9udW1iZXIuZmlyc3RfbmFtZVwiXG4gICAgICB9XSxcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgICBcIm5hbWVcIjogXCJmaXJzdF9uYW1lXCIsXG4gICAgICAgIFwicGxhY2Vob2xkZXJcIjogXCJGaXJzdCBOYW1lXCIsXG4gICAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IChjb250YWN0X251bWJlci5maXJzdF9uYW1lKVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICAgIF92bS4kc2V0KGNvbnRhY3RfbnVtYmVyLCBcImZpcnN0X25hbWVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICAgIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXCJcbiAgICB9LCBbX3ZtLl92KFwiTGFzdCBOYW1lXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTBcIlxuICAgIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGNvbnRhY3RfbnVtYmVyLmxhc3RfbmFtZSksXG4gICAgICAgIGV4cHJlc3Npb246IFwiY29udGFjdF9udW1iZXIubGFzdF9uYW1lXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICAgIFwibmFtZVwiOiBcImxhc3RfbmFtZVwiLFxuICAgICAgICBcInBsYWNlaG9sZGVyXCI6IFwiTGFzdCBOYW1lXCIsXG4gICAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IChjb250YWN0X251bWJlci5sYXN0X25hbWUpXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgICAgX3ZtLiRzZXQoY29udGFjdF9udW1iZXIsIFwibGFzdF9uYW1lXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgICB9LCBbX2MoJ2xhYmVsJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiXG4gICAgfSwgW192bS5fdihcIlBhc3Nwb3J0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTBcIlxuICAgIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGNvbnRhY3RfbnVtYmVyLnBhc3Nwb3J0KSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJjb250YWN0X251bWJlci5wYXNzcG9ydFwiXG4gICAgICB9XSxcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgICBcIm5hbWVcIjogXCJwYXNzcG9ydFwiLFxuICAgICAgICBcInBsYWNlaG9sZGVyXCI6IFwiUGFzc3BvcnRcIixcbiAgICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgICAgfSxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogKGNvbnRhY3RfbnVtYmVyLnBhc3Nwb3J0KVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICAgIF92bS4kc2V0KGNvbnRhY3RfbnVtYmVyLCBcInBhc3Nwb3J0XCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgICB9LCBbX2MoJ2xhYmVsJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgY29udHJvbC1sYWJlbFwiXG4gICAgfSwgW192bS5fdihcIkJpcnRoZGF0ZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEwXCJcbiAgICB9LCBbX2MoJ2lucHV0Jywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgdmFsdWU6IChjb250YWN0X251bWJlci5iaXJ0aGRhdGUpLFxuICAgICAgICBleHByZXNzaW9uOiBcImNvbnRhY3RfbnVtYmVyLmJpcnRoZGF0ZVwiXG4gICAgICB9XSxcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwiZGF0ZVwiLFxuICAgICAgICBcIm5hbWVcIjogXCJiaXJ0aGRhdGVcIixcbiAgICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgICAgfSxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogKGNvbnRhY3RfbnVtYmVyLmJpcnRoZGF0ZSlcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgICBfdm0uJHNldChjb250YWN0X251bWJlciwgXCJiaXJ0aGRhdGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICAgIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiBjb250cm9sLWxhYmVsXCJcbiAgICB9LCBbX3ZtLl92KFwiR2VuZGVyXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTBcIlxuICAgIH0sIFtfYygnc2VsZWN0Jywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgdmFsdWU6IChjb250YWN0X251bWJlci5nZW5kZXIpLFxuICAgICAgICBleHByZXNzaW9uOiBcImNvbnRhY3RfbnVtYmVyLmdlbmRlclwiXG4gICAgICB9XSxcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJuYW1lXCI6IFwiZ2VuZGVyXCJcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICB2YXIgJCRzZWxlY3RlZFZhbCA9IEFycmF5LnByb3RvdHlwZS5maWx0ZXIuY2FsbCgkZXZlbnQudGFyZ2V0Lm9wdGlvbnMsIGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICAgIHJldHVybiBvLnNlbGVjdGVkXG4gICAgICAgICAgfSkubWFwKGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICAgIHZhciB2YWwgPSBcIl92YWx1ZVwiIGluIG8gPyBvLl92YWx1ZSA6IG8udmFsdWU7XG4gICAgICAgICAgICByZXR1cm4gdmFsXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgX3ZtLiRzZXQoY29udGFjdF9udW1iZXIsIFwiZ2VuZGVyXCIsICRldmVudC50YXJnZXQubXVsdGlwbGUgPyAkJHNlbGVjdGVkVmFsIDogJCRzZWxlY3RlZFZhbFswXSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIFtfYygnb3B0aW9uJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBcIk1hbGVcIlxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJNYWxlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdvcHRpb24nLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcInZhbHVlXCI6IFwiRmVtYWxlXCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiRmVtYWxlXCIpXSldKV0pXSldKV0pXSldKVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJzdWJtaXRcIixcbiAgICAgIFwiaWRcIjogXCJ0ZW1wb3JhcnlDbGllbnRGb3JtU3VibWl0QnRuXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHRTYXZlXFxuXFx0XFx0XFx0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnNjcmVlbiA9IDFcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJCYWNrXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0sIDIpXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMTkwNWE5ZTVcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0xOTA1YTllNVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZFRlbXBvcmFyeUNsaWVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDM4MVxuLy8gbW9kdWxlIGNodW5rcyA9IDEzIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0xOTA1YTllNVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZFRlbXBvcmFyeUNsaWVudC52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjI2MzgzZWMzXCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0xOTA1YTllNVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZFRlbXBvcmFyeUNsaWVudC52dWVcIiwgZnVuY3Rpb24oKSB7XG4gICAgIHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTE5MDVhOWU1XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vQWRkVGVtcG9yYXJ5Q2xpZW50LnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTE5MDVhOWU1XCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkVGVtcG9yYXJ5Q2xpZW50LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDM0XG4vLyBtb2R1bGUgY2h1bmtzID0gMTMiLCIvKipcbiAqIFRyYW5zbGF0ZXMgdGhlIGxpc3QgZm9ybWF0IHByb2R1Y2VkIGJ5IGNzcy1sb2FkZXIgaW50byBzb21ldGhpbmdcbiAqIGVhc2llciB0byBtYW5pcHVsYXRlLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGxpc3RUb1N0eWxlcyAocGFyZW50SWQsIGxpc3QpIHtcbiAgdmFyIHN0eWxlcyA9IFtdXG4gIHZhciBuZXdTdHlsZXMgPSB7fVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgaXRlbSA9IGxpc3RbaV1cbiAgICB2YXIgaWQgPSBpdGVtWzBdXG4gICAgdmFyIGNzcyA9IGl0ZW1bMV1cbiAgICB2YXIgbWVkaWEgPSBpdGVtWzJdXG4gICAgdmFyIHNvdXJjZU1hcCA9IGl0ZW1bM11cbiAgICB2YXIgcGFydCA9IHtcbiAgICAgIGlkOiBwYXJlbnRJZCArICc6JyArIGksXG4gICAgICBjc3M6IGNzcyxcbiAgICAgIG1lZGlhOiBtZWRpYSxcbiAgICAgIHNvdXJjZU1hcDogc291cmNlTWFwXG4gICAgfVxuICAgIGlmICghbmV3U3R5bGVzW2lkXSkge1xuICAgICAgc3R5bGVzLnB1c2gobmV3U3R5bGVzW2lkXSA9IHsgaWQ6IGlkLCBwYXJ0czogW3BhcnRdIH0pXG4gICAgfSBlbHNlIHtcbiAgICAgIG5ld1N0eWxlc1tpZF0ucGFydHMucHVzaChwYXJ0KVxuICAgIH1cbiAgfVxuICByZXR1cm4gc3R5bGVzXG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvbGlzdFRvU3R5bGVzLmpzXG4vLyBtb2R1bGUgaWQgPSA3XG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiXSwic291cmNlUm9vdCI6IiJ9