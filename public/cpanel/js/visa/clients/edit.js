/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 476);
/******/ })
/************************************************************************/
/******/ ({

/***/ 186:
/***/ (function(module, exports) {

var data = window.Laravel.data;

var Clients = new Vue({
	el: '#client',
	data: {
		clients: [],
		groups: [],
		clientForm: new Form({
			id: data.id,
			avatar: data.avatar,
			first_name: data.first_name,
			middle_name: data.middle_name,
			last_name: data.last_name,
			birth_date: data.birth_date,
			gender: data.gender,
			civil_status: data.civil_status,
			height: data.height,
			weight: data.weight,
			address: data.address,
			email: data.email,
			passport: data.passport,
			group_id: data.group_id
		}, { baseURL: 'http://' + window.Laravel.cpanel_url })
	},

	directives: {
		datepicker: {
			bind: function bind(el, binding, vnode) {
				$(el).datepicker({
					format: 'yyyy-mm-dd'
				}).on('changeDate', function (e) {
					Clients.$set(Clients.clientForm, 'birth_date', e.format('yyyy-mm-dd'));
				});
			}
		}
	},
	methods: {
		changeUrl: function changeUrl() {
			var cpanelUrl = window.Laravel.cpanel_url;
			var clientId = $('#client-id').val();
			if ($('#email').val() == '') {
				$('#form-client').attr('action', 'http://' + cpanelUrl + '/visa/client/update/' + clientId);
			} else {
				$('#form-client').attr('action', 'http://' + cpanelUrl + '/visa/client/updatewithemail/' + clientId);
			}
		}
	},
	created: function created() {
		var _this = this;

		axios.get('/visa/client/showGroup').then(function (result) {
			_this.groups = result.data;

			setTimeout(function () {
				$(".select2").select2({
					placeholder: "Select group",
					allowClear: true
				});
				$(".select2-selection").css('height', '34px');
			}, 500);
		});

		var cpanelUrl = window.Laravel.cpanel_url;
		var clientId = $('#client-id').val();
		if (data.email == null) {
			$('#form-client').attr('action', 'http://' + cpanelUrl + '/visa/client/update/' + clientId);
		} else {
			$('#form-client').attr('action', 'http://' + cpanelUrl + '/visa/client/updatewithemail/' + clientId);
		}

		Event.listen('Imgfileupload.avatarUpload', function (img) {
			_this.clientForm.avatar = img;
		});
	},
	mounted: function mounted() {
		$('select#group_id').on("change", function () {
			var value = $("select#group_id option:selected").val();
			this.clientForm.group_id = value;
		}.bind(this));
	}
});

$(document).ready(function () {

	$('.btnUpload').click(function () {
		$('.avatar-uploader input').click();
	});

	$('#first_name, #middle_name, #last_name').keypress(function (event) {
		var inputValue = event.charCode;
		//alert(inputValue);
		if (!(inputValue > 64 && inputValue < 91 || inputValue > 96 && inputValue < 123 || inputValue == 32 || inputValue == 0)) {
			event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});

	$('form#form-client').on('submit', function (e) {
		var firstName = $('input#first_name').val();
		var lastName = $('input#last_name').val();
		var gender = $('select[name=gender]').val();
		var birthdate = $('input#birth_date').val();
		var contactNumber = $('input#contact_number').val();
		var passport = $('input#passport').val();

		$.ajax({
			url: '/visa/client/validationBeforeSubmit',
			type: 'get',
			data: {
				id: $('#client-id').val(),
				firstName: firstName,
				lastName: lastName,
				gender: gender,
				birthdate: birthdate,
				contactNumber: contactNumber,
				passport: passport

			},
			async: false,
			success: function success(data) {
				var data = JSON.parse(data);

				Clients.clients = data.users;

				if (data.binded) {
					toastr.error('Contact number is already binded in the visa app.');
					e.preventDefault();
				} else if (data.duplicateEntry) {
					$('#duplicateClientsListModal').modal('show');
					e.preventDefault();
				}
			}
		});
	});
});

/***/ }),

/***/ 476:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(186);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvZWRpdC5qcyJdLCJuYW1lcyI6WyJkYXRhIiwid2luZG93IiwiTGFyYXZlbCIsIkNsaWVudHMiLCJWdWUiLCJlbCIsImNsaWVudHMiLCJncm91cHMiLCJjbGllbnRGb3JtIiwiRm9ybSIsImlkIiwiYXZhdGFyIiwiZmlyc3RfbmFtZSIsIm1pZGRsZV9uYW1lIiwibGFzdF9uYW1lIiwiYmlydGhfZGF0ZSIsImdlbmRlciIsImNpdmlsX3N0YXR1cyIsImhlaWdodCIsIndlaWdodCIsImFkZHJlc3MiLCJlbWFpbCIsInBhc3Nwb3J0IiwiZ3JvdXBfaWQiLCJiYXNlVVJMIiwiY3BhbmVsX3VybCIsImRpcmVjdGl2ZXMiLCJkYXRlcGlja2VyIiwiYmluZCIsImJpbmRpbmciLCJ2bm9kZSIsIiQiLCJmb3JtYXQiLCJvbiIsImUiLCIkc2V0IiwibWV0aG9kcyIsImNoYW5nZVVybCIsImNwYW5lbFVybCIsImNsaWVudElkIiwidmFsIiwiYXR0ciIsImNyZWF0ZWQiLCJheGlvcyIsImdldCIsInRoZW4iLCJyZXN1bHQiLCJzZXRUaW1lb3V0Iiwic2VsZWN0MiIsInBsYWNlaG9sZGVyIiwiYWxsb3dDbGVhciIsImNzcyIsIkV2ZW50IiwibGlzdGVuIiwiaW1nIiwibW91bnRlZCIsInZhbHVlIiwiZG9jdW1lbnQiLCJyZWFkeSIsImNsaWNrIiwia2V5cHJlc3MiLCJldmVudCIsImlucHV0VmFsdWUiLCJjaGFyQ29kZSIsInByZXZlbnREZWZhdWx0Iiwid2hpY2giLCJmaXJzdE5hbWUiLCJsYXN0TmFtZSIsImJpcnRoZGF0ZSIsImNvbnRhY3ROdW1iZXIiLCJhamF4IiwidXJsIiwidHlwZSIsImFzeW5jIiwic3VjY2VzcyIsIkpTT04iLCJwYXJzZSIsInVzZXJzIiwiYmluZGVkIiwidG9hc3RyIiwiZXJyb3IiLCJkdXBsaWNhdGVFbnRyeSIsIm1vZGFsIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBLElBQUlBLE9BQU9DLE9BQU9DLE9BQVAsQ0FBZUYsSUFBMUI7O0FBRUEsSUFBSUcsVUFBVSxJQUFJQyxHQUFKLENBQVE7QUFDckJDLEtBQUcsU0FEa0I7QUFFckJMLE9BQUs7QUFDSk0sV0FBUyxFQURMO0FBRUpDLFVBQU8sRUFGSDtBQUdEQyxjQUFXLElBQUlDLElBQUosQ0FBUztBQUNsQkMsT0FBSVYsS0FBS1UsRUFEUztBQUVsQkMsV0FBUVgsS0FBS1csTUFGSztBQUdsQkMsZUFBWVosS0FBS1ksVUFIQztBQUlsQkMsZ0JBQWFiLEtBQUthLFdBSkE7QUFLbEJDLGNBQVdkLEtBQUtjLFNBTEU7QUFNbEJDLGVBQVlmLEtBQUtlLFVBTkM7QUFPbEJDLFdBQVFoQixLQUFLZ0IsTUFQSztBQVFsQkMsaUJBQWNqQixLQUFLaUIsWUFSRDtBQVNsQkMsV0FBUWxCLEtBQUtrQixNQVRLO0FBVWxCQyxXQUFRbkIsS0FBS21CLE1BVks7QUFXbEJDLFlBQVNwQixLQUFLb0IsT0FYSTtBQVlsQkMsVUFBT3JCLEtBQUtxQixLQVpNO0FBYWxCQyxhQUFVdEIsS0FBS3NCLFFBYkc7QUFjbEJDLGFBQVV2QixLQUFLdUI7QUFkRyxHQUFULEVBZVIsRUFBRUMsU0FBUyxZQUFVdkIsT0FBT0MsT0FBUCxDQUFldUIsVUFBcEMsRUFmUTtBQUhWLEVBRmdCOztBQXVCckJDLGFBQVk7QUFDWEMsY0FBWTtBQUNWQyxPQURVLGdCQUNMdkIsRUFESyxFQUNEd0IsT0FEQyxFQUNRQyxLQURSLEVBQ2U7QUFDdkJDLE1BQUUxQixFQUFGLEVBQU1zQixVQUFOLENBQWlCO0FBQ2ZLLGFBQVE7QUFETyxLQUFqQixFQUVHQyxFQUZILENBRU0sWUFGTixFQUVvQixVQUFDQyxDQUFELEVBQU87QUFDekIvQixhQUFRZ0MsSUFBUixDQUFhaEMsUUFBUUssVUFBckIsRUFBaUMsWUFBakMsRUFBK0MwQixFQUFFRixNQUFGLENBQVMsWUFBVCxDQUEvQztBQUNELEtBSkQ7QUFLRDtBQVBTO0FBREQsRUF2QlM7QUFrQ3JCSSxVQUFTO0FBQ1JDLFdBRFEsdUJBQ0c7QUFDVixPQUFJQyxZQUFZckMsT0FBT0MsT0FBUCxDQUFldUIsVUFBL0I7QUFDQSxPQUFJYyxXQUFXUixFQUFFLFlBQUYsRUFBZ0JTLEdBQWhCLEVBQWY7QUFDQSxPQUFHVCxFQUFFLFFBQUYsRUFBWVMsR0FBWixNQUFtQixFQUF0QixFQUF5QjtBQUNyQlQsTUFBRSxjQUFGLEVBQWtCVSxJQUFsQixDQUF1QixRQUF2QixFQUFnQyxZQUFVSCxTQUFWLEdBQW9CLHNCQUFwQixHQUEyQ0MsUUFBM0U7QUFDSCxJQUZELE1BRU87QUFDSFIsTUFBRSxjQUFGLEVBQWtCVSxJQUFsQixDQUF1QixRQUF2QixFQUFnQyxZQUFVSCxTQUFWLEdBQW9CLCtCQUFwQixHQUFvREMsUUFBcEY7QUFDSDtBQUNEO0FBVE8sRUFsQ1k7QUE2Q3JCRyxRQTdDcUIscUJBNkNaO0FBQUE7O0FBQ1JDLFFBQU1DLEdBQU4sQ0FBVSx3QkFBVixFQUNPQyxJQURQLENBQ1ksa0JBQVU7QUFDWixTQUFLdEMsTUFBTCxHQUFjdUMsT0FBTzlDLElBQXJCOztBQUVBK0MsY0FBVyxZQUFVO0FBQ3BCaEIsTUFBRSxVQUFGLEVBQWNpQixPQUFkLENBQXNCO0FBQ2xCQyxrQkFBYSxjQURLO0FBRWxCQyxpQkFBWTtBQUZNLEtBQXRCO0FBSUFuQixNQUFFLG9CQUFGLEVBQXdCb0IsR0FBeEIsQ0FBNEIsUUFBNUIsRUFBcUMsTUFBckM7QUFDSCxJQU5FLEVBTUEsR0FOQTtBQU9OLEdBWEo7O0FBYUcsTUFBSWIsWUFBWXJDLE9BQU9DLE9BQVAsQ0FBZXVCLFVBQS9CO0FBQ0gsTUFBSWMsV0FBV1IsRUFBRSxZQUFGLEVBQWdCUyxHQUFoQixFQUFmO0FBQ0EsTUFBR3hDLEtBQUtxQixLQUFMLElBQVksSUFBZixFQUFvQjtBQUNoQlUsS0FBRSxjQUFGLEVBQWtCVSxJQUFsQixDQUF1QixRQUF2QixFQUFnQyxZQUFVSCxTQUFWLEdBQW9CLHNCQUFwQixHQUEyQ0MsUUFBM0U7QUFDSCxHQUZELE1BRU87QUFDSFIsS0FBRSxjQUFGLEVBQWtCVSxJQUFsQixDQUF1QixRQUF2QixFQUFnQyxZQUFVSCxTQUFWLEdBQW9CLCtCQUFwQixHQUFvREMsUUFBcEY7QUFDSDs7QUFFSWEsUUFBTUMsTUFBTixDQUFhLDRCQUFiLEVBQTJDLFVBQUNDLEdBQUQsRUFBUztBQUMzQyxTQUFLOUMsVUFBTCxDQUFnQkcsTUFBaEIsR0FBeUIyQyxHQUF6QjtBQUVQLEdBSEY7QUFLTCxFQXhFb0I7QUF5RXJCQyxRQXpFcUIscUJBeUVYO0FBQ1R4QixJQUFFLGlCQUFGLEVBQXFCRSxFQUFyQixDQUF3QixRQUF4QixFQUFrQyxZQUFXO0FBQ3pDLE9BQUl1QixRQUFRekIsRUFBRSxpQ0FBRixFQUFxQ1MsR0FBckMsRUFBWjtBQUNBLFFBQUtoQyxVQUFMLENBQWdCZSxRQUFoQixHQUEyQmlDLEtBQTNCO0FBQ0gsR0FIaUMsQ0FHaEM1QixJQUhnQyxDQUczQixJQUgyQixDQUFsQztBQUlBO0FBOUVvQixDQUFSLENBQWQ7O0FBaUZBRyxFQUFFMEIsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVc7O0FBRXpCM0IsR0FBRSxZQUFGLEVBQWdCNEIsS0FBaEIsQ0FBc0IsWUFBVTtBQUM1QjVCLElBQUUsd0JBQUYsRUFBNEI0QixLQUE1QjtBQUNILEVBRkQ7O0FBSUg1QixHQUFFLHVDQUFGLEVBQTJDNkIsUUFBM0MsQ0FBb0QsVUFBU0MsS0FBVCxFQUFlO0FBQ2xFLE1BQUlDLGFBQWFELE1BQU1FLFFBQXZCO0FBQ0E7QUFDQSxNQUFHLEVBQUdELGFBQWEsRUFBYixJQUFtQkEsYUFBYSxFQUFqQyxJQUF5Q0EsYUFBYSxFQUFiLElBQW1CQSxhQUFhLEdBQXpFLElBQWdGQSxjQUFZLEVBQTVGLElBQW9HQSxjQUFZLENBQWxILENBQUgsRUFBeUg7QUFDekhELFNBQU1HLGNBQU47QUFDQztBQUNELEVBTkQ7O0FBUUFqQyxHQUFFLHVEQUFGLEVBQTJENkIsUUFBM0QsQ0FBb0UsVUFBVTFCLENBQVYsRUFBYTtBQUNoRjtBQUNHLE1BQUlBLEVBQUUrQixLQUFGLElBQVcsQ0FBWCxJQUFnQi9CLEVBQUUrQixLQUFGLElBQVcsQ0FBM0IsS0FBaUMvQixFQUFFK0IsS0FBRixHQUFVLEVBQVYsSUFBZ0IvQixFQUFFK0IsS0FBRixHQUFVLEVBQTNELENBQUosRUFBb0U7QUFDaEUsVUFBTyxLQUFQO0FBQ0g7QUFDSixFQUxEOztBQU9BbEMsR0FBRSxrQkFBRixFQUFzQkUsRUFBdEIsQ0FBeUIsUUFBekIsRUFBbUMsVUFBU0MsQ0FBVCxFQUFZO0FBQzlDLE1BQUlnQyxZQUFZbkMsRUFBRSxrQkFBRixFQUFzQlMsR0FBdEIsRUFBaEI7QUFDQSxNQUFJMkIsV0FBV3BDLEVBQUUsaUJBQUYsRUFBcUJTLEdBQXJCLEVBQWY7QUFDQSxNQUFJeEIsU0FBU2UsRUFBRSxxQkFBRixFQUF5QlMsR0FBekIsRUFBYjtBQUNBLE1BQUk0QixZQUFZckMsRUFBRSxrQkFBRixFQUFzQlMsR0FBdEIsRUFBaEI7QUFDQSxNQUFJNkIsZ0JBQWdCdEMsRUFBRSxzQkFBRixFQUEwQlMsR0FBMUIsRUFBcEI7QUFDQSxNQUFJbEIsV0FBV1MsRUFBRSxnQkFBRixFQUFvQlMsR0FBcEIsRUFBZjs7QUFFQVQsSUFBRXVDLElBQUYsQ0FBTztBQUNOQyxRQUFLLHFDQURDO0FBRUdDLFNBQU0sS0FGVDtBQUdHeEUsU0FBTTtBQUNMVSxRQUFJcUIsRUFBRSxZQUFGLEVBQWdCUyxHQUFoQixFQURDO0FBRUwwQixlQUFVQSxTQUZMO0FBR2RDLGNBQVVBLFFBSEk7QUFJZG5ELFlBQVFBLE1BSk07QUFLZG9ELGVBQVdBLFNBTEc7QUFNZEMsbUJBQWVBLGFBTkQ7QUFPZC9DLGNBQVVBOztBQVBJLElBSFQ7QUFhR21ELFVBQU8sS0FiVjtBQWNHQyxZQUFTLGlCQUFTMUUsSUFBVCxFQUFlO0FBQ3ZCLFFBQUlBLE9BQU8yRSxLQUFLQyxLQUFMLENBQVc1RSxJQUFYLENBQVg7O0FBRUFHLFlBQVFHLE9BQVIsR0FBa0JOLEtBQUs2RSxLQUF2Qjs7QUFFRyxRQUFHN0UsS0FBSzhFLE1BQVIsRUFBZ0I7QUFDZkMsWUFBT0MsS0FBUCxDQUFhLG1EQUFiO0FBQ0E5QyxPQUFFOEIsY0FBRjtBQUNBLEtBSEQsTUFHTyxJQUFHaEUsS0FBS2lGLGNBQVIsRUFBd0I7QUFDM0JsRCxPQUFFLDRCQUFGLEVBQWdDbUQsS0FBaEMsQ0FBc0MsTUFBdEM7QUFDQWhELE9BQUU4QixjQUFGO0FBQ0g7QUFDSjtBQTFCSixHQUFQO0FBNEJBLEVBcENEO0FBc0NBLENBM0RELEUiLCJmaWxlIjoianMvdmlzYS9jbGllbnRzL2VkaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ3Nik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjMiLCJsZXQgZGF0YSA9IHdpbmRvdy5MYXJhdmVsLmRhdGE7XG5cbnZhciBDbGllbnRzID0gbmV3IFZ1ZSh7XG5cdGVsOicjY2xpZW50Jyxcblx0ZGF0YTp7XG5cdFx0Y2xpZW50czogW10sXG5cdFx0Z3JvdXBzOltdLFxuXHQgICAgY2xpZW50Rm9ybTpuZXcgRm9ybSh7XG5cdCAgICAgIGlkOiBkYXRhLmlkLFxuXHQgICAgICBhdmF0YXI6IGRhdGEuYXZhdGFyLFxuXHQgICAgICBmaXJzdF9uYW1lOiBkYXRhLmZpcnN0X25hbWUsXG5cdCAgICAgIG1pZGRsZV9uYW1lOiBkYXRhLm1pZGRsZV9uYW1lLFxuXHQgICAgICBsYXN0X25hbWU6IGRhdGEubGFzdF9uYW1lLFxuXHQgICAgICBiaXJ0aF9kYXRlOiBkYXRhLmJpcnRoX2RhdGUsXG5cdCAgICAgIGdlbmRlcjogZGF0YS5nZW5kZXIsXG5cdCAgICAgIGNpdmlsX3N0YXR1czogZGF0YS5jaXZpbF9zdGF0dXMsXG5cdCAgICAgIGhlaWdodDogZGF0YS5oZWlnaHQsXG5cdCAgICAgIHdlaWdodDogZGF0YS53ZWlnaHQsXG5cdCAgICAgIGFkZHJlc3M6IGRhdGEuYWRkcmVzcyxcblx0ICAgICAgZW1haWw6IGRhdGEuZW1haWwsXG5cdCAgICAgIHBhc3Nwb3J0OiBkYXRhLnBhc3Nwb3J0LFxuXHQgICAgICBncm91cF9pZDogZGF0YS5ncm91cF9pZFxuXHQgICAgfSwgeyBiYXNlVVJMOiAnaHR0cDovLycrd2luZG93LkxhcmF2ZWwuY3BhbmVsX3VybCB9KVxuXHR9LFxuXG5cdGRpcmVjdGl2ZXM6IHtcblx0XHRkYXRlcGlja2VyOiB7XG5cdFx0ICBiaW5kKGVsLCBiaW5kaW5nLCB2bm9kZSkge1xuXHRcdCAgICAkKGVsKS5kYXRlcGlja2VyKHtcblx0XHQgICAgICBmb3JtYXQ6ICd5eXl5LW1tLWRkJ1xuXHRcdCAgICB9KS5vbignY2hhbmdlRGF0ZScsIChlKSA9PiB7XG5cdFx0ICAgICAgQ2xpZW50cy4kc2V0KENsaWVudHMuY2xpZW50Rm9ybSwgJ2JpcnRoX2RhdGUnLCBlLmZvcm1hdCgneXl5eS1tbS1kZCcpKTtcblx0XHQgICAgfSk7XG5cdFx0ICB9XG5cdFx0fVxuXHR9LFxuXHRtZXRob2RzOiB7XG5cdFx0Y2hhbmdlVXJsKCl7XG5cdFx0XHR2YXIgY3BhbmVsVXJsID0gd2luZG93LkxhcmF2ZWwuY3BhbmVsX3VybDtcblx0XHRcdHZhciBjbGllbnRJZCA9ICQoJyNjbGllbnQtaWQnKS52YWwoKTtcblx0XHRcdGlmKCQoJyNlbWFpbCcpLnZhbCgpPT0nJyl7XG5cdFx0ICAgIFx0JCgnI2Zvcm0tY2xpZW50JykuYXR0cignYWN0aW9uJywnaHR0cDovLycrY3BhbmVsVXJsKycvdmlzYS9jbGllbnQvdXBkYXRlLycrY2xpZW50SWQpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHQgICAgXHQkKCcjZm9ybS1jbGllbnQnKS5hdHRyKCdhY3Rpb24nLCdodHRwOi8vJytjcGFuZWxVcmwrJy92aXNhL2NsaWVudC91cGRhdGV3aXRoZW1haWwvJytjbGllbnRJZCk7XHRcdFx0XHRcblx0XHRcdH1cblx0XHR9XHRcblx0fSxcblx0Y3JlYXRlZCgpe1xuXHRcdGF4aW9zLmdldCgnL3Zpc2EvY2xpZW50L3Nob3dHcm91cCcpXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgICAgICB0aGlzLmdyb3VwcyA9IHJlc3VsdC5kYXRhO1xuXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5cdCAgICAgICAgICAgICQoXCIuc2VsZWN0MlwiKS5zZWxlY3QyKHtcblx0ICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIlNlbGVjdCBncm91cFwiLFxuXHQgICAgICAgICAgICAgICAgYWxsb3dDbGVhcjogdHJ1ZVxuXHQgICAgICAgICAgICB9KTtcblx0ICAgICAgICAgICAgJChcIi5zZWxlY3QyLXNlbGVjdGlvblwiKS5jc3MoJ2hlaWdodCcsJzM0cHgnKTtcbiAgICAgICAgXHR9LCA1MDApO1xuICAgIFx0fSk7XG5cbiAgICBcdHZhciBjcGFuZWxVcmwgPSB3aW5kb3cuTGFyYXZlbC5jcGFuZWxfdXJsO1xuXHRcdHZhciBjbGllbnRJZCA9ICQoJyNjbGllbnQtaWQnKS52YWwoKTtcblx0XHRpZihkYXRhLmVtYWlsPT1udWxsKXtcblx0ICAgIFx0JCgnI2Zvcm0tY2xpZW50JykuYXR0cignYWN0aW9uJywnaHR0cDovLycrY3BhbmVsVXJsKycvdmlzYS9jbGllbnQvdXBkYXRlLycrY2xpZW50SWQpO1xuXHRcdH0gZWxzZSB7XG5cdCAgICBcdCQoJyNmb3JtLWNsaWVudCcpLmF0dHIoJ2FjdGlvbicsJ2h0dHA6Ly8nK2NwYW5lbFVybCsnL3Zpc2EvY2xpZW50L3VwZGF0ZXdpdGhlbWFpbC8nK2NsaWVudElkKTtcdFx0XHRcdFxuXHRcdH1cblxuICAgICAgIEV2ZW50Lmxpc3RlbignSW1nZmlsZXVwbG9hZC5hdmF0YXJVcGxvYWQnLCAoaW1nKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5jbGllbnRGb3JtLmF2YXRhciA9IGltZztcbiAgICAgICAgICAgIFxuICAgICAgICB9KTtcdFx0XG5cblx0fSxcblx0bW91bnRlZCgpIHtcblx0XHQkKCdzZWxlY3QjZ3JvdXBfaWQnKS5vbihcImNoYW5nZVwiLCBmdW5jdGlvbigpIHtcblx0XHQgICAgbGV0IHZhbHVlID0gJChcInNlbGVjdCNncm91cF9pZCBvcHRpb246c2VsZWN0ZWRcIikudmFsKCk7XG5cdFx0ICAgIHRoaXMuY2xpZW50Rm9ybS5ncm91cF9pZCA9IHZhbHVlO1xuXHRcdH0uYmluZCh0aGlzKSk7IFxuXHR9XG59KTtcblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG5cbiAgICAkKCcuYnRuVXBsb2FkJykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAgICAgJCgnLmF2YXRhci11cGxvYWRlciBpbnB1dCcpLmNsaWNrKCk7XG4gICAgfSk7XG5cblx0JCgnI2ZpcnN0X25hbWUsICNtaWRkbGVfbmFtZSwgI2xhc3RfbmFtZScpLmtleXByZXNzKGZ1bmN0aW9uKGV2ZW50KXtcblx0XHR2YXIgaW5wdXRWYWx1ZSA9IGV2ZW50LmNoYXJDb2RlO1xuXHRcdC8vYWxlcnQoaW5wdXRWYWx1ZSk7XG5cdFx0aWYoISgoaW5wdXRWYWx1ZSA+IDY0ICYmIGlucHV0VmFsdWUgPCA5MSkgfHwgKGlucHV0VmFsdWUgPiA5NiAmJiBpbnB1dFZhbHVlIDwgMTIzKXx8KGlucHV0VmFsdWU9PTMyKSB8fCAoaW5wdXRWYWx1ZT09MCkpKXtcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdH1cblx0fSk7XG5cblx0JChcIiNjb250YWN0X251bWJlciwgI2FsdGVybmF0ZV9jb250YWN0X251bWJlciwgI3ppcF9jb2RlXCIpLmtleXByZXNzKGZ1bmN0aW9uIChlKSB7XG5cdCAvL2lmIHRoZSBsZXR0ZXIgaXMgbm90IGRpZ2l0IHRoZW4gZGlzcGxheSBlcnJvciBhbmQgZG9uJ3QgdHlwZSBhbnl0aGluZ1xuXHQgICAgaWYgKGUud2hpY2ggIT0gOCAmJiBlLndoaWNoICE9IDAgJiYgKGUud2hpY2ggPCA0OCB8fCBlLndoaWNoID4gNTcpKSB7XG5cdCAgICAgICAgcmV0dXJuIGZhbHNlO1xuXHQgICAgfVxuXHR9KTtcblxuXHQkKCdmb3JtI2Zvcm0tY2xpZW50Jykub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGUpIHtcblx0XHR2YXIgZmlyc3ROYW1lID0gJCgnaW5wdXQjZmlyc3RfbmFtZScpLnZhbCgpO1xuXHRcdHZhciBsYXN0TmFtZSA9ICQoJ2lucHV0I2xhc3RfbmFtZScpLnZhbCgpO1xuXHRcdHZhciBnZW5kZXIgPSAkKCdzZWxlY3RbbmFtZT1nZW5kZXJdJykudmFsKCk7XG5cdFx0dmFyIGJpcnRoZGF0ZSA9ICQoJ2lucHV0I2JpcnRoX2RhdGUnKS52YWwoKTtcblx0XHR2YXIgY29udGFjdE51bWJlciA9ICQoJ2lucHV0I2NvbnRhY3RfbnVtYmVyJykudmFsKCk7XG5cdFx0dmFyIHBhc3Nwb3J0ID0gJCgnaW5wdXQjcGFzc3BvcnQnKS52YWwoKTtcblxuXHRcdCQuYWpheCh7XG5cdFx0XHR1cmw6ICcvdmlzYS9jbGllbnQvdmFsaWRhdGlvbkJlZm9yZVN1Ym1pdCcsXG4gICAgICAgICAgICB0eXBlOiAnZ2V0JyxcbiAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgIFx0aWQ6ICQoJyNjbGllbnQtaWQnKS52YWwoKSxcbiAgICAgICAgICAgIFx0Zmlyc3ROYW1lOmZpcnN0TmFtZSwgXG5cdFx0XHRcdGxhc3ROYW1lOiBsYXN0TmFtZSxcblx0XHRcdFx0Z2VuZGVyOiBnZW5kZXIsXG5cdFx0XHRcdGJpcnRoZGF0ZTogYmlydGhkYXRlLFxuXHRcdFx0XHRjb250YWN0TnVtYmVyOiBjb250YWN0TnVtYmVyLFxuXHRcdFx0XHRwYXNzcG9ydDogcGFzc3BvcnQsXG5cdFx0XHRcdFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGFzeW5jOiBmYWxzZSxcbiAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKGRhdGEpIHtcbiAgICAgICAgICAgIFx0dmFyIGRhdGEgPSBKU09OLnBhcnNlKGRhdGEpO1xuXG4gICAgICAgICAgICBcdENsaWVudHMuY2xpZW50cyA9IGRhdGEudXNlcnM7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYoZGF0YS5iaW5kZWQpIHtcbiAgICAgICAgICAgICAgICBcdHRvYXN0ci5lcnJvcignQ29udGFjdCBudW1iZXIgaXMgYWxyZWFkeSBiaW5kZWQgaW4gdGhlIHZpc2EgYXBwLicpO1xuICAgICAgICAgICAgICAgIFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZihkYXRhLmR1cGxpY2F0ZUVudHJ5KSB7XG4gICAgICAgICAgICAgICAgICAgICQoJyNkdXBsaWNhdGVDbGllbnRzTGlzdE1vZGFsJykubW9kYWwoJ3Nob3cnKTtcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIH0gICBcbiAgICAgICAgICAgIH1cblx0XHR9KTtcblx0fSk7XG5cbn0gKTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NsaWVudHMvZWRpdC5qcyJdLCJzb3VyY2VSb290IjoiIn0=