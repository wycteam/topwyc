/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 484);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 194:
/***/ (function(module, exports, __webpack_require__) {

new Vue({

	el: '#notification-builder',

	data: {

		reportTypes: [],
		docs: [],

		selectedReportTypeId: null,

		selectedDocumentId: null

	},

	methods: {
		loadReportTypes: function loadReportTypes() {
			var _this = this;

			axios.get('/visa/notification/report-type').then(function (response) {
				_this.reportTypes = response.data;

				_this.callDataTable('report-types-table');
			}).catch(function (error) {
				return console.log(error);
			});
		},
		loadDocs: function loadDocs() {
			var _this2 = this;

			axios.get('/visa/notification/doc').then(function (response) {
				_this2.docs = response.data;

				_this2.callDataTable('docs-table');
			}).catch(function (error) {
				return console.log(error);
			});
		},
		callDataTable: function callDataTable(tableId) {
			$('#' + tableId).DataTable().destroy();

			setTimeout(function () {
				$('#' + tableId).DataTable({
					responsive: true,
					"lengthMenu": [[10, 25, 50], [10, 25, 50]],
					"iDisplayLength": 10
				});
			}, 500);
		},
		setSelectedReportTypeId: function setSelectedReportTypeId(val) {
			this.selectedReportTypeId = val;
		},
		setSelectedReportType: function setSelectedReportType(val) {
			this.$refs.updatereporttyperef.updateReportTypeForm.title = val.title;
			this.$refs.updatereporttyperef.updateReportTypeForm.title_cn = val.title_cn;
			this.$refs.updatereporttyperef.updateReportTypeForm.description = val.description;
			this.$refs.updatereporttyperef.updateReportTypeForm.services_id = val.services_id.split(",");
			this.$refs.updatereporttyperef.updateChosen(val.services_id.split(","));
			this.$refs.updatereporttyperef.updateReportTypeForm.with_docs = val.with_docs == 1 ? true : false;
		},
		setSelectedDocumentId: function setSelectedDocumentId(val) {
			this.selectedDocumentId = val;
		},
		setSelectedDocument: function setSelectedDocument(val) {
			this.$refs.updatedocumentref.updateDocumentForm.title = val.title;
			this.$refs.updatedocumentref.updateDocumentForm.title_cn = val.title_cn;
			this.$refs.updatedocumentref.updateDocumentForm.doc_types = val.doc_types;
		}
	},

	created: function created() {
		this.loadReportTypes();

		this.loadDocs();
	},


	components: {
		'add-report-type': __webpack_require__(337),
		'update-report-type': __webpack_require__(350),
		'delete-report-type': __webpack_require__(343),

		'add-document': __webpack_require__(333),
		'update-document': __webpack_require__(349),
		'delete-document': __webpack_require__(341)
	}

});

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 240:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			addDocForm: {
				title: '',
				title_cn: '',
				doc_types: [],
				errors: ''
			}
		};
	},


	methods: {
		addDocumentType: function addDocumentType() {
			this.addDocForm.doc_types.push({ title: '', title_cn: '' });
		},
		removeDocumentType: function removeDocumentType(index) {
			this.addDocForm.doc_types.splice(index, 1);
		},
		addDoc: function addDoc() {
			var _this = this;

			axios.post('/visa/notification/doc', {
				title: this.addDocForm.title,
				title_cn: this.addDocForm.title_cn,
				doc_types: this.addDocForm.doc_types
			}).then(function (response) {
				if (response.data.success) {
					_this.clearAddDocForm();

					_this.$parent.$parent.loadDocs();

					$('#add-document-modal').modal('hide');

					toastr.success(response.data.message);
				}
			}).catch(function (error) {
				return _this.addDocForm.errors = error.response.data;
			});
		},
		clearAddDocForm: function clearAddDocForm() {
			this.addDocForm = {
				title: '',
				title_cn: '',
				doc_types: [],
				errors: ''
			};
		}
	}

});

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			services: [],

			addReportTypeForm: {
				title: '',
				title_cn: '',
				description: '',
				services_id: [],
				with_docs: false,
				errors: ''
			}
		};
	},


	methods: {
		addReportType: function addReportType() {
			var _this = this;

			axios.post('/visa/notification/report-type', {
				title: this.addReportTypeForm.title,
				title_cn: this.addReportTypeForm.title_cn,
				description: this.addReportTypeForm.description,
				services_id: this.addReportTypeForm.services_id,
				with_docs: this.addReportTypeForm.with_docs
			}).then(function (response) {
				if (response.data.success) {
					_this.clearAddReportTypeForm();

					_this.$parent.$parent.loadReportTypes();

					$('#add-report-type-modal').modal('hide');

					toastr.success(response.data.message);
				}
			}).catch(function (error) {
				return _this.addReportTypeForm.errors = error.response.data;
			});
		},
		clearAddReportTypeForm: function clearAddReportTypeForm() {
			this.addReportTypeForm = {
				title: '',
				title_cn: '',
				description: '',
				services_id: [],
				with_docs: false,
				errors: ''

				// Deselect All
			};$('.chosen-select-for-services').val('').trigger('chosen:updated');
		},
		loadServices: function loadServices() {
			var _this2 = this;

			axios.get('/visa/notification/services').then(function (response) {
				_this2.services = response.data;

				setTimeout(function () {
					$(".chosen-select-for-services").trigger("chosen:updated");
				}, 500);
			}).catch(function (error) {
				return console.log(error);
			});
		},
		initChosenSelect: function initChosenSelect() {
			$('.chosen-select-for-services').chosen({
				width: "100%",
				no_results_text: "No result/s found.",
				search_contains: true
			});

			var vm = this;

			$('.chosen-select-for-services').on('change', function () {
				vm.addReportTypeForm.services_id = $(this).val();
			});
		}
	},

	created: function created() {
		this.loadServices();
	},
	mounted: function mounted() {
		this.initChosenSelect();
	}
});

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['documentid'],

	methods: {
		deleteDocument: function deleteDocument() {
			var _this = this;

			axios.delete('/visa/notification/doc/' + this.documentid).then(function (response) {
				if (response.data.success) {
					_this.$parent.$parent.setSelectedDocumentId(null);

					_this.$parent.$parent.loadDocs();

					$('#delete-document-modal').modal('hide');

					toastr.success(response.data.message);
				}
			}).catch(function (error) {
				return console.log(error);
			});
		}
	}

});

/***/ }),

/***/ 250:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['reporttypeid'],

	methods: {
		deleteReportType: function deleteReportType() {
			var _this = this;

			axios.delete('/visa/notification/report-type/' + this.reporttypeid).then(function (response) {
				if (response.data.success) {
					_this.$parent.$parent.setSelectedReportTypeId(null);

					_this.$parent.$parent.loadReportTypes();

					$('#delete-report-type-modal').modal('hide');

					toastr.success(response.data.message);
				}
			}).catch(function (error) {
				return console.log(error);
			});
		}
	}

});

/***/ }),

/***/ 256:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['documentid'],

	data: function data() {
		return {
			updateDocumentForm: {
				title: '',
				title_cn: '',
				doc_types: [],
				errors: ''
			}
		};
	},


	methods: {
		addDocumentType: function addDocumentType() {
			this.updateDocumentForm.doc_types.push({ id: '', title: '', title_cn: '' });
		},
		removeDocumentType: function removeDocumentType(index) {
			this.updateDocumentForm.doc_types.splice(index, 1);
		},
		updateDocument: function updateDocument() {
			var _this = this;

			axios.patch('/visa/notification/doc/' + this.documentid, {
				title: this.updateDocumentForm.title,
				title_cn: this.updateDocumentForm.title_cn,
				doc_types: this.updateDocumentForm.doc_types
			}).then(function (response) {
				if (response.data.success) {
					_this.clearUpdateDocumentForm();

					_this.$parent.$parent.loadDocs();

					$('#update-document-modal').modal('hide');

					toastr.success(response.data.message);
				}
			}).catch(function (error) {
				return _this.updateDocumentForm.errors = error.response.data;
			});
		},
		clearUpdateDocumentForm: function clearUpdateDocumentForm() {
			this.updateDocumentForm = {
				title: '',
				title_cn: '',
				doc_types: [],
				errors: ''
			};
		}
	}

});

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['reporttypeid'],

	data: function data() {
		return {
			services: [],

			updateReportTypeForm: {
				title: '',
				title_cn: '',
				description: '',
				services_id: [],
				with_docs: false,
				errors: ''
			}
		};
	},


	methods: {
		updateReportType: function updateReportType() {
			var _this = this;

			axios.patch('/visa/notification/report-type/' + this.reporttypeid, {
				title: this.updateReportTypeForm.title,
				title_cn: this.updateReportTypeForm.title_cn,
				description: this.updateReportTypeForm.description,
				services_id: this.updateReportTypeForm.services_id,
				with_docs: this.updateReportTypeForm.with_docs
			}).then(function (response) {
				if (response.data.success) {
					_this.clearUpdateReportTypeForm();

					_this.$parent.$parent.loadReportTypes();

					$('#update-report-type-modal').modal('hide');

					toastr.success(response.data.message);
				}
			}).catch(function (error) {
				return _this.updateReportTypeForm.errors = error.response.data;
			});
		},
		clearUpdateReportTypeForm: function clearUpdateReportTypeForm() {
			this.updateReportTypeForm = {
				title: '',
				title_cn: '',
				description: '',
				services_id: [],
				with_docs: false,
				errors: ''

				// Deselect All
			};$('.chosen-select-for-services').val('').trigger('chosen:updated');
		},
		loadServices: function loadServices() {
			var _this2 = this;

			axios.get('/visa/notification/services').then(function (response) {
				_this2.services = response.data;

				setTimeout(function () {
					$(".chosen-select-for-services").trigger("chosen:updated");
				}, 500);
			}).catch(function (error) {
				return console.log(error);
			});
		},
		updateChosen: function updateChosen(servicesId) {
			setTimeout(function () {
				$(".chosen-select-for-services").val(servicesId).trigger("chosen:updated");
			}, 500);
		},
		initChosenSelect: function initChosenSelect() {
			$('.chosen-select-for-services').chosen({
				width: "100%",
				no_results_text: "No result/s found.",
				search_contains: true
			});

			var vm = this;

			$('.chosen-select-for-services').on('change', function () {
				vm.updateReportTypeForm.services_id = $(this).val();
			});
		}
	},

	created: function created() {
		this.loadServices();
	},
	mounted: function mounted() {
		this.initChosenSelect();
	}
});

/***/ }),

/***/ 292:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-0692f896] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-0692f896] {\n\tmargin-right: 10px;\n}\ninput[type=\"checkbox\"][data-v-0692f896] {\n\tmargin-top: 15px;\n}\n", "", {"version":3,"sources":["AddReportType.vue?7c2b15a8"],"names":[],"mappings":";AAkKA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA;AACA;CACA,iBAAA;CACA","file":"AddReportType.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"addReportType\">\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': addReportTypeForm.errors.hasOwnProperty('title') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Title:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"title\" v-model=\"addReportTypeForm.title\">\n\t\t    \t<p class=\"help-block\" v-if=\"addReportTypeForm.errors.hasOwnProperty('title')\" v-text=\"addReportTypeForm.errors.title[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': addReportTypeForm.errors.hasOwnProperty('title_cn') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Title CN:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"title_cn\" v-model=\"addReportTypeForm.title_cn\">\n\t\t    \t<p class=\"help-block\" v-if=\"addReportTypeForm.errors.hasOwnProperty('title_cn')\" v-text=\"addReportTypeForm.errors.title_cn[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': addReportTypeForm.errors.hasOwnProperty('description') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Description:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"description\" v-model=\"addReportTypeForm.description\">\n\t\t    \t<p class=\"help-block\" v-if=\"addReportTypeForm.errors.hasOwnProperty('description')\" v-text=\"addReportTypeForm.errors.description[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': addReportTypeForm.errors.hasOwnProperty('services_id') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Services:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<select data-placeholder=\"Select Service\" class=\"chosen-select-for-services\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t            <option v-for=\"service in services\" :value=\"service.id\">\n\t\t                {{ (service.detail).trim() }}\n\t\t            </option>\n\t\t        </select>\n\t\t        <p class=\"help-block\" v-if=\"addReportTypeForm.errors.hasOwnProperty('services_id')\" v-text=\"addReportTypeForm.errors.services_id[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        With Docs:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"checkbox\" name=\"with_docs\" v-model=\"addReportTypeForm.with_docs\">\n\t\t    </div>\n\t\t</div>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Add</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\t\n\texport default {\n\n\t\tdata() {\n    \t\treturn {\n    \t\t\tservices: [],\n\n    \t\t\taddReportTypeForm: {\n\t\t\t\t\ttitle: '',\n\t\t\t\t\ttitle_cn: '',\n\t\t\t\t\tdescription: '',\n\t\t\t\t\tservices_id: [],\n\t\t\t\t\twith_docs: false,\n\t\t\t\t\terrors: ''\n\t\t\t\t}\n    \t\t}\n    \t},\n\n\t\tmethods: {\n\n\t\t\taddReportType() {\n\t\t\t\taxios.post('/visa/notification/report-type', {\n\t\t\t\t\t\ttitle: this.addReportTypeForm.title,\n\t\t\t\t\t\ttitle_cn: this.addReportTypeForm.title_cn,\n\t\t\t\t\t\tdescription: this.addReportTypeForm.description,\n\t\t\t\t\t\tservices_id: this.addReportTypeForm.services_id,\n\t\t\t\t\t\twith_docs: this.addReportTypeForm.with_docs\n\t\t\t\t\t})\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tif(response.data.success) {\n\t\t\t\t\t\t\tthis.clearAddReportTypeForm();\n\n\t\t                \tthis.$parent.$parent.loadReportTypes();\n\n\t\t                \t$('#add-report-type-modal').modal('hide');\n\n\t\t                \ttoastr.success(response.data.message);\n\t\t                }\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => this.addReportTypeForm.errors = error.response.data);\n\t\t\t},\n\n\t\t\tclearAddReportTypeForm() {\n\t\t\t\tthis.addReportTypeForm = {\n\t\t\t\t\ttitle: '',\n\t\t\t\t\ttitle_cn: '',\n\t\t\t\t\tdescription: '',\n\t\t\t\t\tservices_id: [],\n\t\t\t\t\twith_docs: false,\n\t\t\t\t\terrors: ''\n\t\t\t\t}\n\n\t\t\t\t// Deselect All\n\t\t\t\t$('.chosen-select-for-services').val('').trigger('chosen:updated');\n\t\t\t},\n\n\t\t\tloadServices() {\n\t\t\t\taxios.get('/visa/notification/services')\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tthis.services = response.data;\n\n\t\t\t\t\t\tsetTimeout(() => {\n\t\t\t\t\t\t\t$(\".chosen-select-for-services\").trigger(\"chosen:updated\");\n\t\t\t\t\t\t}, 500);\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => console.log(error));\n\t\t\t},\n\n\t\t\tinitChosenSelect() {\n\t        \t$('.chosen-select-for-services').chosen({\n\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\tsearch_contains: true\n\t\t\t\t});\n\n\t        \tvar vm = this;\n\n\t\t\t\t$('.chosen-select-for-services').on('change', function() {\n\t\t\t\t\tvm.addReportTypeForm.services_id = $(this).val();\n\t\t\t\t});\n\t        }\n\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.loadServices();\n\t\t},\n\n\t\tmounted() {\n        \tthis.initChosenSelect();\n    \t}\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n\tinput[type=\"checkbox\"] {\n\t\tmargin-top: 15px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(7)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 300:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-3bd9bf82] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-3bd9bf82] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["AddDocument.vue?22984510"],"names":[],"mappings":";AAoIA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"AddDocument.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"addDoc\">\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': addDocForm.errors.hasOwnProperty('title') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Title:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"title\" v-model=\"addDocForm.title\">\n\t\t    \t<p class=\"help-block\" v-if=\"addDocForm.errors.hasOwnProperty('title')\" v-text=\"addDocForm.errors.title[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': addDocForm.errors.hasOwnProperty('title_cn') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Title CN:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"title_cn\" v-model=\"addDocForm.title_cn\">\n\t\t    \t<p class=\"help-block\" v-if=\"addDocForm.errors.hasOwnProperty('title_cn')\" v-text=\"addDocForm.errors.title_cn[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<hr>\n\n\t\t\t<button type=\"button\" class=\"btn pull-left\" @click=\"addDocumentType\">\n\t\t\t\t<i class=\"fa fa-plus\"></i> Add Document Type\n\t\t\t</button>\n\n\t\t<div style=\"height:20px; clear:both;\"></div>\n\n\t\t\t<div v-for=\"(doc_type, index) in addDocForm.doc_types\">\n\t\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': addDocForm.errors.hasOwnProperty('doc_types.'+index+'.title') }\">\n\t\t\t\t\t<div class=\"col-md-2\">\n\t\t\t\t    \t<button type=\"button\" class=\"btn btn-danger pull-right\" @click=\"removeDocumentType(index)\">\n\t\t\t\t    \t\t<i class=\"fa fa-times\"></i>\n\t\t\t\t    \t</button>\n\t\t\t\t    </div>\n\t\t\t\t\t<label class=\"col-md-2 control-label\">\n\t\t\t\t        Name:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t    \t<input type=\"text\" class=\"form-control\" v-model=\"doc_type.title\">\n\t\t\t\t    \t<p class=\"help-block\" v-if=\"addDocForm.errors.hasOwnProperty('doc_types.'+index+'.title')\">The name field is required.</p>\n\t\t\t\t    </div>\n\t\t\t    </div>\n\n\t\t\t    <div class=\"form-group\" :class=\"{ 'has-error': addDocForm.errors.hasOwnProperty('doc_types.'+index+'.title_cn') }\">\n\t\t\t    \t<div class=\"col-md-2\">&nbsp;</div>\n\t\t\t\t\t<label class=\"col-md-2 control-label\">\n\t\t\t\t        Name CN:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t    \t<input type=\"text\" class=\"form-control\" v-model=\"doc_type.title_cn\">\n\t\t\t\t    \t<p class=\"help-block\" v-if=\"addDocForm.errors.hasOwnProperty('doc_types.'+index+'.title_cn')\">The name cn field is required.</p>\n\t\t\t\t    </div>\n\t\t\t    </div>\n\t\t\t</div>\n\n\t\t<div style=\"height:20px; clear:both;\"></div>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Add</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\t\n\texport default {\n\n\t\tdata() {\n    \t\treturn {\n    \t\t\taddDocForm: {\n\t\t\t\t\ttitle: '',\n\t\t\t\t\ttitle_cn: '',\n\t\t\t\t\tdoc_types: [],\n\t\t\t\t\terrors: ''\n\t\t\t\t}\n    \t\t}\n    \t},\n\n\t\tmethods: {\n\n\t\t\taddDocumentType() {\n\t\t\t\tthis.addDocForm.doc_types.push({ title: '', title_cn: '' });\n\t\t\t},\n\n\t\t\tremoveDocumentType(index) {\n\t\t\t\tthis.addDocForm.doc_types.splice(index, 1);\n\t\t\t},\n\n\t\t\taddDoc() {\n\t\t\t\taxios.post('/visa/notification/doc', {\n\t\t\t\t\t\ttitle: this.addDocForm.title,\n\t\t\t\t\t\ttitle_cn: this.addDocForm.title_cn,\n\t\t\t\t\t\tdoc_types: this.addDocForm.doc_types\n\t\t\t\t\t})\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tif(response.data.success) {\n\t\t\t\t\t\t\tthis.clearAddDocForm();\n\n\t\t                \tthis.$parent.$parent.loadDocs();\n\n\t\t                \t$('#add-document-modal').modal('hide');\n\n\t\t                \ttoastr.success(response.data.message);\n\t\t                }\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => this.addDocForm.errors = error.response.data);\n\t\t\t},\n\n\n\t\t\tclearAddDocForm() {\n\t\t\t\tthis.addDocForm = {\n\t\t\t\t\ttitle: '',\n\t\t\t\t\ttitle_cn: '',\n\t\t\t\t\tdoc_types: [],\n\t\t\t\t\terrors: ''\n\t\t\t\t};\n\t\t\t}\n\n\n\t\t}\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 303:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-61070601] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-61070601] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["UpdateReportType.vue?15c3f7ce"],"names":[],"mappings":";AA0KA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"UpdateReportType.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"updateReportType\">\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': updateReportTypeForm.errors.hasOwnProperty('title') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Title:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"title\" v-model=\"updateReportTypeForm.title\">\n\t\t    \t<p class=\"help-block\" v-if=\"updateReportTypeForm.errors.hasOwnProperty('title')\" v-text=\"updateReportTypeForm.errors.title[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': updateReportTypeForm.errors.hasOwnProperty('title_cn') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Title CN:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"title_cn\" v-model=\"updateReportTypeForm.title_cn\">\n\t\t    \t<p class=\"help-block\" v-if=\"updateReportTypeForm.errors.hasOwnProperty('title_cn')\" v-text=\"updateReportTypeForm.errors.title_cn[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': updateReportTypeForm.errors.hasOwnProperty('description') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Description:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"description\" v-model=\"updateReportTypeForm.description\">\n\t\t    \t<p class=\"help-block\" v-if=\"updateReportTypeForm.errors.hasOwnProperty('description')\" v-text=\"updateReportTypeForm.errors.description[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': updateReportTypeForm.errors.hasOwnProperty('services_id') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Services:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<select data-placeholder=\"Select Service\" class=\"chosen-select-for-services\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t            <option v-for=\"service in services\" :value=\"service.id\">\n\t\t                {{ (service.detail).trim() }}\n\t\t            </option>\n\t\t        </select>\n\t\t        <p class=\"help-block\" v-if=\"updateReportTypeForm.errors.hasOwnProperty('services_id')\" v-text=\"updateReportTypeForm.errors.services_id[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        With Docs:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"checkbox\" name=\"with_docs\" v-model=\"updateReportTypeForm.with_docs\">\n\t\t    </div>\n\t\t</div>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Update</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\t\n\texport default {\n\n\t\tprops: ['reporttypeid'],\n\n\t\tdata() {\n    \t\treturn {\n    \t\t\tservices: [],\n\n    \t\t\tupdateReportTypeForm: {\n\t\t\t\t\ttitle: '',\n\t\t\t\t\ttitle_cn: '',\n\t\t\t\t\tdescription: '',\n\t\t\t\t\tservices_id: [],\n\t\t\t\t\twith_docs: false,\n\t\t\t\t\terrors: ''\n\t\t\t\t}\n    \t\t}\n    \t},\n\n    \tmethods: {\n\n    \t\tupdateReportType() {\n\t\t\t\taxios.patch('/visa/notification/report-type/' + this.reporttypeid, {\n\t\t\t\t\t\ttitle: this.updateReportTypeForm.title,\n\t\t\t\t\t\ttitle_cn: this.updateReportTypeForm.title_cn,\n\t\t\t\t\t\tdescription: this.updateReportTypeForm.description,\n\t\t\t\t\t\tservices_id: this.updateReportTypeForm.services_id,\n\t\t\t\t\t\twith_docs: this.updateReportTypeForm.with_docs\n\t\t\t\t\t})\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tif(response.data.success) {\n\t\t\t\t\t\t\tthis.clearUpdateReportTypeForm();\n\n\t\t                \tthis.$parent.$parent.loadReportTypes();\n\n\t\t                \t$('#update-report-type-modal').modal('hide');\n\n\t\t                \ttoastr.success(response.data.message);\n\t\t                }\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => this.updateReportTypeForm.errors = error.response.data);\n\t\t\t},\n\n    \t\tclearUpdateReportTypeForm() {\n\t\t\t\tthis.updateReportTypeForm = {\n\t\t\t\t\ttitle: '',\n\t\t\t\t\ttitle_cn: '',\n\t\t\t\t\tdescription: '',\n\t\t\t\t\tservices_id: [],\n\t\t\t\t\twith_docs: false,\n\t\t\t\t\terrors: ''\n\t\t\t\t}\n\n\t\t\t\t// Deselect All\n\t\t\t\t$('.chosen-select-for-services').val('').trigger('chosen:updated');\n\t\t\t},\n\n    \t\tloadServices() {\n\t\t\t\taxios.get('/visa/notification/services')\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tthis.services = response.data;\n\n\t\t\t\t\t\tsetTimeout(() => {\n\t\t\t\t\t\t\t$(\".chosen-select-for-services\").trigger(\"chosen:updated\");\n\t\t\t\t\t\t}, 500);\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => console.log(error));\n\t\t\t},\n\n\t\t\tupdateChosen(servicesId) {\n\t\t\t\tsetTimeout(() => {\n\t\t\t\t\t$(\".chosen-select-for-services\").val(servicesId).trigger(\"chosen:updated\");\n\t\t\t\t}, 500);\n\t\t\t},\n\n\t\t\tinitChosenSelect() {\n\t        \t$('.chosen-select-for-services').chosen({\n\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\tsearch_contains: true\n\t\t\t\t});\n\n\t        \tvar vm = this;\n\n\t\t\t\t$('.chosen-select-for-services').on('change', function() {\n\t\t\t\t\tvm.updateReportTypeForm.services_id = $(this).val();\n\t\t\t\t});\n\t        }\n\n    \t},\n\n    \tcreated() {\n\t\t\tthis.loadServices();\n\t\t},\n\n\t\tmounted() {\n        \tthis.initChosenSelect();\n    \t}\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-72833664] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-72833664] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["UpdateDocument.vue?5860d390"],"names":[],"mappings":";AAuIA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"UpdateDocument.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"updateDocument\">\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': updateDocumentForm.errors.hasOwnProperty('title') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Title:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"title\" v-model=\"updateDocumentForm.title\">\n\t\t    \t<p class=\"help-block\" v-if=\"updateDocumentForm.errors.hasOwnProperty('title')\" v-text=\"updateDocumentForm.errors.title[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<div class=\"form-group\" :class=\"{ 'has-error': updateDocumentForm.errors.hasOwnProperty('title_cn') }\">\n\t\t    <label class=\"col-md-2 control-label\">\n\t\t        Title CN:\n\t        </label>\n\t\t    <div class=\"col-md-10\">\n\t\t    \t<input type=\"text\" class=\"form-control\" name=\"title_cn\" v-model=\"updateDocumentForm.title_cn\">\n\t\t    \t<p class=\"help-block\" v-if=\"updateDocumentForm.errors.hasOwnProperty('title_cn')\" v-text=\"updateDocumentForm.errors.title_cn[0]\"></p>\n\t\t    </div>\n\t\t</div>\n\n\t\t<hr>\n\n\t\t\t<button type=\"button\" class=\"btn pull-left\" @click=\"addDocumentType\">\n\t\t\t\t<i class=\"fa fa-plus\"></i> Add Document Type\n\t\t\t</button>\n\n\t\t<div style=\"height:20px; clear:both;\"></div>\n\n\t\t\t<div v-for=\"(doc_type, index) in updateDocumentForm.doc_types\">\n\n\t\t\t\t<input type=\"hidden\" v-model=\"doc_type.id\">\n\n\t\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': updateDocumentForm.errors.hasOwnProperty('doc_types.'+index+'.title') }\">\n\t\t\t\t\t<div class=\"col-md-2\">\n\t\t\t\t    \t<button type=\"button\" class=\"btn btn-danger pull-right\" @click=\"removeDocumentType(index)\">\n\t\t\t\t    \t\t<i class=\"fa fa-times\"></i>\n\t\t\t\t    \t</button>\n\t\t\t\t    </div>\n\t\t\t\t\t<label class=\"col-md-2 control-label\">\n\t\t\t\t        Name:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t    \t<input type=\"text\" class=\"form-control\" v-model=\"doc_type.title\">\n\t\t\t\t    \t<p class=\"help-block\" v-if=\"updateDocumentForm.errors.hasOwnProperty('doc_types.'+index+'.title')\">The name field is required.</p>\n\t\t\t\t    </div>\n\t\t\t    </div>\n\n\t\t\t    <div class=\"form-group\" :class=\"{ 'has-error': updateDocumentForm.errors.hasOwnProperty('doc_types.'+index+'.title_cn') }\">\n\t\t\t    \t<div class=\"col-md-2\">&nbsp;</div>\n\t\t\t\t\t<label class=\"col-md-2 control-label\">\n\t\t\t\t        Name CN:\n\t\t\t        </label>\n\t\t\t        <div class=\"col-md-8\">\n\t\t\t\t    \t<input type=\"text\" class=\"form-control\" v-model=\"doc_type.title_cn\">\n\t\t\t\t    \t<p class=\"help-block\" v-if=\"updateDocumentForm.errors.hasOwnProperty('doc_types.'+index+'.title_cn')\">The name cn field is required.</p>\n\t\t\t\t    </div>\n\t\t\t    </div>\n\t\t\t</div>\n\n\t\t<div style=\"height:20px; clear:both;\"></div>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Update</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\t\n\texport default {\n\n\t\tprops: ['documentid'],\n\n\t\tdata() {\n    \t\treturn {\n    \t\t\tupdateDocumentForm: {\n\t\t\t\t\ttitle: '',\n\t\t\t\t\ttitle_cn: '',\n\t\t\t\t\tdoc_types: [],\n\t\t\t\t\terrors: ''\n\t\t\t\t}\n    \t\t}\n    \t},\n\n    \tmethods: {\n\n    \t\taddDocumentType() {\n\t\t\t\tthis.updateDocumentForm.doc_types.push({ id: '', title: '', title_cn: '' });\n\t\t\t},\n\n\t\t\tremoveDocumentType(index) {\n\t\t\t\tthis.updateDocumentForm.doc_types.splice(index, 1);\n\t\t\t},\n\n    \t\tupdateDocument() {\n\t\t\t\taxios.patch('/visa/notification/doc/' + this.documentid, {\n\t\t\t\t\t\ttitle: this.updateDocumentForm.title,\n\t\t\t\t\t\ttitle_cn: this.updateDocumentForm.title_cn,\n\t\t\t\t\t\tdoc_types: this.updateDocumentForm.doc_types\n\t\t\t\t\t})\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tif(response.data.success) {\n\t\t\t\t\t\t\tthis.clearUpdateDocumentForm();\n\n\t\t                \tthis.$parent.$parent.loadDocs();\n\n\t\t                \t$('#update-document-modal').modal('hide');\n\n\t\t                \ttoastr.success(response.data.message);\n\t\t                }\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => this.updateDocumentForm.errors = error.response.data);\n\t\t\t},\n\n    \t\tclearUpdateDocumentForm() {\n\t\t\t\tthis.updateDocumentForm = {\n\t\t\t\t\ttitle: '',\n\t\t\t\t\ttitle_cn: '',\n\t\t\t\t\tdoc_types: [],\n\t\t\t\t\terrors: ''\n\t\t\t\t}\n\t\t\t},\n\n    \t}\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 309:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-948322a0] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-948322a0] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["DeleteDocument.vue?0feee9fe"],"names":[],"mappings":";AA4CA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"DeleteDocument.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"deleteDocument\">\n\n\t\t<p>Are you sure you want to delete this document?</p>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Delete</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\t\n\texport default {\n\n\t\tprops: ['documentid'],\n\n\t\tmethods: {\n\n\t\t\tdeleteDocument() {\n\t\t\t\taxios.delete('/visa/notification/doc/' + this.documentid)\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tif(response.data.success) {\n\t\t\t\t\t\t\tthis.$parent.$parent.setSelectedDocumentId(null);\n\n\t\t                \tthis.$parent.$parent.loadDocs();\n\n\t\t                \t$('#delete-document-modal').modal('hide');\n\n\t\t                \ttoastr.success(response.data.message);\n\t\t                }\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => console.log(error));\n\t\t\t}\n\n\t\t}\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 314:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-dfa7c13a] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-dfa7c13a] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["DeleteReportType.vue?7ffd3033"],"names":[],"mappings":";AA4CA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"DeleteReportType.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"deleteReportType\">\n\n\t\t<p>Are you sure you want to delete this report type?</p>\n\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Delete</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t</form>\n\n</template>\n\n<script>\n\t\n\texport default {\n\n\t\tprops: ['reporttypeid'],\n\n\t\tmethods: {\n\n\t\t\tdeleteReportType() {\n\t\t\t\taxios.delete('/visa/notification/report-type/' + this.reporttypeid)\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tif(response.data.success) {\n\t\t\t\t\t\t\tthis.$parent.$parent.setSelectedReportTypeId(null);\n\n\t\t                \tthis.$parent.$parent.loadReportTypes();\n\n\t\t                \t$('#delete-report-type-modal').modal('hide');\n\n\t\t                \ttoastr.success(response.data.message);\n\t\t                }\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error => console.log(error));\n\t\t\t}\n\n\t\t}\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(438)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(240),
  /* template */
  __webpack_require__(387),
  /* scopeId */
  "data-v-3bd9bf82",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/AddDocument.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AddDocument.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3bd9bf82", Component.options)
  } else {
    hotAPI.reload("data-v-3bd9bf82", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 337:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(430)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(244),
  /* template */
  __webpack_require__(377),
  /* scopeId */
  "data-v-0692f896",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/AddReportType.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AddReportType.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0692f896", Component.options)
  } else {
    hotAPI.reload("data-v-0692f896", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 341:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(447)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(248),
  /* template */
  __webpack_require__(413),
  /* scopeId */
  "data-v-948322a0",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/DeleteDocument.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DeleteDocument.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-948322a0", Component.options)
  } else {
    hotAPI.reload("data-v-948322a0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(452)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(250),
  /* template */
  __webpack_require__(423),
  /* scopeId */
  "data-v-dfa7c13a",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/DeleteReportType.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DeleteReportType.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-dfa7c13a", Component.options)
  } else {
    hotAPI.reload("data-v-dfa7c13a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(444)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(256),
  /* template */
  __webpack_require__(407),
  /* scopeId */
  "data-v-72833664",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/UpdateDocument.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] UpdateDocument.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-72833664", Component.options)
  } else {
    hotAPI.reload("data-v-72833664", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(441)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(257),
  /* template */
  __webpack_require__(402),
  /* scopeId */
  "data-v-61070601",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/UpdateReportType.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] UpdateReportType.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-61070601", Component.options)
  } else {
    hotAPI.reload("data-v-61070601", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 377:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addReportType($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addReportTypeForm.errors.hasOwnProperty('title')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Title:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addReportTypeForm.title),
      expression: "addReportTypeForm.title"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "title"
    },
    domProps: {
      "value": (_vm.addReportTypeForm.title)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addReportTypeForm, "title", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addReportTypeForm.errors.hasOwnProperty('title')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addReportTypeForm.errors.title[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addReportTypeForm.errors.hasOwnProperty('title_cn')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Title CN:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addReportTypeForm.title_cn),
      expression: "addReportTypeForm.title_cn"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "title_cn"
    },
    domProps: {
      "value": (_vm.addReportTypeForm.title_cn)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addReportTypeForm, "title_cn", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addReportTypeForm.errors.hasOwnProperty('title_cn')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addReportTypeForm.errors.title_cn[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addReportTypeForm.errors.hasOwnProperty('description')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Description:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addReportTypeForm.description),
      expression: "addReportTypeForm.description"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "description"
    },
    domProps: {
      "value": (_vm.addReportTypeForm.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addReportTypeForm, "description", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addReportTypeForm.errors.hasOwnProperty('description')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addReportTypeForm.errors.description[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addReportTypeForm.errors.hasOwnProperty('services_id')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Services:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-services",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "data-placeholder": "Select Service",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.services), function(service) {
    return _c('option', {
      domProps: {
        "value": service.id
      }
    }, [_vm._v("\n\t                " + _vm._s((service.detail).trim()) + "\n\t            ")])
  }), 0), _vm._v(" "), (_vm.addReportTypeForm.errors.hasOwnProperty('services_id')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addReportTypeForm.errors.services_id[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        With Docs:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addReportTypeForm.with_docs),
      expression: "addReportTypeForm.with_docs"
    }],
    attrs: {
      "type": "checkbox",
      "name": "with_docs"
    },
    domProps: {
      "checked": Array.isArray(_vm.addReportTypeForm.with_docs) ? _vm._i(_vm.addReportTypeForm.with_docs, null) > -1 : (_vm.addReportTypeForm.with_docs)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.addReportTypeForm.with_docs,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = null,
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.addReportTypeForm, "with_docs", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.addReportTypeForm, "with_docs", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.addReportTypeForm, "with_docs", $$c)
        }
      }
    }
  })])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Add")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0692f896", module.exports)
  }
}

/***/ }),

/***/ 387:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addDoc($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addDocForm.errors.hasOwnProperty('title')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Title:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addDocForm.title),
      expression: "addDocForm.title"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "title"
    },
    domProps: {
      "value": (_vm.addDocForm.title)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addDocForm, "title", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addDocForm.errors.hasOwnProperty('title')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addDocForm.errors.title[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.addDocForm.errors.hasOwnProperty('title_cn')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Title CN:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.addDocForm.title_cn),
      expression: "addDocForm.title_cn"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "title_cn"
    },
    domProps: {
      "value": (_vm.addDocForm.title_cn)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.addDocForm, "title_cn", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.addDocForm.errors.hasOwnProperty('title_cn')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.addDocForm.errors.title_cn[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('hr'), _vm._v(" "), _c('button', {
    staticClass: "btn pull-left",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.addDocumentType
    }
  }, [_c('i', {
    staticClass: "fa fa-plus"
  }), _vm._v(" Add Document Type\n\t\t")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _vm._l((_vm.addDocForm.doc_types), function(doc_type, index) {
    return _c('div', [_c('div', {
      staticClass: "form-group",
      class: {
        'has-error': _vm.addDocForm.errors.hasOwnProperty('doc_types.' + index + '.title')
      }
    }, [_c('div', {
      staticClass: "col-md-2"
    }, [_c('button', {
      staticClass: "btn btn-danger pull-right",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.removeDocumentType(index)
        }
      }
    }, [_c('i', {
      staticClass: "fa fa-times"
    })])]), _vm._v(" "), _c('label', {
      staticClass: "col-md-2 control-label"
    }, [_vm._v("\n\t\t\t        Name:\n\t\t        ")]), _vm._v(" "), _c('div', {
      staticClass: "col-md-8"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (doc_type.title),
        expression: "doc_type.title"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (doc_type.title)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(doc_type, "title", $event.target.value)
        }
      }
    }), _vm._v(" "), (_vm.addDocForm.errors.hasOwnProperty('doc_types.' + index + '.title')) ? _c('p', {
      staticClass: "help-block"
    }, [_vm._v("The name field is required.")]) : _vm._e()])]), _vm._v(" "), _c('div', {
      staticClass: "form-group",
      class: {
        'has-error': _vm.addDocForm.errors.hasOwnProperty('doc_types.' + index + '.title_cn')
      }
    }, [_c('div', {
      staticClass: "col-md-2"
    }, [_vm._v(" ")]), _vm._v(" "), _c('label', {
      staticClass: "col-md-2 control-label"
    }, [_vm._v("\n\t\t\t        Name CN:\n\t\t        ")]), _vm._v(" "), _c('div', {
      staticClass: "col-md-8"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (doc_type.title_cn),
        expression: "doc_type.title_cn"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (doc_type.title_cn)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(doc_type, "title_cn", $event.target.value)
        }
      }
    }), _vm._v(" "), (_vm.addDocForm.errors.hasOwnProperty('doc_types.' + index + '.title_cn')) ? _c('p', {
      staticClass: "help-block"
    }, [_vm._v("The name cn field is required.")]) : _vm._e()])])])
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Add")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3bd9bf82", module.exports)
  }
}

/***/ }),

/***/ 402:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.updateReportType($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.updateReportTypeForm.errors.hasOwnProperty('title')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Title:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.updateReportTypeForm.title),
      expression: "updateReportTypeForm.title"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "title"
    },
    domProps: {
      "value": (_vm.updateReportTypeForm.title)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.updateReportTypeForm, "title", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.updateReportTypeForm.errors.hasOwnProperty('title')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.updateReportTypeForm.errors.title[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.updateReportTypeForm.errors.hasOwnProperty('title_cn')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Title CN:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.updateReportTypeForm.title_cn),
      expression: "updateReportTypeForm.title_cn"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "title_cn"
    },
    domProps: {
      "value": (_vm.updateReportTypeForm.title_cn)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.updateReportTypeForm, "title_cn", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.updateReportTypeForm.errors.hasOwnProperty('title_cn')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.updateReportTypeForm.errors.title_cn[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.updateReportTypeForm.errors.hasOwnProperty('description')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Description:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.updateReportTypeForm.description),
      expression: "updateReportTypeForm.description"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "description"
    },
    domProps: {
      "value": (_vm.updateReportTypeForm.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.updateReportTypeForm, "description", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.updateReportTypeForm.errors.hasOwnProperty('description')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.updateReportTypeForm.errors.description[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.updateReportTypeForm.errors.hasOwnProperty('services_id')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Services:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-services",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "data-placeholder": "Select Service",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.services), function(service) {
    return _c('option', {
      domProps: {
        "value": service.id
      }
    }, [_vm._v("\n\t                " + _vm._s((service.detail).trim()) + "\n\t            ")])
  }), 0), _vm._v(" "), (_vm.updateReportTypeForm.errors.hasOwnProperty('services_id')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.updateReportTypeForm.errors.services_id[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        With Docs:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.updateReportTypeForm.with_docs),
      expression: "updateReportTypeForm.with_docs"
    }],
    attrs: {
      "type": "checkbox",
      "name": "with_docs"
    },
    domProps: {
      "checked": Array.isArray(_vm.updateReportTypeForm.with_docs) ? _vm._i(_vm.updateReportTypeForm.with_docs, null) > -1 : (_vm.updateReportTypeForm.with_docs)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.updateReportTypeForm.with_docs,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = null,
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.updateReportTypeForm, "with_docs", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.updateReportTypeForm, "with_docs", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.updateReportTypeForm, "with_docs", $$c)
        }
      }
    }
  })])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Update")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-61070601", module.exports)
  }
}

/***/ }),

/***/ 407:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.updateDocument($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.updateDocumentForm.errors.hasOwnProperty('title')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Title:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.updateDocumentForm.title),
      expression: "updateDocumentForm.title"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "title"
    },
    domProps: {
      "value": (_vm.updateDocumentForm.title)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.updateDocumentForm, "title", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.updateDocumentForm.errors.hasOwnProperty('title')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.updateDocumentForm.errors.title[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.updateDocumentForm.errors.hasOwnProperty('title_cn')
    }
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t        Title CN:\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.updateDocumentForm.title_cn),
      expression: "updateDocumentForm.title_cn"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "title_cn"
    },
    domProps: {
      "value": (_vm.updateDocumentForm.title_cn)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.updateDocumentForm, "title_cn", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.updateDocumentForm.errors.hasOwnProperty('title_cn')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.updateDocumentForm.errors.title_cn[0])
    }
  }) : _vm._e()])]), _vm._v(" "), _c('hr'), _vm._v(" "), _c('button', {
    staticClass: "btn pull-left",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.addDocumentType
    }
  }, [_c('i', {
    staticClass: "fa fa-plus"
  }), _vm._v(" Add Document Type\n\t\t")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _vm._l((_vm.updateDocumentForm.doc_types), function(doc_type, index) {
    return _c('div', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (doc_type.id),
        expression: "doc_type.id"
      }],
      attrs: {
        "type": "hidden"
      },
      domProps: {
        "value": (doc_type.id)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(doc_type, "id", $event.target.value)
        }
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "form-group",
      class: {
        'has-error': _vm.updateDocumentForm.errors.hasOwnProperty('doc_types.' + index + '.title')
      }
    }, [_c('div', {
      staticClass: "col-md-2"
    }, [_c('button', {
      staticClass: "btn btn-danger pull-right",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.removeDocumentType(index)
        }
      }
    }, [_c('i', {
      staticClass: "fa fa-times"
    })])]), _vm._v(" "), _c('label', {
      staticClass: "col-md-2 control-label"
    }, [_vm._v("\n\t\t\t        Name:\n\t\t        ")]), _vm._v(" "), _c('div', {
      staticClass: "col-md-8"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (doc_type.title),
        expression: "doc_type.title"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (doc_type.title)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(doc_type, "title", $event.target.value)
        }
      }
    }), _vm._v(" "), (_vm.updateDocumentForm.errors.hasOwnProperty('doc_types.' + index + '.title')) ? _c('p', {
      staticClass: "help-block"
    }, [_vm._v("The name field is required.")]) : _vm._e()])]), _vm._v(" "), _c('div', {
      staticClass: "form-group",
      class: {
        'has-error': _vm.updateDocumentForm.errors.hasOwnProperty('doc_types.' + index + '.title_cn')
      }
    }, [_c('div', {
      staticClass: "col-md-2"
    }, [_vm._v(" ")]), _vm._v(" "), _c('label', {
      staticClass: "col-md-2 control-label"
    }, [_vm._v("\n\t\t\t        Name CN:\n\t\t        ")]), _vm._v(" "), _c('div', {
      staticClass: "col-md-8"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (doc_type.title_cn),
        expression: "doc_type.title_cn"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (doc_type.title_cn)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(doc_type, "title_cn", $event.target.value)
        }
      }
    }), _vm._v(" "), (_vm.updateDocumentForm.errors.hasOwnProperty('doc_types.' + index + '.title_cn')) ? _c('p', {
      staticClass: "help-block"
    }, [_vm._v("The name cn field is required.")]) : _vm._e()])])])
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Update")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-72833664", module.exports)
  }
}

/***/ }),

/***/ 413:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.deleteDocument($event)
      }
    }
  }, [_c('p', [_vm._v("Are you sure you want to delete this document?")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Delete")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-948322a0", module.exports)
  }
}

/***/ }),

/***/ 423:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.deleteReportType($event)
      }
    }
  }, [_c('p', [_vm._v("Are you sure you want to delete this report type?")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Delete")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-dfa7c13a", module.exports)
  }
}

/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(292);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("3088dde4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-0692f896\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddReportType.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-0692f896\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddReportType.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 438:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(300);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("1ebc78e6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-3bd9bf82\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddDocument.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-3bd9bf82\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddDocument.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 441:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(303);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("30445fca", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-61070601\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UpdateReportType.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-61070601\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UpdateReportType.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 444:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(306);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("21c0aa5c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-72833664\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UpdateDocument.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-72833664\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UpdateDocument.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(309);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("79c772c8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-948322a0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeleteDocument.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-948322a0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeleteDocument.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 452:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(314);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("0f4ab438", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-dfa7c13a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeleteReportType.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-dfa7c13a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeleteReportType.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 484:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(194);


/***/ }),

/***/ 7:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9ub3RpZmljYXRpb25zL2J1aWxkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vfi9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qcz9kYTA0KiIsIndlYnBhY2s6Ly8vQWRkRG9jdW1lbnQudnVlIiwid2VicGFjazovLy9BZGRSZXBvcnRUeXBlLnZ1ZSIsIndlYnBhY2s6Ly8vRGVsZXRlRG9jdW1lbnQudnVlIiwid2VicGFjazovLy9EZWxldGVSZXBvcnRUeXBlLnZ1ZSIsIndlYnBhY2s6Ly8vVXBkYXRlRG9jdW1lbnQudnVlIiwid2VicGFjazovLy9VcGRhdGVSZXBvcnRUeXBlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkUmVwb3J0VHlwZS52dWU/MzE2ZSIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qcz82YjJiKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRG9jdW1lbnQudnVlPzJhNDciLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZVJlcG9ydFR5cGUudnVlPzFiMGEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZURvY3VtZW50LnZ1ZT9kYzA0Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVEb2N1bWVudC52dWU/MWIzYyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlUmVwb3J0VHlwZS52dWU/MDAxNyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRG9jdW1lbnQudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGRSZXBvcnRUeXBlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlRG9jdW1lbnQudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVSZXBvcnRUeXBlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvVXBkYXRlRG9jdW1lbnQudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9VcGRhdGVSZXBvcnRUeXBlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkUmVwb3J0VHlwZS52dWU/M2ZhYSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRG9jdW1lbnQudnVlPzJhNDEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZVJlcG9ydFR5cGUudnVlPzBkOGIiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZURvY3VtZW50LnZ1ZT82NWVhIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVEb2N1bWVudC52dWU/ZTA3MCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlUmVwb3J0VHlwZS52dWU/NjFjNCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkUmVwb3J0VHlwZS52dWU/Yzk1MyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRG9jdW1lbnQudnVlP2QwNzciLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZVJlcG9ydFR5cGUudnVlP2FhZjIiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZURvY3VtZW50LnZ1ZT85MDE4Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVEb2N1bWVudC52dWU/NjM1ZiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlUmVwb3J0VHlwZS52dWU/ZThkNyIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qcz9lNmFjKiJdLCJuYW1lcyI6WyJWdWUiLCJlbCIsImRhdGEiLCJyZXBvcnRUeXBlcyIsImRvY3MiLCJzZWxlY3RlZFJlcG9ydFR5cGVJZCIsInNlbGVjdGVkRG9jdW1lbnRJZCIsIm1ldGhvZHMiLCJsb2FkUmVwb3J0VHlwZXMiLCJheGlvcyIsImdldCIsInRoZW4iLCJyZXNwb25zZSIsImNhbGxEYXRhVGFibGUiLCJjYXRjaCIsImNvbnNvbGUiLCJsb2ciLCJlcnJvciIsImxvYWREb2NzIiwidGFibGVJZCIsIiQiLCJEYXRhVGFibGUiLCJkZXN0cm95Iiwic2V0VGltZW91dCIsInJlc3BvbnNpdmUiLCJzZXRTZWxlY3RlZFJlcG9ydFR5cGVJZCIsInZhbCIsInNldFNlbGVjdGVkUmVwb3J0VHlwZSIsIiRyZWZzIiwidXBkYXRlcmVwb3J0dHlwZXJlZiIsInVwZGF0ZVJlcG9ydFR5cGVGb3JtIiwidGl0bGUiLCJ0aXRsZV9jbiIsImRlc2NyaXB0aW9uIiwic2VydmljZXNfaWQiLCJzcGxpdCIsInVwZGF0ZUNob3NlbiIsIndpdGhfZG9jcyIsInNldFNlbGVjdGVkRG9jdW1lbnRJZCIsInNldFNlbGVjdGVkRG9jdW1lbnQiLCJ1cGRhdGVkb2N1bWVudHJlZiIsInVwZGF0ZURvY3VtZW50Rm9ybSIsImRvY190eXBlcyIsImNyZWF0ZWQiLCJjb21wb25lbnRzIiwicmVxdWlyZSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBLElBQUlBLEdBQUosQ0FBUTs7QUFFUEMsS0FBSSx1QkFGRzs7QUFJUEMsT0FBTTs7QUFFTEMsZUFBYSxFQUZSO0FBR0xDLFFBQU0sRUFIRDs7QUFLTEMsd0JBQXNCLElBTGpCOztBQU9MQyxzQkFBb0I7O0FBUGYsRUFKQzs7QUFlUEMsVUFBUztBQUVSQyxpQkFGUSw2QkFFVTtBQUFBOztBQUNqQkMsU0FBTUMsR0FBTixDQUFVLGdDQUFWLEVBQ0VDLElBREYsQ0FDTyxvQkFBWTtBQUNqQixVQUFLUixXQUFMLEdBQW1CUyxTQUFTVixJQUE1Qjs7QUFFQSxVQUFLVyxhQUFMLENBQW1CLG9CQUFuQjtBQUNBLElBTEYsRUFNRUMsS0FORixDQU1RO0FBQUEsV0FBU0MsUUFBUUMsR0FBUixDQUFZQyxLQUFaLENBQVQ7QUFBQSxJQU5SO0FBT0EsR0FWTztBQVlSQyxVQVpRLHNCQVlHO0FBQUE7O0FBQ1ZULFNBQU1DLEdBQU4sQ0FBVSx3QkFBVixFQUNFQyxJQURGLENBQ08sb0JBQVk7QUFDakIsV0FBS1AsSUFBTCxHQUFZUSxTQUFTVixJQUFyQjs7QUFFQSxXQUFLVyxhQUFMLENBQW1CLFlBQW5CO0FBQ0EsSUFMRixFQU1FQyxLQU5GLENBTVE7QUFBQSxXQUFTQyxRQUFRQyxHQUFSLENBQVlDLEtBQVosQ0FBVDtBQUFBLElBTlI7QUFPQSxHQXBCTztBQXNCUkosZUF0QlEseUJBc0JNTSxPQXRCTixFQXNCYztBQUNmQyxLQUFFLE1BQU1ELE9BQVIsRUFBaUJFLFNBQWpCLEdBQTZCQyxPQUE3Qjs7QUFFQUMsY0FBVyxZQUFNO0FBQ2hCSCxNQUFFLE1BQU1ELE9BQVIsRUFBaUJFLFNBQWpCLENBQTJCO0FBQ3BCRyxpQkFBWSxJQURRO0FBRXBCLG1CQUFjLENBQUMsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBRCxFQUFlLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULENBQWYsQ0FGTTtBQUdwQix1QkFBa0I7QUFIRSxLQUEzQjtBQUtBLElBTkQsRUFNRyxHQU5IO0FBT0EsR0FoQ0M7QUFrQ0ZDLHlCQWxDRSxtQ0FrQ3NCQyxHQWxDdEIsRUFrQzJCO0FBQzVCLFFBQUtyQixvQkFBTCxHQUE0QnFCLEdBQTVCO0FBQ0EsR0FwQ0M7QUFzQ0ZDLHVCQXRDRSxpQ0FzQ29CRCxHQXRDcEIsRUFzQ3lCO0FBQzFCLFFBQUtFLEtBQUwsQ0FBV0MsbUJBQVgsQ0FBK0JDLG9CQUEvQixDQUFvREMsS0FBcEQsR0FBNERMLElBQUlLLEtBQWhFO0FBQ0EsUUFBS0gsS0FBTCxDQUFXQyxtQkFBWCxDQUErQkMsb0JBQS9CLENBQW9ERSxRQUFwRCxHQUErRE4sSUFBSU0sUUFBbkU7QUFDQSxRQUFLSixLQUFMLENBQVdDLG1CQUFYLENBQStCQyxvQkFBL0IsQ0FBb0RHLFdBQXBELEdBQWtFUCxJQUFJTyxXQUF0RTtBQUNBLFFBQUtMLEtBQUwsQ0FBV0MsbUJBQVgsQ0FBK0JDLG9CQUEvQixDQUFvREksV0FBcEQsR0FBbUVSLElBQUlRLFdBQUwsQ0FBa0JDLEtBQWxCLENBQXdCLEdBQXhCLENBQWxFO0FBQ0EsUUFBS1AsS0FBTCxDQUFXQyxtQkFBWCxDQUErQk8sWUFBL0IsQ0FBNkNWLElBQUlRLFdBQUwsQ0FBa0JDLEtBQWxCLENBQXdCLEdBQXhCLENBQTVDO0FBQ0EsUUFBS1AsS0FBTCxDQUFXQyxtQkFBWCxDQUErQkMsb0JBQS9CLENBQW9ETyxTQUFwRCxHQUFpRVgsSUFBSVcsU0FBSixJQUFpQixDQUFsQixHQUF1QixJQUF2QixHQUE4QixLQUE5RjtBQUNBLEdBN0NDO0FBK0NGQyx1QkEvQ0UsaUNBK0NvQlosR0EvQ3BCLEVBK0N5QjtBQUMxQixRQUFLcEIsa0JBQUwsR0FBMEJvQixHQUExQjtBQUNBLEdBakRDO0FBbURGYSxxQkFuREUsK0JBbURrQmIsR0FuRGxCLEVBbUR1QjtBQUN4QixRQUFLRSxLQUFMLENBQVdZLGlCQUFYLENBQTZCQyxrQkFBN0IsQ0FBZ0RWLEtBQWhELEdBQXdETCxJQUFJSyxLQUE1RDtBQUNBLFFBQUtILEtBQUwsQ0FBV1ksaUJBQVgsQ0FBNkJDLGtCQUE3QixDQUFnRFQsUUFBaEQsR0FBMkROLElBQUlNLFFBQS9EO0FBQ0EsUUFBS0osS0FBTCxDQUFXWSxpQkFBWCxDQUE2QkMsa0JBQTdCLENBQWdEQyxTQUFoRCxHQUE0RGhCLElBQUlnQixTQUFoRTtBQUNBO0FBdkRDLEVBZkY7O0FBMEVQQyxRQTFFTyxxQkEwRUc7QUFDVCxPQUFLbkMsZUFBTDs7QUFFQSxPQUFLVSxRQUFMO0FBQ0EsRUE5RU07OztBQWdGUDBCLGFBQVk7QUFDWCxxQkFBbUJDLG1CQUFPQSxDQUFDLEdBQVIsQ0FEUjtBQUVYLHdCQUFzQkEsbUJBQU9BLENBQUMsR0FBUixDQUZYO0FBR1gsd0JBQXNCQSxtQkFBT0EsQ0FBQyxHQUFSLENBSFg7O0FBS1gsa0JBQWdCQSxtQkFBT0EsQ0FBQyxHQUFSLENBTEw7QUFNWCxxQkFBbUJBLG1CQUFPQSxDQUFDLEdBQVIsQ0FOUjtBQU9YLHFCQUFtQkEsbUJBQU9BLENBQUMsR0FBUjtBQVBSOztBQWhGTCxDQUFSLEU7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsaUJBQWlCO0FBQ2pDO0FBQ0E7QUFDQSx3Q0FBd0MsZ0JBQWdCO0FBQ3hELElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsaUJBQWlCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvQkFBb0I7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNzQkE7QUFFQSxLQUZBLGtCQUVBO0FBQ0E7QUFDQTtBQUNBLGFBREE7QUFFQSxnQkFGQTtBQUdBLGlCQUhBO0FBSUE7QUFKQTtBQURBO0FBUUEsRUFYQTs7O0FBYUE7QUFFQSxpQkFGQSw2QkFFQTtBQUNBO0FBQ0EsR0FKQTtBQU1BLG9CQU5BLDhCQU1BLEtBTkEsRUFNQTtBQUNBO0FBQ0EsR0FSQTtBQVVBLFFBVkEsb0JBVUE7QUFBQTs7QUFDQTtBQUNBLGdDQURBO0FBRUEsc0NBRkE7QUFHQTtBQUhBLE1BS0EsSUFMQSxDQUtBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsSUFmQSxFQWdCQSxLQWhCQSxDQWdCQTtBQUFBO0FBQUEsSUFoQkE7QUFpQkEsR0E1QkE7QUErQkEsaUJBL0JBLDZCQStCQTtBQUNBO0FBQ0EsYUFEQTtBQUVBLGdCQUZBO0FBR0EsaUJBSEE7QUFJQTtBQUpBO0FBTUE7QUF0Q0E7O0FBYkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTEE7QUFFQSxLQUZBLGtCQUVBO0FBQ0E7QUFDQSxlQURBOztBQUdBO0FBQ0EsYUFEQTtBQUVBLGdCQUZBO0FBR0EsbUJBSEE7QUFJQSxtQkFKQTtBQUtBLG9CQUxBO0FBTUE7QUFOQTtBQUhBO0FBWUEsRUFmQTs7O0FBaUJBO0FBRUEsZUFGQSwyQkFFQTtBQUFBOztBQUNBO0FBQ0EsdUNBREE7QUFFQSw2Q0FGQTtBQUdBLG1EQUhBO0FBSUEsbURBSkE7QUFLQTtBQUxBLE1BT0EsSUFQQSxDQU9BO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsSUFqQkEsRUFrQkEsS0FsQkEsQ0FrQkE7QUFBQTtBQUFBLElBbEJBO0FBbUJBLEdBdEJBO0FBd0JBLHdCQXhCQSxvQ0F3QkE7QUFDQTtBQUNBLGFBREE7QUFFQSxnQkFGQTtBQUdBLG1CQUhBO0FBSUEsbUJBSkE7QUFLQSxvQkFMQTtBQU1BOztBQUdBO0FBVEEsS0FVQTtBQUNBLEdBcENBO0FBc0NBLGNBdENBLDBCQXNDQTtBQUFBOztBQUNBLDRDQUNBLElBREEsQ0FDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUZBLEVBRUEsR0FGQTtBQUdBLElBUEEsRUFRQSxLQVJBLENBUUE7QUFBQTtBQUFBLElBUkE7QUFTQSxHQWhEQTtBQWtEQSxrQkFsREEsOEJBa0RBO0FBQ0E7QUFDQSxpQkFEQTtBQUVBLHlDQUZBO0FBR0E7QUFIQTs7QUFNQTs7QUFFQTtBQUNBO0FBQ0EsSUFGQTtBQUdBO0FBOURBLEVBakJBOztBQW1GQSxRQW5GQSxxQkFtRkE7QUFDQTtBQUNBLEVBckZBO0FBdUZBLFFBdkZBLHFCQXVGQTtBQUNBO0FBQ0E7QUF6RkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkRBOztBQUVBLHNCQUZBOztBQUlBO0FBRUEsZ0JBRkEsNEJBRUE7QUFBQTs7QUFDQSw2REFDQSxJQURBLENBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxJQVhBLEVBWUEsS0FaQSxDQVlBO0FBQUE7QUFBQSxJQVpBO0FBYUE7QUFoQkE7O0FBSkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7O0FBRUEsd0JBRkE7O0FBSUE7QUFFQSxrQkFGQSw4QkFFQTtBQUFBOztBQUNBLHVFQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLElBWEEsRUFZQSxLQVpBLENBWUE7QUFBQTtBQUFBLElBWkE7QUFhQTtBQWhCQTs7QUFKQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzJEQTs7QUFFQSxzQkFGQTs7QUFJQSxLQUpBLGtCQUlBO0FBQ0E7QUFDQTtBQUNBLGFBREE7QUFFQSxnQkFGQTtBQUdBLGlCQUhBO0FBSUE7QUFKQTtBQURBO0FBUUEsRUFiQTs7O0FBZUE7QUFFQSxpQkFGQSw2QkFFQTtBQUNBO0FBQ0EsR0FKQTtBQU1BLG9CQU5BLDhCQU1BLEtBTkEsRUFNQTtBQUNBO0FBQ0EsR0FSQTtBQVVBLGdCQVZBLDRCQVVBO0FBQUE7O0FBQ0E7QUFDQSx3Q0FEQTtBQUVBLDhDQUZBO0FBR0E7QUFIQSxNQUtBLElBTEEsQ0FLQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLElBZkEsRUFnQkEsS0FoQkEsQ0FnQkE7QUFBQTtBQUFBLElBaEJBO0FBaUJBLEdBNUJBO0FBOEJBLHlCQTlCQSxxQ0E4QkE7QUFDQTtBQUNBLGFBREE7QUFFQSxnQkFGQTtBQUdBLGlCQUhBO0FBSUE7QUFKQTtBQU1BO0FBckNBOztBQWZBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1JBOztBQUVBLHdCQUZBOztBQUlBLEtBSkEsa0JBSUE7QUFDQTtBQUNBLGVBREE7O0FBR0E7QUFDQSxhQURBO0FBRUEsZ0JBRkE7QUFHQSxtQkFIQTtBQUlBLG1CQUpBO0FBS0Esb0JBTEE7QUFNQTtBQU5BO0FBSEE7QUFZQSxFQWpCQTs7O0FBbUJBO0FBRUEsa0JBRkEsOEJBRUE7QUFBQTs7QUFDQTtBQUNBLDBDQURBO0FBRUEsZ0RBRkE7QUFHQSxzREFIQTtBQUlBLHNEQUpBO0FBS0E7QUFMQSxNQU9BLElBUEEsQ0FPQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLElBakJBLEVBa0JBLEtBbEJBLENBa0JBO0FBQUE7QUFBQSxJQWxCQTtBQW1CQSxHQXRCQTtBQXdCQSwyQkF4QkEsdUNBd0JBO0FBQ0E7QUFDQSxhQURBO0FBRUEsZ0JBRkE7QUFHQSxtQkFIQTtBQUlBLG1CQUpBO0FBS0Esb0JBTEE7QUFNQTs7QUFHQTtBQVRBLEtBVUE7QUFDQSxHQXBDQTtBQXNDQSxjQXRDQSwwQkFzQ0E7QUFBQTs7QUFDQSw0Q0FDQSxJQURBLENBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FGQSxFQUVBLEdBRkE7QUFHQSxJQVBBLEVBUUEsS0FSQSxDQVFBO0FBQUE7QUFBQSxJQVJBO0FBU0EsR0FoREE7QUFrREEsY0FsREEsd0JBa0RBLFVBbERBLEVBa0RBO0FBQ0E7QUFDQTtBQUNBLElBRkEsRUFFQSxHQUZBO0FBR0EsR0F0REE7QUF3REEsa0JBeERBLDhCQXdEQTtBQUNBO0FBQ0EsaUJBREE7QUFFQSx5Q0FGQTtBQUdBO0FBSEE7O0FBTUE7O0FBRUE7QUFDQTtBQUNBLElBRkE7QUFHQTtBQXBFQSxFQW5CQTs7QUEyRkEsUUEzRkEscUJBMkZBO0FBQ0E7QUFDQSxFQTdGQTtBQStGQSxRQS9GQSxxQkErRkE7QUFDQTtBQUNBO0FBakdBLEc7Ozs7Ozs7QUNsRUEsMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcsNkNBQTZDLHFCQUFxQixHQUFHLFVBQVUsNkVBQTZFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxvTEFBb0wsZ0VBQWdFLGtjQUFrYyxtRUFBbUUsaWRBQWlkLHNFQUFzRSxnZUFBZ2Usc0VBQXNFLDZQQUE2UCx3SEFBd0gsMkJBQTJCLDR3QkFBNHdCLGdCQUFnQixrQkFBa0IsMkRBQTJELHFLQUFxSyxXQUFXLFNBQVMsbUJBQW1CLDJCQUEyQix3REFBd0QsNFNBQTRTLGdDQUFnQyx5Q0FBeUMsOENBQThDLGlFQUFpRSxvRUFBb0UsZ0VBQWdFLHVCQUF1QixhQUFhLGtGQUFrRixTQUFTLHFDQUFxQyxvQ0FBb0MscUtBQXFLLHdHQUF3RyxTQUFTLDJCQUEyQixpRkFBaUYsNENBQTRDLGtDQUFrQywrRUFBK0UsZUFBZSxPQUFPLGFBQWEsaURBQWlELFNBQVMsK0JBQStCLHVEQUF1RCw0SEFBNEgsRUFBRSw4QkFBOEIsc0VBQXNFLDZEQUE2RCxXQUFXLEVBQUUsYUFBYSxTQUFTLG9CQUFvQiw0QkFBNEIsT0FBTyxvQkFBb0Isb0NBQW9DLFNBQVMsT0FBTyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyw4QkFBOEIsdUJBQXVCLEtBQUssYUFBYSxHOzs7Ozs7O0FDRHR5TDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSxpQkFBaUI7QUFDM0I7QUFDQTs7QUFFQSxtQkFBbUIsbUJBQU8sQ0FBQyxDQUFnQjs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsbUJBQW1CLHNCQUFzQjtBQUN6QztBQUNBO0FBQ0EsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsMkJBQTJCO0FBQ2hEO0FBQ0E7QUFDQSxZQUFZLHVCQUF1QjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EscUJBQXFCLHVCQUF1QjtBQUM1QztBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUN0TkEsMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcsVUFBVSwyRUFBMkUsTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLDJLQUEySyx5REFBeUQsNmFBQTZhLDREQUE0RCwybEJBQTJsQixZQUFZLDBIQUEwSCw2RUFBNkUsNHJCQUE0ckIsZ0ZBQWdGLCtDQUErQyx1ZEFBdWQsWUFBWSwyUUFBMlEsZ0JBQWdCLGtCQUFrQix5QkFBeUIsMEdBQTBHLFdBQVcsU0FBUyxtQkFBbUIsNkJBQTZCLDBDQUEwQywwQkFBMEIsRUFBRSxTQUFTLHNDQUFzQyxxREFBcUQsU0FBUyxxQkFBcUIsZ0RBQWdELDJKQUEySixnQ0FBZ0MseUNBQXlDLHVDQUF1QywwREFBMEQsaUVBQWlFLGdFQUFnRSx1QkFBdUIsYUFBYSwyRUFBMkUsU0FBUyxnQ0FBZ0MsNkJBQTZCLDJHQUEyRyxTQUFTLFdBQVcsT0FBTyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEcm9KLDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsZ0ZBQWdGLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVywwTEFBMEwsbUVBQW1FLDJjQUEyYyxzRUFBc0UsMGRBQTBkLHlFQUF5RSx5ZUFBeWUseUVBQXlFLDZQQUE2UCx3SEFBd0gsMkJBQTJCLHd4QkFBd3hCLGdEQUFnRCxrQkFBa0IsOERBQThELHFLQUFxSyxXQUFXLFNBQVMscUJBQXFCLGdDQUFnQyw4RUFBOEUsMlRBQTJULGdDQUFnQyx5Q0FBeUMsaURBQWlELGlFQUFpRSx1RUFBdUUsZ0VBQWdFLHVCQUF1QixhQUFhLHFGQUFxRixTQUFTLDBDQUEwQyx1Q0FBdUMscUtBQXFLLHdHQUF3RyxTQUFTLDZCQUE2QixpRkFBaUYsNENBQTRDLGtDQUFrQywrRUFBK0UsZUFBZSxPQUFPLGFBQWEsaURBQWlELFNBQVMscUNBQXFDLDRCQUE0QiwyRkFBMkYsV0FBVyxPQUFPLFNBQVMsK0JBQStCLHVEQUF1RCw0SEFBNEgsRUFBRSw4QkFBOEIsc0VBQXNFLGdFQUFnRSxXQUFXLEVBQUUsYUFBYSxXQUFXLHNCQUFzQiw0QkFBNEIsT0FBTyxvQkFBb0Isb0NBQW9DLFNBQVMsT0FBTyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEOStMLDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsOEVBQThFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyxzTEFBc0wsaUVBQWlFLHFjQUFxYyxvRUFBb0UsbW5CQUFtbkIsWUFBWSwrTEFBK0wscUZBQXFGLG9zQkFBb3NCLHdGQUF3RiwrQ0FBK0MsK2RBQStkLFlBQVksOFFBQThRLDhDQUE4QyxrQkFBa0IsaUNBQWlDLDBHQUEwRyxXQUFXLFNBQVMscUJBQXFCLCtCQUErQixrREFBa0Qsa0NBQWtDLEVBQUUsU0FBUyxzQ0FBc0MsNkRBQTZELFNBQVMsK0JBQStCLG9FQUFvRSxtTEFBbUwsZ0NBQWdDLHlDQUF5QywrQ0FBK0MsMERBQTBELG9FQUFvRSxnRUFBZ0UsdUJBQXVCLGFBQWEsbUZBQW1GLFNBQVMsd0NBQXdDLHFDQUFxQywwR0FBMEcsU0FBUyxZQUFZLE9BQU8seUNBQXlDLDBCQUEwQixLQUFLLGFBQWEseUJBQXlCLEtBQUssYUFBYSxHOzs7Ozs7O0FDRHQ5SiwyQkFBMkIsbUJBQU8sQ0FBQyxDQUEyRDtBQUM5RixjQUFjLFFBQVMsNEJBQTRCLHdCQUF3QixHQUFHLDRCQUE0Qix1QkFBdUIsR0FBRyxVQUFVLDhFQUE4RSxNQUFNLFdBQVcsS0FBSyxLQUFLLFdBQVcsNGNBQTRjLGdEQUFnRCw0QkFBNEIsa0dBQWtHLHlDQUF5QyxpRUFBaUUsMERBQTBELG9FQUFvRSxnRUFBZ0UsdUJBQXVCLGFBQWEsaURBQWlELFNBQVMsU0FBUyxPQUFPLHlDQUF5QywwQkFBMEIsS0FBSyxhQUFhLHlCQUF5QixLQUFLLGFBQWEsRzs7Ozs7OztBQ0RsNUMsMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcsVUFBVSxnRkFBZ0YsTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLG1kQUFtZCxrREFBa0QsOEJBQThCLDRHQUE0Ryx5Q0FBeUMsbUVBQW1FLGlFQUFpRSx1RUFBdUUsZ0VBQWdFLHVCQUF1QixhQUFhLGlEQUFpRCxTQUFTLFNBQVMsT0FBTyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7O0FDQXI3QztBQUNBLG1CQUFPLENBQUMsR0FBK1E7O0FBRXZSLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXlPO0FBQ25QO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXFNO0FBQy9NO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7QUM5QkE7QUFDQSxtQkFBTyxDQUFDLEdBQWlSOztBQUV6UixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUEyTztBQUNyUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUF1TTtBQUNqTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7O0FDOUJBO0FBQ0EsbUJBQU8sQ0FBQyxHQUFrUjs7QUFFMVIsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBNE87QUFDdFA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBd007QUFDbE47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7OztBQzlCQTtBQUNBLG1CQUFPLENBQUMsR0FBb1I7O0FBRTVSLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQThPO0FBQ3hQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTBNO0FBQ3BOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7QUM5QkE7QUFDQSxtQkFBTyxDQUFDLEdBQWtSOztBQUUxUixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUE0TztBQUN0UDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUF3TTtBQUNsTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7O0FDOUJBO0FBQ0EsbUJBQU8sQ0FBQyxHQUFvUjs7QUFFNVIsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBOE87QUFDeFA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBME07QUFDcE47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDL0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDek1BLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0Esd0NBQXdDLFFBQVE7QUFDaEQ7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLHdDQUF3QyxRQUFRO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN0TUEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN6TUEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0Esd0NBQXdDLFFBQVE7QUFDaEQ7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLHdDQUF3QyxRQUFRO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSx3Q0FBd0MsUUFBUTtBQUNoRDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDek5BLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQzVCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM1QkE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBOFM7QUFDcFUsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7O0FDcEJBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEdBQTRTO0FBQ2xVLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxHQUFpVDtBQUN2VSw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLENBQXlFO0FBQzlGO0FBQ0EsR0FBRyxLQUFVO0FBQ2I7QUFDQTtBQUNBLDRKQUE0SixvRUFBb0U7QUFDaE8scUtBQXFLLG9FQUFvRTtBQUN6TztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7Ozs7QUNwQkE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBK1M7QUFDclUsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7O0FDcEJBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEdBQStTO0FBQ3JVLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxHQUFpVDtBQUN2VSw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLENBQXlFO0FBQzlGO0FBQ0EsR0FBRyxLQUFVO0FBQ2I7QUFDQTtBQUNBLDRKQUE0SixvRUFBb0U7QUFDaE8scUtBQXFLLG9FQUFvRTtBQUN6TztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7Ozs7Ozs7Ozs7OztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixpQkFBaUI7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLHdCQUF3QjtBQUMzRCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJqcy92aXNhL25vdGlmaWNhdGlvbnMvYnVpbGRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDg0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmMTEzZWQzNmE1YTU2YjdkY2Y2MyIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDE4IDE5IDIwIDIxIDIyIDIzIDI0IDI1IiwibmV3IFZ1ZSh7XG5cblx0ZWw6ICcjbm90aWZpY2F0aW9uLWJ1aWxkZXInLFxuXG5cdGRhdGE6IHtcblxuXHRcdHJlcG9ydFR5cGVzOiBbXSxcblx0XHRkb2NzOiBbXSxcblxuXHRcdHNlbGVjdGVkUmVwb3J0VHlwZUlkOiBudWxsLFxuXG5cdFx0c2VsZWN0ZWREb2N1bWVudElkOiBudWxsLFxuXG5cdH0sXG5cblx0bWV0aG9kczoge1xuXG5cdFx0bG9hZFJlcG9ydFR5cGVzKCkge1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9ub3RpZmljYXRpb24vcmVwb3J0LXR5cGUnKVxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0dGhpcy5yZXBvcnRUeXBlcyA9IHJlc3BvbnNlLmRhdGE7XG5cblx0XHRcdFx0XHR0aGlzLmNhbGxEYXRhVGFibGUoJ3JlcG9ydC10eXBlcy10YWJsZScpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHR9LFxuXG5cdFx0bG9hZERvY3MoKSB7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL25vdGlmaWNhdGlvbi9kb2MnKVxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0dGhpcy5kb2NzID0gcmVzcG9uc2UuZGF0YTtcblxuXHRcdFx0XHRcdHRoaXMuY2FsbERhdGFUYWJsZSgnZG9jcy10YWJsZScpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHR9LFxuXG5cdFx0Y2FsbERhdGFUYWJsZSh0YWJsZUlkKXtcbiAgICAgICAgXHQkKCcjJyArIHRhYmxlSWQpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuICAgICAgICBcdHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICBcdFx0JCgnIycgKyB0YWJsZUlkKS5EYXRhVGFibGUoe1xuXHQgICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0ICAgICAgICAgICAgICAgIFwibGVuZ3RoTWVudVwiOiBbWzEwLCAyNSwgNTBdLCBbMTAsIDI1LCA1MF1dLFxuXHQgICAgICAgICAgICAgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAxMFxuXHQgICAgICAgICAgICB9KTtcbiAgICAgICAgXHR9LCA1MDApO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldFNlbGVjdGVkUmVwb3J0VHlwZUlkKHZhbCkge1xuICAgICAgICBcdHRoaXMuc2VsZWN0ZWRSZXBvcnRUeXBlSWQgPSB2YWw7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0U2VsZWN0ZWRSZXBvcnRUeXBlKHZhbCkge1xuICAgICAgICBcdHRoaXMuJHJlZnMudXBkYXRlcmVwb3J0dHlwZXJlZi51cGRhdGVSZXBvcnRUeXBlRm9ybS50aXRsZSA9IHZhbC50aXRsZTtcbiAgICAgICAgXHR0aGlzLiRyZWZzLnVwZGF0ZXJlcG9ydHR5cGVyZWYudXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGVfY24gPSB2YWwudGl0bGVfY247XG4gICAgICAgIFx0dGhpcy4kcmVmcy51cGRhdGVyZXBvcnR0eXBlcmVmLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLmRlc2NyaXB0aW9uID0gdmFsLmRlc2NyaXB0aW9uO1xuICAgICAgICBcdHRoaXMuJHJlZnMudXBkYXRlcmVwb3J0dHlwZXJlZi51cGRhdGVSZXBvcnRUeXBlRm9ybS5zZXJ2aWNlc19pZCA9ICh2YWwuc2VydmljZXNfaWQpLnNwbGl0KFwiLFwiKTtcbiAgICAgICAgXHR0aGlzLiRyZWZzLnVwZGF0ZXJlcG9ydHR5cGVyZWYudXBkYXRlQ2hvc2VuKCh2YWwuc2VydmljZXNfaWQpLnNwbGl0KFwiLFwiKSk7XG4gICAgICAgIFx0dGhpcy4kcmVmcy51cGRhdGVyZXBvcnR0eXBlcmVmLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLndpdGhfZG9jcyA9ICh2YWwud2l0aF9kb2NzID09IDEpID8gdHJ1ZSA6IGZhbHNlO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldFNlbGVjdGVkRG9jdW1lbnRJZCh2YWwpIHtcbiAgICAgICAgXHR0aGlzLnNlbGVjdGVkRG9jdW1lbnRJZCA9IHZhbDtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXRTZWxlY3RlZERvY3VtZW50KHZhbCkge1xuICAgICAgICBcdHRoaXMuJHJlZnMudXBkYXRlZG9jdW1lbnRyZWYudXBkYXRlRG9jdW1lbnRGb3JtLnRpdGxlID0gdmFsLnRpdGxlO1xuICAgICAgICBcdHRoaXMuJHJlZnMudXBkYXRlZG9jdW1lbnRyZWYudXBkYXRlRG9jdW1lbnRGb3JtLnRpdGxlX2NuID0gdmFsLnRpdGxlX2NuO1xuICAgICAgICBcdHRoaXMuJHJlZnMudXBkYXRlZG9jdW1lbnRyZWYudXBkYXRlRG9jdW1lbnRGb3JtLmRvY190eXBlcyA9IHZhbC5kb2NfdHlwZXM7XG4gICAgICAgIH1cblxuXHR9LFxuXG5cdGNyZWF0ZWQoKSB7XG5cdFx0dGhpcy5sb2FkUmVwb3J0VHlwZXMoKTtcblxuXHRcdHRoaXMubG9hZERvY3MoKTtcblx0fSxcblxuXHRjb21wb25lbnRzOiB7XG5cdFx0J2FkZC1yZXBvcnQtdHlwZSc6IHJlcXVpcmUoJy4uLy4uL2NvbXBvbmVudHMvVmlzYS9BZGRSZXBvcnRUeXBlLnZ1ZScpLFxuXHRcdCd1cGRhdGUtcmVwb3J0LXR5cGUnOiByZXF1aXJlKCcuLi8uLi9jb21wb25lbnRzL1Zpc2EvVXBkYXRlUmVwb3J0VHlwZS52dWUnKSxcblx0XHQnZGVsZXRlLXJlcG9ydC10eXBlJzogcmVxdWlyZSgnLi4vLi4vY29tcG9uZW50cy9WaXNhL0RlbGV0ZVJlcG9ydFR5cGUudnVlJyksXG5cblx0XHQnYWRkLWRvY3VtZW50JzogcmVxdWlyZSgnLi4vLi4vY29tcG9uZW50cy9WaXNhL0FkZERvY3VtZW50LnZ1ZScpLFxuXHRcdCd1cGRhdGUtZG9jdW1lbnQnOiByZXF1aXJlKCcuLi8uLi9jb21wb25lbnRzL1Zpc2EvVXBkYXRlRG9jdW1lbnQudnVlJyksXG5cdFx0J2RlbGV0ZS1kb2N1bWVudCc6IHJlcXVpcmUoJy4uLy4uL2NvbXBvbmVudHMvVmlzYS9EZWxldGVEb2N1bWVudC52dWUnKSxcblx0fVxuXG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL25vdGlmaWNhdGlvbnMvYnVpbGRlci5qcyIsIi8qXHJcblx0TUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcclxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXHJcbiovXHJcbi8vIGNzcyBiYXNlIGNvZGUsIGluamVjdGVkIGJ5IHRoZSBjc3MtbG9hZGVyXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oKSB7XHJcblx0dmFyIGxpc3QgPSBbXTtcclxuXHJcblx0Ly8gcmV0dXJuIHRoZSBsaXN0IG9mIG1vZHVsZXMgYXMgY3NzIHN0cmluZ1xyXG5cdGxpc3QudG9TdHJpbmcgPSBmdW5jdGlvbiB0b1N0cmluZygpIHtcclxuXHRcdHZhciByZXN1bHQgPSBbXTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHZhciBpdGVtID0gdGhpc1tpXTtcclxuXHRcdFx0aWYoaXRlbVsyXSkge1xyXG5cdFx0XHRcdHJlc3VsdC5wdXNoKFwiQG1lZGlhIFwiICsgaXRlbVsyXSArIFwie1wiICsgaXRlbVsxXSArIFwifVwiKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXN1bHQucHVzaChpdGVtWzFdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHJlc3VsdC5qb2luKFwiXCIpO1xyXG5cdH07XHJcblxyXG5cdC8vIGltcG9ydCBhIGxpc3Qgb2YgbW9kdWxlcyBpbnRvIHRoZSBsaXN0XHJcblx0bGlzdC5pID0gZnVuY3Rpb24obW9kdWxlcywgbWVkaWFRdWVyeSkge1xyXG5cdFx0aWYodHlwZW9mIG1vZHVsZXMgPT09IFwic3RyaW5nXCIpXHJcblx0XHRcdG1vZHVsZXMgPSBbW251bGwsIG1vZHVsZXMsIFwiXCJdXTtcclxuXHRcdHZhciBhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzID0ge307XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaWQgPSB0aGlzW2ldWzBdO1xyXG5cdFx0XHRpZih0eXBlb2YgaWQgPT09IFwibnVtYmVyXCIpXHJcblx0XHRcdFx0YWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xyXG5cdFx0fVxyXG5cdFx0Zm9yKGkgPSAwOyBpIDwgbW9kdWxlcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaXRlbSA9IG1vZHVsZXNbaV07XHJcblx0XHRcdC8vIHNraXAgYWxyZWFkeSBpbXBvcnRlZCBtb2R1bGVcclxuXHRcdFx0Ly8gdGhpcyBpbXBsZW1lbnRhdGlvbiBpcyBub3QgMTAwJSBwZXJmZWN0IGZvciB3ZWlyZCBtZWRpYSBxdWVyeSBjb21iaW5hdGlvbnNcclxuXHRcdFx0Ly8gIHdoZW4gYSBtb2R1bGUgaXMgaW1wb3J0ZWQgbXVsdGlwbGUgdGltZXMgd2l0aCBkaWZmZXJlbnQgbWVkaWEgcXVlcmllcy5cclxuXHRcdFx0Ly8gIEkgaG9wZSB0aGlzIHdpbGwgbmV2ZXIgb2NjdXIgKEhleSB0aGlzIHdheSB3ZSBoYXZlIHNtYWxsZXIgYnVuZGxlcylcclxuXHRcdFx0aWYodHlwZW9mIGl0ZW1bMF0gIT09IFwibnVtYmVyXCIgfHwgIWFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaXRlbVswXV0pIHtcclxuXHRcdFx0XHRpZihtZWRpYVF1ZXJ5ICYmICFpdGVtWzJdKSB7XHJcblx0XHRcdFx0XHRpdGVtWzJdID0gbWVkaWFRdWVyeTtcclxuXHRcdFx0XHR9IGVsc2UgaWYobWVkaWFRdWVyeSkge1xyXG5cdFx0XHRcdFx0aXRlbVsyXSA9IFwiKFwiICsgaXRlbVsyXSArIFwiKSBhbmQgKFwiICsgbWVkaWFRdWVyeSArIFwiKVwiO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRsaXN0LnB1c2goaXRlbSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9O1xyXG5cdHJldHVybiBsaXN0O1xyXG59O1xyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcbi8vIG1vZHVsZSBpZCA9IDJcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgMyA0IDUgOSAxMSAxMiAxMyIsIjx0ZW1wbGF0ZT5cblx0XG5cdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCIgQHN1Ym1pdC5wcmV2ZW50PVwiYWRkRG9jXCI+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGFkZERvY0Zvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpIH1cIj5cblx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdCAgICAgICAgVGl0bGU6XG5cdCAgICAgICAgPC9sYWJlbD5cblx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cInRpdGxlXCIgdi1tb2RlbD1cImFkZERvY0Zvcm0udGl0bGVcIj5cblx0XHQgICAgXHQ8cCBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlJylcIiB2LXRleHQ9XCJhZGREb2NGb3JtLmVycm9ycy50aXRsZVswXVwiPjwvcD5cblx0XHQgICAgPC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGFkZERvY0Zvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpIH1cIj5cblx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdCAgICAgICAgVGl0bGUgQ046XG5cdCAgICAgICAgPC9sYWJlbD5cblx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cInRpdGxlX2NuXCIgdi1tb2RlbD1cImFkZERvY0Zvcm0udGl0bGVfY25cIj5cblx0XHQgICAgXHQ8cCBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlX2NuJylcIiB2LXRleHQ9XCJhZGREb2NGb3JtLmVycm9ycy50aXRsZV9jblswXVwiPjwvcD5cblx0XHQgICAgPC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8aHI+XG5cblx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIHB1bGwtbGVmdFwiIEBjbGljaz1cImFkZERvY3VtZW50VHlwZVwiPlxuXHRcdFx0XHQ8aSBjbGFzcz1cImZhIGZhLXBsdXNcIj48L2k+IEFkZCBEb2N1bWVudCBUeXBlXG5cdFx0XHQ8L2J1dHRvbj5cblxuXHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cblxuXHRcdFx0PGRpdiB2LWZvcj1cIihkb2NfdHlwZSwgaW5kZXgpIGluIGFkZERvY0Zvcm0uZG9jX3R5cGVzXCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgOmNsYXNzPVwieyAnaGFzLWVycm9yJzogYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2RvY190eXBlcy4nK2luZGV4KycudGl0bGUnKSB9XCI+XG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC0yXCI+XG5cdFx0XHRcdCAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kYW5nZXIgcHVsbC1yaWdodFwiIEBjbGljaz1cInJlbW92ZURvY3VtZW50VHlwZShpbmRleClcIj5cblx0XHRcdFx0ICAgIFx0XHQ8aSBjbGFzcz1cImZhIGZhLXRpbWVzXCI+PC9pPlxuXHRcdFx0XHQgICAgXHQ8L2J1dHRvbj5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdFx0ICAgICAgICBOYW1lOlxuXHRcdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLThcIj5cblx0XHRcdFx0ICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiB2LW1vZGVsPVwiZG9jX3R5cGUudGl0bGVcIj5cblx0XHRcdFx0ICAgIFx0PHAgY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cImFkZERvY0Zvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJytpbmRleCsnLnRpdGxlJylcIj5UaGUgbmFtZSBmaWVsZCBpcyByZXF1aXJlZC48L3A+XG5cdFx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgOmNsYXNzPVwieyAnaGFzLWVycm9yJzogYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2RvY190eXBlcy4nK2luZGV4KycudGl0bGVfY24nKSB9XCI+XG5cdFx0XHQgICAgXHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTJcIj4mbmJzcDs8L2Rpdj5cblx0XHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHRcdCAgICAgICAgTmFtZSBDTjpcblx0XHRcdCAgICAgICAgPC9sYWJlbD5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC04XCI+XG5cdFx0XHRcdCAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgdi1tb2RlbD1cImRvY190eXBlLnRpdGxlX2NuXCI+XG5cdFx0XHRcdCAgICBcdDxwIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJhZGREb2NGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZG9jX3R5cGVzLicraW5kZXgrJy50aXRsZV9jbicpXCI+VGhlIG5hbWUgY24gZmllbGQgaXMgcmVxdWlyZWQuPC9wPlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cblxuXHRcdDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIj5BZGQ8L2J1dHRvbj5cblx0ICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuXG5cdDwvZm9ybT5cblxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblx0XG5cdGV4cG9ydCBkZWZhdWx0IHtcblxuXHRcdGRhdGEoKSB7XG4gICAgXHRcdHJldHVybiB7XG4gICAgXHRcdFx0YWRkRG9jRm9ybToge1xuXHRcdFx0XHRcdHRpdGxlOiAnJyxcblx0XHRcdFx0XHR0aXRsZV9jbjogJycsXG5cdFx0XHRcdFx0ZG9jX3R5cGVzOiBbXSxcblx0XHRcdFx0XHRlcnJvcnM6ICcnXG5cdFx0XHRcdH1cbiAgICBcdFx0fVxuICAgIFx0fSxcblxuXHRcdG1ldGhvZHM6IHtcblxuXHRcdFx0YWRkRG9jdW1lbnRUeXBlKCkge1xuXHRcdFx0XHR0aGlzLmFkZERvY0Zvcm0uZG9jX3R5cGVzLnB1c2goeyB0aXRsZTogJycsIHRpdGxlX2NuOiAnJyB9KTtcblx0XHRcdH0sXG5cblx0XHRcdHJlbW92ZURvY3VtZW50VHlwZShpbmRleCkge1xuXHRcdFx0XHR0aGlzLmFkZERvY0Zvcm0uZG9jX3R5cGVzLnNwbGljZShpbmRleCwgMSk7XG5cdFx0XHR9LFxuXG5cdFx0XHRhZGREb2MoKSB7XG5cdFx0XHRcdGF4aW9zLnBvc3QoJy92aXNhL25vdGlmaWNhdGlvbi9kb2MnLCB7XG5cdFx0XHRcdFx0XHR0aXRsZTogdGhpcy5hZGREb2NGb3JtLnRpdGxlLFxuXHRcdFx0XHRcdFx0dGl0bGVfY246IHRoaXMuYWRkRG9jRm9ybS50aXRsZV9jbixcblx0XHRcdFx0XHRcdGRvY190eXBlczogdGhpcy5hZGREb2NGb3JtLmRvY190eXBlc1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YS5zdWNjZXNzKSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMuY2xlYXJBZGREb2NGb3JtKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5sb2FkRG9jcygpO1xuXG5cdFx0ICAgICAgICAgICAgICAgIFx0JCgnI2FkZC1kb2N1bWVudC1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHR0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5kYXRhLm1lc3NhZ2UpO1xuXHRcdCAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gdGhpcy5hZGREb2NGb3JtLmVycm9ycyA9IGVycm9yLnJlc3BvbnNlLmRhdGEpO1xuXHRcdFx0fSxcblxuXG5cdFx0XHRjbGVhckFkZERvY0Zvcm0oKSB7XG5cdFx0XHRcdHRoaXMuYWRkRG9jRm9ybSA9IHtcblx0XHRcdFx0XHR0aXRsZTogJycsXG5cdFx0XHRcdFx0dGl0bGVfY246ICcnLFxuXHRcdFx0XHRcdGRvY190eXBlczogW10sXG5cdFx0XHRcdFx0ZXJyb3JzOiAnJ1xuXHRcdFx0XHR9O1xuXHRcdFx0fVxuXG5cblx0XHR9XG5cblx0fVxuXG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIEFkZERvY3VtZW50LnZ1ZT8yMjk4NDUxMCIsIjx0ZW1wbGF0ZT5cblx0XG5cdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCIgQHN1Ym1pdC5wcmV2ZW50PVwiYWRkUmVwb3J0VHlwZVwiPlxuXG5cdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiBhZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlJykgfVwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBUaXRsZTpcblx0ICAgICAgICA8L2xhYmVsPlxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwidGl0bGVcIiB2LW1vZGVsPVwiYWRkUmVwb3J0VHlwZUZvcm0udGl0bGVcIj5cblx0XHQgICAgXHQ8cCBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpXCIgdi10ZXh0PVwiYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLnRpdGxlWzBdXCI+PC9wPlxuXHRcdCAgICA8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgOmNsYXNzPVwieyAnaGFzLWVycm9yJzogYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpIH1cIj5cblx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdCAgICAgICAgVGl0bGUgQ046XG5cdCAgICAgICAgPC9sYWJlbD5cblx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cInRpdGxlX2NuXCIgdi1tb2RlbD1cImFkZFJlcG9ydFR5cGVGb3JtLnRpdGxlX2NuXCI+XG5cdFx0ICAgIFx0PHAgY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cImFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGVfY24nKVwiIHYtdGV4dD1cImFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy50aXRsZV9jblswXVwiPjwvcD5cblx0XHQgICAgPC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZGVzY3JpcHRpb24nKSB9XCI+XG5cdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHQgICAgICAgIERlc2NyaXB0aW9uOlxuXHQgICAgICAgIDwvbGFiZWw+XG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJkZXNjcmlwdGlvblwiIHYtbW9kZWw9XCJhZGRSZXBvcnRUeXBlRm9ybS5kZXNjcmlwdGlvblwiPlxuXHRcdCAgICBcdDxwIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJhZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2Rlc2NyaXB0aW9uJylcIiB2LXRleHQ9XCJhZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMuZGVzY3JpcHRpb25bMF1cIj48L3A+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiBhZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3NlcnZpY2VzX2lkJykgfVwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBTZXJ2aWNlczpcblx0ICAgICAgICA8L2xhYmVsPlxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgIFx0PHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IFNlcnZpY2VcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzXCIgbXVsdGlwbGUgc3R5bGU9XCJ3aWR0aDozNTBweDtcIiB0YWJpbmRleD1cIjRcIj5cblx0XHQgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwic2VydmljZSBpbiBzZXJ2aWNlc1wiIDp2YWx1ZT1cInNlcnZpY2UuaWRcIj5cblx0XHQgICAgICAgICAgICAgICAge3sgKHNlcnZpY2UuZGV0YWlsKS50cmltKCkgfX1cblx0XHQgICAgICAgICAgICA8L29wdGlvbj5cblx0XHQgICAgICAgIDwvc2VsZWN0PlxuXHRcdCAgICAgICAgPHAgY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cImFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnc2VydmljZXNfaWQnKVwiIHYtdGV4dD1cImFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5zZXJ2aWNlc19pZFswXVwiPjwvcD5cblx0XHQgICAgPC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBXaXRoIERvY3M6XG5cdCAgICAgICAgPC9sYWJlbD5cblx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICBcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBuYW1lPVwid2l0aF9kb2NzXCIgdi1tb2RlbD1cImFkZFJlcG9ydFR5cGVGb3JtLndpdGhfZG9jc1wiPlxuXHRcdCAgICA8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIj5BZGQ8L2J1dHRvbj5cblx0ICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuXG5cdDwvZm9ybT5cblxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblx0XG5cdGV4cG9ydCBkZWZhdWx0IHtcblxuXHRcdGRhdGEoKSB7XG4gICAgXHRcdHJldHVybiB7XG4gICAgXHRcdFx0c2VydmljZXM6IFtdLFxuXG4gICAgXHRcdFx0YWRkUmVwb3J0VHlwZUZvcm06IHtcblx0XHRcdFx0XHR0aXRsZTogJycsXG5cdFx0XHRcdFx0dGl0bGVfY246ICcnLFxuXHRcdFx0XHRcdGRlc2NyaXB0aW9uOiAnJyxcblx0XHRcdFx0XHRzZXJ2aWNlc19pZDogW10sXG5cdFx0XHRcdFx0d2l0aF9kb2NzOiBmYWxzZSxcblx0XHRcdFx0XHRlcnJvcnM6ICcnXG5cdFx0XHRcdH1cbiAgICBcdFx0fVxuICAgIFx0fSxcblxuXHRcdG1ldGhvZHM6IHtcblxuXHRcdFx0YWRkUmVwb3J0VHlwZSgpIHtcblx0XHRcdFx0YXhpb3MucG9zdCgnL3Zpc2Evbm90aWZpY2F0aW9uL3JlcG9ydC10eXBlJywge1xuXHRcdFx0XHRcdFx0dGl0bGU6IHRoaXMuYWRkUmVwb3J0VHlwZUZvcm0udGl0bGUsXG5cdFx0XHRcdFx0XHR0aXRsZV9jbjogdGhpcy5hZGRSZXBvcnRUeXBlRm9ybS50aXRsZV9jbixcblx0XHRcdFx0XHRcdGRlc2NyaXB0aW9uOiB0aGlzLmFkZFJlcG9ydFR5cGVGb3JtLmRlc2NyaXB0aW9uLFxuXHRcdFx0XHRcdFx0c2VydmljZXNfaWQ6IHRoaXMuYWRkUmVwb3J0VHlwZUZvcm0uc2VydmljZXNfaWQsXG5cdFx0XHRcdFx0XHR3aXRoX2RvY3M6IHRoaXMuYWRkUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzXG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHRpZihyZXNwb25zZS5kYXRhLnN1Y2Nlc3MpIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5jbGVhckFkZFJlcG9ydFR5cGVGb3JtKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5sb2FkUmVwb3J0VHlwZXMoKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdCQoJyNhZGQtcmVwb3J0LXR5cGUtbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXG5cdFx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UuZGF0YS5tZXNzYWdlKTtcblx0XHQgICAgICAgICAgICAgICAgfVxuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmNhdGNoKGVycm9yID0+IHRoaXMuYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzID0gZXJyb3IucmVzcG9uc2UuZGF0YSk7XG5cdFx0XHR9LFxuXG5cdFx0XHRjbGVhckFkZFJlcG9ydFR5cGVGb3JtKCkge1xuXHRcdFx0XHR0aGlzLmFkZFJlcG9ydFR5cGVGb3JtID0ge1xuXHRcdFx0XHRcdHRpdGxlOiAnJyxcblx0XHRcdFx0XHR0aXRsZV9jbjogJycsXG5cdFx0XHRcdFx0ZGVzY3JpcHRpb246ICcnLFxuXHRcdFx0XHRcdHNlcnZpY2VzX2lkOiBbXSxcblx0XHRcdFx0XHR3aXRoX2RvY3M6IGZhbHNlLFxuXHRcdFx0XHRcdGVycm9yczogJydcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vIERlc2VsZWN0IEFsbFxuXHRcdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXMnKS52YWwoJycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cdFx0XHR9LFxuXG5cdFx0XHRsb2FkU2VydmljZXMoKSB7XG5cdFx0XHRcdGF4aW9zLmdldCgnL3Zpc2Evbm90aWZpY2F0aW9uL3NlcnZpY2VzJylcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHR0aGlzLnNlcnZpY2VzID0gcmVzcG9uc2UuZGF0YTtcblxuXHRcdFx0XHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdCQoXCIuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXNcIikudHJpZ2dlcihcImNob3Nlbjp1cGRhdGVkXCIpO1xuXHRcdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuXHRcdFx0fSxcblxuXHRcdFx0aW5pdENob3NlblNlbGVjdCgpIHtcblx0ICAgICAgICBcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlcycpLmNob3Nlbih7XG5cdFx0XHRcdFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdFx0XHRcdG5vX3Jlc3VsdHNfdGV4dDogXCJObyByZXN1bHQvcyBmb3VuZC5cIixcblx0XHRcdFx0XHRzZWFyY2hfY29udGFpbnM6IHRydWVcblx0XHRcdFx0fSk7XG5cblx0ICAgICAgICBcdHZhciB2bSA9IHRoaXM7XG5cblx0XHRcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzJykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdHZtLmFkZFJlcG9ydFR5cGVGb3JtLnNlcnZpY2VzX2lkID0gJCh0aGlzKS52YWwoKTtcblx0XHRcdFx0fSk7XG5cdCAgICAgICAgfVxuXG5cdFx0fSxcblxuXHRcdGNyZWF0ZWQoKSB7XG5cdFx0XHR0aGlzLmxvYWRTZXJ2aWNlcygpO1xuXHRcdH0sXG5cblx0XHRtb3VudGVkKCkge1xuICAgICAgICBcdHRoaXMuaW5pdENob3NlblNlbGVjdCgpO1xuICAgIFx0fVxuXG5cdH1cblxuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG5cdGZvcm0ge1xuXHRcdG1hcmdpbi1ib3R0b206IDMwcHg7XG5cdH1cblx0Lm0tci0xMCB7XG5cdFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xuXHR9XG5cdGlucHV0W3R5cGU9XCJjaGVja2JveFwiXSB7XG5cdFx0bWFyZ2luLXRvcDogMTVweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQWRkUmVwb3J0VHlwZS52dWU/N2MyYjE1YTgiLCI8dGVtcGxhdGU+XG5cdFxuXHQ8Zm9ybSBjbGFzcz1cImZvcm0taG9yaXpvbnRhbFwiIEBzdWJtaXQucHJldmVudD1cImRlbGV0ZURvY3VtZW50XCI+XG5cblx0XHQ8cD5BcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoaXMgZG9jdW1lbnQ/PC9wPlxuXG5cdFx0PGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPkRlbGV0ZTwvYnV0dG9uPlxuXHQgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG5cblx0PC9mb3JtPlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXHRcblx0ZXhwb3J0IGRlZmF1bHQge1xuXG5cdFx0cHJvcHM6IFsnZG9jdW1lbnRpZCddLFxuXG5cdFx0bWV0aG9kczoge1xuXG5cdFx0XHRkZWxldGVEb2N1bWVudCgpIHtcblx0XHRcdFx0YXhpb3MuZGVsZXRlKCcvdmlzYS9ub3RpZmljYXRpb24vZG9jLycgKyB0aGlzLmRvY3VtZW50aWQpXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YS5zdWNjZXNzKSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMuJHBhcmVudC4kcGFyZW50LnNldFNlbGVjdGVkRG9jdW1lbnRJZChudWxsKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdHRoaXMuJHBhcmVudC4kcGFyZW50LmxvYWREb2NzKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHQkKCcjZGVsZXRlLWRvY3VtZW50LW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLmRhdGEubWVzc2FnZSk7XG5cdFx0ICAgICAgICAgICAgICAgIH1cblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuXHRcdFx0fVxuXG5cdFx0fVxuXG5cdH1cblxuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG5cdGZvcm0ge1xuXHRcdG1hcmdpbi1ib3R0b206IDMwcHg7XG5cdH1cblx0Lm0tci0xMCB7XG5cdFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xuXHR9XG48L3N0eWxlPlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBEZWxldGVEb2N1bWVudC52dWU/MGZlZWU5ZmUiLCI8dGVtcGxhdGU+XG5cdFxuXHQ8Zm9ybSBjbGFzcz1cImZvcm0taG9yaXpvbnRhbFwiIEBzdWJtaXQucHJldmVudD1cImRlbGV0ZVJlcG9ydFR5cGVcIj5cblxuXHRcdDxwPkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhpcyByZXBvcnQgdHlwZT88L3A+XG5cblx0XHQ8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCI+RGVsZXRlPC9idXR0b24+XG5cdCAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cblxuXHQ8L2Zvcm0+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5cdFxuXHRleHBvcnQgZGVmYXVsdCB7XG5cblx0XHRwcm9wczogWydyZXBvcnR0eXBlaWQnXSxcblxuXHRcdG1ldGhvZHM6IHtcblxuXHRcdFx0ZGVsZXRlUmVwb3J0VHlwZSgpIHtcblx0XHRcdFx0YXhpb3MuZGVsZXRlKCcvdmlzYS9ub3RpZmljYXRpb24vcmVwb3J0LXR5cGUvJyArIHRoaXMucmVwb3J0dHlwZWlkKVxuXHRcdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHRcdGlmKHJlc3BvbnNlLmRhdGEuc3VjY2Vzcykge1xuXHRcdFx0XHRcdFx0XHR0aGlzLiRwYXJlbnQuJHBhcmVudC5zZXRTZWxlY3RlZFJlcG9ydFR5cGVJZChudWxsKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdHRoaXMuJHBhcmVudC4kcGFyZW50LmxvYWRSZXBvcnRUeXBlcygpO1xuXG5cdFx0ICAgICAgICAgICAgICAgIFx0JCgnI2RlbGV0ZS1yZXBvcnQtdHlwZS1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHR0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5kYXRhLm1lc3NhZ2UpO1xuXHRcdCAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHRcdH1cblxuXHRcdH1cblxuXHR9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRGVsZXRlUmVwb3J0VHlwZS52dWU/N2ZmZDMwMzMiLCI8dGVtcGxhdGU+XG5cdFxuXHQ8Zm9ybSBjbGFzcz1cImZvcm0taG9yaXpvbnRhbFwiIEBzdWJtaXQucHJldmVudD1cInVwZGF0ZURvY3VtZW50XCI+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IHVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlJykgfVwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBUaXRsZTpcblx0ICAgICAgICA8L2xhYmVsPlxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwidGl0bGVcIiB2LW1vZGVsPVwidXBkYXRlRG9jdW1lbnRGb3JtLnRpdGxlXCI+XG5cdFx0ICAgIFx0PHAgY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cInVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlJylcIiB2LXRleHQ9XCJ1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLnRpdGxlWzBdXCI+PC9wPlxuXHRcdCAgICA8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgOmNsYXNzPVwieyAnaGFzLWVycm9yJzogdXBkYXRlRG9jdW1lbnRGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGVfY24nKSB9XCI+XG5cdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHQgICAgICAgIFRpdGxlIENOOlxuXHQgICAgICAgIDwvbGFiZWw+XG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJ0aXRsZV9jblwiIHYtbW9kZWw9XCJ1cGRhdGVEb2N1bWVudEZvcm0udGl0bGVfY25cIj5cblx0XHQgICAgXHQ8cCBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwidXBkYXRlRG9jdW1lbnRGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGVfY24nKVwiIHYtdGV4dD1cInVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMudGl0bGVfY25bMF1cIj48L3A+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGhyPlxuXG5cdFx0XHQ8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBwdWxsLWxlZnRcIiBAY2xpY2s9XCJhZGREb2N1bWVudFR5cGVcIj5cblx0XHRcdFx0PGkgY2xhc3M9XCJmYSBmYS1wbHVzXCI+PC9pPiBBZGQgRG9jdW1lbnQgVHlwZVxuXHRcdFx0PC9idXR0b24+XG5cblx0XHQ8ZGl2IHN0eWxlPVwiaGVpZ2h0OjIwcHg7IGNsZWFyOmJvdGg7XCI+PC9kaXY+XG5cblx0XHRcdDxkaXYgdi1mb3I9XCIoZG9jX3R5cGUsIGluZGV4KSBpbiB1cGRhdGVEb2N1bWVudEZvcm0uZG9jX3R5cGVzXCI+XG5cblx0XHRcdFx0PGlucHV0IHR5cGU9XCJoaWRkZW5cIiB2LW1vZGVsPVwiZG9jX3R5cGUuaWRcIj5cblxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IHVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2RvY190eXBlcy4nK2luZGV4KycudGl0bGUnKSB9XCI+XG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC0yXCI+XG5cdFx0XHRcdCAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kYW5nZXIgcHVsbC1yaWdodFwiIEBjbGljaz1cInJlbW92ZURvY3VtZW50VHlwZShpbmRleClcIj5cblx0XHRcdFx0ICAgIFx0XHQ8aSBjbGFzcz1cImZhIGZhLXRpbWVzXCI+PC9pPlxuXHRcdFx0XHQgICAgXHQ8L2J1dHRvbj5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdFx0ICAgICAgICBOYW1lOlxuXHRcdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLThcIj5cblx0XHRcdFx0ICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiB2LW1vZGVsPVwiZG9jX3R5cGUudGl0bGVcIj5cblx0XHRcdFx0ICAgIFx0PHAgY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cInVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2RvY190eXBlcy4nK2luZGV4KycudGl0bGUnKVwiPlRoZSBuYW1lIGZpZWxkIGlzIHJlcXVpcmVkLjwvcD5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiB1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJytpbmRleCsnLnRpdGxlX2NuJykgfVwiPlxuXHRcdFx0ICAgIFx0PGRpdiBjbGFzcz1cImNvbC1tZC0yXCI+Jm5ic3A7PC9kaXY+XG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0XHQgICAgICAgIE5hbWUgQ046XG5cdFx0XHQgICAgICAgIDwvbGFiZWw+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtOFwiPlxuXHRcdFx0XHQgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHYtbW9kZWw9XCJkb2NfdHlwZS50aXRsZV9jblwiPlxuXHRcdFx0XHQgICAgXHQ8cCBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwidXBkYXRlRG9jdW1lbnRGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZG9jX3R5cGVzLicraW5kZXgrJy50aXRsZV9jbicpXCI+VGhlIG5hbWUgY24gZmllbGQgaXMgcmVxdWlyZWQuPC9wPlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cblxuXHRcdDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIj5VcGRhdGU8L2J1dHRvbj5cblx0ICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuXG5cdDwvZm9ybT5cblxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblx0XG5cdGV4cG9ydCBkZWZhdWx0IHtcblxuXHRcdHByb3BzOiBbJ2RvY3VtZW50aWQnXSxcblxuXHRcdGRhdGEoKSB7XG4gICAgXHRcdHJldHVybiB7XG4gICAgXHRcdFx0dXBkYXRlRG9jdW1lbnRGb3JtOiB7XG5cdFx0XHRcdFx0dGl0bGU6ICcnLFxuXHRcdFx0XHRcdHRpdGxlX2NuOiAnJyxcblx0XHRcdFx0XHRkb2NfdHlwZXM6IFtdLFxuXHRcdFx0XHRcdGVycm9yczogJydcblx0XHRcdFx0fVxuICAgIFx0XHR9XG4gICAgXHR9LFxuXG4gICAgXHRtZXRob2RzOiB7XG5cbiAgICBcdFx0YWRkRG9jdW1lbnRUeXBlKCkge1xuXHRcdFx0XHR0aGlzLnVwZGF0ZURvY3VtZW50Rm9ybS5kb2NfdHlwZXMucHVzaCh7IGlkOiAnJywgdGl0bGU6ICcnLCB0aXRsZV9jbjogJycgfSk7XG5cdFx0XHR9LFxuXG5cdFx0XHRyZW1vdmVEb2N1bWVudFR5cGUoaW5kZXgpIHtcblx0XHRcdFx0dGhpcy51cGRhdGVEb2N1bWVudEZvcm0uZG9jX3R5cGVzLnNwbGljZShpbmRleCwgMSk7XG5cdFx0XHR9LFxuXG4gICAgXHRcdHVwZGF0ZURvY3VtZW50KCkge1xuXHRcdFx0XHRheGlvcy5wYXRjaCgnL3Zpc2Evbm90aWZpY2F0aW9uL2RvYy8nICsgdGhpcy5kb2N1bWVudGlkLCB7XG5cdFx0XHRcdFx0XHR0aXRsZTogdGhpcy51cGRhdGVEb2N1bWVudEZvcm0udGl0bGUsXG5cdFx0XHRcdFx0XHR0aXRsZV9jbjogdGhpcy51cGRhdGVEb2N1bWVudEZvcm0udGl0bGVfY24sXG5cdFx0XHRcdFx0XHRkb2NfdHlwZXM6IHRoaXMudXBkYXRlRG9jdW1lbnRGb3JtLmRvY190eXBlc1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YS5zdWNjZXNzKSB7XG5cdFx0XHRcdFx0XHRcdHRoaXMuY2xlYXJVcGRhdGVEb2N1bWVudEZvcm0oKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdHRoaXMuJHBhcmVudC4kcGFyZW50LmxvYWREb2NzKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHQkKCcjdXBkYXRlLWRvY3VtZW50LW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLmRhdGEubWVzc2FnZSk7XG5cdFx0ICAgICAgICAgICAgICAgIH1cblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiB0aGlzLnVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMgPSBlcnJvci5yZXNwb25zZS5kYXRhKTtcblx0XHRcdH0sXG5cbiAgICBcdFx0Y2xlYXJVcGRhdGVEb2N1bWVudEZvcm0oKSB7XG5cdFx0XHRcdHRoaXMudXBkYXRlRG9jdW1lbnRGb3JtID0ge1xuXHRcdFx0XHRcdHRpdGxlOiAnJyxcblx0XHRcdFx0XHR0aXRsZV9jbjogJycsXG5cdFx0XHRcdFx0ZG9jX3R5cGVzOiBbXSxcblx0XHRcdFx0XHRlcnJvcnM6ICcnXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cbiAgICBcdH1cblxuXHR9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gVXBkYXRlRG9jdW1lbnQudnVlPzU4NjBkMzkwIiwiPHRlbXBsYXRlPlxuXHRcblx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIiBAc3VibWl0LnByZXZlbnQ9XCJ1cGRhdGVSZXBvcnRUeXBlXCI+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IHVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGUnKSB9XCI+XG5cdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHQgICAgICAgIFRpdGxlOlxuXHQgICAgICAgIDwvbGFiZWw+XG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJ0aXRsZVwiIHYtbW9kZWw9XCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS50aXRsZVwiPlxuXHRcdCAgICBcdDxwIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlJylcIiB2LXRleHQ9XCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMudGl0bGVbMF1cIj48L3A+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiB1cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlX2NuJykgfVwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBUaXRsZSBDTjpcblx0ICAgICAgICA8L2xhYmVsPlxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwidGl0bGVfY25cIiB2LW1vZGVsPVwidXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGVfY25cIj5cblx0XHQgICAgXHQ8cCBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwidXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXCIgdi10ZXh0PVwidXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLnRpdGxlX2NuWzBdXCI+PC9wPlxuXHRcdCAgICA8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgOmNsYXNzPVwieyAnaGFzLWVycm9yJzogdXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkZXNjcmlwdGlvbicpIH1cIj5cblx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdCAgICAgICAgRGVzY3JpcHRpb246XG5cdCAgICAgICAgPC9sYWJlbD5cblx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cImRlc2NyaXB0aW9uXCIgdi1tb2RlbD1cInVwZGF0ZVJlcG9ydFR5cGVGb3JtLmRlc2NyaXB0aW9uXCI+XG5cdFx0ICAgIFx0PHAgY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cInVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZGVzY3JpcHRpb24nKVwiIHYtdGV4dD1cInVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5kZXNjcmlwdGlvblswXVwiPjwvcD5cblx0XHQgICAgPC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IHVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnc2VydmljZXNfaWQnKSB9XCI+XG5cdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHQgICAgICAgIFNlcnZpY2VzOlxuXHQgICAgICAgIDwvbGFiZWw+XG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgXHQ8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XCJTZWxlY3QgU2VydmljZVwiIGNsYXNzPVwiY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXNcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjM1MHB4O1wiIHRhYmluZGV4PVwiNFwiPlxuXHRcdCAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XCJzZXJ2aWNlIGluIHNlcnZpY2VzXCIgOnZhbHVlPVwic2VydmljZS5pZFwiPlxuXHRcdCAgICAgICAgICAgICAgICB7eyAoc2VydmljZS5kZXRhaWwpLnRyaW0oKSB9fVxuXHRcdCAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdCAgICAgICAgPC9zZWxlY3Q+XG5cdFx0ICAgICAgICA8cCBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwidXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdzZXJ2aWNlc19pZCcpXCIgdi10ZXh0PVwidXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLnNlcnZpY2VzX2lkWzBdXCI+PC9wPlxuXHRcdCAgICA8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHQgICAgICAgIFdpdGggRG9jczpcblx0ICAgICAgICA8L2xhYmVsPlxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0ICAgIFx0PGlucHV0IHR5cGU9XCJjaGVja2JveFwiIG5hbWU9XCJ3aXRoX2RvY3NcIiB2LW1vZGVsPVwidXBkYXRlUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzXCI+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiPlVwZGF0ZTwvYnV0dG9uPlxuXHQgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG5cblx0PC9mb3JtPlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXHRcblx0ZXhwb3J0IGRlZmF1bHQge1xuXG5cdFx0cHJvcHM6IFsncmVwb3J0dHlwZWlkJ10sXG5cblx0XHRkYXRhKCkge1xuICAgIFx0XHRyZXR1cm4ge1xuICAgIFx0XHRcdHNlcnZpY2VzOiBbXSxcblxuICAgIFx0XHRcdHVwZGF0ZVJlcG9ydFR5cGVGb3JtOiB7XG5cdFx0XHRcdFx0dGl0bGU6ICcnLFxuXHRcdFx0XHRcdHRpdGxlX2NuOiAnJyxcblx0XHRcdFx0XHRkZXNjcmlwdGlvbjogJycsXG5cdFx0XHRcdFx0c2VydmljZXNfaWQ6IFtdLFxuXHRcdFx0XHRcdHdpdGhfZG9jczogZmFsc2UsXG5cdFx0XHRcdFx0ZXJyb3JzOiAnJ1xuXHRcdFx0XHR9XG4gICAgXHRcdH1cbiAgICBcdH0sXG5cbiAgICBcdG1ldGhvZHM6IHtcblxuICAgIFx0XHR1cGRhdGVSZXBvcnRUeXBlKCkge1xuXHRcdFx0XHRheGlvcy5wYXRjaCgnL3Zpc2Evbm90aWZpY2F0aW9uL3JlcG9ydC10eXBlLycgKyB0aGlzLnJlcG9ydHR5cGVpZCwge1xuXHRcdFx0XHRcdFx0dGl0bGU6IHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGUsXG5cdFx0XHRcdFx0XHR0aXRsZV9jbjogdGhpcy51cGRhdGVSZXBvcnRUeXBlRm9ybS50aXRsZV9jbixcblx0XHRcdFx0XHRcdGRlc2NyaXB0aW9uOiB0aGlzLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLmRlc2NyaXB0aW9uLFxuXHRcdFx0XHRcdFx0c2VydmljZXNfaWQ6IHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0uc2VydmljZXNfaWQsXG5cdFx0XHRcdFx0XHR3aXRoX2RvY3M6IHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzXG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHRpZihyZXNwb25zZS5kYXRhLnN1Y2Nlc3MpIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5jbGVhclVwZGF0ZVJlcG9ydFR5cGVGb3JtKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5sb2FkUmVwb3J0VHlwZXMoKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdCQoJyN1cGRhdGUtcmVwb3J0LXR5cGUtbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXG5cdFx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UuZGF0YS5tZXNzYWdlKTtcblx0XHQgICAgICAgICAgICAgICAgfVxuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmNhdGNoKGVycm9yID0+IHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzID0gZXJyb3IucmVzcG9uc2UuZGF0YSk7XG5cdFx0XHR9LFxuXG4gICAgXHRcdGNsZWFyVXBkYXRlUmVwb3J0VHlwZUZvcm0oKSB7XG5cdFx0XHRcdHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0gPSB7XG5cdFx0XHRcdFx0dGl0bGU6ICcnLFxuXHRcdFx0XHRcdHRpdGxlX2NuOiAnJyxcblx0XHRcdFx0XHRkZXNjcmlwdGlvbjogJycsXG5cdFx0XHRcdFx0c2VydmljZXNfaWQ6IFtdLFxuXHRcdFx0XHRcdHdpdGhfZG9jczogZmFsc2UsXG5cdFx0XHRcdFx0ZXJyb3JzOiAnJ1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly8gRGVzZWxlY3QgQWxsXG5cdFx0XHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlcycpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblx0XHRcdH0sXG5cbiAgICBcdFx0bG9hZFNlcnZpY2VzKCkge1xuXHRcdFx0XHRheGlvcy5nZXQoJy92aXNhL25vdGlmaWNhdGlvbi9zZXJ2aWNlcycpXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5zZXJ2aWNlcyA9IHJlc3BvbnNlLmRhdGE7XG5cblx0XHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHQkKFwiLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzXCIpLnRyaWdnZXIoXCJjaG9zZW46dXBkYXRlZFwiKTtcblx0XHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcblx0XHRcdH0sXG5cblx0XHRcdHVwZGF0ZUNob3NlbihzZXJ2aWNlc0lkKSB7XG5cdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdCQoXCIuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXNcIikudmFsKHNlcnZpY2VzSWQpLnRyaWdnZXIoXCJjaG9zZW46dXBkYXRlZFwiKTtcblx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdH0sXG5cblx0XHRcdGluaXRDaG9zZW5TZWxlY3QoKSB7XG5cdCAgICAgICAgXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXMnKS5jaG9zZW4oe1xuXHRcdFx0XHRcdHdpZHRoOiBcIjEwMCVcIixcblx0XHRcdFx0XHRub19yZXN1bHRzX3RleHQ6IFwiTm8gcmVzdWx0L3MgZm91bmQuXCIsXG5cdFx0XHRcdFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXG5cdFx0XHRcdH0pO1xuXG5cdCAgICAgICAgXHR2YXIgdm0gPSB0aGlzO1xuXG5cdFx0XHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlcycpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHR2bS51cGRhdGVSZXBvcnRUeXBlRm9ybS5zZXJ2aWNlc19pZCA9ICQodGhpcykudmFsKCk7XG5cdFx0XHRcdH0pO1xuXHQgICAgICAgIH1cblxuICAgIFx0fSxcblxuICAgIFx0Y3JlYXRlZCgpIHtcblx0XHRcdHRoaXMubG9hZFNlcnZpY2VzKCk7XG5cdFx0fSxcblxuXHRcdG1vdW50ZWQoKSB7XG4gICAgICAgIFx0dGhpcy5pbml0Q2hvc2VuU2VsZWN0KCk7XG4gICAgXHR9XG5cblx0fVxuXG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFVwZGF0ZVJlcG9ydFR5cGUudnVlPzE1YzNmN2NlIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSgpO1xuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuZm9ybVtkYXRhLXYtMDY5MmY4OTZdIHtcXG5cXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcbn1cXG4ubS1yLTEwW2RhdGEtdi0wNjkyZjg5Nl0ge1xcblxcdG1hcmdpbi1yaWdodDogMTBweDtcXG59XFxuaW5wdXRbdHlwZT1cXFwiY2hlY2tib3hcXFwiXVtkYXRhLXYtMDY5MmY4OTZdIHtcXG5cXHRtYXJnaW4tdG9wOiAxNXB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiQWRkUmVwb3J0VHlwZS52dWU/N2MyYjE1YThcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQWtLQTtDQUNBLG9CQUFBO0NBQ0E7QUFDQTtDQUNBLG1CQUFBO0NBQ0E7QUFDQTtDQUNBLGlCQUFBO0NBQ0FcIixcImZpbGVcIjpcIkFkZFJlcG9ydFR5cGUudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXHRcXG5cXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIiBAc3VibWl0LnByZXZlbnQ9XFxcImFkZFJlcG9ydFR5cGVcXFwiPlxcblxcblxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpIH1cXFwiPlxcblxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgVGl0bGU6XFxuXFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdCAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwidGl0bGVcXFwiIHYtbW9kZWw9XFxcImFkZFJlcG9ydFR5cGVGb3JtLnRpdGxlXFxcIj5cXG5cXHRcXHQgICAgXFx0PHAgY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIHYtaWY9XFxcImFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGUnKVxcXCIgdi10ZXh0PVxcXCJhZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMudGl0bGVbMF1cXFwiPjwvcD5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBhZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlX2NuJykgfVxcXCI+XFxuXFx0XFx0ICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0ICAgICAgICBUaXRsZSBDTjpcXG5cXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0ICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJ0aXRsZV9jblxcXCIgdi1tb2RlbD1cXFwiYWRkUmVwb3J0VHlwZUZvcm0udGl0bGVfY25cXFwiPlxcblxcdFxcdCAgICBcXHQ8cCBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXFxcIiB2LXRleHQ9XFxcImFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy50aXRsZV9jblswXVxcXCI+PC9wPlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiA6Y2xhc3M9XFxcInsgJ2hhcy1lcnJvcic6IGFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZGVzY3JpcHRpb24nKSB9XFxcIj5cXG5cXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHQgICAgICAgIERlc2NyaXB0aW9uOlxcblxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHQgICAgXFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImRlc2NyaXB0aW9uXFxcIiB2LW1vZGVsPVxcXCJhZGRSZXBvcnRUeXBlRm9ybS5kZXNjcmlwdGlvblxcXCI+XFxuXFx0XFx0ICAgIFxcdDxwIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJhZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2Rlc2NyaXB0aW9uJylcXFwiIHYtdGV4dD1cXFwiYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmRlc2NyaXB0aW9uWzBdXFxcIj48L3A+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdDwvZGl2PlxcblxcblxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdzZXJ2aWNlc19pZCcpIH1cXFwiPlxcblxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgU2VydmljZXM6XFxuXFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdCAgICBcXHQ8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XFxcIlNlbGVjdCBTZXJ2aWNlXFxcIiBjbGFzcz1cXFwiY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXNcXFwiIG11bHRpcGxlIHN0eWxlPVxcXCJ3aWR0aDozNTBweDtcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj5cXG5cXHRcXHQgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVxcXCJzZXJ2aWNlIGluIHNlcnZpY2VzXFxcIiA6dmFsdWU9XFxcInNlcnZpY2UuaWRcXFwiPlxcblxcdFxcdCAgICAgICAgICAgICAgICB7eyAoc2VydmljZS5kZXRhaWwpLnRyaW0oKSB9fVxcblxcdFxcdCAgICAgICAgICAgIDwvb3B0aW9uPlxcblxcdFxcdCAgICAgICAgPC9zZWxlY3Q+XFxuXFx0XFx0ICAgICAgICA8cCBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdzZXJ2aWNlc19pZCcpXFxcIiB2LXRleHQ9XFxcImFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5zZXJ2aWNlc19pZFswXVxcXCI+PC9wPlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHQgICAgICAgIFdpdGggRG9jczpcXG5cXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0ICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJjaGVja2JveFxcXCIgbmFtZT1cXFwid2l0aF9kb2NzXFxcIiB2LW1vZGVsPVxcXCJhZGRSZXBvcnRUeXBlRm9ybS53aXRoX2RvY3NcXFwiPlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQ8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5BZGQ8L2J1dHRvbj5cXG5cXHQgICAgPGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFxuXFx0PC9mb3JtPlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXHRcXG5cXHRleHBvcnQgZGVmYXVsdCB7XFxuXFxuXFx0XFx0ZGF0YSgpIHtcXG4gICAgXFx0XFx0cmV0dXJuIHtcXG4gICAgXFx0XFx0XFx0c2VydmljZXM6IFtdLFxcblxcbiAgICBcXHRcXHRcXHRhZGRSZXBvcnRUeXBlRm9ybToge1xcblxcdFxcdFxcdFxcdFxcdHRpdGxlOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aXRsZV9jbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0ZGVzY3JpcHRpb246ICcnLFxcblxcdFxcdFxcdFxcdFxcdHNlcnZpY2VzX2lkOiBbXSxcXG5cXHRcXHRcXHRcXHRcXHR3aXRoX2RvY3M6IGZhbHNlLFxcblxcdFxcdFxcdFxcdFxcdGVycm9yczogJydcXG5cXHRcXHRcXHRcXHR9XFxuICAgIFxcdFxcdH1cXG4gICAgXFx0fSxcXG5cXG5cXHRcXHRtZXRob2RzOiB7XFxuXFxuXFx0XFx0XFx0YWRkUmVwb3J0VHlwZSgpIHtcXG5cXHRcXHRcXHRcXHRheGlvcy5wb3N0KCcvdmlzYS9ub3RpZmljYXRpb24vcmVwb3J0LXR5cGUnLCB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGl0bGU6IHRoaXMuYWRkUmVwb3J0VHlwZUZvcm0udGl0bGUsXFxuXFx0XFx0XFx0XFx0XFx0XFx0dGl0bGVfY246IHRoaXMuYWRkUmVwb3J0VHlwZUZvcm0udGl0bGVfY24sXFxuXFx0XFx0XFx0XFx0XFx0XFx0ZGVzY3JpcHRpb246IHRoaXMuYWRkUmVwb3J0VHlwZUZvcm0uZGVzY3JpcHRpb24sXFxuXFx0XFx0XFx0XFx0XFx0XFx0c2VydmljZXNfaWQ6IHRoaXMuYWRkUmVwb3J0VHlwZUZvcm0uc2VydmljZXNfaWQsXFxuXFx0XFx0XFx0XFx0XFx0XFx0d2l0aF9kb2NzOiB0aGlzLmFkZFJlcG9ydFR5cGVGb3JtLndpdGhfZG9jc1xcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdGlmKHJlc3BvbnNlLmRhdGEuc3VjY2Vzcykge1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuY2xlYXJBZGRSZXBvcnRUeXBlRm9ybSgpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5sb2FkUmVwb3J0VHlwZXMoKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0JCgnI2FkZC1yZXBvcnQtdHlwZS1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLmRhdGEubWVzc2FnZSk7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIH1cXG5cXHRcXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdFxcdC5jYXRjaChlcnJvciA9PiB0aGlzLmFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycyA9IGVycm9yLnJlc3BvbnNlLmRhdGEpO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0Y2xlYXJBZGRSZXBvcnRUeXBlRm9ybSgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLmFkZFJlcG9ydFR5cGVGb3JtID0ge1xcblxcdFxcdFxcdFxcdFxcdHRpdGxlOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aXRsZV9jbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0ZGVzY3JpcHRpb246ICcnLFxcblxcdFxcdFxcdFxcdFxcdHNlcnZpY2VzX2lkOiBbXSxcXG5cXHRcXHRcXHRcXHRcXHR3aXRoX2RvY3M6IGZhbHNlLFxcblxcdFxcdFxcdFxcdFxcdGVycm9yczogJydcXG5cXHRcXHRcXHRcXHR9XFxuXFxuXFx0XFx0XFx0XFx0Ly8gRGVzZWxlY3QgQWxsXFxuXFx0XFx0XFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzJykudmFsKCcnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0bG9hZFNlcnZpY2VzKCkge1xcblxcdFxcdFxcdFxcdGF4aW9zLmdldCgnL3Zpc2Evbm90aWZpY2F0aW9uL3NlcnZpY2VzJylcXG5cXHRcXHRcXHRcXHRcXHQudGhlbihyZXNwb25zZSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5zZXJ2aWNlcyA9IHJlc3BvbnNlLmRhdGE7XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0JChcXFwiLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzXFxcIikudHJpZ2dlcihcXFwiY2hvc2VuOnVwZGF0ZWRcXFwiKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHR9LCA1MDApO1xcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRpbml0Q2hvc2VuU2VsZWN0KCkge1xcblxcdCAgICAgICAgXFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzJykuY2hvc2VuKHtcXG5cXHRcXHRcXHRcXHRcXHR3aWR0aDogXFxcIjEwMCVcXFwiLFxcblxcdFxcdFxcdFxcdFxcdG5vX3Jlc3VsdHNfdGV4dDogXFxcIk5vIHJlc3VsdC9zIGZvdW5kLlxcXCIsXFxuXFx0XFx0XFx0XFx0XFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0ICAgICAgICBcXHR2YXIgdm0gPSB0aGlzO1xcblxcblxcdFxcdFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlcycpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcXG5cXHRcXHRcXHRcXHRcXHR2bS5hZGRSZXBvcnRUeXBlRm9ybS5zZXJ2aWNlc19pZCA9ICQodGhpcykudmFsKCk7XFxuXFx0XFx0XFx0XFx0fSk7XFxuXFx0ICAgICAgICB9XFxuXFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRjcmVhdGVkKCkge1xcblxcdFxcdFxcdHRoaXMubG9hZFNlcnZpY2VzKCk7XFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRtb3VudGVkKCkge1xcbiAgICAgICAgXFx0dGhpcy5pbml0Q2hvc2VuU2VsZWN0KCk7XFxuICAgIFxcdH1cXG5cXG5cXHR9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcblxcdGlucHV0W3R5cGU9XFxcImNoZWNrYm94XFxcIl0ge1xcblxcdFxcdG1hcmdpbi10b3A6IDE1cHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0wNjkyZjg5NlwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZFJlcG9ydFR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSAyOTJcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuICBNb2RpZmllZCBieSBFdmFuIFlvdSBAeXl4OTkwODAzXG4qL1xuXG52YXIgaGFzRG9jdW1lbnQgPSB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG5cbmlmICh0eXBlb2YgREVCVUcgIT09ICd1bmRlZmluZWQnICYmIERFQlVHKSB7XG4gIGlmICghaGFzRG9jdW1lbnQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgJ3Z1ZS1zdHlsZS1sb2FkZXIgY2Fubm90IGJlIHVzZWQgaW4gYSBub24tYnJvd3NlciBlbnZpcm9ubWVudC4gJyArXG4gICAgXCJVc2UgeyB0YXJnZXQ6ICdub2RlJyB9IGluIHlvdXIgV2VicGFjayBjb25maWcgdG8gaW5kaWNhdGUgYSBzZXJ2ZXItcmVuZGVyaW5nIGVudmlyb25tZW50LlwiXG4gICkgfVxufVxuXG52YXIgbGlzdFRvU3R5bGVzID0gcmVxdWlyZSgnLi9saXN0VG9TdHlsZXMnKVxuXG4vKlxudHlwZSBTdHlsZU9iamVjdCA9IHtcbiAgaWQ6IG51bWJlcjtcbiAgcGFydHM6IEFycmF5PFN0eWxlT2JqZWN0UGFydD5cbn1cblxudHlwZSBTdHlsZU9iamVjdFBhcnQgPSB7XG4gIGNzczogc3RyaW5nO1xuICBtZWRpYTogc3RyaW5nO1xuICBzb3VyY2VNYXA6ID9zdHJpbmdcbn1cbiovXG5cbnZhciBzdHlsZXNJbkRvbSA9IHsvKlxuICBbaWQ6IG51bWJlcl06IHtcbiAgICBpZDogbnVtYmVyLFxuICAgIHJlZnM6IG51bWJlcixcbiAgICBwYXJ0czogQXJyYXk8KG9iaj86IFN0eWxlT2JqZWN0UGFydCkgPT4gdm9pZD5cbiAgfVxuKi99XG5cbnZhciBoZWFkID0gaGFzRG9jdW1lbnQgJiYgKGRvY3VtZW50LmhlYWQgfHwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXSlcbnZhciBzaW5nbGV0b25FbGVtZW50ID0gbnVsbFxudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwXG52YXIgaXNQcm9kdWN0aW9uID0gZmFsc2VcbnZhciBub29wID0gZnVuY3Rpb24gKCkge31cblxuLy8gRm9yY2Ugc2luZ2xlLXRhZyBzb2x1dGlvbiBvbiBJRTYtOSwgd2hpY2ggaGFzIGEgaGFyZCBsaW1pdCBvbiB0aGUgIyBvZiA8c3R5bGU+XG4vLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG52YXIgaXNPbGRJRSA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIC9tc2llIFs2LTldXFxiLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSlcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAocGFyZW50SWQsIGxpc3QsIF9pc1Byb2R1Y3Rpb24pIHtcbiAgaXNQcm9kdWN0aW9uID0gX2lzUHJvZHVjdGlvblxuXG4gIHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIGxpc3QpXG4gIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG4gICAgdmFyIG1heVJlbW92ZSA9IFtdXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgICAgZG9tU3R5bGUucmVmcy0tXG4gICAgICBtYXlSZW1vdmUucHVzaChkb21TdHlsZSlcbiAgICB9XG4gICAgaWYgKG5ld0xpc3QpIHtcbiAgICAgIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhwYXJlbnRJZCwgbmV3TGlzdClcbiAgICAgIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVzID0gW11cbiAgICB9XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXVxuICAgICAgaWYgKGRvbVN0eWxlLnJlZnMgPT09IDApIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKClcbiAgICAgICAgfVxuICAgICAgICBkZWxldGUgc3R5bGVzSW5Eb21bZG9tU3R5bGUuaWRdXG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlc1RvRG9tIChzdHlsZXMgLyogQXJyYXk8U3R5bGVPYmplY3Q+ICovKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBzdHlsZXNbaV1cbiAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgZG9tU3R5bGUucmVmcysrXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pXG4gICAgICB9XG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdKSlcbiAgICAgIH1cbiAgICAgIGlmIChkb21TdHlsZS5wYXJ0cy5sZW5ndGggPiBpdGVtLnBhcnRzLmxlbmd0aCkge1xuICAgICAgICBkb21TdHlsZS5wYXJ0cy5sZW5ndGggPSBpdGVtLnBhcnRzLmxlbmd0aFxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgcGFydHMgPSBbXVxuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIHBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBzdHlsZXNJbkRvbVtpdGVtLmlkXSA9IHsgaWQ6IGl0ZW0uaWQsIHJlZnM6IDEsIHBhcnRzOiBwYXJ0cyB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVN0eWxlRWxlbWVudCAoKSB7XG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpXG4gIHN0eWxlRWxlbWVudC50eXBlID0gJ3RleHQvY3NzJ1xuICBoZWFkLmFwcGVuZENoaWxkKHN0eWxlRWxlbWVudClcbiAgcmV0dXJuIHN0eWxlRWxlbWVudFxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZSAob2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICB2YXIgdXBkYXRlLCByZW1vdmVcbiAgdmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3N0eWxlW2RhdGEtdnVlLXNzci1pZH49XCInICsgb2JqLmlkICsgJ1wiXScpXG5cbiAgaWYgKHN0eWxlRWxlbWVudCkge1xuICAgIGlmIChpc1Byb2R1Y3Rpb24pIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGFuZCBpbiBwcm9kdWN0aW9uIG1vZGUuXG4gICAgICAvLyBzaW1wbHkgZG8gbm90aGluZy5cbiAgICAgIHJldHVybiBub29wXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGJ1dCBpbiBkZXYgbW9kZS5cbiAgICAgIC8vIGZvciBzb21lIHJlYXNvbiBDaHJvbWUgY2FuJ3QgaGFuZGxlIHNvdXJjZSBtYXAgaW4gc2VydmVyLXJlbmRlcmVkXG4gICAgICAvLyBzdHlsZSB0YWdzIC0gc291cmNlIG1hcHMgaW4gPHN0eWxlPiBvbmx5IHdvcmtzIGlmIHRoZSBzdHlsZSB0YWcgaXNcbiAgICAgIC8vIGNyZWF0ZWQgYW5kIGluc2VydGVkIGR5bmFtaWNhbGx5LiBTbyB3ZSByZW1vdmUgdGhlIHNlcnZlciByZW5kZXJlZFxuICAgICAgLy8gc3R5bGVzIGFuZCBpbmplY3QgbmV3IG9uZXMuXG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgaWYgKGlzT2xkSUUpIHtcbiAgICAvLyB1c2Ugc2luZ2xldG9uIG1vZGUgZm9yIElFOS5cbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrK1xuICAgIHN0eWxlRWxlbWVudCA9IHNpbmdsZXRvbkVsZW1lbnQgfHwgKHNpbmdsZXRvbkVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKSlcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCBmYWxzZSlcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCB0cnVlKVxuICB9IGVsc2Uge1xuICAgIC8vIHVzZSBtdWx0aS1zdHlsZS10YWcgbW9kZSBpbiBhbGwgb3RoZXIgY2FzZXNcbiAgICBzdHlsZUVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQpXG4gICAgcmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmopXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmogLyogU3R5bGVPYmplY3RQYXJ0ICovKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcbiAgICAgICAgICBuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJlxuICAgICAgICAgIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKVxuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKVxuICAgIH1cbiAgfVxufVxuXG52YXIgcmVwbGFjZVRleHQgPSAoZnVuY3Rpb24gKCkge1xuICB2YXIgdGV4dFN0b3JlID0gW11cblxuICByZXR1cm4gZnVuY3Rpb24gKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudFxuICAgIHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpXG4gIH1cbn0pKClcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyAoc3R5bGVFbGVtZW50LCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcbiAgdmFyIGNzcyA9IHJlbW92ZSA/ICcnIDogb2JqLmNzc1xuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKVxuICB9IGVsc2Uge1xuICAgIHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKVxuICAgIHZhciBjaGlsZE5vZGVzID0gc3R5bGVFbGVtZW50LmNoaWxkTm9kZXNcbiAgICBpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSlcbiAgICBpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChjc3NOb2RlKVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRvVGFnIChzdHlsZUVsZW1lbnQsIG9iaikge1xuICB2YXIgY3NzID0gb2JqLmNzc1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWFcbiAgdmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXBcblxuICBpZiAobWVkaWEpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKCdtZWRpYScsIG1lZGlhKVxuICB9XG5cbiAgaWYgKHNvdXJjZU1hcCkge1xuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLmNocm9tZS5jb20vZGV2dG9vbHMvZG9jcy9qYXZhc2NyaXB0LWRlYnVnZ2luZ1xuICAgIC8vIHRoaXMgbWFrZXMgc291cmNlIG1hcHMgaW5zaWRlIHN0eWxlIHRhZ3Mgd29yayBwcm9wZXJseSBpbiBDaHJvbWVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VVUkw9JyArIHNvdXJjZU1hcC5zb3VyY2VzWzBdICsgJyAqLydcbiAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNjYwMzg3NVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCwnICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArICcgKi8nXG4gIH1cblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzXG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpXG4gICAgfVxuICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKVxuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi0zYmQ5YmY4Ml0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTNiZDliZjgyXSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiQWRkRG9jdW1lbnQudnVlPzIyOTg0NTEwXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUFvSUE7Q0FDQSxvQkFBQTtDQUNBO0FBQ0E7Q0FDQSxtQkFBQTtDQUNBXCIsXCJmaWxlXCI6XCJBZGREb2N1bWVudC52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdFxcblxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwiYWRkRG9jXFxcIj5cXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiA6Y2xhc3M9XFxcInsgJ2hhcy1lcnJvcic6IGFkZERvY0Zvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpIH1cXFwiPlxcblxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgVGl0bGU6XFxuXFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdCAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwidGl0bGVcXFwiIHYtbW9kZWw9XFxcImFkZERvY0Zvcm0udGl0bGVcXFwiPlxcblxcdFxcdCAgICBcXHQ8cCBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlJylcXFwiIHYtdGV4dD1cXFwiYWRkRG9jRm9ybS5lcnJvcnMudGl0bGVbMF1cXFwiPjwvcD5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBhZGREb2NGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGVfY24nKSB9XFxcIj5cXG5cXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHQgICAgICAgIFRpdGxlIENOOlxcblxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHQgICAgXFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcInRpdGxlX2NuXFxcIiB2LW1vZGVsPVxcXCJhZGREb2NGb3JtLnRpdGxlX2NuXFxcIj5cXG5cXHRcXHQgICAgXFx0PHAgY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIHYtaWY9XFxcImFkZERvY0Zvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXFxcIiB2LXRleHQ9XFxcImFkZERvY0Zvcm0uZXJyb3JzLnRpdGxlX2NuWzBdXFxcIj48L3A+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdDwvZGl2PlxcblxcblxcdFxcdDxocj5cXG5cXG5cXHRcXHRcXHQ8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBwdWxsLWxlZnRcXFwiIEBjbGljaz1cXFwiYWRkRG9jdW1lbnRUeXBlXFxcIj5cXG5cXHRcXHRcXHRcXHQ8aSBjbGFzcz1cXFwiZmEgZmEtcGx1c1xcXCI+PC9pPiBBZGQgRG9jdW1lbnQgVHlwZVxcblxcdFxcdFxcdDwvYnV0dG9uPlxcblxcblxcdFxcdDxkaXYgc3R5bGU9XFxcImhlaWdodDoyMHB4OyBjbGVhcjpib3RoO1xcXCI+PC9kaXY+XFxuXFxuXFx0XFx0XFx0PGRpdiB2LWZvcj1cXFwiKGRvY190eXBlLCBpbmRleCkgaW4gYWRkRG9jRm9ybS5kb2NfdHlwZXNcXFwiPlxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2RvY190eXBlcy4nK2luZGV4KycudGl0bGUnKSB9XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMlxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kYW5nZXIgcHVsbC1yaWdodFxcXCIgQGNsaWNrPVxcXCJyZW1vdmVEb2N1bWVudFR5cGUoaW5kZXgpXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgXFx0XFx0PGkgY2xhc3M9XFxcImZhIGZhLXRpbWVzXFxcIj48L2k+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDwvYnV0dG9uPlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHRcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgTmFtZTpcXG5cXHRcXHRcXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtOFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiB2LW1vZGVsPVxcXCJkb2NfdHlwZS50aXRsZVxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDxwIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJhZGREb2NGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZG9jX3R5cGVzLicraW5kZXgrJy50aXRsZScpXFxcIj5UaGUgbmFtZSBmaWVsZCBpcyByZXF1aXJlZC48L3A+XFxuXFx0XFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXG5cXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBhZGREb2NGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZG9jX3R5cGVzLicraW5kZXgrJy50aXRsZV9jbicpIH1cXFwiPlxcblxcdFxcdFxcdCAgICBcXHQ8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMlxcXCI+Jm5ic3A7PC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIE5hbWUgQ046XFxuXFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLThcXFwiPlxcblxcdFxcdFxcdFxcdCAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgdi1tb2RlbD1cXFwiZG9jX3R5cGUudGl0bGVfY25cXFwiPlxcblxcdFxcdFxcdFxcdCAgICBcXHQ8cCBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwiYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2RvY190eXBlcy4nK2luZGV4KycudGl0bGVfY24nKVxcXCI+VGhlIG5hbWUgY24gZmllbGQgaXMgcmVxdWlyZWQuPC9wPlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0PGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjIwcHg7IGNsZWFyOmJvdGg7XFxcIj48L2Rpdj5cXG5cXG5cXHRcXHQ8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5BZGQ8L2J1dHRvbj5cXG5cXHQgICAgPGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFxuXFx0PC9mb3JtPlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXHRcXG5cXHRleHBvcnQgZGVmYXVsdCB7XFxuXFxuXFx0XFx0ZGF0YSgpIHtcXG4gICAgXFx0XFx0cmV0dXJuIHtcXG4gICAgXFx0XFx0XFx0YWRkRG9jRm9ybToge1xcblxcdFxcdFxcdFxcdFxcdHRpdGxlOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aXRsZV9jbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0ZG9jX3R5cGVzOiBbXSxcXG5cXHRcXHRcXHRcXHRcXHRlcnJvcnM6ICcnXFxuXFx0XFx0XFx0XFx0fVxcbiAgICBcXHRcXHR9XFxuICAgIFxcdH0sXFxuXFxuXFx0XFx0bWV0aG9kczoge1xcblxcblxcdFxcdFxcdGFkZERvY3VtZW50VHlwZSgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLmFkZERvY0Zvcm0uZG9jX3R5cGVzLnB1c2goeyB0aXRsZTogJycsIHRpdGxlX2NuOiAnJyB9KTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdHJlbW92ZURvY3VtZW50VHlwZShpbmRleCkge1xcblxcdFxcdFxcdFxcdHRoaXMuYWRkRG9jRm9ybS5kb2NfdHlwZXMuc3BsaWNlKGluZGV4LCAxKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGFkZERvYygpIHtcXG5cXHRcXHRcXHRcXHRheGlvcy5wb3N0KCcvdmlzYS9ub3RpZmljYXRpb24vZG9jJywge1xcblxcdFxcdFxcdFxcdFxcdFxcdHRpdGxlOiB0aGlzLmFkZERvY0Zvcm0udGl0bGUsXFxuXFx0XFx0XFx0XFx0XFx0XFx0dGl0bGVfY246IHRoaXMuYWRkRG9jRm9ybS50aXRsZV9jbixcXG5cXHRcXHRcXHRcXHRcXHRcXHRkb2NfdHlwZXM6IHRoaXMuYWRkRG9jRm9ybS5kb2NfdHlwZXNcXG5cXHRcXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdFxcdC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRpZihyZXNwb25zZS5kYXRhLnN1Y2Nlc3MpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmNsZWFyQWRkRG9jRm9ybSgpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5sb2FkRG9jcygpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHQkKCcjYWRkLWRvY3VtZW50LW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UuZGF0YS5tZXNzYWdlKTtcXG5cXHRcXHQgICAgICAgICAgICAgICAgfVxcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LmNhdGNoKGVycm9yID0+IHRoaXMuYWRkRG9jRm9ybS5lcnJvcnMgPSBlcnJvci5yZXNwb25zZS5kYXRhKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcblxcdFxcdFxcdGNsZWFyQWRkRG9jRm9ybSgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLmFkZERvY0Zvcm0gPSB7XFxuXFx0XFx0XFx0XFx0XFx0dGl0bGU6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHRpdGxlX2NuOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHRkb2NfdHlwZXM6IFtdLFxcblxcdFxcdFxcdFxcdFxcdGVycm9yczogJydcXG5cXHRcXHRcXHRcXHR9O1xcblxcdFxcdFxcdH1cXG5cXG5cXG5cXHRcXHR9XFxuXFxuXFx0fVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtM2JkOWJmODJcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGREb2N1bWVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDMwMFxuLy8gbW9kdWxlIGNodW5rcyA9IDMiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi02MTA3MDYwMV0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTYxMDcwNjAxXSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiVXBkYXRlUmVwb3J0VHlwZS52dWU/MTVjM2Y3Y2VcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQTBLQTtDQUNBLG9CQUFBO0NBQ0E7QUFDQTtDQUNBLG1CQUFBO0NBQ0FcIixcImZpbGVcIjpcIlVwZGF0ZVJlcG9ydFR5cGUudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXHRcXG5cXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIiBAc3VibWl0LnByZXZlbnQ9XFxcInVwZGF0ZVJlcG9ydFR5cGVcXFwiPlxcblxcblxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogdXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpIH1cXFwiPlxcblxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgVGl0bGU6XFxuXFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdCAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwidGl0bGVcXFwiIHYtbW9kZWw9XFxcInVwZGF0ZVJlcG9ydFR5cGVGb3JtLnRpdGxlXFxcIj5cXG5cXHRcXHQgICAgXFx0PHAgY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIHYtaWY9XFxcInVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGUnKVxcXCIgdi10ZXh0PVxcXCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMudGl0bGVbMF1cXFwiPjwvcD5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiB1cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlX2NuJykgfVxcXCI+XFxuXFx0XFx0ICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0ICAgICAgICBUaXRsZSBDTjpcXG5cXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0ICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJ0aXRsZV9jblxcXCIgdi1tb2RlbD1cXFwidXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGVfY25cXFwiPlxcblxcdFxcdCAgICBcXHQ8cCBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwidXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXFxcIiB2LXRleHQ9XFxcInVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy50aXRsZV9jblswXVxcXCI+PC9wPlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiA6Y2xhc3M9XFxcInsgJ2hhcy1lcnJvcic6IHVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZGVzY3JpcHRpb24nKSB9XFxcIj5cXG5cXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHQgICAgICAgIERlc2NyaXB0aW9uOlxcblxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHQgICAgXFx0PGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImRlc2NyaXB0aW9uXFxcIiB2LW1vZGVsPVxcXCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS5kZXNjcmlwdGlvblxcXCI+XFxuXFx0XFx0ICAgIFxcdDxwIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2Rlc2NyaXB0aW9uJylcXFwiIHYtdGV4dD1cXFwidXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmRlc2NyaXB0aW9uWzBdXFxcIj48L3A+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdDwvZGl2PlxcblxcblxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIDpjbGFzcz1cXFwieyAnaGFzLWVycm9yJzogdXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdzZXJ2aWNlc19pZCcpIH1cXFwiPlxcblxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgU2VydmljZXM6XFxuXFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdCAgICBcXHQ8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XFxcIlNlbGVjdCBTZXJ2aWNlXFxcIiBjbGFzcz1cXFwiY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXNcXFwiIG11bHRpcGxlIHN0eWxlPVxcXCJ3aWR0aDozNTBweDtcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj5cXG5cXHRcXHQgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVxcXCJzZXJ2aWNlIGluIHNlcnZpY2VzXFxcIiA6dmFsdWU9XFxcInNlcnZpY2UuaWRcXFwiPlxcblxcdFxcdCAgICAgICAgICAgICAgICB7eyAoc2VydmljZS5kZXRhaWwpLnRyaW0oKSB9fVxcblxcdFxcdCAgICAgICAgICAgIDwvb3B0aW9uPlxcblxcdFxcdCAgICAgICAgPC9zZWxlY3Q+XFxuXFx0XFx0ICAgICAgICA8cCBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwidXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdzZXJ2aWNlc19pZCcpXFxcIiB2LXRleHQ9XFxcInVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5zZXJ2aWNlc19pZFswXVxcXCI+PC9wPlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHQgICAgICAgIFdpdGggRG9jczpcXG5cXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0ICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJjaGVja2JveFxcXCIgbmFtZT1cXFwid2l0aF9kb2NzXFxcIiB2LW1vZGVsPVxcXCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS53aXRoX2RvY3NcXFwiPlxcblxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHQ8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5VcGRhdGU8L2J1dHRvbj5cXG5cXHQgICAgPGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFxuXFx0PC9mb3JtPlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXHRcXG5cXHRleHBvcnQgZGVmYXVsdCB7XFxuXFxuXFx0XFx0cHJvcHM6IFsncmVwb3J0dHlwZWlkJ10sXFxuXFxuXFx0XFx0ZGF0YSgpIHtcXG4gICAgXFx0XFx0cmV0dXJuIHtcXG4gICAgXFx0XFx0XFx0c2VydmljZXM6IFtdLFxcblxcbiAgICBcXHRcXHRcXHR1cGRhdGVSZXBvcnRUeXBlRm9ybToge1xcblxcdFxcdFxcdFxcdFxcdHRpdGxlOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aXRsZV9jbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0ZGVzY3JpcHRpb246ICcnLFxcblxcdFxcdFxcdFxcdFxcdHNlcnZpY2VzX2lkOiBbXSxcXG5cXHRcXHRcXHRcXHRcXHR3aXRoX2RvY3M6IGZhbHNlLFxcblxcdFxcdFxcdFxcdFxcdGVycm9yczogJydcXG5cXHRcXHRcXHRcXHR9XFxuICAgIFxcdFxcdH1cXG4gICAgXFx0fSxcXG5cXG4gICAgXFx0bWV0aG9kczoge1xcblxcbiAgICBcXHRcXHR1cGRhdGVSZXBvcnRUeXBlKCkge1xcblxcdFxcdFxcdFxcdGF4aW9zLnBhdGNoKCcvdmlzYS9ub3RpZmljYXRpb24vcmVwb3J0LXR5cGUvJyArIHRoaXMucmVwb3J0dHlwZWlkLCB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGl0bGU6IHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGUsXFxuXFx0XFx0XFx0XFx0XFx0XFx0dGl0bGVfY246IHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGVfY24sXFxuXFx0XFx0XFx0XFx0XFx0XFx0ZGVzY3JpcHRpb246IHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0uZGVzY3JpcHRpb24sXFxuXFx0XFx0XFx0XFx0XFx0XFx0c2VydmljZXNfaWQ6IHRoaXMudXBkYXRlUmVwb3J0VHlwZUZvcm0uc2VydmljZXNfaWQsXFxuXFx0XFx0XFx0XFx0XFx0XFx0d2l0aF9kb2NzOiB0aGlzLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLndpdGhfZG9jc1xcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdGlmKHJlc3BvbnNlLmRhdGEuc3VjY2Vzcykge1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuY2xlYXJVcGRhdGVSZXBvcnRUeXBlRm9ybSgpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5sb2FkUmVwb3J0VHlwZXMoKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0JCgnI3VwZGF0ZS1yZXBvcnQtdHlwZS1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLmRhdGEubWVzc2FnZSk7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIH1cXG5cXHRcXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdFxcdC5jYXRjaChlcnJvciA9PiB0aGlzLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycyA9IGVycm9yLnJlc3BvbnNlLmRhdGEpO1xcblxcdFxcdFxcdH0sXFxuXFxuICAgIFxcdFxcdGNsZWFyVXBkYXRlUmVwb3J0VHlwZUZvcm0oKSB7XFxuXFx0XFx0XFx0XFx0dGhpcy51cGRhdGVSZXBvcnRUeXBlRm9ybSA9IHtcXG5cXHRcXHRcXHRcXHRcXHR0aXRsZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0dGl0bGVfY246ICcnLFxcblxcdFxcdFxcdFxcdFxcdGRlc2NyaXB0aW9uOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHRzZXJ2aWNlc19pZDogW10sXFxuXFx0XFx0XFx0XFx0XFx0d2l0aF9kb2NzOiBmYWxzZSxcXG5cXHRcXHRcXHRcXHRcXHRlcnJvcnM6ICcnXFxuXFx0XFx0XFx0XFx0fVxcblxcblxcdFxcdFxcdFxcdC8vIERlc2VsZWN0IEFsbFxcblxcdFxcdFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1zZXJ2aWNlcycpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcXG5cXHRcXHRcXHR9LFxcblxcbiAgICBcXHRcXHRsb2FkU2VydmljZXMoKSB7XFxuXFx0XFx0XFx0XFx0YXhpb3MuZ2V0KCcvdmlzYS9ub3RpZmljYXRpb24vc2VydmljZXMnKVxcblxcdFxcdFxcdFxcdFxcdC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLnNlcnZpY2VzID0gcmVzcG9uc2UuZGF0YTtcXG5cXG5cXHRcXHRcXHRcXHRcXHRcXHRzZXRUaW1lb3V0KCgpID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQkKFxcXCIuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXNcXFwiKS50cmlnZ2VyKFxcXCJjaG9zZW46dXBkYXRlZFxcXCIpO1xcblxcdFxcdFxcdFxcdFxcdFxcdH0sIDUwMCk7XFxuXFx0XFx0XFx0XFx0XFx0fSlcXG5cXHRcXHRcXHRcXHRcXHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdHVwZGF0ZUNob3NlbihzZXJ2aWNlc0lkKSB7XFxuXFx0XFx0XFx0XFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0JChcXFwiLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzXFxcIikudmFsKHNlcnZpY2VzSWQpLnRyaWdnZXIoXFxcImNob3Nlbjp1cGRhdGVkXFxcIik7XFxuXFx0XFx0XFx0XFx0fSwgNTAwKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGluaXRDaG9zZW5TZWxlY3QoKSB7XFxuXFx0ICAgICAgICBcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3Itc2VydmljZXMnKS5jaG9zZW4oe1xcblxcdFxcdFxcdFxcdFxcdHdpZHRoOiBcXFwiMTAwJVxcXCIsXFxuXFx0XFx0XFx0XFx0XFx0bm9fcmVzdWx0c190ZXh0OiBcXFwiTm8gcmVzdWx0L3MgZm91bmQuXFxcIixcXG5cXHRcXHRcXHRcXHRcXHRzZWFyY2hfY29udGFpbnM6IHRydWVcXG5cXHRcXHRcXHRcXHR9KTtcXG5cXG5cXHQgICAgICAgIFxcdHZhciB2bSA9IHRoaXM7XFxuXFxuXFx0XFx0XFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzJykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xcblxcdFxcdFxcdFxcdFxcdHZtLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLnNlcnZpY2VzX2lkID0gJCh0aGlzKS52YWwoKTtcXG5cXHRcXHRcXHRcXHR9KTtcXG5cXHQgICAgICAgIH1cXG5cXG4gICAgXFx0fSxcXG5cXG4gICAgXFx0Y3JlYXRlZCgpIHtcXG5cXHRcXHRcXHR0aGlzLmxvYWRTZXJ2aWNlcygpO1xcblxcdFxcdH0sXFxuXFxuXFx0XFx0bW91bnRlZCgpIHtcXG4gICAgICAgIFxcdHRoaXMuaW5pdENob3NlblNlbGVjdCgpO1xcbiAgICBcXHR9XFxuXFxuXFx0fVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNjEwNzA2MDFcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9VcGRhdGVSZXBvcnRUeXBlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoKTtcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcbmZvcm1bZGF0YS12LTcyODMzNjY0XSB7XFxuXFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxuLm0tci0xMFtkYXRhLXYtNzI4MzM2NjRdIHtcXG5cXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJVcGRhdGVEb2N1bWVudC52dWU/NTg2MGQzOTBcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQXVJQTtDQUNBLG9CQUFBO0NBQ0E7QUFDQTtDQUNBLG1CQUFBO0NBQ0FcIixcImZpbGVcIjpcIlVwZGF0ZURvY3VtZW50LnZ1ZVwiLFwic291cmNlc0NvbnRlbnRcIjpbXCI8dGVtcGxhdGU+XFxuXFx0XFxuXFx0PGZvcm0gY2xhc3M9XFxcImZvcm0taG9yaXpvbnRhbFxcXCIgQHN1Ym1pdC5wcmV2ZW50PVxcXCJ1cGRhdGVEb2N1bWVudFxcXCI+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiB1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpIH1cXFwiPlxcblxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgVGl0bGU6XFxuXFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdCAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwidGl0bGVcXFwiIHYtbW9kZWw9XFxcInVwZGF0ZURvY3VtZW50Rm9ybS50aXRsZVxcXCI+XFxuXFx0XFx0ICAgIFxcdDxwIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJ1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpXFxcIiB2LXRleHQ9XFxcInVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMudGl0bGVbMF1cXFwiPjwvcD5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiB1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpIH1cXFwiPlxcblxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdCAgICAgICAgVGl0bGUgQ046XFxuXFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdCAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwidGl0bGVfY25cXFwiIHYtbW9kZWw9XFxcInVwZGF0ZURvY3VtZW50Rm9ybS50aXRsZV9jblxcXCI+XFxuXFx0XFx0ICAgIFxcdDxwIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJ1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXFxcIiB2LXRleHQ9XFxcInVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMudGl0bGVfY25bMF1cXFwiPjwvcD5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0PGhyPlxcblxcblxcdFxcdFxcdDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIHB1bGwtbGVmdFxcXCIgQGNsaWNrPVxcXCJhZGREb2N1bWVudFR5cGVcXFwiPlxcblxcdFxcdFxcdFxcdDxpIGNsYXNzPVxcXCJmYSBmYS1wbHVzXFxcIj48L2k+IEFkZCBEb2N1bWVudCBUeXBlXFxuXFx0XFx0XFx0PC9idXR0b24+XFxuXFxuXFx0XFx0PGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjIwcHg7IGNsZWFyOmJvdGg7XFxcIj48L2Rpdj5cXG5cXG5cXHRcXHRcXHQ8ZGl2IHYtZm9yPVxcXCIoZG9jX3R5cGUsIGluZGV4KSBpbiB1cGRhdGVEb2N1bWVudEZvcm0uZG9jX3R5cGVzXFxcIj5cXG5cXG5cXHRcXHRcXHRcXHQ8aW5wdXQgdHlwZT1cXFwiaGlkZGVuXFxcIiB2LW1vZGVsPVxcXCJkb2NfdHlwZS5pZFxcXCI+XFxuXFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiB1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJytpbmRleCsnLnRpdGxlJykgfVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiY29sLW1kLTJcXFwiPlxcblxcdFxcdFxcdFxcdCAgICBcXHQ8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGFuZ2VyIHB1bGwtcmlnaHRcXFwiIEBjbGljaz1cXFwicmVtb3ZlRG9jdW1lbnRUeXBlKGluZGV4KVxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdFxcdDxpIGNsYXNzPVxcXCJmYSBmYS10aW1lc1xcXCI+PC9pPlxcblxcdFxcdFxcdFxcdCAgICBcXHQ8L2J1dHRvbj5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIE5hbWU6XFxuXFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLThcXFwiPlxcblxcdFxcdFxcdFxcdCAgICBcXHQ8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgdi1tb2RlbD1cXFwiZG9jX3R5cGUudGl0bGVcXFwiPlxcblxcdFxcdFxcdFxcdCAgICBcXHQ8cCBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgdi1pZj1cXFwidXBkYXRlRG9jdW1lbnRGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZG9jX3R5cGVzLicraW5kZXgrJy50aXRsZScpXFxcIj5UaGUgbmFtZSBmaWVsZCBpcyByZXF1aXJlZC48L3A+XFxuXFx0XFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXG5cXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiB1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJytpbmRleCsnLnRpdGxlX2NuJykgfVxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdDxkaXYgY2xhc3M9XFxcImNvbC1tZC0yXFxcIj4mbmJzcDs8L2Rpdj5cXG5cXHRcXHRcXHRcXHRcXHQ8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgTmFtZSBDTjpcXG5cXHRcXHRcXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtOFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiB2LW1vZGVsPVxcXCJkb2NfdHlwZS50aXRsZV9jblxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIFxcdDxwIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJ1cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJytpbmRleCsnLnRpdGxlX2NuJylcXFwiPlRoZSBuYW1lIGNuIGZpZWxkIGlzIHJlcXVpcmVkLjwvcD5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdDxkaXYgc3R5bGU9XFxcImhlaWdodDoyMHB4OyBjbGVhcjpib3RoO1xcXCI+PC9kaXY+XFxuXFxuXFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJzdWJtaXRcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCI+VXBkYXRlPC9idXR0b24+XFxuXFx0ICAgIDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBkYXRhLWRpc21pc3M9XFxcIm1vZGFsXFxcIj5DbG9zZTwvYnV0dG9uPlxcblxcblxcdDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFx0XFxuXFx0ZXhwb3J0IGRlZmF1bHQge1xcblxcblxcdFxcdHByb3BzOiBbJ2RvY3VtZW50aWQnXSxcXG5cXG5cXHRcXHRkYXRhKCkge1xcbiAgICBcXHRcXHRyZXR1cm4ge1xcbiAgICBcXHRcXHRcXHR1cGRhdGVEb2N1bWVudEZvcm06IHtcXG5cXHRcXHRcXHRcXHRcXHR0aXRsZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0dGl0bGVfY246ICcnLFxcblxcdFxcdFxcdFxcdFxcdGRvY190eXBlczogW10sXFxuXFx0XFx0XFx0XFx0XFx0ZXJyb3JzOiAnJ1xcblxcdFxcdFxcdFxcdH1cXG4gICAgXFx0XFx0fVxcbiAgICBcXHR9LFxcblxcbiAgICBcXHRtZXRob2RzOiB7XFxuXFxuICAgIFxcdFxcdGFkZERvY3VtZW50VHlwZSgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLnVwZGF0ZURvY3VtZW50Rm9ybS5kb2NfdHlwZXMucHVzaCh7IGlkOiAnJywgdGl0bGU6ICcnLCB0aXRsZV9jbjogJycgfSk7XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRyZW1vdmVEb2N1bWVudFR5cGUoaW5kZXgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLnVwZGF0ZURvY3VtZW50Rm9ybS5kb2NfdHlwZXMuc3BsaWNlKGluZGV4LCAxKTtcXG5cXHRcXHRcXHR9LFxcblxcbiAgICBcXHRcXHR1cGRhdGVEb2N1bWVudCgpIHtcXG5cXHRcXHRcXHRcXHRheGlvcy5wYXRjaCgnL3Zpc2Evbm90aWZpY2F0aW9uL2RvYy8nICsgdGhpcy5kb2N1bWVudGlkLCB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGl0bGU6IHRoaXMudXBkYXRlRG9jdW1lbnRGb3JtLnRpdGxlLFxcblxcdFxcdFxcdFxcdFxcdFxcdHRpdGxlX2NuOiB0aGlzLnVwZGF0ZURvY3VtZW50Rm9ybS50aXRsZV9jbixcXG5cXHRcXHRcXHRcXHRcXHRcXHRkb2NfdHlwZXM6IHRoaXMudXBkYXRlRG9jdW1lbnRGb3JtLmRvY190eXBlc1xcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdGlmKHJlc3BvbnNlLmRhdGEuc3VjY2Vzcykge1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuY2xlYXJVcGRhdGVEb2N1bWVudEZvcm0oKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0dGhpcy4kcGFyZW50LiRwYXJlbnQubG9hZERvY3MoKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0JCgnI3VwZGF0ZS1kb2N1bWVudC1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLmRhdGEubWVzc2FnZSk7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIH1cXG5cXHRcXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdFxcdC5jYXRjaChlcnJvciA9PiB0aGlzLnVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMgPSBlcnJvci5yZXNwb25zZS5kYXRhKTtcXG5cXHRcXHRcXHR9LFxcblxcbiAgICBcXHRcXHRjbGVhclVwZGF0ZURvY3VtZW50Rm9ybSgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLnVwZGF0ZURvY3VtZW50Rm9ybSA9IHtcXG5cXHRcXHRcXHRcXHRcXHR0aXRsZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0dGl0bGVfY246ICcnLFxcblxcdFxcdFxcdFxcdFxcdGRvY190eXBlczogW10sXFxuXFx0XFx0XFx0XFx0XFx0ZXJyb3JzOiAnJ1xcblxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHR9LFxcblxcbiAgICBcXHR9XFxuXFxuXFx0fVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNzI4MzM2NjRcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9VcGRhdGVEb2N1bWVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDMwNlxuLy8gbW9kdWxlIGNodW5rcyA9IDMiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi05NDgzMjJhMF0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTk0ODMyMmEwXSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiRGVsZXRlRG9jdW1lbnQudnVlPzBmZWVlOWZlXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUE0Q0E7Q0FDQSxvQkFBQTtDQUNBO0FBQ0E7Q0FDQSxtQkFBQTtDQUNBXCIsXCJmaWxlXCI6XCJEZWxldGVEb2N1bWVudC52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdFxcblxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwiZGVsZXRlRG9jdW1lbnRcXFwiPlxcblxcblxcdFxcdDxwPkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhpcyBkb2N1bWVudD88L3A+XFxuXFxuXFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJzdWJtaXRcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCI+RGVsZXRlPC9idXR0b24+XFxuXFx0ICAgIDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBkYXRhLWRpc21pc3M9XFxcIm1vZGFsXFxcIj5DbG9zZTwvYnV0dG9uPlxcblxcblxcdDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFx0XFxuXFx0ZXhwb3J0IGRlZmF1bHQge1xcblxcblxcdFxcdHByb3BzOiBbJ2RvY3VtZW50aWQnXSxcXG5cXG5cXHRcXHRtZXRob2RzOiB7XFxuXFxuXFx0XFx0XFx0ZGVsZXRlRG9jdW1lbnQoKSB7XFxuXFx0XFx0XFx0XFx0YXhpb3MuZGVsZXRlKCcvdmlzYS9ub3RpZmljYXRpb24vZG9jLycgKyB0aGlzLmRvY3VtZW50aWQpXFxuXFx0XFx0XFx0XFx0XFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdGlmKHJlc3BvbnNlLmRhdGEuc3VjY2Vzcykge1xcblxcdFxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LnNldFNlbGVjdGVkRG9jdW1lbnRJZChudWxsKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0dGhpcy4kcGFyZW50LiRwYXJlbnQubG9hZERvY3MoKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0JCgnI2RlbGV0ZS1kb2N1bWVudC1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLmRhdGEubWVzc2FnZSk7XFxuXFx0XFx0ICAgICAgICAgICAgICAgIH1cXG5cXHRcXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdFxcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xcblxcdFxcdFxcdH1cXG5cXG5cXHRcXHR9XFxuXFxuXFx0fVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtOTQ4MzIyYTBcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVEb2N1bWVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDMwOVxuLy8gbW9kdWxlIGNodW5rcyA9IDMiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi1kZmE3YzEzYV0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LWRmYTdjMTNhXSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiRGVsZXRlUmVwb3J0VHlwZS52dWU/N2ZmZDMwMzNcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQTRDQTtDQUNBLG9CQUFBO0NBQ0E7QUFDQTtDQUNBLG1CQUFBO0NBQ0FcIixcImZpbGVcIjpcIkRlbGV0ZVJlcG9ydFR5cGUudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXHRcXG5cXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIiBAc3VibWl0LnByZXZlbnQ9XFxcImRlbGV0ZVJlcG9ydFR5cGVcXFwiPlxcblxcblxcdFxcdDxwPkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhpcyByZXBvcnQgdHlwZT88L3A+XFxuXFxuXFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJzdWJtaXRcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCI+RGVsZXRlPC9idXR0b24+XFxuXFx0ICAgIDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBkYXRhLWRpc21pc3M9XFxcIm1vZGFsXFxcIj5DbG9zZTwvYnV0dG9uPlxcblxcblxcdDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFx0XFxuXFx0ZXhwb3J0IGRlZmF1bHQge1xcblxcblxcdFxcdHByb3BzOiBbJ3JlcG9ydHR5cGVpZCddLFxcblxcblxcdFxcdG1ldGhvZHM6IHtcXG5cXG5cXHRcXHRcXHRkZWxldGVSZXBvcnRUeXBlKCkge1xcblxcdFxcdFxcdFxcdGF4aW9zLmRlbGV0ZSgnL3Zpc2Evbm90aWZpY2F0aW9uL3JlcG9ydC10eXBlLycgKyB0aGlzLnJlcG9ydHR5cGVpZClcXG5cXHRcXHRcXHRcXHRcXHQudGhlbihyZXNwb25zZSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0aWYocmVzcG9uc2UuZGF0YS5zdWNjZXNzKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuc2V0U2VsZWN0ZWRSZXBvcnRUeXBlSWQobnVsbCk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmxvYWRSZXBvcnRUeXBlcygpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHQkKCcjZGVsZXRlLXJlcG9ydC10eXBlLW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UuZGF0YS5tZXNzYWdlKTtcXG5cXHRcXHQgICAgICAgICAgICAgICAgfVxcblxcdFxcdFxcdFxcdFxcdH0pXFxuXFx0XFx0XFx0XFx0XFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XFxuXFx0XFx0XFx0fVxcblxcblxcdFxcdH1cXG5cXG5cXHR9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1kZmE3YzEzYVwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0RlbGV0ZVJlcG9ydFR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzMTRcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtM2JkOWJmODJcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGREb2N1bWVudC52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQWRkRG9jdW1lbnQudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zYmQ5YmY4MlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9BZGREb2N1bWVudC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtM2JkOWJmODJcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGREb2N1bWVudC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBBZGREb2N1bWVudC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtM2JkOWJmODJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0zYmQ5YmY4MlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRG9jdW1lbnQudnVlXG4vLyBtb2R1bGUgaWQgPSAzMzNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMDY5MmY4OTZcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGRSZXBvcnRUeXBlLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9BZGRSZXBvcnRUeXBlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMDY5MmY4OTZcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQWRkUmVwb3J0VHlwZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtMDY5MmY4OTZcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGRSZXBvcnRUeXBlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEFkZFJlcG9ydFR5cGUudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTA2OTJmODk2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMDY5MmY4OTZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZFJlcG9ydFR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzMzdcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtOTQ4MzIyYTBcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9EZWxldGVEb2N1bWVudC52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRGVsZXRlRG9jdW1lbnQudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi05NDgzMjJhMFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9EZWxldGVEb2N1bWVudC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtOTQ4MzIyYTBcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVEb2N1bWVudC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBEZWxldGVEb2N1bWVudC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtOTQ4MzIyYTBcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi05NDgzMjJhMFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlRG9jdW1lbnQudnVlXG4vLyBtb2R1bGUgaWQgPSAzNDFcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZGZhN2MxM2FcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9EZWxldGVSZXBvcnRUeXBlLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9EZWxldGVSZXBvcnRUeXBlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZGZhN2MxM2FcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRGVsZXRlUmVwb3J0VHlwZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtZGZhN2MxM2FcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9EZWxldGVSZXBvcnRUeXBlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIERlbGV0ZVJlcG9ydFR5cGUudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWRmYTdjMTNhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtZGZhN2MxM2FcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0RlbGV0ZVJlcG9ydFR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzNDNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNzI4MzM2NjRcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9VcGRhdGVEb2N1bWVudC52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vVXBkYXRlRG9jdW1lbnQudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03MjgzMzY2NFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9VcGRhdGVEb2N1bWVudC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtNzI4MzM2NjRcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9VcGRhdGVEb2N1bWVudC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBVcGRhdGVEb2N1bWVudC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNzI4MzM2NjRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi03MjgzMzY2NFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvVXBkYXRlRG9jdW1lbnQudnVlXG4vLyBtb2R1bGUgaWQgPSAzNDlcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiXG4vKiBzdHlsZXMgKi9cbnJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjEwNzA2MDFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9VcGRhdGVSZXBvcnRUeXBlLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9VcGRhdGVSZXBvcnRUeXBlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjEwNzA2MDFcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vVXBkYXRlUmVwb3J0VHlwZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgXCJkYXRhLXYtNjEwNzA2MDFcIixcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9VcGRhdGVSZXBvcnRUeXBlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFVwZGF0ZVJlcG9ydFR5cGUudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTYxMDcwNjAxXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNjEwNzA2MDFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZVJlcG9ydFR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzNTBcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0uYWRkUmVwb3J0VHlwZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBUaXRsZTpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkUmVwb3J0VHlwZUZvcm0udGl0bGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJhZGRSZXBvcnRUeXBlRm9ybS50aXRsZVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcInRpdGxlXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uYWRkUmVwb3J0VHlwZUZvcm0udGl0bGUpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uYWRkUmVwb3J0VHlwZUZvcm0sIFwidGl0bGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGUnKSkgPyBfYygncCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5hZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMudGl0bGVbMF0pXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBUaXRsZSBDTjpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkUmVwb3J0VHlwZUZvcm0udGl0bGVfY24pLFxuICAgICAgZXhwcmVzc2lvbjogXCJhZGRSZXBvcnRUeXBlRm9ybS50aXRsZV9jblwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcInRpdGxlX2NuXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uYWRkUmVwb3J0VHlwZUZvcm0udGl0bGVfY24pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uYWRkUmVwb3J0VHlwZUZvcm0sIFwidGl0bGVfY25cIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGVfY24nKSkgPyBfYygncCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5hZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMudGl0bGVfY25bMF0pXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkZXNjcmlwdGlvbicpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBEZXNjcmlwdGlvbjpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkUmVwb3J0VHlwZUZvcm0uZGVzY3JpcHRpb24pLFxuICAgICAgZXhwcmVzc2lvbjogXCJhZGRSZXBvcnRUeXBlRm9ybS5kZXNjcmlwdGlvblwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcImRlc2NyaXB0aW9uXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uYWRkUmVwb3J0VHlwZUZvcm0uZGVzY3JpcHRpb24pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uYWRkUmVwb3J0VHlwZUZvcm0sIFwiZGVzY3JpcHRpb25cIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmFkZFJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZGVzY3JpcHRpb24nKSkgPyBfYygncCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5hZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMuZGVzY3JpcHRpb25bMF0pXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdzZXJ2aWNlc19pZCcpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBTZXJ2aWNlczpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnc2VsZWN0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwid2lkdGhcIjogXCIzNTBweFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLXBsYWNlaG9sZGVyXCI6IFwiU2VsZWN0IFNlcnZpY2VcIixcbiAgICAgIFwibXVsdGlwbGVcIjogXCJcIixcbiAgICAgIFwidGFiaW5kZXhcIjogXCI0XCJcbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLnNlcnZpY2VzKSwgZnVuY3Rpb24oc2VydmljZSkge1xuICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBzZXJ2aWNlLmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdCAgICAgICAgICAgICAgICBcIiArIF92bS5fcygoc2VydmljZS5kZXRhaWwpLnRyaW0oKSkgKyBcIlxcblxcdCAgICAgICAgICAgIFwiKV0pXG4gIH0pLCAwKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5hZGRSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3NlcnZpY2VzX2lkJykpID8gX2MoJ3AnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiLFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInRleHRDb250ZW50XCI6IF92bS5fcyhfdm0uYWRkUmVwb3J0VHlwZUZvcm0uZXJyb3JzLnNlcnZpY2VzX2lkWzBdKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHQgICAgICAgIFdpdGggRG9jczpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzKSxcbiAgICAgIGV4cHJlc3Npb246IFwiYWRkUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiY2hlY2tib3hcIixcbiAgICAgIFwibmFtZVwiOiBcIndpdGhfZG9jc1wiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IEFycmF5LmlzQXJyYXkoX3ZtLmFkZFJlcG9ydFR5cGVGb3JtLndpdGhfZG9jcykgPyBfdm0uX2koX3ZtLmFkZFJlcG9ydFR5cGVGb3JtLndpdGhfZG9jcywgbnVsbCkgPiAtMSA6IChfdm0uYWRkUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRhID0gX3ZtLmFkZFJlcG9ydFR5cGVGb3JtLndpdGhfZG9jcyxcbiAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAkJGMgPSAkJGVsLmNoZWNrZWQgPyAodHJ1ZSkgOiAoZmFsc2UpO1xuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSgkJGEpKSB7XG4gICAgICAgICAgdmFyICQkdiA9IG51bGwsXG4gICAgICAgICAgICAkJGkgPSBfdm0uX2koJCRhLCAkJHYpO1xuICAgICAgICAgIGlmICgkJGVsLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICQkaSA8IDAgJiYgKF92bS4kc2V0KF92bS5hZGRSZXBvcnRUeXBlRm9ybSwgXCJ3aXRoX2RvY3NcIiwgJCRhLmNvbmNhdChbJCR2XSkpKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkJGkgPiAtMSAmJiAoX3ZtLiRzZXQoX3ZtLmFkZFJlcG9ydFR5cGVGb3JtLCBcIndpdGhfZG9jc1wiLCAkJGEuc2xpY2UoMCwgJCRpKS5jb25jYXQoJCRhLnNsaWNlKCQkaSArIDEpKSkpXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF92bS4kc2V0KF92bS5hZGRSZXBvcnRUeXBlRm9ybSwgXCJ3aXRoX2RvY3NcIiwgJCRjKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJBZGRcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMDY5MmY4OTZcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0wNjkyZjg5NlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZFJlcG9ydFR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzNzdcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0uYWRkRG9jKCRldmVudClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIixcbiAgICBjbGFzczoge1xuICAgICAgJ2hhcy1lcnJvcic6IF92bS5hZGREb2NGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGUnKVxuICAgIH1cbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdCAgICAgICAgVGl0bGU6XFxuICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmFkZERvY0Zvcm0udGl0bGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJhZGREb2NGb3JtLnRpdGxlXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJuYW1lXCI6IFwidGl0bGVcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5hZGREb2NGb3JtLnRpdGxlKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmFkZERvY0Zvcm0sIFwidGl0bGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmFkZERvY0Zvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpKSA/IF9jKCdwJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLmFkZERvY0Zvcm0uZXJyb3JzLnRpdGxlWzBdKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnaGFzLWVycm9yJzogX3ZtLmFkZERvY0Zvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBUaXRsZSBDTjpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWRkRG9jRm9ybS50aXRsZV9jbiksXG4gICAgICBleHByZXNzaW9uOiBcImFkZERvY0Zvcm0udGl0bGVfY25cIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJ0aXRsZV9jblwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmFkZERvY0Zvcm0udGl0bGVfY24pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uYWRkRG9jRm9ybSwgXCJ0aXRsZV9jblwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIChfdm0uYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlX2NuJykpID8gX2MoJ3AnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiLFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInRleHRDb250ZW50XCI6IF92bS5fcyhfdm0uYWRkRG9jRm9ybS5lcnJvcnMudGl0bGVfY25bMF0pXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2hyJyksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIHB1bGwtbGVmdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLmFkZERvY3VtZW50VHlwZVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXBsdXNcIlxuICB9KSwgX3ZtLl92KFwiIEFkZCBEb2N1bWVudCBUeXBlXFxuXFx0XFx0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMjBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF92bS5fbCgoX3ZtLmFkZERvY0Zvcm0uZG9jX3R5cGVzKSwgZnVuY3Rpb24oZG9jX3R5cGUsIGluZGV4KSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIixcbiAgICAgIGNsYXNzOiB7XG4gICAgICAgICdoYXMtZXJyb3InOiBfdm0uYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2RvY190eXBlcy4nICsgaW5kZXggKyAnLnRpdGxlJylcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yXCJcbiAgICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGFuZ2VyIHB1bGwtcmlnaHRcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBfdm0ucmVtb3ZlRG9jdW1lbnRUeXBlKGluZGV4KVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW19jKCdpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtdGltZXNcIlxuICAgIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xhYmVsJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgTmFtZTpcXG5cXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLThcIlxuICAgIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGRvY190eXBlLnRpdGxlKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJkb2NfdHlwZS50aXRsZVwiXG4gICAgICB9XSxcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiAoZG9jX3R5cGUudGl0bGUpXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgICAgX3ZtLiRzZXQoZG9jX3R5cGUsIFwidGl0bGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmFkZERvY0Zvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJyArIGluZGV4ICsgJy50aXRsZScpKSA/IF9jKCdwJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiXG4gICAgfSwgW192bS5fdihcIlRoZSBuYW1lIGZpZWxkIGlzIHJlcXVpcmVkLlwiKV0pIDogX3ZtLl9lKCldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgY2xhc3M6IHtcbiAgICAgICAgJ2hhcy1lcnJvcic6IF92bS5hZGREb2NGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZG9jX3R5cGVzLicgKyBpbmRleCArICcudGl0bGVfY24nKVxuICAgICAgfVxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTJcIlxuICAgIH0sIFtfdm0uX3YoXCLCoFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGFiZWwnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgICBOYW1lIENOOlxcblxcdFxcdCAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtOFwiXG4gICAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgIHZhbHVlOiAoZG9jX3R5cGUudGl0bGVfY24pLFxuICAgICAgICBleHByZXNzaW9uOiBcImRvY190eXBlLnRpdGxlX2NuXCJcbiAgICAgIH1dLFxuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJ0ZXh0XCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IChkb2NfdHlwZS50aXRsZV9jbilcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgICBfdm0uJHNldChkb2NfdHlwZSwgXCJ0aXRsZV9jblwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSksIF92bS5fdihcIiBcIiksIChfdm0uYWRkRG9jRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ2RvY190eXBlcy4nICsgaW5kZXggKyAnLnRpdGxlX2NuJykpID8gX2MoJ3AnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCJcbiAgICB9LCBbX3ZtLl92KFwiVGhlIG5hbWUgY24gZmllbGQgaXMgcmVxdWlyZWQuXCIpXSkgOiBfdm0uX2UoKV0pXSldKVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCIyMHB4XCIsXG4gICAgICBcImNsZWFyXCI6IFwiYm90aFwiXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJzdWJtaXRcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkFkZFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldLCAyKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0zYmQ5YmY4MlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTNiZDliZjgyXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvQWRkRG9jdW1lbnQudnVlXG4vLyBtb2R1bGUgaWQgPSAzODdcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0udXBkYXRlUmVwb3J0VHlwZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZScpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBUaXRsZTpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS50aXRsZVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcInRpdGxlXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGUpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0sIFwidGl0bGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGUnKSkgPyBfYygncCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS51cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMudGl0bGVbMF0pXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBUaXRsZSBDTjpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGVfY24pLFxuICAgICAgZXhwcmVzc2lvbjogXCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS50aXRsZV9jblwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcInRpdGxlX2NuXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0udGl0bGVfY24pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0sIFwidGl0bGVfY25cIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgndGl0bGVfY24nKSkgPyBfYygncCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS51cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMudGl0bGVfY25bMF0pXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkZXNjcmlwdGlvbicpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBEZXNjcmlwdGlvbjpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0uZGVzY3JpcHRpb24pLFxuICAgICAgZXhwcmVzc2lvbjogXCJ1cGRhdGVSZXBvcnRUeXBlRm9ybS5kZXNjcmlwdGlvblwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcImRlc2NyaXB0aW9uXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0uZGVzY3JpcHRpb24pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0sIFwiZGVzY3JpcHRpb25cIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eSgnZGVzY3JpcHRpb24nKSkgPyBfYygncCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS51cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMuZGVzY3JpcHRpb25bMF0pXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdzZXJ2aWNlc19pZCcpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBTZXJ2aWNlczpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnc2VsZWN0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNob3Nlbi1zZWxlY3QtZm9yLXNlcnZpY2VzXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwid2lkdGhcIjogXCIzNTBweFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLXBsYWNlaG9sZGVyXCI6IFwiU2VsZWN0IFNlcnZpY2VcIixcbiAgICAgIFwibXVsdGlwbGVcIjogXCJcIixcbiAgICAgIFwidGFiaW5kZXhcIjogXCI0XCJcbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLnNlcnZpY2VzKSwgZnVuY3Rpb24oc2VydmljZSkge1xuICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBzZXJ2aWNlLmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdCAgICAgICAgICAgICAgICBcIiArIF92bS5fcygoc2VydmljZS5kZXRhaWwpLnRyaW0oKSkgKyBcIlxcblxcdCAgICAgICAgICAgIFwiKV0pXG4gIH0pLCAwKSwgX3ZtLl92KFwiIFwiKSwgKF92bS51cGRhdGVSZXBvcnRUeXBlRm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3NlcnZpY2VzX2lkJykpID8gX2MoJ3AnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiLFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInRleHRDb250ZW50XCI6IF92bS5fcyhfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0uZXJyb3JzLnNlcnZpY2VzX2lkWzBdKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHQgICAgICAgIFdpdGggRG9jczpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzKSxcbiAgICAgIGV4cHJlc3Npb246IFwidXBkYXRlUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiY2hlY2tib3hcIixcbiAgICAgIFwibmFtZVwiOiBcIndpdGhfZG9jc1wiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IEFycmF5LmlzQXJyYXkoX3ZtLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLndpdGhfZG9jcykgPyBfdm0uX2koX3ZtLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLndpdGhfZG9jcywgbnVsbCkgPiAtMSA6IChfdm0udXBkYXRlUmVwb3J0VHlwZUZvcm0ud2l0aF9kb2NzKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRhID0gX3ZtLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLndpdGhfZG9jcyxcbiAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAkJGMgPSAkJGVsLmNoZWNrZWQgPyAodHJ1ZSkgOiAoZmFsc2UpO1xuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSgkJGEpKSB7XG4gICAgICAgICAgdmFyICQkdiA9IG51bGwsXG4gICAgICAgICAgICAkJGkgPSBfdm0uX2koJCRhLCAkJHYpO1xuICAgICAgICAgIGlmICgkJGVsLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICQkaSA8IDAgJiYgKF92bS4kc2V0KF92bS51cGRhdGVSZXBvcnRUeXBlRm9ybSwgXCJ3aXRoX2RvY3NcIiwgJCRhLmNvbmNhdChbJCR2XSkpKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkJGkgPiAtMSAmJiAoX3ZtLiRzZXQoX3ZtLnVwZGF0ZVJlcG9ydFR5cGVGb3JtLCBcIndpdGhfZG9jc1wiLCAkJGEuc2xpY2UoMCwgJCRpKS5jb25jYXQoJCRhLnNsaWNlKCQkaSArIDEpKSkpXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF92bS4kc2V0KF92bS51cGRhdGVSZXBvcnRUeXBlRm9ybSwgXCJ3aXRoX2RvY3NcIiwgJCRjKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJVcGRhdGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNjEwNzA2MDFcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi02MTA3MDYwMVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZVJlcG9ydFR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSA0MDJcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taG9yaXpvbnRhbFwiLFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHJldHVybiBfdm0udXBkYXRlRG9jdW1lbnQoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnaGFzLWVycm9yJzogX3ZtLnVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlJylcbiAgICB9XG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHQgICAgICAgIFRpdGxlOlxcbiAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS51cGRhdGVEb2N1bWVudEZvcm0udGl0bGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJ1cGRhdGVEb2N1bWVudEZvcm0udGl0bGVcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJ0aXRsZVwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLnVwZGF0ZURvY3VtZW50Rm9ybS50aXRsZSlcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS51cGRhdGVEb2N1bWVudEZvcm0sIFwidGl0bGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMuaGFzT3duUHJvcGVydHkoJ3RpdGxlJykpID8gX2MoJ3AnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiLFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInRleHRDb250ZW50XCI6IF92bS5fcyhfdm0udXBkYXRlRG9jdW1lbnRGb3JtLmVycm9ycy50aXRsZVswXSlcbiAgICB9XG4gIH0pIDogX3ZtLl9lKCldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIixcbiAgICBjbGFzczoge1xuICAgICAgJ2hhcy1lcnJvcic6IF92bS51cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICBUaXRsZSBDTjpcXG4gICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0udXBkYXRlRG9jdW1lbnRGb3JtLnRpdGxlX2NuKSxcbiAgICAgIGV4cHJlc3Npb246IFwidXBkYXRlRG9jdW1lbnRGb3JtLnRpdGxlX2NuXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJuYW1lXCI6IFwidGl0bGVfY25cIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS51cGRhdGVEb2N1bWVudEZvcm0udGl0bGVfY24pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0udXBkYXRlRG9jdW1lbnRGb3JtLCBcInRpdGxlX2NuXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgKF92bS51cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCd0aXRsZV9jbicpKSA/IF9jKCdwJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLnVwZGF0ZURvY3VtZW50Rm9ybS5lcnJvcnMudGl0bGVfY25bMF0pXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2hyJyksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIHB1bGwtbGVmdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLmFkZERvY3VtZW50VHlwZVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXBsdXNcIlxuICB9KSwgX3ZtLl92KFwiIEFkZCBEb2N1bWVudCBUeXBlXFxuXFx0XFx0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMjBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF92bS5fbCgoX3ZtLnVwZGF0ZURvY3VtZW50Rm9ybS5kb2NfdHlwZXMpLCBmdW5jdGlvbihkb2NfdHlwZSwgaW5kZXgpIHtcbiAgICByZXR1cm4gX2MoJ2RpdicsIFtfYygnaW5wdXQnLCB7XG4gICAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICB2YWx1ZTogKGRvY190eXBlLmlkKSxcbiAgICAgICAgZXhwcmVzc2lvbjogXCJkb2NfdHlwZS5pZFwiXG4gICAgICB9XSxcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidHlwZVwiOiBcImhpZGRlblwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiAoZG9jX3R5cGUuaWQpXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgICAgX3ZtLiRzZXQoZG9jX3R5cGUsIFwiaWRcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgY2xhc3M6IHtcbiAgICAgICAgJ2hhcy1lcnJvcic6IF92bS51cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJyArIGluZGV4ICsgJy50aXRsZScpXG4gICAgICB9XG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMlwiXG4gICAgfSwgW19jKCdidXR0b24nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRhbmdlciBwdWxsLXJpZ2h0XCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLnJlbW92ZURvY3VtZW50VHlwZShpbmRleClcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIFtfYygnaScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXRpbWVzXCJcbiAgICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgIE5hbWU6XFxuXFx0XFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC04XCJcbiAgICB9LCBbX2MoJ2lucHV0Jywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgdmFsdWU6IChkb2NfdHlwZS50aXRsZSksXG4gICAgICAgIGV4cHJlc3Npb246IFwiZG9jX3R5cGUudGl0bGVcIlxuICAgICAgfV0sXG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidHlwZVwiOiBcInRleHRcIlxuICAgICAgfSxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogKGRvY190eXBlLnRpdGxlKVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICAgIF92bS4kc2V0KGRvY190eXBlLCBcInRpdGxlXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KFwiIFwiKSwgKF92bS51cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJyArIGluZGV4ICsgJy50aXRsZScpKSA/IF9jKCdwJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiXG4gICAgfSwgW192bS5fdihcIlRoZSBuYW1lIGZpZWxkIGlzIHJlcXVpcmVkLlwiKV0pIDogX3ZtLl9lKCldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgY2xhc3M6IHtcbiAgICAgICAgJ2hhcy1lcnJvcic6IF92bS51cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJyArIGluZGV4ICsgJy50aXRsZV9jbicpXG4gICAgICB9XG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMlwiXG4gICAgfSwgW192bS5fdihcIsKgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgIE5hbWUgQ046XFxuXFx0XFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC04XCJcbiAgICB9LCBbX2MoJ2lucHV0Jywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgdmFsdWU6IChkb2NfdHlwZS50aXRsZV9jbiksXG4gICAgICAgIGV4cHJlc3Npb246IFwiZG9jX3R5cGUudGl0bGVfY25cIlxuICAgICAgfV0sXG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidHlwZVwiOiBcInRleHRcIlxuICAgICAgfSxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogKGRvY190eXBlLnRpdGxlX2NuKVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICAgIF92bS4kc2V0KGRvY190eXBlLCBcInRpdGxlX2NuXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KFwiIFwiKSwgKF92bS51cGRhdGVEb2N1bWVudEZvcm0uZXJyb3JzLmhhc093blByb3BlcnR5KCdkb2NfdHlwZXMuJyArIGluZGV4ICsgJy50aXRsZV9jbicpKSA/IF9jKCdwJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiXG4gICAgfSwgW192bS5fdihcIlRoZSBuYW1lIGNuIGZpZWxkIGlzIHJlcXVpcmVkLlwiKV0pIDogX3ZtLl9lKCldKV0pXSlcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMjBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJVcGRhdGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSwgMilcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNzI4MzM2NjRcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi03MjgzMzY2NFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZURvY3VtZW50LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDA3XG4vLyBtb2R1bGUgY2h1bmtzID0gMyIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmRlbGV0ZURvY3VtZW50KCRldmVudClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygncCcsIFtfdm0uX3YoXCJBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoaXMgZG9jdW1lbnQ/XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJEZWxldGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtOTQ4MzIyYTBcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi05NDgzMjJhMFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0RlbGV0ZURvY3VtZW50LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDEzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmRlbGV0ZVJlcG9ydFR5cGUoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdwJywgW192bS5fdihcIkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhpcyByZXBvcnQgdHlwZT9cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJzdWJtaXRcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkRlbGV0ZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1kZmE3YzEzYVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWRmYTdjMTNhXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlUmVwb3J0VHlwZS52dWVcbi8vIG1vZHVsZSBpZCA9IDQyM1xuLy8gbW9kdWxlIGNodW5rcyA9IDMiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTA2OTJmODk2XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vQWRkUmVwb3J0VHlwZS52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjMwODhkZGU0XCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wNjkyZjg5NlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZFJlcG9ydFR5cGUudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wNjkyZjg5NlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZFJlcG9ydFR5cGUudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMDY5MmY4OTZcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9BZGRSZXBvcnRUeXBlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDMwXG4vLyBtb2R1bGUgY2h1bmtzID0gMyIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtM2JkOWJmODJcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGREb2N1bWVudC52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjFlYmM3OGU2XCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zYmQ5YmY4MlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0FkZERvY3VtZW50LnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtM2JkOWJmODJcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9BZGREb2N1bWVudC52dWVcIik7XG4gICAgIGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuICAgICB1cGRhdGUobmV3Q29udGVudCk7XG4gICB9KTtcbiB9XG4gLy8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtc3R5bGUtbG9hZGVyIS4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zYmQ5YmY4MlwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0FkZERvY3VtZW50LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDM4XG4vLyBtb2R1bGUgY2h1bmtzID0gMyIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjEwNzA2MDFcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9VcGRhdGVSZXBvcnRUeXBlLnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiMzA0NDVmY2FcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTYxMDcwNjAxXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vVXBkYXRlUmVwb3J0VHlwZS52dWVcIiwgZnVuY3Rpb24oKSB7XG4gICAgIHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTYxMDcwNjAxXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vVXBkYXRlUmVwb3J0VHlwZS52dWVcIik7XG4gICAgIGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuICAgICB1cGRhdGUobmV3Q29udGVudCk7XG4gICB9KTtcbiB9XG4gLy8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtc3R5bGUtbG9hZGVyIS4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi02MTA3MDYwMVwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL1VwZGF0ZVJlcG9ydFR5cGUudnVlXG4vLyBtb2R1bGUgaWQgPSA0NDFcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03MjgzMzY2NFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL1VwZGF0ZURvY3VtZW50LnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiMjFjMGFhNWNcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTcyODMzNjY0XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vVXBkYXRlRG9jdW1lbnQudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03MjgzMzY2NFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL1VwZGF0ZURvY3VtZW50LnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTcyODMzNjY0XCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvVXBkYXRlRG9jdW1lbnQudnVlXG4vLyBtb2R1bGUgaWQgPSA0NDRcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi05NDgzMjJhMFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0RlbGV0ZURvY3VtZW50LnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiNzljNzcyYzhcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTk0ODMyMmEwXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vRGVsZXRlRG9jdW1lbnQudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi05NDgzMjJhMFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0RlbGV0ZURvY3VtZW50LnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTk0ODMyMmEwXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlRG9jdW1lbnQudnVlXG4vLyBtb2R1bGUgaWQgPSA0NDdcbi8vIG1vZHVsZSBjaHVua3MgPSAzIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1kZmE3YzEzYVxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0RlbGV0ZVJlcG9ydFR5cGUudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCIwZjRhYjQzOFwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZGZhN2MxM2FcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9EZWxldGVSZXBvcnRUeXBlLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZGZhN2MxM2FcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9EZWxldGVSZXBvcnRUeXBlLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWRmYTdjMTNhXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRGVsZXRlUmVwb3J0VHlwZS52dWVcbi8vIG1vZHVsZSBpZCA9IDQ1MlxuLy8gbW9kdWxlIGNodW5rcyA9IDMiLCIvKipcbiAqIFRyYW5zbGF0ZXMgdGhlIGxpc3QgZm9ybWF0IHByb2R1Y2VkIGJ5IGNzcy1sb2FkZXIgaW50byBzb21ldGhpbmdcbiAqIGVhc2llciB0byBtYW5pcHVsYXRlLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGxpc3RUb1N0eWxlcyAocGFyZW50SWQsIGxpc3QpIHtcbiAgdmFyIHN0eWxlcyA9IFtdXG4gIHZhciBuZXdTdHlsZXMgPSB7fVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgaXRlbSA9IGxpc3RbaV1cbiAgICB2YXIgaWQgPSBpdGVtWzBdXG4gICAgdmFyIGNzcyA9IGl0ZW1bMV1cbiAgICB2YXIgbWVkaWEgPSBpdGVtWzJdXG4gICAgdmFyIHNvdXJjZU1hcCA9IGl0ZW1bM11cbiAgICB2YXIgcGFydCA9IHtcbiAgICAgIGlkOiBwYXJlbnRJZCArICc6JyArIGksXG4gICAgICBjc3M6IGNzcyxcbiAgICAgIG1lZGlhOiBtZWRpYSxcbiAgICAgIHNvdXJjZU1hcDogc291cmNlTWFwXG4gICAgfVxuICAgIGlmICghbmV3U3R5bGVzW2lkXSkge1xuICAgICAgc3R5bGVzLnB1c2gobmV3U3R5bGVzW2lkXSA9IHsgaWQ6IGlkLCBwYXJ0czogW3BhcnRdIH0pXG4gICAgfSBlbHNlIHtcbiAgICAgIG5ld1N0eWxlc1tpZF0ucGFydHMucHVzaChwYXJ0KVxuICAgIH1cbiAgfVxuICByZXR1cm4gc3R5bGVzXG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvbGlzdFRvU3R5bGVzLmpzXG4vLyBtb2R1bGUgaWQgPSA3XG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiXSwic291cmNlUm9vdCI6IiJ9