/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 485);
/******/ })
/************************************************************************/
/******/ ({

/***/ 195:
/***/ (function(module, exports) {

var manager = new Vue({
    el: '#manager',
    data: {
        clients: [],
        services: [],
        reportType: [],
        docs: [],
        selected: [],
        selectedReportType: [],
        selectedDocs: [],
        reportForm: new Form({
            id: '',
            message: ''
        }, { baseURL: 'http://' + window.Laravel.cpanel_url })
    },
    methods: {
        addClient: function addClient() {
            var rt = $(".select2").val();
            var clids = $("#selService").attr("cl-ids");
            if (rt == "") {
                toastr.error("Please select report type!");
            } else if (clids == "") {
                toastr.error("Please select client first!");
            } else {
                var href = "/visa/notification/select-services/" + clids + '/' + rt;
                window.location.href = href;
            }
        },
        showServices: function showServices(id, e) {
            if (e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#client-' + id).toggleClass('hide');
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },
        selectServices: function selectServices(item) {
            if (this.selected.indexOf(item) > -1) {
                indx = this.selected.indexOf(item);
                this.selected.splice(indx, 1);
            } else {
                this.selected.push(item);
            }
        },
        selectDocs: function selectDocs(item) {
            if (this.selectedDocs.indexOf(item) > -1) {
                indx = this.selectedDocs.indexOf(item);
                this.selectedDocs.splice(indx, 1);
            } else {
                this.selectedDocs.push(item);
            }
        },
        saveReport: function saveReport() {
            var _this = this;

            if (this.selected.length != 0) {

                var id = '';
                $.each(this.selected, function (key, value) {
                    id = value.id + ';' + id;
                });

                this.reportForm.id = id;

                if (this.selectedReportType.with_docs == '1') {

                    if (this.selectedDocs.length != 0) {

                        var title = '';
                        $.each(this.selectedDocs, function (key, value) {
                            title = value.title + ', ' + title;
                        });

                        var title = title.slice(0, -2);

                        this.reportForm.message = this.selectedReportType.description + ' with the following document/s: ' + title + '.';

                        this.reportForm.submit('post', '/visa/notification/save-report').then(function (result) {
                            toastr.success('New Report Added.');
                            _this.selected = [];
                            _this.selectedDocs = [];
                            $('input:checkbox').prop('checked', false);
                        });
                    } else toastr.error("Please select documents first!");
                } else {

                    this.reportForm.message = this.selectedReportType.description;

                    this.reportForm.submit('post', '/visa/notification/save-report').then(function (result) {
                        toastr.success('New Report Added.');
                        _this.selected = [];
                        _this.selectedDocs = [];
                        $('input:checkbox').prop('checked', false);
                    });
                }
            } else toastr.error("Please select service first!");
        },
        hideService: function hideService(data) {
            if (this.selectedReportType.length != 0) {
                var c = 0;
                var a = this.selectedReportType.services_id;
                var s = a.split(',');
                $.each(s, function (i, item) {
                    if (s[i] == data.service_id) {
                        c++;
                    }
                });
                return c;
            }
        }
    },
    created: function created() {
        var _this2 = this;

        axios.get('/visa/notification/getReportType').then(function (response) {
            _this2.reportType = response.data;

            $(".select2").select2({
                placeholder: "Select report type",
                allowClear: true
            });
        });

        axios.get('/visa/notification/get-clients/' + window.Laravel.data).then(function (response) {
            _this2.clients = response.data;

            axios.get('/visa/notification/getSelectedReportType/' + window.Laravel.message).then(function (response) {
                _this2.selectedReportType = response.data[0];
            });
        });

        axios.get('/visa/notification/doc').then(function (response) {
            _this2.docs = response.data;
        });
    }
});

$(document).ready(function () {

    $('#lists').DataTable({
        "ajax": "/storage/data/ClientsReport.txt",
        responsive: true,
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "iDisplayLength": 10
    });
});

/***/ }),

/***/ 485:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(195);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9ub3RpZmljYXRpb25zL21hbmFnZXIuanMiXSwibmFtZXMiOlsibWFuYWdlciIsIlZ1ZSIsImVsIiwiZGF0YSIsImNsaWVudHMiLCJzZXJ2aWNlcyIsInJlcG9ydFR5cGUiLCJkb2NzIiwic2VsZWN0ZWQiLCJzZWxlY3RlZFJlcG9ydFR5cGUiLCJzZWxlY3RlZERvY3MiLCJyZXBvcnRGb3JtIiwiRm9ybSIsImlkIiwibWVzc2FnZSIsImJhc2VVUkwiLCJ3aW5kb3ciLCJMYXJhdmVsIiwiY3BhbmVsX3VybCIsIm1ldGhvZHMiLCJhZGRDbGllbnQiLCJydCIsIiQiLCJ2YWwiLCJjbGlkcyIsImF0dHIiLCJ0b2FzdHIiLCJlcnJvciIsImhyZWYiLCJsb2NhdGlvbiIsInNob3dTZXJ2aWNlcyIsImUiLCJ0YXJnZXQiLCJub2RlTmFtZSIsInRvZ2dsZUNsYXNzIiwic2VsZWN0U2VydmljZXMiLCJpdGVtIiwiaW5kZXhPZiIsImluZHgiLCJzcGxpY2UiLCJwdXNoIiwic2VsZWN0RG9jcyIsInNhdmVSZXBvcnQiLCJsZW5ndGgiLCJlYWNoIiwia2V5IiwidmFsdWUiLCJ3aXRoX2RvY3MiLCJ0aXRsZSIsInNsaWNlIiwiZGVzY3JpcHRpb24iLCJzdWJtaXQiLCJ0aGVuIiwic3VjY2VzcyIsInByb3AiLCJoaWRlU2VydmljZSIsImMiLCJhIiwic2VydmljZXNfaWQiLCJzIiwic3BsaXQiLCJpIiwic2VydmljZV9pZCIsImNyZWF0ZWQiLCJheGlvcyIsImdldCIsInJlc3BvbnNlIiwic2VsZWN0MiIsInBsYWNlaG9sZGVyIiwiYWxsb3dDbGVhciIsImRvY3VtZW50IiwicmVhZHkiLCJEYXRhVGFibGUiLCJyZXNwb25zaXZlIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBLElBQUlBLFVBQVUsSUFBSUMsR0FBSixDQUFRO0FBQ3JCQyxRQUFHLFVBRGtCO0FBRXJCQyxVQUFLO0FBQ0pDLGlCQUFRLEVBREo7QUFFRUMsa0JBQVMsRUFGWDtBQUdFQyxvQkFBVyxFQUhiO0FBSUVDLGNBQUssRUFKUDtBQUtFQyxrQkFBUyxFQUxYO0FBTUVDLDRCQUFtQixFQU5yQjtBQU9FQyxzQkFBYSxFQVBmO0FBUUVDLG9CQUFXLElBQUlDLElBQUosQ0FBUztBQUNsQkMsZ0JBQUksRUFEYztBQUVsQkMscUJBQVM7QUFGUyxTQUFULEVBR1IsRUFBRUMsU0FBUyxZQUFVQyxPQUFPQyxPQUFQLENBQWVDLFVBQXBDLEVBSFE7QUFSYixLQUZnQjtBQWVyQkMsYUFBUztBQUNSQyxpQkFEUSx1QkFDRztBQUNELGdCQUFJQyxLQUFLQyxFQUFFLFVBQUYsRUFBY0MsR0FBZCxFQUFUO0FBQ04sZ0JBQUlDLFFBQVFGLEVBQUUsYUFBRixFQUFpQkcsSUFBakIsQ0FBc0IsUUFBdEIsQ0FBWjtBQUNNLGdCQUFHSixNQUFJLEVBQVAsRUFBVTtBQUNOSyx1QkFBT0MsS0FBUCxDQUFhLDRCQUFiO0FBQ1QsYUFGSyxNQUVDLElBQUlILFNBQU8sRUFBWCxFQUFjO0FBQ1hFLHVCQUFPQyxLQUFQLENBQWEsNkJBQWI7QUFDSCxhQUZBLE1BRUs7QUFDWCxvQkFBSUMsT0FBTyx3Q0FBc0NKLEtBQXRDLEdBQTRDLEdBQTVDLEdBQWdESCxFQUEzRDtBQUNBTCx1QkFBT2EsUUFBUCxDQUFnQkQsSUFBaEIsR0FBdUJBLElBQXZCO0FBQ0E7QUFDSixTQVpPO0FBYUZFLG9CQWJFLHdCQWFXakIsRUFiWCxFQWFla0IsQ0FiZixFQWFrQjtBQUNoQixnQkFBR0EsRUFBRUMsTUFBRixDQUFTQyxRQUFULElBQXFCLFFBQXJCLElBQWlDRixFQUFFQyxNQUFGLENBQVNDLFFBQVQsSUFBcUIsTUFBekQsRUFBaUU7QUFDN0RYLGtCQUFFLGFBQWFULEVBQWYsRUFBbUJxQixXQUFuQixDQUErQixNQUEvQjtBQUNBWixrQkFBRSxTQUFTVCxFQUFYLEVBQWVxQixXQUFmLENBQTJCLGVBQTNCO0FBQ0g7QUFDSixTQWxCQztBQW1CRkMsc0JBbkJFLDBCQW1CYUMsSUFuQmIsRUFtQmtCO0FBQ2hCLGdCQUFHLEtBQUs1QixRQUFMLENBQWM2QixPQUFkLENBQXNCRCxJQUF0QixJQUE0QixDQUFDLENBQWhDLEVBQ0E7QUFDSUUsdUJBQU8sS0FBSzlCLFFBQUwsQ0FBYzZCLE9BQWQsQ0FBc0JELElBQXRCLENBQVA7QUFDQSxxQkFBSzVCLFFBQUwsQ0FBYytCLE1BQWQsQ0FBcUJELElBQXJCLEVBQTJCLENBQTNCO0FBRUgsYUFMRCxNQU1BO0FBQ0kscUJBQUs5QixRQUFMLENBQWNnQyxJQUFkLENBQW1CSixJQUFuQjtBQUNIO0FBQ0osU0E3QkM7QUE4QkZLLGtCQTlCRSxzQkE4QlNMLElBOUJULEVBOEJjO0FBQ1osZ0JBQUcsS0FBSzFCLFlBQUwsQ0FBa0IyQixPQUFsQixDQUEwQkQsSUFBMUIsSUFBZ0MsQ0FBQyxDQUFwQyxFQUNBO0FBQ0lFLHVCQUFPLEtBQUs1QixZQUFMLENBQWtCMkIsT0FBbEIsQ0FBMEJELElBQTFCLENBQVA7QUFDQSxxQkFBSzFCLFlBQUwsQ0FBa0I2QixNQUFsQixDQUF5QkQsSUFBekIsRUFBK0IsQ0FBL0I7QUFFSCxhQUxELE1BTUE7QUFDSSxxQkFBSzVCLFlBQUwsQ0FBa0I4QixJQUFsQixDQUF1QkosSUFBdkI7QUFDSDtBQUNKLFNBeENDO0FBeUNGTSxrQkF6Q0Usd0JBeUNVO0FBQUE7O0FBQ1IsZ0JBQUcsS0FBS2xDLFFBQUwsQ0FBY21DLE1BQWQsSUFBc0IsQ0FBekIsRUFBMkI7O0FBRXZCLG9CQUFJOUIsS0FBSyxFQUFUO0FBQ0FTLGtCQUFFc0IsSUFBRixDQUFRLEtBQUtwQyxRQUFiLEVBQXVCLFVBQVVxQyxHQUFWLEVBQWVDLEtBQWYsRUFBdUI7QUFDNUNqQyx5QkFBS2lDLE1BQU1qQyxFQUFOLEdBQVcsR0FBWCxHQUFpQkEsRUFBdEI7QUFDRCxpQkFGRDs7QUFJQSxxQkFBS0YsVUFBTCxDQUFnQkUsRUFBaEIsR0FBbUJBLEVBQW5COztBQUVBLG9CQUFHLEtBQUtKLGtCQUFMLENBQXdCc0MsU0FBeEIsSUFBcUMsR0FBeEMsRUFBNEM7O0FBRXhDLHdCQUFHLEtBQUtyQyxZQUFMLENBQWtCaUMsTUFBbEIsSUFBMEIsQ0FBN0IsRUFBK0I7O0FBRTNCLDRCQUFJSyxRQUFRLEVBQVo7QUFDQTFCLDBCQUFFc0IsSUFBRixDQUFRLEtBQUtsQyxZQUFiLEVBQTJCLFVBQVVtQyxHQUFWLEVBQWVDLEtBQWYsRUFBdUI7QUFDaERFLG9DQUFRRixNQUFNRSxLQUFOLEdBQWMsSUFBZCxHQUFxQkEsS0FBN0I7QUFDRCx5QkFGRDs7QUFJQSw0QkFBSUEsUUFBUUEsTUFBTUMsS0FBTixDQUFZLENBQVosRUFBZSxDQUFDLENBQWhCLENBQVo7O0FBRUEsNkJBQUt0QyxVQUFMLENBQWdCRyxPQUFoQixHQUEwQixLQUFLTCxrQkFBTCxDQUF3QnlDLFdBQXhCLEdBQXNDLGtDQUF0QyxHQUEyRUYsS0FBM0UsR0FBbUYsR0FBN0c7O0FBRUEsNkJBQUtyQyxVQUFMLENBQWdCd0MsTUFBaEIsQ0FBdUIsTUFBdkIsRUFBK0IsZ0NBQS9CLEVBQ0NDLElBREQsQ0FDTSxrQkFBVTtBQUNaMUIsbUNBQU8yQixPQUFQLENBQWUsbUJBQWY7QUFDQSxrQ0FBSzdDLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxrQ0FBS0UsWUFBTCxHQUFvQixFQUFwQjtBQUNBWSw4QkFBRSxnQkFBRixFQUFvQmdDLElBQXBCLENBQXlCLFNBQXpCLEVBQW1DLEtBQW5DO0FBQ0gseUJBTkQ7QUFPSCxxQkFsQkQsTUFvQkk1QixPQUFPQyxLQUFQLENBQWEsZ0NBQWI7QUFFUCxpQkF4QkQsTUF3Qk87O0FBRUMseUJBQUtoQixVQUFMLENBQWdCRyxPQUFoQixHQUEwQixLQUFLTCxrQkFBTCxDQUF3QnlDLFdBQWxEOztBQUVBLHlCQUFLdkMsVUFBTCxDQUFnQndDLE1BQWhCLENBQXVCLE1BQXZCLEVBQStCLGdDQUEvQixFQUNDQyxJQURELENBQ00sa0JBQVU7QUFDWjFCLCtCQUFPMkIsT0FBUCxDQUFlLG1CQUFmO0FBQ0EsOEJBQUs3QyxRQUFMLEdBQWdCLEVBQWhCO0FBQ0EsOEJBQUtFLFlBQUwsR0FBb0IsRUFBcEI7QUFDQVksMEJBQUUsZ0JBQUYsRUFBb0JnQyxJQUFwQixDQUF5QixTQUF6QixFQUFtQyxLQUFuQztBQUNILHFCQU5EO0FBT1A7QUFDSixhQTdDRCxNQStDSTVCLE9BQU9DLEtBQVAsQ0FBYSw4QkFBYjtBQUNQLFNBMUZDO0FBNEZGNEIsbUJBNUZFLHVCQTRGVXBELElBNUZWLEVBNEZlO0FBQ2IsZ0JBQUcsS0FBS00sa0JBQUwsQ0FBd0JrQyxNQUF4QixJQUFrQyxDQUFyQyxFQUF1QztBQUNuQyxvQkFBSWEsSUFBSSxDQUFSO0FBQ0Esb0JBQUlDLElBQUksS0FBS2hELGtCQUFMLENBQXdCaUQsV0FBaEM7QUFDQSxvQkFBSUMsSUFBSUYsRUFBRUcsS0FBRixDQUFRLEdBQVIsQ0FBUjtBQUNBdEMsa0JBQUVzQixJQUFGLENBQU9lLENBQVAsRUFBVSxVQUFTRSxDQUFULEVBQVl6QixJQUFaLEVBQWtCO0FBQzFCLHdCQUFHdUIsRUFBRUUsQ0FBRixLQUFRMUQsS0FBSzJELFVBQWhCLEVBQTJCO0FBQ3pCTjtBQUNEO0FBQ0YsaUJBSkQ7QUFLQSx1QkFBT0EsQ0FBUDtBQUNIO0FBQ0o7QUF4R0MsS0FmWTtBQTBIckJPLFdBMUhxQixxQkEwSFo7QUFBQTs7QUFFRkMsY0FBTUMsR0FBTixDQUFVLGtDQUFWLEVBQ0NiLElBREQsQ0FDTSxvQkFBWTtBQUNkLG1CQUFLOUMsVUFBTCxHQUFrQjRELFNBQVMvRCxJQUEzQjs7QUFFQW1CLGNBQUUsVUFBRixFQUFjNkMsT0FBZCxDQUFzQjtBQUNsQkMsNkJBQWEsb0JBREs7QUFFbEJDLDRCQUFZO0FBRk0sYUFBdEI7QUFJSCxTQVJEOztBQVVITCxjQUFNQyxHQUFOLENBQVUsb0NBQWtDakQsT0FBT0MsT0FBUCxDQUFlZCxJQUEzRCxFQUNDaUQsSUFERCxDQUNNLG9CQUFZO0FBQ2QsbUJBQUtoRCxPQUFMLEdBQWU4RCxTQUFTL0QsSUFBeEI7O0FBRUc2RCxrQkFBTUMsR0FBTixDQUFVLDhDQUE0Q2pELE9BQU9DLE9BQVAsQ0FBZUgsT0FBckUsRUFDQ3NDLElBREQsQ0FDTSxvQkFBWTtBQUNkLHVCQUFLM0Msa0JBQUwsR0FBMEJ5RCxTQUFTL0QsSUFBVCxDQUFjLENBQWQsQ0FBMUI7QUFDSCxhQUhEO0FBSU4sU0FSRDs7QUFVRzZELGNBQU1DLEdBQU4sQ0FBVSx3QkFBVixFQUNDYixJQURELENBQ00sb0JBQVk7QUFDZCxtQkFBSzdDLElBQUwsR0FBWTJELFNBQVMvRCxJQUFyQjtBQUNILFNBSEQ7QUFJTjtBQXBKb0IsQ0FBUixDQUFkOztBQXVKQW1CLEVBQUVnRCxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVzs7QUFFekJqRCxNQUFFLFFBQUYsRUFBWWtELFNBQVosQ0FBc0I7QUFDckIsZ0JBQVEsaUNBRGE7QUFFbEJDLG9CQUFZLElBRk07QUFHbEIsc0JBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxDQUFELEVBQWUsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBZixDQUhJO0FBSWxCLDBCQUFrQjtBQUpBLEtBQXRCO0FBT0gsQ0FURCxFIiwiZmlsZSI6ImpzL3Zpc2Evbm90aWZpY2F0aW9ucy9tYW5hZ2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0ODUpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGYxMTNlZDM2YTVhNTZiN2RjZjYzIiwidmFyIG1hbmFnZXIgPSBuZXcgVnVlKHtcblx0ZWw6JyNtYW5hZ2VyJyxcblx0ZGF0YTp7XG5cdFx0Y2xpZW50czpbXSxcbiAgICAgICAgc2VydmljZXM6W10sXG4gICAgICAgIHJlcG9ydFR5cGU6W10sXG4gICAgICAgIGRvY3M6W10sXG4gICAgICAgIHNlbGVjdGVkOltdLFxuICAgICAgICBzZWxlY3RlZFJlcG9ydFR5cGU6W10sXG4gICAgICAgIHNlbGVjdGVkRG9jczpbXSxcbiAgICAgICAgcmVwb3J0Rm9ybTpuZXcgRm9ybSh7XG4gICAgICAgICAgaWQ6ICcnLFxuICAgICAgICAgIG1lc3NhZ2U6ICcnLFxuICAgICAgICB9LCB7IGJhc2VVUkw6ICdodHRwOi8vJyt3aW5kb3cuTGFyYXZlbC5jcGFuZWxfdXJsIH0pXG5cdH0sXG5cdG1ldGhvZHM6IHtcblx0XHRhZGRDbGllbnQoKXtcbiAgICAgICAgICAgIHZhciBydCA9ICQoXCIuc2VsZWN0MlwiKS52YWwoKTtcblx0XHQgICAgdmFyIGNsaWRzID0gJChcIiNzZWxTZXJ2aWNlXCIpLmF0dHIoXCJjbC1pZHNcIik7XG4gICAgICAgICAgICBpZihydD09XCJcIil7XG4gICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKFwiUGxlYXNlIHNlbGVjdCByZXBvcnQgdHlwZSFcIik7XG5cdFx0ICAgIH0gZWxzZSBpZiAoY2xpZHM9PVwiXCIpe1xuICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcihcIlBsZWFzZSBzZWxlY3QgY2xpZW50IGZpcnN0IVwiKTtcbiAgICAgICAgICAgIH0gZWxzZXtcblx0XHRcdCAgICB2YXIgaHJlZiA9IFwiL3Zpc2Evbm90aWZpY2F0aW9uL3NlbGVjdC1zZXJ2aWNlcy9cIitjbGlkcysnLycrcnQ7XG5cdFx0XHQgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBocmVmO1xuXHRcdCAgICB9XG5cdFx0fSxcbiAgICAgICAgc2hvd1NlcnZpY2VzKGlkLCBlKSB7XG4gICAgICAgICAgICBpZihlLnRhcmdldC5ub2RlTmFtZSAhPSAnQlVUVE9OJyAmJiBlLnRhcmdldC5ub2RlTmFtZSAhPSAnU1BBTicpIHtcbiAgICAgICAgICAgICAgICAkKCcjY2xpZW50LScgKyBpZCkudG9nZ2xlQ2xhc3MoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAkKCcjZmEtJyArIGlkKS50b2dnbGVDbGFzcygnZmEtYXJyb3ctZG93bicpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBzZWxlY3RTZXJ2aWNlcyhpdGVtKXtcbiAgICAgICAgICAgIGlmKHRoaXMuc2VsZWN0ZWQuaW5kZXhPZihpdGVtKT4tMSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpbmR4ID0gdGhpcy5zZWxlY3RlZC5pbmRleE9mKGl0ZW0pO1xuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWQuc3BsaWNlKGluZHgsIDEpO1xuXG4gICAgICAgICAgICB9ZWxzZVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWQucHVzaChpdGVtKTsgXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHNlbGVjdERvY3MoaXRlbSl7XG4gICAgICAgICAgICBpZih0aGlzLnNlbGVjdGVkRG9jcy5pbmRleE9mKGl0ZW0pPi0xKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGluZHggPSB0aGlzLnNlbGVjdGVkRG9jcy5pbmRleE9mKGl0ZW0pO1xuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWREb2NzLnNwbGljZShpbmR4LCAxKTtcblxuICAgICAgICAgICAgfWVsc2VcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkRG9jcy5wdXNoKGl0ZW0pOyBcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgc2F2ZVJlcG9ydCgpe1xuICAgICAgICAgICAgaWYodGhpcy5zZWxlY3RlZC5sZW5ndGghPTApe1xuXG4gICAgICAgICAgICAgICAgdmFyIGlkID0gJydcbiAgICAgICAgICAgICAgICAkLmVhY2goIHRoaXMuc2VsZWN0ZWQsIGZ1bmN0aW9uKCBrZXksIHZhbHVlICkge1xuICAgICAgICAgICAgICAgICAgaWQgPSB2YWx1ZS5pZCArICc7JyArIGlkO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5yZXBvcnRGb3JtLmlkPWlkO1xuXG4gICAgICAgICAgICAgICAgaWYodGhpcy5zZWxlY3RlZFJlcG9ydFR5cGUud2l0aF9kb2NzID09ICcxJyl7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5zZWxlY3RlZERvY3MubGVuZ3RoIT0wKXtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRpdGxlID0gJydcbiAgICAgICAgICAgICAgICAgICAgICAgICQuZWFjaCggdGhpcy5zZWxlY3RlZERvY3MsIGZ1bmN0aW9uKCBrZXksIHZhbHVlICkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZSA9IHZhbHVlLnRpdGxlICsgJywgJyArIHRpdGxlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0aXRsZSA9IHRpdGxlLnNsaWNlKDAsIC0yKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXBvcnRGb3JtLm1lc3NhZ2UgPSB0aGlzLnNlbGVjdGVkUmVwb3J0VHlwZS5kZXNjcmlwdGlvbiArICcgd2l0aCB0aGUgZm9sbG93aW5nIGRvY3VtZW50L3M6ICcgKyB0aXRsZSArICcuJztcblxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXBvcnRGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ub3RpZmljYXRpb24vc2F2ZS1yZXBvcnQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnTmV3IFJlcG9ydCBBZGRlZC4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZERvY3MgPSBbXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCdpbnB1dDpjaGVja2JveCcpLnByb3AoJ2NoZWNrZWQnLGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcihcIlBsZWFzZSBzZWxlY3QgZG9jdW1lbnRzIGZpcnN0IVwiKTsgICAgICAgICAgICAgICAgICAgIFxuXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXBvcnRGb3JtLm1lc3NhZ2UgPSB0aGlzLnNlbGVjdGVkUmVwb3J0VHlwZS5kZXNjcmlwdGlvbjtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXBvcnRGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9ub3RpZmljYXRpb24vc2F2ZS1yZXBvcnQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnTmV3IFJlcG9ydCBBZGRlZC4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZERvY3MgPSBbXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCdpbnB1dDpjaGVja2JveCcpLnByb3AoJ2NoZWNrZWQnLGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoXCJQbGVhc2Ugc2VsZWN0IHNlcnZpY2UgZmlyc3QhXCIpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGhpZGVTZXJ2aWNlKGRhdGEpe1xuICAgICAgICAgICAgaWYodGhpcy5zZWxlY3RlZFJlcG9ydFR5cGUubGVuZ3RoICE9IDApe1xuICAgICAgICAgICAgICAgIHZhciBjID0gMDtcbiAgICAgICAgICAgICAgICB2YXIgYSA9IHRoaXMuc2VsZWN0ZWRSZXBvcnRUeXBlLnNlcnZpY2VzX2lkO1xuICAgICAgICAgICAgICAgIHZhciBzID0gYS5zcGxpdCgnLCcpO1xuICAgICAgICAgICAgICAgICQuZWFjaChzLCBmdW5jdGlvbihpLCBpdGVtKSB7XG4gICAgICAgICAgICAgICAgICBpZihzW2ldID09IGRhdGEuc2VydmljZV9pZCl7XG4gICAgICAgICAgICAgICAgICAgIGMrKztcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICByZXR1cm4gYztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cdH0sXG5cdGNyZWF0ZWQoKXtcblxuICAgICAgICBheGlvcy5nZXQoJy92aXNhL25vdGlmaWNhdGlvbi9nZXRSZXBvcnRUeXBlJylcbiAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgdGhpcy5yZXBvcnRUeXBlID0gcmVzcG9uc2UuZGF0YTtcblxuICAgICAgICAgICAgJChcIi5zZWxlY3QyXCIpLnNlbGVjdDIoe1xuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIlNlbGVjdCByZXBvcnQgdHlwZVwiLFxuICAgICAgICAgICAgICAgIGFsbG93Q2xlYXI6IHRydWVcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuXHQgICAgYXhpb3MuZ2V0KCcvdmlzYS9ub3RpZmljYXRpb24vZ2V0LWNsaWVudHMvJyt3aW5kb3cuTGFyYXZlbC5kYXRhKVxuXHQgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHQgICAgICAgIHRoaXMuY2xpZW50cyA9IHJlc3BvbnNlLmRhdGE7XG5cbiAgICAgICAgICAgIGF4aW9zLmdldCgnL3Zpc2Evbm90aWZpY2F0aW9uL2dldFNlbGVjdGVkUmVwb3J0VHlwZS8nK3dpbmRvdy5MYXJhdmVsLm1lc3NhZ2UpXG4gICAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFJlcG9ydFR5cGUgPSByZXNwb25zZS5kYXRhWzBdO1xuICAgICAgICAgICAgfSk7ICAgICAgICAgICAgXG5cdCAgICB9KTtcblxuICAgICAgICBheGlvcy5nZXQoJy92aXNhL25vdGlmaWNhdGlvbi9kb2MnKVxuICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICB0aGlzLmRvY3MgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICB9KTtcblx0fSxcbn0pO1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcblxuICAgICQoJyNsaXN0cycpLkRhdGFUYWJsZSh7XG4gICAgXHRcImFqYXhcIjogXCIvc3RvcmFnZS9kYXRhL0NsaWVudHNSZXBvcnQudHh0XCIsXG4gICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG4gICAgICAgIFwibGVuZ3RoTWVudVwiOiBbWzEwLCAyNSwgNTBdLCBbMTAsIDI1LCA1MF1dLFxuICAgICAgICBcImlEaXNwbGF5TGVuZ3RoXCI6IDEwXG4gICAgfSk7XG5cbn0gKTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL25vdGlmaWNhdGlvbnMvbWFuYWdlci5qcyJdLCJzb3VyY2VSb290IjoiIn0=