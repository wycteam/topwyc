/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 478);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 188:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('dialog-modal', __webpack_require__(5));

var data = window.Laravel.user;

var dashboard = new Vue({
    el: '#dashboard',
    data: {
        pendingServices: [],
        onprocessServices: [],
        todaysReports: [],
        lastReportedDate: [],
        todaysServices: [],
        deliverySchedules: [],
        service: '',
        setting: data.access_control[0].setting,
        permissions: data.permissions,
        form: new Form({
            id: '',
            reason: ''
        }, { baseURL: 'http://' + window.Laravel.cpanel_url }),
        addSchedule: new Form({
            id: '',
            item: '',
            rider_id: '',
            location: '',
            scheduled_date: '',
            scheduled_time: ''
        }, { baseURL: '/visa/home/' }),
        scheduled_hour: '',
        scheduled_minute: '',
        companyCouriers: [],
        dt: '',
        selId: '',
        mode: ''
    },
    methods: {
        callDataTables1: function callDataTables1() {
            var _this = this;

            $('#pendingLists').DataTable().destroy();

            axios.get('/visa/home/pendingServices').then(function (result) {
                _this.pendingServices = result.data.filter(function (r) {
                    return r.active == 1;
                });

                setTimeout(function () {
                    $('#pendingLists').DataTable({
                        responsive: true,
                        order: [],
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }, 2000);
            });
        },
        callDataTables2: function callDataTables2() {
            var _this2 = this;

            $('#onprocessLists').DataTable().destroy();

            axios.get('/visa/home/onprocessServices').then(function (result) {
                _this2.onprocessServices = result.data;

                setTimeout(function () {
                    $('#onprocessLists').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }, 2000);
            });
        },
        callDataTables3: function callDataTables3() {
            var _this3 = this;

            $('#todaysReportLists').DataTable().destroy();

            axios.get('/visa/report/get-reports/').then(function (response) {
                _this3.todaysReports = response.data;
                _this3.createDatatableToggle(_this3.todaysReports, '#todaysReportLists');
                // this.createDatatableToggle(this.todaysReports,'#todaysReportLists');
            });
        },
        callDataTables4: function callDataTables4() {
            var _this4 = this;

            $('#lastReportedDateLists').DataTable().destroy();

            axios.get('/visa/report/get-reports-yesterday/').then(function (response) {
                _this4.lastReportedDate = response.data;

                _this4.createDatatableToggle(_this4.lastReportedDate, '#lastReportedDateLists');
                // this.createDatatableToggle(this.lastReportedDate,'#lastReportedDateLists');
            });
        },
        callDataTables5: function callDataTables5() {
            var _this5 = this;

            $('#todaysServicesLists').DataTable().destroy();
            params = {
                date: 0
            };
            axios.get('/visa/home/get-service-byDate', {
                params: params
            }).then(function (response) {
                _this5.todaysServices = response.data;
                setTimeout(function () {
                    $('#todaysServicesLists').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }, 2000);
                // this.createDatatableToggle(this.todaysServices,'#todaysServicesLists');
            });
        },
        callDataTables6: function callDataTables6() {
            var _this6 = this;

            $('#deliverySchedule').DataTable().destroy();
            params = {
                date: 0
            };
            axios.get('/visa/home/getSchedulesbyDate', {
                params: params
            }).then(function (response) {
                _this6.deliverySchedules = response.data;
                setTimeout(function () {
                    $('#deliverySchedule').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }, 2000);
                // this.createDatatableToggle(this.todaysServices,'#todaysServicesLists');
            });
        },
        ifToday: function ifToday(date) {
            var today = moment().format('YYYY-MM-DD');
            if (today == date) {
                return 'Due Today';
            }
            // return moment(this.reminder.extend).fromNow();
            return 'Due on ' + date;
        },


        //     createDatatableToggle(reports,tablename) {

        //         $(tablename).DataTable().destroy();
        //         // $(tablename).DataTable().destroy();

        //         setTimeout(function() {

        //             var format = function(d) {
        //                 console.log(tablename);
        //                 var ids = '';
        //                 var clids = [];
        //                 for(var i=0; i<(d.client).length; i++) {
        //                     var found = jQuery.inArray(d.client[i].client_id, clids);
        //                      // console.log(found);
        //                     if (found < 0) {
        //                         // Element was not found, add it.
        //                         clids.push(d.client[i].client_id);
        //                         ids += '<a href="/visa/client/'+d.client[i].client_id+'" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="'+d.client[i].fullname +'" data-trigger="hover">'+d.client[i].client_id +'</a>'+",";
        //                     }
        //                     // console.log(clids);
        //                     // if(i != ((d.client).length - 1) ){
        //                     //     ids = ids+",";
        //                     // }

        //                 }
        //                 // ids = rtrim(ids,',');
        //                 return '<table cellpadding="5" cellspacing="0" class="table table-bordered table-striped table-hover" border="1" >'+
        //                             '<tr style="padding-left:50px;">'+
        //                                 '<td width="200"><b>Processor : </b>'+d.processor+'</td>'+
        //                                 '<td><b>Client IDs : </b> '+ids+'</td>'+
        //                             '</tr>'+
        //                         '</table>';
        //             };

        //             table2 = $(tablename).DataTable( {
        //                 "data": reports,
        //                 "columns": [
        //                     {
        //                         "width": "5%",
        //                         "className":      'service-control text-center',
        //                         "orderable":      false,
        //                         "data":           null,
        //                         "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
        //                     },
        //                     { 
        //                         "width": "20%",
        //                         data: null, render: function(reports, type, row) {
        //                             if(reports.type == "report"){
        //                                 return '<b style="color: #1ab394;">' + reports.service + '</b>'
        //                             }
        //                             return '<b style="color: black;">' + reports.service + '</b>'
        //                         }
        //                     },
        //                     {
        //                         "width": "15%",
        //                         data: null, render: function(reports, type, row) {
        //                             var sid = '';
        //                             for(var i=0; i<(reports.client).length; i++) {
        //                                 sid += reports.client[i].client_id+",";
        //                             }
        //                             return '<b>'+reports.log_date+ '</b><span style="display:none;">'+sid+'</span>'
        //                         }
        //                     },
        //                     {
        //                         "width": "40%",
        //                         data: null, render: function(reports, type, row, meta) {
        //                             return '<b>'+reports.detail+ '</b>'
        //                         }
        //                     },
        //                 ]
        //             });

        // $('body').off('click', 'table'+tablename+' tbody td.service-control');
        // 	// $(tablename+' tbody').on('click', 'td.service-control', function (e) {
        //             $('body').on('click', 'table'+tablename+' tbody td.service-control', function (e) {
        //                 var tr2 = $(this).closest('tr');
        //                 var row = table2.row( tr2 );

        //                 if(row.child.isShown()) {
        //                     row.child.hide();
        //                     //tr2.removeClass('shown');
        //                 } else {
        //                     row.child( format(row.data()) ).show();
        //                     //tr2.addClass('shown');
        //                 }
        //             });

        //         }.bind(this), 1000);

        //     },

        createDatatableToggle: function createDatatableToggle(reports, tablename) {

            $(tablename).DataTable().destroy();

            setTimeout(function () {

                var format = function format(d) {
                    // console.log(d);
                    var ids = '';
                    var clids = [];
                    for (var i = 0; i < d.client.length; i++) {
                        var found = jQuery.inArray(d.client[i].client_id, clids);
                        // console.log(found);
                        if (found < 0) {
                            // Element was not found, add it.
                            clids.push(d.client[i].client_id);
                            ids += '<a href="/visa/client/' + d.client[i].client_id + '" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="' + d.client[i].fullname + '" data-trigger="hover">' + d.client[i].client_id + '</a>' + ",";
                        }
                        // console.log(clids);
                        // if(i != ((d.client).length - 1) ){
                        //     ids = ids+",";
                        // }
                    }
                    // ids = rtrim(ids,',');
                    return '<table cellpadding="5" cellspacing="0" class="table table-striped table-hover dtchildrow" >' + '<tr style="padding-left:50px;">' + '<td><b>Report : </b>' + d.detail + '</td>' +
                    // '<td><b>Client IDs : </b> '+ids+'</td>'+
                    '</tr>' + '</table>';
                };

                table2 = $(tablename).DataTable({
                    "data": reports,
                    responsive: true,
                    "columns": [
                    // {
                    //     "width": "5%",
                    //     "className":      'service-control text-center',
                    //     "orderable":      false,
                    //     "data":           null,
                    //     "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
                    // },
                    {
                        "width": "40%",
                        "className": 'service-control',
                        data: null, render: function render(reports, type, row) {
                            if (reports.type == "report") {
                                return '<b style="color: #1ab394;">' + reports.service + '</b>';
                            }
                            return '<b style="color: black;">' + reports.service + '</b>';
                        }
                    }, {
                        "className": 'service-control',
                        "width": "13%",
                        data: null, render: function render(reports, type, row) {
                            var sid = '';
                            for (var i = 0; i < reports.client.length; i++) {
                                sid += reports.client[i].client_id + ",";
                            }
                            return '<b>' + reports.log_date + '</b><span style="display:none;">' + sid + '</span>';
                        }
                    }, {
                        "className": 'service-control',
                        "width": "12%",
                        data: null, render: function render(reports, type, row, meta) {
                            return '<b>' + reports.processor + '</b>';
                        }
                    }, {
                        "width": "35%",
                        "className": 'service-control wrapok',
                        // "class": "wrapnot",
                        data: null, render: function render(reports, type, row, meta) {
                            var ids = '';
                            var clids = [];
                            for (var i = 0; i < reports.client.length; i++) {
                                var found = jQuery.inArray(reports.client[i].client_id, clids);
                                // console.log(found);
                                if (found < 0) {
                                    // Element was not found, add it.
                                    clids.push(reports.client[i].client_id);
                                    ids += '<a href="/visa/client/' + reports.client[i].client_id + '" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="' + reports.client[i].client_id + '" data-trigger="hover">' + reports.client[i].fullname + '</a>' + " , ";
                                }
                                // console.log(clids);
                                // if(i != ((d.client).length - 1) ){
                                //     ids = ids+",";
                                // }
                            }
                            return ids;
                        }
                    }]
                });
                $('body').off('click', 'table' + tablename + ' tbody td.service-control');
                $('body').on('click', 'table' + tablename + ' tbody td.service-control', function (e) {
                    var tr2 = $(this).closest('tr');
                    var row = table2.row(tr2);

                    if (row.child.isShown()) {
                        row.child.hide();
                        tr2.removeClass('shown');
                    } else {
                        row.child(format(row.data())).show();
                        tr2.addClass('shown');
                    }
                });
                //EXPAND ALL CHILD ROWS BY DEFAULT
                table2.rows().every(function () {
                    // If row has details collapsed
                    if (!this.child.isShown()) {
                        // Open this row
                        this.child(format(this.data())).show();
                        $(this.node()).addClass('shown');
                    }
                });

                // Handle click on "Expand All" button
                $('#btn-show-all-children').on('click', function () {
                    // Enumerate all rows
                    table2.rows().every(function () {
                        // If row has details collapsed
                        if (!this.child.isShown()) {
                            // Open this row
                            this.child(format(this.data())).show();
                            $(this.node()).addClass('shown');
                        }
                    });
                });

                // Handle click on "Collapse All" button
                $('#btn-hide-all-children').on('click', function () {
                    // Enumerate all rows
                    table2.rows().every(function () {
                        // If row has details expanded
                        if (this.child.isShown()) {
                            // Collapse row details
                            this.child.hide();
                            $(this.node()).removeClass('shown');
                        }
                    });
                });
            }.bind(this), 2000);
        },
        openProcessService: function openProcessService(service) {
            this.service = service;
            $('#dialogRemove').modal('toggle');
        },
        removeService: function removeService() {
            var _this7 = this;

            axios.get('/visa/home/removeService/' + this.service.id).then(function (result) {
                _this7.callDataTables2();
                toastr.success('Service successfully removed.');
            });
        },
        openPendingService: function openPendingService(service) {
            this.service = service;
            this.form.id = this.service.id;
            $('#dialogReason').modal('toggle');
        },
        markAsPending: function markAsPending() {
            var _this8 = this;

            this.form.submit('post', '/visa/home/markAsPending').then(function (result) {
                _this8.callDataTables1();
                _this8.callDataTables2();
                toastr.success('Reason successfully saved.');
                $('#dialogReason').modal('toggle');
                _this8.service = '';
                _this8.form.id = '';
                _this8.form.reason = '';
            });
        },
        setService: function setService(service) {
            var id = service.id,
                tracking = service.tracking,
                tip = service.tip,
                status = service.status,
                cost = service.cost,
                active = service.active,
                extend = service.extend,
                rcv_docs = service.rcv_docs,
                docs_needed = service.docs_needed,
                docs_optional = service.docs_optional;


            if (service.discount2.length == 0) {
                discount = '';
                reason = '';
            } else {
                discount = service.discount2[0].discount_amount;
                reason = service.discount2[0].reason;
            }

            this.$refs.editservicedashboardref.setService(id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs);
            this.$refs.editservicedashboardref.fetchDocs(docs_needed, docs_optional);
        },
        submitFilter: function submitFilter() {
            var _this9 = this;

            alert();

            $('#todaysServicesLists').DataTable().destroy();
            params = {
                date: $('form#date-range-form input[name=start]').val()
            };
            axios.get('/visa/home/get-service-byDate', {
                params: params
            }).then(function (response) {
                _this9.todaysServices = response.data;
                setTimeout(function () {
                    $('#todaysServicesLists').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }, 2000);
            });
        },
        submitFilter2: function submitFilter2(val) {
            var _this10 = this;

            $('#todaysServicesLists').DataTable().destroy();
            if (val == 1) {
                $('#yesterday-serv').removeClass('btn-outline');
                $('#today-serv').addClass('btn-outline');
            } else {
                $('#today-serv').removeClass('btn-outline');
                $('#yesterday-serv').addClass('btn-outline');
            }
            params = {
                date: val
            };
            axios.get('/visa/home/get-service-byDate', {
                params: params
            }).then(function (response) {
                _this10.todaysServices = response.data;
                setTimeout(function () {
                    $('#todaysServicesLists').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }, 2000);
            });
        },
        submitFilter3: function submitFilter3(val) {
            var _this11 = this;

            $('#deliverySchedule').DataTable().destroy();
            if (val == 1) {
                $('#tom-sched').removeClass('btn-outline');
                $('#today-sched').addClass('btn-outline');
            } else if (val == 0) {
                $('#today-sched').removeClass('btn-outline');
                $('#tom-sched').addClass('btn-outline');
            } else {
                val = $('form#date-range-form2 input[name=start2]').val();
            }
            params = {
                date: val
            };
            axios.get('/visa/home/getSchedulesbyDate', {
                params: params
            }).then(function (response) {
                _this11.deliverySchedules = response.data;
                setTimeout(function () {
                    $('#deliverySchedule').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }, 2000);
            });
        },
        showScheduleModal: function showScheduleModal(val, id) {
            var _this12 = this;

            if (val == "add") {
                $('#addScheduleModal').modal('toggle');
                this.mode = "Add";
            } else if (val == "edit") {
                axios.post('/visa/home/getSched/' + id).then(function (result) {
                    _this12.addSchedule = new Form({
                        id: result.data.id,
                        item: result.data.item,
                        rider_id: result.data.rider_id,
                        location: result.data.location,
                        scheduled_date: result.data.scheduled_date,
                        scheduled_time: result.data.scheduled_time
                    }, { baseURL: '/visa/home/' });
                    _this12.dt = result.data.scheduled_date;
                    $('form#frm_deliverysched .input[name=date]').val(result.data.scheduled_date);
                    _this12.mode = "Edit";
                });
                $('#addScheduleModal').modal('toggle');
            }
        },
        showDeleteSchedPrompt: function showDeleteSchedPrompt(val) {
            this.selId = val;
            $('#deleteSchedPrompt').modal('toggle');
        },
        loadCompanyCouriers: function loadCompanyCouriers() {
            var _this13 = this;

            axios.get('/visa/home/showCouriers').then(function (response) {
                _this13.companyCouriers = response.data;
            });
        },
        resetSchedFrm: function resetSchedFrm() {
            $('form#frm_deliverysched input[name=date]').val();
            this.addSchedule = new Form({
                item: '',
                rider_id: '',
                location: '',
                scheduled_date: '',
                scheduled_time: ''
            }, { baseURL: '/visa/home/' });
        },
        getDate: function getDate() {
            //console.log($('form#frm_deliverysched .input[name=date]').val());
            this.dt = $('form#frm_deliverysched .input[name=date]').val();
        },
        saveDeliverSchedule: function saveDeliverSchedule() {
            var _this14 = this;

            var m = this.mode;
            var combineTime = this.$refs.delivery.alwaysTwoDigit(this.scheduled_hour) + ":" + this.$refs.delivery.alwaysTwoDigit(this.scheduled_minute) + ":00";
            this.addSchedule.scheduled_time = combineTime;
            this.addSchedule.scheduled_date = this.$refs.delivery.formatDateMDY(this.addSchedule.scheduled_date);

            if (m == "Add") {
                // if(this.dt == ''){
                //     this.dt = $('form#frm_deliverysched .input[name=date]').val();
                // }
                // this.addSchedule.scheduled_date = this.dt;
                this.addSchedule.submit('post', '/save-delivery-schedule').then(function (result) {
                    $('#addScheduleModal').modal('toggle');
                    toastr.success('Schedule Added.');
                    _this14.resetSchedFrm();
                }).catch(function (error) {
                    toastr.error('Failed. All Fields are required!');
                });
                this.resetSchedFrm();
                //sthis.callDataTables6();
            } else if (m == "Edit") {
                // this.addSchedule.scheduled_date = this.dt;
                this.addSchedule.submit('post', '/editSched').then(function (result) {
                    $('#addScheduleModal').modal('toggle');
                    toastr.success('Update Schedule.');
                    _this14.resetSchedFrm();
                }).catch(function (error) {
                    toastr.error('Update Failed. All Fields are required!');
                });
                this.resetSchedFrm();
                //this.callDataTables6();
            }
            this.$refs.delivery.dateFilter = this.addSchedule.scheduled_date;
            this.$refs.delivery.filterSched('form');
        },
        deleteSchedule: function deleteSchedule(id) {
            var _this15 = this;

            axios.post('/visa/home/delSched/' + id).then(function (result) {
                $('#deleteSchedPrompt').modal('toggle');
                toastr.success('Successfully Deleted');
                _this15.$refs.delivery.filterSched('today');
            }).catch(function (error) {
                $('#deleteSchedPrompt').modal('toggle');
                toastr.error('Delete Failed');
            });
        }
    },
    created: function created() {
        this.callDataTables1();
        this.callDataTables2();
        this.callDataTables3();
        this.callDataTables4();
        // this.callDataTables5();
        // this.callDataTables6();
        this.loadCompanyCouriers();
        $('form#date-range-form input[name=start]').datepicker({ format: 'mm/dd/yyyy' });
        $('form#date-range-form input[name=start]').datepicker('setDate', 'today');
    },

    components: {
        'edit-service-dashboard': __webpack_require__(346),
        'todays-tasks': __webpack_require__(371),
        'todays-services': __webpack_require__(370),
        'delivery-schedule': __webpack_require__(365)
    },
    mounted: function mounted() {
        $('body').popover({ // Ok
            html: true,
            trigger: 'hover',
            selector: '[data-toggle="popover"]'
        });

        $(document).on('focus', 'form#date-range-form .input-daterange', function () {
            $(this).datepicker({
                format: 'mm/dd/yyyy',
                todayHighlight: true
            });
        });

        $(document).on('focus', 'form#date-range-form2 .input-daterange', function () {
            $(this).datepicker({
                format: 'mm/dd/yyyy',
                todayHighlight: true
            });
        });

        $(document).on('focus', 'form#frm_deliverysched .input-daterange', function () {
            $(this).datepicker({
                format: 'mm/dd/yyyy',
                todayHighlight: true
            });
        });
        // $(document).on('focus', 'form#daily-form .input-group.date', function(){
        //     $(this).datepicker({
        //         format:'yyyy-mm-dd',     
        //         todayHighlight: true,
        //     });
        // });
    }
});

$(document).ready(function () {
    $('[data-toggle=popover]').popover();

    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });
});

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 253:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			dt: '',
			requiredDocs: [],
			optionalDocs: [],
			saveServiceForm: {
				id: '',
				tracking: '',
				tip: '',
				status: '',
				cost: '',
				discount: '',
				reason: '',
				note: '',
				active: '',
				reporting: '',
				extend: ''
			}
		};
	},


	methods: {
		initChosenSelect: function initChosenSelect() {

			$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({
				width: "100%",
				no_results_text: "No result/s found.",
				search_contains: true
			});
		},
		getDate: function getDate() {
			console.log($('form#daily-form input[name=date]').val());
			this.dt = $('form#daily-form input[name=date]').val();
			this.saveServiceForm.extend = this.dt;
		},
		setService: function setService(id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs) {
			this.saveServiceForm = {
				id: id, tracking: tracking, tip: tip, status: status, cost: cost, discount: discount, reason: reason, active: active, extend: extend, rcv_docs: rcv_docs };
		},
		clearSaveServiceForm: function clearSaveServiceForm() {
			this.saveServiceForm = {
				id: '',
				tracking: '',
				tip: '',
				status: '',
				cost: '',
				discount: '',
				reason: '',
				note: '',
				active: '',
				reporting: '',
				extend: '',
				rcv_docs: ''
			};
		},
		saveService: function saveService() {
			var _this = this;

			var requiredDocs = '';
			$('.chosen-select-for-required-docs option:selected').each(function (e) {
				requiredDocs += $(this).val() + ',';
			});

			var optionalDocs = '';
			$('.chosen-select-for-optional-docs option:selected').each(function (e) {
				optionalDocs += $(this).val() + ',';
			});

			var docs = requiredDocs + optionalDocs;
			this.saveServiceForm.rcv_docs = docs.slice(0, -1);

			if (this.saveServiceForm.discount > 0 && this.saveServiceForm.reason == '') {
				toastr.error('Please input discount reason.');
			} else if (requiredDocs == '' && this.requiredDocs.length != 0) {
				toastr.error('Please select Required Documents.');
			} else {

				axios.post('/visa/client/clientEditService', {
					id: this.saveServiceForm.id,
					tracking: this.saveServiceForm.tracking,
					tip: this.saveServiceForm.tip,
					status: this.saveServiceForm.status,
					cost: this.saveServiceForm.cost,
					discount: this.saveServiceForm.discount,
					reason: this.saveServiceForm.reason,
					note: this.saveServiceForm.note,
					active: this.saveServiceForm.active,
					reporting: this.saveServiceForm.reporting,
					extend: this.saveServiceForm.extend,
					rcv_docs: this.saveServiceForm.rcv_docs
				}).then(function (response) {
					if (response.data.success) {
						_this.clearSaveServiceForm();

						_this.$parent.$parent.callDataTables1();
						_this.$parent.$parent.callDataTables2();
						$(".chosen-select-for-required-docs, .chosen-select-for-optional-docs").val('').trigger('chosen:updated');

						$('#editServiceModal').modal('hide');

						toastr.success('Service updated successfully.');
					}
				}).catch(function (error) {
					for (var key in error) {
						if (error.hasOwnProperty(key)) toastr.error(error[key][0]);
					}
				});
			}
		},
		fetchDocs: function fetchDocs(rd, od) {
			var _this2 = this;

			$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');

			if (this.saveServiceForm.rcv_docs == '' || this.saveServiceForm.rcv_docs == null) var rcv = this.saveServiceForm.rcv_docs != null ? this.saveServiceForm.rcv_docs.split(",") : '';else var rcv = this.saveServiceForm.rcv_docs.split(",");

			axios.get('/visa/group/' + rd + '/service-docs').then(function (result) {
				_this2.requiredDocs = result.data;
			});

			axios.get('/visa/group/' + od + '/service-docs').then(function (result) {
				_this2.optionalDocs = result.data;
			});

			setTimeout(function () {
				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen('destroy');
				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({
					width: "100%",
					no_results_text: "No result/s found.",
					search_contains: true
				});

				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val(rcv).trigger('chosen:updated');
				$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');
			}, 2000);
		}
	},
	mounted: function mounted() {
		this.initChosenSelect();
		// DatePicker
		$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			autoclose: true,
			format: "yyyy-mm-dd"
		}).on('changeDate', function (e) {
			// Bootstrap Datepicker onChange
			var id = e.target.id;
			var date = e.target.value;

			console.log('ID : ' + id);
			console.log('DATE : ' + date);
			this.getDate();
		}.bind(this)).on('change', function (e) {
			// native onChange
			var id = e.target.id;
			var date = e.target.value;

			console.log('ID : ' + id);
			console.log('DATE : ' + date);
		}.bind(this));
	}
});

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			activeButton: 'today',

			dateFilter: '',

			deliverySchedules: []
		};
	},


	methods: {
		formatDatePickerValue: function formatDatePickerValue(value) {
			var datePickerValue = value.split('/');

			return datePickerValue[2] + '-' + datePickerValue[0] + '-' + datePickerValue[1];
		},
		showScheduleModal: function showScheduleModal(val, id) {
			var _this = this;

			if (val == "add") {
				$('#addScheduleModal').modal('toggle');
				this.$parent.mode = "Add";
			} else if (val == "edit") {
				axios.post('/visa/home/getSched/' + id).then(function (result) {
					_this.$parent.addSchedule = new Form({
						id: result.data.id,
						item: result.data.item,
						rider_id: result.data.rider_id,
						location: result.data.location,
						scheduled_date: _this.formatDateYMD(result.data.scheduled_date),
						scheduled_time: result.data.scheduled_time
					}, { baseURL: '/visa/home/' });
					_this.$parent.dt = result.data.scheduled_date;
					_this.$parent.scheduled_hour = _this.getHour(result.data.scheduled_time);
					_this.$parent.scheduled_minute = _this.getMinute(result.data.scheduled_time);
					//$('form#frm_deliverysched input[name=date]').val(result.data.scheduled_date);
					_this.$parent.mode = "Edit";
				});
				$('#addScheduleModal').modal('toggle');
			}
		},


		//YYYY-MM-DD
		formatDateYMD: function formatDateYMD(value) {
			var date = value.split('/');
			var newDate = date[2] + '-' + date[0] + '-' + date[1];

			return newDate;
		},


		//mm/dd/yyyy
		formatDateMDY: function formatDateMDY(value) {
			var date = value.split('-');
			var newDate = date[1] + '/' + date[2] + '/' + date[0];

			return newDate;
		},
		alwaysTwoDigit: function alwaysTwoDigit(value) {
			if (value.toString().length == 1) {
				value = "0" + value;
			}
			return value;
		},
		getHour: function getHour(value) {
			var time = value.split(':');
			return time[0];
		},
		getMinute: function getMinute(value) {
			var time = value.split(':');
			return time[1];
		},
		filterSched: function filterSched(date) {
			var _this2 = this;

			if (date == 'form') {
				var datePickerValue = $('form#delivery-sched-form input[name=delivery-sched-date]').val();

				if (datePickerValue == '') {
					datePickerValue = moment().format('MM/DD/YYYY');
				} else {
					datePickerValue = datePickerValue;
				}

				this.dateFilter = datePickerValue;
			} else {
				this.activeButton = date;

				if (date == 'today') this.dateFilter = moment().format('MM/DD/YYYY');else if (date == 'tomorrow') this.dateFilter = moment(new Date()).add(+1, 'days').format('MM/DD/YYYY');

				$('form#delivery-sched-form input[name=delivery-sched-date]').val(this.dateFilter);
			}

			axios.get('/visa/home/getSchedulesbyDate', {
				params: {
					dateFilter: this.dateFilter
				}
			}).then(function (response) {
				var _response$data = response.data,
				    success = _response$data.success,
				    schedules = _response$data.schedules;


				if (success) {
					$('#deliverySchedule').DataTable().destroy();

					_this2.deliverySchedules = schedules;

					setTimeout(function () {
						$('#deliverySchedule').DataTable({
							responsive: true,
							autoWidth: false,
							"columnDefs": [{ "width": "25%", "targets": 1 }],
							"lengthMenu": [[10, 25, 50], [10, 25, 50]],
							"iDisplayLength": 10
						});

						$(function () {
							$('[data-toggle="tooltip"]').tooltip();
						});
					}, 1000);
				}
			});
		}
	},

	created: function created() {
		this.filterSched('today');
	},
	mounted: function mounted() {
		$('#delivery-scheds-datepicker').datepicker({
			format: 'mm/dd/yyyy',
			todayHighlight: true
		});
	}
});

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			activeButton: 'today',

			dateFilter: '',

			filServices: []
		};
	},


	methods: {
		formatDatePickerValue: function formatDatePickerValue(value) {
			var datePickerValue = value.split('/');

			return datePickerValue[2] + '-' + datePickerValue[0] + '-' + datePickerValue[1];
		},
		formatDate: function formatDate(value) {
			var date = value.split('-');
			var newDate = date[1] + '/' + date[2] + '/' + date[0];

			return newDate;
		},
		filterServices: function filterServices(date) {
			var _this = this;

			if (date == 'form') {
				var datePickerValue = $('form#todays-services-form input[name=todays-service-date]').val();

				if (datePickerValue == '') {
					datePickerValue = moment().format('MM/DD/YYYY');
				} else {
					datePickerValue = datePickerValue;
				}

				this.dateFilter = datePickerValue;
			} else {
				this.activeButton = date;

				if (date == 'today') this.dateFilter = moment().format('MM/DD/YYYY');else if (date == 'yesterday') this.dateFilter = moment(new Date()).add(-1, 'days').format('MM/DD/YYYY');

				$('form#todays-services-form input[name=todays-service-date]').val(this.dateFilter);
			}

			axios.get('/visa/home/get-service-byDate', {
				params: {
					dateFilter: this.dateFilter
				}
			}).then(function (response) {
				var _response$data = response.data,
				    success = _response$data.success,
				    services = _response$data.services;


				if (success) {
					$('#todaysServicesTable').DataTable().destroy();

					_this.filServices = services;

					setTimeout(function () {
						$('#todaysServicesTable').DataTable({
							responsive: true,
							autoWidth: false,
							"columnDefs": [{ "width": "25%", "targets": 1 }],
							"lengthMenu": [[10, 25, 50], [10, 25, 50]],
							"iDisplayLength": 10
						});

						$(function () {
							$('[data-toggle="tooltip"]').tooltip();
						});
					}, 1000);
				}
			});
		}
	},

	created: function created() {
		this.filterServices('today');
	},
	mounted: function mounted() {
		$('#todays-services-datepicker').datepicker({
			format: 'mm/dd/yyyy',
			todayHighlight: true
		});
	}
});

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			activeButton: 'today',

			dateFilter: '',

			tasks: []
		};
	},


	methods: {
		formatDatePickerValue: function formatDatePickerValue(value) {
			var datePickerValue = value.split('/');

			return datePickerValue[2] + '-' + datePickerValue[0] + '-' + datePickerValue[1];
		},
		formatDate: function formatDate(value) {
			var date = value.split('-');
			var newDate = date[1] + '/' + date[2] + '/' + date[0];

			return newDate;
		},
		filterTasks: function filterTasks(date) {
			var _this = this;

			if (date == 'form') {
				var datePickerValue = $('form#todays-tasks-form input[name=todays-task-date]').val();

				if (datePickerValue == '') {
					datePickerValue = moment().format('YYYY-MM-DD');
				} else {
					datePickerValue = this.formatDatePickerValue(datePickerValue);
				}

				this.dateFilter = datePickerValue;
			} else {
				this.activeButton = date;

				if (date == 'tomorrow') this.dateFilter = moment(new Date()).add(1, 'day').format('YYYY-MM-DD');else if (date == 'today') this.dateFilter = moment().format('YYYY-MM-DD');else if (date == 'yesterday') this.dateFilter = moment(new Date()).add(-1, 'days').format('YYYY-MM-DD');

				$('form#todays-tasks-form input[name=todays-task-date]').val(this.formatDate(this.dateFilter));
			}

			axios.get('/visa/home/get-todays-tasks', {
				params: {
					dateFilter: this.dateFilter
				}
			}).then(function (response) {
				var _response$data = response.data,
				    success = _response$data.success,
				    tasks = _response$data.tasks;


				if (success) {
					$('#todaysTasksLists').DataTable().destroy();

					_this.tasks = tasks;

					setTimeout(function () {
						$('#todaysTasksLists').DataTable({
							responsive: true,
							"lengthMenu": [[10, 25, 50], [10, 25, 50]],
							"iDisplayLength": 10,

							"footerCallback": function footerCallback(row, data, start, end, display) {
								var api = this.api(),
								    data;

								var intVal = function intVal(i) {
									return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
								};

								var total = api.column(3).data().reduce(function (a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								var pageTotal = api.column(3, { page: 'current' }).data().reduce(function (a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(0).footer()).html('Total Estimated Cost: &#x20b1;' + total);
							}
						});

						$(function () {
							$('[data-toggle="tooltip"]').tooltip();
						});
					}, 1000);
				}
			});
		}
	},

	created: function created() {
		this.filterTasks('today');
	},
	mounted: function mounted() {
		$('#todays-task-datepicker').datepicker({
			format: 'mm/dd/yyyy',
			todayHighlight: true
		});
	}
});

/***/ }),

/***/ 299:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-3afdb553] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-3afdb553] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["EditServiceDashboard.vue?5109217a"],"names":[],"mappings":";AA+VA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"EditServiceDashboard.vue","sourcesContent":["<template>\n\n\t<form class=\"form-horizontal\" @submit.prevent=\"saveService\">\n\n\t\t<div>\n\t\t\t\n\t\t\t<div classs=\"col-md-12\">\n\t\t\t\t<div class=\"form-group col-md-6\">\n\t\t\t\t    <label class=\"col-md-4 control-label \">\n\t\t\t\t        Tip:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-8\">\n\t\t\t\t        <input type=\"number\" min=\"0\" class=\"form-control\" name=\"tip\" style=\"width:175px;\" v-model=\"saveServiceForm.tip\">\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group col-md-6 m-l-2\">\n\t\t\t\t    <label class=\"col-md-4 control-label \">\n\t\t\t\t        Status:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-8\">\n\t\t\t\t        <select class=\"form-control\" name=\"status\" style=\"width:175px;\" v-model=\"saveServiceForm.status\">\n\t                        <option value=\"complete\">Complete</option>\n\t                        <option value=\"on process\">On Process</option>\n\t                        <option value=\"pending\">Pending</option>\n\t                    </select>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div classs=\"col-md-12\">\n\t\t\t\t<div class=\"form-group col-md-6\">\n\t\t\t\t    <label class=\"col-md-4 control-label \">\n\t\t\t\t        Cost:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-8\">\n\t\t\t\t        <input type=\"number\" min=\"0\" class=\"form-control\" name=\"cost\" style=\"width:175px;\" v-model=\"saveServiceForm.cost\">\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group col-md-6 m-l-2\">\n\t\t\t\t    <label class=\"col-md-4 control-label \">\n\t\t\t\t        Discount:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-8\">\n\t\t\t\t        <input type=\"number\" class=\"form-control\" name=\"discount\" min=\"0\" style=\"width:175px;\" v-model=\"saveServiceForm.discount\">\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div classs=\"col-md-12\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t        Reason:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t        <input type=\"text\" class=\"form-control\" name=\"reason\" v-model=\"saveServiceForm.reason\">\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t\n\t\t\t<div classs=\"col-md-12\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t        Note:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t        <input type=\"text\" class=\"form-control\" name=\"note\" v-model=\"saveServiceForm.note\">\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div classs=\"col-md-12\">\n\t\t\t\t<div class=\"form-group col-md-6\">\n\t\t\t\t\t<label class=\"col-md-4 control-label\">\n\t\t\t\t        Status:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-8\">\n\t\t\t\t    \t<div style=\"height:5px;clear:both;\"></div>\n\t\t\t\t        <label class=\"\"> \n\t\t\t\t        \t<input type=\"radio\" value=\"0\" name=\"active\" v-model=\"saveServiceForm.active\"> <i></i> \n\t\t\t\t        \tDisabled \n\t\t\t\t        </label>\n\n\t                    <label class=\"m-l-2\"> \n\t                    \t<input type=\"radio\" value=\"1\" name=\"active\" v-model=\"saveServiceForm.active\"> <i></i> \n\t                    \tEnabled \n\t                    </label>\n\t                </div>\n\t\t\t    </div>\n\n\t\t\t    <div class=\"form-group col-md-6\">\n\t\t\t\t    <label class=\"col-md-4 control-label \">\n\t\t\t\t        Extend To:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-6\">\n\t\t\t\t    \t<form id=\"daily-form\" class=\"form-inline\">\n\t                        <div class=\"form-group\">\n\t                            <div class=\"input-group date\">\n\t                                <span class=\"input-group-addon\">\n\t                                    <i class=\"fa fa-calendar\"></i>\n\t                                </span>\n\t                                <input type=\"text\" id=\"date\" v-model=\"saveServiceForm.extend\" autocomplete=\"off\" class=\"form-control datepicker\" name=\"date\">\n\t                            </div>\n\t                        </div>\n\t                    </form>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t       \tRequired Documents: <span class=\"text-danger\">*</span>\n\t\t\t\t    </label>\n\n\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t            <select data-placeholder=\"Select Docs\" class=\"chosen-select-for-required-docs\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t\t                <option v-for=\"doc in requiredDocs\" :value=\"doc.id\">\n\t\t\t                \t{{ (doc.title).trim() }}\n\t\t\t                </option>\n\t\t\t            </select>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t       \tOptional Documents:\n\t\t\t\t    </label>\n\n\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t            <select data-placeholder=\"Select Docs\" class=\"chosen-select-for-optional-docs\" multiple style=\"width:350px;\" tabindex=\"4\">\n\t\t\t                <option v-for=\"doc in optionalDocs\" :value=\"doc.id\">\n\t\t\t                \t{{ (doc.title).trim() }}\n\t\t\t                </option>\n\t\t\t            </select>\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\n<!-- \t\t\t<div classs=\"col-md-12\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t\t        Reporting:\n\t\t\t\t    </label>\n\t\t\t\t    <div class=\"col-md-10\">\n\t\t\t\t        <input type=\"text\" class=\"form-control\" name=\"reporting\" v-model=\"$parent.$parent.editServ.reporting\">\n\t\t\t\t    </div>\n\t\t\t\t</div>\n\t\t\t</div> -->\n\t\t\t\n\n\t\t</div>\n\n\t    <div class=\"col-md-12\">\n\t\t\t<button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\t\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Save</button>\n\t    </div>\n\t</form>\n\n</template>\n\n<script>\n\t\n\texport default {\n\n\t\tdata() {\n\t\t\treturn {\n\t\t\t\tdt:'',\n\t\t\t\trequiredDocs: [],\n        \t\toptionalDocs: [],\n\t\t\t\tsaveServiceForm: {\n\t\t\t\t\tid: '',\n\t\t\t\t\ttracking: '',\n\t\t\t\t\ttip: '',\n\t\t\t\t\tstatus: '',\n\t\t\t\t\tcost: '',\n\t\t\t\t\tdiscount: '',\n\t\t\t\t\treason: '',\n\t\t\t\t\tnote: '', \n\t\t\t\t\tactive: '',\n\t\t\t\t\treporting: '',\n\t\t\t\t\textend:''\n\t\t\t\t},\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\n\t\t\tinitChosenSelect() {\n\n\t\t\t\t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({\n\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\tsearch_contains: true\n\t\t\t\t});\n\n\t\t\t},\n\n\t\t\t getDate(){\n\t\t    \tconsole.log($('form#daily-form input[name=date]').val());\n\t\t    \tthis.dt = $('form#daily-form input[name=date]').val();\n\t\t    \tthis.saveServiceForm.extend = this.dt;\n\t\t    },\n\n\t\t\tsetService(id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs) {\n\t\t\t\tthis.saveServiceForm = {\n\t\t\t\t\tid, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs}\n\t\t\t},\n\n\t\t\tclearSaveServiceForm() {\n\t\t\t\tthis.saveServiceForm = {\n\t\t\t\t\tid: '',\n\t\t\t\t\ttracking: '',\n\t\t\t\t\ttip: '',\n\t\t\t\t\tstatus: '',\n\t\t\t\t\tcost: '',\n\t\t\t\t\tdiscount: '',\n\t\t\t\t\treason: '',\n\t\t\t\t\tnote: '', \n\t\t\t\t\tactive: '',\n\t\t\t\t\treporting: '',\n\t\t\t\t\textend:'',\n\t\t\t\t\trcv_docs:''\t\t\t\t\t\n\t\t\t\t}\n\t\t\t},\n\n\t\t\tsaveService() {\n\t\t\t\tvar requiredDocs = '';\n        \t\t$('.chosen-select-for-required-docs option:selected').each( function(e) {\n\t\t\t        requiredDocs += $(this).val() + ',';\n\t\t\t    });\n\n        \t\tvar optionalDocs = '';\n        \t\t$('.chosen-select-for-optional-docs option:selected').each( function(e) {\n\t\t\t        optionalDocs += $(this).val() + ',';\n\t\t\t    });\n\n\t\t\t    var docs = requiredDocs + optionalDocs;\n\t\t\t\tthis.saveServiceForm.rcv_docs = docs.slice(0, -1);\n\n\t\t\t\tif(this.saveServiceForm.discount > 0 && this.saveServiceForm.reason == ''){\n\t\t\t        toastr.error('Please input discount reason.');\n\t\t\t    }else if(requiredDocs == '' && this.requiredDocs.length!=0){\n        \t\t\ttoastr.error('Please select Required Documents.');        \t\t\n\t\t\t    } else {\n\n        \t\t\taxios.post('/visa/client/clientEditService', {\n\t\t\t\t\t\tid: this.saveServiceForm.id,\n\t\t\t\t\t\ttracking: this.saveServiceForm.tracking,\n\t\t\t\t\t\ttip: this.saveServiceForm.tip,\n\t\t\t\t\t\tstatus: this.saveServiceForm.status,\n\t\t\t\t\t\tcost: this.saveServiceForm.cost,\n\t\t\t\t\t\tdiscount: this.saveServiceForm.discount,\n\t\t\t\t\t\treason: this.saveServiceForm.reason,\n\t\t\t\t\t\tnote: this.saveServiceForm.note,\n\t\t\t\t\t\tactive: this.saveServiceForm.active,\n\t\t\t\t\t\treporting: this.saveServiceForm.reporting,\n\t\t\t\t\t\textend: this.saveServiceForm.extend,\n\t\t\t\t\t\trcv_docs: this.saveServiceForm.rcv_docs\n\t\t\t\t\t})\n\t\t\t\t\t.then(response => {\n\t\t\t\t\t\tif(response.data.success) {\n\t\t\t\t\t\t\tthis.clearSaveServiceForm();\n\n\t\t                \tthis.$parent.$parent.callDataTables1();\n\t\t                \tthis.$parent.$parent.callDataTables2();\n\t\t                \t$(\".chosen-select-for-required-docs, .chosen-select-for-optional-docs\").val('').trigger('chosen:updated');\n\n\t\t                \t$('#editServiceModal').modal('hide');\n\n\t\t                \ttoastr.success('Service updated successfully.');\n\t\t                }\n\t\t\t\t\t})\n\t\t\t\t\t.catch(error =>{\n\t\t                for(var key in error) {\n\t\t\t            \tif(error.hasOwnProperty(key)) toastr.error(error[key][0])\n\t\t\t            }\n\t\t            });\n\n        \t\t}\n\t\t\t},\n\n\t\t\tfetchDocs(rd, od){\n\n\t\t\t\t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val('').trigger('chosen:updated');\n\n\t\t\t\tif(this.saveServiceForm.rcv_docs == '' || this.saveServiceForm.rcv_docs == null)\n\t\t\t\t\tvar rcv = ( this.saveServiceForm.rcv_docs!=null ? this.saveServiceForm.rcv_docs.split(\",\") : '');\n\t\t\t\telse\n\t\t\t\t\tvar rcv = this.saveServiceForm.rcv_docs.split(\",\");\n\n\t\t\t \taxios.get('/visa/group/'+rd+'/service-docs')\n\t\t\t \t  .then(result => {\n\t\t            this.requiredDocs = result.data;\n\t\t        });\n\t        \t\n\t\t\t \taxios.get('/visa/group/'+od+'/service-docs')\n\t\t\t \t  .then(result => {\n\t\t            this.optionalDocs = result.data;\n\t\t        });\n\n\t\t        setTimeout(() => {\n\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen('destroy');\n\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').chosen({\n\t\t\t\t\twidth: \"100%\",\n\t\t\t\t\tno_results_text: \"No result/s found.\",\n\t\t\t\t\tsearch_contains: true\n\t\t\t\t});\n\n\t        \t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').val(rcv).trigger('chosen:updated');\n        \t\t$('.chosen-select-for-required-docs, .chosen-select-for-optional-docs').trigger('chosen:updated');\n        \t}, 2000);\n\n\t\t\t},\n\n\t\t\n\n\n\t\t},\n\t\tmounted() {\n\t\t\tthis.initChosenSelect();\n        \t// DatePicker\n\t        \t$('.datepicker').datepicker({\n\t                todayBtn: \"linked\",\n\t                keyboardNavigation: false,\n\t                forceParse: false,\n\t                calendarWeeks: true,\n\t                autoclose: true,\n\t                format: \"yyyy-mm-dd\",\n\t            })\n\t            .on('changeDate', function(e) { // Bootstrap Datepicker onChange\n\t            \tlet id = e.target.id;\n\t\t\t        let date = e.target.value;\n\n\t\t\t        console.log('ID : ' + id);\n\t\t\t        console.log('DATE : ' + date);\n\t\t\t        this.getDate();\n\t\t\t    }.bind(this))\n\t\t\t    .on('change', function(e) { // native onChange\n\t\t\t    \tlet id = e.target.id;\n\t\t\t    \tlet date = e.target.value;\n\n\t\t\t    \tconsole.log('ID : ' + id);\n\t\t\t        console.log('DATE : ' + date);\n\t\t\t    }.bind(this));\n\n        }\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(7)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 304:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform button.btn-filter[data-v-6b581acb] {\n\tfloat: right;\n\tline-height: 1.2; \n\tmargin-top: 5px;\n}\n", "", {"version":3,"sources":["DeliverySchedule.vue?f43c9126"],"names":[],"mappings":";AAuMA;CACA,aAAA;CACA,iBAAA;CACA,gBAAA;CACA","file":"DeliverySchedule.vue","sourcesContent":["<template>\n\t\n\t<div>\n\t\t\n\t\t<form id=\"delivery-sched-form\" class=\"form-inline\" @submit.prevent=\"filterSched('form')\">\n\t\t    <div class=\"form-group\">\n\t\t        <div class=\"input-daterange input-group\" id=\"delivery-scheds-datepicker\">\n\t\t            <input type=\"text\" class=\"input-sm form-control\" name=\"delivery-sched-date\" autocomplete=\"off\" />\n\t\t        </div>\n\t\t    </div>\n\t\t    \n\t\t    <div class=\"form-group\">\n\t\t        <button type=\"submit\" class=\"btn btn-primary\" style=\"line-height: 1.2;\">\n\t\t            <strong>Filter</strong>\n\t\t        </button>\n\t\t    </div>\n\t\t\t<button id=\"add_schedule\" class=\"btn btn-warning\" style=\"line-height: 1.2; margin-left: 2px;\" type=\"button\" @click=\"showScheduleModal('add', '')\">Add Schedule </button>\n\n\t\t\t<button @click=\"filterSched('today')\" class=\"btn btn-primary btn-filter\" :class=\"{'btn-outline' : activeButton!='today'}\" style=\"margin-right: 2px;\" type=\"button\">Today</button>\n\n\t\t\t<button @click=\"filterSched('tomorrow')\" class=\"btn btn-primary btn-filter\" :class=\"{'btn-outline' : activeButton!='yesterday'}\" style=\"margin-right: 2px;\" type=\"button\">Tomorrow</button>\n\t\t</form>\n\n\t\t<div style=\"height:20px; clear:both;\"></div>\n\n    \t<table id=\"deliverySchedule\" class=\"table table-striped table-bordered table-hover dataTables-example\">\n\t        <thead>\n\t            <tr>\n\t                <th>Item</th>\n\t                <th>Schedule</th>\n\t                <th>Location</th>\n\t                <th>Rider</th>\n\t                <th>Action</th>         \n\t            </tr>\n\t        </thead>\n\t            <tbody>\n\t            \t<tr v-for=\"ds in deliverySchedules\" v-cloak>\n\t            \t\t<td>{{ ds.item }}</td>\n\t\t\t\t\t\t<td>{{ ds.scheduled_date }} {{ ds.scheduled_time}}</td>\n\t\t\t\t\t\t<td>{{ ds.location }}</td>\n\t\t\t\t\t\t<td><span>{{ ds.rider.full_name }}</span></td>\n\t\t\t\t\t\t<td><a @click=\"showScheduleModal('edit', ds.id)\">Edit</a> | <a @click=\"$parent.showDeleteSchedPrompt(ds.id)\">Delete</a></td>         \n\t            \t</tr>\n\t            \t    \n\t            </tbody>                  \n\t    </table>\n\n\t</div>\n\n</template>\n\n<script>\n\t\n\texport default {\n\t\tdata() {\n\t\t\treturn {\n\t\t\t\tactiveButton: 'today',\n\n\t\t\t\tdateFilter: '',\n\n\t\t\t\tdeliverySchedules: []\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\t\t\tformatDatePickerValue(value) {\n\t\t\t\tlet datePickerValue = value.split('/');\n\n\t\t\t\treturn datePickerValue[2] + '-' + datePickerValue[0] + '-' + datePickerValue[1];\n\t\t\t},\n\n\t\t\tshowScheduleModal(val, id){\n\t            if(val == \"add\"){\n\t                $('#addScheduleModal').modal('toggle');\n\t                this.$parent.mode = \"Add\";\n\t            }else if(val == \"edit\"){\n\t                axios.post('/visa/home/getSched/'+ id) \n\t                .then(result => {\n\t                    this.$parent.addSchedule = new Form ({\n\t                        id              :result.data.id,\n\t                        item            :result.data.item,\n\t                        rider_id        :result.data.rider_id,\n\t                        location        :result.data.location,\n\t                        scheduled_date  :this.formatDateYMD(result.data.scheduled_date),\n\t                        scheduled_time  :result.data.scheduled_time\n\t                    },{ baseURL: '/visa/home/'});\n\t                    this.$parent.dt = result.data.scheduled_date;\n\t                    this.$parent.scheduled_hour = this.getHour(result.data.scheduled_time);\n\t                    this.$parent.scheduled_minute = this.getMinute(result.data.scheduled_time);\n\t                    //$('form#frm_deliverysched input[name=date]').val(result.data.scheduled_date);\n\t                    this.$parent.mode = \"Edit\";\n\t                });\n\t                $('#addScheduleModal').modal('toggle');\n\t            }\n\t        },\n\n\t        //YYYY-MM-DD\n\t\t\tformatDateYMD(value) {\n\t\t\t\tlet date = value.split('/');\n\t\t\t\tlet newDate = date[2] + '-' + date[0] + '-' + date[1];\n\n\t\t\t\treturn newDate;\n\t\t\t},\n\n\t\t\t//mm/dd/yyyy\n\t\t\tformatDateMDY(value) {\n\t\t\t\tlet date = value.split('-');\n\t\t\t\tlet newDate = date[1] + '/' + date[2] + '/' + date[0];\n\n\t\t\t\treturn newDate;\n\t\t\t},\n\n\t\t\talwaysTwoDigit(value){\n\t\t\t\tif (value.toString().length == 1) {\n\t\t            value = \"0\" + value;\n\t\t        }\n\t\t        return value;\n\t\t\t},\n\n\t\t\tgetHour(value){\n\t\t\t\tlet time = value.split(':');\n\t\t        return time[0];\n\t\t\t},\n\n\t\t\tgetMinute(value){\n\t\t\t\tlet time = value.split(':');\n\t\t        return time[1];\n\t\t\t},\n\n\t\t\tfilterSched(date) {\n\t\t\t\tif(date == 'form') {\n\t\t\t\t\tlet datePickerValue = $('form#delivery-sched-form input[name=delivery-sched-date]').val();\n\n\t\t\t\t\tif(datePickerValue == '') {\n\t\t\t\t\t\tdatePickerValue = moment().format('MM/DD/YYYY');\n\t\t\t\t\t} else {\n\t\t\t\t\t\tdatePickerValue = datePickerValue;\n\t\t\t\t\t}\n\n\t\t\t\t\tthis.dateFilter = datePickerValue;\n\t\t\t\t} else {\n\t\t\t\t\tthis.activeButton = date;\n\n\t\t\t\t\tif(date == 'today')\n\t\t\t\t\t\tthis.dateFilter = moment().format('MM/DD/YYYY');\n\t\t\t\t\telse if(date == 'tomorrow')\n\t\t\t\t\t\tthis.dateFilter = moment(new Date()).add(+1,'days').format('MM/DD/YYYY');\n\n\t\t\t\t\t$('form#delivery-sched-form input[name=delivery-sched-date]').val(this.dateFilter);\n\t\t\t\t}\n\n\t\t\t\taxios.get('/visa/home/getSchedulesbyDate', {\n\t                params: {\n\t                \tdateFilter: this.dateFilter\n\t                }\n\t            })\n\t            .then(response => {\n\t            \tlet { success, schedules } = response.data;\n\n\t            \tif(success) {\n\t            \t\t$('#deliverySchedule').DataTable().destroy();\n\n\t            \t\tthis.deliverySchedules = schedules;\n\n\t            \t\tsetTimeout(() => {\n\t            \t\t\t$('#deliverySchedule').DataTable({\n\t\t                        responsive: true,\n\t\t                        autoWidth: false,\n\t\t                        \"columnDefs\": [\n\t\t\t                            { \"width\": \"25%\", \"targets\": 1 }\n\t\t\t                        ],\n\t\t                        \"lengthMenu\": [[10, 25, 50], [10, 25, 50]],\n\t\t                        \"iDisplayLength\": 10\n\t\t                    });\n\n\t\t                    $(function () {\n\t\t                      \t$('[data-toggle=\"tooltip\"]').tooltip()\n\t\t                    });\n\t            \t\t}, 1000);\n\t            \t}\n\t            });\n\t\t\t}\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.filterSched('today');\n\t\t},\n\n\t\tmounted() {\n\t\t\t$('#delivery-scheds-datepicker').datepicker({\n                format:'mm/dd/yyyy',\n                todayHighlight: true\n            });\n\t\t}\n\t}\n\n</script>\n\n<style scoped>\n\tform button.btn-filter {\n\t\tfloat: right;\n\t\tline-height: 1.2; \n\t\tmargin-top: 5px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 308:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform button.btn-filter[data-v-7c95cdec] {\n\tfloat: right;\n\tline-height: 1.2; \n\tmargin-top: 5px;\n}\n", "", {"version":3,"sources":["TodaysTasks.vue?58e85cd1"],"names":[],"mappings":";AA+LA;CACA,aAAA;CACA,iBAAA;CACA,gBAAA;CACA","file":"TodaysTasks.vue","sourcesContent":["<template>\n\t\n\t<div>\n\t\t\n\t\t<form id=\"todays-tasks-form\" class=\"form-inline\" @submit.prevent=\"filterTasks('form')\">\n\t\t    <div class=\"form-group\">\n\t\t        <div class=\"input-daterange input-group\" id=\"todays-task-datepicker\">\n\t\t            <input type=\"text\" class=\"input-sm form-control\" name=\"todays-task-date\" autocomplete=\"off\" />\n\t\t        </div>\n\t\t    </div>\n\t\t    \n\t\t    <div class=\"form-group\">\n\t\t        <button type=\"submit\" class=\"btn btn-primary\" style=\"line-height: 1.2;\">\n\t\t            <strong>Filter</strong>\n\t\t        </button>\n\t\t    </div>\n\n\t\t    <button @click=\"filterTasks('tomorrow')\" class=\"btn btn-primary btn-filter\" :class=\"{'btn-outline' : activeButton!='tomorrow'}\" type=\"button\">Tomorrow</button>\n\n\t\t\t<button @click=\"filterTasks('today')\" class=\"btn btn-primary btn-filter\" :class=\"{'btn-outline' : activeButton!='today'}\" style=\"margin-right: 2px;\" type=\"button\">Today</button>\n\n\t\t\t<button @click=\"filterTasks('yesterday')\" class=\"btn btn-primary btn-filter\" :class=\"{'btn-outline' : activeButton!='yesterday'}\" style=\"margin-right: 2px;\" type=\"button\">Yesterday</button>\n\t\t</form>\n\n\t\t<div style=\"height:20px; clear:both;\"></div>\n\n    \t<table id=\"todaysTasksLists\" class=\"table table-striped table-bordered table-hover dataTables-example\">\n\t\t    <thead>\n\t\t        <tr>\n\t\t            <th>Name</th>\n\t\t            <th>Client No.</th>\n\t\t            <th>Service</th>\n\t\t            <th>Estimated Cost</th>\n\t\t            <th>Status</th>\n\t\t            <th>Filing Date</th>\n\t\t            <th>Releasing Date</th>\n\t\t        </tr>\n\t\t    </thead>\n\t\t    <tbody>\n\t\t        <tr v-for=\"task in tasks\">\n\t\t       \t\t<td>\n\t\t       \t\t\t<a :href=\"'/visa/client/' + task.client_service.user.id\" target=\"_blank\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"View client profile\">\n\t\t       \t\t\t\t{{ task.client_service.user.full_name }}\n\t\t       \t\t\t</a>\n\t\t       \t\t</td>\n\t\t       \t\t<td>{{ task.client_service.user.id }}</td>\n\t\t       \t\t<td>{{ task.client_service.service_category.detail }}</td>\n\t\t       \t\t<td>\n\t\t       \t\t\t{{ task.client_service.service_category.estimated_cost }}\n\t\t       \t\t</td>\n\t\t       \t\t<td>{{ task.status }}</td>\n\t\t       \t\t<td>{{ task.filing_date }}</td>\n\t\t       \t\t<td>{{ task.releasing_date }}</td>\n\t\t        </tr>\n\t\t    </tbody>\n\t\t    <tfoot>\n\t            <tr>\n\t                <th colspan=\"7\" style=\"text-align:right\"></th>\n\t            </tr>\n\t        </tfoot>\n\t\t</table>\n\n\t</div>\n\n</template>\n\n<script>\n\t\n\texport default {\n\t\tdata() {\n\t\t\treturn {\n\t\t\t\tactiveButton: 'today',\n\n\t\t\t\tdateFilter: '',\n\n\t\t\t\ttasks: []\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\t\t\tformatDatePickerValue(value) {\n\t\t\t\tlet datePickerValue = value.split('/');\n\n\t\t\t\treturn datePickerValue[2] + '-' + datePickerValue[0] + '-' + datePickerValue[1];\n\t\t\t},\n\n\t\t\tformatDate(value) {\n\t\t\t\tlet date = value.split('-');\n\t\t\t\tlet newDate = date[1] + '/' + date[2] + '/' + date[0];\n\n\t\t\t\treturn newDate;\n\t\t\t},\n\n\t\t\tfilterTasks(date) {\n\t\t\t\tif(date == 'form') {\n\t\t\t\t\tlet datePickerValue = $('form#todays-tasks-form input[name=todays-task-date]').val();\n\n\t\t\t\t\tif(datePickerValue == '') {\n\t\t\t\t\t\tdatePickerValue = moment().format('YYYY-MM-DD');\n\t\t\t\t\t} else {\n\t\t\t\t\t\tdatePickerValue = this.formatDatePickerValue(datePickerValue);\n\t\t\t\t\t}\n\n\t\t\t\t\tthis.dateFilter = datePickerValue;\n\t\t\t\t} else {\n\t\t\t\t\tthis.activeButton = date;\n\n\t\t\t\t\tif(date == 'tomorrow')\n\t\t\t\t\t\tthis.dateFilter = moment(new Date()).add(1,'day').format('YYYY-MM-DD');\n\t\t\t\t\telse if(date == 'today')\n\t\t\t\t\t\tthis.dateFilter = moment().format('YYYY-MM-DD');\n\t\t\t\t\telse if(date == 'yesterday')\n\t\t\t\t\t\tthis.dateFilter = moment(new Date()).add(-1,'days').format('YYYY-MM-DD');\n\n\t\t\t\t\t$('form#todays-tasks-form input[name=todays-task-date]').val(this.formatDate(this.dateFilter));\n\t\t\t\t}\n\n\t\t\t\taxios.get('/visa/home/get-todays-tasks', {\n\t                params: {\n\t                \tdateFilter: this.dateFilter\n\t                }\n\t            })\n\t            .then(response => {\n\t            \tlet { success, tasks } = response.data;\n\n\t            \tif(success) {\n\t            \t\t$('#todaysTasksLists').DataTable().destroy();\n\n\t            \t\tthis.tasks = tasks;\n\n\t            \t\tsetTimeout(() => {\n\t            \t\t\t$('#todaysTasksLists').DataTable({\n\t\t                        responsive: true,\n\t\t                        \"lengthMenu\": [[10, 25, 50], [10, 25, 50]],\n\t\t                        \"iDisplayLength\": 10,\n\n\t\t                        \"footerCallback\": function ( row, data, start, end, display ) {\n\t\t\t\t\t\t            var api = this.api(), data;\n\t\t\t\t\t\t \n\t\t\t\t\t\t            var intVal = function ( i ) {\n\t\t\t\t\t\t                return typeof i === 'string' ?\n\t\t\t\t\t\t                    i.replace(/[\\$,]/g, '')*1 :\n\t\t\t\t\t\t                    typeof i === 'number' ?\n\t\t\t\t\t\t                        i : 0;\n\t\t\t\t\t\t            };\n\t\t\t\t\t\t \n\t\t\t\t\t\t            var total = api\n\t\t\t\t\t\t                .column( 3 )\n\t\t\t\t\t\t                .data()\n\t\t\t\t\t\t                .reduce( function (a, b) {\n\t\t\t\t\t\t                    return intVal(a) + intVal(b);\n\t\t\t\t\t\t                }, 0 );\n\t\t\t\t\t\t \n\t\t\t\t\t\t            var pageTotal = api\n\t\t\t\t\t\t                .column( 3, { page: 'current'} )\n\t\t\t\t\t\t                .data()\n\t\t\t\t\t\t                .reduce( function (a, b) {\n\t\t\t\t\t\t                    return intVal(a) + intVal(b);\n\t\t\t\t\t\t                }, 0 );\n\t\t\t\t\t\t \n\t\t\t\t\t\t            // Update footer\n\t\t\t\t\t\t            $( api.column( 0 ).footer() ).html(\n\t\t\t\t\t\t            \t'Total Estimated Cost: &#x20b1;' + total\n\t\t\t\t\t\t            );\n\t\t\t\t\t\t        }\n\t\t                    });\n\n\t\t                    $(function () {\n\t\t                      \t$('[data-toggle=\"tooltip\"]').tooltip()\n\t\t                    });\n\t            \t\t}, 1000);\n\t            \t}\n\t            });\n\t\t\t}\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.filterTasks('today');\n\t\t},\n\n\t\tmounted() {\n\t\t\t$('#todays-task-datepicker').datepicker({\n                format:'mm/dd/yyyy',\n                todayHighlight: true\n            });\n\t\t}\n\t}\n\n</script>\n\n<style scoped>\n\tform button.btn-filter {\n\t\tfloat: right;\n\t\tline-height: 1.2; \n\t\tmargin-top: 5px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 313:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform button.btn-filter[data-v-c9d0e120] {\n\tfloat: right;\n\tline-height: 1.2; \n\tmargin-top: 5px;\n}\n", "", {"version":3,"sources":["TodaysServices.vue?27958897"],"names":[],"mappings":";AA8KA;CACA,aAAA;CACA,iBAAA;CACA,gBAAA;CACA","file":"TodaysServices.vue","sourcesContent":["<template>\n\t\n\t<div>\n\t\t\n\t\t<form id=\"todays-services-form\" class=\"form-inline\" @submit.prevent=\"filterServices('form')\">\n\t\t    <div class=\"form-group\">\n\t\t        <div class=\"input-daterange input-group\" id=\"todays-services-datepicker\">\n\t\t            <input type=\"text\" class=\"input-sm form-control\" name=\"todays-service-date\" autocomplete=\"off\" />\n\t\t        </div>\n\t\t    </div>\n\t\t    \n\t\t    <div class=\"form-group\">\n\t\t        <button type=\"submit\" class=\"btn btn-primary\" style=\"line-height: 1.2;\">\n\t\t            <strong>Filter</strong>\n\t\t        </button>\n\t\t    </div>\n\n\t\t\t<button @click=\"filterServices('today')\" class=\"btn btn-primary btn-filter\" :class=\"{'btn-outline' : activeButton!='today'}\" style=\"margin-right: 2px;\" type=\"button\">Today</button>\n\n\t\t\t<button @click=\"filterServices('yesterday')\" class=\"btn btn-primary btn-filter\" :class=\"{'btn-outline' : activeButton!='yesterday'}\" style=\"margin-right: 2px;\" type=\"button\">Yesterday</button>\n\t\t</form>\n\n\t\t<div style=\"height:20px; clear:both;\"></div>\n\n    \t<table id=\"todaysServicesTable\" class=\"table table-striped table-bordered table-hover dataTables-example\">\n\t\t    <thead>\n\t\t        <tr>\n                <th>Date</th>\n\t\t\t\t<th>Name</th>\n                <th>ID</th>\n                <th>Package</th>\n                <th style=\"width:30% !important\">Detail</th>\n                <th>Cost</th>\n                <th>Charge</th>\n\t\t\t\t<th>Tip</th>\n\t\t\t\t<th style=\"width:5%\">Action</th>\n\t\t        </tr>\n\t\t    </thead>\n\t\t    <tbody>\n\t\t        <tr v-for=\"service in filServices\">\n\t\t       \t\t<td>{{ service.service_date }}</td>\n\t\t       \t\t<td><span v-if=\"service.user!=null\">{{ service.user.full_name }}</span>\n\t\t\t\t\t\t<a class=\"pull-right\" href=\"#\" data-toggle=\"popover\" data-placement=\"top\" data-container=\"body\" :data-content=\"service.user.group_binded\" data-trigger=\"hover\" v-if=\"service.group_binded != ''\">\n\t\t\t\t\t\t\t<b class=\"fa fa-user-circle group-icon m-r-2\"></b>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a class=\"pull-right\" href=\"#\" data-toggle=\"popover\" data-placement=\"top\" data-container=\"body\" :data-content=\"'Unread Notifications : '+service.user.unread_notif\" data-trigger=\"hover\"  v-if=\"service.user.unread_notif > 0\">\n\t\t\t\t\t\t\t<b class=\"fa fa-envelope unread-icon m-r-1\"></b>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a class=\"pull-right\" href=\"#\" data-toggle=\"popover\" data-placement=\"top\" data-container=\"body\" data-content=\"App Installed by Client\" data-trigger=\"hover\"  v-if=\"service.user.binded\">\n\t\t\t\t\t\t\t<b class=\"fa fa-mobile app-icon m-r-1\"></b>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td><span v-if=\"service.user!=null\">{{ service.user.id }}</span></td>\n\t\t\t\t\t<td>{{ service.tracking }}</td>\n\t\t\t\t\t<td style=\"width:30% !important\">\n\t\t\t\t\t\t<a href=\"#\" v-if=\"service.remarks!=''\" data-toggle=\"popover\" data-placement=\"right\" data-container=\"body\" :data-content=\"service.remarks\" data-trigger=\"hover\" >\n\t\t\t              {{ service.detail }}\n\t\t\t            </a>\n\t\t\t            <span v-else>{{ service.detail }}</span>\n\t\t\t        </td>\n\t\t\t\t\t<td>{{ service.cost }}</td>\n\t\t\t\t\t<td>{{ service.charge }}</td>\n\t\t\t\t\t<td>{{ service.tip }}</td>\n\t\t\t\t\t<td class=\"text-center\">\n\t\t\t\t\t\t<a href=\"#\" data-toggle=\"modal\" data-target=\"#editServiceModal\" @click=\"$parent.setService(service)\" v-cloak>\n\t\t\t\t\t\t\t<i class=\"fa fa-pencil-square-o\"></i>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a :href=\"'/visa/client/'+service.client_id\" target=\"_blank\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"View client profile\"><i class=\"fa fa-arrow-right\"></i></a>\n\t\t\t\t\t</td>\t\n\t\t        </tr>\n\t\t    </tbody>\n\t\t</table>\n\n\t</div>\n\n</template>\n\n<script>\n\t\n\texport default {\n\t\tdata() {\n\t\t\treturn {\n\t\t\t\tactiveButton: 'today',\n\n\t\t\t\tdateFilter: '',\n\n\t\t\t\tfilServices: []\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\t\t\tformatDatePickerValue(value) {\n\t\t\t\tlet datePickerValue = value.split('/');\n\n\t\t\t\treturn datePickerValue[2] + '-' + datePickerValue[0] + '-' + datePickerValue[1];\n\t\t\t},\n\n\t\t\tformatDate(value) {\n\t\t\t\tlet date = value.split('-');\n\t\t\t\tlet newDate = date[1] + '/' + date[2] + '/' + date[0];\n\n\t\t\t\treturn newDate;\n\t\t\t},\n\n\t\t\tfilterServices(date) {\n\t\t\t\tif(date == 'form') {\n\t\t\t\t\tlet datePickerValue = $('form#todays-services-form input[name=todays-service-date]').val();\n\n\t\t\t\t\tif(datePickerValue == '') {\n\t\t\t\t\t\tdatePickerValue = moment().format('MM/DD/YYYY');\n\t\t\t\t\t} else {\n\t\t\t\t\t\tdatePickerValue = datePickerValue;\n\t\t\t\t\t}\n\n\t\t\t\t\tthis.dateFilter = datePickerValue;\n\t\t\t\t} else {\n\t\t\t\t\tthis.activeButton = date;\n\n\t\t\t\t\tif(date == 'today')\n\t\t\t\t\t\tthis.dateFilter = moment().format('MM/DD/YYYY');\n\t\t\t\t\telse if(date == 'yesterday')\n\t\t\t\t\t\tthis.dateFilter = moment(new Date()).add(-1,'days').format('MM/DD/YYYY');\n\n\t\t\t\t\t$('form#todays-services-form input[name=todays-service-date]').val(this.dateFilter);\n\t\t\t\t}\n\n\t\t\t\taxios.get('/visa/home/get-service-byDate', {\n\t                params: {\n\t                \tdateFilter: this.dateFilter\n\t                }\n\t            })\n\t            .then(response => {\n\t            \tlet { success, services } = response.data;\n\n\t            \tif(success) {\n\t            \t\t$('#todaysServicesTable').DataTable().destroy();\n\n\t            \t\tthis.filServices = services;\n\n\t            \t\tsetTimeout(() => {\n\t            \t\t\t$('#todaysServicesTable').DataTable({\n\t\t                        responsive: true,\n\t\t                        autoWidth: false,\n\t\t                        \"columnDefs\": [\n\t\t\t                            { \"width\": \"25%\", \"targets\": 1 }\n\t\t\t                        ],\n\t\t                        \"lengthMenu\": [[10, 25, 50], [10, 25, 50]],\n\t\t                        \"iDisplayLength\": 10\n\t\t                    });\n\n\t\t                    $(function () {\n\t\t                      \t$('[data-toggle=\"tooltip\"]').tooltip()\n\t\t                    });\n\t            \t\t}, 1000);\n\t            \t}\n\t            });\n\t\t\t}\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.filterServices('today');\n\t\t},\n\n\t\tmounted() {\n\t\t\t$('#todays-services-datepicker').datepicker({\n                format:'mm/dd/yyyy',\n                todayHighlight: true\n            });\n\t\t}\n\t}\n\n</script>\n\n<style scoped>\n\tform button.btn-filter {\n\t\tfloat: right;\n\t\tline-height: 1.2; \n\t\tmargin-top: 5px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(437)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(253),
  /* template */
  __webpack_require__(386),
  /* scopeId */
  "data-v-3afdb553",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/EditServiceDashboard.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] EditServiceDashboard.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3afdb553", Component.options)
  } else {
    hotAPI.reload("data-v-3afdb553", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 365:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(442)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(272),
  /* template */
  __webpack_require__(404),
  /* scopeId */
  "data-v-6b581acb",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DeliverySchedule.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DeliverySchedule.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6b581acb", Component.options)
  } else {
    hotAPI.reload("data-v-6b581acb", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 370:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(451)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(277),
  /* template */
  __webpack_require__(420),
  /* scopeId */
  "data-v-c9d0e120",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/TodaysServices.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] TodaysServices.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c9d0e120", Component.options)
  } else {
    hotAPI.reload("data-v-c9d0e120", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(446)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(278),
  /* template */
  __webpack_require__(410),
  /* scopeId */
  "data-v-7c95cdec",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/TodaysTasks.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] TodaysTasks.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7c95cdec", Component.options)
  } else {
    hotAPI.reload("data-v-7c95cdec", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 386:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.saveService($event)
      }
    }
  }, [_c('div', [_c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t        Tip:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.tip),
      expression: "saveServiceForm.tip"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "type": "number",
      "min": "0",
      "name": "tip"
    },
    domProps: {
      "value": (_vm.saveServiceForm.tip)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.saveServiceForm, "tip", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-6 m-l-2"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t        Status:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.status),
      expression: "saveServiceForm.status"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "name": "status"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.saveServiceForm, "status", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "complete"
    }
  }, [_vm._v("Complete")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "on process"
    }
  }, [_vm._v("On Process")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "pending"
    }
  }, [_vm._v("Pending")])])])])]), _vm._v(" "), _c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t        Cost:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.cost),
      expression: "saveServiceForm.cost"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "type": "number",
      "min": "0",
      "name": "cost"
    },
    domProps: {
      "value": (_vm.saveServiceForm.cost)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.saveServiceForm, "cost", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-6 m-l-2"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t        Discount:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.discount),
      expression: "saveServiceForm.discount"
    }],
    staticClass: "form-control",
    staticStyle: {
      "width": "175px"
    },
    attrs: {
      "type": "number",
      "name": "discount",
      "min": "0"
    },
    domProps: {
      "value": (_vm.saveServiceForm.discount)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.saveServiceForm, "discount", $event.target.value)
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t\t        Reason:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.reason),
      expression: "saveServiceForm.reason"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "reason"
    },
    domProps: {
      "value": (_vm.saveServiceForm.reason)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.saveServiceForm, "reason", $event.target.value)
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t\t        Note:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.note),
      expression: "saveServiceForm.note"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "note"
    },
    domProps: {
      "value": (_vm.saveServiceForm.note)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.saveServiceForm, "note", $event.target.value)
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    attrs: {
      "classs": "col-md-12"
    }
  }, [_c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('label', {
    staticClass: "col-md-4 control-label"
  }, [_vm._v("\n\t\t\t\t        Status:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticStyle: {
      "height": "5px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('label', {}, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.active),
      expression: "saveServiceForm.active"
    }],
    attrs: {
      "type": "radio",
      "value": "0",
      "name": "active"
    },
    domProps: {
      "checked": _vm._q(_vm.saveServiceForm.active, "0")
    },
    on: {
      "change": function($event) {
        _vm.$set(_vm.saveServiceForm, "active", "0")
      }
    }
  }), _vm._v(" "), _c('i'), _vm._v(" \n\t\t\t\t        \tDisabled \n\t\t\t\t        ")]), _vm._v(" "), _c('label', {
    staticClass: "m-l-2"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.active),
      expression: "saveServiceForm.active"
    }],
    attrs: {
      "type": "radio",
      "value": "1",
      "name": "active"
    },
    domProps: {
      "checked": _vm._q(_vm.saveServiceForm.active, "1")
    },
    on: {
      "change": function($event) {
        _vm.$set(_vm.saveServiceForm, "active", "1")
      }
    }
  }), _vm._v(" "), _c('i'), _vm._v(" \n\t                    \tEnabled \n\t                    ")])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-6"
  }, [_c('label', {
    staticClass: "col-md-4 control-label "
  }, [_vm._v("\n\t\t\t\t        Extend To:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('form', {
    staticClass: "form-inline",
    attrs: {
      "id": "daily-form"
    }
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(0), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.saveServiceForm.extend),
      expression: "saveServiceForm.extend"
    }],
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date",
      "autocomplete": "off",
      "name": "date"
    },
    domProps: {
      "value": (_vm.saveServiceForm.extend)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.saveServiceForm, "extend", $event.target.value)
      }
    }
  })])])])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-required-docs",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "data-placeholder": "Select Docs",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.requiredDocs), function(doc) {
    return _c('option', {
      domProps: {
        "value": doc.id
      }
    }, [_vm._v("\n\t\t\t                \t" + _vm._s((doc.title).trim()) + "\n\t\t\t                ")])
  }), 0)])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t\t       \tOptional Documents:\n\t\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-optional-docs",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "data-placeholder": "Select Docs",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.optionalDocs), function(doc) {
    return _c('option', {
      domProps: {
        "value": doc.id
      }
    }, [_vm._v("\n\t\t\t                \t" + _vm._s((doc.title).trim()) + "\n\t\t\t                ")])
  }), 0)])])])]), _vm._v(" "), _vm._m(2)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t\t       \tRequired Documents: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-12"
  }, [_c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Save")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3afdb553", module.exports)
  }
}

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 404:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('form', {
    staticClass: "form-inline",
    attrs: {
      "id": "delivery-sched-form"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        _vm.filterSched('form')
      }
    }
  }, [_vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('button', {
    staticClass: "btn btn-warning",
    staticStyle: {
      "line-height": "1.2",
      "margin-left": "2px"
    },
    attrs: {
      "id": "add_schedule",
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.showScheduleModal('add', '')
      }
    }
  }, [_vm._v("Add Schedule ")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-filter",
    class: {
      'btn-outline': _vm.activeButton != 'today'
    },
    staticStyle: {
      "margin-right": "2px"
    },
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.filterSched('today')
      }
    }
  }, [_vm._v("Today")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-filter",
    class: {
      'btn-outline': _vm.activeButton != 'yesterday'
    },
    staticStyle: {
      "margin-right": "2px"
    },
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.filterSched('tomorrow')
      }
    }
  }, [_vm._v("Tomorrow")])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('table', {
    staticClass: "table table-striped table-bordered table-hover dataTables-example",
    attrs: {
      "id": "deliverySchedule"
    }
  }, [_vm._m(2), _vm._v(" "), _c('tbody', _vm._l((_vm.deliverySchedules), function(ds) {
    return _c('tr', {}, [_c('td', [_vm._v(_vm._s(ds.item))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ds.scheduled_date) + " " + _vm._s(ds.scheduled_time))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(ds.location))]), _vm._v(" "), _c('td', [_c('span', [_vm._v(_vm._s(ds.rider.full_name))])]), _vm._v(" "), _c('td', [_c('a', {
      on: {
        "click": function($event) {
          _vm.showScheduleModal('edit', ds.id)
        }
      }
    }, [_vm._v("Edit")]), _vm._v(" | "), _c('a', {
      on: {
        "click": function($event) {
          _vm.$parent.showDeleteSchedPrompt(ds.id)
        }
      }
    }, [_vm._v("Delete")])])])
  }), 0)])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "input-daterange input-group",
    attrs: {
      "id": "delivery-scheds-datepicker"
    }
  }, [_c('input', {
    staticClass: "input-sm form-control",
    attrs: {
      "type": "text",
      "name": "delivery-sched-date",
      "autocomplete": "off"
    }
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group"
  }, [_c('button', {
    staticClass: "btn btn-primary",
    staticStyle: {
      "line-height": "1.2"
    },
    attrs: {
      "type": "submit"
    }
  }, [_c('strong', [_vm._v("Filter")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Item")]), _vm._v(" "), _c('th', [_vm._v("Schedule")]), _vm._v(" "), _c('th', [_vm._v("Location")]), _vm._v(" "), _c('th', [_vm._v("Rider")]), _vm._v(" "), _c('th', [_vm._v("Action")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6b581acb", module.exports)
  }
}

/***/ }),

/***/ 410:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('form', {
    staticClass: "form-inline",
    attrs: {
      "id": "todays-tasks-form"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        _vm.filterTasks('form')
      }
    }
  }, [_vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-filter",
    class: {
      'btn-outline': _vm.activeButton != 'tomorrow'
    },
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.filterTasks('tomorrow')
      }
    }
  }, [_vm._v("Tomorrow")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-filter",
    class: {
      'btn-outline': _vm.activeButton != 'today'
    },
    staticStyle: {
      "margin-right": "2px"
    },
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.filterTasks('today')
      }
    }
  }, [_vm._v("Today")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-filter",
    class: {
      'btn-outline': _vm.activeButton != 'yesterday'
    },
    staticStyle: {
      "margin-right": "2px"
    },
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.filterTasks('yesterday')
      }
    }
  }, [_vm._v("Yesterday")])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('table', {
    staticClass: "table table-striped table-bordered table-hover dataTables-example",
    attrs: {
      "id": "todaysTasksLists"
    }
  }, [_vm._m(2), _vm._v(" "), _c('tbody', _vm._l((_vm.tasks), function(task) {
    return _c('tr', [_c('td', [_c('a', {
      attrs: {
        "href": '/visa/client/' + task.client_service.user.id,
        "target": "_blank",
        "data-toggle": "tooltip",
        "data-placement": "right",
        "title": "View client profile"
      }
    }, [_vm._v("\n\t\t       \t\t\t\t" + _vm._s(task.client_service.user.full_name) + "\n\t\t       \t\t\t")])]), _vm._v(" "), _c('td', [_vm._v(_vm._s(task.client_service.user.id))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(task.client_service.service_category.detail))]), _vm._v(" "), _c('td', [_vm._v("\n\t\t       \t\t\t" + _vm._s(task.client_service.service_category.estimated_cost) + "\n\t\t       \t\t")]), _vm._v(" "), _c('td', [_vm._v(_vm._s(task.status))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(task.filing_date))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(task.releasing_date))])])
  }), 0), _vm._v(" "), _vm._m(3)])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "input-daterange input-group",
    attrs: {
      "id": "todays-task-datepicker"
    }
  }, [_c('input', {
    staticClass: "input-sm form-control",
    attrs: {
      "type": "text",
      "name": "todays-task-date",
      "autocomplete": "off"
    }
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group"
  }, [_c('button', {
    staticClass: "btn btn-primary",
    staticStyle: {
      "line-height": "1.2"
    },
    attrs: {
      "type": "submit"
    }
  }, [_c('strong', [_vm._v("Filter")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("Client No.")]), _vm._v(" "), _c('th', [_vm._v("Service")]), _vm._v(" "), _c('th', [_vm._v("Estimated Cost")]), _vm._v(" "), _c('th', [_vm._v("Status")]), _vm._v(" "), _c('th', [_vm._v("Filing Date")]), _vm._v(" "), _c('th', [_vm._v("Releasing Date")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('tfoot', [_c('tr', [_c('th', {
    staticStyle: {
      "text-align": "right"
    },
    attrs: {
      "colspan": "7"
    }
  })])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7c95cdec", module.exports)
  }
}

/***/ }),

/***/ 420:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('form', {
    staticClass: "form-inline",
    attrs: {
      "id": "todays-services-form"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        _vm.filterServices('form')
      }
    }
  }, [_vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-filter",
    class: {
      'btn-outline': _vm.activeButton != 'today'
    },
    staticStyle: {
      "margin-right": "2px"
    },
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.filterServices('today')
      }
    }
  }, [_vm._v("Today")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-filter",
    class: {
      'btn-outline': _vm.activeButton != 'yesterday'
    },
    staticStyle: {
      "margin-right": "2px"
    },
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.filterServices('yesterday')
      }
    }
  }, [_vm._v("Yesterday")])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('table', {
    staticClass: "table table-striped table-bordered table-hover dataTables-example",
    attrs: {
      "id": "todaysServicesTable"
    }
  }, [_vm._m(2), _vm._v(" "), _c('tbody', _vm._l((_vm.filServices), function(service) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(service.service_date))]), _vm._v(" "), _c('td', [(service.user != null) ? _c('span', [_vm._v(_vm._s(service.user.full_name))]) : _vm._e(), _vm._v(" "), (service.group_binded != '') ? _c('a', {
      staticClass: "pull-right",
      attrs: {
        "href": "#",
        "data-toggle": "popover",
        "data-placement": "top",
        "data-container": "body",
        "data-content": service.user.group_binded,
        "data-trigger": "hover"
      }
    }, [_c('b', {
      staticClass: "fa fa-user-circle group-icon m-r-2"
    })]) : _vm._e(), _vm._v(" "), (service.user.unread_notif > 0) ? _c('a', {
      staticClass: "pull-right",
      attrs: {
        "href": "#",
        "data-toggle": "popover",
        "data-placement": "top",
        "data-container": "body",
        "data-content": 'Unread Notifications : ' + service.user.unread_notif,
        "data-trigger": "hover"
      }
    }, [_c('b', {
      staticClass: "fa fa-envelope unread-icon m-r-1"
    })]) : _vm._e(), _vm._v(" "), (service.user.binded) ? _c('a', {
      staticClass: "pull-right",
      attrs: {
        "href": "#",
        "data-toggle": "popover",
        "data-placement": "top",
        "data-container": "body",
        "data-content": "App Installed by Client",
        "data-trigger": "hover"
      }
    }, [_c('b', {
      staticClass: "fa fa-mobile app-icon m-r-1"
    })]) : _vm._e()]), _vm._v(" "), _c('td', [(service.user != null) ? _c('span', [_vm._v(_vm._s(service.user.id))]) : _vm._e()]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.tracking))]), _vm._v(" "), _c('td', {
      staticStyle: {
        "width": "30% !important"
      }
    }, [(service.remarks != '') ? _c('a', {
      attrs: {
        "href": "#",
        "data-toggle": "popover",
        "data-placement": "right",
        "data-container": "body",
        "data-content": service.remarks,
        "data-trigger": "hover"
      }
    }, [_vm._v("\n\t\t\t              " + _vm._s(service.detail) + "\n\t\t\t            ")]) : _c('span', [_vm._v(_vm._s(service.detail))])]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.cost))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.charge))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.tip))]), _vm._v(" "), _c('td', {
      staticClass: "text-center"
    }, [_c('a', {
      attrs: {
        "href": "#",
        "data-toggle": "modal",
        "data-target": "#editServiceModal"
      },
      on: {
        "click": function($event) {
          _vm.$parent.setService(service)
        }
      }
    }, [_c('i', {
      staticClass: "fa fa-pencil-square-o"
    })]), _vm._v(" "), _c('a', {
      attrs: {
        "href": '/visa/client/' + service.client_id,
        "target": "_blank",
        "data-toggle": "tooltip",
        "data-placement": "left",
        "title": "View client profile"
      }
    }, [_c('i', {
      staticClass: "fa fa-arrow-right"
    })])])])
  }), 0)])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "input-daterange input-group",
    attrs: {
      "id": "todays-services-datepicker"
    }
  }, [_c('input', {
    staticClass: "input-sm form-control",
    attrs: {
      "type": "text",
      "name": "todays-service-date",
      "autocomplete": "off"
    }
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group"
  }, [_c('button', {
    staticClass: "btn btn-primary",
    staticStyle: {
      "line-height": "1.2"
    },
    attrs: {
      "type": "submit"
    }
  }, [_c('strong', [_vm._v("Filter")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Date")]), _vm._v(" "), _c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("ID")]), _vm._v(" "), _c('th', [_vm._v("Package")]), _vm._v(" "), _c('th', {
    staticStyle: {
      "width": "30% !important"
    }
  }, [_vm._v("Detail")]), _vm._v(" "), _c('th', [_vm._v("Cost")]), _vm._v(" "), _c('th', [_vm._v("Charge")]), _vm._v(" "), _c('th', [_vm._v("Tip")]), _vm._v(" "), _c('th', {
    staticStyle: {
      "width": "5%"
    }
  }, [_vm._v("Action")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-c9d0e120", module.exports)
  }
}

/***/ }),

/***/ 437:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(299);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("015c8f3d", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-3afdb553\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EditServiceDashboard.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-3afdb553\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EditServiceDashboard.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 442:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(304);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("f2c2cdc8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-6b581acb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeliverySchedule.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-6b581acb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DeliverySchedule.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 446:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(308);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("2c88baa8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-7c95cdec\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TodaysTasks.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-7c95cdec\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TodaysTasks.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 451:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(313);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("1cfbea45", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-c9d0e120\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TodaysServices.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-c9d0e120\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TodaysServices.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 478:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(188);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvZGFzaGJvYXJkL2luZGV4LmpzIiwid2VicGFjazovLy8uL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanM/ZGEwNCoqKiIsIndlYnBhY2s6Ly8vRWRpdFNlcnZpY2VEYXNoYm9hcmQudnVlIiwid2VicGFjazovLy9EZWxpdmVyeVNjaGVkdWxlLnZ1ZSIsIndlYnBhY2s6Ly8vVG9kYXlzU2VydmljZXMudnVlIiwid2VicGFjazovLy9Ub2RheXNUYXNrcy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZT9mMzdjIiwid2VicGFjazovLy8uL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzPzZiMmIqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RlbGl2ZXJ5U2NoZWR1bGUudnVlPzFhY2MiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL1RvZGF5c1Rhc2tzLnZ1ZT80MDA5Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9Ub2RheXNTZXJ2aWNlcy52dWU/YThiMiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRWRpdFNlcnZpY2VEYXNoYm9hcmQudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EZWxpdmVyeVNjaGVkdWxlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvVG9kYXlzU2VydmljZXMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9Ub2RheXNUYXNrcy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZT8wZTkzIiwid2VicGFjazovLy9EaWFsb2dNb2RhbC52dWU/ZTQ3NCoqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGVsaXZlcnlTY2hlZHVsZS52dWU/YzRjNiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvVG9kYXlzVGFza3MudnVlPzY4N2YiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL1RvZGF5c1NlcnZpY2VzLnZ1ZT9mZmJiIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0U2VydmljZURhc2hib2FyZC52dWU/ZjgyMSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGVsaXZlcnlTY2hlZHVsZS52dWU/NDVkNyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvVG9kYXlzVGFza3MudnVlPzFjMTQiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL1RvZGF5c1NlcnZpY2VzLnZ1ZT85Yjg2Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWU/MjA3YyoiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT9mZTFiKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qcz9lNmFjKioqIl0sIm5hbWVzIjpbIlZ1ZSIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJkYXRhIiwid2luZG93IiwiTGFyYXZlbCIsInVzZXIiLCJkYXNoYm9hcmQiLCJlbCIsInBlbmRpbmdTZXJ2aWNlcyIsIm9ucHJvY2Vzc1NlcnZpY2VzIiwidG9kYXlzUmVwb3J0cyIsImxhc3RSZXBvcnRlZERhdGUiLCJ0b2RheXNTZXJ2aWNlcyIsImRlbGl2ZXJ5U2NoZWR1bGVzIiwic2VydmljZSIsInNldHRpbmciLCJhY2Nlc3NfY29udHJvbCIsInBlcm1pc3Npb25zIiwiZm9ybSIsIkZvcm0iLCJpZCIsInJlYXNvbiIsImJhc2VVUkwiLCJjcGFuZWxfdXJsIiwiYWRkU2NoZWR1bGUiLCJpdGVtIiwicmlkZXJfaWQiLCJsb2NhdGlvbiIsInNjaGVkdWxlZF9kYXRlIiwic2NoZWR1bGVkX3RpbWUiLCJzY2hlZHVsZWRfaG91ciIsInNjaGVkdWxlZF9taW51dGUiLCJjb21wYW55Q291cmllcnMiLCJkdCIsInNlbElkIiwibW9kZSIsIm1ldGhvZHMiLCJjYWxsRGF0YVRhYmxlczEiLCIkIiwiRGF0YVRhYmxlIiwiZGVzdHJveSIsImF4aW9zIiwiZ2V0IiwidGhlbiIsInJlc3VsdCIsImZpbHRlciIsInIiLCJhY3RpdmUiLCJzZXRUaW1lb3V0IiwicmVzcG9uc2l2ZSIsIm9yZGVyIiwidG9vbHRpcCIsImNhbGxEYXRhVGFibGVzMiIsImNhbGxEYXRhVGFibGVzMyIsInJlc3BvbnNlIiwiY3JlYXRlRGF0YXRhYmxlVG9nZ2xlIiwiY2FsbERhdGFUYWJsZXM0IiwiY2FsbERhdGFUYWJsZXM1IiwicGFyYW1zIiwiZGF0ZSIsImNhbGxEYXRhVGFibGVzNiIsImlmVG9kYXkiLCJ0b2RheSIsIm1vbWVudCIsImZvcm1hdCIsInJlcG9ydHMiLCJ0YWJsZW5hbWUiLCJkIiwiaWRzIiwiY2xpZHMiLCJpIiwiY2xpZW50IiwibGVuZ3RoIiwiZm91bmQiLCJqUXVlcnkiLCJpbkFycmF5IiwiY2xpZW50X2lkIiwicHVzaCIsImZ1bGxuYW1lIiwiZGV0YWlsIiwidGFibGUyIiwicmVuZGVyIiwidHlwZSIsInJvdyIsInNpZCIsImxvZ19kYXRlIiwibWV0YSIsInByb2Nlc3NvciIsIm9mZiIsIm9uIiwiZSIsInRyMiIsImNsb3Nlc3QiLCJjaGlsZCIsImlzU2hvd24iLCJoaWRlIiwicmVtb3ZlQ2xhc3MiLCJzaG93IiwiYWRkQ2xhc3MiLCJyb3dzIiwiZXZlcnkiLCJub2RlIiwiYmluZCIsIm9wZW5Qcm9jZXNzU2VydmljZSIsIm1vZGFsIiwicmVtb3ZlU2VydmljZSIsInRvYXN0ciIsInN1Y2Nlc3MiLCJvcGVuUGVuZGluZ1NlcnZpY2UiLCJtYXJrQXNQZW5kaW5nIiwic3VibWl0Iiwic2V0U2VydmljZSIsInRyYWNraW5nIiwidGlwIiwic3RhdHVzIiwiY29zdCIsImV4dGVuZCIsInJjdl9kb2NzIiwiZG9jc19uZWVkZWQiLCJkb2NzX29wdGlvbmFsIiwiZGlzY291bnQyIiwiZGlzY291bnQiLCJkaXNjb3VudF9hbW91bnQiLCIkcmVmcyIsImVkaXRzZXJ2aWNlZGFzaGJvYXJkcmVmIiwiZmV0Y2hEb2NzIiwic3VibWl0RmlsdGVyIiwiYWxlcnQiLCJ2YWwiLCJzdWJtaXRGaWx0ZXIyIiwic3VibWl0RmlsdGVyMyIsInNob3dTY2hlZHVsZU1vZGFsIiwicG9zdCIsInNob3dEZWxldGVTY2hlZFByb21wdCIsImxvYWRDb21wYW55Q291cmllcnMiLCJyZXNldFNjaGVkRnJtIiwiZ2V0RGF0ZSIsInNhdmVEZWxpdmVyU2NoZWR1bGUiLCJtIiwiY29tYmluZVRpbWUiLCJkZWxpdmVyeSIsImFsd2F5c1R3b0RpZ2l0IiwiZm9ybWF0RGF0ZU1EWSIsImNhdGNoIiwiZXJyb3IiLCJkYXRlRmlsdGVyIiwiZmlsdGVyU2NoZWQiLCJkZWxldGVTY2hlZHVsZSIsImNyZWF0ZWQiLCJkYXRlcGlja2VyIiwiY29tcG9uZW50cyIsIm1vdW50ZWQiLCJwb3BvdmVyIiwiaHRtbCIsInRyaWdnZXIiLCJzZWxlY3RvciIsImRvY3VtZW50IiwidG9kYXlIaWdobGlnaHQiLCJyZWFkeSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBQSxJQUFJQyxTQUFKLENBQWMsY0FBZCxFQUErQkMsbUJBQU9BLENBQUMsQ0FBUixDQUEvQjs7QUFFQSxJQUFJQyxPQUFPQyxPQUFPQyxPQUFQLENBQWVDLElBQTFCOztBQUVBLElBQUlDLFlBQVksSUFBSVAsR0FBSixDQUFRO0FBQ3ZCUSxRQUFHLFlBRG9CO0FBRXZCTCxVQUFLO0FBQ0pNLHlCQUFnQixFQURaO0FBRUpDLDJCQUFrQixFQUZkO0FBR0pDLHVCQUFjLEVBSFY7QUFJRUMsMEJBQWlCLEVBSm5CO0FBS0pDLHdCQUFlLEVBTFg7QUFNRUMsMkJBQWtCLEVBTnBCO0FBT0pDLGlCQUFRLEVBUEo7QUFRRUMsaUJBQVNiLEtBQUtjLGNBQUwsQ0FBb0IsQ0FBcEIsRUFBdUJELE9BUmxDO0FBU0VFLHFCQUFhZixLQUFLZSxXQVRwQjtBQVVFQyxjQUFLLElBQUlDLElBQUosQ0FBUztBQUNaQyxnQkFBSSxFQURRO0FBRVpDLG9CQUFRO0FBRkksU0FBVCxFQUdGLEVBQUVDLFNBQVMsWUFBVW5CLE9BQU9DLE9BQVAsQ0FBZW1CLFVBQXBDLEVBSEUsQ0FWUDtBQWNFQyxxQkFBWSxJQUFJTCxJQUFKLENBQVM7QUFDbkJDLGdCQUFHLEVBRGdCO0FBRW5CSyxrQkFBSyxFQUZjO0FBR25CQyxzQkFBUyxFQUhVO0FBSW5CQyxzQkFBUyxFQUpVO0FBS25CQyw0QkFBZSxFQUxJO0FBTW5CQyw0QkFBZTtBQU5JLFNBQVQsRUFPVCxFQUFFUCxTQUFTLGFBQVgsRUFQUyxDQWRkO0FBc0JFUSx3QkFBZSxFQXRCakI7QUF1QkVDLDBCQUFpQixFQXZCbkI7QUF3QkVDLHlCQUFnQixFQXhCbEI7QUF5QkVDLFlBQUcsRUF6Qkw7QUEwQkVDLGVBQU0sRUExQlI7QUEyQkVDLGNBQUs7QUEzQlAsS0FGa0I7QUErQnZCQyxhQUFTO0FBQ0ZDLHVCQURFLDZCQUNlO0FBQUE7O0FBQ2hCQyxjQUFFLGVBQUYsRUFBbUJDLFNBQW5CLEdBQStCQyxPQUEvQjs7QUFFRkMsa0JBQU1DLEdBQU4sQ0FBVSw0QkFBVixFQUNHQyxJQURILENBQ1Esa0JBQVU7QUFDZCxzQkFBS25DLGVBQUwsR0FBdUJvQyxPQUFPMUMsSUFBUCxDQUFZMkMsTUFBWixDQUFtQjtBQUFBLDJCQUFLQyxFQUFFQyxNQUFGLElBQVksQ0FBakI7QUFBQSxpQkFBbkIsQ0FBdkI7O0FBRUVDLDJCQUFXLFlBQVU7QUFDakJWLHNCQUFFLGVBQUYsRUFBbUJDLFNBQW5CLENBQTZCO0FBQ3pCVSxvQ0FBWSxJQURhO0FBRXpCQywrQkFBTyxFQUZrQjtBQUd6QixzQ0FBYyxDQUFDLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULENBQUQsRUFBZSxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxDQUFmLENBSFc7QUFJekIsMENBQWtCO0FBSk8scUJBQTdCO0FBTVRaLHNCQUFFLFlBQVk7QUFDWkEsMEJBQUUseUJBQUYsRUFBNkJhLE9BQTdCO0FBQ0QscUJBRkQ7QUFHTSxpQkFWRCxFQVVFLElBVkY7QUFZTixhQWhCQTtBQWlCRSxTQXJCQztBQXNCRkMsdUJBdEJFLDZCQXNCZTtBQUFBOztBQUNoQmQsY0FBRSxpQkFBRixFQUFxQkMsU0FBckIsR0FBaUNDLE9BQWpDOztBQUVGQyxrQkFBTUMsR0FBTixDQUFVLDhCQUFWLEVBQ0dDLElBREgsQ0FDUSxrQkFBVTtBQUNmLHVCQUFLbEMsaUJBQUwsR0FBeUJtQyxPQUFPMUMsSUFBaEM7O0FBRUc4QywyQkFBVyxZQUFVO0FBQ2pCVixzQkFBRSxpQkFBRixFQUFxQkMsU0FBckIsQ0FBK0I7QUFDM0JVLG9DQUFZLElBRGU7QUFFM0Isc0NBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxDQUFELEVBQWUsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBZixDQUZhO0FBRzNCLDBDQUFrQjtBQUhTLHFCQUEvQjtBQUtUWCxzQkFBRSxZQUFZO0FBQ1pBLDBCQUFFLHlCQUFGLEVBQTZCYSxPQUE3QjtBQUNELHFCQUZEO0FBR00saUJBVEQsRUFTRSxJQVRGO0FBVU4sYUFkQTtBQWVFLFNBeENDO0FBeUNGRSx1QkF6Q0UsNkJBeUNlO0FBQUE7O0FBQ2hCZixjQUFFLG9CQUFGLEVBQXdCQyxTQUF4QixHQUFvQ0MsT0FBcEM7O0FBRUFDLGtCQUFNQyxHQUFOLENBQVUsMkJBQVYsRUFDRkMsSUFERSxDQUNHLG9CQUFZO0FBQ2QsdUJBQUtqQyxhQUFMLEdBQXFCNEMsU0FBU3BELElBQTlCO0FBQ00sdUJBQUtxRCxxQkFBTCxDQUEyQixPQUFLN0MsYUFBaEMsRUFBOEMsb0JBQTlDO0FBQ0E7QUFFVCxhQU5FO0FBT0EsU0FuREM7QUFvREY4Qyx1QkFwREUsNkJBb0RlO0FBQUE7O0FBQ2hCbEIsY0FBRSx3QkFBRixFQUE0QkMsU0FBNUIsR0FBd0NDLE9BQXhDOztBQUVBQyxrQkFBTUMsR0FBTixDQUFVLHFDQUFWLEVBQ0ZDLElBREUsQ0FDRyxvQkFBWTtBQUNkLHVCQUFLaEMsZ0JBQUwsR0FBd0IyQyxTQUFTcEQsSUFBakM7O0FBRU0sdUJBQUtxRCxxQkFBTCxDQUEyQixPQUFLNUMsZ0JBQWhDLEVBQWlELHdCQUFqRDtBQUNBO0FBRVQsYUFQRTtBQVFBLFNBL0RDO0FBaUVGOEMsdUJBakVFLDZCQWlFZTtBQUFBOztBQUNibkIsY0FBRSxzQkFBRixFQUEwQkMsU0FBMUIsR0FBc0NDLE9BQXRDO0FBQ0FrQixxQkFBUztBQUNMQyxzQkFBTTtBQURELGFBQVQ7QUFHQWxCLGtCQUFNQyxHQUFOLENBQVUsK0JBQVYsRUFBMkM7QUFDdkNnQix3QkFBUUE7QUFEK0IsYUFBM0MsRUFHQ2YsSUFIRCxDQUdNLG9CQUFZO0FBQ2QsdUJBQUsvQixjQUFMLEdBQXNCMEMsU0FBU3BELElBQS9CO0FBQ0E4QywyQkFBVyxZQUFVO0FBQ2pCVixzQkFBRSxzQkFBRixFQUEwQkMsU0FBMUIsQ0FBb0M7QUFDaENVLG9DQUFZLElBRG9CO0FBRWhDLHNDQUFjLENBQUMsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBRCxFQUFlLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULENBQWYsQ0FGa0I7QUFHaEMsMENBQWtCO0FBSGMscUJBQXBDO0FBS0FYLHNCQUFFLFlBQVk7QUFDWkEsMEJBQUUseUJBQUYsRUFBNkJhLE9BQTdCO0FBQ0QscUJBRkQ7QUFHSCxpQkFURCxFQVNFLElBVEY7QUFVQTtBQUVILGFBakJEO0FBa0JILFNBeEZDO0FBMEZGUyx1QkExRkUsNkJBMEZlO0FBQUE7O0FBQ2J0QixjQUFFLG1CQUFGLEVBQXVCQyxTQUF2QixHQUFtQ0MsT0FBbkM7QUFDQWtCLHFCQUFTO0FBQ0xDLHNCQUFNO0FBREQsYUFBVDtBQUdBbEIsa0JBQU1DLEdBQU4sQ0FBVSwrQkFBVixFQUEyQztBQUN2Q2dCLHdCQUFRQTtBQUQrQixhQUEzQyxFQUdDZixJQUhELENBR00sb0JBQVk7QUFDZCx1QkFBSzlCLGlCQUFMLEdBQXlCeUMsU0FBU3BELElBQWxDO0FBQ0E4QywyQkFBVyxZQUFVO0FBQ2pCVixzQkFBRSxtQkFBRixFQUF1QkMsU0FBdkIsQ0FBaUM7QUFDN0JVLG9DQUFZLElBRGlCO0FBRTdCLHNDQUFjLENBQUMsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBRCxFQUFlLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULENBQWYsQ0FGZTtBQUc3QiwwQ0FBa0I7QUFIVyxxQkFBakM7QUFLQVgsc0JBQUUsWUFBWTtBQUNaQSwwQkFBRSx5QkFBRixFQUE2QmEsT0FBN0I7QUFDRCxxQkFGRDtBQUdILGlCQVRELEVBU0UsSUFURjtBQVVBO0FBRUgsYUFqQkQ7QUFrQkgsU0FqSEM7QUFtSEZVLGVBbkhFLG1CQW1ITUYsSUFuSE4sRUFtSFk7QUFDVixnQkFBSUcsUUFBUUMsU0FBU0MsTUFBVCxDQUFnQixZQUFoQixDQUFaO0FBQ0EsZ0JBQUdGLFNBQVNILElBQVosRUFBaUI7QUFDYix1QkFBTyxXQUFQO0FBQ0g7QUFDRDtBQUNBLG1CQUFPLFlBQVVBLElBQWpCO0FBRUgsU0EzSEM7OztBQTZITjs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUlKLDZCQXhORSxpQ0F3Tm9CVSxPQXhOcEIsRUF3TjRCQyxTQXhONUIsRUF3TnVDOztBQUVyQzVCLGNBQUU0QixTQUFGLEVBQWEzQixTQUFiLEdBQXlCQyxPQUF6Qjs7QUFFQVEsdUJBQVcsWUFBVzs7QUFFbEIsb0JBQUlnQixTQUFTLFNBQVRBLE1BQVMsQ0FBU0csQ0FBVCxFQUFZO0FBQ3JCO0FBQ0Esd0JBQUlDLE1BQU0sRUFBVjtBQUNBLHdCQUFJQyxRQUFRLEVBQVo7QUFDQSx5QkFBSSxJQUFJQyxJQUFFLENBQVYsRUFBYUEsSUFBR0gsRUFBRUksTUFBSCxDQUFXQyxNQUExQixFQUFrQ0YsR0FBbEMsRUFBdUM7QUFDbkMsNEJBQUlHLFFBQVFDLE9BQU9DLE9BQVAsQ0FBZVIsRUFBRUksTUFBRixDQUFTRCxDQUFULEVBQVlNLFNBQTNCLEVBQXNDUCxLQUF0QyxDQUFaO0FBQ0M7QUFDRCw0QkFBSUksUUFBUSxDQUFaLEVBQWU7QUFDWDtBQUNBSixrQ0FBTVEsSUFBTixDQUFXVixFQUFFSSxNQUFGLENBQVNELENBQVQsRUFBWU0sU0FBdkI7QUFDQVIsbUNBQU8sMkJBQXlCRCxFQUFFSSxNQUFGLENBQVNELENBQVQsRUFBWU0sU0FBckMsR0FBK0MsbUdBQS9DLEdBQW1KVCxFQUFFSSxNQUFGLENBQVNELENBQVQsRUFBWVEsUUFBL0osR0FBeUsseUJBQXpLLEdBQW1NWCxFQUFFSSxNQUFGLENBQVNELENBQVQsRUFBWU0sU0FBL00sR0FBME4sTUFBMU4sR0FBaU8sR0FBeE87QUFDSDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBRUg7QUFDRDtBQUNBLDJCQUFPLGdHQUNLLGlDQURMLEdBRVMsc0JBRlQsR0FFZ0NULEVBQUVZLE1BRmxDLEdBRXlDLE9BRnpDO0FBR1M7QUFDSiwyQkFKTCxHQUtDLFVBTFI7QUFNSCxpQkF6QkQ7O0FBMkJBQyx5QkFBUzFDLEVBQUU0QixTQUFGLEVBQWEzQixTQUFiLENBQXdCO0FBQzdCLDRCQUFRMEIsT0FEcUI7QUFFN0JoQixnQ0FBWSxJQUZpQjtBQUc3QiwrQkFBVztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSSxpQ0FBUyxLQURiO0FBRUkscUNBQWtCLGlCQUZ0QjtBQUdJL0MsOEJBQU0sSUFIVixFQUdnQitFLFFBQVEsZ0JBQVNoQixPQUFULEVBQWtCaUIsSUFBbEIsRUFBd0JDLEdBQXhCLEVBQTZCO0FBQzdDLGdDQUFHbEIsUUFBUWlCLElBQVIsSUFBZ0IsUUFBbkIsRUFBNEI7QUFDeEIsdUNBQU8sZ0NBQWdDakIsUUFBUW5ELE9BQXhDLEdBQWtELE1BQXpEO0FBQ0g7QUFDRCxtQ0FBTyw4QkFBOEJtRCxRQUFRbkQsT0FBdEMsR0FBZ0QsTUFBdkQ7QUFDSDtBQVJMLHFCQVJPLEVBa0JQO0FBQ0kscUNBQWtCLGlCQUR0QjtBQUVJLGlDQUFTLEtBRmI7QUFHSVosOEJBQU0sSUFIVixFQUdnQitFLFFBQVEsZ0JBQVNoQixPQUFULEVBQWtCaUIsSUFBbEIsRUFBd0JDLEdBQXhCLEVBQTZCO0FBQzdDLGdDQUFJQyxNQUFNLEVBQVY7QUFDQSxpQ0FBSSxJQUFJZCxJQUFFLENBQVYsRUFBYUEsSUFBR0wsUUFBUU0sTUFBVCxDQUFpQkMsTUFBaEMsRUFBd0NGLEdBQXhDLEVBQTZDO0FBQ3pDYyx1Q0FBT25CLFFBQVFNLE1BQVIsQ0FBZUQsQ0FBZixFQUFrQk0sU0FBbEIsR0FBNEIsR0FBbkM7QUFDSDtBQUNELG1DQUFPLFFBQU1YLFFBQVFvQixRQUFkLEdBQXdCLGtDQUF4QixHQUEyREQsR0FBM0QsR0FBK0QsU0FBdEU7QUFDSDtBQVRMLHFCQWxCTyxFQTZCUDtBQUNJLHFDQUFrQixpQkFEdEI7QUFFSSxpQ0FBUyxLQUZiO0FBR0lsRiw4QkFBTSxJQUhWLEVBR2dCK0UsUUFBUSxnQkFBU2hCLE9BQVQsRUFBa0JpQixJQUFsQixFQUF3QkMsR0FBeEIsRUFBNkJHLElBQTdCLEVBQW1DO0FBQ25ELG1DQUFPLFFBQU1yQixRQUFRc0IsU0FBZCxHQUF5QixNQUFoQztBQUNIO0FBTEwscUJBN0JPLEVBb0NQO0FBQ0ksaUNBQVMsS0FEYjtBQUVJLHFDQUFrQix3QkFGdEI7QUFHSTtBQUNBckYsOEJBQU0sSUFKVixFQUlnQitFLFFBQVEsZ0JBQVNoQixPQUFULEVBQWtCaUIsSUFBbEIsRUFBd0JDLEdBQXhCLEVBQTZCRyxJQUE3QixFQUFtQztBQUNuRCxnQ0FBSWxCLE1BQU0sRUFBVjtBQUNBLGdDQUFJQyxRQUFRLEVBQVo7QUFDQSxpQ0FBSSxJQUFJQyxJQUFFLENBQVYsRUFBYUEsSUFBR0wsUUFBUU0sTUFBVCxDQUFpQkMsTUFBaEMsRUFBd0NGLEdBQXhDLEVBQTZDO0FBQ3pDLG9DQUFJRyxRQUFRQyxPQUFPQyxPQUFQLENBQWVWLFFBQVFNLE1BQVIsQ0FBZUQsQ0FBZixFQUFrQk0sU0FBakMsRUFBNENQLEtBQTVDLENBQVo7QUFDQztBQUNELG9DQUFJSSxRQUFRLENBQVosRUFBZTtBQUNYO0FBQ0FKLDBDQUFNUSxJQUFOLENBQVdaLFFBQVFNLE1BQVIsQ0FBZUQsQ0FBZixFQUFrQk0sU0FBN0I7QUFDQVIsMkNBQU8sMkJBQXlCSCxRQUFRTSxNQUFSLENBQWVELENBQWYsRUFBa0JNLFNBQTNDLEdBQXFELG1HQUFyRCxHQUF5SlgsUUFBUU0sTUFBUixDQUFlRCxDQUFmLEVBQWtCTSxTQUEzSyxHQUFzTCx5QkFBdEwsR0FBZ05YLFFBQVFNLE1BQVIsQ0FBZUQsQ0FBZixFQUFrQlEsUUFBbE8sR0FBNE8sTUFBNU8sR0FBbVAsS0FBMVA7QUFDSDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBRUg7QUFDRCxtQ0FBT1YsR0FBUDtBQUNIO0FBdEJMLHFCQXBDTztBQUhrQixpQkFBeEIsQ0FBVDtBQWlFQTlCLGtCQUFFLE1BQUYsRUFBVWtELEdBQVYsQ0FBYyxPQUFkLEVBQXVCLFVBQVF0QixTQUFSLEdBQWtCLDJCQUF6QztBQUNBNUIsa0JBQUUsTUFBRixFQUFVbUQsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBUXZCLFNBQVIsR0FBa0IsMkJBQXhDLEVBQXFFLFVBQVV3QixDQUFWLEVBQWE7QUFDOUUsd0JBQUlDLE1BQU1yRCxFQUFFLElBQUYsRUFBUXNELE9BQVIsQ0FBZ0IsSUFBaEIsQ0FBVjtBQUNBLHdCQUFJVCxNQUFNSCxPQUFPRyxHQUFQLENBQVlRLEdBQVosQ0FBVjs7QUFFQSx3QkFBR1IsSUFBSVUsS0FBSixDQUFVQyxPQUFWLEVBQUgsRUFBd0I7QUFDcEJYLDRCQUFJVSxLQUFKLENBQVVFLElBQVY7QUFDQUosNEJBQUlLLFdBQUosQ0FBZ0IsT0FBaEI7QUFDSCxxQkFIRCxNQUdPO0FBQ0hiLDRCQUFJVSxLQUFKLENBQVc3QixPQUFPbUIsSUFBSWpGLElBQUosRUFBUCxDQUFYLEVBQWdDK0YsSUFBaEM7QUFDQU4sNEJBQUlPLFFBQUosQ0FBYSxPQUFiO0FBQ0g7QUFDSixpQkFYRDtBQVlBO0FBQ0FsQix1QkFBT21CLElBQVAsR0FBY0MsS0FBZCxDQUFvQixZQUFVO0FBQzFCO0FBQ0Esd0JBQUcsQ0FBQyxLQUFLUCxLQUFMLENBQVdDLE9BQVgsRUFBSixFQUF5QjtBQUNyQjtBQUNBLDZCQUFLRCxLQUFMLENBQVc3QixPQUFPLEtBQUs5RCxJQUFMLEVBQVAsQ0FBWCxFQUFnQytGLElBQWhDO0FBQ0EzRCwwQkFBRSxLQUFLK0QsSUFBTCxFQUFGLEVBQWVILFFBQWYsQ0FBd0IsT0FBeEI7QUFDSDtBQUNKLGlCQVBEOztBQVVBO0FBQ0E1RCxrQkFBRSx3QkFBRixFQUE0Qm1ELEVBQTVCLENBQStCLE9BQS9CLEVBQXdDLFlBQVU7QUFDOUM7QUFDQVQsMkJBQU9tQixJQUFQLEdBQWNDLEtBQWQsQ0FBb0IsWUFBVTtBQUMxQjtBQUNBLDRCQUFHLENBQUMsS0FBS1AsS0FBTCxDQUFXQyxPQUFYLEVBQUosRUFBeUI7QUFDckI7QUFDQSxpQ0FBS0QsS0FBTCxDQUFXN0IsT0FBTyxLQUFLOUQsSUFBTCxFQUFQLENBQVgsRUFBZ0MrRixJQUFoQztBQUNBM0QsOEJBQUUsS0FBSytELElBQUwsRUFBRixFQUFlSCxRQUFmLENBQXdCLE9BQXhCO0FBQ0g7QUFDSixxQkFQRDtBQVFILGlCQVZEOztBQVlBO0FBQ0E1RCxrQkFBRSx3QkFBRixFQUE0Qm1ELEVBQTVCLENBQStCLE9BQS9CLEVBQXdDLFlBQVU7QUFDOUM7QUFDQVQsMkJBQU9tQixJQUFQLEdBQWNDLEtBQWQsQ0FBb0IsWUFBVTtBQUMxQjtBQUNBLDRCQUFHLEtBQUtQLEtBQUwsQ0FBV0MsT0FBWCxFQUFILEVBQXdCO0FBQ3BCO0FBQ0EsaUNBQUtELEtBQUwsQ0FBV0UsSUFBWDtBQUNBekQsOEJBQUUsS0FBSytELElBQUwsRUFBRixFQUFlTCxXQUFmLENBQTJCLE9BQTNCO0FBQ0g7QUFDSixxQkFQRDtBQVFILGlCQVZEO0FBWUgsYUFoSlUsQ0FnSlRNLElBaEpTLENBZ0pKLElBaEpJLENBQVgsRUFnSmMsSUFoSmQ7QUFrSkgsU0E5V0M7QUFpWEZDLDBCQWpYRSw4QkFpWGlCekYsT0FqWGpCLEVBaVh5QjtBQUMxQixpQkFBS0EsT0FBTCxHQUFlQSxPQUFmO0FBQ0F3QixjQUFFLGVBQUYsRUFBbUJrRSxLQUFuQixDQUF5QixRQUF6QjtBQUNBLFNBcFhDO0FBcVhGQyxxQkFyWEUsMkJBcVhhO0FBQUE7O0FBQ3BCaEUsa0JBQU1DLEdBQU4sQ0FBVSw4QkFBNEIsS0FBSzVCLE9BQUwsQ0FBYU0sRUFBbkQsRUFDT3VCLElBRFAsQ0FDWSxrQkFBVTtBQUNkLHVCQUFLUyxlQUFMO0FBQ0FzRCx1QkFBT0MsT0FBUCxDQUFlLCtCQUFmO0FBQ0osYUFKSjtBQUtNLFNBM1hDO0FBNFhGQywwQkE1WEUsOEJBNFhpQjlGLE9BNVhqQixFQTRYeUI7QUFDMUIsaUJBQUtBLE9BQUwsR0FBZUEsT0FBZjtBQUNBLGlCQUFLSSxJQUFMLENBQVVFLEVBQVYsR0FBZSxLQUFLTixPQUFMLENBQWFNLEVBQTVCO0FBQ0FrQixjQUFFLGVBQUYsRUFBbUJrRSxLQUFuQixDQUF5QixRQUF6QjtBQUNBLFNBaFlDO0FBaVlGSyxxQkFqWUUsMkJBaVlhO0FBQUE7O0FBQ1gsaUJBQUszRixJQUFMLENBQVU0RixNQUFWLENBQWlCLE1BQWpCLEVBQXlCLDBCQUF6QixFQUNDbkUsSUFERCxDQUNNLGtCQUFVO0FBQ2YsdUJBQUtOLGVBQUw7QUFDQSx1QkFBS2UsZUFBTDtBQUNHc0QsdUJBQU9DLE9BQVAsQ0FBZSw0QkFBZjtBQUNBckUsa0JBQUUsZUFBRixFQUFtQmtFLEtBQW5CLENBQXlCLFFBQXpCO0FBQ0EsdUJBQUsxRixPQUFMLEdBQWUsRUFBZjtBQUNBLHVCQUFLSSxJQUFMLENBQVVFLEVBQVYsR0FBZSxFQUFmO0FBQ0EsdUJBQUtGLElBQUwsQ0FBVUcsTUFBVixHQUFtQixFQUFuQjtBQUNILGFBVEQ7QUFVSCxTQTVZQztBQTZZRjBGLGtCQTdZRSxzQkE2WVNqRyxPQTdZVCxFQTZZa0I7QUFBQSxnQkFDYk0sRUFEYSxHQUM0RU4sT0FENUUsQ0FDYk0sRUFEYTtBQUFBLGdCQUNUNEYsUUFEUyxHQUM0RWxHLE9BRDVFLENBQ1RrRyxRQURTO0FBQUEsZ0JBQ0NDLEdBREQsR0FDNEVuRyxPQUQ1RSxDQUNDbUcsR0FERDtBQUFBLGdCQUNNQyxNQUROLEdBQzRFcEcsT0FENUUsQ0FDTW9HLE1BRE47QUFBQSxnQkFDY0MsSUFEZCxHQUM0RXJHLE9BRDVFLENBQ2NxRyxJQURkO0FBQUEsZ0JBQ29CcEUsTUFEcEIsR0FDNEVqQyxPQUQ1RSxDQUNvQmlDLE1BRHBCO0FBQUEsZ0JBQzRCcUUsTUFENUIsR0FDNEV0RyxPQUQ1RSxDQUM0QnNHLE1BRDVCO0FBQUEsZ0JBQ29DQyxRQURwQyxHQUM0RXZHLE9BRDVFLENBQ29DdUcsUUFEcEM7QUFBQSxnQkFDOENDLFdBRDlDLEdBQzRFeEcsT0FENUUsQ0FDOEN3RyxXQUQ5QztBQUFBLGdCQUMyREMsYUFEM0QsR0FDNEV6RyxPQUQ1RSxDQUMyRHlHLGFBRDNEOzs7QUFHbkIsZ0JBQUd6RyxRQUFRMEcsU0FBUixDQUFrQmhELE1BQWxCLElBQTRCLENBQS9CLEVBQWtDO0FBQ2pDaUQsMkJBQVcsRUFBWDtBQUNBcEcseUJBQVMsRUFBVDtBQUNBLGFBSEQsTUFHTztBQUNOb0csMkJBQVczRyxRQUFRMEcsU0FBUixDQUFrQixDQUFsQixFQUFxQkUsZUFBaEM7QUFDQXJHLHlCQUFTUCxRQUFRMEcsU0FBUixDQUFrQixDQUFsQixFQUFxQm5HLE1BQTlCO0FBQ0E7O0FBRUQsaUJBQUtzRyxLQUFMLENBQVdDLHVCQUFYLENBQW1DYixVQUFuQyxDQUE4QzNGLEVBQTlDLEVBQWtENEYsUUFBbEQsRUFBNERDLEdBQTVELEVBQWlFQyxNQUFqRSxFQUF5RUMsSUFBekUsRUFBK0VNLFFBQS9FLEVBQXlGcEcsTUFBekYsRUFBaUcwQixNQUFqRyxFQUF5R3FFLE1BQXpHLEVBQWlIQyxRQUFqSDtBQUNHLGlCQUFLTSxLQUFMLENBQVdDLHVCQUFYLENBQW1DQyxTQUFuQyxDQUE2Q1AsV0FBN0MsRUFBMERDLGFBQTFEO0FBQ0gsU0ExWkM7QUEyWkZPLG9CQTNaRSwwQkEyWlk7QUFBQTs7QUFDVkM7O0FBRUF6RixjQUFFLHNCQUFGLEVBQTBCQyxTQUExQixHQUFzQ0MsT0FBdEM7QUFDQWtCLHFCQUFTO0FBQ0xDLHNCQUFNckIsRUFBRSx3Q0FBRixFQUE0QzBGLEdBQTVDO0FBREQsYUFBVDtBQUdBdkYsa0JBQU1DLEdBQU4sQ0FBVSwrQkFBVixFQUEyQztBQUN2Q2dCLHdCQUFRQTtBQUQrQixhQUEzQyxFQUdDZixJQUhELENBR00sb0JBQVk7QUFDZCx1QkFBSy9CLGNBQUwsR0FBc0IwQyxTQUFTcEQsSUFBL0I7QUFDQThDLDJCQUFXLFlBQVU7QUFDakJWLHNCQUFFLHNCQUFGLEVBQTBCQyxTQUExQixDQUFvQztBQUNoQ1Usb0NBQVksSUFEb0I7QUFFaEMsc0NBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxDQUFELEVBQWUsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBZixDQUZrQjtBQUdoQywwQ0FBa0I7QUFIYyxxQkFBcEM7QUFLQVgsc0JBQUUsWUFBWTtBQUNaQSwwQkFBRSx5QkFBRixFQUE2QmEsT0FBN0I7QUFDRCxxQkFGRDtBQUdILGlCQVRELEVBU0UsSUFURjtBQVdILGFBaEJEO0FBa0JILFNBcGJDO0FBc2JGOEUscUJBdGJFLHlCQXNiWUQsR0F0YlosRUFzYmdCO0FBQUE7O0FBQ2QxRixjQUFFLHNCQUFGLEVBQTBCQyxTQUExQixHQUFzQ0MsT0FBdEM7QUFDQSxnQkFBR3dGLE9BQU8sQ0FBVixFQUFZO0FBQ1IxRixrQkFBRSxpQkFBRixFQUFxQjBELFdBQXJCLENBQWlDLGFBQWpDO0FBQ0ExRCxrQkFBRSxhQUFGLEVBQWlCNEQsUUFBakIsQ0FBMEIsYUFBMUI7QUFDSCxhQUhELE1BSUk7QUFDQTVELGtCQUFFLGFBQUYsRUFBaUIwRCxXQUFqQixDQUE2QixhQUE3QjtBQUNBMUQsa0JBQUUsaUJBQUYsRUFBcUI0RCxRQUFyQixDQUE4QixhQUE5QjtBQUNIO0FBQ0R4QyxxQkFBUztBQUNMQyxzQkFBTXFFO0FBREQsYUFBVDtBQUdBdkYsa0JBQU1DLEdBQU4sQ0FBVSwrQkFBVixFQUEyQztBQUN2Q2dCLHdCQUFRQTtBQUQrQixhQUEzQyxFQUdDZixJQUhELENBR00sb0JBQVk7QUFDZCx3QkFBSy9CLGNBQUwsR0FBc0IwQyxTQUFTcEQsSUFBL0I7QUFDQThDLDJCQUFXLFlBQVU7QUFDakJWLHNCQUFFLHNCQUFGLEVBQTBCQyxTQUExQixDQUFvQztBQUNoQ1Usb0NBQVksSUFEb0I7QUFFaEMsc0NBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxDQUFELEVBQWUsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBZixDQUZrQjtBQUdoQywwQ0FBa0I7QUFIYyxxQkFBcEM7QUFLQVgsc0JBQUUsWUFBWTtBQUNaQSwwQkFBRSx5QkFBRixFQUE2QmEsT0FBN0I7QUFDRCxxQkFGRDtBQUdILGlCQVRELEVBU0UsSUFURjtBQVdILGFBaEJEO0FBa0JILFNBcmRDO0FBdWRGK0UscUJBdmRFLHlCQXVkWUYsR0F2ZFosRUF1ZGdCO0FBQUE7O0FBQ2QxRixjQUFFLG1CQUFGLEVBQXVCQyxTQUF2QixHQUFtQ0MsT0FBbkM7QUFDQSxnQkFBR3dGLE9BQU8sQ0FBVixFQUFZO0FBQ1IxRixrQkFBRSxZQUFGLEVBQWdCMEQsV0FBaEIsQ0FBNEIsYUFBNUI7QUFDQTFELGtCQUFFLGNBQUYsRUFBa0I0RCxRQUFsQixDQUEyQixhQUEzQjtBQUNILGFBSEQsTUFJSyxJQUFHOEIsT0FBTyxDQUFWLEVBQVk7QUFDYjFGLGtCQUFFLGNBQUYsRUFBa0IwRCxXQUFsQixDQUE4QixhQUE5QjtBQUNBMUQsa0JBQUUsWUFBRixFQUFnQjRELFFBQWhCLENBQXlCLGFBQXpCO0FBQ0gsYUFISSxNQUlEO0FBQ0E4QixzQkFBTTFGLEVBQUUsMENBQUYsRUFBOEMwRixHQUE5QyxFQUFOO0FBQ0g7QUFDRHRFLHFCQUFTO0FBQ0xDLHNCQUFNcUU7QUFERCxhQUFUO0FBR0F2RixrQkFBTUMsR0FBTixDQUFVLCtCQUFWLEVBQTJDO0FBQ3ZDZ0Isd0JBQVFBO0FBRCtCLGFBQTNDLEVBR0NmLElBSEQsQ0FHTSxvQkFBWTtBQUNkLHdCQUFLOUIsaUJBQUwsR0FBeUJ5QyxTQUFTcEQsSUFBbEM7QUFDQThDLDJCQUFXLFlBQVU7QUFDakJWLHNCQUFFLG1CQUFGLEVBQXVCQyxTQUF2QixDQUFpQztBQUM3QlUsb0NBQVksSUFEaUI7QUFFN0Isc0NBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxDQUFELEVBQWUsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBZixDQUZlO0FBRzdCLDBDQUFrQjtBQUhXLHFCQUFqQztBQUtBWCxzQkFBRSxZQUFZO0FBQ1pBLDBCQUFFLHlCQUFGLEVBQTZCYSxPQUE3QjtBQUNELHFCQUZEO0FBR0gsaUJBVEQsRUFTRSxJQVRGO0FBV0gsYUFoQkQ7QUFpQkgsU0F4ZkM7QUEwZkZnRix5QkExZkUsNkJBMGZnQkgsR0ExZmhCLEVBMGZxQjVHLEVBMWZyQixFQTBmd0I7QUFBQTs7QUFDdEIsZ0JBQUc0RyxPQUFPLEtBQVYsRUFBZ0I7QUFDWjFGLGtCQUFFLG1CQUFGLEVBQXVCa0UsS0FBdkIsQ0FBNkIsUUFBN0I7QUFDQSxxQkFBS3JFLElBQUwsR0FBWSxLQUFaO0FBQ0gsYUFIRCxNQUdNLElBQUc2RixPQUFPLE1BQVYsRUFBaUI7QUFDbkJ2RixzQkFBTTJGLElBQU4sQ0FBVyx5QkFBd0JoSCxFQUFuQyxFQUNDdUIsSUFERCxDQUNNLGtCQUFVO0FBQ1osNEJBQUtuQixXQUFMLEdBQW1CLElBQUlMLElBQUosQ0FBVTtBQUN6QkMsNEJBQWlCd0IsT0FBTzFDLElBQVAsQ0FBWWtCLEVBREo7QUFFekJLLDhCQUFpQm1CLE9BQU8xQyxJQUFQLENBQVl1QixJQUZKO0FBR3pCQyxrQ0FBaUJrQixPQUFPMUMsSUFBUCxDQUFZd0IsUUFISjtBQUl6QkMsa0NBQWlCaUIsT0FBTzFDLElBQVAsQ0FBWXlCLFFBSko7QUFLekJDLHdDQUFpQmdCLE9BQU8xQyxJQUFQLENBQVkwQixjQUxKO0FBTXpCQyx3Q0FBaUJlLE9BQU8xQyxJQUFQLENBQVkyQjtBQU5KLHFCQUFWLEVBT2pCLEVBQUVQLFNBQVMsYUFBWCxFQVBpQixDQUFuQjtBQVFBLDRCQUFLVyxFQUFMLEdBQVVXLE9BQU8xQyxJQUFQLENBQVkwQixjQUF0QjtBQUNBVSxzQkFBRSwwQ0FBRixFQUE4QzBGLEdBQTlDLENBQWtEcEYsT0FBTzFDLElBQVAsQ0FBWTBCLGNBQTlEO0FBQ0EsNEJBQUtPLElBQUwsR0FBWSxNQUFaO0FBQ0gsaUJBYkQ7QUFjQUcsa0JBQUUsbUJBQUYsRUFBdUJrRSxLQUF2QixDQUE2QixRQUE3QjtBQUNIO0FBQ0osU0EvZ0JDO0FBaWhCRjZCLDZCQWpoQkUsaUNBaWhCb0JMLEdBamhCcEIsRUFpaEJ3QjtBQUN0QixpQkFBSzlGLEtBQUwsR0FBYThGLEdBQWI7QUFDQTFGLGNBQUUsb0JBQUYsRUFBd0JrRSxLQUF4QixDQUE4QixRQUE5QjtBQUNILFNBcGhCQztBQXNoQkY4QiwyQkF0aEJFLGlDQXNoQm1CO0FBQUE7O0FBQ2pCN0Ysa0JBQU1DLEdBQU4sQ0FBVSx5QkFBVixFQUNDQyxJQURELENBQ00sb0JBQVk7QUFDZCx3QkFBS1gsZUFBTCxHQUF1QnNCLFNBQVNwRCxJQUFoQztBQUNILGFBSEQ7QUFJSCxTQTNoQkM7QUE2aEJGcUkscUJBN2hCRSwyQkE2aEJhO0FBQ1hqRyxjQUFFLHlDQUFGLEVBQTZDMEYsR0FBN0M7QUFDQSxpQkFBS3hHLFdBQUwsR0FBbUIsSUFBSUwsSUFBSixDQUFVO0FBQ3pCTSxzQkFBTSxFQURtQjtBQUV6QkMsMEJBQVUsRUFGZTtBQUd6QkMsMEJBQVMsRUFIZ0I7QUFJekJDLGdDQUFlLEVBSlU7QUFLekJDLGdDQUFlO0FBTFUsYUFBVixFQU1qQixFQUFFUCxTQUFTLGFBQVgsRUFOaUIsQ0FBbkI7QUFPSCxTQXRpQkM7QUF3aUJGa0gsZUF4aUJFLHFCQXdpQk87QUFDTDtBQUNBLGlCQUFLdkcsRUFBTCxHQUFVSyxFQUFFLDBDQUFGLEVBQThDMEYsR0FBOUMsRUFBVjtBQUNILFNBM2lCQztBQTZpQkZTLDJCQTdpQkUsaUNBNmlCbUI7QUFBQTs7QUFDakIsZ0JBQUlDLElBQUksS0FBS3ZHLElBQWI7QUFDQSxnQkFBSXdHLGNBQWMsS0FBS2hCLEtBQUwsQ0FBV2lCLFFBQVgsQ0FBb0JDLGNBQXBCLENBQW1DLEtBQUsvRyxjQUF4QyxJQUF3RCxHQUF4RCxHQUE0RCxLQUFLNkYsS0FBTCxDQUFXaUIsUUFBWCxDQUFvQkMsY0FBcEIsQ0FBbUMsS0FBSzlHLGdCQUF4QyxDQUE1RCxHQUFzSCxLQUF4STtBQUNBLGlCQUFLUCxXQUFMLENBQWlCSyxjQUFqQixHQUFrQzhHLFdBQWxDO0FBQ0EsaUJBQUtuSCxXQUFMLENBQWlCSSxjQUFqQixHQUFrQyxLQUFLK0YsS0FBTCxDQUFXaUIsUUFBWCxDQUFvQkUsYUFBcEIsQ0FBa0MsS0FBS3RILFdBQUwsQ0FBaUJJLGNBQW5ELENBQWxDOztBQUVBLGdCQUFHOEcsS0FBSyxLQUFSLEVBQWM7QUFDVjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFLbEgsV0FBTCxDQUFpQnNGLE1BQWpCLENBQXdCLE1BQXhCLEVBQWtDLHlCQUFsQyxFQUNLbkUsSUFETCxDQUNVLGtCQUFVO0FBQ2hCTCxzQkFBRSxtQkFBRixFQUF1QmtFLEtBQXZCLENBQTZCLFFBQTdCO0FBQ0FFLDJCQUFPQyxPQUFQLENBQWUsaUJBQWY7QUFDQSw0QkFBSzRCLGFBQUw7QUFDSCxpQkFMRCxFQU1DUSxLQU5ELENBTU8saUJBQVM7QUFDWnJDLDJCQUFPc0MsS0FBUCxDQUFhLGtDQUFiO0FBQ0gsaUJBUkQ7QUFTQSxxQkFBS1QsYUFBTDtBQUNBO0FBQ0gsYUFoQkQsTUFnQk0sSUFBSUcsS0FBSyxNQUFULEVBQWdCO0FBQ2xCO0FBQ0EscUJBQUtsSCxXQUFMLENBQWlCc0YsTUFBakIsQ0FBd0IsTUFBeEIsRUFBa0MsWUFBbEMsRUFDS25FLElBREwsQ0FDVSxrQkFBVTtBQUNoQkwsc0JBQUUsbUJBQUYsRUFBdUJrRSxLQUF2QixDQUE2QixRQUE3QjtBQUNBRSwyQkFBT0MsT0FBUCxDQUFlLGtCQUFmO0FBQ0EsNEJBQUs0QixhQUFMO0FBQ0gsaUJBTEQsRUFNQ1EsS0FORCxDQU1PLGlCQUFTO0FBQ1pyQywyQkFBT3NDLEtBQVAsQ0FBYSx5Q0FBYjtBQUNILGlCQVJEO0FBU0EscUJBQUtULGFBQUw7QUFDQTtBQUNIO0FBQ0QsaUJBQUtaLEtBQUwsQ0FBV2lCLFFBQVgsQ0FBb0JLLFVBQXBCLEdBQWlDLEtBQUt6SCxXQUFMLENBQWlCSSxjQUFsRDtBQUNBLGlCQUFLK0YsS0FBTCxDQUFXaUIsUUFBWCxDQUFvQk0sV0FBcEIsQ0FBZ0MsTUFBaEM7QUFDSCxTQW5sQkM7QUFxbEJGQyxzQkFybEJFLDBCQXFsQmEvSCxFQXJsQmIsRUFxbEJnQjtBQUFBOztBQUNkcUIsa0JBQU0yRixJQUFOLENBQVcseUJBQXVCaEgsRUFBbEMsRUFDQ3VCLElBREQsQ0FDTSxrQkFBVTtBQUNaTCxrQkFBRSxvQkFBRixFQUF3QmtFLEtBQXhCLENBQThCLFFBQTlCO0FBQ0FFLHVCQUFPQyxPQUFQLENBQWUsc0JBQWY7QUFDQSx3QkFBS2dCLEtBQUwsQ0FBV2lCLFFBQVgsQ0FBb0JNLFdBQXBCLENBQWdDLE9BQWhDO0FBQ0MsYUFMTCxFQU1DSCxLQU5ELENBTU8saUJBQVM7QUFDWnpHLGtCQUFFLG9CQUFGLEVBQXdCa0UsS0FBeEIsQ0FBOEIsUUFBOUI7QUFDQUUsdUJBQU9zQyxLQUFQLENBQWEsZUFBYjtBQUNILGFBVEQ7QUFXSDtBQWptQkMsS0EvQmM7QUFtb0J2QkksV0Fub0J1QixxQkFtb0JkO0FBQ1IsYUFBSy9HLGVBQUw7QUFDQSxhQUFLZSxlQUFMO0FBQ0EsYUFBS0MsZUFBTDtBQUNNLGFBQUtHLGVBQUw7QUFDTjtBQUNNO0FBQ0EsYUFBSzhFLG1CQUFMO0FBQ0loRyxVQUFFLHdDQUFGLEVBQTRDK0csVUFBNUMsQ0FBdUQsRUFBQ3JGLFFBQU8sWUFBUixFQUF2RDtBQUNBMUIsVUFBRSx3Q0FBRixFQUE0QytHLFVBQTVDLENBQXVELFNBQXZELEVBQWtFLE9BQWxFO0FBQ1YsS0E3b0JzQjs7QUE4b0J2QkMsZ0JBQVk7QUFDWCxrQ0FBMEJySixtQkFBT0EsQ0FBQyxHQUFSLENBRGY7QUFFTCx3QkFBZ0JBLG1CQUFPQSxDQUFDLEdBQVIsQ0FGWDtBQUdMLDJCQUFtQkEsbUJBQU9BLENBQUMsR0FBUixDQUhkO0FBSUwsNkJBQXFCQSxtQkFBT0EsQ0FBQyxHQUFSO0FBSmhCLEtBOW9CVztBQW9wQnBCc0osV0FwcEJvQixxQkFvcEJWO0FBQ05qSCxVQUFFLE1BQUYsRUFBVWtILE9BQVYsQ0FBa0IsRUFBRTtBQUNoQkMsa0JBQUssSUFEUztBQUVkQyxxQkFBUyxPQUZLO0FBR2RDLHNCQUFVO0FBSEksU0FBbEI7O0FBTUFySCxVQUFFc0gsUUFBRixFQUFZbkUsRUFBWixDQUFlLE9BQWYsRUFBd0IsdUNBQXhCLEVBQWlFLFlBQVU7QUFDdkVuRCxjQUFFLElBQUYsRUFBUStHLFVBQVIsQ0FBbUI7QUFDZnJGLHdCQUFPLFlBRFE7QUFFZjZGLGdDQUFnQjtBQUZELGFBQW5CO0FBSUgsU0FMRDs7QUFPQXZILFVBQUVzSCxRQUFGLEVBQVluRSxFQUFaLENBQWUsT0FBZixFQUF3Qix3Q0FBeEIsRUFBa0UsWUFBVTtBQUN4RW5ELGNBQUUsSUFBRixFQUFRK0csVUFBUixDQUFtQjtBQUNmckYsd0JBQU8sWUFEUTtBQUVmNkYsZ0NBQWdCO0FBRkQsYUFBbkI7QUFJSCxTQUxEOztBQU9BdkgsVUFBRXNILFFBQUYsRUFBWW5FLEVBQVosQ0FBZSxPQUFmLEVBQXdCLHlDQUF4QixFQUFtRSxZQUFVO0FBQ3pFbkQsY0FBRSxJQUFGLEVBQVErRyxVQUFSLENBQW1CO0FBQ2ZyRix3QkFBTyxZQURRO0FBRWY2RixnQ0FBZ0I7QUFGRCxhQUFuQjtBQUlILFNBTEQ7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSDtBQXJyQm1CLENBQVIsQ0FBaEI7O0FBd3JCQXZILEVBQUdzSCxRQUFILEVBQWNFLEtBQWQsQ0FBb0IsWUFBVztBQUMzQnhILE1BQUUsdUJBQUYsRUFBMkJrSCxPQUEzQjs7QUFFQWxILE1BQUUsTUFBRixFQUFVYSxPQUFWLENBQWtCO0FBQ2R3RyxrQkFBVTtBQURJLEtBQWxCO0FBR0gsQ0FORCxFOzs7Ozs7O0FDNXJCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsaUJBQWlCO0FBQ2pDO0FBQ0E7QUFDQSx3Q0FBd0MsZ0JBQWdCO0FBQ3hELElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsaUJBQWlCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvQkFBb0I7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaUhBO0FBRUEsS0FGQSxrQkFFQTtBQUNBO0FBQ0EsU0FEQTtBQUVBLG1CQUZBO0FBR0EsbUJBSEE7QUFJQTtBQUNBLFVBREE7QUFFQSxnQkFGQTtBQUdBLFdBSEE7QUFJQSxjQUpBO0FBS0EsWUFMQTtBQU1BLGdCQU5BO0FBT0EsY0FQQTtBQVFBLFlBUkE7QUFTQSxjQVRBO0FBVUEsaUJBVkE7QUFXQTtBQVhBO0FBSkE7QUFrQkEsRUFyQkE7OztBQXVCQTtBQUVBLGtCQUZBLDhCQUVBOztBQUVBO0FBQ0EsaUJBREE7QUFFQSx5Q0FGQTtBQUdBO0FBSEE7QUFNQSxHQVZBO0FBWUEsU0FaQSxxQkFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBaEJBO0FBa0JBLFlBbEJBLHNCQWtCQSxFQWxCQSxFQWtCQSxRQWxCQSxFQWtCQSxHQWxCQSxFQWtCQSxNQWxCQSxFQWtCQSxJQWxCQSxFQWtCQSxRQWxCQSxFQWtCQSxNQWxCQSxFQWtCQSxNQWxCQSxFQWtCQSxNQWxCQSxFQWtCQSxRQWxCQSxFQWtCQTtBQUNBO0FBQ0EsVUFEQSxFQUNBLGtCQURBLEVBQ0EsUUFEQSxFQUNBLGNBREEsRUFDQSxVQURBLEVBQ0Esa0JBREEsRUFDQSxjQURBLEVBQ0EsY0FEQSxFQUNBLGNBREEsRUFDQSxrQkFEQTtBQUVBLEdBckJBO0FBdUJBLHNCQXZCQSxrQ0F1QkE7QUFDQTtBQUNBLFVBREE7QUFFQSxnQkFGQTtBQUdBLFdBSEE7QUFJQSxjQUpBO0FBS0EsWUFMQTtBQU1BLGdCQU5BO0FBT0EsY0FQQTtBQVFBLFlBUkE7QUFTQSxjQVRBO0FBVUEsaUJBVkE7QUFXQSxjQVhBO0FBWUE7QUFaQTtBQWNBLEdBdENBO0FBd0NBLGFBeENBLHlCQXdDQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBLElBRkE7O0FBSUE7QUFDQTtBQUNBO0FBQ0EsSUFGQTs7QUFJQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQUZBLE1BRUE7QUFDQTtBQUNBLElBRkEsTUFFQTs7QUFFQTtBQUNBLGdDQURBO0FBRUEsNENBRkE7QUFHQSxrQ0FIQTtBQUlBLHdDQUpBO0FBS0Esb0NBTEE7QUFNQSw0Q0FOQTtBQU9BLHdDQVBBO0FBUUEsb0NBUkE7QUFTQSx3Q0FUQTtBQVVBLDhDQVZBO0FBV0Esd0NBWEE7QUFZQTtBQVpBLE9BY0EsSUFkQSxDQWNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBMUJBLEVBMkJBLEtBM0JBLENBMkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0EvQkE7QUFpQ0E7QUFDQSxHQTlGQTtBQWdHQSxXQWhHQSxxQkFnR0EsRUFoR0EsRUFnR0EsRUFoR0EsRUFnR0E7QUFBQTs7QUFFQTs7QUFFQSxxRkFDQSxnR0FEQSxLQUdBOztBQUVBLG9EQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0EsSUFIQTs7QUFLQSxvREFDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEE7O0FBS0E7QUFDQTtBQUNBO0FBQ0Esa0JBREE7QUFFQSwwQ0FGQTtBQUdBO0FBSEE7O0FBTUE7QUFDQTtBQUNBLElBVkEsRUFVQSxJQVZBO0FBWUE7QUEvSEEsRUF2QkE7QUE0SkEsUUE1SkEscUJBNEpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBREE7QUFFQSw0QkFGQTtBQUdBLG9CQUhBO0FBSUEsc0JBSkE7QUFLQSxrQkFMQTtBQU1BO0FBTkEsS0FRQSxFQVJBLENBUUEsWUFSQSxFQVFBO0FBQUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBUEEsQ0FPQSxJQVBBLENBT0EsSUFQQSxDQVJBLEVBZ0JBLEVBaEJBLENBZ0JBLFFBaEJBLEVBZ0JBO0FBQUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQU5BLENBTUEsSUFOQSxDQU1BLElBTkEsQ0FoQkE7QUF3QkE7QUF2TEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3R0E7QUFDQSxLQURBLGtCQUNBO0FBQ0E7QUFDQSx3QkFEQTs7QUFHQSxpQkFIQTs7QUFLQTtBQUxBO0FBT0EsRUFUQTs7O0FBV0E7QUFDQSx1QkFEQSxpQ0FDQSxLQURBLEVBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBTEE7QUFPQSxtQkFQQSw2QkFPQSxHQVBBLEVBT0EsRUFQQSxFQU9BO0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFIQSxNQUdBO0FBQ0EsNENBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQSx3QkFEQTtBQUVBLDRCQUZBO0FBR0Esb0NBSEE7QUFJQSxvQ0FKQTtBQUtBLHFFQUxBO0FBTUE7QUFOQSxRQU9BLDBCQVBBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBZkE7QUFnQkE7QUFDQTtBQUNBLEdBOUJBOzs7QUFnQ0E7QUFDQSxlQWpDQSx5QkFpQ0EsS0FqQ0EsRUFpQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsR0F0Q0E7OztBQXdDQTtBQUNBLGVBekNBLHlCQXlDQSxLQXpDQSxFQXlDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxHQTlDQTtBQWdEQSxnQkFoREEsMEJBZ0RBLEtBaERBLEVBZ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQXJEQTtBQXVEQSxTQXZEQSxtQkF1REEsS0F2REEsRUF1REE7QUFDQTtBQUNBO0FBQ0EsR0ExREE7QUE0REEsV0E1REEscUJBNERBLEtBNURBLEVBNERBO0FBQ0E7QUFDQTtBQUNBLEdBL0RBO0FBaUVBLGFBakVBLHVCQWlFQSxJQWpFQSxFQWlFQTtBQUFBOztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBRkEsTUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxJQVZBLE1BVUE7QUFDQTs7QUFFQSx5QkFDQSxnREFEQSxLQUVBLHdCQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFEQSxNQUtBLElBTEEsQ0FLQTtBQUFBLHlCQUNBLGFBREE7QUFBQSxRQUNBLE9BREEsa0JBQ0EsT0FEQTtBQUFBLFFBQ0EsU0FEQSxrQkFDQSxTQURBOzs7QUFHQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSx1QkFEQTtBQUVBLHVCQUZBO0FBR0Esc0JBQ0EsZ0NBREEsQ0FIQTtBQU1BLGlEQU5BO0FBT0E7QUFQQTs7QUFVQTtBQUNBO0FBQ0EsT0FGQTtBQUdBLE1BZEEsRUFjQSxJQWRBO0FBZUE7QUFDQSxJQTdCQTtBQThCQTtBQXJIQSxFQVhBOztBQW1JQSxRQW5JQSxxQkFtSUE7QUFDQTtBQUNBLEVBcklBO0FBdUlBLFFBdklBLHFCQXVJQTtBQUNBO0FBQ0EsdUJBREE7QUFFQTtBQUZBO0FBSUE7QUE1SUEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzBCQTtBQUNBLEtBREEsa0JBQ0E7QUFDQTtBQUNBLHdCQURBOztBQUdBLGlCQUhBOztBQUtBO0FBTEE7QUFPQSxFQVRBOzs7QUFXQTtBQUNBLHVCQURBLGlDQUNBLEtBREEsRUFDQTtBQUNBOztBQUVBO0FBQ0EsR0FMQTtBQU9BLFlBUEEsc0JBT0EsS0FQQSxFQU9BO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBWkE7QUFjQSxnQkFkQSwwQkFjQSxJQWRBLEVBY0E7QUFBQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUZBLE1BRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsSUFWQSxNQVVBO0FBQ0E7O0FBRUEseUJBQ0EsZ0RBREEsS0FFQSx5QkFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBREEsTUFLQSxJQUxBLENBS0E7QUFBQSx5QkFDQSxhQURBO0FBQUEsUUFDQSxPQURBLGtCQUNBLE9BREE7QUFBQSxRQUNBLFFBREEsa0JBQ0EsUUFEQTs7O0FBR0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsdUJBREE7QUFFQSx1QkFGQTtBQUdBLHNCQUNBLGdDQURBLENBSEE7QUFNQSxpREFOQTtBQU9BO0FBUEE7O0FBVUE7QUFDQTtBQUNBLE9BRkE7QUFHQSxNQWRBLEVBY0EsSUFkQTtBQWVBO0FBQ0EsSUE3QkE7QUE4QkE7QUFsRUEsRUFYQTs7QUFnRkEsUUFoRkEscUJBZ0ZBO0FBQ0E7QUFDQSxFQWxGQTtBQW9GQSxRQXBGQSxxQkFvRkE7QUFDQTtBQUNBLHVCQURBO0FBRUE7QUFGQTtBQUlBO0FBekZBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWEE7QUFDQSxLQURBLGtCQUNBO0FBQ0E7QUFDQSx3QkFEQTs7QUFHQSxpQkFIQTs7QUFLQTtBQUxBO0FBT0EsRUFUQTs7O0FBV0E7QUFDQSx1QkFEQSxpQ0FDQSxLQURBLEVBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBTEE7QUFPQSxZQVBBLHNCQU9BLEtBUEEsRUFPQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxHQVpBO0FBY0EsYUFkQSx1QkFjQSxJQWRBLEVBY0E7QUFBQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUZBLE1BRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsSUFWQSxNQVVBO0FBQ0E7O0FBRUEsNEJBQ0Esd0VBREEsS0FFQSxxQkFDQSxnREFEQSxLQUVBLHlCQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFEQSxNQUtBLElBTEEsQ0FLQTtBQUFBLHlCQUNBLGFBREE7QUFBQSxRQUNBLE9BREEsa0JBQ0EsT0FEQTtBQUFBLFFBQ0EsS0FEQSxrQkFDQSxLQURBOzs7QUFHQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSx1QkFEQTtBQUVBLGlEQUZBO0FBR0EsMkJBSEE7O0FBS0E7QUFDQTtBQUFBOztBQUVBO0FBQ0Esd0NBQ0EsMkJBREEsR0FFQSx3QkFDQSxDQURBLEdBQ0EsQ0FIQTtBQUlBLFNBTEE7O0FBT0Esd0JBQ0EsTUFEQSxDQUNBLENBREEsRUFFQSxJQUZBLEdBR0EsTUFIQSxDQUdBO0FBQ0E7QUFDQSxTQUxBLEVBS0EsQ0FMQTs7QUFPQSw0QkFDQSxNQURBLENBQ0EsQ0FEQSxFQUNBLG1CQURBLEVBRUEsSUFGQSxHQUdBLE1BSEEsQ0FHQTtBQUNBO0FBQ0EsU0FMQSxFQUtBLENBTEE7O0FBT0E7QUFDQSx1Q0FDQSx3Q0FEQTtBQUdBO0FBakNBOztBQW9DQTtBQUNBO0FBQ0EsT0FGQTtBQUdBLE1BeENBLEVBd0NBLElBeENBO0FBeUNBO0FBQ0EsSUF2REE7QUF3REE7QUE5RkEsRUFYQTs7QUE0R0EsUUE1R0EscUJBNEdBO0FBQ0E7QUFDQSxFQTlHQTtBQWdIQSxRQWhIQSxxQkFnSEE7QUFDQTtBQUNBLHVCQURBO0FBRUE7QUFGQTtBQUlBO0FBckhBLEc7Ozs7Ozs7QUNwRUEsMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDRCQUE0Qix3QkFBd0IsR0FBRyw0QkFBNEIsdUJBQXVCLEdBQUcsVUFBVSxvRkFBb0YsTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLG1lQUFtZSw2VkFBNlYsMHBCQUEwcEIsMlhBQTJYLDBrQ0FBMGtDLFdBQVcsOHJEQUE4ckQsOEhBQThILHNCQUFzQixxYUFBcWEsOEhBQThILHNCQUFzQiw0MEJBQTQwQixnQkFBZ0IsZ0JBQWdCLHNHQUFzRyxtUUFBbVEsVUFBVSxPQUFPLG1CQUFtQiw4QkFBOEIsNEZBQTRGLDRIQUE0SCxFQUFFLFdBQVcsc0JBQXNCLHFFQUFxRSxrRUFBa0Usa0RBQWtELFdBQVcsb0dBQW9HLGtDQUFrQyx3RkFBd0YsU0FBUyxtQ0FBbUMsa0NBQWtDLHFTQUFxUyxTQUFTLDBCQUEwQixnQ0FBZ0MsdUZBQXVGLG9EQUFvRCxhQUFhLEVBQUUsc0NBQXNDLHVGQUF1RixvREFBb0QsYUFBYSxFQUFFLHFEQUFxRCw0REFBNEQsdUZBQXVGLDhEQUE4RCxhQUFhLDJEQUEyRCxrRUFBa0UseUJBQXlCLE9BQU8sZ0VBQWdFLGdtQkFBZ21CLGdDQUFnQyx5Q0FBeUMsNENBQTRDLGlFQUFpRSwrREFBK0Qsb0lBQW9JLCtEQUErRCwwRUFBMEUsdUJBQXVCLGFBQWEsNkJBQTZCLDZDQUE2QyxvR0FBb0csbUJBQW1CLEVBQUUsaUJBQWlCLFNBQVMsNkJBQTZCLHNIQUFzSCwyTUFBMk0sK0VBQStFLHVGQUF1RixrREFBa0QsZUFBZSxFQUFFLG1HQUFtRyxrREFBa0QsZUFBZSxFQUFFLGtDQUFrQyx3R0FBd0csOEZBQThGLDRIQUE0SCxFQUFFLDJIQUEySCxnSEFBZ0gsYUFBYSxRQUFRLFdBQVcsb0JBQW9CLGtCQUFrQixnQ0FBZ0Msb0VBQW9FLHFRQUFxUSxnREFBZ0Qsd0VBQXdFLDBDQUEwQyw0Q0FBNEMsOENBQThDLCtCQUErQixhQUFhLG1EQUFtRCxzREFBc0Qsd0NBQXdDLDBDQUEwQyw4Q0FBOEMsYUFBYSxhQUFhLGFBQWEsS0FBSyx5Q0FBeUMsMEJBQTBCLEtBQUssYUFBYSx5QkFBeUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEOS9ZO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLGlCQUFpQjtBQUMzQjtBQUNBOztBQUVBLG1CQUFtQixtQkFBTyxDQUFDLENBQWdCOztBQUUzQztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsbUJBQW1CLG1CQUFtQjtBQUN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxtQkFBbUIsc0JBQXNCO0FBQ3pDO0FBQ0E7QUFDQSx1QkFBdUIsMkJBQTJCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCLG1CQUFtQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiwyQkFBMkI7QUFDaEQ7QUFDQTtBQUNBLFlBQVksdUJBQXVCO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxxQkFBcUIsdUJBQXVCO0FBQzVDO0FBQ0E7QUFDQSw4QkFBOEI7QUFDOUI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseURBQXlEO0FBQ3pEOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3ROQSwyQkFBMkIsbUJBQU8sQ0FBQyxDQUEyRDtBQUM5RixjQUFjLFFBQVMsOENBQThDLGlCQUFpQixxQkFBcUIscUJBQXFCLEdBQUcsVUFBVSxnRkFBZ0YsTUFBTSxVQUFVLFdBQVcsV0FBVyw4bEJBQThsQiwwS0FBMEssa0JBQWtCLGtMQUFrTCxzQ0FBc0MsNkJBQTZCLHFJQUFxSSwwQ0FBMEMsNkJBQTZCLGtGQUFrRixZQUFZLHdmQUF3ZixXQUFXLHlCQUF5QixxQkFBcUIsR0FBRyxvQkFBb0IseUJBQXlCLGVBQWUsK0JBQStCLHNCQUFzQix5VUFBeVUsY0FBYyxnQkFBZ0IscUdBQXFHLE9BQU8sbUJBQW1CLHNDQUFzQyxpREFBaUQsNEZBQTRGLFNBQVMsc0NBQXNDLG1DQUFtQywyREFBMkQsZ0RBQWdELGlCQUFpQix5QkFBeUIsZ0dBQWdHLDhEQUE4RCwwYkFBMGIsRUFBRSx3QkFBd0IsRUFBRSxxRUFBcUUsK0ZBQStGLG1HQUFtRyx1R0FBdUcscURBQXFELHFCQUFxQixFQUFFLDJEQUEyRCxpQkFBaUIsYUFBYSx5REFBeUQsc0NBQXNDLGdFQUFnRSwyQkFBMkIsU0FBUyxxREFBcUQsc0NBQXNDLGdFQUFnRSwyQkFBMkIsU0FBUyxpQ0FBaUMsNkNBQTZDLHdDQUF3QyxlQUFlLDJCQUEyQixTQUFTLDBCQUEwQixzQ0FBc0MsNkJBQTZCLFNBQVMsNEJBQTRCLHNDQUFzQyw2QkFBNkIsU0FBUyw4QkFBOEIsOEJBQThCLHNHQUFzRyx5Q0FBeUMsOERBQThELGFBQWEsT0FBTyxnREFBZ0QsYUFBYSxnREFBZ0QsV0FBVyxPQUFPLHFDQUFxQywrRkFBK0YsOEhBQThILGlHQUFpRyxXQUFXLHdEQUF3RCw2QkFBNkIsc0VBQXNFLGlCQUFpQixvQ0FBb0MsdUJBQXVCLHFCQUFxQixpQkFBaUIsaUNBQWlDLGlFQUFpRSx5REFBeUQsd0NBQXdDLHdEQUF3RCxrTEFBa0wscUNBQXFDLDRMQUE0TCxFQUFFLDJDQUEyQyxpR0FBaUcsRUFBRSxxQkFBcUIsUUFBUSxtQkFBbUIsaUJBQWlCLEVBQUUsU0FBUyxPQUFPLG9CQUFvQixrQ0FBa0MsT0FBTyxvQkFBb0IscURBQXFELDJGQUEyRixFQUFFLE9BQU8sS0FBSywyREFBMkQsbUJBQW1CLHVCQUF1Qix1QkFBdUIsS0FBSyxhQUFhLEc7Ozs7Ozs7QUNEdHVPLDJCQUEyQixtQkFBTyxDQUFDLENBQTJEO0FBQzlGLGNBQWMsUUFBUyw4Q0FBOEMsaUJBQWlCLHFCQUFxQixxQkFBcUIsR0FBRyxVQUFVLDJFQUEyRSxNQUFNLFVBQVUsV0FBVyxXQUFXLGdsQkFBZ2xCLHlMQUF5TCx5Q0FBeUMscUlBQXFJLHNDQUFzQyw2QkFBNkIsc0lBQXNJLDBDQUEwQyw2QkFBNkIsbUZBQW1GLFlBQVksMnRCQUEydEIsc0NBQXNDLG9FQUFvRSwrQkFBK0IsNEJBQTRCLCtDQUErQywrQ0FBK0MsdURBQXVELDZDQUE2QyxlQUFlLDRCQUE0QixvQkFBb0IsNEJBQTRCLHVCQUF1QiwyUUFBMlEsY0FBYyxnQkFBZ0IseUZBQXlGLE9BQU8sbUJBQW1CLHNDQUFzQyxpREFBaUQsNEZBQTRGLFNBQVMsOEJBQThCLHNDQUFzQyxnRUFBZ0UsMkJBQTJCLFNBQVMsOEJBQThCLDhCQUE4QixpR0FBaUcseUNBQXlDLDhEQUE4RCxhQUFhLE9BQU8sNEVBQTRFLGFBQWEsZ0RBQWdELFdBQVcsT0FBTyxxQ0FBcUMseUhBQXlILGtHQUFrRywrSEFBK0gsNkdBQTZHLFdBQVcsc0RBQXNELDZCQUE2QixzRUFBc0UsaUJBQWlCLG9DQUFvQyx1QkFBdUIsaUJBQWlCLGlCQUFpQixpQ0FBaUMsaUVBQWlFLHlDQUF5Qyx3Q0FBd0Msd0RBQXdELGdSQUFnUixxREFBcUQsc0VBQXNFLCtOQUErTiw0QkFBNEIsK0xBQStMLCtEQUErRCwrQkFBK0IsTUFBTSx1R0FBdUcsaUJBQWlCLCtGQUErRiwrREFBK0QsK0JBQStCLE1BQU0saUxBQWlMLHFDQUFxQyx1QkFBdUIsMkJBQTJCLEVBQUUsMkNBQTJDLGlHQUFpRyxFQUFFLHFCQUFxQixRQUFRLG1CQUFtQixpQkFBaUIsRUFBRSxTQUFTLE9BQU8sb0JBQW9CLGtDQUFrQyxPQUFPLG9CQUFvQixpREFBaUQsMkZBQTJGLEVBQUUsT0FBTyxLQUFLLDJEQUEyRCxtQkFBbUIsdUJBQXVCLHVCQUF1QixLQUFLLGFBQWEsRzs7Ozs7OztBQ0Q1N04sMkJBQTJCLG1CQUFPLENBQUMsQ0FBMkQ7QUFDOUYsY0FBYyxRQUFTLDhDQUE4QyxpQkFBaUIscUJBQXFCLHFCQUFxQixHQUFHLFVBQVUsOEVBQThFLE1BQU0sVUFBVSxXQUFXLFdBQVcsZ21CQUFnbUIsdUxBQXVMLHNDQUFzQyw2QkFBNkIseUlBQXlJLDBDQUEwQyw2QkFBNkIsbUZBQW1GLFlBQVksMmxCQUEybEIsd0JBQXdCLDhEQUE4RCwwQkFBMEIsdS9CQUF1L0IsbUJBQW1CLDhCQUE4QixvQkFBb0Isd1FBQXdRLGtCQUFrQiwyREFBMkQsa0JBQWtCLDhDQUE4QyxnQkFBZ0IsdUJBQXVCLGtCQUFrQix1QkFBdUIsZUFBZSxna0JBQWdrQixjQUFjLGdCQUFnQiwrRkFBK0YsT0FBTyxtQkFBbUIsc0NBQXNDLGlEQUFpRCw0RkFBNEYsU0FBUyw4QkFBOEIsc0NBQXNDLGdFQUFnRSwyQkFBMkIsU0FBUyxpQ0FBaUMsOEJBQThCLHVHQUF1Ryx5Q0FBeUMsOERBQThELGFBQWEsT0FBTyxnREFBZ0QsYUFBYSxnREFBZ0QsV0FBVyxPQUFPLHFDQUFxQywrRkFBK0YsK0hBQStILGtHQUFrRyxXQUFXLHdEQUF3RCw2QkFBNkIsc0VBQXNFLGlCQUFpQixvQ0FBb0MsdUJBQXVCLG9CQUFvQixpQkFBaUIsaUNBQWlDLG9FQUFvRSxrREFBa0Qsd0NBQXdDLDJEQUEyRCxrTEFBa0wscUNBQXFDLDRMQUE0TCxFQUFFLDJDQUEyQyxpR0FBaUcsRUFBRSxxQkFBcUIsUUFBUSxtQkFBbUIsaUJBQWlCLEVBQUUsU0FBUyxPQUFPLG9CQUFvQixxQ0FBcUMsT0FBTyxvQkFBb0IscURBQXFELDJGQUEyRixFQUFFLE9BQU8sS0FBSywyREFBMkQsbUJBQW1CLHVCQUF1Qix1QkFBdUIsS0FBSyxhQUFhLEc7Ozs7Ozs7O0FDQTU4TjtBQUNBLG1CQUFPLENBQUMsR0FBd1I7O0FBRWhTLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQWtQO0FBQzVQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQThNO0FBQ3hOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7QUM5QkE7QUFDQSxtQkFBTyxDQUFDLEdBQW9SOztBQUU1UixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUE4TztBQUN4UDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUEwTTtBQUNwTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7O0FDOUJBO0FBQ0EsbUJBQU8sQ0FBQyxHQUFrUjs7QUFFMVIsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBNE87QUFDdFA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBd007QUFDbE47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7OztBQzlCQTtBQUNBLG1CQUFPLENBQUMsR0FBK1E7O0FBRXZSLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXlPO0FBQ25QO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXFNO0FBQy9NO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQy9CQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRyw4QkFBOEI7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1V0E7QUFDQTtBQUNBLGdDQURBO0FBRUE7QUFGQTtBQURBLEc7Ozs7Ozs7QUN2QkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxzQkFBc0I7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUMsYUFBYSxhQUFhLDBCQUEwQjtBQUNyRDtBQUNBLENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pIQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQSxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM1SEEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUMsYUFBYSxhQUFhLDBCQUEwQjtBQUNyRDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUNoTEE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBcVQ7QUFDM1UsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7O0FDcEJBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEdBQWlUO0FBQ3ZVLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxHQUE0UztBQUNsVSw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLENBQXlFO0FBQzlGO0FBQ0EsR0FBRyxLQUFVO0FBQ2I7QUFDQTtBQUNBLDRKQUE0SixvRUFBb0U7QUFDaE8scUtBQXFLLG9FQUFvRTtBQUN6TztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7Ozs7QUNwQkE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsR0FBK1M7QUFDclUsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUF5RTtBQUM5RjtBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSw0SkFBNEosb0VBQW9FO0FBQ2hPLHFLQUFxSyxvRUFBb0U7QUFDek87QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7Ozs7Ozs7Ozs7QUNwQkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBeU87QUFDblA7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBcU07QUFDL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixpQkFBaUI7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLHdCQUF3QjtBQUMzRCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJqcy92aXNhL2Rhc2hib2FyZC9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDc4KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmMTEzZWQzNmE1YTU2YjdkY2Y2MyIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDE4IDE5IDIwIDIxIDIyIDIzIDI0IDI1IiwiVnVlLmNvbXBvbmVudCgnZGlhbG9nLW1vZGFsJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlJykpO1xuXG5sZXQgZGF0YSA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XG5cbnZhciBkYXNoYm9hcmQgPSBuZXcgVnVlKHtcblx0ZWw6JyNkYXNoYm9hcmQnLFxuXHRkYXRhOntcblx0XHRwZW5kaW5nU2VydmljZXM6W10sXG5cdFx0b25wcm9jZXNzU2VydmljZXM6W10sXG5cdFx0dG9kYXlzUmVwb3J0czpbXSxcbiAgICAgICAgbGFzdFJlcG9ydGVkRGF0ZTpbXSxcblx0XHR0b2RheXNTZXJ2aWNlczpbXSxcbiAgICAgICAgZGVsaXZlcnlTY2hlZHVsZXM6W10sXG5cdFx0c2VydmljZTonJyxcbiAgICAgICAgc2V0dGluZzogZGF0YS5hY2Nlc3NfY29udHJvbFswXS5zZXR0aW5nLFxuICAgICAgICBwZXJtaXNzaW9uczogZGF0YS5wZXJtaXNzaW9ucyxcbiAgICAgICAgZm9ybTpuZXcgRm9ybSh7XG4gICAgICAgICAgaWQ6ICcnLFxuICAgICAgICAgIHJlYXNvbjogJycsXG4gICAgICAgIH0sIHsgYmFzZVVSTDogJ2h0dHA6Ly8nK3dpbmRvdy5MYXJhdmVsLmNwYW5lbF91cmwgfSksXG4gICAgICAgIGFkZFNjaGVkdWxlOm5ldyBGb3JtKHtcbiAgICAgICAgICBpZDonJyxcbiAgICAgICAgICBpdGVtOicnLFxuICAgICAgICAgIHJpZGVyX2lkOicnLFxuICAgICAgICAgIGxvY2F0aW9uOicnLFxuICAgICAgICAgIHNjaGVkdWxlZF9kYXRlOicnLFxuICAgICAgICAgIHNjaGVkdWxlZF90aW1lOicnLFxuICAgICAgICB9LCB7IGJhc2VVUkw6ICcvdmlzYS9ob21lLyd9KSxcbiAgICAgICAgc2NoZWR1bGVkX2hvdXI6JycsXG4gICAgICAgIHNjaGVkdWxlZF9taW51dGU6JycsICAgXG4gICAgICAgIGNvbXBhbnlDb3VyaWVyczpbXSxcbiAgICAgICAgZHQ6JycsXG4gICAgICAgIHNlbElkOicnLFxuICAgICAgICBtb2RlOicnXG5cdH0sXG5cdG1ldGhvZHM6IHtcbiAgICAgICAgY2FsbERhdGFUYWJsZXMxKCl7XG4gICAgICAgIFx0JCgnI3BlbmRpbmdMaXN0cycpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHQgICAgIFx0YXhpb3MuZ2V0KCcvdmlzYS9ob21lL3BlbmRpbmdTZXJ2aWNlcycpXG5cdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICAgXHR0aGlzLnBlbmRpbmdTZXJ2aWNlcyA9IHJlc3VsdC5kYXRhLmZpbHRlcihyID0+IHIuYWN0aXZlID09IDEpO1xuXG5cdCAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblx0ICAgICAgICAgICAgICAgICQoJyNwZW5kaW5nTGlzdHMnKS5EYXRhVGFibGUoe1xuXHQgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdCAgICAgICAgICAgICAgICAgICAgb3JkZXI6IFtdLFxuXHQgICAgICAgICAgICAgICAgICAgIFwibGVuZ3RoTWVudVwiOiBbWzEwLCAyNSwgNTBdLCBbMTAsIDI1LCA1MF1dLFxuXHQgICAgICAgICAgICAgICAgICAgIFwiaURpc3BsYXlMZW5ndGhcIjogMTBcblx0ICAgICAgICAgICAgICAgIH0pO1xuXHRcdFx0XHQgICAgJChmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdCAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKClcblx0XHRcdFx0ICAgIH0pXG5cdCAgICAgICAgICAgIH0sMjAwMCk7IFxuXG5cdCAgICBcdH0pO1xuICAgICAgICB9LFxuICAgICAgICBjYWxsRGF0YVRhYmxlczIoKXtcbiAgICAgICAgXHQkKCcjb25wcm9jZXNzTGlzdHMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cblx0ICAgICBcdGF4aW9zLmdldCgnL3Zpc2EvaG9tZS9vbnByb2Nlc3NTZXJ2aWNlcycpXG5cdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICBcdHRoaXMub25wcm9jZXNzU2VydmljZXMgPSByZXN1bHQuZGF0YTtcblxuXHQgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5cdCAgICAgICAgICAgICAgICAkKCcjb25wcm9jZXNzTGlzdHMnKS5EYXRhVGFibGUoe1xuXHQgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdCAgICAgICAgICAgICAgICAgICAgXCJsZW5ndGhNZW51XCI6IFtbMTAsIDI1LCA1MF0sIFsxMCwgMjUsIDUwXV0sXG5cdCAgICAgICAgICAgICAgICAgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAxMFxuXHQgICAgICAgICAgICAgICAgfSk7XG5cdFx0XHRcdCAgICAkKGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0ICAgICAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoKVxuXHRcdFx0XHQgICAgfSlcblx0ICAgICAgICAgICAgfSwyMDAwKTtcdCAgICAgICAgXHRcblx0ICAgIFx0fSk7XG4gICAgICAgIH0sXG4gICAgICAgIGNhbGxEYXRhVGFibGVzMygpe1xuICAgICAgICBcdCQoJyN0b2RheXNSZXBvcnRMaXN0cycpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHQgICAgICAgIGF4aW9zLmdldCgnL3Zpc2EvcmVwb3J0L2dldC1yZXBvcnRzLycpXG5cdFx0ICAgIC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHQgICAgICAgIHRoaXMudG9kYXlzUmVwb3J0cyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVEYXRhdGFibGVUb2dnbGUodGhpcy50b2RheXNSZXBvcnRzLCcjdG9kYXlzUmVwb3J0TGlzdHMnKTtcbiAgICAgICAgICAgICAgICAvLyB0aGlzLmNyZWF0ZURhdGF0YWJsZVRvZ2dsZSh0aGlzLnRvZGF5c1JlcG9ydHMsJyN0b2RheXNSZXBvcnRMaXN0cycpO1xuXHRcdCAgICAgICAgXHQgICAgICAgIFxuXHRcdCAgICB9KTtcbiAgICAgICAgfSxcbiAgICAgICAgY2FsbERhdGFUYWJsZXM0KCl7XG4gICAgICAgIFx0JCgnI2xhc3RSZXBvcnRlZERhdGVMaXN0cycpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcblxuXHQgICAgICAgIGF4aW9zLmdldCgnL3Zpc2EvcmVwb3J0L2dldC1yZXBvcnRzLXllc3RlcmRheS8nKVxuXHRcdCAgICAudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0ICAgICAgICB0aGlzLmxhc3RSZXBvcnRlZERhdGUgPSByZXNwb25zZS5kYXRhO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVEYXRhdGFibGVUb2dnbGUodGhpcy5sYXN0UmVwb3J0ZWREYXRlLCcjbGFzdFJlcG9ydGVkRGF0ZUxpc3RzJyk7XG4gICAgICAgICAgICAgICAgLy8gdGhpcy5jcmVhdGVEYXRhdGFibGVUb2dnbGUodGhpcy5sYXN0UmVwb3J0ZWREYXRlLCcjbGFzdFJlcG9ydGVkRGF0ZUxpc3RzJyk7XG4gICAgXG5cdFx0ICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNhbGxEYXRhVGFibGVzNSgpe1xuICAgICAgICAgICAgJCgnI3RvZGF5c1NlcnZpY2VzTGlzdHMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG4gICAgICAgICAgICBwYXJhbXMgPSB7XG4gICAgICAgICAgICAgICAgZGF0ZTogMFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGF4aW9zLmdldCgnL3Zpc2EvaG9tZS9nZXQtc2VydmljZS1ieURhdGUnLCB7XG4gICAgICAgICAgICAgICAgcGFyYW1zOiBwYXJhbXNcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy50b2RheXNTZXJ2aWNlcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAkKCcjdG9kYXlzU2VydmljZXNMaXN0cycpLkRhdGFUYWJsZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJsZW5ndGhNZW51XCI6IFtbMTAsIDI1LCA1MF0sIFsxMCwgMjUsIDUwXV0sXG4gICAgICAgICAgICAgICAgICAgICAgICBcImlEaXNwbGF5TGVuZ3RoXCI6IDEwXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAkKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykudG9vbHRpcCgpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSwyMDAwKTtcbiAgICAgICAgICAgICAgICAvLyB0aGlzLmNyZWF0ZURhdGF0YWJsZVRvZ2dsZSh0aGlzLnRvZGF5c1NlcnZpY2VzLCcjdG9kYXlzU2VydmljZXNMaXN0cycpO1xuICAgIFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2FsbERhdGFUYWJsZXM2KCl7XG4gICAgICAgICAgICAkKCcjZGVsaXZlcnlTY2hlZHVsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcbiAgICAgICAgICAgIHBhcmFtcyA9IHtcbiAgICAgICAgICAgICAgICBkYXRlOiAwXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgYXhpb3MuZ2V0KCcvdmlzYS9ob21lL2dldFNjaGVkdWxlc2J5RGF0ZScsIHtcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHBhcmFtc1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJ5U2NoZWR1bGVzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICQoJyNkZWxpdmVyeVNjaGVkdWxlJykuRGF0YVRhYmxlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwXSwgWzEwLCAyNSwgNTBdXSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiaURpc3BsYXlMZW5ndGhcIjogMTBcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKClcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9LDIwMDApO1xuICAgICAgICAgICAgICAgIC8vIHRoaXMuY3JlYXRlRGF0YXRhYmxlVG9nZ2xlKHRoaXMudG9kYXlzU2VydmljZXMsJyN0b2RheXNTZXJ2aWNlc0xpc3RzJyk7XG4gICAgXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBpZlRvZGF5KGRhdGUpIHtcbiAgICAgICAgICAgIGxldCB0b2RheSA9IG1vbWVudCgpLmZvcm1hdCgnWVlZWS1NTS1ERCcpO1xuICAgICAgICAgICAgaWYodG9kYXkgPT0gZGF0ZSl7XG4gICAgICAgICAgICAgICAgcmV0dXJuICdEdWUgVG9kYXknO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gcmV0dXJuIG1vbWVudCh0aGlzLnJlbWluZGVyLmV4dGVuZCkuZnJvbU5vdygpO1xuICAgICAgICAgICAgcmV0dXJuICdEdWUgb24gJytkYXRlO1xuICAgICAgICBcbiAgICAgICAgfSxcblxuICAgIC8vICAgICBjcmVhdGVEYXRhdGFibGVUb2dnbGUocmVwb3J0cyx0YWJsZW5hbWUpIHtcblxuICAgIC8vICAgICAgICAgJCh0YWJsZW5hbWUpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcbiAgICAvLyAgICAgICAgIC8vICQodGFibGVuYW1lKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XG5cbiAgICAvLyAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cbiAgICAvLyAgICAgICAgICAgICB2YXIgZm9ybWF0ID0gZnVuY3Rpb24oZCkge1xuICAgIC8vICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0YWJsZW5hbWUpO1xuICAgIC8vICAgICAgICAgICAgICAgICB2YXIgaWRzID0gJyc7XG4gICAgLy8gICAgICAgICAgICAgICAgIHZhciBjbGlkcyA9IFtdO1xuICAgIC8vICAgICAgICAgICAgICAgICBmb3IodmFyIGk9MDsgaTwoZC5jbGllbnQpLmxlbmd0aDsgaSsrKSB7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICB2YXIgZm91bmQgPSBqUXVlcnkuaW5BcnJheShkLmNsaWVudFtpXS5jbGllbnRfaWQsIGNsaWRzKTtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhmb3VuZCk7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICBpZiAoZm91bmQgPCAwKSB7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgLy8gRWxlbWVudCB3YXMgbm90IGZvdW5kLCBhZGQgaXQuXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgY2xpZHMucHVzaChkLmNsaWVudFtpXS5jbGllbnRfaWQpO1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIGlkcyArPSAnPGEgaHJlZj1cIi92aXNhL2NsaWVudC8nK2QuY2xpZW50W2ldLmNsaWVudF9pZCsnXCIgdGFyZ2V0PVwiX2JsYW5rXCIgZGF0YS10b2dnbGU9XCJwb3BvdmVyXCIgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIiBkYXRhLWNvbnRhaW5lcj1cImJvZHlcIiBkYXRhLWNvbnRlbnQ9XCInK2QuY2xpZW50W2ldLmZ1bGxuYW1lICsnXCIgZGF0YS10cmlnZ2VyPVwiaG92ZXJcIj4nK2QuY2xpZW50W2ldLmNsaWVudF9pZCArJzwvYT4nK1wiLFwiO1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgfVxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coY2xpZHMpO1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgLy8gaWYoaSAhPSAoKGQuY2xpZW50KS5sZW5ndGggLSAxKSApe1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgLy8gICAgIGlkcyA9IGlkcytcIixcIjtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIC8vIH1cblxuICAgIC8vICAgICAgICAgICAgICAgICB9XG4gICAgLy8gICAgICAgICAgICAgICAgIC8vIGlkcyA9IHJ0cmltKGlkcywnLCcpO1xuICAgIC8vICAgICAgICAgICAgICAgICByZXR1cm4gJzx0YWJsZSBjZWxscGFkZGluZz1cIjVcIiBjZWxsc3BhY2luZz1cIjBcIiBjbGFzcz1cInRhYmxlIHRhYmxlLWJvcmRlcmVkIHRhYmxlLXN0cmlwZWQgdGFibGUtaG92ZXJcIiBib3JkZXI9XCIxXCIgPicrXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dHIgc3R5bGU9XCJwYWRkaW5nLWxlZnQ6NTBweDtcIj4nK1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0ZCB3aWR0aD1cIjIwMFwiPjxiPlByb2Nlc3NvciA6IDwvYj4nK2QucHJvY2Vzc29yKyc8L3RkPicrXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPHRkPjxiPkNsaWVudCBJRHMgOiA8L2I+ICcraWRzKyc8L3RkPicrXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8L3RyPicrXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgJzwvdGFibGU+JztcbiAgICAvLyAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIFxuICAgIC8vICAgICAgICAgICAgIHRhYmxlMiA9ICQodGFibGVuYW1lKS5EYXRhVGFibGUoIHtcbiAgICAvLyAgICAgICAgICAgICAgICAgXCJkYXRhXCI6IHJlcG9ydHMsXG4gICAgLy8gICAgICAgICAgICAgICAgIFwiY29sdW1uc1wiOiBbXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICB7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiBcIjUlXCIsXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgXCJjbGFzc05hbWVcIjogICAgICAnc2VydmljZS1jb250cm9sIHRleHQtY2VudGVyJyxcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICBcIm9yZGVyYWJsZVwiOiAgICAgIGZhbHNlLFxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YVwiOiAgICAgICAgICAgbnVsbCxcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICBcImRlZmF1bHRDb250ZW50XCI6ICc8aSBjbGFzcz1cImZhIGZhLWFycm93LXJpZ2h0XCIgc3R5bGU9XCJjdXJzb3I6cG9pbnRlcjtcIj48L2k+J1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIHsgXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiBcIjIwJVwiLFxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24ocmVwb3J0cywgdHlwZSwgcm93KSB7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHJlcG9ydHMudHlwZSA9PSBcInJlcG9ydFwiKXtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnPGIgc3R5bGU9XCJjb2xvcjogIzFhYjM5NDtcIj4nICsgcmVwb3J0cy5zZXJ2aWNlICsgJzwvYj4nXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICc8YiBzdHlsZT1cImNvbG9yOiBibGFjaztcIj4nICsgcmVwb3J0cy5zZXJ2aWNlICsgJzwvYj4nXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICBcIndpZHRoXCI6IFwiMTUlXCIsXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihyZXBvcnRzLCB0eXBlLCByb3cpIHtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHNpZCA9ICcnO1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGk9MDsgaTwocmVwb3J0cy5jbGllbnQpLmxlbmd0aDsgaSsrKSB7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaWQgKz0gcmVwb3J0cy5jbGllbnRbaV0uY2xpZW50X2lkK1wiLFwiO1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnPGI+JytyZXBvcnRzLmxvZ19kYXRlKyAnPC9iPjxzcGFuIHN0eWxlPVwiZGlzcGxheTpub25lO1wiPicrc2lkKyc8L3NwYW4+J1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICB7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiBcIjQwJVwiLFxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24ocmVwb3J0cywgdHlwZSwgcm93LCBtZXRhKSB7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnPGI+JytyZXBvcnRzLmRldGFpbCsgJzwvYj4nXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAvLyAgICAgICAgICAgICAgICAgXVxuICAgIC8vICAgICAgICAgICAgIH0pO1xuXG5cdFx0XHRcdC8vICQoJ2JvZHknKS5vZmYoJ2NsaWNrJywgJ3RhYmxlJyt0YWJsZW5hbWUrJyB0Ym9keSB0ZC5zZXJ2aWNlLWNvbnRyb2wnKTtcbiBcdFx0XHQvLyBcdC8vICQodGFibGVuYW1lKycgdGJvZHknKS5vbignY2xpY2snLCAndGQuc2VydmljZS1jb250cm9sJywgZnVuY3Rpb24gKGUpIHtcbiAgICAvLyAgICAgICAgICAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJ3RhYmxlJyt0YWJsZW5hbWUrJyB0Ym9keSB0ZC5zZXJ2aWNlLWNvbnRyb2wnLCBmdW5jdGlvbiAoZSkge1xuICAgIC8vICAgICAgICAgICAgICAgICB2YXIgdHIyID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpO1xuICAgIC8vICAgICAgICAgICAgICAgICB2YXIgcm93ID0gdGFibGUyLnJvdyggdHIyICk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgLy8gICAgICAgICAgICAgICAgIGlmKHJvdy5jaGlsZC5pc1Nob3duKCkpIHtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIHJvdy5jaGlsZC5oaWRlKCk7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAvL3RyMi5yZW1vdmVDbGFzcygnc2hvd24nKTtcbiAgICAvLyAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIHJvdy5jaGlsZCggZm9ybWF0KHJvdy5kYXRhKCkpICkuc2hvdygpO1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgLy90cjIuYWRkQ2xhc3MoJ3Nob3duJyk7XG4gICAgLy8gICAgICAgICAgICAgICAgIH1cbiAgICAvLyAgICAgICAgICAgICB9KTtcblxuICAgIC8vICAgICAgICAgfS5iaW5kKHRoaXMpLCAxMDAwKTtcblxuICAgIC8vICAgICB9LFxuXG4gICAgICAgIGNyZWF0ZURhdGF0YWJsZVRvZ2dsZShyZXBvcnRzLHRhYmxlbmFtZSkge1xuXG4gICAgICAgICAgICAkKHRhYmxlbmFtZSkuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgdmFyIGZvcm1hdCA9IGZ1bmN0aW9uKGQpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coZCk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBpZHMgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgdmFyIGNsaWRzID0gW107XG4gICAgICAgICAgICAgICAgICAgIGZvcih2YXIgaT0wOyBpPChkLmNsaWVudCkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBmb3VuZCA9IGpRdWVyeS5pbkFycmF5KGQuY2xpZW50W2ldLmNsaWVudF9pZCwgY2xpZHMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGZvdW5kKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmb3VuZCA8IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBFbGVtZW50IHdhcyBub3QgZm91bmQsIGFkZCBpdC5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGlkcy5wdXNoKGQuY2xpZW50W2ldLmNsaWVudF9pZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWRzICs9ICc8YSBocmVmPVwiL3Zpc2EvY2xpZW50LycrZC5jbGllbnRbaV0uY2xpZW50X2lkKydcIiB0YXJnZXQ9XCJfYmxhbmtcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInRvcFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIGRhdGEtY29udGVudD1cIicrZC5jbGllbnRbaV0uZnVsbG5hbWUgKydcIiBkYXRhLXRyaWdnZXI9XCJob3ZlclwiPicrZC5jbGllbnRbaV0uY2xpZW50X2lkICsnPC9hPicrXCIsXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhjbGlkcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZihpICE9ICgoZC5jbGllbnQpLmxlbmd0aCAtIDEpICl7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgaWRzID0gaWRzK1wiLFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gfVxuXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLy8gaWRzID0gcnRyaW0oaWRzLCcsJyk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnPHRhYmxlIGNlbGxwYWRkaW5nPVwiNVwiIGNlbGxzcGFjaW5nPVwiMFwiIGNsYXNzPVwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ob3ZlciBkdGNoaWxkcm93XCIgPicrXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dHIgc3R5bGU9XCJwYWRkaW5nLWxlZnQ6NTBweDtcIj4nK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0ZD48Yj5SZXBvcnQgOiA8L2I+JytkLmRldGFpbCsnPC90ZD4nK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gJzx0ZD48Yj5DbGllbnQgSURzIDogPC9iPiAnK2lkcysnPC90ZD4nK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC90cj4nK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8L3RhYmxlPic7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0YWJsZTIgPSAkKHRhYmxlbmFtZSkuRGF0YVRhYmxlKCB7XG4gICAgICAgICAgICAgICAgICAgIFwiZGF0YVwiOiByZXBvcnRzLFxuICAgICAgICAgICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBcImNvbHVtbnNcIjogW1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8ge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIFwid2lkdGhcIjogXCI1JVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIFwiY2xhc3NOYW1lXCI6ICAgICAgJ3NlcnZpY2UtY29udHJvbCB0ZXh0LWNlbnRlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgXCJvcmRlcmFibGVcIjogICAgICBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICBcImRhdGFcIjogICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgXCJkZWZhdWx0Q29udGVudFwiOiAnPGkgY2xhc3M9XCJmYSBmYS1hcnJvdy1yaWdodFwiIHN0eWxlPVwiY3Vyc29yOnBvaW50ZXI7XCI+PC9pPidcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwid2lkdGhcIjogXCI0MCVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdzZXJ2aWNlLWNvbnRyb2wnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24ocmVwb3J0cywgdHlwZSwgcm93KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHJlcG9ydHMudHlwZSA9PSBcInJlcG9ydFwiKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnPGIgc3R5bGU9XCJjb2xvcjogIzFhYjM5NDtcIj4nICsgcmVwb3J0cy5zZXJ2aWNlICsgJzwvYj4nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICc8YiBzdHlsZT1cImNvbG9yOiBibGFjaztcIj4nICsgcmVwb3J0cy5zZXJ2aWNlICsgJzwvYj4nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdzZXJ2aWNlLWNvbnRyb2wnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwid2lkdGhcIjogXCIxMyVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKHJlcG9ydHMsIHR5cGUsIHJvdykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgc2lkID0gJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcih2YXIgaT0wOyBpPChyZXBvcnRzLmNsaWVudCkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNpZCArPSByZXBvcnRzLmNsaWVudFtpXS5jbGllbnRfaWQrXCIsXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICc8Yj4nK3JlcG9ydHMubG9nX2RhdGUrICc8L2I+PHNwYW4gc3R5bGU9XCJkaXNwbGF5Om5vbmU7XCI+JytzaWQrJzwvc3Bhbj4nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNsYXNzTmFtZVwiOiAgICAgICdzZXJ2aWNlLWNvbnRyb2wnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwid2lkdGhcIjogXCIxMiVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKHJlcG9ydHMsIHR5cGUsIHJvdywgbWV0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJzxiPicrcmVwb3J0cy5wcm9jZXNzb3IrICc8L2I+J1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiBcIjM1JVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiY2xhc3NOYW1lXCI6ICAgICAgJ3NlcnZpY2UtY29udHJvbCB3cmFwb2snLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIFwiY2xhc3NcIjogXCJ3cmFwbm90XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihyZXBvcnRzLCB0eXBlLCByb3csIG1ldGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGlkcyA9ICcnO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY2xpZHMgPSBbXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yKHZhciBpPTA7IGk8KHJlcG9ydHMuY2xpZW50KS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGZvdW5kID0galF1ZXJ5LmluQXJyYXkocmVwb3J0cy5jbGllbnRbaV0uY2xpZW50X2lkLCBjbGlkcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coZm91bmQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZvdW5kIDwgMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIEVsZW1lbnQgd2FzIG5vdCBmb3VuZCwgYWRkIGl0LlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWRzLnB1c2gocmVwb3J0cy5jbGllbnRbaV0uY2xpZW50X2lkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZHMgKz0gJzxhIGhyZWY9XCIvdmlzYS9jbGllbnQvJytyZXBvcnRzLmNsaWVudFtpXS5jbGllbnRfaWQrJ1wiIHRhcmdldD1cIl9ibGFua1wiIGRhdGEtdG9nZ2xlPVwicG9wb3ZlclwiIGRhdGEtcGxhY2VtZW50PVwidG9wXCIgZGF0YS1jb250YWluZXI9XCJib2R5XCIgZGF0YS1jb250ZW50PVwiJytyZXBvcnRzLmNsaWVudFtpXS5jbGllbnRfaWQgKydcIiBkYXRhLXRyaWdnZXI9XCJob3ZlclwiPicrcmVwb3J0cy5jbGllbnRbaV0uZnVsbG5hbWUgKyc8L2E+JytcIiAsIFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coY2xpZHMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYoaSAhPSAoKGQuY2xpZW50KS5sZW5ndGggLSAxKSApe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIGlkcyA9IGlkcytcIixcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBpZHM7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICQoJ2JvZHknKS5vZmYoJ2NsaWNrJywgJ3RhYmxlJyt0YWJsZW5hbWUrJyB0Ym9keSB0ZC5zZXJ2aWNlLWNvbnRyb2wnKTtcbiAgICAgICAgICAgICAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJ3RhYmxlJyt0YWJsZW5hbWUrJyB0Ym9keSB0ZC5zZXJ2aWNlLWNvbnRyb2wnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgdHIyID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgcm93ID0gdGFibGUyLnJvdyggdHIyICk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIGlmKHJvdy5jaGlsZC5pc1Nob3duKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdy5jaGlsZC5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0cjIucmVtb3ZlQ2xhc3MoJ3Nob3duJyk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByb3cuY2hpbGQoIGZvcm1hdChyb3cuZGF0YSgpKSApLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyMi5hZGRDbGFzcygnc2hvd24nKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIC8vRVhQQU5EIEFMTCBDSElMRCBST1dTIEJZIERFRkFVTFRcbiAgICAgICAgICAgICAgICB0YWJsZTIucm93cygpLmV2ZXJ5KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgIC8vIElmIHJvdyBoYXMgZGV0YWlscyBjb2xsYXBzZWRcbiAgICAgICAgICAgICAgICAgICAgaWYoIXRoaXMuY2hpbGQuaXNTaG93bigpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIE9wZW4gdGhpcyByb3dcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGQoZm9ybWF0KHRoaXMuZGF0YSgpKSkuc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzLm5vZGUoKSkuYWRkQ2xhc3MoJ3Nob3duJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcblxuXG4gICAgICAgICAgICAgICAgLy8gSGFuZGxlIGNsaWNrIG9uIFwiRXhwYW5kIEFsbFwiIGJ1dHRvblxuICAgICAgICAgICAgICAgICQoJyNidG4tc2hvdy1hbGwtY2hpbGRyZW4nKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAvLyBFbnVtZXJhdGUgYWxsIHJvd3NcbiAgICAgICAgICAgICAgICAgICAgdGFibGUyLnJvd3MoKS5ldmVyeShmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gSWYgcm93IGhhcyBkZXRhaWxzIGNvbGxhcHNlZFxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoIXRoaXMuY2hpbGQuaXNTaG93bigpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBPcGVuIHRoaXMgcm93XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGlsZChmb3JtYXQodGhpcy5kYXRhKCkpKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzLm5vZGUoKSkuYWRkQ2xhc3MoJ3Nob3duJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgLy8gSGFuZGxlIGNsaWNrIG9uIFwiQ29sbGFwc2UgQWxsXCIgYnV0dG9uXG4gICAgICAgICAgICAgICAgJCgnI2J0bi1oaWRlLWFsbC1jaGlsZHJlbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgIC8vIEVudW1lcmF0ZSBhbGwgcm93c1xuICAgICAgICAgICAgICAgICAgICB0YWJsZTIucm93cygpLmV2ZXJ5KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBJZiByb3cgaGFzIGRldGFpbHMgZXhwYW5kZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuY2hpbGQuaXNTaG93bigpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBDb2xsYXBzZSByb3cgZGV0YWlsc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGQuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcy5ub2RlKCkpLnJlbW92ZUNsYXNzKCdzaG93bicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAyMDAwKTtcblxuICAgICAgICB9LFxuXG5cbiAgICAgICAgb3BlblByb2Nlc3NTZXJ2aWNlKHNlcnZpY2Upe1xuICAgICAgICBcdHRoaXMuc2VydmljZSA9IHNlcnZpY2U7XG4gICAgICAgIFx0JCgnI2RpYWxvZ1JlbW92ZScpLm1vZGFsKCd0b2dnbGUnKTtcbiAgICAgICAgfSxcbiAgICAgICAgcmVtb3ZlU2VydmljZSgpe1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9ob21lL3JlbW92ZVNlcnZpY2UvJyt0aGlzLnNlcnZpY2UuaWQpXG5cdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICAgIHRoaXMuY2FsbERhdGFUYWJsZXMyKCk7XG5cdCAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnU2VydmljZSBzdWNjZXNzZnVsbHkgcmVtb3ZlZC4nKTtcblx0ICAgIFx0fSk7XG4gICAgICAgIH0sXG4gICAgICAgIG9wZW5QZW5kaW5nU2VydmljZShzZXJ2aWNlKXtcbiAgICAgICAgXHR0aGlzLnNlcnZpY2UgPSBzZXJ2aWNlO1xuICAgICAgICBcdHRoaXMuZm9ybS5pZCA9IHRoaXMuc2VydmljZS5pZDtcbiAgICAgICAgXHQkKCcjZGlhbG9nUmVhc29uJykubW9kYWwoJ3RvZ2dsZScpO1xuICAgICAgICB9LFxuICAgICAgICBtYXJrQXNQZW5kaW5nKCl7XG4gICAgICAgICAgICB0aGlzLmZvcm0uc3VibWl0KCdwb3N0JywgJy92aXNhL2hvbWUvbWFya0FzUGVuZGluZycpXG4gICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgXHR0aGlzLmNhbGxEYXRhVGFibGVzMSgpO1xuICAgICAgICAgICAgXHR0aGlzLmNhbGxEYXRhVGFibGVzMigpO1xuICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKCdSZWFzb24gc3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xuICAgICAgICAgICAgICAgICQoJyNkaWFsb2dSZWFzb24nKS5tb2RhbCgndG9nZ2xlJyk7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXJ2aWNlID0gJyc7XG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtLmlkID0gJyc7XG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtLnJlYXNvbiA9ICcnO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG4gICAgICAgIHNldFNlcnZpY2Uoc2VydmljZSkge1xuICAgICAgICBcdGxldCB7IGlkLCB0cmFja2luZywgdGlwLCBzdGF0dXMsIGNvc3QsIGFjdGl2ZSwgZXh0ZW5kLCByY3ZfZG9jcywgZG9jc19uZWVkZWQsIGRvY3Nfb3B0aW9uYWx9ID0gc2VydmljZTtcblxuICAgICAgICBcdGlmKHNlcnZpY2UuZGlzY291bnQyLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgIFx0XHRkaXNjb3VudCA9ICcnO1xuICAgICAgICBcdFx0cmVhc29uID0gJyc7XG4gICAgICAgIFx0fSBlbHNlIHtcbiAgICAgICAgXHRcdGRpc2NvdW50ID0gc2VydmljZS5kaXNjb3VudDJbMF0uZGlzY291bnRfYW1vdW50O1xuICAgICAgICBcdFx0cmVhc29uID0gc2VydmljZS5kaXNjb3VudDJbMF0ucmVhc29uO1xuICAgICAgICBcdH1cblxuICAgICAgICBcdHRoaXMuJHJlZnMuZWRpdHNlcnZpY2VkYXNoYm9hcmRyZWYuc2V0U2VydmljZShpZCwgdHJhY2tpbmcsIHRpcCwgc3RhdHVzLCBjb3N0LCBkaXNjb3VudCwgcmVhc29uLCBhY3RpdmUsIGV4dGVuZCwgcmN2X2RvY3MpO1xuICAgICAgICAgICAgdGhpcy4kcmVmcy5lZGl0c2VydmljZWRhc2hib2FyZHJlZi5mZXRjaERvY3MoZG9jc19uZWVkZWQsIGRvY3Nfb3B0aW9uYWwpO1xuICAgICAgICB9LFxuICAgICAgICBzdWJtaXRGaWx0ZXIoKXtcbiAgICAgICAgICAgIGFsZXJ0KCk7XG5cbiAgICAgICAgICAgICQoJyN0b2RheXNTZXJ2aWNlc0xpc3RzJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuICAgICAgICAgICAgcGFyYW1zID0ge1xuICAgICAgICAgICAgICAgIGRhdGU6ICQoJ2Zvcm0jZGF0ZS1yYW5nZS1mb3JtIGlucHV0W25hbWU9c3RhcnRdJykudmFsKClcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBheGlvcy5nZXQoJy92aXNhL2hvbWUvZ2V0LXNlcnZpY2UtYnlEYXRlJywge1xuICAgICAgICAgICAgICAgIHBhcmFtczogcGFyYW1zXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMudG9kYXlzU2VydmljZXMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgJCgnI3RvZGF5c1NlcnZpY2VzTGlzdHMnKS5EYXRhVGFibGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwibGVuZ3RoTWVudVwiOiBbWzEwLCAyNSwgNTBdLCBbMTAsIDI1LCA1MF1dLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAxMFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0sMjAwMCk7XG4gICAgXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICB9LFxuXG4gICAgICAgIHN1Ym1pdEZpbHRlcjIodmFsKXtcbiAgICAgICAgICAgICQoJyN0b2RheXNTZXJ2aWNlc0xpc3RzJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuICAgICAgICAgICAgaWYodmFsID09IDEpe1xuICAgICAgICAgICAgICAgICQoJyN5ZXN0ZXJkYXktc2VydicpLnJlbW92ZUNsYXNzKCdidG4tb3V0bGluZScpO1xuICAgICAgICAgICAgICAgICQoJyN0b2RheS1zZXJ2JykuYWRkQ2xhc3MoJ2J0bi1vdXRsaW5lJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgICAgICQoJyN0b2RheS1zZXJ2JykucmVtb3ZlQ2xhc3MoJ2J0bi1vdXRsaW5lJyk7XG4gICAgICAgICAgICAgICAgJCgnI3llc3RlcmRheS1zZXJ2JykuYWRkQ2xhc3MoJ2J0bi1vdXRsaW5lJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBwYXJhbXMgPSB7XG4gICAgICAgICAgICAgICAgZGF0ZTogdmFsXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgYXhpb3MuZ2V0KCcvdmlzYS9ob21lL2dldC1zZXJ2aWNlLWJ5RGF0ZScsIHtcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHBhcmFtc1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnRvZGF5c1NlcnZpY2VzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICQoJyN0b2RheXNTZXJ2aWNlc0xpc3RzJykuRGF0YVRhYmxlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwXSwgWzEwLCAyNSwgNTBdXSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiaURpc3BsYXlMZW5ndGhcIjogMTBcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKClcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9LDIwMDApO1xuICAgIFxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgfSxcblxuICAgICAgICBzdWJtaXRGaWx0ZXIzKHZhbCl7XG4gICAgICAgICAgICAkKCcjZGVsaXZlcnlTY2hlZHVsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcbiAgICAgICAgICAgIGlmKHZhbCA9PSAxKXtcbiAgICAgICAgICAgICAgICAkKCcjdG9tLXNjaGVkJykucmVtb3ZlQ2xhc3MoJ2J0bi1vdXRsaW5lJyk7XG4gICAgICAgICAgICAgICAgJCgnI3RvZGF5LXNjaGVkJykuYWRkQ2xhc3MoJ2J0bi1vdXRsaW5lJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKHZhbCA9PSAwKXtcbiAgICAgICAgICAgICAgICAkKCcjdG9kYXktc2NoZWQnKS5yZW1vdmVDbGFzcygnYnRuLW91dGxpbmUnKTtcbiAgICAgICAgICAgICAgICAkKCcjdG9tLXNjaGVkJykuYWRkQ2xhc3MoJ2J0bi1vdXRsaW5lJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgICAgIHZhbCA9ICQoJ2Zvcm0jZGF0ZS1yYW5nZS1mb3JtMiBpbnB1dFtuYW1lPXN0YXJ0Ml0nKS52YWwoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBhcmFtcyA9IHtcbiAgICAgICAgICAgICAgICBkYXRlOiB2YWxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBheGlvcy5nZXQoJy92aXNhL2hvbWUvZ2V0U2NoZWR1bGVzYnlEYXRlJywge1xuICAgICAgICAgICAgICAgIHBhcmFtczogcGFyYW1zXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcnlTY2hlZHVsZXMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgJCgnI2RlbGl2ZXJ5U2NoZWR1bGUnKS5EYXRhVGFibGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwibGVuZ3RoTWVudVwiOiBbWzEwLCAyNSwgNTBdLCBbMTAsIDI1LCA1MF1dLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAxMFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0sMjAwMCk7XG4gICAgXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBzaG93U2NoZWR1bGVNb2RhbCh2YWwsIGlkKXtcbiAgICAgICAgICAgIGlmKHZhbCA9PSBcImFkZFwiKXtcbiAgICAgICAgICAgICAgICAkKCcjYWRkU2NoZWR1bGVNb2RhbCcpLm1vZGFsKCd0b2dnbGUnKTtcbiAgICAgICAgICAgICAgICB0aGlzLm1vZGUgPSBcIkFkZFwiO1xuICAgICAgICAgICAgfWVsc2UgaWYodmFsID09IFwiZWRpdFwiKXtcbiAgICAgICAgICAgICAgICBheGlvcy5wb3N0KCcvdmlzYS9ob21lL2dldFNjaGVkLycrIGlkKSBcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZFNjaGVkdWxlID0gbmV3IEZvcm0gKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkICAgICAgICAgICAgICA6cmVzdWx0LmRhdGEuaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICBpdGVtICAgICAgICAgICAgOnJlc3VsdC5kYXRhLml0ZW0sXG4gICAgICAgICAgICAgICAgICAgICAgICByaWRlcl9pZCAgICAgICAgOnJlc3VsdC5kYXRhLnJpZGVyX2lkLFxuICAgICAgICAgICAgICAgICAgICAgICAgbG9jYXRpb24gICAgICAgIDpyZXN1bHQuZGF0YS5sb2NhdGlvbixcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjaGVkdWxlZF9kYXRlICA6cmVzdWx0LmRhdGEuc2NoZWR1bGVkX2RhdGUsXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2hlZHVsZWRfdGltZSAgOnJlc3VsdC5kYXRhLnNjaGVkdWxlZF90aW1lXG4gICAgICAgICAgICAgICAgICAgIH0seyBiYXNlVVJMOiAnL3Zpc2EvaG9tZS8nfSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZHQgPSByZXN1bHQuZGF0YS5zY2hlZHVsZWRfZGF0ZTtcbiAgICAgICAgICAgICAgICAgICAgJCgnZm9ybSNmcm1fZGVsaXZlcnlzY2hlZCAuaW5wdXRbbmFtZT1kYXRlXScpLnZhbChyZXN1bHQuZGF0YS5zY2hlZHVsZWRfZGF0ZSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubW9kZSA9IFwiRWRpdFwiO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICQoJyNhZGRTY2hlZHVsZU1vZGFsJykubW9kYWwoJ3RvZ2dsZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNob3dEZWxldGVTY2hlZFByb21wdCh2YWwpe1xuICAgICAgICAgICAgdGhpcy5zZWxJZCA9IHZhbDtcbiAgICAgICAgICAgICQoJyNkZWxldGVTY2hlZFByb21wdCcpLm1vZGFsKCd0b2dnbGUnKTtcbiAgICAgICAgfSxcblxuICAgICAgICBsb2FkQ29tcGFueUNvdXJpZXJzKCl7XG4gICAgICAgICAgICBheGlvcy5nZXQoJy92aXNhL2hvbWUvc2hvd0NvdXJpZXJzJylcbiAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvbXBhbnlDb3VyaWVycyA9IHJlc3BvbnNlLmRhdGE7ICAgIFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVzZXRTY2hlZEZybSgpe1xuICAgICAgICAgICAgJCgnZm9ybSNmcm1fZGVsaXZlcnlzY2hlZCBpbnB1dFtuYW1lPWRhdGVdJykudmFsKCk7XG4gICAgICAgICAgICB0aGlzLmFkZFNjaGVkdWxlID0gbmV3IEZvcm0gKHtcbiAgICAgICAgICAgICAgICBpdGVtOiAnJyxcbiAgICAgICAgICAgICAgICByaWRlcl9pZDogJycsXG4gICAgICAgICAgICAgICAgbG9jYXRpb246JycsXG4gICAgICAgICAgICAgICAgc2NoZWR1bGVkX2RhdGU6JycsXG4gICAgICAgICAgICAgICAgc2NoZWR1bGVkX3RpbWU6JydcbiAgICAgICAgICAgIH0seyBiYXNlVVJMOiAnL3Zpc2EvaG9tZS8nfSk7XG4gICAgICAgIH0sXG4gICAgICAgIFxuICAgICAgICBnZXREYXRlKCl7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKCQoJ2Zvcm0jZnJtX2RlbGl2ZXJ5c2NoZWQgLmlucHV0W25hbWU9ZGF0ZV0nKS52YWwoKSk7XG4gICAgICAgICAgICB0aGlzLmR0ID0gJCgnZm9ybSNmcm1fZGVsaXZlcnlzY2hlZCAuaW5wdXRbbmFtZT1kYXRlXScpLnZhbCgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNhdmVEZWxpdmVyU2NoZWR1bGUoKXtcbiAgICAgICAgICAgIHZhciBtID0gdGhpcy5tb2RlO1xuICAgICAgICAgICAgdmFyIGNvbWJpbmVUaW1lID0gdGhpcy4kcmVmcy5kZWxpdmVyeS5hbHdheXNUd29EaWdpdCh0aGlzLnNjaGVkdWxlZF9ob3VyKStcIjpcIit0aGlzLiRyZWZzLmRlbGl2ZXJ5LmFsd2F5c1R3b0RpZ2l0KHRoaXMuc2NoZWR1bGVkX21pbnV0ZSkrXCI6MDBcIjtcbiAgICAgICAgICAgIHRoaXMuYWRkU2NoZWR1bGUuc2NoZWR1bGVkX3RpbWUgPSBjb21iaW5lVGltZTtcbiAgICAgICAgICAgIHRoaXMuYWRkU2NoZWR1bGUuc2NoZWR1bGVkX2RhdGUgPSB0aGlzLiRyZWZzLmRlbGl2ZXJ5LmZvcm1hdERhdGVNRFkodGhpcy5hZGRTY2hlZHVsZS5zY2hlZHVsZWRfZGF0ZSk7XG5cbiAgICAgICAgICAgIGlmKG0gPT0gXCJBZGRcIil7XG4gICAgICAgICAgICAgICAgLy8gaWYodGhpcy5kdCA9PSAnJyl7XG4gICAgICAgICAgICAgICAgLy8gICAgIHRoaXMuZHQgPSAkKCdmb3JtI2ZybV9kZWxpdmVyeXNjaGVkIC5pbnB1dFtuYW1lPWRhdGVdJykudmFsKCk7XG4gICAgICAgICAgICAgICAgLy8gfVxuICAgICAgICAgICAgICAgIC8vIHRoaXMuYWRkU2NoZWR1bGUuc2NoZWR1bGVkX2RhdGUgPSB0aGlzLmR0O1xuICAgICAgICAgICAgICAgIHRoaXMuYWRkU2NoZWR1bGUuc3VibWl0KCdwb3N0JyAgLCAnL3NhdmUtZGVsaXZlcnktc2NoZWR1bGUnKVxuICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkKCcjYWRkU2NoZWR1bGVNb2RhbCcpLm1vZGFsKCd0b2dnbGUnKTtcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1NjaGVkdWxlIEFkZGVkLicpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc2V0U2NoZWRGcm0oKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignRmFpbGVkLiBBbGwgRmllbGRzIGFyZSByZXF1aXJlZCEnKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2V0U2NoZWRGcm0oKTtcbiAgICAgICAgICAgICAgICAvL3N0aGlzLmNhbGxEYXRhVGFibGVzNigpO1xuICAgICAgICAgICAgfWVsc2UgaWYgKG0gPT0gXCJFZGl0XCIpe1xuICAgICAgICAgICAgICAgIC8vIHRoaXMuYWRkU2NoZWR1bGUuc2NoZWR1bGVkX2RhdGUgPSB0aGlzLmR0O1xuICAgICAgICAgICAgICAgIHRoaXMuYWRkU2NoZWR1bGUuc3VibWl0KCdwb3N0JyAgLCAnL2VkaXRTY2hlZCcpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgICAgICAgICAgICAgICQoJyNhZGRTY2hlZHVsZU1vZGFsJykubW9kYWwoJ3RvZ2dsZScpO1xuICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnVXBkYXRlIFNjaGVkdWxlLicpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc2V0U2NoZWRGcm0oKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignVXBkYXRlIEZhaWxlZC4gQWxsIEZpZWxkcyBhcmUgcmVxdWlyZWQhJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXNldFNjaGVkRnJtKCk7XG4gICAgICAgICAgICAgICAgLy90aGlzLmNhbGxEYXRhVGFibGVzNigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy4kcmVmcy5kZWxpdmVyeS5kYXRlRmlsdGVyID0gdGhpcy5hZGRTY2hlZHVsZS5zY2hlZHVsZWRfZGF0ZTtcbiAgICAgICAgICAgIHRoaXMuJHJlZnMuZGVsaXZlcnkuZmlsdGVyU2NoZWQoJ2Zvcm0nKTtcbiAgICAgICAgfSxcblxuICAgICAgICBkZWxldGVTY2hlZHVsZShpZCl7XG4gICAgICAgICAgICBheGlvcy5wb3N0KCcvdmlzYS9ob21lL2RlbFNjaGVkLycraWQpXG4gICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgICAgICQoJyNkZWxldGVTY2hlZFByb21wdCcpLm1vZGFsKCd0b2dnbGUnKTtcbiAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IERlbGV0ZWQnKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmRlbGl2ZXJ5LmZpbHRlclNjaGVkKCd0b2RheScpOyBcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICAkKCcjZGVsZXRlU2NoZWRQcm9tcHQnKS5tb2RhbCgndG9nZ2xlJyk7XG4gICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKCdEZWxldGUgRmFpbGVkJyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIFxuICAgICAgICB9LFxuXG5cdH0sXG5cdGNyZWF0ZWQoKXtcblx0XHR0aGlzLmNhbGxEYXRhVGFibGVzMSgpO1xuXHRcdHRoaXMuY2FsbERhdGFUYWJsZXMyKCk7XG5cdFx0dGhpcy5jYWxsRGF0YVRhYmxlczMoKTtcbiAgICAgICAgdGhpcy5jYWxsRGF0YVRhYmxlczQoKTtcblx0XHQvLyB0aGlzLmNhbGxEYXRhVGFibGVzNSgpO1xuICAgICAgICAvLyB0aGlzLmNhbGxEYXRhVGFibGVzNigpO1xuICAgICAgICB0aGlzLmxvYWRDb21wYW55Q291cmllcnMoKTtcbiAgICAgICAgICAgICQoJ2Zvcm0jZGF0ZS1yYW5nZS1mb3JtIGlucHV0W25hbWU9c3RhcnRdJykuZGF0ZXBpY2tlcih7Zm9ybWF0OidtbS9kZC95eXl5J30pO1xuICAgICAgICAgICAgJCgnZm9ybSNkYXRlLXJhbmdlLWZvcm0gaW5wdXRbbmFtZT1zdGFydF0nKS5kYXRlcGlja2VyKCdzZXREYXRlJywgJ3RvZGF5Jyk7XG5cdH0sXG5cdGNvbXBvbmVudHM6IHtcblx0XHQnZWRpdC1zZXJ2aWNlLWRhc2hib2FyZCc6IHJlcXVpcmUoJy4uLy4uL2NvbXBvbmVudHMvVmlzYS9FZGl0U2VydmljZURhc2hib2FyZC52dWUnKSxcbiAgICAgICAgJ3RvZGF5cy10YXNrcyc6IHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvVG9kYXlzVGFza3MudnVlJyksXG4gICAgICAgICd0b2RheXMtc2VydmljZXMnOiByZXF1aXJlKCcuLi9jb21wb25lbnRzL1RvZGF5c1NlcnZpY2VzLnZ1ZScpLFxuICAgICAgICAnZGVsaXZlcnktc2NoZWR1bGUnOiByZXF1aXJlKCcuLi9jb21wb25lbnRzL0RlbGl2ZXJ5U2NoZWR1bGUudnVlJyksXG5cdH0sXG4gICAgbW91bnRlZCgpIHtcbiAgICAgICAgJCgnYm9keScpLnBvcG92ZXIoeyAvLyBPa1xuICAgICAgICAgICAgaHRtbDp0cnVlLFxuICAgICAgICAgICAgdHJpZ2dlcjogJ2hvdmVyJyxcbiAgICAgICAgICAgIHNlbGVjdG9yOiAnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXSdcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJChkb2N1bWVudCkub24oJ2ZvY3VzJywgJ2Zvcm0jZGF0ZS1yYW5nZS1mb3JtIC5pbnB1dC1kYXRlcmFuZ2UnLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgJCh0aGlzKS5kYXRlcGlja2VyKHtcbiAgICAgICAgICAgICAgICBmb3JtYXQ6J21tL2RkL3l5eXknLFxuICAgICAgICAgICAgICAgIHRvZGF5SGlnaGxpZ2h0OiB0cnVlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJChkb2N1bWVudCkub24oJ2ZvY3VzJywgJ2Zvcm0jZGF0ZS1yYW5nZS1mb3JtMiAuaW5wdXQtZGF0ZXJhbmdlJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICQodGhpcykuZGF0ZXBpY2tlcih7XG4gICAgICAgICAgICAgICAgZm9ybWF0OidtbS9kZC95eXl5JyxcbiAgICAgICAgICAgICAgICB0b2RheUhpZ2hsaWdodDogdHJ1ZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoZG9jdW1lbnQpLm9uKCdmb2N1cycsICdmb3JtI2ZybV9kZWxpdmVyeXNjaGVkIC5pbnB1dC1kYXRlcmFuZ2UnLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgJCh0aGlzKS5kYXRlcGlja2VyKHtcbiAgICAgICAgICAgICAgICBmb3JtYXQ6J21tL2RkL3l5eXknLFxuICAgICAgICAgICAgICAgIHRvZGF5SGlnaGxpZ2h0OiB0cnVlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgICAvLyAkKGRvY3VtZW50KS5vbignZm9jdXMnLCAnZm9ybSNkYWlseS1mb3JtIC5pbnB1dC1ncm91cC5kYXRlJywgZnVuY3Rpb24oKXtcbiAgICAgICAgLy8gICAgICQodGhpcykuZGF0ZXBpY2tlcih7XG4gICAgICAgIC8vICAgICAgICAgZm9ybWF0Oid5eXl5LW1tLWRkJywgICAgIFxuICAgICAgICAvLyAgICAgICAgIHRvZGF5SGlnaGxpZ2h0OiB0cnVlLFxuICAgICAgICAvLyAgICAgfSk7XG4gICAgICAgIC8vIH0pO1xuICAgIH1cbn0pO1xuXG4kKCBkb2N1bWVudCApLnJlYWR5KGZ1bmN0aW9uKCkge1xuICAgICQoJ1tkYXRhLXRvZ2dsZT1wb3BvdmVyXScpLnBvcG92ZXIoKTtcblxuICAgICQoJ2JvZHknKS50b29sdGlwKHtcbiAgICAgICAgc2VsZWN0b3I6ICdbZGF0YS10b2dnbGU9dG9vbHRpcF0nXG4gICAgfSk7XG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2Rhc2hib2FyZC9pbmRleC5qcyIsIi8qXHJcblx0TUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcclxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXHJcbiovXHJcbi8vIGNzcyBiYXNlIGNvZGUsIGluamVjdGVkIGJ5IHRoZSBjc3MtbG9hZGVyXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oKSB7XHJcblx0dmFyIGxpc3QgPSBbXTtcclxuXHJcblx0Ly8gcmV0dXJuIHRoZSBsaXN0IG9mIG1vZHVsZXMgYXMgY3NzIHN0cmluZ1xyXG5cdGxpc3QudG9TdHJpbmcgPSBmdW5jdGlvbiB0b1N0cmluZygpIHtcclxuXHRcdHZhciByZXN1bHQgPSBbXTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHZhciBpdGVtID0gdGhpc1tpXTtcclxuXHRcdFx0aWYoaXRlbVsyXSkge1xyXG5cdFx0XHRcdHJlc3VsdC5wdXNoKFwiQG1lZGlhIFwiICsgaXRlbVsyXSArIFwie1wiICsgaXRlbVsxXSArIFwifVwiKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXN1bHQucHVzaChpdGVtWzFdKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHJlc3VsdC5qb2luKFwiXCIpO1xyXG5cdH07XHJcblxyXG5cdC8vIGltcG9ydCBhIGxpc3Qgb2YgbW9kdWxlcyBpbnRvIHRoZSBsaXN0XHJcblx0bGlzdC5pID0gZnVuY3Rpb24obW9kdWxlcywgbWVkaWFRdWVyeSkge1xyXG5cdFx0aWYodHlwZW9mIG1vZHVsZXMgPT09IFwic3RyaW5nXCIpXHJcblx0XHRcdG1vZHVsZXMgPSBbW251bGwsIG1vZHVsZXMsIFwiXCJdXTtcclxuXHRcdHZhciBhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzID0ge307XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaWQgPSB0aGlzW2ldWzBdO1xyXG5cdFx0XHRpZih0eXBlb2YgaWQgPT09IFwibnVtYmVyXCIpXHJcblx0XHRcdFx0YWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xyXG5cdFx0fVxyXG5cdFx0Zm9yKGkgPSAwOyBpIDwgbW9kdWxlcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaXRlbSA9IG1vZHVsZXNbaV07XHJcblx0XHRcdC8vIHNraXAgYWxyZWFkeSBpbXBvcnRlZCBtb2R1bGVcclxuXHRcdFx0Ly8gdGhpcyBpbXBsZW1lbnRhdGlvbiBpcyBub3QgMTAwJSBwZXJmZWN0IGZvciB3ZWlyZCBtZWRpYSBxdWVyeSBjb21iaW5hdGlvbnNcclxuXHRcdFx0Ly8gIHdoZW4gYSBtb2R1bGUgaXMgaW1wb3J0ZWQgbXVsdGlwbGUgdGltZXMgd2l0aCBkaWZmZXJlbnQgbWVkaWEgcXVlcmllcy5cclxuXHRcdFx0Ly8gIEkgaG9wZSB0aGlzIHdpbGwgbmV2ZXIgb2NjdXIgKEhleSB0aGlzIHdheSB3ZSBoYXZlIHNtYWxsZXIgYnVuZGxlcylcclxuXHRcdFx0aWYodHlwZW9mIGl0ZW1bMF0gIT09IFwibnVtYmVyXCIgfHwgIWFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaXRlbVswXV0pIHtcclxuXHRcdFx0XHRpZihtZWRpYVF1ZXJ5ICYmICFpdGVtWzJdKSB7XHJcblx0XHRcdFx0XHRpdGVtWzJdID0gbWVkaWFRdWVyeTtcclxuXHRcdFx0XHR9IGVsc2UgaWYobWVkaWFRdWVyeSkge1xyXG5cdFx0XHRcdFx0aXRlbVsyXSA9IFwiKFwiICsgaXRlbVsyXSArIFwiKSBhbmQgKFwiICsgbWVkaWFRdWVyeSArIFwiKVwiO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRsaXN0LnB1c2goaXRlbSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9O1xyXG5cdHJldHVybiBsaXN0O1xyXG59O1xyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcbi8vIG1vZHVsZSBpZCA9IDJcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgMyA0IDUgOSAxMSAxMiAxMyIsIjx0ZW1wbGF0ZT5cblxuXHQ8Zm9ybSBjbGFzcz1cImZvcm0taG9yaXpvbnRhbFwiIEBzdWJtaXQucHJldmVudD1cInNhdmVTZXJ2aWNlXCI+XG5cblx0XHQ8ZGl2PlxuXHRcdFx0XG5cdFx0XHQ8ZGl2IGNsYXNzcz1cImNvbC1tZC0xMlwiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBjb2wtbWQtNlwiPlxuXHRcdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTQgY29udHJvbC1sYWJlbCBcIj5cblx0XHRcdFx0ICAgICAgICBUaXA6XG5cdFx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC04XCI+XG5cdFx0XHRcdCAgICAgICAgPGlucHV0IHR5cGU9XCJudW1iZXJcIiBtaW49XCIwXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwidGlwXCIgc3R5bGU9XCJ3aWR0aDoxNzVweDtcIiB2LW1vZGVsPVwic2F2ZVNlcnZpY2VGb3JtLnRpcFwiPlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNvbC1tZC02IG0tbC0yXCI+XG5cdFx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFwiPlxuXHRcdFx0XHQgICAgICAgIFN0YXR1czpcblx0XHRcdFx0ICAgIDwvbGFiZWw+XG5cdFx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLThcIj5cblx0XHRcdFx0ICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cInN0YXR1c1wiIHN0eWxlPVwid2lkdGg6MTc1cHg7XCIgdi1tb2RlbD1cInNhdmVTZXJ2aWNlRm9ybS5zdGF0dXNcIj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImNvbXBsZXRlXCI+Q29tcGxldGU8L29wdGlvbj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIm9uIHByb2Nlc3NcIj5PbiBQcm9jZXNzPC9vcHRpb24+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJwZW5kaW5nXCI+UGVuZGluZzwvb3B0aW9uPlxuXHQgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cblx0XHRcdDxkaXYgY2xhc3NzPVwiY29sLW1kLTEyXCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIGNvbC1tZC02XCI+XG5cdFx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFwiPlxuXHRcdFx0XHQgICAgICAgIENvc3Q6XG5cdFx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC04XCI+XG5cdFx0XHRcdCAgICAgICAgPGlucHV0IHR5cGU9XCJudW1iZXJcIiBtaW49XCIwXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwiY29zdFwiIHN0eWxlPVwid2lkdGg6MTc1cHg7XCIgdi1tb2RlbD1cInNhdmVTZXJ2aWNlRm9ybS5jb3N0XCI+XG5cdFx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdFx0PC9kaXY+XG5cblx0XHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgY29sLW1kLTYgbS1sLTJcIj5cblx0XHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCI+XG5cdFx0XHRcdCAgICAgICAgRGlzY291bnQ6XG5cdFx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC04XCI+XG5cdFx0XHRcdCAgICAgICAgPGlucHV0IHR5cGU9XCJudW1iZXJcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJkaXNjb3VudFwiIG1pbj1cIjBcIiBzdHlsZT1cIndpZHRoOjE3NXB4O1wiIHYtbW9kZWw9XCJzYXZlU2VydmljZUZvcm0uZGlzY291bnRcIj5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGNsYXNzcz1cImNvbC1tZC0xMlwiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0XHQgICAgICAgIFJlYXNvbjpcblx0XHRcdFx0ICAgIDwvbGFiZWw+XG5cdFx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHRcdCAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwicmVhc29uXCIgdi1tb2RlbD1cInNhdmVTZXJ2aWNlRm9ybS5yZWFzb25cIj5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdFx0XG5cdFx0XHQ8ZGl2IGNsYXNzcz1cImNvbC1tZC0xMlwiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0XHQgICAgICAgIE5vdGU6XG5cdFx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0XHQgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cIm5vdGVcIiB2LW1vZGVsPVwic2F2ZVNlcnZpY2VGb3JtLm5vdGVcIj5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGNsYXNzcz1cImNvbC1tZC0xMlwiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBjb2wtbWQtNlwiPlxuXHRcdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC00IGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdFx0ICAgICAgICBTdGF0dXM6XG5cdFx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC04XCI+XG5cdFx0XHRcdCAgICBcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6NXB4O2NsZWFyOmJvdGg7XCI+PC9kaXY+XG5cdFx0XHRcdCAgICAgICAgPGxhYmVsIGNsYXNzPVwiXCI+IFxuXHRcdFx0XHQgICAgICAgIFx0PGlucHV0IHR5cGU9XCJyYWRpb1wiIHZhbHVlPVwiMFwiIG5hbWU9XCJhY3RpdmVcIiB2LW1vZGVsPVwic2F2ZVNlcnZpY2VGb3JtLmFjdGl2ZVwiPiA8aT48L2k+IFxuXHRcdFx0XHQgICAgICAgIFx0RGlzYWJsZWQgXG5cdFx0XHRcdCAgICAgICAgPC9sYWJlbD5cblxuXHQgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cIm0tbC0yXCI+IFxuXHQgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJyYWRpb1wiIHZhbHVlPVwiMVwiIG5hbWU9XCJhY3RpdmVcIiB2LW1vZGVsPVwic2F2ZVNlcnZpY2VGb3JtLmFjdGl2ZVwiPiA8aT48L2k+IFxuXHQgICAgICAgICAgICAgICAgICAgIFx0RW5hYmxlZCBcblx0ICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxuXHQgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBjb2wtbWQtNlwiPlxuXHRcdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTQgY29udHJvbC1sYWJlbCBcIj5cblx0XHRcdFx0ICAgICAgICBFeHRlbmQgVG86XG5cdFx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XG5cdFx0XHRcdCAgICBcdDxmb3JtIGlkPVwiZGFpbHktZm9ybVwiIGNsYXNzPVwiZm9ybS1pbmxpbmVcIj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlxuXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cblx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cImRhdGVcIiB2LW1vZGVsPVwic2F2ZVNlcnZpY2VGb3JtLmV4dGVuZFwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBuYW1lPVwiZGF0ZVwiPlxuXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHQgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cblx0XHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0XHQgICAgICAgXHRSZXF1aXJlZCBEb2N1bWVudHM6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICAgICAgPHNlbGVjdCBkYXRhLXBsYWNlaG9sZGVyPVwiU2VsZWN0IERvY3NcIiBjbGFzcz1cImNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3NcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjM1MHB4O1wiIHRhYmluZGV4PVwiNFwiPlxuXHRcdFx0ICAgICAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XCJkb2MgaW4gcmVxdWlyZWREb2NzXCIgOnZhbHVlPVwiZG9jLmlkXCI+XG5cdFx0XHQgICAgICAgICAgICAgICAgXHR7eyAoZG9jLnRpdGxlKS50cmltKCkgfX1cblx0XHRcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cblx0XHRcdCAgICAgICAgICAgIDwvc2VsZWN0PlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0XHQgICAgICAgXHRPcHRpb25hbCBEb2N1bWVudHM6XG5cdFx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgICAgICA8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XCJTZWxlY3QgRG9jc1wiIGNsYXNzPVwiY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jc1wiIG11bHRpcGxlIHN0eWxlPVwid2lkdGg6MzUwcHg7XCIgdGFiaW5kZXg9XCI0XCI+XG5cdFx0XHQgICAgICAgICAgICAgICAgPG9wdGlvbiB2LWZvcj1cImRvYyBpbiBvcHRpb25hbERvY3NcIiA6dmFsdWU9XCJkb2MuaWRcIj5cblx0XHRcdCAgICAgICAgICAgICAgICBcdHt7IChkb2MudGl0bGUpLnRyaW0oKSB9fVxuXHRcdFx0ICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdFx0ICAgICAgICAgICAgPC9zZWxlY3Q+XG5cdFx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXG48IS0tIFx0XHRcdDxkaXYgY2xhc3NzPVwiY29sLW1kLTEyXCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHRcdCAgICAgICAgUmVwb3J0aW5nOlxuXHRcdFx0XHQgICAgPC9sYWJlbD5cblx0XHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdFx0ICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJyZXBvcnRpbmdcIiB2LW1vZGVsPVwiJHBhcmVudC4kcGFyZW50LmVkaXRTZXJ2LnJlcG9ydGluZ1wiPlxuXHRcdFx0XHQgICAgPC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+IC0tPlxuXHRcdFx0XG5cblx0XHQ8L2Rpdj5cblxuXHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxuXHRcdFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG5cdFx0XHQ8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCI+U2F2ZTwvYnV0dG9uPlxuXHQgICAgPC9kaXY+XG5cdDwvZm9ybT5cblxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblx0XG5cdGV4cG9ydCBkZWZhdWx0IHtcblxuXHRcdGRhdGEoKSB7XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRkdDonJyxcblx0XHRcdFx0cmVxdWlyZWREb2NzOiBbXSxcbiAgICAgICAgXHRcdG9wdGlvbmFsRG9jczogW10sXG5cdFx0XHRcdHNhdmVTZXJ2aWNlRm9ybToge1xuXHRcdFx0XHRcdGlkOiAnJyxcblx0XHRcdFx0XHR0cmFja2luZzogJycsXG5cdFx0XHRcdFx0dGlwOiAnJyxcblx0XHRcdFx0XHRzdGF0dXM6ICcnLFxuXHRcdFx0XHRcdGNvc3Q6ICcnLFxuXHRcdFx0XHRcdGRpc2NvdW50OiAnJyxcblx0XHRcdFx0XHRyZWFzb246ICcnLFxuXHRcdFx0XHRcdG5vdGU6ICcnLCBcblx0XHRcdFx0XHRhY3RpdmU6ICcnLFxuXHRcdFx0XHRcdHJlcG9ydGluZzogJycsXG5cdFx0XHRcdFx0ZXh0ZW5kOicnXG5cdFx0XHRcdH0sXG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdG1ldGhvZHM6IHtcblxuXHRcdFx0aW5pdENob3NlblNlbGVjdCgpIHtcblxuXHRcdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS5jaG9zZW4oe1xuXHRcdFx0XHRcdHdpZHRoOiBcIjEwMCVcIixcblx0XHRcdFx0XHRub19yZXN1bHRzX3RleHQ6IFwiTm8gcmVzdWx0L3MgZm91bmQuXCIsXG5cdFx0XHRcdFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHR9LFxuXG5cdFx0XHQgZ2V0RGF0ZSgpe1xuXHRcdCAgICBcdGNvbnNvbGUubG9nKCQoJ2Zvcm0jZGFpbHktZm9ybSBpbnB1dFtuYW1lPWRhdGVdJykudmFsKCkpO1xuXHRcdCAgICBcdHRoaXMuZHQgPSAkKCdmb3JtI2RhaWx5LWZvcm0gaW5wdXRbbmFtZT1kYXRlXScpLnZhbCgpO1xuXHRcdCAgICBcdHRoaXMuc2F2ZVNlcnZpY2VGb3JtLmV4dGVuZCA9IHRoaXMuZHQ7XG5cdFx0ICAgIH0sXG5cblx0XHRcdHNldFNlcnZpY2UoaWQsIHRyYWNraW5nLCB0aXAsIHN0YXR1cywgY29zdCwgZGlzY291bnQsIHJlYXNvbiwgYWN0aXZlLCBleHRlbmQsIHJjdl9kb2NzKSB7XG5cdFx0XHRcdHRoaXMuc2F2ZVNlcnZpY2VGb3JtID0ge1xuXHRcdFx0XHRcdGlkLCB0cmFja2luZywgdGlwLCBzdGF0dXMsIGNvc3QsIGRpc2NvdW50LCByZWFzb24sIGFjdGl2ZSwgZXh0ZW5kLCByY3ZfZG9jc31cblx0XHRcdH0sXG5cblx0XHRcdGNsZWFyU2F2ZVNlcnZpY2VGb3JtKCkge1xuXHRcdFx0XHR0aGlzLnNhdmVTZXJ2aWNlRm9ybSA9IHtcblx0XHRcdFx0XHRpZDogJycsXG5cdFx0XHRcdFx0dHJhY2tpbmc6ICcnLFxuXHRcdFx0XHRcdHRpcDogJycsXG5cdFx0XHRcdFx0c3RhdHVzOiAnJyxcblx0XHRcdFx0XHRjb3N0OiAnJyxcblx0XHRcdFx0XHRkaXNjb3VudDogJycsXG5cdFx0XHRcdFx0cmVhc29uOiAnJyxcblx0XHRcdFx0XHRub3RlOiAnJywgXG5cdFx0XHRcdFx0YWN0aXZlOiAnJyxcblx0XHRcdFx0XHRyZXBvcnRpbmc6ICcnLFxuXHRcdFx0XHRcdGV4dGVuZDonJyxcblx0XHRcdFx0XHRyY3ZfZG9jczonJ1x0XHRcdFx0XHRcblx0XHRcdFx0fVxuXHRcdFx0fSxcblxuXHRcdFx0c2F2ZVNlcnZpY2UoKSB7XG5cdFx0XHRcdHZhciByZXF1aXJlZERvY3MgPSAnJztcbiAgICAgICAgXHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzIG9wdGlvbjpzZWxlY3RlZCcpLmVhY2goIGZ1bmN0aW9uKGUpIHtcblx0XHRcdCAgICAgICAgcmVxdWlyZWREb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XG5cdFx0XHQgICAgfSk7XG5cbiAgICAgICAgXHRcdHZhciBvcHRpb25hbERvY3MgPSAnJztcbiAgICAgICAgXHRcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzIG9wdGlvbjpzZWxlY3RlZCcpLmVhY2goIGZ1bmN0aW9uKGUpIHtcblx0XHRcdCAgICAgICAgb3B0aW9uYWxEb2NzICs9ICQodGhpcykudmFsKCkgKyAnLCc7XG5cdFx0XHQgICAgfSk7XG5cblx0XHRcdCAgICB2YXIgZG9jcyA9IHJlcXVpcmVkRG9jcyArIG9wdGlvbmFsRG9jcztcblx0XHRcdFx0dGhpcy5zYXZlU2VydmljZUZvcm0ucmN2X2RvY3MgPSBkb2NzLnNsaWNlKDAsIC0xKTtcblxuXHRcdFx0XHRpZih0aGlzLnNhdmVTZXJ2aWNlRm9ybS5kaXNjb3VudCA+IDAgJiYgdGhpcy5zYXZlU2VydmljZUZvcm0ucmVhc29uID09ICcnKXtcblx0XHRcdCAgICAgICAgdG9hc3RyLmVycm9yKCdQbGVhc2UgaW5wdXQgZGlzY291bnQgcmVhc29uLicpO1xuXHRcdFx0ICAgIH1lbHNlIGlmKHJlcXVpcmVkRG9jcyA9PSAnJyAmJiB0aGlzLnJlcXVpcmVkRG9jcy5sZW5ndGghPTApe1xuICAgICAgICBcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgUmVxdWlyZWQgRG9jdW1lbnRzLicpOyAgICAgICAgXHRcdFxuXHRcdFx0ICAgIH0gZWxzZSB7XG5cbiAgICAgICAgXHRcdFx0YXhpb3MucG9zdCgnL3Zpc2EvY2xpZW50L2NsaWVudEVkaXRTZXJ2aWNlJywge1xuXHRcdFx0XHRcdFx0aWQ6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLmlkLFxuXHRcdFx0XHRcdFx0dHJhY2tpbmc6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnRyYWNraW5nLFxuXHRcdFx0XHRcdFx0dGlwOiB0aGlzLnNhdmVTZXJ2aWNlRm9ybS50aXAsXG5cdFx0XHRcdFx0XHRzdGF0dXM6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnN0YXR1cyxcblx0XHRcdFx0XHRcdGNvc3Q6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLmNvc3QsXG5cdFx0XHRcdFx0XHRkaXNjb3VudDogdGhpcy5zYXZlU2VydmljZUZvcm0uZGlzY291bnQsXG5cdFx0XHRcdFx0XHRyZWFzb246IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnJlYXNvbixcblx0XHRcdFx0XHRcdG5vdGU6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLm5vdGUsXG5cdFx0XHRcdFx0XHRhY3RpdmU6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLmFjdGl2ZSxcblx0XHRcdFx0XHRcdHJlcG9ydGluZzogdGhpcy5zYXZlU2VydmljZUZvcm0ucmVwb3J0aW5nLFxuXHRcdFx0XHRcdFx0ZXh0ZW5kOiB0aGlzLnNhdmVTZXJ2aWNlRm9ybS5leHRlbmQsXG5cdFx0XHRcdFx0XHRyY3ZfZG9jczogdGhpcy5zYXZlU2VydmljZUZvcm0ucmN2X2RvY3Ncblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHRcdGlmKHJlc3BvbnNlLmRhdGEuc3VjY2Vzcykge1xuXHRcdFx0XHRcdFx0XHR0aGlzLmNsZWFyU2F2ZVNlcnZpY2VGb3JtKCk7XG5cblx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5jYWxsRGF0YVRhYmxlczEoKTtcblx0XHQgICAgICAgICAgICAgICAgXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5jYWxsRGF0YVRhYmxlczIoKTtcblx0XHQgICAgICAgICAgICAgICAgXHQkKFwiLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzXCIpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdCQoJyNlZGl0U2VydmljZU1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdCAgICAgICAgICAgICAgICBcdHRvYXN0ci5zdWNjZXNzKCdTZXJ2aWNlIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5LicpO1xuXHRcdCAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT57XG5cdFx0ICAgICAgICAgICAgICAgIGZvcih2YXIga2V5IGluIGVycm9yKSB7XG5cdFx0XHQgICAgICAgICAgICBcdGlmKGVycm9yLmhhc093blByb3BlcnR5KGtleSkpIHRvYXN0ci5lcnJvcihlcnJvcltrZXldWzBdKVxuXHRcdFx0ICAgICAgICAgICAgfVxuXHRcdCAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIFx0XHR9XG5cdFx0XHR9LFxuXG5cdFx0XHRmZXRjaERvY3MocmQsIG9kKXtcblxuXHRcdFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS52YWwoJycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cblx0XHRcdFx0aWYodGhpcy5zYXZlU2VydmljZUZvcm0ucmN2X2RvY3MgPT0gJycgfHwgdGhpcy5zYXZlU2VydmljZUZvcm0ucmN2X2RvY3MgPT0gbnVsbClcblx0XHRcdFx0XHR2YXIgcmN2ID0gKCB0aGlzLnNhdmVTZXJ2aWNlRm9ybS5yY3ZfZG9jcyE9bnVsbCA/IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnJjdl9kb2NzLnNwbGl0KFwiLFwiKSA6ICcnKTtcblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdHZhciByY3YgPSB0aGlzLnNhdmVTZXJ2aWNlRm9ybS5yY3ZfZG9jcy5zcGxpdChcIixcIik7XG5cblx0XHRcdCBcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJytyZCsnL3NlcnZpY2UtZG9jcycpXG5cdFx0XHQgXHQgIC50aGVuKHJlc3VsdCA9PiB7XG5cdFx0ICAgICAgICAgICAgdGhpcy5yZXF1aXJlZERvY3MgPSByZXN1bHQuZGF0YTtcblx0XHQgICAgICAgIH0pO1xuXHQgICAgICAgIFx0XG5cdFx0XHQgXHRheGlvcy5nZXQoJy92aXNhL2dyb3VwLycrb2QrJy9zZXJ2aWNlLWRvY3MnKVxuXHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdCAgICAgICAgICAgIHRoaXMub3B0aW9uYWxEb2NzID0gcmVzdWx0LmRhdGE7XG5cdFx0ICAgICAgICB9KTtcblxuXHRcdCAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG5cdCAgICAgICAgXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS5jaG9zZW4oJ2Rlc3Ryb3knKTtcblx0ICAgICAgICBcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLmNob3Nlbih7XG5cdFx0XHRcdFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdFx0XHRcdG5vX3Jlc3VsdHNfdGV4dDogXCJObyByZXN1bHQvcyBmb3VuZC5cIixcblx0XHRcdFx0XHRzZWFyY2hfY29udGFpbnM6IHRydWVcblx0XHRcdFx0fSk7XG5cblx0ICAgICAgICBcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLnZhbChyY3YpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG4gICAgICAgIFx0XHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xuICAgICAgICBcdH0sIDIwMDApO1xuXG5cdFx0XHR9LFxuXG5cdFx0XG5cblxuXHRcdH0sXG5cdFx0bW91bnRlZCgpIHtcblx0XHRcdHRoaXMuaW5pdENob3NlblNlbGVjdCgpO1xuICAgICAgICBcdC8vIERhdGVQaWNrZXJcblx0ICAgICAgICBcdCQoJy5kYXRlcGlja2VyJykuZGF0ZXBpY2tlcih7XG5cdCAgICAgICAgICAgICAgICB0b2RheUJ0bjogXCJsaW5rZWRcIixcblx0ICAgICAgICAgICAgICAgIGtleWJvYXJkTmF2aWdhdGlvbjogZmFsc2UsXG5cdCAgICAgICAgICAgICAgICBmb3JjZVBhcnNlOiBmYWxzZSxcblx0ICAgICAgICAgICAgICAgIGNhbGVuZGFyV2Vla3M6IHRydWUsXG5cdCAgICAgICAgICAgICAgICBhdXRvY2xvc2U6IHRydWUsXG5cdCAgICAgICAgICAgICAgICBmb3JtYXQ6IFwieXl5eS1tbS1kZFwiLFxuXHQgICAgICAgICAgICB9KVxuXHQgICAgICAgICAgICAub24oJ2NoYW5nZURhdGUnLCBmdW5jdGlvbihlKSB7IC8vIEJvb3RzdHJhcCBEYXRlcGlja2VyIG9uQ2hhbmdlXG5cdCAgICAgICAgICAgIFx0bGV0IGlkID0gZS50YXJnZXQuaWQ7XG5cdFx0XHQgICAgICAgIGxldCBkYXRlID0gZS50YXJnZXQudmFsdWU7XG5cblx0XHRcdCAgICAgICAgY29uc29sZS5sb2coJ0lEIDogJyArIGlkKTtcblx0XHRcdCAgICAgICAgY29uc29sZS5sb2coJ0RBVEUgOiAnICsgZGF0ZSk7XG5cdFx0XHQgICAgICAgIHRoaXMuZ2V0RGF0ZSgpO1xuXHRcdFx0ICAgIH0uYmluZCh0aGlzKSlcblx0XHRcdCAgICAub24oJ2NoYW5nZScsIGZ1bmN0aW9uKGUpIHsgLy8gbmF0aXZlIG9uQ2hhbmdlXG5cdFx0XHQgICAgXHRsZXQgaWQgPSBlLnRhcmdldC5pZDtcblx0XHRcdCAgICBcdGxldCBkYXRlID0gZS50YXJnZXQudmFsdWU7XG5cblx0XHRcdCAgICBcdGNvbnNvbGUubG9nKCdJRCA6ICcgKyBpZCk7XG5cdFx0XHQgICAgICAgIGNvbnNvbGUubG9nKCdEQVRFIDogJyArIGRhdGUpO1xuXHRcdFx0ICAgIH0uYmluZCh0aGlzKSk7XG5cbiAgICAgICAgfVxuXHR9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRWRpdFNlcnZpY2VEYXNoYm9hcmQudnVlPzUxMDkyMTdhIiwiPHRlbXBsYXRlPlxuXHRcblx0PGRpdj5cblx0XHRcblx0XHQ8Zm9ybSBpZD1cImRlbGl2ZXJ5LXNjaGVkLWZvcm1cIiBjbGFzcz1cImZvcm0taW5saW5lXCIgQHN1Ym1pdC5wcmV2ZW50PVwiZmlsdGVyU2NoZWQoJ2Zvcm0nKVwiPlxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWRhdGVyYW5nZSBpbnB1dC1ncm91cFwiIGlkPVwiZGVsaXZlcnktc2NoZWRzLWRhdGVwaWNrZXJcIj5cblx0XHQgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlucHV0LXNtIGZvcm0tY29udHJvbFwiIG5hbWU9XCJkZWxpdmVyeS1zY2hlZC1kYXRlXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgLz5cblx0XHQgICAgICAgIDwvZGl2PlxuXHRcdCAgICA8L2Rpdj5cblx0XHQgICAgXG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0ICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIHN0eWxlPVwibGluZS1oZWlnaHQ6IDEuMjtcIj5cblx0XHQgICAgICAgICAgICA8c3Ryb25nPkZpbHRlcjwvc3Ryb25nPlxuXHRcdCAgICAgICAgPC9idXR0b24+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdFx0PGJ1dHRvbiBpZD1cImFkZF9zY2hlZHVsZVwiIGNsYXNzPVwiYnRuIGJ0bi13YXJuaW5nXCIgc3R5bGU9XCJsaW5lLWhlaWdodDogMS4yOyBtYXJnaW4tbGVmdDogMnB4O1wiIHR5cGU9XCJidXR0b25cIiBAY2xpY2s9XCJzaG93U2NoZWR1bGVNb2RhbCgnYWRkJywgJycpXCI+QWRkIFNjaGVkdWxlIDwvYnV0dG9uPlxuXG5cdFx0XHQ8YnV0dG9uIEBjbGljaz1cImZpbHRlclNjaGVkKCd0b2RheScpXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclwiIDpjbGFzcz1cInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0ndG9kYXknfVwiIHN0eWxlPVwibWFyZ2luLXJpZ2h0OiAycHg7XCIgdHlwZT1cImJ1dHRvblwiPlRvZGF5PC9idXR0b24+XG5cblx0XHRcdDxidXR0b24gQGNsaWNrPVwiZmlsdGVyU2NoZWQoJ3RvbW9ycm93JylcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXCIgOmNsYXNzPVwieydidG4tb3V0bGluZScgOiBhY3RpdmVCdXR0b24hPSd5ZXN0ZXJkYXknfVwiIHN0eWxlPVwibWFyZ2luLXJpZ2h0OiAycHg7XCIgdHlwZT1cImJ1dHRvblwiPlRvbW9ycm93PC9idXR0b24+XG5cdFx0PC9mb3JtPlxuXG5cdFx0PGRpdiBzdHlsZT1cImhlaWdodDoyMHB4OyBjbGVhcjpib3RoO1wiPjwvZGl2PlxuXG4gICAgXHQ8dGFibGUgaWQ9XCJkZWxpdmVyeVNjaGVkdWxlXCIgY2xhc3M9XCJ0YWJsZSB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmVkIHRhYmxlLWhvdmVyIGRhdGFUYWJsZXMtZXhhbXBsZVwiPlxuXHQgICAgICAgIDx0aGVhZD5cblx0ICAgICAgICAgICAgPHRyPlxuXHQgICAgICAgICAgICAgICAgPHRoPkl0ZW08L3RoPlxuXHQgICAgICAgICAgICAgICAgPHRoPlNjaGVkdWxlPC90aD5cblx0ICAgICAgICAgICAgICAgIDx0aD5Mb2NhdGlvbjwvdGg+XG5cdCAgICAgICAgICAgICAgICA8dGg+UmlkZXI8L3RoPlxuXHQgICAgICAgICAgICAgICAgPHRoPkFjdGlvbjwvdGg+ICAgICAgICAgXG5cdCAgICAgICAgICAgIDwvdHI+XG5cdCAgICAgICAgPC90aGVhZD5cblx0ICAgICAgICAgICAgPHRib2R5PlxuXHQgICAgICAgICAgICBcdDx0ciB2LWZvcj1cImRzIGluIGRlbGl2ZXJ5U2NoZWR1bGVzXCIgdi1jbG9haz5cblx0ICAgICAgICAgICAgXHRcdDx0ZD57eyBkcy5pdGVtIH19PC90ZD5cblx0XHRcdFx0XHRcdDx0ZD57eyBkcy5zY2hlZHVsZWRfZGF0ZSB9fSB7eyBkcy5zY2hlZHVsZWRfdGltZX19PC90ZD5cblx0XHRcdFx0XHRcdDx0ZD57eyBkcy5sb2NhdGlvbiB9fTwvdGQ+XG5cdFx0XHRcdFx0XHQ8dGQ+PHNwYW4+e3sgZHMucmlkZXIuZnVsbF9uYW1lIH19PC9zcGFuPjwvdGQ+XG5cdFx0XHRcdFx0XHQ8dGQ+PGEgQGNsaWNrPVwic2hvd1NjaGVkdWxlTW9kYWwoJ2VkaXQnLCBkcy5pZClcIj5FZGl0PC9hPiB8IDxhIEBjbGljaz1cIiRwYXJlbnQuc2hvd0RlbGV0ZVNjaGVkUHJvbXB0KGRzLmlkKVwiPkRlbGV0ZTwvYT48L3RkPiAgICAgICAgIFxuXHQgICAgICAgICAgICBcdDwvdHI+XG5cdCAgICAgICAgICAgIFx0ICAgIFxuXHQgICAgICAgICAgICA8L3Rib2R5PiAgICAgICAgICAgICAgICAgIFxuXHQgICAgPC90YWJsZT5cblxuXHQ8L2Rpdj5cblxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblx0XG5cdGV4cG9ydCBkZWZhdWx0IHtcblx0XHRkYXRhKCkge1xuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0YWN0aXZlQnV0dG9uOiAndG9kYXknLFxuXG5cdFx0XHRcdGRhdGVGaWx0ZXI6ICcnLFxuXG5cdFx0XHRcdGRlbGl2ZXJ5U2NoZWR1bGVzOiBbXVxuXHRcdFx0fVxuXHRcdH0sXG5cblx0XHRtZXRob2RzOiB7XG5cdFx0XHRmb3JtYXREYXRlUGlja2VyVmFsdWUodmFsdWUpIHtcblx0XHRcdFx0bGV0IGRhdGVQaWNrZXJWYWx1ZSA9IHZhbHVlLnNwbGl0KCcvJyk7XG5cblx0XHRcdFx0cmV0dXJuIGRhdGVQaWNrZXJWYWx1ZVsyXSArICctJyArIGRhdGVQaWNrZXJWYWx1ZVswXSArICctJyArIGRhdGVQaWNrZXJWYWx1ZVsxXTtcblx0XHRcdH0sXG5cblx0XHRcdHNob3dTY2hlZHVsZU1vZGFsKHZhbCwgaWQpe1xuXHQgICAgICAgICAgICBpZih2YWwgPT0gXCJhZGRcIil7XG5cdCAgICAgICAgICAgICAgICAkKCcjYWRkU2NoZWR1bGVNb2RhbCcpLm1vZGFsKCd0b2dnbGUnKTtcblx0ICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5tb2RlID0gXCJBZGRcIjtcblx0ICAgICAgICAgICAgfWVsc2UgaWYodmFsID09IFwiZWRpdFwiKXtcblx0ICAgICAgICAgICAgICAgIGF4aW9zLnBvc3QoJy92aXNhL2hvbWUvZ2V0U2NoZWQvJysgaWQpIFxuXHQgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuYWRkU2NoZWR1bGUgPSBuZXcgRm9ybSAoe1xuXHQgICAgICAgICAgICAgICAgICAgICAgICBpZCAgICAgICAgICAgICAgOnJlc3VsdC5kYXRhLmlkLFxuXHQgICAgICAgICAgICAgICAgICAgICAgICBpdGVtICAgICAgICAgICAgOnJlc3VsdC5kYXRhLml0ZW0sXG5cdCAgICAgICAgICAgICAgICAgICAgICAgIHJpZGVyX2lkICAgICAgICA6cmVzdWx0LmRhdGEucmlkZXJfaWQsXG5cdCAgICAgICAgICAgICAgICAgICAgICAgIGxvY2F0aW9uICAgICAgICA6cmVzdWx0LmRhdGEubG9jYXRpb24sXG5cdCAgICAgICAgICAgICAgICAgICAgICAgIHNjaGVkdWxlZF9kYXRlICA6dGhpcy5mb3JtYXREYXRlWU1EKHJlc3VsdC5kYXRhLnNjaGVkdWxlZF9kYXRlKSxcblx0ICAgICAgICAgICAgICAgICAgICAgICAgc2NoZWR1bGVkX3RpbWUgIDpyZXN1bHQuZGF0YS5zY2hlZHVsZWRfdGltZVxuXHQgICAgICAgICAgICAgICAgICAgIH0seyBiYXNlVVJMOiAnL3Zpc2EvaG9tZS8nfSk7XG5cdCAgICAgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LmR0ID0gcmVzdWx0LmRhdGEuc2NoZWR1bGVkX2RhdGU7XG5cdCAgICAgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LnNjaGVkdWxlZF9ob3VyID0gdGhpcy5nZXRIb3VyKHJlc3VsdC5kYXRhLnNjaGVkdWxlZF90aW1lKTtcblx0ICAgICAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuc2NoZWR1bGVkX21pbnV0ZSA9IHRoaXMuZ2V0TWludXRlKHJlc3VsdC5kYXRhLnNjaGVkdWxlZF90aW1lKTtcblx0ICAgICAgICAgICAgICAgICAgICAvLyQoJ2Zvcm0jZnJtX2RlbGl2ZXJ5c2NoZWQgaW5wdXRbbmFtZT1kYXRlXScpLnZhbChyZXN1bHQuZGF0YS5zY2hlZHVsZWRfZGF0ZSk7XG5cdCAgICAgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50Lm1vZGUgPSBcIkVkaXRcIjtcblx0ICAgICAgICAgICAgICAgIH0pO1xuXHQgICAgICAgICAgICAgICAgJCgnI2FkZFNjaGVkdWxlTW9kYWwnKS5tb2RhbCgndG9nZ2xlJyk7XG5cdCAgICAgICAgICAgIH1cblx0ICAgICAgICB9LFxuXG5cdCAgICAgICAgLy9ZWVlZLU1NLUREXG5cdFx0XHRmb3JtYXREYXRlWU1EKHZhbHVlKSB7XG5cdFx0XHRcdGxldCBkYXRlID0gdmFsdWUuc3BsaXQoJy8nKTtcblx0XHRcdFx0bGV0IG5ld0RhdGUgPSBkYXRlWzJdICsgJy0nICsgZGF0ZVswXSArICctJyArIGRhdGVbMV07XG5cblx0XHRcdFx0cmV0dXJuIG5ld0RhdGU7XG5cdFx0XHR9LFxuXG5cdFx0XHQvL21tL2RkL3l5eXlcblx0XHRcdGZvcm1hdERhdGVNRFkodmFsdWUpIHtcblx0XHRcdFx0bGV0IGRhdGUgPSB2YWx1ZS5zcGxpdCgnLScpO1xuXHRcdFx0XHRsZXQgbmV3RGF0ZSA9IGRhdGVbMV0gKyAnLycgKyBkYXRlWzJdICsgJy8nICsgZGF0ZVswXTtcblxuXHRcdFx0XHRyZXR1cm4gbmV3RGF0ZTtcblx0XHRcdH0sXG5cblx0XHRcdGFsd2F5c1R3b0RpZ2l0KHZhbHVlKXtcblx0XHRcdFx0aWYgKHZhbHVlLnRvU3RyaW5nKCkubGVuZ3RoID09IDEpIHtcblx0XHQgICAgICAgICAgICB2YWx1ZSA9IFwiMFwiICsgdmFsdWU7XG5cdFx0ICAgICAgICB9XG5cdFx0ICAgICAgICByZXR1cm4gdmFsdWU7XG5cdFx0XHR9LFxuXG5cdFx0XHRnZXRIb3VyKHZhbHVlKXtcblx0XHRcdFx0bGV0IHRpbWUgPSB2YWx1ZS5zcGxpdCgnOicpO1xuXHRcdCAgICAgICAgcmV0dXJuIHRpbWVbMF07XG5cdFx0XHR9LFxuXG5cdFx0XHRnZXRNaW51dGUodmFsdWUpe1xuXHRcdFx0XHRsZXQgdGltZSA9IHZhbHVlLnNwbGl0KCc6Jyk7XG5cdFx0ICAgICAgICByZXR1cm4gdGltZVsxXTtcblx0XHRcdH0sXG5cblx0XHRcdGZpbHRlclNjaGVkKGRhdGUpIHtcblx0XHRcdFx0aWYoZGF0ZSA9PSAnZm9ybScpIHtcblx0XHRcdFx0XHRsZXQgZGF0ZVBpY2tlclZhbHVlID0gJCgnZm9ybSNkZWxpdmVyeS1zY2hlZC1mb3JtIGlucHV0W25hbWU9ZGVsaXZlcnktc2NoZWQtZGF0ZV0nKS52YWwoKTtcblxuXHRcdFx0XHRcdGlmKGRhdGVQaWNrZXJWYWx1ZSA9PSAnJykge1xuXHRcdFx0XHRcdFx0ZGF0ZVBpY2tlclZhbHVlID0gbW9tZW50KCkuZm9ybWF0KCdNTS9ERC9ZWVlZJyk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdGRhdGVQaWNrZXJWYWx1ZSA9IGRhdGVQaWNrZXJWYWx1ZTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHR0aGlzLmRhdGVGaWx0ZXIgPSBkYXRlUGlja2VyVmFsdWU7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0dGhpcy5hY3RpdmVCdXR0b24gPSBkYXRlO1xuXG5cdFx0XHRcdFx0aWYoZGF0ZSA9PSAndG9kYXknKVxuXHRcdFx0XHRcdFx0dGhpcy5kYXRlRmlsdGVyID0gbW9tZW50KCkuZm9ybWF0KCdNTS9ERC9ZWVlZJyk7XG5cdFx0XHRcdFx0ZWxzZSBpZihkYXRlID09ICd0b21vcnJvdycpXG5cdFx0XHRcdFx0XHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQobmV3IERhdGUoKSkuYWRkKCsxLCdkYXlzJykuZm9ybWF0KCdNTS9ERC9ZWVlZJyk7XG5cblx0XHRcdFx0XHQkKCdmb3JtI2RlbGl2ZXJ5LXNjaGVkLWZvcm0gaW5wdXRbbmFtZT1kZWxpdmVyeS1zY2hlZC1kYXRlXScpLnZhbCh0aGlzLmRhdGVGaWx0ZXIpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9ob21lL2dldFNjaGVkdWxlc2J5RGF0ZScsIHtcblx0ICAgICAgICAgICAgICAgIHBhcmFtczoge1xuXHQgICAgICAgICAgICAgICAgXHRkYXRlRmlsdGVyOiB0aGlzLmRhdGVGaWx0ZXJcblx0ICAgICAgICAgICAgICAgIH1cblx0ICAgICAgICAgICAgfSlcblx0ICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHQgICAgICAgICAgICBcdGxldCB7IHN1Y2Nlc3MsIHNjaGVkdWxlcyB9ID0gcmVzcG9uc2UuZGF0YTtcblxuXHQgICAgICAgICAgICBcdGlmKHN1Y2Nlc3MpIHtcblx0ICAgICAgICAgICAgXHRcdCQoJyNkZWxpdmVyeVNjaGVkdWxlJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuXG5cdCAgICAgICAgICAgIFx0XHR0aGlzLmRlbGl2ZXJ5U2NoZWR1bGVzID0gc2NoZWR1bGVzO1xuXG5cdCAgICAgICAgICAgIFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0ICAgICAgICAgICAgXHRcdFx0JCgnI2RlbGl2ZXJ5U2NoZWR1bGUnKS5EYXRhVGFibGUoe1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgYXV0b1dpZHRoOiBmYWxzZSxcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcImNvbHVtbkRlZnNcIjogW1xuXHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgXCJ3aWR0aFwiOiBcIjI1JVwiLCBcInRhcmdldHNcIjogMSB9XG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICBdLFxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFwibGVuZ3RoTWVudVwiOiBbWzEwLCAyNSwgNTBdLCBbMTAsIDI1LCA1MF1dLFxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFwiaURpc3BsYXlMZW5ndGhcIjogMTBcblx0XHQgICAgICAgICAgICAgICAgICAgIH0pO1xuXG5cdFx0ICAgICAgICAgICAgICAgICAgICAkKGZ1bmN0aW9uICgpIHtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgXHQkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykudG9vbHRpcCgpXG5cdFx0ICAgICAgICAgICAgICAgICAgICB9KTtcblx0ICAgICAgICAgICAgXHRcdH0sIDEwMDApO1xuXHQgICAgICAgICAgICBcdH1cblx0ICAgICAgICAgICAgfSk7XG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdGNyZWF0ZWQoKSB7XG5cdFx0XHR0aGlzLmZpbHRlclNjaGVkKCd0b2RheScpO1xuXHRcdH0sXG5cblx0XHRtb3VudGVkKCkge1xuXHRcdFx0JCgnI2RlbGl2ZXJ5LXNjaGVkcy1kYXRlcGlja2VyJykuZGF0ZXBpY2tlcih7XG4gICAgICAgICAgICAgICAgZm9ybWF0OidtbS9kZC95eXl5JyxcbiAgICAgICAgICAgICAgICB0b2RheUhpZ2hsaWdodDogdHJ1ZVxuICAgICAgICAgICAgfSk7XG5cdFx0fVxuXHR9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIGJ1dHRvbi5idG4tZmlsdGVyIHtcblx0XHRmbG9hdDogcmlnaHQ7XG5cdFx0bGluZS1oZWlnaHQ6IDEuMjsgXG5cdFx0bWFyZ2luLXRvcDogNXB4O1xuXHR9XG48L3N0eWxlPlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBEZWxpdmVyeVNjaGVkdWxlLnZ1ZT9mNDNjOTEyNiIsIjx0ZW1wbGF0ZT5cblx0XG5cdDxkaXY+XG5cdFx0XG5cdFx0PGZvcm0gaWQ9XCJ0b2RheXMtc2VydmljZXMtZm9ybVwiIGNsYXNzPVwiZm9ybS1pbmxpbmVcIiBAc3VibWl0LnByZXZlbnQ9XCJmaWx0ZXJTZXJ2aWNlcygnZm9ybScpXCI+XG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZGF0ZXJhbmdlIGlucHV0LWdyb3VwXCIgaWQ9XCJ0b2RheXMtc2VydmljZXMtZGF0ZXBpY2tlclwiPlxuXHRcdCAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaW5wdXQtc20gZm9ybS1jb250cm9sXCIgbmFtZT1cInRvZGF5cy1zZXJ2aWNlLWRhdGVcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPlxuXHRcdCAgICAgICAgPC9kaXY+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdCAgICBcblx0XHQgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHQgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5XCIgc3R5bGU9XCJsaW5lLWhlaWdodDogMS4yO1wiPlxuXHRcdCAgICAgICAgICAgIDxzdHJvbmc+RmlsdGVyPC9zdHJvbmc+XG5cdFx0ICAgICAgICA8L2J1dHRvbj5cblx0XHQgICAgPC9kaXY+XG5cblx0XHRcdDxidXR0b24gQGNsaWNrPVwiZmlsdGVyU2VydmljZXMoJ3RvZGF5JylcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXCIgOmNsYXNzPVwieydidG4tb3V0bGluZScgOiBhY3RpdmVCdXR0b24hPSd0b2RheSd9XCIgc3R5bGU9XCJtYXJnaW4tcmlnaHQ6IDJweDtcIiB0eXBlPVwiYnV0dG9uXCI+VG9kYXk8L2J1dHRvbj5cblxuXHRcdFx0PGJ1dHRvbiBAY2xpY2s9XCJmaWx0ZXJTZXJ2aWNlcygneWVzdGVyZGF5JylcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXCIgOmNsYXNzPVwieydidG4tb3V0bGluZScgOiBhY3RpdmVCdXR0b24hPSd5ZXN0ZXJkYXknfVwiIHN0eWxlPVwibWFyZ2luLXJpZ2h0OiAycHg7XCIgdHlwZT1cImJ1dHRvblwiPlllc3RlcmRheTwvYnV0dG9uPlxuXHRcdDwvZm9ybT5cblxuXHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cblxuICAgIFx0PHRhYmxlIGlkPVwidG9kYXlzU2VydmljZXNUYWJsZVwiIGNsYXNzPVwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJlZCB0YWJsZS1ob3ZlciBkYXRhVGFibGVzLWV4YW1wbGVcIj5cblx0XHQgICAgPHRoZWFkPlxuXHRcdCAgICAgICAgPHRyPlxuICAgICAgICAgICAgICAgIDx0aD5EYXRlPC90aD5cblx0XHRcdFx0PHRoPk5hbWU8L3RoPlxuICAgICAgICAgICAgICAgIDx0aD5JRDwvdGg+XG4gICAgICAgICAgICAgICAgPHRoPlBhY2thZ2U8L3RoPlxuICAgICAgICAgICAgICAgIDx0aCBzdHlsZT1cIndpZHRoOjMwJSAhaW1wb3J0YW50XCI+RGV0YWlsPC90aD5cbiAgICAgICAgICAgICAgICA8dGg+Q29zdDwvdGg+XG4gICAgICAgICAgICAgICAgPHRoPkNoYXJnZTwvdGg+XG5cdFx0XHRcdDx0aD5UaXA8L3RoPlxuXHRcdFx0XHQ8dGggc3R5bGU9XCJ3aWR0aDo1JVwiPkFjdGlvbjwvdGg+XG5cdFx0ICAgICAgICA8L3RyPlxuXHRcdCAgICA8L3RoZWFkPlxuXHRcdCAgICA8dGJvZHk+XG5cdFx0ICAgICAgICA8dHIgdi1mb3I9XCJzZXJ2aWNlIGluIGZpbFNlcnZpY2VzXCI+XG5cdFx0ICAgICAgIFx0XHQ8dGQ+e3sgc2VydmljZS5zZXJ2aWNlX2RhdGUgfX08L3RkPlxuXHRcdCAgICAgICBcdFx0PHRkPjxzcGFuIHYtaWY9XCJzZXJ2aWNlLnVzZXIhPW51bGxcIj57eyBzZXJ2aWNlLnVzZXIuZnVsbF9uYW1lIH19PC9zcGFuPlxuXHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJwdWxsLXJpZ2h0XCIgaHJlZj1cIiNcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInRvcFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIDpkYXRhLWNvbnRlbnQ9XCJzZXJ2aWNlLnVzZXIuZ3JvdXBfYmluZGVkXCIgZGF0YS10cmlnZ2VyPVwiaG92ZXJcIiB2LWlmPVwic2VydmljZS5ncm91cF9iaW5kZWQgIT0gJydcIj5cblx0XHRcdFx0XHRcdFx0PGIgY2xhc3M9XCJmYSBmYS11c2VyLWNpcmNsZSBncm91cC1pY29uIG0tci0yXCI+PC9iPlxuXHRcdFx0XHRcdFx0PC9hPlxuXHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJwdWxsLXJpZ2h0XCIgaHJlZj1cIiNcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInRvcFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIDpkYXRhLWNvbnRlbnQ9XCInVW5yZWFkIE5vdGlmaWNhdGlvbnMgOiAnK3NlcnZpY2UudXNlci51bnJlYWRfbm90aWZcIiBkYXRhLXRyaWdnZXI9XCJob3ZlclwiICB2LWlmPVwic2VydmljZS51c2VyLnVucmVhZF9ub3RpZiA+IDBcIj5cblx0XHRcdFx0XHRcdFx0PGIgY2xhc3M9XCJmYSBmYS1lbnZlbG9wZSB1bnJlYWQtaWNvbiBtLXItMVwiPjwvYj5cblx0XHRcdFx0XHRcdDwvYT5cblx0XHRcdFx0XHRcdDxhIGNsYXNzPVwicHVsbC1yaWdodFwiIGhyZWY9XCIjXCIgZGF0YS10b2dnbGU9XCJwb3BvdmVyXCIgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIiBkYXRhLWNvbnRhaW5lcj1cImJvZHlcIiBkYXRhLWNvbnRlbnQ9XCJBcHAgSW5zdGFsbGVkIGJ5IENsaWVudFwiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCIgIHYtaWY9XCJzZXJ2aWNlLnVzZXIuYmluZGVkXCI+XG5cdFx0XHRcdFx0XHRcdDxiIGNsYXNzPVwiZmEgZmEtbW9iaWxlIGFwcC1pY29uIG0tci0xXCI+PC9iPlxuXHRcdFx0XHRcdFx0PC9hPlxuXHRcdFx0XHRcdDwvdGQ+XG5cdFx0XHRcdFx0PHRkPjxzcGFuIHYtaWY9XCJzZXJ2aWNlLnVzZXIhPW51bGxcIj57eyBzZXJ2aWNlLnVzZXIuaWQgfX08L3NwYW4+PC90ZD5cblx0XHRcdFx0XHQ8dGQ+e3sgc2VydmljZS50cmFja2luZyB9fTwvdGQ+XG5cdFx0XHRcdFx0PHRkIHN0eWxlPVwid2lkdGg6MzAlICFpbXBvcnRhbnRcIj5cblx0XHRcdFx0XHRcdDxhIGhyZWY9XCIjXCIgdi1pZj1cInNlcnZpY2UucmVtYXJrcyE9JydcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInJpZ2h0XCIgZGF0YS1jb250YWluZXI9XCJib2R5XCIgOmRhdGEtY29udGVudD1cInNlcnZpY2UucmVtYXJrc1wiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCIgPlxuXHRcdFx0ICAgICAgICAgICAgICB7eyBzZXJ2aWNlLmRldGFpbCB9fVxuXHRcdFx0ICAgICAgICAgICAgPC9hPlxuXHRcdFx0ICAgICAgICAgICAgPHNwYW4gdi1lbHNlPnt7IHNlcnZpY2UuZGV0YWlsIH19PC9zcGFuPlxuXHRcdFx0ICAgICAgICA8L3RkPlxuXHRcdFx0XHRcdDx0ZD57eyBzZXJ2aWNlLmNvc3QgfX08L3RkPlxuXHRcdFx0XHRcdDx0ZD57eyBzZXJ2aWNlLmNoYXJnZSB9fTwvdGQ+XG5cdFx0XHRcdFx0PHRkPnt7IHNlcnZpY2UudGlwIH19PC90ZD5cblx0XHRcdFx0XHQ8dGQgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPlxuXHRcdFx0XHRcdFx0PGEgaHJlZj1cIiNcIiBkYXRhLXRvZ2dsZT1cIm1vZGFsXCIgZGF0YS10YXJnZXQ9XCIjZWRpdFNlcnZpY2VNb2RhbFwiIEBjbGljaz1cIiRwYXJlbnQuc2V0U2VydmljZShzZXJ2aWNlKVwiIHYtY2xvYWs+XG5cdFx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtcGVuY2lsLXNxdWFyZS1vXCI+PC9pPlxuXHRcdFx0XHRcdFx0PC9hPlxuXHRcdFx0XHRcdFx0PGEgOmhyZWY9XCInL3Zpc2EvY2xpZW50Lycrc2VydmljZS5jbGllbnRfaWRcIiB0YXJnZXQ9XCJfYmxhbmtcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiBkYXRhLXBsYWNlbWVudD1cImxlZnRcIiB0aXRsZT1cIlZpZXcgY2xpZW50IHByb2ZpbGVcIj48aSBjbGFzcz1cImZhIGZhLWFycm93LXJpZ2h0XCI+PC9pPjwvYT5cblx0XHRcdFx0XHQ8L3RkPlx0XG5cdFx0ICAgICAgICA8L3RyPlxuXHRcdCAgICA8L3Rib2R5PlxuXHRcdDwvdGFibGU+XG5cblx0PC9kaXY+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5cdFxuXHRleHBvcnQgZGVmYXVsdCB7XG5cdFx0ZGF0YSgpIHtcblx0XHRcdHJldHVybiB7XG5cdFx0XHRcdGFjdGl2ZUJ1dHRvbjogJ3RvZGF5JyxcblxuXHRcdFx0XHRkYXRlRmlsdGVyOiAnJyxcblxuXHRcdFx0XHRmaWxTZXJ2aWNlczogW11cblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0bWV0aG9kczoge1xuXHRcdFx0Zm9ybWF0RGF0ZVBpY2tlclZhbHVlKHZhbHVlKSB7XG5cdFx0XHRcdGxldCBkYXRlUGlja2VyVmFsdWUgPSB2YWx1ZS5zcGxpdCgnLycpO1xuXG5cdFx0XHRcdHJldHVybiBkYXRlUGlja2VyVmFsdWVbMl0gKyAnLScgKyBkYXRlUGlja2VyVmFsdWVbMF0gKyAnLScgKyBkYXRlUGlja2VyVmFsdWVbMV07XG5cdFx0XHR9LFxuXG5cdFx0XHRmb3JtYXREYXRlKHZhbHVlKSB7XG5cdFx0XHRcdGxldCBkYXRlID0gdmFsdWUuc3BsaXQoJy0nKTtcblx0XHRcdFx0bGV0IG5ld0RhdGUgPSBkYXRlWzFdICsgJy8nICsgZGF0ZVsyXSArICcvJyArIGRhdGVbMF07XG5cblx0XHRcdFx0cmV0dXJuIG5ld0RhdGU7XG5cdFx0XHR9LFxuXG5cdFx0XHRmaWx0ZXJTZXJ2aWNlcyhkYXRlKSB7XG5cdFx0XHRcdGlmKGRhdGUgPT0gJ2Zvcm0nKSB7XG5cdFx0XHRcdFx0bGV0IGRhdGVQaWNrZXJWYWx1ZSA9ICQoJ2Zvcm0jdG9kYXlzLXNlcnZpY2VzLWZvcm0gaW5wdXRbbmFtZT10b2RheXMtc2VydmljZS1kYXRlXScpLnZhbCgpO1xuXG5cdFx0XHRcdFx0aWYoZGF0ZVBpY2tlclZhbHVlID09ICcnKSB7XG5cdFx0XHRcdFx0XHRkYXRlUGlja2VyVmFsdWUgPSBtb21lbnQoKS5mb3JtYXQoJ01NL0REL1lZWVknKTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0ZGF0ZVBpY2tlclZhbHVlID0gZGF0ZVBpY2tlclZhbHVlO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHRoaXMuZGF0ZUZpbHRlciA9IGRhdGVQaWNrZXJWYWx1ZTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR0aGlzLmFjdGl2ZUJ1dHRvbiA9IGRhdGU7XG5cblx0XHRcdFx0XHRpZihkYXRlID09ICd0b2RheScpXG5cdFx0XHRcdFx0XHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQoKS5mb3JtYXQoJ01NL0REL1lZWVknKTtcblx0XHRcdFx0XHRlbHNlIGlmKGRhdGUgPT0gJ3llc3RlcmRheScpXG5cdFx0XHRcdFx0XHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQobmV3IERhdGUoKSkuYWRkKC0xLCdkYXlzJykuZm9ybWF0KCdNTS9ERC9ZWVlZJyk7XG5cblx0XHRcdFx0XHQkKCdmb3JtI3RvZGF5cy1zZXJ2aWNlcy1mb3JtIGlucHV0W25hbWU9dG9kYXlzLXNlcnZpY2UtZGF0ZV0nKS52YWwodGhpcy5kYXRlRmlsdGVyKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvaG9tZS9nZXQtc2VydmljZS1ieURhdGUnLCB7XG5cdCAgICAgICAgICAgICAgICBwYXJhbXM6IHtcblx0ICAgICAgICAgICAgICAgIFx0ZGF0ZUZpbHRlcjogdGhpcy5kYXRlRmlsdGVyXG5cdCAgICAgICAgICAgICAgICB9XG5cdCAgICAgICAgICAgIH0pXG5cdCAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcblx0ICAgICAgICAgICAgXHRsZXQgeyBzdWNjZXNzLCBzZXJ2aWNlcyB9ID0gcmVzcG9uc2UuZGF0YTtcblxuXHQgICAgICAgICAgICBcdGlmKHN1Y2Nlc3MpIHtcblx0ICAgICAgICAgICAgXHRcdCQoJyN0b2RheXNTZXJ2aWNlc1RhYmxlJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuXG5cdCAgICAgICAgICAgIFx0XHR0aGlzLmZpbFNlcnZpY2VzID0gc2VydmljZXM7XG5cblx0ICAgICAgICAgICAgXHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHQgICAgICAgICAgICBcdFx0XHQkKCcjdG9kYXlzU2VydmljZXNUYWJsZScpLkRhdGFUYWJsZSh7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBhdXRvV2lkdGg6IGZhbHNlLFxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIFwiY29sdW1uRGVmc1wiOiBbXG5cdFx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBcIndpZHRoXCI6IFwiMjUlXCIsIFwidGFyZ2V0c1wiOiAxIH1cblx0XHRcdCAgICAgICAgICAgICAgICAgICAgICAgIF0sXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXCJsZW5ndGhNZW51XCI6IFtbMTAsIDI1LCA1MF0sIFsxMCwgMjUsIDUwXV0sXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAxMFxuXHRcdCAgICAgICAgICAgICAgICAgICAgfSk7XG5cblx0XHQgICAgICAgICAgICAgICAgICAgICQoZnVuY3Rpb24gKCkge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICBcdCQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKClcblx0XHQgICAgICAgICAgICAgICAgICAgIH0pO1xuXHQgICAgICAgICAgICBcdFx0fSwgMTAwMCk7XG5cdCAgICAgICAgICAgIFx0fVxuXHQgICAgICAgICAgICB9KTtcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0Y3JlYXRlZCgpIHtcblx0XHRcdHRoaXMuZmlsdGVyU2VydmljZXMoJ3RvZGF5Jyk7XG5cdFx0fSxcblxuXHRcdG1vdW50ZWQoKSB7XG5cdFx0XHQkKCcjdG9kYXlzLXNlcnZpY2VzLWRhdGVwaWNrZXInKS5kYXRlcGlja2VyKHtcbiAgICAgICAgICAgICAgICBmb3JtYXQ6J21tL2RkL3l5eXknLFxuICAgICAgICAgICAgICAgIHRvZGF5SGlnaGxpZ2h0OiB0cnVlXG4gICAgICAgICAgICB9KTtcblx0XHR9XG5cdH1cblxuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG5cdGZvcm0gYnV0dG9uLmJ0bi1maWx0ZXIge1xuXHRcdGZsb2F0OiByaWdodDtcblx0XHRsaW5lLWhlaWdodDogMS4yOyBcblx0XHRtYXJnaW4tdG9wOiA1cHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFRvZGF5c1NlcnZpY2VzLnZ1ZT8yNzk1ODg5NyIsIjx0ZW1wbGF0ZT5cblx0XG5cdDxkaXY+XG5cdFx0XG5cdFx0PGZvcm0gaWQ9XCJ0b2RheXMtdGFza3MtZm9ybVwiIGNsYXNzPVwiZm9ybS1pbmxpbmVcIiBAc3VibWl0LnByZXZlbnQ9XCJmaWx0ZXJUYXNrcygnZm9ybScpXCI+XG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZGF0ZXJhbmdlIGlucHV0LWdyb3VwXCIgaWQ9XCJ0b2RheXMtdGFzay1kYXRlcGlja2VyXCI+XG5cdFx0ICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2xcIiBuYW1lPVwidG9kYXlzLXRhc2stZGF0ZVwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG5cdFx0ICAgICAgICA8L2Rpdj5cblx0XHQgICAgPC9kaXY+XG5cdFx0ICAgIFxuXHRcdCAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdCAgICAgICAgPGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiBzdHlsZT1cImxpbmUtaGVpZ2h0OiAxLjI7XCI+XG5cdFx0ICAgICAgICAgICAgPHN0cm9uZz5GaWx0ZXI8L3N0cm9uZz5cblx0XHQgICAgICAgIDwvYnV0dG9uPlxuXHRcdCAgICA8L2Rpdj5cblxuXHRcdCAgICA8YnV0dG9uIEBjbGljaz1cImZpbHRlclRhc2tzKCd0b21vcnJvdycpXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclwiIDpjbGFzcz1cInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0ndG9tb3Jyb3cnfVwiIHR5cGU9XCJidXR0b25cIj5Ub21vcnJvdzwvYnV0dG9uPlxuXG5cdFx0XHQ8YnV0dG9uIEBjbGljaz1cImZpbHRlclRhc2tzKCd0b2RheScpXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclwiIDpjbGFzcz1cInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0ndG9kYXknfVwiIHN0eWxlPVwibWFyZ2luLXJpZ2h0OiAycHg7XCIgdHlwZT1cImJ1dHRvblwiPlRvZGF5PC9idXR0b24+XG5cblx0XHRcdDxidXR0b24gQGNsaWNrPVwiZmlsdGVyVGFza3MoJ3llc3RlcmRheScpXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclwiIDpjbGFzcz1cInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0neWVzdGVyZGF5J31cIiBzdHlsZT1cIm1hcmdpbi1yaWdodDogMnB4O1wiIHR5cGU9XCJidXR0b25cIj5ZZXN0ZXJkYXk8L2J1dHRvbj5cblx0XHQ8L2Zvcm0+XG5cblx0XHQ8ZGl2IHN0eWxlPVwiaGVpZ2h0OjIwcHg7IGNsZWFyOmJvdGg7XCI+PC9kaXY+XG5cbiAgICBcdDx0YWJsZSBpZD1cInRvZGF5c1Rhc2tzTGlzdHNcIiBjbGFzcz1cInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVyZWQgdGFibGUtaG92ZXIgZGF0YVRhYmxlcy1leGFtcGxlXCI+XG5cdFx0ICAgIDx0aGVhZD5cblx0XHQgICAgICAgIDx0cj5cblx0XHQgICAgICAgICAgICA8dGg+TmFtZTwvdGg+XG5cdFx0ICAgICAgICAgICAgPHRoPkNsaWVudCBOby48L3RoPlxuXHRcdCAgICAgICAgICAgIDx0aD5TZXJ2aWNlPC90aD5cblx0XHQgICAgICAgICAgICA8dGg+RXN0aW1hdGVkIENvc3Q8L3RoPlxuXHRcdCAgICAgICAgICAgIDx0aD5TdGF0dXM8L3RoPlxuXHRcdCAgICAgICAgICAgIDx0aD5GaWxpbmcgRGF0ZTwvdGg+XG5cdFx0ICAgICAgICAgICAgPHRoPlJlbGVhc2luZyBEYXRlPC90aD5cblx0XHQgICAgICAgIDwvdHI+XG5cdFx0ICAgIDwvdGhlYWQ+XG5cdFx0ICAgIDx0Ym9keT5cblx0XHQgICAgICAgIDx0ciB2LWZvcj1cInRhc2sgaW4gdGFza3NcIj5cblx0XHQgICAgICAgXHRcdDx0ZD5cblx0XHQgICAgICAgXHRcdFx0PGEgOmhyZWY9XCInL3Zpc2EvY2xpZW50LycgKyB0YXNrLmNsaWVudF9zZXJ2aWNlLnVzZXIuaWRcIiB0YXJnZXQ9XCJfYmxhbmtcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiBkYXRhLXBsYWNlbWVudD1cInJpZ2h0XCIgdGl0bGU9XCJWaWV3IGNsaWVudCBwcm9maWxlXCI+XG5cdFx0ICAgICAgIFx0XHRcdFx0e3sgdGFzay5jbGllbnRfc2VydmljZS51c2VyLmZ1bGxfbmFtZSB9fVxuXHRcdCAgICAgICBcdFx0XHQ8L2E+XG5cdFx0ICAgICAgIFx0XHQ8L3RkPlxuXHRcdCAgICAgICBcdFx0PHRkPnt7IHRhc2suY2xpZW50X3NlcnZpY2UudXNlci5pZCB9fTwvdGQ+XG5cdFx0ICAgICAgIFx0XHQ8dGQ+e3sgdGFzay5jbGllbnRfc2VydmljZS5zZXJ2aWNlX2NhdGVnb3J5LmRldGFpbCB9fTwvdGQ+XG5cdFx0ICAgICAgIFx0XHQ8dGQ+XG5cdFx0ICAgICAgIFx0XHRcdHt7IHRhc2suY2xpZW50X3NlcnZpY2Uuc2VydmljZV9jYXRlZ29yeS5lc3RpbWF0ZWRfY29zdCB9fVxuXHRcdCAgICAgICBcdFx0PC90ZD5cblx0XHQgICAgICAgXHRcdDx0ZD57eyB0YXNrLnN0YXR1cyB9fTwvdGQ+XG5cdFx0ICAgICAgIFx0XHQ8dGQ+e3sgdGFzay5maWxpbmdfZGF0ZSB9fTwvdGQ+XG5cdFx0ICAgICAgIFx0XHQ8dGQ+e3sgdGFzay5yZWxlYXNpbmdfZGF0ZSB9fTwvdGQ+XG5cdFx0ICAgICAgICA8L3RyPlxuXHRcdCAgICA8L3Rib2R5PlxuXHRcdCAgICA8dGZvb3Q+XG5cdCAgICAgICAgICAgIDx0cj5cblx0ICAgICAgICAgICAgICAgIDx0aCBjb2xzcGFuPVwiN1wiIHN0eWxlPVwidGV4dC1hbGlnbjpyaWdodFwiPjwvdGg+XG5cdCAgICAgICAgICAgIDwvdHI+XG5cdCAgICAgICAgPC90Zm9vdD5cblx0XHQ8L3RhYmxlPlxuXG5cdDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXHRcblx0ZXhwb3J0IGRlZmF1bHQge1xuXHRcdGRhdGEoKSB7XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRhY3RpdmVCdXR0b246ICd0b2RheScsXG5cblx0XHRcdFx0ZGF0ZUZpbHRlcjogJycsXG5cblx0XHRcdFx0dGFza3M6IFtdXG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdG1ldGhvZHM6IHtcblx0XHRcdGZvcm1hdERhdGVQaWNrZXJWYWx1ZSh2YWx1ZSkge1xuXHRcdFx0XHRsZXQgZGF0ZVBpY2tlclZhbHVlID0gdmFsdWUuc3BsaXQoJy8nKTtcblxuXHRcdFx0XHRyZXR1cm4gZGF0ZVBpY2tlclZhbHVlWzJdICsgJy0nICsgZGF0ZVBpY2tlclZhbHVlWzBdICsgJy0nICsgZGF0ZVBpY2tlclZhbHVlWzFdO1xuXHRcdFx0fSxcblxuXHRcdFx0Zm9ybWF0RGF0ZSh2YWx1ZSkge1xuXHRcdFx0XHRsZXQgZGF0ZSA9IHZhbHVlLnNwbGl0KCctJyk7XG5cdFx0XHRcdGxldCBuZXdEYXRlID0gZGF0ZVsxXSArICcvJyArIGRhdGVbMl0gKyAnLycgKyBkYXRlWzBdO1xuXG5cdFx0XHRcdHJldHVybiBuZXdEYXRlO1xuXHRcdFx0fSxcblxuXHRcdFx0ZmlsdGVyVGFza3MoZGF0ZSkge1xuXHRcdFx0XHRpZihkYXRlID09ICdmb3JtJykge1xuXHRcdFx0XHRcdGxldCBkYXRlUGlja2VyVmFsdWUgPSAkKCdmb3JtI3RvZGF5cy10YXNrcy1mb3JtIGlucHV0W25hbWU9dG9kYXlzLXRhc2stZGF0ZV0nKS52YWwoKTtcblxuXHRcdFx0XHRcdGlmKGRhdGVQaWNrZXJWYWx1ZSA9PSAnJykge1xuXHRcdFx0XHRcdFx0ZGF0ZVBpY2tlclZhbHVlID0gbW9tZW50KCkuZm9ybWF0KCdZWVlZLU1NLUREJyk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdGRhdGVQaWNrZXJWYWx1ZSA9IHRoaXMuZm9ybWF0RGF0ZVBpY2tlclZhbHVlKGRhdGVQaWNrZXJWYWx1ZSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0dGhpcy5kYXRlRmlsdGVyID0gZGF0ZVBpY2tlclZhbHVlO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHRoaXMuYWN0aXZlQnV0dG9uID0gZGF0ZTtcblxuXHRcdFx0XHRcdGlmKGRhdGUgPT0gJ3RvbW9ycm93Jylcblx0XHRcdFx0XHRcdHRoaXMuZGF0ZUZpbHRlciA9IG1vbWVudChuZXcgRGF0ZSgpKS5hZGQoMSwnZGF5JykuZm9ybWF0KCdZWVlZLU1NLUREJyk7XG5cdFx0XHRcdFx0ZWxzZSBpZihkYXRlID09ICd0b2RheScpXG5cdFx0XHRcdFx0XHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQoKS5mb3JtYXQoJ1lZWVktTU0tREQnKTtcblx0XHRcdFx0XHRlbHNlIGlmKGRhdGUgPT0gJ3llc3RlcmRheScpXG5cdFx0XHRcdFx0XHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQobmV3IERhdGUoKSkuYWRkKC0xLCdkYXlzJykuZm9ybWF0KCdZWVlZLU1NLUREJyk7XG5cblx0XHRcdFx0XHQkKCdmb3JtI3RvZGF5cy10YXNrcy1mb3JtIGlucHV0W25hbWU9dG9kYXlzLXRhc2stZGF0ZV0nKS52YWwodGhpcy5mb3JtYXREYXRlKHRoaXMuZGF0ZUZpbHRlcikpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9ob21lL2dldC10b2RheXMtdGFza3MnLCB7XG5cdCAgICAgICAgICAgICAgICBwYXJhbXM6IHtcblx0ICAgICAgICAgICAgICAgIFx0ZGF0ZUZpbHRlcjogdGhpcy5kYXRlRmlsdGVyXG5cdCAgICAgICAgICAgICAgICB9XG5cdCAgICAgICAgICAgIH0pXG5cdCAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcblx0ICAgICAgICAgICAgXHRsZXQgeyBzdWNjZXNzLCB0YXNrcyB9ID0gcmVzcG9uc2UuZGF0YTtcblxuXHQgICAgICAgICAgICBcdGlmKHN1Y2Nlc3MpIHtcblx0ICAgICAgICAgICAgXHRcdCQoJyN0b2RheXNUYXNrc0xpc3RzJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuXG5cdCAgICAgICAgICAgIFx0XHR0aGlzLnRhc2tzID0gdGFza3M7XG5cblx0ICAgICAgICAgICAgXHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHQgICAgICAgICAgICBcdFx0XHQkKCcjdG9kYXlzVGFza3NMaXN0cycpLkRhdGFUYWJsZSh7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwXSwgWzEwLCAyNSwgNTBdXSxcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICBcImlEaXNwbGF5TGVuZ3RoXCI6IDEwLFxuXG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXCJmb290ZXJDYWxsYmFja1wiOiBmdW5jdGlvbiAoIHJvdywgZGF0YSwgc3RhcnQsIGVuZCwgZGlzcGxheSApIHtcblx0XHRcdFx0XHRcdCAgICAgICAgICAgIHZhciBhcGkgPSB0aGlzLmFwaSgpLCBkYXRhO1xuXHRcdFx0XHRcdFx0IFxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgdmFyIGludFZhbCA9IGZ1bmN0aW9uICggaSApIHtcblx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICByZXR1cm4gdHlwZW9mIGkgPT09ICdzdHJpbmcnID9cblx0XHRcdFx0XHRcdCAgICAgICAgICAgICAgICAgICAgaS5yZXBsYWNlKC9bXFwkLF0vZywgJycpKjEgOlxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICB0eXBlb2YgaSA9PT0gJ251bWJlcicgP1xuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgICAgICAgICAgaSA6IDA7XG5cdFx0XHRcdFx0XHQgICAgICAgICAgICB9O1xuXHRcdFx0XHRcdFx0IFxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgdmFyIHRvdGFsID0gYXBpXG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgLmNvbHVtbiggMyApXG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgLmRhdGEoKVxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgIC5yZWR1Y2UoIGZ1bmN0aW9uIChhLCBiKSB7XG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgICAgIHJldHVybiBpbnRWYWwoYSkgKyBpbnRWYWwoYik7XG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgfSwgMCApO1xuXHRcdFx0XHRcdFx0IFxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgdmFyIHBhZ2VUb3RhbCA9IGFwaVxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgIC5jb2x1bW4oIDMsIHsgcGFnZTogJ2N1cnJlbnQnfSApXG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgLmRhdGEoKVxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgICAgIC5yZWR1Y2UoIGZ1bmN0aW9uIChhLCBiKSB7XG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgICAgIHJldHVybiBpbnRWYWwoYSkgKyBpbnRWYWwoYik7XG5cdFx0XHRcdFx0XHQgICAgICAgICAgICAgICAgfSwgMCApO1xuXHRcdFx0XHRcdFx0IFxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgLy8gVXBkYXRlIGZvb3RlclxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgJCggYXBpLmNvbHVtbiggMCApLmZvb3RlcigpICkuaHRtbChcblx0XHRcdFx0XHRcdCAgICAgICAgICAgIFx0J1RvdGFsIEVzdGltYXRlZCBDb3N0OiAmI3gyMGIxOycgKyB0b3RhbFxuXHRcdFx0XHRcdFx0ICAgICAgICAgICAgKTtcblx0XHRcdFx0XHRcdCAgICAgICAgfVxuXHRcdCAgICAgICAgICAgICAgICAgICAgfSk7XG5cblx0XHQgICAgICAgICAgICAgICAgICAgICQoZnVuY3Rpb24gKCkge1xuXHRcdCAgICAgICAgICAgICAgICAgICAgICBcdCQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKClcblx0XHQgICAgICAgICAgICAgICAgICAgIH0pO1xuXHQgICAgICAgICAgICBcdFx0fSwgMTAwMCk7XG5cdCAgICAgICAgICAgIFx0fVxuXHQgICAgICAgICAgICB9KTtcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0Y3JlYXRlZCgpIHtcblx0XHRcdHRoaXMuZmlsdGVyVGFza3MoJ3RvZGF5Jyk7XG5cdFx0fSxcblxuXHRcdG1vdW50ZWQoKSB7XG5cdFx0XHQkKCcjdG9kYXlzLXRhc2stZGF0ZXBpY2tlcicpLmRhdGVwaWNrZXIoe1xuICAgICAgICAgICAgICAgIGZvcm1hdDonbW0vZGQveXl5eScsXG4gICAgICAgICAgICAgICAgdG9kYXlIaWdobGlnaHQ6IHRydWVcbiAgICAgICAgICAgIH0pO1xuXHRcdH1cblx0fVxuXG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSBidXR0b24uYnRuLWZpbHRlciB7XG5cdFx0ZmxvYXQ6IHJpZ2h0O1xuXHRcdGxpbmUtaGVpZ2h0OiAxLjI7IFxuXHRcdG1hcmdpbi10b3A6IDVweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gVG9kYXlzVGFza3MudnVlPzU4ZTg1Y2QxIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSgpO1xuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuZm9ybVtkYXRhLXYtM2FmZGI1NTNdIHtcXG5cXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcbn1cXG4ubS1yLTEwW2RhdGEtdi0zYWZkYjU1M10ge1xcblxcdG1hcmdpbi1yaWdodDogMTBweDtcXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIkVkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZT81MTA5MjE3YVwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBK1ZBO0NBQ0Esb0JBQUE7Q0FDQTtBQUNBO0NBQ0EsbUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiRWRpdFNlcnZpY2VEYXNoYm9hcmQudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXG5cXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIiBAc3VibWl0LnByZXZlbnQ9XFxcInNhdmVTZXJ2aWNlXFxcIj5cXG5cXG5cXHRcXHQ8ZGl2PlxcblxcdFxcdFxcdFxcblxcdFxcdFxcdDxkaXYgY2xhc3NzPVxcXCJjb2wtbWQtMTJcXFwiPlxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXAgY29sLW1kLTZcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIFRpcDpcXG5cXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLThcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgPGlucHV0IHR5cGU9XFxcIm51bWJlclxcXCIgbWluPVxcXCIwXFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJ0aXBcXFwiIHN0eWxlPVxcXCJ3aWR0aDoxNzVweDtcXFwiIHYtbW9kZWw9XFxcInNhdmVTZXJ2aWNlRm9ybS50aXBcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwIGNvbC1tZC02IG0tbC0yXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBTdGF0dXM6XFxuXFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC04XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDxzZWxlY3QgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwic3RhdHVzXFxcIiBzdHlsZT1cXFwid2lkdGg6MTc1cHg7XFxcIiB2LW1vZGVsPVxcXCJzYXZlU2VydmljZUZvcm0uc3RhdHVzXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVxcXCJjb21wbGV0ZVxcXCI+Q29tcGxldGU8L29wdGlvbj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVxcXCJvbiBwcm9jZXNzXFxcIj5PbiBQcm9jZXNzPC9vcHRpb24+XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cXFwicGVuZGluZ1xcXCI+UGVuZGluZzwvb3B0aW9uPlxcblxcdCAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XFxuXFx0XFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdDxkaXYgY2xhc3NzPVxcXCJjb2wtbWQtMTJcXFwiPlxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXAgY29sLW1kLTZcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIENvc3Q6XFxuXFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC04XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDxpbnB1dCB0eXBlPVxcXCJudW1iZXJcXFwiIG1pbj1cXFwiMFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwiY29zdFxcXCIgc3R5bGU9XFxcIndpZHRoOjE3NXB4O1xcXCIgdi1tb2RlbD1cXFwic2F2ZVNlcnZpY2VGb3JtLmNvc3RcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwIGNvbC1tZC02IG0tbC0yXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBEaXNjb3VudDpcXG5cXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLThcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgPGlucHV0IHR5cGU9XFxcIm51bWJlclxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwiZGlzY291bnRcXFwiIG1pbj1cXFwiMFxcXCIgc3R5bGU9XFxcIndpZHRoOjE3NXB4O1xcXCIgdi1tb2RlbD1cXFwic2F2ZVNlcnZpY2VGb3JtLmRpc2NvdW50XFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0PGRpdiBjbGFzc3M9XFxcImNvbC1tZC0xMlxcXCI+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBSZWFzb246XFxuXFx0XFx0XFx0XFx0ICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICA8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwicmVhc29uXFxcIiB2LW1vZGVsPVxcXCJzYXZlU2VydmljZUZvcm0ucmVhc29uXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0XFxuXFx0XFx0XFx0PGRpdiBjbGFzc3M9XFxcImNvbC1tZC0xMlxcXCI+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgICBOb3RlOlxcblxcdFxcdFxcdFxcdCAgICA8L2xhYmVsPlxcblxcdFxcdFxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICAgPGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcIm5vdGVcXFwiIHYtbW9kZWw9XFxcInNhdmVTZXJ2aWNlRm9ybS5ub3RlXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0PGRpdiBjbGFzc3M9XFxcImNvbC1tZC0xMlxcXCI+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cCBjb2wtbWQtNlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0PGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIFN0YXR1czpcXG5cXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLThcXFwiPlxcblxcdFxcdFxcdFxcdCAgICBcXHQ8ZGl2IHN0eWxlPVxcXCJoZWlnaHQ6NXB4O2NsZWFyOmJvdGg7XFxcIj48L2Rpdj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDxsYWJlbCBjbGFzcz1cXFwiXFxcIj4gXFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHQ8aW5wdXQgdHlwZT1cXFwicmFkaW9cXFwiIHZhbHVlPVxcXCIwXFxcIiBuYW1lPVxcXCJhY3RpdmVcXFwiIHYtbW9kZWw9XFxcInNhdmVTZXJ2aWNlRm9ybS5hY3RpdmVcXFwiPiA8aT48L2k+IFxcblxcdFxcdFxcdFxcdCAgICAgICAgXFx0RGlzYWJsZWQgXFxuXFx0XFx0XFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcblxcdCAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVxcXCJtLWwtMlxcXCI+IFxcblxcdCAgICAgICAgICAgICAgICAgICAgXFx0PGlucHV0IHR5cGU9XFxcInJhZGlvXFxcIiB2YWx1ZT1cXFwiMVxcXCIgbmFtZT1cXFwiYWN0aXZlXFxcIiB2LW1vZGVsPVxcXCJzYXZlU2VydmljZUZvcm0uYWN0aXZlXFxcIj4gPGk+PC9pPiBcXG5cXHQgICAgICAgICAgICAgICAgICAgIFxcdEVuYWJsZWQgXFxuXFx0ICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxcblxcdCAgICAgICAgICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXAgY29sLW1kLTZcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIEV4dGVuZCBUbzpcXG5cXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTZcXFwiPlxcblxcdFxcdFxcdFxcdCAgICBcXHQ8Zm9ybSBpZD1cXFwiZGFpbHktZm9ybVxcXCIgY2xhc3M9XFxcImZvcm0taW5saW5lXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiaW5wdXQtZ3JvdXAgZGF0ZVxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiaW5wdXQtZ3JvdXAtYWRkb25cXFwiPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVxcXCJmYSBmYS1jYWxlbmRhclxcXCI+PC9pPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGlkPVxcXCJkYXRlXFxcIiB2LW1vZGVsPVxcXCJzYXZlU2VydmljZUZvcm0uZXh0ZW5kXFxcIiBhdXRvY29tcGxldGU9XFxcIm9mZlxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbCBkYXRlcGlja2VyXFxcIiBuYW1lPVxcXCJkYXRlXFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxuXFx0ICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XFxuXFx0XFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdFxcdCAgICAgICBcXHRSZXF1aXJlZCBEb2N1bWVudHM6IDxzcGFuIGNsYXNzPVxcXCJ0ZXh0LWRhbmdlclxcXCI+Kjwvc3Bhbj5cXG5cXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHQgICAgICAgICAgICA8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XFxcIlNlbGVjdCBEb2NzXFxcIiBjbGFzcz1cXFwiY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jc1xcXCIgbXVsdGlwbGUgc3R5bGU9XFxcIndpZHRoOjM1MHB4O1xcXCIgdGFiaW5kZXg9XFxcIjRcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVxcXCJkb2MgaW4gcmVxdWlyZWREb2NzXFxcIiA6dmFsdWU9XFxcImRvYy5pZFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdHt7IChkb2MudGl0bGUpLnRyaW0oKSB9fVxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cXG5cXHRcXHRcXHQgICAgICAgICAgICA8L3NlbGVjdD5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0XFx0XFx0ICAgICAgIFxcdE9wdGlvbmFsIERvY3VtZW50czpcXG5cXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHQgICAgICAgICAgICA8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XFxcIlNlbGVjdCBEb2NzXFxcIiBjbGFzcz1cXFwiY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jc1xcXCIgbXVsdGlwbGUgc3R5bGU9XFxcIndpZHRoOjM1MHB4O1xcXCIgdGFiaW5kZXg9XFxcIjRcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVxcXCJkb2MgaW4gb3B0aW9uYWxEb2NzXFxcIiA6dmFsdWU9XFxcImRvYy5pZFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFxcdHt7IChkb2MudGl0bGUpLnRyaW0oKSB9fVxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICA8L29wdGlvbj5cXG5cXHRcXHRcXHQgICAgICAgICAgICA8L3NlbGVjdD5cXG5cXHRcXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0PC9kaXY+XFxuXFxuXFxuPCEtLSBcXHRcXHRcXHQ8ZGl2IGNsYXNzcz1cXFwiY29sLW1kLTEyXFxcIj5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIFJlcG9ydGluZzpcXG5cXHRcXHRcXHRcXHQgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHRcXHQgICAgICAgIDxpbnB1dCB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJyZXBvcnRpbmdcXFwiIHYtbW9kZWw9XFxcIiRwYXJlbnQuJHBhcmVudC5lZGl0U2Vydi5yZXBvcnRpbmdcXFwiPlxcblxcdFxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHQ8L2Rpdj4gLS0+XFxuXFx0XFx0XFx0XFxuXFxuXFx0XFx0PC9kaXY+XFxuXFxuXFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMlxcXCI+XFxuXFx0XFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFx0XFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJzdWJtaXRcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCI+U2F2ZTwvYnV0dG9uPlxcblxcdCAgICA8L2Rpdj5cXG5cXHQ8L2Zvcm0+XFxuXFxuPC90ZW1wbGF0ZT5cXG5cXG48c2NyaXB0PlxcblxcdFxcblxcdGV4cG9ydCBkZWZhdWx0IHtcXG5cXG5cXHRcXHRkYXRhKCkge1xcblxcdFxcdFxcdHJldHVybiB7XFxuXFx0XFx0XFx0XFx0ZHQ6JycsXFxuXFx0XFx0XFx0XFx0cmVxdWlyZWREb2NzOiBbXSxcXG4gICAgICAgIFxcdFxcdG9wdGlvbmFsRG9jczogW10sXFxuXFx0XFx0XFx0XFx0c2F2ZVNlcnZpY2VGb3JtOiB7XFxuXFx0XFx0XFx0XFx0XFx0aWQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHRyYWNraW5nOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aXA6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHN0YXR1czogJycsXFxuXFx0XFx0XFx0XFx0XFx0Y29zdDogJycsXFxuXFx0XFx0XFx0XFx0XFx0ZGlzY291bnQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHJlYXNvbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0bm90ZTogJycsIFxcblxcdFxcdFxcdFxcdFxcdGFjdGl2ZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0cmVwb3J0aW5nOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHRleHRlbmQ6JydcXG5cXHRcXHRcXHRcXHR9LFxcblxcdFxcdFxcdH1cXG5cXHRcXHR9LFxcblxcblxcdFxcdG1ldGhvZHM6IHtcXG5cXG5cXHRcXHRcXHRpbml0Q2hvc2VuU2VsZWN0KCkge1xcblxcblxcdFxcdFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLmNob3Nlbih7XFxuXFx0XFx0XFx0XFx0XFx0d2lkdGg6IFxcXCIxMDAlXFxcIixcXG5cXHRcXHRcXHRcXHRcXHRub19yZXN1bHRzX3RleHQ6IFxcXCJObyByZXN1bHQvcyBmb3VuZC5cXFwiLFxcblxcdFxcdFxcdFxcdFxcdHNlYXJjaF9jb250YWluczogdHJ1ZVxcblxcdFxcdFxcdFxcdH0pO1xcblxcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0IGdldERhdGUoKXtcXG5cXHRcXHQgICAgXFx0Y29uc29sZS5sb2coJCgnZm9ybSNkYWlseS1mb3JtIGlucHV0W25hbWU9ZGF0ZV0nKS52YWwoKSk7XFxuXFx0XFx0ICAgIFxcdHRoaXMuZHQgPSAkKCdmb3JtI2RhaWx5LWZvcm0gaW5wdXRbbmFtZT1kYXRlXScpLnZhbCgpO1xcblxcdFxcdCAgICBcXHR0aGlzLnNhdmVTZXJ2aWNlRm9ybS5leHRlbmQgPSB0aGlzLmR0O1xcblxcdFxcdCAgICB9LFxcblxcblxcdFxcdFxcdHNldFNlcnZpY2UoaWQsIHRyYWNraW5nLCB0aXAsIHN0YXR1cywgY29zdCwgZGlzY291bnQsIHJlYXNvbiwgYWN0aXZlLCBleHRlbmQsIHJjdl9kb2NzKSB7XFxuXFx0XFx0XFx0XFx0dGhpcy5zYXZlU2VydmljZUZvcm0gPSB7XFxuXFx0XFx0XFx0XFx0XFx0aWQsIHRyYWNraW5nLCB0aXAsIHN0YXR1cywgY29zdCwgZGlzY291bnQsIHJlYXNvbiwgYWN0aXZlLCBleHRlbmQsIHJjdl9kb2NzfVxcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0Y2xlYXJTYXZlU2VydmljZUZvcm0oKSB7XFxuXFx0XFx0XFx0XFx0dGhpcy5zYXZlU2VydmljZUZvcm0gPSB7XFxuXFx0XFx0XFx0XFx0XFx0aWQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHRyYWNraW5nOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aXA6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHN0YXR1czogJycsXFxuXFx0XFx0XFx0XFx0XFx0Y29zdDogJycsXFxuXFx0XFx0XFx0XFx0XFx0ZGlzY291bnQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHJlYXNvbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0bm90ZTogJycsIFxcblxcdFxcdFxcdFxcdFxcdGFjdGl2ZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0cmVwb3J0aW5nOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHRleHRlbmQ6JycsXFxuXFx0XFx0XFx0XFx0XFx0cmN2X2RvY3M6JydcXHRcXHRcXHRcXHRcXHRcXG5cXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRzYXZlU2VydmljZSgpIHtcXG5cXHRcXHRcXHRcXHR2YXIgcmVxdWlyZWREb2NzID0gJyc7XFxuICAgICAgICBcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcyBvcHRpb246c2VsZWN0ZWQnKS5lYWNoKCBmdW5jdGlvbihlKSB7XFxuXFx0XFx0XFx0ICAgICAgICByZXF1aXJlZERvY3MgKz0gJCh0aGlzKS52YWwoKSArICcsJztcXG5cXHRcXHRcXHQgICAgfSk7XFxuXFxuICAgICAgICBcXHRcXHR2YXIgb3B0aW9uYWxEb2NzID0gJyc7XFxuICAgICAgICBcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcyBvcHRpb246c2VsZWN0ZWQnKS5lYWNoKCBmdW5jdGlvbihlKSB7XFxuXFx0XFx0XFx0ICAgICAgICBvcHRpb25hbERvY3MgKz0gJCh0aGlzKS52YWwoKSArICcsJztcXG5cXHRcXHRcXHQgICAgfSk7XFxuXFxuXFx0XFx0XFx0ICAgIHZhciBkb2NzID0gcmVxdWlyZWREb2NzICsgb3B0aW9uYWxEb2NzO1xcblxcdFxcdFxcdFxcdHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnJjdl9kb2NzID0gZG9jcy5zbGljZSgwLCAtMSk7XFxuXFxuXFx0XFx0XFx0XFx0aWYodGhpcy5zYXZlU2VydmljZUZvcm0uZGlzY291bnQgPiAwICYmIHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnJlYXNvbiA9PSAnJyl7XFxuXFx0XFx0XFx0ICAgICAgICB0b2FzdHIuZXJyb3IoJ1BsZWFzZSBpbnB1dCBkaXNjb3VudCByZWFzb24uJyk7XFxuXFx0XFx0XFx0ICAgIH1lbHNlIGlmKHJlcXVpcmVkRG9jcyA9PSAnJyAmJiB0aGlzLnJlcXVpcmVkRG9jcy5sZW5ndGghPTApe1xcbiAgICAgICAgXFx0XFx0XFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IFJlcXVpcmVkIERvY3VtZW50cy4nKTsgICAgICAgIFxcdFxcdFxcblxcdFxcdFxcdCAgICB9IGVsc2Uge1xcblxcbiAgICAgICAgXFx0XFx0XFx0YXhpb3MucG9zdCgnL3Zpc2EvY2xpZW50L2NsaWVudEVkaXRTZXJ2aWNlJywge1xcblxcdFxcdFxcdFxcdFxcdFxcdGlkOiB0aGlzLnNhdmVTZXJ2aWNlRm9ybS5pZCxcXG5cXHRcXHRcXHRcXHRcXHRcXHR0cmFja2luZzogdGhpcy5zYXZlU2VydmljZUZvcm0udHJhY2tpbmcsXFxuXFx0XFx0XFx0XFx0XFx0XFx0dGlwOiB0aGlzLnNhdmVTZXJ2aWNlRm9ybS50aXAsXFxuXFx0XFx0XFx0XFx0XFx0XFx0c3RhdHVzOiB0aGlzLnNhdmVTZXJ2aWNlRm9ybS5zdGF0dXMsXFxuXFx0XFx0XFx0XFx0XFx0XFx0Y29zdDogdGhpcy5zYXZlU2VydmljZUZvcm0uY29zdCxcXG5cXHRcXHRcXHRcXHRcXHRcXHRkaXNjb3VudDogdGhpcy5zYXZlU2VydmljZUZvcm0uZGlzY291bnQsXFxuXFx0XFx0XFx0XFx0XFx0XFx0cmVhc29uOiB0aGlzLnNhdmVTZXJ2aWNlRm9ybS5yZWFzb24sXFxuXFx0XFx0XFx0XFx0XFx0XFx0bm90ZTogdGhpcy5zYXZlU2VydmljZUZvcm0ubm90ZSxcXG5cXHRcXHRcXHRcXHRcXHRcXHRhY3RpdmU6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLmFjdGl2ZSxcXG5cXHRcXHRcXHRcXHRcXHRcXHRyZXBvcnRpbmc6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnJlcG9ydGluZyxcXG5cXHRcXHRcXHRcXHRcXHRcXHRleHRlbmQ6IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLmV4dGVuZCxcXG5cXHRcXHRcXHRcXHRcXHRcXHRyY3ZfZG9jczogdGhpcy5zYXZlU2VydmljZUZvcm0ucmN2X2RvY3NcXG5cXHRcXHRcXHRcXHRcXHR9KVxcblxcdFxcdFxcdFxcdFxcdC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRpZihyZXNwb25zZS5kYXRhLnN1Y2Nlc3MpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmNsZWFyU2F2ZVNlcnZpY2VGb3JtKCk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRoaXMuJHBhcmVudC4kcGFyZW50LmNhbGxEYXRhVGFibGVzMSgpO1xcblxcdFxcdCAgICAgICAgICAgICAgICBcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5jYWxsRGF0YVRhYmxlczIoKTtcXG5cXHRcXHQgICAgICAgICAgICAgICAgXFx0JChcXFwiLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzXFxcIikudmFsKCcnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHQkKCcjZWRpdFNlcnZpY2VNb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFxcdHRvYXN0ci5zdWNjZXNzKCdTZXJ2aWNlIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5LicpO1xcblxcdFxcdCAgICAgICAgICAgICAgICB9XFxuXFx0XFx0XFx0XFx0XFx0fSlcXG5cXHRcXHRcXHRcXHRcXHQuY2F0Y2goZXJyb3IgPT57XFxuXFx0XFx0ICAgICAgICAgICAgICAgIGZvcih2YXIga2V5IGluIGVycm9yKSB7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0aWYoZXJyb3IuaGFzT3duUHJvcGVydHkoa2V5KSkgdG9hc3RyLmVycm9yKGVycm9yW2tleV1bMF0pXFxuXFx0XFx0XFx0ICAgICAgICAgICAgfVxcblxcdFxcdCAgICAgICAgICAgIH0pO1xcblxcbiAgICAgICAgXFx0XFx0fVxcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0ZmV0Y2hEb2NzKHJkLCBvZCl7XFxuXFxuXFx0XFx0XFx0XFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykudmFsKCcnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xcblxcblxcdFxcdFxcdFxcdGlmKHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnJjdl9kb2NzID09ICcnIHx8IHRoaXMuc2F2ZVNlcnZpY2VGb3JtLnJjdl9kb2NzID09IG51bGwpXFxuXFx0XFx0XFx0XFx0XFx0dmFyIHJjdiA9ICggdGhpcy5zYXZlU2VydmljZUZvcm0ucmN2X2RvY3MhPW51bGwgPyB0aGlzLnNhdmVTZXJ2aWNlRm9ybS5yY3ZfZG9jcy5zcGxpdChcXFwiLFxcXCIpIDogJycpO1xcblxcdFxcdFxcdFxcdGVsc2VcXG5cXHRcXHRcXHRcXHRcXHR2YXIgcmN2ID0gdGhpcy5zYXZlU2VydmljZUZvcm0ucmN2X2RvY3Muc3BsaXQoXFxcIixcXFwiKTtcXG5cXG5cXHRcXHRcXHQgXFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC8nK3JkKycvc2VydmljZS1kb2NzJylcXG5cXHRcXHRcXHQgXFx0ICAudGhlbihyZXN1bHQgPT4ge1xcblxcdFxcdCAgICAgICAgICAgIHRoaXMucmVxdWlyZWREb2NzID0gcmVzdWx0LmRhdGE7XFxuXFx0XFx0ICAgICAgICB9KTtcXG5cXHQgICAgICAgIFxcdFxcblxcdFxcdFxcdCBcXHRheGlvcy5nZXQoJy92aXNhL2dyb3VwLycrb2QrJy9zZXJ2aWNlLWRvY3MnKVxcblxcdFxcdFxcdCBcXHQgIC50aGVuKHJlc3VsdCA9PiB7XFxuXFx0XFx0ICAgICAgICAgICAgdGhpcy5vcHRpb25hbERvY3MgPSByZXN1bHQuZGF0YTtcXG5cXHRcXHQgICAgICAgIH0pO1xcblxcblxcdFxcdCAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XFxuXFx0ICAgICAgICBcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jcywgLmNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3MnKS5jaG9zZW4oJ2Rlc3Ryb3knKTtcXG5cXHQgICAgICAgIFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLmNob3Nlbih7XFxuXFx0XFx0XFx0XFx0XFx0d2lkdGg6IFxcXCIxMDAlXFxcIixcXG5cXHRcXHRcXHRcXHRcXHRub19yZXN1bHRzX3RleHQ6IFxcXCJObyByZXN1bHQvcyBmb3VuZC5cXFwiLFxcblxcdFxcdFxcdFxcdFxcdHNlYXJjaF9jb250YWluczogdHJ1ZVxcblxcdFxcdFxcdFxcdH0pO1xcblxcblxcdCAgICAgICAgXFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLXJlcXVpcmVkLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1vcHRpb25hbC1kb2NzJykudmFsKHJjdikudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcXG4gICAgICAgIFxcdFxcdCQoJy5jaG9zZW4tc2VsZWN0LWZvci1yZXF1aXJlZC1kb2NzLCAuY2hvc2VuLXNlbGVjdC1mb3Itb3B0aW9uYWwtZG9jcycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XFxuICAgICAgICBcXHR9LCAyMDAwKTtcXG5cXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcblxcblxcblxcdFxcdH0sXFxuXFx0XFx0bW91bnRlZCgpIHtcXG5cXHRcXHRcXHR0aGlzLmluaXRDaG9zZW5TZWxlY3QoKTtcXG4gICAgICAgIFxcdC8vIERhdGVQaWNrZXJcXG5cXHQgICAgICAgIFxcdCQoJy5kYXRlcGlja2VyJykuZGF0ZXBpY2tlcih7XFxuXFx0ICAgICAgICAgICAgICAgIHRvZGF5QnRuOiBcXFwibGlua2VkXFxcIixcXG5cXHQgICAgICAgICAgICAgICAga2V5Ym9hcmROYXZpZ2F0aW9uOiBmYWxzZSxcXG5cXHQgICAgICAgICAgICAgICAgZm9yY2VQYXJzZTogZmFsc2UsXFxuXFx0ICAgICAgICAgICAgICAgIGNhbGVuZGFyV2Vla3M6IHRydWUsXFxuXFx0ICAgICAgICAgICAgICAgIGF1dG9jbG9zZTogdHJ1ZSxcXG5cXHQgICAgICAgICAgICAgICAgZm9ybWF0OiBcXFwieXl5eS1tbS1kZFxcXCIsXFxuXFx0ICAgICAgICAgICAgfSlcXG5cXHQgICAgICAgICAgICAub24oJ2NoYW5nZURhdGUnLCBmdW5jdGlvbihlKSB7IC8vIEJvb3RzdHJhcCBEYXRlcGlja2VyIG9uQ2hhbmdlXFxuXFx0ICAgICAgICAgICAgXFx0bGV0IGlkID0gZS50YXJnZXQuaWQ7XFxuXFx0XFx0XFx0ICAgICAgICBsZXQgZGF0ZSA9IGUudGFyZ2V0LnZhbHVlO1xcblxcblxcdFxcdFxcdCAgICAgICAgY29uc29sZS5sb2coJ0lEIDogJyArIGlkKTtcXG5cXHRcXHRcXHQgICAgICAgIGNvbnNvbGUubG9nKCdEQVRFIDogJyArIGRhdGUpO1xcblxcdFxcdFxcdCAgICAgICAgdGhpcy5nZXREYXRlKCk7XFxuXFx0XFx0XFx0ICAgIH0uYmluZCh0aGlzKSlcXG5cXHRcXHRcXHQgICAgLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbihlKSB7IC8vIG5hdGl2ZSBvbkNoYW5nZVxcblxcdFxcdFxcdCAgICBcXHRsZXQgaWQgPSBlLnRhcmdldC5pZDtcXG5cXHRcXHRcXHQgICAgXFx0bGV0IGRhdGUgPSBlLnRhcmdldC52YWx1ZTtcXG5cXG5cXHRcXHRcXHQgICAgXFx0Y29uc29sZS5sb2coJ0lEIDogJyArIGlkKTtcXG5cXHRcXHRcXHQgICAgICAgIGNvbnNvbGUubG9nKCdEQVRFIDogJyArIGRhdGUpO1xcblxcdFxcdFxcdCAgICB9LmJpbmQodGhpcykpO1xcblxcbiAgICAgICAgfVxcblxcdH1cXG5cXG48L3NjcmlwdD5cXG5cXG48c3R5bGUgc2NvcGVkPlxcblxcdGZvcm0ge1xcblxcdFxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxuXFx0fVxcblxcdC5tLXItMTAge1xcblxcdFxcdG1hcmdpbi1yaWdodDogMTBweDtcXG5cXHR9XFxuPC9zdHlsZT5cIl19XSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTNhZmRiNTUzXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRWRpdFNlcnZpY2VEYXNoYm9hcmQudnVlXG4vLyBtb2R1bGUgaWQgPSAyOTlcbi8vIG1vZHVsZSBjaHVua3MgPSA1IiwiLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuICBNb2RpZmllZCBieSBFdmFuIFlvdSBAeXl4OTkwODAzXG4qL1xuXG52YXIgaGFzRG9jdW1lbnQgPSB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG5cbmlmICh0eXBlb2YgREVCVUcgIT09ICd1bmRlZmluZWQnICYmIERFQlVHKSB7XG4gIGlmICghaGFzRG9jdW1lbnQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgJ3Z1ZS1zdHlsZS1sb2FkZXIgY2Fubm90IGJlIHVzZWQgaW4gYSBub24tYnJvd3NlciBlbnZpcm9ubWVudC4gJyArXG4gICAgXCJVc2UgeyB0YXJnZXQ6ICdub2RlJyB9IGluIHlvdXIgV2VicGFjayBjb25maWcgdG8gaW5kaWNhdGUgYSBzZXJ2ZXItcmVuZGVyaW5nIGVudmlyb25tZW50LlwiXG4gICkgfVxufVxuXG52YXIgbGlzdFRvU3R5bGVzID0gcmVxdWlyZSgnLi9saXN0VG9TdHlsZXMnKVxuXG4vKlxudHlwZSBTdHlsZU9iamVjdCA9IHtcbiAgaWQ6IG51bWJlcjtcbiAgcGFydHM6IEFycmF5PFN0eWxlT2JqZWN0UGFydD5cbn1cblxudHlwZSBTdHlsZU9iamVjdFBhcnQgPSB7XG4gIGNzczogc3RyaW5nO1xuICBtZWRpYTogc3RyaW5nO1xuICBzb3VyY2VNYXA6ID9zdHJpbmdcbn1cbiovXG5cbnZhciBzdHlsZXNJbkRvbSA9IHsvKlxuICBbaWQ6IG51bWJlcl06IHtcbiAgICBpZDogbnVtYmVyLFxuICAgIHJlZnM6IG51bWJlcixcbiAgICBwYXJ0czogQXJyYXk8KG9iaj86IFN0eWxlT2JqZWN0UGFydCkgPT4gdm9pZD5cbiAgfVxuKi99XG5cbnZhciBoZWFkID0gaGFzRG9jdW1lbnQgJiYgKGRvY3VtZW50LmhlYWQgfHwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXSlcbnZhciBzaW5nbGV0b25FbGVtZW50ID0gbnVsbFxudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwXG52YXIgaXNQcm9kdWN0aW9uID0gZmFsc2VcbnZhciBub29wID0gZnVuY3Rpb24gKCkge31cblxuLy8gRm9yY2Ugc2luZ2xlLXRhZyBzb2x1dGlvbiBvbiBJRTYtOSwgd2hpY2ggaGFzIGEgaGFyZCBsaW1pdCBvbiB0aGUgIyBvZiA8c3R5bGU+XG4vLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG52YXIgaXNPbGRJRSA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIC9tc2llIFs2LTldXFxiLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSlcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAocGFyZW50SWQsIGxpc3QsIF9pc1Byb2R1Y3Rpb24pIHtcbiAgaXNQcm9kdWN0aW9uID0gX2lzUHJvZHVjdGlvblxuXG4gIHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIGxpc3QpXG4gIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG4gICAgdmFyIG1heVJlbW92ZSA9IFtdXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgICAgZG9tU3R5bGUucmVmcy0tXG4gICAgICBtYXlSZW1vdmUucHVzaChkb21TdHlsZSlcbiAgICB9XG4gICAgaWYgKG5ld0xpc3QpIHtcbiAgICAgIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhwYXJlbnRJZCwgbmV3TGlzdClcbiAgICAgIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVzID0gW11cbiAgICB9XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXVxuICAgICAgaWYgKGRvbVN0eWxlLnJlZnMgPT09IDApIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKClcbiAgICAgICAgfVxuICAgICAgICBkZWxldGUgc3R5bGVzSW5Eb21bZG9tU3R5bGUuaWRdXG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlc1RvRG9tIChzdHlsZXMgLyogQXJyYXk8U3R5bGVPYmplY3Q+ICovKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBzdHlsZXNbaV1cbiAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgZG9tU3R5bGUucmVmcysrXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pXG4gICAgICB9XG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdKSlcbiAgICAgIH1cbiAgICAgIGlmIChkb21TdHlsZS5wYXJ0cy5sZW5ndGggPiBpdGVtLnBhcnRzLmxlbmd0aCkge1xuICAgICAgICBkb21TdHlsZS5wYXJ0cy5sZW5ndGggPSBpdGVtLnBhcnRzLmxlbmd0aFxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgcGFydHMgPSBbXVxuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIHBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBzdHlsZXNJbkRvbVtpdGVtLmlkXSA9IHsgaWQ6IGl0ZW0uaWQsIHJlZnM6IDEsIHBhcnRzOiBwYXJ0cyB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVN0eWxlRWxlbWVudCAoKSB7XG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpXG4gIHN0eWxlRWxlbWVudC50eXBlID0gJ3RleHQvY3NzJ1xuICBoZWFkLmFwcGVuZENoaWxkKHN0eWxlRWxlbWVudClcbiAgcmV0dXJuIHN0eWxlRWxlbWVudFxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZSAob2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICB2YXIgdXBkYXRlLCByZW1vdmVcbiAgdmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3N0eWxlW2RhdGEtdnVlLXNzci1pZH49XCInICsgb2JqLmlkICsgJ1wiXScpXG5cbiAgaWYgKHN0eWxlRWxlbWVudCkge1xuICAgIGlmIChpc1Byb2R1Y3Rpb24pIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGFuZCBpbiBwcm9kdWN0aW9uIG1vZGUuXG4gICAgICAvLyBzaW1wbHkgZG8gbm90aGluZy5cbiAgICAgIHJldHVybiBub29wXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGJ1dCBpbiBkZXYgbW9kZS5cbiAgICAgIC8vIGZvciBzb21lIHJlYXNvbiBDaHJvbWUgY2FuJ3QgaGFuZGxlIHNvdXJjZSBtYXAgaW4gc2VydmVyLXJlbmRlcmVkXG4gICAgICAvLyBzdHlsZSB0YWdzIC0gc291cmNlIG1hcHMgaW4gPHN0eWxlPiBvbmx5IHdvcmtzIGlmIHRoZSBzdHlsZSB0YWcgaXNcbiAgICAgIC8vIGNyZWF0ZWQgYW5kIGluc2VydGVkIGR5bmFtaWNhbGx5LiBTbyB3ZSByZW1vdmUgdGhlIHNlcnZlciByZW5kZXJlZFxuICAgICAgLy8gc3R5bGVzIGFuZCBpbmplY3QgbmV3IG9uZXMuXG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgaWYgKGlzT2xkSUUpIHtcbiAgICAvLyB1c2Ugc2luZ2xldG9uIG1vZGUgZm9yIElFOS5cbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrK1xuICAgIHN0eWxlRWxlbWVudCA9IHNpbmdsZXRvbkVsZW1lbnQgfHwgKHNpbmdsZXRvbkVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKSlcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCBmYWxzZSlcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCB0cnVlKVxuICB9IGVsc2Uge1xuICAgIC8vIHVzZSBtdWx0aS1zdHlsZS10YWcgbW9kZSBpbiBhbGwgb3RoZXIgY2FzZXNcbiAgICBzdHlsZUVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQpXG4gICAgcmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmopXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmogLyogU3R5bGVPYmplY3RQYXJ0ICovKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcbiAgICAgICAgICBuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJlxuICAgICAgICAgIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKVxuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKVxuICAgIH1cbiAgfVxufVxuXG52YXIgcmVwbGFjZVRleHQgPSAoZnVuY3Rpb24gKCkge1xuICB2YXIgdGV4dFN0b3JlID0gW11cblxuICByZXR1cm4gZnVuY3Rpb24gKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudFxuICAgIHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpXG4gIH1cbn0pKClcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyAoc3R5bGVFbGVtZW50LCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcbiAgdmFyIGNzcyA9IHJlbW92ZSA/ICcnIDogb2JqLmNzc1xuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKVxuICB9IGVsc2Uge1xuICAgIHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKVxuICAgIHZhciBjaGlsZE5vZGVzID0gc3R5bGVFbGVtZW50LmNoaWxkTm9kZXNcbiAgICBpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSlcbiAgICBpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChjc3NOb2RlKVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRvVGFnIChzdHlsZUVsZW1lbnQsIG9iaikge1xuICB2YXIgY3NzID0gb2JqLmNzc1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWFcbiAgdmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXBcblxuICBpZiAobWVkaWEpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKCdtZWRpYScsIG1lZGlhKVxuICB9XG5cbiAgaWYgKHNvdXJjZU1hcCkge1xuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLmNocm9tZS5jb20vZGV2dG9vbHMvZG9jcy9qYXZhc2NyaXB0LWRlYnVnZ2luZ1xuICAgIC8vIHRoaXMgbWFrZXMgc291cmNlIG1hcHMgaW5zaWRlIHN0eWxlIHRhZ3Mgd29yayBwcm9wZXJseSBpbiBDaHJvbWVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VVUkw9JyArIHNvdXJjZU1hcC5zb3VyY2VzWzBdICsgJyAqLydcbiAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNjYwMzg3NVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCwnICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArICcgKi8nXG4gIH1cblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzXG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpXG4gICAgfVxuICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKVxuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtIGJ1dHRvbi5idG4tZmlsdGVyW2RhdGEtdi02YjU4MWFjYl0ge1xcblxcdGZsb2F0OiByaWdodDtcXG5cXHRsaW5lLWhlaWdodDogMS4yOyBcXG5cXHRtYXJnaW4tdG9wOiA1cHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJEZWxpdmVyeVNjaGVkdWxlLnZ1ZT9mNDNjOTEyNlwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBdU1BO0NBQ0EsYUFBQTtDQUNBLGlCQUFBO0NBQ0EsZ0JBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiRGVsaXZlcnlTY2hlZHVsZS52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdFxcblxcdDxkaXY+XFxuXFx0XFx0XFxuXFx0XFx0PGZvcm0gaWQ9XFxcImRlbGl2ZXJ5LXNjaGVkLWZvcm1cXFwiIGNsYXNzPVxcXCJmb3JtLWlubGluZVxcXCIgQHN1Ym1pdC5wcmV2ZW50PVxcXCJmaWx0ZXJTY2hlZCgnZm9ybScpXFxcIj5cXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJpbnB1dC1kYXRlcmFuZ2UgaW5wdXQtZ3JvdXBcXFwiIGlkPVxcXCJkZWxpdmVyeS1zY2hlZHMtZGF0ZXBpY2tlclxcXCI+XFxuXFx0XFx0ICAgICAgICAgICAgPGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJpbnB1dC1zbSBmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcImRlbGl2ZXJ5LXNjaGVkLWRhdGVcXFwiIGF1dG9jb21wbGV0ZT1cXFwib2ZmXFxcIiAvPlxcblxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdCAgICBcXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0ICAgICAgICA8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeVxcXCIgc3R5bGU9XFxcImxpbmUtaGVpZ2h0OiAxLjI7XFxcIj5cXG5cXHRcXHQgICAgICAgICAgICA8c3Ryb25nPkZpbHRlcjwvc3Ryb25nPlxcblxcdFxcdCAgICAgICAgPC9idXR0b24+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdDxidXR0b24gaWQ9XFxcImFkZF9zY2hlZHVsZVxcXCIgY2xhc3M9XFxcImJ0biBidG4td2FybmluZ1xcXCIgc3R5bGU9XFxcImxpbmUtaGVpZ2h0OiAxLjI7IG1hcmdpbi1sZWZ0OiAycHg7XFxcIiB0eXBlPVxcXCJidXR0b25cXFwiIEBjbGljaz1cXFwic2hvd1NjaGVkdWxlTW9kYWwoJ2FkZCcsICcnKVxcXCI+QWRkIFNjaGVkdWxlIDwvYnV0dG9uPlxcblxcblxcdFxcdFxcdDxidXR0b24gQGNsaWNrPVxcXCJmaWx0ZXJTY2hlZCgndG9kYXknKVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXFxcIiA6Y2xhc3M9XFxcInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0ndG9kYXknfVxcXCIgc3R5bGU9XFxcIm1hcmdpbi1yaWdodDogMnB4O1xcXCIgdHlwZT1cXFwiYnV0dG9uXFxcIj5Ub2RheTwvYnV0dG9uPlxcblxcblxcdFxcdFxcdDxidXR0b24gQGNsaWNrPVxcXCJmaWx0ZXJTY2hlZCgndG9tb3Jyb3cnKVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXFxcIiA6Y2xhc3M9XFxcInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0neWVzdGVyZGF5J31cXFwiIHN0eWxlPVxcXCJtYXJnaW4tcmlnaHQ6IDJweDtcXFwiIHR5cGU9XFxcImJ1dHRvblxcXCI+VG9tb3Jyb3c8L2J1dHRvbj5cXG5cXHRcXHQ8L2Zvcm0+XFxuXFxuXFx0XFx0PGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjIwcHg7IGNsZWFyOmJvdGg7XFxcIj48L2Rpdj5cXG5cXG4gICAgXFx0PHRhYmxlIGlkPVxcXCJkZWxpdmVyeVNjaGVkdWxlXFxcIiBjbGFzcz1cXFwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJlZCB0YWJsZS1ob3ZlciBkYXRhVGFibGVzLWV4YW1wbGVcXFwiPlxcblxcdCAgICAgICAgPHRoZWFkPlxcblxcdCAgICAgICAgICAgIDx0cj5cXG5cXHQgICAgICAgICAgICAgICAgPHRoPkl0ZW08L3RoPlxcblxcdCAgICAgICAgICAgICAgICA8dGg+U2NoZWR1bGU8L3RoPlxcblxcdCAgICAgICAgICAgICAgICA8dGg+TG9jYXRpb248L3RoPlxcblxcdCAgICAgICAgICAgICAgICA8dGg+UmlkZXI8L3RoPlxcblxcdCAgICAgICAgICAgICAgICA8dGg+QWN0aW9uPC90aD4gICAgICAgICBcXG5cXHQgICAgICAgICAgICA8L3RyPlxcblxcdCAgICAgICAgPC90aGVhZD5cXG5cXHQgICAgICAgICAgICA8dGJvZHk+XFxuXFx0ICAgICAgICAgICAgXFx0PHRyIHYtZm9yPVxcXCJkcyBpbiBkZWxpdmVyeVNjaGVkdWxlc1xcXCIgdi1jbG9haz5cXG5cXHQgICAgICAgICAgICBcXHRcXHQ8dGQ+e3sgZHMuaXRlbSB9fTwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PHRkPnt7IGRzLnNjaGVkdWxlZF9kYXRlIH19IHt7IGRzLnNjaGVkdWxlZF90aW1lfX08L3RkPlxcblxcdFxcdFxcdFxcdFxcdFxcdDx0ZD57eyBkcy5sb2NhdGlvbiB9fTwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PHRkPjxzcGFuPnt7IGRzLnJpZGVyLmZ1bGxfbmFtZSB9fTwvc3Bhbj48L3RkPlxcblxcdFxcdFxcdFxcdFxcdFxcdDx0ZD48YSBAY2xpY2s9XFxcInNob3dTY2hlZHVsZU1vZGFsKCdlZGl0JywgZHMuaWQpXFxcIj5FZGl0PC9hPiB8IDxhIEBjbGljaz1cXFwiJHBhcmVudC5zaG93RGVsZXRlU2NoZWRQcm9tcHQoZHMuaWQpXFxcIj5EZWxldGU8L2E+PC90ZD4gICAgICAgICBcXG5cXHQgICAgICAgICAgICBcXHQ8L3RyPlxcblxcdCAgICAgICAgICAgIFxcdCAgICBcXG5cXHQgICAgICAgICAgICA8L3Rib2R5PiAgICAgICAgICAgICAgICAgIFxcblxcdCAgICA8L3RhYmxlPlxcblxcblxcdDwvZGl2PlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXHRcXG5cXHRleHBvcnQgZGVmYXVsdCB7XFxuXFx0XFx0ZGF0YSgpIHtcXG5cXHRcXHRcXHRyZXR1cm4ge1xcblxcdFxcdFxcdFxcdGFjdGl2ZUJ1dHRvbjogJ3RvZGF5JyxcXG5cXG5cXHRcXHRcXHRcXHRkYXRlRmlsdGVyOiAnJyxcXG5cXG5cXHRcXHRcXHRcXHRkZWxpdmVyeVNjaGVkdWxlczogW11cXG5cXHRcXHRcXHR9XFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRtZXRob2RzOiB7XFxuXFx0XFx0XFx0Zm9ybWF0RGF0ZVBpY2tlclZhbHVlKHZhbHVlKSB7XFxuXFx0XFx0XFx0XFx0bGV0IGRhdGVQaWNrZXJWYWx1ZSA9IHZhbHVlLnNwbGl0KCcvJyk7XFxuXFxuXFx0XFx0XFx0XFx0cmV0dXJuIGRhdGVQaWNrZXJWYWx1ZVsyXSArICctJyArIGRhdGVQaWNrZXJWYWx1ZVswXSArICctJyArIGRhdGVQaWNrZXJWYWx1ZVsxXTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdHNob3dTY2hlZHVsZU1vZGFsKHZhbCwgaWQpe1xcblxcdCAgICAgICAgICAgIGlmKHZhbCA9PSBcXFwiYWRkXFxcIil7XFxuXFx0ICAgICAgICAgICAgICAgICQoJyNhZGRTY2hlZHVsZU1vZGFsJykubW9kYWwoJ3RvZ2dsZScpO1xcblxcdCAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQubW9kZSA9IFxcXCJBZGRcXFwiO1xcblxcdCAgICAgICAgICAgIH1lbHNlIGlmKHZhbCA9PSBcXFwiZWRpdFxcXCIpe1xcblxcdCAgICAgICAgICAgICAgICBheGlvcy5wb3N0KCcvdmlzYS9ob21lL2dldFNjaGVkLycrIGlkKSBcXG5cXHQgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcXG5cXHQgICAgICAgICAgICAgICAgICAgIHRoaXMuJHBhcmVudC5hZGRTY2hlZHVsZSA9IG5ldyBGb3JtICh7XFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgaWQgICAgICAgICAgICAgIDpyZXN1bHQuZGF0YS5pZCxcXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICBpdGVtICAgICAgICAgICAgOnJlc3VsdC5kYXRhLml0ZW0sXFxuXFx0ICAgICAgICAgICAgICAgICAgICAgICAgcmlkZXJfaWQgICAgICAgIDpyZXN1bHQuZGF0YS5yaWRlcl9pZCxcXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICBsb2NhdGlvbiAgICAgICAgOnJlc3VsdC5kYXRhLmxvY2F0aW9uLFxcblxcdCAgICAgICAgICAgICAgICAgICAgICAgIHNjaGVkdWxlZF9kYXRlICA6dGhpcy5mb3JtYXREYXRlWU1EKHJlc3VsdC5kYXRhLnNjaGVkdWxlZF9kYXRlKSxcXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICBzY2hlZHVsZWRfdGltZSAgOnJlc3VsdC5kYXRhLnNjaGVkdWxlZF90aW1lXFxuXFx0ICAgICAgICAgICAgICAgICAgICB9LHsgYmFzZVVSTDogJy92aXNhL2hvbWUvJ30pO1xcblxcdCAgICAgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LmR0ID0gcmVzdWx0LmRhdGEuc2NoZWR1bGVkX2RhdGU7XFxuXFx0ICAgICAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuc2NoZWR1bGVkX2hvdXIgPSB0aGlzLmdldEhvdXIocmVzdWx0LmRhdGEuc2NoZWR1bGVkX3RpbWUpO1xcblxcdCAgICAgICAgICAgICAgICAgICAgdGhpcy4kcGFyZW50LnNjaGVkdWxlZF9taW51dGUgPSB0aGlzLmdldE1pbnV0ZShyZXN1bHQuZGF0YS5zY2hlZHVsZWRfdGltZSk7XFxuXFx0ICAgICAgICAgICAgICAgICAgICAvLyQoJ2Zvcm0jZnJtX2RlbGl2ZXJ5c2NoZWQgaW5wdXRbbmFtZT1kYXRlXScpLnZhbChyZXN1bHQuZGF0YS5zY2hlZHVsZWRfZGF0ZSk7XFxuXFx0ICAgICAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQubW9kZSA9IFxcXCJFZGl0XFxcIjtcXG5cXHQgICAgICAgICAgICAgICAgfSk7XFxuXFx0ICAgICAgICAgICAgICAgICQoJyNhZGRTY2hlZHVsZU1vZGFsJykubW9kYWwoJ3RvZ2dsZScpO1xcblxcdCAgICAgICAgICAgIH1cXG5cXHQgICAgICAgIH0sXFxuXFxuXFx0ICAgICAgICAvL1lZWVktTU0tRERcXG5cXHRcXHRcXHRmb3JtYXREYXRlWU1EKHZhbHVlKSB7XFxuXFx0XFx0XFx0XFx0bGV0IGRhdGUgPSB2YWx1ZS5zcGxpdCgnLycpO1xcblxcdFxcdFxcdFxcdGxldCBuZXdEYXRlID0gZGF0ZVsyXSArICctJyArIGRhdGVbMF0gKyAnLScgKyBkYXRlWzFdO1xcblxcblxcdFxcdFxcdFxcdHJldHVybiBuZXdEYXRlO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0Ly9tbS9kZC95eXl5XFxuXFx0XFx0XFx0Zm9ybWF0RGF0ZU1EWSh2YWx1ZSkge1xcblxcdFxcdFxcdFxcdGxldCBkYXRlID0gdmFsdWUuc3BsaXQoJy0nKTtcXG5cXHRcXHRcXHRcXHRsZXQgbmV3RGF0ZSA9IGRhdGVbMV0gKyAnLycgKyBkYXRlWzJdICsgJy8nICsgZGF0ZVswXTtcXG5cXG5cXHRcXHRcXHRcXHRyZXR1cm4gbmV3RGF0ZTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGFsd2F5c1R3b0RpZ2l0KHZhbHVlKXtcXG5cXHRcXHRcXHRcXHRpZiAodmFsdWUudG9TdHJpbmcoKS5sZW5ndGggPT0gMSkge1xcblxcdFxcdCAgICAgICAgICAgIHZhbHVlID0gXFxcIjBcXFwiICsgdmFsdWU7XFxuXFx0XFx0ICAgICAgICB9XFxuXFx0XFx0ICAgICAgICByZXR1cm4gdmFsdWU7XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRnZXRIb3VyKHZhbHVlKXtcXG5cXHRcXHRcXHRcXHRsZXQgdGltZSA9IHZhbHVlLnNwbGl0KCc6Jyk7XFxuXFx0XFx0ICAgICAgICByZXR1cm4gdGltZVswXTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGdldE1pbnV0ZSh2YWx1ZSl7XFxuXFx0XFx0XFx0XFx0bGV0IHRpbWUgPSB2YWx1ZS5zcGxpdCgnOicpO1xcblxcdFxcdCAgICAgICAgcmV0dXJuIHRpbWVbMV07XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRmaWx0ZXJTY2hlZChkYXRlKSB7XFxuXFx0XFx0XFx0XFx0aWYoZGF0ZSA9PSAnZm9ybScpIHtcXG5cXHRcXHRcXHRcXHRcXHRsZXQgZGF0ZVBpY2tlclZhbHVlID0gJCgnZm9ybSNkZWxpdmVyeS1zY2hlZC1mb3JtIGlucHV0W25hbWU9ZGVsaXZlcnktc2NoZWQtZGF0ZV0nKS52YWwoKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHRpZihkYXRlUGlja2VyVmFsdWUgPT0gJycpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRkYXRlUGlja2VyVmFsdWUgPSBtb21lbnQoKS5mb3JtYXQoJ01NL0REL1lZWVknKTtcXG5cXHRcXHRcXHRcXHRcXHR9IGVsc2Uge1xcblxcdFxcdFxcdFxcdFxcdFxcdGRhdGVQaWNrZXJWYWx1ZSA9IGRhdGVQaWNrZXJWYWx1ZTtcXG5cXHRcXHRcXHRcXHRcXHR9XFxuXFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5kYXRlRmlsdGVyID0gZGF0ZVBpY2tlclZhbHVlO1xcblxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5hY3RpdmVCdXR0b24gPSBkYXRlO1xcblxcblxcdFxcdFxcdFxcdFxcdGlmKGRhdGUgPT0gJ3RvZGF5JylcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQoKS5mb3JtYXQoJ01NL0REL1lZWVknKTtcXG5cXHRcXHRcXHRcXHRcXHRlbHNlIGlmKGRhdGUgPT0gJ3RvbW9ycm93JylcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQobmV3IERhdGUoKSkuYWRkKCsxLCdkYXlzJykuZm9ybWF0KCdNTS9ERC9ZWVlZJyk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0JCgnZm9ybSNkZWxpdmVyeS1zY2hlZC1mb3JtIGlucHV0W25hbWU9ZGVsaXZlcnktc2NoZWQtZGF0ZV0nKS52YWwodGhpcy5kYXRlRmlsdGVyKTtcXG5cXHRcXHRcXHRcXHR9XFxuXFxuXFx0XFx0XFx0XFx0YXhpb3MuZ2V0KCcvdmlzYS9ob21lL2dldFNjaGVkdWxlc2J5RGF0ZScsIHtcXG5cXHQgICAgICAgICAgICAgICAgcGFyYW1zOiB7XFxuXFx0ICAgICAgICAgICAgICAgIFxcdGRhdGVGaWx0ZXI6IHRoaXMuZGF0ZUZpbHRlclxcblxcdCAgICAgICAgICAgICAgICB9XFxuXFx0ICAgICAgICAgICAgfSlcXG5cXHQgICAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XFxuXFx0ICAgICAgICAgICAgXFx0bGV0IHsgc3VjY2Vzcywgc2NoZWR1bGVzIH0gPSByZXNwb25zZS5kYXRhO1xcblxcblxcdCAgICAgICAgICAgIFxcdGlmKHN1Y2Nlc3MpIHtcXG5cXHQgICAgICAgICAgICBcXHRcXHQkKCcjZGVsaXZlcnlTY2hlZHVsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcXG5cXG5cXHQgICAgICAgICAgICBcXHRcXHR0aGlzLmRlbGl2ZXJ5U2NoZWR1bGVzID0gc2NoZWR1bGVzO1xcblxcblxcdCAgICAgICAgICAgIFxcdFxcdHNldFRpbWVvdXQoKCkgPT4ge1xcblxcdCAgICAgICAgICAgIFxcdFxcdFxcdCQoJyNkZWxpdmVyeVNjaGVkdWxlJykuRGF0YVRhYmxlKHtcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9XaWR0aDogZmFsc2UsXFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgXFxcImNvbHVtbkRlZnNcXFwiOiBbXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgXFxcIndpZHRoXFxcIjogXFxcIjI1JVxcXCIsIFxcXCJ0YXJnZXRzXFxcIjogMSB9XFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgXSxcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICBcXFwibGVuZ3RoTWVudVxcXCI6IFtbMTAsIDI1LCA1MF0sIFsxMCwgMjUsIDUwXV0sXFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgXFxcImlEaXNwbGF5TGVuZ3RoXFxcIjogMTBcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgIH0pO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgJChmdW5jdGlvbiAoKSB7XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAgIFxcdCQoJ1tkYXRhLXRvZ2dsZT1cXFwidG9vbHRpcFxcXCJdJykudG9vbHRpcCgpXFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICB9KTtcXG5cXHQgICAgICAgICAgICBcXHRcXHR9LCAxMDAwKTtcXG5cXHQgICAgICAgICAgICBcXHR9XFxuXFx0ICAgICAgICAgICAgfSk7XFxuXFx0XFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y3JlYXRlZCgpIHtcXG5cXHRcXHRcXHR0aGlzLmZpbHRlclNjaGVkKCd0b2RheScpO1xcblxcdFxcdH0sXFxuXFxuXFx0XFx0bW91bnRlZCgpIHtcXG5cXHRcXHRcXHQkKCcjZGVsaXZlcnktc2NoZWRzLWRhdGVwaWNrZXInKS5kYXRlcGlja2VyKHtcXG4gICAgICAgICAgICAgICAgZm9ybWF0OidtbS9kZC95eXl5JyxcXG4gICAgICAgICAgICAgICAgdG9kYXlIaWdobGlnaHQ6IHRydWVcXG4gICAgICAgICAgICB9KTtcXG5cXHRcXHR9XFxuXFx0fVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSBidXR0b24uYnRuLWZpbHRlciB7XFxuXFx0XFx0ZmxvYXQ6IHJpZ2h0O1xcblxcdFxcdGxpbmUtaGVpZ2h0OiAxLjI7IFxcblxcdFxcdG1hcmdpbi10b3A6IDVweDtcXG5cXHR9XFxuPC9zdHlsZT5cIl19XSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTZiNTgxYWNiXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGVsaXZlcnlTY2hlZHVsZS52dWVcbi8vIG1vZHVsZSBpZCA9IDMwNFxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtIGJ1dHRvbi5idG4tZmlsdGVyW2RhdGEtdi03Yzk1Y2RlY10ge1xcblxcdGZsb2F0OiByaWdodDtcXG5cXHRsaW5lLWhlaWdodDogMS4yOyBcXG5cXHRtYXJnaW4tdG9wOiA1cHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJUb2RheXNUYXNrcy52dWU/NThlODVjZDFcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQStMQTtDQUNBLGFBQUE7Q0FDQSxpQkFBQTtDQUNBLGdCQUFBO0NBQ0FcIixcImZpbGVcIjpcIlRvZGF5c1Rhc2tzLnZ1ZVwiLFwic291cmNlc0NvbnRlbnRcIjpbXCI8dGVtcGxhdGU+XFxuXFx0XFxuXFx0PGRpdj5cXG5cXHRcXHRcXG5cXHRcXHQ8Zm9ybSBpZD1cXFwidG9kYXlzLXRhc2tzLWZvcm1cXFwiIGNsYXNzPVxcXCJmb3JtLWlubGluZVxcXCIgQHN1Ym1pdC5wcmV2ZW50PVxcXCJmaWx0ZXJUYXNrcygnZm9ybScpXFxcIj5cXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJpbnB1dC1kYXRlcmFuZ2UgaW5wdXQtZ3JvdXBcXFwiIGlkPVxcXCJ0b2RheXMtdGFzay1kYXRlcGlja2VyXFxcIj5cXG5cXHRcXHQgICAgICAgICAgICA8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImlucHV0LXNtIGZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwidG9kYXlzLXRhc2stZGF0ZVxcXCIgYXV0b2NvbXBsZXRlPVxcXCJvZmZcXFwiIC8+XFxuXFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0ICAgIFxcblxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHQgICAgICAgIDxidXR0b24gdHlwZT1cXFwic3VibWl0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5XFxcIiBzdHlsZT1cXFwibGluZS1oZWlnaHQ6IDEuMjtcXFwiPlxcblxcdFxcdCAgICAgICAgICAgIDxzdHJvbmc+RmlsdGVyPC9zdHJvbmc+XFxuXFx0XFx0ICAgICAgICA8L2J1dHRvbj5cXG5cXHRcXHQgICAgPC9kaXY+XFxuXFxuXFx0XFx0ICAgIDxidXR0b24gQGNsaWNrPVxcXCJmaWx0ZXJUYXNrcygndG9tb3Jyb3cnKVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXFxcIiA6Y2xhc3M9XFxcInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0ndG9tb3Jyb3cnfVxcXCIgdHlwZT1cXFwiYnV0dG9uXFxcIj5Ub21vcnJvdzwvYnV0dG9uPlxcblxcblxcdFxcdFxcdDxidXR0b24gQGNsaWNrPVxcXCJmaWx0ZXJUYXNrcygndG9kYXknKVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXFxcIiA6Y2xhc3M9XFxcInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0ndG9kYXknfVxcXCIgc3R5bGU9XFxcIm1hcmdpbi1yaWdodDogMnB4O1xcXCIgdHlwZT1cXFwiYnV0dG9uXFxcIj5Ub2RheTwvYnV0dG9uPlxcblxcblxcdFxcdFxcdDxidXR0b24gQGNsaWNrPVxcXCJmaWx0ZXJUYXNrcygneWVzdGVyZGF5JylcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclxcXCIgOmNsYXNzPVxcXCJ7J2J0bi1vdXRsaW5lJyA6IGFjdGl2ZUJ1dHRvbiE9J3llc3RlcmRheSd9XFxcIiBzdHlsZT1cXFwibWFyZ2luLXJpZ2h0OiAycHg7XFxcIiB0eXBlPVxcXCJidXR0b25cXFwiPlllc3RlcmRheTwvYnV0dG9uPlxcblxcdFxcdDwvZm9ybT5cXG5cXG5cXHRcXHQ8ZGl2IHN0eWxlPVxcXCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcXFwiPjwvZGl2PlxcblxcbiAgICBcXHQ8dGFibGUgaWQ9XFxcInRvZGF5c1Rhc2tzTGlzdHNcXFwiIGNsYXNzPVxcXCJ0YWJsZSB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmVkIHRhYmxlLWhvdmVyIGRhdGFUYWJsZXMtZXhhbXBsZVxcXCI+XFxuXFx0XFx0ICAgIDx0aGVhZD5cXG5cXHRcXHQgICAgICAgIDx0cj5cXG5cXHRcXHQgICAgICAgICAgICA8dGg+TmFtZTwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgPHRoPkNsaWVudCBOby48L3RoPlxcblxcdFxcdCAgICAgICAgICAgIDx0aD5TZXJ2aWNlPC90aD5cXG5cXHRcXHQgICAgICAgICAgICA8dGg+RXN0aW1hdGVkIENvc3Q8L3RoPlxcblxcdFxcdCAgICAgICAgICAgIDx0aD5TdGF0dXM8L3RoPlxcblxcdFxcdCAgICAgICAgICAgIDx0aD5GaWxpbmcgRGF0ZTwvdGg+XFxuXFx0XFx0ICAgICAgICAgICAgPHRoPlJlbGVhc2luZyBEYXRlPC90aD5cXG5cXHRcXHQgICAgICAgIDwvdHI+XFxuXFx0XFx0ICAgIDwvdGhlYWQ+XFxuXFx0XFx0ICAgIDx0Ym9keT5cXG5cXHRcXHQgICAgICAgIDx0ciB2LWZvcj1cXFwidGFzayBpbiB0YXNrc1xcXCI+XFxuXFx0XFx0ICAgICAgIFxcdFxcdDx0ZD5cXG5cXHRcXHQgICAgICAgXFx0XFx0XFx0PGEgOmhyZWY9XFxcIicvdmlzYS9jbGllbnQvJyArIHRhc2suY2xpZW50X3NlcnZpY2UudXNlci5pZFxcXCIgdGFyZ2V0PVxcXCJfYmxhbmtcXFwiIGRhdGEtdG9nZ2xlPVxcXCJ0b29sdGlwXFxcIiBkYXRhLXBsYWNlbWVudD1cXFwicmlnaHRcXFwiIHRpdGxlPVxcXCJWaWV3IGNsaWVudCBwcm9maWxlXFxcIj5cXG5cXHRcXHQgICAgICAgXFx0XFx0XFx0XFx0e3sgdGFzay5jbGllbnRfc2VydmljZS51c2VyLmZ1bGxfbmFtZSB9fVxcblxcdFxcdCAgICAgICBcXHRcXHRcXHQ8L2E+XFxuXFx0XFx0ICAgICAgIFxcdFxcdDwvdGQ+XFxuXFx0XFx0ICAgICAgIFxcdFxcdDx0ZD57eyB0YXNrLmNsaWVudF9zZXJ2aWNlLnVzZXIuaWQgfX08L3RkPlxcblxcdFxcdCAgICAgICBcXHRcXHQ8dGQ+e3sgdGFzay5jbGllbnRfc2VydmljZS5zZXJ2aWNlX2NhdGVnb3J5LmRldGFpbCB9fTwvdGQ+XFxuXFx0XFx0ICAgICAgIFxcdFxcdDx0ZD5cXG5cXHRcXHQgICAgICAgXFx0XFx0XFx0e3sgdGFzay5jbGllbnRfc2VydmljZS5zZXJ2aWNlX2NhdGVnb3J5LmVzdGltYXRlZF9jb3N0IH19XFxuXFx0XFx0ICAgICAgIFxcdFxcdDwvdGQ+XFxuXFx0XFx0ICAgICAgIFxcdFxcdDx0ZD57eyB0YXNrLnN0YXR1cyB9fTwvdGQ+XFxuXFx0XFx0ICAgICAgIFxcdFxcdDx0ZD57eyB0YXNrLmZpbGluZ19kYXRlIH19PC90ZD5cXG5cXHRcXHQgICAgICAgXFx0XFx0PHRkPnt7IHRhc2sucmVsZWFzaW5nX2RhdGUgfX08L3RkPlxcblxcdFxcdCAgICAgICAgPC90cj5cXG5cXHRcXHQgICAgPC90Ym9keT5cXG5cXHRcXHQgICAgPHRmb290PlxcblxcdCAgICAgICAgICAgIDx0cj5cXG5cXHQgICAgICAgICAgICAgICAgPHRoIGNvbHNwYW49XFxcIjdcXFwiIHN0eWxlPVxcXCJ0ZXh0LWFsaWduOnJpZ2h0XFxcIj48L3RoPlxcblxcdCAgICAgICAgICAgIDwvdHI+XFxuXFx0ICAgICAgICA8L3Rmb290PlxcblxcdFxcdDwvdGFibGU+XFxuXFxuXFx0PC9kaXY+XFxuXFxuPC90ZW1wbGF0ZT5cXG5cXG48c2NyaXB0PlxcblxcdFxcblxcdGV4cG9ydCBkZWZhdWx0IHtcXG5cXHRcXHRkYXRhKCkge1xcblxcdFxcdFxcdHJldHVybiB7XFxuXFx0XFx0XFx0XFx0YWN0aXZlQnV0dG9uOiAndG9kYXknLFxcblxcblxcdFxcdFxcdFxcdGRhdGVGaWx0ZXI6ICcnLFxcblxcblxcdFxcdFxcdFxcdHRhc2tzOiBbXVxcblxcdFxcdFxcdH1cXG5cXHRcXHR9LFxcblxcblxcdFxcdG1ldGhvZHM6IHtcXG5cXHRcXHRcXHRmb3JtYXREYXRlUGlja2VyVmFsdWUodmFsdWUpIHtcXG5cXHRcXHRcXHRcXHRsZXQgZGF0ZVBpY2tlclZhbHVlID0gdmFsdWUuc3BsaXQoJy8nKTtcXG5cXG5cXHRcXHRcXHRcXHRyZXR1cm4gZGF0ZVBpY2tlclZhbHVlWzJdICsgJy0nICsgZGF0ZVBpY2tlclZhbHVlWzBdICsgJy0nICsgZGF0ZVBpY2tlclZhbHVlWzFdO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0Zm9ybWF0RGF0ZSh2YWx1ZSkge1xcblxcdFxcdFxcdFxcdGxldCBkYXRlID0gdmFsdWUuc3BsaXQoJy0nKTtcXG5cXHRcXHRcXHRcXHRsZXQgbmV3RGF0ZSA9IGRhdGVbMV0gKyAnLycgKyBkYXRlWzJdICsgJy8nICsgZGF0ZVswXTtcXG5cXG5cXHRcXHRcXHRcXHRyZXR1cm4gbmV3RGF0ZTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGZpbHRlclRhc2tzKGRhdGUpIHtcXG5cXHRcXHRcXHRcXHRpZihkYXRlID09ICdmb3JtJykge1xcblxcdFxcdFxcdFxcdFxcdGxldCBkYXRlUGlja2VyVmFsdWUgPSAkKCdmb3JtI3RvZGF5cy10YXNrcy1mb3JtIGlucHV0W25hbWU9dG9kYXlzLXRhc2stZGF0ZV0nKS52YWwoKTtcXG5cXG5cXHRcXHRcXHRcXHRcXHRpZihkYXRlUGlja2VyVmFsdWUgPT0gJycpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRkYXRlUGlja2VyVmFsdWUgPSBtb21lbnQoKS5mb3JtYXQoJ1lZWVktTU0tREQnKTtcXG5cXHRcXHRcXHRcXHRcXHR9IGVsc2Uge1xcblxcdFxcdFxcdFxcdFxcdFxcdGRhdGVQaWNrZXJWYWx1ZSA9IHRoaXMuZm9ybWF0RGF0ZVBpY2tlclZhbHVlKGRhdGVQaWNrZXJWYWx1ZSk7XFxuXFx0XFx0XFx0XFx0XFx0fVxcblxcblxcdFxcdFxcdFxcdFxcdHRoaXMuZGF0ZUZpbHRlciA9IGRhdGVQaWNrZXJWYWx1ZTtcXG5cXHRcXHRcXHRcXHR9IGVsc2Uge1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuYWN0aXZlQnV0dG9uID0gZGF0ZTtcXG5cXG5cXHRcXHRcXHRcXHRcXHRpZihkYXRlID09ICd0b21vcnJvdycpXFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5kYXRlRmlsdGVyID0gbW9tZW50KG5ldyBEYXRlKCkpLmFkZCgxLCdkYXknKS5mb3JtYXQoJ1lZWVktTU0tREQnKTtcXG5cXHRcXHRcXHRcXHRcXHRlbHNlIGlmKGRhdGUgPT0gJ3RvZGF5JylcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQoKS5mb3JtYXQoJ1lZWVktTU0tREQnKTtcXG5cXHRcXHRcXHRcXHRcXHRlbHNlIGlmKGRhdGUgPT0gJ3llc3RlcmRheScpXFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5kYXRlRmlsdGVyID0gbW9tZW50KG5ldyBEYXRlKCkpLmFkZCgtMSwnZGF5cycpLmZvcm1hdCgnWVlZWS1NTS1ERCcpO1xcblxcblxcdFxcdFxcdFxcdFxcdCQoJ2Zvcm0jdG9kYXlzLXRhc2tzLWZvcm0gaW5wdXRbbmFtZT10b2RheXMtdGFzay1kYXRlXScpLnZhbCh0aGlzLmZvcm1hdERhdGUodGhpcy5kYXRlRmlsdGVyKSk7XFxuXFx0XFx0XFx0XFx0fVxcblxcblxcdFxcdFxcdFxcdGF4aW9zLmdldCgnL3Zpc2EvaG9tZS9nZXQtdG9kYXlzLXRhc2tzJywge1xcblxcdCAgICAgICAgICAgICAgICBwYXJhbXM6IHtcXG5cXHQgICAgICAgICAgICAgICAgXFx0ZGF0ZUZpbHRlcjogdGhpcy5kYXRlRmlsdGVyXFxuXFx0ICAgICAgICAgICAgICAgIH1cXG5cXHQgICAgICAgICAgICB9KVxcblxcdCAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHQgICAgICAgICAgICBcXHRsZXQgeyBzdWNjZXNzLCB0YXNrcyB9ID0gcmVzcG9uc2UuZGF0YTtcXG5cXG5cXHQgICAgICAgICAgICBcXHRpZihzdWNjZXNzKSB7XFxuXFx0ICAgICAgICAgICAgXFx0XFx0JCgnI3RvZGF5c1Rhc2tzTGlzdHMnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XFxuXFxuXFx0ICAgICAgICAgICAgXFx0XFx0dGhpcy50YXNrcyA9IHRhc2tzO1xcblxcblxcdCAgICAgICAgICAgIFxcdFxcdHNldFRpbWVvdXQoKCkgPT4ge1xcblxcdCAgICAgICAgICAgIFxcdFxcdFxcdCQoJyN0b2RheXNUYXNrc0xpc3RzJykuRGF0YVRhYmxlKHtcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgICAgIFxcXCJsZW5ndGhNZW51XFxcIjogW1sxMCwgMjUsIDUwXSwgWzEwLCAyNSwgNTBdXSxcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICBcXFwiaURpc3BsYXlMZW5ndGhcXFwiOiAxMCxcXG5cXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICBcXFwiZm9vdGVyQ2FsbGJhY2tcXFwiOiBmdW5jdGlvbiAoIHJvdywgZGF0YSwgc3RhcnQsIGVuZCwgZGlzcGxheSApIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICB2YXIgYXBpID0gdGhpcy5hcGkoKSwgZGF0YTtcXG5cXHRcXHRcXHRcXHRcXHRcXHQgXFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgdmFyIGludFZhbCA9IGZ1bmN0aW9uICggaSApIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgcmV0dXJuIHR5cGVvZiBpID09PSAnc3RyaW5nJyA/XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgICAgICBpLnJlcGxhY2UoL1tcXFxcJCxdL2csICcnKSoxIDpcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgICAgIHR5cGVvZiBpID09PSAnbnVtYmVyJyA/XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgaSA6IDA7XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgfTtcXG5cXHRcXHRcXHRcXHRcXHRcXHQgXFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgdmFyIHRvdGFsID0gYXBpXFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIC5jb2x1bW4oIDMgKVxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICAuZGF0YSgpXFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgIC5yZWR1Y2UoIGZ1bmN0aW9uIChhLCBiKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgICAgICAgICByZXR1cm4gaW50VmFsKGEpICsgaW50VmFsKGIpO1xcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICB9LCAwICk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0IFxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgIHZhciBwYWdlVG90YWwgPSBhcGlcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgLmNvbHVtbiggMywgeyBwYWdlOiAnY3VycmVudCd9IClcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgLmRhdGEoKVxcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICAucmVkdWNlKCBmdW5jdGlvbiAoYSwgYikge1xcblxcdFxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGludFZhbChhKSArIGludFZhbChiKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAgICAgfSwgMCApO1xcblxcdFxcdFxcdFxcdFxcdFxcdCBcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICAvLyBVcGRhdGUgZm9vdGVyXFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgJCggYXBpLmNvbHVtbiggMCApLmZvb3RlcigpICkuaHRtbChcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICBcXHQnVG90YWwgRXN0aW1hdGVkIENvc3Q6ICYjeDIwYjE7JyArIHRvdGFsXFxuXFx0XFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgKTtcXG5cXHRcXHRcXHRcXHRcXHRcXHQgICAgICAgIH1cXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgIH0pO1xcblxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgJChmdW5jdGlvbiAoKSB7XFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAgIFxcdCQoJ1tkYXRhLXRvZ2dsZT1cXFwidG9vbHRpcFxcXCJdJykudG9vbHRpcCgpXFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICB9KTtcXG5cXHQgICAgICAgICAgICBcXHRcXHR9LCAxMDAwKTtcXG5cXHQgICAgICAgICAgICBcXHR9XFxuXFx0ICAgICAgICAgICAgfSk7XFxuXFx0XFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y3JlYXRlZCgpIHtcXG5cXHRcXHRcXHR0aGlzLmZpbHRlclRhc2tzKCd0b2RheScpO1xcblxcdFxcdH0sXFxuXFxuXFx0XFx0bW91bnRlZCgpIHtcXG5cXHRcXHRcXHQkKCcjdG9kYXlzLXRhc2stZGF0ZXBpY2tlcicpLmRhdGVwaWNrZXIoe1xcbiAgICAgICAgICAgICAgICBmb3JtYXQ6J21tL2RkL3l5eXknLFxcbiAgICAgICAgICAgICAgICB0b2RheUhpZ2hsaWdodDogdHJ1ZVxcbiAgICAgICAgICAgIH0pO1xcblxcdFxcdH1cXG5cXHR9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIGJ1dHRvbi5idG4tZmlsdGVyIHtcXG5cXHRcXHRmbG9hdDogcmlnaHQ7XFxuXFx0XFx0bGluZS1oZWlnaHQ6IDEuMjsgXFxuXFx0XFx0bWFyZ2luLXRvcDogNXB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtN2M5NWNkZWNcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9Ub2RheXNUYXNrcy52dWVcbi8vIG1vZHVsZSBpZCA9IDMwOFxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtIGJ1dHRvbi5idG4tZmlsdGVyW2RhdGEtdi1jOWQwZTEyMF0ge1xcblxcdGZsb2F0OiByaWdodDtcXG5cXHRsaW5lLWhlaWdodDogMS4yOyBcXG5cXHRtYXJnaW4tdG9wOiA1cHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJUb2RheXNTZXJ2aWNlcy52dWU/Mjc5NTg4OTdcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQThLQTtDQUNBLGFBQUE7Q0FDQSxpQkFBQTtDQUNBLGdCQUFBO0NBQ0FcIixcImZpbGVcIjpcIlRvZGF5c1NlcnZpY2VzLnZ1ZVwiLFwic291cmNlc0NvbnRlbnRcIjpbXCI8dGVtcGxhdGU+XFxuXFx0XFxuXFx0PGRpdj5cXG5cXHRcXHRcXG5cXHRcXHQ8Zm9ybSBpZD1cXFwidG9kYXlzLXNlcnZpY2VzLWZvcm1cXFwiIGNsYXNzPVxcXCJmb3JtLWlubGluZVxcXCIgQHN1Ym1pdC5wcmV2ZW50PVxcXCJmaWx0ZXJTZXJ2aWNlcygnZm9ybScpXFxcIj5cXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0ICAgICAgICA8ZGl2IGNsYXNzPVxcXCJpbnB1dC1kYXRlcmFuZ2UgaW5wdXQtZ3JvdXBcXFwiIGlkPVxcXCJ0b2RheXMtc2VydmljZXMtZGF0ZXBpY2tlclxcXCI+XFxuXFx0XFx0ICAgICAgICAgICAgPGlucHV0IHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJpbnB1dC1zbSBmb3JtLWNvbnRyb2xcXFwiIG5hbWU9XFxcInRvZGF5cy1zZXJ2aWNlLWRhdGVcXFwiIGF1dG9jb21wbGV0ZT1cXFwib2ZmXFxcIiAvPlxcblxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdCAgICBcXG5cXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0ICAgICAgICA8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeVxcXCIgc3R5bGU9XFxcImxpbmUtaGVpZ2h0OiAxLjI7XFxcIj5cXG5cXHRcXHQgICAgICAgICAgICA8c3Ryb25nPkZpbHRlcjwvc3Ryb25nPlxcblxcdFxcdCAgICAgICAgPC9idXR0b24+XFxuXFx0XFx0ICAgIDwvZGl2PlxcblxcblxcdFxcdFxcdDxidXR0b24gQGNsaWNrPVxcXCJmaWx0ZXJTZXJ2aWNlcygndG9kYXknKVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXFxcIiA6Y2xhc3M9XFxcInsnYnRuLW91dGxpbmUnIDogYWN0aXZlQnV0dG9uIT0ndG9kYXknfVxcXCIgc3R5bGU9XFxcIm1hcmdpbi1yaWdodDogMnB4O1xcXCIgdHlwZT1cXFwiYnV0dG9uXFxcIj5Ub2RheTwvYnV0dG9uPlxcblxcblxcdFxcdFxcdDxidXR0b24gQGNsaWNrPVxcXCJmaWx0ZXJTZXJ2aWNlcygneWVzdGVyZGF5JylcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclxcXCIgOmNsYXNzPVxcXCJ7J2J0bi1vdXRsaW5lJyA6IGFjdGl2ZUJ1dHRvbiE9J3llc3RlcmRheSd9XFxcIiBzdHlsZT1cXFwibWFyZ2luLXJpZ2h0OiAycHg7XFxcIiB0eXBlPVxcXCJidXR0b25cXFwiPlllc3RlcmRheTwvYnV0dG9uPlxcblxcdFxcdDwvZm9ybT5cXG5cXG5cXHRcXHQ8ZGl2IHN0eWxlPVxcXCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcXFwiPjwvZGl2PlxcblxcbiAgICBcXHQ8dGFibGUgaWQ9XFxcInRvZGF5c1NlcnZpY2VzVGFibGVcXFwiIGNsYXNzPVxcXCJ0YWJsZSB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmVkIHRhYmxlLWhvdmVyIGRhdGFUYWJsZXMtZXhhbXBsZVxcXCI+XFxuXFx0XFx0ICAgIDx0aGVhZD5cXG5cXHRcXHQgICAgICAgIDx0cj5cXG4gICAgICAgICAgICAgICAgPHRoPkRhdGU8L3RoPlxcblxcdFxcdFxcdFxcdDx0aD5OYW1lPC90aD5cXG4gICAgICAgICAgICAgICAgPHRoPklEPC90aD5cXG4gICAgICAgICAgICAgICAgPHRoPlBhY2thZ2U8L3RoPlxcbiAgICAgICAgICAgICAgICA8dGggc3R5bGU9XFxcIndpZHRoOjMwJSAhaW1wb3J0YW50XFxcIj5EZXRhaWw8L3RoPlxcbiAgICAgICAgICAgICAgICA8dGg+Q29zdDwvdGg+XFxuICAgICAgICAgICAgICAgIDx0aD5DaGFyZ2U8L3RoPlxcblxcdFxcdFxcdFxcdDx0aD5UaXA8L3RoPlxcblxcdFxcdFxcdFxcdDx0aCBzdHlsZT1cXFwid2lkdGg6NSVcXFwiPkFjdGlvbjwvdGg+XFxuXFx0XFx0ICAgICAgICA8L3RyPlxcblxcdFxcdCAgICA8L3RoZWFkPlxcblxcdFxcdCAgICA8dGJvZHk+XFxuXFx0XFx0ICAgICAgICA8dHIgdi1mb3I9XFxcInNlcnZpY2UgaW4gZmlsU2VydmljZXNcXFwiPlxcblxcdFxcdCAgICAgICBcXHRcXHQ8dGQ+e3sgc2VydmljZS5zZXJ2aWNlX2RhdGUgfX08L3RkPlxcblxcdFxcdCAgICAgICBcXHRcXHQ8dGQ+PHNwYW4gdi1pZj1cXFwic2VydmljZS51c2VyIT1udWxsXFxcIj57eyBzZXJ2aWNlLnVzZXIuZnVsbF9uYW1lIH19PC9zcGFuPlxcblxcdFxcdFxcdFxcdFxcdFxcdDxhIGNsYXNzPVxcXCJwdWxsLXJpZ2h0XFxcIiBocmVmPVxcXCIjXFxcIiBkYXRhLXRvZ2dsZT1cXFwicG9wb3ZlclxcXCIgZGF0YS1wbGFjZW1lbnQ9XFxcInRvcFxcXCIgZGF0YS1jb250YWluZXI9XFxcImJvZHlcXFwiIDpkYXRhLWNvbnRlbnQ9XFxcInNlcnZpY2UudXNlci5ncm91cF9iaW5kZWRcXFwiIGRhdGEtdHJpZ2dlcj1cXFwiaG92ZXJcXFwiIHYtaWY9XFxcInNlcnZpY2UuZ3JvdXBfYmluZGVkICE9ICcnXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQ8YiBjbGFzcz1cXFwiZmEgZmEtdXNlci1jaXJjbGUgZ3JvdXAtaWNvbiBtLXItMlxcXCI+PC9iPlxcblxcdFxcdFxcdFxcdFxcdFxcdDwvYT5cXG5cXHRcXHRcXHRcXHRcXHRcXHQ8YSBjbGFzcz1cXFwicHVsbC1yaWdodFxcXCIgaHJlZj1cXFwiI1xcXCIgZGF0YS10b2dnbGU9XFxcInBvcG92ZXJcXFwiIGRhdGEtcGxhY2VtZW50PVxcXCJ0b3BcXFwiIGRhdGEtY29udGFpbmVyPVxcXCJib2R5XFxcIiA6ZGF0YS1jb250ZW50PVxcXCInVW5yZWFkIE5vdGlmaWNhdGlvbnMgOiAnK3NlcnZpY2UudXNlci51bnJlYWRfbm90aWZcXFwiIGRhdGEtdHJpZ2dlcj1cXFwiaG92ZXJcXFwiICB2LWlmPVxcXCJzZXJ2aWNlLnVzZXIudW5yZWFkX25vdGlmID4gMFxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0PGIgY2xhc3M9XFxcImZhIGZhLWVudmVsb3BlIHVucmVhZC1pY29uIG0tci0xXFxcIj48L2I+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PC9hPlxcblxcdFxcdFxcdFxcdFxcdFxcdDxhIGNsYXNzPVxcXCJwdWxsLXJpZ2h0XFxcIiBocmVmPVxcXCIjXFxcIiBkYXRhLXRvZ2dsZT1cXFwicG9wb3ZlclxcXCIgZGF0YS1wbGFjZW1lbnQ9XFxcInRvcFxcXCIgZGF0YS1jb250YWluZXI9XFxcImJvZHlcXFwiIGRhdGEtY29udGVudD1cXFwiQXBwIEluc3RhbGxlZCBieSBDbGllbnRcXFwiIGRhdGEtdHJpZ2dlcj1cXFwiaG92ZXJcXFwiICB2LWlmPVxcXCJzZXJ2aWNlLnVzZXIuYmluZGVkXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQ8YiBjbGFzcz1cXFwiZmEgZmEtbW9iaWxlIGFwcC1pY29uIG0tci0xXFxcIj48L2I+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PC9hPlxcblxcdFxcdFxcdFxcdFxcdDwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0PHRkPjxzcGFuIHYtaWY9XFxcInNlcnZpY2UudXNlciE9bnVsbFxcXCI+e3sgc2VydmljZS51c2VyLmlkIH19PC9zcGFuPjwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0PHRkPnt7IHNlcnZpY2UudHJhY2tpbmcgfX08L3RkPlxcblxcdFxcdFxcdFxcdFxcdDx0ZCBzdHlsZT1cXFwid2lkdGg6MzAlICFpbXBvcnRhbnRcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdDxhIGhyZWY9XFxcIiNcXFwiIHYtaWY9XFxcInNlcnZpY2UucmVtYXJrcyE9JydcXFwiIGRhdGEtdG9nZ2xlPVxcXCJwb3BvdmVyXFxcIiBkYXRhLXBsYWNlbWVudD1cXFwicmlnaHRcXFwiIGRhdGEtY29udGFpbmVyPVxcXCJib2R5XFxcIiA6ZGF0YS1jb250ZW50PVxcXCJzZXJ2aWNlLnJlbWFya3NcXFwiIGRhdGEtdHJpZ2dlcj1cXFwiaG92ZXJcXFwiID5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgIHt7IHNlcnZpY2UuZGV0YWlsIH19XFxuXFx0XFx0XFx0ICAgICAgICAgICAgPC9hPlxcblxcdFxcdFxcdCAgICAgICAgICAgIDxzcGFuIHYtZWxzZT57eyBzZXJ2aWNlLmRldGFpbCB9fTwvc3Bhbj5cXG5cXHRcXHRcXHQgICAgICAgIDwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0PHRkPnt7IHNlcnZpY2UuY29zdCB9fTwvdGQ+XFxuXFx0XFx0XFx0XFx0XFx0PHRkPnt7IHNlcnZpY2UuY2hhcmdlIH19PC90ZD5cXG5cXHRcXHRcXHRcXHRcXHQ8dGQ+e3sgc2VydmljZS50aXAgfX08L3RkPlxcblxcdFxcdFxcdFxcdFxcdDx0ZCBjbGFzcz1cXFwidGV4dC1jZW50ZXJcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdDxhIGhyZWY9XFxcIiNcXFwiIGRhdGEtdG9nZ2xlPVxcXCJtb2RhbFxcXCIgZGF0YS10YXJnZXQ9XFxcIiNlZGl0U2VydmljZU1vZGFsXFxcIiBAY2xpY2s9XFxcIiRwYXJlbnQuc2V0U2VydmljZShzZXJ2aWNlKVxcXCIgdi1jbG9haz5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQ8aSBjbGFzcz1cXFwiZmEgZmEtcGVuY2lsLXNxdWFyZS1vXFxcIj48L2k+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PC9hPlxcblxcdFxcdFxcdFxcdFxcdFxcdDxhIDpocmVmPVxcXCInL3Zpc2EvY2xpZW50Lycrc2VydmljZS5jbGllbnRfaWRcXFwiIHRhcmdldD1cXFwiX2JsYW5rXFxcIiBkYXRhLXRvZ2dsZT1cXFwidG9vbHRpcFxcXCIgZGF0YS1wbGFjZW1lbnQ9XFxcImxlZnRcXFwiIHRpdGxlPVxcXCJWaWV3IGNsaWVudCBwcm9maWxlXFxcIj48aSBjbGFzcz1cXFwiZmEgZmEtYXJyb3ctcmlnaHRcXFwiPjwvaT48L2E+XFxuXFx0XFx0XFx0XFx0XFx0PC90ZD5cXHRcXG5cXHRcXHQgICAgICAgIDwvdHI+XFxuXFx0XFx0ICAgIDwvdGJvZHk+XFxuXFx0XFx0PC90YWJsZT5cXG5cXG5cXHQ8L2Rpdj5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFx0XFxuXFx0ZXhwb3J0IGRlZmF1bHQge1xcblxcdFxcdGRhdGEoKSB7XFxuXFx0XFx0XFx0cmV0dXJuIHtcXG5cXHRcXHRcXHRcXHRhY3RpdmVCdXR0b246ICd0b2RheScsXFxuXFxuXFx0XFx0XFx0XFx0ZGF0ZUZpbHRlcjogJycsXFxuXFxuXFx0XFx0XFx0XFx0ZmlsU2VydmljZXM6IFtdXFxuXFx0XFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0bWV0aG9kczoge1xcblxcdFxcdFxcdGZvcm1hdERhdGVQaWNrZXJWYWx1ZSh2YWx1ZSkge1xcblxcdFxcdFxcdFxcdGxldCBkYXRlUGlja2VyVmFsdWUgPSB2YWx1ZS5zcGxpdCgnLycpO1xcblxcblxcdFxcdFxcdFxcdHJldHVybiBkYXRlUGlja2VyVmFsdWVbMl0gKyAnLScgKyBkYXRlUGlja2VyVmFsdWVbMF0gKyAnLScgKyBkYXRlUGlja2VyVmFsdWVbMV07XFxuXFx0XFx0XFx0fSxcXG5cXG5cXHRcXHRcXHRmb3JtYXREYXRlKHZhbHVlKSB7XFxuXFx0XFx0XFx0XFx0bGV0IGRhdGUgPSB2YWx1ZS5zcGxpdCgnLScpO1xcblxcdFxcdFxcdFxcdGxldCBuZXdEYXRlID0gZGF0ZVsxXSArICcvJyArIGRhdGVbMl0gKyAnLycgKyBkYXRlWzBdO1xcblxcblxcdFxcdFxcdFxcdHJldHVybiBuZXdEYXRlO1xcblxcdFxcdFxcdH0sXFxuXFxuXFx0XFx0XFx0ZmlsdGVyU2VydmljZXMoZGF0ZSkge1xcblxcdFxcdFxcdFxcdGlmKGRhdGUgPT0gJ2Zvcm0nKSB7XFxuXFx0XFx0XFx0XFx0XFx0bGV0IGRhdGVQaWNrZXJWYWx1ZSA9ICQoJ2Zvcm0jdG9kYXlzLXNlcnZpY2VzLWZvcm0gaW5wdXRbbmFtZT10b2RheXMtc2VydmljZS1kYXRlXScpLnZhbCgpO1xcblxcblxcdFxcdFxcdFxcdFxcdGlmKGRhdGVQaWNrZXJWYWx1ZSA9PSAnJykge1xcblxcdFxcdFxcdFxcdFxcdFxcdGRhdGVQaWNrZXJWYWx1ZSA9IG1vbWVudCgpLmZvcm1hdCgnTU0vREQvWVlZWScpO1xcblxcdFxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0ZGF0ZVBpY2tlclZhbHVlID0gZGF0ZVBpY2tlclZhbHVlO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmRhdGVGaWx0ZXIgPSBkYXRlUGlja2VyVmFsdWU7XFxuXFx0XFx0XFx0XFx0fSBlbHNlIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmFjdGl2ZUJ1dHRvbiA9IGRhdGU7XFxuXFxuXFx0XFx0XFx0XFx0XFx0aWYoZGF0ZSA9PSAndG9kYXknKVxcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuZGF0ZUZpbHRlciA9IG1vbWVudCgpLmZvcm1hdCgnTU0vREQvWVlZWScpO1xcblxcdFxcdFxcdFxcdFxcdGVsc2UgaWYoZGF0ZSA9PSAneWVzdGVyZGF5JylcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmRhdGVGaWx0ZXIgPSBtb21lbnQobmV3IERhdGUoKSkuYWRkKC0xLCdkYXlzJykuZm9ybWF0KCdNTS9ERC9ZWVlZJyk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0JCgnZm9ybSN0b2RheXMtc2VydmljZXMtZm9ybSBpbnB1dFtuYW1lPXRvZGF5cy1zZXJ2aWNlLWRhdGVdJykudmFsKHRoaXMuZGF0ZUZpbHRlcik7XFxuXFx0XFx0XFx0XFx0fVxcblxcblxcdFxcdFxcdFxcdGF4aW9zLmdldCgnL3Zpc2EvaG9tZS9nZXQtc2VydmljZS1ieURhdGUnLCB7XFxuXFx0ICAgICAgICAgICAgICAgIHBhcmFtczoge1xcblxcdCAgICAgICAgICAgICAgICBcXHRkYXRlRmlsdGVyOiB0aGlzLmRhdGVGaWx0ZXJcXG5cXHQgICAgICAgICAgICAgICAgfVxcblxcdCAgICAgICAgICAgIH0pXFxuXFx0ICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdCAgICAgICAgICAgIFxcdGxldCB7IHN1Y2Nlc3MsIHNlcnZpY2VzIH0gPSByZXNwb25zZS5kYXRhO1xcblxcblxcdCAgICAgICAgICAgIFxcdGlmKHN1Y2Nlc3MpIHtcXG5cXHQgICAgICAgICAgICBcXHRcXHQkKCcjdG9kYXlzU2VydmljZXNUYWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcXG5cXG5cXHQgICAgICAgICAgICBcXHRcXHR0aGlzLmZpbFNlcnZpY2VzID0gc2VydmljZXM7XFxuXFxuXFx0ICAgICAgICAgICAgXFx0XFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0ICAgICAgICAgICAgXFx0XFx0XFx0JCgnI3RvZGF5c1NlcnZpY2VzVGFibGUnKS5EYXRhVGFibGUoe1xcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAgICAgYXV0b1dpZHRoOiBmYWxzZSxcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICBcXFwiY29sdW1uRGVmc1xcXCI6IFtcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBcXFwid2lkdGhcXFwiOiBcXFwiMjUlXFxcIiwgXFxcInRhcmdldHNcXFwiOiAxIH1cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICBdLFxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgICAgIFxcXCJsZW5ndGhNZW51XFxcIjogW1sxMCwgMjUsIDUwXSwgWzEwLCAyNSwgNTBdXSxcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgICBcXFwiaURpc3BsYXlMZW5ndGhcXFwiOiAxMFxcblxcdFxcdCAgICAgICAgICAgICAgICAgICAgfSk7XFxuXFxuXFx0XFx0ICAgICAgICAgICAgICAgICAgICAkKGZ1bmN0aW9uICgpIHtcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgICAgXFx0JCgnW2RhdGEtdG9nZ2xlPVxcXCJ0b29sdGlwXFxcIl0nKS50b29sdGlwKClcXG5cXHRcXHQgICAgICAgICAgICAgICAgICAgIH0pO1xcblxcdCAgICAgICAgICAgIFxcdFxcdH0sIDEwMDApO1xcblxcdCAgICAgICAgICAgIFxcdH1cXG5cXHQgICAgICAgICAgICB9KTtcXG5cXHRcXHRcXHR9XFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRjcmVhdGVkKCkge1xcblxcdFxcdFxcdHRoaXMuZmlsdGVyU2VydmljZXMoJ3RvZGF5Jyk7XFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRtb3VudGVkKCkge1xcblxcdFxcdFxcdCQoJyN0b2RheXMtc2VydmljZXMtZGF0ZXBpY2tlcicpLmRhdGVwaWNrZXIoe1xcbiAgICAgICAgICAgICAgICBmb3JtYXQ6J21tL2RkL3l5eXknLFxcbiAgICAgICAgICAgICAgICB0b2RheUhpZ2hsaWdodDogdHJ1ZVxcbiAgICAgICAgICAgIH0pO1xcblxcdFxcdH1cXG5cXHR9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIGJ1dHRvbi5idG4tZmlsdGVyIHtcXG5cXHRcXHRmbG9hdDogcmlnaHQ7XFxuXFx0XFx0bGluZS1oZWlnaHQ6IDEuMjsgXFxuXFx0XFx0bWFyZ2luLXRvcDogNXB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYzlkMGUxMjBcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9Ub2RheXNTZXJ2aWNlcy52dWVcbi8vIG1vZHVsZSBpZCA9IDMxM1xuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zYWZkYjU1M1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0VkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9FZGl0U2VydmljZURhc2hib2FyZC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTNhZmRiNTUzXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0VkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi0zYWZkYjU1M1wiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL0VkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEVkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0zYWZkYjU1M1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTNhZmRiNTUzXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0U2VydmljZURhc2hib2FyZC52dWVcbi8vIG1vZHVsZSBpZCA9IDM0NlxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi02YjU4MWFjYlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0RlbGl2ZXJ5U2NoZWR1bGUudnVlXCIpXG5cbnZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0RlbGl2ZXJ5U2NoZWR1bGUudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi02YjU4MWFjYlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9EZWxpdmVyeVNjaGVkdWxlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi02YjU4MWFjYlwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RlbGl2ZXJ5U2NoZWR1bGUudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRGVsaXZlcnlTY2hlZHVsZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNmI1ODFhY2JcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi02YjU4MWFjYlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGVsaXZlcnlTY2hlZHVsZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM2NVxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1jOWQwZTEyMFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL1RvZGF5c1NlcnZpY2VzLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Ub2RheXNTZXJ2aWNlcy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LWM5ZDBlMTIwXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1RvZGF5c1NlcnZpY2VzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi1jOWQwZTEyMFwiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL1RvZGF5c1NlcnZpY2VzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFRvZGF5c1NlcnZpY2VzLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1jOWQwZTEyMFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWM5ZDBlMTIwXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9Ub2RheXNTZXJ2aWNlcy52dWVcbi8vIG1vZHVsZSBpZCA9IDM3MFxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJcbi8qIHN0eWxlcyAqL1xucmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03Yzk1Y2RlY1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL1RvZGF5c1Rhc2tzLnZ1ZVwiKVxuXG52YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Ub2RheXNUYXNrcy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTdjOTVjZGVjXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1RvZGF5c1Rhc2tzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBcImRhdGEtdi03Yzk1Y2RlY1wiLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL1RvZGF5c1Rhc2tzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFRvZGF5c1Rhc2tzLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi03Yzk1Y2RlY1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTdjOTVjZGVjXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9Ub2RheXNUYXNrcy52dWVcbi8vIG1vZHVsZSBpZCA9IDM3MVxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2Zvcm0nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ob3Jpem9udGFsXCIsXG4gICAgb246IHtcbiAgICAgIFwic3VibWl0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgcmV0dXJuIF92bS5zYXZlU2VydmljZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIFtfYygnZGl2Jywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImNsYXNzc1wiOiBcImNvbC1tZC0xMlwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwIGNvbC1tZC02XCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHQgICAgICAgIFRpcDpcXG5cXHRcXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLThcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLnNhdmVTZXJ2aWNlRm9ybS50aXApLFxuICAgICAgZXhwcmVzc2lvbjogXCJzYXZlU2VydmljZUZvcm0udGlwXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjE3NXB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJudW1iZXJcIixcbiAgICAgIFwibWluXCI6IFwiMFwiLFxuICAgICAgXCJuYW1lXCI6IFwidGlwXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uc2F2ZVNlcnZpY2VGb3JtLnRpcClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5zYXZlU2VydmljZUZvcm0sIFwidGlwXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cCBjb2wtbWQtNiBtLWwtMlwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTQgY29udHJvbC1sYWJlbCBcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0ICAgICAgICBTdGF0dXM6XFxuXFx0XFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC04XCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uc2F2ZVNlcnZpY2VGb3JtLnN0YXR1cyksXG4gICAgICBleHByZXNzaW9uOiBcInNhdmVTZXJ2aWNlRm9ybS5zdGF0dXNcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMTc1cHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwibmFtZVwiOiBcInN0YXR1c1wiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjaGFuZ2VcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIHZhciAkJHNlbGVjdGVkVmFsID0gQXJyYXkucHJvdG90eXBlLmZpbHRlci5jYWxsKCRldmVudC50YXJnZXQub3B0aW9ucywgZnVuY3Rpb24obykge1xuICAgICAgICAgIHJldHVybiBvLnNlbGVjdGVkXG4gICAgICAgIH0pLm1hcChmdW5jdGlvbihvKSB7XG4gICAgICAgICAgdmFyIHZhbCA9IFwiX3ZhbHVlXCIgaW4gbyA/IG8uX3ZhbHVlIDogby52YWx1ZTtcbiAgICAgICAgICByZXR1cm4gdmFsXG4gICAgICAgIH0pO1xuICAgICAgICBfdm0uJHNldChfdm0uc2F2ZVNlcnZpY2VGb3JtLCBcInN0YXR1c1wiLCAkZXZlbnQudGFyZ2V0Lm11bHRpcGxlID8gJCRzZWxlY3RlZFZhbCA6ICQkc2VsZWN0ZWRWYWxbMF0pXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ29wdGlvbicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJ2YWx1ZVwiOiBcImNvbXBsZXRlXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDb21wbGV0ZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwib24gcHJvY2Vzc1wiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiT24gUHJvY2Vzc1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwicGVuZGluZ1wiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiUGVuZGluZ1wiKV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiY2xhc3NzXCI6IFwiY29sLW1kLTEyXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXAgY29sLW1kLTZcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICAgQ29zdDpcXG5cXHRcXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLThcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLnNhdmVTZXJ2aWNlRm9ybS5jb3N0KSxcbiAgICAgIGV4cHJlc3Npb246IFwic2F2ZVNlcnZpY2VGb3JtLmNvc3RcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMTc1cHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcIm51bWJlclwiLFxuICAgICAgXCJtaW5cIjogXCIwXCIsXG4gICAgICBcIm5hbWVcIjogXCJjb3N0XCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uc2F2ZVNlcnZpY2VGb3JtLmNvc3QpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uc2F2ZVNlcnZpY2VGb3JtLCBcImNvc3RcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwIGNvbC1tZC02IG0tbC0yXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsIFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHQgICAgICAgIERpc2NvdW50OlxcblxcdFxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtOFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uc2F2ZVNlcnZpY2VGb3JtLmRpc2NvdW50KSxcbiAgICAgIGV4cHJlc3Npb246IFwic2F2ZVNlcnZpY2VGb3JtLmRpc2NvdW50XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjE3NXB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJudW1iZXJcIixcbiAgICAgIFwibmFtZVwiOiBcImRpc2NvdW50XCIsXG4gICAgICBcIm1pblwiOiBcIjBcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5zYXZlU2VydmljZUZvcm0uZGlzY291bnQpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uc2F2ZVNlcnZpY2VGb3JtLCBcImRpc2NvdW50XCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJjbGFzc3NcIjogXCJjb2wtbWQtMTJcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHQgICAgICAgIFJlYXNvbjpcXG5cXHRcXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5zYXZlU2VydmljZUZvcm0ucmVhc29uKSxcbiAgICAgIGV4cHJlc3Npb246IFwic2F2ZVNlcnZpY2VGb3JtLnJlYXNvblwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwibmFtZVwiOiBcInJlYXNvblwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLnNhdmVTZXJ2aWNlRm9ybS5yZWFzb24pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uc2F2ZVNlcnZpY2VGb3JtLCBcInJlYXNvblwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiY2xhc3NzXCI6IFwiY29sLW1kLTEyXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0ICAgICAgICBOb3RlOlxcblxcdFxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLnNhdmVTZXJ2aWNlRm9ybS5ub3RlKSxcbiAgICAgIGV4cHJlc3Npb246IFwic2F2ZVNlcnZpY2VGb3JtLm5vdGVcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJub3RlXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uc2F2ZVNlcnZpY2VGb3JtLm5vdGUpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uc2F2ZVNlcnZpY2VGb3JtLCBcIm5vdGVcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImNsYXNzc1wiOiBcImNvbC1tZC0xMlwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwIGNvbC1tZC02XCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICAgU3RhdHVzOlxcblxcdFxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtOFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImhlaWdodFwiOiBcIjVweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHt9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLnNhdmVTZXJ2aWNlRm9ybS5hY3RpdmUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzYXZlU2VydmljZUZvcm0uYWN0aXZlXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwicmFkaW9cIixcbiAgICAgIFwidmFsdWVcIjogXCIwXCIsXG4gICAgICBcIm5hbWVcIjogXCJhY3RpdmVcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBfdm0uX3EoX3ZtLnNhdmVTZXJ2aWNlRm9ybS5hY3RpdmUsIFwiMFwiKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJHNldChfdm0uc2F2ZVNlcnZpY2VGb3JtLCBcImFjdGl2ZVwiLCBcIjBcIilcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaScpLCBfdm0uX3YoXCIgXFxuXFx0XFx0XFx0XFx0ICAgICAgICBcXHREaXNhYmxlZCBcXG5cXHRcXHRcXHRcXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibS1sLTJcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLnNhdmVTZXJ2aWNlRm9ybS5hY3RpdmUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzYXZlU2VydmljZUZvcm0uYWN0aXZlXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwicmFkaW9cIixcbiAgICAgIFwidmFsdWVcIjogXCIxXCIsXG4gICAgICBcIm5hbWVcIjogXCJhY3RpdmVcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBfdm0uX3EoX3ZtLnNhdmVTZXJ2aWNlRm9ybS5hY3RpdmUsIFwiMVwiKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJHNldChfdm0uc2F2ZVNlcnZpY2VGb3JtLCBcImFjdGl2ZVwiLCBcIjFcIilcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaScpLCBfdm0uX3YoXCIgXFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXHRFbmFibGVkIFxcblxcdCAgICAgICAgICAgICAgICAgICAgXCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXAgY29sLW1kLTZcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC00IGNvbnRyb2wtbGFiZWwgXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICAgRXh0ZW5kIFRvOlxcblxcdFxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNlwiXG4gIH0sIFtfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWlubGluZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwiZGFpbHktZm9ybVwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMCksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5zYXZlU2VydmljZUZvcm0uZXh0ZW5kKSxcbiAgICAgIGV4cHJlc3Npb246IFwic2F2ZVNlcnZpY2VGb3JtLmV4dGVuZFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGVcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCIsXG4gICAgICBcIm5hbWVcIjogXCJkYXRlXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uc2F2ZVNlcnZpY2VGb3JtLmV4dGVuZClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5zYXZlU2VydmljZUZvcm0sIFwiZXh0ZW5kXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hvc2VuLXNlbGVjdC1mb3ItcmVxdWlyZWQtZG9jc1wiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMzUwcHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1wbGFjZWhvbGRlclwiOiBcIlNlbGVjdCBEb2NzXCIsXG4gICAgICBcIm11bHRpcGxlXCI6IFwiXCIsXG4gICAgICBcInRhYmluZGV4XCI6IFwiNFwiXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5yZXF1aXJlZERvY3MpLCBmdW5jdGlvbihkb2MpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogZG9jLmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXHRcIiArIF92bS5fcygoZG9jLnRpdGxlKS50cmltKCkpICsgXCJcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXCIpXSlcbiAgfSksIDApXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICBcXHRPcHRpb25hbCBEb2N1bWVudHM6XFxuXFx0XFx0XFx0XFx0ICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnc2VsZWN0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNob3Nlbi1zZWxlY3QtZm9yLW9wdGlvbmFsLWRvY3NcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjM1MHB4XCJcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtcGxhY2Vob2xkZXJcIjogXCJTZWxlY3QgRG9jc1wiLFxuICAgICAgXCJtdWx0aXBsZVwiOiBcIlwiLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIjRcIlxuICAgIH1cbiAgfSwgX3ZtLl9sKChfdm0ub3B0aW9uYWxEb2NzKSwgZnVuY3Rpb24oZG9jKSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IGRvYy5pZFxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXFx0XCIgKyBfdm0uX3MoKGRvYy50aXRsZSkudHJpbSgpKSArIFwiXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIFwiKV0pXG4gIH0pLCAwKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX20oMildKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdCAgICAgICBcXHRSZXF1aXJlZCBEb2N1bWVudHM6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMlwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJTYXZlXCIpXSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTNhZmRiNTUzXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtM2FmZGI1NTNcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9FZGl0U2VydmljZURhc2hib2FyZC52dWVcbi8vIG1vZHVsZSBpZCA9IDM4NlxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCI8dGVtcGxhdGU+XG48ZGl2IGNsYXNzPVwibW9kYWwgbWQtbW9kYWwgZmFkZVwiIDppZD1cImlkXCIgdGFiaW5kZXg9XCItMVwiIHJvbGU9XCJkaWFsb2dcIiBhcmlhLWxhYmVsbGVkYnk9XCJtb2RhbC1sYWJlbFwiPlxuICAgIDxkaXYgOmNsYXNzPVwiJ21vZGFsLWRpYWxvZyAnK3NpemVcIiByb2xlPVwiZG9jdW1lbnRcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cbiAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJtb2RhbC10aXRsZVwiIGlkPVwibW9kYWwtbGFiZWxcIj5cbiAgICAgICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLXRpdGxlXCI+TW9kYWwgVGl0bGU8L3Nsb3Q+XG4gICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtYm9keVwiPk1vZGFsIEJvZHk8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5XCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbjwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICBwcm9wczoge1xuICAgICAgICAnaWQnOntyZXF1aXJlZDp0cnVlfVxuICAgICAgICAsJ3NpemUnOiB7ZGVmYXVsdDonbW9kYWwtbWQnfVxuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIERpYWxvZ01vZGFsLnZ1ZT8wMDNiZGE4OCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2JywgW19jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taW5saW5lXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJkZWxpdmVyeS1zY2hlZC1mb3JtXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIF92bS5maWx0ZXJTY2hlZCgnZm9ybScpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl9tKDApLCBfdm0uX3YoXCIgXCIpLCBfdm0uX20oMSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi13YXJuaW5nXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibGluZS1oZWlnaHRcIjogXCIxLjJcIixcbiAgICAgIFwibWFyZ2luLWxlZnRcIjogXCIycHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJhZGRfc2NoZWR1bGVcIixcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnNob3dTY2hlZHVsZU1vZGFsKCdhZGQnLCAnJylcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJBZGQgU2NoZWR1bGUgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1maWx0ZXJcIixcbiAgICBjbGFzczoge1xuICAgICAgJ2J0bi1vdXRsaW5lJzogX3ZtLmFjdGl2ZUJ1dHRvbiAhPSAndG9kYXknXG4gICAgfSxcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJtYXJnaW4tcmlnaHRcIjogXCIycHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZpbHRlclNjaGVkKCd0b2RheScpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiVG9kYXlcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnYnRuLW91dGxpbmUnOiBfdm0uYWN0aXZlQnV0dG9uICE9ICd5ZXN0ZXJkYXknXG4gICAgfSxcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJtYXJnaW4tcmlnaHRcIjogXCIycHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZpbHRlclNjaGVkKCd0b21vcnJvdycpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiVG9tb3Jyb3dcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImhlaWdodFwiOiBcIjIwcHhcIixcbiAgICAgIFwiY2xlYXJcIjogXCJib3RoXCJcbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGFibGUnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJlZCB0YWJsZS1ob3ZlciBkYXRhVGFibGVzLWV4YW1wbGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImRlbGl2ZXJ5U2NoZWR1bGVcIlxuICAgIH1cbiAgfSwgW192bS5fbSgyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0uZGVsaXZlcnlTY2hlZHVsZXMpLCBmdW5jdGlvbihkcykge1xuICAgIHJldHVybiBfYygndHInLCB7fSwgW19jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKGRzLml0ZW0pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhkcy5zY2hlZHVsZWRfZGF0ZSkgKyBcIiBcIiArIF92bS5fcyhkcy5zY2hlZHVsZWRfdGltZSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKGRzLmxvY2F0aW9uKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW19jKCdzcGFuJywgW192bS5fdihfdm0uX3MoZHMucmlkZXIuZnVsbF9uYW1lKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX2MoJ2EnLCB7XG4gICAgICBvbjoge1xuICAgICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS5zaG93U2NoZWR1bGVNb2RhbCgnZWRpdCcsIGRzLmlkKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIkVkaXRcIildKSwgX3ZtLl92KFwiIHwgXCIpLCBfYygnYScsIHtcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLiRwYXJlbnQuc2hvd0RlbGV0ZVNjaGVkUHJvbXB0KGRzLmlkKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIkRlbGV0ZVwiKV0pXSldKVxuICB9KSwgMCldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZGF0ZXJhbmdlIGlucHV0LWdyb3VwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJkZWxpdmVyeS1zY2hlZHMtZGF0ZXBpY2tlclwiXG4gICAgfVxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LXNtIGZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJkZWxpdmVyeS1zY2hlZC1kYXRlXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnlcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJsaW5lLWhlaWdodFwiOiBcIjEuMlwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfYygnc3Ryb25nJywgW192bS5fdihcIkZpbHRlclwiKV0pXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0aGVhZCcsIFtfYygndHInLCBbX2MoJ3RoJywgW192bS5fdihcIkl0ZW1cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIlNjaGVkdWxlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJMb2NhdGlvblwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiUmlkZXJcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkFjdGlvblwiKV0pXSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTZiNTgxYWNiXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNmI1ODFhY2JcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EZWxpdmVyeVNjaGVkdWxlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDA0XG4vLyBtb2R1bGUgY2h1bmtzID0gNSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2JywgW19jKCdmb3JtJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0taW5saW5lXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0b2RheXMtdGFza3MtZm9ybVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBfdm0uZmlsdGVyVGFza3MoJ2Zvcm0nKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl9tKDEpLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdidG4tb3V0bGluZSc6IF92bS5hY3RpdmVCdXR0b24gIT0gJ3RvbW9ycm93J1xuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZpbHRlclRhc2tzKCd0b21vcnJvdycpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiVG9tb3Jyb3dcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnYnRuLW91dGxpbmUnOiBfdm0uYWN0aXZlQnV0dG9uICE9ICd0b2RheSdcbiAgICB9LFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIm1hcmdpbi1yaWdodFwiOiBcIjJweFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZmlsdGVyVGFza3MoJ3RvZGF5JylcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJUb2RheVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBidG4tZmlsdGVyXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdidG4tb3V0bGluZSc6IF92bS5hY3RpdmVCdXR0b24gIT0gJ3llc3RlcmRheSdcbiAgICB9LFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIm1hcmdpbi1yaWdodFwiOiBcIjJweFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZmlsdGVyVGFza3MoJ3llc3RlcmRheScpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiWWVzdGVyZGF5XCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCIyMHB4XCIsXG4gICAgICBcImNsZWFyXCI6IFwiYm90aFwiXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RhYmxlJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVyZWQgdGFibGUtaG92ZXIgZGF0YVRhYmxlcy1leGFtcGxlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0b2RheXNUYXNrc0xpc3RzXCJcbiAgICB9XG4gIH0sIFtfdm0uX20oMiksIF92bS5fdihcIiBcIiksIF9jKCd0Ym9keScsIF92bS5fbCgoX3ZtLnRhc2tzKSwgZnVuY3Rpb24odGFzaykge1xuICAgIHJldHVybiBfYygndHInLCBbX2MoJ3RkJywgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvdmlzYS9jbGllbnQvJyArIHRhc2suY2xpZW50X3NlcnZpY2UudXNlci5pZCxcbiAgICAgICAgXCJ0YXJnZXRcIjogXCJfYmxhbmtcIixcbiAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcbiAgICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInJpZ2h0XCIsXG4gICAgICAgIFwidGl0bGVcIjogXCJWaWV3IGNsaWVudCBwcm9maWxlXCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgIFxcdFxcdFxcdFxcdFwiICsgX3ZtLl9zKHRhc2suY2xpZW50X3NlcnZpY2UudXNlci5mdWxsX25hbWUpICsgXCJcXG5cXHRcXHQgICAgICAgXFx0XFx0XFx0XCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3ModGFzay5jbGllbnRfc2VydmljZS51c2VyLmlkKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3ModGFzay5jbGllbnRfc2VydmljZS5zZXJ2aWNlX2NhdGVnb3J5LmRldGFpbCkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgXFx0XFx0XFx0XCIgKyBfdm0uX3ModGFzay5jbGllbnRfc2VydmljZS5zZXJ2aWNlX2NhdGVnb3J5LmVzdGltYXRlZF9jb3N0KSArIFwiXFxuXFx0XFx0ICAgICAgIFxcdFxcdFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyh0YXNrLnN0YXR1cykpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKHRhc2suZmlsaW5nX2RhdGUpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyh0YXNrLnJlbGVhc2luZ19kYXRlKSldKV0pXG4gIH0pLCAwKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl9tKDMpXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWRhdGVyYW5nZSBpbnB1dC1ncm91cFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidG9kYXlzLXRhc2stZGF0ZXBpY2tlclwiXG4gICAgfVxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LXNtIGZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJ0b2RheXMtdGFzay1kYXRlXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnlcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJsaW5lLWhlaWdodFwiOiBcIjEuMlwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfYygnc3Ryb25nJywgW192bS5fdihcIkZpbHRlclwiKV0pXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0aGVhZCcsIFtfYygndHInLCBbX2MoJ3RoJywgW192bS5fdihcIk5hbWVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkNsaWVudCBOby5cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIlNlcnZpY2VcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkVzdGltYXRlZCBDb3N0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJTdGF0dXNcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkZpbGluZyBEYXRlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJSZWxlYXNpbmcgRGF0ZVwiKV0pXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0Zm9vdCcsIFtfYygndHInLCBbX2MoJ3RoJywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcInRleHQtYWxpZ25cIjogXCJyaWdodFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJjb2xzcGFuXCI6IFwiN1wiXG4gICAgfVxuICB9KV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi03Yzk1Y2RlY1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTdjOTVjZGVjXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvVG9kYXlzVGFza3MudnVlXG4vLyBtb2R1bGUgaWQgPSA0MTBcbi8vIG1vZHVsZSBjaHVua3MgPSA1IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2Zvcm0nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1pbmxpbmVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcInRvZGF5cy1zZXJ2aWNlcy1mb3JtXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcInN1Ym1pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIF92bS5maWx0ZXJTZXJ2aWNlcygnZm9ybScpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl9tKDApLCBfdm0uX3YoXCIgXCIpLCBfdm0uX20oMSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1maWx0ZXJcIixcbiAgICBjbGFzczoge1xuICAgICAgJ2J0bi1vdXRsaW5lJzogX3ZtLmFjdGl2ZUJ1dHRvbiAhPSAndG9kYXknXG4gICAgfSxcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJtYXJnaW4tcmlnaHRcIjogXCIycHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZpbHRlclNlcnZpY2VzKCd0b2RheScpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiVG9kYXlcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnkgYnRuLWZpbHRlclwiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnYnRuLW91dGxpbmUnOiBfdm0uYWN0aXZlQnV0dG9uICE9ICd5ZXN0ZXJkYXknXG4gICAgfSxcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJtYXJnaW4tcmlnaHRcIjogXCIycHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZpbHRlclNlcnZpY2VzKCd5ZXN0ZXJkYXknKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihcIlllc3RlcmRheVwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMjBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmVkIHRhYmxlLWhvdmVyIGRhdGFUYWJsZXMtZXhhbXBsZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidG9kYXlzU2VydmljZXNUYWJsZVwiXG4gICAgfVxuICB9LCBbX3ZtLl9tKDIpLCBfdm0uX3YoXCIgXCIpLCBfYygndGJvZHknLCBfdm0uX2woKF92bS5maWxTZXJ2aWNlcyksIGZ1bmN0aW9uKHNlcnZpY2UpIHtcbiAgICByZXR1cm4gX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKHNlcnZpY2Uuc2VydmljZV9kYXRlKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgWyhzZXJ2aWNlLnVzZXIgIT0gbnVsbCkgPyBfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKHNlcnZpY2UudXNlci5mdWxsX25hbWUpKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChzZXJ2aWNlLmdyb3VwX2JpbmRlZCAhPSAnJykgPyBfYygnYScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInB1bGwtcmlnaHRcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiBcIiNcIixcbiAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInBvcG92ZXJcIixcbiAgICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiLFxuICAgICAgICBcImRhdGEtY29udGFpbmVyXCI6IFwiYm9keVwiLFxuICAgICAgICBcImRhdGEtY29udGVudFwiOiBzZXJ2aWNlLnVzZXIuZ3JvdXBfYmluZGVkLFxuICAgICAgICBcImRhdGEtdHJpZ2dlclwiOiBcImhvdmVyXCJcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2InLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmYSBmYS11c2VyLWNpcmNsZSBncm91cC1pY29uIG0tci0yXCJcbiAgICB9KV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChzZXJ2aWNlLnVzZXIudW5yZWFkX25vdGlmID4gMCkgPyBfYygnYScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInB1bGwtcmlnaHRcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiBcIiNcIixcbiAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInBvcG92ZXJcIixcbiAgICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiLFxuICAgICAgICBcImRhdGEtY29udGFpbmVyXCI6IFwiYm9keVwiLFxuICAgICAgICBcImRhdGEtY29udGVudFwiOiAnVW5yZWFkIE5vdGlmaWNhdGlvbnMgOiAnICsgc2VydmljZS51c2VyLnVucmVhZF9ub3RpZixcbiAgICAgICAgXCJkYXRhLXRyaWdnZXJcIjogXCJob3ZlclwiXG4gICAgICB9XG4gICAgfSwgW19jKCdiJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtZW52ZWxvcGUgdW5yZWFkLWljb24gbS1yLTFcIlxuICAgIH0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKHNlcnZpY2UudXNlci5iaW5kZWQpID8gX2MoJ2EnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJwdWxsLXJpZ2h0XCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogXCIjXCIsXG4gICAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJwb3BvdmVyXCIsXG4gICAgICAgIFwiZGF0YS1wbGFjZW1lbnRcIjogXCJ0b3BcIixcbiAgICAgICAgXCJkYXRhLWNvbnRhaW5lclwiOiBcImJvZHlcIixcbiAgICAgICAgXCJkYXRhLWNvbnRlbnRcIjogXCJBcHAgSW5zdGFsbGVkIGJ5IENsaWVudFwiLFxuICAgICAgICBcImRhdGEtdHJpZ2dlclwiOiBcImhvdmVyXCJcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2InLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1tb2JpbGUgYXBwLWljb24gbS1yLTFcIlxuICAgIH0pXSkgOiBfdm0uX2UoKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbKHNlcnZpY2UudXNlciAhPSBudWxsKSA/IF9jKCdzcGFuJywgW192bS5fdihfdm0uX3Moc2VydmljZS51c2VyLmlkKSldKSA6IF92bS5fZSgpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKHNlcnZpY2UudHJhY2tpbmcpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCB7XG4gICAgICBzdGF0aWNTdHlsZToge1xuICAgICAgICBcIndpZHRoXCI6IFwiMzAlICFpbXBvcnRhbnRcIlxuICAgICAgfVxuICAgIH0sIFsoc2VydmljZS5yZW1hcmtzICE9ICcnKSA/IF9jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6IFwiI1wiLFxuICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwicG9wb3ZlclwiLFxuICAgICAgICBcImRhdGEtcGxhY2VtZW50XCI6IFwicmlnaHRcIixcbiAgICAgICAgXCJkYXRhLWNvbnRhaW5lclwiOiBcImJvZHlcIixcbiAgICAgICAgXCJkYXRhLWNvbnRlbnRcIjogc2VydmljZS5yZW1hcmtzLFxuICAgICAgICBcImRhdGEtdHJpZ2dlclwiOiBcImhvdmVyXCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICBcIiArIF92bS5fcyhzZXJ2aWNlLmRldGFpbCkgKyBcIlxcblxcdFxcdFxcdCAgICAgICAgICAgIFwiKV0pIDogX2MoJ3NwYW4nLCBbX3ZtLl92KF92bS5fcyhzZXJ2aWNlLmRldGFpbCkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3Moc2VydmljZS5jb3N0KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3Moc2VydmljZS5jaGFyZ2UpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhzZXJ2aWNlLnRpcCkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyXCJcbiAgICB9LCBbX2MoJ2EnLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogXCIjXCIsXG4gICAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJtb2RhbFwiLFxuICAgICAgICBcImRhdGEtdGFyZ2V0XCI6IFwiI2VkaXRTZXJ2aWNlTW9kYWxcIlxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLiRwYXJlbnQuc2V0U2VydmljZShzZXJ2aWNlKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW19jKCdpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtcGVuY2lsLXNxdWFyZS1vXCJcbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiAnL3Zpc2EvY2xpZW50LycgKyBzZXJ2aWNlLmNsaWVudF9pZCxcbiAgICAgICAgXCJ0YXJnZXRcIjogXCJfYmxhbmtcIixcbiAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcbiAgICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcImxlZnRcIixcbiAgICAgICAgXCJ0aXRsZVwiOiBcIlZpZXcgY2xpZW50IHByb2ZpbGVcIlxuICAgICAgfVxuICAgIH0sIFtfYygnaScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWFycm93LXJpZ2h0XCJcbiAgICB9KV0pXSldKVxuICB9KSwgMCldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZGF0ZXJhbmdlIGlucHV0LWdyb3VwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0b2RheXMtc2VydmljZXMtZGF0ZXBpY2tlclwiXG4gICAgfVxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LXNtIGZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcIm5hbWVcIjogXCJ0b2RheXMtc2VydmljZS1kYXRlXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnlcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJsaW5lLWhlaWdodFwiOiBcIjEuMlwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfYygnc3Ryb25nJywgW192bS5fdihcIkZpbHRlclwiKV0pXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0aGVhZCcsIFtfYygndHInLCBbX2MoJ3RoJywgW192bS5fdihcIkRhdGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIk5hbWVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIklEXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJQYWNrYWdlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjMwJSAhaW1wb3J0YW50XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJEZXRhaWxcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkNvc3RcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIkNoYXJnZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiVGlwXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJ3aWR0aFwiOiBcIjUlXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJBY3Rpb25cIildKV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1jOWQwZTEyMFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWM5ZDBlMTIwXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvVG9kYXlzU2VydmljZXMudnVlXG4vLyBtb2R1bGUgaWQgPSA0MjBcbi8vIG1vZHVsZSBjaHVua3MgPSA1IiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zYWZkYjU1M1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0VkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiMDE1YzhmM2RcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTNhZmRiNTUzXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vRWRpdFNlcnZpY2VEYXNoYm9hcmQudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zYWZkYjU1M1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0VkaXRTZXJ2aWNlRGFzaGJvYXJkLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTNhZmRiNTUzXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvRWRpdFNlcnZpY2VEYXNoYm9hcmQudnVlXG4vLyBtb2R1bGUgaWQgPSA0Mzdcbi8vIG1vZHVsZSBjaHVua3MgPSA1IiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi02YjU4MWFjYlxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL0RlbGl2ZXJ5U2NoZWR1bGUudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCJmMmMyY2RjOFwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNmI1ODFhY2JcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9EZWxpdmVyeVNjaGVkdWxlLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNmI1ODFhY2JcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9EZWxpdmVyeVNjaGVkdWxlLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTZiNTgxYWNiXCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGVsaXZlcnlTY2hlZHVsZS52dWVcbi8vIG1vZHVsZSBpZCA9IDQ0MlxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTdjOTVjZGVjXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vVG9kYXlzVGFza3MudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCIyYzg4YmFhOFwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtN2M5NWNkZWNcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9Ub2RheXNUYXNrcy52dWVcIiwgZnVuY3Rpb24oKSB7XG4gICAgIHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTdjOTVjZGVjXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vVG9kYXlzVGFza3MudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtN2M5NWNkZWNcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9Ub2RheXNUYXNrcy52dWVcbi8vIG1vZHVsZSBpZCA9IDQ0NlxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LWM5ZDBlMTIwXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vVG9kYXlzU2VydmljZXMudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCIxY2ZiZWE0NVwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtYzlkMGUxMjBcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9Ub2RheXNTZXJ2aWNlcy52dWVcIiwgZnVuY3Rpb24oKSB7XG4gICAgIHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LWM5ZDBlMTIwXFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vVG9kYXlzU2VydmljZXMudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYzlkMGUxMjBcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9Ub2RheXNTZXJ2aWNlcy52dWVcbi8vIG1vZHVsZSBpZCA9IDQ1MVxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTRkZTYwNjU1XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIERpYWxvZ01vZGFsLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi00ZGU2MDY1NVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTRkZTYwNjU1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDVcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDUgNiA5IDExIDE4IDE5IDIwIDIxIDIyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwgbWQtbW9kYWwgZmFkZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IF92bS5pZCxcbiAgICAgIFwidGFiaW5kZXhcIjogXCItMVwiLFxuICAgICAgXCJyb2xlXCI6IFwiZGlhbG9nXCIsXG4gICAgICBcImFyaWEtbGFiZWxsZWRieVwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGNsYXNzOiAnbW9kYWwtZGlhbG9nICcgKyBfdm0uc2l6ZSxcbiAgICBhdHRyczoge1xuICAgICAgXCJyb2xlXCI6IFwiZG9jdW1lbnRcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWhlYWRlclwiXG4gIH0sIFtfYygnaDQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtdGl0bGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiLCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtYm9keVwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1ib2R5XCIsIFtfdm0uX3YoXCJNb2RhbCBCb2R5XCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtZm9vdGVyXCIsIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTRkZTYwNjU1XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNGRlNjA2NTVcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDUgNiA5IDExIDE4IDE5IDIwIDIxIDIyIiwiLyoqXG4gKiBUcmFuc2xhdGVzIHRoZSBsaXN0IGZvcm1hdCBwcm9kdWNlZCBieSBjc3MtbG9hZGVyIGludG8gc29tZXRoaW5nXG4gKiBlYXNpZXIgdG8gbWFuaXB1bGF0ZS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBsaXN0VG9TdHlsZXMgKHBhcmVudElkLCBsaXN0KSB7XG4gIHZhciBzdHlsZXMgPSBbXVxuICB2YXIgbmV3U3R5bGVzID0ge31cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldXG4gICAgdmFyIGlkID0gaXRlbVswXVxuICAgIHZhciBjc3MgPSBpdGVtWzFdXG4gICAgdmFyIG1lZGlhID0gaXRlbVsyXVxuICAgIHZhciBzb3VyY2VNYXAgPSBpdGVtWzNdXG4gICAgdmFyIHBhcnQgPSB7XG4gICAgICBpZDogcGFyZW50SWQgKyAnOicgKyBpLFxuICAgICAgY3NzOiBjc3MsXG4gICAgICBtZWRpYTogbWVkaWEsXG4gICAgICBzb3VyY2VNYXA6IHNvdXJjZU1hcFxuICAgIH1cbiAgICBpZiAoIW5ld1N0eWxlc1tpZF0pIHtcbiAgICAgIHN0eWxlcy5wdXNoKG5ld1N0eWxlc1tpZF0gPSB7IGlkOiBpZCwgcGFydHM6IFtwYXJ0XSB9KVxuICAgIH0gZWxzZSB7XG4gICAgICBuZXdTdHlsZXNbaWRdLnBhcnRzLnB1c2gocGFydClcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHN0eWxlc1xufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qc1xuLy8gbW9kdWxlIGlkID0gN1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiAzIDQgNSA5IDExIDEyIDEzIl0sInNvdXJjZVJvb3QiOiIifQ==