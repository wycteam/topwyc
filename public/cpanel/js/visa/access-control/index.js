/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 472);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 182:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('dialog-modal', __webpack_require__(5));

var UserApp = new Vue({
	el: '#access-control',
	data: {
		roles: [],
		permissions: [],
		rolename: '',
		permissiontype: [],
		form: new Form({
			roleid: 0,
			selpermissions: []
		}, { baseURL: '/visa/access-control' }),
		roleForm: new Form({
			id: 0,
			role: ''
		}, { baseURL: '/visa/access-control' })

	},
	methods: {
		showPermissionDialog: function showPermissionDialog(role) {
			var _this = this;

			this.rolename = role.label;
			this.form.roleid = role.id;

			axios.get('/visa/access-control/getSelectedPermission/' + role.id).then(function (result) {
				_this.form.selpermissions = result.data;
			});

			axios.get('/visa/access-control/getPermissions').then(function (result) {
				_this.permissions = result.data;
			});

			axios.get('/visa/access-control/getPermissionType').then(function (result) {
				_this.permissiontype = result.data;
			});

			$('#dialogPermissions').modal('toggle');
		},
		toggle: function toggle(id, e) {
			if (e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
				$('#fa-' + id).toggleClass('fa-arrow-down');
			}
		},
		selectItem: function selectItem(data, index) {
			if (this.check(data) == 1) {
				var sp = this.form.selpermissions;
				var vi = this;
				$.each(sp, function (i, item) {
					if (sp[i] != undefined) {
						if (sp[i].permission_id == data.id) {
							vi.form.selpermissions.splice(i, 1);
						}
					}
				});
			} else {
				var d = { "permission_id": data.id, "role_id": this.form.roleid };
				this.form.selpermissions.push(d);
			}
		},
		check: function check(id) {
			var a = 0;
			var sp = this.form.selpermissions;
			$.each(sp, function (i, item) {
				if (sp[i].permission_id == id.id) {
					a = 1;
				}
			});
			return a;
		},
		saveAccessControl: function saveAccessControl() {
			var _this2 = this;

			if (this.form.selpermissions.length != 0) {
				this.form.submit('post', '/updateAccessControl').then(function (result) {
					$('input:checkbox').prop('checked', false);
					_this2.form.selpermissions = [];
					_this2.reset();
					$('#dialogPermissions').modal('toggle');
					toastr.success('Successfully saved.');
				});
			} else toastr.error('Please select at least one permission.');
		},
		reset: function reset() {
			$('#form')[0].reset();
		},
		showRoleDialog: function showRoleDialog() {
			$('#dialogRole').modal('toggle');
		},
		addRole: function addRole() {
			var _this3 = this;

			this.roleForm.submit('post', '/addRole').then(function (result) {
				toastr.success('Successfully saved.');
				_this3.roleForm.role = '';
				$('#dialogRole').modal('toggle');
				_this3.getRoles();
			});
		},
		showEditRoleDialog: function showEditRoleDialog(role) {
			var _this4 = this;

			axios.get('/visa/access-control/getRole/' + role.id).then(function (result) {
				_this4.roleForm.id = result.data[0].id;
				_this4.roleForm.role = result.data[0].label;
				$('#dialogEditRole').modal('toggle');
			});
		},
		getRoles: function getRoles() {
			var _this5 = this;

			axios.get('/visa/access-control/getRoles').then(function (result) {
				_this5.roles = result.data;
			});
		},
		saveRole: function saveRole() {
			var _this6 = this;

			this.roleForm.submit('post', '/saveRole').then(function (result) {
				toastr.success('Successfully saved.');
				_this6.roleForm.id = 0;
				_this6.roleForm.role = '';
				$('#dialogEditRole').modal('toggle');
				_this6.getRoles();
			});
		}
	},
	created: function created() {
		this.getRoles();
	},
	mounted: function mounted() {
		$('#dialogPermissions').on("hidden.bs.modal", this.reset);

		$('body').popover({ // Ok
			html: true,
			trigger: 'hover',
			selector: '[data-toggle="popover"]'
		});
	}
});

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 472:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(182);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2FjY2Vzcy1jb250cm9sL2luZGV4LmpzIiwid2VicGFjazovLy9EaWFsb2dNb2RhbC52dWU/ZTQ3NCoqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWU/MjA3YyoqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWU/ZmUxYioqKioqKioqKiJdLCJuYW1lcyI6WyJWdWUiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwiVXNlckFwcCIsImVsIiwiZGF0YSIsInJvbGVzIiwicGVybWlzc2lvbnMiLCJyb2xlbmFtZSIsInBlcm1pc3Npb250eXBlIiwiZm9ybSIsIkZvcm0iLCJyb2xlaWQiLCJzZWxwZXJtaXNzaW9ucyIsImJhc2VVUkwiLCJyb2xlRm9ybSIsImlkIiwicm9sZSIsIm1ldGhvZHMiLCJzaG93UGVybWlzc2lvbkRpYWxvZyIsImxhYmVsIiwiYXhpb3MiLCJnZXQiLCJ0aGVuIiwicmVzdWx0IiwiJCIsIm1vZGFsIiwidG9nZ2xlIiwiZSIsInRhcmdldCIsIm5vZGVOYW1lIiwidG9nZ2xlQ2xhc3MiLCJzZWxlY3RJdGVtIiwiaW5kZXgiLCJjaGVjayIsInNwIiwidmkiLCJlYWNoIiwiaSIsIml0ZW0iLCJ1bmRlZmluZWQiLCJwZXJtaXNzaW9uX2lkIiwic3BsaWNlIiwiZCIsInB1c2giLCJhIiwic2F2ZUFjY2Vzc0NvbnRyb2wiLCJsZW5ndGgiLCJzdWJtaXQiLCJwcm9wIiwicmVzZXQiLCJ0b2FzdHIiLCJzdWNjZXNzIiwiZXJyb3IiLCJzaG93Um9sZURpYWxvZyIsImFkZFJvbGUiLCJnZXRSb2xlcyIsInNob3dFZGl0Um9sZURpYWxvZyIsInNhdmVSb2xlIiwiY3JlYXRlZCIsIm1vdW50ZWQiLCJvbiIsInBvcG92ZXIiLCJodG1sIiwidHJpZ2dlciIsInNlbGVjdG9yIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNsREFBLElBQUlDLFNBQUosQ0FBYyxjQUFkLEVBQStCQyxtQkFBT0EsQ0FBQyxDQUFSLENBQS9COztBQUVBLElBQUlDLFVBQVUsSUFBSUgsR0FBSixDQUFRO0FBQ3JCSSxLQUFHLGlCQURrQjtBQUVyQkMsT0FBSztBQUNKQyxTQUFNLEVBREY7QUFFSkMsZUFBWSxFQUZSO0FBR0pDLFlBQVMsRUFITDtBQUlKQyxrQkFBZSxFQUpYO0FBS0RDLFFBQU0sSUFBSUMsSUFBSixDQUFTO0FBQ1pDLFdBQU8sQ0FESztBQUVaQyxtQkFBZTtBQUZILEdBQVQsRUFHSixFQUFFQyxTQUFTLHNCQUFYLEVBSEksQ0FMTDtBQVNEQyxZQUFVLElBQUlKLElBQUosQ0FBUztBQUNoQkssT0FBRyxDQURhO0FBRWhCQyxTQUFLO0FBRlcsR0FBVCxFQUdSLEVBQUVILFNBQVMsc0JBQVgsRUFIUTs7QUFUVCxFQUZnQjtBQWlCckJJLFVBQVM7QUFDUkMsc0JBRFEsZ0NBQ2FGLElBRGIsRUFDbUI7QUFBQTs7QUFDMUIsUUFBS1QsUUFBTCxHQUFnQlMsS0FBS0csS0FBckI7QUFDQSxRQUFLVixJQUFMLENBQVVFLE1BQVYsR0FBbUJLLEtBQUtELEVBQXhCOztBQUVBSyxTQUFNQyxHQUFOLENBQVUsZ0RBQThDTCxLQUFLRCxFQUE3RCxFQUNPTyxJQURQLENBQ1ksa0JBQVU7QUFDYixVQUFLYixJQUFMLENBQVVHLGNBQVYsR0FBMkJXLE9BQU9uQixJQUFsQztBQUNMLElBSEo7O0FBS0FnQixTQUFNQyxHQUFOLENBQVUscUNBQVYsRUFDT0MsSUFEUCxDQUNZLGtCQUFVO0FBQ2IsVUFBS2hCLFdBQUwsR0FBbUJpQixPQUFPbkIsSUFBMUI7QUFDTCxJQUhKOztBQUtHZ0IsU0FBTUMsR0FBTixDQUFVLHdDQUFWLEVBQ0lDLElBREosQ0FDUyxrQkFBVTtBQUNiLFVBQUtkLGNBQUwsR0FBc0JlLE9BQU9uQixJQUE3QjtBQUNMLElBSEQ7O0FBS0hvQixLQUFFLG9CQUFGLEVBQXdCQyxLQUF4QixDQUE4QixRQUE5QjtBQUVBLEdBdEJPO0FBdUJGQyxRQXZCRSxrQkF1QktYLEVBdkJMLEVBdUJTWSxDQXZCVCxFQXVCWTtBQUNWLE9BQUdBLEVBQUVDLE1BQUYsQ0FBU0MsUUFBVCxJQUFxQixRQUFyQixJQUFpQ0YsRUFBRUMsTUFBRixDQUFTQyxRQUFULElBQXFCLE1BQXpELEVBQWlFO0FBQzdETCxNQUFFLFNBQVNULEVBQVgsRUFBZWUsV0FBZixDQUEyQixlQUEzQjtBQUNIO0FBQ0osR0EzQkM7QUE0QlJDLFlBNUJRLHNCQTRCRzNCLElBNUJILEVBNEJRNEIsS0E1QlIsRUE0QmM7QUFDWixPQUFHLEtBQUtDLEtBQUwsQ0FBVzdCLElBQVgsS0FBa0IsQ0FBckIsRUFBd0I7QUFDaEMsUUFBSThCLEtBQUssS0FBS3pCLElBQUwsQ0FBVUcsY0FBbkI7QUFDTSxRQUFJdUIsS0FBSyxJQUFUO0FBQ05YLE1BQUVZLElBQUYsQ0FBT0YsRUFBUCxFQUFXLFVBQVNHLENBQVQsRUFBWUMsSUFBWixFQUFrQjtBQUMzQixTQUFHSixHQUFHRyxDQUFILEtBQU9FLFNBQVYsRUFBb0I7QUFDckIsVUFBR0wsR0FBR0csQ0FBSCxFQUFNRyxhQUFOLElBQXVCcEMsS0FBS1csRUFBL0IsRUFBa0M7QUFDL0JvQixVQUFHMUIsSUFBSCxDQUFRRyxjQUFSLENBQXVCNkIsTUFBdkIsQ0FBOEJKLENBQTlCLEVBQWdDLENBQWhDO0FBQ0E7QUFDRDtBQUNGLEtBTkQ7QUFPUyxJQVZELE1BVU87QUFDZixRQUFJSyxJQUFJLEVBQUMsaUJBQWlCdEMsS0FBS1csRUFBdkIsRUFBMkIsV0FBVyxLQUFLTixJQUFMLENBQVVFLE1BQWhELEVBQVI7QUFDWSxTQUFLRixJQUFMLENBQVVHLGNBQVYsQ0FBeUIrQixJQUF6QixDQUE4QkQsQ0FBOUI7QUFDSDtBQUNKLEdBM0NDO0FBNENGVCxPQTVDRSxpQkE0Q0lsQixFQTVDSixFQTRDTztBQUNSLE9BQUk2QixJQUFJLENBQVI7QUFDQSxPQUFJVixLQUFLLEtBQUt6QixJQUFMLENBQVVHLGNBQW5CO0FBQ05ZLEtBQUVZLElBQUYsQ0FBT0YsRUFBUCxFQUFXLFVBQVNHLENBQVQsRUFBWUMsSUFBWixFQUFrQjtBQUMzQixRQUFHSixHQUFHRyxDQUFILEVBQU1HLGFBQU4sSUFBdUJ6QixHQUFHQSxFQUE3QixFQUFnQztBQUMvQjZCLFNBQUksQ0FBSjtBQUNBO0FBQ0YsSUFKRDtBQUtBLFVBQU9BLENBQVA7QUFDTSxHQXJEQztBQXNEUkMsbUJBdERRLCtCQXNEVztBQUFBOztBQUNsQixPQUFHLEtBQUtwQyxJQUFMLENBQVVHLGNBQVYsQ0FBeUJrQyxNQUF6QixJQUFtQyxDQUF0QyxFQUF5QztBQUN4QyxTQUFLckMsSUFBTCxDQUFVc0MsTUFBVixDQUFpQixNQUFqQixFQUF5QixzQkFBekIsRUFDU3pCLElBRFQsQ0FDYyxrQkFBVTtBQUNmRSxPQUFFLGdCQUFGLEVBQW9Cd0IsSUFBcEIsQ0FBeUIsU0FBekIsRUFBbUMsS0FBbkM7QUFDQSxZQUFLdkMsSUFBTCxDQUFVRyxjQUFWLEdBQTJCLEVBQTNCO0FBQ0EsWUFBS3FDLEtBQUw7QUFDQXpCLE9BQUUsb0JBQUYsRUFBd0JDLEtBQXhCLENBQThCLFFBQTlCO0FBQ0F5QixZQUFPQyxPQUFQLENBQWUscUJBQWY7QUFDQSxLQVBUO0FBUU0sSUFUUCxNQVVPRCxPQUFPRSxLQUFQLENBQWEsd0NBQWI7QUFDUCxHQWxFTztBQW1FUkgsT0FuRVEsbUJBbUVEO0FBQ056QixLQUFFLE9BQUYsRUFBVyxDQUFYLEVBQWN5QixLQUFkO0FBQ0EsR0FyRU87QUFzRVJJLGdCQXRFUSw0QkFzRVE7QUFDZjdCLEtBQUUsYUFBRixFQUFpQkMsS0FBakIsQ0FBdUIsUUFBdkI7QUFDQSxHQXhFTztBQXlFUjZCLFNBekVRLHFCQXlFQztBQUFBOztBQUNSLFFBQUt4QyxRQUFMLENBQWNpQyxNQUFkLENBQXFCLE1BQXJCLEVBQTZCLFVBQTdCLEVBQ1N6QixJQURULENBQ2Msa0JBQVU7QUFDZjRCLFdBQU9DLE9BQVAsQ0FBZSxxQkFBZjtBQUNBLFdBQUtyQyxRQUFMLENBQWNFLElBQWQsR0FBcUIsRUFBckI7QUFDQVEsTUFBRSxhQUFGLEVBQWlCQyxLQUFqQixDQUF1QixRQUF2QjtBQUNBLFdBQUs4QixRQUFMO0FBRUEsSUFQVDtBQVFBLEdBbEZPO0FBbUZSQyxvQkFuRlEsOEJBbUZXeEMsSUFuRlgsRUFtRmdCO0FBQUE7O0FBQ3ZCSSxTQUFNQyxHQUFOLENBQVUsa0NBQWdDTCxLQUFLRCxFQUEvQyxFQUNPTyxJQURQLENBQ1ksa0JBQVU7QUFDZixXQUFLUixRQUFMLENBQWNDLEVBQWQsR0FBbUJRLE9BQU9uQixJQUFQLENBQVksQ0FBWixFQUFlVyxFQUFsQztBQUNFLFdBQUtELFFBQUwsQ0FBY0UsSUFBZCxHQUFxQk8sT0FBT25CLElBQVAsQ0FBWSxDQUFaLEVBQWVlLEtBQXBDO0FBQ0FLLE1BQUUsaUJBQUYsRUFBcUJDLEtBQXJCLENBQTJCLFFBQTNCO0FBQ0wsSUFMSjtBQU9BLEdBM0ZPO0FBNEZSOEIsVUE1RlEsc0JBNEZFO0FBQUE7O0FBQ1RuQyxTQUFNQyxHQUFOLENBQVUsK0JBQVYsRUFDT0MsSUFEUCxDQUNZLGtCQUFVO0FBQ2IsV0FBS2pCLEtBQUwsR0FBYWtCLE9BQU9uQixJQUFwQjtBQUNMLElBSEo7QUFJQSxHQWpHTztBQWtHUnFELFVBbEdRLHNCQWtHRTtBQUFBOztBQUNULFFBQUszQyxRQUFMLENBQWNpQyxNQUFkLENBQXFCLE1BQXJCLEVBQTZCLFdBQTdCLEVBQ1N6QixJQURULENBQ2Msa0JBQVU7QUFDZjRCLFdBQU9DLE9BQVAsQ0FBZSxxQkFBZjtBQUNBLFdBQUtyQyxRQUFMLENBQWNDLEVBQWQsR0FBbUIsQ0FBbkI7QUFDQSxXQUFLRCxRQUFMLENBQWNFLElBQWQsR0FBcUIsRUFBckI7QUFDQVEsTUFBRSxpQkFBRixFQUFxQkMsS0FBckIsQ0FBMkIsUUFBM0I7QUFDQSxXQUFLOEIsUUFBTDtBQUNBLElBUFQ7QUFRQTtBQTNHTyxFQWpCWTtBQThIckJHLFFBOUhxQixxQkE4SFo7QUFDUixPQUFLSCxRQUFMO0FBQ0EsRUFoSW9CO0FBaUlyQkksUUFqSXFCLHFCQWlJWjtBQUNSbkMsSUFBRSxvQkFBRixFQUF3Qm9DLEVBQXhCLENBQTJCLGlCQUEzQixFQUE4QyxLQUFLWCxLQUFuRDs7QUFFTXpCLElBQUUsTUFBRixFQUFVcUMsT0FBVixDQUFrQixFQUFFO0FBQ2hCQyxTQUFLLElBRFM7QUFFZEMsWUFBUyxPQUZLO0FBR2RDLGFBQVU7QUFISSxHQUFsQjtBQUtOO0FBeklvQixDQUFSLENBQWQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNxQkE7QUFDQTtBQUNBLGdDQURBO0FBRUE7QUFGQTtBQURBLEc7Ozs7Ozs7Ozs7Ozs7OztBQ3ZCQSxnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxDQUF5TztBQUNuUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxDQUFxTTtBQUMvTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3Zpc2EvYWNjZXNzLWNvbnRyb2wvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ3Mik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjMiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSIsIlZ1ZS5jb21wb25lbnQoJ2RpYWxvZy1tb2RhbCcsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZScpKTtcblxudmFyIFVzZXJBcHAgPSBuZXcgVnVlKHtcblx0ZWw6JyNhY2Nlc3MtY29udHJvbCcsXG5cdGRhdGE6e1xuXHRcdHJvbGVzOltdLFxuXHRcdHBlcm1pc3Npb25zOltdLFxuXHRcdHJvbGVuYW1lOicnLFxuXHRcdHBlcm1pc3Npb250eXBlOltdLFxuXHQgICAgZm9ybTogbmV3IEZvcm0oe1xuXHQgICAgICAgcm9sZWlkOjAsXG5cdCAgICAgICBzZWxwZXJtaXNzaW9uczpbXVxuXHQgICAgfSx7IGJhc2VVUkw6ICcvdmlzYS9hY2Nlc3MtY29udHJvbCd9KSxcblx0ICAgIHJvbGVGb3JtOiBuZXcgRm9ybSh7XG5cdCAgICAgICBpZDowLFxuXHQgICAgICAgcm9sZTonJ1xuXHQgICAgfSx7IGJhc2VVUkw6ICcvdmlzYS9hY2Nlc3MtY29udHJvbCd9KVxuXG5cdH0sXG5cdG1ldGhvZHM6IHtcblx0XHRzaG93UGVybWlzc2lvbkRpYWxvZyhyb2xlKSB7XG5cdFx0XHR0aGlzLnJvbGVuYW1lID0gcm9sZS5sYWJlbDtcblx0XHRcdHRoaXMuZm9ybS5yb2xlaWQgPSByb2xlLmlkO1xuXG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2FjY2Vzcy1jb250cm9sL2dldFNlbGVjdGVkUGVybWlzc2lvbi8nK3JvbGUuaWQpXG5cdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICAgIFx0dGhpcy5mb3JtLnNlbHBlcm1pc3Npb25zID0gcmVzdWx0LmRhdGE7XG5cdCAgICBcdH0pO1xuXG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2FjY2Vzcy1jb250cm9sL2dldFBlcm1pc3Npb25zJylcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHQgICAgICAgICAgXHR0aGlzLnBlcm1pc3Npb25zID0gcmVzdWx0LmRhdGE7XG5cdCAgICBcdH0pO1xuXG5cdCAgICBcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0UGVybWlzc2lvblR5cGUnKVxuXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICBcdHRoaXMucGVybWlzc2lvbnR5cGUgPSByZXN1bHQuZGF0YTtcblx0ICAgIFx0fSk7XG5cblx0XHRcdCQoJyNkaWFsb2dQZXJtaXNzaW9ucycpLm1vZGFsKCd0b2dnbGUnKTtcblxuXHRcdH0sXG4gICAgICAgIHRvZ2dsZShpZCwgZSkge1xuICAgICAgICAgICAgaWYoZS50YXJnZXQubm9kZU5hbWUgIT0gJ0JVVFRPTicgJiYgZS50YXJnZXQubm9kZU5hbWUgIT0gJ1NQQU4nKSB7XG4gICAgICAgICAgICAgICAgJCgnI2ZhLScgKyBpZCkudG9nZ2xlQ2xhc3MoJ2ZhLWFycm93LWRvd24nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblx0XHRzZWxlY3RJdGVtKGRhdGEsaW5kZXgpe1xuICAgICAgICAgICAgaWYodGhpcy5jaGVjayhkYXRhKT09MSkge1xuXHRcdFx0XHR2YXIgc3AgPSB0aGlzLmZvcm0uc2VscGVybWlzc2lvbnM7XG5cdCAgICAgICAgXHR2YXIgdmkgPSB0aGlzO1xuXHRcdFx0XHQkLmVhY2goc3AsIGZ1bmN0aW9uKGksIGl0ZW0pIHtcblx0XHRcdFx0ICBpZihzcFtpXSE9dW5kZWZpbmVkKXtcblx0XHRcdFx0XHRpZihzcFtpXS5wZXJtaXNzaW9uX2lkID09IGRhdGEuaWQpe1xuXHRcdFx0XHQgIFx0XHR2aS5mb3JtLnNlbHBlcm1pc3Npb25zLnNwbGljZShpLDEpO1xuXHRcdFx0XHQgIFx0fVxuXHRcdFx0XHQgIH1cblx0XHRcdFx0fSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuXHRcdFx0XHR2YXIgZCA9IHtcInBlcm1pc3Npb25faWRcIjogZGF0YS5pZCwgXCJyb2xlX2lkXCI6IHRoaXMuZm9ybS5yb2xlaWR9O1xuICAgICAgICAgICAgICAgIHRoaXMuZm9ybS5zZWxwZXJtaXNzaW9ucy5wdXNoKGQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBjaGVjayhpZCl7XG4gICAgICAgIFx0dmFyIGEgPSAwO1xuICAgICAgICBcdHZhciBzcCA9IHRoaXMuZm9ybS5zZWxwZXJtaXNzaW9ucztcblx0XHRcdCQuZWFjaChzcCwgZnVuY3Rpb24oaSwgaXRlbSkge1xuXHRcdFx0ICBpZihzcFtpXS5wZXJtaXNzaW9uX2lkID09IGlkLmlkKXtcblx0XHRcdCAgXHRhID0gMTtcblx0XHRcdCAgfVxuXHRcdFx0fSk7XG5cdFx0XHRyZXR1cm4gYTtcbiAgICAgICAgfSxcblx0XHRzYXZlQWNjZXNzQ29udHJvbCgpe1xuXHRcdFx0aWYodGhpcy5mb3JtLnNlbHBlcm1pc3Npb25zLmxlbmd0aCAhPSAwKSB7XG5cdFx0XHRcdHRoaXMuZm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3VwZGF0ZUFjY2Vzc0NvbnRyb2wnKVxuXHRcdCAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdCAgICAgICAgICBcdCQoJ2lucHV0OmNoZWNrYm94JykucHJvcCgnY2hlY2tlZCcsZmFsc2UpO1xuXHRcdCAgICAgICAgICBcdHRoaXMuZm9ybS5zZWxwZXJtaXNzaW9ucyA9IFtdO1xuXHRcdCAgICAgICAgICBcdHRoaXMucmVzZXQoKTtcblx0XHQgICAgICAgICAgXHQkKCcjZGlhbG9nUGVybWlzc2lvbnMnKS5tb2RhbCgndG9nZ2xlJyk7XG5cdFx0ICAgICAgICAgIFx0dG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBzYXZlZC4nKTtcblx0XHQgICAgICAgICAgfSk7XG5cdCAgICAgICAgfSBlbHNlIFxuXHQgICAgICAgIFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGF0IGxlYXN0IG9uZSBwZXJtaXNzaW9uLicpO1xuXHRcdH0sXG5cdFx0cmVzZXQoKXtcblx0XHRcdCQoJyNmb3JtJylbMF0ucmVzZXQoKTtcblx0XHR9LFxuXHRcdHNob3dSb2xlRGlhbG9nKCl7XG5cdFx0XHQkKCcjZGlhbG9nUm9sZScpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHR9LFxuXHRcdGFkZFJvbGUoKXtcblx0XHRcdHRoaXMucm9sZUZvcm0uc3VibWl0KCdwb3N0JywgJy9hZGRSb2xlJylcblx0ICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICBcdHRvYXN0ci5zdWNjZXNzKCdTdWNjZXNzZnVsbHkgc2F2ZWQuJyk7XG5cdCAgICAgICAgICBcdHRoaXMucm9sZUZvcm0ucm9sZSA9ICcnO1xuXHQgICAgICAgICAgXHQkKCcjZGlhbG9nUm9sZScpLm1vZGFsKCd0b2dnbGUnKTtcblx0ICAgICAgICAgIFx0dGhpcy5nZXRSb2xlcygpO1xuXG5cdCAgICAgICAgICB9KTtcblx0XHR9LFxuXHRcdHNob3dFZGl0Um9sZURpYWxvZyhyb2xlKXtcblx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0Um9sZS8nK3JvbGUuaWQpXG5cdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICBcdHRoaXMucm9sZUZvcm0uaWQgPSByZXN1bHQuZGF0YVswXS5pZDtcblx0ICAgICAgICAgIFx0dGhpcy5yb2xlRm9ybS5yb2xlID0gcmVzdWx0LmRhdGFbMF0ubGFiZWw7XG5cdCAgICAgICAgICBcdCQoJyNkaWFsb2dFZGl0Um9sZScpLm1vZGFsKCd0b2dnbGUnKTtcblx0ICAgIFx0fSk7XG5cblx0XHR9LFxuXHRcdGdldFJvbGVzKCl7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2FjY2Vzcy1jb250cm9sL2dldFJvbGVzJylcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHQgICAgICAgICAgXHR0aGlzLnJvbGVzID0gcmVzdWx0LmRhdGE7XG5cdCAgICBcdH0pO1xuXHRcdH0sXG5cdFx0c2F2ZVJvbGUoKXtcblx0XHRcdHRoaXMucm9sZUZvcm0uc3VibWl0KCdwb3N0JywgJy9zYXZlUm9sZScpXG5cdCAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHQgICAgICAgICAgXHR0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xuXHQgICAgICAgICAgXHR0aGlzLnJvbGVGb3JtLmlkID0gMDtcblx0ICAgICAgICAgIFx0dGhpcy5yb2xlRm9ybS5yb2xlID0gJyc7XG5cdCAgICAgICAgICBcdCQoJyNkaWFsb2dFZGl0Um9sZScpLm1vZGFsKCd0b2dnbGUnKTtcblx0ICAgICAgICAgIFx0dGhpcy5nZXRSb2xlcygpO1xuXHQgICAgICAgICAgfSk7XG5cdFx0fVxuXHR9LFxuXHRjcmVhdGVkKCl7XG5cdFx0dGhpcy5nZXRSb2xlcygpO1xuXHR9LFxuXHRtb3VudGVkKCl7XG5cdFx0JCgnI2RpYWxvZ1Blcm1pc3Npb25zJykub24oXCJoaWRkZW4uYnMubW9kYWxcIiwgdGhpcy5yZXNldCk7XG5cbiAgICAgICAgJCgnYm9keScpLnBvcG92ZXIoeyAvLyBPa1xuICAgICAgICAgICAgaHRtbDp0cnVlLFxuICAgICAgICAgICAgdHJpZ2dlcjogJ2hvdmVyJyxcbiAgICAgICAgICAgIHNlbGVjdG9yOiAnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXSdcbiAgICAgICAgfSk7XG5cdH1cbn0pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9hY2Nlc3MtY29udHJvbC9pbmRleC5qcyIsIjx0ZW1wbGF0ZT5cbjxkaXYgY2xhc3M9XCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIgOmlkPVwiaWRcIiB0YWJpbmRleD1cIi0xXCIgcm9sZT1cImRpYWxvZ1wiIGFyaWEtbGFiZWxsZWRieT1cIm1vZGFsLWxhYmVsXCI+XG4gICAgPGRpdiA6Y2xhc3M9XCInbW9kYWwtZGlhbG9nICcrc2l6ZVwiIHJvbGU9XCJkb2N1bWVudFwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPlxuICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCIgaWQ9XCJtb2RhbC1sYWJlbFwiPlxuICAgICAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtdGl0bGVcIj5Nb2RhbCBUaXRsZTwvc2xvdD5cbiAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1ib2R5XCI+TW9kYWwgQm9keTwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPlxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9zbG90PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICAgIHByb3BzOiB7XG4gICAgICAgICdpZCc6e3JlcXVpcmVkOnRydWV9XG4gICAgICAgICwnc2l6ZSc6IHtkZWZhdWx0Oidtb2RhbC1tZCd9XG4gICAgfVxufVxuPC9zY3JpcHQ+XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRGlhbG9nTW9kYWwudnVlPzAwM2JkYTg4IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi00ZGU2MDY1NVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBEaWFsb2dNb2RhbC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNGRlNjA2NTVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi00ZGU2MDY1NVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSA1XG4vLyBtb2R1bGUgY2h1bmtzID0gNCA1IDYgOSAxMSAxOCAxOSAyMCAyMSAyMiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsIG1kLW1vZGFsIGZhZGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBfdm0uaWQsXG4gICAgICBcInRhYmluZGV4XCI6IFwiLTFcIixcbiAgICAgIFwicm9sZVwiOiBcImRpYWxvZ1wiLFxuICAgICAgXCJhcmlhLWxhYmVsbGVkYnlcIjogXCJtb2RhbC1sYWJlbFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBjbGFzczogJ21vZGFsLWRpYWxvZyAnICsgX3ZtLnNpemUsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwicm9sZVwiOiBcImRvY3VtZW50XCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1oZWFkZXJcIlxuICB9LCBbX2MoJ2g0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLXRpdGxlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJtb2RhbC1sYWJlbFwiXG4gICAgfVxuICB9LCBbX3ZtLl90KFwibW9kYWwtdGl0bGVcIiwgW192bS5fdihcIk1vZGFsIFRpdGxlXCIpXSldLCAyKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWJvZHlcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtYm9keVwiLCBbX3ZtLl92KFwiTW9kYWwgQm9keVwiKV0pXSwgMiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtZm9vdGVyXCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWZvb3RlclwiLCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnlcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSldLCAyKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi00ZGU2MDY1NVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTRkZTYwNjU1XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSA2XG4vLyBtb2R1bGUgY2h1bmtzID0gNCA1IDYgOSAxMSAxOCAxOSAyMCAyMSAyMiJdLCJzb3VyY2VSb290IjoiIn0=