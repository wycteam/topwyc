/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 480);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('dialog-modal', __webpack_require__(5));

var UserApp = new Vue({
	el: '#access-control',
	data: {
		roles: [],
		permissions: [],
		rolename: '',
		permissiontype: [],
		form: new Form({
			roleid: 0,
			selpermissions: []
		}, { baseURL: '/visa/access-control' }),
		roleForm: new Form({
			id: 0,
			role: ''
		}, { baseURL: '/visa/access-control' })

	},
	methods: {
		showPermissionDialog: function showPermissionDialog(role) {
			var _this = this;

			this.rolename = role.label;
			this.form.roleid = role.id;

			axios.get('/visa/access-control/getSelectedPermission/' + role.id).then(function (result) {
				_this.form.selpermissions = result.data;
			});

			axios.get('/visa/access-control/getPermissions').then(function (result) {
				_this.permissions = result.data;
			});

			axios.get('/visa/access-control/getPermissionType').then(function (result) {
				_this.permissiontype = result.data;
			});

			$('#dialogPermissions').modal('toggle');
		},
		toggle: function toggle(id, e) {
			if (e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
				$('#fa-' + id).toggleClass('fa-arrow-down');
			}
		},
		selectItem: function selectItem(data, index) {
			if (this.check(data) == 1) {
				var sp = this.form.selpermissions;
				var vi = this;
				$.each(sp, function (i, item) {
					if (sp[i] != undefined) {
						if (sp[i].permission_id == data.id) {
							vi.form.selpermissions.splice(i, 1);
						}
					}
				});
			} else {
				var d = { "permission_id": data.id, "role_id": this.form.roleid };
				this.form.selpermissions.push(d);
			}
		},
		check: function check(id) {
			var a = 0;
			var sp = this.form.selpermissions;
			$.each(sp, function (i, item) {
				if (sp[i].permission_id == id.id) {
					a = 1;
				}
			});
			return a;
		},
		saveAccessControl: function saveAccessControl() {
			var _this2 = this;

			if (this.form.selpermissions.length != 0) {
				this.form.submit('post', '/updateAccessControl').then(function (result) {
					$('input:checkbox').prop('checked', false);
					_this2.form.selpermissions = [];
					_this2.reset();
					$('#dialogPermissions').modal('toggle');
					toastr.success('Successfully saved.');
				});
			} else toastr.error('Please select at least one permission.');
		},
		reset: function reset() {
			$('#form')[0].reset();
		},
		showRoleDialog: function showRoleDialog() {
			$('#dialogRole').modal('toggle');
		},
		addRole: function addRole() {
			var _this3 = this;

			this.roleForm.submit('post', '/addRole').then(function (result) {
				toastr.success('Successfully saved.');
				_this3.roleForm.role = '';
				$('#dialogRole').modal('toggle');
				_this3.getRoles();
			});
		},
		showEditRoleDialog: function showEditRoleDialog(role) {
			var _this4 = this;

			axios.get('/visa/access-control/getRole/' + role.id).then(function (result) {
				_this4.roleForm.id = result.data[0].id;
				_this4.roleForm.role = result.data[0].label;
				$('#dialogEditRole').modal('toggle');
			});
		},
		getRoles: function getRoles() {
			var _this5 = this;

			axios.get('/visa/access-control/getRoles').then(function (result) {
				_this5.roles = result.data;
			});
		},
		saveRole: function saveRole() {
			var _this6 = this;

			this.roleForm.submit('post', '/saveRole').then(function (result) {
				toastr.success('Successfully saved.');
				_this6.roleForm.id = 0;
				_this6.roleForm.role = '';
				$('#dialogEditRole').modal('toggle');
				_this6.getRoles();
			});
		}
	},
	created: function created() {
		// 
	},
	mounted: function mounted() {
		$('#dialogPermissions').on("hidden.bs.modal", this.reset);

		$('body').popover({ // Ok
			html: true,
			trigger: 'hover',
			selector: '[data-toggle="popover"]'
		});
	}
});

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 480:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(190);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvZmluYW5jaW5nL2luZGV4LmpzIiwid2VicGFjazovLy9EaWFsb2dNb2RhbC52dWU/ZTQ3NCoqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlPzIwN2MqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT9mZTFiKioqKioqKiJdLCJuYW1lcyI6WyJWdWUiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwiVXNlckFwcCIsImVsIiwiZGF0YSIsInJvbGVzIiwicGVybWlzc2lvbnMiLCJyb2xlbmFtZSIsInBlcm1pc3Npb250eXBlIiwiZm9ybSIsIkZvcm0iLCJyb2xlaWQiLCJzZWxwZXJtaXNzaW9ucyIsImJhc2VVUkwiLCJyb2xlRm9ybSIsImlkIiwicm9sZSIsIm1ldGhvZHMiLCJzaG93UGVybWlzc2lvbkRpYWxvZyIsImxhYmVsIiwiYXhpb3MiLCJnZXQiLCJ0aGVuIiwicmVzdWx0IiwiJCIsIm1vZGFsIiwidG9nZ2xlIiwiZSIsInRhcmdldCIsIm5vZGVOYW1lIiwidG9nZ2xlQ2xhc3MiLCJzZWxlY3RJdGVtIiwiaW5kZXgiLCJjaGVjayIsInNwIiwidmkiLCJlYWNoIiwiaSIsIml0ZW0iLCJ1bmRlZmluZWQiLCJwZXJtaXNzaW9uX2lkIiwic3BsaWNlIiwiZCIsInB1c2giLCJhIiwic2F2ZUFjY2Vzc0NvbnRyb2wiLCJsZW5ndGgiLCJzdWJtaXQiLCJwcm9wIiwicmVzZXQiLCJ0b2FzdHIiLCJzdWNjZXNzIiwiZXJyb3IiLCJzaG93Um9sZURpYWxvZyIsImFkZFJvbGUiLCJnZXRSb2xlcyIsInNob3dFZGl0Um9sZURpYWxvZyIsInNhdmVSb2xlIiwiY3JlYXRlZCIsIm1vdW50ZWQiLCJvbiIsInBvcG92ZXIiLCJodG1sIiwidHJpZ2dlciIsInNlbGVjdG9yIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNsREFBLElBQUlDLFNBQUosQ0FBYyxjQUFkLEVBQStCQyxtQkFBT0EsQ0FBQyxDQUFSLENBQS9COztBQUVBLElBQUlDLFVBQVUsSUFBSUgsR0FBSixDQUFRO0FBQ3JCSSxLQUFHLGlCQURrQjtBQUVyQkMsT0FBSztBQUNKQyxTQUFNLEVBREY7QUFFSkMsZUFBWSxFQUZSO0FBR0pDLFlBQVMsRUFITDtBQUlKQyxrQkFBZSxFQUpYO0FBS0RDLFFBQU0sSUFBSUMsSUFBSixDQUFTO0FBQ1pDLFdBQU8sQ0FESztBQUVaQyxtQkFBZTtBQUZILEdBQVQsRUFHSixFQUFFQyxTQUFTLHNCQUFYLEVBSEksQ0FMTDtBQVNEQyxZQUFVLElBQUlKLElBQUosQ0FBUztBQUNoQkssT0FBRyxDQURhO0FBRWhCQyxTQUFLO0FBRlcsR0FBVCxFQUdSLEVBQUVILFNBQVMsc0JBQVgsRUFIUTs7QUFUVCxFQUZnQjtBQWlCckJJLFVBQVM7QUFDUkMsc0JBRFEsZ0NBQ2FGLElBRGIsRUFDbUI7QUFBQTs7QUFDMUIsUUFBS1QsUUFBTCxHQUFnQlMsS0FBS0csS0FBckI7QUFDQSxRQUFLVixJQUFMLENBQVVFLE1BQVYsR0FBbUJLLEtBQUtELEVBQXhCOztBQUVBSyxTQUFNQyxHQUFOLENBQVUsZ0RBQThDTCxLQUFLRCxFQUE3RCxFQUNPTyxJQURQLENBQ1ksa0JBQVU7QUFDYixVQUFLYixJQUFMLENBQVVHLGNBQVYsR0FBMkJXLE9BQU9uQixJQUFsQztBQUNMLElBSEo7O0FBS0FnQixTQUFNQyxHQUFOLENBQVUscUNBQVYsRUFDT0MsSUFEUCxDQUNZLGtCQUFVO0FBQ2IsVUFBS2hCLFdBQUwsR0FBbUJpQixPQUFPbkIsSUFBMUI7QUFDTCxJQUhKOztBQUtHZ0IsU0FBTUMsR0FBTixDQUFVLHdDQUFWLEVBQ0lDLElBREosQ0FDUyxrQkFBVTtBQUNiLFVBQUtkLGNBQUwsR0FBc0JlLE9BQU9uQixJQUE3QjtBQUNMLElBSEQ7O0FBS0hvQixLQUFFLG9CQUFGLEVBQXdCQyxLQUF4QixDQUE4QixRQUE5QjtBQUVBLEdBdEJPO0FBdUJGQyxRQXZCRSxrQkF1QktYLEVBdkJMLEVBdUJTWSxDQXZCVCxFQXVCWTtBQUNWLE9BQUdBLEVBQUVDLE1BQUYsQ0FBU0MsUUFBVCxJQUFxQixRQUFyQixJQUFpQ0YsRUFBRUMsTUFBRixDQUFTQyxRQUFULElBQXFCLE1BQXpELEVBQWlFO0FBQzdETCxNQUFFLFNBQVNULEVBQVgsRUFBZWUsV0FBZixDQUEyQixlQUEzQjtBQUNIO0FBQ0osR0EzQkM7QUE0QlJDLFlBNUJRLHNCQTRCRzNCLElBNUJILEVBNEJRNEIsS0E1QlIsRUE0QmM7QUFDWixPQUFHLEtBQUtDLEtBQUwsQ0FBVzdCLElBQVgsS0FBa0IsQ0FBckIsRUFBd0I7QUFDaEMsUUFBSThCLEtBQUssS0FBS3pCLElBQUwsQ0FBVUcsY0FBbkI7QUFDTSxRQUFJdUIsS0FBSyxJQUFUO0FBQ05YLE1BQUVZLElBQUYsQ0FBT0YsRUFBUCxFQUFXLFVBQVNHLENBQVQsRUFBWUMsSUFBWixFQUFrQjtBQUMzQixTQUFHSixHQUFHRyxDQUFILEtBQU9FLFNBQVYsRUFBb0I7QUFDckIsVUFBR0wsR0FBR0csQ0FBSCxFQUFNRyxhQUFOLElBQXVCcEMsS0FBS1csRUFBL0IsRUFBa0M7QUFDL0JvQixVQUFHMUIsSUFBSCxDQUFRRyxjQUFSLENBQXVCNkIsTUFBdkIsQ0FBOEJKLENBQTlCLEVBQWdDLENBQWhDO0FBQ0E7QUFDRDtBQUNGLEtBTkQ7QUFPUyxJQVZELE1BVU87QUFDZixRQUFJSyxJQUFJLEVBQUMsaUJBQWlCdEMsS0FBS1csRUFBdkIsRUFBMkIsV0FBVyxLQUFLTixJQUFMLENBQVVFLE1BQWhELEVBQVI7QUFDWSxTQUFLRixJQUFMLENBQVVHLGNBQVYsQ0FBeUIrQixJQUF6QixDQUE4QkQsQ0FBOUI7QUFDSDtBQUNKLEdBM0NDO0FBNENGVCxPQTVDRSxpQkE0Q0lsQixFQTVDSixFQTRDTztBQUNSLE9BQUk2QixJQUFJLENBQVI7QUFDQSxPQUFJVixLQUFLLEtBQUt6QixJQUFMLENBQVVHLGNBQW5CO0FBQ05ZLEtBQUVZLElBQUYsQ0FBT0YsRUFBUCxFQUFXLFVBQVNHLENBQVQsRUFBWUMsSUFBWixFQUFrQjtBQUMzQixRQUFHSixHQUFHRyxDQUFILEVBQU1HLGFBQU4sSUFBdUJ6QixHQUFHQSxFQUE3QixFQUFnQztBQUMvQjZCLFNBQUksQ0FBSjtBQUNBO0FBQ0YsSUFKRDtBQUtBLFVBQU9BLENBQVA7QUFDTSxHQXJEQztBQXNEUkMsbUJBdERRLCtCQXNEVztBQUFBOztBQUNsQixPQUFHLEtBQUtwQyxJQUFMLENBQVVHLGNBQVYsQ0FBeUJrQyxNQUF6QixJQUFtQyxDQUF0QyxFQUF5QztBQUN4QyxTQUFLckMsSUFBTCxDQUFVc0MsTUFBVixDQUFpQixNQUFqQixFQUF5QixzQkFBekIsRUFDU3pCLElBRFQsQ0FDYyxrQkFBVTtBQUNmRSxPQUFFLGdCQUFGLEVBQW9Cd0IsSUFBcEIsQ0FBeUIsU0FBekIsRUFBbUMsS0FBbkM7QUFDQSxZQUFLdkMsSUFBTCxDQUFVRyxjQUFWLEdBQTJCLEVBQTNCO0FBQ0EsWUFBS3FDLEtBQUw7QUFDQXpCLE9BQUUsb0JBQUYsRUFBd0JDLEtBQXhCLENBQThCLFFBQTlCO0FBQ0F5QixZQUFPQyxPQUFQLENBQWUscUJBQWY7QUFDQSxLQVBUO0FBUU0sSUFUUCxNQVVPRCxPQUFPRSxLQUFQLENBQWEsd0NBQWI7QUFDUCxHQWxFTztBQW1FUkgsT0FuRVEsbUJBbUVEO0FBQ056QixLQUFFLE9BQUYsRUFBVyxDQUFYLEVBQWN5QixLQUFkO0FBQ0EsR0FyRU87QUFzRVJJLGdCQXRFUSw0QkFzRVE7QUFDZjdCLEtBQUUsYUFBRixFQUFpQkMsS0FBakIsQ0FBdUIsUUFBdkI7QUFDQSxHQXhFTztBQXlFUjZCLFNBekVRLHFCQXlFQztBQUFBOztBQUNSLFFBQUt4QyxRQUFMLENBQWNpQyxNQUFkLENBQXFCLE1BQXJCLEVBQTZCLFVBQTdCLEVBQ1N6QixJQURULENBQ2Msa0JBQVU7QUFDZjRCLFdBQU9DLE9BQVAsQ0FBZSxxQkFBZjtBQUNBLFdBQUtyQyxRQUFMLENBQWNFLElBQWQsR0FBcUIsRUFBckI7QUFDQVEsTUFBRSxhQUFGLEVBQWlCQyxLQUFqQixDQUF1QixRQUF2QjtBQUNBLFdBQUs4QixRQUFMO0FBRUEsSUFQVDtBQVFBLEdBbEZPO0FBbUZSQyxvQkFuRlEsOEJBbUZXeEMsSUFuRlgsRUFtRmdCO0FBQUE7O0FBQ3ZCSSxTQUFNQyxHQUFOLENBQVUsa0NBQWdDTCxLQUFLRCxFQUEvQyxFQUNPTyxJQURQLENBQ1ksa0JBQVU7QUFDZixXQUFLUixRQUFMLENBQWNDLEVBQWQsR0FBbUJRLE9BQU9uQixJQUFQLENBQVksQ0FBWixFQUFlVyxFQUFsQztBQUNFLFdBQUtELFFBQUwsQ0FBY0UsSUFBZCxHQUFxQk8sT0FBT25CLElBQVAsQ0FBWSxDQUFaLEVBQWVlLEtBQXBDO0FBQ0FLLE1BQUUsaUJBQUYsRUFBcUJDLEtBQXJCLENBQTJCLFFBQTNCO0FBQ0wsSUFMSjtBQU9BLEdBM0ZPO0FBNEZSOEIsVUE1RlEsc0JBNEZFO0FBQUE7O0FBQ1RuQyxTQUFNQyxHQUFOLENBQVUsK0JBQVYsRUFDT0MsSUFEUCxDQUNZLGtCQUFVO0FBQ2IsV0FBS2pCLEtBQUwsR0FBYWtCLE9BQU9uQixJQUFwQjtBQUNMLElBSEo7QUFJQSxHQWpHTztBQWtHUnFELFVBbEdRLHNCQWtHRTtBQUFBOztBQUNULFFBQUszQyxRQUFMLENBQWNpQyxNQUFkLENBQXFCLE1BQXJCLEVBQTZCLFdBQTdCLEVBQ1N6QixJQURULENBQ2Msa0JBQVU7QUFDZjRCLFdBQU9DLE9BQVAsQ0FBZSxxQkFBZjtBQUNBLFdBQUtyQyxRQUFMLENBQWNDLEVBQWQsR0FBbUIsQ0FBbkI7QUFDQSxXQUFLRCxRQUFMLENBQWNFLElBQWQsR0FBcUIsRUFBckI7QUFDQVEsTUFBRSxpQkFBRixFQUFxQkMsS0FBckIsQ0FBMkIsUUFBM0I7QUFDQSxXQUFLOEIsUUFBTDtBQUNBLElBUFQ7QUFRQTtBQTNHTyxFQWpCWTtBQThIckJHLFFBOUhxQixxQkE4SFo7QUFDUjtBQUNBLEVBaElvQjtBQWlJckJDLFFBaklxQixxQkFpSVo7QUFDUm5DLElBQUUsb0JBQUYsRUFBd0JvQyxFQUF4QixDQUEyQixpQkFBM0IsRUFBOEMsS0FBS1gsS0FBbkQ7O0FBRU16QixJQUFFLE1BQUYsRUFBVXFDLE9BQVYsQ0FBa0IsRUFBRTtBQUNoQkMsU0FBSyxJQURTO0FBRWRDLFlBQVMsT0FGSztBQUdkQyxhQUFVO0FBSEksR0FBbEI7QUFLTjtBQXpJb0IsQ0FBUixDQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcUJBO0FBQ0E7QUFDQSxnQ0FEQTtBQUVBO0FBRkE7QUFEQSxHOzs7Ozs7Ozs7Ozs7Ozs7QUN2QkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBeU87QUFDblA7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBcU07QUFDL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy92aXNhL2ZpbmFuY2luZy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDgwKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmMTEzZWQzNmE1YTU2YjdkY2Y2MyIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDE4IDE5IDIwIDIxIDIyIDIzIDI0IDI1IiwiVnVlLmNvbXBvbmVudCgnZGlhbG9nLW1vZGFsJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlJykpO1xuXG52YXIgVXNlckFwcCA9IG5ldyBWdWUoe1xuXHRlbDonI2FjY2Vzcy1jb250cm9sJyxcblx0ZGF0YTp7XG5cdFx0cm9sZXM6W10sXG5cdFx0cGVybWlzc2lvbnM6W10sXG5cdFx0cm9sZW5hbWU6JycsXG5cdFx0cGVybWlzc2lvbnR5cGU6W10sXG5cdCAgICBmb3JtOiBuZXcgRm9ybSh7XG5cdCAgICAgICByb2xlaWQ6MCxcblx0ICAgICAgIHNlbHBlcm1pc3Npb25zOltdXG5cdCAgICB9LHsgYmFzZVVSTDogJy92aXNhL2FjY2Vzcy1jb250cm9sJ30pLFxuXHQgICAgcm9sZUZvcm06IG5ldyBGb3JtKHtcblx0ICAgICAgIGlkOjAsXG5cdCAgICAgICByb2xlOicnXG5cdCAgICB9LHsgYmFzZVVSTDogJy92aXNhL2FjY2Vzcy1jb250cm9sJ30pXG5cblx0fSxcblx0bWV0aG9kczoge1xuXHRcdHNob3dQZXJtaXNzaW9uRGlhbG9nKHJvbGUpIHtcblx0XHRcdHRoaXMucm9sZW5hbWUgPSByb2xlLmxhYmVsO1xuXHRcdFx0dGhpcy5mb3JtLnJvbGVpZCA9IHJvbGUuaWQ7XG5cblx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0U2VsZWN0ZWRQZXJtaXNzaW9uLycrcm9sZS5pZClcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHQgICAgICAgICAgXHR0aGlzLmZvcm0uc2VscGVybWlzc2lvbnMgPSByZXN1bHQuZGF0YTtcblx0ICAgIFx0fSk7XG5cblx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0UGVybWlzc2lvbnMnKVxuXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICBcdHRoaXMucGVybWlzc2lvbnMgPSByZXN1bHQuZGF0YTtcblx0ICAgIFx0fSk7XG5cblx0ICAgIFx0YXhpb3MuZ2V0KCcvdmlzYS9hY2Nlc3MtY29udHJvbC9nZXRQZXJtaXNzaW9uVHlwZScpXG5cdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICAgIFx0dGhpcy5wZXJtaXNzaW9udHlwZSA9IHJlc3VsdC5kYXRhO1xuXHQgICAgXHR9KTtcblxuXHRcdFx0JCgnI2RpYWxvZ1Blcm1pc3Npb25zJykubW9kYWwoJ3RvZ2dsZScpO1xuXG5cdFx0fSxcbiAgICAgICAgdG9nZ2xlKGlkLCBlKSB7XG4gICAgICAgICAgICBpZihlLnRhcmdldC5ub2RlTmFtZSAhPSAnQlVUVE9OJyAmJiBlLnRhcmdldC5ub2RlTmFtZSAhPSAnU1BBTicpIHtcbiAgICAgICAgICAgICAgICAkKCcjZmEtJyArIGlkKS50b2dnbGVDbGFzcygnZmEtYXJyb3ctZG93bicpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXHRcdHNlbGVjdEl0ZW0oZGF0YSxpbmRleCl7XG4gICAgICAgICAgICBpZih0aGlzLmNoZWNrKGRhdGEpPT0xKSB7XG5cdFx0XHRcdHZhciBzcCA9IHRoaXMuZm9ybS5zZWxwZXJtaXNzaW9ucztcblx0ICAgICAgICBcdHZhciB2aSA9IHRoaXM7XG5cdFx0XHRcdCQuZWFjaChzcCwgZnVuY3Rpb24oaSwgaXRlbSkge1xuXHRcdFx0XHQgIGlmKHNwW2ldIT11bmRlZmluZWQpe1xuXHRcdFx0XHRcdGlmKHNwW2ldLnBlcm1pc3Npb25faWQgPT0gZGF0YS5pZCl7XG5cdFx0XHRcdCAgXHRcdHZpLmZvcm0uc2VscGVybWlzc2lvbnMuc3BsaWNlKGksMSk7XG5cdFx0XHRcdCAgXHR9XG5cdFx0XHRcdCAgfVxuXHRcdFx0XHR9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG5cdFx0XHRcdHZhciBkID0ge1wicGVybWlzc2lvbl9pZFwiOiBkYXRhLmlkLCBcInJvbGVfaWRcIjogdGhpcy5mb3JtLnJvbGVpZH07XG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtLnNlbHBlcm1pc3Npb25zLnB1c2goZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGNoZWNrKGlkKXtcbiAgICAgICAgXHR2YXIgYSA9IDA7XG4gICAgICAgIFx0dmFyIHNwID0gdGhpcy5mb3JtLnNlbHBlcm1pc3Npb25zO1xuXHRcdFx0JC5lYWNoKHNwLCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cdFx0XHQgIGlmKHNwW2ldLnBlcm1pc3Npb25faWQgPT0gaWQuaWQpe1xuXHRcdFx0ICBcdGEgPSAxO1xuXHRcdFx0ICB9XG5cdFx0XHR9KTtcblx0XHRcdHJldHVybiBhO1xuICAgICAgICB9LFxuXHRcdHNhdmVBY2Nlc3NDb250cm9sKCl7XG5cdFx0XHRpZih0aGlzLmZvcm0uc2VscGVybWlzc2lvbnMubGVuZ3RoICE9IDApIHtcblx0XHRcdFx0dGhpcy5mb3JtLnN1Ym1pdCgncG9zdCcsICcvdXBkYXRlQWNjZXNzQ29udHJvbCcpXG5cdFx0ICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdFx0ICAgICAgICAgIFx0JCgnaW5wdXQ6Y2hlY2tib3gnKS5wcm9wKCdjaGVja2VkJyxmYWxzZSk7XG5cdFx0ICAgICAgICAgIFx0dGhpcy5mb3JtLnNlbHBlcm1pc3Npb25zID0gW107XG5cdFx0ICAgICAgICAgIFx0dGhpcy5yZXNldCgpO1xuXHRcdCAgICAgICAgICBcdCQoJyNkaWFsb2dQZXJtaXNzaW9ucycpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHQgICAgICAgICAgXHR0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xuXHRcdCAgICAgICAgICB9KTtcblx0ICAgICAgICB9IGVsc2UgXG5cdCAgICAgICAgXHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgYXQgbGVhc3Qgb25lIHBlcm1pc3Npb24uJyk7XG5cdFx0fSxcblx0XHRyZXNldCgpe1xuXHRcdFx0JCgnI2Zvcm0nKVswXS5yZXNldCgpO1xuXHRcdH0sXG5cdFx0c2hvd1JvbGVEaWFsb2coKXtcblx0XHRcdCQoJyNkaWFsb2dSb2xlJykubW9kYWwoJ3RvZ2dsZScpO1xuXHRcdH0sXG5cdFx0YWRkUm9sZSgpe1xuXHRcdFx0dGhpcy5yb2xlRm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL2FkZFJvbGUnKVxuXHQgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICAgIFx0dG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBzYXZlZC4nKTtcblx0ICAgICAgICAgIFx0dGhpcy5yb2xlRm9ybS5yb2xlID0gJyc7XG5cdCAgICAgICAgICBcdCQoJyNkaWFsb2dSb2xlJykubW9kYWwoJ3RvZ2dsZScpO1xuXHQgICAgICAgICAgXHR0aGlzLmdldFJvbGVzKCk7XG5cblx0ICAgICAgICAgIH0pO1xuXHRcdH0sXG5cdFx0c2hvd0VkaXRSb2xlRGlhbG9nKHJvbGUpe1xuXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9hY2Nlc3MtY29udHJvbC9nZXRSb2xlLycrcm9sZS5pZClcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHQgICAgICAgIFx0dGhpcy5yb2xlRm9ybS5pZCA9IHJlc3VsdC5kYXRhWzBdLmlkO1xuXHQgICAgICAgICAgXHR0aGlzLnJvbGVGb3JtLnJvbGUgPSByZXN1bHQuZGF0YVswXS5sYWJlbDtcblx0ICAgICAgICAgIFx0JCgnI2RpYWxvZ0VkaXRSb2xlJykubW9kYWwoJ3RvZ2dsZScpO1xuXHQgICAgXHR9KTtcblxuXHRcdH0sXG5cdFx0Z2V0Um9sZXMoKXtcblx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0Um9sZXMnKVxuXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICBcdHRoaXMucm9sZXMgPSByZXN1bHQuZGF0YTtcblx0ICAgIFx0fSk7XG5cdFx0fSxcblx0XHRzYXZlUm9sZSgpe1xuXHRcdFx0dGhpcy5yb2xlRm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3NhdmVSb2xlJylcblx0ICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICBcdHRvYXN0ci5zdWNjZXNzKCdTdWNjZXNzZnVsbHkgc2F2ZWQuJyk7XG5cdCAgICAgICAgICBcdHRoaXMucm9sZUZvcm0uaWQgPSAwO1xuXHQgICAgICAgICAgXHR0aGlzLnJvbGVGb3JtLnJvbGUgPSAnJztcblx0ICAgICAgICAgIFx0JCgnI2RpYWxvZ0VkaXRSb2xlJykubW9kYWwoJ3RvZ2dsZScpO1xuXHQgICAgICAgICAgXHR0aGlzLmdldFJvbGVzKCk7XG5cdCAgICAgICAgICB9KTtcblx0XHR9XG5cdH0sXG5cdGNyZWF0ZWQoKXtcblx0XHQvLyBcblx0fSxcblx0bW91bnRlZCgpe1xuXHRcdCQoJyNkaWFsb2dQZXJtaXNzaW9ucycpLm9uKFwiaGlkZGVuLmJzLm1vZGFsXCIsIHRoaXMucmVzZXQpO1xuXG4gICAgICAgICQoJ2JvZHknKS5wb3BvdmVyKHsgLy8gT2tcbiAgICAgICAgICAgIGh0bWw6dHJ1ZSxcbiAgICAgICAgICAgIHRyaWdnZXI6ICdob3ZlcicsXG4gICAgICAgICAgICBzZWxlY3RvcjogJ1tkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIl0nXG4gICAgICAgIH0pO1xuXHR9XG59KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvZmluYW5jaW5nL2luZGV4LmpzIiwiPHRlbXBsYXRlPlxuPGRpdiBjbGFzcz1cIm1vZGFsIG1kLW1vZGFsIGZhZGVcIiA6aWQ9XCJpZFwiIHRhYmluZGV4PVwiLTFcIiByb2xlPVwiZGlhbG9nXCIgYXJpYS1sYWJlbGxlZGJ5PVwibW9kYWwtbGFiZWxcIj5cbiAgICA8ZGl2IDpjbGFzcz1cIidtb2RhbC1kaWFsb2cgJytzaXplXCIgcm9sZT1cImRvY3VtZW50XCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtaGVhZGVyXCI+XG4gICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwibW9kYWwtdGl0bGVcIiBpZD1cIm1vZGFsLWxhYmVsXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC10aXRsZVwiPk1vZGFsIFRpdGxlPC9zbG90PlxuICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+XG4gICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLWJvZHlcIj5Nb2RhbCBCb2R5PC9zbG90PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLWZvb3RlclwiPlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG48L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gICAgcHJvcHM6IHtcbiAgICAgICAgJ2lkJzp7cmVxdWlyZWQ6dHJ1ZX1cbiAgICAgICAgLCdzaXplJzoge2RlZmF1bHQ6J21vZGFsLW1kJ31cbiAgICB9XG59XG48L3NjcmlwdD5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBEaWFsb2dNb2RhbC52dWU/MDAzYmRhODgiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTRkZTYwNjU1XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIERpYWxvZ01vZGFsLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi00ZGU2MDY1NVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTRkZTYwNjU1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDVcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDUgNiA5IDExIDE4IDE5IDIwIDIxIDIyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwgbWQtbW9kYWwgZmFkZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IF92bS5pZCxcbiAgICAgIFwidGFiaW5kZXhcIjogXCItMVwiLFxuICAgICAgXCJyb2xlXCI6IFwiZGlhbG9nXCIsXG4gICAgICBcImFyaWEtbGFiZWxsZWRieVwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGNsYXNzOiAnbW9kYWwtZGlhbG9nICcgKyBfdm0uc2l6ZSxcbiAgICBhdHRyczoge1xuICAgICAgXCJyb2xlXCI6IFwiZG9jdW1lbnRcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWhlYWRlclwiXG4gIH0sIFtfYygnaDQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtdGl0bGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiLCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtYm9keVwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1ib2R5XCIsIFtfdm0uX3YoXCJNb2RhbCBCb2R5XCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtZm9vdGVyXCIsIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTRkZTYwNjU1XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNGRlNjA2NTVcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDUgNiA5IDExIDE4IDE5IDIwIDIxIDIyIl0sInNvdXJjZVJvb3QiOiIifQ==