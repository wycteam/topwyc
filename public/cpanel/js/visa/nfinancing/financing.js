/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 529);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 10:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 206:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('finance-content', __webpack_require__(400));

Vue.http.interceptors.push(function (request, next) {
  request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

  next();
});
var app = new Vue({
  el: '#nFinancings'
});

var toggler = document.getElementsByClassName("carets");
var processLocator = document.getElementById("processLocator");
var i;
var sel;
for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function () {
    this.parentElement.querySelector(".nested").classList.toggle("active");
    this.classList.toggle("carets-down");
    // if($.trim(processLocator.innerHTML).length == 0){
    //     processLocator.append(this.innerHTML);
    // }else{
    //     processLocator.append(" > "+this.innerHTML);
    // }
  });
}

var toggler2 = document.getElementsByClassName("root");

for (i = 0; i < toggler2.length; i++) {
  toggler2[i].addEventListener("click", function () {
    $(sel).removeClass('rootSelected');
    sel = this;
    var inputForm = document.getElementById("inputForm");
    $(inputForm).removeClass("inputForm");
    $(inputForm).addClass('inputForm2');
    $(sel).addClass('rootSelected');
    var p = this;
    while ($(p).parent('.carets').length) {
      p = $(p).parent('.carets');
      console.log($(p).parent('.carets').html());
    }
    //$(processLocator).html($(this).parents().parentsUntil('#myUL').html().replace(/(<([^>]+)>)/ig,"").replace( /\s\s+/g, '|'));
    //console.log($(this).parentsUntil('.liParent').closest('span').html())
    //$(this).parents().parentsUntil('li .liParent').css("background-color","black");
  });
}

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['columns', 'sortKey', 'sortOrders']
});

$(function () {
    $('.wrapper11').on('scroll', function (e) {
        $('.wrapper22').scrollLeft($('.wrapper11').scrollLeft());
    });
    $('.wrapper22').on('scroll', function (e) {
        $('.wrapper11').scrollLeft($('.wrapper22').scrollLeft());
    });
});
$(document).ready(function () {
    //$('.div11').width($('#ftable').width());
    $('.div22').width($('#ftable').width());
    $('.div11').width($('#ftable').width() + 300);

    //alert($('.div22')[0].scrollWidth+' | '+$('#ftable').width());
});

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    initial_cash: Number,
    initial_bank: Number,
    bank_balance: Number,
    cash_balance: Number,
    total_balance: Number,
    total_profit: Number,
    process_output: Number,
    we_receive_before: Number,
    we_receive_after: Number,
    saveProject: Function,
    numberWithCommas: Function,
    checkTrans: Function,
    borrowed: Array,
    cat_type: String
  }
});

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['pagination', 'client', 'filtered']
});

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Financing_vue__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Financing_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Financing_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__DataTable_vue__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__DataTable_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__DataTable_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Pagination_vue__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Pagination_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__Pagination_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var process_output = 0;



var resource = __webpack_require__(9);
var user = window.Laravel.user;

/* harmony default export */ __webpack_exports__["default"] = ({
    components: { FinanceEditor: __WEBPACK_IMPORTED_MODULE_0__Financing_vue___default.a, FinanceTable: __WEBPACK_IMPORTED_MODULE_1__DataTable_vue___default.a, Pagination: __WEBPACK_IMPORTED_MODULE_2__Pagination_vue___default.a },

    data: function data() {

        var sortOrders = {};
        var columns = [{ width: '20%', label: 'Transaction Description', name: 'trans_desc' }, { width: '5.33%', label: 'Type', name: 'cat_type' }, { width: '5.33%', label: 'Storage', name: 'cat_storage' }, { width: '5.33%', label: 'Cash Deposit/Payment', name: 'cash_client_depo_payment' }, { width: '5.33%', label: 'Refund', name: 'cash_client_refund' }, { width: '5.33%', label: 'Process Budget Return', name: 'cash_client_process_budget_return' }, { width: '5.33%', label: 'Process Cost', name: 'cash_process_cost' }, { width: '5.33%', label: 'Borrowed Process Cost', name: 'borrowed_process_cost' }, { width: '5.33%', label: 'Admin Budget Return', name: 'cash_admin_budget_return' }, { width: '5.33%', label: 'Borrowed Admin Cost', name: 'borrowed_admin_cost' }, { width: '5.33%', label: 'Admin Cost', name: 'cash_admin_cost' }, { width: '5.33%', label: 'Bank Deposit/Payment', name: 'bank_client_depo_payment' }, { width: '5.33%', label: 'Bank Cost', name: 'bank_cost' }, { width: '5.33%', label: 'Deposit Other Matter', name: 'deposit_other' }, { width: '5.33%', label: 'Cost Other Matter', name: 'cost_other' }, { width: '5.33%', label: 'Process Output', name: 'process_output' }, { width: '5.33%', label: 'How much we received from client', name: 'amount_before' }, { width: '5.33%', label: 'How much we received after we spent for client', name: 'amount_after' }, { width: '5.33%', label: 'Admin Total', name: 'admin_total' }, { width: '5.33%', label: 'Cash Balance', name: 'cash_balance' }, { width: '5.33%', label: 'Bank Balance', name: 'bank_balance' }, { width: '5.33%', label: 'Total', name: 'total' }, { width: '5.33%', label: 'Profit', name: 'profit' }, { width: '5.33%', label: 'Checks', name: 'postdated_checks' }];
        columns.forEach(function (column) {
            sortOrders[column.name] = -1;
        });
        return {
            cat_type: '',
            borrowed: [],
            selectedFile: null,
            initial_cash: 0,
            initial_bank: 0,
            cash_balance: 0,
            bank_balance: 0,
            total_balance: 0,
            total_profit: 0,
            process_output: 0,
            we_receive_before: 0,
            we_receive_after: 0,
            finance: {
                trans_desc: '',
                cat_type: '',
                cat_storage: '',
                cash_client_depo_payment: '',
                cash_client_refund: '',
                cash_client_process_budget_return: '',
                cash_process_cost: '',
                borrowed_process_cost: '',
                cash_admin_budget_return: '',
                borrowed_admin_cost: '',
                cash_admin_cost: '',
                bank_client_depo_payment: '',
                bank_cost: '',
                cash_balance: '',
                bank_balance: '',
                postdated_checks: '',
                cash_ther: '',
                deposit_other: ''
            },
            success: false,
            edit: false,
            tmpHeader: '',
            tmpContent: '',
            tmpHref: false,
            projects: [],
            projects2: [],
            columns: columns,
            sortKey: 'id',
            sortOrders: sortOrders,
            perPage: ['100000', '200000', '300000'],
            tableData: {
                draw: 0,
                length: 100000,
                search: '',
                column: 0,
                dir: 'desc'
            },
            pagination: {
                lastPage: '',
                currentPage: '',
                total: '',
                lastPageUrl: '',
                nextPageUrl: '',
                prevPageUrl: '',
                from: '',
                to: ''
            }

        };
    },

    methods: {
        numberWithCommas: function numberWithCommas(x) {
            if (x == '' || x == null) {
                return '';
            }

            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        checkTrans: function checkTrans(event) {
            var _this = this;

            //alert(event.target.id);
            this.cat_type = event.target.value;
            this.borrowed = [];
            if (event.target.value != 'bpc' && event.target.value != 'bac') {
                axios.get('./nfinancing/get-borrowed', {
                    params: {
                        cat_type: event.target.value
                    }
                }).then(function (response) {
                    console.log(response.data);
                    _this.borrowed = response.data;
                });
            }
        },
        getProjects: function getProjects() {
            var _this2 = this;

            var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : './nfinancing/get-finance';

            this.projects2 = [];
            var initial_bank = 0,
                initial_cash = 0,
                processt = 0,
                tclient_dep = 0,
                tclient_dep2 = 0,
                admin_total = 0,
                cash_total_balance = 0,
                bank_total_balance = 0,
                total = 0,
                profit = 0;
            this.tableData.draw++;
            axios.get(url, { params: this.tableData }).then(function (response) {
                var data = response.data;
                if (_this2.tableData.draw == data.draw) {
                    _this2.projects = data.data.data;
                    _this2.configPagination(data.data);

                    var finance2 = {};
                    var arr = [];

                    //console.log(this.projects);
                    $.each(_this2.projects.reverse(), function (key, value) {
                        var cost = (value.cash_process_cost != null && value.cash_process_cost != '' ? parseFloat(value.cash_process_cost) : 0) + (value.borrowed_process_cost != null && value.borrowed_process_cost != '' ? parseFloat(value.borrowed_process_cost) : 0) - (value.cash_client_process_budget_return != null && value.cash_client_process_budget_return != '' ? parseFloat(value.cash_client_process_budget_return) : 0);
                        var client_dep = (value.borrowed_process_cost != null && value.borrowed_process_cost != '' ? parseFloat(value.borrowed_process_cost) : 0) + (value.cash_client_depo_payment != null && value.cash_client_depo_payment != '' ? parseFloat(value.cash_client_depo_payment) : 0) + (value.bank_client_depo_payment != null && value.bank_client_depo_payment != '' ? parseFloat(value.bank_client_depo_payment) : 0) - (value.cash_client_refund != null && value.cash_client_refund != '' ? parseFloat(value.cash_client_refund) : 0);
                        var client_dep_after = (value.cash_client_depo_payment != null && value.cash_client_depo_payment != '' ? parseFloat(value.cash_client_depo_payment) : 0) + (value.cash_client_process_budget_return != null && value.cash_client_process_budget_return != '' ? parseFloat(value.cash_client_process_budget_return) : 0) + (value.borrowed_process_cost != null && value.borrowed_process_cost != '' ? parseFloat(value.borrowed_process_cost) : 0) + (value.bank_client_depo_payment != null && value.bank_client_depo_payment != '' ? parseFloat(value.bank_client_depo_payment) : 0) - ((value.cash_client_refund != null && value.cash_client_refund != '' ? parseFloat(value.cash_client_refund) : 0) + (value.cash_process_cost != null && value.cash_process_cost != '' ? parseFloat(value.cash_process_cost) : 0));
                        var admin_t = (value.cash_admin_cost != null && value.cash_admin_cost != '' ? parseFloat(value.cash_admin_cost) : 0) - (value.cash_admin_budget_return != null && value.cash_admin_budget_return != '' ? parseFloat(value.cash_admin_budget_return) : 0);
                        var tmpcash_balance = (value.cash_balance != null && value.cash_balance != '' ? parseFloat(value.cash_balance) : 0) + (value.cash_client_depo_payment != null && value.cash_client_depo_payment != '' ? parseFloat(value.cash_client_depo_payment) : 0) + (value.cash_client_process_budget_return != null && value.cash_client_process_budget_return != '' ? parseFloat(value.cash_client_process_budget_return) : 0) + (value.cash_admin_budget_return != null && value.cash_admin_budget_return != '' ? parseFloat(value.cash_admin_budget_return) : 0) + (value.deposit_other != null && value.deposit_other != '' && value.cat_storage == 'Cash' ? parseFloat(value.deposit_other) : 0) - ((value.cash_client_refund != null && value.cash_client_refund != '' ? parseFloat(value.cash_client_refund) : 0) + (value.cash_process_cost != null && value.cash_process_cost != '' ? parseFloat(value.cash_process_cost) : 0) + (value.cash_admin_cost != null && value.cash_admin_cost != '' ? parseFloat(value.cash_admin_cost) : 0) + (value.borrowed_admin_cost != null && value.borrowed_admin_cost != '' ? parseFloat(value.borrowed_admin_cost) : 0) + (value.borrowed_process_cost != null && value.borrowed_process_cost != '' ? parseFloat(value.borrowed_process_cost) : 0) + (value.cost_other != null && value.cost_other != '' && value.cat_storage == 'Cash' ? parseFloat(value.cost_other) : 0));
                        var tmpbank_balance = (value.bank_balance != null && value.bank_balance != '' ? parseFloat(value.bank_balance) : 0) + (value.bank_client_depo_payment != null && value.bank_client_depo_payment != '' ? parseFloat(value.bank_client_depo_payment) : 0) + (value.deposit_other != null && value.deposit_other != '' && value.cat_storage == 'Bank' ? parseFloat(value.deposit_other) : 0) - ((value.bank_cost != null && value.bank_cost != '' ? parseFloat(value.bank_cost) : 0) + (value.cost_other != null && value.cost_other != '' && value.cat_storage == 'Bank' ? parseFloat(value.cost_other) : 0));
                        var tmptotal = (value.cash_balance != null && value.cash_balance != '' ? parseFloat(value.cash_balance) : 0) + (value.cash_client_depo_payment != null && value.cash_client_depo_payment != '' ? parseFloat(value.cash_client_depo_payment) : 0) + (value.cash_client_process_budget_return != null && value.cash_client_process_budget_return != '' ? parseFloat(value.cash_client_process_budget_return) : 0) + (value.cash_admin_budget_return != null && value.cash_admin_budget_return != '' ? parseFloat(value.cash_admin_budget_return) : 0) + (value.bank_balance != null && value.bank_balance != '' ? parseFloat(value.bank_balance) : 0) + (value.bank_client_depo_payment != null && value.bank_client_depo_payment != '' ? parseFloat(value.bank_client_depo_payment) : 0) + (value.deposit_other != null && value.deposit_other != '' ? parseFloat(value.deposit_other) : 0) - ((value.cash_client_refund != null && value.cash_client_refund != '' ? parseFloat(value.cash_client_refund) : 0) + (value.cash_process_cost != null && value.cash_process_cost != '' ? parseFloat(value.cash_process_cost) : 0) + (value.cash_admin_cost != null && value.cash_admin_cost != '' ? parseFloat(value.cash_admin_cost) : 0) + (value.borrowed_admin_cost != null && value.borrowed_admin_cost != '' ? parseFloat(value.borrowed_admin_cost) : 0) + (value.borrowed_process_cost != null && value.borrowed_process_cost != '' ? parseFloat(value.borrowed_process_cost) : 0) + (value.cost_other != null && value.cost_other != '' ? parseFloat(value.cost_other) : 0) + (value.bank_cost != null && value.bank_cost != '' ? parseFloat(value.bank_cost) : 0));
                        var tmpprofit = (value.cash_client_depo_payment != null && value.cash_client_depo_payment != '' ? parseFloat(value.cash_client_depo_payment) : 0) + (value.cash_client_process_budget_return != null && value.cash_client_process_budget_return != '' ? parseFloat(value.cash_client_process_budget_return) : 0) + (value.borrowed_process_cost != null && value.borrowed_process_cost != '' ? parseFloat(value.borrowed_process_cost) : 0) + (value.cash_admin_budget_return != null && value.cash_admin_budget_return != '' ? parseFloat(value.cash_admin_budget_return) : 0) + (value.borrowed_admin_cost != null && value.borrowed_admin_cost != '' ? parseFloat(value.borrowed_admin_cost) : 0) + (value.bank_client_depo_payment != null && value.bank_client_depo_payment != '' ? parseFloat(value.bank_client_depo_payment) : 0) - ((value.cash_client_refund != null && value.cash_client_refund != '' ? parseFloat(value.cash_client_refund) : 0) + (value.cash_process_cost != null && value.cash_process_cost != '' ? parseFloat(value.cash_process_cost) : 0) + (value.cash_admin_cost != null && value.cash_admin_cost != '' ? parseFloat(value.cash_admin_cost) : 0) + (value.bank_cost != null && value.bank_cost != '' ? parseFloat(value.bank_cost) : 0));
                        initial_cash += value.cash_balance != null && value.cash_balance != '' ? parseFloat(value.cash_balance) : 0;
                        initial_bank += value.bank_balance != null && value.bank_balance != '' ? parseFloat(value.bank_balance) : 0;
                        processt = processt + cost; //process cost
                        tclient_dep = tclient_dep + client_dep; //how much we have before
                        tclient_dep2 = tclient_dep2 + client_dep_after;
                        admin_total = admin_total + admin_t;
                        cash_total_balance = cash_total_balance + tmpcash_balance;
                        bank_total_balance = bank_total_balance + tmpbank_balance;
                        total = total + tmptotal;
                        profit = profit + tmpprofit;
                        finance2 = {
                            id: value.id,
                            trans_desc: value.trans_desc,
                            cat_type: value.cat_type,
                            cat_storage: value.cat_storage,
                            cash_client_depo_payment: value.cash_client_depo_payment,
                            cash_client_refund: value.cash_client_refund,
                            cash_client_process_budget_return: value.cash_client_process_budget_return,
                            cash_process_cost: value.cash_process_cost,
                            borrowed_process_cost: value.borrowed_process_cost,
                            cash_admin_budget_return: value.cash_admin_budget_return,
                            borrowed_admin_cost: value.borrowed_admin_cost,
                            cash_admin_cost: value.cash_admin_cost,
                            bank_client_depo_payment: value.bank_client_depo_payment,
                            bank_cost: value.bank_cost,
                            cash_balance: cash_total_balance,
                            bank_balance: bank_total_balance,
                            postdated_checks: value.postdated_checks,
                            cost_other: value.cost_other,
                            deposit_other: value.deposit_other,
                            total_process: processt,
                            we_have_before: tclient_dep,
                            we_have_after: tclient_dep2,
                            admin_total: admin_total,
                            total: total,
                            profit: profit
                        };
                        arr.push(finance2);
                    });
                    if (_this2.tableData.search == '') {
                        _this2.we_receive_before = tclient_dep;
                        _this2.we_receive_after = tclient_dep2;
                        _this2.cash_balance = cash_total_balance;
                        _this2.bank_balance = bank_total_balance;
                        _this2.initial_cash = initial_cash;
                        _this2.initial_bank = initial_bank;
                        _this2.total_profit = profit;
                        _this2.total_balance = total;
                        _this2.process_output = processt;
                    }
                    _this2.projects2 = arr.reverse();
                }
            }).catch(function (errors) {
                console.log(errors);
            });
        },
        saveProject: function saveProject() {
            //  console.log(jQuery.trim($('#txtAmount').val()));
            if (jQuery.trim($('#txtAmount').val()).length == 0) {
                swal({
                    title: "Warning",
                    text: "Please insert amount"
                });
            } else if (jQuery.trim($('#txtDescription').val()).length == 0 && (this.cat_type == 'bpc' || this.cat_type == 'bac')) {
                swal({
                    title: "Warning",
                    text: "Please input description"
                });
            } else if (!$.isNumeric($('#txtAmount').val())) {
                swal({
                    title: "Warning",
                    text: "Amount must be number only"
                });
            } else {
                if (event.target.value == 'bpc' || event.target.value == 'bac') {
                    //console.log($("#transaction_type").val());
                    this.finance = {
                        trans_desc: $('#txtDescription').val(),
                        cat_type: $("#transaction_type").val() == 'pbr' || $("#transaction_type").val() == 'pc' || $("#transaction_type").val() == 'bpc' ? 'Process' : 'Admin',
                        cat_storage: $("#storage_type").val(),
                        cash_client_depo_payment: '',
                        cash_client_refund: '',
                        cash_client_process_budget_return: $("#transaction_type").val() == 'pbr' ? $('#txtAmount').val() : '',
                        cash_process_cost: $("#transaction_type").val() == 'pc' ? $('#txtAmount').val() : '',
                        borrowed_process_cost: $("#transaction_type").val() == 'bpc' ? $('#txtAmount').val() : '',
                        cash_admin_budget_return: $("#transaction_type").val() == 'abr' ? $('#txtAmount').val() : '',
                        borrowed_admin_cost: $("#transaction_type").val() == 'bac' ? $('#txtAmount').val() : '',
                        cash_admin_cost: $("#transaction_type").val() == 'ac' ? $('#txtAmount').val() : '',
                        bank_client_depo_payment: '',
                        bank_cost: '',
                        cash_balance: '',
                        bank_balance: '',
                        postdated_checks: ''
                    };
                    this.$http.post('./nfinancing/get-finance', this.finance).catch(function (error) {
                        if (error.status == 422) {}
                    }).then(function () {
                        this.finance = {
                            trans_desc: '',
                            cat_type: '',
                            cat_storage: '',
                            cash_client_depo_payment: '',
                            cash_client_refund: '',
                            cash_client_process_budget_return: '',
                            cash_process_cost: '',
                            borrowed_process_cost: '',
                            cash_admin_budget_return: '',
                            borrowed_admin_cost: '',
                            cash_admin_cost: '',
                            bank_client_depo_payment: '',
                            bank_cost: '',
                            cash_balance: '',
                            bank_balance: '',
                            postdated_checks: ''
                        };
                        $('#txtDescription').val('');
                        $('#txtAmount').val('');
                        swal({
                            title: "Successful!",
                            text: "Data has been saved!",
                            type: "success"
                        });
                        this.cat_type = '';
                        this.borrowed = [];
                        this.getProjects();
                    });
                } else {
                    this.$http.post('./nfinancing/update-finance', {
                        params: {
                            cat_type: this.cat_type,
                            amount: $('#txtAmount').val(),
                            id: $('#borrowed_list').val()
                        }
                    }).catch(function (error) {
                        if (error.status == 422) {}
                    }).then(function () {
                        $('#txtDescription').val('');
                        $('#txtAmount').val('');
                        swal({
                            title: "Successful!",
                            text: "Data has been updated!",
                            type: "success"
                        });
                        this.cat_type = '';
                        this.borrowed = [];
                        this.getProjects();
                    });
                }
            }
        },

        computeProcessOutput: function computeProcessOutput(cost, borrowed, cash_return) {
            cost = cost != null && cost != '' ? cost : 0;
            borrowed = borrowed != null && borrowed != '' ? borrowed : 0;
            cash_return = cash_return != null && cash_return != '' ? cash_return : 0;
            //console.log(cost+'--'+borrowed+'--'+cash_return);
            process_output = process_output + (cost + borrowed - cash_return);
            //console.log(process_output);
            return cost + borrowed - cash_return;
        },
        configPagination: function configPagination(data) {
            this.pagination.lastPage = data.last_page;
            this.pagination.currentPage = data.current_page;
            this.pagination.total = data.total;
            this.pagination.lastPageUrl = data.last_page_url;
            this.pagination.nextPageUrl = data.next_page_url;
            this.pagination.prevPageUrl = data.prev_page_url;
            this.pagination.from = data.from;
            this.pagination.to = data.to;
        },
        sortBy: function sortBy(key) {
            this.sortKey = key;
            this.sortOrders[key] = this.sortOrders[key] * -1;
            this.tableData.column = this.getIndex(this.columns, 'name', key);
            this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
            this.getProjects();
        },
        getIndex: function getIndex(array, key, value) {
            return array.findIndex(function (i) {
                return i[key] == value;
            });
        }
    },
    computed: {},
    created: function created() {
        this.getProjects();
    },

    updated: function updated() {
        //this.getProjects();

    }
});

/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(294),
  /* template */
  __webpack_require__(421),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/NewFinancing/DataTable.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DataTable.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-246b700c", Component.options)
  } else {
    hotAPI.reload("data-v-246b700c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 398:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(295),
  /* template */
  __webpack_require__(417),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/NewFinancing/Financing.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Financing.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1629965f", Component.options)
  } else {
    hotAPI.reload("data-v-1629965f", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 399:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(296),
  /* template */
  __webpack_require__(436),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/NewFinancing/Pagination.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Pagination.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-54d46bfc", Component.options)
  } else {
    hotAPI.reload("data-v-54d46bfc", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 400:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(297),
  /* template */
  __webpack_require__(412),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/NewFinancing/Projects.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Projects.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0139a23c", Component.options)
  } else {
    hotAPI.reload("data-v-0139a23c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 412:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('FinanceEditor', {
    attrs: {
      "cat_type": _vm.cat_type,
      "borrowed": _vm.borrowed,
      "checkTrans": _vm.checkTrans,
      "numberWithCommas": _vm.numberWithCommas,
      "we_receive_before": _vm.we_receive_before,
      "we_receive_after": _vm.we_receive_after,
      "process_output": _vm.process_output,
      "total_profit": _vm.total_profit,
      "total_balance": _vm.total_balance,
      "cash_balance": _vm.cash_balance,
      "bank_balance": _vm.bank_balance,
      "initial_bank": _vm.initial_bank,
      "initial_cash": _vm.initial_cash,
      "saveProject": _vm.saveProject
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "ibox float-e-margins"
  }, [_c('div', {
    staticClass: "ibox-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-1 m-b-xs",
    staticStyle: {
      "display": "none"
    }
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.tableData.length),
      expression: "tableData.length"
    }],
    staticClass: "input-sm form-control input-s-sm inline",
    on: {
      "change": [function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.tableData.length = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }, function($event) {
        _vm.getProjects()
      }]
    }
  }, _vm._l((_vm.perPage), function(records, index) {
    return _c('option', {
      key: index,
      domProps: {
        "value": records
      }
    }, [_vm._v(_vm._s(records))])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-offset-7 col-sm-4"
  }, [_c('div', {
    staticClass: "input-group pull-right"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.tableData.search),
      expression: "tableData.search"
    }],
    staticClass: "input-sm form-control",
    attrs: {
      "type": "text",
      "placeholder": "Search"
    },
    domProps: {
      "value": (_vm.tableData.search)
    },
    on: {
      "input": [function($event) {
        if ($event.target.composing) { return; }
        _vm.tableData.search = $event.target.value
      }, function($event) {
        _vm.getProjects()
      }]
    }
  })])])]), _vm._v(" "), _c('FinanceTable', {
    attrs: {
      "columns": _vm.columns,
      "sortKey": _vm.sortKey,
      "sortOrders": _vm.sortOrders
    },
    on: {
      "sort": _vm.sortBy
    }
  }, [_c('tbody', _vm._l((_vm.projects2), function(project) {
    return _c('tr', {
      key: _vm.projects2.id
    }, [_c('td', [_vm._v(_vm._s(project.trans_desc))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(project.cat_type))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(project.cat_storage))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.cash_client_depo_payment)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.cash_client_refund)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.cash_client_process_budget_return)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.cash_process_cost)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.borrowed_process_cost)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.cash_admin_budget_return)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.borrowed_admin_cost)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.cash_admin_cost)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.bank_client_depo_payment)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.bank_cost)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.deposit_other)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.cost_other)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.total_process)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.we_have_before)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.we_have_after)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.admin_total)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.cash_balance)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.bank_balance)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.total)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.profit)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.numberWithCommas(project.postdated_checks)))])])
  }))]), _vm._v(" "), _c('pagination', {
    staticStyle: {
      "display": "none !important"
    },
    attrs: {
      "pagination": _vm.pagination
    },
    on: {
      "prev": function($event) {
        _vm.getProjects(_vm.pagination.prevPageUrl)
      },
      "next": function($event) {
        _vm.getProjects(_vm.pagination.nextPageUrl)
      }
    }
  })], 1)])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0139a23c", module.exports)
  }
}

/***/ }),

/***/ 417:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "ibox float-e-margins no-padding"
  }, [_c('div', {
    staticClass: "ibox-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-4 b-r"
  }, [_c('div', {
    staticClass: "ibox"
  }, [_c('table', {
    staticClass: "tbl-computation"
  }, [_c('tr', [_c('td', [_vm._v("Initial Cash:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.initial_cash)))])])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("Initial Bank:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.initial_bank)))])])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("Cash Balance:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.cash_balance)))])])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("Bank Balance:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.bank_balance)))])])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("Total:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.total_balance)))])])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("Profit:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.total_profit)))])])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("Process Output:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.process_output)))])])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("How much we received from client:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.we_receive_before)))])])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("How much we received after we spent for client:")]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "text-muted"
  }, [_vm._v(_vm._s(_vm.numberWithCommas(_vm.we_receive_after)))])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-8"
  }, [_c('div', {
    staticClass: "inputForm",
    attrs: {
      "method": "POST",
      "role": "form",
      "id": "inputForm"
    }
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Transaction Type")]), _vm._v(" "), _c('select', {
    staticClass: "form-control m-b",
    attrs: {
      "id": "transaction_type"
    },
    on: {
      "change": function($event) {
        _vm.checkTrans($event)
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "",
      "disabled": "",
      "selected": ""
    }
  }, [_vm._v("Please Select")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "pbr"
    }
  }, [_vm._v("Processing Budget Return")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "pc"
    }
  }, [_vm._v("Processing Cost")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "bpc"
    }
  }, [_vm._v("Borrowed Processing Cost")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "abr"
    }
  }, [_vm._v("Admin Budget Return")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "ac"
    }
  }, [_vm._v("Admin Cost")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "bac"
    }
  }, [_vm._v("Borrowed Admin Cost")])])]), _vm._v(" "), (_vm.borrowed.length > 0) ? _c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Borrowed List")]), _vm._v(" "), _c('select', {
    staticClass: "form-control m-b",
    attrs: {
      "id": "borrowed_list"
    }
  }, _vm._l((_vm.borrowed), function(b) {
    return _c('option', {
      key: b.id,
      domProps: {
        "value": b.id
      }
    }, [_vm._v(_vm._s(b.trans_desc))])
  }))]) : _vm._e(), _vm._v(" "), (_vm.cat_type == 'bpc' || _vm.cat_type == 'bac') ? _c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Storage Type")]), _vm._v(" "), _vm._m(0)]) : _vm._e(), _vm._v(" "), (_vm.cat_type != '') ? _c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Amount")]), _vm._v(" "), _c('input', {
    staticClass: "form-control",
    attrs: {
      "id": "txtAmount",
      "type": "text",
      "placeholder": "0.00"
    }
  })]) : _vm._e(), _vm._v(" "), (_vm.cat_type == 'bpc' || _vm.cat_type == 'bac') ? _c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Description")]), _vm._v(" "), _c('textarea', {
    staticClass: "form-control",
    attrs: {
      "id": "txtDescription",
      "row": "5"
    }
  })]) : _vm._e(), _vm._v(" "), _c('div', [(_vm.cat_type != '') ? _c('button', {
    staticClass: "btn btn-sm btn-primary pull-right m-t-n-xs",
    on: {
      "click": function($event) {
        _vm.saveProject()
      }
    }
  }, [_c('strong', [_vm._v("Save")])]) : _vm._e()])]), _vm._v(" "), _c('h2', {
    staticClass: "processLocator",
    attrs: {
      "id": "processLocator"
    }
  })])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('select', {
    staticClass: "form-control m-b",
    attrs: {
      "id": "storage_type"
    }
  }, [_c('option', {
    attrs: {
      "value": "Cash"
    }
  }, [_vm._v("Cash")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "Bank"
    }
  }, [_vm._v("Bank")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-1629965f", module.exports)
  }
}

/***/ }),

/***/ 421:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "wrapper22"
  }, [_c('div', {
    staticClass: "div22"
  }, [_c('table', {
    staticClass: "table table-hover table-striped table-bordered",
    attrs: {
      "id": "ftable"
    }
  }, [_c('thead', [_c('tr', _vm._l((_vm.columns), function(column) {
    return _c('th', {
      key: column.name
    }, [_vm._v("\n                        " + _vm._s(column.label) + "\n                    ")])
  }))]), _vm._v(" "), _vm._t("default")], 2)])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrapper11"
  }, [_c('div', {
    staticClass: "div11"
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-246b700c", module.exports)
  }
}

/***/ }),

/***/ 436:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(!_vm.client) ? _c('nav', {
    staticClass: "pagination"
  }, [_c('span', {
    staticClass: "page-stats"
  }, [_vm._v(_vm._s(_vm.pagination.from) + " - " + _vm._s(_vm.pagination.to) + " of " + _vm._s(_vm.pagination.total))]), _vm._v(" "), (_vm.pagination.prevPageUrl) ? _c('a', {
    staticClass: "btn btn-default pagination-previous",
    on: {
      "click": function($event) {
        _vm.$emit('prev');
      }
    }
  }, [_vm._v("\n          Prev\n      ")]) : _c('a', {
    staticClass: "btn btn-default pagination-previous",
    attrs: {
      "disabled": true
    }
  }, [_vm._v("\n         Prev\n      ")]), _vm._v(" "), (_vm.pagination.nextPageUrl) ? _c('a', {
    staticClass: "btn btn-default pagination-next",
    on: {
      "click": function($event) {
        _vm.$emit('next');
      }
    }
  }, [_vm._v("\n          Next\n      ")]) : _c('a', {
    staticClass: "btn btn-default pagination-next",
    attrs: {
      "disabled": true
    }
  }, [_vm._v("\n          Next\n      ")])]) : _c('nav', {
    staticClass: "pagination"
  }, [_c('span', {
    staticClass: "page-stats"
  }, [_vm._v("\n          " + _vm._s(_vm.pagination.from) + " - " + _vm._s(_vm.pagination.to) + " of " + _vm._s(_vm.filtered.length) + "\n          "), (_vm.filtered.length < _vm.pagination.total) ? _c('span', [_vm._v("(filtered from " + _vm._s(_vm.pagination.total) + " total entries)")]) : _vm._e()]), _vm._v(" "), (_vm.pagination.prevPage) ? _c('a', {
    staticClass: "btn btn-default pagination-previous",
    on: {
      "click": function($event) {
        _vm.$emit('prev');
      }
    }
  }, [_vm._v("\n          Prev\n      ")]) : _c('a', {
    staticClass: "btn btn-default pagination-previous",
    attrs: {
      "disabled": true
    }
  }, [_vm._v("\n         Prev\n      ")]), _vm._v(" "), (_vm.pagination.nextPage) ? _c('a', {
    staticClass: "btn btn-default pagination-next",
    on: {
      "click": function($event) {
        _vm.$emit('next');
      }
    }
  }, [_vm._v("\n          Next\n      ")]) : _c('a', {
    staticClass: "btn btn-default pagination-next",
    attrs: {
      "disabled": true
    }
  }, [_vm._v("\n          Next\n      ")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-54d46bfc", module.exports)
  }
}

/***/ }),

/***/ 529:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(206);


/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Url", function() { return Url; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Http", function() { return Http; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Resource", function() { return Resource; });
/*!
 * vue-resource v1.3.4
 * https://github.com/pagekit/vue-resource
 * Released under the MIT License.
 */

/**
 * Promises/A+ polyfill v1.1.4 (https://github.com/bramstein/promis)
 */

var RESOLVED = 0;
var REJECTED = 1;
var PENDING  = 2;

function Promise$1(executor) {

    this.state = PENDING;
    this.value = undefined;
    this.deferred = [];

    var promise = this;

    try {
        executor(function (x) {
            promise.resolve(x);
        }, function (r) {
            promise.reject(r);
        });
    } catch (e) {
        promise.reject(e);
    }
}

Promise$1.reject = function (r) {
    return new Promise$1(function (resolve, reject) {
        reject(r);
    });
};

Promise$1.resolve = function (x) {
    return new Promise$1(function (resolve, reject) {
        resolve(x);
    });
};

Promise$1.all = function all(iterable) {
    return new Promise$1(function (resolve, reject) {
        var count = 0, result = [];

        if (iterable.length === 0) {
            resolve(result);
        }

        function resolver(i) {
            return function (x) {
                result[i] = x;
                count += 1;

                if (count === iterable.length) {
                    resolve(result);
                }
            };
        }

        for (var i = 0; i < iterable.length; i += 1) {
            Promise$1.resolve(iterable[i]).then(resolver(i), reject);
        }
    });
};

Promise$1.race = function race(iterable) {
    return new Promise$1(function (resolve, reject) {
        for (var i = 0; i < iterable.length; i += 1) {
            Promise$1.resolve(iterable[i]).then(resolve, reject);
        }
    });
};

var p$1 = Promise$1.prototype;

p$1.resolve = function resolve(x) {
    var promise = this;

    if (promise.state === PENDING) {
        if (x === promise) {
            throw new TypeError('Promise settled with itself.');
        }

        var called = false;

        try {
            var then = x && x['then'];

            if (x !== null && typeof x === 'object' && typeof then === 'function') {
                then.call(x, function (x) {
                    if (!called) {
                        promise.resolve(x);
                    }
                    called = true;

                }, function (r) {
                    if (!called) {
                        promise.reject(r);
                    }
                    called = true;
                });
                return;
            }
        } catch (e) {
            if (!called) {
                promise.reject(e);
            }
            return;
        }

        promise.state = RESOLVED;
        promise.value = x;
        promise.notify();
    }
};

p$1.reject = function reject(reason) {
    var promise = this;

    if (promise.state === PENDING) {
        if (reason === promise) {
            throw new TypeError('Promise settled with itself.');
        }

        promise.state = REJECTED;
        promise.value = reason;
        promise.notify();
    }
};

p$1.notify = function notify() {
    var promise = this;

    nextTick(function () {
        if (promise.state !== PENDING) {
            while (promise.deferred.length) {
                var deferred = promise.deferred.shift(),
                    onResolved = deferred[0],
                    onRejected = deferred[1],
                    resolve = deferred[2],
                    reject = deferred[3];

                try {
                    if (promise.state === RESOLVED) {
                        if (typeof onResolved === 'function') {
                            resolve(onResolved.call(undefined, promise.value));
                        } else {
                            resolve(promise.value);
                        }
                    } else if (promise.state === REJECTED) {
                        if (typeof onRejected === 'function') {
                            resolve(onRejected.call(undefined, promise.value));
                        } else {
                            reject(promise.value);
                        }
                    }
                } catch (e) {
                    reject(e);
                }
            }
        }
    });
};

p$1.then = function then(onResolved, onRejected) {
    var promise = this;

    return new Promise$1(function (resolve, reject) {
        promise.deferred.push([onResolved, onRejected, resolve, reject]);
        promise.notify();
    });
};

p$1.catch = function (onRejected) {
    return this.then(undefined, onRejected);
};

/**
 * Promise adapter.
 */

if (typeof Promise === 'undefined') {
    window.Promise = Promise$1;
}

function PromiseObj(executor, context) {

    if (executor instanceof Promise) {
        this.promise = executor;
    } else {
        this.promise = new Promise(executor.bind(context));
    }

    this.context = context;
}

PromiseObj.all = function (iterable, context) {
    return new PromiseObj(Promise.all(iterable), context);
};

PromiseObj.resolve = function (value, context) {
    return new PromiseObj(Promise.resolve(value), context);
};

PromiseObj.reject = function (reason, context) {
    return new PromiseObj(Promise.reject(reason), context);
};

PromiseObj.race = function (iterable, context) {
    return new PromiseObj(Promise.race(iterable), context);
};

var p = PromiseObj.prototype;

p.bind = function (context) {
    this.context = context;
    return this;
};

p.then = function (fulfilled, rejected) {

    if (fulfilled && fulfilled.bind && this.context) {
        fulfilled = fulfilled.bind(this.context);
    }

    if (rejected && rejected.bind && this.context) {
        rejected = rejected.bind(this.context);
    }

    return new PromiseObj(this.promise.then(fulfilled, rejected), this.context);
};

p.catch = function (rejected) {

    if (rejected && rejected.bind && this.context) {
        rejected = rejected.bind(this.context);
    }

    return new PromiseObj(this.promise.catch(rejected), this.context);
};

p.finally = function (callback) {

    return this.then(function (value) {
            callback.call(this);
            return value;
        }, function (reason) {
            callback.call(this);
            return Promise.reject(reason);
        }
    );
};

/**
 * Utility functions.
 */

var ref = {};
var hasOwnProperty = ref.hasOwnProperty;

var ref$1 = [];
var slice = ref$1.slice;
var debug = false;
var ntick;

var inBrowser = typeof window !== 'undefined';

var Util = function (ref) {
    var config = ref.config;
    var nextTick = ref.nextTick;

    ntick = nextTick;
    debug = config.debug || !config.silent;
};

function warn(msg) {
    if (typeof console !== 'undefined' && debug) {
        console.warn('[VueResource warn]: ' + msg);
    }
}

function error(msg) {
    if (typeof console !== 'undefined') {
        console.error(msg);
    }
}

function nextTick(cb, ctx) {
    return ntick(cb, ctx);
}

function trim(str) {
    return str ? str.replace(/^\s*|\s*$/g, '') : '';
}

function trimEnd(str, chars) {

    if (str && chars === undefined) {
        return str.replace(/\s+$/, '');
    }

    if (!str || !chars) {
        return str;
    }

    return str.replace(new RegExp(("[" + chars + "]+$")), '');
}

function toLower(str) {
    return str ? str.toLowerCase() : '';
}

function toUpper(str) {
    return str ? str.toUpperCase() : '';
}

var isArray = Array.isArray;

function isString(val) {
    return typeof val === 'string';
}



function isFunction(val) {
    return typeof val === 'function';
}

function isObject(obj) {
    return obj !== null && typeof obj === 'object';
}

function isPlainObject(obj) {
    return isObject(obj) && Object.getPrototypeOf(obj) == Object.prototype;
}

function isBlob(obj) {
    return typeof Blob !== 'undefined' && obj instanceof Blob;
}

function isFormData(obj) {
    return typeof FormData !== 'undefined' && obj instanceof FormData;
}

function when(value, fulfilled, rejected) {

    var promise = PromiseObj.resolve(value);

    if (arguments.length < 2) {
        return promise;
    }

    return promise.then(fulfilled, rejected);
}

function options(fn, obj, opts) {

    opts = opts || {};

    if (isFunction(opts)) {
        opts = opts.call(obj);
    }

    return merge(fn.bind({$vm: obj, $options: opts}), fn, {$options: opts});
}

function each(obj, iterator) {

    var i, key;

    if (isArray(obj)) {
        for (i = 0; i < obj.length; i++) {
            iterator.call(obj[i], obj[i], i);
        }
    } else if (isObject(obj)) {
        for (key in obj) {
            if (hasOwnProperty.call(obj, key)) {
                iterator.call(obj[key], obj[key], key);
            }
        }
    }

    return obj;
}

var assign = Object.assign || _assign;

function merge(target) {

    var args = slice.call(arguments, 1);

    args.forEach(function (source) {
        _merge(target, source, true);
    });

    return target;
}

function defaults(target) {

    var args = slice.call(arguments, 1);

    args.forEach(function (source) {

        for (var key in source) {
            if (target[key] === undefined) {
                target[key] = source[key];
            }
        }

    });

    return target;
}

function _assign(target) {

    var args = slice.call(arguments, 1);

    args.forEach(function (source) {
        _merge(target, source);
    });

    return target;
}

function _merge(target, source, deep) {
    for (var key in source) {
        if (deep && (isPlainObject(source[key]) || isArray(source[key]))) {
            if (isPlainObject(source[key]) && !isPlainObject(target[key])) {
                target[key] = {};
            }
            if (isArray(source[key]) && !isArray(target[key])) {
                target[key] = [];
            }
            _merge(target[key], source[key], deep);
        } else if (source[key] !== undefined) {
            target[key] = source[key];
        }
    }
}

/**
 * Root Prefix Transform.
 */

var root = function (options$$1, next) {

    var url = next(options$$1);

    if (isString(options$$1.root) && !/^(https?:)?\//.test(url)) {
        url = trimEnd(options$$1.root, '/') + '/' + url;
    }

    return url;
};

/**
 * Query Parameter Transform.
 */

var query = function (options$$1, next) {

    var urlParams = Object.keys(Url.options.params), query = {}, url = next(options$$1);

    each(options$$1.params, function (value, key) {
        if (urlParams.indexOf(key) === -1) {
            query[key] = value;
        }
    });

    query = Url.params(query);

    if (query) {
        url += (url.indexOf('?') == -1 ? '?' : '&') + query;
    }

    return url;
};

/**
 * URL Template v2.0.6 (https://github.com/bramstein/url-template)
 */

function expand(url, params, variables) {

    var tmpl = parse(url), expanded = tmpl.expand(params);

    if (variables) {
        variables.push.apply(variables, tmpl.vars);
    }

    return expanded;
}

function parse(template) {

    var operators = ['+', '#', '.', '/', ';', '?', '&'], variables = [];

    return {
        vars: variables,
        expand: function expand(context) {
            return template.replace(/\{([^\{\}]+)\}|([^\{\}]+)/g, function (_, expression, literal) {
                if (expression) {

                    var operator = null, values = [];

                    if (operators.indexOf(expression.charAt(0)) !== -1) {
                        operator = expression.charAt(0);
                        expression = expression.substr(1);
                    }

                    expression.split(/,/g).forEach(function (variable) {
                        var tmp = /([^:\*]*)(?::(\d+)|(\*))?/.exec(variable);
                        values.push.apply(values, getValues(context, operator, tmp[1], tmp[2] || tmp[3]));
                        variables.push(tmp[1]);
                    });

                    if (operator && operator !== '+') {

                        var separator = ',';

                        if (operator === '?') {
                            separator = '&';
                        } else if (operator !== '#') {
                            separator = operator;
                        }

                        return (values.length !== 0 ? operator : '') + values.join(separator);
                    } else {
                        return values.join(',');
                    }

                } else {
                    return encodeReserved(literal);
                }
            });
        }
    };
}

function getValues(context, operator, key, modifier) {

    var value = context[key], result = [];

    if (isDefined(value) && value !== '') {
        if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
            value = value.toString();

            if (modifier && modifier !== '*') {
                value = value.substring(0, parseInt(modifier, 10));
            }

            result.push(encodeValue(operator, value, isKeyOperator(operator) ? key : null));
        } else {
            if (modifier === '*') {
                if (Array.isArray(value)) {
                    value.filter(isDefined).forEach(function (value) {
                        result.push(encodeValue(operator, value, isKeyOperator(operator) ? key : null));
                    });
                } else {
                    Object.keys(value).forEach(function (k) {
                        if (isDefined(value[k])) {
                            result.push(encodeValue(operator, value[k], k));
                        }
                    });
                }
            } else {
                var tmp = [];

                if (Array.isArray(value)) {
                    value.filter(isDefined).forEach(function (value) {
                        tmp.push(encodeValue(operator, value));
                    });
                } else {
                    Object.keys(value).forEach(function (k) {
                        if (isDefined(value[k])) {
                            tmp.push(encodeURIComponent(k));
                            tmp.push(encodeValue(operator, value[k].toString()));
                        }
                    });
                }

                if (isKeyOperator(operator)) {
                    result.push(encodeURIComponent(key) + '=' + tmp.join(','));
                } else if (tmp.length !== 0) {
                    result.push(tmp.join(','));
                }
            }
        }
    } else {
        if (operator === ';') {
            result.push(encodeURIComponent(key));
        } else if (value === '' && (operator === '&' || operator === '?')) {
            result.push(encodeURIComponent(key) + '=');
        } else if (value === '') {
            result.push('');
        }
    }

    return result;
}

function isDefined(value) {
    return value !== undefined && value !== null;
}

function isKeyOperator(operator) {
    return operator === ';' || operator === '&' || operator === '?';
}

function encodeValue(operator, value, key) {

    value = (operator === '+' || operator === '#') ? encodeReserved(value) : encodeURIComponent(value);

    if (key) {
        return encodeURIComponent(key) + '=' + value;
    } else {
        return value;
    }
}

function encodeReserved(str) {
    return str.split(/(%[0-9A-Fa-f]{2})/g).map(function (part) {
        if (!/%[0-9A-Fa-f]/.test(part)) {
            part = encodeURI(part);
        }
        return part;
    }).join('');
}

/**
 * URL Template (RFC 6570) Transform.
 */

var template = function (options) {

    var variables = [], url = expand(options.url, options.params, variables);

    variables.forEach(function (key) {
        delete options.params[key];
    });

    return url;
};

/**
 * Service for URL templating.
 */

function Url(url, params) {

    var self = this || {}, options$$1 = url, transform;

    if (isString(url)) {
        options$$1 = {url: url, params: params};
    }

    options$$1 = merge({}, Url.options, self.$options, options$$1);

    Url.transforms.forEach(function (handler) {

        if (isString(handler)) {
            handler = Url.transform[handler];
        }

        if (isFunction(handler)) {
            transform = factory(handler, transform, self.$vm);
        }

    });

    return transform(options$$1);
}

/**
 * Url options.
 */

Url.options = {
    url: '',
    root: null,
    params: {}
};

/**
 * Url transforms.
 */

Url.transform = {template: template, query: query, root: root};
Url.transforms = ['template', 'query', 'root'];

/**
 * Encodes a Url parameter string.
 *
 * @param {Object} obj
 */

Url.params = function (obj) {

    var params = [], escape = encodeURIComponent;

    params.add = function (key, value) {

        if (isFunction(value)) {
            value = value();
        }

        if (value === null) {
            value = '';
        }

        this.push(escape(key) + '=' + escape(value));
    };

    serialize(params, obj);

    return params.join('&').replace(/%20/g, '+');
};

/**
 * Parse a URL and return its components.
 *
 * @param {String} url
 */

Url.parse = function (url) {

    var el = document.createElement('a');

    if (document.documentMode) {
        el.href = url;
        url = el.href;
    }

    el.href = url;

    return {
        href: el.href,
        protocol: el.protocol ? el.protocol.replace(/:$/, '') : '',
        port: el.port,
        host: el.host,
        hostname: el.hostname,
        pathname: el.pathname.charAt(0) === '/' ? el.pathname : '/' + el.pathname,
        search: el.search ? el.search.replace(/^\?/, '') : '',
        hash: el.hash ? el.hash.replace(/^#/, '') : ''
    };
};

function factory(handler, next, vm) {
    return function (options$$1) {
        return handler.call(vm, options$$1, next);
    };
}

function serialize(params, obj, scope) {

    var array = isArray(obj), plain = isPlainObject(obj), hash;

    each(obj, function (value, key) {

        hash = isObject(value) || isArray(value);

        if (scope) {
            key = scope + '[' + (plain || hash ? key : '') + ']';
        }

        if (!scope && array) {
            params.add(value.name, value.value);
        } else if (hash) {
            serialize(params, value, key);
        } else {
            params.add(key, value);
        }
    });
}

/**
 * XDomain client (Internet Explorer).
 */

var xdrClient = function (request) {
    return new PromiseObj(function (resolve) {

        var xdr = new XDomainRequest(), handler = function (ref) {
            var type = ref.type;


            var status = 0;

            if (type === 'load') {
                status = 200;
            } else if (type === 'error') {
                status = 500;
            }

            resolve(request.respondWith(xdr.responseText, {status: status}));
        };

        request.abort = function () { return xdr.abort(); };

        xdr.open(request.method, request.getUrl());

        if (request.timeout) {
            xdr.timeout = request.timeout;
        }

        xdr.onload = handler;
        xdr.onabort = handler;
        xdr.onerror = handler;
        xdr.ontimeout = handler;
        xdr.onprogress = function () {};
        xdr.send(request.getBody());
    });
};

/**
 * CORS Interceptor.
 */

var SUPPORTS_CORS = inBrowser && 'withCredentials' in new XMLHttpRequest();

var cors = function (request, next) {

    if (inBrowser) {

        var orgUrl = Url.parse(location.href);
        var reqUrl = Url.parse(request.getUrl());

        if (reqUrl.protocol !== orgUrl.protocol || reqUrl.host !== orgUrl.host) {

            request.crossOrigin = true;
            request.emulateHTTP = false;

            if (!SUPPORTS_CORS) {
                request.client = xdrClient;
            }
        }
    }

    next();
};

/**
 * Form data Interceptor.
 */

var form = function (request, next) {

    if (isFormData(request.body)) {

        request.headers.delete('Content-Type');

    } else if (isObject(request.body) && request.emulateJSON) {

        request.body = Url.params(request.body);
        request.headers.set('Content-Type', 'application/x-www-form-urlencoded');
    }

    next();
};

/**
 * JSON Interceptor.
 */

var json = function (request, next) {

    var type = request.headers.get('Content-Type') || '';

    if (isObject(request.body) && type.indexOf('application/json') === 0) {
        request.body = JSON.stringify(request.body);
    }

    next(function (response) {

        return response.bodyText ? when(response.text(), function (text) {

            type = response.headers.get('Content-Type') || '';

            if (type.indexOf('application/json') === 0 || isJson(text)) {

                try {
                    response.body = JSON.parse(text);
                } catch (e) {
                    response.body = null;
                }

            } else {
                response.body = text;
            }

            return response;

        }) : response;

    });
};

function isJson(str) {

    var start = str.match(/^\[|^\{(?!\{)/), end = {'[': /]$/, '{': /}$/};

    return start && end[start[0]].test(str);
}

/**
 * JSONP client (Browser).
 */

var jsonpClient = function (request) {
    return new PromiseObj(function (resolve) {

        var name = request.jsonp || 'callback', callback = request.jsonpCallback || '_jsonp' + Math.random().toString(36).substr(2), body = null, handler, script;

        handler = function (ref) {
            var type = ref.type;


            var status = 0;

            if (type === 'load' && body !== null) {
                status = 200;
            } else if (type === 'error') {
                status = 500;
            }

            if (status && window[callback]) {
                delete window[callback];
                document.body.removeChild(script);
            }

            resolve(request.respondWith(body, {status: status}));
        };

        window[callback] = function (result) {
            body = JSON.stringify(result);
        };

        request.abort = function () {
            handler({type: 'abort'});
        };

        request.params[name] = callback;

        if (request.timeout) {
            setTimeout(request.abort, request.timeout);
        }

        script = document.createElement('script');
        script.src = request.getUrl();
        script.type = 'text/javascript';
        script.async = true;
        script.onload = handler;
        script.onerror = handler;

        document.body.appendChild(script);
    });
};

/**
 * JSONP Interceptor.
 */

var jsonp = function (request, next) {

    if (request.method == 'JSONP') {
        request.client = jsonpClient;
    }

    next();
};

/**
 * Before Interceptor.
 */

var before = function (request, next) {

    if (isFunction(request.before)) {
        request.before.call(this, request);
    }

    next();
};

/**
 * HTTP method override Interceptor.
 */

var method = function (request, next) {

    if (request.emulateHTTP && /^(PUT|PATCH|DELETE)$/i.test(request.method)) {
        request.headers.set('X-HTTP-Method-Override', request.method);
        request.method = 'POST';
    }

    next();
};

/**
 * Header Interceptor.
 */

var header = function (request, next) {

    var headers = assign({}, Http.headers.common,
        !request.crossOrigin ? Http.headers.custom : {},
        Http.headers[toLower(request.method)]
    );

    each(headers, function (value, name) {
        if (!request.headers.has(name)) {
            request.headers.set(name, value);
        }
    });

    next();
};

/**
 * XMLHttp client (Browser).
 */

var xhrClient = function (request) {
    return new PromiseObj(function (resolve) {

        var xhr = new XMLHttpRequest(), handler = function (event) {

            var response = request.respondWith(
                'response' in xhr ? xhr.response : xhr.responseText, {
                    status: xhr.status === 1223 ? 204 : xhr.status, // IE9 status bug
                    statusText: xhr.status === 1223 ? 'No Content' : trim(xhr.statusText)
                }
            );

            each(trim(xhr.getAllResponseHeaders()).split('\n'), function (row) {
                response.headers.append(row.slice(0, row.indexOf(':')), row.slice(row.indexOf(':') + 1));
            });

            resolve(response);
        };

        request.abort = function () { return xhr.abort(); };

        if (request.progress) {
            if (request.method === 'GET') {
                xhr.addEventListener('progress', request.progress);
            } else if (/^(POST|PUT)$/i.test(request.method)) {
                xhr.upload.addEventListener('progress', request.progress);
            }
        }

        xhr.open(request.method, request.getUrl(), true);

        if (request.timeout) {
            xhr.timeout = request.timeout;
        }

        if (request.responseType && 'responseType' in xhr) {
            xhr.responseType = request.responseType;
        }

        if (request.withCredentials || request.credentials) {
            xhr.withCredentials = true;
        }

        if (!request.crossOrigin) {
            request.headers.set('X-Requested-With', 'XMLHttpRequest');
        }

        request.headers.forEach(function (value, name) {
            xhr.setRequestHeader(name, value);
        });

        xhr.onload = handler;
        xhr.onabort = handler;
        xhr.onerror = handler;
        xhr.ontimeout = handler;
        xhr.send(request.getBody());
    });
};

/**
 * Http client (Node).
 */

var nodeClient = function (request) {

    var client = __webpack_require__(10);

    return new PromiseObj(function (resolve) {

        var url = request.getUrl();
        var body = request.getBody();
        var method = request.method;
        var headers = {}, handler;

        request.headers.forEach(function (value, name) {
            headers[name] = value;
        });

        client(url, {body: body, method: method, headers: headers}).then(handler = function (resp) {

            var response = request.respondWith(resp.body, {
                    status: resp.statusCode,
                    statusText: trim(resp.statusMessage)
                }
            );

            each(resp.headers, function (value, name) {
                response.headers.set(name, value);
            });

            resolve(response);

        }, function (error$$1) { return handler(error$$1.response); });
    });
};

/**
 * Base client.
 */

var Client = function (context) {

    var reqHandlers = [sendRequest], resHandlers = [], handler;

    if (!isObject(context)) {
        context = null;
    }

    function Client(request) {
        return new PromiseObj(function (resolve, reject) {

            function exec() {

                handler = reqHandlers.pop();

                if (isFunction(handler)) {
                    handler.call(context, request, next);
                } else {
                    warn(("Invalid interceptor of type " + (typeof handler) + ", must be a function"));
                    next();
                }
            }

            function next(response) {

                if (isFunction(response)) {

                    resHandlers.unshift(response);

                } else if (isObject(response)) {

                    resHandlers.forEach(function (handler) {
                        response = when(response, function (response) {
                            return handler.call(context, response) || response;
                        }, reject);
                    });

                    when(response, resolve, reject);

                    return;
                }

                exec();
            }

            exec();

        }, context);
    }

    Client.use = function (handler) {
        reqHandlers.push(handler);
    };

    return Client;
};

function sendRequest(request, resolve) {

    var client = request.client || (inBrowser ? xhrClient : nodeClient);

    resolve(client(request));
}

/**
 * HTTP Headers.
 */

var Headers = function Headers(headers) {
    var this$1 = this;


    this.map = {};

    each(headers, function (value, name) { return this$1.append(name, value); });
};

Headers.prototype.has = function has (name) {
    return getName(this.map, name) !== null;
};

Headers.prototype.get = function get (name) {

    var list = this.map[getName(this.map, name)];

    return list ? list.join() : null;
};

Headers.prototype.getAll = function getAll (name) {
    return this.map[getName(this.map, name)] || [];
};

Headers.prototype.set = function set (name, value) {
    this.map[normalizeName(getName(this.map, name) || name)] = [trim(value)];
};

Headers.prototype.append = function append (name, value){

    var list = this.map[getName(this.map, name)];

    if (list) {
        list.push(trim(value));
    } else {
        this.set(name, value);
    }
};

Headers.prototype.delete = function delete$1 (name){
    delete this.map[getName(this.map, name)];
};

Headers.prototype.deleteAll = function deleteAll (){
    this.map = {};
};

Headers.prototype.forEach = function forEach (callback, thisArg) {
        var this$1 = this;

    each(this.map, function (list, name) {
        each(list, function (value) { return callback.call(thisArg, value, name, this$1); });
    });
};

function getName(map, name) {
    return Object.keys(map).reduce(function (prev, curr) {
        return toLower(name) === toLower(curr) ? curr : prev;
    }, null);
}

function normalizeName(name) {

    if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
        throw new TypeError('Invalid character in header field name');
    }

    return trim(name);
}

/**
 * HTTP Response.
 */

var Response = function Response(body, ref) {
    var url = ref.url;
    var headers = ref.headers;
    var status = ref.status;
    var statusText = ref.statusText;


    this.url = url;
    this.ok = status >= 200 && status < 300;
    this.status = status || 0;
    this.statusText = statusText || '';
    this.headers = new Headers(headers);
    this.body = body;

    if (isString(body)) {

        this.bodyText = body;

    } else if (isBlob(body)) {

        this.bodyBlob = body;

        if (isBlobText(body)) {
            this.bodyText = blobText(body);
        }
    }
};

Response.prototype.blob = function blob () {
    return when(this.bodyBlob);
};

Response.prototype.text = function text () {
    return when(this.bodyText);
};

Response.prototype.json = function json () {
    return when(this.text(), function (text) { return JSON.parse(text); });
};

Object.defineProperty(Response.prototype, 'data', {

    get: function get() {
        return this.body;
    },

    set: function set(body) {
        this.body = body;
    }

});

function blobText(body) {
    return new PromiseObj(function (resolve) {

        var reader = new FileReader();

        reader.readAsText(body);
        reader.onload = function () {
            resolve(reader.result);
        };

    });
}

function isBlobText(body) {
    return body.type.indexOf('text') === 0 || body.type.indexOf('json') !== -1;
}

/**
 * HTTP Request.
 */

var Request = function Request(options$$1) {

    this.body = null;
    this.params = {};

    assign(this, options$$1, {
        method: toUpper(options$$1.method || 'GET')
    });

    if (!(this.headers instanceof Headers)) {
        this.headers = new Headers(this.headers);
    }
};

Request.prototype.getUrl = function getUrl (){
    return Url(this);
};

Request.prototype.getBody = function getBody (){
    return this.body;
};

Request.prototype.respondWith = function respondWith (body, options$$1) {
    return new Response(body, assign(options$$1 || {}, {url: this.getUrl()}));
};

/**
 * Service for sending network requests.
 */

var COMMON_HEADERS = {'Accept': 'application/json, text/plain, */*'};
var JSON_CONTENT_TYPE = {'Content-Type': 'application/json;charset=utf-8'};

function Http(options$$1) {

    var self = this || {}, client = Client(self.$vm);

    defaults(options$$1 || {}, self.$options, Http.options);

    Http.interceptors.forEach(function (handler) {

        if (isString(handler)) {
            handler = Http.interceptor[handler];
        }

        if (isFunction(handler)) {
            client.use(handler);
        }

    });

    return client(new Request(options$$1)).then(function (response) {

        return response.ok ? response : PromiseObj.reject(response);

    }, function (response) {

        if (response instanceof Error) {
            error(response);
        }

        return PromiseObj.reject(response);
    });
}

Http.options = {};

Http.headers = {
    put: JSON_CONTENT_TYPE,
    post: JSON_CONTENT_TYPE,
    patch: JSON_CONTENT_TYPE,
    delete: JSON_CONTENT_TYPE,
    common: COMMON_HEADERS,
    custom: {}
};

Http.interceptor = {before: before, method: method, jsonp: jsonp, json: json, form: form, header: header, cors: cors};
Http.interceptors = ['before', 'method', 'jsonp', 'json', 'form', 'header', 'cors'];

['get', 'delete', 'head', 'jsonp'].forEach(function (method$$1) {

    Http[method$$1] = function (url, options$$1) {
        return this(assign(options$$1 || {}, {url: url, method: method$$1}));
    };

});

['post', 'put', 'patch'].forEach(function (method$$1) {

    Http[method$$1] = function (url, body, options$$1) {
        return this(assign(options$$1 || {}, {url: url, method: method$$1, body: body}));
    };

});

/**
 * Service for interacting with RESTful services.
 */

function Resource(url, params, actions, options$$1) {

    var self = this || {}, resource = {};

    actions = assign({},
        Resource.actions,
        actions
    );

    each(actions, function (action, name) {

        action = merge({url: url, params: assign({}, params)}, options$$1, action);

        resource[name] = function () {
            return (self.$http || Http)(opts(action, arguments));
        };
    });

    return resource;
}

function opts(action, args) {

    var options$$1 = assign({}, action), params = {}, body;

    switch (args.length) {

        case 2:

            params = args[0];
            body = args[1];

            break;

        case 1:

            if (/^(POST|PUT|PATCH)$/i.test(options$$1.method)) {
                body = args[0];
            } else {
                params = args[0];
            }

            break;

        case 0:

            break;

        default:

            throw 'Expected up to 2 arguments [params, body], got ' + args.length + ' arguments';
    }

    options$$1.body = body;
    options$$1.params = assign({}, options$$1.params, params);

    return options$$1;
}

Resource.actions = {

    get: {method: 'GET'},
    save: {method: 'POST'},
    query: {method: 'GET'},
    update: {method: 'PUT'},
    remove: {method: 'DELETE'},
    delete: {method: 'DELETE'}

};

/**
 * Install plugin.
 */

function plugin(Vue) {

    if (plugin.installed) {
        return;
    }

    Util(Vue);

    Vue.url = Url;
    Vue.http = Http;
    Vue.resource = Resource;
    Vue.Promise = PromiseObj;

    Object.defineProperties(Vue.prototype, {

        $url: {
            get: function get() {
                return options(Vue.url, this, this.$options.url);
            }
        },

        $http: {
            get: function get() {
                return options(Vue.http, this, this.$options.http);
            }
        },

        $resource: {
            get: function get() {
                return Vue.resource.bind(this);
            }
        },

        $promise: {
            get: function get() {
                var this$1 = this;

                return function (executor) { return new Vue.Promise(executor, this$1); };
            }
        }

    });
}

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(plugin);
}

/* harmony default export */ __webpack_exports__["default"] = (plugin);



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZDQyYmFiYzA1MDNjZWJkNjk4OTQiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly8vZ290IChpZ25vcmVkKSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL25maW5hbmNpbmcvZmluYW5jaW5nLmpzIiwid2VicGFjazovLy9EYXRhVGFibGUudnVlIiwid2VicGFjazovLy9GaW5hbmNpbmcudnVlIiwid2VicGFjazovLy9QYWdpbmF0aW9uLnZ1ZSIsIndlYnBhY2s6Ly8vUHJvamVjdHMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvRGF0YVRhYmxlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvTmV3RmluYW5jaW5nL0ZpbmFuY2luZy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL05ld0ZpbmFuY2luZy9QYWdpbmF0aW9uLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvTmV3RmluYW5jaW5nL1Byb2plY3RzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvTmV3RmluYW5jaW5nL1Byb2plY3RzLnZ1ZT9mMTlmIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvRmluYW5jaW5nLnZ1ZT9iYTczIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvRGF0YVRhYmxlLnZ1ZT85NGIyIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvUGFnaW5hdGlvbi52dWU/ZmQ2ZSIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1yZXNvdXJjZS9kaXN0L3Z1ZS1yZXNvdXJjZS5lczIwMTUuanMiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImh0dHAiLCJpbnRlcmNlcHRvcnMiLCJwdXNoIiwicmVxdWVzdCIsIm5leHQiLCJoZWFkZXJzIiwic2V0IiwiTGFyYXZlbCIsImNzcmZUb2tlbiIsImFwcCIsImVsIiwidG9nZ2xlciIsImRvY3VtZW50IiwiZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSIsInByb2Nlc3NMb2NhdG9yIiwiZ2V0RWxlbWVudEJ5SWQiLCJpIiwic2VsIiwibGVuZ3RoIiwiYWRkRXZlbnRMaXN0ZW5lciIsInBhcmVudEVsZW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwiY2xhc3NMaXN0IiwidG9nZ2xlIiwidG9nZ2xlcjIiLCIkIiwicmVtb3ZlQ2xhc3MiLCJpbnB1dEZvcm0iLCJhZGRDbGFzcyIsInAiLCJwYXJlbnQiLCJjb25zb2xlIiwibG9nIiwiaHRtbCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBLGU7Ozs7Ozs7QUNBQUEsSUFBSUMsU0FBSixDQUFjLGlCQUFkLEVBQWlDLG1CQUFBQyxDQUFRLEdBQVIsQ0FBakM7O0FBRUFGLElBQUlHLElBQUosQ0FBU0MsWUFBVCxDQUFzQkMsSUFBdEIsQ0FBMkIsVUFBQ0MsT0FBRCxFQUFVQyxJQUFWLEVBQW1CO0FBQzFDRCxVQUFRRSxPQUFSLENBQWdCQyxHQUFoQixDQUFvQixjQUFwQixFQUFvQ0MsUUFBUUMsU0FBNUM7O0FBRUFKO0FBQ0gsQ0FKRDtBQUtBLElBQU1LLE1BQU0sSUFBSVosR0FBSixDQUFRO0FBQ2xCYSxNQUFJO0FBRGMsQ0FBUixDQUFaOztBQUlBLElBQUlDLFVBQVVDLFNBQVNDLHNCQUFULENBQWdDLFFBQWhDLENBQWQ7QUFDQSxJQUFJQyxpQkFBaUJGLFNBQVNHLGNBQVQsQ0FBd0IsZ0JBQXhCLENBQXJCO0FBQ0EsSUFBSUMsQ0FBSjtBQUNBLElBQUlDLEdBQUo7QUFDQSxLQUFLRCxJQUFJLENBQVQsRUFBWUEsSUFBSUwsUUFBUU8sTUFBeEIsRUFBZ0NGLEdBQWhDLEVBQXFDO0FBQ3JDTCxVQUFRSyxDQUFSLEVBQVdHLGdCQUFYLENBQTRCLE9BQTVCLEVBQXFDLFlBQVc7QUFDOUMsU0FBS0MsYUFBTCxDQUFtQkMsYUFBbkIsQ0FBaUMsU0FBakMsRUFBNENDLFNBQTVDLENBQXNEQyxNQUF0RCxDQUE2RCxRQUE3RDtBQUNBLFNBQUtELFNBQUwsQ0FBZUMsTUFBZixDQUFzQixhQUF0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFRCxHQVREO0FBVUM7O0FBRUQsSUFBSUMsV0FBV1osU0FBU0Msc0JBQVQsQ0FBZ0MsTUFBaEMsQ0FBZjs7QUFFQSxLQUFLRyxJQUFJLENBQVQsRUFBWUEsSUFBSVEsU0FBU04sTUFBekIsRUFBaUNGLEdBQWpDLEVBQXNDO0FBQ3BDUSxXQUFTUixDQUFULEVBQVlHLGdCQUFaLENBQTZCLE9BQTdCLEVBQXNDLFlBQVc7QUFDL0NNLE1BQUVSLEdBQUYsRUFBT1MsV0FBUCxDQUFtQixjQUFuQjtBQUNBVCxVQUFNLElBQU47QUFDQSxRQUFJVSxZQUFhZixTQUFTRyxjQUFULENBQXdCLFdBQXhCLENBQWpCO0FBQ0FVLE1BQUVFLFNBQUYsRUFBYUQsV0FBYixDQUF5QixXQUF6QjtBQUNBRCxNQUFFRSxTQUFGLEVBQWFDLFFBQWIsQ0FBc0IsWUFBdEI7QUFDQUgsTUFBRVIsR0FBRixFQUFPVyxRQUFQLENBQWdCLGNBQWhCO0FBQ0EsUUFBSUMsSUFBSSxJQUFSO0FBQ0EsV0FBTUosRUFBRUksQ0FBRixFQUFLQyxNQUFMLENBQVksU0FBWixFQUF1QlosTUFBN0IsRUFBb0M7QUFDbENXLFVBQUlKLEVBQUVJLENBQUYsRUFBS0MsTUFBTCxDQUFZLFNBQVosQ0FBSjtBQUNBQyxjQUFRQyxHQUFSLENBQVlQLEVBQUVJLENBQUYsRUFBS0MsTUFBTCxDQUFZLFNBQVosRUFBdUJHLElBQXZCLEVBQVo7QUFDRDtBQUNEO0FBQ0E7QUFDQTtBQUNELEdBZkQ7QUFtQkQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUJEO0FBQ0E7QUFEQTs7QUFJQTtBQUNBO0FBQ0E7QUFDQSxLQUZBO0FBR0E7QUFDQTtBQUNBLEtBRkE7QUFHQSxDQVBBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDQU5BLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzREQTtBQUNBO0FBQ0Esd0JBREE7QUFFQSx3QkFGQTtBQUdBLHdCQUhBO0FBSUEsd0JBSkE7QUFLQSx5QkFMQTtBQU1BLHdCQU5BO0FBT0EsMEJBUEE7QUFRQSw2QkFSQTtBQVNBLDRCQVRBO0FBVUEseUJBVkE7QUFXQSw4QkFYQTtBQVlBLHdCQVpBO0FBYUEsbUJBYkE7QUFjQTtBQWRBO0FBREEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2REE7QUFDQTtBQURBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3lCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxvT0FEQTs7QUFJQSxRQUpBLGtCQUlBOztBQUVBO0FBQ0EsdUJBQ0Esc0VBREEsRUFFQSxtREFGQSxFQUdBLHlEQUhBLEVBSUEsbUZBSkEsRUFLQSwrREFMQSxFQU1BLDZGQU5BLEVBT0Esb0VBUEEsRUFRQSxpRkFSQSxFQVNBLGtGQVRBLEVBVUEsNkVBVkEsRUFXQSxnRUFYQSxFQVlBLG1GQVpBLEVBYUEseURBYkEsRUFjQSx3RUFkQSxFQWVBLGtFQWZBLEVBZ0JBLG1FQWhCQSxFQWlCQSxvRkFqQkEsRUFrQkEsaUdBbEJBLEVBbUJBLDZEQW5CQSxFQW9CQSwrREFwQkEsRUFxQkEsK0RBckJBLEVBc0JBLGlEQXRCQSxFQXVCQSxtREF2QkEsRUF3QkEsNkRBeEJBO0FBMEJBO0FBQ0E7QUFDQSxTQUZBO0FBR0E7QUFDQSx3QkFEQTtBQUVBLHdCQUZBO0FBR0EsOEJBSEE7QUFJQSwyQkFKQTtBQUtBLDJCQUxBO0FBTUEsMkJBTkE7QUFPQSwyQkFQQTtBQVFBLDRCQVJBO0FBU0EsMkJBVEE7QUFVQSw2QkFWQTtBQVdBLGdDQVhBO0FBWUEsK0JBWkE7QUFhQTtBQUNBLDhCQURBO0FBRUEsNEJBRkE7QUFHQSwrQkFIQTtBQUlBLDRDQUpBO0FBS0Esc0NBTEE7QUFNQSxxREFOQTtBQU9BLHFDQVBBO0FBUUEseUNBUkE7QUFTQSw0Q0FUQTtBQVVBLHVDQVZBO0FBV0EsbUNBWEE7QUFZQSw0Q0FaQTtBQWFBLDZCQWJBO0FBY0EsZ0NBZEE7QUFlQSxnQ0FmQTtBQWdCQSxvQ0FoQkE7QUFpQkEsNkJBakJBO0FBa0JBO0FBbEJBLGFBYkE7QUFpQ0EsMEJBakNBO0FBa0NBLHVCQWxDQTtBQW1DQSx5QkFuQ0E7QUFvQ0EsMEJBcENBO0FBcUNBLDBCQXJDQTtBQXNDQSx3QkF0Q0E7QUF1Q0EseUJBdkNBO0FBd0NBLDRCQXhDQTtBQXlDQSx5QkF6Q0E7QUEwQ0Esa0NBMUNBO0FBMkNBLG1EQTNDQTtBQTRDQTtBQUNBLHVCQURBO0FBRUEsOEJBRkE7QUFHQSwwQkFIQTtBQUlBLHlCQUpBO0FBS0E7QUFMQSxhQTVDQTtBQW1EQTtBQUNBLDRCQURBO0FBRUEsK0JBRkE7QUFHQSx5QkFIQTtBQUlBLCtCQUpBO0FBS0EsK0JBTEE7QUFNQSwrQkFOQTtBQU9BLHdCQVBBO0FBUUE7QUFSQTs7QUFuREE7QUErREEsS0FuR0E7O0FBb0dBO0FBQ0Esd0JBREEsNEJBQ0EsQ0FEQSxFQUNBO0FBQ0E7QUFBQTtBQUFBOztBQUVBO0FBRUEsU0FOQTtBQU9BLGtCQVBBLHNCQU9BLEtBUEEsRUFPQTtBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQSxtQkFJQSxJQUpBLENBSUE7QUFDQTtBQUNBO0FBQ0EsaUJBUEE7QUFRQTtBQUNBLFNBckJBO0FBdUJBLG1CQXZCQSx5QkF1QkE7QUFBQTs7QUFBQTs7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQSx1REFDQSxJQURBLENBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtJQUNBLCtIQURBLEtBRUEsMEpBRkEsS0FHQSwrSEFIQSxLQUlBLDZIQUpBLEtBS0EsbUhBQ0EsMEdBREEsS0FFQSxvR0FGQSxLQUdBLGdIQUhBLEtBSUEsc0hBSkEsS0FLQSxvSEFMQSxDQUxBO0FBV0EsK0lBQ0EsK0hBREEsS0FFQSw2SEFGQSxLQUdBLHdGQUNBLG9IQURBLENBSEE7QUFLQSx3SUFDQSwrSEFEQSxLQUVBLDBKQUZBLEtBR0EsK0hBSEEsS0FJQSwyRkFKQSxLQUtBLCtIQUxBLEtBTUEsOEZBTkEsS0FPQSxtSEFDQSwwR0FEQSxLQUVBLG9HQUZBLEtBR0EsZ0hBSEEsS0FJQSxzSEFKQSxLQUtBLHFGQUxBLEtBTUEsa0ZBTkEsQ0FQQTtBQWNBLDZLQUNBLDBKQURBLEtBRUEsc0hBRkEsS0FHQSwrSEFIQSxLQUlBLGdIQUpBLEtBS0EsK0hBTEEsS0FNQSxtSEFDQSwwR0FEQSxLQUVBLG9HQUZBLEtBR0Esa0ZBSEEsQ0FOQTtBQVVBO0FBQ0E7QUFDQSxtREEvQ0EsQ0ErQ0E7QUFDQSwrREFoREEsQ0FnREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdDQURBO0FBRUEsd0RBRkE7QUFHQSxvREFIQTtBQUlBLDBEQUpBO0FBS0Esb0ZBTEE7QUFNQSx3RUFOQTtBQU9BLHNHQVBBO0FBUUEsc0VBUkE7QUFTQSw4RUFUQTtBQVVBLG9GQVZBO0FBV0EsMEVBWEE7QUFZQSxrRUFaQTtBQWFBLG9GQWJBO0FBY0Esc0RBZEE7QUFlQSw0REFmQTtBQWdCQSw0REFoQkE7QUFpQkEsb0VBakJBO0FBa0JBLHdEQWxCQTtBQW1CQSw4REFuQkE7QUFvQkEsbURBcEJBO0FBcUJBLHVEQXJCQTtBQXNCQSx1REF0QkE7QUF1QkEsb0RBdkJBO0FBd0JBLHdDQXhCQTtBQXlCQTtBQXpCQTtBQTJCQTtBQUNBLHFCQW5GQTtBQW9GQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBN0dBLEVBOEdBLEtBOUdBLENBOEdBO0FBQ0E7QUFDQSxhQWhIQTtBQWlIQSxTQTVJQTtBQTZJQSxtQkE3SUEseUJBNklBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0NBREE7QUFFQTtBQUZBO0FBSUEsYUFMQSxNQUtBO0FBQ0E7QUFDQSxvQ0FEQTtBQUVBO0FBRkE7QUFLQSxhQU5BLE1BTUE7QUFDQTtBQUNBLG9DQURBO0FBRUE7QUFGQTtBQUlBLGFBTEEsTUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhEQURBO0FBRUEsOEtBRkE7QUFHQSw2REFIQTtBQUlBLG9EQUpBO0FBS0EsOENBTEE7QUFNQSw2SEFOQTtBQU9BLDRHQVBBO0FBUUEsaUhBUkE7QUFTQSxvSEFUQTtBQVVBLCtHQVZBO0FBV0EsMEdBWEE7QUFZQSxvREFaQTtBQWFBLHFDQWJBO0FBY0Esd0NBZEE7QUFlQSx3Q0FmQTtBQWdCQTtBQWhCQTtBQWtCQTtBQUNBLGtEQUVBO0FBRUEscUJBTEEsRUFLQSxJQUxBLENBS0E7QUFDQTtBQUNBLDBDQURBO0FBRUEsd0NBRkE7QUFHQSwyQ0FIQTtBQUlBLHdEQUpBO0FBS0Esa0RBTEE7QUFNQSxpRUFOQTtBQU9BLGlEQVBBO0FBUUEscURBUkE7QUFTQSx3REFUQTtBQVVBLG1EQVZBO0FBV0EsK0NBWEE7QUFZQSx3REFaQTtBQWFBLHlDQWJBO0FBY0EsNENBZEE7QUFlQSw0Q0FmQTtBQWdCQTtBQWhCQTtBQWtCQTtBQUNBO0FBQ0E7QUFDQSxnREFEQTtBQUVBLHdEQUZBO0FBR0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBLHFCQWxDQTtBQW1DQSxpQkF2REEsTUF1REE7QUFDQTtBQUNBO0FBQ0EsbURBREE7QUFFQSx5REFGQTtBQUdBO0FBSEE7QUFEQSx1QkFNQSxLQU5BLENBTUE7QUFDQSxrREFFQTtBQUVBLHFCQVhBLEVBV0EsSUFYQSxDQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0RBREE7QUFFQSwwREFGQTtBQUdBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQSxxQkF0QkE7QUF1QkE7QUFDQTtBQUNBLFNBalBBOztBQWtQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0ExUEE7QUEyUEEsd0JBM1BBLDRCQTJQQSxJQTNQQSxFQTJQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQXBRQTtBQXFRQSxjQXJRQSxrQkFxUUEsR0FyUUEsRUFxUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0EzUUE7QUE0UUEsZ0JBNVFBLG9CQTRRQSxLQTVRQSxFQTRRQSxHQTVRQSxFQTRRQSxLQTVRQSxFQTRRQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBOVFBLEtBcEdBO0FBb1hBLGdCQXBYQTtBQXVYQSxXQXZYQSxxQkF1WEE7QUFDQTtBQUNBLEtBelhBOztBQTBYQTtBQUNBOztBQUVBO0FBN1hBLEc7Ozs7Ozs7QUN6RUE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUFrSDtBQUNsSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUFrSDtBQUNsSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUFrSDtBQUNsSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUFrSDtBQUNsSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMxSEEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMxSkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM1QkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsdUJBQXVCLHFCQUFxQjtBQUM1QztBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQSx1QkFBdUIscUJBQXFCO0FBQzVDO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSwwQkFBMEIseUJBQXlCLFFBQVEsZUFBZTtBQUMxRTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBLG1CQUFtQixnQkFBZ0I7QUFDbkM7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSzs7QUFFTDtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSwrREFBK0Q7O0FBRS9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsMkNBQTJDOztBQUUzQztBQUNBO0FBQ0E7QUFDQSx1Q0FBdUMsS0FBSyxFQUFFLEtBQUssTUFBTSxFQUFFO0FBQzNEOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCOztBQUVyQjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7O0FBRUE7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTs7QUFFQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQixpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQSxhQUFhO0FBQ2I7O0FBRUE7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsMkJBQTJCO0FBQzNCO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDBCQUEwQjtBQUMxQjs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLG9DQUFvQyxFQUFFO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSx5QkFBeUI7O0FBRXpCO0FBQ0Esc0JBQXNCO0FBQ3RCOztBQUVBLHlCQUF5Qjs7QUFFekI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGlCQUFpQjtBQUNqQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEI7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7OztBQUdBOztBQUVBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTs7QUFFQSwyREFBMkQsZUFBZTtBQUMxRTs7QUFFQSxxQ0FBcUMsb0JBQW9COztBQUV6RDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBOztBQUVBLGFBQWE7QUFDYjtBQUNBOztBQUVBOztBQUVBLFNBQVM7O0FBRVQsS0FBSztBQUNMOztBQUVBOztBQUVBLGtDQUFrQyxLQUFLLFlBQVksYUFBYSxLQUFLOztBQUVyRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7OztBQUdBOztBQUVBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrQ0FBK0MsZUFBZTtBQUM5RDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxxQkFBcUIsY0FBYztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBLDJCQUEyQjtBQUMzQix1REFBdUQ7QUFDdkQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7O0FBRWI7QUFDQTs7QUFFQSxxQ0FBcUMsb0JBQW9COztBQUV6RDtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0I7O0FBRXhCO0FBQ0E7QUFDQSxTQUFTOztBQUVULHFCQUFxQiw2Q0FBNkM7O0FBRWxFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7O0FBRWI7O0FBRUEsU0FBUyx1QkFBdUIsbUNBQW1DLEVBQUU7QUFDckUsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLGlCQUFpQjs7QUFFakI7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCLHFCQUFxQjs7QUFFckI7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7QUFHQTs7QUFFQSwwQ0FBMEMsbUNBQW1DLEVBQUU7QUFDL0U7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxxQ0FBcUMsb0RBQW9ELEVBQUU7QUFDM0YsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLEtBQUs7O0FBRUw7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOENBQThDLHlCQUF5QixFQUFFO0FBQ3pFOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTs7QUFFQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EscURBQXFELEdBQUcsbUJBQW1CO0FBQzNFOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxzQkFBc0I7QUFDdEIseUJBQXlCLGtDQUFrQzs7QUFFM0Q7O0FBRUEseUJBQXlCOztBQUV6Qiw2QkFBNkI7O0FBRTdCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSzs7QUFFTDs7QUFFQTs7QUFFQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG9CQUFvQjtBQUNwQjs7QUFFQTs7QUFFQTtBQUNBLDJDQUEyQyxHQUFHLDRCQUE0QjtBQUMxRTs7QUFFQSxDQUFDOztBQUVEOztBQUVBO0FBQ0EsMkNBQTJDLEdBQUcsd0NBQXdDO0FBQ3RGOztBQUVBLENBQUM7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBLHlCQUF5Qjs7QUFFekIsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSx3QkFBd0IsMkJBQTJCLFVBQVU7O0FBRTdEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTs7QUFFQTs7QUFFQSw4QkFBOEIsc0JBQXNCOztBQUVwRDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsaUNBQWlDOztBQUVqQztBQUNBOztBQUVBOztBQUVBLFVBQVUsY0FBYztBQUN4QixXQUFXLGVBQWU7QUFDMUIsWUFBWSxjQUFjO0FBQzFCLGFBQWEsY0FBYztBQUMzQixhQUFhLGlCQUFpQjtBQUM5QixhQUFhOztBQUViOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBOztBQUVBLDRDQUE0QywwQ0FBMEM7QUFDdEY7QUFDQTs7QUFFQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ1EiLCJmaWxlIjoianMvdmlzYS9uZmluYW5jaW5nL2ZpbmFuY2luZy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNTI5KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBkNDJiYWJjMDUwM2NlYmQ2OTg5NCIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDE4IDE5IDIwIDIxIDIyIDIzIDI0IDI1IDI2IDI3IDI4IiwiLyogKGlnbm9yZWQpICovXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gZ290IChpZ25vcmVkKVxuLy8gbW9kdWxlIGlkID0gMTBcbi8vIG1vZHVsZSBjaHVua3MgPSA2IDkgMTAgMTciLCJWdWUuY29tcG9uZW50KCdmaW5hbmNlLWNvbnRlbnQnLCByZXF1aXJlKCcuLi9jb21wb25lbnRzL05ld0ZpbmFuY2luZy9Qcm9qZWN0cy52dWUnKSk7XG5cblZ1ZS5odHRwLmludGVyY2VwdG9ycy5wdXNoKChyZXF1ZXN0LCBuZXh0KSA9PiB7XG4gICAgcmVxdWVzdC5oZWFkZXJzLnNldCgnWC1DU1JGLVRPS0VOJywgTGFyYXZlbC5jc3JmVG9rZW4pO1xuXG4gICAgbmV4dCgpO1xufSk7XG5jb25zdCBhcHAgPSBuZXcgVnVlKHtcbiAgZWw6ICcjbkZpbmFuY2luZ3MnLFxufSk7XG5cbnZhciB0b2dnbGVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcImNhcmV0c1wiKTtcbnZhciBwcm9jZXNzTG9jYXRvciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicHJvY2Vzc0xvY2F0b3JcIik7XG52YXIgaTtcbnZhciBzZWw7XG5mb3IgKGkgPSAwOyBpIDwgdG9nZ2xlci5sZW5ndGg7IGkrKykge1xudG9nZ2xlcltpXS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG4gIHRoaXMucGFyZW50RWxlbWVudC5xdWVyeVNlbGVjdG9yKFwiLm5lc3RlZFwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiYWN0aXZlXCIpO1xuICB0aGlzLmNsYXNzTGlzdC50b2dnbGUoXCJjYXJldHMtZG93blwiKTtcbiAgLy8gaWYoJC50cmltKHByb2Nlc3NMb2NhdG9yLmlubmVySFRNTCkubGVuZ3RoID09IDApe1xuICAvLyAgICAgcHJvY2Vzc0xvY2F0b3IuYXBwZW5kKHRoaXMuaW5uZXJIVE1MKTtcbiAgLy8gfWVsc2V7XG4gIC8vICAgICBwcm9jZXNzTG9jYXRvci5hcHBlbmQoXCIgPiBcIit0aGlzLmlubmVySFRNTCk7XG4gIC8vIH1cblxufSk7XG59XG5cbnZhciB0b2dnbGVyMiA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJyb290XCIpO1xuXG5mb3IgKGkgPSAwOyBpIDwgdG9nZ2xlcjIubGVuZ3RoOyBpKyspIHtcbiAgdG9nZ2xlcjJbaV0uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xuICAgICQoc2VsKS5yZW1vdmVDbGFzcygncm9vdFNlbGVjdGVkJyk7XG4gICAgc2VsID0gdGhpcztcbiAgICB2YXIgaW5wdXRGb3JtID0gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaW5wdXRGb3JtXCIpO1xuICAgICQoaW5wdXRGb3JtKS5yZW1vdmVDbGFzcyhcImlucHV0Rm9ybVwiKTtcbiAgICAkKGlucHV0Rm9ybSkuYWRkQ2xhc3MoJ2lucHV0Rm9ybTInKTtcbiAgICAkKHNlbCkuYWRkQ2xhc3MoJ3Jvb3RTZWxlY3RlZCcpO1xuICAgIHZhciBwID0gdGhpcztcbiAgICB3aGlsZSgkKHApLnBhcmVudCgnLmNhcmV0cycpLmxlbmd0aCl7XG4gICAgICBwID0gJChwKS5wYXJlbnQoJy5jYXJldHMnKTtcbiAgICAgIGNvbnNvbGUubG9nKCQocCkucGFyZW50KCcuY2FyZXRzJykuaHRtbCgpKTtcbiAgICB9XG4gICAgLy8kKHByb2Nlc3NMb2NhdG9yKS5odG1sKCQodGhpcykucGFyZW50cygpLnBhcmVudHNVbnRpbCgnI215VUwnKS5odG1sKCkucmVwbGFjZSgvKDwoW14+XSspPikvaWcsXCJcIikucmVwbGFjZSggL1xcc1xccysvZywgJ3wnKSk7XG4gICAgLy9jb25zb2xlLmxvZygkKHRoaXMpLnBhcmVudHNVbnRpbCgnLmxpUGFyZW50JykuY2xvc2VzdCgnc3BhbicpLmh0bWwoKSlcbiAgICAvLyQodGhpcykucGFyZW50cygpLnBhcmVudHNVbnRpbCgnbGkgLmxpUGFyZW50JykuY3NzKFwiYmFja2dyb3VuZC1jb2xvclwiLFwiYmxhY2tcIik7XG4gIH0pO1xuXG5cblxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9uZmluYW5jaW5nL2ZpbmFuY2luZy5qcyIsIjx0ZW1wbGF0ZT5cbiAgICA8ZGl2PlxuICAgIDxkaXYgY2xhc3M9XCJ3cmFwcGVyMTFcIj5cbiAgICAgIDxkaXYgY2xhc3M9XCJkaXYxMVwiPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICAgPGRpdiBjbGFzcz1cIndyYXBwZXIyMlwiPlxuICAgICAgPGRpdiBjbGFzcz1cImRpdjIyXCI+XG4gICAgICAgICAgICAgIDx0YWJsZSBpZD1cImZ0YWJsZVwiIGNsYXNzPVwidGFibGUgdGFibGUtaG92ZXIgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJlZFwiPlxuICAgICAgICAgICAgICAgIDx0aGVhZD5cbiAgICAgICAgICAgICAgICAgICAgPHRyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRoIHYtZm9yPVwiY29sdW1uIGluIGNvbHVtbnNcIiA6a2V5PVwiY29sdW1uLm5hbWVcIiA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3tjb2x1bW4ubGFiZWx9fVxuICAgICAgICAgICAgICAgICAgICAgICAgPC90aD5cbiAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICA8L3RoZWFkPlxuICAgICAgICAgICAgICAgIDxzbG90Pjwvc2xvdD5cbiAgICAgICAgICAgICAgPC90YWJsZT5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbiAgZXhwb3J0IGRlZmF1bHR7XG4gICAgcHJvcHM6IFsnY29sdW1ucycsJ3NvcnRLZXknLCdzb3J0T3JkZXJzJ11cbiAgfVxuXG4gICQoZnVuY3Rpb24gKCkge1xuICAgICAgJCgnLndyYXBwZXIxMScpLm9uKCdzY3JvbGwnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICQoJy53cmFwcGVyMjInKS5zY3JvbGxMZWZ0KCQoJy53cmFwcGVyMTEnKS5zY3JvbGxMZWZ0KCkpO1xuICAgICAgfSk7XG4gICAgICAkKCcud3JhcHBlcjIyJykub24oJ3Njcm9sbCcsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgJCgnLndyYXBwZXIxMScpLnNjcm9sbExlZnQoJCgnLndyYXBwZXIyMicpLnNjcm9sbExlZnQoKSk7XG4gICAgICB9KTtcbiAgfSk7XG4gICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG4gICAgICAvLyQoJy5kaXYxMScpLndpZHRoKCQoJyNmdGFibGUnKS53aWR0aCgpKTtcbiAgICAgICQoJy5kaXYyMicpLndpZHRoKCQoJyNmdGFibGUnKS53aWR0aCgpKTtcbiAgICAgICQoJy5kaXYxMScpLndpZHRoKCQoJyNmdGFibGUnKS53aWR0aCgpKzMwMCk7XG5cbiAgICAgIC8vYWxlcnQoJCgnLmRpdjIyJylbMF0uc2Nyb2xsV2lkdGgrJyB8ICcrJCgnI2Z0YWJsZScpLndpZHRoKCkpO1xuICB9KTtcbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIERhdGFUYWJsZS52dWU/MTJlZDRjZjQiLCJcbjx0ZW1wbGF0ZT5cbiAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxuICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwiaWJveCBmbG9hdC1lLW1hcmdpbnMgbm8tcGFkZGluZ1wiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWJveC1jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS00IGItclwiID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpYm94XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGFibGUgY2xhc3M9XCJ0YmwtY29tcHV0YXRpb25cIiA+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5Jbml0aWFsIENhc2g6PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPjxzcGFuIGNsYXNzPVwidGV4dC1tdXRlZFwiPnt7bnVtYmVyV2l0aENvbW1hcyhpbml0aWFsX2Nhc2gpfX08L3NwYW4+PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPkluaXRpYWwgQmFuazo8L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+PHNwYW4gY2xhc3M9XCJ0ZXh0LW11dGVkXCI+e3tudW1iZXJXaXRoQ29tbWFzKGluaXRpYWxfYmFuayl9fTwvc3Bhbj48L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRyPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+Q2FzaCBCYWxhbmNlOjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD48c3BhbiBjbGFzcz1cInRleHQtbXV0ZWRcIj57e251bWJlcldpdGhDb21tYXMoY2FzaF9iYWxhbmNlKX19PC9zcGFuPjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5CYW5rIEJhbGFuY2U6PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPjxzcGFuIGNsYXNzPVwidGV4dC1tdXRlZFwiPnt7bnVtYmVyV2l0aENvbW1hcyhiYW5rX2JhbGFuY2UpfX08L3NwYW4+PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPlRvdGFsOjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD48c3BhbiBjbGFzcz1cInRleHQtbXV0ZWRcIj57e251bWJlcldpdGhDb21tYXModG90YWxfYmFsYW5jZSl9fTwvc3Bhbj48L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRyPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+UHJvZml0OjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD48c3BhbiBjbGFzcz1cInRleHQtbXV0ZWRcIj57e251bWJlcldpdGhDb21tYXModG90YWxfcHJvZml0KX19PC9zcGFuPjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5Qcm9jZXNzIE91dHB1dDo8L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+PHNwYW4gY2xhc3M9XCJ0ZXh0LW11dGVkXCI+e3tudW1iZXJXaXRoQ29tbWFzKHByb2Nlc3Nfb3V0cHV0KX19PC9zcGFuPjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5Ib3cgbXVjaCB3ZSByZWNlaXZlZCBmcm9tIGNsaWVudDo8L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+PHNwYW4gY2xhc3M9XCJ0ZXh0LW11dGVkXCI+e3tudW1iZXJXaXRoQ29tbWFzKHdlX3JlY2VpdmVfYmVmb3JlKX19PC9zcGFuPjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5Ib3cgbXVjaCB3ZSByZWNlaXZlZCBhZnRlciB3ZSBzcGVudCBmb3IgY2xpZW50OjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD48c3BhbiBjbGFzcz1cInRleHQtbXV0ZWRcIj57e251bWJlcldpdGhDb21tYXMod2VfcmVjZWl2ZV9hZnRlcil9fTwvc3Bhbj48L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+XG5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90YWJsZT5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tOFwiPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IG1ldGhvZD1cIlBPU1RcIiByb2xlPVwiZm9ybVwiIGlkPVwiaW5wdXRGb3JtXCIgY2xhc3M9XCJpbnB1dEZvcm1cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj48bGFiZWw+VHJhbnNhY3Rpb24gVHlwZTwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBpZD1cInRyYW5zYWN0aW9uX3R5cGVcIiBjbGFzcz1cImZvcm0tY29udHJvbCBtLWJcIiBAY2hhbmdlPVwiY2hlY2tUcmFucygkZXZlbnQpXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJcIiBkaXNhYmxlZCBzZWxlY3RlZD5QbGVhc2UgU2VsZWN0PC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJwYnJcIj5Qcm9jZXNzaW5nIEJ1ZGdldCBSZXR1cm48L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cInBjXCI+UHJvY2Vzc2luZyBDb3N0PC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJicGNcIj5Cb3Jyb3dlZCBQcm9jZXNzaW5nIENvc3Q8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImFiclwiPkFkbWluIEJ1ZGdldCBSZXR1cm48L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImFjXCI+QWRtaW4gQ29zdDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiYmFjXCI+Qm9ycm93ZWQgQWRtaW4gQ29zdDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiB2LWlmPVwiYm9ycm93ZWQubGVuZ3RoPjBcIiBjbGFzcz1cImZvcm0tZ3JvdXBcIj48bGFiZWw+Qm9ycm93ZWQgTGlzdDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbCBtLWJcIiBpZD1cImJvcnJvd2VkX2xpc3RcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiAgIHYtZm9yPVwiYiBpbiBib3Jyb3dlZFwiIDp2YWx1ZT1cImIuaWRcIiA6a2V5PVwiYi5pZFwiPnt7Yi50cmFuc19kZXNjfX08L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgdi1pZj1cImNhdF90eXBlPT0nYnBjJyB8fCBjYXRfdHlwZT09J2JhYydcIj48bGFiZWw+U3RvcmFnZSBUeXBlPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sIG0tYlwiIGlkPVwic3RvcmFnZV90eXBlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJDYXNoXCI+Q2FzaDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiQmFua1wiPkJhbms8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiB2LWlmPVwiY2F0X3R5cGUhPScnXCI+PGxhYmVsPkFtb3VudDwvbGFiZWw+IDxpbnB1dCBpZD1cInR4dEFtb3VudFwiIHR5cGU9XCJ0ZXh0XCIgcGxhY2Vob2xkZXI9XCIwLjAwXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIj48L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtaWY9XCJjYXRfdHlwZT09J2JwYycgfHwgY2F0X3R5cGU9PSdiYWMnXCI+PGxhYmVsPkRlc2NyaXB0aW9uPC9sYWJlbD4gPHRleHRhcmVhIGlkPVwidHh0RGVzY3JpcHRpb25cIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHJvdz1cIjVcIj48L3RleHRhcmVhPjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdi1pZj1cImNhdF90eXBlIT0nJ1wiIGNsYXNzPVwiYnRuIGJ0bi1zbSBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0IG0tdC1uLXhzXCIgQGNsaWNrPVwic2F2ZVByb2plY3QoKVwiPjxzdHJvbmc+U2F2ZTwvc3Ryb25nPjwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDIgY2xhc3M9XCJwcm9jZXNzTG9jYXRvclwiIGlkPVwicHJvY2Vzc0xvY2F0b3JcIj48L2gyPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuXG5cbiAgICAgIDwvZGl2PlxuXG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIHByb3BzOiB7XG4gICAgaW5pdGlhbF9jYXNoOk51bWJlcixcbiAgICBpbml0aWFsX2Jhbms6TnVtYmVyLFxuICAgIGJhbmtfYmFsYW5jZTpOdW1iZXIsXG4gICAgY2FzaF9iYWxhbmNlOk51bWJlcixcbiAgICB0b3RhbF9iYWxhbmNlOk51bWJlcixcbiAgICB0b3RhbF9wcm9maXQ6TnVtYmVyLFxuICAgIHByb2Nlc3Nfb3V0cHV0Ok51bWJlcixcbiAgICB3ZV9yZWNlaXZlX2JlZm9yZTpOdW1iZXIsXG4gICAgd2VfcmVjZWl2ZV9hZnRlcjpOdW1iZXIsXG4gICAgc2F2ZVByb2plY3Q6IEZ1bmN0aW9uLFxuICAgIG51bWJlcldpdGhDb21tYXM6IEZ1bmN0aW9uLFxuICAgIGNoZWNrVHJhbnM6IEZ1bmN0aW9uLFxuICAgIGJvcnJvd2VkOiBBcnJheSxcbiAgICBjYXRfdHlwZTogU3RyaW5nXG4gIH1cbn1cblxuPC9zY3JpcHQ+XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRmluYW5jaW5nLnZ1ZT82ZjhiYmRmZiIsIjx0ZW1wbGF0ZT5cbiAgPGRpdiA+XG4gIDxuYXYgY2xhc3M9XCJwYWdpbmF0aW9uXCIgdi1pZj1cIiFjbGllbnRcIj5cbiAgICAgICAgPHNwYW4gY2xhc3M9XCJwYWdlLXN0YXRzXCI+e3twYWdpbmF0aW9uLmZyb219fSAtIHt7cGFnaW5hdGlvbi50b319IG9mIHt7cGFnaW5hdGlvbi50b3RhbH19PC9zcGFuPlxuICAgICAgICA8YSB2LWlmPVwicGFnaW5hdGlvbi5wcmV2UGFnZVVybFwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tcHJldmlvdXNcIiBAY2xpY2s9XCIkZW1pdCgncHJldicpO1wiPlxuICAgICAgICAgICAgUHJldlxuICAgICAgICA8L2E+XG4gICAgICAgIDxhIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tcHJldmlvdXNcIiB2LWVsc2UgOmRpc2FibGVkPVwidHJ1ZVwiPlxuICAgICAgICAgICBQcmV2XG4gICAgICAgIDwvYT5cblxuICAgICAgICA8YSB2LWlmPVwicGFnaW5hdGlvbi5uZXh0UGFnZVVybFwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tbmV4dFwiIEBjbGljaz1cIiRlbWl0KCduZXh0Jyk7XCI+XG4gICAgICAgICAgICBOZXh0XG4gICAgICAgIDwvYT5cbiAgICAgICAgPGEgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1uZXh0XCIgdi1lbHNlIDpkaXNhYmxlZD1cInRydWVcIj5cbiAgICAgICAgICAgIE5leHRcbiAgICAgICAgPC9hPlxuICAgIDwvbmF2PlxuXG4gICAgPG5hdiBjbGFzcz1cInBhZ2luYXRpb25cIiB2LWVsc2U+XG4gICAgICAgIDxzcGFuIGNsYXNzPVwicGFnZS1zdGF0c1wiPlxuICAgICAgICAgICAge3twYWdpbmF0aW9uLmZyb219fSAtIHt7cGFnaW5hdGlvbi50b319IG9mIHt7ZmlsdGVyZWQubGVuZ3RofX1cbiAgICAgICAgICAgIDxzcGFuIHYtaWY9XCJmaWx0ZXJlZC5sZW5ndGggPCBwYWdpbmF0aW9uLnRvdGFsXCI+KGZpbHRlcmVkIGZyb20ge3twYWdpbmF0aW9uLnRvdGFsfX0gdG90YWwgZW50cmllcyk8L3NwYW4+XG4gICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPGEgdi1pZj1cInBhZ2luYXRpb24ucHJldlBhZ2VcIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLXByZXZpb3VzXCIgQGNsaWNrPVwiJGVtaXQoJ3ByZXYnKTtcIj5cbiAgICAgICAgICAgIFByZXZcbiAgICAgICAgPC9hPlxuICAgICAgICA8YSBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLXByZXZpb3VzXCIgdi1lbHNlIDpkaXNhYmxlZD1cInRydWVcIj5cbiAgICAgICAgICAgUHJldlxuICAgICAgICA8L2E+XG5cbiAgICAgICAgPGEgdi1pZj1cInBhZ2luYXRpb24ubmV4dFBhZ2VcIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLW5leHRcIiBAY2xpY2s9XCIkZW1pdCgnbmV4dCcpO1wiPlxuICAgICAgICAgICAgTmV4dFxuICAgICAgICA8L2E+XG4gICAgICAgIDxhIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tbmV4dFwiIHYtZWxzZSA6ZGlzYWJsZWQ9XCJ0cnVlXCI+XG4gICAgICAgICAgICBOZXh0XG4gICAgICAgIDwvYT5cbiAgICA8L25hdj5cbiAgPC9kaXY+XG48L3RlbXBsYXRlPlxuPHNjcmlwdD5cbiAgZXhwb3J0IGRlZmF1bHR7XG4gICAgcHJvcHM6IFsncGFnaW5hdGlvbicsICdjbGllbnQnLCAnZmlsdGVyZWQnXVxuICB9XG48L3NjcmlwdD5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBQYWdpbmF0aW9uLnZ1ZT8xZTYyMjRiNiIsIlxuPHRlbXBsYXRlPlxuXG4gIDxkaXY+XG5cbiAgICA8RmluYW5jZUVkaXRvciB2LWJpbmQ6Y2F0X3R5cGU9XCJjYXRfdHlwZVwiIHYtYmluZDpib3Jyb3dlZD1cImJvcnJvd2VkXCIgdi1iaW5kOmNoZWNrVHJhbnM9XCJjaGVja1RyYW5zXCIgdi1iaW5kOm51bWJlcldpdGhDb21tYXM9XCJudW1iZXJXaXRoQ29tbWFzXCIgdi1iaW5kOndlX3JlY2VpdmVfYmVmb3JlPVwid2VfcmVjZWl2ZV9iZWZvcmVcIiAgdi1iaW5kOndlX3JlY2VpdmVfYWZ0ZXI9XCJ3ZV9yZWNlaXZlX2FmdGVyXCIgdi1iaW5kOnByb2Nlc3Nfb3V0cHV0PVwicHJvY2Vzc19vdXRwdXRcIiB2LWJpbmQ6dG90YWxfcHJvZml0PVwidG90YWxfcHJvZml0XCIgdi1iaW5kOnRvdGFsX2JhbGFuY2U9XCJ0b3RhbF9iYWxhbmNlXCIgdi1iaW5kOmNhc2hfYmFsYW5jZT1cImNhc2hfYmFsYW5jZVwiIHYtYmluZDpiYW5rX2JhbGFuY2U9XCJiYW5rX2JhbGFuY2VcIiB2LWJpbmQ6aW5pdGlhbF9iYW5rPVwiaW5pdGlhbF9iYW5rXCIgdi1iaW5kOmluaXRpYWxfY2FzaD1cImluaXRpYWxfY2FzaFwiIHYtYmluZDpzYXZlUHJvamVjdD1cInNhdmVQcm9qZWN0XCIgPlxuXG4gICAgPC9GaW5hbmNlRWRpdG9yPlxuXG4gICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwiaWJveCBmbG9hdC1lLW1hcmdpbnNcIj5cblxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlib3gtY29udGVudFwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPVwiZGlzcGxheTpub25lXCIgY2xhc3M9XCJjb2wtc20tMSBtLWIteHNcIj48c2VsZWN0IGNsYXNzPVwiaW5wdXQtc20gZm9ybS1jb250cm9sIGlucHV0LXMtc20gaW5saW5lXCIgdi1tb2RlbD1cInRhYmxlRGF0YS5sZW5ndGhcIiBAY2hhbmdlPVwiZ2V0UHJvamVjdHMoKVwiPlxuICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwiKHJlY29yZHMsIGluZGV4KSBpbiBwZXJQYWdlXCIgOmtleT1cImluZGV4XCIgOnZhbHVlPVwicmVjb3Jkc1wiPnt7cmVjb3Jkc319PC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLW9mZnNldC03IGNvbC1zbS00XCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIHB1bGwtcmlnaHRcIj48aW5wdXQgdi1tb2RlbD1cInRhYmxlRGF0YS5zZWFyY2hcIiBAaW5wdXQ9XCJnZXRQcm9qZWN0cygpXCIgdHlwZT1cInRleHRcIiBwbGFjZWhvbGRlcj1cIlNlYXJjaFwiIGNsYXNzPVwiaW5wdXQtc20gZm9ybS1jb250cm9sXCI+IDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgPEZpbmFuY2VUYWJsZSA6Y29sdW1ucz1cImNvbHVtbnNcIiA6c29ydEtleT1cInNvcnRLZXlcIiA6c29ydE9yZGVycz1cInNvcnRPcmRlcnNcIiBAc29ydD1cInNvcnRCeVwiPlxuICAgICAgICAgICAgICAgICAgPHRib2R5PlxuICAgICAgICAgICAgICAgICAgICAgIDx0ciB2LWZvcj1cInByb2plY3QgaW4gcHJvamVjdHMyXCIgOmtleT1cInByb2plY3RzMi5pZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3twcm9qZWN0LnRyYW5zX2Rlc2N9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e3Byb2plY3QuY2F0X3R5cGV9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e3Byb2plY3QuY2F0X3N0b3JhZ2V9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e251bWJlcldpdGhDb21tYXMocHJvamVjdC5jYXNoX2NsaWVudF9kZXBvX3BheW1lbnQpfX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3tudW1iZXJXaXRoQ29tbWFzKHByb2plY3QuY2FzaF9jbGllbnRfcmVmdW5kKX19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPnt7bnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmNhc2hfY2xpZW50X3Byb2Nlc3NfYnVkZ2V0X3JldHVybil9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e251bWJlcldpdGhDb21tYXMocHJvamVjdC5jYXNoX3Byb2Nlc3NfY29zdCl9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e251bWJlcldpdGhDb21tYXMocHJvamVjdC5ib3Jyb3dlZF9wcm9jZXNzX2Nvc3QpfX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3tudW1iZXJXaXRoQ29tbWFzKHByb2plY3QuY2FzaF9hZG1pbl9idWRnZXRfcmV0dXJuKX19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPnt7bnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmJvcnJvd2VkX2FkbWluX2Nvc3QpfX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3tudW1iZXJXaXRoQ29tbWFzKHByb2plY3QuY2FzaF9hZG1pbl9jb3N0KX19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPnt7bnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmJhbmtfY2xpZW50X2RlcG9fcGF5bWVudCl9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e251bWJlcldpdGhDb21tYXMocHJvamVjdC5iYW5rX2Nvc3QpfX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3tudW1iZXJXaXRoQ29tbWFzKHByb2plY3QuZGVwb3NpdF9vdGhlcil9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e251bWJlcldpdGhDb21tYXMocHJvamVjdC5jb3N0X290aGVyKX19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPnt7bnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LnRvdGFsX3Byb2Nlc3MpfX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3tudW1iZXJXaXRoQ29tbWFzKHByb2plY3Qud2VfaGF2ZV9iZWZvcmUpfX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3tudW1iZXJXaXRoQ29tbWFzKHByb2plY3Qud2VfaGF2ZV9hZnRlcil9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e251bWJlcldpdGhDb21tYXMocHJvamVjdC5hZG1pbl90b3RhbCl9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e251bWJlcldpdGhDb21tYXMocHJvamVjdC5jYXNoX2JhbGFuY2UpfX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3tudW1iZXJXaXRoQ29tbWFzKHByb2plY3QuYmFua19iYWxhbmNlKX19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPnt7bnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LnRvdGFsKX19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPnt7bnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LnByb2ZpdCl9fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57e251bWJlcldpdGhDb21tYXMocHJvamVjdC5wb3N0ZGF0ZWRfY2hlY2tzKX19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgPC90Ym9keT5cbiAgICAgICAgICAgICAgPC9GaW5hbmNlVGFibGU+XG4gICAgICAgICAgICAgIDxwYWdpbmF0aW9uIDpwYWdpbmF0aW9uPVwicGFnaW5hdGlvblwiXG4gICAgICAgICAgICAgICAgICAgIEBwcmV2PVwiZ2V0UHJvamVjdHMocGFnaW5hdGlvbi5wcmV2UGFnZVVybClcIlxuICAgICAgICAgICAgICAgICAgICBAbmV4dD1cImdldFByb2plY3RzKHBhZ2luYXRpb24ubmV4dFBhZ2VVcmwpXCIgc3R5bGU9XCJkaXNwbGF5Om5vbmUgIWltcG9ydGFudFwiPlxuICAgICAgICAgICAgICA8L3BhZ2luYXRpb24+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblxuICB2YXIgcHJvY2Vzc19vdXRwdXQgPSAwO1xuICBpbXBvcnQgRmluYW5jZUVkaXRvciBmcm9tICcuL0ZpbmFuY2luZy52dWUnO1xuICBpbXBvcnQgRmluYW5jZVRhYmxlIGZyb20gJy4vRGF0YVRhYmxlLnZ1ZSc7XG4gIGltcG9ydCBQYWdpbmF0aW9uIGZyb20gJy4vUGFnaW5hdGlvbi52dWUnO1xuICBjb25zdCByZXNvdXJjZSA9IHJlcXVpcmUoJ3Z1ZS1yZXNvdXJjZScpO1xuICBsZXQgdXNlciA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XG5cbiAgZXhwb3J0IGRlZmF1bHQge1xuICAgIGNvbXBvbmVudHM6IHsgRmluYW5jZUVkaXRvcjogRmluYW5jZUVkaXRvciwgRmluYW5jZVRhYmxlOiBGaW5hbmNlVGFibGUsIFBhZ2luYXRpb246IFBhZ2luYXRpb259LFxuXG5cbiAgICBkYXRhKCkge1xuXG4gICAgICAgIGxldCBzb3J0T3JkZXJzID0ge307XG4gICAgICAgIGxldCBjb2x1bW5zID0gW1xuICAgICAgICAgICAge3dpZHRoOiAnMjAlJywgICBsYWJlbDogJ1RyYW5zYWN0aW9uIERlc2NyaXB0aW9uJywgbmFtZTogJ3RyYW5zX2Rlc2MnIH0sXG4gICAgICAgICAgICB7d2lkdGg6ICc1LjMzJScsIGxhYmVsOiAnVHlwZScsIG5hbWU6ICdjYXRfdHlwZScgfSxcbiAgICAgICAgICAgIHt3aWR0aDogJzUuMzMlJywgbGFiZWw6ICdTdG9yYWdlJywgbmFtZTogJ2NhdF9zdG9yYWdlJyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0Nhc2ggRGVwb3NpdC9QYXltZW50JywgbmFtZTogJ2Nhc2hfY2xpZW50X2RlcG9fcGF5bWVudCcgfSxcbiAgICAgICAgICAgIHt3aWR0aDogJzUuMzMlJywgbGFiZWw6ICdSZWZ1bmQnLCBuYW1lOiAnY2FzaF9jbGllbnRfcmVmdW5kJyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ1Byb2Nlc3MgQnVkZ2V0IFJldHVybicsIG5hbWU6ICdjYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4nIH0sXG4gICAgICAgICAgICB7d2lkdGg6ICc1LjMzJScsIGxhYmVsOiAnUHJvY2VzcyBDb3N0JywgbmFtZTogJ2Nhc2hfcHJvY2Vzc19jb3N0JyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0JvcnJvd2VkIFByb2Nlc3MgQ29zdCcsIG5hbWU6ICdib3Jyb3dlZF9wcm9jZXNzX2Nvc3QnIH0sXG4gICAgICAgICAgICB7d2lkdGg6ICc1LjMzJScsIGxhYmVsOiAnQWRtaW4gQnVkZ2V0IFJldHVybicsIG5hbWU6ICdjYXNoX2FkbWluX2J1ZGdldF9yZXR1cm4nIH0sXG4gICAgICAgICAgICB7d2lkdGg6ICc1LjMzJScsIGxhYmVsOiAnQm9ycm93ZWQgQWRtaW4gQ29zdCcsIG5hbWU6ICdib3Jyb3dlZF9hZG1pbl9jb3N0JyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0FkbWluIENvc3QnLCBuYW1lOiAnY2FzaF9hZG1pbl9jb3N0JyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0JhbmsgRGVwb3NpdC9QYXltZW50JywgbmFtZTogJ2JhbmtfY2xpZW50X2RlcG9fcGF5bWVudCcgfSxcbiAgICAgICAgICAgIHt3aWR0aDogJzUuMzMlJywgbGFiZWw6ICdCYW5rIENvc3QnLCBuYW1lOiAnYmFua19jb3N0JyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0RlcG9zaXQgT3RoZXIgTWF0dGVyJywgbmFtZTogJ2RlcG9zaXRfb3RoZXInIH0sXG4gICAgICAgICAgICB7d2lkdGg6ICc1LjMzJScsIGxhYmVsOiAnQ29zdCBPdGhlciBNYXR0ZXInLCBuYW1lOiAnY29zdF9vdGhlcicgfSxcbiAgICAgICAgICAgIHt3aWR0aDogJzUuMzMlJywgbGFiZWw6ICdQcm9jZXNzIE91dHB1dCcsIG5hbWU6ICdwcm9jZXNzX291dHB1dCcgfSxcbiAgICAgICAgICAgIHt3aWR0aDogJzUuMzMlJywgbGFiZWw6ICdIb3cgbXVjaCB3ZSByZWNlaXZlZCBmcm9tIGNsaWVudCcsIG5hbWU6ICdhbW91bnRfYmVmb3JlJyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0hvdyBtdWNoIHdlIHJlY2VpdmVkIGFmdGVyIHdlIHNwZW50IGZvciBjbGllbnQnLCBuYW1lOiAnYW1vdW50X2FmdGVyJyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0FkbWluIFRvdGFsJywgbmFtZTogJ2FkbWluX3RvdGFsJyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0Nhc2ggQmFsYW5jZScsIG5hbWU6ICdjYXNoX2JhbGFuY2UnIH0sXG4gICAgICAgICAgICB7d2lkdGg6ICc1LjMzJScsIGxhYmVsOiAnQmFuayBCYWxhbmNlJywgbmFtZTogJ2JhbmtfYmFsYW5jZScgfSxcbiAgICAgICAgICAgIHt3aWR0aDogJzUuMzMlJywgbGFiZWw6ICdUb3RhbCcsIG5hbWU6ICd0b3RhbCcgfSxcbiAgICAgICAgICAgIHt3aWR0aDogJzUuMzMlJywgbGFiZWw6ICdQcm9maXQnLCBuYW1lOiAncHJvZml0JyB9LFxuICAgICAgICAgICAge3dpZHRoOiAnNS4zMyUnLCBsYWJlbDogJ0NoZWNrcycsIG5hbWU6ICdwb3N0ZGF0ZWRfY2hlY2tzJyB9LFxuICAgICAgICBdO1xuICAgICAgICBjb2x1bW5zLmZvckVhY2goKGNvbHVtbikgPT4ge1xuICAgICAgICAgICBzb3J0T3JkZXJzW2NvbHVtbi5uYW1lXSA9IC0xO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGNhdF90eXBlOicnLFxuICAgICAgICAgICAgYm9ycm93ZWQ6W10sXG4gICAgICAgICAgICBzZWxlY3RlZEZpbGU6IG51bGwsXG4gICAgICAgICAgICBpbml0aWFsX2Nhc2g6MCxcbiAgICAgICAgICAgIGluaXRpYWxfYmFuazowLFxuICAgICAgICAgICAgY2FzaF9iYWxhbmNlOjAsXG4gICAgICAgICAgICBiYW5rX2JhbGFuY2U6MCxcbiAgICAgICAgICAgIHRvdGFsX2JhbGFuY2U6MCxcbiAgICAgICAgICAgIHRvdGFsX3Byb2ZpdDowLFxuICAgICAgICAgICAgcHJvY2Vzc19vdXRwdXQ6MCxcbiAgICAgICAgICAgIHdlX3JlY2VpdmVfYmVmb3JlOjAsXG4gICAgICAgICAgICB3ZV9yZWNlaXZlX2FmdGVyOjAsXG4gICAgICAgICAgICBmaW5hbmNlOiB7XG4gICAgICAgIFx0XHRcdHRyYW5zX2Rlc2M6ICcnLFxuICAgICAgICBcdFx0XHRjYXRfdHlwZTogJycsXG4gICAgICAgIFx0XHRcdGNhdF9zdG9yYWdlOiAnJyxcbiAgICAgICAgXHRcdFx0Y2FzaF9jbGllbnRfZGVwb19wYXltZW50OiAnJyxcbiAgICAgICAgICAgICAgY2FzaF9jbGllbnRfcmVmdW5kOiAnJyxcbiAgICAgICAgICAgICAgY2FzaF9jbGllbnRfcHJvY2Vzc19idWRnZXRfcmV0dXJuOiAnJyxcbiAgICAgICAgICAgICAgY2FzaF9wcm9jZXNzX2Nvc3Q6ICcnLFxuICAgICAgICAgICAgICBib3Jyb3dlZF9wcm9jZXNzX2Nvc3Q6ICcnLFxuICAgICAgICAgICAgICBjYXNoX2FkbWluX2J1ZGdldF9yZXR1cm46ICcnLFxuICAgICAgICAgICAgICBib3Jyb3dlZF9hZG1pbl9jb3N0OiAnJyxcbiAgICAgICAgICAgICAgY2FzaF9hZG1pbl9jb3N0OiAnJyxcbiAgICAgICAgICAgICAgYmFua19jbGllbnRfZGVwb19wYXltZW50OicnLFxuICAgICAgICAgICAgICBiYW5rX2Nvc3Q6JycsXG4gICAgICAgICAgICAgIGNhc2hfYmFsYW5jZTonJyxcbiAgICAgICAgICAgICAgYmFua19iYWxhbmNlOicnLFxuICAgICAgICAgICAgICBwb3N0ZGF0ZWRfY2hlY2tzOicnLFxuICAgICAgICAgICAgICBjYXNoX3RoZXI6JycsXG4gICAgICAgICAgICAgIGRlcG9zaXRfb3RoZXI6ICcnXG4gICAgICAgIFx0XHR9LFxuICAgICAgICBcdFx0c3VjY2VzczogZmFsc2UsXG4gICAgICAgIFx0XHRlZGl0OiBmYWxzZSxcbiAgICAgICAgICAgIHRtcEhlYWRlcjonJyxcbiAgICAgICAgICAgIHRtcENvbnRlbnQ6ICcnLFxuICAgICAgICAgICAgdG1wSHJlZjpmYWxzZSxcbiAgICAgICAgICAgIHByb2plY3RzOiBbXSxcbiAgICAgICAgICAgIHByb2plY3RzMjpbXSxcbiAgICAgICAgICAgIGNvbHVtbnM6IGNvbHVtbnMsXG4gICAgICAgICAgICBzb3J0S2V5OiAnaWQnLFxuICAgICAgICAgICAgc29ydE9yZGVyczogc29ydE9yZGVycyxcbiAgICAgICAgICAgIHBlclBhZ2U6IFsnMTAwMDAwJywgJzIwMDAwMCcsICczMDAwMDAnXSxcbiAgICAgICAgICAgIHRhYmxlRGF0YToge1xuICAgICAgICAgICAgICAgIGRyYXc6IDAsXG4gICAgICAgICAgICAgICAgbGVuZ3RoOiAxMDAwMDAsXG4gICAgICAgICAgICAgICAgc2VhcmNoOiAnJyxcbiAgICAgICAgICAgICAgICBjb2x1bW46IDAsXG4gICAgICAgICAgICAgICAgZGlyOiAnZGVzYycsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgcGFnaW5hdGlvbjoge1xuICAgICAgICAgICAgICAgIGxhc3RQYWdlOiAnJyxcbiAgICAgICAgICAgICAgICBjdXJyZW50UGFnZTogJycsXG4gICAgICAgICAgICAgICAgdG90YWw6ICcnLFxuICAgICAgICAgICAgICAgIGxhc3RQYWdlVXJsOiAnJyxcbiAgICAgICAgICAgICAgICBuZXh0UGFnZVVybDogJycsXG4gICAgICAgICAgICAgICAgcHJldlBhZ2VVcmw6ICcnLFxuICAgICAgICAgICAgICAgIGZyb206ICcnLFxuICAgICAgICAgICAgICAgIHRvOiAnJ1xuICAgICAgICAgICAgfSxcblxuICAgICAgICB9XG4gICAgfSxcbiAgICBtZXRob2RzOiB7XG4gICAgICAgIG51bWJlcldpdGhDb21tYXMoeCkge1xuICAgICAgICAgIGlmKHg9PScnIHx8IHg9PW51bGwpe3JldHVybiAnJzt9XG5cbiAgICAgICAgICByZXR1cm4geC50b1N0cmluZygpLnJlcGxhY2UoL1xcQig/PShcXGR7M30pKyg/IVxcZCkpL2csIFwiLFwiKTtcblxuICAgICAgICB9LFxuICAgICAgICBjaGVja1RyYW5zKGV2ZW50KXtcbiAgICAgICAgICAvL2FsZXJ0KGV2ZW50LnRhcmdldC5pZCk7XG4gICAgICAgICAgdGhpcy5jYXRfdHlwZSA9IGV2ZW50LnRhcmdldC52YWx1ZTtcbiAgICAgICAgICB0aGlzLmJvcnJvd2VkID0gW107XG4gICAgICAgICAgICBpZihldmVudC50YXJnZXQudmFsdWUhPSdicGMnICYmIGV2ZW50LnRhcmdldC52YWx1ZSE9J2JhYycpe1xuICAgICAgICAgICAgYXhpb3MuZ2V0KCcuL25maW5hbmNpbmcvZ2V0LWJvcnJvd2VkJyx7XG4gICAgICAgICAgICAgIHBhcmFtczp7XG4gICAgICAgICAgICAgICAgY2F0X3R5cGU6IGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlLmRhdGEpO1xuICAgICAgICAgICAgICAgIHRoaXMuYm9ycm93ZWQgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGdldFByb2plY3RzKHVybCA9ICcuL25maW5hbmNpbmcvZ2V0LWZpbmFuY2UnKSB7XG4gICAgICAgICAgICB0aGlzLnByb2plY3RzMiA9IFtdO1xuICAgICAgICAgICAgdmFyIGluaXRpYWxfYmFuayA9IDAsIGluaXRpYWxfY2FzaCA9IDAsIHByb2Nlc3N0ID0gMCwgdGNsaWVudF9kZXAgPSAwLHRjbGllbnRfZGVwMiA9IDAsIGFkbWluX3RvdGFsPTAsIGNhc2hfdG90YWxfYmFsYW5jZSA9IDAsYmFua190b3RhbF9iYWxhbmNlID0gMCwgdG90YWwgPSAwLCBwcm9maXQgPSAwO1xuICAgICAgICAgICAgdGhpcy50YWJsZURhdGEuZHJhdysrO1xuICAgICAgICAgICAgYXhpb3MuZ2V0KHVybCwge3BhcmFtczogdGhpcy50YWJsZURhdGF9KVxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGRhdGEgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50YWJsZURhdGEuZHJhdyA9PSBkYXRhLmRyYXcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdHMgPSBkYXRhLmRhdGEuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29uZmlnUGFnaW5hdGlvbihkYXRhLmRhdGEpO1xuXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBmaW5hbmNlMj17fTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcnI9W107XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2codGhpcy5wcm9qZWN0cyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJC5lYWNoKHRoaXMucHJvamVjdHMucmV2ZXJzZSgpLCBmdW5jdGlvbigga2V5LCB2YWx1ZSApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjb3N0ID0gKCh2YWx1ZS5jYXNoX3Byb2Nlc3NfY29zdCAhPSBudWxsICYmIHZhbHVlLmNhc2hfcHJvY2Vzc19jb3N0ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX3Byb2Nlc3NfY29zdCk6IDApKyh2YWx1ZS5ib3Jyb3dlZF9wcm9jZXNzX2Nvc3QgIT0gbnVsbCAmJiB2YWx1ZS5ib3Jyb3dlZF9wcm9jZXNzX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmJvcnJvd2VkX3Byb2Nlc3NfY29zdCk6IDApLSh2YWx1ZS5jYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4gIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4gIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfY2xpZW50X3Byb2Nlc3NfYnVkZ2V0X3JldHVybik6IDApKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjbGllbnRfZGVwID0gKCgodmFsdWUuYm9ycm93ZWRfcHJvY2Vzc19jb3N0ICE9IG51bGwgJiYgdmFsdWUuYm9ycm93ZWRfcHJvY2Vzc19jb3N0ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5ib3Jyb3dlZF9wcm9jZXNzX2Nvc3QpOiAwKSsodmFsdWUuY2FzaF9jbGllbnRfZGVwb19wYXltZW50ICE9IG51bGwgJiYgdmFsdWUuY2FzaF9jbGllbnRfZGVwb19wYXltZW50ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX2NsaWVudF9kZXBvX3BheW1lbnQpOiAwKSsodmFsdWUuYmFua19jbGllbnRfZGVwb19wYXltZW50ICE9IG51bGwgJiYgdmFsdWUuYmFua19jbGllbnRfZGVwb19wYXltZW50ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQpOiAwKSktKHZhbHVlLmNhc2hfY2xpZW50X3JlZnVuZCAhPSBudWxsICYmIHZhbHVlLmNhc2hfY2xpZW50X3JlZnVuZCAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9jbGllbnRfcmVmdW5kKTogMCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNsaWVudF9kZXBfYWZ0ZXIgPSAoKCh2YWx1ZS5jYXNoX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfY2xpZW50X2RlcG9fcGF5bWVudCk6IDApKyh2YWx1ZS5jYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4gIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4gIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfY2xpZW50X3Byb2Nlc3NfYnVkZ2V0X3JldHVybik6IDApKyh2YWx1ZS5ib3Jyb3dlZF9wcm9jZXNzX2Nvc3QgIT0gbnVsbCAmJiB2YWx1ZS5ib3Jyb3dlZF9wcm9jZXNzX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmJvcnJvd2VkX3Byb2Nlc3NfY29zdCk6IDApKyh2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gbnVsbCAmJiB2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmJhbmtfY2xpZW50X2RlcG9fcGF5bWVudCk6IDApKS0oKHZhbHVlLmNhc2hfY2xpZW50X3JlZnVuZCAhPSBudWxsICYmIHZhbHVlLmNhc2hfY2xpZW50X3JlZnVuZCAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9jbGllbnRfcmVmdW5kKTogMCkrKHZhbHVlLmNhc2hfcHJvY2Vzc19jb3N0ICE9IG51bGwgJiYgdmFsdWUuY2FzaF9wcm9jZXNzX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfcHJvY2Vzc19jb3N0KTogMCkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhZG1pbl90ID0gKCh2YWx1ZS5jYXNoX2FkbWluX2Nvc3QgIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2FkbWluX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfYWRtaW5fY29zdCk6IDApLSh2YWx1ZS5jYXNoX2FkbWluX2J1ZGdldF9yZXR1cm4gIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2FkbWluX2J1ZGdldF9yZXR1cm4gIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybik6IDApKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0bXBjYXNoX2JhbGFuY2UgPSAoKHZhbHVlLmNhc2hfYmFsYW5jZSAhPSBudWxsICYmIHZhbHVlLmNhc2hfYmFsYW5jZSAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9iYWxhbmNlKTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5jYXNoX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfY2xpZW50X2RlcG9fcGF5bWVudCk6IDApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuY2FzaF9jbGllbnRfcHJvY2Vzc19idWRnZXRfcmV0dXJuICE9IG51bGwgJiYgdmFsdWUuY2FzaF9jbGllbnRfcHJvY2Vzc19idWRnZXRfcmV0dXJuICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4pOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybiAhPSBudWxsICYmIHZhbHVlLmNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybiAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9hZG1pbl9idWRnZXRfcmV0dXJuKTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5kZXBvc2l0X290aGVyICE9IG51bGwgJiYgdmFsdWUuZGVwb3NpdF9vdGhlciAhPSAnJyAmJiB2YWx1ZS5jYXRfc3RvcmFnZSA9PSAnQ2FzaCcgPyBwYXJzZUZsb2F0KHZhbHVlLmRlcG9zaXRfb3RoZXIpOiAwKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLSgodmFsdWUuY2FzaF9jbGllbnRfcmVmdW5kICE9IG51bGwgJiYgdmFsdWUuY2FzaF9jbGllbnRfcmVmdW5kICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX2NsaWVudF9yZWZ1bmQpOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmNhc2hfcHJvY2Vzc19jb3N0ICE9IG51bGwgJiYgdmFsdWUuY2FzaF9wcm9jZXNzX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfcHJvY2Vzc19jb3N0KTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5jYXNoX2FkbWluX2Nvc3QgIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2FkbWluX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfYWRtaW5fY29zdCk6IDApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuYm9ycm93ZWRfYWRtaW5fY29zdCAhPSBudWxsICYmIHZhbHVlLmJvcnJvd2VkX2FkbWluX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmJvcnJvd2VkX2FkbWluX2Nvc3QpOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmJvcnJvd2VkX3Byb2Nlc3NfY29zdCAhPSBudWxsICYmIHZhbHVlLmJvcnJvd2VkX3Byb2Nlc3NfY29zdCAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuYm9ycm93ZWRfcHJvY2Vzc19jb3N0KTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5jb3N0X290aGVyICE9IG51bGwgJiYgdmFsdWUuY29zdF9vdGhlciAhPSAnJyAmJiB2YWx1ZS5jYXRfc3RvcmFnZSA9PSAnQ2FzaCcgPyBwYXJzZUZsb2F0KHZhbHVlLmNvc3Rfb3RoZXIpOiAwKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0bXBiYW5rX2JhbGFuY2UgPSAoKHZhbHVlLmJhbmtfYmFsYW5jZSAhPSBudWxsICYmIHZhbHVlLmJhbmtfYmFsYW5jZSAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuYmFua19iYWxhbmNlKTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuYmFua19jbGllbnRfZGVwb19wYXltZW50ICE9IG51bGwgJiYgdmFsdWUuYmFua19jbGllbnRfZGVwb19wYXltZW50ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQpOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5kZXBvc2l0X290aGVyICE9IG51bGwgJiYgdmFsdWUuZGVwb3NpdF9vdGhlciAhPSAnJyAmJiB2YWx1ZS5jYXRfc3RvcmFnZSA9PSAnQmFuaycgPyBwYXJzZUZsb2F0KHZhbHVlLmRlcG9zaXRfb3RoZXIpOiAwKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC0oKHZhbHVlLmJhbmtfY29zdCAhPSBudWxsICYmIHZhbHVlLmJhbmtfY29zdCAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuYmFua19jb3N0KTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuY29zdF9vdGhlciAhPSBudWxsICYmIHZhbHVlLmNvc3Rfb3RoZXIgIT0gJycgJiYgdmFsdWUuY2F0X3N0b3JhZ2UgPT0gJ0JhbmsnID8gcGFyc2VGbG9hdCh2YWx1ZS5jb3N0X290aGVyKTogMCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG1wdG90YWwgPSAoKHZhbHVlLmNhc2hfYmFsYW5jZSAhPSBudWxsICYmIHZhbHVlLmNhc2hfYmFsYW5jZSAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9iYWxhbmNlKTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuY2FzaF9jbGllbnRfZGVwb19wYXltZW50ICE9IG51bGwgJiYgdmFsdWUuY2FzaF9jbGllbnRfZGVwb19wYXltZW50ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX2NsaWVudF9kZXBvX3BheW1lbnQpOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5jYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4gIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4gIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfY2xpZW50X3Byb2Nlc3NfYnVkZ2V0X3JldHVybik6IDApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybiAhPSBudWxsICYmIHZhbHVlLmNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybiAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9hZG1pbl9idWRnZXRfcmV0dXJuKTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuYmFua19iYWxhbmNlICE9IG51bGwgJiYgdmFsdWUuYmFua19iYWxhbmNlICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5iYW5rX2JhbGFuY2UpOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gbnVsbCAmJiB2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmJhbmtfY2xpZW50X2RlcG9fcGF5bWVudCk6IDApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmRlcG9zaXRfb3RoZXIgIT0gbnVsbCAmJiB2YWx1ZS5kZXBvc2l0X290aGVyICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5kZXBvc2l0X290aGVyKTogMCkpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAtKCh2YWx1ZS5jYXNoX2NsaWVudF9yZWZ1bmQgIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2NsaWVudF9yZWZ1bmQgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfY2xpZW50X3JlZnVuZCk6IDApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmNhc2hfcHJvY2Vzc19jb3N0ICE9IG51bGwgJiYgdmFsdWUuY2FzaF9wcm9jZXNzX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfcHJvY2Vzc19jb3N0KTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuY2FzaF9hZG1pbl9jb3N0ICE9IG51bGwgJiYgdmFsdWUuY2FzaF9hZG1pbl9jb3N0ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX2FkbWluX2Nvc3QpOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5ib3Jyb3dlZF9hZG1pbl9jb3N0ICE9IG51bGwgJiYgdmFsdWUuYm9ycm93ZWRfYWRtaW5fY29zdCAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuYm9ycm93ZWRfYWRtaW5fY29zdCk6IDApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmJvcnJvd2VkX3Byb2Nlc3NfY29zdCAhPSBudWxsICYmIHZhbHVlLmJvcnJvd2VkX3Byb2Nlc3NfY29zdCAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuYm9ycm93ZWRfcHJvY2Vzc19jb3N0KTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuY29zdF9vdGhlciAhPSBudWxsICYmIHZhbHVlLmNvc3Rfb3RoZXIgIT0gJycgID8gcGFyc2VGbG9hdCh2YWx1ZS5jb3N0X290aGVyKTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuYmFua19jb3N0ICE9IG51bGwgJiYgdmFsdWUuYmFua19jb3N0ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5iYW5rX2Nvc3QpOiAwKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0bXBwcm9maXQgPSAoKHZhbHVlLmNhc2hfY2xpZW50X2RlcG9fcGF5bWVudCAhPSBudWxsICYmIHZhbHVlLmNhc2hfY2xpZW50X2RlcG9fcGF5bWVudCAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9jbGllbnRfZGVwb19wYXltZW50KTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuY2FzaF9jbGllbnRfcHJvY2Vzc19idWRnZXRfcmV0dXJuICE9IG51bGwgJiYgdmFsdWUuY2FzaF9jbGllbnRfcHJvY2Vzc19idWRnZXRfcmV0dXJuICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm4pOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5ib3Jyb3dlZF9wcm9jZXNzX2Nvc3QgIT0gbnVsbCAmJiB2YWx1ZS5ib3Jyb3dlZF9wcm9jZXNzX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmJvcnJvd2VkX3Byb2Nlc3NfY29zdCk6IDApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybiAhPSBudWxsICYmIHZhbHVlLmNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybiAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9hZG1pbl9idWRnZXRfcmV0dXJuKTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuYm9ycm93ZWRfYWRtaW5fY29zdCAhPSBudWxsICYmIHZhbHVlLmJvcnJvd2VkX2FkbWluX2Nvc3QgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmJvcnJvd2VkX2FkbWluX2Nvc3QpOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gbnVsbCAmJiB2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmJhbmtfY2xpZW50X2RlcG9fcGF5bWVudCk6IDApKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLSgodmFsdWUuY2FzaF9jbGllbnRfcmVmdW5kICE9IG51bGwgJiYgdmFsdWUuY2FzaF9jbGllbnRfcmVmdW5kICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX2NsaWVudF9yZWZ1bmQpOiAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKyh2YWx1ZS5jYXNoX3Byb2Nlc3NfY29zdCAhPSBudWxsICYmIHZhbHVlLmNhc2hfcHJvY2Vzc19jb3N0ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5jYXNoX3Byb2Nlc3NfY29zdCk6IDApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICArKHZhbHVlLmNhc2hfYWRtaW5fY29zdCAhPSBudWxsICYmIHZhbHVlLmNhc2hfYWRtaW5fY29zdCAhPSAnJyA/IHBhcnNlRmxvYXQodmFsdWUuY2FzaF9hZG1pbl9jb3N0KTogMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsodmFsdWUuYmFua19jb3N0ICE9IG51bGwgJiYgdmFsdWUuYmFua19jb3N0ICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5iYW5rX2Nvc3QpOiAwKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICBpbml0aWFsX2Nhc2grPSh2YWx1ZS5jYXNoX2JhbGFuY2UgIT0gbnVsbCAmJiB2YWx1ZS5jYXNoX2JhbGFuY2UgIT0gJycgPyBwYXJzZUZsb2F0KHZhbHVlLmNhc2hfYmFsYW5jZSk6IDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5pdGlhbF9iYW5rKz0odmFsdWUuYmFua19iYWxhbmNlICE9IG51bGwgJiYgdmFsdWUuYmFua19iYWxhbmNlICE9ICcnID8gcGFyc2VGbG9hdCh2YWx1ZS5iYW5rX2JhbGFuY2UpOiAwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3N0ID0gcHJvY2Vzc3QgKyBjb3N0OyAvL3Byb2Nlc3MgY29zdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGNsaWVudF9kZXAgPSB0Y2xpZW50X2RlcCArIGNsaWVudF9kZXA7IC8vaG93IG11Y2ggd2UgaGF2ZSBiZWZvcmVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRjbGllbnRfZGVwMiA9IHRjbGllbnRfZGVwMiArIGNsaWVudF9kZXBfYWZ0ZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICBhZG1pbl90b3RhbCA9IGFkbWluX3RvdGFsICsgYWRtaW5fdDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2hfdG90YWxfYmFsYW5jZSA9IGNhc2hfdG90YWxfYmFsYW5jZSArIHRtcGNhc2hfYmFsYW5jZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhbmtfdG90YWxfYmFsYW5jZSA9IGJhbmtfdG90YWxfYmFsYW5jZSArIHRtcGJhbmtfYmFsYW5jZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsID0gdG90YWwgKyB0bXB0b3RhbDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2ZpdCA9IHByb2ZpdCArIHRtcHByb2ZpdDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbmFuY2UyPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6dmFsdWUuaWQsXG4gICAgICAgICAgICAgICAgICAgICAgIFx0XHRcdHRyYW5zX2Rlc2M6IHZhbHVlLnRyYW5zX2Rlc2MsXG4gICAgICAgICAgICAgICAgICAgICAgIFx0XHRcdGNhdF90eXBlOiB2YWx1ZS5jYXRfdHlwZSxcbiAgICAgICAgICAgICAgICAgICAgICAgXHRcdFx0Y2F0X3N0b3JhZ2U6IHZhbHVlLmNhdF9zdG9yYWdlLFxuICAgICAgICAgICAgICAgICAgICAgICBcdFx0XHRjYXNoX2NsaWVudF9kZXBvX3BheW1lbnQ6IHZhbHVlLmNhc2hfY2xpZW50X2RlcG9fcGF5bWVudCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzaF9jbGllbnRfcmVmdW5kOiB2YWx1ZS5jYXNoX2NsaWVudF9yZWZ1bmQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2hfY2xpZW50X3Byb2Nlc3NfYnVkZ2V0X3JldHVybjogdmFsdWUuY2FzaF9jbGllbnRfcHJvY2Vzc19idWRnZXRfcmV0dXJuLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNoX3Byb2Nlc3NfY29zdDogdmFsdWUuY2FzaF9wcm9jZXNzX2Nvc3QsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcnJvd2VkX3Byb2Nlc3NfY29zdDogdmFsdWUuYm9ycm93ZWRfcHJvY2Vzc19jb3N0LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNoX2FkbWluX2J1ZGdldF9yZXR1cm46IHZhbHVlLmNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9ycm93ZWRfYWRtaW5fY29zdDogdmFsdWUuYm9ycm93ZWRfYWRtaW5fY29zdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzaF9hZG1pbl9jb3N0OiB2YWx1ZS5jYXNoX2FkbWluX2Nvc3QsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhbmtfY2xpZW50X2RlcG9fcGF5bWVudDp2YWx1ZS5iYW5rX2NsaWVudF9kZXBvX3BheW1lbnQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhbmtfY29zdDp2YWx1ZS5iYW5rX2Nvc3QsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2hfYmFsYW5jZTpjYXNoX3RvdGFsX2JhbGFuY2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhbmtfYmFsYW5jZTpiYW5rX3RvdGFsX2JhbGFuY2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc3RkYXRlZF9jaGVja3M6dmFsdWUucG9zdGRhdGVkX2NoZWNrcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29zdF9vdGhlcjogdmFsdWUuY29zdF9vdGhlcixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVwb3NpdF9vdGhlcjogdmFsdWUuZGVwb3NpdF9vdGhlcixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG90YWxfcHJvY2Vzczpwcm9jZXNzdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2VfaGF2ZV9iZWZvcmU6dGNsaWVudF9kZXAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdlX2hhdmVfYWZ0ZXI6dGNsaWVudF9kZXAyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhZG1pbl90b3RhbDogYWRtaW5fdG90YWwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsOiB0b3RhbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZml0OnByb2ZpdFxuICAgICAgICAgICAgICAgICAgICAgICBcdFx0fTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgYXJyLnB1c2goZmluYW5jZTIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgIGlmKHRoaXMudGFibGVEYXRhLnNlYXJjaD09Jycpe1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMud2VfcmVjZWl2ZV9iZWZvcmUgPSB0Y2xpZW50X2RlcDtcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLndlX3JlY2VpdmVfYWZ0ZXIgPSB0Y2xpZW50X2RlcDI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYXNoX2JhbGFuY2UgPSBjYXNoX3RvdGFsX2JhbGFuY2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iYW5rX2JhbGFuY2UgPSBiYW5rX3RvdGFsX2JhbGFuY2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbml0aWFsX2Nhc2ggPSBpbml0aWFsX2Nhc2g7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbml0aWFsX2JhbmsgPSBpbml0aWFsX2Jhbms7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50b3RhbF9wcm9maXQgPSBwcm9maXQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50b3RhbF9iYWxhbmNlID0gdG90YWw7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9jZXNzX291dHB1dCA9IHByb2Nlc3N0O1xuICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0czIgPSBhcnIucmV2ZXJzZSgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goZXJyb3JzID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3JzKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcbiAgICAgICAgc2F2ZVByb2plY3QoKXtcbiAgICAgICAgLy8gIGNvbnNvbGUubG9nKGpRdWVyeS50cmltKCQoJyN0eHRBbW91bnQnKS52YWwoKSkpO1xuICAgICAgICAgIGlmKGpRdWVyeS50cmltKCQoJyN0eHRBbW91bnQnKS52YWwoKSkubGVuZ3RoICA9PSAwKXtcbiAgICAgICAgICAgIHN3YWwoe1xuICAgICAgICAgICAgICAgIHRpdGxlOiBcIldhcm5pbmdcIixcbiAgICAgICAgICAgICAgICB0ZXh0OiBcIlBsZWFzZSBpbnNlcnQgYW1vdW50XCJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1lbHNlIGlmKGpRdWVyeS50cmltKCQoJyN0eHREZXNjcmlwdGlvbicpLnZhbCgpKS5sZW5ndGggPT0gMCAmJiAodGhpcy5jYXRfdHlwZT09J2JwYycgfHwgdGhpcy5jYXRfdHlwZT09J2JhYycpKXtcbiAgICAgICAgICAgIHN3YWwoe1xuICAgICAgICAgICAgICAgIHRpdGxlOiBcIldhcm5pbmdcIixcbiAgICAgICAgICAgICAgICB0ZXh0OiBcIlBsZWFzZSBpbnB1dCBkZXNjcmlwdGlvblwiXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgIH1lbHNlIGlmKCEkLmlzTnVtZXJpYygkKCcjdHh0QW1vdW50JykudmFsKCkpKXtcbiAgICAgICAgICAgIHN3YWwoe1xuICAgICAgICAgICAgICAgIHRpdGxlOiBcIldhcm5pbmdcIixcbiAgICAgICAgICAgICAgICB0ZXh0OiBcIkFtb3VudCBtdXN0IGJlIG51bWJlciBvbmx5XCJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgaWYoZXZlbnQudGFyZ2V0LnZhbHVlPT0nYnBjJyB8fCBldmVudC50YXJnZXQudmFsdWU9PSdiYWMnKXtcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coJChcIiN0cmFuc2FjdGlvbl90eXBlXCIpLnZhbCgpKTtcbiAgICAgICAgICAgIHRoaXMuZmluYW5jZT0ge1xuICAgICAgICBcdFx0XHR0cmFuc19kZXNjOiAkKCcjdHh0RGVzY3JpcHRpb24nKS52YWwoKSxcbiAgICAgICAgXHRcdFx0Y2F0X3R5cGU6ICgoJChcIiN0cmFuc2FjdGlvbl90eXBlXCIpLnZhbCgpPT0ncGJyJyB8fCAkKFwiI3RyYW5zYWN0aW9uX3R5cGVcIikudmFsKCk9PSdwYycgfHwgJChcIiN0cmFuc2FjdGlvbl90eXBlXCIpLnZhbCgpPT0nYnBjJykgPyAnUHJvY2VzcycgOiAnQWRtaW4nKSxcbiAgICAgICAgXHRcdFx0Y2F0X3N0b3JhZ2U6ICQoXCIjc3RvcmFnZV90eXBlXCIpLnZhbCgpLFxuICAgICAgICBcdFx0XHRjYXNoX2NsaWVudF9kZXBvX3BheW1lbnQ6ICcnLFxuICAgICAgICAgICAgICBjYXNoX2NsaWVudF9yZWZ1bmQ6ICcnLFxuICAgICAgICAgICAgICBjYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm46ICgoJChcIiN0cmFuc2FjdGlvbl90eXBlXCIpLnZhbCgpPT0ncGJyJykgPyAkKCcjdHh0QW1vdW50JykudmFsKCkgOiAnJyksXG4gICAgICAgICAgICAgIGNhc2hfcHJvY2Vzc19jb3N0OiAoKCQoXCIjdHJhbnNhY3Rpb25fdHlwZVwiKS52YWwoKT09J3BjJykgPyAkKCcjdHh0QW1vdW50JykudmFsKCkgOiAnJyksXG4gICAgICAgICAgICAgIGJvcnJvd2VkX3Byb2Nlc3NfY29zdDogKCgkKFwiI3RyYW5zYWN0aW9uX3R5cGVcIikudmFsKCk9PSdicGMnKSA/ICQoJyN0eHRBbW91bnQnKS52YWwoKSA6ICcnKSxcbiAgICAgICAgICAgICAgY2FzaF9hZG1pbl9idWRnZXRfcmV0dXJuOiAoKCQoXCIjdHJhbnNhY3Rpb25fdHlwZVwiKS52YWwoKT09J2FicicpID8gJCgnI3R4dEFtb3VudCcpLnZhbCgpIDogJycpLFxuICAgICAgICAgICAgICBib3Jyb3dlZF9hZG1pbl9jb3N0OiAoKCQoXCIjdHJhbnNhY3Rpb25fdHlwZVwiKS52YWwoKT09J2JhYycpID8gJCgnI3R4dEFtb3VudCcpLnZhbCgpIDogJycpLFxuICAgICAgICAgICAgICBjYXNoX2FkbWluX2Nvc3Q6ICgoJChcIiN0cmFuc2FjdGlvbl90eXBlXCIpLnZhbCgpPT0nYWMnKSA/ICQoJyN0eHRBbW91bnQnKS52YWwoKSA6ICcnKSxcbiAgICAgICAgICAgICAgYmFua19jbGllbnRfZGVwb19wYXltZW50OicnLFxuICAgICAgICAgICAgICBiYW5rX2Nvc3Q6JycsXG4gICAgICAgICAgICAgIGNhc2hfYmFsYW5jZTonJyxcbiAgICAgICAgICAgICAgYmFua19iYWxhbmNlOicnLFxuICAgICAgICAgICAgICBwb3N0ZGF0ZWRfY2hlY2tzOicnXG4gICAgICAgIFx0XHR9O1xuICAgICAgICAgICAgdGhpcy4kaHR0cC5wb3N0KCcuL25maW5hbmNpbmcvZ2V0LWZpbmFuY2UnLCB0aGlzLmZpbmFuY2UpLmNhdGNoKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgaWYoZXJyb3Iuc3RhdHVzPT00MjIpe1xuXG4gICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICB0aGlzLmZpbmFuY2U9IHtcbiAgICAgICAgICAgICAgICB0cmFuc19kZXNjOiAnJyxcbiAgICAgICAgICBcdFx0XHRjYXRfdHlwZTogJycsXG4gICAgICAgICAgXHRcdFx0Y2F0X3N0b3JhZ2U6ICcnLFxuICAgICAgICAgIFx0XHRcdGNhc2hfY2xpZW50X2RlcG9fcGF5bWVudDogJycsXG4gICAgICAgICAgICAgICAgY2FzaF9jbGllbnRfcmVmdW5kOiAnJyxcbiAgICAgICAgICAgICAgICBjYXNoX2NsaWVudF9wcm9jZXNzX2J1ZGdldF9yZXR1cm46ICcnLFxuICAgICAgICAgICAgICAgIGNhc2hfcHJvY2Vzc19jb3N0OiAnJyxcbiAgICAgICAgICAgICAgICBib3Jyb3dlZF9wcm9jZXNzX2Nvc3Q6ICcnLFxuICAgICAgICAgICAgICAgIGNhc2hfYWRtaW5fYnVkZ2V0X3JldHVybjogJycsXG4gICAgICAgICAgICAgICAgYm9ycm93ZWRfYWRtaW5fY29zdDogJycsXG4gICAgICAgICAgICAgICAgY2FzaF9hZG1pbl9jb3N0OiAnJyxcbiAgICAgICAgICAgICAgICBiYW5rX2NsaWVudF9kZXBvX3BheW1lbnQ6JycsXG4gICAgICAgICAgICAgICAgYmFua19jb3N0OicnLFxuICAgICAgICAgICAgICAgIGNhc2hfYmFsYW5jZTonJyxcbiAgICAgICAgICAgICAgICBiYW5rX2JhbGFuY2U6JycsXG4gICAgICAgICAgICAgICAgcG9zdGRhdGVkX2NoZWNrczonJ1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAkKCcjdHh0RGVzY3JpcHRpb24nKS52YWwoJycpO1xuICAgICAgICAgICAgICAkKCcjdHh0QW1vdW50JykudmFsKCcnKTtcbiAgICAgICAgICAgICAgICAgIHN3YWwoe1xuICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIlN1Y2Nlc3NmdWwhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgdGV4dDogXCJEYXRhIGhhcyBiZWVuIHNhdmVkIVwiLFxuICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwic3VjY2Vzc1wiXG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgdGhpcy5jYXRfdHlwZSA9ICcnO1xuICAgICAgICAgICAgICB0aGlzLmJvcnJvd2VkPVtdO1xuICAgICAgICAgICAgICB0aGlzLmdldFByb2plY3RzKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIHRoaXMuJGh0dHAucG9zdCgnLi9uZmluYW5jaW5nL3VwZGF0ZS1maW5hbmNlJywge1xuICAgICAgICAgICAgICBwYXJhbXM6e1xuICAgICAgICAgICAgICAgIGNhdF90eXBlOiB0aGlzLmNhdF90eXBlLFxuICAgICAgICAgICAgICAgIGFtb3VudDogJCgnI3R4dEFtb3VudCcpLnZhbCgpLFxuICAgICAgICAgICAgICAgIGlkOiAkKCcjYm9ycm93ZWRfbGlzdCcpLnZhbCgpXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLmNhdGNoKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgaWYoZXJyb3Iuc3RhdHVzPT00MjIpe1xuXG4gICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAkKCcjdHh0RGVzY3JpcHRpb24nKS52YWwoJycpO1xuICAgICAgICAgICAgICAkKCcjdHh0QW1vdW50JykudmFsKCcnKTtcbiAgICAgICAgICAgICAgICAgIHN3YWwoe1xuICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIlN1Y2Nlc3NmdWwhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgdGV4dDogXCJEYXRhIGhhcyBiZWVuIHVwZGF0ZWQhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJzdWNjZXNzXCJcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB0aGlzLmNhdF90eXBlID0gJyc7XG4gICAgICAgICAgICAgIHRoaXMuYm9ycm93ZWQ9W107XG4gICAgICAgICAgICAgIHRoaXMuZ2V0UHJvamVjdHMoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGNvbXB1dGVQcm9jZXNzT3V0cHV0OiBmdW5jdGlvbihjb3N0LGJvcnJvd2VkLGNhc2hfcmV0dXJuKXtcbiAgICAgICAgICAgIGNvc3QgPSAoY29zdCAhPSBudWxsICYmIGNvc3QgIT0gJycgPyBjb3N0OiAwKTtcbiAgICAgICAgICAgIGJvcnJvd2VkID0gKGJvcnJvd2VkICE9IG51bGwgJiYgYm9ycm93ZWQgIT0gJycgPyBib3Jyb3dlZDogMCk7XG4gICAgICAgICAgICBjYXNoX3JldHVybiA9IChjYXNoX3JldHVybiAhPSBudWxsICYmIGNhc2hfcmV0dXJuICE9ICcnPyBjYXNoX3JldHVybjogMCk7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKGNvc3QrJy0tJytib3Jyb3dlZCsnLS0nK2Nhc2hfcmV0dXJuKTtcbiAgICAgICAgICAgIHByb2Nlc3Nfb3V0cHV0ID0gcHJvY2Vzc19vdXRwdXQrKChjb3N0K2JvcnJvd2VkKS1jYXNoX3JldHVybik7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKHByb2Nlc3Nfb3V0cHV0KTtcbiAgICAgICAgICAgIHJldHVybiAoKGNvc3QrYm9ycm93ZWQpLWNhc2hfcmV0dXJuKTtcbiAgICAgICAgfSxcbiAgICAgICAgY29uZmlnUGFnaW5hdGlvbihkYXRhKSB7XG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24ubGFzdFBhZ2UgPSBkYXRhLmxhc3RfcGFnZTtcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi5jdXJyZW50UGFnZSA9IGRhdGEuY3VycmVudF9wYWdlO1xuICAgICAgICAgICAgdGhpcy5wYWdpbmF0aW9uLnRvdGFsID0gZGF0YS50b3RhbDtcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi5sYXN0UGFnZVVybCA9IGRhdGEubGFzdF9wYWdlX3VybDtcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi5uZXh0UGFnZVVybCA9IGRhdGEubmV4dF9wYWdlX3VybDtcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi5wcmV2UGFnZVVybCA9IGRhdGEucHJldl9wYWdlX3VybDtcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi5mcm9tID0gZGF0YS5mcm9tO1xuICAgICAgICAgICAgdGhpcy5wYWdpbmF0aW9uLnRvID0gZGF0YS50bztcbiAgICAgICAgfSxcbiAgICAgICAgc29ydEJ5KGtleSkge1xuICAgICAgICAgICAgdGhpcy5zb3J0S2V5ID0ga2V5O1xuICAgICAgICAgICAgdGhpcy5zb3J0T3JkZXJzW2tleV0gPSB0aGlzLnNvcnRPcmRlcnNba2V5XSAqIC0xO1xuICAgICAgICAgICAgdGhpcy50YWJsZURhdGEuY29sdW1uID0gdGhpcy5nZXRJbmRleCh0aGlzLmNvbHVtbnMsICduYW1lJywga2V5KTtcbiAgICAgICAgICAgIHRoaXMudGFibGVEYXRhLmRpciA9IHRoaXMuc29ydE9yZGVyc1trZXldID09PSAxID8gJ2FzYycgOiAnZGVzYyc7XG4gICAgICAgICAgICB0aGlzLmdldFByb2plY3RzKCk7XG4gICAgICAgIH0sXG4gICAgICAgIGdldEluZGV4KGFycmF5LCBrZXksIHZhbHVlKSB7XG4gICAgICAgICAgICByZXR1cm4gYXJyYXkuZmluZEluZGV4KGkgPT4gaVtrZXldID09IHZhbHVlKVxuICAgICAgICB9LFxuICAgIH0sXG4gICAgY29tcHV0ZWQ6IHtcblxuICBcdH0sXG4gICAgY3JlYXRlZCgpIHtcbiAgICAgICAgdGhpcy5nZXRQcm9qZWN0cygpO1xuICAgIH0sXG4gICAgdXBkYXRlZDogZnVuY3Rpb24gKCkge1xuICAgICAgICAvL3RoaXMuZ2V0UHJvamVjdHMoKTtcblxuICAgIH1cbiAgfTtcbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFByb2plY3RzLnZ1ZT8wZGZlZjczZiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0RhdGFUYWJsZS52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTI0NmI3MDBjXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0RhdGFUYWJsZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvRGF0YVRhYmxlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIERhdGFUYWJsZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMjQ2YjcwMGNcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0yNDZiNzAwY1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvTmV3RmluYW5jaW5nL0RhdGFUYWJsZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM5N1xuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRmluYW5jaW5nLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMTYyOTk2NWZcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRmluYW5jaW5nLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL05ld0ZpbmFuY2luZy9GaW5hbmNpbmcudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRmluYW5jaW5nLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0xNjI5OTY1ZlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTE2Mjk5NjVmXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvRmluYW5jaW5nLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzk4XG4vLyBtb2R1bGUgY2h1bmtzID0gMTAiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9QYWdpbmF0aW9uLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNTRkNDZiZmNcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vUGFnaW5hdGlvbi52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvUGFnaW5hdGlvbi52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBQYWdpbmF0aW9uLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi01NGQ0NmJmY1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTU0ZDQ2YmZjXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvUGFnaW5hdGlvbi52dWVcbi8vIG1vZHVsZSBpZCA9IDM5OVxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vUHJvamVjdHMudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wMTM5YTIzY1xcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Qcm9qZWN0cy52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvUHJvamVjdHMudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gUHJvamVjdHMudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTAxMzlhMjNjXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMDEzOWEyM2NcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL05ld0ZpbmFuY2luZy9Qcm9qZWN0cy52dWVcbi8vIG1vZHVsZSBpZCA9IDQwMFxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ0ZpbmFuY2VFZGl0b3InLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiY2F0X3R5cGVcIjogX3ZtLmNhdF90eXBlLFxuICAgICAgXCJib3Jyb3dlZFwiOiBfdm0uYm9ycm93ZWQsXG4gICAgICBcImNoZWNrVHJhbnNcIjogX3ZtLmNoZWNrVHJhbnMsXG4gICAgICBcIm51bWJlcldpdGhDb21tYXNcIjogX3ZtLm51bWJlcldpdGhDb21tYXMsXG4gICAgICBcIndlX3JlY2VpdmVfYmVmb3JlXCI6IF92bS53ZV9yZWNlaXZlX2JlZm9yZSxcbiAgICAgIFwid2VfcmVjZWl2ZV9hZnRlclwiOiBfdm0ud2VfcmVjZWl2ZV9hZnRlcixcbiAgICAgIFwicHJvY2Vzc19vdXRwdXRcIjogX3ZtLnByb2Nlc3Nfb3V0cHV0LFxuICAgICAgXCJ0b3RhbF9wcm9maXRcIjogX3ZtLnRvdGFsX3Byb2ZpdCxcbiAgICAgIFwidG90YWxfYmFsYW5jZVwiOiBfdm0udG90YWxfYmFsYW5jZSxcbiAgICAgIFwiY2FzaF9iYWxhbmNlXCI6IF92bS5jYXNoX2JhbGFuY2UsXG4gICAgICBcImJhbmtfYmFsYW5jZVwiOiBfdm0uYmFua19iYWxhbmNlLFxuICAgICAgXCJpbml0aWFsX2JhbmtcIjogX3ZtLmluaXRpYWxfYmFuayxcbiAgICAgIFwiaW5pdGlhbF9jYXNoXCI6IF92bS5pbml0aWFsX2Nhc2gsXG4gICAgICBcInNhdmVQcm9qZWN0XCI6IF92bS5zYXZlUHJvamVjdFxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEyXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaWJveCBmbG9hdC1lLW1hcmdpbnNcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpYm94LWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMSBtLWIteHNcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJkaXNwbGF5XCI6IFwibm9uZVwiXG4gICAgfVxuICB9LCBbX2MoJ3NlbGVjdCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS50YWJsZURhdGEubGVuZ3RoKSxcbiAgICAgIGV4cHJlc3Npb246IFwidGFibGVEYXRhLmxlbmd0aFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtc20gZm9ybS1jb250cm9sIGlucHV0LXMtc20gaW5saW5lXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IFtmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkc2VsZWN0ZWRWYWwgPSBBcnJheS5wcm90b3R5cGUuZmlsdGVyLmNhbGwoJGV2ZW50LnRhcmdldC5vcHRpb25zLCBmdW5jdGlvbihvKSB7XG4gICAgICAgICAgcmV0dXJuIG8uc2VsZWN0ZWRcbiAgICAgICAgfSkubWFwKGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICB2YXIgdmFsID0gXCJfdmFsdWVcIiBpbiBvID8gby5fdmFsdWUgOiBvLnZhbHVlO1xuICAgICAgICAgIHJldHVybiB2YWxcbiAgICAgICAgfSk7XG4gICAgICAgIF92bS50YWJsZURhdGEubGVuZ3RoID0gJGV2ZW50LnRhcmdldC5tdWx0aXBsZSA/ICQkc2VsZWN0ZWRWYWwgOiAkJHNlbGVjdGVkVmFsWzBdXG4gICAgICB9LCBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmdldFByb2plY3RzKClcbiAgICAgIH1dXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5wZXJQYWdlKSwgZnVuY3Rpb24ocmVjb3JkcywgaW5kZXgpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGtleTogaW5kZXgsXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IHJlY29yZHNcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KF92bS5fcyhyZWNvcmRzKSldKVxuICB9KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tb2Zmc2V0LTcgY29sLXNtLTRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBwdWxsLXJpZ2h0XCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS50YWJsZURhdGEuc2VhcmNoKSxcbiAgICAgIGV4cHJlc3Npb246IFwidGFibGVEYXRhLnNlYXJjaFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtc20gZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwicGxhY2Vob2xkZXJcIjogXCJTZWFyY2hcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS50YWJsZURhdGEuc2VhcmNoKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogW2Z1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS50YWJsZURhdGEuc2VhcmNoID0gJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgfSwgZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5nZXRQcm9qZWN0cygpXG4gICAgICB9XVxuICAgIH1cbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdGaW5hbmNlVGFibGUnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiY29sdW1uc1wiOiBfdm0uY29sdW1ucyxcbiAgICAgIFwic29ydEtleVwiOiBfdm0uc29ydEtleSxcbiAgICAgIFwic29ydE9yZGVyc1wiOiBfdm0uc29ydE9yZGVyc1xuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwic29ydFwiOiBfdm0uc29ydEJ5XG4gICAgfVxuICB9LCBbX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0ucHJvamVjdHMyKSwgZnVuY3Rpb24ocHJvamVjdCkge1xuICAgIHJldHVybiBfYygndHInLCB7XG4gICAgICBrZXk6IF92bS5wcm9qZWN0czIuaWRcbiAgICB9LCBbX2MoJ3RkJywgW192bS5fdihfdm0uX3MocHJvamVjdC50cmFuc19kZXNjKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MocHJvamVjdC5jYXRfdHlwZSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKHByb2plY3QuY2F0X3N0b3JhZ2UpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmNhc2hfY2xpZW50X2RlcG9fcGF5bWVudCkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmNhc2hfY2xpZW50X3JlZnVuZCkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmNhc2hfY2xpZW50X3Byb2Nlc3NfYnVkZ2V0X3JldHVybikpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmNhc2hfcHJvY2Vzc19jb3N0KSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5udW1iZXJXaXRoQ29tbWFzKHByb2plY3QuYm9ycm93ZWRfcHJvY2Vzc19jb3N0KSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5udW1iZXJXaXRoQ29tbWFzKHByb2plY3QuY2FzaF9hZG1pbl9idWRnZXRfcmV0dXJuKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5udW1iZXJXaXRoQ29tbWFzKHByb2plY3QuYm9ycm93ZWRfYWRtaW5fY29zdCkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmNhc2hfYWRtaW5fY29zdCkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmJhbmtfY2xpZW50X2RlcG9fcGF5bWVudCkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmJhbmtfY29zdCkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmRlcG9zaXRfb3RoZXIpKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMocHJvamVjdC5jb3N0X290aGVyKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5udW1iZXJXaXRoQ29tbWFzKHByb2plY3QudG90YWxfcHJvY2VzcykpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LndlX2hhdmVfYmVmb3JlKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5udW1iZXJXaXRoQ29tbWFzKHByb2plY3Qud2VfaGF2ZV9hZnRlcikpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhwcm9qZWN0LmFkbWluX3RvdGFsKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5udW1iZXJXaXRoQ29tbWFzKHByb2plY3QuY2FzaF9iYWxhbmNlKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5udW1iZXJXaXRoQ29tbWFzKHByb2plY3QuYmFua19iYWxhbmNlKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5udW1iZXJXaXRoQ29tbWFzKHByb2plY3QudG90YWwpKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMocHJvamVjdC5wcm9maXQpKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMocHJvamVjdC5wb3N0ZGF0ZWRfY2hlY2tzKSkpXSldKVxuICB9KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3BhZ2luYXRpb24nLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiZGlzcGxheVwiOiBcIm5vbmUgIWltcG9ydGFudFwiXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJwYWdpbmF0aW9uXCI6IF92bS5wYWdpbmF0aW9uXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJwcmV2XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZ2V0UHJvamVjdHMoX3ZtLnBhZ2luYXRpb24ucHJldlBhZ2VVcmwpXG4gICAgICB9LFxuICAgICAgXCJuZXh0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZ2V0UHJvamVjdHMoX3ZtLnBhZ2luYXRpb24ubmV4dFBhZ2VVcmwpXG4gICAgICB9XG4gICAgfVxuICB9KV0sIDEpXSldKV0sIDEpXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTAxMzlhMjNjXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMDEzOWEyM2NcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9OZXdGaW5hbmNpbmcvUHJvamVjdHMudnVlXG4vLyBtb2R1bGUgaWQgPSA0MTJcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMlwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlib3ggZmxvYXQtZS1tYXJnaW5zIG5vLXBhZGRpbmdcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpYm94LWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tNCBiLXJcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpYm94XCJcbiAgfSwgW19jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YmwtY29tcHV0YXRpb25cIlxuICB9LCBbX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoXCJJbml0aWFsIENhc2g6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMoX3ZtLmluaXRpYWxfY2FzaCkpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoXCJJbml0aWFsIEJhbms6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMoX3ZtLmluaXRpYWxfYmFuaykpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoXCJDYXNoIEJhbGFuY2U6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMoX3ZtLmNhc2hfYmFsYW5jZSkpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoXCJCYW5rIEJhbGFuY2U6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMoX3ZtLmJhbmtfYmFsYW5jZSkpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoXCJUb3RhbDpcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtbXV0ZWRcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhfdm0udG90YWxfYmFsYW5jZSkpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoXCJQcm9maXQ6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMoX3ZtLnRvdGFsX3Byb2ZpdCkpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoXCJQcm9jZXNzIE91dHB1dDpcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtbXV0ZWRcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubnVtYmVyV2l0aENvbW1hcyhfdm0ucHJvY2Vzc19vdXRwdXQpKSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0cicsIFtfYygndGQnLCBbX3ZtLl92KFwiSG93IG11Y2ggd2UgcmVjZWl2ZWQgZnJvbSBjbGllbnQ6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMoX3ZtLndlX3JlY2VpdmVfYmVmb3JlKSkpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndHInLCBbX2MoJ3RkJywgW192bS5fdihcIkhvdyBtdWNoIHdlIHJlY2VpdmVkIGFmdGVyIHdlIHNwZW50IGZvciBjbGllbnQ6XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LW11dGVkXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm51bWJlcldpdGhDb21tYXMoX3ZtLndlX3JlY2VpdmVfYWZ0ZXIpKSldKV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLThcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dEZvcm1cIixcbiAgICBhdHRyczoge1xuICAgICAgXCJtZXRob2RcIjogXCJQT1NUXCIsXG4gICAgICBcInJvbGVcIjogXCJmb3JtXCIsXG4gICAgICBcImlkXCI6IFwiaW5wdXRGb3JtXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywgW192bS5fdihcIlRyYW5zYWN0aW9uIFR5cGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NlbGVjdCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgbS1iXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0cmFuc2FjdGlvbl90eXBlXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmNoZWNrVHJhbnMoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJcIixcbiAgICAgIFwiZGlzYWJsZWRcIjogXCJcIixcbiAgICAgIFwic2VsZWN0ZWRcIjogXCJcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlBsZWFzZSBTZWxlY3RcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ29wdGlvbicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJ2YWx1ZVwiOiBcInBiclwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiUHJvY2Vzc2luZyBCdWRnZXQgUmV0dXJuXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJwY1wiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiUHJvY2Vzc2luZyBDb3N0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJicGNcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkJvcnJvd2VkIFByb2Nlc3NpbmcgQ29zdFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwiYWJyXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJBZG1pbiBCdWRnZXQgUmV0dXJuXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdvcHRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwidmFsdWVcIjogXCJhY1wiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQWRtaW4gQ29zdFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwiYmFjXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJCb3Jyb3dlZCBBZG1pbiBDb3N0XCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmJvcnJvd2VkLmxlbmd0aCA+IDApID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIFtfdm0uX3YoXCJCb3Jyb3dlZCBMaXN0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdzZWxlY3QnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIG0tYlwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwiYm9ycm93ZWRfbGlzdFwiXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5ib3Jyb3dlZCksIGZ1bmN0aW9uKGIpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGtleTogYi5pZCxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogYi5pZFxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoX3ZtLl9zKGIudHJhbnNfZGVzYykpXSlcbiAgfSkpXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5jYXRfdHlwZSA9PSAnYnBjJyB8fCBfdm0uY2F0X3R5cGUgPT0gJ2JhYycpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIFtfdm0uX3YoXCJTdG9yYWdlIFR5cGVcIildKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl9tKDApXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5jYXRfdHlwZSAhPSAnJykgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywgW192bS5fdihcIkFtb3VudFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0eHRBbW91bnRcIixcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwicGxhY2Vob2xkZXJcIjogXCIwLjAwXCJcbiAgICB9XG4gIH0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5jYXRfdHlwZSA9PSAnYnBjJyB8fCBfdm0uY2F0X3R5cGUgPT0gJ2JhYycpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIFtfdm0uX3YoXCJEZXNjcmlwdGlvblwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGV4dGFyZWEnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0eHREZXNjcmlwdGlvblwiLFxuICAgICAgXCJyb3dcIjogXCI1XCJcbiAgICB9XG4gIH0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIFsoX3ZtLmNhdF90eXBlICE9ICcnKSA/IF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1zbSBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0IG0tdC1uLXhzXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5zYXZlUHJvamVjdCgpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ3N0cm9uZycsIFtfdm0uX3YoXCJTYXZlXCIpXSldKSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2gyJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInByb2Nlc3NMb2NhdG9yXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJwcm9jZXNzTG9jYXRvclwiXG4gICAgfVxuICB9KV0pXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzZWxlY3QnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIG0tYlwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwic3RvcmFnZV90eXBlXCJcbiAgICB9XG4gIH0sIFtfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwiQ2FzaFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2FzaFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnb3B0aW9uJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInZhbHVlXCI6IFwiQmFua1wiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQmFua1wiKV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0xNjI5OTY1ZlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTE2Mjk5NjVmXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvTmV3RmluYW5jaW5nL0ZpbmFuY2luZy52dWVcbi8vIG1vZHVsZSBpZCA9IDQxN1xuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbX3ZtLl9tKDApLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIndyYXBwZXIyMlwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImRpdjIyXCJcbiAgfSwgW19jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1ob3ZlciB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmVkXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJmdGFibGVcIlxuICAgIH1cbiAgfSwgW19jKCd0aGVhZCcsIFtfYygndHInLCBfdm0uX2woKF92bS5jb2x1bW5zKSwgZnVuY3Rpb24oY29sdW1uKSB7XG4gICAgcmV0dXJuIF9jKCd0aCcsIHtcbiAgICAgIGtleTogY29sdW1uLm5hbWVcbiAgICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoY29sdW1uLmxhYmVsKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICBcIildKVxuICB9KSldKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl90KFwiZGVmYXVsdFwiKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ3cmFwcGVyMTFcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJkaXYxMVwiXG4gIH0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0yNDZiNzAwY1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTI0NmI3MDBjXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvTmV3RmluYW5jaW5nL0RhdGFUYWJsZS52dWVcbi8vIG1vZHVsZSBpZCA9IDQyMVxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbKCFfdm0uY2xpZW50KSA/IF9jKCduYXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFnaW5hdGlvblwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYWdlLXN0YXRzXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLnBhZ2luYXRpb24uZnJvbSkgKyBcIiAtIFwiICsgX3ZtLl9zKF92bS5wYWdpbmF0aW9uLnRvKSArIFwiIG9mIFwiICsgX3ZtLl9zKF92bS5wYWdpbmF0aW9uLnRvdGFsKSldKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5wYWdpbmF0aW9uLnByZXZQYWdlVXJsKSA/IF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLXByZXZpb3VzXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS4kZW1pdCgncHJldicpO1xuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICBQcmV2XFxuICAgICAgXCIpXSkgOiBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1wcmV2aW91c1wiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRpc2FibGVkXCI6IHRydWVcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICBQcmV2XFxuICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIChfdm0ucGFnaW5hdGlvbi5uZXh0UGFnZVVybCkgPyBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1uZXh0XCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS4kZW1pdCgnbmV4dCcpO1xuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICBOZXh0XFxuICAgICAgXCIpXSkgOiBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1uZXh0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGlzYWJsZWRcIjogdHJ1ZVxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICBOZXh0XFxuICAgICAgXCIpXSldKSA6IF9jKCduYXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFnaW5hdGlvblwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYWdlLXN0YXRzXCJcbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICBcIiArIF92bS5fcyhfdm0ucGFnaW5hdGlvbi5mcm9tKSArIFwiIC0gXCIgKyBfdm0uX3MoX3ZtLnBhZ2luYXRpb24udG8pICsgXCIgb2YgXCIgKyBfdm0uX3MoX3ZtLmZpbHRlcmVkLmxlbmd0aCkgKyBcIlxcbiAgICAgICAgICBcIiksIChfdm0uZmlsdGVyZWQubGVuZ3RoIDwgX3ZtLnBhZ2luYXRpb24udG90YWwpID8gX2MoJ3NwYW4nLCBbX3ZtLl92KFwiKGZpbHRlcmVkIGZyb20gXCIgKyBfdm0uX3MoX3ZtLnBhZ2luYXRpb24udG90YWwpICsgXCIgdG90YWwgZW50cmllcylcIildKSA6IF92bS5fZSgpXSksIF92bS5fdihcIiBcIiksIChfdm0ucGFnaW5hdGlvbi5wcmV2UGFnZSkgPyBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1wcmV2aW91c1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJGVtaXQoJ3ByZXYnKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgUHJldlxcbiAgICAgIFwiKV0pIDogX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tcHJldmlvdXNcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkaXNhYmxlZFwiOiB0cnVlXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgUHJldlxcbiAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnBhZ2luYXRpb24ubmV4dFBhZ2UpID8gX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tbmV4dFwiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJGVtaXQoJ25leHQnKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgTmV4dFxcbiAgICAgIFwiKV0pIDogX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tbmV4dFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRpc2FibGVkXCI6IHRydWVcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgTmV4dFxcbiAgICAgIFwiKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi01NGQ0NmJmY1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTU0ZDQ2YmZjXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvTmV3RmluYW5jaW5nL1BhZ2luYXRpb24udnVlXG4vLyBtb2R1bGUgaWQgPSA0MzZcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsIi8qIVxuICogdnVlLXJlc291cmNlIHYxLjMuNFxuICogaHR0cHM6Ly9naXRodWIuY29tL3BhZ2VraXQvdnVlLXJlc291cmNlXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuXG4gKi9cblxuLyoqXG4gKiBQcm9taXNlcy9BKyBwb2x5ZmlsbCB2MS4xLjQgKGh0dHBzOi8vZ2l0aHViLmNvbS9icmFtc3RlaW4vcHJvbWlzKVxuICovXG5cbnZhciBSRVNPTFZFRCA9IDA7XG52YXIgUkVKRUNURUQgPSAxO1xudmFyIFBFTkRJTkcgID0gMjtcblxuZnVuY3Rpb24gUHJvbWlzZSQxKGV4ZWN1dG9yKSB7XG5cbiAgICB0aGlzLnN0YXRlID0gUEVORElORztcbiAgICB0aGlzLnZhbHVlID0gdW5kZWZpbmVkO1xuICAgIHRoaXMuZGVmZXJyZWQgPSBbXTtcblxuICAgIHZhciBwcm9taXNlID0gdGhpcztcblxuICAgIHRyeSB7XG4gICAgICAgIGV4ZWN1dG9yKGZ1bmN0aW9uICh4KSB7XG4gICAgICAgICAgICBwcm9taXNlLnJlc29sdmUoeCk7XG4gICAgICAgIH0sIGZ1bmN0aW9uIChyKSB7XG4gICAgICAgICAgICBwcm9taXNlLnJlamVjdChyKTtcbiAgICAgICAgfSk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBwcm9taXNlLnJlamVjdChlKTtcbiAgICB9XG59XG5cblByb21pc2UkMS5yZWplY3QgPSBmdW5jdGlvbiAocikge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSQxKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgcmVqZWN0KHIpO1xuICAgIH0pO1xufTtcblxuUHJvbWlzZSQxLnJlc29sdmUgPSBmdW5jdGlvbiAoeCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSQxKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgcmVzb2x2ZSh4KTtcbiAgICB9KTtcbn07XG5cblByb21pc2UkMS5hbGwgPSBmdW5jdGlvbiBhbGwoaXRlcmFibGUpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UkMShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIHZhciBjb3VudCA9IDAsIHJlc3VsdCA9IFtdO1xuXG4gICAgICAgIGlmIChpdGVyYWJsZS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHJlc29sdmVyKGkpIHtcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbiAoeCkge1xuICAgICAgICAgICAgICAgIHJlc3VsdFtpXSA9IHg7XG4gICAgICAgICAgICAgICAgY291bnQgKz0gMTtcblxuICAgICAgICAgICAgICAgIGlmIChjb3VudCA9PT0gaXRlcmFibGUubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBpdGVyYWJsZS5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICAgICAgUHJvbWlzZSQxLnJlc29sdmUoaXRlcmFibGVbaV0pLnRoZW4ocmVzb2x2ZXIoaSksIHJlamVjdCk7XG4gICAgICAgIH1cbiAgICB9KTtcbn07XG5cblByb21pc2UkMS5yYWNlID0gZnVuY3Rpb24gcmFjZShpdGVyYWJsZSkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSQxKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBpdGVyYWJsZS5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICAgICAgUHJvbWlzZSQxLnJlc29sdmUoaXRlcmFibGVbaV0pLnRoZW4ocmVzb2x2ZSwgcmVqZWN0KTtcbiAgICAgICAgfVxuICAgIH0pO1xufTtcblxudmFyIHAkMSA9IFByb21pc2UkMS5wcm90b3R5cGU7XG5cbnAkMS5yZXNvbHZlID0gZnVuY3Rpb24gcmVzb2x2ZSh4KSB7XG4gICAgdmFyIHByb21pc2UgPSB0aGlzO1xuXG4gICAgaWYgKHByb21pc2Uuc3RhdGUgPT09IFBFTkRJTkcpIHtcbiAgICAgICAgaWYgKHggPT09IHByb21pc2UpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1Byb21pc2Ugc2V0dGxlZCB3aXRoIGl0c2VsZi4nKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBjYWxsZWQgPSBmYWxzZTtcblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdmFyIHRoZW4gPSB4ICYmIHhbJ3RoZW4nXTtcblxuICAgICAgICAgICAgaWYgKHggIT09IG51bGwgJiYgdHlwZW9mIHggPT09ICdvYmplY3QnICYmIHR5cGVvZiB0aGVuID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgdGhlbi5jYWxsKHgsIGZ1bmN0aW9uICh4KSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghY2FsbGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9taXNlLnJlc29sdmUoeCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2FsbGVkID0gdHJ1ZTtcblxuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChyKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghY2FsbGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9taXNlLnJlamVjdChyKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjYWxsZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgaWYgKCFjYWxsZWQpIHtcbiAgICAgICAgICAgICAgICBwcm9taXNlLnJlamVjdChlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHByb21pc2Uuc3RhdGUgPSBSRVNPTFZFRDtcbiAgICAgICAgcHJvbWlzZS52YWx1ZSA9IHg7XG4gICAgICAgIHByb21pc2Uubm90aWZ5KCk7XG4gICAgfVxufTtcblxucCQxLnJlamVjdCA9IGZ1bmN0aW9uIHJlamVjdChyZWFzb24pIHtcbiAgICB2YXIgcHJvbWlzZSA9IHRoaXM7XG5cbiAgICBpZiAocHJvbWlzZS5zdGF0ZSA9PT0gUEVORElORykge1xuICAgICAgICBpZiAocmVhc29uID09PSBwcm9taXNlKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdQcm9taXNlIHNldHRsZWQgd2l0aCBpdHNlbGYuJyk7XG4gICAgICAgIH1cblxuICAgICAgICBwcm9taXNlLnN0YXRlID0gUkVKRUNURUQ7XG4gICAgICAgIHByb21pc2UudmFsdWUgPSByZWFzb247XG4gICAgICAgIHByb21pc2Uubm90aWZ5KCk7XG4gICAgfVxufTtcblxucCQxLm5vdGlmeSA9IGZ1bmN0aW9uIG5vdGlmeSgpIHtcbiAgICB2YXIgcHJvbWlzZSA9IHRoaXM7XG5cbiAgICBuZXh0VGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChwcm9taXNlLnN0YXRlICE9PSBQRU5ESU5HKSB7XG4gICAgICAgICAgICB3aGlsZSAocHJvbWlzZS5kZWZlcnJlZC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICB2YXIgZGVmZXJyZWQgPSBwcm9taXNlLmRlZmVycmVkLnNoaWZ0KCksXG4gICAgICAgICAgICAgICAgICAgIG9uUmVzb2x2ZWQgPSBkZWZlcnJlZFswXSxcbiAgICAgICAgICAgICAgICAgICAgb25SZWplY3RlZCA9IGRlZmVycmVkWzFdLFxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlID0gZGVmZXJyZWRbMl0sXG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCA9IGRlZmVycmVkWzNdO1xuXG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHByb21pc2Uuc3RhdGUgPT09IFJFU09MVkVEKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG9uUmVzb2x2ZWQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG9uUmVzb2x2ZWQuY2FsbCh1bmRlZmluZWQsIHByb21pc2UudmFsdWUpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShwcm9taXNlLnZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChwcm9taXNlLnN0YXRlID09PSBSRUpFQ1RFRCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBvblJlamVjdGVkID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShvblJlamVjdGVkLmNhbGwodW5kZWZpbmVkLCBwcm9taXNlLnZhbHVlKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChwcm9taXNlLnZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pO1xufTtcblxucCQxLnRoZW4gPSBmdW5jdGlvbiB0aGVuKG9uUmVzb2x2ZWQsIG9uUmVqZWN0ZWQpIHtcbiAgICB2YXIgcHJvbWlzZSA9IHRoaXM7XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2UkMShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIHByb21pc2UuZGVmZXJyZWQucHVzaChbb25SZXNvbHZlZCwgb25SZWplY3RlZCwgcmVzb2x2ZSwgcmVqZWN0XSk7XG4gICAgICAgIHByb21pc2Uubm90aWZ5KCk7XG4gICAgfSk7XG59O1xuXG5wJDEuY2F0Y2ggPSBmdW5jdGlvbiAob25SZWplY3RlZCkge1xuICAgIHJldHVybiB0aGlzLnRoZW4odW5kZWZpbmVkLCBvblJlamVjdGVkKTtcbn07XG5cbi8qKlxuICogUHJvbWlzZSBhZGFwdGVyLlxuICovXG5cbmlmICh0eXBlb2YgUHJvbWlzZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICB3aW5kb3cuUHJvbWlzZSA9IFByb21pc2UkMTtcbn1cblxuZnVuY3Rpb24gUHJvbWlzZU9iaihleGVjdXRvciwgY29udGV4dCkge1xuXG4gICAgaWYgKGV4ZWN1dG9yIGluc3RhbmNlb2YgUHJvbWlzZSkge1xuICAgICAgICB0aGlzLnByb21pc2UgPSBleGVjdXRvcjtcbiAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnByb21pc2UgPSBuZXcgUHJvbWlzZShleGVjdXRvci5iaW5kKGNvbnRleHQpKTtcbiAgICB9XG5cbiAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xufVxuXG5Qcm9taXNlT2JqLmFsbCA9IGZ1bmN0aW9uIChpdGVyYWJsZSwgY29udGV4dCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZU9iaihQcm9taXNlLmFsbChpdGVyYWJsZSksIGNvbnRleHQpO1xufTtcblxuUHJvbWlzZU9iai5yZXNvbHZlID0gZnVuY3Rpb24gKHZhbHVlLCBjb250ZXh0KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKFByb21pc2UucmVzb2x2ZSh2YWx1ZSksIGNvbnRleHQpO1xufTtcblxuUHJvbWlzZU9iai5yZWplY3QgPSBmdW5jdGlvbiAocmVhc29uLCBjb250ZXh0KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKFByb21pc2UucmVqZWN0KHJlYXNvbiksIGNvbnRleHQpO1xufTtcblxuUHJvbWlzZU9iai5yYWNlID0gZnVuY3Rpb24gKGl0ZXJhYmxlLCBjb250ZXh0KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKFByb21pc2UucmFjZShpdGVyYWJsZSksIGNvbnRleHQpO1xufTtcblxudmFyIHAgPSBQcm9taXNlT2JqLnByb3RvdHlwZTtcblxucC5iaW5kID0gZnVuY3Rpb24gKGNvbnRleHQpIHtcbiAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgIHJldHVybiB0aGlzO1xufTtcblxucC50aGVuID0gZnVuY3Rpb24gKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpIHtcblxuICAgIGlmIChmdWxmaWxsZWQgJiYgZnVsZmlsbGVkLmJpbmQgJiYgdGhpcy5jb250ZXh0KSB7XG4gICAgICAgIGZ1bGZpbGxlZCA9IGZ1bGZpbGxlZC5iaW5kKHRoaXMuY29udGV4dCk7XG4gICAgfVxuXG4gICAgaWYgKHJlamVjdGVkICYmIHJlamVjdGVkLmJpbmQgJiYgdGhpcy5jb250ZXh0KSB7XG4gICAgICAgIHJlamVjdGVkID0gcmVqZWN0ZWQuYmluZCh0aGlzLmNvbnRleHQpO1xuICAgIH1cblxuICAgIHJldHVybiBuZXcgUHJvbWlzZU9iaih0aGlzLnByb21pc2UudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKSwgdGhpcy5jb250ZXh0KTtcbn07XG5cbnAuY2F0Y2ggPSBmdW5jdGlvbiAocmVqZWN0ZWQpIHtcblxuICAgIGlmIChyZWplY3RlZCAmJiByZWplY3RlZC5iaW5kICYmIHRoaXMuY29udGV4dCkge1xuICAgICAgICByZWplY3RlZCA9IHJlamVjdGVkLmJpbmQodGhpcy5jb250ZXh0KTtcbiAgICB9XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2VPYmoodGhpcy5wcm9taXNlLmNhdGNoKHJlamVjdGVkKSwgdGhpcy5jb250ZXh0KTtcbn07XG5cbnAuZmluYWxseSA9IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuXG4gICAgcmV0dXJuIHRoaXMudGhlbihmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgIGNhbGxiYWNrLmNhbGwodGhpcyk7XG4gICAgICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICAgIH0sIGZ1bmN0aW9uIChyZWFzb24pIHtcbiAgICAgICAgICAgIGNhbGxiYWNrLmNhbGwodGhpcyk7XG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QocmVhc29uKTtcbiAgICAgICAgfVxuICAgICk7XG59O1xuXG4vKipcbiAqIFV0aWxpdHkgZnVuY3Rpb25zLlxuICovXG5cbnZhciByZWYgPSB7fTtcbnZhciBoYXNPd25Qcm9wZXJ0eSA9IHJlZi5oYXNPd25Qcm9wZXJ0eTtcblxudmFyIHJlZiQxID0gW107XG52YXIgc2xpY2UgPSByZWYkMS5zbGljZTtcbnZhciBkZWJ1ZyA9IGZhbHNlO1xudmFyIG50aWNrO1xuXG52YXIgaW5Ccm93c2VyID0gdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCc7XG5cbnZhciBVdGlsID0gZnVuY3Rpb24gKHJlZikge1xuICAgIHZhciBjb25maWcgPSByZWYuY29uZmlnO1xuICAgIHZhciBuZXh0VGljayA9IHJlZi5uZXh0VGljaztcblxuICAgIG50aWNrID0gbmV4dFRpY2s7XG4gICAgZGVidWcgPSBjb25maWcuZGVidWcgfHwgIWNvbmZpZy5zaWxlbnQ7XG59O1xuXG5mdW5jdGlvbiB3YXJuKG1zZykge1xuICAgIGlmICh0eXBlb2YgY29uc29sZSAhPT0gJ3VuZGVmaW5lZCcgJiYgZGVidWcpIHtcbiAgICAgICAgY29uc29sZS53YXJuKCdbVnVlUmVzb3VyY2Ugd2Fybl06ICcgKyBtc2cpO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gZXJyb3IobXNnKSB7XG4gICAgaWYgKHR5cGVvZiBjb25zb2xlICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBjb25zb2xlLmVycm9yKG1zZyk7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBuZXh0VGljayhjYiwgY3R4KSB7XG4gICAgcmV0dXJuIG50aWNrKGNiLCBjdHgpO1xufVxuXG5mdW5jdGlvbiB0cmltKHN0cikge1xuICAgIHJldHVybiBzdHIgPyBzdHIucmVwbGFjZSgvXlxccyp8XFxzKiQvZywgJycpIDogJyc7XG59XG5cbmZ1bmN0aW9uIHRyaW1FbmQoc3RyLCBjaGFycykge1xuXG4gICAgaWYgKHN0ciAmJiBjaGFycyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJldHVybiBzdHIucmVwbGFjZSgvXFxzKyQvLCAnJyk7XG4gICAgfVxuXG4gICAgaWYgKCFzdHIgfHwgIWNoYXJzKSB7XG4gICAgICAgIHJldHVybiBzdHI7XG4gICAgfVxuXG4gICAgcmV0dXJuIHN0ci5yZXBsYWNlKG5ldyBSZWdFeHAoKFwiW1wiICsgY2hhcnMgKyBcIl0rJFwiKSksICcnKTtcbn1cblxuZnVuY3Rpb24gdG9Mb3dlcihzdHIpIHtcbiAgICByZXR1cm4gc3RyID8gc3RyLnRvTG93ZXJDYXNlKCkgOiAnJztcbn1cblxuZnVuY3Rpb24gdG9VcHBlcihzdHIpIHtcbiAgICByZXR1cm4gc3RyID8gc3RyLnRvVXBwZXJDYXNlKCkgOiAnJztcbn1cblxudmFyIGlzQXJyYXkgPSBBcnJheS5pc0FycmF5O1xuXG5mdW5jdGlvbiBpc1N0cmluZyh2YWwpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ3N0cmluZyc7XG59XG5cblxuXG5mdW5jdGlvbiBpc0Z1bmN0aW9uKHZhbCkge1xuICAgIHJldHVybiB0eXBlb2YgdmFsID09PSAnZnVuY3Rpb24nO1xufVxuXG5mdW5jdGlvbiBpc09iamVjdChvYmopIHtcbiAgICByZXR1cm4gb2JqICE9PSBudWxsICYmIHR5cGVvZiBvYmogPT09ICdvYmplY3QnO1xufVxuXG5mdW5jdGlvbiBpc1BsYWluT2JqZWN0KG9iaikge1xuICAgIHJldHVybiBpc09iamVjdChvYmopICYmIE9iamVjdC5nZXRQcm90b3R5cGVPZihvYmopID09IE9iamVjdC5wcm90b3R5cGU7XG59XG5cbmZ1bmN0aW9uIGlzQmxvYihvYmopIHtcbiAgICByZXR1cm4gdHlwZW9mIEJsb2IgIT09ICd1bmRlZmluZWQnICYmIG9iaiBpbnN0YW5jZW9mIEJsb2I7XG59XG5cbmZ1bmN0aW9uIGlzRm9ybURhdGEob2JqKSB7XG4gICAgcmV0dXJuIHR5cGVvZiBGb3JtRGF0YSAhPT0gJ3VuZGVmaW5lZCcgJiYgb2JqIGluc3RhbmNlb2YgRm9ybURhdGE7XG59XG5cbmZ1bmN0aW9uIHdoZW4odmFsdWUsIGZ1bGZpbGxlZCwgcmVqZWN0ZWQpIHtcblxuICAgIHZhciBwcm9taXNlID0gUHJvbWlzZU9iai5yZXNvbHZlKHZhbHVlKTtcblxuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoIDwgMikge1xuICAgICAgICByZXR1cm4gcHJvbWlzZTtcbiAgICB9XG5cbiAgICByZXR1cm4gcHJvbWlzZS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpO1xufVxuXG5mdW5jdGlvbiBvcHRpb25zKGZuLCBvYmosIG9wdHMpIHtcblxuICAgIG9wdHMgPSBvcHRzIHx8IHt9O1xuXG4gICAgaWYgKGlzRnVuY3Rpb24ob3B0cykpIHtcbiAgICAgICAgb3B0cyA9IG9wdHMuY2FsbChvYmopO1xuICAgIH1cblxuICAgIHJldHVybiBtZXJnZShmbi5iaW5kKHskdm06IG9iaiwgJG9wdGlvbnM6IG9wdHN9KSwgZm4sIHskb3B0aW9uczogb3B0c30pO1xufVxuXG5mdW5jdGlvbiBlYWNoKG9iaiwgaXRlcmF0b3IpIHtcblxuICAgIHZhciBpLCBrZXk7XG5cbiAgICBpZiAoaXNBcnJheShvYmopKSB7XG4gICAgICAgIGZvciAoaSA9IDA7IGkgPCBvYmoubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGl0ZXJhdG9yLmNhbGwob2JqW2ldLCBvYmpbaV0sIGkpO1xuICAgICAgICB9XG4gICAgfSBlbHNlIGlmIChpc09iamVjdChvYmopKSB7XG4gICAgICAgIGZvciAoa2V5IGluIG9iaikge1xuICAgICAgICAgICAgaWYgKGhhc093blByb3BlcnR5LmNhbGwob2JqLCBrZXkpKSB7XG4gICAgICAgICAgICAgICAgaXRlcmF0b3IuY2FsbChvYmpba2V5XSwgb2JqW2tleV0sIGtleSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gb2JqO1xufVxuXG52YXIgYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBfYXNzaWduO1xuXG5mdW5jdGlvbiBtZXJnZSh0YXJnZXQpIHtcblxuICAgIHZhciBhcmdzID0gc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpO1xuXG4gICAgYXJncy5mb3JFYWNoKGZ1bmN0aW9uIChzb3VyY2UpIHtcbiAgICAgICAgX21lcmdlKHRhcmdldCwgc291cmNlLCB0cnVlKTtcbiAgICB9KTtcblxuICAgIHJldHVybiB0YXJnZXQ7XG59XG5cbmZ1bmN0aW9uIGRlZmF1bHRzKHRhcmdldCkge1xuXG4gICAgdmFyIGFyZ3MgPSBzbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG5cbiAgICBhcmdzLmZvckVhY2goZnVuY3Rpb24gKHNvdXJjZSkge1xuXG4gICAgICAgIGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHtcbiAgICAgICAgICAgIGlmICh0YXJnZXRba2V5XSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGFyZ2V0O1xufVxuXG5mdW5jdGlvbiBfYXNzaWduKHRhcmdldCkge1xuXG4gICAgdmFyIGFyZ3MgPSBzbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG5cbiAgICBhcmdzLmZvckVhY2goZnVuY3Rpb24gKHNvdXJjZSkge1xuICAgICAgICBfbWVyZ2UodGFyZ2V0LCBzb3VyY2UpO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRhcmdldDtcbn1cblxuZnVuY3Rpb24gX21lcmdlKHRhcmdldCwgc291cmNlLCBkZWVwKSB7XG4gICAgZm9yICh2YXIga2V5IGluIHNvdXJjZSkge1xuICAgICAgICBpZiAoZGVlcCAmJiAoaXNQbGFpbk9iamVjdChzb3VyY2Vba2V5XSkgfHwgaXNBcnJheShzb3VyY2Vba2V5XSkpKSB7XG4gICAgICAgICAgICBpZiAoaXNQbGFpbk9iamVjdChzb3VyY2Vba2V5XSkgJiYgIWlzUGxhaW5PYmplY3QodGFyZ2V0W2tleV0pKSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0W2tleV0gPSB7fTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChpc0FycmF5KHNvdXJjZVtrZXldKSAmJiAhaXNBcnJheSh0YXJnZXRba2V5XSkpIHtcbiAgICAgICAgICAgICAgICB0YXJnZXRba2V5XSA9IFtdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgX21lcmdlKHRhcmdldFtrZXldLCBzb3VyY2Vba2V5XSwgZGVlcCk7XG4gICAgICAgIH0gZWxzZSBpZiAoc291cmNlW2tleV0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLyoqXG4gKiBSb290IFByZWZpeCBUcmFuc2Zvcm0uXG4gKi9cblxudmFyIHJvb3QgPSBmdW5jdGlvbiAob3B0aW9ucyQkMSwgbmV4dCkge1xuXG4gICAgdmFyIHVybCA9IG5leHQob3B0aW9ucyQkMSk7XG5cbiAgICBpZiAoaXNTdHJpbmcob3B0aW9ucyQkMS5yb290KSAmJiAhL14oaHR0cHM/Oik/XFwvLy50ZXN0KHVybCkpIHtcbiAgICAgICAgdXJsID0gdHJpbUVuZChvcHRpb25zJCQxLnJvb3QsICcvJykgKyAnLycgKyB1cmw7XG4gICAgfVxuXG4gICAgcmV0dXJuIHVybDtcbn07XG5cbi8qKlxuICogUXVlcnkgUGFyYW1ldGVyIFRyYW5zZm9ybS5cbiAqL1xuXG52YXIgcXVlcnkgPSBmdW5jdGlvbiAob3B0aW9ucyQkMSwgbmV4dCkge1xuXG4gICAgdmFyIHVybFBhcmFtcyA9IE9iamVjdC5rZXlzKFVybC5vcHRpb25zLnBhcmFtcyksIHF1ZXJ5ID0ge30sIHVybCA9IG5leHQob3B0aW9ucyQkMSk7XG5cbiAgICBlYWNoKG9wdGlvbnMkJDEucGFyYW1zLCBmdW5jdGlvbiAodmFsdWUsIGtleSkge1xuICAgICAgICBpZiAodXJsUGFyYW1zLmluZGV4T2Yoa2V5KSA9PT0gLTEpIHtcbiAgICAgICAgICAgIHF1ZXJ5W2tleV0gPSB2YWx1ZTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgcXVlcnkgPSBVcmwucGFyYW1zKHF1ZXJ5KTtcblxuICAgIGlmIChxdWVyeSkge1xuICAgICAgICB1cmwgKz0gKHVybC5pbmRleE9mKCc/JykgPT0gLTEgPyAnPycgOiAnJicpICsgcXVlcnk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHVybDtcbn07XG5cbi8qKlxuICogVVJMIFRlbXBsYXRlIHYyLjAuNiAoaHR0cHM6Ly9naXRodWIuY29tL2JyYW1zdGVpbi91cmwtdGVtcGxhdGUpXG4gKi9cblxuZnVuY3Rpb24gZXhwYW5kKHVybCwgcGFyYW1zLCB2YXJpYWJsZXMpIHtcblxuICAgIHZhciB0bXBsID0gcGFyc2UodXJsKSwgZXhwYW5kZWQgPSB0bXBsLmV4cGFuZChwYXJhbXMpO1xuXG4gICAgaWYgKHZhcmlhYmxlcykge1xuICAgICAgICB2YXJpYWJsZXMucHVzaC5hcHBseSh2YXJpYWJsZXMsIHRtcGwudmFycyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGV4cGFuZGVkO1xufVxuXG5mdW5jdGlvbiBwYXJzZSh0ZW1wbGF0ZSkge1xuXG4gICAgdmFyIG9wZXJhdG9ycyA9IFsnKycsICcjJywgJy4nLCAnLycsICc7JywgJz8nLCAnJiddLCB2YXJpYWJsZXMgPSBbXTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIHZhcnM6IHZhcmlhYmxlcyxcbiAgICAgICAgZXhwYW5kOiBmdW5jdGlvbiBleHBhbmQoY29udGV4dCkge1xuICAgICAgICAgICAgcmV0dXJuIHRlbXBsYXRlLnJlcGxhY2UoL1xceyhbXlxce1xcfV0rKVxcfXwoW15cXHtcXH1dKykvZywgZnVuY3Rpb24gKF8sIGV4cHJlc3Npb24sIGxpdGVyYWwpIHtcbiAgICAgICAgICAgICAgICBpZiAoZXhwcmVzc2lvbikge1xuXG4gICAgICAgICAgICAgICAgICAgIHZhciBvcGVyYXRvciA9IG51bGwsIHZhbHVlcyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcGVyYXRvcnMuaW5kZXhPZihleHByZXNzaW9uLmNoYXJBdCgwKSkgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGVyYXRvciA9IGV4cHJlc3Npb24uY2hhckF0KDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgZXhwcmVzc2lvbiA9IGV4cHJlc3Npb24uc3Vic3RyKDEpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgZXhwcmVzc2lvbi5zcGxpdCgvLC9nKS5mb3JFYWNoKGZ1bmN0aW9uICh2YXJpYWJsZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRtcCA9IC8oW146XFwqXSopKD86OihcXGQrKXwoXFwqKSk/Ly5leGVjKHZhcmlhYmxlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlcy5wdXNoLmFwcGx5KHZhbHVlcywgZ2V0VmFsdWVzKGNvbnRleHQsIG9wZXJhdG9yLCB0bXBbMV0sIHRtcFsyXSB8fCB0bXBbM10pKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhcmlhYmxlcy5wdXNoKHRtcFsxXSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcGVyYXRvciAmJiBvcGVyYXRvciAhPT0gJysnKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzZXBhcmF0b3IgPSAnLCc7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcGVyYXRvciA9PT0gJz8nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VwYXJhdG9yID0gJyYnO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChvcGVyYXRvciAhPT0gJyMnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VwYXJhdG9yID0gb3BlcmF0b3I7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAodmFsdWVzLmxlbmd0aCAhPT0gMCA/IG9wZXJhdG9yIDogJycpICsgdmFsdWVzLmpvaW4oc2VwYXJhdG9yKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZXMuam9pbignLCcpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZW5jb2RlUmVzZXJ2ZWQobGl0ZXJhbCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9O1xufVxuXG5mdW5jdGlvbiBnZXRWYWx1ZXMoY29udGV4dCwgb3BlcmF0b3IsIGtleSwgbW9kaWZpZXIpIHtcblxuICAgIHZhciB2YWx1ZSA9IGNvbnRleHRba2V5XSwgcmVzdWx0ID0gW107XG5cbiAgICBpZiAoaXNEZWZpbmVkKHZhbHVlKSAmJiB2YWx1ZSAhPT0gJycpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycgfHwgdHlwZW9mIHZhbHVlID09PSAnbnVtYmVyJyB8fCB0eXBlb2YgdmFsdWUgPT09ICdib29sZWFuJykge1xuICAgICAgICAgICAgdmFsdWUgPSB2YWx1ZS50b1N0cmluZygpO1xuXG4gICAgICAgICAgICBpZiAobW9kaWZpZXIgJiYgbW9kaWZpZXIgIT09ICcqJykge1xuICAgICAgICAgICAgICAgIHZhbHVlID0gdmFsdWUuc3Vic3RyaW5nKDAsIHBhcnNlSW50KG1vZGlmaWVyLCAxMCkpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXN1bHQucHVzaChlbmNvZGVWYWx1ZShvcGVyYXRvciwgdmFsdWUsIGlzS2V5T3BlcmF0b3Iob3BlcmF0b3IpID8ga2V5IDogbnVsbCkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKG1vZGlmaWVyID09PSAnKicpIHtcbiAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUuZmlsdGVyKGlzRGVmaW5lZCkuZm9yRWFjaChmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5wdXNoKGVuY29kZVZhbHVlKG9wZXJhdG9yLCB2YWx1ZSwgaXNLZXlPcGVyYXRvcihvcGVyYXRvcikgPyBrZXkgOiBudWxsKSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKHZhbHVlKS5mb3JFYWNoKGZ1bmN0aW9uIChrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNEZWZpbmVkKHZhbHVlW2tdKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5wdXNoKGVuY29kZVZhbHVlKG9wZXJhdG9yLCB2YWx1ZVtrXSwgaykpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhciB0bXAgPSBbXTtcblxuICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xuICAgICAgICAgICAgICAgICAgICB2YWx1ZS5maWx0ZXIoaXNEZWZpbmVkKS5mb3JFYWNoKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdG1wLnB1c2goZW5jb2RlVmFsdWUob3BlcmF0b3IsIHZhbHVlKSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKHZhbHVlKS5mb3JFYWNoKGZ1bmN0aW9uIChrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNEZWZpbmVkKHZhbHVlW2tdKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRtcC5wdXNoKGVuY29kZVVSSUNvbXBvbmVudChrKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG1wLnB1c2goZW5jb2RlVmFsdWUob3BlcmF0b3IsIHZhbHVlW2tdLnRvU3RyaW5nKCkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGlzS2V5T3BlcmF0b3Iob3BlcmF0b3IpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC5wdXNoKGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgdG1wLmpvaW4oJywnKSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0bXAubGVuZ3RoICE9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC5wdXNoKHRtcC5qb2luKCcsJykpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChvcGVyYXRvciA9PT0gJzsnKSB7XG4gICAgICAgICAgICByZXN1bHQucHVzaChlbmNvZGVVUklDb21wb25lbnQoa2V5KSk7XG4gICAgICAgIH0gZWxzZSBpZiAodmFsdWUgPT09ICcnICYmIChvcGVyYXRvciA9PT0gJyYnIHx8IG9wZXJhdG9yID09PSAnPycpKSB7XG4gICAgICAgICAgICByZXN1bHQucHVzaChlbmNvZGVVUklDb21wb25lbnQoa2V5KSArICc9Jyk7XG4gICAgICAgIH0gZWxzZSBpZiAodmFsdWUgPT09ICcnKSB7XG4gICAgICAgICAgICByZXN1bHQucHVzaCgnJyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gcmVzdWx0O1xufVxuXG5mdW5jdGlvbiBpc0RlZmluZWQodmFsdWUpIHtcbiAgICByZXR1cm4gdmFsdWUgIT09IHVuZGVmaW5lZCAmJiB2YWx1ZSAhPT0gbnVsbDtcbn1cblxuZnVuY3Rpb24gaXNLZXlPcGVyYXRvcihvcGVyYXRvcikge1xuICAgIHJldHVybiBvcGVyYXRvciA9PT0gJzsnIHx8IG9wZXJhdG9yID09PSAnJicgfHwgb3BlcmF0b3IgPT09ICc/Jztcbn1cblxuZnVuY3Rpb24gZW5jb2RlVmFsdWUob3BlcmF0b3IsIHZhbHVlLCBrZXkpIHtcblxuICAgIHZhbHVlID0gKG9wZXJhdG9yID09PSAnKycgfHwgb3BlcmF0b3IgPT09ICcjJykgPyBlbmNvZGVSZXNlcnZlZCh2YWx1ZSkgOiBlbmNvZGVVUklDb21wb25lbnQodmFsdWUpO1xuXG4gICAgaWYgKGtleSkge1xuICAgICAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnPScgKyB2YWx1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBlbmNvZGVSZXNlcnZlZChzdHIpIHtcbiAgICByZXR1cm4gc3RyLnNwbGl0KC8oJVswLTlBLUZhLWZdezJ9KS9nKS5tYXAoZnVuY3Rpb24gKHBhcnQpIHtcbiAgICAgICAgaWYgKCEvJVswLTlBLUZhLWZdLy50ZXN0KHBhcnQpKSB7XG4gICAgICAgICAgICBwYXJ0ID0gZW5jb2RlVVJJKHBhcnQpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBwYXJ0O1xuICAgIH0pLmpvaW4oJycpO1xufVxuXG4vKipcbiAqIFVSTCBUZW1wbGF0ZSAoUkZDIDY1NzApIFRyYW5zZm9ybS5cbiAqL1xuXG52YXIgdGVtcGxhdGUgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuXG4gICAgdmFyIHZhcmlhYmxlcyA9IFtdLCB1cmwgPSBleHBhbmQob3B0aW9ucy51cmwsIG9wdGlvbnMucGFyYW1zLCB2YXJpYWJsZXMpO1xuXG4gICAgdmFyaWFibGVzLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICBkZWxldGUgb3B0aW9ucy5wYXJhbXNba2V5XTtcbiAgICB9KTtcblxuICAgIHJldHVybiB1cmw7XG59O1xuXG4vKipcbiAqIFNlcnZpY2UgZm9yIFVSTCB0ZW1wbGF0aW5nLlxuICovXG5cbmZ1bmN0aW9uIFVybCh1cmwsIHBhcmFtcykge1xuXG4gICAgdmFyIHNlbGYgPSB0aGlzIHx8IHt9LCBvcHRpb25zJCQxID0gdXJsLCB0cmFuc2Zvcm07XG5cbiAgICBpZiAoaXNTdHJpbmcodXJsKSkge1xuICAgICAgICBvcHRpb25zJCQxID0ge3VybDogdXJsLCBwYXJhbXM6IHBhcmFtc307XG4gICAgfVxuXG4gICAgb3B0aW9ucyQkMSA9IG1lcmdlKHt9LCBVcmwub3B0aW9ucywgc2VsZi4kb3B0aW9ucywgb3B0aW9ucyQkMSk7XG5cbiAgICBVcmwudHJhbnNmb3Jtcy5mb3JFYWNoKGZ1bmN0aW9uIChoYW5kbGVyKSB7XG5cbiAgICAgICAgaWYgKGlzU3RyaW5nKGhhbmRsZXIpKSB7XG4gICAgICAgICAgICBoYW5kbGVyID0gVXJsLnRyYW5zZm9ybVtoYW5kbGVyXTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChpc0Z1bmN0aW9uKGhhbmRsZXIpKSB7XG4gICAgICAgICAgICB0cmFuc2Zvcm0gPSBmYWN0b3J5KGhhbmRsZXIsIHRyYW5zZm9ybSwgc2VsZi4kdm0pO1xuICAgICAgICB9XG5cbiAgICB9KTtcblxuICAgIHJldHVybiB0cmFuc2Zvcm0ob3B0aW9ucyQkMSk7XG59XG5cbi8qKlxuICogVXJsIG9wdGlvbnMuXG4gKi9cblxuVXJsLm9wdGlvbnMgPSB7XG4gICAgdXJsOiAnJyxcbiAgICByb290OiBudWxsLFxuICAgIHBhcmFtczoge31cbn07XG5cbi8qKlxuICogVXJsIHRyYW5zZm9ybXMuXG4gKi9cblxuVXJsLnRyYW5zZm9ybSA9IHt0ZW1wbGF0ZTogdGVtcGxhdGUsIHF1ZXJ5OiBxdWVyeSwgcm9vdDogcm9vdH07XG5VcmwudHJhbnNmb3JtcyA9IFsndGVtcGxhdGUnLCAncXVlcnknLCAncm9vdCddO1xuXG4vKipcbiAqIEVuY29kZXMgYSBVcmwgcGFyYW1ldGVyIHN0cmluZy5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqXG4gKi9cblxuVXJsLnBhcmFtcyA9IGZ1bmN0aW9uIChvYmopIHtcblxuICAgIHZhciBwYXJhbXMgPSBbXSwgZXNjYXBlID0gZW5jb2RlVVJJQ29tcG9uZW50O1xuXG4gICAgcGFyYW1zLmFkZCA9IGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG5cbiAgICAgICAgaWYgKGlzRnVuY3Rpb24odmFsdWUpKSB7XG4gICAgICAgICAgICB2YWx1ZSA9IHZhbHVlKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodmFsdWUgPT09IG51bGwpIHtcbiAgICAgICAgICAgIHZhbHVlID0gJyc7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnB1c2goZXNjYXBlKGtleSkgKyAnPScgKyBlc2NhcGUodmFsdWUpKTtcbiAgICB9O1xuXG4gICAgc2VyaWFsaXplKHBhcmFtcywgb2JqKTtcblxuICAgIHJldHVybiBwYXJhbXMuam9pbignJicpLnJlcGxhY2UoLyUyMC9nLCAnKycpO1xufTtcblxuLyoqXG4gKiBQYXJzZSBhIFVSTCBhbmQgcmV0dXJuIGl0cyBjb21wb25lbnRzLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSB1cmxcbiAqL1xuXG5VcmwucGFyc2UgPSBmdW5jdGlvbiAodXJsKSB7XG5cbiAgICB2YXIgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XG5cbiAgICBpZiAoZG9jdW1lbnQuZG9jdW1lbnRNb2RlKSB7XG4gICAgICAgIGVsLmhyZWYgPSB1cmw7XG4gICAgICAgIHVybCA9IGVsLmhyZWY7XG4gICAgfVxuXG4gICAgZWwuaHJlZiA9IHVybDtcblxuICAgIHJldHVybiB7XG4gICAgICAgIGhyZWY6IGVsLmhyZWYsXG4gICAgICAgIHByb3RvY29sOiBlbC5wcm90b2NvbCA/IGVsLnByb3RvY29sLnJlcGxhY2UoLzokLywgJycpIDogJycsXG4gICAgICAgIHBvcnQ6IGVsLnBvcnQsXG4gICAgICAgIGhvc3Q6IGVsLmhvc3QsXG4gICAgICAgIGhvc3RuYW1lOiBlbC5ob3N0bmFtZSxcbiAgICAgICAgcGF0aG5hbWU6IGVsLnBhdGhuYW1lLmNoYXJBdCgwKSA9PT0gJy8nID8gZWwucGF0aG5hbWUgOiAnLycgKyBlbC5wYXRobmFtZSxcbiAgICAgICAgc2VhcmNoOiBlbC5zZWFyY2ggPyBlbC5zZWFyY2gucmVwbGFjZSgvXlxcPy8sICcnKSA6ICcnLFxuICAgICAgICBoYXNoOiBlbC5oYXNoID8gZWwuaGFzaC5yZXBsYWNlKC9eIy8sICcnKSA6ICcnXG4gICAgfTtcbn07XG5cbmZ1bmN0aW9uIGZhY3RvcnkoaGFuZGxlciwgbmV4dCwgdm0pIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKG9wdGlvbnMkJDEpIHtcbiAgICAgICAgcmV0dXJuIGhhbmRsZXIuY2FsbCh2bSwgb3B0aW9ucyQkMSwgbmV4dCk7XG4gICAgfTtcbn1cblxuZnVuY3Rpb24gc2VyaWFsaXplKHBhcmFtcywgb2JqLCBzY29wZSkge1xuXG4gICAgdmFyIGFycmF5ID0gaXNBcnJheShvYmopLCBwbGFpbiA9IGlzUGxhaW5PYmplY3Qob2JqKSwgaGFzaDtcblxuICAgIGVhY2gob2JqLCBmdW5jdGlvbiAodmFsdWUsIGtleSkge1xuXG4gICAgICAgIGhhc2ggPSBpc09iamVjdCh2YWx1ZSkgfHwgaXNBcnJheSh2YWx1ZSk7XG5cbiAgICAgICAgaWYgKHNjb3BlKSB7XG4gICAgICAgICAgICBrZXkgPSBzY29wZSArICdbJyArIChwbGFpbiB8fCBoYXNoID8ga2V5IDogJycpICsgJ10nO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFzY29wZSAmJiBhcnJheSkge1xuICAgICAgICAgICAgcGFyYW1zLmFkZCh2YWx1ZS5uYW1lLCB2YWx1ZS52YWx1ZSk7XG4gICAgICAgIH0gZWxzZSBpZiAoaGFzaCkge1xuICAgICAgICAgICAgc2VyaWFsaXplKHBhcmFtcywgdmFsdWUsIGtleSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBwYXJhbXMuYWRkKGtleSwgdmFsdWUpO1xuICAgICAgICB9XG4gICAgfSk7XG59XG5cbi8qKlxuICogWERvbWFpbiBjbGllbnQgKEludGVybmV0IEV4cGxvcmVyKS5cbiAqL1xuXG52YXIgeGRyQ2xpZW50ID0gZnVuY3Rpb24gKHJlcXVlc3QpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2VPYmooZnVuY3Rpb24gKHJlc29sdmUpIHtcblxuICAgICAgICB2YXIgeGRyID0gbmV3IFhEb21haW5SZXF1ZXN0KCksIGhhbmRsZXIgPSBmdW5jdGlvbiAocmVmKSB7XG4gICAgICAgICAgICB2YXIgdHlwZSA9IHJlZi50eXBlO1xuXG5cbiAgICAgICAgICAgIHZhciBzdGF0dXMgPSAwO1xuXG4gICAgICAgICAgICBpZiAodHlwZSA9PT0gJ2xvYWQnKSB7XG4gICAgICAgICAgICAgICAgc3RhdHVzID0gMjAwO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlID09PSAnZXJyb3InKSB7XG4gICAgICAgICAgICAgICAgc3RhdHVzID0gNTAwO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXNvbHZlKHJlcXVlc3QucmVzcG9uZFdpdGgoeGRyLnJlc3BvbnNlVGV4dCwge3N0YXR1czogc3RhdHVzfSkpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJlcXVlc3QuYWJvcnQgPSBmdW5jdGlvbiAoKSB7IHJldHVybiB4ZHIuYWJvcnQoKTsgfTtcblxuICAgICAgICB4ZHIub3BlbihyZXF1ZXN0Lm1ldGhvZCwgcmVxdWVzdC5nZXRVcmwoKSk7XG5cbiAgICAgICAgaWYgKHJlcXVlc3QudGltZW91dCkge1xuICAgICAgICAgICAgeGRyLnRpbWVvdXQgPSByZXF1ZXN0LnRpbWVvdXQ7XG4gICAgICAgIH1cblxuICAgICAgICB4ZHIub25sb2FkID0gaGFuZGxlcjtcbiAgICAgICAgeGRyLm9uYWJvcnQgPSBoYW5kbGVyO1xuICAgICAgICB4ZHIub25lcnJvciA9IGhhbmRsZXI7XG4gICAgICAgIHhkci5vbnRpbWVvdXQgPSBoYW5kbGVyO1xuICAgICAgICB4ZHIub25wcm9ncmVzcyA9IGZ1bmN0aW9uICgpIHt9O1xuICAgICAgICB4ZHIuc2VuZChyZXF1ZXN0LmdldEJvZHkoKSk7XG4gICAgfSk7XG59O1xuXG4vKipcbiAqIENPUlMgSW50ZXJjZXB0b3IuXG4gKi9cblxudmFyIFNVUFBPUlRTX0NPUlMgPSBpbkJyb3dzZXIgJiYgJ3dpdGhDcmVkZW50aWFscycgaW4gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG5cbnZhciBjb3JzID0gZnVuY3Rpb24gKHJlcXVlc3QsIG5leHQpIHtcblxuICAgIGlmIChpbkJyb3dzZXIpIHtcblxuICAgICAgICB2YXIgb3JnVXJsID0gVXJsLnBhcnNlKGxvY2F0aW9uLmhyZWYpO1xuICAgICAgICB2YXIgcmVxVXJsID0gVXJsLnBhcnNlKHJlcXVlc3QuZ2V0VXJsKCkpO1xuXG4gICAgICAgIGlmIChyZXFVcmwucHJvdG9jb2wgIT09IG9yZ1VybC5wcm90b2NvbCB8fCByZXFVcmwuaG9zdCAhPT0gb3JnVXJsLmhvc3QpIHtcblxuICAgICAgICAgICAgcmVxdWVzdC5jcm9zc09yaWdpbiA9IHRydWU7XG4gICAgICAgICAgICByZXF1ZXN0LmVtdWxhdGVIVFRQID0gZmFsc2U7XG5cbiAgICAgICAgICAgIGlmICghU1VQUE9SVFNfQ09SUykge1xuICAgICAgICAgICAgICAgIHJlcXVlc3QuY2xpZW50ID0geGRyQ2xpZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgbmV4dCgpO1xufTtcblxuLyoqXG4gKiBGb3JtIGRhdGEgSW50ZXJjZXB0b3IuXG4gKi9cblxudmFyIGZvcm0gPSBmdW5jdGlvbiAocmVxdWVzdCwgbmV4dCkge1xuXG4gICAgaWYgKGlzRm9ybURhdGEocmVxdWVzdC5ib2R5KSkge1xuXG4gICAgICAgIHJlcXVlc3QuaGVhZGVycy5kZWxldGUoJ0NvbnRlbnQtVHlwZScpO1xuXG4gICAgfSBlbHNlIGlmIChpc09iamVjdChyZXF1ZXN0LmJvZHkpICYmIHJlcXVlc3QuZW11bGF0ZUpTT04pIHtcblxuICAgICAgICByZXF1ZXN0LmJvZHkgPSBVcmwucGFyYW1zKHJlcXVlc3QuYm9keSk7XG4gICAgICAgIHJlcXVlc3QuaGVhZGVycy5zZXQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnKTtcbiAgICB9XG5cbiAgICBuZXh0KCk7XG59O1xuXG4vKipcbiAqIEpTT04gSW50ZXJjZXB0b3IuXG4gKi9cblxudmFyIGpzb24gPSBmdW5jdGlvbiAocmVxdWVzdCwgbmV4dCkge1xuXG4gICAgdmFyIHR5cGUgPSByZXF1ZXN0LmhlYWRlcnMuZ2V0KCdDb250ZW50LVR5cGUnKSB8fCAnJztcblxuICAgIGlmIChpc09iamVjdChyZXF1ZXN0LmJvZHkpICYmIHR5cGUuaW5kZXhPZignYXBwbGljYXRpb24vanNvbicpID09PSAwKSB7XG4gICAgICAgIHJlcXVlc3QuYm9keSA9IEpTT04uc3RyaW5naWZ5KHJlcXVlc3QuYm9keSk7XG4gICAgfVxuXG4gICAgbmV4dChmdW5jdGlvbiAocmVzcG9uc2UpIHtcblxuICAgICAgICByZXR1cm4gcmVzcG9uc2UuYm9keVRleHQgPyB3aGVuKHJlc3BvbnNlLnRleHQoKSwgZnVuY3Rpb24gKHRleHQpIHtcblxuICAgICAgICAgICAgdHlwZSA9IHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCdDb250ZW50LVR5cGUnKSB8fCAnJztcblxuICAgICAgICAgICAgaWYgKHR5cGUuaW5kZXhPZignYXBwbGljYXRpb24vanNvbicpID09PSAwIHx8IGlzSnNvbih0ZXh0KSkge1xuXG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuYm9keSA9IEpTT04ucGFyc2UodGV4dCk7XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5ib2R5ID0gbnVsbDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmVzcG9uc2UuYm9keSA9IHRleHQ7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcblxuICAgICAgICB9KSA6IHJlc3BvbnNlO1xuXG4gICAgfSk7XG59O1xuXG5mdW5jdGlvbiBpc0pzb24oc3RyKSB7XG5cbiAgICB2YXIgc3RhcnQgPSBzdHIubWF0Y2goL15cXFt8Xlxceyg/IVxceykvKSwgZW5kID0geydbJzogL10kLywgJ3snOiAvfSQvfTtcblxuICAgIHJldHVybiBzdGFydCAmJiBlbmRbc3RhcnRbMF1dLnRlc3Qoc3RyKTtcbn1cblxuLyoqXG4gKiBKU09OUCBjbGllbnQgKEJyb3dzZXIpLlxuICovXG5cbnZhciBqc29ucENsaWVudCA9IGZ1bmN0aW9uIChyZXF1ZXN0KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG5cbiAgICAgICAgdmFyIG5hbWUgPSByZXF1ZXN0Lmpzb25wIHx8ICdjYWxsYmFjaycsIGNhbGxiYWNrID0gcmVxdWVzdC5qc29ucENhbGxiYWNrIHx8ICdfanNvbnAnICsgTWF0aC5yYW5kb20oKS50b1N0cmluZygzNikuc3Vic3RyKDIpLCBib2R5ID0gbnVsbCwgaGFuZGxlciwgc2NyaXB0O1xuXG4gICAgICAgIGhhbmRsZXIgPSBmdW5jdGlvbiAocmVmKSB7XG4gICAgICAgICAgICB2YXIgdHlwZSA9IHJlZi50eXBlO1xuXG5cbiAgICAgICAgICAgIHZhciBzdGF0dXMgPSAwO1xuXG4gICAgICAgICAgICBpZiAodHlwZSA9PT0gJ2xvYWQnICYmIGJvZHkgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICBzdGF0dXMgPSAyMDA7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGUgPT09ICdlcnJvcicpIHtcbiAgICAgICAgICAgICAgICBzdGF0dXMgPSA1MDA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChzdGF0dXMgJiYgd2luZG93W2NhbGxiYWNrXSkge1xuICAgICAgICAgICAgICAgIGRlbGV0ZSB3aW5kb3dbY2FsbGJhY2tdO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQoc2NyaXB0KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmVzb2x2ZShyZXF1ZXN0LnJlc3BvbmRXaXRoKGJvZHksIHtzdGF0dXM6IHN0YXR1c30pKTtcbiAgICAgICAgfTtcblxuICAgICAgICB3aW5kb3dbY2FsbGJhY2tdID0gZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgICAgICAgYm9keSA9IEpTT04uc3RyaW5naWZ5KHJlc3VsdCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgcmVxdWVzdC5hYm9ydCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGhhbmRsZXIoe3R5cGU6ICdhYm9ydCd9KTtcbiAgICAgICAgfTtcblxuICAgICAgICByZXF1ZXN0LnBhcmFtc1tuYW1lXSA9IGNhbGxiYWNrO1xuXG4gICAgICAgIGlmIChyZXF1ZXN0LnRpbWVvdXQpIHtcbiAgICAgICAgICAgIHNldFRpbWVvdXQocmVxdWVzdC5hYm9ydCwgcmVxdWVzdC50aW1lb3V0KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHNjcmlwdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgICBzY3JpcHQuc3JjID0gcmVxdWVzdC5nZXRVcmwoKTtcbiAgICAgICAgc2NyaXB0LnR5cGUgPSAndGV4dC9qYXZhc2NyaXB0JztcbiAgICAgICAgc2NyaXB0LmFzeW5jID0gdHJ1ZTtcbiAgICAgICAgc2NyaXB0Lm9ubG9hZCA9IGhhbmRsZXI7XG4gICAgICAgIHNjcmlwdC5vbmVycm9yID0gaGFuZGxlcjtcblxuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcmlwdCk7XG4gICAgfSk7XG59O1xuXG4vKipcbiAqIEpTT05QIEludGVyY2VwdG9yLlxuICovXG5cbnZhciBqc29ucCA9IGZ1bmN0aW9uIChyZXF1ZXN0LCBuZXh0KSB7XG5cbiAgICBpZiAocmVxdWVzdC5tZXRob2QgPT0gJ0pTT05QJykge1xuICAgICAgICByZXF1ZXN0LmNsaWVudCA9IGpzb25wQ2xpZW50O1xuICAgIH1cblxuICAgIG5leHQoKTtcbn07XG5cbi8qKlxuICogQmVmb3JlIEludGVyY2VwdG9yLlxuICovXG5cbnZhciBiZWZvcmUgPSBmdW5jdGlvbiAocmVxdWVzdCwgbmV4dCkge1xuXG4gICAgaWYgKGlzRnVuY3Rpb24ocmVxdWVzdC5iZWZvcmUpKSB7XG4gICAgICAgIHJlcXVlc3QuYmVmb3JlLmNhbGwodGhpcywgcmVxdWVzdCk7XG4gICAgfVxuXG4gICAgbmV4dCgpO1xufTtcblxuLyoqXG4gKiBIVFRQIG1ldGhvZCBvdmVycmlkZSBJbnRlcmNlcHRvci5cbiAqL1xuXG52YXIgbWV0aG9kID0gZnVuY3Rpb24gKHJlcXVlc3QsIG5leHQpIHtcblxuICAgIGlmIChyZXF1ZXN0LmVtdWxhdGVIVFRQICYmIC9eKFBVVHxQQVRDSHxERUxFVEUpJC9pLnRlc3QocmVxdWVzdC5tZXRob2QpKSB7XG4gICAgICAgIHJlcXVlc3QuaGVhZGVycy5zZXQoJ1gtSFRUUC1NZXRob2QtT3ZlcnJpZGUnLCByZXF1ZXN0Lm1ldGhvZCk7XG4gICAgICAgIHJlcXVlc3QubWV0aG9kID0gJ1BPU1QnO1xuICAgIH1cblxuICAgIG5leHQoKTtcbn07XG5cbi8qKlxuICogSGVhZGVyIEludGVyY2VwdG9yLlxuICovXG5cbnZhciBoZWFkZXIgPSBmdW5jdGlvbiAocmVxdWVzdCwgbmV4dCkge1xuXG4gICAgdmFyIGhlYWRlcnMgPSBhc3NpZ24oe30sIEh0dHAuaGVhZGVycy5jb21tb24sXG4gICAgICAgICFyZXF1ZXN0LmNyb3NzT3JpZ2luID8gSHR0cC5oZWFkZXJzLmN1c3RvbSA6IHt9LFxuICAgICAgICBIdHRwLmhlYWRlcnNbdG9Mb3dlcihyZXF1ZXN0Lm1ldGhvZCldXG4gICAgKTtcblxuICAgIGVhY2goaGVhZGVycywgZnVuY3Rpb24gKHZhbHVlLCBuYW1lKSB7XG4gICAgICAgIGlmICghcmVxdWVzdC5oZWFkZXJzLmhhcyhuYW1lKSkge1xuICAgICAgICAgICAgcmVxdWVzdC5oZWFkZXJzLnNldChuYW1lLCB2YWx1ZSk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIG5leHQoKTtcbn07XG5cbi8qKlxuICogWE1MSHR0cCBjbGllbnQgKEJyb3dzZXIpLlxuICovXG5cbnZhciB4aHJDbGllbnQgPSBmdW5jdGlvbiAocmVxdWVzdCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZU9iaihmdW5jdGlvbiAocmVzb2x2ZSkge1xuXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKSwgaGFuZGxlciA9IGZ1bmN0aW9uIChldmVudCkge1xuXG4gICAgICAgICAgICB2YXIgcmVzcG9uc2UgPSByZXF1ZXN0LnJlc3BvbmRXaXRoKFxuICAgICAgICAgICAgICAgICdyZXNwb25zZScgaW4geGhyID8geGhyLnJlc3BvbnNlIDogeGhyLnJlc3BvbnNlVGV4dCwge1xuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHhoci5zdGF0dXMgPT09IDEyMjMgPyAyMDQgOiB4aHIuc3RhdHVzLCAvLyBJRTkgc3RhdHVzIGJ1Z1xuICAgICAgICAgICAgICAgICAgICBzdGF0dXNUZXh0OiB4aHIuc3RhdHVzID09PSAxMjIzID8gJ05vIENvbnRlbnQnIDogdHJpbSh4aHIuc3RhdHVzVGV4dClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuXG4gICAgICAgICAgICBlYWNoKHRyaW0oeGhyLmdldEFsbFJlc3BvbnNlSGVhZGVycygpKS5zcGxpdCgnXFxuJyksIGZ1bmN0aW9uIChyb3cpIHtcbiAgICAgICAgICAgICAgICByZXNwb25zZS5oZWFkZXJzLmFwcGVuZChyb3cuc2xpY2UoMCwgcm93LmluZGV4T2YoJzonKSksIHJvdy5zbGljZShyb3cuaW5kZXhPZignOicpICsgMSkpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJlcXVlc3QuYWJvcnQgPSBmdW5jdGlvbiAoKSB7IHJldHVybiB4aHIuYWJvcnQoKTsgfTtcblxuICAgICAgICBpZiAocmVxdWVzdC5wcm9ncmVzcykge1xuICAgICAgICAgICAgaWYgKHJlcXVlc3QubWV0aG9kID09PSAnR0VUJykge1xuICAgICAgICAgICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKCdwcm9ncmVzcycsIHJlcXVlc3QucHJvZ3Jlc3MpO1xuICAgICAgICAgICAgfSBlbHNlIGlmICgvXihQT1NUfFBVVCkkL2kudGVzdChyZXF1ZXN0Lm1ldGhvZCkpIHtcbiAgICAgICAgICAgICAgICB4aHIudXBsb2FkLmFkZEV2ZW50TGlzdGVuZXIoJ3Byb2dyZXNzJywgcmVxdWVzdC5wcm9ncmVzcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB4aHIub3BlbihyZXF1ZXN0Lm1ldGhvZCwgcmVxdWVzdC5nZXRVcmwoKSwgdHJ1ZSk7XG5cbiAgICAgICAgaWYgKHJlcXVlc3QudGltZW91dCkge1xuICAgICAgICAgICAgeGhyLnRpbWVvdXQgPSByZXF1ZXN0LnRpbWVvdXQ7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocmVxdWVzdC5yZXNwb25zZVR5cGUgJiYgJ3Jlc3BvbnNlVHlwZScgaW4geGhyKSB7XG4gICAgICAgICAgICB4aHIucmVzcG9uc2VUeXBlID0gcmVxdWVzdC5yZXNwb25zZVR5cGU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocmVxdWVzdC53aXRoQ3JlZGVudGlhbHMgfHwgcmVxdWVzdC5jcmVkZW50aWFscykge1xuICAgICAgICAgICAgeGhyLndpdGhDcmVkZW50aWFscyA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIXJlcXVlc3QuY3Jvc3NPcmlnaW4pIHtcbiAgICAgICAgICAgIHJlcXVlc3QuaGVhZGVycy5zZXQoJ1gtUmVxdWVzdGVkLVdpdGgnLCAnWE1MSHR0cFJlcXVlc3QnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlcXVlc3QuaGVhZGVycy5mb3JFYWNoKGZ1bmN0aW9uICh2YWx1ZSwgbmFtZSkge1xuICAgICAgICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIobmFtZSwgdmFsdWUpO1xuICAgICAgICB9KTtcblxuICAgICAgICB4aHIub25sb2FkID0gaGFuZGxlcjtcbiAgICAgICAgeGhyLm9uYWJvcnQgPSBoYW5kbGVyO1xuICAgICAgICB4aHIub25lcnJvciA9IGhhbmRsZXI7XG4gICAgICAgIHhoci5vbnRpbWVvdXQgPSBoYW5kbGVyO1xuICAgICAgICB4aHIuc2VuZChyZXF1ZXN0LmdldEJvZHkoKSk7XG4gICAgfSk7XG59O1xuXG4vKipcbiAqIEh0dHAgY2xpZW50IChOb2RlKS5cbiAqL1xuXG52YXIgbm9kZUNsaWVudCA9IGZ1bmN0aW9uIChyZXF1ZXN0KSB7XG5cbiAgICB2YXIgY2xpZW50ID0gcmVxdWlyZSgnZ290Jyk7XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2VPYmooZnVuY3Rpb24gKHJlc29sdmUpIHtcblxuICAgICAgICB2YXIgdXJsID0gcmVxdWVzdC5nZXRVcmwoKTtcbiAgICAgICAgdmFyIGJvZHkgPSByZXF1ZXN0LmdldEJvZHkoKTtcbiAgICAgICAgdmFyIG1ldGhvZCA9IHJlcXVlc3QubWV0aG9kO1xuICAgICAgICB2YXIgaGVhZGVycyA9IHt9LCBoYW5kbGVyO1xuXG4gICAgICAgIHJlcXVlc3QuaGVhZGVycy5mb3JFYWNoKGZ1bmN0aW9uICh2YWx1ZSwgbmFtZSkge1xuICAgICAgICAgICAgaGVhZGVyc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICB9KTtcblxuICAgICAgICBjbGllbnQodXJsLCB7Ym9keTogYm9keSwgbWV0aG9kOiBtZXRob2QsIGhlYWRlcnM6IGhlYWRlcnN9KS50aGVuKGhhbmRsZXIgPSBmdW5jdGlvbiAocmVzcCkge1xuXG4gICAgICAgICAgICB2YXIgcmVzcG9uc2UgPSByZXF1ZXN0LnJlc3BvbmRXaXRoKHJlc3AuYm9keSwge1xuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHJlc3Auc3RhdHVzQ29kZSxcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzVGV4dDogdHJpbShyZXNwLnN0YXR1c01lc3NhZ2UpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgZWFjaChyZXNwLmhlYWRlcnMsIGZ1bmN0aW9uICh2YWx1ZSwgbmFtZSkge1xuICAgICAgICAgICAgICAgIHJlc3BvbnNlLmhlYWRlcnMuc2V0KG5hbWUsIHZhbHVlKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICByZXNvbHZlKHJlc3BvbnNlKTtcblxuICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IkJDEpIHsgcmV0dXJuIGhhbmRsZXIoZXJyb3IkJDEucmVzcG9uc2UpOyB9KTtcbiAgICB9KTtcbn07XG5cbi8qKlxuICogQmFzZSBjbGllbnQuXG4gKi9cblxudmFyIENsaWVudCA9IGZ1bmN0aW9uIChjb250ZXh0KSB7XG5cbiAgICB2YXIgcmVxSGFuZGxlcnMgPSBbc2VuZFJlcXVlc3RdLCByZXNIYW5kbGVycyA9IFtdLCBoYW5kbGVyO1xuXG4gICAgaWYgKCFpc09iamVjdChjb250ZXh0KSkge1xuICAgICAgICBjb250ZXh0ID0gbnVsbDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBDbGllbnQocmVxdWVzdCkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2VPYmooZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuXG4gICAgICAgICAgICBmdW5jdGlvbiBleGVjKCkge1xuXG4gICAgICAgICAgICAgICAgaGFuZGxlciA9IHJlcUhhbmRsZXJzLnBvcCgpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGlzRnVuY3Rpb24oaGFuZGxlcikpIHtcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlci5jYWxsKGNvbnRleHQsIHJlcXVlc3QsIG5leHQpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHdhcm4oKFwiSW52YWxpZCBpbnRlcmNlcHRvciBvZiB0eXBlIFwiICsgKHR5cGVvZiBoYW5kbGVyKSArIFwiLCBtdXN0IGJlIGEgZnVuY3Rpb25cIikpO1xuICAgICAgICAgICAgICAgICAgICBuZXh0KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBuZXh0KHJlc3BvbnNlKSB7XG5cbiAgICAgICAgICAgICAgICBpZiAoaXNGdW5jdGlvbihyZXNwb25zZSkpIHtcblxuICAgICAgICAgICAgICAgICAgICByZXNIYW5kbGVycy51bnNoaWZ0KHJlc3BvbnNlKTtcblxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoaXNPYmplY3QocmVzcG9uc2UpKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgcmVzSGFuZGxlcnMuZm9yRWFjaChmdW5jdGlvbiAoaGFuZGxlcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgPSB3aGVuKHJlc3BvbnNlLCBmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gaGFuZGxlci5jYWxsKGNvbnRleHQsIHJlc3BvbnNlKSB8fCByZXNwb25zZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIHJlamVjdCk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHdoZW4ocmVzcG9uc2UsIHJlc29sdmUsIHJlamVjdCk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGV4ZWMoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZXhlYygpO1xuXG4gICAgICAgIH0sIGNvbnRleHQpO1xuICAgIH1cblxuICAgIENsaWVudC51c2UgPSBmdW5jdGlvbiAoaGFuZGxlcikge1xuICAgICAgICByZXFIYW5kbGVycy5wdXNoKGhhbmRsZXIpO1xuICAgIH07XG5cbiAgICByZXR1cm4gQ2xpZW50O1xufTtcblxuZnVuY3Rpb24gc2VuZFJlcXVlc3QocmVxdWVzdCwgcmVzb2x2ZSkge1xuXG4gICAgdmFyIGNsaWVudCA9IHJlcXVlc3QuY2xpZW50IHx8IChpbkJyb3dzZXIgPyB4aHJDbGllbnQgOiBub2RlQ2xpZW50KTtcblxuICAgIHJlc29sdmUoY2xpZW50KHJlcXVlc3QpKTtcbn1cblxuLyoqXG4gKiBIVFRQIEhlYWRlcnMuXG4gKi9cblxudmFyIEhlYWRlcnMgPSBmdW5jdGlvbiBIZWFkZXJzKGhlYWRlcnMpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuXG4gICAgdGhpcy5tYXAgPSB7fTtcblxuICAgIGVhY2goaGVhZGVycywgZnVuY3Rpb24gKHZhbHVlLCBuYW1lKSB7IHJldHVybiB0aGlzJDEuYXBwZW5kKG5hbWUsIHZhbHVlKTsgfSk7XG59O1xuXG5IZWFkZXJzLnByb3RvdHlwZS5oYXMgPSBmdW5jdGlvbiBoYXMgKG5hbWUpIHtcbiAgICByZXR1cm4gZ2V0TmFtZSh0aGlzLm1hcCwgbmFtZSkgIT09IG51bGw7XG59O1xuXG5IZWFkZXJzLnByb3RvdHlwZS5nZXQgPSBmdW5jdGlvbiBnZXQgKG5hbWUpIHtcblxuICAgIHZhciBsaXN0ID0gdGhpcy5tYXBbZ2V0TmFtZSh0aGlzLm1hcCwgbmFtZSldO1xuXG4gICAgcmV0dXJuIGxpc3QgPyBsaXN0LmpvaW4oKSA6IG51bGw7XG59O1xuXG5IZWFkZXJzLnByb3RvdHlwZS5nZXRBbGwgPSBmdW5jdGlvbiBnZXRBbGwgKG5hbWUpIHtcbiAgICByZXR1cm4gdGhpcy5tYXBbZ2V0TmFtZSh0aGlzLm1hcCwgbmFtZSldIHx8IFtdO1xufTtcblxuSGVhZGVycy5wcm90b3R5cGUuc2V0ID0gZnVuY3Rpb24gc2V0IChuYW1lLCB2YWx1ZSkge1xuICAgIHRoaXMubWFwW25vcm1hbGl6ZU5hbWUoZ2V0TmFtZSh0aGlzLm1hcCwgbmFtZSkgfHwgbmFtZSldID0gW3RyaW0odmFsdWUpXTtcbn07XG5cbkhlYWRlcnMucHJvdG90eXBlLmFwcGVuZCA9IGZ1bmN0aW9uIGFwcGVuZCAobmFtZSwgdmFsdWUpe1xuXG4gICAgdmFyIGxpc3QgPSB0aGlzLm1hcFtnZXROYW1lKHRoaXMubWFwLCBuYW1lKV07XG5cbiAgICBpZiAobGlzdCkge1xuICAgICAgICBsaXN0LnB1c2godHJpbSh2YWx1ZSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc2V0KG5hbWUsIHZhbHVlKTtcbiAgICB9XG59O1xuXG5IZWFkZXJzLnByb3RvdHlwZS5kZWxldGUgPSBmdW5jdGlvbiBkZWxldGUkMSAobmFtZSl7XG4gICAgZGVsZXRlIHRoaXMubWFwW2dldE5hbWUodGhpcy5tYXAsIG5hbWUpXTtcbn07XG5cbkhlYWRlcnMucHJvdG90eXBlLmRlbGV0ZUFsbCA9IGZ1bmN0aW9uIGRlbGV0ZUFsbCAoKXtcbiAgICB0aGlzLm1hcCA9IHt9O1xufTtcblxuSGVhZGVycy5wcm90b3R5cGUuZm9yRWFjaCA9IGZ1bmN0aW9uIGZvckVhY2ggKGNhbGxiYWNrLCB0aGlzQXJnKSB7XG4gICAgICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgZWFjaCh0aGlzLm1hcCwgZnVuY3Rpb24gKGxpc3QsIG5hbWUpIHtcbiAgICAgICAgZWFjaChsaXN0LCBmdW5jdGlvbiAodmFsdWUpIHsgcmV0dXJuIGNhbGxiYWNrLmNhbGwodGhpc0FyZywgdmFsdWUsIG5hbWUsIHRoaXMkMSk7IH0pO1xuICAgIH0pO1xufTtcblxuZnVuY3Rpb24gZ2V0TmFtZShtYXAsIG5hbWUpIHtcbiAgICByZXR1cm4gT2JqZWN0LmtleXMobWFwKS5yZWR1Y2UoZnVuY3Rpb24gKHByZXYsIGN1cnIpIHtcbiAgICAgICAgcmV0dXJuIHRvTG93ZXIobmFtZSkgPT09IHRvTG93ZXIoY3VycikgPyBjdXJyIDogcHJldjtcbiAgICB9LCBudWxsKTtcbn1cblxuZnVuY3Rpb24gbm9ybWFsaXplTmFtZShuYW1lKSB7XG5cbiAgICBpZiAoL1teYS16MC05XFwtIyQlJicqKy5cXF5fYHx+XS9pLnRlc3QobmFtZSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignSW52YWxpZCBjaGFyYWN0ZXIgaW4gaGVhZGVyIGZpZWxkIG5hbWUnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdHJpbShuYW1lKTtcbn1cblxuLyoqXG4gKiBIVFRQIFJlc3BvbnNlLlxuICovXG5cbnZhciBSZXNwb25zZSA9IGZ1bmN0aW9uIFJlc3BvbnNlKGJvZHksIHJlZikge1xuICAgIHZhciB1cmwgPSByZWYudXJsO1xuICAgIHZhciBoZWFkZXJzID0gcmVmLmhlYWRlcnM7XG4gICAgdmFyIHN0YXR1cyA9IHJlZi5zdGF0dXM7XG4gICAgdmFyIHN0YXR1c1RleHQgPSByZWYuc3RhdHVzVGV4dDtcblxuXG4gICAgdGhpcy51cmwgPSB1cmw7XG4gICAgdGhpcy5vayA9IHN0YXR1cyA+PSAyMDAgJiYgc3RhdHVzIDwgMzAwO1xuICAgIHRoaXMuc3RhdHVzID0gc3RhdHVzIHx8IDA7XG4gICAgdGhpcy5zdGF0dXNUZXh0ID0gc3RhdHVzVGV4dCB8fCAnJztcbiAgICB0aGlzLmhlYWRlcnMgPSBuZXcgSGVhZGVycyhoZWFkZXJzKTtcbiAgICB0aGlzLmJvZHkgPSBib2R5O1xuXG4gICAgaWYgKGlzU3RyaW5nKGJvZHkpKSB7XG5cbiAgICAgICAgdGhpcy5ib2R5VGV4dCA9IGJvZHk7XG5cbiAgICB9IGVsc2UgaWYgKGlzQmxvYihib2R5KSkge1xuXG4gICAgICAgIHRoaXMuYm9keUJsb2IgPSBib2R5O1xuXG4gICAgICAgIGlmIChpc0Jsb2JUZXh0KGJvZHkpKSB7XG4gICAgICAgICAgICB0aGlzLmJvZHlUZXh0ID0gYmxvYlRleHQoYm9keSk7XG4gICAgICAgIH1cbiAgICB9XG59O1xuXG5SZXNwb25zZS5wcm90b3R5cGUuYmxvYiA9IGZ1bmN0aW9uIGJsb2IgKCkge1xuICAgIHJldHVybiB3aGVuKHRoaXMuYm9keUJsb2IpO1xufTtcblxuUmVzcG9uc2UucHJvdG90eXBlLnRleHQgPSBmdW5jdGlvbiB0ZXh0ICgpIHtcbiAgICByZXR1cm4gd2hlbih0aGlzLmJvZHlUZXh0KTtcbn07XG5cblJlc3BvbnNlLnByb3RvdHlwZS5qc29uID0gZnVuY3Rpb24ganNvbiAoKSB7XG4gICAgcmV0dXJuIHdoZW4odGhpcy50ZXh0KCksIGZ1bmN0aW9uICh0ZXh0KSB7IHJldHVybiBKU09OLnBhcnNlKHRleHQpOyB9KTtcbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShSZXNwb25zZS5wcm90b3R5cGUsICdkYXRhJywge1xuXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmJvZHk7XG4gICAgfSxcblxuICAgIHNldDogZnVuY3Rpb24gc2V0KGJvZHkpIHtcbiAgICAgICAgdGhpcy5ib2R5ID0gYm9keTtcbiAgICB9XG5cbn0pO1xuXG5mdW5jdGlvbiBibG9iVGV4dChib2R5KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG5cbiAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG5cbiAgICAgICAgcmVhZGVyLnJlYWRBc1RleHQoYm9keSk7XG4gICAgICAgIHJlYWRlci5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXNvbHZlKHJlYWRlci5yZXN1bHQpO1xuICAgICAgICB9O1xuXG4gICAgfSk7XG59XG5cbmZ1bmN0aW9uIGlzQmxvYlRleHQoYm9keSkge1xuICAgIHJldHVybiBib2R5LnR5cGUuaW5kZXhPZigndGV4dCcpID09PSAwIHx8IGJvZHkudHlwZS5pbmRleE9mKCdqc29uJykgIT09IC0xO1xufVxuXG4vKipcbiAqIEhUVFAgUmVxdWVzdC5cbiAqL1xuXG52YXIgUmVxdWVzdCA9IGZ1bmN0aW9uIFJlcXVlc3Qob3B0aW9ucyQkMSkge1xuXG4gICAgdGhpcy5ib2R5ID0gbnVsbDtcbiAgICB0aGlzLnBhcmFtcyA9IHt9O1xuXG4gICAgYXNzaWduKHRoaXMsIG9wdGlvbnMkJDEsIHtcbiAgICAgICAgbWV0aG9kOiB0b1VwcGVyKG9wdGlvbnMkJDEubWV0aG9kIHx8ICdHRVQnKVxuICAgIH0pO1xuXG4gICAgaWYgKCEodGhpcy5oZWFkZXJzIGluc3RhbmNlb2YgSGVhZGVycykpIHtcbiAgICAgICAgdGhpcy5oZWFkZXJzID0gbmV3IEhlYWRlcnModGhpcy5oZWFkZXJzKTtcbiAgICB9XG59O1xuXG5SZXF1ZXN0LnByb3RvdHlwZS5nZXRVcmwgPSBmdW5jdGlvbiBnZXRVcmwgKCl7XG4gICAgcmV0dXJuIFVybCh0aGlzKTtcbn07XG5cblJlcXVlc3QucHJvdG90eXBlLmdldEJvZHkgPSBmdW5jdGlvbiBnZXRCb2R5ICgpe1xuICAgIHJldHVybiB0aGlzLmJvZHk7XG59O1xuXG5SZXF1ZXN0LnByb3RvdHlwZS5yZXNwb25kV2l0aCA9IGZ1bmN0aW9uIHJlc3BvbmRXaXRoIChib2R5LCBvcHRpb25zJCQxKSB7XG4gICAgcmV0dXJuIG5ldyBSZXNwb25zZShib2R5LCBhc3NpZ24ob3B0aW9ucyQkMSB8fCB7fSwge3VybDogdGhpcy5nZXRVcmwoKX0pKTtcbn07XG5cbi8qKlxuICogU2VydmljZSBmb3Igc2VuZGluZyBuZXR3b3JrIHJlcXVlc3RzLlxuICovXG5cbnZhciBDT01NT05fSEVBREVSUyA9IHsnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24sIHRleHQvcGxhaW4sICovKid9O1xudmFyIEpTT05fQ09OVEVOVF9UWVBFID0geydDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04J307XG5cbmZ1bmN0aW9uIEh0dHAob3B0aW9ucyQkMSkge1xuXG4gICAgdmFyIHNlbGYgPSB0aGlzIHx8IHt9LCBjbGllbnQgPSBDbGllbnQoc2VsZi4kdm0pO1xuXG4gICAgZGVmYXVsdHMob3B0aW9ucyQkMSB8fCB7fSwgc2VsZi4kb3B0aW9ucywgSHR0cC5vcHRpb25zKTtcblxuICAgIEh0dHAuaW50ZXJjZXB0b3JzLmZvckVhY2goZnVuY3Rpb24gKGhhbmRsZXIpIHtcblxuICAgICAgICBpZiAoaXNTdHJpbmcoaGFuZGxlcikpIHtcbiAgICAgICAgICAgIGhhbmRsZXIgPSBIdHRwLmludGVyY2VwdG9yW2hhbmRsZXJdO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGlzRnVuY3Rpb24oaGFuZGxlcikpIHtcbiAgICAgICAgICAgIGNsaWVudC51c2UoaGFuZGxlcik7XG4gICAgICAgIH1cblxuICAgIH0pO1xuXG4gICAgcmV0dXJuIGNsaWVudChuZXcgUmVxdWVzdChvcHRpb25zJCQxKSkudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcblxuICAgICAgICByZXR1cm4gcmVzcG9uc2Uub2sgPyByZXNwb25zZSA6IFByb21pc2VPYmoucmVqZWN0KHJlc3BvbnNlKTtcblxuICAgIH0sIGZ1bmN0aW9uIChyZXNwb25zZSkge1xuXG4gICAgICAgIGlmIChyZXNwb25zZSBpbnN0YW5jZW9mIEVycm9yKSB7XG4gICAgICAgICAgICBlcnJvcihyZXNwb25zZSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gUHJvbWlzZU9iai5yZWplY3QocmVzcG9uc2UpO1xuICAgIH0pO1xufVxuXG5IdHRwLm9wdGlvbnMgPSB7fTtcblxuSHR0cC5oZWFkZXJzID0ge1xuICAgIHB1dDogSlNPTl9DT05URU5UX1RZUEUsXG4gICAgcG9zdDogSlNPTl9DT05URU5UX1RZUEUsXG4gICAgcGF0Y2g6IEpTT05fQ09OVEVOVF9UWVBFLFxuICAgIGRlbGV0ZTogSlNPTl9DT05URU5UX1RZUEUsXG4gICAgY29tbW9uOiBDT01NT05fSEVBREVSUyxcbiAgICBjdXN0b206IHt9XG59O1xuXG5IdHRwLmludGVyY2VwdG9yID0ge2JlZm9yZTogYmVmb3JlLCBtZXRob2Q6IG1ldGhvZCwganNvbnA6IGpzb25wLCBqc29uOiBqc29uLCBmb3JtOiBmb3JtLCBoZWFkZXI6IGhlYWRlciwgY29yczogY29yc307XG5IdHRwLmludGVyY2VwdG9ycyA9IFsnYmVmb3JlJywgJ21ldGhvZCcsICdqc29ucCcsICdqc29uJywgJ2Zvcm0nLCAnaGVhZGVyJywgJ2NvcnMnXTtcblxuWydnZXQnLCAnZGVsZXRlJywgJ2hlYWQnLCAnanNvbnAnXS5mb3JFYWNoKGZ1bmN0aW9uIChtZXRob2QkJDEpIHtcblxuICAgIEh0dHBbbWV0aG9kJCQxXSA9IGZ1bmN0aW9uICh1cmwsIG9wdGlvbnMkJDEpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMoYXNzaWduKG9wdGlvbnMkJDEgfHwge30sIHt1cmw6IHVybCwgbWV0aG9kOiBtZXRob2QkJDF9KSk7XG4gICAgfTtcblxufSk7XG5cblsncG9zdCcsICdwdXQnLCAncGF0Y2gnXS5mb3JFYWNoKGZ1bmN0aW9uIChtZXRob2QkJDEpIHtcblxuICAgIEh0dHBbbWV0aG9kJCQxXSA9IGZ1bmN0aW9uICh1cmwsIGJvZHksIG9wdGlvbnMkJDEpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMoYXNzaWduKG9wdGlvbnMkJDEgfHwge30sIHt1cmw6IHVybCwgbWV0aG9kOiBtZXRob2QkJDEsIGJvZHk6IGJvZHl9KSk7XG4gICAgfTtcblxufSk7XG5cbi8qKlxuICogU2VydmljZSBmb3IgaW50ZXJhY3Rpbmcgd2l0aCBSRVNUZnVsIHNlcnZpY2VzLlxuICovXG5cbmZ1bmN0aW9uIFJlc291cmNlKHVybCwgcGFyYW1zLCBhY3Rpb25zLCBvcHRpb25zJCQxKSB7XG5cbiAgICB2YXIgc2VsZiA9IHRoaXMgfHwge30sIHJlc291cmNlID0ge307XG5cbiAgICBhY3Rpb25zID0gYXNzaWduKHt9LFxuICAgICAgICBSZXNvdXJjZS5hY3Rpb25zLFxuICAgICAgICBhY3Rpb25zXG4gICAgKTtcblxuICAgIGVhY2goYWN0aW9ucywgZnVuY3Rpb24gKGFjdGlvbiwgbmFtZSkge1xuXG4gICAgICAgIGFjdGlvbiA9IG1lcmdlKHt1cmw6IHVybCwgcGFyYW1zOiBhc3NpZ24oe30sIHBhcmFtcyl9LCBvcHRpb25zJCQxLCBhY3Rpb24pO1xuXG4gICAgICAgIHJlc291cmNlW25hbWVdID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIChzZWxmLiRodHRwIHx8IEh0dHApKG9wdHMoYWN0aW9uLCBhcmd1bWVudHMpKTtcbiAgICAgICAgfTtcbiAgICB9KTtcblxuICAgIHJldHVybiByZXNvdXJjZTtcbn1cblxuZnVuY3Rpb24gb3B0cyhhY3Rpb24sIGFyZ3MpIHtcblxuICAgIHZhciBvcHRpb25zJCQxID0gYXNzaWduKHt9LCBhY3Rpb24pLCBwYXJhbXMgPSB7fSwgYm9keTtcblxuICAgIHN3aXRjaCAoYXJncy5sZW5ndGgpIHtcblxuICAgICAgICBjYXNlIDI6XG5cbiAgICAgICAgICAgIHBhcmFtcyA9IGFyZ3NbMF07XG4gICAgICAgICAgICBib2R5ID0gYXJnc1sxXTtcblxuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAxOlxuXG4gICAgICAgICAgICBpZiAoL14oUE9TVHxQVVR8UEFUQ0gpJC9pLnRlc3Qob3B0aW9ucyQkMS5tZXRob2QpKSB7XG4gICAgICAgICAgICAgICAgYm9keSA9IGFyZ3NbMF07XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHBhcmFtcyA9IGFyZ3NbMF07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgMDpcblxuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgZGVmYXVsdDpcblxuICAgICAgICAgICAgdGhyb3cgJ0V4cGVjdGVkIHVwIHRvIDIgYXJndW1lbnRzIFtwYXJhbXMsIGJvZHldLCBnb3QgJyArIGFyZ3MubGVuZ3RoICsgJyBhcmd1bWVudHMnO1xuICAgIH1cblxuICAgIG9wdGlvbnMkJDEuYm9keSA9IGJvZHk7XG4gICAgb3B0aW9ucyQkMS5wYXJhbXMgPSBhc3NpZ24oe30sIG9wdGlvbnMkJDEucGFyYW1zLCBwYXJhbXMpO1xuXG4gICAgcmV0dXJuIG9wdGlvbnMkJDE7XG59XG5cblJlc291cmNlLmFjdGlvbnMgPSB7XG5cbiAgICBnZXQ6IHttZXRob2Q6ICdHRVQnfSxcbiAgICBzYXZlOiB7bWV0aG9kOiAnUE9TVCd9LFxuICAgIHF1ZXJ5OiB7bWV0aG9kOiAnR0VUJ30sXG4gICAgdXBkYXRlOiB7bWV0aG9kOiAnUFVUJ30sXG4gICAgcmVtb3ZlOiB7bWV0aG9kOiAnREVMRVRFJ30sXG4gICAgZGVsZXRlOiB7bWV0aG9kOiAnREVMRVRFJ31cblxufTtcblxuLyoqXG4gKiBJbnN0YWxsIHBsdWdpbi5cbiAqL1xuXG5mdW5jdGlvbiBwbHVnaW4oVnVlKSB7XG5cbiAgICBpZiAocGx1Z2luLmluc3RhbGxlZCkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgVXRpbChWdWUpO1xuXG4gICAgVnVlLnVybCA9IFVybDtcbiAgICBWdWUuaHR0cCA9IEh0dHA7XG4gICAgVnVlLnJlc291cmNlID0gUmVzb3VyY2U7XG4gICAgVnVlLlByb21pc2UgPSBQcm9taXNlT2JqO1xuXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnRpZXMoVnVlLnByb3RvdHlwZSwge1xuXG4gICAgICAgICR1cmw6IHtcbiAgICAgICAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBvcHRpb25zKFZ1ZS51cmwsIHRoaXMsIHRoaXMuJG9wdGlvbnMudXJsKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAkaHR0cDoge1xuICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG9wdGlvbnMoVnVlLmh0dHAsIHRoaXMsIHRoaXMuJG9wdGlvbnMuaHR0cCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgJHJlc291cmNlOiB7XG4gICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gVnVlLnJlc291cmNlLmJpbmQodGhpcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgJHByb21pc2U6IHtcbiAgICAgICAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICAgICAgICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChleGVjdXRvcikgeyByZXR1cm4gbmV3IFZ1ZS5Qcm9taXNlKGV4ZWN1dG9yLCB0aGlzJDEpOyB9O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9KTtcbn1cblxuaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5WdWUpIHtcbiAgICB3aW5kb3cuVnVlLnVzZShwbHVnaW4pO1xufVxuXG5leHBvcnQgZGVmYXVsdCBwbHVnaW47XG5leHBvcnQgeyBVcmwsIEh0dHAsIFJlc291cmNlIH07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXJlc291cmNlL2Rpc3QvdnVlLXJlc291cmNlLmVzMjAxNS5qc1xuLy8gbW9kdWxlIGlkID0gOVxuLy8gbW9kdWxlIGNodW5rcyA9IDYgOSAxMCAxNyJdLCJzb3VyY2VSb290IjoiIn0=