/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 473);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 183:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('dialog-modal', __webpack_require__(5));

var data = window.Laravel.data;

var Accounts = new Vue({
	el: '#editAccount',
	data: {
		groups: [],
		permissions: [],
		selpermissions: data.permissions,
		permissiontype: [],
		getpermissions: [],
		scheduleTypes: [],
		roles: data.roles,
		accountForm: new Form({
			id: data.id,
			avatar: data.avatar,
			first_name: data.first_name,
			middle_name: data.middle_name,
			last_name: data.last_name,
			birth_date: data.birth_date,
			gender: data.gender,
			civil_status: data.civil_status,
			height: data.height,
			weight: data.weight,
			address: data.address,
			email: data.email,
			scheduleType: data.schedule ? data.schedule.schedule_type.id : '',
			timeIn: data.schedule && data.schedule.schedule_type.id == 1 ? moment(data.schedule.time_in, 'HH:mm:ss').format("h:mm A") : '',
			timeOut: data.schedule && data.schedule.schedule_type.id == 1 ? moment(data.schedule.time_out, 'HH:mm:ss').format("h:mm A") : '',
			timeInFrom: data.schedule && data.schedule.schedule_type.id == 2 ? moment(data.schedule.time_in_from, 'HH:mm:ss').format("h:mm A") : '',
			timeInTo: data.schedule && data.schedule.schedule_type.id == 2 ? moment(data.schedule.time_in_to, 'HH:mm:ss').format("h:mm A") : ''
		}, { baseURL: 'http://' + window.Laravel.cpanel_url })
	},

	methods: {
		getScheduleTypes: function getScheduleTypes() {
			var _this = this;

			axiosAPIv1.get('/schedule-types').then(function (response) {
				_this.scheduleTypes = response.data;
			});
		},
		initTimePicker: function initTimePicker() {
			var _this2 = this;

			setTimeout(function () {
				$('.timepicker-input').timepicker();
			}, 500);

			$('.timepicker-input').on('click', function (e) {
				var target = e.target;

				$('#' + target.id).timepicker('showWidget');

				if (target.value == '') {
					$('#' + target.id).timepicker('setTime', '12:00 AM');
				}
			});

			$('.timepicker-input').timepicker().on('changeTime.timepicker', function (e) {
				var id = e.target.id;

				if (id == 'timepicker1') {
					_this2.accountForm.timeIn = e.time.value;
				} else if (id == 'timepicker2') {
					_this2.accountForm.timeOut = e.time.value;
				} else if (id == 'timepicker3') {
					_this2.accountForm.timeInFrom = e.time.value;
				} else if (id == 'timepicker4') {
					_this2.accountForm.timeInTo = e.time.value;
				}
			});
		},
		showPermissions: function showPermissions() {
			var _this3 = this;

			axios.get('/visa/access-control/getPermissions').then(function (result) {
				_this3.getpermissions = result.data;
			});

			axios.get('/visa/access-control/getPermissionType').then(function (result) {
				_this3.permissiontype = result.data;
			});

			$('#dialogPermissions').modal('toggle');
		},
		toggle: function toggle(id, e) {
			if (e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
				$('#fa-' + id).toggleClass('fa-arrow-down');
			}
		},
		selectItem: function selectItem(data) {
			if (this.check(data) == 1) {
				var sp = this.selpermissions;
				var vi = this;
				$.each(sp, function (i, item) {
					if (sp[i] != undefined) {
						if (sp[i].id == data.id) {
							vi.selpermissions.splice(i, 1);
						}
					}
				});
			} else {
				this.selpermissions.push(data);
			}

			var p = this.selpermissions;
			var sp = [];
			$.each(p, function (i, item) {
				sp.push(p[i].id);
			});
			$('#permissions').val(sp).trigger('chosen:updated');
		},
		check: function check(id) {
			var a = 0;
			var sp = this.selpermissions;
			$.each(sp, function (i, item) {
				if (sp[i].id == id.id) {
					a = 1;
				}
			});
			return a;
		},
		selectRole: function selectRole(role) {
			var _this4 = this;

			// console.log(role);
			if (role.join(',') != "") {
				axios.get('/visa/access-control/getSelectedPermissions/' + role.join(',')).then(function (result) {

					var ids = result.data;
					var uniqueList = ids.split(',').filter(function (allItems, i, a) {
						return i == a.indexOf(allItems);
					}).join(',');

					if (ids != '') {
						axios.get('/visa/access-control/getSelectedPermissions2/' + uniqueList).then(function (result) {
							_this4.permissions = result.data;
						});
					} else {
						_this4.permissions = [];
					}

					setTimeout(function () {
						$('#permissions').chosen({
							width: "100%",
							no_results_text: "No result/s found.",
							search_contains: true
						});

						$("#permissions").trigger("chosen:updated");
					}, 1000);
				});
			}
		},
		initChosen: function initChosen() {
			var _this5 = this;

			var r = data.roles;
			var sr = [];
			$.each(r, function (i, item) {
				sr.push(r[i].id);
			});

			axios.get('/visa/access-control/getSelectedPermissions/' + sr.join(',')).then(function (result) {

				var ids = result.data;
				var uniqueList = ids.split(',').filter(function (allItems, i, a) {
					return i == a.indexOf(allItems);
				}).join(',');

				if (ids != '') {
					axios.get('/visa/access-control/getSelectedPermissions2/' + uniqueList).then(function (result) {
						_this5.permissions = result.data;

						var p = data.permissions;
						var sp = [];
						$.each(p, function (i, item) {
							sp.push(p[i].id);
						});

						setTimeout(function () {
							$('#permissions').chosen('destroy');
							$('#permissions').chosen({
								width: "100%",
								no_results_text: "No result/s found.",
								search_contains: true
							});

							$('#permissions').val(sp).trigger('chosen:updated');
							$('#permissions').trigger('chosen:updated');
						}, 1000);
					});
				} else {
					_this5.permissions = [];

					var p = data.permissions;
					var sp = [];
					$.each(p, function (i, item) {
						sp.push(p[i].id);
					});

					setTimeout(function () {
						$('#permissions').chosen('destroy');
						$('#permissions').chosen({
							width: "100%",
							no_results_text: "No result/s found.",
							search_contains: true
						});

						$('#permissions').val(sp).trigger('chosen:updated');
						$('#permissions').trigger('chosen:updated');
					}, 1000);
				}
			});
		}
	},

	directives: {
		datepicker: {
			bind: function bind(el, binding, vnode) {
				$(el).datepicker({
					format: 'yyyy-mm-dd'
				}).on('changeDate', function (e) {
					accountForm.$set(accountForm, 'birth_date', e.format('yyyy-mm-dd'));
				});
			}
		}
	},
	created: function created() {
		var _this6 = this;

		Event.listen('Imgfileupload.avatarUpload', function (img) {
			_this6.accountForm.avatar = img;
		});

		this.initChosen();

		this.getScheduleTypes();
	},
	mounted: function mounted() {
		this.initTimePicker();
	}
});

$(document).ready(function () {

	$('.btnUpload').click(function () {
		$('.avatar-uploader input').click();
	});

	$('#first_name, #middle_name, #last_name').keypress(function (event) {
		var inputValue = event.charCode;
		//alert(inputValue);
		if (!(inputValue > 64 && inputValue < 91 || inputValue > 96 && inputValue < 123 || inputValue == 32 || inputValue == 0)) {
			event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});

	$('#roles').on('change', function () {
		var role = $(this).val();
		Accounts.selectRole(role);
	});
});

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 473:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(183);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9hY2NvdW50cy9lZGl0LmpzIiwid2VicGFjazovLy9EaWFsb2dNb2RhbC52dWU/ZTQ3NCoqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT8yMDdjKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlP2ZlMWIqKioqKioqKiJdLCJuYW1lcyI6WyJWdWUiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwiZGF0YSIsIndpbmRvdyIsIkxhcmF2ZWwiLCJBY2NvdW50cyIsImVsIiwiZ3JvdXBzIiwicGVybWlzc2lvbnMiLCJzZWxwZXJtaXNzaW9ucyIsInBlcm1pc3Npb250eXBlIiwiZ2V0cGVybWlzc2lvbnMiLCJzY2hlZHVsZVR5cGVzIiwicm9sZXMiLCJhY2NvdW50Rm9ybSIsIkZvcm0iLCJpZCIsImF2YXRhciIsImZpcnN0X25hbWUiLCJtaWRkbGVfbmFtZSIsImxhc3RfbmFtZSIsImJpcnRoX2RhdGUiLCJnZW5kZXIiLCJjaXZpbF9zdGF0dXMiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJhZGRyZXNzIiwiZW1haWwiLCJzY2hlZHVsZVR5cGUiLCJzY2hlZHVsZSIsInNjaGVkdWxlX3R5cGUiLCJ0aW1lSW4iLCJtb21lbnQiLCJ0aW1lX2luIiwiZm9ybWF0IiwidGltZU91dCIsInRpbWVfb3V0IiwidGltZUluRnJvbSIsInRpbWVfaW5fZnJvbSIsInRpbWVJblRvIiwidGltZV9pbl90byIsImJhc2VVUkwiLCJjcGFuZWxfdXJsIiwibWV0aG9kcyIsImdldFNjaGVkdWxlVHlwZXMiLCJheGlvc0FQSXYxIiwiZ2V0IiwidGhlbiIsInJlc3BvbnNlIiwiaW5pdFRpbWVQaWNrZXIiLCJzZXRUaW1lb3V0IiwiJCIsInRpbWVwaWNrZXIiLCJvbiIsImUiLCJ0YXJnZXQiLCJ2YWx1ZSIsInRpbWUiLCJzaG93UGVybWlzc2lvbnMiLCJheGlvcyIsInJlc3VsdCIsIm1vZGFsIiwidG9nZ2xlIiwibm9kZU5hbWUiLCJ0b2dnbGVDbGFzcyIsInNlbGVjdEl0ZW0iLCJjaGVjayIsInNwIiwidmkiLCJlYWNoIiwiaSIsIml0ZW0iLCJ1bmRlZmluZWQiLCJzcGxpY2UiLCJwdXNoIiwicCIsInZhbCIsInRyaWdnZXIiLCJhIiwic2VsZWN0Um9sZSIsInJvbGUiLCJqb2luIiwiaWRzIiwidW5pcXVlTGlzdCIsInNwbGl0IiwiZmlsdGVyIiwiYWxsSXRlbXMiLCJpbmRleE9mIiwiY2hvc2VuIiwid2lkdGgiLCJub19yZXN1bHRzX3RleHQiLCJzZWFyY2hfY29udGFpbnMiLCJpbml0Q2hvc2VuIiwiciIsInNyIiwiZGlyZWN0aXZlcyIsImRhdGVwaWNrZXIiLCJiaW5kIiwiYmluZGluZyIsInZub2RlIiwiJHNldCIsImNyZWF0ZWQiLCJFdmVudCIsImxpc3RlbiIsImltZyIsIm1vdW50ZWQiLCJkb2N1bWVudCIsInJlYWR5IiwiY2xpY2siLCJrZXlwcmVzcyIsImV2ZW50IiwiaW5wdXRWYWx1ZSIsImNoYXJDb2RlIiwicHJldmVudERlZmF1bHQiLCJ3aGljaCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBQSxJQUFJQyxTQUFKLENBQWMsY0FBZCxFQUErQkMsbUJBQU9BLENBQUMsQ0FBUixDQUEvQjs7QUFFQSxJQUFJQyxPQUFPQyxPQUFPQyxPQUFQLENBQWVGLElBQTFCOztBQUVBLElBQUlHLFdBQVcsSUFBSU4sR0FBSixDQUFRO0FBQ3RCTyxLQUFHLGNBRG1CO0FBRXRCSixPQUFLO0FBQ0pLLFVBQU8sRUFESDtBQUVKQyxlQUFhLEVBRlQ7QUFHSkMsa0JBQWdCUCxLQUFLTSxXQUhqQjtBQUlKRSxrQkFBZSxFQUpYO0FBS0pDLGtCQUFlLEVBTFg7QUFNSkMsaUJBQWUsRUFOWDtBQU9KQyxTQUFPWCxLQUFLVyxLQVBSO0FBUURDLGVBQVksSUFBSUMsSUFBSixDQUFTO0FBQ25CQyxPQUFJZCxLQUFLYyxFQURVO0FBRW5CQyxXQUFRZixLQUFLZSxNQUZNO0FBR25CQyxlQUFZaEIsS0FBS2dCLFVBSEU7QUFJbkJDLGdCQUFhakIsS0FBS2lCLFdBSkM7QUFLbkJDLGNBQVdsQixLQUFLa0IsU0FMRztBQU1uQkMsZUFBWW5CLEtBQUttQixVQU5FO0FBT25CQyxXQUFRcEIsS0FBS29CLE1BUE07QUFRbkJDLGlCQUFjckIsS0FBS3FCLFlBUkE7QUFTbkJDLFdBQVF0QixLQUFLc0IsTUFUTTtBQVVuQkMsV0FBUXZCLEtBQUt1QixNQVZNO0FBV25CQyxZQUFTeEIsS0FBS3dCLE9BWEs7QUFZbkJDLFVBQU96QixLQUFLeUIsS0FaTztBQWFuQkMsaUJBQWUxQixLQUFLMkIsUUFBTixHQUFrQjNCLEtBQUsyQixRQUFMLENBQWNDLGFBQWQsQ0FBNEJkLEVBQTlDLEdBQW1ELEVBYjlDO0FBY25CZSxXQUFTN0IsS0FBSzJCLFFBQUwsSUFBaUIzQixLQUFLMkIsUUFBTCxDQUFjQyxhQUFkLENBQTRCZCxFQUE1QixJQUFrQyxDQUFwRCxHQUNMZ0IsT0FBTzlCLEtBQUsyQixRQUFMLENBQWNJLE9BQXJCLEVBQThCLFVBQTlCLEVBQTBDQyxNQUExQyxDQUFpRCxRQUFqRCxDQURLLEdBQ3dELEVBZjdDO0FBZ0JuQkMsWUFBVWpDLEtBQUsyQixRQUFMLElBQWlCM0IsS0FBSzJCLFFBQUwsQ0FBY0MsYUFBZCxDQUE0QmQsRUFBNUIsSUFBa0MsQ0FBcEQsR0FDTmdCLE9BQU85QixLQUFLMkIsUUFBTCxDQUFjTyxRQUFyQixFQUErQixVQUEvQixFQUEyQ0YsTUFBM0MsQ0FBa0QsUUFBbEQsQ0FETSxHQUN3RCxFQWpCOUM7QUFrQm5CRyxlQUFhbkMsS0FBSzJCLFFBQUwsSUFBaUIzQixLQUFLMkIsUUFBTCxDQUFjQyxhQUFkLENBQTRCZCxFQUE1QixJQUFrQyxDQUFwRCxHQUNUZ0IsT0FBTzlCLEtBQUsyQixRQUFMLENBQWNTLFlBQXJCLEVBQW1DLFVBQW5DLEVBQStDSixNQUEvQyxDQUFzRCxRQUF0RCxDQURTLEdBQ3lELEVBbkJsRDtBQW9CdEJLLGFBQVdyQyxLQUFLMkIsUUFBTCxJQUFpQjNCLEtBQUsyQixRQUFMLENBQWNDLGFBQWQsQ0FBNEJkLEVBQTVCLElBQWtDLENBQXBELEdBQ1BnQixPQUFPOUIsS0FBSzJCLFFBQUwsQ0FBY1csVUFBckIsRUFBaUMsVUFBakMsRUFBNkNOLE1BQTdDLENBQW9ELFFBQXBELENBRE8sR0FDeUQ7QUFyQjdDLEdBQVQsRUFzQlQsRUFBRU8sU0FBUyxZQUFVdEMsT0FBT0MsT0FBUCxDQUFlc0MsVUFBcEMsRUF0QlM7QUFSWCxFQUZpQjs7QUFtQ3RCQyxVQUFTO0FBQ1JDLGtCQURRLDhCQUNXO0FBQUE7O0FBQ2xCQyxjQUFXQyxHQUFYLENBQWUsaUJBQWYsRUFDSUMsSUFESixDQUNTLFVBQUNDLFFBQUQsRUFBYztBQUNsQixVQUFLcEMsYUFBTCxHQUFxQm9DLFNBQVM5QyxJQUE5QjtBQUNILElBSEY7QUFJQSxHQU5PO0FBT1IrQyxnQkFQUSw0QkFPUztBQUFBOztBQUNoQkMsY0FBVyxZQUFNO0FBQ2hCQyxNQUFFLG1CQUFGLEVBQXVCQyxVQUF2QjtBQUNBLElBRkQsRUFFRyxHQUZIOztBQUlBRCxLQUFFLG1CQUFGLEVBQXVCRSxFQUF2QixDQUEwQixPQUExQixFQUFtQyxVQUFTQyxDQUFULEVBQVk7QUFDOUMsUUFBSUMsU0FBU0QsRUFBRUMsTUFBZjs7QUFFQUosTUFBRSxNQUFNSSxPQUFPdkMsRUFBZixFQUFtQm9DLFVBQW5CLENBQThCLFlBQTlCOztBQUVBLFFBQUdHLE9BQU9DLEtBQVAsSUFBZ0IsRUFBbkIsRUFBdUI7QUFDdEJMLE9BQUUsTUFBTUksT0FBT3ZDLEVBQWYsRUFBbUJvQyxVQUFuQixDQUE4QixTQUE5QixFQUF5QyxVQUF6QztBQUNBO0FBQ0QsSUFSRDs7QUFVQUQsS0FBRSxtQkFBRixFQUF1QkMsVUFBdkIsR0FBb0NDLEVBQXBDLENBQXVDLHVCQUF2QyxFQUFnRSxVQUFDQyxDQUFELEVBQU87QUFDdEUsUUFBSXRDLEtBQUtzQyxFQUFFQyxNQUFGLENBQVN2QyxFQUFsQjs7QUFFQSxRQUFHQSxNQUFNLGFBQVQsRUFBd0I7QUFDdkIsWUFBS0YsV0FBTCxDQUFpQmlCLE1BQWpCLEdBQTBCdUIsRUFBRUcsSUFBRixDQUFPRCxLQUFqQztBQUNBLEtBRkQsTUFFTyxJQUFHeEMsTUFBTSxhQUFULEVBQXdCO0FBQzlCLFlBQUtGLFdBQUwsQ0FBaUJxQixPQUFqQixHQUEyQm1CLEVBQUVHLElBQUYsQ0FBT0QsS0FBbEM7QUFDQSxLQUZNLE1BRUEsSUFBR3hDLE1BQU0sYUFBVCxFQUF3QjtBQUM5QixZQUFLRixXQUFMLENBQWlCdUIsVUFBakIsR0FBOEJpQixFQUFFRyxJQUFGLENBQU9ELEtBQXJDO0FBQ0EsS0FGTSxNQUVBLElBQUd4QyxNQUFNLGFBQVQsRUFBd0I7QUFDOUIsWUFBS0YsV0FBTCxDQUFpQnlCLFFBQWpCLEdBQTRCZSxFQUFFRyxJQUFGLENBQU9ELEtBQW5DO0FBQ0E7QUFDRCxJQVpEO0FBYUEsR0FuQ087QUFvQ1JFLGlCQXBDUSw2QkFvQ1M7QUFBQTs7QUFDaEJDLFNBQU1iLEdBQU4sQ0FBVSxxQ0FBVixFQUNPQyxJQURQLENBQ1ksa0JBQVU7QUFDYixXQUFLcEMsY0FBTCxHQUFzQmlELE9BQU8xRCxJQUE3QjtBQUNMLElBSEo7O0FBS0d5RCxTQUFNYixHQUFOLENBQVUsd0NBQVYsRUFDSUMsSUFESixDQUNTLGtCQUFVO0FBQ2IsV0FBS3JDLGNBQUwsR0FBc0JrRCxPQUFPMUQsSUFBN0I7QUFDTCxJQUhEOztBQUtIaUQsS0FBRSxvQkFBRixFQUF3QlUsS0FBeEIsQ0FBOEIsUUFBOUI7QUFDQSxHQWhETztBQWlERkMsUUFqREUsa0JBaURLOUMsRUFqREwsRUFpRFNzQyxDQWpEVCxFQWlEWTtBQUNWLE9BQUdBLEVBQUVDLE1BQUYsQ0FBU1EsUUFBVCxJQUFxQixRQUFyQixJQUFpQ1QsRUFBRUMsTUFBRixDQUFTUSxRQUFULElBQXFCLE1BQXpELEVBQWlFO0FBQzdEWixNQUFFLFNBQVNuQyxFQUFYLEVBQWVnRCxXQUFmLENBQTJCLGVBQTNCO0FBQ0g7QUFDSixHQXJEQztBQXVEUkMsWUF2RFEsc0JBdURHL0QsSUF2REgsRUF1RFE7QUFDTixPQUFHLEtBQUtnRSxLQUFMLENBQVdoRSxJQUFYLEtBQWtCLENBQXJCLEVBQXdCO0FBQ2hDLFFBQUlpRSxLQUFLLEtBQUsxRCxjQUFkO0FBQ00sUUFBSTJELEtBQUssSUFBVDtBQUNOakIsTUFBRWtCLElBQUYsQ0FBT0YsRUFBUCxFQUFXLFVBQVNHLENBQVQsRUFBWUMsSUFBWixFQUFrQjtBQUMzQixTQUFHSixHQUFHRyxDQUFILEtBQU9FLFNBQVYsRUFBb0I7QUFDckIsVUFBR0wsR0FBR0csQ0FBSCxFQUFNdEQsRUFBTixJQUFZZCxLQUFLYyxFQUFwQixFQUF1QjtBQUNwQm9ELFVBQUczRCxjQUFILENBQWtCZ0UsTUFBbEIsQ0FBeUJILENBQXpCLEVBQTJCLENBQTNCO0FBQ0E7QUFDRDtBQUNGLEtBTkQ7QUFPUyxJQVZELE1BVU87QUFDSCxTQUFLN0QsY0FBTCxDQUFvQmlFLElBQXBCLENBQXlCeEUsSUFBekI7QUFDSDs7QUFFSixPQUFJeUUsSUFBSSxLQUFLbEUsY0FBYjtBQUNBLE9BQUkwRCxLQUFLLEVBQVQ7QUFDTmhCLEtBQUVrQixJQUFGLENBQU9NLENBQVAsRUFBVSxVQUFTTCxDQUFULEVBQVlDLElBQVosRUFBa0I7QUFDM0JKLE9BQUdPLElBQUgsQ0FBUUMsRUFBRUwsQ0FBRixFQUFLdEQsRUFBYjtBQUNBLElBRkQ7QUFHQW1DLEtBQUUsY0FBRixFQUFrQnlCLEdBQWxCLENBQXNCVCxFQUF0QixFQUEwQlUsT0FBMUIsQ0FBa0MsZ0JBQWxDO0FBRU0sR0E3RUM7QUE4RUZYLE9BOUVFLGlCQThFSWxELEVBOUVKLEVBOEVPO0FBQ1IsT0FBSThELElBQUksQ0FBUjtBQUNBLE9BQUlYLEtBQUssS0FBSzFELGNBQWQ7QUFDTjBDLEtBQUVrQixJQUFGLENBQU9GLEVBQVAsRUFBVyxVQUFTRyxDQUFULEVBQVlDLElBQVosRUFBa0I7QUFDM0IsUUFBR0osR0FBR0csQ0FBSCxFQUFNdEQsRUFBTixJQUFZQSxHQUFHQSxFQUFsQixFQUFxQjtBQUNwQjhELFNBQUksQ0FBSjtBQUNBO0FBQ0YsSUFKRDtBQUtBLFVBQU9BLENBQVA7QUFDTSxHQXZGQztBQXdGRkMsWUF4RkUsc0JBd0ZTQyxJQXhGVCxFQXdGYztBQUFBOztBQUNmO0FBQ0EsT0FBR0EsS0FBS0MsSUFBTCxDQUFVLEdBQVYsS0FBZ0IsRUFBbkIsRUFBc0I7QUFDMUJ0QixVQUFNYixHQUFOLENBQVUsaURBQStDa0MsS0FBS0MsSUFBTCxDQUFVLEdBQVYsQ0FBekQsRUFDTWxDLElBRE4sQ0FDVyxrQkFBVTs7QUFFYixTQUFJbUMsTUFBTXRCLE9BQU8xRCxJQUFqQjtBQUNSLFNBQUlpRixhQUFXRCxJQUFJRSxLQUFKLENBQVUsR0FBVixFQUFlQyxNQUFmLENBQXNCLFVBQVNDLFFBQVQsRUFBa0JoQixDQUFsQixFQUFvQlEsQ0FBcEIsRUFBc0I7QUFDdkQsYUFBT1IsS0FBR1EsRUFBRVMsT0FBRixDQUFVRCxRQUFWLENBQVY7QUFDSCxNQUZjLEVBRVpMLElBRlksQ0FFUCxHQUZPLENBQWY7O0FBSU0sU0FBR0MsT0FBSyxFQUFSLEVBQVc7QUFDaEJ2QixZQUFNYixHQUFOLENBQVUsa0RBQWdEcUMsVUFBMUQsRUFDSXBDLElBREosQ0FDUyxrQkFBVTtBQUNULGNBQUt2QyxXQUFMLEdBQW1Cb0QsT0FBTzFELElBQTFCO0FBQ0gsT0FIUDtBQUlNLE1BTEQsTUFLTztBQUNaLGFBQUtNLFdBQUwsR0FBbUIsRUFBbkI7QUFDTTs7QUFFUjBDLGdCQUFXLFlBQU07QUFDaEJDLFFBQUUsY0FBRixFQUFrQnFDLE1BQWxCLENBQXlCO0FBQ3hCQyxjQUFPLE1BRGlCO0FBRXhCQyx3QkFBaUIsb0JBRk87QUFHeEJDLHdCQUFpQjtBQUhPLE9BQXpCOztBQU1BeEMsUUFBRSxjQUFGLEVBQWtCMEIsT0FBbEIsQ0FBMEIsZ0JBQTFCO0FBQ0EsTUFSRCxFQVFHLElBUkg7QUFVSSxLQTNCSDtBQTRCSztBQUVELEdBekhDO0FBMEhQZSxZQTFITyx3QkEwSEs7QUFBQTs7QUFFTixPQUFJQyxJQUFJM0YsS0FBS1csS0FBYjtBQUNBLE9BQUlpRixLQUFLLEVBQVQ7QUFDTjNDLEtBQUVrQixJQUFGLENBQU93QixDQUFQLEVBQVUsVUFBU3ZCLENBQVQsRUFBWUMsSUFBWixFQUFrQjtBQUMzQnVCLE9BQUdwQixJQUFILENBQVFtQixFQUFFdkIsQ0FBRixFQUFLdEQsRUFBYjtBQUNBLElBRkQ7O0FBSUMyQyxTQUFNYixHQUFOLENBQVUsaURBQStDZ0QsR0FBR2IsSUFBSCxDQUFRLEdBQVIsQ0FBekQsRUFDTWxDLElBRE4sQ0FDVyxrQkFBVTs7QUFFYixRQUFJbUMsTUFBTXRCLE9BQU8xRCxJQUFqQjtBQUNSLFFBQUlpRixhQUFXRCxJQUFJRSxLQUFKLENBQVUsR0FBVixFQUFlQyxNQUFmLENBQXNCLFVBQVNDLFFBQVQsRUFBa0JoQixDQUFsQixFQUFvQlEsQ0FBcEIsRUFBc0I7QUFDdkQsWUFBT1IsS0FBR1EsRUFBRVMsT0FBRixDQUFVRCxRQUFWLENBQVY7QUFDSCxLQUZjLEVBRVpMLElBRlksQ0FFUCxHQUZPLENBQWY7O0FBSU0sUUFBR0MsT0FBSyxFQUFSLEVBQVc7QUFDaEJ2QixXQUFNYixHQUFOLENBQVUsa0RBQWdEcUMsVUFBMUQsRUFDSXBDLElBREosQ0FDUyxrQkFBVTtBQUNULGFBQUt2QyxXQUFMLEdBQW1Cb0QsT0FBTzFELElBQTFCOztBQUVILFVBQUl5RSxJQUFJekUsS0FBS00sV0FBYjtBQUNBLFVBQUkyRCxLQUFLLEVBQVQ7QUFDTmhCLFFBQUVrQixJQUFGLENBQU9NLENBQVAsRUFBVSxVQUFTTCxDQUFULEVBQVlDLElBQVosRUFBa0I7QUFDM0JKLFVBQUdPLElBQUgsQ0FBUUMsRUFBRUwsQ0FBRixFQUFLdEQsRUFBYjtBQUNBLE9BRkQ7O0FBSUFrQyxpQkFBVyxZQUFNO0FBQ1ZDLFNBQUUsY0FBRixFQUFrQnFDLE1BQWxCLENBQXlCLFNBQXpCO0FBQ0FyQyxTQUFFLGNBQUYsRUFBa0JxQyxNQUFsQixDQUF5QjtBQUM5QkMsZUFBTyxNQUR1QjtBQUU5QkMseUJBQWlCLG9CQUZhO0FBRzlCQyx5QkFBaUI7QUFIYSxRQUF6Qjs7QUFNQXhDLFNBQUUsY0FBRixFQUFrQnlCLEdBQWxCLENBQXNCVCxFQUF0QixFQUEwQlUsT0FBMUIsQ0FBa0MsZ0JBQWxDO0FBQ0ExQixTQUFFLGNBQUYsRUFBa0IwQixPQUFsQixDQUEwQixnQkFBMUI7QUFDQSxPQVZQLEVBVVMsSUFWVDtBQVlNLE1BdEJQO0FBd0JNLEtBekJELE1BeUJPO0FBQ1osWUFBS3JFLFdBQUwsR0FBbUIsRUFBbkI7O0FBRU0sU0FBSW1FLElBQUl6RSxLQUFLTSxXQUFiO0FBQ0EsU0FBSTJELEtBQUssRUFBVDtBQUNOaEIsT0FBRWtCLElBQUYsQ0FBT00sQ0FBUCxFQUFVLFVBQVNMLENBQVQsRUFBWUMsSUFBWixFQUFrQjtBQUMzQkosU0FBR08sSUFBSCxDQUFRQyxFQUFFTCxDQUFGLEVBQUt0RCxFQUFiO0FBQ0EsTUFGRDs7QUFJQWtDLGdCQUFXLFlBQU07QUFDVkMsUUFBRSxjQUFGLEVBQWtCcUMsTUFBbEIsQ0FBeUIsU0FBekI7QUFDQXJDLFFBQUUsY0FBRixFQUFrQnFDLE1BQWxCLENBQXlCO0FBQzlCQyxjQUFPLE1BRHVCO0FBRTlCQyx3QkFBaUIsb0JBRmE7QUFHOUJDLHdCQUFpQjtBQUhhLE9BQXpCOztBQU1BeEMsUUFBRSxjQUFGLEVBQWtCeUIsR0FBbEIsQ0FBc0JULEVBQXRCLEVBQTBCVSxPQUExQixDQUFrQyxnQkFBbEM7QUFDQTFCLFFBQUUsY0FBRixFQUFrQjBCLE9BQWxCLENBQTBCLGdCQUExQjtBQUNBLE1BVlAsRUFVUyxJQVZUO0FBWU07QUFHSixJQXpESDtBQTBERDtBQTVMTyxFQW5DYTs7QUFrT3RCa0IsYUFBWTtBQUNYQyxjQUFZO0FBQ1ZDLE9BRFUsZ0JBQ0wzRixFQURLLEVBQ0Q0RixPQURDLEVBQ1FDLEtBRFIsRUFDZTtBQUN2QmhELE1BQUU3QyxFQUFGLEVBQU0wRixVQUFOLENBQWlCO0FBQ2Y5RCxhQUFRO0FBRE8sS0FBakIsRUFFR21CLEVBRkgsQ0FFTSxZQUZOLEVBRW9CLFVBQUNDLENBQUQsRUFBTztBQUN6QnhDLGlCQUFZc0YsSUFBWixDQUFpQnRGLFdBQWpCLEVBQThCLFlBQTlCLEVBQTRDd0MsRUFBRXBCLE1BQUYsQ0FBUyxZQUFULENBQTVDO0FBQ0QsS0FKRDtBQUtEO0FBUFM7QUFERCxFQWxPVTtBQTZPdEJtRSxRQTdPc0IscUJBNk9iO0FBQUE7O0FBRUhDLFFBQU1DLE1BQU4sQ0FBYSw0QkFBYixFQUEyQyxVQUFDQyxHQUFELEVBQVM7QUFDM0MsVUFBSzFGLFdBQUwsQ0FBaUJHLE1BQWpCLEdBQTBCdUYsR0FBMUI7QUFDUixHQUZEOztBQUlBLE9BQUtaLFVBQUw7O0FBRUEsT0FBS2hELGdCQUFMO0FBRUwsRUF2UHFCO0FBeVB0QjZELFFBelBzQixxQkF5UFo7QUFDVCxPQUFLeEQsY0FBTDtBQUNBO0FBM1BxQixDQUFSLENBQWY7O0FBOFBBRSxFQUFFdUQsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVc7O0FBRXpCeEQsR0FBRSxZQUFGLEVBQWdCeUQsS0FBaEIsQ0FBc0IsWUFBVTtBQUM1QnpELElBQUUsd0JBQUYsRUFBNEJ5RCxLQUE1QjtBQUNILEVBRkQ7O0FBSUh6RCxHQUFFLHVDQUFGLEVBQTJDMEQsUUFBM0MsQ0FBb0QsVUFBU0MsS0FBVCxFQUFlO0FBQ2xFLE1BQUlDLGFBQWFELE1BQU1FLFFBQXZCO0FBQ0E7QUFDQSxNQUFHLEVBQUdELGFBQWEsRUFBYixJQUFtQkEsYUFBYSxFQUFqQyxJQUF5Q0EsYUFBYSxFQUFiLElBQW1CQSxhQUFhLEdBQXpFLElBQWdGQSxjQUFZLEVBQTVGLElBQW9HQSxjQUFZLENBQWxILENBQUgsRUFBeUg7QUFDekhELFNBQU1HLGNBQU47QUFDQztBQUNELEVBTkQ7O0FBUUE5RCxHQUFFLHVEQUFGLEVBQTJEMEQsUUFBM0QsQ0FBb0UsVUFBVXZELENBQVYsRUFBYTtBQUNoRjtBQUNHLE1BQUlBLEVBQUU0RCxLQUFGLElBQVcsQ0FBWCxJQUFnQjVELEVBQUU0RCxLQUFGLElBQVcsQ0FBM0IsS0FBaUM1RCxFQUFFNEQsS0FBRixHQUFVLEVBQVYsSUFBZ0I1RCxFQUFFNEQsS0FBRixHQUFVLEVBQTNELENBQUosRUFBb0U7QUFDaEUsVUFBTyxLQUFQO0FBQ0g7QUFDSixFQUxEOztBQU9BL0QsR0FBRSxRQUFGLEVBQVlFLEVBQVosQ0FBZSxRQUFmLEVBQXlCLFlBQVc7QUFDaEMsTUFBSTJCLE9BQU83QixFQUFFLElBQUYsRUFBUXlCLEdBQVIsRUFBWDtBQUNBdkUsV0FBUzBFLFVBQVQsQ0FBb0JDLElBQXBCO0FBQ0gsRUFIRDtBQUtBLENBMUJELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM09BO0FBQ0E7QUFDQSxnQ0FEQTtBQUVBO0FBRkE7QUFEQSxHOzs7Ozs7Ozs7Ozs7Ozs7QUN2QkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBeU87QUFDblA7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBcU07QUFDL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy92aXNhL2FjY291bnRzL2VkaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ3Myk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjMiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSIsIlZ1ZS5jb21wb25lbnQoJ2RpYWxvZy1tb2RhbCcsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZScpKTtcblxubGV0IGRhdGEgPSB3aW5kb3cuTGFyYXZlbC5kYXRhO1xuXG52YXIgQWNjb3VudHMgPSBuZXcgVnVlKHtcblx0ZWw6JyNlZGl0QWNjb3VudCcsXG5cdGRhdGE6e1xuXHRcdGdyb3VwczpbXSxcblx0XHRwZXJtaXNzaW9uczogW10sXG5cdFx0c2VscGVybWlzc2lvbnM6IGRhdGEucGVybWlzc2lvbnMsXG5cdFx0cGVybWlzc2lvbnR5cGU6W10sXG5cdFx0Z2V0cGVybWlzc2lvbnM6W10sXG5cdFx0c2NoZWR1bGVUeXBlczogW10sXG5cdFx0cm9sZXM6IGRhdGEucm9sZXMsXG5cdCAgICBhY2NvdW50Rm9ybTpuZXcgRm9ybSh7XG5cdCAgICAgIGlkOiBkYXRhLmlkLFxuXHQgICAgICBhdmF0YXI6IGRhdGEuYXZhdGFyLFxuXHQgICAgICBmaXJzdF9uYW1lOiBkYXRhLmZpcnN0X25hbWUsXG5cdCAgICAgIG1pZGRsZV9uYW1lOiBkYXRhLm1pZGRsZV9uYW1lLFxuXHQgICAgICBsYXN0X25hbWU6IGRhdGEubGFzdF9uYW1lLFxuXHQgICAgICBiaXJ0aF9kYXRlOiBkYXRhLmJpcnRoX2RhdGUsXG5cdCAgICAgIGdlbmRlcjogZGF0YS5nZW5kZXIsXG5cdCAgICAgIGNpdmlsX3N0YXR1czogZGF0YS5jaXZpbF9zdGF0dXMsXG5cdCAgICAgIGhlaWdodDogZGF0YS5oZWlnaHQsXG5cdCAgICAgIHdlaWdodDogZGF0YS53ZWlnaHQsXG5cdCAgICAgIGFkZHJlc3M6IGRhdGEuYWRkcmVzcyxcblx0ICAgICAgZW1haWw6IGRhdGEuZW1haWwsXG5cdCAgICAgIHNjaGVkdWxlVHlwZTogKGRhdGEuc2NoZWR1bGUpID8gZGF0YS5zY2hlZHVsZS5zY2hlZHVsZV90eXBlLmlkIDogJycsXG5cdCAgICAgIHRpbWVJbjogKGRhdGEuc2NoZWR1bGUgJiYgZGF0YS5zY2hlZHVsZS5zY2hlZHVsZV90eXBlLmlkID09IDEpIFxuXHQgICAgICBcdD8gbW9tZW50KGRhdGEuc2NoZWR1bGUudGltZV9pbiwgJ0hIOm1tOnNzJykuZm9ybWF0KFwiaDptbSBBXCIpIDogJycsXG5cdCAgICAgIHRpbWVPdXQ6IChkYXRhLnNjaGVkdWxlICYmIGRhdGEuc2NoZWR1bGUuc2NoZWR1bGVfdHlwZS5pZCA9PSAxKSBcblx0ICAgICAgXHQ/IG1vbWVudChkYXRhLnNjaGVkdWxlLnRpbWVfb3V0LCAnSEg6bW06c3MnKS5mb3JtYXQoXCJoOm1tIEFcIikgOiAnJyxcblx0ICAgICAgdGltZUluRnJvbTogKGRhdGEuc2NoZWR1bGUgJiYgZGF0YS5zY2hlZHVsZS5zY2hlZHVsZV90eXBlLmlkID09IDIpIFxuXHQgICAgICBcdD8gbW9tZW50KGRhdGEuc2NoZWR1bGUudGltZV9pbl9mcm9tLCAnSEg6bW06c3MnKS5mb3JtYXQoXCJoOm1tIEFcIikgOiAnJyxcblx0XHQgIHRpbWVJblRvOiAoZGF0YS5zY2hlZHVsZSAmJiBkYXRhLnNjaGVkdWxlLnNjaGVkdWxlX3R5cGUuaWQgPT0gMikgXG5cdFx0ICBcdD8gbW9tZW50KGRhdGEuc2NoZWR1bGUudGltZV9pbl90bywgJ0hIOm1tOnNzJykuZm9ybWF0KFwiaDptbSBBXCIpIDogJydcblx0ICAgIH0sIHsgYmFzZVVSTDogJ2h0dHA6Ly8nK3dpbmRvdy5MYXJhdmVsLmNwYW5lbF91cmwgfSlcblx0fSxcblxuXHRtZXRob2RzOiB7XG5cdFx0Z2V0U2NoZWR1bGVUeXBlcygpIHtcblx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvc2NoZWR1bGUtdHlwZXMnKVxuXHRcdFx0ICBcdC50aGVuKChyZXNwb25zZSkgPT4ge1xuXHRcdFx0XHQgICAgdGhpcy5zY2hlZHVsZVR5cGVzID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0fSk7XG5cdFx0fSxcblx0XHRpbml0VGltZVBpY2tlcigpIHtcblx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHQkKCcudGltZXBpY2tlci1pbnB1dCcpLnRpbWVwaWNrZXIoKTtcblx0XHRcdH0sIDUwMCk7XG5cblx0XHRcdCQoJy50aW1lcGlja2VyLWlucHV0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRsZXQgdGFyZ2V0ID0gZS50YXJnZXQ7XG5cblx0XHRcdFx0JCgnIycgKyB0YXJnZXQuaWQpLnRpbWVwaWNrZXIoJ3Nob3dXaWRnZXQnKTtcblxuXHRcdFx0XHRpZih0YXJnZXQudmFsdWUgPT0gJycpIHtcblx0XHRcdFx0XHQkKCcjJyArIHRhcmdldC5pZCkudGltZXBpY2tlcignc2V0VGltZScsICcxMjowMCBBTScpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdFx0JCgnLnRpbWVwaWNrZXItaW5wdXQnKS50aW1lcGlja2VyKCkub24oJ2NoYW5nZVRpbWUudGltZXBpY2tlcicsIChlKSA9PiB7XG5cdFx0XHRcdGxldCBpZCA9IGUudGFyZ2V0LmlkO1xuXG5cdFx0XHRcdGlmKGlkID09ICd0aW1lcGlja2VyMScpIHtcblx0XHRcdFx0XHR0aGlzLmFjY291bnRGb3JtLnRpbWVJbiA9IGUudGltZS52YWx1ZTtcblx0XHRcdFx0fSBlbHNlIGlmKGlkID09ICd0aW1lcGlja2VyMicpIHtcblx0XHRcdFx0XHR0aGlzLmFjY291bnRGb3JtLnRpbWVPdXQgPSBlLnRpbWUudmFsdWU7XG5cdFx0XHRcdH0gZWxzZSBpZihpZCA9PSAndGltZXBpY2tlcjMnKSB7XG5cdFx0XHRcdFx0dGhpcy5hY2NvdW50Rm9ybS50aW1lSW5Gcm9tID0gZS50aW1lLnZhbHVlO1xuXHRcdFx0XHR9IGVsc2UgaWYoaWQgPT0gJ3RpbWVwaWNrZXI0Jykge1xuXHRcdFx0XHRcdHRoaXMuYWNjb3VudEZvcm0udGltZUluVG8gPSBlLnRpbWUudmFsdWU7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH0sXG5cdFx0c2hvd1Blcm1pc3Npb25zKCl7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2FjY2Vzcy1jb250cm9sL2dldFBlcm1pc3Npb25zJylcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHQgICAgICAgICAgXHR0aGlzLmdldHBlcm1pc3Npb25zID0gcmVzdWx0LmRhdGE7XG5cdCAgICBcdH0pO1xuXG5cdCAgICBcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0UGVybWlzc2lvblR5cGUnKVxuXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICBcdHRoaXMucGVybWlzc2lvbnR5cGUgPSByZXN1bHQuZGF0YTtcblx0ICAgIFx0fSk7XG5cblx0XHRcdCQoJyNkaWFsb2dQZXJtaXNzaW9ucycpLm1vZGFsKCd0b2dnbGUnKTtcdFx0XHRcblx0XHR9LFxuICAgICAgICB0b2dnbGUoaWQsIGUpIHtcbiAgICAgICAgICAgIGlmKGUudGFyZ2V0Lm5vZGVOYW1lICE9ICdCVVRUT04nICYmIGUudGFyZ2V0Lm5vZGVOYW1lICE9ICdTUEFOJykge1xuICAgICAgICAgICAgICAgICQoJyNmYS0nICsgaWQpLnRvZ2dsZUNsYXNzKCdmYS1hcnJvdy1kb3duJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cblx0XHRzZWxlY3RJdGVtKGRhdGEpe1xuICAgICAgICAgICAgaWYodGhpcy5jaGVjayhkYXRhKT09MSkge1xuXHRcdFx0XHR2YXIgc3AgPSB0aGlzLnNlbHBlcm1pc3Npb25zO1xuXHQgICAgICAgIFx0dmFyIHZpID0gdGhpcztcblx0XHRcdFx0JC5lYWNoKHNwLCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cdFx0XHRcdCAgaWYoc3BbaV0hPXVuZGVmaW5lZCl7XG5cdFx0XHRcdFx0aWYoc3BbaV0uaWQgPT0gZGF0YS5pZCl7XG5cdFx0XHRcdCAgXHRcdHZpLnNlbHBlcm1pc3Npb25zLnNwbGljZShpLDEpO1xuXHRcdFx0XHQgIFx0fVxuXHRcdFx0XHQgIH1cblx0XHRcdFx0fSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuc2VscGVybWlzc2lvbnMucHVzaChkYXRhKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICBcdHZhciBwID0gdGhpcy5zZWxwZXJtaXNzaW9ucztcbiAgICAgICAgXHR2YXIgc3AgPSBbXTtcblx0XHRcdCQuZWFjaChwLCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cdFx0XHRcdHNwLnB1c2gocFtpXS5pZCk7XG5cdFx0XHR9KTtcblx0XHRcdCQoJyNwZXJtaXNzaW9ucycpLnZhbChzcCkudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblxuICAgICAgICB9LFxuICAgICAgICBjaGVjayhpZCl7XG4gICAgICAgIFx0dmFyIGEgPSAwO1xuICAgICAgICBcdHZhciBzcCA9IHRoaXMuc2VscGVybWlzc2lvbnM7XG5cdFx0XHQkLmVhY2goc3AsIGZ1bmN0aW9uKGksIGl0ZW0pIHtcblx0XHRcdCAgaWYoc3BbaV0uaWQgPT0gaWQuaWQpe1xuXHRcdFx0ICBcdGEgPSAxO1xuXHRcdFx0ICB9XG5cdFx0XHR9KTtcblx0XHRcdHJldHVybiBhO1xuICAgICAgICB9LFxuICAgICAgICBzZWxlY3RSb2xlKHJvbGUpe1xuICAgICAgICBcdC8vIGNvbnNvbGUubG9nKHJvbGUpO1xuICAgICAgICBcdGlmKHJvbGUuam9pbignLCcpIT1cIlwiKXtcblx0IFx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0U2VsZWN0ZWRQZXJtaXNzaW9ucy8nK3JvbGUuam9pbignLCcpKVxuXHRcdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0XHQgICAgICAgICAgXHRcblx0XHQgICAgICAgICAgXHR2YXIgaWRzID0gcmVzdWx0LmRhdGE7XG5cdFx0XHRcdFx0dmFyIHVuaXF1ZUxpc3Q9aWRzLnNwbGl0KCcsJykuZmlsdGVyKGZ1bmN0aW9uKGFsbEl0ZW1zLGksYSl7XG5cdFx0XHRcdFx0ICAgIHJldHVybiBpPT1hLmluZGV4T2YoYWxsSXRlbXMpO1xuXHRcdFx0XHRcdH0pLmpvaW4oJywnKTtcblx0XHQgICAgICAgIFx0XG5cdFx0ICAgICAgICBcdGlmKGlkcyE9Jycpe1xuXHRcdFx0XHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9hY2Nlc3MtY29udHJvbC9nZXRTZWxlY3RlZFBlcm1pc3Npb25zMi8nK3VuaXF1ZUxpc3QpXG5cdFx0XHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0XHQgICAgICAgICAgICB0aGlzLnBlcm1pc3Npb25zID0gcmVzdWx0LmRhdGE7XG5cdFx0XHRcdCAgICAgICAgfSk7XG5cdFx0ICAgICAgICBcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLnBlcm1pc3Npb25zID0gW107XG5cdFx0ICAgICAgICBcdH1cblxuXHRcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0XHQkKCcjcGVybWlzc2lvbnMnKS5jaG9zZW4oe1xuXHRcdFx0XHRcdFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdFx0XHRcdFx0bm9fcmVzdWx0c190ZXh0OiBcIk5vIHJlc3VsdC9zIGZvdW5kLlwiLFxuXHRcdFx0XHRcdFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXG5cdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHQkKFwiI3Blcm1pc3Npb25zXCIpLnRyaWdnZXIoXCJjaG9zZW46dXBkYXRlZFwiKTtcblx0XHRcdFx0fSwgMTAwMCk7XG5cblx0XHRcdCAgICB9KTtcbiAgICAgICAgXHR9XG4gICAgICBcdFxuICAgICAgICB9LFxuIFx0XHRpbml0Q2hvc2VuKCl7XG5cbiAgICAgICAgXHR2YXIgciA9IGRhdGEucm9sZXM7XG4gICAgICAgIFx0dmFyIHNyID0gW107XG5cdFx0XHQkLmVhY2gociwgZnVuY3Rpb24oaSwgaXRlbSkge1xuXHRcdFx0XHRzci5wdXNoKHJbaV0uaWQpO1xuXHRcdFx0fSk7XG5cbiBcdFx0XHRheGlvcy5nZXQoJy92aXNhL2FjY2Vzcy1jb250cm9sL2dldFNlbGVjdGVkUGVybWlzc2lvbnMvJytzci5qb2luKCcsJykpXG5cdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0ICAgICAgICAgIFx0XG5cdCAgICAgICAgICBcdHZhciBpZHMgPSByZXN1bHQuZGF0YTtcblx0XHRcdFx0dmFyIHVuaXF1ZUxpc3Q9aWRzLnNwbGl0KCcsJykuZmlsdGVyKGZ1bmN0aW9uKGFsbEl0ZW1zLGksYSl7XG5cdFx0XHRcdCAgICByZXR1cm4gaT09YS5pbmRleE9mKGFsbEl0ZW1zKTtcblx0XHRcdFx0fSkuam9pbignLCcpO1xuXHQgICAgICAgIFx0XG5cdCAgICAgICAgXHRpZihpZHMhPScnKXtcblx0XHRcdFx0XHRheGlvcy5nZXQoJy92aXNhL2FjY2Vzcy1jb250cm9sL2dldFNlbGVjdGVkUGVybWlzc2lvbnMyLycrdW5pcXVlTGlzdClcblx0XHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0ICAgICAgICAgICAgdGhpcy5wZXJtaXNzaW9ucyA9IHJlc3VsdC5kYXRhO1xuXG5cdFx0XHQgICAgICAgIFx0dmFyIHAgPSBkYXRhLnBlcm1pc3Npb25zO1xuXHRcdFx0ICAgICAgICBcdHZhciBzcCA9IFtdO1xuXHRcdFx0XHRcdFx0JC5lYWNoKHAsIGZ1bmN0aW9uKGksIGl0ZW0pIHtcblx0XHRcdFx0XHRcdFx0c3AucHVzaChwW2ldLmlkKTtcblx0XHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0ICAgICAgICBcdCQoJyNwZXJtaXNzaW9ucycpLmNob3NlbignZGVzdHJveScpO1xuXHRcdFx0XHQgICAgICAgIFx0JCgnI3Blcm1pc3Npb25zJykuY2hvc2VuKHtcblx0XHRcdFx0XHRcdFx0XHR3aWR0aDogXCIxMDAlXCIsXG5cdFx0XHRcdFx0XHRcdFx0bm9fcmVzdWx0c190ZXh0OiBcIk5vIHJlc3VsdC9zIGZvdW5kLlwiLFxuXHRcdFx0XHRcdFx0XHRcdHNlYXJjaF9jb250YWluczogdHJ1ZVxuXHRcdFx0XHRcdFx0XHR9KTtcblxuXHRcdFx0XHQgICAgICAgIFx0JCgnI3Blcm1pc3Npb25zJykudmFsKHNwKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xuXHRcdFx0ICAgICAgICBcdFx0JCgnI3Blcm1pc3Npb25zJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblx0XHRcdCAgICAgICAgXHR9LCAxMDAwKTtcblxuXHRcdFx0ICAgICAgICB9KTtcblxuXHQgICAgICAgIFx0fSBlbHNlIHtcblx0XHRcdFx0XHR0aGlzLnBlcm1pc3Npb25zID0gW107XG5cblx0XHQgICAgICAgIFx0dmFyIHAgPSBkYXRhLnBlcm1pc3Npb25zO1xuXHRcdCAgICAgICAgXHR2YXIgc3AgPSBbXTtcblx0XHRcdFx0XHQkLmVhY2gocCwgZnVuY3Rpb24oaSwgaXRlbSkge1xuXHRcdFx0XHRcdFx0c3AucHVzaChwW2ldLmlkKTtcblx0XHRcdFx0XHR9KTtcblxuXHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0ICAgICAgICBcdCQoJyNwZXJtaXNzaW9ucycpLmNob3NlbignZGVzdHJveScpO1xuXHRcdFx0ICAgICAgICBcdCQoJyNwZXJtaXNzaW9ucycpLmNob3Nlbih7XG5cdFx0XHRcdFx0XHRcdHdpZHRoOiBcIjEwMCVcIixcblx0XHRcdFx0XHRcdFx0bm9fcmVzdWx0c190ZXh0OiBcIk5vIHJlc3VsdC9zIGZvdW5kLlwiLFxuXHRcdFx0XHRcdFx0XHRzZWFyY2hfY29udGFpbnM6IHRydWVcblx0XHRcdFx0XHRcdH0pO1xuXG5cdFx0XHQgICAgICAgIFx0JCgnI3Blcm1pc3Npb25zJykudmFsKHNwKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1xuXHRcdCAgICAgICAgXHRcdCQoJyNwZXJtaXNzaW9ucycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XG5cdFx0ICAgICAgICBcdH0sIDEwMDApO1xuXG5cdCAgICAgICAgXHR9XG5cblxuXHRcdCAgICB9KTtcdFx0XHRcblx0XHR9ICAgICAgIFxuXHR9LFxuXG5cdGRpcmVjdGl2ZXM6IHtcblx0XHRkYXRlcGlja2VyOiB7XG5cdFx0ICBiaW5kKGVsLCBiaW5kaW5nLCB2bm9kZSkge1xuXHRcdCAgICAkKGVsKS5kYXRlcGlja2VyKHtcblx0XHQgICAgICBmb3JtYXQ6ICd5eXl5LW1tLWRkJ1xuXHRcdCAgICB9KS5vbignY2hhbmdlRGF0ZScsIChlKSA9PiB7XG5cdFx0ICAgICAgYWNjb3VudEZvcm0uJHNldChhY2NvdW50Rm9ybSwgJ2JpcnRoX2RhdGUnLCBlLmZvcm1hdCgneXl5eS1tbS1kZCcpKTtcblx0XHQgICAgfSk7XG5cdFx0ICB9XG5cdFx0fVxuXHR9LFxuXHRjcmVhdGVkKCl7XG5cbiAgICAgICBFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQuYXZhdGFyVXBsb2FkJywgKGltZykgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuYWNjb3VudEZvcm0uYXZhdGFyID0gaW1nO1xuICAgICAgIH0pO1xuXG4gICAgICAgdGhpcy5pbml0Q2hvc2VuKCk7XHRcblxuICAgICAgIHRoaXMuZ2V0U2NoZWR1bGVUeXBlcygpO1xuXG5cdH0sXG5cblx0bW91bnRlZCgpIHtcblx0XHR0aGlzLmluaXRUaW1lUGlja2VyKCk7XG5cdH1cbn0pO1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcblxuICAgICQoJy5idG5VcGxvYWQnKS5jbGljayhmdW5jdGlvbigpe1xuICAgICAgICAkKCcuYXZhdGFyLXVwbG9hZGVyIGlucHV0JykuY2xpY2soKTtcbiAgICB9KVxuXG5cdCQoJyNmaXJzdF9uYW1lLCAjbWlkZGxlX25hbWUsICNsYXN0X25hbWUnKS5rZXlwcmVzcyhmdW5jdGlvbihldmVudCl7XG5cdFx0dmFyIGlucHV0VmFsdWUgPSBldmVudC5jaGFyQ29kZTtcblx0XHQvL2FsZXJ0KGlucHV0VmFsdWUpO1xuXHRcdGlmKCEoKGlucHV0VmFsdWUgPiA2NCAmJiBpbnB1dFZhbHVlIDwgOTEpIHx8IChpbnB1dFZhbHVlID4gOTYgJiYgaW5wdXRWYWx1ZSA8IDEyMyl8fChpbnB1dFZhbHVlPT0zMikgfHwgKGlucHV0VmFsdWU9PTApKSl7XG5cdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHR9XG5cdH0pO1xuXG5cdCQoXCIjY29udGFjdF9udW1iZXIsICNhbHRlcm5hdGVfY29udGFjdF9udW1iZXIsICN6aXBfY29kZVwiKS5rZXlwcmVzcyhmdW5jdGlvbiAoZSkge1xuXHQgLy9pZiB0aGUgbGV0dGVyIGlzIG5vdCBkaWdpdCB0aGVuIGRpc3BsYXkgZXJyb3IgYW5kIGRvbid0IHR5cGUgYW55dGhpbmdcblx0ICAgIGlmIChlLndoaWNoICE9IDggJiYgZS53aGljaCAhPSAwICYmIChlLndoaWNoIDwgNDggfHwgZS53aGljaCA+IDU3KSkge1xuXHQgICAgICAgIHJldHVybiBmYWxzZTtcblx0ICAgIH1cblx0fSk7XG5cblx0JCgnI3JvbGVzJykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xuXHQgICAgdmFyIHJvbGUgPSAkKHRoaXMpLnZhbCgpO1xuXHQgICAgQWNjb3VudHMuc2VsZWN0Um9sZShyb2xlKTtcblx0fSk7XG5cbn0gKTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2FjY291bnRzL2VkaXQuanMiLCI8dGVtcGxhdGU+XG48ZGl2IGNsYXNzPVwibW9kYWwgbWQtbW9kYWwgZmFkZVwiIDppZD1cImlkXCIgdGFiaW5kZXg9XCItMVwiIHJvbGU9XCJkaWFsb2dcIiBhcmlhLWxhYmVsbGVkYnk9XCJtb2RhbC1sYWJlbFwiPlxuICAgIDxkaXYgOmNsYXNzPVwiJ21vZGFsLWRpYWxvZyAnK3NpemVcIiByb2xlPVwiZG9jdW1lbnRcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cbiAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJtb2RhbC10aXRsZVwiIGlkPVwibW9kYWwtbGFiZWxcIj5cbiAgICAgICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLXRpdGxlXCI+TW9kYWwgVGl0bGU8L3Nsb3Q+XG4gICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtYm9keVwiPk1vZGFsIEJvZHk8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5XCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbjwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICBwcm9wczoge1xuICAgICAgICAnaWQnOntyZXF1aXJlZDp0cnVlfVxuICAgICAgICAsJ3NpemUnOiB7ZGVmYXVsdDonbW9kYWwtbWQnfVxuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIERpYWxvZ01vZGFsLnZ1ZT8wMDNiZGE4OCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNGRlNjA2NTVcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRGlhbG9nTW9kYWwudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTRkZTYwNjU1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNGRlNjA2NTVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNVxuLy8gbW9kdWxlIGNodW5rcyA9IDQgNSA2IDkgMTEgMTggMTkgMjAgMjEgMjIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogX3ZtLmlkLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIi0xXCIsXG4gICAgICBcInJvbGVcIjogXCJkaWFsb2dcIixcbiAgICAgIFwiYXJpYS1sYWJlbGxlZGJ5XCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgY2xhc3M6ICdtb2RhbC1kaWFsb2cgJyArIF92bS5zaXplLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInJvbGVcIjogXCJkb2N1bWVudFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtaGVhZGVyXCJcbiAgfSwgW19jKCdoNCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC10aXRsZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW192bS5fdChcIm1vZGFsLXRpdGxlXCIsIFtfdm0uX3YoXCJNb2RhbCBUaXRsZVwiKV0pXSwgMildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1ib2R5XCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWJvZHlcIiwgW192bS5fdihcIk1vZGFsIEJvZHlcIildKV0sIDIpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWZvb3RlclwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1mb290ZXJcIiwgW19jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXSwgMildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNGRlNjA2NTVcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi00ZGU2MDY1NVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNlxuLy8gbW9kdWxlIGNodW5rcyA9IDQgNSA2IDkgMTEgMTggMTkgMjAgMjEgMjIiXSwic291cmNlUm9vdCI6IiJ9