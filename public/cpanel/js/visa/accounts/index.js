/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 474);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 184:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('dialog-modal', __webpack_require__(5));

var UserApp = new Vue({
	el: '#userlist',
	data: {
		users: [],
		seluser: '',
		permissions: [],
		permissiontype: [],
		getpermissions: [],
		getrolepermissions: []
	},
	methods: {
		editSchedule: function editSchedule(user) {
			this.$refs.editschedule.setSelectedUser(user);

			$('#editSchedule').modal('show');
		},
		getUsers: function getUsers() {
			var _this = this;

			function getUsers() {
				return axiosAPIv1.get('/users');
			}
			axios.all([getUsers()]).then(axios.spread(function (users) {
				_this.users = users.data;
			})).catch($.noop);
		},
		parseMoment: function parseMoment(time) {
			return moment(time, 'HH:mm:ss').format("h:mm A");
		},
		showRemoveDialog: function showRemoveDialog(user) {
			this.seluser = user;

			$('#dialogRemove').modal('toggle');
		},
		showPermissions: function showPermissions() {
			var _this2 = this;

			axios.get('/visa/access-control/getPermissions').then(function (result) {
				_this2.getpermissions = result.data;
			});

			axios.get('/visa/access-control/getPermissionType').then(function (result) {
				_this2.permissiontype = result.data;
			});

			$('#dialogPermissions').modal('toggle');
		},
		toggle: function toggle(id, e) {
			if (e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
				$('#fa-' + id).toggleClass('fa-arrow-down');
			}
		},
		selectItem: function selectItem(data) {
			if (this.check(data) == 1) {
				var sp = this.permissions;
				var vi = this;
				$.each(sp, function (i, item) {
					if (sp[i] != undefined) {
						if (sp[i].permission_id == data.id) {
							vi.permissions.splice(i, 1);
						}
					}
				});
			} else {
				// var d = {"permission_id": data.id,"permissions":{
				// 	"type": data.type,
				// 	"label": data.label
				// } };
				this.permissions.push(data);
			}
			this.initChosen();
		},
		check: function check(id) {
			var a = 0;
			var sp = this.permissions;
			$.each(sp, function (i, item) {
				if (sp[i].permission_id == id.id) {
					a = 1;
				}
			});
			return a;
		},
		selectRole: function selectRole(role) {
			var _this3 = this;

			if (role.join(',') != "") {
				axios.get('/visa/access-control/getSelectedPermissions/' + role.join(',')).then(function (result) {

					var ids = result.data;
					var uniqueList = ids.split(',').filter(function (allItems, i, a) {
						return i == a.indexOf(allItems);
					}).join(',');

					if (ids != '') {
						axios.get('/visa/access-control/getSelectedPermissions2/' + uniqueList).then(function (result) {
							_this3.permissions = result.data;
							_this3.initChosen();
						});
					} else {
						_this3.permissions = [];
						_this3.initChosen();
					}
				});
			}
		},
		deleteUser: function deleteUser() {
			var _this4 = this;

			axiosAPIv1.delete('/users/' + this.seluser.id).then(function (response) {
				toastr.success('User Deleted!');

				var self = _this4;
				axiosAPIv1.get('/users').then(function (response) {
					self.users = response.data;
				});
			});
		},
		initChosen: function initChosen() {
			setTimeout(function () {
				$('#permissions').chosen({
					width: "100%",
					no_results_text: "No result/s found.",
					search_contains: true
				});

				$("#permissions").trigger("chosen:updated");
			}, 1000);
		}
	},
	created: function created() {
		this.getUsers();
	},
	mounted: function mounted() {
		var _this5 = this;

		setTimeout(function () {
			var roles = $('#roles').val();

			_this5.selectRole(roles);
		}, 1000);
	},
	updated: function updated() {
		$('#lists').DataTable({
			pageLength: 10,
			responsive: true,
			dom: '<"top"lf>rt<"bottom"ip><"clear">'

		});
	},

	components: {
		'edit-schedule': __webpack_require__(28)
	}
});

$(document).ready(function () {

	$('#first_name, #middle_name, #last_name').keypress(function (event) {
		var inputValue = event.charCode;
		//alert(inputValue);
		if (!(inputValue > 64 && inputValue < 91 || inputValue > 96 && inputValue < 123 || inputValue == 32 || inputValue == 0)) {
			event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});

	$('#roles').on('change', function () {
		var role = $(this).val();
		UserApp.selectRole(role);
	});
});

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['selecteduser'],

	data: function data() {
		return {
			scheduleTypes: [],

			editScheduleForm: new Form({
				userId: '',
				scheduleType: '',
				timeIn: '',
				timeOut: '',
				timeInFrom: '',
				timeInTo: ''
			}, { baseURL: axiosAPIv1.defaults.baseURL })
		};
	},


	methods: {
		initTimePicker: function initTimePicker() {
			var _this = this;

			setTimeout(function () {
				$('.timepicker-input').timepicker();
			}, 500);

			$('.timepicker-input').on('click', function (e) {
				var target = e.target;

				$('#' + target.id).timepicker('showWidget');

				if (target.value == '') {
					$('#' + target.id).timepicker('setTime', '12:00 AM');
				}
			});

			$('.timepicker-input').timepicker().on('changeTime.timepicker', function (e) {
				var id = e.target.id;

				if (id == 'timepicker1') {
					_this.editScheduleForm.timeIn = e.time.value;
				} else if (id == 'timepicker2') {
					_this.editScheduleForm.timeOut = e.time.value;
				} else if (id == 'timepicker3') {
					_this.editScheduleForm.timeInFrom = e.time.value;
				} else if (id == 'timepicker4') {
					_this.editScheduleForm.timeInTo = e.time.value;
				}
			});
		},
		getScheduleTypes: function getScheduleTypes() {
			var _this2 = this;

			axiosAPIv1.get('/schedule-types').then(function (response) {
				_this2.scheduleTypes = response.data;
			});
		},
		resetEditScheduleForm: function resetEditScheduleForm() {
			this.editScheduleForm = new Form({
				userId: '',
				scheduleType: '',
				timeIn: '',
				timeOut: '',
				timeInFrom: '',
				timeInTo: ''
			}, { baseURL: axiosAPIv1.defaults.baseURL });
		},
		setSelectedUser: function setSelectedUser(user) {
			this.editScheduleForm.userId = user.id;

			if (user.schedule) {
				this.editScheduleForm.scheduleType = user.schedule.schedule_type_id;
			} else {
				this.editScheduleForm.scheduleType = '';
			}

			if (user.schedule && user.schedule.time_in && user.schedule.time_out) {
				this.editScheduleForm.timeIn = moment(user.schedule.time_in, 'HH:mm:ss').format("h:mm A");
				this.editScheduleForm.timeOut = moment(user.schedule.time_out, 'HH:mm:ss').format("h:mm A");
			} else {
				this.editScheduleForm.timeIn = '';
				this.editScheduleForm.timeOut = '';
			}

			if (user.schedule && user.schedule.time_in_from && user.schedule.time_in_to) {
				this.editScheduleForm.timeInFrom = moment(user.schedule.time_in_from, 'HH:mm:ss').format("h:mm A");
				this.editScheduleForm.timeInTo = moment(user.schedule.time_in_to, 'HH:mm:ss').format("h:mm A");
			} else {
				this.editScheduleForm.timeInFrom = '';
				this.editScheduleForm.timeInTo = '';
			}
		},
		editSchedule: function editSchedule() {
			var _this3 = this;

			this.editScheduleForm.submit('patch', '/schedule/' + this.editScheduleForm.userId).then(function (response) {
				if (response.success) {
					_this3.$parent.$parent.getUsers();

					_this3.resetEditScheduleForm();

					$('#editSchedule').modal('hide');

					toastr.success(response.message);
				}
			}).catch(function (error) {
				var errors = JSON.parse(JSON.stringify(error));

				for (var key in errors) {
					var _error = errors[key].message;

					_error.forEach(function (e) {
						toastr.error(e);
					});
				}
			});
		}
	},

	created: function created() {
		this.getScheduleTypes();
	},
	mounted: function mounted() {
		this.initTimePicker();
	}
});

/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-9f916844] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-9f916844] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["edit-schedule.vue?d46060ce"],"names":[],"mappings":";AAoMA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"edit-schedule.vue","sourcesContent":["<template>\n\t\n\t<div>\n\t\t\n\t\t<form class=\"form-horizontal\" @submit.prevent=\"editSchedule\">\n\n\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': editScheduleForm.errors.has('schedule_type') }\">\n\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t        Schedule Type: <span class=\"text-danger\">*</span>\n\t\t        </label>\n\t\t\t    <div class=\"col-md-10\">\n\t\t\t    \t<select class=\"form-control\" name=\"schedule_type\" v-model=\"editScheduleForm.scheduleType\">\n\t\t\t    \t\t<option v-for=\"scheduleType in scheduleTypes\" :value=\"scheduleType.id\">\n\t\t\t    \t\t\t{{ scheduleType.name }}\n\t\t\t    \t\t</option>\n\t\t\t    \t</select>\n\t\t\t    \t<span class=\"help-block\" v-if=\"editScheduleForm.errors.has('schedule_type')\" v-text=\"editScheduleForm.errors.get('schedule_type')\"></span>\n\t\t\t    </div>\n\t\t\t</div>\n\n\t\t\t<div v-show=\"editScheduleForm.scheduleType == 1 ||  editScheduleForm.scheduleType == 2\" class=\"form-group\">\n\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t        Schedule Details: <span class=\"text-danger\">*</span>\n\t\t        </label>\n\t\t\t    <div class=\"col-md-10\">\n\n\t\t\t    \t<div v-show=\"editScheduleForm.scheduleType == 1\">\n\t\t\t    \t\t<div class=\"input-group\">\n\t\t\t    \t\t\t<div class=\"bootstrap-timepicker timepicker\">\n\t\t\t\t\t            <input id=\"timepicker1\" type=\"text\" class=\"form-control input-small timepicker-input\" v-model=\"editScheduleForm.timeIn\" autocomplete=\"off\">\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <span class=\"input-group-addon\">to</span>\n\t\t\t\t\t        <div class=\"bootstrap-timepicker timepicker\">\n\t\t\t\t\t            <input id=\"timepicker2\" type=\"text\" class=\"form-control input-small timepicker-input\" v-model=\"editScheduleForm.timeOut\" autocomplete=\"off\">\n\t\t\t\t\t        </div>\n\t\t\t    \t\t</div>\n\t\t\t    \t</div>\n\n\t\t\t    \t<div v-show=\"editScheduleForm.scheduleType == 2\">\n\t\t\t    \t\t<div class=\"input-group\">\n\t\t\t    \t\t\t<span class=\"input-group-addon\">Time In From</span>\n\t\t\t    \t\t\t<div class=\"bootstrap-timepicker timepicker\">\n\t\t\t\t\t            <input id=\"timepicker3\" type=\"text\" class=\"form-control input-small timepicker-input\" v-model=\"editScheduleForm.timeInFrom\" autocomplete=\"off\">\n\t\t\t\t\t        </div>\n\t\t\t    \t\t</div>\n\t\t\t    \t\t<div style=\"height:10px; clear:both;\"></div>\n\t\t\t    \t\t<div class=\"input-group\">\n\t\t\t    \t\t\t<span class=\"input-group-addon\">Time In To</span>\n\t\t\t    \t\t\t<div class=\"bootstrap-timepicker timepicker\">\n\t\t\t\t\t            <input id=\"timepicker4\" type=\"text\" class=\"form-control input-small timepicker-input\" v-model=\"editScheduleForm.timeInTo\" autocomplete=\"off\">\n\t\t\t\t\t        </div>\n\t\t\t    \t\t</div>\n\t\t\t    \t</div>\n\n\t\t\t    </div>\n\t\t\t</div>\n\n\t\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Edit</button>\n\t    \t<button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t\t</form>\n\n\t</div>\n\n</template>\n\n<script>\n\t\n\texport default {\n\t\tprops: ['selecteduser'],\n\n\t\tdata() {\n\t\t\treturn {\n\t\t\t\tscheduleTypes: [],\n\n\t\t\t\teditScheduleForm: new Form({\n\t\t\t\t\tuserId: '',\n\t\t\t\t\tscheduleType: '',\n\t\t\t\t\ttimeIn: '',\n\t\t\t\t\ttimeOut: '',\n\t\t\t\t\ttimeInFrom: '',\n\t\t\t\t\ttimeInTo: ''\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\t\t\tinitTimePicker() {\n\t\t\t\tsetTimeout(() => {\n\t\t\t\t\t$('.timepicker-input').timepicker();\n\t\t\t\t}, 500);\n\n\t\t\t\t$('.timepicker-input').on('click', function(e) {\n\t\t\t\t\tlet target = e.target;\n\n\t\t\t\t\t$('#' + target.id).timepicker('showWidget');\n\n\t\t\t\t\tif(target.value == '') {\n\t\t\t\t\t\t$('#' + target.id).timepicker('setTime', '12:00 AM');\n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\t$('.timepicker-input').timepicker().on('changeTime.timepicker', (e) => {\n\t\t\t\t\tlet id = e.target.id;\n\n\t\t\t\t\tif(id == 'timepicker1') {\n\t\t\t\t\t\tthis.editScheduleForm.timeIn = e.time.value;\n\t\t\t\t\t} else if(id == 'timepicker2') {\n\t\t\t\t\t\tthis.editScheduleForm.timeOut = e.time.value;\n\t\t\t\t\t} else if(id == 'timepicker3') {\n\t\t\t\t\t\tthis.editScheduleForm.timeInFrom = e.time.value;\n\t\t\t\t\t} else if(id == 'timepicker4') {\n\t\t\t\t\t\tthis.editScheduleForm.timeInTo = e.time.value;\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t\t},\n\t\t\tgetScheduleTypes() {\n\t\t\t\taxiosAPIv1.get('/schedule-types')\n\t\t\t\t  \t.then((response) => {\n\t\t\t\t    \tthis.scheduleTypes = response.data;\n\t\t\t\t  \t});\n\t\t\t},\n\t\t\tresetEditScheduleForm() {\n\t\t\t\tthis.editScheduleForm = new Form({\n\t\t\t\t\tuserId: '',\n\t\t\t\t\tscheduleType: '',\n\t\t\t\t\ttimeIn: '',\n\t\t\t\t\ttimeOut: '',\n\t\t\t\t\ttimeInFrom: '',\n\t\t\t\t\ttimeInTo: ''\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL });\n\t\t\t},\n\t\t\tsetSelectedUser(user) {\n\t\t\t\tthis.editScheduleForm.userId = user.id;\n\n\t\t\t\tif(user.schedule) {\n\t\t\t\t\tthis.editScheduleForm.scheduleType = user.schedule.schedule_type_id;\n\t\t\t\t} else {\n\t\t\t\t\tthis.editScheduleForm.scheduleType = '';\n\t\t\t\t}\n\t\t\t\t\t\t\n\t\t\t\tif(user.schedule && user.schedule.time_in && user.schedule.time_out) {\n\t\t\t\t\tthis.editScheduleForm.timeIn = moment(user.schedule.time_in, 'HH:mm:ss').format(\"h:mm A\");\n\t\t\t\t\tthis.editScheduleForm.timeOut = moment(user.schedule.time_out, 'HH:mm:ss').format(\"h:mm A\")\n\t\t\t\t} else {\n\t\t\t\t\tthis.editScheduleForm.timeIn = '';\n\t\t\t\t\tthis.editScheduleForm.timeOut = '';\n\t\t\t\t}\n\n\t\t\t\tif(user.schedule && user.schedule.time_in_from && user.schedule.time_in_to) {\n\t\t\t\t\tthis.editScheduleForm.timeInFrom = moment(user.schedule.time_in_from, 'HH:mm:ss').format(\"h:mm A\");\n\t\t\t\t\tthis.editScheduleForm.timeInTo = moment(user.schedule.time_in_to, 'HH:mm:ss').format(\"h:mm A\");\n\t\t\t\t} else {\n\t\t\t\t\tthis.editScheduleForm.timeInFrom = '';\n\t\t\t\t\tthis.editScheduleForm.timeInTo = '';\n\t\t\t\t}\n\t\t\t},\n\t\t\teditSchedule() {\n\t\t\t\tthis.editScheduleForm.submit('patch', '/schedule/' + this.editScheduleForm.userId)\n\t\t\t        .then(response => {\n\t\t\t            if(response.success) {\n\t\t\t            \tthis.$parent.$parent.getUsers();\n\t\t\t                \n\t\t\t                this.resetEditScheduleForm();\n\n\t\t\t                $('#editSchedule').modal('hide');\n\n\t\t\t                toastr.success(response.message);\n\t\t\t            }\n\t\t\t        })\n\t\t\t        .catch(error => {\n\t\t\t        \tlet errors = JSON.parse(JSON.stringify(error));\n\n\t\t\t        \tfor(var key in errors) {\n\t\t\t        \t\tlet _error = errors[key].message;\n\n\t\t\t        \t\t_error.forEach(e => {\n\t\t\t        \t\t\ttoastr.error(e);\n\t\t\t        \t\t});\n\t\t\t        \t}\n\t\t\t        });\n\t\t\t}\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.getScheduleTypes();\n\t\t},\n\n\t\tmounted() {\n\t\t\tthis.initTimePicker();\n\t\t}\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(34)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(23),
  /* template */
  __webpack_require__(33),
  /* scopeId */
  "data-v-9f916844",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/employee/accounts/components/edit-schedule.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] edit-schedule.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9f916844", Component.options)
  } else {
    hotAPI.reload("data-v-9f916844", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(7)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 33:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.editSchedule($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editScheduleForm.errors.has('schedule_type')
    }
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.scheduleType),
      expression: "editScheduleForm.scheduleType"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "schedule_type"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.editScheduleForm, "scheduleType", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.scheduleTypes), function(scheduleType) {
    return _c('option', {
      domProps: {
        "value": scheduleType.id
      }
    }, [_vm._v("\n\t\t    \t\t\t" + _vm._s(scheduleType.name) + "\n\t\t    \t\t")])
  }), 0), _vm._v(" "), (_vm.editScheduleForm.errors.has('schedule_type')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editScheduleForm.errors.get('schedule_type'))
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.editScheduleForm.scheduleType == 1 || _vm.editScheduleForm.scheduleType == 2),
      expression: "editScheduleForm.scheduleType == 1 ||  editScheduleForm.scheduleType == 2"
    }],
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.editScheduleForm.scheduleType == 1),
      expression: "editScheduleForm.scheduleType == 1"
    }]
  }, [_c('div', {
    staticClass: "input-group"
  }, [_c('div', {
    staticClass: "bootstrap-timepicker timepicker"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.timeIn),
      expression: "editScheduleForm.timeIn"
    }],
    staticClass: "form-control input-small timepicker-input",
    attrs: {
      "id": "timepicker1",
      "type": "text",
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.editScheduleForm.timeIn)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editScheduleForm, "timeIn", $event.target.value)
      }
    }
  })]), _vm._v(" "), _c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("to")]), _vm._v(" "), _c('div', {
    staticClass: "bootstrap-timepicker timepicker"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.timeOut),
      expression: "editScheduleForm.timeOut"
    }],
    staticClass: "form-control input-small timepicker-input",
    attrs: {
      "id": "timepicker2",
      "type": "text",
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.editScheduleForm.timeOut)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editScheduleForm, "timeOut", $event.target.value)
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.editScheduleForm.scheduleType == 2),
      expression: "editScheduleForm.scheduleType == 2"
    }]
  }, [_c('div', {
    staticClass: "input-group"
  }, [_c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("Time In From")]), _vm._v(" "), _c('div', {
    staticClass: "bootstrap-timepicker timepicker"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.timeInFrom),
      expression: "editScheduleForm.timeInFrom"
    }],
    staticClass: "form-control input-small timepicker-input",
    attrs: {
      "id": "timepicker3",
      "type": "text",
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.editScheduleForm.timeInFrom)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editScheduleForm, "timeInFrom", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "input-group"
  }, [_c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("Time In To")]), _vm._v(" "), _c('div', {
    staticClass: "bootstrap-timepicker timepicker"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.timeInTo),
      expression: "editScheduleForm.timeInTo"
    }],
    staticClass: "form-control input-small timepicker-input",
    attrs: {
      "id": "timepicker4",
      "type": "text",
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.editScheduleForm.timeInTo)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editScheduleForm, "timeInTo", $event.target.value)
      }
    }
  })])])])])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Edit")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Schedule Type: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Schedule Details: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-9f916844", module.exports)
  }
}

/***/ }),

/***/ 34:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(26);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("88059542", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-9f916844\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./edit-schedule.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-9f916844\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./edit-schedule.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 474:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(184);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9hY2NvdW50cy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9+L2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzP2RhMDQqKioqKiIsIndlYnBhY2s6Ly8vZWRpdC1zY2hlZHVsZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvY29tcG9uZW50cy9lZGl0LXNjaGVkdWxlLnZ1ZT9mZGE1Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2FjY291bnRzL2NvbXBvbmVudHMvZWRpdC1zY2hlZHVsZS52dWUiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanM/NmIyYioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2FjY291bnRzL2NvbXBvbmVudHMvZWRpdC1zY2hlZHVsZS52dWU/ZTljZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlPzFmNzciLCJ3ZWJwYWNrOi8vL0RpYWxvZ01vZGFsLnZ1ZT9lNDc0KioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWU/MjA3YyoqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT9mZTFiKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qcz9lNmFjKioqKioiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsIlVzZXJBcHAiLCJlbCIsImRhdGEiLCJ1c2VycyIsInNlbHVzZXIiLCJwZXJtaXNzaW9ucyIsInBlcm1pc3Npb250eXBlIiwiZ2V0cGVybWlzc2lvbnMiLCJnZXRyb2xlcGVybWlzc2lvbnMiLCJtZXRob2RzIiwiZWRpdFNjaGVkdWxlIiwidXNlciIsIiRyZWZzIiwiZWRpdHNjaGVkdWxlIiwic2V0U2VsZWN0ZWRVc2VyIiwiJCIsIm1vZGFsIiwiZ2V0VXNlcnMiLCJheGlvc0FQSXYxIiwiZ2V0IiwiYXhpb3MiLCJhbGwiLCJ0aGVuIiwic3ByZWFkIiwiY2F0Y2giLCJub29wIiwicGFyc2VNb21lbnQiLCJ0aW1lIiwibW9tZW50IiwiZm9ybWF0Iiwic2hvd1JlbW92ZURpYWxvZyIsInNob3dQZXJtaXNzaW9ucyIsInJlc3VsdCIsInRvZ2dsZSIsImlkIiwiZSIsInRhcmdldCIsIm5vZGVOYW1lIiwidG9nZ2xlQ2xhc3MiLCJzZWxlY3RJdGVtIiwiY2hlY2siLCJzcCIsInZpIiwiZWFjaCIsImkiLCJpdGVtIiwidW5kZWZpbmVkIiwicGVybWlzc2lvbl9pZCIsInNwbGljZSIsInB1c2giLCJpbml0Q2hvc2VuIiwiYSIsInNlbGVjdFJvbGUiLCJyb2xlIiwiam9pbiIsImlkcyIsInVuaXF1ZUxpc3QiLCJzcGxpdCIsImZpbHRlciIsImFsbEl0ZW1zIiwiaW5kZXhPZiIsImRlbGV0ZVVzZXIiLCJkZWxldGUiLCJ0b2FzdHIiLCJzdWNjZXNzIiwic2VsZiIsInJlc3BvbnNlIiwic2V0VGltZW91dCIsImNob3NlbiIsIndpZHRoIiwibm9fcmVzdWx0c190ZXh0Iiwic2VhcmNoX2NvbnRhaW5zIiwidHJpZ2dlciIsImNyZWF0ZWQiLCJtb3VudGVkIiwicm9sZXMiLCJ2YWwiLCJ1cGRhdGVkIiwiRGF0YVRhYmxlIiwicGFnZUxlbmd0aCIsInJlc3BvbnNpdmUiLCJkb20iLCJjb21wb25lbnRzIiwiZG9jdW1lbnQiLCJyZWFkeSIsImtleXByZXNzIiwiZXZlbnQiLCJpbnB1dFZhbHVlIiwiY2hhckNvZGUiLCJwcmV2ZW50RGVmYXVsdCIsIndoaWNoIiwib24iXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQUEsSUFBSUMsU0FBSixDQUFjLGNBQWQsRUFBK0JDLG1CQUFPQSxDQUFDLENBQVIsQ0FBL0I7O0FBRUEsSUFBSUMsVUFBVSxJQUFJSCxHQUFKLENBQVE7QUFDckJJLEtBQUcsV0FEa0I7QUFFckJDLE9BQUs7QUFDSkMsU0FBTSxFQURGO0FBRUpDLFdBQVUsRUFGTjtBQUdKQyxlQUFhLEVBSFQ7QUFJSkMsa0JBQWUsRUFKWDtBQUtKQyxrQkFBZSxFQUxYO0FBTUpDLHNCQUFtQjtBQU5mLEVBRmdCO0FBVXJCQyxVQUFTO0FBQ1JDLGNBRFEsd0JBQ0tDLElBREwsRUFDVztBQUNsQixRQUFLQyxLQUFMLENBQVdDLFlBQVgsQ0FBd0JDLGVBQXhCLENBQXdDSCxJQUF4Qzs7QUFFQUksS0FBRSxlQUFGLEVBQW1CQyxLQUFuQixDQUF5QixNQUF6QjtBQUNBLEdBTE87QUFNUkMsVUFOUSxzQkFNRztBQUFBOztBQUNWLFlBQVNBLFFBQVQsR0FBb0I7QUFDbkIsV0FBT0MsV0FBV0MsR0FBWCxDQUFlLFFBQWYsQ0FBUDtBQUNBO0FBQ0VDLFNBQU1DLEdBQU4sQ0FBVSxDQUNaSixVQURZLENBQVYsRUFFQUssSUFGQSxDQUVLRixNQUFNRyxNQUFOLENBQ1AsVUFDRXBCLEtBREYsRUFFSztBQUNMLFVBQUtBLEtBQUwsR0FBYUEsTUFBTUQsSUFBbkI7QUFDQSxJQUxPLENBRkwsRUFRRnNCLEtBUkUsQ0FRSVQsRUFBRVUsSUFSTjtBQVNILEdBbkJPO0FBb0JSQyxhQXBCUSx1QkFvQklDLElBcEJKLEVBb0JVO0FBQ2pCLFVBQU9DLE9BQU9ELElBQVAsRUFBYSxVQUFiLEVBQXlCRSxNQUF6QixDQUFnQyxRQUFoQyxDQUFQO0FBQ0EsR0F0Qk87QUF1QlJDLGtCQXZCUSw0QkF1QlNuQixJQXZCVCxFQXVCZTtBQUN0QixRQUFLUCxPQUFMLEdBQWVPLElBQWY7O0FBRUFJLEtBQUUsZUFBRixFQUFtQkMsS0FBbkIsQ0FBeUIsUUFBekI7QUFDQSxHQTNCTztBQTRCUmUsaUJBNUJRLDZCQTRCUztBQUFBOztBQUNoQlgsU0FBTUQsR0FBTixDQUFVLHFDQUFWLEVBQ09HLElBRFAsQ0FDWSxrQkFBVTtBQUNiLFdBQUtmLGNBQUwsR0FBc0J5QixPQUFPOUIsSUFBN0I7QUFDTCxJQUhKOztBQUtHa0IsU0FBTUQsR0FBTixDQUFVLHdDQUFWLEVBQ0lHLElBREosQ0FDUyxrQkFBVTtBQUNiLFdBQUtoQixjQUFMLEdBQXNCMEIsT0FBTzlCLElBQTdCO0FBQ0wsSUFIRDs7QUFLSGEsS0FBRSxvQkFBRixFQUF3QkMsS0FBeEIsQ0FBOEIsUUFBOUI7QUFDQSxHQXhDTztBQXlDRmlCLFFBekNFLGtCQXlDS0MsRUF6Q0wsRUF5Q1NDLENBekNULEVBeUNZO0FBQ1YsT0FBR0EsRUFBRUMsTUFBRixDQUFTQyxRQUFULElBQXFCLFFBQXJCLElBQWlDRixFQUFFQyxNQUFGLENBQVNDLFFBQVQsSUFBcUIsTUFBekQsRUFBaUU7QUFDN0R0QixNQUFFLFNBQVNtQixFQUFYLEVBQWVJLFdBQWYsQ0FBMkIsZUFBM0I7QUFDSDtBQUNKLEdBN0NDO0FBOENSQyxZQTlDUSxzQkE4Q0dyQyxJQTlDSCxFQThDUTtBQUNOLE9BQUcsS0FBS3NDLEtBQUwsQ0FBV3RDLElBQVgsS0FBa0IsQ0FBckIsRUFBd0I7QUFDaEMsUUFBSXVDLEtBQUssS0FBS3BDLFdBQWQ7QUFDTSxRQUFJcUMsS0FBSyxJQUFUO0FBQ04zQixNQUFFNEIsSUFBRixDQUFPRixFQUFQLEVBQVcsVUFBU0csQ0FBVCxFQUFZQyxJQUFaLEVBQWtCO0FBQzNCLFNBQUdKLEdBQUdHLENBQUgsS0FBT0UsU0FBVixFQUFvQjtBQUNyQixVQUFHTCxHQUFHRyxDQUFILEVBQU1HLGFBQU4sSUFBdUI3QyxLQUFLZ0MsRUFBL0IsRUFBa0M7QUFDL0JRLFVBQUdyQyxXQUFILENBQWUyQyxNQUFmLENBQXNCSixDQUF0QixFQUF3QixDQUF4QjtBQUNBO0FBQ0Q7QUFDRixLQU5EO0FBT1MsSUFWRCxNQVVPO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDRyxTQUFLdkMsV0FBTCxDQUFpQjRDLElBQWpCLENBQXNCL0MsSUFBdEI7QUFDSDtBQUNELFFBQUtnRCxVQUFMO0FBQ0gsR0FqRUM7QUFrRUZWLE9BbEVFLGlCQWtFSU4sRUFsRUosRUFrRU87QUFDUixPQUFJaUIsSUFBSSxDQUFSO0FBQ0EsT0FBSVYsS0FBSyxLQUFLcEMsV0FBZDtBQUNOVSxLQUFFNEIsSUFBRixDQUFPRixFQUFQLEVBQVcsVUFBU0csQ0FBVCxFQUFZQyxJQUFaLEVBQWtCO0FBQzNCLFFBQUdKLEdBQUdHLENBQUgsRUFBTUcsYUFBTixJQUF1QmIsR0FBR0EsRUFBN0IsRUFBZ0M7QUFDL0JpQixTQUFJLENBQUo7QUFDQTtBQUNGLElBSkQ7QUFLQSxVQUFPQSxDQUFQO0FBQ00sR0EzRUM7QUE0RUZDLFlBNUVFLHNCQTRFU0MsSUE1RVQsRUE0RWM7QUFBQTs7QUFDZixPQUFHQSxLQUFLQyxJQUFMLENBQVUsR0FBVixLQUFnQixFQUFuQixFQUFzQjtBQUMxQmxDLFVBQU1ELEdBQU4sQ0FBVSxpREFBK0NrQyxLQUFLQyxJQUFMLENBQVUsR0FBVixDQUF6RCxFQUNNaEMsSUFETixDQUNXLGtCQUFVOztBQUViLFNBQUlpQyxNQUFNdkIsT0FBTzlCLElBQWpCO0FBQ1IsU0FBSXNELGFBQVdELElBQUlFLEtBQUosQ0FBVSxHQUFWLEVBQWVDLE1BQWYsQ0FBc0IsVUFBU0MsUUFBVCxFQUFrQmYsQ0FBbEIsRUFBb0JPLENBQXBCLEVBQXNCO0FBQ3ZELGFBQU9QLEtBQUdPLEVBQUVTLE9BQUYsQ0FBVUQsUUFBVixDQUFWO0FBQ0gsTUFGYyxFQUVaTCxJQUZZLENBRVAsR0FGTyxDQUFmOztBQUlNLFNBQUdDLE9BQUssRUFBUixFQUFXO0FBQ2hCbkMsWUFBTUQsR0FBTixDQUFVLGtEQUFnRHFDLFVBQTFELEVBQ0lsQyxJQURKLENBQ1Msa0JBQVU7QUFDVCxjQUFLakIsV0FBTCxHQUFtQjJCLE9BQU85QixJQUExQjtBQUNBLGNBQUtnRCxVQUFMO0FBQ0gsT0FKUDtBQUtNLE1BTkQsTUFNTztBQUNaLGFBQUs3QyxXQUFMLEdBQW1CLEVBQW5CO0FBQ0EsYUFBSzZDLFVBQUw7QUFDTTtBQUNKLEtBbEJIO0FBbUJFO0FBQ0UsR0FsR0M7QUFtR1JXLFlBbkdRLHdCQW1HSztBQUFBOztBQUNaM0MsY0FBVzRDLE1BQVgsQ0FBa0IsWUFBVSxLQUFLMUQsT0FBTCxDQUFhOEIsRUFBekMsRUFDRVosSUFERixDQUNPLG9CQUFZO0FBQ2pCeUMsV0FBT0MsT0FBUCxDQUFlLGVBQWY7O0FBRUEsUUFBSUMsT0FBTyxNQUFYO0FBQ0EvQyxlQUFXQyxHQUFYLENBQWUsUUFBZixFQUNFRyxJQURGLENBQ08sb0JBQVk7QUFDakIyQyxVQUFLOUQsS0FBTCxHQUFhK0QsU0FBU2hFLElBQXRCO0FBQ0EsS0FIRjtBQUlBLElBVEY7QUFVQSxHQTlHTztBQStHUmdELFlBL0dRLHdCQStHSTtBQUNYaUIsY0FBVyxZQUFNO0FBQ2hCcEQsTUFBRSxjQUFGLEVBQWtCcUQsTUFBbEIsQ0FBeUI7QUFDeEJDLFlBQU8sTUFEaUI7QUFFeEJDLHNCQUFpQixvQkFGTztBQUd4QkMsc0JBQWlCO0FBSE8sS0FBekI7O0FBTUF4RCxNQUFFLGNBQUYsRUFBa0J5RCxPQUFsQixDQUEwQixnQkFBMUI7QUFDQSxJQVJELEVBUUcsSUFSSDtBQVNBO0FBekhPLEVBVlk7QUFxSXJCQyxRQXJJcUIscUJBcUlaO0FBQ1IsT0FBS3hELFFBQUw7QUFDQSxFQXZJb0I7QUF3SXJCeUQsUUF4SXFCLHFCQXdJWDtBQUFBOztBQUNUUCxhQUFXLFlBQU07QUFDaEIsT0FBSVEsUUFBUTVELEVBQUUsUUFBRixFQUFZNkQsR0FBWixFQUFaOztBQUVBLFVBQUt4QixVQUFMLENBQWdCdUIsS0FBaEI7QUFDQSxHQUpELEVBSUcsSUFKSDtBQUtBLEVBOUlvQjtBQStJckJFLFFBL0lxQixxQkErSVg7QUFDVDlELElBQUUsUUFBRixFQUFZK0QsU0FBWixDQUFzQjtBQUNaQyxlQUFZLEVBREE7QUFFWkMsZUFBWSxJQUZBO0FBR1pDLFFBQUs7O0FBSE8sR0FBdEI7QUFNQSxFQXRKb0I7O0FBdUpyQkMsYUFBWTtBQUNYLG1CQUFpQm5GLG1CQUFPQSxDQUFDLEVBQVI7QUFETjtBQXZKUyxDQUFSLENBQWQ7O0FBNkpBZ0IsRUFBRW9FLFFBQUYsRUFBWUMsS0FBWixDQUFrQixZQUFXOztBQUU1QnJFLEdBQUUsdUNBQUYsRUFBMkNzRSxRQUEzQyxDQUFvRCxVQUFTQyxLQUFULEVBQWU7QUFDbEUsTUFBSUMsYUFBYUQsTUFBTUUsUUFBdkI7QUFDQTtBQUNBLE1BQUcsRUFBR0QsYUFBYSxFQUFiLElBQW1CQSxhQUFhLEVBQWpDLElBQXlDQSxhQUFhLEVBQWIsSUFBbUJBLGFBQWEsR0FBekUsSUFBZ0ZBLGNBQVksRUFBNUYsSUFBb0dBLGNBQVksQ0FBbEgsQ0FBSCxFQUF5SDtBQUN6SEQsU0FBTUcsY0FBTjtBQUNDO0FBQ0QsRUFORDs7QUFRQTFFLEdBQUUsdURBQUYsRUFBMkRzRSxRQUEzRCxDQUFvRSxVQUFVbEQsQ0FBVixFQUFhO0FBQ2hGO0FBQ0csTUFBSUEsRUFBRXVELEtBQUYsSUFBVyxDQUFYLElBQWdCdkQsRUFBRXVELEtBQUYsSUFBVyxDQUEzQixLQUFpQ3ZELEVBQUV1RCxLQUFGLEdBQVUsRUFBVixJQUFnQnZELEVBQUV1RCxLQUFGLEdBQVUsRUFBM0QsQ0FBSixFQUFvRTtBQUNoRSxVQUFPLEtBQVA7QUFDSDtBQUNKLEVBTEQ7O0FBT0EzRSxHQUFFLFFBQUYsRUFBWTRFLEVBQVosQ0FBZSxRQUFmLEVBQXlCLFlBQVc7QUFDaEMsTUFBSXRDLE9BQU90QyxFQUFFLElBQUYsRUFBUTZELEdBQVIsRUFBWDtBQUNBNUUsVUFBUW9ELFVBQVIsQ0FBbUJDLElBQW5CO0FBQ0gsRUFIRDtBQUtBLENBdEJELEU7Ozs7Ozs7QUMvSkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0Esd0NBQXdDLGdCQUFnQjtBQUN4RCxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksb0JBQW9CO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbUJBO0FBQ0Esd0JBREE7O0FBR0EsS0FIQSxrQkFHQTtBQUNBO0FBQ0Esb0JBREE7O0FBR0E7QUFDQSxjQURBO0FBRUEsb0JBRkE7QUFHQSxjQUhBO0FBSUEsZUFKQTtBQUtBLGtCQUxBO0FBTUE7QUFOQSxNQU9BLHdDQVBBO0FBSEE7QUFZQSxFQWhCQTs7O0FBa0JBO0FBQ0EsZ0JBREEsNEJBQ0E7QUFBQTs7QUFDQTtBQUNBO0FBQ0EsSUFGQSxFQUVBLEdBRkE7O0FBSUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQVJBOztBQVVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBRkEsTUFFQTtBQUNBO0FBQ0EsS0FGQSxNQUVBO0FBQ0E7QUFDQSxLQUZBLE1BRUE7QUFDQTtBQUNBO0FBQ0EsSUFaQTtBQWFBLEdBN0JBO0FBOEJBLGtCQTlCQSw4QkE4QkE7QUFBQTs7QUFDQSxxQ0FDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEE7QUFJQSxHQW5DQTtBQW9DQSx1QkFwQ0EsbUNBb0NBO0FBQ0E7QUFDQSxjQURBO0FBRUEsb0JBRkE7QUFHQSxjQUhBO0FBSUEsZUFKQTtBQUtBLGtCQUxBO0FBTUE7QUFOQSxNQU9BLHdDQVBBO0FBUUEsR0E3Q0E7QUE4Q0EsaUJBOUNBLDJCQThDQSxJQTlDQSxFQThDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQUZBLE1BRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQXRFQTtBQXVFQSxjQXZFQSwwQkF1RUE7QUFBQTs7QUFDQSxzRkFDQSxJQURBLENBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxJQVhBLEVBWUEsS0FaQSxDQVlBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsTUFGQTtBQUdBO0FBQ0EsSUF0QkE7QUF1QkE7QUEvRkEsRUFsQkE7O0FBb0hBLFFBcEhBLHFCQW9IQTtBQUNBO0FBQ0EsRUF0SEE7QUF3SEEsUUF4SEEscUJBd0hBO0FBQ0E7QUFDQTtBQTFIQSxHOzs7Ozs7O0FDcEVBLDJCQUEyQixtQkFBTyxDQUFDLENBQThEO0FBQ2pHLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsNkVBQTZFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyxzTUFBc00sNERBQTRELGtaQUFrWixxQkFBcUIsc3dEQUFzd0QsWUFBWSxvdUJBQW91Qiw4Q0FBOEMsZ0JBQWdCLG9FQUFvRSxpS0FBaUssR0FBRyx1Q0FBdUMsVUFBVSxPQUFPLG1CQUFtQiwwQkFBMEIsNEJBQTRCLGdEQUFnRCxXQUFXLE9BQU8sNERBQTRELGtDQUFrQywwREFBMEQsc0NBQXNDLG1FQUFtRSxhQUFhLFdBQVcsRUFBRSxvRkFBb0YsaUNBQWlDLHVDQUF1QywwREFBMEQsYUFBYSwrQkFBK0IsMkRBQTJELGFBQWEsK0JBQStCLDhEQUE4RCxhQUFhLCtCQUErQiw0REFBNEQsYUFBYSxXQUFXLEVBQUUsU0FBUyw2QkFBNkIsOEVBQThFLG1EQUFtRCxlQUFlLEVBQUUsU0FBUyxrQ0FBa0MsNENBQTRDLGlLQUFpSyxHQUFHLHVDQUF1QyxFQUFFLFNBQVMsZ0NBQWdDLGlEQUFpRCwrQkFBK0IsZ0ZBQWdGLFdBQVcsT0FBTyxvREFBb0QsV0FBVyw4RkFBOEYsd0dBQXdHLG9IQUFvSCxPQUFPLDhDQUE4QywrQ0FBK0MsV0FBVyx5RkFBeUYsaUhBQWlILDZHQUE2RyxXQUFXLE9BQU8sa0RBQWtELGdEQUFnRCxXQUFXLFNBQVMseUJBQXlCLCtIQUErSCwwQ0FBMEMsc0RBQXNELDZFQUE2RSwyREFBMkQsMkRBQTJELHFCQUFxQixpQkFBaUIsa0NBQWtDLGlFQUFpRSw0Q0FBNEMscURBQXFELDJDQUEyQyxzQ0FBc0MscUJBQXFCLEVBQUUsbUJBQW1CLGlCQUFpQixFQUFFLFNBQVMsT0FBTyxvQkFBb0IsZ0NBQWdDLE9BQU8sb0JBQW9CLDhCQUE4QixPQUFPLEtBQUsseUNBQXlDLDBCQUEwQixLQUFLLGFBQWEseUJBQXlCLEtBQUssYUFBYSxHOzs7Ozs7OztBQ0F2Mk87QUFDQSxtQkFBTyxDQUFDLEVBQXVSOztBQUUvUixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUF3RTtBQUNoRztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxFQUE4TztBQUN4UDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxFQUE2TTtBQUN2TjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsaUJBQWlCO0FBQzNCO0FBQ0E7O0FBRUEsbUJBQW1CLG1CQUFPLENBQUMsQ0FBZ0I7O0FBRTNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQkFBbUIsbUJBQW1CO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLG1CQUFtQixzQkFBc0I7QUFDekM7QUFDQTtBQUNBLHVCQUF1QiwyQkFBMkI7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUIsbUJBQW1CO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLDJCQUEyQjtBQUNoRDtBQUNBO0FBQ0EsWUFBWSx1QkFBdUI7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLHFCQUFxQix1QkFBdUI7QUFDNUM7QUFDQTtBQUNBLDhCQUE4QjtBQUM5QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5REFBeUQ7QUFDekQ7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDdE5BLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN6TkE7O0FBRUE7QUFDQSxjQUFjLG1CQUFPLENBQUMsRUFBdVQ7QUFDN1UsNENBQTRDLFFBQVM7QUFDckQ7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyxDQUE0RTtBQUNqRztBQUNBLEdBQUcsS0FBVTtBQUNiO0FBQ0E7QUFDQSxrS0FBa0ssb0VBQW9FO0FBQ3RPLDJLQUEySyxvRUFBb0U7QUFDL087QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0dBO0FBQ0E7QUFDQSxnQ0FEQTtBQUVBO0FBRkE7QUFEQSxHOzs7Ozs7Ozs7Ozs7Ozs7QUN2QkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBeU87QUFDblA7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBcU07QUFDL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixpQkFBaUI7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLHdCQUF3QjtBQUMzRCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJqcy92aXNhL2FjY291bnRzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0NzQpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGYxMTNlZDM2YTVhNTZiN2RjZjYzIiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUiLCJWdWUuY29tcG9uZW50KCdkaWFsb2ctbW9kYWwnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWUnKSk7XG5cbnZhciBVc2VyQXBwID0gbmV3IFZ1ZSh7XG5cdGVsOicjdXNlcmxpc3QnLFxuXHRkYXRhOntcblx0XHR1c2VyczpbXSxcblx0XHRzZWx1c2VyIDogJycsXG5cdFx0cGVybWlzc2lvbnM6IFtdLFxuXHRcdHBlcm1pc3Npb250eXBlOltdLFxuXHRcdGdldHBlcm1pc3Npb25zOltdLFxuXHRcdGdldHJvbGVwZXJtaXNzaW9uczpbXVxuXHR9LFxuXHRtZXRob2RzOiB7XG5cdFx0ZWRpdFNjaGVkdWxlKHVzZXIpIHtcblx0XHRcdHRoaXMuJHJlZnMuZWRpdHNjaGVkdWxlLnNldFNlbGVjdGVkVXNlcih1c2VyKTtcblxuXHRcdFx0JCgnI2VkaXRTY2hlZHVsZScpLm1vZGFsKCdzaG93Jyk7XG5cdFx0fSxcblx0XHRnZXRVc2VycygpIHtcblx0XHRcdGZ1bmN0aW9uIGdldFVzZXJzKCkge1xuXHRcdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy91c2VycycpO1xuXHRcdFx0fVxuXHQgICAgXHRheGlvcy5hbGwoW1xuXHRcdFx0XHRnZXRVc2VycygpXG5cdFx0XHRdKS50aGVuKGF4aW9zLnNwcmVhZChcblx0XHRcdFx0KFxuXHRcdFx0XHRcdCB1c2Vycyxcblx0XHRcdFx0KSA9PiB7XG5cdFx0XHRcdHRoaXMudXNlcnMgPSB1c2Vycy5kYXRhO1xuXHRcdFx0fSkpXG5cdFx0XHQuY2F0Y2goJC5ub29wKTtcblx0XHR9LFxuXHRcdHBhcnNlTW9tZW50KHRpbWUpIHtcblx0XHRcdHJldHVybiBtb21lbnQodGltZSwgJ0hIOm1tOnNzJykuZm9ybWF0KFwiaDptbSBBXCIpO1xuXHRcdH0sXG5cdFx0c2hvd1JlbW92ZURpYWxvZyh1c2VyKSB7XG5cdFx0XHR0aGlzLnNlbHVzZXIgPSB1c2VyO1xuXG5cdFx0XHQkKCcjZGlhbG9nUmVtb3ZlJykubW9kYWwoJ3RvZ2dsZScpO1xuXHRcdH0sXG5cdFx0c2hvd1Blcm1pc3Npb25zKCl7XG5cdFx0XHRheGlvcy5nZXQoJy92aXNhL2FjY2Vzcy1jb250cm9sL2dldFBlcm1pc3Npb25zJylcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuXHQgICAgICAgICAgXHR0aGlzLmdldHBlcm1pc3Npb25zID0gcmVzdWx0LmRhdGE7XG5cdCAgICBcdH0pO1xuXG5cdCAgICBcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0UGVybWlzc2lvblR5cGUnKVxuXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG5cdCAgICAgICAgICBcdHRoaXMucGVybWlzc2lvbnR5cGUgPSByZXN1bHQuZGF0YTtcblx0ICAgIFx0fSk7XG5cblx0XHRcdCQoJyNkaWFsb2dQZXJtaXNzaW9ucycpLm1vZGFsKCd0b2dnbGUnKTtcdFx0XHRcblx0XHR9LFxuICAgICAgICB0b2dnbGUoaWQsIGUpIHtcbiAgICAgICAgICAgIGlmKGUudGFyZ2V0Lm5vZGVOYW1lICE9ICdCVVRUT04nICYmIGUudGFyZ2V0Lm5vZGVOYW1lICE9ICdTUEFOJykge1xuICAgICAgICAgICAgICAgICQoJyNmYS0nICsgaWQpLnRvZ2dsZUNsYXNzKCdmYS1hcnJvdy1kb3duJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cdFx0c2VsZWN0SXRlbShkYXRhKXtcbiAgICAgICAgICAgIGlmKHRoaXMuY2hlY2soZGF0YSk9PTEpIHtcblx0XHRcdFx0dmFyIHNwID0gdGhpcy5wZXJtaXNzaW9ucztcblx0ICAgICAgICBcdHZhciB2aSA9IHRoaXM7XG5cdFx0XHRcdCQuZWFjaChzcCwgZnVuY3Rpb24oaSwgaXRlbSkge1xuXHRcdFx0XHQgIGlmKHNwW2ldIT11bmRlZmluZWQpe1xuXHRcdFx0XHRcdGlmKHNwW2ldLnBlcm1pc3Npb25faWQgPT0gZGF0YS5pZCl7XG5cdFx0XHRcdCAgXHRcdHZpLnBlcm1pc3Npb25zLnNwbGljZShpLDEpO1xuXHRcdFx0XHQgIFx0fVxuXHRcdFx0XHQgIH1cblx0XHRcdFx0fSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgXHQvLyB2YXIgZCA9IHtcInBlcm1pc3Npb25faWRcIjogZGF0YS5pZCxcInBlcm1pc3Npb25zXCI6e1xuICAgICAgICAgICAgXHQvLyBcdFwidHlwZVwiOiBkYXRhLnR5cGUsXG4gICAgICAgICAgICBcdC8vIFx0XCJsYWJlbFwiOiBkYXRhLmxhYmVsXG4gICAgICAgICAgICBcdC8vIH0gfTtcbiAgICAgICAgICAgICAgICB0aGlzLnBlcm1pc3Npb25zLnB1c2goZGF0YSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLmluaXRDaG9zZW4oKTtcbiAgICAgICAgfSxcbiAgICAgICAgY2hlY2soaWQpe1xuICAgICAgICBcdHZhciBhID0gMDtcbiAgICAgICAgXHR2YXIgc3AgPSB0aGlzLnBlcm1pc3Npb25zO1xuXHRcdFx0JC5lYWNoKHNwLCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cdFx0XHQgIGlmKHNwW2ldLnBlcm1pc3Npb25faWQgPT0gaWQuaWQpe1xuXHRcdFx0ICBcdGEgPSAxO1xuXHRcdFx0ICB9XG5cdFx0XHR9KTtcblx0XHRcdHJldHVybiBhO1xuICAgICAgICB9LFxuICAgICAgICBzZWxlY3RSb2xlKHJvbGUpe1xuICAgICAgICBcdGlmKHJvbGUuam9pbignLCcpIT1cIlwiKXtcblx0IFx0XHRcdGF4aW9zLmdldCgnL3Zpc2EvYWNjZXNzLWNvbnRyb2wvZ2V0U2VsZWN0ZWRQZXJtaXNzaW9ucy8nK3JvbGUuam9pbignLCcpKVxuXHRcdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcblx0XHQgICAgICAgICAgXHRcblx0XHQgICAgICAgICAgXHR2YXIgaWRzID0gcmVzdWx0LmRhdGE7XG5cdFx0XHRcdFx0dmFyIHVuaXF1ZUxpc3Q9aWRzLnNwbGl0KCcsJykuZmlsdGVyKGZ1bmN0aW9uKGFsbEl0ZW1zLGksYSl7XG5cdFx0XHRcdFx0ICAgIHJldHVybiBpPT1hLmluZGV4T2YoYWxsSXRlbXMpO1xuXHRcdFx0XHRcdH0pLmpvaW4oJywnKTtcblx0XHQgICAgICAgIFx0XG5cdFx0ICAgICAgICBcdGlmKGlkcyE9Jycpe1xuXHRcdFx0XHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9hY2Nlc3MtY29udHJvbC9nZXRTZWxlY3RlZFBlcm1pc3Npb25zMi8nK3VuaXF1ZUxpc3QpXG5cdFx0XHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdFx0XHQgICAgICAgICAgICB0aGlzLnBlcm1pc3Npb25zID0gcmVzdWx0LmRhdGE7XG5cdFx0XHRcdCAgICAgICAgICAgIHRoaXMuaW5pdENob3NlbigpO1xuXHRcdFx0XHQgICAgICAgIH0pO1xuXHRcdCAgICAgICAgXHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5wZXJtaXNzaW9ucyA9IFtdO1xuXHRcdFx0XHRcdFx0dGhpcy5pbml0Q2hvc2VuKCk7XG5cdFx0ICAgICAgICBcdH1cblx0XHQgICAgXHR9KTtcblx0ICAgIFx0fSAgXHRcbiAgICAgICAgfSxcblx0XHRkZWxldGVVc2VyKCkge1xuXHRcdFx0YXhpb3NBUEl2MS5kZWxldGUoJy91c2Vycy8nK3RoaXMuc2VsdXNlci5pZClcblx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKCdVc2VyIERlbGV0ZWQhJyk7XG5cblx0XHRcdFx0XHR2YXIgc2VsZiA9IHRoaXM7XG5cdFx0XHRcdFx0YXhpb3NBUEl2MS5nZXQoJy91c2VycycpXG5cdFx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHRcdFx0XHRcdHNlbGYudXNlcnMgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH0pO1xuXHRcdH0sXG5cdFx0aW5pdENob3Nlbigpe1xuXHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XG5cdFx0XHRcdCQoJyNwZXJtaXNzaW9ucycpLmNob3Nlbih7XG5cdFx0XHRcdFx0d2lkdGg6IFwiMTAwJVwiLFxuXHRcdFx0XHRcdG5vX3Jlc3VsdHNfdGV4dDogXCJObyByZXN1bHQvcyBmb3VuZC5cIixcblx0XHRcdFx0XHRzZWFyY2hfY29udGFpbnM6IHRydWVcblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0JChcIiNwZXJtaXNzaW9uc1wiKS50cmlnZ2VyKFwiY2hvc2VuOnVwZGF0ZWRcIik7XG5cdFx0XHR9LCAxMDAwKTtcdFx0XHRcblx0XHR9XG5cdH0sXG5cdGNyZWF0ZWQoKXtcblx0XHR0aGlzLmdldFVzZXJzKCk7XG5cdH0sXG5cdG1vdW50ZWQoKSB7XG5cdFx0c2V0VGltZW91dCgoKSA9PiB7XG5cdFx0XHRsZXQgcm9sZXMgPSAkKCcjcm9sZXMnKS52YWwoKTtcblxuXHRcdFx0dGhpcy5zZWxlY3RSb2xlKHJvbGVzKTtcblx0XHR9LCAxMDAwKTtcblx0fSxcblx0dXBkYXRlZCgpIHtcblx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuICAgICAgICAgICAgcGFnZUxlbmd0aDogMTAsXG4gICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxuICAgICAgICAgICAgZG9tOiAnPFwidG9wXCJsZj5ydDxcImJvdHRvbVwiaXA+PFwiY2xlYXJcIj4nXG5cbiAgICAgICAgfSk7XG5cdH0sXG5cdGNvbXBvbmVudHM6IHtcblx0XHQnZWRpdC1zY2hlZHVsZSc6IHJlcXVpcmUoJy4uLy4uL2VtcGxveWVlL2FjY291bnRzL2NvbXBvbmVudHMvZWRpdC1zY2hlZHVsZS52dWUnKVxuXHR9XG59KTtcblxuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcblxuXHQkKCcjZmlyc3RfbmFtZSwgI21pZGRsZV9uYW1lLCAjbGFzdF9uYW1lJykua2V5cHJlc3MoZnVuY3Rpb24oZXZlbnQpe1xuXHRcdHZhciBpbnB1dFZhbHVlID0gZXZlbnQuY2hhckNvZGU7XG5cdFx0Ly9hbGVydChpbnB1dFZhbHVlKTtcblx0XHRpZighKChpbnB1dFZhbHVlID4gNjQgJiYgaW5wdXRWYWx1ZSA8IDkxKSB8fCAoaW5wdXRWYWx1ZSA+IDk2ICYmIGlucHV0VmFsdWUgPCAxMjMpfHwoaW5wdXRWYWx1ZT09MzIpIHx8IChpbnB1dFZhbHVlPT0wKSkpe1xuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0fVxuXHR9KTtcblxuXHQkKFwiI2NvbnRhY3RfbnVtYmVyLCAjYWx0ZXJuYXRlX2NvbnRhY3RfbnVtYmVyLCAjemlwX2NvZGVcIikua2V5cHJlc3MoZnVuY3Rpb24gKGUpIHtcblx0IC8vaWYgdGhlIGxldHRlciBpcyBub3QgZGlnaXQgdGhlbiBkaXNwbGF5IGVycm9yIGFuZCBkb24ndCB0eXBlIGFueXRoaW5nXG5cdCAgICBpZiAoZS53aGljaCAhPSA4ICYmIGUud2hpY2ggIT0gMCAmJiAoZS53aGljaCA8IDQ4IHx8IGUud2hpY2ggPiA1NykpIHtcblx0ICAgICAgICByZXR1cm4gZmFsc2U7XG5cdCAgICB9XG5cdH0pO1xuXG5cdCQoJyNyb2xlcycpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0ICAgIHZhciByb2xlID0gJCh0aGlzKS52YWwoKTtcblx0ICAgIFVzZXJBcHAuc2VsZWN0Um9sZShyb2xlKTtcblx0fSk7XG5cbn0gKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvYWNjb3VudHMvaW5kZXguanMiLCIvKlxyXG5cdE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXHJcblx0QXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxyXG4qL1xyXG4vLyBjc3MgYmFzZSBjb2RlLCBpbmplY3RlZCBieSB0aGUgY3NzLWxvYWRlclxyXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKCkge1xyXG5cdHZhciBsaXN0ID0gW107XHJcblxyXG5cdC8vIHJldHVybiB0aGUgbGlzdCBvZiBtb2R1bGVzIGFzIGNzcyBzdHJpbmdcclxuXHRsaXN0LnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gW107XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaXRlbSA9IHRoaXNbaV07XHJcblx0XHRcdGlmKGl0ZW1bMl0pIHtcclxuXHRcdFx0XHRyZXN1bHQucHVzaChcIkBtZWRpYSBcIiArIGl0ZW1bMl0gKyBcIntcIiArIGl0ZW1bMV0gKyBcIn1cIik7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0cmVzdWx0LnB1c2goaXRlbVsxXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiByZXN1bHQuam9pbihcIlwiKTtcclxuXHR9O1xyXG5cclxuXHQvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxyXG5cdGxpc3QuaSA9IGZ1bmN0aW9uKG1vZHVsZXMsIG1lZGlhUXVlcnkpIHtcclxuXHRcdGlmKHR5cGVvZiBtb2R1bGVzID09PSBcInN0cmluZ1wiKVxyXG5cdFx0XHRtb2R1bGVzID0gW1tudWxsLCBtb2R1bGVzLCBcIlwiXV07XHJcblx0XHR2YXIgYWxyZWFkeUltcG9ydGVkTW9kdWxlcyA9IHt9O1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGlkID0gdGhpc1tpXVswXTtcclxuXHRcdFx0aWYodHlwZW9mIGlkID09PSBcIm51bWJlclwiKVxyXG5cdFx0XHRcdGFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaWRdID0gdHJ1ZTtcclxuXHRcdH1cclxuXHRcdGZvcihpID0gMDsgaSA8IG1vZHVsZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGl0ZW0gPSBtb2R1bGVzW2ldO1xyXG5cdFx0XHQvLyBza2lwIGFscmVhZHkgaW1wb3J0ZWQgbW9kdWxlXHJcblx0XHRcdC8vIHRoaXMgaW1wbGVtZW50YXRpb24gaXMgbm90IDEwMCUgcGVyZmVjdCBmb3Igd2VpcmQgbWVkaWEgcXVlcnkgY29tYmluYXRpb25zXHJcblx0XHRcdC8vICB3aGVuIGEgbW9kdWxlIGlzIGltcG9ydGVkIG11bHRpcGxlIHRpbWVzIHdpdGggZGlmZmVyZW50IG1lZGlhIHF1ZXJpZXMuXHJcblx0XHRcdC8vICBJIGhvcGUgdGhpcyB3aWxsIG5ldmVyIG9jY3VyIChIZXkgdGhpcyB3YXkgd2UgaGF2ZSBzbWFsbGVyIGJ1bmRsZXMpXHJcblx0XHRcdGlmKHR5cGVvZiBpdGVtWzBdICE9PSBcIm51bWJlclwiIHx8ICFhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzW2l0ZW1bMF1dKSB7XHJcblx0XHRcdFx0aWYobWVkaWFRdWVyeSAmJiAhaXRlbVsyXSkge1xyXG5cdFx0XHRcdFx0aXRlbVsyXSA9IG1lZGlhUXVlcnk7XHJcblx0XHRcdFx0fSBlbHNlIGlmKG1lZGlhUXVlcnkpIHtcclxuXHRcdFx0XHRcdGl0ZW1bMl0gPSBcIihcIiArIGl0ZW1bMl0gKyBcIikgYW5kIChcIiArIG1lZGlhUXVlcnkgKyBcIilcIjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0bGlzdC5wdXNoKGl0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fTtcclxuXHRyZXR1cm4gbGlzdDtcclxufTtcclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXG4vLyBtb2R1bGUgaWQgPSAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCI8dGVtcGxhdGU+XG5cdFxuXHQ8ZGl2PlxuXHRcdFxuXHRcdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCIgQHN1Ym1pdC5wcmV2ZW50PVwiZWRpdFNjaGVkdWxlXCI+XG5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgOmNsYXNzPVwieyAnaGFzLWVycm9yJzogZWRpdFNjaGVkdWxlRm9ybS5lcnJvcnMuaGFzKCdzY2hlZHVsZV90eXBlJykgfVwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICAgU2NoZWR1bGUgVHlwZTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICBcdDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwic2NoZWR1bGVfdHlwZVwiIHYtbW9kZWw9XCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZVwiPlxuXHRcdFx0ICAgIFx0XHQ8b3B0aW9uIHYtZm9yPVwic2NoZWR1bGVUeXBlIGluIHNjaGVkdWxlVHlwZXNcIiA6dmFsdWU9XCJzY2hlZHVsZVR5cGUuaWRcIj5cblx0XHRcdCAgICBcdFx0XHR7eyBzY2hlZHVsZVR5cGUubmFtZSB9fVxuXHRcdFx0ICAgIFx0XHQ8L29wdGlvbj5cblx0XHRcdCAgICBcdDwvc2VsZWN0PlxuXHRcdFx0ICAgIFx0PHNwYW4gY2xhc3M9XCJoZWxwLWJsb2NrXCIgdi1pZj1cImVkaXRTY2hlZHVsZUZvcm0uZXJyb3JzLmhhcygnc2NoZWR1bGVfdHlwZScpXCIgdi10ZXh0PVwiZWRpdFNjaGVkdWxlRm9ybS5lcnJvcnMuZ2V0KCdzY2hlZHVsZV90eXBlJylcIj48L3NwYW4+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiB2LXNob3c9XCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAxIHx8ICBlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAyXCIgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgICBTY2hlZHVsZSBEZXRhaWxzOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHQgICAgICAgIDwvbGFiZWw+XG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXG5cdFx0XHQgICAgXHQ8ZGl2IHYtc2hvdz1cImVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDFcIj5cblx0XHRcdCAgICBcdFx0PGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XG5cdFx0XHQgICAgXHRcdFx0PGRpdiBjbGFzcz1cImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcIj5cblx0XHRcdFx0XHQgICAgICAgICAgICA8aW5wdXQgaWQ9XCJ0aW1lcGlja2VyMVwiIHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiIHYtbW9kZWw9XCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVJblwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuXHRcdFx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHRcdFx0ICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+dG88L3NwYW4+XG5cdFx0XHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiYm9vdHN0cmFwLXRpbWVwaWNrZXIgdGltZXBpY2tlclwiPlxuXHRcdFx0XHRcdCAgICAgICAgICAgIDxpbnB1dCBpZD1cInRpbWVwaWNrZXIyXCIgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBpbnB1dC1zbWFsbCB0aW1lcGlja2VyLWlucHV0XCIgdi1tb2RlbD1cImVkaXRTY2hlZHVsZUZvcm0udGltZU91dFwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuXHRcdFx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgXHRcdDwvZGl2PlxuXHRcdFx0ICAgIFx0PC9kaXY+XG5cblx0XHRcdCAgICBcdDxkaXYgdi1zaG93PVwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMlwiPlxuXHRcdFx0ICAgIFx0XHQ8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXBcIj5cblx0XHRcdCAgICBcdFx0XHQ8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+VGltZSBJbiBGcm9tPC9zcGFuPlxuXHRcdFx0ICAgIFx0XHRcdDxkaXYgY2xhc3M9XCJib290c3RyYXAtdGltZXBpY2tlciB0aW1lcGlja2VyXCI+XG5cdFx0XHRcdFx0ICAgICAgICAgICAgPGlucHV0IGlkPVwidGltZXBpY2tlcjNcIiB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGlucHV0LXNtYWxsIHRpbWVwaWNrZXItaW5wdXRcIiB2LW1vZGVsPVwiZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5Gcm9tXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG5cdFx0XHRcdFx0ICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICBcdFx0PC9kaXY+XG5cdFx0XHQgICAgXHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MTBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cblx0XHRcdCAgICBcdFx0PGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XG5cdFx0XHQgICAgXHRcdFx0PHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlRpbWUgSW4gVG88L3NwYW4+XG5cdFx0XHQgICAgXHRcdFx0PGRpdiBjbGFzcz1cImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcIj5cblx0XHRcdFx0XHQgICAgICAgICAgICA8aW5wdXQgaWQ9XCJ0aW1lcGlja2VyNFwiIHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiIHYtbW9kZWw9XCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG5cdFx0XHRcdFx0ICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICBcdFx0PC9kaXY+XG5cdFx0XHQgICAgXHQ8L2Rpdj5cblxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cblx0XHRcdDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIj5FZGl0PC9idXR0b24+XG5cdCAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuXG5cdFx0PC9mb3JtPlxuXG5cdDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXHRcblx0ZXhwb3J0IGRlZmF1bHQge1xuXHRcdHByb3BzOiBbJ3NlbGVjdGVkdXNlciddLFxuXG5cdFx0ZGF0YSgpIHtcblx0XHRcdHJldHVybiB7XG5cdFx0XHRcdHNjaGVkdWxlVHlwZXM6IFtdLFxuXG5cdFx0XHRcdGVkaXRTY2hlZHVsZUZvcm06IG5ldyBGb3JtKHtcblx0XHRcdFx0XHR1c2VySWQ6ICcnLFxuXHRcdFx0XHRcdHNjaGVkdWxlVHlwZTogJycsXG5cdFx0XHRcdFx0dGltZUluOiAnJyxcblx0XHRcdFx0XHR0aW1lT3V0OiAnJyxcblx0XHRcdFx0XHR0aW1lSW5Gcm9tOiAnJyxcblx0XHRcdFx0XHR0aW1lSW5UbzogJydcblx0XHRcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSlcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0bWV0aG9kczoge1xuXHRcdFx0aW5pdFRpbWVQaWNrZXIoKSB7XG5cdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdCQoJy50aW1lcGlja2VyLWlucHV0JykudGltZXBpY2tlcigpO1xuXHRcdFx0XHR9LCA1MDApO1xuXG5cdFx0XHRcdCQoJy50aW1lcGlja2VyLWlucHV0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuXHRcdFx0XHRcdGxldCB0YXJnZXQgPSBlLnRhcmdldDtcblxuXHRcdFx0XHRcdCQoJyMnICsgdGFyZ2V0LmlkKS50aW1lcGlja2VyKCdzaG93V2lkZ2V0Jyk7XG5cblx0XHRcdFx0XHRpZih0YXJnZXQudmFsdWUgPT0gJycpIHtcblx0XHRcdFx0XHRcdCQoJyMnICsgdGFyZ2V0LmlkKS50aW1lcGlja2VyKCdzZXRUaW1lJywgJzEyOjAwIEFNJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHQkKCcudGltZXBpY2tlci1pbnB1dCcpLnRpbWVwaWNrZXIoKS5vbignY2hhbmdlVGltZS50aW1lcGlja2VyJywgKGUpID0+IHtcblx0XHRcdFx0XHRsZXQgaWQgPSBlLnRhcmdldC5pZDtcblxuXHRcdFx0XHRcdGlmKGlkID09ICd0aW1lcGlja2VyMScpIHtcblx0XHRcdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW4gPSBlLnRpbWUudmFsdWU7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKGlkID09ICd0aW1lcGlja2VyMicpIHtcblx0XHRcdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0ID0gZS50aW1lLnZhbHVlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZihpZCA9PSAndGltZXBpY2tlcjMnKSB7XG5cdFx0XHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluRnJvbSA9IGUudGltZS52YWx1ZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoaWQgPT0gJ3RpbWVwaWNrZXI0Jykge1xuXHRcdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvID0gZS50aW1lLnZhbHVlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9LFxuXHRcdFx0Z2V0U2NoZWR1bGVUeXBlcygpIHtcblx0XHRcdFx0YXhpb3NBUEl2MS5nZXQoJy9zY2hlZHVsZS10eXBlcycpXG5cdFx0XHRcdCAgXHQudGhlbigocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0ICAgIFx0dGhpcy5zY2hlZHVsZVR5cGVzID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0ICBcdH0pO1xuXHRcdFx0fSxcblx0XHRcdHJlc2V0RWRpdFNjaGVkdWxlRm9ybSgpIHtcblx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtID0gbmV3IEZvcm0oe1xuXHRcdFx0XHRcdHVzZXJJZDogJycsXG5cdFx0XHRcdFx0c2NoZWR1bGVUeXBlOiAnJyxcblx0XHRcdFx0XHR0aW1lSW46ICcnLFxuXHRcdFx0XHRcdHRpbWVPdXQ6ICcnLFxuXHRcdFx0XHRcdHRpbWVJbkZyb206ICcnLFxuXHRcdFx0XHRcdHRpbWVJblRvOiAnJ1xuXHRcdFx0XHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KTtcblx0XHRcdH0sXG5cdFx0XHRzZXRTZWxlY3RlZFVzZXIodXNlcikge1xuXHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udXNlcklkID0gdXNlci5pZDtcblxuXHRcdFx0XHRpZih1c2VyLnNjaGVkdWxlKSB7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9IHVzZXIuc2NoZWR1bGUuc2NoZWR1bGVfdHlwZV9pZDtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID0gJyc7XG5cdFx0XHRcdH1cblx0XHRcdFx0XHRcdFxuXHRcdFx0XHRpZih1c2VyLnNjaGVkdWxlICYmIHVzZXIuc2NoZWR1bGUudGltZV9pbiAmJiB1c2VyLnNjaGVkdWxlLnRpbWVfb3V0KSB7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbiA9IG1vbWVudCh1c2VyLnNjaGVkdWxlLnRpbWVfaW4sICdISDptbTpzcycpLmZvcm1hdChcImg6bW0gQVwiKTtcblx0XHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZU91dCA9IG1vbWVudCh1c2VyLnNjaGVkdWxlLnRpbWVfb3V0LCAnSEg6bW06c3MnKS5mb3JtYXQoXCJoOm1tIEFcIilcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluID0gJyc7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVPdXQgPSAnJztcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKHVzZXIuc2NoZWR1bGUgJiYgdXNlci5zY2hlZHVsZS50aW1lX2luX2Zyb20gJiYgdXNlci5zY2hlZHVsZS50aW1lX2luX3RvKSB7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20gPSBtb21lbnQodXNlci5zY2hlZHVsZS50aW1lX2luX2Zyb20sICdISDptbTpzcycpLmZvcm1hdChcImg6bW0gQVwiKTtcblx0XHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluVG8gPSBtb21lbnQodXNlci5zY2hlZHVsZS50aW1lX2luX3RvLCAnSEg6bW06c3MnKS5mb3JtYXQoXCJoOm1tIEFcIik7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20gPSAnJztcblx0XHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluVG8gPSAnJztcblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdGVkaXRTY2hlZHVsZSgpIHtcblx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnN1Ym1pdCgncGF0Y2gnLCAnL3NjaGVkdWxlLycgKyB0aGlzLmVkaXRTY2hlZHVsZUZvcm0udXNlcklkKVxuXHRcdFx0ICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG5cdFx0XHQgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XG5cdFx0XHQgICAgICAgICAgICBcdHRoaXMuJHBhcmVudC4kcGFyZW50LmdldFVzZXJzKCk7XG5cdFx0XHQgICAgICAgICAgICAgICAgXG5cdFx0XHQgICAgICAgICAgICAgICAgdGhpcy5yZXNldEVkaXRTY2hlZHVsZUZvcm0oKTtcblxuXHRcdFx0ICAgICAgICAgICAgICAgICQoJyNlZGl0U2NoZWR1bGUnKS5tb2RhbCgnaGlkZScpO1xuXG5cdFx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XG5cdFx0XHQgICAgICAgICAgICB9XG5cdFx0XHQgICAgICAgIH0pXG5cdFx0XHQgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XG5cdFx0XHQgICAgICAgIFx0bGV0IGVycm9ycyA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoZXJyb3IpKTtcblxuXHRcdFx0ICAgICAgICBcdGZvcih2YXIga2V5IGluIGVycm9ycykge1xuXHRcdFx0ICAgICAgICBcdFx0bGV0IF9lcnJvciA9IGVycm9yc1trZXldLm1lc3NhZ2U7XG5cblx0XHRcdCAgICAgICAgXHRcdF9lcnJvci5mb3JFYWNoKGUgPT4ge1xuXHRcdFx0ICAgICAgICBcdFx0XHR0b2FzdHIuZXJyb3IoZSk7XG5cdFx0XHQgICAgICAgIFx0XHR9KTtcblx0XHRcdCAgICAgICAgXHR9XG5cdFx0XHQgICAgICAgIH0pO1xuXHRcdFx0fVxuXHRcdH0sXG5cblx0XHRjcmVhdGVkKCkge1xuXHRcdFx0dGhpcy5nZXRTY2hlZHVsZVR5cGVzKCk7XG5cdFx0fSxcblxuXHRcdG1vdW50ZWQoKSB7XG5cdFx0XHR0aGlzLmluaXRUaW1lUGlja2VyKCk7XG5cdFx0fVxuXHR9XG5cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXHRmb3JtIHtcblx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHR9XG5cdC5tLXItMTAge1xuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcblx0fVxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gZWRpdC1zY2hlZHVsZS52dWU/ZDQ2MDYwY2UiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKCk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5mb3JtW2RhdGEtdi05ZjkxNjg0NF0ge1xcblxcdG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcbi5tLXItMTBbZGF0YS12LTlmOTE2ODQ0XSB7XFxuXFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcbn1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiZWRpdC1zY2hlZHVsZS52dWU/ZDQ2MDYwY2VcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQW9NQTtDQUNBLG9CQUFBO0NBQ0E7QUFDQTtDQUNBLG1CQUFBO0NBQ0FcIixcImZpbGVcIjpcImVkaXQtc2NoZWR1bGUudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG5cXHRcXG5cXHQ8ZGl2PlxcblxcdFxcdFxcblxcdFxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwiZWRpdFNjaGVkdWxlXFxcIj5cXG5cXG5cXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiA6Y2xhc3M9XFxcInsgJ2hhcy1lcnJvcic6IGVkaXRTY2hlZHVsZUZvcm0uZXJyb3JzLmhhcygnc2NoZWR1bGVfdHlwZScpIH1cXFwiPlxcblxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgU2NoZWR1bGUgVHlwZTogPHNwYW4gY2xhc3M9XFxcInRleHQtZGFuZ2VyXFxcIj4qPC9zcGFuPlxcblxcdFxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXHRcXHRcXHQgICAgXFx0PHNlbGVjdCBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiBuYW1lPVxcXCJzY2hlZHVsZV90eXBlXFxcIiB2LW1vZGVsPVxcXCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZVxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdDxvcHRpb24gdi1mb3I9XFxcInNjaGVkdWxlVHlwZSBpbiBzY2hlZHVsZVR5cGVzXFxcIiA6dmFsdWU9XFxcInNjaGVkdWxlVHlwZS5pZFxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdFxcdHt7IHNjaGVkdWxlVHlwZS5uYW1lIH19XFxuXFx0XFx0XFx0ICAgIFxcdFxcdDwvb3B0aW9uPlxcblxcdFxcdFxcdCAgICBcXHQ8L3NlbGVjdD5cXG5cXHRcXHRcXHQgICAgXFx0PHNwYW4gY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIHYtaWY9XFxcImVkaXRTY2hlZHVsZUZvcm0uZXJyb3JzLmhhcygnc2NoZWR1bGVfdHlwZScpXFxcIiB2LXRleHQ9XFxcImVkaXRTY2hlZHVsZUZvcm0uZXJyb3JzLmdldCgnc2NoZWR1bGVfdHlwZScpXFxcIj48L3NwYW4+XFxuXFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdDxkaXYgdi1zaG93PVxcXCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAxIHx8ICBlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAyXFxcIiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCI+XFxuXFx0XFx0XFx0ICAgIDxsYWJlbCBjbGFzcz1cXFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFxcXCI+XFxuXFx0XFx0XFx0ICAgICAgICBTY2hlZHVsZSBEZXRhaWxzOiA8c3BhbiBjbGFzcz1cXFwidGV4dC1kYW5nZXJcXFwiPio8L3NwYW4+XFxuXFx0XFx0ICAgICAgICA8L2xhYmVsPlxcblxcdFxcdFxcdCAgICA8ZGl2IGNsYXNzPVxcXCJjb2wtbWQtMTBcXFwiPlxcblxcblxcdFxcdFxcdCAgICBcXHQ8ZGl2IHYtc2hvdz1cXFwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMVxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdDxkaXYgY2xhc3M9XFxcImlucHV0LWdyb3VwXFxcIj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiYm9vdHN0cmFwLXRpbWVwaWNrZXIgdGltZXBpY2tlclxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgPGlucHV0IGlkPVxcXCJ0aW1lcGlja2VyMVxcXCIgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbCBpbnB1dC1zbWFsbCB0aW1lcGlja2VyLWlucHV0XFxcIiB2LW1vZGVsPVxcXCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVJblxcXCIgYXV0b2NvbXBsZXRlPVxcXCJvZmZcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICA8c3BhbiBjbGFzcz1cXFwiaW5wdXQtZ3JvdXAtYWRkb25cXFwiPnRvPC9zcGFuPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgPGRpdiBjbGFzcz1cXFwiYm9vdHN0cmFwLXRpbWVwaWNrZXIgdGltZXBpY2tlclxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgPGlucHV0IGlkPVxcXCJ0aW1lcGlja2VyMlxcXCIgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbCBpbnB1dC1zbWFsbCB0aW1lcGlja2VyLWlucHV0XFxcIiB2LW1vZGVsPVxcXCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVPdXRcXFwiIGF1dG9jb21wbGV0ZT1cXFwib2ZmXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICAgIDwvZGl2PlxcblxcdFxcdFxcdCAgICBcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHQgICAgXFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0ICAgIFxcdDxkaXYgdi1zaG93PVxcXCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAyXFxcIj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0PGRpdiBjbGFzcz1cXFwiaW5wdXQtZ3JvdXBcXFwiPlxcblxcdFxcdFxcdCAgICBcXHRcXHRcXHQ8c3BhbiBjbGFzcz1cXFwiaW5wdXQtZ3JvdXAtYWRkb25cXFwiPlRpbWUgSW4gRnJvbTwvc3Bhbj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiYm9vdHN0cmFwLXRpbWVwaWNrZXIgdGltZXBpY2tlclxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICAgICAgPGlucHV0IGlkPVxcXCJ0aW1lcGlja2VyM1xcXCIgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbCBpbnB1dC1zbWFsbCB0aW1lcGlja2VyLWlucHV0XFxcIiB2LW1vZGVsPVxcXCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb21cXFwiIGF1dG9jb21wbGV0ZT1cXFwib2ZmXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICAgIDwvZGl2PlxcblxcdFxcdFxcdCAgICBcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0PGRpdiBzdHlsZT1cXFwiaGVpZ2h0OjEwcHg7IGNsZWFyOmJvdGg7XFxcIj48L2Rpdj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0PGRpdiBjbGFzcz1cXFwiaW5wdXQtZ3JvdXBcXFwiPlxcblxcdFxcdFxcdCAgICBcXHRcXHRcXHQ8c3BhbiBjbGFzcz1cXFwiaW5wdXQtZ3JvdXAtYWRkb25cXFwiPlRpbWUgSW4gVG88L3NwYW4+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgIDxpbnB1dCBpZD1cXFwidGltZXBpY2tlcjRcXFwiIHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFxcXCIgdi1tb2RlbD1cXFwiZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5Ub1xcXCIgYXV0b2NvbXBsZXRlPVxcXCJvZmZcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgPC9kaXY+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdCAgICBcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHQgICAgPC9kaXY+XFxuXFx0XFx0XFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0PGJ1dHRvbiB0eXBlPVxcXCJzdWJtaXRcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCI+RWRpdDwvYnV0dG9uPlxcblxcdCAgICBcXHQ8YnV0dG9uIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFxcXCIgZGF0YS1kaXNtaXNzPVxcXCJtb2RhbFxcXCI+Q2xvc2U8L2J1dHRvbj5cXG5cXG5cXHRcXHQ8L2Zvcm0+XFxuXFxuXFx0PC9kaXY+XFxuXFxuPC90ZW1wbGF0ZT5cXG5cXG48c2NyaXB0PlxcblxcdFxcblxcdGV4cG9ydCBkZWZhdWx0IHtcXG5cXHRcXHRwcm9wczogWydzZWxlY3RlZHVzZXInXSxcXG5cXG5cXHRcXHRkYXRhKCkge1xcblxcdFxcdFxcdHJldHVybiB7XFxuXFx0XFx0XFx0XFx0c2NoZWR1bGVUeXBlczogW10sXFxuXFxuXFx0XFx0XFx0XFx0ZWRpdFNjaGVkdWxlRm9ybTogbmV3IEZvcm0oe1xcblxcdFxcdFxcdFxcdFxcdHVzZXJJZDogJycsXFxuXFx0XFx0XFx0XFx0XFx0c2NoZWR1bGVUeXBlOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aW1lSW46ICcnLFxcblxcdFxcdFxcdFxcdFxcdHRpbWVPdXQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHRpbWVJbkZyb206ICcnLFxcblxcdFxcdFxcdFxcdFxcdHRpbWVJblRvOiAnJ1xcblxcdFxcdFxcdFxcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXFxuXFx0XFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0bWV0aG9kczoge1xcblxcdFxcdFxcdGluaXRUaW1lUGlja2VyKCkge1xcblxcdFxcdFxcdFxcdHNldFRpbWVvdXQoKCkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdCQoJy50aW1lcGlja2VyLWlucHV0JykudGltZXBpY2tlcigpO1xcblxcdFxcdFxcdFxcdH0sIDUwMCk7XFxuXFxuXFx0XFx0XFx0XFx0JCgnLnRpbWVwaWNrZXItaW5wdXQnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XFxuXFx0XFx0XFx0XFx0XFx0bGV0IHRhcmdldCA9IGUudGFyZ2V0O1xcblxcblxcdFxcdFxcdFxcdFxcdCQoJyMnICsgdGFyZ2V0LmlkKS50aW1lcGlja2VyKCdzaG93V2lkZ2V0Jyk7XFxuXFxuXFx0XFx0XFx0XFx0XFx0aWYodGFyZ2V0LnZhbHVlID09ICcnKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0JCgnIycgKyB0YXJnZXQuaWQpLnRpbWVwaWNrZXIoJ3NldFRpbWUnLCAnMTI6MDAgQU0nKTtcXG5cXHRcXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0XFx0JCgnLnRpbWVwaWNrZXItaW5wdXQnKS50aW1lcGlja2VyKCkub24oJ2NoYW5nZVRpbWUudGltZXBpY2tlcicsIChlKSA9PiB7XFxuXFx0XFx0XFx0XFx0XFx0bGV0IGlkID0gZS50YXJnZXQuaWQ7XFxuXFxuXFx0XFx0XFx0XFx0XFx0aWYoaWQgPT0gJ3RpbWVwaWNrZXIxJykge1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW4gPSBlLnRpbWUudmFsdWU7XFxuXFx0XFx0XFx0XFx0XFx0fSBlbHNlIGlmKGlkID09ICd0aW1lcGlja2VyMicpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZU91dCA9IGUudGltZS52YWx1ZTtcXG5cXHRcXHRcXHRcXHRcXHR9IGVsc2UgaWYoaWQgPT0gJ3RpbWVwaWNrZXIzJykge1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5Gcm9tID0gZS50aW1lLnZhbHVlO1xcblxcdFxcdFxcdFxcdFxcdH0gZWxzZSBpZihpZCA9PSAndGltZXBpY2tlcjQnKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvID0gZS50aW1lLnZhbHVlO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHR9KTtcXG5cXHRcXHRcXHR9LFxcblxcdFxcdFxcdGdldFNjaGVkdWxlVHlwZXMoKSB7XFxuXFx0XFx0XFx0XFx0YXhpb3NBUEl2MS5nZXQoJy9zY2hlZHVsZS10eXBlcycpXFxuXFx0XFx0XFx0XFx0ICBcXHQudGhlbigocmVzcG9uc2UpID0+IHtcXG5cXHRcXHRcXHRcXHQgICAgXFx0dGhpcy5zY2hlZHVsZVR5cGVzID0gcmVzcG9uc2UuZGF0YTtcXG5cXHRcXHRcXHRcXHQgIFxcdH0pO1xcblxcdFxcdFxcdH0sXFxuXFx0XFx0XFx0cmVzZXRFZGl0U2NoZWR1bGVGb3JtKCkge1xcblxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybSA9IG5ldyBGb3JtKHtcXG5cXHRcXHRcXHRcXHRcXHR1c2VySWQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHNjaGVkdWxlVHlwZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0dGltZUluOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aW1lT3V0OiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aW1lSW5Gcm9tOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aW1lSW5UbzogJydcXG5cXHRcXHRcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KTtcXG5cXHRcXHRcXHR9LFxcblxcdFxcdFxcdHNldFNlbGVjdGVkVXNlcih1c2VyKSB7XFxuXFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnVzZXJJZCA9IHVzZXIuaWQ7XFxuXFxuXFx0XFx0XFx0XFx0aWYodXNlci5zY2hlZHVsZSkge1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPSB1c2VyLnNjaGVkdWxlLnNjaGVkdWxlX3R5cGVfaWQ7XFxuXFx0XFx0XFx0XFx0fSBlbHNlIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID0gJyc7XFxuXFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdFxcdFxcdFxcdFxcblxcdFxcdFxcdFxcdGlmKHVzZXIuc2NoZWR1bGUgJiYgdXNlci5zY2hlZHVsZS50aW1lX2luICYmIHVzZXIuc2NoZWR1bGUudGltZV9vdXQpIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluID0gbW9tZW50KHVzZXIuc2NoZWR1bGUudGltZV9pbiwgJ0hIOm1tOnNzJykuZm9ybWF0KFxcXCJoOm1tIEFcXFwiKTtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZU91dCA9IG1vbWVudCh1c2VyLnNjaGVkdWxlLnRpbWVfb3V0LCAnSEg6bW06c3MnKS5mb3JtYXQoXFxcImg6bW0gQVxcXCIpXFxuXFx0XFx0XFx0XFx0fSBlbHNlIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluID0gJyc7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVPdXQgPSAnJztcXG5cXHRcXHRcXHRcXHR9XFxuXFxuXFx0XFx0XFx0XFx0aWYodXNlci5zY2hlZHVsZSAmJiB1c2VyLnNjaGVkdWxlLnRpbWVfaW5fZnJvbSAmJiB1c2VyLnNjaGVkdWxlLnRpbWVfaW5fdG8pIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluRnJvbSA9IG1vbWVudCh1c2VyLnNjaGVkdWxlLnRpbWVfaW5fZnJvbSwgJ0hIOm1tOnNzJykuZm9ybWF0KFxcXCJoOm1tIEFcXFwiKTtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluVG8gPSBtb21lbnQodXNlci5zY2hlZHVsZS50aW1lX2luX3RvLCAnSEg6bW06c3MnKS5mb3JtYXQoXFxcImg6bW0gQVxcXCIpO1xcblxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20gPSAnJztcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluVG8gPSAnJztcXG5cXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0fSxcXG5cXHRcXHRcXHRlZGl0U2NoZWR1bGUoKSB7XFxuXFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnN1Ym1pdCgncGF0Y2gnLCAnL3NjaGVkdWxlLycgKyB0aGlzLmVkaXRTY2hlZHVsZUZvcm0udXNlcklkKVxcblxcdFxcdFxcdCAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdCAgICAgICAgICAgIGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5nZXRVc2VycygpO1xcblxcdFxcdFxcdCAgICAgICAgICAgICAgICBcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgdGhpcy5yZXNldEVkaXRTY2hlZHVsZUZvcm0oKTtcXG5cXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgJCgnI2VkaXRTY2hlZHVsZScpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xcblxcdFxcdFxcdCAgICAgICAgICAgIH1cXG5cXHRcXHRcXHQgICAgICAgIH0pXFxuXFx0XFx0XFx0ICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xcblxcdFxcdFxcdCAgICAgICAgXFx0bGV0IGVycm9ycyA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoZXJyb3IpKTtcXG5cXG5cXHRcXHRcXHQgICAgICAgIFxcdGZvcih2YXIga2V5IGluIGVycm9ycykge1xcblxcdFxcdFxcdCAgICAgICAgXFx0XFx0bGV0IF9lcnJvciA9IGVycm9yc1trZXldLm1lc3NhZ2U7XFxuXFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHRfZXJyb3IuZm9yRWFjaChlID0+IHtcXG5cXHRcXHRcXHQgICAgICAgIFxcdFxcdFxcdHRvYXN0ci5lcnJvcihlKTtcXG5cXHRcXHRcXHQgICAgICAgIFxcdFxcdH0pO1xcblxcdFxcdFxcdCAgICAgICAgXFx0fVxcblxcdFxcdFxcdCAgICAgICAgfSk7XFxuXFx0XFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y3JlYXRlZCgpIHtcXG5cXHRcXHRcXHR0aGlzLmdldFNjaGVkdWxlVHlwZXMoKTtcXG5cXHRcXHR9LFxcblxcblxcdFxcdG1vdW50ZWQoKSB7XFxuXFx0XFx0XFx0dGhpcy5pbml0VGltZVBpY2tlcigpO1xcblxcdFxcdH1cXG5cXHR9XFxuXFxuPC9zY3JpcHQ+XFxuXFxuPHN0eWxlIHNjb3BlZD5cXG5cXHRmb3JtIHtcXG5cXHRcXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcblxcdH1cXG5cXHQubS1yLTEwIHtcXG5cXHRcXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxuXFx0fVxcbjwvc3R5bGU+XCJdfV0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL34vdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi05ZjkxNjg0NFwiLFwic2NvcGVkXCI6dHJ1ZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvY29tcG9uZW50cy9lZGl0LXNjaGVkdWxlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjZcbi8vIG1vZHVsZSBjaHVua3MgPSAxMSAxMiIsIlxuLyogc3R5bGVzICovXG5yZXF1aXJlKFwiISF2dWUtc3R5bGUtbG9hZGVyIWNzcy1sb2FkZXI/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTlmOTE2ODQ0XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXN0eWxlcyZpbmRleD0wIS4vZWRpdC1zY2hlZHVsZS52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vZWRpdC1zY2hlZHVsZS52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTlmOTE2ODQ0XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL2VkaXQtc2NoZWR1bGUudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIFwiZGF0YS12LTlmOTE2ODQ0XCIsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gZWRpdC1zY2hlZHVsZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtOWY5MTY4NDRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi05ZjkxNjg0NFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlXG4vLyBtb2R1bGUgaWQgPSAyOFxuLy8gbW9kdWxlIGNodW5rcyA9IDExIDEyIiwiLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuICBNb2RpZmllZCBieSBFdmFuIFlvdSBAeXl4OTkwODAzXG4qL1xuXG52YXIgaGFzRG9jdW1lbnQgPSB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG5cbmlmICh0eXBlb2YgREVCVUcgIT09ICd1bmRlZmluZWQnICYmIERFQlVHKSB7XG4gIGlmICghaGFzRG9jdW1lbnQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgJ3Z1ZS1zdHlsZS1sb2FkZXIgY2Fubm90IGJlIHVzZWQgaW4gYSBub24tYnJvd3NlciBlbnZpcm9ubWVudC4gJyArXG4gICAgXCJVc2UgeyB0YXJnZXQ6ICdub2RlJyB9IGluIHlvdXIgV2VicGFjayBjb25maWcgdG8gaW5kaWNhdGUgYSBzZXJ2ZXItcmVuZGVyaW5nIGVudmlyb25tZW50LlwiXG4gICkgfVxufVxuXG52YXIgbGlzdFRvU3R5bGVzID0gcmVxdWlyZSgnLi9saXN0VG9TdHlsZXMnKVxuXG4vKlxudHlwZSBTdHlsZU9iamVjdCA9IHtcbiAgaWQ6IG51bWJlcjtcbiAgcGFydHM6IEFycmF5PFN0eWxlT2JqZWN0UGFydD5cbn1cblxudHlwZSBTdHlsZU9iamVjdFBhcnQgPSB7XG4gIGNzczogc3RyaW5nO1xuICBtZWRpYTogc3RyaW5nO1xuICBzb3VyY2VNYXA6ID9zdHJpbmdcbn1cbiovXG5cbnZhciBzdHlsZXNJbkRvbSA9IHsvKlxuICBbaWQ6IG51bWJlcl06IHtcbiAgICBpZDogbnVtYmVyLFxuICAgIHJlZnM6IG51bWJlcixcbiAgICBwYXJ0czogQXJyYXk8KG9iaj86IFN0eWxlT2JqZWN0UGFydCkgPT4gdm9pZD5cbiAgfVxuKi99XG5cbnZhciBoZWFkID0gaGFzRG9jdW1lbnQgJiYgKGRvY3VtZW50LmhlYWQgfHwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXSlcbnZhciBzaW5nbGV0b25FbGVtZW50ID0gbnVsbFxudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwXG52YXIgaXNQcm9kdWN0aW9uID0gZmFsc2VcbnZhciBub29wID0gZnVuY3Rpb24gKCkge31cblxuLy8gRm9yY2Ugc2luZ2xlLXRhZyBzb2x1dGlvbiBvbiBJRTYtOSwgd2hpY2ggaGFzIGEgaGFyZCBsaW1pdCBvbiB0aGUgIyBvZiA8c3R5bGU+XG4vLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG52YXIgaXNPbGRJRSA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIC9tc2llIFs2LTldXFxiLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSlcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAocGFyZW50SWQsIGxpc3QsIF9pc1Byb2R1Y3Rpb24pIHtcbiAgaXNQcm9kdWN0aW9uID0gX2lzUHJvZHVjdGlvblxuXG4gIHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIGxpc3QpXG4gIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG4gICAgdmFyIG1heVJlbW92ZSA9IFtdXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgICAgZG9tU3R5bGUucmVmcy0tXG4gICAgICBtYXlSZW1vdmUucHVzaChkb21TdHlsZSlcbiAgICB9XG4gICAgaWYgKG5ld0xpc3QpIHtcbiAgICAgIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhwYXJlbnRJZCwgbmV3TGlzdClcbiAgICAgIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVzID0gW11cbiAgICB9XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXVxuICAgICAgaWYgKGRvbVN0eWxlLnJlZnMgPT09IDApIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKClcbiAgICAgICAgfVxuICAgICAgICBkZWxldGUgc3R5bGVzSW5Eb21bZG9tU3R5bGUuaWRdXG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlc1RvRG9tIChzdHlsZXMgLyogQXJyYXk8U3R5bGVPYmplY3Q+ICovKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBzdHlsZXNbaV1cbiAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgZG9tU3R5bGUucmVmcysrXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pXG4gICAgICB9XG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdKSlcbiAgICAgIH1cbiAgICAgIGlmIChkb21TdHlsZS5wYXJ0cy5sZW5ndGggPiBpdGVtLnBhcnRzLmxlbmd0aCkge1xuICAgICAgICBkb21TdHlsZS5wYXJ0cy5sZW5ndGggPSBpdGVtLnBhcnRzLmxlbmd0aFxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgcGFydHMgPSBbXVxuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIHBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBzdHlsZXNJbkRvbVtpdGVtLmlkXSA9IHsgaWQ6IGl0ZW0uaWQsIHJlZnM6IDEsIHBhcnRzOiBwYXJ0cyB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVN0eWxlRWxlbWVudCAoKSB7XG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpXG4gIHN0eWxlRWxlbWVudC50eXBlID0gJ3RleHQvY3NzJ1xuICBoZWFkLmFwcGVuZENoaWxkKHN0eWxlRWxlbWVudClcbiAgcmV0dXJuIHN0eWxlRWxlbWVudFxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZSAob2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICB2YXIgdXBkYXRlLCByZW1vdmVcbiAgdmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3N0eWxlW2RhdGEtdnVlLXNzci1pZH49XCInICsgb2JqLmlkICsgJ1wiXScpXG5cbiAgaWYgKHN0eWxlRWxlbWVudCkge1xuICAgIGlmIChpc1Byb2R1Y3Rpb24pIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGFuZCBpbiBwcm9kdWN0aW9uIG1vZGUuXG4gICAgICAvLyBzaW1wbHkgZG8gbm90aGluZy5cbiAgICAgIHJldHVybiBub29wXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGJ1dCBpbiBkZXYgbW9kZS5cbiAgICAgIC8vIGZvciBzb21lIHJlYXNvbiBDaHJvbWUgY2FuJ3QgaGFuZGxlIHNvdXJjZSBtYXAgaW4gc2VydmVyLXJlbmRlcmVkXG4gICAgICAvLyBzdHlsZSB0YWdzIC0gc291cmNlIG1hcHMgaW4gPHN0eWxlPiBvbmx5IHdvcmtzIGlmIHRoZSBzdHlsZSB0YWcgaXNcbiAgICAgIC8vIGNyZWF0ZWQgYW5kIGluc2VydGVkIGR5bmFtaWNhbGx5LiBTbyB3ZSByZW1vdmUgdGhlIHNlcnZlciByZW5kZXJlZFxuICAgICAgLy8gc3R5bGVzIGFuZCBpbmplY3QgbmV3IG9uZXMuXG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgaWYgKGlzT2xkSUUpIHtcbiAgICAvLyB1c2Ugc2luZ2xldG9uIG1vZGUgZm9yIElFOS5cbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrK1xuICAgIHN0eWxlRWxlbWVudCA9IHNpbmdsZXRvbkVsZW1lbnQgfHwgKHNpbmdsZXRvbkVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKSlcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCBmYWxzZSlcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCB0cnVlKVxuICB9IGVsc2Uge1xuICAgIC8vIHVzZSBtdWx0aS1zdHlsZS10YWcgbW9kZSBpbiBhbGwgb3RoZXIgY2FzZXNcbiAgICBzdHlsZUVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQpXG4gICAgcmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmopXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmogLyogU3R5bGVPYmplY3RQYXJ0ICovKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcbiAgICAgICAgICBuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJlxuICAgICAgICAgIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKVxuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKVxuICAgIH1cbiAgfVxufVxuXG52YXIgcmVwbGFjZVRleHQgPSAoZnVuY3Rpb24gKCkge1xuICB2YXIgdGV4dFN0b3JlID0gW11cblxuICByZXR1cm4gZnVuY3Rpb24gKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudFxuICAgIHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpXG4gIH1cbn0pKClcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyAoc3R5bGVFbGVtZW50LCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcbiAgdmFyIGNzcyA9IHJlbW92ZSA/ICcnIDogb2JqLmNzc1xuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKVxuICB9IGVsc2Uge1xuICAgIHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKVxuICAgIHZhciBjaGlsZE5vZGVzID0gc3R5bGVFbGVtZW50LmNoaWxkTm9kZXNcbiAgICBpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSlcbiAgICBpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChjc3NOb2RlKVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRvVGFnIChzdHlsZUVsZW1lbnQsIG9iaikge1xuICB2YXIgY3NzID0gb2JqLmNzc1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWFcbiAgdmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXBcblxuICBpZiAobWVkaWEpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKCdtZWRpYScsIG1lZGlhKVxuICB9XG5cbiAgaWYgKHNvdXJjZU1hcCkge1xuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLmNocm9tZS5jb20vZGV2dG9vbHMvZG9jcy9qYXZhc2NyaXB0LWRlYnVnZ2luZ1xuICAgIC8vIHRoaXMgbWFrZXMgc291cmNlIG1hcHMgaW5zaWRlIHN0eWxlIHRhZ3Mgd29yayBwcm9wZXJseSBpbiBDaHJvbWVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VVUkw9JyArIHNvdXJjZU1hcC5zb3VyY2VzWzBdICsgJyAqLydcbiAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNjYwMzg3NVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCwnICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArICcgKi8nXG4gIH1cblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzXG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpXG4gICAgfVxuICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKVxuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmVkaXRTY2hlZHVsZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uZWRpdFNjaGVkdWxlRm9ybS5lcnJvcnMuaGFzKCdzY2hlZHVsZV90eXBlJylcbiAgICB9XG4gIH0sIFtfdm0uX20oMCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwibmFtZVwiOiBcInNjaGVkdWxlX3R5cGVcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRzZWxlY3RlZFZhbCA9IEFycmF5LnByb3RvdHlwZS5maWx0ZXIuY2FsbCgkZXZlbnQudGFyZ2V0Lm9wdGlvbnMsIGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICByZXR1cm4gby5zZWxlY3RlZFxuICAgICAgICB9KS5tYXAoZnVuY3Rpb24obykge1xuICAgICAgICAgIHZhciB2YWwgPSBcIl92YWx1ZVwiIGluIG8gPyBvLl92YWx1ZSA6IG8udmFsdWU7XG4gICAgICAgICAgcmV0dXJuIHZhbFxuICAgICAgICB9KTtcbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmVkaXRTY2hlZHVsZUZvcm0sIFwic2NoZWR1bGVUeXBlXCIsICRldmVudC50YXJnZXQubXVsdGlwbGUgPyAkJHNlbGVjdGVkVmFsIDogJCRzZWxlY3RlZFZhbFswXSlcbiAgICAgIH1cbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLnNjaGVkdWxlVHlwZXMpLCBmdW5jdGlvbihzY2hlZHVsZVR5cGUpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogc2NoZWR1bGVUeXBlLmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICBcXHRcXHRcXHRcIiArIF92bS5fcyhzY2hlZHVsZVR5cGUubmFtZSkgKyBcIlxcblxcdFxcdCAgICBcXHRcXHRcIildKVxuICB9KSwgMCksIF92bS5fdihcIiBcIiksIChfdm0uZWRpdFNjaGVkdWxlRm9ybS5lcnJvcnMuaGFzKCdzY2hlZHVsZV90eXBlJykpID8gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiLFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInRleHRDb250ZW50XCI6IF92bS5fcyhfdm0uZWRpdFNjaGVkdWxlRm9ybS5lcnJvcnMuZ2V0KCdzY2hlZHVsZV90eXBlJykpXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMSB8fCBfdm0uZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMiksXG4gICAgICBleHByZXNzaW9uOiBcImVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDEgfHwgIGVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDJcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDEpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5lZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAxKSxcbiAgICAgIGV4cHJlc3Npb246IFwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJib290c3RyYXAtdGltZXBpY2tlciB0aW1lcGlja2VyXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbiksXG4gICAgICBleHByZXNzaW9uOiBcImVkaXRTY2hlZHVsZUZvcm0udGltZUluXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidGltZXBpY2tlcjFcIixcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS50aW1lSW4pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNjaGVkdWxlRm9ybSwgXCJ0aW1lSW5cIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW192bS5fdihcInRvXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYm9vdHN0cmFwLXRpbWVwaWNrZXIgdGltZXBpY2tlclwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0KSxcbiAgICAgIGV4cHJlc3Npb246IFwiZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidGltZXBpY2tlcjJcIixcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0KVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmVkaXRTY2hlZHVsZUZvcm0sIFwidGltZU91dFwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAyXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cFwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfdm0uX3YoXCJUaW1lIEluIEZyb21cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJib290c3RyYXAtdGltZXBpY2tlciB0aW1lcGlja2VyXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20pLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb21cIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBpbnB1dC1zbWFsbCB0aW1lcGlja2VyLWlucHV0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0aW1lcGlja2VyM1wiLFxuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNjaGVkdWxlRm9ybSwgXCJ0aW1lSW5Gcm9tXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMTBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXBcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX3ZtLl92KFwiVGltZSBJbiBUb1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTY2hlZHVsZUZvcm0udGltZUluVG8pLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidGltZXBpY2tlcjRcIixcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5UbylcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5lZGl0U2NoZWR1bGVGb3JtLCBcInRpbWVJblRvXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJFZGl0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgIFNjaGVkdWxlIFR5cGU6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgIFNjaGVkdWxlIERldGFpbHM6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi05ZjkxNjg0NFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTlmOTE2ODQ0XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzM1xuLy8gbW9kdWxlIGNodW5rcyA9IDExIDEyIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi05ZjkxNjg0NFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL2VkaXQtc2NoZWR1bGUudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCI4ODA1OTU0MlwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtOWY5MTY4NDRcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9lZGl0LXNjaGVkdWxlLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtOWY5MTY4NDRcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9lZGl0LXNjaGVkdWxlLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTlmOTE2ODQ0XCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzNFxuLy8gbW9kdWxlIGNodW5rcyA9IDExIDEyIiwiPHRlbXBsYXRlPlxuPGRpdiBjbGFzcz1cIm1vZGFsIG1kLW1vZGFsIGZhZGVcIiA6aWQ9XCJpZFwiIHRhYmluZGV4PVwiLTFcIiByb2xlPVwiZGlhbG9nXCIgYXJpYS1sYWJlbGxlZGJ5PVwibW9kYWwtbGFiZWxcIj5cbiAgICA8ZGl2IDpjbGFzcz1cIidtb2RhbC1kaWFsb2cgJytzaXplXCIgcm9sZT1cImRvY3VtZW50XCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtaGVhZGVyXCI+XG4gICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwibW9kYWwtdGl0bGVcIiBpZD1cIm1vZGFsLWxhYmVsXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC10aXRsZVwiPk1vZGFsIFRpdGxlPC9zbG90PlxuICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+XG4gICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLWJvZHlcIj5Nb2RhbCBCb2R5PC9zbG90PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLWZvb3RlclwiPlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG48L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gICAgcHJvcHM6IHtcbiAgICAgICAgJ2lkJzp7cmVxdWlyZWQ6dHJ1ZX1cbiAgICAgICAgLCdzaXplJzoge2RlZmF1bHQ6J21vZGFsLW1kJ31cbiAgICB9XG59XG48L3NjcmlwdD5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBEaWFsb2dNb2RhbC52dWU/MDAzYmRhODgiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTRkZTYwNjU1XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIERpYWxvZ01vZGFsLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi00ZGU2MDY1NVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTRkZTYwNjU1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDVcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDUgNiA5IDExIDE4IDE5IDIwIDIxIDIyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwgbWQtbW9kYWwgZmFkZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IF92bS5pZCxcbiAgICAgIFwidGFiaW5kZXhcIjogXCItMVwiLFxuICAgICAgXCJyb2xlXCI6IFwiZGlhbG9nXCIsXG4gICAgICBcImFyaWEtbGFiZWxsZWRieVwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGNsYXNzOiAnbW9kYWwtZGlhbG9nICcgKyBfdm0uc2l6ZSxcbiAgICBhdHRyczoge1xuICAgICAgXCJyb2xlXCI6IFwiZG9jdW1lbnRcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWhlYWRlclwiXG4gIH0sIFtfYygnaDQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtdGl0bGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiLCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtYm9keVwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1ib2R5XCIsIFtfdm0uX3YoXCJNb2RhbCBCb2R5XCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtZm9vdGVyXCIsIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTRkZTYwNjU1XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNGRlNjA2NTVcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDUgNiA5IDExIDE4IDE5IDIwIDIxIDIyIiwiLyoqXG4gKiBUcmFuc2xhdGVzIHRoZSBsaXN0IGZvcm1hdCBwcm9kdWNlZCBieSBjc3MtbG9hZGVyIGludG8gc29tZXRoaW5nXG4gKiBlYXNpZXIgdG8gbWFuaXB1bGF0ZS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBsaXN0VG9TdHlsZXMgKHBhcmVudElkLCBsaXN0KSB7XG4gIHZhciBzdHlsZXMgPSBbXVxuICB2YXIgbmV3U3R5bGVzID0ge31cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldXG4gICAgdmFyIGlkID0gaXRlbVswXVxuICAgIHZhciBjc3MgPSBpdGVtWzFdXG4gICAgdmFyIG1lZGlhID0gaXRlbVsyXVxuICAgIHZhciBzb3VyY2VNYXAgPSBpdGVtWzNdXG4gICAgdmFyIHBhcnQgPSB7XG4gICAgICBpZDogcGFyZW50SWQgKyAnOicgKyBpLFxuICAgICAgY3NzOiBjc3MsXG4gICAgICBtZWRpYTogbWVkaWEsXG4gICAgICBzb3VyY2VNYXA6IHNvdXJjZU1hcFxuICAgIH1cbiAgICBpZiAoIW5ld1N0eWxlc1tpZF0pIHtcbiAgICAgIHN0eWxlcy5wdXNoKG5ld1N0eWxlc1tpZF0gPSB7IGlkOiBpZCwgcGFydHM6IFtwYXJ0XSB9KVxuICAgIH0gZWxzZSB7XG4gICAgICBuZXdTdHlsZXNbaWRdLnBhcnRzLnB1c2gocGFydClcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHN0eWxlc1xufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qc1xuLy8gbW9kdWxlIGlkID0gN1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiAzIDQgNSA5IDExIDEyIDEzIl0sInNvdXJjZVJvb3QiOiIifQ==