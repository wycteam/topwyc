/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 486);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['groupreport', 'groupid', 'clientsid', 'clientservices', 'trackings', 'iswritereportpage'],

	data: function data() {
		return {

			docs: [],
			serviceDocs: [],

			multipleQuickReportForm: new Form({
				distinctClientServices: [],

				groupReport: null,
				groupId: null,

				clientsId: [],
				clientServicesId: [],
				trackings: [],

				actionId: [],

				categoryId: [],
				categoryName: [],

				date1: [],
				date2: [],
				date3: [],
				dateTimeArray: [],

				extraDetails: []
			}, { baseURL: axiosAPIv1.defaults.baseURL })

		};
	},


	methods: {
		fetchDocs: function fetchDocs(id) {
			var _this = this;

			var ids = '';
			$.each(id, function (i, item) {
				ids += id[i].docs_needed + ',' + id[i].docs_optional + ',';
			});

			var uniqueList = ids.split(',').filter(function (allItems, i, a) {
				return i == a.indexOf(allItems);
			}).join(',');

			axios.get('/visa/group/' + uniqueList + '/service-docs').then(function (result) {
				_this.docs = result.data;
			});

			setTimeout(function () {
				$('.chosen-select-for-docs').trigger('chosen:updated');
			}, 5000);
		},
		getServiceDocs: function getServiceDocs() {
			var _this2 = this;

			axios.get('/visa/group/service-docs-index').then(function (result) {
				_this2.serviceDocs = result.data;
			});
		},
		resetMultipleQuickReportForm: function resetMultipleQuickReportForm() {
			var _this3 = this;

			// MultipleQuickReportForm.vue
			this.distinctClientServices.forEach(function (distinctClientService, index) {
				var ref = 'form-' + index;

				_this3.$refs[ref][0].multipleQuickReportform = {
					actionId: 1,

					categoryId: null,
					categoryName: null,

					date1: null,
					date2: null,
					date3: null,
					dateTimeArray: [],

					extraDetails: []
				};

				_this3.$refs[ref][0].$refs.date2.value = '';
				_this3.$refs[ref][0].$refs.date3.value = '';
				_this3.$refs[ref][0].$refs.date4.value = '';
				_this3.$refs[ref][0].$refs.date5.value = '';
				_this3.$refs[ref][0].$refs.date6.value = '';
				_this3.$refs[ref][0].$refs.date7.value = '';
				_this3.$refs[ref][0].$refs.date8.value = '';
				_this3.$refs[ref][0].$refs.date9.value = '';
				_this3.$refs[ref][0].$refs.date10.value = '';
				_this3.$refs[ref][0].$refs.date11.value = '';
				_this3.$refs[ref][0].$refs.date12.value = '';
				_this3.$refs[ref][0].$refs.date13.value = '';
				_this3.$refs[ref][0].$refs.date14.value = '';
				_this3.$refs[ref][0].$refs.date15.value = '';
				_this3.$refs[ref][0].$refs.date16.value = '';
				_this3.$refs[ref][0].$refs.categoryName.value = '';

				_this3.$refs[ref][0].customCategory = false;
			});

			// MultipleQuickReport.vue
			this.multipleQuickReportForm = new Form({
				distinctClientServices: [],

				actionId: [],

				categoryId: [],
				categoryName: [],

				date1: [],
				date2: [],
				date3: [],
				dateTimeArray: [],

				extraDetails: []
			}, { baseURL: axiosAPIv1.defaults.baseURL });
		},
		validate: function validate() {
			var _this4 = this;

			var isValid = true;

			this.distinctClientServices.forEach(function (distinctClientService, index) {
				var ref = 'form-' + index;

				if (!_this4.$refs[ref][0].validate(distinctClientService)) {
					isValid = false;
				}
			});

			return isValid;
		},
		addMultipleQuickReport: function addMultipleQuickReport() {
			var _this5 = this;

			if (this.validate()) {
				this.multipleQuickReportForm.groupReport = this.groupreport;
				this.multipleQuickReportForm.groupId = this.groupid;
				this.multipleQuickReportForm.clientsId = this.clientsid;
				this.multipleQuickReportForm.clientServicesId = this.clientservices;
				this.multipleQuickReportForm.trackings = this.trackings;
				this.multipleQuickReportForm.distinctClientServices = this.distinctClientServices;

				this.distinctClientServices.forEach(function (distinctClientService, index) {
					var ref = 'form-' + index;

					var _actionId = _this5.$refs[ref][0].multipleQuickReportform.actionId;
					var _categoryId = _this5.$refs[ref][0].multipleQuickReportform.categoryId;
					var _categoryName = _this5.$refs[ref][0].multipleQuickReportform.categoryName;
					var _date1 = _this5.$refs[ref][0].multipleQuickReportform.date1;
					var _date2 = _this5.$refs[ref][0].multipleQuickReportform.date2;
					var _date3 = _this5.$refs[ref][0].multipleQuickReportform.date3;
					var _dateTimeArray = _this5.$refs[ref][0].multipleQuickReportform.dateTimeArray;
					var _extraDetails = _this5.$refs[ref][0].multipleQuickReportform.extraDetails;

					_this5.multipleQuickReportForm.actionId.push(_actionId);
					_this5.multipleQuickReportForm.categoryId.push(_categoryId);
					_this5.multipleQuickReportForm.categoryName.push(_categoryName);
					_this5.multipleQuickReportForm.date1.push(_date1);
					_this5.multipleQuickReportForm.date2.push(_date2);
					_this5.multipleQuickReportForm.date3.push(_date3);
					_this5.multipleQuickReportForm.dateTimeArray.push(_dateTimeArray);
					_this5.multipleQuickReportForm.extraDetails.push(_extraDetails);
				});

				this.multipleQuickReportForm.submit('post', '/visa/quick-report').then(function (response) {
					if (response.success) {
						_this5.resetMultipleQuickReportForm();

						$('#add-quick-report-modal').modal('hide');

						toastr.success(response.message);

						if (_this5.iswritereportpage) {
							$('#add-quick-report-modal').on('hidden.bs.modal', function (e) {
								window.location.href = '/visa/report/write-report';
							});
						} else {
							setTimeout(function () {
								location.reload();
							}, 1000);
						}
					}
				});
			}
		}
	},

	created: function created() {
		this.getServiceDocs();
	},


	computed: {
		_isWriteReportPage: function _isWriteReportPage() {
			return this.iswritereportpage || false;
		},
		distinctClientServices: function distinctClientServices() {
			var distinctClientServices = [];

			this.clientservices.forEach(function (clientService) {
				if (distinctClientServices.indexOf(clientService.detail) == -1) {
					distinctClientServices.push(clientService.detail);
				}
			});

			return distinctClientServices;
		}
	},

	components: {
		'multiple-quick-report-form': __webpack_require__(15)
	}

});

/***/ }),

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

	props: ['index', 'docs', 'servicedocs'],

	data: function data() {
		return {

			actions: [],
			categories: [],

			companyCouriers: [],

			customCategory: false,

			multipleQuickReportform: {
				actionId: 1,

				categoryId: null,
				categoryName: null,

				date1: null,
				date2: null,
				date3: null,
				dateTimeArray: [],

				extraDetails: []
			}

		};
	},


	methods: {
		getActions: function getActions() {
			var _this = this;

			axios.get('/visa/actions').then(function (response) {
				_this.actions = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		getCompanyCouriers: function getCompanyCouriers() {
			var _this2 = this;

			var role = 'company-courier';

			axios.get('/visa/users-by-role/' + role).then(function (response) {
				_this2.companyCouriers = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		getCurrentDate: function getCurrentDate() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1;
			var yyyy = today.getFullYear();

			if (dd < 10) {
				dd = '0' + dd;
			}

			if (mm < 10) {
				mm = '0' + mm;
			}

			return mm + '/' + dd + '/' + yyyy;
		},
		changeActionHandler: function changeActionHandler(actionId) {
			// Reset
			this.multipleQuickReportform.date1 = null;
			this.multipleQuickReportform.date2 = null;
			this.multipleQuickReportform.date3 = null;
			this.multipleQuickReportform.dateTimeArray = [];
			this.multipleQuickReportform.extraDetails = [];

			// For auto capture date
			if (actionId == 1 || actionId == 3 || actionId == 4 || actionId == 7 || actionId == 11 || actionId == 15 || actionId == 16) {
				this.multipleQuickReportform.date1 = this.getCurrentDate();
			}
		},
		getCategories: function getCategories() {
			var _this3 = this;

			var actionId = this.multipleQuickReportform.actionId;

			this.changeActionHandler(actionId);

			this.categories = [];

			axios.get('/visa/categories-by-action-id', {
				params: {
					actionId: actionId
				}
			}).then(function (response) {
				_this3.categories = response.data;
			}).catch(function (error) {
				return console.log(error);
			});
		},
		changeCategoryHandler: function changeCategoryHandler() {
			if (this.multipleQuickReportform.actionId == 1 && this.multipleQuickReportform.categoryId == 1) {
				this.addDateTimePickerArray();
			}
		},
		addDateTimePickerArray: function addDateTimePickerArray() {
			this.multipleQuickReportform.dateTimeArray.push({ value: null });

			setTimeout(function () {
				$('.datetimepickerarray').inputmask("datetime", {
					mask: "y-2-1 h:s:s",
					placeholder: "yyyy-mm-dd hh:mm:ss",
					leapday: "-02-29",
					separator: "-",
					alias: "yyyy-mm-dd"
				});
			}, 500);
		},
		removeDateTimePickerArray: function removeDateTimePickerArray(index) {
			this.multipleQuickReportform.dateTimeArray.splice(index, 1);
		},
		validate: function validate(distinctClientService) {
			var isValid = true;

			if (this.multipleQuickReportform.actionId == 1) {
				var date2 = this.$refs.date2.value;
				var date3 = this.$refs.date3.value ? this.$refs.date3.value : '';

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date2 == '') {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					var dateTimeArray = [];
					$('input[name^="dateTimeArray"]').each(function (e) {
						if (this.value != '') {
							dateTimeArray.push(this.value);
						}
					});

					this.multipleQuickReportform.date2 = date2;
					if (this.multipleQuickReportform.categoryId == 6 || this.multipleQuickReportform.categoryId == 7 || this.multipleQuickReportform.categoryId == 10 || this.multipleQuickReportform.categoryId == 14 || this.multipleQuickReportform.categoryId == 15) {
						this.multipleQuickReportform.date3 = date3;
					}

					this.multipleQuickReportform.dateTimeArray = dateTimeArray;
				}
			} else if (this.multipleQuickReportform.actionId == 2 || this.multipleQuickReportform.actionId == 3) {
				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				}
			} else if (this.multipleQuickReportform.actionId == 4) {
				var docs = [];
				$('#chosen-' + this.index + ' option:selected').each(function (e) {
					docs.push($(this).val());
				});

				if (docs.length == 0) {
					toastr.error('Please select documents.', distinctClientService);
					isValid = false;
				} else {
					$('#chosen-' + this.index).val('').trigger('chosen:updated');

					this.multipleQuickReportform.extraDetails = docs;
				}
			} else if (this.multipleQuickReportform.actionId == 6) {
				var date4 = this.$refs.date4.value;

				if (date4 == '') {
					toastr.error('Please fill up submission date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date4;
				}
			} else if (this.multipleQuickReportform.actionId == 7) {
				var date5 = this.$refs.date5.value;

				if (date5 == '') {
					toastr.error('Please fill up estimated time of finishing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date5;
				}
			} else if (this.multipleQuickReportform.actionId == 8) {
				var date6 = this.$refs.date6.value;
				var date7 = this.$refs.date7.value;
				var date8 = this.$refs.date8.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date6 == '' || date7 == '') {
					toastr.error('Please fill up affected date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date6;
					this.multipleQuickReportform.date2 = date7;
					this.multipleQuickReportform.date3 = date8;
				}
			} else if (this.multipleQuickReportform.actionId == 9) {
				if (this.customCategory) {
					var categoryName = this.$refs.categoryName.value;

					if (categoryName == '') {
						toastr.error('Please select category.', distinctClientService);
						isValid = false;
					} else {
						this.multipleQuickReportform.categoryId = 0;
						this.multipleQuickReportform.categoryName = categoryName;
					}
				}

				var date9 = this.$refs.date9.value;
				var date10 = this.$refs.date10.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date9 == '' || date10 == '') {
					toastr.error('Please fill up date range.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date9;
					this.multipleQuickReportform.date2 = date10;
				}
			} else if (this.multipleQuickReportform.actionId == 10) {
				var date11 = this.$refs.date11.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date11 == '') {
					toastr.error('Please fill up extended period for processing.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date11;
				}
			} else if (this.multipleQuickReportform.actionId == 11) {
				var date12 = this.$refs.date12.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date12 == '') {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date12;
				}
			} else if (this.multipleQuickReportform.actionId == 12) {
				var date13 = this.$refs.date13.value;

				if (date13 == '') {
					toastr.error('Please fill up date and time pick up.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date13;
				}
			} else if (this.multipleQuickReportform.actionId == 13) {
				var date14 = this.$refs.date14.value;

				if (date14 == '') {
					toastr.error('Please fill up date and time deliver.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date14;
				}
			} else if (this.multipleQuickReportform.actionId == 14) {
				var date15 = this.$refs.date15.value;
				var companyCourier = this.$refs.company_courier.value;
				var trackingNumber = this.$refs.tracking_number.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (this.multipleQuickReportform.categoryId == 128 && companyCourier == '') {
					toastr.error('Please select company courier.', distinctClientService);
					isValid = false;
				} else if (this.multipleQuickReportform.categoryId != 128 && trackingNumber == '') {
					toastr.error('Please fill up tracking number.', distinctClientService);
					isValid = false;
				} else if (date15 == '') {
					toastr.error('Please fill up date and time delivered.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date1 = date15;

					var extraDetail = this.multipleQuickReportform.categoryId == 128 ? companyCourier : trackingNumber;
					this.multipleQuickReportform.extraDetails = [extraDetail];
				}
			} else if (this.multipleQuickReportform.actionId == 15) {
				var date16 = this.$refs.date16.value;

				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				} else if (date16 == '' && (this.multipleQuickReportform.categoryId == 76 || this.multipleQuickReportform.categoryId == 77 || this.multipleQuickReportform.categoryId == 78 || this.multipleQuickReportform.categoryId == 79 || this.multipleQuickReportform.categoryId == 80 || this.multipleQuickReportform.categoryId == 81 || this.multipleQuickReportform.categoryId == 82 || this.multipleQuickReportform.categoryId == 83)) {
					toastr.error('Please fill up estimated releasing date.', distinctClientService);
					isValid = false;
				} else {
					this.multipleQuickReportform.date2 = date16;
				}
			} else if (this.multipleQuickReportform.actionId == 16) {
				if (this.multipleQuickReportform.categoryId == null) {
					toastr.error('Please select category.', distinctClientService);
					isValid = false;
				}
			}

			return isValid;
		}
	},

	created: function created() {
		this.getActions();

		this.getCategories();

		this.getCompanyCouriers();
	},
	mounted: function mounted() {

		// Chosen
		$('.chosen-select-for-docs, .chosen-select-for-docs-2').chosen({
			width: "100%",
			no_results_text: "No result/s found.",
			search_contains: true
		});

		// DatePicker
		$('.datepicker').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			autoclose: true,
			format: "mm/dd/yyyy",
			startDate: new Date()
		});

		// DateTime Picker
		$('.datetimepicker').inputmask("datetime", {
			mask: "y-2-1 h:s:s",
			placeholder: "yyyy-mm-dd hh:mm:ss",
			leapday: "-02-29",
			separator: "-",
			alias: "yyyy-mm-dd"
		});
	}
});

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-0f2f2b77] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-0f2f2b77] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["MultipleQuickReport.vue?35f996b9"],"names":[],"mappings":";AA4PA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"MultipleQuickReport.vue","sourcesContent":["<template>\n\t\n\t<form class=\"form-horizontal\" @submit.prevent=\"addMultipleQuickReport\">\n\n\t\t<div class=\"ios-scroll\">\n\t\t\t<div v-for=\"(distinctClientService, index) in distinctClientServices.length\">\n\n\t\t\t\t<div class=\"panel panel-default\">\n\t                <div class=\"panel-heading\">\n\t                    {{ distinctClientServices[index] }}\n\t                </div>\n\t                <div class=\"panel-body\">\n\t                    \n\t                \t<multiple-quick-report-form :key=\"index\" :ref=\"'form-' + index\" :docs=\"docs\" :servicedocs=\"serviceDocs\" :index=\"index\"></multiple-quick-report-form>\n\n\t                </div> <!-- panel-body -->\n\t            </div> <!-- panel -->\n\n\t\t\t</div> <!-- loop -->\n\t\t</div> <!-- ios-scroll -->\n\t\t\n\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Create Report</button>\n\t    <button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\t</form>\n\n</template>\n\n<script>\n\n\texport default {\n\n\t\tprops: ['groupreport', 'groupid', 'clientsid', 'clientservices', 'trackings', 'iswritereportpage'],\n\n\t\tdata() {\n\t\t\treturn {\n\t\t\n\t\t\t\tdocs: [],\n\t\t\t\tserviceDocs: [],\n\n        \t\tmultipleQuickReportForm : new Form({\n        \t\t\tdistinctClientServices:[],\n\n        \t\t\tgroupReport: null,\n        \t\t\tgroupId: null,\n\n        \t\t\tclientsId: [],\n        \t\t\tclientServicesId: [],\n        \t\t\ttrackings: [],\n\n        \t\t\tactionId: [],\n\n        \t\t\tcategoryId: [],\n        \t\t\tcategoryName: [],\n\n        \t\t\tdate1: [],\n        \t\t\tdate2: [],\n        \t\t\tdate3: [],\n        \t\t\tdateTimeArray: [],\n\n        \t\t\textraDetails: []\n        \t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\n\t\t\tfetchDocs(id) {\n\t\t\t\tvar ids = '';\n\t\t\t\t$.each(id, function(i, item) {\n\t\t\t\t    ids += id[i].docs_needed + ',' + id[i].docs_optional + ',';\n\t\t\t\t});\n\n\t\t\t\tvar uniqueList=ids.split(',').filter(function(allItems,i,a){\n\t\t\t\t    return i==a.indexOf(allItems);\n\t\t\t\t}).join(',');\n\t        \t\n\t\t\t \taxios.get('/visa/group/'+uniqueList+'/service-docs')\n\t\t\t \t  .then(result => {\n\t\t            this.docs = result.data;\n\t\t        });\n\n\t        \tsetTimeout(() => {\n\t        \t\t$('.chosen-select-for-docs').trigger('chosen:updated');\t\n\t        \t}, 5000);\n\t\t\t},\n\n\t\t\tgetServiceDocs() {\n        \t\taxios.get('/visa/group/service-docs-index')\n\t\t\t \t  .then(result => {\n\t\t            this.serviceDocs = result.data;\n\t\t        });\n        \t},\n\n\t\t\tresetMultipleQuickReportForm() {\n\t\t\t\t// MultipleQuickReportForm.vue\n\t\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\t\tthis.$refs[ref][0].multipleQuickReportform = {\n\t\t        \t\t\tactionId: 1,\n\n\t\t        \t\t\tcategoryId: null,\n\t\t        \t\t\tcategoryName: null,\n\n\t\t        \t\t\tdate1: null,\n\t\t        \t\t\tdate2: null,\n\t\t        \t\t\tdate3: null,\n\t\t        \t\t\tdateTimeArray: [],\n\n\t\t        \t\t\textraDetails: []\n\t\t        \t\t}\n\n\t\t        \t\tthis.$refs[ref][0].$refs.date2.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date3.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date4.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date5.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date6.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date7.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date8.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date9.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date10.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date11.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date12.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date13.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date14.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date15.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.date16.value = '';\n\t\t        \t\tthis.$refs[ref][0].$refs.categoryName.value = '';\n\n\t\t        \t\tthis.$refs[ref][0].customCategory = false;\n\t\t\t\t\t});\n\n        \t\t// MultipleQuickReport.vue\n\t        \t\tthis.multipleQuickReportForm = new Form({\n\t        \t\t\tdistinctClientServices:[],\n\n\t        \t\t\tactionId: [],\n\n\t        \t\t\tcategoryId: [],\n\t        \t\t\tcategoryName: [],\n\n\t        \t\t\tdate1: [],\n\t        \t\t\tdate2: [],\n\t        \t\t\tdate3: [],\n\t        \t\t\tdateTimeArray: [],\n\n\t        \t\t\textraDetails: []\n\t        \t\t}, { baseURL: axiosAPIv1.defaults.baseURL });\n\t\t\t},\n\n\t\t\tvalidate() {\n\t\t\t\tvar isValid = true;\n\n\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\tif( ! this.$refs[ref][0].validate(distinctClientService) ) {\n\t\t\t\t\t\tisValid = false;\n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\treturn isValid;\n\t\t\t},\n\n\t\t\taddMultipleQuickReport() {\n\t\t\t\tif(this.validate()) {\n\t\t\t\t\tthis.multipleQuickReportForm.groupReport = this.groupreport;\n\t\t\t\t\tthis.multipleQuickReportForm.groupId = this.groupid;\n\t\t\t\t\tthis.multipleQuickReportForm.clientsId = this.clientsid;\n\t\t\t\t\tthis.multipleQuickReportForm.clientServicesId = this.clientservices;\n\t\t\t\t\tthis.multipleQuickReportForm.trackings = this.trackings;\n\t\t\t\t\tthis.multipleQuickReportForm.distinctClientServices = this.distinctClientServices;\n\n\t\t\t\t\tthis.distinctClientServices.forEach((distinctClientService, index) => {\n\t\t\t\t\t\tlet ref = 'form-' + index;\n\n\t\t\t\t\t\tlet _actionId = this.$refs[ref][0].multipleQuickReportform.actionId;\n\t\t\t\t\t\tlet _categoryId = this.$refs[ref][0].multipleQuickReportform.categoryId;\n\t\t\t\t\t\tlet _categoryName = this.$refs[ref][0].multipleQuickReportform.categoryName;\n\t\t\t\t\t\tlet _date1 = this.$refs[ref][0].multipleQuickReportform.date1;\n\t\t\t\t\t\tlet _date2 = this.$refs[ref][0].multipleQuickReportform.date2;\n\t\t\t\t\t\tlet _date3 = this.$refs[ref][0].multipleQuickReportform.date3;\n\t\t\t\t\t\tlet _dateTimeArray = this.$refs[ref][0].multipleQuickReportform.dateTimeArray;\n\t\t\t\t\t\tlet _extraDetails = this.$refs[ref][0].multipleQuickReportform.extraDetails;\n\n\t\t\t\t\t\tthis.multipleQuickReportForm.actionId.push(_actionId);\n\t\t\t\t\t\tthis.multipleQuickReportForm.categoryId.push(_categoryId);\n\t        \t\t\tthis.multipleQuickReportForm.categoryName.push(_categoryName);\n\t        \t\t\tthis.multipleQuickReportForm.date1.push(_date1);\n\t        \t\t\tthis.multipleQuickReportForm.date2.push(_date2);\n\t        \t\t\tthis.multipleQuickReportForm.date3.push(_date3);\n\t        \t\t\tthis.multipleQuickReportForm.dateTimeArray.push(_dateTimeArray);\n\t        \t\t\tthis.multipleQuickReportForm.extraDetails.push(_extraDetails);\n\t\t\t\t\t});\n\n\t\t\t\t\tthis.multipleQuickReportForm.submit('post', '/visa/quick-report')\n\t\t\t            .then(response => {\n\t\t\t            \tif(response.success) {\n\t\t\t            \t\tthis.resetMultipleQuickReportForm();\n\t\t\t            \t\t\n\t\t\t            \t\t$('#add-quick-report-modal').modal('hide');\n\n\t\t\t            \t\ttoastr.success(response.message);\n\n\t\t\t            \t\tif(this.iswritereportpage) {\n\t\t\t            \t\t\t$('#add-quick-report-modal').on('hidden.bs.modal', function (e) {\n\t\t\t\t\t\t\t\t\t  \twindow.location.href = '/visa/report/write-report';\n\t\t\t\t\t\t\t\t\t});\n\t\t\t            \t\t} else {\n\t\t\t            \t\t\tsetTimeout(() => {\n\t\t\t\t            \t\t\tlocation.reload();\n\t\t\t\t            \t\t}, 1000);\n\t\t\t            \t\t}\n\t\t\t            \t}\n\t\t\t            });\n\t\t\t\t}\n\t\t\t}\n\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.getServiceDocs();\n\t\t},\n\n\t\tcomputed: {\n\t\t\t_isWriteReportPage() {\n\t\t      \treturn this.iswritereportpage || false;\n\t\t    },\n\n\t\t\tdistinctClientServices() {\n\t\t\t\tlet distinctClientServices = [];\n\n\t\t\t\tthis.clientservices.forEach(clientService => {\n\t\t\t\t\tif(distinctClientServices.indexOf(clientService.detail) == -1) {\n\t\t\t\t\t\tdistinctClientServices.push(clientService.detail);\n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\treturn distinctClientServices;\n\t\t\t}\n\t\t},\n\n\t\tcomponents: {\n\t        'multiple-quick-report-form': require('./MultipleQuickReportForm.vue')\n\t    }\n\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(20)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(10),
  /* template */
  __webpack_require__(16),
  /* scopeId */
  "data-v-0f2f2b77",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/MultipleQuickReport.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] MultipleQuickReport.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f2f2b77", Component.options)
  } else {
    hotAPI.reload("data-v-0f2f2b77", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(11),
  /* template */
  __webpack_require__(18),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/components/Visa/MultipleQuickReportForm.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] MultipleQuickReportForm.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-eb2fda4a", Component.options)
  } else {
    hotAPI.reload("data-v-eb2fda4a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addMultipleQuickReport($event)
      }
    }
  }, [_c('div', {
    staticClass: "ios-scroll"
  }, _vm._l((_vm.distinctClientServices.length), function(distinctClientService, index) {
    return _c('div', [_c('div', {
      staticClass: "panel panel-default"
    }, [_c('div', {
      staticClass: "panel-heading"
    }, [_vm._v("\n                    " + _vm._s(_vm.distinctClientServices[index]) + "\n                ")]), _vm._v(" "), _c('div', {
      staticClass: "panel-body"
    }, [_c('multiple-quick-report-form', {
      key: index,
      ref: 'form-' + index,
      refInFor: true,
      attrs: {
        "docs": _vm.docs,
        "servicedocs": _vm.serviceDocs,
        "index": index
      }
    })], 1)])])
  }), 0), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Create Report")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0f2f2b77", module.exports)
  }
}

/***/ }),

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.actionId),
      expression: "multipleQuickReportform.actionId"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "action"
    },
    on: {
      "change": [function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.multipleQuickReportform, "actionId", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }, _vm.getCategories]
    }
  }, _vm._l((_vm.actions), function(action) {
    return _c('option', {
      domProps: {
        "value": action.id
      }
    }, [_vm._v("\n\t\t           \t\t" + _vm._s(action.name) + "\n\t\t            ")])
  }), 0)])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 1 || _vm.multipleQuickReportform.actionId == 2 || _vm.multipleQuickReportform.actionId == 3 || _vm.multipleQuickReportform.actionId == 8 || _vm.multipleQuickReportform.actionId == 9 || _vm.multipleQuickReportform.actionId == 10 || _vm.multipleQuickReportform.actionId == 11 || _vm.multipleQuickReportform.actionId == 14 || _vm.multipleQuickReportform.actionId == 15 || _vm.multipleQuickReportform.actionId == 16),
      expression: "multipleQuickReportform.actionId == 1 || multipleQuickReportform.actionId == 2 || multipleQuickReportform.actionId == 3 || multipleQuickReportform.actionId == 8 || multipleQuickReportform.actionId == 9 || multipleQuickReportform.actionId == 10 || multipleQuickReportform.actionId == 11 || multipleQuickReportform.actionId == 14 || multipleQuickReportform.actionId == 15 || multipleQuickReportform.actionId == 16"
    }],
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.categoryId),
      expression: "multipleQuickReportform.categoryId"
    }, {
      name: "show",
      rawName: "v-show",
      value: (!_vm.customCategory),
      expression: "!customCategory"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "category"
    },
    on: {
      "change": [function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.multipleQuickReportform, "categoryId", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }, _vm.changeCategoryHandler]
    }
  }, _vm._l((_vm.categories), function(category) {
    return _c('option', {
      domProps: {
        "value": category.id
      }
    }, [_vm._v("\n\t\t            \t" + _vm._s(category.name) + "\n\t\t            ")])
  }), 0), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 9),
      expression: "multipleQuickReportform.actionId == 9"
    }]
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.categoryName),
      expression: "multipleQuickReportform.categoryName"
    }, {
      name: "show",
      rawName: "v-show",
      value: (_vm.customCategory),
      expression: "customCategory"
    }],
    ref: "categoryName",
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "placeholder": "Enter category"
    },
    domProps: {
      "value": (_vm.multipleQuickReportform.categoryName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.multipleQuickReportform, "categoryName", $event.target.value)
      }
    }
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-sm btn-primary pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.customCategory = !_vm.customCategory;
        _vm.multipleQuickReportform.categoryId = null
      }
    }
  }, [_c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (!_vm.customCategory),
      expression: "!customCategory"
    }]
  }, [_vm._v("Use Custom Category")]), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.customCategory),
      expression: "customCategory"
    }]
  }, [_vm._v("Use Predefined Category")])])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 1),
      expression: "multipleQuickReportform.actionId == 1"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(3), _vm._v(" "), _c('input', {
    ref: "date2",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date2",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 1),
      expression: "multipleQuickReportform.categoryId == 1"
    }],
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tScheduled Hearing Date and Time:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_vm._l((_vm.multipleQuickReportform.dateTimeArray), function(dateTimeElement, index) {
    return _c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "col-md-11"
    }, [_c('div', {
      staticClass: "input-group date",
      staticStyle: {
        "margin-bottom": "10px"
      }
    }, [_vm._m(4, true), _vm._v(" "), _c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dateTimeElement.value),
        expression: "dateTimeElement.value"
      }],
      staticClass: "form-control datetimepickerarray",
      attrs: {
        "type": "text",
        "name": "dateTimeArray[]",
        "placeholder": "yyyy-mm-dd hh:mm:ss",
        "autocomplete": "off"
      },
      domProps: {
        "value": (dateTimeElement.value)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(dateTimeElement, "value", $event.target.value)
        }
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "col-md-1 text-center"
    }, [_c('button', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (index != 0),
        expression: "index != 0"
      }],
      staticClass: "btn btn-danger btn-sm",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.removeDateTimePickerArray(index)
        }
      }
    }, [_c('i', {
      staticClass: "fa fa-times"
    })])])])
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-sm pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.addDateTimePickerArray
    }
  }, [_c('i', {
    staticClass: "fa fa-plus"
  }), _vm._v(" Add\n                    ")])], 2)]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 6 || _vm.multipleQuickReportform.categoryId == 7 || _vm.multipleQuickReportform.categoryId == 10 || _vm.multipleQuickReportform.categoryId == 14 || _vm.multipleQuickReportform.categoryId == 15),
      expression: "multipleQuickReportform.categoryId == 6 || multipleQuickReportform.categoryId == 7 || multipleQuickReportform.categoryId == 10 || multipleQuickReportform.categoryId == 14 || multipleQuickReportform.categoryId == 15"
    }],
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tScheduled Appointment Date and Time:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(5), _vm._v(" "), _c('input', {
    ref: "date3",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date3",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 2),
      expression: "multipleQuickReportform.actionId == 2"
    }]
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 3),
      expression: "multipleQuickReportform.actionId == 3"
    }]
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 4),
      expression: "multipleQuickReportform.actionId == 4"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    staticClass: "chosen-select-for-docs-2",
    staticStyle: {
      "width": "350px"
    },
    attrs: {
      "id": 'chosen-' + _vm.index,
      "data-placeholder": "Select Docs",
      "multiple": "",
      "tabindex": "4"
    }
  }, _vm._l((_vm.servicedocs), function(servicedoc) {
    return _c('option', {
      domProps: {
        "value": servicedoc.title
      }
    }, [_vm._v("\n\t\t                \t" + _vm._s((servicedoc.title).trim()) + "\n\t\t                ")])
  }), 0)])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 6),
      expression: "multipleQuickReportform.actionId == 6"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(8), _vm._v(" "), _c('input', {
    ref: "date4",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date4",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tConsequence of non submission:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Additional cost maybe charged."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Additional cost maybe charged.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Additional cost maybe charged.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tAdditional cost maybe charged.\n\t\t\t    \t\t")])]), _vm._v(" "), _c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Application maybe forfeited and has to re-apply."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Application maybe forfeited and has to re-apply.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Application maybe forfeited and has to re-apply.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tApplication maybe forfeited and has to re-apply.\n\t\t\t    \t\t")])]), _vm._v(" "), _c('div', [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.multipleQuickReportform.extraDetails),
      expression: "multipleQuickReportform.extraDetails"
    }],
    attrs: {
      "type": "checkbox",
      "value": "Initial deposit maybe forfeited."
    },
    domProps: {
      "checked": Array.isArray(_vm.multipleQuickReportform.extraDetails) ? _vm._i(_vm.multipleQuickReportform.extraDetails, "Initial deposit maybe forfeited.") > -1 : (_vm.multipleQuickReportform.extraDetails)
    },
    on: {
      "change": function($event) {
        var $$a = _vm.multipleQuickReportform.extraDetails,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "Initial deposit maybe forfeited.",
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.concat([$$v])))
          } else {
            $$i > -1 && (_vm.$set(_vm.multipleQuickReportform, "extraDetails", $$a.slice(0, $$i).concat($$a.slice($$i + 1))))
          }
        } else {
          _vm.$set(_vm.multipleQuickReportform, "extraDetails", $$c)
        }
      }
    }
  }), _vm._v("\n\t\t\t    \t\t\tInitial deposit maybe forfeited.\n\t\t\t    \t\t")])])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 7),
      expression: "multipleQuickReportform.actionId == 7"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(10), _vm._v(" "), _c('input', {
    ref: "date5",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date5",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 8),
      expression: "multipleQuickReportform.actionId == 8"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(11), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-daterange input-group"
  }, [_c('input', {
    ref: "date6",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date6",
      "autocomplete": "off"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("to")]), _vm._v(" "), _c('input', {
    ref: "date7",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date7",
      "autocomplete": "off"
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tTarget Filling Date:\n\t\t\t    ")]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(12), _vm._v(" "), _c('input', {
    ref: "date8",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date8",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 9),
      expression: "multipleQuickReportform.actionId == 9"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(13), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-daterange input-group"
  }, [_c('input', {
    ref: "date9",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date9",
      "autocomplete": "off"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("to")]), _vm._v(" "), _c('input', {
    ref: "date10",
    staticClass: "input-sm form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date10",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 10),
      expression: "multipleQuickReportform.actionId == 10"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(14), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(15), _vm._v(" "), _c('input', {
    ref: "date11",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date11",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 11),
      expression: "multipleQuickReportform.actionId == 11"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(16), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(17), _vm._v(" "), _c('input', {
    ref: "date12",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date12",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 12),
      expression: "multipleQuickReportform.actionId == 12"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(18), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(19), _vm._v(" "), _c('input', {
    ref: "date13",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date13",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 13),
      expression: "multipleQuickReportform.actionId == 13"
    }]
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(20), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(21), _vm._v(" "), _c('input', {
    ref: "date14",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date14",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 14),
      expression: "multipleQuickReportform.actionId == 14"
    }]
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 128),
      expression: "multipleQuickReportform.categoryId == 128"
    }],
    staticClass: "form-group"
  }, [_vm._m(22), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    ref: "company_courier",
    staticClass: "form-control",
    attrs: {
      "id": "company_courier"
    }
  }, _vm._l((_vm.companyCouriers), function(companyCourier) {
    return _c('option', {
      domProps: {
        "value": companyCourier.address.contact_number
      }
    }, [_vm._v("\n\t\t\t        \t\t" + _vm._s(companyCourier.first_name + ' ' + companyCourier.last_name) + " \n\t\t\t        \t\t(" + _vm._s(companyCourier.address.contact_number) + ")\n\t\t\t        \t")])
  }), 0)])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId != 128),
      expression: "multipleQuickReportform.categoryId != 128"
    }],
    staticClass: "form-group"
  }, [_vm._m(23), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('input', {
    ref: "tracking_number",
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "tracking_number"
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(24), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(25), _vm._v(" "), _c('input', {
    ref: "date15",
    staticClass: "form-control datetimepicker",
    attrs: {
      "type": "text",
      "id": "date15",
      "autocomplete": "off"
    }
  })])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.actionId == 15),
      expression: "multipleQuickReportform.actionId == 15"
    }]
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.multipleQuickReportform.categoryId == 76 || _vm.multipleQuickReportform.categoryId == 77 || _vm.multipleQuickReportform.categoryId == 78 || _vm.multipleQuickReportform.categoryId == 79 || _vm.multipleQuickReportform.categoryId == 80 || _vm.multipleQuickReportform.categoryId == 81 || _vm.multipleQuickReportform.categoryId == 82 || _vm.multipleQuickReportform.categoryId == 83),
      expression: "multipleQuickReportform.categoryId == 76 || multipleQuickReportform.categoryId == 77 || multipleQuickReportform.categoryId == 78 || multipleQuickReportform.categoryId == 79 || multipleQuickReportform.categoryId == 80 || multipleQuickReportform.categoryId == 81 || multipleQuickReportform.categoryId == 82 || multipleQuickReportform.categoryId == 83"
    }],
    staticClass: "form-group"
  }, [_vm._m(26), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "input-group date"
  }, [_vm._m(27), _vm._v(" "), _c('input', {
    ref: "date16",
    staticClass: "form-control datepicker",
    attrs: {
      "type": "text",
      "id": "date16",
      "autocomplete": "off"
    }
  })])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Action: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Category: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDocuments: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tSubmission Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Time of Finishing: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tAffected Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate Range: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tExtended period for processing: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Pick Up: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Deliver: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tCompany Courier: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tTracking Number: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tDate and Time Delivered: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t\t       \tEstimated Releasing Date: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "input-group-addon"
  }, [_c('i', {
    staticClass: "fa fa-calendar"
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-eb2fda4a", module.exports)
  }
}

/***/ }),

/***/ 196:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('dialog-modal', __webpack_require__(5));

var data = window.Laravel.user;

var writeReport = new Vue({
    el: '#write-report',
    data: {
        clients: [],
        services: [],
        selected: [],
        reportForm: new Form({
            id: '',
            message: ''
        }, { baseURL: 'http://' + window.Laravel.cpanel_url }),

        quickReportClientsId: [],
        quickReportClientServicesId: [],
        quickReportTrackings: [],

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            continue: 0
        }]
    },
    methods: {
        addClient: function addClient() {
            var clids = $("#selService").attr("cl-ids");
            if (clids == "") {
                toastr.error("Please select client first!");
            } else {
                var href = "/visa/report/select-services/" + clids;
                window.location.href = href;
            }
        },
        showServices: function showServices(id, e) {
            if (e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#client-' + id).toggleClass('hide');
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },
        selectServices: function selectServices(item) {
            if (this.selected.indexOf(item) > -1) {
                indx = this.selected.indexOf(item);
                this.selected.splice(indx, 1);
            } else {
                this.selected.push(item);
            }
        },
        getQuickReportClientIdsArray: function getQuickReportClientIdsArray(clientServices) {
            var clientIds = [];
            clientServices.forEach(function (clientService) {
                if (clientIds.indexOf(clientService.client_id) == -1) clientIds.push(clientService.client_id);
            });

            return clientIds;
        },
        getQuickReportTrackingsArray: function getQuickReportTrackingsArray(clientServices) {
            var trackings = [];
            clientServices.forEach(function (clientService) {
                if (trackings.indexOf(clientService.tracking) == -1) trackings.push(clientService.tracking);
            });

            return trackings;
        },
        showReport: function showReport() {
            if (this.selected.length != 0) {
                var id = '';
                $.each(this.selected, function (key, value) {
                    id = value.id + ';' + id;
                });
                this.reportForm.id = id;
                // $('#reports').modal('toggle');

                this.quickReportClientsId = this.getQuickReportClientIdsArray(this.selected);
                this.quickReportClientServicesId = this.selected;
                this.quickReportTrackings = this.getQuickReportTrackingsArray(this.selected);
                this.$refs.multiplequickreportref.fetchDocs(this.selected);

                setTimeout(function () {
                    $('#add-quick-report-modal').modal('show');

                    // swal({
                    //     title: "Do you want to create a report?",
                    //     type: 'info',
                    //     showCancelButton: true,
                    //     confirmButtonColor: '#3085d6',
                    //     cancelButtonColor: '#d33',
                    //     confirmButtonText: 'YES'
                    // })
                    // .then(function(response) {
                    //     $('#add-quick-report-modal').modal('show');
                    // })
                    // .catch(function(response) {

                    // });

                    $('body').css("padding-right", "0px");
                }, 1000);
            } else toastr.error("Please select service first!");
        },
        saveReport: function saveReport() {
            // this.reportForm.submit('post', '/visa/report/save-report')
            // .then(result => {
            //     toastr.success('New Report Added.');
            //     $('#reports').modal('toggle');
            //     this.selected = [];
            //     this.reportForm.id = '';
            //     this.reportForm.message = '';
            //     $('input:checkbox').prop('checked',false);
            // });

        }
    },
    created: function created() {
        var _this = this;

        axios.get('/visa/report/get-clients/' + window.Laravel.data).then(function (response) {
            _this.clients = response.data;
        });

        var x = this.permissions.filter(function (obj) {
            return obj.type === 'Reports';
        });

        var y = x.filter(function (obj) {
            return obj.name === 'Continue';
        });

        if (y.length == 0) this.selpermissions.continue = 0;else this.selpermissions.continue = 1;
    },

    components: {
        'multiple-quick-report': __webpack_require__(14)
    }
});

$(document).ready(function () {

    $('#lists').DataTable({
        "ajax": "/storage/data/ClientsReport.txt",
        responsive: true,
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "iDisplayLength": 10
    });
});

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 20:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(12);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("01398020", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-0f2f2b77\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MultipleQuickReport.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-0f2f2b77\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MultipleQuickReport.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(7)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 486:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(196);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNDJjZTBjNmM2ZTJiM2UyMjc4MzU/YzkyZCoiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKiIsIndlYnBhY2s6Ly8vTXVsdGlwbGVRdWlja1JlcG9ydC52dWU/NDQ4MyoiLCJ3ZWJwYWNrOi8vL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZT80YzljKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWU/NDcwNCoqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZT9kNTY5KiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlPzJiM2MqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZT85Yzc4KioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZT9jMzdkKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9yZXBvcnRzL2luZGV4LmpzIiwid2VicGFjazovLy8uL34vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanM/ZGEwNCoiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnQudnVlP2I4NTMqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qcz82YjJiKiIsIndlYnBhY2s6Ly8vRGlhbG9nTW9kYWwudnVlP2U0NzQiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlP2ZlMWIiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtc3R5bGUtbG9hZGVyL2xpYi9saXN0VG9TdHlsZXMuanM/ZTZhYyoiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImRhdGEiLCJ3aW5kb3ciLCJMYXJhdmVsIiwidXNlciIsIndyaXRlUmVwb3J0IiwiZWwiLCJjbGllbnRzIiwic2VydmljZXMiLCJzZWxlY3RlZCIsInJlcG9ydEZvcm0iLCJGb3JtIiwiaWQiLCJtZXNzYWdlIiwiYmFzZVVSTCIsImNwYW5lbF91cmwiLCJxdWlja1JlcG9ydENsaWVudHNJZCIsInF1aWNrUmVwb3J0Q2xpZW50U2VydmljZXNJZCIsInF1aWNrUmVwb3J0VHJhY2tpbmdzIiwic2V0dGluZyIsImFjY2Vzc19jb250cm9sIiwicGVybWlzc2lvbnMiLCJzZWxwZXJtaXNzaW9ucyIsImNvbnRpbnVlIiwibWV0aG9kcyIsImFkZENsaWVudCIsImNsaWRzIiwiJCIsImF0dHIiLCJ0b2FzdHIiLCJlcnJvciIsImhyZWYiLCJsb2NhdGlvbiIsInNob3dTZXJ2aWNlcyIsImUiLCJ0YXJnZXQiLCJub2RlTmFtZSIsInRvZ2dsZUNsYXNzIiwic2VsZWN0U2VydmljZXMiLCJpdGVtIiwiaW5kZXhPZiIsImluZHgiLCJzcGxpY2UiLCJwdXNoIiwiZ2V0UXVpY2tSZXBvcnRDbGllbnRJZHNBcnJheSIsImNsaWVudFNlcnZpY2VzIiwiY2xpZW50SWRzIiwiZm9yRWFjaCIsImNsaWVudFNlcnZpY2UiLCJjbGllbnRfaWQiLCJnZXRRdWlja1JlcG9ydFRyYWNraW5nc0FycmF5IiwidHJhY2tpbmdzIiwidHJhY2tpbmciLCJzaG93UmVwb3J0IiwibGVuZ3RoIiwiZWFjaCIsImtleSIsInZhbHVlIiwiJHJlZnMiLCJtdWx0aXBsZXF1aWNrcmVwb3J0cmVmIiwiZmV0Y2hEb2NzIiwic2V0VGltZW91dCIsIm1vZGFsIiwiY3NzIiwic2F2ZVJlcG9ydCIsImNyZWF0ZWQiLCJheGlvcyIsImdldCIsInRoZW4iLCJyZXNwb25zZSIsIngiLCJmaWx0ZXIiLCJvYmoiLCJ0eXBlIiwieSIsIm5hbWUiLCJjb21wb25lbnRzIiwiZG9jdW1lbnQiLCJyZWFkeSIsIkRhdGFUYWJsZSIsInJlc3BvbnNpdmUiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyQkE7O0FBRUEsbUdBRkE7O0FBSUEsS0FKQSxrQkFJQTtBQUNBOztBQUVBLFdBRkE7QUFHQSxrQkFIQTs7QUFLQTtBQUNBLDhCQURBOztBQUdBLHFCQUhBO0FBSUEsaUJBSkE7O0FBTUEsaUJBTkE7QUFPQSx3QkFQQTtBQVFBLGlCQVJBOztBQVVBLGdCQVZBOztBQVlBLGtCQVpBO0FBYUEsb0JBYkE7O0FBZUEsYUFmQTtBQWdCQSxhQWhCQTtBQWlCQSxhQWpCQTtBQWtCQSxxQkFsQkE7O0FBb0JBO0FBcEJBLE1BcUJBLHdDQXJCQTs7QUFMQTtBQTZCQSxFQWxDQTs7O0FBb0NBO0FBRUEsV0FGQSxxQkFFQSxFQUZBLEVBRUE7QUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUZBOztBQUlBO0FBQ0E7QUFDQSxJQUZBLEVBRUEsSUFGQSxDQUVBLEdBRkE7O0FBSUEsNERBQ0EsSUFEQSxDQUNBO0FBQ0E7QUFDQSxJQUhBOztBQUtBO0FBQ0E7QUFDQSxJQUZBLEVBRUEsSUFGQTtBQUdBLEdBcEJBO0FBc0JBLGdCQXRCQSw0QkFzQkE7QUFBQTs7QUFDQSwrQ0FDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEE7QUFJQSxHQTNCQTtBQTZCQSw4QkE3QkEsMENBNkJBO0FBQUE7O0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0JBREE7O0FBR0EscUJBSEE7QUFJQSx1QkFKQTs7QUFNQSxnQkFOQTtBQU9BLGdCQVBBO0FBUUEsZ0JBUkE7QUFTQSxzQkFUQTs7QUFXQTtBQVhBOztBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsSUFuQ0E7O0FBcUNBO0FBQ0E7QUFDQSw4QkFEQTs7QUFHQSxnQkFIQTs7QUFLQSxrQkFMQTtBQU1BLG9CQU5BOztBQVFBLGFBUkE7QUFTQSxhQVRBO0FBVUEsYUFWQTtBQVdBLHFCQVhBOztBQWFBO0FBYkEsTUFjQSx3Q0FkQTtBQWVBLEdBcEZBO0FBc0ZBLFVBdEZBLHNCQXNGQTtBQUFBOztBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsSUFOQTs7QUFRQTtBQUNBLEdBbEdBO0FBb0dBLHdCQXBHQSxvQ0FvR0E7QUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBcEJBOztBQXNCQSxzRUFDQSxJQURBLENBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFFBRkE7QUFHQSxPQUpBLE1BSUE7QUFDQTtBQUNBO0FBQ0EsUUFGQSxFQUVBLElBRkE7QUFHQTtBQUNBO0FBQ0EsS0FuQkE7QUFvQkE7QUFDQTtBQXhKQSxFQXBDQTs7QUFnTUEsUUFoTUEscUJBZ01BO0FBQ0E7QUFDQSxFQWxNQTs7O0FBb01BO0FBQ0Esb0JBREEsZ0NBQ0E7QUFDQTtBQUNBLEdBSEE7QUFLQSx3QkFMQSxvQ0FLQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFKQTs7QUFNQTtBQUNBO0FBZkEsRUFwTUE7O0FBc05BO0FBQ0E7QUFEQTs7QUF0TkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdVZBOztBQUVBLHdDQUZBOztBQUlBLEtBSkEsa0JBSUE7QUFDQTs7QUFFQSxjQUZBO0FBR0EsaUJBSEE7O0FBS0Esc0JBTEE7O0FBT0Esd0JBUEE7O0FBU0E7QUFDQSxlQURBOztBQUdBLG9CQUhBO0FBSUEsc0JBSkE7O0FBTUEsZUFOQTtBQU9BLGVBUEE7QUFRQSxlQVJBO0FBU0EscUJBVEE7O0FBV0E7QUFYQTs7QUFUQTtBQXdCQSxFQTdCQTs7O0FBK0JBO0FBRUEsWUFGQSx3QkFFQTtBQUFBOztBQUNBLDhCQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0EsSUFIQSxFQUlBLEtBSkEsQ0FJQTtBQUFBO0FBQUEsSUFKQTtBQUtBLEdBUkE7QUFVQSxvQkFWQSxnQ0FVQTtBQUFBOztBQUNBOztBQUVBLDRDQUNBLElBREEsQ0FDQTtBQUNBO0FBQ0EsSUFIQSxFQUlBLEtBSkEsQ0FJQTtBQUFBO0FBQUEsSUFKQTtBQUtBLEdBbEJBO0FBb0JBLGdCQXBCQSw0QkFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsR0FuQ0E7QUFxQ0EscUJBckNBLCtCQXFDQSxRQXJDQSxFQXFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBakRBO0FBbURBLGVBbkRBLDJCQW1EQTtBQUFBOztBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBREEsTUFLQSxJQUxBLENBS0E7QUFDQTtBQUNBLElBUEEsRUFRQSxLQVJBLENBUUE7QUFBQTtBQUFBLElBUkE7QUFTQSxHQW5FQTtBQXFFQSx1QkFyRUEsbUNBcUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0F6RUE7QUEyRUEsd0JBM0VBLG9DQTJFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx3QkFEQTtBQUVBLHVDQUZBO0FBR0Esc0JBSEE7QUFJQSxtQkFKQTtBQUtBO0FBTEE7QUFPQSxJQVJBLEVBUUEsR0FSQTtBQVNBLEdBdkZBO0FBeUZBLDJCQXpGQSxxQ0F5RkEsS0F6RkEsRUF5RkE7QUFDQTtBQUNBLEdBM0ZBO0FBNkZBLFVBN0ZBLG9CQTZGQSxxQkE3RkEsRUE2RkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQSxNQUZBOztBQUlBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQXZCQSxNQXVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFMQSxNQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FGQTs7QUFJQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsSUFkQSxNQWNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLElBVEEsTUFTQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxJQVRBLE1BU0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFoQkEsTUFnQkE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBMUJBLE1BMEJBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxJQVpBLE1BWUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLElBWkEsTUFZQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxJQVRBLE1BU0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsSUFUQSxNQVNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxLQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBdkJBLE1BdUJBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FIQSxNQUdBO0FBQ0E7QUFDQTtBQUNBLEtBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQSxJQVpBLE1BWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUEzUkEsRUEvQkE7O0FBOFRBLFFBOVRBLHFCQThUQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsRUFwVUE7QUFzVUEsUUF0VUEscUJBc1VBOztBQUVBO0FBQ0E7QUFDQSxnQkFEQTtBQUVBLHdDQUZBO0FBR0E7QUFIQTs7QUFNQTtBQUNBO0FBQ0EscUJBREE7QUFFQSw0QkFGQTtBQUdBLG9CQUhBO0FBSUEsc0JBSkE7QUFLQSxrQkFMQTtBQU1BLHVCQU5BO0FBT0E7QUFQQTs7QUFVQTtBQUNBO0FBQ0Esc0JBREE7QUFFQSxxQ0FGQTtBQUdBLG9CQUhBO0FBSUEsaUJBSkE7QUFLQTtBQUxBO0FBUUE7QUFuV0EsRzs7Ozs7OztBQ3BYQSwyQkFBMkIsbUJBQU8sQ0FBQyxDQUEyRDtBQUM5RixjQUFjLFFBQVMsNEJBQTRCLHdCQUF3QixHQUFHLDRCQUE0Qix1QkFBdUIsR0FBRyxVQUFVLG1GQUFtRixNQUFNLFdBQVcsS0FBSyxLQUFLLFdBQVcsMFlBQTBZLGlDQUFpQyxtckJBQW1yQiwySEFBMkgsZ0JBQWdCLHVHQUF1RyxtZEFBbWQsR0FBRyx1Q0FBdUMsWUFBWSxPQUFPLG1CQUFtQix5QkFBeUIsdUJBQXVCLHdDQUF3Qyx5RUFBeUUsV0FBVyxFQUFFLHdFQUF3RSw0Q0FBNEMsV0FBVyxZQUFZLDJHQUEyRywwQ0FBMEMsZUFBZSxFQUFFLGtDQUFrQyx1RUFBdUUsaUJBQWlCLFFBQVEsU0FBUyw2QkFBNkIsdUZBQXVGLGlEQUFpRCxlQUFlLEVBQUUsYUFBYSwyQ0FBMkMsMkhBQTJILHdDQUF3Qyw4REFBOEQsK1NBQStTLDhEQUE4RCw0REFBNEQsNERBQTRELDREQUE0RCw0REFBNEQsNERBQTRELDREQUE0RCw0REFBNEQsNkRBQTZELDZEQUE2RCw2REFBNkQsNkRBQTZELDZEQUE2RCw2REFBNkQsNkRBQTZELG1FQUFtRSw4REFBOEQsYUFBYSxFQUFFLG1HQUFtRyxrVUFBa1UsR0FBRyx1Q0FBdUMsRUFBRSxTQUFTLHVCQUF1Qiw2QkFBNkIsbUZBQW1GLHNDQUFzQywwRUFBMEUsOEJBQThCLGFBQWEsV0FBVyxFQUFFLDJCQUEyQixTQUFTLHFDQUFxQywrQkFBK0Isd0VBQXdFLGdFQUFnRSxvRUFBb0UsZ0ZBQWdGLG9FQUFvRSw4RkFBOEYscUZBQXFGLHdDQUF3QyxvRkFBb0Ysc0ZBQXNGLDBGQUEwRiw0RUFBNEUsNEVBQTRFLDRFQUE0RSw0RkFBNEYsMEZBQTBGLHNFQUFzRSx3RUFBd0UsZ0ZBQWdGLGtFQUFrRSxrRUFBa0Usa0VBQWtFLGtGQUFrRixnRkFBZ0YsYUFBYSxFQUFFLHNIQUFzSCw0Q0FBNEMsNERBQTRELDJGQUEyRiwyREFBMkQsc0RBQXNELDJGQUEyRiwyRUFBMkUscUJBQXFCLEVBQUUseUJBQXlCLE9BQU8sNENBQTRDLDhDQUE4QywyQkFBMkIsUUFBUSx5QkFBeUIsdUJBQXVCLHFCQUFxQixFQUFFLFdBQVcsU0FBUyxTQUFTLG9CQUFvQiw4QkFBOEIsT0FBTyxvQkFBb0IsOEJBQThCLHFEQUFxRCxXQUFXLHFDQUFxQywwQ0FBMEMsMERBQTBELDRFQUE0RSxnRUFBZ0UsYUFBYSxXQUFXLEVBQUUsMENBQTBDLFNBQVMsT0FBTyxzQkFBc0IsMkZBQTJGLE9BQU8seUNBQXlDLDBCQUEwQixLQUFLLGFBQWEseUJBQXlCLEtBQUssYUFBYSxHOzs7Ozs7OztBQ0FseFI7QUFDQSxtQkFBTyxDQUFDLEVBQXVSOztBQUUvUixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUFxRTtBQUM3RjtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxFQUFpUDtBQUMzUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxFQUE2TTtBQUN2TjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMvQkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBcVA7QUFDL1A7QUFDQSxFQUFFLG1CQUFPLENBQUMsRUFBaU47QUFDM047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDL0NBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxPQUFPO0FBQ1A7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0Esd0NBQXdDLFFBQVE7QUFDaEQ7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLGFBQWEsYUFBYSwwQkFBMEI7QUFDckQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMzMkJBQSxJQUFJQyxTQUFKLENBQWMsY0FBZCxFQUErQkMsbUJBQU9BLENBQUMsQ0FBUixDQUEvQjs7QUFFQSxJQUFJQyxPQUFPQyxPQUFPQyxPQUFQLENBQWVDLElBQTFCOztBQUVBLElBQUlDLGNBQWMsSUFBSVAsR0FBSixDQUFRO0FBQ3pCUSxRQUFHLGVBRHNCO0FBRXpCTCxVQUFLO0FBQ0pNLGlCQUFRLEVBREo7QUFFRUMsa0JBQVMsRUFGWDtBQUdFQyxrQkFBUyxFQUhYO0FBSUVDLG9CQUFXLElBQUlDLElBQUosQ0FBUztBQUNsQkMsZ0JBQUksRUFEYztBQUVsQkMscUJBQVM7QUFGUyxTQUFULEVBR1IsRUFBRUMsU0FBUyxZQUFVWixPQUFPQyxPQUFQLENBQWVZLFVBQXBDLEVBSFEsQ0FKYjs7QUFTRUMsOEJBQXNCLEVBVHhCO0FBVUVDLHFDQUE2QixFQVYvQjtBQVdFQyw4QkFBc0IsRUFYeEI7O0FBYUVDLGlCQUFTbEIsS0FBS21CLGNBQUwsQ0FBb0IsQ0FBcEIsRUFBdUJELE9BYmxDO0FBY0VFLHFCQUFhcEIsS0FBS29CLFdBZHBCO0FBZUVDLHdCQUFnQixDQUFDO0FBQ2JDLHNCQUFTO0FBREksU0FBRDtBQWZsQixLQUZvQjtBQXFCekJDLGFBQVM7QUFDUkMsaUJBRFEsdUJBQ0c7QUFDWCxnQkFBSUMsUUFBUUMsRUFBRSxhQUFGLEVBQWlCQyxJQUFqQixDQUFzQixRQUF0QixDQUFaO0FBQ00sZ0JBQUdGLFNBQU8sRUFBVixFQUFhO0FBQ1hHLHVCQUFPQyxLQUFQLENBQWEsNkJBQWI7QUFDRCxhQUZELE1BR0k7QUFDTCxvQkFBSUMsT0FBTyxrQ0FBZ0NMLEtBQTNDO0FBQ0F4Qix1QkFBTzhCLFFBQVAsQ0FBZ0JELElBQWhCLEdBQXVCQSxJQUF2QjtBQUNBO0FBQ0osU0FWTztBQVdGRSxvQkFYRSx3QkFXV3JCLEVBWFgsRUFXZXNCLENBWGYsRUFXa0I7QUFDaEIsZ0JBQUdBLEVBQUVDLE1BQUYsQ0FBU0MsUUFBVCxJQUFxQixRQUFyQixJQUFpQ0YsRUFBRUMsTUFBRixDQUFTQyxRQUFULElBQXFCLE1BQXpELEVBQWlFO0FBQzdEVCxrQkFBRSxhQUFhZixFQUFmLEVBQW1CeUIsV0FBbkIsQ0FBK0IsTUFBL0I7QUFDQVYsa0JBQUUsU0FBU2YsRUFBWCxFQUFleUIsV0FBZixDQUEyQixlQUEzQjtBQUNIO0FBQ0osU0FoQkM7QUFpQkZDLHNCQWpCRSwwQkFpQmFDLElBakJiLEVBaUJrQjtBQUNoQixnQkFBRyxLQUFLOUIsUUFBTCxDQUFjK0IsT0FBZCxDQUFzQkQsSUFBdEIsSUFBNEIsQ0FBQyxDQUFoQyxFQUNBO0FBQ0lFLHVCQUFPLEtBQUtoQyxRQUFMLENBQWMrQixPQUFkLENBQXNCRCxJQUF0QixDQUFQO0FBQ0EscUJBQUs5QixRQUFMLENBQWNpQyxNQUFkLENBQXFCRCxJQUFyQixFQUEyQixDQUEzQjtBQUVILGFBTEQsTUFNQTtBQUNJLHFCQUFLaEMsUUFBTCxDQUFja0MsSUFBZCxDQUFtQkosSUFBbkI7QUFDSDtBQUNKLFNBM0JDO0FBNEJGSyxvQ0E1QkUsd0NBNEIyQkMsY0E1QjNCLEVBNEIyQztBQUN6QyxnQkFBSUMsWUFBWSxFQUFoQjtBQUNBRCwyQkFBZUUsT0FBZixDQUF1Qix5QkFBaUI7QUFDcEMsb0JBQUdELFVBQVVOLE9BQVYsQ0FBa0JRLGNBQWNDLFNBQWhDLEtBQThDLENBQUMsQ0FBbEQsRUFDSUgsVUFBVUgsSUFBVixDQUFlSyxjQUFjQyxTQUE3QjtBQUNQLGFBSEQ7O0FBS0EsbUJBQU9ILFNBQVA7QUFDSCxTQXBDQztBQXFDRkksb0NBckNFLHdDQXFDMkJMLGNBckMzQixFQXFDMkM7QUFDekMsZ0JBQUlNLFlBQVksRUFBaEI7QUFDQU4sMkJBQWVFLE9BQWYsQ0FBdUIseUJBQWlCO0FBQ3BDLG9CQUFHSSxVQUFVWCxPQUFWLENBQWtCUSxjQUFjSSxRQUFoQyxLQUE2QyxDQUFDLENBQWpELEVBQ0lELFVBQVVSLElBQVYsQ0FBZUssY0FBY0ksUUFBN0I7QUFDUCxhQUhEOztBQUtBLG1CQUFPRCxTQUFQO0FBQ0gsU0E3Q0M7QUE4Q0ZFLGtCQTlDRSx3QkE4Q1U7QUFDUixnQkFBRyxLQUFLNUMsUUFBTCxDQUFjNkMsTUFBZCxJQUFzQixDQUF6QixFQUEyQjtBQUN2QixvQkFBSTFDLEtBQUssRUFBVDtBQUNBZSxrQkFBRTRCLElBQUYsQ0FBUSxLQUFLOUMsUUFBYixFQUF1QixVQUFVK0MsR0FBVixFQUFlQyxLQUFmLEVBQXVCO0FBQzVDN0MseUJBQUs2QyxNQUFNN0MsRUFBTixHQUFXLEdBQVgsR0FBaUJBLEVBQXRCO0FBQ0QsaUJBRkQ7QUFHQSxxQkFBS0YsVUFBTCxDQUFnQkUsRUFBaEIsR0FBbUJBLEVBQW5CO0FBQ0E7O0FBRUEscUJBQUtJLG9CQUFMLEdBQTRCLEtBQUs0Qiw0QkFBTCxDQUFrQyxLQUFLbkMsUUFBdkMsQ0FBNUI7QUFDQSxxQkFBS1EsMkJBQUwsR0FBbUMsS0FBS1IsUUFBeEM7QUFDQSxxQkFBS1Msb0JBQUwsR0FBNEIsS0FBS2dDLDRCQUFMLENBQWtDLEtBQUt6QyxRQUF2QyxDQUE1QjtBQUNBLHFCQUFLaUQsS0FBTCxDQUFXQyxzQkFBWCxDQUFrQ0MsU0FBbEMsQ0FBNEMsS0FBS25ELFFBQWpEOztBQUVBb0QsMkJBQVcsWUFBTTtBQUNibEMsc0JBQUUseUJBQUYsRUFBNkJtQyxLQUE3QixDQUFtQyxNQUFuQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUFuQyxzQkFBRSxNQUFGLEVBQVVvQyxHQUFWLENBQWMsZUFBZCxFQUErQixLQUEvQjtBQUNILGlCQW5CRCxFQW1CRyxJQW5CSDtBQW9CSCxhQWpDRCxNQW1DSWxDLE9BQU9DLEtBQVAsQ0FBYSw4QkFBYjtBQUNQLFNBbkZDO0FBb0ZGa0Msa0JBcEZFLHdCQW9GVTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSDtBQS9GQyxLQXJCZ0I7QUF1SHpCQyxXQXZIeUIscUJBdUhoQjtBQUFBOztBQUNMQyxjQUFNQyxHQUFOLENBQVUsOEJBQTRCakUsT0FBT0MsT0FBUCxDQUFlRixJQUFyRCxFQUNDbUUsSUFERCxDQUNNLG9CQUFZO0FBQ2Qsa0JBQUs3RCxPQUFMLEdBQWU4RCxTQUFTcEUsSUFBeEI7QUFDSCxTQUhEOztBQUtHLFlBQUlxRSxJQUFJLEtBQUtqRCxXQUFMLENBQWlCa0QsTUFBakIsQ0FBd0IsZUFBTztBQUFFLG1CQUFPQyxJQUFJQyxJQUFKLEtBQWEsU0FBcEI7QUFBOEIsU0FBL0QsQ0FBUjs7QUFFQSxZQUFJQyxJQUFJSixFQUFFQyxNQUFGLENBQVMsZUFBTztBQUFFLG1CQUFPQyxJQUFJRyxJQUFKLEtBQWEsVUFBcEI7QUFBK0IsU0FBakQsQ0FBUjs7QUFFQSxZQUFHRCxFQUFFcEIsTUFBRixJQUFVLENBQWIsRUFBZ0IsS0FBS2hDLGNBQUwsQ0FBb0JDLFFBQXBCLEdBQStCLENBQS9CLENBQWhCLEtBQ0ssS0FBS0QsY0FBTCxDQUFvQkMsUUFBcEIsR0FBK0IsQ0FBL0I7QUFDWCxLQW5Jd0I7O0FBb0l0QnFELGdCQUFZO0FBQ1IsaUNBQXlCNUUsbUJBQU9BLENBQUMsRUFBUjtBQURqQjtBQXBJVSxDQUFSLENBQWxCOztBQXlJQTJCLEVBQUVrRCxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVzs7QUFFekJuRCxNQUFFLFFBQUYsRUFBWW9ELFNBQVosQ0FBc0I7QUFDckIsZ0JBQVEsaUNBRGE7QUFFbEJDLG9CQUFZLElBRk07QUFHbEIsc0JBQWMsQ0FBQyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxDQUFELEVBQWUsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsQ0FBZixDQUhJO0FBSWxCLDBCQUFrQjtBQUpBLEtBQXRCO0FBT0gsQ0FURCxFOzs7Ozs7O0FDN0lBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixpQkFBaUI7QUFDakM7QUFDQTtBQUNBLHdDQUF3QyxnQkFBZ0I7QUFDeEQsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixpQkFBaUI7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9CQUFvQjtBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDakRBOztBQUVBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLEVBQW9UO0FBQzFVLDRDQUE0QyxRQUFTO0FBQ3JEO0FBQ0E7QUFDQSxhQUFhLG1CQUFPLENBQUMsQ0FBeUU7QUFDOUY7QUFDQSxHQUFHLEtBQVU7QUFDYjtBQUNBO0FBQ0EsNEpBQTRKLG9FQUFvRTtBQUNoTyxxS0FBcUssb0VBQW9FO0FBQ3pPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7OztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSxpQkFBaUI7QUFDM0I7QUFDQTs7QUFFQSxtQkFBbUIsbUJBQU8sQ0FBQyxDQUFnQjs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsbUJBQW1CLHNCQUFzQjtBQUN6QztBQUNBO0FBQ0EsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsMkJBQTJCO0FBQ2hEO0FBQ0E7QUFDQSxZQUFZLHVCQUF1QjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EscUJBQXFCLHVCQUF1QjtBQUM1QztBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0xBO0FBQ0E7QUFDQSxnQ0FEQTtBQUVBO0FBRkE7QUFEQSxHOzs7Ozs7Ozs7Ozs7Ozs7QUN2QkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBeU87QUFDblA7QUFDQSxFQUFFLG1CQUFPLENBQUMsQ0FBcU07QUFDL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixpQkFBaUI7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLHdCQUF3QjtBQUMzRCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJqcy92aXNhL3JlcG9ydHMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ4Nik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgNDJjZTBjNmM2ZTJiM2UyMjc4MzUiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSIsIjx0ZW1wbGF0ZT5cblx0XG5cdDxmb3JtIGNsYXNzPVwiZm9ybS1ob3Jpem9udGFsXCIgQHN1Ym1pdC5wcmV2ZW50PVwiYWRkTXVsdGlwbGVRdWlja1JlcG9ydFwiPlxuXG5cdFx0PGRpdiBjbGFzcz1cImlvcy1zY3JvbGxcIj5cblx0XHRcdDxkaXYgdi1mb3I9XCIoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgaW4gZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5sZW5ndGhcIj5cblxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwicGFuZWwgcGFuZWwtZGVmYXVsdFwiPlxuXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWhlYWRpbmdcIj5cblx0ICAgICAgICAgICAgICAgICAgICB7eyBkaXN0aW5jdENsaWVudFNlcnZpY2VzW2luZGV4XSB9fVxuXHQgICAgICAgICAgICAgICAgPC9kaXY+XG5cdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGFuZWwtYm9keVwiPlxuXHQgICAgICAgICAgICAgICAgICAgIFxuXHQgICAgICAgICAgICAgICAgXHQ8bXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0gOmtleT1cImluZGV4XCIgOnJlZj1cIidmb3JtLScgKyBpbmRleFwiIDpkb2NzPVwiZG9jc1wiIDpzZXJ2aWNlZG9jcz1cInNlcnZpY2VEb2NzXCIgOmluZGV4PVwiaW5kZXhcIj48L211bHRpcGxlLXF1aWNrLXJlcG9ydC1mb3JtPlxuXG5cdCAgICAgICAgICAgICAgICA8L2Rpdj4gPCEtLSBwYW5lbC1ib2R5IC0tPlxuXHQgICAgICAgICAgICA8L2Rpdj4gPCEtLSBwYW5lbCAtLT5cblxuXHRcdFx0PC9kaXY+IDwhLS0gbG9vcCAtLT5cblx0XHQ8L2Rpdj4gPCEtLSBpb3Mtc2Nyb2xsIC0tPlxuXHRcdFxuXHRcdDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIj5DcmVhdGUgUmVwb3J0PC9idXR0b24+XG5cdCAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cblx0PC9mb3JtPlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG5cdGV4cG9ydCBkZWZhdWx0IHtcblxuXHRcdHByb3BzOiBbJ2dyb3VwcmVwb3J0JywgJ2dyb3VwaWQnLCAnY2xpZW50c2lkJywgJ2NsaWVudHNlcnZpY2VzJywgJ3RyYWNraW5ncycsICdpc3dyaXRlcmVwb3J0cGFnZSddLFxuXG5cdFx0ZGF0YSgpIHtcblx0XHRcdHJldHVybiB7XG5cdFx0XG5cdFx0XHRcdGRvY3M6IFtdLFxuXHRcdFx0XHRzZXJ2aWNlRG9jczogW10sXG5cbiAgICAgICAgXHRcdG11bHRpcGxlUXVpY2tSZXBvcnRGb3JtIDogbmV3IEZvcm0oe1xuICAgICAgICBcdFx0XHRkaXN0aW5jdENsaWVudFNlcnZpY2VzOltdLFxuXG4gICAgICAgIFx0XHRcdGdyb3VwUmVwb3J0OiBudWxsLFxuICAgICAgICBcdFx0XHRncm91cElkOiBudWxsLFxuXG4gICAgICAgIFx0XHRcdGNsaWVudHNJZDogW10sXG4gICAgICAgIFx0XHRcdGNsaWVudFNlcnZpY2VzSWQ6IFtdLFxuICAgICAgICBcdFx0XHR0cmFja2luZ3M6IFtdLFxuXG4gICAgICAgIFx0XHRcdGFjdGlvbklkOiBbXSxcblxuICAgICAgICBcdFx0XHRjYXRlZ29yeUlkOiBbXSxcbiAgICAgICAgXHRcdFx0Y2F0ZWdvcnlOYW1lOiBbXSxcblxuICAgICAgICBcdFx0XHRkYXRlMTogW10sXG4gICAgICAgIFx0XHRcdGRhdGUyOiBbXSxcbiAgICAgICAgXHRcdFx0ZGF0ZTM6IFtdLFxuICAgICAgICBcdFx0XHRkYXRlVGltZUFycmF5OiBbXSxcblxuICAgICAgICBcdFx0XHRleHRyYURldGFpbHM6IFtdXG4gICAgICAgIFx0XHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxuXG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdG1ldGhvZHM6IHtcblxuXHRcdFx0ZmV0Y2hEb2NzKGlkKSB7XG5cdFx0XHRcdHZhciBpZHMgPSAnJztcblx0XHRcdFx0JC5lYWNoKGlkLCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cdFx0XHRcdCAgICBpZHMgKz0gaWRbaV0uZG9jc19uZWVkZWQgKyAnLCcgKyBpZFtpXS5kb2NzX29wdGlvbmFsICsgJywnO1xuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHR2YXIgdW5pcXVlTGlzdD1pZHMuc3BsaXQoJywnKS5maWx0ZXIoZnVuY3Rpb24oYWxsSXRlbXMsaSxhKXtcblx0XHRcdFx0ICAgIHJldHVybiBpPT1hLmluZGV4T2YoYWxsSXRlbXMpO1xuXHRcdFx0XHR9KS5qb2luKCcsJyk7XG5cdCAgICAgICAgXHRcblx0XHRcdCBcdGF4aW9zLmdldCgnL3Zpc2EvZ3JvdXAvJyt1bmlxdWVMaXN0Kycvc2VydmljZS1kb2NzJylcblx0XHRcdCBcdCAgLnRoZW4ocmVzdWx0ID0+IHtcblx0XHQgICAgICAgICAgICB0aGlzLmRvY3MgPSByZXN1bHQuZGF0YTtcblx0XHQgICAgICAgIH0pO1xuXG5cdCAgICAgICAgXHRzZXRUaW1lb3V0KCgpID0+IHtcblx0ICAgICAgICBcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLWRvY3MnKS50cmlnZ2VyKCdjaG9zZW46dXBkYXRlZCcpO1x0XG5cdCAgICAgICAgXHR9LCA1MDAwKTtcblx0XHRcdH0sXG5cblx0XHRcdGdldFNlcnZpY2VEb2NzKCkge1xuICAgICAgICBcdFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC9zZXJ2aWNlLWRvY3MtaW5kZXgnKVxuXHRcdFx0IFx0ICAudGhlbihyZXN1bHQgPT4ge1xuXHRcdCAgICAgICAgICAgIHRoaXMuc2VydmljZURvY3MgPSByZXN1bHQuZGF0YTtcblx0XHQgICAgICAgIH0pO1xuICAgICAgICBcdH0sXG5cblx0XHRcdHJlc2V0TXVsdGlwbGVRdWlja1JlcG9ydEZvcm0oKSB7XG5cdFx0XHRcdC8vIE11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZVxuXHRcdFx0XHRcdHRoaXMuZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5mb3JFYWNoKChkaXN0aW5jdENsaWVudFNlcnZpY2UsIGluZGV4KSA9PiB7XG5cdFx0XHRcdFx0XHRsZXQgcmVmID0gJ2Zvcm0tJyArIGluZGV4O1xuXG5cdFx0XHRcdFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0gPSB7XG5cdFx0ICAgICAgICBcdFx0XHRhY3Rpb25JZDogMSxcblxuXHRcdCAgICAgICAgXHRcdFx0Y2F0ZWdvcnlJZDogbnVsbCxcblx0XHQgICAgICAgIFx0XHRcdGNhdGVnb3J5TmFtZTogbnVsbCxcblxuXHRcdCAgICAgICAgXHRcdFx0ZGF0ZTE6IG51bGwsXG5cdFx0ICAgICAgICBcdFx0XHRkYXRlMjogbnVsbCxcblx0XHQgICAgICAgIFx0XHRcdGRhdGUzOiBudWxsLFxuXHRcdCAgICAgICAgXHRcdFx0ZGF0ZVRpbWVBcnJheTogW10sXG5cblx0XHQgICAgICAgIFx0XHRcdGV4dHJhRGV0YWlsczogW11cblx0XHQgICAgICAgIFx0XHR9XG5cblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTIudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTMudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTQudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTUudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTYudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTcudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTgudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTkudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTEwLnZhbHVlID0gJyc7XG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxMS52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTIudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTEzLnZhbHVlID0gJyc7XG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUxNC52YWx1ZSA9ICcnO1xuXHRcdCAgICAgICAgXHRcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTUudmFsdWUgPSAnJztcblx0XHQgICAgICAgIFx0XHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTE2LnZhbHVlID0gJyc7XG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmNhdGVnb3J5TmFtZS52YWx1ZSA9ICcnO1xuXG5cdFx0ICAgICAgICBcdFx0dGhpcy4kcmVmc1tyZWZdWzBdLmN1c3RvbUNhdGVnb3J5ID0gZmFsc2U7XG5cdFx0XHRcdFx0fSk7XG5cbiAgICAgICAgXHRcdC8vIE11bHRpcGxlUXVpY2tSZXBvcnQudnVlXG5cdCAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0gPSBuZXcgRm9ybSh7XG5cdCAgICAgICAgXHRcdFx0ZGlzdGluY3RDbGllbnRTZXJ2aWNlczpbXSxcblxuXHQgICAgICAgIFx0XHRcdGFjdGlvbklkOiBbXSxcblxuXHQgICAgICAgIFx0XHRcdGNhdGVnb3J5SWQ6IFtdLFxuXHQgICAgICAgIFx0XHRcdGNhdGVnb3J5TmFtZTogW10sXG5cblx0ICAgICAgICBcdFx0XHRkYXRlMTogW10sXG5cdCAgICAgICAgXHRcdFx0ZGF0ZTI6IFtdLFxuXHQgICAgICAgIFx0XHRcdGRhdGUzOiBbXSxcblx0ICAgICAgICBcdFx0XHRkYXRlVGltZUFycmF5OiBbXSxcblxuXHQgICAgICAgIFx0XHRcdGV4dHJhRGV0YWlsczogW11cblx0ICAgICAgICBcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSk7XG5cdFx0XHR9LFxuXG5cdFx0XHR2YWxpZGF0ZSgpIHtcblx0XHRcdFx0dmFyIGlzVmFsaWQgPSB0cnVlO1xuXG5cdFx0XHRcdHRoaXMuZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5mb3JFYWNoKChkaXN0aW5jdENsaWVudFNlcnZpY2UsIGluZGV4KSA9PiB7XG5cdFx0XHRcdFx0bGV0IHJlZiA9ICdmb3JtLScgKyBpbmRleDtcblxuXHRcdFx0XHRcdGlmKCAhIHRoaXMuJHJlZnNbcmVmXVswXS52YWxpZGF0ZShkaXN0aW5jdENsaWVudFNlcnZpY2UpICkge1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0cmV0dXJuIGlzVmFsaWQ7XG5cdFx0XHR9LFxuXG5cdFx0XHRhZGRNdWx0aXBsZVF1aWNrUmVwb3J0KCkge1xuXHRcdFx0XHRpZih0aGlzLnZhbGlkYXRlKCkpIHtcblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmdyb3VwUmVwb3J0ID0gdGhpcy5ncm91cHJlcG9ydDtcblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmdyb3VwSWQgPSB0aGlzLmdyb3VwaWQ7XG5cdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5jbGllbnRzSWQgPSB0aGlzLmNsaWVudHNpZDtcblx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmNsaWVudFNlcnZpY2VzSWQgPSB0aGlzLmNsaWVudHNlcnZpY2VzO1xuXHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udHJhY2tpbmdzID0gdGhpcy50cmFja2luZ3M7XG5cdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kaXN0aW5jdENsaWVudFNlcnZpY2VzID0gdGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzO1xuXG5cdFx0XHRcdFx0dGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzLmZvckVhY2goKGRpc3RpbmN0Q2xpZW50U2VydmljZSwgaW5kZXgpID0+IHtcblx0XHRcdFx0XHRcdGxldCByZWYgPSAnZm9ybS0nICsgaW5kZXg7XG5cblx0XHRcdFx0XHRcdGxldCBfYWN0aW9uSWQgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQ7XG5cdFx0XHRcdFx0XHRsZXQgX2NhdGVnb3J5SWQgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZDtcblx0XHRcdFx0XHRcdGxldCBfY2F0ZWdvcnlOYW1lID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZTtcblx0XHRcdFx0XHRcdGxldCBfZGF0ZTEgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTE7XG5cdFx0XHRcdFx0XHRsZXQgX2RhdGUyID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyO1xuXHRcdFx0XHRcdFx0bGV0IF9kYXRlMyA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMztcblx0XHRcdFx0XHRcdGxldCBfZGF0ZVRpbWVBcnJheSA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlVGltZUFycmF5O1xuXHRcdFx0XHRcdFx0bGV0IF9leHRyYURldGFpbHMgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzO1xuXG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmFjdGlvbklkLnB1c2goX2FjdGlvbklkKTtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2F0ZWdvcnlJZC5wdXNoKF9jYXRlZ29yeUlkKTtcblx0ICAgICAgICBcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmNhdGVnb3J5TmFtZS5wdXNoKF9jYXRlZ29yeU5hbWUpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTEucHVzaChfZGF0ZTEpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTIucHVzaChfZGF0ZTIpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTMucHVzaChfZGF0ZTMpO1xuXHQgICAgICAgIFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZVRpbWVBcnJheS5wdXNoKF9kYXRlVGltZUFycmF5KTtcblx0ICAgICAgICBcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmV4dHJhRGV0YWlscy5wdXNoKF9leHRyYURldGFpbHMpO1xuXHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3Zpc2EvcXVpY2stcmVwb3J0Jylcblx0XHRcdCAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdCAgICAgICAgICAgIFx0aWYocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0ICAgICAgICAgICAgXHRcdHRoaXMucmVzZXRNdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSgpO1xuXHRcdFx0ICAgICAgICAgICAgXHRcdFxuXHRcdFx0ICAgICAgICAgICAgXHRcdCQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdFx0ICAgICAgICAgICAgXHRcdHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xuXG5cdFx0XHQgICAgICAgICAgICBcdFx0aWYodGhpcy5pc3dyaXRlcmVwb3J0cGFnZSkge1xuXHRcdFx0ICAgICAgICAgICAgXHRcdFx0JCgnI2FkZC1xdWljay1yZXBvcnQtbW9kYWwnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0XHRcdFx0XHRcdCAgXHR3aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvdmlzYS9yZXBvcnQvd3JpdGUtcmVwb3J0Jztcblx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0ICAgICAgICAgICAgXHRcdH0gZWxzZSB7XG5cdFx0XHQgICAgICAgICAgICBcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0ICAgICAgICAgICAgXHRcdFx0bG9jYXRpb24ucmVsb2FkKCk7XG5cdFx0XHRcdCAgICAgICAgICAgIFx0XHR9LCAxMDAwKTtcblx0XHRcdCAgICAgICAgICAgIFx0XHR9XG5cdFx0XHQgICAgICAgICAgICBcdH1cblx0XHRcdCAgICAgICAgICAgIH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHR9LFxuXG5cdFx0Y3JlYXRlZCgpIHtcblx0XHRcdHRoaXMuZ2V0U2VydmljZURvY3MoKTtcblx0XHR9LFxuXG5cdFx0Y29tcHV0ZWQ6IHtcblx0XHRcdF9pc1dyaXRlUmVwb3J0UGFnZSgpIHtcblx0XHQgICAgICBcdHJldHVybiB0aGlzLmlzd3JpdGVyZXBvcnRwYWdlIHx8IGZhbHNlO1xuXHRcdCAgICB9LFxuXG5cdFx0XHRkaXN0aW5jdENsaWVudFNlcnZpY2VzKCkge1xuXHRcdFx0XHRsZXQgZGlzdGluY3RDbGllbnRTZXJ2aWNlcyA9IFtdO1xuXG5cdFx0XHRcdHRoaXMuY2xpZW50c2VydmljZXMuZm9yRWFjaChjbGllbnRTZXJ2aWNlID0+IHtcblx0XHRcdFx0XHRpZihkaXN0aW5jdENsaWVudFNlcnZpY2VzLmluZGV4T2YoY2xpZW50U2VydmljZS5kZXRhaWwpID09IC0xKSB7XG5cdFx0XHRcdFx0XHRkaXN0aW5jdENsaWVudFNlcnZpY2VzLnB1c2goY2xpZW50U2VydmljZS5kZXRhaWwpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0cmV0dXJuIGRpc3RpbmN0Q2xpZW50U2VydmljZXM7XG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdGNvbXBvbmVudHM6IHtcblx0ICAgICAgICAnbXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0nOiByZXF1aXJlKCcuL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZScpXG5cdCAgICB9XG5cblx0fVxuXG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE11bHRpcGxlUXVpY2tSZXBvcnQudnVlPzM1Zjk5NmI5IiwiPHRlbXBsYXRlPlxuXHRcblx0PGRpdj5cblx0XHRcblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBBY3Rpb246IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwiYWN0aW9uXCIgdi1tb2RlbD1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkXCIgQGNoYW5nZT1cImdldENhdGVnb3JpZXNcIj5cblx0XHQgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwiYWN0aW9uIGluIGFjdGlvbnNcIiA6dmFsdWU9XCJhY3Rpb24uaWRcIj5cblx0XHQgICAgICAgICAgIFx0XHR7eyBhY3Rpb24ubmFtZSB9fVxuXHRcdCAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdCAgICAgICAgPC9zZWxlY3Q+XG5cdFx0ICAgIDwvZGl2PlxuXHRcdDwvZGl2PiA8IS0tIGZvcm0tZ3JvdXAgLS0+XG5cblx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMiB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAzIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDggfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNlwiPlxuXHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0ICAgICAgICBDYXRlZ29yeTogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0ICAgIDwvbGFiZWw+XG5cblx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdCAgICAgICAgPHNlbGVjdCBjbGFzcz1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJjYXRlZ29yeVwiIHYtbW9kZWw9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkXCIgQGNoYW5nZT1cImNoYW5nZUNhdGVnb3J5SGFuZGxlclwiIHYtc2hvdz1cIiFjdXN0b21DYXRlZ29yeVwiPlxuXHRcdCAgICAgICAgICAgIDxvcHRpb24gdi1mb3I9XCJjYXRlZ29yeSBpbiBjYXRlZ29yaWVzXCIgOnZhbHVlPVwiY2F0ZWdvcnkuaWRcIj5cblx0XHQgICAgICAgICAgICBcdHt7IGNhdGVnb3J5Lm5hbWUgfX1cblx0XHQgICAgICAgICAgICA8L29wdGlvbj5cblx0XHQgICAgICAgIDwvc2VsZWN0PlxuXG5cdFx0ICAgICAgICA8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDlcIj5cblx0XHQgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiByZWY9XCJjYXRlZ29yeU5hbWVcIiBwbGFjZWhvbGRlcj1cIkVudGVyIGNhdGVnb3J5XCIgdi1tb2RlbD1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZVwiIHYtc2hvdz1cImN1c3RvbUNhdGVnb3J5XCI+XG5cblx0XHQgICAgICAgIFx0PGRpdiBzdHlsZT1cImhlaWdodDoxMHB4OyBjbGVhcjpib3RoO1wiPjwvZGl2PlxuXG5cdFx0ICAgICAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zbSBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIgQGNsaWNrPVwiY3VzdG9tQ2F0ZWdvcnkgPSAhY3VzdG9tQ2F0ZWdvcnk7IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPSBudWxsXCI+XG5cdFx0XHQgICAgICAgIFx0PHNwYW4gdi1zaG93PVwiIWN1c3RvbUNhdGVnb3J5XCI+VXNlIEN1c3RvbSBDYXRlZ29yeTwvc3Bhbj5cblx0XHRcdCAgICAgICAgXHQ8c3BhbiB2LXNob3c9XCJjdXN0b21DYXRlZ29yeVwiPlVzZSBQcmVkZWZpbmVkIENhdGVnb3J5PC9zcGFuPlxuXHRcdFx0ICAgICAgICA8L2J1dHRvbj5cblx0XHQgICAgICAgIDwvZGl2PlxuXHRcdCAgICA8L2Rpdj5cblx0XHQ8L2Rpdj4gPCEtLSBmb3JtLWdyb3VwIC0tPlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxXCI+IDwhLS0gRmlsZWQgLS0+XG5cdFx0XHQ8IS0tIERhdGUgcGlja2VyIChkYXRlMSA6IGF1dG8gY2FwdHVyZSA6IHJlcXVpcmVkKSAtLT5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGUyXCIgcmVmPVwiZGF0ZTJcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDFcIj5cblx0XHRcdFx0PGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0U2NoZWR1bGVkIEhlYXJpbmcgRGF0ZSBhbmQgVGltZTpcblx0XHRcdCAgICA8L2xhYmVsPlxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJyb3dcIiB2LWZvcj1cIihkYXRlVGltZUVsZW1lbnQsIGluZGV4KSBpbiBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlVGltZUFycmF5XCI+XG5cdFx0XHRcdCAgICBcdDxkaXYgY2xhc3M9XCJjb2wtbWQtMTFcIj5cblx0XHRcdFx0XHQgICAgXHQ8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAgZGF0ZVwiIHN0eWxlPVwibWFyZ2luLWJvdHRvbTogMTBweDtcIj5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgXHQ8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG5cblx0XHQgICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJhcnJheVwiIG5hbWU9XCJkYXRlVGltZUFycmF5W11cIiB2LW1vZGVsPVwiZGF0ZVRpbWVFbGVtZW50LnZhbHVlXCIgcGxhY2Vob2xkZXI9XCJ5eXl5LW1tLWRkIGhoOm1tOnNzXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG5cdFx0ICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0ICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0ICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEgdGV4dC1jZW50ZXJcIj5cblx0ICAgICAgICAgICAgICAgICAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kYW5nZXIgYnRuLXNtXCIgQGNsaWNrPVwicmVtb3ZlRGF0ZVRpbWVQaWNrZXJBcnJheShpbmRleClcIiB2LXNob3c9XCJpbmRleCAhPSAwXCI+XG5cdCAgICAgICAgICAgICAgICAgICAgXHRcdDxpIGNsYXNzPVwiZmEgZmEtdGltZXNcIj48L2k+XG5cdCAgICAgICAgICAgICAgICAgICAgXHQ8L2J1dHRvbj5cblx0ICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT1cImhlaWdodDoxMHB4O2NsZWFyOmJvdGg7XCI+PC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLXNtIHB1bGwtcmlnaHRcIiBAY2xpY2s9XCJhZGREYXRlVGltZVBpY2tlckFycmF5XCI+XG4gICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1wbHVzXCI+PC9pPiBBZGRcbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDYgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTAgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDE1XCI+XG5cdFx0XHRcdDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdFNjaGVkdWxlZCBBcHBvaW50bWVudCBEYXRlIGFuZCBUaW1lOlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIgaWQ9XCJkYXRlM1wiIHJlZj1cImRhdGUzXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDJcIj4gPCEtLSBSZWxlYXNlZCAtLT5cblxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAzXCI+IDwhLS0gUGFpZCBPUFMgLS0+XG5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNFwiPiA8IS0tIFVuYWJsZSB0byBmaWxlIG5lZWQgYWRkaXRpb25hbCByZXF1aXJlbWVudHMgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdERvY3VtZW50czogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHQgICAgICAgICAgICA8c2VsZWN0IDppZD1cIidjaG9zZW4tJyArIGluZGV4XCIgZGF0YS1wbGFjZWhvbGRlcj1cIlNlbGVjdCBEb2NzXCIgY2xhc3M9XCJjaG9zZW4tc2VsZWN0LWZvci1kb2NzLTJcIiBtdWx0aXBsZSBzdHlsZT1cIndpZHRoOjM1MHB4O1wiIHRhYmluZGV4PVwiNFwiPlxuXHRcdCAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwic2VydmljZWRvYyBpbiBzZXJ2aWNlZG9jc1wiIDp2YWx1ZT1cInNlcnZpY2Vkb2MudGl0bGVcIj5cblx0XHQgICAgICAgICAgICAgICAgXHR7eyAoc2VydmljZWRvYy50aXRsZSkudHJpbSgpIH19XG5cdFx0ICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuXHRcdCAgICAgICAgICAgIDwvc2VsZWN0PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDZcIj4gPCEtLSBEZWFkbGluZSBvZiBTdWJtaXNzaW9uIC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRTdWJtaXNzaW9uIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGU0XCIgcmVmPVwiZGF0ZTRcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRDb25zZXF1ZW5jZSBvZiBub24gc3VibWlzc2lvbjpcblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgIFx0PGRpdj5cblx0XHRcdCAgICBcdFx0PGxhYmVsPlxuXHRcdFx0ICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiB2LW1vZGVsPVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCIgdmFsdWU9XCJBZGRpdGlvbmFsIGNvc3QgbWF5YmUgY2hhcmdlZC5cIj5cblx0XHRcdCAgICBcdFx0XHRBZGRpdGlvbmFsIGNvc3QgbWF5YmUgY2hhcmdlZC5cblx0XHRcdCAgICBcdFx0PC9sYWJlbD5cblx0XHRcdCAgICBcdDwvZGl2PlxuXHRcdFx0ICAgIFx0PGRpdj5cblx0XHRcdCAgICBcdFx0PGxhYmVsPlxuXHRcdFx0ICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiB2LW1vZGVsPVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCIgdmFsdWU9XCJBcHBsaWNhdGlvbiBtYXliZSBmb3JmZWl0ZWQgYW5kIGhhcyB0byByZS1hcHBseS5cIj5cblx0XHRcdCAgICBcdFx0XHRBcHBsaWNhdGlvbiBtYXliZSBmb3JmZWl0ZWQgYW5kIGhhcyB0byByZS1hcHBseS5cblx0XHRcdCAgICBcdFx0PC9sYWJlbD5cblx0XHRcdCAgICBcdDwvZGl2PlxuXHRcdFx0ICAgIFx0PGRpdj5cblx0XHRcdCAgICBcdFx0PGxhYmVsPlxuXHRcdFx0ICAgIFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiB2LW1vZGVsPVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCIgdmFsdWU9XCJJbml0aWFsIGRlcG9zaXQgbWF5YmUgZm9yZmVpdGVkLlwiPlxuXHRcdFx0ICAgIFx0XHRcdEluaXRpYWwgZGVwb3NpdCBtYXliZSBmb3JmZWl0ZWQuXG5cdFx0XHQgICAgXHRcdDwvbGFiZWw+XG5cdFx0XHQgICAgXHQ8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA3XCI+IDwhLS0gRm9yIEltcGxlbWVudGF0aW9uIC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFc3RpbWF0ZWQgVGltZSBvZiBGaW5pc2hpbmc6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIgaWQ9XCJkYXRlNVwiIHJlZj1cImRhdGU1XCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDhcIj4gPCEtLSBVbmFibGUgdG8gZmlsZSBkdWUgdG8gYmFkIHdlYXRoZXIgLS0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdEFmZmVjdGVkIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgXHQ8ZGl2IGNsYXNzPVwiaW5wdXQtZGF0ZXJhbmdlIGlucHV0LWdyb3VwXCI+XG5cdFx0ICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaW5wdXQtc20gZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGU2XCIgcmVmPVwiZGF0ZTZcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPlxuXHRcdCAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+dG88L3NwYW4+XG5cdFx0ICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaW5wdXQtc20gZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGU3XCIgcmVmPVwiZGF0ZTdcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPlxuXHRcdCAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0VGFyZ2V0IEZpbGxpbmcgRGF0ZTpcblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAgZGF0ZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgXHQ8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIgaWQ9XCJkYXRlOFwiIHJlZj1cImRhdGU4XCIgYXV0b2NvbXBsZXRlPVwib2ZmXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDlcIj4gPCEtLSBVbmFibGUgdG8gZmluaXNoIGR1ZSB0byBzdXNwZW5zaW9uIG9mIHdvcmsgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICBcdERhdGUgUmFuZ2U6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1kYXRlcmFuZ2UgaW5wdXQtZ3JvdXBcIj5cblx0XHQgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTlcIiByZWY9XCJkYXRlOVwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG5cdFx0ICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj50bzwvc3Bhbj5cblx0XHQgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiIGlkPVwiZGF0ZTEwXCIgcmVmPVwiZGF0ZTEwXCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgLz5cblx0XHQgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMFwiPiA8IS0tIFByb2Nlc3NpbmcgUGVyaW9kIEV4dGVuc2lvbiAtLT5cblx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0RXh0ZW5kZWQgcGVyaW9kIGZvciBwcm9jZXNzaW5nOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAgZGF0ZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgXHQ8aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIgaWQ9XCJkYXRlMTFcIiByZWY9XCJkYXRlMTFcIiBhdXRvY29tcGxldGU9XCJvZmZcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMVwiPiA8IS0tIEZvciBwYXltZW50IC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGUxMlwiIHJlZj1cImRhdGUxMlwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+XG5cblx0XHQ8ZGl2IHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEyXCI+IDwhLS0gUGlja2VkIHVwIGZyb20gY2xpZW50IC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHREYXRlIGFuZCBUaW1lIFBpY2sgVXA6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIgaWQ9XCJkYXRlMTNcIiByZWY9XCJkYXRlMTNcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTNcIj4gPCEtLSBGb3IgZGVsaXZlcnkgbm93IC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHREYXRlIGFuZCBUaW1lIERlbGl2ZXI6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGV0aW1lcGlja2VyXCIgaWQ9XCJkYXRlMTRcIiByZWY9XCJkYXRlMTRcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQ8L2Rpdj5cblxuXHRcdDxkaXYgdi1zaG93PVwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTRcIj4gPCEtLSBEZWxpdmVyZWQgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTI4XCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0Q29tcGFueSBDb3VyaWVyOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJjb21wYW55X2NvdXJpZXJcIiByZWY9XCJjb21wYW55X2NvdXJpZXJcIj5cblx0XHRcdCAgICAgICAgXHQ8b3B0aW9uIHYtZm9yPVwiY29tcGFueUNvdXJpZXIgaW4gY29tcGFueUNvdXJpZXJzXCIgOnZhbHVlPVwiY29tcGFueUNvdXJpZXIuYWRkcmVzcy5jb250YWN0X251bWJlclwiPlxuXHRcdFx0ICAgICAgICBcdFx0e3sgY29tcGFueUNvdXJpZXIuZmlyc3RfbmFtZSArICcgJyArIGNvbXBhbnlDb3VyaWVyLmxhc3RfbmFtZSB9fSBcblx0XHRcdCAgICAgICAgXHRcdCh7eyBjb21wYW55Q291cmllci5hZGRyZXNzLmNvbnRhY3RfbnVtYmVyIH19KVxuXHRcdFx0ICAgICAgICBcdDwvb3B0aW9uPlxuXHRcdFx0ICAgICAgICA8L3NlbGVjdD5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIHYtc2hvdz1cIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgIT0gMTI4XCI+XG5cdFx0XHQgICAgPGxhYmVsIGNsYXNzPVwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiPlxuXHRcdFx0ICAgICAgIFx0VHJhY2tpbmcgTnVtYmVyOiA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Kjwvc3Bhbj5cblx0XHRcdCAgICA8L2xhYmVsPlxuXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMFwiPlxuXHRcdFx0ICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIGlkPVwidHJhY2tpbmdfbnVtYmVyXCIgcmVmPVwidHJhY2tpbmdfbnVtYmVyXCI+XG5cdFx0XHQgICAgPC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHREYXRlIGFuZCBUaW1lIERlbGl2ZXJlZDogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0XHQgICAgPC9sYWJlbD5cblxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwIGRhdGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIFx0PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgXHQ8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIiBpZD1cImRhdGUxNVwiIHJlZj1cImRhdGUxNVwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNVwiPiA8IS0tIEZvciBwcm9jZXNzIG9mIC0tPlxuXHRcdFx0PGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIiB2LXNob3c9XCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc2IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzcgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3OCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc5IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODAgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgyIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODNcIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdFx0ICAgIDwvbGFiZWw+XG5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBkYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICBcdDxpIGNsYXNzPVwiZmEgZmEtY2FsZW5kYXJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgIFx0PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICBcdDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIiBpZD1cImRhdGUxNlwiIHJlZj1cImRhdGUxNlwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0PC9kaXY+IFxuXG5cdDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXHRcblx0ZXhwb3J0IGRlZmF1bHQge1xuXG5cdFx0cHJvcHM6IFsnaW5kZXgnLCAnZG9jcycsICdzZXJ2aWNlZG9jcyddLFxuXG5cdFx0ZGF0YSgpIHtcbiAgICAgICAgXHRyZXR1cm4ge1xuXG4gICAgICAgIFx0XHRhY3Rpb25zOiBbXSxcbiAgICAgICAgXHRcdGNhdGVnb3JpZXM6IFtdLFxuXG4gICAgICAgIFx0XHRjb21wYW55Q291cmllcnM6IFtdLFxuXG4gICAgICAgIFx0XHRjdXN0b21DYXRlZ29yeTogZmFsc2UsXG5cbiAgICAgICAgXHRcdG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtOiB7XG4gICAgICAgIFx0XHRcdGFjdGlvbklkOiAxLFxuXG4gICAgICAgIFx0XHRcdGNhdGVnb3J5SWQ6IG51bGwsXG4gICAgICAgIFx0XHRcdGNhdGVnb3J5TmFtZTogbnVsbCxcblxuICAgICAgICBcdFx0XHRkYXRlMTogbnVsbCxcbiAgICAgICAgXHRcdFx0ZGF0ZTI6IG51bGwsXG4gICAgICAgIFx0XHRcdGRhdGUzOiBudWxsLFxuICAgICAgICBcdFx0XHRkYXRlVGltZUFycmF5OiBbXSxcblxuICAgICAgICBcdFx0XHRleHRyYURldGFpbHM6IFtdXG4gICAgICAgIFx0XHR9XG5cbiAgICAgICAgXHR9XG4gICAgICAgIH0sXG5cbiAgICAgICAgbWV0aG9kczoge1xuXG4gICAgICAgIFx0Z2V0QWN0aW9ucygpIHtcbiAgICAgICAgXHRcdGF4aW9zLmdldCgnL3Zpc2EvYWN0aW9ucycpXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5hY3Rpb25zID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgICAgICBcdH0sXG5cbiAgICAgICAgXHRnZXRDb21wYW55Q291cmllcnMoKSB7XG4gICAgICAgIFx0XHRsZXQgcm9sZSA9ICdjb21wYW55LWNvdXJpZXInO1xuXG4gICAgICAgIFx0XHRheGlvcy5nZXQoJy92aXNhL3VzZXJzLWJ5LXJvbGUvJyArIHJvbGUpXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5jb21wYW55Q291cmllcnMgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGdldEN1cnJlbnREYXRlKCkge1xuICAgICAgICBcdFx0bGV0IHRvZGF5ID0gbmV3IERhdGUoKTtcblx0XHRcdFx0bGV0IGRkID0gdG9kYXkuZ2V0RGF0ZSgpO1xuXHRcdFx0XHRsZXQgbW0gPSB0b2RheS5nZXRNb250aCgpICsgMTtcblx0XHRcdFx0bGV0IHl5eXkgPSB0b2RheS5nZXRGdWxsWWVhcigpO1xuXG5cdFx0XHRcdGlmKGRkIDwgMTApIHtcblx0XHRcdFx0ICAgIGRkID0gJzAnICsgZGQ7XG5cdFx0XHRcdH0gXG5cblx0XHRcdFx0aWYobW0gPCAxMCkge1xuXHRcdFx0XHQgICAgbW0gPSAnMCcgKyBtbTtcblx0XHRcdFx0fSBcblxuXHRcdFx0XHRyZXR1cm4gbW0gKyAnLycgKyBkZCArICcvJyArIHl5eXk7XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGNoYW5nZUFjdGlvbkhhbmRsZXIoYWN0aW9uSWQpIHtcbiAgICAgICAgXHRcdC8vIFJlc2V0XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gbnVsbDtcbiAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTIgPSBudWxsO1xuICAgICAgICBcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMyA9IG51bGw7XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkgPSBbXTtcbiAgICAgICAgXHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzID0gW107XG5cbiAgICAgICAgXHRcdC8vIEZvciBhdXRvIGNhcHR1cmUgZGF0ZVxuICAgICAgICBcdFx0aWYoYWN0aW9uSWQgPT0gMSB8fCBhY3Rpb25JZCA9PSAzIHx8IGFjdGlvbklkID09IDQgfHwgIGFjdGlvbklkID09IDcgfHwgYWN0aW9uSWQgPT0gMTEgfHwgYWN0aW9uSWQgPT0gMTUgfHwgYWN0aW9uSWQgPT0gMTYpIHtcbiAgICAgICAgXHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IHRoaXMuZ2V0Q3VycmVudERhdGUoKTtcbiAgICAgICAgXHRcdH1cbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0Z2V0Q2F0ZWdvcmllcygpIHtcbiAgICAgICAgXHRcdGxldCBhY3Rpb25JZCA9IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQ7XG5cbiAgICAgICAgXHRcdHRoaXMuY2hhbmdlQWN0aW9uSGFuZGxlcihhY3Rpb25JZCk7XG5cbiAgICAgICAgXHRcdHRoaXMuY2F0ZWdvcmllcyA9IFtdO1xuXG4gICAgICAgIFx0XHRheGlvcy5nZXQoJy92aXNhL2NhdGVnb3JpZXMtYnktYWN0aW9uLWlkJywge1xuICAgICAgICBcdFx0XHRcdHBhcmFtczoge1xuXHRcdFx0XHRcdCAgICBcdGFjdGlvbklkOiBhY3Rpb25JZFxuXHRcdFx0XHRcdCAgICB9XG4gICAgICAgIFx0XHRcdH0pXG5cdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5jYXRlZ29yaWVzID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgICAgICBcdH0sXG5cbiAgICAgICAgXHRjaGFuZ2VDYXRlZ29yeUhhbmRsZXIoKSB7XG4gICAgICAgIFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEgJiYgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEpIHtcbiAgICAgICAgXHRcdFx0dGhpcy5hZGREYXRlVGltZVBpY2tlckFycmF5KCk7XG4gICAgICAgIFx0XHR9XG4gICAgICAgIFx0fSxcblxuICAgICAgICBcdGFkZERhdGVUaW1lUGlja2VyQXJyYXkoKSB7XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkucHVzaCh7dmFsdWU6bnVsbH0pO1xuXG4gICAgICAgIFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdCAgICAgICAgJCgnLmRhdGV0aW1lcGlja2VyYXJyYXknKS5pbnB1dG1hc2soXCJkYXRldGltZVwiLCB7XG5cdFx0ICAgICAgICAgICAgICAgIG1hc2s6IFwieS0yLTEgaDpzOnNcIiwgXG5cdFx0ICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcInl5eXktbW0tZGQgaGg6bW06c3NcIiwgXG5cdFx0ICAgICAgICAgICAgICAgIGxlYXBkYXk6IFwiLTAyLTI5XCIsIFxuXHRcdCAgICAgICAgICAgICAgICBzZXBhcmF0b3I6IFwiLVwiLCBcblx0XHQgICAgICAgICAgICAgICAgYWxpYXM6IFwieXl5eS1tbS1kZFwiXG5cdFx0ICAgICAgICAgICAgfSk7XG4gICAgICAgIFx0XHR9LCA1MDApO1xuICAgICAgICBcdH0sXG5cbiAgICAgICAgXHRyZW1vdmVEYXRlVGltZVBpY2tlckFycmF5KGluZGV4KSB7XG4gICAgICAgIFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgXHR9LFxuXG4gICAgICAgIFx0dmFsaWRhdGUoZGlzdGluY3RDbGllbnRTZXJ2aWNlKSB7XG4gICAgICAgIFx0XHR2YXIgaXNWYWxpZCA9IHRydWU7XG5cbiAgICAgICAgXHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMSkge1xuXHRcdFx0XHRcdGxldCBkYXRlMiA9IHRoaXMuJHJlZnMuZGF0ZTIudmFsdWU7XG5cdFx0XHRcdFx0bGV0IGRhdGUzID0gKHRoaXMuJHJlZnMuZGF0ZTMudmFsdWUpID8gdGhpcy4kcmVmcy5kYXRlMy52YWx1ZSA6ICcnO1xuXG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZihkYXRlMiA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBlc3RpbWF0ZWQgcmVsZWFzaW5nIGRhdGUuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0bGV0IGRhdGVUaW1lQXJyYXkgPSBbXTtcblx0XHQgICAgICAgIFx0XHQkKCdpbnB1dFtuYW1lXj1cImRhdGVUaW1lQXJyYXlcIl0nKS5lYWNoKCBmdW5jdGlvbihlKSB7XG5cdFx0ICAgICAgICBcdFx0XHRpZih0aGlzLnZhbHVlICE9ICcnKSB7IGRhdGVUaW1lQXJyYXkucHVzaCh0aGlzLnZhbHVlKTsgfVxuXHRcdFx0XHRcdCAgICB9KTtcblxuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IGRhdGUyO1xuXHRcdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDYgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDcgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEwIHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNCB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTUpIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMyA9IGRhdGUzO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXkgPSBkYXRlVGltZUFycmF5O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMiB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDMpIHtcblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA0KSB7XG5cdFx0XHRcdFx0bGV0IGRvY3MgPSBbXTtcblx0ICAgICAgICBcdFx0JCgnI2Nob3Nlbi0nICsgdGhpcy5pbmRleCArJyBvcHRpb246c2VsZWN0ZWQnKS5lYWNoKCBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdCAgICAgICAgZG9jcy5wdXNoKCQodGhpcykudmFsKCkpO1xuXHRcdFx0XHQgICAgfSk7XG5cblx0XHRcdFx0XHRpZihkb2NzLmxlbmd0aCA9PSAwKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgZG9jdW1lbnRzLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdCQoJyNjaG9zZW4tJyArIHRoaXMuaW5kZXgpLnZhbCgnJykudHJpZ2dlcignY2hvc2VuOnVwZGF0ZWQnKTtcblxuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMgPSBkb2NzO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNikge1xuXHRcdFx0XHRcdGxldCBkYXRlNCA9IHRoaXMuJHJlZnMuZGF0ZTQudmFsdWU7XG5cblx0XHRcdFx0XHRpZihkYXRlNCA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBzdWJtaXNzaW9uIGRhdGUuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGU0O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNykge1xuXHRcdFx0XHRcdGxldCBkYXRlNSA9IHRoaXMuJHJlZnMuZGF0ZTUudmFsdWU7XG5cblx0XHRcdFx0XHRpZihkYXRlNSA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBlc3RpbWF0ZWQgdGltZSBvZiBmaW5pc2hpbmcgZGF0ZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyID0gZGF0ZTU7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA4KSB7XG5cdFx0XHRcdFx0bGV0IGRhdGU2ID0gdGhpcy4kcmVmcy5kYXRlNi52YWx1ZTtcblx0XHRcdFx0XHRsZXQgZGF0ZTcgPSB0aGlzLiRyZWZzLmRhdGU3LnZhbHVlO1xuXHRcdFx0XHRcdGxldCBkYXRlOCA9IHRoaXMuJHJlZnMuZGF0ZTgudmFsdWU7XG5cblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKGRhdGU2ID09ICcnIHx8IGRhdGU3ID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGFmZmVjdGVkIGRhdGUuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGU2O1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IGRhdGU3O1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMyA9IGRhdGU4O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOSkge1xuXHRcdFx0XHRcdGlmKHRoaXMuY3VzdG9tQ2F0ZWdvcnkpIHtcblx0XHRcdFx0XHRcdGxldCBjYXRlZ29yeU5hbWUgPSB0aGlzLiRyZWZzLmNhdGVnb3J5TmFtZS52YWx1ZTtcblxuXHRcdFx0XHRcdFx0aWYoY2F0ZWdvcnlOYW1lID09ICcnKSB7XG5cdFx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPSAwO1xuXHRcdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZSA9IGNhdGVnb3J5TmFtZTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRsZXQgZGF0ZTkgPSB0aGlzLiRyZWZzLmRhdGU5LnZhbHVlO1xuXHRcdFx0XHRcdGxldCBkYXRlMTAgPSB0aGlzLiRyZWZzLmRhdGUxMC52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSBudWxsKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY2F0ZWdvcnkuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoZGF0ZTkgPT0gJycgfHwgZGF0ZTEwID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGRhdGUgcmFuZ2UuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGU5O1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IGRhdGUxMDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEwKSB7XG5cdFx0XHRcdFx0bGV0IGRhdGUxMSA9IHRoaXMuJHJlZnMuZGF0ZTExLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZihkYXRlMTEgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgZXh0ZW5kZWQgcGVyaW9kIGZvciBwcm9jZXNzaW5nLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTEgPSBkYXRlMTE7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMSkge1xuXHRcdFx0XHRcdGxldCBkYXRlMTIgPSB0aGlzLiRyZWZzLmRhdGUxMi52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSBudWxsKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY2F0ZWdvcnkuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoZGF0ZTEyID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBmaWxsIHVwIGVzdGltYXRlZCByZWxlYXNpbmcgZGF0ZS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyID0gZGF0ZTEyO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTIpIHtcblx0XHRcdFx0XHRsZXQgZGF0ZTEzID0gdGhpcy4kcmVmcy5kYXRlMTMudmFsdWU7XG5cblx0XHRcdFx0XHRpZihkYXRlMTMgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgZGF0ZSBhbmQgdGltZSBwaWNrIHVwLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTEgPSBkYXRlMTM7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9IGVsc2UgaWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMykge1xuXHRcdFx0XHRcdGxldCBkYXRlMTQgPSB0aGlzLiRyZWZzLmRhdGUxNC52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKGRhdGUxNCA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBkYXRlIGFuZCB0aW1lIGRlbGl2ZXIuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMSA9IGRhdGUxNDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE0KSB7XG5cdFx0XHRcdFx0bGV0IGRhdGUxNSA9IHRoaXMuJHJlZnMuZGF0ZTE1LnZhbHVlO1xuXHRcdFx0XHRcdGxldCBjb21wYW55Q291cmllciA9IHRoaXMuJHJlZnMuY29tcGFueV9jb3VyaWVyLnZhbHVlO1xuXHRcdFx0XHRcdGxldCB0cmFja2luZ051bWJlciA9IHRoaXMuJHJlZnMudHJhY2tpbmdfbnVtYmVyLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTI4ICYmIGNvbXBhbnlDb3VyaWVyID09ICcnKSB7XG5cdFx0XHRcdFx0XHR0b2FzdHIuZXJyb3IoJ1BsZWFzZSBzZWxlY3QgY29tcGFueSBjb3VyaWVyLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCAhPSAxMjggJiYgdHJhY2tpbmdOdW1iZXIgPT0gJycpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIGZpbGwgdXAgdHJhY2tpbmcgbnVtYmVyLicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKGRhdGUxNSA9PSAnJykge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBkYXRlIGFuZCB0aW1lIGRlbGl2ZXJlZC4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUxID0gZGF0ZTE1O1xuXG5cdFx0XHRcdFx0XHRsZXQgZXh0cmFEZXRhaWwgPSAodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEyOCkgPyBjb21wYW55Q291cmllciA6IHRyYWNraW5nTnVtYmVyO1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMgPSBbZXh0cmFEZXRhaWxdO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIGlmKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTUpIHtcblx0XHRcdFx0XHRsZXQgZGF0ZTE2ID0gdGhpcy4kcmVmcy5kYXRlMTYudmFsdWU7XG5cblx0XHRcdFx0XHRpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2Ugc2VsZWN0IGNhdGVnb3J5LicsIGRpc3RpbmN0Q2xpZW50U2VydmljZSk7XG5cdFx0XHRcdFx0XHRpc1ZhbGlkID0gZmFsc2U7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKCBkYXRlMTYgPT0gJycgJiYgKHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3NiB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzcgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc4IHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3OSB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODAgfHwgdGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgxIHx8IHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MiB8fCB0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODMpICkge1xuXHRcdFx0XHRcdFx0dG9hc3RyLmVycm9yKCdQbGVhc2UgZmlsbCB1cCBlc3RpbWF0ZWQgcmVsZWFzaW5nIGRhdGUuJywgZGlzdGluY3RDbGllbnRTZXJ2aWNlKTtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSBmYWxzZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMiA9IGRhdGUxNjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0gZWxzZSBpZih0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE2KSB7XG5cdFx0XHRcdFx0aWYodGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IG51bGwpIHtcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcignUGxlYXNlIHNlbGVjdCBjYXRlZ29yeS4nLCBkaXN0aW5jdENsaWVudFNlcnZpY2UpO1xuXHRcdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cdFx0XHRcdHJldHVybiBpc1ZhbGlkO1xuICAgICAgICBcdH1cblxuICAgICAgICB9LFxuXG4gICAgICAgIGNyZWF0ZWQoKSB7XG4gICAgICAgIFx0dGhpcy5nZXRBY3Rpb25zKCk7XG5cbiAgICAgICAgXHR0aGlzLmdldENhdGVnb3JpZXMoKTtcblxuICAgICAgICBcdHRoaXMuZ2V0Q29tcGFueUNvdXJpZXJzKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgbW91bnRlZCgpIHtcblxuICAgICAgICBcdC8vIENob3NlblxuICAgICAgICBcdFx0JCgnLmNob3Nlbi1zZWxlY3QtZm9yLWRvY3MsIC5jaG9zZW4tc2VsZWN0LWZvci1kb2NzLTInKS5jaG9zZW4oe1xuXHRcdFx0XHRcdHdpZHRoOiBcIjEwMCVcIixcblx0XHRcdFx0XHRub19yZXN1bHRzX3RleHQ6IFwiTm8gcmVzdWx0L3MgZm91bmQuXCIsXG5cdFx0XHRcdFx0c2VhcmNoX2NvbnRhaW5zOiB0cnVlXG5cdFx0XHRcdH0pO1xuXG4gICAgICAgIFx0Ly8gRGF0ZVBpY2tlclxuXHQgICAgICAgIFx0JCgnLmRhdGVwaWNrZXInKS5kYXRlcGlja2VyKHtcblx0ICAgICAgICAgICAgICAgIHRvZGF5QnRuOiBcImxpbmtlZFwiLFxuXHQgICAgICAgICAgICAgICAga2V5Ym9hcmROYXZpZ2F0aW9uOiBmYWxzZSxcblx0ICAgICAgICAgICAgICAgIGZvcmNlUGFyc2U6IGZhbHNlLFxuXHQgICAgICAgICAgICAgICAgY2FsZW5kYXJXZWVrczogdHJ1ZSxcblx0ICAgICAgICAgICAgICAgIGF1dG9jbG9zZTogdHJ1ZSxcblx0ICAgICAgICAgICAgICAgIGZvcm1hdDogXCJtbS9kZC95eXl5XCIsXG5cdCAgICAgICAgICAgICAgICBzdGFydERhdGU6IG5ldyBEYXRlKClcblx0ICAgICAgICAgICAgfSk7XG5cblx0XHRcdC8vIERhdGVUaW1lIFBpY2tlclxuXHRcdFx0XHQkKCcuZGF0ZXRpbWVwaWNrZXInKS5pbnB1dG1hc2soXCJkYXRldGltZVwiLCB7XG5cdFx0ICAgICAgICAgICAgbWFzazogXCJ5LTItMSBoOnM6c1wiLCBcblx0XHQgICAgICAgICAgICBwbGFjZWhvbGRlcjogXCJ5eXl5LW1tLWRkIGhoOm1tOnNzXCIsIFxuXHRcdCAgICAgICAgICAgIGxlYXBkYXk6IFwiLTAyLTI5XCIsIFxuXHRcdCAgICAgICAgICAgIHNlcGFyYXRvcjogXCItXCIsIFxuXHRcdCAgICAgICAgICAgIGFsaWFzOiBcInl5eXktbW0tZGRcIlxuXHRcdCAgICAgICAgfSk7XG5cbiAgICAgICAgfVxuXG5cdH1cblxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZT83YTJmODU2NyIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoKTtcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcbmZvcm1bZGF0YS12LTBmMmYyYjc3XSB7XFxuXFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxuLm0tci0xMFtkYXRhLXYtMGYyZjJiNzddIHtcXG5cXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJNdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZT8zNWY5OTZiOVwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiO0FBNFBBO0NBQ0Esb0JBQUE7Q0FDQTtBQUNBO0NBQ0EsbUJBQUE7Q0FDQVwiLFwiZmlsZVwiOlwiTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdFxcblxcdDxmb3JtIGNsYXNzPVxcXCJmb3JtLWhvcml6b250YWxcXFwiIEBzdWJtaXQucHJldmVudD1cXFwiYWRkTXVsdGlwbGVRdWlja1JlcG9ydFxcXCI+XFxuXFxuXFx0XFx0PGRpdiBjbGFzcz1cXFwiaW9zLXNjcm9sbFxcXCI+XFxuXFx0XFx0XFx0PGRpdiB2LWZvcj1cXFwiKGRpc3RpbmN0Q2xpZW50U2VydmljZSwgaW5kZXgpIGluIGRpc3RpbmN0Q2xpZW50U2VydmljZXMubGVuZ3RoXFxcIj5cXG5cXG5cXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJwYW5lbCBwYW5lbC1kZWZhdWx0XFxcIj5cXG5cXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicGFuZWwtaGVhZGluZ1xcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICB7eyBkaXN0aW5jdENsaWVudFNlcnZpY2VzW2luZGV4XSB9fVxcblxcdCAgICAgICAgICAgICAgICA8L2Rpdj5cXG5cXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicGFuZWwtYm9keVxcXCI+XFxuXFx0ICAgICAgICAgICAgICAgICAgICBcXG5cXHQgICAgICAgICAgICAgICAgXFx0PG11bHRpcGxlLXF1aWNrLXJlcG9ydC1mb3JtIDprZXk9XFxcImluZGV4XFxcIiA6cmVmPVxcXCInZm9ybS0nICsgaW5kZXhcXFwiIDpkb2NzPVxcXCJkb2NzXFxcIiA6c2VydmljZWRvY3M9XFxcInNlcnZpY2VEb2NzXFxcIiA6aW5kZXg9XFxcImluZGV4XFxcIj48L211bHRpcGxlLXF1aWNrLXJlcG9ydC1mb3JtPlxcblxcblxcdCAgICAgICAgICAgICAgICA8L2Rpdj4gPCEtLSBwYW5lbC1ib2R5IC0tPlxcblxcdCAgICAgICAgICAgIDwvZGl2PiA8IS0tIHBhbmVsIC0tPlxcblxcblxcdFxcdFxcdDwvZGl2PiA8IS0tIGxvb3AgLS0+XFxuXFx0XFx0PC9kaXY+IDwhLS0gaW9zLXNjcm9sbCAtLT5cXG5cXHRcXHRcXG5cXHRcXHQ8YnV0dG9uIHR5cGU9XFxcInN1Ym1pdFxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XFxcIj5DcmVhdGUgUmVwb3J0PC9idXR0b24+XFxuXFx0ICAgIDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXFxcIiBkYXRhLWRpc21pc3M9XFxcIm1vZGFsXFxcIj5DbG9zZTwvYnV0dG9uPlxcblxcdDwvZm9ybT5cXG5cXG48L3RlbXBsYXRlPlxcblxcbjxzY3JpcHQ+XFxuXFxuXFx0ZXhwb3J0IGRlZmF1bHQge1xcblxcblxcdFxcdHByb3BzOiBbJ2dyb3VwcmVwb3J0JywgJ2dyb3VwaWQnLCAnY2xpZW50c2lkJywgJ2NsaWVudHNlcnZpY2VzJywgJ3RyYWNraW5ncycsICdpc3dyaXRlcmVwb3J0cGFnZSddLFxcblxcblxcdFxcdGRhdGEoKSB7XFxuXFx0XFx0XFx0cmV0dXJuIHtcXG5cXHRcXHRcXG5cXHRcXHRcXHRcXHRkb2NzOiBbXSxcXG5cXHRcXHRcXHRcXHRzZXJ2aWNlRG9jczogW10sXFxuXFxuICAgICAgICBcXHRcXHRtdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybSA6IG5ldyBGb3JtKHtcXG4gICAgICAgIFxcdFxcdFxcdGRpc3RpbmN0Q2xpZW50U2VydmljZXM6W10sXFxuXFxuICAgICAgICBcXHRcXHRcXHRncm91cFJlcG9ydDogbnVsbCxcXG4gICAgICAgIFxcdFxcdFxcdGdyb3VwSWQ6IG51bGwsXFxuXFxuICAgICAgICBcXHRcXHRcXHRjbGllbnRzSWQ6IFtdLFxcbiAgICAgICAgXFx0XFx0XFx0Y2xpZW50U2VydmljZXNJZDogW10sXFxuICAgICAgICBcXHRcXHRcXHR0cmFja2luZ3M6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0XFx0YWN0aW9uSWQ6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0XFx0Y2F0ZWdvcnlJZDogW10sXFxuICAgICAgICBcXHRcXHRcXHRjYXRlZ29yeU5hbWU6IFtdLFxcblxcbiAgICAgICAgXFx0XFx0XFx0ZGF0ZTE6IFtdLFxcbiAgICAgICAgXFx0XFx0XFx0ZGF0ZTI6IFtdLFxcbiAgICAgICAgXFx0XFx0XFx0ZGF0ZTM6IFtdLFxcbiAgICAgICAgXFx0XFx0XFx0ZGF0ZVRpbWVBcnJheTogW10sXFxuXFxuICAgICAgICBcXHRcXHRcXHRleHRyYURldGFpbHM6IFtdXFxuICAgICAgICBcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxcblxcblxcdFxcdFxcdH1cXG5cXHRcXHR9LFxcblxcblxcdFxcdG1ldGhvZHM6IHtcXG5cXG5cXHRcXHRcXHRmZXRjaERvY3MoaWQpIHtcXG5cXHRcXHRcXHRcXHR2YXIgaWRzID0gJyc7XFxuXFx0XFx0XFx0XFx0JC5lYWNoKGlkLCBmdW5jdGlvbihpLCBpdGVtKSB7XFxuXFx0XFx0XFx0XFx0ICAgIGlkcyArPSBpZFtpXS5kb2NzX25lZWRlZCArICcsJyArIGlkW2ldLmRvY3Nfb3B0aW9uYWwgKyAnLCc7XFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0XFx0dmFyIHVuaXF1ZUxpc3Q9aWRzLnNwbGl0KCcsJykuZmlsdGVyKGZ1bmN0aW9uKGFsbEl0ZW1zLGksYSl7XFxuXFx0XFx0XFx0XFx0ICAgIHJldHVybiBpPT1hLmluZGV4T2YoYWxsSXRlbXMpO1xcblxcdFxcdFxcdFxcdH0pLmpvaW4oJywnKTtcXG5cXHQgICAgICAgIFxcdFxcblxcdFxcdFxcdCBcXHRheGlvcy5nZXQoJy92aXNhL2dyb3VwLycrdW5pcXVlTGlzdCsnL3NlcnZpY2UtZG9jcycpXFxuXFx0XFx0XFx0IFxcdCAgLnRoZW4ocmVzdWx0ID0+IHtcXG5cXHRcXHQgICAgICAgICAgICB0aGlzLmRvY3MgPSByZXN1bHQuZGF0YTtcXG5cXHRcXHQgICAgICAgIH0pO1xcblxcblxcdCAgICAgICAgXFx0c2V0VGltZW91dCgoKSA9PiB7XFxuXFx0ICAgICAgICBcXHRcXHQkKCcuY2hvc2VuLXNlbGVjdC1mb3ItZG9jcycpLnRyaWdnZXIoJ2Nob3Nlbjp1cGRhdGVkJyk7XFx0XFxuXFx0ICAgICAgICBcXHR9LCA1MDAwKTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGdldFNlcnZpY2VEb2NzKCkge1xcbiAgICAgICAgXFx0XFx0YXhpb3MuZ2V0KCcvdmlzYS9ncm91cC9zZXJ2aWNlLWRvY3MtaW5kZXgnKVxcblxcdFxcdFxcdCBcXHQgIC50aGVuKHJlc3VsdCA9PiB7XFxuXFx0XFx0ICAgICAgICAgICAgdGhpcy5zZXJ2aWNlRG9jcyA9IHJlc3VsdC5kYXRhO1xcblxcdFxcdCAgICAgICAgfSk7XFxuICAgICAgICBcXHR9LFxcblxcblxcdFxcdFxcdHJlc2V0TXVsdGlwbGVRdWlja1JlcG9ydEZvcm0oKSB7XFxuXFx0XFx0XFx0XFx0Ly8gTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5kaXN0aW5jdENsaWVudFNlcnZpY2VzLmZvckVhY2goKGRpc3RpbmN0Q2xpZW50U2VydmljZSwgaW5kZXgpID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgcmVmID0gJ2Zvcm0tJyArIGluZGV4O1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSA9IHtcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGFjdGlvbklkOiAxLFxcblxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0Y2F0ZWdvcnlJZDogbnVsbCxcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGNhdGVnb3J5TmFtZTogbnVsbCxcXG5cXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGRhdGUxOiBudWxsLFxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTI6IG51bGwsXFxuXFx0XFx0ICAgICAgICBcXHRcXHRcXHRkYXRlMzogbnVsbCxcXG5cXHRcXHQgICAgICAgIFxcdFxcdFxcdGRhdGVUaW1lQXJyYXk6IFtdLFxcblxcblxcdFxcdCAgICAgICAgXFx0XFx0XFx0ZXh0cmFEZXRhaWxzOiBbXVxcblxcdFxcdCAgICAgICAgXFx0XFx0fVxcblxcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGUyLnZhbHVlID0gJyc7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTMudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNC52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGU1LnZhbHVlID0gJyc7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTYudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlNy52YWx1ZSA9ICcnO1xcblxcdFxcdCAgICAgICAgXFx0XFx0dGhpcy4kcmVmc1tyZWZdWzBdLiRyZWZzLmRhdGU4LnZhbHVlID0gJyc7XFxuXFx0XFx0ICAgICAgICBcXHRcXHR0aGlzLiRyZWZzW3JlZl1bMF0uJHJlZnMuZGF0ZTkudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTAudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTEudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTIudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTMudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTQudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTUudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5kYXRlMTYudmFsdWUgPSAnJztcXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS4kcmVmcy5jYXRlZ29yeU5hbWUudmFsdWUgPSAnJztcXG5cXG5cXHRcXHQgICAgICAgIFxcdFxcdHRoaXMuJHJlZnNbcmVmXVswXS5jdXN0b21DYXRlZ29yeSA9IGZhbHNlO1xcblxcdFxcdFxcdFxcdFxcdH0pO1xcblxcbiAgICAgICAgXFx0XFx0Ly8gTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcXG5cXHQgICAgICAgIFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0gPSBuZXcgRm9ybSh7XFxuXFx0ICAgICAgICBcXHRcXHRcXHRkaXN0aW5jdENsaWVudFNlcnZpY2VzOltdLFxcblxcblxcdCAgICAgICAgXFx0XFx0XFx0YWN0aW9uSWQ6IFtdLFxcblxcblxcdCAgICAgICAgXFx0XFx0XFx0Y2F0ZWdvcnlJZDogW10sXFxuXFx0ICAgICAgICBcXHRcXHRcXHRjYXRlZ29yeU5hbWU6IFtdLFxcblxcblxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTE6IFtdLFxcblxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTI6IFtdLFxcblxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZTM6IFtdLFxcblxcdCAgICAgICAgXFx0XFx0XFx0ZGF0ZVRpbWVBcnJheTogW10sXFxuXFxuXFx0ICAgICAgICBcXHRcXHRcXHRleHRyYURldGFpbHM6IFtdXFxuXFx0ICAgICAgICBcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KTtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdHZhbGlkYXRlKCkge1xcblxcdFxcdFxcdFxcdHZhciBpc1ZhbGlkID0gdHJ1ZTtcXG5cXG5cXHRcXHRcXHRcXHR0aGlzLmRpc3RpbmN0Q2xpZW50U2VydmljZXMuZm9yRWFjaCgoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdGxldCByZWYgPSAnZm9ybS0nICsgaW5kZXg7XFxuXFxuXFx0XFx0XFx0XFx0XFx0aWYoICEgdGhpcy4kcmVmc1tyZWZdWzBdLnZhbGlkYXRlKGRpc3RpbmN0Q2xpZW50U2VydmljZSkgKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0aXNWYWxpZCA9IGZhbHNlO1xcblxcdFxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHR9KTtcXG5cXG5cXHRcXHRcXHRcXHRyZXR1cm4gaXNWYWxpZDtcXG5cXHRcXHRcXHR9LFxcblxcblxcdFxcdFxcdGFkZE11bHRpcGxlUXVpY2tSZXBvcnQoKSB7XFxuXFx0XFx0XFx0XFx0aWYodGhpcy52YWxpZGF0ZSgpKSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5ncm91cFJlcG9ydCA9IHRoaXMuZ3JvdXByZXBvcnQ7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5ncm91cElkID0gdGhpcy5ncm91cGlkO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2xpZW50c0lkID0gdGhpcy5jbGllbnRzaWQ7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5jbGllbnRTZXJ2aWNlc0lkID0gdGhpcy5jbGllbnRzZXJ2aWNlcztcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnRyYWNraW5ncyA9IHRoaXMudHJhY2tpbmdzO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGlzdGluY3RDbGllbnRTZXJ2aWNlcyA9IHRoaXMuZGlzdGluY3RDbGllbnRTZXJ2aWNlcztcXG5cXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmRpc3RpbmN0Q2xpZW50U2VydmljZXMuZm9yRWFjaCgoZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdFxcdGxldCByZWYgPSAnZm9ybS0nICsgaW5kZXg7XFxuXFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9hY3Rpb25JZCA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZDtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2NhdGVnb3J5SWQgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZDtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2NhdGVnb3J5TmFtZSA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeU5hbWU7XFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9kYXRlMSA9IHRoaXMuJHJlZnNbcmVmXVswXS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5kYXRlMTtcXG5cXHRcXHRcXHRcXHRcXHRcXHRsZXQgX2RhdGUyID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGUyO1xcblxcdFxcdFxcdFxcdFxcdFxcdGxldCBfZGF0ZTMgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZTM7XFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9kYXRlVGltZUFycmF5ID0gdGhpcy4kcmVmc1tyZWZdWzBdLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmRhdGVUaW1lQXJyYXk7XFxuXFx0XFx0XFx0XFx0XFx0XFx0bGV0IF9leHRyYURldGFpbHMgPSB0aGlzLiRyZWZzW3JlZl1bMF0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzO1xcblxcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uYWN0aW9uSWQucHVzaChfYWN0aW9uSWQpO1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2F0ZWdvcnlJZC5wdXNoKF9jYXRlZ29yeUlkKTtcXG5cXHQgICAgICAgIFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uY2F0ZWdvcnlOYW1lLnB1c2goX2NhdGVnb3J5TmFtZSk7XFxuXFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmRhdGUxLnB1c2goX2RhdGUxKTtcXG5cXHQgICAgICAgIFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uZGF0ZTIucHVzaChfZGF0ZTIpO1xcblxcdCAgICAgICAgXFx0XFx0XFx0dGhpcy5tdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS5kYXRlMy5wdXNoKF9kYXRlMyk7XFxuXFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmRhdGVUaW1lQXJyYXkucHVzaChfZGF0ZVRpbWVBcnJheSk7XFxuXFx0ICAgICAgICBcXHRcXHRcXHR0aGlzLm11bHRpcGxlUXVpY2tSZXBvcnRGb3JtLmV4dHJhRGV0YWlscy5wdXNoKF9leHRyYURldGFpbHMpO1xcblxcdFxcdFxcdFxcdFxcdH0pO1xcblxcblxcdFxcdFxcdFxcdFxcdHRoaXMubXVsdGlwbGVRdWlja1JlcG9ydEZvcm0uc3VibWl0KCdwb3N0JywgJy92aXNhL3F1aWNrLXJlcG9ydCcpXFxuXFx0XFx0XFx0ICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdGlmKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHR0aGlzLnJlc2V0TXVsdGlwbGVRdWlja1JlcG9ydEZvcm0oKTtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHRcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHQkKCcjYWRkLXF1aWNrLXJlcG9ydC1tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0dG9hc3RyLnN1Y2Nlc3MocmVzcG9uc2UubWVzc2FnZSk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0XFx0aWYodGhpcy5pc3dyaXRlcmVwb3J0cGFnZSkge1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdFxcdCQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykub24oJ2hpZGRlbi5icy5tb2RhbCcsIGZ1bmN0aW9uIChlKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICBcXHR3aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvdmlzYS9yZXBvcnQvd3JpdGUtcmVwb3J0JztcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHR9KTtcXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHRcXHR9IGVsc2Uge1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdFxcdHNldFRpbWVvdXQoKCkgPT4ge1xcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdFxcdGxvY2F0aW9uLnJlbG9hZCgpO1xcblxcdFxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdH0sIDEwMDApO1xcblxcdFxcdFxcdCAgICAgICAgICAgIFxcdFxcdH1cXG5cXHRcXHRcXHQgICAgICAgICAgICBcXHR9XFxuXFx0XFx0XFx0ICAgICAgICAgICAgfSk7XFxuXFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdH1cXG5cXG5cXHRcXHR9LFxcblxcblxcdFxcdGNyZWF0ZWQoKSB7XFxuXFx0XFx0XFx0dGhpcy5nZXRTZXJ2aWNlRG9jcygpO1xcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y29tcHV0ZWQ6IHtcXG5cXHRcXHRcXHRfaXNXcml0ZVJlcG9ydFBhZ2UoKSB7XFxuXFx0XFx0ICAgICAgXFx0cmV0dXJuIHRoaXMuaXN3cml0ZXJlcG9ydHBhZ2UgfHwgZmFsc2U7XFxuXFx0XFx0ICAgIH0sXFxuXFxuXFx0XFx0XFx0ZGlzdGluY3RDbGllbnRTZXJ2aWNlcygpIHtcXG5cXHRcXHRcXHRcXHRsZXQgZGlzdGluY3RDbGllbnRTZXJ2aWNlcyA9IFtdO1xcblxcblxcdFxcdFxcdFxcdHRoaXMuY2xpZW50c2VydmljZXMuZm9yRWFjaChjbGllbnRTZXJ2aWNlID0+IHtcXG5cXHRcXHRcXHRcXHRcXHRpZihkaXN0aW5jdENsaWVudFNlcnZpY2VzLmluZGV4T2YoY2xpZW50U2VydmljZS5kZXRhaWwpID09IC0xKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0ZGlzdGluY3RDbGllbnRTZXJ2aWNlcy5wdXNoKGNsaWVudFNlcnZpY2UuZGV0YWlsKTtcXG5cXHRcXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0XFx0fSk7XFxuXFxuXFx0XFx0XFx0XFx0cmV0dXJuIGRpc3RpbmN0Q2xpZW50U2VydmljZXM7XFxuXFx0XFx0XFx0fVxcblxcdFxcdH0sXFxuXFxuXFx0XFx0Y29tcG9uZW50czoge1xcblxcdCAgICAgICAgJ211bHRpcGxlLXF1aWNrLXJlcG9ydC1mb3JtJzogcmVxdWlyZSgnLi9NdWx0aXBsZVF1aWNrUmVwb3J0Rm9ybS52dWUnKVxcblxcdCAgICB9XFxuXFxuXFx0fVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMGYyZjJiNzdcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTJcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsIlxuLyogc3R5bGVzICovXG5yZXF1aXJlKFwiISF2dWUtc3R5bGUtbG9hZGVyIWNzcy1sb2FkZXI/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTBmMmYyYjc3XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXN0eWxlcyZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTBmMmYyYjc3XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIFwiZGF0YS12LTBmMmYyYjc3XCIsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBNdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0wZjJmMmI3N1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTBmMmYyYjc3XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTRcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZWIyZmRhNGFcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWViMmZkYTRhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtZWIyZmRhNGFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9WaXNhL011bHRpcGxlUXVpY2tSZXBvcnRGb3JtLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTVcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmFkZE11bHRpcGxlUXVpY2tSZXBvcnQoJGV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW9zLXNjcm9sbFwiXG4gIH0sIF92bS5fbCgoX3ZtLmRpc3RpbmN0Q2xpZW50U2VydmljZXMubGVuZ3RoKSwgZnVuY3Rpb24oZGlzdGluY3RDbGllbnRTZXJ2aWNlLCBpbmRleCkge1xuICAgIHJldHVybiBfYygnZGl2JywgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWhlYWRpbmdcIlxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5kaXN0aW5jdENsaWVudFNlcnZpY2VzW2luZGV4XSkgKyBcIlxcbiAgICAgICAgICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIlxuICAgIH0sIFtfYygnbXVsdGlwbGUtcXVpY2stcmVwb3J0LWZvcm0nLCB7XG4gICAgICBrZXk6IGluZGV4LFxuICAgICAgcmVmOiAnZm9ybS0nICsgaW5kZXgsXG4gICAgICByZWZJbkZvcjogdHJ1ZSxcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiZG9jc1wiOiBfdm0uZG9jcyxcbiAgICAgICAgXCJzZXJ2aWNlZG9jc1wiOiBfdm0uc2VydmljZURvY3MsXG4gICAgICAgIFwiaW5kZXhcIjogaW5kZXhcbiAgICAgIH1cbiAgICB9KV0sIDEpXSldKVxuICB9KSwgMCksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDcmVhdGUgUmVwb3J0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTBmMmYyYjc3XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMGYyZjJiNzdcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTZcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2JywgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwibmFtZVwiOiBcImFjdGlvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjaGFuZ2VcIjogW2Z1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRzZWxlY3RlZFZhbCA9IEFycmF5LnByb3RvdHlwZS5maWx0ZXIuY2FsbCgkZXZlbnQudGFyZ2V0Lm9wdGlvbnMsIGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICByZXR1cm4gby5zZWxlY3RlZFxuICAgICAgICB9KS5tYXAoZnVuY3Rpb24obykge1xuICAgICAgICAgIHZhciB2YWwgPSBcIl92YWx1ZVwiIGluIG8gPyBvLl92YWx1ZSA6IG8udmFsdWU7XG4gICAgICAgICAgcmV0dXJuIHZhbFxuICAgICAgICB9KTtcbiAgICAgICAgX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImFjdGlvbklkXCIsICRldmVudC50YXJnZXQubXVsdGlwbGUgPyAkJHNlbGVjdGVkVmFsIDogJCRzZWxlY3RlZFZhbFswXSlcbiAgICAgIH0sIF92bS5nZXRDYXRlZ29yaWVzXVxuICAgIH1cbiAgfSwgX3ZtLl9sKChfdm0uYWN0aW9ucyksIGZ1bmN0aW9uKGFjdGlvbikge1xuICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBhY3Rpb24uaWRcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0ICAgICAgICAgICBcXHRcXHRcIiArIF92bS5fcyhhY3Rpb24ubmFtZSkgKyBcIlxcblxcdFxcdCAgICAgICAgICAgIFwiKV0pXG4gIH0pLCAwKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDIgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDMgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDggfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDkgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEwIHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMSB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTQgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE1IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNiksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMiB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAzIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDggfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gOSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNSB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNlwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWRcIlxuICAgIH0sIHtcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoIV92bS5jdXN0b21DYXRlZ29yeSksXG4gICAgICBleHByZXNzaW9uOiBcIiFjdXN0b21DYXRlZ29yeVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwibmFtZVwiOiBcImNhdGVnb3J5XCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBbZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIHZhciAkJHNlbGVjdGVkVmFsID0gQXJyYXkucHJvdG90eXBlLmZpbHRlci5jYWxsKCRldmVudC50YXJnZXQub3B0aW9ucywgZnVuY3Rpb24obykge1xuICAgICAgICAgIHJldHVybiBvLnNlbGVjdGVkXG4gICAgICAgIH0pLm1hcChmdW5jdGlvbihvKSB7XG4gICAgICAgICAgdmFyIHZhbCA9IFwiX3ZhbHVlXCIgaW4gbyA/IG8uX3ZhbHVlIDogby52YWx1ZTtcbiAgICAgICAgICByZXR1cm4gdmFsXG4gICAgICAgIH0pO1xuICAgICAgICBfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiY2F0ZWdvcnlJZFwiLCAkZXZlbnQudGFyZ2V0Lm11bHRpcGxlID8gJCRzZWxlY3RlZFZhbCA6ICQkc2VsZWN0ZWRWYWxbMF0pXG4gICAgICB9LCBfdm0uY2hhbmdlQ2F0ZWdvcnlIYW5kbGVyXVxuICAgIH1cbiAgfSwgX3ZtLl9sKChfdm0uY2F0ZWdvcmllcyksIGZ1bmN0aW9uKGNhdGVnb3J5KSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IGNhdGVnb3J5LmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICAgICAgICAgIFxcdFwiICsgX3ZtLl9zKGNhdGVnb3J5Lm5hbWUpICsgXCJcXG5cXHRcXHQgICAgICAgICAgICBcIildKVxuICB9KSwgMCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDkpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA5XCJcbiAgICB9XVxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZSksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5TmFtZVwiXG4gICAgfSwge1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uY3VzdG9tQ2F0ZWdvcnkpLFxuICAgICAgZXhwcmVzc2lvbjogXCJjdXN0b21DYXRlZ29yeVwiXG4gICAgfV0sXG4gICAgcmVmOiBcImNhdGVnb3J5TmFtZVwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcInBsYWNlaG9sZGVyXCI6IFwiRW50ZXIgY2F0ZWdvcnlcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeU5hbWUpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiY2F0ZWdvcnlOYW1lXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCIxMHB4XCIsXG4gICAgICBcImNsZWFyXCI6IFwiYm90aFwiXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXNtIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uY3VzdG9tQ2F0ZWdvcnkgPSAhX3ZtLmN1c3RvbUNhdGVnb3J5O1xuICAgICAgICBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9IG51bGxcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6ICghX3ZtLmN1c3RvbUNhdGVnb3J5KSxcbiAgICAgIGV4cHJlc3Npb246IFwiIWN1c3RvbUNhdGVnb3J5XCJcbiAgICB9XVxuICB9LCBbX3ZtLl92KFwiVXNlIEN1c3RvbSBDYXRlZ29yeVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uY3VzdG9tQ2F0ZWdvcnkpLFxuICAgICAgZXhwcmVzc2lvbjogXCJjdXN0b21DYXRlZ29yeVwiXG4gICAgfV1cbiAgfSwgW192bS5fdihcIlVzZSBQcmVkZWZpbmVkIENhdGVnb3J5XCIpXSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMSksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDFcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDIpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDMpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUyXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUyXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRTY2hlZHVsZWQgSGVhcmluZyBEYXRlIGFuZCBUaW1lOlxcblxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX3ZtLl9sKChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZGF0ZVRpbWVBcnJheSksIGZ1bmN0aW9uKGRhdGVUaW1lRWxlbWVudCwgaW5kZXgpIHtcbiAgICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTFcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiLFxuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJtYXJnaW4tYm90dG9tXCI6IFwiMTBweFwiXG4gICAgICB9XG4gICAgfSwgW192bS5fbSg0LCB0cnVlKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgdmFsdWU6IChkYXRlVGltZUVsZW1lbnQudmFsdWUpLFxuICAgICAgICBleHByZXNzaW9uOiBcImRhdGVUaW1lRWxlbWVudC52YWx1ZVwiXG4gICAgICB9XSxcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRldGltZXBpY2tlcmFycmF5XCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICAgIFwibmFtZVwiOiBcImRhdGVUaW1lQXJyYXlbXVwiLFxuICAgICAgICBcInBsYWNlaG9sZGVyXCI6IFwieXl5eS1tbS1kZCBoaDptbTpzc1wiLFxuICAgICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgICB9LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiAoZGF0ZVRpbWVFbGVtZW50LnZhbHVlKVxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICAgIF92bS4kc2V0KGRhdGVUaW1lRWxlbWVudCwgXCJ2YWx1ZVwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEgdGV4dC1jZW50ZXJcIlxuICAgIH0sIFtfYygnYnV0dG9uJywge1xuICAgICAgZGlyZWN0aXZlczogW3tcbiAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgIHZhbHVlOiAoaW5kZXggIT0gMCksXG4gICAgICAgIGV4cHJlc3Npb246IFwiaW5kZXggIT0gMFwiXG4gICAgICB9XSxcbiAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGFuZ2VyIGJ0bi1zbVwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS5yZW1vdmVEYXRlVGltZVBpY2tlckFycmF5KGluZGV4KVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW19jKCdpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtdGltZXNcIlxuICAgIH0pXSldKV0pXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImhlaWdodFwiOiBcIjEwcHhcIixcbiAgICAgIFwiY2xlYXJcIjogXCJib3RoXCJcbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBidG4tc20gcHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLmFkZERhdGVUaW1lUGlja2VyQXJyYXlcbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1wbHVzXCJcbiAgfSksIF92bS5fdihcIiBBZGRcXG4gICAgICAgICAgICAgICAgICAgIFwiKV0pXSwgMildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA2IHx8IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDcgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTAgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTQgfHwgX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDYgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gMTAgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxNCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDE1XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRTY2hlZHVsZWQgQXBwb2ludG1lbnQgRGF0ZSBhbmQgVGltZTpcXG5cXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oNSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTNcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXRpbWVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUzXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAyKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMlwiXG4gICAgfV1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDMpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAzXCJcbiAgICB9XVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNCksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDRcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDYpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnc2VsZWN0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNob3Nlbi1zZWxlY3QtZm9yLWRvY3MtMlwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIndpZHRoXCI6IFwiMzUwcHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ2Nob3Nlbi0nICsgX3ZtLmluZGV4LFxuICAgICAgXCJkYXRhLXBsYWNlaG9sZGVyXCI6IFwiU2VsZWN0IERvY3NcIixcbiAgICAgIFwibXVsdGlwbGVcIjogXCJcIixcbiAgICAgIFwidGFiaW5kZXhcIjogXCI0XCJcbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLnNlcnZpY2Vkb2NzKSwgZnVuY3Rpb24oc2VydmljZWRvYykge1xuICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBzZXJ2aWNlZG9jLnRpdGxlXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICAgICAgICAgICAgICBcXHRcIiArIF92bS5fcygoc2VydmljZWRvYy50aXRsZSkudHJpbSgpKSArIFwiXFxuXFx0XFx0ICAgICAgICAgICAgICAgIFwiKV0pXG4gIH0pLCAwKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNiksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDZcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDcpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDgpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGU0XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGU0XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRDb25zZXF1ZW5jZSBvZiBub24gc3VibWlzc2lvbjpcXG5cXHRcXHRcXHQgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCBbX2MoJ2xhYmVsJywgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHNcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiLFxuICAgICAgXCJ2YWx1ZVwiOiBcIkFkZGl0aW9uYWwgY29zdCBtYXliZSBjaGFyZ2VkLlwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IEFycmF5LmlzQXJyYXkoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscykgPyBfdm0uX2koX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscywgXCJBZGRpdGlvbmFsIGNvc3QgbWF5YmUgY2hhcmdlZC5cIikgPiAtMSA6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRhID0gX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscyxcbiAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAkJGMgPSAkJGVsLmNoZWNrZWQgPyAodHJ1ZSkgOiAoZmFsc2UpO1xuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSgkJGEpKSB7XG4gICAgICAgICAgdmFyICQkdiA9IFwiQWRkaXRpb25hbCBjb3N0IG1heWJlIGNoYXJnZWQuXCIsXG4gICAgICAgICAgICAkJGkgPSBfdm0uX2koJCRhLCAkJHYpO1xuICAgICAgICAgIGlmICgkJGVsLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICQkaSA8IDAgJiYgKF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRhLmNvbmNhdChbJCR2XSkpKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkJGkgPiAtMSAmJiAoX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGEuc2xpY2UoMCwgJCRpKS5jb25jYXQoJCRhLnNsaWNlKCQkaSArIDEpKSkpXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRjKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgIFxcdFxcdFxcdEFkZGl0aW9uYWwgY29zdCBtYXliZSBjaGFyZ2VkLlxcblxcdFxcdFxcdCAgICBcXHRcXHRcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2JywgW19jKCdsYWJlbCcsIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiY2hlY2tib3hcIixcbiAgICAgIFwidmFsdWVcIjogXCJBcHBsaWNhdGlvbiBtYXliZSBmb3JmZWl0ZWQgYW5kIGhhcyB0byByZS1hcHBseS5cIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBBcnJheS5pc0FycmF5KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpID8gX3ZtLl9pKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMsIFwiQXBwbGljYXRpb24gbWF5YmUgZm9yZmVpdGVkIGFuZCBoYXMgdG8gcmUtYXBwbHkuXCIpID4gLTEgOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscylcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNoYW5nZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkYSA9IF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMsXG4gICAgICAgICAgJCRlbCA9ICRldmVudC50YXJnZXQsXG4gICAgICAgICAgJCRjID0gJCRlbC5jaGVja2VkID8gKHRydWUpIDogKGZhbHNlKTtcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgIHZhciAkJHYgPSBcIkFwcGxpY2F0aW9uIG1heWJlIGZvcmZlaXRlZCBhbmQgaGFzIHRvIHJlLWFwcGx5LlwiLFxuICAgICAgICAgICAgJCRpID0gX3ZtLl9pKCQkYSwgJCR2KTtcbiAgICAgICAgICBpZiAoJCRlbC5jaGVja2VkKSB7XG4gICAgICAgICAgICAkJGkgPCAwICYmIChfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYS5jb25jYXQoWyQkdl0pKSlcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCRpID4gLTEgJiYgKF92bS4kc2V0KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybSwgXCJleHRyYURldGFpbHNcIiwgJCRhLnNsaWNlKDAsICQkaSkuY29uY2F0KCQkYS5zbGljZSgkJGkgKyAxKSkpKVxuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYylcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIlxcblxcdFxcdFxcdCAgICBcXHRcXHRcXHRBcHBsaWNhdGlvbiBtYXliZSBmb3JmZWl0ZWQgYW5kIGhhcyB0byByZS1hcHBseS5cXG5cXHRcXHRcXHQgICAgXFx0XFx0XCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIFtfYygnbGFiZWwnLCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscyksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlsc1wiXG4gICAgfV0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImNoZWNrYm94XCIsXG4gICAgICBcInZhbHVlXCI6IFwiSW5pdGlhbCBkZXBvc2l0IG1heWJlIGZvcmZlaXRlZC5cIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBBcnJheS5pc0FycmF5KF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMpID8gX3ZtLl9pKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5leHRyYURldGFpbHMsIFwiSW5pdGlhbCBkZXBvc2l0IG1heWJlIGZvcmZlaXRlZC5cIikgPiAtMSA6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uZXh0cmFEZXRhaWxzKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRhID0gX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmV4dHJhRGV0YWlscyxcbiAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAkJGMgPSAkJGVsLmNoZWNrZWQgPyAodHJ1ZSkgOiAoZmFsc2UpO1xuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSgkJGEpKSB7XG4gICAgICAgICAgdmFyICQkdiA9IFwiSW5pdGlhbCBkZXBvc2l0IG1heWJlIGZvcmZlaXRlZC5cIixcbiAgICAgICAgICAgICQkaSA9IF92bS5faSgkJGEsICQkdik7XG4gICAgICAgICAgaWYgKCQkZWwuY2hlY2tlZCkge1xuICAgICAgICAgICAgJCRpIDwgMCAmJiAoX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGEuY29uY2F0KFskJHZdKSkpXG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQkaSA+IC0xICYmIChfdm0uJHNldChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0sIFwiZXh0cmFEZXRhaWxzXCIsICQkYS5zbGljZSgwLCAkJGkpLmNvbmNhdCgkJGEuc2xpY2UoJCRpICsgMSkpKSlcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgX3ZtLiRzZXQoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLCBcImV4dHJhRGV0YWlsc1wiLCAkJGMpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgXFx0XFx0XFx0SW5pdGlhbCBkZXBvc2l0IG1heWJlIGZvcmZlaXRlZC5cXG5cXHRcXHRcXHQgICAgXFx0XFx0XCIpXSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gNyksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDdcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDkpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDEwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlNVwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRldGltZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTVcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDgpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA4XCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgxMSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZGF0ZXJhbmdlIGlucHV0LWdyb3VwXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTZcIixcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTZcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfdm0uX3YoXCJ0b1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGU3XCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtc20gZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGU3XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRUYXJnZXQgRmlsbGluZyBEYXRlOlxcblxcdFxcdFxcdCAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgxMiksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZThcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZThcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDkpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSA5XCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgxMyksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZGF0ZXJhbmdlIGlucHV0LWdyb3VwXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTlcIixcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTlcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfdm0uX3YoXCJ0b1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUxMFwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LXNtIGZvcm0tY29udHJvbCBkYXRlcGlja2VyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiaWRcIjogXCJkYXRlMTBcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9XG4gIH0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEwKSxcbiAgICAgIGV4cHJlc3Npb246IFwibXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTBcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDE0KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cCBkYXRlXCJcbiAgfSwgW192bS5fbSgxNSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICByZWY6IFwiZGF0ZTExXCIsXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sIGRhdGVwaWNrZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJpZFwiOiBcImRhdGUxMVwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH1cbiAgfSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uYWN0aW9uSWQgPT0gMTEpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMTYpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDE3KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlMTJcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTEyXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMiksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEyXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgxOCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMTkpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUxM1wiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRldGltZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTEzXCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxMyksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDEzXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgyMCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMjEpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUxNFwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRldGltZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTE0XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNCksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE0XCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSAxMjgpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDEyOFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMjIpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnc2VsZWN0Jywge1xuICAgIHJlZjogXCJjb21wYW55X2NvdXJpZXJcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImNvbXBhbnlfY291cmllclwiXG4gICAgfVxuICB9LCBfdm0uX2woKF92bS5jb21wYW55Q291cmllcnMpLCBmdW5jdGlvbihjb21wYW55Q291cmllcikge1xuICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiBjb21wYW55Q291cmllci5hZGRyZXNzLmNvbnRhY3RfbnVtYmVyXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgXFx0XFx0XCIgKyBfdm0uX3MoY29tcGFueUNvdXJpZXIuZmlyc3RfbmFtZSArICcgJyArIGNvbXBhbnlDb3VyaWVyLmxhc3RfbmFtZSkgKyBcIiBcXG5cXHRcXHRcXHQgICAgICAgIFxcdFxcdChcIiArIF92bS5fcyhjb21wYW55Q291cmllci5hZGRyZXNzLmNvbnRhY3RfbnVtYmVyKSArIFwiKVxcblxcdFxcdFxcdCAgICAgICAgXFx0XCIpXSlcbiAgfSksIDApXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCAhPSAxMjgpLFxuICAgICAgZXhwcmVzc2lvbjogXCJtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkICE9IDEyOFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMjMpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcInRyYWNraW5nX251bWJlclwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwidHJhY2tpbmdfbnVtYmVyXCJcbiAgICB9XG4gIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgfSwgW192bS5fbSgyNCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgZGF0ZVwiXG4gIH0sIFtfdm0uX20oMjUpLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgcmVmOiBcImRhdGUxNVwiLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBkYXRldGltZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTE1XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5tdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5hY3Rpb25JZCA9PSAxNSksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmFjdGlvbklkID09IDE1XCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3NiB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3NyB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3OCB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3OSB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MCB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MSB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MiB8fCBfdm0ubXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MyksXG4gICAgICBleHByZXNzaW9uOiBcIm11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzYgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA3NyB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDc4IHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gNzkgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4MCB8fCBtdWx0aXBsZVF1aWNrUmVwb3J0Zm9ybS5jYXRlZ29yeUlkID09IDgxIHx8IG11bHRpcGxlUXVpY2tSZXBvcnRmb3JtLmNhdGVnb3J5SWQgPT0gODIgfHwgbXVsdGlwbGVRdWlja1JlcG9ydGZvcm0uY2F0ZWdvcnlJZCA9PSA4M1wiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfdm0uX20oMjYpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwIGRhdGVcIlxuICB9LCBbX3ZtLl9tKDI3KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIHJlZjogXCJkYXRlMTZcIixcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgZGF0ZXBpY2tlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwiZGF0ZTE2XCIsXG4gICAgICBcImF1dG9jb21wbGV0ZVwiOiBcIm9mZlwiXG4gICAgfVxuICB9KV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgIEFjdGlvbjogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICAgICAgQ2F0ZWdvcnk6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RXN0aW1hdGVkIFJlbGVhc2luZyBEYXRlOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdERvY3VtZW50czogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRTdWJtaXNzaW9uIERhdGU6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RXN0aW1hdGVkIFRpbWUgb2YgRmluaXNoaW5nOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdEFmZmVjdGVkIERhdGU6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RGF0ZSBSYW5nZTogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRFeHRlbmRlZCBwZXJpb2QgZm9yIHByb2Nlc3Npbmc6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0RXN0aW1hdGVkIFJlbGVhc2luZyBEYXRlOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIlxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdERhdGUgYW5kIFRpbWUgUGljayBVcDogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHREYXRlIGFuZCBUaW1lIERlbGl2ZXI6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHQgICAgICAgXFx0Q29tcGFueSBDb3VyaWVyOiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZGFuZ2VyXCJcbiAgfSwgW192bS5fdihcIipcIildKV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0ICAgICAgIFxcdFRyYWNraW5nIE51bWJlcjogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHREYXRlIGFuZCBUaW1lIERlbGl2ZXJlZDogXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlclwiXG4gIH0sIFtfdm0uX3YoXCIqXCIpXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWNhbGVuZGFyXCJcbiAgfSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICBcXHRFc3RpbWF0ZWQgUmVsZWFzaW5nIERhdGU6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiXG4gIH0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1lYjJmZGE0YVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWViMmZkYTRhXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydEZvcm0udnVlXG4vLyBtb2R1bGUgaWQgPSAxOFxuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiA5IiwiVnVlLmNvbXBvbmVudCgnZGlhbG9nLW1vZGFsJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlJykpO1xuXG5sZXQgZGF0YSA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XG5cbnZhciB3cml0ZVJlcG9ydCA9IG5ldyBWdWUoe1xuXHRlbDonI3dyaXRlLXJlcG9ydCcsXG5cdGRhdGE6e1xuXHRcdGNsaWVudHM6W10sXG4gICAgICAgIHNlcnZpY2VzOltdLFxuICAgICAgICBzZWxlY3RlZDpbXSxcbiAgICAgICAgcmVwb3J0Rm9ybTpuZXcgRm9ybSh7XG4gICAgICAgICAgaWQ6ICcnLFxuICAgICAgICAgIG1lc3NhZ2U6ICcnLFxuICAgICAgICB9LCB7IGJhc2VVUkw6ICdodHRwOi8vJyt3aW5kb3cuTGFyYXZlbC5jcGFuZWxfdXJsIH0pLFxuXG4gICAgICAgIHF1aWNrUmVwb3J0Q2xpZW50c0lkOiBbXSxcbiAgICAgICAgcXVpY2tSZXBvcnRDbGllbnRTZXJ2aWNlc0lkOiBbXSxcbiAgICAgICAgcXVpY2tSZXBvcnRUcmFja2luZ3M6IFtdLFxuXG4gICAgICAgIHNldHRpbmc6IGRhdGEuYWNjZXNzX2NvbnRyb2xbMF0uc2V0dGluZyxcbiAgICAgICAgcGVybWlzc2lvbnM6IGRhdGEucGVybWlzc2lvbnMsXG4gICAgICAgIHNlbHBlcm1pc3Npb25zOiBbe1xuICAgICAgICAgICAgY29udGludWU6MFxuICAgICAgICB9XVxuXHR9LFxuXHRtZXRob2RzOiB7XG5cdFx0YWRkQ2xpZW50KCl7XG5cdFx0dmFyIGNsaWRzID0gJChcIiNzZWxTZXJ2aWNlXCIpLmF0dHIoXCJjbC1pZHNcIik7XG5cdFx0ICAgICAgaWYoY2xpZHM9PVwiXCIpe1xuXHRcdCAgICAgICAgdG9hc3RyLmVycm9yKFwiUGxlYXNlIHNlbGVjdCBjbGllbnQgZmlyc3QhXCIpO1xuXHRcdCAgICAgIH1cblx0XHQgICAgICBlbHNle1xuXHRcdFx0ICAgIHZhciBocmVmID0gXCIvdmlzYS9yZXBvcnQvc2VsZWN0LXNlcnZpY2VzL1wiK2NsaWRzO1xuXHRcdFx0ICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gaHJlZjtcblx0XHQgICAgfVxuXHRcdH0sXG4gICAgICAgIHNob3dTZXJ2aWNlcyhpZCwgZSkge1xuICAgICAgICAgICAgaWYoZS50YXJnZXQubm9kZU5hbWUgIT0gJ0JVVFRPTicgJiYgZS50YXJnZXQubm9kZU5hbWUgIT0gJ1NQQU4nKSB7XG4gICAgICAgICAgICAgICAgJCgnI2NsaWVudC0nICsgaWQpLnRvZ2dsZUNsYXNzKCdoaWRlJyk7XG4gICAgICAgICAgICAgICAgJCgnI2ZhLScgKyBpZCkudG9nZ2xlQ2xhc3MoJ2ZhLWFycm93LWRvd24nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgc2VsZWN0U2VydmljZXMoaXRlbSl7XG4gICAgICAgICAgICBpZih0aGlzLnNlbGVjdGVkLmluZGV4T2YoaXRlbSk+LTEpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaW5keCA9IHRoaXMuc2VsZWN0ZWQuaW5kZXhPZihpdGVtKTtcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkLnNwbGljZShpbmR4LCAxKTtcblxuICAgICAgICAgICAgfWVsc2VcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkLnB1c2goaXRlbSk7IFxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBnZXRRdWlja1JlcG9ydENsaWVudElkc0FycmF5KGNsaWVudFNlcnZpY2VzKSB7XG4gICAgICAgICAgICBsZXQgY2xpZW50SWRzID0gW107XG4gICAgICAgICAgICBjbGllbnRTZXJ2aWNlcy5mb3JFYWNoKGNsaWVudFNlcnZpY2UgPT4ge1xuICAgICAgICAgICAgICAgIGlmKGNsaWVudElkcy5pbmRleE9mKGNsaWVudFNlcnZpY2UuY2xpZW50X2lkKSA9PSAtMSlcbiAgICAgICAgICAgICAgICAgICAgY2xpZW50SWRzLnB1c2goY2xpZW50U2VydmljZS5jbGllbnRfaWQpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHJldHVybiBjbGllbnRJZHM7XG4gICAgICAgIH0sXG4gICAgICAgIGdldFF1aWNrUmVwb3J0VHJhY2tpbmdzQXJyYXkoY2xpZW50U2VydmljZXMpIHtcbiAgICAgICAgICAgIGxldCB0cmFja2luZ3MgPSBbXTtcbiAgICAgICAgICAgIGNsaWVudFNlcnZpY2VzLmZvckVhY2goY2xpZW50U2VydmljZSA9PiB7XG4gICAgICAgICAgICAgICAgaWYodHJhY2tpbmdzLmluZGV4T2YoY2xpZW50U2VydmljZS50cmFja2luZykgPT0gLTEpXG4gICAgICAgICAgICAgICAgICAgIHRyYWNraW5ncy5wdXNoKGNsaWVudFNlcnZpY2UudHJhY2tpbmcpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHJldHVybiB0cmFja2luZ3M7XG4gICAgICAgIH0sXG4gICAgICAgIHNob3dSZXBvcnQoKXtcbiAgICAgICAgICAgIGlmKHRoaXMuc2VsZWN0ZWQubGVuZ3RoIT0wKXtcbiAgICAgICAgICAgICAgICB2YXIgaWQgPSAnJ1xuICAgICAgICAgICAgICAgICQuZWFjaCggdGhpcy5zZWxlY3RlZCwgZnVuY3Rpb24oIGtleSwgdmFsdWUgKSB7XG4gICAgICAgICAgICAgICAgICBpZCA9IHZhbHVlLmlkICsgJzsnICsgaWQ7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXBvcnRGb3JtLmlkPWlkO1xuICAgICAgICAgICAgICAgIC8vICQoJyNyZXBvcnRzJykubW9kYWwoJ3RvZ2dsZScpO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5xdWlja1JlcG9ydENsaWVudHNJZCA9IHRoaXMuZ2V0UXVpY2tSZXBvcnRDbGllbnRJZHNBcnJheSh0aGlzLnNlbGVjdGVkKTtcbiAgICAgICAgICAgICAgICB0aGlzLnF1aWNrUmVwb3J0Q2xpZW50U2VydmljZXNJZCA9IHRoaXMuc2VsZWN0ZWQ7XG4gICAgICAgICAgICAgICAgdGhpcy5xdWlja1JlcG9ydFRyYWNraW5ncyA9IHRoaXMuZ2V0UXVpY2tSZXBvcnRUcmFja2luZ3NBcnJheSh0aGlzLnNlbGVjdGVkKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLm11bHRpcGxlcXVpY2tyZXBvcnRyZWYuZmV0Y2hEb2NzKHRoaXMuc2VsZWN0ZWQpO1xuXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykubW9kYWwoJ3Nob3cnKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBzd2FsKHtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHRpdGxlOiBcIkRvIHlvdSB3YW50IHRvIGNyZWF0ZSBhIHJlcG9ydD9cIixcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHR5cGU6ICdpbmZvJyxcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHNob3dDYW5jZWxCdXR0b246IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICBjb25maXJtQnV0dG9uQ29sb3I6ICcjMzA4NWQ2JyxcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIGNhbmNlbEJ1dHRvbkNvbG9yOiAnI2QzMycsXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICBjb25maXJtQnV0dG9uVGV4dDogJ1lFUydcbiAgICAgICAgICAgICAgICAgICAgLy8gfSlcbiAgICAgICAgICAgICAgICAgICAgLy8gLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICQoJyNhZGQtcXVpY2stcmVwb3J0LW1vZGFsJykubW9kYWwoJ3Nob3cnKTtcbiAgICAgICAgICAgICAgICAgICAgLy8gfSlcbiAgICAgICAgICAgICAgICAgICAgLy8gLmNhdGNoKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgJCgnYm9keScpLmNzcyhcInBhZGRpbmctcmlnaHRcIiwgXCIwcHhcIik7XG4gICAgICAgICAgICAgICAgfSwgMTAwMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKFwiUGxlYXNlIHNlbGVjdCBzZXJ2aWNlIGZpcnN0IVwiKTtcbiAgICAgICAgfSxcbiAgICAgICAgc2F2ZVJlcG9ydCgpe1xuICAgICAgICAgICAgLy8gdGhpcy5yZXBvcnRGb3JtLnN1Ym1pdCgncG9zdCcsICcvdmlzYS9yZXBvcnQvc2F2ZS1yZXBvcnQnKVxuICAgICAgICAgICAgLy8gLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICAgIC8vICAgICB0b2FzdHIuc3VjY2VzcygnTmV3IFJlcG9ydCBBZGRlZC4nKTtcbiAgICAgICAgICAgIC8vICAgICAkKCcjcmVwb3J0cycpLm1vZGFsKCd0b2dnbGUnKTtcbiAgICAgICAgICAgIC8vICAgICB0aGlzLnNlbGVjdGVkID0gW107XG4gICAgICAgICAgICAvLyAgICAgdGhpcy5yZXBvcnRGb3JtLmlkID0gJyc7XG4gICAgICAgICAgICAvLyAgICAgdGhpcy5yZXBvcnRGb3JtLm1lc3NhZ2UgPSAnJztcbiAgICAgICAgICAgIC8vICAgICAkKCdpbnB1dDpjaGVja2JveCcpLnByb3AoJ2NoZWNrZWQnLGZhbHNlKTtcbiAgICAgICAgICAgIC8vIH0pO1xuXG4gICAgICAgIH1cblxuXHR9LFxuXHRjcmVhdGVkKCl7XG5cdCAgICBheGlvcy5nZXQoJy92aXNhL3JlcG9ydC9nZXQtY2xpZW50cy8nK3dpbmRvdy5MYXJhdmVsLmRhdGEpXG5cdCAgICAudGhlbihyZXNwb25zZSA9PiB7XG5cdCAgICAgICAgdGhpcy5jbGllbnRzID0gcmVzcG9uc2UuZGF0YTtcblx0ICAgIH0pO1xuXG4gICAgICAgIHZhciB4ID0gdGhpcy5wZXJtaXNzaW9ucy5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAnUmVwb3J0cyd9KTtcblxuICAgICAgICB2YXIgeSA9IHguZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoubmFtZSA9PT0gJ0NvbnRpbnVlJ30pO1xuXG4gICAgICAgIGlmKHkubGVuZ3RoPT0wKSB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbnRpbnVlID0gMDtcbiAgICAgICAgZWxzZSB0aGlzLnNlbHBlcm1pc3Npb25zLmNvbnRpbnVlID0gMTtcblx0fSxcbiAgICBjb21wb25lbnRzOiB7XG4gICAgICAgICdtdWx0aXBsZS1xdWljay1yZXBvcnQnOiByZXF1aXJlKCcuLi8uLi9jb21wb25lbnRzL1Zpc2EvTXVsdGlwbGVRdWlja1JlcG9ydC52dWUnKVxuICAgIH1cbn0pO1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcblxuICAgICQoJyNsaXN0cycpLkRhdGFUYWJsZSh7XG4gICAgXHRcImFqYXhcIjogXCIvc3RvcmFnZS9kYXRhL0NsaWVudHNSZXBvcnQudHh0XCIsXG4gICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG4gICAgICAgIFwibGVuZ3RoTWVudVwiOiBbWzEwLCAyNSwgNTBdLCBbMTAsIDI1LCA1MF1dLFxuICAgICAgICBcImlEaXNwbGF5TGVuZ3RoXCI6IDEwXG4gICAgfSk7XG5cbn0gKTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL3JlcG9ydHMvaW5kZXguanMiLCIvKlxyXG5cdE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXHJcblx0QXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxyXG4qL1xyXG4vLyBjc3MgYmFzZSBjb2RlLCBpbmplY3RlZCBieSB0aGUgY3NzLWxvYWRlclxyXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKCkge1xyXG5cdHZhciBsaXN0ID0gW107XHJcblxyXG5cdC8vIHJldHVybiB0aGUgbGlzdCBvZiBtb2R1bGVzIGFzIGNzcyBzdHJpbmdcclxuXHRsaXN0LnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XHJcblx0XHR2YXIgcmVzdWx0ID0gW107XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHR2YXIgaXRlbSA9IHRoaXNbaV07XHJcblx0XHRcdGlmKGl0ZW1bMl0pIHtcclxuXHRcdFx0XHRyZXN1bHQucHVzaChcIkBtZWRpYSBcIiArIGl0ZW1bMl0gKyBcIntcIiArIGl0ZW1bMV0gKyBcIn1cIik7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0cmVzdWx0LnB1c2goaXRlbVsxXSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiByZXN1bHQuam9pbihcIlwiKTtcclxuXHR9O1xyXG5cclxuXHQvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxyXG5cdGxpc3QuaSA9IGZ1bmN0aW9uKG1vZHVsZXMsIG1lZGlhUXVlcnkpIHtcclxuXHRcdGlmKHR5cGVvZiBtb2R1bGVzID09PSBcInN0cmluZ1wiKVxyXG5cdFx0XHRtb2R1bGVzID0gW1tudWxsLCBtb2R1bGVzLCBcIlwiXV07XHJcblx0XHR2YXIgYWxyZWFkeUltcG9ydGVkTW9kdWxlcyA9IHt9O1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGlkID0gdGhpc1tpXVswXTtcclxuXHRcdFx0aWYodHlwZW9mIGlkID09PSBcIm51bWJlclwiKVxyXG5cdFx0XHRcdGFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaWRdID0gdHJ1ZTtcclxuXHRcdH1cclxuXHRcdGZvcihpID0gMDsgaSA8IG1vZHVsZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGl0ZW0gPSBtb2R1bGVzW2ldO1xyXG5cdFx0XHQvLyBza2lwIGFscmVhZHkgaW1wb3J0ZWQgbW9kdWxlXHJcblx0XHRcdC8vIHRoaXMgaW1wbGVtZW50YXRpb24gaXMgbm90IDEwMCUgcGVyZmVjdCBmb3Igd2VpcmQgbWVkaWEgcXVlcnkgY29tYmluYXRpb25zXHJcblx0XHRcdC8vICB3aGVuIGEgbW9kdWxlIGlzIGltcG9ydGVkIG11bHRpcGxlIHRpbWVzIHdpdGggZGlmZmVyZW50IG1lZGlhIHF1ZXJpZXMuXHJcblx0XHRcdC8vICBJIGhvcGUgdGhpcyB3aWxsIG5ldmVyIG9jY3VyIChIZXkgdGhpcyB3YXkgd2UgaGF2ZSBzbWFsbGVyIGJ1bmRsZXMpXHJcblx0XHRcdGlmKHR5cGVvZiBpdGVtWzBdICE9PSBcIm51bWJlclwiIHx8ICFhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzW2l0ZW1bMF1dKSB7XHJcblx0XHRcdFx0aWYobWVkaWFRdWVyeSAmJiAhaXRlbVsyXSkge1xyXG5cdFx0XHRcdFx0aXRlbVsyXSA9IG1lZGlhUXVlcnk7XHJcblx0XHRcdFx0fSBlbHNlIGlmKG1lZGlhUXVlcnkpIHtcclxuXHRcdFx0XHRcdGl0ZW1bMl0gPSBcIihcIiArIGl0ZW1bMl0gKyBcIikgYW5kIChcIiArIG1lZGlhUXVlcnkgKyBcIilcIjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0bGlzdC5wdXNoKGl0ZW0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fTtcclxuXHRyZXR1cm4gbGlzdDtcclxufTtcclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXG4vLyBtb2R1bGUgaWQgPSAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcImlkXFxcIjpcXFwiZGF0YS12LTBmMmYyYjc3XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vTXVsdGlwbGVRdWlja1JlcG9ydC52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjAxMzk4MDIwXCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wZjJmMmI3N1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wZjJmMmI3N1xcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL011bHRpcGxlUXVpY2tSZXBvcnQudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlciEuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMGYyZjJiNzdcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvVmlzYS9NdWx0aXBsZVF1aWNrUmVwb3J0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjBcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgOSIsIi8qXG4gIE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXG4gIEF1dGhvciBUb2JpYXMgS29wcGVycyBAc29rcmFcbiAgTW9kaWZpZWQgYnkgRXZhbiBZb3UgQHl5eDk5MDgwM1xuKi9cblxudmFyIGhhc0RvY3VtZW50ID0gdHlwZW9mIGRvY3VtZW50ICE9PSAndW5kZWZpbmVkJ1xuXG5pZiAodHlwZW9mIERFQlVHICE9PSAndW5kZWZpbmVkJyAmJiBERUJVRykge1xuICBpZiAoIWhhc0RvY3VtZW50KSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICd2dWUtc3R5bGUtbG9hZGVyIGNhbm5vdCBiZSB1c2VkIGluIGEgbm9uLWJyb3dzZXIgZW52aXJvbm1lbnQuICcgK1xuICAgIFwiVXNlIHsgdGFyZ2V0OiAnbm9kZScgfSBpbiB5b3VyIFdlYnBhY2sgY29uZmlnIHRvIGluZGljYXRlIGEgc2VydmVyLXJlbmRlcmluZyBlbnZpcm9ubWVudC5cIlxuICApIH1cbn1cblxudmFyIGxpc3RUb1N0eWxlcyA9IHJlcXVpcmUoJy4vbGlzdFRvU3R5bGVzJylcblxuLypcbnR5cGUgU3R5bGVPYmplY3QgPSB7XG4gIGlkOiBudW1iZXI7XG4gIHBhcnRzOiBBcnJheTxTdHlsZU9iamVjdFBhcnQ+XG59XG5cbnR5cGUgU3R5bGVPYmplY3RQYXJ0ID0ge1xuICBjc3M6IHN0cmluZztcbiAgbWVkaWE6IHN0cmluZztcbiAgc291cmNlTWFwOiA/c3RyaW5nXG59XG4qL1xuXG52YXIgc3R5bGVzSW5Eb20gPSB7LypcbiAgW2lkOiBudW1iZXJdOiB7XG4gICAgaWQ6IG51bWJlcixcbiAgICByZWZzOiBudW1iZXIsXG4gICAgcGFydHM6IEFycmF5PChvYmo/OiBTdHlsZU9iamVjdFBhcnQpID0+IHZvaWQ+XG4gIH1cbiovfVxuXG52YXIgaGVhZCA9IGhhc0RvY3VtZW50ICYmIChkb2N1bWVudC5oZWFkIHx8IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdoZWFkJylbMF0pXG52YXIgc2luZ2xldG9uRWxlbWVudCA9IG51bGxcbnZhciBzaW5nbGV0b25Db3VudGVyID0gMFxudmFyIGlzUHJvZHVjdGlvbiA9IGZhbHNlXG52YXIgbm9vcCA9IGZ1bmN0aW9uICgpIHt9XG5cbi8vIEZvcmNlIHNpbmdsZS10YWcgc29sdXRpb24gb24gSUU2LTksIHdoaWNoIGhhcyBhIGhhcmQgbGltaXQgb24gdGhlICMgb2YgPHN0eWxlPlxuLy8gdGFncyBpdCB3aWxsIGFsbG93IG9uIGEgcGFnZVxudmFyIGlzT2xkSUUgPSB0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJyAmJiAvbXNpZSBbNi05XVxcYi8udGVzdChuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkpXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKHBhcmVudElkLCBsaXN0LCBfaXNQcm9kdWN0aW9uKSB7XG4gIGlzUHJvZHVjdGlvbiA9IF9pc1Byb2R1Y3Rpb25cblxuICB2YXIgc3R5bGVzID0gbGlzdFRvU3R5bGVzKHBhcmVudElkLCBsaXN0KVxuICBhZGRTdHlsZXNUb0RvbShzdHlsZXMpXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZSAobmV3TGlzdCkge1xuICAgIHZhciBtYXlSZW1vdmUgPSBbXVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IHN0eWxlc1tpXVxuICAgICAgdmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF1cbiAgICAgIGRvbVN0eWxlLnJlZnMtLVxuICAgICAgbWF5UmVtb3ZlLnB1c2goZG9tU3R5bGUpXG4gICAgfVxuICAgIGlmIChuZXdMaXN0KSB7XG4gICAgICBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIG5ld0xpc3QpXG4gICAgICBhZGRTdHlsZXNUb0RvbShzdHlsZXMpXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlcyA9IFtdXG4gICAgfVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbWF5UmVtb3ZlLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZG9tU3R5bGUgPSBtYXlSZW1vdmVbaV1cbiAgICAgIGlmIChkb21TdHlsZS5yZWZzID09PSAwKSB7XG4gICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBkb21TdHlsZS5wYXJ0c1tqXSgpXG4gICAgICAgIH1cbiAgICAgICAgZGVsZXRlIHN0eWxlc0luRG9tW2RvbVN0eWxlLmlkXVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZXNUb0RvbSAoc3R5bGVzIC8qIEFycmF5PFN0eWxlT2JqZWN0PiAqLykge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0eWxlcy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgdmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF1cbiAgICBpZiAoZG9tU3R5bGUpIHtcbiAgICAgIGRvbVN0eWxlLnJlZnMrK1xuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICBkb21TdHlsZS5wYXJ0c1tqXShpdGVtLnBhcnRzW2pdKVxuICAgICAgfVxuICAgICAgZm9yICg7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBpZiAoZG9tU3R5bGUucGFydHMubGVuZ3RoID4gaXRlbS5wYXJ0cy5sZW5ndGgpIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMubGVuZ3RoID0gaXRlbS5wYXJ0cy5sZW5ndGhcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIHBhcnRzID0gW11cbiAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgaXRlbS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICBwYXJ0cy5wdXNoKGFkZFN0eWxlKGl0ZW0ucGFydHNbal0pKVxuICAgICAgfVxuICAgICAgc3R5bGVzSW5Eb21baXRlbS5pZF0gPSB7IGlkOiBpdGVtLmlkLCByZWZzOiAxLCBwYXJ0czogcGFydHMgfVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBjcmVhdGVTdHlsZUVsZW1lbnQgKCkge1xuICB2YXIgc3R5bGVFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKVxuICBzdHlsZUVsZW1lbnQudHlwZSA9ICd0ZXh0L2NzcydcbiAgaGVhZC5hcHBlbmRDaGlsZChzdHlsZUVsZW1lbnQpXG4gIHJldHVybiBzdHlsZUVsZW1lbnRcbn1cblxuZnVuY3Rpb24gYWRkU3R5bGUgKG9iaiAvKiBTdHlsZU9iamVjdFBhcnQgKi8pIHtcbiAgdmFyIHVwZGF0ZSwgcmVtb3ZlXG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzdHlsZVtkYXRhLXZ1ZS1zc3ItaWR+PVwiJyArIG9iai5pZCArICdcIl0nKVxuXG4gIGlmIChzdHlsZUVsZW1lbnQpIHtcbiAgICBpZiAoaXNQcm9kdWN0aW9uKSB7XG4gICAgICAvLyBoYXMgU1NSIHN0eWxlcyBhbmQgaW4gcHJvZHVjdGlvbiBtb2RlLlxuICAgICAgLy8gc2ltcGx5IGRvIG5vdGhpbmcuXG4gICAgICByZXR1cm4gbm9vcFxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBoYXMgU1NSIHN0eWxlcyBidXQgaW4gZGV2IG1vZGUuXG4gICAgICAvLyBmb3Igc29tZSByZWFzb24gQ2hyb21lIGNhbid0IGhhbmRsZSBzb3VyY2UgbWFwIGluIHNlcnZlci1yZW5kZXJlZFxuICAgICAgLy8gc3R5bGUgdGFncyAtIHNvdXJjZSBtYXBzIGluIDxzdHlsZT4gb25seSB3b3JrcyBpZiB0aGUgc3R5bGUgdGFnIGlzXG4gICAgICAvLyBjcmVhdGVkIGFuZCBpbnNlcnRlZCBkeW5hbWljYWxseS4gU28gd2UgcmVtb3ZlIHRoZSBzZXJ2ZXIgcmVuZGVyZWRcbiAgICAgIC8vIHN0eWxlcyBhbmQgaW5qZWN0IG5ldyBvbmVzLlxuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIGlmIChpc09sZElFKSB7XG4gICAgLy8gdXNlIHNpbmdsZXRvbiBtb2RlIGZvciBJRTkuXG4gICAgdmFyIHN0eWxlSW5kZXggPSBzaW5nbGV0b25Db3VudGVyKytcbiAgICBzdHlsZUVsZW1lbnQgPSBzaW5nbGV0b25FbGVtZW50IHx8IChzaW5nbGV0b25FbGVtZW50ID0gY3JlYXRlU3R5bGVFbGVtZW50KCkpXG4gICAgdXBkYXRlID0gYXBwbHlUb1NpbmdsZXRvblRhZy5iaW5kKG51bGwsIHN0eWxlRWxlbWVudCwgc3R5bGVJbmRleCwgZmFsc2UpXG4gICAgcmVtb3ZlID0gYXBwbHlUb1NpbmdsZXRvblRhZy5iaW5kKG51bGwsIHN0eWxlRWxlbWVudCwgc3R5bGVJbmRleCwgdHJ1ZSlcbiAgfSBlbHNlIHtcbiAgICAvLyB1c2UgbXVsdGktc3R5bGUtdGFnIG1vZGUgaW4gYWxsIG90aGVyIGNhc2VzXG4gICAgc3R5bGVFbGVtZW50ID0gY3JlYXRlU3R5bGVFbGVtZW50KClcbiAgICB1cGRhdGUgPSBhcHBseVRvVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50KVxuICAgIHJlbW92ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudClcbiAgICB9XG4gIH1cblxuICB1cGRhdGUob2JqKVxuXG4gIHJldHVybiBmdW5jdGlvbiB1cGRhdGVTdHlsZSAobmV3T2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICAgIGlmIChuZXdPYmopIHtcbiAgICAgIGlmIChuZXdPYmouY3NzID09PSBvYmouY3NzICYmXG4gICAgICAgICAgbmV3T2JqLm1lZGlhID09PSBvYmoubWVkaWEgJiZcbiAgICAgICAgICBuZXdPYmouc291cmNlTWFwID09PSBvYmouc291cmNlTWFwKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuICAgICAgdXBkYXRlKG9iaiA9IG5ld09iailcbiAgICB9IGVsc2Uge1xuICAgICAgcmVtb3ZlKClcbiAgICB9XG4gIH1cbn1cblxudmFyIHJlcGxhY2VUZXh0ID0gKGZ1bmN0aW9uICgpIHtcbiAgdmFyIHRleHRTdG9yZSA9IFtdXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIChpbmRleCwgcmVwbGFjZW1lbnQpIHtcbiAgICB0ZXh0U3RvcmVbaW5kZXhdID0gcmVwbGFjZW1lbnRcbiAgICByZXR1cm4gdGV4dFN0b3JlLmZpbHRlcihCb29sZWFuKS5qb2luKCdcXG4nKVxuICB9XG59KSgpXG5cbmZ1bmN0aW9uIGFwcGx5VG9TaW5nbGV0b25UYWcgKHN0eWxlRWxlbWVudCwgaW5kZXgsIHJlbW92ZSwgb2JqKSB7XG4gIHZhciBjc3MgPSByZW1vdmUgPyAnJyA6IG9iai5jc3NcblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gcmVwbGFjZVRleHQoaW5kZXgsIGNzcylcbiAgfSBlbHNlIHtcbiAgICB2YXIgY3NzTm9kZSA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcylcbiAgICB2YXIgY2hpbGROb2RlcyA9IHN0eWxlRWxlbWVudC5jaGlsZE5vZGVzXG4gICAgaWYgKGNoaWxkTm9kZXNbaW5kZXhdKSBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgaWYgKGNoaWxkTm9kZXMubGVuZ3RoKSB7XG4gICAgICBzdHlsZUVsZW1lbnQuaW5zZXJ0QmVmb3JlKGNzc05vZGUsIGNoaWxkTm9kZXNbaW5kZXhdKVxuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZUVsZW1lbnQuYXBwZW5kQ2hpbGQoY3NzTm9kZSlcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gYXBwbHlUb1RhZyAoc3R5bGVFbGVtZW50LCBvYmopIHtcbiAgdmFyIGNzcyA9IG9iai5jc3NcbiAgdmFyIG1lZGlhID0gb2JqLm1lZGlhXG4gIHZhciBzb3VyY2VNYXAgPSBvYmouc291cmNlTWFwXG5cbiAgaWYgKG1lZGlhKSB7XG4gICAgc3R5bGVFbGVtZW50LnNldEF0dHJpYnV0ZSgnbWVkaWEnLCBtZWRpYSlcbiAgfVxuXG4gIGlmIChzb3VyY2VNYXApIHtcbiAgICAvLyBodHRwczovL2RldmVsb3Blci5jaHJvbWUuY29tL2RldnRvb2xzL2RvY3MvamF2YXNjcmlwdC1kZWJ1Z2dpbmdcbiAgICAvLyB0aGlzIG1ha2VzIHNvdXJjZSBtYXBzIGluc2lkZSBzdHlsZSB0YWdzIHdvcmsgcHJvcGVybHkgaW4gQ2hyb21lXG4gICAgY3NzICs9ICdcXG4vKiMgc291cmNlVVJMPScgKyBzb3VyY2VNYXAuc291cmNlc1swXSArICcgKi8nXG4gICAgLy8gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMjY2MDM4NzVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtiYXNlNjQsJyArIGJ0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHNvdXJjZU1hcCkpKSkgKyAnICovJ1xuICB9XG5cbiAgaWYgKHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0KSB7XG4gICAgc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQuY3NzVGV4dCA9IGNzc1xuICB9IGVsc2Uge1xuICAgIHdoaWxlIChzdHlsZUVsZW1lbnQuZmlyc3RDaGlsZCkge1xuICAgICAgc3R5bGVFbGVtZW50LnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKVxuICAgIH1cbiAgICBzdHlsZUVsZW1lbnQuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKSlcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1xuLy8gbW9kdWxlIGlkID0gM1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiAzIDQgNSA5IDExIDEyIDEzIiwiPHRlbXBsYXRlPlxuPGRpdiBjbGFzcz1cIm1vZGFsIG1kLW1vZGFsIGZhZGVcIiA6aWQ9XCJpZFwiIHRhYmluZGV4PVwiLTFcIiByb2xlPVwiZGlhbG9nXCIgYXJpYS1sYWJlbGxlZGJ5PVwibW9kYWwtbGFiZWxcIj5cbiAgICA8ZGl2IDpjbGFzcz1cIidtb2RhbC1kaWFsb2cgJytzaXplXCIgcm9sZT1cImRvY3VtZW50XCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtaGVhZGVyXCI+XG4gICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwibW9kYWwtdGl0bGVcIiBpZD1cIm1vZGFsLWxhYmVsXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC10aXRsZVwiPk1vZGFsIFRpdGxlPC9zbG90PlxuICAgICAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+XG4gICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLWJvZHlcIj5Nb2RhbCBCb2R5PC9zbG90PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLWZvb3RlclwiPlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG48L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gICAgcHJvcHM6IHtcbiAgICAgICAgJ2lkJzp7cmVxdWlyZWQ6dHJ1ZX1cbiAgICAgICAgLCdzaXplJzoge2RlZmF1bHQ6J21vZGFsLW1kJ31cbiAgICB9XG59XG48L3NjcmlwdD5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBEaWFsb2dNb2RhbC52dWU/MDAzYmRhODgiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTRkZTYwNjU1XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIERpYWxvZ01vZGFsLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi00ZGU2MDY1NVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTRkZTYwNjU1XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDVcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDUgNiA5IDExIDE4IDE5IDIwIDIxIDIyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwgbWQtbW9kYWwgZmFkZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IF92bS5pZCxcbiAgICAgIFwidGFiaW5kZXhcIjogXCItMVwiLFxuICAgICAgXCJyb2xlXCI6IFwiZGlhbG9nXCIsXG4gICAgICBcImFyaWEtbGFiZWxsZWRieVwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGNsYXNzOiAnbW9kYWwtZGlhbG9nICcgKyBfdm0uc2l6ZSxcbiAgICBhdHRyczoge1xuICAgICAgXCJyb2xlXCI6IFwiZG9jdW1lbnRcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWhlYWRlclwiXG4gIH0sIFtfYygnaDQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtdGl0bGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiLCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtYm9keVwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1ib2R5XCIsIFtfdm0uX3YoXCJNb2RhbCBCb2R5XCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtZm9vdGVyXCIsIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTRkZTYwNjU1XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNGRlNjA2NTVcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDUgNiA5IDExIDE4IDE5IDIwIDIxIDIyIiwiLyoqXG4gKiBUcmFuc2xhdGVzIHRoZSBsaXN0IGZvcm1hdCBwcm9kdWNlZCBieSBjc3MtbG9hZGVyIGludG8gc29tZXRoaW5nXG4gKiBlYXNpZXIgdG8gbWFuaXB1bGF0ZS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBsaXN0VG9TdHlsZXMgKHBhcmVudElkLCBsaXN0KSB7XG4gIHZhciBzdHlsZXMgPSBbXVxuICB2YXIgbmV3U3R5bGVzID0ge31cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldXG4gICAgdmFyIGlkID0gaXRlbVswXVxuICAgIHZhciBjc3MgPSBpdGVtWzFdXG4gICAgdmFyIG1lZGlhID0gaXRlbVsyXVxuICAgIHZhciBzb3VyY2VNYXAgPSBpdGVtWzNdXG4gICAgdmFyIHBhcnQgPSB7XG4gICAgICBpZDogcGFyZW50SWQgKyAnOicgKyBpLFxuICAgICAgY3NzOiBjc3MsXG4gICAgICBtZWRpYTogbWVkaWEsXG4gICAgICBzb3VyY2VNYXA6IHNvdXJjZU1hcFxuICAgIH1cbiAgICBpZiAoIW5ld1N0eWxlc1tpZF0pIHtcbiAgICAgIHN0eWxlcy5wdXNoKG5ld1N0eWxlc1tpZF0gPSB7IGlkOiBpZCwgcGFydHM6IFtwYXJ0XSB9KVxuICAgIH0gZWxzZSB7XG4gICAgICBuZXdTdHlsZXNbaWRdLnBhcnRzLnB1c2gocGFydClcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHN0eWxlc1xufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qc1xuLy8gbW9kdWxlIGlkID0gN1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiAzIDQgNSA5IDExIDEyIDEzIl0sInNvdXJjZVJvb3QiOiIifQ==