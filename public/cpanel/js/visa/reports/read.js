/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 487);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 197:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('dialog-modal', __webpack_require__(5));

var data = window.Laravel.user;

var readReport = new Vue({
    el: '#read-report',
    data: {
        reports: [],
        services: [],
        trackingno: '',
        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            filter: 0
        }]
    },
    methods: {
        submitFilter: function submitFilter() {
            var _this = this;

            params = {
                start: $('form#date-range-form input[name=start]').val(),
                end: $('form#date-range-form input[name=end]').val(),
                client_id: $('form#date-range-form input[name=client_id]').val()
            };

            axios.get('/visa/report/report-filter', {
                params: params
            }).then(function (response) {
                //this.reports = response.data;
                //this.callDataTables();

                _this.reports = response.data;
                _this.createDatatableToggle(_this.reports);
                // this.createDatatableToggle(this.reports);
            });
        },

        // callDataTables(){
        //     $('#lists').DataTable().destroy();    
        //     setTimeout(function(){
        //         $('#lists').DataTable({
        //             responsive: true,
        //             "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        //             "iDisplayLength": 10,
        //             "order": [[ 2, "desc" ]],
        //             "columns": [
        //                 {
        //                     "width": "35%",
        //                 },
        //                 { 
        //                     "width": "5%",
        //                 },
        //                 {
        //                     "width": "20%",
        //                 },
        //                 {
        //                     "width": "40%",
        //                 }, 

        //             ]
        //         });
        //     },2000);           
        // },

        createDatatableToggle: function createDatatableToggle(reports) {

            $('#lists').DataTable().destroy();

            setTimeout(function () {

                var format = function format(d) {
                    // console.log(d);
                    var ids = '';
                    var clids = [];
                    for (var i = 0; i < d.client.length; i++) {
                        var found = jQuery.inArray(d.client[i].client_id, clids);
                        // console.log(found);
                        if (found < 0) {
                            // Element was not found, add it.
                            clids.push(d.client[i].client_id);
                            ids += '<a href="/visa/client/' + d.client[i].client_id + '" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="' + d.client[i].fullname + '" data-trigger="hover">' + d.client[i].client_id + '</a>' + ",";
                        }
                        // console.log(clids);
                        // if(i != ((d.client).length - 1) ){
                        //     ids = ids+",";
                        // }
                    }
                    // ids = rtrim(ids,',');
                    return '<table cellpadding="5" cellspacing="0" class="table table-striped table-hover dtchildrow" >' + '<tr style="padding-left:50px;">' + '<td><b>Report : </b>' + d.detail + '</td>' +
                    // '<td><b>Client IDs : </b> '+ids+'</td>'+
                    '</tr>' + '</table>';
                };

                table2 = $('#lists').DataTable({
                    "data": reports,
                    responsive: true,
                    "columns": [
                    // {
                    //     "width": "5%",
                    //     "className":      'service-control text-center',
                    //     "orderable":      false,
                    //     "data":           null,
                    //     "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
                    // },
                    {
                        "width": "40%",
                        "className": 'service-control',
                        data: null, render: function render(reports, type, row) {
                            if (reports.type == "report") {
                                return '<b style="color: #1ab394;">' + reports.service + '</b>';
                            }
                            return '<b style="color: black;">' + reports.service + '</b>';
                        }
                    }, {
                        "className": 'service-control',
                        "width": "13%",
                        data: null, render: function render(reports, type, row) {
                            var sid = '';
                            for (var i = 0; i < reports.client.length; i++) {
                                sid += reports.client[i].client_id + ",";
                            }
                            return '<b>' + reports.log_date + '</b><span style="display:none;">' + sid + '</span>';
                        }
                    }, {
                        "className": 'service-control',
                        "width": "12%",
                        data: null, render: function render(reports, type, row, meta) {
                            return '<b>' + reports.processor + '</b>';
                        }
                    }, {
                        "width": "35%",
                        "className": 'service-control wrapok',
                        // "class": "wrapnot",
                        data: null, render: function render(reports, type, row, meta) {
                            var ids = '';
                            var clids = [];
                            for (var i = 0; i < reports.client.length; i++) {
                                var found = jQuery.inArray(reports.client[i].client_id, clids);
                                // console.log(found);
                                if (found < 0) {
                                    // Element was not found, add it.
                                    clids.push(reports.client[i].client_id);
                                    ids += '<a href="/visa/client/' + reports.client[i].client_id + '" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="' + reports.client[i].client_id + '" data-trigger="hover">' + reports.client[i].fullname + '</a>' + " , ";
                                }
                                // console.log(clids);
                                // if(i != ((d.client).length - 1) ){
                                //     ids = ids+",";
                                // }
                            }
                            return ids;
                        }
                    }]
                });
                $('body').off('click', 'table#lists tbody td.service-control');
                $('body').on('click', 'table#lists tbody td.service-control', function (e) {
                    var tr2 = $(this).closest('tr');
                    var row = table2.row(tr2);

                    if (row.child.isShown()) {
                        row.child.hide();
                        tr2.removeClass('shown');
                    } else {
                        row.child(format(row.data())).show();
                        tr2.addClass('shown');
                    }
                });
                //EXPAND ALL CHILD ROWS BY DEFAULT
                table2.rows().every(function () {
                    // If row has details collapsed
                    if (!this.child.isShown()) {
                        // Open this row
                        this.child(format(this.data())).show();
                        $(this.node()).addClass('shown');
                    }
                });

                // Handle click on "Expand All" button
                $('#btn-show-all-children').on('click', function () {
                    // Enumerate all rows
                    table2.rows().every(function () {
                        // If row has details collapsed
                        if (!this.child.isShown()) {
                            // Open this row
                            this.child(format(this.data())).show();
                            $(this.node()).addClass('shown');
                        }
                    });
                });

                // Handle click on "Collapse All" button
                $('#btn-hide-all-children').on('click', function () {
                    // Enumerate all rows
                    table2.rows().every(function () {
                        // If row has details expanded
                        if (this.child.isShown()) {
                            // Collapse row details
                            this.child.hide();
                            $(this.node()).removeClass('shown');
                        }
                    });
                });
            }.bind(this), 2000);
        },
        showTracking: function showTracking(id) {
            var _this2 = this;

            this.trackingno = id;
            axios.get('/visa/report/get-services/' + id).then(function (response) {
                _this2.services = response.data;
                $('#tracking').modal('toggle');
            });
        }
    },
    created: function created() {
        var _this3 = this;

        axios.get('/visa/report/get-reports/').then(function (response) {
            $('form#date-range-form input[name=start],form#date-range-form input[name=end]').datepicker({ format: 'yyyy/mm/dd' });
            $('form#date-range-form input[name=start],form#date-range-form input[name=end]').datepicker('setDate', 'today');
            _this3.reports = response.data;
            _this3.createDatatableToggle(_this3.reports);
        });

        var x = this.permissions.filter(function (obj) {
            return obj.type === 'Reports';
        });

        var y = x.filter(function (obj) {
            return obj.name === 'Filter';
        });

        if (y.length == 0) this.selpermissions.filter = 0;else this.selpermissions.filter = 1;

        console.log(this.selpermissions.filter);
    },
    mounted: function mounted() {
        $(document).on('focus', 'form#date-range-form .input-daterange', function () {
            $(this).datepicker({
                format: 'yyyy/mm/dd',
                todayHighlight: true
            });
        });
        $('body').popover({ // Ok
            html: true,
            trigger: 'hover',
            selector: '[data-toggle="popover"]'
        });
    }
});

$(document).ready(function () {
    $('[data-toggle=popover]').popover();

    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });
});

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 487:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(197);


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(6),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/visa/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de60655", Component.options)
  } else {
    hotAPI.reload("data-v-4de60655", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4de60655", module.exports)
  }
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL3JlcG9ydHMvcmVhZC5qcyIsIndlYnBhY2s6Ly8vRGlhbG9nTW9kYWwudnVlP2U0NzQqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlPzIwN2MqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlP2ZlMWIqKioqKioiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImRhdGEiLCJ3aW5kb3ciLCJMYXJhdmVsIiwidXNlciIsInJlYWRSZXBvcnQiLCJlbCIsInJlcG9ydHMiLCJzZXJ2aWNlcyIsInRyYWNraW5nbm8iLCJzZXR0aW5nIiwiYWNjZXNzX2NvbnRyb2wiLCJwZXJtaXNzaW9ucyIsInNlbHBlcm1pc3Npb25zIiwiZmlsdGVyIiwibWV0aG9kcyIsInN1Ym1pdEZpbHRlciIsInBhcmFtcyIsInN0YXJ0IiwiJCIsInZhbCIsImVuZCIsImNsaWVudF9pZCIsImF4aW9zIiwiZ2V0IiwidGhlbiIsInJlc3BvbnNlIiwiY3JlYXRlRGF0YXRhYmxlVG9nZ2xlIiwiRGF0YVRhYmxlIiwiZGVzdHJveSIsInNldFRpbWVvdXQiLCJmb3JtYXQiLCJkIiwiaWRzIiwiY2xpZHMiLCJpIiwiY2xpZW50IiwibGVuZ3RoIiwiZm91bmQiLCJqUXVlcnkiLCJpbkFycmF5IiwicHVzaCIsImZ1bGxuYW1lIiwiZGV0YWlsIiwidGFibGUyIiwicmVzcG9uc2l2ZSIsInJlbmRlciIsInR5cGUiLCJyb3ciLCJzZXJ2aWNlIiwic2lkIiwibG9nX2RhdGUiLCJtZXRhIiwicHJvY2Vzc29yIiwib2ZmIiwib24iLCJlIiwidHIyIiwiY2xvc2VzdCIsImNoaWxkIiwiaXNTaG93biIsImhpZGUiLCJyZW1vdmVDbGFzcyIsInNob3ciLCJhZGRDbGFzcyIsInJvd3MiLCJldmVyeSIsIm5vZGUiLCJiaW5kIiwic2hvd1RyYWNraW5nIiwiaWQiLCJtb2RhbCIsImNyZWF0ZWQiLCJkYXRlcGlja2VyIiwieCIsIm9iaiIsInkiLCJuYW1lIiwiY29uc29sZSIsImxvZyIsIm1vdW50ZWQiLCJkb2N1bWVudCIsInRvZGF5SGlnaGxpZ2h0IiwicG9wb3ZlciIsImh0bWwiLCJ0cmlnZ2VyIiwic2VsZWN0b3IiLCJyZWFkeSIsInRvb2x0aXAiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQUEsSUFBSUMsU0FBSixDQUFjLGNBQWQsRUFBK0JDLG1CQUFPQSxDQUFDLENBQVIsQ0FBL0I7O0FBRUEsSUFBSUMsT0FBT0MsT0FBT0MsT0FBUCxDQUFlQyxJQUExQjs7QUFFQSxJQUFJQyxhQUFhLElBQUlQLEdBQUosQ0FBUTtBQUN4QlEsUUFBRyxjQURxQjtBQUV4QkwsVUFBSztBQUNKTSxpQkFBUSxFQURKO0FBRUVDLGtCQUFTLEVBRlg7QUFHRUMsb0JBQVcsRUFIYjtBQUlFQyxpQkFBU1QsS0FBS1UsY0FBTCxDQUFvQixDQUFwQixFQUF1QkQsT0FKbEM7QUFLRUUscUJBQWFYLEtBQUtXLFdBTHBCO0FBTUVDLHdCQUFnQixDQUFDO0FBQ2JDLG9CQUFPO0FBRE0sU0FBRDtBQU5sQixLQUZtQjtBQVl4QkMsYUFBUztBQUNSQyxvQkFEUSwwQkFDTTtBQUFBOztBQUNKQyxxQkFBUztBQUNMQyx1QkFBT0MsRUFBRSx3Q0FBRixFQUE0Q0MsR0FBNUMsRUFERjtBQUVMQyxxQkFBS0YsRUFBRSxzQ0FBRixFQUEwQ0MsR0FBMUMsRUFGQTtBQUdMRSwyQkFBV0gsRUFBRSw0Q0FBRixFQUFnREMsR0FBaEQ7QUFITixhQUFUOztBQU1BRyxrQkFBTUMsR0FBTixDQUFVLDRCQUFWLEVBQXdDO0FBQ3BDUCx3QkFBUUE7QUFENEIsYUFBeEMsRUFHQ1EsSUFIRCxDQUdNLG9CQUFZO0FBQ2Q7QUFDQTs7QUFFQSxzQkFBS2xCLE9BQUwsR0FBZW1CLFNBQVN6QixJQUF4QjtBQUNBLHNCQUFLMEIscUJBQUwsQ0FBMkIsTUFBS3BCLE9BQWhDO0FBQ0E7QUFDSCxhQVZEO0FBV1QsU0FuQk87O0FBb0JGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQW9CLDZCQS9DRSxpQ0ErQ29CcEIsT0EvQ3BCLEVBK0M2Qjs7QUFFM0JZLGNBQUUsUUFBRixFQUFZUyxTQUFaLEdBQXdCQyxPQUF4Qjs7QUFFQUMsdUJBQVcsWUFBVzs7QUFFbEIsb0JBQUlDLFNBQVMsU0FBVEEsTUFBUyxDQUFTQyxDQUFULEVBQVk7QUFDckI7QUFDQSx3QkFBSUMsTUFBTSxFQUFWO0FBQ0Esd0JBQUlDLFFBQVEsRUFBWjtBQUNBLHlCQUFJLElBQUlDLElBQUUsQ0FBVixFQUFhQSxJQUFHSCxFQUFFSSxNQUFILENBQVdDLE1BQTFCLEVBQWtDRixHQUFsQyxFQUF1QztBQUNuQyw0QkFBSUcsUUFBUUMsT0FBT0MsT0FBUCxDQUFlUixFQUFFSSxNQUFGLENBQVNELENBQVQsRUFBWWIsU0FBM0IsRUFBc0NZLEtBQXRDLENBQVo7QUFDQztBQUNELDRCQUFJSSxRQUFRLENBQVosRUFBZTtBQUNYO0FBQ0FKLGtDQUFNTyxJQUFOLENBQVdULEVBQUVJLE1BQUYsQ0FBU0QsQ0FBVCxFQUFZYixTQUF2QjtBQUNBVyxtQ0FBTywyQkFBeUJELEVBQUVJLE1BQUYsQ0FBU0QsQ0FBVCxFQUFZYixTQUFyQyxHQUErQyxtR0FBL0MsR0FBbUpVLEVBQUVJLE1BQUYsQ0FBU0QsQ0FBVCxFQUFZTyxRQUEvSixHQUF5Syx5QkFBekssR0FBbU1WLEVBQUVJLE1BQUYsQ0FBU0QsQ0FBVCxFQUFZYixTQUEvTSxHQUEwTixNQUExTixHQUFpTyxHQUF4TztBQUNIO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFFSDtBQUNEO0FBQ0EsMkJBQU8sZ0dBQ0ssaUNBREwsR0FFUyxzQkFGVCxHQUVnQ1UsRUFBRVcsTUFGbEMsR0FFeUMsT0FGekM7QUFHUztBQUNKLDJCQUpMLEdBS0MsVUFMUjtBQU1ILGlCQXpCRDs7QUEyQkFDLHlCQUFTekIsRUFBRSxRQUFGLEVBQVlTLFNBQVosQ0FBdUI7QUFDNUIsNEJBQVFyQixPQURvQjtBQUU1QnNDLGdDQUFZLElBRmdCO0FBRzVCLCtCQUFXO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNJLGlDQUFTLEtBRGI7QUFFSSxxQ0FBa0IsaUJBRnRCO0FBR0k1Qyw4QkFBTSxJQUhWLEVBR2dCNkMsUUFBUSxnQkFBU3ZDLE9BQVQsRUFBa0J3QyxJQUFsQixFQUF3QkMsR0FBeEIsRUFBNkI7QUFDN0MsZ0NBQUd6QyxRQUFRd0MsSUFBUixJQUFnQixRQUFuQixFQUE0QjtBQUN4Qix1Q0FBTyxnQ0FBZ0N4QyxRQUFRMEMsT0FBeEMsR0FBa0QsTUFBekQ7QUFDSDtBQUNELG1DQUFPLDhCQUE4QjFDLFFBQVEwQyxPQUF0QyxHQUFnRCxNQUF2RDtBQUNIO0FBUkwscUJBUk8sRUFrQlA7QUFDSSxxQ0FBa0IsaUJBRHRCO0FBRUksaUNBQVMsS0FGYjtBQUdJaEQsOEJBQU0sSUFIVixFQUdnQjZDLFFBQVEsZ0JBQVN2QyxPQUFULEVBQWtCd0MsSUFBbEIsRUFBd0JDLEdBQXhCLEVBQTZCO0FBQzdDLGdDQUFJRSxNQUFNLEVBQVY7QUFDQSxpQ0FBSSxJQUFJZixJQUFFLENBQVYsRUFBYUEsSUFBRzVCLFFBQVE2QixNQUFULENBQWlCQyxNQUFoQyxFQUF3Q0YsR0FBeEMsRUFBNkM7QUFDekNlLHVDQUFPM0MsUUFBUTZCLE1BQVIsQ0FBZUQsQ0FBZixFQUFrQmIsU0FBbEIsR0FBNEIsR0FBbkM7QUFDSDtBQUNELG1DQUFPLFFBQU1mLFFBQVE0QyxRQUFkLEdBQXdCLGtDQUF4QixHQUEyREQsR0FBM0QsR0FBK0QsU0FBdEU7QUFDSDtBQVRMLHFCQWxCTyxFQTZCUDtBQUNJLHFDQUFrQixpQkFEdEI7QUFFSSxpQ0FBUyxLQUZiO0FBR0lqRCw4QkFBTSxJQUhWLEVBR2dCNkMsUUFBUSxnQkFBU3ZDLE9BQVQsRUFBa0J3QyxJQUFsQixFQUF3QkMsR0FBeEIsRUFBNkJJLElBQTdCLEVBQW1DO0FBQ25ELG1DQUFPLFFBQU03QyxRQUFROEMsU0FBZCxHQUF5QixNQUFoQztBQUNIO0FBTEwscUJBN0JPLEVBb0NQO0FBQ0ksaUNBQVMsS0FEYjtBQUVJLHFDQUFrQix3QkFGdEI7QUFHSTtBQUNBcEQsOEJBQU0sSUFKVixFQUlnQjZDLFFBQVEsZ0JBQVN2QyxPQUFULEVBQWtCd0MsSUFBbEIsRUFBd0JDLEdBQXhCLEVBQTZCSSxJQUE3QixFQUFtQztBQUNuRCxnQ0FBSW5CLE1BQU0sRUFBVjtBQUNBLGdDQUFJQyxRQUFRLEVBQVo7QUFDQSxpQ0FBSSxJQUFJQyxJQUFFLENBQVYsRUFBYUEsSUFBRzVCLFFBQVE2QixNQUFULENBQWlCQyxNQUFoQyxFQUF3Q0YsR0FBeEMsRUFBNkM7QUFDekMsb0NBQUlHLFFBQVFDLE9BQU9DLE9BQVAsQ0FBZWpDLFFBQVE2QixNQUFSLENBQWVELENBQWYsRUFBa0JiLFNBQWpDLEVBQTRDWSxLQUE1QyxDQUFaO0FBQ0M7QUFDRCxvQ0FBSUksUUFBUSxDQUFaLEVBQWU7QUFDWDtBQUNBSiwwQ0FBTU8sSUFBTixDQUFXbEMsUUFBUTZCLE1BQVIsQ0FBZUQsQ0FBZixFQUFrQmIsU0FBN0I7QUFDQVcsMkNBQU8sMkJBQXlCMUIsUUFBUTZCLE1BQVIsQ0FBZUQsQ0FBZixFQUFrQmIsU0FBM0MsR0FBcUQsbUdBQXJELEdBQXlKZixRQUFRNkIsTUFBUixDQUFlRCxDQUFmLEVBQWtCYixTQUEzSyxHQUFzTCx5QkFBdEwsR0FBZ05mLFFBQVE2QixNQUFSLENBQWVELENBQWYsRUFBa0JPLFFBQWxPLEdBQTRPLE1BQTVPLEdBQW1QLEtBQTFQO0FBQ0g7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUVIO0FBQ0QsbUNBQU9ULEdBQVA7QUFDSDtBQXRCTCxxQkFwQ087QUFIaUIsaUJBQXZCLENBQVQ7QUFpRUFkLGtCQUFFLE1BQUYsRUFBVW1DLEdBQVYsQ0FBYyxPQUFkLEVBQXVCLHNDQUF2QjtBQUNBbkMsa0JBQUUsTUFBRixFQUFVb0MsRUFBVixDQUFhLE9BQWIsRUFBc0Isc0NBQXRCLEVBQThELFVBQVVDLENBQVYsRUFBYTtBQUN2RSx3QkFBSUMsTUFBTXRDLEVBQUUsSUFBRixFQUFRdUMsT0FBUixDQUFnQixJQUFoQixDQUFWO0FBQ0Esd0JBQUlWLE1BQU1KLE9BQU9JLEdBQVAsQ0FBWVMsR0FBWixDQUFWOztBQUVBLHdCQUFHVCxJQUFJVyxLQUFKLENBQVVDLE9BQVYsRUFBSCxFQUF3QjtBQUNwQlosNEJBQUlXLEtBQUosQ0FBVUUsSUFBVjtBQUNBSiw0QkFBSUssV0FBSixDQUFnQixPQUFoQjtBQUNILHFCQUhELE1BR087QUFDSGQsNEJBQUlXLEtBQUosQ0FBVzVCLE9BQU9pQixJQUFJL0MsSUFBSixFQUFQLENBQVgsRUFBZ0M4RCxJQUFoQztBQUNBTiw0QkFBSU8sUUFBSixDQUFhLE9BQWI7QUFDSDtBQUNKLGlCQVhEO0FBWUE7QUFDQXBCLHVCQUFPcUIsSUFBUCxHQUFjQyxLQUFkLENBQW9CLFlBQVU7QUFDMUI7QUFDQSx3QkFBRyxDQUFDLEtBQUtQLEtBQUwsQ0FBV0MsT0FBWCxFQUFKLEVBQXlCO0FBQ3JCO0FBQ0EsNkJBQUtELEtBQUwsQ0FBVzVCLE9BQU8sS0FBSzlCLElBQUwsRUFBUCxDQUFYLEVBQWdDOEQsSUFBaEM7QUFDQTVDLDBCQUFFLEtBQUtnRCxJQUFMLEVBQUYsRUFBZUgsUUFBZixDQUF3QixPQUF4QjtBQUNIO0FBQ0osaUJBUEQ7O0FBVUE7QUFDQTdDLGtCQUFFLHdCQUFGLEVBQTRCb0MsRUFBNUIsQ0FBK0IsT0FBL0IsRUFBd0MsWUFBVTtBQUM5QztBQUNBWCwyQkFBT3FCLElBQVAsR0FBY0MsS0FBZCxDQUFvQixZQUFVO0FBQzFCO0FBQ0EsNEJBQUcsQ0FBQyxLQUFLUCxLQUFMLENBQVdDLE9BQVgsRUFBSixFQUF5QjtBQUNyQjtBQUNBLGlDQUFLRCxLQUFMLENBQVc1QixPQUFPLEtBQUs5QixJQUFMLEVBQVAsQ0FBWCxFQUFnQzhELElBQWhDO0FBQ0E1Qyw4QkFBRSxLQUFLZ0QsSUFBTCxFQUFGLEVBQWVILFFBQWYsQ0FBd0IsT0FBeEI7QUFDSDtBQUNKLHFCQVBEO0FBUUgsaUJBVkQ7O0FBWUE7QUFDQTdDLGtCQUFFLHdCQUFGLEVBQTRCb0MsRUFBNUIsQ0FBK0IsT0FBL0IsRUFBd0MsWUFBVTtBQUM5QztBQUNBWCwyQkFBT3FCLElBQVAsR0FBY0MsS0FBZCxDQUFvQixZQUFVO0FBQzFCO0FBQ0EsNEJBQUcsS0FBS1AsS0FBTCxDQUFXQyxPQUFYLEVBQUgsRUFBd0I7QUFDcEI7QUFDQSxpQ0FBS0QsS0FBTCxDQUFXRSxJQUFYO0FBQ0ExQyw4QkFBRSxLQUFLZ0QsSUFBTCxFQUFGLEVBQWVMLFdBQWYsQ0FBMkIsT0FBM0I7QUFDSDtBQUNKLHFCQVBEO0FBUUgsaUJBVkQ7QUFZSCxhQWhKVSxDQWdKVE0sSUFoSlMsQ0FnSkosSUFoSkksQ0FBWCxFQWdKYyxJQWhKZDtBQWtKSCxTQXJNQztBQXdNRkMsb0JBeE1FLHdCQXdNV0MsRUF4TVgsRUF3TWM7QUFBQTs7QUFDWixpQkFBSzdELFVBQUwsR0FBa0I2RCxFQUFsQjtBQUNBL0Msa0JBQU1DLEdBQU4sQ0FBVSwrQkFBNkI4QyxFQUF2QyxFQUNDN0MsSUFERCxDQUNNLG9CQUFZO0FBQ2QsdUJBQUtqQixRQUFMLEdBQWdCa0IsU0FBU3pCLElBQXpCO0FBQ0FrQixrQkFBRSxXQUFGLEVBQWVvRCxLQUFmLENBQXFCLFFBQXJCO0FBQ0gsYUFKRDtBQUtIO0FBL01DLEtBWmU7QUE2TnhCQyxXQTdOd0IscUJBNk5mO0FBQUE7O0FBQ0xqRCxjQUFNQyxHQUFOLENBQVUsMkJBQVYsRUFDQ0MsSUFERCxDQUNNLG9CQUFZO0FBQ1hOLGNBQUUsNkVBQUYsRUFBaUZzRCxVQUFqRixDQUE0RixFQUFDMUMsUUFBTyxZQUFSLEVBQTVGO0FBQ0FaLGNBQUUsNkVBQUYsRUFBaUZzRCxVQUFqRixDQUE0RixTQUE1RixFQUF1RyxPQUF2RztBQUNILG1CQUFLbEUsT0FBTCxHQUFlbUIsU0FBU3pCLElBQXhCO0FBQ0csbUJBQUswQixxQkFBTCxDQUEyQixPQUFLcEIsT0FBaEM7QUFDTixTQU5EOztBQVFHLFlBQUltRSxJQUFJLEtBQUs5RCxXQUFMLENBQWlCRSxNQUFqQixDQUF3QixlQUFPO0FBQUUsbUJBQU82RCxJQUFJNUIsSUFBSixLQUFhLFNBQXBCO0FBQThCLFNBQS9ELENBQVI7O0FBRUEsWUFBSTZCLElBQUlGLEVBQUU1RCxNQUFGLENBQVMsZUFBTztBQUFFLG1CQUFPNkQsSUFBSUUsSUFBSixLQUFhLFFBQXBCO0FBQTZCLFNBQS9DLENBQVI7O0FBRUEsWUFBR0QsRUFBRXZDLE1BQUYsSUFBVSxDQUFiLEVBQWdCLEtBQUt4QixjQUFMLENBQW9CQyxNQUFwQixHQUE2QixDQUE3QixDQUFoQixLQUNLLEtBQUtELGNBQUwsQ0FBb0JDLE1BQXBCLEdBQTZCLENBQTdCOztBQUVMZ0UsZ0JBQVFDLEdBQVIsQ0FBWSxLQUFLbEUsY0FBTCxDQUFvQkMsTUFBaEM7QUFDTixLQTlPdUI7QUErT3JCa0UsV0EvT3FCLHFCQStPWDtBQUNON0QsVUFBRThELFFBQUYsRUFBWTFCLEVBQVosQ0FBZSxPQUFmLEVBQXdCLHVDQUF4QixFQUFpRSxZQUFVO0FBQ3ZFcEMsY0FBRSxJQUFGLEVBQVFzRCxVQUFSLENBQW1CO0FBQ2YxQyx3QkFBTyxZQURRO0FBRWZtRCxnQ0FBZ0I7QUFGRCxhQUFuQjtBQUlILFNBTEQ7QUFNQS9ELFVBQUUsTUFBRixFQUFVZ0UsT0FBVixDQUFrQixFQUFFO0FBQ2hCQyxrQkFBSyxJQURTO0FBRWRDLHFCQUFTLE9BRks7QUFHZEMsc0JBQVU7QUFISSxTQUFsQjtBQUtIO0FBM1BvQixDQUFSLENBQWpCOztBQStQQW5FLEVBQUc4RCxRQUFILEVBQWNNLEtBQWQsQ0FBb0IsWUFBVztBQUMzQnBFLE1BQUUsdUJBQUYsRUFBMkJnRSxPQUEzQjs7QUFFQWhFLE1BQUUsTUFBRixFQUFVcUUsT0FBVixDQUFrQjtBQUNkRixrQkFBVTtBQURJLEtBQWxCO0FBS0gsQ0FSRCxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVPQTtBQUNBO0FBQ0EsZ0NBREE7QUFFQTtBQUZBO0FBREEsRzs7Ozs7Ozs7Ozs7Ozs7O0FDdkJBLGdCQUFnQixtQkFBTyxDQUFDLENBQXFFO0FBQzdGO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLENBQXlPO0FBQ25QO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLENBQXFNO0FBQy9NO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEMiLCJmaWxlIjoianMvdmlzYS9yZXBvcnRzL3JlYWQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ4Nyk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjMiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSIsIlZ1ZS5jb21wb25lbnQoJ2RpYWxvZy1tb2RhbCcsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZScpKTtcblxubGV0IGRhdGEgPSB3aW5kb3cuTGFyYXZlbC51c2VyO1xuXG52YXIgcmVhZFJlcG9ydCA9IG5ldyBWdWUoe1xuXHRlbDonI3JlYWQtcmVwb3J0Jyxcblx0ZGF0YTp7XG5cdFx0cmVwb3J0czpbXSxcbiAgICAgICAgc2VydmljZXM6W10sXG4gICAgICAgIHRyYWNraW5nbm86JycsXG4gICAgICAgIHNldHRpbmc6IGRhdGEuYWNjZXNzX2NvbnRyb2xbMF0uc2V0dGluZyxcbiAgICAgICAgcGVybWlzc2lvbnM6IGRhdGEucGVybWlzc2lvbnMsXG4gICAgICAgIHNlbHBlcm1pc3Npb25zOiBbe1xuICAgICAgICAgICAgZmlsdGVyOjBcbiAgICAgICAgfV1cblx0fSxcblx0bWV0aG9kczoge1xuXHRcdHN1Ym1pdEZpbHRlcigpe1xuICAgICAgICAgICAgcGFyYW1zID0ge1xuICAgICAgICAgICAgICAgIHN0YXJ0OiAkKCdmb3JtI2RhdGUtcmFuZ2UtZm9ybSBpbnB1dFtuYW1lPXN0YXJ0XScpLnZhbCgpLFxuICAgICAgICAgICAgICAgIGVuZDogJCgnZm9ybSNkYXRlLXJhbmdlLWZvcm0gaW5wdXRbbmFtZT1lbmRdJykudmFsKCksXG4gICAgICAgICAgICAgICAgY2xpZW50X2lkOiAkKCdmb3JtI2RhdGUtcmFuZ2UtZm9ybSBpbnB1dFtuYW1lPWNsaWVudF9pZF0nKS52YWwoKSxcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIGF4aW9zLmdldCgnL3Zpc2EvcmVwb3J0L3JlcG9ydC1maWx0ZXInLCB7XG4gICAgICAgICAgICAgICAgcGFyYW1zOiBwYXJhbXNcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgLy90aGlzLnJlcG9ydHMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgIC8vdGhpcy5jYWxsRGF0YVRhYmxlcygpO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5yZXBvcnRzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZURhdGF0YWJsZVRvZ2dsZSh0aGlzLnJlcG9ydHMpO1xuICAgICAgICAgICAgICAgIC8vIHRoaXMuY3JlYXRlRGF0YXRhYmxlVG9nZ2xlKHRoaXMucmVwb3J0cyk7XG4gICAgICAgICAgICB9KTtcblx0XHR9LFxuICAgICAgICAvLyBjYWxsRGF0YVRhYmxlcygpe1xuICAgICAgICAvLyAgICAgJCgnI2xpc3RzJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpOyAgICBcbiAgICAgICAgLy8gICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgLy8gICAgICAgICAkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuICAgICAgICAvLyAgICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxuICAgICAgICAvLyAgICAgICAgICAgICBcImxlbmd0aE1lbnVcIjogW1sxMCwgMjUsIDUwXSwgWzEwLCAyNSwgNTBdXSxcbiAgICAgICAgLy8gICAgICAgICAgICAgXCJpRGlzcGxheUxlbmd0aFwiOiAxMCxcbiAgICAgICAgLy8gICAgICAgICAgICAgXCJvcmRlclwiOiBbWyAyLCBcImRlc2NcIiBdXSxcbiAgICAgICAgLy8gICAgICAgICAgICAgXCJjb2x1bW5zXCI6IFtcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIHtcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICBcIndpZHRoXCI6IFwiMzUlXCIsXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgeyBcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICBcIndpZHRoXCI6IFwiNSVcIixcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIH0sXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICB7XG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiBcIjIwJVwiLFxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIHtcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICBcIndpZHRoXCI6IFwiNDAlXCIsXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICB9LCBcblxuICAgICAgICAvLyAgICAgICAgICAgICBdXG4gICAgICAgIC8vICAgICAgICAgfSk7XG4gICAgICAgIC8vICAgICB9LDIwMDApOyAgICAgICAgICAgXG4gICAgICAgIC8vIH0sXG5cbiAgICAgICAgY3JlYXRlRGF0YXRhYmxlVG9nZ2xlKHJlcG9ydHMpIHtcblxuICAgICAgICAgICAgJCgnI2xpc3RzJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xuXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgdmFyIGZvcm1hdCA9IGZ1bmN0aW9uKGQpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coZCk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBpZHMgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgdmFyIGNsaWRzID0gW107XG4gICAgICAgICAgICAgICAgICAgIGZvcih2YXIgaT0wOyBpPChkLmNsaWVudCkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBmb3VuZCA9IGpRdWVyeS5pbkFycmF5KGQuY2xpZW50W2ldLmNsaWVudF9pZCwgY2xpZHMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGZvdW5kKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmb3VuZCA8IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBFbGVtZW50IHdhcyBub3QgZm91bmQsIGFkZCBpdC5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGlkcy5wdXNoKGQuY2xpZW50W2ldLmNsaWVudF9pZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWRzICs9ICc8YSBocmVmPVwiL3Zpc2EvY2xpZW50LycrZC5jbGllbnRbaV0uY2xpZW50X2lkKydcIiB0YXJnZXQ9XCJfYmxhbmtcIiBkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIiBkYXRhLXBsYWNlbWVudD1cInRvcFwiIGRhdGEtY29udGFpbmVyPVwiYm9keVwiIGRhdGEtY29udGVudD1cIicrZC5jbGllbnRbaV0uZnVsbG5hbWUgKydcIiBkYXRhLXRyaWdnZXI9XCJob3ZlclwiPicrZC5jbGllbnRbaV0uY2xpZW50X2lkICsnPC9hPicrXCIsXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhjbGlkcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZihpICE9ICgoZC5jbGllbnQpLmxlbmd0aCAtIDEpICl7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgaWRzID0gaWRzK1wiLFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gfVxuXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLy8gaWRzID0gcnRyaW0oaWRzLCcsJyk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnPHRhYmxlIGNlbGxwYWRkaW5nPVwiNVwiIGNlbGxzcGFjaW5nPVwiMFwiIGNsYXNzPVwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ob3ZlciBkdGNoaWxkcm93XCIgPicrXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8dHIgc3R5bGU9XCJwYWRkaW5nLWxlZnQ6NTBweDtcIj4nK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzx0ZD48Yj5SZXBvcnQgOiA8L2I+JytkLmRldGFpbCsnPC90ZD4nK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gJzx0ZD48Yj5DbGllbnQgSURzIDogPC9iPiAnK2lkcysnPC90ZD4nK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC90cj4nK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8L3RhYmxlPic7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0YWJsZTIgPSAkKCcjbGlzdHMnKS5EYXRhVGFibGUoIHtcbiAgICAgICAgICAgICAgICAgICAgXCJkYXRhXCI6IHJlcG9ydHMsXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIFwiY29sdW1uc1wiOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgXCJ3aWR0aFwiOiBcIjUlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgXCJjbGFzc05hbWVcIjogICAgICAnc2VydmljZS1jb250cm9sIHRleHQtY2VudGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICBcIm9yZGVyYWJsZVwiOiAgICAgIGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIFwiZGF0YVwiOiAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICBcImRlZmF1bHRDb250ZW50XCI6ICc8aSBjbGFzcz1cImZhIGZhLWFycm93LXJpZ2h0XCIgc3R5bGU9XCJjdXJzb3I6cG9pbnRlcjtcIj48L2k+J1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiBcIjQwJVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiY2xhc3NOYW1lXCI6ICAgICAgJ3NlcnZpY2UtY29udHJvbCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogbnVsbCwgcmVuZGVyOiBmdW5jdGlvbihyZXBvcnRzLCB0eXBlLCByb3cpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYocmVwb3J0cy50eXBlID09IFwicmVwb3J0XCIpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICc8YiBzdHlsZT1cImNvbG9yOiAjMWFiMzk0O1wiPicgKyByZXBvcnRzLnNlcnZpY2UgKyAnPC9iPidcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJzxiIHN0eWxlPVwiY29sb3I6IGJsYWNrO1wiPicgKyByZXBvcnRzLnNlcnZpY2UgKyAnPC9iPidcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiY2xhc3NOYW1lXCI6ICAgICAgJ3NlcnZpY2UtY29udHJvbCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiBcIjEzJVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24ocmVwb3J0cywgdHlwZSwgcm93KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzaWQgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yKHZhciBpPTA7IGk8KHJlcG9ydHMuY2xpZW50KS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2lkICs9IHJlcG9ydHMuY2xpZW50W2ldLmNsaWVudF9pZCtcIixcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJzxiPicrcmVwb3J0cy5sb2dfZGF0ZSsgJzwvYj48c3BhbiBzdHlsZT1cImRpc3BsYXk6bm9uZTtcIj4nK3NpZCsnPC9zcGFuPidcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiY2xhc3NOYW1lXCI6ICAgICAgJ3NlcnZpY2UtY29udHJvbCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiBcIjEyJVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IG51bGwsIHJlbmRlcjogZnVuY3Rpb24ocmVwb3J0cywgdHlwZSwgcm93LCBtZXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnPGI+JytyZXBvcnRzLnByb2Nlc3NvcisgJzwvYj4nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIndpZHRoXCI6IFwiMzUlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJjbGFzc05hbWVcIjogICAgICAnc2VydmljZS1jb250cm9sIHdyYXBvaycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gXCJjbGFzc1wiOiBcIndyYXBub3RcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBudWxsLCByZW5kZXI6IGZ1bmN0aW9uKHJlcG9ydHMsIHR5cGUsIHJvdywgbWV0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaWRzID0gJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjbGlkcyA9IFtdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGk9MDsgaTwocmVwb3J0cy5jbGllbnQpLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgZm91bmQgPSBqUXVlcnkuaW5BcnJheShyZXBvcnRzLmNsaWVudFtpXS5jbGllbnRfaWQsIGNsaWRzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhmb3VuZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZm91bmQgPCAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gRWxlbWVudCB3YXMgbm90IGZvdW5kLCBhZGQgaXQuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpZHMucHVzaChyZXBvcnRzLmNsaWVudFtpXS5jbGllbnRfaWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkcyArPSAnPGEgaHJlZj1cIi92aXNhL2NsaWVudC8nK3JlcG9ydHMuY2xpZW50W2ldLmNsaWVudF9pZCsnXCIgdGFyZ2V0PVwiX2JsYW5rXCIgZGF0YS10b2dnbGU9XCJwb3BvdmVyXCIgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIiBkYXRhLWNvbnRhaW5lcj1cImJvZHlcIiBkYXRhLWNvbnRlbnQ9XCInK3JlcG9ydHMuY2xpZW50W2ldLmNsaWVudF9pZCArJ1wiIGRhdGEtdHJpZ2dlcj1cImhvdmVyXCI+JytyZXBvcnRzLmNsaWVudFtpXS5mdWxsbmFtZSArJzwvYT4nK1wiICwgXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhjbGlkcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBpZihpICE9ICgoZC5jbGllbnQpLmxlbmd0aCAtIDEpICl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgaWRzID0gaWRzK1wiLFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGlkcztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgJCgnYm9keScpLm9mZignY2xpY2snLCAndGFibGUjbGlzdHMgdGJvZHkgdGQuc2VydmljZS1jb250cm9sJyk7XG4gICAgICAgICAgICAgICAgJCgnYm9keScpLm9uKCdjbGljaycsICd0YWJsZSNsaXN0cyB0Ym9keSB0ZC5zZXJ2aWNlLWNvbnRyb2wnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgdHIyID0gJCh0aGlzKS5jbG9zZXN0KCd0cicpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgcm93ID0gdGFibGUyLnJvdyggdHIyICk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIGlmKHJvdy5jaGlsZC5pc1Nob3duKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdy5jaGlsZC5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0cjIucmVtb3ZlQ2xhc3MoJ3Nob3duJyk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByb3cuY2hpbGQoIGZvcm1hdChyb3cuZGF0YSgpKSApLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyMi5hZGRDbGFzcygnc2hvd24nKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIC8vRVhQQU5EIEFMTCBDSElMRCBST1dTIEJZIERFRkFVTFRcbiAgICAgICAgICAgICAgICB0YWJsZTIucm93cygpLmV2ZXJ5KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgIC8vIElmIHJvdyBoYXMgZGV0YWlscyBjb2xsYXBzZWRcbiAgICAgICAgICAgICAgICAgICAgaWYoIXRoaXMuY2hpbGQuaXNTaG93bigpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIE9wZW4gdGhpcyByb3dcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGQoZm9ybWF0KHRoaXMuZGF0YSgpKSkuc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzLm5vZGUoKSkuYWRkQ2xhc3MoJ3Nob3duJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcblxuXG4gICAgICAgICAgICAgICAgLy8gSGFuZGxlIGNsaWNrIG9uIFwiRXhwYW5kIEFsbFwiIGJ1dHRvblxuICAgICAgICAgICAgICAgICQoJyNidG4tc2hvdy1hbGwtY2hpbGRyZW4nKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAvLyBFbnVtZXJhdGUgYWxsIHJvd3NcbiAgICAgICAgICAgICAgICAgICAgdGFibGUyLnJvd3MoKS5ldmVyeShmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gSWYgcm93IGhhcyBkZXRhaWxzIGNvbGxhcHNlZFxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoIXRoaXMuY2hpbGQuaXNTaG93bigpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBPcGVuIHRoaXMgcm93XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGlsZChmb3JtYXQodGhpcy5kYXRhKCkpKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzLm5vZGUoKSkuYWRkQ2xhc3MoJ3Nob3duJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgLy8gSGFuZGxlIGNsaWNrIG9uIFwiQ29sbGFwc2UgQWxsXCIgYnV0dG9uXG4gICAgICAgICAgICAgICAgJCgnI2J0bi1oaWRlLWFsbC1jaGlsZHJlbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgIC8vIEVudW1lcmF0ZSBhbGwgcm93c1xuICAgICAgICAgICAgICAgICAgICB0YWJsZTIucm93cygpLmV2ZXJ5KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBJZiByb3cgaGFzIGRldGFpbHMgZXhwYW5kZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuY2hpbGQuaXNTaG93bigpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBDb2xsYXBzZSByb3cgZGV0YWlsc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hpbGQuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcy5ub2RlKCkpLnJlbW92ZUNsYXNzKCdzaG93bicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAyMDAwKTtcblxuICAgICAgICB9LFxuXG5cbiAgICAgICAgc2hvd1RyYWNraW5nKGlkKXtcbiAgICAgICAgICAgIHRoaXMudHJhY2tpbmdubyA9IGlkO1xuICAgICAgICAgICAgYXhpb3MuZ2V0KCcvdmlzYS9yZXBvcnQvZ2V0LXNlcnZpY2VzLycraWQpXG4gICAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXJ2aWNlcyA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgJCgnI3RyYWNraW5nJykubW9kYWwoJ3RvZ2dsZScpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblx0fSxcblx0Y3JlYXRlZCgpe1xuXHQgICAgYXhpb3MuZ2V0KCcvdmlzYS9yZXBvcnQvZ2V0LXJlcG9ydHMvJylcblx0ICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICQoJ2Zvcm0jZGF0ZS1yYW5nZS1mb3JtIGlucHV0W25hbWU9c3RhcnRdLGZvcm0jZGF0ZS1yYW5nZS1mb3JtIGlucHV0W25hbWU9ZW5kXScpLmRhdGVwaWNrZXIoe2Zvcm1hdDoneXl5eS9tbS9kZCd9KTtcbiAgICAgICAgICAgICQoJ2Zvcm0jZGF0ZS1yYW5nZS1mb3JtIGlucHV0W25hbWU9c3RhcnRdLGZvcm0jZGF0ZS1yYW5nZS1mb3JtIGlucHV0W25hbWU9ZW5kXScpLmRhdGVwaWNrZXIoJ3NldERhdGUnLCAndG9kYXknKTtcblx0ICAgICAgICB0aGlzLnJlcG9ydHMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVEYXRhdGFibGVUb2dnbGUodGhpcy5yZXBvcnRzKTtcblx0ICAgIH0pO1xuXG4gICAgICAgIHZhciB4ID0gdGhpcy5wZXJtaXNzaW9ucy5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAnUmVwb3J0cyd9KTtcblxuICAgICAgICB2YXIgeSA9IHguZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoubmFtZSA9PT0gJ0ZpbHRlcid9KTtcblxuICAgICAgICBpZih5Lmxlbmd0aD09MCkgdGhpcy5zZWxwZXJtaXNzaW9ucy5maWx0ZXIgPSAwO1xuICAgICAgICBlbHNlIHRoaXMuc2VscGVybWlzc2lvbnMuZmlsdGVyID0gMTtcblxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnNlbHBlcm1pc3Npb25zLmZpbHRlcik7XG5cdH0sXG4gICAgbW91bnRlZCgpIHtcbiAgICAgICAgJChkb2N1bWVudCkub24oJ2ZvY3VzJywgJ2Zvcm0jZGF0ZS1yYW5nZS1mb3JtIC5pbnB1dC1kYXRlcmFuZ2UnLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgJCh0aGlzKS5kYXRlcGlja2VyKHtcbiAgICAgICAgICAgICAgICBmb3JtYXQ6J3l5eXkvbW0vZGQnLFxuICAgICAgICAgICAgICAgIHRvZGF5SGlnaGxpZ2h0OiB0cnVlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICAgICQoJ2JvZHknKS5wb3BvdmVyKHsgLy8gT2tcbiAgICAgICAgICAgIGh0bWw6dHJ1ZSxcbiAgICAgICAgICAgIHRyaWdnZXI6ICdob3ZlcicsXG4gICAgICAgICAgICBzZWxlY3RvcjogJ1tkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIl0nXG4gICAgICAgIH0pO1xuICAgIH1cblxufSk7XG5cbiQoIGRvY3VtZW50ICkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJCgnW2RhdGEtdG9nZ2xlPXBvcG92ZXJdJykucG9wb3ZlcigpO1xuXG4gICAgJCgnYm9keScpLnRvb2x0aXAoe1xuICAgICAgICBzZWxlY3RvcjogJ1tkYXRhLXRvZ2dsZT10b29sdGlwXSdcbiAgICB9KTtcblxuXG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL3JlcG9ydHMvcmVhZC5qcyIsIjx0ZW1wbGF0ZT5cbjxkaXYgY2xhc3M9XCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIgOmlkPVwiaWRcIiB0YWJpbmRleD1cIi0xXCIgcm9sZT1cImRpYWxvZ1wiIGFyaWEtbGFiZWxsZWRieT1cIm1vZGFsLWxhYmVsXCI+XG4gICAgPGRpdiA6Y2xhc3M9XCInbW9kYWwtZGlhbG9nICcrc2l6ZVwiIHJvbGU9XCJkb2N1bWVudFwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPlxuICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCIgaWQ9XCJtb2RhbC1sYWJlbFwiPlxuICAgICAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtdGl0bGVcIj5Nb2RhbCBUaXRsZTwvc2xvdD5cbiAgICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1ib2R5XCI+TW9kYWwgQm9keTwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPlxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9zbG90PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICAgIHByb3BzOiB7XG4gICAgICAgICdpZCc6e3JlcXVpcmVkOnRydWV9XG4gICAgICAgICwnc2l6ZSc6IHtkZWZhdWx0Oidtb2RhbC1tZCd9XG4gICAgfVxufVxuPC9zY3JpcHQ+XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRGlhbG9nTW9kYWwudnVlPzAwM2JkYTg4IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi00ZGU2MDY1NVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBEaWFsb2dNb2RhbC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNGRlNjA2NTVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi00ZGU2MDY1NVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSA1XG4vLyBtb2R1bGUgY2h1bmtzID0gNCA1IDYgOSAxMSAxOCAxOSAyMCAyMSAyMiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsIG1kLW1vZGFsIGZhZGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBfdm0uaWQsXG4gICAgICBcInRhYmluZGV4XCI6IFwiLTFcIixcbiAgICAgIFwicm9sZVwiOiBcImRpYWxvZ1wiLFxuICAgICAgXCJhcmlhLWxhYmVsbGVkYnlcIjogXCJtb2RhbC1sYWJlbFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBjbGFzczogJ21vZGFsLWRpYWxvZyAnICsgX3ZtLnNpemUsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwicm9sZVwiOiBcImRvY3VtZW50XCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1oZWFkZXJcIlxuICB9LCBbX2MoJ2g0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLXRpdGxlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJtb2RhbC1sYWJlbFwiXG4gICAgfVxuICB9LCBbX3ZtLl90KFwibW9kYWwtdGl0bGVcIiwgW192bS5fdihcIk1vZGFsIFRpdGxlXCIpXSldLCAyKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWJvZHlcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtYm9keVwiLCBbX3ZtLl92KFwiTW9kYWwgQm9keVwiKV0pXSwgMiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtZm9vdGVyXCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWZvb3RlclwiLCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnlcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDbG9zZVwiKV0pXSldLCAyKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi00ZGU2MDY1NVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTRkZTYwNjU1XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSA2XG4vLyBtb2R1bGUgY2h1bmtzID0gNCA1IDYgOSAxMSAxOCAxOSAyMCAyMSAyMiJdLCJzb3VyY2VSb290IjoiIn0=