/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 503);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue2_dropzone__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue2_dropzone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue2_dropzone__);

var resource = __webpack_require__(20);
Vue.http.interceptors.push(function (request, next) {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    next();
});
Vue.component('album-list', __webpack_require__(383));

var app = new Vue({
    el: '#albums'
});

// let data = window.Laravel.user;
// var url  = window.location.href;
// var tid = url.substring(url.lastIndexOf('/') + 1);
//

// var app = new Vue({
// 	el:'#albums',
// 	data:{
// 		dz: '',
//         headers: {
//             'X-CSRF-TOKEN': window.Laravel.csrfToken,
//         },
//         typeid: tid,
// 		documents:[],
// 		act:'',
// 		sel_id:'',
//
// 		files: [],
//
//         setting: data.access_control[0].setting,
//         permissions: data.permissions,
//         selpermissions: [{
//             addNewDocument:0,
//             editDocument:0,
//             deleteDocument:0
//         }],
//
// 		docsFrm: new Form({
// 	       	id 		: '',
// 		    name 	: '',
// 	    },{ baseURL	: '/visa/gallery'})
// 	},
//
// 	mounted () {
// 		$('#dialogAddDocument').on('show.bs.modal', function (e) {
// 			app.docsFrm.errors.clear();
// 		});
//
// 		if(this.typeid != 'list') {
// 			this.getFiles();
//
// 			var vm = this;
//
// 	        this.dz = 'dropzone';
//
// 	        this.$nextTick(() => {
// 	            this.$refs.returnedItemFiles.$on('vdropzone-removedFile', this.dropzoneFileRemoved.bind(this));
// 	            this.$refs.returnedItemFiles.dropzone.options.headers = this.headers;
// 	            this.$refs.returnedItemFiles.dropzone.options.autoProcessQueue = false;
// 	            this.$refs.returnedItemFiles.dropzone.options.parallelUploads = 100;
// 	            // parallelUploads defines how many files should be uploaded to the server at once.
// 				// autoProcessQueue tells Dropzone that it should not start the queue automatically, not initially, and not after files finished uploading.
// 				// If you want autoProcessQueue to be true after the first upload, then just listen to the processing event, and set this.options.autoProcessQueue = true; inside.
// 	        });
//
// 	        $('#multipleUploadModal').on('hidden.bs.modal', e => {
// 	            this.dz = '';
// 	            this.selectedOrder = {};
// 	            this.returnItemForm = new Form();
// 	        });
// 		}
// 	},
// 	methods:{
// 		getFiles() {
// 			axios.get('/visa/service-manager/client-documents/type/' + this.typeid)
// 	        .then(result => {
// 	          	this.files = result.data;
// 	    	});
// 		},
//
// 		loadDocs(){
// 			axios.get('/visa/gallery')
// 	        .then(result => {
// 	          this.documents = result.data;
//
// 	          this.reloadDataTable();
// 	    	});
// 		},
//
// 		dropzoneUploadSuccess: function (file, response) {
//             let _response = JSON.parse(response);
//
//             let { success, message, path } = _response;
//
//             if(success) {
//             	toastr.success(message);
//
//             	this.$refs.returnedItemFiles.dropzone.removeAllFiles();
//
//             	this.getFiles();
//             }
//         },
//
//         dropzoneFileRemoved(file, error, xhr) {
//             //
//         },
//
//         dropzoneUploadError(file, message, xhr) {
//
//         },
//
//         dropzoneFileAdded(file) {
//         	let { name, type } = file;
//         	let filename = name.substr(0, name.lastIndexOf('.'));
// 			let f = filename.split("&&");
//
// 			let success = true;
// 			let message = '';
//
//         	if(type != 'image/jpeg' && type != 'image/png') {
//         		success = false;
//         		message = 'You can\'t upload files of this type.';
//         	} else if(f.length != 3) {
//         		success = false;
//         		message = 'Incorrect filename format';
//         	} else if(isNaN(f[0])) {
//         		success = false;
//         		message = 'Incorrect filename format';
//         	} else if(!moment(f[1], 'YYYY-MM-DD').isValid()) {
//         		success = false;
//         		message = 'Incorrect filename format';
//         	} else if(!moment(f[2], 'YYYY-MM-DD').isValid()) {
//         		success = false;
//         		message = 'Incorrect filename format';
//         	}
//
//         	if(!success) {
//         		this.$refs.returnedItemFiles.dropzone.removeFile(file);
//         		toastr.error(message, name);
//         	}
//         },
//
//         dropzoneQueueComplete(files) {
//             // $('#multipleUploadModal').modal('hide');
//
//             // toastr.success('Successfully saved.');
//
//             // this.$refs.returnedItemFiles.dropzone.removeAllFiles();
//
//             //this.selectedOrder.status = 'Returned';
//             // this.returnItemForm.submitted = true;
//             // this.returnItemForm.errors.clear();
//         },
//
//         submit() {
//             if(this.$refs.returnedItemFiles.dropzone.getAcceptedFiles().length > 0) {
//                 this.$refs.returnedItemFiles.dropzone.processQueue();
//             } else {
//                 toastr.error('Images field is required.');
//             }
//         },
//
// 		reloadDataTable(){
// 			setTimeout(function(){
// 				$('#lists').DataTable({
// 		            responsive: true,
// 		        });
// 			},1000);
// 		},
// 		saveDocs(e){
// 		   	if(e == "Add"){
// 				this.docsFrm.submit('post', '/album/add')
// 			    .then(result => {
// 			    	let { success, message } = result;
//
// 			    	if(success) {
// 			    		toastr.success(message);
// 			        	$('#dialogAddAlbum').modal('toggle');
// 			        	this.reloadAll();
// 			    	}
// 			    })
// 			    .catch(error => {
// 			    	for(var key in error) {
// 					    if(error.hasOwnProperty(key)) {
// 					    	toastr.error(error[key]);
// 					    }
// 					}
// 			    });
// 			}else if (e == "Edit"){
// 				this.docsFrm.submit('post', '/edit')
// 			    .then(result => {
// 			    	let { success, message } = result;
//
// 			    	if(success) {
// 			    		toastr.success(message);
// 			        	$('#dialogAddDocument').modal('toggle');
// 			        	this.reloadAll();
// 			    	}
// 			    })
// 			    .catch(error => {
// 			        for(var key in error) {
// 					    if(error.hasOwnProperty(key)) {
// 					    	toastr.error(error[key]);
// 					    }
// 					}
// 			    });
// 			}else if (e == "Delete"){
// 				axios.post('/visa/service-manager/service-documents/delete/'+ this.sel_id)
// 			    .then(result => {
// 			        toastr.success('Service Document Deleted');
// 			        this.reloadAll();
// 			    })
// 			    .catch(error => {
// 			        toastr.error('Delete Failed.');
// 			    });
// 			}
// 		},
//
// 		actionDoc(e, id){
// 			if(e == "add"){
// 				$('#dialogAddDocument').modal('toggle');
// 				this.act = "Add";
// 			}else if (e == "edit"){
// 				this.act = "Edit";
// 				axios.get('/visa/service-manager/client-documents/info/'+ id)
// 					.then(result =>{
// 					this.docsFrm.id = result.data.id;
// 					this.docsFrm.name = result.data.name;
// 				})
// 				$('#dialogAddDocument').modal('toggle');
// 			}else if (e == "delete"){
// 				this.act = "Delete";
// 				this.sel_id = id;
// 				$('#dialogRemoveDocument').modal('toggle');
// 			}
// 		},
//
// 		resetDocFrm(){
// 			this.docsFrm = new Form({
// 				id 		: '',
// 		        name 	: '',
// 		    },{ baseURL	: '/visa/gallery'})
// 		},
// 		reloadAll(){
// 			$('#lists').DataTable().destroy();
// 			this.resetDocFrm();
// 			this.loadDocs();
// 		}
// 	},
// 	created(){
// 		this.loadDocs();
// 	},
//
// 	components: {
// 		Dropzone
// 	}
// });

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Url", function() { return Url; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Http", function() { return Http; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Resource", function() { return Resource; });
/*!
 * vue-resource v1.5.1
 * https://github.com/pagekit/vue-resource
 * Released under the MIT License.
 */

/**
 * Promises/A+ polyfill v1.1.4 (https://github.com/bramstein/promis)
 */

var RESOLVED = 0;
var REJECTED = 1;
var PENDING = 2;

function Promise$1(executor) {

    this.state = PENDING;
    this.value = undefined;
    this.deferred = [];

    var promise = this;

    try {
        executor(function (x) {
            promise.resolve(x);
        }, function (r) {
            promise.reject(r);
        });
    } catch (e) {
        promise.reject(e);
    }
}

Promise$1.reject = function (r) {
    return new Promise$1(function (resolve, reject) {
        reject(r);
    });
};

Promise$1.resolve = function (x) {
    return new Promise$1(function (resolve, reject) {
        resolve(x);
    });
};

Promise$1.all = function all(iterable) {
    return new Promise$1(function (resolve, reject) {
        var count = 0, result = [];

        if (iterable.length === 0) {
            resolve(result);
        }

        function resolver(i) {
            return function (x) {
                result[i] = x;
                count += 1;

                if (count === iterable.length) {
                    resolve(result);
                }
            };
        }

        for (var i = 0; i < iterable.length; i += 1) {
            Promise$1.resolve(iterable[i]).then(resolver(i), reject);
        }
    });
};

Promise$1.race = function race(iterable) {
    return new Promise$1(function (resolve, reject) {
        for (var i = 0; i < iterable.length; i += 1) {
            Promise$1.resolve(iterable[i]).then(resolve, reject);
        }
    });
};

var p = Promise$1.prototype;

p.resolve = function resolve(x) {
    var promise = this;

    if (promise.state === PENDING) {
        if (x === promise) {
            throw new TypeError('Promise settled with itself.');
        }

        var called = false;

        try {
            var then = x && x['then'];

            if (x !== null && typeof x === 'object' && typeof then === 'function') {
                then.call(x, function (x) {
                    if (!called) {
                        promise.resolve(x);
                    }
                    called = true;

                }, function (r) {
                    if (!called) {
                        promise.reject(r);
                    }
                    called = true;
                });
                return;
            }
        } catch (e) {
            if (!called) {
                promise.reject(e);
            }
            return;
        }

        promise.state = RESOLVED;
        promise.value = x;
        promise.notify();
    }
};

p.reject = function reject(reason) {
    var promise = this;

    if (promise.state === PENDING) {
        if (reason === promise) {
            throw new TypeError('Promise settled with itself.');
        }

        promise.state = REJECTED;
        promise.value = reason;
        promise.notify();
    }
};

p.notify = function notify() {
    var promise = this;

    nextTick(function () {
        if (promise.state !== PENDING) {
            while (promise.deferred.length) {
                var deferred = promise.deferred.shift(),
                    onResolved = deferred[0],
                    onRejected = deferred[1],
                    resolve = deferred[2],
                    reject = deferred[3];

                try {
                    if (promise.state === RESOLVED) {
                        if (typeof onResolved === 'function') {
                            resolve(onResolved.call(undefined, promise.value));
                        } else {
                            resolve(promise.value);
                        }
                    } else if (promise.state === REJECTED) {
                        if (typeof onRejected === 'function') {
                            resolve(onRejected.call(undefined, promise.value));
                        } else {
                            reject(promise.value);
                        }
                    }
                } catch (e) {
                    reject(e);
                }
            }
        }
    });
};

p.then = function then(onResolved, onRejected) {
    var promise = this;

    return new Promise$1(function (resolve, reject) {
        promise.deferred.push([onResolved, onRejected, resolve, reject]);
        promise.notify();
    });
};

p.catch = function (onRejected) {
    return this.then(undefined, onRejected);
};

/**
 * Promise adapter.
 */

if (typeof Promise === 'undefined') {
    window.Promise = Promise$1;
}

function PromiseObj(executor, context) {

    if (executor instanceof Promise) {
        this.promise = executor;
    } else {
        this.promise = new Promise(executor.bind(context));
    }

    this.context = context;
}

PromiseObj.all = function (iterable, context) {
    return new PromiseObj(Promise.all(iterable), context);
};

PromiseObj.resolve = function (value, context) {
    return new PromiseObj(Promise.resolve(value), context);
};

PromiseObj.reject = function (reason, context) {
    return new PromiseObj(Promise.reject(reason), context);
};

PromiseObj.race = function (iterable, context) {
    return new PromiseObj(Promise.race(iterable), context);
};

var p$1 = PromiseObj.prototype;

p$1.bind = function (context) {
    this.context = context;
    return this;
};

p$1.then = function (fulfilled, rejected) {

    if (fulfilled && fulfilled.bind && this.context) {
        fulfilled = fulfilled.bind(this.context);
    }

    if (rejected && rejected.bind && this.context) {
        rejected = rejected.bind(this.context);
    }

    return new PromiseObj(this.promise.then(fulfilled, rejected), this.context);
};

p$1.catch = function (rejected) {

    if (rejected && rejected.bind && this.context) {
        rejected = rejected.bind(this.context);
    }

    return new PromiseObj(this.promise.catch(rejected), this.context);
};

p$1.finally = function (callback) {

    return this.then(function (value) {
        callback.call(this);
        return value;
    }, function (reason) {
        callback.call(this);
        return Promise.reject(reason);
    }
    );
};

/**
 * Utility functions.
 */

var ref = {};
var hasOwnProperty = ref.hasOwnProperty;
var ref$1 = [];
var slice = ref$1.slice;
var debug = false, ntick;

var inBrowser = typeof window !== 'undefined';

function Util (ref) {
    var config = ref.config;
    var nextTick = ref.nextTick;

    ntick = nextTick;
    debug = config.debug || !config.silent;
}

function warn(msg) {
    if (typeof console !== 'undefined' && debug) {
        console.warn('[VueResource warn]: ' + msg);
    }
}

function error(msg) {
    if (typeof console !== 'undefined') {
        console.error(msg);
    }
}

function nextTick(cb, ctx) {
    return ntick(cb, ctx);
}

function trim(str) {
    return str ? str.replace(/^\s*|\s*$/g, '') : '';
}

function trimEnd(str, chars) {

    if (str && chars === undefined) {
        return str.replace(/\s+$/, '');
    }

    if (!str || !chars) {
        return str;
    }

    return str.replace(new RegExp(("[" + chars + "]+$")), '');
}

function toLower(str) {
    return str ? str.toLowerCase() : '';
}

function toUpper(str) {
    return str ? str.toUpperCase() : '';
}

var isArray = Array.isArray;

function isString(val) {
    return typeof val === 'string';
}

function isFunction(val) {
    return typeof val === 'function';
}

function isObject(obj) {
    return obj !== null && typeof obj === 'object';
}

function isPlainObject(obj) {
    return isObject(obj) && Object.getPrototypeOf(obj) == Object.prototype;
}

function isBlob(obj) {
    return typeof Blob !== 'undefined' && obj instanceof Blob;
}

function isFormData(obj) {
    return typeof FormData !== 'undefined' && obj instanceof FormData;
}

function when(value, fulfilled, rejected) {

    var promise = PromiseObj.resolve(value);

    if (arguments.length < 2) {
        return promise;
    }

    return promise.then(fulfilled, rejected);
}

function options(fn, obj, opts) {

    opts = opts || {};

    if (isFunction(opts)) {
        opts = opts.call(obj);
    }

    return merge(fn.bind({$vm: obj, $options: opts}), fn, {$options: opts});
}

function each(obj, iterator) {

    var i, key;

    if (isArray(obj)) {
        for (i = 0; i < obj.length; i++) {
            iterator.call(obj[i], obj[i], i);
        }
    } else if (isObject(obj)) {
        for (key in obj) {
            if (hasOwnProperty.call(obj, key)) {
                iterator.call(obj[key], obj[key], key);
            }
        }
    }

    return obj;
}

var assign = Object.assign || _assign;

function merge(target) {

    var args = slice.call(arguments, 1);

    args.forEach(function (source) {
        _merge(target, source, true);
    });

    return target;
}

function defaults(target) {

    var args = slice.call(arguments, 1);

    args.forEach(function (source) {

        for (var key in source) {
            if (target[key] === undefined) {
                target[key] = source[key];
            }
        }

    });

    return target;
}

function _assign(target) {

    var args = slice.call(arguments, 1);

    args.forEach(function (source) {
        _merge(target, source);
    });

    return target;
}

function _merge(target, source, deep) {
    for (var key in source) {
        if (deep && (isPlainObject(source[key]) || isArray(source[key]))) {
            if (isPlainObject(source[key]) && !isPlainObject(target[key])) {
                target[key] = {};
            }
            if (isArray(source[key]) && !isArray(target[key])) {
                target[key] = [];
            }
            _merge(target[key], source[key], deep);
        } else if (source[key] !== undefined) {
            target[key] = source[key];
        }
    }
}

/**
 * Root Prefix Transform.
 */

function root (options$$1, next) {

    var url = next(options$$1);

    if (isString(options$$1.root) && !/^(https?:)?\//.test(url)) {
        url = trimEnd(options$$1.root, '/') + '/' + url;
    }

    return url;
}

/**
 * Query Parameter Transform.
 */

function query (options$$1, next) {

    var urlParams = Object.keys(Url.options.params), query = {}, url = next(options$$1);

    each(options$$1.params, function (value, key) {
        if (urlParams.indexOf(key) === -1) {
            query[key] = value;
        }
    });

    query = Url.params(query);

    if (query) {
        url += (url.indexOf('?') == -1 ? '?' : '&') + query;
    }

    return url;
}

/**
 * URL Template v2.0.6 (https://github.com/bramstein/url-template)
 */

function expand(url, params, variables) {

    var tmpl = parse(url), expanded = tmpl.expand(params);

    if (variables) {
        variables.push.apply(variables, tmpl.vars);
    }

    return expanded;
}

function parse(template) {

    var operators = ['+', '#', '.', '/', ';', '?', '&'], variables = [];

    return {
        vars: variables,
        expand: function expand(context) {
            return template.replace(/\{([^{}]+)\}|([^{}]+)/g, function (_, expression, literal) {
                if (expression) {

                    var operator = null, values = [];

                    if (operators.indexOf(expression.charAt(0)) !== -1) {
                        operator = expression.charAt(0);
                        expression = expression.substr(1);
                    }

                    expression.split(/,/g).forEach(function (variable) {
                        var tmp = /([^:*]*)(?::(\d+)|(\*))?/.exec(variable);
                        values.push.apply(values, getValues(context, operator, tmp[1], tmp[2] || tmp[3]));
                        variables.push(tmp[1]);
                    });

                    if (operator && operator !== '+') {

                        var separator = ',';

                        if (operator === '?') {
                            separator = '&';
                        } else if (operator !== '#') {
                            separator = operator;
                        }

                        return (values.length !== 0 ? operator : '') + values.join(separator);
                    } else {
                        return values.join(',');
                    }

                } else {
                    return encodeReserved(literal);
                }
            });
        }
    };
}

function getValues(context, operator, key, modifier) {

    var value = context[key], result = [];

    if (isDefined(value) && value !== '') {
        if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
            value = value.toString();

            if (modifier && modifier !== '*') {
                value = value.substring(0, parseInt(modifier, 10));
            }

            result.push(encodeValue(operator, value, isKeyOperator(operator) ? key : null));
        } else {
            if (modifier === '*') {
                if (Array.isArray(value)) {
                    value.filter(isDefined).forEach(function (value) {
                        result.push(encodeValue(operator, value, isKeyOperator(operator) ? key : null));
                    });
                } else {
                    Object.keys(value).forEach(function (k) {
                        if (isDefined(value[k])) {
                            result.push(encodeValue(operator, value[k], k));
                        }
                    });
                }
            } else {
                var tmp = [];

                if (Array.isArray(value)) {
                    value.filter(isDefined).forEach(function (value) {
                        tmp.push(encodeValue(operator, value));
                    });
                } else {
                    Object.keys(value).forEach(function (k) {
                        if (isDefined(value[k])) {
                            tmp.push(encodeURIComponent(k));
                            tmp.push(encodeValue(operator, value[k].toString()));
                        }
                    });
                }

                if (isKeyOperator(operator)) {
                    result.push(encodeURIComponent(key) + '=' + tmp.join(','));
                } else if (tmp.length !== 0) {
                    result.push(tmp.join(','));
                }
            }
        }
    } else {
        if (operator === ';') {
            result.push(encodeURIComponent(key));
        } else if (value === '' && (operator === '&' || operator === '?')) {
            result.push(encodeURIComponent(key) + '=');
        } else if (value === '') {
            result.push('');
        }
    }

    return result;
}

function isDefined(value) {
    return value !== undefined && value !== null;
}

function isKeyOperator(operator) {
    return operator === ';' || operator === '&' || operator === '?';
}

function encodeValue(operator, value, key) {

    value = (operator === '+' || operator === '#') ? encodeReserved(value) : encodeURIComponent(value);

    if (key) {
        return encodeURIComponent(key) + '=' + value;
    } else {
        return value;
    }
}

function encodeReserved(str) {
    return str.split(/(%[0-9A-Fa-f]{2})/g).map(function (part) {
        if (!/%[0-9A-Fa-f]/.test(part)) {
            part = encodeURI(part);
        }
        return part;
    }).join('');
}

/**
 * URL Template (RFC 6570) Transform.
 */

function template (options) {

    var variables = [], url = expand(options.url, options.params, variables);

    variables.forEach(function (key) {
        delete options.params[key];
    });

    return url;
}

/**
 * Service for URL templating.
 */

function Url(url, params) {

    var self = this || {}, options$$1 = url, transform;

    if (isString(url)) {
        options$$1 = {url: url, params: params};
    }

    options$$1 = merge({}, Url.options, self.$options, options$$1);

    Url.transforms.forEach(function (handler) {

        if (isString(handler)) {
            handler = Url.transform[handler];
        }

        if (isFunction(handler)) {
            transform = factory(handler, transform, self.$vm);
        }

    });

    return transform(options$$1);
}

/**
 * Url options.
 */

Url.options = {
    url: '',
    root: null,
    params: {}
};

/**
 * Url transforms.
 */

Url.transform = {template: template, query: query, root: root};
Url.transforms = ['template', 'query', 'root'];

/**
 * Encodes a Url parameter string.
 *
 * @param {Object} obj
 */

Url.params = function (obj) {

    var params = [], escape = encodeURIComponent;

    params.add = function (key, value) {

        if (isFunction(value)) {
            value = value();
        }

        if (value === null) {
            value = '';
        }

        this.push(escape(key) + '=' + escape(value));
    };

    serialize(params, obj);

    return params.join('&').replace(/%20/g, '+');
};

/**
 * Parse a URL and return its components.
 *
 * @param {String} url
 */

Url.parse = function (url) {

    var el = document.createElement('a');

    if (document.documentMode) {
        el.href = url;
        url = el.href;
    }

    el.href = url;

    return {
        href: el.href,
        protocol: el.protocol ? el.protocol.replace(/:$/, '') : '',
        port: el.port,
        host: el.host,
        hostname: el.hostname,
        pathname: el.pathname.charAt(0) === '/' ? el.pathname : '/' + el.pathname,
        search: el.search ? el.search.replace(/^\?/, '') : '',
        hash: el.hash ? el.hash.replace(/^#/, '') : ''
    };
};

function factory(handler, next, vm) {
    return function (options$$1) {
        return handler.call(vm, options$$1, next);
    };
}

function serialize(params, obj, scope) {

    var array = isArray(obj), plain = isPlainObject(obj), hash;

    each(obj, function (value, key) {

        hash = isObject(value) || isArray(value);

        if (scope) {
            key = scope + '[' + (plain || hash ? key : '') + ']';
        }

        if (!scope && array) {
            params.add(value.name, value.value);
        } else if (hash) {
            serialize(params, value, key);
        } else {
            params.add(key, value);
        }
    });
}

/**
 * XDomain client (Internet Explorer).
 */

function xdrClient (request) {
    return new PromiseObj(function (resolve) {

        var xdr = new XDomainRequest(), handler = function (ref) {
                var type = ref.type;


                var status = 0;

                if (type === 'load') {
                    status = 200;
                } else if (type === 'error') {
                    status = 500;
                }

                resolve(request.respondWith(xdr.responseText, {status: status}));
            };

        request.abort = function () { return xdr.abort(); };

        xdr.open(request.method, request.getUrl());

        if (request.timeout) {
            xdr.timeout = request.timeout;
        }

        xdr.onload = handler;
        xdr.onabort = handler;
        xdr.onerror = handler;
        xdr.ontimeout = handler;
        xdr.onprogress = function () {};
        xdr.send(request.getBody());
    });
}

/**
 * CORS Interceptor.
 */

var SUPPORTS_CORS = inBrowser && 'withCredentials' in new XMLHttpRequest();

function cors (request) {

    if (inBrowser) {

        var orgUrl = Url.parse(location.href);
        var reqUrl = Url.parse(request.getUrl());

        if (reqUrl.protocol !== orgUrl.protocol || reqUrl.host !== orgUrl.host) {

            request.crossOrigin = true;
            request.emulateHTTP = false;

            if (!SUPPORTS_CORS) {
                request.client = xdrClient;
            }
        }
    }

}

/**
 * Form data Interceptor.
 */

function form (request) {

    if (isFormData(request.body)) {
        request.headers.delete('Content-Type');
    } else if (isObject(request.body) && request.emulateJSON) {
        request.body = Url.params(request.body);
        request.headers.set('Content-Type', 'application/x-www-form-urlencoded');
    }

}

/**
 * JSON Interceptor.
 */

function json (request) {

    var type = request.headers.get('Content-Type') || '';

    if (isObject(request.body) && type.indexOf('application/json') === 0) {
        request.body = JSON.stringify(request.body);
    }

    return function (response) {

        return response.bodyText ? when(response.text(), function (text) {

            var type = response.headers.get('Content-Type') || '';

            if (type.indexOf('application/json') === 0 || isJson(text)) {

                try {
                    response.body = JSON.parse(text);
                } catch (e) {
                    response.body = null;
                }

            } else {
                response.body = text;
            }

            return response;

        }) : response;

    };
}

function isJson(str) {

    var start = str.match(/^\s*(\[|\{)/);
    var end = {'[': /]\s*$/, '{': /}\s*$/};

    return start && end[start[1]].test(str);
}

/**
 * JSONP client (Browser).
 */

function jsonpClient (request) {
    return new PromiseObj(function (resolve) {

        var name = request.jsonp || 'callback', callback = request.jsonpCallback || '_jsonp' + Math.random().toString(36).substr(2), body = null, handler, script;

        handler = function (ref) {
            var type = ref.type;


            var status = 0;

            if (type === 'load' && body !== null) {
                status = 200;
            } else if (type === 'error') {
                status = 500;
            }

            if (status && window[callback]) {
                delete window[callback];
                document.body.removeChild(script);
            }

            resolve(request.respondWith(body, {status: status}));
        };

        window[callback] = function (result) {
            body = JSON.stringify(result);
        };

        request.abort = function () {
            handler({type: 'abort'});
        };

        request.params[name] = callback;

        if (request.timeout) {
            setTimeout(request.abort, request.timeout);
        }

        script = document.createElement('script');
        script.src = request.getUrl();
        script.type = 'text/javascript';
        script.async = true;
        script.onload = handler;
        script.onerror = handler;

        document.body.appendChild(script);
    });
}

/**
 * JSONP Interceptor.
 */

function jsonp (request) {

    if (request.method == 'JSONP') {
        request.client = jsonpClient;
    }

}

/**
 * Before Interceptor.
 */

function before (request) {

    if (isFunction(request.before)) {
        request.before.call(this, request);
    }

}

/**
 * HTTP method override Interceptor.
 */

function method (request) {

    if (request.emulateHTTP && /^(PUT|PATCH|DELETE)$/i.test(request.method)) {
        request.headers.set('X-HTTP-Method-Override', request.method);
        request.method = 'POST';
    }

}

/**
 * Header Interceptor.
 */

function header (request) {

    var headers = assign({}, Http.headers.common,
        !request.crossOrigin ? Http.headers.custom : {},
        Http.headers[toLower(request.method)]
    );

    each(headers, function (value, name) {
        if (!request.headers.has(name)) {
            request.headers.set(name, value);
        }
    });

}

/**
 * XMLHttp client (Browser).
 */

function xhrClient (request) {
    return new PromiseObj(function (resolve) {

        var xhr = new XMLHttpRequest(), handler = function (event) {

                var response = request.respondWith(
                'response' in xhr ? xhr.response : xhr.responseText, {
                    status: xhr.status === 1223 ? 204 : xhr.status, // IE9 status bug
                    statusText: xhr.status === 1223 ? 'No Content' : trim(xhr.statusText)
                });

                each(trim(xhr.getAllResponseHeaders()).split('\n'), function (row) {
                    response.headers.append(row.slice(0, row.indexOf(':')), row.slice(row.indexOf(':') + 1));
                });

                resolve(response);
            };

        request.abort = function () { return xhr.abort(); };

        xhr.open(request.method, request.getUrl(), true);

        if (request.timeout) {
            xhr.timeout = request.timeout;
        }

        if (request.responseType && 'responseType' in xhr) {
            xhr.responseType = request.responseType;
        }

        if (request.withCredentials || request.credentials) {
            xhr.withCredentials = true;
        }

        if (!request.crossOrigin) {
            request.headers.set('X-Requested-With', 'XMLHttpRequest');
        }

        // deprecated use downloadProgress
        if (isFunction(request.progress) && request.method === 'GET') {
            xhr.addEventListener('progress', request.progress);
        }

        if (isFunction(request.downloadProgress)) {
            xhr.addEventListener('progress', request.downloadProgress);
        }

        // deprecated use uploadProgress
        if (isFunction(request.progress) && /^(POST|PUT)$/i.test(request.method)) {
            xhr.upload.addEventListener('progress', request.progress);
        }

        if (isFunction(request.uploadProgress) && xhr.upload) {
            xhr.upload.addEventListener('progress', request.uploadProgress);
        }

        request.headers.forEach(function (value, name) {
            xhr.setRequestHeader(name, value);
        });

        xhr.onload = handler;
        xhr.onabort = handler;
        xhr.onerror = handler;
        xhr.ontimeout = handler;
        xhr.send(request.getBody());
    });
}

/**
 * Http client (Node).
 */

function nodeClient (request) {

    var client = __webpack_require__(23);

    return new PromiseObj(function (resolve) {

        var url = request.getUrl();
        var body = request.getBody();
        var method = request.method;
        var headers = {}, handler;

        request.headers.forEach(function (value, name) {
            headers[name] = value;
        });

        client(url, {body: body, method: method, headers: headers}).then(handler = function (resp) {

            var response = request.respondWith(resp.body, {
                status: resp.statusCode,
                statusText: trim(resp.statusMessage)
            });

            each(resp.headers, function (value, name) {
                response.headers.set(name, value);
            });

            resolve(response);

        }, function (error$$1) { return handler(error$$1.response); });
    });
}

/**
 * Base client.
 */

function Client (context) {

    var reqHandlers = [sendRequest], resHandlers = [];

    if (!isObject(context)) {
        context = null;
    }

    function Client(request) {
        while (reqHandlers.length) {

            var handler = reqHandlers.pop();

            if (isFunction(handler)) {

                var response = (void 0), next = (void 0);

                response = handler.call(context, request, function (val) { return next = val; }) || next;

                if (isObject(response)) {
                    return new PromiseObj(function (resolve, reject) {

                        resHandlers.forEach(function (handler) {
                            response = when(response, function (response) {
                                return handler.call(context, response) || response;
                            }, reject);
                        });

                        when(response, resolve, reject);

                    }, context);
                }

                if (isFunction(response)) {
                    resHandlers.unshift(response);
                }

            } else {
                warn(("Invalid interceptor of type " + (typeof handler) + ", must be a function"));
            }
        }
    }

    Client.use = function (handler) {
        reqHandlers.push(handler);
    };

    return Client;
}

function sendRequest(request) {

    var client = request.client || (inBrowser ? xhrClient : nodeClient);

    return client(request);
}

/**
 * HTTP Headers.
 */

var Headers = function Headers(headers) {
    var this$1 = this;


    this.map = {};

    each(headers, function (value, name) { return this$1.append(name, value); });
};

Headers.prototype.has = function has (name) {
    return getName(this.map, name) !== null;
};

Headers.prototype.get = function get (name) {

    var list = this.map[getName(this.map, name)];

    return list ? list.join() : null;
};

Headers.prototype.getAll = function getAll (name) {
    return this.map[getName(this.map, name)] || [];
};

Headers.prototype.set = function set (name, value) {
    this.map[normalizeName(getName(this.map, name) || name)] = [trim(value)];
};

Headers.prototype.append = function append (name, value) {

    var list = this.map[getName(this.map, name)];

    if (list) {
        list.push(trim(value));
    } else {
        this.set(name, value);
    }
};

Headers.prototype.delete = function delete$1 (name) {
    delete this.map[getName(this.map, name)];
};

Headers.prototype.deleteAll = function deleteAll () {
    this.map = {};
};

Headers.prototype.forEach = function forEach (callback, thisArg) {
        var this$1 = this;

    each(this.map, function (list, name) {
        each(list, function (value) { return callback.call(thisArg, value, name, this$1); });
    });
};

function getName(map, name) {
    return Object.keys(map).reduce(function (prev, curr) {
        return toLower(name) === toLower(curr) ? curr : prev;
    }, null);
}

function normalizeName(name) {

    if (/[^a-z0-9\-#$%&'*+.^_`|~]/i.test(name)) {
        throw new TypeError('Invalid character in header field name');
    }

    return trim(name);
}

/**
 * HTTP Response.
 */

var Response = function Response(body, ref) {
    var url = ref.url;
    var headers = ref.headers;
    var status = ref.status;
    var statusText = ref.statusText;


    this.url = url;
    this.ok = status >= 200 && status < 300;
    this.status = status || 0;
    this.statusText = statusText || '';
    this.headers = new Headers(headers);
    this.body = body;

    if (isString(body)) {

        this.bodyText = body;

    } else if (isBlob(body)) {

        this.bodyBlob = body;

        if (isBlobText(body)) {
            this.bodyText = blobText(body);
        }
    }
};

Response.prototype.blob = function blob () {
    return when(this.bodyBlob);
};

Response.prototype.text = function text () {
    return when(this.bodyText);
};

Response.prototype.json = function json () {
    return when(this.text(), function (text) { return JSON.parse(text); });
};

Object.defineProperty(Response.prototype, 'data', {

    get: function get() {
        return this.body;
    },

    set: function set(body) {
        this.body = body;
    }

});

function blobText(body) {
    return new PromiseObj(function (resolve) {

        var reader = new FileReader();

        reader.readAsText(body);
        reader.onload = function () {
            resolve(reader.result);
        };

    });
}

function isBlobText(body) {
    return body.type.indexOf('text') === 0 || body.type.indexOf('json') !== -1;
}

/**
 * HTTP Request.
 */

var Request = function Request(options$$1) {

    this.body = null;
    this.params = {};

    assign(this, options$$1, {
        method: toUpper(options$$1.method || 'GET')
    });

    if (!(this.headers instanceof Headers)) {
        this.headers = new Headers(this.headers);
    }
};

Request.prototype.getUrl = function getUrl () {
    return Url(this);
};

Request.prototype.getBody = function getBody () {
    return this.body;
};

Request.prototype.respondWith = function respondWith (body, options$$1) {
    return new Response(body, assign(options$$1 || {}, {url: this.getUrl()}));
};

/**
 * Service for sending network requests.
 */

var COMMON_HEADERS = {'Accept': 'application/json, text/plain, */*'};
var JSON_CONTENT_TYPE = {'Content-Type': 'application/json;charset=utf-8'};

function Http(options$$1) {

    var self = this || {}, client = Client(self.$vm);

    defaults(options$$1 || {}, self.$options, Http.options);

    Http.interceptors.forEach(function (handler) {

        if (isString(handler)) {
            handler = Http.interceptor[handler];
        }

        if (isFunction(handler)) {
            client.use(handler);
        }

    });

    return client(new Request(options$$1)).then(function (response) {

        return response.ok ? response : PromiseObj.reject(response);

    }, function (response) {

        if (response instanceof Error) {
            error(response);
        }

        return PromiseObj.reject(response);
    });
}

Http.options = {};

Http.headers = {
    put: JSON_CONTENT_TYPE,
    post: JSON_CONTENT_TYPE,
    patch: JSON_CONTENT_TYPE,
    delete: JSON_CONTENT_TYPE,
    common: COMMON_HEADERS,
    custom: {}
};

Http.interceptor = {before: before, method: method, jsonp: jsonp, json: json, form: form, header: header, cors: cors};
Http.interceptors = ['before', 'method', 'jsonp', 'json', 'form', 'header', 'cors'];

['get', 'delete', 'head', 'jsonp'].forEach(function (method$$1) {

    Http[method$$1] = function (url, options$$1) {
        return this(assign(options$$1 || {}, {url: url, method: method$$1}));
    };

});

['post', 'put', 'patch'].forEach(function (method$$1) {

    Http[method$$1] = function (url, body, options$$1) {
        return this(assign(options$$1 || {}, {url: url, method: method$$1, body: body}));
    };

});

/**
 * Service for interacting with RESTful services.
 */

function Resource(url, params, actions, options$$1) {

    var self = this || {}, resource = {};

    actions = assign({},
        Resource.actions,
        actions
    );

    each(actions, function (action, name) {

        action = merge({url: url, params: assign({}, params)}, options$$1, action);

        resource[name] = function () {
            return (self.$http || Http)(opts(action, arguments));
        };
    });

    return resource;
}

function opts(action, args) {

    var options$$1 = assign({}, action), params = {}, body;

    switch (args.length) {

        case 2:

            params = args[0];
            body = args[1];

            break;

        case 1:

            if (/^(POST|PUT|PATCH)$/i.test(options$$1.method)) {
                body = args[0];
            } else {
                params = args[0];
            }

            break;

        case 0:

            break;

        default:

            throw 'Expected up to 2 arguments [params, body], got ' + args.length + ' arguments';
    }

    options$$1.body = body;
    options$$1.params = assign({}, options$$1.params, params);

    return options$$1;
}

Resource.actions = {

    get: {method: 'GET'},
    save: {method: 'POST'},
    query: {method: 'GET'},
    update: {method: 'PUT'},
    remove: {method: 'DELETE'},
    delete: {method: 'DELETE'}

};

/**
 * Install plugin.
 */

function plugin(Vue) {

    if (plugin.installed) {
        return;
    }

    Util(Vue);

    Vue.url = Url;
    Vue.http = Http;
    Vue.resource = Resource;
    Vue.Promise = PromiseObj;

    Object.defineProperties(Vue.prototype, {

        $url: {
            get: function get() {
                return options(Vue.url, this, this.$options.url);
            }
        },

        $http: {
            get: function get() {
                return options(Vue.http, this, this.$options.http);
            }
        },

        $resource: {
            get: function get() {
                return Vue.resource.bind(this);
            }
        },

        $promise: {
            get: function get() {
                var this$1 = this;

                return function (executor) { return new Vue.Promise(executor, this$1); };
            }
        }

    });
}

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(plugin);
}

/* harmony default export */ __webpack_exports__["default"] = (plugin);



/***/ }),

/***/ 23:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['columns', 'sortKey', 'sortOrders']
});

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['pagination', 'client', 'filtered']
});

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__AlbumList_vue__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__AlbumList_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__AlbumList_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Pagination_vue__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Pagination_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__Pagination_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: { AlbumTable: __WEBPACK_IMPORTED_MODULE_0__AlbumList_vue___default.a, Pagination: __WEBPACK_IMPORTED_MODULE_1__Pagination_vue___default.a },
  data: function data() {
    var sortOrders = {};
    var columns = [{ width: '40%', label: 'Album', name: 'title' }, { width: '10%', label: 'Action' }];
    columns.forEach(function (column) {
      sortOrders[column.name] = 0;
    });
    return {
      act: '',
      album: {
        id: '',
        title: ''
      },
      url: './gallery/album-view/',
      success: false,
      error: false,
      error2: false,
      projects: [],
      columns: columns,
      sortKey: 'title',
      sortOrders: sortOrders,
      perPage: ['10', '20', '30'],
      sendReq: false,
      tableData: {
        draw: 0,
        length: 10,
        search: '',
        column: 0,
        dir: 'asc'
      },
      pagination: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      }
    };
  },

  methods: {
    configPagination: function configPagination(data) {
      this.pagination.lastPage = data.last_page;
      this.pagination.currentPage = data.current_page;
      this.pagination.total = data.total;
      this.pagination.lastPageUrl = data.last_page_url;
      this.pagination.nextPageUrl = data.next_page_url;
      this.pagination.prevPageUrl = data.prev_page_url;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
      this.tableData.column = this.getIndex(this.columns, 'name', key);
      this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
      this.getProjects();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    getAlbums: function getAlbums() {
      var _this = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : './gallery/album-list';

      this.tableData.draw++;
      axios.get(url, { params: this.tableData }).then(function (response) {
        var data = response.data;
        if (_this.tableData.draw == data.draw) {

          _this.projects = data.data.data;
          _this.configPagination(data.data);
        }
      }).catch(function (errors) {
        console.log(errors);
      });
    },
    actionDoc: function actionDoc(e, id) {
      var _this2 = this;

      if (e == "add") {
        $('#dialogAddAlbum').modal('toggle');
        this.act = "Add";
      } else if (e === 'edit') {
        this.album.id = id;
        this.$http.get('./gallery/album-list/' + id).then(function (response) {
          var data = response.data;
          _this2.album.title = data.title;
        });
        $('#dialogAddAlbum').modal('toggle');
        this.act = "Edit";
      }
    },
    resetAll: function resetAll() {
      this.error = false;
      this.error2 = false;
      $('#dialogAddAlbum').modal('toggle');
      this.act = "";
      this.album = {
        id: '',
        title: ''
      };
    },
    saveAlbum: function saveAlbum(e, id) {
      this.error = false;
      this.error2 = false;
      if (e == 'Add') {
        this.$http.post('./gallery/album-list', this.album).catch(function (error) {
          if (error.status == 422) {
            this.error = true;
          }
          if (error.status == 500) {
            this.error2 = true;
          }
        }).then(function () {
          if (!this.error && !this.error2) {
            $('#dialogAddAlbum').modal('toggle');
            this.act = "";
            this.album = {
              id: '',
              title: ''
            };
          }
        });
      } else if (e == 'Edit') {
        this.error = false;
        this.error2 = false;
        this.$http.post('./gallery/album-list/edit/' + id, this.album).catch(function (error) {
          if (error.status == 422) {
            this.error = true;
          }
          if (error.status == 500) {
            this.error2 = true;
          }
        }).then(function () {
          if (!this.error && !this.error2) {
            $('#dialogAddAlbum').modal('toggle');
            this.act = "";
            this.album = {
              id: '',
              title: ''
            };
          }
        });
      }
      this.sendReq = true;
      this.getAlbums();
    },
    deleteAlbum: function deleteAlbum(id) {
      if (confirm('Are you sure?')) {
        //this.updateProjects('./news-list/update','delete',id);
        this.$http.delete('./gallery/album-list/' + id).then(function (res) {
          if (res.data.response == 'failed') {
            alert("This album is not empty!");
          }
        });
      }
      this.sendReq = true;
      this.getAlbums();
    }
  },
  created: function created() {
    this.getAlbums();
  },
  updated: function updated() {
    if (this.sendReq) {
      for (var i = 0; i < 2; i++) {
        this.getAlbums();
      }
      this.sendReq = false;
    }
  }
});

/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

!function(e,t){ true?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports["vue2-dropzone"]=t():e["vue2-dropzone"]=t()}(this,function(){return function(e){function t(n){if(i[n])return i[n].exports;var o=i[n]={exports:{},id:n,loaded:!1};return e[n].call(o.exports,o,o.exports,t),o.loaded=!0,o.exports}var i={};return t.m=e,t.c=i,t.p="",t(0)}([function(e,t,i){var n,o,r={};i(8),n=i(2),o=i(6),e.exports=n||{},e.exports.__esModule&&(e.exports=e.exports.default);var s="function"==typeof e.exports?e.exports.options||(e.exports.options={}):e.exports;o&&(s.template=o),s.computed||(s.computed={}),Object.keys(r).forEach(function(e){var t=r[e];s.computed[e]=function(){return t}})},function(e,t){e.exports=function(){var e=[];return e.toString=function(){for(var e=[],t=0;t<this.length;t++){var i=this[t];i[2]?e.push("@media "+i[2]+"{"+i[1]+"}"):e.push(i[1])}return e.join("")},e.i=function(t,i){"string"==typeof t&&(t=[[null,t,""]]);for(var n={},o=0;o<this.length;o++){var r=this[o][0];"number"==typeof r&&(n[r]=!0)}for(o=0;o<t.length;o++){var s=t[o];"number"==typeof s[0]&&n[s[0]]||(i&&!s[2]?s[2]=i:i&&(s[2]="("+s[2]+") and ("+i+")"),e.push(s))}},e}},function(e,t,i){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default={props:{id:{type:String,required:!0},url:{type:String,required:!0},clickable:{type:[Boolean,String],default:!0},confirm:{type:Function,default:void 0},paramName:{type:String,default:"file"},acceptedFileTypes:{type:String},thumbnailHeight:{type:Number,default:200},thumbnailWidth:{type:Number,default:200},showRemoveLink:{type:Boolean,default:!0},maxFileSizeInMB:{type:Number,default:2},maxNumberOfFiles:{type:Number,default:5},autoProcessQueue:{type:Boolean,default:!0},useFontAwesome:{type:Boolean,default:!1},headers:{type:Object},language:{type:Object,default:function(){return{}}},previewTemplate:{type:Function,default:function(e){return'\n                    <div class="dz-preview dz-file-preview">\n                        <div class="dz-image" style="width: '+e.thumbnailWidth+"px;height: "+e.thumbnailHeight+'px">\n                        <img data-dz-thumbnail /></div>\n                        <div class="dz-details">\n                            <div class="dz-size"><span data-dz-size></span></div>\n                            <div class="dz-filename"><span data-dz-name></span></div>\n                        </div>\n                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n                        <div class="dz-error-message"><span data-dz-errormessage></span></div>\n                        <div class="dz-success-mark">'+e.doneIcon+'</div>\n                        <div class="dz-error-mark">'+e.errorIcon+"</div>\n                    </div>\n                "}},useCustomDropzoneOptions:{type:Boolean,default:!1},dropzoneOptions:{type:Object,default:function(){return{}}},resizeWidth:{type:Number,default:null},resizeHeight:{type:Number,default:null},resizeMimeType:{type:String,default:null},resizeQuality:{type:Number,default:.8},resizeMethod:{type:String,default:"contain"},uploadMultiple:{type:Boolean,default:!1},duplicateCheck:{type:Boolean,default:!1},parallelUploads:{type:Number,default:2},timeout:{type:Number,default:3e4},method:{type:String,default:"POST"},withCredentials:{type:Boolean,default:!1},capture:{type:String,default:null},hiddenInputContainer:{type:String,default:"body"}},methods:{manuallyAddFile:function(e,t,i,n,o){this.dropzone.emit("addedfile",e),this.dropzone.emit("thumbnail",e,t),this.dropzone.createThumbnailFromUrl(e,t,i,n),this.dropzone.emit("complete",e),"undefined"!=typeof o.dontSubstractMaxFiles&&o.dontSubstractMaxFiles||(this.dropzone.options.maxFiles=this.dropzone.options.maxFiles-1),"undefined"!=typeof o.addToFiles&&o.addToFiles&&this.dropzone.files.push(e),this.$emit("vdropzone-file-added-manually",e)},setOption:function(e,t){this.dropzone.options[e]=t},removeAllFiles:function(){this.dropzone.removeAllFiles(!0)},processQueue:function(){var e=this.dropzone;this.dropzone.processQueue(),this.dropzone.on("success",function(){e.options.autoProcessQueue=!0}),this.dropzone.on("queuecomplete",function(){e.options.autoProcessQueue=!1})},removeFile:function(e){this.dropzone.removeFile(e)},getAcceptedFiles:function(){return this.dropzone.getAcceptedFiles()},getRejectedFiles:function(){return this.dropzone.getRejectedFiles()},getUploadingFiles:function(){return this.dropzone.getUploadingFiles()},getQueuedFiles:function(){return this.dropzone.getQueuedFiles()},getProp:function(e,t){return this.useCustomDropzoneOptions&&void 0!==t&&null!==t&&""!==t?t:e}},computed:{languageSettings:function(){var e={dictDefaultMessage:"<br>Drop files here to upload",dictCancelUpload:"Cancel upload",dictCancelUploadConfirmation:"Are you sure you want to cancel this upload?",dictFallbackMessage:"Your browser does not support drag and drop file uploads.",dictFallbackText:"Please use the fallback form below to upload your files like in the olden days.",dictFileTooBig:"File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",dictInvalidFileType:"You can't upload files of this type.",dictMaxFilesExceeded:"You can not upload any more files. (max: {{maxFiles}})",dictRemoveFile:"Remove",dictRemoveFileConfirmation:null,dictResponseError:"Server responded with {{statusCode}} code."};for(var t in this.language)e[t]=this.language[t];if(this.useCustomDropzoneOptions&&this.dropzoneOptions.language)for(var i in this.dropzoneOptions.language)e[i]=this.dropzoneOptions.language[i];return e},cloudIcon:function(){return this.useFontAwesome?'<i class="fa fa-cloud-upload"></i>':'<i class="material-icons">cloud_upload</i>'},doneIcon:function(){return this.useFontAwesome?'<i class="fa fa-check"></i>':' <i class="material-icons">done</i>'},errorIcon:function(){return this.useFontAwesome?'<i class="fa fa-close"></i>':' <i class="material-icons">error</i>'}},mounted:function(){if(!this.$isServer){var e=i(5);e.autoDiscover=!1,this.confirm&&(e.confirm=this.getProp(this.confirm,this.dropzoneOptions.confirm));var t=document.getElementById(this.id);this.dropzone=new e(t,{clickable:this.getProp(this.clickable,this.dropzoneOptions.clickable),paramName:this.getProp(this.paramName,this.dropzoneOptions.paramName),thumbnailWidth:this.getProp(this.thumbnailWidth,this.dropzoneOptions.thumbnailWidth),thumbnailHeight:this.getProp(this.thumbnailHeight,this.dropzoneOptions.thumbnailHeight),maxFiles:this.getProp(this.maxNumberOfFiles,this.dropzoneOptions.maxNumberOfFiles),maxFilesize:this.getProp(this.maxFileSizeInMB,this.dropzoneOptions.maxFileSizeInMB),addRemoveLinks:this.getProp(this.showRemoveLink,this.dropzoneOptions.showRemoveLink),acceptedFiles:this.getProp(this.acceptedFileTypes,this.dropzoneOptions.acceptedFileTypes),autoProcessQueue:this.getProp(this.autoProcessQueue,this.dropzoneOptions.autoProcessQueue),headers:this.getProp(this.headers,this.dropzoneOptions.headers),previewTemplate:this.previewTemplate(this),dictDefaultMessage:this.cloudIcon+this.languageSettings.dictDefaultMessage,dictCancelUpload:this.languageSettings.dictCancelUpload,dictCancelUploadConfirmation:this.languageSettings.dictCancelUploadConfirmation,dictFallbackMessage:this.languageSettings.dictFallbackMessage,dictFallbackText:this.languageSettings.dictFallbackText,dictFileTooBig:this.languageSettings.dictFileTooBig,dictInvalidFileType:this.languageSettings.dictInvalidFileType,dictMaxFilesExceeded:this.languageSettings.dictMaxFilesExceeded,dictRemoveFile:this.languageSettings.dictRemoveFile,dictRemoveFileConfirmation:this.languageSettings.dictRemoveFileConfirmation,dictResponseError:this.languageSettings.dictResponseError,resizeWidth:this.getProp(this.resizeWidth,this.dropzoneOptions.resizeWidth),resizeHeight:this.getProp(this.resizeHeight,this.dropzoneOptions.resizeHeight),resizeMimeType:this.getProp(this.resizeMimeType,this.dropzoneOptions.resizeMimeType),resizeQuality:this.getProp(this.resizeQuality,this.dropzoneOptions.resizeQuality),resizeMethod:this.getProp(this.resizeMethod,this.dropzoneOptions.resizeMethod),uploadMultiple:this.getProp(this.uploadMultiple,this.dropzoneOptions.uploadMultiple),parallelUploads:this.getProp(this.parallelUploads,this.dropzoneOptions.parallelUploads),timeout:this.getProp(this.timeout,this.dropzoneOptions.timeout),method:this.getProp(this.method,this.dropzoneOptions.method),capture:this.getProp(this.capture,this.dropzoneOptions.capture),hiddenInputContainer:this.getProp(this.hiddenInputContainer,this.dropzoneOptions.hiddenInputContainer),withCredentials:this.getProp(this.withCredentials,this.dropzoneOptions.withCredentials)});var n=this;this.dropzone.on("thumbnail",function(e,t){n.$emit("vdropzone-thumbnail",e,t)}),this.dropzone.on("addedfile",function(e){if(n.duplicateCheck&&this.files.length){var t,i;for(t=0,i=this.files.length;t<i-1;t++)this.files[t].name===e.name&&(this.removeFile(e),n.$emit("duplicate-file",e))}n.$emit("vdropzone-file-added",e)}),this.dropzone.on("addedfiles",function(e){n.$emit("vdropzone-files-added",e)}),this.dropzone.on("removedfile",function(e){n.$emit("vdropzone-removed-file",e)}),this.dropzone.on("success",function(e,t){n.$emit("vdropzone-success",e,t)}),this.dropzone.on("successmultiple",function(e,t){n.$emit("vdropzone-success-multiple",e,t)}),this.dropzone.on("error",function(e,t,i){n.$emit("vdropzone-error",e,t,i)}),this.dropzone.on("sending",function(e,t,i){n.$emit("vdropzone-sending",e,t,i)}),this.dropzone.on("sendingmultiple",function(e,t,i){n.$emit("vdropzone-sending-multiple",e,t,i)}),this.dropzone.on("queuecomplete",function(e,t,i){n.$emit("vdropzone-queue-complete",e,t,i)}),this.dropzone.on("totaluploadprogress",function(e,t,i){n.$emit("vdropzone-total-upload-progress",e,t,i)}),n.$emit("vdropzone-mounted")}},beforeDestroy:function(){this.dropzone.destroy()}}},function(e,t,i){t=e.exports=i(1)(),t.push([e.id,'@-webkit-keyframes passing-through{0%{opacity:0;-webkit-transform:translateY(40px);transform:translateY(40px)}30%,70%{opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}to{opacity:0;-webkit-transform:translateY(-40px);transform:translateY(-40px)}}@keyframes passing-through{0%{opacity:0;-webkit-transform:translateY(40px);transform:translateY(40px)}30%,70%{opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}to{opacity:0;-webkit-transform:translateY(-40px);transform:translateY(-40px)}}@-webkit-keyframes slide-in{0%{opacity:0;-webkit-transform:translateY(40px);transform:translateY(40px)}30%{opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes slide-in{0%{opacity:0;-webkit-transform:translateY(40px);transform:translateY(40px)}30%{opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}}@-webkit-keyframes pulse{0%{-webkit-transform:scale(1);transform:scale(1)}10%{-webkit-transform:scale(1.1);transform:scale(1.1)}20%{-webkit-transform:scale(1);transform:scale(1)}}@keyframes pulse{0%{-webkit-transform:scale(1);transform:scale(1)}10%{-webkit-transform:scale(1.1);transform:scale(1.1)}20%{-webkit-transform:scale(1);transform:scale(1)}}.dropzone,.dropzone *{box-sizing:border-box}.dropzone{min-height:150px;border:2px solid rgba(0,0,0,.3);background:#fff;padding:20px}.dropzone.dz-clickable{cursor:pointer}.dropzone.dz-clickable *{cursor:default}.dropzone.dz-clickable .dz-message,.dropzone.dz-clickable .dz-message *{cursor:pointer}.dropzone.dz-started .dz-message{display:none}.dropzone.dz-drag-hover{border-style:solid}.dropzone.dz-drag-hover .dz-message{opacity:.5}.dropzone .dz-message{text-align:center;margin:2em 0}.dropzone .dz-preview{position:relative;display:inline-block;vertical-align:top;margin:16px;min-height:100px}.dropzone .dz-preview:hover{z-index:1000}.dropzone .dz-preview.dz-file-preview .dz-image{border-radius:20px;background:#999;background:linear-gradient(180deg,#eee,#ddd)}.dropzone .dz-preview.dz-file-preview .dz-details{opacity:1}.dropzone .dz-preview.dz-image-preview{background:#fff}.dropzone .dz-preview.dz-image-preview .dz-details{transition:opacity .2s linear}.dropzone .dz-preview .dz-remove{font-size:14px;text-align:center;display:block;cursor:pointer;border:none}.dropzone .dz-preview .dz-remove:hover{text-decoration:underline}.dropzone .dz-preview:hover .dz-details{opacity:1}.dropzone .dz-preview .dz-details{z-index:20;position:absolute;top:0;left:0;opacity:0;font-size:13px;min-width:100%;max-width:100%;padding:2em 1em;text-align:center;color:rgba(0,0,0,.9);line-height:150%}.dropzone .dz-preview .dz-details .dz-size{margin-bottom:1em;font-size:16px}.dropzone .dz-preview .dz-details .dz-filename{white-space:nowrap}.dropzone .dz-preview .dz-details .dz-filename:hover span{border:1px solid hsla(0,0%,78%,.8);background-color:hsla(0,0%,100%,.8)}.dropzone .dz-preview .dz-details .dz-filename:not(:hover){overflow:hidden;text-overflow:ellipsis}.dropzone .dz-preview .dz-details .dz-filename:not(:hover) span{border:1px solid transparent}.dropzone .dz-preview .dz-details .dz-filename span,.dropzone .dz-preview .dz-details .dz-size span{background-color:hsla(0,0%,100%,.4);padding:0 .4em;border-radius:3px}.dropzone .dz-preview:hover .dz-image img{-webkit-transform:scale(1.05);transform:scale(1.05);-webkit-filter:blur(8px);filter:blur(8px)}.dropzone .dz-preview .dz-image{border-radius:20px;overflow:hidden;width:120px;height:120px;position:relative;display:block;z-index:10}.dropzone .dz-preview .dz-image img{display:block}.dropzone .dz-preview.dz-success .dz-success-mark{-webkit-animation:passing-through 3s cubic-bezier(.77,0,.175,1);animation:passing-through 3s cubic-bezier(.77,0,.175,1)}.dropzone .dz-preview.dz-error .dz-error-mark{opacity:1;-webkit-animation:slide-in 3s cubic-bezier(.77,0,.175,1);animation:slide-in 3s cubic-bezier(.77,0,.175,1)}.dropzone .dz-preview .dz-error-mark,.dropzone .dz-preview .dz-success-mark{pointer-events:none;opacity:0;z-index:500;position:absolute;display:block;top:50%;left:50%;margin-left:-27px;margin-top:-27px}.dropzone .dz-preview .dz-error-mark svg,.dropzone .dz-preview .dz-success-mark svg{display:block;width:54px;height:54px}.dropzone .dz-preview.dz-processing .dz-progress{opacity:1;transition:all .2s linear}.dropzone .dz-preview.dz-complete .dz-progress{opacity:0;transition:opacity .4s ease-in}.dropzone .dz-preview:not(.dz-processing) .dz-progress{-webkit-animation:pulse 6s ease infinite;animation:pulse 6s ease infinite}.dropzone .dz-preview .dz-progress{opacity:1;z-index:1000;pointer-events:none;position:absolute;height:16px;left:50%;top:50%;margin-top:-8px;width:80px;margin-left:-40px;background:hsla(0,0%,100%,.9);-webkit-transform:scale(1);border-radius:8px;overflow:hidden}.dropzone .dz-preview .dz-progress .dz-upload{background:#333;background:linear-gradient(180deg,#666,#444);position:absolute;top:0;left:0;bottom:0;width:0;transition:width .3s ease-in-out}.dropzone .dz-preview.dz-error .dz-error-message{display:block}.dropzone .dz-preview.dz-error:hover .dz-error-message{opacity:1;pointer-events:auto}.dropzone .dz-preview .dz-error-message{pointer-events:none;z-index:1000;position:absolute;display:block;display:none;opacity:0;transition:opacity .3s ease;border-radius:8px;font-size:13px;top:130px;left:-10px;width:140px;background:#be2626;background:linear-gradient(180deg,#be2626,#a92222);padding:.5em 1.2em;color:#fff}.dropzone .dz-preview .dz-error-message:after{content:"";position:absolute;top:-6px;left:64px;width:0;height:0;border-left:6px solid transparent;border-right:6px solid transparent;border-bottom:6px solid #be2626}',""])},function(e,t,i){t=e.exports=i(1)(),t.i(i(3),""),t.push([e.id,".vue-dropzone{border:2px solid #e5e5e5;font-family:Arial,sans-serif;letter-spacing:.2px;color:#777;transition:background-color .2s linear}.vue-dropzone:hover{background-color:#f6f6f6}.vue-dropzone i{color:#ccc}.vue-dropzone .dz-preview .dz-image{border-radius:0}.vue-dropzone .dz-preview .dz-image:hover img{-webkit-transform:none;transform:none;-webkit-filter:none}.vue-dropzone .dz-preview .dz-details{bottom:0;top:0;color:#fff;background-color:rgba(33,150,243,.8);transition:opacity .2s linear;text-align:left}.vue-dropzone .dz-preview .dz-details .dz-filename span,.vue-dropzone .dz-preview .dz-details .dz-size span{background-color:transparent}.vue-dropzone .dz-preview .dz-details .dz-filename:not(:hover) span{border:none}.vue-dropzone .dz-preview .dz-details .dz-filename:hover span{background-color:transparent;border:none}.vue-dropzone .dz-preview .dz-progress .dz-upload{background:#ccc}.vue-dropzone .dz-preview .dz-remove{position:absolute;z-index:30;color:#fff;margin-left:15px;padding:10px;top:inherit;bottom:15px;border:2px solid #fff;text-decoration:none;text-transform:uppercase;font-size:.8rem;font-weight:800;letter-spacing:1.1px;opacity:0}.vue-dropzone .dz-preview:hover .dz-remove{opacity:1}.vue-dropzone .dz-preview .dz-error-mark,.vue-dropzone .dz-preview .dz-success-mark{margin-left:auto!important;margin-top:auto!important;width:100%!important;top:35%!important;left:0}.vue-dropzone .dz-preview .dz-error-mark i,.vue-dropzone .dz-preview .dz-success-mark i{color:#fff!important;font-size:5rem!important}.vue-dropzone .dz-preview .dz-error-message{top:75%;left:15%}",""])},function(e,t,i){(function(e){(function(){var t,i,n,o,r,s,a,l,d,p=[].slice,u=function(e,t){function i(){this.constructor=e}for(var n in t)c.call(t,n)&&(e[n]=t[n]);return i.prototype=t.prototype,e.prototype=new i,e.__super__=t.prototype,e},c={}.hasOwnProperty;l=function(){},i=function(){function e(){}return e.prototype.addEventListener=e.prototype.on,e.prototype.on=function(e,t){return this._callbacks=this._callbacks||{},this._callbacks[e]||(this._callbacks[e]=[]),this._callbacks[e].push(t),this},e.prototype.emit=function(){var e,t,i,n,o,r;if(n=arguments[0],e=2<=arguments.length?p.call(arguments,1):[],this._callbacks=this._callbacks||{},i=this._callbacks[n])for(o=0,r=i.length;o<r;o++)t=i[o],t.apply(this,e);return this},e.prototype.removeListener=e.prototype.off,e.prototype.removeAllListeners=e.prototype.off,e.prototype.removeEventListener=e.prototype.off,e.prototype.off=function(e,t){var i,n,o,r,s;if(!this._callbacks||0===arguments.length)return this._callbacks={},this;if(n=this._callbacks[e],!n)return this;if(1===arguments.length)return delete this._callbacks[e],this;for(o=r=0,s=n.length;r<s;o=++r)if(i=n[o],i===t){n.splice(o,1);break}return this},e}(),t=function(e){function t(e,i){var n,r,s;if(this.element=e,this.version=t.version,this.defaultOptions.previewTemplate=this.defaultOptions.previewTemplate.replace(/\n*/g,""),this.clickableElements=[],this.listeners=[],this.files=[],"string"==typeof this.element&&(this.element=document.querySelector(this.element)),!this.element||null==this.element.nodeType)throw new Error("Invalid dropzone element.");if(this.element.dropzone)throw new Error("Dropzone already attached.");if(t.instances.push(this),this.element.dropzone=this,n=null!=(s=t.optionsForElement(this.element))?s:{},this.options=o({},this.defaultOptions,n,null!=i?i:{}),this.options.forceFallback||!t.isBrowserSupported())return this.options.fallback.call(this);if(null==this.options.url&&(this.options.url=this.element.getAttribute("action")),!this.options.url)throw new Error("No URL provided.");if(this.options.acceptedFiles&&this.options.acceptedMimeTypes)throw new Error("You can't provide both 'acceptedFiles' and 'acceptedMimeTypes'. 'acceptedMimeTypes' is deprecated.");this.options.acceptedMimeTypes&&(this.options.acceptedFiles=this.options.acceptedMimeTypes,delete this.options.acceptedMimeTypes),null!=this.options.renameFilename&&(this.options.renameFile=function(e){return function(t){return e.options.renameFilename.call(e,t.name,t)}}(this)),this.options.method=this.options.method.toUpperCase(),(r=this.getExistingFallback())&&r.parentNode&&r.parentNode.removeChild(r),this.options.previewsContainer!==!1&&(this.options.previewsContainer?this.previewsContainer=t.getElement(this.options.previewsContainer,"previewsContainer"):this.previewsContainer=this.element),this.options.clickable&&(this.options.clickable===!0?this.clickableElements=[this.element]:this.clickableElements=t.getElements(this.options.clickable,"clickable")),this.init()}var o,r;return u(t,e),t.prototype.Emitter=i,t.prototype.events=["drop","dragstart","dragend","dragenter","dragover","dragleave","addedfile","addedfiles","removedfile","thumbnail","error","errormultiple","processing","processingmultiple","uploadprogress","totaluploadprogress","sending","sendingmultiple","success","successmultiple","canceled","canceledmultiple","complete","completemultiple","reset","maxfilesexceeded","maxfilesreached","queuecomplete"],t.prototype.defaultOptions={url:null,method:"post",withCredentials:!1,timeout:3e4,parallelUploads:2,uploadMultiple:!1,maxFilesize:256,paramName:"file",createImageThumbnails:!0,maxThumbnailFilesize:10,thumbnailWidth:120,thumbnailHeight:120,thumbnailMethod:"crop",resizeWidth:null,resizeHeight:null,resizeMimeType:null,resizeQuality:.8,resizeMethod:"contain",filesizeBase:1e3,maxFiles:null,params:{},headers:null,clickable:!0,ignoreHiddenFiles:!0,acceptedFiles:null,acceptedMimeTypes:null,autoProcessQueue:!0,autoQueue:!0,addRemoveLinks:!1,previewsContainer:null,hiddenInputContainer:"body",capture:null,renameFilename:null,renameFile:null,forceFallback:!1,dictDefaultMessage:"Drop files here to upload",dictFallbackMessage:"Your browser does not support drag'n'drop file uploads.",dictFallbackText:"Please use the fallback form below to upload your files like in the olden days.",dictFileTooBig:"File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",dictInvalidFileType:"You can't upload files of this type.",dictResponseError:"Server responded with {{statusCode}} code.",dictCancelUpload:"Cancel upload",dictCancelUploadConfirmation:"Are you sure you want to cancel this upload?",dictRemoveFile:"Remove file",dictRemoveFileConfirmation:null,dictMaxFilesExceeded:"You can not upload any more files.",dictFileSizeUnits:{tb:"TB",gb:"GB",mb:"MB",kb:"KB",b:"b"},init:function(){return l},accept:function(e,t){return t()},fallback:function(){var e,i,n,o,r,s;for(this.element.className=this.element.className+" dz-browser-not-supported",r=this.element.getElementsByTagName("div"),i=0,n=r.length;i<n;i++)e=r[i],/(^| )dz-message($| )/.test(e.className)&&(o=e,e.className="dz-message");return o||(o=t.createElement('<div class="dz-message"><span></span></div>'),this.element.appendChild(o)),s=o.getElementsByTagName("span")[0],s&&(null!=s.textContent?s.textContent=this.options.dictFallbackMessage:null!=s.innerText&&(s.innerText=this.options.dictFallbackMessage)),this.element.appendChild(this.getFallbackForm())},resize:function(e,t,i,n){var o,r,s;if(o={srcX:0,srcY:0,srcWidth:e.width,srcHeight:e.height},r=e.width/e.height,null==t&&null==i?(t=o.srcWidth,i=o.srcHeight):null==t?t=i*r:null==i&&(i=t/r),t=Math.min(t,o.srcWidth),i=Math.min(i,o.srcHeight),s=t/i,o.srcWidth>t||o.srcHeight>i)if("crop"===n)r>s?(o.srcHeight=e.height,o.srcWidth=o.srcHeight*s):(o.srcWidth=e.width,o.srcHeight=o.srcWidth/s);else{if("contain"!==n)throw new Error("Unknown resizeMethod '"+n+"'");r>s?i=t/r:t=i*r}return o.srcX=(e.width-o.srcWidth)/2,o.srcY=(e.height-o.srcHeight)/2,o.trgWidth=t,o.trgHeight=i,o},transformFile:function(e,t){return(this.options.resizeWidth||this.options.resizeHeight)&&e.type.match(/image.*/)?this.resizeImage(e,this.options.resizeWidth,this.options.resizeHeight,this.options.resizeMethod,t):t(e)},previewTemplate:'<div class="dz-preview dz-file-preview">\n  <div class="dz-image"><img data-dz-thumbnail /></div>\n  <div class="dz-details">\n    <div class="dz-size"><span data-dz-size></span></div>\n    <div class="dz-filename"><span data-dz-name></span></div>\n  </div>\n  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n  <div class="dz-error-message"><span data-dz-errormessage></span></div>\n  <div class="dz-success-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Check</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>\n      </g>\n    </svg>\n  </div>\n  <div class="dz-error-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Error</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">\n          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n</div>',drop:function(e){return this.element.classList.remove("dz-drag-hover")},dragstart:l,dragend:function(e){return this.element.classList.remove("dz-drag-hover")},dragenter:function(e){return this.element.classList.add("dz-drag-hover")},dragover:function(e){return this.element.classList.add("dz-drag-hover")},dragleave:function(e){return this.element.classList.remove("dz-drag-hover")},paste:l,reset:function(){return this.element.classList.remove("dz-started")},addedfile:function(e){var i,n,o,r,s,a,l,d,p,u,c,h,f;if(this.element===this.previewsContainer&&this.element.classList.add("dz-started"),this.previewsContainer){for(e.previewElement=t.createElement(this.options.previewTemplate.trim()),e.previewTemplate=e.previewElement,this.previewsContainer.appendChild(e.previewElement),d=e.previewElement.querySelectorAll("[data-dz-name]"),i=0,r=d.length;i<r;i++)l=d[i],l.textContent=e.name;for(p=e.previewElement.querySelectorAll("[data-dz-size]"),n=0,s=p.length;n<s;n++)l=p[n],l.innerHTML=this.filesize(e.size);for(this.options.addRemoveLinks&&(e._removeLink=t.createElement('<a class="dz-remove" href="javascript:undefined;" data-dz-remove>'+this.options.dictRemoveFile+"</a>"),e.previewElement.appendChild(e._removeLink)),c=function(i){return function(n){return n.preventDefault(),n.stopPropagation(),e.status===t.UPLOADING?t.confirm(i.options.dictCancelUploadConfirmation,function(){return i.removeFile(e)}):i.options.dictRemoveFileConfirmation?t.confirm(i.options.dictRemoveFileConfirmation,function(){return i.removeFile(e)}):i.removeFile(e)}}(this),u=e.previewElement.querySelectorAll("[data-dz-remove]"),f=[],o=0,a=u.length;o<a;o++)h=u[o],f.push(h.addEventListener("click",c));return f}},removedfile:function(e){var t;return e.previewElement&&null!=(t=e.previewElement)&&t.parentNode.removeChild(e.previewElement),this._updateMaxFilesReachedClass()},thumbnail:function(e,t){var i,n,o,r;if(e.previewElement){for(e.previewElement.classList.remove("dz-file-preview"),o=e.previewElement.querySelectorAll("[data-dz-thumbnail]"),i=0,n=o.length;i<n;i++)r=o[i],r.alt=e.name,r.src=t;return setTimeout(function(t){return function(){return e.previewElement.classList.add("dz-image-preview")}}(this),1)}},error:function(e,t){var i,n,o,r,s;if(e.previewElement){for(e.previewElement.classList.add("dz-error"),"String"!=typeof t&&t.error&&(t=t.error),r=e.previewElement.querySelectorAll("[data-dz-errormessage]"),s=[],i=0,n=r.length;i<n;i++)o=r[i],s.push(o.textContent=t);return s}},errormultiple:l,processing:function(e){if(e.previewElement&&(e.previewElement.classList.add("dz-processing"),e._removeLink))return e._removeLink.textContent=this.options.dictCancelUpload},processingmultiple:l,uploadprogress:function(e,t,i){var n,o,r,s,a;if(e.previewElement){for(s=e.previewElement.querySelectorAll("[data-dz-uploadprogress]"),a=[],n=0,o=s.length;n<o;n++)r=s[n],"PROGRESS"===r.nodeName?a.push(r.value=t):a.push(r.style.width=t+"%");return a}},totaluploadprogress:l,sending:l,sendingmultiple:l,success:function(e){if(e.previewElement)return e.previewElement.classList.add("dz-success")},successmultiple:l,canceled:function(e){return this.emit("error",e,"Upload canceled.")},canceledmultiple:l,complete:function(e){if(e._removeLink&&(e._removeLink.textContent=this.options.dictRemoveFile),e.previewElement)return e.previewElement.classList.add("dz-complete")},completemultiple:l,maxfilesexceeded:l,maxfilesreached:l,queuecomplete:l,addedfiles:l},o=function(){var e,t,i,n,o,r,s;for(r=arguments[0],o=2<=arguments.length?p.call(arguments,1):[],e=0,i=o.length;e<i;e++){n=o[e];for(t in n)s=n[t],r[t]=s}return r},t.prototype.getAcceptedFiles=function(){var e,t,i,n,o;for(n=this.files,o=[],t=0,i=n.length;t<i;t++)e=n[t],e.accepted&&o.push(e);return o},t.prototype.getRejectedFiles=function(){var e,t,i,n,o;for(n=this.files,o=[],t=0,i=n.length;t<i;t++)e=n[t],e.accepted||o.push(e);return o},t.prototype.getFilesWithStatus=function(e){var t,i,n,o,r;for(o=this.files,r=[],i=0,n=o.length;i<n;i++)t=o[i],t.status===e&&r.push(t);return r},t.prototype.getQueuedFiles=function(){return this.getFilesWithStatus(t.QUEUED)},t.prototype.getUploadingFiles=function(){return this.getFilesWithStatus(t.UPLOADING)},t.prototype.getAddedFiles=function(){return this.getFilesWithStatus(t.ADDED)},t.prototype.getActiveFiles=function(){var e,i,n,o,r;for(o=this.files,r=[],i=0,n=o.length;i<n;i++)e=o[i],e.status!==t.UPLOADING&&e.status!==t.QUEUED||r.push(e);return r},t.prototype.init=function(){var e,i,n,o,r,s,a;for("form"===this.element.tagName&&this.element.setAttribute("enctype","multipart/form-data"),this.element.classList.contains("dropzone")&&!this.element.querySelector(".dz-message")&&this.element.appendChild(t.createElement('<div class="dz-default dz-message"><span>'+this.options.dictDefaultMessage+"</span></div>")),this.clickableElements.length&&(a=function(e){return function(){return e.hiddenFileInput&&e.hiddenFileInput.parentNode.removeChild(e.hiddenFileInput),e.hiddenFileInput=document.createElement("input"),e.hiddenFileInput.setAttribute("type","file"),(null==e.options.maxFiles||e.options.maxFiles>1)&&e.hiddenFileInput.setAttribute("multiple","multiple"),e.hiddenFileInput.className="dz-hidden-input",null!=e.options.acceptedFiles&&e.hiddenFileInput.setAttribute("accept",e.options.acceptedFiles),null!=e.options.capture&&e.hiddenFileInput.setAttribute("capture",e.options.capture),
e.hiddenFileInput.style.visibility="hidden",e.hiddenFileInput.style.position="absolute",e.hiddenFileInput.style.top="0",e.hiddenFileInput.style.left="0",e.hiddenFileInput.style.height="0",e.hiddenFileInput.style.width="0",document.querySelector(e.options.hiddenInputContainer).appendChild(e.hiddenFileInput),e.hiddenFileInput.addEventListener("change",function(){var t,i,n,o;if(i=e.hiddenFileInput.files,i.length)for(n=0,o=i.length;n<o;n++)t=i[n],e.addFile(t);return e.emit("addedfiles",i),a()})}}(this))(),this.URL=null!=(r=window.URL)?r:window.webkitURL,s=this.events,i=0,n=s.length;i<n;i++)e=s[i],this.on(e,this.options[e]);return this.on("uploadprogress",function(e){return function(){return e.updateTotalUploadProgress()}}(this)),this.on("removedfile",function(e){return function(){return e.updateTotalUploadProgress()}}(this)),this.on("canceled",function(e){return function(t){return e.emit("complete",t)}}(this)),this.on("complete",function(e){return function(t){if(0===e.getAddedFiles().length&&0===e.getUploadingFiles().length&&0===e.getQueuedFiles().length)return setTimeout(function(){return e.emit("queuecomplete")},0)}}(this)),o=function(e){return e.stopPropagation(),e.preventDefault?e.preventDefault():e.returnValue=!1},this.listeners=[{element:this.element,events:{dragstart:function(e){return function(t){return e.emit("dragstart",t)}}(this),dragenter:function(e){return function(t){return o(t),e.emit("dragenter",t)}}(this),dragover:function(e){return function(t){var i;try{i=t.dataTransfer.effectAllowed}catch(e){}return t.dataTransfer.dropEffect="move"===i||"linkMove"===i?"move":"copy",o(t),e.emit("dragover",t)}}(this),dragleave:function(e){return function(t){return e.emit("dragleave",t)}}(this),drop:function(e){return function(t){return o(t),e.drop(t)}}(this),dragend:function(e){return function(t){return e.emit("dragend",t)}}(this)}}],this.clickableElements.forEach(function(e){return function(i){return e.listeners.push({element:i,events:{click:function(n){return(i!==e.element||n.target===e.element||t.elementInside(n.target,e.element.querySelector(".dz-message")))&&e.hiddenFileInput.click(),!0}}})}}(this)),this.enable(),this.options.init.call(this)},t.prototype.destroy=function(){var e;return this.disable(),this.removeAllFiles(!0),(null!=(e=this.hiddenFileInput)?e.parentNode:void 0)&&(this.hiddenFileInput.parentNode.removeChild(this.hiddenFileInput),this.hiddenFileInput=null),delete this.element.dropzone,t.instances.splice(t.instances.indexOf(this),1)},t.prototype.updateTotalUploadProgress=function(){var e,t,i,n,o,r,s,a;if(s=0,r=0,e=this.getActiveFiles(),e.length){for(o=this.getActiveFiles(),i=0,n=o.length;i<n;i++)t=o[i],s+=t.upload.bytesSent,r+=t.upload.total;a=100*s/r}else a=100;return this.emit("totaluploadprogress",a,r,s)},t.prototype._getParamName=function(e){return"function"==typeof this.options.paramName?this.options.paramName(e):""+this.options.paramName+(this.options.uploadMultiple?"["+e+"]":"")},t.prototype._renameFile=function(e){return"function"!=typeof this.options.renameFile?e.name:this.options.renameFile(e)},t.prototype.getFallbackForm=function(){var e,i,n,o;return(e=this.getExistingFallback())?e:(n='<div class="dz-fallback">',this.options.dictFallbackText&&(n+="<p>"+this.options.dictFallbackText+"</p>"),n+='<input type="file" name="'+this._getParamName(0)+'" '+(this.options.uploadMultiple?'multiple="multiple"':void 0)+' /><input type="submit" value="Upload!"></div>',i=t.createElement(n),"FORM"!==this.element.tagName?(o=t.createElement('<form action="'+this.options.url+'" enctype="multipart/form-data" method="'+this.options.method+'"></form>'),o.appendChild(i)):(this.element.setAttribute("enctype","multipart/form-data"),this.element.setAttribute("method",this.options.method)),null!=o?o:i)},t.prototype.getExistingFallback=function(){var e,t,i,n,o,r;for(t=function(e){var t,i,n;for(i=0,n=e.length;i<n;i++)if(t=e[i],/(^| )fallback($| )/.test(t.className))return t},o=["div","form"],i=0,n=o.length;i<n;i++)if(r=o[i],e=t(this.element.getElementsByTagName(r)))return e},t.prototype.setupEventListeners=function(){var e,t,i,n,o,r,s;for(r=this.listeners,s=[],i=0,n=r.length;i<n;i++)e=r[i],s.push(function(){var i,n;i=e.events,n=[];for(t in i)o=i[t],n.push(e.element.addEventListener(t,o,!1));return n}());return s},t.prototype.removeEventListeners=function(){var e,t,i,n,o,r,s;for(r=this.listeners,s=[],i=0,n=r.length;i<n;i++)e=r[i],s.push(function(){var i,n;i=e.events,n=[];for(t in i)o=i[t],n.push(e.element.removeEventListener(t,o,!1));return n}());return s},t.prototype.disable=function(){var e,t,i,n,o;for(this.clickableElements.forEach(function(e){return e.classList.remove("dz-clickable")}),this.removeEventListeners(),n=this.files,o=[],t=0,i=n.length;t<i;t++)e=n[t],o.push(this.cancelUpload(e));return o},t.prototype.enable=function(){return this.clickableElements.forEach(function(e){return e.classList.add("dz-clickable")}),this.setupEventListeners()},t.prototype.filesize=function(e){var t,i,n,o,r,s,a,l;if(r=0,s="b",e>0){for(l=["tb","gb","mb","kb","b"],i=n=0,o=l.length;n<o;i=++n)if(a=l[i],t=Math.pow(this.options.filesizeBase,4-i)/10,e>=t){r=e/Math.pow(this.options.filesizeBase,4-i),s=a;break}r=Math.round(10*r)/10}return"<strong>"+r+"</strong> "+this.options.dictFileSizeUnits[s]},t.prototype._updateMaxFilesReachedClass=function(){return null!=this.options.maxFiles&&this.getAcceptedFiles().length>=this.options.maxFiles?(this.getAcceptedFiles().length===this.options.maxFiles&&this.emit("maxfilesreached",this.files),this.element.classList.add("dz-max-files-reached")):this.element.classList.remove("dz-max-files-reached")},t.prototype.drop=function(e){var t,i;e.dataTransfer&&(this.emit("drop",e),t=e.dataTransfer.files,this.emit("addedfiles",t),t.length&&(i=e.dataTransfer.items,i&&i.length&&null!=i[0].webkitGetAsEntry?this._addFilesFromItems(i):this.handleFiles(t)))},t.prototype.paste=function(e){var t,i;if(null!=(null!=e&&null!=(i=e.clipboardData)?i.items:void 0))return this.emit("paste",e),t=e.clipboardData.items,t.length?this._addFilesFromItems(t):void 0},t.prototype.handleFiles=function(e){var t,i,n,o;for(o=[],i=0,n=e.length;i<n;i++)t=e[i],o.push(this.addFile(t));return o},t.prototype._addFilesFromItems=function(e){var t,i,n,o,r;for(r=[],n=0,o=e.length;n<o;n++)i=e[n],null!=i.webkitGetAsEntry&&(t=i.webkitGetAsEntry())?t.isFile?r.push(this.addFile(i.getAsFile())):t.isDirectory?r.push(this._addFilesFromDirectory(t,t.name)):r.push(void 0):null!=i.getAsFile&&(null==i.kind||"file"===i.kind)?r.push(this.addFile(i.getAsFile())):r.push(void 0);return r},t.prototype._addFilesFromDirectory=function(e,t){var i,n,o;return i=e.createReader(),n=function(e){return"undefined"!=typeof console&&null!==console&&"function"==typeof console.log?console.log(e):void 0},(o=function(e){return function(){return i.readEntries(function(i){var n,r,s;if(i.length>0){for(r=0,s=i.length;r<s;r++)n=i[r],n.isFile?n.file(function(i){if(!e.options.ignoreHiddenFiles||"."!==i.name.substring(0,1))return i.fullPath=t+"/"+i.name,e.addFile(i)}):n.isDirectory&&e._addFilesFromDirectory(n,t+"/"+n.name);o()}return null},n)}}(this))()},t.prototype.accept=function(e,i){return e.size>1024*this.options.maxFilesize*1024?i(this.options.dictFileTooBig.replace("{{filesize}}",Math.round(e.size/1024/10.24)/100).replace("{{maxFilesize}}",this.options.maxFilesize)):t.isValidFile(e,this.options.acceptedFiles)?null!=this.options.maxFiles&&this.getAcceptedFiles().length>=this.options.maxFiles?(i(this.options.dictMaxFilesExceeded.replace("{{maxFiles}}",this.options.maxFiles)),this.emit("maxfilesexceeded",e)):this.options.accept.call(this,e,i):i(this.options.dictInvalidFileType)},t.prototype.addFile=function(e){return e.upload={progress:0,total:e.size,bytesSent:0,filename:this._renameFile(e)},this.files.push(e),e.status=t.ADDED,this.emit("addedfile",e),this._enqueueThumbnail(e),this.accept(e,function(t){return function(i){return i?(e.accepted=!1,t._errorProcessing([e],i)):(e.accepted=!0,t.options.autoQueue&&t.enqueueFile(e)),t._updateMaxFilesReachedClass()}}(this))},t.prototype.enqueueFiles=function(e){var t,i,n;for(i=0,n=e.length;i<n;i++)t=e[i],this.enqueueFile(t);return null},t.prototype.enqueueFile=function(e){if(e.status!==t.ADDED||e.accepted!==!0)throw new Error("This file can't be queued because it has already been processed or was rejected.");if(e.status=t.QUEUED,this.options.autoProcessQueue)return setTimeout(function(e){return function(){return e.processQueue()}}(this),0)},t.prototype._thumbnailQueue=[],t.prototype._processingThumbnail=!1,t.prototype._enqueueThumbnail=function(e){if(this.options.createImageThumbnails&&e.type.match(/image.*/)&&e.size<=1024*this.options.maxThumbnailFilesize*1024)return this._thumbnailQueue.push(e),setTimeout(function(e){return function(){return e._processThumbnailQueue()}}(this),0)},t.prototype._processThumbnailQueue=function(){var e;if(!this._processingThumbnail&&0!==this._thumbnailQueue.length)return this._processingThumbnail=!0,e=this._thumbnailQueue.shift(),this.createThumbnail(e,this.options.thumbnailWidth,this.options.thumbnailHeight,this.options.thumbnailMethod,!0,function(t){return function(i){return t.emit("thumbnail",e,i),t._processingThumbnail=!1,t._processThumbnailQueue()}}(this))},t.prototype.removeFile=function(e){if(e.status===t.UPLOADING&&this.cancelUpload(e),this.files=d(this.files,e),this.emit("removedfile",e),0===this.files.length)return this.emit("reset")},t.prototype.removeAllFiles=function(e){var i,n,o,r;for(null==e&&(e=!1),r=this.files.slice(),n=0,o=r.length;n<o;n++)i=r[n],(i.status!==t.UPLOADING||e)&&this.removeFile(i);return null},t.prototype.resizeImage=function(e,i,o,r,s){return this.createThumbnail(e,i,o,r,!1,function(i){return function(o,r){var a,l;return null===r?s(e):(a=i.options.resizeMimeType,null==a&&(a=e.type),l=r.toDataURL(a,i.options.resizeQuality),"image/jpeg"!==a&&"image/jpg"!==a||(l=n.restore(e.dataURL,l)),s(t.dataURItoBlob(l)))}}(this))},t.prototype.createThumbnail=function(e,t,i,n,o,r){var s;return s=new FileReader,s.onload=function(a){return function(){return e.dataURL=s.result,"image/svg+xml"===e.type?void(null!=r&&r(s.result)):a.createThumbnailFromUrl(e,t,i,n,o,r)}}(this),s.readAsDataURL(e)},t.prototype.createThumbnailFromUrl=function(e,t,i,n,o,r,s){var l;return l=document.createElement("img"),s&&(l.crossOrigin=s),l.onload=function(s){return function(){var d;return d=function(e){return e(1)},"undefined"!=typeof EXIF&&null!==EXIF&&o&&(d=function(e){return EXIF.getData(l,function(){return e(EXIF.getTag(this,"Orientation"))})}),d(function(o){var d,p,u,c,h,f,m,g;switch(e.width=l.width,e.height=l.height,m=s.options.resize.call(s,e,t,i,n),d=document.createElement("canvas"),p=d.getContext("2d"),d.width=m.trgWidth,d.height=m.trgHeight,o>4&&(d.width=m.trgHeight,d.height=m.trgWidth),o){case 2:p.translate(d.width,0),p.scale(-1,1);break;case 3:p.translate(d.width,d.height),p.rotate(Math.PI);break;case 4:p.translate(0,d.height),p.scale(1,-1);break;case 5:p.rotate(.5*Math.PI),p.scale(1,-1);break;case 6:p.rotate(.5*Math.PI),p.translate(0,-d.height);break;case 7:p.rotate(.5*Math.PI),p.translate(d.width,-d.height),p.scale(-1,1);break;case 8:p.rotate(-.5*Math.PI),p.translate(-d.width,0)}if(a(p,l,null!=(u=m.srcX)?u:0,null!=(c=m.srcY)?c:0,m.srcWidth,m.srcHeight,null!=(h=m.trgX)?h:0,null!=(f=m.trgY)?f:0,m.trgWidth,m.trgHeight),g=d.toDataURL("image/png"),null!=r)return r(g,d)})}}(this),null!=r&&(l.onerror=r),l.src=e.dataURL},t.prototype.processQueue=function(){var e,t,i,n;if(t=this.options.parallelUploads,i=this.getUploadingFiles().length,e=i,!(i>=t)&&(n=this.getQueuedFiles(),n.length>0)){if(this.options.uploadMultiple)return this.processFiles(n.slice(0,t-i));for(;e<t;){if(!n.length)return;this.processFile(n.shift()),e++}}},t.prototype.processFile=function(e){return this.processFiles([e])},t.prototype.processFiles=function(e){var i,n,o;for(n=0,o=e.length;n<o;n++)i=e[n],i.processing=!0,i.status=t.UPLOADING,this.emit("processing",i);return this.options.uploadMultiple&&this.emit("processingmultiple",e),this.uploadFiles(e)},t.prototype._getFilesWithXhr=function(e){var t,i;return i=function(){var i,n,o,r;for(o=this.files,r=[],i=0,n=o.length;i<n;i++)t=o[i],t.xhr===e&&r.push(t);return r}.call(this)},t.prototype.cancelUpload=function(e){var i,n,o,r,s,a,l;if(e.status===t.UPLOADING){for(n=this._getFilesWithXhr(e.xhr),o=0,s=n.length;o<s;o++)i=n[o],i.status=t.CANCELED;for(e.xhr.abort(),r=0,a=n.length;r<a;r++)i=n[r],this.emit("canceled",i);this.options.uploadMultiple&&this.emit("canceledmultiple",n)}else(l=e.status)!==t.ADDED&&l!==t.QUEUED||(e.status=t.CANCELED,this.emit("canceled",e),this.options.uploadMultiple&&this.emit("canceledmultiple",[e]));if(this.options.autoProcessQueue)return this.processQueue()},r=function(){var e,t;return t=arguments[0],e=2<=arguments.length?p.call(arguments,1):[],"function"==typeof t?t.apply(this,e):t},t.prototype.uploadFile=function(e){return this.uploadFiles([e])},t.prototype.uploadFiles=function(e){var i,n,s,a,l,d,p,u,c,h,f,m,g,v,z,b,y,w,F,x,k,E,C,T,S,M,A,L,I,O,U,N,P,R,D,_,B;for(B=new XMLHttpRequest,g=0,y=e.length;g<y;g++)s=e[g],s.xhr=B;E=r(this.options.method,e),D=r(this.options.url,e),B.open(E,D,!0),B.timeout=r(this.options.timeout,e),B.withCredentials=!!this.options.withCredentials,N=null,l=function(t){return function(){var i,n,o;for(o=[],i=0,n=e.length;i<n;i++)s=e[i],o.push(t._errorProcessing(e,N||t.options.dictResponseError.replace("{{statusCode}}",B.status),B));return o}}(this),R=function(t){return function(i){var n,o,r,a,l,d,p,u,c;if(null!=i)for(u=100*i.loaded/i.total,o=0,a=e.length;o<a;o++)s=e[o],s.upload.progress=u,s.upload.total=i.total,s.upload.bytesSent=i.loaded;else{for(n=!0,u=100,r=0,l=e.length;r<l;r++)s=e[r],100===s.upload.progress&&s.upload.bytesSent===s.upload.total||(n=!1),s.upload.progress=u,s.upload.bytesSent=s.upload.total;if(n)return}for(c=[],p=0,d=e.length;p<d;p++)s=e[p],c.push(t.emit("uploadprogress",s,u,s.upload.bytesSent));return c}}(this),B.onload=function(i){return function(n){var o;if(e[0].status!==t.CANCELED&&4===B.readyState){if("arraybuffer"!==B.responseType&&"blob"!==B.responseType&&(N=B.responseText,B.getResponseHeader("content-type")&&~B.getResponseHeader("content-type").indexOf("application/json")))try{N=JSON.parse(N)}catch(e){n=e,N="Invalid JSON response from server."}return R(),200<=(o=B.status)&&o<300?i._finished(e,N,n):l()}}}(this),B.onerror=function(i){return function(){if(e[0].status!==t.CANCELED)return l()}}(this),S=null!=(M=B.upload)?M:B,S.onprogress=R,u={Accept:"application/json","Cache-Control":"no-cache","X-Requested-With":"XMLHttpRequest"},this.options.headers&&o(u,this.options.headers);for(d in u)p=u[d],p&&B.setRequestHeader(d,p);if(a=new FormData,this.options.params){A=this.options.params;for(z in A)_=A[z],a.append(z,_)}for(v=0,w=e.length;v<w;v++)s=e[v],this.emit("sending",s,B,a);if(this.options.uploadMultiple&&this.emit("sendingmultiple",e,B,a),"FORM"===this.element.tagName)for(L=this.element.querySelectorAll("input, textarea, select, button"),b=0,F=L.length;b<F;b++)if(h=L[b],f=h.getAttribute("name"),m=h.getAttribute("type"),"SELECT"===h.tagName&&h.hasAttribute("multiple"))for(I=h.options,k=0,x=I.length;k<x;k++)T=I[k],T.selected&&a.append(f,T.value);else(!m||"checkbox"!==(O=m.toLowerCase())&&"radio"!==O||h.checked)&&a.append(f,h.value);for(i=0,P=[],c=C=0,U=e.length-1;0<=U?C<=U:C>=U;c=0<=U?++C:--C)n=function(t){return function(n,o,r){return function(n){if(a.append(o,n,r),++i===e.length)return t.submitRequest(B,a,e)}}}(this),P.push(this.options.transformFile.call(this,e[c],n(e[c],this._getParamName(c),e[c].upload.filename)));return P},t.prototype.submitRequest=function(e,t,i){return e.send(t)},t.prototype._finished=function(e,i,n){var o,r,s;for(r=0,s=e.length;r<s;r++)o=e[r],o.status=t.SUCCESS,this.emit("success",o,i,n),this.emit("complete",o);if(this.options.uploadMultiple&&(this.emit("successmultiple",e,i,n),this.emit("completemultiple",e)),this.options.autoProcessQueue)return this.processQueue()},t.prototype._errorProcessing=function(e,i,n){var o,r,s;for(r=0,s=e.length;r<s;r++)o=e[r],o.status=t.ERROR,this.emit("error",o,i,n),this.emit("complete",o);if(this.options.uploadMultiple&&(this.emit("errormultiple",e,i,n),this.emit("completemultiple",e)),this.options.autoProcessQueue)return this.processQueue()},t}(i),t.version="5.1.1",t.options={},t.optionsForElement=function(e){return e.getAttribute("id")?t.options[o(e.getAttribute("id"))]:void 0},t.instances=[],t.forElement=function(e){if("string"==typeof e&&(e=document.querySelector(e)),null==(null!=e?e.dropzone:void 0))throw new Error("No Dropzone found for given element. This is probably because you're trying to access it before Dropzone had the time to initialize. Use the `init` option to setup any additional observers on your Dropzone.");return e.dropzone},t.autoDiscover=!0,t.discover=function(){var e,i,n,o,r,s;for(document.querySelectorAll?n=document.querySelectorAll(".dropzone"):(n=[],e=function(e){var t,i,o,r;for(r=[],i=0,o=e.length;i<o;i++)t=e[i],/(^| )dropzone($| )/.test(t.className)?r.push(n.push(t)):r.push(void 0);return r},e(document.getElementsByTagName("div")),e(document.getElementsByTagName("form"))),s=[],o=0,r=n.length;o<r;o++)i=n[o],t.optionsForElement(i)!==!1?s.push(new t(i)):s.push(void 0);return s},t.blacklistedBrowsers=[/opera.*Macintosh.*version\/12/i],t.isBrowserSupported=function(){var e,i,n,o,r;if(e=!0,window.File&&window.FileReader&&window.FileList&&window.Blob&&window.FormData&&document.querySelector)if("classList"in document.createElement("a"))for(o=t.blacklistedBrowsers,i=0,n=o.length;i<n;i++)r=o[i],r.test(navigator.userAgent)&&(e=!1);else e=!1;else e=!1;return e},t.dataURItoBlob=function(e){var t,i,n,o,r,s,a;for(i=atob(e.split(",")[1]),s=e.split(",")[0].split(":")[1].split(";")[0],t=new ArrayBuffer(i.length),o=new Uint8Array(t),n=r=0,a=i.length;0<=a?r<=a:r>=a;n=0<=a?++r:--r)o[n]=i.charCodeAt(n);return new Blob([t],{type:s})},d=function(e,t){var i,n,o,r;for(r=[],n=0,o=e.length;n<o;n++)i=e[n],i!==t&&r.push(i);return r},o=function(e){return e.replace(/[\-_](\w)/g,function(e){return e.charAt(1).toUpperCase()})},t.createElement=function(e){var t;return t=document.createElement("div"),t.innerHTML=e,t.childNodes[0]},t.elementInside=function(e,t){if(e===t)return!0;for(;e=e.parentNode;)if(e===t)return!0;return!1},t.getElement=function(e,t){var i;if("string"==typeof e?i=document.querySelector(e):null!=e.nodeType&&(i=e),null==i)throw new Error("Invalid `"+t+"` option provided. Please provide a CSS selector or a plain HTML element.");return i},t.getElements=function(e,t){var i,n,o,r,s,a,l,d;if(e instanceof Array){o=[];try{for(r=0,a=e.length;r<a;r++)n=e[r],o.push(this.getElement(n,t))}catch(e){i=e,o=null}}else if("string"==typeof e)for(o=[],d=document.querySelectorAll(e),s=0,l=d.length;s<l;s++)n=d[s],o.push(n);else null!=e.nodeType&&(o=[e]);if(null==o||!o.length)throw new Error("Invalid `"+t+"` option provided. Please provide a CSS selector, a plain HTML element or a list of those.");return o},t.confirm=function(e,t,i){return window.confirm(e)?t():null!=i?i():void 0},t.isValidFile=function(e,t){var i,n,o,r,s;if(!t)return!0;for(t=t.split(","),r=e.type,i=r.replace(/\/.*$/,""),n=0,o=t.length;n<o;n++)if(s=t[n],s=s.trim(),"."===s.charAt(0)){if(e.name.toLowerCase().indexOf(s.toLowerCase(),e.name.length-s.length)!==-1)return!0}else if(/\/\*$/.test(s)){if(i===s.replace(/\/.*$/,""))return!0}else if(r===s)return!0;return!1},"undefined"!=typeof jQuery&&null!==jQuery&&(jQuery.fn.dropzone=function(e){return this.each(function(){return new t(this,e)})}),"undefined"!=typeof e&&null!==e?e.exports=t:window.Dropzone=t,t.ADDED="added",t.QUEUED="queued",t.ACCEPTED=t.QUEUED,t.UPLOADING="uploading",t.PROCESSING=t.UPLOADING,t.CANCELED="canceled",t.ERROR="error",t.SUCCESS="success",s=function(e){var t,i,n,o,r,s,a,l,d,p;for(a=e.naturalWidth,s=e.naturalHeight,i=document.createElement("canvas"),i.width=1,i.height=s,n=i.getContext("2d"),n.drawImage(e,0,0),o=n.getImageData(1,0,1,s).data,p=0,r=s,l=s;l>p;)t=o[4*(l-1)+3],0===t?r=l:p=l,l=r+p>>1;return d=l/s,0===d?1:d},a=function(e,t,i,n,o,r,a,l,d,p){var u;return u=s(t),e.drawImage(t,i,n,o,r,a,l,d,p/u)},n=function(){function e(){}return e.KEY_STR="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",e.encode64=function(e){var t,i,n,o,r,s,a,l,d;for(d="",t=void 0,i=void 0,n="",o=void 0,r=void 0,s=void 0,a="",l=0;;)if(t=e[l++],i=e[l++],n=e[l++],o=t>>2,r=(3&t)<<4|i>>4,s=(15&i)<<2|n>>6,a=63&n,isNaN(i)?s=a=64:isNaN(n)&&(a=64),d=d+this.KEY_STR.charAt(o)+this.KEY_STR.charAt(r)+this.KEY_STR.charAt(s)+this.KEY_STR.charAt(a),t=i=n="",o=r=s=a="",!(l<e.length))break;return d},e.restore=function(e,t){var i,n,o;return e.match("data:image/jpeg;base64,")?(n=this.decode64(e.replace("data:image/jpeg;base64,","")),o=this.slice2Segments(n),i=this.exifManipulation(t,o),"data:image/jpeg;base64,"+this.encode64(i)):t},e.exifManipulation=function(e,t){var i,n,o;return n=this.getExifArray(t),o=this.insertExif(e,n),i=new Uint8Array(o)},e.getExifArray=function(e){var t,i;for(t=void 0,i=0;i<e.length;){if(t=e[i],255===t[0]&225===t[1])return t;i++}return[]},e.insertExif=function(e,t){var i,n,o,r,s,a;return r=e.replace("data:image/jpeg;base64,",""),o=this.decode64(r),a=o.indexOf(255,3),s=o.slice(0,a),n=o.slice(a),i=s,i=i.concat(t),i=i.concat(n)},e.slice2Segments=function(e){var t,i,n,o,r;for(i=0,r=[];;){if(255===e[i]&218===e[i+1])break;if(255===e[i]&216===e[i+1]?i+=2:(n=256*e[i+2]+e[i+3],t=i+n+2,o=e.slice(i,t),r.push(o),i=t),i>e.length)break}return r},e.decode64=function(e){var t,i,n,o,r,s,a,l,d,p,u;for(u="",n=void 0,o=void 0,r="",s=void 0,a=void 0,l=void 0,d="",p=0,i=[],t=/[^A-Za-z0-9\+\/\=]/g,t.exec(e)&&console.warning("There were invalid base64 characters in the input text.\nValid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\nExpect errors in decoding."),e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");;)if(s=this.KEY_STR.indexOf(e.charAt(p++)),a=this.KEY_STR.indexOf(e.charAt(p++)),l=this.KEY_STR.indexOf(e.charAt(p++)),d=this.KEY_STR.indexOf(e.charAt(p++)),n=s<<2|a>>4,o=(15&a)<<4|l>>2,r=(3&l)<<6|d,i.push(n),64!==l&&i.push(o),64!==d&&i.push(r),n=o=r="",s=a=l=d="",!(p<e.length))break;return i},e}(),r=function(e,t){var i,n,o,r,s,a,l,d,p;if(o=!1,p=!0,n=e.document,d=n.documentElement,i=n.addEventListener?"addEventListener":"attachEvent",l=n.addEventListener?"removeEventListener":"detachEvent",a=n.addEventListener?"":"on",r=function(i){if("readystatechange"!==i.type||"complete"===n.readyState)return("load"===i.type?e:n)[l](a+i.type,r,!1),!o&&(o=!0)?t.call(e,i.type||i):void 0},s=function(){var e;try{d.doScroll("left")}catch(t){return e=t,void setTimeout(s,50)}return r("poll")},"complete"!==n.readyState){if(n.createEventObject&&d.doScroll){try{p=!e.frameElement}catch(e){}p&&s()}return n[i](a+"DOMContentLoaded",r,!1),n[i](a+"readystatechange",r,!1),e[i](a+"load",r,!1)}},t._autoDiscoverFunction=function(){if(t.autoDiscover)return t.discover()},r(window,t._autoDiscoverFunction)}).call(this)}).call(t,i(9)(e))},function(e,t){e.exports=' <form :action=url class="vue-dropzone dropzone" :id=id> <slot></slot> </form> '},function(e,t,i){function n(e,t){for(var i=0;i<e.length;i++){var n=e[i],o=u[n.id];if(o){o.refs++;for(var r=0;r<o.parts.length;r++)o.parts[r](n.parts[r]);for(;r<n.parts.length;r++)o.parts.push(l(n.parts[r],t))}else{for(var s=[],r=0;r<n.parts.length;r++)s.push(l(n.parts[r],t));u[n.id]={id:n.id,refs:1,parts:s}}}}function o(e){for(var t=[],i={},n=0;n<e.length;n++){var o=e[n],r=o[0],s=o[1],a=o[2],l=o[3],d={css:s,media:a,sourceMap:l};i[r]?i[r].parts.push(d):t.push(i[r]={id:r,parts:[d]})}return t}function r(e,t){var i=f(),n=v[v.length-1];if("top"===e.insertAt)n?n.nextSibling?i.insertBefore(t,n.nextSibling):i.appendChild(t):i.insertBefore(t,i.firstChild),v.push(t);else{if("bottom"!==e.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");i.appendChild(t)}}function s(e){e.parentNode.removeChild(e);var t=v.indexOf(e);t>=0&&v.splice(t,1)}function a(e){var t=document.createElement("style");return t.type="text/css",r(e,t),t}function l(e,t){var i,n,o;if(t.singleton){var r=g++;i=m||(m=a(t)),n=d.bind(null,i,r,!1),o=d.bind(null,i,r,!0)}else i=a(t),n=p.bind(null,i),o=function(){s(i)};return n(e),function(t){if(t){if(t.css===e.css&&t.media===e.media&&t.sourceMap===e.sourceMap)return;n(e=t)}else o()}}function d(e,t,i,n){var o=i?"":n.css;if(e.styleSheet)e.styleSheet.cssText=z(t,o);else{var r=document.createTextNode(o),s=e.childNodes;s[t]&&e.removeChild(s[t]),s.length?e.insertBefore(r,s[t]):e.appendChild(r)}}function p(e,t){var i=t.css,n=t.media,o=t.sourceMap;if(n&&e.setAttribute("media",n),o&&(i+="\n/*# sourceURL="+o.sources[0]+" */",i+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(o))))+" */"),e.styleSheet)e.styleSheet.cssText=i;else{for(;e.firstChild;)e.removeChild(e.firstChild);e.appendChild(document.createTextNode(i))}}var u={},c=function(e){var t;return function(){return"undefined"==typeof t&&(t=e.apply(this,arguments)),t}},h=c(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),f=c(function(){return document.head||document.getElementsByTagName("head")[0]}),m=null,g=0,v=[];e.exports=function(e,t){t=t||{},"undefined"==typeof t.singleton&&(t.singleton=h()),"undefined"==typeof t.insertAt&&(t.insertAt="bottom");var i=o(e);return n(i,t),function(e){for(var r=[],s=0;s<i.length;s++){var a=i[s],l=u[a.id];l.refs--,r.push(l)}if(e){var d=o(e);n(d,t)}for(var s=0;s<r.length;s++){var l=r[s];if(0===l.refs){for(var p=0;p<l.parts.length;p++)l.parts[p]();delete u[l.id]}}}};var z=function(){var e=[];return function(t,i){return e[t]=i,e.filter(Boolean).join("\n")}}()},function(e,t,i){var n=i(4);"string"==typeof n&&(n=[[e.id,n,""]]);i(7)(n,{});n.locals&&(e.exports=n.locals)},function(e,t){e.exports=function(e){return e.webpackPolyfill||(e.deprecate=function(){},e.paths=[],e.children=[],e.webpackPolyfill=1),e}}])});

/***/ }),

/***/ 380:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(280),
  /* template */
  __webpack_require__(427),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\topwyc\\resources\\assets\\js\\cpanel\\visa\\components\\Gallery\\AlbumList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AlbumList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-702e65a0", Component.options)
  } else {
    hotAPI.reload("data-v-702e65a0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 382:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(282),
  /* template */
  __webpack_require__(408),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\topwyc\\resources\\assets\\js\\cpanel\\visa\\components\\Gallery\\Pagination.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Pagination.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3c2dcab7", Component.options)
  } else {
    hotAPI.reload("data-v-3c2dcab7", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(283),
  /* template */
  __webpack_require__(442),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\topwyc\\resources\\assets\\js\\cpanel\\visa\\components\\Gallery\\Projects.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Projects.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bc044e52", Component.options)
  } else {
    hotAPI.reload("data-v-bc044e52", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 408:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(!_vm.client) ? _c('nav', {
    staticClass: "pagination"
  }, [_c('span', {
    staticClass: "page-stats"
  }, [_vm._v(_vm._s(_vm.pagination.from) + " - " + _vm._s(_vm.pagination.to) + " of " + _vm._s(_vm.pagination.total))]), _vm._v(" "), (_vm.pagination.prevPageUrl) ? _c('a', {
    staticClass: "btn btn-default pagination-previous",
    on: {
      "click": function($event) {
        _vm.$emit('prev');
      }
    }
  }, [_vm._v("\n          Prev\n      ")]) : _c('a', {
    staticClass: "btn btn-default pagination-previous",
    attrs: {
      "disabled": true
    }
  }, [_vm._v("\n         Prev\n      ")]), _vm._v(" "), (_vm.pagination.nextPageUrl) ? _c('a', {
    staticClass: "btn btn-default pagination-next",
    on: {
      "click": function($event) {
        _vm.$emit('next');
      }
    }
  }, [_vm._v("\n          Next\n      ")]) : _c('a', {
    staticClass: "btn btn-default pagination-next",
    attrs: {
      "disabled": true
    }
  }, [_vm._v("\n          Next\n      ")])]) : _c('nav', {
    staticClass: "pagination"
  }, [_c('span', {
    staticClass: "page-stats"
  }, [_vm._v("\n          " + _vm._s(_vm.pagination.from) + " - " + _vm._s(_vm.pagination.to) + " of " + _vm._s(_vm.filtered.length) + "\n          "), (_vm.filtered.length < _vm.pagination.total) ? _c('span', [_vm._v("(filtered from " + _vm._s(_vm.pagination.total) + " total entries)")]) : _vm._e()]), _vm._v(" "), (_vm.pagination.prevPage) ? _c('a', {
    staticClass: "btn btn-default pagination-previous",
    on: {
      "click": function($event) {
        _vm.$emit('prev');
      }
    }
  }, [_vm._v("\n          Prev\n      ")]) : _c('a', {
    staticClass: "btn btn-default pagination-previous",
    attrs: {
      "disabled": true
    }
  }, [_vm._v("\n         Prev\n      ")]), _vm._v(" "), (_vm.pagination.nextPage) ? _c('a', {
    staticClass: "btn btn-default pagination-next",
    on: {
      "click": function($event) {
        _vm.$emit('next');
      }
    }
  }, [_vm._v("\n          Next\n      ")]) : _c('a', {
    staticClass: "btn btn-default pagination-next",
    attrs: {
      "disabled": true
    }
  }, [_vm._v("\n          Next\n      ")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3c2dcab7", module.exports)
  }
}

/***/ }),

/***/ 427:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('table', {
    staticClass: "table table-hover"
  }, [_c('thead', [_c('tr', _vm._l((_vm.columns), function(column) {
    return _c('th', {
      key: column.title,
      class: _vm.sortKey === column.title ? (_vm.sortOrders[column.title] > 0 ? 'sorting_asc' : 'sorting_desc') : 'sorting',
      style: ('width:' + column.width + ';' + 'cursor:pointer;'),
      on: {
        "click": function($event) {
          _vm.$emit('sort', column.title)
        }
      }
    }, [_vm._v("\n              " + _vm._s(column.label) + "\n          ")])
  }), 0)]), _vm._v(" "), _vm._t("default")], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-702e65a0", module.exports)
  }
}

/***/ }),

/***/ 442:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "modal fade",
    attrs: {
      "id": "dialogAddAlbum",
      "aria-hidden": "true"
    }
  }, [_c('div', {
    staticClass: "modal-dialog"
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-body"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-12"
  }, [_c('h3', {
    staticClass: "m-t-none m-b"
  }, [_vm._v("Add new album")]), _vm._v(" "), _c('form', {
    attrs: {
      "role": "form",
      "action": "#",
      "method": "POST"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        _vm.saveAlbum(_vm.act, '')
      }
    }
  }, [(_vm.error) ? _c('p', {
    staticStyle: {
      "color": "red"
    }
  }, [_vm._v("Title cannot duplicate or empty")]) : _vm._e(), _vm._v(" "), (_vm.error2) ? _c('p', {
    staticStyle: {
      "color": "red"
    }
  }, [_vm._v("Album already exist")]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.album.title),
      expression: "album.title"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "title",
      "id": "title",
      "type": "text",
      "placeholder": "Title"
    },
    domProps: {
      "value": (_vm.album.title)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.album, "title", $event.target.value)
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-6 col-sm-offset-6"
  }, [(_vm.act == 'Add') ? _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.saveAlbum(_vm.act, '')
      }
    }
  }, [_vm._v("Save Changes")]) : _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.saveAlbum(_vm.act, _vm.album.id)
      }
    }
  }, [_vm._v("Save Changes")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-white pull-right",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.resetAll()
      }
    }
  }, [_vm._v("Cancel")])])])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('a', {
    staticClass: "btn btn-primary btn-sm pull-right",
    on: {
      "click": function($event) {
        _vm.actionDoc('add', '')
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-plus fa-fw"
  }), _vm._v("Add New Album")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "ibox float-e-margins"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "ibox-content"
  }, [_c('div', {
    staticClass: "table-responsive"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "ibox float-e-margins"
  }, [_c('div', {
    staticClass: "ibox-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-1 m-b-xs"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.tableData.length),
      expression: "tableData.length"
    }],
    staticClass: "input-sm form-control input-s-sm inline",
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.tableData, "length", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.perPage), function(records, index) {
    return _c('option', {
      key: index,
      domProps: {
        "value": records
      }
    }, [_vm._v(_vm._s(records))])
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-offset-7 col-sm-4"
  }, [_c('div', {
    staticClass: "input-group pull-right"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.tableData.search),
      expression: "tableData.search"
    }],
    staticClass: "input-sm form-control",
    attrs: {
      "type": "text",
      "placeholder": "Search"
    },
    domProps: {
      "value": (_vm.tableData.search)
    },
    on: {
      "input": [function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.tableData, "search", $event.target.value)
      }, function($event) {
        _vm.getAlbums()
      }]
    }
  })])])]), _vm._v(" "), _c('AlbumTable', {
    attrs: {
      "columns": _vm.columns,
      "sortKey": _vm.sortKey,
      "sortOrders": _vm.sortOrders
    },
    on: {
      "sort": _vm.sortBy
    }
  }, [(_vm.projects.length != 0) ? _c('tbody', _vm._l((_vm.projects), function(project) {
    return _c('tr', {
      key: project.id
    }, [_c('td', [_c('a', {
      attrs: {
        "href": _vm.url + project.id
      }
    }, [_vm._v(_vm._s(project.title))])]), _vm._v(" "), _c('td', [_c('div', {
      staticClass: "form-group"
    }, [_c('button', {
      staticClass: "btn btn-danger btn-xs pull-right",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.deleteAlbum(project.id)
        }
      }
    }, [_c('span', {
      staticClass: "glyphicon glyphicon-trash"
    })]), _vm._v(" "), _c('button', {
      staticClass: "btn btn-primary btn-xs pull-right",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.actionDoc('edit', project.id)
        }
      }
    }, [_c('span', {
      staticClass: "glyphicon glyphicon-pencil"
    })])])])])
  }), 0) : _c('tbody', [_c('tr', [_c('td', [_vm._v("\n                                              No record found\n                                            ")])])])]), _vm._v(" "), _c('pagination', {
    attrs: {
      "pagination": _vm.pagination
    },
    on: {
      "prev": function($event) {
        _vm.getProjects(_vm.pagination.prevPageUrl)
      },
      "next": function($event) {
        _vm.getProjects(_vm.pagination.nextPageUrl)
      }
    }
  })], 1)])])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "ibox-title"
  }, [_c('h3', [_vm._v("List of Albums")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-bc044e52", module.exports)
  }
}

/***/ }),

/***/ 503:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(194);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjNjY2E2MWJlMjViZjgzNzk5OGY/ZmNiNCoqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9nYWxsZXJ5L2FsYnVtLmpzIiwid2VicGFjazovLy8uL34vdnVlLXJlc291cmNlL2Rpc3QvdnVlLXJlc291cmNlLmVzbS5qcz8wMWFkIiwid2VicGFjazovLy9nb3QgKGlnbm9yZWQpP2I4NmYiLCJ3ZWJwYWNrOi8vL0FsYnVtTGlzdC52dWUiLCJ3ZWJwYWNrOi8vL1BhZ2luYXRpb24udnVlP2E3YzYiLCJ3ZWJwYWNrOi8vL1Byb2plY3RzLnZ1ZT83ZDIwIiwid2VicGFjazovLy8uL34vdnVlMi1kcm9wem9uZS9kaXN0L3Z1ZTItZHJvcHpvbmUuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0dhbGxlcnkvQWxidW1MaXN0LnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvR2FsbGVyeS9QYWdpbmF0aW9uLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvR2FsbGVyeS9Qcm9qZWN0cy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0dhbGxlcnkvUGFnaW5hdGlvbi52dWU/YjY3MCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvR2FsbGVyeS9BbGJ1bUxpc3QudnVlP2NmNDEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0dhbGxlcnkvUHJvamVjdHMudnVlP2QzNzkiXSwibmFtZXMiOlsicmVzb3VyY2UiLCJyZXF1aXJlIiwiVnVlIiwiaHR0cCIsImludGVyY2VwdG9ycyIsInB1c2giLCJyZXF1ZXN0IiwibmV4dCIsImhlYWRlcnMiLCJzZXQiLCJMYXJhdmVsIiwiY3NyZlRva2VuIiwiY29tcG9uZW50IiwiYXBwIiwiZWwiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUNsREE7QUFBQTtBQUFBO0FBQUE7QUFDQSxJQUFNQSxXQUFXQyxtQkFBT0EsQ0FBQyxFQUFSLENBQWpCO0FBQ0FDLElBQUlDLElBQUosQ0FBU0MsWUFBVCxDQUFzQkMsSUFBdEIsQ0FBMkIsVUFBQ0MsT0FBRCxFQUFVQyxJQUFWLEVBQW1CO0FBQzFDRCxZQUFRRSxPQUFSLENBQWdCQyxHQUFoQixDQUFvQixjQUFwQixFQUFvQ0MsUUFBUUMsU0FBNUM7O0FBRUFKO0FBQ0gsQ0FKRDtBQUtBTCxJQUFJVSxTQUFKLENBQWMsWUFBZCxFQUE2QlgsbUJBQU9BLENBQUMsR0FBUixDQUE3Qjs7QUFFQSxJQUFJWSxNQUFNLElBQUlYLEdBQUosQ0FBUTtBQUNqQlksUUFBRztBQURjLENBQVIsQ0FBVjs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE07Ozs7Ozs7O0FDblFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1QsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSx1QkFBdUIscUJBQXFCO0FBQzVDO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBLHVCQUF1QixxQkFBcUI7QUFDNUM7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSwwQkFBMEIseUJBQXlCLFFBQVEsZUFBZTtBQUMxRTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBLG1CQUFtQixnQkFBZ0I7QUFDbkM7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSzs7QUFFTDtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSwrREFBK0Q7O0FBRS9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsMkNBQTJDOztBQUUzQztBQUNBO0FBQ0E7QUFDQSx1Q0FBdUMsS0FBSyxLQUFLLE1BQU07QUFDdkQ7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7O0FBRXJCOztBQUVBOztBQUVBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTs7QUFFQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBOztBQUVBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBLGFBQWE7QUFDYjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCwyQkFBMkI7QUFDM0I7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsMEJBQTBCO0FBQzFCOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0Esb0NBQW9DLEVBQUU7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBLHlCQUF5Qjs7QUFFekI7QUFDQSxzQkFBc0I7QUFDdEI7O0FBRUEseUJBQXlCOztBQUV6Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEtBQUs7O0FBRUw7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsaUJBQWlCO0FBQ2pCOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEI7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBOztBQUVBLCtEQUErRCxlQUFlO0FBQzlFOztBQUVBLHFDQUFxQyxvQkFBb0I7O0FBRXpEOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTs7QUFFQSxhQUFhO0FBQ2I7QUFDQTs7QUFFQTs7QUFFQSxTQUFTOztBQUVUO0FBQ0E7O0FBRUE7O0FBRUEscUNBQXFDO0FBQ3JDLGVBQWUsZ0JBQWdCLEtBQUs7O0FBRXBDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtDQUErQyxlQUFlO0FBQzlEOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHFCQUFxQixjQUFjO0FBQ25DOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsMkJBQTJCO0FBQzNCLHVEQUF1RDtBQUN2RDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjs7QUFFakI7QUFDQTtBQUNBLGlCQUFpQjs7QUFFakI7QUFDQTs7QUFFQSxxQ0FBcUMsb0JBQW9COztBQUV6RDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxpQkFBaUIsbUJBQU8sQ0FBQyxFQUFLOztBQUU5Qjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0I7O0FBRXhCO0FBQ0E7QUFDQSxTQUFTOztBQUVULHFCQUFxQiw2Q0FBNkM7O0FBRWxFO0FBQ0E7QUFDQTtBQUNBLGFBQWE7O0FBRWI7QUFDQTtBQUNBLGFBQWE7O0FBRWI7O0FBRUEsU0FBUyx1QkFBdUIsbUNBQW1DLEVBQUU7QUFDckUsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSwwRUFBMEUsbUJBQW1CLEVBQUU7O0FBRS9GO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCLHlCQUF5Qjs7QUFFekI7O0FBRUEscUJBQXFCO0FBQ3JCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7O0FBR0E7O0FBRUEsMENBQTBDLG1DQUFtQyxFQUFFO0FBQy9FOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EscUNBQXFDLG9EQUFvRCxFQUFFO0FBQzNGLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxLQUFLOztBQUVMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhDQUE4Qyx5QkFBeUIsRUFBRTtBQUN6RTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7O0FBRUEsQ0FBQzs7QUFFRDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHFEQUFxRCxHQUFHLG1CQUFtQjtBQUMzRTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsc0JBQXNCO0FBQ3RCLHlCQUF5QixrQ0FBa0M7O0FBRTNEOztBQUVBLHlCQUF5Qjs7QUFFekIsNkJBQTZCOztBQUU3Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEtBQUs7O0FBRUw7O0FBRUE7O0FBRUEsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxvQkFBb0I7QUFDcEI7O0FBRUE7O0FBRUE7QUFDQSwyQ0FBMkMsR0FBRyw0QkFBNEI7QUFDMUU7O0FBRUEsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBLDJDQUEyQyxHQUFHLHdDQUF3QztBQUN0Rjs7QUFFQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSx5QkFBeUI7O0FBRXpCLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsd0JBQXdCLDJCQUEyQixVQUFVOztBQUU3RDtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7O0FBRUEsOEJBQThCLHNCQUFzQjs7QUFFcEQ7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLGlDQUFpQzs7QUFFakM7QUFDQTs7QUFFQTs7QUFFQSxVQUFVLGNBQWM7QUFDeEIsV0FBVyxlQUFlO0FBQzFCLFlBQVksY0FBYztBQUMxQixhQUFhLGNBQWM7QUFDM0IsYUFBYSxpQkFBaUI7QUFDOUIsYUFBYTs7QUFFYjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTs7QUFFQSw0Q0FBNEMsMENBQTBDO0FBQ3RGO0FBQ0E7O0FBRUEsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFZSxxRUFBTSxFQUFDO0FBQ1M7Ozs7Ozs7O0FDbmhEL0IsZTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNpQkE7QUFDQTtBQURBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDd0JBO0FBQ0E7QUFEQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzhEQTtBQUNBOztBQUVBO0FBQ0EseUpBREE7QUFFQSxNQUZBLGtCQUVBO0FBQ0E7QUFDQSxtQkFDQSwrQ0FEQSxFQUVBLGlDQUZBO0FBSUE7QUFDQTtBQUNBLEtBRkE7QUFHQTtBQUNBLGFBREE7QUFFQTtBQUNBLGNBREE7QUFFQTtBQUZBLE9BRkE7QUFNQSxrQ0FOQTtBQU9BLG9CQVBBO0FBUUEsa0JBUkE7QUFTQSxtQkFUQTtBQVVBLGtCQVZBO0FBV0Esc0JBWEE7QUFZQSxzQkFaQTtBQWFBLDRCQWJBO0FBY0EsaUNBZEE7QUFlQSxvQkFmQTtBQWdCQTtBQUNBLGVBREE7QUFFQSxrQkFGQTtBQUdBLGtCQUhBO0FBSUEsaUJBSkE7QUFLQTtBQUxBLE9BaEJBO0FBdUJBO0FBQ0Esb0JBREE7QUFFQSx1QkFGQTtBQUdBLGlCQUhBO0FBSUEsdUJBSkE7QUFLQSx1QkFMQTtBQU1BLHVCQU5BO0FBT0EsZ0JBUEE7QUFRQTtBQVJBO0FBdkJBO0FBbUNBLEdBOUNBOztBQStDQTtBQUVBLG9CQUZBLDRCQUVBLElBRkEsRUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQVhBO0FBWUEsVUFaQSxrQkFZQSxHQVpBLEVBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FsQkE7QUFtQkEsWUFuQkEsb0JBbUJBLEtBbkJBLEVBbUJBLEdBbkJBLEVBbUJBLEtBbkJBLEVBbUJBO0FBQ0E7QUFBQTtBQUFBO0FBQ0EsS0FyQkE7QUFzQkEsYUF0QkEsdUJBc0JBO0FBQUE7O0FBQUE7O0FBQ0E7QUFDQSxpREFDQSxJQURBLENBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BUkEsRUFTQSxLQVRBLENBU0E7QUFDQTtBQUNBLE9BWEE7QUFZQSxLQXBDQTtBQXFDQSxhQXJDQSxxQkFxQ0EsQ0FyQ0EsRUFxQ0EsRUFyQ0EsRUFxQ0E7QUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBSEE7QUFJQTtBQUNBO0FBQ0E7QUFDQSxLQWxEQTtBQW1EQSxZQW5EQSxzQkFtREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FEQTtBQUVBO0FBRkE7QUFJQSxLQTVEQTtBQTZEQSxhQTdEQSxxQkE2REEsQ0E3REEsRUE2REEsRUE3REEsRUE2REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBUEEsRUFPQSxJQVBBLENBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQURBO0FBRUE7QUFGQTtBQUlBO0FBQ0EsU0FoQkE7QUFrQkEsT0FuQkEsTUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQVBBLEVBT0EsSUFQQSxDQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFEQTtBQUVBO0FBRkE7QUFJQTtBQUNBLFNBaEJBO0FBaUJBO0FBQ0E7QUFDQTtBQUVBLEtBM0dBO0FBNEdBLGVBNUdBLHVCQTRHQSxFQTVHQSxFQTRHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBSkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQXZIQSxHQS9DQTtBQXlLQSxTQXpLQSxxQkF5S0E7QUFDQTtBQUNBLEdBM0tBO0FBNEtBLFNBNUtBLHFCQTRLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUVBO0FBdExBLEc7Ozs7Ozs7QUMxR0EsZUFBZSxLQUFpRCxvSkFBb0osaUJBQWlCLG1CQUFtQixjQUFjLDRCQUE0QixZQUFZLFVBQVUsaUJBQWlCLGdFQUFnRSxTQUFTLCtCQUErQixrQkFBa0IsYUFBYSxrQ0FBa0MscURBQXFELDJFQUEyRSxZQUFZLDRDQUE0QyxxQ0FBcUMsV0FBVyx5QkFBeUIsVUFBVSxFQUFFLGVBQWUscUJBQXFCLFNBQVMsNkJBQTZCLGlCQUFpQixjQUFjLEtBQUssY0FBYyw2QkFBNkIsU0FBUyxnQkFBZ0Isa0JBQWtCLG1CQUFtQixzQ0FBc0MsWUFBWSxLQUFLLGNBQWMsS0FBSyxpQkFBaUIsOEJBQThCLFFBQVEsV0FBVyxLQUFLLFdBQVcsZ0dBQWdHLElBQUksaUJBQWlCLGFBQWEsc0NBQXNDLFNBQVMsYUFBYSxPQUFPLElBQUksd0JBQXdCLE1BQU0sd0JBQXdCLFlBQVksaUNBQWlDLFVBQVUsNkJBQTZCLFlBQVksMkJBQTJCLG9CQUFvQixZQUFZLGtCQUFrQix3QkFBd0IsaUJBQWlCLHdCQUF3QixpQkFBaUIsd0JBQXdCLGtCQUFrQixzQkFBc0IsbUJBQW1CLHNCQUFzQixtQkFBbUIsd0JBQXdCLGlCQUFpQix3QkFBd0IsVUFBVSxZQUFZLFdBQVcsK0JBQStCLFVBQVUsa0JBQWtCLGtDQUFrQywwSkFBMEosMnVCQUEydUIsMkJBQTJCLHdCQUF3QixrQkFBa0IsK0JBQStCLFVBQVUsY0FBYyx5QkFBeUIsZUFBZSx5QkFBeUIsaUJBQWlCLHlCQUF5QixnQkFBZ0IsdUJBQXVCLGVBQWUsOEJBQThCLGlCQUFpQix3QkFBd0IsaUJBQWlCLHdCQUF3QixrQkFBa0Isc0JBQXNCLFVBQVUsd0JBQXdCLFNBQVMsMkJBQTJCLGtCQUFrQix3QkFBd0IsVUFBVSx5QkFBeUIsdUJBQXVCLDRCQUE0QixVQUFVLG9DQUFvQyx1WkFBdVoseUJBQXlCLDJCQUEyQiwyQkFBMkIsaUNBQWlDLHlCQUF5QixvQkFBb0IsbUVBQW1FLDhCQUE4Qiw4Q0FBOEMsOEJBQThCLEVBQUUsd0JBQXdCLDRCQUE0Qiw2QkFBNkIsd0NBQXdDLDZCQUE2Qix3Q0FBd0MsOEJBQThCLHlDQUF5QywyQkFBMkIsc0NBQXNDLHVCQUF1Qix3RUFBd0UsV0FBVyw0QkFBNEIsT0FBTyxzWEFBc1gsVUFBVSxzQkFBc0IsYUFBYSxrSUFBa0ksVUFBVSxzR0FBc0csWUFBWSxTQUFTLGlEQUFpRCxpSkFBaUosU0FBUyxzQkFBc0IsNkdBQTZHLHFCQUFxQiwrRkFBK0Ysc0JBQXNCLGlHQUFpRyxvQkFBb0Isb0JBQW9CLFdBQVcsb0dBQW9HLHVDQUF1Qyx1QkFBdUIsbzhFQUFvOEUsRUFBRSxXQUFXLDJDQUEyQyxtQ0FBbUMsMkNBQTJDLHdDQUF3QyxRQUFRLDRCQUE0QixNQUFNLGtGQUFrRixrQ0FBa0MsNENBQTRDLG1DQUFtQyw2Q0FBNkMsb0NBQW9DLDJDQUEyQyxpQ0FBaUMsbURBQW1ELDBDQUEwQywyQ0FBMkMsaUNBQWlDLDZDQUE2QyxtQ0FBbUMscURBQXFELDRDQUE0QyxtREFBbUQsMENBQTBDLHlEQUF5RCxpREFBaUQsZ0NBQWdDLDBCQUEwQiwwQkFBMEIsaUJBQWlCLG9FQUFvRSxHQUFHLFVBQVUsbUNBQW1DLDJCQUEyQixRQUFRLFVBQVUsZ0NBQWdDLHdCQUF3QixHQUFHLFVBQVUsb0NBQW9DLDZCQUE2QiwyQkFBMkIsR0FBRyxVQUFVLG1DQUFtQywyQkFBMkIsUUFBUSxVQUFVLGdDQUFnQyx3QkFBd0IsR0FBRyxVQUFVLG9DQUFvQyw2QkFBNkIsNEJBQTRCLEdBQUcsVUFBVSxtQ0FBbUMsMkJBQTJCLElBQUksVUFBVSxnQ0FBZ0MseUJBQXlCLG9CQUFvQixHQUFHLFVBQVUsbUNBQW1DLDJCQUEyQixJQUFJLFVBQVUsZ0NBQWdDLHlCQUF5Qix5QkFBeUIsR0FBRywyQkFBMkIsbUJBQW1CLElBQUksNkJBQTZCLHFCQUFxQixJQUFJLDJCQUEyQixvQkFBb0IsaUJBQWlCLEdBQUcsMkJBQTJCLG1CQUFtQixJQUFJLDZCQUE2QixxQkFBcUIsSUFBSSwyQkFBMkIsb0JBQW9CLHNCQUFzQixzQkFBc0IsVUFBVSxpQkFBaUIsZ0NBQWdDLGdCQUFnQixhQUFhLHVCQUF1QixlQUFlLHlCQUF5QixlQUFlLHdFQUF3RSxlQUFlLGlDQUFpQyxhQUFhLHdCQUF3QixtQkFBbUIsb0NBQW9DLFdBQVcsc0JBQXNCLGtCQUFrQixhQUFhLHNCQUFzQixrQkFBa0IscUJBQXFCLG1CQUFtQixZQUFZLGlCQUFpQiw0QkFBNEIsYUFBYSxnREFBZ0QsbUJBQW1CLGdCQUFnQiw2Q0FBNkMsa0RBQWtELFVBQVUsdUNBQXVDLGdCQUFnQixtREFBbUQsOEJBQThCLGlDQUFpQyxlQUFlLGtCQUFrQixjQUFjLGVBQWUsWUFBWSx1Q0FBdUMsMEJBQTBCLHdDQUF3QyxVQUFVLGtDQUFrQyxXQUFXLGtCQUFrQixNQUFNLE9BQU8sVUFBVSxlQUFlLGVBQWUsZUFBZSxnQkFBZ0Isa0JBQWtCLHFCQUFxQixpQkFBaUIsMkNBQTJDLGtCQUFrQixlQUFlLCtDQUErQyxtQkFBbUIsMERBQTBELG1DQUFtQyxvQ0FBb0MsMkRBQTJELGdCQUFnQix1QkFBdUIsZ0VBQWdFLDZCQUE2QixvR0FBb0csb0NBQW9DLGVBQWUsa0JBQWtCLDBDQUEwQyw4QkFBOEIsc0JBQXNCLHlCQUF5QixpQkFBaUIsZ0NBQWdDLG1CQUFtQixnQkFBZ0IsWUFBWSxhQUFhLGtCQUFrQixjQUFjLFdBQVcsb0NBQW9DLGNBQWMsa0RBQWtELGdFQUFnRSx3REFBd0QsOENBQThDLFVBQVUseURBQXlELGlEQUFpRCw0RUFBNEUsb0JBQW9CLFVBQVUsWUFBWSxrQkFBa0IsY0FBYyxRQUFRLFNBQVMsa0JBQWtCLGlCQUFpQixvRkFBb0YsY0FBYyxXQUFXLFlBQVksaURBQWlELFVBQVUsMEJBQTBCLCtDQUErQyxVQUFVLCtCQUErQix1REFBdUQseUNBQXlDLGlDQUFpQyxtQ0FBbUMsVUFBVSxhQUFhLG9CQUFvQixrQkFBa0IsWUFBWSxTQUFTLFFBQVEsZ0JBQWdCLFdBQVcsa0JBQWtCLDhCQUE4QiwyQkFBMkIsa0JBQWtCLGdCQUFnQiw4Q0FBOEMsZ0JBQWdCLDZDQUE2QyxrQkFBa0IsTUFBTSxPQUFPLFNBQVMsUUFBUSxpQ0FBaUMsaURBQWlELGNBQWMsdURBQXVELFVBQVUsb0JBQW9CLHdDQUF3QyxvQkFBb0IsYUFBYSxrQkFBa0IsY0FBYyxhQUFhLFVBQVUsNEJBQTRCLGtCQUFrQixlQUFlLFVBQVUsV0FBVyxZQUFZLG1CQUFtQixtREFBbUQsbUJBQW1CLFdBQVcsOENBQThDLFdBQVcsa0JBQWtCLFNBQVMsVUFBVSxRQUFRLFNBQVMsa0NBQWtDLG1DQUFtQyxnQ0FBZ0MsT0FBTyxpQkFBaUIsNERBQTRELHlCQUF5Qiw2QkFBNkIsb0JBQW9CLFdBQVcsdUNBQXVDLG9CQUFvQix5QkFBeUIsZ0JBQWdCLFdBQVcsb0NBQW9DLGdCQUFnQiw4Q0FBOEMsdUJBQXVCLGVBQWUsb0JBQW9CLHNDQUFzQyxTQUFTLE1BQU0sV0FBVyxxQ0FBcUMsOEJBQThCLGdCQUFnQiw0R0FBNEcsNkJBQTZCLG9FQUFvRSxZQUFZLDhEQUE4RCw2QkFBNkIsWUFBWSxrREFBa0QsZ0JBQWdCLHFDQUFxQyxrQkFBa0IsV0FBVyxXQUFXLGlCQUFpQixhQUFhLFlBQVksWUFBWSxzQkFBc0IscUJBQXFCLHlCQUF5QixnQkFBZ0IsZ0JBQWdCLHFCQUFxQixVQUFVLDJDQUEyQyxVQUFVLG9GQUFvRiwyQkFBMkIsMEJBQTBCLHFCQUFxQixrQkFBa0IsT0FBTyx3RkFBd0YscUJBQXFCLHlCQUF5Qiw0Q0FBNEMsUUFBUSxTQUFTLE9BQU8saUJBQWlCLGFBQWEsWUFBWSxpREFBaUQsYUFBYSxtQkFBbUIsd0NBQXdDLDJFQUEyRSxLQUFLLGdCQUFnQixjQUFjLGNBQWMsY0FBYyxnRkFBZ0YsMENBQTBDLDZFQUE2RSw2QkFBNkIsZ0JBQWdCLGtHQUFrRyx5Q0FBeUMsSUFBSSwyQkFBMkIsWUFBWSx5S0FBeUssY0FBYyxtRUFBbUUsTUFBTSx1Q0FBdUMsOERBQThELHFCQUFxQixJQUFJLHVCQUF1QixjQUFjLE1BQU0sWUFBWSxHQUFHLGlCQUFpQixnQkFBZ0IsVUFBVSx5V0FBeVcsdUVBQXVFLHVHQUF1RyxrQkFBa0IsbUNBQW1DLDhGQUE4Rix3SUFBd0ksb0xBQW9MLDBNQUEwTSxtQkFBbUIsa0RBQWtELDJmQUEyZixRQUFRLDBkQUEwZCxpWEFBaVgscWdCQUFxZ0IsVUFBVSxzQkFBc0IsYUFBYSw0R0FBNEcsWUFBWSwrUEFBK1Asc0NBQXNDLGlCQUFpQixTQUFTLHNCQUFzQixXQUFXLHFCQUFxQixnQkFBZ0Isd0lBQXdJLElBQUksb0ZBQW9GLHdVQUF3VSwwQkFBMEIsVUFBVSxNQUFNLGtEQUFrRCxzU0FBc1MsS0FBSyxpRUFBaUUsZ0JBQWdCLGtHQUFrRyw2QkFBNkIsNkxBQTZMLCtuR0FBK25HLHNEQUFzRCxpQ0FBaUMsc0RBQXNELHVCQUF1QixtREFBbUQsc0JBQXNCLG1EQUFtRCx1QkFBdUIsc0RBQXNELDBCQUEwQixtREFBbUQsdUJBQXVCLDhCQUE4QiwyR0FBMkcsdU9BQXVPLElBQUksZ0NBQWdDLHlFQUF5RSxJQUFJLDZDQUE2QyxpSEFBaUgsa0hBQWtILG1CQUFtQixpSUFBaUksdUJBQXVCLGlHQUFpRyx1QkFBdUIsbUJBQW1CLG1GQUFtRixJQUFJLGlEQUFpRCxVQUFVLHlCQUF5QixNQUFNLG1JQUFtSSx5QkFBeUIsWUFBWSxxQkFBcUIsbUlBQW1JLElBQUksZ0NBQWdDLDhCQUE4QixrQkFBa0IsMkRBQTJELFdBQVcscUJBQXFCLGNBQWMscUJBQXFCLDBLQUEwSyxJQUFJLG1DQUFtQyxVQUFVLHdDQUF3QyxvSkFBb0oscURBQXFELGNBQWMscUJBQXFCLHdGQUF3RixJQUFJLGlGQUFpRixVQUFVLHVFQUF1RSx3RUFBd0Usd0NBQXdDLCtDQUErQyx5Q0FBeUMsZ0pBQWdKLHNGQUFzRixjQUFjLGtCQUFrQiwrRUFBK0UsSUFBSSxLQUFLLE9BQU8seUJBQXlCLFNBQVMseUNBQXlDLGNBQWMscUNBQXFDLElBQUksaUNBQWlDLFNBQVMseUNBQXlDLGNBQWMscUNBQXFDLElBQUksaUNBQWlDLFNBQVMsNENBQTRDLGNBQWMscUNBQXFDLElBQUksbUNBQW1DLFNBQVMsdUNBQXVDLHlDQUF5QywwQ0FBMEMsNENBQTRDLHNDQUFzQyx3Q0FBd0MsdUNBQXVDLGNBQWMscUNBQXFDLElBQUksa0VBQWtFLFNBQVMsNkJBQTZCLGtCQUFrQiw0V0FBNFcsa0JBQWtCO0FBQ3p6OUIsMldBQTJXLFlBQVkseURBQXlELElBQUksd0JBQXdCLGtDQUFrQyxHQUFHLHdGQUF3RixJQUFJLHNDQUFzQyw0Q0FBNEMsa0JBQWtCLHNDQUFzQywwQ0FBMEMsa0JBQWtCLHNDQUFzQyx1Q0FBdUMsbUJBQW1CLDZCQUE2Qix1Q0FBdUMsbUJBQW1CLDhIQUE4SCwrQkFBK0IsS0FBSyxzQkFBc0IsZ0ZBQWdGLGtCQUFrQiw2QkFBNkIsc0JBQXNCLG1CQUFtQiw4QkFBOEIsNkJBQTZCLG1CQUFtQixtQ0FBbUMsNEJBQTRCLG1CQUFtQixNQUFNLElBQUksK0JBQStCLFVBQVUscUdBQXFHLDZCQUE2QixtQkFBbUIsOEJBQThCLHdCQUF3QixtQkFBbUIsdUJBQXVCLDJCQUEyQixtQkFBbUIsNEJBQTRCLFFBQVEsNkNBQTZDLG1CQUFtQix5QkFBeUIsa0JBQWtCLGtCQUFrQiw4SUFBOEksR0FBRyxtREFBbUQsZ0NBQWdDLE1BQU0sK1FBQStRLGtEQUFrRCxvQkFBb0IsNkNBQTZDLDJDQUEyQyxJQUFJLG1EQUFtRCxVQUFVLFdBQVcsOENBQThDLHVDQUF1QywrSUFBK0kscUNBQXFDLG1GQUFtRix3Q0FBd0MsWUFBWSxtb0JBQW1vQiw0Q0FBNEMsZ0JBQWdCLGtCQUFrQixVQUFVLG1CQUFtQixJQUFJLDhEQUE4RCxpQ0FBaUMsSUFBSSxpRUFBaUUsNENBQTRDLGtCQUFrQix5Q0FBeUMsSUFBSSw2QkFBNkIsUUFBUSxnQkFBZ0IsNkRBQTZELFNBQVMsSUFBSSxTQUFTLDZDQUE2QyxrQkFBa0IseUNBQXlDLElBQUksNkJBQTZCLFFBQVEsZ0JBQWdCLGdFQUFnRSxTQUFTLElBQUksU0FBUyxnQ0FBZ0MsY0FBYywrQ0FBK0MsMENBQTBDLCtEQUErRCxJQUFJLHdDQUF3QyxTQUFTLCtCQUErQixrREFBa0QsdUNBQXVDLDZCQUE2QixrQ0FBa0Msb0JBQW9CLGtCQUFrQixpREFBaUQsSUFBSSxtRUFBbUUsZ0RBQWdELE1BQU0sc0JBQXNCLGtFQUFrRSxvREFBb0QscVNBQXFTLDhCQUE4QixRQUFRLGtOQUFrTiwrQkFBK0IsUUFBUSw0SkFBNEoscUNBQXFDLFlBQVksd0JBQXdCLElBQUksbUNBQW1DLFNBQVMsNENBQTRDLGNBQWMsd0JBQXdCLElBQUksNFJBQTRSLFNBQVMsa0RBQWtELFVBQVUsd0NBQXdDLHdHQUF3RyxnQkFBZ0Isa0JBQWtCLGlDQUFpQyxVQUFVLGVBQWUsbUJBQW1CLElBQUksdUNBQXVDLHlHQUF5RywwREFBMEQsSUFBSSxZQUFZLEtBQUssVUFBVSxrQ0FBa0MsMEZBQTBGLFVBQVUsZ0RBQWdELGFBQWEsNE1BQTRNLFVBQVUsa0lBQWtJLGlDQUFpQyxpQkFBaUIsaUVBQWlFLGtIQUFrSCxtQkFBbUIsMElBQTBJLFFBQVEsc0NBQXNDLFVBQVUsbUJBQW1CLElBQUksK0JBQStCLFlBQVkscUNBQXFDLDJJQUEySSxpRkFBaUYsa0JBQWtCLHlCQUF5QixVQUFVLDhHQUE4RywrS0FBK0ssa0JBQWtCLG1DQUFtQyxVQUFVLCtDQUErQyxNQUFNLDhQQUE4UCxtQkFBbUIscUZBQXFGLFFBQVEsb0NBQW9DLHNKQUFzSix3Q0FBd0MsWUFBWSx3REFBd0QsSUFBSSwyREFBMkQsWUFBWSw2Q0FBNkMsbURBQW1ELHFCQUFxQixRQUFRLG9NQUFvTSxRQUFRLG1EQUFtRCxNQUFNLDZDQUE2QyxrQkFBa0IscUhBQXFILDBCQUEwQiw0REFBNEQsTUFBTSxpRkFBaUYsa0JBQWtCLE1BQU0scUJBQXFCLFlBQVksMERBQTBELGlDQUFpQywwQ0FBMEMsRUFBRSxnQkFBZ0Isb0JBQW9CLDhOQUE4Tiw0Q0FBNEMsTUFBTSx1REFBdUQsTUFBTSw2Q0FBNkMsTUFBTSwwQ0FBMEMsTUFBTSxxREFBcUQsTUFBTSx5RUFBeUUsTUFBTSxxREFBcUQsNkxBQTZMLEdBQUcsOENBQThDLHFDQUFxQyxZQUFZLHVIQUF1SCx3RUFBd0UsS0FBSyxJQUFJLEVBQUUsb0JBQW9CLGtDQUFrQyxxQ0FBcUMsOEJBQThCLHNDQUFzQyxVQUFVLG1CQUFtQixJQUFJLDBFQUEwRSwwRkFBMEYsMENBQTBDLFFBQVEsb0JBQW9CLFlBQVkscUNBQXFDLElBQUksZ0NBQWdDLFNBQVMsWUFBWSxzQ0FBc0Msa0JBQWtCLDJCQUEyQixrREFBa0QsSUFBSSwrQkFBK0IsaUNBQWlDLElBQUksbUNBQW1DLDZEQUE2RCx1SkFBdUosNERBQTRELGNBQWMsUUFBUSwwR0FBMEcsb0NBQW9DLDZCQUE2QixxQ0FBcUMsOEVBQThFLHdDQUF3QyxJQUFJLG1CQUFtQiw0S0FBNEssa0JBQWtCLFVBQVUsd0JBQXdCLElBQUksaUZBQWlGLFlBQVksZ0JBQWdCLFVBQVUscUJBQXFCLG1CQUFtQixzQkFBc0IscURBQXFELElBQUksa0ZBQWtGLEtBQUssOEJBQThCLElBQUksc0lBQXNJLFlBQVksd0JBQXdCLElBQUksbUVBQW1FLFVBQVUsNEJBQTRCLG1CQUFtQixNQUFNLCtDQUErQyx5TEFBeUwsZ0JBQWdCLFNBQVMsMkNBQTJDLDZEQUE2RCw2QkFBNkIsa0JBQWtCLHdDQUF3QyxrREFBa0QseUZBQXlGLGlEQUFpRCw2Q0FBNkMsdUNBQXVDLHNCQUFzQixnQ0FBZ0MsbUJBQW1CLElBQUksc0NBQXNDLHVMQUF1TCxJQUFJLGdKQUFnSixJQUFJLDJDQUEyQyx3RkFBd0YsZ0NBQWdDLGVBQWUsNkJBQTZCLHVCQUF1QixtQkFBbUIsa0VBQWtFLDZHQUE2RyxTQUFTLDJDQUEyQyxpQkFBaUIsdUNBQXVDLFVBQVUsbUJBQW1CLElBQUksaUZBQWlGLDhKQUE4Siw4Q0FBOEMsVUFBVSxtQkFBbUIsSUFBSSw2RUFBNkUsNEpBQTRKLEdBQUcsa0NBQWtDLGlDQUFpQyxzRUFBc0UseUNBQXlDLHlUQUF5VCxrQkFBa0IseUNBQXlDLGdCQUFnQiwyRkFBMkYsWUFBWSx3QkFBd0IsSUFBSSxtRkFBbUYsU0FBUyx1R0FBdUcsSUFBSSx1RUFBdUUsU0FBUywwRkFBMEYsY0FBYyxzTUFBc00sSUFBSSwrQ0FBK0MsVUFBVSxVQUFVLFNBQVMsNkJBQTZCLGtCQUFrQixvRUFBb0UsdUVBQXVFLGVBQWUsb0NBQW9DLHFCQUFxQixPQUFPLEVBQUUsaUJBQWlCLFlBQVksd0JBQXdCLElBQUksNEJBQTRCLFNBQVMsZUFBZSwwQ0FBMEMsaUNBQWlDLEVBQUUsNkJBQTZCLE1BQU0scUVBQXFFLCtCQUErQixrQkFBa0IsS0FBSyxlQUFlLG1CQUFtQixTQUFTLDRCQUE0QixNQUFNLDZMQUE2TCxTQUFTLDZCQUE2QixvQkFBb0IsdUJBQXVCLEtBQUssSUFBSSxtQkFBbUIsSUFBSSx3Q0FBd0MsU0FBUyxZQUFZLGtGQUFrRixJQUFJLHFCQUFxQiwrQkFBK0Isa0pBQWtKLFNBQVMsMkJBQTJCLGdEQUFnRCw2QkFBNkIsY0FBYyxlQUFlLG1FQUFtRSxJQUFJLDRDQUE0QyxzRkFBc0YseUJBQXlCLHNDQUFzQyx1QkFBdUIsU0FBUyw0RUFBNEUsNEJBQTRCLHFCQUFxQixFQUFFLCtPQUErTyx3QkFBd0Isa0xBQWtMLElBQUksdUNBQXVDLHVCQUF1QixpQ0FBaUMsTUFBTSwrQ0FBK0MsY0FBYyxjQUFjLDRHQUE0RyxzQkFBc0IscUVBQXFFLHVQQUF1UCxTQUFTLHlCQUF5QixVQUFVLGdDQUFnQyxzREFBc0QscUZBQXFGLDZCQUE2QixrQ0FBa0MsVUFBVSx5RUFBeUUsNEJBQTRCLFFBQVEsaUJBQWlCLFdBQVcsRUFBRSx5Q0FBeUMsSUFBSSxTQUFTLDRCQUE0QixnQkFBZ0Isb0NBQW9DLCtHQUErRyw4QkFBOEIsY0FBYyxjQUFjLEVBQUUsaUNBQWlDLDRHQUE0RyxTQUFTLHdCQUF3QiwwQkFBMEIsdVRBQXVULDRSQUE0UixTQUFTLEdBQUcsbUJBQW1CLHNCQUFzQix3TUFBd00sOElBQThJLGNBQWMsTUFBTSxJQUFJLG1CQUFtQixTQUFTLGlDQUFpQyxpQkFBaUIsNEJBQTRCLG9DQUFvQyxJQUFJLGtCQUFrQixVQUFVLE9BQU8sNEZBQTRGLG9DQUFvQyxzQ0FBc0MsbUNBQW1DLGFBQWEsa0JBQWtCLGVBQWUsNEZBQTRGLGlCQUFpQixnQkFBZ0IsWUFBWSxXQUFXLEtBQUsscUJBQXFCLE1BQU0sU0FBUyxZQUFZLGlCQUFpQiwyQkFBMkIsS0FBSyxpQkFBaUIsa0NBQWtDLEtBQUssaUJBQWlCLGlCQUFpQiw0QkFBNEIsU0FBUywwQkFBMEIsY0FBYyxpQkFBaUIsS0FBSyxXQUFXLEtBQUssMENBQTBDLDJCQUEyQixxQ0FBcUMsZUFBZSxFQUFFLFNBQVMsZ0JBQWdCLDBCQUEwQixnSUFBZ0ksS0FBSywrR0FBK0csa0JBQWtCLGNBQWMsNEJBQTRCLG1CQUFtQixvQkFBb0IsY0FBYyxzQ0FBc0Msa0NBQWtDLGdCQUFnQixVQUFVLGdCQUFnQixVQUFVLDBEQUEwRCwwQ0FBMEMsTUFBTSx3QkFBd0IsTUFBTSxzRUFBc0UsT0FBTyxVQUFVLG9CQUFvQixpQkFBaUIsNENBQTRDLEtBQUssZ0RBQWdELDRFQUE0RSxnQkFBZ0Isb0NBQW9DLDhIQUE4SCwwR0FBMEcsS0FBSyxLQUFLLGFBQWEsNkJBQTZCLDJDQUEyQyxRQUFRLGVBQWUsTUFBTSxrQkFBa0IsNERBQTRELGdCQUFnQixvRUFBb0UsaUJBQWlCLCtEQUErRCxrQkFBa0Isd0JBQXdCLE9BQU8sMEdBQTBHLFdBQVcsMEJBQTBCLGlCQUFpQixXQUFXLEtBQUsscUJBQXFCLG1CQUFtQixNQUFNLFdBQVcsT0FBTyxZQUFZLFdBQVcsS0FBSyxXQUFXLGVBQWUsWUFBWSxpQkFBaUIsaUJBQWlCLG1CQUFtQixpQkFBaUIsU0FBUyxxQkFBcUIsNENBQTRDLEdBQUcsaUJBQWlCLFdBQVcsc0NBQXNDLFNBQVMsRUFBRSwrQkFBK0IsZUFBZSxzQkFBc0IsbURBQW1ELGtEQUFrRCxHQUFHLEU7Ozs7Ozs7QUNENXp5QixnQkFBZ0IsbUJBQU8sQ0FBQyxDQUF3RTtBQUNoRztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUEwTztBQUNwUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUF5TTtBQUNuTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBd0U7QUFDaEc7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBMk87QUFDclA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBME07QUFDcE47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBTyxDQUFDLENBQXdFO0FBQ2hHO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXlPO0FBQ25QO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQXdNO0FBQ2xOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ2pFQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsMENBQTBDLG9CQUFvQjtBQUM5RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN0QkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3Zpc2EvZ2FsbGVyeS9hbGJ1bS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNTAzKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmM2NjYTYxYmUyNWJmODM3OTk4ZiIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDE4IDE5IDIwIDIxIDIyIDIzIDI0IDI1IDI2IDI3IiwiaW1wb3J0IERyb3B6b25lIGZyb20gJ3Z1ZTItZHJvcHpvbmUnO1xyXG5jb25zdCByZXNvdXJjZSA9IHJlcXVpcmUoJ3Z1ZS1yZXNvdXJjZScpO1xyXG5WdWUuaHR0cC5pbnRlcmNlcHRvcnMucHVzaCgocmVxdWVzdCwgbmV4dCkgPT4ge1xyXG4gICAgcmVxdWVzdC5oZWFkZXJzLnNldCgnWC1DU1JGLVRPS0VOJywgTGFyYXZlbC5jc3JmVG9rZW4pO1xyXG5cclxuICAgIG5leHQoKTtcclxufSk7XHJcblZ1ZS5jb21wb25lbnQoJ2FsYnVtLWxpc3QnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9HYWxsZXJ5L1Byb2plY3RzLnZ1ZScpKTtcclxuXHJcbnZhciBhcHAgPSBuZXcgVnVlKHtcclxuXHRlbDonI2FsYnVtcycsXHJcbn0pO1xyXG5cclxuLy8gbGV0IGRhdGEgPSB3aW5kb3cuTGFyYXZlbC51c2VyO1xyXG4vLyB2YXIgdXJsICA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xyXG4vLyB2YXIgdGlkID0gdXJsLnN1YnN0cmluZyh1cmwubGFzdEluZGV4T2YoJy8nKSArIDEpO1xyXG4vL1xyXG5cclxuLy8gdmFyIGFwcCA9IG5ldyBWdWUoe1xyXG4vLyBcdGVsOicjYWxidW1zJyxcclxuLy8gXHRkYXRhOntcclxuLy8gXHRcdGR6OiAnJyxcclxuLy8gICAgICAgICBoZWFkZXJzOiB7XHJcbi8vICAgICAgICAgICAgICdYLUNTUkYtVE9LRU4nOiB3aW5kb3cuTGFyYXZlbC5jc3JmVG9rZW4sXHJcbi8vICAgICAgICAgfSxcclxuLy8gICAgICAgICB0eXBlaWQ6IHRpZCxcclxuLy8gXHRcdGRvY3VtZW50czpbXSxcclxuLy8gXHRcdGFjdDonJyxcclxuLy8gXHRcdHNlbF9pZDonJyxcclxuLy9cclxuLy8gXHRcdGZpbGVzOiBbXSxcclxuLy9cclxuLy8gICAgICAgICBzZXR0aW5nOiBkYXRhLmFjY2Vzc19jb250cm9sWzBdLnNldHRpbmcsXHJcbi8vICAgICAgICAgcGVybWlzc2lvbnM6IGRhdGEucGVybWlzc2lvbnMsXHJcbi8vICAgICAgICAgc2VscGVybWlzc2lvbnM6IFt7XHJcbi8vICAgICAgICAgICAgIGFkZE5ld0RvY3VtZW50OjAsXHJcbi8vICAgICAgICAgICAgIGVkaXREb2N1bWVudDowLFxyXG4vLyAgICAgICAgICAgICBkZWxldGVEb2N1bWVudDowXHJcbi8vICAgICAgICAgfV0sXHJcbi8vXHJcbi8vIFx0XHRkb2NzRnJtOiBuZXcgRm9ybSh7XHJcbi8vIFx0ICAgICAgIFx0aWQgXHRcdDogJycsXHJcbi8vIFx0XHQgICAgbmFtZSBcdDogJycsXHJcbi8vIFx0ICAgIH0seyBiYXNlVVJMXHQ6ICcvdmlzYS9nYWxsZXJ5J30pXHJcbi8vIFx0fSxcclxuLy9cclxuLy8gXHRtb3VudGVkICgpIHtcclxuLy8gXHRcdCQoJyNkaWFsb2dBZGREb2N1bWVudCcpLm9uKCdzaG93LmJzLm1vZGFsJywgZnVuY3Rpb24gKGUpIHtcclxuLy8gXHRcdFx0YXBwLmRvY3NGcm0uZXJyb3JzLmNsZWFyKCk7XHJcbi8vIFx0XHR9KTtcclxuLy9cclxuLy8gXHRcdGlmKHRoaXMudHlwZWlkICE9ICdsaXN0Jykge1xyXG4vLyBcdFx0XHR0aGlzLmdldEZpbGVzKCk7XHJcbi8vXHJcbi8vIFx0XHRcdHZhciB2bSA9IHRoaXM7XHJcbi8vXHJcbi8vIFx0ICAgICAgICB0aGlzLmR6ID0gJ2Ryb3B6b25lJztcclxuLy9cclxuLy8gXHQgICAgICAgIHRoaXMuJG5leHRUaWNrKCgpID0+IHtcclxuLy8gXHQgICAgICAgICAgICB0aGlzLiRyZWZzLnJldHVybmVkSXRlbUZpbGVzLiRvbigndmRyb3B6b25lLXJlbW92ZWRGaWxlJywgdGhpcy5kcm9wem9uZUZpbGVSZW1vdmVkLmJpbmQodGhpcykpO1xyXG4vLyBcdCAgICAgICAgICAgIHRoaXMuJHJlZnMucmV0dXJuZWRJdGVtRmlsZXMuZHJvcHpvbmUub3B0aW9ucy5oZWFkZXJzID0gdGhpcy5oZWFkZXJzO1xyXG4vLyBcdCAgICAgICAgICAgIHRoaXMuJHJlZnMucmV0dXJuZWRJdGVtRmlsZXMuZHJvcHpvbmUub3B0aW9ucy5hdXRvUHJvY2Vzc1F1ZXVlID0gZmFsc2U7XHJcbi8vIFx0ICAgICAgICAgICAgdGhpcy4kcmVmcy5yZXR1cm5lZEl0ZW1GaWxlcy5kcm9wem9uZS5vcHRpb25zLnBhcmFsbGVsVXBsb2FkcyA9IDEwMDtcclxuLy8gXHQgICAgICAgICAgICAvLyBwYXJhbGxlbFVwbG9hZHMgZGVmaW5lcyBob3cgbWFueSBmaWxlcyBzaG91bGQgYmUgdXBsb2FkZWQgdG8gdGhlIHNlcnZlciBhdCBvbmNlLlxyXG4vLyBcdFx0XHRcdC8vIGF1dG9Qcm9jZXNzUXVldWUgdGVsbHMgRHJvcHpvbmUgdGhhdCBpdCBzaG91bGQgbm90IHN0YXJ0IHRoZSBxdWV1ZSBhdXRvbWF0aWNhbGx5LCBub3QgaW5pdGlhbGx5LCBhbmQgbm90IGFmdGVyIGZpbGVzIGZpbmlzaGVkIHVwbG9hZGluZy5cclxuLy8gXHRcdFx0XHQvLyBJZiB5b3Ugd2FudCBhdXRvUHJvY2Vzc1F1ZXVlIHRvIGJlIHRydWUgYWZ0ZXIgdGhlIGZpcnN0IHVwbG9hZCwgdGhlbiBqdXN0IGxpc3RlbiB0byB0aGUgcHJvY2Vzc2luZyBldmVudCwgYW5kIHNldCB0aGlzLm9wdGlvbnMuYXV0b1Byb2Nlc3NRdWV1ZSA9IHRydWU7IGluc2lkZS5cclxuLy8gXHQgICAgICAgIH0pO1xyXG4vL1xyXG4vLyBcdCAgICAgICAgJCgnI211bHRpcGxlVXBsb2FkTW9kYWwnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZSA9PiB7XHJcbi8vIFx0ICAgICAgICAgICAgdGhpcy5keiA9ICcnO1xyXG4vLyBcdCAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcmRlciA9IHt9O1xyXG4vLyBcdCAgICAgICAgICAgIHRoaXMucmV0dXJuSXRlbUZvcm0gPSBuZXcgRm9ybSgpO1xyXG4vLyBcdCAgICAgICAgfSk7XHJcbi8vIFx0XHR9XHJcbi8vIFx0fSxcclxuLy8gXHRtZXRob2RzOntcclxuLy8gXHRcdGdldEZpbGVzKCkge1xyXG4vLyBcdFx0XHRheGlvcy5nZXQoJy92aXNhL3NlcnZpY2UtbWFuYWdlci9jbGllbnQtZG9jdW1lbnRzL3R5cGUvJyArIHRoaXMudHlwZWlkKVxyXG4vLyBcdCAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuLy8gXHQgICAgICAgICAgXHR0aGlzLmZpbGVzID0gcmVzdWx0LmRhdGE7XHJcbi8vIFx0ICAgIFx0fSk7XHJcbi8vIFx0XHR9LFxyXG4vL1xyXG4vLyBcdFx0bG9hZERvY3MoKXtcclxuLy8gXHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9nYWxsZXJ5JylcclxuLy8gXHQgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbi8vIFx0ICAgICAgICAgIHRoaXMuZG9jdW1lbnRzID0gcmVzdWx0LmRhdGE7XHJcbi8vXHJcbi8vIFx0ICAgICAgICAgIHRoaXMucmVsb2FkRGF0YVRhYmxlKCk7XHJcbi8vIFx0ICAgIFx0fSk7XHJcbi8vIFx0XHR9LFxyXG4vL1xyXG4vLyBcdFx0ZHJvcHpvbmVVcGxvYWRTdWNjZXNzOiBmdW5jdGlvbiAoZmlsZSwgcmVzcG9uc2UpIHtcclxuLy8gICAgICAgICAgICAgbGV0IF9yZXNwb25zZSA9IEpTT04ucGFyc2UocmVzcG9uc2UpO1xyXG4vL1xyXG4vLyAgICAgICAgICAgICBsZXQgeyBzdWNjZXNzLCBtZXNzYWdlLCBwYXRoIH0gPSBfcmVzcG9uc2U7XHJcbi8vXHJcbi8vICAgICAgICAgICAgIGlmKHN1Y2Nlc3MpIHtcclxuLy8gICAgICAgICAgICAgXHR0b2FzdHIuc3VjY2VzcyhtZXNzYWdlKTtcclxuLy9cclxuLy8gICAgICAgICAgICAgXHR0aGlzLiRyZWZzLnJldHVybmVkSXRlbUZpbGVzLmRyb3B6b25lLnJlbW92ZUFsbEZpbGVzKCk7XHJcbi8vXHJcbi8vICAgICAgICAgICAgIFx0dGhpcy5nZXRGaWxlcygpO1xyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfSxcclxuLy9cclxuLy8gICAgICAgICBkcm9wem9uZUZpbGVSZW1vdmVkKGZpbGUsIGVycm9yLCB4aHIpIHtcclxuLy8gICAgICAgICAgICAgLy9cclxuLy8gICAgICAgICB9LFxyXG4vL1xyXG4vLyAgICAgICAgIGRyb3B6b25lVXBsb2FkRXJyb3IoZmlsZSwgbWVzc2FnZSwgeGhyKSB7XHJcbi8vXHJcbi8vICAgICAgICAgfSxcclxuLy9cclxuLy8gICAgICAgICBkcm9wem9uZUZpbGVBZGRlZChmaWxlKSB7XHJcbi8vICAgICAgICAgXHRsZXQgeyBuYW1lLCB0eXBlIH0gPSBmaWxlO1xyXG4vLyAgICAgICAgIFx0bGV0IGZpbGVuYW1lID0gbmFtZS5zdWJzdHIoMCwgbmFtZS5sYXN0SW5kZXhPZignLicpKTtcclxuLy8gXHRcdFx0bGV0IGYgPSBmaWxlbmFtZS5zcGxpdChcIiYmXCIpO1xyXG4vL1xyXG4vLyBcdFx0XHRsZXQgc3VjY2VzcyA9IHRydWU7XHJcbi8vIFx0XHRcdGxldCBtZXNzYWdlID0gJyc7XHJcbi8vXHJcbi8vICAgICAgICAgXHRpZih0eXBlICE9ICdpbWFnZS9qcGVnJyAmJiB0eXBlICE9ICdpbWFnZS9wbmcnKSB7XHJcbi8vICAgICAgICAgXHRcdHN1Y2Nlc3MgPSBmYWxzZTtcclxuLy8gICAgICAgICBcdFx0bWVzc2FnZSA9ICdZb3UgY2FuXFwndCB1cGxvYWQgZmlsZXMgb2YgdGhpcyB0eXBlLic7XHJcbi8vICAgICAgICAgXHR9IGVsc2UgaWYoZi5sZW5ndGggIT0gMykge1xyXG4vLyAgICAgICAgIFx0XHRzdWNjZXNzID0gZmFsc2U7XHJcbi8vICAgICAgICAgXHRcdG1lc3NhZ2UgPSAnSW5jb3JyZWN0IGZpbGVuYW1lIGZvcm1hdCc7XHJcbi8vICAgICAgICAgXHR9IGVsc2UgaWYoaXNOYU4oZlswXSkpIHtcclxuLy8gICAgICAgICBcdFx0c3VjY2VzcyA9IGZhbHNlO1xyXG4vLyAgICAgICAgIFx0XHRtZXNzYWdlID0gJ0luY29ycmVjdCBmaWxlbmFtZSBmb3JtYXQnO1xyXG4vLyAgICAgICAgIFx0fSBlbHNlIGlmKCFtb21lbnQoZlsxXSwgJ1lZWVktTU0tREQnKS5pc1ZhbGlkKCkpIHtcclxuLy8gICAgICAgICBcdFx0c3VjY2VzcyA9IGZhbHNlO1xyXG4vLyAgICAgICAgIFx0XHRtZXNzYWdlID0gJ0luY29ycmVjdCBmaWxlbmFtZSBmb3JtYXQnO1xyXG4vLyAgICAgICAgIFx0fSBlbHNlIGlmKCFtb21lbnQoZlsyXSwgJ1lZWVktTU0tREQnKS5pc1ZhbGlkKCkpIHtcclxuLy8gICAgICAgICBcdFx0c3VjY2VzcyA9IGZhbHNlO1xyXG4vLyAgICAgICAgIFx0XHRtZXNzYWdlID0gJ0luY29ycmVjdCBmaWxlbmFtZSBmb3JtYXQnO1xyXG4vLyAgICAgICAgIFx0fVxyXG4vL1xyXG4vLyAgICAgICAgIFx0aWYoIXN1Y2Nlc3MpIHtcclxuLy8gICAgICAgICBcdFx0dGhpcy4kcmVmcy5yZXR1cm5lZEl0ZW1GaWxlcy5kcm9wem9uZS5yZW1vdmVGaWxlKGZpbGUpO1xyXG4vLyAgICAgICAgIFx0XHR0b2FzdHIuZXJyb3IobWVzc2FnZSwgbmFtZSk7XHJcbi8vICAgICAgICAgXHR9XHJcbi8vICAgICAgICAgfSxcclxuLy9cclxuLy8gICAgICAgICBkcm9wem9uZVF1ZXVlQ29tcGxldGUoZmlsZXMpIHtcclxuLy8gICAgICAgICAgICAgLy8gJCgnI211bHRpcGxlVXBsb2FkTW9kYWwnKS5tb2RhbCgnaGlkZScpO1xyXG4vL1xyXG4vLyAgICAgICAgICAgICAvLyB0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xyXG4vL1xyXG4vLyAgICAgICAgICAgICAvLyB0aGlzLiRyZWZzLnJldHVybmVkSXRlbUZpbGVzLmRyb3B6b25lLnJlbW92ZUFsbEZpbGVzKCk7XHJcbi8vXHJcbi8vICAgICAgICAgICAgIC8vdGhpcy5zZWxlY3RlZE9yZGVyLnN0YXR1cyA9ICdSZXR1cm5lZCc7XHJcbi8vICAgICAgICAgICAgIC8vIHRoaXMucmV0dXJuSXRlbUZvcm0uc3VibWl0dGVkID0gdHJ1ZTtcclxuLy8gICAgICAgICAgICAgLy8gdGhpcy5yZXR1cm5JdGVtRm9ybS5lcnJvcnMuY2xlYXIoKTtcclxuLy8gICAgICAgICB9LFxyXG4vL1xyXG4vLyAgICAgICAgIHN1Ym1pdCgpIHtcclxuLy8gICAgICAgICAgICAgaWYodGhpcy4kcmVmcy5yZXR1cm5lZEl0ZW1GaWxlcy5kcm9wem9uZS5nZXRBY2NlcHRlZEZpbGVzKCkubGVuZ3RoID4gMCkge1xyXG4vLyAgICAgICAgICAgICAgICAgdGhpcy4kcmVmcy5yZXR1cm5lZEl0ZW1GaWxlcy5kcm9wem9uZS5wcm9jZXNzUXVldWUoKTtcclxuLy8gICAgICAgICAgICAgfSBlbHNlIHtcclxuLy8gICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignSW1hZ2VzIGZpZWxkIGlzIHJlcXVpcmVkLicpO1xyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfSxcclxuLy9cclxuLy8gXHRcdHJlbG9hZERhdGFUYWJsZSgpe1xyXG4vLyBcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbi8vIFx0XHRcdFx0JCgnI2xpc3RzJykuRGF0YVRhYmxlKHtcclxuLy8gXHRcdCAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWUsXHJcbi8vIFx0XHQgICAgICAgIH0pO1xyXG4vLyBcdFx0XHR9LDEwMDApO1xyXG4vLyBcdFx0fSxcclxuLy8gXHRcdHNhdmVEb2NzKGUpe1xyXG4vLyBcdFx0ICAgXHRpZihlID09IFwiQWRkXCIpe1xyXG4vLyBcdFx0XHRcdHRoaXMuZG9jc0ZybS5zdWJtaXQoJ3Bvc3QnLCAnL2FsYnVtL2FkZCcpXHJcbi8vIFx0XHRcdCAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4vLyBcdFx0XHQgICAgXHRsZXQgeyBzdWNjZXNzLCBtZXNzYWdlIH0gPSByZXN1bHQ7XHJcbi8vXHJcbi8vIFx0XHRcdCAgICBcdGlmKHN1Y2Nlc3MpIHtcclxuLy8gXHRcdFx0ICAgIFx0XHR0b2FzdHIuc3VjY2VzcyhtZXNzYWdlKTtcclxuLy8gXHRcdFx0ICAgICAgICBcdCQoJyNkaWFsb2dBZGRBbGJ1bScpLm1vZGFsKCd0b2dnbGUnKTtcclxuLy8gXHRcdFx0ICAgICAgICBcdHRoaXMucmVsb2FkQWxsKCk7XHJcbi8vIFx0XHRcdCAgICBcdH1cclxuLy8gXHRcdFx0ICAgIH0pXHJcbi8vIFx0XHRcdCAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4vLyBcdFx0XHQgICAgXHRmb3IodmFyIGtleSBpbiBlcnJvcikge1xyXG4vLyBcdFx0XHRcdFx0ICAgIGlmKGVycm9yLmhhc093blByb3BlcnR5KGtleSkpIHtcclxuLy8gXHRcdFx0XHRcdCAgICBcdHRvYXN0ci5lcnJvcihlcnJvcltrZXldKTtcclxuLy8gXHRcdFx0XHRcdCAgICB9XHJcbi8vIFx0XHRcdFx0XHR9XHJcbi8vIFx0XHRcdCAgICB9KTtcclxuLy8gXHRcdFx0fWVsc2UgaWYgKGUgPT0gXCJFZGl0XCIpe1xyXG4vLyBcdFx0XHRcdHRoaXMuZG9jc0ZybS5zdWJtaXQoJ3Bvc3QnLCAnL2VkaXQnKVxyXG4vLyBcdFx0XHQgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuLy8gXHRcdFx0ICAgIFx0bGV0IHsgc3VjY2VzcywgbWVzc2FnZSB9ID0gcmVzdWx0O1xyXG4vL1xyXG4vLyBcdFx0XHQgICAgXHRpZihzdWNjZXNzKSB7XHJcbi8vIFx0XHRcdCAgICBcdFx0dG9hc3RyLnN1Y2Nlc3MobWVzc2FnZSk7XHJcbi8vIFx0XHRcdCAgICAgICAgXHQkKCcjZGlhbG9nQWRkRG9jdW1lbnQnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbi8vIFx0XHRcdCAgICAgICAgXHR0aGlzLnJlbG9hZEFsbCgpO1xyXG4vLyBcdFx0XHQgICAgXHR9XHJcbi8vIFx0XHRcdCAgICB9KVxyXG4vLyBcdFx0XHQgICAgLmNhdGNoKGVycm9yID0+IHtcclxuLy8gXHRcdFx0ICAgICAgICBmb3IodmFyIGtleSBpbiBlcnJvcikge1xyXG4vLyBcdFx0XHRcdFx0ICAgIGlmKGVycm9yLmhhc093blByb3BlcnR5KGtleSkpIHtcclxuLy8gXHRcdFx0XHRcdCAgICBcdHRvYXN0ci5lcnJvcihlcnJvcltrZXldKTtcclxuLy8gXHRcdFx0XHRcdCAgICB9XHJcbi8vIFx0XHRcdFx0XHR9XHJcbi8vIFx0XHRcdCAgICB9KTtcclxuLy8gXHRcdFx0fWVsc2UgaWYgKGUgPT0gXCJEZWxldGVcIil7XHJcbi8vIFx0XHRcdFx0YXhpb3MucG9zdCgnL3Zpc2Evc2VydmljZS1tYW5hZ2VyL3NlcnZpY2UtZG9jdW1lbnRzL2RlbGV0ZS8nKyB0aGlzLnNlbF9pZClcclxuLy8gXHRcdFx0ICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbi8vIFx0XHRcdCAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1NlcnZpY2UgRG9jdW1lbnQgRGVsZXRlZCcpO1xyXG4vLyBcdFx0XHQgICAgICAgIHRoaXMucmVsb2FkQWxsKCk7XHJcbi8vIFx0XHRcdCAgICB9KVxyXG4vLyBcdFx0XHQgICAgLmNhdGNoKGVycm9yID0+IHtcclxuLy8gXHRcdFx0ICAgICAgICB0b2FzdHIuZXJyb3IoJ0RlbGV0ZSBGYWlsZWQuJyk7XHJcbi8vIFx0XHRcdCAgICB9KTtcclxuLy8gXHRcdFx0fVxyXG4vLyBcdFx0fSxcclxuLy9cclxuLy8gXHRcdGFjdGlvbkRvYyhlLCBpZCl7XHJcbi8vIFx0XHRcdGlmKGUgPT0gXCJhZGRcIil7XHJcbi8vIFx0XHRcdFx0JCgnI2RpYWxvZ0FkZERvY3VtZW50JykubW9kYWwoJ3RvZ2dsZScpO1xyXG4vLyBcdFx0XHRcdHRoaXMuYWN0ID0gXCJBZGRcIjtcclxuLy8gXHRcdFx0fWVsc2UgaWYgKGUgPT0gXCJlZGl0XCIpe1xyXG4vLyBcdFx0XHRcdHRoaXMuYWN0ID0gXCJFZGl0XCI7XHJcbi8vIFx0XHRcdFx0YXhpb3MuZ2V0KCcvdmlzYS9zZXJ2aWNlLW1hbmFnZXIvY2xpZW50LWRvY3VtZW50cy9pbmZvLycrIGlkKVxyXG4vLyBcdFx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+e1xyXG4vLyBcdFx0XHRcdFx0dGhpcy5kb2NzRnJtLmlkID0gcmVzdWx0LmRhdGEuaWQ7XHJcbi8vIFx0XHRcdFx0XHR0aGlzLmRvY3NGcm0ubmFtZSA9IHJlc3VsdC5kYXRhLm5hbWU7XHJcbi8vIFx0XHRcdFx0fSlcclxuLy8gXHRcdFx0XHQkKCcjZGlhbG9nQWRkRG9jdW1lbnQnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbi8vIFx0XHRcdH1lbHNlIGlmIChlID09IFwiZGVsZXRlXCIpe1xyXG4vLyBcdFx0XHRcdHRoaXMuYWN0ID0gXCJEZWxldGVcIjtcclxuLy8gXHRcdFx0XHR0aGlzLnNlbF9pZCA9IGlkO1xyXG4vLyBcdFx0XHRcdCQoJyNkaWFsb2dSZW1vdmVEb2N1bWVudCcpLm1vZGFsKCd0b2dnbGUnKTtcclxuLy8gXHRcdFx0fVxyXG4vLyBcdFx0fSxcclxuLy9cclxuLy8gXHRcdHJlc2V0RG9jRnJtKCl7XHJcbi8vIFx0XHRcdHRoaXMuZG9jc0ZybSA9IG5ldyBGb3JtKHtcclxuLy8gXHRcdFx0XHRpZCBcdFx0OiAnJyxcclxuLy8gXHRcdCAgICAgICAgbmFtZSBcdDogJycsXHJcbi8vIFx0XHQgICAgfSx7IGJhc2VVUkxcdDogJy92aXNhL2dhbGxlcnknfSlcclxuLy8gXHRcdH0sXHJcbi8vIFx0XHRyZWxvYWRBbGwoKXtcclxuLy8gXHRcdFx0JCgnI2xpc3RzJykuRGF0YVRhYmxlKCkuZGVzdHJveSgpO1xyXG4vLyBcdFx0XHR0aGlzLnJlc2V0RG9jRnJtKCk7XHJcbi8vIFx0XHRcdHRoaXMubG9hZERvY3MoKTtcclxuLy8gXHRcdH1cclxuLy8gXHR9LFxyXG4vLyBcdGNyZWF0ZWQoKXtcclxuLy8gXHRcdHRoaXMubG9hZERvY3MoKTtcclxuLy8gXHR9LFxyXG4vL1xyXG4vLyBcdGNvbXBvbmVudHM6IHtcclxuLy8gXHRcdERyb3B6b25lXHJcbi8vIFx0fVxyXG4vLyB9KTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9nYWxsZXJ5L2FsYnVtLmpzIiwiLyohXG4gKiB2dWUtcmVzb3VyY2UgdjEuNS4xXG4gKiBodHRwczovL2dpdGh1Yi5jb20vcGFnZWtpdC92dWUtcmVzb3VyY2VcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS5cbiAqL1xuXG4vKipcbiAqIFByb21pc2VzL0ErIHBvbHlmaWxsIHYxLjEuNCAoaHR0cHM6Ly9naXRodWIuY29tL2JyYW1zdGVpbi9wcm9taXMpXG4gKi9cblxudmFyIFJFU09MVkVEID0gMDtcbnZhciBSRUpFQ1RFRCA9IDE7XG52YXIgUEVORElORyA9IDI7XG5cbmZ1bmN0aW9uIFByb21pc2UkMShleGVjdXRvcikge1xuXG4gICAgdGhpcy5zdGF0ZSA9IFBFTkRJTkc7XG4gICAgdGhpcy52YWx1ZSA9IHVuZGVmaW5lZDtcbiAgICB0aGlzLmRlZmVycmVkID0gW107XG5cbiAgICB2YXIgcHJvbWlzZSA9IHRoaXM7XG5cbiAgICB0cnkge1xuICAgICAgICBleGVjdXRvcihmdW5jdGlvbiAoeCkge1xuICAgICAgICAgICAgcHJvbWlzZS5yZXNvbHZlKHgpO1xuICAgICAgICB9LCBmdW5jdGlvbiAocikge1xuICAgICAgICAgICAgcHJvbWlzZS5yZWplY3Qocik7XG4gICAgICAgIH0pO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcHJvbWlzZS5yZWplY3QoZSk7XG4gICAgfVxufVxuXG5Qcm9taXNlJDEucmVqZWN0ID0gZnVuY3Rpb24gKHIpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UkMShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIHJlamVjdChyKTtcbiAgICB9KTtcbn07XG5cblByb21pc2UkMS5yZXNvbHZlID0gZnVuY3Rpb24gKHgpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UkMShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIHJlc29sdmUoeCk7XG4gICAgfSk7XG59O1xuXG5Qcm9taXNlJDEuYWxsID0gZnVuY3Rpb24gYWxsKGl0ZXJhYmxlKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlJDEoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICB2YXIgY291bnQgPSAwLCByZXN1bHQgPSBbXTtcblxuICAgICAgICBpZiAoaXRlcmFibGUubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiByZXNvbHZlcihpKSB7XG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKHgpIHtcbiAgICAgICAgICAgICAgICByZXN1bHRbaV0gPSB4O1xuICAgICAgICAgICAgICAgIGNvdW50ICs9IDE7XG5cbiAgICAgICAgICAgICAgICBpZiAoY291bnQgPT09IGl0ZXJhYmxlLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaXRlcmFibGUubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgICAgIFByb21pc2UkMS5yZXNvbHZlKGl0ZXJhYmxlW2ldKS50aGVuKHJlc29sdmVyKGkpLCByZWplY3QpO1xuICAgICAgICB9XG4gICAgfSk7XG59O1xuXG5Qcm9taXNlJDEucmFjZSA9IGZ1bmN0aW9uIHJhY2UoaXRlcmFibGUpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UkMShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaXRlcmFibGUubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgICAgIFByb21pc2UkMS5yZXNvbHZlKGl0ZXJhYmxlW2ldKS50aGVuKHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgIH1cbiAgICB9KTtcbn07XG5cbnZhciBwID0gUHJvbWlzZSQxLnByb3RvdHlwZTtcblxucC5yZXNvbHZlID0gZnVuY3Rpb24gcmVzb2x2ZSh4KSB7XG4gICAgdmFyIHByb21pc2UgPSB0aGlzO1xuXG4gICAgaWYgKHByb21pc2Uuc3RhdGUgPT09IFBFTkRJTkcpIHtcbiAgICAgICAgaWYgKHggPT09IHByb21pc2UpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1Byb21pc2Ugc2V0dGxlZCB3aXRoIGl0c2VsZi4nKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBjYWxsZWQgPSBmYWxzZTtcblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdmFyIHRoZW4gPSB4ICYmIHhbJ3RoZW4nXTtcblxuICAgICAgICAgICAgaWYgKHggIT09IG51bGwgJiYgdHlwZW9mIHggPT09ICdvYmplY3QnICYmIHR5cGVvZiB0aGVuID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgdGhlbi5jYWxsKHgsIGZ1bmN0aW9uICh4KSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghY2FsbGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9taXNlLnJlc29sdmUoeCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2FsbGVkID0gdHJ1ZTtcblxuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChyKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghY2FsbGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9taXNlLnJlamVjdChyKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjYWxsZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgaWYgKCFjYWxsZWQpIHtcbiAgICAgICAgICAgICAgICBwcm9taXNlLnJlamVjdChlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHByb21pc2Uuc3RhdGUgPSBSRVNPTFZFRDtcbiAgICAgICAgcHJvbWlzZS52YWx1ZSA9IHg7XG4gICAgICAgIHByb21pc2Uubm90aWZ5KCk7XG4gICAgfVxufTtcblxucC5yZWplY3QgPSBmdW5jdGlvbiByZWplY3QocmVhc29uKSB7XG4gICAgdmFyIHByb21pc2UgPSB0aGlzO1xuXG4gICAgaWYgKHByb21pc2Uuc3RhdGUgPT09IFBFTkRJTkcpIHtcbiAgICAgICAgaWYgKHJlYXNvbiA9PT0gcHJvbWlzZSkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignUHJvbWlzZSBzZXR0bGVkIHdpdGggaXRzZWxmLicpO1xuICAgICAgICB9XG5cbiAgICAgICAgcHJvbWlzZS5zdGF0ZSA9IFJFSkVDVEVEO1xuICAgICAgICBwcm9taXNlLnZhbHVlID0gcmVhc29uO1xuICAgICAgICBwcm9taXNlLm5vdGlmeSgpO1xuICAgIH1cbn07XG5cbnAubm90aWZ5ID0gZnVuY3Rpb24gbm90aWZ5KCkge1xuICAgIHZhciBwcm9taXNlID0gdGhpcztcblxuICAgIG5leHRUaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHByb21pc2Uuc3RhdGUgIT09IFBFTkRJTkcpIHtcbiAgICAgICAgICAgIHdoaWxlIChwcm9taXNlLmRlZmVycmVkLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgIHZhciBkZWZlcnJlZCA9IHByb21pc2UuZGVmZXJyZWQuc2hpZnQoKSxcbiAgICAgICAgICAgICAgICAgICAgb25SZXNvbHZlZCA9IGRlZmVycmVkWzBdLFxuICAgICAgICAgICAgICAgICAgICBvblJlamVjdGVkID0gZGVmZXJyZWRbMV0sXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUgPSBkZWZlcnJlZFsyXSxcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0ID0gZGVmZXJyZWRbM107XG5cbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBpZiAocHJvbWlzZS5zdGF0ZSA9PT0gUkVTT0xWRUQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2Ygb25SZXNvbHZlZCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUob25SZXNvbHZlZC5jYWxsKHVuZGVmaW5lZCwgcHJvbWlzZS52YWx1ZSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHByb21pc2UudmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHByb21pc2Uuc3RhdGUgPT09IFJFSkVDVEVEKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG9uUmVqZWN0ZWQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG9uUmVqZWN0ZWQuY2FsbCh1bmRlZmluZWQsIHByb21pc2UudmFsdWUpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KHByb21pc2UudmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSk7XG59O1xuXG5wLnRoZW4gPSBmdW5jdGlvbiB0aGVuKG9uUmVzb2x2ZWQsIG9uUmVqZWN0ZWQpIHtcbiAgICB2YXIgcHJvbWlzZSA9IHRoaXM7XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2UkMShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIHByb21pc2UuZGVmZXJyZWQucHVzaChbb25SZXNvbHZlZCwgb25SZWplY3RlZCwgcmVzb2x2ZSwgcmVqZWN0XSk7XG4gICAgICAgIHByb21pc2Uubm90aWZ5KCk7XG4gICAgfSk7XG59O1xuXG5wLmNhdGNoID0gZnVuY3Rpb24gKG9uUmVqZWN0ZWQpIHtcbiAgICByZXR1cm4gdGhpcy50aGVuKHVuZGVmaW5lZCwgb25SZWplY3RlZCk7XG59O1xuXG4vKipcbiAqIFByb21pc2UgYWRhcHRlci5cbiAqL1xuXG5pZiAodHlwZW9mIFByb21pc2UgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgd2luZG93LlByb21pc2UgPSBQcm9taXNlJDE7XG59XG5cbmZ1bmN0aW9uIFByb21pc2VPYmooZXhlY3V0b3IsIGNvbnRleHQpIHtcblxuICAgIGlmIChleGVjdXRvciBpbnN0YW5jZW9mIFByb21pc2UpIHtcbiAgICAgICAgdGhpcy5wcm9taXNlID0gZXhlY3V0b3I7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5wcm9taXNlID0gbmV3IFByb21pc2UoZXhlY3V0b3IuYmluZChjb250ZXh0KSk7XG4gICAgfVxuXG4gICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbn1cblxuUHJvbWlzZU9iai5hbGwgPSBmdW5jdGlvbiAoaXRlcmFibGUsIGNvbnRleHQpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2VPYmooUHJvbWlzZS5hbGwoaXRlcmFibGUpLCBjb250ZXh0KTtcbn07XG5cblByb21pc2VPYmoucmVzb2x2ZSA9IGZ1bmN0aW9uICh2YWx1ZSwgY29udGV4dCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZU9iaihQcm9taXNlLnJlc29sdmUodmFsdWUpLCBjb250ZXh0KTtcbn07XG5cblByb21pc2VPYmoucmVqZWN0ID0gZnVuY3Rpb24gKHJlYXNvbiwgY29udGV4dCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZU9iaihQcm9taXNlLnJlamVjdChyZWFzb24pLCBjb250ZXh0KTtcbn07XG5cblByb21pc2VPYmoucmFjZSA9IGZ1bmN0aW9uIChpdGVyYWJsZSwgY29udGV4dCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZU9iaihQcm9taXNlLnJhY2UoaXRlcmFibGUpLCBjb250ZXh0KTtcbn07XG5cbnZhciBwJDEgPSBQcm9taXNlT2JqLnByb3RvdHlwZTtcblxucCQxLmJpbmQgPSBmdW5jdGlvbiAoY29udGV4dCkge1xuICAgIHRoaXMuY29udGV4dCA9IGNvbnRleHQ7XG4gICAgcmV0dXJuIHRoaXM7XG59O1xuXG5wJDEudGhlbiA9IGZ1bmN0aW9uIChmdWxmaWxsZWQsIHJlamVjdGVkKSB7XG5cbiAgICBpZiAoZnVsZmlsbGVkICYmIGZ1bGZpbGxlZC5iaW5kICYmIHRoaXMuY29udGV4dCkge1xuICAgICAgICBmdWxmaWxsZWQgPSBmdWxmaWxsZWQuYmluZCh0aGlzLmNvbnRleHQpO1xuICAgIH1cblxuICAgIGlmIChyZWplY3RlZCAmJiByZWplY3RlZC5iaW5kICYmIHRoaXMuY29udGV4dCkge1xuICAgICAgICByZWplY3RlZCA9IHJlamVjdGVkLmJpbmQodGhpcy5jb250ZXh0KTtcbiAgICB9XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2VPYmoodGhpcy5wcm9taXNlLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCksIHRoaXMuY29udGV4dCk7XG59O1xuXG5wJDEuY2F0Y2ggPSBmdW5jdGlvbiAocmVqZWN0ZWQpIHtcblxuICAgIGlmIChyZWplY3RlZCAmJiByZWplY3RlZC5iaW5kICYmIHRoaXMuY29udGV4dCkge1xuICAgICAgICByZWplY3RlZCA9IHJlamVjdGVkLmJpbmQodGhpcy5jb250ZXh0KTtcbiAgICB9XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2VPYmoodGhpcy5wcm9taXNlLmNhdGNoKHJlamVjdGVkKSwgdGhpcy5jb250ZXh0KTtcbn07XG5cbnAkMS5maW5hbGx5ID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG5cbiAgICByZXR1cm4gdGhpcy50aGVuKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICBjYWxsYmFjay5jYWxsKHRoaXMpO1xuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgfSwgZnVuY3Rpb24gKHJlYXNvbikge1xuICAgICAgICBjYWxsYmFjay5jYWxsKHRoaXMpO1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QocmVhc29uKTtcbiAgICB9XG4gICAgKTtcbn07XG5cbi8qKlxuICogVXRpbGl0eSBmdW5jdGlvbnMuXG4gKi9cblxudmFyIHJlZiA9IHt9O1xudmFyIGhhc093blByb3BlcnR5ID0gcmVmLmhhc093blByb3BlcnR5O1xudmFyIHJlZiQxID0gW107XG52YXIgc2xpY2UgPSByZWYkMS5zbGljZTtcbnZhciBkZWJ1ZyA9IGZhbHNlLCBudGljaztcblxudmFyIGluQnJvd3NlciA9IHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnO1xuXG5mdW5jdGlvbiBVdGlsIChyZWYpIHtcbiAgICB2YXIgY29uZmlnID0gcmVmLmNvbmZpZztcbiAgICB2YXIgbmV4dFRpY2sgPSByZWYubmV4dFRpY2s7XG5cbiAgICBudGljayA9IG5leHRUaWNrO1xuICAgIGRlYnVnID0gY29uZmlnLmRlYnVnIHx8ICFjb25maWcuc2lsZW50O1xufVxuXG5mdW5jdGlvbiB3YXJuKG1zZykge1xuICAgIGlmICh0eXBlb2YgY29uc29sZSAhPT0gJ3VuZGVmaW5lZCcgJiYgZGVidWcpIHtcbiAgICAgICAgY29uc29sZS53YXJuKCdbVnVlUmVzb3VyY2Ugd2Fybl06ICcgKyBtc2cpO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gZXJyb3IobXNnKSB7XG4gICAgaWYgKHR5cGVvZiBjb25zb2xlICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBjb25zb2xlLmVycm9yKG1zZyk7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBuZXh0VGljayhjYiwgY3R4KSB7XG4gICAgcmV0dXJuIG50aWNrKGNiLCBjdHgpO1xufVxuXG5mdW5jdGlvbiB0cmltKHN0cikge1xuICAgIHJldHVybiBzdHIgPyBzdHIucmVwbGFjZSgvXlxccyp8XFxzKiQvZywgJycpIDogJyc7XG59XG5cbmZ1bmN0aW9uIHRyaW1FbmQoc3RyLCBjaGFycykge1xuXG4gICAgaWYgKHN0ciAmJiBjaGFycyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJldHVybiBzdHIucmVwbGFjZSgvXFxzKyQvLCAnJyk7XG4gICAgfVxuXG4gICAgaWYgKCFzdHIgfHwgIWNoYXJzKSB7XG4gICAgICAgIHJldHVybiBzdHI7XG4gICAgfVxuXG4gICAgcmV0dXJuIHN0ci5yZXBsYWNlKG5ldyBSZWdFeHAoKFwiW1wiICsgY2hhcnMgKyBcIl0rJFwiKSksICcnKTtcbn1cblxuZnVuY3Rpb24gdG9Mb3dlcihzdHIpIHtcbiAgICByZXR1cm4gc3RyID8gc3RyLnRvTG93ZXJDYXNlKCkgOiAnJztcbn1cblxuZnVuY3Rpb24gdG9VcHBlcihzdHIpIHtcbiAgICByZXR1cm4gc3RyID8gc3RyLnRvVXBwZXJDYXNlKCkgOiAnJztcbn1cblxudmFyIGlzQXJyYXkgPSBBcnJheS5pc0FycmF5O1xuXG5mdW5jdGlvbiBpc1N0cmluZyh2YWwpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ3N0cmluZyc7XG59XG5cbmZ1bmN0aW9uIGlzRnVuY3Rpb24odmFsKSB7XG4gICAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICdmdW5jdGlvbic7XG59XG5cbmZ1bmN0aW9uIGlzT2JqZWN0KG9iaikge1xuICAgIHJldHVybiBvYmogIT09IG51bGwgJiYgdHlwZW9mIG9iaiA9PT0gJ29iamVjdCc7XG59XG5cbmZ1bmN0aW9uIGlzUGxhaW5PYmplY3Qob2JqKSB7XG4gICAgcmV0dXJuIGlzT2JqZWN0KG9iaikgJiYgT2JqZWN0LmdldFByb3RvdHlwZU9mKG9iaikgPT0gT2JqZWN0LnByb3RvdHlwZTtcbn1cblxuZnVuY3Rpb24gaXNCbG9iKG9iaikge1xuICAgIHJldHVybiB0eXBlb2YgQmxvYiAhPT0gJ3VuZGVmaW5lZCcgJiYgb2JqIGluc3RhbmNlb2YgQmxvYjtcbn1cblxuZnVuY3Rpb24gaXNGb3JtRGF0YShvYmopIHtcbiAgICByZXR1cm4gdHlwZW9mIEZvcm1EYXRhICE9PSAndW5kZWZpbmVkJyAmJiBvYmogaW5zdGFuY2VvZiBGb3JtRGF0YTtcbn1cblxuZnVuY3Rpb24gd2hlbih2YWx1ZSwgZnVsZmlsbGVkLCByZWplY3RlZCkge1xuXG4gICAgdmFyIHByb21pc2UgPSBQcm9taXNlT2JqLnJlc29sdmUodmFsdWUpO1xuXG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPCAyKSB7XG4gICAgICAgIHJldHVybiBwcm9taXNlO1xuICAgIH1cblxuICAgIHJldHVybiBwcm9taXNlLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7XG59XG5cbmZ1bmN0aW9uIG9wdGlvbnMoZm4sIG9iaiwgb3B0cykge1xuXG4gICAgb3B0cyA9IG9wdHMgfHwge307XG5cbiAgICBpZiAoaXNGdW5jdGlvbihvcHRzKSkge1xuICAgICAgICBvcHRzID0gb3B0cy5jYWxsKG9iaik7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1lcmdlKGZuLmJpbmQoeyR2bTogb2JqLCAkb3B0aW9uczogb3B0c30pLCBmbiwgeyRvcHRpb25zOiBvcHRzfSk7XG59XG5cbmZ1bmN0aW9uIGVhY2gob2JqLCBpdGVyYXRvcikge1xuXG4gICAgdmFyIGksIGtleTtcblxuICAgIGlmIChpc0FycmF5KG9iaikpIHtcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IG9iai5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaXRlcmF0b3IuY2FsbChvYmpbaV0sIG9ialtpXSwgaSk7XG4gICAgICAgIH1cbiAgICB9IGVsc2UgaWYgKGlzT2JqZWN0KG9iaikpIHtcbiAgICAgICAgZm9yIChrZXkgaW4gb2JqKSB7XG4gICAgICAgICAgICBpZiAoaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGtleSkpIHtcbiAgICAgICAgICAgICAgICBpdGVyYXRvci5jYWxsKG9ialtrZXldLCBvYmpba2V5XSwga2V5KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBvYmo7XG59XG5cbnZhciBhc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IF9hc3NpZ247XG5cbmZ1bmN0aW9uIG1lcmdlKHRhcmdldCkge1xuXG4gICAgdmFyIGFyZ3MgPSBzbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG5cbiAgICBhcmdzLmZvckVhY2goZnVuY3Rpb24gKHNvdXJjZSkge1xuICAgICAgICBfbWVyZ2UodGFyZ2V0LCBzb3VyY2UsIHRydWUpO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRhcmdldDtcbn1cblxuZnVuY3Rpb24gZGVmYXVsdHModGFyZ2V0KSB7XG5cbiAgICB2YXIgYXJncyA9IHNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcblxuICAgIGFyZ3MuZm9yRWFjaChmdW5jdGlvbiAoc291cmNlKSB7XG5cbiAgICAgICAgZm9yICh2YXIga2V5IGluIHNvdXJjZSkge1xuICAgICAgICAgICAgaWYgKHRhcmdldFtrZXldID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9KTtcblxuICAgIHJldHVybiB0YXJnZXQ7XG59XG5cbmZ1bmN0aW9uIF9hc3NpZ24odGFyZ2V0KSB7XG5cbiAgICB2YXIgYXJncyA9IHNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcblxuICAgIGFyZ3MuZm9yRWFjaChmdW5jdGlvbiAoc291cmNlKSB7XG4gICAgICAgIF9tZXJnZSh0YXJnZXQsIHNvdXJjZSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGFyZ2V0O1xufVxuXG5mdW5jdGlvbiBfbWVyZ2UodGFyZ2V0LCBzb3VyY2UsIGRlZXApIHtcbiAgICBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7XG4gICAgICAgIGlmIChkZWVwICYmIChpc1BsYWluT2JqZWN0KHNvdXJjZVtrZXldKSB8fCBpc0FycmF5KHNvdXJjZVtrZXldKSkpIHtcbiAgICAgICAgICAgIGlmIChpc1BsYWluT2JqZWN0KHNvdXJjZVtrZXldKSAmJiAhaXNQbGFpbk9iamVjdCh0YXJnZXRba2V5XSkpIHtcbiAgICAgICAgICAgICAgICB0YXJnZXRba2V5XSA9IHt9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGlzQXJyYXkoc291cmNlW2tleV0pICYmICFpc0FycmF5KHRhcmdldFtrZXldKSkge1xuICAgICAgICAgICAgICAgIHRhcmdldFtrZXldID0gW107XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBfbWVyZ2UodGFyZ2V0W2tleV0sIHNvdXJjZVtrZXldLCBkZWVwKTtcbiAgICAgICAgfSBlbHNlIGlmIChzb3VyY2Vba2V5XSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4vKipcbiAqIFJvb3QgUHJlZml4IFRyYW5zZm9ybS5cbiAqL1xuXG5mdW5jdGlvbiByb290IChvcHRpb25zJCQxLCBuZXh0KSB7XG5cbiAgICB2YXIgdXJsID0gbmV4dChvcHRpb25zJCQxKTtcblxuICAgIGlmIChpc1N0cmluZyhvcHRpb25zJCQxLnJvb3QpICYmICEvXihodHRwcz86KT9cXC8vLnRlc3QodXJsKSkge1xuICAgICAgICB1cmwgPSB0cmltRW5kKG9wdGlvbnMkJDEucm9vdCwgJy8nKSArICcvJyArIHVybDtcbiAgICB9XG5cbiAgICByZXR1cm4gdXJsO1xufVxuXG4vKipcbiAqIFF1ZXJ5IFBhcmFtZXRlciBUcmFuc2Zvcm0uXG4gKi9cblxuZnVuY3Rpb24gcXVlcnkgKG9wdGlvbnMkJDEsIG5leHQpIHtcblxuICAgIHZhciB1cmxQYXJhbXMgPSBPYmplY3Qua2V5cyhVcmwub3B0aW9ucy5wYXJhbXMpLCBxdWVyeSA9IHt9LCB1cmwgPSBuZXh0KG9wdGlvbnMkJDEpO1xuXG4gICAgZWFjaChvcHRpb25zJCQxLnBhcmFtcywgZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcbiAgICAgICAgaWYgKHVybFBhcmFtcy5pbmRleE9mKGtleSkgPT09IC0xKSB7XG4gICAgICAgICAgICBxdWVyeVtrZXldID0gdmFsdWU7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIHF1ZXJ5ID0gVXJsLnBhcmFtcyhxdWVyeSk7XG5cbiAgICBpZiAocXVlcnkpIHtcbiAgICAgICAgdXJsICs9ICh1cmwuaW5kZXhPZignPycpID09IC0xID8gJz8nIDogJyYnKSArIHF1ZXJ5O1xuICAgIH1cblxuICAgIHJldHVybiB1cmw7XG59XG5cbi8qKlxuICogVVJMIFRlbXBsYXRlIHYyLjAuNiAoaHR0cHM6Ly9naXRodWIuY29tL2JyYW1zdGVpbi91cmwtdGVtcGxhdGUpXG4gKi9cblxuZnVuY3Rpb24gZXhwYW5kKHVybCwgcGFyYW1zLCB2YXJpYWJsZXMpIHtcblxuICAgIHZhciB0bXBsID0gcGFyc2UodXJsKSwgZXhwYW5kZWQgPSB0bXBsLmV4cGFuZChwYXJhbXMpO1xuXG4gICAgaWYgKHZhcmlhYmxlcykge1xuICAgICAgICB2YXJpYWJsZXMucHVzaC5hcHBseSh2YXJpYWJsZXMsIHRtcGwudmFycyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGV4cGFuZGVkO1xufVxuXG5mdW5jdGlvbiBwYXJzZSh0ZW1wbGF0ZSkge1xuXG4gICAgdmFyIG9wZXJhdG9ycyA9IFsnKycsICcjJywgJy4nLCAnLycsICc7JywgJz8nLCAnJiddLCB2YXJpYWJsZXMgPSBbXTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIHZhcnM6IHZhcmlhYmxlcyxcbiAgICAgICAgZXhwYW5kOiBmdW5jdGlvbiBleHBhbmQoY29udGV4dCkge1xuICAgICAgICAgICAgcmV0dXJuIHRlbXBsYXRlLnJlcGxhY2UoL1xceyhbXnt9XSspXFx9fChbXnt9XSspL2csIGZ1bmN0aW9uIChfLCBleHByZXNzaW9uLCBsaXRlcmFsKSB7XG4gICAgICAgICAgICAgICAgaWYgKGV4cHJlc3Npb24pIHtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgb3BlcmF0b3IgPSBudWxsLCB2YWx1ZXMgPSBbXTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAob3BlcmF0b3JzLmluZGV4T2YoZXhwcmVzc2lvbi5jaGFyQXQoMCkpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BlcmF0b3IgPSBleHByZXNzaW9uLmNoYXJBdCgwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb24gPSBleHByZXNzaW9uLnN1YnN0cigxKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb24uc3BsaXQoLywvZykuZm9yRWFjaChmdW5jdGlvbiAodmFyaWFibGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0bXAgPSAvKFteOipdKikoPzo6KFxcZCspfChcXCopKT8vLmV4ZWModmFyaWFibGUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzLnB1c2guYXBwbHkodmFsdWVzLCBnZXRWYWx1ZXMoY29udGV4dCwgb3BlcmF0b3IsIHRtcFsxXSwgdG1wWzJdIHx8IHRtcFszXSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyaWFibGVzLnB1c2godG1wWzFdKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wZXJhdG9yICYmIG9wZXJhdG9yICE9PSAnKycpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHNlcGFyYXRvciA9ICcsJztcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9wZXJhdG9yID09PSAnPycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXBhcmF0b3IgPSAnJic7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKG9wZXJhdG9yICE9PSAnIycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXBhcmF0b3IgPSBvcGVyYXRvcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICh2YWx1ZXMubGVuZ3RoICE9PSAwID8gb3BlcmF0b3IgOiAnJykgKyB2YWx1ZXMuam9pbihzZXBhcmF0b3IpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlcy5qb2luKCcsJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBlbmNvZGVSZXNlcnZlZChsaXRlcmFsKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH07XG59XG5cbmZ1bmN0aW9uIGdldFZhbHVlcyhjb250ZXh0LCBvcGVyYXRvciwga2V5LCBtb2RpZmllcikge1xuXG4gICAgdmFyIHZhbHVlID0gY29udGV4dFtrZXldLCByZXN1bHQgPSBbXTtcblxuICAgIGlmIChpc0RlZmluZWQodmFsdWUpICYmIHZhbHVlICE9PSAnJykge1xuICAgICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJyB8fCB0eXBlb2YgdmFsdWUgPT09ICdudW1iZXInIHx8IHR5cGVvZiB2YWx1ZSA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnRvU3RyaW5nKCk7XG5cbiAgICAgICAgICAgIGlmIChtb2RpZmllciAmJiBtb2RpZmllciAhPT0gJyonKSB7XG4gICAgICAgICAgICAgICAgdmFsdWUgPSB2YWx1ZS5zdWJzdHJpbmcoMCwgcGFyc2VJbnQobW9kaWZpZXIsIDEwKSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJlc3VsdC5wdXNoKGVuY29kZVZhbHVlKG9wZXJhdG9yLCB2YWx1ZSwgaXNLZXlPcGVyYXRvcihvcGVyYXRvcikgPyBrZXkgOiBudWxsKSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAobW9kaWZpZXIgPT09ICcqJykge1xuICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xuICAgICAgICAgICAgICAgICAgICB2YWx1ZS5maWx0ZXIoaXNEZWZpbmVkKS5mb3JFYWNoKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LnB1c2goZW5jb2RlVmFsdWUob3BlcmF0b3IsIHZhbHVlLCBpc0tleU9wZXJhdG9yKG9wZXJhdG9yKSA/IGtleSA6IG51bGwpKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXModmFsdWUpLmZvckVhY2goZnVuY3Rpb24gKGspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc0RlZmluZWQodmFsdWVba10pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LnB1c2goZW5jb2RlVmFsdWUob3BlcmF0b3IsIHZhbHVlW2tdLCBrKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdmFyIHRtcCA9IFtdO1xuXG4gICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlLmZpbHRlcihpc0RlZmluZWQpLmZvckVhY2goZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0bXAucHVzaChlbmNvZGVWYWx1ZShvcGVyYXRvciwgdmFsdWUpKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXModmFsdWUpLmZvckVhY2goZnVuY3Rpb24gKGspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc0RlZmluZWQodmFsdWVba10pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG1wLnB1c2goZW5jb2RlVVJJQ29tcG9uZW50KGspKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0bXAucHVzaChlbmNvZGVWYWx1ZShvcGVyYXRvciwgdmFsdWVba10udG9TdHJpbmcoKSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoaXNLZXlPcGVyYXRvcihvcGVyYXRvcikpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LnB1c2goZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnPScgKyB0bXAuam9pbignLCcpKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRtcC5sZW5ndGggIT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LnB1c2godG1wLmpvaW4oJywnKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKG9wZXJhdG9yID09PSAnOycpIHtcbiAgICAgICAgICAgIHJlc3VsdC5wdXNoKGVuY29kZVVSSUNvbXBvbmVudChrZXkpKTtcbiAgICAgICAgfSBlbHNlIGlmICh2YWx1ZSA9PT0gJycgJiYgKG9wZXJhdG9yID09PSAnJicgfHwgb3BlcmF0b3IgPT09ICc/JykpIHtcbiAgICAgICAgICAgIHJlc3VsdC5wdXNoKGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nKTtcbiAgICAgICAgfSBlbHNlIGlmICh2YWx1ZSA9PT0gJycpIHtcbiAgICAgICAgICAgIHJlc3VsdC5wdXNoKCcnKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiByZXN1bHQ7XG59XG5cbmZ1bmN0aW9uIGlzRGVmaW5lZCh2YWx1ZSkge1xuICAgIHJldHVybiB2YWx1ZSAhPT0gdW5kZWZpbmVkICYmIHZhbHVlICE9PSBudWxsO1xufVxuXG5mdW5jdGlvbiBpc0tleU9wZXJhdG9yKG9wZXJhdG9yKSB7XG4gICAgcmV0dXJuIG9wZXJhdG9yID09PSAnOycgfHwgb3BlcmF0b3IgPT09ICcmJyB8fCBvcGVyYXRvciA9PT0gJz8nO1xufVxuXG5mdW5jdGlvbiBlbmNvZGVWYWx1ZShvcGVyYXRvciwgdmFsdWUsIGtleSkge1xuXG4gICAgdmFsdWUgPSAob3BlcmF0b3IgPT09ICcrJyB8fCBvcGVyYXRvciA9PT0gJyMnKSA/IGVuY29kZVJlc2VydmVkKHZhbHVlKSA6IGVuY29kZVVSSUNvbXBvbmVudCh2YWx1ZSk7XG5cbiAgICBpZiAoa2V5KSB7XG4gICAgICAgIHJldHVybiBlbmNvZGVVUklDb21wb25lbnQoa2V5KSArICc9JyArIHZhbHVlO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGVuY29kZVJlc2VydmVkKHN0cikge1xuICAgIHJldHVybiBzdHIuc3BsaXQoLyglWzAtOUEtRmEtZl17Mn0pL2cpLm1hcChmdW5jdGlvbiAocGFydCkge1xuICAgICAgICBpZiAoIS8lWzAtOUEtRmEtZl0vLnRlc3QocGFydCkpIHtcbiAgICAgICAgICAgIHBhcnQgPSBlbmNvZGVVUkkocGFydCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHBhcnQ7XG4gICAgfSkuam9pbignJyk7XG59XG5cbi8qKlxuICogVVJMIFRlbXBsYXRlIChSRkMgNjU3MCkgVHJhbnNmb3JtLlxuICovXG5cbmZ1bmN0aW9uIHRlbXBsYXRlIChvcHRpb25zKSB7XG5cbiAgICB2YXIgdmFyaWFibGVzID0gW10sIHVybCA9IGV4cGFuZChvcHRpb25zLnVybCwgb3B0aW9ucy5wYXJhbXMsIHZhcmlhYmxlcyk7XG5cbiAgICB2YXJpYWJsZXMuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgIGRlbGV0ZSBvcHRpb25zLnBhcmFtc1trZXldO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHVybDtcbn1cblxuLyoqXG4gKiBTZXJ2aWNlIGZvciBVUkwgdGVtcGxhdGluZy5cbiAqL1xuXG5mdW5jdGlvbiBVcmwodXJsLCBwYXJhbXMpIHtcblxuICAgIHZhciBzZWxmID0gdGhpcyB8fCB7fSwgb3B0aW9ucyQkMSA9IHVybCwgdHJhbnNmb3JtO1xuXG4gICAgaWYgKGlzU3RyaW5nKHVybCkpIHtcbiAgICAgICAgb3B0aW9ucyQkMSA9IHt1cmw6IHVybCwgcGFyYW1zOiBwYXJhbXN9O1xuICAgIH1cblxuICAgIG9wdGlvbnMkJDEgPSBtZXJnZSh7fSwgVXJsLm9wdGlvbnMsIHNlbGYuJG9wdGlvbnMsIG9wdGlvbnMkJDEpO1xuXG4gICAgVXJsLnRyYW5zZm9ybXMuZm9yRWFjaChmdW5jdGlvbiAoaGFuZGxlcikge1xuXG4gICAgICAgIGlmIChpc1N0cmluZyhoYW5kbGVyKSkge1xuICAgICAgICAgICAgaGFuZGxlciA9IFVybC50cmFuc2Zvcm1baGFuZGxlcl07XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaXNGdW5jdGlvbihoYW5kbGVyKSkge1xuICAgICAgICAgICAgdHJhbnNmb3JtID0gZmFjdG9yeShoYW5kbGVyLCB0cmFuc2Zvcm0sIHNlbGYuJHZtKTtcbiAgICAgICAgfVxuXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdHJhbnNmb3JtKG9wdGlvbnMkJDEpO1xufVxuXG4vKipcbiAqIFVybCBvcHRpb25zLlxuICovXG5cblVybC5vcHRpb25zID0ge1xuICAgIHVybDogJycsXG4gICAgcm9vdDogbnVsbCxcbiAgICBwYXJhbXM6IHt9XG59O1xuXG4vKipcbiAqIFVybCB0cmFuc2Zvcm1zLlxuICovXG5cblVybC50cmFuc2Zvcm0gPSB7dGVtcGxhdGU6IHRlbXBsYXRlLCBxdWVyeTogcXVlcnksIHJvb3Q6IHJvb3R9O1xuVXJsLnRyYW5zZm9ybXMgPSBbJ3RlbXBsYXRlJywgJ3F1ZXJ5JywgJ3Jvb3QnXTtcblxuLyoqXG4gKiBFbmNvZGVzIGEgVXJsIHBhcmFtZXRlciBzdHJpbmcuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9ialxuICovXG5cblVybC5wYXJhbXMgPSBmdW5jdGlvbiAob2JqKSB7XG5cbiAgICB2YXIgcGFyYW1zID0gW10sIGVzY2FwZSA9IGVuY29kZVVSSUNvbXBvbmVudDtcblxuICAgIHBhcmFtcy5hZGQgPSBmdW5jdGlvbiAoa2V5LCB2YWx1ZSkge1xuXG4gICAgICAgIGlmIChpc0Z1bmN0aW9uKHZhbHVlKSkge1xuICAgICAgICAgICAgdmFsdWUgPSB2YWx1ZSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHZhbHVlID09PSBudWxsKSB7XG4gICAgICAgICAgICB2YWx1ZSA9ICcnO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5wdXNoKGVzY2FwZShrZXkpICsgJz0nICsgZXNjYXBlKHZhbHVlKSk7XG4gICAgfTtcblxuICAgIHNlcmlhbGl6ZShwYXJhbXMsIG9iaik7XG5cbiAgICByZXR1cm4gcGFyYW1zLmpvaW4oJyYnKS5yZXBsYWNlKC8lMjAvZywgJysnKTtcbn07XG5cbi8qKlxuICogUGFyc2UgYSBVUkwgYW5kIHJldHVybiBpdHMgY29tcG9uZW50cy5cbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gdXJsXG4gKi9cblxuVXJsLnBhcnNlID0gZnVuY3Rpb24gKHVybCkge1xuXG4gICAgdmFyIGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xuXG4gICAgaWYgKGRvY3VtZW50LmRvY3VtZW50TW9kZSkge1xuICAgICAgICBlbC5ocmVmID0gdXJsO1xuICAgICAgICB1cmwgPSBlbC5ocmVmO1xuICAgIH1cblxuICAgIGVsLmhyZWYgPSB1cmw7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBocmVmOiBlbC5ocmVmLFxuICAgICAgICBwcm90b2NvbDogZWwucHJvdG9jb2wgPyBlbC5wcm90b2NvbC5yZXBsYWNlKC86JC8sICcnKSA6ICcnLFxuICAgICAgICBwb3J0OiBlbC5wb3J0LFxuICAgICAgICBob3N0OiBlbC5ob3N0LFxuICAgICAgICBob3N0bmFtZTogZWwuaG9zdG5hbWUsXG4gICAgICAgIHBhdGhuYW1lOiBlbC5wYXRobmFtZS5jaGFyQXQoMCkgPT09ICcvJyA/IGVsLnBhdGhuYW1lIDogJy8nICsgZWwucGF0aG5hbWUsXG4gICAgICAgIHNlYXJjaDogZWwuc2VhcmNoID8gZWwuc2VhcmNoLnJlcGxhY2UoL15cXD8vLCAnJykgOiAnJyxcbiAgICAgICAgaGFzaDogZWwuaGFzaCA/IGVsLmhhc2gucmVwbGFjZSgvXiMvLCAnJykgOiAnJ1xuICAgIH07XG59O1xuXG5mdW5jdGlvbiBmYWN0b3J5KGhhbmRsZXIsIG5leHQsIHZtKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChvcHRpb25zJCQxKSB7XG4gICAgICAgIHJldHVybiBoYW5kbGVyLmNhbGwodm0sIG9wdGlvbnMkJDEsIG5leHQpO1xuICAgIH07XG59XG5cbmZ1bmN0aW9uIHNlcmlhbGl6ZShwYXJhbXMsIG9iaiwgc2NvcGUpIHtcblxuICAgIHZhciBhcnJheSA9IGlzQXJyYXkob2JqKSwgcGxhaW4gPSBpc1BsYWluT2JqZWN0KG9iaiksIGhhc2g7XG5cbiAgICBlYWNoKG9iaiwgZnVuY3Rpb24gKHZhbHVlLCBrZXkpIHtcblxuICAgICAgICBoYXNoID0gaXNPYmplY3QodmFsdWUpIHx8IGlzQXJyYXkodmFsdWUpO1xuXG4gICAgICAgIGlmIChzY29wZSkge1xuICAgICAgICAgICAga2V5ID0gc2NvcGUgKyAnWycgKyAocGxhaW4gfHwgaGFzaCA/IGtleSA6ICcnKSArICddJztcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghc2NvcGUgJiYgYXJyYXkpIHtcbiAgICAgICAgICAgIHBhcmFtcy5hZGQodmFsdWUubmFtZSwgdmFsdWUudmFsdWUpO1xuICAgICAgICB9IGVsc2UgaWYgKGhhc2gpIHtcbiAgICAgICAgICAgIHNlcmlhbGl6ZShwYXJhbXMsIHZhbHVlLCBrZXkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGFyYW1zLmFkZChrZXksIHZhbHVlKTtcbiAgICAgICAgfVxuICAgIH0pO1xufVxuXG4vKipcbiAqIFhEb21haW4gY2xpZW50IChJbnRlcm5ldCBFeHBsb3JlcikuXG4gKi9cblxuZnVuY3Rpb24geGRyQ2xpZW50IChyZXF1ZXN0KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG5cbiAgICAgICAgdmFyIHhkciA9IG5ldyBYRG9tYWluUmVxdWVzdCgpLCBoYW5kbGVyID0gZnVuY3Rpb24gKHJlZikge1xuICAgICAgICAgICAgICAgIHZhciB0eXBlID0gcmVmLnR5cGU7XG5cblxuICAgICAgICAgICAgICAgIHZhciBzdGF0dXMgPSAwO1xuXG4gICAgICAgICAgICAgICAgaWYgKHR5cGUgPT09ICdsb2FkJykge1xuICAgICAgICAgICAgICAgICAgICBzdGF0dXMgPSAyMDA7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlID09PSAnZXJyb3InKSB7XG4gICAgICAgICAgICAgICAgICAgIHN0YXR1cyA9IDUwMDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICByZXNvbHZlKHJlcXVlc3QucmVzcG9uZFdpdGgoeGRyLnJlc3BvbnNlVGV4dCwge3N0YXR1czogc3RhdHVzfSkpO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICByZXF1ZXN0LmFib3J0ID0gZnVuY3Rpb24gKCkgeyByZXR1cm4geGRyLmFib3J0KCk7IH07XG5cbiAgICAgICAgeGRyLm9wZW4ocmVxdWVzdC5tZXRob2QsIHJlcXVlc3QuZ2V0VXJsKCkpO1xuXG4gICAgICAgIGlmIChyZXF1ZXN0LnRpbWVvdXQpIHtcbiAgICAgICAgICAgIHhkci50aW1lb3V0ID0gcmVxdWVzdC50aW1lb3V0O1xuICAgICAgICB9XG5cbiAgICAgICAgeGRyLm9ubG9hZCA9IGhhbmRsZXI7XG4gICAgICAgIHhkci5vbmFib3J0ID0gaGFuZGxlcjtcbiAgICAgICAgeGRyLm9uZXJyb3IgPSBoYW5kbGVyO1xuICAgICAgICB4ZHIub250aW1lb3V0ID0gaGFuZGxlcjtcbiAgICAgICAgeGRyLm9ucHJvZ3Jlc3MgPSBmdW5jdGlvbiAoKSB7fTtcbiAgICAgICAgeGRyLnNlbmQocmVxdWVzdC5nZXRCb2R5KCkpO1xuICAgIH0pO1xufVxuXG4vKipcbiAqIENPUlMgSW50ZXJjZXB0b3IuXG4gKi9cblxudmFyIFNVUFBPUlRTX0NPUlMgPSBpbkJyb3dzZXIgJiYgJ3dpdGhDcmVkZW50aWFscycgaW4gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG5cbmZ1bmN0aW9uIGNvcnMgKHJlcXVlc3QpIHtcblxuICAgIGlmIChpbkJyb3dzZXIpIHtcblxuICAgICAgICB2YXIgb3JnVXJsID0gVXJsLnBhcnNlKGxvY2F0aW9uLmhyZWYpO1xuICAgICAgICB2YXIgcmVxVXJsID0gVXJsLnBhcnNlKHJlcXVlc3QuZ2V0VXJsKCkpO1xuXG4gICAgICAgIGlmIChyZXFVcmwucHJvdG9jb2wgIT09IG9yZ1VybC5wcm90b2NvbCB8fCByZXFVcmwuaG9zdCAhPT0gb3JnVXJsLmhvc3QpIHtcblxuICAgICAgICAgICAgcmVxdWVzdC5jcm9zc09yaWdpbiA9IHRydWU7XG4gICAgICAgICAgICByZXF1ZXN0LmVtdWxhdGVIVFRQID0gZmFsc2U7XG5cbiAgICAgICAgICAgIGlmICghU1VQUE9SVFNfQ09SUykge1xuICAgICAgICAgICAgICAgIHJlcXVlc3QuY2xpZW50ID0geGRyQ2xpZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG59XG5cbi8qKlxuICogRm9ybSBkYXRhIEludGVyY2VwdG9yLlxuICovXG5cbmZ1bmN0aW9uIGZvcm0gKHJlcXVlc3QpIHtcblxuICAgIGlmIChpc0Zvcm1EYXRhKHJlcXVlc3QuYm9keSkpIHtcbiAgICAgICAgcmVxdWVzdC5oZWFkZXJzLmRlbGV0ZSgnQ29udGVudC1UeXBlJyk7XG4gICAgfSBlbHNlIGlmIChpc09iamVjdChyZXF1ZXN0LmJvZHkpICYmIHJlcXVlc3QuZW11bGF0ZUpTT04pIHtcbiAgICAgICAgcmVxdWVzdC5ib2R5ID0gVXJsLnBhcmFtcyhyZXF1ZXN0LmJvZHkpO1xuICAgICAgICByZXF1ZXN0LmhlYWRlcnMuc2V0KCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJyk7XG4gICAgfVxuXG59XG5cbi8qKlxuICogSlNPTiBJbnRlcmNlcHRvci5cbiAqL1xuXG5mdW5jdGlvbiBqc29uIChyZXF1ZXN0KSB7XG5cbiAgICB2YXIgdHlwZSA9IHJlcXVlc3QuaGVhZGVycy5nZXQoJ0NvbnRlbnQtVHlwZScpIHx8ICcnO1xuXG4gICAgaWYgKGlzT2JqZWN0KHJlcXVlc3QuYm9keSkgJiYgdHlwZS5pbmRleE9mKCdhcHBsaWNhdGlvbi9qc29uJykgPT09IDApIHtcbiAgICAgICAgcmVxdWVzdC5ib2R5ID0gSlNPTi5zdHJpbmdpZnkocmVxdWVzdC5ib2R5KTtcbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG5cbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmJvZHlUZXh0ID8gd2hlbihyZXNwb25zZS50ZXh0KCksIGZ1bmN0aW9uICh0ZXh0KSB7XG5cbiAgICAgICAgICAgIHZhciB0eXBlID0gcmVzcG9uc2UuaGVhZGVycy5nZXQoJ0NvbnRlbnQtVHlwZScpIHx8ICcnO1xuXG4gICAgICAgICAgICBpZiAodHlwZS5pbmRleE9mKCdhcHBsaWNhdGlvbi9qc29uJykgPT09IDAgfHwgaXNKc29uKHRleHQpKSB7XG5cbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5ib2R5ID0gSlNPTi5wYXJzZSh0ZXh0KTtcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmJvZHkgPSBudWxsO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXNwb25zZS5ib2R5ID0gdGV4dDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuXG4gICAgICAgIH0pIDogcmVzcG9uc2U7XG5cbiAgICB9O1xufVxuXG5mdW5jdGlvbiBpc0pzb24oc3RyKSB7XG5cbiAgICB2YXIgc3RhcnQgPSBzdHIubWF0Y2goL15cXHMqKFxcW3xcXHspLyk7XG4gICAgdmFyIGVuZCA9IHsnWyc6IC9dXFxzKiQvLCAneyc6IC99XFxzKiQvfTtcblxuICAgIHJldHVybiBzdGFydCAmJiBlbmRbc3RhcnRbMV1dLnRlc3Qoc3RyKTtcbn1cblxuLyoqXG4gKiBKU09OUCBjbGllbnQgKEJyb3dzZXIpLlxuICovXG5cbmZ1bmN0aW9uIGpzb25wQ2xpZW50IChyZXF1ZXN0KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG5cbiAgICAgICAgdmFyIG5hbWUgPSByZXF1ZXN0Lmpzb25wIHx8ICdjYWxsYmFjaycsIGNhbGxiYWNrID0gcmVxdWVzdC5qc29ucENhbGxiYWNrIHx8ICdfanNvbnAnICsgTWF0aC5yYW5kb20oKS50b1N0cmluZygzNikuc3Vic3RyKDIpLCBib2R5ID0gbnVsbCwgaGFuZGxlciwgc2NyaXB0O1xuXG4gICAgICAgIGhhbmRsZXIgPSBmdW5jdGlvbiAocmVmKSB7XG4gICAgICAgICAgICB2YXIgdHlwZSA9IHJlZi50eXBlO1xuXG5cbiAgICAgICAgICAgIHZhciBzdGF0dXMgPSAwO1xuXG4gICAgICAgICAgICBpZiAodHlwZSA9PT0gJ2xvYWQnICYmIGJvZHkgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICBzdGF0dXMgPSAyMDA7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGUgPT09ICdlcnJvcicpIHtcbiAgICAgICAgICAgICAgICBzdGF0dXMgPSA1MDA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChzdGF0dXMgJiYgd2luZG93W2NhbGxiYWNrXSkge1xuICAgICAgICAgICAgICAgIGRlbGV0ZSB3aW5kb3dbY2FsbGJhY2tdO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQoc2NyaXB0KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmVzb2x2ZShyZXF1ZXN0LnJlc3BvbmRXaXRoKGJvZHksIHtzdGF0dXM6IHN0YXR1c30pKTtcbiAgICAgICAgfTtcblxuICAgICAgICB3aW5kb3dbY2FsbGJhY2tdID0gZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgICAgICAgYm9keSA9IEpTT04uc3RyaW5naWZ5KHJlc3VsdCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgcmVxdWVzdC5hYm9ydCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGhhbmRsZXIoe3R5cGU6ICdhYm9ydCd9KTtcbiAgICAgICAgfTtcblxuICAgICAgICByZXF1ZXN0LnBhcmFtc1tuYW1lXSA9IGNhbGxiYWNrO1xuXG4gICAgICAgIGlmIChyZXF1ZXN0LnRpbWVvdXQpIHtcbiAgICAgICAgICAgIHNldFRpbWVvdXQocmVxdWVzdC5hYm9ydCwgcmVxdWVzdC50aW1lb3V0KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHNjcmlwdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgICBzY3JpcHQuc3JjID0gcmVxdWVzdC5nZXRVcmwoKTtcbiAgICAgICAgc2NyaXB0LnR5cGUgPSAndGV4dC9qYXZhc2NyaXB0JztcbiAgICAgICAgc2NyaXB0LmFzeW5jID0gdHJ1ZTtcbiAgICAgICAgc2NyaXB0Lm9ubG9hZCA9IGhhbmRsZXI7XG4gICAgICAgIHNjcmlwdC5vbmVycm9yID0gaGFuZGxlcjtcblxuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcmlwdCk7XG4gICAgfSk7XG59XG5cbi8qKlxuICogSlNPTlAgSW50ZXJjZXB0b3IuXG4gKi9cblxuZnVuY3Rpb24ganNvbnAgKHJlcXVlc3QpIHtcblxuICAgIGlmIChyZXF1ZXN0Lm1ldGhvZCA9PSAnSlNPTlAnKSB7XG4gICAgICAgIHJlcXVlc3QuY2xpZW50ID0ganNvbnBDbGllbnQ7XG4gICAgfVxuXG59XG5cbi8qKlxuICogQmVmb3JlIEludGVyY2VwdG9yLlxuICovXG5cbmZ1bmN0aW9uIGJlZm9yZSAocmVxdWVzdCkge1xuXG4gICAgaWYgKGlzRnVuY3Rpb24ocmVxdWVzdC5iZWZvcmUpKSB7XG4gICAgICAgIHJlcXVlc3QuYmVmb3JlLmNhbGwodGhpcywgcmVxdWVzdCk7XG4gICAgfVxuXG59XG5cbi8qKlxuICogSFRUUCBtZXRob2Qgb3ZlcnJpZGUgSW50ZXJjZXB0b3IuXG4gKi9cblxuZnVuY3Rpb24gbWV0aG9kIChyZXF1ZXN0KSB7XG5cbiAgICBpZiAocmVxdWVzdC5lbXVsYXRlSFRUUCAmJiAvXihQVVR8UEFUQ0h8REVMRVRFKSQvaS50ZXN0KHJlcXVlc3QubWV0aG9kKSkge1xuICAgICAgICByZXF1ZXN0LmhlYWRlcnMuc2V0KCdYLUhUVFAtTWV0aG9kLU92ZXJyaWRlJywgcmVxdWVzdC5tZXRob2QpO1xuICAgICAgICByZXF1ZXN0Lm1ldGhvZCA9ICdQT1NUJztcbiAgICB9XG5cbn1cblxuLyoqXG4gKiBIZWFkZXIgSW50ZXJjZXB0b3IuXG4gKi9cblxuZnVuY3Rpb24gaGVhZGVyIChyZXF1ZXN0KSB7XG5cbiAgICB2YXIgaGVhZGVycyA9IGFzc2lnbih7fSwgSHR0cC5oZWFkZXJzLmNvbW1vbixcbiAgICAgICAgIXJlcXVlc3QuY3Jvc3NPcmlnaW4gPyBIdHRwLmhlYWRlcnMuY3VzdG9tIDoge30sXG4gICAgICAgIEh0dHAuaGVhZGVyc1t0b0xvd2VyKHJlcXVlc3QubWV0aG9kKV1cbiAgICApO1xuXG4gICAgZWFjaChoZWFkZXJzLCBmdW5jdGlvbiAodmFsdWUsIG5hbWUpIHtcbiAgICAgICAgaWYgKCFyZXF1ZXN0LmhlYWRlcnMuaGFzKG5hbWUpKSB7XG4gICAgICAgICAgICByZXF1ZXN0LmhlYWRlcnMuc2V0KG5hbWUsIHZhbHVlKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG59XG5cbi8qKlxuICogWE1MSHR0cCBjbGllbnQgKEJyb3dzZXIpLlxuICovXG5cbmZ1bmN0aW9uIHhockNsaWVudCAocmVxdWVzdCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZU9iaihmdW5jdGlvbiAocmVzb2x2ZSkge1xuXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKSwgaGFuZGxlciA9IGZ1bmN0aW9uIChldmVudCkge1xuXG4gICAgICAgICAgICAgICAgdmFyIHJlc3BvbnNlID0gcmVxdWVzdC5yZXNwb25kV2l0aChcbiAgICAgICAgICAgICAgICAncmVzcG9uc2UnIGluIHhociA/IHhoci5yZXNwb25zZSA6IHhoci5yZXNwb25zZVRleHQsIHtcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiB4aHIuc3RhdHVzID09PSAxMjIzID8gMjA0IDogeGhyLnN0YXR1cywgLy8gSUU5IHN0YXR1cyBidWdcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzVGV4dDogeGhyLnN0YXR1cyA9PT0gMTIyMyA/ICdObyBDb250ZW50JyA6IHRyaW0oeGhyLnN0YXR1c1RleHQpXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICBlYWNoKHRyaW0oeGhyLmdldEFsbFJlc3BvbnNlSGVhZGVycygpKS5zcGxpdCgnXFxuJyksIGZ1bmN0aW9uIChyb3cpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuaGVhZGVycy5hcHBlbmQocm93LnNsaWNlKDAsIHJvdy5pbmRleE9mKCc6JykpLCByb3cuc2xpY2Uocm93LmluZGV4T2YoJzonKSArIDEpKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICByZXF1ZXN0LmFib3J0ID0gZnVuY3Rpb24gKCkgeyByZXR1cm4geGhyLmFib3J0KCk7IH07XG5cbiAgICAgICAgeGhyLm9wZW4ocmVxdWVzdC5tZXRob2QsIHJlcXVlc3QuZ2V0VXJsKCksIHRydWUpO1xuXG4gICAgICAgIGlmIChyZXF1ZXN0LnRpbWVvdXQpIHtcbiAgICAgICAgICAgIHhoci50aW1lb3V0ID0gcmVxdWVzdC50aW1lb3V0O1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHJlcXVlc3QucmVzcG9uc2VUeXBlICYmICdyZXNwb25zZVR5cGUnIGluIHhocikge1xuICAgICAgICAgICAgeGhyLnJlc3BvbnNlVHlwZSA9IHJlcXVlc3QucmVzcG9uc2VUeXBlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHJlcXVlc3Qud2l0aENyZWRlbnRpYWxzIHx8IHJlcXVlc3QuY3JlZGVudGlhbHMpIHtcbiAgICAgICAgICAgIHhoci53aXRoQ3JlZGVudGlhbHMgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFyZXF1ZXN0LmNyb3NzT3JpZ2luKSB7XG4gICAgICAgICAgICByZXF1ZXN0LmhlYWRlcnMuc2V0KCdYLVJlcXVlc3RlZC1XaXRoJywgJ1hNTEh0dHBSZXF1ZXN0Jyk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBkZXByZWNhdGVkIHVzZSBkb3dubG9hZFByb2dyZXNzXG4gICAgICAgIGlmIChpc0Z1bmN0aW9uKHJlcXVlc3QucHJvZ3Jlc3MpICYmIHJlcXVlc3QubWV0aG9kID09PSAnR0VUJykge1xuICAgICAgICAgICAgeGhyLmFkZEV2ZW50TGlzdGVuZXIoJ3Byb2dyZXNzJywgcmVxdWVzdC5wcm9ncmVzcyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaXNGdW5jdGlvbihyZXF1ZXN0LmRvd25sb2FkUHJvZ3Jlc3MpKSB7XG4gICAgICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcigncHJvZ3Jlc3MnLCByZXF1ZXN0LmRvd25sb2FkUHJvZ3Jlc3MpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gZGVwcmVjYXRlZCB1c2UgdXBsb2FkUHJvZ3Jlc3NcbiAgICAgICAgaWYgKGlzRnVuY3Rpb24ocmVxdWVzdC5wcm9ncmVzcykgJiYgL14oUE9TVHxQVVQpJC9pLnRlc3QocmVxdWVzdC5tZXRob2QpKSB7XG4gICAgICAgICAgICB4aHIudXBsb2FkLmFkZEV2ZW50TGlzdGVuZXIoJ3Byb2dyZXNzJywgcmVxdWVzdC5wcm9ncmVzcyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaXNGdW5jdGlvbihyZXF1ZXN0LnVwbG9hZFByb2dyZXNzKSAmJiB4aHIudXBsb2FkKSB7XG4gICAgICAgICAgICB4aHIudXBsb2FkLmFkZEV2ZW50TGlzdGVuZXIoJ3Byb2dyZXNzJywgcmVxdWVzdC51cGxvYWRQcm9ncmVzcyk7XG4gICAgICAgIH1cblxuICAgICAgICByZXF1ZXN0LmhlYWRlcnMuZm9yRWFjaChmdW5jdGlvbiAodmFsdWUsIG5hbWUpIHtcbiAgICAgICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKG5hbWUsIHZhbHVlKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgeGhyLm9ubG9hZCA9IGhhbmRsZXI7XG4gICAgICAgIHhoci5vbmFib3J0ID0gaGFuZGxlcjtcbiAgICAgICAgeGhyLm9uZXJyb3IgPSBoYW5kbGVyO1xuICAgICAgICB4aHIub250aW1lb3V0ID0gaGFuZGxlcjtcbiAgICAgICAgeGhyLnNlbmQocmVxdWVzdC5nZXRCb2R5KCkpO1xuICAgIH0pO1xufVxuXG4vKipcbiAqIEh0dHAgY2xpZW50IChOb2RlKS5cbiAqL1xuXG5mdW5jdGlvbiBub2RlQ2xpZW50IChyZXF1ZXN0KSB7XG5cbiAgICB2YXIgY2xpZW50ID0gcmVxdWlyZSgnZ290Jyk7XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2VPYmooZnVuY3Rpb24gKHJlc29sdmUpIHtcblxuICAgICAgICB2YXIgdXJsID0gcmVxdWVzdC5nZXRVcmwoKTtcbiAgICAgICAgdmFyIGJvZHkgPSByZXF1ZXN0LmdldEJvZHkoKTtcbiAgICAgICAgdmFyIG1ldGhvZCA9IHJlcXVlc3QubWV0aG9kO1xuICAgICAgICB2YXIgaGVhZGVycyA9IHt9LCBoYW5kbGVyO1xuXG4gICAgICAgIHJlcXVlc3QuaGVhZGVycy5mb3JFYWNoKGZ1bmN0aW9uICh2YWx1ZSwgbmFtZSkge1xuICAgICAgICAgICAgaGVhZGVyc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICB9KTtcblxuICAgICAgICBjbGllbnQodXJsLCB7Ym9keTogYm9keSwgbWV0aG9kOiBtZXRob2QsIGhlYWRlcnM6IGhlYWRlcnN9KS50aGVuKGhhbmRsZXIgPSBmdW5jdGlvbiAocmVzcCkge1xuXG4gICAgICAgICAgICB2YXIgcmVzcG9uc2UgPSByZXF1ZXN0LnJlc3BvbmRXaXRoKHJlc3AuYm9keSwge1xuICAgICAgICAgICAgICAgIHN0YXR1czogcmVzcC5zdGF0dXNDb2RlLFxuICAgICAgICAgICAgICAgIHN0YXR1c1RleHQ6IHRyaW0ocmVzcC5zdGF0dXNNZXNzYWdlKVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGVhY2gocmVzcC5oZWFkZXJzLCBmdW5jdGlvbiAodmFsdWUsIG5hbWUpIHtcbiAgICAgICAgICAgICAgICByZXNwb25zZS5oZWFkZXJzLnNldChuYW1lLCB2YWx1ZSk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG5cbiAgICAgICAgfSwgZnVuY3Rpb24gKGVycm9yJCQxKSB7IHJldHVybiBoYW5kbGVyKGVycm9yJCQxLnJlc3BvbnNlKTsgfSk7XG4gICAgfSk7XG59XG5cbi8qKlxuICogQmFzZSBjbGllbnQuXG4gKi9cblxuZnVuY3Rpb24gQ2xpZW50IChjb250ZXh0KSB7XG5cbiAgICB2YXIgcmVxSGFuZGxlcnMgPSBbc2VuZFJlcXVlc3RdLCByZXNIYW5kbGVycyA9IFtdO1xuXG4gICAgaWYgKCFpc09iamVjdChjb250ZXh0KSkge1xuICAgICAgICBjb250ZXh0ID0gbnVsbDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBDbGllbnQocmVxdWVzdCkge1xuICAgICAgICB3aGlsZSAocmVxSGFuZGxlcnMubGVuZ3RoKSB7XG5cbiAgICAgICAgICAgIHZhciBoYW5kbGVyID0gcmVxSGFuZGxlcnMucG9wKCk7XG5cbiAgICAgICAgICAgIGlmIChpc0Z1bmN0aW9uKGhhbmRsZXIpKSB7XG5cbiAgICAgICAgICAgICAgICB2YXIgcmVzcG9uc2UgPSAodm9pZCAwKSwgbmV4dCA9ICh2b2lkIDApO1xuXG4gICAgICAgICAgICAgICAgcmVzcG9uc2UgPSBoYW5kbGVyLmNhbGwoY29udGV4dCwgcmVxdWVzdCwgZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gbmV4dCA9IHZhbDsgfSkgfHwgbmV4dDtcblxuICAgICAgICAgICAgICAgIGlmIChpc09iamVjdChyZXNwb25zZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzSGFuZGxlcnMuZm9yRWFjaChmdW5jdGlvbiAoaGFuZGxlcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlID0gd2hlbihyZXNwb25zZSwgZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBoYW5kbGVyLmNhbGwoY29udGV4dCwgcmVzcG9uc2UpIHx8IHJlc3BvbnNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHJlamVjdCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgd2hlbihyZXNwb25zZSwgcmVzb2x2ZSwgcmVqZWN0KTtcblxuICAgICAgICAgICAgICAgICAgICB9LCBjb250ZXh0KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoaXNGdW5jdGlvbihyZXNwb25zZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzSGFuZGxlcnMudW5zaGlmdChyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHdhcm4oKFwiSW52YWxpZCBpbnRlcmNlcHRvciBvZiB0eXBlIFwiICsgKHR5cGVvZiBoYW5kbGVyKSArIFwiLCBtdXN0IGJlIGEgZnVuY3Rpb25cIikpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgQ2xpZW50LnVzZSA9IGZ1bmN0aW9uIChoYW5kbGVyKSB7XG4gICAgICAgIHJlcUhhbmRsZXJzLnB1c2goaGFuZGxlcik7XG4gICAgfTtcblxuICAgIHJldHVybiBDbGllbnQ7XG59XG5cbmZ1bmN0aW9uIHNlbmRSZXF1ZXN0KHJlcXVlc3QpIHtcblxuICAgIHZhciBjbGllbnQgPSByZXF1ZXN0LmNsaWVudCB8fCAoaW5Ccm93c2VyID8geGhyQ2xpZW50IDogbm9kZUNsaWVudCk7XG5cbiAgICByZXR1cm4gY2xpZW50KHJlcXVlc3QpO1xufVxuXG4vKipcbiAqIEhUVFAgSGVhZGVycy5cbiAqL1xuXG52YXIgSGVhZGVycyA9IGZ1bmN0aW9uIEhlYWRlcnMoaGVhZGVycykge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG5cbiAgICB0aGlzLm1hcCA9IHt9O1xuXG4gICAgZWFjaChoZWFkZXJzLCBmdW5jdGlvbiAodmFsdWUsIG5hbWUpIHsgcmV0dXJuIHRoaXMkMS5hcHBlbmQobmFtZSwgdmFsdWUpOyB9KTtcbn07XG5cbkhlYWRlcnMucHJvdG90eXBlLmhhcyA9IGZ1bmN0aW9uIGhhcyAobmFtZSkge1xuICAgIHJldHVybiBnZXROYW1lKHRoaXMubWFwLCBuYW1lKSAhPT0gbnVsbDtcbn07XG5cbkhlYWRlcnMucHJvdG90eXBlLmdldCA9IGZ1bmN0aW9uIGdldCAobmFtZSkge1xuXG4gICAgdmFyIGxpc3QgPSB0aGlzLm1hcFtnZXROYW1lKHRoaXMubWFwLCBuYW1lKV07XG5cbiAgICByZXR1cm4gbGlzdCA/IGxpc3Quam9pbigpIDogbnVsbDtcbn07XG5cbkhlYWRlcnMucHJvdG90eXBlLmdldEFsbCA9IGZ1bmN0aW9uIGdldEFsbCAobmFtZSkge1xuICAgIHJldHVybiB0aGlzLm1hcFtnZXROYW1lKHRoaXMubWFwLCBuYW1lKV0gfHwgW107XG59O1xuXG5IZWFkZXJzLnByb3RvdHlwZS5zZXQgPSBmdW5jdGlvbiBzZXQgKG5hbWUsIHZhbHVlKSB7XG4gICAgdGhpcy5tYXBbbm9ybWFsaXplTmFtZShnZXROYW1lKHRoaXMubWFwLCBuYW1lKSB8fCBuYW1lKV0gPSBbdHJpbSh2YWx1ZSldO1xufTtcblxuSGVhZGVycy5wcm90b3R5cGUuYXBwZW5kID0gZnVuY3Rpb24gYXBwZW5kIChuYW1lLCB2YWx1ZSkge1xuXG4gICAgdmFyIGxpc3QgPSB0aGlzLm1hcFtnZXROYW1lKHRoaXMubWFwLCBuYW1lKV07XG5cbiAgICBpZiAobGlzdCkge1xuICAgICAgICBsaXN0LnB1c2godHJpbSh2YWx1ZSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc2V0KG5hbWUsIHZhbHVlKTtcbiAgICB9XG59O1xuXG5IZWFkZXJzLnByb3RvdHlwZS5kZWxldGUgPSBmdW5jdGlvbiBkZWxldGUkMSAobmFtZSkge1xuICAgIGRlbGV0ZSB0aGlzLm1hcFtnZXROYW1lKHRoaXMubWFwLCBuYW1lKV07XG59O1xuXG5IZWFkZXJzLnByb3RvdHlwZS5kZWxldGVBbGwgPSBmdW5jdGlvbiBkZWxldGVBbGwgKCkge1xuICAgIHRoaXMubWFwID0ge307XG59O1xuXG5IZWFkZXJzLnByb3RvdHlwZS5mb3JFYWNoID0gZnVuY3Rpb24gZm9yRWFjaCAoY2FsbGJhY2ssIHRoaXNBcmcpIHtcbiAgICAgICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICBlYWNoKHRoaXMubWFwLCBmdW5jdGlvbiAobGlzdCwgbmFtZSkge1xuICAgICAgICBlYWNoKGxpc3QsIGZ1bmN0aW9uICh2YWx1ZSkgeyByZXR1cm4gY2FsbGJhY2suY2FsbCh0aGlzQXJnLCB2YWx1ZSwgbmFtZSwgdGhpcyQxKTsgfSk7XG4gICAgfSk7XG59O1xuXG5mdW5jdGlvbiBnZXROYW1lKG1hcCwgbmFtZSkge1xuICAgIHJldHVybiBPYmplY3Qua2V5cyhtYXApLnJlZHVjZShmdW5jdGlvbiAocHJldiwgY3Vycikge1xuICAgICAgICByZXR1cm4gdG9Mb3dlcihuYW1lKSA9PT0gdG9Mb3dlcihjdXJyKSA/IGN1cnIgOiBwcmV2O1xuICAgIH0sIG51bGwpO1xufVxuXG5mdW5jdGlvbiBub3JtYWxpemVOYW1lKG5hbWUpIHtcblxuICAgIGlmICgvW15hLXowLTlcXC0jJCUmJyorLl5fYHx+XS9pLnRlc3QobmFtZSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignSW52YWxpZCBjaGFyYWN0ZXIgaW4gaGVhZGVyIGZpZWxkIG5hbWUnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdHJpbShuYW1lKTtcbn1cblxuLyoqXG4gKiBIVFRQIFJlc3BvbnNlLlxuICovXG5cbnZhciBSZXNwb25zZSA9IGZ1bmN0aW9uIFJlc3BvbnNlKGJvZHksIHJlZikge1xuICAgIHZhciB1cmwgPSByZWYudXJsO1xuICAgIHZhciBoZWFkZXJzID0gcmVmLmhlYWRlcnM7XG4gICAgdmFyIHN0YXR1cyA9IHJlZi5zdGF0dXM7XG4gICAgdmFyIHN0YXR1c1RleHQgPSByZWYuc3RhdHVzVGV4dDtcblxuXG4gICAgdGhpcy51cmwgPSB1cmw7XG4gICAgdGhpcy5vayA9IHN0YXR1cyA+PSAyMDAgJiYgc3RhdHVzIDwgMzAwO1xuICAgIHRoaXMuc3RhdHVzID0gc3RhdHVzIHx8IDA7XG4gICAgdGhpcy5zdGF0dXNUZXh0ID0gc3RhdHVzVGV4dCB8fCAnJztcbiAgICB0aGlzLmhlYWRlcnMgPSBuZXcgSGVhZGVycyhoZWFkZXJzKTtcbiAgICB0aGlzLmJvZHkgPSBib2R5O1xuXG4gICAgaWYgKGlzU3RyaW5nKGJvZHkpKSB7XG5cbiAgICAgICAgdGhpcy5ib2R5VGV4dCA9IGJvZHk7XG5cbiAgICB9IGVsc2UgaWYgKGlzQmxvYihib2R5KSkge1xuXG4gICAgICAgIHRoaXMuYm9keUJsb2IgPSBib2R5O1xuXG4gICAgICAgIGlmIChpc0Jsb2JUZXh0KGJvZHkpKSB7XG4gICAgICAgICAgICB0aGlzLmJvZHlUZXh0ID0gYmxvYlRleHQoYm9keSk7XG4gICAgICAgIH1cbiAgICB9XG59O1xuXG5SZXNwb25zZS5wcm90b3R5cGUuYmxvYiA9IGZ1bmN0aW9uIGJsb2IgKCkge1xuICAgIHJldHVybiB3aGVuKHRoaXMuYm9keUJsb2IpO1xufTtcblxuUmVzcG9uc2UucHJvdG90eXBlLnRleHQgPSBmdW5jdGlvbiB0ZXh0ICgpIHtcbiAgICByZXR1cm4gd2hlbih0aGlzLmJvZHlUZXh0KTtcbn07XG5cblJlc3BvbnNlLnByb3RvdHlwZS5qc29uID0gZnVuY3Rpb24ganNvbiAoKSB7XG4gICAgcmV0dXJuIHdoZW4odGhpcy50ZXh0KCksIGZ1bmN0aW9uICh0ZXh0KSB7IHJldHVybiBKU09OLnBhcnNlKHRleHQpOyB9KTtcbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShSZXNwb25zZS5wcm90b3R5cGUsICdkYXRhJywge1xuXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmJvZHk7XG4gICAgfSxcblxuICAgIHNldDogZnVuY3Rpb24gc2V0KGJvZHkpIHtcbiAgICAgICAgdGhpcy5ib2R5ID0gYm9keTtcbiAgICB9XG5cbn0pO1xuXG5mdW5jdGlvbiBibG9iVGV4dChib2R5KSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlT2JqKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG5cbiAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG5cbiAgICAgICAgcmVhZGVyLnJlYWRBc1RleHQoYm9keSk7XG4gICAgICAgIHJlYWRlci5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXNvbHZlKHJlYWRlci5yZXN1bHQpO1xuICAgICAgICB9O1xuXG4gICAgfSk7XG59XG5cbmZ1bmN0aW9uIGlzQmxvYlRleHQoYm9keSkge1xuICAgIHJldHVybiBib2R5LnR5cGUuaW5kZXhPZigndGV4dCcpID09PSAwIHx8IGJvZHkudHlwZS5pbmRleE9mKCdqc29uJykgIT09IC0xO1xufVxuXG4vKipcbiAqIEhUVFAgUmVxdWVzdC5cbiAqL1xuXG52YXIgUmVxdWVzdCA9IGZ1bmN0aW9uIFJlcXVlc3Qob3B0aW9ucyQkMSkge1xuXG4gICAgdGhpcy5ib2R5ID0gbnVsbDtcbiAgICB0aGlzLnBhcmFtcyA9IHt9O1xuXG4gICAgYXNzaWduKHRoaXMsIG9wdGlvbnMkJDEsIHtcbiAgICAgICAgbWV0aG9kOiB0b1VwcGVyKG9wdGlvbnMkJDEubWV0aG9kIHx8ICdHRVQnKVxuICAgIH0pO1xuXG4gICAgaWYgKCEodGhpcy5oZWFkZXJzIGluc3RhbmNlb2YgSGVhZGVycykpIHtcbiAgICAgICAgdGhpcy5oZWFkZXJzID0gbmV3IEhlYWRlcnModGhpcy5oZWFkZXJzKTtcbiAgICB9XG59O1xuXG5SZXF1ZXN0LnByb3RvdHlwZS5nZXRVcmwgPSBmdW5jdGlvbiBnZXRVcmwgKCkge1xuICAgIHJldHVybiBVcmwodGhpcyk7XG59O1xuXG5SZXF1ZXN0LnByb3RvdHlwZS5nZXRCb2R5ID0gZnVuY3Rpb24gZ2V0Qm9keSAoKSB7XG4gICAgcmV0dXJuIHRoaXMuYm9keTtcbn07XG5cblJlcXVlc3QucHJvdG90eXBlLnJlc3BvbmRXaXRoID0gZnVuY3Rpb24gcmVzcG9uZFdpdGggKGJvZHksIG9wdGlvbnMkJDEpIHtcbiAgICByZXR1cm4gbmV3IFJlc3BvbnNlKGJvZHksIGFzc2lnbihvcHRpb25zJCQxIHx8IHt9LCB7dXJsOiB0aGlzLmdldFVybCgpfSkpO1xufTtcblxuLyoqXG4gKiBTZXJ2aWNlIGZvciBzZW5kaW5nIG5ldHdvcmsgcmVxdWVzdHMuXG4gKi9cblxudmFyIENPTU1PTl9IRUFERVJTID0geydBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbiwgdGV4dC9wbGFpbiwgKi8qJ307XG52YXIgSlNPTl9DT05URU5UX1RZUEUgPSB7J0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uO2NoYXJzZXQ9dXRmLTgnfTtcblxuZnVuY3Rpb24gSHR0cChvcHRpb25zJCQxKSB7XG5cbiAgICB2YXIgc2VsZiA9IHRoaXMgfHwge30sIGNsaWVudCA9IENsaWVudChzZWxmLiR2bSk7XG5cbiAgICBkZWZhdWx0cyhvcHRpb25zJCQxIHx8IHt9LCBzZWxmLiRvcHRpb25zLCBIdHRwLm9wdGlvbnMpO1xuXG4gICAgSHR0cC5pbnRlcmNlcHRvcnMuZm9yRWFjaChmdW5jdGlvbiAoaGFuZGxlcikge1xuXG4gICAgICAgIGlmIChpc1N0cmluZyhoYW5kbGVyKSkge1xuICAgICAgICAgICAgaGFuZGxlciA9IEh0dHAuaW50ZXJjZXB0b3JbaGFuZGxlcl07XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaXNGdW5jdGlvbihoYW5kbGVyKSkge1xuICAgICAgICAgICAgY2xpZW50LnVzZShoYW5kbGVyKTtcbiAgICAgICAgfVxuXG4gICAgfSk7XG5cbiAgICByZXR1cm4gY2xpZW50KG5ldyBSZXF1ZXN0KG9wdGlvbnMkJDEpKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuXG4gICAgICAgIHJldHVybiByZXNwb25zZS5vayA/IHJlc3BvbnNlIDogUHJvbWlzZU9iai5yZWplY3QocmVzcG9uc2UpO1xuXG4gICAgfSwgZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG5cbiAgICAgICAgaWYgKHJlc3BvbnNlIGluc3RhbmNlb2YgRXJyb3IpIHtcbiAgICAgICAgICAgIGVycm9yKHJlc3BvbnNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBQcm9taXNlT2JqLnJlamVjdChyZXNwb25zZSk7XG4gICAgfSk7XG59XG5cbkh0dHAub3B0aW9ucyA9IHt9O1xuXG5IdHRwLmhlYWRlcnMgPSB7XG4gICAgcHV0OiBKU09OX0NPTlRFTlRfVFlQRSxcbiAgICBwb3N0OiBKU09OX0NPTlRFTlRfVFlQRSxcbiAgICBwYXRjaDogSlNPTl9DT05URU5UX1RZUEUsXG4gICAgZGVsZXRlOiBKU09OX0NPTlRFTlRfVFlQRSxcbiAgICBjb21tb246IENPTU1PTl9IRUFERVJTLFxuICAgIGN1c3RvbToge31cbn07XG5cbkh0dHAuaW50ZXJjZXB0b3IgPSB7YmVmb3JlOiBiZWZvcmUsIG1ldGhvZDogbWV0aG9kLCBqc29ucDoganNvbnAsIGpzb246IGpzb24sIGZvcm06IGZvcm0sIGhlYWRlcjogaGVhZGVyLCBjb3JzOiBjb3JzfTtcbkh0dHAuaW50ZXJjZXB0b3JzID0gWydiZWZvcmUnLCAnbWV0aG9kJywgJ2pzb25wJywgJ2pzb24nLCAnZm9ybScsICdoZWFkZXInLCAnY29ycyddO1xuXG5bJ2dldCcsICdkZWxldGUnLCAnaGVhZCcsICdqc29ucCddLmZvckVhY2goZnVuY3Rpb24gKG1ldGhvZCQkMSkge1xuXG4gICAgSHR0cFttZXRob2QkJDFdID0gZnVuY3Rpb24gKHVybCwgb3B0aW9ucyQkMSkge1xuICAgICAgICByZXR1cm4gdGhpcyhhc3NpZ24ob3B0aW9ucyQkMSB8fCB7fSwge3VybDogdXJsLCBtZXRob2Q6IG1ldGhvZCQkMX0pKTtcbiAgICB9O1xuXG59KTtcblxuWydwb3N0JywgJ3B1dCcsICdwYXRjaCddLmZvckVhY2goZnVuY3Rpb24gKG1ldGhvZCQkMSkge1xuXG4gICAgSHR0cFttZXRob2QkJDFdID0gZnVuY3Rpb24gKHVybCwgYm9keSwgb3B0aW9ucyQkMSkge1xuICAgICAgICByZXR1cm4gdGhpcyhhc3NpZ24ob3B0aW9ucyQkMSB8fCB7fSwge3VybDogdXJsLCBtZXRob2Q6IG1ldGhvZCQkMSwgYm9keTogYm9keX0pKTtcbiAgICB9O1xuXG59KTtcblxuLyoqXG4gKiBTZXJ2aWNlIGZvciBpbnRlcmFjdGluZyB3aXRoIFJFU1RmdWwgc2VydmljZXMuXG4gKi9cblxuZnVuY3Rpb24gUmVzb3VyY2UodXJsLCBwYXJhbXMsIGFjdGlvbnMsIG9wdGlvbnMkJDEpIHtcblxuICAgIHZhciBzZWxmID0gdGhpcyB8fCB7fSwgcmVzb3VyY2UgPSB7fTtcblxuICAgIGFjdGlvbnMgPSBhc3NpZ24oe30sXG4gICAgICAgIFJlc291cmNlLmFjdGlvbnMsXG4gICAgICAgIGFjdGlvbnNcbiAgICApO1xuXG4gICAgZWFjaChhY3Rpb25zLCBmdW5jdGlvbiAoYWN0aW9uLCBuYW1lKSB7XG5cbiAgICAgICAgYWN0aW9uID0gbWVyZ2Uoe3VybDogdXJsLCBwYXJhbXM6IGFzc2lnbih7fSwgcGFyYW1zKX0sIG9wdGlvbnMkJDEsIGFjdGlvbik7XG5cbiAgICAgICAgcmVzb3VyY2VbbmFtZV0gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gKHNlbGYuJGh0dHAgfHwgSHR0cCkob3B0cyhhY3Rpb24sIGFyZ3VtZW50cykpO1xuICAgICAgICB9O1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHJlc291cmNlO1xufVxuXG5mdW5jdGlvbiBvcHRzKGFjdGlvbiwgYXJncykge1xuXG4gICAgdmFyIG9wdGlvbnMkJDEgPSBhc3NpZ24oe30sIGFjdGlvbiksIHBhcmFtcyA9IHt9LCBib2R5O1xuXG4gICAgc3dpdGNoIChhcmdzLmxlbmd0aCkge1xuXG4gICAgICAgIGNhc2UgMjpcblxuICAgICAgICAgICAgcGFyYW1zID0gYXJnc1swXTtcbiAgICAgICAgICAgIGJvZHkgPSBhcmdzWzFdO1xuXG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIDE6XG5cbiAgICAgICAgICAgIGlmICgvXihQT1NUfFBVVHxQQVRDSCkkL2kudGVzdChvcHRpb25zJCQxLm1ldGhvZCkpIHtcbiAgICAgICAgICAgICAgICBib2R5ID0gYXJnc1swXTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcGFyYW1zID0gYXJnc1swXTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAwOlxuXG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICBkZWZhdWx0OlxuXG4gICAgICAgICAgICB0aHJvdyAnRXhwZWN0ZWQgdXAgdG8gMiBhcmd1bWVudHMgW3BhcmFtcywgYm9keV0sIGdvdCAnICsgYXJncy5sZW5ndGggKyAnIGFyZ3VtZW50cyc7XG4gICAgfVxuXG4gICAgb3B0aW9ucyQkMS5ib2R5ID0gYm9keTtcbiAgICBvcHRpb25zJCQxLnBhcmFtcyA9IGFzc2lnbih7fSwgb3B0aW9ucyQkMS5wYXJhbXMsIHBhcmFtcyk7XG5cbiAgICByZXR1cm4gb3B0aW9ucyQkMTtcbn1cblxuUmVzb3VyY2UuYWN0aW9ucyA9IHtcblxuICAgIGdldDoge21ldGhvZDogJ0dFVCd9LFxuICAgIHNhdmU6IHttZXRob2Q6ICdQT1NUJ30sXG4gICAgcXVlcnk6IHttZXRob2Q6ICdHRVQnfSxcbiAgICB1cGRhdGU6IHttZXRob2Q6ICdQVVQnfSxcbiAgICByZW1vdmU6IHttZXRob2Q6ICdERUxFVEUnfSxcbiAgICBkZWxldGU6IHttZXRob2Q6ICdERUxFVEUnfVxuXG59O1xuXG4vKipcbiAqIEluc3RhbGwgcGx1Z2luLlxuICovXG5cbmZ1bmN0aW9uIHBsdWdpbihWdWUpIHtcblxuICAgIGlmIChwbHVnaW4uaW5zdGFsbGVkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBVdGlsKFZ1ZSk7XG5cbiAgICBWdWUudXJsID0gVXJsO1xuICAgIFZ1ZS5odHRwID0gSHR0cDtcbiAgICBWdWUucmVzb3VyY2UgPSBSZXNvdXJjZTtcbiAgICBWdWUuUHJvbWlzZSA9IFByb21pc2VPYmo7XG5cbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydGllcyhWdWUucHJvdG90eXBlLCB7XG5cbiAgICAgICAgJHVybDoge1xuICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG9wdGlvbnMoVnVlLnVybCwgdGhpcywgdGhpcy4kb3B0aW9ucy51cmwpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgICRodHRwOiB7XG4gICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gb3B0aW9ucyhWdWUuaHR0cCwgdGhpcywgdGhpcy4kb3B0aW9ucy5odHRwKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAkcmVzb3VyY2U6IHtcbiAgICAgICAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBWdWUucmVzb3VyY2UuYmluZCh0aGlzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAkcHJvbWlzZToge1xuICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgICAgICAgICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGV4ZWN1dG9yKSB7IHJldHVybiBuZXcgVnVlLlByb21pc2UoZXhlY3V0b3IsIHRoaXMkMSk7IH07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgIH0pO1xufVxuXG5pZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LlZ1ZSkge1xuICAgIHdpbmRvdy5WdWUudXNlKHBsdWdpbik7XG59XG5cbmV4cG9ydCBkZWZhdWx0IHBsdWdpbjtcbmV4cG9ydCB7IFVybCwgSHR0cCwgUmVzb3VyY2UgfTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtcmVzb3VyY2UvZGlzdC92dWUtcmVzb3VyY2UuZXNtLmpzXG4vLyBtb2R1bGUgaWQgPSAyMFxuLy8gbW9kdWxlIGNodW5rcyA9IDggMTEgMTYiLCIvKiAoaWdub3JlZCkgKi9cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyBnb3QgKGlnbm9yZWQpXG4vLyBtb2R1bGUgaWQgPSAyM1xuLy8gbW9kdWxlIGNodW5rcyA9IDggMTEgMTYiLCI8dGVtcGxhdGU+XHJcblxyXG4gICAgICAgICAgICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLWhvdmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8dGggdi1mb3I9XCJjb2x1bW4gaW4gY29sdW1uc1wiIDprZXk9XCJjb2x1bW4udGl0bGVcIiBAY2xpY2s9XCIkZW1pdCgnc29ydCcsIGNvbHVtbi50aXRsZSlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgOmNsYXNzPVwic29ydEtleSA9PT0gY29sdW1uLnRpdGxlID8gKHNvcnRPcmRlcnNbY29sdW1uLnRpdGxlXSA+IDAgPyAnc29ydGluZ19hc2MnIDogJ3NvcnRpbmdfZGVzYycpIDogJ3NvcnRpbmcnXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpzdHlsZT1cIid3aWR0aDonK2NvbHVtbi53aWR0aCsnOycrJ2N1cnNvcjpwb2ludGVyOydcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7Y29sdW1uLmxhYmVsfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC90aD5cclxuICAgICAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPC90aGVhZD5cclxuICAgICAgICAgICAgICAgIDxzbG90Pjwvc2xvdD5cclxuICAgICAgICAgICAgICA8L3RhYmxlPlxyXG5cclxuPC90ZW1wbGF0ZT5cclxuPHNjcmlwdD5cclxuICBleHBvcnQgZGVmYXVsdHtcclxuICAgIHByb3BzOiBbJ2NvbHVtbnMnLCdzb3J0S2V5Jywnc29ydE9yZGVycyddXHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIEFsYnVtTGlzdC52dWU/MDk2MTA3NzgiLCI8dGVtcGxhdGU+XHJcbiAgPGRpdiA+XHJcbiAgPG5hdiBjbGFzcz1cInBhZ2luYXRpb25cIiB2LWlmPVwiIWNsaWVudFwiPlxyXG4gICAgICAgIDxzcGFuIGNsYXNzPVwicGFnZS1zdGF0c1wiPnt7cGFnaW5hdGlvbi5mcm9tfX0gLSB7e3BhZ2luYXRpb24udG99fSBvZiB7e3BhZ2luYXRpb24udG90YWx9fTwvc3Bhbj5cclxuICAgICAgICA8YSB2LWlmPVwicGFnaW5hdGlvbi5wcmV2UGFnZVVybFwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tcHJldmlvdXNcIiBAY2xpY2s9XCIkZW1pdCgncHJldicpO1wiPlxyXG4gICAgICAgICAgICBQcmV2XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICAgIDxhIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tcHJldmlvdXNcIiB2LWVsc2UgOmRpc2FibGVkPVwidHJ1ZVwiPlxyXG4gICAgICAgICAgIFByZXZcclxuICAgICAgICA8L2E+XHJcblxyXG4gICAgICAgIDxhIHYtaWY9XCJwYWdpbmF0aW9uLm5leHRQYWdlVXJsXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1uZXh0XCIgQGNsaWNrPVwiJGVtaXQoJ25leHQnKTtcIj5cclxuICAgICAgICAgICAgTmV4dFxyXG4gICAgICAgIDwvYT5cclxuICAgICAgICA8YSBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLW5leHRcIiB2LWVsc2UgOmRpc2FibGVkPVwidHJ1ZVwiPlxyXG4gICAgICAgICAgICBOZXh0XHJcbiAgICAgICAgPC9hPlxyXG4gICAgPC9uYXY+XHJcblxyXG4gICAgPG5hdiBjbGFzcz1cInBhZ2luYXRpb25cIiB2LWVsc2U+XHJcbiAgICAgICAgPHNwYW4gY2xhc3M9XCJwYWdlLXN0YXRzXCI+XHJcbiAgICAgICAgICAgIHt7cGFnaW5hdGlvbi5mcm9tfX0gLSB7e3BhZ2luYXRpb24udG99fSBvZiB7e2ZpbHRlcmVkLmxlbmd0aH19XHJcbiAgICAgICAgICAgIDxzcGFuIHYtaWY9XCJmaWx0ZXJlZC5sZW5ndGggPCBwYWdpbmF0aW9uLnRvdGFsXCI+KGZpbHRlcmVkIGZyb20ge3twYWdpbmF0aW9uLnRvdGFsfX0gdG90YWwgZW50cmllcyk8L3NwYW4+XHJcbiAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgIDxhIHYtaWY9XCJwYWdpbmF0aW9uLnByZXZQYWdlXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1wcmV2aW91c1wiIEBjbGljaz1cIiRlbWl0KCdwcmV2Jyk7XCI+XHJcbiAgICAgICAgICAgIFByZXZcclxuICAgICAgICA8L2E+XHJcbiAgICAgICAgPGEgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1wcmV2aW91c1wiIHYtZWxzZSA6ZGlzYWJsZWQ9XCJ0cnVlXCI+XHJcbiAgICAgICAgICAgUHJldlxyXG4gICAgICAgIDwvYT5cclxuXHJcbiAgICAgICAgPGEgdi1pZj1cInBhZ2luYXRpb24ubmV4dFBhZ2VcIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLW5leHRcIiBAY2xpY2s9XCIkZW1pdCgnbmV4dCcpO1wiPlxyXG4gICAgICAgICAgICBOZXh0XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICAgIDxhIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tbmV4dFwiIHYtZWxzZSA6ZGlzYWJsZWQ9XCJ0cnVlXCI+XHJcbiAgICAgICAgICAgIE5leHRcclxuICAgICAgICA8L2E+XHJcbiAgICA8L25hdj5cclxuICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHR7XHJcbiAgcHJvcHM6IFsncGFnaW5hdGlvbicsICdjbGllbnQnLCAnZmlsdGVyZWQnXVxyXG59XHJcbjwvc2NyaXB0PlxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gUGFnaW5hdGlvbi52dWU/NGJkMzMyYWUiLCI8dGVtcGxhdGU+XHJcblxyXG4gIDxkaXY+XHJcbiAgICA8ZGl2IGlkPVwiZGlhbG9nQWRkQWxidW1cIiBjbGFzcz1cIm1vZGFsIGZhZGVcIiBhcmlhLWhpZGRlbj1cInRydWVcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZGlhbG9nXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMlwiPjxoMyBjbGFzcz1cIm0tdC1ub25lIG0tYlwiPkFkZCBuZXcgYWxidW08L2gzPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIHJvbGU9XCJmb3JtXCIgYWN0aW9uPVwiI1wiIG1ldGhvZD1cIlBPU1RcIiB2LW9uOnN1Ym1pdC5wcmV2ZW50PVwic2F2ZUFsYnVtKGFjdCwnJylcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgc3R5bGU9XCJjb2xvcjpyZWRcIiB2LWlmPVwiZXJyb3JcIj5UaXRsZSBjYW5ub3QgZHVwbGljYXRlIG9yIGVtcHR5PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBzdHlsZT1cImNvbG9yOnJlZFwiIHYtaWY9XCJlcnJvcjJcIj5BbGJ1bSBhbHJlYWR5IGV4aXN0PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+PGlucHV0ICB2LW1vZGVsPVwiYWxidW0udGl0bGVcIiBuYW1lPVwidGl0bGVcIiBpZD1cInRpdGxlXCIgdHlwZT1cInRleHRcIiBwbGFjZWhvbGRlcj1cIlRpdGxlXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIj48L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS02IGNvbC1zbS1vZmZzZXQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIgdHlwZT1cImJ1dHRvblwiIHYtaWY9XCJhY3Q9PSdBZGQnXCIgQGNsaWNrPVwic2F2ZUFsYnVtKGFjdCwnJylcIj5TYXZlIENoYW5nZXM8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFwiIHR5cGU9XCJidXR0b25cIiB2LWVsc2UgQGNsaWNrPVwic2F2ZUFsYnVtKGFjdCxhbGJ1bS5pZClcIj5TYXZlIENoYW5nZXM8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLXdoaXRlIHB1bGwtcmlnaHRcIiB0eXBlPVwiYnV0dG9uXCIgQGNsaWNrPVwicmVzZXRBbGwoKVwiPkNhbmNlbDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG48L2Rpdj5cclxuXHJcbiAgICA8ZGl2IGNsYXNzPVwicm93XCIgPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbGctMTJcIj5cclxuICAgICAgICAgICAgPGEgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLXNtIHB1bGwtcmlnaHRcIiAgQGNsaWNrPVwiYWN0aW9uRG9jKCdhZGQnLCAnJylcIiA+PGkgY2xhc3M9XCJmYSBmYS1wbHVzIGZhLWZ3XCI+PC9pPkFkZCBOZXcgQWxidW08L2E+XHJcbiAgICAgICAgICAgIDxkaXYgc3R5bGU9XCJoZWlnaHQ6MTBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cclxuXHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWJveCBmbG9hdC1lLW1hcmdpbnNcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpYm94LXRpdGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGgzPkxpc3Qgb2YgQWxidW1zPC9oMz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlib3gtY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDwhLS0gU0hPVyBJRiBET0NVTUVOVFMgQVJFIEVNUFRZXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm5vLXJlc3VsdFwiIHYtaWY9XCJwcm9qZWN0cy5sZW5ndGggPT0gMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDM+Tm8gYWxidW1zIHNob3cuPC9oMz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgLS0+XHJcbiAgICAgICAgICAgICAgICAgICAgPCEtLSBISURFIFRBQkxFIElGIEVNUFRZIC0tPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0YWJsZS1yZXNwb25zaXZlXCIgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpYm94IGZsb2F0LWUtbWFyZ2luc1wiPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlib3gtY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xIG0tYi14c1wiPjxzZWxlY3QgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2wgaW5wdXQtcy1zbSBpbmxpbmVcIiB2LW1vZGVsPVwidGFibGVEYXRhLmxlbmd0aFwiID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHYtZm9yPVwiKHJlY29yZHMsIGluZGV4KSBpbiBwZXJQYWdlXCIgOmtleT1cImluZGV4XCIgOnZhbHVlPVwicmVjb3Jkc1wiPnt7cmVjb3Jkc319PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tb2Zmc2V0LTcgY29sLXNtLTRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBwdWxsLXJpZ2h0XCI+PGlucHV0IHYtbW9kZWw9XCJ0YWJsZURhdGEuc2VhcmNoXCIgQGlucHV0PVwiZ2V0QWxidW1zKClcIiB0eXBlPVwidGV4dFwiIHBsYWNlaG9sZGVyPVwiU2VhcmNoXCIgY2xhc3M9XCJpbnB1dC1zbSBmb3JtLWNvbnRyb2xcIj4gPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBbGJ1bVRhYmxlIDpjb2x1bW5zPVwiY29sdW1uc1wiIDpzb3J0S2V5PVwic29ydEtleVwiIDpzb3J0T3JkZXJzPVwic29ydE9yZGVyc1wiIEBzb3J0PVwic29ydEJ5XCIgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGJvZHkgdi1pZj1cInByb2plY3RzLmxlbmd0aCAhPSAwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHIgdi1mb3I9XCJwcm9qZWN0IGluIHByb2plY3RzXCIgOmtleT1cInByb2plY3QuaWRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+PGEgOmhyZWY9XCJ1cmwrcHJvamVjdC5pZFwiPnt7cHJvamVjdC50aXRsZX19PC9hPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRhbmdlciBidG4teHMgcHVsbC1yaWdodFwiIEBjbGljaz1cImRlbGV0ZUFsYnVtKHByb2plY3QuaWQpXCI+PHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLXRyYXNoXCI+PC9zcGFuPjwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBidG4teHMgcHVsbC1yaWdodFwiIEBjbGljaz1cImFjdGlvbkRvYygnZWRpdCcsIHByb2plY3QuaWQpXCI+PHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLXBlbmNpbFwiPjwvc3Bhbj48L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPCEtLTxhIHRhcmdldD1cIl9ibGFua1wiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi14cyBwdWxsLXJpZ2h0XCIgOmhyZWY9XCJ1cmwrcHJvamVjdC5pZFwiPjxzcGFuIGNsYXNzPVwiZ2x5cGhpY29uIGdseXBoaWNvbi10aFwiPjwvc3Bhbj48L2E+LS0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90Ym9keT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRib2R5ICB2LWVsc2U+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE5vIHJlY29yZCBmb3VuZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0FsYnVtVGFibGU+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cGFnaW5hdGlvbiA6cGFnaW5hdGlvbj1cInBhZ2luYXRpb25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBAcHJldj1cImdldFByb2plY3RzKHBhZ2luYXRpb24ucHJldlBhZ2VVcmwpXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQG5leHQ9XCJnZXRQcm9qZWN0cyhwYWdpbmF0aW9uLm5leHRQYWdlVXJsKVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3BhZ2luYXRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+PCEtLSBlbmQgdGFibGUtcmVzcG9uc2l2ZSAtLT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG5cclxuXHJcbiAgPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG4gIGltcG9ydCBBbGJ1bVRhYmxlIGZyb20gJy4vQWxidW1MaXN0LnZ1ZSc7XHJcbiAgaW1wb3J0IFBhZ2luYXRpb24gZnJvbSAnLi9QYWdpbmF0aW9uLnZ1ZSc7XHJcblxyXG4gIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgIGNvbXBvbmVudHM6IHtBbGJ1bVRhYmxlOiBBbGJ1bVRhYmxlLCBQYWdpbmF0aW9uOiBQYWdpbmF0aW9ufSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgIGxldCBzb3J0T3JkZXJzID0ge307XHJcbiAgICAgIGxldCBjb2x1bW5zID0gW1xyXG4gICAgICAgICAge3dpZHRoOiAnNDAlJywgbGFiZWw6ICdBbGJ1bScsIG5hbWU6ICd0aXRsZScgfSxcclxuICAgICAgICAgIHt3aWR0aDogJzEwJScsIGxhYmVsOiAnQWN0aW9uJ30sXHJcbiAgICAgIF07XHJcbiAgICAgIGNvbHVtbnMuZm9yRWFjaCgoY29sdW1uKSA9PiB7XHJcbiAgICAgICAgIHNvcnRPcmRlcnNbY29sdW1uLm5hbWVdID0gMDtcclxuICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICBhY3Q6JycsXHJcbiAgICAgICAgICAgICAgYWxidW06e1xyXG4gICAgICAgICAgICAgICAgaWQ6JycsXHJcbiAgICAgICAgICAgICAgICB0aXRsZTonJ1xyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgdXJsOiAnLi9nYWxsZXJ5L2FsYnVtLXZpZXcvJyxcclxuICAgICAgICAgICAgICBzdWNjZXNzOiBmYWxzZSxcclxuICAgICAgICAgICAgICBlcnJvcjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgZXJyb3IyOiBmYWxzZSxcclxuICAgICAgICAgICAgICBwcm9qZWN0czogW10sXHJcbiAgICAgICAgICAgICAgY29sdW1uczogY29sdW1ucyxcclxuICAgICAgICAgICAgICBzb3J0S2V5OiAndGl0bGUnLFxyXG4gICAgICAgICAgICAgIHNvcnRPcmRlcnM6IHNvcnRPcmRlcnMsXHJcbiAgICAgICAgICAgICAgcGVyUGFnZTogWycxMCcsICcyMCcsICczMCddLFxyXG4gICAgICAgICAgICAgIHNlbmRSZXE6IGZhbHNlLFxyXG4gICAgICAgICAgICAgIHRhYmxlRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICBkcmF3OiAwLFxyXG4gICAgICAgICAgICAgICAgICBsZW5ndGg6IDEwLFxyXG4gICAgICAgICAgICAgICAgICBzZWFyY2g6ICcnLFxyXG4gICAgICAgICAgICAgICAgICBjb2x1bW46IDAsXHJcbiAgICAgICAgICAgICAgICAgIGRpcjogJ2FzYycsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICBwYWdpbmF0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICAgIGxhc3RQYWdlOiAnJyxcclxuICAgICAgICAgICAgICAgICAgY3VycmVudFBhZ2U6ICcnLFxyXG4gICAgICAgICAgICAgICAgICB0b3RhbDogJycsXHJcbiAgICAgICAgICAgICAgICAgIGxhc3RQYWdlVXJsOiAnJyxcclxuICAgICAgICAgICAgICAgICAgbmV4dFBhZ2VVcmw6ICcnLFxyXG4gICAgICAgICAgICAgICAgICBwcmV2UGFnZVVybDogJycsXHJcbiAgICAgICAgICAgICAgICAgIGZyb206ICcnLFxyXG4gICAgICAgICAgICAgICAgICB0bzogJydcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgfSxcclxuICAgICAgbWV0aG9kczp7XHJcblxyXG4gICAgICAgIGNvbmZpZ1BhZ2luYXRpb24oZGF0YSkge1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24ubGFzdFBhZ2UgPSBkYXRhLmxhc3RfcGFnZTtcclxuICAgICAgICAgICAgdGhpcy5wYWdpbmF0aW9uLmN1cnJlbnRQYWdlID0gZGF0YS5jdXJyZW50X3BhZ2U7XHJcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi50b3RhbCA9IGRhdGEudG90YWw7XHJcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi5sYXN0UGFnZVVybCA9IGRhdGEubGFzdF9wYWdlX3VybDtcclxuICAgICAgICAgICAgdGhpcy5wYWdpbmF0aW9uLm5leHRQYWdlVXJsID0gZGF0YS5uZXh0X3BhZ2VfdXJsO1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24ucHJldlBhZ2VVcmwgPSBkYXRhLnByZXZfcGFnZV91cmw7XHJcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi5mcm9tID0gZGF0YS5mcm9tO1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24udG8gPSBkYXRhLnRvO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc29ydEJ5KGtleSkge1xyXG4gICAgICAgICAgICB0aGlzLnNvcnRLZXkgPSBrZXk7XHJcbiAgICAgICAgICAgIHRoaXMuc29ydE9yZGVyc1trZXldID0gdGhpcy5zb3J0T3JkZXJzW2tleV0gKiAtMTtcclxuICAgICAgICAgICAgdGhpcy50YWJsZURhdGEuY29sdW1uID0gdGhpcy5nZXRJbmRleCh0aGlzLmNvbHVtbnMsICduYW1lJywga2V5KTtcclxuICAgICAgICAgICAgdGhpcy50YWJsZURhdGEuZGlyID0gdGhpcy5zb3J0T3JkZXJzW2tleV0gPT09IDEgPyAnYXNjJyA6ICdkZXNjJztcclxuICAgICAgICAgICAgdGhpcy5nZXRQcm9qZWN0cygpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZ2V0SW5kZXgoYXJyYXksIGtleSwgdmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGFycmF5LmZpbmRJbmRleChpID0+IGlba2V5XSA9PSB2YWx1ZSlcclxuICAgICAgICB9LFxyXG4gICAgICAgIGdldEFsYnVtcyh1cmwgPSAnLi9nYWxsZXJ5L2FsYnVtLWxpc3QnKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFibGVEYXRhLmRyYXcrKztcclxuICAgICAgICAgICAgYXhpb3MuZ2V0KHVybCwge3BhcmFtczogdGhpcy50YWJsZURhdGF9KVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkYXRhID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50YWJsZURhdGEuZHJhdyA9PSBkYXRhLmRyYXcpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdHMgPSBkYXRhLmRhdGEuZGF0YTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb25maWdQYWdpbmF0aW9uKGRhdGEuZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5jYXRjaChlcnJvcnMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9ycyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGFjdGlvbkRvYyhlLCBpZCl7XHJcbiAgICAgICAgICBpZihlID09IFwiYWRkXCIpe1xyXG4gICAgICAgICAgICAkKCcjZGlhbG9nQWRkQWxidW0nKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgICAgIHRoaXMuYWN0ID0gXCJBZGRcIjtcclxuICAgICAgICAgIH1lbHNlIGlmKGU9PT0nZWRpdCcpe1xyXG4gICAgICAgICAgICB0aGlzLmFsYnVtLmlkID0gaWQ7XHJcbiAgICAgICAgICAgIHRoaXMuJGh0dHAuZ2V0KCcuL2dhbGxlcnkvYWxidW0tbGlzdC8nICsgaWQpLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IGRhdGEgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hbGJ1bS50aXRsZSA9IGRhdGEudGl0bGU7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICQoJyNkaWFsb2dBZGRBbGJ1bScpLm1vZGFsKCd0b2dnbGUnKTtcclxuICAgICAgICAgICAgdGhpcy5hY3QgPSBcIkVkaXRcIjtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHJlc2V0QWxsKCl7XHJcbiAgICAgICAgICB0aGlzLmVycm9yID0gZmFsc2U7XHJcbiAgICAgICAgICB0aGlzLmVycm9yMiA9IGZhbHNlO1xyXG4gICAgICAgICAgJCgnI2RpYWxvZ0FkZEFsYnVtJykubW9kYWwoJ3RvZ2dsZScpO1xyXG4gICAgICAgICAgdGhpcy5hY3QgPSBcIlwiO1xyXG4gICAgICAgICAgdGhpcy5hbGJ1bSA9IHtcclxuICAgICAgICAgICAgaWQ6JycsXHJcbiAgICAgICAgICAgIHRpdGxlOicnXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzYXZlQWxidW0oZSxpZCl7XHJcbiAgICAgICAgICB0aGlzLmVycm9yID0gZmFsc2U7XHJcbiAgICAgICAgICB0aGlzLmVycm9yMiA9IGZhbHNlO1xyXG4gICAgICAgICAgaWYoZT09J0FkZCcpe1xyXG4gICAgICAgICAgICB0aGlzLiRodHRwLnBvc3QoJy4vZ2FsbGVyeS9hbGJ1bS1saXN0JywgdGhpcy5hbGJ1bSkuY2F0Y2goZnVuY3Rpb24gKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgIGlmKGVycm9yLnN0YXR1cz09NDIyKXtcclxuICAgICAgICAgICAgICAgICB0aGlzLmVycm9yID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICBpZihlcnJvci5zdGF0dXM9PTUwMCl7XHJcbiAgICAgICAgICAgICAgICAgdGhpcy5lcnJvcjIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgIGlmKCF0aGlzLmVycm9yICYmICF0aGlzLmVycm9yMil7XHJcbiAgICAgICAgICAgICAgICAkKCcjZGlhbG9nQWRkQWxidW0nKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjdCA9IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFsYnVtID0ge1xyXG4gICAgICAgICAgICAgICAgICBpZDonJyxcclxuICAgICAgICAgICAgICAgICAgdGl0bGU6JydcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgIH1lbHNlIGlmKGU9PSdFZGl0Jyl7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3IgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5lcnJvcjIgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy4kaHR0cC5wb3N0KCcuL2dhbGxlcnkvYWxidW0tbGlzdC9lZGl0LycrIGlkLCB0aGlzLmFsYnVtKS5jYXRjaChmdW5jdGlvbiAoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgaWYoZXJyb3Iuc3RhdHVzPT00MjIpe1xyXG4gICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgIGlmKGVycm9yLnN0YXR1cz09NTAwKXtcclxuICAgICAgICAgICAgICAgICB0aGlzLmVycm9yMiA9IHRydWU7XHJcbiAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgaWYoIXRoaXMuZXJyb3IgJiYgIXRoaXMuZXJyb3IyKXtcclxuICAgICAgICAgICAgICAgICQoJyNkaWFsb2dBZGRBbGJ1bScpLm1vZGFsKCd0b2dnbGUnKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWN0ID0gXCJcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWxidW0gPSB7XHJcbiAgICAgICAgICAgICAgICAgIGlkOicnLFxyXG4gICAgICAgICAgICAgICAgICB0aXRsZTonJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLnNlbmRSZXEgPSB0cnVlO1xyXG4gICAgICAgICAgdGhpcy5nZXRBbGJ1bXMoKTtcclxuXHJcbiAgICAgICAgfSxcclxuICAgICAgICBkZWxldGVBbGJ1bShpZCl7XHJcbiAgICAgICAgICBpZihjb25maXJtKCdBcmUgeW91IHN1cmU/Jykpe1xyXG4gICAgICAgICAgICAvL3RoaXMudXBkYXRlUHJvamVjdHMoJy4vbmV3cy1saXN0L3VwZGF0ZScsJ2RlbGV0ZScsaWQpO1xyXG4gICAgICAgICAgICB0aGlzLiRodHRwLmRlbGV0ZSgnLi9nYWxsZXJ5L2FsYnVtLWxpc3QvJyArIGlkKS50aGVuKGZ1bmN0aW9uIChyZXMpIHtcclxuICAgICAgICAgICAgICBpZihyZXMuZGF0YS5yZXNwb25zZT09J2ZhaWxlZCcpe1xyXG4gICAgICAgICAgICAgICAgYWxlcnQoXCJUaGlzIGFsYnVtIGlzIG5vdCBlbXB0eSFcIik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMuc2VuZFJlcSA9IHRydWU7XHJcbiAgICAgICAgICB0aGlzLmdldEFsYnVtcygpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgIH0sXHJcbiAgICAgIGNyZWF0ZWQoKXtcclxuICAgICAgICB0aGlzLmdldEFsYnVtcygpO1xyXG4gICAgICB9LFxyXG4gICAgICB1cGRhdGVkKCl7XHJcbiAgICAgICAgaWYodGhpcy5zZW5kUmVxKXtcclxuICAgICAgICAgIGZvcih2YXIgaT0wO2k8MjtpKyspe1xyXG4gICAgICAgICAgICB0aGlzLmdldEFsYnVtcygpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnNlbmRSZXEgPSBmYWxzZTtcclxuXHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgIH1cclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gUHJvamVjdHMudnVlPzZmYjMwYzY4IiwiIWZ1bmN0aW9uKGUsdCl7XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHMmJlwib2JqZWN0XCI9PXR5cGVvZiBtb2R1bGU/bW9kdWxlLmV4cG9ydHM9dCgpOlwiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoW10sdCk6XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHM/ZXhwb3J0c1tcInZ1ZTItZHJvcHpvbmVcIl09dCgpOmVbXCJ2dWUyLWRyb3B6b25lXCJdPXQoKX0odGhpcyxmdW5jdGlvbigpe3JldHVybiBmdW5jdGlvbihlKXtmdW5jdGlvbiB0KG4pe2lmKGlbbl0pcmV0dXJuIGlbbl0uZXhwb3J0czt2YXIgbz1pW25dPXtleHBvcnRzOnt9LGlkOm4sbG9hZGVkOiExfTtyZXR1cm4gZVtuXS5jYWxsKG8uZXhwb3J0cyxvLG8uZXhwb3J0cyx0KSxvLmxvYWRlZD0hMCxvLmV4cG9ydHN9dmFyIGk9e307cmV0dXJuIHQubT1lLHQuYz1pLHQucD1cIlwiLHQoMCl9KFtmdW5jdGlvbihlLHQsaSl7dmFyIG4sbyxyPXt9O2koOCksbj1pKDIpLG89aSg2KSxlLmV4cG9ydHM9bnx8e30sZS5leHBvcnRzLl9fZXNNb2R1bGUmJihlLmV4cG9ydHM9ZS5leHBvcnRzLmRlZmF1bHQpO3ZhciBzPVwiZnVuY3Rpb25cIj09dHlwZW9mIGUuZXhwb3J0cz9lLmV4cG9ydHMub3B0aW9uc3x8KGUuZXhwb3J0cy5vcHRpb25zPXt9KTplLmV4cG9ydHM7byYmKHMudGVtcGxhdGU9bykscy5jb21wdXRlZHx8KHMuY29tcHV0ZWQ9e30pLE9iamVjdC5rZXlzKHIpLmZvckVhY2goZnVuY3Rpb24oZSl7dmFyIHQ9cltlXTtzLmNvbXB1dGVkW2VdPWZ1bmN0aW9uKCl7cmV0dXJuIHR9fSl9LGZ1bmN0aW9uKGUsdCl7ZS5leHBvcnRzPWZ1bmN0aW9uKCl7dmFyIGU9W107cmV0dXJuIGUudG9TdHJpbmc9ZnVuY3Rpb24oKXtmb3IodmFyIGU9W10sdD0wO3Q8dGhpcy5sZW5ndGg7dCsrKXt2YXIgaT10aGlzW3RdO2lbMl0/ZS5wdXNoKFwiQG1lZGlhIFwiK2lbMl0rXCJ7XCIraVsxXStcIn1cIik6ZS5wdXNoKGlbMV0pfXJldHVybiBlLmpvaW4oXCJcIil9LGUuaT1mdW5jdGlvbih0LGkpe1wic3RyaW5nXCI9PXR5cGVvZiB0JiYodD1bW251bGwsdCxcIlwiXV0pO2Zvcih2YXIgbj17fSxvPTA7bzx0aGlzLmxlbmd0aDtvKyspe3ZhciByPXRoaXNbb11bMF07XCJudW1iZXJcIj09dHlwZW9mIHImJihuW3JdPSEwKX1mb3Iobz0wO288dC5sZW5ndGg7bysrKXt2YXIgcz10W29dO1wibnVtYmVyXCI9PXR5cGVvZiBzWzBdJiZuW3NbMF1dfHwoaSYmIXNbMl0/c1syXT1pOmkmJihzWzJdPVwiKFwiK3NbMl0rXCIpIGFuZCAoXCIraStcIilcIiksZS5wdXNoKHMpKX19LGV9fSxmdW5jdGlvbihlLHQsaSl7XCJ1c2Ugc3RyaWN0XCI7T2JqZWN0LmRlZmluZVByb3BlcnR5KHQsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSksdC5kZWZhdWx0PXtwcm9wczp7aWQ6e3R5cGU6U3RyaW5nLHJlcXVpcmVkOiEwfSx1cmw6e3R5cGU6U3RyaW5nLHJlcXVpcmVkOiEwfSxjbGlja2FibGU6e3R5cGU6W0Jvb2xlYW4sU3RyaW5nXSxkZWZhdWx0OiEwfSxjb25maXJtOnt0eXBlOkZ1bmN0aW9uLGRlZmF1bHQ6dm9pZCAwfSxwYXJhbU5hbWU6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJmaWxlXCJ9LGFjY2VwdGVkRmlsZVR5cGVzOnt0eXBlOlN0cmluZ30sdGh1bWJuYWlsSGVpZ2h0Ont0eXBlOk51bWJlcixkZWZhdWx0OjIwMH0sdGh1bWJuYWlsV2lkdGg6e3R5cGU6TnVtYmVyLGRlZmF1bHQ6MjAwfSxzaG93UmVtb3ZlTGluazp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITB9LG1heEZpbGVTaXplSW5NQjp7dHlwZTpOdW1iZXIsZGVmYXVsdDoyfSxtYXhOdW1iZXJPZkZpbGVzOnt0eXBlOk51bWJlcixkZWZhdWx0OjV9LGF1dG9Qcm9jZXNzUXVldWU6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiEwfSx1c2VGb250QXdlc29tZTp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITF9LGhlYWRlcnM6e3R5cGU6T2JqZWN0fSxsYW5ndWFnZTp7dHlwZTpPYmplY3QsZGVmYXVsdDpmdW5jdGlvbigpe3JldHVybnt9fX0scHJldmlld1RlbXBsYXRlOnt0eXBlOkZ1bmN0aW9uLGRlZmF1bHQ6ZnVuY3Rpb24oZSl7cmV0dXJuJ1xcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImR6LXByZXZpZXcgZHotZmlsZS1wcmV2aWV3XCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImR6LWltYWdlXCIgc3R5bGU9XCJ3aWR0aDogJytlLnRodW1ibmFpbFdpZHRoK1wicHg7aGVpZ2h0OiBcIitlLnRodW1ibmFpbEhlaWdodCsncHhcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nIGRhdGEtZHotdGh1bWJuYWlsIC8+PC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImR6LWRldGFpbHNcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImR6LXNpemVcIj48c3BhbiBkYXRhLWR6LXNpemU+PC9zcGFuPjwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHotZmlsZW5hbWVcIj48c3BhbiBkYXRhLWR6LW5hbWU+PC9zcGFuPjwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkei1wcm9ncmVzc1wiPjxzcGFuIGNsYXNzPVwiZHotdXBsb2FkXCIgZGF0YS1kei11cGxvYWRwcm9ncmVzcz48L3NwYW4+PC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImR6LWVycm9yLW1lc3NhZ2VcIj48c3BhbiBkYXRhLWR6LWVycm9ybWVzc2FnZT48L3NwYW4+PC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImR6LXN1Y2Nlc3MtbWFya1wiPicrZS5kb25lSWNvbisnPC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImR6LWVycm9yLW1hcmtcIj4nK2UuZXJyb3JJY29uK1wiPC9kaXY+XFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICAgICAgXCJ9fSx1c2VDdXN0b21Ecm9wem9uZU9wdGlvbnM6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxkcm9wem9uZU9wdGlvbnM6e3R5cGU6T2JqZWN0LGRlZmF1bHQ6ZnVuY3Rpb24oKXtyZXR1cm57fX19LHJlc2l6ZVdpZHRoOnt0eXBlOk51bWJlcixkZWZhdWx0Om51bGx9LHJlc2l6ZUhlaWdodDp7dHlwZTpOdW1iZXIsZGVmYXVsdDpudWxsfSxyZXNpemVNaW1lVHlwZTp7dHlwZTpTdHJpbmcsZGVmYXVsdDpudWxsfSxyZXNpemVRdWFsaXR5Ont0eXBlOk51bWJlcixkZWZhdWx0Oi44fSxyZXNpemVNZXRob2Q6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJjb250YWluXCJ9LHVwbG9hZE11bHRpcGxlOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0sZHVwbGljYXRlQ2hlY2s6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxwYXJhbGxlbFVwbG9hZHM6e3R5cGU6TnVtYmVyLGRlZmF1bHQ6Mn0sdGltZW91dDp7dHlwZTpOdW1iZXIsZGVmYXVsdDozZTR9LG1ldGhvZDp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIlBPU1RcIn0sd2l0aENyZWRlbnRpYWxzOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0sY2FwdHVyZTp7dHlwZTpTdHJpbmcsZGVmYXVsdDpudWxsfSxoaWRkZW5JbnB1dENvbnRhaW5lcjp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcImJvZHlcIn19LG1ldGhvZHM6e21hbnVhbGx5QWRkRmlsZTpmdW5jdGlvbihlLHQsaSxuLG8pe3RoaXMuZHJvcHpvbmUuZW1pdChcImFkZGVkZmlsZVwiLGUpLHRoaXMuZHJvcHpvbmUuZW1pdChcInRodW1ibmFpbFwiLGUsdCksdGhpcy5kcm9wem9uZS5jcmVhdGVUaHVtYm5haWxGcm9tVXJsKGUsdCxpLG4pLHRoaXMuZHJvcHpvbmUuZW1pdChcImNvbXBsZXRlXCIsZSksXCJ1bmRlZmluZWRcIiE9dHlwZW9mIG8uZG9udFN1YnN0cmFjdE1heEZpbGVzJiZvLmRvbnRTdWJzdHJhY3RNYXhGaWxlc3x8KHRoaXMuZHJvcHpvbmUub3B0aW9ucy5tYXhGaWxlcz10aGlzLmRyb3B6b25lLm9wdGlvbnMubWF4RmlsZXMtMSksXCJ1bmRlZmluZWRcIiE9dHlwZW9mIG8uYWRkVG9GaWxlcyYmby5hZGRUb0ZpbGVzJiZ0aGlzLmRyb3B6b25lLmZpbGVzLnB1c2goZSksdGhpcy4kZW1pdChcInZkcm9wem9uZS1maWxlLWFkZGVkLW1hbnVhbGx5XCIsZSl9LHNldE9wdGlvbjpmdW5jdGlvbihlLHQpe3RoaXMuZHJvcHpvbmUub3B0aW9uc1tlXT10fSxyZW1vdmVBbGxGaWxlczpmdW5jdGlvbigpe3RoaXMuZHJvcHpvbmUucmVtb3ZlQWxsRmlsZXMoITApfSxwcm9jZXNzUXVldWU6ZnVuY3Rpb24oKXt2YXIgZT10aGlzLmRyb3B6b25lO3RoaXMuZHJvcHpvbmUucHJvY2Vzc1F1ZXVlKCksdGhpcy5kcm9wem9uZS5vbihcInN1Y2Nlc3NcIixmdW5jdGlvbigpe2Uub3B0aW9ucy5hdXRvUHJvY2Vzc1F1ZXVlPSEwfSksdGhpcy5kcm9wem9uZS5vbihcInF1ZXVlY29tcGxldGVcIixmdW5jdGlvbigpe2Uub3B0aW9ucy5hdXRvUHJvY2Vzc1F1ZXVlPSExfSl9LHJlbW92ZUZpbGU6ZnVuY3Rpb24oZSl7dGhpcy5kcm9wem9uZS5yZW1vdmVGaWxlKGUpfSxnZXRBY2NlcHRlZEZpbGVzOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZHJvcHpvbmUuZ2V0QWNjZXB0ZWRGaWxlcygpfSxnZXRSZWplY3RlZEZpbGVzOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZHJvcHpvbmUuZ2V0UmVqZWN0ZWRGaWxlcygpfSxnZXRVcGxvYWRpbmdGaWxlczpmdW5jdGlvbigpe3JldHVybiB0aGlzLmRyb3B6b25lLmdldFVwbG9hZGluZ0ZpbGVzKCl9LGdldFF1ZXVlZEZpbGVzOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZHJvcHpvbmUuZ2V0UXVldWVkRmlsZXMoKX0sZ2V0UHJvcDpmdW5jdGlvbihlLHQpe3JldHVybiB0aGlzLnVzZUN1c3RvbURyb3B6b25lT3B0aW9ucyYmdm9pZCAwIT09dCYmbnVsbCE9PXQmJlwiXCIhPT10P3Q6ZX19LGNvbXB1dGVkOntsYW5ndWFnZVNldHRpbmdzOmZ1bmN0aW9uKCl7dmFyIGU9e2RpY3REZWZhdWx0TWVzc2FnZTpcIjxicj5Ecm9wIGZpbGVzIGhlcmUgdG8gdXBsb2FkXCIsZGljdENhbmNlbFVwbG9hZDpcIkNhbmNlbCB1cGxvYWRcIixkaWN0Q2FuY2VsVXBsb2FkQ29uZmlybWF0aW9uOlwiQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNhbmNlbCB0aGlzIHVwbG9hZD9cIixkaWN0RmFsbGJhY2tNZXNzYWdlOlwiWW91ciBicm93c2VyIGRvZXMgbm90IHN1cHBvcnQgZHJhZyBhbmQgZHJvcCBmaWxlIHVwbG9hZHMuXCIsZGljdEZhbGxiYWNrVGV4dDpcIlBsZWFzZSB1c2UgdGhlIGZhbGxiYWNrIGZvcm0gYmVsb3cgdG8gdXBsb2FkIHlvdXIgZmlsZXMgbGlrZSBpbiB0aGUgb2xkZW4gZGF5cy5cIixkaWN0RmlsZVRvb0JpZzpcIkZpbGUgaXMgdG9vIGJpZyAoe3tmaWxlc2l6ZX19TWlCKS4gTWF4IGZpbGVzaXplOiB7e21heEZpbGVzaXplfX1NaUIuXCIsZGljdEludmFsaWRGaWxlVHlwZTpcIllvdSBjYW4ndCB1cGxvYWQgZmlsZXMgb2YgdGhpcyB0eXBlLlwiLGRpY3RNYXhGaWxlc0V4Y2VlZGVkOlwiWW91IGNhbiBub3QgdXBsb2FkIGFueSBtb3JlIGZpbGVzLiAobWF4OiB7e21heEZpbGVzfX0pXCIsZGljdFJlbW92ZUZpbGU6XCJSZW1vdmVcIixkaWN0UmVtb3ZlRmlsZUNvbmZpcm1hdGlvbjpudWxsLGRpY3RSZXNwb25zZUVycm9yOlwiU2VydmVyIHJlc3BvbmRlZCB3aXRoIHt7c3RhdHVzQ29kZX19IGNvZGUuXCJ9O2Zvcih2YXIgdCBpbiB0aGlzLmxhbmd1YWdlKWVbdF09dGhpcy5sYW5ndWFnZVt0XTtpZih0aGlzLnVzZUN1c3RvbURyb3B6b25lT3B0aW9ucyYmdGhpcy5kcm9wem9uZU9wdGlvbnMubGFuZ3VhZ2UpZm9yKHZhciBpIGluIHRoaXMuZHJvcHpvbmVPcHRpb25zLmxhbmd1YWdlKWVbaV09dGhpcy5kcm9wem9uZU9wdGlvbnMubGFuZ3VhZ2VbaV07cmV0dXJuIGV9LGNsb3VkSWNvbjpmdW5jdGlvbigpe3JldHVybiB0aGlzLnVzZUZvbnRBd2Vzb21lPyc8aSBjbGFzcz1cImZhIGZhLWNsb3VkLXVwbG9hZFwiPjwvaT4nOic8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCI+Y2xvdWRfdXBsb2FkPC9pPid9LGRvbmVJY29uOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMudXNlRm9udEF3ZXNvbWU/JzxpIGNsYXNzPVwiZmEgZmEtY2hlY2tcIj48L2k+JzonIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5kb25lPC9pPid9LGVycm9ySWNvbjpmdW5jdGlvbigpe3JldHVybiB0aGlzLnVzZUZvbnRBd2Vzb21lPyc8aSBjbGFzcz1cImZhIGZhLWNsb3NlXCI+PC9pPic6JyA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCI+ZXJyb3I8L2k+J319LG1vdW50ZWQ6ZnVuY3Rpb24oKXtpZighdGhpcy4kaXNTZXJ2ZXIpe3ZhciBlPWkoNSk7ZS5hdXRvRGlzY292ZXI9ITEsdGhpcy5jb25maXJtJiYoZS5jb25maXJtPXRoaXMuZ2V0UHJvcCh0aGlzLmNvbmZpcm0sdGhpcy5kcm9wem9uZU9wdGlvbnMuY29uZmlybSkpO3ZhciB0PWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRoaXMuaWQpO3RoaXMuZHJvcHpvbmU9bmV3IGUodCx7Y2xpY2thYmxlOnRoaXMuZ2V0UHJvcCh0aGlzLmNsaWNrYWJsZSx0aGlzLmRyb3B6b25lT3B0aW9ucy5jbGlja2FibGUpLHBhcmFtTmFtZTp0aGlzLmdldFByb3AodGhpcy5wYXJhbU5hbWUsdGhpcy5kcm9wem9uZU9wdGlvbnMucGFyYW1OYW1lKSx0aHVtYm5haWxXaWR0aDp0aGlzLmdldFByb3AodGhpcy50aHVtYm5haWxXaWR0aCx0aGlzLmRyb3B6b25lT3B0aW9ucy50aHVtYm5haWxXaWR0aCksdGh1bWJuYWlsSGVpZ2h0OnRoaXMuZ2V0UHJvcCh0aGlzLnRodW1ibmFpbEhlaWdodCx0aGlzLmRyb3B6b25lT3B0aW9ucy50aHVtYm5haWxIZWlnaHQpLG1heEZpbGVzOnRoaXMuZ2V0UHJvcCh0aGlzLm1heE51bWJlck9mRmlsZXMsdGhpcy5kcm9wem9uZU9wdGlvbnMubWF4TnVtYmVyT2ZGaWxlcyksbWF4RmlsZXNpemU6dGhpcy5nZXRQcm9wKHRoaXMubWF4RmlsZVNpemVJbk1CLHRoaXMuZHJvcHpvbmVPcHRpb25zLm1heEZpbGVTaXplSW5NQiksYWRkUmVtb3ZlTGlua3M6dGhpcy5nZXRQcm9wKHRoaXMuc2hvd1JlbW92ZUxpbmssdGhpcy5kcm9wem9uZU9wdGlvbnMuc2hvd1JlbW92ZUxpbmspLGFjY2VwdGVkRmlsZXM6dGhpcy5nZXRQcm9wKHRoaXMuYWNjZXB0ZWRGaWxlVHlwZXMsdGhpcy5kcm9wem9uZU9wdGlvbnMuYWNjZXB0ZWRGaWxlVHlwZXMpLGF1dG9Qcm9jZXNzUXVldWU6dGhpcy5nZXRQcm9wKHRoaXMuYXV0b1Byb2Nlc3NRdWV1ZSx0aGlzLmRyb3B6b25lT3B0aW9ucy5hdXRvUHJvY2Vzc1F1ZXVlKSxoZWFkZXJzOnRoaXMuZ2V0UHJvcCh0aGlzLmhlYWRlcnMsdGhpcy5kcm9wem9uZU9wdGlvbnMuaGVhZGVycykscHJldmlld1RlbXBsYXRlOnRoaXMucHJldmlld1RlbXBsYXRlKHRoaXMpLGRpY3REZWZhdWx0TWVzc2FnZTp0aGlzLmNsb3VkSWNvbit0aGlzLmxhbmd1YWdlU2V0dGluZ3MuZGljdERlZmF1bHRNZXNzYWdlLGRpY3RDYW5jZWxVcGxvYWQ6dGhpcy5sYW5ndWFnZVNldHRpbmdzLmRpY3RDYW5jZWxVcGxvYWQsZGljdENhbmNlbFVwbG9hZENvbmZpcm1hdGlvbjp0aGlzLmxhbmd1YWdlU2V0dGluZ3MuZGljdENhbmNlbFVwbG9hZENvbmZpcm1hdGlvbixkaWN0RmFsbGJhY2tNZXNzYWdlOnRoaXMubGFuZ3VhZ2VTZXR0aW5ncy5kaWN0RmFsbGJhY2tNZXNzYWdlLGRpY3RGYWxsYmFja1RleHQ6dGhpcy5sYW5ndWFnZVNldHRpbmdzLmRpY3RGYWxsYmFja1RleHQsZGljdEZpbGVUb29CaWc6dGhpcy5sYW5ndWFnZVNldHRpbmdzLmRpY3RGaWxlVG9vQmlnLGRpY3RJbnZhbGlkRmlsZVR5cGU6dGhpcy5sYW5ndWFnZVNldHRpbmdzLmRpY3RJbnZhbGlkRmlsZVR5cGUsZGljdE1heEZpbGVzRXhjZWVkZWQ6dGhpcy5sYW5ndWFnZVNldHRpbmdzLmRpY3RNYXhGaWxlc0V4Y2VlZGVkLGRpY3RSZW1vdmVGaWxlOnRoaXMubGFuZ3VhZ2VTZXR0aW5ncy5kaWN0UmVtb3ZlRmlsZSxkaWN0UmVtb3ZlRmlsZUNvbmZpcm1hdGlvbjp0aGlzLmxhbmd1YWdlU2V0dGluZ3MuZGljdFJlbW92ZUZpbGVDb25maXJtYXRpb24sZGljdFJlc3BvbnNlRXJyb3I6dGhpcy5sYW5ndWFnZVNldHRpbmdzLmRpY3RSZXNwb25zZUVycm9yLHJlc2l6ZVdpZHRoOnRoaXMuZ2V0UHJvcCh0aGlzLnJlc2l6ZVdpZHRoLHRoaXMuZHJvcHpvbmVPcHRpb25zLnJlc2l6ZVdpZHRoKSxyZXNpemVIZWlnaHQ6dGhpcy5nZXRQcm9wKHRoaXMucmVzaXplSGVpZ2h0LHRoaXMuZHJvcHpvbmVPcHRpb25zLnJlc2l6ZUhlaWdodCkscmVzaXplTWltZVR5cGU6dGhpcy5nZXRQcm9wKHRoaXMucmVzaXplTWltZVR5cGUsdGhpcy5kcm9wem9uZU9wdGlvbnMucmVzaXplTWltZVR5cGUpLHJlc2l6ZVF1YWxpdHk6dGhpcy5nZXRQcm9wKHRoaXMucmVzaXplUXVhbGl0eSx0aGlzLmRyb3B6b25lT3B0aW9ucy5yZXNpemVRdWFsaXR5KSxyZXNpemVNZXRob2Q6dGhpcy5nZXRQcm9wKHRoaXMucmVzaXplTWV0aG9kLHRoaXMuZHJvcHpvbmVPcHRpb25zLnJlc2l6ZU1ldGhvZCksdXBsb2FkTXVsdGlwbGU6dGhpcy5nZXRQcm9wKHRoaXMudXBsb2FkTXVsdGlwbGUsdGhpcy5kcm9wem9uZU9wdGlvbnMudXBsb2FkTXVsdGlwbGUpLHBhcmFsbGVsVXBsb2Fkczp0aGlzLmdldFByb3AodGhpcy5wYXJhbGxlbFVwbG9hZHMsdGhpcy5kcm9wem9uZU9wdGlvbnMucGFyYWxsZWxVcGxvYWRzKSx0aW1lb3V0OnRoaXMuZ2V0UHJvcCh0aGlzLnRpbWVvdXQsdGhpcy5kcm9wem9uZU9wdGlvbnMudGltZW91dCksbWV0aG9kOnRoaXMuZ2V0UHJvcCh0aGlzLm1ldGhvZCx0aGlzLmRyb3B6b25lT3B0aW9ucy5tZXRob2QpLGNhcHR1cmU6dGhpcy5nZXRQcm9wKHRoaXMuY2FwdHVyZSx0aGlzLmRyb3B6b25lT3B0aW9ucy5jYXB0dXJlKSxoaWRkZW5JbnB1dENvbnRhaW5lcjp0aGlzLmdldFByb3AodGhpcy5oaWRkZW5JbnB1dENvbnRhaW5lcix0aGlzLmRyb3B6b25lT3B0aW9ucy5oaWRkZW5JbnB1dENvbnRhaW5lciksd2l0aENyZWRlbnRpYWxzOnRoaXMuZ2V0UHJvcCh0aGlzLndpdGhDcmVkZW50aWFscyx0aGlzLmRyb3B6b25lT3B0aW9ucy53aXRoQ3JlZGVudGlhbHMpfSk7dmFyIG49dGhpczt0aGlzLmRyb3B6b25lLm9uKFwidGh1bWJuYWlsXCIsZnVuY3Rpb24oZSx0KXtuLiRlbWl0KFwidmRyb3B6b25lLXRodW1ibmFpbFwiLGUsdCl9KSx0aGlzLmRyb3B6b25lLm9uKFwiYWRkZWRmaWxlXCIsZnVuY3Rpb24oZSl7aWYobi5kdXBsaWNhdGVDaGVjayYmdGhpcy5maWxlcy5sZW5ndGgpe3ZhciB0LGk7Zm9yKHQ9MCxpPXRoaXMuZmlsZXMubGVuZ3RoO3Q8aS0xO3QrKyl0aGlzLmZpbGVzW3RdLm5hbWU9PT1lLm5hbWUmJih0aGlzLnJlbW92ZUZpbGUoZSksbi4kZW1pdChcImR1cGxpY2F0ZS1maWxlXCIsZSkpfW4uJGVtaXQoXCJ2ZHJvcHpvbmUtZmlsZS1hZGRlZFwiLGUpfSksdGhpcy5kcm9wem9uZS5vbihcImFkZGVkZmlsZXNcIixmdW5jdGlvbihlKXtuLiRlbWl0KFwidmRyb3B6b25lLWZpbGVzLWFkZGVkXCIsZSl9KSx0aGlzLmRyb3B6b25lLm9uKFwicmVtb3ZlZGZpbGVcIixmdW5jdGlvbihlKXtuLiRlbWl0KFwidmRyb3B6b25lLXJlbW92ZWQtZmlsZVwiLGUpfSksdGhpcy5kcm9wem9uZS5vbihcInN1Y2Nlc3NcIixmdW5jdGlvbihlLHQpe24uJGVtaXQoXCJ2ZHJvcHpvbmUtc3VjY2Vzc1wiLGUsdCl9KSx0aGlzLmRyb3B6b25lLm9uKFwic3VjY2Vzc211bHRpcGxlXCIsZnVuY3Rpb24oZSx0KXtuLiRlbWl0KFwidmRyb3B6b25lLXN1Y2Nlc3MtbXVsdGlwbGVcIixlLHQpfSksdGhpcy5kcm9wem9uZS5vbihcImVycm9yXCIsZnVuY3Rpb24oZSx0LGkpe24uJGVtaXQoXCJ2ZHJvcHpvbmUtZXJyb3JcIixlLHQsaSl9KSx0aGlzLmRyb3B6b25lLm9uKFwic2VuZGluZ1wiLGZ1bmN0aW9uKGUsdCxpKXtuLiRlbWl0KFwidmRyb3B6b25lLXNlbmRpbmdcIixlLHQsaSl9KSx0aGlzLmRyb3B6b25lLm9uKFwic2VuZGluZ211bHRpcGxlXCIsZnVuY3Rpb24oZSx0LGkpe24uJGVtaXQoXCJ2ZHJvcHpvbmUtc2VuZGluZy1tdWx0aXBsZVwiLGUsdCxpKX0pLHRoaXMuZHJvcHpvbmUub24oXCJxdWV1ZWNvbXBsZXRlXCIsZnVuY3Rpb24oZSx0LGkpe24uJGVtaXQoXCJ2ZHJvcHpvbmUtcXVldWUtY29tcGxldGVcIixlLHQsaSl9KSx0aGlzLmRyb3B6b25lLm9uKFwidG90YWx1cGxvYWRwcm9ncmVzc1wiLGZ1bmN0aW9uKGUsdCxpKXtuLiRlbWl0KFwidmRyb3B6b25lLXRvdGFsLXVwbG9hZC1wcm9ncmVzc1wiLGUsdCxpKX0pLG4uJGVtaXQoXCJ2ZHJvcHpvbmUtbW91bnRlZFwiKX19LGJlZm9yZURlc3Ryb3k6ZnVuY3Rpb24oKXt0aGlzLmRyb3B6b25lLmRlc3Ryb3koKX19fSxmdW5jdGlvbihlLHQsaSl7dD1lLmV4cG9ydHM9aSgxKSgpLHQucHVzaChbZS5pZCwnQC13ZWJraXQta2V5ZnJhbWVzIHBhc3NpbmctdGhyb3VnaHswJXtvcGFjaXR5OjA7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSg0MHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSg0MHB4KX0zMCUsNzAle29wYWNpdHk6MTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfXRve29wYWNpdHk6MDstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC00MHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtNDBweCl9fUBrZXlmcmFtZXMgcGFzc2luZy10aHJvdWdoezAle29wYWNpdHk6MDstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDQwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDQwcHgpfTMwJSw3MCV7b3BhY2l0eToxOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9dG97b3BhY2l0eTowOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTQwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC00MHB4KX19QC13ZWJraXQta2V5ZnJhbWVzIHNsaWRlLWluezAle29wYWNpdHk6MDstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDQwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDQwcHgpfTMwJXtvcGFjaXR5OjE7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgwKX19QGtleWZyYW1lcyBzbGlkZS1pbnswJXtvcGFjaXR5OjA7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSg0MHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSg0MHB4KX0zMCV7b3BhY2l0eToxOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9fUAtd2Via2l0LWtleWZyYW1lcyBwdWxzZXswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgxKTt0cmFuc2Zvcm06c2NhbGUoMSl9MTAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDEuMSk7dHJhbnNmb3JtOnNjYWxlKDEuMSl9MjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDEpO3RyYW5zZm9ybTpzY2FsZSgxKX19QGtleWZyYW1lcyBwdWxzZXswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgxKTt0cmFuc2Zvcm06c2NhbGUoMSl9MTAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDEuMSk7dHJhbnNmb3JtOnNjYWxlKDEuMSl9MjAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDEpO3RyYW5zZm9ybTpzY2FsZSgxKX19LmRyb3B6b25lLC5kcm9wem9uZSAqe2JveC1zaXppbmc6Ym9yZGVyLWJveH0uZHJvcHpvbmV7bWluLWhlaWdodDoxNTBweDtib3JkZXI6MnB4IHNvbGlkIHJnYmEoMCwwLDAsLjMpO2JhY2tncm91bmQ6I2ZmZjtwYWRkaW5nOjIwcHh9LmRyb3B6b25lLmR6LWNsaWNrYWJsZXtjdXJzb3I6cG9pbnRlcn0uZHJvcHpvbmUuZHotY2xpY2thYmxlICp7Y3Vyc29yOmRlZmF1bHR9LmRyb3B6b25lLmR6LWNsaWNrYWJsZSAuZHotbWVzc2FnZSwuZHJvcHpvbmUuZHotY2xpY2thYmxlIC5kei1tZXNzYWdlICp7Y3Vyc29yOnBvaW50ZXJ9LmRyb3B6b25lLmR6LXN0YXJ0ZWQgLmR6LW1lc3NhZ2V7ZGlzcGxheTpub25lfS5kcm9wem9uZS5kei1kcmFnLWhvdmVye2JvcmRlci1zdHlsZTpzb2xpZH0uZHJvcHpvbmUuZHotZHJhZy1ob3ZlciAuZHotbWVzc2FnZXtvcGFjaXR5Oi41fS5kcm9wem9uZSAuZHotbWVzc2FnZXt0ZXh0LWFsaWduOmNlbnRlcjttYXJnaW46MmVtIDB9LmRyb3B6b25lIC5kei1wcmV2aWV3e3Bvc2l0aW9uOnJlbGF0aXZlO2Rpc3BsYXk6aW5saW5lLWJsb2NrO3ZlcnRpY2FsLWFsaWduOnRvcDttYXJnaW46MTZweDttaW4taGVpZ2h0OjEwMHB4fS5kcm9wem9uZSAuZHotcHJldmlldzpob3Zlcnt6LWluZGV4OjEwMDB9LmRyb3B6b25lIC5kei1wcmV2aWV3LmR6LWZpbGUtcHJldmlldyAuZHotaW1hZ2V7Ym9yZGVyLXJhZGl1czoyMHB4O2JhY2tncm91bmQ6Izk5OTtiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCgxODBkZWcsI2VlZSwjZGRkKX0uZHJvcHpvbmUgLmR6LXByZXZpZXcuZHotZmlsZS1wcmV2aWV3IC5kei1kZXRhaWxze29wYWNpdHk6MX0uZHJvcHpvbmUgLmR6LXByZXZpZXcuZHotaW1hZ2UtcHJldmlld3tiYWNrZ3JvdW5kOiNmZmZ9LmRyb3B6b25lIC5kei1wcmV2aWV3LmR6LWltYWdlLXByZXZpZXcgLmR6LWRldGFpbHN7dHJhbnNpdGlvbjpvcGFjaXR5IC4ycyBsaW5lYXJ9LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1yZW1vdmV7Zm9udC1zaXplOjE0cHg7dGV4dC1hbGlnbjpjZW50ZXI7ZGlzcGxheTpibG9jaztjdXJzb3I6cG9pbnRlcjtib3JkZXI6bm9uZX0uZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LXJlbW92ZTpob3Zlcnt0ZXh0LWRlY29yYXRpb246dW5kZXJsaW5lfS5kcm9wem9uZSAuZHotcHJldmlldzpob3ZlciAuZHotZGV0YWlsc3tvcGFjaXR5OjF9LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1kZXRhaWxze3otaW5kZXg6MjA7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7bGVmdDowO29wYWNpdHk6MDtmb250LXNpemU6MTNweDttaW4td2lkdGg6MTAwJTttYXgtd2lkdGg6MTAwJTtwYWRkaW5nOjJlbSAxZW07dGV4dC1hbGlnbjpjZW50ZXI7Y29sb3I6cmdiYSgwLDAsMCwuOSk7bGluZS1oZWlnaHQ6MTUwJX0uZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LWRldGFpbHMgLmR6LXNpemV7bWFyZ2luLWJvdHRvbToxZW07Zm9udC1zaXplOjE2cHh9LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1kZXRhaWxzIC5kei1maWxlbmFtZXt3aGl0ZS1zcGFjZTpub3dyYXB9LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1kZXRhaWxzIC5kei1maWxlbmFtZTpob3ZlciBzcGFue2JvcmRlcjoxcHggc29saWQgaHNsYSgwLDAlLDc4JSwuOCk7YmFja2dyb3VuZC1jb2xvcjpoc2xhKDAsMCUsMTAwJSwuOCl9LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1kZXRhaWxzIC5kei1maWxlbmFtZTpub3QoOmhvdmVyKXtvdmVyZmxvdzpoaWRkZW47dGV4dC1vdmVyZmxvdzplbGxpcHNpc30uZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LWRldGFpbHMgLmR6LWZpbGVuYW1lOm5vdCg6aG92ZXIpIHNwYW57Ym9yZGVyOjFweCBzb2xpZCB0cmFuc3BhcmVudH0uZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LWRldGFpbHMgLmR6LWZpbGVuYW1lIHNwYW4sLmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1kZXRhaWxzIC5kei1zaXplIHNwYW57YmFja2dyb3VuZC1jb2xvcjpoc2xhKDAsMCUsMTAwJSwuNCk7cGFkZGluZzowIC40ZW07Ym9yZGVyLXJhZGl1czozcHh9LmRyb3B6b25lIC5kei1wcmV2aWV3OmhvdmVyIC5kei1pbWFnZSBpbWd7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoMS4wNSk7dHJhbnNmb3JtOnNjYWxlKDEuMDUpOy13ZWJraXQtZmlsdGVyOmJsdXIoOHB4KTtmaWx0ZXI6Ymx1cig4cHgpfS5kcm9wem9uZSAuZHotcHJldmlldyAuZHotaW1hZ2V7Ym9yZGVyLXJhZGl1czoyMHB4O292ZXJmbG93OmhpZGRlbjt3aWR0aDoxMjBweDtoZWlnaHQ6MTIwcHg7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jazt6LWluZGV4OjEwfS5kcm9wem9uZSAuZHotcHJldmlldyAuZHotaW1hZ2UgaW1ne2Rpc3BsYXk6YmxvY2t9LmRyb3B6b25lIC5kei1wcmV2aWV3LmR6LXN1Y2Nlc3MgLmR6LXN1Y2Nlc3MtbWFya3std2Via2l0LWFuaW1hdGlvbjpwYXNzaW5nLXRocm91Z2ggM3MgY3ViaWMtYmV6aWVyKC43NywwLC4xNzUsMSk7YW5pbWF0aW9uOnBhc3NpbmctdGhyb3VnaCAzcyBjdWJpYy1iZXppZXIoLjc3LDAsLjE3NSwxKX0uZHJvcHpvbmUgLmR6LXByZXZpZXcuZHotZXJyb3IgLmR6LWVycm9yLW1hcmt7b3BhY2l0eToxOy13ZWJraXQtYW5pbWF0aW9uOnNsaWRlLWluIDNzIGN1YmljLWJlemllciguNzcsMCwuMTc1LDEpO2FuaW1hdGlvbjpzbGlkZS1pbiAzcyBjdWJpYy1iZXppZXIoLjc3LDAsLjE3NSwxKX0uZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LWVycm9yLW1hcmssLmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1zdWNjZXNzLW1hcmt7cG9pbnRlci1ldmVudHM6bm9uZTtvcGFjaXR5OjA7ei1pbmRleDo1MDA7cG9zaXRpb246YWJzb2x1dGU7ZGlzcGxheTpibG9jazt0b3A6NTAlO2xlZnQ6NTAlO21hcmdpbi1sZWZ0Oi0yN3B4O21hcmdpbi10b3A6LTI3cHh9LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1lcnJvci1tYXJrIHN2ZywuZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LXN1Y2Nlc3MtbWFyayBzdmd7ZGlzcGxheTpibG9jazt3aWR0aDo1NHB4O2hlaWdodDo1NHB4fS5kcm9wem9uZSAuZHotcHJldmlldy5kei1wcm9jZXNzaW5nIC5kei1wcm9ncmVzc3tvcGFjaXR5OjE7dHJhbnNpdGlvbjphbGwgLjJzIGxpbmVhcn0uZHJvcHpvbmUgLmR6LXByZXZpZXcuZHotY29tcGxldGUgLmR6LXByb2dyZXNze29wYWNpdHk6MDt0cmFuc2l0aW9uOm9wYWNpdHkgLjRzIGVhc2UtaW59LmRyb3B6b25lIC5kei1wcmV2aWV3Om5vdCguZHotcHJvY2Vzc2luZykgLmR6LXByb2dyZXNzey13ZWJraXQtYW5pbWF0aW9uOnB1bHNlIDZzIGVhc2UgaW5maW5pdGU7YW5pbWF0aW9uOnB1bHNlIDZzIGVhc2UgaW5maW5pdGV9LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1wcm9ncmVzc3tvcGFjaXR5OjE7ei1pbmRleDoxMDAwO3BvaW50ZXItZXZlbnRzOm5vbmU7cG9zaXRpb246YWJzb2x1dGU7aGVpZ2h0OjE2cHg7bGVmdDo1MCU7dG9wOjUwJTttYXJnaW4tdG9wOi04cHg7d2lkdGg6ODBweDttYXJnaW4tbGVmdDotNDBweDtiYWNrZ3JvdW5kOmhzbGEoMCwwJSwxMDAlLC45KTstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgxKTtib3JkZXItcmFkaXVzOjhweDtvdmVyZmxvdzpoaWRkZW59LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1wcm9ncmVzcyAuZHotdXBsb2Fke2JhY2tncm91bmQ6IzMzMztiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCgxODBkZWcsIzY2NiwjNDQ0KTtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MDtsZWZ0OjA7Ym90dG9tOjA7d2lkdGg6MDt0cmFuc2l0aW9uOndpZHRoIC4zcyBlYXNlLWluLW91dH0uZHJvcHpvbmUgLmR6LXByZXZpZXcuZHotZXJyb3IgLmR6LWVycm9yLW1lc3NhZ2V7ZGlzcGxheTpibG9ja30uZHJvcHpvbmUgLmR6LXByZXZpZXcuZHotZXJyb3I6aG92ZXIgLmR6LWVycm9yLW1lc3NhZ2V7b3BhY2l0eToxO3BvaW50ZXItZXZlbnRzOmF1dG99LmRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1lcnJvci1tZXNzYWdle3BvaW50ZXItZXZlbnRzOm5vbmU7ei1pbmRleDoxMDAwO3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6YmxvY2s7ZGlzcGxheTpub25lO29wYWNpdHk6MDt0cmFuc2l0aW9uOm9wYWNpdHkgLjNzIGVhc2U7Ym9yZGVyLXJhZGl1czo4cHg7Zm9udC1zaXplOjEzcHg7dG9wOjEzMHB4O2xlZnQ6LTEwcHg7d2lkdGg6MTQwcHg7YmFja2dyb3VuZDojYmUyNjI2O2JhY2tncm91bmQ6bGluZWFyLWdyYWRpZW50KDE4MGRlZywjYmUyNjI2LCNhOTIyMjIpO3BhZGRpbmc6LjVlbSAxLjJlbTtjb2xvcjojZmZmfS5kcm9wem9uZSAuZHotcHJldmlldyAuZHotZXJyb3ItbWVzc2FnZTphZnRlcntjb250ZW50OlwiXCI7cG9zaXRpb246YWJzb2x1dGU7dG9wOi02cHg7bGVmdDo2NHB4O3dpZHRoOjA7aGVpZ2h0OjA7Ym9yZGVyLWxlZnQ6NnB4IHNvbGlkIHRyYW5zcGFyZW50O2JvcmRlci1yaWdodDo2cHggc29saWQgdHJhbnNwYXJlbnQ7Ym9yZGVyLWJvdHRvbTo2cHggc29saWQgI2JlMjYyNn0nLFwiXCJdKX0sZnVuY3Rpb24oZSx0LGkpe3Q9ZS5leHBvcnRzPWkoMSkoKSx0LmkoaSgzKSxcIlwiKSx0LnB1c2goW2UuaWQsXCIudnVlLWRyb3B6b25le2JvcmRlcjoycHggc29saWQgI2U1ZTVlNTtmb250LWZhbWlseTpBcmlhbCxzYW5zLXNlcmlmO2xldHRlci1zcGFjaW5nOi4ycHg7Y29sb3I6Izc3Nzt0cmFuc2l0aW9uOmJhY2tncm91bmQtY29sb3IgLjJzIGxpbmVhcn0udnVlLWRyb3B6b25lOmhvdmVye2JhY2tncm91bmQtY29sb3I6I2Y2ZjZmNn0udnVlLWRyb3B6b25lIGl7Y29sb3I6I2NjY30udnVlLWRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1pbWFnZXtib3JkZXItcmFkaXVzOjB9LnZ1ZS1kcm9wem9uZSAuZHotcHJldmlldyAuZHotaW1hZ2U6aG92ZXIgaW1ney13ZWJraXQtdHJhbnNmb3JtOm5vbmU7dHJhbnNmb3JtOm5vbmU7LXdlYmtpdC1maWx0ZXI6bm9uZX0udnVlLWRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1kZXRhaWxze2JvdHRvbTowO3RvcDowO2NvbG9yOiNmZmY7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDMzLDE1MCwyNDMsLjgpO3RyYW5zaXRpb246b3BhY2l0eSAuMnMgbGluZWFyO3RleHQtYWxpZ246bGVmdH0udnVlLWRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1kZXRhaWxzIC5kei1maWxlbmFtZSBzcGFuLC52dWUtZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LWRldGFpbHMgLmR6LXNpemUgc3BhbntiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50fS52dWUtZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LWRldGFpbHMgLmR6LWZpbGVuYW1lOm5vdCg6aG92ZXIpIHNwYW57Ym9yZGVyOm5vbmV9LnZ1ZS1kcm9wem9uZSAuZHotcHJldmlldyAuZHotZGV0YWlscyAuZHotZmlsZW5hbWU6aG92ZXIgc3BhbntiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2JvcmRlcjpub25lfS52dWUtZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LXByb2dyZXNzIC5kei11cGxvYWR7YmFja2dyb3VuZDojY2NjfS52dWUtZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LXJlbW92ZXtwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4OjMwO2NvbG9yOiNmZmY7bWFyZ2luLWxlZnQ6MTVweDtwYWRkaW5nOjEwcHg7dG9wOmluaGVyaXQ7Ym90dG9tOjE1cHg7Ym9yZGVyOjJweCBzb2xpZCAjZmZmO3RleHQtZGVjb3JhdGlvbjpub25lO3RleHQtdHJhbnNmb3JtOnVwcGVyY2FzZTtmb250LXNpemU6LjhyZW07Zm9udC13ZWlnaHQ6ODAwO2xldHRlci1zcGFjaW5nOjEuMXB4O29wYWNpdHk6MH0udnVlLWRyb3B6b25lIC5kei1wcmV2aWV3OmhvdmVyIC5kei1yZW1vdmV7b3BhY2l0eToxfS52dWUtZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LWVycm9yLW1hcmssLnZ1ZS1kcm9wem9uZSAuZHotcHJldmlldyAuZHotc3VjY2Vzcy1tYXJre21hcmdpbi1sZWZ0OmF1dG8haW1wb3J0YW50O21hcmdpbi10b3A6YXV0byFpbXBvcnRhbnQ7d2lkdGg6MTAwJSFpbXBvcnRhbnQ7dG9wOjM1JSFpbXBvcnRhbnQ7bGVmdDowfS52dWUtZHJvcHpvbmUgLmR6LXByZXZpZXcgLmR6LWVycm9yLW1hcmsgaSwudnVlLWRyb3B6b25lIC5kei1wcmV2aWV3IC5kei1zdWNjZXNzLW1hcmsgaXtjb2xvcjojZmZmIWltcG9ydGFudDtmb250LXNpemU6NXJlbSFpbXBvcnRhbnR9LnZ1ZS1kcm9wem9uZSAuZHotcHJldmlldyAuZHotZXJyb3ItbWVzc2FnZXt0b3A6NzUlO2xlZnQ6MTUlfVwiLFwiXCJdKX0sZnVuY3Rpb24oZSx0LGkpeyhmdW5jdGlvbihlKXsoZnVuY3Rpb24oKXt2YXIgdCxpLG4sbyxyLHMsYSxsLGQscD1bXS5zbGljZSx1PWZ1bmN0aW9uKGUsdCl7ZnVuY3Rpb24gaSgpe3RoaXMuY29uc3RydWN0b3I9ZX1mb3IodmFyIG4gaW4gdCljLmNhbGwodCxuKSYmKGVbbl09dFtuXSk7cmV0dXJuIGkucHJvdG90eXBlPXQucHJvdG90eXBlLGUucHJvdG90eXBlPW5ldyBpLGUuX19zdXBlcl9fPXQucHJvdG90eXBlLGV9LGM9e30uaGFzT3duUHJvcGVydHk7bD1mdW5jdGlvbigpe30saT1mdW5jdGlvbigpe2Z1bmN0aW9uIGUoKXt9cmV0dXJuIGUucHJvdG90eXBlLmFkZEV2ZW50TGlzdGVuZXI9ZS5wcm90b3R5cGUub24sZS5wcm90b3R5cGUub249ZnVuY3Rpb24oZSx0KXtyZXR1cm4gdGhpcy5fY2FsbGJhY2tzPXRoaXMuX2NhbGxiYWNrc3x8e30sdGhpcy5fY2FsbGJhY2tzW2VdfHwodGhpcy5fY2FsbGJhY2tzW2VdPVtdKSx0aGlzLl9jYWxsYmFja3NbZV0ucHVzaCh0KSx0aGlzfSxlLnByb3RvdHlwZS5lbWl0PWZ1bmN0aW9uKCl7dmFyIGUsdCxpLG4sbyxyO2lmKG49YXJndW1lbnRzWzBdLGU9Mjw9YXJndW1lbnRzLmxlbmd0aD9wLmNhbGwoYXJndW1lbnRzLDEpOltdLHRoaXMuX2NhbGxiYWNrcz10aGlzLl9jYWxsYmFja3N8fHt9LGk9dGhpcy5fY2FsbGJhY2tzW25dKWZvcihvPTAscj1pLmxlbmd0aDtvPHI7bysrKXQ9aVtvXSx0LmFwcGx5KHRoaXMsZSk7cmV0dXJuIHRoaXN9LGUucHJvdG90eXBlLnJlbW92ZUxpc3RlbmVyPWUucHJvdG90eXBlLm9mZixlLnByb3RvdHlwZS5yZW1vdmVBbGxMaXN0ZW5lcnM9ZS5wcm90b3R5cGUub2ZmLGUucHJvdG90eXBlLnJlbW92ZUV2ZW50TGlzdGVuZXI9ZS5wcm90b3R5cGUub2ZmLGUucHJvdG90eXBlLm9mZj1mdW5jdGlvbihlLHQpe3ZhciBpLG4sbyxyLHM7aWYoIXRoaXMuX2NhbGxiYWNrc3x8MD09PWFyZ3VtZW50cy5sZW5ndGgpcmV0dXJuIHRoaXMuX2NhbGxiYWNrcz17fSx0aGlzO2lmKG49dGhpcy5fY2FsbGJhY2tzW2VdLCFuKXJldHVybiB0aGlzO2lmKDE9PT1hcmd1bWVudHMubGVuZ3RoKXJldHVybiBkZWxldGUgdGhpcy5fY2FsbGJhY2tzW2VdLHRoaXM7Zm9yKG89cj0wLHM9bi5sZW5ndGg7cjxzO289KytyKWlmKGk9bltvXSxpPT09dCl7bi5zcGxpY2UobywxKTticmVha31yZXR1cm4gdGhpc30sZX0oKSx0PWZ1bmN0aW9uKGUpe2Z1bmN0aW9uIHQoZSxpKXt2YXIgbixyLHM7aWYodGhpcy5lbGVtZW50PWUsdGhpcy52ZXJzaW9uPXQudmVyc2lvbix0aGlzLmRlZmF1bHRPcHRpb25zLnByZXZpZXdUZW1wbGF0ZT10aGlzLmRlZmF1bHRPcHRpb25zLnByZXZpZXdUZW1wbGF0ZS5yZXBsYWNlKC9cXG4qL2csXCJcIiksdGhpcy5jbGlja2FibGVFbGVtZW50cz1bXSx0aGlzLmxpc3RlbmVycz1bXSx0aGlzLmZpbGVzPVtdLFwic3RyaW5nXCI9PXR5cGVvZiB0aGlzLmVsZW1lbnQmJih0aGlzLmVsZW1lbnQ9ZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0aGlzLmVsZW1lbnQpKSwhdGhpcy5lbGVtZW50fHxudWxsPT10aGlzLmVsZW1lbnQubm9kZVR5cGUpdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCBkcm9wem9uZSBlbGVtZW50LlwiKTtpZih0aGlzLmVsZW1lbnQuZHJvcHpvbmUpdGhyb3cgbmV3IEVycm9yKFwiRHJvcHpvbmUgYWxyZWFkeSBhdHRhY2hlZC5cIik7aWYodC5pbnN0YW5jZXMucHVzaCh0aGlzKSx0aGlzLmVsZW1lbnQuZHJvcHpvbmU9dGhpcyxuPW51bGwhPShzPXQub3B0aW9uc0ZvckVsZW1lbnQodGhpcy5lbGVtZW50KSk/czp7fSx0aGlzLm9wdGlvbnM9byh7fSx0aGlzLmRlZmF1bHRPcHRpb25zLG4sbnVsbCE9aT9pOnt9KSx0aGlzLm9wdGlvbnMuZm9yY2VGYWxsYmFja3x8IXQuaXNCcm93c2VyU3VwcG9ydGVkKCkpcmV0dXJuIHRoaXMub3B0aW9ucy5mYWxsYmFjay5jYWxsKHRoaXMpO2lmKG51bGw9PXRoaXMub3B0aW9ucy51cmwmJih0aGlzLm9wdGlvbnMudXJsPXRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJhY3Rpb25cIikpLCF0aGlzLm9wdGlvbnMudXJsKXRocm93IG5ldyBFcnJvcihcIk5vIFVSTCBwcm92aWRlZC5cIik7aWYodGhpcy5vcHRpb25zLmFjY2VwdGVkRmlsZXMmJnRoaXMub3B0aW9ucy5hY2NlcHRlZE1pbWVUeXBlcyl0aHJvdyBuZXcgRXJyb3IoXCJZb3UgY2FuJ3QgcHJvdmlkZSBib3RoICdhY2NlcHRlZEZpbGVzJyBhbmQgJ2FjY2VwdGVkTWltZVR5cGVzJy4gJ2FjY2VwdGVkTWltZVR5cGVzJyBpcyBkZXByZWNhdGVkLlwiKTt0aGlzLm9wdGlvbnMuYWNjZXB0ZWRNaW1lVHlwZXMmJih0aGlzLm9wdGlvbnMuYWNjZXB0ZWRGaWxlcz10aGlzLm9wdGlvbnMuYWNjZXB0ZWRNaW1lVHlwZXMsZGVsZXRlIHRoaXMub3B0aW9ucy5hY2NlcHRlZE1pbWVUeXBlcyksbnVsbCE9dGhpcy5vcHRpb25zLnJlbmFtZUZpbGVuYW1lJiYodGhpcy5vcHRpb25zLnJlbmFtZUZpbGU9ZnVuY3Rpb24oZSl7cmV0dXJuIGZ1bmN0aW9uKHQpe3JldHVybiBlLm9wdGlvbnMucmVuYW1lRmlsZW5hbWUuY2FsbChlLHQubmFtZSx0KX19KHRoaXMpKSx0aGlzLm9wdGlvbnMubWV0aG9kPXRoaXMub3B0aW9ucy5tZXRob2QudG9VcHBlckNhc2UoKSwocj10aGlzLmdldEV4aXN0aW5nRmFsbGJhY2soKSkmJnIucGFyZW50Tm9kZSYmci5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHIpLHRoaXMub3B0aW9ucy5wcmV2aWV3c0NvbnRhaW5lciE9PSExJiYodGhpcy5vcHRpb25zLnByZXZpZXdzQ29udGFpbmVyP3RoaXMucHJldmlld3NDb250YWluZXI9dC5nZXRFbGVtZW50KHRoaXMub3B0aW9ucy5wcmV2aWV3c0NvbnRhaW5lcixcInByZXZpZXdzQ29udGFpbmVyXCIpOnRoaXMucHJldmlld3NDb250YWluZXI9dGhpcy5lbGVtZW50KSx0aGlzLm9wdGlvbnMuY2xpY2thYmxlJiYodGhpcy5vcHRpb25zLmNsaWNrYWJsZT09PSEwP3RoaXMuY2xpY2thYmxlRWxlbWVudHM9W3RoaXMuZWxlbWVudF06dGhpcy5jbGlja2FibGVFbGVtZW50cz10LmdldEVsZW1lbnRzKHRoaXMub3B0aW9ucy5jbGlja2FibGUsXCJjbGlja2FibGVcIikpLHRoaXMuaW5pdCgpfXZhciBvLHI7cmV0dXJuIHUodCxlKSx0LnByb3RvdHlwZS5FbWl0dGVyPWksdC5wcm90b3R5cGUuZXZlbnRzPVtcImRyb3BcIixcImRyYWdzdGFydFwiLFwiZHJhZ2VuZFwiLFwiZHJhZ2VudGVyXCIsXCJkcmFnb3ZlclwiLFwiZHJhZ2xlYXZlXCIsXCJhZGRlZGZpbGVcIixcImFkZGVkZmlsZXNcIixcInJlbW92ZWRmaWxlXCIsXCJ0aHVtYm5haWxcIixcImVycm9yXCIsXCJlcnJvcm11bHRpcGxlXCIsXCJwcm9jZXNzaW5nXCIsXCJwcm9jZXNzaW5nbXVsdGlwbGVcIixcInVwbG9hZHByb2dyZXNzXCIsXCJ0b3RhbHVwbG9hZHByb2dyZXNzXCIsXCJzZW5kaW5nXCIsXCJzZW5kaW5nbXVsdGlwbGVcIixcInN1Y2Nlc3NcIixcInN1Y2Nlc3NtdWx0aXBsZVwiLFwiY2FuY2VsZWRcIixcImNhbmNlbGVkbXVsdGlwbGVcIixcImNvbXBsZXRlXCIsXCJjb21wbGV0ZW11bHRpcGxlXCIsXCJyZXNldFwiLFwibWF4ZmlsZXNleGNlZWRlZFwiLFwibWF4ZmlsZXNyZWFjaGVkXCIsXCJxdWV1ZWNvbXBsZXRlXCJdLHQucHJvdG90eXBlLmRlZmF1bHRPcHRpb25zPXt1cmw6bnVsbCxtZXRob2Q6XCJwb3N0XCIsd2l0aENyZWRlbnRpYWxzOiExLHRpbWVvdXQ6M2U0LHBhcmFsbGVsVXBsb2FkczoyLHVwbG9hZE11bHRpcGxlOiExLG1heEZpbGVzaXplOjI1NixwYXJhbU5hbWU6XCJmaWxlXCIsY3JlYXRlSW1hZ2VUaHVtYm5haWxzOiEwLG1heFRodW1ibmFpbEZpbGVzaXplOjEwLHRodW1ibmFpbFdpZHRoOjEyMCx0aHVtYm5haWxIZWlnaHQ6MTIwLHRodW1ibmFpbE1ldGhvZDpcImNyb3BcIixyZXNpemVXaWR0aDpudWxsLHJlc2l6ZUhlaWdodDpudWxsLHJlc2l6ZU1pbWVUeXBlOm51bGwscmVzaXplUXVhbGl0eTouOCxyZXNpemVNZXRob2Q6XCJjb250YWluXCIsZmlsZXNpemVCYXNlOjFlMyxtYXhGaWxlczpudWxsLHBhcmFtczp7fSxoZWFkZXJzOm51bGwsY2xpY2thYmxlOiEwLGlnbm9yZUhpZGRlbkZpbGVzOiEwLGFjY2VwdGVkRmlsZXM6bnVsbCxhY2NlcHRlZE1pbWVUeXBlczpudWxsLGF1dG9Qcm9jZXNzUXVldWU6ITAsYXV0b1F1ZXVlOiEwLGFkZFJlbW92ZUxpbmtzOiExLHByZXZpZXdzQ29udGFpbmVyOm51bGwsaGlkZGVuSW5wdXRDb250YWluZXI6XCJib2R5XCIsY2FwdHVyZTpudWxsLHJlbmFtZUZpbGVuYW1lOm51bGwscmVuYW1lRmlsZTpudWxsLGZvcmNlRmFsbGJhY2s6ITEsZGljdERlZmF1bHRNZXNzYWdlOlwiRHJvcCBmaWxlcyBoZXJlIHRvIHVwbG9hZFwiLGRpY3RGYWxsYmFja01lc3NhZ2U6XCJZb3VyIGJyb3dzZXIgZG9lcyBub3Qgc3VwcG9ydCBkcmFnJ24nZHJvcCBmaWxlIHVwbG9hZHMuXCIsZGljdEZhbGxiYWNrVGV4dDpcIlBsZWFzZSB1c2UgdGhlIGZhbGxiYWNrIGZvcm0gYmVsb3cgdG8gdXBsb2FkIHlvdXIgZmlsZXMgbGlrZSBpbiB0aGUgb2xkZW4gZGF5cy5cIixkaWN0RmlsZVRvb0JpZzpcIkZpbGUgaXMgdG9vIGJpZyAoe3tmaWxlc2l6ZX19TWlCKS4gTWF4IGZpbGVzaXplOiB7e21heEZpbGVzaXplfX1NaUIuXCIsZGljdEludmFsaWRGaWxlVHlwZTpcIllvdSBjYW4ndCB1cGxvYWQgZmlsZXMgb2YgdGhpcyB0eXBlLlwiLGRpY3RSZXNwb25zZUVycm9yOlwiU2VydmVyIHJlc3BvbmRlZCB3aXRoIHt7c3RhdHVzQ29kZX19IGNvZGUuXCIsZGljdENhbmNlbFVwbG9hZDpcIkNhbmNlbCB1cGxvYWRcIixkaWN0Q2FuY2VsVXBsb2FkQ29uZmlybWF0aW9uOlwiQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNhbmNlbCB0aGlzIHVwbG9hZD9cIixkaWN0UmVtb3ZlRmlsZTpcIlJlbW92ZSBmaWxlXCIsZGljdFJlbW92ZUZpbGVDb25maXJtYXRpb246bnVsbCxkaWN0TWF4RmlsZXNFeGNlZWRlZDpcIllvdSBjYW4gbm90IHVwbG9hZCBhbnkgbW9yZSBmaWxlcy5cIixkaWN0RmlsZVNpemVVbml0czp7dGI6XCJUQlwiLGdiOlwiR0JcIixtYjpcIk1CXCIsa2I6XCJLQlwiLGI6XCJiXCJ9LGluaXQ6ZnVuY3Rpb24oKXtyZXR1cm4gbH0sYWNjZXB0OmZ1bmN0aW9uKGUsdCl7cmV0dXJuIHQoKX0sZmFsbGJhY2s6ZnVuY3Rpb24oKXt2YXIgZSxpLG4sbyxyLHM7Zm9yKHRoaXMuZWxlbWVudC5jbGFzc05hbWU9dGhpcy5lbGVtZW50LmNsYXNzTmFtZStcIiBkei1icm93c2VyLW5vdC1zdXBwb3J0ZWRcIixyPXRoaXMuZWxlbWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImRpdlwiKSxpPTAsbj1yLmxlbmd0aDtpPG47aSsrKWU9cltpXSwvKF58IClkei1tZXNzYWdlKCR8ICkvLnRlc3QoZS5jbGFzc05hbWUpJiYobz1lLGUuY2xhc3NOYW1lPVwiZHotbWVzc2FnZVwiKTtyZXR1cm4gb3x8KG89dC5jcmVhdGVFbGVtZW50KCc8ZGl2IGNsYXNzPVwiZHotbWVzc2FnZVwiPjxzcGFuPjwvc3Bhbj48L2Rpdj4nKSx0aGlzLmVsZW1lbnQuYXBwZW5kQ2hpbGQobykpLHM9by5nZXRFbGVtZW50c0J5VGFnTmFtZShcInNwYW5cIilbMF0scyYmKG51bGwhPXMudGV4dENvbnRlbnQ/cy50ZXh0Q29udGVudD10aGlzLm9wdGlvbnMuZGljdEZhbGxiYWNrTWVzc2FnZTpudWxsIT1zLmlubmVyVGV4dCYmKHMuaW5uZXJUZXh0PXRoaXMub3B0aW9ucy5kaWN0RmFsbGJhY2tNZXNzYWdlKSksdGhpcy5lbGVtZW50LmFwcGVuZENoaWxkKHRoaXMuZ2V0RmFsbGJhY2tGb3JtKCkpfSxyZXNpemU6ZnVuY3Rpb24oZSx0LGksbil7dmFyIG8scixzO2lmKG89e3NyY1g6MCxzcmNZOjAsc3JjV2lkdGg6ZS53aWR0aCxzcmNIZWlnaHQ6ZS5oZWlnaHR9LHI9ZS53aWR0aC9lLmhlaWdodCxudWxsPT10JiZudWxsPT1pPyh0PW8uc3JjV2lkdGgsaT1vLnNyY0hlaWdodCk6bnVsbD09dD90PWkqcjpudWxsPT1pJiYoaT10L3IpLHQ9TWF0aC5taW4odCxvLnNyY1dpZHRoKSxpPU1hdGgubWluKGksby5zcmNIZWlnaHQpLHM9dC9pLG8uc3JjV2lkdGg+dHx8by5zcmNIZWlnaHQ+aSlpZihcImNyb3BcIj09PW4pcj5zPyhvLnNyY0hlaWdodD1lLmhlaWdodCxvLnNyY1dpZHRoPW8uc3JjSGVpZ2h0KnMpOihvLnNyY1dpZHRoPWUud2lkdGgsby5zcmNIZWlnaHQ9by5zcmNXaWR0aC9zKTtlbHNle2lmKFwiY29udGFpblwiIT09bil0aHJvdyBuZXcgRXJyb3IoXCJVbmtub3duIHJlc2l6ZU1ldGhvZCAnXCIrbitcIidcIik7cj5zP2k9dC9yOnQ9aSpyfXJldHVybiBvLnNyY1g9KGUud2lkdGgtby5zcmNXaWR0aCkvMixvLnNyY1k9KGUuaGVpZ2h0LW8uc3JjSGVpZ2h0KS8yLG8udHJnV2lkdGg9dCxvLnRyZ0hlaWdodD1pLG99LHRyYW5zZm9ybUZpbGU6ZnVuY3Rpb24oZSx0KXtyZXR1cm4odGhpcy5vcHRpb25zLnJlc2l6ZVdpZHRofHx0aGlzLm9wdGlvbnMucmVzaXplSGVpZ2h0KSYmZS50eXBlLm1hdGNoKC9pbWFnZS4qLyk/dGhpcy5yZXNpemVJbWFnZShlLHRoaXMub3B0aW9ucy5yZXNpemVXaWR0aCx0aGlzLm9wdGlvbnMucmVzaXplSGVpZ2h0LHRoaXMub3B0aW9ucy5yZXNpemVNZXRob2QsdCk6dChlKX0scHJldmlld1RlbXBsYXRlOic8ZGl2IGNsYXNzPVwiZHotcHJldmlldyBkei1maWxlLXByZXZpZXdcIj5cXG4gIDxkaXYgY2xhc3M9XCJkei1pbWFnZVwiPjxpbWcgZGF0YS1kei10aHVtYm5haWwgLz48L2Rpdj5cXG4gIDxkaXYgY2xhc3M9XCJkei1kZXRhaWxzXCI+XFxuICAgIDxkaXYgY2xhc3M9XCJkei1zaXplXCI+PHNwYW4gZGF0YS1kei1zaXplPjwvc3Bhbj48L2Rpdj5cXG4gICAgPGRpdiBjbGFzcz1cImR6LWZpbGVuYW1lXCI+PHNwYW4gZGF0YS1kei1uYW1lPjwvc3Bhbj48L2Rpdj5cXG4gIDwvZGl2PlxcbiAgPGRpdiBjbGFzcz1cImR6LXByb2dyZXNzXCI+PHNwYW4gY2xhc3M9XCJkei11cGxvYWRcIiBkYXRhLWR6LXVwbG9hZHByb2dyZXNzPjwvc3Bhbj48L2Rpdj5cXG4gIDxkaXYgY2xhc3M9XCJkei1lcnJvci1tZXNzYWdlXCI+PHNwYW4gZGF0YS1kei1lcnJvcm1lc3NhZ2U+PC9zcGFuPjwvZGl2PlxcbiAgPGRpdiBjbGFzcz1cImR6LXN1Y2Nlc3MtbWFya1wiPlxcbiAgICA8c3ZnIHdpZHRoPVwiNTRweFwiIGhlaWdodD1cIjU0cHhcIiB2aWV3Qm94PVwiMCAwIDU0IDU0XCIgdmVyc2lvbj1cIjEuMVwiIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB4bWxuczp4bGluaz1cImh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmtcIiB4bWxuczpza2V0Y2g9XCJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnNcIj5cXG4gICAgICA8dGl0bGU+Q2hlY2s8L3RpdGxlPlxcbiAgICAgIDxkZWZzPjwvZGVmcz5cXG4gICAgICA8ZyBpZD1cIlBhZ2UtMVwiIHN0cm9rZT1cIm5vbmVcIiBzdHJva2Utd2lkdGg9XCIxXCIgZmlsbD1cIm5vbmVcIiBmaWxsLXJ1bGU9XCJldmVub2RkXCIgc2tldGNoOnR5cGU9XCJNU1BhZ2VcIj5cXG4gICAgICAgIDxwYXRoIGQ9XCJNMjMuNSwzMS44NDMxNDU4IEwxNy41ODUyNDE5LDI1LjkyODM4NzcgQzE2LjAyNDgyNTMsMjQuMzY3OTcxMSAxMy40OTEwMjk0LDI0LjM2NjgzNSAxMS45Mjg5MzIyLDI1LjkyODkzMjIgQzEwLjM3MDAxMzYsMjcuNDg3ODUwOCAxMC4zNjY1OTEyLDMwLjAyMzQ0NTUgMTEuOTI4Mzg3NywzMS41ODUyNDE5IEwyMC40MTQ3NTgxLDQwLjA3MTYxMjMgQzIwLjUxMzM5OTksNDAuMTcwMjU0MSAyMC42MTU5MzE1LDQwLjI2MjY2NDkgMjAuNzIxODYxNSw0MC4zNDg4NDM1IEMyMi4yODM1NjY5LDQxLjg3MjU2NTEgMjQuNzk0MjM0LDQxLjg2MjYyMDIgMjYuMzQ2MTU2NCw0MC4zMTA2OTc4IEw0My4zMTA2OTc4LDIzLjM0NjE1NjQgQzQ0Ljg3NzEwMjEsMjEuNzc5NzUyMSA0NC44NzU4MDU3LDE5LjI0ODM4ODcgNDMuMzEzNzA4NSwxNy42ODYyOTE1IEM0MS43NTQ3ODk5LDE2LjEyNzM3MjkgMzkuMjE3NjAzNSwxNi4xMjU1NDIyIDM3LjY1Mzg0MzYsMTcuNjg5MzAyMiBMMjMuNSwzMS44NDMxNDU4IFogTTI3LDUzIEM0MS4zNTk0MDM1LDUzIDUzLDQxLjM1OTQwMzUgNTMsMjcgQzUzLDEyLjY0MDU5NjUgNDEuMzU5NDAzNSwxIDI3LDEgQzEyLjY0MDU5NjUsMSAxLDEyLjY0MDU5NjUgMSwyNyBDMSw0MS4zNTk0MDM1IDEyLjY0MDU5NjUsNTMgMjcsNTMgWlwiIGlkPVwiT3ZhbC0yXCIgc3Ryb2tlLW9wYWNpdHk9XCIwLjE5ODc5NDE1OFwiIHN0cm9rZT1cIiM3NDc0NzRcIiBmaWxsLW9wYWNpdHk9XCIwLjgxNjUxOTQ3NVwiIGZpbGw9XCIjRkZGRkZGXCIgc2tldGNoOnR5cGU9XCJNU1NoYXBlR3JvdXBcIj48L3BhdGg+XFxuICAgICAgPC9nPlxcbiAgICA8L3N2Zz5cXG4gIDwvZGl2PlxcbiAgPGRpdiBjbGFzcz1cImR6LWVycm9yLW1hcmtcIj5cXG4gICAgPHN2ZyB3aWR0aD1cIjU0cHhcIiBoZWlnaHQ9XCI1NHB4XCIgdmlld0JveD1cIjAgMCA1NCA1NFwiIHZlcnNpb249XCIxLjFcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIgeG1sbnM6eGxpbms9XCJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rXCIgeG1sbnM6c2tldGNoPVwiaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zXCI+XFxuICAgICAgPHRpdGxlPkVycm9yPC90aXRsZT5cXG4gICAgICA8ZGVmcz48L2RlZnM+XFxuICAgICAgPGcgaWQ9XCJQYWdlLTFcIiBzdHJva2U9XCJub25lXCIgc3Ryb2tlLXdpZHRoPVwiMVwiIGZpbGw9XCJub25lXCIgZmlsbC1ydWxlPVwiZXZlbm9kZFwiIHNrZXRjaDp0eXBlPVwiTVNQYWdlXCI+XFxuICAgICAgICA8ZyBpZD1cIkNoZWNrLSstT3ZhbC0yXCIgc2tldGNoOnR5cGU9XCJNU0xheWVyR3JvdXBcIiBzdHJva2U9XCIjNzQ3NDc0XCIgc3Ryb2tlLW9wYWNpdHk9XCIwLjE5ODc5NDE1OFwiIGZpbGw9XCIjRkZGRkZGXCIgZmlsbC1vcGFjaXR5PVwiMC44MTY1MTk0NzVcIj5cXG4gICAgICAgICAgPHBhdGggZD1cIk0zMi42NTY4NTQyLDI5IEwzOC4zMTA2OTc4LDIzLjM0NjE1NjQgQzM5Ljg3NzEwMjEsMjEuNzc5NzUyMSAzOS44NzU4MDU3LDE5LjI0ODM4ODcgMzguMzEzNzA4NSwxNy42ODYyOTE1IEMzNi43NTQ3ODk5LDE2LjEyNzM3MjkgMzQuMjE3NjAzNSwxNi4xMjU1NDIyIDMyLjY1Mzg0MzYsMTcuNjg5MzAyMiBMMjcsMjMuMzQzMTQ1OCBMMjEuMzQ2MTU2NCwxNy42ODkzMDIyIEMxOS43ODIzOTY1LDE2LjEyNTU0MjIgMTcuMjQ1MjEwMSwxNi4xMjczNzI5IDE1LjY4NjI5MTUsMTcuNjg2MjkxNSBDMTQuMTI0MTk0MywxOS4yNDgzODg3IDE0LjEyMjg5NzksMjEuNzc5NzUyMSAxNS42ODkzMDIyLDIzLjM0NjE1NjQgTDIxLjM0MzE0NTgsMjkgTDE1LjY4OTMwMjIsMzQuNjUzODQzNiBDMTQuMTIyODk3OSwzNi4yMjAyNDc5IDE0LjEyNDE5NDMsMzguNzUxNjExMyAxNS42ODYyOTE1LDQwLjMxMzcwODUgQzE3LjI0NTIxMDEsNDEuODcyNjI3MSAxOS43ODIzOTY1LDQxLjg3NDQ1NzggMjEuMzQ2MTU2NCw0MC4zMTA2OTc4IEwyNywzNC42NTY4NTQyIEwzMi42NTM4NDM2LDQwLjMxMDY5NzggQzM0LjIxNzYwMzUsNDEuODc0NDU3OCAzNi43NTQ3ODk5LDQxLjg3MjYyNzEgMzguMzEzNzA4NSw0MC4zMTM3MDg1IEMzOS44NzU4MDU3LDM4Ljc1MTYxMTMgMzkuODc3MTAyMSwzNi4yMjAyNDc5IDM4LjMxMDY5NzgsMzQuNjUzODQzNiBMMzIuNjU2ODU0MiwyOSBaIE0yNyw1MyBDNDEuMzU5NDAzNSw1MyA1Myw0MS4zNTk0MDM1IDUzLDI3IEM1MywxMi42NDA1OTY1IDQxLjM1OTQwMzUsMSAyNywxIEMxMi42NDA1OTY1LDEgMSwxMi42NDA1OTY1IDEsMjcgQzEsNDEuMzU5NDAzNSAxMi42NDA1OTY1LDUzIDI3LDUzIFpcIiBpZD1cIk92YWwtMlwiIHNrZXRjaDp0eXBlPVwiTVNTaGFwZUdyb3VwXCI+PC9wYXRoPlxcbiAgICAgICAgPC9nPlxcbiAgICAgIDwvZz5cXG4gICAgPC9zdmc+XFxuICA8L2Rpdj5cXG48L2Rpdj4nLGRyb3A6ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwiZHotZHJhZy1ob3ZlclwiKX0sZHJhZ3N0YXJ0OmwsZHJhZ2VuZDpmdW5jdGlvbihlKXtyZXR1cm4gdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJkei1kcmFnLWhvdmVyXCIpfSxkcmFnZW50ZXI6ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotZHJhZy1ob3ZlclwiKX0sZHJhZ292ZXI6ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotZHJhZy1ob3ZlclwiKX0sZHJhZ2xlYXZlOmZ1bmN0aW9uKGUpe3JldHVybiB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImR6LWRyYWctaG92ZXJcIil9LHBhc3RlOmwscmVzZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJkei1zdGFydGVkXCIpfSxhZGRlZGZpbGU6ZnVuY3Rpb24oZSl7dmFyIGksbixvLHIscyxhLGwsZCxwLHUsYyxoLGY7aWYodGhpcy5lbGVtZW50PT09dGhpcy5wcmV2aWV3c0NvbnRhaW5lciYmdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJkei1zdGFydGVkXCIpLHRoaXMucHJldmlld3NDb250YWluZXIpe2ZvcihlLnByZXZpZXdFbGVtZW50PXQuY3JlYXRlRWxlbWVudCh0aGlzLm9wdGlvbnMucHJldmlld1RlbXBsYXRlLnRyaW0oKSksZS5wcmV2aWV3VGVtcGxhdGU9ZS5wcmV2aWV3RWxlbWVudCx0aGlzLnByZXZpZXdzQ29udGFpbmVyLmFwcGVuZENoaWxkKGUucHJldmlld0VsZW1lbnQpLGQ9ZS5wcmV2aWV3RWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtZHotbmFtZV1cIiksaT0wLHI9ZC5sZW5ndGg7aTxyO2krKylsPWRbaV0sbC50ZXh0Q29udGVudD1lLm5hbWU7Zm9yKHA9ZS5wcmV2aWV3RWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtZHotc2l6ZV1cIiksbj0wLHM9cC5sZW5ndGg7bjxzO24rKylsPXBbbl0sbC5pbm5lckhUTUw9dGhpcy5maWxlc2l6ZShlLnNpemUpO2Zvcih0aGlzLm9wdGlvbnMuYWRkUmVtb3ZlTGlua3MmJihlLl9yZW1vdmVMaW5rPXQuY3JlYXRlRWxlbWVudCgnPGEgY2xhc3M9XCJkei1yZW1vdmVcIiBocmVmPVwiamF2YXNjcmlwdDp1bmRlZmluZWQ7XCIgZGF0YS1kei1yZW1vdmU+Jyt0aGlzLm9wdGlvbnMuZGljdFJlbW92ZUZpbGUrXCI8L2E+XCIpLGUucHJldmlld0VsZW1lbnQuYXBwZW5kQ2hpbGQoZS5fcmVtb3ZlTGluaykpLGM9ZnVuY3Rpb24oaSl7cmV0dXJuIGZ1bmN0aW9uKG4pe3JldHVybiBuLnByZXZlbnREZWZhdWx0KCksbi5zdG9wUHJvcGFnYXRpb24oKSxlLnN0YXR1cz09PXQuVVBMT0FESU5HP3QuY29uZmlybShpLm9wdGlvbnMuZGljdENhbmNlbFVwbG9hZENvbmZpcm1hdGlvbixmdW5jdGlvbigpe3JldHVybiBpLnJlbW92ZUZpbGUoZSl9KTppLm9wdGlvbnMuZGljdFJlbW92ZUZpbGVDb25maXJtYXRpb24/dC5jb25maXJtKGkub3B0aW9ucy5kaWN0UmVtb3ZlRmlsZUNvbmZpcm1hdGlvbixmdW5jdGlvbigpe3JldHVybiBpLnJlbW92ZUZpbGUoZSl9KTppLnJlbW92ZUZpbGUoZSl9fSh0aGlzKSx1PWUucHJldmlld0VsZW1lbnQucXVlcnlTZWxlY3RvckFsbChcIltkYXRhLWR6LXJlbW92ZV1cIiksZj1bXSxvPTAsYT11Lmxlbmd0aDtvPGE7bysrKWg9dVtvXSxmLnB1c2goaC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIixjKSk7cmV0dXJuIGZ9fSxyZW1vdmVkZmlsZTpmdW5jdGlvbihlKXt2YXIgdDtyZXR1cm4gZS5wcmV2aWV3RWxlbWVudCYmbnVsbCE9KHQ9ZS5wcmV2aWV3RWxlbWVudCkmJnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChlLnByZXZpZXdFbGVtZW50KSx0aGlzLl91cGRhdGVNYXhGaWxlc1JlYWNoZWRDbGFzcygpfSx0aHVtYm5haWw6ZnVuY3Rpb24oZSx0KXt2YXIgaSxuLG8scjtpZihlLnByZXZpZXdFbGVtZW50KXtmb3IoZS5wcmV2aWV3RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwiZHotZmlsZS1wcmV2aWV3XCIpLG89ZS5wcmV2aWV3RWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtZHotdGh1bWJuYWlsXVwiKSxpPTAsbj1vLmxlbmd0aDtpPG47aSsrKXI9b1tpXSxyLmFsdD1lLm5hbWUsci5zcmM9dDtyZXR1cm4gc2V0VGltZW91dChmdW5jdGlvbih0KXtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4gZS5wcmV2aWV3RWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotaW1hZ2UtcHJldmlld1wiKX19KHRoaXMpLDEpfX0sZXJyb3I6ZnVuY3Rpb24oZSx0KXt2YXIgaSxuLG8scixzO2lmKGUucHJldmlld0VsZW1lbnQpe2ZvcihlLnByZXZpZXdFbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJkei1lcnJvclwiKSxcIlN0cmluZ1wiIT10eXBlb2YgdCYmdC5lcnJvciYmKHQ9dC5lcnJvcikscj1lLnByZXZpZXdFbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbZGF0YS1kei1lcnJvcm1lc3NhZ2VdXCIpLHM9W10saT0wLG49ci5sZW5ndGg7aTxuO2krKylvPXJbaV0scy5wdXNoKG8udGV4dENvbnRlbnQ9dCk7cmV0dXJuIHN9fSxlcnJvcm11bHRpcGxlOmwscHJvY2Vzc2luZzpmdW5jdGlvbihlKXtpZihlLnByZXZpZXdFbGVtZW50JiYoZS5wcmV2aWV3RWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotcHJvY2Vzc2luZ1wiKSxlLl9yZW1vdmVMaW5rKSlyZXR1cm4gZS5fcmVtb3ZlTGluay50ZXh0Q29udGVudD10aGlzLm9wdGlvbnMuZGljdENhbmNlbFVwbG9hZH0scHJvY2Vzc2luZ211bHRpcGxlOmwsdXBsb2FkcHJvZ3Jlc3M6ZnVuY3Rpb24oZSx0LGkpe3ZhciBuLG8scixzLGE7aWYoZS5wcmV2aWV3RWxlbWVudCl7Zm9yKHM9ZS5wcmV2aWV3RWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtZHotdXBsb2FkcHJvZ3Jlc3NdXCIpLGE9W10sbj0wLG89cy5sZW5ndGg7bjxvO24rKylyPXNbbl0sXCJQUk9HUkVTU1wiPT09ci5ub2RlTmFtZT9hLnB1c2goci52YWx1ZT10KTphLnB1c2goci5zdHlsZS53aWR0aD10K1wiJVwiKTtyZXR1cm4gYX19LHRvdGFsdXBsb2FkcHJvZ3Jlc3M6bCxzZW5kaW5nOmwsc2VuZGluZ211bHRpcGxlOmwsc3VjY2VzczpmdW5jdGlvbihlKXtpZihlLnByZXZpZXdFbGVtZW50KXJldHVybiBlLnByZXZpZXdFbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJkei1zdWNjZXNzXCIpfSxzdWNjZXNzbXVsdGlwbGU6bCxjYW5jZWxlZDpmdW5jdGlvbihlKXtyZXR1cm4gdGhpcy5lbWl0KFwiZXJyb3JcIixlLFwiVXBsb2FkIGNhbmNlbGVkLlwiKX0sY2FuY2VsZWRtdWx0aXBsZTpsLGNvbXBsZXRlOmZ1bmN0aW9uKGUpe2lmKGUuX3JlbW92ZUxpbmsmJihlLl9yZW1vdmVMaW5rLnRleHRDb250ZW50PXRoaXMub3B0aW9ucy5kaWN0UmVtb3ZlRmlsZSksZS5wcmV2aWV3RWxlbWVudClyZXR1cm4gZS5wcmV2aWV3RWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotY29tcGxldGVcIil9LGNvbXBsZXRlbXVsdGlwbGU6bCxtYXhmaWxlc2V4Y2VlZGVkOmwsbWF4ZmlsZXNyZWFjaGVkOmwscXVldWVjb21wbGV0ZTpsLGFkZGVkZmlsZXM6bH0sbz1mdW5jdGlvbigpe3ZhciBlLHQsaSxuLG8scixzO2ZvcihyPWFyZ3VtZW50c1swXSxvPTI8PWFyZ3VtZW50cy5sZW5ndGg/cC5jYWxsKGFyZ3VtZW50cywxKTpbXSxlPTAsaT1vLmxlbmd0aDtlPGk7ZSsrKXtuPW9bZV07Zm9yKHQgaW4gbilzPW5bdF0sclt0XT1zfXJldHVybiByfSx0LnByb3RvdHlwZS5nZXRBY2NlcHRlZEZpbGVzPWZ1bmN0aW9uKCl7dmFyIGUsdCxpLG4sbztmb3Iobj10aGlzLmZpbGVzLG89W10sdD0wLGk9bi5sZW5ndGg7dDxpO3QrKyllPW5bdF0sZS5hY2NlcHRlZCYmby5wdXNoKGUpO3JldHVybiBvfSx0LnByb3RvdHlwZS5nZXRSZWplY3RlZEZpbGVzPWZ1bmN0aW9uKCl7dmFyIGUsdCxpLG4sbztmb3Iobj10aGlzLmZpbGVzLG89W10sdD0wLGk9bi5sZW5ndGg7dDxpO3QrKyllPW5bdF0sZS5hY2NlcHRlZHx8by5wdXNoKGUpO3JldHVybiBvfSx0LnByb3RvdHlwZS5nZXRGaWxlc1dpdGhTdGF0dXM9ZnVuY3Rpb24oZSl7dmFyIHQsaSxuLG8scjtmb3Iobz10aGlzLmZpbGVzLHI9W10saT0wLG49by5sZW5ndGg7aTxuO2krKyl0PW9baV0sdC5zdGF0dXM9PT1lJiZyLnB1c2godCk7cmV0dXJuIHJ9LHQucHJvdG90eXBlLmdldFF1ZXVlZEZpbGVzPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZ2V0RmlsZXNXaXRoU3RhdHVzKHQuUVVFVUVEKX0sdC5wcm90b3R5cGUuZ2V0VXBsb2FkaW5nRmlsZXM9ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5nZXRGaWxlc1dpdGhTdGF0dXModC5VUExPQURJTkcpfSx0LnByb3RvdHlwZS5nZXRBZGRlZEZpbGVzPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZ2V0RmlsZXNXaXRoU3RhdHVzKHQuQURERUQpfSx0LnByb3RvdHlwZS5nZXRBY3RpdmVGaWxlcz1mdW5jdGlvbigpe3ZhciBlLGksbixvLHI7Zm9yKG89dGhpcy5maWxlcyxyPVtdLGk9MCxuPW8ubGVuZ3RoO2k8bjtpKyspZT1vW2ldLGUuc3RhdHVzIT09dC5VUExPQURJTkcmJmUuc3RhdHVzIT09dC5RVUVVRUR8fHIucHVzaChlKTtyZXR1cm4gcn0sdC5wcm90b3R5cGUuaW5pdD1mdW5jdGlvbigpe3ZhciBlLGksbixvLHIscyxhO2ZvcihcImZvcm1cIj09PXRoaXMuZWxlbWVudC50YWdOYW1lJiZ0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFwiZW5jdHlwZVwiLFwibXVsdGlwYXJ0L2Zvcm0tZGF0YVwiKSx0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKFwiZHJvcHpvbmVcIikmJiF0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihcIi5kei1tZXNzYWdlXCIpJiZ0aGlzLmVsZW1lbnQuYXBwZW5kQ2hpbGQodC5jcmVhdGVFbGVtZW50KCc8ZGl2IGNsYXNzPVwiZHotZGVmYXVsdCBkei1tZXNzYWdlXCI+PHNwYW4+Jyt0aGlzLm9wdGlvbnMuZGljdERlZmF1bHRNZXNzYWdlK1wiPC9zcGFuPjwvZGl2PlwiKSksdGhpcy5jbGlja2FibGVFbGVtZW50cy5sZW5ndGgmJihhPWZ1bmN0aW9uKGUpe3JldHVybiBmdW5jdGlvbigpe3JldHVybiBlLmhpZGRlbkZpbGVJbnB1dCYmZS5oaWRkZW5GaWxlSW5wdXQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChlLmhpZGRlbkZpbGVJbnB1dCksZS5oaWRkZW5GaWxlSW5wdXQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImlucHV0XCIpLGUuaGlkZGVuRmlsZUlucHV0LnNldEF0dHJpYnV0ZShcInR5cGVcIixcImZpbGVcIiksKG51bGw9PWUub3B0aW9ucy5tYXhGaWxlc3x8ZS5vcHRpb25zLm1heEZpbGVzPjEpJiZlLmhpZGRlbkZpbGVJbnB1dC5zZXRBdHRyaWJ1dGUoXCJtdWx0aXBsZVwiLFwibXVsdGlwbGVcIiksZS5oaWRkZW5GaWxlSW5wdXQuY2xhc3NOYW1lPVwiZHotaGlkZGVuLWlucHV0XCIsbnVsbCE9ZS5vcHRpb25zLmFjY2VwdGVkRmlsZXMmJmUuaGlkZGVuRmlsZUlucHV0LnNldEF0dHJpYnV0ZShcImFjY2VwdFwiLGUub3B0aW9ucy5hY2NlcHRlZEZpbGVzKSxudWxsIT1lLm9wdGlvbnMuY2FwdHVyZSYmZS5oaWRkZW5GaWxlSW5wdXQuc2V0QXR0cmlidXRlKFwiY2FwdHVyZVwiLGUub3B0aW9ucy5jYXB0dXJlKSxcbmUuaGlkZGVuRmlsZUlucHV0LnN0eWxlLnZpc2liaWxpdHk9XCJoaWRkZW5cIixlLmhpZGRlbkZpbGVJbnB1dC5zdHlsZS5wb3NpdGlvbj1cImFic29sdXRlXCIsZS5oaWRkZW5GaWxlSW5wdXQuc3R5bGUudG9wPVwiMFwiLGUuaGlkZGVuRmlsZUlucHV0LnN0eWxlLmxlZnQ9XCIwXCIsZS5oaWRkZW5GaWxlSW5wdXQuc3R5bGUuaGVpZ2h0PVwiMFwiLGUuaGlkZGVuRmlsZUlucHV0LnN0eWxlLndpZHRoPVwiMFwiLGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZS5vcHRpb25zLmhpZGRlbklucHV0Q29udGFpbmVyKS5hcHBlbmRDaGlsZChlLmhpZGRlbkZpbGVJbnB1dCksZS5oaWRkZW5GaWxlSW5wdXQuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLGZ1bmN0aW9uKCl7dmFyIHQsaSxuLG87aWYoaT1lLmhpZGRlbkZpbGVJbnB1dC5maWxlcyxpLmxlbmd0aClmb3Iobj0wLG89aS5sZW5ndGg7bjxvO24rKyl0PWlbbl0sZS5hZGRGaWxlKHQpO3JldHVybiBlLmVtaXQoXCJhZGRlZGZpbGVzXCIsaSksYSgpfSl9fSh0aGlzKSkoKSx0aGlzLlVSTD1udWxsIT0ocj13aW5kb3cuVVJMKT9yOndpbmRvdy53ZWJraXRVUkwscz10aGlzLmV2ZW50cyxpPTAsbj1zLmxlbmd0aDtpPG47aSsrKWU9c1tpXSx0aGlzLm9uKGUsdGhpcy5vcHRpb25zW2VdKTtyZXR1cm4gdGhpcy5vbihcInVwbG9hZHByb2dyZXNzXCIsZnVuY3Rpb24oZSl7cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuIGUudXBkYXRlVG90YWxVcGxvYWRQcm9ncmVzcygpfX0odGhpcykpLHRoaXMub24oXCJyZW1vdmVkZmlsZVwiLGZ1bmN0aW9uKGUpe3JldHVybiBmdW5jdGlvbigpe3JldHVybiBlLnVwZGF0ZVRvdGFsVXBsb2FkUHJvZ3Jlc3MoKX19KHRoaXMpKSx0aGlzLm9uKFwiY2FuY2VsZWRcIixmdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24odCl7cmV0dXJuIGUuZW1pdChcImNvbXBsZXRlXCIsdCl9fSh0aGlzKSksdGhpcy5vbihcImNvbXBsZXRlXCIsZnVuY3Rpb24oZSl7cmV0dXJuIGZ1bmN0aW9uKHQpe2lmKDA9PT1lLmdldEFkZGVkRmlsZXMoKS5sZW5ndGgmJjA9PT1lLmdldFVwbG9hZGluZ0ZpbGVzKCkubGVuZ3RoJiYwPT09ZS5nZXRRdWV1ZWRGaWxlcygpLmxlbmd0aClyZXR1cm4gc2V0VGltZW91dChmdW5jdGlvbigpe3JldHVybiBlLmVtaXQoXCJxdWV1ZWNvbXBsZXRlXCIpfSwwKX19KHRoaXMpKSxvPWZ1bmN0aW9uKGUpe3JldHVybiBlLnN0b3BQcm9wYWdhdGlvbigpLGUucHJldmVudERlZmF1bHQ/ZS5wcmV2ZW50RGVmYXVsdCgpOmUucmV0dXJuVmFsdWU9ITF9LHRoaXMubGlzdGVuZXJzPVt7ZWxlbWVudDp0aGlzLmVsZW1lbnQsZXZlbnRzOntkcmFnc3RhcnQ6ZnVuY3Rpb24oZSl7cmV0dXJuIGZ1bmN0aW9uKHQpe3JldHVybiBlLmVtaXQoXCJkcmFnc3RhcnRcIix0KX19KHRoaXMpLGRyYWdlbnRlcjpmdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24odCl7cmV0dXJuIG8odCksZS5lbWl0KFwiZHJhZ2VudGVyXCIsdCl9fSh0aGlzKSxkcmFnb3ZlcjpmdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24odCl7dmFyIGk7dHJ5e2k9dC5kYXRhVHJhbnNmZXIuZWZmZWN0QWxsb3dlZH1jYXRjaChlKXt9cmV0dXJuIHQuZGF0YVRyYW5zZmVyLmRyb3BFZmZlY3Q9XCJtb3ZlXCI9PT1pfHxcImxpbmtNb3ZlXCI9PT1pP1wibW92ZVwiOlwiY29weVwiLG8odCksZS5lbWl0KFwiZHJhZ292ZXJcIix0KX19KHRoaXMpLGRyYWdsZWF2ZTpmdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24odCl7cmV0dXJuIGUuZW1pdChcImRyYWdsZWF2ZVwiLHQpfX0odGhpcyksZHJvcDpmdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24odCl7cmV0dXJuIG8odCksZS5kcm9wKHQpfX0odGhpcyksZHJhZ2VuZDpmdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24odCl7cmV0dXJuIGUuZW1pdChcImRyYWdlbmRcIix0KX19KHRoaXMpfX1dLHRoaXMuY2xpY2thYmxlRWxlbWVudHMuZm9yRWFjaChmdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24oaSl7cmV0dXJuIGUubGlzdGVuZXJzLnB1c2goe2VsZW1lbnQ6aSxldmVudHM6e2NsaWNrOmZ1bmN0aW9uKG4pe3JldHVybihpIT09ZS5lbGVtZW50fHxuLnRhcmdldD09PWUuZWxlbWVudHx8dC5lbGVtZW50SW5zaWRlKG4udGFyZ2V0LGUuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFwiLmR6LW1lc3NhZ2VcIikpKSYmZS5oaWRkZW5GaWxlSW5wdXQuY2xpY2soKSwhMH19fSl9fSh0aGlzKSksdGhpcy5lbmFibGUoKSx0aGlzLm9wdGlvbnMuaW5pdC5jYWxsKHRoaXMpfSx0LnByb3RvdHlwZS5kZXN0cm95PWZ1bmN0aW9uKCl7dmFyIGU7cmV0dXJuIHRoaXMuZGlzYWJsZSgpLHRoaXMucmVtb3ZlQWxsRmlsZXMoITApLChudWxsIT0oZT10aGlzLmhpZGRlbkZpbGVJbnB1dCk/ZS5wYXJlbnROb2RlOnZvaWQgMCkmJih0aGlzLmhpZGRlbkZpbGVJbnB1dC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHRoaXMuaGlkZGVuRmlsZUlucHV0KSx0aGlzLmhpZGRlbkZpbGVJbnB1dD1udWxsKSxkZWxldGUgdGhpcy5lbGVtZW50LmRyb3B6b25lLHQuaW5zdGFuY2VzLnNwbGljZSh0Lmluc3RhbmNlcy5pbmRleE9mKHRoaXMpLDEpfSx0LnByb3RvdHlwZS51cGRhdGVUb3RhbFVwbG9hZFByb2dyZXNzPWZ1bmN0aW9uKCl7dmFyIGUsdCxpLG4sbyxyLHMsYTtpZihzPTAscj0wLGU9dGhpcy5nZXRBY3RpdmVGaWxlcygpLGUubGVuZ3RoKXtmb3Iobz10aGlzLmdldEFjdGl2ZUZpbGVzKCksaT0wLG49by5sZW5ndGg7aTxuO2krKyl0PW9baV0scys9dC51cGxvYWQuYnl0ZXNTZW50LHIrPXQudXBsb2FkLnRvdGFsO2E9MTAwKnMvcn1lbHNlIGE9MTAwO3JldHVybiB0aGlzLmVtaXQoXCJ0b3RhbHVwbG9hZHByb2dyZXNzXCIsYSxyLHMpfSx0LnByb3RvdHlwZS5fZ2V0UGFyYW1OYW1lPWZ1bmN0aW9uKGUpe3JldHVyblwiZnVuY3Rpb25cIj09dHlwZW9mIHRoaXMub3B0aW9ucy5wYXJhbU5hbWU/dGhpcy5vcHRpb25zLnBhcmFtTmFtZShlKTpcIlwiK3RoaXMub3B0aW9ucy5wYXJhbU5hbWUrKHRoaXMub3B0aW9ucy51cGxvYWRNdWx0aXBsZT9cIltcIitlK1wiXVwiOlwiXCIpfSx0LnByb3RvdHlwZS5fcmVuYW1lRmlsZT1mdW5jdGlvbihlKXtyZXR1cm5cImZ1bmN0aW9uXCIhPXR5cGVvZiB0aGlzLm9wdGlvbnMucmVuYW1lRmlsZT9lLm5hbWU6dGhpcy5vcHRpb25zLnJlbmFtZUZpbGUoZSl9LHQucHJvdG90eXBlLmdldEZhbGxiYWNrRm9ybT1mdW5jdGlvbigpe3ZhciBlLGksbixvO3JldHVybihlPXRoaXMuZ2V0RXhpc3RpbmdGYWxsYmFjaygpKT9lOihuPSc8ZGl2IGNsYXNzPVwiZHotZmFsbGJhY2tcIj4nLHRoaXMub3B0aW9ucy5kaWN0RmFsbGJhY2tUZXh0JiYobis9XCI8cD5cIit0aGlzLm9wdGlvbnMuZGljdEZhbGxiYWNrVGV4dCtcIjwvcD5cIiksbis9JzxpbnB1dCB0eXBlPVwiZmlsZVwiIG5hbWU9XCInK3RoaXMuX2dldFBhcmFtTmFtZSgwKSsnXCIgJysodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlPydtdWx0aXBsZT1cIm11bHRpcGxlXCInOnZvaWQgMCkrJyAvPjxpbnB1dCB0eXBlPVwic3VibWl0XCIgdmFsdWU9XCJVcGxvYWQhXCI+PC9kaXY+JyxpPXQuY3JlYXRlRWxlbWVudChuKSxcIkZPUk1cIiE9PXRoaXMuZWxlbWVudC50YWdOYW1lPyhvPXQuY3JlYXRlRWxlbWVudCgnPGZvcm0gYWN0aW9uPVwiJyt0aGlzLm9wdGlvbnMudXJsKydcIiBlbmN0eXBlPVwibXVsdGlwYXJ0L2Zvcm0tZGF0YVwiIG1ldGhvZD1cIicrdGhpcy5vcHRpb25zLm1ldGhvZCsnXCI+PC9mb3JtPicpLG8uYXBwZW5kQ2hpbGQoaSkpOih0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFwiZW5jdHlwZVwiLFwibXVsdGlwYXJ0L2Zvcm0tZGF0YVwiKSx0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFwibWV0aG9kXCIsdGhpcy5vcHRpb25zLm1ldGhvZCkpLG51bGwhPW8/bzppKX0sdC5wcm90b3R5cGUuZ2V0RXhpc3RpbmdGYWxsYmFjaz1mdW5jdGlvbigpe3ZhciBlLHQsaSxuLG8scjtmb3IodD1mdW5jdGlvbihlKXt2YXIgdCxpLG47Zm9yKGk9MCxuPWUubGVuZ3RoO2k8bjtpKyspaWYodD1lW2ldLC8oXnwgKWZhbGxiYWNrKCR8ICkvLnRlc3QodC5jbGFzc05hbWUpKXJldHVybiB0fSxvPVtcImRpdlwiLFwiZm9ybVwiXSxpPTAsbj1vLmxlbmd0aDtpPG47aSsrKWlmKHI9b1tpXSxlPXQodGhpcy5lbGVtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKHIpKSlyZXR1cm4gZX0sdC5wcm90b3R5cGUuc2V0dXBFdmVudExpc3RlbmVycz1mdW5jdGlvbigpe3ZhciBlLHQsaSxuLG8scixzO2ZvcihyPXRoaXMubGlzdGVuZXJzLHM9W10saT0wLG49ci5sZW5ndGg7aTxuO2krKyllPXJbaV0scy5wdXNoKGZ1bmN0aW9uKCl7dmFyIGksbjtpPWUuZXZlbnRzLG49W107Zm9yKHQgaW4gaSlvPWlbdF0sbi5wdXNoKGUuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKHQsbywhMSkpO3JldHVybiBufSgpKTtyZXR1cm4gc30sdC5wcm90b3R5cGUucmVtb3ZlRXZlbnRMaXN0ZW5lcnM9ZnVuY3Rpb24oKXt2YXIgZSx0LGksbixvLHIscztmb3Iocj10aGlzLmxpc3RlbmVycyxzPVtdLGk9MCxuPXIubGVuZ3RoO2k8bjtpKyspZT1yW2ldLHMucHVzaChmdW5jdGlvbigpe3ZhciBpLG47aT1lLmV2ZW50cyxuPVtdO2Zvcih0IGluIGkpbz1pW3RdLG4ucHVzaChlLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcih0LG8sITEpKTtyZXR1cm4gbn0oKSk7cmV0dXJuIHN9LHQucHJvdG90eXBlLmRpc2FibGU9ZnVuY3Rpb24oKXt2YXIgZSx0LGksbixvO2Zvcih0aGlzLmNsaWNrYWJsZUVsZW1lbnRzLmZvckVhY2goZnVuY3Rpb24oZSl7cmV0dXJuIGUuY2xhc3NMaXN0LnJlbW92ZShcImR6LWNsaWNrYWJsZVwiKX0pLHRoaXMucmVtb3ZlRXZlbnRMaXN0ZW5lcnMoKSxuPXRoaXMuZmlsZXMsbz1bXSx0PTAsaT1uLmxlbmd0aDt0PGk7dCsrKWU9blt0XSxvLnB1c2godGhpcy5jYW5jZWxVcGxvYWQoZSkpO3JldHVybiBvfSx0LnByb3RvdHlwZS5lbmFibGU9ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5jbGlja2FibGVFbGVtZW50cy5mb3JFYWNoKGZ1bmN0aW9uKGUpe3JldHVybiBlLmNsYXNzTGlzdC5hZGQoXCJkei1jbGlja2FibGVcIil9KSx0aGlzLnNldHVwRXZlbnRMaXN0ZW5lcnMoKX0sdC5wcm90b3R5cGUuZmlsZXNpemU9ZnVuY3Rpb24oZSl7dmFyIHQsaSxuLG8scixzLGEsbDtpZihyPTAscz1cImJcIixlPjApe2ZvcihsPVtcInRiXCIsXCJnYlwiLFwibWJcIixcImtiXCIsXCJiXCJdLGk9bj0wLG89bC5sZW5ndGg7bjxvO2k9KytuKWlmKGE9bFtpXSx0PU1hdGgucG93KHRoaXMub3B0aW9ucy5maWxlc2l6ZUJhc2UsNC1pKS8xMCxlPj10KXtyPWUvTWF0aC5wb3codGhpcy5vcHRpb25zLmZpbGVzaXplQmFzZSw0LWkpLHM9YTticmVha31yPU1hdGgucm91bmQoMTAqcikvMTB9cmV0dXJuXCI8c3Ryb25nPlwiK3IrXCI8L3N0cm9uZz4gXCIrdGhpcy5vcHRpb25zLmRpY3RGaWxlU2l6ZVVuaXRzW3NdfSx0LnByb3RvdHlwZS5fdXBkYXRlTWF4RmlsZXNSZWFjaGVkQ2xhc3M9ZnVuY3Rpb24oKXtyZXR1cm4gbnVsbCE9dGhpcy5vcHRpb25zLm1heEZpbGVzJiZ0aGlzLmdldEFjY2VwdGVkRmlsZXMoKS5sZW5ndGg+PXRoaXMub3B0aW9ucy5tYXhGaWxlcz8odGhpcy5nZXRBY2NlcHRlZEZpbGVzKCkubGVuZ3RoPT09dGhpcy5vcHRpb25zLm1heEZpbGVzJiZ0aGlzLmVtaXQoXCJtYXhmaWxlc3JlYWNoZWRcIix0aGlzLmZpbGVzKSx0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LmFkZChcImR6LW1heC1maWxlcy1yZWFjaGVkXCIpKTp0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImR6LW1heC1maWxlcy1yZWFjaGVkXCIpfSx0LnByb3RvdHlwZS5kcm9wPWZ1bmN0aW9uKGUpe3ZhciB0LGk7ZS5kYXRhVHJhbnNmZXImJih0aGlzLmVtaXQoXCJkcm9wXCIsZSksdD1lLmRhdGFUcmFuc2Zlci5maWxlcyx0aGlzLmVtaXQoXCJhZGRlZGZpbGVzXCIsdCksdC5sZW5ndGgmJihpPWUuZGF0YVRyYW5zZmVyLml0ZW1zLGkmJmkubGVuZ3RoJiZudWxsIT1pWzBdLndlYmtpdEdldEFzRW50cnk/dGhpcy5fYWRkRmlsZXNGcm9tSXRlbXMoaSk6dGhpcy5oYW5kbGVGaWxlcyh0KSkpfSx0LnByb3RvdHlwZS5wYXN0ZT1mdW5jdGlvbihlKXt2YXIgdCxpO2lmKG51bGwhPShudWxsIT1lJiZudWxsIT0oaT1lLmNsaXBib2FyZERhdGEpP2kuaXRlbXM6dm9pZCAwKSlyZXR1cm4gdGhpcy5lbWl0KFwicGFzdGVcIixlKSx0PWUuY2xpcGJvYXJkRGF0YS5pdGVtcyx0Lmxlbmd0aD90aGlzLl9hZGRGaWxlc0Zyb21JdGVtcyh0KTp2b2lkIDB9LHQucHJvdG90eXBlLmhhbmRsZUZpbGVzPWZ1bmN0aW9uKGUpe3ZhciB0LGksbixvO2ZvcihvPVtdLGk9MCxuPWUubGVuZ3RoO2k8bjtpKyspdD1lW2ldLG8ucHVzaCh0aGlzLmFkZEZpbGUodCkpO3JldHVybiBvfSx0LnByb3RvdHlwZS5fYWRkRmlsZXNGcm9tSXRlbXM9ZnVuY3Rpb24oZSl7dmFyIHQsaSxuLG8scjtmb3Iocj1bXSxuPTAsbz1lLmxlbmd0aDtuPG87bisrKWk9ZVtuXSxudWxsIT1pLndlYmtpdEdldEFzRW50cnkmJih0PWkud2Via2l0R2V0QXNFbnRyeSgpKT90LmlzRmlsZT9yLnB1c2godGhpcy5hZGRGaWxlKGkuZ2V0QXNGaWxlKCkpKTp0LmlzRGlyZWN0b3J5P3IucHVzaCh0aGlzLl9hZGRGaWxlc0Zyb21EaXJlY3RvcnkodCx0Lm5hbWUpKTpyLnB1c2godm9pZCAwKTpudWxsIT1pLmdldEFzRmlsZSYmKG51bGw9PWkua2luZHx8XCJmaWxlXCI9PT1pLmtpbmQpP3IucHVzaCh0aGlzLmFkZEZpbGUoaS5nZXRBc0ZpbGUoKSkpOnIucHVzaCh2b2lkIDApO3JldHVybiByfSx0LnByb3RvdHlwZS5fYWRkRmlsZXNGcm9tRGlyZWN0b3J5PWZ1bmN0aW9uKGUsdCl7dmFyIGksbixvO3JldHVybiBpPWUuY3JlYXRlUmVhZGVyKCksbj1mdW5jdGlvbihlKXtyZXR1cm5cInVuZGVmaW5lZFwiIT10eXBlb2YgY29uc29sZSYmbnVsbCE9PWNvbnNvbGUmJlwiZnVuY3Rpb25cIj09dHlwZW9mIGNvbnNvbGUubG9nP2NvbnNvbGUubG9nKGUpOnZvaWQgMH0sKG89ZnVuY3Rpb24oZSl7cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuIGkucmVhZEVudHJpZXMoZnVuY3Rpb24oaSl7dmFyIG4scixzO2lmKGkubGVuZ3RoPjApe2ZvcihyPTAscz1pLmxlbmd0aDtyPHM7cisrKW49aVtyXSxuLmlzRmlsZT9uLmZpbGUoZnVuY3Rpb24oaSl7aWYoIWUub3B0aW9ucy5pZ25vcmVIaWRkZW5GaWxlc3x8XCIuXCIhPT1pLm5hbWUuc3Vic3RyaW5nKDAsMSkpcmV0dXJuIGkuZnVsbFBhdGg9dCtcIi9cIitpLm5hbWUsZS5hZGRGaWxlKGkpfSk6bi5pc0RpcmVjdG9yeSYmZS5fYWRkRmlsZXNGcm9tRGlyZWN0b3J5KG4sdCtcIi9cIituLm5hbWUpO28oKX1yZXR1cm4gbnVsbH0sbil9fSh0aGlzKSkoKX0sdC5wcm90b3R5cGUuYWNjZXB0PWZ1bmN0aW9uKGUsaSl7cmV0dXJuIGUuc2l6ZT4xMDI0KnRoaXMub3B0aW9ucy5tYXhGaWxlc2l6ZSoxMDI0P2kodGhpcy5vcHRpb25zLmRpY3RGaWxlVG9vQmlnLnJlcGxhY2UoXCJ7e2ZpbGVzaXplfX1cIixNYXRoLnJvdW5kKGUuc2l6ZS8xMDI0LzEwLjI0KS8xMDApLnJlcGxhY2UoXCJ7e21heEZpbGVzaXplfX1cIix0aGlzLm9wdGlvbnMubWF4RmlsZXNpemUpKTp0LmlzVmFsaWRGaWxlKGUsdGhpcy5vcHRpb25zLmFjY2VwdGVkRmlsZXMpP251bGwhPXRoaXMub3B0aW9ucy5tYXhGaWxlcyYmdGhpcy5nZXRBY2NlcHRlZEZpbGVzKCkubGVuZ3RoPj10aGlzLm9wdGlvbnMubWF4RmlsZXM/KGkodGhpcy5vcHRpb25zLmRpY3RNYXhGaWxlc0V4Y2VlZGVkLnJlcGxhY2UoXCJ7e21heEZpbGVzfX1cIix0aGlzLm9wdGlvbnMubWF4RmlsZXMpKSx0aGlzLmVtaXQoXCJtYXhmaWxlc2V4Y2VlZGVkXCIsZSkpOnRoaXMub3B0aW9ucy5hY2NlcHQuY2FsbCh0aGlzLGUsaSk6aSh0aGlzLm9wdGlvbnMuZGljdEludmFsaWRGaWxlVHlwZSl9LHQucHJvdG90eXBlLmFkZEZpbGU9ZnVuY3Rpb24oZSl7cmV0dXJuIGUudXBsb2FkPXtwcm9ncmVzczowLHRvdGFsOmUuc2l6ZSxieXRlc1NlbnQ6MCxmaWxlbmFtZTp0aGlzLl9yZW5hbWVGaWxlKGUpfSx0aGlzLmZpbGVzLnB1c2goZSksZS5zdGF0dXM9dC5BRERFRCx0aGlzLmVtaXQoXCJhZGRlZGZpbGVcIixlKSx0aGlzLl9lbnF1ZXVlVGh1bWJuYWlsKGUpLHRoaXMuYWNjZXB0KGUsZnVuY3Rpb24odCl7cmV0dXJuIGZ1bmN0aW9uKGkpe3JldHVybiBpPyhlLmFjY2VwdGVkPSExLHQuX2Vycm9yUHJvY2Vzc2luZyhbZV0saSkpOihlLmFjY2VwdGVkPSEwLHQub3B0aW9ucy5hdXRvUXVldWUmJnQuZW5xdWV1ZUZpbGUoZSkpLHQuX3VwZGF0ZU1heEZpbGVzUmVhY2hlZENsYXNzKCl9fSh0aGlzKSl9LHQucHJvdG90eXBlLmVucXVldWVGaWxlcz1mdW5jdGlvbihlKXt2YXIgdCxpLG47Zm9yKGk9MCxuPWUubGVuZ3RoO2k8bjtpKyspdD1lW2ldLHRoaXMuZW5xdWV1ZUZpbGUodCk7cmV0dXJuIG51bGx9LHQucHJvdG90eXBlLmVucXVldWVGaWxlPWZ1bmN0aW9uKGUpe2lmKGUuc3RhdHVzIT09dC5BRERFRHx8ZS5hY2NlcHRlZCE9PSEwKXRocm93IG5ldyBFcnJvcihcIlRoaXMgZmlsZSBjYW4ndCBiZSBxdWV1ZWQgYmVjYXVzZSBpdCBoYXMgYWxyZWFkeSBiZWVuIHByb2Nlc3NlZCBvciB3YXMgcmVqZWN0ZWQuXCIpO2lmKGUuc3RhdHVzPXQuUVVFVUVELHRoaXMub3B0aW9ucy5hdXRvUHJvY2Vzc1F1ZXVlKXJldHVybiBzZXRUaW1lb3V0KGZ1bmN0aW9uKGUpe3JldHVybiBmdW5jdGlvbigpe3JldHVybiBlLnByb2Nlc3NRdWV1ZSgpfX0odGhpcyksMCl9LHQucHJvdG90eXBlLl90aHVtYm5haWxRdWV1ZT1bXSx0LnByb3RvdHlwZS5fcHJvY2Vzc2luZ1RodW1ibmFpbD0hMSx0LnByb3RvdHlwZS5fZW5xdWV1ZVRodW1ibmFpbD1mdW5jdGlvbihlKXtpZih0aGlzLm9wdGlvbnMuY3JlYXRlSW1hZ2VUaHVtYm5haWxzJiZlLnR5cGUubWF0Y2goL2ltYWdlLiovKSYmZS5zaXplPD0xMDI0KnRoaXMub3B0aW9ucy5tYXhUaHVtYm5haWxGaWxlc2l6ZSoxMDI0KXJldHVybiB0aGlzLl90aHVtYm5haWxRdWV1ZS5wdXNoKGUpLHNldFRpbWVvdXQoZnVuY3Rpb24oZSl7cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuIGUuX3Byb2Nlc3NUaHVtYm5haWxRdWV1ZSgpfX0odGhpcyksMCl9LHQucHJvdG90eXBlLl9wcm9jZXNzVGh1bWJuYWlsUXVldWU9ZnVuY3Rpb24oKXt2YXIgZTtpZighdGhpcy5fcHJvY2Vzc2luZ1RodW1ibmFpbCYmMCE9PXRoaXMuX3RodW1ibmFpbFF1ZXVlLmxlbmd0aClyZXR1cm4gdGhpcy5fcHJvY2Vzc2luZ1RodW1ibmFpbD0hMCxlPXRoaXMuX3RodW1ibmFpbFF1ZXVlLnNoaWZ0KCksdGhpcy5jcmVhdGVUaHVtYm5haWwoZSx0aGlzLm9wdGlvbnMudGh1bWJuYWlsV2lkdGgsdGhpcy5vcHRpb25zLnRodW1ibmFpbEhlaWdodCx0aGlzLm9wdGlvbnMudGh1bWJuYWlsTWV0aG9kLCEwLGZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbihpKXtyZXR1cm4gdC5lbWl0KFwidGh1bWJuYWlsXCIsZSxpKSx0Ll9wcm9jZXNzaW5nVGh1bWJuYWlsPSExLHQuX3Byb2Nlc3NUaHVtYm5haWxRdWV1ZSgpfX0odGhpcykpfSx0LnByb3RvdHlwZS5yZW1vdmVGaWxlPWZ1bmN0aW9uKGUpe2lmKGUuc3RhdHVzPT09dC5VUExPQURJTkcmJnRoaXMuY2FuY2VsVXBsb2FkKGUpLHRoaXMuZmlsZXM9ZCh0aGlzLmZpbGVzLGUpLHRoaXMuZW1pdChcInJlbW92ZWRmaWxlXCIsZSksMD09PXRoaXMuZmlsZXMubGVuZ3RoKXJldHVybiB0aGlzLmVtaXQoXCJyZXNldFwiKX0sdC5wcm90b3R5cGUucmVtb3ZlQWxsRmlsZXM9ZnVuY3Rpb24oZSl7dmFyIGksbixvLHI7Zm9yKG51bGw9PWUmJihlPSExKSxyPXRoaXMuZmlsZXMuc2xpY2UoKSxuPTAsbz1yLmxlbmd0aDtuPG87bisrKWk9cltuXSwoaS5zdGF0dXMhPT10LlVQTE9BRElOR3x8ZSkmJnRoaXMucmVtb3ZlRmlsZShpKTtyZXR1cm4gbnVsbH0sdC5wcm90b3R5cGUucmVzaXplSW1hZ2U9ZnVuY3Rpb24oZSxpLG8scixzKXtyZXR1cm4gdGhpcy5jcmVhdGVUaHVtYm5haWwoZSxpLG8sciwhMSxmdW5jdGlvbihpKXtyZXR1cm4gZnVuY3Rpb24obyxyKXt2YXIgYSxsO3JldHVybiBudWxsPT09cj9zKGUpOihhPWkub3B0aW9ucy5yZXNpemVNaW1lVHlwZSxudWxsPT1hJiYoYT1lLnR5cGUpLGw9ci50b0RhdGFVUkwoYSxpLm9wdGlvbnMucmVzaXplUXVhbGl0eSksXCJpbWFnZS9qcGVnXCIhPT1hJiZcImltYWdlL2pwZ1wiIT09YXx8KGw9bi5yZXN0b3JlKGUuZGF0YVVSTCxsKSkscyh0LmRhdGFVUkl0b0Jsb2IobCkpKX19KHRoaXMpKX0sdC5wcm90b3R5cGUuY3JlYXRlVGh1bWJuYWlsPWZ1bmN0aW9uKGUsdCxpLG4sbyxyKXt2YXIgcztyZXR1cm4gcz1uZXcgRmlsZVJlYWRlcixzLm9ubG9hZD1mdW5jdGlvbihhKXtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4gZS5kYXRhVVJMPXMucmVzdWx0LFwiaW1hZ2Uvc3ZnK3htbFwiPT09ZS50eXBlP3ZvaWQobnVsbCE9ciYmcihzLnJlc3VsdCkpOmEuY3JlYXRlVGh1bWJuYWlsRnJvbVVybChlLHQsaSxuLG8scil9fSh0aGlzKSxzLnJlYWRBc0RhdGFVUkwoZSl9LHQucHJvdG90eXBlLmNyZWF0ZVRodW1ibmFpbEZyb21Vcmw9ZnVuY3Rpb24oZSx0LGksbixvLHIscyl7dmFyIGw7cmV0dXJuIGw9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImltZ1wiKSxzJiYobC5jcm9zc09yaWdpbj1zKSxsLm9ubG9hZD1mdW5jdGlvbihzKXtyZXR1cm4gZnVuY3Rpb24oKXt2YXIgZDtyZXR1cm4gZD1mdW5jdGlvbihlKXtyZXR1cm4gZSgxKX0sXCJ1bmRlZmluZWRcIiE9dHlwZW9mIEVYSUYmJm51bGwhPT1FWElGJiZvJiYoZD1mdW5jdGlvbihlKXtyZXR1cm4gRVhJRi5nZXREYXRhKGwsZnVuY3Rpb24oKXtyZXR1cm4gZShFWElGLmdldFRhZyh0aGlzLFwiT3JpZW50YXRpb25cIikpfSl9KSxkKGZ1bmN0aW9uKG8pe3ZhciBkLHAsdSxjLGgsZixtLGc7c3dpdGNoKGUud2lkdGg9bC53aWR0aCxlLmhlaWdodD1sLmhlaWdodCxtPXMub3B0aW9ucy5yZXNpemUuY2FsbChzLGUsdCxpLG4pLGQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImNhbnZhc1wiKSxwPWQuZ2V0Q29udGV4dChcIjJkXCIpLGQud2lkdGg9bS50cmdXaWR0aCxkLmhlaWdodD1tLnRyZ0hlaWdodCxvPjQmJihkLndpZHRoPW0udHJnSGVpZ2h0LGQuaGVpZ2h0PW0udHJnV2lkdGgpLG8pe2Nhc2UgMjpwLnRyYW5zbGF0ZShkLndpZHRoLDApLHAuc2NhbGUoLTEsMSk7YnJlYWs7Y2FzZSAzOnAudHJhbnNsYXRlKGQud2lkdGgsZC5oZWlnaHQpLHAucm90YXRlKE1hdGguUEkpO2JyZWFrO2Nhc2UgNDpwLnRyYW5zbGF0ZSgwLGQuaGVpZ2h0KSxwLnNjYWxlKDEsLTEpO2JyZWFrO2Nhc2UgNTpwLnJvdGF0ZSguNSpNYXRoLlBJKSxwLnNjYWxlKDEsLTEpO2JyZWFrO2Nhc2UgNjpwLnJvdGF0ZSguNSpNYXRoLlBJKSxwLnRyYW5zbGF0ZSgwLC1kLmhlaWdodCk7YnJlYWs7Y2FzZSA3OnAucm90YXRlKC41Kk1hdGguUEkpLHAudHJhbnNsYXRlKGQud2lkdGgsLWQuaGVpZ2h0KSxwLnNjYWxlKC0xLDEpO2JyZWFrO2Nhc2UgODpwLnJvdGF0ZSgtLjUqTWF0aC5QSSkscC50cmFuc2xhdGUoLWQud2lkdGgsMCl9aWYoYShwLGwsbnVsbCE9KHU9bS5zcmNYKT91OjAsbnVsbCE9KGM9bS5zcmNZKT9jOjAsbS5zcmNXaWR0aCxtLnNyY0hlaWdodCxudWxsIT0oaD1tLnRyZ1gpP2g6MCxudWxsIT0oZj1tLnRyZ1kpP2Y6MCxtLnRyZ1dpZHRoLG0udHJnSGVpZ2h0KSxnPWQudG9EYXRhVVJMKFwiaW1hZ2UvcG5nXCIpLG51bGwhPXIpcmV0dXJuIHIoZyxkKX0pfX0odGhpcyksbnVsbCE9ciYmKGwub25lcnJvcj1yKSxsLnNyYz1lLmRhdGFVUkx9LHQucHJvdG90eXBlLnByb2Nlc3NRdWV1ZT1mdW5jdGlvbigpe3ZhciBlLHQsaSxuO2lmKHQ9dGhpcy5vcHRpb25zLnBhcmFsbGVsVXBsb2FkcyxpPXRoaXMuZ2V0VXBsb2FkaW5nRmlsZXMoKS5sZW5ndGgsZT1pLCEoaT49dCkmJihuPXRoaXMuZ2V0UXVldWVkRmlsZXMoKSxuLmxlbmd0aD4wKSl7aWYodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlKXJldHVybiB0aGlzLnByb2Nlc3NGaWxlcyhuLnNsaWNlKDAsdC1pKSk7Zm9yKDtlPHQ7KXtpZighbi5sZW5ndGgpcmV0dXJuO3RoaXMucHJvY2Vzc0ZpbGUobi5zaGlmdCgpKSxlKyt9fX0sdC5wcm90b3R5cGUucHJvY2Vzc0ZpbGU9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMucHJvY2Vzc0ZpbGVzKFtlXSl9LHQucHJvdG90eXBlLnByb2Nlc3NGaWxlcz1mdW5jdGlvbihlKXt2YXIgaSxuLG87Zm9yKG49MCxvPWUubGVuZ3RoO248bztuKyspaT1lW25dLGkucHJvY2Vzc2luZz0hMCxpLnN0YXR1cz10LlVQTE9BRElORyx0aGlzLmVtaXQoXCJwcm9jZXNzaW5nXCIsaSk7cmV0dXJuIHRoaXMub3B0aW9ucy51cGxvYWRNdWx0aXBsZSYmdGhpcy5lbWl0KFwicHJvY2Vzc2luZ211bHRpcGxlXCIsZSksdGhpcy51cGxvYWRGaWxlcyhlKX0sdC5wcm90b3R5cGUuX2dldEZpbGVzV2l0aFhocj1mdW5jdGlvbihlKXt2YXIgdCxpO3JldHVybiBpPWZ1bmN0aW9uKCl7dmFyIGksbixvLHI7Zm9yKG89dGhpcy5maWxlcyxyPVtdLGk9MCxuPW8ubGVuZ3RoO2k8bjtpKyspdD1vW2ldLHQueGhyPT09ZSYmci5wdXNoKHQpO3JldHVybiByfS5jYWxsKHRoaXMpfSx0LnByb3RvdHlwZS5jYW5jZWxVcGxvYWQ9ZnVuY3Rpb24oZSl7dmFyIGksbixvLHIscyxhLGw7aWYoZS5zdGF0dXM9PT10LlVQTE9BRElORyl7Zm9yKG49dGhpcy5fZ2V0RmlsZXNXaXRoWGhyKGUueGhyKSxvPTAscz1uLmxlbmd0aDtvPHM7bysrKWk9bltvXSxpLnN0YXR1cz10LkNBTkNFTEVEO2ZvcihlLnhoci5hYm9ydCgpLHI9MCxhPW4ubGVuZ3RoO3I8YTtyKyspaT1uW3JdLHRoaXMuZW1pdChcImNhbmNlbGVkXCIsaSk7dGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlJiZ0aGlzLmVtaXQoXCJjYW5jZWxlZG11bHRpcGxlXCIsbil9ZWxzZShsPWUuc3RhdHVzKSE9PXQuQURERUQmJmwhPT10LlFVRVVFRHx8KGUuc3RhdHVzPXQuQ0FOQ0VMRUQsdGhpcy5lbWl0KFwiY2FuY2VsZWRcIixlKSx0aGlzLm9wdGlvbnMudXBsb2FkTXVsdGlwbGUmJnRoaXMuZW1pdChcImNhbmNlbGVkbXVsdGlwbGVcIixbZV0pKTtpZih0aGlzLm9wdGlvbnMuYXV0b1Byb2Nlc3NRdWV1ZSlyZXR1cm4gdGhpcy5wcm9jZXNzUXVldWUoKX0scj1mdW5jdGlvbigpe3ZhciBlLHQ7cmV0dXJuIHQ9YXJndW1lbnRzWzBdLGU9Mjw9YXJndW1lbnRzLmxlbmd0aD9wLmNhbGwoYXJndW1lbnRzLDEpOltdLFwiZnVuY3Rpb25cIj09dHlwZW9mIHQ/dC5hcHBseSh0aGlzLGUpOnR9LHQucHJvdG90eXBlLnVwbG9hZEZpbGU9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMudXBsb2FkRmlsZXMoW2VdKX0sdC5wcm90b3R5cGUudXBsb2FkRmlsZXM9ZnVuY3Rpb24oZSl7dmFyIGksbixzLGEsbCxkLHAsdSxjLGgsZixtLGcsdix6LGIseSx3LEYseCxrLEUsQyxULFMsTSxBLEwsSSxPLFUsTixQLFIsRCxfLEI7Zm9yKEI9bmV3IFhNTEh0dHBSZXF1ZXN0LGc9MCx5PWUubGVuZ3RoO2c8eTtnKyspcz1lW2ddLHMueGhyPUI7RT1yKHRoaXMub3B0aW9ucy5tZXRob2QsZSksRD1yKHRoaXMub3B0aW9ucy51cmwsZSksQi5vcGVuKEUsRCwhMCksQi50aW1lb3V0PXIodGhpcy5vcHRpb25zLnRpbWVvdXQsZSksQi53aXRoQ3JlZGVudGlhbHM9ISF0aGlzLm9wdGlvbnMud2l0aENyZWRlbnRpYWxzLE49bnVsbCxsPWZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbigpe3ZhciBpLG4sbztmb3Iobz1bXSxpPTAsbj1lLmxlbmd0aDtpPG47aSsrKXM9ZVtpXSxvLnB1c2godC5fZXJyb3JQcm9jZXNzaW5nKGUsTnx8dC5vcHRpb25zLmRpY3RSZXNwb25zZUVycm9yLnJlcGxhY2UoXCJ7e3N0YXR1c0NvZGV9fVwiLEIuc3RhdHVzKSxCKSk7cmV0dXJuIG99fSh0aGlzKSxSPWZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbihpKXt2YXIgbixvLHIsYSxsLGQscCx1LGM7aWYobnVsbCE9aSlmb3IodT0xMDAqaS5sb2FkZWQvaS50b3RhbCxvPTAsYT1lLmxlbmd0aDtvPGE7bysrKXM9ZVtvXSxzLnVwbG9hZC5wcm9ncmVzcz11LHMudXBsb2FkLnRvdGFsPWkudG90YWwscy51cGxvYWQuYnl0ZXNTZW50PWkubG9hZGVkO2Vsc2V7Zm9yKG49ITAsdT0xMDAscj0wLGw9ZS5sZW5ndGg7cjxsO3IrKylzPWVbcl0sMTAwPT09cy51cGxvYWQucHJvZ3Jlc3MmJnMudXBsb2FkLmJ5dGVzU2VudD09PXMudXBsb2FkLnRvdGFsfHwobj0hMSkscy51cGxvYWQucHJvZ3Jlc3M9dSxzLnVwbG9hZC5ieXRlc1NlbnQ9cy51cGxvYWQudG90YWw7aWYobilyZXR1cm59Zm9yKGM9W10scD0wLGQ9ZS5sZW5ndGg7cDxkO3ArKylzPWVbcF0sYy5wdXNoKHQuZW1pdChcInVwbG9hZHByb2dyZXNzXCIscyx1LHMudXBsb2FkLmJ5dGVzU2VudCkpO3JldHVybiBjfX0odGhpcyksQi5vbmxvYWQ9ZnVuY3Rpb24oaSl7cmV0dXJuIGZ1bmN0aW9uKG4pe3ZhciBvO2lmKGVbMF0uc3RhdHVzIT09dC5DQU5DRUxFRCYmND09PUIucmVhZHlTdGF0ZSl7aWYoXCJhcnJheWJ1ZmZlclwiIT09Qi5yZXNwb25zZVR5cGUmJlwiYmxvYlwiIT09Qi5yZXNwb25zZVR5cGUmJihOPUIucmVzcG9uc2VUZXh0LEIuZ2V0UmVzcG9uc2VIZWFkZXIoXCJjb250ZW50LXR5cGVcIikmJn5CLmdldFJlc3BvbnNlSGVhZGVyKFwiY29udGVudC10eXBlXCIpLmluZGV4T2YoXCJhcHBsaWNhdGlvbi9qc29uXCIpKSl0cnl7Tj1KU09OLnBhcnNlKE4pfWNhdGNoKGUpe249ZSxOPVwiSW52YWxpZCBKU09OIHJlc3BvbnNlIGZyb20gc2VydmVyLlwifXJldHVybiBSKCksMjAwPD0obz1CLnN0YXR1cykmJm88MzAwP2kuX2ZpbmlzaGVkKGUsTixuKTpsKCl9fX0odGhpcyksQi5vbmVycm9yPWZ1bmN0aW9uKGkpe3JldHVybiBmdW5jdGlvbigpe2lmKGVbMF0uc3RhdHVzIT09dC5DQU5DRUxFRClyZXR1cm4gbCgpfX0odGhpcyksUz1udWxsIT0oTT1CLnVwbG9hZCk/TTpCLFMub25wcm9ncmVzcz1SLHU9e0FjY2VwdDpcImFwcGxpY2F0aW9uL2pzb25cIixcIkNhY2hlLUNvbnRyb2xcIjpcIm5vLWNhY2hlXCIsXCJYLVJlcXVlc3RlZC1XaXRoXCI6XCJYTUxIdHRwUmVxdWVzdFwifSx0aGlzLm9wdGlvbnMuaGVhZGVycyYmbyh1LHRoaXMub3B0aW9ucy5oZWFkZXJzKTtmb3IoZCBpbiB1KXA9dVtkXSxwJiZCLnNldFJlcXVlc3RIZWFkZXIoZCxwKTtpZihhPW5ldyBGb3JtRGF0YSx0aGlzLm9wdGlvbnMucGFyYW1zKXtBPXRoaXMub3B0aW9ucy5wYXJhbXM7Zm9yKHogaW4gQSlfPUFbel0sYS5hcHBlbmQoeixfKX1mb3Iodj0wLHc9ZS5sZW5ndGg7djx3O3YrKylzPWVbdl0sdGhpcy5lbWl0KFwic2VuZGluZ1wiLHMsQixhKTtpZih0aGlzLm9wdGlvbnMudXBsb2FkTXVsdGlwbGUmJnRoaXMuZW1pdChcInNlbmRpbmdtdWx0aXBsZVwiLGUsQixhKSxcIkZPUk1cIj09PXRoaXMuZWxlbWVudC50YWdOYW1lKWZvcihMPXRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiaW5wdXQsIHRleHRhcmVhLCBzZWxlY3QsIGJ1dHRvblwiKSxiPTAsRj1MLmxlbmd0aDtiPEY7YisrKWlmKGg9TFtiXSxmPWguZ2V0QXR0cmlidXRlKFwibmFtZVwiKSxtPWguZ2V0QXR0cmlidXRlKFwidHlwZVwiKSxcIlNFTEVDVFwiPT09aC50YWdOYW1lJiZoLmhhc0F0dHJpYnV0ZShcIm11bHRpcGxlXCIpKWZvcihJPWgub3B0aW9ucyxrPTAseD1JLmxlbmd0aDtrPHg7aysrKVQ9SVtrXSxULnNlbGVjdGVkJiZhLmFwcGVuZChmLFQudmFsdWUpO2Vsc2UoIW18fFwiY2hlY2tib3hcIiE9PShPPW0udG9Mb3dlckNhc2UoKSkmJlwicmFkaW9cIiE9PU98fGguY2hlY2tlZCkmJmEuYXBwZW5kKGYsaC52YWx1ZSk7Zm9yKGk9MCxQPVtdLGM9Qz0wLFU9ZS5sZW5ndGgtMTswPD1VP0M8PVU6Qz49VTtjPTA8PVU/KytDOi0tQyluPWZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbihuLG8scil7cmV0dXJuIGZ1bmN0aW9uKG4pe2lmKGEuYXBwZW5kKG8sbixyKSwrK2k9PT1lLmxlbmd0aClyZXR1cm4gdC5zdWJtaXRSZXF1ZXN0KEIsYSxlKX19fSh0aGlzKSxQLnB1c2godGhpcy5vcHRpb25zLnRyYW5zZm9ybUZpbGUuY2FsbCh0aGlzLGVbY10sbihlW2NdLHRoaXMuX2dldFBhcmFtTmFtZShjKSxlW2NdLnVwbG9hZC5maWxlbmFtZSkpKTtyZXR1cm4gUH0sdC5wcm90b3R5cGUuc3VibWl0UmVxdWVzdD1mdW5jdGlvbihlLHQsaSl7cmV0dXJuIGUuc2VuZCh0KX0sdC5wcm90b3R5cGUuX2ZpbmlzaGVkPWZ1bmN0aW9uKGUsaSxuKXt2YXIgbyxyLHM7Zm9yKHI9MCxzPWUubGVuZ3RoO3I8cztyKyspbz1lW3JdLG8uc3RhdHVzPXQuU1VDQ0VTUyx0aGlzLmVtaXQoXCJzdWNjZXNzXCIsbyxpLG4pLHRoaXMuZW1pdChcImNvbXBsZXRlXCIsbyk7aWYodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlJiYodGhpcy5lbWl0KFwic3VjY2Vzc211bHRpcGxlXCIsZSxpLG4pLHRoaXMuZW1pdChcImNvbXBsZXRlbXVsdGlwbGVcIixlKSksdGhpcy5vcHRpb25zLmF1dG9Qcm9jZXNzUXVldWUpcmV0dXJuIHRoaXMucHJvY2Vzc1F1ZXVlKCl9LHQucHJvdG90eXBlLl9lcnJvclByb2Nlc3Npbmc9ZnVuY3Rpb24oZSxpLG4pe3ZhciBvLHIscztmb3Iocj0wLHM9ZS5sZW5ndGg7cjxzO3IrKylvPWVbcl0sby5zdGF0dXM9dC5FUlJPUix0aGlzLmVtaXQoXCJlcnJvclwiLG8saSxuKSx0aGlzLmVtaXQoXCJjb21wbGV0ZVwiLG8pO2lmKHRoaXMub3B0aW9ucy51cGxvYWRNdWx0aXBsZSYmKHRoaXMuZW1pdChcImVycm9ybXVsdGlwbGVcIixlLGksbiksdGhpcy5lbWl0KFwiY29tcGxldGVtdWx0aXBsZVwiLGUpKSx0aGlzLm9wdGlvbnMuYXV0b1Byb2Nlc3NRdWV1ZSlyZXR1cm4gdGhpcy5wcm9jZXNzUXVldWUoKX0sdH0oaSksdC52ZXJzaW9uPVwiNS4xLjFcIix0Lm9wdGlvbnM9e30sdC5vcHRpb25zRm9yRWxlbWVudD1mdW5jdGlvbihlKXtyZXR1cm4gZS5nZXRBdHRyaWJ1dGUoXCJpZFwiKT90Lm9wdGlvbnNbbyhlLmdldEF0dHJpYnV0ZShcImlkXCIpKV06dm9pZCAwfSx0Lmluc3RhbmNlcz1bXSx0LmZvckVsZW1lbnQ9ZnVuY3Rpb24oZSl7aWYoXCJzdHJpbmdcIj09dHlwZW9mIGUmJihlPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZSkpLG51bGw9PShudWxsIT1lP2UuZHJvcHpvbmU6dm9pZCAwKSl0aHJvdyBuZXcgRXJyb3IoXCJObyBEcm9wem9uZSBmb3VuZCBmb3IgZ2l2ZW4gZWxlbWVudC4gVGhpcyBpcyBwcm9iYWJseSBiZWNhdXNlIHlvdSdyZSB0cnlpbmcgdG8gYWNjZXNzIGl0IGJlZm9yZSBEcm9wem9uZSBoYWQgdGhlIHRpbWUgdG8gaW5pdGlhbGl6ZS4gVXNlIHRoZSBgaW5pdGAgb3B0aW9uIHRvIHNldHVwIGFueSBhZGRpdGlvbmFsIG9ic2VydmVycyBvbiB5b3VyIERyb3B6b25lLlwiKTtyZXR1cm4gZS5kcm9wem9uZX0sdC5hdXRvRGlzY292ZXI9ITAsdC5kaXNjb3Zlcj1mdW5jdGlvbigpe3ZhciBlLGksbixvLHIscztmb3IoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbD9uPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIuZHJvcHpvbmVcIik6KG49W10sZT1mdW5jdGlvbihlKXt2YXIgdCxpLG8scjtmb3Iocj1bXSxpPTAsbz1lLmxlbmd0aDtpPG87aSsrKXQ9ZVtpXSwvKF58IClkcm9wem9uZSgkfCApLy50ZXN0KHQuY2xhc3NOYW1lKT9yLnB1c2gobi5wdXNoKHQpKTpyLnB1c2godm9pZCAwKTtyZXR1cm4gcn0sZShkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImRpdlwiKSksZShkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImZvcm1cIikpKSxzPVtdLG89MCxyPW4ubGVuZ3RoO288cjtvKyspaT1uW29dLHQub3B0aW9uc0ZvckVsZW1lbnQoaSkhPT0hMT9zLnB1c2gobmV3IHQoaSkpOnMucHVzaCh2b2lkIDApO3JldHVybiBzfSx0LmJsYWNrbGlzdGVkQnJvd3NlcnM9Wy9vcGVyYS4qTWFjaW50b3NoLip2ZXJzaW9uXFwvMTIvaV0sdC5pc0Jyb3dzZXJTdXBwb3J0ZWQ9ZnVuY3Rpb24oKXt2YXIgZSxpLG4sbyxyO2lmKGU9ITAsd2luZG93LkZpbGUmJndpbmRvdy5GaWxlUmVhZGVyJiZ3aW5kb3cuRmlsZUxpc3QmJndpbmRvdy5CbG9iJiZ3aW5kb3cuRm9ybURhdGEmJmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IpaWYoXCJjbGFzc0xpc3RcImluIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJhXCIpKWZvcihvPXQuYmxhY2tsaXN0ZWRCcm93c2VycyxpPTAsbj1vLmxlbmd0aDtpPG47aSsrKXI9b1tpXSxyLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCkmJihlPSExKTtlbHNlIGU9ITE7ZWxzZSBlPSExO3JldHVybiBlfSx0LmRhdGFVUkl0b0Jsb2I9ZnVuY3Rpb24oZSl7dmFyIHQsaSxuLG8scixzLGE7Zm9yKGk9YXRvYihlLnNwbGl0KFwiLFwiKVsxXSkscz1lLnNwbGl0KFwiLFwiKVswXS5zcGxpdChcIjpcIilbMV0uc3BsaXQoXCI7XCIpWzBdLHQ9bmV3IEFycmF5QnVmZmVyKGkubGVuZ3RoKSxvPW5ldyBVaW50OEFycmF5KHQpLG49cj0wLGE9aS5sZW5ndGg7MDw9YT9yPD1hOnI+PWE7bj0wPD1hPysrcjotLXIpb1tuXT1pLmNoYXJDb2RlQXQobik7cmV0dXJuIG5ldyBCbG9iKFt0XSx7dHlwZTpzfSl9LGQ9ZnVuY3Rpb24oZSx0KXt2YXIgaSxuLG8scjtmb3Iocj1bXSxuPTAsbz1lLmxlbmd0aDtuPG87bisrKWk9ZVtuXSxpIT09dCYmci5wdXNoKGkpO3JldHVybiByfSxvPWZ1bmN0aW9uKGUpe3JldHVybiBlLnJlcGxhY2UoL1tcXC1fXShcXHcpL2csZnVuY3Rpb24oZSl7cmV0dXJuIGUuY2hhckF0KDEpLnRvVXBwZXJDYXNlKCl9KX0sdC5jcmVhdGVFbGVtZW50PWZ1bmN0aW9uKGUpe3ZhciB0O3JldHVybiB0PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiksdC5pbm5lckhUTUw9ZSx0LmNoaWxkTm9kZXNbMF19LHQuZWxlbWVudEluc2lkZT1mdW5jdGlvbihlLHQpe2lmKGU9PT10KXJldHVybiEwO2Zvcig7ZT1lLnBhcmVudE5vZGU7KWlmKGU9PT10KXJldHVybiEwO3JldHVybiExfSx0LmdldEVsZW1lbnQ9ZnVuY3Rpb24oZSx0KXt2YXIgaTtpZihcInN0cmluZ1wiPT10eXBlb2YgZT9pPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZSk6bnVsbCE9ZS5ub2RlVHlwZSYmKGk9ZSksbnVsbD09aSl0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIGBcIit0K1wiYCBvcHRpb24gcHJvdmlkZWQuIFBsZWFzZSBwcm92aWRlIGEgQ1NTIHNlbGVjdG9yIG9yIGEgcGxhaW4gSFRNTCBlbGVtZW50LlwiKTtyZXR1cm4gaX0sdC5nZXRFbGVtZW50cz1mdW5jdGlvbihlLHQpe3ZhciBpLG4sbyxyLHMsYSxsLGQ7aWYoZSBpbnN0YW5jZW9mIEFycmF5KXtvPVtdO3RyeXtmb3Iocj0wLGE9ZS5sZW5ndGg7cjxhO3IrKyluPWVbcl0sby5wdXNoKHRoaXMuZ2V0RWxlbWVudChuLHQpKX1jYXRjaChlKXtpPWUsbz1udWxsfX1lbHNlIGlmKFwic3RyaW5nXCI9PXR5cGVvZiBlKWZvcihvPVtdLGQ9ZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChlKSxzPTAsbD1kLmxlbmd0aDtzPGw7cysrKW49ZFtzXSxvLnB1c2gobik7ZWxzZSBudWxsIT1lLm5vZGVUeXBlJiYobz1bZV0pO2lmKG51bGw9PW98fCFvLmxlbmd0aCl0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIGBcIit0K1wiYCBvcHRpb24gcHJvdmlkZWQuIFBsZWFzZSBwcm92aWRlIGEgQ1NTIHNlbGVjdG9yLCBhIHBsYWluIEhUTUwgZWxlbWVudCBvciBhIGxpc3Qgb2YgdGhvc2UuXCIpO3JldHVybiBvfSx0LmNvbmZpcm09ZnVuY3Rpb24oZSx0LGkpe3JldHVybiB3aW5kb3cuY29uZmlybShlKT90KCk6bnVsbCE9aT9pKCk6dm9pZCAwfSx0LmlzVmFsaWRGaWxlPWZ1bmN0aW9uKGUsdCl7dmFyIGksbixvLHIscztpZighdClyZXR1cm4hMDtmb3IodD10LnNwbGl0KFwiLFwiKSxyPWUudHlwZSxpPXIucmVwbGFjZSgvXFwvLiokLyxcIlwiKSxuPTAsbz10Lmxlbmd0aDtuPG87bisrKWlmKHM9dFtuXSxzPXMudHJpbSgpLFwiLlwiPT09cy5jaGFyQXQoMCkpe2lmKGUubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2Yocy50b0xvd2VyQ2FzZSgpLGUubmFtZS5sZW5ndGgtcy5sZW5ndGgpIT09LTEpcmV0dXJuITB9ZWxzZSBpZigvXFwvXFwqJC8udGVzdChzKSl7aWYoaT09PXMucmVwbGFjZSgvXFwvLiokLyxcIlwiKSlyZXR1cm4hMH1lbHNlIGlmKHI9PT1zKXJldHVybiEwO3JldHVybiExfSxcInVuZGVmaW5lZFwiIT10eXBlb2YgalF1ZXJ5JiZudWxsIT09alF1ZXJ5JiYoalF1ZXJ5LmZuLmRyb3B6b25lPWZ1bmN0aW9uKGUpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtyZXR1cm4gbmV3IHQodGhpcyxlKX0pfSksXCJ1bmRlZmluZWRcIiE9dHlwZW9mIGUmJm51bGwhPT1lP2UuZXhwb3J0cz10OndpbmRvdy5Ecm9wem9uZT10LHQuQURERUQ9XCJhZGRlZFwiLHQuUVVFVUVEPVwicXVldWVkXCIsdC5BQ0NFUFRFRD10LlFVRVVFRCx0LlVQTE9BRElORz1cInVwbG9hZGluZ1wiLHQuUFJPQ0VTU0lORz10LlVQTE9BRElORyx0LkNBTkNFTEVEPVwiY2FuY2VsZWRcIix0LkVSUk9SPVwiZXJyb3JcIix0LlNVQ0NFU1M9XCJzdWNjZXNzXCIscz1mdW5jdGlvbihlKXt2YXIgdCxpLG4sbyxyLHMsYSxsLGQscDtmb3IoYT1lLm5hdHVyYWxXaWR0aCxzPWUubmF0dXJhbEhlaWdodCxpPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJjYW52YXNcIiksaS53aWR0aD0xLGkuaGVpZ2h0PXMsbj1pLmdldENvbnRleHQoXCIyZFwiKSxuLmRyYXdJbWFnZShlLDAsMCksbz1uLmdldEltYWdlRGF0YSgxLDAsMSxzKS5kYXRhLHA9MCxyPXMsbD1zO2w+cDspdD1vWzQqKGwtMSkrM10sMD09PXQ/cj1sOnA9bCxsPXIrcD4+MTtyZXR1cm4gZD1sL3MsMD09PWQ/MTpkfSxhPWZ1bmN0aW9uKGUsdCxpLG4sbyxyLGEsbCxkLHApe3ZhciB1O3JldHVybiB1PXModCksZS5kcmF3SW1hZ2UodCxpLG4sbyxyLGEsbCxkLHAvdSl9LG49ZnVuY3Rpb24oKXtmdW5jdGlvbiBlKCl7fXJldHVybiBlLktFWV9TVFI9XCJBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWmFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6MDEyMzQ1Njc4OSsvPVwiLGUuZW5jb2RlNjQ9ZnVuY3Rpb24oZSl7dmFyIHQsaSxuLG8scixzLGEsbCxkO2ZvcihkPVwiXCIsdD12b2lkIDAsaT12b2lkIDAsbj1cIlwiLG89dm9pZCAwLHI9dm9pZCAwLHM9dm9pZCAwLGE9XCJcIixsPTA7OylpZih0PWVbbCsrXSxpPWVbbCsrXSxuPWVbbCsrXSxvPXQ+PjIscj0oMyZ0KTw8NHxpPj40LHM9KDE1JmkpPDwyfG4+PjYsYT02MyZuLGlzTmFOKGkpP3M9YT02NDppc05hTihuKSYmKGE9NjQpLGQ9ZCt0aGlzLktFWV9TVFIuY2hhckF0KG8pK3RoaXMuS0VZX1NUUi5jaGFyQXQocikrdGhpcy5LRVlfU1RSLmNoYXJBdChzKSt0aGlzLktFWV9TVFIuY2hhckF0KGEpLHQ9aT1uPVwiXCIsbz1yPXM9YT1cIlwiLCEobDxlLmxlbmd0aCkpYnJlYWs7cmV0dXJuIGR9LGUucmVzdG9yZT1mdW5jdGlvbihlLHQpe3ZhciBpLG4sbztyZXR1cm4gZS5tYXRjaChcImRhdGE6aW1hZ2UvanBlZztiYXNlNjQsXCIpPyhuPXRoaXMuZGVjb2RlNjQoZS5yZXBsYWNlKFwiZGF0YTppbWFnZS9qcGVnO2Jhc2U2NCxcIixcIlwiKSksbz10aGlzLnNsaWNlMlNlZ21lbnRzKG4pLGk9dGhpcy5leGlmTWFuaXB1bGF0aW9uKHQsbyksXCJkYXRhOmltYWdlL2pwZWc7YmFzZTY0LFwiK3RoaXMuZW5jb2RlNjQoaSkpOnR9LGUuZXhpZk1hbmlwdWxhdGlvbj1mdW5jdGlvbihlLHQpe3ZhciBpLG4sbztyZXR1cm4gbj10aGlzLmdldEV4aWZBcnJheSh0KSxvPXRoaXMuaW5zZXJ0RXhpZihlLG4pLGk9bmV3IFVpbnQ4QXJyYXkobyl9LGUuZ2V0RXhpZkFycmF5PWZ1bmN0aW9uKGUpe3ZhciB0LGk7Zm9yKHQ9dm9pZCAwLGk9MDtpPGUubGVuZ3RoOyl7aWYodD1lW2ldLDI1NT09PXRbMF0mMjI1PT09dFsxXSlyZXR1cm4gdDtpKyt9cmV0dXJuW119LGUuaW5zZXJ0RXhpZj1mdW5jdGlvbihlLHQpe3ZhciBpLG4sbyxyLHMsYTtyZXR1cm4gcj1lLnJlcGxhY2UoXCJkYXRhOmltYWdlL2pwZWc7YmFzZTY0LFwiLFwiXCIpLG89dGhpcy5kZWNvZGU2NChyKSxhPW8uaW5kZXhPZigyNTUsMykscz1vLnNsaWNlKDAsYSksbj1vLnNsaWNlKGEpLGk9cyxpPWkuY29uY2F0KHQpLGk9aS5jb25jYXQobil9LGUuc2xpY2UyU2VnbWVudHM9ZnVuY3Rpb24oZSl7dmFyIHQsaSxuLG8scjtmb3IoaT0wLHI9W107Oyl7aWYoMjU1PT09ZVtpXSYyMTg9PT1lW2krMV0pYnJlYWs7aWYoMjU1PT09ZVtpXSYyMTY9PT1lW2krMV0/aSs9Mjoobj0yNTYqZVtpKzJdK2VbaSszXSx0PWkrbisyLG89ZS5zbGljZShpLHQpLHIucHVzaChvKSxpPXQpLGk+ZS5sZW5ndGgpYnJlYWt9cmV0dXJuIHJ9LGUuZGVjb2RlNjQ9ZnVuY3Rpb24oZSl7dmFyIHQsaSxuLG8scixzLGEsbCxkLHAsdTtmb3IodT1cIlwiLG49dm9pZCAwLG89dm9pZCAwLHI9XCJcIixzPXZvaWQgMCxhPXZvaWQgMCxsPXZvaWQgMCxkPVwiXCIscD0wLGk9W10sdD0vW15BLVphLXowLTlcXCtcXC9cXD1dL2csdC5leGVjKGUpJiZjb25zb2xlLndhcm5pbmcoXCJUaGVyZSB3ZXJlIGludmFsaWQgYmFzZTY0IGNoYXJhY3RlcnMgaW4gdGhlIGlucHV0IHRleHQuXFxuVmFsaWQgYmFzZTY0IGNoYXJhY3RlcnMgYXJlIEEtWiwgYS16LCAwLTksICcrJywgJy8nLGFuZCAnPSdcXG5FeHBlY3QgZXJyb3JzIGluIGRlY29kaW5nLlwiKSxlPWUucmVwbGFjZSgvW15BLVphLXowLTlcXCtcXC9cXD1dL2csXCJcIik7OylpZihzPXRoaXMuS0VZX1NUUi5pbmRleE9mKGUuY2hhckF0KHArKykpLGE9dGhpcy5LRVlfU1RSLmluZGV4T2YoZS5jaGFyQXQocCsrKSksbD10aGlzLktFWV9TVFIuaW5kZXhPZihlLmNoYXJBdChwKyspKSxkPXRoaXMuS0VZX1NUUi5pbmRleE9mKGUuY2hhckF0KHArKykpLG49czw8MnxhPj40LG89KDE1JmEpPDw0fGw+PjIscj0oMyZsKTw8NnxkLGkucHVzaChuKSw2NCE9PWwmJmkucHVzaChvKSw2NCE9PWQmJmkucHVzaChyKSxuPW89cj1cIlwiLHM9YT1sPWQ9XCJcIiwhKHA8ZS5sZW5ndGgpKWJyZWFrO3JldHVybiBpfSxlfSgpLHI9ZnVuY3Rpb24oZSx0KXt2YXIgaSxuLG8scixzLGEsbCxkLHA7aWYobz0hMSxwPSEwLG49ZS5kb2N1bWVudCxkPW4uZG9jdW1lbnRFbGVtZW50LGk9bi5hZGRFdmVudExpc3RlbmVyP1wiYWRkRXZlbnRMaXN0ZW5lclwiOlwiYXR0YWNoRXZlbnRcIixsPW4uYWRkRXZlbnRMaXN0ZW5lcj9cInJlbW92ZUV2ZW50TGlzdGVuZXJcIjpcImRldGFjaEV2ZW50XCIsYT1uLmFkZEV2ZW50TGlzdGVuZXI/XCJcIjpcIm9uXCIscj1mdW5jdGlvbihpKXtpZihcInJlYWR5c3RhdGVjaGFuZ2VcIiE9PWkudHlwZXx8XCJjb21wbGV0ZVwiPT09bi5yZWFkeVN0YXRlKXJldHVybihcImxvYWRcIj09PWkudHlwZT9lOm4pW2xdKGEraS50eXBlLHIsITEpLCFvJiYobz0hMCk/dC5jYWxsKGUsaS50eXBlfHxpKTp2b2lkIDB9LHM9ZnVuY3Rpb24oKXt2YXIgZTt0cnl7ZC5kb1Njcm9sbChcImxlZnRcIil9Y2F0Y2godCl7cmV0dXJuIGU9dCx2b2lkIHNldFRpbWVvdXQocyw1MCl9cmV0dXJuIHIoXCJwb2xsXCIpfSxcImNvbXBsZXRlXCIhPT1uLnJlYWR5U3RhdGUpe2lmKG4uY3JlYXRlRXZlbnRPYmplY3QmJmQuZG9TY3JvbGwpe3RyeXtwPSFlLmZyYW1lRWxlbWVudH1jYXRjaChlKXt9cCYmcygpfXJldHVybiBuW2ldKGErXCJET01Db250ZW50TG9hZGVkXCIsciwhMSksbltpXShhK1wicmVhZHlzdGF0ZWNoYW5nZVwiLHIsITEpLGVbaV0oYStcImxvYWRcIixyLCExKX19LHQuX2F1dG9EaXNjb3ZlckZ1bmN0aW9uPWZ1bmN0aW9uKCl7aWYodC5hdXRvRGlzY292ZXIpcmV0dXJuIHQuZGlzY292ZXIoKX0scih3aW5kb3csdC5fYXV0b0Rpc2NvdmVyRnVuY3Rpb24pfSkuY2FsbCh0aGlzKX0pLmNhbGwodCxpKDkpKGUpKX0sZnVuY3Rpb24oZSx0KXtlLmV4cG9ydHM9JyA8Zm9ybSA6YWN0aW9uPXVybCBjbGFzcz1cInZ1ZS1kcm9wem9uZSBkcm9wem9uZVwiIDppZD1pZD4gPHNsb3Q+PC9zbG90PiA8L2Zvcm0+ICd9LGZ1bmN0aW9uKGUsdCxpKXtmdW5jdGlvbiBuKGUsdCl7Zm9yKHZhciBpPTA7aTxlLmxlbmd0aDtpKyspe3ZhciBuPWVbaV0sbz11W24uaWRdO2lmKG8pe28ucmVmcysrO2Zvcih2YXIgcj0wO3I8by5wYXJ0cy5sZW5ndGg7cisrKW8ucGFydHNbcl0obi5wYXJ0c1tyXSk7Zm9yKDtyPG4ucGFydHMubGVuZ3RoO3IrKylvLnBhcnRzLnB1c2gobChuLnBhcnRzW3JdLHQpKX1lbHNle2Zvcih2YXIgcz1bXSxyPTA7cjxuLnBhcnRzLmxlbmd0aDtyKyspcy5wdXNoKGwobi5wYXJ0c1tyXSx0KSk7dVtuLmlkXT17aWQ6bi5pZCxyZWZzOjEscGFydHM6c319fX1mdW5jdGlvbiBvKGUpe2Zvcih2YXIgdD1bXSxpPXt9LG49MDtuPGUubGVuZ3RoO24rKyl7dmFyIG89ZVtuXSxyPW9bMF0scz1vWzFdLGE9b1syXSxsPW9bM10sZD17Y3NzOnMsbWVkaWE6YSxzb3VyY2VNYXA6bH07aVtyXT9pW3JdLnBhcnRzLnB1c2goZCk6dC5wdXNoKGlbcl09e2lkOnIscGFydHM6W2RdfSl9cmV0dXJuIHR9ZnVuY3Rpb24gcihlLHQpe3ZhciBpPWYoKSxuPXZbdi5sZW5ndGgtMV07aWYoXCJ0b3BcIj09PWUuaW5zZXJ0QXQpbj9uLm5leHRTaWJsaW5nP2kuaW5zZXJ0QmVmb3JlKHQsbi5uZXh0U2libGluZyk6aS5hcHBlbmRDaGlsZCh0KTppLmluc2VydEJlZm9yZSh0LGkuZmlyc3RDaGlsZCksdi5wdXNoKHQpO2Vsc2V7aWYoXCJib3R0b21cIiE9PWUuaW5zZXJ0QXQpdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCB2YWx1ZSBmb3IgcGFyYW1ldGVyICdpbnNlcnRBdCcuIE11c3QgYmUgJ3RvcCcgb3IgJ2JvdHRvbScuXCIpO2kuYXBwZW5kQ2hpbGQodCl9fWZ1bmN0aW9uIHMoZSl7ZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGUpO3ZhciB0PXYuaW5kZXhPZihlKTt0Pj0wJiZ2LnNwbGljZSh0LDEpfWZ1bmN0aW9uIGEoZSl7dmFyIHQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInN0eWxlXCIpO3JldHVybiB0LnR5cGU9XCJ0ZXh0L2Nzc1wiLHIoZSx0KSx0fWZ1bmN0aW9uIGwoZSx0KXt2YXIgaSxuLG87aWYodC5zaW5nbGV0b24pe3ZhciByPWcrKztpPW18fChtPWEodCkpLG49ZC5iaW5kKG51bGwsaSxyLCExKSxvPWQuYmluZChudWxsLGksciwhMCl9ZWxzZSBpPWEodCksbj1wLmJpbmQobnVsbCxpKSxvPWZ1bmN0aW9uKCl7cyhpKX07cmV0dXJuIG4oZSksZnVuY3Rpb24odCl7aWYodCl7aWYodC5jc3M9PT1lLmNzcyYmdC5tZWRpYT09PWUubWVkaWEmJnQuc291cmNlTWFwPT09ZS5zb3VyY2VNYXApcmV0dXJuO24oZT10KX1lbHNlIG8oKX19ZnVuY3Rpb24gZChlLHQsaSxuKXt2YXIgbz1pP1wiXCI6bi5jc3M7aWYoZS5zdHlsZVNoZWV0KWUuc3R5bGVTaGVldC5jc3NUZXh0PXoodCxvKTtlbHNle3ZhciByPWRvY3VtZW50LmNyZWF0ZVRleHROb2RlKG8pLHM9ZS5jaGlsZE5vZGVzO3NbdF0mJmUucmVtb3ZlQ2hpbGQoc1t0XSkscy5sZW5ndGg/ZS5pbnNlcnRCZWZvcmUocixzW3RdKTplLmFwcGVuZENoaWxkKHIpfX1mdW5jdGlvbiBwKGUsdCl7dmFyIGk9dC5jc3Msbj10Lm1lZGlhLG89dC5zb3VyY2VNYXA7aWYobiYmZS5zZXRBdHRyaWJ1dGUoXCJtZWRpYVwiLG4pLG8mJihpKz1cIlxcbi8qIyBzb3VyY2VVUkw9XCIrby5zb3VyY2VzWzBdK1wiICovXCIsaSs9XCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiK2J0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KG8pKSkpK1wiICovXCIpLGUuc3R5bGVTaGVldCllLnN0eWxlU2hlZXQuY3NzVGV4dD1pO2Vsc2V7Zm9yKDtlLmZpcnN0Q2hpbGQ7KWUucmVtb3ZlQ2hpbGQoZS5maXJzdENoaWxkKTtlLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGkpKX19dmFyIHU9e30sYz1mdW5jdGlvbihlKXt2YXIgdDtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm5cInVuZGVmaW5lZFwiPT10eXBlb2YgdCYmKHQ9ZS5hcHBseSh0aGlzLGFyZ3VtZW50cykpLHR9fSxoPWMoZnVuY3Rpb24oKXtyZXR1cm4vbXNpZSBbNi05XVxcYi8udGVzdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpKX0pLGY9YyhmdW5jdGlvbigpe3JldHVybiBkb2N1bWVudC5oZWFkfHxkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImhlYWRcIilbMF19KSxtPW51bGwsZz0wLHY9W107ZS5leHBvcnRzPWZ1bmN0aW9uKGUsdCl7dD10fHx7fSxcInVuZGVmaW5lZFwiPT10eXBlb2YgdC5zaW5nbGV0b24mJih0LnNpbmdsZXRvbj1oKCkpLFwidW5kZWZpbmVkXCI9PXR5cGVvZiB0Lmluc2VydEF0JiYodC5pbnNlcnRBdD1cImJvdHRvbVwiKTt2YXIgaT1vKGUpO3JldHVybiBuKGksdCksZnVuY3Rpb24oZSl7Zm9yKHZhciByPVtdLHM9MDtzPGkubGVuZ3RoO3MrKyl7dmFyIGE9aVtzXSxsPXVbYS5pZF07bC5yZWZzLS0sci5wdXNoKGwpfWlmKGUpe3ZhciBkPW8oZSk7bihkLHQpfWZvcih2YXIgcz0wO3M8ci5sZW5ndGg7cysrKXt2YXIgbD1yW3NdO2lmKDA9PT1sLnJlZnMpe2Zvcih2YXIgcD0wO3A8bC5wYXJ0cy5sZW5ndGg7cCsrKWwucGFydHNbcF0oKTtkZWxldGUgdVtsLmlkXX19fX07dmFyIHo9ZnVuY3Rpb24oKXt2YXIgZT1bXTtyZXR1cm4gZnVuY3Rpb24odCxpKXtyZXR1cm4gZVt0XT1pLGUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oXCJcXG5cIil9fSgpfSxmdW5jdGlvbihlLHQsaSl7dmFyIG49aSg0KTtcInN0cmluZ1wiPT10eXBlb2YgbiYmKG49W1tlLmlkLG4sXCJcIl1dKTtpKDcpKG4se30pO24ubG9jYWxzJiYoZS5leHBvcnRzPW4ubG9jYWxzKX0sZnVuY3Rpb24oZSx0KXtlLmV4cG9ydHM9ZnVuY3Rpb24oZSl7cmV0dXJuIGUud2VicGFja1BvbHlmaWxsfHwoZS5kZXByZWNhdGU9ZnVuY3Rpb24oKXt9LGUucGF0aHM9W10sZS5jaGlsZHJlbj1bXSxlLndlYnBhY2tQb2x5ZmlsbD0xKSxlfX1dKX0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUyLWRyb3B6b25lL2Rpc3QvdnVlMi1kcm9wem9uZS5qc1xuLy8gbW9kdWxlIGlkID0gMzdcbi8vIG1vZHVsZSBjaHVua3MgPSAxMSAxNyIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0FsYnVtTGlzdC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTcwMmU2NWEwXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0FsYnVtTGlzdC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx0b3B3eWNcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxjcGFuZWxcXFxcdmlzYVxcXFxjb21wb25lbnRzXFxcXEdhbGxlcnlcXFxcQWxidW1MaXN0LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEFsYnVtTGlzdC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNzAyZTY1YTBcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi03MDJlNjVhMFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC92aXNhL2NvbXBvbmVudHMvR2FsbGVyeS9BbGJ1bUxpc3QudnVlXG4vLyBtb2R1bGUgaWQgPSAzODBcbi8vIG1vZHVsZSBjaHVua3MgPSAxMSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1BhZ2luYXRpb24udnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zYzJkY2FiN1xcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9QYWdpbmF0aW9uLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHRvcHd5Y1xcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXGNwYW5lbFxcXFx2aXNhXFxcXGNvbXBvbmVudHNcXFxcR2FsbGVyeVxcXFxQYWdpbmF0aW9uLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFBhZ2luYXRpb24udnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTNjMmRjYWI3XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtM2MyZGNhYjdcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0dhbGxlcnkvUGFnaW5hdGlvbi52dWVcbi8vIG1vZHVsZSBpZCA9IDM4MlxuLy8gbW9kdWxlIGNodW5rcyA9IDExIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vUHJvamVjdHMudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1iYzA0NGU1MlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Qcm9qZWN0cy52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx0b3B3eWNcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxjcGFuZWxcXFxcdmlzYVxcXFxjb21wb25lbnRzXFxcXEdhbGxlcnlcXFxcUHJvamVjdHMudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gUHJvamVjdHMudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWJjMDQ0ZTUyXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtYmMwNDRlNTJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0dhbGxlcnkvUHJvamVjdHMudnVlXG4vLyBtb2R1bGUgaWQgPSAzODNcbi8vIG1vZHVsZSBjaHVua3MgPSAxMSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2JywgWyghX3ZtLmNsaWVudCkgPyBfYygnbmF2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhZ2luYXRpb25cIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFnZS1zdGF0c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5wYWdpbmF0aW9uLmZyb20pICsgXCIgLSBcIiArIF92bS5fcyhfdm0ucGFnaW5hdGlvbi50bykgKyBcIiBvZiBcIiArIF92bS5fcyhfdm0ucGFnaW5hdGlvbi50b3RhbCkpXSksIF92bS5fdihcIiBcIiksIChfdm0ucGFnaW5hdGlvbi5wcmV2UGFnZVVybCkgPyBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHQgcGFnaW5hdGlvbi1wcmV2aW91c1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJGVtaXQoJ3ByZXYnKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgUHJldlxcbiAgICAgIFwiKV0pIDogX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tcHJldmlvdXNcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkaXNhYmxlZFwiOiB0cnVlXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgUHJldlxcbiAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnBhZ2luYXRpb24ubmV4dFBhZ2VVcmwpID8gX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tbmV4dFwiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uJGVtaXQoJ25leHQnKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgTmV4dFxcbiAgICAgIFwiKV0pIDogX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tbmV4dFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRpc2FibGVkXCI6IHRydWVcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgTmV4dFxcbiAgICAgIFwiKV0pXSkgOiBfYygnbmF2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhZ2luYXRpb25cIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFnZS1zdGF0c1wiXG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLnBhZ2luYXRpb24uZnJvbSkgKyBcIiAtIFwiICsgX3ZtLl9zKF92bS5wYWdpbmF0aW9uLnRvKSArIFwiIG9mIFwiICsgX3ZtLl9zKF92bS5maWx0ZXJlZC5sZW5ndGgpICsgXCJcXG4gICAgICAgICAgXCIpLCAoX3ZtLmZpbHRlcmVkLmxlbmd0aCA8IF92bS5wYWdpbmF0aW9uLnRvdGFsKSA/IF9jKCdzcGFuJywgW192bS5fdihcIihmaWx0ZXJlZCBmcm9tIFwiICsgX3ZtLl9zKF92bS5wYWdpbmF0aW9uLnRvdGFsKSArIFwiIHRvdGFsIGVudHJpZXMpXCIpXSkgOiBfdm0uX2UoKV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnBhZ2luYXRpb24ucHJldlBhZ2UpID8gX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHBhZ2luYXRpb24tcHJldmlvdXNcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLiRlbWl0KCdwcmV2Jyk7XG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgIFByZXZcXG4gICAgICBcIildKSA6IF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLXByZXZpb3VzXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGlzYWJsZWRcIjogdHJ1ZVxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgIFByZXZcXG4gICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5wYWdpbmF0aW9uLm5leHRQYWdlKSA/IF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLW5leHRcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLiRlbWl0KCduZXh0Jyk7XG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgIE5leHRcXG4gICAgICBcIildKSA6IF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBwYWdpbmF0aW9uLW5leHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkaXNhYmxlZFwiOiB0cnVlXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgIE5leHRcXG4gICAgICBcIildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtM2MyZGNhYjdcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zYzJkY2FiN1wifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvdmlzYS9jb21wb25lbnRzL0dhbGxlcnkvUGFnaW5hdGlvbi52dWVcbi8vIG1vZHVsZSBpZCA9IDQwOFxuLy8gbW9kdWxlIGNodW5rcyA9IDExIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1ob3ZlclwiXG4gIH0sIFtfYygndGhlYWQnLCBbX2MoJ3RyJywgX3ZtLl9sKChfdm0uY29sdW1ucyksIGZ1bmN0aW9uKGNvbHVtbikge1xuICAgIHJldHVybiBfYygndGgnLCB7XG4gICAgICBrZXk6IGNvbHVtbi50aXRsZSxcbiAgICAgIGNsYXNzOiBfdm0uc29ydEtleSA9PT0gY29sdW1uLnRpdGxlID8gKF92bS5zb3J0T3JkZXJzW2NvbHVtbi50aXRsZV0gPiAwID8gJ3NvcnRpbmdfYXNjJyA6ICdzb3J0aW5nX2Rlc2MnKSA6ICdzb3J0aW5nJyxcbiAgICAgIHN0eWxlOiAoJ3dpZHRoOicgKyBjb2x1bW4ud2lkdGggKyAnOycgKyAnY3Vyc29yOnBvaW50ZXI7JyksXG4gICAgICBvbjoge1xuICAgICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS4kZW1pdCgnc29ydCcsIGNvbHVtbi50aXRsZSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGNvbHVtbi5sYWJlbCkgKyBcIlxcbiAgICAgICAgICBcIildKVxuICB9KSwgMCldKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl90KFwiZGVmYXVsdFwiKV0sIDIpXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTcwMmU2NWEwXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNzAyZTY1YTBcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9HYWxsZXJ5L0FsYnVtTGlzdC52dWVcbi8vIG1vZHVsZSBpZCA9IDQyN1xuLy8gbW9kdWxlIGNodW5rcyA9IDExIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbCBmYWRlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJkaWFsb2dBZGRBbGJ1bVwiLFxuICAgICAgXCJhcmlhLWhpZGRlblwiOiBcInRydWVcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtZGlhbG9nXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWJvZHlcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTJcIlxuICB9LCBbX2MoJ2gzJywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm0tdC1ub25lIG0tYlwiXG4gIH0sIFtfdm0uX3YoXCJBZGQgbmV3IGFsYnVtXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdmb3JtJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInJvbGVcIjogXCJmb3JtXCIsXG4gICAgICBcImFjdGlvblwiOiBcIiNcIixcbiAgICAgIFwibWV0aG9kXCI6IFwiUE9TVFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBfdm0uc2F2ZUFsYnVtKF92bS5hY3QsICcnKVxuICAgICAgfVxuICAgIH1cbiAgfSwgWyhfdm0uZXJyb3IpID8gX2MoJ3AnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiY29sb3JcIjogXCJyZWRcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlRpdGxlIGNhbm5vdCBkdXBsaWNhdGUgb3IgZW1wdHlcIildKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmVycm9yMikgPyBfYygncCcsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJjb2xvclwiOiBcInJlZFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQWxidW0gYWxyZWFkeSBleGlzdFwiKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uYWxidW0udGl0bGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJhbGJ1bS50aXRsZVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwibmFtZVwiOiBcInRpdGxlXCIsXG4gICAgICBcImlkXCI6IFwidGl0bGVcIixcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwicGxhY2Vob2xkZXJcIjogXCJUaXRsZVwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmFsYnVtLnRpdGxlKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmFsYnVtLCBcInRpdGxlXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS02IGNvbC1zbS1vZmZzZXQtNlwiXG4gIH0sIFsoX3ZtLmFjdCA9PSAnQWRkJykgPyBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnNhdmVBbGJ1bShfdm0uYWN0LCAnJylcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJTYXZlIENoYW5nZXNcIildKSA6IF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uc2F2ZUFsYnVtKF92bS5hY3QsIF92bS5hbGJ1bS5pZClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJTYXZlIENoYW5nZXNcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXdoaXRlIHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0ucmVzZXRBbGwoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihcIkNhbmNlbFwiKV0pXSldKV0pXSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMTJcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1zbSBwdWxsLXJpZ2h0XCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5hY3Rpb25Eb2MoJ2FkZCcsICcnKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXBsdXMgZmEtZndcIlxuICB9KSwgX3ZtLl92KFwiQWRkIE5ldyBBbGJ1bVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImhlaWdodFwiOiBcIjEwcHhcIixcbiAgICAgIFwiY2xlYXJcIjogXCJib3RoXCJcbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlib3ggZmxvYXQtZS1tYXJnaW5zXCJcbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpYm94LWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZS1yZXNwb25zaXZlXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEyXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaWJveCBmbG9hdC1lLW1hcmdpbnNcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpYm94LWNvbnRlbnRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMSBtLWIteHNcIlxuICB9LCBbX2MoJ3NlbGVjdCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS50YWJsZURhdGEubGVuZ3RoKSxcbiAgICAgIGV4cHJlc3Npb246IFwidGFibGVEYXRhLmxlbmd0aFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtc20gZm9ybS1jb250cm9sIGlucHV0LXMtc20gaW5saW5lXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRzZWxlY3RlZFZhbCA9IEFycmF5LnByb3RvdHlwZS5maWx0ZXIuY2FsbCgkZXZlbnQudGFyZ2V0Lm9wdGlvbnMsIGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICByZXR1cm4gby5zZWxlY3RlZFxuICAgICAgICB9KS5tYXAoZnVuY3Rpb24obykge1xuICAgICAgICAgIHZhciB2YWwgPSBcIl92YWx1ZVwiIGluIG8gPyBvLl92YWx1ZSA6IG8udmFsdWU7XG4gICAgICAgICAgcmV0dXJuIHZhbFxuICAgICAgICB9KTtcbiAgICAgICAgX3ZtLiRzZXQoX3ZtLnRhYmxlRGF0YSwgXCJsZW5ndGhcIiwgJGV2ZW50LnRhcmdldC5tdWx0aXBsZSA/ICQkc2VsZWN0ZWRWYWwgOiAkJHNlbGVjdGVkVmFsWzBdKVxuICAgICAgfVxuICAgIH1cbiAgfSwgX3ZtLl9sKChfdm0ucGVyUGFnZSksIGZ1bmN0aW9uKHJlY29yZHMsIGluZGV4KSB7XG4gICAgcmV0dXJuIF9jKCdvcHRpb24nLCB7XG4gICAgICBrZXk6IGluZGV4LFxuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiByZWNvcmRzXG4gICAgICB9XG4gICAgfSwgW192bS5fdihfdm0uX3MocmVjb3JkcykpXSlcbiAgfSksIDApXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLW9mZnNldC03IGNvbC1zbS00XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAgcHVsbC1yaWdodFwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0udGFibGVEYXRhLnNlYXJjaCksXG4gICAgICBleHByZXNzaW9uOiBcInRhYmxlRGF0YS5zZWFyY2hcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LXNtIGZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcInBsYWNlaG9sZGVyXCI6IFwiU2VhcmNoXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0udGFibGVEYXRhLnNlYXJjaClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IFtmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0udGFibGVEYXRhLCBcInNlYXJjaFwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfSwgZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5nZXRBbGJ1bXMoKVxuICAgICAgfV1cbiAgICB9XG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnQWxidW1UYWJsZScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJjb2x1bW5zXCI6IF92bS5jb2x1bW5zLFxuICAgICAgXCJzb3J0S2V5XCI6IF92bS5zb3J0S2V5LFxuICAgICAgXCJzb3J0T3JkZXJzXCI6IF92bS5zb3J0T3JkZXJzXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJzb3J0XCI6IF92bS5zb3J0QnlcbiAgICB9XG4gIH0sIFsoX3ZtLnByb2plY3RzLmxlbmd0aCAhPSAwKSA/IF9jKCd0Ym9keScsIF92bS5fbCgoX3ZtLnByb2plY3RzKSwgZnVuY3Rpb24ocHJvamVjdCkge1xuICAgIHJldHVybiBfYygndHInLCB7XG4gICAgICBrZXk6IHByb2plY3QuaWRcbiAgICB9LCBbX2MoJ3RkJywgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6IF92bS51cmwgKyBwcm9qZWN0LmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihfdm0uX3MocHJvamVjdC50aXRsZSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCJcbiAgICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGFuZ2VyIGJ0bi14cyBwdWxsLXJpZ2h0XCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLmRlbGV0ZUFsYnVtKHByb2plY3QuaWQpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJnbHlwaGljb24gZ2x5cGhpY29uLXRyYXNoXCJcbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi14cyBwdWxsLXJpZ2h0XCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLmFjdGlvbkRvYygnZWRpdCcsIHByb2plY3QuaWQpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJnbHlwaGljb24gZ2x5cGhpY29uLXBlbmNpbFwiXG4gICAgfSldKV0pXSldKVxuICB9KSwgMCkgOiBfYygndGJvZHknLCBbX2MoJ3RyJywgW19jKCd0ZCcsIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTm8gcmVjb3JkIGZvdW5kXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIildKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3BhZ2luYXRpb24nLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwicGFnaW5hdGlvblwiOiBfdm0ucGFnaW5hdGlvblxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwicHJldlwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmdldFByb2plY3RzKF92bS5wYWdpbmF0aW9uLnByZXZQYWdlVXJsKVxuICAgICAgfSxcbiAgICAgIFwibmV4dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmdldFByb2plY3RzKF92bS5wYWdpbmF0aW9uLm5leHRQYWdlVXJsKVxuICAgICAgfVxuICAgIH1cbiAgfSldLCAxKV0pXSldKV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlib3gtdGl0bGVcIlxuICB9LCBbX2MoJ2gzJywgW192bS5fdihcIkxpc3Qgb2YgQWxidW1zXCIpXSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWJjMDQ0ZTUyXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYmMwNDRlNTJcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL3Zpc2EvY29tcG9uZW50cy9HYWxsZXJ5L1Byb2plY3RzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDQyXG4vLyBtb2R1bGUgY2h1bmtzID0gMTEiXSwic291cmNlUm9vdCI6IiJ9