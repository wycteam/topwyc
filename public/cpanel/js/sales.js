/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 273);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('order-list', __webpack_require__(227));
Vue.component('orderdetails-list', __webpack_require__(226));

Vue.filter('currency', function (value) {
    return '₱' + numberWithCommas(parseFloat(value).toFixed(2));
});

var app = new Vue({

    el: '#orders-content',
    data: {
        items: [],
        items2: [],
        no_item: '',
        order_id: '',
        tracking_no: '',
        receiver_name: ''
    },
    created: function created() {
        var _this = this;

        function getOrders() {
            return axios.get('orders');
        }
        axios.all([getOrders()]).then(axios.spread(function (orders) {
            if (orders.data.data != 0) _this.items = orders.status == 200 ? orders.data : [];else _this.no_item = "There are no orders";
        })).catch($.noop);
    },

    methods: {
        openModal: function openModal(id) {
            var _this2 = this;

            axios.get('/orderDetails/' + id).then(function (result) {
                _this2.items2 = result.data;
                $("#myModal").modal();
            });
        },
        searchByOrderId: function searchByOrderId() {
            var _this3 = this;

            if (this.order_id != '') {
                axios.get('/searchByOrderId/' + this.order_id).then(function (result) {
                    _this3.items = result.data;
                });
            } else {
                axios.get('orders').then(function (result) {
                    _this3.items = result.data;
                });
            }
            $('.footable-odd').show();
            $('#pagination').hide();
        },
        searchByTrackingNo: function searchByTrackingNo() {
            var _this4 = this;

            if (this.tracking_no != '') {
                axios.get('/searchByTrackingNo/' + this.tracking_no).then(function (result) {
                    _this4.items = result.data;
                });
            } else {
                axios.get('orders').then(function (result) {
                    _this4.items = result.data;
                });
            }
            $('.footable-odd').show();
            $('#pagination').hide();
        },
        searchByReceiverName: function searchByReceiverName() {
            var _this5 = this;

            if (this.receiver_name != '') {
                axios.get('/searchByReceiverName/' + this.receiver_name).then(function (result) {
                    _this5.items = result.data;
                });
            } else {
                axios.get('orders').then(function (result) {
                    _this5.items = result.data;
                });
            }
            $('.footable-odd').show();
            $('#pagination').hide();
        }
    }

});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['items2']
});

/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['items'],
    methods: {
        openModal: function openModal(id) {
            this.$parent.openModal(id);
        }
    }
});

/***/ }),

/***/ 226:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(189),
  /* template */
  __webpack_require__(246),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\Users\\IanO\\Code\\wyc06\\resources\\assets\\js\\cpanel\\components\\sales\\OrderDetails.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] OrderDetails.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-cbff77c8", Component.options)
  } else {
    hotAPI.reload("data-v-cbff77c8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 227:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(190),
  /* template */
  __webpack_require__(241),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\Users\\IanO\\Code\\wyc06\\resources\\assets\\js\\cpanel\\components\\sales\\Orders.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Orders.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7f5f9b6d", Component.options)
  } else {
    hotAPI.reload("data-v-7f5f9b6d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "ibox-content"
  }, [_c('table', {
    staticClass: "footable table table-stripped toggle-arrow-tiny",
    attrs: {
      "id": "footable",
      "data-page-size": "15"
    }
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.items), function(item, index) {
    return _c('tr', {
      key: item.id
    }, [_c('td', [_vm._v("\r\n               " + _vm._s(item.id) + "\r\n            ")]), _vm._v(" "), _c('td', [_vm._v("\r\n               " + _vm._s(item.tracking_number) + "\r\n            ")]), _vm._v(" "), _c('td', [_vm._v("\r\n               " + _vm._s(item.name_of_receiver) + "\r\n            ")]), _vm._v(" "), _c('td', [_vm._v("\r\n               " + _vm._s(item.contact_number) + "\r\n            ")]), _vm._v(" "), _c('td', [_vm._v("\r\n               " + _vm._s(item.alternate_contact_number) + "\r\n            ")]), _vm._v(" "), _c('td', [_vm._v("\r\n               " + _vm._s(item.total) + "\r\n            ")]), _vm._v(" "), _c('td', [(item.status == 'Complete') ? _c('span', {
      staticClass: "label label-primary"
    }, [_vm._v(_vm._s(item.status))]) : _c('span', {
      staticClass: "label label-danger"
    }, [_vm._v(_vm._s(item.status))])]), _vm._v(" "), _c('td', {
      staticClass: "text-right"
    }, [_c('div', {
      staticClass: "btn-group"
    }, [_c('button', {
      staticClass: "btn-white btn btn-xs",
      on: {
        "click": function($event) {
          _vm.openModal(item.id)
        }
      }
    }, [_vm._v("View")])])])])
  })), _vm._v(" "), _c('tfoot', {
    attrs: {
      "id": "pagination"
    }
  }, [_c('tr', [_c('td', {
    attrs: {
      "colspan": "8"
    }
  }, [_c('ul', {
    staticClass: "pagination pull-right"
  })])])])], 1)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Order ID")]), _vm._v(" "), _c('th', {
    attrs: {
      "data-hide": "phone"
    }
  }, [_vm._v("Tracking Number")]), _vm._v(" "), _c('th', {
    attrs: {
      "data-hide": "phone"
    }
  }, [_vm._v("Receiver Name")]), _vm._v(" "), _c('th', {
    attrs: {
      "data-hide": "phone"
    }
  }, [_vm._v("Contact No.")]), _vm._v(" "), _c('th', {
    attrs: {
      "data-hide": "phone"
    }
  }, [_vm._v("Alt Contact No.")]), _vm._v(" "), _c('th', {
    attrs: {
      "data-hide": "phone"
    }
  }, [_vm._v("Total")]), _vm._v(" "), _c('th', {
    attrs: {
      "data-hide": "phone"
    }
  }, [_vm._v("Status")]), _vm._v(" "), _c('th', {
    staticClass: "text-right"
  }, [_vm._v("Action")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7f5f9b6d", module.exports)
  }
}

/***/ }),

/***/ 246:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "table-responsive"
  }, [_c('table', {
    staticClass: "table table-striped"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.items2), function(item, index) {
    return _c('tr', {
      key: item.id
    }, [_c('td', [_c('a', {
      attrs: {
        "href": "#"
      }
    }, [_vm._v(_vm._s(item.product.name))])]), _vm._v(" "), _c('td', [_vm._v(_vm._s(item.quantity))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(item.price_per_item)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(item.quantity * item.price_per_item)))]), _vm._v(" "), _c('td', [(item.status == 'Pending') ? _c('span', {
      staticClass: "label label-primary"
    }, [_vm._v(_vm._s(item.status))]) : _vm._e(), _vm._v(" "), (item.status == 'Shipped') ? _c('span', {
      staticClass: "label label-success"
    }, [_vm._v(_vm._s(item.status))]) : _vm._e(), _vm._v(" "), (item.status == 'Delivered') ? _c('span', {
      staticClass: "label label-warning"
    }, [_vm._v(_vm._s(item.status))]) : _vm._e(), _vm._v(" "), (item.status == 'Cancelled') ? _c('span', {
      staticClass: "label label-danger"
    }, [_vm._v(_vm._s(item.status))]) : _vm._e()]), _vm._v(" "), _c('td', [_vm._v(_vm._s(item.shipped_date))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(item.received_date))])])
  }))])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Product Name")]), _vm._v(" "), _c('th', [_vm._v("Qty")]), _vm._v(" "), _c('th', [_vm._v("Price")]), _vm._v(" "), _c('th', [_vm._v("Subtotal")]), _vm._v(" "), _c('th', [_vm._v("Status")]), _vm._v(" "), _c('th', [_vm._v("Shipped Date")]), _vm._v(" "), _c('th', [_vm._v("Received Date")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-cbff77c8", module.exports)
  }
}

/***/ }),

/***/ 273:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(150);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgM2Q2YWQ2YjJjZGVhZWYxYjY4ZDY/YjEwOSoqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9zYWxlcy5qcyIsIndlYnBhY2s6Ly8vT3JkZXJEZXRhaWxzLnZ1ZSIsIndlYnBhY2s6Ly8vT3JkZXJzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL3NhbGVzL09yZGVyRGV0YWlscy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9zYWxlcy9PcmRlcnMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvc2FsZXMvT3JkZXJzLnZ1ZT82ODBiIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvc2FsZXMvT3JkZXJEZXRhaWxzLnZ1ZT83Mjk5Il0sIm5hbWVzIjpbIlZ1ZSIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJmaWx0ZXIiLCJ2YWx1ZSIsIm51bWJlcldpdGhDb21tYXMiLCJwYXJzZUZsb2F0IiwidG9GaXhlZCIsImFwcCIsImVsIiwiZGF0YSIsIml0ZW1zIiwiaXRlbXMyIiwibm9faXRlbSIsIm9yZGVyX2lkIiwidHJhY2tpbmdfbm8iLCJyZWNlaXZlcl9uYW1lIiwiY3JlYXRlZCIsImdldE9yZGVycyIsImF4aW9zIiwiZ2V0IiwiYWxsIiwidGhlbiIsInNwcmVhZCIsIm9yZGVycyIsInN0YXR1cyIsImNhdGNoIiwiJCIsIm5vb3AiLCJtZXRob2RzIiwib3Blbk1vZGFsIiwiaWQiLCJyZXN1bHQiLCJtb2RhbCIsInNlYXJjaEJ5T3JkZXJJZCIsInNob3ciLCJoaWRlIiwic2VhcmNoQnlUcmFja2luZ05vIiwic2VhcmNoQnlSZWNlaXZlck5hbWUiLCJ4IiwidG9TdHJpbmciLCJyZXBsYWNlIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNsREFBLElBQUlDLFNBQUosQ0FBYyxZQUFkLEVBQTZCLG1CQUFBQyxDQUFRLEdBQVIsQ0FBN0I7QUFDQUYsSUFBSUMsU0FBSixDQUFjLG1CQUFkLEVBQW9DLG1CQUFBQyxDQUFRLEdBQVIsQ0FBcEM7O0FBRUFGLElBQUlHLE1BQUosQ0FBVyxVQUFYLEVBQXVCLFVBQVVDLEtBQVYsRUFBaUI7QUFDcEMsV0FBUSxNQUFJQyxpQkFBaUJDLFdBQVdGLEtBQVgsRUFBa0JHLE9BQWxCLENBQTBCLENBQTFCLENBQWpCLENBQVo7QUFDSCxDQUZEOztBQUlBLElBQUlDLE1BQU0sSUFBSVIsR0FBSixDQUFROztBQUVkUyxRQUFJLGlCQUZVO0FBR2RDLFVBQU07QUFDRkMsZUFBTSxFQURKO0FBRUZDLGdCQUFPLEVBRkw7QUFHRkMsaUJBQVMsRUFIUDtBQUlGQyxrQkFBUyxFQUpQO0FBS0ZDLHFCQUFZLEVBTFY7QUFNRkMsdUJBQWM7QUFOWixLQUhRO0FBV2RDLFdBWGMscUJBV0w7QUFBQTs7QUFDTCxpQkFBU0MsU0FBVCxHQUFxQjtBQUNqQixtQkFBT0MsTUFBTUMsR0FBTixDQUFVLFFBQVYsQ0FBUDtBQUNIO0FBQ0RELGNBQU1FLEdBQU4sQ0FBVSxDQUNOSCxXQURNLENBQVYsRUFFR0ksSUFGSCxDQUVRSCxNQUFNSSxNQUFOLENBQ0osVUFDS0MsTUFETCxFQUVLO0FBQ0wsZ0JBQUdBLE9BQU9kLElBQVAsQ0FBWUEsSUFBWixJQUFvQixDQUF2QixFQUNJLE1BQUtDLEtBQUwsR0FBYWEsT0FBT0MsTUFBUCxJQUFpQixHQUFqQixHQUFzQkQsT0FBT2QsSUFBN0IsR0FBa0MsRUFBL0MsQ0FESixLQUdJLE1BQUtHLE9BQUwsR0FBZSxxQkFBZjtBQUNQLFNBUk8sQ0FGUixFQVdDYSxLQVhELENBV09DLEVBQUVDLElBWFQ7QUFZSCxLQTNCYTs7QUE0QmRDLGFBQVM7QUFDTEMsaUJBREsscUJBQ0tDLEVBREwsRUFDUTtBQUFBOztBQUNUWixrQkFBTUMsR0FBTixDQUFVLG1CQUFtQlcsRUFBN0IsRUFDQ1QsSUFERCxDQUNNLGtCQUFVO0FBQ1osdUJBQUtWLE1BQUwsR0FBY29CLE9BQU90QixJQUFyQjtBQUNBaUIsa0JBQUUsVUFBRixFQUFjTSxLQUFkO0FBQ0gsYUFKRDtBQUtILFNBUEk7QUFRTEMsdUJBUkssNkJBUVk7QUFBQTs7QUFDYixnQkFBRyxLQUFLcEIsUUFBTCxJQUFpQixFQUFwQixFQUF1QjtBQUNsQkssc0JBQU1DLEdBQU4sQ0FBVSxzQkFBc0IsS0FBS04sUUFBckMsRUFDQVEsSUFEQSxDQUNLLGtCQUFVO0FBQ1osMkJBQUtYLEtBQUwsR0FBYXFCLE9BQU90QixJQUFwQjtBQUNILGlCQUhBO0FBSUosYUFMRCxNQU1LO0FBQ0RTLHNCQUFNQyxHQUFOLENBQVUsUUFBVixFQUNDRSxJQURELENBQ00sa0JBQVU7QUFDWiwyQkFBS1gsS0FBTCxHQUFhcUIsT0FBT3RCLElBQXBCO0FBQ0gsaUJBSEQ7QUFJSDtBQUNEaUIsY0FBRSxlQUFGLEVBQW1CUSxJQUFuQjtBQUNBUixjQUFFLGFBQUYsRUFBaUJTLElBQWpCO0FBQ0gsU0F2Qkk7QUF3QkxDLDBCQXhCSyxnQ0F3QmU7QUFBQTs7QUFDaEIsZ0JBQUcsS0FBS3RCLFdBQUwsSUFBb0IsRUFBdkIsRUFBMEI7QUFDckJJLHNCQUFNQyxHQUFOLENBQVUseUJBQXlCLEtBQUtMLFdBQXhDLEVBQ0FPLElBREEsQ0FDSyxrQkFBVTtBQUNaLDJCQUFLWCxLQUFMLEdBQWFxQixPQUFPdEIsSUFBcEI7QUFDSCxpQkFIQTtBQUlKLGFBTEQsTUFNSztBQUNEUyxzQkFBTUMsR0FBTixDQUFVLFFBQVYsRUFDQ0UsSUFERCxDQUNNLGtCQUFVO0FBQ1osMkJBQUtYLEtBQUwsR0FBYXFCLE9BQU90QixJQUFwQjtBQUNILGlCQUhEO0FBSUg7QUFDRGlCLGNBQUUsZUFBRixFQUFtQlEsSUFBbkI7QUFDQVIsY0FBRSxhQUFGLEVBQWlCUyxJQUFqQjtBQUNILFNBdkNJO0FBd0NMRSw0QkF4Q0ssa0NBd0NpQjtBQUFBOztBQUNsQixnQkFBRyxLQUFLdEIsYUFBTCxJQUFzQixFQUF6QixFQUE0QjtBQUN2Qkcsc0JBQU1DLEdBQU4sQ0FBVSwyQkFBMkIsS0FBS0osYUFBMUMsRUFDQU0sSUFEQSxDQUNLLGtCQUFVO0FBQ1osMkJBQUtYLEtBQUwsR0FBYXFCLE9BQU90QixJQUFwQjtBQUNILGlCQUhBO0FBSUosYUFMRCxNQU1LO0FBQ0RTLHNCQUFNQyxHQUFOLENBQVUsUUFBVixFQUNDRSxJQURELENBQ00sa0JBQVU7QUFDWiwyQkFBS1gsS0FBTCxHQUFhcUIsT0FBT3RCLElBQXBCO0FBQ0gsaUJBSEQ7QUFJSDtBQUNEaUIsY0FBRSxlQUFGLEVBQW1CUSxJQUFuQjtBQUNBUixjQUFFLGFBQUYsRUFBaUJTLElBQWpCO0FBQ0g7QUF2REk7O0FBNUJLLENBQVIsQ0FBVjs7QUF5RkEsU0FBUy9CLGdCQUFULENBQTBCa0MsQ0FBMUIsRUFBNkI7QUFDekIsV0FBT0EsRUFBRUMsUUFBRixHQUFhQyxPQUFiLENBQXFCLHVCQUFyQixFQUE4QyxHQUE5QyxDQUFQO0FBQ0gsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvREQ7U0FFQTtBQURBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzJCQTtZQUVBOzswQ0FFQTttQ0FDQTtBQUVBO0FBSkE7QUFGQSxHOzs7Ozs7O0FDaEVBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMxRUEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3NhbGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAyNzMpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDNkNmFkNmIyY2RlYWVmMWI2OGQ2IiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA4IDkgMTAgMTEgMTIgMTMiLCJWdWUuY29tcG9uZW50KCdvcmRlci1saXN0JywgIHJlcXVpcmUoJy4uL2NwYW5lbC9jb21wb25lbnRzL3NhbGVzL09yZGVycy52dWUnKSk7XHJcblZ1ZS5jb21wb25lbnQoJ29yZGVyZGV0YWlscy1saXN0JywgIHJlcXVpcmUoJy4uL2NwYW5lbC9jb21wb25lbnRzL3NhbGVzL09yZGVyRGV0YWlscy52dWUnKSk7XHJcblxyXG5WdWUuZmlsdGVyKCdjdXJyZW5jeScsIGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuICAn4oKxJytudW1iZXJXaXRoQ29tbWFzKHBhcnNlRmxvYXQodmFsdWUpLnRvRml4ZWQoMikpO1xyXG59KTtcclxuXHJcbnZhciBhcHAgPSBuZXcgVnVlKHtcclxuXHJcbiAgICBlbDogJyNvcmRlcnMtY29udGVudCcsXHJcbiAgICBkYXRhOiB7XHJcbiAgICAgICAgaXRlbXM6W10sXHJcbiAgICAgICAgaXRlbXMyOltdLFxyXG4gICAgICAgIG5vX2l0ZW06ICcnLFxyXG4gICAgICAgIG9yZGVyX2lkOicnLFxyXG4gICAgICAgIHRyYWNraW5nX25vOicnLFxyXG4gICAgICAgIHJlY2VpdmVyX25hbWU6JydcclxuICAgIH0sXHJcbiAgICBjcmVhdGVkKCl7XHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0T3JkZXJzKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCdvcmRlcnMnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgYXhpb3MuYWxsKFtcclxuICAgICAgICAgICAgZ2V0T3JkZXJzKClcclxuICAgICAgICBdKS50aGVuKGF4aW9zLnNwcmVhZChcclxuICAgICAgICAgICAgKFxyXG4gICAgICAgICAgICAgICAgIG9yZGVyc1xyXG4gICAgICAgICAgICApID0+IHsgIFxyXG4gICAgICAgICAgICBpZihvcmRlcnMuZGF0YS5kYXRhICE9IDApXHJcbiAgICAgICAgICAgICAgICB0aGlzLml0ZW1zID0gb3JkZXJzLnN0YXR1cyA9PSAyMDA/IG9yZGVycy5kYXRhOltdO1xyXG4gICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vX2l0ZW0gPSBcIlRoZXJlIGFyZSBubyBvcmRlcnNcIjtcclxuICAgICAgICB9KSlcclxuICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgb3Blbk1vZGFsKGlkKXtcclxuICAgICAgICAgICAgYXhpb3MuZ2V0KCcvb3JkZXJEZXRhaWxzLycgKyBpZClcclxuICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXRlbXMyID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICAkKFwiI215TW9kYWxcIikubW9kYWwoKTtcclxuICAgICAgICAgICAgfSk7ICAgICAgICAgXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZWFyY2hCeU9yZGVySWQoKXtcclxuICAgICAgICAgICAgaWYodGhpcy5vcmRlcl9pZCAhPSAnJyl7XHJcbiAgICAgICAgICAgICAgICAgYXhpb3MuZ2V0KCcvc2VhcmNoQnlPcmRlcklkLycgKyB0aGlzLm9yZGVyX2lkKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLml0ZW1zID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KTsgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGF4aW9zLmdldCgnb3JkZXJzJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtcyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgJCgnLmZvb3RhYmxlLW9kZCcpLnNob3coKTtcclxuICAgICAgICAgICAgJCgnI3BhZ2luYXRpb24nKS5oaWRlKCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZWFyY2hCeVRyYWNraW5nTm8oKXtcclxuICAgICAgICAgICAgaWYodGhpcy50cmFja2luZ19ubyAhPSAnJyl7XHJcbiAgICAgICAgICAgICAgICAgYXhpb3MuZ2V0KCcvc2VhcmNoQnlUcmFja2luZ05vLycgKyB0aGlzLnRyYWNraW5nX25vKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLml0ZW1zID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KTsgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGF4aW9zLmdldCgnb3JkZXJzJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtcyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgJCgnLmZvb3RhYmxlLW9kZCcpLnNob3coKTtcclxuICAgICAgICAgICAgJCgnI3BhZ2luYXRpb24nKS5oaWRlKCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZWFyY2hCeVJlY2VpdmVyTmFtZSgpe1xyXG4gICAgICAgICAgICBpZih0aGlzLnJlY2VpdmVyX25hbWUgIT0gJycpe1xyXG4gICAgICAgICAgICAgICAgIGF4aW9zLmdldCgnL3NlYXJjaEJ5UmVjZWl2ZXJOYW1lLycgKyB0aGlzLnJlY2VpdmVyX25hbWUpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXRlbXMgPSByZXN1bHQuZGF0YTtcclxuICAgICAgICAgICAgICAgIH0pOyAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYXhpb3MuZ2V0KCdvcmRlcnMnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLml0ZW1zID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAkKCcuZm9vdGFibGUtb2RkJykuc2hvdygpO1xyXG4gICAgICAgICAgICAkKCcjcGFnaW5hdGlvbicpLmhpZGUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxufSk7XHJcblxyXG5mdW5jdGlvbiBudW1iZXJXaXRoQ29tbWFzKHgpIHtcclxuICAgIHJldHVybiB4LnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIsXCIpO1xyXG59ICAgIFxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9zYWxlcy5qcyIsIjx0ZW1wbGF0ZT5cclxuPGRpdiBjbGFzcz1cInRhYmxlLXJlc3BvbnNpdmVcIj5cclxuICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLXN0cmlwZWRcIj5cclxuICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICA8dGg+UHJvZHVjdCBOYW1lPC90aD5cclxuICAgICAgICAgICAgPHRoPlF0eTwvdGg+XHJcbiAgICAgICAgICAgIDx0aD5QcmljZTwvdGg+XHJcbiAgICAgICAgICAgIDx0aD5TdWJ0b3RhbDwvdGg+XHJcbiAgICAgICAgICAgIDx0aD5TdGF0dXM8L3RoPlxyXG4gICAgICAgICAgICA8dGg+U2hpcHBlZCBEYXRlPC90aD5cclxuICAgICAgICAgICAgPHRoPlJlY2VpdmVkIERhdGU8L3RoPlxyXG4gICAgICAgIDwvdHI+XHJcbiAgICAgICAgPC90aGVhZD5cclxuICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgPHRyIHYtZm9yPVwiKGl0ZW0saW5kZXgpIGluIGl0ZW1zMlwiIDprZXk9XCJpdGVtLmlkXCI+XHJcbiAgICAgICAgICAgIDx0ZD48YSBocmVmPVwiI1wiID57e2l0ZW0ucHJvZHVjdC5uYW1lfX08L2E+PC90ZD5cclxuICAgICAgICAgICAgPHRkPnt7aXRlbS5xdWFudGl0eX19PC90ZD5cclxuICAgICAgICAgICAgPHRkPnt7aXRlbS5wcmljZV9wZXJfaXRlbSB8IGN1cnJlbmN5fX08L3RkPlxyXG4gICAgICAgICAgICA8dGQ+e3tpdGVtLnF1YW50aXR5Kml0ZW0ucHJpY2VfcGVyX2l0ZW0gfCBjdXJyZW5jeX19PC90ZD5cclxuICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gdi1pZj1cIml0ZW0uc3RhdHVzPT0nUGVuZGluZydcIiBjbGFzcz1cImxhYmVsIGxhYmVsLXByaW1hcnlcIj57e2l0ZW0uc3RhdHVzfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiB2LWlmPVwiaXRlbS5zdGF0dXM9PSdTaGlwcGVkJ1wiIGNsYXNzPVwibGFiZWwgbGFiZWwtc3VjY2Vzc1wiPnt7aXRlbS5zdGF0dXN9fTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIHYtaWY9XCJpdGVtLnN0YXR1cz09J0RlbGl2ZXJlZCdcIiBjbGFzcz1cImxhYmVsIGxhYmVsLXdhcm5pbmdcIj57e2l0ZW0uc3RhdHVzfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiB2LWlmPVwiaXRlbS5zdGF0dXM9PSdDYW5jZWxsZWQnXCIgY2xhc3M9XCJsYWJlbCBsYWJlbC1kYW5nZXJcIj57e2l0ZW0uc3RhdHVzfX08L3NwYW4+XHJcbiAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgIDx0ZD57e2l0ZW0uc2hpcHBlZF9kYXRlfX08L3RkPlxyXG4gICAgICAgICAgICA8dGQ+e3tpdGVtLnJlY2VpdmVkX2RhdGV9fTwvdGQ+XHJcbiAgICAgICAgPC90cj5cclxuICAgICAgICA8L3Rib2R5PlxyXG4gICAgPC90YWJsZT5cclxuPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcblx0cHJvcHM6IFsnaXRlbXMyJ11cclxufVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gT3JkZXJEZXRhaWxzLnZ1ZT9jMjY0ZDE2NCIsIjx0ZW1wbGF0ZT5cclxuPGRpdiBjbGFzcz1cImlib3gtY29udGVudFwiPlxyXG4gICAgPHRhYmxlIGlkPVwiZm9vdGFibGVcIiBjbGFzcz1cImZvb3RhYmxlIHRhYmxlIHRhYmxlLXN0cmlwcGVkIHRvZ2dsZS1hcnJvdy10aW55XCIgZGF0YS1wYWdlLXNpemU9XCIxNVwiPlxyXG4gICAgICAgIDx0aGVhZD5cclxuICAgICAgICA8dHI+XHJcblxyXG4gICAgICAgICAgICA8dGg+T3JkZXIgSUQ8L3RoPlxyXG4gICAgICAgICAgICA8dGggZGF0YS1oaWRlPVwicGhvbmVcIj5UcmFja2luZyBOdW1iZXI8L3RoPlxyXG4gICAgICAgICAgICA8dGggZGF0YS1oaWRlPVwicGhvbmVcIj5SZWNlaXZlciBOYW1lPC90aD5cclxuICAgICAgICAgICAgPHRoIGRhdGEtaGlkZT1cInBob25lXCI+Q29udGFjdCBOby48L3RoPlxyXG4gICAgICAgICAgICA8dGggZGF0YS1oaWRlPVwicGhvbmVcIj5BbHQgQ29udGFjdCBOby48L3RoPlxyXG4gICAgICAgICAgICA8dGggZGF0YS1oaWRlPVwicGhvbmVcIj5Ub3RhbDwvdGg+XHJcbiAgICAgICAgICAgIDx0aCBkYXRhLWhpZGU9XCJwaG9uZVwiPlN0YXR1czwvdGg+XHJcbiAgICAgICAgICAgIDx0aCBjbGFzcz1cInRleHQtcmlnaHRcIj5BY3Rpb248L3RoPlxyXG5cclxuICAgICAgICA8L3RyPlxyXG4gICAgICAgIDwvdGhlYWQ+XHJcbiAgICAgICAgPHRib2R5PlxyXG4gICAgICAgIDx0ciB2LWZvcj1cIihpdGVtLGluZGV4KSBpbiBpdGVtc1wiIDprZXk9XCJpdGVtLmlkXCI+XHJcbiAgICAgICAgICAgIDx0ZD5cclxuICAgICAgICAgICAgICAge3tpdGVtLmlkfX1cclxuICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICB7e2l0ZW0udHJhY2tpbmdfbnVtYmVyfX1cclxuICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICB7e2l0ZW0ubmFtZV9vZl9yZWNlaXZlcn19XHJcbiAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgIDx0ZD5cclxuICAgICAgICAgICAgICAge3tpdGVtLmNvbnRhY3RfbnVtYmVyfX1cclxuICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICB7e2l0ZW0uYWx0ZXJuYXRlX2NvbnRhY3RfbnVtYmVyfX1cclxuICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICB7e2l0ZW0udG90YWx9fVxyXG4gICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgICAgICA8dGQ+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiB2LWlmPVwiaXRlbS5zdGF0dXM9PSdDb21wbGV0ZSdcIiBjbGFzcz1cImxhYmVsIGxhYmVsLXByaW1hcnlcIj57e2l0ZW0uc3RhdHVzfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiB2LWVsc2UgY2xhc3M9XCJsYWJlbCBsYWJlbC1kYW5nZXJcIj57e2l0ZW0uc3RhdHVzfX08L3NwYW4+XHJcbiAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgIDx0ZCBjbGFzcz1cInRleHQtcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJidG4tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIEBjbGljaz1cIm9wZW5Nb2RhbChpdGVtLmlkKVwiIGNsYXNzPVwiYnRuLXdoaXRlIGJ0biBidG4teHNcIj5WaWV3PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICA8L3RyPlxyXG5cclxuXHJcblxyXG4gICAgICAgIDwvdGJvZHk+XHJcbiAgICAgICAgPHRmb290IGlkPVwicGFnaW5hdGlvblwiPlxyXG4gICAgICAgIDx0cj5cclxuICAgICAgICAgICAgPHRkIGNvbHNwYW49XCI4XCI+XHJcbiAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJwYWdpbmF0aW9uIHB1bGwtcmlnaHRcIj48L3VsPlxyXG4gICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgIDwvdHI+XHJcbiAgICAgICAgPC90Zm9vdD5cclxuICAgIDwvdGFibGU+XHJcbjwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cdHByb3BzOiBbJ2l0ZW1zJ10sXHJcbiAgICBtZXRob2RzOntcclxuICAgICAgICBvcGVuTW9kYWwoaWQpe1xyXG4gICAgICAgICAgICAgdGhpcy4kcGFyZW50Lm9wZW5Nb2RhbChpZCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBPcmRlcnMudnVlPzZhMTVkNTAzIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vT3JkZXJEZXRhaWxzLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtY2JmZjc3YzhcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vT3JkZXJEZXRhaWxzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxcVXNlcnNcXFxcSWFuT1xcXFxDb2RlXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcY3BhbmVsXFxcXGNvbXBvbmVudHNcXFxcc2FsZXNcXFxcT3JkZXJEZXRhaWxzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIE9yZGVyRGV0YWlscy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtY2JmZjc3YzhcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi1jYmZmNzdjOFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9jb21wb25lbnRzL3NhbGVzL09yZGVyRGV0YWlscy52dWVcbi8vIG1vZHVsZSBpZCA9IDIyNlxuLy8gbW9kdWxlIGNodW5rcyA9IDQiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9PcmRlcnMudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03ZjVmOWI2ZFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9PcmRlcnMudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFxVc2Vyc1xcXFxJYW5PXFxcXENvZGVcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxjcGFuZWxcXFxcY29tcG9uZW50c1xcXFxzYWxlc1xcXFxPcmRlcnMudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gT3JkZXJzLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi03ZjVmOWI2ZFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTdmNWY5YjZkXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvc2FsZXMvT3JkZXJzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjI3XG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlib3gtY29udGVudFwiXG4gIH0sIFtfYygndGFibGUnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9vdGFibGUgdGFibGUgdGFibGUtc3RyaXBwZWQgdG9nZ2xlLWFycm93LXRpbnlcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImZvb3RhYmxlXCIsXG4gICAgICBcImRhdGEtcGFnZS1zaXplXCI6IFwiMTVcIlxuICAgIH1cbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0uaXRlbXMpLCBmdW5jdGlvbihpdGVtLCBpbmRleCkge1xuICAgIHJldHVybiBfYygndHInLCB7XG4gICAgICBrZXk6IGl0ZW0uaWRcbiAgICB9LCBbX2MoJ3RkJywgW192bS5fdihcIlxcclxcbiAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGl0ZW0uaWQpICsgXCJcXHJcXG4gICAgICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihcIlxcclxcbiAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGl0ZW0udHJhY2tpbmdfbnVtYmVyKSArIFwiXFxyXFxuICAgICAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoXCJcXHJcXG4gICAgICAgICAgICAgICBcIiArIF92bS5fcyhpdGVtLm5hbWVfb2ZfcmVjZWl2ZXIpICsgXCJcXHJcXG4gICAgICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihcIlxcclxcbiAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGl0ZW0uY29udGFjdF9udW1iZXIpICsgXCJcXHJcXG4gICAgICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihcIlxcclxcbiAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGl0ZW0uYWx0ZXJuYXRlX2NvbnRhY3RfbnVtYmVyKSArIFwiXFxyXFxuICAgICAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoXCJcXHJcXG4gICAgICAgICAgICAgICBcIiArIF92bS5fcyhpdGVtLnRvdGFsKSArIFwiXFxyXFxuICAgICAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFsoaXRlbS5zdGF0dXMgPT0gJ0NvbXBsZXRlJykgPyBfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImxhYmVsIGxhYmVsLXByaW1hcnlcIlxuICAgIH0sIFtfdm0uX3YoX3ZtLl9zKGl0ZW0uc3RhdHVzKSldKSA6IF9jKCdzcGFuJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGFiZWwgbGFiZWwtZGFuZ2VyXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhpdGVtLnN0YXR1cykpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwidGV4dC1yaWdodFwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJidG4tZ3JvdXBcIlxuICAgIH0sIFtfYygnYnV0dG9uJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuLXdoaXRlIGJ0biBidG4teHNcIixcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLm9wZW5Nb2RhbChpdGVtLmlkKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlZpZXdcIildKV0pXSldKVxuICB9KSksIF92bS5fdihcIiBcIiksIF9jKCd0Zm9vdCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcInBhZ2luYXRpb25cIlxuICAgIH1cbiAgfSwgW19jKCd0cicsIFtfYygndGQnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiY29sc3BhblwiOiBcIjhcIlxuICAgIH1cbiAgfSwgW19jKCd1bCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYWdpbmF0aW9uIHB1bGwtcmlnaHRcIlxuICB9KV0pXSldKV0sIDEpXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygndGhlYWQnLCBbX2MoJ3RyJywgW19jKCd0aCcsIFtfdm0uX3YoXCJPcmRlciBJRFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1oaWRlXCI6IFwicGhvbmVcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlRyYWNraW5nIE51bWJlclwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1oaWRlXCI6IFwicGhvbmVcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlJlY2VpdmVyIE5hbWVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtaGlkZVwiOiBcInBob25lXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJDb250YWN0IE5vLlwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1oaWRlXCI6IFwicGhvbmVcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkFsdCBDb250YWN0IE5vLlwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1oaWRlXCI6IFwicGhvbmVcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlRvdGFsXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLWhpZGVcIjogXCJwaG9uZVwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiU3RhdHVzXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LXJpZ2h0XCJcbiAgfSwgW192bS5fdihcIkFjdGlvblwiKV0pXSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTdmNWY5YjZkXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtN2Y1ZjliNmRcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvc2FsZXMvT3JkZXJzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjQxXG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYmxlLXJlc3BvbnNpdmVcIlxuICB9LCBbX2MoJ3RhYmxlJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYmxlIHRhYmxlLXN0cmlwZWRcIlxuICB9LCBbX3ZtLl9tKDApLCBfdm0uX3YoXCIgXCIpLCBfYygndGJvZHknLCBfdm0uX2woKF92bS5pdGVtczIpLCBmdW5jdGlvbihpdGVtLCBpbmRleCkge1xuICAgIHJldHVybiBfYygndHInLCB7XG4gICAgICBrZXk6IGl0ZW0uaWRcbiAgICB9LCBbX2MoJ3RkJywgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgICB9XG4gICAgfSwgW192bS5fdihfdm0uX3MoaXRlbS5wcm9kdWN0Lm5hbWUpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKGl0ZW0ucXVhbnRpdHkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShpdGVtLnByaWNlX3Blcl9pdGVtKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKGl0ZW0ucXVhbnRpdHkgKiBpdGVtLnByaWNlX3Blcl9pdGVtKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFsoaXRlbS5zdGF0dXMgPT0gJ1BlbmRpbmcnKSA/IF9jKCdzcGFuJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGFiZWwgbGFiZWwtcHJpbWFyeVwiXG4gICAgfSwgW192bS5fdihfdm0uX3MoaXRlbS5zdGF0dXMpKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChpdGVtLnN0YXR1cyA9PSAnU2hpcHBlZCcpID8gX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1zdWNjZXNzXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhpdGVtLnN0YXR1cykpXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKGl0ZW0uc3RhdHVzID09ICdEZWxpdmVyZWQnKSA/IF9jKCdzcGFuJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGFiZWwgbGFiZWwtd2FybmluZ1wiXG4gICAgfSwgW192bS5fdihfdm0uX3MoaXRlbS5zdGF0dXMpKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChpdGVtLnN0YXR1cyA9PSAnQ2FuY2VsbGVkJykgPyBfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImxhYmVsIGxhYmVsLWRhbmdlclwiXG4gICAgfSwgW192bS5fdihfdm0uX3MoaXRlbS5zdGF0dXMpKV0pIDogX3ZtLl9lKCldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoaXRlbS5zaGlwcGVkX2RhdGUpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhpdGVtLnJlY2VpdmVkX2RhdGUpKV0pXSlcbiAgfSkpXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0aGVhZCcsIFtfYygndHInLCBbX2MoJ3RoJywgW192bS5fdihcIlByb2R1Y3QgTmFtZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiUXR5XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJQcmljZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiU3VidG90YWxcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIlN0YXR1c1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiU2hpcHBlZCBEYXRlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJSZWNlaXZlZCBEYXRlXCIpXSldKV0pXG59XX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtY2JmZjc3YzhcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1jYmZmNzdjOFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9zYWxlcy9PcmRlckRldGFpbHMudnVlXG4vLyBtb2R1bGUgaWQgPSAyNDZcbi8vIG1vZHVsZSBjaHVua3MgPSA0Il0sInNvdXJjZVJvb3QiOiIifQ==