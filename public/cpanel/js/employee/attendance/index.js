/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 462);
/******/ })
/************************************************************************/
/******/ ({

/***/ 172:
/***/ (function(module, exports) {

new Vue({
	el: '#attendance_monitoring',

	data: {
		loading: true,

		search: '',

		current: true,

		selectMonths: [{ value: '1', text: 'January' }, { value: '2', text: 'February' }, { value: '3', text: 'March' }, { value: '4', text: 'April' }, { value: '5', text: 'May' }, { value: '6', text: 'June' }, { value: '7', text: 'July' }, { value: '8', text: 'August' }, { value: '9', text: 'September' }, { value: '10', text: 'October' }, { value: '11', text: 'November' }, { value: '12', text: 'December' }],

		selectYears: ['2018', '2019', '2020', '2021', '2022'],

		filterMonth: new Date().getMonth() + 1,
		filterYear: new Date().getFullYear(),

		numberOfDays: 0,

		employees: []
	},

	methods: {
		setNumberOfDays: function setNumberOfDays() {
			this.numberOfDays = new Date(this.filterYear, this.filterMonth, 0).getDate();
		},
		setCurrent: function setCurrent() {
			if (this.filterMonth == new Date().getMonth() + 1 && this.filterYear == new Date().getFullYear()) this.current = true;else this.current = false;
		},
		refreshTableHeadFixer: function refreshTableHeadFixer() {
			setTimeout(function () {
				$("#fixTable").tableHeadFixer({ "head": false, "left": 1 });
			}, 1000);
		},
		getAttendance: function getAttendance() {
			var _this = this;

			this.loading = true;

			axiosAPIv1.get('/attendance', {
				params: {
					filterMonth: this.filterMonth,
					filterYear: this.filterYear,
					numberOfDays: this.numberOfDays
				}
			}).then(function (response) {
				_this.employees = response.data.employees;

				setTimeout(function () {
					_this.loading = false;

					$("#fixTable").tableHeadFixer({ "head": false, "left": 1 });

					var position = _this.current ? 200 * new Date().getDate() - 200 : 0;
					$("#fixTable-wrapper").animate({ scrollLeft: position }, 100);
				}, 1000);
			});
		},
		filter: function filter() {
			this.setCurrent();

			this.getAttendance();
		}
	},

	created: function created() {
		this.setNumberOfDays();

		this.getAttendance();
	},


	computed: {
		filteredEmployees: function filteredEmployees() {
			var _this2 = this;

			return this.employees.filter(function (employee) {
				return employee.first_name.toLowerCase().indexOf(_this2.search.toLowerCase()) > -1 || employee.last_name.toLowerCase().indexOf(_this2.search.toLowerCase()) > -1;
			});
		}
	}
});

/***/ }),

/***/ 462:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(172);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hdHRlbmRhbmNlL2luZGV4LmpzIl0sIm5hbWVzIjpbIlZ1ZSIsImVsIiwiZGF0YSIsImxvYWRpbmciLCJzZWFyY2giLCJjdXJyZW50Iiwic2VsZWN0TW9udGhzIiwidmFsdWUiLCJ0ZXh0Iiwic2VsZWN0WWVhcnMiLCJmaWx0ZXJNb250aCIsIkRhdGUiLCJnZXRNb250aCIsImZpbHRlclllYXIiLCJnZXRGdWxsWWVhciIsIm51bWJlck9mRGF5cyIsImVtcGxveWVlcyIsIm1ldGhvZHMiLCJzZXROdW1iZXJPZkRheXMiLCJnZXREYXRlIiwic2V0Q3VycmVudCIsInJlZnJlc2hUYWJsZUhlYWRGaXhlciIsInNldFRpbWVvdXQiLCIkIiwidGFibGVIZWFkRml4ZXIiLCJnZXRBdHRlbmRhbmNlIiwiYXhpb3NBUEl2MSIsImdldCIsInBhcmFtcyIsInRoZW4iLCJyZXNwb25zZSIsInBvc2l0aW9uIiwiYW5pbWF0ZSIsInNjcm9sbExlZnQiLCJmaWx0ZXIiLCJjcmVhdGVkIiwiY29tcHV0ZWQiLCJmaWx0ZXJlZEVtcGxveWVlcyIsImVtcGxveWVlIiwiZmlyc3RfbmFtZSIsInRvTG93ZXJDYXNlIiwiaW5kZXhPZiIsImxhc3RfbmFtZSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQSxJQUFJQSxHQUFKLENBQVE7QUFDUEMsS0FBRyx3QkFESTs7QUFHUEMsT0FBSztBQUNKQyxXQUFTLElBREw7O0FBR0pDLFVBQVEsRUFISjs7QUFLSkMsV0FBUyxJQUxMOztBQU9KQyxnQkFBYyxDQUNiLEVBQUNDLE9BQU8sR0FBUixFQUFhQyxNQUFNLFNBQW5CLEVBRGEsRUFFYixFQUFDRCxPQUFPLEdBQVIsRUFBYUMsTUFBTSxVQUFuQixFQUZhLEVBR2IsRUFBQ0QsT0FBTyxHQUFSLEVBQWFDLE1BQU0sT0FBbkIsRUFIYSxFQUliLEVBQUNELE9BQU8sR0FBUixFQUFhQyxNQUFNLE9BQW5CLEVBSmEsRUFLYixFQUFDRCxPQUFPLEdBQVIsRUFBYUMsTUFBTSxLQUFuQixFQUxhLEVBTWIsRUFBQ0QsT0FBTyxHQUFSLEVBQWFDLE1BQU0sTUFBbkIsRUFOYSxFQU9iLEVBQUNELE9BQU8sR0FBUixFQUFhQyxNQUFNLE1BQW5CLEVBUGEsRUFRYixFQUFDRCxPQUFPLEdBQVIsRUFBYUMsTUFBTSxRQUFuQixFQVJhLEVBU2IsRUFBQ0QsT0FBTyxHQUFSLEVBQWFDLE1BQU0sV0FBbkIsRUFUYSxFQVViLEVBQUNELE9BQU8sSUFBUixFQUFjQyxNQUFNLFNBQXBCLEVBVmEsRUFXYixFQUFDRCxPQUFPLElBQVIsRUFBY0MsTUFBTSxVQUFwQixFQVhhLEVBWWIsRUFBQ0QsT0FBTyxJQUFSLEVBQWNDLE1BQU0sVUFBcEIsRUFaYSxDQVBWOztBQXNCSkMsZUFBYSxDQUFDLE1BQUQsRUFBUyxNQUFULEVBQWlCLE1BQWpCLEVBQXlCLE1BQXpCLEVBQWlDLE1BQWpDLENBdEJUOztBQXdCSkMsZUFBYSxJQUFJQyxJQUFKLEdBQVdDLFFBQVgsS0FBd0IsQ0F4QmpDO0FBeUJKQyxjQUFZLElBQUlGLElBQUosR0FBV0csV0FBWCxFQXpCUjs7QUEyQkpDLGdCQUFjLENBM0JWOztBQTZCSkMsYUFBVTtBQTdCTixFQUhFOztBQW1DUEMsVUFBUztBQUNSQyxpQkFEUSw2QkFDVTtBQUNqQixRQUFLSCxZQUFMLEdBQW9CLElBQUlKLElBQUosQ0FBUyxLQUFLRSxVQUFkLEVBQTBCLEtBQUtILFdBQS9CLEVBQTRDLENBQTVDLEVBQStDUyxPQUEvQyxFQUFwQjtBQUNBLEdBSE87QUFJUkMsWUFKUSx3QkFJSztBQUNaLE9BQUcsS0FBS1YsV0FBTCxJQUFvQixJQUFJQyxJQUFKLEdBQVdDLFFBQVgsS0FBd0IsQ0FBNUMsSUFBaUQsS0FBS0MsVUFBTCxJQUFtQixJQUFJRixJQUFKLEdBQVdHLFdBQVgsRUFBdkUsRUFDQyxLQUFLVCxPQUFMLEdBQWUsSUFBZixDQURELEtBR0MsS0FBS0EsT0FBTCxHQUFlLEtBQWY7QUFDRCxHQVRPO0FBVVJnQix1QkFWUSxtQ0FVZ0I7QUFDdkJDLGNBQVcsWUFBTTtBQUNkQyxNQUFFLFdBQUYsRUFBZUMsY0FBZixDQUE4QixFQUFDLFFBQVMsS0FBVixFQUFpQixRQUFTLENBQTFCLEVBQTlCO0FBQ0EsSUFGSCxFQUVLLElBRkw7QUFHQSxHQWRPO0FBZVJDLGVBZlEsMkJBZVE7QUFBQTs7QUFDZixRQUFLdEIsT0FBTCxHQUFlLElBQWY7O0FBRUF1QixjQUFXQyxHQUFYLENBQWUsYUFBZixFQUE4QjtBQUN6QkMsWUFBUTtBQUNMbEIsa0JBQWEsS0FBS0EsV0FEYjtBQUVMRyxpQkFBWSxLQUFLQSxVQUZaO0FBR0xFLG1CQUFjLEtBQUtBO0FBSGQ7QUFEaUIsSUFBOUIsRUFPSWMsSUFQSixDQU9TLFVBQUNDLFFBQUQsRUFBYztBQUNuQixVQUFLZCxTQUFMLEdBQWlCYyxTQUFTNUIsSUFBVCxDQUFjYyxTQUEvQjs7QUFFQU0sZUFBVyxZQUFNO0FBQ2hCLFdBQUtuQixPQUFMLEdBQWUsS0FBZjs7QUFFQW9CLE9BQUUsV0FBRixFQUFlQyxjQUFmLENBQThCLEVBQUMsUUFBUyxLQUFWLEVBQWlCLFFBQVMsQ0FBMUIsRUFBOUI7O0FBRUEsU0FBSU8sV0FBWSxNQUFLMUIsT0FBTixHQUFrQixNQUFPLElBQUlNLElBQUosR0FBV1EsT0FBWCxFQUFSLEdBQWlDLEdBQWxELEdBQXdELENBQXZFO0FBQ0FJLE9BQUUsbUJBQUYsRUFBdUJTLE9BQXZCLENBQStCLEVBQUNDLFlBQVlGLFFBQWIsRUFBL0IsRUFBdUQsR0FBdkQ7QUFDQSxLQVBELEVBT0csSUFQSDtBQVFBLElBbEJKO0FBbUJBLEdBckNPO0FBc0NSRyxRQXRDUSxvQkFzQ0M7QUFDUixRQUFLZCxVQUFMOztBQUVBLFFBQUtLLGFBQUw7QUFDQTtBQTFDTyxFQW5DRjs7QUFnRlBVLFFBaEZPLHFCQWdGRztBQUNULE9BQUtqQixlQUFMOztBQUVBLE9BQUtPLGFBQUw7QUFDQSxFQXBGTTs7O0FBc0ZQVyxXQUFVO0FBQ05DLG1CQURNLCtCQUNjO0FBQUE7O0FBQ2pCLFVBQU8sS0FBS3JCLFNBQUwsQ0FBZWtCLE1BQWYsQ0FBc0Isb0JBQVk7QUFDdEMsV0FBUUksU0FBU0MsVUFBVCxDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTBDLE9BQUtyQyxNQUFMLENBQVlvQyxXQUFaLEVBQTFDLElBQXVFLENBQUMsQ0FBekUsSUFDSEYsU0FBU0ksU0FBVCxDQUFtQkYsV0FBbkIsR0FBaUNDLE9BQWpDLENBQXlDLE9BQUtyQyxNQUFMLENBQVlvQyxXQUFaLEVBQXpDLElBQXNFLENBQUMsQ0FEM0U7QUFFRixJQUhNLENBQVA7QUFJRjtBQU5LO0FBdEZILENBQVIsRSIsImZpbGUiOiJqcy9lbXBsb3llZS9hdHRlbmRhbmNlL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0NjIpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGYxMTNlZDM2YTVhNTZiN2RjZjYzIiwibmV3IFZ1ZSh7XG5cdGVsOicjYXR0ZW5kYW5jZV9tb25pdG9yaW5nJyxcblxuXHRkYXRhOntcblx0XHRsb2FkaW5nOiB0cnVlLFxuXG5cdFx0c2VhcmNoOiAnJyxcblxuXHRcdGN1cnJlbnQ6IHRydWUsXG5cblx0XHRzZWxlY3RNb250aHM6IFtcblx0XHRcdHt2YWx1ZTogJzEnLCB0ZXh0OiAnSmFudWFyeSd9LFxuXHRcdFx0e3ZhbHVlOiAnMicsIHRleHQ6ICdGZWJydWFyeSd9LFxuXHRcdFx0e3ZhbHVlOiAnMycsIHRleHQ6ICdNYXJjaCd9LFxuXHRcdFx0e3ZhbHVlOiAnNCcsIHRleHQ6ICdBcHJpbCd9LFxuXHRcdFx0e3ZhbHVlOiAnNScsIHRleHQ6ICdNYXknfSxcblx0XHRcdHt2YWx1ZTogJzYnLCB0ZXh0OiAnSnVuZSd9LFxuXHRcdFx0e3ZhbHVlOiAnNycsIHRleHQ6ICdKdWx5J30sXG5cdFx0XHR7dmFsdWU6ICc4JywgdGV4dDogJ0F1Z3VzdCd9LFxuXHRcdFx0e3ZhbHVlOiAnOScsIHRleHQ6ICdTZXB0ZW1iZXInfSxcblx0XHRcdHt2YWx1ZTogJzEwJywgdGV4dDogJ09jdG9iZXInfSxcblx0XHRcdHt2YWx1ZTogJzExJywgdGV4dDogJ05vdmVtYmVyJ30sXG5cdFx0XHR7dmFsdWU6ICcxMicsIHRleHQ6ICdEZWNlbWJlcid9XG5cdFx0XSxcblxuXHRcdHNlbGVjdFllYXJzOiBbJzIwMTgnLCAnMjAxOScsICcyMDIwJywgJzIwMjEnLCAnMjAyMiddLFxuXG5cdFx0ZmlsdGVyTW9udGg6IG5ldyBEYXRlKCkuZ2V0TW9udGgoKSArIDEsXG5cdFx0ZmlsdGVyWWVhcjogbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpLFxuXG5cdFx0bnVtYmVyT2ZEYXlzOiAwLFxuXG5cdFx0ZW1wbG95ZWVzOltdXG5cdH0sXG5cblx0bWV0aG9kczoge1xuXHRcdHNldE51bWJlck9mRGF5cygpIHtcblx0XHRcdHRoaXMubnVtYmVyT2ZEYXlzID0gbmV3IERhdGUodGhpcy5maWx0ZXJZZWFyLCB0aGlzLmZpbHRlck1vbnRoLCAwKS5nZXREYXRlKCk7XG5cdFx0fSxcblx0XHRzZXRDdXJyZW50KCkge1xuXHRcdFx0aWYodGhpcy5maWx0ZXJNb250aCA9PSBuZXcgRGF0ZSgpLmdldE1vbnRoKCkgKyAxICYmIHRoaXMuZmlsdGVyWWVhciA9PSBuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCkpXG5cdFx0XHRcdHRoaXMuY3VycmVudCA9IHRydWU7XG5cdFx0XHRlbHNlXG5cdFx0XHRcdHRoaXMuY3VycmVudCA9IGZhbHNlO1xuXHRcdH0sXG5cdFx0cmVmcmVzaFRhYmxlSGVhZEZpeGVyKCkge1xuXHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XG5cdFx0ICBcdFx0JChcIiNmaXhUYWJsZVwiKS50YWJsZUhlYWRGaXhlcih7XCJoZWFkXCIgOiBmYWxzZSwgXCJsZWZ0XCIgOiAxfSk7XG5cdFx0ICBcdH0sIDEwMDApO1xuXHRcdH0sXG5cdFx0Z2V0QXR0ZW5kYW5jZSgpIHtcblx0XHRcdHRoaXMubG9hZGluZyA9IHRydWU7XG5cblx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvYXR0ZW5kYW5jZScsIHtcblx0XHRcdFx0ICAgIHBhcmFtczoge1xuXHRcdFx0XHQgICAgICBcdGZpbHRlck1vbnRoOiB0aGlzLmZpbHRlck1vbnRoLFxuXHRcdFx0XHQgICAgICBcdGZpbHRlclllYXI6IHRoaXMuZmlsdGVyWWVhcixcblx0XHRcdFx0ICAgICAgXHRudW1iZXJPZkRheXM6IHRoaXMubnVtYmVyT2ZEYXlzXG5cdFx0XHRcdCAgICB9XG5cdFx0XHRcdH0pXG5cdFx0ICBcdFx0LnRoZW4oKHJlc3BvbnNlKSA9PiB7XG5cdFx0ICBcdFx0XHR0aGlzLmVtcGxveWVlcyA9IHJlc3BvbnNlLmRhdGEuZW1wbG95ZWVzO1xuXG5cdFx0ICBcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHQgIFx0XHRcdFx0dGhpcy5sb2FkaW5nID0gZmFsc2U7XG5cblx0XHQgIFx0XHRcdFx0JChcIiNmaXhUYWJsZVwiKS50YWJsZUhlYWRGaXhlcih7XCJoZWFkXCIgOiBmYWxzZSwgXCJsZWZ0XCIgOiAxfSk7XG5cblx0XHQgIFx0XHRcdFx0bGV0IHBvc2l0aW9uID0gKHRoaXMuY3VycmVudCkgPyAoMjAwICogKG5ldyBEYXRlKCkuZ2V0RGF0ZSgpKSkgLSAyMDAgOiAwO1xuXHRcdCAgXHRcdFx0XHQkKFwiI2ZpeFRhYmxlLXdyYXBwZXJcIikuYW5pbWF0ZSh7c2Nyb2xsTGVmdDogcG9zaXRpb259LCAxMDApO1xuXHRcdCAgXHRcdFx0fSwgMTAwMCk7XG5cdFx0ICBcdFx0fSk7XG5cdFx0fSxcblx0XHRmaWx0ZXIoKSB7XG5cdFx0XHR0aGlzLnNldEN1cnJlbnQoKTtcblxuXHRcdFx0dGhpcy5nZXRBdHRlbmRhbmNlKCk7XG5cdFx0fVxuXHR9LFxuXG5cdGNyZWF0ZWQoKSB7XG5cdFx0dGhpcy5zZXROdW1iZXJPZkRheXMoKTtcblxuXHRcdHRoaXMuZ2V0QXR0ZW5kYW5jZSgpO1xuXHR9LFxuXG5cdGNvbXB1dGVkOiB7XG5cdCAgICBmaWx0ZXJlZEVtcGxveWVlcygpIHtcblx0ICAgICAgXHRyZXR1cm4gdGhpcy5lbXBsb3llZXMuZmlsdGVyKGVtcGxveWVlID0+IHtcblx0ICAgICAgICAgXHRyZXR1cm4gKGVtcGxveWVlLmZpcnN0X25hbWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKHRoaXMuc2VhcmNoLnRvTG93ZXJDYXNlKCkpID4gLTEpIFxuXHQgICAgICAgICBcdHx8IChlbXBsb3llZS5sYXN0X25hbWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKHRoaXMuc2VhcmNoLnRvTG93ZXJDYXNlKCkpID4gLTEpO1xuXHQgICAgICBcdH0pO1xuXHQgICAgfVxuXHR9XG59KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2F0dGVuZGFuY2UvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9