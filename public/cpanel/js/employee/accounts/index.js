/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 460);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 170:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('dialog-modal', __webpack_require__(360));

var UserApp = new Vue({
	el: '#userlist',
	data: {
		users: [],
		seluser: '',

		selectedUser: null
	},
	methods: {
		showRemoveDialog: function showRemoveDialog(user) {
			this.seluser = user;

			$('#dialogRemove').modal('toggle');
		},
		getUsers: function getUsers() {
			var _this = this;

			function getUsers() {
				return axiosAPIv1.get('/employees');
			}
			axios.all([getUsers()]).then(axios.spread(function (users) {
				_this.users = users.data;
			})).catch($.noop);
		},
		deleteUser: function deleteUser() {
			var _this2 = this;

			axiosAPIv1.delete('/users/' + this.seluser.id).then(function (response) {
				toastr.success('User Deleted!');

				var self = _this2;
				axiosAPIv1.get('/users').then(function (response) {
					self.users = response.data;
				});
			});
		},
		editSchedule: function editSchedule(user) {
			this.$refs.editschedule.setSelectedUser(user);

			$('#editSchedule').modal('show');
		}
	},
	created: function created() {
		this.getUsers();
	},
	updated: function updated() {
		$('#lists').DataTable({
			pageLength: 10,
			responsive: true,
			dom: '<"top"lf>rt<"bottom"ip><"clear">'

		});
	},

	components: {
		'edit-schedule': __webpack_require__(28)
	}
});

$(document).ready(function () {

	$('#first_name, #middle_name, #last_name').keypress(function (event) {
		var inputValue = event.charCode;
		//alert(inputValue);
		if (!(inputValue > 64 && inputValue < 91 || inputValue > 96 && inputValue < 123 || inputValue == 32 || inputValue == 0)) {
			event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
});

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['selecteduser'],

	data: function data() {
		return {
			scheduleTypes: [],

			editScheduleForm: new Form({
				userId: '',
				scheduleType: '',
				timeIn: '',
				timeOut: '',
				timeInFrom: '',
				timeInTo: ''
			}, { baseURL: axiosAPIv1.defaults.baseURL })
		};
	},


	methods: {
		initTimePicker: function initTimePicker() {
			var _this = this;

			setTimeout(function () {
				$('.timepicker-input').timepicker();
			}, 500);

			$('.timepicker-input').on('click', function (e) {
				var target = e.target;

				$('#' + target.id).timepicker('showWidget');

				if (target.value == '') {
					$('#' + target.id).timepicker('setTime', '12:00 AM');
				}
			});

			$('.timepicker-input').timepicker().on('changeTime.timepicker', function (e) {
				var id = e.target.id;

				if (id == 'timepicker1') {
					_this.editScheduleForm.timeIn = e.time.value;
				} else if (id == 'timepicker2') {
					_this.editScheduleForm.timeOut = e.time.value;
				} else if (id == 'timepicker3') {
					_this.editScheduleForm.timeInFrom = e.time.value;
				} else if (id == 'timepicker4') {
					_this.editScheduleForm.timeInTo = e.time.value;
				}
			});
		},
		getScheduleTypes: function getScheduleTypes() {
			var _this2 = this;

			axiosAPIv1.get('/schedule-types').then(function (response) {
				_this2.scheduleTypes = response.data;
			});
		},
		resetEditScheduleForm: function resetEditScheduleForm() {
			this.editScheduleForm = new Form({
				userId: '',
				scheduleType: '',
				timeIn: '',
				timeOut: '',
				timeInFrom: '',
				timeInTo: ''
			}, { baseURL: axiosAPIv1.defaults.baseURL });
		},
		setSelectedUser: function setSelectedUser(user) {
			this.editScheduleForm.userId = user.id;

			if (user.schedule) {
				this.editScheduleForm.scheduleType = user.schedule.schedule_type_id;
			} else {
				this.editScheduleForm.scheduleType = '';
			}

			if (user.schedule && user.schedule.time_in && user.schedule.time_out) {
				this.editScheduleForm.timeIn = moment(user.schedule.time_in, 'HH:mm:ss').format("h:mm A");
				this.editScheduleForm.timeOut = moment(user.schedule.time_out, 'HH:mm:ss').format("h:mm A");
			} else {
				this.editScheduleForm.timeIn = '';
				this.editScheduleForm.timeOut = '';
			}

			if (user.schedule && user.schedule.time_in_from && user.schedule.time_in_to) {
				this.editScheduleForm.timeInFrom = moment(user.schedule.time_in_from, 'HH:mm:ss').format("h:mm A");
				this.editScheduleForm.timeInTo = moment(user.schedule.time_in_to, 'HH:mm:ss').format("h:mm A");
			} else {
				this.editScheduleForm.timeInFrom = '';
				this.editScheduleForm.timeInTo = '';
			}
		},
		editSchedule: function editSchedule() {
			var _this3 = this;

			this.editScheduleForm.submit('patch', '/schedule/' + this.editScheduleForm.userId).then(function (response) {
				if (response.success) {
					_this3.$parent.$parent.getUsers();

					_this3.resetEditScheduleForm();

					$('#editSchedule').modal('hide');

					toastr.success(response.message);
				}
			}).catch(function (error) {
				var errors = JSON.parse(JSON.stringify(error));

				for (var key in errors) {
					var _error = errors[key].message;

					_error.forEach(function (e) {
						toastr.error(e);
					});
				}
			});
		}
	},

	created: function created() {
		this.getScheduleTypes();
	},
	mounted: function mounted() {
		this.initTimePicker();
	}
});

/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
exports.push([module.i, "\nform[data-v-9f916844] {\n\tmargin-bottom: 30px;\n}\n.m-r-10[data-v-9f916844] {\n\tmargin-right: 10px;\n}\n", "", {"version":3,"sources":["edit-schedule.vue?d46060ce"],"names":[],"mappings":";AAoMA;CACA,oBAAA;CACA;AACA;CACA,mBAAA;CACA","file":"edit-schedule.vue","sourcesContent":["<template>\n\t\n\t<div>\n\t\t\n\t\t<form class=\"form-horizontal\" @submit.prevent=\"editSchedule\">\n\n\t\t\t<div class=\"form-group\" :class=\"{ 'has-error': editScheduleForm.errors.has('schedule_type') }\">\n\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t        Schedule Type: <span class=\"text-danger\">*</span>\n\t\t        </label>\n\t\t\t    <div class=\"col-md-10\">\n\t\t\t    \t<select class=\"form-control\" name=\"schedule_type\" v-model=\"editScheduleForm.scheduleType\">\n\t\t\t    \t\t<option v-for=\"scheduleType in scheduleTypes\" :value=\"scheduleType.id\">\n\t\t\t    \t\t\t{{ scheduleType.name }}\n\t\t\t    \t\t</option>\n\t\t\t    \t</select>\n\t\t\t    \t<span class=\"help-block\" v-if=\"editScheduleForm.errors.has('schedule_type')\" v-text=\"editScheduleForm.errors.get('schedule_type')\"></span>\n\t\t\t    </div>\n\t\t\t</div>\n\n\t\t\t<div v-show=\"editScheduleForm.scheduleType == 1 ||  editScheduleForm.scheduleType == 2\" class=\"form-group\">\n\t\t\t    <label class=\"col-md-2 control-label\">\n\t\t\t        Schedule Details: <span class=\"text-danger\">*</span>\n\t\t        </label>\n\t\t\t    <div class=\"col-md-10\">\n\n\t\t\t    \t<div v-show=\"editScheduleForm.scheduleType == 1\">\n\t\t\t    \t\t<div class=\"input-group\">\n\t\t\t    \t\t\t<div class=\"bootstrap-timepicker timepicker\">\n\t\t\t\t\t            <input id=\"timepicker1\" type=\"text\" class=\"form-control input-small timepicker-input\" v-model=\"editScheduleForm.timeIn\" autocomplete=\"off\">\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <span class=\"input-group-addon\">to</span>\n\t\t\t\t\t        <div class=\"bootstrap-timepicker timepicker\">\n\t\t\t\t\t            <input id=\"timepicker2\" type=\"text\" class=\"form-control input-small timepicker-input\" v-model=\"editScheduleForm.timeOut\" autocomplete=\"off\">\n\t\t\t\t\t        </div>\n\t\t\t    \t\t</div>\n\t\t\t    \t</div>\n\n\t\t\t    \t<div v-show=\"editScheduleForm.scheduleType == 2\">\n\t\t\t    \t\t<div class=\"input-group\">\n\t\t\t    \t\t\t<span class=\"input-group-addon\">Time In From</span>\n\t\t\t    \t\t\t<div class=\"bootstrap-timepicker timepicker\">\n\t\t\t\t\t            <input id=\"timepicker3\" type=\"text\" class=\"form-control input-small timepicker-input\" v-model=\"editScheduleForm.timeInFrom\" autocomplete=\"off\">\n\t\t\t\t\t        </div>\n\t\t\t    \t\t</div>\n\t\t\t    \t\t<div style=\"height:10px; clear:both;\"></div>\n\t\t\t    \t\t<div class=\"input-group\">\n\t\t\t    \t\t\t<span class=\"input-group-addon\">Time In To</span>\n\t\t\t    \t\t\t<div class=\"bootstrap-timepicker timepicker\">\n\t\t\t\t\t            <input id=\"timepicker4\" type=\"text\" class=\"form-control input-small timepicker-input\" v-model=\"editScheduleForm.timeInTo\" autocomplete=\"off\">\n\t\t\t\t\t        </div>\n\t\t\t    \t\t</div>\n\t\t\t    \t</div>\n\n\t\t\t    </div>\n\t\t\t</div>\n\n\t\t\t<button type=\"submit\" class=\"btn btn-primary pull-right\">Edit</button>\n\t    \t<button type=\"button\" class=\"btn btn-default pull-right m-r-10\" data-dismiss=\"modal\">Close</button>\n\n\t\t</form>\n\n\t</div>\n\n</template>\n\n<script>\n\t\n\texport default {\n\t\tprops: ['selecteduser'],\n\n\t\tdata() {\n\t\t\treturn {\n\t\t\t\tscheduleTypes: [],\n\n\t\t\t\teditScheduleForm: new Form({\n\t\t\t\t\tuserId: '',\n\t\t\t\t\tscheduleType: '',\n\t\t\t\t\ttimeIn: '',\n\t\t\t\t\ttimeOut: '',\n\t\t\t\t\ttimeInFrom: '',\n\t\t\t\t\ttimeInTo: ''\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL })\n\t\t\t}\n\t\t},\n\n\t\tmethods: {\n\t\t\tinitTimePicker() {\n\t\t\t\tsetTimeout(() => {\n\t\t\t\t\t$('.timepicker-input').timepicker();\n\t\t\t\t}, 500);\n\n\t\t\t\t$('.timepicker-input').on('click', function(e) {\n\t\t\t\t\tlet target = e.target;\n\n\t\t\t\t\t$('#' + target.id).timepicker('showWidget');\n\n\t\t\t\t\tif(target.value == '') {\n\t\t\t\t\t\t$('#' + target.id).timepicker('setTime', '12:00 AM');\n\t\t\t\t\t}\n\t\t\t\t});\n\n\t\t\t\t$('.timepicker-input').timepicker().on('changeTime.timepicker', (e) => {\n\t\t\t\t\tlet id = e.target.id;\n\n\t\t\t\t\tif(id == 'timepicker1') {\n\t\t\t\t\t\tthis.editScheduleForm.timeIn = e.time.value;\n\t\t\t\t\t} else if(id == 'timepicker2') {\n\t\t\t\t\t\tthis.editScheduleForm.timeOut = e.time.value;\n\t\t\t\t\t} else if(id == 'timepicker3') {\n\t\t\t\t\t\tthis.editScheduleForm.timeInFrom = e.time.value;\n\t\t\t\t\t} else if(id == 'timepicker4') {\n\t\t\t\t\t\tthis.editScheduleForm.timeInTo = e.time.value;\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t\t},\n\t\t\tgetScheduleTypes() {\n\t\t\t\taxiosAPIv1.get('/schedule-types')\n\t\t\t\t  \t.then((response) => {\n\t\t\t\t    \tthis.scheduleTypes = response.data;\n\t\t\t\t  \t});\n\t\t\t},\n\t\t\tresetEditScheduleForm() {\n\t\t\t\tthis.editScheduleForm = new Form({\n\t\t\t\t\tuserId: '',\n\t\t\t\t\tscheduleType: '',\n\t\t\t\t\ttimeIn: '',\n\t\t\t\t\ttimeOut: '',\n\t\t\t\t\ttimeInFrom: '',\n\t\t\t\t\ttimeInTo: ''\n\t\t\t\t}, { baseURL: axiosAPIv1.defaults.baseURL });\n\t\t\t},\n\t\t\tsetSelectedUser(user) {\n\t\t\t\tthis.editScheduleForm.userId = user.id;\n\n\t\t\t\tif(user.schedule) {\n\t\t\t\t\tthis.editScheduleForm.scheduleType = user.schedule.schedule_type_id;\n\t\t\t\t} else {\n\t\t\t\t\tthis.editScheduleForm.scheduleType = '';\n\t\t\t\t}\n\t\t\t\t\t\t\n\t\t\t\tif(user.schedule && user.schedule.time_in && user.schedule.time_out) {\n\t\t\t\t\tthis.editScheduleForm.timeIn = moment(user.schedule.time_in, 'HH:mm:ss').format(\"h:mm A\");\n\t\t\t\t\tthis.editScheduleForm.timeOut = moment(user.schedule.time_out, 'HH:mm:ss').format(\"h:mm A\")\n\t\t\t\t} else {\n\t\t\t\t\tthis.editScheduleForm.timeIn = '';\n\t\t\t\t\tthis.editScheduleForm.timeOut = '';\n\t\t\t\t}\n\n\t\t\t\tif(user.schedule && user.schedule.time_in_from && user.schedule.time_in_to) {\n\t\t\t\t\tthis.editScheduleForm.timeInFrom = moment(user.schedule.time_in_from, 'HH:mm:ss').format(\"h:mm A\");\n\t\t\t\t\tthis.editScheduleForm.timeInTo = moment(user.schedule.time_in_to, 'HH:mm:ss').format(\"h:mm A\");\n\t\t\t\t} else {\n\t\t\t\t\tthis.editScheduleForm.timeInFrom = '';\n\t\t\t\t\tthis.editScheduleForm.timeInTo = '';\n\t\t\t\t}\n\t\t\t},\n\t\t\teditSchedule() {\n\t\t\t\tthis.editScheduleForm.submit('patch', '/schedule/' + this.editScheduleForm.userId)\n\t\t\t        .then(response => {\n\t\t\t            if(response.success) {\n\t\t\t            \tthis.$parent.$parent.getUsers();\n\t\t\t                \n\t\t\t                this.resetEditScheduleForm();\n\n\t\t\t                $('#editSchedule').modal('hide');\n\n\t\t\t                toastr.success(response.message);\n\t\t\t            }\n\t\t\t        })\n\t\t\t        .catch(error => {\n\t\t\t        \tlet errors = JSON.parse(JSON.stringify(error));\n\n\t\t\t        \tfor(var key in errors) {\n\t\t\t        \t\tlet _error = errors[key].message;\n\n\t\t\t        \t\t_error.forEach(e => {\n\t\t\t        \t\t\ttoastr.error(e);\n\t\t\t        \t\t});\n\t\t\t        \t}\n\t\t\t        });\n\t\t\t}\n\t\t},\n\n\t\tcreated() {\n\t\t\tthis.getScheduleTypes();\n\t\t},\n\n\t\tmounted() {\n\t\t\tthis.initTimePicker();\n\t\t}\n\t}\n\n</script>\n\n<style scoped>\n\tform {\n\t\tmargin-bottom: 30px;\n\t}\n\t.m-r-10 {\n\t\tmargin-right: 10px;\n\t}\n</style>"]}]);

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-md' }
    }
});

/***/ }),

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(34)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(23),
  /* template */
  __webpack_require__(33),
  /* scopeId */
  "data-v-9f916844",
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/employee/accounts/components/edit-schedule.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] edit-schedule.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9f916844", Component.options)
  } else {
    hotAPI.reload("data-v-9f916844", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(7)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 33:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.editSchedule($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group",
    class: {
      'has-error': _vm.editScheduleForm.errors.has('schedule_type')
    }
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.scheduleType),
      expression: "editScheduleForm.scheduleType"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "schedule_type"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.editScheduleForm, "scheduleType", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.scheduleTypes), function(scheduleType) {
    return _c('option', {
      domProps: {
        "value": scheduleType.id
      }
    }, [_vm._v("\n\t\t    \t\t\t" + _vm._s(scheduleType.name) + "\n\t\t    \t\t")])
  }), 0), _vm._v(" "), (_vm.editScheduleForm.errors.has('schedule_type')) ? _c('span', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.editScheduleForm.errors.get('schedule_type'))
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.editScheduleForm.scheduleType == 1 || _vm.editScheduleForm.scheduleType == 2),
      expression: "editScheduleForm.scheduleType == 1 ||  editScheduleForm.scheduleType == 2"
    }],
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.editScheduleForm.scheduleType == 1),
      expression: "editScheduleForm.scheduleType == 1"
    }]
  }, [_c('div', {
    staticClass: "input-group"
  }, [_c('div', {
    staticClass: "bootstrap-timepicker timepicker"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.timeIn),
      expression: "editScheduleForm.timeIn"
    }],
    staticClass: "form-control input-small timepicker-input",
    attrs: {
      "id": "timepicker1",
      "type": "text",
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.editScheduleForm.timeIn)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editScheduleForm, "timeIn", $event.target.value)
      }
    }
  })]), _vm._v(" "), _c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("to")]), _vm._v(" "), _c('div', {
    staticClass: "bootstrap-timepicker timepicker"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.timeOut),
      expression: "editScheduleForm.timeOut"
    }],
    staticClass: "form-control input-small timepicker-input",
    attrs: {
      "id": "timepicker2",
      "type": "text",
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.editScheduleForm.timeOut)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editScheduleForm, "timeOut", $event.target.value)
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.editScheduleForm.scheduleType == 2),
      expression: "editScheduleForm.scheduleType == 2"
    }]
  }, [_c('div', {
    staticClass: "input-group"
  }, [_c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("Time In From")]), _vm._v(" "), _c('div', {
    staticClass: "bootstrap-timepicker timepicker"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.timeInFrom),
      expression: "editScheduleForm.timeInFrom"
    }],
    staticClass: "form-control input-small timepicker-input",
    attrs: {
      "id": "timepicker3",
      "type": "text",
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.editScheduleForm.timeInFrom)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editScheduleForm, "timeInFrom", $event.target.value)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "10px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "input-group"
  }, [_c('span', {
    staticClass: "input-group-addon"
  }, [_vm._v("Time In To")]), _vm._v(" "), _c('div', {
    staticClass: "bootstrap-timepicker timepicker"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.editScheduleForm.timeInTo),
      expression: "editScheduleForm.timeInTo"
    }],
    staticClass: "form-control input-small timepicker-input",
    attrs: {
      "id": "timepicker4",
      "type": "text",
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.editScheduleForm.timeInTo)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.editScheduleForm, "timeInTo", $event.target.value)
      }
    }
  })])])])])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Edit")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Schedule Type: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('label', {
    staticClass: "col-md-2 control-label"
  }, [_vm._v("\n\t\t        Schedule Details: "), _c('span', {
    staticClass: "text-danger"
  }, [_vm._v("*")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-9f916844", module.exports)
  }
}

/***/ }),

/***/ 34:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(26);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("88059542", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-9f916844\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./edit-schedule.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-9f916844\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./edit-schedule.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 360:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(267),
  /* template */
  __webpack_require__(390),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/employee/components/DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-40954648", Component.options)
  } else {
    hotAPI.reload("data-v-40954648", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 390:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-40954648", module.exports)
  }
}

/***/ }),

/***/ 460:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(170);


/***/ }),

/***/ 7:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9+L2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzP2RhMDQqKioqKioiLCJ3ZWJwYWNrOi8vL2VkaXQtc2NoZWR1bGUudnVlPzUzY2UiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvY29tcG9uZW50cy9lZGl0LXNjaGVkdWxlLnZ1ZT9mZGE1KiIsIndlYnBhY2s6Ly8vRGlhbG9nTW9kYWwudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2FjY291bnRzL2NvbXBvbmVudHMvZWRpdC1zY2hlZHVsZS52dWU/YWEzMiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qcz82YjJiKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2FjY291bnRzL2NvbXBvbmVudHMvZWRpdC1zY2hlZHVsZS52dWU/ZTljZSoiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvY29tcG9uZW50cy9lZGl0LXNjaGVkdWxlLnZ1ZT8xZjc3KiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT83NjY3Iiwid2VicGFjazovLy8uL34vdnVlLXN0eWxlLWxvYWRlci9saWIvbGlzdFRvU3R5bGVzLmpzP2U2YWMqKioqKioiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsIlVzZXJBcHAiLCJlbCIsImRhdGEiLCJ1c2VycyIsInNlbHVzZXIiLCJzZWxlY3RlZFVzZXIiLCJtZXRob2RzIiwic2hvd1JlbW92ZURpYWxvZyIsInVzZXIiLCIkIiwibW9kYWwiLCJnZXRVc2VycyIsImF4aW9zQVBJdjEiLCJnZXQiLCJheGlvcyIsImFsbCIsInRoZW4iLCJzcHJlYWQiLCJjYXRjaCIsIm5vb3AiLCJkZWxldGVVc2VyIiwiZGVsZXRlIiwiaWQiLCJ0b2FzdHIiLCJzdWNjZXNzIiwic2VsZiIsInJlc3BvbnNlIiwiZWRpdFNjaGVkdWxlIiwiJHJlZnMiLCJlZGl0c2NoZWR1bGUiLCJzZXRTZWxlY3RlZFVzZXIiLCJjcmVhdGVkIiwidXBkYXRlZCIsIkRhdGFUYWJsZSIsInBhZ2VMZW5ndGgiLCJyZXNwb25zaXZlIiwiZG9tIiwiY29tcG9uZW50cyIsImRvY3VtZW50IiwicmVhZHkiLCJrZXlwcmVzcyIsImV2ZW50IiwiaW5wdXRWYWx1ZSIsImNoYXJDb2RlIiwicHJldmVudERlZmF1bHQiLCJlIiwid2hpY2giXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQUEsSUFBSUMsU0FBSixDQUFjLGNBQWQsRUFBK0JDLG1CQUFPQSxDQUFDLEdBQVIsQ0FBL0I7O0FBRUEsSUFBSUMsVUFBVSxJQUFJSCxHQUFKLENBQVE7QUFDckJJLEtBQUcsV0FEa0I7QUFFckJDLE9BQUs7QUFDSkMsU0FBTSxFQURGO0FBRUpDLFdBQVUsRUFGTjs7QUFJSkMsZ0JBQWM7QUFKVixFQUZnQjtBQVFyQkMsVUFBUztBQUNSQyxrQkFEUSw0QkFDU0MsSUFEVCxFQUNlO0FBQ3RCLFFBQUtKLE9BQUwsR0FBZUksSUFBZjs7QUFFQUMsS0FBRSxlQUFGLEVBQW1CQyxLQUFuQixDQUF5QixRQUF6QjtBQUNBLEdBTE87QUFNUkMsVUFOUSxzQkFNRztBQUFBOztBQUNWLFlBQVNBLFFBQVQsR0FBb0I7QUFDbkIsV0FBT0MsV0FBV0MsR0FBWCxDQUFlLFlBQWYsQ0FBUDtBQUNBO0FBQ0VDLFNBQU1DLEdBQU4sQ0FBVSxDQUNaSixVQURZLENBQVYsRUFFQUssSUFGQSxDQUVLRixNQUFNRyxNQUFOLENBQ1AsVUFDQ2QsS0FERCxFQUVLO0FBQ0wsVUFBS0EsS0FBTCxHQUFhQSxNQUFNRCxJQUFuQjtBQUNBLElBTE8sQ0FGTCxFQVFGZ0IsS0FSRSxDQVFJVCxFQUFFVSxJQVJOO0FBU0gsR0FuQk87QUFvQlJDLFlBcEJRLHdCQW9CSztBQUFBOztBQUNaUixjQUFXUyxNQUFYLENBQWtCLFlBQVUsS0FBS2pCLE9BQUwsQ0FBYWtCLEVBQXpDLEVBQ0VOLElBREYsQ0FDTyxvQkFBWTtBQUNqQk8sV0FBT0MsT0FBUCxDQUFlLGVBQWY7O0FBRUEsUUFBSUMsT0FBTyxNQUFYO0FBQ0FiLGVBQVdDLEdBQVgsQ0FBZSxRQUFmLEVBQ0VHLElBREYsQ0FDTyxvQkFBWTtBQUNqQlMsVUFBS3RCLEtBQUwsR0FBYXVCLFNBQVN4QixJQUF0QjtBQUNBLEtBSEY7QUFJQSxJQVRGO0FBVUEsR0EvQk87QUFnQ1J5QixjQWhDUSx3QkFnQ0tuQixJQWhDTCxFQWdDVztBQUNsQixRQUFLb0IsS0FBTCxDQUFXQyxZQUFYLENBQXdCQyxlQUF4QixDQUF3Q3RCLElBQXhDOztBQUVBQyxLQUFFLGVBQUYsRUFBbUJDLEtBQW5CLENBQXlCLE1BQXpCO0FBQ0E7QUFwQ08sRUFSWTtBQThDckJxQixRQTlDcUIscUJBOENaO0FBQ1IsT0FBS3BCLFFBQUw7QUFDQSxFQWhEb0I7QUFpRHJCcUIsUUFqRHFCLHFCQWlEWDtBQUNUdkIsSUFBRSxRQUFGLEVBQVl3QixTQUFaLENBQXNCO0FBQ1pDLGVBQVksRUFEQTtBQUVaQyxlQUFZLElBRkE7QUFHWkMsUUFBSzs7QUFITyxHQUF0QjtBQU1BLEVBeERvQjs7QUF5RHJCQyxhQUFZO0FBQ1gsbUJBQWlCdEMsbUJBQU9BLENBQUMsRUFBUjtBQUROO0FBekRTLENBQVIsQ0FBZDs7QUErREFVLEVBQUU2QixRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVzs7QUFFNUI5QixHQUFFLHVDQUFGLEVBQTJDK0IsUUFBM0MsQ0FBb0QsVUFBU0MsS0FBVCxFQUFlO0FBQ2xFLE1BQUlDLGFBQWFELE1BQU1FLFFBQXZCO0FBQ0E7QUFDQSxNQUFHLEVBQUdELGFBQWEsRUFBYixJQUFtQkEsYUFBYSxFQUFqQyxJQUF5Q0EsYUFBYSxFQUFiLElBQW1CQSxhQUFhLEdBQXpFLElBQWdGQSxjQUFZLEVBQTVGLElBQW9HQSxjQUFZLENBQWxILENBQUgsRUFBeUg7QUFDekhELFNBQU1HLGNBQU47QUFDQztBQUNELEVBTkQ7O0FBUUFuQyxHQUFFLHVEQUFGLEVBQTJEK0IsUUFBM0QsQ0FBb0UsVUFBVUssQ0FBVixFQUFhO0FBQ2hGO0FBQ0csTUFBSUEsRUFBRUMsS0FBRixJQUFXLENBQVgsSUFBZ0JELEVBQUVDLEtBQUYsSUFBVyxDQUEzQixLQUFpQ0QsRUFBRUMsS0FBRixHQUFVLEVBQVYsSUFBZ0JELEVBQUVDLEtBQUYsR0FBVSxFQUEzRCxDQUFKLEVBQW9FO0FBQ2hFLFVBQU8sS0FBUDtBQUNIO0FBQ0osRUFMRDtBQU9BLENBakJELEU7Ozs7Ozs7QUNqRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0Esd0NBQXdDLGdCQUFnQjtBQUN4RCxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksb0JBQW9CO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbUJBO0FBQ0Esd0JBREE7O0FBR0EsS0FIQSxrQkFHQTtBQUNBO0FBQ0Esb0JBREE7O0FBR0E7QUFDQSxjQURBO0FBRUEsb0JBRkE7QUFHQSxjQUhBO0FBSUEsZUFKQTtBQUtBLGtCQUxBO0FBTUE7QUFOQSxNQU9BLHdDQVBBO0FBSEE7QUFZQSxFQWhCQTs7O0FBa0JBO0FBQ0EsZ0JBREEsNEJBQ0E7QUFBQTs7QUFDQTtBQUNBO0FBQ0EsSUFGQSxFQUVBLEdBRkE7O0FBSUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQVJBOztBQVVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBRkEsTUFFQTtBQUNBO0FBQ0EsS0FGQSxNQUVBO0FBQ0E7QUFDQSxLQUZBLE1BRUE7QUFDQTtBQUNBO0FBQ0EsSUFaQTtBQWFBLEdBN0JBO0FBOEJBLGtCQTlCQSw4QkE4QkE7QUFBQTs7QUFDQSxxQ0FDQSxJQURBLENBQ0E7QUFDQTtBQUNBLElBSEE7QUFJQSxHQW5DQTtBQW9DQSx1QkFwQ0EsbUNBb0NBO0FBQ0E7QUFDQSxjQURBO0FBRUEsb0JBRkE7QUFHQSxjQUhBO0FBSUEsZUFKQTtBQUtBLGtCQUxBO0FBTUE7QUFOQSxNQU9BLHdDQVBBO0FBUUEsR0E3Q0E7QUE4Q0EsaUJBOUNBLDJCQThDQSxJQTlDQSxFQThDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQUZBLE1BRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBSEEsTUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQUhBLE1BR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQXRFQTtBQXVFQSxjQXZFQSwwQkF1RUE7QUFBQTs7QUFDQSxzRkFDQSxJQURBLENBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxJQVhBLEVBWUEsS0FaQSxDQVlBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsTUFGQTtBQUdBO0FBQ0EsSUF0QkE7QUF1QkE7QUEvRkEsRUFsQkE7O0FBb0hBLFFBcEhBLHFCQW9IQTtBQUNBO0FBQ0EsRUF0SEE7QUF3SEEsUUF4SEEscUJBd0hBO0FBQ0E7QUFDQTtBQTFIQSxHOzs7Ozs7O0FDcEVBLDJCQUEyQixtQkFBTyxDQUFDLENBQThEO0FBQ2pHLGNBQWMsUUFBUyw0QkFBNEIsd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixHQUFHLFVBQVUsNkVBQTZFLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyxzTUFBc00sNERBQTRELGtaQUFrWixxQkFBcUIsc3dEQUFzd0QsWUFBWSxvdUJBQW91Qiw4Q0FBOEMsZ0JBQWdCLG9FQUFvRSxpS0FBaUssR0FBRyx1Q0FBdUMsVUFBVSxPQUFPLG1CQUFtQiwwQkFBMEIsNEJBQTRCLGdEQUFnRCxXQUFXLE9BQU8sNERBQTRELGtDQUFrQywwREFBMEQsc0NBQXNDLG1FQUFtRSxhQUFhLFdBQVcsRUFBRSxvRkFBb0YsaUNBQWlDLHVDQUF1QywwREFBMEQsYUFBYSwrQkFBK0IsMkRBQTJELGFBQWEsK0JBQStCLDhEQUE4RCxhQUFhLCtCQUErQiw0REFBNEQsYUFBYSxXQUFXLEVBQUUsU0FBUyw2QkFBNkIsOEVBQThFLG1EQUFtRCxlQUFlLEVBQUUsU0FBUyxrQ0FBa0MsNENBQTRDLGlLQUFpSyxHQUFHLHVDQUF1QyxFQUFFLFNBQVMsZ0NBQWdDLGlEQUFpRCwrQkFBK0IsZ0ZBQWdGLFdBQVcsT0FBTyxvREFBb0QsV0FBVyw4RkFBOEYsd0dBQXdHLG9IQUFvSCxPQUFPLDhDQUE4QywrQ0FBK0MsV0FBVyx5RkFBeUYsaUhBQWlILDZHQUE2RyxXQUFXLE9BQU8sa0RBQWtELGdEQUFnRCxXQUFXLFNBQVMseUJBQXlCLCtIQUErSCwwQ0FBMEMsc0RBQXNELDZFQUE2RSwyREFBMkQsMkRBQTJELHFCQUFxQixpQkFBaUIsa0NBQWtDLGlFQUFpRSw0Q0FBNEMscURBQXFELDJDQUEyQyxzQ0FBc0MscUJBQXFCLEVBQUUsbUJBQW1CLGlCQUFpQixFQUFFLFNBQVMsT0FBTyxvQkFBb0IsZ0NBQWdDLE9BQU8sb0JBQW9CLDhCQUE4QixPQUFPLEtBQUsseUNBQXlDLDBCQUEwQixLQUFLLGFBQWEseUJBQXlCLEtBQUssYUFBYSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3NCdjJPO0FBQ0E7QUFDQSxnQ0FEQTtBQUVBO0FBRkE7QUFEQSxHOzs7Ozs7OztBQ3RCQTtBQUNBLG1CQUFPLENBQUMsRUFBdVI7O0FBRS9SLGdCQUFnQixtQkFBTyxDQUFDLENBQXdFO0FBQ2hHO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEVBQThPO0FBQ3hQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEVBQTZNO0FBQ3ZOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQy9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSxpQkFBaUI7QUFDM0I7QUFDQTs7QUFFQSxtQkFBbUIsbUJBQU8sQ0FBQyxDQUFnQjs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsbUJBQW1CLHNCQUFzQjtBQUN6QztBQUNBO0FBQ0EsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsMkJBQTJCO0FBQ2hEO0FBQ0E7QUFDQSxZQUFZLHVCQUF1QjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EscUJBQXFCLHVCQUF1QjtBQUM1QztBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUN0TkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUMsYUFBYSxhQUFhLDBCQUEwQjtBQUNyRDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pOQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxFQUF1VDtBQUM3VSw0Q0FBNEMsUUFBUztBQUNyRDtBQUNBO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLENBQTRFO0FBQ2pHO0FBQ0EsR0FBRyxLQUFVO0FBQ2I7QUFDQTtBQUNBLGtLQUFrSyxvRUFBb0U7QUFDdE8sMktBQTJLLG9FQUFvRTtBQUMvTztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7Ozs7QUNwQkEsZ0JBQWdCLG1CQUFPLENBQUMsQ0FBcUU7QUFDN0Y7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBeU87QUFDblA7QUFDQSxFQUFFLG1CQUFPLENBQUMsR0FBcU07QUFDL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxJQUFJLEtBQVUsR0FBRztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLGlCQUFpQjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsd0JBQXdCO0FBQzNELEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImpzL2VtcGxveWVlL2FjY291bnRzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0NjApO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGYxMTNlZDM2YTVhNTZiN2RjZjYzIiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUiLCJWdWUuY29tcG9uZW50KCdkaWFsb2ctbW9kYWwnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWUnKSk7XG5cbnZhciBVc2VyQXBwID0gbmV3IFZ1ZSh7XG5cdGVsOicjdXNlcmxpc3QnLFxuXHRkYXRhOntcblx0XHR1c2VyczpbXSxcblx0XHRzZWx1c2VyIDogJycsXG5cblx0XHRzZWxlY3RlZFVzZXI6IG51bGxcblx0fSxcblx0bWV0aG9kczoge1xuXHRcdHNob3dSZW1vdmVEaWFsb2codXNlcikge1xuXHRcdFx0dGhpcy5zZWx1c2VyID0gdXNlcjtcblxuXHRcdFx0JCgnI2RpYWxvZ1JlbW92ZScpLm1vZGFsKCd0b2dnbGUnKTtcblx0XHR9LFxuXHRcdGdldFVzZXJzKCkge1xuXHRcdFx0ZnVuY3Rpb24gZ2V0VXNlcnMoKSB7XG5cdFx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2VtcGxveWVlcycpO1xuXHRcdFx0fVxuXHQgICAgXHRheGlvcy5hbGwoW1xuXHRcdFx0XHRnZXRVc2VycygpXG5cdFx0XHRdKS50aGVuKGF4aW9zLnNwcmVhZChcblx0XHRcdFx0KFxuXHRcdFx0XHRcdHVzZXJzLFxuXHRcdFx0XHQpID0+IHtcblx0XHRcdFx0dGhpcy51c2VycyA9IHVzZXJzLmRhdGE7XG5cdFx0XHR9KSlcblx0XHRcdC5jYXRjaCgkLm5vb3ApO1xuXHRcdH0sXG5cdFx0ZGVsZXRlVXNlcigpIHtcblx0XHRcdGF4aW9zQVBJdjEuZGVsZXRlKCcvdXNlcnMvJyt0aGlzLnNlbHVzZXIuaWQpXG5cdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcblx0XHRcdFx0XHR0b2FzdHIuc3VjY2VzcygnVXNlciBEZWxldGVkIScpO1xuXG5cdFx0XHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvdXNlcnMnKVxuXHRcdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0XHRcdFx0XHRzZWxmLnVzZXJzID0gcmVzcG9uc2UuZGF0YTtcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9KTtcblx0XHR9LFxuXHRcdGVkaXRTY2hlZHVsZSh1c2VyKSB7XG5cdFx0XHR0aGlzLiRyZWZzLmVkaXRzY2hlZHVsZS5zZXRTZWxlY3RlZFVzZXIodXNlcik7XG5cblx0XHRcdCQoJyNlZGl0U2NoZWR1bGUnKS5tb2RhbCgnc2hvdycpO1xuXHRcdH1cblx0fSxcblx0Y3JlYXRlZCgpe1xuXHRcdHRoaXMuZ2V0VXNlcnMoKTtcblx0fSxcblx0dXBkYXRlZCgpIHtcblx0XHQkKCcjbGlzdHMnKS5EYXRhVGFibGUoe1xuICAgICAgICAgICAgcGFnZUxlbmd0aDogMTAsXG4gICAgICAgICAgICByZXNwb25zaXZlOiB0cnVlLFxuICAgICAgICAgICAgZG9tOiAnPFwidG9wXCJsZj5ydDxcImJvdHRvbVwiaXA+PFwiY2xlYXJcIj4nXG5cbiAgICAgICAgfSk7XG5cdH0sXG5cdGNvbXBvbmVudHM6IHtcblx0XHQnZWRpdC1zY2hlZHVsZSc6IHJlcXVpcmUoJy4vY29tcG9uZW50cy9lZGl0LXNjaGVkdWxlLnZ1ZScpXG5cdH1cbn0pO1xuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuXG5cdCQoJyNmaXJzdF9uYW1lLCAjbWlkZGxlX25hbWUsICNsYXN0X25hbWUnKS5rZXlwcmVzcyhmdW5jdGlvbihldmVudCl7XG5cdFx0dmFyIGlucHV0VmFsdWUgPSBldmVudC5jaGFyQ29kZTtcblx0XHQvL2FsZXJ0KGlucHV0VmFsdWUpO1xuXHRcdGlmKCEoKGlucHV0VmFsdWUgPiA2NCAmJiBpbnB1dFZhbHVlIDwgOTEpIHx8IChpbnB1dFZhbHVlID4gOTYgJiYgaW5wdXRWYWx1ZSA8IDEyMyl8fChpbnB1dFZhbHVlPT0zMikgfHwgKGlucHV0VmFsdWU9PTApKSl7XG5cdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHR9XG5cdH0pO1xuXG5cdCQoXCIjY29udGFjdF9udW1iZXIsICNhbHRlcm5hdGVfY29udGFjdF9udW1iZXIsICN6aXBfY29kZVwiKS5rZXlwcmVzcyhmdW5jdGlvbiAoZSkge1xuXHQgLy9pZiB0aGUgbGV0dGVyIGlzIG5vdCBkaWdpdCB0aGVuIGRpc3BsYXkgZXJyb3IgYW5kIGRvbid0IHR5cGUgYW55dGhpbmdcblx0ICAgIGlmIChlLndoaWNoICE9IDggJiYgZS53aGljaCAhPSAwICYmIChlLndoaWNoIDwgNDggfHwgZS53aGljaCA+IDU3KSkge1xuXHQgICAgICAgIHJldHVybiBmYWxzZTtcblx0ICAgIH1cblx0fSk7XG5cbn0gKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2FjY291bnRzL2luZGV4LmpzIiwiLypcclxuXHRNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxyXG5cdEF1dGhvciBUb2JpYXMgS29wcGVycyBAc29rcmFcclxuKi9cclxuLy8gY3NzIGJhc2UgY29kZSwgaW5qZWN0ZWQgYnkgdGhlIGNzcy1sb2FkZXJcclxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbigpIHtcclxuXHR2YXIgbGlzdCA9IFtdO1xyXG5cclxuXHQvLyByZXR1cm4gdGhlIGxpc3Qgb2YgbW9kdWxlcyBhcyBjc3Mgc3RyaW5nXHJcblx0bGlzdC50b1N0cmluZyA9IGZ1bmN0aW9uIHRvU3RyaW5nKCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IFtdO1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0dmFyIGl0ZW0gPSB0aGlzW2ldO1xyXG5cdFx0XHRpZihpdGVtWzJdKSB7XHJcblx0XHRcdFx0cmVzdWx0LnB1c2goXCJAbWVkaWEgXCIgKyBpdGVtWzJdICsgXCJ7XCIgKyBpdGVtWzFdICsgXCJ9XCIpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHJlc3VsdC5wdXNoKGl0ZW1bMV0pO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gcmVzdWx0LmpvaW4oXCJcIik7XHJcblx0fTtcclxuXHJcblx0Ly8gaW1wb3J0IGEgbGlzdCBvZiBtb2R1bGVzIGludG8gdGhlIGxpc3RcclxuXHRsaXN0LmkgPSBmdW5jdGlvbihtb2R1bGVzLCBtZWRpYVF1ZXJ5KSB7XHJcblx0XHRpZih0eXBlb2YgbW9kdWxlcyA9PT0gXCJzdHJpbmdcIilcclxuXHRcdFx0bW9kdWxlcyA9IFtbbnVsbCwgbW9kdWxlcywgXCJcIl1dO1xyXG5cdFx0dmFyIGFscmVhZHlJbXBvcnRlZE1vZHVsZXMgPSB7fTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHZhciBpZCA9IHRoaXNbaV1bMF07XHJcblx0XHRcdGlmKHR5cGVvZiBpZCA9PT0gXCJudW1iZXJcIilcclxuXHRcdFx0XHRhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzW2lkXSA9IHRydWU7XHJcblx0XHR9XHJcblx0XHRmb3IoaSA9IDA7IGkgPCBtb2R1bGVzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdHZhciBpdGVtID0gbW9kdWxlc1tpXTtcclxuXHRcdFx0Ly8gc2tpcCBhbHJlYWR5IGltcG9ydGVkIG1vZHVsZVxyXG5cdFx0XHQvLyB0aGlzIGltcGxlbWVudGF0aW9uIGlzIG5vdCAxMDAlIHBlcmZlY3QgZm9yIHdlaXJkIG1lZGlhIHF1ZXJ5IGNvbWJpbmF0aW9uc1xyXG5cdFx0XHQvLyAgd2hlbiBhIG1vZHVsZSBpcyBpbXBvcnRlZCBtdWx0aXBsZSB0aW1lcyB3aXRoIGRpZmZlcmVudCBtZWRpYSBxdWVyaWVzLlxyXG5cdFx0XHQvLyAgSSBob3BlIHRoaXMgd2lsbCBuZXZlciBvY2N1ciAoSGV5IHRoaXMgd2F5IHdlIGhhdmUgc21hbGxlciBidW5kbGVzKVxyXG5cdFx0XHRpZih0eXBlb2YgaXRlbVswXSAhPT0gXCJudW1iZXJcIiB8fCAhYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpdGVtWzBdXSkge1xyXG5cdFx0XHRcdGlmKG1lZGlhUXVlcnkgJiYgIWl0ZW1bMl0pIHtcclxuXHRcdFx0XHRcdGl0ZW1bMl0gPSBtZWRpYVF1ZXJ5O1xyXG5cdFx0XHRcdH0gZWxzZSBpZihtZWRpYVF1ZXJ5KSB7XHJcblx0XHRcdFx0XHRpdGVtWzJdID0gXCIoXCIgKyBpdGVtWzJdICsgXCIpIGFuZCAoXCIgKyBtZWRpYVF1ZXJ5ICsgXCIpXCI7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGxpc3QucHVzaChpdGVtKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH07XHJcblx0cmV0dXJuIGxpc3Q7XHJcbn07XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1xuLy8gbW9kdWxlIGlkID0gMlxuLy8gbW9kdWxlIGNodW5rcyA9IDEgMiAzIDQgNSA5IDExIDEyIDEzIiwiPHRlbXBsYXRlPlxuXHRcblx0PGRpdj5cblx0XHRcblx0XHQ8Zm9ybSBjbGFzcz1cImZvcm0taG9yaXpvbnRhbFwiIEBzdWJtaXQucHJldmVudD1cImVkaXRTY2hlZHVsZVwiPlxuXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGVkaXRTY2hlZHVsZUZvcm0uZXJyb3JzLmhhcygnc2NoZWR1bGVfdHlwZScpIH1cIj5cblx0XHRcdCAgICA8bGFiZWwgY2xhc3M9XCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXCI+XG5cdFx0XHQgICAgICAgIFNjaGVkdWxlIFR5cGU6IDxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4qPC9zcGFuPlxuXHRcdCAgICAgICAgPC9sYWJlbD5cblx0XHRcdCAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEwXCI+XG5cdFx0XHQgICAgXHQ8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cInNjaGVkdWxlX3R5cGVcIiB2LW1vZGVsPVwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGVcIj5cblx0XHRcdCAgICBcdFx0PG9wdGlvbiB2LWZvcj1cInNjaGVkdWxlVHlwZSBpbiBzY2hlZHVsZVR5cGVzXCIgOnZhbHVlPVwic2NoZWR1bGVUeXBlLmlkXCI+XG5cdFx0XHQgICAgXHRcdFx0e3sgc2NoZWR1bGVUeXBlLm5hbWUgfX1cblx0XHRcdCAgICBcdFx0PC9vcHRpb24+XG5cdFx0XHQgICAgXHQ8L3NlbGVjdD5cblx0XHRcdCAgICBcdDxzcGFuIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJlZGl0U2NoZWR1bGVGb3JtLmVycm9ycy5oYXMoJ3NjaGVkdWxlX3R5cGUnKVwiIHYtdGV4dD1cImVkaXRTY2hlZHVsZUZvcm0uZXJyb3JzLmdldCgnc2NoZWR1bGVfdHlwZScpXCI+PC9zcGFuPlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cblx0XHRcdDxkaXYgdi1zaG93PVwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMSB8fCAgZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMlwiIGNsYXNzPVwiZm9ybS1ncm91cFwiPlxuXHRcdFx0ICAgIDxsYWJlbCBjbGFzcz1cImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcIj5cblx0XHRcdCAgICAgICAgU2NoZWR1bGUgRGV0YWlsczogPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPio8L3NwYW4+XG5cdFx0ICAgICAgICA8L2xhYmVsPlxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTBcIj5cblxuXHRcdFx0ICAgIFx0PGRpdiB2LXNob3c9XCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAxXCI+XG5cdFx0XHQgICAgXHRcdDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxuXHRcdFx0ICAgIFx0XHRcdDxkaXYgY2xhc3M9XCJib290c3RyYXAtdGltZXBpY2tlciB0aW1lcGlja2VyXCI+XG5cdFx0XHRcdFx0ICAgICAgICAgICAgPGlucHV0IGlkPVwidGltZXBpY2tlcjFcIiB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGlucHV0LXNtYWxsIHRpbWVwaWNrZXItaW5wdXRcIiB2LW1vZGVsPVwiZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5cIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cblx0XHRcdFx0XHQgICAgICAgIDwvZGl2PlxuXHRcdFx0XHRcdCAgICAgICAgPHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPnRvPC9zcGFuPlxuXHRcdFx0XHRcdCAgICAgICAgPGRpdiBjbGFzcz1cImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcIj5cblx0XHRcdFx0XHQgICAgICAgICAgICA8aW5wdXQgaWQ9XCJ0aW1lcGlja2VyMlwiIHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiIHYtbW9kZWw9XCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVPdXRcIiBhdXRvY29tcGxldGU9XCJvZmZcIj5cblx0XHRcdFx0XHQgICAgICAgIDwvZGl2PlxuXHRcdFx0ICAgIFx0XHQ8L2Rpdj5cblx0XHRcdCAgICBcdDwvZGl2PlxuXG5cdFx0XHQgICAgXHQ8ZGl2IHYtc2hvdz1cImVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDJcIj5cblx0XHRcdCAgICBcdFx0PGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XG5cdFx0XHQgICAgXHRcdFx0PHNwYW4gY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiPlRpbWUgSW4gRnJvbTwvc3Bhbj5cblx0XHRcdCAgICBcdFx0XHQ8ZGl2IGNsYXNzPVwiYm9vdHN0cmFwLXRpbWVwaWNrZXIgdGltZXBpY2tlclwiPlxuXHRcdFx0XHRcdCAgICAgICAgICAgIDxpbnB1dCBpZD1cInRpbWVwaWNrZXIzXCIgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbCBpbnB1dC1zbWFsbCB0aW1lcGlja2VyLWlucHV0XCIgdi1tb2RlbD1cImVkaXRTY2hlZHVsZUZvcm0udGltZUluRnJvbVwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuXHRcdFx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgXHRcdDwvZGl2PlxuXHRcdFx0ICAgIFx0XHQ8ZGl2IHN0eWxlPVwiaGVpZ2h0OjEwcHg7IGNsZWFyOmJvdGg7XCI+PC9kaXY+XG5cdFx0XHQgICAgXHRcdDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxuXHRcdFx0ICAgIFx0XHRcdDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj5UaW1lIEluIFRvPC9zcGFuPlxuXHRcdFx0ICAgIFx0XHRcdDxkaXYgY2xhc3M9XCJib290c3RyYXAtdGltZXBpY2tlciB0aW1lcGlja2VyXCI+XG5cdFx0XHRcdFx0ICAgICAgICAgICAgPGlucHV0IGlkPVwidGltZXBpY2tlcjRcIiB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sIGlucHV0LXNtYWxsIHRpbWVwaWNrZXItaW5wdXRcIiB2LW1vZGVsPVwiZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5Ub1wiIGF1dG9jb21wbGV0ZT1cIm9mZlwiPlxuXHRcdFx0XHRcdCAgICAgICAgPC9kaXY+XG5cdFx0XHQgICAgXHRcdDwvZGl2PlxuXHRcdFx0ICAgIFx0PC9kaXY+XG5cblx0XHRcdCAgICA8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCI+RWRpdDwvYnV0dG9uPlxuXHQgICAgXHQ8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBwdWxsLXJpZ2h0IG0tci0xMFwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cblxuXHRcdDwvZm9ybT5cblxuXHQ8L2Rpdj5cblxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblx0XG5cdGV4cG9ydCBkZWZhdWx0IHtcblx0XHRwcm9wczogWydzZWxlY3RlZHVzZXInXSxcblxuXHRcdGRhdGEoKSB7XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRzY2hlZHVsZVR5cGVzOiBbXSxcblxuXHRcdFx0XHRlZGl0U2NoZWR1bGVGb3JtOiBuZXcgRm9ybSh7XG5cdFx0XHRcdFx0dXNlcklkOiAnJyxcblx0XHRcdFx0XHRzY2hlZHVsZVR5cGU6ICcnLFxuXHRcdFx0XHRcdHRpbWVJbjogJycsXG5cdFx0XHRcdFx0dGltZU91dDogJycsXG5cdFx0XHRcdFx0dGltZUluRnJvbTogJycsXG5cdFx0XHRcdFx0dGltZUluVG86ICcnXG5cdFx0XHRcdH0sIHsgYmFzZVVSTDogYXhpb3NBUEl2MS5kZWZhdWx0cy5iYXNlVVJMIH0pXG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdG1ldGhvZHM6IHtcblx0XHRcdGluaXRUaW1lUGlja2VyKCkge1xuXHRcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0XHQkKCcudGltZXBpY2tlci1pbnB1dCcpLnRpbWVwaWNrZXIoKTtcblx0XHRcdFx0fSwgNTAwKTtcblxuXHRcdFx0XHQkKCcudGltZXBpY2tlci1pbnB1dCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0XHRsZXQgdGFyZ2V0ID0gZS50YXJnZXQ7XG5cblx0XHRcdFx0XHQkKCcjJyArIHRhcmdldC5pZCkudGltZXBpY2tlcignc2hvd1dpZGdldCcpO1xuXG5cdFx0XHRcdFx0aWYodGFyZ2V0LnZhbHVlID09ICcnKSB7XG5cdFx0XHRcdFx0XHQkKCcjJyArIHRhcmdldC5pZCkudGltZXBpY2tlcignc2V0VGltZScsICcxMjowMCBBTScpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0JCgnLnRpbWVwaWNrZXItaW5wdXQnKS50aW1lcGlja2VyKCkub24oJ2NoYW5nZVRpbWUudGltZXBpY2tlcicsIChlKSA9PiB7XG5cdFx0XHRcdFx0bGV0IGlkID0gZS50YXJnZXQuaWQ7XG5cblx0XHRcdFx0XHRpZihpZCA9PSAndGltZXBpY2tlcjEnKSB7XG5cdFx0XHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluID0gZS50aW1lLnZhbHVlO1xuXHRcdFx0XHRcdH0gZWxzZSBpZihpZCA9PSAndGltZXBpY2tlcjInKSB7XG5cdFx0XHRcdFx0XHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZU91dCA9IGUudGltZS52YWx1ZTtcblx0XHRcdFx0XHR9IGVsc2UgaWYoaWQgPT0gJ3RpbWVwaWNrZXIzJykge1xuXHRcdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20gPSBlLnRpbWUudmFsdWU7XG5cdFx0XHRcdFx0fSBlbHNlIGlmKGlkID09ICd0aW1lcGlja2VyNCcpIHtcblx0XHRcdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5UbyA9IGUudGltZS52YWx1ZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSxcblx0XHRcdGdldFNjaGVkdWxlVHlwZXMoKSB7XG5cdFx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvc2NoZWR1bGUtdHlwZXMnKVxuXHRcdFx0XHQgIFx0LnRoZW4oKHJlc3BvbnNlKSA9PiB7XG5cdFx0XHRcdCAgICBcdHRoaXMuc2NoZWR1bGVUeXBlcyA9IHJlc3BvbnNlLmRhdGE7XG5cdFx0XHRcdCAgXHR9KTtcblx0XHRcdH0sXG5cdFx0XHRyZXNldEVkaXRTY2hlZHVsZUZvcm0oKSB7XG5cdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybSA9IG5ldyBGb3JtKHtcblx0XHRcdFx0XHR1c2VySWQ6ICcnLFxuXHRcdFx0XHRcdHNjaGVkdWxlVHlwZTogJycsXG5cdFx0XHRcdFx0dGltZUluOiAnJyxcblx0XHRcdFx0XHR0aW1lT3V0OiAnJyxcblx0XHRcdFx0XHR0aW1lSW5Gcm9tOiAnJyxcblx0XHRcdFx0XHR0aW1lSW5UbzogJydcblx0XHRcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSk7XG5cdFx0XHR9LFxuXHRcdFx0c2V0U2VsZWN0ZWRVc2VyKHVzZXIpIHtcblx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnVzZXJJZCA9IHVzZXIuaWQ7XG5cblx0XHRcdFx0aWYodXNlci5zY2hlZHVsZSkge1xuXHRcdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPSB1c2VyLnNjaGVkdWxlLnNjaGVkdWxlX3R5cGVfaWQ7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9ICcnO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0aWYodXNlci5zY2hlZHVsZSAmJiB1c2VyLnNjaGVkdWxlLnRpbWVfaW4gJiYgdXNlci5zY2hlZHVsZS50aW1lX291dCkge1xuXHRcdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW4gPSBtb21lbnQodXNlci5zY2hlZHVsZS50aW1lX2luLCAnSEg6bW06c3MnKS5mb3JtYXQoXCJoOm1tIEFcIik7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVPdXQgPSBtb21lbnQodXNlci5zY2hlZHVsZS50aW1lX291dCwgJ0hIOm1tOnNzJykuZm9ybWF0KFwiaDptbSBBXCIpXG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbiA9ICcnO1xuXHRcdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0ID0gJyc7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZih1c2VyLnNjaGVkdWxlICYmIHVzZXIuc2NoZWR1bGUudGltZV9pbl9mcm9tICYmIHVzZXIuc2NoZWR1bGUudGltZV9pbl90bykge1xuXHRcdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5Gcm9tID0gbW9tZW50KHVzZXIuc2NoZWR1bGUudGltZV9pbl9mcm9tLCAnSEg6bW06c3MnKS5mb3JtYXQoXCJoOm1tIEFcIik7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvID0gbW9tZW50KHVzZXIuc2NoZWR1bGUudGltZV9pbl90bywgJ0hIOm1tOnNzJykuZm9ybWF0KFwiaDptbSBBXCIpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5Gcm9tID0gJyc7XG5cdFx0XHRcdFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvID0gJyc7XG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRlZGl0U2NoZWR1bGUoKSB7XG5cdFx0XHRcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS5zdWJtaXQoJ3BhdGNoJywgJy9zY2hlZHVsZS8nICsgdGhpcy5lZGl0U2NoZWR1bGVGb3JtLnVzZXJJZClcblx0XHRcdCAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuXHRcdFx0ICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2Vzcykge1xuXHRcdFx0ICAgICAgICAgICAgXHR0aGlzLiRwYXJlbnQuJHBhcmVudC5nZXRVc2VycygpO1xuXHRcdFx0ICAgICAgICAgICAgICAgIFxuXHRcdFx0ICAgICAgICAgICAgICAgIHRoaXMucmVzZXRFZGl0U2NoZWR1bGVGb3JtKCk7XG5cblx0XHRcdCAgICAgICAgICAgICAgICAkKCcjZWRpdFNjaGVkdWxlJykubW9kYWwoJ2hpZGUnKTtcblxuXHRcdFx0ICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHJlc3BvbnNlLm1lc3NhZ2UpO1xuXHRcdFx0ICAgICAgICAgICAgfVxuXHRcdFx0ICAgICAgICB9KVxuXHRcdFx0ICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuXHRcdFx0ICAgICAgICBcdGxldCBlcnJvcnMgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGVycm9yKSk7XG5cblx0XHRcdCAgICAgICAgXHRmb3IodmFyIGtleSBpbiBlcnJvcnMpIHtcblx0XHRcdCAgICAgICAgXHRcdGxldCBfZXJyb3IgPSBlcnJvcnNba2V5XS5tZXNzYWdlO1xuXG5cdFx0XHQgICAgICAgIFx0XHRfZXJyb3IuZm9yRWFjaChlID0+IHtcblx0XHRcdCAgICAgICAgXHRcdFx0dG9hc3RyLmVycm9yKGUpO1xuXHRcdFx0ICAgICAgICBcdFx0fSk7XG5cdFx0XHQgICAgICAgIFx0fVxuXHRcdFx0ICAgICAgICB9KTtcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0Y3JlYXRlZCgpIHtcblx0XHRcdHRoaXMuZ2V0U2NoZWR1bGVUeXBlcygpO1xuXHRcdH0sXG5cblx0XHRtb3VudGVkKCkge1xuXHRcdFx0dGhpcy5pbml0VGltZVBpY2tlcigpO1xuXHRcdH1cblx0fVxuXG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cblx0Zm9ybSB7XG5cdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0fVxuXHQubS1yLTEwIHtcblx0XHRtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cdH1cbjwvc3R5bGU+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIGVkaXQtc2NoZWR1bGUudnVlP2Q0NjA2MGNlIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSgpO1xuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuZm9ybVtkYXRhLXYtOWY5MTY4NDRdIHtcXG5cXHRtYXJnaW4tYm90dG9tOiAzMHB4O1xcbn1cXG4ubS1yLTEwW2RhdGEtdi05ZjkxNjg0NF0ge1xcblxcdG1hcmdpbi1yaWdodDogMTBweDtcXG59XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcImVkaXQtc2NoZWR1bGUudnVlP2Q0NjA2MGNlXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUFvTUE7Q0FDQSxvQkFBQTtDQUNBO0FBQ0E7Q0FDQSxtQkFBQTtDQUNBXCIsXCJmaWxlXCI6XCJlZGl0LXNjaGVkdWxlLnZ1ZVwiLFwic291cmNlc0NvbnRlbnRcIjpbXCI8dGVtcGxhdGU+XFxuXFx0XFxuXFx0PGRpdj5cXG5cXHRcXHRcXG5cXHRcXHQ8Zm9ybSBjbGFzcz1cXFwiZm9ybS1ob3Jpem9udGFsXFxcIiBAc3VibWl0LnByZXZlbnQ9XFxcImVkaXRTY2hlZHVsZVxcXCI+XFxuXFxuXFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiZm9ybS1ncm91cFxcXCIgOmNsYXNzPVxcXCJ7ICdoYXMtZXJyb3InOiBlZGl0U2NoZWR1bGVGb3JtLmVycm9ycy5oYXMoJ3NjaGVkdWxlX3R5cGUnKSB9XFxcIj5cXG5cXHRcXHRcXHQgICAgPGxhYmVsIGNsYXNzPVxcXCJjb2wtbWQtMiBjb250cm9sLWxhYmVsXFxcIj5cXG5cXHRcXHRcXHQgICAgICAgIFNjaGVkdWxlIFR5cGU6IDxzcGFuIGNsYXNzPVxcXCJ0ZXh0LWRhbmdlclxcXCI+Kjwvc3Bhbj5cXG5cXHRcXHQgICAgICAgIDwvbGFiZWw+XFxuXFx0XFx0XFx0ICAgIDxkaXYgY2xhc3M9XFxcImNvbC1tZC0xMFxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdDxzZWxlY3QgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgbmFtZT1cXFwic2NoZWR1bGVfdHlwZVxcXCIgdi1tb2RlbD1cXFwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGVcXFwiPlxcblxcdFxcdFxcdCAgICBcXHRcXHQ8b3B0aW9uIHYtZm9yPVxcXCJzY2hlZHVsZVR5cGUgaW4gc2NoZWR1bGVUeXBlc1xcXCIgOnZhbHVlPVxcXCJzY2hlZHVsZVR5cGUuaWRcXFwiPlxcblxcdFxcdFxcdCAgICBcXHRcXHRcXHR7eyBzY2hlZHVsZVR5cGUubmFtZSB9fVxcblxcdFxcdFxcdCAgICBcXHRcXHQ8L29wdGlvbj5cXG5cXHRcXHRcXHQgICAgXFx0PC9zZWxlY3Q+XFxuXFx0XFx0XFx0ICAgIFxcdDxzcGFuIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiB2LWlmPVxcXCJlZGl0U2NoZWR1bGVGb3JtLmVycm9ycy5oYXMoJ3NjaGVkdWxlX3R5cGUnKVxcXCIgdi10ZXh0PVxcXCJlZGl0U2NoZWR1bGVGb3JtLmVycm9ycy5nZXQoJ3NjaGVkdWxlX3R5cGUnKVxcXCI+PC9zcGFuPlxcblxcdFxcdFxcdCAgICA8L2Rpdj5cXG5cXHRcXHRcXHQ8L2Rpdj5cXG5cXG5cXHRcXHRcXHQ8ZGl2IHYtc2hvdz1cXFwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMSB8fCAgZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMlxcXCIgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdCAgICA8bGFiZWwgY2xhc3M9XFxcImNvbC1tZC0yIGNvbnRyb2wtbGFiZWxcXFwiPlxcblxcdFxcdFxcdCAgICAgICAgU2NoZWR1bGUgRGV0YWlsczogPHNwYW4gY2xhc3M9XFxcInRleHQtZGFuZ2VyXFxcIj4qPC9zcGFuPlxcblxcdFxcdCAgICAgICAgPC9sYWJlbD5cXG5cXHRcXHRcXHQgICAgPGRpdiBjbGFzcz1cXFwiY29sLW1kLTEwXFxcIj5cXG5cXG5cXHRcXHRcXHQgICAgXFx0PGRpdiB2LXNob3c9XFxcImVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDFcXFwiPlxcblxcdFxcdFxcdCAgICBcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJpbnB1dC1ncm91cFxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgIDxpbnB1dCBpZD1cXFwidGltZXBpY2tlcjFcXFwiIHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFxcXCIgdi1tb2RlbD1cXFwiZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5cXFwiIGF1dG9jb21wbGV0ZT1cXFwib2ZmXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICAgIDwvZGl2PlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgPHNwYW4gY2xhc3M9XFxcImlucHV0LWdyb3VwLWFkZG9uXFxcIj50bzwvc3Bhbj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICAgIDxkaXYgY2xhc3M9XFxcImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgIDxpbnB1dCBpZD1cXFwidGltZXBpY2tlcjJcXFwiIHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFxcXCIgdi1tb2RlbD1cXFwiZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0XFxcIiBhdXRvY29tcGxldGU9XFxcIm9mZlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0ICAgIFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdCAgICBcXHQ8ZGl2IHYtc2hvdz1cXFwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMlxcXCI+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdDxkaXYgY2xhc3M9XFxcImlucHV0LWdyb3VwXFxcIj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0XFx0PHNwYW4gY2xhc3M9XFxcImlucHV0LWdyb3VwLWFkZG9uXFxcIj5UaW1lIEluIEZyb208L3NwYW4+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcXFwiPlxcblxcdFxcdFxcdFxcdFxcdCAgICAgICAgICAgIDxpbnB1dCBpZD1cXFwidGltZXBpY2tlcjNcXFwiIHR5cGU9XFxcInRleHRcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFxcXCIgdi1tb2RlbD1cXFwiZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5Gcm9tXFxcIiBhdXRvY29tcGxldGU9XFxcIm9mZlxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0ICAgICAgICA8L2Rpdj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdDxkaXYgc3R5bGU9XFxcImhlaWdodDoxMHB4OyBjbGVhcjpib3RoO1xcXCI+PC9kaXY+XFxuXFx0XFx0XFx0ICAgIFxcdFxcdDxkaXYgY2xhc3M9XFxcImlucHV0LWdyb3VwXFxcIj5cXG5cXHRcXHRcXHQgICAgXFx0XFx0XFx0PHNwYW4gY2xhc3M9XFxcImlucHV0LWdyb3VwLWFkZG9uXFxcIj5UaW1lIEluIFRvPC9zcGFuPlxcblxcdFxcdFxcdCAgICBcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJib290c3RyYXAtdGltZXBpY2tlciB0aW1lcGlja2VyXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICAgICAgICA8aW5wdXQgaWQ9XFxcInRpbWVwaWNrZXI0XFxcIiB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sIGlucHV0LXNtYWxsIHRpbWVwaWNrZXItaW5wdXRcXFwiIHYtbW9kZWw9XFxcImVkaXRTY2hlZHVsZUZvcm0udGltZUluVG9cXFwiIGF1dG9jb21wbGV0ZT1cXFwib2ZmXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQgICAgICAgIDwvZGl2PlxcblxcdFxcdFxcdCAgICBcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHQgICAgXFx0PC9kaXY+XFxuXFxuXFx0XFx0XFx0ICAgIDwvZGl2PlxcblxcdFxcdFxcdDwvZGl2PlxcblxcblxcdFxcdFxcdDxidXR0b24gdHlwZT1cXFwic3VibWl0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcXFwiPkVkaXQ8L2J1dHRvbj5cXG5cXHQgICAgXFx0PGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgcHVsbC1yaWdodCBtLXItMTBcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiPkNsb3NlPC9idXR0b24+XFxuXFxuXFx0XFx0PC9mb3JtPlxcblxcblxcdDwvZGl2PlxcblxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5cXHRcXG5cXHRleHBvcnQgZGVmYXVsdCB7XFxuXFx0XFx0cHJvcHM6IFsnc2VsZWN0ZWR1c2VyJ10sXFxuXFxuXFx0XFx0ZGF0YSgpIHtcXG5cXHRcXHRcXHRyZXR1cm4ge1xcblxcdFxcdFxcdFxcdHNjaGVkdWxlVHlwZXM6IFtdLFxcblxcblxcdFxcdFxcdFxcdGVkaXRTY2hlZHVsZUZvcm06IG5ldyBGb3JtKHtcXG5cXHRcXHRcXHRcXHRcXHR1c2VySWQ6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHNjaGVkdWxlVHlwZTogJycsXFxuXFx0XFx0XFx0XFx0XFx0dGltZUluOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aW1lT3V0OiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aW1lSW5Gcm9tOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHR0aW1lSW5UbzogJydcXG5cXHRcXHRcXHRcXHR9LCB7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KVxcblxcdFxcdFxcdH1cXG5cXHRcXHR9LFxcblxcblxcdFxcdG1ldGhvZHM6IHtcXG5cXHRcXHRcXHRpbml0VGltZVBpY2tlcigpIHtcXG5cXHRcXHRcXHRcXHRzZXRUaW1lb3V0KCgpID0+IHtcXG5cXHRcXHRcXHRcXHRcXHQkKCcudGltZXBpY2tlci1pbnB1dCcpLnRpbWVwaWNrZXIoKTtcXG5cXHRcXHRcXHRcXHR9LCA1MDApO1xcblxcblxcdFxcdFxcdFxcdCQoJy50aW1lcGlja2VyLWlucHV0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xcblxcdFxcdFxcdFxcdFxcdGxldCB0YXJnZXQgPSBlLnRhcmdldDtcXG5cXG5cXHRcXHRcXHRcXHRcXHQkKCcjJyArIHRhcmdldC5pZCkudGltZXBpY2tlcignc2hvd1dpZGdldCcpO1xcblxcblxcdFxcdFxcdFxcdFxcdGlmKHRhcmdldC52YWx1ZSA9PSAnJykge1xcblxcdFxcdFxcdFxcdFxcdFxcdCQoJyMnICsgdGFyZ2V0LmlkKS50aW1lcGlja2VyKCdzZXRUaW1lJywgJzEyOjAwIEFNJyk7XFxuXFx0XFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdFxcdH0pO1xcblxcblxcdFxcdFxcdFxcdCQoJy50aW1lcGlja2VyLWlucHV0JykudGltZXBpY2tlcigpLm9uKCdjaGFuZ2VUaW1lLnRpbWVwaWNrZXInLCAoZSkgPT4ge1xcblxcdFxcdFxcdFxcdFxcdGxldCBpZCA9IGUudGFyZ2V0LmlkO1xcblxcblxcdFxcdFxcdFxcdFxcdGlmKGlkID09ICd0aW1lcGlja2VyMScpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluID0gZS50aW1lLnZhbHVlO1xcblxcdFxcdFxcdFxcdFxcdH0gZWxzZSBpZihpZCA9PSAndGltZXBpY2tlcjInKSB7XFxuXFx0XFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVPdXQgPSBlLnRpbWUudmFsdWU7XFxuXFx0XFx0XFx0XFx0XFx0fSBlbHNlIGlmKGlkID09ICd0aW1lcGlja2VyMycpIHtcXG5cXHRcXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0udGltZUluRnJvbSA9IGUudGltZS52YWx1ZTtcXG5cXHRcXHRcXHRcXHRcXHR9IGVsc2UgaWYoaWQgPT0gJ3RpbWVwaWNrZXI0Jykge1xcblxcdFxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5UbyA9IGUudGltZS52YWx1ZTtcXG5cXHRcXHRcXHRcXHRcXHR9XFxuXFx0XFx0XFx0XFx0fSk7XFxuXFx0XFx0XFx0fSxcXG5cXHRcXHRcXHRnZXRTY2hlZHVsZVR5cGVzKCkge1xcblxcdFxcdFxcdFxcdGF4aW9zQVBJdjEuZ2V0KCcvc2NoZWR1bGUtdHlwZXMnKVxcblxcdFxcdFxcdFxcdCAgXFx0LnRoZW4oKHJlc3BvbnNlKSA9PiB7XFxuXFx0XFx0XFx0XFx0ICAgIFxcdHRoaXMuc2NoZWR1bGVUeXBlcyA9IHJlc3BvbnNlLmRhdGE7XFxuXFx0XFx0XFx0XFx0ICBcXHR9KTtcXG5cXHRcXHRcXHR9LFxcblxcdFxcdFxcdHJlc2V0RWRpdFNjaGVkdWxlRm9ybSgpIHtcXG5cXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0gPSBuZXcgRm9ybSh7XFxuXFx0XFx0XFx0XFx0XFx0dXNlcklkOiAnJyxcXG5cXHRcXHRcXHRcXHRcXHRzY2hlZHVsZVR5cGU6ICcnLFxcblxcdFxcdFxcdFxcdFxcdHRpbWVJbjogJycsXFxuXFx0XFx0XFx0XFx0XFx0dGltZU91dDogJycsXFxuXFx0XFx0XFx0XFx0XFx0dGltZUluRnJvbTogJycsXFxuXFx0XFx0XFx0XFx0XFx0dGltZUluVG86ICcnXFxuXFx0XFx0XFx0XFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSk7XFxuXFx0XFx0XFx0fSxcXG5cXHRcXHRcXHRzZXRTZWxlY3RlZFVzZXIodXNlcikge1xcblxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS51c2VySWQgPSB1c2VyLmlkO1xcblxcblxcdFxcdFxcdFxcdGlmKHVzZXIuc2NoZWR1bGUpIHtcXG5cXHRcXHRcXHRcXHRcXHR0aGlzLmVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID0gdXNlci5zY2hlZHVsZS5zY2hlZHVsZV90eXBlX2lkO1xcblxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9ICcnO1xcblxcdFxcdFxcdFxcdH1cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXG5cXHRcXHRcXHRcXHRpZih1c2VyLnNjaGVkdWxlICYmIHVzZXIuc2NoZWR1bGUudGltZV9pbiAmJiB1c2VyLnNjaGVkdWxlLnRpbWVfb3V0KSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbiA9IG1vbWVudCh1c2VyLnNjaGVkdWxlLnRpbWVfaW4sICdISDptbTpzcycpLmZvcm1hdChcXFwiaDptbSBBXFxcIik7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVPdXQgPSBtb21lbnQodXNlci5zY2hlZHVsZS50aW1lX291dCwgJ0hIOm1tOnNzJykuZm9ybWF0KFxcXCJoOm1tIEFcXFwiKVxcblxcdFxcdFxcdFxcdH0gZWxzZSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbiA9ICcnO1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0ID0gJyc7XFxuXFx0XFx0XFx0XFx0fVxcblxcblxcdFxcdFxcdFxcdGlmKHVzZXIuc2NoZWR1bGUgJiYgdXNlci5zY2hlZHVsZS50aW1lX2luX2Zyb20gJiYgdXNlci5zY2hlZHVsZS50aW1lX2luX3RvKSB7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20gPSBtb21lbnQodXNlci5zY2hlZHVsZS50aW1lX2luX2Zyb20sICdISDptbTpzcycpLmZvcm1hdChcXFwiaDptbSBBXFxcIik7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvID0gbW9tZW50KHVzZXIuc2NoZWR1bGUudGltZV9pbl90bywgJ0hIOm1tOnNzJykuZm9ybWF0KFxcXCJoOm1tIEFcXFwiKTtcXG5cXHRcXHRcXHRcXHR9IGVsc2Uge1xcblxcdFxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5Gcm9tID0gJyc7XFxuXFx0XFx0XFx0XFx0XFx0dGhpcy5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvID0gJyc7XFxuXFx0XFx0XFx0XFx0fVxcblxcdFxcdFxcdH0sXFxuXFx0XFx0XFx0ZWRpdFNjaGVkdWxlKCkge1xcblxcdFxcdFxcdFxcdHRoaXMuZWRpdFNjaGVkdWxlRm9ybS5zdWJtaXQoJ3BhdGNoJywgJy9zY2hlZHVsZS8nICsgdGhpcy5lZGl0U2NoZWR1bGVGb3JtLnVzZXJJZClcXG5cXHRcXHRcXHQgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcXG5cXHRcXHRcXHQgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzKSB7XFxuXFx0XFx0XFx0ICAgICAgICAgICAgXFx0dGhpcy4kcGFyZW50LiRwYXJlbnQuZ2V0VXNlcnMoKTtcXG5cXHRcXHRcXHQgICAgICAgICAgICAgICAgXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgIHRoaXMucmVzZXRFZGl0U2NoZWR1bGVGb3JtKCk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICAgICAgICAgICQoJyNlZGl0U2NoZWR1bGUnKS5tb2RhbCgnaGlkZScpO1xcblxcblxcdFxcdFxcdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcyhyZXNwb25zZS5tZXNzYWdlKTtcXG5cXHRcXHRcXHQgICAgICAgICAgICB9XFxuXFx0XFx0XFx0ICAgICAgICB9KVxcblxcdFxcdFxcdCAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcXG5cXHRcXHRcXHQgICAgICAgIFxcdGxldCBlcnJvcnMgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGVycm9yKSk7XFxuXFxuXFx0XFx0XFx0ICAgICAgICBcXHRmb3IodmFyIGtleSBpbiBlcnJvcnMpIHtcXG5cXHRcXHRcXHQgICAgICAgIFxcdFxcdGxldCBfZXJyb3IgPSBlcnJvcnNba2V5XS5tZXNzYWdlO1xcblxcblxcdFxcdFxcdCAgICAgICAgXFx0XFx0X2Vycm9yLmZvckVhY2goZSA9PiB7XFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHRcXHR0b2FzdHIuZXJyb3IoZSk7XFxuXFx0XFx0XFx0ICAgICAgICBcXHRcXHR9KTtcXG5cXHRcXHRcXHQgICAgICAgIFxcdH1cXG5cXHRcXHRcXHQgICAgICAgIH0pO1xcblxcdFxcdFxcdH1cXG5cXHRcXHR9LFxcblxcblxcdFxcdGNyZWF0ZWQoKSB7XFxuXFx0XFx0XFx0dGhpcy5nZXRTY2hlZHVsZVR5cGVzKCk7XFxuXFx0XFx0fSxcXG5cXG5cXHRcXHRtb3VudGVkKCkge1xcblxcdFxcdFxcdHRoaXMuaW5pdFRpbWVQaWNrZXIoKTtcXG5cXHRcXHR9XFxuXFx0fVxcblxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSBzY29wZWQ+XFxuXFx0Zm9ybSB7XFxuXFx0XFx0bWFyZ2luLWJvdHRvbTogMzBweDtcXG5cXHR9XFxuXFx0Lm0tci0xMCB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAxMHB4O1xcblxcdH1cXG48L3N0eWxlPlwiXX1dKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9+L3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtOWY5MTY4NDRcIixcInNjb3BlZFwiOnRydWUsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2FjY291bnRzL2NvbXBvbmVudHMvZWRpdC1zY2hlZHVsZS52dWVcbi8vIG1vZHVsZSBpZCA9IDI2XG4vLyBtb2R1bGUgY2h1bmtzID0gMTEgMTIiLCI8dGVtcGxhdGU+XG48ZGl2IGNsYXNzPVwibW9kYWwgbWQtbW9kYWwgZmFkZVwiIDppZD1cImlkXCIgdGFiaW5kZXg9XCItMVwiIHJvbGU9XCJkaWFsb2dcIiBhcmlhLWxhYmVsbGVkYnk9XCJtb2RhbC1sYWJlbFwiPlxuICAgIDxkaXYgOmNsYXNzPVwiJ21vZGFsLWRpYWxvZyAnK3NpemVcIiByb2xlPVwiZG9jdW1lbnRcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cbiAgICAgICAgICAgICAgICA8aDQgY2xhc3M9XCJtb2RhbC10aXRsZVwiIGlkPVwibW9kYWwtbGFiZWxcIj5cbiAgICAgICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLXRpdGxlXCI+TW9kYWwgVGl0bGU8L3Nsb3Q+XG4gICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtYm9keVwiPk1vZGFsIEJvZHk8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5XCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbjwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICBwcm9wczoge1xuICAgICAgICAnaWQnOntyZXF1aXJlZDp0cnVlfVxuICAgICAgICAsJ3NpemUnOiB7ZGVmYXVsdDonbW9kYWwtbWQnfVxuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIERpYWxvZ01vZGFsLnZ1ZT8wMDNiZGE4OCIsIlxuLyogc3R5bGVzICovXG5yZXF1aXJlKFwiISF2dWUtc3R5bGUtbG9hZGVyIWNzcy1sb2FkZXI/c291cmNlTWFwIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTlmOTE2ODQ0XFxcIixcXFwic2NvcGVkXFxcIjp0cnVlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXN0eWxlcyZpbmRleD0wIS4vZWRpdC1zY2hlZHVsZS52dWVcIilcblxudmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vZWRpdC1zY2hlZHVsZS52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTlmOTE2ODQ0XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL2VkaXQtc2NoZWR1bGUudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIFwiZGF0YS12LTlmOTE2ODQ0XCIsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gZWRpdC1zY2hlZHVsZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtOWY5MTY4NDRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi05ZjkxNjg0NFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlXG4vLyBtb2R1bGUgaWQgPSAyOFxuLy8gbW9kdWxlIGNodW5rcyA9IDExIDEyIiwiLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuICBNb2RpZmllZCBieSBFdmFuIFlvdSBAeXl4OTkwODAzXG4qL1xuXG52YXIgaGFzRG9jdW1lbnQgPSB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG5cbmlmICh0eXBlb2YgREVCVUcgIT09ICd1bmRlZmluZWQnICYmIERFQlVHKSB7XG4gIGlmICghaGFzRG9jdW1lbnQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgJ3Z1ZS1zdHlsZS1sb2FkZXIgY2Fubm90IGJlIHVzZWQgaW4gYSBub24tYnJvd3NlciBlbnZpcm9ubWVudC4gJyArXG4gICAgXCJVc2UgeyB0YXJnZXQ6ICdub2RlJyB9IGluIHlvdXIgV2VicGFjayBjb25maWcgdG8gaW5kaWNhdGUgYSBzZXJ2ZXItcmVuZGVyaW5nIGVudmlyb25tZW50LlwiXG4gICkgfVxufVxuXG52YXIgbGlzdFRvU3R5bGVzID0gcmVxdWlyZSgnLi9saXN0VG9TdHlsZXMnKVxuXG4vKlxudHlwZSBTdHlsZU9iamVjdCA9IHtcbiAgaWQ6IG51bWJlcjtcbiAgcGFydHM6IEFycmF5PFN0eWxlT2JqZWN0UGFydD5cbn1cblxudHlwZSBTdHlsZU9iamVjdFBhcnQgPSB7XG4gIGNzczogc3RyaW5nO1xuICBtZWRpYTogc3RyaW5nO1xuICBzb3VyY2VNYXA6ID9zdHJpbmdcbn1cbiovXG5cbnZhciBzdHlsZXNJbkRvbSA9IHsvKlxuICBbaWQ6IG51bWJlcl06IHtcbiAgICBpZDogbnVtYmVyLFxuICAgIHJlZnM6IG51bWJlcixcbiAgICBwYXJ0czogQXJyYXk8KG9iaj86IFN0eWxlT2JqZWN0UGFydCkgPT4gdm9pZD5cbiAgfVxuKi99XG5cbnZhciBoZWFkID0gaGFzRG9jdW1lbnQgJiYgKGRvY3VtZW50LmhlYWQgfHwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXSlcbnZhciBzaW5nbGV0b25FbGVtZW50ID0gbnVsbFxudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwXG52YXIgaXNQcm9kdWN0aW9uID0gZmFsc2VcbnZhciBub29wID0gZnVuY3Rpb24gKCkge31cblxuLy8gRm9yY2Ugc2luZ2xlLXRhZyBzb2x1dGlvbiBvbiBJRTYtOSwgd2hpY2ggaGFzIGEgaGFyZCBsaW1pdCBvbiB0aGUgIyBvZiA8c3R5bGU+XG4vLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG52YXIgaXNPbGRJRSA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIC9tc2llIFs2LTldXFxiLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSlcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAocGFyZW50SWQsIGxpc3QsIF9pc1Byb2R1Y3Rpb24pIHtcbiAgaXNQcm9kdWN0aW9uID0gX2lzUHJvZHVjdGlvblxuXG4gIHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIGxpc3QpXG4gIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG4gICAgdmFyIG1heVJlbW92ZSA9IFtdXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgICAgZG9tU3R5bGUucmVmcy0tXG4gICAgICBtYXlSZW1vdmUucHVzaChkb21TdHlsZSlcbiAgICB9XG4gICAgaWYgKG5ld0xpc3QpIHtcbiAgICAgIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhwYXJlbnRJZCwgbmV3TGlzdClcbiAgICAgIGFkZFN0eWxlc1RvRG9tKHN0eWxlcylcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVzID0gW11cbiAgICB9XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtYXlSZW1vdmUubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkb21TdHlsZSA9IG1heVJlbW92ZVtpXVxuICAgICAgaWYgKGRvbVN0eWxlLnJlZnMgPT09IDApIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKClcbiAgICAgICAgfVxuICAgICAgICBkZWxldGUgc3R5bGVzSW5Eb21bZG9tU3R5bGUuaWRdXG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlc1RvRG9tIChzdHlsZXMgLyogQXJyYXk8U3R5bGVPYmplY3Q+ICovKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBzdHlsZXNbaV1cbiAgICB2YXIgZG9tU3R5bGUgPSBzdHlsZXNJbkRvbVtpdGVtLmlkXVxuICAgIGlmIChkb21TdHlsZSkge1xuICAgICAgZG9tU3R5bGUucmVmcysrXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzW2pdKGl0ZW0ucGFydHNbal0pXG4gICAgICB9XG4gICAgICBmb3IgKDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdKSlcbiAgICAgIH1cbiAgICAgIGlmIChkb21TdHlsZS5wYXJ0cy5sZW5ndGggPiBpdGVtLnBhcnRzLmxlbmd0aCkge1xuICAgICAgICBkb21TdHlsZS5wYXJ0cy5sZW5ndGggPSBpdGVtLnBhcnRzLmxlbmd0aFxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgcGFydHMgPSBbXVxuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIHBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBzdHlsZXNJbkRvbVtpdGVtLmlkXSA9IHsgaWQ6IGl0ZW0uaWQsIHJlZnM6IDEsIHBhcnRzOiBwYXJ0cyB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVN0eWxlRWxlbWVudCAoKSB7XG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpXG4gIHN0eWxlRWxlbWVudC50eXBlID0gJ3RleHQvY3NzJ1xuICBoZWFkLmFwcGVuZENoaWxkKHN0eWxlRWxlbWVudClcbiAgcmV0dXJuIHN0eWxlRWxlbWVudFxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZSAob2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICB2YXIgdXBkYXRlLCByZW1vdmVcbiAgdmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ3N0eWxlW2RhdGEtdnVlLXNzci1pZH49XCInICsgb2JqLmlkICsgJ1wiXScpXG5cbiAgaWYgKHN0eWxlRWxlbWVudCkge1xuICAgIGlmIChpc1Byb2R1Y3Rpb24pIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGFuZCBpbiBwcm9kdWN0aW9uIG1vZGUuXG4gICAgICAvLyBzaW1wbHkgZG8gbm90aGluZy5cbiAgICAgIHJldHVybiBub29wXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGhhcyBTU1Igc3R5bGVzIGJ1dCBpbiBkZXYgbW9kZS5cbiAgICAgIC8vIGZvciBzb21lIHJlYXNvbiBDaHJvbWUgY2FuJ3QgaGFuZGxlIHNvdXJjZSBtYXAgaW4gc2VydmVyLXJlbmRlcmVkXG4gICAgICAvLyBzdHlsZSB0YWdzIC0gc291cmNlIG1hcHMgaW4gPHN0eWxlPiBvbmx5IHdvcmtzIGlmIHRoZSBzdHlsZSB0YWcgaXNcbiAgICAgIC8vIGNyZWF0ZWQgYW5kIGluc2VydGVkIGR5bmFtaWNhbGx5LiBTbyB3ZSByZW1vdmUgdGhlIHNlcnZlciByZW5kZXJlZFxuICAgICAgLy8gc3R5bGVzIGFuZCBpbmplY3QgbmV3IG9uZXMuXG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgaWYgKGlzT2xkSUUpIHtcbiAgICAvLyB1c2Ugc2luZ2xldG9uIG1vZGUgZm9yIElFOS5cbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrK1xuICAgIHN0eWxlRWxlbWVudCA9IHNpbmdsZXRvbkVsZW1lbnQgfHwgKHNpbmdsZXRvbkVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKSlcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCBmYWxzZSlcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50LCBzdHlsZUluZGV4LCB0cnVlKVxuICB9IGVsc2Uge1xuICAgIC8vIHVzZSBtdWx0aS1zdHlsZS10YWcgbW9kZSBpbiBhbGwgb3RoZXIgY2FzZXNcbiAgICBzdHlsZUVsZW1lbnQgPSBjcmVhdGVTdHlsZUVsZW1lbnQoKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQpXG4gICAgcmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmopXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmogLyogU3R5bGVPYmplY3RQYXJ0ICovKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcbiAgICAgICAgICBuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJlxuICAgICAgICAgIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKVxuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKVxuICAgIH1cbiAgfVxufVxuXG52YXIgcmVwbGFjZVRleHQgPSAoZnVuY3Rpb24gKCkge1xuICB2YXIgdGV4dFN0b3JlID0gW11cblxuICByZXR1cm4gZnVuY3Rpb24gKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudFxuICAgIHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpXG4gIH1cbn0pKClcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyAoc3R5bGVFbGVtZW50LCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcbiAgdmFyIGNzcyA9IHJlbW92ZSA/ICcnIDogb2JqLmNzc1xuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKVxuICB9IGVsc2Uge1xuICAgIHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKVxuICAgIHZhciBjaGlsZE5vZGVzID0gc3R5bGVFbGVtZW50LmNoaWxkTm9kZXNcbiAgICBpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSlcbiAgICBpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChjc3NOb2RlKVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRvVGFnIChzdHlsZUVsZW1lbnQsIG9iaikge1xuICB2YXIgY3NzID0gb2JqLmNzc1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWFcbiAgdmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXBcblxuICBpZiAobWVkaWEpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKCdtZWRpYScsIG1lZGlhKVxuICB9XG5cbiAgaWYgKHNvdXJjZU1hcCkge1xuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLmNocm9tZS5jb20vZGV2dG9vbHMvZG9jcy9qYXZhc2NyaXB0LWRlYnVnZ2luZ1xuICAgIC8vIHRoaXMgbWFrZXMgc291cmNlIG1hcHMgaW5zaWRlIHN0eWxlIHRhZ3Mgd29yayBwcm9wZXJseSBpbiBDaHJvbWVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VVUkw9JyArIHNvdXJjZU1hcC5zb3VyY2VzWzBdICsgJyAqLydcbiAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNjYwMzg3NVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCwnICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArICcgKi8nXG4gIH1cblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzXG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpXG4gICAgfVxuICAgIHN0eWxlRWxlbWVudC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKVxuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyIDMgNCA1IDkgMTEgMTIgMTMiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmVkaXRTY2hlZHVsZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uZWRpdFNjaGVkdWxlRm9ybS5lcnJvcnMuaGFzKCdzY2hlZHVsZV90eXBlJylcbiAgICB9XG4gIH0sIFtfdm0uX20oMCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEwXCJcbiAgfSwgW19jKCdzZWxlY3QnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZVwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwibmFtZVwiOiBcInNjaGVkdWxlX3R5cGVcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRzZWxlY3RlZFZhbCA9IEFycmF5LnByb3RvdHlwZS5maWx0ZXIuY2FsbCgkZXZlbnQudGFyZ2V0Lm9wdGlvbnMsIGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICByZXR1cm4gby5zZWxlY3RlZFxuICAgICAgICB9KS5tYXAoZnVuY3Rpb24obykge1xuICAgICAgICAgIHZhciB2YWwgPSBcIl92YWx1ZVwiIGluIG8gPyBvLl92YWx1ZSA6IG8udmFsdWU7XG4gICAgICAgICAgcmV0dXJuIHZhbFxuICAgICAgICB9KTtcbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmVkaXRTY2hlZHVsZUZvcm0sIFwic2NoZWR1bGVUeXBlXCIsICRldmVudC50YXJnZXQubXVsdGlwbGUgPyAkJHNlbGVjdGVkVmFsIDogJCRzZWxlY3RlZFZhbFswXSlcbiAgICAgIH1cbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLnNjaGVkdWxlVHlwZXMpLCBmdW5jdGlvbihzY2hlZHVsZVR5cGUpIHtcbiAgICByZXR1cm4gX2MoJ29wdGlvbicsIHtcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogc2NoZWR1bGVUeXBlLmlkXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcblxcdFxcdCAgICBcXHRcXHRcXHRcIiArIF92bS5fcyhzY2hlZHVsZVR5cGUubmFtZSkgKyBcIlxcblxcdFxcdCAgICBcXHRcXHRcIildKVxuICB9KSwgMCksIF92bS5fdihcIiBcIiksIChfdm0uZWRpdFNjaGVkdWxlRm9ybS5lcnJvcnMuaGFzKCdzY2hlZHVsZV90eXBlJykpID8gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiLFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInRleHRDb250ZW50XCI6IF92bS5fcyhfdm0uZWRpdFNjaGVkdWxlRm9ybS5lcnJvcnMuZ2V0KCdzY2hlZHVsZV90eXBlJykpXG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMSB8fCBfdm0uZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMiksXG4gICAgICBleHByZXNzaW9uOiBcImVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDEgfHwgIGVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDJcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX3ZtLl9tKDEpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5lZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAxKSxcbiAgICAgIGV4cHJlc3Npb246IFwiZWRpdFNjaGVkdWxlRm9ybS5zY2hlZHVsZVR5cGUgPT0gMVwiXG4gICAgfV1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXBcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJib290c3RyYXAtdGltZXBpY2tlciB0aW1lcGlja2VyXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbiksXG4gICAgICBleHByZXNzaW9uOiBcImVkaXRTY2hlZHVsZUZvcm0udGltZUluXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidGltZXBpY2tlcjFcIixcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS50aW1lSW4pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNjaGVkdWxlRm9ybSwgXCJ0aW1lSW5cIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgIH1cbiAgICB9XG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgfSwgW192bS5fdihcInRvXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYm9vdHN0cmFwLXRpbWVwaWNrZXIgdGltZXBpY2tlclwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0KSxcbiAgICAgIGV4cHJlc3Npb246IFwiZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidGltZXBpY2tlcjJcIixcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS50aW1lT3V0KVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLiRzZXQoX3ZtLmVkaXRTY2hlZHVsZUZvcm0sIFwidGltZU91dFwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTY2hlZHVsZUZvcm0uc2NoZWR1bGVUeXBlID09IDIpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2NoZWR1bGVGb3JtLnNjaGVkdWxlVHlwZSA9PSAyXCJcbiAgICB9XVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cFwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cC1hZGRvblwiXG4gIH0sIFtfdm0uX3YoXCJUaW1lIEluIEZyb21cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJib290c3RyYXAtdGltZXBpY2tlciB0aW1lcGlja2VyXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20pLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb21cIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbCBpbnB1dC1zbWFsbCB0aW1lcGlja2VyLWlucHV0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0aW1lcGlja2VyM1wiLFxuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJhdXRvY29tcGxldGVcIjogXCJvZmZcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5lZGl0U2NoZWR1bGVGb3JtLnRpbWVJbkZyb20pXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uZWRpdFNjaGVkdWxlRm9ybSwgXCJ0aW1lSW5Gcm9tXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMTBweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXBcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIlxuICB9LCBbX3ZtLl92KFwiVGltZSBJbiBUb1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJvb3RzdHJhcC10aW1lcGlja2VyIHRpbWVwaWNrZXJcIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmVkaXRTY2hlZHVsZUZvcm0udGltZUluVG8pLFxuICAgICAgZXhwcmVzc2lvbjogXCJlZGl0U2NoZWR1bGVGb3JtLnRpbWVJblRvXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgaW5wdXQtc21hbGwgdGltZXBpY2tlci1pbnB1dFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidGltZXBpY2tlcjRcIixcbiAgICAgIFwidHlwZVwiOiBcInRleHRcIixcbiAgICAgIFwiYXV0b2NvbXBsZXRlXCI6IFwib2ZmXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0uZWRpdFNjaGVkdWxlRm9ybS50aW1lSW5UbylcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5lZGl0U2NoZWR1bGVGb3JtLCBcInRpbWVJblRvXCIsICRldmVudC50YXJnZXQudmFsdWUpXG4gICAgICB9XG4gICAgfVxuICB9KV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IHB1bGwtcmlnaHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwic3VibWl0XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJFZGl0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgIFNjaGVkdWxlIFR5cGU6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29udHJvbC1sYWJlbFwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHQgICAgICAgIFNjaGVkdWxlIERldGFpbHM6IFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGV4dC1kYW5nZXJcIlxuICB9LCBbX3ZtLl92KFwiKlwiKV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi05ZjkxNjg0NFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTlmOTE2ODQ0XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzM1xuLy8gbW9kdWxlIGNodW5rcyA9IDExIDEyIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi05ZjkxNjg0NFxcXCIsXFxcInNjb3BlZFxcXCI6dHJ1ZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL2VkaXQtc2NoZWR1bGUudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCI4ODA1OTU0MlwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtOWY5MTY4NDRcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9lZGl0LXNjaGVkdWxlLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtOWY5MTY4NDRcXFwiLFxcXCJzY29wZWRcXFwiOnRydWUsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9lZGl0LXNjaGVkdWxlLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1zdHlsZS1sb2FkZXIhLi9+L2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vfi92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTlmOTE2ODQ0XCIsXCJzY29wZWRcIjp0cnVlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2VkaXQtc2NoZWR1bGUudnVlXG4vLyBtb2R1bGUgaWQgPSAzNFxuLy8gbW9kdWxlIGNodW5rcyA9IDExIDEyIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi00MDk1NDY0OFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvdG9wd3ljL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRGlhbG9nTW9kYWwudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTQwOTU0NjQ4XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNDA5NTQ2NDhcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDM2MFxuLy8gbW9kdWxlIGNodW5rcyA9IDEyIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwgbWQtbW9kYWwgZmFkZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IF92bS5pZCxcbiAgICAgIFwidGFiaW5kZXhcIjogXCItMVwiLFxuICAgICAgXCJyb2xlXCI6IFwiZGlhbG9nXCIsXG4gICAgICBcImFyaWEtbGFiZWxsZWRieVwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGNsYXNzOiAnbW9kYWwtZGlhbG9nICcgKyBfdm0uc2l6ZSxcbiAgICBhdHRyczoge1xuICAgICAgXCJyb2xlXCI6IFwiZG9jdW1lbnRcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWhlYWRlclwiXG4gIH0sIFtfYygnaDQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtdGl0bGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiLCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtYm9keVwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1ib2R5XCIsIFtfdm0uX3YoXCJNb2RhbCBCb2R5XCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtZm9vdGVyXCIsIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTQwOTU0NjQ4XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNDA5NTQ2NDhcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSAzOTBcbi8vIG1vZHVsZSBjaHVua3MgPSAxMiIsIi8qKlxuICogVHJhbnNsYXRlcyB0aGUgbGlzdCBmb3JtYXQgcHJvZHVjZWQgYnkgY3NzLWxvYWRlciBpbnRvIHNvbWV0aGluZ1xuICogZWFzaWVyIHRvIG1hbmlwdWxhdGUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbGlzdFRvU3R5bGVzIChwYXJlbnRJZCwgbGlzdCkge1xuICB2YXIgc3R5bGVzID0gW11cbiAgdmFyIG5ld1N0eWxlcyA9IHt9XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGlzdC5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gbGlzdFtpXVxuICAgIHZhciBpZCA9IGl0ZW1bMF1cbiAgICB2YXIgY3NzID0gaXRlbVsxXVxuICAgIHZhciBtZWRpYSA9IGl0ZW1bMl1cbiAgICB2YXIgc291cmNlTWFwID0gaXRlbVszXVxuICAgIHZhciBwYXJ0ID0ge1xuICAgICAgaWQ6IHBhcmVudElkICsgJzonICsgaSxcbiAgICAgIGNzczogY3NzLFxuICAgICAgbWVkaWE6IG1lZGlhLFxuICAgICAgc291cmNlTWFwOiBzb3VyY2VNYXBcbiAgICB9XG4gICAgaWYgKCFuZXdTdHlsZXNbaWRdKSB7XG4gICAgICBzdHlsZXMucHVzaChuZXdTdHlsZXNbaWRdID0geyBpZDogaWQsIHBhcnRzOiBbcGFydF0gfSlcbiAgICB9IGVsc2Uge1xuICAgICAgbmV3U3R5bGVzW2lkXS5wYXJ0cy5wdXNoKHBhcnQpXG4gICAgfVxuICB9XG4gIHJldHVybiBzdHlsZXNcbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtc3R5bGUtbG9hZGVyL2xpYi9saXN0VG9TdHlsZXMuanNcbi8vIG1vZHVsZSBpZCA9IDdcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDIgMyA0IDUgOSAxMSAxMiAxMyJdLCJzb3VyY2VSb290IjoiIn0=