/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 461);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 171:
/***/ (function(module, exports, __webpack_require__) {

new Vue({
	el: '#show-employee',

	data: {
		employee: null
	},

	methods: {
		getEmployee: function getEmployee() {
			var _this = this;

			var pathArray = window.location.pathname.split('/');

			var employeeId = pathArray[2];

			axios.get('/emp/' + employeeId + '/show-detail').then(function (response) {
				_this.employee = response.data;
			});
		},
		showLeaveModal: function showLeaveModal() {
			$('#fileLeaveModal').modal('show');
		}
	},

	created: function created() {
		this.getEmployee();
	},


	components: {
		'file-leave': __webpack_require__(359),
		'attendance': __webpack_require__(358)
	}
});

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['employeeid'],

	data: function data() {
		return {
			loading: true,

			selectYears: ['2018', '2019', '2020', '2021', '2022'],

			filterYear: new Date().getFullYear(),

			current: true,

			attendance: []
		};
	},


	methods: {
		filter: function filter() {
			this.setCurrent();

			this.getAttendance();
		},
		setCurrent: function setCurrent() {
			if (this.filterYear == new Date().getFullYear()) this.current = true;else this.current = false;
		},
		getAttendance: function getAttendance() {
			var _this = this;

			this.loading = true;

			axiosAPIv1.get('/employee/' + this.employeeid + '/attendance', {
				params: {
					filterYear: this.filterYear
				}
			}).then(function (response) {
				_this.attendance = response.data.attendance;

				setTimeout(function () {
					_this.loading = false;

					$("#fixTable").tableHeadFixer({ "head": false, "left": 1 });

					var position = _this.current ? 200 * new Date().getDate() - 200 : 0;
					$("#fixTable-wrapper").animate({ scrollLeft: position }, 100);
				}, 1000);
			});
		}
	},

	created: function created() {
		this.setCurrent();

		this.getAttendance();
	}
});

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	props: [],

	data: function data() {
		return {
			fileLeaveForm: new Form({
				title: '',
				reason: '',
				start_date: '',
				end_date: '',
				type: ''
			}, { baseURL: axiosAPIv1.defaults.baseURL })
		};
	},


	methods: {},
	created: function created() {},
	mounted: function mounted() {}
});

/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(265),
  /* template */
  __webpack_require__(409),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/employee/accounts/components/attendance.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] attendance.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-73ee6d96", Component.options)
  } else {
    hotAPI.reload("data-v-73ee6d96", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(266),
  /* template */
  __webpack_require__(399),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/cpanel/employee/accounts/components/file-leave.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] file-leave.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5deccaf2", Component.options)
  } else {
    hotAPI.reload("data-v-5deccaf2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 399:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('form', {
    staticClass: "form-horizontal",
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.editSchedule($event)
      }
    }
  }, [_c('div', {
    staticClass: "form-group label-floating",
    class: {
      'has-error': _vm.fileLeaveForm.errors.has('title')
    }
  }, [_c('label', {
    attrs: {
      "for": "title"
    }
  }, [_vm._v("Title:")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.fileLeaveForm.title),
      expression: "fileLeaveForm.title"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "required": "",
      "id": "title",
      "name": "title"
    },
    domProps: {
      "value": (_vm.fileLeaveForm.title)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.fileLeaveForm, "title", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.fileLeaveForm.errors.has('title')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.fileLeaveForm.errors.get('title'))
    }
  }) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "form-group label-floating",
    class: {
      'has-error': _vm.fileLeaveForm.errors.has('reason')
    }
  }, [_c('label', {
    attrs: {
      "for": "reason"
    }
  }, [_vm._v("Reason:")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.fileLeaveForm.reason),
      expression: "fileLeaveForm.reason"
    }],
    staticClass: "form-control",
    attrs: {
      "required": "",
      "id": "reason",
      "name": "reason"
    },
    domProps: {
      "value": (_vm.fileLeaveForm.reason)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.fileLeaveForm, "reason", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.fileLeaveForm.errors.has('reason')) ? _c('p', {
    staticClass: "help-block",
    domProps: {
      "textContent": _vm._s(_vm.fileLeaveForm.errors.get('reason'))
    }
  }) : _vm._e()]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("Submit")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default pull-right m-r-10",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5deccaf2", module.exports)
  }
}

/***/ }),

/***/ 409:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.employeeid) ? _c('div', {
    staticStyle: {
      "margin": "10px"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (!_vm.loading),
      expression: "!loading"
    }]
  }, [_c('center', [_c('h1', [_vm._v(_vm._s(_vm.filterYear))]), _vm._v(" "), _c('form', {
    staticClass: "form-inline"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Year")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.filterYear),
      expression: "filterYear"
    }],
    staticClass: "form-control",
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.filterYear = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, _vm._l((_vm.selectYears), function(year) {
    return _c('option', {
      domProps: {
        "value": year
      }
    }, [_vm._v("\n                                " + _vm._s(year) + "\n                            ")])
  }), 0)]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "id": "filter-btn",
      "type": "button"
    },
    on: {
      "click": _vm.filter
    }
  }, [_vm._v("Filter")])])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "15px",
      "clear": "both"
    }
  }), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "15px",
      "clear": "both"
    }
  }), _vm._v(" "), _c('div', {
    attrs: {
      "id": "fixTable-wrapper"
    }
  }, [_c('table', {
    staticClass: "table table-bordered table-striped",
    attrs: {
      "id": "fixTable"
    }
  }, [_c('thead', [_c('tr', [_c('th', [_vm._v("Month")]), _vm._v(" "), _vm._l((31), function(n) {
    return _c('th', [_c('strong', [_vm._v(_vm._s(n))])])
  })], 2)]), _vm._v(" "), _c('tbody', _vm._l((_vm.attendance), function(a) {
    return _c('tr', [_c('td', {
      staticClass: "month"
    }, [_c('strong', [_vm._v(_vm._s(a.month))])]), _vm._v(" "), _vm._l((a.data), function(d) {
      return _c('td', {
        class: {
          'current': d.current,
            'weekends': (d.time_in_status == 'WEEKENDS') && (d.time_out_status == 'WEEKENDS')
        }
      }, [(d.has_data) ? _c('span', [(d.time_in_status == 'ABSENT' && d.time_out_status == 'ABSENT') ? _c('span', [_c('span', {
        staticClass: "label label-absent"
      }, [_vm._v("ABSENT")])]) : _c('span', [(d.time_in) ? _c('span', {
        staticClass: "label label-def",
        class: {
          'label-late': d.time_in_status == 'LATE',
            'label-slate': d.time_in_status == 'SLATE'
        }
      }, [_vm._v("\n                                            " + _vm._s(d.time_in) + "\n                                        ")]) : _vm._e(), _vm._v("\n                                          \n                                        "), (d.time_out) ? _c('span', {
        staticClass: "label label-def",
        class: {
          'label-earlyout': d.time_out_status == 'EARLYOUT'
        }
      }, [_vm._v("\n                                            " + _vm._s(d.time_out) + "\n                                        ")]) : _vm._e()])]) : _vm._e()])
    })], 2)
  }), 0)])])], 1), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.loading),
      expression: "loading"
    }]
  }, [_vm._m(1)])]) : _vm._e()
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "pull-right"
  }, [_vm._v("\n\t\t\t        Legend:\n\t\t\t        "), _c('span', {
    staticClass: "label label-def"
  }, [_vm._v("DEFAULT")]), _vm._v(" "), _c('span', {
    staticClass: "label label-late"
  }, [_vm._v("LATE")]), _vm._v(" "), _c('span', {
    staticClass: "label label-slate"
  }, [_vm._v("SUPER LATE")]), _vm._v(" "), _c('span', {
    staticClass: "label label-earlyout"
  }, [_vm._v("EARLY OUT")]), _vm._v(" "), _c('span', {
    staticClass: "label label-absent"
  }, [_vm._v("ABSENT")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "spiner-example"
  }, [_c('div', {
    staticClass: "sk-spinner sk-spinner-wave"
  }, [_c('div', {
    staticClass: "sk-rect1"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect2"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect3"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect4"
  }), _vm._v(" "), _c('div', {
    staticClass: "sk-rect5"
  })])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-73ee6d96", module.exports)
  }
}

/***/ }),

/***/ 461:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(171);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjExM2VkMzZhNWE1NmI3ZGNmNjM/ZGIyNyoqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9zaG93LmpzIiwid2VicGFjazovLy9hdHRlbmRhbmNlLnZ1ZSIsIndlYnBhY2s6Ly8vZmlsZS1sZWF2ZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvY29tcG9uZW50cy9hdHRlbmRhbmNlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2ZpbGUtbGVhdmUudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2VtcGxveWVlL2FjY291bnRzL2NvbXBvbmVudHMvZmlsZS1sZWF2ZS52dWU/YWUwYiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2F0dGVuZGFuY2UudnVlPzViMjciXSwibmFtZXMiOlsiVnVlIiwiZWwiLCJkYXRhIiwiZW1wbG95ZWUiLCJtZXRob2RzIiwiZ2V0RW1wbG95ZWUiLCJwYXRoQXJyYXkiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInBhdGhuYW1lIiwic3BsaXQiLCJlbXBsb3llZUlkIiwiYXhpb3MiLCJnZXQiLCJ0aGVuIiwicmVzcG9uc2UiLCJzaG93TGVhdmVNb2RhbCIsIiQiLCJtb2RhbCIsImNyZWF0ZWQiLCJjb21wb25lbnRzIiwicmVxdWlyZSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBLElBQUlBLEdBQUosQ0FBUTtBQUNQQyxLQUFJLGdCQURHOztBQUdQQyxPQUFNO0FBQ0xDLFlBQVU7QUFETCxFQUhDOztBQU9QQyxVQUFTO0FBQ1JDLGFBRFEseUJBQ007QUFBQTs7QUFDYixPQUFJQyxZQUFZQyxPQUFPQyxRQUFQLENBQWdCQyxRQUFoQixDQUF5QkMsS0FBekIsQ0FBZ0MsR0FBaEMsQ0FBaEI7O0FBRUEsT0FBSUMsYUFBYUwsVUFBVSxDQUFWLENBQWpCOztBQUVBTSxTQUFNQyxHQUFOLENBQVUsVUFBVUYsVUFBVixHQUF1QixjQUFqQyxFQUNJRyxJQURKLENBQ1MsVUFBQ0MsUUFBRCxFQUFjO0FBQ25CLFVBQUtaLFFBQUwsR0FBZ0JZLFNBQVNiLElBQXpCO0FBQ0EsSUFISjtBQUlBLEdBVk87QUFZUmMsZ0JBWlEsNEJBWVE7QUFDZkMsS0FBRSxpQkFBRixFQUFxQkMsS0FBckIsQ0FBMkIsTUFBM0I7QUFDQTtBQWRPLEVBUEY7O0FBd0JQQyxRQXhCTyxxQkF3Qkc7QUFDVCxPQUFLZCxXQUFMO0FBQ0EsRUExQk07OztBQTRCUGUsYUFBWTtBQUNYLGdCQUFjQyxtQkFBT0EsQ0FBQyxHQUFSLENBREg7QUFFWCxnQkFBY0EsbUJBQU9BLENBQUMsR0FBUjtBQUZIO0FBNUJMLENBQVIsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZ0dBO0FBQ0Esc0JBREE7O0FBR0EsS0FIQSxrQkFHQTtBQUNBO0FBQ0EsZ0JBREE7O0FBR0Esd0RBSEE7O0FBS0EsdUNBTEE7O0FBT0EsZ0JBUEE7O0FBU0E7QUFUQTtBQVdBLEVBZkE7OztBQWlCQTtBQUNBLFFBREEsb0JBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBTEE7QUFPQSxZQVBBLHdCQU9BO0FBQ0Esb0RBQ0Esb0JBREEsS0FHQTtBQUNBLEdBWkE7QUFjQSxlQWRBLDJCQWNBO0FBQUE7O0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFEQSxNQUtBLElBTEEsQ0FLQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBUEEsRUFPQSxJQVBBO0FBUUEsSUFoQkE7QUFpQkE7QUFsQ0EsRUFqQkE7O0FBc0RBLFFBdERBLHFCQXNEQTtBQUNBOztBQUVBO0FBQ0E7QUExREEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hEQTtBQUNBLFVBREE7O0FBR0EsS0FIQSxrQkFHQTtBQUNBO0FBQ0E7QUFDQSxhQURBO0FBRUEsY0FGQTtBQUdBLGtCQUhBO0FBSUEsZ0JBSkE7QUFLQTtBQUxBLE1BTUEsd0NBTkE7QUFEQTtBQVNBLEVBYkE7OztBQWVBLFlBZkE7QUFnQkEsUUFoQkEscUJBZ0JBLEVBaEJBO0FBaUJBLFFBakJBLHFCQWlCQTtBQWpCQSxHOzs7Ozs7O0FDeENBLGdCQUFnQixtQkFBTyxDQUFDLENBQXdFO0FBQ2hHO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTJPO0FBQ3JQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTBNO0FBQ3BOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQU8sQ0FBQyxDQUF3RTtBQUNoRztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUEyTztBQUNyUDtBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUEwTTtBQUNwTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUNyR0EsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUMsYUFBYSxhQUFhLDBCQUEwQjtBQUNyRDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9lbXBsb3llZS9hY2NvdW50cy9zaG93LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0NjEpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGYxMTNlZDM2YTVhNTZiN2RjZjYzIiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUiLCJuZXcgVnVlKHtcblx0ZWw6ICcjc2hvdy1lbXBsb3llZScsXG5cblx0ZGF0YToge1xuXHRcdGVtcGxveWVlOiBudWxsXG5cdH0sXG5cblx0bWV0aG9kczoge1xuXHRcdGdldEVtcGxveWVlKCkge1xuXHRcdFx0bGV0IHBhdGhBcnJheSA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZS5zcGxpdCggJy8nICk7XG5cblx0XHRcdGxldCBlbXBsb3llZUlkID0gcGF0aEFycmF5WzJdO1xuXG5cdFx0XHRheGlvcy5nZXQoJy9lbXAvJyArIGVtcGxveWVlSWQgKyAnL3Nob3ctZGV0YWlsJylcblx0XHRcdCAgXHQudGhlbigocmVzcG9uc2UpID0+IHtcblx0XHRcdCAgXHRcdHRoaXMuZW1wbG95ZWUgPSByZXNwb25zZS5kYXRhO1xuXHRcdFx0ICBcdH0pO1xuXHRcdH0sXG5cblx0XHRzaG93TGVhdmVNb2RhbCgpe1xuXHRcdFx0JCgnI2ZpbGVMZWF2ZU1vZGFsJykubW9kYWwoJ3Nob3cnKTtcblx0XHR9XG5cdH0sXG5cblx0Y3JlYXRlZCgpIHtcblx0XHR0aGlzLmdldEVtcGxveWVlKCk7XG5cdH0sXG5cblx0Y29tcG9uZW50czoge1xuXHRcdCdmaWxlLWxlYXZlJzogcmVxdWlyZSgnLi9jb21wb25lbnRzL2ZpbGUtbGVhdmUudnVlJyksXG5cdFx0J2F0dGVuZGFuY2UnOiByZXF1aXJlKCcuL2NvbXBvbmVudHMvYXR0ZW5kYW5jZS52dWUnKVxuXHR9XG59KTtcblxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvc2hvdy5qcyIsIjx0ZW1wbGF0ZT5cblx0XG5cdDxkaXYgdi1pZj1cImVtcGxveWVlaWRcIiBzdHlsZT1cIm1hcmdpbjoxMHB4O1wiPlxuXG5cdFx0PGRpdiB2LXNob3c9XCIhbG9hZGluZ1wiPlxuXHRcdFx0PGNlbnRlcj5cbiAgICAgICAgICAgICAgICA8aDE+e3sgZmlsdGVyWWVhciB9fTwvaDE+XG5cbiAgICAgICAgICAgICAgICA8Zm9ybSBjbGFzcz1cImZvcm0taW5saW5lXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+WWVhcjwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c2VsZWN0IGNsYXNzPVwiZm9ybS1jb250cm9sXCIgdi1tb2RlbD1cImZpbHRlclllYXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIDp2YWx1ZT1cInllYXJcIiB2LWZvcj1cInllYXIgaW4gc2VsZWN0WWVhcnNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3sgeWVhciB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGlkPVwiZmlsdGVyLWJ0blwiIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIEBjbGljaz1cImZpbHRlclwiPkZpbHRlcjwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgICAgIDwvY2VudGVyPlxuXG4gICAgICAgICAgICA8ZGl2IHN0eWxlPVwiaGVpZ2h0OjE1cHg7Y2xlYXI6Ym90aDtcIj48L2Rpdj5cblxuXHRcdFx0PGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxuXHRcdFx0ICAgXHQ8ZGl2IGNsYXNzPVwicHVsbC1yaWdodFwiPlxuXHRcdFx0ICAgICAgICBMZWdlbmQ6XG5cdFx0XHQgICAgICAgIDxzcGFuIGNsYXNzPVwibGFiZWwgbGFiZWwtZGVmXCI+REVGQVVMVDwvc3Bhbj5cblx0XHRcdCAgICAgICAgPHNwYW4gY2xhc3M9XCJsYWJlbCBsYWJlbC1sYXRlXCI+TEFURTwvc3Bhbj5cblx0XHRcdCAgICAgICAgPHNwYW4gY2xhc3M9XCJsYWJlbCBsYWJlbC1zbGF0ZVwiPlNVUEVSIExBVEU8L3NwYW4+XG5cdFx0XHQgICAgICAgIDxzcGFuIGNsYXNzPVwibGFiZWwgbGFiZWwtZWFybHlvdXRcIj5FQVJMWSBPVVQ8L3NwYW4+XG5cdFx0XHQgICAgICAgIDxzcGFuIGNsYXNzPVwibGFiZWwgbGFiZWwtYWJzZW50XCI+QUJTRU5UPC9zcGFuPlxuXHRcdFx0ICAgIDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cblx0XHRcdDxkaXYgc3R5bGU9XCJoZWlnaHQ6MTVweDtjbGVhcjpib3RoO1wiPjwvZGl2PlxuXG5cdFx0XHQ8ZGl2IGlkPVwiZml4VGFibGUtd3JhcHBlclwiPlxuXHRcdFx0XHQ8dGFibGUgaWQ9XCJmaXhUYWJsZVwiIGNsYXNzPVwidGFibGUgdGFibGUtYm9yZGVyZWQgdGFibGUtc3RyaXBlZFwiPlxuICAgICAgICAgICAgICAgICAgICA8dGhlYWQ+XG4gICAgICAgICAgICAgICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoPk1vbnRoPC90aD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGggdi1mb3I9XCJuIGluIDMxXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzdHJvbmc+e3sgbiB9fTwvc3Ryb25nPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGg+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgICA8L3RoZWFkPlxuICAgICAgICAgICAgICAgICAgICA8dGJvZHk+XG4gICAgICAgICAgICAgICAgICAgIFx0PHRyIHYtZm9yPVwiYSBpbiBhdHRlbmRhbmNlXCI+XG4gICAgICAgICAgICAgICAgICAgIFx0XHQ8dGQgY2xhc3M9XCJtb250aFwiPjxzdHJvbmc+e3sgYS5tb250aCB9fTwvc3Ryb25nPjwvdGQ+XG4gICAgICAgICAgICAgICAgICAgIFx0XHQ8dGQgdi1mb3I9XCJkIGluIGEuZGF0YVwiIDpjbGFzcz1cIntcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2N1cnJlbnQnIDogZC5jdXJyZW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnd2Vla2VuZHMnIDogKGQudGltZV9pbl9zdGF0dXMgPT0gJ1dFRUtFTkRTJykgJiYgKGQudGltZV9vdXRfc3RhdHVzID09ICdXRUVLRU5EUycpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVwiPlxuICAgICAgICAgICAgICAgICAgICBcdFx0XHQ8c3BhbiB2LWlmPVwiZC5oYXNfZGF0YVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gdi1pZj1cImQudGltZV9pbl9zdGF0dXMgPT0gJ0FCU0VOVCcgJiYgZC50aW1lX291dF9zdGF0dXMgPT0gJ0FCU0VOVCdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImxhYmVsIGxhYmVsLWFic2VudFwiPkFCU0VOVDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIHYtZWxzZT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiB2LWlmPVwiZC50aW1lX2luXCIgY2xhc3M9XCJsYWJlbCBsYWJlbC1kZWZcIiA6Y2xhc3M9XCJ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdsYWJlbC1sYXRlJyA6IGQudGltZV9pbl9zdGF0dXMgPT0gJ0xBVEUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnbGFiZWwtc2xhdGUnIDogZC50aW1lX2luX3N0YXR1cyA9PSAnU0xBVEUnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7eyBkLnRpbWVfaW4gfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJm5ic3A7Jm5ic3A7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gdi1pZj1cImQudGltZV9vdXRcIiBjbGFzcz1cImxhYmVsIGxhYmVsLWRlZlwiIDpjbGFzcz1cIntcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2xhYmVsLWVhcmx5b3V0JyA6IGQudGltZV9vdXRfc3RhdHVzID09ICdFQVJMWU9VVCdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGQudGltZV9vdXQgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgXHRcdDwvdGQ+XG4gICAgICAgICAgICAgICAgICAgIFx0PC90cj5cbiAgICAgICAgICAgICAgICAgICAgPC90Ym9keT5cbiAgICAgICAgICAgICAgICAgPC90YWJsZT5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cdFx0PGRpdiB2LXNob3c9XCJsb2FkaW5nXCI+XG5cdCAgICAgICAgPGRpdiBjbGFzcz1cInNwaW5lci1leGFtcGxlXCI+XG5cdCAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzay1zcGlubmVyIHNrLXNwaW5uZXItd2F2ZVwiPlxuXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNrLXJlY3QxXCI+PC9kaXY+XG5cdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2stcmVjdDJcIj48L2Rpdj5cblx0ICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzay1yZWN0M1wiPjwvZGl2PlxuXHQgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNrLXJlY3Q0XCI+PC9kaXY+XG5cdCAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2stcmVjdDVcIj48L2Rpdj5cblx0ICAgICAgICAgICAgPC9kaXY+XG5cdCAgICAgICAgPC9kaXY+XG5cdCAgICA8L2Rpdj5cblxuICAgIDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXHRleHBvcnQgZGVmYXVsdCB7XG5cdFx0cHJvcHM6IFsnZW1wbG95ZWVpZCddLFxuXG5cdFx0ZGF0YSgpIHtcblx0XHRcdHJldHVybiB7XG5cdFx0XHRcdGxvYWRpbmc6IHRydWUsXG5cblx0XHRcdFx0c2VsZWN0WWVhcnM6IFsnMjAxOCcsICcyMDE5JywgJzIwMjAnLCAnMjAyMScsICcyMDIyJ10sXG5cblx0XHRcdFx0ZmlsdGVyWWVhcjogbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpLFxuXG5cdFx0XHRcdGN1cnJlbnQ6IHRydWUsXG5cblx0XHRcdFx0YXR0ZW5kYW5jZTogW11cblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0bWV0aG9kczoge1xuXHRcdFx0ZmlsdGVyKCkge1xuXHRcdFx0XHR0aGlzLnNldEN1cnJlbnQoKTtcblxuXHRcdFx0XHR0aGlzLmdldEF0dGVuZGFuY2UoKTtcblx0XHRcdH0sXG5cblx0XHRcdHNldEN1cnJlbnQoKSB7XG5cdFx0XHRcdGlmKHRoaXMuZmlsdGVyWWVhciA9PSBuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCkpXG5cdFx0XHRcdFx0dGhpcy5jdXJyZW50ID0gdHJ1ZTtcblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdHRoaXMuY3VycmVudCA9IGZhbHNlO1xuXHRcdFx0fSxcblxuXHRcdFx0Z2V0QXR0ZW5kYW5jZSgpIHtcblx0XHRcdFx0dGhpcy5sb2FkaW5nID0gdHJ1ZTtcblxuXHRcdFx0XHRheGlvc0FQSXYxLmdldCgnL2VtcGxveWVlLycgKyB0aGlzLmVtcGxveWVlaWQgKyAnL2F0dGVuZGFuY2UnLCB7XG5cdFx0XHRcdFx0ICAgIHBhcmFtczoge1xuXHRcdFx0XHRcdCAgICAgIFx0ZmlsdGVyWWVhcjogdGhpcy5maWx0ZXJZZWFyXG5cdFx0XHRcdFx0ICAgIH1cblx0XHRcdFx0XHR9KVxuXHRcdFx0ICBcdFx0LnRoZW4oKHJlc3BvbnNlKSA9PiB7XG5cdFx0XHQgIFx0XHRcdHRoaXMuYXR0ZW5kYW5jZSA9IHJlc3BvbnNlLmRhdGEuYXR0ZW5kYW5jZTtcblxuXHRcdFx0ICBcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdCAgXHRcdFx0XHR0aGlzLmxvYWRpbmcgPSBmYWxzZTtcblxuXHRcdFx0ICBcdFx0XHRcdCQoXCIjZml4VGFibGVcIikudGFibGVIZWFkRml4ZXIoe1wiaGVhZFwiIDogZmFsc2UsIFwibGVmdFwiIDogMX0pO1xuXG5cdFx0XHQgIFx0XHRcdFx0bGV0IHBvc2l0aW9uID0gKHRoaXMuY3VycmVudCkgPyAoMjAwICogKG5ldyBEYXRlKCkuZ2V0RGF0ZSgpKSkgLSAyMDAgOiAwO1xuXHRcdCAgXHRcdFx0XHRcdCQoXCIjZml4VGFibGUtd3JhcHBlclwiKS5hbmltYXRlKHtzY3JvbGxMZWZ0OiBwb3NpdGlvbn0sIDEwMCk7XG5cdFx0XHQgIFx0XHRcdH0sIDEwMDApO1xuXHRcdFx0ICBcdFx0fSk7XG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdGNyZWF0ZWQoKSB7XG5cdFx0XHR0aGlzLnNldEN1cnJlbnQoKTtcblxuXHRcdFx0dGhpcy5nZXRBdHRlbmRhbmNlKCk7XG5cdFx0fVxuXHR9XG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gYXR0ZW5kYW5jZS52dWU/ZTMxOTlkMDYiLCI8dGVtcGxhdGU+XG5cdDxkaXY+XG5cdFx0PGZvcm0gY2xhc3M9XCJmb3JtLWhvcml6b250YWxcIiBAc3VibWl0LnByZXZlbnQ9XCJlZGl0U2NoZWR1bGVcIj5cblx0XHQgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgbGFiZWwtZmxvYXRpbmdcIiA6Y2xhc3M9XCJ7ICdoYXMtZXJyb3InOiBmaWxlTGVhdmVGb3JtLmVycm9ycy5oYXMoJ3RpdGxlJykgfVwiPlxuICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJ0aXRsZVwiPlRpdGxlOjwvbGFiZWw+XG4gICAgICAgICAgICAgICAgIDxpbnB1dCBcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIiBcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWRcbiAgICAgICAgICAgICAgICAgICAgaWQ9XCJ0aXRsZVwiIFxuICAgICAgICAgICAgICAgICAgICBuYW1lPVwidGl0bGVcIlxuICAgICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiZmlsZUxlYXZlRm9ybS50aXRsZVwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwiaGVscC1ibG9ja1wiIHYtaWY9XCJmaWxlTGVhdmVGb3JtLmVycm9ycy5oYXMoJ3RpdGxlJylcIiB2LXRleHQ9XCJmaWxlTGVhdmVGb3JtLmVycm9ycy5nZXQoJ3RpdGxlJylcIj48L3A+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIFxuXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBsYWJlbC1mbG9hdGluZ1wiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGZpbGVMZWF2ZUZvcm0uZXJyb3JzLmhhcygncmVhc29uJykgfVwiPlxuICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJyZWFzb25cIj5SZWFzb246PC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgPHRleHRhcmVhXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgXG4gICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkXG4gICAgICAgICAgICAgICAgICAgIGlkPVwicmVhc29uXCIgXG4gICAgICAgICAgICAgICAgICAgIG5hbWU9XCJyZWFzb25cIlxuICAgICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiZmlsZUxlYXZlRm9ybS5yZWFzb25cIlxuICAgICAgICAgICAgICAgID48L3RleHRhcmVhPiBcbiAgICAgICAgICAgICAgICA8cCBjbGFzcz1cImhlbHAtYmxvY2tcIiB2LWlmPVwiZmlsZUxlYXZlRm9ybS5lcnJvcnMuaGFzKCdyZWFzb24nKVwiIHYtdGV4dD1cImZpbGVMZWF2ZUZvcm0uZXJyb3JzLmdldCgncmVhc29uJylcIj48L3A+XG4gICAgICAgICAgICA8L2Rpdj5cblxuXG5cdFx0XHQ8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCI+U3VibWl0PC9idXR0b24+XG5cdCAgICBcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuXG5cdFx0PC9mb3JtPlx0XG5cdDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXHRcblx0ZXhwb3J0IGRlZmF1bHQge1xuXHRcdHByb3BzOiBbXSxcblxuXHRcdGRhdGEoKSB7XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRmaWxlTGVhdmVGb3JtOiBuZXcgRm9ybSh7XG5cdFx0XHRcdFx0dGl0bGU6JycsXG5cdFx0XHRcdFx0cmVhc29uOicnLFxuXHRcdFx0XHRcdHN0YXJ0X2RhdGU6JycsXG5cdFx0XHRcdFx0ZW5kX2RhdGU6JycsXG5cdFx0XHRcdFx0dHlwZTonJyxcblx0XHRcdFx0fSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSlcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0bWV0aG9kczoge30sXG5cdFx0Y3JlYXRlZCgpIHt9LFxuXHRcdG1vdW50ZWQoKSB7fVxuXHR9XG5cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIGZpbGUtbGVhdmUudnVlPzE1MTY2ODExIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vYXR0ZW5kYW5jZS52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTczZWU2ZDk2XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL2F0dGVuZGFuY2UudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2F0dGVuZGFuY2UudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gYXR0ZW5kYW5jZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNzNlZTZkOTZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi03M2VlNmQ5NlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2F0dGVuZGFuY2UudnVlXG4vLyBtb2R1bGUgaWQgPSAzNThcbi8vIG1vZHVsZSBjaHVua3MgPSAxNCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL2ZpbGUtbGVhdmUudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi01ZGVjY2FmMlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9maWxlLWxlYXZlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvY29tcG9uZW50cy9maWxlLWxlYXZlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIGZpbGUtbGVhdmUudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTVkZWNjYWYyXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNWRlY2NhZjJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvY29tcG9uZW50cy9maWxlLWxlYXZlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzU5XG4vLyBtb2R1bGUgY2h1bmtzID0gMTQiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWhvcml6b250YWxcIixcbiAgICBvbjoge1xuICAgICAgXCJzdWJtaXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICByZXR1cm4gX3ZtLmVkaXRTY2hlZHVsZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwIGxhYmVsLWZsb2F0aW5nXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uZmlsZUxlYXZlRm9ybS5lcnJvcnMuaGFzKCd0aXRsZScpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImZvclwiOiBcInRpdGxlXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJUaXRsZTpcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmZpbGVMZWF2ZUZvcm0udGl0bGUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJmaWxlTGVhdmVGb3JtLnRpdGxlXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwidGV4dFwiLFxuICAgICAgXCJyZXF1aXJlZFwiOiBcIlwiLFxuICAgICAgXCJpZFwiOiBcInRpdGxlXCIsXG4gICAgICBcIm5hbWVcIjogXCJ0aXRsZVwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmZpbGVMZWF2ZUZvcm0udGl0bGUpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uJHNldChfdm0uZmlsZUxlYXZlRm9ybSwgXCJ0aXRsZVwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIChfdm0uZmlsZUxlYXZlRm9ybS5lcnJvcnMuaGFzKCd0aXRsZScpKSA/IF9jKCdwJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIixcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ0ZXh0Q29udGVudFwiOiBfdm0uX3MoX3ZtLmZpbGVMZWF2ZUZvcm0uZXJyb3JzLmdldCgndGl0bGUnKSlcbiAgICB9XG4gIH0pIDogX3ZtLl9lKCldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwIGxhYmVsLWZsb2F0aW5nXCIsXG4gICAgY2xhc3M6IHtcbiAgICAgICdoYXMtZXJyb3InOiBfdm0uZmlsZUxlYXZlRm9ybS5lcnJvcnMuaGFzKCdyZWFzb24nKVxuICAgIH1cbiAgfSwgW19jKCdsYWJlbCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJmb3JcIjogXCJyZWFzb25cIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlJlYXNvbjpcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RleHRhcmVhJywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmZpbGVMZWF2ZUZvcm0ucmVhc29uKSxcbiAgICAgIGV4cHJlc3Npb246IFwiZmlsZUxlYXZlRm9ybS5yZWFzb25cIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInJlcXVpcmVkXCI6IFwiXCIsXG4gICAgICBcImlkXCI6IFwicmVhc29uXCIsXG4gICAgICBcIm5hbWVcIjogXCJyZWFzb25cIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5maWxlTGVhdmVGb3JtLnJlYXNvbilcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS4kc2V0KF92bS5maWxlTGVhdmVGb3JtLCBcInJlYXNvblwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIChfdm0uZmlsZUxlYXZlRm9ybS5lcnJvcnMuaGFzKCdyZWFzb24nKSkgPyBfYygncCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5maWxlTGVhdmVGb3JtLmVycm9ycy5nZXQoJ3JlYXNvbicpKVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBwdWxsLXJpZ2h0XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcInN1Ym1pdFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiU3VibWl0XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0IHB1bGwtcmlnaHQgbS1yLTEwXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNWRlY2NhZjJcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi01ZGVjY2FmMlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvZW1wbG95ZWUvYWNjb3VudHMvY29tcG9uZW50cy9maWxlLWxlYXZlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzk5XG4vLyBtb2R1bGUgY2h1bmtzID0gMTQiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gKF92bS5lbXBsb3llZWlkKSA/IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luXCI6IFwiMTBweFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6ICghX3ZtLmxvYWRpbmcpLFxuICAgICAgZXhwcmVzc2lvbjogXCIhbG9hZGluZ1wiXG4gICAgfV1cbiAgfSwgW19jKCdjZW50ZXInLCBbX2MoJ2gxJywgW192bS5fdihfdm0uX3MoX3ZtLmZpbHRlclllYXIpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZm9ybScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWlubGluZVwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIlxuICB9LCBbX2MoJ2xhYmVsJywgW192bS5fdihcIlllYXJcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NlbGVjdCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5maWx0ZXJZZWFyKSxcbiAgICAgIGV4cHJlc3Npb246IFwiZmlsdGVyWWVhclwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2hhbmdlXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICB2YXIgJCRzZWxlY3RlZFZhbCA9IEFycmF5LnByb3RvdHlwZS5maWx0ZXIuY2FsbCgkZXZlbnQudGFyZ2V0Lm9wdGlvbnMsIGZ1bmN0aW9uKG8pIHtcbiAgICAgICAgICByZXR1cm4gby5zZWxlY3RlZFxuICAgICAgICB9KS5tYXAoZnVuY3Rpb24obykge1xuICAgICAgICAgIHZhciB2YWwgPSBcIl92YWx1ZVwiIGluIG8gPyBvLl92YWx1ZSA6IG8udmFsdWU7XG4gICAgICAgICAgcmV0dXJuIHZhbFxuICAgICAgICB9KTtcbiAgICAgICAgX3ZtLmZpbHRlclllYXIgPSAkZXZlbnQudGFyZ2V0Lm11bHRpcGxlID8gJCRzZWxlY3RlZFZhbCA6ICQkc2VsZWN0ZWRWYWxbMF1cbiAgICAgIH1cbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLnNlbGVjdFllYXJzKSwgZnVuY3Rpb24oeWVhcikge1xuICAgIHJldHVybiBfYygnb3B0aW9uJywge1xuICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgXCJ2YWx1ZVwiOiB5ZWFyXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoeWVhcikgKyBcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIildKVxuICB9KSwgMCldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXByaW1hcnlcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImZpbHRlci1idG5cIixcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0uZmlsdGVyXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiRmlsdGVyXCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImhlaWdodFwiOiBcIjE1cHhcIixcbiAgICAgIFwiY2xlYXJcIjogXCJib3RoXCJcbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX20oMCksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiaGVpZ2h0XCI6IFwiMTVweFwiLFxuICAgICAgXCJjbGVhclwiOiBcImJvdGhcIlxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJmaXhUYWJsZS13cmFwcGVyXCJcbiAgICB9XG4gIH0sIFtfYygndGFibGUnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGFibGUgdGFibGUtYm9yZGVyZWQgdGFibGUtc3RyaXBlZFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwiZml4VGFibGVcIlxuICAgIH1cbiAgfSwgW19jKCd0aGVhZCcsIFtfYygndHInLCBbX2MoJ3RoJywgW192bS5fdihcIk1vbnRoXCIpXSksIF92bS5fdihcIiBcIiksIF92bS5fbCgoMzEpLCBmdW5jdGlvbihuKSB7XG4gICAgcmV0dXJuIF9jKCd0aCcsIFtfYygnc3Ryb25nJywgW192bS5fdihfdm0uX3MobikpXSldKVxuICB9KV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0Ym9keScsIF92bS5fbCgoX3ZtLmF0dGVuZGFuY2UpLCBmdW5jdGlvbihhKSB7XG4gICAgcmV0dXJuIF9jKCd0cicsIFtfYygndGQnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJtb250aFwiXG4gICAgfSwgW19jKCdzdHJvbmcnLCBbX3ZtLl92KF92bS5fcyhhLm1vbnRoKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX2woKGEuZGF0YSksIGZ1bmN0aW9uKGQpIHtcbiAgICAgIHJldHVybiBfYygndGQnLCB7XG4gICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgJ2N1cnJlbnQnOiBkLmN1cnJlbnQsXG4gICAgICAgICAgICAnd2Vla2VuZHMnOiAoZC50aW1lX2luX3N0YXR1cyA9PSAnV0VFS0VORFMnKSAmJiAoZC50aW1lX291dF9zdGF0dXMgPT0gJ1dFRUtFTkRTJylcbiAgICAgICAgfVxuICAgICAgfSwgWyhkLmhhc19kYXRhKSA/IF9jKCdzcGFuJywgWyhkLnRpbWVfaW5fc3RhdHVzID09ICdBQlNFTlQnICYmIGQudGltZV9vdXRfc3RhdHVzID09ICdBQlNFTlQnKSA/IF9jKCdzcGFuJywgW19jKCdzcGFuJywge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1hYnNlbnRcIlxuICAgICAgfSwgW192bS5fdihcIkFCU0VOVFwiKV0pXSkgOiBfYygnc3BhbicsIFsoZC50aW1lX2luKSA/IF9jKCdzcGFuJywge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1kZWZcIixcbiAgICAgICAgY2xhc3M6IHtcbiAgICAgICAgICAnbGFiZWwtbGF0ZSc6IGQudGltZV9pbl9zdGF0dXMgPT0gJ0xBVEUnLFxuICAgICAgICAgICAgJ2xhYmVsLXNsYXRlJzogZC50aW1lX2luX3N0YXR1cyA9PSAnU0xBVEUnXG4gICAgICAgIH1cbiAgICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGQudGltZV9pbikgKyBcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIildKSA6IF92bS5fZSgpLCBfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgwqDCoFxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIiksIChkLnRpbWVfb3V0KSA/IF9jKCdzcGFuJywge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1kZWZcIixcbiAgICAgICAgY2xhc3M6IHtcbiAgICAgICAgICAnbGFiZWwtZWFybHlvdXQnOiBkLnRpbWVfb3V0X3N0YXR1cyA9PSAnRUFSTFlPVVQnXG4gICAgICAgIH1cbiAgICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGQudGltZV9vdXQpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIpXSkgOiBfdm0uX2UoKV0pXSkgOiBfdm0uX2UoKV0pXG4gICAgfSldLCAyKVxuICB9KSwgMCldKV0pXSwgMSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmxvYWRpbmcpLFxuICAgICAgZXhwcmVzc2lvbjogXCJsb2FkaW5nXCJcbiAgICB9XVxuICB9LCBbX3ZtLl9tKDEpXSldKSA6IF92bS5fZSgpXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTJcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwdWxsLXJpZ2h0XCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdCAgICAgICAgTGVnZW5kOlxcblxcdFxcdFxcdCAgICAgICAgXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1kZWZcIlxuICB9LCBbX3ZtLl92KFwiREVGQVVMVFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1sYXRlXCJcbiAgfSwgW192bS5fdihcIkxBVEVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWwgbGFiZWwtc2xhdGVcIlxuICB9LCBbX3ZtLl92KFwiU1VQRVIgTEFURVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1lYXJseW91dFwiXG4gIH0sIFtfdm0uX3YoXCJFQVJMWSBPVVRcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWwgbGFiZWwtYWJzZW50XCJcbiAgfSwgW192bS5fdihcIkFCU0VOVFwiKV0pXSldKVxufSxmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwic3BpbmVyLWV4YW1wbGVcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzay1zcGlubmVyIHNrLXNwaW5uZXItd2F2ZVwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNrLXJlY3QxXCJcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwic2stcmVjdDJcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzay1yZWN0M1wiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInNrLXJlY3Q0XCJcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwic2stcmVjdDVcIlxuICB9KV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi03M2VlNmQ5NlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTczZWU2ZDk2XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9lbXBsb3llZS9hY2NvdW50cy9jb21wb25lbnRzL2F0dGVuZGFuY2UudnVlXG4vLyBtb2R1bGUgaWQgPSA0MDlcbi8vIG1vZHVsZSBjaHVua3MgPSAxNCJdLCJzb3VyY2VSb290IjoiIn0=