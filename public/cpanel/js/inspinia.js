/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 258);
/******/ })
/************************************************************************/
/******/ ({

/***/ 136:
/***/ (function(module, exports) {

/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.7.1
 *
 */

$(document).ready(function () {

    // Add body-small class if window less than 768px
    if ($(this).width() < 769) {
        $('body').addClass('body-small');
    } else {
        $('body').removeClass('body-small');
    }

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Collapse ibox function
    $('.collapse-link').on('click', function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.children('.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $('.close-link').on('click', function () {
        var content = $(this).closest('div.ibox');
        content.remove();
    });

    // Fullscreen ibox function
    $('.fullscreen-link').on('click', function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        $('body').toggleClass('fullscreen-ibox-mode');
        button.toggleClass('fa-expand').toggleClass('fa-compress');
        ibox.toggleClass('fullscreen');
        setTimeout(function () {
            $(window).trigger('resize');
        }, 100);
    });

    // Minimalize menu
    $('.navbar-minimalize').on('click', function (event) {
        event.preventDefault();
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    });

    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeight = $('nav.navbar-default').height();
        var wrapperHeight = $('#page-wrapper').height();

        if (navbarHeight > wrapperHeight) {
            $('#page-wrapper').css("min-height", navbarHeight + "px");
        }

        if (navbarHeight < wrapperHeight) {
            $('#page-wrapper').css("min-height", $(window).height() + "px");
        }

        if ($('body').hasClass('fixed-nav')) {
            if (navbarHeight > wrapperHeight) {
                $('#page-wrapper').css("min-height", navbarHeight + "px");
            } else {
                $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
            }
        }
    }

    fix_height();

    // Fixed Sidebar
    $(window).bind("load", function () {
        if ($("body").hasClass('fixed-sidebar')) {
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }
    });

    $(window).bind("load resize scroll", function () {
        if (!$("body").hasClass('body-small')) {
            fix_height();
        }
    });

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    });
});

// Minimalize menu when screen is less than 768px
$(window).bind("resize", function () {
    if ($(this).width() < 769) {
        $('body').addClass('body-small');
    } else {
        $('body').removeClass('body-small');
    }
});

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(function () {
            $('#side-menu').fadeIn(400);
        }, 200);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(function () {
            $('#side-menu').fadeIn(400);
        }, 100);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

/***/ }),

/***/ 258:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(136);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDE/NTk0YSoqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2luc3BpbmlhLmpzIl0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5Iiwid2lkdGgiLCJhZGRDbGFzcyIsInJlbW92ZUNsYXNzIiwibWV0aXNNZW51Iiwib24iLCJpYm94IiwiY2xvc2VzdCIsImJ1dHRvbiIsImZpbmQiLCJjb250ZW50IiwiY2hpbGRyZW4iLCJzbGlkZVRvZ2dsZSIsInRvZ2dsZUNsYXNzIiwic2V0VGltZW91dCIsInJlc2l6ZSIsInJlbW92ZSIsIndpbmRvdyIsInRyaWdnZXIiLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiU21vb3RobHlNZW51IiwiZml4X2hlaWdodCIsImhlaWdodFdpdGhvdXROYXZiYXIiLCJoZWlnaHQiLCJjc3MiLCJuYXZiYXJIZWlnaHQiLCJ3cmFwcGVySGVpZ2h0IiwiaGFzQ2xhc3MiLCJiaW5kIiwic2xpbVNjcm9sbCIsInJhaWxPcGFjaXR5Iiwic2xpbXNjcm9sbCIsImhpZGUiLCJmYWRlSW4iLCJyZW1vdmVBdHRyIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBOzs7Ozs7O0FBT0FBLEVBQUVDLFFBQUYsRUFBWUMsS0FBWixDQUFrQixZQUFZOztBQUcxQjtBQUNBLFFBQUlGLEVBQUUsSUFBRixFQUFRRyxLQUFSLEtBQWtCLEdBQXRCLEVBQTJCO0FBQ3ZCSCxVQUFFLE1BQUYsRUFBVUksUUFBVixDQUFtQixZQUFuQjtBQUNILEtBRkQsTUFFTztBQUNISixVQUFFLE1BQUYsRUFBVUssV0FBVixDQUFzQixZQUF0QjtBQUNIOztBQUVEO0FBQ0FMLE1BQUUsWUFBRixFQUFnQk0sU0FBaEI7O0FBRUE7QUFDQU4sTUFBRSxnQkFBRixFQUFvQk8sRUFBcEIsQ0FBdUIsT0FBdkIsRUFBZ0MsWUFBWTtBQUN4QyxZQUFJQyxPQUFPUixFQUFFLElBQUYsRUFBUVMsT0FBUixDQUFnQixVQUFoQixDQUFYO0FBQ0EsWUFBSUMsU0FBU1YsRUFBRSxJQUFGLEVBQVFXLElBQVIsQ0FBYSxHQUFiLENBQWI7QUFDQSxZQUFJQyxVQUFVSixLQUFLSyxRQUFMLENBQWMsZUFBZCxDQUFkO0FBQ0FELGdCQUFRRSxXQUFSLENBQW9CLEdBQXBCO0FBQ0FKLGVBQU9LLFdBQVAsQ0FBbUIsZUFBbkIsRUFBb0NBLFdBQXBDLENBQWdELGlCQUFoRDtBQUNBUCxhQUFLTyxXQUFMLENBQWlCLEVBQWpCLEVBQXFCQSxXQUFyQixDQUFpQyxlQUFqQztBQUNBQyxtQkFBVyxZQUFZO0FBQ25CUixpQkFBS1MsTUFBTDtBQUNBVCxpQkFBS0csSUFBTCxDQUFVLFlBQVYsRUFBd0JNLE1BQXhCO0FBQ0gsU0FIRCxFQUdHLEVBSEg7QUFJSCxLQVhEOztBQWFBO0FBQ0FqQixNQUFFLGFBQUYsRUFBaUJPLEVBQWpCLENBQW9CLE9BQXBCLEVBQTZCLFlBQVk7QUFDckMsWUFBSUssVUFBVVosRUFBRSxJQUFGLEVBQVFTLE9BQVIsQ0FBZ0IsVUFBaEIsQ0FBZDtBQUNBRyxnQkFBUU0sTUFBUjtBQUNILEtBSEQ7O0FBS0E7QUFDQWxCLE1BQUUsa0JBQUYsRUFBc0JPLEVBQXRCLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDMUMsWUFBSUMsT0FBT1IsRUFBRSxJQUFGLEVBQVFTLE9BQVIsQ0FBZ0IsVUFBaEIsQ0FBWDtBQUNBLFlBQUlDLFNBQVNWLEVBQUUsSUFBRixFQUFRVyxJQUFSLENBQWEsR0FBYixDQUFiO0FBQ0FYLFVBQUUsTUFBRixFQUFVZSxXQUFWLENBQXNCLHNCQUF0QjtBQUNBTCxlQUFPSyxXQUFQLENBQW1CLFdBQW5CLEVBQWdDQSxXQUFoQyxDQUE0QyxhQUE1QztBQUNBUCxhQUFLTyxXQUFMLENBQWlCLFlBQWpCO0FBQ0FDLG1CQUFXLFlBQVk7QUFDbkJoQixjQUFFbUIsTUFBRixFQUFVQyxPQUFWLENBQWtCLFFBQWxCO0FBQ0gsU0FGRCxFQUVHLEdBRkg7QUFHSCxLQVREOztBQVdBO0FBQ0FwQixNQUFFLG9CQUFGLEVBQXdCTyxFQUF4QixDQUEyQixPQUEzQixFQUFvQyxVQUFVYyxLQUFWLEVBQWlCO0FBQ2pEQSxjQUFNQyxjQUFOO0FBQ0F0QixVQUFFLE1BQUYsRUFBVWUsV0FBVixDQUFzQixhQUF0QjtBQUNBUTtBQUVILEtBTEQ7O0FBT0E7QUFDQSxhQUFTQyxVQUFULEdBQXNCO0FBQ2xCLFlBQUlDLHNCQUFzQnpCLEVBQUUsaUJBQUYsRUFBcUIwQixNQUFyQixLQUFnQyxFQUExRDtBQUNBMUIsVUFBRSxpQkFBRixFQUFxQjJCLEdBQXJCLENBQXlCLFlBQXpCLEVBQXVDRixzQkFBc0IsSUFBN0Q7O0FBRUEsWUFBSUcsZUFBZTVCLEVBQUUsb0JBQUYsRUFBd0IwQixNQUF4QixFQUFuQjtBQUNBLFlBQUlHLGdCQUFnQjdCLEVBQUUsZUFBRixFQUFtQjBCLE1BQW5CLEVBQXBCOztBQUVBLFlBQUlFLGVBQWVDLGFBQW5CLEVBQWtDO0FBQzlCN0IsY0FBRSxlQUFGLEVBQW1CMkIsR0FBbkIsQ0FBdUIsWUFBdkIsRUFBcUNDLGVBQWUsSUFBcEQ7QUFDSDs7QUFFRCxZQUFJQSxlQUFlQyxhQUFuQixFQUFrQztBQUM5QjdCLGNBQUUsZUFBRixFQUFtQjJCLEdBQW5CLENBQXVCLFlBQXZCLEVBQXFDM0IsRUFBRW1CLE1BQUYsRUFBVU8sTUFBVixLQUFxQixJQUExRDtBQUNIOztBQUVELFlBQUkxQixFQUFFLE1BQUYsRUFBVThCLFFBQVYsQ0FBbUIsV0FBbkIsQ0FBSixFQUFxQztBQUNqQyxnQkFBSUYsZUFBZUMsYUFBbkIsRUFBa0M7QUFDOUI3QixrQkFBRSxlQUFGLEVBQW1CMkIsR0FBbkIsQ0FBdUIsWUFBdkIsRUFBcUNDLGVBQWUsSUFBcEQ7QUFDSCxhQUZELE1BRU87QUFDSDVCLGtCQUFFLGVBQUYsRUFBbUIyQixHQUFuQixDQUF1QixZQUF2QixFQUFxQzNCLEVBQUVtQixNQUFGLEVBQVVPLE1BQVYsS0FBcUIsRUFBckIsR0FBMEIsSUFBL0Q7QUFDSDtBQUNKO0FBRUo7O0FBRURGOztBQUVBO0FBQ0F4QixNQUFFbUIsTUFBRixFQUFVWSxJQUFWLENBQWUsTUFBZixFQUF1QixZQUFZO0FBQy9CLFlBQUkvQixFQUFFLE1BQUYsRUFBVThCLFFBQVYsQ0FBbUIsZUFBbkIsQ0FBSixFQUF5QztBQUNyQzlCLGNBQUUsbUJBQUYsRUFBdUJnQyxVQUF2QixDQUFrQztBQUM5Qk4sd0JBQVEsTUFEc0I7QUFFOUJPLDZCQUFhO0FBRmlCLGFBQWxDO0FBSUg7QUFDSixLQVBEOztBQVNBakMsTUFBRW1CLE1BQUYsRUFBVVksSUFBVixDQUFlLG9CQUFmLEVBQXFDLFlBQVk7QUFDN0MsWUFBSSxDQUFDL0IsRUFBRSxNQUFGLEVBQVU4QixRQUFWLENBQW1CLFlBQW5CLENBQUwsRUFBdUM7QUFDbkNOO0FBQ0g7QUFDSixLQUpEOztBQU1BO0FBQ0F4QixNQUFFLHFCQUFGLEVBQXlCa0MsVUFBekIsQ0FBb0M7QUFDaENSLGdCQUFRO0FBRHdCLEtBQXBDO0FBR0gsQ0FyR0Q7O0FBd0dBO0FBQ0ExQixFQUFFbUIsTUFBRixFQUFVWSxJQUFWLENBQWUsUUFBZixFQUF5QixZQUFZO0FBQ2pDLFFBQUkvQixFQUFFLElBQUYsRUFBUUcsS0FBUixLQUFrQixHQUF0QixFQUEyQjtBQUN2QkgsVUFBRSxNQUFGLEVBQVVJLFFBQVYsQ0FBbUIsWUFBbkI7QUFDSCxLQUZELE1BRU87QUFDSEosVUFBRSxNQUFGLEVBQVVLLFdBQVYsQ0FBc0IsWUFBdEI7QUFDSDtBQUNKLENBTkQ7O0FBUUEsU0FBU2tCLFlBQVQsR0FBd0I7QUFDcEIsUUFBSSxDQUFDdkIsRUFBRSxNQUFGLEVBQVU4QixRQUFWLENBQW1CLGFBQW5CLENBQUQsSUFBc0M5QixFQUFFLE1BQUYsRUFBVThCLFFBQVYsQ0FBbUIsWUFBbkIsQ0FBMUMsRUFBNEU7QUFDeEU7QUFDQTlCLFVBQUUsWUFBRixFQUFnQm1DLElBQWhCO0FBQ0E7QUFDQW5CLG1CQUNJLFlBQVk7QUFDUmhCLGNBQUUsWUFBRixFQUFnQm9DLE1BQWhCLENBQXVCLEdBQXZCO0FBQ0gsU0FITCxFQUdPLEdBSFA7QUFJSCxLQVJELE1BUU8sSUFBSXBDLEVBQUUsTUFBRixFQUFVOEIsUUFBVixDQUFtQixlQUFuQixDQUFKLEVBQXlDO0FBQzVDOUIsVUFBRSxZQUFGLEVBQWdCbUMsSUFBaEI7QUFDQW5CLG1CQUNJLFlBQVk7QUFDUmhCLGNBQUUsWUFBRixFQUFnQm9DLE1BQWhCLENBQXVCLEdBQXZCO0FBQ0gsU0FITCxFQUdPLEdBSFA7QUFJSCxLQU5NLE1BTUE7QUFDSDtBQUNBcEMsVUFBRSxZQUFGLEVBQWdCcUMsVUFBaEIsQ0FBMkIsT0FBM0I7QUFDSDtBQUNKLEMiLCJmaWxlIjoianMvaW5zcGluaWEuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDI1OCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDEiLCIvKlxyXG4gKlxyXG4gKiAgIElOU1BJTklBIC0gUmVzcG9uc2l2ZSBBZG1pbiBUaGVtZVxyXG4gKiAgIHZlcnNpb24gMi43LjFcclxuICpcclxuICovXHJcblxyXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcblxyXG5cclxuICAgIC8vIEFkZCBib2R5LXNtYWxsIGNsYXNzIGlmIHdpbmRvdyBsZXNzIHRoYW4gNzY4cHhcclxuICAgIGlmICgkKHRoaXMpLndpZHRoKCkgPCA3NjkpIHtcclxuICAgICAgICAkKCdib2R5JykuYWRkQ2xhc3MoJ2JvZHktc21hbGwnKVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICAkKCdib2R5JykucmVtb3ZlQ2xhc3MoJ2JvZHktc21hbGwnKVxyXG4gICAgfVxyXG5cclxuICAgIC8vIE1ldHNpTWVudVxyXG4gICAgJCgnI3NpZGUtbWVudScpLm1ldGlzTWVudSgpO1xyXG5cclxuICAgIC8vIENvbGxhcHNlIGlib3ggZnVuY3Rpb25cclxuICAgICQoJy5jb2xsYXBzZS1saW5rJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBpYm94ID0gJCh0aGlzKS5jbG9zZXN0KCdkaXYuaWJveCcpO1xyXG4gICAgICAgIHZhciBidXR0b24gPSAkKHRoaXMpLmZpbmQoJ2knKTtcclxuICAgICAgICB2YXIgY29udGVudCA9IGlib3guY2hpbGRyZW4oJy5pYm94LWNvbnRlbnQnKTtcclxuICAgICAgICBjb250ZW50LnNsaWRlVG9nZ2xlKDIwMCk7XHJcbiAgICAgICAgYnV0dG9uLnRvZ2dsZUNsYXNzKCdmYS1jaGV2cm9uLXVwJykudG9nZ2xlQ2xhc3MoJ2ZhLWNoZXZyb24tZG93bicpO1xyXG4gICAgICAgIGlib3gudG9nZ2xlQ2xhc3MoJycpLnRvZ2dsZUNsYXNzKCdib3JkZXItYm90dG9tJyk7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlib3gucmVzaXplKCk7XHJcbiAgICAgICAgICAgIGlib3guZmluZCgnW2lkXj1tYXAtXScpLnJlc2l6ZSgpO1xyXG4gICAgICAgIH0sIDUwKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIENsb3NlIGlib3ggZnVuY3Rpb25cclxuICAgICQoJy5jbG9zZS1saW5rJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBjb250ZW50ID0gJCh0aGlzKS5jbG9zZXN0KCdkaXYuaWJveCcpO1xyXG4gICAgICAgIGNvbnRlbnQucmVtb3ZlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBGdWxsc2NyZWVuIGlib3ggZnVuY3Rpb25cclxuICAgICQoJy5mdWxsc2NyZWVuLWxpbmsnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGlib3ggPSAkKHRoaXMpLmNsb3Nlc3QoJ2Rpdi5pYm94Jyk7XHJcbiAgICAgICAgdmFyIGJ1dHRvbiA9ICQodGhpcykuZmluZCgnaScpO1xyXG4gICAgICAgICQoJ2JvZHknKS50b2dnbGVDbGFzcygnZnVsbHNjcmVlbi1pYm94LW1vZGUnKTtcclxuICAgICAgICBidXR0b24udG9nZ2xlQ2xhc3MoJ2ZhLWV4cGFuZCcpLnRvZ2dsZUNsYXNzKCdmYS1jb21wcmVzcycpO1xyXG4gICAgICAgIGlib3gudG9nZ2xlQ2xhc3MoJ2Z1bGxzY3JlZW4nKTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJCh3aW5kb3cpLnRyaWdnZXIoJ3Jlc2l6ZScpO1xyXG4gICAgICAgIH0sIDEwMCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBNaW5pbWFsaXplIG1lbnVcclxuICAgICQoJy5uYXZiYXItbWluaW1hbGl6ZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgJChcImJvZHlcIikudG9nZ2xlQ2xhc3MoXCJtaW5pLW5hdmJhclwiKTtcclxuICAgICAgICBTbW9vdGhseU1lbnUoKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBGdWxsIGhlaWdodCBvZiBzaWRlYmFyXHJcbiAgICBmdW5jdGlvbiBmaXhfaGVpZ2h0KCkge1xyXG4gICAgICAgIHZhciBoZWlnaHRXaXRob3V0TmF2YmFyID0gJChcImJvZHkgPiAjd3JhcHBlclwiKS5oZWlnaHQoKSAtIDYxO1xyXG4gICAgICAgICQoXCIuc2lkZWJhcmQtcGFuZWxcIikuY3NzKFwibWluLWhlaWdodFwiLCBoZWlnaHRXaXRob3V0TmF2YmFyICsgXCJweFwiKTtcclxuXHJcbiAgICAgICAgdmFyIG5hdmJhckhlaWdodCA9ICQoJ25hdi5uYXZiYXItZGVmYXVsdCcpLmhlaWdodCgpO1xyXG4gICAgICAgIHZhciB3cmFwcGVySGVpZ2h0ID0gJCgnI3BhZ2Utd3JhcHBlcicpLmhlaWdodCgpO1xyXG5cclxuICAgICAgICBpZiAobmF2YmFySGVpZ2h0ID4gd3JhcHBlckhlaWdodCkge1xyXG4gICAgICAgICAgICAkKCcjcGFnZS13cmFwcGVyJykuY3NzKFwibWluLWhlaWdodFwiLCBuYXZiYXJIZWlnaHQgKyBcInB4XCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG5hdmJhckhlaWdodCA8IHdyYXBwZXJIZWlnaHQpIHtcclxuICAgICAgICAgICAgJCgnI3BhZ2Utd3JhcHBlcicpLmNzcyhcIm1pbi1oZWlnaHRcIiwgJCh3aW5kb3cpLmhlaWdodCgpICsgXCJweFwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICgkKCdib2R5JykuaGFzQ2xhc3MoJ2ZpeGVkLW5hdicpKSB7XHJcbiAgICAgICAgICAgIGlmIChuYXZiYXJIZWlnaHQgPiB3cmFwcGVySGVpZ2h0KSB7XHJcbiAgICAgICAgICAgICAgICAkKCcjcGFnZS13cmFwcGVyJykuY3NzKFwibWluLWhlaWdodFwiLCBuYXZiYXJIZWlnaHQgKyBcInB4XCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgJCgnI3BhZ2Utd3JhcHBlcicpLmNzcyhcIm1pbi1oZWlnaHRcIiwgJCh3aW5kb3cpLmhlaWdodCgpIC0gNjAgKyBcInB4XCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBmaXhfaGVpZ2h0KCk7XHJcblxyXG4gICAgLy8gRml4ZWQgU2lkZWJhclxyXG4gICAgJCh3aW5kb3cpLmJpbmQoXCJsb2FkXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoJChcImJvZHlcIikuaGFzQ2xhc3MoJ2ZpeGVkLXNpZGViYXInKSkge1xyXG4gICAgICAgICAgICAkKCcuc2lkZWJhci1jb2xsYXBzZScpLnNsaW1TY3JvbGwoe1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAnMTAwJScsXHJcbiAgICAgICAgICAgICAgICByYWlsT3BhY2l0eTogMC45XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgICQod2luZG93KS5iaW5kKFwibG9hZCByZXNpemUgc2Nyb2xsXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoISQoXCJib2R5XCIpLmhhc0NsYXNzKCdib2R5LXNtYWxsJykpIHtcclxuICAgICAgICAgICAgZml4X2hlaWdodCgpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIC8vIEFkZCBzbGltc2Nyb2xsIHRvIGVsZW1lbnRcclxuICAgICQoJy5mdWxsLWhlaWdodC1zY3JvbGwnKS5zbGltc2Nyb2xsKHtcclxuICAgICAgICBoZWlnaHQ6ICcxMDAlJ1xyXG4gICAgfSlcclxufSk7XHJcblxyXG5cclxuLy8gTWluaW1hbGl6ZSBtZW51IHdoZW4gc2NyZWVuIGlzIGxlc3MgdGhhbiA3NjhweFxyXG4kKHdpbmRvdykuYmluZChcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAoJCh0aGlzKS53aWR0aCgpIDwgNzY5KSB7XHJcbiAgICAgICAgJCgnYm9keScpLmFkZENsYXNzKCdib2R5LXNtYWxsJylcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgJCgnYm9keScpLnJlbW92ZUNsYXNzKCdib2R5LXNtYWxsJylcclxuICAgIH1cclxufSk7XHJcblxyXG5mdW5jdGlvbiBTbW9vdGhseU1lbnUoKSB7XHJcbiAgICBpZiAoISQoJ2JvZHknKS5oYXNDbGFzcygnbWluaS1uYXZiYXInKSB8fCAkKCdib2R5JykuaGFzQ2xhc3MoJ2JvZHktc21hbGwnKSkge1xyXG4gICAgICAgIC8vIEhpZGUgbWVudSBpbiBvcmRlciB0byBzbW9vdGhseSB0dXJuIG9uIHdoZW4gbWF4aW1pemUgbWVudVxyXG4gICAgICAgICQoJyNzaWRlLW1lbnUnKS5oaWRlKCk7XHJcbiAgICAgICAgLy8gRm9yIHNtb290aGx5IHR1cm4gb24gbWVudVxyXG4gICAgICAgIHNldFRpbWVvdXQoXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICQoJyNzaWRlLW1lbnUnKS5mYWRlSW4oNDAwKTtcclxuICAgICAgICAgICAgfSwgMjAwKTtcclxuICAgIH0gZWxzZSBpZiAoJCgnYm9keScpLmhhc0NsYXNzKCdmaXhlZC1zaWRlYmFyJykpIHtcclxuICAgICAgICAkKCcjc2lkZS1tZW51JykuaGlkZSgpO1xyXG4gICAgICAgIHNldFRpbWVvdXQoXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICQoJyNzaWRlLW1lbnUnKS5mYWRlSW4oNDAwKTtcclxuICAgICAgICAgICAgfSwgMTAwKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gUmVtb3ZlIGFsbCBpbmxpbmUgc3R5bGUgZnJvbSBqcXVlcnkgZmFkZUluIGZ1bmN0aW9uIHRvIHJlc2V0IG1lbnUgc3RhdGVcclxuICAgICAgICAkKCcjc2lkZS1tZW51JykucmVtb3ZlQXR0cignc3R5bGUnKTtcclxuICAgIH1cclxufVxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9pbnNwaW5pYS5qcyJdLCJzb3VyY2VSb290IjoiIn0=