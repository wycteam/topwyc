/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 264);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 142:
/***/ (function(module, exports, __webpack_require__) {

var pendocs = null;

var vm = new Vue({
    el: '#documents-manager',
    data: {
        countlist: [],
        showlist: [],
        selected: {},
        activeStat: "all",
        validity: new Form({
            'expires_at': "",
            'status': "",
            'id': "",
            'month': "",
            'day': ""
        }, { baseURL: '/documents/update' }),
        denied: new Form({
            'reason': "",
            'status': "",
            'id': ""
        }, { baseURL: '/documents/update' }),
        activeId: {},

        baseZoom: 1,
        baseHeight: 0,
        baseWidth: 0,

        disableBtns: false
    },
    components: {
        'documentlist': __webpack_require__(215)
    },

    watch: {
        baseZoom: function baseZoom(newValue) {
            var changedHeightSize = this.baseHeight * newValue;
            var changedWidthSize = this.baseWidth * newValue;
            $('#doc-container').find('img').css('height', changedHeightSize);
            $('#doc-container').find('img').css('width', changedWidthSize);
        }
    },

    created: function created() {
        var _this = this;

        axios.get('documents/get-pending').then(function (result) {
            _this.updateDocumentLists(result.data, "pending");
        });
    },
    updated: function updated() {
        // pendocs =  $('#documentslist').dataTable( {
        //     paging: true,
        //     searching: true,
        //     order: [[ 0, "desc" ]]
        // });
    },

    directives: {
        datepicker: {
            bind: function bind(el, binding, vnode) {
                $(el).datepicker({
                    format: 'yyyy-mm-dd'
                }).on('changeDate', function (e) {
                    vm.$set(vm.validity, 'expires_at', e.format('yyyy-mm-dd'));
                });
            }
        }
    },

    mounted: function mounted() {
        var _this2 = this;

        $('#viewDocument').on('shown.bs.modal,show.bs.modal', function () {
            _this2.baseZoom = 1;

            img = $('<img id="shittyimage" class="center-block" src="/images/documents/' + _this2.selected.documentable_id + '/' + _this2.selected.name + '">').on('load', function () {
                var $modalImg = $('<img src="/images/documents/' + _this2.selected.documentable_id + '/' + _this2.selected.name + '" class="center-block" >');
                // $modalImg.appendTo('#doc-container');
                $('#doc-container').html($modalImg);
                _this2.baseHeight = $modalImg.height();
                _this2.baseWidth = $modalImg.width();

                $modalImg.draggable();
            });
        });
    },


    ready: function ready() {
        this.$nextTick(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="tooltip"]').tooltip('fixTitle');
        });
    },

    methods: {

        changeView: function changeView(status) {
            var _this3 = this;

            this.disableBtns = true;

            if (status == 'all') {
                axios.get('documents/get-documents').then(function (result) {
                    _this3.updateDocumentLists(result.data, status);
                });
            } else if (status == 'pending') {

                axios.get('documents/get-pending').then(function (result) {
                    _this3.updateDocumentLists(result.data, status);
                });
            } else if (status == 'verified') {

                axios.get('documents/get-approved').then(function (result) {
                    _this3.updateDocumentLists(result.data, status);
                });
            } else if (status == 'denied') {

                axios.get('documents/get-declined').then(function (result) {
                    _this3.updateDocumentLists(result.data, status);
                });
            }
        },

        updateDocumentLists: function updateDocumentLists(data, activeBtn) {
            this.showlist = data;
            this.activeStat = activeBtn;

            this.disableBtns = false;
            $('#documentslist').DataTable().destroy();
            setTimeout(function () {
                pendocs = $('#documentslist').dataTable({
                    paging: true,
                    searching: true,
                    order: [[0, "desc"]]
                });
            }, 200);
        },
        countDocs: function countDocs() {
            var _this4 = this;

            axios.get('documents/get-count').then(function (result) {
                _this4.countlist = result.data;
            });
        },
        initDocs: function initDocs() {
            var _this5 = this;

            axios.get('documents/get-documents').then(function (result) {
                _this5.showlist = result.data;
            });
        },
        saveValidity: function saveValidity(id, dateSel) {
            var _this6 = this;

            $(".verify").popover('hide');
            this.validity.id = id;
            this.validity.status = "Verified";
            this.validity.expires_at = dateSel;
            var month = parseInt(dateSel.substr(5, 2));
            var day = parseInt(dateSel.substr(8, 2));
            $('#documentslist').DataTable().destroy();

            if (month != 0) {
                if (day != 0) {
                    this.validity.submit('post', '/verify').then(function (result) {
                        var stat = _this6.activeStat;
                        if (stat == "all") {
                            axios.get('documents/get-documents').then(function (result) {
                                _this6.updateDocumentLists(result.data, stat);
                            });
                        } else if (stat == "pending") {
                            axios.get('documents/get-pending').then(function (result) {
                                _this6.updateDocumentLists(result.data, stat);
                            });
                        } else if (stat == "verified") {
                            axios.get('documents/get-approved').then(function (result) {
                                _this6.updateDocumentLists(result.data, stat);
                            });
                        } else if (stat == "denied") {
                            axios.get('documents/get-declined').then(function (result) {
                                _this6.updateDocumentLists(result.data, stat);
                            });
                        }
                        toastr.success('Successfully saved.');
                    }).catch(function (error) {
                        if (error.expires_at) {
                            $('.popover #validset').addClass('has-error');
                            toastr.error("Please use valid date of expiry.");
                        } else {
                            toastr.error('Update failed.');
                        }
                        $("#ver" + id).popover('show');
                        pendocs = $('#documentslist').dataTable({
                            paging: true,
                            searching: true,
                            order: [[0, "desc"]]
                        });
                    });
                } else {
                    toastr.error('Please use valid expiration date');
                }
            } else {
                toastr.error('Please use valid expiration date');
            }
            $('#documentslist').DataTable().destroy();
        },
        saveReason: function saveReason(id, reason) {
            var _this7 = this;

            $(".deny").popover('hide');
            this.denied.id = id;
            this.denied.status = "Denied";
            this.denied.reason = reason;
            this.denied.submit('post', '/deny').then(function (result) {
                var stat = _this7.activeStat;
                if (stat == "all") {
                    axios.get('documents/get-documents').then(function (result) {
                        _this7.updateDocumentLists(result.data, stat);
                    });
                } else if (stat == "pending") {
                    axios.get('documents/get-pending').then(function (result) {
                        _this7.updateDocumentLists(result.data, stat);
                    });
                } else if (stat == "verified") {
                    axios.get('documents/get-approved').then(function (result) {
                        _this7.updateDocumentLists(result.data, stat);
                    });
                } else if (stat == "denied") {
                    axios.get('documents/get-declined').then(function (result) {
                        _this7.updateDocumentLists(result.data, stat);
                    });
                }
                toastr.success('Successfully saved.');
            }).catch(function (error) {
                if (error.reason) {
                    $('.popover #reasonset').addClass('has-error');
                    toastr.error("Please indicate reason to deny.");
                } else {
                    toastr.error('Update failed.');
                }
                $("#den" + id).popover('show');
            });
        }
    }
});

$(document).ready(function () {

    $('.tile'
    // tile mouse actions
    ).on('mouseover', function () {
        $(this).children('.photo').css({ 'transform': 'scale(' + $(this).attr('data-scale') + ')' });
    }).on('mouseout', function () {
        $(this).children('.photo').css({ 'transform': 'scale(1)' });
    }).on('mousemove', function (e) {
        $(this).children('.photo').css({
            'transform-origin': (e.pageX - $(this).offset().left) / $(this).width() * 100 + '% ' + (e.pageY - $(this).offset().top) / $(this).height() * 100 + '%'
        });
    });
});
$(document).on("click", ".save-verification", function () {
    var id = vm.activeId;
    var dateSel = $('.popover #expires_at').val();
    vm.saveValidity(id, dateSel);
});

$(document).on("click", ".save-denied", function () {
    var id = vm.activeId;
    var reason = $('.popover #reason').val();
    vm.saveReason(id, reason);
});

$(document).on('click', function (e) {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false; // fix for BS 3.3.6
        }
    });
});

$(document).on('focus.autoExpand', 'textarea.autoExpand', function () {
    var savedValue = this.value;
    this.value = '';
    this.baseScrollHeight = this.scrollHeight;
    this.value = savedValue;
}).on('input.autoExpand', 'textarea.autoExpand', function () {
    var minRows = this.getAttribute('data-min-rows') | 0,
        rows;
    this.rows = minRows;
    rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 17);
    this.rows = minRows + rows;
});

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['docs'],
	methods: {
		viewModal: function viewModal(id) {
			var _this = this;

			$('#viewImg').attr('src', '');
			axios.get('documents/get-details/' + id).then(function (result) {
				_this.$parent.selected = result.data;
				$('#viewDocument').modal('toggle');
			});
		},
		updateStatus: function updateStatus(id) {
			this.$parent.activeId = id;
		}
	},
	mounted: function mounted() {
		$(".verify").popover({
			html: true,
			content: function content() {
				// setTimeout(function () {
				// 	$(this.nextSibling).find('input').datepicker();
				// }.bind(this), 1);
				return $('#frmValidUntil').html();
			}
		});

		$(".deny").popover({
			html: true,
			content: $('#frmReason').html()
		});
	}
});

/***/ }),

/***/ 215:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(179),
  /* template */
  __webpack_require__(235),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\cpanel\\components\\Documents\\List.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] List.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-47b0761a", Component.options)
  } else {
    hotAPI.reload("data-v-47b0761a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 235:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('tr', [_c('td', [_vm._v(_vm._s(_vm.docs.id))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.docs.uploader.first_name) + " " + _vm._s(_vm.docs.uploader.last_name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.docs.type))]), _vm._v(" "), _c('td', [_c('span', {
    staticClass: "label"
  }, [_vm._v(_vm._s(_vm.docs.status))])]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.docs.created_at))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.docs.expires_at))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm.docs.reason))]), _vm._v(" "), _c('td', {
    staticStyle: {
      "text-align": "center"
    }
  }, [_c('a', {
    staticClass: "btn btn-success btn-outline btn-sm",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        _vm.viewModal(_vm.docs.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-eye"
  })]), _vm._v(" "), (_vm.docs.status == 'Pending') ? _c('a', {
    staticClass: "btn btn-warning btn-outline btn-sm verify",
    attrs: {
      "href": "javascript:void(0)",
      "id": 'ver' + _vm.docs.id,
      "data-placement": "bottom"
    },
    on: {
      "click": function($event) {
        _vm.updateStatus(_vm.docs.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-thumbs-up"
  })]) : _vm._e(), _vm._v(" "), (_vm.docs.status == 'Pending') ? _c('a', {
    staticClass: "btn btn-danger btn-outline btn-sm deny",
    attrs: {
      "href": "javascript:void(0)",
      "id": 'den' + _vm.docs.id,
      "data-placement": "bottom"
    },
    on: {
      "click": function($event) {
        _vm.updateStatus(_vm.docs.id)
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-thumbs-down"
  })]) : _vm._e()])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-47b0761a", module.exports)
  }
}

/***/ }),

/***/ 264:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(142);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDE/NTk0YSoqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NwYW5lbC9wYWdlcy9kb2N1bWVudHMuanMiLCJ3ZWJwYWNrOi8vL0xpc3QudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvRG9jdW1lbnRzL0xpc3QudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvRG9jdW1lbnRzL0xpc3QudnVlPzBlZTQiXSwibmFtZXMiOlsicGVuZG9jcyIsInZtIiwiVnVlIiwiZWwiLCJkYXRhIiwiY291bnRsaXN0Iiwic2hvd2xpc3QiLCJzZWxlY3RlZCIsImFjdGl2ZVN0YXQiLCJ2YWxpZGl0eSIsIkZvcm0iLCJiYXNlVVJMIiwiZGVuaWVkIiwiYWN0aXZlSWQiLCJiYXNlWm9vbSIsImJhc2VIZWlnaHQiLCJiYXNlV2lkdGgiLCJkaXNhYmxlQnRucyIsImNvbXBvbmVudHMiLCJyZXF1aXJlIiwid2F0Y2giLCJuZXdWYWx1ZSIsImNoYW5nZWRIZWlnaHRTaXplIiwiY2hhbmdlZFdpZHRoU2l6ZSIsIiQiLCJmaW5kIiwiY3NzIiwiY3JlYXRlZCIsImF4aW9zIiwiZ2V0IiwidGhlbiIsInVwZGF0ZURvY3VtZW50TGlzdHMiLCJyZXN1bHQiLCJ1cGRhdGVkIiwiZGlyZWN0aXZlcyIsImRhdGVwaWNrZXIiLCJiaW5kIiwiYmluZGluZyIsInZub2RlIiwiZm9ybWF0Iiwib24iLCJlIiwiJHNldCIsIm1vdW50ZWQiLCJpbWciLCJkb2N1bWVudGFibGVfaWQiLCJuYW1lIiwiJG1vZGFsSW1nIiwiaHRtbCIsImhlaWdodCIsIndpZHRoIiwiZHJhZ2dhYmxlIiwicmVhZHkiLCIkbmV4dFRpY2siLCJ0b29sdGlwIiwibWV0aG9kcyIsImNoYW5nZVZpZXciLCJzdGF0dXMiLCJhY3RpdmVCdG4iLCJEYXRhVGFibGUiLCJkZXN0cm95Iiwic2V0VGltZW91dCIsImRhdGFUYWJsZSIsInBhZ2luZyIsInNlYXJjaGluZyIsIm9yZGVyIiwiY291bnREb2NzIiwiaW5pdERvY3MiLCJzYXZlVmFsaWRpdHkiLCJpZCIsImRhdGVTZWwiLCJwb3BvdmVyIiwiZXhwaXJlc19hdCIsIm1vbnRoIiwicGFyc2VJbnQiLCJzdWJzdHIiLCJkYXkiLCJzdWJtaXQiLCJzdGF0IiwidG9hc3RyIiwic3VjY2VzcyIsImNhdGNoIiwiZXJyb3IiLCJhZGRDbGFzcyIsInNhdmVSZWFzb24iLCJyZWFzb24iLCJkb2N1bWVudCIsImNoaWxkcmVuIiwiYXR0ciIsInBhZ2VYIiwib2Zmc2V0IiwibGVmdCIsInBhZ2VZIiwidG9wIiwidmFsIiwiZWFjaCIsImlzIiwidGFyZ2V0IiwiaGFzIiwibGVuZ3RoIiwiaW5TdGF0ZSIsImNsaWNrIiwic2F2ZWRWYWx1ZSIsInZhbHVlIiwiYmFzZVNjcm9sbEhlaWdodCIsInNjcm9sbEhlaWdodCIsIm1pblJvd3MiLCJnZXRBdHRyaWJ1dGUiLCJyb3dzIiwiTWF0aCIsImNlaWwiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQSxJQUFJQSxVQUFVLElBQWQ7O0FBRUEsSUFBSUMsS0FBSyxJQUFJQyxHQUFKLENBQVM7QUFDZEMsUUFBRyxvQkFEVztBQUVkQyxVQUFLO0FBQ0RDLG1CQUFVLEVBRFQ7QUFFREMsa0JBQVMsRUFGUjtBQUdEQyxrQkFBUyxFQUhSO0FBSURDLG9CQUFXLEtBSlY7QUFLREMsa0JBQVUsSUFBSUMsSUFBSixDQUFTO0FBQ2YsMEJBQWUsRUFEQTtBQUVmLHNCQUFTLEVBRk07QUFHZixrQkFBSyxFQUhVO0FBSWYscUJBQVEsRUFKTztBQUtmLG1CQUFNO0FBTFMsU0FBVCxFQU1SLEVBQUVDLFNBQVcsbUJBQWIsRUFOUSxDQUxUO0FBWURDLGdCQUFRLElBQUlGLElBQUosQ0FBUztBQUNiLHNCQUFVLEVBREc7QUFFYixzQkFBVSxFQUZHO0FBR2Isa0JBQUs7QUFIUSxTQUFULEVBSU4sRUFBRUMsU0FBVyxtQkFBYixFQUpNLENBWlA7QUFpQkRFLGtCQUFTLEVBakJSOztBQW1CREMsa0JBQVMsQ0FuQlI7QUFvQkRDLG9CQUFXLENBcEJWO0FBcUJEQyxtQkFBVSxDQXJCVDs7QUF1QkRDLHFCQUFZO0FBdkJYLEtBRlM7QUEyQmRDLGdCQUFZO0FBQ1Isd0JBQWdCLG1CQUFBQyxDQUFRLEdBQVI7QUFEUixLQTNCRTs7QUErQmJDLFdBQU07QUFDSE4sa0JBQVMsa0JBQVNPLFFBQVQsRUFBa0I7QUFDekIsZ0JBQUlDLG9CQUFxQixLQUFLUCxVQUFMLEdBQWtCTSxRQUEzQztBQUNBLGdCQUFJRSxtQkFBbUIsS0FBS1AsU0FBTCxHQUFpQkssUUFBeEM7QUFDQUcsY0FBRSxnQkFBRixFQUFvQkMsSUFBcEIsQ0FBeUIsS0FBekIsRUFBZ0NDLEdBQWhDLENBQW9DLFFBQXBDLEVBQTZDSixpQkFBN0M7QUFDQUUsY0FBRSxnQkFBRixFQUFvQkMsSUFBcEIsQ0FBeUIsS0FBekIsRUFBZ0NDLEdBQWhDLENBQW9DLE9BQXBDLEVBQTRDSCxnQkFBNUM7QUFDRDtBQU5FLEtBL0JPOztBQXdDZEksV0F4Q2MscUJBd0NMO0FBQUE7O0FBQ0pDLGNBQ0lDLEdBREosQ0FDUSx1QkFEUixFQUVJQyxJQUZKLENBRVMsa0JBQVM7QUFDWCxrQkFBS0MsbUJBQUwsQ0FBeUJDLE9BQU81QixJQUFoQyxFQUFxQyxTQUFyQztBQUNILFNBSko7QUFLSixLQTlDYTtBQStDZDZCLFdBL0NjLHFCQStDTDtBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSCxLQXJEYTs7QUFzRGRDLGdCQUFZO0FBQ1JDLG9CQUFZO0FBQ1JDLGdCQURRLGdCQUNIakMsRUFERyxFQUNDa0MsT0FERCxFQUNVQyxLQURWLEVBQ2lCO0FBQ3JCZCxrQkFBRXJCLEVBQUYsRUFBTWdDLFVBQU4sQ0FBaUI7QUFDYkksNEJBQVE7QUFESyxpQkFBakIsRUFFR0MsRUFGSCxDQUVNLFlBRk4sRUFFb0IsVUFBQ0MsQ0FBRCxFQUFPO0FBQ3ZCeEMsdUJBQUd5QyxJQUFILENBQVF6QyxHQUFHUSxRQUFYLEVBQXFCLFlBQXJCLEVBQW1DZ0MsRUFBRUYsTUFBRixDQUFTLFlBQVQsQ0FBbkM7QUFDSCxpQkFKRDtBQUtIO0FBUE87QUFESixLQXRERTs7QUFrRWRJLFdBbEVjLHFCQWtFTDtBQUFBOztBQUNKbkIsVUFBRSxlQUFGLEVBQW1CZ0IsRUFBbkIsQ0FBc0IsOEJBQXRCLEVBQXNELFlBQU07QUFDekQsbUJBQUsxQixRQUFMLEdBQWdCLENBQWhCOztBQUVBOEIsa0JBQU1wQixFQUFFLHVFQUFxRSxPQUFLakIsUUFBTCxDQUFjc0MsZUFBbkYsR0FBbUcsR0FBbkcsR0FBdUcsT0FBS3RDLFFBQUwsQ0FBY3VDLElBQXJILEdBQTBILElBQTVILEVBQ0ROLEVBREMsQ0FDRSxNQURGLEVBQ1UsWUFBTTtBQUNwQixvQkFBSU8sWUFBWXZCLEVBQUUsaUNBQStCLE9BQUtqQixRQUFMLENBQWNzQyxlQUE3QyxHQUE2RCxHQUE3RCxHQUFpRSxPQUFLdEMsUUFBTCxDQUFjdUMsSUFBL0UsR0FBb0YsMEJBQXRGLENBQWhCO0FBQ0E7QUFDQXRCLGtCQUFFLGdCQUFGLEVBQW9Cd0IsSUFBcEIsQ0FBeUJELFNBQXpCO0FBQ0EsdUJBQUtoQyxVQUFMLEdBQWtCZ0MsVUFBVUUsTUFBVixFQUFsQjtBQUNBLHVCQUFLakMsU0FBTCxHQUFpQitCLFVBQVVHLEtBQVYsRUFBakI7O0FBRUFILDBCQUFVSSxTQUFWO0FBRUQsYUFWSyxDQUFOO0FBYUgsU0FoQkE7QUFtQkosS0F0RmE7OztBQXdGZEMsV0FBTyxpQkFBVztBQUNkLGFBQUtDLFNBQUwsQ0FBZSxZQUFZO0FBQ3ZCN0IsY0FBRSx5QkFBRixFQUE2QjhCLE9BQTdCO0FBQ0E5QixjQUFFLHlCQUFGLEVBQTZCOEIsT0FBN0IsQ0FBcUMsVUFBckM7QUFDSCxTQUhEO0FBSUgsS0E3RmE7O0FBK0ZkQyxhQUFROztBQUVKQyxvQkFBVyxvQkFBU0MsTUFBVCxFQUFnQjtBQUFBOztBQUN2QixpQkFBS3hDLFdBQUwsR0FBbUIsSUFBbkI7O0FBRUEsZ0JBQUd3QyxVQUFXLEtBQWQsRUFBb0I7QUFDaEI3QixzQkFDS0MsR0FETCxDQUNTLHlCQURULEVBRUtDLElBRkwsQ0FFVSxrQkFBUztBQUNYLDJCQUFLQyxtQkFBTCxDQUF5QkMsT0FBTzVCLElBQWhDLEVBQXNDcUQsTUFBdEM7QUFDSCxpQkFKTDtBQU1ILGFBUEQsTUFPTSxJQUFHQSxVQUFVLFNBQWIsRUFBdUI7O0FBRXpCN0Isc0JBQ0tDLEdBREwsQ0FDUyx1QkFEVCxFQUVLQyxJQUZMLENBRVUsa0JBQVU7QUFDWiwyQkFBS0MsbUJBQUwsQ0FBeUJDLE9BQU81QixJQUFoQyxFQUFzQ3FELE1BQXRDO0FBQ0gsaUJBSkw7QUFNSCxhQVJLLE1BUUEsSUFBR0EsVUFBVSxVQUFiLEVBQXdCOztBQUUxQjdCLHNCQUNLQyxHQURMLENBQ1Msd0JBRFQsRUFFS0MsSUFGTCxDQUVVLGtCQUFTO0FBQ1gsMkJBQUtDLG1CQUFMLENBQXlCQyxPQUFPNUIsSUFBaEMsRUFBc0NxRCxNQUF0QztBQUNILGlCQUpMO0FBTUgsYUFSSyxNQVFBLElBQUdBLFVBQVUsUUFBYixFQUFzQjs7QUFFeEI3QixzQkFDS0MsR0FETCxDQUNTLHdCQURULEVBRUtDLElBRkwsQ0FFVSxrQkFBUztBQUNYLDJCQUFLQyxtQkFBTCxDQUF5QkMsT0FBTzVCLElBQWhDLEVBQXNDcUQsTUFBdEM7QUFDSCxpQkFKTDtBQUtIO0FBQ0osU0FwQ0c7O0FBc0NKMUIsNkJBQW9CLDZCQUFTM0IsSUFBVCxFQUFjc0QsU0FBZCxFQUF3QjtBQUN4QyxpQkFBS3BELFFBQUwsR0FBZ0JGLElBQWhCO0FBQ0EsaUJBQUtJLFVBQUwsR0FBa0JrRCxTQUFsQjs7QUFFQSxpQkFBS3pDLFdBQUwsR0FBbUIsS0FBbkI7QUFDQU8sY0FBRSxnQkFBRixFQUFvQm1DLFNBQXBCLEdBQWdDQyxPQUFoQztBQUNBQyx1QkFBVyxZQUFVO0FBQ2pCN0QsMEJBQVd3QixFQUFFLGdCQUFGLEVBQW9Cc0MsU0FBcEIsQ0FBK0I7QUFDdENDLDRCQUFRLElBRDhCO0FBRXRDQywrQkFBVyxJQUYyQjtBQUd0Q0MsMkJBQU8sQ0FBQyxDQUFFLENBQUYsRUFBSyxNQUFMLENBQUQ7QUFIK0IsaUJBQS9CLENBQVg7QUFLSCxhQU5ELEVBTUUsR0FORjtBQVFILFNBcERHO0FBcURKQyxpQkFyREksdUJBcURPO0FBQUE7O0FBQ1B0QyxrQkFBTUMsR0FBTixDQUFVLHFCQUFWLEVBQ0tDLElBREwsQ0FDVSxrQkFBUztBQUNmLHVCQUFLekIsU0FBTCxHQUFpQjJCLE9BQU81QixJQUF4QjtBQUNILGFBSEQ7QUFJSCxTQTFERztBQTJESitELGdCQTNESSxzQkEyRE07QUFBQTs7QUFDTnZDLGtCQUFNQyxHQUFOLENBQVUseUJBQVYsRUFDS0MsSUFETCxDQUNVLGtCQUFTO0FBQ2YsdUJBQUt4QixRQUFMLEdBQWdCMEIsT0FBTzVCLElBQXZCO0FBQ0gsYUFIRDtBQUlILFNBaEVHO0FBaUVKZ0Usb0JBakVJLHdCQWlFU0MsRUFqRVQsRUFpRWFDLE9BakViLEVBaUVxQjtBQUFBOztBQUNyQjlDLGNBQUUsU0FBRixFQUFhK0MsT0FBYixDQUFxQixNQUFyQjtBQUNBLGlCQUFLOUQsUUFBTCxDQUFjNEQsRUFBZCxHQUFtQkEsRUFBbkI7QUFDQSxpQkFBSzVELFFBQUwsQ0FBY2dELE1BQWQsR0FBdUIsVUFBdkI7QUFDQSxpQkFBS2hELFFBQUwsQ0FBYytELFVBQWQsR0FBMkJGLE9BQTNCO0FBQ0EsZ0JBQUlHLFFBQVFDLFNBQVNKLFFBQVFLLE1BQVIsQ0FBZSxDQUFmLEVBQWlCLENBQWpCLENBQVQsQ0FBWjtBQUNBLGdCQUFJQyxNQUFNRixTQUFTSixRQUFRSyxNQUFSLENBQWUsQ0FBZixFQUFpQixDQUFqQixDQUFULENBQVY7QUFDQ25ELGNBQUUsZ0JBQUYsRUFBb0JtQyxTQUFwQixHQUFnQ0MsT0FBaEM7O0FBRUQsZ0JBQUlhLFNBQVMsQ0FBYixFQUFlO0FBQ1osb0JBQUdHLE9BQU8sQ0FBVixFQUFZO0FBQ1AseUJBQUtuRSxRQUFMLENBQWNvRSxNQUFkLENBQXFCLE1BQXJCLEVBQTZCLFNBQTdCLEVBQ0MvQyxJQURELENBQ00sa0JBQVU7QUFDWiw0QkFBSWdELE9BQU8sT0FBS3RFLFVBQWhCO0FBQ0MsNEJBQUdzRSxRQUFRLEtBQVgsRUFBaUI7QUFDYmxELGtDQUFNQyxHQUFOLENBQVUseUJBQVYsRUFDQ0MsSUFERCxDQUNNLGtCQUFTO0FBQ1osdUNBQUtDLG1CQUFMLENBQXlCQyxPQUFPNUIsSUFBaEMsRUFBc0MwRSxJQUF0QztBQUNILDZCQUhBO0FBSUgseUJBTEQsTUFLTSxJQUFHQSxRQUFRLFNBQVgsRUFBcUI7QUFDdkJsRCxrQ0FBTUMsR0FBTixDQUFVLHVCQUFWLEVBQ0NDLElBREQsQ0FDTSxrQkFBUztBQUNaLHVDQUFLQyxtQkFBTCxDQUF5QkMsT0FBTzVCLElBQWhDLEVBQXNDMEUsSUFBdEM7QUFDSCw2QkFIQTtBQUlILHlCQUxLLE1BS0EsSUFBR0EsUUFBUSxVQUFYLEVBQXNCO0FBQ3hCbEQsa0NBQU1DLEdBQU4sQ0FBVSx3QkFBVixFQUNDQyxJQURELENBQ00sa0JBQVM7QUFDWix1Q0FBS0MsbUJBQUwsQ0FBeUJDLE9BQU81QixJQUFoQyxFQUFzQzBFLElBQXRDO0FBQ0gsNkJBSEE7QUFJSCx5QkFMSyxNQUtBLElBQUdBLFFBQVEsUUFBWCxFQUFvQjtBQUN2QmxELGtDQUFNQyxHQUFOLENBQVUsd0JBQVYsRUFDQ0MsSUFERCxDQUNNLGtCQUFTO0FBQ1gsdUNBQUtDLG1CQUFMLENBQXlCQyxPQUFPNUIsSUFBaEMsRUFBc0MwRSxJQUF0QztBQUNILDZCQUhEO0FBSUg7QUFDREMsK0JBQU9DLE9BQVAsQ0FBZSxxQkFBZjtBQUNILHFCQXpCRCxFQTBCQ0MsS0ExQkQsQ0EwQk8saUJBQVM7QUFDWCw0QkFBR0MsTUFBTVYsVUFBVCxFQUFvQjtBQUNqQmhELDhCQUFFLG9CQUFGLEVBQXdCMkQsUUFBeEIsQ0FBaUMsV0FBakM7QUFDQUosbUNBQU9HLEtBQVAsQ0FBYSxrQ0FBYjtBQUNILHlCQUhBLE1BR0k7QUFDRkgsbUNBQU9HLEtBQVAsQ0FBYSxnQkFBYjtBQUNGO0FBQ0QxRCwwQkFBRSxTQUFPNkMsRUFBVCxFQUFhRSxPQUFiLENBQXFCLE1BQXJCO0FBQ0N2RSxrQ0FBV3dCLEVBQUUsZ0JBQUYsRUFBb0JzQyxTQUFwQixDQUErQjtBQUN2Q0Msb0NBQVEsSUFEK0I7QUFFdkNDLHVDQUFXLElBRjRCO0FBR3ZDQyxtQ0FBTyxDQUFDLENBQUUsQ0FBRixFQUFLLE1BQUwsQ0FBRDtBQUhnQyx5QkFBL0IsQ0FBWDtBQUtKLHFCQXZDRDtBQXdDSixpQkF6Q0QsTUF5Q0s7QUFDQWMsMkJBQU9HLEtBQVAsQ0FBYSxrQ0FBYjtBQUNKO0FBQ0gsYUE3Q0QsTUE2Q0s7QUFDREgsdUJBQU9HLEtBQVAsQ0FBYSxrQ0FBYjtBQUNIO0FBQ0QxRCxjQUFFLGdCQUFGLEVBQW9CbUMsU0FBcEIsR0FBZ0NDLE9BQWhDO0FBQ0gsU0EzSEc7QUE0SEp3QixrQkE1SEksc0JBNEhPZixFQTVIUCxFQTRIV2dCLE1BNUhYLEVBNEhrQjtBQUFBOztBQUVsQjdELGNBQUUsT0FBRixFQUFXK0MsT0FBWCxDQUFtQixNQUFuQjtBQUNBLGlCQUFLM0QsTUFBTCxDQUFZeUQsRUFBWixHQUFpQkEsRUFBakI7QUFDQSxpQkFBS3pELE1BQUwsQ0FBWTZDLE1BQVosR0FBcUIsUUFBckI7QUFDQSxpQkFBSzdDLE1BQUwsQ0FBWXlFLE1BQVosR0FBcUJBLE1BQXJCO0FBQ0EsaUJBQUt6RSxNQUFMLENBQVlpRSxNQUFaLENBQW1CLE1BQW5CLEVBQTBCLE9BQTFCLEVBQ0MvQyxJQURELENBQ00sa0JBQVU7QUFDWixvQkFBSWdELE9BQU8sT0FBS3RFLFVBQWhCO0FBQ0Msb0JBQUdzRSxRQUFRLEtBQVgsRUFBaUI7QUFDYmxELDBCQUFNQyxHQUFOLENBQVUseUJBQVYsRUFDQ0MsSUFERCxDQUNNLGtCQUFTO0FBQ1osK0JBQUtDLG1CQUFMLENBQXlCQyxPQUFPNUIsSUFBaEMsRUFBc0MwRSxJQUF0QztBQUNILHFCQUhBO0FBSUgsaUJBTEQsTUFLTSxJQUFHQSxRQUFRLFNBQVgsRUFBcUI7QUFDdkJsRCwwQkFBTUMsR0FBTixDQUFVLHVCQUFWLEVBQ0NDLElBREQsQ0FDTSxrQkFBUztBQUNaLCtCQUFLQyxtQkFBTCxDQUF5QkMsT0FBTzVCLElBQWhDLEVBQXNDMEUsSUFBdEM7QUFDSCxxQkFIQTtBQUlILGlCQUxLLE1BS0EsSUFBR0EsUUFBUSxVQUFYLEVBQXNCO0FBQ3hCbEQsMEJBQU1DLEdBQU4sQ0FBVSx3QkFBVixFQUNDQyxJQURELENBQ00sa0JBQVM7QUFDWiwrQkFBS0MsbUJBQUwsQ0FBeUJDLE9BQU81QixJQUFoQyxFQUFzQzBFLElBQXRDO0FBQ0gscUJBSEE7QUFJSCxpQkFMSyxNQUtBLElBQUdBLFFBQVEsUUFBWCxFQUFvQjtBQUN2QmxELDBCQUFNQyxHQUFOLENBQVUsd0JBQVYsRUFDQ0MsSUFERCxDQUNNLGtCQUFTO0FBQ1gsK0JBQUtDLG1CQUFMLENBQXlCQyxPQUFPNUIsSUFBaEMsRUFBc0MwRSxJQUF0QztBQUNILHFCQUhEO0FBSUg7QUFDREMsdUJBQU9DLE9BQVAsQ0FBZSxxQkFBZjtBQUNILGFBekJELEVBMEJDQyxLQTFCRCxDQTBCTyxpQkFBUTtBQUNYLG9CQUFHQyxNQUFNRyxNQUFULEVBQWdCO0FBQ1o3RCxzQkFBRSxxQkFBRixFQUF5QjJELFFBQXpCLENBQWtDLFdBQWxDO0FBQ0FKLDJCQUFPRyxLQUFQLENBQWEsaUNBQWI7QUFDSCxpQkFIRCxNQUdLO0FBQ0ZILDJCQUFPRyxLQUFQLENBQWEsZ0JBQWI7QUFDRjtBQUNEMUQsa0JBQUUsU0FBTzZDLEVBQVQsRUFBYUUsT0FBYixDQUFxQixNQUFyQjtBQUNILGFBbENEO0FBbUNIO0FBcktHO0FBL0ZNLENBQVQsQ0FBVDs7QUF3UUEvQyxFQUFFOEQsUUFBRixFQUFZbEMsS0FBWixDQUFrQixZQUFVOztBQUV6QjVCLE1BQUU7QUFDRDtBQURELE1BRUVnQixFQUZGLENBRUssV0FGTCxFQUVrQixZQUFVO0FBQ3pCaEIsVUFBRSxJQUFGLEVBQVErRCxRQUFSLENBQWlCLFFBQWpCLEVBQTJCN0QsR0FBM0IsQ0FBK0IsRUFBQyxhQUFhLFdBQVVGLEVBQUUsSUFBRixFQUFRZ0UsSUFBUixDQUFhLFlBQWIsQ0FBVixHQUFzQyxHQUFwRCxFQUEvQjtBQUNELEtBSkYsRUFLRWhELEVBTEYsQ0FLSyxVQUxMLEVBS2lCLFlBQVU7QUFDeEJoQixVQUFFLElBQUYsRUFBUStELFFBQVIsQ0FBaUIsUUFBakIsRUFBMkI3RCxHQUEzQixDQUErQixFQUFDLGFBQWEsVUFBZCxFQUEvQjtBQUNELEtBUEYsRUFRRWMsRUFSRixDQVFLLFdBUkwsRUFRa0IsVUFBU0MsQ0FBVCxFQUFXO0FBQzFCakIsVUFBRSxJQUFGLEVBQVErRCxRQUFSLENBQWlCLFFBQWpCLEVBQTJCN0QsR0FBM0IsQ0FBK0I7QUFDM0IsZ0NBQXFCLENBQUNlLEVBQUVnRCxLQUFGLEdBQVVqRSxFQUFFLElBQUYsRUFBUWtFLE1BQVIsR0FBaUJDLElBQTVCLElBQW9DbkUsRUFBRSxJQUFGLEVBQVEwQixLQUFSLEVBQXJDLEdBQXdELEdBQXhELEdBQThELElBQTlELEdBQXNFLENBQUNULEVBQUVtRCxLQUFGLEdBQVVwRSxFQUFFLElBQUYsRUFBUWtFLE1BQVIsR0FBaUJHLEdBQTVCLElBQW1DckUsRUFBRSxJQUFGLEVBQVF5QixNQUFSLEVBQXBDLEdBQXdELEdBQTdILEdBQWtJO0FBRDNILFNBQS9CO0FBR0QsS0FaRjtBQWFGLENBZkQ7QUFnQkF6QixFQUFFOEQsUUFBRixFQUFZOUMsRUFBWixDQUFlLE9BQWYsRUFBd0Isb0JBQXhCLEVBQThDLFlBQVc7QUFDckQsUUFBSTZCLEtBQUtwRSxHQUFHWSxRQUFaO0FBQ0EsUUFBSXlELFVBQVU5QyxFQUFFLHNCQUFGLEVBQTBCc0UsR0FBMUIsRUFBZDtBQUNBN0YsT0FBR21FLFlBQUgsQ0FBZ0JDLEVBQWhCLEVBQW9CQyxPQUFwQjtBQUNILENBSkQ7O0FBTUE5QyxFQUFFOEQsUUFBRixFQUFZOUMsRUFBWixDQUFlLE9BQWYsRUFBd0IsY0FBeEIsRUFBd0MsWUFBVztBQUMvQyxRQUFJNkIsS0FBS3BFLEdBQUdZLFFBQVo7QUFDQSxRQUFJd0UsU0FBUzdELEVBQUUsa0JBQUYsRUFBc0JzRSxHQUF0QixFQUFiO0FBQ0E3RixPQUFHbUYsVUFBSCxDQUFjZixFQUFkLEVBQWtCZ0IsTUFBbEI7QUFDSCxDQUpEOztBQU1BN0QsRUFBRThELFFBQUYsRUFBWTlDLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFVBQVVDLENBQVYsRUFBYTtBQUNqQ2pCLE1BQUUsK0NBQUYsRUFBbUR1RSxJQUFuRCxDQUF3RCxZQUFZO0FBQ2hFLFlBQUksQ0FBQ3ZFLEVBQUUsSUFBRixFQUFRd0UsRUFBUixDQUFXdkQsRUFBRXdELE1BQWIsQ0FBRCxJQUF5QnpFLEVBQUUsSUFBRixFQUFRMEUsR0FBUixDQUFZekQsRUFBRXdELE1BQWQsRUFBc0JFLE1BQXRCLEtBQWlDLENBQTFELElBQStEM0UsRUFBRSxVQUFGLEVBQWMwRSxHQUFkLENBQWtCekQsRUFBRXdELE1BQXBCLEVBQTRCRSxNQUE1QixLQUF1QyxDQUExRyxFQUE2RztBQUN6RyxhQUFDLENBQUMzRSxFQUFFLElBQUYsRUFBUStDLE9BQVIsQ0FBZ0IsTUFBaEIsRUFBd0JuRSxJQUF4QixDQUE2QixZQUE3QixLQUE0QyxFQUE3QyxFQUFpRGdHLE9BQWpELElBQTBELEVBQTNELEVBQStEQyxLQUEvRCxHQUF1RSxLQUF2RSxDQUR5RyxDQUMzQjtBQUNqRjtBQUNKLEtBSkQ7QUFLSCxDQU5EOztBQVFBN0UsRUFBRThELFFBQUYsRUFBWTlDLEVBQVosQ0FBZSxrQkFBZixFQUFtQyxxQkFBbkMsRUFBMEQsWUFBVTtBQUM1RCxRQUFJOEQsYUFBYSxLQUFLQyxLQUF0QjtBQUNBLFNBQUtBLEtBQUwsR0FBYSxFQUFiO0FBQ0EsU0FBS0MsZ0JBQUwsR0FBd0IsS0FBS0MsWUFBN0I7QUFDQSxTQUFLRixLQUFMLEdBQWFELFVBQWI7QUFDSCxDQUxMLEVBTUs5RCxFQU5MLENBTVEsa0JBTlIsRUFNNEIscUJBTjVCLEVBTW1ELFlBQVU7QUFDckQsUUFBSWtFLFVBQVUsS0FBS0MsWUFBTCxDQUFrQixlQUFsQixJQUFtQyxDQUFqRDtBQUFBLFFBQW9EQyxJQUFwRDtBQUNBLFNBQUtBLElBQUwsR0FBWUYsT0FBWjtBQUNBRSxXQUFPQyxLQUFLQyxJQUFMLENBQVUsQ0FBQyxLQUFLTCxZQUFMLEdBQW9CLEtBQUtELGdCQUExQixJQUE4QyxFQUF4RCxDQUFQO0FBQ0EsU0FBS0ksSUFBTCxHQUFZRixVQUFVRSxJQUF0QjtBQUNILENBWEwsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxUkE7U0FFQTs7O0FBRUE7OzZCQUNBO3dDQUNBLDJCQUNBO29DQUNBOzZCQUNBO0FBRUE7QUFDQTswQ0FDQTsyQkFDQTtBQUdBO0FBZEE7NkJBZUE7SUFDQTtTQUVBOytCQUNBO0FBQ0E7QUFDQTtBQUNBOytCQUNBO0FBR0E7QUFUQTs7SUFVQTtTQUVBOzRCQUVBO0FBSEE7QUFNQTtBQXBDQSxHOzs7Ozs7O0FDckJBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9kb2N1bWVudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDI2NCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgYjIwYTJhMGIyOTU2YTlkNmMwZDEiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDUgNiA3IDggOSAxMCAxMSAxMiIsInZhciBwZW5kb2NzID0gbnVsbDtcclxuXHJcbnZhciB2bSA9IG5ldyBWdWUgKHtcclxuICAgIGVsOicjZG9jdW1lbnRzLW1hbmFnZXInLFxyXG4gICAgZGF0YTp7XHJcbiAgICAgICAgY291bnRsaXN0OltdLFxyXG4gICAgICAgIHNob3dsaXN0OltdLFxyXG4gICAgICAgIHNlbGVjdGVkOnt9LFxyXG4gICAgICAgIGFjdGl2ZVN0YXQ6XCJhbGxcIixcclxuICAgICAgICB2YWxpZGl0eTogbmV3IEZvcm0oe1xyXG4gICAgICAgICAgICAnZXhwaXJlc19hdCcgOiBcIlwiLFxyXG4gICAgICAgICAgICAnc3RhdHVzJzpcIlwiLFxyXG4gICAgICAgICAgICAnaWQnOlwiXCIsXHJcbiAgICAgICAgICAgICdtb250aCc6XCJcIixcclxuICAgICAgICAgICAgJ2RheSc6XCJcIlxyXG4gICAgICAgIH0seyBiYXNlVVJMICA6ICcvZG9jdW1lbnRzL3VwZGF0ZSd9KSxcclxuICAgICAgICBkZW5pZWQ6IG5ldyBGb3JtKHtcclxuICAgICAgICAgICAgJ3JlYXNvbic6IFwiXCIsXHJcbiAgICAgICAgICAgICdzdGF0dXMnOiBcIlwiLFxyXG4gICAgICAgICAgICAnaWQnOlwiXCJcclxuICAgICAgICB9LHsgYmFzZVVSTCAgOiAnL2RvY3VtZW50cy91cGRhdGUnfSksXHJcbiAgICAgICAgYWN0aXZlSWQ6e30sXHJcblxyXG4gICAgICAgIGJhc2Vab29tOjEsXHJcbiAgICAgICAgYmFzZUhlaWdodDowLFxyXG4gICAgICAgIGJhc2VXaWR0aDowLFxyXG5cclxuICAgICAgICBkaXNhYmxlQnRuczpmYWxzZVxyXG4gICAgfSxcclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgICAnZG9jdW1lbnRsaXN0JzogcmVxdWlyZSgnLi4vY29tcG9uZW50cy9Eb2N1bWVudHMvTGlzdC52dWUnKVxyXG4gICAgfSxcclxuXHJcbiAgICAgd2F0Y2g6e1xyXG4gICAgICAgIGJhc2Vab29tOmZ1bmN0aW9uKG5ld1ZhbHVlKXtcclxuICAgICAgICAgIHZhciBjaGFuZ2VkSGVpZ2h0U2l6ZSA9ICB0aGlzLmJhc2VIZWlnaHQgKiBuZXdWYWx1ZTtcclxuICAgICAgICAgIHZhciBjaGFuZ2VkV2lkdGhTaXplID0gdGhpcy5iYXNlV2lkdGggKiBuZXdWYWx1ZTtcclxuICAgICAgICAgICQoJyNkb2MtY29udGFpbmVyJykuZmluZCgnaW1nJykuY3NzKCdoZWlnaHQnLGNoYW5nZWRIZWlnaHRTaXplKTtcclxuICAgICAgICAgICQoJyNkb2MtY29udGFpbmVyJykuZmluZCgnaW1nJykuY3NzKCd3aWR0aCcsY2hhbmdlZFdpZHRoU2l6ZSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCl7XHJcbiAgICAgICAgIGF4aW9zXHJcbiAgICAgICAgICAgIC5nZXQoJ2RvY3VtZW50cy9nZXQtcGVuZGluZycpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PntcclxuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlRG9jdW1lbnRMaXN0cyhyZXN1bHQuZGF0YSxcInBlbmRpbmdcIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfSxcclxuICAgIHVwZGF0ZWQoKXtcclxuICAgICAgICAvLyBwZW5kb2NzID0gICQoJyNkb2N1bWVudHNsaXN0JykuZGF0YVRhYmxlKCB7XHJcbiAgICAgICAgLy8gICAgIHBhZ2luZzogdHJ1ZSxcclxuICAgICAgICAvLyAgICAgc2VhcmNoaW5nOiB0cnVlLFxyXG4gICAgICAgIC8vICAgICBvcmRlcjogW1sgMCwgXCJkZXNjXCIgXV1cclxuICAgICAgICAvLyB9KTtcclxuICAgIH0sXHJcbiAgICBkaXJlY3RpdmVzOiB7XHJcbiAgICAgICAgZGF0ZXBpY2tlcjoge1xyXG4gICAgICAgICAgICBiaW5kKGVsLCBiaW5kaW5nLCB2bm9kZSkge1xyXG4gICAgICAgICAgICAgICAgJChlbCkuZGF0ZXBpY2tlcih7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9ybWF0OiAneXl5eS1tbS1kZCdcclxuICAgICAgICAgICAgICAgIH0pLm9uKCdjaGFuZ2VEYXRlJywgKGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB2bS4kc2V0KHZtLnZhbGlkaXR5LCAnZXhwaXJlc19hdCcsIGUuZm9ybWF0KCd5eXl5LW1tLWRkJykpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1vdW50ZWQoKXtcclxuICAgICAgICAgJCgnI3ZpZXdEb2N1bWVudCcpLm9uKCdzaG93bi5icy5tb2RhbCxzaG93LmJzLm1vZGFsJywgKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmJhc2Vab29tID0gMTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGltZyA9ICQoJzxpbWcgaWQ9XCJzaGl0dHlpbWFnZVwiIGNsYXNzPVwiY2VudGVyLWJsb2NrXCIgc3JjPVwiL2ltYWdlcy9kb2N1bWVudHMvJyt0aGlzLnNlbGVjdGVkLmRvY3VtZW50YWJsZV9pZCsnLycrdGhpcy5zZWxlY3RlZC5uYW1lKydcIj4nKVxyXG4gICAgICAgICAgICAgICAgLm9uKCdsb2FkJywgKCkgPT4ge1xyXG4gICAgICAgICAgICAgIHZhciAkbW9kYWxJbWcgPSAkKCc8aW1nIHNyYz1cIi9pbWFnZXMvZG9jdW1lbnRzLycrdGhpcy5zZWxlY3RlZC5kb2N1bWVudGFibGVfaWQrJy8nK3RoaXMuc2VsZWN0ZWQubmFtZSsnXCIgY2xhc3M9XCJjZW50ZXItYmxvY2tcIiA+Jyk7XHJcbiAgICAgICAgICAgICAgLy8gJG1vZGFsSW1nLmFwcGVuZFRvKCcjZG9jLWNvbnRhaW5lcicpO1xyXG4gICAgICAgICAgICAgICQoJyNkb2MtY29udGFpbmVyJykuaHRtbCgkbW9kYWxJbWcpO1xyXG4gICAgICAgICAgICAgIHRoaXMuYmFzZUhlaWdodCA9ICRtb2RhbEltZy5oZWlnaHQoKTtcclxuICAgICAgICAgICAgICB0aGlzLmJhc2VXaWR0aCA9ICRtb2RhbEltZy53aWR0aCgpO1xyXG5cclxuICAgICAgICAgICAgICAkbW9kYWxJbWcuZHJhZ2dhYmxlKCk7XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgIH0pO1xyXG5cclxuXHJcbiAgICB9LFxyXG5cclxuICAgIHJlYWR5OiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLiRuZXh0VGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCk7XHJcbiAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCdmaXhUaXRsZScpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOntcclxuXHJcbiAgICAgICAgY2hhbmdlVmlldzpmdW5jdGlvbihzdGF0dXMpe1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVCdG5zID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgIGlmKHN0YXR1cyA9PSAgJ2FsbCcpe1xyXG4gICAgICAgICAgICAgICAgYXhpb3NcclxuICAgICAgICAgICAgICAgICAgICAuZ2V0KCdkb2N1bWVudHMvZ2V0LWRvY3VtZW50cycpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZURvY3VtZW50TGlzdHMocmVzdWx0LmRhdGEsIHN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9ZWxzZSBpZihzdGF0dXMgPT0gJ3BlbmRpbmcnKXtcclxuXHJcbiAgICAgICAgICAgICAgICBheGlvc1xyXG4gICAgICAgICAgICAgICAgICAgIC5nZXQoJ2RvY3VtZW50cy9nZXQtcGVuZGluZycpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGVEb2N1bWVudExpc3RzKHJlc3VsdC5kYXRhLCBzdGF0dXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgfWVsc2UgaWYoc3RhdHVzID09ICd2ZXJpZmllZCcpe1xyXG5cclxuICAgICAgICAgICAgICAgIGF4aW9zXHJcbiAgICAgICAgICAgICAgICAgICAgLmdldCgnZG9jdW1lbnRzL2dldC1hcHByb3ZlZCcpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZURvY3VtZW50TGlzdHMocmVzdWx0LmRhdGEsIHN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9ZWxzZSBpZihzdGF0dXMgPT0gJ2RlbmllZCcpe1xyXG5cclxuICAgICAgICAgICAgICAgIGF4aW9zXHJcbiAgICAgICAgICAgICAgICAgICAgLmdldCgnZG9jdW1lbnRzL2dldC1kZWNsaW5lZCcpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZURvY3VtZW50TGlzdHMocmVzdWx0LmRhdGEsIHN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB1cGRhdGVEb2N1bWVudExpc3RzOmZ1bmN0aW9uKGRhdGEsYWN0aXZlQnRuKXtcclxuICAgICAgICAgICAgdGhpcy5zaG93bGlzdCA9IGRhdGE7XHJcbiAgICAgICAgICAgIHRoaXMuYWN0aXZlU3RhdCA9IGFjdGl2ZUJ0bjtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZUJ0bnMgPSBmYWxzZTtcclxuICAgICAgICAgICAgJCgnI2RvY3VtZW50c2xpc3QnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgIHBlbmRvY3MgPSAgJCgnI2RvY3VtZW50c2xpc3QnKS5kYXRhVGFibGUoIHtcclxuICAgICAgICAgICAgICAgICAgICBwYWdpbmc6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyOiBbWyAwLCBcImRlc2NcIiBdXVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sMjAwKTtcclxuXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjb3VudERvY3MoKXtcclxuICAgICAgICAgICAgYXhpb3MuZ2V0KCdkb2N1bWVudHMvZ2V0LWNvdW50JylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PntcclxuICAgICAgICAgICAgICAgIHRoaXMuY291bnRsaXN0ID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaW5pdERvY3MoKXtcclxuICAgICAgICAgICAgYXhpb3MuZ2V0KCdkb2N1bWVudHMvZ2V0LWRvY3VtZW50cycpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT57XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dsaXN0ID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2F2ZVZhbGlkaXR5KGlkLCBkYXRlU2VsKXtcclxuICAgICAgICAgICAgJChcIi52ZXJpZnlcIikucG9wb3ZlcignaGlkZScpO1xyXG4gICAgICAgICAgICB0aGlzLnZhbGlkaXR5LmlkID0gaWQ7XHJcbiAgICAgICAgICAgIHRoaXMudmFsaWRpdHkuc3RhdHVzID0gXCJWZXJpZmllZFwiO1xyXG4gICAgICAgICAgICB0aGlzLnZhbGlkaXR5LmV4cGlyZXNfYXQgPSBkYXRlU2VsO1xyXG4gICAgICAgICAgICB2YXIgbW9udGggPSBwYXJzZUludChkYXRlU2VsLnN1YnN0cig1LDIpKTtcclxuICAgICAgICAgICAgdmFyIGRheSA9IHBhcnNlSW50KGRhdGVTZWwuc3Vic3RyKDgsMikpO1xyXG4gICAgICAgICAgICAgJCgnI2RvY3VtZW50c2xpc3QnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XHJcblxyXG4gICAgICAgICAgICBpZiAobW9udGggIT0gMCl7XHJcbiAgICAgICAgICAgICAgIGlmKGRheSAhPSAwKXtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbGlkaXR5LnN1Ym1pdCgncG9zdCcsICcvdmVyaWZ5JylcclxuICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgc3RhdCA9IHRoaXMuYWN0aXZlU3RhdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHN0YXQgPT0gXCJhbGxcIil7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXhpb3MuZ2V0KCdkb2N1bWVudHMvZ2V0LWRvY3VtZW50cycpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlRG9jdW1lbnRMaXN0cyhyZXN1bHQuZGF0YSwgc3RhdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNlIGlmKHN0YXQgPT0gXCJwZW5kaW5nXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF4aW9zLmdldCgnZG9jdW1lbnRzL2dldC1wZW5kaW5nJylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGVEb2N1bWVudExpc3RzKHJlc3VsdC5kYXRhLCBzdGF0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgfWVsc2UgaWYoc3RhdCA9PSBcInZlcmlmaWVkXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF4aW9zLmdldCgnZG9jdW1lbnRzL2dldC1hcHByb3ZlZCcpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlRG9jdW1lbnRMaXN0cyhyZXN1bHQuZGF0YSwgc3RhdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNlIGlmKHN0YXQgPT0gXCJkZW5pZWRcIil7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBheGlvcy5nZXQoJ2RvY3VtZW50cy9nZXQtZGVjbGluZWQnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlRG9jdW1lbnRMaXN0cyhyZXN1bHQuZGF0YSwgc3RhdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGVycm9yLmV4cGlyZXNfYXQpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnLnBvcG92ZXIgI3ZhbGlkc2V0JykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKFwiUGxlYXNlIHVzZSB2YWxpZCBkYXRlIG9mIGV4cGlyeS5cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoJ1VwZGF0ZSBmYWlsZWQuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiN2ZXJcIitpZCkucG9wb3Zlcignc2hvdycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgcGVuZG9jcyA9ICAkKCcjZG9jdW1lbnRzbGlzdCcpLmRhdGFUYWJsZSgge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3JkZXI6IFtbIDAsIFwiZGVzY1wiIF1dXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoJ1BsZWFzZSB1c2UgdmFsaWQgZXhwaXJhdGlvbiBkYXRlJyk7XHJcbiAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IoJ1BsZWFzZSB1c2UgdmFsaWQgZXhwaXJhdGlvbiBkYXRlJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgJCgnI2RvY3VtZW50c2xpc3QnKS5EYXRhVGFibGUoKS5kZXN0cm95KCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzYXZlUmVhc29uKGlkLCByZWFzb24pe1xyXG5cclxuICAgICAgICAgICAgJChcIi5kZW55XCIpLnBvcG92ZXIoJ2hpZGUnKTtcclxuICAgICAgICAgICAgdGhpcy5kZW5pZWQuaWQgPSBpZDtcclxuICAgICAgICAgICAgdGhpcy5kZW5pZWQuc3RhdHVzID0gXCJEZW5pZWRcIjtcclxuICAgICAgICAgICAgdGhpcy5kZW5pZWQucmVhc29uID0gcmVhc29uO1xyXG4gICAgICAgICAgICB0aGlzLmRlbmllZC5zdWJtaXQoJ3Bvc3QnLCcvZGVueScpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB2YXIgc3RhdCA9IHRoaXMuYWN0aXZlU3RhdDtcclxuICAgICAgICAgICAgICAgICBpZihzdGF0ID09IFwiYWxsXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgICBheGlvcy5nZXQoJ2RvY3VtZW50cy9nZXQtZG9jdW1lbnRzJylcclxuICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZURvY3VtZW50TGlzdHMocmVzdWx0LmRhdGEsIHN0YXQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgIH1lbHNlIGlmKHN0YXQgPT0gXCJwZW5kaW5nXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgICBheGlvcy5nZXQoJ2RvY3VtZW50cy9nZXQtcGVuZGluZycpXHJcbiAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PntcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGVEb2N1bWVudExpc3RzKHJlc3VsdC5kYXRhLCBzdGF0KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICB9ZWxzZSBpZihzdGF0ID09IFwidmVyaWZpZWRcIil7XHJcbiAgICAgICAgICAgICAgICAgICAgIGF4aW9zLmdldCgnZG9jdW1lbnRzL2dldC1hcHByb3ZlZCcpXHJcbiAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PntcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGVEb2N1bWVudExpc3RzKHJlc3VsdC5kYXRhLCBzdGF0KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICB9ZWxzZSBpZihzdGF0ID09IFwiZGVuaWVkXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgIGF4aW9zLmdldCgnZG9jdW1lbnRzL2dldC1kZWNsaW5lZCcpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZURvY3VtZW50TGlzdHMocmVzdWx0LmRhdGEsIHN0YXQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBzYXZlZC4nKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+e1xyXG4gICAgICAgICAgICAgICAgaWYoZXJyb3IucmVhc29uKXtcclxuICAgICAgICAgICAgICAgICAgICAkKCcucG9wb3ZlciAjcmVhc29uc2V0JykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcihcIlBsZWFzZSBpbmRpY2F0ZSByZWFzb24gdG8gZGVueS5cIik7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignVXBkYXRlIGZhaWxlZC4nKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICQoXCIjZGVuXCIraWQpLnBvcG92ZXIoJ3Nob3cnKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KVxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcclxuXHJcbiAgICQoJy50aWxlJylcclxuICAgIC8vIHRpbGUgbW91c2UgYWN0aW9uc1xyXG4gICAgLm9uKCdtb3VzZW92ZXInLCBmdW5jdGlvbigpe1xyXG4gICAgICAkKHRoaXMpLmNoaWxkcmVuKCcucGhvdG8nKS5jc3Moeyd0cmFuc2Zvcm0nOiAnc2NhbGUoJysgJCh0aGlzKS5hdHRyKCdkYXRhLXNjYWxlJykgKycpJ30pO1xyXG4gICAgfSlcclxuICAgIC5vbignbW91c2VvdXQnLCBmdW5jdGlvbigpe1xyXG4gICAgICAkKHRoaXMpLmNoaWxkcmVuKCcucGhvdG8nKS5jc3Moeyd0cmFuc2Zvcm0nOiAnc2NhbGUoMSknfSk7XHJcbiAgICB9KVxyXG4gICAgLm9uKCdtb3VzZW1vdmUnLCBmdW5jdGlvbihlKXtcclxuICAgICAgJCh0aGlzKS5jaGlsZHJlbignLnBob3RvJykuY3NzKHtcclxuICAgICAgICAgICd0cmFuc2Zvcm0tb3JpZ2luJzogKChlLnBhZ2VYIC0gJCh0aGlzKS5vZmZzZXQoKS5sZWZ0KSAvICQodGhpcykud2lkdGgoKSkgKiAxMDAgKyAnJSAnICsgKChlLnBhZ2VZIC0gJCh0aGlzKS5vZmZzZXQoKS50b3ApIC8gJCh0aGlzKS5oZWlnaHQoKSkgKiAxMDAgKyclJ1xyXG4gICAgICB9KTtcclxuICAgIH0pXHJcbn0pXHJcbiQoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIiwgXCIuc2F2ZS12ZXJpZmljYXRpb25cIiwgZnVuY3Rpb24oKSB7XHJcbiAgICB2YXIgaWQgPSB2bS5hY3RpdmVJZDtcclxuICAgIHZhciBkYXRlU2VsID0gJCgnLnBvcG92ZXIgI2V4cGlyZXNfYXQnKS52YWwoKTtcclxuICAgIHZtLnNhdmVWYWxpZGl0eShpZCwgZGF0ZVNlbCk7XHJcbn0pO1xyXG5cclxuJChkb2N1bWVudCkub24oXCJjbGlja1wiLCBcIi5zYXZlLWRlbmllZFwiLCBmdW5jdGlvbigpIHtcclxuICAgIHZhciBpZCA9IHZtLmFjdGl2ZUlkO1xyXG4gICAgdmFyIHJlYXNvbiA9ICQoJy5wb3BvdmVyICNyZWFzb24nKS52YWwoKTtcclxuICAgIHZtLnNhdmVSZWFzb24oaWQsIHJlYXNvbik7XHJcbn0pO1xyXG5cclxuJChkb2N1bWVudCkub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuICAgICQoJ1tkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIl0sW2RhdGEtb3JpZ2luYWwtdGl0bGVdJykuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKCEkKHRoaXMpLmlzKGUudGFyZ2V0KSAmJiAkKHRoaXMpLmhhcyhlLnRhcmdldCkubGVuZ3RoID09PSAwICYmICQoJy5wb3BvdmVyJykuaGFzKGUudGFyZ2V0KS5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgKCgkKHRoaXMpLnBvcG92ZXIoJ2hpZGUnKS5kYXRhKCdicy5wb3BvdmVyJyl8fHt9KS5pblN0YXRlfHx7fSkuY2xpY2sgPSBmYWxzZSAgLy8gZml4IGZvciBCUyAzLjMuNlxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59KTtcclxuXHJcbiQoZG9jdW1lbnQpLm9uKCdmb2N1cy5hdXRvRXhwYW5kJywgJ3RleHRhcmVhLmF1dG9FeHBhbmQnLCBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciBzYXZlZFZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgICAgICB0aGlzLnZhbHVlID0gJyc7XHJcbiAgICAgICAgdGhpcy5iYXNlU2Nyb2xsSGVpZ2h0ID0gdGhpcy5zY3JvbGxIZWlnaHQ7XHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IHNhdmVkVmFsdWU7XHJcbiAgICB9KVxyXG4gICAgLm9uKCdpbnB1dC5hdXRvRXhwYW5kJywgJ3RleHRhcmVhLmF1dG9FeHBhbmQnLCBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciBtaW5Sb3dzID0gdGhpcy5nZXRBdHRyaWJ1dGUoJ2RhdGEtbWluLXJvd3MnKXwwLCByb3dzO1xyXG4gICAgICAgIHRoaXMucm93cyA9IG1pblJvd3M7XHJcbiAgICAgICAgcm93cyA9IE1hdGguY2VpbCgodGhpcy5zY3JvbGxIZWlnaHQgLSB0aGlzLmJhc2VTY3JvbGxIZWlnaHQpIC8gMTcpO1xyXG4gICAgICAgIHRoaXMucm93cyA9IG1pblJvd3MgKyByb3dzO1xyXG4gICAgfSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvcGFnZXMvZG9jdW1lbnRzLmpzIiwiPHRlbXBsYXRlPlxyXG5cdDx0cj5cclxuXHRcdDx0ZD57e2RvY3MuaWR9fTwvdGQ+XHJcblx0XHQ8dGQ+e3tkb2NzLnVwbG9hZGVyLmZpcnN0X25hbWV9fSZuYnNwO3t7ZG9jcy51cGxvYWRlci5sYXN0X25hbWV9fTwvdGQ+XHJcblx0XHQ8dGQ+e3tkb2NzLnR5cGV9fTwvdGQ+XHJcblx0XHQ8dGQ+XHJcblx0XHRcdDxzcGFuIGNsYXNzPVwibGFiZWxcIj57e2RvY3Muc3RhdHVzfX08L3NwYW4+XHJcblx0XHQ8L3RkPlxyXG5cdFx0PHRkPnt7ZG9jcy5jcmVhdGVkX2F0fX08L3RkPlxyXG5cdFx0PHRkPnt7ZG9jcy5leHBpcmVzX2F0fX08L3RkPlxyXG5cdFx0PHRkPnt7ZG9jcy5yZWFzb259fTwvdGQ+XHJcblx0XHQ8dGQgc3R5bGU9XCJ0ZXh0LWFsaWduOmNlbnRlcjtcIj5cclxuXHRcdFx0PGEgaHJlZj1cIiNcIiBjbGFzcz1cImJ0biBidG4tc3VjY2VzcyBidG4tb3V0bGluZSBidG4tc21cIiBAY2xpY2s9XCJ2aWV3TW9kYWwoZG9jcy5pZClcIj48aSBjbGFzcz1cImZhIGZhLWV5ZVwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiA6aWQ9XCIndmVyJytkb2NzLmlkXCIgY2xhc3M9XCJidG4gYnRuLXdhcm5pbmcgYnRuLW91dGxpbmUgYnRuLXNtIHZlcmlmeVwiIGRhdGEtcGxhY2VtZW50PVwiYm90dG9tXCIgdi1pZj1cImRvY3Muc3RhdHVzID09ICdQZW5kaW5nJ1wiIEBjbGljaz1cInVwZGF0ZVN0YXR1cyhkb2NzLmlkKVwiPjxpIGNsYXNzPVwiZmEgZmEtdGh1bWJzLXVwXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIDppZD1cIidkZW4nK2RvY3MuaWRcIiBjbGFzcz1cImJ0biBidG4tZGFuZ2VyIGJ0bi1vdXRsaW5lIGJ0bi1zbSBkZW55XCIgZGF0YS1wbGFjZW1lbnQ9XCJib3R0b21cIiB2LWlmPVwiZG9jcy5zdGF0dXMgPT0gJ1BlbmRpbmcnXCIgQGNsaWNrPVwidXBkYXRlU3RhdHVzKGRvY3MuaWQpXCI+PGkgY2xhc3M9XCJmYSBmYS10aHVtYnMtZG93blwiPjwvaT48L2E+XHJcblx0XHQ8L3RkPlxyXG5cdDwvdHI+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5cdGV4cG9ydCBkZWZhdWx0e1xyXG5cdFx0cHJvcHM6IFsnZG9jcyddLFxyXG5cdFx0bWV0aG9kczp7XHJcblx0XHRcdHZpZXdNb2RhbChpZCl7XHJcblx0XHRcdFx0JCgnI3ZpZXdJbWcnKS5hdHRyKCdzcmMnLCAnJyk7XHJcblx0XHRcdFx0YXhpb3MuZ2V0KCdkb2N1bWVudHMvZ2V0LWRldGFpbHMvJytpZClcclxuXHRcdFx0XHQudGhlbihyZXN1bHQgPT57XHJcblx0XHRcdFx0XHR0aGlzLiRwYXJlbnQuc2VsZWN0ZWQgPSByZXN1bHQuZGF0YTtcclxuXHRcdFx0XHRcdCQoJyN2aWV3RG9jdW1lbnQnKS5tb2RhbCgndG9nZ2xlJyk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdH0sXHJcblx0XHRcdHVwZGF0ZVN0YXR1cyhpZCl7XHJcblx0XHRcdFx0dGhpcy4kcGFyZW50LmFjdGl2ZUlkID0gaWQ7XHRcdFx0XHRcclxuXHRcdFx0fSxcclxuXHJcblx0XHR9LFxyXG5cdFx0bW91bnRlZCgpIHtcclxuXHRcdFx0JChcIi52ZXJpZnlcIilcclxuXHRcdCAgICAgICAgLnBvcG92ZXIoe1xyXG5cdFx0ICAgICAgICBcdGh0bWw6dHJ1ZSxcclxuXHRcdCAgICAgICAgXHRjb250ZW50OiBmdW5jdGlvbiAoKSB7XHJcblx0XHQgICAgICAgIFx0XHQvLyBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuXHRcdCAgICAgICAgXHRcdC8vIFx0JCh0aGlzLm5leHRTaWJsaW5nKS5maW5kKCdpbnB1dCcpLmRhdGVwaWNrZXIoKTtcclxuXHRcdCAgICAgICAgXHRcdC8vIH0uYmluZCh0aGlzKSwgMSk7XHJcblx0XHQgICAgICAgIFx0XHRyZXR1cm4gJCgnI2ZybVZhbGlkVW50aWwnKS5odG1sKCk7XHJcblx0XHQgICAgICAgIFx0fSxcclxuXHRcdCAgICAgICBcdH0pO1xyXG5cclxuXHRcdCAgICAkKFwiLmRlbnlcIilcclxuXHRcdCAgICAgICAgLnBvcG92ZXIoe1xyXG5cdFx0ICAgICAgICBcdGh0bWw6dHJ1ZSxcclxuXHRcdCAgICAgICAgXHRjb250ZW50OiAkKCcjZnJtUmVhc29uJykuaHRtbCgpLFxyXG5cdFx0ICAgICAgIFx0fSk7XHJcblx0XHR9LFxyXG5cclxuXHJcblx0fVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gTGlzdC52dWU/ZDQxN2NjZWEiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9MaXN0LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNDdiMDc2MWFcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vTGlzdC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXGNwYW5lbFxcXFxjb21wb25lbnRzXFxcXERvY3VtZW50c1xcXFxMaXN0LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIExpc3QudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTQ3YjA3NjFhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNDdiMDc2MWFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jcGFuZWwvY29tcG9uZW50cy9Eb2N1bWVudHMvTGlzdC52dWVcbi8vIG1vZHVsZSBpZCA9IDIxNVxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0cicsIFtfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0uZG9jcy5pZCkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5kb2NzLnVwbG9hZGVyLmZpcnN0X25hbWUpICsgXCLCoFwiICsgX3ZtLl9zKF92bS5kb2NzLnVwbG9hZGVyLmxhc3RfbmFtZSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5kb2NzLnR5cGUpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWxcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uZG9jcy5zdGF0dXMpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5kb2NzLmNyZWF0ZWRfYXQpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0uZG9jcy5leHBpcmVzX2F0KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoX3ZtLmRvY3MucmVhc29uKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcInRleHQtYWxpZ25cIjogXCJjZW50ZXJcIlxuICAgIH1cbiAgfSwgW19jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tc3VjY2VzcyBidG4tb3V0bGluZSBidG4tc21cIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnZpZXdNb2RhbChfdm0uZG9jcy5pZClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1leWVcIlxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmRvY3Muc3RhdHVzID09ICdQZW5kaW5nJykgPyBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXdhcm5pbmcgYnRuLW91dGxpbmUgYnRuLXNtIHZlcmlmeVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiaWRcIjogJ3ZlcicgKyBfdm0uZG9jcy5pZCxcbiAgICAgIFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS51cGRhdGVTdGF0dXMoX3ZtLmRvY3MuaWQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtdGh1bWJzLXVwXCJcbiAgfSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmRvY3Muc3RhdHVzID09ICdQZW5kaW5nJykgPyBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRhbmdlciBidG4tb3V0bGluZSBidG4tc20gZGVueVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiaWRcIjogJ2RlbicgKyBfdm0uZG9jcy5pZCxcbiAgICAgIFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS51cGRhdGVTdGF0dXMoX3ZtLmRvY3MuaWQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtdGh1bWJzLWRvd25cIlxuICB9KV0pIDogX3ZtLl9lKCldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTQ3YjA3NjFhXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNDdiMDc2MWFcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY3BhbmVsL2NvbXBvbmVudHMvRG9jdW1lbnRzL0xpc3QudnVlXG4vLyBtb2R1bGUgaWQgPSAyMzVcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCJdLCJzb3VyY2VSb290IjoiIn0=