/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 422);
/******/ })
/************************************************************************/
/******/ ({

/***/ 172:
/***/ (function(module, exports) {

new Vue({
    el: '#offcanvas',

    data: {
        search: null,
        users: [],
        tmpUsers: [],
        supports: [],
        timeout: null,
        loading: false,
        $usersContainer: null,
        form: new Form({
            subject: null,
            body: null
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        suppClick: null,
        newConvoForm: new Form({
            supportIds: []
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        notif: [],
        disabled: false
    },

    watch: {
        search: function search(val) {
            if (val != null && val.length === 0) {
                this.users = this.tmpUsers;
            }
        },
        users: function users(val) {
            if (val.length) {
                setTimeout(function () {
                    this.$usersContainer.perfectScrollbar('update');
                }.bind(this), 1);
            }
        }
    },

    methods: {
        getFriends: function getFriends() {
            var _this = this;

            return axiosAPIv1.get('/friends2').then(function (result) {
                _this.users = result.data;
                _this.tmpUsers = result.data;
            });
        },


        //3 default support
        getSupports: function getSupports() {
            var _this2 = this;

            return axiosAPIv1.get('/getSupports').then(function (result) {
                _this2.supports = result.data;
            });
        },
        searchUser: function searchUser(e) {
            if (!(e.which <= 90 && e.which >= 48 || e.which === 8)) {
                return;
            }

            clearTimeout(this.timeout);

            this.timeout = setTimeout(function () {
                var _this3 = this;

                if (this.search && this.search.length >= 2) {
                    this.loading = true;

                    axiosAPIv1.get('/users', {
                        params: {
                            search: this.search
                        }
                    }).then(function (result) {
                        _this3.users = result.data;

                        _this3.loading = false;
                    });
                }
            }.bind(this), 200);
        },
        contactSupport: function contactSupport(user) {
            //save selected support
            this.suppClick = user;
            $('#modalHelp').modal('toggle');
        },
        select: function select(user) {
            this.toggleOffcanvas();

            var chat = [];
            $(".panel-chat-box").each(function () {
                chat.push($(this).attr('id'));
            });

            if ($.inArray('user' + user.id, chat) == '-1') Event.fire('chat-box.show', user);

            setTimeout(function () {
                $('.js-auto-size').textareaAutoSize();
            }, 5000);
        },
        toggleOffcanvas: function toggleOffcanvas() {
            $(this.$el).toggleClass('expanded');
        },
        avatar: function avatar(user) {
            return user.avatar ? user.avatar : '/images/avatar/' + (user.gender === 'Male' ? 'boy' : 'girl') + '-avatar.png';
        },
        fullName: function fullName(user) {
            return user.first_name + ' ' + user.last_name;
        },


        //submit concern
        submitConcern: function submitConcern() {
            var _this4 = this;

            this.$validator.validateAll();

            if ($('#modalHelp #subject').val() != '' && $('#modalHelp #body').val() != '') {
                this.disabled = true;
            }

            if (!this.errors.any()) {
                this.form.submit('post', '/issues').then(function (result) {
                    _this4.$validator.clean();
                    //let supp = this.suppClick.id; //get id of clicked supp
                    var supp = 2; // mam cha's account
                    var issuer = Laravel.user.id;
                    var casenum = result.data.issue_number;
                    //create new chat-room and in chat room user include selected support and the user
                    _this4.newConvoForm.supportIds.push(supp);
                    _this4.newConvoForm.submit('put', '/customer-service/' + result.data.id).then(function (result) {
                        //alert(result.data.chat_room.name);
                        _this4.notif = result.data;
                        _this4.form.subject = null;
                        _this4.form.body = null;
                        $('#modalHelp').modal('toggle');
                        // Allow user to send another ticket
                        _this4.disabled = false;
                        toastr.success('Successfully Submitted!');
                        _this4.newConvoForm.supportIds = [];
                        //open chat box with case number as title for the conversation
                        _this4.toggleOffcanvas();
                        var chat = [];
                        $(".panel-chat-box").each(function () {
                            chat.push($(this).attr('id'));
                        });

                        if (casenum) {
                            if ($.inArray('user' + casenum, chat) == '-1') {
                                Event.fire('chat-box.show', _this4.notif[0]);
                            }
                        }

                        setTimeout(function () {
                            $('.js-auto-size').textareaAutoSize();
                        }, 5000);
                    }).catch($.noop);
                }).catch($.noop);
            }
        }
    },

    created: function created() {
        this.getFriends();
        this.getSupports();
    },
    mounted: function mounted() {
        this.$usersContainer = $(this.$el).find('.user-list');
        this.$usersContainer.perfectScrollbar();
    }
});

/***/ }),

/***/ 422:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(172);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvb2ZmY2FudmFzLmpzIl0sIm5hbWVzIjpbIlZ1ZSIsImVsIiwiZGF0YSIsInNlYXJjaCIsInVzZXJzIiwidG1wVXNlcnMiLCJzdXBwb3J0cyIsInRpbWVvdXQiLCJsb2FkaW5nIiwiJHVzZXJzQ29udGFpbmVyIiwiZm9ybSIsIkZvcm0iLCJzdWJqZWN0IiwiYm9keSIsImJhc2VVUkwiLCJheGlvc0FQSXYxIiwiZGVmYXVsdHMiLCJzdXBwQ2xpY2siLCJuZXdDb252b0Zvcm0iLCJzdXBwb3J0SWRzIiwibm90aWYiLCJkaXNhYmxlZCIsIndhdGNoIiwidmFsIiwibGVuZ3RoIiwic2V0VGltZW91dCIsInBlcmZlY3RTY3JvbGxiYXIiLCJiaW5kIiwibWV0aG9kcyIsImdldEZyaWVuZHMiLCJnZXQiLCJ0aGVuIiwicmVzdWx0IiwiZ2V0U3VwcG9ydHMiLCJzZWFyY2hVc2VyIiwiZSIsIndoaWNoIiwiY2xlYXJUaW1lb3V0IiwicGFyYW1zIiwiY29udGFjdFN1cHBvcnQiLCJ1c2VyIiwiJCIsIm1vZGFsIiwic2VsZWN0IiwidG9nZ2xlT2ZmY2FudmFzIiwiY2hhdCIsImVhY2giLCJwdXNoIiwiYXR0ciIsImluQXJyYXkiLCJpZCIsIkV2ZW50IiwiZmlyZSIsInRleHRhcmVhQXV0b1NpemUiLCIkZWwiLCJ0b2dnbGVDbGFzcyIsImF2YXRhciIsImdlbmRlciIsImZ1bGxOYW1lIiwiZmlyc3RfbmFtZSIsImxhc3RfbmFtZSIsInN1Ym1pdENvbmNlcm4iLCIkdmFsaWRhdG9yIiwidmFsaWRhdGVBbGwiLCJlcnJvcnMiLCJhbnkiLCJzdWJtaXQiLCJjbGVhbiIsInN1cHAiLCJpc3N1ZXIiLCJMYXJhdmVsIiwiY2FzZW51bSIsImlzc3VlX251bWJlciIsInRvYXN0ciIsInN1Y2Nlc3MiLCJjYXRjaCIsIm5vb3AiLCJjcmVhdGVkIiwibW91bnRlZCIsImZpbmQiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUEsSUFBSUEsR0FBSixDQUFRO0FBQ0pDLFFBQUksWUFEQTs7QUFHSkMsVUFBTTtBQUNGQyxnQkFBUSxJQUROO0FBRUZDLGVBQU8sRUFGTDtBQUdGQyxrQkFBVSxFQUhSO0FBSUZDLGtCQUFVLEVBSlI7QUFLRkMsaUJBQVMsSUFMUDtBQU1GQyxpQkFBUyxLQU5QO0FBT0ZDLHlCQUFpQixJQVBmO0FBUUZDLGNBQU0sSUFBSUMsSUFBSixDQUFTO0FBQ1hDLHFCQUFTLElBREU7QUFFWEMsa0JBQU07QUFGSyxTQUFULEVBR0gsRUFBRUMsU0FBU0MsV0FBV0MsUUFBWCxDQUFvQkYsT0FBL0IsRUFIRyxDQVJKO0FBWUZHLG1CQUFZLElBWlY7QUFhRkMsc0JBQWMsSUFBSVAsSUFBSixDQUFTO0FBQ25CUSx3QkFBWTtBQURPLFNBQVQsRUFFWixFQUFFTCxTQUFTQyxXQUFXQyxRQUFYLENBQW9CRixPQUEvQixFQUZZLENBYlo7QUFnQkZNLGVBQVEsRUFoQk47QUFpQkZDLGtCQUFVO0FBakJSLEtBSEY7O0FBdUJKQyxXQUFPO0FBQ0huQixjQURHLGtCQUNJb0IsR0FESixFQUNTO0FBQ1IsZ0JBQUlBLE9BQU8sSUFBUCxJQUFlQSxJQUFJQyxNQUFKLEtBQWUsQ0FBbEMsRUFBcUM7QUFDakMscUJBQUtwQixLQUFMLEdBQWEsS0FBS0MsUUFBbEI7QUFDSDtBQUNKLFNBTEU7QUFPSEQsYUFQRyxpQkFPR21CLEdBUEgsRUFPUTtBQUNQLGdCQUFJQSxJQUFJQyxNQUFSLEVBQWdCO0FBQ1pDLDJCQUFXLFlBQVk7QUFDbkIseUJBQUtoQixlQUFMLENBQXFCaUIsZ0JBQXJCLENBQXNDLFFBQXRDO0FBQ0gsaUJBRlUsQ0FFVEMsSUFGUyxDQUVKLElBRkksQ0FBWCxFQUVjLENBRmQ7QUFHSDtBQUNKO0FBYkUsS0F2Qkg7O0FBdUNKQyxhQUFTO0FBQ0xDLGtCQURLLHdCQUNRO0FBQUE7O0FBQ1QsbUJBQU9kLFdBQVdlLEdBQVgsQ0FBZSxXQUFmLEVBQ0ZDLElBREUsQ0FDRyxrQkFBVTtBQUNaLHNCQUFLM0IsS0FBTCxHQUFhNEIsT0FBTzlCLElBQXBCO0FBQ0Esc0JBQUtHLFFBQUwsR0FBZ0IyQixPQUFPOUIsSUFBdkI7QUFDSCxhQUpFLENBQVA7QUFLSCxTQVBJOzs7QUFTTDtBQUNBK0IsbUJBVksseUJBVVM7QUFBQTs7QUFDVixtQkFBT2xCLFdBQVdlLEdBQVgsQ0FBZSxjQUFmLEVBQ0ZDLElBREUsQ0FDRyxrQkFBVTtBQUNaLHVCQUFLekIsUUFBTCxHQUFnQjBCLE9BQU85QixJQUF2QjtBQUNILGFBSEUsQ0FBUDtBQUlILFNBZkk7QUFnQkxnQyxrQkFoQkssc0JBZ0JNQyxDQWhCTixFQWdCUztBQUNWLGdCQUFJLEVBQUtBLEVBQUVDLEtBQUYsSUFBVyxFQUFaLElBQW9CRCxFQUFFQyxLQUFGLElBQVcsRUFBaEMsSUFBd0NELEVBQUVDLEtBQUYsS0FBWSxDQUF2RCxDQUFKLEVBQStEO0FBQzNEO0FBQ0g7O0FBRURDLHlCQUFhLEtBQUs5QixPQUFsQjs7QUFFQSxpQkFBS0EsT0FBTCxHQUFla0IsV0FBVyxZQUFZO0FBQUE7O0FBQ2xDLG9CQUFJLEtBQUt0QixNQUFMLElBQWdCLEtBQUtBLE1BQUwsQ0FBWXFCLE1BQVosSUFBc0IsQ0FBMUMsRUFBOEM7QUFDMUMseUJBQUtoQixPQUFMLEdBQWUsSUFBZjs7QUFFQU8sK0JBQVdlLEdBQVgsQ0FBZSxRQUFmLEVBQXlCO0FBQ3JCUSxnQ0FBUTtBQUNKbkMsb0NBQVEsS0FBS0E7QUFEVDtBQURhLHFCQUF6QixFQUlHNEIsSUFKSCxDQUlRLGtCQUFVO0FBQ2QsK0JBQUszQixLQUFMLEdBQWE0QixPQUFPOUIsSUFBcEI7O0FBRUEsK0JBQUtNLE9BQUwsR0FBZSxLQUFmO0FBQ0gscUJBUkQ7QUFTSDtBQUNKLGFBZHlCLENBY3hCbUIsSUFkd0IsQ0FjbkIsSUFkbUIsQ0FBWCxFQWNELEdBZEMsQ0FBZjtBQWVILFNBdENJO0FBdUNMWSxzQkF2Q0ssMEJBdUNVQyxJQXZDVixFQXVDZ0I7QUFDakI7QUFDQSxpQkFBS3ZCLFNBQUwsR0FBaUJ1QixJQUFqQjtBQUNBQyxjQUFFLFlBQUYsRUFBZ0JDLEtBQWhCLENBQXNCLFFBQXRCO0FBQ0gsU0EzQ0k7QUE0Q0xDLGNBNUNLLGtCQTRDRUgsSUE1Q0YsRUE0Q1E7QUFDVCxpQkFBS0ksZUFBTDs7QUFFQSxnQkFBSUMsT0FBTyxFQUFYO0FBQ0FKLGNBQUUsaUJBQUYsRUFBcUJLLElBQXJCLENBQTBCLFlBQVU7QUFDaENELHFCQUFLRSxJQUFMLENBQVVOLEVBQUUsSUFBRixFQUFRTyxJQUFSLENBQWEsSUFBYixDQUFWO0FBQ0gsYUFGRDs7QUFJQSxnQkFBR1AsRUFBRVEsT0FBRixDQUFVLFNBQU9ULEtBQUtVLEVBQXRCLEVBQXlCTCxJQUF6QixLQUFnQyxJQUFuQyxFQUNJTSxNQUFNQyxJQUFOLENBQVcsZUFBWCxFQUE0QlosSUFBNUI7O0FBRUpmLHVCQUFXLFlBQVU7QUFDbkJnQixrQkFBRSxlQUFGLEVBQW1CWSxnQkFBbkI7QUFDRCxhQUZELEVBRUcsSUFGSDtBQUdILFNBMURJO0FBNERMVCx1QkE1REssNkJBNERhO0FBQ2RILGNBQUUsS0FBS2EsR0FBUCxFQUFZQyxXQUFaLENBQXdCLFVBQXhCO0FBQ0gsU0E5REk7QUFnRUxDLGNBaEVLLGtCQWdFRWhCLElBaEVGLEVBZ0VRO0FBQ1QsbUJBQU9BLEtBQUtnQixNQUFMLEdBQ0RoQixLQUFLZ0IsTUFESixHQUVELHFCQUFzQmhCLEtBQUtpQixNQUFMLEtBQWdCLE1BQWpCLEdBQTJCLEtBQTNCLEdBQW1DLE1BQXhELElBQWtFLGFBRnhFO0FBR0gsU0FwRUk7QUFzRUxDLGdCQXRFSyxvQkFzRUlsQixJQXRFSixFQXNFVTtBQUNYLG1CQUFPQSxLQUFLbUIsVUFBTCxHQUFrQixHQUFsQixHQUF3Qm5CLEtBQUtvQixTQUFwQztBQUNILFNBeEVJOzs7QUEwRUw7QUFDQUMscUJBM0VLLDJCQTJFVztBQUFBOztBQUNaLGlCQUFLQyxVQUFMLENBQWdCQyxXQUFoQjs7QUFFQSxnQkFBR3RCLEVBQUUscUJBQUYsRUFBeUJsQixHQUF6QixNQUFrQyxFQUFsQyxJQUF5Q2tCLEVBQUUsa0JBQUYsRUFBc0JsQixHQUF0QixNQUErQixFQUEzRSxFQUErRTtBQUMzRSxxQkFBS0YsUUFBTCxHQUFnQixJQUFoQjtBQUNIOztBQUVELGdCQUFJLENBQUUsS0FBSzJDLE1BQUwsQ0FBWUMsR0FBWixFQUFOLEVBQXlCO0FBQ3JCLHFCQUFLdkQsSUFBTCxDQUFVd0QsTUFBVixDQUFpQixNQUFqQixFQUF5QixTQUF6QixFQUNLbkMsSUFETCxDQUNVLGtCQUFVO0FBQ1osMkJBQUsrQixVQUFMLENBQWdCSyxLQUFoQjtBQUNBO0FBQ0Esd0JBQUlDLE9BQU8sQ0FBWCxDQUhZLENBR0U7QUFDZCx3QkFBSUMsU0FBU0MsUUFBUTlCLElBQVIsQ0FBYVUsRUFBMUI7QUFDQSx3QkFBSXFCLFVBQVV2QyxPQUFPOUIsSUFBUCxDQUFZc0UsWUFBMUI7QUFDQTtBQUNBLDJCQUFLdEQsWUFBTCxDQUFrQkMsVUFBbEIsQ0FBNkI0QixJQUE3QixDQUFrQ3FCLElBQWxDO0FBQ0EsMkJBQUtsRCxZQUFMLENBQWtCZ0QsTUFBbEIsQ0FBeUIsS0FBekIsRUFBZ0MsdUJBQXVCbEMsT0FBTzlCLElBQVAsQ0FBWWdELEVBQW5FLEVBQ0NuQixJQURELENBQ00sa0JBQVU7QUFDWjtBQUNBLCtCQUFLWCxLQUFMLEdBQWFZLE9BQU85QixJQUFwQjtBQUNBLCtCQUFLUSxJQUFMLENBQVVFLE9BQVYsR0FBb0IsSUFBcEI7QUFDQSwrQkFBS0YsSUFBTCxDQUFVRyxJQUFWLEdBQWlCLElBQWpCO0FBQ0E0QiwwQkFBRSxZQUFGLEVBQWdCQyxLQUFoQixDQUFzQixRQUF0QjtBQUNBO0FBQ0EsK0JBQUtyQixRQUFMLEdBQWdCLEtBQWhCO0FBQ0FvRCwrQkFBT0MsT0FBUCxDQUFlLHlCQUFmO0FBQ0EsK0JBQUt4RCxZQUFMLENBQWtCQyxVQUFsQixHQUErQixFQUEvQjtBQUNBO0FBQ0EsK0JBQUt5QixlQUFMO0FBQ0EsNEJBQUlDLE9BQU8sRUFBWDtBQUNBSiwwQkFBRSxpQkFBRixFQUFxQkssSUFBckIsQ0FBMEIsWUFBVTtBQUNoQ0QsaUNBQUtFLElBQUwsQ0FBVU4sRUFBRSxJQUFGLEVBQVFPLElBQVIsQ0FBYSxJQUFiLENBQVY7QUFDSCx5QkFGRDs7QUFJQSw0QkFBSXVCLE9BQUosRUFBYTtBQUNULGdDQUFHOUIsRUFBRVEsT0FBRixDQUFVLFNBQU9zQixPQUFqQixFQUF5QjFCLElBQXpCLEtBQWdDLElBQW5DLEVBQXdDO0FBQ3BDTSxzQ0FBTUMsSUFBTixDQUFXLGVBQVgsRUFBNEIsT0FBS2hDLEtBQUwsQ0FBVyxDQUFYLENBQTVCO0FBQ0g7QUFDSjs7QUFFREssbUNBQVcsWUFBVTtBQUNuQmdCLDhCQUFFLGVBQUYsRUFBbUJZLGdCQUFuQjtBQUNELHlCQUZELEVBRUcsSUFGSDtBQUdILHFCQTNCRCxFQTRCQ3NCLEtBNUJELENBNEJPbEMsRUFBRW1DLElBNUJUO0FBOEJILGlCQXZDTCxFQXdDS0QsS0F4Q0wsQ0F3Q1dsQyxFQUFFbUMsSUF4Q2I7QUF5Q0g7QUFDSjtBQTdISSxLQXZDTDs7QUF1S0pDLFdBdktJLHFCQXVLTTtBQUNOLGFBQUtoRCxVQUFMO0FBQ0EsYUFBS0ksV0FBTDtBQUNILEtBMUtHO0FBNEtKNkMsV0E1S0kscUJBNEtNO0FBQ04sYUFBS3JFLGVBQUwsR0FBdUJnQyxFQUFFLEtBQUthLEdBQVAsRUFBWXlCLElBQVosQ0FBaUIsWUFBakIsQ0FBdkI7QUFDQSxhQUFLdEUsZUFBTCxDQUFxQmlCLGdCQUFyQjtBQUNIO0FBL0tHLENBQVIsRSIsImZpbGUiOiJqcy9vZmZjYW52YXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQyMik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCJuZXcgVnVlKHtcclxuICAgIGVsOiAnI29mZmNhbnZhcycsXHJcblxyXG4gICAgZGF0YToge1xyXG4gICAgICAgIHNlYXJjaDogbnVsbCxcclxuICAgICAgICB1c2VyczogW10sXHJcbiAgICAgICAgdG1wVXNlcnM6IFtdLFxyXG4gICAgICAgIHN1cHBvcnRzOiBbXSxcclxuICAgICAgICB0aW1lb3V0OiBudWxsLFxyXG4gICAgICAgIGxvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgICR1c2Vyc0NvbnRhaW5lcjogbnVsbCxcclxuICAgICAgICBmb3JtOiBuZXcgRm9ybSh7XHJcbiAgICAgICAgICAgIHN1YmplY3Q6IG51bGwsXHJcbiAgICAgICAgICAgIGJvZHk6IG51bGwsXHJcbiAgICAgICAgfSwgeyBiYXNlVVJMOiBheGlvc0FQSXYxLmRlZmF1bHRzLmJhc2VVUkwgfSksXHJcbiAgICAgICAgc3VwcENsaWNrIDogbnVsbCxcclxuICAgICAgICBuZXdDb252b0Zvcm06IG5ldyBGb3JtKHtcclxuICAgICAgICAgICAgc3VwcG9ydElkczogW10sXHJcbiAgICAgICAgfSx7IGJhc2VVUkw6IGF4aW9zQVBJdjEuZGVmYXVsdHMuYmFzZVVSTCB9KSxcclxuICAgICAgICBub3RpZiA6IFtdLFxyXG4gICAgICAgIGRpc2FibGVkOiBmYWxzZVxyXG4gICAgfSxcclxuXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgIHNlYXJjaCh2YWwpIHtcclxuICAgICAgICAgICAgaWYgKHZhbCAhPSBudWxsICYmIHZhbC5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlcnMgPSB0aGlzLnRtcFVzZXJzO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdXNlcnModmFsKSB7XHJcbiAgICAgICAgICAgIGlmICh2YWwubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLiR1c2Vyc0NvbnRhaW5lci5wZXJmZWN0U2Nyb2xsYmFyKCd1cGRhdGUnKTtcclxuICAgICAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXRGcmllbmRzKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9mcmllbmRzMicpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXNlcnMgPSByZXN1bHQuZGF0YTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRtcFVzZXJzID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICAvLzMgZGVmYXVsdCBzdXBwb3J0XHJcbiAgICAgICAgZ2V0U3VwcG9ydHMoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL2dldFN1cHBvcnRzJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdXBwb3J0cyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZWFyY2hVc2VyKGUpIHtcclxuICAgICAgICAgICAgaWYgKCEgKCgoZS53aGljaCA8PSA5MCkgJiYgKGUud2hpY2ggPj0gNDgpKSB8fCBlLndoaWNoID09PSA4KSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lb3V0KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VhcmNoICYmICh0aGlzLnNlYXJjaC5sZW5ndGggPj0gMikpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL3VzZXJzJywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaDogdGhpcy5zZWFyY2hcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VycyA9IHJlc3VsdC5kYXRhO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgMjAwKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNvbnRhY3RTdXBwb3J0KHVzZXIpIHtcclxuICAgICAgICAgICAgLy9zYXZlIHNlbGVjdGVkIHN1cHBvcnRcclxuICAgICAgICAgICAgdGhpcy5zdXBwQ2xpY2sgPSB1c2VyO1xyXG4gICAgICAgICAgICAkKCcjbW9kYWxIZWxwJykubW9kYWwoJ3RvZ2dsZScpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2VsZWN0KHVzZXIpIHtcclxuICAgICAgICAgICAgdGhpcy50b2dnbGVPZmZjYW52YXMoKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBjaGF0ID0gW107XHJcbiAgICAgICAgICAgICQoXCIucGFuZWwtY2hhdC1ib3hcIikuZWFjaChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgY2hhdC5wdXNoKCQodGhpcykuYXR0cignaWQnKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYoJC5pbkFycmF5KCd1c2VyJyt1c2VyLmlkLGNoYXQpPT0nLTEnKVxyXG4gICAgICAgICAgICAgICAgRXZlbnQuZmlyZSgnY2hhdC1ib3guc2hvdycsIHVzZXIpO1xyXG5cclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICQoJy5qcy1hdXRvLXNpemUnKS50ZXh0YXJlYUF1dG9TaXplKCk7XHJcbiAgICAgICAgICAgIH0sIDUwMDApO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHRvZ2dsZU9mZmNhbnZhcygpIHtcclxuICAgICAgICAgICAgJCh0aGlzLiRlbCkudG9nZ2xlQ2xhc3MoJ2V4cGFuZGVkJyk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgYXZhdGFyKHVzZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHVzZXIuYXZhdGFyXHJcbiAgICAgICAgICAgICAgICA/IHVzZXIuYXZhdGFyXHJcbiAgICAgICAgICAgICAgICA6ICcvaW1hZ2VzL2F2YXRhci8nICsgKCh1c2VyLmdlbmRlciA9PT0gJ01hbGUnKSA/ICdib3knIDogJ2dpcmwnKSArICctYXZhdGFyLnBuZyc7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZnVsbE5hbWUodXNlcikge1xyXG4gICAgICAgICAgICByZXR1cm4gdXNlci5maXJzdF9uYW1lICsgJyAnICsgdXNlci5sYXN0X25hbWU7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgLy9zdWJtaXQgY29uY2VyblxyXG4gICAgICAgIHN1Ym1pdENvbmNlcm4oKSB7XHJcbiAgICAgICAgICAgIHRoaXMuJHZhbGlkYXRvci52YWxpZGF0ZUFsbCgpO1xyXG5cclxuICAgICAgICAgICAgaWYoJCgnI21vZGFsSGVscCAjc3ViamVjdCcpLnZhbCgpICE9ICcnICYmICAkKCcjbW9kYWxIZWxwICNib2R5JykudmFsKCkgIT0gJycpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoISB0aGlzLmVycm9ycy5hbnkoKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtLnN1Ym1pdCgncG9zdCcsICcvaXNzdWVzJylcclxuICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiR2YWxpZGF0b3IuY2xlYW4oKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy9sZXQgc3VwcCA9IHRoaXMuc3VwcENsaWNrLmlkOyAvL2dldCBpZCBvZiBjbGlja2VkIHN1cHBcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHN1cHAgPSAyOyAvLyBtYW0gY2hhJ3MgYWNjb3VudFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgaXNzdWVyID0gTGFyYXZlbC51c2VyLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgY2FzZW51bSA9IHJlc3VsdC5kYXRhLmlzc3VlX251bWJlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy9jcmVhdGUgbmV3IGNoYXQtcm9vbSBhbmQgaW4gY2hhdCByb29tIHVzZXIgaW5jbHVkZSBzZWxlY3RlZCBzdXBwb3J0IGFuZCB0aGUgdXNlclxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5ld0NvbnZvRm9ybS5zdXBwb3J0SWRzLnB1c2goc3VwcCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubmV3Q29udm9Gb3JtLnN1Ym1pdCgncHV0JywgJy9jdXN0b21lci1zZXJ2aWNlLycgKyByZXN1bHQuZGF0YS5pZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vYWxlcnQocmVzdWx0LmRhdGEuY2hhdF9yb29tLm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZiA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnN1YmplY3QgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLmJvZHkgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI21vZGFsSGVscCcpLm1vZGFsKCd0b2dnbGUnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFsbG93IHVzZXIgdG8gc2VuZCBhbm90aGVyIHRpY2tldFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBTdWJtaXR0ZWQhJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5ld0NvbnZvRm9ybS5zdXBwb3J0SWRzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL29wZW4gY2hhdCBib3ggd2l0aCBjYXNlIG51bWJlciBhcyB0aXRsZSBmb3IgdGhlIGNvbnZlcnNhdGlvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50b2dnbGVPZmZjYW52YXMoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjaGF0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiLnBhbmVsLWNoYXQtYm94XCIpLmVhY2goZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGF0LnB1c2goJCh0aGlzKS5hdHRyKCdpZCcpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjYXNlbnVtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoJC5pbkFycmF5KCd1c2VyJytjYXNlbnVtLGNoYXQpPT0nLTEnKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgRXZlbnQuZmlyZSgnY2hhdC1ib3guc2hvdycsIHRoaXMubm90aWZbMF0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJy5qcy1hdXRvLXNpemUnKS50ZXh0YXJlYUF1dG9TaXplKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCA1MDAwKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0RnJpZW5kcygpO1xyXG4gICAgICAgIHRoaXMuZ2V0U3VwcG9ydHMoKTtcclxuICAgIH0sXHJcblxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICB0aGlzLiR1c2Vyc0NvbnRhaW5lciA9ICQodGhpcy4kZWwpLmZpbmQoJy51c2VyLWxpc3QnKTtcclxuICAgICAgICB0aGlzLiR1c2Vyc0NvbnRhaW5lci5wZXJmZWN0U2Nyb2xsYmFyKCk7XHJcbiAgICB9XHJcbn0pO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvb2ZmY2FudmFzLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==