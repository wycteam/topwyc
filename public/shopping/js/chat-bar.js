/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 417);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ChatBox_vue__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ChatBox_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__ChatBox_vue__);


new Vue({
    el: '#chat-bar',

    data: {
        users: [],
        notifications: []
    },

    methods: {
        showChatBox: function showChatBox(user) {
            if (!!~this.users.indexOf(user)) {
                return;
            }

            this.users.push(user);
        },
        closeChatBox: function closeChatBox(user) {
            var index = this.users.findIndex(function (el) {
                return el.id === user.id;
            });

            this.users.splice(index, 1);
        },
        fireNewMessage: function fireNewMessage(data) {
            data.message.user = this.users[0];
            Event.fire('chat-room-' + data.message.chat_room_id, data.message);
        },
        getNotifications: function getNotifications() {
            var _this = this;

            return axiosAPIv1.get('/messages/notifications2').then(function (result) {
                _this.notifications = result.data;
            });
        },
        markAllAsSeen: function markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (!notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });
            $(".cntmsg").html('');
            return axiosAPIv1.post('/messages/mark-all-as-seen2');
        }
    },

    components: {
        ChatBox: __WEBPACK_IMPORTED_MODULE_0__ChatBox_vue___default.a
    },

    created: function created() {
        this.getNotifications();
        Event.listen('chat-box.show', this.showChatBox);
        Event.listen('chat-box.close', this.closeChatBox);
        Event.listen('new-chat-message', this.fireNewMessage);
    }
});

setTimeout(function () {
    $('.js-auto-size').textareaAutoSize();
}, 5000);

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['user'],

    data: function data() {
        return {
            messages: [],
            newMessage: null,
            newMessage2: null,
            currentPage: 0,
            chatRoomId: null,
            $chatBox: null,
            $conversation: null,
            language: [],
            issue: null,
            closed: null,
            isblock: null,
            height: 'issue-height',
            form: new Form({
                case_solved: null,
                rate_agent: null,
                comments: null
            }, { baseURL: 'http://' + Laravel.base_api_url })
        };
    },


    methods: {
        onInfinite: function onInfinite() {
            if (!this.$chatBox.is(':visible')) {
                return this.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
            }

            this.getMessages();
            setTimeout(function () {
                $(".panel-chat-box [data-toggle='tooltip']").tooltip();
                $(".panel-chat-box [data-toggle='tooltip']").tooltip('fixTitle');
            }, 300);
        },
        getMessages: function getMessages() {
            var _this = this;

            var prevScrollHeight = this.$conversation.prop('scrollHeight');

            if (this.user.hasOwnProperty('chat_room')) {

                return axiosAPIv1.get('/messages/chat-room/' + this.user.chat_room_id, {
                    params: {
                        page: ++this.currentPage
                    }
                }).then(function (result) {
                    var data = result.data.data;

                    if (data && data.length) {
                        data.sort(function (a, b) {
                            return a.id - b.id;
                        });

                        _this.messages = data.concat(_this.messages);
                        _this.currentPage = result.data.current_page;

                        setTimeout(function () {
                            if (this.currentPage === 1) {
                                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
                            } else {
                                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight') - prevScrollHeight);
                            }

                            this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
                        }.bind(_this), 1);
                    } else {
                        _this.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                    }
                });
            } else {

                return axiosAPIv1.get('/messages/user/' + this.user.id, {
                    params: {
                        page: ++this.currentPage
                    }
                }).then(function (result) {
                    var data = result.data.data;

                    if (data && data.length) {
                        data.sort(function (a, b) {
                            return a.id - b.id;
                        });

                        _this.messages = data.concat(_this.messages);
                        _this.currentPage = result.data.current_page;
                        if (!_this.chatRoomId) {
                            _this.chatRoomId = data[0].chat_room_id;
                            _this.listen();
                        }

                        setTimeout(function () {
                            if (this.currentPage === 1) {
                                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
                            } else {
                                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight') - prevScrollHeight);
                            }

                            this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
                        }.bind(_this), 1);
                    } else {
                        _this.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                    }
                });
            }
        },
        send: function send() {
            var _this2 = this;

            if (!this.newMessage) {
                return;
            }
            //if user sends spaces only
            if ($.trim(this.newMessage) == '') {
                $('#id' + this.userId).val('');
                return;
            }
            this.messages.push({
                user_id: window.Laravel.user.id,
                body: this.newMessage
            });

            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));

                this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
            }.bind(this), 1);

            if (this.user.hasOwnProperty('chat_room')) {

                axiosAPIv1.post('/messages', {
                    to_chat_room: this.user.chat_room_id,
                    body: this.newMessage
                }).then(function (result) {
                    if (!_this2.chatRoomId) {
                        _this2.chatRoomId = result.data.data.chat_room_id;
                        _this2.listen();
                    }
                });
            } else {

                axiosAPIv1.post('/messages', {
                    to_user: this.user.id,
                    body: this.newMessage
                }).then(function (result) {
                    if (!_this2.chatRoomId) {
                        _this2.chatRoomId = result.data.data.chat_room_id;
                        _this2.listen();
                    }
                });
            }

            this.newMessage = null;
        },
        close: function close() {
            Event.fire('chat-box.close', this.user);
        },
        viewChatRoom: function viewChatRoom() {
            window.location.href = '/messages#/cr/' + this.user.chat_room.id;
        },
        viewChatUser: function viewChatUser() {
            window.location.href = '/messages#/u/' + this.user.id;
        },
        toggleVisibility: function toggleVisibility() {
            var _this3 = this;

            this.$chatBox.slideToggle(300, function () {
                if (_this3.$chatBox.is(':visible')) {
                    _this3.$refs.infiniteLoading.$emit('$InfiniteLoading:reset');
                } else {
                    _this3.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                }
            });
        },
        isMe: function isMe(message) {
            return window.Laravel.user.id == message.user_id;
        },
        scrollToBottom: function scrollToBottom() {
            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
            }.bind(this), 1);
        },
        listen: function listen() {
            var _this4 = this;

            Event.listen('chat-room-' + this.chatRoomId, function (message) {

                if (_this4.user.hasOwnProperty('chat_room') && message.user.hasOwnProperty('user')) {
                    _this4.newMessage2 = { "body": message.body,
                        "user": { "avatar": message.user.user.avatar,
                            "full_name": message.user.user.full_name,
                            "nick_name": message.user.user.nick_name,
                            "first_name": message.user.user.first_name }
                    };
                    _this4.messages.push(_this4.newMessage2);
                } else _this4.messages.push(message);

                // $('.chat-avatar').each(function(){
                //     $(this).attr('title',$(this).data('temp-title'));
                // })
                // $("[data-toggle='title']").tooltip('fixTitle');
                _this4.scrollToBottom();
                // var audio = new Audio('/sound/wycnotif.mp3');
                // audio.play();
            });
        },
        markAllAsSeen: function markAllAsSeen() {
            this.$parent.markAllAsSeen();
        },
        saveIssueFeedback: function saveIssueFeedback() {
            var _this5 = this;

            this.form.submit('post', '/v1/saveIssueFeedback/' + this.user.chat_room.name).then(function (result) {
                toastr.success(_this5.language['feedback'] + ' :)');
                _this5.issue = 0;
                _this5.closed = 1;
            });
        }
    },

    computed: {
        fullName: function fullName() {
            if (this.user.hasOwnProperty('chat_room')) return this.user.chat_room.name;else return this.user.first_name + ' ' + this.user.last_name;
        },
        userId: function userId() {
            if (this.user.hasOwnProperty('chat_room')) return this.user.chat_room.name;else return this.user.id;
        },
        cr_check: function cr_check() {
            if (this.user.hasOwnProperty('chat_room')) return true;else return false;
        }
    },

    components: {
        InfiniteLoading: __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default.a
    },

    created: function created() {
        var _this6 = this;

        function getTranslation() {
            return axios.get('/translate/chat-box');
        }

        axios.all([getTranslation()]).then(axios.spread(function (language) {

            _this6.language = language.data.data;
        })).catch($.noop);

        if (this.user.hasOwnProperty('chat_room')) {
            axiosAPIv1.get('/getIssueStatus/' + this.user.chat_room.name).then(function (result) {
                if (result.data[0].status == "closed" && result.data[0].case_solved == "") {
                    _this6.issue = 1;
                    _this6.closed = 0;
                } else {
                    _this6.issue = 0;
                    if (result.data[0].status == "open") _this6.closed = 0;else _this6.closed = 1;
                }
            });
        } else {
            this.issue = 0;
            this.closed = 0;
        }

        if (!this.user.hasOwnProperty('chat_room')) {
            axiosAPIv1.get('/users/' + this.user.id + '?include=relationship').then(function (result) {
                _this6.isblock = result.data.friend_status;
            });
        }

        setTimeout(function () {
            $('.alert-reminder').hide();
        }, 5000);
    },
    mounted: function mounted() {
        var $el = $(this.$el);

        this.$chatBox = $el.find('.chat-box');
        this.$conversation = $el.find('.panel-body');

        setTimeout(function () {
            // $("[data-toggle='tooltip']").tooltip();
            $(".panel-chat-box [data-toggle='tooltip']").tooltip();

            //chatbox sticky alert message
            $('.alert-grey').each(function () {
                var $that = $(this),
                    $page = $that.closest('.panel-body');
                $page.scroll(function () {
                    $that.css({ top: this.scrollTop });
                });
            });
        }, 1000);
    }
});

/***/ }),

/***/ 323:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(258),
  /* template */
  __webpack_require__(400),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\ChatBox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ChatBox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bef63e74", Component.options)
  } else {
    hotAPI.reload("data-v-bef63e74", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 400:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "panel panel-chat-box",
    attrs: {
      "id": 'user' + _vm.userId
    }
  }, [_c('div', {
    staticClass: "panel-heading",
    on: {
      "click": _vm.toggleVisibility
    }
  }, [_vm._v("\r\n    " + _vm._s(_vm.fullName) + "\r\n    "), _c('span', {
    staticClass: "pull-right",
    attrs: {
      "title": "Close",
      "data-toggle": "tooltip",
      "data-placement": "top"
    },
    on: {
      "click": function($event) {
        $event.stopPropagation();
        _vm.close($event)
      }
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("clear")])]), _vm._v(" "), (_vm.cr_check) ? _c('span', {
    staticClass: "pull-right m-r-1 p-r-1",
    attrs: {
      "title": "Open in Messenger",
      "data-toggle": "tooltip",
      "data-placement": "top"
    },
    on: {
      "click": _vm.viewChatRoom
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("aspect_ratio")])]) : _c('span', {
    staticClass: "pull-right m-r-1 p-r-1",
    on: {
      "click": _vm.viewChatUser
    }
  }, [_c('i', {
    staticClass: "material-icons",
    attrs: {
      "title": "Open in Messenger",
      "data-toggle": "tooltip",
      "data-placement": "top"
    }
  }, [_vm._v("aspect_ratio")])])]), _vm._v(" "), _c('div', {
    staticClass: "chat-box"
  }, [_c('div', {
    staticClass: "panel-body",
    class: [_vm.issue == '1' || _vm.closed == '1' ? _vm.height : ''],
    attrs: {
      "id": 'panel-body-' + _vm.userId
    }
  }, [_c('div', {
    staticClass: "row alert-reminder"
  }, [_c('div', {
    staticClass: "abs alert alert-grey alert-dismissable"
  }, [_c('a', {
    staticClass: "close",
    attrs: {
      "href": "#",
      "data-dismiss": "alert",
      "aria-label": "close"
    }
  }, [_vm._v("×")]), _vm._v(" "), _c('strong', [_vm._v(_vm._s(_vm.language["reminder"]) + ":")]), _vm._v(" " + _vm._s(_vm.language['alert-msg']) + "\r\n        ")])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.issue == "0"),
      expression: "issue==\"0\""
    }]
  }, [_c('infinite-loading', {
    ref: "infiniteLoading",
    attrs: {
      "on-infinite": _vm.onInfinite,
      "direction": "top",
      "distance": 0
    }
  }), _vm._v(" "), _c('ul', _vm._l((_vm.messages), function(message) {
    return _c('li', {
      class: {
        me: _vm.isMe(message)
      }
    }, [(!_vm.isMe(message)) ? _c('div', {
      staticClass: "someoneMessage"
    }, [_c('div', {
      staticClass: "col-sm-2 col-md-2"
    }, [_c('img', {
      staticClass: "img-responsive chat-avatar",
      attrs: {
        "src": message.user.avatar,
        "data-toggle": "tooltip",
        "data-placement": "left",
        "data-container": "#chat-bar",
        "data-temp-title": message.user.full_name,
        "title": message.user.full_name + ' ' + message.created_at
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "col-sm-9 col-md-9"
    }, [(message.user.nick_name !== null) ? _c('span', {
      staticClass: "username"
    }, [_vm._v(_vm._s(message.user.nick_name))]) : _c('span', {
      staticClass: "username"
    }, [_vm._v(_vm._s(message.user.first_name))]), _vm._v(" "), _c('br'), _vm._v(" "), _c('span', {
      staticClass: "message",
      attrs: {
        "data-toggle": "tooltip",
        "data-placement": "left",
        "data-container": "#chat-bar",
        "title": message.user.full_name + ' ' + message.created_at
      }
    }, [_vm._v(_vm._s(message.body))])])]) : _vm._e(), _vm._v(" "), (_vm.isMe(message)) ? _c('span', {
      staticClass: "message",
      attrs: {
        "data-toggle": "tooltip",
        "data-placement": "left",
        "data-container": "#chat-bar",
        "title": message.created_at
      }
    }, [_vm._v("\r\n                        " + _vm._s(message.body) + "\r\n                    ")]) : _vm._e()])
  }))], 1), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.issue == "1"),
      expression: "issue==\"1\""
    }]
  }, [_c('div', {
    staticClass: "thanks"
  }, [_vm._v(_vm._s(_vm.language["thank-you"]))]), _vm._v(" "), _c('br'), _vm._v(" "), _c('div', [_vm._v(_vm._s(_vm.language["case-solved"]))]), _vm._v(" "), _c('div', {
    staticClass: "cd-filter-block"
  }, [_c('ul', {
    staticClass: "cd-filter-content cd-filters list"
  }, [_c('li', {
    staticClass: "margin"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.form.case_solved),
      expression: "form.case_solved"
    }],
    staticClass: "filter",
    attrs: {
      "data-filter": ".radio1",
      "type": "radio",
      "value": "yes",
      "name": "case_solved",
      "id": "yes"
    },
    domProps: {
      "checked": _vm._q(_vm.form.case_solved, "yes")
    },
    on: {
      "__c": function($event) {
        _vm.form.case_solved = "yes"
      }
    }
  }), _vm._v(" "), _c('label', {
    staticClass: "radio-label",
    attrs: {
      "for": "radio1"
    }
  }, [_vm._v(_vm._s(_vm.language["yes"]))])]), _vm._v(" "), _c('li', {
    staticClass: "margin"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.form.case_solved),
      expression: "form.case_solved"
    }],
    staticClass: "filter",
    attrs: {
      "data-filter": ".radio2",
      "type": "radio",
      "value": "no",
      "name": "case_solved",
      "id": "no"
    },
    domProps: {
      "checked": _vm._q(_vm.form.case_solved, "no")
    },
    on: {
      "__c": function($event) {
        _vm.form.case_solved = "no"
      }
    }
  }), _vm._v(" "), _c('label', {
    staticClass: "radio-label",
    attrs: {
      "for": "radio2"
    }
  }, [_vm._v(_vm._s(_vm.language["no"]))])]), _vm._v(" "), (_vm.form.errors.has('case_solved')) ? _c('span', {
    staticClass: "help-block error"
  }, [_vm._v(_vm._s(_vm.language["please"]))]) : _vm._e()])]), _vm._v(" "), _c('div', [_vm._v(_vm._s(_vm.language["rate-agent"]))]), _vm._v(" "), _c('div', {
    staticClass: "cd-filter-block"
  }, [_c('ul', {
    staticClass: "cd-filter-content cd-filters list"
  }, [_c('li', {
    staticClass: "margin"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.form.rate_agent),
      expression: "form.rate_agent"
    }],
    staticClass: "filter",
    attrs: {
      "data-filter": ".radio1",
      "type": "radio",
      "value": "good",
      "name": "rate_agent",
      "id": "good"
    },
    domProps: {
      "checked": _vm._q(_vm.form.rate_agent, "good")
    },
    on: {
      "__c": function($event) {
        _vm.form.rate_agent = "good"
      }
    }
  }), _vm._v(" "), _c('label', {
    staticClass: "radio-label",
    attrs: {
      "for": "radio1"
    }
  }, [_vm._v(_vm._s(_vm.language["good"]))])]), _vm._v(" "), _c('li', {
    staticClass: "margin"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.form.rate_agent),
      expression: "form.rate_agent"
    }],
    staticClass: "filter",
    attrs: {
      "data-filter": ".radio2",
      "type": "radio",
      "value": "average",
      "name": "rate_agent",
      "id": "average"
    },
    domProps: {
      "checked": _vm._q(_vm.form.rate_agent, "average")
    },
    on: {
      "__c": function($event) {
        _vm.form.rate_agent = "average"
      }
    }
  }), _vm._v(" "), _c('label', {
    staticClass: "radio-label",
    attrs: {
      "for": "radio2"
    }
  }, [_vm._v(_vm._s(_vm.language["average"]))])]), _vm._v(" "), _c('li', {
    staticClass: "margin"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.form.rate_agent),
      expression: "form.rate_agent"
    }],
    staticClass: "filter",
    attrs: {
      "data-filter": ".radio3",
      "type": "radio",
      "value": "bad",
      "name": "rate_agent",
      "id": "bad"
    },
    domProps: {
      "checked": _vm._q(_vm.form.rate_agent, "bad")
    },
    on: {
      "__c": function($event) {
        _vm.form.rate_agent = "bad"
      }
    }
  }), _vm._v(" "), _c('label', {
    staticClass: "radio-label",
    attrs: {
      "for": "radio3"
    }
  }, [_vm._v(_vm._s(_vm.language["bad"]))])]), _vm._v(" "), (_vm.form.errors.has('case_solved')) ? _c('span', {
    staticClass: "help-block error"
  }, [_vm._v(_vm._s(_vm.language["please"]))]) : _vm._e()])]), _vm._v(" "), _c('div', [_vm._v(_vm._s(_vm.language["comments"]))]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.form.comments),
      expression: "form.comments"
    }],
    attrs: {
      "id": "add-comment",
      "rows": 4,
      "name": "comments"
    },
    domProps: {
      "value": (_vm.form.comments)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.form.comments = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    staticClass: "btn btn-light-green",
    attrs: {
      "id": "submit",
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.saveIssueFeedback()
      }
    }
  }, [_vm._v(_vm._s(_vm.language["submit"]))])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.closed == "1"),
      expression: "closed==\"1\""
    }],
    staticClass: "thanks"
  }, [_vm._v(_vm._s(_vm.language["issue"]) + " :)")])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.issue == "0" && _vm.closed == "0"),
      expression: "issue==\"0\" && closed==\"0\""
    }],
    staticClass: "panel-footer"
  }, [_c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newMessage),
      expression: "newMessage"
    }],
    staticClass: "js-auto-size",
    attrs: {
      "data-chat": 'panel-body-' + _vm.userId,
      "id": 'id' + _vm.userId,
      "rows": 1,
      "placeholder": _vm.language['type-a-message'],
      "disabled": _vm.isblock == 'Blocked'
    },
    domProps: {
      "value": (_vm.newMessage)
    },
    on: {
      "keydown": function($event) {
        if (!('button' in $event) && _vm._k($event.keyCode, "enter", 13)) { return null; }
        $event.preventDefault();
        _vm.send($event)
      },
      "focus": _vm.markAllAsSeen,
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.newMessage = $event.target.value
      }
    }
  })])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-bef63e74", module.exports)
  }
}

/***/ }),

/***/ 417:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(167);


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

!function(p,x){ true?module.exports=x():"function"==typeof define&&define.amd?define([],x):"object"==typeof exports?exports.VueInfiniteLoading=x():p.VueInfiniteLoading=x()}(this,function(){return function(p){function x(a){if(t[a])return t[a].exports;var e=t[a]={exports:{},id:a,loaded:!1};return p[a].call(e.exports,e,e.exports,x),e.loaded=!0,e.exports}var t={};return x.m=p,x.c=t,x.p="/",x(0)}([function(p,x,t){"use strict";function a(p){return p&&p.__esModule?p:{default:p}}Object.defineProperty(x,"__esModule",{value:!0});var e=t(4),o=a(e);x.default=o.default,"undefined"!=typeof window&&window.Vue&&window.Vue.component("infinite-loading",o.default)},function(p,x){"use strict";function t(p){return"BODY"===p.tagName?window:["scroll","auto"].indexOf(getComputedStyle(p).overflowY)>-1?p:p.hasAttribute("infinite-wrapper")||p.hasAttribute("data-infinite-wrapper")?p:t(p.parentNode)}function a(p,x,t){var a=void 0;if("top"===t)a=isNaN(p.scrollTop)?p.pageYOffset:p.scrollTop;else{var e=x.getBoundingClientRect().top,o=p===window?window.innerHeight:p.getBoundingClientRect().bottom;a=e-o}return a}Object.defineProperty(x,"__esModule",{value:!0});var e={bubbles:"loading-bubbles",circles:"loading-circles",default:"loading-default",spiral:"loading-spiral",waveDots:"loading-wave-dots"};x.default={data:function(){return{scrollParent:null,scrollHandler:null,isLoading:!1,isComplete:!1,isFirstLoad:!0}},computed:{spinnerType:function(){return e[this.spinner]||e.default}},props:{distance:{type:Number,default:100},onInfinite:Function,spinner:String,direction:{type:String,default:"bottom"}},mounted:function(){var p=this;this.scrollParent=t(this.$el),this.scrollHandler=function(){this.isLoading||this.attemptLoad()}.bind(this),setTimeout(this.scrollHandler,1),this.scrollParent.addEventListener("scroll",this.scrollHandler),this.$on("$InfiniteLoading:loaded",function(){p.isFirstLoad=!1,p.isLoading&&p.$nextTick(p.attemptLoad)}),this.$on("$InfiniteLoading:complete",function(){p.isLoading=!1,p.isComplete=!0,p.scrollParent.removeEventListener("scroll",p.scrollHandler)}),this.$on("$InfiniteLoading:reset",function(){p.isLoading=!1,p.isComplete=!1,p.isFirstLoad=!0,p.scrollParent.addEventListener("scroll",p.scrollHandler),setTimeout(p.scrollHandler,1)})},deactivated:function(){this.isLoading=!1,this.scrollParent.removeEventListener("scroll",this.scrollHandler)},activated:function(){this.scrollParent.addEventListener("scroll",this.scrollHandler)},methods:{attemptLoad:function(){var p=a(this.scrollParent,this.$el,this.direction);!this.isComplete&&p<=this.distance?(this.isLoading=!0,this.onInfinite.call()):this.isLoading=!1}},destroyed:function(){this.isComplete||this.scrollParent.removeEventListener("scroll",this.scrollHandler)}}},function(p,x,t){x=p.exports=t(3)(),x.push([p.id,'.loading-wave-dots[data-v-50793f02]{position:relative}.loading-wave-dots[data-v-50793f02]:before{content:"";position:absolute;top:50%;left:50%;margin-left:-4px;margin-top:-4px;width:8px;height:8px;background-color:#bbb;border-radius:50%;-webkit-animation:linear loading-wave-dots 2.8s infinite;animation:linear loading-wave-dots 2.8s infinite}@-webkit-keyframes loading-wave-dots{0%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}5%{box-shadow:-32px -4px 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}10%{box-shadow:-32px -6px 0 #999,-16px -4px 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}15%{box-shadow:-32px 2px 0 #bbb,-16px -2px 0 #999,16px 4px 0 #bbb,32px 4px 0 #bbb;-webkit-transform:translateY(-4px);transform:translateY(-4px);background-color:#bbb}20%{box-shadow:-32px 6px 0 #bbb,-16px 4px 0 #bbb,16px 2px 0 #bbb,32px 6px 0 #bbb;-webkit-transform:translateY(-6px);transform:translateY(-6px);background-color:#999}25%{box-shadow:-32px 2px 0 #bbb,-16px 2px 0 #bbb,16px -4px 0 #999,32px -2px 0 #bbb;-webkit-transform:translateY(-2px);transform:translateY(-2px);background-color:#bbb}30%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px -2px 0 #bbb,32px -6px 0 #999;-webkit-transform:translateY(0);transform:translateY(0)}35%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px -2px 0 #bbb}40%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}to{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}}@keyframes loading-wave-dots{0%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}5%{box-shadow:-32px -4px 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}10%{box-shadow:-32px -6px 0 #999,-16px -4px 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}15%{box-shadow:-32px 2px 0 #bbb,-16px -2px 0 #999,16px 4px 0 #bbb,32px 4px 0 #bbb;-webkit-transform:translateY(-4px);transform:translateY(-4px);background-color:#bbb}20%{box-shadow:-32px 6px 0 #bbb,-16px 4px 0 #bbb,16px 2px 0 #bbb,32px 6px 0 #bbb;-webkit-transform:translateY(-6px);transform:translateY(-6px);background-color:#999}25%{box-shadow:-32px 2px 0 #bbb,-16px 2px 0 #bbb,16px -4px 0 #999,32px -2px 0 #bbb;-webkit-transform:translateY(-2px);transform:translateY(-2px);background-color:#bbb}30%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px -2px 0 #bbb,32px -6px 0 #999;-webkit-transform:translateY(0);transform:translateY(0)}35%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px -2px 0 #bbb}40%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}to{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}}.loading-circles[data-v-50793f02]{position:relative}.loading-circles[data-v-50793f02]:before{content:"";position:absolute;left:50%;top:50%;margin-top:-2.5px;margin-left:-2.5px;width:5px;height:5px;border-radius:50%;-webkit-animation:linear loading-circles .75s infinite;animation:linear loading-circles .75s infinite}@-webkit-keyframes loading-circles{0%{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}12.5%{box-shadow:0 -12px 0 #dfdfdf,8.52px -8.52px 0 #505050,12px 0 0 #646464,8.52px 8.52px 0 #797979,0 12px 0 #8d8d8d,-8.52px 8.52px 0 #a2a2a2,-12px 0 0 #b6b6b6,-8.52px -8.52px 0 #cacaca}25%{box-shadow:0 -12px 0 #cacaca,8.52px -8.52px 0 #dfdfdf,12px 0 0 #505050,8.52px 8.52px 0 #646464,0 12px 0 #797979,-8.52px 8.52px 0 #8d8d8d,-12px 0 0 #a2a2a2,-8.52px -8.52px 0 #b6b6b6}37.5%{box-shadow:0 -12px 0 #b6b6b6,8.52px -8.52px 0 #cacaca,12px 0 0 #dfdfdf,8.52px 8.52px 0 #505050,0 12px 0 #646464,-8.52px 8.52px 0 #797979,-12px 0 0 #8d8d8d,-8.52px -8.52px 0 #a2a2a2}50%{box-shadow:0 -12px 0 #a2a2a2,8.52px -8.52px 0 #b6b6b6,12px 0 0 #cacaca,8.52px 8.52px 0 #dfdfdf,0 12px 0 #505050,-8.52px 8.52px 0 #646464,-12px 0 0 #797979,-8.52px -8.52px 0 #8d8d8d}62.5%{box-shadow:0 -12px 0 #8d8d8d,8.52px -8.52px 0 #a2a2a2,12px 0 0 #b6b6b6,8.52px 8.52px 0 #cacaca,0 12px 0 #dfdfdf,-8.52px 8.52px 0 #505050,-12px 0 0 #646464,-8.52px -8.52px 0 #797979}75%{box-shadow:0 -12px 0 #797979,8.52px -8.52px 0 #8d8d8d,12px 0 0 #a2a2a2,8.52px 8.52px 0 #b6b6b6,0 12px 0 #cacaca,-8.52px 8.52px 0 #dfdfdf,-12px 0 0 #505050,-8.52px -8.52px 0 #646464}87.5%{box-shadow:0 -12px 0 #646464,8.52px -8.52px 0 #797979,12px 0 0 #8d8d8d,8.52px 8.52px 0 #a2a2a2,0 12px 0 #b6b6b6,-8.52px 8.52px 0 #cacaca,-12px 0 0 #dfdfdf,-8.52px -8.52px 0 #505050}to{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}}@keyframes loading-circles{0%{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}12.5%{box-shadow:0 -12px 0 #dfdfdf,8.52px -8.52px 0 #505050,12px 0 0 #646464,8.52px 8.52px 0 #797979,0 12px 0 #8d8d8d,-8.52px 8.52px 0 #a2a2a2,-12px 0 0 #b6b6b6,-8.52px -8.52px 0 #cacaca}25%{box-shadow:0 -12px 0 #cacaca,8.52px -8.52px 0 #dfdfdf,12px 0 0 #505050,8.52px 8.52px 0 #646464,0 12px 0 #797979,-8.52px 8.52px 0 #8d8d8d,-12px 0 0 #a2a2a2,-8.52px -8.52px 0 #b6b6b6}37.5%{box-shadow:0 -12px 0 #b6b6b6,8.52px -8.52px 0 #cacaca,12px 0 0 #dfdfdf,8.52px 8.52px 0 #505050,0 12px 0 #646464,-8.52px 8.52px 0 #797979,-12px 0 0 #8d8d8d,-8.52px -8.52px 0 #a2a2a2}50%{box-shadow:0 -12px 0 #a2a2a2,8.52px -8.52px 0 #b6b6b6,12px 0 0 #cacaca,8.52px 8.52px 0 #dfdfdf,0 12px 0 #505050,-8.52px 8.52px 0 #646464,-12px 0 0 #797979,-8.52px -8.52px 0 #8d8d8d}62.5%{box-shadow:0 -12px 0 #8d8d8d,8.52px -8.52px 0 #a2a2a2,12px 0 0 #b6b6b6,8.52px 8.52px 0 #cacaca,0 12px 0 #dfdfdf,-8.52px 8.52px 0 #505050,-12px 0 0 #646464,-8.52px -8.52px 0 #797979}75%{box-shadow:0 -12px 0 #797979,8.52px -8.52px 0 #8d8d8d,12px 0 0 #a2a2a2,8.52px 8.52px 0 #b6b6b6,0 12px 0 #cacaca,-8.52px 8.52px 0 #dfdfdf,-12px 0 0 #505050,-8.52px -8.52px 0 #646464}87.5%{box-shadow:0 -12px 0 #646464,8.52px -8.52px 0 #797979,12px 0 0 #8d8d8d,8.52px 8.52px 0 #a2a2a2,0 12px 0 #b6b6b6,-8.52px 8.52px 0 #cacaca,-12px 0 0 #dfdfdf,-8.52px -8.52px 0 #505050}to{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}}.loading-bubbles[data-v-50793f02]{position:relative}.loading-bubbles[data-v-50793f02]:before{content:"";position:absolute;left:50%;top:50%;margin-top:-.5px;margin-left:-.5px;width:1px;height:1px;border-radius:50%;-webkit-animation:linear loading-bubbles .85s infinite;animation:linear loading-bubbles .85s infinite}@-webkit-keyframes loading-bubbles{0%{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}12.5%{box-shadow:0 -12px 0 3.2px #666,8.52px -8.52px 0 .4px #666,12px 0 0 .8px #666,8.52px 8.52px 0 1.2px #666,0 12px 0 1.6px #666,-8.52px 8.52px 0 2px #666,-12px 0 0 2.4px #666,-8.52px -8.52px 0 2.8px #666}25%{box-shadow:0 -12px 0 2.8px #666,8.52px -8.52px 0 3.2px #666,12px 0 0 .4px #666,8.52px 8.52px 0 .8px #666,0 12px 0 1.2px #666,-8.52px 8.52px 0 1.6px #666,-12px 0 0 2px #666,-8.52px -8.52px 0 2.4px #666}37.5%{box-shadow:0 -12px 0 2.4px #666,8.52px -8.52px 0 2.8px #666,12px 0 0 3.2px #666,8.52px 8.52px 0 .4px #666,0 12px 0 .8px #666,-8.52px 8.52px 0 1.2px #666,-12px 0 0 1.6px #666,-8.52px -8.52px 0 2px #666}50%{box-shadow:0 -12px 0 2px #666,8.52px -8.52px 0 2.4px #666,12px 0 0 2.8px #666,8.52px 8.52px 0 3.2px #666,0 12px 0 .4px #666,-8.52px 8.52px 0 .8px #666,-12px 0 0 1.2px #666,-8.52px -8.52px 0 1.6px #666}62.5%{box-shadow:0 -12px 0 1.6px #666,8.52px -8.52px 0 2px #666,12px 0 0 2.4px #666,8.52px 8.52px 0 2.8px #666,0 12px 0 3.2px #666,-8.52px 8.52px 0 .4px #666,-12px 0 0 .8px #666,-8.52px -8.52px 0 1.2px #666}75%{box-shadow:0 -12px 0 1.2px #666,8.52px -8.52px 0 1.6px #666,12px 0 0 2px #666,8.52px 8.52px 0 2.4px #666,0 12px 0 2.8px #666,-8.52px 8.52px 0 3.2px #666,-12px 0 0 .4px #666,-8.52px -8.52px 0 .8px #666}87.5%{box-shadow:0 -12px 0 .8px #666,8.52px -8.52px 0 1.2px #666,12px 0 0 1.6px #666,8.52px 8.52px 0 2px #666,0 12px 0 2.4px #666,-8.52px 8.52px 0 2.8px #666,-12px 0 0 3.2px #666,-8.52px -8.52px 0 .4px #666}to{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}}@keyframes loading-bubbles{0%{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}12.5%{box-shadow:0 -12px 0 3.2px #666,8.52px -8.52px 0 .4px #666,12px 0 0 .8px #666,8.52px 8.52px 0 1.2px #666,0 12px 0 1.6px #666,-8.52px 8.52px 0 2px #666,-12px 0 0 2.4px #666,-8.52px -8.52px 0 2.8px #666}25%{box-shadow:0 -12px 0 2.8px #666,8.52px -8.52px 0 3.2px #666,12px 0 0 .4px #666,8.52px 8.52px 0 .8px #666,0 12px 0 1.2px #666,-8.52px 8.52px 0 1.6px #666,-12px 0 0 2px #666,-8.52px -8.52px 0 2.4px #666}37.5%{box-shadow:0 -12px 0 2.4px #666,8.52px -8.52px 0 2.8px #666,12px 0 0 3.2px #666,8.52px 8.52px 0 .4px #666,0 12px 0 .8px #666,-8.52px 8.52px 0 1.2px #666,-12px 0 0 1.6px #666,-8.52px -8.52px 0 2px #666}50%{box-shadow:0 -12px 0 2px #666,8.52px -8.52px 0 2.4px #666,12px 0 0 2.8px #666,8.52px 8.52px 0 3.2px #666,0 12px 0 .4px #666,-8.52px 8.52px 0 .8px #666,-12px 0 0 1.2px #666,-8.52px -8.52px 0 1.6px #666}62.5%{box-shadow:0 -12px 0 1.6px #666,8.52px -8.52px 0 2px #666,12px 0 0 2.4px #666,8.52px 8.52px 0 2.8px #666,0 12px 0 3.2px #666,-8.52px 8.52px 0 .4px #666,-12px 0 0 .8px #666,-8.52px -8.52px 0 1.2px #666}75%{box-shadow:0 -12px 0 1.2px #666,8.52px -8.52px 0 1.6px #666,12px 0 0 2px #666,8.52px 8.52px 0 2.4px #666,0 12px 0 2.8px #666,-8.52px 8.52px 0 3.2px #666,-12px 0 0 .4px #666,-8.52px -8.52px 0 .8px #666}87.5%{box-shadow:0 -12px 0 .8px #666,8.52px -8.52px 0 1.2px #666,12px 0 0 1.6px #666,8.52px 8.52px 0 2px #666,0 12px 0 2.4px #666,-8.52px 8.52px 0 2.8px #666,-12px 0 0 3.2px #666,-8.52px -8.52px 0 .4px #666}to{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}}.loading-default[data-v-50793f02]{position:relative;border:1px solid #999;-webkit-animation:ease loading-rotating 1.5s infinite;animation:ease loading-rotating 1.5s infinite}.loading-default[data-v-50793f02]:before{content:"";position:absolute;display:block;top:0;left:50%;margin-top:-3px;margin-left:-3px;width:6px;height:6px;background-color:#999;border-radius:50%}.loading-spiral[data-v-50793f02]{border:2px solid #777;border-right-color:transparent;-webkit-animation:linear loading-rotating .85s infinite;animation:linear loading-rotating .85s infinite}@-webkit-keyframes loading-rotating{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes loading-rotating{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}.infinite-loading-container[data-v-50793f02]{clear:both;text-align:center}.infinite-loading-container [class^=loading-][data-v-50793f02]{display:inline-block;margin:15px 0;width:28px;height:28px;font-size:28px;line-height:28px;border-radius:50%}.infinite-status-prompt[data-v-50793f02]{color:#666;font-size:14px;text-align:center;padding:10px 0}',""])},function(p,x){p.exports=function(){var p=[];return p.toString=function(){for(var p=[],x=0;x<this.length;x++){var t=this[x];t[2]?p.push("@media "+t[2]+"{"+t[1]+"}"):p.push(t[1])}return p.join("")},p.i=function(x,t){"string"==typeof x&&(x=[[null,x,""]]);for(var a={},e=0;e<this.length;e++){var o=this[e][0];"number"==typeof o&&(a[o]=!0)}for(e=0;e<x.length;e++){var n=x[e];"number"==typeof n[0]&&a[n[0]]||(t&&!n[2]?n[2]=t:t&&(n[2]="("+n[2]+") and ("+t+")"),p.push(n))}},p}},function(p,x,t){var a,e;t(7),a=t(1);var o=t(5);e=a=a||{},"object"!=typeof a.default&&"function"!=typeof a.default||(e=a=a.default),"function"==typeof e&&(e=e.options),e.render=o.render,e.staticRenderFns=o.staticRenderFns,e._scopeId="data-v-50793f02",p.exports=a},function(p,x){p.exports={render:function(){var p=this,x=p.$createElement,t=p._self._c||x;return t("div",{staticClass:"infinite-loading-container"},[t("div",{directives:[{name:"show",rawName:"v-show",value:p.isLoading,expression:"isLoading"}]},[p._t("spinner",[t("i",{class:p.spinnerType})])],2),p._v(" "),t("div",{directives:[{name:"show",rawName:"v-show",value:!p.isLoading&&p.isComplete&&p.isFirstLoad,expression:"!isLoading && isComplete && isFirstLoad"}],staticClass:"infinite-status-prompt"},[p._t("no-results",[p._v("No results :(")])],2),p._v(" "),t("div",{directives:[{name:"show",rawName:"v-show",value:!p.isLoading&&p.isComplete&&!p.isFirstLoad,expression:"!isLoading && isComplete && !isFirstLoad"}],staticClass:"infinite-status-prompt"},[p._t("no-more",[p._v("No more data :)")])],2)])},staticRenderFns:[]}},function(p,x,t){function a(p,x){for(var t=0;t<p.length;t++){var a=p[t],e=d[a.id];if(e){e.refs++;for(var o=0;o<e.parts.length;o++)e.parts[o](a.parts[o]);for(;o<a.parts.length;o++)e.parts.push(r(a.parts[o],x))}else{for(var n=[],o=0;o<a.parts.length;o++)n.push(r(a.parts[o],x));d[a.id]={id:a.id,refs:1,parts:n}}}}function e(p){for(var x=[],t={},a=0;a<p.length;a++){var e=p[a],o=e[0],n=e[1],i=e[2],r=e[3],s={css:n,media:i,sourceMap:r};t[o]?t[o].parts.push(s):x.push(t[o]={id:o,parts:[s]})}return x}function o(p,x){var t=c(),a=m[m.length-1];if("top"===p.insertAt)a?a.nextSibling?t.insertBefore(x,a.nextSibling):t.appendChild(x):t.insertBefore(x,t.firstChild),m.push(x);else{if("bottom"!==p.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");t.appendChild(x)}}function n(p){p.parentNode.removeChild(p);var x=m.indexOf(p);x>=0&&m.splice(x,1)}function i(p){var x=document.createElement("style");return x.type="text/css",o(p,x),x}function r(p,x){var t,a,e;if(x.singleton){var o=h++;t=u||(u=i(x)),a=s.bind(null,t,o,!1),e=s.bind(null,t,o,!0)}else t=i(x),a=b.bind(null,t),e=function(){n(t)};return a(p),function(x){if(x){if(x.css===p.css&&x.media===p.media&&x.sourceMap===p.sourceMap)return;a(p=x)}else e()}}function s(p,x,t,a){var e=t?"":a.css;if(p.styleSheet)p.styleSheet.cssText=g(x,e);else{var o=document.createTextNode(e),n=p.childNodes;n[x]&&p.removeChild(n[x]),n.length?p.insertBefore(o,n[x]):p.appendChild(o)}}function b(p,x){var t=x.css,a=x.media,e=x.sourceMap;if(a&&p.setAttribute("media",a),e&&(t+="\n/*# sourceURL="+e.sources[0]+" */",t+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(e))))+" */"),p.styleSheet)p.styleSheet.cssText=t;else{for(;p.firstChild;)p.removeChild(p.firstChild);p.appendChild(document.createTextNode(t))}}var d={},l=function(p){var x;return function(){return"undefined"==typeof x&&(x=p.apply(this,arguments)),x}},f=l(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),c=l(function(){return document.head||document.getElementsByTagName("head")[0]}),u=null,h=0,m=[];p.exports=function(p,x){x=x||{},"undefined"==typeof x.singleton&&(x.singleton=f()),"undefined"==typeof x.insertAt&&(x.insertAt="bottom");var t=e(p);return a(t,x),function(p){for(var o=[],n=0;n<t.length;n++){var i=t[n],r=d[i.id];r.refs--,o.push(r)}if(p){var s=e(p);a(s,x)}for(var n=0;n<o.length;n++){var r=o[n];if(0===r.refs){for(var b=0;b<r.parts.length;b++)r.parts[b]();delete d[r.id]}}}};var g=function(){var p=[];return function(x,t){return p[x]=t,p.filter(Boolean).join("\n")}}()},function(p,x,t){var a=t(2);"string"==typeof a&&(a=[[p.id,a,""]]);t(6)(a,{});a.locals&&(p.exports=a.locals)}])});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jaGF0LWJhci5qcyIsIndlYnBhY2s6Ly8vQ2hhdEJveC52dWU/YmY4YiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL0NoYXRCb3gudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvQ2hhdEJveC52dWU/ZWIxYyIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1pbmZpbml0ZS1sb2FkaW5nL2Rpc3QvdnVlLWluZmluaXRlLWxvYWRpbmcuanM/ODJjNSoiXSwibmFtZXMiOlsiVnVlIiwiZWwiLCJkYXRhIiwidXNlcnMiLCJub3RpZmljYXRpb25zIiwibWV0aG9kcyIsInNob3dDaGF0Qm94IiwidXNlciIsImluZGV4T2YiLCJwdXNoIiwiY2xvc2VDaGF0Qm94IiwiaW5kZXgiLCJmaW5kSW5kZXgiLCJpZCIsInNwbGljZSIsImZpcmVOZXdNZXNzYWdlIiwibWVzc2FnZSIsIkV2ZW50IiwiZmlyZSIsImNoYXRfcm9vbV9pZCIsImdldE5vdGlmaWNhdGlvbnMiLCJheGlvc0FQSXYxIiwiZ2V0IiwidGhlbiIsInJlc3VsdCIsIm1hcmtBbGxBc1NlZW4iLCJmb3JFYWNoIiwibm90aWYiLCJzZWVuX2F0IiwiRGF0ZSIsIiQiLCJodG1sIiwicG9zdCIsImNvbXBvbmVudHMiLCJDaGF0Qm94IiwiY3JlYXRlZCIsImxpc3RlbiIsInNldFRpbWVvdXQiLCJ0ZXh0YXJlYUF1dG9TaXplIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDbERBOztBQUVBLElBQUlBLEdBQUosQ0FBUTtBQUNKQyxRQUFJLFdBREE7O0FBR0pDLFVBQU07QUFDRkMsZUFBTyxFQURMO0FBRUZDLHVCQUFlO0FBRmIsS0FIRjs7QUFRSkMsYUFBUztBQUNMQyxtQkFESyx1QkFDT0MsSUFEUCxFQUNhO0FBQ2QsZ0JBQUksQ0FBQyxDQUFDLENBQUMsS0FBS0osS0FBTCxDQUFXSyxPQUFYLENBQW1CRCxJQUFuQixDQUFQLEVBQWlDO0FBQzdCO0FBQ0g7O0FBRUQsaUJBQUtKLEtBQUwsQ0FBV00sSUFBWCxDQUFnQkYsSUFBaEI7QUFDSCxTQVBJO0FBU0xHLG9CQVRLLHdCQVNRSCxJQVRSLEVBU2M7QUFDZixnQkFBSUksUUFBUSxLQUFLUixLQUFMLENBQVdTLFNBQVgsQ0FBcUIsVUFBVVgsRUFBVixFQUFjO0FBQzNDLHVCQUFPQSxHQUFHWSxFQUFILEtBQVVOLEtBQUtNLEVBQXRCO0FBQ0gsYUFGVyxDQUFaOztBQUlBLGlCQUFLVixLQUFMLENBQVdXLE1BQVgsQ0FBa0JILEtBQWxCLEVBQXlCLENBQXpCO0FBQ0gsU0FmSTtBQWlCTEksc0JBakJLLDBCQWlCVWIsSUFqQlYsRUFpQmdCO0FBQ2pCQSxpQkFBS2MsT0FBTCxDQUFhVCxJQUFiLEdBQW9CLEtBQUtKLEtBQUwsQ0FBVyxDQUFYLENBQXBCO0FBQ0FjLGtCQUFNQyxJQUFOLENBQVcsZUFBZWhCLEtBQUtjLE9BQUwsQ0FBYUcsWUFBdkMsRUFBcURqQixLQUFLYyxPQUExRDtBQUNILFNBcEJJO0FBc0JMSSx3QkF0QkssOEJBc0JjO0FBQUE7O0FBQ2YsbUJBQU9DLFdBQVdDLEdBQVgsQ0FBZSwwQkFBZixFQUNGQyxJQURFLENBQ0csa0JBQVU7QUFDWixzQkFBS25CLGFBQUwsR0FBcUJvQixPQUFPdEIsSUFBNUI7QUFDSCxhQUhFLENBQVA7QUFJSCxTQTNCSTtBQTZCTHVCLHFCQTdCSywyQkE2Qlc7QUFDWixpQkFBS3JCLGFBQUwsQ0FBbUJzQixPQUFuQixDQUEyQixVQUFVQyxLQUFWLEVBQWlCO0FBQ3hDLG9CQUFJLENBQUVBLE1BQU1DLE9BQVosRUFBcUI7QUFDakJELDBCQUFNQyxPQUFOLEdBQWdCLElBQUlDLElBQUosRUFBaEI7QUFDSDtBQUNKLGFBSkQ7QUFLQUMsY0FBRSxTQUFGLEVBQWFDLElBQWIsQ0FBa0IsRUFBbEI7QUFDQSxtQkFBT1YsV0FBV1csSUFBWCxDQUFnQiw2QkFBaEIsQ0FBUDtBQUNIO0FBckNJLEtBUkw7O0FBZ0RKQyxnQkFBWTtBQUNSQyxpQkFBQSxvREFBQUE7QUFEUSxLQWhEUjs7QUFvREpDLFdBcERJLHFCQW9ETTtBQUNOLGFBQUtmLGdCQUFMO0FBQ0FILGNBQU1tQixNQUFOLENBQWEsZUFBYixFQUE4QixLQUFLOUIsV0FBbkM7QUFDQVcsY0FBTW1CLE1BQU4sQ0FBYSxnQkFBYixFQUErQixLQUFLMUIsWUFBcEM7QUFDQU8sY0FBTW1CLE1BQU4sQ0FBYSxrQkFBYixFQUFpQyxLQUFLckIsY0FBdEM7QUFDSDtBQXpERyxDQUFSOztBQTZEQXNCLFdBQVcsWUFBVTtBQUNuQlAsTUFBRSxlQUFGLEVBQW1CUSxnQkFBbkI7QUFDRCxDQUZELEVBRUcsSUFGSCxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMrQkE7O0FBRUE7WUFHQTs7MEJBQ0E7O3NCQUVBO3dCQUNBO3lCQUNBO3lCQUNBO3dCQUNBO3NCQUNBOzJCQUNBO3NCQUNBO21CQUNBO29CQUNBO3FCQUNBO29CQUNBOzs2QkFFQTs0QkFDQTswQkFDQTtBQUhBLDhDQUtBO0FBbEJBO0FBb0JBOzs7OzBDQUVBOytDQUNBO3dEQUNBO0FBRUE7O2lCQUNBO21DQUNBOzZEQUNBO3FFQUNBO2VBRUE7QUFFQTs7QUFDQTs7MkRBRUE7O3VEQUVBOzs7O3FDQUlBO0FBRkE7QUFEQSwwQ0FJQTsyQ0FFQTs7NkNBQ0E7a0RBQ0E7NENBQ0E7QUFFQTs7MkRBQ0E7d0RBRUE7OytDQUNBO3dEQUNBO3FGQUNBO21DQUNBO3VHQUNBO0FBRUE7OzZEQUNBO3VDQUNBOzJCQUNBOzBEQUNBO0FBQ0E7QUFFQTttQkFFQTs7OztxQ0FJQTtBQUZBO0FBREEsMENBSUE7MkNBRUE7OzZDQUNBO2tEQUNBOzRDQUNBO0FBRUE7OzJEQUNBO3dEQUNBOytDQUNBO3VEQUNBO2tDQUNBO0FBRUE7OytDQUNBO3dEQUNBO3FGQUNBO21DQUNBO3VHQUNBO0FBRUE7OzZEQUNBO3VDQUNBOzJCQUNBOzBEQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7O0FBQ0E7O2tDQUNBO0FBQ0E7QUFDQTtBQUNBOytDQUNBOzJDQUNBO0FBQ0E7QUFDQTs7NkNBRUE7MkJBR0E7QUFKQTs7bUNBS0E7cUVBRUE7O2lEQUNBOzBCQUVBOzt1REFFQTs7OzRDQUVBOytCQUNBO0FBRkEsMENBR0E7NENBQ0E7NkRBQ0E7K0JBQ0E7QUFDQTtBQUVBO21CQUVBOzs7dUNBRUE7K0JBQ0E7QUFGQSwwQ0FHQTs0Q0FDQTs2REFDQTsrQkFDQTtBQUNBO0FBRUE7QUFFQTs7OEJBQ0E7QUFFQTtnQ0FDQTs4Q0FDQTtBQUNBOzhDQUNBOzBFQUNBO0FBRUE7OENBQ0E7K0RBQ0E7QUFFQTs7QUFDQTs7dURBQ0E7b0RBQ0E7dURBQ0E7dUJBQ0E7dURBQ0E7QUFDQTtBQUNBO0FBRUE7cUNBQ0E7cURBQ0E7QUFFQTtrREFDQTttQ0FDQTtxRUFDQTswQkFDQTtBQUVBOztBQUNBOzs0RUFFQTs7b0dBQ0E7MkRBQ0E7OERBQ0E7MkRBQ0E7MkRBQ0E7NERBRUE7O2dEQUNBO0FBRUEsNENBRUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7dUJBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtnREFDQTt5QkFDQTtBQUVBOztBQUNBOztvRkFDQSw2QkFDQTs2REFDQTsrQkFDQTtnQ0FDQTtBQUNBO0FBSUE7QUE3TUE7OztzQ0ErTUE7eUNBQ0EseUNBRUEsd0RBQ0E7QUFDQTtrQ0FDQTt5Q0FDQSx5Q0FFQSwyQkFDQTtBQUNBO3NDQUNBO3lDQUNBLHFCQUVBLGlCQUNBO0FBR0E7QUFwQkE7OztBQXdCQTtBQUhBOzs7QUFJQTs7a0NBQ0E7NkJBQ0E7QUFFQTs7a0JBQ0EsQ0FDQSw4QkFDQSxPQUNBLFVBQ0EsVUFFQTs7NENBQ0E7QUFDQSxvQkFFQTs7bURBQ0E7b0VBQ0EsNkJBQ0E7MkZBQ0E7bUNBQ0E7b0NBQ0E7dUJBQ0E7bUNBQ0E7aURBQ0Esd0JBRUEsdUJBQ0E7QUFDQTtBQUNBO2VBQ0E7eUJBQ0E7MEJBQ0E7QUFFQTs7b0RBQ0E7c0RBQ0EsZ0RBQ0E7NkNBQ0E7QUFDQTtBQUVBOzsrQkFDQTtpQ0FDQTtXQUVBO0FBRUE7Z0NBQ0E7eUJBRUE7O2lDQUNBO3NDQUVBOzsrQkFDQTtBQUNBO3lEQUVBOztBQUNBOzhDQUNBOzhCQUNBOzBDQUNBO3lDQUNBOzBDQUNBO0FBQ0E7QUFDQTtXQUVBO0FBQ0E7QUFwVUEsRzs7Ozs7OztBQ2pHQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQXlHO0FBQ3pHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsMkVBQTJFLGFBQWE7QUFDeEY7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7Ozs7QUM5WEEsZUFBZSw2SkFBeU0saUJBQWlCLG1CQUFtQixjQUFjLDRCQUE0QixZQUFZLFVBQVUsaUJBQWlCLGdFQUFnRSxTQUFTLGdDQUFnQyxrQkFBa0IsYUFBYSxjQUFjLDBCQUEwQixXQUFXLHNDQUFzQyxTQUFTLEVBQUUsa0JBQWtCLCtHQUErRyxlQUFlLGFBQWEsY0FBYyw0TEFBNEwsa0JBQWtCLGFBQWEsNERBQTRELEtBQUsscUdBQXFHLE1BQU0sU0FBUyxzQ0FBc0MsU0FBUyxFQUFFLE9BQU8sb0lBQW9JLFdBQVcsZ0JBQWdCLE9BQU8sZ0ZBQWdGLFdBQVcsdUJBQXVCLG1DQUFtQyxRQUFRLFVBQVUsd0JBQXdCLCtDQUErQyw4QkFBOEIsb0JBQW9CLFdBQVcsNERBQTRELG1DQUFtQywySkFBMkoseURBQXlELGtEQUFrRCw0RkFBNEYsK0NBQStDLHdJQUF3SSxFQUFFLHdCQUF3QixxRkFBcUYsc0JBQXNCLGdFQUFnRSxVQUFVLHVCQUF1QixtREFBbUQsaUdBQWlHLHNCQUFzQixzRkFBc0YsaUJBQWlCLHFFQUFxRSxrQkFBa0IsMkNBQTJDLFdBQVcsa0JBQWtCLFFBQVEsU0FBUyxpQkFBaUIsZ0JBQWdCLFVBQVUsV0FBVyxzQkFBc0Isa0JBQWtCLHlEQUF5RCxpREFBaUQscUNBQXFDLEdBQUcscUVBQXFFLEdBQUcsd0VBQXdFLGdDQUFnQyx3QkFBd0IsSUFBSSwyRUFBMkUsZ0NBQWdDLHdCQUF3QixJQUFJLDhFQUE4RSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLDZFQUE2RSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLCtFQUErRSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLDJFQUEyRSxnQ0FBZ0Msd0JBQXdCLElBQUksd0VBQXdFLElBQUkscUVBQXFFLEdBQUcsc0VBQXNFLDZCQUE2QixHQUFHLHFFQUFxRSxHQUFHLHdFQUF3RSxnQ0FBZ0Msd0JBQXdCLElBQUksMkVBQTJFLGdDQUFnQyx3QkFBd0IsSUFBSSw4RUFBOEUsbUNBQW1DLDJCQUEyQixzQkFBc0IsSUFBSSw2RUFBNkUsbUNBQW1DLDJCQUEyQixzQkFBc0IsSUFBSSwrRUFBK0UsbUNBQW1DLDJCQUEyQixzQkFBc0IsSUFBSSwyRUFBMkUsZ0NBQWdDLHdCQUF3QixJQUFJLHdFQUF3RSxJQUFJLHFFQUFxRSxHQUFHLHNFQUFzRSxrQ0FBa0Msa0JBQWtCLHlDQUF5QyxXQUFXLGtCQUFrQixTQUFTLFFBQVEsa0JBQWtCLG1CQUFtQixVQUFVLFdBQVcsa0JBQWtCLHVEQUF1RCwrQ0FBK0MsbUNBQW1DLEdBQUcscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLEdBQUcsc0xBQXNMLDJCQUEyQixHQUFHLHFMQUFxTCxNQUFNLHFMQUFxTCxJQUFJLHFMQUFxTCxNQUFNLHFMQUFxTCxJQUFJLHFMQUFxTCxNQUFNLHFMQUFxTCxJQUFJLHFMQUFxTCxNQUFNLHFMQUFxTCxHQUFHLHNMQUFzTCxrQ0FBa0Msa0JBQWtCLHlDQUF5QyxXQUFXLGtCQUFrQixTQUFTLFFBQVEsaUJBQWlCLGtCQUFrQixVQUFVLFdBQVcsa0JBQWtCLHVEQUF1RCwrQ0FBK0MsbUNBQW1DLEdBQUcseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLEdBQUcsME1BQTBNLDJCQUEyQixHQUFHLHlNQUF5TSxNQUFNLHlNQUF5TSxJQUFJLHlNQUF5TSxNQUFNLHlNQUF5TSxJQUFJLHlNQUF5TSxNQUFNLHlNQUF5TSxJQUFJLHlNQUF5TSxNQUFNLHlNQUF5TSxHQUFHLDBNQUEwTSxrQ0FBa0Msa0JBQWtCLHNCQUFzQixzREFBc0QsOENBQThDLHlDQUF5QyxXQUFXLGtCQUFrQixjQUFjLE1BQU0sU0FBUyxnQkFBZ0IsaUJBQWlCLFVBQVUsV0FBVyxzQkFBc0Isa0JBQWtCLGlDQUFpQyxzQkFBc0IsK0JBQStCLHdEQUF3RCxnREFBZ0Qsb0NBQW9DLEdBQUcsNEJBQTRCLG9CQUFvQixHQUFHLGdDQUFnQyx5QkFBeUIsNEJBQTRCLEdBQUcsNEJBQTRCLG9CQUFvQixHQUFHLGdDQUFnQyx5QkFBeUIsNkNBQTZDLFdBQVcsa0JBQWtCLCtEQUErRCxxQkFBcUIsY0FBYyxXQUFXLFlBQVksZUFBZSxpQkFBaUIsa0JBQWtCLHlDQUF5QyxXQUFXLGVBQWUsa0JBQWtCLGVBQWUsT0FBTyxlQUFlLHFCQUFxQixTQUFTLDZCQUE2QixpQkFBaUIsY0FBYyxLQUFLLGNBQWMsNkJBQTZCLFNBQVMsZ0JBQWdCLGtCQUFrQixtQkFBbUIsc0NBQXNDLFlBQVksS0FBSyxjQUFjLEtBQUssaUJBQWlCLDhCQUE4QixRQUFRLFdBQVcsS0FBSyxXQUFXLGdHQUFnRyxJQUFJLGlCQUFpQixRQUFRLFlBQVksV0FBVyxTQUFTLDhNQUE4TSxlQUFlLFdBQVcsa0JBQWtCLDhDQUE4QyxnQkFBZ0IseUNBQXlDLFdBQVcsYUFBYSxzRUFBc0UsRUFBRSx5QkFBeUIsb0JBQW9CLDJCQUEyQixhQUFhLGtJQUFrSSx1Q0FBdUMsb0VBQW9FLGFBQWEsb0lBQW9JLHVDQUF1QyxrREFBa0QscUJBQXFCLGlCQUFpQixnQkFBZ0IsWUFBWSxXQUFXLEtBQUsscUJBQXFCLE1BQU0sU0FBUyxZQUFZLGlCQUFpQiwyQkFBMkIsS0FBSyxpQkFBaUIsa0NBQWtDLEtBQUssaUJBQWlCLGlCQUFpQiw0QkFBNEIsU0FBUywwQkFBMEIsY0FBYyxpQkFBaUIsS0FBSyxXQUFXLEtBQUssMENBQTBDLDJCQUEyQixxQ0FBcUMsZUFBZSxFQUFFLFNBQVMsZ0JBQWdCLDBCQUEwQixnSUFBZ0ksS0FBSywrR0FBK0csa0JBQWtCLGNBQWMsNEJBQTRCLG1CQUFtQixvQkFBb0IsY0FBYyxzQ0FBc0Msa0NBQWtDLGdCQUFnQixVQUFVLGdCQUFnQixVQUFVLDBEQUEwRCwwQ0FBMEMsTUFBTSx3QkFBd0IsTUFBTSxzRUFBc0UsT0FBTyxVQUFVLG9CQUFvQixpQkFBaUIsNENBQTRDLEtBQUssZ0RBQWdELDRFQUE0RSxnQkFBZ0Isb0NBQW9DLDhIQUE4SCwwR0FBMEcsS0FBSyxLQUFLLGFBQWEsNkJBQTZCLDJDQUEyQyxRQUFRLGVBQWUsTUFBTSxrQkFBa0IsNERBQTRELGdCQUFnQixvRUFBb0UsaUJBQWlCLCtEQUErRCxrQkFBa0Isd0JBQXdCLE9BQU8sMEdBQTBHLFdBQVcsMEJBQTBCLGlCQUFpQixXQUFXLEtBQUsscUJBQXFCLG1CQUFtQixNQUFNLFdBQVcsT0FBTyxZQUFZLFdBQVcsS0FBSyxXQUFXLGVBQWUsWUFBWSxpQkFBaUIsaUJBQWlCLG1CQUFtQixpQkFBaUIsU0FBUyxxQkFBcUIsNENBQTRDLEdBQUcsaUJBQWlCLFdBQVcsc0NBQXNDLFNBQVMsRUFBRSwrQkFBK0IsR0FBRyxFIiwiZmlsZSI6ImpzL2NoYXQtYmFyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0MTcpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGY1ZmNmZjBlMWYwODAwOTdjZjQ2IiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUgMjYgMjciLCJpbXBvcnQgQ2hhdEJveCBmcm9tICcuL0NoYXRCb3gudnVlJ1xyXG5cclxubmV3IFZ1ZSh7XHJcbiAgICBlbDogJyNjaGF0LWJhcicsXHJcblxyXG4gICAgZGF0YToge1xyXG4gICAgICAgIHVzZXJzOiBbXSxcclxuICAgICAgICBub3RpZmljYXRpb25zOiBbXSxcclxuICAgIH0sXHJcblxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHNob3dDaGF0Qm94KHVzZXIpIHtcclxuICAgICAgICAgICAgaWYgKCEhfnRoaXMudXNlcnMuaW5kZXhPZih1c2VyKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnVzZXJzLnB1c2godXNlcik7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY2xvc2VDaGF0Qm94KHVzZXIpIHtcclxuICAgICAgICAgICAgdmFyIGluZGV4ID0gdGhpcy51c2Vycy5maW5kSW5kZXgoZnVuY3Rpb24gKGVsKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZWwuaWQgPT09IHVzZXIuaWQ7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgdGhpcy51c2Vycy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGZpcmVOZXdNZXNzYWdlKGRhdGEpIHtcclxuICAgICAgICAgICAgZGF0YS5tZXNzYWdlLnVzZXIgPSB0aGlzLnVzZXJzWzBdO1xyXG4gICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LXJvb20tJyArIGRhdGEubWVzc2FnZS5jaGF0X3Jvb21faWQsIGRhdGEubWVzc2FnZSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0Tm90aWZpY2F0aW9ucygpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvbWVzc2FnZXMvbm90aWZpY2F0aW9uczInKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbnMgPSByZXN1bHQuZGF0YTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG1hcmtBbGxBc1NlZW4oKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucy5mb3JFYWNoKGZ1bmN0aW9uIChub3RpZikge1xyXG4gICAgICAgICAgICAgICAgaWYgKCEgbm90aWYuc2Vlbl9hdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIG5vdGlmLnNlZW5fYXQgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgJChcIi5jbnRtc2dcIikuaHRtbCgnJyk7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLnBvc3QoJy9tZXNzYWdlcy9tYXJrLWFsbC1hcy1zZWVuMicpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAgIENoYXRCb3hcclxuICAgIH0sXHJcblxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgICB0aGlzLmdldE5vdGlmaWNhdGlvbnMoKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ2NoYXQtYm94LnNob3cnLCB0aGlzLnNob3dDaGF0Qm94KTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ2NoYXQtYm94LmNsb3NlJywgdGhpcy5jbG9zZUNoYXRCb3gpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignbmV3LWNoYXQtbWVzc2FnZScsIHRoaXMuZmlyZU5ld01lc3NhZ2UpO1xyXG4gICAgfVxyXG59KTtcclxuXHJcblxyXG5zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgJCgnLmpzLWF1dG8tc2l6ZScpLnRleHRhcmVhQXV0b1NpemUoKTtcclxufSwgNTAwMCk7XHJcbiAgICBcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jaGF0LWJhci5qcyIsIjx0ZW1wbGF0ZT5cclxuPGRpdiA6aWQ9XCIndXNlcicrdXNlcklkXCIgY2xhc3M9XCJwYW5lbCBwYW5lbC1jaGF0LWJveFwiPlxyXG4gIDxkaXYgQGNsaWNrPVwidG9nZ2xlVmlzaWJpbGl0eVwiIGNsYXNzPVwicGFuZWwtaGVhZGluZ1wiPlxyXG4gICAge3sgZnVsbE5hbWUgfX1cclxuICAgIDxzcGFuIEBjbGljay5zdG9wPVwiY2xvc2VcIiBjbGFzcz1cInB1bGwtcmlnaHRcIiB0aXRsZT1cIkNsb3NlXCIgZGF0YS10b2dnbGU9J3Rvb2x0aXAnIGRhdGEtcGxhY2VtZW50PSd0b3AnPjxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5jbGVhcjwvaT48L3NwYW4+XHJcbiAgICA8c3BhbiBAY2xpY2s9XCJ2aWV3Q2hhdFJvb21cIiBjbGFzcz1cInB1bGwtcmlnaHQgbS1yLTEgcC1yLTFcIiB2LWlmPVwiY3JfY2hlY2tcIiB0aXRsZT1cIk9wZW4gaW4gTWVzc2VuZ2VyXCIgZGF0YS10b2dnbGU9J3Rvb2x0aXAnIGRhdGEtcGxhY2VtZW50PSd0b3AnPjxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5hc3BlY3RfcmF0aW88L2k+PC9zcGFuPlxyXG4gICAgPHNwYW4gQGNsaWNrPVwidmlld0NoYXRVc2VyXCIgY2xhc3M9XCJwdWxsLXJpZ2h0IG0tci0xIHAtci0xXCIgdi1lbHNlPjxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIiB0aXRsZT1cIk9wZW4gaW4gTWVzc2VuZ2VyXCIgZGF0YS10b2dnbGU9J3Rvb2x0aXAnIGRhdGEtcGxhY2VtZW50PSd0b3AnPmFzcGVjdF9yYXRpbzwvaT48L3NwYW4+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cImNoYXQtYm94XCI+XHJcbiAgICA8ZGl2IDppZD1cIidwYW5lbC1ib2R5LScrdXNlcklkXCIgY2xhc3M9XCJwYW5lbC1ib2R5XCIgOmNsYXNzPVwiW2lzc3VlPT0nMScgfHwgY2xvc2VkPT0nMScgPyBoZWlnaHQgOiAnJ11cIj5cclxuICAgICAgPGRpdiBjbGFzcz1cInJvdyBhbGVydC1yZW1pbmRlclwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJhYnMgYWxlcnQgYWxlcnQtZ3JleSBhbGVydC1kaXNtaXNzYWJsZVwiPlxyXG4gICAgICAgICAgPGEgaHJlZj1cIiNcIiBjbGFzcz1cImNsb3NlXCIgZGF0YS1kaXNtaXNzPVwiYWxlcnRcIiBhcmlhLWxhYmVsPVwiY2xvc2VcIj4mdGltZXM7PC9hPlxyXG4gICAgICAgICAgPHN0cm9uZz57e2xhbmd1YWdlW1wicmVtaW5kZXJcIl19fTo8L3N0cm9uZz4ge3tsYW5ndWFnZVsnYWxlcnQtbXNnJ119fVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiB2LXNob3c9J2lzc3VlPT1cIjBcIic+XHJcbiAgICAgICAgICA8aW5maW5pdGUtbG9hZGluZyA6b24taW5maW5pdGU9XCJvbkluZmluaXRlXCIgcmVmPVwiaW5maW5pdGVMb2FkaW5nXCIgZGlyZWN0aW9uPVwidG9wXCIgOmRpc3RhbmNlPVwiMFwiPjwvaW5maW5pdGUtbG9hZGluZz5cclxuICAgICAgICAgIDx1bD4gXHJcbiAgICAgICAgICAgICAgPGxpIHYtZm9yPVwibWVzc2FnZSBpbiBtZXNzYWdlc1wiIDpjbGFzcz1cInsgbWU6IGlzTWUobWVzc2FnZSkgfVwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IHYtaWY9JyFpc01lKG1lc3NhZ2UpJyBjbGFzcz0nc29tZW9uZU1lc3NhZ2UnPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMiBjb2wtbWQtMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpzcmM9XCJtZXNzYWdlLnVzZXIuYXZhdGFyXCIgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz0naW1nLXJlc3BvbnNpdmUgY2hhdC1hdmF0YXInIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS10b2dnbGU9J3Rvb2x0aXAnIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1wbGFjZW1lbnQ9J2xlZnQnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLWNvbnRhaW5lcj1cIiNjaGF0LWJhclwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA6ZGF0YS10ZW1wLXRpdGxlPSdtZXNzYWdlLnVzZXIuZnVsbF9uYW1lJyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDp0aXRsZT1cIm1lc3NhZ2UudXNlci5mdWxsX25hbWUgKyAnICcgKyBtZXNzYWdlLmNyZWF0ZWRfYXRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTkgY29sLW1kLTlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ1c2VybmFtZVwiIHYtaWY9XCJtZXNzYWdlLnVzZXIubmlja19uYW1lIT09bnVsbFwiPnt7IG1lc3NhZ2UudXNlci5uaWNrX25hbWUgfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwidXNlcm5hbWVcIiB2LWVsc2U+e3sgbWVzc2FnZS51c2VyLmZpcnN0X25hbWUgfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxicj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJtZXNzYWdlXCIgZGF0YS10b2dnbGU9J3Rvb2x0aXAnIGRhdGEtcGxhY2VtZW50PSdsZWZ0JyBkYXRhLWNvbnRhaW5lcj1cIiNjaGF0LWJhclwiIDp0aXRsZT1cIm1lc3NhZ2UudXNlci5mdWxsX25hbWUgKyAnICcgKyBtZXNzYWdlLmNyZWF0ZWRfYXRcIj57eyBtZXNzYWdlLmJvZHkgfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIHYtaWY9J2lzTWUobWVzc2FnZSknIGNsYXNzPVwibWVzc2FnZVwiIGRhdGEtdG9nZ2xlPSd0b29sdGlwJyBkYXRhLXBsYWNlbWVudD0nbGVmdCcgZGF0YS1jb250YWluZXI9XCIjY2hhdC1iYXJcIiA6dGl0bGU9XCJtZXNzYWdlLmNyZWF0ZWRfYXRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3sgbWVzc2FnZS5ib2R5IH19XHJcbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICA8L3VsPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiB2LXNob3c9J2lzc3VlPT1cIjFcIic+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInRoYW5rc1wiPnt7bGFuZ3VhZ2VbXCJ0aGFuay15b3VcIl19fTwvZGl2PlxyXG4gICAgICAgIDxicj5cclxuICAgICAgICA8ZGl2Pnt7bGFuZ3VhZ2VbXCJjYXNlLXNvbHZlZFwiXX19PC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNkLWZpbHRlci1ibG9ja1wiPlxyXG4gICAgICAgICAgICA8dWwgY2xhc3M9XCJjZC1maWx0ZXItY29udGVudCBjZC1maWx0ZXJzIGxpc3RcIj5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm1hcmdpblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cImZpbHRlclwiIGRhdGEtZmlsdGVyPVwiLnJhZGlvMVwiIHR5cGU9XCJyYWRpb1wiIHZhbHVlPVwieWVzXCIgdi1tb2RlbD1cImZvcm0uY2FzZV9zb2x2ZWRcIiBuYW1lPVwiY2FzZV9zb2x2ZWRcIiBpZD1cInllc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cInJhZGlvLWxhYmVsXCIgZm9yPVwicmFkaW8xXCI+e3tsYW5ndWFnZVtcInllc1wiXX19PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJtYXJnaW5cIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJmaWx0ZXJcIiBkYXRhLWZpbHRlcj1cIi5yYWRpbzJcIiB0eXBlPVwicmFkaW9cIiB2YWx1ZT1cIm5vXCIgdi1tb2RlbD1cImZvcm0uY2FzZV9zb2x2ZWRcIiBuYW1lPVwiY2FzZV9zb2x2ZWRcIiBpZD1cIm5vXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwicmFkaW8tbGFiZWxcIiBmb3I9XCJyYWRpbzJcIj57e2xhbmd1YWdlW1wibm9cIl19fTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJoZWxwLWJsb2NrIGVycm9yXCIgdi1pZj1cImZvcm0uZXJyb3JzLmhhcygnY2FzZV9zb2x2ZWQnKVwiPnt7bGFuZ3VhZ2VbXCJwbGVhc2VcIl19fTwvc3Bhbj5cclxuICAgICAgICAgICAgPC91bD5cclxuICAgICAgICA8L2Rpdj4gICAgXHJcbiAgICAgICAgPGRpdj57e2xhbmd1YWdlW1wicmF0ZS1hZ2VudFwiXX19PC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNkLWZpbHRlci1ibG9ja1wiPlxyXG4gICAgICAgICAgICA8dWwgY2xhc3M9XCJjZC1maWx0ZXItY29udGVudCBjZC1maWx0ZXJzIGxpc3RcIj5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cIm1hcmdpblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cImZpbHRlclwiIGRhdGEtZmlsdGVyPVwiLnJhZGlvMVwiIHR5cGU9XCJyYWRpb1wiIHZhbHVlPVwiZ29vZFwiIHYtbW9kZWw9XCJmb3JtLnJhdGVfYWdlbnRcIiBuYW1lPVwicmF0ZV9hZ2VudFwiIGlkPVwiZ29vZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cInJhZGlvLWxhYmVsXCIgZm9yPVwicmFkaW8xXCI+e3tsYW5ndWFnZVtcImdvb2RcIl19fTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibWFyZ2luXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzPVwiZmlsdGVyXCIgZGF0YS1maWx0ZXI9XCIucmFkaW8yXCIgdHlwZT1cInJhZGlvXCIgdmFsdWU9XCJhdmVyYWdlXCIgdi1tb2RlbD1cImZvcm0ucmF0ZV9hZ2VudFwiIG5hbWU9XCJyYXRlX2FnZW50XCIgaWQ9XCJhdmVyYWdlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwicmFkaW8tbGFiZWxcIiBmb3I9XCJyYWRpbzJcIj57e2xhbmd1YWdlW1wiYXZlcmFnZVwiXX19PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJtYXJnaW5cIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJmaWx0ZXJcIiBkYXRhLWZpbHRlcj1cIi5yYWRpbzNcIiB0eXBlPVwicmFkaW9cIiB2YWx1ZT1cImJhZFwiIHYtbW9kZWw9XCJmb3JtLnJhdGVfYWdlbnRcIiBuYW1lPVwicmF0ZV9hZ2VudFwiIGlkPVwiYmFkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwicmFkaW8tbGFiZWxcIiBmb3I9XCJyYWRpbzNcIj57e2xhbmd1YWdlW1wiYmFkXCJdfX08L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaGVscC1ibG9jayBlcnJvclwiIHYtaWY9XCJmb3JtLmVycm9ycy5oYXMoJ2Nhc2Vfc29sdmVkJylcIj57e2xhbmd1YWdlW1wicGxlYXNlXCJdfX08L3NwYW4+XHJcbiAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj57e2xhbmd1YWdlW1wiY29tbWVudHNcIl19fTwvZGl2PlxyXG4gICAgICAgIDx0ZXh0YXJlYSBpZD1cImFkZC1jb21tZW50XCIgOnJvd3M9XCI0XCIgbmFtZT1cImNvbW1lbnRzXCIgdi1tb2RlbD1cImZvcm0uY29tbWVudHNcIj48L3RleHRhcmVhPlxyXG4gICAgICAgIDxhIGlkPVwic3VibWl0XCIgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwiYnRuIGJ0bi1saWdodC1ncmVlblwiIEBjbGljaz0nc2F2ZUlzc3VlRmVlZGJhY2soKSc+e3tsYW5ndWFnZVtcInN1Ym1pdFwiXX19PC9hPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiBjbGFzcz1cInRoYW5rc1wiIHYtc2hvdz0nY2xvc2VkPT1cIjFcIic+e3tsYW5ndWFnZVtcImlzc3VlXCJdfX0gOik8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBjbGFzcz1cInBhbmVsLWZvb3RlclwiIHYtc2hvdz0naXNzdWU9PVwiMFwiICYmIGNsb3NlZD09XCIwXCInPlxyXG4gICAgICA8dGV4dGFyZWEgY2xhc3M9XCJqcy1hdXRvLXNpemVcIiA6ZGF0YS1jaGF0PVwiJ3BhbmVsLWJvZHktJyt1c2VySWRcIiA6aWQ9XCInaWQnK3VzZXJJZFwiIDpyb3dzPVwiMVwiIHYtbW9kZWw9XCJuZXdNZXNzYWdlXCIgQGtleWRvd24uZW50ZXIucHJldmVudD1cInNlbmRcIiBAZm9jdXM9XCJtYXJrQWxsQXNTZWVuXCIgIDpwbGFjZWhvbGRlcj1cImxhbmd1YWdlWyd0eXBlLWEtbWVzc2FnZSddXCIgOmRpc2FibGVkPVwiaXNibG9jaz09J0Jsb2NrZWQnXCI+PC90ZXh0YXJlYT5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuXHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5pbXBvcnQgSW5maW5pdGVMb2FkaW5nIGZyb20gJ3Z1ZS1pbmZpbml0ZS1sb2FkaW5nJ1xyXG5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgcHJvcHM6IFsndXNlciddLFxyXG5cclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbWVzc2FnZXM6IFtdLFxyXG4gICAgICAgICAgICBuZXdNZXNzYWdlOiBudWxsLFxyXG4gICAgICAgICAgICBuZXdNZXNzYWdlMjogbnVsbCxcclxuICAgICAgICAgICAgY3VycmVudFBhZ2U6IDAsXHJcbiAgICAgICAgICAgIGNoYXRSb29tSWQ6IG51bGwsXHJcbiAgICAgICAgICAgICRjaGF0Qm94OiBudWxsLFxyXG4gICAgICAgICAgICAkY29udmVyc2F0aW9uOiBudWxsLFxyXG4gICAgICAgICAgICBsYW5ndWFnZTpbXSxcclxuICAgICAgICAgICAgaXNzdWU6bnVsbCxcclxuICAgICAgICAgICAgY2xvc2VkOm51bGwsXHJcbiAgICAgICAgICAgIGlzYmxvY2s6bnVsbCxcclxuICAgICAgICAgICAgaGVpZ2h0OiAnaXNzdWUtaGVpZ2h0JyxcclxuICAgICAgICAgICAgZm9ybTogbmV3IEZvcm0oe1xyXG4gICAgICAgICAgICAgICBjYXNlX3NvbHZlZDogbnVsbCxcclxuICAgICAgICAgICAgICAgcmF0ZV9hZ2VudDogbnVsbCxcclxuICAgICAgICAgICAgICAgY29tbWVudHM6IG51bGxcclxuICAgICAgICAgICAgfSx7IGJhc2VVUkw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCB9KVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIG9uSW5maW5pdGUoKSB7XHJcbiAgICAgICAgICAgIGlmICghIHRoaXMuJGNoYXRCb3guaXMoJzp2aXNpYmxlJykpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZScpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLmdldE1lc3NhZ2VzKCk7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgJChcIi5wYW5lbC1jaGF0LWJveCBbZGF0YS10b2dnbGU9J3Rvb2x0aXAnXVwiKS50b29sdGlwKCk7XHJcbiAgICAgICAgICAgICAgICAkKFwiLnBhbmVsLWNoYXQtYm94IFtkYXRhLXRvZ2dsZT0ndG9vbHRpcCddXCIpLnRvb2x0aXAoJ2ZpeFRpdGxlJyk7XHJcbiAgICAgICAgICAgIH0sIDMwMCk7XHJcblxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdldE1lc3NhZ2VzKCkge1xyXG4gICAgICAgICAgICB2YXIgcHJldlNjcm9sbEhlaWdodCA9IHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVzZXIuaGFzT3duUHJvcGVydHkoJ2NoYXRfcm9vbScpKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvbWVzc2FnZXMvY2hhdC1yb29tLycgKyB0aGlzLnVzZXIuY2hhdF9yb29tX2lkLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2U6ICsrdGhpcy5jdXJyZW50UGFnZSxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGRhdGEgPSByZXN1bHQuZGF0YS5kYXRhO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhLmlkIC0gYi5pZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1lc3NhZ2VzID0gZGF0YS5jb25jYXQodGhpcy5tZXNzYWdlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFBhZ2UgPSByZXN1bHQuZGF0YS5jdXJyZW50X3BhZ2U7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRQYWdlID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uLnNjcm9sbFRvcCh0aGlzLiRjb252ZXJzYXRpb24ucHJvcCgnc2Nyb2xsSGVpZ2h0JykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSAtIHByZXZTY3JvbGxIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJlZnMuaW5maW5pdGVMb2FkaW5nLiRlbWl0KCckSW5maW5pdGVMb2FkaW5nOmxvYWRlZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJlZnMuaW5maW5pdGVMb2FkaW5nLiRlbWl0KCckSW5maW5pdGVMb2FkaW5nOmNvbXBsZXRlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL21lc3NhZ2VzL3VzZXIvJyArIHRoaXMudXNlci5pZCwge1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlOiArK3RoaXMuY3VycmVudFBhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSkudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBkYXRhID0gcmVzdWx0LmRhdGEuZGF0YTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS5zb3J0KChhLCBiKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYS5pZCAtIGIuaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tZXNzYWdlcyA9IGRhdGEuY29uY2F0KHRoaXMubWVzc2FnZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRQYWdlID0gcmVzdWx0LmRhdGEuY3VycmVudF9wYWdlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoISB0aGlzLmNoYXRSb29tSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhdFJvb21JZCA9IGRhdGFbMF0uY2hhdF9yb29tX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0ZW4oKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UGFnZSA9PT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbi5zY3JvbGxUb3AodGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uLnNjcm9sbFRvcCh0aGlzLiRjb252ZXJzYXRpb24ucHJvcCgnc2Nyb2xsSGVpZ2h0JykgLSBwcmV2U2Nyb2xsSGVpZ2h0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpsb2FkZWQnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHNlbmQoKSB7XHJcbiAgICAgICAgICAgIGlmICghIHRoaXMubmV3TWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vaWYgdXNlciBzZW5kcyBzcGFjZXMgb25seVxyXG4gICAgICAgICAgICBpZigkLnRyaW0odGhpcy5uZXdNZXNzYWdlKT09Jycpe1xyXG4gICAgICAgICAgICAgICAgJCgnI2lkJyt0aGlzLnVzZXJJZCkudmFsKCcnKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgdXNlcl9pZDogd2luZG93LkxhcmF2ZWwudXNlci5pZCxcclxuICAgICAgICAgICAgICAgIGJvZHk6IHRoaXMubmV3TWVzc2FnZVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uLnNjcm9sbFRvcCh0aGlzLiRjb252ZXJzYXRpb24ucHJvcCgnc2Nyb2xsSGVpZ2h0JykpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuJHJlZnMuaW5maW5pdGVMb2FkaW5nLiRlbWl0KCckSW5maW5pdGVMb2FkaW5nOmxvYWRlZCcpO1xyXG4gICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMudXNlci5oYXNPd25Qcm9wZXJ0eSgnY2hhdF9yb29tJykpIHtcclxuXHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLnBvc3QoJy9tZXNzYWdlcycsIHtcclxuICAgICAgICAgICAgICAgICAgICB0b19jaGF0X3Jvb206IHRoaXMudXNlci5jaGF0X3Jvb21faWQsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTogdGhpcy5uZXdNZXNzYWdlXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEgdGhpcy5jaGF0Um9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhdFJvb21JZCA9IHJlc3VsdC5kYXRhLmRhdGEuY2hhdF9yb29tX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RlbigpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLnBvc3QoJy9tZXNzYWdlcycsIHtcclxuICAgICAgICAgICAgICAgICAgICB0b191c2VyOiB0aGlzLnVzZXIuaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTogdGhpcy5uZXdNZXNzYWdlXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEgdGhpcy5jaGF0Um9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhdFJvb21JZCA9IHJlc3VsdC5kYXRhLmRhdGEuY2hhdF9yb29tX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RlbigpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMubmV3TWVzc2FnZSA9IG51bGw7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY2xvc2UoKSB7XHJcbiAgICAgICAgICAgIEV2ZW50LmZpcmUoJ2NoYXQtYm94LmNsb3NlJywgdGhpcy51c2VyKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHZpZXdDaGF0Um9vbSgpIHtcclxuICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSAnL21lc3NhZ2VzIy9jci8nK3RoaXMudXNlci5jaGF0X3Jvb20uaWQ7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdmlld0NoYXRVc2VyKCkge1xyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvbWVzc2FnZXMjL3UvJyt0aGlzLnVzZXIuaWQ7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdG9nZ2xlVmlzaWJpbGl0eSgpIHtcclxuICAgICAgICAgICAgdGhpcy4kY2hhdEJveC5zbGlkZVRvZ2dsZSgzMDAsICgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLiRjaGF0Qm94LmlzKCc6dmlzaWJsZScpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kcmVmcy5pbmZpbml0ZUxvYWRpbmcuJGVtaXQoJyRJbmZpbml0ZUxvYWRpbmc6cmVzZXQnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kcmVmcy5pbmZpbml0ZUxvYWRpbmcuJGVtaXQoJyRJbmZpbml0ZUxvYWRpbmc6Y29tcGxldGUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaXNNZShtZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB3aW5kb3cuTGFyYXZlbC51c2VyLmlkID09IG1lc3NhZ2UudXNlcl9pZDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzY3JvbGxUb0JvdHRvbSgpIHtcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSk7XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgMSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgbGlzdGVuKCkge1xyXG4gICAgICAgICAgICBFdmVudC5saXN0ZW4oJ2NoYXQtcm9vbS0nICsgdGhpcy5jaGF0Um9vbUlkLCBtZXNzYWdlID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy51c2VyLmhhc093blByb3BlcnR5KCdjaGF0X3Jvb20nKSAmJiBtZXNzYWdlLnVzZXIuaGFzT3duUHJvcGVydHkoJ3VzZXInKSl7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5uZXdNZXNzYWdlMiA9IHtcImJvZHlcIjogbWVzc2FnZS5ib2R5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ1c2VyXCI6IHtcImF2YXRhclwiOiBtZXNzYWdlLnVzZXIudXNlci5hdmF0YXIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJmdWxsX25hbWVcIjogbWVzc2FnZS51c2VyLnVzZXIuZnVsbF9uYW1lLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibmlja19uYW1lXCI6IG1lc3NhZ2UudXNlci51c2VyLm5pY2tfbmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmlyc3RfbmFtZVwiOiBtZXNzYWdlLnVzZXIudXNlci5maXJzdF9uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZXNzYWdlcy5wdXNoKHRoaXMubmV3TWVzc2FnZTIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWVzc2FnZXMucHVzaChtZXNzYWdlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyAkKCcuY2hhdC1hdmF0YXInKS5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgJCh0aGlzKS5hdHRyKCd0aXRsZScsJCh0aGlzKS5kYXRhKCd0ZW1wLXRpdGxlJykpO1xyXG4gICAgICAgICAgICAgICAgLy8gfSlcclxuICAgICAgICAgICAgICAgIC8vICQoXCJbZGF0YS10b2dnbGU9J3RpdGxlJ11cIikudG9vbHRpcCgnZml4VGl0bGUnKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2Nyb2xsVG9Cb3R0b20oKTtcclxuICAgICAgICAgICAgICAgIC8vIHZhciBhdWRpbyA9IG5ldyBBdWRpbygnL3NvdW5kL3d5Y25vdGlmLm1wMycpO1xyXG4gICAgICAgICAgICAgICAgLy8gYXVkaW8ucGxheSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBtYXJrQWxsQXNTZWVuKCkge1xyXG4gICAgICAgICAgICB0aGlzLiRwYXJlbnQubWFya0FsbEFzU2VlbigpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHNhdmVJc3N1ZUZlZWRiYWNrKCkge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm0uc3VibWl0KCdwb3N0JywgJy92MS9zYXZlSXNzdWVGZWVkYmFjay8nK3RoaXMudXNlci5jaGF0X3Jvb20ubmFtZSlcclxuICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ3VhZ2VbJ2ZlZWRiYWNrJ10gKyAnIDopJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc3N1ZSA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZWQgPSAxO1xyXG4gICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgZnVsbE5hbWUoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVzZXIuaGFzT3duUHJvcGVydHkoJ2NoYXRfcm9vbScpKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMudXNlci5jaGF0X3Jvb20ubmFtZTtcclxuICAgICAgICAgICAgZWxzZSBcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnVzZXIuZmlyc3RfbmFtZSArICcgJyArIHRoaXMudXNlci5sYXN0X25hbWU7XHJcbiAgICAgICAgfSxcclxuICAgICAgICB1c2VySWQoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVzZXIuaGFzT3duUHJvcGVydHkoJ2NoYXRfcm9vbScpKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMudXNlci5jaGF0X3Jvb20ubmFtZTtcclxuICAgICAgICAgICAgZWxzZSBcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnVzZXIuaWQ7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjcl9jaGVjaygpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudXNlci5oYXNPd25Qcm9wZXJ0eSgnY2hhdF9yb29tJykpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgZWxzZSBcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9LFxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgSW5maW5pdGVMb2FkaW5nXHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0VHJhbnNsYXRpb24oKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvcy5nZXQoJy90cmFuc2xhdGUvY2hhdC1ib3gnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGF4aW9zLmFsbChbXHJcbiAgICAgICAgICAgIGdldFRyYW5zbGF0aW9uKClcclxuICAgICAgICBdKS50aGVuKGF4aW9zLnNwcmVhZChcclxuICAgICAgICAgICAgKFxyXG4gICAgICAgICAgICBsYW5ndWFnZVxyXG4gICAgICAgICAgICApID0+IHtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLmxhbmd1YWdlID0gbGFuZ3VhZ2UuZGF0YS5kYXRhO1xyXG4gICAgICAgIH0pKVxyXG4gICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG5cclxuICAgICAgICBpZiAodGhpcy51c2VyLmhhc093blByb3BlcnR5KCdjaGF0X3Jvb20nKSkge1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2dldElzc3VlU3RhdHVzLycrdGhpcy51c2VyLmNoYXRfcm9vbS5uYW1lKVxyXG4gICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYocmVzdWx0LmRhdGFbMF0uc3RhdHVzPT1cImNsb3NlZFwiICYmIHJlc3VsdC5kYXRhWzBdLmNhc2Vfc29sdmVkPT1cIlwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc3N1ZSA9IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZWQgPSAwO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzc3VlID0gMDtcclxuICAgICAgICAgICAgICAgICAgICBpZihyZXN1bHQuZGF0YVswXS5zdGF0dXM9PVwib3BlblwiKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlZCA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlZCA9IDE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNzdWUgPSAwO1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NlZCA9IDA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMudXNlci5oYXNPd25Qcm9wZXJ0eSgnY2hhdF9yb29tJykpIHtcclxuICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy91c2Vycy8nICsgdGhpcy51c2VyLmlkICsgJz9pbmNsdWRlPXJlbGF0aW9uc2hpcCcpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzYmxvY2sgPSByZXN1bHQuZGF0YS5mcmllbmRfc3RhdHVzOyBcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKCcuYWxlcnQtcmVtaW5kZXInKS5oaWRlKCk7XHJcbiAgICAgICAgfSw1MDAwKTtcclxuXHJcbiAgICB9LFxyXG5cclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgdmFyICRlbCA9ICQodGhpcy4kZWwpO1xyXG5cclxuICAgICAgICB0aGlzLiRjaGF0Qm94ID0gJGVsLmZpbmQoJy5jaGF0LWJveCcpO1xyXG4gICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbiA9ICRlbC5maW5kKCcucGFuZWwtYm9keScpO1xyXG5cclxuICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIC8vICQoXCJbZGF0YS10b2dnbGU9J3Rvb2x0aXAnXVwiKS50b29sdGlwKCk7XHJcbiAgICAgICAgICAgICQoXCIucGFuZWwtY2hhdC1ib3ggW2RhdGEtdG9nZ2xlPSd0b29sdGlwJ11cIikudG9vbHRpcCgpO1xyXG5cclxuICAgICAgICAgICAgLy9jaGF0Ym94IHN0aWNreSBhbGVydCBtZXNzYWdlXHJcbiAgICAgICAgICAgICQoJy5hbGVydC1ncmV5JykuZWFjaChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgdmFyICR0aGF0ID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgICAgICAgICAkcGFnZSA9ICR0aGF0LmNsb3Nlc3QoJy5wYW5lbC1ib2R5Jyk7XHJcbiAgICAgICAgICAgICAgICAkcGFnZS5zY3JvbGwoZnVuY3Rpb24oKXsgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAkdGhhdC5jc3Moe3RvcDogdGhpcy5zY3JvbGxUb3B9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LDEwMDApO1xyXG5cclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBDaGF0Qm94LnZ1ZT85NWI0YjhiYyIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NoYXRCb3gudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1iZWY2M2U3NFxcXCJ9IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9DaGF0Qm94LnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcQ2hhdEJveC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBDaGF0Qm94LnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1iZWY2M2U3NFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWJlZjYzZTc0XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvQ2hhdEJveC52dWVcbi8vIG1vZHVsZSBpZCA9IDMyM1xuLy8gbW9kdWxlIGNodW5rcyA9IDE2IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwgcGFuZWwtY2hhdC1ib3hcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiAndXNlcicgKyBfdm0udXNlcklkXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1oZWFkaW5nXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLnRvZ2dsZVZpc2liaWxpdHlcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJcXHJcXG4gICAgXCIgKyBfdm0uX3MoX3ZtLmZ1bGxOYW1lKSArIFwiXFxyXFxuICAgIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInRpdGxlXCI6IFwiQ2xvc2VcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcImRhdGEtcGxhY2VtZW50XCI6IFwidG9wXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgIF92bS5jbG9zZSgkZXZlbnQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibWF0ZXJpYWwtaWNvbnNcIlxuICB9LCBbX3ZtLl92KFwiY2xlYXJcIildKV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmNyX2NoZWNrKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInB1bGwtcmlnaHQgbS1yLTEgcC1yLTFcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0aXRsZVwiOiBcIk9wZW4gaW4gTWVzc2VuZ2VyXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0udmlld0NoYXRSb29tXG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibWF0ZXJpYWwtaWNvbnNcIlxuICB9LCBbX3ZtLl92KFwiYXNwZWN0X3JhdGlvXCIpXSldKSA6IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInB1bGwtcmlnaHQgbS1yLTEgcC1yLTFcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0udmlld0NoYXRVc2VyXG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibWF0ZXJpYWwtaWNvbnNcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0aXRsZVwiOiBcIk9wZW4gaW4gTWVzc2VuZ2VyXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiYXNwZWN0X3JhdGlvXCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNoYXQtYm94XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiLFxuICAgIGNsYXNzOiBbX3ZtLmlzc3VlID09ICcxJyB8fCBfdm0uY2xvc2VkID09ICcxJyA/IF92bS5oZWlnaHQgOiAnJ10sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ3BhbmVsLWJvZHktJyArIF92bS51c2VySWRcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvdyBhbGVydC1yZW1pbmRlclwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImFicyBhbGVydCBhbGVydC1ncmV5IGFsZXJ0LWRpc21pc3NhYmxlXCJcbiAgfSwgW19jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNsb3NlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIiNcIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwiYWxlcnRcIixcbiAgICAgIFwiYXJpYS1sYWJlbFwiOiBcImNsb3NlXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCLDl1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3Ryb25nJywgW192bS5fdihfdm0uX3MoX3ZtLmxhbmd1YWdlW1wicmVtaW5kZXJcIl0pICsgXCI6XCIpXSksIF92bS5fdihcIiBcIiArIF92bS5fcyhfdm0ubGFuZ3VhZ2VbJ2FsZXJ0LW1zZyddKSArIFwiXFxyXFxuICAgICAgICBcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5pc3N1ZSA9PSBcIjBcIiksXG4gICAgICBleHByZXNzaW9uOiBcImlzc3VlPT1cXFwiMFxcXCJcIlxuICAgIH1dXG4gIH0sIFtfYygnaW5maW5pdGUtbG9hZGluZycsIHtcbiAgICByZWY6IFwiaW5maW5pdGVMb2FkaW5nXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwib24taW5maW5pdGVcIjogX3ZtLm9uSW5maW5pdGUsXG4gICAgICBcImRpcmVjdGlvblwiOiBcInRvcFwiLFxuICAgICAgXCJkaXN0YW5jZVwiOiAwXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3VsJywgX3ZtLl9sKChfdm0ubWVzc2FnZXMpLCBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgcmV0dXJuIF9jKCdsaScsIHtcbiAgICAgIGNsYXNzOiB7XG4gICAgICAgIG1lOiBfdm0uaXNNZShtZXNzYWdlKVxuICAgICAgfVxuICAgIH0sIFsoIV92bS5pc01lKG1lc3NhZ2UpKSA/IF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJzb21lb25lTWVzc2FnZVwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiBjb2wtbWQtMlwiXG4gICAgfSwgW19jKCdpbWcnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZSBjaGF0LWF2YXRhclwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJzcmNcIjogbWVzc2FnZS51c2VyLmF2YXRhcixcbiAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcbiAgICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcImxlZnRcIixcbiAgICAgICAgXCJkYXRhLWNvbnRhaW5lclwiOiBcIiNjaGF0LWJhclwiLFxuICAgICAgICBcImRhdGEtdGVtcC10aXRsZVwiOiBtZXNzYWdlLnVzZXIuZnVsbF9uYW1lLFxuICAgICAgICBcInRpdGxlXCI6IG1lc3NhZ2UudXNlci5mdWxsX25hbWUgKyAnICcgKyBtZXNzYWdlLmNyZWF0ZWRfYXRcbiAgICAgIH1cbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTkgY29sLW1kLTlcIlxuICAgIH0sIFsobWVzc2FnZS51c2VyLm5pY2tfbmFtZSAhPT0gbnVsbCkgPyBfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInVzZXJuYW1lXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhtZXNzYWdlLnVzZXIubmlja19uYW1lKSldKSA6IF9jKCdzcGFuJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwidXNlcm5hbWVcIlxuICAgIH0sIFtfdm0uX3YoX3ZtLl9zKG1lc3NhZ2UudXNlci5maXJzdF9uYW1lKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2JyJyksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibWVzc2FnZVwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcbiAgICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcImxlZnRcIixcbiAgICAgICAgXCJkYXRhLWNvbnRhaW5lclwiOiBcIiNjaGF0LWJhclwiLFxuICAgICAgICBcInRpdGxlXCI6IG1lc3NhZ2UudXNlci5mdWxsX25hbWUgKyAnICcgKyBtZXNzYWdlLmNyZWF0ZWRfYXRcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KF92bS5fcyhtZXNzYWdlLmJvZHkpKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmlzTWUobWVzc2FnZSkpID8gX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJtZXNzYWdlXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgICBcImRhdGEtcGxhY2VtZW50XCI6IFwibGVmdFwiLFxuICAgICAgICBcImRhdGEtY29udGFpbmVyXCI6IFwiI2NoYXQtYmFyXCIsXG4gICAgICAgIFwidGl0bGVcIjogbWVzc2FnZS5jcmVhdGVkX2F0XG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKG1lc3NhZ2UuYm9keSkgKyBcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgXCIpXSkgOiBfdm0uX2UoKV0pXG4gIH0pKV0sIDEpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICB2YWx1ZTogKF92bS5pc3N1ZSA9PSBcIjFcIiksXG4gICAgICBleHByZXNzaW9uOiBcImlzc3VlPT1cXFwiMVxcXCJcIlxuICAgIH1dXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRoYW5rc1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sYW5ndWFnZVtcInRoYW5rLXlvdVwiXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdicicpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2JywgW192bS5fdihfdm0uX3MoX3ZtLmxhbmd1YWdlW1wiY2FzZS1zb2x2ZWRcIl0pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNkLWZpbHRlci1ibG9ja1wiXG4gIH0sIFtfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2QtZmlsdGVyLWNvbnRlbnQgY2QtZmlsdGVycyBsaXN0XCJcbiAgfSwgW19jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtYXJnaW5cIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmZvcm0uY2FzZV9zb2x2ZWQpLFxuICAgICAgZXhwcmVzc2lvbjogXCJmb3JtLmNhc2Vfc29sdmVkXCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmaWx0ZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLWZpbHRlclwiOiBcIi5yYWRpbzFcIixcbiAgICAgIFwidHlwZVwiOiBcInJhZGlvXCIsXG4gICAgICBcInZhbHVlXCI6IFwieWVzXCIsXG4gICAgICBcIm5hbWVcIjogXCJjYXNlX3NvbHZlZFwiLFxuICAgICAgXCJpZFwiOiBcInllc1wiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IF92bS5fcShfdm0uZm9ybS5jYXNlX3NvbHZlZCwgXCJ5ZXNcIilcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcIl9fY1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZvcm0uY2FzZV9zb2x2ZWQgPSBcInllc1wiXG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJhZGlvLWxhYmVsXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZm9yXCI6IFwicmFkaW8xXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sYW5ndWFnZVtcInllc1wiXSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1hcmdpblwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uZm9ybS5jYXNlX3NvbHZlZCksXG4gICAgICBleHByZXNzaW9uOiBcImZvcm0uY2FzZV9zb2x2ZWRcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZpbHRlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtZmlsdGVyXCI6IFwiLnJhZGlvMlwiLFxuICAgICAgXCJ0eXBlXCI6IFwicmFkaW9cIixcbiAgICAgIFwidmFsdWVcIjogXCJub1wiLFxuICAgICAgXCJuYW1lXCI6IFwiY2FzZV9zb2x2ZWRcIixcbiAgICAgIFwiaWRcIjogXCJub1wiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IF92bS5fcShfdm0uZm9ybS5jYXNlX3NvbHZlZCwgXCJub1wiKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiX19jXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZm9ybS5jYXNlX3NvbHZlZCA9IFwibm9cIlxuICAgICAgfVxuICAgIH1cbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdsYWJlbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyYWRpby1sYWJlbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImZvclwiOiBcInJhZGlvMlwiXG4gICAgfVxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubGFuZ3VhZ2VbXCJub1wiXSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5mb3JtLmVycm9ycy5oYXMoJ2Nhc2Vfc29sdmVkJykpID8gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9jayBlcnJvclwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sYW5ndWFnZVtcInBsZWFzZVwiXSkpXSkgOiBfdm0uX2UoKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCBbX3ZtLl92KF92bS5fcyhfdm0ubGFuZ3VhZ2VbXCJyYXRlLWFnZW50XCJdKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjZC1maWx0ZXItYmxvY2tcIlxuICB9LCBbX2MoJ3VsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNkLWZpbHRlci1jb250ZW50IGNkLWZpbHRlcnMgbGlzdFwiXG4gIH0sIFtfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibWFyZ2luXCJcbiAgfSwgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5mb3JtLnJhdGVfYWdlbnQpLFxuICAgICAgZXhwcmVzc2lvbjogXCJmb3JtLnJhdGVfYWdlbnRcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZpbHRlclwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtZmlsdGVyXCI6IFwiLnJhZGlvMVwiLFxuICAgICAgXCJ0eXBlXCI6IFwicmFkaW9cIixcbiAgICAgIFwidmFsdWVcIjogXCJnb29kXCIsXG4gICAgICBcIm5hbWVcIjogXCJyYXRlX2FnZW50XCIsXG4gICAgICBcImlkXCI6IFwiZ29vZFwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJjaGVja2VkXCI6IF92bS5fcShfdm0uZm9ybS5yYXRlX2FnZW50LCBcImdvb2RcIilcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcIl9fY1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZvcm0ucmF0ZV9hZ2VudCA9IFwiZ29vZFwiXG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJhZGlvLWxhYmVsXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZm9yXCI6IFwicmFkaW8xXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sYW5ndWFnZVtcImdvb2RcIl0pKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtYXJnaW5cIlxuICB9LCBbX2MoJ2lucHV0Jywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLmZvcm0ucmF0ZV9hZ2VudCksXG4gICAgICBleHByZXNzaW9uOiBcImZvcm0ucmF0ZV9hZ2VudFwiXG4gICAgfV0sXG4gICAgc3RhdGljQ2xhc3M6IFwiZmlsdGVyXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1maWx0ZXJcIjogXCIucmFkaW8yXCIsXG4gICAgICBcInR5cGVcIjogXCJyYWRpb1wiLFxuICAgICAgXCJ2YWx1ZVwiOiBcImF2ZXJhZ2VcIixcbiAgICAgIFwibmFtZVwiOiBcInJhdGVfYWdlbnRcIixcbiAgICAgIFwiaWRcIjogXCJhdmVyYWdlXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcImNoZWNrZWRcIjogX3ZtLl9xKF92bS5mb3JtLnJhdGVfYWdlbnQsIFwiYXZlcmFnZVwiKVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiX19jXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZm9ybS5yYXRlX2FnZW50ID0gXCJhdmVyYWdlXCJcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicmFkaW8tbGFiZWxcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJmb3JcIjogXCJyYWRpbzJcIlxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLmxhbmd1YWdlW1wiYXZlcmFnZVwiXSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1hcmdpblwiXG4gIH0sIFtfYygnaW5wdXQnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0uZm9ybS5yYXRlX2FnZW50KSxcbiAgICAgIGV4cHJlc3Npb246IFwiZm9ybS5yYXRlX2FnZW50XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmaWx0ZXJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJkYXRhLWZpbHRlclwiOiBcIi5yYWRpbzNcIixcbiAgICAgIFwidHlwZVwiOiBcInJhZGlvXCIsXG4gICAgICBcInZhbHVlXCI6IFwiYmFkXCIsXG4gICAgICBcIm5hbWVcIjogXCJyYXRlX2FnZW50XCIsXG4gICAgICBcImlkXCI6IFwiYmFkXCJcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcImNoZWNrZWRcIjogX3ZtLl9xKF92bS5mb3JtLnJhdGVfYWdlbnQsIFwiYmFkXCIpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJfX2NcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5mb3JtLnJhdGVfYWdlbnQgPSBcImJhZFwiXG4gICAgICB9XG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJhZGlvLWxhYmVsXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZm9yXCI6IFwicmFkaW8zXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sYW5ndWFnZVtcImJhZFwiXSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5mb3JtLmVycm9ycy5oYXMoJ2Nhc2Vfc29sdmVkJykpID8gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9jayBlcnJvclwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sYW5ndWFnZVtcInBsZWFzZVwiXSkpXSkgOiBfdm0uX2UoKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCBbX3ZtLl92KF92bS5fcyhfdm0ubGFuZ3VhZ2VbXCJjb21tZW50c1wiXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZXh0YXJlYScsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5mb3JtLmNvbW1lbnRzKSxcbiAgICAgIGV4cHJlc3Npb246IFwiZm9ybS5jb21tZW50c1wiXG4gICAgfV0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJhZGQtY29tbWVudFwiLFxuICAgICAgXCJyb3dzXCI6IDQsXG4gICAgICBcIm5hbWVcIjogXCJjb21tZW50c1wiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLmZvcm0uY29tbWVudHMpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uZm9ybS5jb21tZW50cyA9ICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWxpZ2h0LWdyZWVuXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJzdWJtaXRcIixcbiAgICAgIFwiaHJlZlwiOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnNhdmVJc3N1ZUZlZWRiYWNrKClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sYW5ndWFnZVtcInN1Ym1pdFwiXSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uY2xvc2VkID09IFwiMVwiKSxcbiAgICAgIGV4cHJlc3Npb246IFwiY2xvc2VkPT1cXFwiMVxcXCJcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcInRoYW5rc1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5sYW5ndWFnZVtcImlzc3VlXCJdKSArIFwiIDopXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uaXNzdWUgPT0gXCIwXCIgJiYgX3ZtLmNsb3NlZCA9PSBcIjBcIiksXG4gICAgICBleHByZXNzaW9uOiBcImlzc3VlPT1cXFwiMFxcXCIgJiYgY2xvc2VkPT1cXFwiMFxcXCJcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWZvb3RlclwiXG4gIH0sIFtfYygndGV4dGFyZWEnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubmV3TWVzc2FnZSksXG4gICAgICBleHByZXNzaW9uOiBcIm5ld01lc3NhZ2VcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImpzLWF1dG8tc2l6ZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtY2hhdFwiOiAncGFuZWwtYm9keS0nICsgX3ZtLnVzZXJJZCxcbiAgICAgIFwiaWRcIjogJ2lkJyArIF92bS51c2VySWQsXG4gICAgICBcInJvd3NcIjogMSxcbiAgICAgIFwicGxhY2Vob2xkZXJcIjogX3ZtLmxhbmd1YWdlWyd0eXBlLWEtbWVzc2FnZSddLFxuICAgICAgXCJkaXNhYmxlZFwiOiBfdm0uaXNibG9jayA9PSAnQmxvY2tlZCdcbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0ubmV3TWVzc2FnZSlcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImtleWRvd25cIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICghKCdidXR0b24nIGluICRldmVudCkgJiYgX3ZtLl9rKCRldmVudC5rZXlDb2RlLCBcImVudGVyXCIsIDEzKSkgeyByZXR1cm4gbnVsbDsgfVxuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgX3ZtLnNlbmQoJGV2ZW50KVxuICAgICAgfSxcbiAgICAgIFwiZm9jdXNcIjogX3ZtLm1hcmtBbGxBc1NlZW4sXG4gICAgICBcImlucHV0XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHsgcmV0dXJuOyB9XG4gICAgICAgIF92bS5uZXdNZXNzYWdlID0gJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgfVxuICAgIH1cbiAgfSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtYmVmNjNlNzRcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1iZWY2M2U3NFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9DaGF0Qm94LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDAwXG4vLyBtb2R1bGUgY2h1bmtzID0gMTYiLCIhZnVuY3Rpb24ocCx4KXtcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cyYmXCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZT9tb2R1bGUuZXhwb3J0cz14KCk6XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXSx4KTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9leHBvcnRzLlZ1ZUluZmluaXRlTG9hZGluZz14KCk6cC5WdWVJbmZpbml0ZUxvYWRpbmc9eCgpfSh0aGlzLGZ1bmN0aW9uKCl7cmV0dXJuIGZ1bmN0aW9uKHApe2Z1bmN0aW9uIHgoYSl7aWYodFthXSlyZXR1cm4gdFthXS5leHBvcnRzO3ZhciBlPXRbYV09e2V4cG9ydHM6e30saWQ6YSxsb2FkZWQ6ITF9O3JldHVybiBwW2FdLmNhbGwoZS5leHBvcnRzLGUsZS5leHBvcnRzLHgpLGUubG9hZGVkPSEwLGUuZXhwb3J0c312YXIgdD17fTtyZXR1cm4geC5tPXAseC5jPXQseC5wPVwiL1wiLHgoMCl9KFtmdW5jdGlvbihwLHgsdCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gYShwKXtyZXR1cm4gcCYmcC5fX2VzTW9kdWxlP3A6e2RlZmF1bHQ6cH19T2JqZWN0LmRlZmluZVByb3BlcnR5KHgsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSk7dmFyIGU9dCg0KSxvPWEoZSk7eC5kZWZhdWx0PW8uZGVmYXVsdCxcInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93JiZ3aW5kb3cuVnVlJiZ3aW5kb3cuVnVlLmNvbXBvbmVudChcImluZmluaXRlLWxvYWRpbmdcIixvLmRlZmF1bHQpfSxmdW5jdGlvbihwLHgpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHQocCl7cmV0dXJuXCJCT0RZXCI9PT1wLnRhZ05hbWU/d2luZG93OltcInNjcm9sbFwiLFwiYXV0b1wiXS5pbmRleE9mKGdldENvbXB1dGVkU3R5bGUocCkub3ZlcmZsb3dZKT4tMT9wOnAuaGFzQXR0cmlidXRlKFwiaW5maW5pdGUtd3JhcHBlclwiKXx8cC5oYXNBdHRyaWJ1dGUoXCJkYXRhLWluZmluaXRlLXdyYXBwZXJcIik/cDp0KHAucGFyZW50Tm9kZSl9ZnVuY3Rpb24gYShwLHgsdCl7dmFyIGE9dm9pZCAwO2lmKFwidG9wXCI9PT10KWE9aXNOYU4ocC5zY3JvbGxUb3ApP3AucGFnZVlPZmZzZXQ6cC5zY3JvbGxUb3A7ZWxzZXt2YXIgZT14LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcCxvPXA9PT13aW5kb3c/d2luZG93LmlubmVySGVpZ2h0OnAuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuYm90dG9tO2E9ZS1vfXJldHVybiBhfU9iamVjdC5kZWZpbmVQcm9wZXJ0eSh4LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pO3ZhciBlPXtidWJibGVzOlwibG9hZGluZy1idWJibGVzXCIsY2lyY2xlczpcImxvYWRpbmctY2lyY2xlc1wiLGRlZmF1bHQ6XCJsb2FkaW5nLWRlZmF1bHRcIixzcGlyYWw6XCJsb2FkaW5nLXNwaXJhbFwiLHdhdmVEb3RzOlwibG9hZGluZy13YXZlLWRvdHNcIn07eC5kZWZhdWx0PXtkYXRhOmZ1bmN0aW9uKCl7cmV0dXJue3Njcm9sbFBhcmVudDpudWxsLHNjcm9sbEhhbmRsZXI6bnVsbCxpc0xvYWRpbmc6ITEsaXNDb21wbGV0ZTohMSxpc0ZpcnN0TG9hZDohMH19LGNvbXB1dGVkOntzcGlubmVyVHlwZTpmdW5jdGlvbigpe3JldHVybiBlW3RoaXMuc3Bpbm5lcl18fGUuZGVmYXVsdH19LHByb3BzOntkaXN0YW5jZTp7dHlwZTpOdW1iZXIsZGVmYXVsdDoxMDB9LG9uSW5maW5pdGU6RnVuY3Rpb24sc3Bpbm5lcjpTdHJpbmcsZGlyZWN0aW9uOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiYm90dG9tXCJ9fSxtb3VudGVkOmZ1bmN0aW9uKCl7dmFyIHA9dGhpczt0aGlzLnNjcm9sbFBhcmVudD10KHRoaXMuJGVsKSx0aGlzLnNjcm9sbEhhbmRsZXI9ZnVuY3Rpb24oKXt0aGlzLmlzTG9hZGluZ3x8dGhpcy5hdHRlbXB0TG9hZCgpfS5iaW5kKHRoaXMpLHNldFRpbWVvdXQodGhpcy5zY3JvbGxIYW5kbGVyLDEpLHRoaXMuc2Nyb2xsUGFyZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIix0aGlzLnNjcm9sbEhhbmRsZXIpLHRoaXMuJG9uKFwiJEluZmluaXRlTG9hZGluZzpsb2FkZWRcIixmdW5jdGlvbigpe3AuaXNGaXJzdExvYWQ9ITEscC5pc0xvYWRpbmcmJnAuJG5leHRUaWNrKHAuYXR0ZW1wdExvYWQpfSksdGhpcy4kb24oXCIkSW5maW5pdGVMb2FkaW5nOmNvbXBsZXRlXCIsZnVuY3Rpb24oKXtwLmlzTG9hZGluZz0hMSxwLmlzQ29tcGxldGU9ITAscC5zY3JvbGxQYXJlbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHAuc2Nyb2xsSGFuZGxlcil9KSx0aGlzLiRvbihcIiRJbmZpbml0ZUxvYWRpbmc6cmVzZXRcIixmdW5jdGlvbigpe3AuaXNMb2FkaW5nPSExLHAuaXNDb21wbGV0ZT0hMSxwLmlzRmlyc3RMb2FkPSEwLHAuc2Nyb2xsUGFyZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIixwLnNjcm9sbEhhbmRsZXIpLHNldFRpbWVvdXQocC5zY3JvbGxIYW5kbGVyLDEpfSl9LGRlYWN0aXZhdGVkOmZ1bmN0aW9uKCl7dGhpcy5pc0xvYWRpbmc9ITEsdGhpcy5zY3JvbGxQYXJlbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHRoaXMuc2Nyb2xsSGFuZGxlcil9LGFjdGl2YXRlZDpmdW5jdGlvbigpe3RoaXMuc2Nyb2xsUGFyZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIix0aGlzLnNjcm9sbEhhbmRsZXIpfSxtZXRob2RzOnthdHRlbXB0TG9hZDpmdW5jdGlvbigpe3ZhciBwPWEodGhpcy5zY3JvbGxQYXJlbnQsdGhpcy4kZWwsdGhpcy5kaXJlY3Rpb24pOyF0aGlzLmlzQ29tcGxldGUmJnA8PXRoaXMuZGlzdGFuY2U/KHRoaXMuaXNMb2FkaW5nPSEwLHRoaXMub25JbmZpbml0ZS5jYWxsKCkpOnRoaXMuaXNMb2FkaW5nPSExfX0sZGVzdHJveWVkOmZ1bmN0aW9uKCl7dGhpcy5pc0NvbXBsZXRlfHx0aGlzLnNjcm9sbFBhcmVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsdGhpcy5zY3JvbGxIYW5kbGVyKX19fSxmdW5jdGlvbihwLHgsdCl7eD1wLmV4cG9ydHM9dCgzKSgpLHgucHVzaChbcC5pZCwnLmxvYWRpbmctd2F2ZS1kb3RzW2RhdGEtdi01MDc5M2YwMl17cG9zaXRpb246cmVsYXRpdmV9LmxvYWRpbmctd2F2ZS1kb3RzW2RhdGEtdi01MDc5M2YwMl06YmVmb3Jle2NvbnRlbnQ6XCJcIjtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6NTAlO2xlZnQ6NTAlO21hcmdpbi1sZWZ0Oi00cHg7bWFyZ2luLXRvcDotNHB4O3dpZHRoOjhweDtoZWlnaHQ6OHB4O2JhY2tncm91bmQtY29sb3I6I2JiYjtib3JkZXItcmFkaXVzOjUwJTstd2Via2l0LWFuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy13YXZlLWRvdHMgMi44cyBpbmZpbml0ZTthbmltYXRpb246bGluZWFyIGxvYWRpbmctd2F2ZS1kb3RzIDIuOHMgaW5maW5pdGV9QC13ZWJraXQta2V5ZnJhbWVzIGxvYWRpbmctd2F2ZS1kb3RzezAle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJifTUle2JveC1zaGFkb3c6LTMycHggLTRweCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MTAle2JveC1zaGFkb3c6LTMycHggLTZweCAwICM5OTksLTE2cHggLTRweCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MTUle2JveC1zaGFkb3c6LTMycHggMnB4IDAgI2JiYiwtMTZweCAtMnB4IDAgIzk5OSwxNnB4IDRweCAwICNiYmIsMzJweCA0cHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTRweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTRweCk7YmFja2dyb3VuZC1jb2xvcjojYmJifTIwJXtib3gtc2hhZG93Oi0zMnB4IDZweCAwICNiYmIsLTE2cHggNHB4IDAgI2JiYiwxNnB4IDJweCAwICNiYmIsMzJweCA2cHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTZweCk7YmFja2dyb3VuZC1jb2xvcjojOTk5fTI1JXtib3gtc2hhZG93Oi0zMnB4IDJweCAwICNiYmIsLTE2cHggMnB4IDAgI2JiYiwxNnB4IC00cHggMCAjOTk5LDMycHggLTJweCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtMnB4KTtiYWNrZ3JvdW5kLWNvbG9yOiNiYmJ9MzAle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAtMnB4IDAgI2JiYiwzMnB4IC02cHggMCAjOTk5Oy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MzUle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IC0ycHggMCAjYmJifTQwJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn10b3tib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn19QGtleWZyYW1lcyBsb2FkaW5nLXdhdmUtZG90c3swJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn01JXtib3gtc2hhZG93Oi0zMnB4IC00cHggMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTEwJXtib3gtc2hhZG93Oi0zMnB4IC02cHggMCAjOTk5LC0xNnB4IC00cHggMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTE1JXtib3gtc2hhZG93Oi0zMnB4IDJweCAwICNiYmIsLTE2cHggLTJweCAwICM5OTksMTZweCA0cHggMCAjYmJiLDMycHggNHB4IDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC00cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC00cHgpO2JhY2tncm91bmQtY29sb3I6I2JiYn0yMCV7Ym94LXNoYWRvdzotMzJweCA2cHggMCAjYmJiLC0xNnB4IDRweCAwICNiYmIsMTZweCAycHggMCAjYmJiLDMycHggNnB4IDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC02cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC02cHgpO2JhY2tncm91bmQtY29sb3I6Izk5OX0yNSV7Ym94LXNoYWRvdzotMzJweCAycHggMCAjYmJiLC0xNnB4IDJweCAwICNiYmIsMTZweCAtNHB4IDAgIzk5OSwzMnB4IC0ycHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTJweCk7YmFja2dyb3VuZC1jb2xvcjojYmJifTMwJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggLTJweCAwICNiYmIsMzJweCAtNnB4IDAgIzk5OTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTM1JXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAtMnB4IDAgI2JiYn00MCV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmJ9dG97Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmJ9fS5sb2FkaW5nLWNpcmNsZXNbZGF0YS12LTUwNzkzZjAyXXtwb3NpdGlvbjpyZWxhdGl2ZX0ubG9hZGluZy1jaXJjbGVzW2RhdGEtdi01MDc5M2YwMl06YmVmb3Jle2NvbnRlbnQ6XCJcIjtwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjUwJTt0b3A6NTAlO21hcmdpbi10b3A6LTIuNXB4O21hcmdpbi1sZWZ0Oi0yLjVweDt3aWR0aDo1cHg7aGVpZ2h0OjVweDtib3JkZXItcmFkaXVzOjUwJTstd2Via2l0LWFuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1jaXJjbGVzIC43NXMgaW5maW5pdGU7YW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLWNpcmNsZXMgLjc1cyBpbmZpbml0ZX1ALXdlYmtpdC1rZXlmcmFtZXMgbG9hZGluZy1jaXJjbGVzezAle2JveC1zaGFkb3c6MCAtMTJweCAwICM1MDUwNTAsOC41MnB4IC04LjUycHggMCAjNjQ2NDY0LDEycHggMCAwICM3OTc5NzksOC41MnB4IDguNTJweCAwICM4ZDhkOGQsMCAxMnB4IDAgI2EyYTJhMiwtOC41MnB4IDguNTJweCAwICNiNmI2YjYsLTEycHggMCAwICNjYWNhY2EsLTguNTJweCAtOC41MnB4IDAgI2RmZGZkZn0xMi41JXtib3gtc2hhZG93OjAgLTEycHggMCAjZGZkZmRmLDguNTJweCAtOC41MnB4IDAgIzUwNTA1MCwxMnB4IDAgMCAjNjQ2NDY0LDguNTJweCA4LjUycHggMCAjNzk3OTc5LDAgMTJweCAwICM4ZDhkOGQsLTguNTJweCA4LjUycHggMCAjYTJhMmEyLC0xMnB4IDAgMCAjYjZiNmI2LC04LjUycHggLTguNTJweCAwICNjYWNhY2F9MjUle2JveC1zaGFkb3c6MCAtMTJweCAwICNjYWNhY2EsOC41MnB4IC04LjUycHggMCAjZGZkZmRmLDEycHggMCAwICM1MDUwNTAsOC41MnB4IDguNTJweCAwICM2NDY0NjQsMCAxMnB4IDAgIzc5Nzk3OSwtOC41MnB4IDguNTJweCAwICM4ZDhkOGQsLTEycHggMCAwICNhMmEyYTIsLTguNTJweCAtOC41MnB4IDAgI2I2YjZiNn0zNy41JXtib3gtc2hhZG93OjAgLTEycHggMCAjYjZiNmI2LDguNTJweCAtOC41MnB4IDAgI2NhY2FjYSwxMnB4IDAgMCAjZGZkZmRmLDguNTJweCA4LjUycHggMCAjNTA1MDUwLDAgMTJweCAwICM2NDY0NjQsLTguNTJweCA4LjUycHggMCAjNzk3OTc5LC0xMnB4IDAgMCAjOGQ4ZDhkLC04LjUycHggLTguNTJweCAwICNhMmEyYTJ9NTAle2JveC1zaGFkb3c6MCAtMTJweCAwICNhMmEyYTIsOC41MnB4IC04LjUycHggMCAjYjZiNmI2LDEycHggMCAwICNjYWNhY2EsOC41MnB4IDguNTJweCAwICNkZmRmZGYsMCAxMnB4IDAgIzUwNTA1MCwtOC41MnB4IDguNTJweCAwICM2NDY0NjQsLTEycHggMCAwICM3OTc5NzksLTguNTJweCAtOC41MnB4IDAgIzhkOGQ4ZH02Mi41JXtib3gtc2hhZG93OjAgLTEycHggMCAjOGQ4ZDhkLDguNTJweCAtOC41MnB4IDAgI2EyYTJhMiwxMnB4IDAgMCAjYjZiNmI2LDguNTJweCA4LjUycHggMCAjY2FjYWNhLDAgMTJweCAwICNkZmRmZGYsLTguNTJweCA4LjUycHggMCAjNTA1MDUwLC0xMnB4IDAgMCAjNjQ2NDY0LC04LjUycHggLTguNTJweCAwICM3OTc5Nzl9NzUle2JveC1zaGFkb3c6MCAtMTJweCAwICM3OTc5NzksOC41MnB4IC04LjUycHggMCAjOGQ4ZDhkLDEycHggMCAwICNhMmEyYTIsOC41MnB4IDguNTJweCAwICNiNmI2YjYsMCAxMnB4IDAgI2NhY2FjYSwtOC41MnB4IDguNTJweCAwICNkZmRmZGYsLTEycHggMCAwICM1MDUwNTAsLTguNTJweCAtOC41MnB4IDAgIzY0NjQ2NH04Ny41JXtib3gtc2hhZG93OjAgLTEycHggMCAjNjQ2NDY0LDguNTJweCAtOC41MnB4IDAgIzc5Nzk3OSwxMnB4IDAgMCAjOGQ4ZDhkLDguNTJweCA4LjUycHggMCAjYTJhMmEyLDAgMTJweCAwICNiNmI2YjYsLTguNTJweCA4LjUycHggMCAjY2FjYWNhLC0xMnB4IDAgMCAjZGZkZmRmLC04LjUycHggLTguNTJweCAwICM1MDUwNTB9dG97Ym94LXNoYWRvdzowIC0xMnB4IDAgIzUwNTA1MCw4LjUycHggLTguNTJweCAwICM2NDY0NjQsMTJweCAwIDAgIzc5Nzk3OSw4LjUycHggOC41MnB4IDAgIzhkOGQ4ZCwwIDEycHggMCAjYTJhMmEyLC04LjUycHggOC41MnB4IDAgI2I2YjZiNiwtMTJweCAwIDAgI2NhY2FjYSwtOC41MnB4IC04LjUycHggMCAjZGZkZmRmfX1Aa2V5ZnJhbWVzIGxvYWRpbmctY2lyY2xlc3swJXtib3gtc2hhZG93OjAgLTEycHggMCAjNTA1MDUwLDguNTJweCAtOC41MnB4IDAgIzY0NjQ2NCwxMnB4IDAgMCAjNzk3OTc5LDguNTJweCA4LjUycHggMCAjOGQ4ZDhkLDAgMTJweCAwICNhMmEyYTIsLTguNTJweCA4LjUycHggMCAjYjZiNmI2LC0xMnB4IDAgMCAjY2FjYWNhLC04LjUycHggLTguNTJweCAwICNkZmRmZGZ9MTIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2RmZGZkZiw4LjUycHggLTguNTJweCAwICM1MDUwNTAsMTJweCAwIDAgIzY0NjQ2NCw4LjUycHggOC41MnB4IDAgIzc5Nzk3OSwwIDEycHggMCAjOGQ4ZDhkLC04LjUycHggOC41MnB4IDAgI2EyYTJhMiwtMTJweCAwIDAgI2I2YjZiNiwtOC41MnB4IC04LjUycHggMCAjY2FjYWNhfTI1JXtib3gtc2hhZG93OjAgLTEycHggMCAjY2FjYWNhLDguNTJweCAtOC41MnB4IDAgI2RmZGZkZiwxMnB4IDAgMCAjNTA1MDUwLDguNTJweCA4LjUycHggMCAjNjQ2NDY0LDAgMTJweCAwICM3OTc5NzksLTguNTJweCA4LjUycHggMCAjOGQ4ZDhkLC0xMnB4IDAgMCAjYTJhMmEyLC04LjUycHggLTguNTJweCAwICNiNmI2YjZ9MzcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2I2YjZiNiw4LjUycHggLTguNTJweCAwICNjYWNhY2EsMTJweCAwIDAgI2RmZGZkZiw4LjUycHggOC41MnB4IDAgIzUwNTA1MCwwIDEycHggMCAjNjQ2NDY0LC04LjUycHggOC41MnB4IDAgIzc5Nzk3OSwtMTJweCAwIDAgIzhkOGQ4ZCwtOC41MnB4IC04LjUycHggMCAjYTJhMmEyfTUwJXtib3gtc2hhZG93OjAgLTEycHggMCAjYTJhMmEyLDguNTJweCAtOC41MnB4IDAgI2I2YjZiNiwxMnB4IDAgMCAjY2FjYWNhLDguNTJweCA4LjUycHggMCAjZGZkZmRmLDAgMTJweCAwICM1MDUwNTAsLTguNTJweCA4LjUycHggMCAjNjQ2NDY0LC0xMnB4IDAgMCAjNzk3OTc5LC04LjUycHggLTguNTJweCAwICM4ZDhkOGR9NjIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzhkOGQ4ZCw4LjUycHggLTguNTJweCAwICNhMmEyYTIsMTJweCAwIDAgI2I2YjZiNiw4LjUycHggOC41MnB4IDAgI2NhY2FjYSwwIDEycHggMCAjZGZkZmRmLC04LjUycHggOC41MnB4IDAgIzUwNTA1MCwtMTJweCAwIDAgIzY0NjQ2NCwtOC41MnB4IC04LjUycHggMCAjNzk3OTc5fTc1JXtib3gtc2hhZG93OjAgLTEycHggMCAjNzk3OTc5LDguNTJweCAtOC41MnB4IDAgIzhkOGQ4ZCwxMnB4IDAgMCAjYTJhMmEyLDguNTJweCA4LjUycHggMCAjYjZiNmI2LDAgMTJweCAwICNjYWNhY2EsLTguNTJweCA4LjUycHggMCAjZGZkZmRmLC0xMnB4IDAgMCAjNTA1MDUwLC04LjUycHggLTguNTJweCAwICM2NDY0NjR9ODcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzY0NjQ2NCw4LjUycHggLTguNTJweCAwICM3OTc5NzksMTJweCAwIDAgIzhkOGQ4ZCw4LjUycHggOC41MnB4IDAgI2EyYTJhMiwwIDEycHggMCAjYjZiNmI2LC04LjUycHggOC41MnB4IDAgI2NhY2FjYSwtMTJweCAwIDAgI2RmZGZkZiwtOC41MnB4IC04LjUycHggMCAjNTA1MDUwfXRve2JveC1zaGFkb3c6MCAtMTJweCAwICM1MDUwNTAsOC41MnB4IC04LjUycHggMCAjNjQ2NDY0LDEycHggMCAwICM3OTc5NzksOC41MnB4IDguNTJweCAwICM4ZDhkOGQsMCAxMnB4IDAgI2EyYTJhMiwtOC41MnB4IDguNTJweCAwICNiNmI2YjYsLTEycHggMCAwICNjYWNhY2EsLTguNTJweCAtOC41MnB4IDAgI2RmZGZkZn19LmxvYWRpbmctYnViYmxlc1tkYXRhLXYtNTA3OTNmMDJde3Bvc2l0aW9uOnJlbGF0aXZlfS5sb2FkaW5nLWJ1YmJsZXNbZGF0YS12LTUwNzkzZjAyXTpiZWZvcmV7Y29udGVudDpcIlwiO3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6NTAlO3RvcDo1MCU7bWFyZ2luLXRvcDotLjVweDttYXJnaW4tbGVmdDotLjVweDt3aWR0aDoxcHg7aGVpZ2h0OjFweDtib3JkZXItcmFkaXVzOjUwJTstd2Via2l0LWFuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1idWJibGVzIC44NXMgaW5maW5pdGU7YW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLWJ1YmJsZXMgLjg1cyBpbmZpbml0ZX1ALXdlYmtpdC1rZXlmcmFtZXMgbG9hZGluZy1idWJibGVzezAle2JveC1zaGFkb3c6MCAtMTJweCAwIC40cHggIzY2Niw4LjUycHggLTguNTJweCAwIC44cHggIzY2NiwxMnB4IDAgMCAxLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjZweCAjNjY2LDAgMTJweCAwIDJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwtMTJweCAwIDAgMi44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAzLjJweCAjNjY2fTEyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDMuMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuNHB4ICM2NjYsMTJweCAwIDAgLjhweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjJweCAjNjY2LDAgMTJweCAwIDEuNnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAycHggIzY2NiwtMTJweCAwIDAgMi40cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAyLjhweCAjNjY2fTI1JXtib3gtc2hhZG93OjAgLTEycHggMCAyLjhweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2NiwxMnB4IDAgMCAuNHB4ICM2NjYsOC41MnB4IDguNTJweCAwIC44cHggIzY2NiwwIDEycHggMCAxLjJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwtMTJweCAwIDAgMnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMi40cHggIzY2Nn0zNy41JXtib3gtc2hhZG93OjAgLTEycHggMCAyLjRweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMi44cHggIzY2NiwxMnB4IDAgMCAzLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAuNHB4ICM2NjYsMCAxMnB4IDAgLjhweCAjNjY2LC04LjUycHggOC41MnB4IDAgMS4ycHggIzY2NiwtMTJweCAwIDAgMS42cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAycHggIzY2Nn01MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAyLjRweCAjNjY2LDEycHggMCAwIDIuOHB4ICM2NjYsOC41MnB4IDguNTJweCAwIDMuMnB4ICM2NjYsMCAxMnB4IDAgLjRweCAjNjY2LC04LjUycHggOC41MnB4IDAgLjhweCAjNjY2LC0xMnB4IDAgMCAxLjJweCAjNjY2LC04LjUycHggLTguNTJweCAwIDEuNnB4ICM2NjZ9NjIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMS42cHggIzY2Niw4LjUycHggLTguNTJweCAwIDJweCAjNjY2LDEycHggMCAwIDIuNHB4ICM2NjYsOC41MnB4IDguNTJweCAwIDIuOHB4ICM2NjYsMCAxMnB4IDAgMy4ycHggIzY2NiwtOC41MnB4IDguNTJweCAwIC40cHggIzY2NiwtMTJweCAwIDAgLjhweCAjNjY2LC04LjUycHggLTguNTJweCAwIDEuMnB4ICM2NjZ9NzUle2JveC1zaGFkb3c6MCAtMTJweCAwIDEuMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAxLjZweCAjNjY2LDEycHggMCAwIDJweCAjNjY2LDguNTJweCA4LjUycHggMCAyLjRweCAjNjY2LDAgMTJweCAwIDIuOHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAzLjJweCAjNjY2LC0xMnB4IDAgMCAuNHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgLjhweCAjNjY2fTg3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwIC44cHggIzY2Niw4LjUycHggLTguNTJweCAwIDEuMnB4ICM2NjYsMTJweCAwIDAgMS42cHggIzY2Niw4LjUycHggOC41MnB4IDAgMnB4ICM2NjYsMCAxMnB4IDAgMi40cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuOHB4ICM2NjYsLTEycHggMCAwIDMuMnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgLjRweCAjNjY2fXRve2JveC1zaGFkb3c6MCAtMTJweCAwIC40cHggIzY2Niw4LjUycHggLTguNTJweCAwIC44cHggIzY2NiwxMnB4IDAgMCAxLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjZweCAjNjY2LDAgMTJweCAwIDJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwtMTJweCAwIDAgMi44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAzLjJweCAjNjY2fX1Aa2V5ZnJhbWVzIGxvYWRpbmctYnViYmxlc3swJXtib3gtc2hhZG93OjAgLTEycHggMCAuNHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuOHB4ICM2NjYsMTJweCAwIDAgMS4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwwIDEycHggMCAycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuNHB4ICM2NjYsLTEycHggMCAwIDIuOHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2Nn0xMi41JXtib3gtc2hhZG93OjAgLTEycHggMCAzLjJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgLjRweCAjNjY2LDEycHggMCAwIC44cHggIzY2Niw4LjUycHggOC41MnB4IDAgMS4ycHggIzY2NiwwIDEycHggMCAxLjZweCAjNjY2LC04LjUycHggOC41MnB4IDAgMnB4ICM2NjYsLTEycHggMCAwIDIuNHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMi44cHggIzY2Nn0yNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMi44cHggIzY2Niw4LjUycHggLTguNTJweCAwIDMuMnB4ICM2NjYsMTJweCAwIDAgLjRweCAjNjY2LDguNTJweCA4LjUycHggMCAuOHB4ICM2NjYsMCAxMnB4IDAgMS4ycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDEuNnB4ICM2NjYsLTEycHggMCAwIDJweCAjNjY2LC04LjUycHggLTguNTJweCAwIDIuNHB4ICM2NjZ9MzcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMi40cHggIzY2Niw4LjUycHggLTguNTJweCAwIDIuOHB4ICM2NjYsMTJweCAwIDAgMy4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgLjRweCAjNjY2LDAgMTJweCAwIC44cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDEuMnB4ICM2NjYsLTEycHggMCAwIDEuNnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMnB4ICM2NjZ9NTAle2JveC1zaGFkb3c6MCAtMTJweCAwIDJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMi40cHggIzY2NiwxMnB4IDAgMCAyLjhweCAjNjY2LDguNTJweCA4LjUycHggMCAzLjJweCAjNjY2LDAgMTJweCAwIC40cHggIzY2NiwtOC41MnB4IDguNTJweCAwIC44cHggIzY2NiwtMTJweCAwIDAgMS4ycHggIzY2NiwtOC41MnB4IC04LjUycHggMCAxLjZweCAjNjY2fTYyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDEuNnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAycHggIzY2NiwxMnB4IDAgMCAyLjRweCAjNjY2LDguNTJweCA4LjUycHggMCAyLjhweCAjNjY2LDAgMTJweCAwIDMuMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAuNHB4ICM2NjYsLTEycHggMCAwIC44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAxLjJweCAjNjY2fTc1JXtib3gtc2hhZG93OjAgLTEycHggMCAxLjJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMS42cHggIzY2NiwxMnB4IDAgMCAycHggIzY2Niw4LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwwIDEycHggMCAyLjhweCAjNjY2LC04LjUycHggOC41MnB4IDAgMy4ycHggIzY2NiwtMTJweCAwIDAgLjRweCAjNjY2LC04LjUycHggLTguNTJweCAwIC44cHggIzY2Nn04Ny41JXtib3gtc2hhZG93OjAgLTEycHggMCAuOHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAxLjJweCAjNjY2LDEycHggMCAwIDEuNnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDJweCAjNjY2LDAgMTJweCAwIDIuNHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAyLjhweCAjNjY2LC0xMnB4IDAgMCAzLjJweCAjNjY2LC04LjUycHggLTguNTJweCAwIC40cHggIzY2Nn10b3tib3gtc2hhZG93OjAgLTEycHggMCAuNHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuOHB4ICM2NjYsMTJweCAwIDAgMS4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwwIDEycHggMCAycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuNHB4ICM2NjYsLTEycHggMCAwIDIuOHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2Nn19LmxvYWRpbmctZGVmYXVsdFtkYXRhLXYtNTA3OTNmMDJde3Bvc2l0aW9uOnJlbGF0aXZlO2JvcmRlcjoxcHggc29saWQgIzk5OTstd2Via2l0LWFuaW1hdGlvbjplYXNlIGxvYWRpbmctcm90YXRpbmcgMS41cyBpbmZpbml0ZTthbmltYXRpb246ZWFzZSBsb2FkaW5nLXJvdGF0aW5nIDEuNXMgaW5maW5pdGV9LmxvYWRpbmctZGVmYXVsdFtkYXRhLXYtNTA3OTNmMDJdOmJlZm9yZXtjb250ZW50OlwiXCI7cG9zaXRpb246YWJzb2x1dGU7ZGlzcGxheTpibG9jazt0b3A6MDtsZWZ0OjUwJTttYXJnaW4tdG9wOi0zcHg7bWFyZ2luLWxlZnQ6LTNweDt3aWR0aDo2cHg7aGVpZ2h0OjZweDtiYWNrZ3JvdW5kLWNvbG9yOiM5OTk7Ym9yZGVyLXJhZGl1czo1MCV9LmxvYWRpbmctc3BpcmFsW2RhdGEtdi01MDc5M2YwMl17Ym9yZGVyOjJweCBzb2xpZCAjNzc3O2JvcmRlci1yaWdodC1jb2xvcjp0cmFuc3BhcmVudDstd2Via2l0LWFuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1yb3RhdGluZyAuODVzIGluZmluaXRlO2FuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1yb3RhdGluZyAuODVzIGluZmluaXRlfUAtd2Via2l0LWtleWZyYW1lcyBsb2FkaW5nLXJvdGF0aW5nezAley13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgwKTt0cmFuc2Zvcm06cm90YXRlKDApfXRvey13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgxdHVybik7dHJhbnNmb3JtOnJvdGF0ZSgxdHVybil9fUBrZXlmcmFtZXMgbG9hZGluZy1yb3RhdGluZ3swJXstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMCk7dHJhbnNmb3JtOnJvdGF0ZSgwKX10b3std2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMXR1cm4pO3RyYW5zZm9ybTpyb3RhdGUoMXR1cm4pfX0uaW5maW5pdGUtbG9hZGluZy1jb250YWluZXJbZGF0YS12LTUwNzkzZjAyXXtjbGVhcjpib3RoO3RleHQtYWxpZ246Y2VudGVyfS5pbmZpbml0ZS1sb2FkaW5nLWNvbnRhaW5lciBbY2xhc3NePWxvYWRpbmctXVtkYXRhLXYtNTA3OTNmMDJde2Rpc3BsYXk6aW5saW5lLWJsb2NrO21hcmdpbjoxNXB4IDA7d2lkdGg6MjhweDtoZWlnaHQ6MjhweDtmb250LXNpemU6MjhweDtsaW5lLWhlaWdodDoyOHB4O2JvcmRlci1yYWRpdXM6NTAlfS5pbmZpbml0ZS1zdGF0dXMtcHJvbXB0W2RhdGEtdi01MDc5M2YwMl17Y29sb3I6IzY2Njtmb250LXNpemU6MTRweDt0ZXh0LWFsaWduOmNlbnRlcjtwYWRkaW5nOjEwcHggMH0nLFwiXCJdKX0sZnVuY3Rpb24ocCx4KXtwLmV4cG9ydHM9ZnVuY3Rpb24oKXt2YXIgcD1bXTtyZXR1cm4gcC50b1N0cmluZz1mdW5jdGlvbigpe2Zvcih2YXIgcD1bXSx4PTA7eDx0aGlzLmxlbmd0aDt4Kyspe3ZhciB0PXRoaXNbeF07dFsyXT9wLnB1c2goXCJAbWVkaWEgXCIrdFsyXStcIntcIit0WzFdK1wifVwiKTpwLnB1c2godFsxXSl9cmV0dXJuIHAuam9pbihcIlwiKX0scC5pPWZ1bmN0aW9uKHgsdCl7XCJzdHJpbmdcIj09dHlwZW9mIHgmJih4PVtbbnVsbCx4LFwiXCJdXSk7Zm9yKHZhciBhPXt9LGU9MDtlPHRoaXMubGVuZ3RoO2UrKyl7dmFyIG89dGhpc1tlXVswXTtcIm51bWJlclwiPT10eXBlb2YgbyYmKGFbb109ITApfWZvcihlPTA7ZTx4Lmxlbmd0aDtlKyspe3ZhciBuPXhbZV07XCJudW1iZXJcIj09dHlwZW9mIG5bMF0mJmFbblswXV18fCh0JiYhblsyXT9uWzJdPXQ6dCYmKG5bMl09XCIoXCIrblsyXStcIikgYW5kIChcIit0K1wiKVwiKSxwLnB1c2gobikpfX0scH19LGZ1bmN0aW9uKHAseCx0KXt2YXIgYSxlO3QoNyksYT10KDEpO3ZhciBvPXQoNSk7ZT1hPWF8fHt9LFwib2JqZWN0XCIhPXR5cGVvZiBhLmRlZmF1bHQmJlwiZnVuY3Rpb25cIiE9dHlwZW9mIGEuZGVmYXVsdHx8KGU9YT1hLmRlZmF1bHQpLFwiZnVuY3Rpb25cIj09dHlwZW9mIGUmJihlPWUub3B0aW9ucyksZS5yZW5kZXI9by5yZW5kZXIsZS5zdGF0aWNSZW5kZXJGbnM9by5zdGF0aWNSZW5kZXJGbnMsZS5fc2NvcGVJZD1cImRhdGEtdi01MDc5M2YwMlwiLHAuZXhwb3J0cz1hfSxmdW5jdGlvbihwLHgpe3AuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uKCl7dmFyIHA9dGhpcyx4PXAuJGNyZWF0ZUVsZW1lbnQsdD1wLl9zZWxmLl9jfHx4O3JldHVybiB0KFwiZGl2XCIse3N0YXRpY0NsYXNzOlwiaW5maW5pdGUtbG9hZGluZy1jb250YWluZXJcIn0sW3QoXCJkaXZcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJzaG93XCIscmF3TmFtZTpcInYtc2hvd1wiLHZhbHVlOnAuaXNMb2FkaW5nLGV4cHJlc3Npb246XCJpc0xvYWRpbmdcIn1dfSxbcC5fdChcInNwaW5uZXJcIixbdChcImlcIix7Y2xhc3M6cC5zcGlubmVyVHlwZX0pXSldLDIpLHAuX3YoXCIgXCIpLHQoXCJkaXZcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJzaG93XCIscmF3TmFtZTpcInYtc2hvd1wiLHZhbHVlOiFwLmlzTG9hZGluZyYmcC5pc0NvbXBsZXRlJiZwLmlzRmlyc3RMb2FkLGV4cHJlc3Npb246XCIhaXNMb2FkaW5nICYmIGlzQ29tcGxldGUgJiYgaXNGaXJzdExvYWRcIn1dLHN0YXRpY0NsYXNzOlwiaW5maW5pdGUtc3RhdHVzLXByb21wdFwifSxbcC5fdChcIm5vLXJlc3VsdHNcIixbcC5fdihcIk5vIHJlc3VsdHMgOihcIildKV0sMikscC5fdihcIiBcIiksdChcImRpdlwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6IXAuaXNMb2FkaW5nJiZwLmlzQ29tcGxldGUmJiFwLmlzRmlyc3RMb2FkLGV4cHJlc3Npb246XCIhaXNMb2FkaW5nICYmIGlzQ29tcGxldGUgJiYgIWlzRmlyc3RMb2FkXCJ9XSxzdGF0aWNDbGFzczpcImluZmluaXRlLXN0YXR1cy1wcm9tcHRcIn0sW3AuX3QoXCJuby1tb3JlXCIsW3AuX3YoXCJObyBtb3JlIGRhdGEgOilcIildKV0sMildKX0sc3RhdGljUmVuZGVyRm5zOltdfX0sZnVuY3Rpb24ocCx4LHQpe2Z1bmN0aW9uIGEocCx4KXtmb3IodmFyIHQ9MDt0PHAubGVuZ3RoO3QrKyl7dmFyIGE9cFt0XSxlPWRbYS5pZF07aWYoZSl7ZS5yZWZzKys7Zm9yKHZhciBvPTA7bzxlLnBhcnRzLmxlbmd0aDtvKyspZS5wYXJ0c1tvXShhLnBhcnRzW29dKTtmb3IoO288YS5wYXJ0cy5sZW5ndGg7bysrKWUucGFydHMucHVzaChyKGEucGFydHNbb10seCkpfWVsc2V7Zm9yKHZhciBuPVtdLG89MDtvPGEucGFydHMubGVuZ3RoO28rKyluLnB1c2gocihhLnBhcnRzW29dLHgpKTtkW2EuaWRdPXtpZDphLmlkLHJlZnM6MSxwYXJ0czpufX19fWZ1bmN0aW9uIGUocCl7Zm9yKHZhciB4PVtdLHQ9e30sYT0wO2E8cC5sZW5ndGg7YSsrKXt2YXIgZT1wW2FdLG89ZVswXSxuPWVbMV0saT1lWzJdLHI9ZVszXSxzPXtjc3M6bixtZWRpYTppLHNvdXJjZU1hcDpyfTt0W29dP3Rbb10ucGFydHMucHVzaChzKTp4LnB1c2godFtvXT17aWQ6byxwYXJ0czpbc119KX1yZXR1cm4geH1mdW5jdGlvbiBvKHAseCl7dmFyIHQ9YygpLGE9bVttLmxlbmd0aC0xXTtpZihcInRvcFwiPT09cC5pbnNlcnRBdClhP2EubmV4dFNpYmxpbmc/dC5pbnNlcnRCZWZvcmUoeCxhLm5leHRTaWJsaW5nKTp0LmFwcGVuZENoaWxkKHgpOnQuaW5zZXJ0QmVmb3JlKHgsdC5maXJzdENoaWxkKSxtLnB1c2goeCk7ZWxzZXtpZihcImJvdHRvbVwiIT09cC5pbnNlcnRBdCl0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIHZhbHVlIGZvciBwYXJhbWV0ZXIgJ2luc2VydEF0Jy4gTXVzdCBiZSAndG9wJyBvciAnYm90dG9tJy5cIik7dC5hcHBlbmRDaGlsZCh4KX19ZnVuY3Rpb24gbihwKXtwLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQocCk7dmFyIHg9bS5pbmRleE9mKHApO3g+PTAmJm0uc3BsaWNlKHgsMSl9ZnVuY3Rpb24gaShwKXt2YXIgeD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic3R5bGVcIik7cmV0dXJuIHgudHlwZT1cInRleHQvY3NzXCIsbyhwLHgpLHh9ZnVuY3Rpb24gcihwLHgpe3ZhciB0LGEsZTtpZih4LnNpbmdsZXRvbil7dmFyIG89aCsrO3Q9dXx8KHU9aSh4KSksYT1zLmJpbmQobnVsbCx0LG8sITEpLGU9cy5iaW5kKG51bGwsdCxvLCEwKX1lbHNlIHQ9aSh4KSxhPWIuYmluZChudWxsLHQpLGU9ZnVuY3Rpb24oKXtuKHQpfTtyZXR1cm4gYShwKSxmdW5jdGlvbih4KXtpZih4KXtpZih4LmNzcz09PXAuY3NzJiZ4Lm1lZGlhPT09cC5tZWRpYSYmeC5zb3VyY2VNYXA9PT1wLnNvdXJjZU1hcClyZXR1cm47YShwPXgpfWVsc2UgZSgpfX1mdW5jdGlvbiBzKHAseCx0LGEpe3ZhciBlPXQ/XCJcIjphLmNzcztpZihwLnN0eWxlU2hlZXQpcC5zdHlsZVNoZWV0LmNzc1RleHQ9Zyh4LGUpO2Vsc2V7dmFyIG89ZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoZSksbj1wLmNoaWxkTm9kZXM7blt4XSYmcC5yZW1vdmVDaGlsZChuW3hdKSxuLmxlbmd0aD9wLmluc2VydEJlZm9yZShvLG5beF0pOnAuYXBwZW5kQ2hpbGQobyl9fWZ1bmN0aW9uIGIocCx4KXt2YXIgdD14LmNzcyxhPXgubWVkaWEsZT14LnNvdXJjZU1hcDtpZihhJiZwLnNldEF0dHJpYnV0ZShcIm1lZGlhXCIsYSksZSYmKHQrPVwiXFxuLyojIHNvdXJjZVVSTD1cIitlLnNvdXJjZXNbMF0rXCIgKi9cIix0Kz1cIlxcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtiYXNlNjQsXCIrYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoZSkpKSkrXCIgKi9cIikscC5zdHlsZVNoZWV0KXAuc3R5bGVTaGVldC5jc3NUZXh0PXQ7ZWxzZXtmb3IoO3AuZmlyc3RDaGlsZDspcC5yZW1vdmVDaGlsZChwLmZpcnN0Q2hpbGQpO3AuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUodCkpfX12YXIgZD17fSxsPWZ1bmN0aW9uKHApe3ZhciB4O3JldHVybiBmdW5jdGlvbigpe3JldHVyblwidW5kZWZpbmVkXCI9PXR5cGVvZiB4JiYoeD1wLmFwcGx5KHRoaXMsYXJndW1lbnRzKSkseH19LGY9bChmdW5jdGlvbigpe3JldHVybi9tc2llIFs2LTldXFxiLy50ZXN0KHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkpfSksYz1sKGZ1bmN0aW9uKCl7cmV0dXJuIGRvY3VtZW50LmhlYWR8fGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaGVhZFwiKVswXX0pLHU9bnVsbCxoPTAsbT1bXTtwLmV4cG9ydHM9ZnVuY3Rpb24ocCx4KXt4PXh8fHt9LFwidW5kZWZpbmVkXCI9PXR5cGVvZiB4LnNpbmdsZXRvbiYmKHguc2luZ2xldG9uPWYoKSksXCJ1bmRlZmluZWRcIj09dHlwZW9mIHguaW5zZXJ0QXQmJih4Lmluc2VydEF0PVwiYm90dG9tXCIpO3ZhciB0PWUocCk7cmV0dXJuIGEodCx4KSxmdW5jdGlvbihwKXtmb3IodmFyIG89W10sbj0wO248dC5sZW5ndGg7bisrKXt2YXIgaT10W25dLHI9ZFtpLmlkXTtyLnJlZnMtLSxvLnB1c2gocil9aWYocCl7dmFyIHM9ZShwKTthKHMseCl9Zm9yKHZhciBuPTA7bjxvLmxlbmd0aDtuKyspe3ZhciByPW9bbl07aWYoMD09PXIucmVmcyl7Zm9yKHZhciBiPTA7YjxyLnBhcnRzLmxlbmd0aDtiKyspci5wYXJ0c1tiXSgpO2RlbGV0ZSBkW3IuaWRdfX19fTt2YXIgZz1mdW5jdGlvbigpe3ZhciBwPVtdO3JldHVybiBmdW5jdGlvbih4LHQpe3JldHVybiBwW3hdPXQscC5maWx0ZXIoQm9vbGVhbikuam9pbihcIlxcblwiKX19KCl9LGZ1bmN0aW9uKHAseCx0KXt2YXIgYT10KDIpO1wic3RyaW5nXCI9PXR5cGVvZiBhJiYoYT1bW3AuaWQsYSxcIlwiXV0pO3QoNikoYSx7fSk7YS5sb2NhbHMmJihwLmV4cG9ydHM9YS5sb2NhbHMpfV0pfSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1pbmZpbml0ZS1sb2FkaW5nL2Rpc3QvdnVlLWluZmluaXRlLWxvYWRpbmcuanNcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDggMTYiXSwic291cmNlUm9vdCI6IiJ9