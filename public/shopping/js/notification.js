/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 421);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 171:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(302);
__webpack_require__(303);
__webpack_require__(304);

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['notification'],

    methods: {
        action: function action() {
            var _this = this;

            axiosAPIv1.post('/messages/' + this.notification.id + '/mark-as-read').then(function (result) {
                _this.$parent.getNotifications();
            });

            if (this.notification.chat_room.name) {
                //return window.location.href = '/messages/#/cr/' + this.notification.chat_room_id;
            }

            var chat = [];
            $(".panel-chat-box").each(function () {
                chat.push($(this).attr('id'));
            });

            if (this.notification.chat_room.name) {
                if ($.inArray('user' + this.notification.chat_room.name, chat) == '-1') Event.fire('chat-box.show', this.notification);
            } else {
                if ($.inArray('user' + this.notification.chat_room.users[0].id, chat) == '-1') Event.fire('chat-box.show', this.notification.chat_room.users[0]);
            }

            setTimeout(function () {
                $('.js-auto-size').textareaAutoSize();
            }, 5000);
        }
    },

    computed: {
        fromNow: function fromNow() {
            return moment(this.notification.created_at).fromNow();
        },
        href: function href() {
            if (this.notification.chat_room.name) {
                //return '/messages/#/cr/' + this.notification.chat_room_id;
            }

            return '/messages/#/u/' + this.notification.chat_room.users[0].id;
        },
        isRead: function isRead() {
            if (this.notification.user_id == window.Laravel.user.id) return true;else return !!this.notification.read_at;
        },
        fullName: function fullName() {
            if (this.notification.chat_room.name) return this.notification.chat_room.name;else return this.notification.chat_room.users[0].first_name + ' ' + this.notification.chat_room.users[0].last_name;
        }
    }
});

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['notification'],

    methods: {
        markAsRead: function markAsRead() {
            if (!this.notification.read_at) {
                return axiosAPIv1.post('/notifications/' + this.notification.id + '/mark-as-read');
            }
        }
    },

    computed: {
        href: function href() {
            if (!!~this.type.search('document')) {
                if (!!~this.notification.data.body.search('uploaded a new document')) {
                    return 'http://' + Laravel.cpanel_url + '/documents';
                } else {
                    return '/profile?open=documents';
                }
            } else if (!!~this.type.search('new-issue') || !!~this.type.search('issue-assignment')) {
                return 'http://' + Laravel.cpanel_url + '/customer-service';
            } else if (!!~this.type.search('feedback')) {
                return '/feedback';
            } else if (!!~this.type.search('friend')) {
                if (!!~this.notification.data.body.search('accepted your friend request')) {
                    return '/users/' + this.notification.from_id;
                }
                return '/friends';
            } else if (!!~this.type.search('new-purchase')) {
                return '/user/sales-report';
            } else if (!!~this.type.search('order-status-update')) {
                if (!!~this.notification.data.body.search('requested to return your item')) {
                    return '/user/sales-report';
                } else if (!!~this.notification.data.body.search('successfully received the item')) {
                    return '/user/sales-report';
                } else {
                    return '/user/purchase-report';
                }
            } else if (!!~this.type.search('new-product-review') || !!~this.type.search('new-product-review-comment')) {
                if (this.notification.data.product) {
                    return '/product/' + this.notification.data.product.slug;
                }

                return '#';
            } else if (!!~this.type.search('change-ewallet-transaction-status')) {
                return '/user/ewallet';
            } else if (!!~this.type.search('new-ewallet-transaction')) {
                if (!!~this.notification.data.body.search('uploaded a deposit slip') || !!~this.notification.data.body.search('deposited money for approval') || !!~this.notification.data.body.search('requested for a Withdraw')) {
                    return 'http://' + Laravel.cpanel_url + '/ewallet';
                }
                return '/user/ewallet';
            }
            return '#';
        },
        fromNow: function fromNow() {
            return this.notification.human_created_at;
        },
        isRead: function isRead() {
            return !!this.notification.read_at;
        },
        type: function type() {
            var arrType = this.notification.type.split('\\');
            var type = arrType[arrType.length - 1].replace(/([a-z](?=[A-Z]))/g, '$1-').toLowerCase();

            return type;
        }
    },

    mounted: function mounted() {
        $('[data-toggle="tooltip"]').tooltip();

        $('.notif-body', this.$el).ellipsis({
            lines: 2,
            responsive: true
        });
    }
});

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            hlang: [],
            clang: []
        };
    },

    props: ['notification'],
    created: function created() {
        var _this = this;

        function getHeaderTranslation() {
            return axios.get('/translate/header');
        }
        function getCommonTranslation() {
            return axios.get('/translate/common');
        }

        axios.all([getHeaderTranslation(), getCommonTranslation()]).then(axios.spread(function (htranslation, ctranslation) {

            _this.hlang = htranslation.data.data;
            _this.clang = ctranslation.data.data;
        })).catch($.noop);
    },


    methods: {
        confirm: function confirm() {
            var _this2 = this;

            return axiosAPIv1.post('/friends/' + this.notification.data.friend.id + '/confirm', {
                notif_id: this.notification.id
            }).then(function (result) {
                Event.fire('friend-request.accepted', _this2.notification);
            });
        },
        deleteRequest: function deleteRequest() {
            var _this3 = this;

            return axiosAPIv1.post('/friends/' + this.notification.data.friend.id + '/delete-request', {
                notif_id: this.notification.id
            }).then(function (result) {
                Event.fire('friend-request.deleted', _this3.notificaiton);
            });
        }
    },

    computed: {
        isRead: function isRead() {
            return !!this.notification.read_at;
        }
    }
});

/***/ }),

/***/ 302:
/***/ (function(module, exports, __webpack_require__) {

window.WycNotifFriendRequest = new Vue({
    el: '#friend-request',

    data: {
        notifications: [],
        notSeenCount: null,
        $container: null
    },

    watch: {
        notifications: function notifications(val) {
            var _this = this;

            setTimeout(function () {
                _this.$container.perfectScrollbar('update');
            }, 1);
        }
    },

    methods: {
        getNotifications: function getNotifications() {
            var _this2 = this;

            return axiosAPIv1.get('/friends/notifications').then(function (result) {
                _this2.notifications = result.data.notifications;
                _this2.notSeenCount = result.data.notseen_count;
            });
        },
        markAllAsSeen: function markAllAsSeen() {
            var _this3 = this;

            return axiosAPIv1.post('/friends/mark-all-as-seen').then(function (result) {
                _this3.notSeenCount = null;
            });
        },
        accepted: function accepted(data) {
            var index = this.notifications.indexOf(data);

            this.getNotifications();
        },
        deleted: function deleted(data) {
            var index = this.notifications.indexOf(data);

            this.getNotifications();
        }
    },

    computed: {
        count: function count() {
            return this.notSeenCount ? this.notSeenCount : '';
        }
    },

    components: {
        'request': __webpack_require__(338)
    },

    created: function created() {
        this.getNotifications();

        Event.listen('new-friend-request', this.getNotifications);
        Event.listen('friend-request.accepted', this.accepted);
        Event.listen('friend-request.deleted', this.deleted);
    },
    mounted: function mounted() {
        this.$container = $(this.$el).find('.panel-body');
        this.$container.perfectScrollbar();
    }
});

/***/ }),

/***/ 303:
/***/ (function(module, exports, __webpack_require__) {

window.WycNotifInbox = new Vue({
    el: '#inbox',

    data: {
        notifications: [],
        $messageContainer: null
    },

    watch: {
        notifications: function notifications(val) {
            var _this = this;

            setTimeout(function () {
                _this.$messageContainer.perfectScrollbar('update');
            }, 1);
        }
    },

    methods: {
        getNotifications: function getNotifications() {
            var _this2 = this;

            return axiosAPIv1.get('/messages/notifications2').then(function (result) {
                _this2.notifications = result.data;
            });
        },
        getNotifications2: function getNotifications2() {
            var _this3 = this;

            return axiosAPIv1.get('/messages/notifications2').then(function (result) {
                _this3.notifications = result.data;

                if (_this3.notifications) {
                    var chat = [];
                    $(".panel-chat-box").each(function () {
                        chat.push($(this).attr('id'));
                    });
                    var oxp = false;
                    _this3.notifications.forEach(function (notif) {
                        if (notif.user_id != window.Laravel.user.id) {
                            if (!notif.seen_at && oxp == false) {
                                if (notif.chat_room.name) {
                                    if ($.inArray('user' + notif.chat_room.name, chat) == '-1') Event.fire('chat-box.show', notif);
                                } else {
                                    if ($.inArray('user' + notif.chat_room.users[0].id, chat) == '-1') Event.fire('chat-box.show', notif.chat_room.users[0]);
                                }
                                oxp = true;
                            }
                        }
                    });
                }
                //notif sound
                var audio = new Audio('/sound/wycnotif.wav');
                audio.play();
            });
        },
        markAllAsSeen: function markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (!notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });

            this.getNotifications();

            return axiosAPIv1.post('/messages/mark-all-as-seen2');
        }
    },

    computed: {
        count: function count() {
            var count = 0;

            if (this.notifications) {
                this.notifications.forEach(function (notif) {
                    if (notif.user_id != window.Laravel.user.id) {
                        if (!notif.seen_at) {
                            ++count;
                        }
                    }
                });
            }

            return count || '';
        }
    },

    components: {
        'inbox-message': __webpack_require__(336)
    },

    created: function created() {
        this.getNotifications();

        Event.listen('new-chat-message', this.getNotifications2);
    },
    mounted: function mounted() {
        this.$messageContainer = $(this.$el).find('.panel-body');
        this.$messageContainer.perfectScrollbar();
    }
});

/***/ }),

/***/ 304:
/***/ (function(module, exports, __webpack_require__) {

window.WycNotifGeneral = new Vue({
    el: '#notification',

    data: {
        notifications: [],
        $container: null
    },

    watch: {
        notifications: function notifications(val) {
            var _this = this;

            setTimeout(function () {
                _this.$container.perfectScrollbar('update');
            }, 1);
        }
    },

    methods: {
        getNotifications: function getNotifications() {
            var _this2 = this;

            return axiosAPIv1.get('/notifications').then(function (result) {
                _this2.notifications = result.data;
            });
        },


        //upcoming notif with sound
        getNotifications2: function getNotifications2() {
            var _this3 = this;

            return axiosAPIv1.get('/notifications').then(function (result) {
                _this3.notifications = result.data;
                var audio = new Audio('/sound/wycnotif.mp3');
                audio.play();
            });
        },
        markAllAsSeen: function markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (!notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });

            return axiosAPIv1.post('/notifications/mark-all-as-seen');
        },
        removeNotif: function removeNotif(data) {
            var index = this.notifications.indexOf(data);

            this.notifications.splice(index, 1);
        }
    },

    computed: {
        count: function count() {
            var count = 0;

            if (this.notifications) {
                this.notifications.forEach(function (notif) {
                    if (!notif.seen_at) {
                        ++count;
                    }
                });
            }

            return count || '';
        }
    },

    components: {
        notification: __webpack_require__(337)
    },

    created: function created() {
        this.getNotifications();

        Event.listen('friend-request-accepted', this.getNotifications2);
        Event.listen('feedback-approved', this.getNotifications2);
        Event.listen('document-approved', this.getNotifications2);
        Event.listen('document-denied', this.getNotifications2);
        Event.listen('expired-document', this.getNotifications2);
        Event.listen('new-purchase', this.getNotifications2);
        Event.listen('order-status-update', this.getNotifications2);
        Event.listen('change-ewallet-transaction-status', this.getNotifications2);
        Event.listen('new-product-review', this.getNotifications2);
        Event.listen('new-product-review-comment', this.getNotifications2);
    },
    mounted: function mounted() {
        this.$container = $(this.$el).find('.panel-body');
        this.$container.perfectScrollbar();

        $('[data-toggle="tooltip"]').tooltip();

        $('.dropdown-general-notif').on('shown.bs.dropdown', function () {
            $('.notif-body', this).ellipsis({
                lines: 2,
                responsive: true
            });
        });
    }
});

/***/ }),

/***/ 336:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(271),
  /* template */
  __webpack_require__(393),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\notifications\\InboxMessage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] InboxMessage.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6af71718", Component.options)
  } else {
    hotAPI.reload("data-v-6af71718", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 337:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(272),
  /* template */
  __webpack_require__(386),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\notifications\\Notification.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Notification.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5d1b15be", Component.options)
  } else {
    hotAPI.reload("data-v-5d1b15be", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 338:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(273),
  /* template */
  __webpack_require__(396),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\notifications\\Request.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Request.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a38f55e8", Component.options)
  } else {
    hotAPI.reload("data-v-a38f55e8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 386:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    class: {
      'not-read': !_vm.isRead
    },
    attrs: {
      "target": "_blank",
      "href": _vm.href
    },
    on: {
      "click": _vm.markAsRead
    }
  }, [_c('div', {
    staticClass: "row",
    attrs: {
      "data-toggle": "tooltip",
      "data-placement": "left",
      "data-container": "body",
      "title": _vm.notification.data.body
    }
  }, [_c('div', {
    staticClass: "col-xs-2 notif-image"
  }, [_c('img', {
    attrs: {
      "src": _vm.notification.data.image
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-7 notif-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 notif-title ellipsis"
  }, [_vm._v("\r\n                        " + _vm._s(_vm.notification.data.title) + "\r\n                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 notif-body"
  }, [_vm._v("\r\n                        " + _vm._s(_vm.notification.data.body) + "\r\n                    ")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-3 notif-date"
  }, [_c('small', [_vm._v(_vm._s(_vm.fromNow))])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5d1b15be", module.exports)
  }
}

/***/ }),

/***/ 393:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    class: {
      'not-read': !_vm.isRead
    },
    attrs: {
      "href": "#"
    },
    on: {
      "click": _vm.action
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-2 notif-image"
  }, [_c('img', {
    attrs: {
      "src": _vm.notification.chat_room.users[0].avatar
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-7 notif-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 notif-title ellipsis"
  }, [_vm._v("\r\n                        " + _vm._s(_vm.fullName) + "\r\n                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 notif-body ellipsis"
  }, [_vm._v("\r\n                        " + _vm._s(_vm.notification.body) + "\r\n                    ")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-3 notif-date"
  }, [_c('small', [_vm._v(_vm._s(_vm.fromNow))])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6af71718", module.exports)
  }
}

/***/ }),

/***/ 396:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    class: {
      'not-read': !_vm.isRead
    },
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
      }
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-2 notif-image"
  }, [_c('img', {
    attrs: {
      "src": _vm.notification.data.image
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-5 notif-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 notif-title ellipsis"
  }, [_vm._v("\r\n                        " + _vm._s(_vm.notification.data.user.full_name) + "\r\n                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 notif-body ellipsis"
  }, [_vm._v("\r\n                        " + _vm._s(_vm.hlang['sent']) + "\r\n                    ")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-5 notif-button"
  }, [_c('button', {
    on: {
      "click": _vm.confirm
    }
  }, [_vm._v(_vm._s(_vm.clang['confirm']))]), _vm._v(" "), _c('button', {
    on: {
      "click": _vm.deleteRequest
    }
  }, [_vm._v(_vm._s(_vm.clang['delete']))])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-a38f55e8", module.exports)
  }
}

/***/ }),

/***/ 421:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(171);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvbm90aWZpY2F0aW9uLmpzIiwid2VicGFjazovLy9JbmJveE1lc3NhZ2UudnVlIiwid2VicGFjazovLy9Ob3RpZmljYXRpb24udnVlIiwid2VicGFjazovLy9SZXF1ZXN0LnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvbm90aWZpY2F0aW9ucy9mcmllbmQtcmVxdWVzdC5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvbm90aWZpY2F0aW9ucy9pbmJveC5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL25vdGlmaWNhdGlvbnMvSW5ib3hNZXNzYWdlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvbm90aWZpY2F0aW9ucy9Ob3RpZmljYXRpb24udnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9ub3RpZmljYXRpb25zL1JlcXVlc3QudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9ub3RpZmljYXRpb25zL05vdGlmaWNhdGlvbi52dWU/Y2I5MiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvbm90aWZpY2F0aW9ucy9JbmJveE1lc3NhZ2UudnVlPzg0MzciLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL25vdGlmaWNhdGlvbnMvUmVxdWVzdC52dWU/ZGFkYyJdLCJuYW1lcyI6WyJyZXF1aXJlIiwid2luZG93IiwiV3ljTm90aWZGcmllbmRSZXF1ZXN0IiwiVnVlIiwiZWwiLCJkYXRhIiwibm90aWZpY2F0aW9ucyIsIm5vdFNlZW5Db3VudCIsIiRjb250YWluZXIiLCJ3YXRjaCIsInZhbCIsInNldFRpbWVvdXQiLCJwZXJmZWN0U2Nyb2xsYmFyIiwibWV0aG9kcyIsImdldE5vdGlmaWNhdGlvbnMiLCJheGlvc0FQSXYxIiwiZ2V0IiwidGhlbiIsInJlc3VsdCIsIm5vdHNlZW5fY291bnQiLCJtYXJrQWxsQXNTZWVuIiwicG9zdCIsImFjY2VwdGVkIiwiaW5kZXgiLCJpbmRleE9mIiwiZGVsZXRlZCIsImNvbXB1dGVkIiwiY291bnQiLCJjb21wb25lbnRzIiwiY3JlYXRlZCIsIkV2ZW50IiwibGlzdGVuIiwibW91bnRlZCIsIiQiLCIkZWwiLCJmaW5kIiwiV3ljTm90aWZJbmJveCIsIiRtZXNzYWdlQ29udGFpbmVyIiwiZ2V0Tm90aWZpY2F0aW9uczIiLCJjaGF0IiwiZWFjaCIsInB1c2giLCJhdHRyIiwib3hwIiwiZm9yRWFjaCIsIm5vdGlmIiwidXNlcl9pZCIsIkxhcmF2ZWwiLCJ1c2VyIiwiaWQiLCJzZWVuX2F0IiwiY2hhdF9yb29tIiwibmFtZSIsImluQXJyYXkiLCJmaXJlIiwidXNlcnMiLCJhdWRpbyIsIkF1ZGlvIiwicGxheSIsIkRhdGUiLCJXeWNOb3RpZkdlbmVyYWwiLCJyZW1vdmVOb3RpZiIsInNwbGljZSIsIm5vdGlmaWNhdGlvbiIsInRvb2x0aXAiLCJvbiIsImVsbGlwc2lzIiwibGluZXMiLCJyZXNwb25zaXZlIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNsREEsbUJBQUFBLENBQVEsR0FBUjtBQUNBLG1CQUFBQSxDQUFRLEdBQVI7QUFDQSxtQkFBQUEsQ0FBUSxHQUFSLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMwQkE7WUFHQTs7OztBQUVBOztrRUFDQSx3Q0FDQTs4QkFDQTtBQUVBOztrREFDQTtBQUNBO0FBRUE7O3VCQUNBO2tEQUNBO3VDQUNBO0FBRUE7O2tEQUNBO2tGQUNBLHVDQUVBO21CQUNBO3lGQUNBLG9FQUVBO0FBRUE7O21DQUNBO21DQUNBO2VBQ0E7QUFHQTtBQS9CQTs7O29DQWlDQTt3REFDQTtBQUVBOzhCQUNBO2tEQUNBO0FBQ0E7QUFFQTs7MkVBQ0E7QUFFQTtrQ0FDQTtpRUFDQSxXQUVBLHFDQUNBO0FBQ0E7c0NBQ0E7NENBQ0EseUNBRUEsOEdBQ0E7QUFFQTtBQXpCQTtBQW5DQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDREE7WUFHQTs7OzBDQUVBOzRDQUNBO2tGQUNBO0FBQ0E7QUFHQTtBQVBBOzs7OEJBU0E7aURBQ0E7c0ZBQ0E7NERBQ0E7QUFDQSx1QkFDQTsyQkFDQTtBQUNBO0FBRUEsb0dBQ0E7d0RBQ0E7QUFFQSx3REFDQTt1QkFDQTtBQUVBLHNEQUNBOzJGQUNBO3lEQUNBO0FBQ0E7dUJBQ0E7QUFFQSw0REFDQTt1QkFDQTtBQUVBLG1FQUNBOzRGQUNBOzJCQUNBO0FBQ0Esb0dBQ0E7MkJBQ0E7QUFDQSx1QkFDQTsyQkFDQTtBQUNBO0FBRUEsdUhBQ0E7b0RBQ0E7d0VBQ0E7QUFFQTs7dUJBQ0E7QUFFQSxpRkFDQTt1QkFDQTtBQUNBLHVFQUNBO29PQUNBOzREQUNBO0FBQ0E7dUJBQ0E7QUFDQTttQkFDQTtBQUVBO29DQUNBO3FDQUNBO0FBRUE7a0NBQ0E7dUNBQ0E7QUFFQTs4QkFDQTt1REFDQTt1RkFFQTs7bUJBQ0E7QUFHQTtBQTdFQTs7Z0NBOEVBO3FDQUVBOzs7bUJBRUE7d0JBRUE7QUFIQTtBQUlBO0FBaEdBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7MEJBRUE7O21CQUVBO21CQUVBO0FBSEE7QUFJQTs7WUFDQTs7QUFDQTs7d0NBQ0E7NkJBQ0E7QUFDQTt3Q0FDQTs2QkFDQTtBQUVBOztrQkFDQSxDQUNBLHdCQUNBLG9DQUNBLE9BQ0EsVUFDQSxjQUNBLGNBRUE7OzRDQUNBOzRDQUdBO0FBQ0Esb0JBQ0E7QUFFQTs7Ozs7QUFFQTs7OzRDQUVBO0FBREEsc0NBRUE7NkRBQ0E7QUFDQTtBQUVBOztBQUNBOzs7NENBRUE7QUFEQSxzQ0FFQTs0REFDQTtBQUNBO0FBR0E7QUFqQkE7OztrQ0FtQkE7dUNBQ0E7QUFFQTtBQUpBO0FBbkRBLEc7Ozs7Ozs7QUM5QkFDLE9BQU9DLHFCQUFQLEdBQStCLElBQUlDLEdBQUosQ0FBUTtBQUNuQ0MsUUFBSSxpQkFEK0I7O0FBR25DQyxVQUFNO0FBQ0ZDLHVCQUFlLEVBRGI7QUFFRkMsc0JBQWMsSUFGWjtBQUdGQyxvQkFBWTtBQUhWLEtBSDZCOztBQVNuQ0MsV0FBTztBQUNISCxxQkFERyx5QkFDV0ksR0FEWCxFQUNnQjtBQUFBOztBQUNmQyx1QkFBVyxZQUFNO0FBQ2Isc0JBQUtILFVBQUwsQ0FBZ0JJLGdCQUFoQixDQUFpQyxRQUFqQztBQUNILGFBRkQsRUFFRyxDQUZIO0FBR0g7QUFMRSxLQVQ0Qjs7QUFpQm5DQyxhQUFTO0FBQ0xDLHdCQURLLDhCQUNjO0FBQUE7O0FBQ2YsbUJBQU9DLFdBQVdDLEdBQVgsQ0FBZSx3QkFBZixFQUNGQyxJQURFLENBQ0csa0JBQVU7QUFDWix1QkFBS1gsYUFBTCxHQUFxQlksT0FBT2IsSUFBUCxDQUFZQyxhQUFqQztBQUNBLHVCQUFLQyxZQUFMLEdBQW9CVyxPQUFPYixJQUFQLENBQVljLGFBQWhDO0FBQ0gsYUFKRSxDQUFQO0FBS0gsU0FQSTtBQVNMQyxxQkFUSywyQkFTVztBQUFBOztBQUNaLG1CQUFPTCxXQUFXTSxJQUFYLENBQWdCLDJCQUFoQixFQUNGSixJQURFLENBQ0csa0JBQVU7QUFDWix1QkFBS1YsWUFBTCxHQUFvQixJQUFwQjtBQUNILGFBSEUsQ0FBUDtBQUlILFNBZEk7QUFnQkxlLGdCQWhCSyxvQkFnQklqQixJQWhCSixFQWdCVTtBQUNYLGdCQUFJa0IsUUFBUSxLQUFLakIsYUFBTCxDQUFtQmtCLE9BQW5CLENBQTJCbkIsSUFBM0IsQ0FBWjs7QUFFQSxpQkFBS1MsZ0JBQUw7QUFDSCxTQXBCSTtBQXNCTFcsZUF0QkssbUJBc0JHcEIsSUF0QkgsRUFzQlM7QUFDVixnQkFBSWtCLFFBQVEsS0FBS2pCLGFBQUwsQ0FBbUJrQixPQUFuQixDQUEyQm5CLElBQTNCLENBQVo7O0FBRUEsaUJBQUtTLGdCQUFMO0FBQ0g7QUExQkksS0FqQjBCOztBQThDbkNZLGNBQVU7QUFDTkMsYUFETSxtQkFDRTtBQUNKLG1CQUFPLEtBQUtwQixZQUFMLEdBQW9CLEtBQUtBLFlBQXpCLEdBQXdDLEVBQS9DO0FBQ0g7QUFISyxLQTlDeUI7O0FBb0RuQ3FCLGdCQUFZO0FBQ1IsbUJBQVcsbUJBQUE1QixDQUFRLEdBQVI7QUFESCxLQXBEdUI7O0FBd0RuQzZCLFdBeERtQyxxQkF3RHpCO0FBQ04sYUFBS2YsZ0JBQUw7O0FBRUFnQixjQUFNQyxNQUFOLENBQWEsb0JBQWIsRUFBbUMsS0FBS2pCLGdCQUF4QztBQUNBZ0IsY0FBTUMsTUFBTixDQUFhLHlCQUFiLEVBQXdDLEtBQUtULFFBQTdDO0FBQ0FRLGNBQU1DLE1BQU4sQ0FBYSx3QkFBYixFQUF1QyxLQUFLTixPQUE1QztBQUNILEtBOURrQztBQWdFbkNPLFdBaEVtQyxxQkFnRXpCO0FBQ04sYUFBS3hCLFVBQUwsR0FBa0J5QixFQUFFLEtBQUtDLEdBQVAsRUFBWUMsSUFBWixDQUFpQixhQUFqQixDQUFsQjtBQUNBLGFBQUszQixVQUFMLENBQWdCSSxnQkFBaEI7QUFDSDtBQW5Fa0MsQ0FBUixDQUEvQixDOzs7Ozs7O0FDQUFYLE9BQU9tQyxhQUFQLEdBQXVCLElBQUlqQyxHQUFKLENBQVE7QUFDM0JDLFFBQUksUUFEdUI7O0FBRzNCQyxVQUFNO0FBQ0ZDLHVCQUFlLEVBRGI7QUFFRitCLDJCQUFtQjtBQUZqQixLQUhxQjs7QUFRM0I1QixXQUFPO0FBQ0hILHFCQURHLHlCQUNXSSxHQURYLEVBQ2dCO0FBQUE7O0FBQ2ZDLHVCQUFXLFlBQU07QUFDYixzQkFBSzBCLGlCQUFMLENBQXVCekIsZ0JBQXZCLENBQXdDLFFBQXhDO0FBQ0gsYUFGRCxFQUVHLENBRkg7QUFHSDtBQUxFLEtBUm9COztBQWdCM0JDLGFBQVM7QUFDTEMsd0JBREssOEJBQ2M7QUFBQTs7QUFDZixtQkFBT0MsV0FBV0MsR0FBWCxDQUFlLDBCQUFmLEVBQ0ZDLElBREUsQ0FDRyxrQkFBVTtBQUNaLHVCQUFLWCxhQUFMLEdBQXFCWSxPQUFPYixJQUE1QjtBQUNILGFBSEUsQ0FBUDtBQUlILFNBTkk7QUFRTGlDLHlCQVJLLCtCQVFlO0FBQUE7O0FBQ2hCLG1CQUFPdkIsV0FBV0MsR0FBWCxDQUFlLDBCQUFmLEVBQ0ZDLElBREUsQ0FDRyxrQkFBVTtBQUNaLHVCQUFLWCxhQUFMLEdBQXFCWSxPQUFPYixJQUE1Qjs7QUFFQSxvQkFBSSxPQUFLQyxhQUFULEVBQXdCO0FBQ3BCLHdCQUFJaUMsT0FBTyxFQUFYO0FBQ0FOLHNCQUFFLGlCQUFGLEVBQXFCTyxJQUFyQixDQUEwQixZQUFVO0FBQ2hDRCw2QkFBS0UsSUFBTCxDQUFVUixFQUFFLElBQUYsRUFBUVMsSUFBUixDQUFhLElBQWIsQ0FBVjtBQUNILHFCQUZEO0FBR0Esd0JBQUlDLE1BQU0sS0FBVjtBQUNBLDJCQUFLckMsYUFBTCxDQUFtQnNDLE9BQW5CLENBQTJCLFVBQVVDLEtBQVYsRUFBaUI7QUFDeEMsNEJBQUdBLE1BQU1DLE9BQU4sSUFBaUI3QyxPQUFPOEMsT0FBUCxDQUFlQyxJQUFmLENBQW9CQyxFQUF4QyxFQUEyQztBQUN2QyxnQ0FBSSxDQUFFSixNQUFNSyxPQUFSLElBQW1CUCxPQUFPLEtBQTlCLEVBQXFDO0FBQ2pDLG9DQUFJRSxNQUFNTSxTQUFOLENBQWdCQyxJQUFwQixFQUEwQjtBQUN0Qix3Q0FBR25CLEVBQUVvQixPQUFGLENBQVUsU0FBT1IsTUFBTU0sU0FBTixDQUFnQkMsSUFBakMsRUFBc0NiLElBQXRDLEtBQTZDLElBQWhELEVBQ0lULE1BQU13QixJQUFOLENBQVcsZUFBWCxFQUE0QlQsS0FBNUI7QUFFUCxpQ0FKRCxNQUlPO0FBQ0Ysd0NBQUdaLEVBQUVvQixPQUFGLENBQVUsU0FBT1IsTUFBTU0sU0FBTixDQUFnQkksS0FBaEIsQ0FBc0IsQ0FBdEIsRUFBeUJOLEVBQTFDLEVBQTZDVixJQUE3QyxLQUFvRCxJQUF2RCxFQUNHVCxNQUFNd0IsSUFBTixDQUFXLGVBQVgsRUFBNEJULE1BQU1NLFNBQU4sQ0FBZ0JJLEtBQWhCLENBQXNCLENBQXRCLENBQTVCO0FBRVA7QUFDRFosc0NBQU0sSUFBTjtBQUNIO0FBQ0o7QUFDSixxQkFmRDtBQWdCSDtBQUNEO0FBQ0Esb0JBQUlhLFFBQVEsSUFBSUMsS0FBSixDQUFVLHFCQUFWLENBQVo7QUFDQUQsc0JBQU1FLElBQU47QUFDSCxhQTlCRSxDQUFQO0FBK0JILFNBeENJO0FBMkNMdEMscUJBM0NLLDJCQTJDVztBQUNaLGlCQUFLZCxhQUFMLENBQW1Cc0MsT0FBbkIsQ0FBMkIsVUFBVUMsS0FBVixFQUFpQjtBQUN4QyxvQkFBSSxDQUFFQSxNQUFNSyxPQUFaLEVBQXFCO0FBQ2pCTCwwQkFBTUssT0FBTixHQUFnQixJQUFJUyxJQUFKLEVBQWhCO0FBQ0g7QUFDSixhQUpEOztBQU1BLGlCQUFLN0MsZ0JBQUw7O0FBRUEsbUJBQU9DLFdBQVdNLElBQVgsQ0FBZ0IsNkJBQWhCLENBQVA7QUFDSDtBQXJESSxLQWhCa0I7O0FBd0UzQkssY0FBVTtBQUNOQyxhQURNLG1CQUNFO0FBQ0osZ0JBQUlBLFFBQVEsQ0FBWjs7QUFFQSxnQkFBSSxLQUFLckIsYUFBVCxFQUF3QjtBQUNwQixxQkFBS0EsYUFBTCxDQUFtQnNDLE9BQW5CLENBQTJCLFVBQVVDLEtBQVYsRUFBaUI7QUFDeEMsd0JBQUdBLE1BQU1DLE9BQU4sSUFBaUI3QyxPQUFPOEMsT0FBUCxDQUFlQyxJQUFmLENBQW9CQyxFQUF4QyxFQUEyQztBQUN2Qyw0QkFBSSxDQUFFSixNQUFNSyxPQUFaLEVBQXFCO0FBQ2pCLDhCQUFFdkIsS0FBRjtBQUNIO0FBQ0o7QUFDSixpQkFORDtBQU9IOztBQUVELG1CQUFPQSxTQUFTLEVBQWhCO0FBQ0g7QUFmSyxLQXhFaUI7O0FBMEYzQkMsZ0JBQVk7QUFDUix5QkFBaUIsbUJBQUE1QixDQUFRLEdBQVI7QUFEVCxLQTFGZTs7QUE4RjNCNkIsV0E5RjJCLHFCQThGakI7QUFDTixhQUFLZixnQkFBTDs7QUFFQWdCLGNBQU1DLE1BQU4sQ0FBYSxrQkFBYixFQUFpQyxLQUFLTyxpQkFBdEM7QUFDSCxLQWxHMEI7QUFvRzNCTixXQXBHMkIscUJBb0dqQjtBQUNOLGFBQUtLLGlCQUFMLEdBQXlCSixFQUFFLEtBQUtDLEdBQVAsRUFBWUMsSUFBWixDQUFpQixhQUFqQixDQUF6QjtBQUNBLGFBQUtFLGlCQUFMLENBQXVCekIsZ0JBQXZCO0FBQ0g7QUF2RzBCLENBQVIsQ0FBdkIsQzs7Ozs7OztBQ0FBWCxPQUFPMkQsZUFBUCxHQUF5QixJQUFJekQsR0FBSixDQUFRO0FBQzdCQyxRQUFJLGVBRHlCOztBQUc3QkMsVUFBTTtBQUNGQyx1QkFBZSxFQURiO0FBRUZFLG9CQUFZO0FBRlYsS0FIdUI7O0FBUTdCQyxXQUFPO0FBQ0hILHFCQURHLHlCQUNXSSxHQURYLEVBQ2dCO0FBQUE7O0FBQ2ZDLHVCQUFXLFlBQU07QUFDYixzQkFBS0gsVUFBTCxDQUFnQkksZ0JBQWhCLENBQWlDLFFBQWpDO0FBQ0gsYUFGRCxFQUVHLENBRkg7QUFHSDtBQUxFLEtBUnNCOztBQWdCN0JDLGFBQVM7QUFDTEMsd0JBREssOEJBQ2M7QUFBQTs7QUFDZixtQkFBT0MsV0FBV0MsR0FBWCxDQUFlLGdCQUFmLEVBQ0ZDLElBREUsQ0FDRyxrQkFBVTtBQUNaLHVCQUFLWCxhQUFMLEdBQXFCWSxPQUFPYixJQUE1QjtBQUNILGFBSEUsQ0FBUDtBQUlILFNBTkk7OztBQVFMO0FBQ0FpQyx5QkFUSywrQkFTZTtBQUFBOztBQUNoQixtQkFBT3ZCLFdBQVdDLEdBQVgsQ0FBZSxnQkFBZixFQUNGQyxJQURFLENBQ0csa0JBQVU7QUFDWix1QkFBS1gsYUFBTCxHQUFxQlksT0FBT2IsSUFBNUI7QUFDQSxvQkFBSW1ELFFBQVEsSUFBSUMsS0FBSixDQUFVLHFCQUFWLENBQVo7QUFDQUQsc0JBQU1FLElBQU47QUFDSCxhQUxFLENBQVA7QUFNSCxTQWhCSTtBQWtCTHRDLHFCQWxCSywyQkFrQlc7QUFDWixpQkFBS2QsYUFBTCxDQUFtQnNDLE9BQW5CLENBQTJCLFVBQVVDLEtBQVYsRUFBaUI7QUFDeEMsb0JBQUksQ0FBRUEsTUFBTUssT0FBWixFQUFxQjtBQUNqQkwsMEJBQU1LLE9BQU4sR0FBZ0IsSUFBSVMsSUFBSixFQUFoQjtBQUNIO0FBQ0osYUFKRDs7QUFNQSxtQkFBTzVDLFdBQVdNLElBQVgsQ0FBZ0IsaUNBQWhCLENBQVA7QUFDSCxTQTFCSTtBQTRCTHdDLG1CQTVCSyx1QkE0Qk94RCxJQTVCUCxFQTRCYTtBQUNkLGdCQUFJa0IsUUFBUSxLQUFLakIsYUFBTCxDQUFtQmtCLE9BQW5CLENBQTJCbkIsSUFBM0IsQ0FBWjs7QUFFQSxpQkFBS0MsYUFBTCxDQUFtQndELE1BQW5CLENBQTBCdkMsS0FBMUIsRUFBaUMsQ0FBakM7QUFDSDtBQWhDSSxLQWhCb0I7O0FBbUQ3QkcsY0FBVTtBQUNOQyxhQURNLG1CQUNFO0FBQ0osZ0JBQUlBLFFBQVEsQ0FBWjs7QUFFQSxnQkFBSSxLQUFLckIsYUFBVCxFQUF3QjtBQUNwQixxQkFBS0EsYUFBTCxDQUFtQnNDLE9BQW5CLENBQTJCLFVBQVVDLEtBQVYsRUFBaUI7QUFDeEMsd0JBQUksQ0FBRUEsTUFBTUssT0FBWixFQUFxQjtBQUNqQiwwQkFBRXZCLEtBQUY7QUFDSDtBQUNKLGlCQUpEO0FBS0g7O0FBRUQsbUJBQU9BLFNBQVMsRUFBaEI7QUFDSDtBQWJLLEtBbkRtQjs7QUFtRTdCQyxnQkFBWTtBQUNSbUMsc0JBQWMsbUJBQUEvRCxDQUFRLEdBQVI7QUFETixLQW5FaUI7O0FBdUU3QjZCLFdBdkU2QixxQkF1RW5CO0FBQ04sYUFBS2YsZ0JBQUw7O0FBRUFnQixjQUFNQyxNQUFOLENBQWEseUJBQWIsRUFBd0MsS0FBS08saUJBQTdDO0FBQ0FSLGNBQU1DLE1BQU4sQ0FBYSxtQkFBYixFQUFrQyxLQUFLTyxpQkFBdkM7QUFDQVIsY0FBTUMsTUFBTixDQUFhLG1CQUFiLEVBQWtDLEtBQUtPLGlCQUF2QztBQUNBUixjQUFNQyxNQUFOLENBQWEsaUJBQWIsRUFBZ0MsS0FBS08saUJBQXJDO0FBQ0FSLGNBQU1DLE1BQU4sQ0FBYSxrQkFBYixFQUFpQyxLQUFLTyxpQkFBdEM7QUFDQVIsY0FBTUMsTUFBTixDQUFhLGNBQWIsRUFBNkIsS0FBS08saUJBQWxDO0FBQ0FSLGNBQU1DLE1BQU4sQ0FBYSxxQkFBYixFQUFvQyxLQUFLTyxpQkFBekM7QUFDQVIsY0FBTUMsTUFBTixDQUFhLG1DQUFiLEVBQWtELEtBQUtPLGlCQUF2RDtBQUNBUixjQUFNQyxNQUFOLENBQWEsb0JBQWIsRUFBbUMsS0FBS08saUJBQXhDO0FBQ0FSLGNBQU1DLE1BQU4sQ0FBYSw0QkFBYixFQUEyQyxLQUFLTyxpQkFBaEQ7QUFDSCxLQXBGNEI7QUFzRjdCTixXQXRGNkIscUJBc0ZuQjtBQUNOLGFBQUt4QixVQUFMLEdBQWtCeUIsRUFBRSxLQUFLQyxHQUFQLEVBQVlDLElBQVosQ0FBaUIsYUFBakIsQ0FBbEI7QUFDQSxhQUFLM0IsVUFBTCxDQUFnQkksZ0JBQWhCOztBQUVBcUIsVUFBRSx5QkFBRixFQUE2QitCLE9BQTdCOztBQUVBL0IsVUFBRSx5QkFBRixFQUE2QmdDLEVBQTdCLENBQWdDLG1CQUFoQyxFQUFxRCxZQUFZO0FBQzdEaEMsY0FBRSxhQUFGLEVBQWlCLElBQWpCLEVBQXVCaUMsUUFBdkIsQ0FBZ0M7QUFDNUJDLHVCQUFPLENBRHFCO0FBRTVCQyw0QkFBWTtBQUZnQixhQUFoQztBQUlILFNBTEQ7QUFNSDtBQWxHNEIsQ0FBUixDQUF6QixDOzs7Ozs7O0FDQUE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM5Q0EsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDdkNBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9ub3RpZmljYXRpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQyMSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsInJlcXVpcmUoJy4vY29tcG9uZW50cy9ub3RpZmljYXRpb25zL2ZyaWVuZC1yZXF1ZXN0LmpzJyk7XHJcbnJlcXVpcmUoJy4vY29tcG9uZW50cy9ub3RpZmljYXRpb25zL2luYm94LmpzJyk7XHJcbnJlcXVpcmUoJy4vY29tcG9uZW50cy9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbi5qcycpO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL25vdGlmaWNhdGlvbi5qcyIsIjx0ZW1wbGF0ZT5cclxuPGxpPlxyXG4gICAgPGEgQGNsaWNrPVwiYWN0aW9uXCIgaHJlZj1cIiNcIiA6Y2xhc3M9XCJ7ICdub3QtcmVhZCc6ICFpc1JlYWQgfVwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIG5vdGlmLWltYWdlXCI+XHJcbiAgICAgICAgICAgICAgICA8aW1nIDpzcmM9XCJub3RpZmljYXRpb24uY2hhdF9yb29tLnVzZXJzWzBdLmF2YXRhclwiPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy03IG5vdGlmLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyIG5vdGlmLXRpdGxlIGVsbGlwc2lzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IGZ1bGxOYW1lIH19XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyIG5vdGlmLWJvZHkgZWxsaXBzaXNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge3sgbm90aWZpY2F0aW9uLmJvZHkgfX1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0zIG5vdGlmLWRhdGVcIj5cclxuICAgICAgICAgICAgICAgIDxzbWFsbD57eyBmcm9tTm93IH19PC9zbWFsbD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2E+XHJcbjwvbGk+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczogWydub3RpZmljYXRpb24nXSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgYWN0aW9uKCkge1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLnBvc3QoJy9tZXNzYWdlcy8nICsgdGhpcy5ub3RpZmljYXRpb24uaWQgKyAnL21hcmstYXMtcmVhZCcpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRwYXJlbnQuZ2V0Tm90aWZpY2F0aW9ucygpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLm5vdGlmaWNhdGlvbi5jaGF0X3Jvb20ubmFtZSkge1xyXG4gICAgICAgICAgICAgICAgLy9yZXR1cm4gd2luZG93LmxvY2F0aW9uLmhyZWYgPSAnL21lc3NhZ2VzLyMvY3IvJyArIHRoaXMubm90aWZpY2F0aW9uLmNoYXRfcm9vbV9pZDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdmFyIGNoYXQgPSBbXTtcclxuICAgICAgICAgICAgJChcIi5wYW5lbC1jaGF0LWJveFwiKS5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBjaGF0LnB1c2goJCh0aGlzKS5hdHRyKCdpZCcpKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5ub3RpZmljYXRpb24uY2hhdF9yb29tLm5hbWUpIHtcclxuICAgICAgICAgICAgICAgIGlmKCQuaW5BcnJheSgndXNlcicrdGhpcy5ub3RpZmljYXRpb24uY2hhdF9yb29tLm5hbWUsY2hhdCk9PSctMScpXHJcbiAgICAgICAgICAgICAgICAgICAgRXZlbnQuZmlyZSgnY2hhdC1ib3guc2hvdycsIHRoaXMubm90aWZpY2F0aW9uKTtcclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgaWYoJC5pbkFycmF5KCd1c2VyJyt0aGlzLm5vdGlmaWNhdGlvbi5jaGF0X3Jvb20udXNlcnNbMF0uaWQsY2hhdCk9PSctMScpXHJcbiAgICAgICAgICAgICAgICAgICAgRXZlbnQuZmlyZSgnY2hhdC1ib3guc2hvdycsIHRoaXMubm90aWZpY2F0aW9uLmNoYXRfcm9vbS51c2Vyc1swXSk7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAkKCcuanMtYXV0by1zaXplJykudGV4dGFyZWFBdXRvU2l6ZSgpO1xyXG4gICAgICAgICAgICB9LCA1MDAwKTtcclxuICAgICAgICB9LFxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGZyb21Ob3coKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBtb21lbnQodGhpcy5ub3RpZmljYXRpb24uY3JlYXRlZF9hdCkuZnJvbU5vdygpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGhyZWYoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm5vdGlmaWNhdGlvbi5jaGF0X3Jvb20ubmFtZSkge1xyXG4gICAgICAgICAgICAgICAgLy9yZXR1cm4gJy9tZXNzYWdlcy8jL2NyLycgKyB0aGlzLm5vdGlmaWNhdGlvbi5jaGF0X3Jvb21faWQ7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiAnL21lc3NhZ2VzLyMvdS8nICsgdGhpcy5ub3RpZmljYXRpb24uY2hhdF9yb29tLnVzZXJzWzBdLmlkO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGlzUmVhZCgpIHtcclxuICAgICAgICAgICAgaWYodGhpcy5ub3RpZmljYXRpb24udXNlcl9pZCA9PSAgd2luZG93LkxhcmF2ZWwudXNlci5pZClcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gISEgdGhpcy5ub3RpZmljYXRpb24ucmVhZF9hdDtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZ1bGxOYW1lKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5ub3RpZmljYXRpb24uY2hhdF9yb29tLm5hbWUpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpZmljYXRpb24uY2hhdF9yb29tLm5hbWU7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLm5vdGlmaWNhdGlvbi5jaGF0X3Jvb20udXNlcnNbMF0uZmlyc3RfbmFtZSArICcgJyArIHRoaXMubm90aWZpY2F0aW9uLmNoYXRfcm9vbS51c2Vyc1swXS5sYXN0X25hbWU7IFxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIEluYm94TWVzc2FnZS52dWU/OWFjZmRiYWMiLCI8dGVtcGxhdGU+XHJcbjxsaT5cclxuICAgIDxhIEBjbGljaz1cIm1hcmtBc1JlYWRcIiB0YXJnZXQ9XCJfYmxhbmtcIiA6aHJlZj1cImhyZWZcIiA6Y2xhc3M9XCJ7ICdub3QtcmVhZCc6ICFpc1JlYWQgfVwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiBkYXRhLXBsYWNlbWVudD1cImxlZnRcIiBkYXRhLWNvbnRhaW5lcj1cImJvZHlcIiA6dGl0bGU9XCJub3RpZmljYXRpb24uZGF0YS5ib2R5XCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBub3RpZi1pbWFnZVwiPlxyXG4gICAgICAgICAgICAgICAgPGltZyA6c3JjPVwibm90aWZpY2F0aW9uLmRhdGEuaW1hZ2VcIj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtNyBub3RpZi1jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMiBub3RpZi10aXRsZSBlbGxpcHNpc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7eyBub3RpZmljYXRpb24uZGF0YS50aXRsZSB9fVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMiBub3RpZi1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IG5vdGlmaWNhdGlvbi5kYXRhLmJvZHkgfX1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0zIG5vdGlmLWRhdGVcIj5cclxuICAgICAgICAgICAgICAgIDxzbWFsbD57eyBmcm9tTm93IH19PC9zbWFsbD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2E+XHJcbjwvbGk+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczogWydub3RpZmljYXRpb24nXSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgbWFya0FzUmVhZCgpIHtcclxuICAgICAgICAgICAgaWYgKCEgdGhpcy5ub3RpZmljYXRpb24ucmVhZF9hdCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEucG9zdCgnL25vdGlmaWNhdGlvbnMvJyArIHRoaXMubm90aWZpY2F0aW9uLmlkICsgJy9tYXJrLWFzLXJlYWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBocmVmKCkge1xyXG4gICAgICAgICAgICBpZiAoISF+dGhpcy50eXBlLnNlYXJjaCgnZG9jdW1lbnQnKSkge1xyXG4gICAgICAgICAgICAgICAgIGlmKCEhfnRoaXMubm90aWZpY2F0aW9uLmRhdGEuYm9keS5zZWFyY2goJ3VwbG9hZGVkIGEgbmV3IGRvY3VtZW50Jykpe1xyXG4gICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ2h0dHA6Ly8nK0xhcmF2ZWwuY3BhbmVsX3VybCsnL2RvY3VtZW50cyc7XHJcbiAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICcvcHJvZmlsZT9vcGVuPWRvY3VtZW50cyc7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGVsc2UgaWYgKCEhfnRoaXMudHlwZS5zZWFyY2goJ25ldy1pc3N1ZScpIHx8ICEhfnRoaXMudHlwZS5zZWFyY2goJ2lzc3VlLWFzc2lnbm1lbnQnKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdodHRwOi8vJytMYXJhdmVsLmNwYW5lbF91cmwrJy9jdXN0b21lci1zZXJ2aWNlJztcclxuICAgICAgICAgICAgfSBcclxuXHJcbiAgICAgICAgICAgIGVsc2UgaWYgKCEhfnRoaXMudHlwZS5zZWFyY2goJ2ZlZWRiYWNrJykpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnL2ZlZWRiYWNrJztcclxuICAgICAgICAgICAgfSBcclxuXHJcbiAgICAgICAgICAgIGVsc2UgaWYgKCEhfnRoaXMudHlwZS5zZWFyY2goJ2ZyaWVuZCcpKSB7XHJcbiAgICAgICAgICAgICAgICBpZighIX50aGlzLm5vdGlmaWNhdGlvbi5kYXRhLmJvZHkuc2VhcmNoKCdhY2NlcHRlZCB5b3VyIGZyaWVuZCByZXF1ZXN0Jykpe1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnL3VzZXJzLycrdGhpcy5ub3RpZmljYXRpb24uZnJvbV9pZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiAnL2ZyaWVuZHMnO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBlbHNlIGlmICghIX50aGlzLnR5cGUuc2VhcmNoKCduZXctcHVyY2hhc2UnKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICcvdXNlci9zYWxlcy1yZXBvcnQnO1xyXG4gICAgICAgICAgICB9IFxyXG5cclxuICAgICAgICAgICAgZWxzZSBpZiAoISF+dGhpcy50eXBlLnNlYXJjaCgnb3JkZXItc3RhdHVzLXVwZGF0ZScpKSB7XHJcbiAgICAgICAgICAgICAgICBpZighIX50aGlzLm5vdGlmaWNhdGlvbi5kYXRhLmJvZHkuc2VhcmNoKCdyZXF1ZXN0ZWQgdG8gcmV0dXJuIHlvdXIgaXRlbScpKXtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJy91c2VyL3NhbGVzLXJlcG9ydCc7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIGlmKCEhfnRoaXMubm90aWZpY2F0aW9uLmRhdGEuYm9keS5zZWFyY2goJ3N1Y2Nlc3NmdWxseSByZWNlaXZlZCB0aGUgaXRlbScpKXtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJy91c2VyL3NhbGVzLXJlcG9ydCc7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICcvdXNlci9wdXJjaGFzZS1yZXBvcnQnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IFxyXG5cclxuICAgICAgICAgICAgZWxzZSBpZiAoISF+dGhpcy50eXBlLnNlYXJjaCgnbmV3LXByb2R1Y3QtcmV2aWV3JykgfHwgISF+dGhpcy50eXBlLnNlYXJjaCgnbmV3LXByb2R1Y3QtcmV2aWV3LWNvbW1lbnQnKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubm90aWZpY2F0aW9uLmRhdGEucHJvZHVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnL3Byb2R1Y3QvJyArIHRoaXMubm90aWZpY2F0aW9uLmRhdGEucHJvZHVjdC5zbHVnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiAnIyc7XHJcbiAgICAgICAgICAgIH0gXHJcblxyXG4gICAgICAgICAgICBlbHNlIGlmICghIX50aGlzLnR5cGUuc2VhcmNoKCdjaGFuZ2UtZXdhbGxldC10cmFuc2FjdGlvbi1zdGF0dXMnKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICcvdXNlci9ld2FsbGV0JztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmICghIX50aGlzLnR5cGUuc2VhcmNoKCduZXctZXdhbGxldC10cmFuc2FjdGlvbicpKSB7XHJcbiAgICAgICAgICAgICAgICBpZighIX50aGlzLm5vdGlmaWNhdGlvbi5kYXRhLmJvZHkuc2VhcmNoKCd1cGxvYWRlZCBhIGRlcG9zaXQgc2xpcCcpIHx8ICEhfnRoaXMubm90aWZpY2F0aW9uLmRhdGEuYm9keS5zZWFyY2goJ2RlcG9zaXRlZCBtb25leSBmb3IgYXBwcm92YWwnKSB8fCAhIX50aGlzLm5vdGlmaWNhdGlvbi5kYXRhLmJvZHkuc2VhcmNoKCdyZXF1ZXN0ZWQgZm9yIGEgV2l0aGRyYXcnKSl7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdodHRwOi8vJytMYXJhdmVsLmNwYW5lbF91cmwrJy9ld2FsbGV0JztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiAnL3VzZXIvZXdhbGxldCc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuICcjJztcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBmcm9tTm93KCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpZmljYXRpb24uaHVtYW5fY3JlYXRlZF9hdDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBpc1JlYWQoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAhISB0aGlzLm5vdGlmaWNhdGlvbi5yZWFkX2F0O1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHR5cGUoKSB7XHJcbiAgICAgICAgICAgIGxldCBhcnJUeXBlID0gdGhpcy5ub3RpZmljYXRpb24udHlwZS5zcGxpdCgnXFxcXCcpO1xyXG4gICAgICAgICAgICBsZXQgdHlwZSA9IGFyclR5cGVbYXJyVHlwZS5sZW5ndGggLSAxXS5yZXBsYWNlKC8oW2Etel0oPz1bQS1aXSkpL2csICckMS0nKS50b0xvd2VyQ2FzZSgpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHR5cGU7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCk7XHJcblxyXG4gICAgICAgICQoJy5ub3RpZi1ib2R5JywgdGhpcy4kZWwpLmVsbGlwc2lzKHtcclxuICAgICAgICAgICAgbGluZXM6IDIsXHJcbiAgICAgICAgICAgIHJlc3BvbnNpdmU6IHRydWVcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE5vdGlmaWNhdGlvbi52dWU/ZWEyMDVlNWUiLCI8dGVtcGxhdGU+XHJcbjxsaT5cclxuICAgIDxhIEBjbGljay5wcmV2ZW50IGhyZWY9XCIjXCIgOmNsYXNzPVwieyAnbm90LXJlYWQnOiAhaXNSZWFkIH1cIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBub3RpZi1pbWFnZVwiPlxyXG4gICAgICAgICAgICAgICAgPGltZyA6c3JjPVwibm90aWZpY2F0aW9uLmRhdGEuaW1hZ2VcIj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtNSBub3RpZi1jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMiBub3RpZi10aXRsZSBlbGxpcHNpc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7eyBub3RpZmljYXRpb24uZGF0YS51c2VyLmZ1bGxfbmFtZSB9fVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMiBub3RpZi1ib2R5IGVsbGlwc2lzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt7IGhsYW5nWydzZW50J10gfX1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy01IG5vdGlmLWJ1dHRvblwiPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBAY2xpY2s9XCJjb25maXJtXCI+e3sgY2xhbmdbJ2NvbmZpcm0nXSB9fTwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBAY2xpY2s9XCJkZWxldGVSZXF1ZXN0XCI+e3sgY2xhbmdbJ2RlbGV0ZSddIH19PC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgPC9hPlxyXG48L2xpPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgZGF0YSgpe1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGhsYW5nOltdLFxyXG4gICAgICAgICAgICBjbGFuZzpbXVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBwcm9wczogWydub3RpZmljYXRpb24nXSxcclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0SGVhZGVyVHJhbnNsYXRpb24oKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvcy5nZXQoJy90cmFuc2xhdGUvaGVhZGVyJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZ1bmN0aW9uIGdldENvbW1vblRyYW5zbGF0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2NvbW1vbicpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYXhpb3MuYWxsKFtcclxuICAgICAgICAgICAgZ2V0SGVhZGVyVHJhbnNsYXRpb24oKSxcclxuICAgICAgICAgICAgZ2V0Q29tbW9uVHJhbnNsYXRpb24oKVxyXG4gICAgICAgIF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG4gICAgICAgICAgICAoXHJcbiAgICAgICAgICAgIGh0cmFuc2xhdGlvbixcclxuICAgICAgICAgICAgY3RyYW5zbGF0aW9uXHJcbiAgICAgICAgICAgICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHRoaXMuaGxhbmcgPSBodHJhbnNsYXRpb24uZGF0YS5kYXRhO1xyXG4gICAgICAgICAgICB0aGlzLmNsYW5nID0gY3RyYW5zbGF0aW9uLmRhdGEuZGF0YTtcclxuXHJcblxyXG4gICAgICAgIH0pKVxyXG4gICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgY29uZmlybSgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEucG9zdCgnL2ZyaWVuZHMvJyArIHRoaXMubm90aWZpY2F0aW9uLmRhdGEuZnJpZW5kLmlkICsgJy9jb25maXJtJywge1xyXG4gICAgICAgICAgICAgICAgbm90aWZfaWQ6IHRoaXMubm90aWZpY2F0aW9uLmlkXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIEV2ZW50LmZpcmUoJ2ZyaWVuZC1yZXF1ZXN0LmFjY2VwdGVkJywgdGhpcy5ub3RpZmljYXRpb24pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBkZWxldGVSZXF1ZXN0KCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvZnJpZW5kcy8nICsgdGhpcy5ub3RpZmljYXRpb24uZGF0YS5mcmllbmQuaWQgKyAnL2RlbGV0ZS1yZXF1ZXN0Jywge1xyXG4gICAgICAgICAgICAgICAgbm90aWZfaWQ6IHRoaXMubm90aWZpY2F0aW9uLmlkXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIEV2ZW50LmZpcmUoJ2ZyaWVuZC1yZXF1ZXN0LmRlbGV0ZWQnLCB0aGlzLm5vdGlmaWNhaXRvbik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBpc1JlYWQoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAhISB0aGlzLm5vdGlmaWNhdGlvbi5yZWFkX2F0O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFJlcXVlc3QudnVlPzM3YjgwZWJkIiwid2luZG93Lld5Y05vdGlmRnJpZW5kUmVxdWVzdCA9IG5ldyBWdWUoe1xyXG4gICAgZWw6ICcjZnJpZW5kLXJlcXVlc3QnLFxyXG5cclxuICAgIGRhdGE6IHtcclxuICAgICAgICBub3RpZmljYXRpb25zOiBbXSxcclxuICAgICAgICBub3RTZWVuQ291bnQ6IG51bGwsXHJcbiAgICAgICAgJGNvbnRhaW5lcjogbnVsbCxcclxuICAgIH0sXHJcblxyXG4gICAgd2F0Y2g6IHtcclxuICAgICAgICBub3RpZmljYXRpb25zKHZhbCkge1xyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJGNvbnRhaW5lci5wZXJmZWN0U2Nyb2xsYmFyKCd1cGRhdGUnKTtcclxuICAgICAgICAgICAgfSwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgZ2V0Tm90aWZpY2F0aW9ucygpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvZnJpZW5kcy9ub3RpZmljYXRpb25zJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zID0gcmVzdWx0LmRhdGEubm90aWZpY2F0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdFNlZW5Db3VudCA9IHJlc3VsdC5kYXRhLm5vdHNlZW5fY291bnQ7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBtYXJrQWxsQXNTZWVuKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvZnJpZW5kcy9tYXJrLWFsbC1hcy1zZWVuJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RTZWVuQ291bnQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgYWNjZXB0ZWQoZGF0YSkge1xyXG4gICAgICAgICAgICB2YXIgaW5kZXggPSB0aGlzLm5vdGlmaWNhdGlvbnMuaW5kZXhPZihkYXRhKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucygpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGRlbGV0ZWQoZGF0YSkge1xyXG4gICAgICAgICAgICB2YXIgaW5kZXggPSB0aGlzLm5vdGlmaWNhdGlvbnMuaW5kZXhPZihkYXRhKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucygpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBjb3VudCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubm90U2VlbkNvdW50ID8gdGhpcy5ub3RTZWVuQ291bnQgOiAnJztcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgICAncmVxdWVzdCc6IHJlcXVpcmUoJy4vUmVxdWVzdC52dWUnKVxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucygpO1xyXG5cclxuICAgICAgICBFdmVudC5saXN0ZW4oJ25ldy1mcmllbmQtcmVxdWVzdCcsIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucyk7XHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCdmcmllbmQtcmVxdWVzdC5hY2NlcHRlZCcsIHRoaXMuYWNjZXB0ZWQpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignZnJpZW5kLXJlcXVlc3QuZGVsZXRlZCcsIHRoaXMuZGVsZXRlZCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgdGhpcy4kY29udGFpbmVyID0gJCh0aGlzLiRlbCkuZmluZCgnLnBhbmVsLWJvZHknKTtcclxuICAgICAgICB0aGlzLiRjb250YWluZXIucGVyZmVjdFNjcm9sbGJhcigpO1xyXG4gICAgfVxyXG59KTtcclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9ub3RpZmljYXRpb25zL2ZyaWVuZC1yZXF1ZXN0LmpzIiwid2luZG93Lld5Y05vdGlmSW5ib3ggPSBuZXcgVnVlKHtcclxuICAgIGVsOiAnI2luYm94JyxcclxuXHJcbiAgICBkYXRhOiB7XHJcbiAgICAgICAgbm90aWZpY2F0aW9uczogW10sXHJcbiAgICAgICAgJG1lc3NhZ2VDb250YWluZXI6IG51bGwsXHJcbiAgICB9LFxyXG5cclxuICAgIHdhdGNoOiB7XHJcbiAgICAgICAgbm90aWZpY2F0aW9ucyh2YWwpIHtcclxuICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRtZXNzYWdlQ29udGFpbmVyLnBlcmZlY3RTY3JvbGxiYXIoJ3VwZGF0ZScpO1xyXG4gICAgICAgICAgICB9LCAxKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXROb3RpZmljYXRpb25zKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9tZXNzYWdlcy9ub3RpZmljYXRpb25zMicpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0Tm90aWZpY2F0aW9uczIoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL21lc3NhZ2VzL25vdGlmaWNhdGlvbnMyJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zID0gcmVzdWx0LmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vdGlmaWNhdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNoYXQgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIi5wYW5lbC1jaGF0LWJveFwiKS5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGF0LnB1c2goJCh0aGlzKS5hdHRyKCdpZCcpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBveHAgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKG5vdGlmKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihub3RpZi51c2VyX2lkICE9IHdpbmRvdy5MYXJhdmVsLnVzZXIuaWQpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghIG5vdGlmLnNlZW5fYXQgJiYgb3hwID09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChub3RpZi5jaGF0X3Jvb20ubmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoJC5pbkFycmF5KCd1c2VyJytub3RpZi5jaGF0X3Jvb20ubmFtZSxjaGF0KT09Jy0xJylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LWJveC5zaG93Jywgbm90aWYpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZigkLmluQXJyYXkoJ3VzZXInK25vdGlmLmNoYXRfcm9vbS51c2Vyc1swXS5pZCxjaGF0KT09Jy0xJylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LWJveC5zaG93Jywgbm90aWYuY2hhdF9yb29tLnVzZXJzWzBdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBveHAgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vbm90aWYgc291bmRcclxuICAgICAgICAgICAgICAgICAgICB2YXIgYXVkaW8gPSBuZXcgQXVkaW8oJy9zb3VuZC93eWNub3RpZi53YXYnKTtcclxuICAgICAgICAgICAgICAgICAgICBhdWRpby5wbGF5KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgbWFya0FsbEFzU2VlbigpIHtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKG5vdGlmKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoISBub3RpZi5zZWVuX2F0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbm90aWYuc2Vlbl9hdCA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5nZXROb3RpZmljYXRpb25zKCk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvbWVzc2FnZXMvbWFyay1hbGwtYXMtc2VlbjInKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgY291bnQoKSB7XHJcbiAgICAgICAgICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZiAodGhpcy5ub3RpZmljYXRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbnMuZm9yRWFjaChmdW5jdGlvbiAobm90aWYpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZihub3RpZi51c2VyX2lkICE9IHdpbmRvdy5MYXJhdmVsLnVzZXIuaWQpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoISBub3RpZi5zZWVuX2F0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICArK2NvdW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBjb3VudCB8fCAnJztcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgICAnaW5ib3gtbWVzc2FnZSc6IHJlcXVpcmUoJy4vSW5ib3hNZXNzYWdlLnZ1ZScpXHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5nZXROb3RpZmljYXRpb25zKCk7XHJcblxyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignbmV3LWNoYXQtbWVzc2FnZScsIHRoaXMuZ2V0Tm90aWZpY2F0aW9uczIpO1xyXG4gICAgfSxcclxuXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMuJG1lc3NhZ2VDb250YWluZXIgPSAkKHRoaXMuJGVsKS5maW5kKCcucGFuZWwtYm9keScpO1xyXG4gICAgICAgIHRoaXMuJG1lc3NhZ2VDb250YWluZXIucGVyZmVjdFNjcm9sbGJhcigpO1xyXG4gICAgfVxyXG59KTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL25vdGlmaWNhdGlvbnMvaW5ib3guanMiLCJ3aW5kb3cuV3ljTm90aWZHZW5lcmFsID0gbmV3IFZ1ZSh7XHJcbiAgICBlbDogJyNub3RpZmljYXRpb24nLFxyXG5cclxuICAgIGRhdGE6IHtcclxuICAgICAgICBub3RpZmljYXRpb25zOiBbXSxcclxuICAgICAgICAkY29udGFpbmVyOiBudWxsLFxyXG4gICAgfSxcclxuXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgIG5vdGlmaWNhdGlvbnModmFsKSB7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kY29udGFpbmVyLnBlcmZlY3RTY3JvbGxiYXIoJ3VwZGF0ZScpO1xyXG4gICAgICAgICAgICB9LCAxKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXROb3RpZmljYXRpb25zKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9ub3RpZmljYXRpb25zJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICAvL3VwY29taW5nIG5vdGlmIHdpdGggc291bmRcclxuICAgICAgICBnZXROb3RpZmljYXRpb25zMigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvbm90aWZpY2F0aW9ucycpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBhdWRpbyA9IG5ldyBBdWRpbygnL3NvdW5kL3d5Y25vdGlmLm1wMycpO1xyXG4gICAgICAgICAgICAgICAgICAgIGF1ZGlvLnBsYXkoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG1hcmtBbGxBc1NlZW4oKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucy5mb3JFYWNoKGZ1bmN0aW9uIChub3RpZikge1xyXG4gICAgICAgICAgICAgICAgaWYgKCEgbm90aWYuc2Vlbl9hdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIG5vdGlmLnNlZW5fYXQgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLnBvc3QoJy9ub3RpZmljYXRpb25zL21hcmstYWxsLWFzLXNlZW4nKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZW1vdmVOb3RpZihkYXRhKSB7XHJcbiAgICAgICAgICAgIHZhciBpbmRleCA9IHRoaXMubm90aWZpY2F0aW9ucy5pbmRleE9mKGRhdGEpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGNvdW50KCkge1xyXG4gICAgICAgICAgICBsZXQgY291bnQgPSAwO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMubm90aWZpY2F0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKG5vdGlmKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEgbm90aWYuc2Vlbl9hdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICArK2NvdW50O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gY291bnQgfHwgJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgbm90aWZpY2F0aW9uOiByZXF1aXJlKCcuL05vdGlmaWNhdGlvbi52dWUnKVxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucygpO1xyXG5cclxuICAgICAgICBFdmVudC5saXN0ZW4oJ2ZyaWVuZC1yZXF1ZXN0LWFjY2VwdGVkJywgdGhpcy5nZXROb3RpZmljYXRpb25zMik7XHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCdmZWVkYmFjay1hcHByb3ZlZCcsIHRoaXMuZ2V0Tm90aWZpY2F0aW9uczIpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignZG9jdW1lbnQtYXBwcm92ZWQnLCB0aGlzLmdldE5vdGlmaWNhdGlvbnMyKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ2RvY3VtZW50LWRlbmllZCcsIHRoaXMuZ2V0Tm90aWZpY2F0aW9uczIpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignZXhwaXJlZC1kb2N1bWVudCcsIHRoaXMuZ2V0Tm90aWZpY2F0aW9uczIpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignbmV3LXB1cmNoYXNlJywgdGhpcy5nZXROb3RpZmljYXRpb25zMik7XHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCdvcmRlci1zdGF0dXMtdXBkYXRlJywgdGhpcy5nZXROb3RpZmljYXRpb25zMik7XHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCdjaGFuZ2UtZXdhbGxldC10cmFuc2FjdGlvbi1zdGF0dXMnLCB0aGlzLmdldE5vdGlmaWNhdGlvbnMyKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ25ldy1wcm9kdWN0LXJldmlldycsIHRoaXMuZ2V0Tm90aWZpY2F0aW9uczIpO1xyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignbmV3LXByb2R1Y3QtcmV2aWV3LWNvbW1lbnQnLCB0aGlzLmdldE5vdGlmaWNhdGlvbnMyKTtcclxuICAgIH0sXHJcblxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICB0aGlzLiRjb250YWluZXIgPSAkKHRoaXMuJGVsKS5maW5kKCcucGFuZWwtYm9keScpO1xyXG4gICAgICAgIHRoaXMuJGNvbnRhaW5lci5wZXJmZWN0U2Nyb2xsYmFyKCk7XHJcblxyXG4gICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCk7XHJcblxyXG4gICAgICAgICQoJy5kcm9wZG93bi1nZW5lcmFsLW5vdGlmJykub24oJ3Nob3duLmJzLmRyb3Bkb3duJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKCcubm90aWYtYm9keScsIHRoaXMpLmVsbGlwc2lzKHtcclxuICAgICAgICAgICAgICAgIGxpbmVzOiAyLFxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSk7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbi5qcyIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0luYm94TWVzc2FnZS52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTZhZjcxNzE4XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0luYm94TWVzc2FnZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxcbm90aWZpY2F0aW9uc1xcXFxJbmJveE1lc3NhZ2UudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gSW5ib3hNZXNzYWdlLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi02YWY3MTcxOFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTZhZjcxNzE4XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9ub3RpZmljYXRpb25zL0luYm94TWVzc2FnZS52dWVcbi8vIG1vZHVsZSBpZCA9IDMzNlxuLy8gbW9kdWxlIGNodW5rcyA9IDciLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Ob3RpZmljYXRpb24udnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi01ZDFiMTViZVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Ob3RpZmljYXRpb24udnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXG5vdGlmaWNhdGlvbnNcXFxcTm90aWZpY2F0aW9uLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIE5vdGlmaWNhdGlvbi52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNWQxYjE1YmVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi01ZDFiMTViZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvbm90aWZpY2F0aW9ucy9Ob3RpZmljYXRpb24udnVlXG4vLyBtb2R1bGUgaWQgPSAzMzdcbi8vIG1vZHVsZSBjaHVua3MgPSA3IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vUmVxdWVzdC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LWEzOGY1NWU4XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1JlcXVlc3QudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXG5vdGlmaWNhdGlvbnNcXFxcUmVxdWVzdC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBSZXF1ZXN0LnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1hMzhmNTVlOFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWEzOGY1NWU4XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9ub3RpZmljYXRpb25zL1JlcXVlc3QudnVlXG4vLyBtb2R1bGUgaWQgPSAzMzhcbi8vIG1vZHVsZSBjaHVua3MgPSA3IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsaScsIFtfYygnYScsIHtcbiAgICBjbGFzczoge1xuICAgICAgJ25vdC1yZWFkJzogIV92bS5pc1JlYWRcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcInRhcmdldFwiOiBcIl9ibGFua1wiLFxuICAgICAgXCJocmVmXCI6IF92bS5ocmVmXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0ubWFya0FzUmVhZFxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcImRhdGEtcGxhY2VtZW50XCI6IFwibGVmdFwiLFxuICAgICAgXCJkYXRhLWNvbnRhaW5lclwiOiBcImJvZHlcIixcbiAgICAgIFwidGl0bGVcIjogX3ZtLm5vdGlmaWNhdGlvbi5kYXRhLmJvZHlcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0yIG5vdGlmLWltYWdlXCJcbiAgfSwgW19jKCdpbWcnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5ub3RpZmljYXRpb24uZGF0YS5pbWFnZVxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtNyBub3RpZi1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTEyIG5vdGlmLXRpdGxlIGVsbGlwc2lzXCJcbiAgfSwgW192bS5fdihcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5ub3RpZmljYXRpb24uZGF0YS50aXRsZSkgKyBcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMTIgbm90aWYtYm9keVwiXG4gIH0sIFtfdm0uX3YoXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0ubm90aWZpY2F0aW9uLmRhdGEuYm9keSkgKyBcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgXCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0zIG5vdGlmLWRhdGVcIlxuICB9LCBbX2MoJ3NtYWxsJywgW192bS5fdihfdm0uX3MoX3ZtLmZyb21Ob3cpKV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNWQxYjE1YmVcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi01ZDFiMTViZVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL25vdGlmaWNhdGlvbnMvTm90aWZpY2F0aW9uLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzg2XG4vLyBtb2R1bGUgY2h1bmtzID0gNyIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGknLCBbX2MoJ2EnLCB7XG4gICAgY2xhc3M6IHtcbiAgICAgICdub3QtcmVhZCc6ICFfdm0uaXNSZWFkXG4gICAgfSxcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0uYWN0aW9uXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMiBub3RpZi1pbWFnZVwiXG4gIH0sIFtfYygnaW1nJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInNyY1wiOiBfdm0ubm90aWZpY2F0aW9uLmNoYXRfcm9vbS51c2Vyc1swXS5hdmF0YXJcbiAgICB9XG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTcgbm90aWYtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMiBub3RpZi10aXRsZSBlbGxpcHNpc1wiXG4gIH0sIFtfdm0uX3YoXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uZnVsbE5hbWUpICsgXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIFwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTEyIG5vdGlmLWJvZHkgZWxsaXBzaXNcIlxuICB9LCBbX3ZtLl92KFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLm5vdGlmaWNhdGlvbi5ib2R5KSArIFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBcIildKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTMgbm90aWYtZGF0ZVwiXG4gIH0sIFtfYygnc21hbGwnLCBbX3ZtLl92KF92bS5fcyhfdm0uZnJvbU5vdykpXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi02YWY3MTcxOFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTZhZjcxNzE4XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvbm90aWZpY2F0aW9ucy9JbmJveE1lc3NhZ2UudnVlXG4vLyBtb2R1bGUgaWQgPSAzOTNcbi8vIG1vZHVsZSBjaHVua3MgPSA3IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsaScsIFtfYygnYScsIHtcbiAgICBjbGFzczoge1xuICAgICAgJ25vdC1yZWFkJzogIV92bS5pc1JlYWRcbiAgICB9LFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCIjXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0yIG5vdGlmLWltYWdlXCJcbiAgfSwgW19jKCdpbWcnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5ub3RpZmljYXRpb24uZGF0YS5pbWFnZVxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtNSBub3RpZi1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTEyIG5vdGlmLXRpdGxlIGVsbGlwc2lzXCJcbiAgfSwgW192bS5fdihcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5ub3RpZmljYXRpb24uZGF0YS51c2VyLmZ1bGxfbmFtZSkgKyBcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMTIgbm90aWYtYm9keSBlbGxpcHNpc1wiXG4gIH0sIFtfdm0uX3YoXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uaGxhbmdbJ3NlbnQnXSkgKyBcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgXCIpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy01IG5vdGlmLWJ1dHRvblwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS5jb25maXJtXG4gICAgfVxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uY2xhbmdbJ2NvbmZpcm0nXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLmRlbGV0ZVJlcXVlc3RcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5jbGFuZ1snZGVsZXRlJ10pKV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtYTM4ZjU1ZThcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1hMzhmNTVlOFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL25vdGlmaWNhdGlvbnMvUmVxdWVzdC52dWVcbi8vIG1vZHVsZSBpZCA9IDM5NlxuLy8gbW9kdWxlIGNodW5rcyA9IDciXSwic291cmNlUm9vdCI6IiJ9