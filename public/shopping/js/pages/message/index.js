/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 432);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 182:
/***/ (function(module, exports, __webpack_require__) {

new Vue({
    el: '.messages-container',

    data: {
        userLatestMessages: [],
        selected: null,

        lang: []
    },

    router: new VueRouter({
        routes: [{
            path: '/u/:id',
            name: 'user',
            component: __webpack_require__(358)
        }, {
            path: '/cr/:id',
            name: 'chat-room',
            component: __webpack_require__(359)
        }]
    }),

    watch: {
        userLatestMessages: function userLatestMessages(val) {
            setTimeout(function () {
                $('.user-list-container').perfectScrollbar('update');
            }, 1);
        }
    },

    methods: {
        getLatestMessages: function getLatestMessages() {
            var _this = this;

            axiosAPIv1.get('/messages').then(function (result) {
                _this.userLatestMessages = result.data;
            });
        },
        loadSelectedUser: function loadSelectedUser(data) {
            this.selected = data;

            this.$router.push({
                name: 'user',
                params: {
                    id: data.id
                }
            });
        },
        loadSelectedChatRoom: function loadSelectedChatRoom(data) {
            this.selected = data;

            this.$router.push({
                name: 'chat-room',
                params: {
                    id: data.id
                }
            });
        },
        newMessage: function newMessage() {
            Event.fire('user-search.show');
        }
    },

    components: {
        'user-latest-message': __webpack_require__(361),
        'user-chat-room-message': __webpack_require__(360),
        'user-search': __webpack_require__(362)
    },

    created: function created() {
        var _this2 = this;

        this.getLatestMessages();

        Event.listen('user-search.selected', this.loadSelectedUser);
        Event.listen('chat-room.selected', this.loadSelectedChatRoom);
        Event.listen('chat-box.send', this.getLatestMessages);
        Event.listen('chat-box.marked-as-read', this.getLatestMessages);
        Event.listen('new-chat-message', this.getLatestMessages);

        axios.get('/translate/messages').then(function (language) {
            _this2.lang = language.data.data;
            toastr.info(_this2.lang['alert-msg'], _this2.lang['reminder'], {
                timeOut: 0,
                extendedTimeOut: 0
            });
        });

        setTimeout(function () {
            $('#toast-container').hide();
        }, 5000);
    }
});

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['message']
});

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['message'],

    computed: {
        avatar: function avatar() {
            return this.message.user.avatar;
        }
    }
});

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var settings = window.Laravel.user.settings ? window.Laravel.user.settings.settings : {};

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            user: null,
            messages: [],
            newMessage: null,
            currentPage: 0,
            chatRoomId: null,
            enterEnabled: settings ? !!settings.message_enter_enabled : false,
            $chatBox: null,
            $conversation: null
        };
    },


    watch: {
        $route: function $route(to, from) {
            Event.unlisten('chat-room-' + this.chatRoomId);

            this.user = null;
            this.messages = [];
            this.newMessage = null;
            this.currentPage = 0;
            this.chatRoomId = null;
            this.$chatBox = null;
            this.$conversation = null;

            this.getUser();
        },
        enterEnabled: function enterEnabled(val) {
            return axiosAPI.post('/user/settings', {
                'message_enter_enabled': val
            });
        }
    },

    methods: {
        onInfinite: function onInfinite() {
            this.getMessages();
        },
        getUser: function getUser() {
            var _this = this;

            return axiosAPIv1.get('/users/' + this.$route.params.id + '?include=relationship').then(function (result) {
                _this.user = result.data;

                setTimeout(function () {
                    var $el = $(this.$el);

                    this.$chatBox = $el.find('.chat-box');
                    this.$conversation = $el.find('.panel-body');
                    this.$conversation.perfectScrollbar();
                }.bind(_this), 1);

                _this.markAllAsRead();

                Event.fire('user-search.selected', result.data);

                _this.getMessages2();
            });
        },
        getMessages: function getMessages() {
            var _this2 = this;

            var prevScrollHeight = this.$conversation.prop('scrollHeight');

            return axiosAPIv1.get('/messages/user/' + this.user.id, {
                params: {
                    page: ++this.currentPage,
                    limit: 999
                }
            }).then(function (result) {
                var data = result.data.data;

                if (data && data.length) {
                    data.sort(function (a, b) {
                        return a.id - b.id;
                    });

                    _this2.messages = data.concat(_this2.messages);
                    _this2.currentPage = result.data.current_page;

                    if (!_this2.chatRoomId) {
                        _this2.chatRoomId = data[0].chat_room_id;
                        _this2.listen();
                    }

                    setTimeout(function () {
                        if (this.currentPage === 1) {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
                        } else {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight') - prevScrollHeight);
                        }

                        this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
                    }.bind(_this2), 1);
                } else {
                    _this2.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                }
            });
        },
        getMessages2: function getMessages2() {
            var _this3 = this;

            return axiosAPIv1.get('/messages/user/' + this.user.id, {
                params: {
                    page: ++this.currentPage,
                    limit: 999
                }
            }).then(function (result) {
                var data = result.data.data;

                if (data && data.length) {
                    data.sort(function (a, b) {
                        return a.id - b.id;
                    });

                    _this3.messages = data.concat(_this3.messages);
                    _this3.currentPage = result.data.current_page;

                    if (!_this3.chatRoomId) {
                        _this3.chatRoomId = data[0].chat_room_id;
                        _this3.listen();
                    }

                    setTimeout(function () {
                        if (this.currentPage === 1) {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
                        } else {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight') - prevScrollHeight);
                        }

                        this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
                    }.bind(_this3), 1);
                } else {
                    _this3.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                }
            });
        },
        markAllAsRead: function markAllAsRead() {
            return axiosAPIv1.post('/messages/mark-all-as-read', {
                to_user: this.user.id
            }).then(function (result) {
                Event.fire('chat-box.marked-as-read');
            });
        },
        send: function send(e) {
            var _this4 = this;

            if (e.keyCode === 13 && !this.enterEnabled || !this.newMessage || this.isBlocked) {
                return;
            }

            //if user sends spaces only
            if ($.trim(this.newMessage) == '') {
                $('#areamsg').val('');
                return;
            }

            if (e.keyCode === 13 && this.enterEnabled) {
                e.preventDefault();
            }

            this.messages.push({
                user_id: window.Laravel.user.id,
                body: this.newMessage
            });

            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));

                this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
            }.bind(this), 1);

            axiosAPIv1.post('/messages', {
                to_user: this.user.id,
                body: this.newMessage
            }).then(function (result) {
                if (!_this4.chatRoomId) {
                    _this4.chatRoomId = result.data.data.chat_room_id;
                    _this4.listen();
                }

                Event.fire('chat-box.send');
            });

            this.newMessage = null;
        },
        close: function close() {
            Event.fire('chat-box.close', this.user);
        },
        isMe: function isMe(message) {
            return window.Laravel.user.id == message.user_id;
        },
        scrollToBottom: function scrollToBottom() {
            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
            }.bind(this), 1);
        },
        listen: function listen() {
            Event.listen('chat-room-' + this.chatRoomId, function (message) {
                if (message.chat_room_id == this.chatRoomId) {
                    this.messages.push(message);
                    this.scrollToBottom();
                }
            }.bind(this));
            // var audio = new Audio('/sound/wycnotif.mp3');
            // audio.play();
        },
        request: function request() {
            var _this5 = this;

            return axiosAPIv1.post('/friends', {
                user_id2: this.user.id
            }).then(function (result) {
                return _this5.getUser();
            });
        },
        confirm: function confirm() {
            var _this6 = this;

            return axiosAPIv1.post('/friends/' + this.user.friend_id + '/confirm').then(function (result) {
                return _this6.getUser();
            });
        },
        cancel: function cancel() {
            return this.deleteRequest();
        },
        unfriend: function unfriend() {
            return this.deleteRequest();
        },
        block: function block() {
            var _this7 = this;

            return axiosAPIv1.post('/friends/block', {
                user_id2: this.user.id
            }).then(function (result) {
                return _this7.getUser();
            });
        },
        unblock: function unblock() {
            return this.deleteRequest();
        },
        deleteRequest: function deleteRequest() {
            var _this8 = this;

            return axiosAPIv1.delete('/friends/' + this.user.friend_id).then(function (result) {
                return _this8.getUser();
            });
        }
    },

    computed: {
        fullName: function fullName() {
            return this.user.first_name + ' ' + this.user.last_name;
        },
        isBlocked: function isBlocked() {
            return this.user.friend_status === 'Blocked';
        },
        showSettings: function showSettings() {
            if (this.user.friend_user_id == window.Laravel.user.id) {
                return true;
            } else if (this.user.friend_status === 'Blocked') {
                return false;
            }

            return true;
        }
    },

    components: {
        InfiniteLoading: __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default.a
    },

    created: function created() {
        this.getUser();
    },
    mounted: function mounted() {
        $.contextMenu({
            selector: '#chat-box-settings',
            trigger: 'left',

            callback: function (key, options) {
                this[key].apply(this, arguments);
            }.bind(this),

            build: function ($triggerElement, e) {
                var userId = this.user.friend_user_id,
                    status = this.user.friend_status,
                    items = {};

                if (status === 'Request' && userId != window.Laravel.user.id) {
                    items['confirm'] = { name: this.$parent.lang['accept'], icon: 'fa-check' };
                    items['deleteRequest'] = { name: this.$parent.lang['reject'], icon: 'fa-times' };
                } else if (status === 'Request') {
                    items['cancel'] = { name: this.$parent.lang['reject'], icon: 'fa-times' };
                } else if (status === 'Friend') {
                    items['unfriend'] = { name: this.$parent.lang['unfriend'], icon: 'fa-times' };
                } else {
                    items['request'] = { name: this.$parent.lang['add'], icon: 'fa-plus' };
                }

                if (status === 'Blocked') {
                    items = {
                        unblock: { name: this.$parent.lang['unblock'], icon: 'fa-check' }
                    };
                } else {
                    items['block'] = { name: this.$parent.lang['block-msg'], icon: 'fa-ban' };
                }

                return { items: items };
            }.bind(this)
        });
    }
});

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var settings = window.Laravel.user.settings ? window.Laravel.user.settings.settings : {};

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            chatRoom: null,
            messages: [],
            newMessage: null,
            newMessage2: null,
            currentPage: 0,
            enterEnabled: settings ? !!settings.message_enter_enabled : false,
            $chatBox: null,
            $conversation: null
        };
    },


    watch: {
        $route: function $route(to, from) {
            Event.unlisten( true ? this.chatRoom.id : null);

            this.chatRoom = null;
            this.messages = [];
            this.newMessage = null;
            this.currentPage = 0;
            this.$chatBox = null;
            this.$conversation = null;

            this.getChatRoom();
        }
    },

    methods: {
        onInfinite: function onInfinite() {
            this.getMessages();
            $('[data-toggle="tooltip"]').tooltip();
        },
        getChatRoom: function getChatRoom() {
            var _this = this;

            axiosAPIv1.get('/chat-rooms/' + this.$route.params.id).then(function (result) {
                _this.chatRoom = result.data;

                setTimeout(function () {
                    var $el = $(this.$el);

                    this.$chatBox = $el.find('.chat-box');
                    this.$conversation = $el.find('.panel-body');
                    this.$conversation.perfectScrollbar();
                }.bind(_this), 1);

                _this.markAllAsRead();

                _this.listen();

                Event.fire('chat-room.selected', result.data);

                _this.getMessages2();
            });
        },
        getMessages: function getMessages() {
            var _this2 = this;

            var prevScrollHeight = this.$conversation.prop('scrollHeight');
            return axiosAPIv1.get('/messages/chat-room/' + this.chatRoom.id, {
                params: {
                    page: ++this.currentPage,
                    limit: 999
                }
            }).then(function (result) {
                var data = result.data.data;

                if (data && data.length) {
                    data.sort(function (a, b) {
                        return a.id - b.id;
                    });

                    _this2.messages = data.concat(_this2.messages);
                    _this2.currentPage = result.data.current_page;

                    setTimeout(function () {
                        if (this.currentPage === 1) {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
                        } else {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight') - prevScrollHeight);
                        }

                        this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
                    }.bind(_this2), 1);
                } else {
                    _this2.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                }
            });
        },
        getMessages2: function getMessages2() {
            var _this3 = this;

            return axiosAPIv1.get('/messages/chat-room/' + this.chatRoom.id, {
                params: {
                    page: ++this.currentPage,
                    limit: 999
                }
            }).then(function (result) {
                var data = result.data.data;

                if (data && data.length) {
                    data.sort(function (a, b) {
                        return a.id - b.id;
                    });

                    _this3.messages = data.concat(_this3.messages);
                    _this3.currentPage = result.data.current_page;

                    setTimeout(function () {
                        if (this.currentPage === 1) {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
                        } else {
                            this.$conversation.scrollTop(this.$conversation.prop('scrollHeight') - prevScrollHeight);
                        }

                        this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
                    }.bind(_this3), 1);
                } else {
                    _this3.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
                }
            });
        },
        markAllAsRead: function markAllAsRead() {
            return axiosAPIv1.post('/messages/mark-all-as-read', {
                to_chat_room: this.chatRoom.id
            }).then(function (result) {
                Event.fire('chat-box.marked-as-read');
            });
        },
        send: function send(e) {
            if (e.keyCode === 13 && !this.enterEnabled || !this.newMessage) {
                return;
            }

            if (e.keyCode === 13 && this.enterEnabled) {
                e.preventDefault();
            }

            this.messages.push({
                user_id: window.Laravel.user.id,
                body: this.newMessage
            });

            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));

                this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
            }.bind(this), 1);

            axiosAPIv1.post('/messages', {
                to_chat_room: this.chatRoom.id,
                body: this.newMessage
            }).then(function (result) {
                return Event.fire('chat-box.send');
            });

            this.newMessage = null;
        },
        close: function close() {
            Event.fire('chat-box.close', this.chatRoom);
        },
        isMe: function isMe(message) {
            return window.Laravel.user.id == message.user_id;
        },
        scrollToBottom: function scrollToBottom() {
            setTimeout(function () {
                this.$conversation.scrollTop(this.$conversation.prop('scrollHeight'));
            }.bind(this), 1);
        },
        listen: function listen() {
            Event.listen('chat-room-' + this.chatRoom.id, function (message) {
                var _this4 = this;

                if (message.chat_room_id == this.chatRoom.id) {

                    axiosAPIv1.get('/users/' + message.user_id).then(function (result) {
                        _this4.newMessage2 = { "body": message.body,
                            "user": { "avatar": result.data.avatar,
                                "full_name": result.data.full_name,
                                "nick_name": result.data.nick_name,
                                "first_name": result.data.first_name } };
                        _this4.messages.push(_this4.newMessage2);
                        _this4.scrollToBottom();
                    });
                }
            }.bind(this));
        }
    },

    computed: {
        // avatar() {
        //     return this.message.chat_room.users[0]['avatar'];
        // },
        chatRoomName: function chatRoomName() {
            if (!this.chatRoom) {
                return '';
            }

            return this.chatRoom.name;
        }
    },

    components: {
        InfiniteLoading: __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default.a
    },

    created: function created() {
        this.getChatRoom();
    }
});

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['message'],

    computed: {
        isMe: function isMe() {
            return this.message.user_id == window.Laravel.user.id;
        }
    },

    components: {
        'bubble-not-me': __webpack_require__(357),
        'bubble-me': __webpack_require__(356)
    }
});

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['message'],

    computed: {
        avatar: function avatar() {
            return this.message.chat_room.users[0]['avatar'];
        },
        fullName: function fullName() {
            if (this.message.chat_room.name) {
                return this.message.chat_room.users[0].full_name + ' - ' + this.message.chat_room.name;
            }

            return this.message.chat_room.name || this.message.chat_room.users[0]['full_name'];
        },
        routeTo: function routeTo() {
            if (this.message.chat_room.name) {
                return {
                    name: 'chat-room',
                    params: {
                        id: this.message.chat_room.id
                    }
                };
            }

            return {
                name: 'user',
                params: {
                    id: this.message.chat_room.users[0].id
                }
            };
        }
    }
});

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['target', 'panelClass'],

    data: function data() {
        return {
            targetEl: null,
            search: '',
            users: [],
            usersContainer: null,
            timeout: null
        };
    },


    watch: {
        search: function search(val) {
            if (val != null && val.length === 0) {
                this.users = [];
            }
        },
        users: function users(val) {
            if (val.length) {
                setTimeout(function () {
                    this.usersContainer.perfectScrollbar('update');
                }.bind(this), 1);
            }
        }
    },

    methods: {
        searchUser: function searchUser(e) {
            if ((e.which <= 90 && e.which >= 48 || e.which === 8) && this.search && this.search.length >= 2) {
                clearTimeout(this.timeout);

                this.timeout = setTimeout(function () {
                    var _this = this;

                    axiosAPIv1.get('/messages/users?search=' + this.search).then(function (result) {
                        _this.users = result.data;
                    });
                }.bind(this), 200);
            }
        },
        select: function select(user) {
            Event.fire('user-search.selected', user);

            this._reset();
        },
        avatar: function avatar(user) {
            return user.avatar;
        },
        _positionEl: function _positionEl() {
            var offset = this.targetEl.offset(),
                teWidth = this.targetEl.outerWidth(),
                teHeight = this.targetEl.outerHeight(),
                wWidth = window.innerWidth,
                position = {
                top: offset.top + teHeight + 10,
                left: offset.left
            };

            var $el = $(this.$el),
                $elWidth = $el.outerWidth() || $el.innerWidth();

            if (offset.left + $elWidth > wWidth) {
                position.left = offset.left - $elWidth + teWidth;
            }

            $el.offset(position);
        },
        _reset: function _reset() {
            var $el = $(this.$el),
                $input = $el.find('input'),
                $event = $.Event('keyup', { which: 8 });

            this.search = null;
            this.users = [];

            $input.trigger($event);
            $input.blur();

            $el.hide();
        },
        _registerEventListeners: function _registerEventListeners() {
            var _this2 = this;

            $(window).resize(this._positionEl);

            Event.listen('user-search.show', function () {
                var $el = $(_this2.$el);

                $el.show();
                $el.find('input').focus();

                _this2._positionEl();
            });

            // Remove/Hide floating panels on outside click
            $(document).mouseup(function (e) {
                var $el = $(this.$el);

                // if the target of the click isn't the panel nor a descendant of the panel
                if (!$el.is(e.target) && $el.has(e.target).length === 0) {
                    this._reset();
                }
            }.bind(this));
        }
    },

    created: function created() {
        this._registerEventListeners();
    },
    mounted: function mounted() {
        this.targetEl = $(this.target);
        this.usersContainer = $('.users-container');
        this.usersContainer.perfectScrollbar();
    }
});

/***/ }),

/***/ 356:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(291),
  /* template */
  __webpack_require__(368),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\pages\\message\\BubbleMe.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] BubbleMe.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1bfcf094", Component.options)
  } else {
    hotAPI.reload("data-v-1bfcf094", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 357:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(292),
  /* template */
  __webpack_require__(385),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\pages\\message\\BubbleNotMe.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] BubbleNotMe.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5a154c02", Component.options)
  } else {
    hotAPI.reload("data-v-5a154c02", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(293),
  /* template */
  __webpack_require__(372),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\pages\\message\\ChatBox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ChatBox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-23e838b3", Component.options)
  } else {
    hotAPI.reload("data-v-23e838b3", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(294),
  /* template */
  __webpack_require__(375),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\pages\\message\\ChatBoxRoom.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ChatBoxRoom.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2e8fb2ae", Component.options)
  } else {
    hotAPI.reload("data-v-2e8fb2ae", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 360:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(295),
  /* template */
  __webpack_require__(363),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\pages\\message\\UserChatRoomMessage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] UserChatRoomMessage.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0e9ca349", Component.options)
  } else {
    hotAPI.reload("data-v-0e9ca349", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 361:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(296),
  /* template */
  __webpack_require__(402),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\pages\\message\\UserLatestMessage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] UserLatestMessage.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d23f3a56", Component.options)
  } else {
    hotAPI.reload("data-v-d23f3a56", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 362:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(297),
  /* template */
  __webpack_require__(398),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\pages\\message\\UserSearch.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] UserSearch.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b0099dba", Component.options)
  } else {
    hotAPI.reload("data-v-b0099dba", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 363:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('ul', {
    staticClass: "list-group"
  }, [(!_vm.isMe) ? _c('bubble-not-me', {
    attrs: {
      "message": _vm.message
    }
  }) : _vm._e(), _vm._v(" "), (_vm.isMe) ? _c('bubble-me', {
    attrs: {
      "message": _vm.message
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0e9ca349", module.exports)
  }
}

/***/ }),

/***/ 368:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', {
    staticClass: "list-group-item"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 user-chat me"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12"
  }, [_c('div', {
    staticClass: "user-message"
  }, [_vm._v(_vm._s(_vm.message.body))])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-1bfcf094", module.exports)
  }
}

/***/ }),

/***/ 372:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.user) ? _c('div', {
    staticClass: "panel panel-chat-box"
  }, [_c('div', {
    staticClass: "panel-heading"
  }, [_vm._v("\r\n        " + _vm._s(_vm.fullName) + "\r\n        "), (_vm.showSettings) ? _c('a', {
    staticClass: "pull-right",
    attrs: {
      "href": "javascript:void(0)",
      "id": "chat-box-settings"
    }
  }, [_c('i', {
    staticClass: "fa fa-cog"
  })]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "chat-box"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('infinite-loading', {
    ref: "infiniteLoading",
    attrs: {
      "on-infinite": _vm.onInfinite,
      "direction": "top",
      "distance": 0
    }
  }, [_c('span', {
    slot: "no-more"
  }), _vm._v(" "), _c('span', {
    slot: "no-results"
  })]), _vm._v(" "), _c('ul', _vm._l((_vm.messages), function(message) {
    return _c('li', {
      class: {
        me: _vm.isMe(message)
      }
    }, [_c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "col-xs-12"
    }, [_c('div', {
      staticClass: "message"
    }, [_vm._v(_vm._s(message.body))])])])])
  }))], 1), _vm._v(" "), _c('div', {
    staticClass: "panel-footer"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-11"
  }, [_c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newMessage),
      expression: "newMessage"
    }],
    attrs: {
      "id": "areamsg",
      "disabled": _vm.isBlocked,
      "placeholder": this.$parent.lang['write-something']
    },
    domProps: {
      "value": (_vm.newMessage)
    },
    on: {
      "keydown": function($event) {
        if (!('button' in $event) && _vm._k($event.keyCode, "enter", 13)) { return null; }
        _vm.send($event)
      },
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.newMessage = $event.target.value
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "enter-send"
  }, [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.enterEnabled),
      expression: "enterEnabled"
    }],
    attrs: {
      "type": "checkbox",
      "id": "enter_enabled"
    },
    domProps: {
      "checked": Array.isArray(_vm.enterEnabled) ? _vm._i(_vm.enterEnabled, null) > -1 : (_vm.enterEnabled)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.enterEnabled,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = null,
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.enterEnabled = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.enterEnabled = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.enterEnabled = $$c
        }
      }
    }
  }), _vm._v(" " + _vm._s(this.$parent.lang['enter-to-send']) + "\r\n                        ")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-1 btn-send-container"
  }, [_c('a', {
    staticClass: "btn-send",
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": _vm.send
    }
  }, [_c('i', {
    staticClass: "fa fa-send"
  })])])])])])]) : _vm._e()
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-23e838b3", module.exports)
  }
}

/***/ }),

/***/ 375:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.chatRoom) ? _c('div', {
    staticClass: "panel panel-chat-box"
  }, [_c('div', {
    staticClass: "panel-heading"
  }, [_vm._v("\r\n        " + _vm._s(_vm.chatRoomName) + "\r\n    ")]), _vm._v(" "), _c('div', {
    staticClass: "chat-box"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('infinite-loading', {
    ref: "infiniteLoading",
    attrs: {
      "on-infinite": _vm.onInfinite,
      "direction": "top",
      "distance": 0
    }
  }, [_c('span', {
    slot: "no-more"
  }), _vm._v(" "), _c('span', {
    slot: "no-results"
  })]), _vm._v(" "), _c('ul', _vm._l((_vm.messages), function(message, key) {
    return _c('li', {
      class: {
        me: _vm.isMe(message)
      }
    }, [_c('div', {
      staticClass: "row"
    }, [(!_vm.isMe(message)) ? _c('div', {
      staticClass: "col-xs-1 user-avatar",
      staticStyle: {
        "padding-right": "0px"
      }
    }, [_c('img', {
      staticClass: "img-circle userAvatar",
      attrs: {
        "src": message.user.avatar,
        "data-toggle": "tooltip",
        "data-placement": "right",
        "title": message.user.full_name
      }
    })]) : _vm._e(), _vm._v(" "), (!_vm.isMe(message)) ? _c('div', {
      staticClass: "col-xs-11",
      staticStyle: {
        "padding-left": "0px",
        "margin-left": "-10px"
      }
    }, [(message.user.nick_name !== null) ? _c('span', {
      staticClass: "username"
    }, [_vm._v(_vm._s(message.user.nick_name))]) : _c('span', {
      staticClass: "username"
    }, [_vm._v(_vm._s(message.user.first_name))]), _vm._v(" "), _c('br'), _vm._v(" "), _c('div', {
      staticClass: "message"
    }, [_vm._v(_vm._s(message.body))])]) : _c('div', {
      staticClass: "col-xs-12",
      staticStyle: {
        "padding-left": "0px"
      }
    }, [_c('div', {
      staticClass: "message"
    }, [_vm._v(_vm._s(message.body))])])])])
  }))], 1), _vm._v(" "), _c('div', {
    staticClass: "panel-footer"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-11"
  }, [(_vm.chatRoom.is_active) ? _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newMessage),
      expression: "newMessage"
    }],
    attrs: {
      "placeholder": "Write something..."
    },
    domProps: {
      "value": (_vm.newMessage)
    },
    on: {
      "keydown": function($event) {
        if (!('button' in $event) && _vm._k($event.keyCode, "enter", 13)) { return null; }
        if (!$event.ctrlKey) { return null; }
        $event.preventDefault();
        _vm.send($event)
      },
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.newMessage = $event.target.value
      }
    }
  }) : _c('textarea', {
    attrs: {
      "disabled": ""
    }
  }, [_vm._v("This issue is already closed")]), _vm._v(" "), _c('div', {
    staticClass: "enter-send"
  }, [_c('label', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.enterEnabled),
      expression: "enterEnabled"
    }],
    attrs: {
      "type": "checkbox",
      "id": "enter_enabled"
    },
    domProps: {
      "checked": Array.isArray(_vm.enterEnabled) ? _vm._i(_vm.enterEnabled, null) > -1 : (_vm.enterEnabled)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.enterEnabled,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = null,
            $$i = _vm._i($$a, $$v);
          if ($$el.checked) {
            $$i < 0 && (_vm.enterEnabled = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.enterEnabled = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.enterEnabled = $$c
        }
      }
    }
  }), _vm._v(" Press enter to send the message\r\n                        ")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-1 btn-send-container"
  }, [_c('a', {
    staticClass: "btn-send",
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": _vm.send
    }
  }, [_c('i', {
    staticClass: "fa fa-send"
  })])])])])])]) : _vm._e()
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-2e8fb2ae", module.exports)
  }
}

/***/ }),

/***/ 385:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', {
    staticClass: "list-group-item"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-1 user-avatar"
  }, [_c('img', {
    staticClass: "img-circle",
    attrs: {
      "src": _vm.avatar
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-11 user-chat"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12"
  }, [_c('div', {
    staticClass: "user-message"
  }, [_vm._v(_vm._s(_vm.message.body))])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5a154c02", module.exports)
  }
}

/***/ }),

/***/ 398:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "panel panel-default panel-float",
    class: _vm.panelClass
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "form-group label-placeholder"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "user-search"
    }
  }, [_vm._v(_vm._s(this.$parent.lang['search-user']) + "...")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.search),
      expression: "search"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "id": "user-search"
    },
    domProps: {
      "value": (_vm.search)
    },
    on: {
      "keyup": _vm.searchUser,
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.search = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('ul', {
    staticClass: "users-container"
  }, _vm._l((_vm.users), function(user) {
    return _c('li', {
      on: {
        "click": function($event) {
          _vm.select(user)
        }
      }
    }, [_c('img', {
      staticClass: "img-circle",
      attrs: {
        "src": _vm.avatar(user)
      }
    }), _vm._v(" "), _c('span', [_vm._v(_vm._s(user.full_name))])])
  }))])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-b0099dba", module.exports)
  }
}

/***/ }),

/***/ 402:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "list-group"
  }, [_c('router-link', {
    staticClass: "list-group-item",
    attrs: {
      "to": _vm.routeTo
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-3 user-avatar"
  }, [_c('img', {
    staticClass: "img-circle",
    attrs: {
      "src": _vm.avatar
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-9 user-detail"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 user-fullname"
  }, [_c('b', [_vm._v(_vm._s(_vm.fullName))])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 user-latest-message"
  }, [_vm._v(_vm._s(_vm.message.body))])])])])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-d23f3a56", module.exports)
  }
}

/***/ }),

/***/ 432:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(182);


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

!function(p,x){ true?module.exports=x():"function"==typeof define&&define.amd?define([],x):"object"==typeof exports?exports.VueInfiniteLoading=x():p.VueInfiniteLoading=x()}(this,function(){return function(p){function x(a){if(t[a])return t[a].exports;var e=t[a]={exports:{},id:a,loaded:!1};return p[a].call(e.exports,e,e.exports,x),e.loaded=!0,e.exports}var t={};return x.m=p,x.c=t,x.p="/",x(0)}([function(p,x,t){"use strict";function a(p){return p&&p.__esModule?p:{default:p}}Object.defineProperty(x,"__esModule",{value:!0});var e=t(4),o=a(e);x.default=o.default,"undefined"!=typeof window&&window.Vue&&window.Vue.component("infinite-loading",o.default)},function(p,x){"use strict";function t(p){return"BODY"===p.tagName?window:["scroll","auto"].indexOf(getComputedStyle(p).overflowY)>-1?p:p.hasAttribute("infinite-wrapper")||p.hasAttribute("data-infinite-wrapper")?p:t(p.parentNode)}function a(p,x,t){var a=void 0;if("top"===t)a=isNaN(p.scrollTop)?p.pageYOffset:p.scrollTop;else{var e=x.getBoundingClientRect().top,o=p===window?window.innerHeight:p.getBoundingClientRect().bottom;a=e-o}return a}Object.defineProperty(x,"__esModule",{value:!0});var e={bubbles:"loading-bubbles",circles:"loading-circles",default:"loading-default",spiral:"loading-spiral",waveDots:"loading-wave-dots"};x.default={data:function(){return{scrollParent:null,scrollHandler:null,isLoading:!1,isComplete:!1,isFirstLoad:!0}},computed:{spinnerType:function(){return e[this.spinner]||e.default}},props:{distance:{type:Number,default:100},onInfinite:Function,spinner:String,direction:{type:String,default:"bottom"}},mounted:function(){var p=this;this.scrollParent=t(this.$el),this.scrollHandler=function(){this.isLoading||this.attemptLoad()}.bind(this),setTimeout(this.scrollHandler,1),this.scrollParent.addEventListener("scroll",this.scrollHandler),this.$on("$InfiniteLoading:loaded",function(){p.isFirstLoad=!1,p.isLoading&&p.$nextTick(p.attemptLoad)}),this.$on("$InfiniteLoading:complete",function(){p.isLoading=!1,p.isComplete=!0,p.scrollParent.removeEventListener("scroll",p.scrollHandler)}),this.$on("$InfiniteLoading:reset",function(){p.isLoading=!1,p.isComplete=!1,p.isFirstLoad=!0,p.scrollParent.addEventListener("scroll",p.scrollHandler),setTimeout(p.scrollHandler,1)})},deactivated:function(){this.isLoading=!1,this.scrollParent.removeEventListener("scroll",this.scrollHandler)},activated:function(){this.scrollParent.addEventListener("scroll",this.scrollHandler)},methods:{attemptLoad:function(){var p=a(this.scrollParent,this.$el,this.direction);!this.isComplete&&p<=this.distance?(this.isLoading=!0,this.onInfinite.call()):this.isLoading=!1}},destroyed:function(){this.isComplete||this.scrollParent.removeEventListener("scroll",this.scrollHandler)}}},function(p,x,t){x=p.exports=t(3)(),x.push([p.id,'.loading-wave-dots[data-v-50793f02]{position:relative}.loading-wave-dots[data-v-50793f02]:before{content:"";position:absolute;top:50%;left:50%;margin-left:-4px;margin-top:-4px;width:8px;height:8px;background-color:#bbb;border-radius:50%;-webkit-animation:linear loading-wave-dots 2.8s infinite;animation:linear loading-wave-dots 2.8s infinite}@-webkit-keyframes loading-wave-dots{0%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}5%{box-shadow:-32px -4px 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}10%{box-shadow:-32px -6px 0 #999,-16px -4px 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}15%{box-shadow:-32px 2px 0 #bbb,-16px -2px 0 #999,16px 4px 0 #bbb,32px 4px 0 #bbb;-webkit-transform:translateY(-4px);transform:translateY(-4px);background-color:#bbb}20%{box-shadow:-32px 6px 0 #bbb,-16px 4px 0 #bbb,16px 2px 0 #bbb,32px 6px 0 #bbb;-webkit-transform:translateY(-6px);transform:translateY(-6px);background-color:#999}25%{box-shadow:-32px 2px 0 #bbb,-16px 2px 0 #bbb,16px -4px 0 #999,32px -2px 0 #bbb;-webkit-transform:translateY(-2px);transform:translateY(-2px);background-color:#bbb}30%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px -2px 0 #bbb,32px -6px 0 #999;-webkit-transform:translateY(0);transform:translateY(0)}35%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px -2px 0 #bbb}40%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}to{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}}@keyframes loading-wave-dots{0%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}5%{box-shadow:-32px -4px 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}10%{box-shadow:-32px -6px 0 #999,-16px -4px 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}15%{box-shadow:-32px 2px 0 #bbb,-16px -2px 0 #999,16px 4px 0 #bbb,32px 4px 0 #bbb;-webkit-transform:translateY(-4px);transform:translateY(-4px);background-color:#bbb}20%{box-shadow:-32px 6px 0 #bbb,-16px 4px 0 #bbb,16px 2px 0 #bbb,32px 6px 0 #bbb;-webkit-transform:translateY(-6px);transform:translateY(-6px);background-color:#999}25%{box-shadow:-32px 2px 0 #bbb,-16px 2px 0 #bbb,16px -4px 0 #999,32px -2px 0 #bbb;-webkit-transform:translateY(-2px);transform:translateY(-2px);background-color:#bbb}30%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px -2px 0 #bbb,32px -6px 0 #999;-webkit-transform:translateY(0);transform:translateY(0)}35%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px -2px 0 #bbb}40%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}to{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}}.loading-circles[data-v-50793f02]{position:relative}.loading-circles[data-v-50793f02]:before{content:"";position:absolute;left:50%;top:50%;margin-top:-2.5px;margin-left:-2.5px;width:5px;height:5px;border-radius:50%;-webkit-animation:linear loading-circles .75s infinite;animation:linear loading-circles .75s infinite}@-webkit-keyframes loading-circles{0%{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}12.5%{box-shadow:0 -12px 0 #dfdfdf,8.52px -8.52px 0 #505050,12px 0 0 #646464,8.52px 8.52px 0 #797979,0 12px 0 #8d8d8d,-8.52px 8.52px 0 #a2a2a2,-12px 0 0 #b6b6b6,-8.52px -8.52px 0 #cacaca}25%{box-shadow:0 -12px 0 #cacaca,8.52px -8.52px 0 #dfdfdf,12px 0 0 #505050,8.52px 8.52px 0 #646464,0 12px 0 #797979,-8.52px 8.52px 0 #8d8d8d,-12px 0 0 #a2a2a2,-8.52px -8.52px 0 #b6b6b6}37.5%{box-shadow:0 -12px 0 #b6b6b6,8.52px -8.52px 0 #cacaca,12px 0 0 #dfdfdf,8.52px 8.52px 0 #505050,0 12px 0 #646464,-8.52px 8.52px 0 #797979,-12px 0 0 #8d8d8d,-8.52px -8.52px 0 #a2a2a2}50%{box-shadow:0 -12px 0 #a2a2a2,8.52px -8.52px 0 #b6b6b6,12px 0 0 #cacaca,8.52px 8.52px 0 #dfdfdf,0 12px 0 #505050,-8.52px 8.52px 0 #646464,-12px 0 0 #797979,-8.52px -8.52px 0 #8d8d8d}62.5%{box-shadow:0 -12px 0 #8d8d8d,8.52px -8.52px 0 #a2a2a2,12px 0 0 #b6b6b6,8.52px 8.52px 0 #cacaca,0 12px 0 #dfdfdf,-8.52px 8.52px 0 #505050,-12px 0 0 #646464,-8.52px -8.52px 0 #797979}75%{box-shadow:0 -12px 0 #797979,8.52px -8.52px 0 #8d8d8d,12px 0 0 #a2a2a2,8.52px 8.52px 0 #b6b6b6,0 12px 0 #cacaca,-8.52px 8.52px 0 #dfdfdf,-12px 0 0 #505050,-8.52px -8.52px 0 #646464}87.5%{box-shadow:0 -12px 0 #646464,8.52px -8.52px 0 #797979,12px 0 0 #8d8d8d,8.52px 8.52px 0 #a2a2a2,0 12px 0 #b6b6b6,-8.52px 8.52px 0 #cacaca,-12px 0 0 #dfdfdf,-8.52px -8.52px 0 #505050}to{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}}@keyframes loading-circles{0%{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}12.5%{box-shadow:0 -12px 0 #dfdfdf,8.52px -8.52px 0 #505050,12px 0 0 #646464,8.52px 8.52px 0 #797979,0 12px 0 #8d8d8d,-8.52px 8.52px 0 #a2a2a2,-12px 0 0 #b6b6b6,-8.52px -8.52px 0 #cacaca}25%{box-shadow:0 -12px 0 #cacaca,8.52px -8.52px 0 #dfdfdf,12px 0 0 #505050,8.52px 8.52px 0 #646464,0 12px 0 #797979,-8.52px 8.52px 0 #8d8d8d,-12px 0 0 #a2a2a2,-8.52px -8.52px 0 #b6b6b6}37.5%{box-shadow:0 -12px 0 #b6b6b6,8.52px -8.52px 0 #cacaca,12px 0 0 #dfdfdf,8.52px 8.52px 0 #505050,0 12px 0 #646464,-8.52px 8.52px 0 #797979,-12px 0 0 #8d8d8d,-8.52px -8.52px 0 #a2a2a2}50%{box-shadow:0 -12px 0 #a2a2a2,8.52px -8.52px 0 #b6b6b6,12px 0 0 #cacaca,8.52px 8.52px 0 #dfdfdf,0 12px 0 #505050,-8.52px 8.52px 0 #646464,-12px 0 0 #797979,-8.52px -8.52px 0 #8d8d8d}62.5%{box-shadow:0 -12px 0 #8d8d8d,8.52px -8.52px 0 #a2a2a2,12px 0 0 #b6b6b6,8.52px 8.52px 0 #cacaca,0 12px 0 #dfdfdf,-8.52px 8.52px 0 #505050,-12px 0 0 #646464,-8.52px -8.52px 0 #797979}75%{box-shadow:0 -12px 0 #797979,8.52px -8.52px 0 #8d8d8d,12px 0 0 #a2a2a2,8.52px 8.52px 0 #b6b6b6,0 12px 0 #cacaca,-8.52px 8.52px 0 #dfdfdf,-12px 0 0 #505050,-8.52px -8.52px 0 #646464}87.5%{box-shadow:0 -12px 0 #646464,8.52px -8.52px 0 #797979,12px 0 0 #8d8d8d,8.52px 8.52px 0 #a2a2a2,0 12px 0 #b6b6b6,-8.52px 8.52px 0 #cacaca,-12px 0 0 #dfdfdf,-8.52px -8.52px 0 #505050}to{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}}.loading-bubbles[data-v-50793f02]{position:relative}.loading-bubbles[data-v-50793f02]:before{content:"";position:absolute;left:50%;top:50%;margin-top:-.5px;margin-left:-.5px;width:1px;height:1px;border-radius:50%;-webkit-animation:linear loading-bubbles .85s infinite;animation:linear loading-bubbles .85s infinite}@-webkit-keyframes loading-bubbles{0%{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}12.5%{box-shadow:0 -12px 0 3.2px #666,8.52px -8.52px 0 .4px #666,12px 0 0 .8px #666,8.52px 8.52px 0 1.2px #666,0 12px 0 1.6px #666,-8.52px 8.52px 0 2px #666,-12px 0 0 2.4px #666,-8.52px -8.52px 0 2.8px #666}25%{box-shadow:0 -12px 0 2.8px #666,8.52px -8.52px 0 3.2px #666,12px 0 0 .4px #666,8.52px 8.52px 0 .8px #666,0 12px 0 1.2px #666,-8.52px 8.52px 0 1.6px #666,-12px 0 0 2px #666,-8.52px -8.52px 0 2.4px #666}37.5%{box-shadow:0 -12px 0 2.4px #666,8.52px -8.52px 0 2.8px #666,12px 0 0 3.2px #666,8.52px 8.52px 0 .4px #666,0 12px 0 .8px #666,-8.52px 8.52px 0 1.2px #666,-12px 0 0 1.6px #666,-8.52px -8.52px 0 2px #666}50%{box-shadow:0 -12px 0 2px #666,8.52px -8.52px 0 2.4px #666,12px 0 0 2.8px #666,8.52px 8.52px 0 3.2px #666,0 12px 0 .4px #666,-8.52px 8.52px 0 .8px #666,-12px 0 0 1.2px #666,-8.52px -8.52px 0 1.6px #666}62.5%{box-shadow:0 -12px 0 1.6px #666,8.52px -8.52px 0 2px #666,12px 0 0 2.4px #666,8.52px 8.52px 0 2.8px #666,0 12px 0 3.2px #666,-8.52px 8.52px 0 .4px #666,-12px 0 0 .8px #666,-8.52px -8.52px 0 1.2px #666}75%{box-shadow:0 -12px 0 1.2px #666,8.52px -8.52px 0 1.6px #666,12px 0 0 2px #666,8.52px 8.52px 0 2.4px #666,0 12px 0 2.8px #666,-8.52px 8.52px 0 3.2px #666,-12px 0 0 .4px #666,-8.52px -8.52px 0 .8px #666}87.5%{box-shadow:0 -12px 0 .8px #666,8.52px -8.52px 0 1.2px #666,12px 0 0 1.6px #666,8.52px 8.52px 0 2px #666,0 12px 0 2.4px #666,-8.52px 8.52px 0 2.8px #666,-12px 0 0 3.2px #666,-8.52px -8.52px 0 .4px #666}to{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}}@keyframes loading-bubbles{0%{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}12.5%{box-shadow:0 -12px 0 3.2px #666,8.52px -8.52px 0 .4px #666,12px 0 0 .8px #666,8.52px 8.52px 0 1.2px #666,0 12px 0 1.6px #666,-8.52px 8.52px 0 2px #666,-12px 0 0 2.4px #666,-8.52px -8.52px 0 2.8px #666}25%{box-shadow:0 -12px 0 2.8px #666,8.52px -8.52px 0 3.2px #666,12px 0 0 .4px #666,8.52px 8.52px 0 .8px #666,0 12px 0 1.2px #666,-8.52px 8.52px 0 1.6px #666,-12px 0 0 2px #666,-8.52px -8.52px 0 2.4px #666}37.5%{box-shadow:0 -12px 0 2.4px #666,8.52px -8.52px 0 2.8px #666,12px 0 0 3.2px #666,8.52px 8.52px 0 .4px #666,0 12px 0 .8px #666,-8.52px 8.52px 0 1.2px #666,-12px 0 0 1.6px #666,-8.52px -8.52px 0 2px #666}50%{box-shadow:0 -12px 0 2px #666,8.52px -8.52px 0 2.4px #666,12px 0 0 2.8px #666,8.52px 8.52px 0 3.2px #666,0 12px 0 .4px #666,-8.52px 8.52px 0 .8px #666,-12px 0 0 1.2px #666,-8.52px -8.52px 0 1.6px #666}62.5%{box-shadow:0 -12px 0 1.6px #666,8.52px -8.52px 0 2px #666,12px 0 0 2.4px #666,8.52px 8.52px 0 2.8px #666,0 12px 0 3.2px #666,-8.52px 8.52px 0 .4px #666,-12px 0 0 .8px #666,-8.52px -8.52px 0 1.2px #666}75%{box-shadow:0 -12px 0 1.2px #666,8.52px -8.52px 0 1.6px #666,12px 0 0 2px #666,8.52px 8.52px 0 2.4px #666,0 12px 0 2.8px #666,-8.52px 8.52px 0 3.2px #666,-12px 0 0 .4px #666,-8.52px -8.52px 0 .8px #666}87.5%{box-shadow:0 -12px 0 .8px #666,8.52px -8.52px 0 1.2px #666,12px 0 0 1.6px #666,8.52px 8.52px 0 2px #666,0 12px 0 2.4px #666,-8.52px 8.52px 0 2.8px #666,-12px 0 0 3.2px #666,-8.52px -8.52px 0 .4px #666}to{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}}.loading-default[data-v-50793f02]{position:relative;border:1px solid #999;-webkit-animation:ease loading-rotating 1.5s infinite;animation:ease loading-rotating 1.5s infinite}.loading-default[data-v-50793f02]:before{content:"";position:absolute;display:block;top:0;left:50%;margin-top:-3px;margin-left:-3px;width:6px;height:6px;background-color:#999;border-radius:50%}.loading-spiral[data-v-50793f02]{border:2px solid #777;border-right-color:transparent;-webkit-animation:linear loading-rotating .85s infinite;animation:linear loading-rotating .85s infinite}@-webkit-keyframes loading-rotating{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes loading-rotating{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}.infinite-loading-container[data-v-50793f02]{clear:both;text-align:center}.infinite-loading-container [class^=loading-][data-v-50793f02]{display:inline-block;margin:15px 0;width:28px;height:28px;font-size:28px;line-height:28px;border-radius:50%}.infinite-status-prompt[data-v-50793f02]{color:#666;font-size:14px;text-align:center;padding:10px 0}',""])},function(p,x){p.exports=function(){var p=[];return p.toString=function(){for(var p=[],x=0;x<this.length;x++){var t=this[x];t[2]?p.push("@media "+t[2]+"{"+t[1]+"}"):p.push(t[1])}return p.join("")},p.i=function(x,t){"string"==typeof x&&(x=[[null,x,""]]);for(var a={},e=0;e<this.length;e++){var o=this[e][0];"number"==typeof o&&(a[o]=!0)}for(e=0;e<x.length;e++){var n=x[e];"number"==typeof n[0]&&a[n[0]]||(t&&!n[2]?n[2]=t:t&&(n[2]="("+n[2]+") and ("+t+")"),p.push(n))}},p}},function(p,x,t){var a,e;t(7),a=t(1);var o=t(5);e=a=a||{},"object"!=typeof a.default&&"function"!=typeof a.default||(e=a=a.default),"function"==typeof e&&(e=e.options),e.render=o.render,e.staticRenderFns=o.staticRenderFns,e._scopeId="data-v-50793f02",p.exports=a},function(p,x){p.exports={render:function(){var p=this,x=p.$createElement,t=p._self._c||x;return t("div",{staticClass:"infinite-loading-container"},[t("div",{directives:[{name:"show",rawName:"v-show",value:p.isLoading,expression:"isLoading"}]},[p._t("spinner",[t("i",{class:p.spinnerType})])],2),p._v(" "),t("div",{directives:[{name:"show",rawName:"v-show",value:!p.isLoading&&p.isComplete&&p.isFirstLoad,expression:"!isLoading && isComplete && isFirstLoad"}],staticClass:"infinite-status-prompt"},[p._t("no-results",[p._v("No results :(")])],2),p._v(" "),t("div",{directives:[{name:"show",rawName:"v-show",value:!p.isLoading&&p.isComplete&&!p.isFirstLoad,expression:"!isLoading && isComplete && !isFirstLoad"}],staticClass:"infinite-status-prompt"},[p._t("no-more",[p._v("No more data :)")])],2)])},staticRenderFns:[]}},function(p,x,t){function a(p,x){for(var t=0;t<p.length;t++){var a=p[t],e=d[a.id];if(e){e.refs++;for(var o=0;o<e.parts.length;o++)e.parts[o](a.parts[o]);for(;o<a.parts.length;o++)e.parts.push(r(a.parts[o],x))}else{for(var n=[],o=0;o<a.parts.length;o++)n.push(r(a.parts[o],x));d[a.id]={id:a.id,refs:1,parts:n}}}}function e(p){for(var x=[],t={},a=0;a<p.length;a++){var e=p[a],o=e[0],n=e[1],i=e[2],r=e[3],s={css:n,media:i,sourceMap:r};t[o]?t[o].parts.push(s):x.push(t[o]={id:o,parts:[s]})}return x}function o(p,x){var t=c(),a=m[m.length-1];if("top"===p.insertAt)a?a.nextSibling?t.insertBefore(x,a.nextSibling):t.appendChild(x):t.insertBefore(x,t.firstChild),m.push(x);else{if("bottom"!==p.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");t.appendChild(x)}}function n(p){p.parentNode.removeChild(p);var x=m.indexOf(p);x>=0&&m.splice(x,1)}function i(p){var x=document.createElement("style");return x.type="text/css",o(p,x),x}function r(p,x){var t,a,e;if(x.singleton){var o=h++;t=u||(u=i(x)),a=s.bind(null,t,o,!1),e=s.bind(null,t,o,!0)}else t=i(x),a=b.bind(null,t),e=function(){n(t)};return a(p),function(x){if(x){if(x.css===p.css&&x.media===p.media&&x.sourceMap===p.sourceMap)return;a(p=x)}else e()}}function s(p,x,t,a){var e=t?"":a.css;if(p.styleSheet)p.styleSheet.cssText=g(x,e);else{var o=document.createTextNode(e),n=p.childNodes;n[x]&&p.removeChild(n[x]),n.length?p.insertBefore(o,n[x]):p.appendChild(o)}}function b(p,x){var t=x.css,a=x.media,e=x.sourceMap;if(a&&p.setAttribute("media",a),e&&(t+="\n/*# sourceURL="+e.sources[0]+" */",t+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(e))))+" */"),p.styleSheet)p.styleSheet.cssText=t;else{for(;p.firstChild;)p.removeChild(p.firstChild);p.appendChild(document.createTextNode(t))}}var d={},l=function(p){var x;return function(){return"undefined"==typeof x&&(x=p.apply(this,arguments)),x}},f=l(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),c=l(function(){return document.head||document.getElementsByTagName("head")[0]}),u=null,h=0,m=[];p.exports=function(p,x){x=x||{},"undefined"==typeof x.singleton&&(x.singleton=f()),"undefined"==typeof x.insertAt&&(x.insertAt="bottom");var t=e(p);return a(t,x),function(p){for(var o=[],n=0;n<t.length;n++){var i=t[n],r=d[i.id];r.refs--,o.push(r)}if(p){var s=e(p);a(s,x)}for(var n=0;n<o.length;n++){var r=o[n];if(0===r.refs){for(var b=0;b<r.parts.length;b++)r.parts[b]();delete d[r.id]}}}};var g=function(){var p=[];return function(x,t){return p[x]=t,p.filter(Boolean).join("\n")}}()},function(p,x,t){var a=t(2);"string"==typeof a&&(a=[[p.id,a,""]]);t(6)(a,{});a.locals&&(p.exports=a.locals)}])});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vQnViYmxlTWUudnVlIiwid2VicGFjazovLy9CdWJibGVOb3RNZS52dWUiLCJ3ZWJwYWNrOi8vL0NoYXRCb3gudnVlIiwid2VicGFjazovLy9DaGF0Qm94Um9vbS52dWUiLCJ3ZWJwYWNrOi8vL1VzZXJDaGF0Um9vbU1lc3NhZ2UudnVlIiwid2VicGFjazovLy9Vc2VyTGF0ZXN0TWVzc2FnZS52dWUiLCJ3ZWJwYWNrOi8vL1VzZXJTZWFyY2gudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9CdWJibGVNZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL0J1YmJsZU5vdE1lLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL21lc3NhZ2UvQ2hhdEJveC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL0NoYXRCb3hSb29tLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL21lc3NhZ2UvVXNlckNoYXRSb29tTWVzc2FnZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL1VzZXJMYXRlc3RNZXNzYWdlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL21lc3NhZ2UvVXNlclNlYXJjaC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL1VzZXJDaGF0Um9vbU1lc3NhZ2UudnVlPzk4YzQiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL0J1YmJsZU1lLnZ1ZT8yZDZiIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9DaGF0Qm94LnZ1ZT84OTQ0Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9DaGF0Qm94Um9vbS52dWU/NWI0YyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL21lc3NhZ2UvQnViYmxlTm90TWUudnVlPzE2ODkiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL1VzZXJTZWFyY2gudnVlP2MzYTYiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL1VzZXJMYXRlc3RNZXNzYWdlLnZ1ZT83YTFjIiwid2VicGFjazovLy8uL34vdnVlLWluZmluaXRlLWxvYWRpbmcvZGlzdC92dWUtaW5maW5pdGUtbG9hZGluZy5qcyJdLCJuYW1lcyI6WyJWdWUiLCJlbCIsImRhdGEiLCJ1c2VyTGF0ZXN0TWVzc2FnZXMiLCJzZWxlY3RlZCIsImxhbmciLCJyb3V0ZXIiLCJWdWVSb3V0ZXIiLCJyb3V0ZXMiLCJwYXRoIiwibmFtZSIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJ3YXRjaCIsInZhbCIsInNldFRpbWVvdXQiLCIkIiwicGVyZmVjdFNjcm9sbGJhciIsIm1ldGhvZHMiLCJnZXRMYXRlc3RNZXNzYWdlcyIsImF4aW9zQVBJdjEiLCJnZXQiLCJ0aGVuIiwicmVzdWx0IiwibG9hZFNlbGVjdGVkVXNlciIsIiRyb3V0ZXIiLCJwdXNoIiwicGFyYW1zIiwiaWQiLCJsb2FkU2VsZWN0ZWRDaGF0Um9vbSIsIm5ld01lc3NhZ2UiLCJFdmVudCIsImZpcmUiLCJjb21wb25lbnRzIiwiY3JlYXRlZCIsImxpc3RlbiIsImF4aW9zIiwibGFuZ3VhZ2UiLCJ0b2FzdHIiLCJpbmZvIiwidGltZU91dCIsImV4dGVuZGVkVGltZU91dCIsImhpZGUiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQSxJQUFJQSxHQUFKLENBQVE7QUFDSkMsUUFBSSxxQkFEQTs7QUFHSkMsVUFBTTtBQUNGQyw0QkFBb0IsRUFEbEI7QUFFRkMsa0JBQVUsSUFGUjs7QUFJRkMsY0FBTTtBQUpKLEtBSEY7O0FBVUpDLFlBQVEsSUFBSUMsU0FBSixDQUFjO0FBQ2xCQyxnQkFBUSxDQUNKO0FBQ0lDLGtCQUFNLFFBRFY7QUFFSUMsa0JBQU0sTUFGVjtBQUdJQyx1QkFBVyxtQkFBQUMsQ0FBUSxHQUFSO0FBSGYsU0FESSxFQU1KO0FBQ0lILGtCQUFNLFNBRFY7QUFFSUMsa0JBQU0sV0FGVjtBQUdJQyx1QkFBVyxtQkFBQUMsQ0FBUSxHQUFSO0FBSGYsU0FOSTtBQURVLEtBQWQsQ0FWSjs7QUF5QkpDLFdBQU87QUFDSFYsMEJBREcsOEJBQ2dCVyxHQURoQixFQUNxQjtBQUNwQkMsdUJBQVcsWUFBTTtBQUNiQyxrQkFBRSxzQkFBRixFQUEwQkMsZ0JBQTFCLENBQTJDLFFBQTNDO0FBQ0gsYUFGRCxFQUVHLENBRkg7QUFHSDtBQUxFLEtBekJIOztBQWlDSkMsYUFBUztBQUNMQyx5QkFESywrQkFDZTtBQUFBOztBQUNoQkMsdUJBQVdDLEdBQVgsQ0FBZSxXQUFmLEVBQ0tDLElBREwsQ0FDVSxrQkFBVTtBQUNaLHNCQUFLbkIsa0JBQUwsR0FBMEJvQixPQUFPckIsSUFBakM7QUFDSCxhQUhMO0FBSUgsU0FOSTtBQVFMc0Isd0JBUkssNEJBUVl0QixJQVJaLEVBUWtCO0FBQ25CLGlCQUFLRSxRQUFMLEdBQWdCRixJQUFoQjs7QUFFQSxpQkFBS3VCLE9BQUwsQ0FBYUMsSUFBYixDQUFrQjtBQUNkaEIsc0JBQU0sTUFEUTtBQUVkaUIsd0JBQVE7QUFDSkMsd0JBQUkxQixLQUFLMEI7QUFETDtBQUZNLGFBQWxCO0FBTUgsU0FqQkk7QUFtQkxDLDRCQW5CSyxnQ0FtQmdCM0IsSUFuQmhCLEVBbUJzQjtBQUN2QixpQkFBS0UsUUFBTCxHQUFnQkYsSUFBaEI7O0FBRUEsaUJBQUt1QixPQUFMLENBQWFDLElBQWIsQ0FBa0I7QUFDZGhCLHNCQUFNLFdBRFE7QUFFZGlCLHdCQUFRO0FBQ0pDLHdCQUFJMUIsS0FBSzBCO0FBREw7QUFGTSxhQUFsQjtBQU1ILFNBNUJJO0FBOEJMRSxrQkE5Qkssd0JBOEJRO0FBQ1RDLGtCQUFNQyxJQUFOLENBQVcsa0JBQVg7QUFDSDtBQWhDSSxLQWpDTDs7QUFvRUpDLGdCQUFZO0FBQ1IsK0JBQXVCLG1CQUFBckIsQ0FBUSxHQUFSLENBRGY7QUFFUixrQ0FBMEIsbUJBQUFBLENBQVEsR0FBUixDQUZsQjtBQUdSLHVCQUFlLG1CQUFBQSxDQUFRLEdBQVI7QUFIUCxLQXBFUjs7QUEwRUpzQixXQTFFSSxxQkEwRU07QUFBQTs7QUFDTixhQUFLZixpQkFBTDs7QUFFQVksY0FBTUksTUFBTixDQUFhLHNCQUFiLEVBQXFDLEtBQUtYLGdCQUExQztBQUNBTyxjQUFNSSxNQUFOLENBQWEsb0JBQWIsRUFBbUMsS0FBS04sb0JBQXhDO0FBQ0FFLGNBQU1JLE1BQU4sQ0FBYSxlQUFiLEVBQThCLEtBQUtoQixpQkFBbkM7QUFDQVksY0FBTUksTUFBTixDQUFhLHlCQUFiLEVBQXdDLEtBQUtoQixpQkFBN0M7QUFDQVksY0FBTUksTUFBTixDQUFhLGtCQUFiLEVBQWlDLEtBQUtoQixpQkFBdEM7O0FBRUFpQixjQUFNZixHQUFOLENBQVUscUJBQVYsRUFDS0MsSUFETCxDQUNVLG9CQUFZO0FBQ2QsbUJBQUtqQixJQUFMLEdBQVlnQyxTQUFTbkMsSUFBVCxDQUFjQSxJQUExQjtBQUNBb0MsbUJBQU9DLElBQVAsQ0FBWSxPQUFLbEMsSUFBTCxDQUFVLFdBQVYsQ0FBWixFQUFvQyxPQUFLQSxJQUFMLENBQVUsVUFBVixDQUFwQyxFQUEyRDtBQUN6RG1DLHlCQUFTLENBRGdEO0FBRXpEQyxpQ0FBaUI7QUFGd0MsYUFBM0Q7QUFJUCxTQVBEOztBQVNBMUIsbUJBQVcsWUFBWTtBQUN2QkMsY0FBRSxrQkFBRixFQUFzQjBCLElBQXRCO0FBQ0MsU0FGRCxFQUVFLElBRkY7QUFHSDtBQS9GRyxDQUFSLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2VBO1lBRUE7QUFEQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNFQTtZQUdBOzs7a0NBRUE7cUNBQ0E7QUFFQTtBQUpBO0FBSEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMkJBOztBQUVBOztBQUVBOzBCQUVBOztrQkFFQTtzQkFDQTt3QkFDQTt5QkFDQTt3QkFDQTt3RUFDQTtzQkFDQTsyQkFFQTtBQVRBO0FBV0E7Ozs7MENBRUE7K0NBRUE7O3dCQUNBOzRCQUNBOzhCQUNBOytCQUNBOzhCQUNBOzRCQUNBO2lDQUVBOztpQkFDQTtBQUVBO2lEQUNBOzt5Q0FHQTtBQUZBO0FBS0E7QUFyQkE7OzswQ0F1QkE7aUJBQ0E7QUFFQTs7QUFDQTs7c0VBQ0EsZ0RBQ0E7b0NBRUE7O3VDQUNBO3FDQUVBOzs2Q0FDQTtrREFDQTt1Q0FDQTsrQkFFQTs7c0JBRUE7OzBEQUVBOztzQkFFQTtBQUNBO0FBRUE7O0FBQ0E7OzJEQUVBOzs7O2lDQUdBOzJCQUVBO0FBSEE7QUFEQSxzQ0FLQTt1Q0FFQTs7eUNBQ0E7OENBQ0E7d0NBQ0E7QUFFQTs7eURBQ0E7cURBRUE7OzRDQUNBO29EQUNBOytCQUNBO0FBRUE7OzJDQUNBO29EQUNBO2lGQUNBOytCQUNBO21HQUNBO0FBRUE7O3lEQUNBO29DQUNBO3VCQUNBO3VEQUNBO0FBQ0E7QUFDQTtBQUVBOztBQUVBOzs7O2lDQUdBOzJCQUVBO0FBSEE7QUFEQSxzQ0FLQTt1Q0FFQTs7eUNBQ0E7OENBQ0E7d0NBQ0E7QUFFQTs7eURBQ0E7cURBRUE7OzRDQUNBO29EQUNBOytCQUNBO0FBRUE7OzJDQUNBO29EQUNBO2lGQUNBOytCQUNBO21HQUNBO0FBRUE7O3lEQUNBO29DQUNBO3VCQUNBO3VEQUNBO0FBQ0E7QUFDQTtBQUVBO2dEQUNBOzttQ0FFQTtBQURBLHNDQUVBOzJCQUNBO0FBQ0E7QUFFQTs7QUFDQTs7OEZBQ0E7QUFDQTtBQUVBOztBQUNBOytDQUNBO2tDQUNBO0FBQ0E7QUFFQTs7dURBQ0E7a0JBQ0E7QUFFQTs7OzZDQUVBOzJCQUdBO0FBSkE7O21DQUtBO3FFQUVBOztpREFDQTswQkFFQTs7O21DQUVBOzJCQUNBO0FBRkEsc0NBR0E7d0NBQ0E7eURBQ0E7MkJBQ0E7QUFFQTs7MkJBQ0E7QUFFQTs7OEJBQ0E7QUFFQTtnQ0FDQTs4Q0FDQTtBQUVBO3FDQUNBO3FEQUNBO0FBRUE7a0RBQ0E7bUNBQ0E7cUVBQ0E7MEJBQ0E7QUFFQTtrQ0FDQTs0RUFDQTs2REFDQTt1Q0FDQTt5QkFDQTtBQUNBO21CQUNBO0FBQ0E7QUFDQTtBQUVBOztBQUNBOzs7b0NBRUE7QUFEQTs4QkFFQTs7QUFFQTs7QUFDQTs7dUVBQ0E7OEJBQ0E7O0FBRUE7a0NBQ0E7d0JBQ0E7QUFFQTtzQ0FDQTt3QkFDQTtBQUVBOztBQUNBOzs7b0NBRUE7QUFEQTs4QkFFQTs7QUFFQTtvQ0FDQTt3QkFDQTtBQUVBOztBQUNBOzs2REFDQTs4QkFDQTs7QUFHQTtBQXBOQTs7O3NDQXNOQTswREFDQTtBQUVBO3dDQUNBOytDQUNBO0FBRUE7OENBQ0E7b0VBQ0E7dUJBQ0E7OERBQ0E7dUJBQ0E7QUFFQTs7bUJBQ0E7QUFHQTtBQW5CQTs7O0FBdUJBO0FBSEE7O2dDQUlBO2FBQ0E7QUFFQTtnQ0FDQTs7c0JBRUE7cUJBRUE7OzhDQUNBO3NDQUNBO21CQUVBOztpREFDQTt1Q0FDQTt1Q0FDQTs0QkFFQTs7OEVBQ0E7a0ZBQ0E7d0ZBQ0E7aURBQ0E7aUZBQ0E7Z0RBQ0E7cUZBQ0E7dUJBQ0E7K0VBQ0E7QUFFQTs7MENBQ0E7OzZFQUdBO0FBRkE7dUJBR0E7bUZBQ0E7QUFFQTs7eUJBQ0E7bUJBRUE7QUFsQ0E7QUFtQ0E7QUF6VEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSUE7O0FBRUE7O0FBRUE7MEJBRUE7O3NCQUVBO3NCQUNBO3dCQUNBO3lCQUNBO3lCQUNBO3dFQUNBO3NCQUNBOzJCQUVBO0FBVEE7QUFXQTs7OzswQ0FFQTtzREFFQTs7NEJBQ0E7NEJBQ0E7OEJBQ0E7K0JBQ0E7NEJBQ0E7aUNBRUE7O2lCQUNBO0FBR0E7QUFkQTs7OzBDQWdCQTtpQkFDQTt5Q0FFQTtBQUVBOztBQUNBOzsrREFDQSwyQkFDQTt3Q0FFQTs7dUNBQ0E7cUNBRUE7OzZDQUNBO2tEQUNBO3VDQUNBOytCQUVBOztzQkFFQTs7c0JBRUE7O3dEQUVBOztzQkFDQTtBQUNBO0FBRUE7O0FBQ0E7OzJEQUNBOzs7aUNBR0E7MkJBRUE7QUFIQTtBQURBLHNDQUtBO3VDQUVBOzt5Q0FDQTs4Q0FDQTt3Q0FDQTtBQUVBOzt5REFDQTtxREFFQTs7MkNBQ0E7b0RBQ0E7aUZBQ0E7K0JBQ0E7bUdBQ0E7QUFFQTs7eURBQ0E7b0NBQ0E7dUJBQ0E7dURBQ0E7QUFDQTtBQUNBO0FBRUE7O0FBRUE7Ozs7aUNBR0E7MkJBRUE7QUFIQTtBQURBLHNDQUtBO3VDQUVBOzt5Q0FDQTs4Q0FDQTt3Q0FDQTtBQUVBOzt5REFDQTtxREFFQTs7MkNBQ0E7b0RBQ0E7aUZBQ0E7K0JBQ0E7bUdBQ0E7QUFFQTs7eURBQ0E7b0NBQ0E7dUJBQ0E7dURBQ0E7QUFDQTtBQUNBO0FBRUE7Z0RBQ0E7OzRDQUVBO0FBREEsc0NBRUE7MkJBQ0E7QUFDQTtBQUVBOytCQUNBOzRFQUNBO0FBQ0E7QUFFQTs7dURBQ0E7a0JBQ0E7QUFFQTs7OzZDQUVBOzJCQUdBO0FBSkE7O21DQUtBO3FFQUVBOztpREFDQTswQkFFQTs7OzRDQUVBOzJCQUNBO0FBRkE7a0NBSUE7Ozs4QkFDQTtBQUVBO2dDQUNBOzhDQUNBO0FBRUE7cUNBQ0E7cURBQ0E7QUFFQTtrREFDQTttQ0FDQTtxRUFDQTswQkFDQTtBQUVBO2tDQUNBOztBQUNBOzs4REFFQTs7dUZBQ0E7K0RBQ0E7NERBQ0E7eURBQ0E7eURBQ0E7MERBQ0E7b0RBQ0E7K0JBQ0E7QUFFQTtBQUNBO21CQUNBO0FBR0E7QUFwS0E7OztBQXNLQTtBQUNBO0FBQ0E7OENBQ0E7Z0NBQ0E7dUJBQ0E7QUFFQTs7aUNBQ0E7QUFHQTtBQVpBOzs7QUFnQkE7QUFIQTs7Z0NBSUE7YUFDQTtBQUNBO0FBck5BLEc7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcERBO1lBR0E7Ozs4QkFFQTsrREFDQTtBQUdBO0FBTEE7Ozs2Q0FPQTt5Q0FFQTtBQUhBO0FBVEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWUE7WUFHQTs7O2tDQUVBO21EQUNBO0FBRUE7c0NBQ0E7NkNBQ0E7a0dBQ0E7QUFFQTs7a0ZBQ0E7QUFFQTtvQ0FDQTs2Q0FDQTs7MEJBRUE7O21EQUlBO0FBSEE7QUFGQTtBQU9BOzs7c0JBRUE7O3dEQUlBO0FBSEE7QUFGQTtBQU9BO0FBOUJBO0FBSEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSkE7c0JBR0E7OzBCQUNBOztzQkFFQTtvQkFDQTttQkFDQTs0QkFDQTtxQkFFQTtBQU5BO0FBUUE7Ozs7cUNBRUE7aURBQ0E7NkJBQ0E7QUFDQTtBQUVBO21DQUNBOzRCQUNBO3VDQUNBO3lEQUNBOzhCQUNBO0FBQ0E7QUFHQTtBQWZBOzs7MkNBaUJBOzZHQUNBO2tDQUVBOzs7QUFDQTs7b0VBQ0EsK0JBQ0E7NkNBQ0E7QUFDQTs4QkFDQTtBQUNBO0FBRUE7c0NBQ0E7K0NBRUE7O2lCQUNBO0FBRUE7c0NBQ0E7d0JBQ0E7QUFFQTs0Q0FDQTt1Q0FDQTt3Q0FDQTt5Q0FDQTtnQ0FDQTs7NkNBRUE7NkJBR0E7QUFKQTs7NkJBS0E7bURBRUE7O2lEQUNBO3lEQUNBO0FBRUE7O3VCQUNBO0FBRUE7a0NBQ0E7NkJBQ0E7a0NBQ0E7bURBRUE7OzBCQUNBO3lCQUVBOzsyQkFDQTttQkFFQTs7Z0JBQ0E7QUFFQTs7QUFDQTs7a0NBRUE7O3lEQUNBO21DQUVBOztvQkFDQTtrQ0FFQTs7dUJBQ0E7QUFFQTs7QUFDQTs2Q0FDQTtpQ0FFQTs7QUFDQTt5RUFDQTt5QkFDQTtBQUNBO21CQUNBO0FBR0E7QUFqRkE7O2dDQWtGQTthQUNBO0FBRUE7Z0NBQ0E7K0JBQ0E7Z0NBQ0E7NEJBQ0E7QUFDQTtBQXZIQSxHOzs7Ozs7O0FDbkJBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDbkJBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3JCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsMkVBQTJFLGFBQWE7QUFDeEY7QUFDQSxPQUFPO0FBQ1A7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUM3SEEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsMkVBQTJFLGFBQWE7QUFDeEYsOEJBQThCLGFBQWE7QUFDM0M7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUNySkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDNUJBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQzFEQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7Ozs7OztBQ25DQSxlQUFlLDZKQUF5TSxpQkFBaUIsbUJBQW1CLGNBQWMsNEJBQTRCLFlBQVksVUFBVSxpQkFBaUIsZ0VBQWdFLFNBQVMsZ0NBQWdDLGtCQUFrQixhQUFhLGNBQWMsMEJBQTBCLFdBQVcsc0NBQXNDLFNBQVMsRUFBRSxrQkFBa0IsK0dBQStHLGVBQWUsYUFBYSxjQUFjLDRMQUE0TCxrQkFBa0IsYUFBYSw0REFBNEQsS0FBSyxxR0FBcUcsTUFBTSxTQUFTLHNDQUFzQyxTQUFTLEVBQUUsT0FBTyxvSUFBb0ksV0FBVyxnQkFBZ0IsT0FBTyxnRkFBZ0YsV0FBVyx1QkFBdUIsbUNBQW1DLFFBQVEsVUFBVSx3QkFBd0IsK0NBQStDLDhCQUE4QixvQkFBb0IsV0FBVyw0REFBNEQsbUNBQW1DLDJKQUEySix5REFBeUQsa0RBQWtELDRGQUE0RiwrQ0FBK0Msd0lBQXdJLEVBQUUsd0JBQXdCLHFGQUFxRixzQkFBc0IsZ0VBQWdFLFVBQVUsdUJBQXVCLG1EQUFtRCxpR0FBaUcsc0JBQXNCLHNGQUFzRixpQkFBaUIscUVBQXFFLGtCQUFrQiwyQ0FBMkMsV0FBVyxrQkFBa0IsUUFBUSxTQUFTLGlCQUFpQixnQkFBZ0IsVUFBVSxXQUFXLHNCQUFzQixrQkFBa0IseURBQXlELGlEQUFpRCxxQ0FBcUMsR0FBRyxxRUFBcUUsR0FBRyx3RUFBd0UsZ0NBQWdDLHdCQUF3QixJQUFJLDJFQUEyRSxnQ0FBZ0Msd0JBQXdCLElBQUksOEVBQThFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksNkVBQTZFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksK0VBQStFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksMkVBQTJFLGdDQUFnQyx3QkFBd0IsSUFBSSx3RUFBd0UsSUFBSSxxRUFBcUUsR0FBRyxzRUFBc0UsNkJBQTZCLEdBQUcscUVBQXFFLEdBQUcsd0VBQXdFLGdDQUFnQyx3QkFBd0IsSUFBSSwyRUFBMkUsZ0NBQWdDLHdCQUF3QixJQUFJLDhFQUE4RSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLDZFQUE2RSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLCtFQUErRSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLDJFQUEyRSxnQ0FBZ0Msd0JBQXdCLElBQUksd0VBQXdFLElBQUkscUVBQXFFLEdBQUcsc0VBQXNFLGtDQUFrQyxrQkFBa0IseUNBQXlDLFdBQVcsa0JBQWtCLFNBQVMsUUFBUSxrQkFBa0IsbUJBQW1CLFVBQVUsV0FBVyxrQkFBa0IsdURBQXVELCtDQUErQyxtQ0FBbUMsR0FBRyxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsR0FBRyxzTEFBc0wsMkJBQTJCLEdBQUcscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLEdBQUcsc0xBQXNMLGtDQUFrQyxrQkFBa0IseUNBQXlDLFdBQVcsa0JBQWtCLFNBQVMsUUFBUSxpQkFBaUIsa0JBQWtCLFVBQVUsV0FBVyxrQkFBa0IsdURBQXVELCtDQUErQyxtQ0FBbUMsR0FBRyx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sR0FBRywwTUFBME0sMkJBQTJCLEdBQUcseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLEdBQUcsME1BQTBNLGtDQUFrQyxrQkFBa0Isc0JBQXNCLHNEQUFzRCw4Q0FBOEMseUNBQXlDLFdBQVcsa0JBQWtCLGNBQWMsTUFBTSxTQUFTLGdCQUFnQixpQkFBaUIsVUFBVSxXQUFXLHNCQUFzQixrQkFBa0IsaUNBQWlDLHNCQUFzQiwrQkFBK0Isd0RBQXdELGdEQUFnRCxvQ0FBb0MsR0FBRyw0QkFBNEIsb0JBQW9CLEdBQUcsZ0NBQWdDLHlCQUF5Qiw0QkFBNEIsR0FBRyw0QkFBNEIsb0JBQW9CLEdBQUcsZ0NBQWdDLHlCQUF5Qiw2Q0FBNkMsV0FBVyxrQkFBa0IsK0RBQStELHFCQUFxQixjQUFjLFdBQVcsWUFBWSxlQUFlLGlCQUFpQixrQkFBa0IseUNBQXlDLFdBQVcsZUFBZSxrQkFBa0IsZUFBZSxPQUFPLGVBQWUscUJBQXFCLFNBQVMsNkJBQTZCLGlCQUFpQixjQUFjLEtBQUssY0FBYyw2QkFBNkIsU0FBUyxnQkFBZ0Isa0JBQWtCLG1CQUFtQixzQ0FBc0MsWUFBWSxLQUFLLGNBQWMsS0FBSyxpQkFBaUIsOEJBQThCLFFBQVEsV0FBVyxLQUFLLFdBQVcsZ0dBQWdHLElBQUksaUJBQWlCLFFBQVEsWUFBWSxXQUFXLFNBQVMsOE1BQThNLGVBQWUsV0FBVyxrQkFBa0IsOENBQThDLGdCQUFnQix5Q0FBeUMsV0FBVyxhQUFhLHNFQUFzRSxFQUFFLHlCQUF5QixvQkFBb0IsMkJBQTJCLGFBQWEsa0lBQWtJLHVDQUF1QyxvRUFBb0UsYUFBYSxvSUFBb0ksdUNBQXVDLGtEQUFrRCxxQkFBcUIsaUJBQWlCLGdCQUFnQixZQUFZLFdBQVcsS0FBSyxxQkFBcUIsTUFBTSxTQUFTLFlBQVksaUJBQWlCLDJCQUEyQixLQUFLLGlCQUFpQixrQ0FBa0MsS0FBSyxpQkFBaUIsaUJBQWlCLDRCQUE0QixTQUFTLDBCQUEwQixjQUFjLGlCQUFpQixLQUFLLFdBQVcsS0FBSywwQ0FBMEMsMkJBQTJCLHFDQUFxQyxlQUFlLEVBQUUsU0FBUyxnQkFBZ0IsMEJBQTBCLGdJQUFnSSxLQUFLLCtHQUErRyxrQkFBa0IsY0FBYyw0QkFBNEIsbUJBQW1CLG9CQUFvQixjQUFjLHNDQUFzQyxrQ0FBa0MsZ0JBQWdCLFVBQVUsZ0JBQWdCLFVBQVUsMERBQTBELDBDQUEwQyxNQUFNLHdCQUF3QixNQUFNLHNFQUFzRSxPQUFPLFVBQVUsb0JBQW9CLGlCQUFpQiw0Q0FBNEMsS0FBSyxnREFBZ0QsNEVBQTRFLGdCQUFnQixvQ0FBb0MsOEhBQThILDBHQUEwRyxLQUFLLEtBQUssYUFBYSw2QkFBNkIsMkNBQTJDLFFBQVEsZUFBZSxNQUFNLGtCQUFrQiw0REFBNEQsZ0JBQWdCLG9FQUFvRSxpQkFBaUIsK0RBQStELGtCQUFrQix3QkFBd0IsT0FBTywwR0FBMEcsV0FBVywwQkFBMEIsaUJBQWlCLFdBQVcsS0FBSyxxQkFBcUIsbUJBQW1CLE1BQU0sV0FBVyxPQUFPLFlBQVksV0FBVyxLQUFLLFdBQVcsZUFBZSxZQUFZLGlCQUFpQixpQkFBaUIsbUJBQW1CLGlCQUFpQixTQUFTLHFCQUFxQiw0Q0FBNEMsR0FBRyxpQkFBaUIsV0FBVyxzQ0FBc0MsU0FBUyxFQUFFLCtCQUErQixHQUFHLEUiLCJmaWxlIjoianMvcGFnZXMvbWVzc2FnZS9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDMyKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmNWZjZmYwZTFmMDgwMDk3Y2Y0NiIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDE4IDE5IDIwIDIxIDIyIDIzIDI0IDI1IDI2IDI3IiwibmV3IFZ1ZSh7XHJcbiAgICBlbDogJy5tZXNzYWdlcy1jb250YWluZXInLFxyXG5cclxuICAgIGRhdGE6IHtcclxuICAgICAgICB1c2VyTGF0ZXN0TWVzc2FnZXM6IFtdLFxyXG4gICAgICAgIHNlbGVjdGVkOiBudWxsLFxyXG5cclxuICAgICAgICBsYW5nOiBbXVxyXG4gICAgfSxcclxuXHJcbiAgICByb3V0ZXI6IG5ldyBWdWVSb3V0ZXIoe1xyXG4gICAgICAgIHJvdXRlczogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBwYXRoOiAnL3UvOmlkJyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICd1c2VyJyxcclxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogcmVxdWlyZSgnLi9DaGF0Qm94LnZ1ZScpXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHBhdGg6ICcvY3IvOmlkJyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdjaGF0LXJvb20nLFxyXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL0NoYXRCb3hSb29tLnZ1ZScpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9KSxcclxuXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgIHVzZXJMYXRlc3RNZXNzYWdlcyh2YWwpIHtcclxuICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkKCcudXNlci1saXN0LWNvbnRhaW5lcicpLnBlcmZlY3RTY3JvbGxiYXIoJ3VwZGF0ZScpO1xyXG4gICAgICAgICAgICB9LCAxKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXRMYXRlc3RNZXNzYWdlcygpIHtcclxuICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9tZXNzYWdlcycpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXNlckxhdGVzdE1lc3NhZ2VzID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBsb2FkU2VsZWN0ZWRVc2VyKGRhdGEpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZCA9IGRhdGE7XHJcblxyXG4gICAgICAgICAgICB0aGlzLiRyb3V0ZXIucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAndXNlcicsXHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogZGF0YS5pZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBsb2FkU2VsZWN0ZWRDaGF0Um9vbShkYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWQgPSBkYXRhO1xyXG5cclxuICAgICAgICAgICAgdGhpcy4kcm91dGVyLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogJ2NoYXQtcm9vbScsXHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogZGF0YS5pZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBuZXdNZXNzYWdlKCkge1xyXG4gICAgICAgICAgICBFdmVudC5maXJlKCd1c2VyLXNlYXJjaC5zaG93Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgJ3VzZXItbGF0ZXN0LW1lc3NhZ2UnOiByZXF1aXJlKCcuL1VzZXJMYXRlc3RNZXNzYWdlLnZ1ZScpLFxyXG4gICAgICAgICd1c2VyLWNoYXQtcm9vbS1tZXNzYWdlJzogcmVxdWlyZSgnLi9Vc2VyQ2hhdFJvb21NZXNzYWdlLnZ1ZScpLFxyXG4gICAgICAgICd1c2VyLXNlYXJjaCc6IHJlcXVpcmUoJy4vVXNlclNlYXJjaC52dWUnKVxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0TGF0ZXN0TWVzc2FnZXMoKTtcclxuXHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCd1c2VyLXNlYXJjaC5zZWxlY3RlZCcsIHRoaXMubG9hZFNlbGVjdGVkVXNlcik7XHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCdjaGF0LXJvb20uc2VsZWN0ZWQnLCB0aGlzLmxvYWRTZWxlY3RlZENoYXRSb29tKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ2NoYXQtYm94LnNlbmQnLCB0aGlzLmdldExhdGVzdE1lc3NhZ2VzKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ2NoYXQtYm94Lm1hcmtlZC1hcy1yZWFkJywgdGhpcy5nZXRMYXRlc3RNZXNzYWdlcyk7XHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCduZXctY2hhdC1tZXNzYWdlJywgdGhpcy5nZXRMYXRlc3RNZXNzYWdlcyk7XHJcblxyXG4gICAgICAgIGF4aW9zLmdldCgnL3RyYW5zbGF0ZS9tZXNzYWdlcycpXHJcbiAgICAgICAgICAgIC50aGVuKGxhbmd1YWdlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubGFuZyA9IGxhbmd1YWdlLmRhdGEuZGF0YTtcclxuICAgICAgICAgICAgICAgIHRvYXN0ci5pbmZvKHRoaXMubGFuZ1snYWxlcnQtbXNnJ10sIHRoaXMubGFuZ1sncmVtaW5kZXInXSwge1xyXG4gICAgICAgICAgICAgICAgICB0aW1lT3V0OiAwLFxyXG4gICAgICAgICAgICAgICAgICBleHRlbmRlZFRpbWVPdXQ6IDBcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKCcjdG9hc3QtY29udGFpbmVyJykuaGlkZSgpO1xyXG4gICAgICAgIH0sNTAwMCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIG1vdW50ZWQoKSB7XHJcbiAgICAvLyAgICAgJCgnLnVzZXItbGlzdC1jb250YWluZXInKS5wZXJmZWN0U2Nyb2xsYmFyKCk7XHJcbiAgICAvLyAgICAgJCgnLmNvbnZlcnNhdGlvbicpLnBlcmZlY3RTY3JvbGxiYXIoKTtcclxuICAgIC8vIH1cclxufSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL2luZGV4LmpzIiwiPHRlbXBsYXRlPlxyXG48bGkgY2xhc3M9XCJsaXN0LWdyb3VwLWl0ZW1cIj5cclxuICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyIHVzZXItY2hhdCBtZVwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInVzZXItbWVzc2FnZVwiPnt7IG1lc3NhZ2UuYm9keSB9fTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbjwvbGk+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczogWydtZXNzYWdlJ11cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIEJ1YmJsZU1lLnZ1ZT8zZWY1MTIxZCIsIjx0ZW1wbGF0ZT5cclxuPGxpIGNsYXNzPVwibGlzdC1ncm91cC1pdGVtXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xIHVzZXItYXZhdGFyXCI+XHJcbiAgICAgICAgICAgIDxpbWcgOnNyYz1cImF2YXRhclwiIGNsYXNzPVwiaW1nLWNpcmNsZVwiPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMTEgdXNlci1jaGF0XCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMTJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidXNlci1tZXNzYWdlXCI+e3sgbWVzc2FnZS5ib2R5IH19PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuPC9saT5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICAgIHByb3BzOiBbJ21lc3NhZ2UnXSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGF2YXRhcigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubWVzc2FnZS51c2VyLmF2YXRhcjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuPC9zY3JpcHQ+XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBCdWJibGVOb3RNZS52dWU/MzNiMGFlNTgiLCI8dGVtcGxhdGU+XHJcbjxkaXYgdi1pZj1cInVzZXJcIiBjbGFzcz1cInBhbmVsIHBhbmVsLWNoYXQtYm94XCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwicGFuZWwtaGVhZGluZ1wiPlxyXG4gICAgICAgIHt7IGZ1bGxOYW1lIH19XHJcbiAgICAgICAgPGEgdi1pZj1cInNob3dTZXR0aW5nc1wiIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBpZD1cImNoYXQtYm94LXNldHRpbmdzXCIgY2xhc3M9XCJwdWxsLXJpZ2h0XCI+XHJcbiAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtY29nXCI+PC9pPlxyXG4gICAgICAgIDwvYT5cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBjbGFzcz1cImNoYXQtYm94XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cclxuICAgICAgICAgICAgPGluZmluaXRlLWxvYWRpbmcgOm9uLWluZmluaXRlPVwib25JbmZpbml0ZVwiIHJlZj1cImluZmluaXRlTG9hZGluZ1wiIGRpcmVjdGlvbj1cInRvcFwiIDpkaXN0YW5jZT1cIjBcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIHNsb3Q9XCJuby1tb3JlXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gc2xvdD1cIm5vLXJlc3VsdHNcIj48L3NwYW4+XHJcbiAgICAgICAgICAgIDwvaW5maW5pdGUtbG9hZGluZz5cclxuICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgPGxpIHYtZm9yPVwibWVzc2FnZSBpbiBtZXNzYWdlc1wiIDpjbGFzcz1cInsgbWU6IGlzTWUobWVzc2FnZSkgfVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1lc3NhZ2VcIj57eyBtZXNzYWdlLmJvZHkgfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJwYW5lbC1mb290ZXJcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSB2LW1vZGVsPVwibmV3TWVzc2FnZVwiIEBrZXlkb3duLmVudGVyPVwic2VuZFwiIGlkPVwiYXJlYW1zZ1wiIDpkaXNhYmxlZD1cImlzQmxvY2tlZFwiIDpwbGFjZWhvbGRlcj1cInRoaXMuJHBhcmVudC5sYW5nWyd3cml0ZS1zb21ldGhpbmcnXVwiPjwvdGV4dGFyZWE+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImVudGVyLXNlbmRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGlkPVwiZW50ZXJfZW5hYmxlZFwiIHYtbW9kZWw9XCJlbnRlckVuYWJsZWRcIj4ge3t0aGlzLiRwYXJlbnQubGFuZ1snZW50ZXItdG8tc2VuZCddfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xIGJ0bi1zZW5kLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxhIEBjbGljaz1cInNlbmRcIiBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJidG4tc2VuZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLXNlbmRcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbjwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuaW1wb3J0IEluZmluaXRlTG9hZGluZyBmcm9tICd2dWUtaW5maW5pdGUtbG9hZGluZydcclxuXHJcbmxldCBzZXR0aW5ncyA9IHdpbmRvdy5MYXJhdmVsLnVzZXIuc2V0dGluZ3MgPyB3aW5kb3cuTGFyYXZlbC51c2VyLnNldHRpbmdzLnNldHRpbmdzIDoge307XHJcblxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHVzZXI6IG51bGwsXHJcbiAgICAgICAgICAgIG1lc3NhZ2VzOiBbXSxcclxuICAgICAgICAgICAgbmV3TWVzc2FnZTogbnVsbCxcclxuICAgICAgICAgICAgY3VycmVudFBhZ2U6IDAsXHJcbiAgICAgICAgICAgIGNoYXRSb29tSWQ6IG51bGwsXHJcbiAgICAgICAgICAgIGVudGVyRW5hYmxlZDogc2V0dGluZ3MgPyAhISBzZXR0aW5ncy5tZXNzYWdlX2VudGVyX2VuYWJsZWQgOiBmYWxzZSxcclxuICAgICAgICAgICAgJGNoYXRCb3g6IG51bGwsXHJcbiAgICAgICAgICAgICRjb252ZXJzYXRpb246IG51bGwsXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgICRyb3V0ZSh0bywgZnJvbSkge1xyXG4gICAgICAgICAgICBFdmVudC51bmxpc3RlbignY2hhdC1yb29tLScgKyB0aGlzLmNoYXRSb29tSWQpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy51c2VyID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlcyA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLm5ld01lc3NhZ2UgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRQYWdlID0gMDtcclxuICAgICAgICAgICAgdGhpcy5jaGF0Um9vbUlkID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy4kY2hhdEJveCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbiA9IG51bGw7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmdldFVzZXIoKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBlbnRlckVuYWJsZWQodmFsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSS5wb3N0KCcvdXNlci9zZXR0aW5ncycsIHtcclxuICAgICAgICAgICAgICAgICdtZXNzYWdlX2VudGVyX2VuYWJsZWQnOiB2YWxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgb25JbmZpbml0ZSgpIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRNZXNzYWdlcygpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdldFVzZXIoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL3VzZXJzLycgKyB0aGlzLiRyb3V0ZS5wYXJhbXMuaWQgKyAnP2luY2x1ZGU9cmVsYXRpb25zaGlwJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyID0gcmVzdWx0LmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgJGVsID0gJCh0aGlzLiRlbCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjaGF0Qm94ID0gJGVsLmZpbmQoJy5jaGF0LWJveCcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24gPSAkZWwuZmluZCgnLnBhbmVsLWJvZHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uLnBlcmZlY3RTY3JvbGxiYXIoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1hcmtBbGxBc1JlYWQoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgRXZlbnQuZmlyZSgndXNlci1zZWFyY2guc2VsZWN0ZWQnLCByZXN1bHQuZGF0YSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0TWVzc2FnZXMyKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0TWVzc2FnZXMoKSB7XHJcbiAgICAgICAgICAgIHZhciBwcmV2U2Nyb2xsSGVpZ2h0ID0gdGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvbWVzc2FnZXMvdXNlci8nICsgdGhpcy51c2VyLmlkLCB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBwYWdlOiArK3RoaXMuY3VycmVudFBhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgbGltaXQ6IDk5OVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IHJlc3VsdC5kYXRhLmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRhLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGEuaWQgLSBiLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1lc3NhZ2VzID0gZGF0YS5jb25jYXQodGhpcy5tZXNzYWdlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZSA9IHJlc3VsdC5kYXRhLmN1cnJlbnRfcGFnZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEgdGhpcy5jaGF0Um9vbUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhdFJvb21JZCA9IGRhdGFbMF0uY2hhdF9yb29tX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RlbigpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRQYWdlID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSAtIHByZXZTY3JvbGxIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpsb2FkZWQnKTtcclxuICAgICAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBnZXRNZXNzYWdlczIoKSB7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9tZXNzYWdlcy91c2VyLycgKyB0aGlzLnVzZXIuaWQsIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgIHBhZ2U6ICsrdGhpcy5jdXJyZW50UGFnZSxcclxuICAgICAgICAgICAgICAgICAgICBsaW1pdDogOTk5XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIHZhciBkYXRhID0gcmVzdWx0LmRhdGEuZGF0YTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEuc29ydCgoYSwgYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYS5pZCAtIGIuaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWVzc2FnZXMgPSBkYXRhLmNvbmNhdCh0aGlzLm1lc3NhZ2VzKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRQYWdlID0gcmVzdWx0LmRhdGEuY3VycmVudF9wYWdlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoISB0aGlzLmNoYXRSb29tSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGF0Um9vbUlkID0gZGF0YVswXS5jaGF0X3Jvb21faWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdGVuKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudFBhZ2UgPT09IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbi5zY3JvbGxUb3AodGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbi5zY3JvbGxUb3AodGhpcy4kY29udmVyc2F0aW9uLnByb3AoJ3Njcm9sbEhlaWdodCcpIC0gcHJldlNjcm9sbEhlaWdodCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHJlZnMuaW5maW5pdGVMb2FkaW5nLiRlbWl0KCckSW5maW5pdGVMb2FkaW5nOmxvYWRlZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgMSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJHJlZnMuaW5maW5pdGVMb2FkaW5nLiRlbWl0KCckSW5maW5pdGVMb2FkaW5nOmNvbXBsZXRlJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG1hcmtBbGxBc1JlYWQoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLnBvc3QoJy9tZXNzYWdlcy9tYXJrLWFsbC1hcy1yZWFkJywge1xyXG4gICAgICAgICAgICAgICAgdG9fdXNlcjogdGhpcy51c2VyLmlkXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIEV2ZW50LmZpcmUoJ2NoYXQtYm94Lm1hcmtlZC1hcy1yZWFkJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHNlbmQoZSkge1xyXG4gICAgICAgICAgICBpZiAoKChlLmtleUNvZGUgPT09IDEzKSAmJiAhIHRoaXMuZW50ZXJFbmFibGVkKSB8fCAoISB0aGlzLm5ld01lc3NhZ2UgfHwgdGhpcy5pc0Jsb2NrZWQpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vaWYgdXNlciBzZW5kcyBzcGFjZXMgb25seVxyXG4gICAgICAgICAgICBpZigkLnRyaW0odGhpcy5uZXdNZXNzYWdlKT09Jycpe1xyXG4gICAgICAgICAgICAgICAgJCgnI2FyZWFtc2cnKS52YWwoJycpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoKGUua2V5Q29kZSA9PT0gMTMpICYmIHRoaXMuZW50ZXJFbmFibGVkKSB7XHJcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICB1c2VyX2lkOiB3aW5kb3cuTGFyYXZlbC51c2VyLmlkLFxyXG4gICAgICAgICAgICAgICAgYm9keTogdGhpcy5uZXdNZXNzYWdlXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy4kcmVmcy5pbmZpbml0ZUxvYWRpbmcuJGVtaXQoJyRJbmZpbml0ZUxvYWRpbmc6bG9hZGVkJyk7XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgMSk7XHJcblxyXG4gICAgICAgICAgICBheGlvc0FQSXYxLnBvc3QoJy9tZXNzYWdlcycsIHtcclxuICAgICAgICAgICAgICAgIHRvX3VzZXI6IHRoaXMudXNlci5pZCxcclxuICAgICAgICAgICAgICAgIGJvZHk6IHRoaXMubmV3TWVzc2FnZVxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoISB0aGlzLmNoYXRSb29tSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNoYXRSb29tSWQgPSByZXN1bHQuZGF0YS5kYXRhLmNoYXRfcm9vbV9pZDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RlbigpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIEV2ZW50LmZpcmUoJ2NoYXQtYm94LnNlbmQnKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm5ld01lc3NhZ2UgPSBudWxsO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNsb3NlKCkge1xyXG4gICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LWJveC5jbG9zZScsIHRoaXMudXNlcik7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaXNNZShtZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB3aW5kb3cuTGFyYXZlbC51c2VyLmlkID09IG1lc3NhZ2UudXNlcl9pZDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzY3JvbGxUb0JvdHRvbSgpIHtcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSk7XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgMSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgbGlzdGVuKCkge1xyXG4gICAgICAgICAgICBFdmVudC5saXN0ZW4oJ2NoYXQtcm9vbS0nICsgdGhpcy5jaGF0Um9vbUlkLCBmdW5jdGlvbiAobWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKG1lc3NhZ2UuY2hhdF9yb29tX2lkID09IHRoaXMuY2hhdFJvb21JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWVzc2FnZXMucHVzaChtZXNzYWdlKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbFRvQm90dG9tKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSk7XHJcbiAgICAgICAgICAgIC8vIHZhciBhdWRpbyA9IG5ldyBBdWRpbygnL3NvdW5kL3d5Y25vdGlmLm1wMycpO1xyXG4gICAgICAgICAgICAvLyBhdWRpby5wbGF5KCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgcmVxdWVzdCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEucG9zdCgnL2ZyaWVuZHMnLCB7XHJcbiAgICAgICAgICAgICAgICB1c2VyX2lkMjogdGhpcy51c2VyLmlkLFxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB0aGlzLmdldFVzZXIoKSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY29uZmlybSgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEucG9zdCgnL2ZyaWVuZHMvJyArIHRoaXMudXNlci5mcmllbmRfaWQgKyAnL2NvbmZpcm0nKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHRoaXMuZ2V0VXNlcigpKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjYW5jZWwoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRlbGV0ZVJlcXVlc3QoKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB1bmZyaWVuZCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGVsZXRlUmVxdWVzdCgpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGJsb2NrKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvZnJpZW5kcy9ibG9jaycsIHtcclxuICAgICAgICAgICAgICAgIHVzZXJfaWQyOiB0aGlzLnVzZXIuaWQsXHJcbiAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHRoaXMuZ2V0VXNlcigpKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB1bmJsb2NrKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kZWxldGVSZXF1ZXN0KCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZGVsZXRlUmVxdWVzdCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZGVsZXRlKCcvZnJpZW5kcy8nICsgdGhpcy51c2VyLmZyaWVuZF9pZClcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB0aGlzLmdldFVzZXIoKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGZ1bGxOYW1lKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy51c2VyLmZpcnN0X25hbWUgKyAnICcgKyB0aGlzLnVzZXIubGFzdF9uYW1lO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGlzQmxvY2tlZCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMudXNlci5mcmllbmRfc3RhdHVzID09PSAnQmxvY2tlZCc7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2hvd1NldHRpbmdzKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy51c2VyLmZyaWVuZF91c2VyX2lkID09IHdpbmRvdy5MYXJhdmVsLnVzZXIuaWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudXNlci5mcmllbmRfc3RhdHVzID09PSAnQmxvY2tlZCcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgSW5maW5pdGVMb2FkaW5nXHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5nZXRVc2VyKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgJC5jb250ZXh0TWVudSh7XHJcbiAgICAgICAgICAgIHNlbGVjdG9yOiAnI2NoYXQtYm94LXNldHRpbmdzJyxcclxuICAgICAgICAgICAgdHJpZ2dlcjogJ2xlZnQnLFxyXG5cclxuICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uIChrZXksIG9wdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXNba2V5XS5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xyXG4gICAgICAgICAgICB9LmJpbmQodGhpcyksXHJcblxyXG4gICAgICAgICAgICBidWlsZDogZnVuY3Rpb24gKCR0cmlnZ2VyRWxlbWVudCwgZSkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHVzZXJJZCA9IHRoaXMudXNlci5mcmllbmRfdXNlcl9pZCxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXMgPSB0aGlzLnVzZXIuZnJpZW5kX3N0YXR1cyxcclxuICAgICAgICAgICAgICAgICAgICBpdGVtcyA9IHt9O1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICgoc3RhdHVzID09PSAnUmVxdWVzdCcpICYmICh1c2VySWQgIT0gd2luZG93LkxhcmF2ZWwudXNlci5pZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBpdGVtc1snY29uZmlybSddID0geyBuYW1lOiB0aGlzLiRwYXJlbnQubGFuZ1snYWNjZXB0J10sIGljb246ICdmYS1jaGVjaycgfTtcclxuICAgICAgICAgICAgICAgICAgICBpdGVtc1snZGVsZXRlUmVxdWVzdCddID0geyBuYW1lOiB0aGlzLiRwYXJlbnQubGFuZ1sncmVqZWN0J10sIGljb246ICdmYS10aW1lcycgfTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzID09PSAnUmVxdWVzdCcpIHtcclxuICAgICAgICAgICAgICAgICAgICBpdGVtc1snY2FuY2VsJ10gPSB7IG5hbWU6IHRoaXMuJHBhcmVudC5sYW5nWydyZWplY3QnXSwgaWNvbjogJ2ZhLXRpbWVzJyB9O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGF0dXMgPT09ICdGcmllbmQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbXNbJ3VuZnJpZW5kJ10gPSB7IG5hbWU6IHRoaXMuJHBhcmVudC5sYW5nWyd1bmZyaWVuZCddLCBpY29uOiAnZmEtdGltZXMnIH07XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zWydyZXF1ZXN0J10gPSB7IG5hbWU6IHRoaXMuJHBhcmVudC5sYW5nWydhZGQnXSwgaWNvbjogJ2ZhLXBsdXMnIH07XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHN0YXR1cyA9PT0gJ0Jsb2NrZWQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbXMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVuYmxvY2s6IHsgbmFtZTogdGhpcy4kcGFyZW50LmxhbmdbJ3VuYmxvY2snXSwgaWNvbjogJ2ZhLWNoZWNrJyB9XHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbXNbJ2Jsb2NrJ10gPSB7IG5hbWU6IHRoaXMuJHBhcmVudC5sYW5nWydibG9jay1tc2cnXSwgaWNvbjogJ2ZhLWJhbicgfTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4geyBpdGVtcyB9O1xyXG4gICAgICAgICAgICB9LmJpbmQodGhpcylcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQ2hhdEJveC52dWU/MTFiYzE2MTYiLCI8IS0tIGNoYXQgY29udm8gZm9yIGlzc3VlIHJlbGF0ZWQgLS0+XHJcblxyXG48dGVtcGxhdGU+XHJcbjxkaXYgdi1pZj1cImNoYXRSb29tXCIgY2xhc3M9XCJwYW5lbCBwYW5lbC1jaGF0LWJveFwiPlxyXG4gICAgPGRpdiBjbGFzcz1cInBhbmVsLWhlYWRpbmdcIj5cclxuICAgICAgICB7eyBjaGF0Um9vbU5hbWUgfX1cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBjbGFzcz1cImNoYXQtYm94XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cclxuICAgICAgICAgICAgPGluZmluaXRlLWxvYWRpbmcgOm9uLWluZmluaXRlPVwib25JbmZpbml0ZVwiIHJlZj1cImluZmluaXRlTG9hZGluZ1wiIGRpcmVjdGlvbj1cInRvcFwiIDpkaXN0YW5jZT1cIjBcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIHNsb3Q9XCJuby1tb3JlXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gc2xvdD1cIm5vLXJlc3VsdHNcIj48L3NwYW4+XHJcbiAgICAgICAgICAgIDwvaW5maW5pdGUtbG9hZGluZz5cclxuICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgPGxpIHYtZm9yPVwibWVzc2FnZSxrZXkgaW4gbWVzc2FnZXNcIiA6Y2xhc3M9XCJ7IG1lOiBpc01lKG1lc3NhZ2UpIH1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCIgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEgdXNlci1hdmF0YXJcIiB2LWlmPVwiIWlzTWUobWVzc2FnZSlcIiBzdHlsZT1cInBhZGRpbmctcmlnaHQ6MHB4O1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyA6c3JjPVwibWVzc2FnZS51c2VyLmF2YXRhclwiIGNsYXNzPVwiaW1nLWNpcmNsZSB1c2VyQXZhdGFyXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJyaWdodFwiIDp0aXRsZT1cIm1lc3NhZ2UudXNlci5mdWxsX25hbWVcIiA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTExXCIgdi1pZj1cIiFpc01lKG1lc3NhZ2UpXCIgc3R5bGU9XCJwYWRkaW5nLWxlZnQ6MHB4O21hcmdpbi1sZWZ0Oi0xMHB4O1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ1c2VybmFtZVwiIHYtaWY9XCJtZXNzYWdlLnVzZXIubmlja19uYW1lIT09bnVsbFwiPnt7IG1lc3NhZ2UudXNlci5uaWNrX25hbWUgfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cInVzZXJuYW1lXCIgdi1lbHNlPnt7IG1lc3NhZ2UudXNlci5maXJzdF9uYW1lIH19PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1lc3NhZ2VcIj57eyBtZXNzYWdlLmJvZHkgfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMTJcIiB2LWVsc2Ugc3R5bGU9XCJwYWRkaW5nLWxlZnQ6MHB4O1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1lc3NhZ2VcIj57eyBtZXNzYWdlLmJvZHkgfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJwYW5lbC1mb290ZXJcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSB2LWlmPVwiY2hhdFJvb20uaXNfYWN0aXZlXCIgdi1tb2RlbD1cIm5ld01lc3NhZ2VcIiBAa2V5ZG93bi5jdHJsLmVudGVyLnByZXZlbnQ9XCJzZW5kXCIgcGxhY2Vob2xkZXI9XCJXcml0ZSBzb21ldGhpbmcuLi5cIj48L3RleHRhcmVhPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSB2LWVsc2UgZGlzYWJsZWQ+VGhpcyBpc3N1ZSBpcyBhbHJlYWR5IGNsb3NlZDwvdGV4dGFyZWE+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImVudGVyLXNlbmRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGlkPVwiZW50ZXJfZW5hYmxlZFwiIHYtbW9kZWw9XCJlbnRlckVuYWJsZWRcIj4gUHJlc3MgZW50ZXIgdG8gc2VuZCB0aGUgbWVzc2FnZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEgYnRuLXNlbmQtY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGEgQGNsaWNrPVwic2VuZFwiIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cImJ0bi1zZW5kXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtc2VuZFwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5pbXBvcnQgSW5maW5pdGVMb2FkaW5nIGZyb20gJ3Z1ZS1pbmZpbml0ZS1sb2FkaW5nJ1xyXG5cclxubGV0IHNldHRpbmdzID0gd2luZG93LkxhcmF2ZWwudXNlci5zZXR0aW5ncyA/IHdpbmRvdy5MYXJhdmVsLnVzZXIuc2V0dGluZ3Muc2V0dGluZ3MgOiB7fTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgY2hhdFJvb206IG51bGwsXHJcbiAgICAgICAgICAgIG1lc3NhZ2VzOiBbXSxcclxuICAgICAgICAgICAgbmV3TWVzc2FnZTogbnVsbCxcclxuICAgICAgICAgICAgbmV3TWVzc2FnZTI6IG51bGwsXHJcbiAgICAgICAgICAgIGN1cnJlbnRQYWdlOiAwLFxyXG4gICAgICAgICAgICBlbnRlckVuYWJsZWQ6IHNldHRpbmdzID8gISEgc2V0dGluZ3MubWVzc2FnZV9lbnRlcl9lbmFibGVkIDogZmFsc2UsXHJcbiAgICAgICAgICAgICRjaGF0Qm94OiBudWxsLFxyXG4gICAgICAgICAgICAkY29udmVyc2F0aW9uOiBudWxsLFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgd2F0Y2g6IHtcclxuICAgICAgICAkcm91dGUodG8sIGZyb20pIHtcclxuICAgICAgICAgICAgRXZlbnQudW5saXN0ZW4oJ2NoYXQtcm9vbS0nICsgdGhpcy5jaGF0Um9vbSA/IHRoaXMuY2hhdFJvb20uaWQgOiBudWxsKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuY2hhdFJvb20gPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VzID0gW107XHJcbiAgICAgICAgICAgIHRoaXMubmV3TWVzc2FnZSA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFBhZ2UgPSAwO1xyXG4gICAgICAgICAgICB0aGlzLiRjaGF0Qm94ID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uID0gbnVsbDtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Q2hhdFJvb20oKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBvbkluZmluaXRlKCkge1xyXG4gICAgICAgICAgICB0aGlzLmdldE1lc3NhZ2VzKCk7XHJcbiAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCk7XHJcblxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdldENoYXRSb29tKCkge1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2NoYXQtcm9vbXMvJyArIHRoaXMuJHJvdXRlLnBhcmFtcy5pZClcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGF0Um9vbSA9IHJlc3VsdC5kYXRhO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRlbCA9ICQodGhpcy4kZWwpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY2hhdEJveCA9ICRlbC5maW5kKCcuY2hhdC1ib3gnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uID0gJGVsLmZpbmQoJy5wYW5lbC1ib2R5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnZlcnNhdGlvbi5wZXJmZWN0U2Nyb2xsYmFyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXJrQWxsQXNSZWFkKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdGVuKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIEV2ZW50LmZpcmUoJ2NoYXQtcm9vbS5zZWxlY3RlZCcsIHJlc3VsdC5kYXRhKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRNZXNzYWdlczIoKTsgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBnZXRNZXNzYWdlcygpIHtcclxuICAgICAgICAgICAgdmFyIHByZXZTY3JvbGxIZWlnaHQgPSB0aGlzLiRjb252ZXJzYXRpb24ucHJvcCgnc2Nyb2xsSGVpZ2h0Jyk7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL21lc3NhZ2VzL2NoYXQtcm9vbS8nICsgdGhpcy5jaGF0Um9vbS5pZCwge1xyXG4gICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFnZTogKyt0aGlzLmN1cnJlbnRQYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgIGxpbWl0OiA5OTlcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSkudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgdmFyIGRhdGEgPSByZXN1bHQuZGF0YS5kYXRhO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChkYXRhICYmIGRhdGEubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS5zb3J0KChhLCBiKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhLmlkIC0gYi5pZDtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZXNzYWdlcyA9IGRhdGEuY29uY2F0KHRoaXMubWVzc2FnZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFBhZ2UgPSByZXN1bHQuZGF0YS5jdXJyZW50X3BhZ2U7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UGFnZSA9PT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uLnNjcm9sbFRvcCh0aGlzLiRjb252ZXJzYXRpb24ucHJvcCgnc2Nyb2xsSGVpZ2h0JykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uLnNjcm9sbFRvcCh0aGlzLiRjb252ZXJzYXRpb24ucHJvcCgnc2Nyb2xsSGVpZ2h0JykgLSBwcmV2U2Nyb2xsSGVpZ2h0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kcmVmcy5pbmZpbml0ZUxvYWRpbmcuJGVtaXQoJyRJbmZpbml0ZUxvYWRpbmc6bG9hZGVkJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kcmVmcy5pbmZpbml0ZUxvYWRpbmcuJGVtaXQoJyRJbmZpbml0ZUxvYWRpbmc6Y29tcGxldGUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0TWVzc2FnZXMyKCkge1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvbWVzc2FnZXMvY2hhdC1yb29tLycgKyB0aGlzLmNoYXRSb29tLmlkLCB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBwYWdlOiArK3RoaXMuY3VycmVudFBhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgbGltaXQ6IDk5OVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IHJlc3VsdC5kYXRhLmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRhLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGEuaWQgLSBiLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1lc3NhZ2VzID0gZGF0YS5jb25jYXQodGhpcy5tZXNzYWdlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZSA9IHJlc3VsdC5kYXRhLmN1cnJlbnRfcGFnZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRQYWdlID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSAtIHByZXZTY3JvbGxIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpsb2FkZWQnKTtcclxuICAgICAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBtYXJrQWxsQXNSZWFkKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5wb3N0KCcvbWVzc2FnZXMvbWFyay1hbGwtYXMtcmVhZCcsIHtcclxuICAgICAgICAgICAgICAgIHRvX2NoYXRfcm9vbTogdGhpcy5jaGF0Um9vbS5pZFxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LWJveC5tYXJrZWQtYXMtcmVhZCcpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzZW5kKGUpIHtcclxuICAgICAgICAgICAgaWYgKCgoZS5rZXlDb2RlID09PSAxMykgJiYgISB0aGlzLmVudGVyRW5hYmxlZCkgfHwgISB0aGlzLm5ld01lc3NhZ2UpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKChlLmtleUNvZGUgPT09IDEzKSAmJiB0aGlzLmVudGVyRW5hYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgdXNlcl9pZDogd2luZG93LkxhcmF2ZWwudXNlci5pZCxcclxuICAgICAgICAgICAgICAgIGJvZHk6IHRoaXMubmV3TWVzc2FnZVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kY29udmVyc2F0aW9uLnNjcm9sbFRvcCh0aGlzLiRjb252ZXJzYXRpb24ucHJvcCgnc2Nyb2xsSGVpZ2h0JykpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuJHJlZnMuaW5maW5pdGVMb2FkaW5nLiRlbWl0KCckSW5maW5pdGVMb2FkaW5nOmxvYWRlZCcpO1xyXG4gICAgICAgICAgICB9LmJpbmQodGhpcyksIDEpO1xyXG5cclxuICAgICAgICAgICAgYXhpb3NBUEl2MS5wb3N0KCcvbWVzc2FnZXMnLCB7XHJcbiAgICAgICAgICAgICAgICB0b19jaGF0X3Jvb206IHRoaXMuY2hhdFJvb20uaWQsXHJcbiAgICAgICAgICAgICAgICBib2R5OiB0aGlzLm5ld01lc3NhZ2VcclxuICAgICAgICAgICAgfSkudGhlbihyZXN1bHQgPT4gRXZlbnQuZmlyZSgnY2hhdC1ib3guc2VuZCcpKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubmV3TWVzc2FnZSA9IG51bGw7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY2xvc2UoKSB7XHJcbiAgICAgICAgICAgIEV2ZW50LmZpcmUoJ2NoYXQtYm94LmNsb3NlJywgdGhpcy5jaGF0Um9vbSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaXNNZShtZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB3aW5kb3cuTGFyYXZlbC51c2VyLmlkID09IG1lc3NhZ2UudXNlcl9pZDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzY3JvbGxUb0JvdHRvbSgpIHtcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRjb252ZXJzYXRpb24uc2Nyb2xsVG9wKHRoaXMuJGNvbnZlcnNhdGlvbi5wcm9wKCdzY3JvbGxIZWlnaHQnKSk7XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgMSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgbGlzdGVuKCkge1xyXG4gICAgICAgICAgICBFdmVudC5saXN0ZW4oJ2NoYXQtcm9vbS0nICsgdGhpcy5jaGF0Um9vbS5pZCwgZnVuY3Rpb24gKG1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgICAgIGlmIChtZXNzYWdlLmNoYXRfcm9vbV9pZCA9PSB0aGlzLmNoYXRSb29tLmlkKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvdXNlcnMvJyArIG1lc3NhZ2UudXNlcl9pZCkudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5ld01lc3NhZ2UyID0ge1wiYm9keVwiOiBtZXNzYWdlLmJvZHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ1c2VyXCI6IHtcImF2YXRhclwiOiByZXN1bHQuZGF0YS5hdmF0YXIsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZnVsbF9uYW1lXCI6IHJlc3VsdC5kYXRhLmZ1bGxfbmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm5pY2tfbmFtZVwiOiByZXN1bHQuZGF0YS5uaWNrX25hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJmaXJzdF9uYW1lXCI6IHJlc3VsdC5kYXRhLmZpcnN0X25hbWV9fTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tZXNzYWdlcy5wdXNoKHRoaXMubmV3TWVzc2FnZTIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbFRvQm90dG9tKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LmJpbmQodGhpcykpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICAvLyBhdmF0YXIoKSB7XHJcbiAgICAgICAgLy8gICAgIHJldHVybiB0aGlzLm1lc3NhZ2UuY2hhdF9yb29tLnVzZXJzWzBdWydhdmF0YXInXTtcclxuICAgICAgICAvLyB9LFxyXG4gICAgICAgIGNoYXRSb29tTmFtZSgpIHtcclxuICAgICAgICAgICAgaWYgKCEgdGhpcy5jaGF0Um9vbSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jaGF0Um9vbS5uYW1lO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAgIEluZmluaXRlTG9hZGluZ1xyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0Q2hhdFJvb20oKTtcclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQ2hhdEJveFJvb20udnVlPzNjNTU1YzdiIiwiPHRlbXBsYXRlPlxyXG48dWwgY2xhc3M9XCJsaXN0LWdyb3VwXCI+XHJcbiAgICA8YnViYmxlLW5vdC1tZSB2LWlmPVwiIWlzTWVcIiA6bWVzc2FnZT1cIm1lc3NhZ2VcIj48L2J1YmJsZS1ub3QtbWU+XHJcbiAgICA8YnViYmxlLW1lIHYtaWY9XCJpc01lXCIgOm1lc3NhZ2U9XCJtZXNzYWdlXCI+PC9idWJibGUtbWU+XHJcbjwvdWw+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczogWydtZXNzYWdlJ10sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBpc01lKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5tZXNzYWdlLnVzZXJfaWQgPT0gd2luZG93LkxhcmF2ZWwudXNlci5pZDtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgICAnYnViYmxlLW5vdC1tZSc6IHJlcXVpcmUoJy4vQnViYmxlTm90TWUudnVlJyksXHJcbiAgICAgICAgJ2J1YmJsZS1tZSc6IHJlcXVpcmUoJy4vQnViYmxlTWUudnVlJylcclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gVXNlckNoYXRSb29tTWVzc2FnZS52dWU/MmI2ZmQ1YWEiLCI8dGVtcGxhdGU+XHJcbjxkaXYgY2xhc3M9XCJsaXN0LWdyb3VwXCI+XHJcbiAgICA8cm91dGVyLWxpbmsgOnRvPVwicm91dGVUb1wiIGNsYXNzPVwibGlzdC1ncm91cC1pdGVtXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTMgdXNlci1hdmF0YXJcIj5cclxuICAgICAgICAgICAgICAgIDxpbWcgOnNyYz1cImF2YXRhclwiIGNsYXNzPVwiaW1nLWNpcmNsZVwiPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy05IHVzZXItZGV0YWlsXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMiB1c2VyLWZ1bGxuYW1lXCI+PGI+e3sgZnVsbE5hbWUgfX08L2I+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyIHVzZXItbGF0ZXN0LW1lc3NhZ2VcIj57eyBtZXNzYWdlLmJvZHkgfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvcm91dGVyLWxpbms+XHJcbjwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgcHJvcHM6IFsnbWVzc2FnZSddLFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgYXZhdGFyKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5tZXNzYWdlLmNoYXRfcm9vbS51c2Vyc1swXVsnYXZhdGFyJ107XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZnVsbE5hbWUoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm1lc3NhZ2UuY2hhdF9yb29tLm5hbWUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLm1lc3NhZ2UuY2hhdF9yb29tLnVzZXJzWzBdLmZ1bGxfbmFtZSArICcgLSAnICsgdGhpcy5tZXNzYWdlLmNoYXRfcm9vbS5uYW1lO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5tZXNzYWdlLmNoYXRfcm9vbS5uYW1lIHx8IHRoaXMubWVzc2FnZS5jaGF0X3Jvb20udXNlcnNbMF1bJ2Z1bGxfbmFtZSddO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHJvdXRlVG8oKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm1lc3NhZ2UuY2hhdF9yb29tLm5hbWUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2NoYXQtcm9vbScsXHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiB0aGlzLm1lc3NhZ2UuY2hhdF9yb29tLmlkXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIG5hbWU6ICd1c2VyJyxcclxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiB0aGlzLm1lc3NhZ2UuY2hhdF9yb29tLnVzZXJzWzBdLmlkXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBVc2VyTGF0ZXN0TWVzc2FnZS52dWU/MGIxZWVmNjQiLCI8dGVtcGxhdGU+XHJcbjxkaXYgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0IHBhbmVsLWZsb2F0XCIgOmNsYXNzPVwicGFuZWxDbGFzc1wiPlxyXG4gICAgPGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBsYWJlbC1wbGFjZWhvbGRlclwiPlxyXG4gICAgICAgICAgICA8bGFiZWwgZm9yPVwidXNlci1zZWFyY2hcIiBjbGFzcz1cImNvbnRyb2wtbGFiZWxcIj57e3RoaXMuJHBhcmVudC5sYW5nWydzZWFyY2gtdXNlciddfX0uLi48L2xhYmVsPlxyXG4gICAgICAgICAgICA8aW5wdXQgdi1tb2RlbD1cInNlYXJjaFwiIEBrZXl1cD1cInNlYXJjaFVzZXJcIiB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJ1c2VyLXNlYXJjaFwiPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDx1bCBjbGFzcz1cInVzZXJzLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICA8bGkgdi1mb3I9XCJ1c2VyIGluIHVzZXJzXCIgQGNsaWNrPVwic2VsZWN0KHVzZXIpXCI+XHJcbiAgICAgICAgICAgICAgICA8aW1nIDpzcmM9XCJhdmF0YXIodXNlcilcIiBjbGFzcz1cImltZy1jaXJjbGVcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuPnt7IHVzZXIuZnVsbF9uYW1lIH19PC9zcGFuPlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgIDwvdWw+XHJcbiAgICA8L2Rpdj5cclxuPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczogWyd0YXJnZXQnLCAncGFuZWxDbGFzcyddLFxyXG5cclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgdGFyZ2V0RWw6IG51bGwsXHJcbiAgICAgICAgICAgIHNlYXJjaDogJycsXHJcbiAgICAgICAgICAgIHVzZXJzOiBbXSxcclxuICAgICAgICAgICAgdXNlcnNDb250YWluZXI6IG51bGwsXHJcbiAgICAgICAgICAgIHRpbWVvdXQ6IG51bGxcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHdhdGNoOiB7XHJcbiAgICAgICAgc2VhcmNoKHZhbCkge1xyXG4gICAgICAgICAgICBpZiAodmFsICE9IG51bGwgJiYgdmFsLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2VycyA9IFtdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdXNlcnModmFsKSB7XHJcbiAgICAgICAgICAgIGlmICh2YWwubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJzQ29udGFpbmVyLnBlcmZlY3RTY3JvbGxiYXIoJ3VwZGF0ZScpO1xyXG4gICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHNlYXJjaFVzZXIoZSkge1xyXG4gICAgICAgICAgICBpZiAoKCgoZS53aGljaCA8PSA5MCkgJiYgKGUud2hpY2ggPj0gNDgpKSB8fCBlLndoaWNoID09PSA4KSAmJiB0aGlzLnNlYXJjaCAmJiAodGhpcy5zZWFyY2gubGVuZ3RoID49IDIpKSB7XHJcbiAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lb3V0KTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnRpbWVvdXQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL21lc3NhZ2VzL3VzZXJzP3NlYXJjaD0nICsgdGhpcy5zZWFyY2gpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJzID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfS5iaW5kKHRoaXMpLCAyMDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2VsZWN0KHVzZXIpIHtcclxuICAgICAgICAgICAgRXZlbnQuZmlyZSgndXNlci1zZWFyY2guc2VsZWN0ZWQnLCB1c2VyKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX3Jlc2V0KCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgYXZhdGFyKHVzZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHVzZXIuYXZhdGFyO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIF9wb3NpdGlvbkVsKCkge1xyXG4gICAgICAgICAgICB2YXIgb2Zmc2V0ID0gdGhpcy50YXJnZXRFbC5vZmZzZXQoKSxcclxuICAgICAgICAgICAgICAgIHRlV2lkdGggPSB0aGlzLnRhcmdldEVsLm91dGVyV2lkdGgoKSxcclxuICAgICAgICAgICAgICAgIHRlSGVpZ2h0ID0gdGhpcy50YXJnZXRFbC5vdXRlckhlaWdodCgpLFxyXG4gICAgICAgICAgICAgICAgd1dpZHRoID0gd2luZG93LmlubmVyV2lkdGgsXHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbiA9IHtcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IG9mZnNldC50b3AgKyB0ZUhlaWdodCArIDEwLFxyXG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IG9mZnNldC5sZWZ0XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgdmFyICRlbCA9ICQodGhpcy4kZWwpLFxyXG4gICAgICAgICAgICAgICAgJGVsV2lkdGggPSAkZWwub3V0ZXJXaWR0aCgpIHx8ICRlbC5pbm5lcldpZHRoKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoKG9mZnNldC5sZWZ0ICsgJGVsV2lkdGgpID4gd1dpZHRoKSB7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbi5sZWZ0ID0gKG9mZnNldC5sZWZ0IC0gJGVsV2lkdGggKyB0ZVdpZHRoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgJGVsLm9mZnNldChwb3NpdGlvbik7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgX3Jlc2V0KCkge1xyXG4gICAgICAgICAgICB2YXIgJGVsID0gJCh0aGlzLiRlbCksXHJcbiAgICAgICAgICAgICAgICAkaW5wdXQgPSAkZWwuZmluZCgnaW5wdXQnKSxcclxuICAgICAgICAgICAgICAgICRldmVudCA9ICQuRXZlbnQoJ2tleXVwJywge3doaWNoOiA4fSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMudXNlcnMgPSBbXTtcclxuXHJcbiAgICAgICAgICAgICRpbnB1dC50cmlnZ2VyKCRldmVudCk7XHJcbiAgICAgICAgICAgICRpbnB1dC5ibHVyKCk7XHJcblxyXG4gICAgICAgICAgICAkZWwuaGlkZSgpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIF9yZWdpc3RlckV2ZW50TGlzdGVuZXJzKCkge1xyXG4gICAgICAgICAgICAkKHdpbmRvdykucmVzaXplKHRoaXMuX3Bvc2l0aW9uRWwpO1xyXG5cclxuICAgICAgICAgICAgRXZlbnQubGlzdGVuKCd1c2VyLXNlYXJjaC5zaG93JywgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdmFyICRlbCA9ICQodGhpcy4kZWwpO1xyXG5cclxuICAgICAgICAgICAgICAgICRlbC5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAkZWwuZmluZCgnaW5wdXQnKS5mb2N1cygpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuX3Bvc2l0aW9uRWwoKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBSZW1vdmUvSGlkZSBmbG9hdGluZyBwYW5lbHMgb24gb3V0c2lkZSBjbGlja1xyXG4gICAgICAgICAgICAkKGRvY3VtZW50KS5tb3VzZXVwKGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgJGVsID0gJCh0aGlzLiRlbCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gaWYgdGhlIHRhcmdldCBvZiB0aGUgY2xpY2sgaXNuJ3QgdGhlIHBhbmVsIG5vciBhIGRlc2NlbmRhbnQgb2YgdGhlIHBhbmVsXHJcbiAgICAgICAgICAgICAgICBpZiAoISAkZWwuaXMoZS50YXJnZXQpICYmICRlbC5oYXMoZS50YXJnZXQpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3Jlc2V0KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuX3JlZ2lzdGVyRXZlbnRMaXN0ZW5lcnMoKTtcclxuICAgIH0sXHJcblxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICB0aGlzLnRhcmdldEVsID0gJCh0aGlzLnRhcmdldCk7XHJcbiAgICAgICAgdGhpcy51c2Vyc0NvbnRhaW5lciA9ICQoJy51c2Vycy1jb250YWluZXInKTtcclxuICAgICAgICB0aGlzLnVzZXJzQ29udGFpbmVyLnBlcmZlY3RTY3JvbGxiYXIoKTtcclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFVzZXJTZWFyY2gudnVlPzA3OWYzMTM5IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQnViYmxlTWUudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0xYmZjZjA5NFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9CdWJibGVNZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXHBhZ2VzXFxcXG1lc3NhZ2VcXFxcQnViYmxlTWUudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQnViYmxlTWUudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTFiZmNmMDk0XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMWJmY2YwOTRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL0J1YmJsZU1lLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzU2XG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0J1YmJsZU5vdE1lLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNWExNTRjMDJcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQnViYmxlTm90TWUudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxwYWdlc1xcXFxtZXNzYWdlXFxcXEJ1YmJsZU5vdE1lLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEJ1YmJsZU5vdE1lLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi01YTE1NGMwMlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTVhMTU0YzAyXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9CdWJibGVOb3RNZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM1N1xuLy8gbW9kdWxlIGNodW5rcyA9IDQiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9DaGF0Qm94LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMjNlODM4YjNcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQ2hhdEJveC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXHBhZ2VzXFxcXG1lc3NhZ2VcXFxcQ2hhdEJveC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBDaGF0Qm94LnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0yM2U4MzhiM1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTIzZTgzOGIzXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9DaGF0Qm94LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzU4XG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NoYXRCb3hSb29tLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMmU4ZmIyYWVcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQ2hhdEJveFJvb20udnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxwYWdlc1xcXFxtZXNzYWdlXFxcXENoYXRCb3hSb29tLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIENoYXRCb3hSb29tLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0yZThmYjJhZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTJlOGZiMmFlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9DaGF0Qm94Um9vbS52dWVcbi8vIG1vZHVsZSBpZCA9IDM1OVxuLy8gbW9kdWxlIGNodW5rcyA9IDQiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Vc2VyQ2hhdFJvb21NZXNzYWdlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMGU5Y2EzNDlcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vVXNlckNoYXRSb29tTWVzc2FnZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXHBhZ2VzXFxcXG1lc3NhZ2VcXFxcVXNlckNoYXRSb29tTWVzc2FnZS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBVc2VyQ2hhdFJvb21NZXNzYWdlLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0wZTljYTM0OVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTBlOWNhMzQ5XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9Vc2VyQ2hhdFJvb21NZXNzYWdlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzYwXG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1VzZXJMYXRlc3RNZXNzYWdlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZDIzZjNhNTZcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vVXNlckxhdGVzdE1lc3NhZ2UudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxwYWdlc1xcXFxtZXNzYWdlXFxcXFVzZXJMYXRlc3RNZXNzYWdlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFVzZXJMYXRlc3RNZXNzYWdlLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1kMjNmM2E1NlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWQyM2YzYTU2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9Vc2VyTGF0ZXN0TWVzc2FnZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM2MVxuLy8gbW9kdWxlIGNodW5rcyA9IDQiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Vc2VyU2VhcmNoLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtYjAwOTlkYmFcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vVXNlclNlYXJjaC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXHBhZ2VzXFxcXG1lc3NhZ2VcXFxcVXNlclNlYXJjaC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBVc2VyU2VhcmNoLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1iMDA5OWRiYVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWIwMDk5ZGJhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9Vc2VyU2VhcmNoLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzYyXG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGlzdC1ncm91cFwiXG4gIH0sIFsoIV92bS5pc01lKSA/IF9jKCdidWJibGUtbm90LW1lJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcIm1lc3NhZ2VcIjogX3ZtLm1lc3NhZ2VcbiAgICB9XG4gIH0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uaXNNZSkgPyBfYygnYnViYmxlLW1lJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcIm1lc3NhZ2VcIjogX3ZtLm1lc3NhZ2VcbiAgICB9XG4gIH0pIDogX3ZtLl9lKCldLCAxKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0wZTljYTM0OVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTBlOWNhMzQ5XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL21lc3NhZ2UvVXNlckNoYXRSb29tTWVzc2FnZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM2M1xuLy8gbW9kdWxlIGNodW5rcyA9IDQiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2xpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImxpc3QtZ3JvdXAtaXRlbVwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMiB1c2VyLWNoYXQgbWVcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMTJcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ1c2VyLW1lc3NhZ2VcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubWVzc2FnZS5ib2R5KSldKV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMWJmY2YwOTRcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0xYmZjZjA5NFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL0J1YmJsZU1lLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzY4XG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiAoX3ZtLnVzZXIpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1jaGF0LWJveFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWhlYWRpbmdcIlxuICB9LCBbX3ZtLl92KFwiXFxyXFxuICAgICAgICBcIiArIF92bS5fcyhfdm0uZnVsbE5hbWUpICsgXCJcXHJcXG4gICAgICAgIFwiKSwgKF92bS5zaG93U2V0dGluZ3MpID8gX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHVsbC1yaWdodFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiaWRcIjogXCJjaGF0LWJveC1zZXR0aW5nc1wiXG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY29nXCJcbiAgfSldKSA6IF92bS5fZSgpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hhdC1ib3hcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1ib2R5XCJcbiAgfSwgW19jKCdpbmZpbml0ZS1sb2FkaW5nJywge1xuICAgIHJlZjogXCJpbmZpbml0ZUxvYWRpbmdcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJvbi1pbmZpbml0ZVwiOiBfdm0ub25JbmZpbml0ZSxcbiAgICAgIFwiZGlyZWN0aW9uXCI6IFwidG9wXCIsXG4gICAgICBcImRpc3RhbmNlXCI6IDBcbiAgICB9XG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzbG90OiBcIm5vLW1vcmVcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc2xvdDogXCJuby1yZXN1bHRzXCJcbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3VsJywgX3ZtLl9sKChfdm0ubWVzc2FnZXMpLCBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgcmV0dXJuIF9jKCdsaScsIHtcbiAgICAgIGNsYXNzOiB7XG4gICAgICAgIG1lOiBfdm0uaXNNZShtZXNzYWdlKVxuICAgICAgfVxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMlwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJtZXNzYWdlXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhtZXNzYWdlLmJvZHkpKV0pXSldKV0pXG4gIH0pKV0sIDEpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWZvb3RlclwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMVwiXG4gIH0sIFtfYygndGV4dGFyZWEnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgdmFsdWU6IChfdm0ubmV3TWVzc2FnZSksXG4gICAgICBleHByZXNzaW9uOiBcIm5ld01lc3NhZ2VcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwiYXJlYW1zZ1wiLFxuICAgICAgXCJkaXNhYmxlZFwiOiBfdm0uaXNCbG9ja2VkLFxuICAgICAgXCJwbGFjZWhvbGRlclwiOiB0aGlzLiRwYXJlbnQubGFuZ1snd3JpdGUtc29tZXRoaW5nJ11cbiAgICB9LFxuICAgIGRvbVByb3BzOiB7XG4gICAgICBcInZhbHVlXCI6IChfdm0ubmV3TWVzc2FnZSlcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImtleWRvd25cIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICghKCdidXR0b24nIGluICRldmVudCkgJiYgX3ZtLl9rKCRldmVudC5rZXlDb2RlLCBcImVudGVyXCIsIDEzKSkgeyByZXR1cm4gbnVsbDsgfVxuICAgICAgICBfdm0uc2VuZCgkZXZlbnQpXG4gICAgICB9LFxuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0ubmV3TWVzc2FnZSA9ICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImVudGVyLXNlbmRcIlxuICB9LCBbX2MoJ2xhYmVsJywgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5lbnRlckVuYWJsZWQpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlbnRlckVuYWJsZWRcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiLFxuICAgICAgXCJpZFwiOiBcImVudGVyX2VuYWJsZWRcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBBcnJheS5pc0FycmF5KF92bS5lbnRlckVuYWJsZWQpID8gX3ZtLl9pKF92bS5lbnRlckVuYWJsZWQsIG51bGwpID4gLTEgOiAoX3ZtLmVudGVyRW5hYmxlZClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcIl9fY1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkYSA9IF92bS5lbnRlckVuYWJsZWQsXG4gICAgICAgICAgJCRlbCA9ICRldmVudC50YXJnZXQsXG4gICAgICAgICAgJCRjID0gJCRlbC5jaGVja2VkID8gKHRydWUpIDogKGZhbHNlKTtcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgIHZhciAkJHYgPSBudWxsLFxuICAgICAgICAgICAgJCRpID0gX3ZtLl9pKCQkYSwgJCR2KTtcbiAgICAgICAgICBpZiAoJCRlbC5jaGVja2VkKSB7XG4gICAgICAgICAgICAkJGkgPCAwICYmIChfdm0uZW50ZXJFbmFibGVkID0gJCRhLmNvbmNhdCgkJHYpKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkJGkgPiAtMSAmJiAoX3ZtLmVudGVyRW5hYmxlZCA9ICQkYS5zbGljZSgwLCAkJGkpLmNvbmNhdCgkJGEuc2xpY2UoJCRpICsgMSkpKVxuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBfdm0uZW50ZXJFbmFibGVkID0gJCRjXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgXCIgKyBfdm0uX3ModGhpcy4kcGFyZW50LmxhbmdbJ2VudGVyLXRvLXNlbmQnXSkgKyBcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMSBidG4tc2VuZC1jb250YWluZXJcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuLXNlbmRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS5zZW5kXG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtc2VuZFwiXG4gIH0pXSldKV0pXSldKV0pIDogX3ZtLl9lKClcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMjNlODM4YjNcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0yM2U4MzhiM1wifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL0NoYXRCb3gudnVlXG4vLyBtb2R1bGUgaWQgPSAzNzJcbi8vIG1vZHVsZSBjaHVua3MgPSA0IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIChfdm0uY2hhdFJvb20pID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1jaGF0LWJveFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInBhbmVsLWhlYWRpbmdcIlxuICB9LCBbX3ZtLl92KFwiXFxyXFxuICAgICAgICBcIiArIF92bS5fcyhfdm0uY2hhdFJvb21OYW1lKSArIFwiXFxyXFxuICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNoYXQtYm94XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiXG4gIH0sIFtfYygnaW5maW5pdGUtbG9hZGluZycsIHtcbiAgICByZWY6IFwiaW5maW5pdGVMb2FkaW5nXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwib24taW5maW5pdGVcIjogX3ZtLm9uSW5maW5pdGUsXG4gICAgICBcImRpcmVjdGlvblwiOiBcInRvcFwiLFxuICAgICAgXCJkaXN0YW5jZVwiOiAwXG4gICAgfVxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc2xvdDogXCJuby1tb3JlXCJcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHNsb3Q6IFwibm8tcmVzdWx0c1wiXG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCd1bCcsIF92bS5fbCgoX3ZtLm1lc3NhZ2VzKSwgZnVuY3Rpb24obWVzc2FnZSwga2V5KSB7XG4gICAgcmV0dXJuIF9jKCdsaScsIHtcbiAgICAgIGNsYXNzOiB7XG4gICAgICAgIG1lOiBfdm0uaXNNZShtZXNzYWdlKVxuICAgICAgfVxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgICB9LCBbKCFfdm0uaXNNZShtZXNzYWdlKSkgPyBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTEgdXNlci1hdmF0YXJcIixcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwicGFkZGluZy1yaWdodFwiOiBcIjBweFwiXG4gICAgICB9XG4gICAgfSwgW19jKCdpbWcnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJpbWctY2lyY2xlIHVzZXJBdmF0YXJcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwic3JjXCI6IG1lc3NhZ2UudXNlci5hdmF0YXIsXG4gICAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICAgIFwiZGF0YS1wbGFjZW1lbnRcIjogXCJyaWdodFwiLFxuICAgICAgICBcInRpdGxlXCI6IG1lc3NhZ2UudXNlci5mdWxsX25hbWVcbiAgICAgIH1cbiAgICB9KV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksICghX3ZtLmlzTWUobWVzc2FnZSkpID8gX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMVwiLFxuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJwYWRkaW5nLWxlZnRcIjogXCIwcHhcIixcbiAgICAgICAgXCJtYXJnaW4tbGVmdFwiOiBcIi0xMHB4XCJcbiAgICAgIH1cbiAgICB9LCBbKG1lc3NhZ2UudXNlci5uaWNrX25hbWUgIT09IG51bGwpID8gX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJ1c2VybmFtZVwiXG4gICAgfSwgW192bS5fdihfdm0uX3MobWVzc2FnZS51c2VyLm5pY2tfbmFtZSkpXSkgOiBfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInVzZXJuYW1lXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhtZXNzYWdlLnVzZXIuZmlyc3RfbmFtZSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdicicpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibWVzc2FnZVwiXG4gICAgfSwgW192bS5fdihfdm0uX3MobWVzc2FnZS5ib2R5KSldKV0pIDogX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMlwiLFxuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJwYWRkaW5nLWxlZnRcIjogXCIwcHhcIlxuICAgICAgfVxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibWVzc2FnZVwiXG4gICAgfSwgW192bS5fdihfdm0uX3MobWVzc2FnZS5ib2R5KSldKV0pXSldKVxuICB9KSldLCAxKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1mb290ZXJcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMTFcIlxuICB9LCBbKF92bS5jaGF0Um9vbS5pc19hY3RpdmUpID8gX2MoJ3RleHRhcmVhJywge1xuICAgIGRpcmVjdGl2ZXM6IFt7XG4gICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgIHZhbHVlOiAoX3ZtLm5ld01lc3NhZ2UpLFxuICAgICAgZXhwcmVzc2lvbjogXCJuZXdNZXNzYWdlXCJcbiAgICB9XSxcbiAgICBhdHRyczoge1xuICAgICAgXCJwbGFjZWhvbGRlclwiOiBcIldyaXRlIHNvbWV0aGluZy4uLlwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLm5ld01lc3NhZ2UpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJrZXlkb3duXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBpZiAoISgnYnV0dG9uJyBpbiAkZXZlbnQpICYmIF92bS5faygkZXZlbnQua2V5Q29kZSwgXCJlbnRlclwiLCAxMykpIHsgcmV0dXJuIG51bGw7IH1cbiAgICAgICAgaWYgKCEkZXZlbnQuY3RybEtleSkgeyByZXR1cm4gbnVsbDsgfVxuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgX3ZtLnNlbmQoJGV2ZW50KVxuICAgICAgfSxcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLm5ld01lc3NhZ2UgPSAkZXZlbnQudGFyZ2V0LnZhbHVlXG4gICAgICB9XG4gICAgfVxuICB9KSA6IF9jKCd0ZXh0YXJlYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJkaXNhYmxlZFwiOiBcIlwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiVGhpcyBpc3N1ZSBpcyBhbHJlYWR5IGNsb3NlZFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImVudGVyLXNlbmRcIlxuICB9LCBbX2MoJ2xhYmVsJywgW19jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5lbnRlckVuYWJsZWQpLFxuICAgICAgZXhwcmVzc2lvbjogXCJlbnRlckVuYWJsZWRcIlxuICAgIH1dLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJjaGVja2JveFwiLFxuICAgICAgXCJpZFwiOiBcImVudGVyX2VuYWJsZWRcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwiY2hlY2tlZFwiOiBBcnJheS5pc0FycmF5KF92bS5lbnRlckVuYWJsZWQpID8gX3ZtLl9pKF92bS5lbnRlckVuYWJsZWQsIG51bGwpID4gLTEgOiAoX3ZtLmVudGVyRW5hYmxlZClcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcIl9fY1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgdmFyICQkYSA9IF92bS5lbnRlckVuYWJsZWQsXG4gICAgICAgICAgJCRlbCA9ICRldmVudC50YXJnZXQsXG4gICAgICAgICAgJCRjID0gJCRlbC5jaGVja2VkID8gKHRydWUpIDogKGZhbHNlKTtcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgIHZhciAkJHYgPSBudWxsLFxuICAgICAgICAgICAgJCRpID0gX3ZtLl9pKCQkYSwgJCR2KTtcbiAgICAgICAgICBpZiAoJCRlbC5jaGVja2VkKSB7XG4gICAgICAgICAgICAkJGkgPCAwICYmIChfdm0uZW50ZXJFbmFibGVkID0gJCRhLmNvbmNhdCgkJHYpKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkJGkgPiAtMSAmJiAoX3ZtLmVudGVyRW5hYmxlZCA9ICQkYS5zbGljZSgwLCAkJGkpLmNvbmNhdCgkJGEuc2xpY2UoJCRpICsgMSkpKVxuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBfdm0uZW50ZXJFbmFibGVkID0gJCRjXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0pLCBfdm0uX3YoXCIgUHJlc3MgZW50ZXIgdG8gc2VuZCB0aGUgbWVzc2FnZVxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMSBidG4tc2VuZC1jb250YWluZXJcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuLXNlbmRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS5zZW5kXG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtc2VuZFwiXG4gIH0pXSldKV0pXSldKV0pIDogX3ZtLl9lKClcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMmU4ZmIyYWVcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0yZThmYjJhZVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL0NoYXRCb3hSb29tLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzc1XG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGlzdC1ncm91cC1pdGVtXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTEgdXNlci1hdmF0YXJcIlxuICB9LCBbX2MoJ2ltZycsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbWctY2lyY2xlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5hdmF0YXJcbiAgICB9XG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTExIHVzZXItY2hhdFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMlwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInVzZXItbWVzc2FnZVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5tZXNzYWdlLmJvZHkpKV0pXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi01YTE1NGMwMlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTVhMTU0YzAyXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL21lc3NhZ2UvQnViYmxlTm90TWUudnVlXG4vLyBtb2R1bGUgaWQgPSAzODVcbi8vIG1vZHVsZSBjaHVua3MgPSA0IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwgcGFuZWwtZGVmYXVsdCBwYW5lbC1mbG9hdFwiLFxuICAgIGNsYXNzOiBfdm0ucGFuZWxDbGFzc1xuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbC1ib2R5XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cCBsYWJlbC1wbGFjZWhvbGRlclwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29udHJvbC1sYWJlbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImZvclwiOiBcInVzZXItc2VhcmNoXCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWydzZWFyY2gtdXNlciddKSArIFwiLi4uXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5zZWFyY2gpLFxuICAgICAgZXhwcmVzc2lvbjogXCJzZWFyY2hcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJ0ZXh0XCIsXG4gICAgICBcImlkXCI6IFwidXNlci1zZWFyY2hcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5zZWFyY2gpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJrZXl1cFwiOiBfdm0uc2VhcmNoVXNlcixcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLnNlYXJjaCA9ICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgIH1cbiAgICB9XG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCd1bCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ1c2Vycy1jb250YWluZXJcIlxuICB9LCBfdm0uX2woKF92bS51c2VycyksIGZ1bmN0aW9uKHVzZXIpIHtcbiAgICByZXR1cm4gX2MoJ2xpJywge1xuICAgICAgb246IHtcbiAgICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBfdm0uc2VsZWN0KHVzZXIpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX2MoJ2ltZycsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1jaXJjbGVcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwic3JjXCI6IF92bS5hdmF0YXIodXNlcilcbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCBbX3ZtLl92KF92bS5fcyh1c2VyLmZ1bGxfbmFtZSkpXSldKVxuICB9KSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWIwMDk5ZGJhXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYjAwOTlkYmFcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbWVzc2FnZS9Vc2VyU2VhcmNoLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzk4XG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImxpc3QtZ3JvdXBcIlxuICB9LCBbX2MoJ3JvdXRlci1saW5rJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImxpc3QtZ3JvdXAtaXRlbVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInRvXCI6IF92bS5yb3V0ZVRvXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3dcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMyB1c2VyLWF2YXRhclwiXG4gIH0sIFtfYygnaW1nJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImltZy1jaXJjbGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJzcmNcIjogX3ZtLmF2YXRhclxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtOSB1c2VyLWRldGFpbFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMiB1c2VyLWZ1bGxuYW1lXCJcbiAgfSwgW19jKCdiJywgW192bS5fdihfdm0uX3MoX3ZtLmZ1bGxOYW1lKSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTEyIHVzZXItbGF0ZXN0LW1lc3NhZ2VcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0ubWVzc2FnZS5ib2R5KSldKV0pXSldKV0pXSwgMSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtZDIzZjNhNTZcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1kMjNmM2E1NlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9tZXNzYWdlL1VzZXJMYXRlc3RNZXNzYWdlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDAyXG4vLyBtb2R1bGUgY2h1bmtzID0gNCIsIiFmdW5jdGlvbihwLHgpe1wib2JqZWN0XCI9PXR5cGVvZiBleHBvcnRzJiZcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlP21vZHVsZS5leHBvcnRzPXgoKTpcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFtdLHgpOlwib2JqZWN0XCI9PXR5cGVvZiBleHBvcnRzP2V4cG9ydHMuVnVlSW5maW5pdGVMb2FkaW5nPXgoKTpwLlZ1ZUluZmluaXRlTG9hZGluZz14KCl9KHRoaXMsZnVuY3Rpb24oKXtyZXR1cm4gZnVuY3Rpb24ocCl7ZnVuY3Rpb24geChhKXtpZih0W2FdKXJldHVybiB0W2FdLmV4cG9ydHM7dmFyIGU9dFthXT17ZXhwb3J0czp7fSxpZDphLGxvYWRlZDohMX07cmV0dXJuIHBbYV0uY2FsbChlLmV4cG9ydHMsZSxlLmV4cG9ydHMseCksZS5sb2FkZWQ9ITAsZS5leHBvcnRzfXZhciB0PXt9O3JldHVybiB4Lm09cCx4LmM9dCx4LnA9XCIvXCIseCgwKX0oW2Z1bmN0aW9uKHAseCx0KXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBhKHApe3JldHVybiBwJiZwLl9fZXNNb2R1bGU/cDp7ZGVmYXVsdDpwfX1PYmplY3QuZGVmaW5lUHJvcGVydHkoeCxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KTt2YXIgZT10KDQpLG89YShlKTt4LmRlZmF1bHQ9by5kZWZhdWx0LFwidW5kZWZpbmVkXCIhPXR5cGVvZiB3aW5kb3cmJndpbmRvdy5WdWUmJndpbmRvdy5WdWUuY29tcG9uZW50KFwiaW5maW5pdGUtbG9hZGluZ1wiLG8uZGVmYXVsdCl9LGZ1bmN0aW9uKHAseCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gdChwKXtyZXR1cm5cIkJPRFlcIj09PXAudGFnTmFtZT93aW5kb3c6W1wic2Nyb2xsXCIsXCJhdXRvXCJdLmluZGV4T2YoZ2V0Q29tcHV0ZWRTdHlsZShwKS5vdmVyZmxvd1kpPi0xP3A6cC5oYXNBdHRyaWJ1dGUoXCJpbmZpbml0ZS13cmFwcGVyXCIpfHxwLmhhc0F0dHJpYnV0ZShcImRhdGEtaW5maW5pdGUtd3JhcHBlclwiKT9wOnQocC5wYXJlbnROb2RlKX1mdW5jdGlvbiBhKHAseCx0KXt2YXIgYT12b2lkIDA7aWYoXCJ0b3BcIj09PXQpYT1pc05hTihwLnNjcm9sbFRvcCk/cC5wYWdlWU9mZnNldDpwLnNjcm9sbFRvcDtlbHNle3ZhciBlPXguZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wLG89cD09PXdpbmRvdz93aW5kb3cuaW5uZXJIZWlnaHQ6cC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5ib3R0b207YT1lLW99cmV0dXJuIGF9T2JqZWN0LmRlZmluZVByb3BlcnR5KHgsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSk7dmFyIGU9e2J1YmJsZXM6XCJsb2FkaW5nLWJ1YmJsZXNcIixjaXJjbGVzOlwibG9hZGluZy1jaXJjbGVzXCIsZGVmYXVsdDpcImxvYWRpbmctZGVmYXVsdFwiLHNwaXJhbDpcImxvYWRpbmctc3BpcmFsXCIsd2F2ZURvdHM6XCJsb2FkaW5nLXdhdmUtZG90c1wifTt4LmRlZmF1bHQ9e2RhdGE6ZnVuY3Rpb24oKXtyZXR1cm57c2Nyb2xsUGFyZW50Om51bGwsc2Nyb2xsSGFuZGxlcjpudWxsLGlzTG9hZGluZzohMSxpc0NvbXBsZXRlOiExLGlzRmlyc3RMb2FkOiEwfX0sY29tcHV0ZWQ6e3NwaW5uZXJUeXBlOmZ1bmN0aW9uKCl7cmV0dXJuIGVbdGhpcy5zcGlubmVyXXx8ZS5kZWZhdWx0fX0scHJvcHM6e2Rpc3RhbmNlOnt0eXBlOk51bWJlcixkZWZhdWx0OjEwMH0sb25JbmZpbml0ZTpGdW5jdGlvbixzcGlubmVyOlN0cmluZyxkaXJlY3Rpb246e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCJib3R0b21cIn19LG1vdW50ZWQ6ZnVuY3Rpb24oKXt2YXIgcD10aGlzO3RoaXMuc2Nyb2xsUGFyZW50PXQodGhpcy4kZWwpLHRoaXMuc2Nyb2xsSGFuZGxlcj1mdW5jdGlvbigpe3RoaXMuaXNMb2FkaW5nfHx0aGlzLmF0dGVtcHRMb2FkKCl9LmJpbmQodGhpcyksc2V0VGltZW91dCh0aGlzLnNjcm9sbEhhbmRsZXIsMSksdGhpcy5zY3JvbGxQYXJlbnQuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHRoaXMuc2Nyb2xsSGFuZGxlciksdGhpcy4kb24oXCIkSW5maW5pdGVMb2FkaW5nOmxvYWRlZFwiLGZ1bmN0aW9uKCl7cC5pc0ZpcnN0TG9hZD0hMSxwLmlzTG9hZGluZyYmcC4kbmV4dFRpY2socC5hdHRlbXB0TG9hZCl9KSx0aGlzLiRvbihcIiRJbmZpbml0ZUxvYWRpbmc6Y29tcGxldGVcIixmdW5jdGlvbigpe3AuaXNMb2FkaW5nPSExLHAuaXNDb21wbGV0ZT0hMCxwLnNjcm9sbFBhcmVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIscC5zY3JvbGxIYW5kbGVyKX0pLHRoaXMuJG9uKFwiJEluZmluaXRlTG9hZGluZzpyZXNldFwiLGZ1bmN0aW9uKCl7cC5pc0xvYWRpbmc9ITEscC5pc0NvbXBsZXRlPSExLHAuaXNGaXJzdExvYWQ9ITAscC5zY3JvbGxQYXJlbnQuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHAuc2Nyb2xsSGFuZGxlciksc2V0VGltZW91dChwLnNjcm9sbEhhbmRsZXIsMSl9KX0sZGVhY3RpdmF0ZWQ6ZnVuY3Rpb24oKXt0aGlzLmlzTG9hZGluZz0hMSx0aGlzLnNjcm9sbFBhcmVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsdGhpcy5zY3JvbGxIYW5kbGVyKX0sYWN0aXZhdGVkOmZ1bmN0aW9uKCl7dGhpcy5zY3JvbGxQYXJlbnQuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHRoaXMuc2Nyb2xsSGFuZGxlcil9LG1ldGhvZHM6e2F0dGVtcHRMb2FkOmZ1bmN0aW9uKCl7dmFyIHA9YSh0aGlzLnNjcm9sbFBhcmVudCx0aGlzLiRlbCx0aGlzLmRpcmVjdGlvbik7IXRoaXMuaXNDb21wbGV0ZSYmcDw9dGhpcy5kaXN0YW5jZT8odGhpcy5pc0xvYWRpbmc9ITAsdGhpcy5vbkluZmluaXRlLmNhbGwoKSk6dGhpcy5pc0xvYWRpbmc9ITF9fSxkZXN0cm95ZWQ6ZnVuY3Rpb24oKXt0aGlzLmlzQ29tcGxldGV8fHRoaXMuc2Nyb2xsUGFyZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIix0aGlzLnNjcm9sbEhhbmRsZXIpfX19LGZ1bmN0aW9uKHAseCx0KXt4PXAuZXhwb3J0cz10KDMpKCkseC5wdXNoKFtwLmlkLCcubG9hZGluZy13YXZlLWRvdHNbZGF0YS12LTUwNzkzZjAyXXtwb3NpdGlvbjpyZWxhdGl2ZX0ubG9hZGluZy13YXZlLWRvdHNbZGF0YS12LTUwNzkzZjAyXTpiZWZvcmV7Y29udGVudDpcIlwiO3Bvc2l0aW9uOmFic29sdXRlO3RvcDo1MCU7bGVmdDo1MCU7bWFyZ2luLWxlZnQ6LTRweDttYXJnaW4tdG9wOi00cHg7d2lkdGg6OHB4O2hlaWdodDo4cHg7YmFja2dyb3VuZC1jb2xvcjojYmJiO2JvcmRlci1yYWRpdXM6NTAlOy13ZWJraXQtYW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLXdhdmUtZG90cyAyLjhzIGluZmluaXRlO2FuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy13YXZlLWRvdHMgMi44cyBpbmZpbml0ZX1ALXdlYmtpdC1rZXlmcmFtZXMgbG9hZGluZy13YXZlLWRvdHN7MCV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmJ9NSV7Ym94LXNoYWRvdzotMzJweCAtNHB4IDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgwKX0xMCV7Ym94LXNoYWRvdzotMzJweCAtNnB4IDAgIzk5OSwtMTZweCAtNHB4IDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgwKX0xNSV7Ym94LXNoYWRvdzotMzJweCAycHggMCAjYmJiLC0xNnB4IC0ycHggMCAjOTk5LDE2cHggNHB4IDAgI2JiYiwzMnB4IDRweCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtNHB4KTtiYWNrZ3JvdW5kLWNvbG9yOiNiYmJ9MjAle2JveC1zaGFkb3c6LTMycHggNnB4IDAgI2JiYiwtMTZweCA0cHggMCAjYmJiLDE2cHggMnB4IDAgI2JiYiwzMnB4IDZweCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtNnB4KTtiYWNrZ3JvdW5kLWNvbG9yOiM5OTl9MjUle2JveC1zaGFkb3c6LTMycHggMnB4IDAgI2JiYiwtMTZweCAycHggMCAjYmJiLDE2cHggLTRweCAwICM5OTksMzJweCAtMnB4IDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC0ycHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC0ycHgpO2JhY2tncm91bmQtY29sb3I6I2JiYn0zMCV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IC0ycHggMCAjYmJiLDMycHggLTZweCAwICM5OTk7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgwKX0zNSV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggLTJweCAwICNiYmJ9NDAle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJifXRve2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJifX1Aa2V5ZnJhbWVzIGxvYWRpbmctd2F2ZS1kb3RzezAle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJifTUle2JveC1zaGFkb3c6LTMycHggLTRweCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MTAle2JveC1zaGFkb3c6LTMycHggLTZweCAwICM5OTksLTE2cHggLTRweCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MTUle2JveC1zaGFkb3c6LTMycHggMnB4IDAgI2JiYiwtMTZweCAtMnB4IDAgIzk5OSwxNnB4IDRweCAwICNiYmIsMzJweCA0cHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTRweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTRweCk7YmFja2dyb3VuZC1jb2xvcjojYmJifTIwJXtib3gtc2hhZG93Oi0zMnB4IDZweCAwICNiYmIsLTE2cHggNHB4IDAgI2JiYiwxNnB4IDJweCAwICNiYmIsMzJweCA2cHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTZweCk7YmFja2dyb3VuZC1jb2xvcjojOTk5fTI1JXtib3gtc2hhZG93Oi0zMnB4IDJweCAwICNiYmIsLTE2cHggMnB4IDAgI2JiYiwxNnB4IC00cHggMCAjOTk5LDMycHggLTJweCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtMnB4KTtiYWNrZ3JvdW5kLWNvbG9yOiNiYmJ9MzAle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAtMnB4IDAgI2JiYiwzMnB4IC02cHggMCAjOTk5Oy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MzUle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IC0ycHggMCAjYmJifTQwJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn10b3tib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn19LmxvYWRpbmctY2lyY2xlc1tkYXRhLXYtNTA3OTNmMDJde3Bvc2l0aW9uOnJlbGF0aXZlfS5sb2FkaW5nLWNpcmNsZXNbZGF0YS12LTUwNzkzZjAyXTpiZWZvcmV7Y29udGVudDpcIlwiO3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6NTAlO3RvcDo1MCU7bWFyZ2luLXRvcDotMi41cHg7bWFyZ2luLWxlZnQ6LTIuNXB4O3dpZHRoOjVweDtoZWlnaHQ6NXB4O2JvcmRlci1yYWRpdXM6NTAlOy13ZWJraXQtYW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLWNpcmNsZXMgLjc1cyBpbmZpbml0ZTthbmltYXRpb246bGluZWFyIGxvYWRpbmctY2lyY2xlcyAuNzVzIGluZmluaXRlfUAtd2Via2l0LWtleWZyYW1lcyBsb2FkaW5nLWNpcmNsZXN7MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzUwNTA1MCw4LjUycHggLTguNTJweCAwICM2NDY0NjQsMTJweCAwIDAgIzc5Nzk3OSw4LjUycHggOC41MnB4IDAgIzhkOGQ4ZCwwIDEycHggMCAjYTJhMmEyLC04LjUycHggOC41MnB4IDAgI2I2YjZiNiwtMTJweCAwIDAgI2NhY2FjYSwtOC41MnB4IC04LjUycHggMCAjZGZkZmRmfTEyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwICNkZmRmZGYsOC41MnB4IC04LjUycHggMCAjNTA1MDUwLDEycHggMCAwICM2NDY0NjQsOC41MnB4IDguNTJweCAwICM3OTc5NzksMCAxMnB4IDAgIzhkOGQ4ZCwtOC41MnB4IDguNTJweCAwICNhMmEyYTIsLTEycHggMCAwICNiNmI2YjYsLTguNTJweCAtOC41MnB4IDAgI2NhY2FjYX0yNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2NhY2FjYSw4LjUycHggLTguNTJweCAwICNkZmRmZGYsMTJweCAwIDAgIzUwNTA1MCw4LjUycHggOC41MnB4IDAgIzY0NjQ2NCwwIDEycHggMCAjNzk3OTc5LC04LjUycHggOC41MnB4IDAgIzhkOGQ4ZCwtMTJweCAwIDAgI2EyYTJhMiwtOC41MnB4IC04LjUycHggMCAjYjZiNmI2fTM3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwICNiNmI2YjYsOC41MnB4IC04LjUycHggMCAjY2FjYWNhLDEycHggMCAwICNkZmRmZGYsOC41MnB4IDguNTJweCAwICM1MDUwNTAsMCAxMnB4IDAgIzY0NjQ2NCwtOC41MnB4IDguNTJweCAwICM3OTc5NzksLTEycHggMCAwICM4ZDhkOGQsLTguNTJweCAtOC41MnB4IDAgI2EyYTJhMn01MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2EyYTJhMiw4LjUycHggLTguNTJweCAwICNiNmI2YjYsMTJweCAwIDAgI2NhY2FjYSw4LjUycHggOC41MnB4IDAgI2RmZGZkZiwwIDEycHggMCAjNTA1MDUwLC04LjUycHggOC41MnB4IDAgIzY0NjQ2NCwtMTJweCAwIDAgIzc5Nzk3OSwtOC41MnB4IC04LjUycHggMCAjOGQ4ZDhkfTYyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwICM4ZDhkOGQsOC41MnB4IC04LjUycHggMCAjYTJhMmEyLDEycHggMCAwICNiNmI2YjYsOC41MnB4IDguNTJweCAwICNjYWNhY2EsMCAxMnB4IDAgI2RmZGZkZiwtOC41MnB4IDguNTJweCAwICM1MDUwNTAsLTEycHggMCAwICM2NDY0NjQsLTguNTJweCAtOC41MnB4IDAgIzc5Nzk3OX03NSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzc5Nzk3OSw4LjUycHggLTguNTJweCAwICM4ZDhkOGQsMTJweCAwIDAgI2EyYTJhMiw4LjUycHggOC41MnB4IDAgI2I2YjZiNiwwIDEycHggMCAjY2FjYWNhLC04LjUycHggOC41MnB4IDAgI2RmZGZkZiwtMTJweCAwIDAgIzUwNTA1MCwtOC41MnB4IC04LjUycHggMCAjNjQ2NDY0fTg3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwICM2NDY0NjQsOC41MnB4IC04LjUycHggMCAjNzk3OTc5LDEycHggMCAwICM4ZDhkOGQsOC41MnB4IDguNTJweCAwICNhMmEyYTIsMCAxMnB4IDAgI2I2YjZiNiwtOC41MnB4IDguNTJweCAwICNjYWNhY2EsLTEycHggMCAwICNkZmRmZGYsLTguNTJweCAtOC41MnB4IDAgIzUwNTA1MH10b3tib3gtc2hhZG93OjAgLTEycHggMCAjNTA1MDUwLDguNTJweCAtOC41MnB4IDAgIzY0NjQ2NCwxMnB4IDAgMCAjNzk3OTc5LDguNTJweCA4LjUycHggMCAjOGQ4ZDhkLDAgMTJweCAwICNhMmEyYTIsLTguNTJweCA4LjUycHggMCAjYjZiNmI2LC0xMnB4IDAgMCAjY2FjYWNhLC04LjUycHggLTguNTJweCAwICNkZmRmZGZ9fUBrZXlmcmFtZXMgbG9hZGluZy1jaXJjbGVzezAle2JveC1zaGFkb3c6MCAtMTJweCAwICM1MDUwNTAsOC41MnB4IC04LjUycHggMCAjNjQ2NDY0LDEycHggMCAwICM3OTc5NzksOC41MnB4IDguNTJweCAwICM4ZDhkOGQsMCAxMnB4IDAgI2EyYTJhMiwtOC41MnB4IDguNTJweCAwICNiNmI2YjYsLTEycHggMCAwICNjYWNhY2EsLTguNTJweCAtOC41MnB4IDAgI2RmZGZkZn0xMi41JXtib3gtc2hhZG93OjAgLTEycHggMCAjZGZkZmRmLDguNTJweCAtOC41MnB4IDAgIzUwNTA1MCwxMnB4IDAgMCAjNjQ2NDY0LDguNTJweCA4LjUycHggMCAjNzk3OTc5LDAgMTJweCAwICM4ZDhkOGQsLTguNTJweCA4LjUycHggMCAjYTJhMmEyLC0xMnB4IDAgMCAjYjZiNmI2LC04LjUycHggLTguNTJweCAwICNjYWNhY2F9MjUle2JveC1zaGFkb3c6MCAtMTJweCAwICNjYWNhY2EsOC41MnB4IC04LjUycHggMCAjZGZkZmRmLDEycHggMCAwICM1MDUwNTAsOC41MnB4IDguNTJweCAwICM2NDY0NjQsMCAxMnB4IDAgIzc5Nzk3OSwtOC41MnB4IDguNTJweCAwICM4ZDhkOGQsLTEycHggMCAwICNhMmEyYTIsLTguNTJweCAtOC41MnB4IDAgI2I2YjZiNn0zNy41JXtib3gtc2hhZG93OjAgLTEycHggMCAjYjZiNmI2LDguNTJweCAtOC41MnB4IDAgI2NhY2FjYSwxMnB4IDAgMCAjZGZkZmRmLDguNTJweCA4LjUycHggMCAjNTA1MDUwLDAgMTJweCAwICM2NDY0NjQsLTguNTJweCA4LjUycHggMCAjNzk3OTc5LC0xMnB4IDAgMCAjOGQ4ZDhkLC04LjUycHggLTguNTJweCAwICNhMmEyYTJ9NTAle2JveC1zaGFkb3c6MCAtMTJweCAwICNhMmEyYTIsOC41MnB4IC04LjUycHggMCAjYjZiNmI2LDEycHggMCAwICNjYWNhY2EsOC41MnB4IDguNTJweCAwICNkZmRmZGYsMCAxMnB4IDAgIzUwNTA1MCwtOC41MnB4IDguNTJweCAwICM2NDY0NjQsLTEycHggMCAwICM3OTc5NzksLTguNTJweCAtOC41MnB4IDAgIzhkOGQ4ZH02Mi41JXtib3gtc2hhZG93OjAgLTEycHggMCAjOGQ4ZDhkLDguNTJweCAtOC41MnB4IDAgI2EyYTJhMiwxMnB4IDAgMCAjYjZiNmI2LDguNTJweCA4LjUycHggMCAjY2FjYWNhLDAgMTJweCAwICNkZmRmZGYsLTguNTJweCA4LjUycHggMCAjNTA1MDUwLC0xMnB4IDAgMCAjNjQ2NDY0LC04LjUycHggLTguNTJweCAwICM3OTc5Nzl9NzUle2JveC1zaGFkb3c6MCAtMTJweCAwICM3OTc5NzksOC41MnB4IC04LjUycHggMCAjOGQ4ZDhkLDEycHggMCAwICNhMmEyYTIsOC41MnB4IDguNTJweCAwICNiNmI2YjYsMCAxMnB4IDAgI2NhY2FjYSwtOC41MnB4IDguNTJweCAwICNkZmRmZGYsLTEycHggMCAwICM1MDUwNTAsLTguNTJweCAtOC41MnB4IDAgIzY0NjQ2NH04Ny41JXtib3gtc2hhZG93OjAgLTEycHggMCAjNjQ2NDY0LDguNTJweCAtOC41MnB4IDAgIzc5Nzk3OSwxMnB4IDAgMCAjOGQ4ZDhkLDguNTJweCA4LjUycHggMCAjYTJhMmEyLDAgMTJweCAwICNiNmI2YjYsLTguNTJweCA4LjUycHggMCAjY2FjYWNhLC0xMnB4IDAgMCAjZGZkZmRmLC04LjUycHggLTguNTJweCAwICM1MDUwNTB9dG97Ym94LXNoYWRvdzowIC0xMnB4IDAgIzUwNTA1MCw4LjUycHggLTguNTJweCAwICM2NDY0NjQsMTJweCAwIDAgIzc5Nzk3OSw4LjUycHggOC41MnB4IDAgIzhkOGQ4ZCwwIDEycHggMCAjYTJhMmEyLC04LjUycHggOC41MnB4IDAgI2I2YjZiNiwtMTJweCAwIDAgI2NhY2FjYSwtOC41MnB4IC04LjUycHggMCAjZGZkZmRmfX0ubG9hZGluZy1idWJibGVzW2RhdGEtdi01MDc5M2YwMl17cG9zaXRpb246cmVsYXRpdmV9LmxvYWRpbmctYnViYmxlc1tkYXRhLXYtNTA3OTNmMDJdOmJlZm9yZXtjb250ZW50OlwiXCI7cG9zaXRpb246YWJzb2x1dGU7bGVmdDo1MCU7dG9wOjUwJTttYXJnaW4tdG9wOi0uNXB4O21hcmdpbi1sZWZ0Oi0uNXB4O3dpZHRoOjFweDtoZWlnaHQ6MXB4O2JvcmRlci1yYWRpdXM6NTAlOy13ZWJraXQtYW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLWJ1YmJsZXMgLjg1cyBpbmZpbml0ZTthbmltYXRpb246bGluZWFyIGxvYWRpbmctYnViYmxlcyAuODVzIGluZmluaXRlfUAtd2Via2l0LWtleWZyYW1lcyBsb2FkaW5nLWJ1YmJsZXN7MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgLjRweCAjNjY2LDguNTJweCAtOC41MnB4IDAgLjhweCAjNjY2LDEycHggMCAwIDEuMnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDEuNnB4ICM2NjYsMCAxMnB4IDAgMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAyLjRweCAjNjY2LC0xMnB4IDAgMCAyLjhweCAjNjY2LC04LjUycHggLTguNTJweCAwIDMuMnB4ICM2NjZ9MTIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMy4ycHggIzY2Niw4LjUycHggLTguNTJweCAwIC40cHggIzY2NiwxMnB4IDAgMCAuOHB4ICM2NjYsOC41MnB4IDguNTJweCAwIDEuMnB4ICM2NjYsMCAxMnB4IDAgMS42cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDJweCAjNjY2LC0xMnB4IDAgMCAyLjRweCAjNjY2LC04LjUycHggLTguNTJweCAwIDIuOHB4ICM2NjZ9MjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDIuOHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAzLjJweCAjNjY2LDEycHggMCAwIC40cHggIzY2Niw4LjUycHggOC41MnB4IDAgLjhweCAjNjY2LDAgMTJweCAwIDEuMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAxLjZweCAjNjY2LC0xMnB4IDAgMCAycHggIzY2NiwtOC41MnB4IC04LjUycHggMCAyLjRweCAjNjY2fTM3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDIuNHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAyLjhweCAjNjY2LDEycHggMCAwIDMuMnB4ICM2NjYsOC41MnB4IDguNTJweCAwIC40cHggIzY2NiwwIDEycHggMCAuOHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAxLjJweCAjNjY2LC0xMnB4IDAgMCAxLjZweCAjNjY2LC04LjUycHggLTguNTJweCAwIDJweCAjNjY2fTUwJXtib3gtc2hhZG93OjAgLTEycHggMCAycHggIzY2Niw4LjUycHggLTguNTJweCAwIDIuNHB4ICM2NjYsMTJweCAwIDAgMi44cHggIzY2Niw4LjUycHggOC41MnB4IDAgMy4ycHggIzY2NiwwIDEycHggMCAuNHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAuOHB4ICM2NjYsLTEycHggMCAwIDEuMnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMS42cHggIzY2Nn02Mi41JXtib3gtc2hhZG93OjAgLTEycHggMCAxLjZweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMnB4ICM2NjYsMTJweCAwIDAgMi40cHggIzY2Niw4LjUycHggOC41MnB4IDAgMi44cHggIzY2NiwwIDEycHggMCAzLjJweCAjNjY2LC04LjUycHggOC41MnB4IDAgLjRweCAjNjY2LC0xMnB4IDAgMCAuOHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMS4ycHggIzY2Nn03NSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMS4ycHggIzY2Niw4LjUycHggLTguNTJweCAwIDEuNnB4ICM2NjYsMTJweCAwIDAgMnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDIuNHB4ICM2NjYsMCAxMnB4IDAgMi44cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDMuMnB4ICM2NjYsLTEycHggMCAwIC40cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAuOHB4ICM2NjZ9ODcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgLjhweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMS4ycHggIzY2NiwxMnB4IDAgMCAxLjZweCAjNjY2LDguNTJweCA4LjUycHggMCAycHggIzY2NiwwIDEycHggMCAyLjRweCAjNjY2LC04LjUycHggOC41MnB4IDAgMi44cHggIzY2NiwtMTJweCAwIDAgMy4ycHggIzY2NiwtOC41MnB4IC04LjUycHggMCAuNHB4ICM2NjZ9dG97Ym94LXNoYWRvdzowIC0xMnB4IDAgLjRweCAjNjY2LDguNTJweCAtOC41MnB4IDAgLjhweCAjNjY2LDEycHggMCAwIDEuMnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDEuNnB4ICM2NjYsMCAxMnB4IDAgMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAyLjRweCAjNjY2LC0xMnB4IDAgMCAyLjhweCAjNjY2LC04LjUycHggLTguNTJweCAwIDMuMnB4ICM2NjZ9fUBrZXlmcmFtZXMgbG9hZGluZy1idWJibGVzezAle2JveC1zaGFkb3c6MCAtMTJweCAwIC40cHggIzY2Niw4LjUycHggLTguNTJweCAwIC44cHggIzY2NiwxMnB4IDAgMCAxLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjZweCAjNjY2LDAgMTJweCAwIDJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwtMTJweCAwIDAgMi44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAzLjJweCAjNjY2fTEyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDMuMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuNHB4ICM2NjYsMTJweCAwIDAgLjhweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjJweCAjNjY2LDAgMTJweCAwIDEuNnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAycHggIzY2NiwtMTJweCAwIDAgMi40cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAyLjhweCAjNjY2fTI1JXtib3gtc2hhZG93OjAgLTEycHggMCAyLjhweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2NiwxMnB4IDAgMCAuNHB4ICM2NjYsOC41MnB4IDguNTJweCAwIC44cHggIzY2NiwwIDEycHggMCAxLjJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwtMTJweCAwIDAgMnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMi40cHggIzY2Nn0zNy41JXtib3gtc2hhZG93OjAgLTEycHggMCAyLjRweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMi44cHggIzY2NiwxMnB4IDAgMCAzLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAuNHB4ICM2NjYsMCAxMnB4IDAgLjhweCAjNjY2LC04LjUycHggOC41MnB4IDAgMS4ycHggIzY2NiwtMTJweCAwIDAgMS42cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAycHggIzY2Nn01MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAyLjRweCAjNjY2LDEycHggMCAwIDIuOHB4ICM2NjYsOC41MnB4IDguNTJweCAwIDMuMnB4ICM2NjYsMCAxMnB4IDAgLjRweCAjNjY2LC04LjUycHggOC41MnB4IDAgLjhweCAjNjY2LC0xMnB4IDAgMCAxLjJweCAjNjY2LC04LjUycHggLTguNTJweCAwIDEuNnB4ICM2NjZ9NjIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMS42cHggIzY2Niw4LjUycHggLTguNTJweCAwIDJweCAjNjY2LDEycHggMCAwIDIuNHB4ICM2NjYsOC41MnB4IDguNTJweCAwIDIuOHB4ICM2NjYsMCAxMnB4IDAgMy4ycHggIzY2NiwtOC41MnB4IDguNTJweCAwIC40cHggIzY2NiwtMTJweCAwIDAgLjhweCAjNjY2LC04LjUycHggLTguNTJweCAwIDEuMnB4ICM2NjZ9NzUle2JveC1zaGFkb3c6MCAtMTJweCAwIDEuMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAxLjZweCAjNjY2LDEycHggMCAwIDJweCAjNjY2LDguNTJweCA4LjUycHggMCAyLjRweCAjNjY2LDAgMTJweCAwIDIuOHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAzLjJweCAjNjY2LC0xMnB4IDAgMCAuNHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgLjhweCAjNjY2fTg3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwIC44cHggIzY2Niw4LjUycHggLTguNTJweCAwIDEuMnB4ICM2NjYsMTJweCAwIDAgMS42cHggIzY2Niw4LjUycHggOC41MnB4IDAgMnB4ICM2NjYsMCAxMnB4IDAgMi40cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuOHB4ICM2NjYsLTEycHggMCAwIDMuMnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgLjRweCAjNjY2fXRve2JveC1zaGFkb3c6MCAtMTJweCAwIC40cHggIzY2Niw4LjUycHggLTguNTJweCAwIC44cHggIzY2NiwxMnB4IDAgMCAxLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjZweCAjNjY2LDAgMTJweCAwIDJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwtMTJweCAwIDAgMi44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAzLjJweCAjNjY2fX0ubG9hZGluZy1kZWZhdWx0W2RhdGEtdi01MDc5M2YwMl17cG9zaXRpb246cmVsYXRpdmU7Ym9yZGVyOjFweCBzb2xpZCAjOTk5Oy13ZWJraXQtYW5pbWF0aW9uOmVhc2UgbG9hZGluZy1yb3RhdGluZyAxLjVzIGluZmluaXRlO2FuaW1hdGlvbjplYXNlIGxvYWRpbmctcm90YXRpbmcgMS41cyBpbmZpbml0ZX0ubG9hZGluZy1kZWZhdWx0W2RhdGEtdi01MDc5M2YwMl06YmVmb3Jle2NvbnRlbnQ6XCJcIjtwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5OmJsb2NrO3RvcDowO2xlZnQ6NTAlO21hcmdpbi10b3A6LTNweDttYXJnaW4tbGVmdDotM3B4O3dpZHRoOjZweDtoZWlnaHQ6NnB4O2JhY2tncm91bmQtY29sb3I6Izk5OTtib3JkZXItcmFkaXVzOjUwJX0ubG9hZGluZy1zcGlyYWxbZGF0YS12LTUwNzkzZjAyXXtib3JkZXI6MnB4IHNvbGlkICM3Nzc7Ym9yZGVyLXJpZ2h0LWNvbG9yOnRyYW5zcGFyZW50Oy13ZWJraXQtYW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLXJvdGF0aW5nIC44NXMgaW5maW5pdGU7YW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLXJvdGF0aW5nIC44NXMgaW5maW5pdGV9QC13ZWJraXQta2V5ZnJhbWVzIGxvYWRpbmctcm90YXRpbmd7MCV7LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDApO3RyYW5zZm9ybTpyb3RhdGUoMCl9dG97LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDF0dXJuKTt0cmFuc2Zvcm06cm90YXRlKDF0dXJuKX19QGtleWZyYW1lcyBsb2FkaW5nLXJvdGF0aW5nezAley13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgwKTt0cmFuc2Zvcm06cm90YXRlKDApfXRvey13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgxdHVybik7dHJhbnNmb3JtOnJvdGF0ZSgxdHVybil9fS5pbmZpbml0ZS1sb2FkaW5nLWNvbnRhaW5lcltkYXRhLXYtNTA3OTNmMDJde2NsZWFyOmJvdGg7dGV4dC1hbGlnbjpjZW50ZXJ9LmluZmluaXRlLWxvYWRpbmctY29udGFpbmVyIFtjbGFzc149bG9hZGluZy1dW2RhdGEtdi01MDc5M2YwMl17ZGlzcGxheTppbmxpbmUtYmxvY2s7bWFyZ2luOjE1cHggMDt3aWR0aDoyOHB4O2hlaWdodDoyOHB4O2ZvbnQtc2l6ZToyOHB4O2xpbmUtaGVpZ2h0OjI4cHg7Ym9yZGVyLXJhZGl1czo1MCV9LmluZmluaXRlLXN0YXR1cy1wcm9tcHRbZGF0YS12LTUwNzkzZjAyXXtjb2xvcjojNjY2O2ZvbnQtc2l6ZToxNHB4O3RleHQtYWxpZ246Y2VudGVyO3BhZGRpbmc6MTBweCAwfScsXCJcIl0pfSxmdW5jdGlvbihwLHgpe3AuZXhwb3J0cz1mdW5jdGlvbigpe3ZhciBwPVtdO3JldHVybiBwLnRvU3RyaW5nPWZ1bmN0aW9uKCl7Zm9yKHZhciBwPVtdLHg9MDt4PHRoaXMubGVuZ3RoO3grKyl7dmFyIHQ9dGhpc1t4XTt0WzJdP3AucHVzaChcIkBtZWRpYSBcIit0WzJdK1wie1wiK3RbMV0rXCJ9XCIpOnAucHVzaCh0WzFdKX1yZXR1cm4gcC5qb2luKFwiXCIpfSxwLmk9ZnVuY3Rpb24oeCx0KXtcInN0cmluZ1wiPT10eXBlb2YgeCYmKHg9W1tudWxsLHgsXCJcIl1dKTtmb3IodmFyIGE9e30sZT0wO2U8dGhpcy5sZW5ndGg7ZSsrKXt2YXIgbz10aGlzW2VdWzBdO1wibnVtYmVyXCI9PXR5cGVvZiBvJiYoYVtvXT0hMCl9Zm9yKGU9MDtlPHgubGVuZ3RoO2UrKyl7dmFyIG49eFtlXTtcIm51bWJlclwiPT10eXBlb2YgblswXSYmYVtuWzBdXXx8KHQmJiFuWzJdP25bMl09dDp0JiYoblsyXT1cIihcIituWzJdK1wiKSBhbmQgKFwiK3QrXCIpXCIpLHAucHVzaChuKSl9fSxwfX0sZnVuY3Rpb24ocCx4LHQpe3ZhciBhLGU7dCg3KSxhPXQoMSk7dmFyIG89dCg1KTtlPWE9YXx8e30sXCJvYmplY3RcIiE9dHlwZW9mIGEuZGVmYXVsdCYmXCJmdW5jdGlvblwiIT10eXBlb2YgYS5kZWZhdWx0fHwoZT1hPWEuZGVmYXVsdCksXCJmdW5jdGlvblwiPT10eXBlb2YgZSYmKGU9ZS5vcHRpb25zKSxlLnJlbmRlcj1vLnJlbmRlcixlLnN0YXRpY1JlbmRlckZucz1vLnN0YXRpY1JlbmRlckZucyxlLl9zY29wZUlkPVwiZGF0YS12LTUwNzkzZjAyXCIscC5leHBvcnRzPWF9LGZ1bmN0aW9uKHAseCl7cC5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24oKXt2YXIgcD10aGlzLHg9cC4kY3JlYXRlRWxlbWVudCx0PXAuX3NlbGYuX2N8fHg7cmV0dXJuIHQoXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJpbmZpbml0ZS1sb2FkaW5nLWNvbnRhaW5lclwifSxbdChcImRpdlwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6cC5pc0xvYWRpbmcsZXhwcmVzc2lvbjpcImlzTG9hZGluZ1wifV19LFtwLl90KFwic3Bpbm5lclwiLFt0KFwiaVwiLHtjbGFzczpwLnNwaW5uZXJUeXBlfSldKV0sMikscC5fdihcIiBcIiksdChcImRpdlwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6IXAuaXNMb2FkaW5nJiZwLmlzQ29tcGxldGUmJnAuaXNGaXJzdExvYWQsZXhwcmVzc2lvbjpcIiFpc0xvYWRpbmcgJiYgaXNDb21wbGV0ZSAmJiBpc0ZpcnN0TG9hZFwifV0sc3RhdGljQ2xhc3M6XCJpbmZpbml0ZS1zdGF0dXMtcHJvbXB0XCJ9LFtwLl90KFwibm8tcmVzdWx0c1wiLFtwLl92KFwiTm8gcmVzdWx0cyA6KFwiKV0pXSwyKSxwLl92KFwiIFwiKSx0KFwiZGl2XCIse2RpcmVjdGl2ZXM6W3tuYW1lOlwic2hvd1wiLHJhd05hbWU6XCJ2LXNob3dcIix2YWx1ZTohcC5pc0xvYWRpbmcmJnAuaXNDb21wbGV0ZSYmIXAuaXNGaXJzdExvYWQsZXhwcmVzc2lvbjpcIiFpc0xvYWRpbmcgJiYgaXNDb21wbGV0ZSAmJiAhaXNGaXJzdExvYWRcIn1dLHN0YXRpY0NsYXNzOlwiaW5maW5pdGUtc3RhdHVzLXByb21wdFwifSxbcC5fdChcIm5vLW1vcmVcIixbcC5fdihcIk5vIG1vcmUgZGF0YSA6KVwiKV0pXSwyKV0pfSxzdGF0aWNSZW5kZXJGbnM6W119fSxmdW5jdGlvbihwLHgsdCl7ZnVuY3Rpb24gYShwLHgpe2Zvcih2YXIgdD0wO3Q8cC5sZW5ndGg7dCsrKXt2YXIgYT1wW3RdLGU9ZFthLmlkXTtpZihlKXtlLnJlZnMrKztmb3IodmFyIG89MDtvPGUucGFydHMubGVuZ3RoO28rKyllLnBhcnRzW29dKGEucGFydHNbb10pO2Zvcig7bzxhLnBhcnRzLmxlbmd0aDtvKyspZS5wYXJ0cy5wdXNoKHIoYS5wYXJ0c1tvXSx4KSl9ZWxzZXtmb3IodmFyIG49W10sbz0wO288YS5wYXJ0cy5sZW5ndGg7bysrKW4ucHVzaChyKGEucGFydHNbb10seCkpO2RbYS5pZF09e2lkOmEuaWQscmVmczoxLHBhcnRzOm59fX19ZnVuY3Rpb24gZShwKXtmb3IodmFyIHg9W10sdD17fSxhPTA7YTxwLmxlbmd0aDthKyspe3ZhciBlPXBbYV0sbz1lWzBdLG49ZVsxXSxpPWVbMl0scj1lWzNdLHM9e2NzczpuLG1lZGlhOmksc291cmNlTWFwOnJ9O3Rbb10/dFtvXS5wYXJ0cy5wdXNoKHMpOngucHVzaCh0W29dPXtpZDpvLHBhcnRzOltzXX0pfXJldHVybiB4fWZ1bmN0aW9uIG8ocCx4KXt2YXIgdD1jKCksYT1tW20ubGVuZ3RoLTFdO2lmKFwidG9wXCI9PT1wLmluc2VydEF0KWE/YS5uZXh0U2libGluZz90Lmluc2VydEJlZm9yZSh4LGEubmV4dFNpYmxpbmcpOnQuYXBwZW5kQ2hpbGQoeCk6dC5pbnNlcnRCZWZvcmUoeCx0LmZpcnN0Q2hpbGQpLG0ucHVzaCh4KTtlbHNle2lmKFwiYm90dG9tXCIhPT1wLmluc2VydEF0KXRocm93IG5ldyBFcnJvcihcIkludmFsaWQgdmFsdWUgZm9yIHBhcmFtZXRlciAnaW5zZXJ0QXQnLiBNdXN0IGJlICd0b3AnIG9yICdib3R0b20nLlwiKTt0LmFwcGVuZENoaWxkKHgpfX1mdW5jdGlvbiBuKHApe3AucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChwKTt2YXIgeD1tLmluZGV4T2YocCk7eD49MCYmbS5zcGxpY2UoeCwxKX1mdW5jdGlvbiBpKHApe3ZhciB4PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzdHlsZVwiKTtyZXR1cm4geC50eXBlPVwidGV4dC9jc3NcIixvKHAseCkseH1mdW5jdGlvbiByKHAseCl7dmFyIHQsYSxlO2lmKHguc2luZ2xldG9uKXt2YXIgbz1oKys7dD11fHwodT1pKHgpKSxhPXMuYmluZChudWxsLHQsbywhMSksZT1zLmJpbmQobnVsbCx0LG8sITApfWVsc2UgdD1pKHgpLGE9Yi5iaW5kKG51bGwsdCksZT1mdW5jdGlvbigpe24odCl9O3JldHVybiBhKHApLGZ1bmN0aW9uKHgpe2lmKHgpe2lmKHguY3NzPT09cC5jc3MmJngubWVkaWE9PT1wLm1lZGlhJiZ4LnNvdXJjZU1hcD09PXAuc291cmNlTWFwKXJldHVybjthKHA9eCl9ZWxzZSBlKCl9fWZ1bmN0aW9uIHMocCx4LHQsYSl7dmFyIGU9dD9cIlwiOmEuY3NzO2lmKHAuc3R5bGVTaGVldClwLnN0eWxlU2hlZXQuY3NzVGV4dD1nKHgsZSk7ZWxzZXt2YXIgbz1kb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShlKSxuPXAuY2hpbGROb2RlcztuW3hdJiZwLnJlbW92ZUNoaWxkKG5beF0pLG4ubGVuZ3RoP3AuaW5zZXJ0QmVmb3JlKG8sblt4XSk6cC5hcHBlbmRDaGlsZChvKX19ZnVuY3Rpb24gYihwLHgpe3ZhciB0PXguY3NzLGE9eC5tZWRpYSxlPXguc291cmNlTWFwO2lmKGEmJnAuc2V0QXR0cmlidXRlKFwibWVkaWFcIixhKSxlJiYodCs9XCJcXG4vKiMgc291cmNlVVJMPVwiK2Uuc291cmNlc1swXStcIiAqL1wiLHQrPVwiXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCxcIitidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShlKSkpKStcIiAqL1wiKSxwLnN0eWxlU2hlZXQpcC5zdHlsZVNoZWV0LmNzc1RleHQ9dDtlbHNle2Zvcig7cC5maXJzdENoaWxkOylwLnJlbW92ZUNoaWxkKHAuZmlyc3RDaGlsZCk7cC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSh0KSl9fXZhciBkPXt9LGw9ZnVuY3Rpb24ocCl7dmFyIHg7cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuXCJ1bmRlZmluZWRcIj09dHlwZW9mIHgmJih4PXAuYXBwbHkodGhpcyxhcmd1bWVudHMpKSx4fX0sZj1sKGZ1bmN0aW9uKCl7cmV0dXJuL21zaWUgWzYtOV1cXGIvLnRlc3Qod2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSl9KSxjPWwoZnVuY3Rpb24oKXtyZXR1cm4gZG9jdW1lbnQuaGVhZHx8ZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJoZWFkXCIpWzBdfSksdT1udWxsLGg9MCxtPVtdO3AuZXhwb3J0cz1mdW5jdGlvbihwLHgpe3g9eHx8e30sXCJ1bmRlZmluZWRcIj09dHlwZW9mIHguc2luZ2xldG9uJiYoeC5zaW5nbGV0b249ZigpKSxcInVuZGVmaW5lZFwiPT10eXBlb2YgeC5pbnNlcnRBdCYmKHguaW5zZXJ0QXQ9XCJib3R0b21cIik7dmFyIHQ9ZShwKTtyZXR1cm4gYSh0LHgpLGZ1bmN0aW9uKHApe2Zvcih2YXIgbz1bXSxuPTA7bjx0Lmxlbmd0aDtuKyspe3ZhciBpPXRbbl0scj1kW2kuaWRdO3IucmVmcy0tLG8ucHVzaChyKX1pZihwKXt2YXIgcz1lKHApO2Eocyx4KX1mb3IodmFyIG49MDtuPG8ubGVuZ3RoO24rKyl7dmFyIHI9b1tuXTtpZigwPT09ci5yZWZzKXtmb3IodmFyIGI9MDtiPHIucGFydHMubGVuZ3RoO2IrKylyLnBhcnRzW2JdKCk7ZGVsZXRlIGRbci5pZF19fX19O3ZhciBnPWZ1bmN0aW9uKCl7dmFyIHA9W107cmV0dXJuIGZ1bmN0aW9uKHgsdCl7cmV0dXJuIHBbeF09dCxwLmZpbHRlcihCb29sZWFuKS5qb2luKFwiXFxuXCIpfX0oKX0sZnVuY3Rpb24ocCx4LHQpe3ZhciBhPXQoMik7XCJzdHJpbmdcIj09dHlwZW9mIGEmJihhPVtbcC5pZCxhLFwiXCJdXSk7dCg2KShhLHt9KTthLmxvY2FscyYmKHAuZXhwb3J0cz1hLmxvY2Fscyl9XSl9KTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWluZmluaXRlLWxvYWRpbmcvZGlzdC92dWUtaW5maW5pdGUtbG9hZGluZy5qc1xuLy8gbW9kdWxlIGlkID0gNlxuLy8gbW9kdWxlIGNodW5rcyA9IDQgOCAxNiJdLCJzb3VyY2VSb290IjoiIn0=