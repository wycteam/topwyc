/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 425);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-653e7369", module.exports)
  }
}

/***/ }),

/***/ 175:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('compare-list', __webpack_require__(339));
Vue.component('comp-modal', __webpack_require__(9));

Vue.filter('currency', function (value) {
	return '₱' + numberWithCommas(parseFloat(value).toFixed(2));
});

var app = new Vue({

	el: '#compare-content',
	data: {
		items: [],
		no_item: '',
		id: '',
		index: '',
		lang: []
	},
	created: function created() {
		var _this = this;

		function getTranslation() {
			return axios.get('/translate/compare');
		}

		function getCompare() {
			return axiosAPIv1.get('/compare');
		}

		axios.all([getTranslation(), getCompare()]).then(axios.spread(function (translation, compare) {

			_this.lang = translation.data.data;

			if (compare.data.data != 0) _this.items = compare.status == 200 ? compare.data : [];else $('#no-item').show(); //this.no_item = "There are no items in your comparison";
		})).catch($.noop);
	},

	methods: {
		showRemoveDialog: function showRemoveDialog(id, index) {
			this.id = id;
			this.index = index;
			$('#dialogRemove').modal('toggle');
		},
		deleteCompare: function deleteCompare() {
			var _this2 = this;

			axiosAPIv1.get('/deletecompare/' + this.id).then(function (result) {
				$("#comp-count").text(result.data);
				toastr.success(_this2.lang['removed'], _this2.items.data[_this2.index].name);
				_this2.items.data.splice(_this2.index, 1);
				if (_this2.items.data.length == 0) $('#no-item').show();
			});
		},
		addtocart: function addtocart(id, index) {
			var _this3 = this;

			axios.get(location.origin + '/cart/add', {
				params: {
					prod_id: $('#prod_id' + id).val(),
					qty: $('#qty' + id).val()
				}
			}).then(function (result) {
				$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
				if (result.data.error_msg) {
					if (result.data.error_msg == 'Redirect') window.location = '/product/' + result.data.slug;else toastr.error(result.data.error_msg, _this3.lang['not-added']);
				} else {
					toastr.success(_this3.lang['added'], _this3.items.data[index].name);
					_this3.items.data.splice(index, 1);
					if (_this3.items.data.length == 0) $('#no-item').show();
					axiosAPIv1.get('/deletecompare/' + id).then(function (result) {
						$("#comp-count").text(result.data);
					});
				}
			});
		}
	}

});

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['items'],
	methods: {
		showRemoveDialog: function showRemoveDialog(id, index) {
			this.$parent.showRemoveDialog(id, index);
		},
		addtocart: function addtocart(id, index) {
			this.$parent.addtocart(id, index);
		}
	}
});

/***/ }),

/***/ 339:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(274),
  /* template */
  __webpack_require__(407),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\pages\\Compare.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Compare.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-fbd33d84", Component.options)
  } else {
    hotAPI.reload("data-v-fbd33d84", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 407:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', _vm._l((_vm.items.data), function(item, index) {
    return _c('div', {
      key: item.id,
      staticClass: "col-md-4 col-sm-6",
      attrs: {
        "id": 'div' + item.id
      }
    }, [_c('div', {
      staticClass: "card"
    }, [_c('a', {
      attrs: {
        "href": '/product/' + item.slug
      }
    }, [_c('img', {
      staticClass: "card-img-top",
      attrs: {
        "src": item.image,
        "alt": ""
      }
    })]), _vm._v(" "), _c('ul', {
      staticClass: "list-group list-group-flush"
    }, [_c('li', {
      staticClass: "list-group-item"
    }, [_c('a', {
      staticClass: "comp-name",
      attrs: {
        "href": '/product/' + item.slug
      }
    }, [_vm._v(_vm._s(item.name))])]), _vm._v(" "), _c('li', {
      staticClass: "list-group-item"
    }, [_vm._v(_vm._s(_vm._f("currency")(item.price)))]), _vm._v(" "), _c('li', {
      staticClass: "list-group-item margin1"
    }, [_c('span', {
      staticClass: "font"
    }, [_vm._v("Stock:")]), _vm._v(" " + _vm._s(item.stock))]), _vm._v(" "), _c('li', {
      staticClass: "list-group-item"
    }, [_c('div', {
      staticClass: "rating-block"
    }, [_c('div', {
      staticClass: "star-ratings-sprite"
    }, [_c('span', {
      staticClass: "star-ratings-sprite-rating",
      style: (item.rating)
    })])])]), _vm._v(" "), _c('li', {
      staticClass: "list-group-item margin2"
    }, [_c('span', {
      staticClass: "font"
    }, [_vm._v("Description:")]), _vm._v(" " + _vm._s(item.desc))]), _vm._v(" "), _c('li', {
      staticClass: "list-group-item"
    }, [_c('a', {
      staticClass: "btn btn-raised btn-primary comp-btn",
      attrs: {
        "href": "javascript:void(0)"
      },
      on: {
        "click": function($event) {
          _vm.addtocart(item.id, index)
        }
      }
    }, [_vm._v("ADD TO CART"), _c('div', {
      staticClass: "ripple-container"
    })])]), _vm._v(" "), _c('li', {
      staticClass: "list-group-item"
    }, [_c('a', {
      staticClass: "btn btn-raised btn-primary comp-btn",
      attrs: {
        "href": "javascript:void(0)"
      },
      on: {
        "click": function($event) {
          _vm.showRemoveDialog(item.id, index)
        }
      }
    }, [_vm._v("REMOVE"), _c('div', {
      staticClass: "ripple-container"
    })])]), _vm._v(" "), _c('input', {
      attrs: {
        "id": 'prod_id' + item.id,
        "name": 'prod_id' + item.id,
        "type": "hidden"
      },
      domProps: {
        "value": item.prod_id
      }
    }), _vm._v(" "), _c('input', {
      attrs: {
        "id": 'qty' + item.id,
        "name": 'qty' + item.id,
        "type": "hidden",
        "value": "1"
      }
    })])])])
  }))
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-fbd33d84", module.exports)
  }
}

/***/ }),

/***/ 425:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(175);


/***/ }),

/***/ 7:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-sm' }
    }
});

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(7),
  /* template */
  __webpack_require__(12),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-653e7369", Component.options)
  } else {
    hotAPI.reload("data-v-653e7369", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWU/MDQzMioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvY29tcGFyZS5qcyIsIndlYnBhY2s6Ly8vQ29tcGFyZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3BhZ2VzL0NvbXBhcmUudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wYWdlcy9Db21wYXJlLnZ1ZT81MmE5Iiwid2VicGFjazovLy9EaWFsb2dNb2RhbC52dWU/MjZkMSoiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT8zM2NhKiJdLCJuYW1lcyI6WyJWdWUiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwiZmlsdGVyIiwidmFsdWUiLCJudW1iZXJXaXRoQ29tbWFzIiwicGFyc2VGbG9hdCIsInRvRml4ZWQiLCJhcHAiLCJlbCIsImRhdGEiLCJpdGVtcyIsIm5vX2l0ZW0iLCJpZCIsImluZGV4IiwibGFuZyIsImNyZWF0ZWQiLCJnZXRUcmFuc2xhdGlvbiIsImF4aW9zIiwiZ2V0IiwiZ2V0Q29tcGFyZSIsImF4aW9zQVBJdjEiLCJhbGwiLCJ0aGVuIiwic3ByZWFkIiwidHJhbnNsYXRpb24iLCJjb21wYXJlIiwic3RhdHVzIiwiJCIsInNob3ciLCJjYXRjaCIsIm5vb3AiLCJtZXRob2RzIiwic2hvd1JlbW92ZURpYWxvZyIsIm1vZGFsIiwiZGVsZXRlQ29tcGFyZSIsInRleHQiLCJyZXN1bHQiLCJ0b2FzdHIiLCJzdWNjZXNzIiwibmFtZSIsInNwbGljZSIsImxlbmd0aCIsImFkZHRvY2FydCIsImxvY2F0aW9uIiwib3JpZ2luIiwicGFyYW1zIiwicHJvZF9pZCIsInZhbCIsInF0eSIsImNhcnRfaXRlbV9jb3VudCIsImVycm9yX21zZyIsIndpbmRvdyIsInNsdWciLCJlcnJvciIsIngiLCJ0b1N0cmluZyIsInJlcGxhY2UiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pDQUEsSUFBSUMsU0FBSixDQUFjLGNBQWQsRUFBK0IsbUJBQUFDLENBQVEsR0FBUixDQUEvQjtBQUNBRixJQUFJQyxTQUFKLENBQWMsWUFBZCxFQUE2QixtQkFBQUMsQ0FBUSxDQUFSLENBQTdCOztBQUVBRixJQUFJRyxNQUFKLENBQVcsVUFBWCxFQUF1QixVQUFVQyxLQUFWLEVBQWlCO0FBQ3BDLFFBQVEsTUFBSUMsaUJBQWlCQyxXQUFXRixLQUFYLEVBQWtCRyxPQUFsQixDQUEwQixDQUExQixDQUFqQixDQUFaO0FBQ0gsQ0FGRDs7QUFJQSxJQUFJQyxNQUFNLElBQUlSLEdBQUosQ0FBUTs7QUFFakJTLEtBQUksa0JBRmE7QUFHakJDLE9BQU07QUFDTEMsU0FBTSxFQUREO0FBRUxDLFdBQVMsRUFGSjtBQUdMQyxNQUFHLEVBSEU7QUFJTEMsU0FBTSxFQUpEO0FBS0xDLFFBQUs7QUFMQSxFQUhXO0FBVWpCQyxRQVZpQixxQkFVUjtBQUFBOztBQUNSLFdBQVNDLGNBQVQsR0FBMEI7QUFDaEIsVUFBT0MsTUFBTUMsR0FBTixDQUFVLG9CQUFWLENBQVA7QUFDSDs7QUFFUCxXQUFTQyxVQUFULEdBQXNCO0FBQ3JCLFVBQU9DLFdBQVdGLEdBQVgsQ0FBZSxVQUFmLENBQVA7QUFDQTs7QUFFREQsUUFBTUksR0FBTixDQUFVLENBQ1RMLGdCQURTLEVBRVRHLFlBRlMsQ0FBVixFQUdHRyxJQUhILENBR1FMLE1BQU1NLE1BQU4sQ0FDUCxVQUNDQyxXQURELEVBRUNDLE9BRkQsRUFHSzs7QUFFTCxTQUFLWCxJQUFMLEdBQVlVLFlBQVlmLElBQVosQ0FBaUJBLElBQTdCOztBQUVBLE9BQUdnQixRQUFRaEIsSUFBUixDQUFhQSxJQUFiLElBQXFCLENBQXhCLEVBQ0MsTUFBS0MsS0FBTCxHQUFhZSxRQUFRQyxNQUFSLElBQWtCLEdBQWxCLEdBQXVCRCxRQUFRaEIsSUFBL0IsR0FBb0MsRUFBakQsQ0FERCxLQUdDa0IsRUFBRSxVQUFGLEVBQWNDLElBQWQsR0FQSSxDQU9rQjtBQUN2QixHQVpPLENBSFIsRUFnQkNDLEtBaEJELENBZ0JPRixFQUFFRyxJQWhCVDtBQWlCQSxFQXBDZ0I7O0FBcUNqQkMsVUFBUztBQUNSQyxrQkFEUSw0QkFDU3BCLEVBRFQsRUFDWUMsS0FEWixFQUNrQjtBQUN6QixRQUFLRCxFQUFMLEdBQVVBLEVBQVY7QUFDQSxRQUFLQyxLQUFMLEdBQWFBLEtBQWI7QUFDQWMsS0FBRSxlQUFGLEVBQW1CTSxLQUFuQixDQUF5QixRQUF6QjtBQUNBLEdBTE87QUFNUkMsZUFOUSwyQkFNUztBQUFBOztBQUNoQmQsY0FBV0YsR0FBWCxDQUFlLG9CQUFvQixLQUFLTixFQUF4QyxFQUNDVSxJQURELENBQ00sa0JBQVU7QUFDZkssTUFBRSxhQUFGLEVBQWlCUSxJQUFqQixDQUFzQkMsT0FBTzNCLElBQTdCO0FBQ0E0QixXQUFPQyxPQUFQLENBQWUsT0FBS3hCLElBQUwsQ0FBVSxTQUFWLENBQWYsRUFBb0MsT0FBS0osS0FBTCxDQUFXRCxJQUFYLENBQWdCLE9BQUtJLEtBQXJCLEVBQTRCMEIsSUFBaEU7QUFDQSxXQUFLN0IsS0FBTCxDQUFXRCxJQUFYLENBQWdCK0IsTUFBaEIsQ0FBdUIsT0FBSzNCLEtBQTVCLEVBQW1DLENBQW5DO0FBQ0EsUUFBRyxPQUFLSCxLQUFMLENBQVdELElBQVgsQ0FBZ0JnQyxNQUFoQixJQUEwQixDQUE3QixFQUNDZCxFQUFFLFVBQUYsRUFBY0MsSUFBZDtBQUNELElBUEQ7QUFRQSxHQWZPO0FBZ0JSYyxXQWhCUSxxQkFnQkc5QixFQWhCSCxFQWdCTUMsS0FoQk4sRUFnQmE7QUFBQTs7QUFDcEJJLFNBQU1DLEdBQU4sQ0FBVXlCLFNBQVNDLE1BQVQsR0FBa0IsV0FBNUIsRUFBd0M7QUFDcENDLFlBQVE7QUFDUEMsY0FBUW5CLEVBQUUsYUFBV2YsRUFBYixFQUFpQm1DLEdBQWpCLEVBREQ7QUFFVkMsVUFBSXJCLEVBQUUsU0FBT2YsRUFBVCxFQUFhbUMsR0FBYjtBQUZNO0FBRDRCLElBQXhDLEVBTUN6QixJQU5ELENBTU0sa0JBQVU7QUFDZkssTUFBRSxpQ0FBRixFQUFxQ1EsSUFBckMsQ0FBMENDLE9BQU8zQixJQUFQLENBQVl3QyxlQUF0RDtBQUNZLFFBQUdiLE9BQU8zQixJQUFQLENBQVl5QyxTQUFmLEVBQ0E7QUFDQyxTQUFHZCxPQUFPM0IsSUFBUCxDQUFZeUMsU0FBWixJQUF1QixVQUExQixFQUNDQyxPQUFPUixRQUFQLEdBQWtCLGNBQWNQLE9BQU8zQixJQUFQLENBQVkyQyxJQUE1QyxDQURELEtBR0NmLE9BQU9nQixLQUFQLENBQWFqQixPQUFPM0IsSUFBUCxDQUFZeUMsU0FBekIsRUFBb0MsT0FBS3BDLElBQUwsQ0FBVSxXQUFWLENBQXBDO0FBQ0QsS0FORCxNQU1PO0FBQ2xCdUIsWUFBT0MsT0FBUCxDQUFlLE9BQUt4QixJQUFMLENBQVUsT0FBVixDQUFmLEVBQW1DLE9BQUtKLEtBQUwsQ0FBV0QsSUFBWCxDQUFnQkksS0FBaEIsRUFBdUIwQixJQUExRDtBQUNBLFlBQUs3QixLQUFMLENBQVdELElBQVgsQ0FBZ0IrQixNQUFoQixDQUF1QjNCLEtBQXZCLEVBQThCLENBQTlCO0FBQ0EsU0FBRyxPQUFLSCxLQUFMLENBQVdELElBQVgsQ0FBZ0JnQyxNQUFoQixJQUEwQixDQUE3QixFQUNDZCxFQUFFLFVBQUYsRUFBY0MsSUFBZDtBQUNEUixnQkFBV0YsR0FBWCxDQUFlLG9CQUFvQk4sRUFBbkMsRUFDQ1UsSUFERCxDQUNNLGtCQUFVO0FBQ2ZLLFFBQUUsYUFBRixFQUFpQlEsSUFBakIsQ0FBc0JDLE9BQU8zQixJQUE3QjtBQUNBLE1BSEQ7QUFJWTtBQUNiLElBeEJEO0FBeUJBO0FBMUNPOztBQXJDUSxDQUFSLENBQVY7O0FBb0ZBLFNBQVNMLGdCQUFULENBQTBCa0QsQ0FBMUIsRUFBNkI7QUFDekIsUUFBT0EsRUFBRUMsUUFBRixHQUFhQyxPQUFiLENBQXFCLHVCQUFyQixFQUE4QyxHQUE5QyxDQUFQO0FBQ0gsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuRUQ7U0FFQTs7eURBRUE7cUNBQ0E7QUFDQTsyQ0FDQTs4QkFDQTtBQUVBO0FBUEE7QUFGQSxHOzs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlFQTs7MEJBR0E7MkJBRUE7QUFIQTtBQURBLEc7Ozs7Ozs7QUN4QkE7QUFDQTtBQUNBLHVCQUFxSjtBQUNySjtBQUNBLHdCQUE0RztBQUM1RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCIsImZpbGUiOiJqcy9wYWdlcy9jb21wYXJlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0MjUpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGY1ZmNmZjBlMWYwODAwOTdjZjQ2IiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUgMjYgMjciLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogX3ZtLmlkLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIi0xXCIsXG4gICAgICBcInJvbGVcIjogXCJkaWFsb2dcIixcbiAgICAgIFwiYXJpYS1sYWJlbGxlZGJ5XCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgY2xhc3M6ICdtb2RhbC1kaWFsb2cgJyArIF92bS5zaXplLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInJvbGVcIjogXCJkb2N1bWVudFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtaGVhZGVyXCJcbiAgfSwgW19jKCdoNCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC10aXRsZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW192bS5fdChcIm1vZGFsLXRpdGxlXCIsIFtfdm0uX3YoXCJNb2RhbCBUaXRsZVwiKV0pXSwgMildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1ib2R5XCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWJvZHlcIiwgW192bS5fdihcIk1vZGFsIEJvZHlcIildKV0sIDIpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWZvb3RlclwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1mb290ZXJcIiwgW19jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXSwgMildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNjUzZTczNjlcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi02NTNlNzM2OVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTJcbi8vIG1vZHVsZSBjaHVua3MgPSA2IDE0IDE1IiwiVnVlLmNvbXBvbmVudCgnY29tcGFyZS1saXN0JywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvcGFnZXMvQ29tcGFyZS52dWUnKSk7XHJcblZ1ZS5jb21wb25lbnQoJ2NvbXAtbW9kYWwnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWUnKSk7XHJcblxyXG5WdWUuZmlsdGVyKCdjdXJyZW5jeScsIGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuICAn4oKxJytudW1iZXJXaXRoQ29tbWFzKHBhcnNlRmxvYXQodmFsdWUpLnRvRml4ZWQoMikpO1xyXG59KTtcclxuXHJcbnZhciBhcHAgPSBuZXcgVnVlKHtcclxuXHJcblx0ZWw6ICcjY29tcGFyZS1jb250ZW50JyxcclxuXHRkYXRhOiB7XHJcblx0XHRpdGVtczpbXSxcclxuXHRcdG5vX2l0ZW06ICcnLFxyXG5cdFx0aWQ6JycsXHJcblx0XHRpbmRleDonJyxcclxuXHRcdGxhbmc6W11cclxuXHR9LFxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdGZ1bmN0aW9uIGdldFRyYW5zbGF0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2NvbXBhcmUnKTtcclxuICAgICAgICB9XHJcblxyXG5cdFx0ZnVuY3Rpb24gZ2V0Q29tcGFyZSgpIHtcclxuXHRcdFx0cmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvY29tcGFyZScpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRheGlvcy5hbGwoW1xyXG5cdFx0XHRnZXRUcmFuc2xhdGlvbigpLFxyXG5cdFx0XHRnZXRDb21wYXJlKClcclxuXHRcdF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG5cdFx0XHQoXHJcblx0XHRcdFx0dHJhbnNsYXRpb24sXHJcblx0XHRcdFx0Y29tcGFyZVxyXG5cdFx0XHQpID0+IHtcclxuXHJcblx0XHRcdHRoaXMubGFuZyA9IHRyYW5zbGF0aW9uLmRhdGEuZGF0YTtcclxuXHRcdFx0XHJcblx0XHRcdGlmKGNvbXBhcmUuZGF0YS5kYXRhICE9IDApXHJcblx0XHRcdFx0dGhpcy5pdGVtcyA9IGNvbXBhcmUuc3RhdHVzID09IDIwMD8gY29tcGFyZS5kYXRhOltdO1xyXG5cdFx0XHRlbHNlXHJcblx0XHRcdFx0JCgnI25vLWl0ZW0nKS5zaG93KCk7IC8vdGhpcy5ub19pdGVtID0gXCJUaGVyZSBhcmUgbm8gaXRlbXMgaW4geW91ciBjb21wYXJpc29uXCI7XHJcblx0XHR9KSlcclxuXHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cdH0sXHJcblx0bWV0aG9kczoge1xyXG5cdFx0c2hvd1JlbW92ZURpYWxvZyhpZCxpbmRleCl7XHJcblx0XHRcdHRoaXMuaWQgPSBpZDtcclxuXHRcdFx0dGhpcy5pbmRleCA9IGluZGV4O1xyXG5cdFx0XHQkKCcjZGlhbG9nUmVtb3ZlJykubW9kYWwoJ3RvZ2dsZScpO1xyXG5cdFx0fSxcclxuXHRcdGRlbGV0ZUNvbXBhcmUgKCkge1xyXG5cdFx0XHRheGlvc0FQSXYxLmdldCgnL2RlbGV0ZWNvbXBhcmUvJyArIHRoaXMuaWQpXHRcdFx0XHJcblx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0JChcIiNjb21wLWNvdW50XCIpLnRleHQocmVzdWx0LmRhdGEpO1xyXG5cdFx0XHRcdHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1sncmVtb3ZlZCddLHRoaXMuaXRlbXMuZGF0YVt0aGlzLmluZGV4XS5uYW1lKTtcclxuXHRcdFx0XHR0aGlzLml0ZW1zLmRhdGEuc3BsaWNlKHRoaXMuaW5kZXgsIDEpO1xyXG5cdFx0XHRcdGlmKHRoaXMuaXRlbXMuZGF0YS5sZW5ndGggPT0gMClcclxuXHRcdFx0XHRcdCQoJyNuby1pdGVtJykuc2hvdygpO1x0XHRcdFx0XHRcclxuXHRcdFx0fSk7XHJcblx0XHR9LFxyXG5cdFx0YWRkdG9jYXJ0IChpZCxpbmRleCkge1xyXG5cdFx0XHRheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2FkZCcse1xyXG5cdFx0XHQgICAgcGFyYW1zOiB7XHJcblx0XHRcdCAgICBcdHByb2RfaWQ6JCgnI3Byb2RfaWQnK2lkKS52YWwoKSxcclxuXHRcdFx0XHRcdHF0eTokKCcjcXR5JytpZCkudmFsKClcclxuXHRcdFx0ICAgIH1cclxuXHRcdFx0fSlcclxuXHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHQkKFwiI2NhcnQtY291bnQsICNjYXJ0LWNvdW50LW1vYmlsZVwiKS50ZXh0KHJlc3VsdC5kYXRhLmNhcnRfaXRlbV9jb3VudCk7XHJcbiAgICAgICAgICAgICAgICBpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2cpXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcdGlmKHJlc3VsdC5kYXRhLmVycm9yX21zZz09J1JlZGlyZWN0JylcclxuICAgICAgICAgICAgICAgIFx0XHR3aW5kb3cubG9jYXRpb24gPSAnL3Byb2R1Y3QvJyArIHJlc3VsdC5kYXRhLnNsdWc7XHJcbiAgICAgICAgICAgICAgICBcdGVsc2VcclxuXHQgICAgICAgICAgICAgICAgXHR0b2FzdHIuZXJyb3IocmVzdWx0LmRhdGEuZXJyb3JfbXNnLCB0aGlzLmxhbmdbJ25vdC1hZGRlZCddKTsgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmdbJ2FkZGVkJ10sIHRoaXMuaXRlbXMuZGF0YVtpbmRleF0ubmFtZSk7XHJcblx0XHRcdFx0XHR0aGlzLml0ZW1zLmRhdGEuc3BsaWNlKGluZGV4LCAxKTtcclxuXHRcdFx0XHRcdGlmKHRoaXMuaXRlbXMuZGF0YS5sZW5ndGggPT0gMClcclxuXHRcdFx0XHRcdFx0JCgnI25vLWl0ZW0nKS5zaG93KCk7XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0YXhpb3NBUEl2MS5nZXQoJy9kZWxldGVjb21wYXJlLycgKyBpZClcclxuXHRcdFx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0XHRcdCQoXCIjY29tcC1jb3VudFwiKS50ZXh0KHJlc3VsdC5kYXRhKTtcclxuXHRcdFx0XHRcdH0pOyAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuXHRcdFx0fSk7XHRcclxuXHRcdH1cclxuXHR9XHJcblxyXG59KTtcclxuXHJcbmZ1bmN0aW9uIG51bWJlcldpdGhDb21tYXMoeCkge1xyXG4gICAgcmV0dXJuIHgudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIixcIik7XHJcbn1cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9jb21wYXJlLmpzIiwiPHRlbXBsYXRlPlxyXG48ZGl2PlxyXG5cdDxkaXYgY2xhc3M9XCJjb2wtbWQtNCBjb2wtc20tNlwiIHYtZm9yPVwiKGl0ZW0saW5kZXgpIGluIGl0ZW1zLmRhdGFcIiA6a2V5PVwiaXRlbS5pZFwiIDppZD1cIidkaXYnK2l0ZW0uaWRcIj5cclxuXHQgIDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcblx0ICAgIDxhIDpocmVmPVwiJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcIj48aW1nIGNsYXNzPVwiY2FyZC1pbWctdG9wXCIgOnNyYz1cIml0ZW0uaW1hZ2VcIiBhbHQ9XCJcIj48L2E+XHJcblx0ICAgIDx1bCBjbGFzcz1cImxpc3QtZ3JvdXAgbGlzdC1ncm91cC1mbHVzaFwiPlxyXG5cdCAgICAgIDxsaSBjbGFzcz1cImxpc3QtZ3JvdXAtaXRlbVwiPjxhIDpocmVmPVwiJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcIiBjbGFzcz1cImNvbXAtbmFtZVwiPnt7aXRlbS5uYW1lfX08L2E+PC9saT5cclxuXHQgICAgICA8bGkgY2xhc3M9XCJsaXN0LWdyb3VwLWl0ZW1cIj57e2l0ZW0ucHJpY2UgfCBjdXJyZW5jeX19PC9saT5cdCAgICAgIFxyXG5cdCAgICAgIDxsaSBjbGFzcz1cImxpc3QtZ3JvdXAtaXRlbSBtYXJnaW4xXCI+PHNwYW4gY2xhc3M9XCJmb250XCI+U3RvY2s6PC9zcGFuPiB7e2l0ZW0uc3RvY2t9fTwvbGk+XHJcblx0ICAgICAgPGxpIGNsYXNzPVwibGlzdC1ncm91cC1pdGVtXCI+XHJcblx0XHQgICAgICA8ZGl2IGNsYXNzPVwicmF0aW5nLWJsb2NrXCI+XHJcblx0ICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN0YXItcmF0aW5ncy1zcHJpdGVcIj48c3BhbiA6c3R5bGU9XCJpdGVtLnJhdGluZ1wiIGNsYXNzPVwic3Rhci1yYXRpbmdzLXNwcml0ZS1yYXRpbmdcIj48L3NwYW4+PC9kaXY+XHJcblx0ICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9saT5cclxuICAgICAgICAgIDxsaSBjbGFzcz1cImxpc3QtZ3JvdXAtaXRlbSBtYXJnaW4yXCI+PHNwYW4gY2xhc3M9XCJmb250XCI+RGVzY3JpcHRpb246PC9zcGFuPiB7e2l0ZW0uZGVzY319PC9saT5cclxuXHQgICAgICA8bGkgY2xhc3M9XCJsaXN0LWdyb3VwLWl0ZW1cIj48YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3M9XCJidG4gYnRuLXJhaXNlZCBidG4tcHJpbWFyeSBjb21wLWJ0blwiIEBjbGljaz1cImFkZHRvY2FydChpdGVtLmlkLCBpbmRleClcIj5BREQgVE8gQ0FSVDxkaXYgY2xhc3M9XCJyaXBwbGUtY29udGFpbmVyXCI+PC9kaXY+PC9hPjwvbGk+XHJcblx0ICAgICAgPGxpIGNsYXNzPVwibGlzdC1ncm91cC1pdGVtXCI+PGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGNsYXNzPVwiYnRuIGJ0bi1yYWlzZWQgYnRuLXByaW1hcnkgY29tcC1idG5cIiBAY2xpY2s9XCJzaG93UmVtb3ZlRGlhbG9nKGl0ZW0uaWQsIGluZGV4KVwiPlJFTU9WRTxkaXYgY2xhc3M9XCJyaXBwbGUtY29udGFpbmVyXCI+PC9kaXY+PC9hPjwvbGk+XHJcblx0XHQgIDxpbnB1dCA6aWQ9XCIncHJvZF9pZCcgKyBpdGVtLmlkXCIgOm5hbWU9XCIncHJvZF9pZCcgKyBpdGVtLmlkXCIgdHlwZT1cImhpZGRlblwiIDp2YWx1ZT1cIml0ZW0ucHJvZF9pZFwiPlxyXG5cdFx0ICA8aW5wdXQgOmlkPVwiJ3F0eScgKyBpdGVtLmlkXCIgOm5hbWU9XCIncXR5JytpdGVtLmlkXCIgdHlwZT1cImhpZGRlblwiIHZhbHVlPVwiMVwiPlx0XHJcblx0ICAgIDwvdWw+ICAgICAgIFxyXG5cdCAgPC9kaXY+XHJcblx0PC9kaXY+XHJcbjwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cdHByb3BzOiBbJ2l0ZW1zJ10sXHJcblx0bWV0aG9kczp7XHJcblx0XHRzaG93UmVtb3ZlRGlhbG9nKGlkLGluZGV4KXtcclxuXHRcdFx0dGhpcy4kcGFyZW50LnNob3dSZW1vdmVEaWFsb2coaWQsaW5kZXgpXHJcblx0XHR9LFxyXG5cdFx0YWRkdG9jYXJ0KGlkLGluZGV4KXtcclxuXHRcdFx0dGhpcy4kcGFyZW50LmFkZHRvY2FydChpZCxpbmRleClcclxuXHRcdH1cdFx0XHJcblx0fVxyXG59XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBDb21wYXJlLnZ1ZT84ODA0YTc1YSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NvbXBhcmUudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1mYmQzM2Q4NFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Db21wYXJlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxwYWdlc1xcXFxDb21wYXJlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIENvbXBhcmUudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWZiZDMzZDg0XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtZmJkMzNkODRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3BhZ2VzL0NvbXBhcmUudnVlXG4vLyBtb2R1bGUgaWQgPSAzMzlcbi8vIG1vZHVsZSBjaHVua3MgPSAxNSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2JywgX3ZtLl9sKChfdm0uaXRlbXMuZGF0YSksIGZ1bmN0aW9uKGl0ZW0sIGluZGV4KSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgICBrZXk6IGl0ZW0uaWQsXG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb2wtc20tNlwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJpZFwiOiAnZGl2JyArIGl0ZW0uaWRcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNhcmRcIlxuICAgIH0sIFtfYygnYScsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiAnL3Byb2R1Y3QvJyArIGl0ZW0uc2x1Z1xuICAgICAgfVxuICAgIH0sIFtfYygnaW1nJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY2FyZC1pbWctdG9wXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInNyY1wiOiBpdGVtLmltYWdlLFxuICAgICAgICBcImFsdFwiOiBcIlwiXG4gICAgICB9XG4gICAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3VsJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGlzdC1ncm91cCBsaXN0LWdyb3VwLWZsdXNoXCJcbiAgICB9LCBbX2MoJ2xpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGlzdC1ncm91cC1pdGVtXCJcbiAgICB9LCBbX2MoJ2EnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb21wLW5hbWVcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiAnL3Byb2R1Y3QvJyArIGl0ZW0uc2x1Z1xuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoX3ZtLl9zKGl0ZW0ubmFtZSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGlzdC1ncm91cC1pdGVtXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShpdGVtLnByaWNlKSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImxpc3QtZ3JvdXAtaXRlbSBtYXJnaW4xXCJcbiAgICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmb250XCJcbiAgICB9LCBbX3ZtLl92KFwiU3RvY2s6XCIpXSksIF92bS5fdihcIiBcIiArIF92bS5fcyhpdGVtLnN0b2NrKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGlzdC1ncm91cC1pdGVtXCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInJhdGluZy1ibG9ja1wiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlXCJcbiAgICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXJhdGluZ1wiLFxuICAgICAgc3R5bGU6IChpdGVtLnJhdGluZylcbiAgICB9KV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibGlzdC1ncm91cC1pdGVtIG1hcmdpbjJcIlxuICAgIH0sIFtfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImZvbnRcIlxuICAgIH0sIFtfdm0uX3YoXCJEZXNjcmlwdGlvbjpcIildKSwgX3ZtLl92KFwiIFwiICsgX3ZtLl9zKGl0ZW0uZGVzYykpXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImxpc3QtZ3JvdXAtaXRlbVwiXG4gICAgfSwgW19jKCdhJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1yYWlzZWQgYnRuLXByaW1hcnkgY29tcC1idG5cIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBfdm0uYWRkdG9jYXJ0KGl0ZW0uaWQsIGluZGV4KVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIkFERCBUTyBDQVJUXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicmlwcGxlLWNvbnRhaW5lclwiXG4gICAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJsaXN0LWdyb3VwLWl0ZW1cIlxuICAgIH0sIFtfYygnYScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcmFpc2VkIGJ0bi1wcmltYXJ5IGNvbXAtYnRuXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIlxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLnNob3dSZW1vdmVEaWFsb2coaXRlbS5pZCwgaW5kZXgpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiUkVNT1ZFXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicmlwcGxlLWNvbnRhaW5lclwiXG4gICAgfSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcImlkXCI6ICdwcm9kX2lkJyArIGl0ZW0uaWQsXG4gICAgICAgIFwibmFtZVwiOiAncHJvZF9pZCcgKyBpdGVtLmlkLFxuICAgICAgICBcInR5cGVcIjogXCJoaWRkZW5cIlxuICAgICAgfSxcbiAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgIFwidmFsdWVcIjogaXRlbS5wcm9kX2lkXG4gICAgICB9XG4gICAgfSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaWRcIjogJ3F0eScgKyBpdGVtLmlkLFxuICAgICAgICBcIm5hbWVcIjogJ3F0eScgKyBpdGVtLmlkLFxuICAgICAgICBcInR5cGVcIjogXCJoaWRkZW5cIixcbiAgICAgICAgXCJ2YWx1ZVwiOiBcIjFcIlxuICAgICAgfVxuICAgIH0pXSldKV0pXG4gIH0pKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1mYmQzM2Q4NFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWZiZDMzZDg0XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvQ29tcGFyZS52dWVcbi8vIG1vZHVsZSBpZCA9IDQwN1xuLy8gbW9kdWxlIGNodW5rcyA9IDE1IiwiPHRlbXBsYXRlPlxyXG48ZGl2IGNsYXNzPVwibW9kYWwgbWQtbW9kYWwgZmFkZVwiIDppZD1cImlkXCIgdGFiaW5kZXg9XCItMVwiIHJvbGU9XCJkaWFsb2dcIiBhcmlhLWxhYmVsbGVkYnk9XCJtb2RhbC1sYWJlbFwiPlxyXG4gICAgPGRpdiA6Y2xhc3M9XCInbW9kYWwtZGlhbG9nICcrc2l6ZVwiIHJvbGU9XCJkb2N1bWVudFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCIgaWQ9XCJtb2RhbC1sYWJlbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC10aXRsZVwiPk1vZGFsIFRpdGxlPC9zbG90PlxyXG4gICAgICAgICAgICAgICAgPC9oND5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtYm9keVwiPk1vZGFsIEJvZHk8L3Nsb3Q+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZm9vdGVyXCI+XHJcbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtZm9vdGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L3Nsb3Q+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbjwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICAnaWQnOntyZXF1aXJlZDp0cnVlfVxyXG4gICAgICAgICwnc2l6ZSc6IHtkZWZhdWx0Oidtb2RhbC1zbSd9XHJcbiAgICB9XHJcbn1cclxuPC9zY3JpcHQ+XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBEaWFsb2dNb2RhbC52dWU/M2M5OTA4YTYiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTY1M2U3MzY5XFxcIn0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxEaWFsb2dNb2RhbC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBEaWFsb2dNb2RhbC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNjUzZTczNjlcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi02NTNlNzM2OVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSA5XG4vLyBtb2R1bGUgY2h1bmtzID0gNiAxNCAxNSJdLCJzb3VyY2VSb290IjoiIn0=