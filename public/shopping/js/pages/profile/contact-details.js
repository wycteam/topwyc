/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 444);
/******/ })
/************************************************************************/
/******/ ({

/***/ 194:
/***/ (function(module, exports) {

//NOT USED! 

var user = window.Laravel.user;

var contactDetails = new Vue({
  el: '#contact-details',
  data: {
    user: user,
    optionProv: [],
    optionCity: [],
    addressInfo: new Form({
      address: '',
      barangay: '',
      city: '',
      province: '',
      zip_code: '',
      landmark: '',
      remarks: '',
      landline: '',
      mobile: ''
    }, { baseURL: '/profile/update' }),

    homeAddressInfo: new Form({
      address: '',
      barangay: '',
      city: '',
      province: '',
      zip_code: '',
      landmark: '',
      remarks: '',
      landline: '',
      mobile: ''
    }, { baseURL: '/profile/update' })
  },

  components: {
    Multiselect: window.VueMultiselect.default
  },

  methods: {
    getSecurityAnswers: function getSecurityAnswers() {
      var _this = this;

      axios.get('/profile/security-answers').then(function (result) {
        _this.securityAnswers = result.data.questions;
        if (_this.securityAnswers.length) {
          _this.showSecurity = false;
        }
      });
    },


    getCity: function getCity(data) {
      var _this2 = this;

      this.homeAddressInfo.city = '';
      axios.get('/profile/city/' + data).then(function (result) {
        var array = [];
        for (var i = 0; i < result.data.length; i++) {
          array.push(result.data[i].name);
        }
        _this2.optionCity = array;
      });
    },

    saveHomeAddress: function saveHomeAddress(event) {
      this.homeAddressInfo.submit('post', '/home-address-info').then(function (result) {
        toastr.success('Successfully saved.');
      }).catch(function (error) {
        toastr.error('Updated Failed.');
      });
    }
  },

  created: function created() {
    var _this3 = this;

    axios.get('/profile/get-home-address').then(function (result) {
      if (result.data.mobile != '' && result.data.landline != '' && result.data.residence != null) {
        _this3.homeAddressInfo = new Form($.extend({
          mobile: result.data.mobile,
          landline: result.data.landline,
          residence: result.data.residence
        }, result.data.residence), { baseURL: '/profile/update' });
        axios.get('/profile/city/' + _this3.homeAddressInfo.province).then(function (result) {
          _this3.optionCity = result.data;
        });
      } else {
        homeAddressInfo: new Form({
          address: '',
          barangay: '',
          city: '',
          province: '',
          zip_code: '',
          landmark: '',
          remarks: '',
          landline: '',
          mobile: ''
        }, { baseURL: '/profile/update' });
      }
    });

    axios.get('/profile/provinces').then(function (result) {
      var array = [];
      for (var i = 0; i < result.data.length; i++) {
        array.push(result.data[i].name);
      }
      _this3.optionProv = array;
    });
  }
});

/***/ }),

/***/ 444:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(194);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3Byb2ZpbGUvY29udGFjdC1kZXRhaWxzLmpzIl0sIm5hbWVzIjpbInVzZXIiLCJ3aW5kb3ciLCJMYXJhdmVsIiwiY29udGFjdERldGFpbHMiLCJWdWUiLCJlbCIsImRhdGEiLCJvcHRpb25Qcm92Iiwib3B0aW9uQ2l0eSIsImFkZHJlc3NJbmZvIiwiRm9ybSIsImFkZHJlc3MiLCJiYXJhbmdheSIsImNpdHkiLCJwcm92aW5jZSIsInppcF9jb2RlIiwibGFuZG1hcmsiLCJyZW1hcmtzIiwibGFuZGxpbmUiLCJtb2JpbGUiLCJiYXNlVVJMIiwiaG9tZUFkZHJlc3NJbmZvIiwiY29tcG9uZW50cyIsIk11bHRpc2VsZWN0IiwiVnVlTXVsdGlzZWxlY3QiLCJkZWZhdWx0IiwibWV0aG9kcyIsImdldFNlY3VyaXR5QW5zd2VycyIsImF4aW9zIiwiZ2V0IiwidGhlbiIsInNlY3VyaXR5QW5zd2VycyIsInJlc3VsdCIsInF1ZXN0aW9ucyIsImxlbmd0aCIsInNob3dTZWN1cml0eSIsImdldENpdHkiLCJhcnJheSIsImkiLCJwdXNoIiwibmFtZSIsInNhdmVIb21lQWRkcmVzcyIsImV2ZW50Iiwic3VibWl0IiwidG9hc3RyIiwic3VjY2VzcyIsImNhdGNoIiwiZXJyb3IiLCJjcmVhdGVkIiwicmVzaWRlbmNlIiwiJCIsImV4dGVuZCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTs7QUFFQSxJQUFJQSxPQUFPQyxPQUFPQyxPQUFQLENBQWVGLElBQTFCOztBQUVBLElBQUlHLGlCQUFpQixJQUFJQyxHQUFKLENBQVE7QUFDM0JDLE1BQUksa0JBRHVCO0FBRTNCQyxRQUFNO0FBQ0pOLFVBQUtBLElBREQ7QUFFSk8sZ0JBQVksRUFGUjtBQUdKQyxnQkFBWSxFQUhSO0FBSUpDLGlCQUFhLElBQUlDLElBQUosQ0FBUztBQUNsQkMsZUFBYyxFQURJO0FBRWxCQyxnQkFBYyxFQUZJO0FBR2xCQyxZQUFjLEVBSEk7QUFJbEJDLGdCQUFjLEVBSkk7QUFLbEJDLGdCQUFjLEVBTEk7QUFNbEJDLGdCQUFjLEVBTkk7QUFPbEJDLGVBQWMsRUFQSTtBQVFsQkMsZ0JBQWMsRUFSSTtBQVNsQkMsY0FBYztBQVRJLEtBQVQsRUFVWCxFQUFFQyxTQUFXLGlCQUFiLEVBVlcsQ0FKVDs7QUFpQkpDLHFCQUFpQixJQUFJWCxJQUFKLENBQVM7QUFDcEJDLGVBQWMsRUFETTtBQUVwQkMsZ0JBQWMsRUFGTTtBQUdwQkMsWUFBYyxFQUhNO0FBSXBCQyxnQkFBYyxFQUpNO0FBS3BCQyxnQkFBYyxFQUxNO0FBTXBCQyxnQkFBYyxFQU5NO0FBT3BCQyxlQUFjLEVBUE07QUFRcEJDLGdCQUFjLEVBUk07QUFTcEJDLGNBQWM7QUFUTSxLQUFULEVBVWYsRUFBRUMsU0FBVyxpQkFBYixFQVZlO0FBakJiLEdBRnFCOztBQWdDM0JFLGNBQVk7QUFDWEMsaUJBQWF0QixPQUFPdUIsY0FBUCxDQUFzQkM7QUFEeEIsR0FoQ2U7O0FBb0MzQkMsV0FBUTtBQUNOQyxzQkFETSxnQ0FDZTtBQUFBOztBQUNuQkMsWUFBTUMsR0FBTixDQUFVLDJCQUFWLEVBQ0dDLElBREgsQ0FDUSxrQkFBVTtBQUNkLGNBQUtDLGVBQUwsR0FBdUJDLE9BQU8xQixJQUFQLENBQVkyQixTQUFuQztBQUNBLFlBQUksTUFBS0YsZUFBTCxDQUFxQkcsTUFBekIsRUFBaUM7QUFDL0IsZ0JBQUtDLFlBQUwsR0FBb0IsS0FBcEI7QUFDRDtBQUNKLE9BTkQ7QUFPRCxLQVRLOzs7QUFXTkMsYUFBUSxpQkFBUzlCLElBQVQsRUFBYztBQUFBOztBQUNwQixXQUFLZSxlQUFMLENBQXFCUixJQUFyQixHQUE0QixFQUE1QjtBQUNBZSxZQUFNQyxHQUFOLENBQVUsbUJBQWlCdkIsSUFBM0IsRUFDR3dCLElBREgsQ0FDUSxrQkFBVTtBQUNoQixZQUFJTyxRQUFRLEVBQVo7QUFDQSxhQUFJLElBQUlDLElBQUksQ0FBWixFQUFlQSxJQUFJTixPQUFPMUIsSUFBUCxDQUFZNEIsTUFBL0IsRUFBdUNJLEdBQXZDLEVBQTJDO0FBQ3pDRCxnQkFBTUUsSUFBTixDQUFXUCxPQUFPMUIsSUFBUCxDQUFZZ0MsQ0FBWixFQUFlRSxJQUExQjtBQUNEO0FBQ0QsZUFBS2hDLFVBQUwsR0FBa0I2QixLQUFsQjtBQUNELE9BUEQ7QUFRRCxLQXJCSzs7QUF1Qk5JLHFCQUFnQix5QkFBU0MsS0FBVCxFQUFlO0FBQzdCLFdBQUtyQixlQUFMLENBQXFCc0IsTUFBckIsQ0FBNEIsTUFBNUIsRUFBb0Msb0JBQXBDLEVBQ0NiLElBREQsQ0FDTSxrQkFBVTtBQUNaYyxlQUFPQyxPQUFQLENBQWUscUJBQWY7QUFDSCxPQUhELEVBSUNDLEtBSkQsQ0FJTyxpQkFBUztBQUNaRixlQUFPRyxLQUFQLENBQWEsaUJBQWI7QUFDSCxPQU5EO0FBT0Q7QUEvQkssR0FwQ21COztBQXNFM0JDLFNBdEUyQixxQkFzRWpCO0FBQUE7O0FBR1JwQixVQUFNQyxHQUFOLENBQVUsMkJBQVYsRUFDR0MsSUFESCxDQUNRLGtCQUFVO0FBQ2QsVUFBR0UsT0FBTzFCLElBQVAsQ0FBWWEsTUFBWixJQUFzQixFQUF0QixJQUE0QmEsT0FBTzFCLElBQVAsQ0FBWVksUUFBWixJQUF3QixFQUFwRCxJQUEwRGMsT0FBTzFCLElBQVAsQ0FBWTJDLFNBQVosSUFBeUIsSUFBdEYsRUFBMkY7QUFDekYsZUFBSzVCLGVBQUwsR0FBdUIsSUFBSVgsSUFBSixDQUFTd0MsRUFBRUMsTUFBRixDQUFTO0FBQ3BDaEMsa0JBQVdhLE9BQU8xQixJQUFQLENBQVlhLE1BRGE7QUFFcENELG9CQUFXYyxPQUFPMUIsSUFBUCxDQUFZWSxRQUZhO0FBR3BDK0IscUJBQVdqQixPQUFPMUIsSUFBUCxDQUFZMkM7QUFIYSxTQUFULEVBSTdCakIsT0FBTzFCLElBQVAsQ0FBWTJDLFNBSmlCLENBQVQsRUFJSSxFQUFFN0IsU0FBUyxpQkFBWCxFQUpKLENBQXZCO0FBS0FRLGNBQU1DLEdBQU4sQ0FBVSxtQkFBaUIsT0FBS1IsZUFBTCxDQUFxQlAsUUFBaEQsRUFDR2dCLElBREgsQ0FDUSxrQkFBVTtBQUNoQixpQkFBS3RCLFVBQUwsR0FBa0J3QixPQUFPMUIsSUFBekI7QUFDRCxTQUhEO0FBSUQsT0FWRCxNQVVLO0FBQ0hlLHlCQUFpQixJQUFJWCxJQUFKLENBQVM7QUFDcEJDLG1CQUFjLEVBRE07QUFFcEJDLG9CQUFjLEVBRk07QUFHcEJDLGdCQUFjLEVBSE07QUFJcEJDLG9CQUFjLEVBSk07QUFLcEJDLG9CQUFjLEVBTE07QUFNcEJDLG9CQUFjLEVBTk07QUFPcEJDLG1CQUFjLEVBUE07QUFRcEJDLG9CQUFjLEVBUk07QUFTcEJDLGtCQUFjO0FBVE0sU0FBVCxFQVVmLEVBQUVDLFNBQVcsaUJBQWIsRUFWZTtBQVdsQjtBQUNKLEtBekJEOztBQTRCQVEsVUFBTUMsR0FBTixDQUFVLG9CQUFWLEVBQ0dDLElBREgsQ0FDUSxrQkFBVTtBQUNkLFVBQUlPLFFBQVEsRUFBWjtBQUNBLFdBQUksSUFBSUMsSUFBSSxDQUFaLEVBQWVBLElBQUlOLE9BQU8xQixJQUFQLENBQVk0QixNQUEvQixFQUF1Q0ksR0FBdkMsRUFBMkM7QUFDekNELGNBQU1FLElBQU4sQ0FBV1AsT0FBTzFCLElBQVAsQ0FBWWdDLENBQVosRUFBZUUsSUFBMUI7QUFDRDtBQUNELGFBQUtqQyxVQUFMLEdBQWtCOEIsS0FBbEI7QUFDSCxLQVBEO0FBUUQ7QUE3RzBCLENBQVIsQ0FBckIsQyIsImZpbGUiOiJqcy9wYWdlcy9wcm9maWxlL2NvbnRhY3QtZGV0YWlscy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDQ0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmNWZjZmYwZTFmMDgwMDk3Y2Y0NiIsIi8vTk9UIFVTRUQhIFxyXG5cclxudmFyIHVzZXIgPSB3aW5kb3cuTGFyYXZlbC51c2VyO1xyXG5cclxudmFyIGNvbnRhY3REZXRhaWxzID0gbmV3IFZ1ZSh7XHJcbiAgZWw6ICcjY29udGFjdC1kZXRhaWxzJyxcclxuICBkYXRhOiB7XHJcbiAgICB1c2VyOnVzZXIsXHJcbiAgICBvcHRpb25Qcm92OiBbXSxcclxuICAgIG9wdGlvbkNpdHk6IFtdLFxyXG4gICAgYWRkcmVzc0luZm86IG5ldyBGb3JtKHtcclxuICAgICAgICBhZGRyZXNzICAgICA6ICcnXHJcbiAgICAgICAsYmFyYW5nYXkgICAgOiAnJ1xyXG4gICAgICAgLGNpdHkgICAgICAgIDogJydcclxuICAgICAgICxwcm92aW5jZSAgICA6ICcnXHJcbiAgICAgICAsemlwX2NvZGUgICAgOiAnJ1xyXG4gICAgICAgLGxhbmRtYXJrICAgIDogJydcclxuICAgICAgICxyZW1hcmtzICAgICA6ICcnICBcclxuICAgICAgICxsYW5kbGluZSAgICA6ICcnXHJcbiAgICAgICAsbW9iaWxlICAgICAgOiAnJ1xyXG4gICAgfSx7IGJhc2VVUkwgIDogJy9wcm9maWxlL3VwZGF0ZSd9KSxcclxuXHJcblxyXG4gICAgaG9tZUFkZHJlc3NJbmZvOiBuZXcgRm9ybSh7XHJcbiAgICAgICAgICBhZGRyZXNzICAgICA6ICcnXHJcbiAgICAgICAgICxiYXJhbmdheSAgICA6ICcnXHJcbiAgICAgICAgICxjaXR5ICAgICAgICA6ICcnIFxyXG4gICAgICAgICAscHJvdmluY2UgICAgOiAnJ1xyXG4gICAgICAgICAsemlwX2NvZGUgICAgOiAnJ1xyXG4gICAgICAgICAsbGFuZG1hcmsgICAgOiAnJ1xyXG4gICAgICAgICAscmVtYXJrcyAgICAgOiAnJ1xyXG4gICAgICAgICAsbGFuZGxpbmUgICAgOiAnJ1xyXG4gICAgICAgICAsbW9iaWxlICAgICAgOiAnJ1xyXG4gICAgfSx7IGJhc2VVUkwgIDogJy9wcm9maWxlL3VwZGF0ZSd9KVxyXG4gIH0sXHJcblxyXG4gIGNvbXBvbmVudHM6IHtcclxuICBcdE11bHRpc2VsZWN0OiB3aW5kb3cuVnVlTXVsdGlzZWxlY3QuZGVmYXVsdFxyXG4gIH0sXHJcblxyXG4gIG1ldGhvZHM6e1xyXG4gICAgZ2V0U2VjdXJpdHlBbnN3ZXJzKCkge1xyXG4gICAgICBheGlvcy5nZXQoJy9wcm9maWxlL3NlY3VyaXR5LWFuc3dlcnMnKVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0aGlzLnNlY3VyaXR5QW5zd2VycyA9IHJlc3VsdC5kYXRhLnF1ZXN0aW9ucztcclxuICAgICAgICAgIGlmICh0aGlzLnNlY3VyaXR5QW5zd2Vycy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5zaG93U2VjdXJpdHkgPSBmYWxzZTtcclxuICAgICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldENpdHk6ZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgIHRoaXMuaG9tZUFkZHJlc3NJbmZvLmNpdHkgPSAnJztcclxuICAgICAgYXhpb3MuZ2V0KCcvcHJvZmlsZS9jaXR5LycrZGF0YSlcclxuICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgIHZhciBhcnJheSA9IFtdO1xyXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCByZXN1bHQuZGF0YS5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICBhcnJheS5wdXNoKHJlc3VsdC5kYXRhW2ldLm5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9wdGlvbkNpdHkgPSBhcnJheTtcclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIHNhdmVIb21lQWRkcmVzczpmdW5jdGlvbihldmVudCl7XHJcbiAgICAgIHRoaXMuaG9tZUFkZHJlc3NJbmZvLnN1Ym1pdCgncG9zdCcsICcvaG9tZS1hZGRyZXNzLWluZm8nKVxyXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1N1Y2Nlc3NmdWxseSBzYXZlZC4nKTtcclxuICAgICAgfSlcclxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgIHRvYXN0ci5lcnJvcignVXBkYXRlZCBGYWlsZWQuJyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSxcclxuICB9LFxyXG5cclxuICBjcmVhdGVkKCkge1xyXG4gICAgXHJcblxyXG4gICAgYXhpb3MuZ2V0KCcvcHJvZmlsZS9nZXQtaG9tZS1hZGRyZXNzJylcclxuICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICBpZihyZXN1bHQuZGF0YS5tb2JpbGUgIT0gJycgJiYgcmVzdWx0LmRhdGEubGFuZGxpbmUgIT0gJycgJiYgcmVzdWx0LmRhdGEucmVzaWRlbmNlICE9IG51bGwpe1xyXG4gICAgICAgICAgdGhpcy5ob21lQWRkcmVzc0luZm8gPSBuZXcgRm9ybSgkLmV4dGVuZCh7XHJcbiAgICAgICAgICAgICAgIG1vYmlsZSAgIDogcmVzdWx0LmRhdGEubW9iaWxlXHJcbiAgICAgICAgICAgICAgLGxhbmRsaW5lIDogcmVzdWx0LmRhdGEubGFuZGxpbmVcclxuICAgICAgICAgICAgICAscmVzaWRlbmNlOiByZXN1bHQuZGF0YS5yZXNpZGVuY2VcclxuICAgICAgICAgIH0sIHJlc3VsdC5kYXRhLnJlc2lkZW5jZSksIHsgYmFzZVVSTDogJy9wcm9maWxlL3VwZGF0ZSd9KTtcclxuICAgICAgICAgIGF4aW9zLmdldCgnL3Byb2ZpbGUvY2l0eS8nK3RoaXMuaG9tZUFkZHJlc3NJbmZvLnByb3ZpbmNlKVxyXG4gICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbkNpdHkgPSByZXN1bHQuZGF0YTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgaG9tZUFkZHJlc3NJbmZvOiBuZXcgRm9ybSh7XHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzICAgICA6ICcnXHJcbiAgICAgICAgICAgICAgICxiYXJhbmdheSAgICA6ICcnXHJcbiAgICAgICAgICAgICAgICxjaXR5ICAgICAgICA6ICcnIFxyXG4gICAgICAgICAgICAgICAscHJvdmluY2UgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsemlwX2NvZGUgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsbGFuZG1hcmsgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAscmVtYXJrcyAgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsbGFuZGxpbmUgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsbW9iaWxlICAgICAgOiAnJ1xyXG4gICAgICAgICAgfSx7IGJhc2VVUkwgIDogJy9wcm9maWxlL3VwZGF0ZSd9KVxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIFxyXG4gICAgYXhpb3MuZ2V0KCcvcHJvZmlsZS9wcm92aW5jZXMnKVxyXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgIHZhciBhcnJheSA9IFtdO1xyXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCByZXN1bHQuZGF0YS5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICBhcnJheS5wdXNoKHJlc3VsdC5kYXRhW2ldLm5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9wdGlvblByb3YgPSBhcnJheTtcclxuICAgIH0pO1xyXG4gIH0sXHJcblxyXG4gIFxyXG59KTtcclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvcHJvZmlsZS9jb250YWN0LWRldGFpbHMuanMiXSwic291cmNlUm9vdCI6IiJ9