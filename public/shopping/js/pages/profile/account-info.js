/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 443);
/******/ })
/************************************************************************/
/******/ ({

/***/ 193:
/***/ (function(module, exports) {

//NOT USED! 

var user = window.Laravel.user;
var display_name = window.Laravel.user.display_name;
//checkbox value
var display_stat = "";
if (display_name == 'Real Name') {
    display_stat = false;
} else {
    display_stat = true;
}
var accountInfo = new Vue({
    el: '#account-info',
    data: {
        user: user,
        optionGender: ['Male', 'Female'],

        accInfo: new Form({
            secondary_email: user.secondary_email,
            birth_date: user.birth_date,
            nick_name: user.nick_name,
            gender: user.gender,
            display_name: display_stat
        }, { baseURL: '/profile/update' })
    },
    directives: {
        datepicker: {
            bind: function bind(el, binding, vnode) {
                $(el).datepicker({
                    format: 'yyyy-mm-dd'
                }).on('changeDate', function (e) {
                    accountInfo.$set(accountInfo.accInfo, 'birth_date', e.format('yyyy-mm-dd'));
                });
            }
        }
    },
    methods: {
        //save account info
        saveInfo: function saveInfo(event) {
            var _this = this;

            this.accInfo.submit('post', '/account-info').then(function (result) {
                _this.accInfo = new Form({
                    secondary_email: result.secondary_email,
                    birth_date: result.birth_date,
                    nick_name: result.nick_name,
                    gender: result.gender,
                    display_name: display_stat
                }, { baseURL: '/profile/update' });
                toastr.success('Your profile has been updated.');
            }).catch(function (error) {
                toastr.error('Update failed.');
            });
        }
    }

});

/***/ }),

/***/ 443:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(193);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9wcm9maWxlL2FjY291bnQtaW5mby5qcyJdLCJuYW1lcyI6WyJ1c2VyIiwid2luZG93IiwiTGFyYXZlbCIsImRpc3BsYXlfbmFtZSIsImRpc3BsYXlfc3RhdCIsImFjY291bnRJbmZvIiwiVnVlIiwiZWwiLCJkYXRhIiwib3B0aW9uR2VuZGVyIiwiYWNjSW5mbyIsIkZvcm0iLCJzZWNvbmRhcnlfZW1haWwiLCJiaXJ0aF9kYXRlIiwibmlja19uYW1lIiwiZ2VuZGVyIiwiYmFzZVVSTCIsImRpcmVjdGl2ZXMiLCJkYXRlcGlja2VyIiwiYmluZCIsImJpbmRpbmciLCJ2bm9kZSIsIiQiLCJmb3JtYXQiLCJvbiIsImUiLCIkc2V0IiwibWV0aG9kcyIsInNhdmVJbmZvIiwiZXZlbnQiLCJzdWJtaXQiLCJ0aGVuIiwicmVzdWx0IiwidG9hc3RyIiwic3VjY2VzcyIsImNhdGNoIiwiZXJyb3IiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7O0FBRUEsSUFBSUEsT0FBT0MsT0FBT0MsT0FBUCxDQUFlRixJQUExQjtBQUNBLElBQUlHLGVBQWVGLE9BQU9DLE9BQVAsQ0FBZUYsSUFBZixDQUFvQkcsWUFBdkM7QUFDQTtBQUNBLElBQUlDLGVBQWUsRUFBbkI7QUFDQyxJQUFHRCxnQkFBZ0IsV0FBbkIsRUFBK0I7QUFDNUJDLG1CQUFlLEtBQWY7QUFDRixDQUZELE1BRUs7QUFDRkEsbUJBQWUsSUFBZjtBQUNGO0FBQ0YsSUFBSUMsY0FBYyxJQUFJQyxHQUFKLENBQVE7QUFDdEJDLFFBQUksZUFEa0I7QUFFdEJDLFVBQU07QUFDRlIsY0FBS0EsSUFESDtBQUVGUyxzQkFBYyxDQUFDLE1BQUQsRUFBUyxRQUFULENBRlo7O0FBSUZDLGlCQUFTLElBQUlDLElBQUosQ0FBUztBQUNkQyw2QkFBaUJaLEtBQUtZLGVBRFI7QUFFYkMsd0JBQVliLEtBQUthLFVBRko7QUFHYkMsdUJBQVdkLEtBQUtjLFNBSEg7QUFJYkMsb0JBQVFmLEtBQUtlLE1BSkE7QUFLYlosMEJBQWNDO0FBTEQsU0FBVCxFQU1QLEVBQUVZLFNBQVMsaUJBQVgsRUFOTztBQUpQLEtBRmdCO0FBY3RCQyxnQkFBWTtBQUNSQyxvQkFBWTtBQUNSQyxnQkFEUSxnQkFDSFosRUFERyxFQUNDYSxPQURELEVBQ1VDLEtBRFYsRUFDaUI7QUFDckJDLGtCQUFFZixFQUFGLEVBQU1XLFVBQU4sQ0FBaUI7QUFDYkssNEJBQVE7QUFESyxpQkFBakIsRUFFR0MsRUFGSCxDQUVNLFlBRk4sRUFFb0IsVUFBQ0MsQ0FBRCxFQUFPO0FBQ3ZCcEIsZ0NBQVlxQixJQUFaLENBQWlCckIsWUFBWUssT0FBN0IsRUFBc0MsWUFBdEMsRUFBb0RlLEVBQUVGLE1BQUYsQ0FBUyxZQUFULENBQXBEO0FBQ0gsaUJBSkQ7QUFLSDtBQVBPO0FBREosS0FkVTtBQXlCdEJJLGFBQVE7QUFDSjtBQUNBQyxrQkFBUyxrQkFBVUMsS0FBVixFQUFnQjtBQUFBOztBQUNyQixpQkFBS25CLE9BQUwsQ0FBYW9CLE1BQWIsQ0FBb0IsTUFBcEIsRUFBNEIsZUFBNUIsRUFDQ0MsSUFERCxDQUNNLGtCQUFVO0FBQ1osc0JBQUtyQixPQUFMLEdBQWUsSUFBSUMsSUFBSixDQUFTO0FBQ3BCQyxxQ0FBaUJvQixPQUFPcEIsZUFESjtBQUVuQkMsZ0NBQVltQixPQUFPbkIsVUFGQTtBQUduQkMsK0JBQVdrQixPQUFPbEIsU0FIQztBQUluQkMsNEJBQVFpQixPQUFPakIsTUFKSTtBQUtuQlosa0NBQWNDO0FBTEssaUJBQVQsRUFNYixFQUFFWSxTQUFTLGlCQUFYLEVBTmEsQ0FBZjtBQU9BaUIsdUJBQU9DLE9BQVAsQ0FBZSxnQ0FBZjtBQUNILGFBVkQsRUFXQ0MsS0FYRCxDQVdPLGlCQUFTO0FBQ1pGLHVCQUFPRyxLQUFQLENBQWEsZ0JBQWI7QUFDSCxhQWJEO0FBY0g7QUFqQkc7O0FBekJjLENBQVIsQ0FBbEIsQyIsImZpbGUiOiJqcy9wYWdlcy9wcm9maWxlL2FjY291bnQtaW5mby5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDQzKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmNWZjZmYwZTFmMDgwMDk3Y2Y0NiIsIi8vTk9UIFVTRUQhIFxyXG5cclxudmFyIHVzZXIgPSB3aW5kb3cuTGFyYXZlbC51c2VyO1xyXG52YXIgZGlzcGxheV9uYW1lID0gd2luZG93LkxhcmF2ZWwudXNlci5kaXNwbGF5X25hbWU7XHJcbi8vY2hlY2tib3ggdmFsdWVcclxudmFyIGRpc3BsYXlfc3RhdCA9IFwiXCI7XHJcbiBpZihkaXNwbGF5X25hbWUgPT0gJ1JlYWwgTmFtZScpe1xyXG4gICAgZGlzcGxheV9zdGF0ID0gZmFsc2VcclxuIH1lbHNle1xyXG4gICAgZGlzcGxheV9zdGF0ID0gdHJ1ZVxyXG4gfVxyXG52YXIgYWNjb3VudEluZm8gPSBuZXcgVnVlKHtcclxuICAgIGVsOiAnI2FjY291bnQtaW5mbycsXHJcbiAgICBkYXRhOiB7XHJcbiAgICAgICAgdXNlcjp1c2VyLFxyXG4gICAgICAgIG9wdGlvbkdlbmRlcjogWydNYWxlJywgJ0ZlbWFsZSddLFxyXG5cclxuICAgICAgICBhY2NJbmZvOiBuZXcgRm9ybSh7XHJcbiAgICAgICAgICAgIHNlY29uZGFyeV9lbWFpbDogdXNlci5zZWNvbmRhcnlfZW1haWxcclxuICAgICAgICAgICAgLGJpcnRoX2RhdGU6IHVzZXIuYmlydGhfZGF0ZVxyXG4gICAgICAgICAgICAsbmlja19uYW1lOiB1c2VyLm5pY2tfbmFtZVxyXG4gICAgICAgICAgICAsZ2VuZGVyOiB1c2VyLmdlbmRlclxyXG4gICAgICAgICAgICAsZGlzcGxheV9uYW1lOiBkaXNwbGF5X3N0YXQgXHJcbiAgICAgICAgfSx7IGJhc2VVUkw6ICcvcHJvZmlsZS91cGRhdGUnfSksXHJcbiAgICB9LFxyXG4gICAgZGlyZWN0aXZlczoge1xyXG4gICAgICAgIGRhdGVwaWNrZXI6IHtcclxuICAgICAgICAgICAgYmluZChlbCwgYmluZGluZywgdm5vZGUpIHtcclxuICAgICAgICAgICAgICAgICQoZWwpLmRhdGVwaWNrZXIoe1xyXG4gICAgICAgICAgICAgICAgICAgIGZvcm1hdDogJ3l5eXktbW0tZGQnXHJcbiAgICAgICAgICAgICAgICB9KS5vbignY2hhbmdlRGF0ZScsIChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWNjb3VudEluZm8uJHNldChhY2NvdW50SW5mby5hY2NJbmZvLCAnYmlydGhfZGF0ZScsIGUuZm9ybWF0KCd5eXl5LW1tLWRkJykpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczp7XHJcbiAgICAgICAgLy9zYXZlIGFjY291bnQgaW5mb1xyXG4gICAgICAgIHNhdmVJbmZvOmZ1bmN0aW9uIChldmVudCl7XHJcbiAgICAgICAgICAgIHRoaXMuYWNjSW5mby5zdWJtaXQoJ3Bvc3QnLCAnL2FjY291bnQtaW5mbycpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjY0luZm8gPSBuZXcgRm9ybSh7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vjb25kYXJ5X2VtYWlsOiByZXN1bHQuc2Vjb25kYXJ5X2VtYWlsXHJcbiAgICAgICAgICAgICAgICAgICAgLGJpcnRoX2RhdGU6IHJlc3VsdC5iaXJ0aF9kYXRlXHJcbiAgICAgICAgICAgICAgICAgICAgLG5pY2tfbmFtZTogcmVzdWx0Lm5pY2tfbmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICxnZW5kZXI6IHJlc3VsdC5nZW5kZXJcclxuICAgICAgICAgICAgICAgICAgICAsZGlzcGxheV9uYW1lOiBkaXNwbGF5X3N0YXQgICAgXHJcbiAgICAgICAgICAgICAgICB9LHsgYmFzZVVSTDogJy9wcm9maWxlL3VwZGF0ZSd9KTtcclxuICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKCdZb3VyIHByb2ZpbGUgaGFzIGJlZW4gdXBkYXRlZC4nKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignVXBkYXRlIGZhaWxlZC4nKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuXHJcbn0pO1xyXG5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9wcm9maWxlL2FjY291bnQtaW5mby5qcyJdLCJzb3VyY2VSb290IjoiIn0=