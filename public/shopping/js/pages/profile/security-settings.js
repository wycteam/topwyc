/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 445);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 195:
/***/ (function(module, exports, __webpack_require__) {

//NOT USED! 

var user = window.Laravel.user;
var display_name = window.Laravel.user.display_name;
//checkbox value
var display_stat = "";
if (display_name == 'Real Name') {
  display_stat = false;
} else {
  display_stat = true;
}

Vue.component('que-answer', __webpack_require__(37));

var securitySettings = new Vue({
  el: '#security-settings',
  data: {
    showSecurity: true,
    user: user,
    valueQue1: {},
    valueQue2: {},
    valueQue3: {},
    securityQuestions: [],
    securityAnswers: [],

    changePassword: new Form({
      old_password: '',
      new_password: '',
      new_password_confirmation: ''
    }, { baseURL: '/profile/update' }),

    securityQuestion: new Form({
      question: '',
      question1: '',
      question2: '',
      answer: '',
      answer1: '',
      answer2: ''
    }, { baseURL: '/profile/update' })

  },
  methods: {
    //show modals
    shipAddModal: function shipAddModal(event) {
      var elShow = event.target.name;
      $('#' + elShow + 2).modal('toggle');
    },

    fnchangePassword: function fnchangePassword(event) {
      this.changePassword.submit('post', '/change-password').then(function (result) {
        toastr.success('Your password has been changed.');
      }).catch(function (error) {
        toastr.error('Update failed.');
      });
    },

    saveSecurityQuestion: function saveSecurityQuestion(event) {
      var _this = this;

      this.securityQuestion.submit('post', '/answer-security-questions').then(function (result) {
        toastr.success('Successfully saved.');
        _this.getSecurityAnswers();
      }).catch(function (error) {
        toastr.error('Updated Failed.');
      });
    },

    saveNewAddress: function saveNewAddress(event) {
      this.addressInfo.submit('post', '/address-info');
    },

    getSecurityAnswers: function getSecurityAnswers() {
      var _this2 = this;

      axios.get('/profile/security-answers').then(function (result) {
        _this2.securityAnswers = result.data.questions;
        if (_this2.securityAnswers.length) {
          _this2.showSecurity = false;
        }
      });
    }
  },

  created: function created() {
    var _this3 = this;

    axios.get('/profile/security-questions').then(function (result) {
      _this3.securityQuestions = result.data;
    });

    this.getSecurityAnswers();
  },


  computed: {
    answerEmpty: function answerEmpty() {
      return $.isEmptyObject(this.securityAnswers);
    }
  }
});

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['item']
});

/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(21),
  /* template */
  __webpack_require__(41),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\profile\\SecurityQuestionAnswer.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] SecurityQuestionAnswer.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5feabad6", Component.options)
  } else {
    hotAPI.reload("data-v-5feabad6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-sm-12 security-question-set"
  }, [_c('div', {
    staticClass: "col-sm-8"
  }, [_c('label', {
    staticClass: "account-label question-label"
  }, [_vm._v(_vm._s(_vm.item.question))])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-4"
  }, [_c('label', {
    staticClass: "account-label answer-label"
  }, [_vm._v(_vm._s(this.$parent.lang2.data['ans-on-file']))]), _vm._v(" "), _c('p')])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5feabad6", module.exports)
  }
}

/***/ }),

/***/ 445:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(195);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9wcm9maWxlL3NlY3VyaXR5LXNldHRpbmdzLmpzIiwid2VicGFjazovLy9TZWN1cml0eVF1ZXN0aW9uQW5zd2VyLnZ1ZT9kYWM5Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9maWxlL1NlY3VyaXR5UXVlc3Rpb25BbnN3ZXIudnVlPzVjMmUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3Byb2ZpbGUvU2VjdXJpdHlRdWVzdGlvbkFuc3dlci52dWU/YWQxNSoiXSwibmFtZXMiOlsidXNlciIsIndpbmRvdyIsIkxhcmF2ZWwiLCJkaXNwbGF5X25hbWUiLCJkaXNwbGF5X3N0YXQiLCJWdWUiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwic2VjdXJpdHlTZXR0aW5ncyIsImVsIiwiZGF0YSIsInNob3dTZWN1cml0eSIsInZhbHVlUXVlMSIsInZhbHVlUXVlMiIsInZhbHVlUXVlMyIsInNlY3VyaXR5UXVlc3Rpb25zIiwic2VjdXJpdHlBbnN3ZXJzIiwiY2hhbmdlUGFzc3dvcmQiLCJGb3JtIiwib2xkX3Bhc3N3b3JkIiwibmV3X3Bhc3N3b3JkIiwibmV3X3Bhc3N3b3JkX2NvbmZpcm1hdGlvbiIsImJhc2VVUkwiLCJzZWN1cml0eVF1ZXN0aW9uIiwicXVlc3Rpb24iLCJxdWVzdGlvbjEiLCJxdWVzdGlvbjIiLCJhbnN3ZXIiLCJhbnN3ZXIxIiwiYW5zd2VyMiIsIm1ldGhvZHMiLCJzaGlwQWRkTW9kYWwiLCJldmVudCIsImVsU2hvdyIsInRhcmdldCIsIm5hbWUiLCIkIiwibW9kYWwiLCJmbmNoYW5nZVBhc3N3b3JkIiwic3VibWl0IiwidGhlbiIsInRvYXN0ciIsInN1Y2Nlc3MiLCJjYXRjaCIsImVycm9yIiwic2F2ZVNlY3VyaXR5UXVlc3Rpb24iLCJnZXRTZWN1cml0eUFuc3dlcnMiLCJzYXZlTmV3QWRkcmVzcyIsImFkZHJlc3NJbmZvIiwiYXhpb3MiLCJnZXQiLCJyZXN1bHQiLCJxdWVzdGlvbnMiLCJsZW5ndGgiLCJjcmVhdGVkIiwiY29tcHV0ZWQiLCJhbnN3ZXJFbXB0eSIsImlzRW1wdHlPYmplY3QiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQTs7QUFFQSxJQUFJQSxPQUFPQyxPQUFPQyxPQUFQLENBQWVGLElBQTFCO0FBQ0EsSUFBSUcsZUFBZUYsT0FBT0MsT0FBUCxDQUFlRixJQUFmLENBQW9CRyxZQUF2QztBQUNBO0FBQ0EsSUFBSUMsZUFBZSxFQUFuQjtBQUNDLElBQUdELGdCQUFnQixXQUFuQixFQUErQjtBQUM1QkMsaUJBQWUsS0FBZjtBQUNGLENBRkQsTUFFSztBQUNGQSxpQkFBZSxJQUFmO0FBQ0Y7O0FBRUZDLElBQUlDLFNBQUosQ0FBYyxZQUFkLEVBQTZCLG1CQUFBQyxDQUFRLEVBQVIsQ0FBN0I7O0FBRUEsSUFBSUMsbUJBQW1CLElBQUlILEdBQUosQ0FBUTtBQUM3QkksTUFBSSxvQkFEeUI7QUFFN0JDLFFBQU07QUFDSkMsa0JBQWMsSUFEVjtBQUVKWCxVQUFLQSxJQUZEO0FBR0pZLGVBQVUsRUFITjtBQUlKQyxlQUFVLEVBSk47QUFLSkMsZUFBVSxFQUxOO0FBTUpDLHVCQUFtQixFQU5mO0FBT0pDLHFCQUFpQixFQVBiOztBQVNKQyxvQkFBZ0IsSUFBSUMsSUFBSixDQUFTO0FBQ3RCQyxvQkFBZSxFQURPO0FBRXRCQyxvQkFBZSxFQUZPO0FBR3RCQyxpQ0FBNEI7QUFITixLQUFULEVBSWQsRUFBRUMsU0FBUSxpQkFBVixFQUpjLENBVFo7O0FBZUpDLHNCQUFrQixJQUFJTCxJQUFKLENBQVM7QUFDeEJNLGdCQUFZLEVBRFk7QUFFeEJDLGlCQUFZLEVBRlk7QUFHeEJDLGlCQUFZLEVBSFk7QUFJeEJDLGNBQVksRUFKWTtBQUt4QkMsZUFBWSxFQUxZO0FBTXhCQyxlQUFZO0FBTlksS0FBVCxFQU9oQixFQUFFUCxTQUFRLGlCQUFWLEVBUGdCOztBQWZkLEdBRnVCO0FBMkI3QlEsV0FBUTtBQUNOO0FBQ0FDLGtCQUFhLHNCQUFVQyxLQUFWLEVBQWdCO0FBQzFCLFVBQUlDLFNBQVNELE1BQU1FLE1BQU4sQ0FBYUMsSUFBMUI7QUFDQUMsUUFBRSxNQUFJSCxNQUFKLEdBQVcsQ0FBYixFQUFnQkksS0FBaEIsQ0FBc0IsUUFBdEI7QUFDRixLQUxLOztBQU9OQyxzQkFBaUIsMEJBQVNOLEtBQVQsRUFBZTtBQUM5QixXQUFLZixjQUFMLENBQW9Cc0IsTUFBcEIsQ0FBMkIsTUFBM0IsRUFBbUMsa0JBQW5DLEVBQ0NDLElBREQsQ0FDTSxrQkFBVTtBQUNaQyxlQUFPQyxPQUFQLENBQWUsaUNBQWY7QUFDSCxPQUhELEVBSUNDLEtBSkQsQ0FJTyxpQkFBUztBQUNaRixlQUFPRyxLQUFQLENBQWEsZ0JBQWI7QUFDSCxPQU5EO0FBT0QsS0FmSzs7QUFpQk5DLDBCQUFxQiw4QkFBU2IsS0FBVCxFQUFlO0FBQUE7O0FBQ2xDLFdBQUtULGdCQUFMLENBQXNCZ0IsTUFBdEIsQ0FBNkIsTUFBN0IsRUFBcUMsNEJBQXJDLEVBQ0NDLElBREQsQ0FDTSxrQkFBVTtBQUNaQyxlQUFPQyxPQUFQLENBQWUscUJBQWY7QUFDQSxjQUFLSSxrQkFBTDtBQUNILE9BSkQsRUFLQ0gsS0FMRCxDQUtPLGlCQUFTO0FBQ1pGLGVBQU9HLEtBQVAsQ0FBYSxpQkFBYjtBQUNILE9BUEQ7QUFRRCxLQTFCSzs7QUE0Qk5HLG9CQUFlLHdCQUFTZixLQUFULEVBQWU7QUFDNUIsV0FBS2dCLFdBQUwsQ0FBaUJULE1BQWpCLENBQXdCLE1BQXhCLEVBQWdDLGVBQWhDO0FBQ0QsS0E5Qks7O0FBZ0NOTyxzQkFoQ00sZ0NBZ0NlO0FBQUE7O0FBQ25CRyxZQUFNQyxHQUFOLENBQVUsMkJBQVYsRUFDR1YsSUFESCxDQUNRLGtCQUFVO0FBQ2QsZUFBS3hCLGVBQUwsR0FBdUJtQyxPQUFPekMsSUFBUCxDQUFZMEMsU0FBbkM7QUFDQSxZQUFJLE9BQUtwQyxlQUFMLENBQXFCcUMsTUFBekIsRUFBaUM7QUFDL0IsaUJBQUsxQyxZQUFMLEdBQW9CLEtBQXBCO0FBQ0Q7QUFDSixPQU5EO0FBT0Q7QUF4Q0ssR0EzQnFCOztBQXVFN0IyQyxTQXZFNkIscUJBdUVuQjtBQUFBOztBQUNSTCxVQUFNQyxHQUFOLENBQVUsNkJBQVYsRUFDR1YsSUFESCxDQUNRLGtCQUFVO0FBQ2QsYUFBS3pCLGlCQUFMLEdBQXlCb0MsT0FBT3pDLElBQWhDO0FBQ0gsS0FIRDs7QUFLQSxTQUFLb0Msa0JBQUw7QUFDRCxHQTlFNEI7OztBQWdGN0JTLFlBQVU7QUFDUkMsaUJBQWEsdUJBQVk7QUFDdkIsYUFBT3BCLEVBQUVxQixhQUFGLENBQWdCLEtBQUt6QyxlQUFyQixDQUFQO0FBQ0Q7QUFITztBQWhGbUIsQ0FBUixDQUF2QixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO1NBRUE7QUFEQSxHOzs7Ozs7O0FDZkE7QUFDQTtBQUNBLHdCQUFxSjtBQUNySjtBQUNBLHdCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3BhZ2VzL3Byb2ZpbGUvc2VjdXJpdHktc2V0dGluZ3MuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ0NSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsIi8vTk9UIFVTRUQhIFxyXG5cclxudmFyIHVzZXIgPSB3aW5kb3cuTGFyYXZlbC51c2VyO1xyXG52YXIgZGlzcGxheV9uYW1lID0gd2luZG93LkxhcmF2ZWwudXNlci5kaXNwbGF5X25hbWU7XHJcbi8vY2hlY2tib3ggdmFsdWVcclxudmFyIGRpc3BsYXlfc3RhdCA9IFwiXCI7XHJcbiBpZihkaXNwbGF5X25hbWUgPT0gJ1JlYWwgTmFtZScpe1xyXG4gICAgZGlzcGxheV9zdGF0ID0gZmFsc2VcclxuIH1lbHNle1xyXG4gICAgZGlzcGxheV9zdGF0ID0gdHJ1ZVxyXG4gfVxyXG5cclxuVnVlLmNvbXBvbmVudCgncXVlLWFuc3dlcicsICByZXF1aXJlKCcuLi8uLi9jb21wb25lbnRzL3Byb2ZpbGUvU2VjdXJpdHlRdWVzdGlvbkFuc3dlci52dWUnKSk7XHJcblxyXG52YXIgc2VjdXJpdHlTZXR0aW5ncyA9IG5ldyBWdWUoe1xyXG4gIGVsOiAnI3NlY3VyaXR5LXNldHRpbmdzJyxcclxuICBkYXRhOiB7XHJcbiAgICBzaG93U2VjdXJpdHk6IHRydWUsXHJcbiAgICB1c2VyOnVzZXIsXHJcbiAgICB2YWx1ZVF1ZTE6e30sXHJcbiAgICB2YWx1ZVF1ZTI6e30sXHJcbiAgICB2YWx1ZVF1ZTM6e30sXHJcbiAgICBzZWN1cml0eVF1ZXN0aW9uczogW10sXHJcbiAgICBzZWN1cml0eUFuc3dlcnM6IFtdLFxyXG4gIFxyXG4gICAgY2hhbmdlUGFzc3dvcmQ6IG5ldyBGb3JtKHtcclxuICAgICAgIG9sZF9wYXNzd29yZCA6ICcnXHJcbiAgICAgICxuZXdfcGFzc3dvcmQgOiAnJ1xyXG4gICAgICAsbmV3X3Bhc3N3b3JkX2NvbmZpcm1hdGlvbiA6ICcnXHJcbiAgICB9LHsgYmFzZVVSTDonL3Byb2ZpbGUvdXBkYXRlJ30pLFxyXG5cclxuICAgIHNlY3VyaXR5UXVlc3Rpb246IG5ldyBGb3JtKHtcclxuICAgICAgIHF1ZXN0aW9uICA6ICcnXHJcbiAgICAgICxxdWVzdGlvbjEgOiAnJ1xyXG4gICAgICAscXVlc3Rpb24yIDogJydcclxuICAgICAgLGFuc3dlciAgICA6ICcnXHJcbiAgICAgICxhbnN3ZXIxICAgOiAnJ1xyXG4gICAgICAsYW5zd2VyMiAgIDogJydcclxuICAgIH0seyBiYXNlVVJMOicvcHJvZmlsZS91cGRhdGUnfSksXHJcblxyXG4gIH0sXHJcbiAgbWV0aG9kczp7XHJcbiAgICAvL3Nob3cgbW9kYWxzXHJcbiAgICBzaGlwQWRkTW9kYWw6ZnVuY3Rpb24gKGV2ZW50KXtcclxuICAgICAgIHZhciBlbFNob3cgPSBldmVudC50YXJnZXQubmFtZTtcclxuICAgICAgICQoJyMnK2VsU2hvdysyKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICB9LFxyXG4gIFxyXG4gICAgZm5jaGFuZ2VQYXNzd29yZDpmdW5jdGlvbihldmVudCl7XHJcbiAgICAgIHRoaXMuY2hhbmdlUGFzc3dvcmQuc3VibWl0KCdwb3N0JywgJy9jaGFuZ2UtcGFzc3dvcmQnKVxyXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJ1lvdXIgcGFzc3dvcmQgaGFzIGJlZW4gY2hhbmdlZC4nKTtcclxuICAgICAgfSlcclxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgIHRvYXN0ci5lcnJvcignVXBkYXRlIGZhaWxlZC4nKTtcclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIHNhdmVTZWN1cml0eVF1ZXN0aW9uOmZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgdGhpcy5zZWN1cml0eVF1ZXN0aW9uLnN1Ym1pdCgncG9zdCcsICcvYW5zd2VyLXNlY3VyaXR5LXF1ZXN0aW9ucycpXHJcbiAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnU3VjY2Vzc2Z1bGx5IHNhdmVkLicpO1xyXG4gICAgICAgICAgdGhpcy5nZXRTZWN1cml0eUFuc3dlcnMoKTtcclxuICAgICAgfSlcclxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgIHRvYXN0ci5lcnJvcignVXBkYXRlZCBGYWlsZWQuJyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBzYXZlTmV3QWRkcmVzczpmdW5jdGlvbihldmVudCl7XHJcbiAgICAgIHRoaXMuYWRkcmVzc0luZm8uc3VibWl0KCdwb3N0JywgJy9hZGRyZXNzLWluZm8nKTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0U2VjdXJpdHlBbnN3ZXJzKCkge1xyXG4gICAgICBheGlvcy5nZXQoJy9wcm9maWxlL3NlY3VyaXR5LWFuc3dlcnMnKVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0aGlzLnNlY3VyaXR5QW5zd2VycyA9IHJlc3VsdC5kYXRhLnF1ZXN0aW9ucztcclxuICAgICAgICAgIGlmICh0aGlzLnNlY3VyaXR5QW5zd2Vycy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5zaG93U2VjdXJpdHkgPSBmYWxzZTtcclxuICAgICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG4gICAgXHJcbiAgfSxcclxuXHJcbiAgY3JlYXRlZCgpIHtcclxuICAgIGF4aW9zLmdldCgnL3Byb2ZpbGUvc2VjdXJpdHktcXVlc3Rpb25zJylcclxuICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICB0aGlzLnNlY3VyaXR5UXVlc3Rpb25zID0gcmVzdWx0LmRhdGE7XHJcbiAgICB9KTtcclxuICAgXHJcbiAgICB0aGlzLmdldFNlY3VyaXR5QW5zd2VycygpO1xyXG4gIH0sXHJcblxyXG4gIGNvbXB1dGVkOiB7XHJcbiAgICBhbnN3ZXJFbXB0eTogZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gJC5pc0VtcHR5T2JqZWN0KHRoaXMuc2VjdXJpdHlBbnN3ZXJzKTtcclxuICAgIH1cclxuICB9XHJcbn0pO1xyXG5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9wcm9maWxlL3NlY3VyaXR5LXNldHRpbmdzLmpzIiwiPHRlbXBsYXRlPlxyXG5cdDxkaXYgY2xhc3M9XCJjb2wtc20tMTIgc2VjdXJpdHktcXVlc3Rpb24tc2V0XCI+XHJcblx0XHQ8ZGl2IGNsYXNzPVwiY29sLXNtLThcIj5cclxuXHRcdCBcdDxsYWJlbCBjbGFzcz1cImFjY291bnQtbGFiZWwgcXVlc3Rpb24tbGFiZWxcIj57e2l0ZW0ucXVlc3Rpb259fTwvbGFiZWw+XHJcblx0XHQ8L2Rpdj48IS0tIGVuZCBjb2wtc20tOCAtLT5cclxuXHJcblx0XHQ8ZGl2IGNsYXNzPVwiY29sLXNtLTRcIj5cclxuXHRcdFx0PGxhYmVsIGNsYXNzPVwiYWNjb3VudC1sYWJlbCBhbnN3ZXItbGFiZWxcIj57e3RoaXMuJHBhcmVudC5sYW5nMi5kYXRhWydhbnMtb24tZmlsZSddfX08L2xhYmVsPlxyXG5cdFx0IFx0PHA+PC9wPlxyXG5cdFx0PC9kaXY+PCEtLSBlbmQgY29sLXNtLTQgLS0+XHJcblx0PC9kaXY+PCEtLSBlbmQgc2VjdXJpdHktcXVlc3Rpb24tc2V0IC0tPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuXHRleHBvcnQgZGVmYXVsdHtcclxuXHRcdHByb3BzOlsnaXRlbSddXHJcblx0fVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gU2VjdXJpdHlRdWVzdGlvbkFuc3dlci52dWU/OWIxMjkxYWMiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9TZWN1cml0eVF1ZXN0aW9uQW5zd2VyLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNWZlYWJhZDZcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vU2VjdXJpdHlRdWVzdGlvbkFuc3dlci52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxccHJvZmlsZVxcXFxTZWN1cml0eVF1ZXN0aW9uQW5zd2VyLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFNlY3VyaXR5UXVlc3Rpb25BbnN3ZXIudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTVmZWFiYWQ2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNWZlYWJhZDZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3Byb2ZpbGUvU2VjdXJpdHlRdWVzdGlvbkFuc3dlci52dWVcbi8vIG1vZHVsZSBpZCA9IDM3XG4vLyBtb2R1bGUgY2h1bmtzID0gMTIgMTkiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTIgc2VjdXJpdHktcXVlc3Rpb24tc2V0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLThcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImFjY291bnQtbGFiZWwgcXVlc3Rpb24tbGFiZWxcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uaXRlbS5xdWVzdGlvbikpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tNFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYWNjb3VudC1sYWJlbCBhbnN3ZXItbGFiZWxcIlxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZzIuZGF0YVsnYW5zLW9uLWZpbGUnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdwJyldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTVmZWFiYWQ2XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNWZlYWJhZDZcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9maWxlL1NlY3VyaXR5UXVlc3Rpb25BbnN3ZXIudnVlXG4vLyBtb2R1bGUgaWQgPSA0MVxuLy8gbW9kdWxlIGNodW5rcyA9IDEyIDE5Il0sInNvdXJjZVJvb3QiOiIifQ==