/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 453);
/******/ })
/************************************************************************/
/******/ ({

/***/ 203:
/***/ (function(module, exports) {

window.storeCreate = new Vue({
    el: '#store-form',

    data: {
        cities: [],
        provinces: [],
        categories: [],
        coverLoading: false,
        profileLoading: false,
        tempCover: null,
        tempPrimary: null,
        docs_verified: Laravel.user.is_docs_verified,
        lang: [],
        form: new Form({
            id: null,
            cover: '',
            profile: '',
            name: null,
            cn_name: null,
            description: null,
            cn_description: null,
            email: null,
            categories: [],
            address: {
                id: null,
                address: null,
                city: null,
                province: null,
                landmark: null
            },
            number: {
                id: null,
                number: null
            },
            company: {
                name: null,
                address: null,
                facebook: null,
                twitter: null,
                instagram: null,
                wechat: null
            }
        }, { baseURL: '/' })
    },

    watch: {
        tempCover: function tempCover(val) {
            if (!val) {
                $('#upload-demo').removeClass('ready');
                $('#upload').val(''); // this will clear the input val.
                $uploadCrop.croppie('bind', {
                    url: ''
                }).then(function () {
                    console.log($this.lang['reset-complete']);
                });

                $('#upload-demo .cr-slider-wrap').hide();
                // $('#upload-demo .cr-boundary').hide();
            } else {
                $('#upload-demo .cr-slider-wrap').show();
            }
        },
        tempPrimary: function tempPrimary(val) {
            if (!val) {
                $('#upload-primary').removeClass('ready');
                $('#modalPrimary input[type="file"]').val(''); // this will clear the input val.
                $uploadPrimaryPic.croppie('bind', {
                    url: ''
                }).then(function () {
                    console.log(this.lang['reset-complete']);
                });

                $('#modalPrimary .cr-slider-wrap').hide();
                // $('#upload-demo .cr-boundary').hide();
            } else {
                $('#modalPrimary .cr-slider-wrap').show();
            }
        }
    },

    methods: {
        getCategories: function getCategories() {
            var _this = this;

            axiosAPIv1.get('categories').then(function (result) {
                _this.categories = result.data;
            });
        },
        submit: function submit(e) {
            $("#storecreate").prop('disabled', true);
            this.form.submit('post', e.currentTarget.action).then(function (result) {
                window.location.href = '/stores/' + result.slug;
            }).catch(function (error) {
                setTimeout(function () {
                    $("#storecreate").prop('disabled', false);
                }, 1000);
            });
        },
        triggerUploadPrimary: function triggerUploadPrimary() {
            $('#modalPrimary input[type="file"]').click();
        },
        readFile: function readFile(e) {

            var files = e.target.files || e.dataTransfer.files;

            if (!files.length) return;
            this.createImage(files);
        },
        readFilePrimary: function readFilePrimary(e) {

            var files = e.target.files || e.dataTransfer.files;

            if (!files.length) return;
            this.createPrimaryImage(files);
        },
        createImage: function createImage(input) {
            var _this2 = this;

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload-demo').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    _this2.tempCover = true;
                });
            };
            reader.readAsDataURL(input[0]);
        },
        createPrimaryImage: function createPrimaryImage(input) {
            var _this3 = this;

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload-primary').addClass('ready');
                $uploadPrimaryPic.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    _this3.tempPrimary = true;
                });
            };
            reader.readAsDataURL(input[0]);
        },
        submitCover: function submitCover() {
            var _this4 = this;

            this.coverLoading = true;
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                axios.post('/common/resize-image?width=1140&height=300', {
                    image: resp
                }).then(function (result) {
                    _this4.form.cover = result.data;

                    setTimeout(function () {
                        $('#upload-demo').css('background-image', "url('" + result.data + "')");
                    }, 1000);

                    _this4.tempCover = null;
                    _this4.coverLoading = false;
                }).catch(function (error) {
                    _this4.coverLoading = false;
                });
            });
        },
        submitPrimary: function submitPrimary() {
            var _this5 = this;

            this.profileLoading = true;
            $uploadPrimaryPic.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                axios.post('/common/resize-image?width=300&height=300', {
                    image: resp
                }).then(function (result) {
                    _this5.form.profile = result.data;

                    // $('#upload-demo').css('background-image',"url('" + result.data + "')");
                    _this5.tempPrimary = null;
                    _this5.profileLoading = false;
                    $('#modalPrimary').modal('toggle');
                    toastr.success(_this5.lang.data['prof-pic-set-msg']);
                }).catch(function (error) {
                    _this5.profileLoading = false;
                });
            });
        }
    },

    computed: {
        categoryPlaceholder: function categoryPlaceholder() {
            if (this.form.categories.length) {
                return '';

                return window.isChinese() ? '选择类别' : 'Select Category';
            }
        },
        getCategoryName: function getCategoryName() {
            return window.isChinese() ? 'name_cn' : 'name';
        }
    },

    components: {
        Multiselect: window.VueMultiselect.default
    },

    created: function created() {
        var _this6 = this;

        this.getCategories();

        Event.listen('Imgfileupload.coverLoading', function () {
            _this6.coverLoading = true;
        });

        Event.listen('Imgfileupload.coverOnLoad', function (img) {
            _this6.form.cover = img;
            _this6.coverLoading = false;
        });

        Event.listen('Imgfileupload.profileLoading', function () {
            _this6.profileLoading = true;
        });

        Event.listen('Imgfileupload.profileOnLoad', function (img) {
            _this6.form.profile = img;
            _this6.profileLoading = false;
        });

        axios.get('/translate/store-management').then(function (result) {
            _this6.lang = result.data;
            _this6.catPlaceholder = result.data.data['sel-cat'];
        });
    }
});

$uploadCrop = $('#upload-demo').croppie({
    url: 'https://dummyimage.com/1140x300/b0b0b0/f5f5f5&text=1140px+x+300px',
    viewport: {
        width: 1140,
        height: 300
    }
    // enableExif: true
});

$uploadPrimaryPic = $('#upload-primary').croppie({
    url: '/images/avatar/default.jpg',
    viewport: {
        width: 300,
        height: 300,
        type: 'circle'
    }
});

// $('#upload').on('change', function () { readFile(this); });

/*$('#number').inputmask({
    mask: '(9999) 999-9999'
})*/

function popupResult(result) {
    var html;

    if (result.src) {
        html = '<img src="' + result.src + '" />';
    }
    return;
    swal({
        title: '',
        html: true,
        text: html,
        allowOutsideClick: true
    });
    setTimeout(function () {
        $('.sweet-alert').css('margin', function () {
            var top = -1 * ($(this).height() / 2),
                left = -1 * ($(this).width() / 2);

            return top + 'px 0 0 ' + left + 'px';
        });
    }, 1);
}

/***/ }),

/***/ 453:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(203);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3N0b3JlL2NyZWF0ZS5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCJzdG9yZUNyZWF0ZSIsIlZ1ZSIsImVsIiwiZGF0YSIsImNpdGllcyIsInByb3ZpbmNlcyIsImNhdGVnb3JpZXMiLCJjb3ZlckxvYWRpbmciLCJwcm9maWxlTG9hZGluZyIsInRlbXBDb3ZlciIsInRlbXBQcmltYXJ5IiwiZG9jc192ZXJpZmllZCIsIkxhcmF2ZWwiLCJ1c2VyIiwiaXNfZG9jc192ZXJpZmllZCIsImxhbmciLCJmb3JtIiwiRm9ybSIsImlkIiwiY292ZXIiLCJwcm9maWxlIiwibmFtZSIsImNuX25hbWUiLCJkZXNjcmlwdGlvbiIsImNuX2Rlc2NyaXB0aW9uIiwiZW1haWwiLCJhZGRyZXNzIiwiY2l0eSIsInByb3ZpbmNlIiwibGFuZG1hcmsiLCJudW1iZXIiLCJjb21wYW55IiwiZmFjZWJvb2siLCJ0d2l0dGVyIiwiaW5zdGFncmFtIiwid2VjaGF0IiwiYmFzZVVSTCIsIndhdGNoIiwidmFsIiwiJCIsInJlbW92ZUNsYXNzIiwiJHVwbG9hZENyb3AiLCJjcm9wcGllIiwidXJsIiwidGhlbiIsImNvbnNvbGUiLCJsb2ciLCIkdGhpcyIsImhpZGUiLCJzaG93IiwiJHVwbG9hZFByaW1hcnlQaWMiLCJtZXRob2RzIiwiZ2V0Q2F0ZWdvcmllcyIsImF4aW9zQVBJdjEiLCJnZXQiLCJyZXN1bHQiLCJzdWJtaXQiLCJlIiwicHJvcCIsImN1cnJlbnRUYXJnZXQiLCJhY3Rpb24iLCJsb2NhdGlvbiIsImhyZWYiLCJzbHVnIiwiY2F0Y2giLCJzZXRUaW1lb3V0IiwidHJpZ2dlclVwbG9hZFByaW1hcnkiLCJjbGljayIsInJlYWRGaWxlIiwiZmlsZXMiLCJ0YXJnZXQiLCJkYXRhVHJhbnNmZXIiLCJsZW5ndGgiLCJjcmVhdGVJbWFnZSIsInJlYWRGaWxlUHJpbWFyeSIsImNyZWF0ZVByaW1hcnlJbWFnZSIsImlucHV0IiwicmVhZGVyIiwiRmlsZVJlYWRlciIsIm9ubG9hZCIsImFkZENsYXNzIiwicmVhZEFzRGF0YVVSTCIsInN1Ym1pdENvdmVyIiwidHlwZSIsInNpemUiLCJyZXNwIiwiYXhpb3MiLCJwb3N0IiwiaW1hZ2UiLCJjc3MiLCJzdWJtaXRQcmltYXJ5IiwibW9kYWwiLCJ0b2FzdHIiLCJzdWNjZXNzIiwiY29tcHV0ZWQiLCJjYXRlZ29yeVBsYWNlaG9sZGVyIiwiaXNDaGluZXNlIiwiZ2V0Q2F0ZWdvcnlOYW1lIiwiY29tcG9uZW50cyIsIk11bHRpc2VsZWN0IiwiVnVlTXVsdGlzZWxlY3QiLCJkZWZhdWx0IiwiY3JlYXRlZCIsIkV2ZW50IiwibGlzdGVuIiwiaW1nIiwiY2F0UGxhY2Vob2xkZXIiLCJ2aWV3cG9ydCIsIndpZHRoIiwiaGVpZ2h0IiwicG9wdXBSZXN1bHQiLCJodG1sIiwic3JjIiwic3dhbCIsInRpdGxlIiwidGV4dCIsImFsbG93T3V0c2lkZUNsaWNrIiwidG9wIiwibGVmdCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQUEsT0FBT0MsV0FBUCxHQUFxQixJQUFJQyxHQUFKLENBQVE7QUFDeEJDLFFBQUksYUFEb0I7O0FBR3pCQyxVQUFNO0FBQ0ZDLGdCQUFRLEVBRE47QUFFRkMsbUJBQVcsRUFGVDtBQUdGQyxvQkFBWSxFQUhWO0FBSUZDLHNCQUFjLEtBSlo7QUFLRkMsd0JBQWdCLEtBTGQ7QUFNRkMsbUJBQVUsSUFOUjtBQU9GQyxxQkFBWSxJQVBWO0FBUUZDLHVCQUFlQyxRQUFRQyxJQUFSLENBQWFDLGdCQVIxQjtBQVNGQyxjQUFLLEVBVEg7QUFVRkMsY0FBTSxJQUFJQyxJQUFKLENBQVM7QUFDWEMsZ0JBQUksSUFETztBQUVYQyxtQkFBTyxFQUZJO0FBR1hDLHFCQUFTLEVBSEU7QUFJWEMsa0JBQU0sSUFKSztBQUtYQyxxQkFBUyxJQUxFO0FBTVhDLHlCQUFhLElBTkY7QUFPWEMsNEJBQWdCLElBUEw7QUFRWEMsbUJBQU8sSUFSSTtBQVNYbkIsd0JBQVksRUFURDtBQVVYb0IscUJBQVM7QUFDTFIsb0JBQUksSUFEQztBQUVMUSx5QkFBUyxJQUZKO0FBR0xDLHNCQUFNLElBSEQ7QUFJTEMsMEJBQVUsSUFKTDtBQUtMQywwQkFBVTtBQUxMLGFBVkU7QUFpQlhDLG9CQUFRO0FBQ0paLG9CQUFJLElBREE7QUFFSlksd0JBQVE7QUFGSixhQWpCRztBQXFCWEMscUJBQVM7QUFDTFYsc0JBQU0sSUFERDtBQUVMSyx5QkFBUyxJQUZKO0FBR0xNLDBCQUFVLElBSEw7QUFJTEMseUJBQVMsSUFKSjtBQUtMQywyQkFBVyxJQUxOO0FBTUxDLHdCQUFRO0FBTkg7QUFyQkUsU0FBVCxFQTZCSCxFQUFFQyxTQUFTLEdBQVgsRUE3Qkc7QUFWSixLQUhtQjs7QUE2Q3pCQyxXQUFNO0FBQ0Y1QixtQkFBVSxtQkFBUzZCLEdBQVQsRUFBYTtBQUNuQixnQkFBRyxDQUFDQSxHQUFKLEVBQVE7QUFDSEMsa0JBQUUsY0FBRixFQUFrQkMsV0FBbEIsQ0FBOEIsT0FBOUI7QUFDREQsa0JBQUUsU0FBRixFQUFhRCxHQUFiLENBQWlCLEVBQWpCLEVBRkksQ0FFa0I7QUFDdEJHLDRCQUFZQyxPQUFaLENBQW9CLE1BQXBCLEVBQTRCO0FBQ3hCQyx5QkFBTTtBQURrQixpQkFBNUIsRUFFR0MsSUFGSCxDQUVRLFlBQVk7QUFDaEJDLDRCQUFRQyxHQUFSLENBQVlDLE1BQU1oQyxJQUFOLENBQVcsZ0JBQVgsQ0FBWjtBQUNILGlCQUpEOztBQU1Bd0Isa0JBQUUsOEJBQUYsRUFBa0NTLElBQWxDO0FBQ0E7QUFFSCxhQVpELE1BYUE7QUFDSVQsa0JBQUUsOEJBQUYsRUFBa0NVLElBQWxDO0FBQ0g7QUFDSixTQWxCQztBQW1CRnZDLHFCQUFZLHFCQUFTNEIsR0FBVCxFQUFhO0FBQ3JCLGdCQUFHLENBQUNBLEdBQUosRUFBUTtBQUNIQyxrQkFBRSxpQkFBRixFQUFxQkMsV0FBckIsQ0FBaUMsT0FBakM7QUFDREQsa0JBQUUsa0NBQUYsRUFBc0NELEdBQXRDLENBQTBDLEVBQTFDLEVBRkksQ0FFMkM7QUFDL0NZLGtDQUFrQlIsT0FBbEIsQ0FBMEIsTUFBMUIsRUFBa0M7QUFDOUJDLHlCQUFNO0FBRHdCLGlCQUFsQyxFQUVHQyxJQUZILENBRVEsWUFBWTtBQUNoQkMsNEJBQVFDLEdBQVIsQ0FBWSxLQUFLL0IsSUFBTCxDQUFVLGdCQUFWLENBQVo7QUFDSCxpQkFKRDs7QUFNQXdCLGtCQUFFLCtCQUFGLEVBQW1DUyxJQUFuQztBQUNBO0FBRUgsYUFaRCxNQWFBO0FBQ0lULGtCQUFFLCtCQUFGLEVBQW1DVSxJQUFuQztBQUNIO0FBQ0o7QUFwQ0MsS0E3Q21COztBQW9GekJFLGFBQVM7QUFDTEMscUJBREssMkJBQ1c7QUFBQTs7QUFDWkMsdUJBQVdDLEdBQVgsQ0FBZSxZQUFmLEVBQ0tWLElBREwsQ0FDVSxrQkFBVTtBQUNaLHNCQUFLdEMsVUFBTCxHQUFrQmlELE9BQU9wRCxJQUF6QjtBQUNILGFBSEw7QUFJSCxTQU5JO0FBUUxxRCxjQVJLLGtCQVFFQyxDQVJGLEVBUUs7QUFDTmxCLGNBQUUsY0FBRixFQUFrQm1CLElBQWxCLENBQXVCLFVBQXZCLEVBQW1DLElBQW5DO0FBQ0EsaUJBQUsxQyxJQUFMLENBQVV3QyxNQUFWLENBQWlCLE1BQWpCLEVBQXlCQyxFQUFFRSxhQUFGLENBQWdCQyxNQUF6QyxFQUNLaEIsSUFETCxDQUNVLGtCQUFVO0FBQ1o3Qyx1QkFBTzhELFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCLGFBQWFQLE9BQU9RLElBQTNDO0FBQ0gsYUFITCxFQUlHQyxLQUpILENBSVMsaUJBQVM7QUFDZEMsMkJBQVcsWUFBVTtBQUNyQjFCLHNCQUFFLGNBQUYsRUFBa0JtQixJQUFsQixDQUF1QixVQUF2QixFQUFtQyxLQUFuQztBQUNDLGlCQUZELEVBRUUsSUFGRjtBQUdELGFBUkg7QUFTSCxTQW5CSTtBQXFCTFEsNEJBckJLLGtDQXFCaUI7QUFDbEIzQixjQUFFLGtDQUFGLEVBQXNDNEIsS0FBdEM7QUFDSCxTQXZCSTtBQXlCTEMsZ0JBekJLLG9CQXlCSVgsQ0F6QkosRUF5Qk87O0FBRVYsZ0JBQUlZLFFBQVFaLEVBQUVhLE1BQUYsQ0FBU0QsS0FBVCxJQUFrQlosRUFBRWMsWUFBRixDQUFlRixLQUE3Qzs7QUFHQSxnQkFBSSxDQUFDQSxNQUFNRyxNQUFYLEVBQ0U7QUFDRixpQkFBS0MsV0FBTCxDQUFpQkosS0FBakI7QUFDRCxTQWpDSTtBQW1DTEssdUJBbkNLLDJCQW1DV2pCLENBbkNYLEVBbUNjOztBQUVqQixnQkFBSVksUUFBUVosRUFBRWEsTUFBRixDQUFTRCxLQUFULElBQWtCWixFQUFFYyxZQUFGLENBQWVGLEtBQTdDOztBQUdBLGdCQUFJLENBQUNBLE1BQU1HLE1BQVgsRUFDRTtBQUNGLGlCQUFLRyxrQkFBTCxDQUF3Qk4sS0FBeEI7QUFDRCxTQTNDSTtBQTZDTEksbUJBN0NLLHVCQTZDT0csS0E3Q1AsRUE2Q2M7QUFBQTs7QUFDZixnQkFBSUMsU0FBUyxJQUFJQyxVQUFKLEVBQWI7O0FBRUFELG1CQUFPRSxNQUFQLEdBQWdCLFVBQUN0QixDQUFELEVBQU87QUFDbkJsQixrQkFBRSxjQUFGLEVBQWtCeUMsUUFBbEIsQ0FBMkIsT0FBM0I7QUFDQXZDLDRCQUFZQyxPQUFaLENBQW9CLE1BQXBCLEVBQTRCO0FBQ3hCQyx5QkFBS2MsRUFBRWEsTUFBRixDQUFTZjtBQURVLGlCQUE1QixFQUVHWCxJQUZILENBRVEsWUFBTTtBQUNWLDJCQUFLbkMsU0FBTCxHQUFpQixJQUFqQjtBQUNILGlCQUpEO0FBS0gsYUFQRDtBQVFRb0UsbUJBQU9JLGFBQVAsQ0FBcUJMLE1BQU0sQ0FBTixDQUFyQjtBQUNYLFNBekRJO0FBMERMRCwwQkExREssOEJBMERjQyxLQTFEZCxFQTBEcUI7QUFBQTs7QUFDdEIsZ0JBQUlDLFNBQVMsSUFBSUMsVUFBSixFQUFiOztBQUVBRCxtQkFBT0UsTUFBUCxHQUFnQixVQUFDdEIsQ0FBRCxFQUFPO0FBQ25CbEIsa0JBQUUsaUJBQUYsRUFBcUJ5QyxRQUFyQixDQUE4QixPQUE5QjtBQUNBOUIsa0NBQWtCUixPQUFsQixDQUEwQixNQUExQixFQUFrQztBQUM5QkMseUJBQUtjLEVBQUVhLE1BQUYsQ0FBU2Y7QUFEZ0IsaUJBQWxDLEVBRUdYLElBRkgsQ0FFUSxZQUFNO0FBQ1YsMkJBQUtsQyxXQUFMLEdBQW1CLElBQW5CO0FBQ0gsaUJBSkQ7QUFLSCxhQVBEO0FBUVFtRSxtQkFBT0ksYUFBUCxDQUFxQkwsTUFBTSxDQUFOLENBQXJCO0FBQ1gsU0F0RUk7QUF3RUxNLG1CQXhFSyx5QkF3RVE7QUFBQTs7QUFDVCxpQkFBSzNFLFlBQUwsR0FBb0IsSUFBcEI7QUFDQWtDLHdCQUFZQyxPQUFaLENBQW9CLFFBQXBCLEVBQThCO0FBQzFCeUMsc0JBQU0sUUFEb0I7QUFFMUJDLHNCQUFNO0FBRm9CLGFBQTlCLEVBR0d4QyxJQUhILENBR1MsVUFBQ3lDLElBQUQsRUFBVTtBQUNmQyxzQkFBTUMsSUFBTixDQUFXLDRDQUFYLEVBQXdEO0FBQ3BEQywyQkFBTUg7QUFEOEMsaUJBQXhELEVBRUd6QyxJQUZILENBRVEsa0JBQVU7QUFDZCwyQkFBSzVCLElBQUwsQ0FBVUcsS0FBVixHQUFrQm9DLE9BQU9wRCxJQUF6Qjs7QUFFQThELCtCQUFXLFlBQUk7QUFDWDFCLDBCQUFFLGNBQUYsRUFBa0JrRCxHQUFsQixDQUFzQixrQkFBdEIsRUFBeUMsVUFBVWxDLE9BQU9wRCxJQUFqQixHQUF3QixJQUFqRTtBQUNILHFCQUZELEVBRUUsSUFGRjs7QUFJQSwyQkFBS00sU0FBTCxHQUFpQixJQUFqQjtBQUNBLDJCQUFLRixZQUFMLEdBQW9CLEtBQXBCO0FBRUgsaUJBWkQsRUFZR3lELEtBWkgsQ0FZUyxpQkFBUztBQUNkLDJCQUFLekQsWUFBTCxHQUFvQixLQUFwQjtBQUNILGlCQWREO0FBZUgsYUFuQkQ7QUFvQkgsU0E5Rkk7QUFnR0xtRixxQkFoR0ssMkJBZ0dVO0FBQUE7O0FBQ1gsaUJBQUtsRixjQUFMLEdBQXNCLElBQXRCO0FBQ0EwQyw4QkFBa0JSLE9BQWxCLENBQTBCLFFBQTFCLEVBQW9DO0FBQ2hDeUMsc0JBQU0sUUFEMEI7QUFFaENDLHNCQUFNO0FBRjBCLGFBQXBDLEVBR0d4QyxJQUhILENBR1MsVUFBQ3lDLElBQUQsRUFBVTtBQUNmQyxzQkFBTUMsSUFBTixDQUFXLDJDQUFYLEVBQXVEO0FBQ25EQywyQkFBTUg7QUFENkMsaUJBQXZELEVBRUd6QyxJQUZILENBRVEsa0JBQVU7QUFDZCwyQkFBSzVCLElBQUwsQ0FBVUksT0FBVixHQUFvQm1DLE9BQU9wRCxJQUEzQjs7QUFFQTtBQUNBLDJCQUFLTyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsMkJBQUtGLGNBQUwsR0FBc0IsS0FBdEI7QUFDQStCLHNCQUFFLGVBQUYsRUFBbUJvRCxLQUFuQixDQUF5QixRQUF6QjtBQUNBQywyQkFBT0MsT0FBUCxDQUFlLE9BQUs5RSxJQUFMLENBQVVaLElBQVYsQ0FBZSxrQkFBZixDQUFmO0FBRUgsaUJBWEQsRUFXRzZELEtBWEgsQ0FXUyxpQkFBUztBQUNkLDJCQUFLeEQsY0FBTCxHQUFzQixLQUF0QjtBQUNILGlCQWJEO0FBY0gsYUFsQkQ7QUFtQkg7QUFySEksS0FwRmdCOztBQTRNekJzRixjQUFVO0FBQ05DLDJCQURNLGlDQUNnQjtBQUNsQixnQkFBSSxLQUFLL0UsSUFBTCxDQUFVVixVQUFWLENBQXFCa0UsTUFBekIsRUFBaUM7QUFDN0IsdUJBQU8sRUFBUDs7QUFFSix1QkFBT3pFLE9BQU9pRyxTQUFQLEtBQW9CLE1BQXBCLEdBQTJCLGlCQUFsQztBQUNDO0FBQ0osU0FQSztBQVFOQyx1QkFSTSw2QkFRVztBQUNiLG1CQUFPbEcsT0FBT2lHLFNBQVAsS0FBb0IsU0FBcEIsR0FBOEIsTUFBckM7QUFDSDtBQVZLLEtBNU1lOztBQXlOekJFLGdCQUFZO0FBQ1JDLHFCQUFhcEcsT0FBT3FHLGNBQVAsQ0FBc0JDO0FBRDNCLEtBek5hOztBQTZOekJDLFdBN055QixxQkE2TmY7QUFBQTs7QUFDTixhQUFLbEQsYUFBTDs7QUFFQW1ELGNBQU1DLE1BQU4sQ0FBYSw0QkFBYixFQUEyQyxZQUFNO0FBQzdDLG1CQUFLakcsWUFBTCxHQUFvQixJQUFwQjtBQUNILFNBRkQ7O0FBSUFnRyxjQUFNQyxNQUFOLENBQWEsMkJBQWIsRUFBMEMsVUFBQ0MsR0FBRCxFQUFTO0FBQy9DLG1CQUFLekYsSUFBTCxDQUFVRyxLQUFWLEdBQWtCc0YsR0FBbEI7QUFDQSxtQkFBS2xHLFlBQUwsR0FBb0IsS0FBcEI7QUFDSCxTQUhEOztBQUtBZ0csY0FBTUMsTUFBTixDQUFhLDhCQUFiLEVBQTZDLFlBQU07QUFDL0MsbUJBQUtoRyxjQUFMLEdBQXNCLElBQXRCO0FBQ0gsU0FGRDs7QUFJQStGLGNBQU1DLE1BQU4sQ0FBYSw2QkFBYixFQUE0QyxVQUFDQyxHQUFELEVBQVM7QUFDakQsbUJBQUt6RixJQUFMLENBQVVJLE9BQVYsR0FBb0JxRixHQUFwQjtBQUNBLG1CQUFLakcsY0FBTCxHQUFzQixLQUF0QjtBQUNILFNBSEQ7O0FBS0E4RSxjQUFNaEMsR0FBTixDQUFVLDZCQUFWLEVBQ0tWLElBREwsQ0FDVSxrQkFBVTtBQUNoQixtQkFBSzdCLElBQUwsR0FBWXdDLE9BQU9wRCxJQUFuQjtBQUNBLG1CQUFLdUcsY0FBTCxHQUFzQm5ELE9BQU9wRCxJQUFQLENBQVlBLElBQVosQ0FBaUIsU0FBakIsQ0FBdEI7QUFDSCxTQUpEO0FBTUg7QUF4UHdCLENBQVIsQ0FBckI7O0FBOFBBc0MsY0FBY0YsRUFBRSxjQUFGLEVBQWtCRyxPQUFsQixDQUEwQjtBQUNwQ0MsU0FBSyxtRUFEK0I7QUFFcENnRSxjQUFVO0FBQ05DLGVBQU8sSUFERDtBQUVOQyxnQkFBUTtBQUZGO0FBSVY7QUFOb0MsQ0FBMUIsQ0FBZDs7QUFTQTNELG9CQUFvQlgsRUFBRSxpQkFBRixFQUFxQkcsT0FBckIsQ0FBNkI7QUFDN0NDLFNBQUssNEJBRHdDO0FBRTdDZ0UsY0FBVTtBQUNOQyxlQUFPLEdBREQ7QUFFTkMsZ0JBQVEsR0FGRjtBQUdOMUIsY0FBSztBQUhDO0FBRm1DLENBQTdCLENBQXBCOztBQVNBOztBQUVBOzs7O0FBS0EsU0FBUzJCLFdBQVQsQ0FBcUJ2RCxNQUFyQixFQUE2QjtBQUN6QixRQUFJd0QsSUFBSjs7QUFFQSxRQUFJeEQsT0FBT3lELEdBQVgsRUFBZ0I7QUFDWkQsZUFBTyxlQUFleEQsT0FBT3lELEdBQXRCLEdBQTRCLE1BQW5DO0FBQ0g7QUFDRDtBQUNBQyxTQUFLO0FBQ0RDLGVBQU8sRUFETjtBQUVESCxjQUFNLElBRkw7QUFHREksY0FBTUosSUFITDtBQUlESywyQkFBbUI7QUFKbEIsS0FBTDtBQU1BbkQsZUFBVyxZQUFVO0FBQ2pCMUIsVUFBRSxjQUFGLEVBQWtCa0QsR0FBbEIsQ0FBc0IsUUFBdEIsRUFBZ0MsWUFBVztBQUN2QyxnQkFBSTRCLE1BQU0sQ0FBQyxDQUFELElBQU05RSxFQUFFLElBQUYsRUFBUXNFLE1BQVIsS0FBbUIsQ0FBekIsQ0FBVjtBQUFBLGdCQUNJUyxPQUFPLENBQUMsQ0FBRCxJQUFNL0UsRUFBRSxJQUFGLEVBQVFxRSxLQUFSLEtBQWtCLENBQXhCLENBRFg7O0FBR0EsbUJBQU9TLE1BQU0sU0FBTixHQUFrQkMsSUFBbEIsR0FBeUIsSUFBaEM7QUFDSCxTQUxEO0FBTUgsS0FQRCxFQU9HLENBUEg7QUFRSCxDIiwiZmlsZSI6ImpzL3BhZ2VzL3N0b3JlL2NyZWF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDUzKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmNWZjZmYwZTFmMDgwMDk3Y2Y0NiIsIndpbmRvdy5zdG9yZUNyZWF0ZSA9IG5ldyBWdWUoe1xyXG4gICAgIGVsOiAnI3N0b3JlLWZvcm0nLFxyXG5cclxuICAgIGRhdGE6IHtcclxuICAgICAgICBjaXRpZXM6IFtdLFxyXG4gICAgICAgIHByb3ZpbmNlczogW10sXHJcbiAgICAgICAgY2F0ZWdvcmllczogW10sXHJcbiAgICAgICAgY292ZXJMb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICBwcm9maWxlTG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgdGVtcENvdmVyOm51bGwsXHJcbiAgICAgICAgdGVtcFByaW1hcnk6bnVsbCxcclxuICAgICAgICBkb2NzX3ZlcmlmaWVkOiBMYXJhdmVsLnVzZXIuaXNfZG9jc192ZXJpZmllZCxcclxuICAgICAgICBsYW5nOltdLFxyXG4gICAgICAgIGZvcm06IG5ldyBGb3JtKHtcclxuICAgICAgICAgICAgaWQ6IG51bGwsXHJcbiAgICAgICAgICAgIGNvdmVyOiAnJyxcclxuICAgICAgICAgICAgcHJvZmlsZTogJycsXHJcbiAgICAgICAgICAgIG5hbWU6IG51bGwsXHJcbiAgICAgICAgICAgIGNuX25hbWU6IG51bGwsXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBudWxsLFxyXG4gICAgICAgICAgICBjbl9kZXNjcmlwdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgZW1haWw6IG51bGwsXHJcbiAgICAgICAgICAgIGNhdGVnb3JpZXM6IFtdLFxyXG4gICAgICAgICAgICBhZGRyZXNzOiB7XHJcbiAgICAgICAgICAgICAgICBpZDogbnVsbCxcclxuICAgICAgICAgICAgICAgIGFkZHJlc3M6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBjaXR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcHJvdmluY2U6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBsYW5kbWFyazogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBudW1iZXI6IHtcclxuICAgICAgICAgICAgICAgIGlkOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgbnVtYmVyOiBudWxsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGNvbXBhbnk6IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgZmFjZWJvb2s6IG51bGwsXHJcbiAgICAgICAgICAgICAgICB0d2l0dGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgaW5zdGFncmFtOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgd2VjaGF0OiBudWxsLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgeyBiYXNlVVJMOiAnLycgfSlcclxuICAgIH0sXHJcblxyXG4gICAgd2F0Y2g6e1xyXG4gICAgICAgIHRlbXBDb3ZlcjpmdW5jdGlvbih2YWwpe1xyXG4gICAgICAgICAgICBpZighdmFsKXtcclxuICAgICAgICAgICAgICAgICAkKCcjdXBsb2FkLWRlbW8nKS5yZW1vdmVDbGFzcygncmVhZHknKTtcclxuICAgICAgICAgICAgICAgICQoJyN1cGxvYWQnKS52YWwoJycpOyAvLyB0aGlzIHdpbGwgY2xlYXIgdGhlIGlucHV0IHZhbC5cclxuICAgICAgICAgICAgICAgICR1cGxvYWRDcm9wLmNyb3BwaWUoJ2JpbmQnLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsIDogJydcclxuICAgICAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCR0aGlzLmxhbmdbJ3Jlc2V0LWNvbXBsZXRlJ10pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgJCgnI3VwbG9hZC1kZW1vIC5jci1zbGlkZXItd3JhcCcpLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIC8vICQoJyN1cGxvYWQtZGVtbyAuY3ItYm91bmRhcnknKS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgJCgnI3VwbG9hZC1kZW1vIC5jci1zbGlkZXItd3JhcCcpLnNob3coKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdGVtcFByaW1hcnk6ZnVuY3Rpb24odmFsKXtcclxuICAgICAgICAgICAgaWYoIXZhbCl7XHJcbiAgICAgICAgICAgICAgICAgJCgnI3VwbG9hZC1wcmltYXJ5JykucmVtb3ZlQ2xhc3MoJ3JlYWR5Jyk7XHJcbiAgICAgICAgICAgICAgICAkKCcjbW9kYWxQcmltYXJ5IGlucHV0W3R5cGU9XCJmaWxlXCJdJykudmFsKCcnKTsgLy8gdGhpcyB3aWxsIGNsZWFyIHRoZSBpbnB1dCB2YWwuXHJcbiAgICAgICAgICAgICAgICAkdXBsb2FkUHJpbWFyeVBpYy5jcm9wcGllKCdiaW5kJywge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybCA6ICcnXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmxhbmdbJ3Jlc2V0LWNvbXBsZXRlJ10pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgJCgnI21vZGFsUHJpbWFyeSAuY3Itc2xpZGVyLXdyYXAnKS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAvLyAkKCcjdXBsb2FkLWRlbW8gLmNyLWJvdW5kYXJ5JykuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICQoJyNtb2RhbFByaW1hcnkgLmNyLXNsaWRlci13cmFwJykuc2hvdygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgZ2V0Q2F0ZWdvcmllcygpIHtcclxuICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJ2NhdGVnb3JpZXMnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhdGVnb3JpZXMgPSByZXN1bHQuZGF0YTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHN1Ym1pdChlKSB7XHJcbiAgICAgICAgICAgICQoXCIjc3RvcmVjcmVhdGVcIikucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcclxuICAgICAgICAgICAgdGhpcy5mb3JtLnN1Ym1pdCgncG9zdCcsIGUuY3VycmVudFRhcmdldC5hY3Rpb24pXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9zdG9yZXMvJyArIHJlc3VsdC5zbHVnO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgJChcIiNzdG9yZWNyZWF0ZVwiKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIH0sMTAwMCk7XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdHJpZ2dlclVwbG9hZFByaW1hcnkoKXtcclxuICAgICAgICAgICAgJCgnI21vZGFsUHJpbWFyeSBpbnB1dFt0eXBlPVwiZmlsZVwiXScpLmNsaWNrKCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgcmVhZEZpbGUoZSkge1xyXG5cclxuICAgICAgICAgIHZhciBmaWxlcyA9IGUudGFyZ2V0LmZpbGVzIHx8IGUuZGF0YVRyYW5zZmVyLmZpbGVzO1xyXG5cclxuXHJcbiAgICAgICAgICBpZiAoIWZpbGVzLmxlbmd0aClcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgdGhpcy5jcmVhdGVJbWFnZShmaWxlcyk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgcmVhZEZpbGVQcmltYXJ5KGUpIHtcclxuXHJcbiAgICAgICAgICB2YXIgZmlsZXMgPSBlLnRhcmdldC5maWxlcyB8fCBlLmRhdGFUcmFuc2Zlci5maWxlcztcclxuXHJcblxyXG4gICAgICAgICAgaWYgKCFmaWxlcy5sZW5ndGgpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIHRoaXMuY3JlYXRlUHJpbWFyeUltYWdlKGZpbGVzKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjcmVhdGVJbWFnZShpbnB1dCkge1xyXG4gICAgICAgICAgICB2YXIgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHJlYWRlci5vbmxvYWQgPSAoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgJCgnI3VwbG9hZC1kZW1vJykuYWRkQ2xhc3MoJ3JlYWR5Jyk7XHJcbiAgICAgICAgICAgICAgICAkdXBsb2FkQ3JvcC5jcm9wcGllKCdiaW5kJywge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybDogZS50YXJnZXQucmVzdWx0XHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRlbXBDb3ZlciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGlucHV0WzBdKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNyZWF0ZVByaW1hcnlJbWFnZShpbnB1dCkge1xyXG4gICAgICAgICAgICB2YXIgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHJlYWRlci5vbmxvYWQgPSAoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgJCgnI3VwbG9hZC1wcmltYXJ5JykuYWRkQ2xhc3MoJ3JlYWR5Jyk7XHJcbiAgICAgICAgICAgICAgICAkdXBsb2FkUHJpbWFyeVBpYy5jcm9wcGllKCdiaW5kJywge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybDogZS50YXJnZXQucmVzdWx0XHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRlbXBQcmltYXJ5ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoaW5wdXRbMF0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHN1Ym1pdENvdmVyKCl7XHJcbiAgICAgICAgICAgIHRoaXMuY292ZXJMb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgJHVwbG9hZENyb3AuY3JvcHBpZSgncmVzdWx0Jywge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2NhbnZhcycsXHJcbiAgICAgICAgICAgICAgICBzaXplOiAndmlld3BvcnQnXHJcbiAgICAgICAgICAgIH0pLnRoZW4oIChyZXNwKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBheGlvcy5wb3N0KCcvY29tbW9uL3Jlc2l6ZS1pbWFnZT93aWR0aD0xMTQwJmhlaWdodD0zMDAnLHtcclxuICAgICAgICAgICAgICAgICAgICBpbWFnZTpyZXNwXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLmNvdmVyID0gcmVzdWx0LmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCk9PntcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3VwbG9hZC1kZW1vJykuY3NzKCdiYWNrZ3JvdW5kLWltYWdlJyxcInVybCgnXCIgKyByZXN1bHQuZGF0YSArIFwiJylcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwxMDAwKVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRlbXBDb3ZlciA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3ZlckxvYWRpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3ZlckxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzdWJtaXRQcmltYXJ5KCl7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZmlsZUxvYWRpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAkdXBsb2FkUHJpbWFyeVBpYy5jcm9wcGllKCdyZXN1bHQnLCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnY2FudmFzJyxcclxuICAgICAgICAgICAgICAgIHNpemU6ICd2aWV3cG9ydCdcclxuICAgICAgICAgICAgfSkudGhlbiggKHJlc3ApID0+IHtcclxuICAgICAgICAgICAgICAgIGF4aW9zLnBvc3QoJy9jb21tb24vcmVzaXplLWltYWdlP3dpZHRoPTMwMCZoZWlnaHQ9MzAwJyx7XHJcbiAgICAgICAgICAgICAgICAgICAgaW1hZ2U6cmVzcFxyXG4gICAgICAgICAgICAgICAgfSkudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybS5wcm9maWxlID0gcmVzdWx0LmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vICQoJyN1cGxvYWQtZGVtbycpLmNzcygnYmFja2dyb3VuZC1pbWFnZScsXCJ1cmwoJ1wiICsgcmVzdWx0LmRhdGEgKyBcIicpXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGVtcFByaW1hcnkgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZUxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAkKCcjbW9kYWxQcmltYXJ5JykubW9kYWwoJ3RvZ2dsZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZy5kYXRhWydwcm9mLXBpYy1zZXQtbXNnJ10pXHJcblxyXG4gICAgICAgICAgICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZUxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgY2F0ZWdvcnlQbGFjZWhvbGRlcigpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZm9ybS5jYXRlZ29yaWVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcmV0dXJuIHdpbmRvdy5pc0NoaW5lc2UoKT8gJ+mAieaLqeexu+WIqyc6J1NlbGVjdCBDYXRlZ29yeSc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGdldENhdGVnb3J5TmFtZSgpe1xyXG4gICAgICAgICAgICByZXR1cm4gd2luZG93LmlzQ2hpbmVzZSgpPyAnbmFtZV9jbic6J25hbWUnO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAgIE11bHRpc2VsZWN0OiB3aW5kb3cuVnVlTXVsdGlzZWxlY3QuZGVmYXVsdFxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0Q2F0ZWdvcmllcygpO1xyXG5cclxuICAgICAgICBFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQuY292ZXJMb2FkaW5nJywgKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmNvdmVyTG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignSW1nZmlsZXVwbG9hZC5jb3Zlck9uTG9hZCcsIChpbWcpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtLmNvdmVyID0gaW1nO1xyXG4gICAgICAgICAgICB0aGlzLmNvdmVyTG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQucHJvZmlsZUxvYWRpbmcnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZmlsZUxvYWRpbmcgPSB0cnVlO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQucHJvZmlsZU9uTG9hZCcsIChpbWcpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtLnByb2ZpbGUgPSBpbWc7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZmlsZUxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL3N0b3JlLW1hbmFnZW1lbnQnKVxyXG4gICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxhbmcgPSByZXN1bHQuZGF0YTtcclxuICAgICAgICAgICAgdGhpcy5jYXRQbGFjZWhvbGRlciA9IHJlc3VsdC5kYXRhLmRhdGFbJ3NlbC1jYXQnXTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICB9LFxyXG59KTtcclxuICAgXHJcblxyXG5cclxuXHJcbiR1cGxvYWRDcm9wID0gJCgnI3VwbG9hZC1kZW1vJykuY3JvcHBpZSh7XHJcbiAgICB1cmw6ICdodHRwczovL2R1bW15aW1hZ2UuY29tLzExNDB4MzAwL2IwYjBiMC9mNWY1ZjUmdGV4dD0xMTQwcHgreCszMDBweCcgLFxyXG4gICAgdmlld3BvcnQ6IHtcclxuICAgICAgICB3aWR0aDogMTE0MCxcclxuICAgICAgICBoZWlnaHQ6IDMwMCxcclxuICAgIH0sXHJcbiAgICAvLyBlbmFibGVFeGlmOiB0cnVlXHJcbn0pO1xyXG5cclxuJHVwbG9hZFByaW1hcnlQaWMgPSAkKCcjdXBsb2FkLXByaW1hcnknKS5jcm9wcGllKHtcclxuICAgIHVybDogJy9pbWFnZXMvYXZhdGFyL2RlZmF1bHQuanBnJyAsXHJcbiAgICB2aWV3cG9ydDoge1xyXG4gICAgICAgIHdpZHRoOiAzMDAsXHJcbiAgICAgICAgaGVpZ2h0OiAzMDAsXHJcbiAgICAgICAgdHlwZTonY2lyY2xlJ1xyXG4gICAgfSxcclxufSk7XHJcblxyXG4vLyAkKCcjdXBsb2FkJykub24oJ2NoYW5nZScsIGZ1bmN0aW9uICgpIHsgcmVhZEZpbGUodGhpcyk7IH0pO1xyXG5cclxuLyokKCcjbnVtYmVyJykuaW5wdXRtYXNrKHtcclxuICAgIG1hc2s6ICcoOTk5OSkgOTk5LTk5OTknXHJcbn0pKi9cclxuXHJcbiAgICBcclxuZnVuY3Rpb24gcG9wdXBSZXN1bHQocmVzdWx0KSB7XHJcbiAgICB2YXIgaHRtbDtcclxuICAgXHJcbiAgICBpZiAocmVzdWx0LnNyYykge1xyXG4gICAgICAgIGh0bWwgPSAnPGltZyBzcmM9XCInICsgcmVzdWx0LnNyYyArICdcIiAvPic7XHJcbiAgICB9XHJcbiAgICByZXR1cm47XHJcbiAgICBzd2FsKHtcclxuICAgICAgICB0aXRsZTogJycsXHJcbiAgICAgICAgaHRtbDogdHJ1ZSxcclxuICAgICAgICB0ZXh0OiBodG1sLFxyXG4gICAgICAgIGFsbG93T3V0c2lkZUNsaWNrOiB0cnVlXHJcbiAgICB9KTtcclxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAkKCcuc3dlZXQtYWxlcnQnKS5jc3MoJ21hcmdpbicsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICB2YXIgdG9wID0gLTEgKiAoJCh0aGlzKS5oZWlnaHQoKSAvIDIpLFxyXG4gICAgICAgICAgICAgICAgbGVmdCA9IC0xICogKCQodGhpcykud2lkdGgoKSAvIDIpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRvcCArICdweCAwIDAgJyArIGxlZnQgKyAncHgnO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSwgMSk7XHJcbn1cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9zdG9yZS9jcmVhdGUuanMiXSwic291cmNlUm9vdCI6IiJ9