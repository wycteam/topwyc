/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 454);
/******/ })
/************************************************************************/
/******/ ({

/***/ 204:
/***/ (function(module, exports) {

var data = window.Laravel.data;

window.storeEdit = new Vue({
    el: '#store-form',

    data: {
        cities: [],
        provinces: [],
        categories: [],
        coverLoading: false,
        profileLoading: false,
        tempCover: null,
        tempPrimary: null,
        form: {},
        lang: []
    },

    watch: {
        tempCover: function tempCover(val) {
            if (!val) {
                $('#upload-demo').removeClass('ready');
                $('#upload').val(''); // this will clear the input val.
                $uploadCrop.croppie('bind', {
                    url: ''
                }).then(function () {
                    console.log('reset complete');
                });

                $('#upload-demo .cr-slider-wrap').hide();
                // $('#upload-demo .cr-boundary').hide();
            } else {
                $('#upload-demo .cr-slider-wrap').show();
            }
        },
        tempPrimary: function tempPrimary(val) {
            if (!val) {
                $('#upload-primary').removeClass('ready');
                $('#modalPrimary input[type="file"]').val(''); // this will clear the input val.
                $uploadPrimaryPic.croppie('bind', {
                    url: ''
                }).then(function () {
                    console.log('reset complete');
                });

                $('#modalPrimary .cr-slider-wrap').hide();
                // $('#upload-demo .cr-boundary').hide();
            } else {
                $('#modalPrimary .cr-slider-wrap').show();
            }
        }
    },

    methods: {
        getCategories: function getCategories() {
            var _this = this;

            axiosAPIv1.get('categories').then(function (result) {
                _this.categories = result.data;
            });
        },
        submit: function submit(e) {
            this.form.submit('put', e.currentTarget.action).then(function (result) {
                // console.log(result);
                window.location.href = '/stores/' + result.slug;
            }).catch($.noop);
        },
        triggerUploadPrimary: function triggerUploadPrimary() {
            $('#modalPrimary input[type="file"]').click();
        },
        readFile: function readFile(e) {

            var files = e.target.files || e.dataTransfer.files;

            if (!files.length) return;
            this.createImage(files);
        },
        readFilePrimary: function readFilePrimary(e) {

            var files = e.target.files || e.dataTransfer.files;

            if (!files.length) return;
            this.createPrimaryImage(files);
        },
        createImage: function createImage(input) {
            var _this2 = this;

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload-demo').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    _this2.tempCover = true;
                });
            };
            reader.readAsDataURL(input[0]);
        },
        createPrimaryImage: function createPrimaryImage(input) {
            var _this3 = this;

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload-primary').addClass('ready');
                $uploadPrimaryPic.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    _this3.tempPrimary = true;
                });
            };
            reader.readAsDataURL(input[0]);
        },
        submitCover: function submitCover() {
            var _this4 = this;

            this.coverLoading = true;
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                axios.post('/common/resize-image?width=1140&height=300', {
                    image: resp
                }).then(function (result) {
                    _this4.form.cover = result.data;

                    setTimeout(function () {
                        $('#upload-demo').css('background-image', "url('" + result.data + "')");
                    }, 500);

                    _this4.tempCover = null;
                    _this4.coverLoading = false;
                }).catch(function (error) {
                    _this4.coverLoading = false;
                });
            });
        },
        submitPrimary: function submitPrimary() {
            var _this5 = this;

            this.profileLoading = true;
            $uploadPrimaryPic.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                axios.post('/common/resize-image?width=300&height=300', {
                    image: resp
                }).then(function (result) {
                    _this5.form.profile = result.data;

                    // $('#upload-demo').css('background-image',"url('" + result.data + "')");
                    _this5.tempPrimary = null;
                    _this5.profileLoading = false;
                    $('#modalPrimary').modal('toggle');
                    toastr.success(_this5.lang.data['prof-pic-set-msg']);
                }).catch(function (error) {
                    _this5.profileLoading = false;
                });
            });
        }
    },

    computed: {
        categoryPlaceholder: function categoryPlaceholder() {
            if (this.form.categories && this.form.categories.length) {
                return '';
            }

            return window.isChinese() ? '选择类别' : 'Select Category';
        },
        getCategoryName: function getCategoryName() {
            return window.isChinese() ? 'name_cn' : 'name';
        }
    },

    components: {
        Multiselect: window.VueMultiselect.default
    },

    created: function created() {
        var _this6 = this;

        axios.all([this.getCategories()]);

        axiosAPIv1.get('/countorder/' + this.form.id).then(function (result) {
            console.log(result.data.length + '-' + _this6.form.id);
            if (result.data.length > 0) {
                $('#address, #city, #province, #landmark').attr("disabled", "true");
            }
        });

        this.form = new Form($.extend(data, {
            cover: data.cover_image,
            profile: data.profile_image
        }), { baseURL: '/' });

        Event.listen('Imgfileupload.coverLoading', function () {
            _this6.coverLoading = true;
        });

        Event.listen('Imgfileupload.coverOnLoad', function (img) {
            _this6.form.cover = img;
            _this6.coverLoading = false;
        });

        Event.listen('Imgfileupload.profileLoading', function () {
            _this6.profileLoading = true;
        });

        Event.listen('Imgfileupload.profileOnLoad', function (img) {
            _this6.form.profile = img;
            _this6.profileLoading = false;
        });

        axios.get('/translate/store-management').then(function (result) {
            _this6.lang = result.data;
        });
    }
});

$uploadCrop = $('#upload-demo').croppie({
    viewport: {
        width: 1140,
        height: 300
    }
    // enableExif: true
});

$uploadPrimaryPic = $('#upload-primary').croppie({
    viewport: {
        width: 300,
        height: 300,
        type: 'circle'
    }
});

/*$('#number').inputmask({
    mask: '(9999) 999-9999'
})*/

/***/ }),

/***/ 454:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(204);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvc3RvcmUvZWRpdC5qcyJdLCJuYW1lcyI6WyJkYXRhIiwid2luZG93IiwiTGFyYXZlbCIsInN0b3JlRWRpdCIsIlZ1ZSIsImVsIiwiY2l0aWVzIiwicHJvdmluY2VzIiwiY2F0ZWdvcmllcyIsImNvdmVyTG9hZGluZyIsInByb2ZpbGVMb2FkaW5nIiwidGVtcENvdmVyIiwidGVtcFByaW1hcnkiLCJmb3JtIiwibGFuZyIsIndhdGNoIiwidmFsIiwiJCIsInJlbW92ZUNsYXNzIiwiJHVwbG9hZENyb3AiLCJjcm9wcGllIiwidXJsIiwidGhlbiIsImNvbnNvbGUiLCJsb2ciLCJoaWRlIiwic2hvdyIsIiR1cGxvYWRQcmltYXJ5UGljIiwibWV0aG9kcyIsImdldENhdGVnb3JpZXMiLCJheGlvc0FQSXYxIiwiZ2V0IiwicmVzdWx0Iiwic3VibWl0IiwiZSIsImN1cnJlbnRUYXJnZXQiLCJhY3Rpb24iLCJsb2NhdGlvbiIsImhyZWYiLCJzbHVnIiwiY2F0Y2giLCJub29wIiwidHJpZ2dlclVwbG9hZFByaW1hcnkiLCJjbGljayIsInJlYWRGaWxlIiwiZmlsZXMiLCJ0YXJnZXQiLCJkYXRhVHJhbnNmZXIiLCJsZW5ndGgiLCJjcmVhdGVJbWFnZSIsInJlYWRGaWxlUHJpbWFyeSIsImNyZWF0ZVByaW1hcnlJbWFnZSIsImlucHV0IiwicmVhZGVyIiwiRmlsZVJlYWRlciIsIm9ubG9hZCIsImFkZENsYXNzIiwicmVhZEFzRGF0YVVSTCIsInN1Ym1pdENvdmVyIiwidHlwZSIsInNpemUiLCJyZXNwIiwiYXhpb3MiLCJwb3N0IiwiaW1hZ2UiLCJjb3ZlciIsInNldFRpbWVvdXQiLCJjc3MiLCJzdWJtaXRQcmltYXJ5IiwicHJvZmlsZSIsIm1vZGFsIiwidG9hc3RyIiwic3VjY2VzcyIsImNvbXB1dGVkIiwiY2F0ZWdvcnlQbGFjZWhvbGRlciIsImlzQ2hpbmVzZSIsImdldENhdGVnb3J5TmFtZSIsImNvbXBvbmVudHMiLCJNdWx0aXNlbGVjdCIsIlZ1ZU11bHRpc2VsZWN0IiwiZGVmYXVsdCIsImNyZWF0ZWQiLCJhbGwiLCJpZCIsImF0dHIiLCJGb3JtIiwiZXh0ZW5kIiwiY292ZXJfaW1hZ2UiLCJwcm9maWxlX2ltYWdlIiwiYmFzZVVSTCIsIkV2ZW50IiwibGlzdGVuIiwiaW1nIiwidmlld3BvcnQiLCJ3aWR0aCIsImhlaWdodCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQSxJQUFJQSxPQUFPQyxPQUFPQyxPQUFQLENBQWVGLElBQTFCOztBQUVBQyxPQUFPRSxTQUFQLEdBQW1CLElBQUlDLEdBQUosQ0FBUTtBQUN2QkMsUUFBSSxhQURtQjs7QUFHdkJMLFVBQU07QUFDRk0sZ0JBQVEsRUFETjtBQUVGQyxtQkFBVyxFQUZUO0FBR0ZDLG9CQUFZLEVBSFY7QUFJRkMsc0JBQWMsS0FKWjtBQUtGQyx3QkFBZ0IsS0FMZDtBQU1GQyxtQkFBVSxJQU5SO0FBT0ZDLHFCQUFZLElBUFY7QUFRRkMsY0FBTSxFQVJKO0FBU0ZDLGNBQUs7QUFUSCxLQUhpQjs7QUFldEJDLFdBQU07QUFDSEosbUJBQVUsbUJBQVNLLEdBQVQsRUFBYTtBQUNuQixnQkFBRyxDQUFDQSxHQUFKLEVBQVE7QUFDSEMsa0JBQUUsY0FBRixFQUFrQkMsV0FBbEIsQ0FBOEIsT0FBOUI7QUFDREQsa0JBQUUsU0FBRixFQUFhRCxHQUFiLENBQWlCLEVBQWpCLEVBRkksQ0FFa0I7QUFDdEJHLDRCQUFZQyxPQUFaLENBQW9CLE1BQXBCLEVBQTRCO0FBQ3hCQyx5QkFBTTtBQURrQixpQkFBNUIsRUFFR0MsSUFGSCxDQUVRLFlBQVk7QUFDaEJDLDRCQUFRQyxHQUFSLENBQVksZ0JBQVo7QUFDSCxpQkFKRDs7QUFNQVAsa0JBQUUsOEJBQUYsRUFBa0NRLElBQWxDO0FBQ0E7QUFFSCxhQVpELE1BYUE7QUFDSVIsa0JBQUUsOEJBQUYsRUFBa0NTLElBQWxDO0FBQ0g7QUFDSixTQWxCRTtBQW1CSGQscUJBQVkscUJBQVNJLEdBQVQsRUFBYTtBQUNyQixnQkFBRyxDQUFDQSxHQUFKLEVBQVE7QUFDSEMsa0JBQUUsaUJBQUYsRUFBcUJDLFdBQXJCLENBQWlDLE9BQWpDO0FBQ0RELGtCQUFFLGtDQUFGLEVBQXNDRCxHQUF0QyxDQUEwQyxFQUExQyxFQUZJLENBRTJDO0FBQy9DVyxrQ0FBa0JQLE9BQWxCLENBQTBCLE1BQTFCLEVBQWtDO0FBQzlCQyx5QkFBTTtBQUR3QixpQkFBbEMsRUFFR0MsSUFGSCxDQUVRLFlBQVk7QUFDaEJDLDRCQUFRQyxHQUFSLENBQVksZ0JBQVo7QUFDSCxpQkFKRDs7QUFNQVAsa0JBQUUsK0JBQUYsRUFBbUNRLElBQW5DO0FBQ0E7QUFFSCxhQVpELE1BYUE7QUFDSVIsa0JBQUUsK0JBQUYsRUFBbUNTLElBQW5DO0FBQ0g7QUFDSjtBQXBDRSxLQWZnQjs7QUFzRHZCRSxhQUFTO0FBQ0xDLHFCQURLLDJCQUNXO0FBQUE7O0FBQ1pDLHVCQUFXQyxHQUFYLENBQWUsWUFBZixFQUNLVCxJQURMLENBQ1Usa0JBQVU7QUFDWixzQkFBS2QsVUFBTCxHQUFrQndCLE9BQU9oQyxJQUF6QjtBQUNILGFBSEw7QUFJSCxTQU5JO0FBUUxpQyxjQVJLLGtCQVFFQyxDQVJGLEVBUUs7QUFDTixpQkFBS3JCLElBQUwsQ0FBVW9CLE1BQVYsQ0FBaUIsS0FBakIsRUFBd0JDLEVBQUVDLGFBQUYsQ0FBZ0JDLE1BQXhDLEVBQ0tkLElBREwsQ0FDVSxrQkFBVTtBQUNaO0FBQ0FyQix1QkFBT29DLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCLGFBQWFOLE9BQU9PLElBQTNDO0FBQ0gsYUFKTCxFQUtLQyxLQUxMLENBS1d2QixFQUFFd0IsSUFMYjtBQU1ILFNBZkk7QUFpQkxDLDRCQWpCSyxrQ0FpQmlCO0FBQ2xCekIsY0FBRSxrQ0FBRixFQUFzQzBCLEtBQXRDO0FBQ0gsU0FuQkk7QUFxQkxDLGdCQXJCSyxvQkFxQklWLENBckJKLEVBcUJPOztBQUVWLGdCQUFJVyxRQUFRWCxFQUFFWSxNQUFGLENBQVNELEtBQVQsSUFBa0JYLEVBQUVhLFlBQUYsQ0FBZUYsS0FBN0M7O0FBR0EsZ0JBQUksQ0FBQ0EsTUFBTUcsTUFBWCxFQUNFO0FBQ0YsaUJBQUtDLFdBQUwsQ0FBaUJKLEtBQWpCO0FBQ0QsU0E3Qkk7QUErQkxLLHVCQS9CSywyQkErQldoQixDQS9CWCxFQStCYzs7QUFFakIsZ0JBQUlXLFFBQVFYLEVBQUVZLE1BQUYsQ0FBU0QsS0FBVCxJQUFrQlgsRUFBRWEsWUFBRixDQUFlRixLQUE3Qzs7QUFHQSxnQkFBSSxDQUFDQSxNQUFNRyxNQUFYLEVBQ0U7QUFDRixpQkFBS0csa0JBQUwsQ0FBd0JOLEtBQXhCO0FBQ0QsU0F2Q0k7QUF5Q0xJLG1CQXpDSyx1QkF5Q09HLEtBekNQLEVBeUNjO0FBQUE7O0FBQ2YsZ0JBQUlDLFNBQVMsSUFBSUMsVUFBSixFQUFiOztBQUVBRCxtQkFBT0UsTUFBUCxHQUFnQixVQUFDckIsQ0FBRCxFQUFPO0FBQ25CakIsa0JBQUUsY0FBRixFQUFrQnVDLFFBQWxCLENBQTJCLE9BQTNCO0FBQ0FyQyw0QkFBWUMsT0FBWixDQUFvQixNQUFwQixFQUE0QjtBQUN4QkMseUJBQUthLEVBQUVZLE1BQUYsQ0FBU2Q7QUFEVSxpQkFBNUIsRUFFR1YsSUFGSCxDQUVRLFlBQU07QUFDViwyQkFBS1gsU0FBTCxHQUFpQixJQUFqQjtBQUNILGlCQUpEO0FBS0gsYUFQRDtBQVFRMEMsbUJBQU9JLGFBQVAsQ0FBcUJMLE1BQU0sQ0FBTixDQUFyQjtBQUNYLFNBckRJO0FBc0RMRCwwQkF0REssOEJBc0RjQyxLQXREZCxFQXNEcUI7QUFBQTs7QUFDdEIsZ0JBQUlDLFNBQVMsSUFBSUMsVUFBSixFQUFiOztBQUVBRCxtQkFBT0UsTUFBUCxHQUFnQixVQUFDckIsQ0FBRCxFQUFPO0FBQ25CakIsa0JBQUUsaUJBQUYsRUFBcUJ1QyxRQUFyQixDQUE4QixPQUE5QjtBQUNBN0Isa0NBQWtCUCxPQUFsQixDQUEwQixNQUExQixFQUFrQztBQUM5QkMseUJBQUthLEVBQUVZLE1BQUYsQ0FBU2Q7QUFEZ0IsaUJBQWxDLEVBRUdWLElBRkgsQ0FFUSxZQUFNO0FBQ1YsMkJBQUtWLFdBQUwsR0FBbUIsSUFBbkI7QUFDSCxpQkFKRDtBQUtILGFBUEQ7QUFRUXlDLG1CQUFPSSxhQUFQLENBQXFCTCxNQUFNLENBQU4sQ0FBckI7QUFDWCxTQWxFSTtBQW9FTE0sbUJBcEVLLHlCQW9FUTtBQUFBOztBQUNULGlCQUFLakQsWUFBTCxHQUFvQixJQUFwQjtBQUNBVSx3QkFBWUMsT0FBWixDQUFvQixRQUFwQixFQUE4QjtBQUMxQnVDLHNCQUFNLFFBRG9CO0FBRTFCQyxzQkFBTTtBQUZvQixhQUE5QixFQUdHdEMsSUFISCxDQUdTLFVBQUN1QyxJQUFELEVBQVU7QUFDZkMsc0JBQU1DLElBQU4sQ0FBVyw0Q0FBWCxFQUF3RDtBQUNwREMsMkJBQU1IO0FBRDhDLGlCQUF4RCxFQUVHdkMsSUFGSCxDQUVRLGtCQUFVO0FBQ2QsMkJBQUtULElBQUwsQ0FBVW9ELEtBQVYsR0FBa0JqQyxPQUFPaEMsSUFBekI7O0FBRUFrRSwrQkFBVyxZQUFJO0FBQ1hqRCwwQkFBRSxjQUFGLEVBQWtCa0QsR0FBbEIsQ0FBc0Isa0JBQXRCLEVBQXlDLFVBQVVuQyxPQUFPaEMsSUFBakIsR0FBd0IsSUFBakU7QUFDSCxxQkFGRCxFQUVFLEdBRkY7O0FBSUEsMkJBQUtXLFNBQUwsR0FBaUIsSUFBakI7QUFDQSwyQkFBS0YsWUFBTCxHQUFvQixLQUFwQjtBQUVILGlCQVpELEVBWUcrQixLQVpILENBWVMsaUJBQVM7QUFDZCwyQkFBSy9CLFlBQUwsR0FBb0IsS0FBcEI7QUFDSCxpQkFkRDtBQWVILGFBbkJEO0FBb0JILFNBMUZJO0FBNEZMMkQscUJBNUZLLDJCQTRGVTtBQUFBOztBQUNYLGlCQUFLMUQsY0FBTCxHQUFzQixJQUF0QjtBQUNBaUIsOEJBQWtCUCxPQUFsQixDQUEwQixRQUExQixFQUFvQztBQUNoQ3VDLHNCQUFNLFFBRDBCO0FBRWhDQyxzQkFBTTtBQUYwQixhQUFwQyxFQUdHdEMsSUFISCxDQUdTLFVBQUN1QyxJQUFELEVBQVU7QUFDZkMsc0JBQU1DLElBQU4sQ0FBVywyQ0FBWCxFQUF1RDtBQUNuREMsMkJBQU1IO0FBRDZDLGlCQUF2RCxFQUVHdkMsSUFGSCxDQUVRLGtCQUFVO0FBQ2QsMkJBQUtULElBQUwsQ0FBVXdELE9BQVYsR0FBb0JyQyxPQUFPaEMsSUFBM0I7O0FBRUE7QUFDQSwyQkFBS1ksV0FBTCxHQUFtQixJQUFuQjtBQUNBLDJCQUFLRixjQUFMLEdBQXNCLEtBQXRCO0FBQ0FPLHNCQUFFLGVBQUYsRUFBbUJxRCxLQUFuQixDQUF5QixRQUF6QjtBQUNBQywyQkFBT0MsT0FBUCxDQUFlLE9BQUsxRCxJQUFMLENBQVVkLElBQVYsQ0FBZSxrQkFBZixDQUFmO0FBRUgsaUJBWEQsRUFXR3dDLEtBWEgsQ0FXUyxpQkFBUztBQUNkLDJCQUFLOUIsY0FBTCxHQUFzQixLQUF0QjtBQUNILGlCQWJEO0FBY0gsYUFsQkQ7QUFtQkg7QUFqSEksS0F0RGM7O0FBMEt2QitELGNBQVU7QUFDTkMsMkJBRE0saUNBQ2dCO0FBQ2xCLGdCQUFJLEtBQUs3RCxJQUFMLENBQVVMLFVBQVYsSUFBd0IsS0FBS0ssSUFBTCxDQUFVTCxVQUFWLENBQXFCd0MsTUFBakQsRUFBeUQ7QUFDckQsdUJBQU8sRUFBUDtBQUNIOztBQUVELG1CQUFPL0MsT0FBTzBFLFNBQVAsS0FBb0IsTUFBcEIsR0FBMkIsaUJBQWxDO0FBQ0gsU0FQSztBQVNOQyx1QkFUTSw2QkFTVztBQUNiLG1CQUFPM0UsT0FBTzBFLFNBQVAsS0FBb0IsU0FBcEIsR0FBOEIsTUFBckM7QUFDSDtBQVhLLEtBMUthOztBQXdMdkJFLGdCQUFZO0FBQ1JDLHFCQUFhN0UsT0FBTzhFLGNBQVAsQ0FBc0JDO0FBRDNCLEtBeExXOztBQTRMdkJDLFdBNUx1QixxQkE0TGI7QUFBQTs7QUFDTm5CLGNBQU1vQixHQUFOLENBQVUsQ0FBQyxLQUFLckQsYUFBTCxFQUFELENBQVY7O0FBRUFDLG1CQUFXQyxHQUFYLENBQWUsaUJBQWUsS0FBS2xCLElBQUwsQ0FBVXNFLEVBQXhDLEVBQ0M3RCxJQURELENBQ00sa0JBQVU7QUFDWkMsb0JBQVFDLEdBQVIsQ0FBWVEsT0FBT2hDLElBQVAsQ0FBWWdELE1BQVosR0FBbUIsR0FBbkIsR0FBdUIsT0FBS25DLElBQUwsQ0FBVXNFLEVBQTdDO0FBQ0EsZ0JBQUduRCxPQUFPaEMsSUFBUCxDQUFZZ0QsTUFBWixHQUFxQixDQUF4QixFQUEwQjtBQUN0Qi9CLGtCQUFFLHVDQUFGLEVBQTJDbUUsSUFBM0MsQ0FBZ0QsVUFBaEQsRUFBNEQsTUFBNUQ7QUFDSDtBQUNKLFNBTkQ7O0FBUUEsYUFBS3ZFLElBQUwsR0FBWSxJQUFJd0UsSUFBSixDQUFTcEUsRUFBRXFFLE1BQUYsQ0FBU3RGLElBQVQsRUFBZTtBQUNoQ2lFLG1CQUFPakUsS0FBS3VGLFdBRG9CO0FBRWhDbEIscUJBQVNyRSxLQUFLd0Y7QUFGa0IsU0FBZixDQUFULEVBR1IsRUFBRUMsU0FBUyxHQUFYLEVBSFEsQ0FBWjs7QUFLQUMsY0FBTUMsTUFBTixDQUFhLDRCQUFiLEVBQTJDLFlBQU07QUFDN0MsbUJBQUtsRixZQUFMLEdBQW9CLElBQXBCO0FBQ0gsU0FGRDs7QUFJQWlGLGNBQU1DLE1BQU4sQ0FBYSwyQkFBYixFQUEwQyxVQUFDQyxHQUFELEVBQVM7QUFDL0MsbUJBQUsvRSxJQUFMLENBQVVvRCxLQUFWLEdBQWtCMkIsR0FBbEI7QUFDQSxtQkFBS25GLFlBQUwsR0FBb0IsS0FBcEI7QUFDSCxTQUhEOztBQUtBaUYsY0FBTUMsTUFBTixDQUFhLDhCQUFiLEVBQTZDLFlBQU07QUFDL0MsbUJBQUtqRixjQUFMLEdBQXNCLElBQXRCO0FBQ0gsU0FGRDs7QUFJQWdGLGNBQU1DLE1BQU4sQ0FBYSw2QkFBYixFQUE0QyxVQUFDQyxHQUFELEVBQVM7QUFDakQsbUJBQUsvRSxJQUFMLENBQVV3RCxPQUFWLEdBQW9CdUIsR0FBcEI7QUFDQSxtQkFBS2xGLGNBQUwsR0FBc0IsS0FBdEI7QUFDSCxTQUhEOztBQUtBb0QsY0FBTS9CLEdBQU4sQ0FBVSw2QkFBVixFQUNLVCxJQURMLENBQ1Usa0JBQVU7QUFDaEIsbUJBQUtSLElBQUwsR0FBWWtCLE9BQU9oQyxJQUFuQjtBQUNILFNBSEQ7QUFJSDtBQWxPc0IsQ0FBUixDQUFuQjs7QUFzT0FtQixjQUFjRixFQUFFLGNBQUYsRUFBa0JHLE9BQWxCLENBQTBCO0FBQ3BDeUUsY0FBVTtBQUNOQyxlQUFPLElBREQ7QUFFTkMsZ0JBQVE7QUFGRjtBQUlWO0FBTG9DLENBQTFCLENBQWQ7O0FBUUFwRSxvQkFBb0JWLEVBQUUsaUJBQUYsRUFBcUJHLE9BQXJCLENBQTZCO0FBQzdDeUUsY0FBVTtBQUNOQyxlQUFPLEdBREQ7QUFFTkMsZ0JBQVEsR0FGRjtBQUdOcEMsY0FBSztBQUhDO0FBRG1DLENBQTdCLENBQXBCOztBQVFBIiwiZmlsZSI6ImpzL3BhZ2VzL3N0b3JlL2VkaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ1NCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCJsZXQgZGF0YSA9IHdpbmRvdy5MYXJhdmVsLmRhdGE7XHJcblxyXG53aW5kb3cuc3RvcmVFZGl0ID0gbmV3IFZ1ZSh7XHJcbiAgICBlbDogJyNzdG9yZS1mb3JtJyxcclxuXHJcbiAgICBkYXRhOiB7XHJcbiAgICAgICAgY2l0aWVzOiBbXSxcclxuICAgICAgICBwcm92aW5jZXM6IFtdLFxyXG4gICAgICAgIGNhdGVnb3JpZXM6IFtdLFxyXG4gICAgICAgIGNvdmVyTG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgcHJvZmlsZUxvYWRpbmc6IGZhbHNlLFxyXG4gICAgICAgIHRlbXBDb3ZlcjpudWxsLFxyXG4gICAgICAgIHRlbXBQcmltYXJ5Om51bGwsXHJcbiAgICAgICAgZm9ybToge30sXHJcbiAgICAgICAgbGFuZzpbXSxcclxuICAgIH0sXHJcblxyXG4gICAgIHdhdGNoOntcclxuICAgICAgICB0ZW1wQ292ZXI6ZnVuY3Rpb24odmFsKXtcclxuICAgICAgICAgICAgaWYoIXZhbCl7XHJcbiAgICAgICAgICAgICAgICAgJCgnI3VwbG9hZC1kZW1vJykucmVtb3ZlQ2xhc3MoJ3JlYWR5Jyk7XHJcbiAgICAgICAgICAgICAgICAkKCcjdXBsb2FkJykudmFsKCcnKTsgLy8gdGhpcyB3aWxsIGNsZWFyIHRoZSBpbnB1dCB2YWwuXHJcbiAgICAgICAgICAgICAgICAkdXBsb2FkQ3JvcC5jcm9wcGllKCdiaW5kJywge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybCA6ICcnXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncmVzZXQgY29tcGxldGUnKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICQoJyN1cGxvYWQtZGVtbyAuY3Itc2xpZGVyLXdyYXAnKS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAvLyAkKCcjdXBsb2FkLWRlbW8gLmNyLWJvdW5kYXJ5JykuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICQoJyN1cGxvYWQtZGVtbyAuY3Itc2xpZGVyLXdyYXAnKS5zaG93KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHRlbXBQcmltYXJ5OmZ1bmN0aW9uKHZhbCl7XHJcbiAgICAgICAgICAgIGlmKCF2YWwpe1xyXG4gICAgICAgICAgICAgICAgICQoJyN1cGxvYWQtcHJpbWFyeScpLnJlbW92ZUNsYXNzKCdyZWFkeScpO1xyXG4gICAgICAgICAgICAgICAgJCgnI21vZGFsUHJpbWFyeSBpbnB1dFt0eXBlPVwiZmlsZVwiXScpLnZhbCgnJyk7IC8vIHRoaXMgd2lsbCBjbGVhciB0aGUgaW5wdXQgdmFsLlxyXG4gICAgICAgICAgICAgICAgJHVwbG9hZFByaW1hcnlQaWMuY3JvcHBpZSgnYmluZCcsIHtcclxuICAgICAgICAgICAgICAgICAgICB1cmwgOiAnJ1xyXG4gICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Jlc2V0IGNvbXBsZXRlJyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAkKCcjbW9kYWxQcmltYXJ5IC5jci1zbGlkZXItd3JhcCcpLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIC8vICQoJyN1cGxvYWQtZGVtbyAuY3ItYm91bmRhcnknKS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgJCgnI21vZGFsUHJpbWFyeSAuY3Itc2xpZGVyLXdyYXAnKS5zaG93KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXRDYXRlZ29yaWVzKCkge1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnY2F0ZWdvcmllcycpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2F0ZWdvcmllcyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc3VibWl0KGUpIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtLnN1Ym1pdCgncHV0JywgZS5jdXJyZW50VGFyZ2V0LmFjdGlvbilcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2cocmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvc3RvcmVzLycgKyByZXN1bHQuc2x1ZztcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB0cmlnZ2VyVXBsb2FkUHJpbWFyeSgpe1xyXG4gICAgICAgICAgICAkKCcjbW9kYWxQcmltYXJ5IGlucHV0W3R5cGU9XCJmaWxlXCJdJykuY2xpY2soKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZWFkRmlsZShlKSB7XHJcblxyXG4gICAgICAgICAgdmFyIGZpbGVzID0gZS50YXJnZXQuZmlsZXMgfHwgZS5kYXRhVHJhbnNmZXIuZmlsZXM7XHJcblxyXG5cclxuICAgICAgICAgIGlmICghZmlsZXMubGVuZ3RoKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICB0aGlzLmNyZWF0ZUltYWdlKGZpbGVzKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZWFkRmlsZVByaW1hcnkoZSkge1xyXG5cclxuICAgICAgICAgIHZhciBmaWxlcyA9IGUudGFyZ2V0LmZpbGVzIHx8IGUuZGF0YVRyYW5zZmVyLmZpbGVzO1xyXG5cclxuXHJcbiAgICAgICAgICBpZiAoIWZpbGVzLmxlbmd0aClcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgdGhpcy5jcmVhdGVQcmltYXJ5SW1hZ2UoZmlsZXMpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNyZWF0ZUltYWdlKGlucHV0KSB7XHJcbiAgICAgICAgICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcmVhZGVyLm9ubG9hZCA9IChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkKCcjdXBsb2FkLWRlbW8nKS5hZGRDbGFzcygncmVhZHknKTtcclxuICAgICAgICAgICAgICAgICR1cGxvYWRDcm9wLmNyb3BwaWUoJ2JpbmQnLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiBlLnRhcmdldC5yZXN1bHRcclxuICAgICAgICAgICAgICAgIH0pLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGVtcENvdmVyID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoaW5wdXRbMF0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3JlYXRlUHJpbWFyeUltYWdlKGlucHV0KSB7XHJcbiAgICAgICAgICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcmVhZGVyLm9ubG9hZCA9IChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkKCcjdXBsb2FkLXByaW1hcnknKS5hZGRDbGFzcygncmVhZHknKTtcclxuICAgICAgICAgICAgICAgICR1cGxvYWRQcmltYXJ5UGljLmNyb3BwaWUoJ2JpbmQnLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiBlLnRhcmdldC5yZXN1bHRcclxuICAgICAgICAgICAgICAgIH0pLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGVtcFByaW1hcnkgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChpbnB1dFswXSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc3VibWl0Q292ZXIoKXtcclxuICAgICAgICAgICAgdGhpcy5jb3ZlckxvYWRpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAkdXBsb2FkQ3JvcC5jcm9wcGllKCdyZXN1bHQnLCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnY2FudmFzJyxcclxuICAgICAgICAgICAgICAgIHNpemU6ICd2aWV3cG9ydCdcclxuICAgICAgICAgICAgfSkudGhlbiggKHJlc3ApID0+IHtcclxuICAgICAgICAgICAgICAgIGF4aW9zLnBvc3QoJy9jb21tb24vcmVzaXplLWltYWdlP3dpZHRoPTExNDAmaGVpZ2h0PTMwMCcse1xyXG4gICAgICAgICAgICAgICAgICAgIGltYWdlOnJlc3BcclxuICAgICAgICAgICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0uY292ZXIgPSByZXN1bHQuZGF0YTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKT0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcjdXBsb2FkLWRlbW8nKS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLFwidXJsKCdcIiArIHJlc3VsdC5kYXRhICsgXCInKVwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9LDUwMClcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRlbXBDb3ZlciA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3ZlckxvYWRpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3ZlckxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzdWJtaXRQcmltYXJ5KCl7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZmlsZUxvYWRpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAkdXBsb2FkUHJpbWFyeVBpYy5jcm9wcGllKCdyZXN1bHQnLCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnY2FudmFzJyxcclxuICAgICAgICAgICAgICAgIHNpemU6ICd2aWV3cG9ydCdcclxuICAgICAgICAgICAgfSkudGhlbiggKHJlc3ApID0+IHtcclxuICAgICAgICAgICAgICAgIGF4aW9zLnBvc3QoJy9jb21tb24vcmVzaXplLWltYWdlP3dpZHRoPTMwMCZoZWlnaHQ9MzAwJyx7XHJcbiAgICAgICAgICAgICAgICAgICAgaW1hZ2U6cmVzcFxyXG4gICAgICAgICAgICAgICAgfSkudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybS5wcm9maWxlID0gcmVzdWx0LmRhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vICQoJyN1cGxvYWQtZGVtbycpLmNzcygnYmFja2dyb3VuZC1pbWFnZScsXCJ1cmwoJ1wiICsgcmVzdWx0LmRhdGEgKyBcIicpXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGVtcFByaW1hcnkgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZUxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAkKCcjbW9kYWxQcmltYXJ5JykubW9kYWwoJ3RvZ2dsZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZy5kYXRhWydwcm9mLXBpYy1zZXQtbXNnJ10pXHJcblxyXG4gICAgICAgICAgICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZUxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGNhdGVnb3J5UGxhY2Vob2xkZXIoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZvcm0uY2F0ZWdvcmllcyAmJiB0aGlzLmZvcm0uY2F0ZWdvcmllcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHdpbmRvdy5pc0NoaW5lc2UoKT8gJ+mAieaLqeexu+WIqyc6J1NlbGVjdCBDYXRlZ29yeSc7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0Q2F0ZWdvcnlOYW1lKCl7XHJcbiAgICAgICAgICAgIHJldHVybiB3aW5kb3cuaXNDaGluZXNlKCk/ICduYW1lX2NuJzonbmFtZSc7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICAgTXVsdGlzZWxlY3Q6IHdpbmRvdy5WdWVNdWx0aXNlbGVjdC5kZWZhdWx0XHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgICAgYXhpb3MuYWxsKFt0aGlzLmdldENhdGVnb3JpZXMoKV0pO1xyXG5cclxuICAgICAgICBheGlvc0FQSXYxLmdldCgnL2NvdW50b3JkZXIvJyt0aGlzLmZvcm0uaWQpXHJcbiAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0LmRhdGEubGVuZ3RoKyctJyt0aGlzLmZvcm0uaWQpO1xyXG4gICAgICAgICAgICBpZihyZXN1bHQuZGF0YS5sZW5ndGggPiAwKXtcclxuICAgICAgICAgICAgICAgICQoJyNhZGRyZXNzLCAjY2l0eSwgI3Byb3ZpbmNlLCAjbGFuZG1hcmsnKS5hdHRyKFwiZGlzYWJsZWRcIiwgXCJ0cnVlXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybSA9IG5ldyBGb3JtKCQuZXh0ZW5kKGRhdGEsIHtcclxuICAgICAgICAgICAgY292ZXI6IGRhdGEuY292ZXJfaW1hZ2UsXHJcbiAgICAgICAgICAgIHByb2ZpbGU6IGRhdGEucHJvZmlsZV9pbWFnZSxcclxuICAgICAgICB9KSwgeyBiYXNlVVJMOiAnLycgfSk7XHJcblxyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignSW1nZmlsZXVwbG9hZC5jb3ZlckxvYWRpbmcnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuY292ZXJMb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgRXZlbnQubGlzdGVuKCdJbWdmaWxldXBsb2FkLmNvdmVyT25Mb2FkJywgKGltZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm0uY292ZXIgPSBpbWc7XHJcbiAgICAgICAgICAgIHRoaXMuY292ZXJMb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignSW1nZmlsZXVwbG9hZC5wcm9maWxlTG9hZGluZycsICgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5wcm9maWxlTG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIEV2ZW50Lmxpc3RlbignSW1nZmlsZXVwbG9hZC5wcm9maWxlT25Mb2FkJywgKGltZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm0ucHJvZmlsZSA9IGltZztcclxuICAgICAgICAgICAgdGhpcy5wcm9maWxlTG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBheGlvcy5nZXQoJy90cmFuc2xhdGUvc3RvcmUtbWFuYWdlbWVudCcpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubGFuZyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59KTtcclxuXHJcblxyXG4kdXBsb2FkQ3JvcCA9ICQoJyN1cGxvYWQtZGVtbycpLmNyb3BwaWUoe1xyXG4gICAgdmlld3BvcnQ6IHtcclxuICAgICAgICB3aWR0aDogMTE0MCxcclxuICAgICAgICBoZWlnaHQ6IDMwMCxcclxuICAgIH0sXHJcbiAgICAvLyBlbmFibGVFeGlmOiB0cnVlXHJcbn0pO1xyXG5cclxuJHVwbG9hZFByaW1hcnlQaWMgPSAkKCcjdXBsb2FkLXByaW1hcnknKS5jcm9wcGllKHtcclxuICAgIHZpZXdwb3J0OiB7XHJcbiAgICAgICAgd2lkdGg6IDMwMCxcclxuICAgICAgICBoZWlnaHQ6IDMwMCxcclxuICAgICAgICB0eXBlOidjaXJjbGUnXHJcbiAgICB9LFxyXG59KTtcclxuXHJcbi8qJCgnI251bWJlcicpLmlucHV0bWFzayh7XHJcbiAgICBtYXNrOiAnKDk5OTkpIDk5OS05OTk5J1xyXG59KSovXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9zdG9yZS9lZGl0LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==