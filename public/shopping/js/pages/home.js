/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 431);
/******/ })
/************************************************************************/
/******/ ({

/***/ 181:
/***/ (function(module, exports) {

// Function to check element is exist
$.fn.exist = function () {
  return $(this).length > 0;
};

// Function to get window width
function get_width() {
  return $(window).width();
}

$(function () {

  // open navigation dropdown on hover (only when width >= 768px)
  $('ul.nav li.dropdown').hover(function () {
    if (get_width() >= 767) {
      $(this).addClass('open');
    }
  }, function () {
    if (get_width() >= 767) {
      $(this).removeClass('open');
    }
  });

  // Navigation submenu
  $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
    event.preventDefault();
    event.stopPropagation();
    $(this).parent().siblings().removeClass('open');
    $(this).parent().toggleClass('open');
  });

  // owlCarousel for Home Slider
  if ($('.home-slider').exist()) {
    $('.home-slider').owlCarousel({
      items: 1,
      autoplay: true,
      autoplayHoverPause: true,
      dots: false,
      nav: true,
      stagePadding: -5,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
    });
  }

  // owlCarousel for Widget Slider
  if ($('.widget-slider').exist()) {
    var widget_slider = $('.widget-slider');
    widget_slider.owlCarousel({
      items: 1,
      dots: false,
      nav: true,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      responsive: {
        0: {
          items: 1
        },
        480: {
          items: 2
        },
        768: {
          items: 3
        },
        992: {
          items: 1
        }
      }
    });
  }

  // owlCarousel for Product Slider
  if ($('.product-slider').exist()) {}
  // var product_slider = $('.product-slider')
  // product_slider.owlCarousel({
  //   items:1,
  //   dots: false,
  //   nav: true,
  //   navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
  //   responsive:{
  //       0:{
  //         items:1,
  //       },
  //       480:{
  //         items:2,
  //       },
  //       768:{
  //         items:3,
  //       },
  //       992:{
  //         items:3,
  //       },
  //       1200:{
  //         items:4,
  //       }
  //     }
  // });


  // owlCarousel for Related Product Slider
  if ($('.related-product-slider').exist()) {
    var related_product_slider = $('.related-product-slider');
    related_product_slider.owlCarousel({
      dots: false,
      nav: true,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      responsive: {
        0: {
          items: 1
        },
        480: {
          items: 2
        },
        768: {
          items: 3
        },
        992: {
          items: 5
        },
        1200: {
          items: 6
        }
      }
    });
  }

  // owlCarousel for Brand Slider
  if ($('.brand-slider').exist()) {
    var brand_slider = $('.brand-slider');
    brand_slider.owlCarousel({
      dots: false,
      nav: true,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      responsive: {
        0: {
          items: 1
        },
        480: {
          items: 2,
          margin: 15
        },
        768: {
          items: 3,
          margin: 15
        },
        992: {
          items: 4,
          margin: 30
        },
        1200: {
          items: 6,
          margin: 30
        }
      }
    });
  }

  // Back top Top
  $(window).scroll(function () {
    if ($(this).scrollTop() > 70) {
      $('.back-top').fadeIn();
    } else {
      $('.back-top').fadeOut();
    }
  });

  var toggleAds = '<div class="toggle-ads"><i class="fa fa-eye-slash" aria-hidden="true"></i></div>';
  $('div.home-slider-row').prepend(toggleAds);

  $('.toggle-ads').on('click', function () {
    $('div.home-slider').toggleClass('hide');

    $('.toggle-ads i').toggleClass('fa-eye-slash').toggleClass('fa-eye');

    if (!$('div.home-slider').is(':visible')) {
      $('.toggle-ads').css('margin-top', '2px');
    } else {
      $('.toggle-ads').css('margin-top', '35px');
    }
  });
});

/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(181);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL2hvbWUuanMiXSwibmFtZXMiOlsiJCIsImZuIiwiZXhpc3QiLCJsZW5ndGgiLCJnZXRfd2lkdGgiLCJ3aW5kb3ciLCJ3aWR0aCIsImhvdmVyIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsIm9uIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsInN0b3BQcm9wYWdhdGlvbiIsInBhcmVudCIsInNpYmxpbmdzIiwidG9nZ2xlQ2xhc3MiLCJvd2xDYXJvdXNlbCIsIml0ZW1zIiwiYXV0b3BsYXkiLCJhdXRvcGxheUhvdmVyUGF1c2UiLCJkb3RzIiwibmF2Iiwic3RhZ2VQYWRkaW5nIiwibmF2VGV4dCIsIndpZGdldF9zbGlkZXIiLCJyZXNwb25zaXZlIiwicmVsYXRlZF9wcm9kdWN0X3NsaWRlciIsImJyYW5kX3NsaWRlciIsIm1hcmdpbiIsInNjcm9sbCIsInNjcm9sbFRvcCIsImZhZGVJbiIsImZhZGVPdXQiLCJ0b2dnbGVBZHMiLCJwcmVwZW5kIiwiaXMiLCJjc3MiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQUEsRUFBRUMsRUFBRixDQUFLQyxLQUFMLEdBQWEsWUFBVTtBQUFFLFNBQU9GLEVBQUUsSUFBRixFQUFRRyxNQUFSLEdBQWlCLENBQXhCO0FBQTRCLENBQXJEOztBQUVBO0FBQ0EsU0FBU0MsU0FBVCxHQUFxQjtBQUNuQixTQUFPSixFQUFFSyxNQUFGLEVBQVVDLEtBQVYsRUFBUDtBQUNEOztBQUVETixFQUFFLFlBQVU7O0FBRVY7QUFDQUEsSUFBRSxvQkFBRixFQUF3Qk8sS0FBeEIsQ0FBOEIsWUFBVztBQUN2QyxRQUFJSCxlQUFlLEdBQW5CLEVBQXdCO0FBQ3RCSixRQUFFLElBQUYsRUFBUVEsUUFBUixDQUFpQixNQUFqQjtBQUNEO0FBQ0YsR0FKRCxFQUlHLFlBQVc7QUFDWixRQUFJSixlQUFlLEdBQW5CLEVBQXdCO0FBQ3RCSixRQUFFLElBQUYsRUFBUVMsV0FBUixDQUFvQixNQUFwQjtBQUNEO0FBQ0YsR0FSRDs7QUFVQTtBQUNBVCxJQUFFLHlDQUFGLEVBQTZDVSxFQUE3QyxDQUFnRCxPQUFoRCxFQUF5RCxVQUFTQyxLQUFULEVBQWdCO0FBQ3ZFQSxVQUFNQyxjQUFOO0FBQ0FELFVBQU1FLGVBQU47QUFDQWIsTUFBRSxJQUFGLEVBQVFjLE1BQVIsR0FBaUJDLFFBQWpCLEdBQTRCTixXQUE1QixDQUF3QyxNQUF4QztBQUNBVCxNQUFFLElBQUYsRUFBUWMsTUFBUixHQUFpQkUsV0FBakIsQ0FBNkIsTUFBN0I7QUFDRCxHQUxEOztBQU9BO0FBQ0EsTUFBSWhCLEVBQUUsY0FBRixFQUFrQkUsS0FBbEIsRUFBSixFQUErQjtBQUM3QkYsTUFBRSxjQUFGLEVBQWtCaUIsV0FBbEIsQ0FBOEI7QUFDNUJDLGFBQU0sQ0FEc0I7QUFFNUJDLGdCQUFTLElBRm1CO0FBRzVCQywwQkFBbUIsSUFIUztBQUk1QkMsWUFBSyxLQUp1QjtBQUs1QkMsV0FBSSxJQUx3QjtBQU01QkMsb0JBQWEsQ0FBQyxDQU5jO0FBTzVCQyxlQUFRLENBQUMsa0NBQUQsRUFBb0MsbUNBQXBDO0FBUG9CLEtBQTlCO0FBU0Q7O0FBRUQ7QUFDQSxNQUFJeEIsRUFBRSxnQkFBRixFQUFvQkUsS0FBcEIsRUFBSixFQUFpQztBQUMvQixRQUFJdUIsZ0JBQWdCekIsRUFBRSxnQkFBRixDQUFwQjtBQUNBeUIsa0JBQWNSLFdBQWQsQ0FBMEI7QUFDeEJDLGFBQU0sQ0FEa0I7QUFFeEJHLFlBQU0sS0FGa0I7QUFHeEJDLFdBQUssSUFIbUI7QUFJeEJFLGVBQVEsQ0FBQyxrQ0FBRCxFQUFvQyxtQ0FBcEMsQ0FKZ0I7QUFLeEJFLGtCQUFXO0FBQ1QsV0FBRTtBQUNBUixpQkFBTTtBQUROLFNBRE87QUFJVCxhQUFJO0FBQ0ZBLGlCQUFNO0FBREosU0FKSztBQU9ULGFBQUk7QUFDRkEsaUJBQU07QUFESixTQVBLO0FBVVQsYUFBSTtBQUNGQSxpQkFBTTtBQURKO0FBVks7QUFMYSxLQUExQjtBQW9CRDs7QUFFRDtBQUNBLE1BQUlsQixFQUFFLGlCQUFGLEVBQXFCRSxLQUFyQixFQUFKLEVBQWtDLENBeUJqQztBQXhCQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdEO0FBQ0QsTUFBSUYsRUFBRSx5QkFBRixFQUE2QkUsS0FBN0IsRUFBSixFQUEwQztBQUN4QyxRQUFJeUIseUJBQXlCM0IsRUFBRSx5QkFBRixDQUE3QjtBQUNBMkIsMkJBQXVCVixXQUF2QixDQUFtQztBQUNqQ0ksWUFBTSxLQUQyQjtBQUVqQ0MsV0FBSyxJQUY0QjtBQUdqQ0UsZUFBUSxDQUFDLGtDQUFELEVBQW9DLG1DQUFwQyxDQUh5QjtBQUlqQ0Usa0JBQVc7QUFDUCxXQUFFO0FBQ0FSLGlCQUFNO0FBRE4sU0FESztBQUlQLGFBQUk7QUFDRkEsaUJBQU07QUFESixTQUpHO0FBT1AsYUFBSTtBQUNGQSxpQkFBTTtBQURKLFNBUEc7QUFVUCxhQUFJO0FBQ0ZBLGlCQUFNO0FBREosU0FWRztBQWFQLGNBQUs7QUFDSEEsaUJBQU07QUFESDtBQWJFO0FBSnNCLEtBQW5DO0FBc0JEOztBQUVEO0FBQ0EsTUFBSWxCLEVBQUUsZUFBRixFQUFtQkUsS0FBbkIsRUFBSixFQUFnQztBQUM5QixRQUFJMEIsZUFBZTVCLEVBQUUsZUFBRixDQUFuQjtBQUNBNEIsaUJBQWFYLFdBQWIsQ0FBeUI7QUFDdkJJLFlBQUssS0FEa0I7QUFFdkJDLFdBQUksSUFGbUI7QUFHdkJFLGVBQVEsQ0FBQyxrQ0FBRCxFQUFvQyxtQ0FBcEMsQ0FIZTtBQUl2QkUsa0JBQVc7QUFDVCxXQUFFO0FBQ0FSLGlCQUFNO0FBRE4sU0FETztBQUlULGFBQUk7QUFDRkEsaUJBQU0sQ0FESjtBQUVGVyxrQkFBTztBQUZMLFNBSks7QUFRVCxhQUFJO0FBQ0ZYLGlCQUFNLENBREo7QUFFRlcsa0JBQU87QUFGTCxTQVJLO0FBWVQsYUFBSTtBQUNGWCxpQkFBTSxDQURKO0FBRUZXLGtCQUFRO0FBRk4sU0FaSztBQWdCVCxjQUFLO0FBQ0hYLGlCQUFNLENBREg7QUFFSFcsa0JBQVE7QUFGTDtBQWhCSTtBQUpZLEtBQXpCO0FBMEJEOztBQUlEO0FBQ0U3QixJQUFFSyxNQUFGLEVBQVV5QixNQUFWLENBQWlCLFlBQVU7QUFDM0IsUUFBSTlCLEVBQUUsSUFBRixFQUFRK0IsU0FBUixLQUFvQixFQUF4QixFQUE0QjtBQUMxQi9CLFFBQUUsV0FBRixFQUFlZ0MsTUFBZjtBQUNELEtBRkQsTUFFTztBQUNMaEMsUUFBRSxXQUFGLEVBQWVpQyxPQUFmO0FBQ0Q7QUFDRixHQU5DOztBQVFBLE1BQUlDLFlBQVksa0ZBQWhCO0FBQ0FsQyxJQUFFLHFCQUFGLEVBQXlCbUMsT0FBekIsQ0FBaUNELFNBQWpDOztBQUVBbEMsSUFBRSxhQUFGLEVBQWlCVSxFQUFqQixDQUFvQixPQUFwQixFQUE2QixZQUFXO0FBQ3RDVixNQUFFLGlCQUFGLEVBQXFCZ0IsV0FBckIsQ0FBaUMsTUFBakM7O0FBRUFoQixNQUFFLGVBQUYsRUFBbUJnQixXQUFuQixDQUErQixjQUEvQixFQUErQ0EsV0FBL0MsQ0FBMkQsUUFBM0Q7O0FBRUEsUUFBRyxDQUFDaEIsRUFBRSxpQkFBRixFQUFxQm9DLEVBQXJCLENBQXdCLFVBQXhCLENBQUosRUFBeUM7QUFDdkNwQyxRQUFFLGFBQUYsRUFBaUJxQyxHQUFqQixDQUFxQixZQUFyQixFQUFtQyxLQUFuQztBQUNELEtBRkQsTUFFTztBQUNMckMsUUFBRSxhQUFGLEVBQWlCcUMsR0FBakIsQ0FBcUIsWUFBckIsRUFBbUMsTUFBbkM7QUFDRDtBQUNGLEdBVkQ7QUFZSCxDQTNLRCxFIiwiZmlsZSI6ImpzL3BhZ2VzL2hvbWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQzMSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyBGdW5jdGlvbiB0byBjaGVjayBlbGVtZW50IGlzIGV4aXN0XHJcbiQuZm4uZXhpc3QgPSBmdW5jdGlvbigpeyByZXR1cm4gJCh0aGlzKS5sZW5ndGggPiAwOyB9XHJcblxyXG4vLyBGdW5jdGlvbiB0byBnZXQgd2luZG93IHdpZHRoXHJcbmZ1bmN0aW9uIGdldF93aWR0aCgpIHtcclxuICByZXR1cm4gJCh3aW5kb3cpLndpZHRoKCk7XHJcbn1cclxuXHJcbiQoZnVuY3Rpb24oKXtcclxuXHJcbiAgLy8gb3BlbiBuYXZpZ2F0aW9uIGRyb3Bkb3duIG9uIGhvdmVyIChvbmx5IHdoZW4gd2lkdGggPj0gNzY4cHgpXHJcbiAgJCgndWwubmF2IGxpLmRyb3Bkb3duJykuaG92ZXIoZnVuY3Rpb24oKSB7XHJcbiAgICBpZiAoZ2V0X3dpZHRoKCkgPj0gNzY3KSB7XHJcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoJ29wZW4nKTtcclxuICAgIH1cclxuICB9LCBmdW5jdGlvbigpIHtcclxuICAgIGlmIChnZXRfd2lkdGgoKSA+PSA3NjcpIHtcclxuICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcygnb3BlbicpO1xyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICAvLyBOYXZpZ2F0aW9uIHN1Ym1lbnVcclxuICAkKCd1bC5kcm9wZG93bi1tZW51IFtkYXRhLXRvZ2dsZT1kcm9wZG93bl0nKS5vbignY2xpY2snLCBmdW5jdGlvbihldmVudCkge1xyXG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgJCh0aGlzKS5wYXJlbnQoKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdvcGVuJyk7XHJcbiAgICAkKHRoaXMpLnBhcmVudCgpLnRvZ2dsZUNsYXNzKCdvcGVuJyk7XHJcbiAgfSk7XHJcblxyXG4gIC8vIG93bENhcm91c2VsIGZvciBIb21lIFNsaWRlclxyXG4gIGlmICgkKCcuaG9tZS1zbGlkZXInKS5leGlzdCgpKSB7XHJcbiAgICAkKCcuaG9tZS1zbGlkZXInKS5vd2xDYXJvdXNlbCh7XHJcbiAgICAgIGl0ZW1zOjEsXHJcbiAgICAgIGF1dG9wbGF5OnRydWUsXHJcbiAgICAgIGF1dG9wbGF5SG92ZXJQYXVzZTp0cnVlLFxyXG4gICAgICBkb3RzOmZhbHNlLFxyXG4gICAgICBuYXY6dHJ1ZSxcclxuICAgICAgc3RhZ2VQYWRkaW5nOi01LFxyXG4gICAgICBuYXZUZXh0OlsnPGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1sZWZ0XCI+PC9pPicsJzxpIGNsYXNzPVwiZmEgZmEtYW5nbGUtcmlnaHRcIj48L2k+J10sXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vIG93bENhcm91c2VsIGZvciBXaWRnZXQgU2xpZGVyXHJcbiAgaWYgKCQoJy53aWRnZXQtc2xpZGVyJykuZXhpc3QoKSkge1xyXG4gICAgdmFyIHdpZGdldF9zbGlkZXIgPSAkKCcud2lkZ2V0LXNsaWRlcicpO1xyXG4gICAgd2lkZ2V0X3NsaWRlci5vd2xDYXJvdXNlbCh7XHJcbiAgICAgIGl0ZW1zOjEsXHJcbiAgICAgIGRvdHM6IGZhbHNlLFxyXG4gICAgICBuYXY6IHRydWUsXHJcbiAgICAgIG5hdlRleHQ6Wyc8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLWxlZnRcIj48L2k+JywnPGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1yaWdodFwiPjwvaT4nXSxcclxuICAgICAgcmVzcG9uc2l2ZTp7XHJcbiAgICAgICAgMDp7XHJcbiAgICAgICAgICBpdGVtczoxLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNDgwOntcclxuICAgICAgICAgIGl0ZW1zOjIsXHJcbiAgICAgICAgfSxcclxuICAgICAgICA3Njg6e1xyXG4gICAgICAgICAgaXRlbXM6MyxcclxuICAgICAgICB9LFxyXG4gICAgICAgIDk5Mjp7XHJcbiAgICAgICAgICBpdGVtczoxLFxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvLyBvd2xDYXJvdXNlbCBmb3IgUHJvZHVjdCBTbGlkZXJcclxuICBpZiAoJCgnLnByb2R1Y3Qtc2xpZGVyJykuZXhpc3QoKSkge1xyXG4gICAgLy8gdmFyIHByb2R1Y3Rfc2xpZGVyID0gJCgnLnByb2R1Y3Qtc2xpZGVyJylcclxuICAgIC8vIHByb2R1Y3Rfc2xpZGVyLm93bENhcm91c2VsKHtcclxuICAgIC8vICAgaXRlbXM6MSxcclxuICAgIC8vICAgZG90czogZmFsc2UsXHJcbiAgICAvLyAgIG5hdjogdHJ1ZSxcclxuICAgIC8vICAgbmF2VGV4dDpbJzxpIGNsYXNzPVwiZmEgZmEtYW5nbGUtbGVmdFwiPjwvaT4nLCc8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPiddLFxyXG4gICAgLy8gICByZXNwb25zaXZlOntcclxuICAgIC8vICAgICAgIDA6e1xyXG4gICAgLy8gICAgICAgICBpdGVtczoxLFxyXG4gICAgLy8gICAgICAgfSxcclxuICAgIC8vICAgICAgIDQ4MDp7XHJcbiAgICAvLyAgICAgICAgIGl0ZW1zOjIsXHJcbiAgICAvLyAgICAgICB9LFxyXG4gICAgLy8gICAgICAgNzY4OntcclxuICAgIC8vICAgICAgICAgaXRlbXM6MyxcclxuICAgIC8vICAgICAgIH0sXHJcbiAgICAvLyAgICAgICA5OTI6e1xyXG4gICAgLy8gICAgICAgICBpdGVtczozLFxyXG4gICAgLy8gICAgICAgfSxcclxuICAgIC8vICAgICAgIDEyMDA6e1xyXG4gICAgLy8gICAgICAgICBpdGVtczo0LFxyXG4gICAgLy8gICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH0pO1xyXG4gIH1cclxuXHJcbiAgIC8vIG93bENhcm91c2VsIGZvciBSZWxhdGVkIFByb2R1Y3QgU2xpZGVyXHJcbiAgaWYgKCQoJy5yZWxhdGVkLXByb2R1Y3Qtc2xpZGVyJykuZXhpc3QoKSkge1xyXG4gICAgdmFyIHJlbGF0ZWRfcHJvZHVjdF9zbGlkZXIgPSAkKCcucmVsYXRlZC1wcm9kdWN0LXNsaWRlcicpXHJcbiAgICByZWxhdGVkX3Byb2R1Y3Rfc2xpZGVyLm93bENhcm91c2VsKHtcclxuICAgICAgZG90czogZmFsc2UsXHJcbiAgICAgIG5hdjogdHJ1ZSxcclxuICAgICAgbmF2VGV4dDpbJzxpIGNsYXNzPVwiZmEgZmEtYW5nbGUtbGVmdFwiPjwvaT4nLCc8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPiddLFxyXG4gICAgICByZXNwb25zaXZlOntcclxuICAgICAgICAgIDA6e1xyXG4gICAgICAgICAgICBpdGVtczoxLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIDQ4MDp7XHJcbiAgICAgICAgICAgIGl0ZW1zOjIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgNzY4OntcclxuICAgICAgICAgICAgaXRlbXM6MyxcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICA5OTI6e1xyXG4gICAgICAgICAgICBpdGVtczo1LFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIDEyMDA6e1xyXG4gICAgICAgICAgICBpdGVtczo2LFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLy8gb3dsQ2Fyb3VzZWwgZm9yIEJyYW5kIFNsaWRlclxyXG4gIGlmICgkKCcuYnJhbmQtc2xpZGVyJykuZXhpc3QoKSkge1xyXG4gICAgdmFyIGJyYW5kX3NsaWRlciA9ICQoJy5icmFuZC1zbGlkZXInKTtcclxuICAgIGJyYW5kX3NsaWRlci5vd2xDYXJvdXNlbCh7XHJcbiAgICAgIGRvdHM6ZmFsc2UsXHJcbiAgICAgIG5hdjp0cnVlLFxyXG4gICAgICBuYXZUZXh0OlsnPGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1sZWZ0XCI+PC9pPicsJzxpIGNsYXNzPVwiZmEgZmEtYW5nbGUtcmlnaHRcIj48L2k+J10sXHJcbiAgICAgIHJlc3BvbnNpdmU6e1xyXG4gICAgICAgIDA6e1xyXG4gICAgICAgICAgaXRlbXM6MSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIDQ4MDp7XHJcbiAgICAgICAgICBpdGVtczoyLFxyXG4gICAgICAgICAgbWFyZ2luOjE1XHJcbiAgICAgICAgfSxcclxuICAgICAgICA3Njg6e1xyXG4gICAgICAgICAgaXRlbXM6MyxcclxuICAgICAgICAgIG1hcmdpbjoxNVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgOTkyOntcclxuICAgICAgICAgIGl0ZW1zOjQsXHJcbiAgICAgICAgICBtYXJnaW46IDMwXHJcbiAgICAgICAgfSxcclxuICAgICAgICAxMjAwOntcclxuICAgICAgICAgIGl0ZW1zOjYsXHJcbiAgICAgICAgICBtYXJnaW46IDMwXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgXHJcblxyXG5cclxuICAvLyBCYWNrIHRvcCBUb3BcclxuICAgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKXtcclxuICAgIGlmICgkKHRoaXMpLnNjcm9sbFRvcCgpPjcwKSB7XHJcbiAgICAgICQoJy5iYWNrLXRvcCcpLmZhZGVJbigpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgJCgnLmJhY2stdG9wJykuZmFkZU91dCgpO1xyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICAgIHZhciB0b2dnbGVBZHMgPSAnPGRpdiBjbGFzcz1cInRvZ2dsZS1hZHNcIj48aSBjbGFzcz1cImZhIGZhLWV5ZS1zbGFzaFwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L2Rpdj4nO1xyXG4gICAgJCgnZGl2LmhvbWUtc2xpZGVyLXJvdycpLnByZXBlbmQodG9nZ2xlQWRzKTtcclxuXHJcbiAgICAkKCcudG9nZ2xlLWFkcycpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAkKCdkaXYuaG9tZS1zbGlkZXInKS50b2dnbGVDbGFzcygnaGlkZScpO1xyXG5cclxuICAgICAgJCgnLnRvZ2dsZS1hZHMgaScpLnRvZ2dsZUNsYXNzKCdmYS1leWUtc2xhc2gnKS50b2dnbGVDbGFzcygnZmEtZXllJyk7XHJcblxyXG4gICAgICBpZighJCgnZGl2LmhvbWUtc2xpZGVyJykuaXMoJzp2aXNpYmxlJykpIHtcclxuICAgICAgICAkKCcudG9nZ2xlLWFkcycpLmNzcygnbWFyZ2luLXRvcCcsICcycHgnKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAkKCcudG9nZ2xlLWFkcycpLmNzcygnbWFyZ2luLXRvcCcsICczNXB4Jyk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxufSk7XHJcblxyXG5cclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvaG9tZS5qcyJdLCJzb3VyY2VSb290IjoiIn0=