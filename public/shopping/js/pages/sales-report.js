/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 450);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 200:
/***/ (function(module, exports, __webpack_require__) {

$(document).ready(function () {
    ctx = $('#table_chart');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            datasets: [{
                label: 'All Stores',
                data: [12, 19, 3, 5, 2, 3, 24, 23, 12, 6, 34, 10],
                backgroundColor: ['rgba(255, 99, 132, 0.2)'],
                borderColor: ['rgba(255,99,132,1)'],
                borderWidth: 3
            }, {
                label: 'Store 1',
                data: [1, 9, 13, 25, 32, 21, 4, 2, 2, 16, 4, 10],
                backgroundColor: ['rgba(75, 192, 192, 0.2)'],
                borderColor: ['rgba(75, 192, 192, 1)'],
                borderWidth: 1
            }, {
                label: 'Store 2',
                data: [0, 3, 1, 5, 2, 33, 41, 10, 2, 16, 10, 12],
                backgroundColor: ['rgba(75, 0, 192, 0.2)'],
                borderColor: ['rgba(75, 0, 192, 1)'],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            responsive: true,
            maintainAspectRatio: true
        }
    });
});

Vue.component('store-panel', __webpack_require__(353));

Vue.filter('currency', function (value) {
    return numberWithCommas(parseFloat(value).toFixed(2));
});

var vm = new Vue({
    el: '#sales_report_container',
    data: {
        listStore: [],
        details: [],
        serviceType: ['Drop Off', 'Pick Up'],
        updateRemarks: new Form({
            id: "",
            seller_notes: ""
        }, { baseURL: '/user/sales-report' }),
        optionPickUp: false,
        optionDrop: false,
        selectedOrder: null,
        usable: null,
        fee: null,
        transitForm: new Form({
            shipment_type: null,
            courier: null,
            tracking_number: null
        }),
        cancelForm: new Form({
            reason: null
        }),
        hasDiscount: false,
        lang: []
    },

    components: {
        Multiselect: window.VueMultiselect.default
    },
    methods: {
        showAirwayBill: function showAirwayBill(id) {
            var _this = this;

            this.details = '';
            axios.get('/user/sales-report/details/' + id).then(function (result) {
                _this.details = result.data;
                if (result.data.discount != 0) {
                    _this.hasDiscount = true;
                } else {
                    _this.hasDiscount = false;
                }
            });
            $('#viewAirwayBill').modal('toggle');
        },
        getUsableEwalletBalance: function getUsableEwalletBalance() {
            var _this2 = this;

            if (Laravel.user) {
                axiosAPIv1.get('/getUsable').then(function (result) {
                    _this2.usable = parseFloat(result.data.data.usable);
                });
            }
        },
        showDetails: function showDetails(id) {
            var _this3 = this;

            this.details = "";
            axios.get('/user/sales-report/details/' + id).then(function (result) {
                _this3.details = result.data;
                _this3.updateRemarks.seller_notes = result.data.seller_notes;
                if (result.data.discount != 0) {
                    _this3.hasDiscount = true;
                } else {
                    _this3.hasDiscount = false;
                }
                $('#orderProducts').modal('show');
            });
        },

        submitNote: function submitNote(id) {
            var _this4 = this;

            this.updateRemarks.id = id;
            this.updateRemarks.submit('post', '/update-remarks').then(function (result) {
                toastr.success(_this4.lang.data['updated-note-msg']);
            }).catch(function (error) {
                toastr.error(_this4.lang.data['update-failed-msg']);
            });
        },
        getStores: function getStores() {
            var _this5 = this;

            axios.get('/user/sales-report/get-store').then(function (result) {
                _this5.listStore = result.data.stores;
            });
        },
        changeOption: function changeOption(value) {
            if (value == "Pick Up") {
                this.optionPickUp = true;
                this.optionDrop = false;
            } else if (value == "Drop Off") {
                this.optionDrop = true;
                this.optionPickUp = false;
            }
        },
        submitTrackingNumber: function submitTrackingNumber() {
            var _this6 = this;

            var errors = {};

            if (!this.transitForm.shipment_type || !this.transitForm.courier || !this.transitForm.tracking_number) {
                !this.transitForm.courier ? errors['courier'] = true : void 0;
                !this.transitForm.shipment_type ? errors['shipment_type'] = true : void 0;
                !this.transitForm.tracking_number ? errors['tracking_number'] = true : void 0;

                this.transitForm.errors.record(errors);
                this.removeHasLoading();

                return;
            }

            axios.post('/user/sales-report/update-to-transit', {
                id: this.transitForm.id,
                status: this.transitForm.status,
                shipment_type: this.transitForm.shipment_type,
                courier: this.transitForm.courier,
                tracking_number: this.transitForm.tracking_number
            }).then(function (result) {
                toastr.success(_this6.lang.data['saved-msg']);

                _this6.selectedOrder.original_status = _this6.selectedOrder.status;
                _this6.selectedOrder.shipment_type = _this6.transitForm.shipment_type;
                _this6.selectedOrder.tracking_number = _this6.transitForm.tracking_number;
                _this6.transitForm.submitted = true;

                _this6.removeHasLoading();

                _this6.getStores();

                $('#inTransitModal').modal('hide');
            }).catch(function (error) {
                toastr.error(_this6.lang.data['update-failed-msg']);

                _this6.removeHasLoading();
            });
        },
        submit2Go: function submit2Go() {
            var _this7 = this;

            var enoughBalance = this.usable - this.fee;
            if (enoughBalance >= 0) {
                // var parcel2go = {
                //     d : {},
                // };
                // var data2 = {
                //     "id_": "APIUSER",
                //     "token_": "Xs2SakJ1M",
                //     "account_no": "",
                //     "bkg_request": false,
                //     "awb_code": "A0000001A",
                //     "origin_port": "MNL",
                //     "paymode ": "CS",
                //     "cod_amt": 5000,
                //     "instruction": "Near train station",
                //     "attachment": "ABC-132131232",
                //     cnee_dtls: {},
                //     px_dtls: [],
                // };  

                // var dtls = {
                //     "name": "JUAN DELA CRUZ",
                //     "street": "L10 B20 PUTATAN VILLAGE",
                //     "barangay": "PUTATAN",
                //     "city": "MUNTINLUPA CITY",
                //     "area_code": "1958",
                //     "mobile_number": "09281234567",
                //     "email_address": "juan_delacruz@yahoo.com.ph"
                // }
                // data2.cnee_dtls = dtls;

                // data2.px_dtls.push({ 
                //     "item_qty": 1,
                //     "pkg_code": "BOX",
                //     "actual_wt": 5,
                //     "length": 5,
                //     "width": 5,
                //     "height": 5,
                //     "declared_value": 1000,
                //     "item_description": "Cellular Phone Unit"
                // });
                // parcel2go.d = data2;

                // var jsontest = JSON.stringify(parcel2go);
                // //console.log(jsontest);

                // axios.post('https://expressapps.2go.com.ph/2gowebdev/model/API/shipment_request.asmx', {
                //     d: parcel2go,
                //     headers: {
                //         'Access-Control-Allow-Origin': '*',
                //         'Content-Type': 'application/json',
                //     }
                //     ,
                // }).then(result => {

                //     console.log(result);
                // })
                // .catch(error => {
                // });

                axios.post('/user/sales-report/update-to-transit', {
                    id: this.transitForm.id,
                    status: this.transitForm.status,
                    shipment_type: 'Pick Up',
                    courier: '2GO',
                    tracking_number: null
                }).then(function (result) {

                    toastr.success(_this7.lang.data['saved-msg']);

                    _this7.selectedOrder.original_status = _this7.selectedOrder.status;
                    _this7.selectedOrder.shipment_type = _this7.transitForm.shipment_type;
                    _this7.selectedOrder.tracking_number = result.tracking_number;
                    _this7.transitForm.submitted = true;

                    _this7.removeHasLoading();

                    _this7.getStores();

                    $('#inTransitModal2').modal('hide');
                }).catch(function (error) {
                    toastr.error(_this7.lang.data['update-failed-msg']);

                    _this7.removeHasLoading();
                });
            } else {
                $('#inTransitModal2').modal('hide');
                swal('Oops...', 'Your e-wallet usable balance is not enough for this transaction to proceed! ' + 'Check your e-wallet <a href="/user/ewallet">here.</a> ', 'warning');
            }
        },
        inTransit: function inTransit(data) {
            this.selectedOrder = data;
            var fee = parseFloat(data.shipping_fee) + parseFloat(data.vat) + parseFloat(data.charge);
            this.fee = fee.toFixed(2);
            this.transitForm = new Form(data);

            $('#inTransitModal2').modal('show');
        },
        cancelled: function cancelled(data) {
            this.selectedOrder = data;
            this.cancelForm = new Form(data);

            $('#cancelModal').modal('show');
        },
        submitReason: function submitReason() {
            var _this8 = this;

            var errors = {};

            if (!this.cancelForm.reason) {
                !this.cancelForm.reason ? errors['reason'] = true : void 0;

                this.cancelForm.errors.record(errors);
                this.removeHasLoadingFromCancelForm();

                return;
            }

            axios.post('/user/sales-report/cancel-order', {
                id: this.cancelForm.id,
                status: this.cancelForm.status,
                reason: this.cancelForm.reason
            }).then(function (result) {
                toastr.success(_this8.lang.data['saved-msg']);

                _this8.selectedOrder.original_status = _this8.selectedOrder.status;
                _this8.selectedOrder.reason = _this8.cancelForm.reason;
                _this8.cancelForm.submitted = true;

                _this8.removeHasLoadingFromCancelForm();

                _this8.getStores();

                $('#cancelModal').modal('hide');
            }).catch(function (error) {
                toastr.error(_this8.lang.data['update-failed-msg']);

                _this8.removeHasLoadingFromCancelForm();
            });
        },
        removeHasLoading: function removeHasLoading() {
            setTimeout(function () {
                var $btn = $('.btn-transit-submit');
                var $content = $btn.find('.btn-content');
                var $loader = $btn.find('.loader');

                $loader.fadeOut(100, function () {
                    $content.fadeIn(100);
                });
            }, 300);
        },
        removeHasLoadingFromCancelForm: function removeHasLoadingFromCancelForm() {
            setTimeout(function () {
                var $btn = $('.btn-cancel-form-submit');
                var $content = $btn.find('.btn-content');
                var $loader = $btn.find('.loader');

                $loader.fadeOut(100, function () {
                    $content.fadeIn(100);
                });
            }, 300);
        },
        print: function print() {
            var html_container = '';
            html_container = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns:fb="http://ogp.me/ns/fb#"><head>';
            html_container += '<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons" />';
            html_container += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>';
            html_container += '<style>.airway-product-modal .modal-content{border:none;font-size:11px}.airway-product-modal .modal-content .modal-body{overflow-y:auto;padding:24px 6px!important}.airway-product-modal .modal-content .modal-body .deliver{font-family:Cambria;font-size:15px;font-weight:700}.airway-product-modal .modal-content .modal-body .address{width:400px}.airway-product-modal .modal-content .modal-body table,.airway-product-modal .modal-content .modal-body td,.airway-product-modal .modal-content .modal-body th{border:1px solid #000}.airway-product-modal .modal-content .modal-body table{border-collapse:collapse;width:100%}.airway-product-modal .modal-content .modal-body th{text-align:left;font-weight:700}.airway-product-modal .modal-content .modal-body .awb{float:right;margin-right:20px;margin-top:-120px}.airway-product-modal .modal-content .modal-body .received{float:left;margin-left:15px;margin-top:5px}.airway-product-modal .modal-content .modal-body .attempt{float:right;margin-right:20px;margin-top:5px}.airway-product-modal .modal-content .modal-body .text-align{text-align:center}.airway-product-modal .modal-content .modal-body .padding-left{padding-left:10px}.airway-product-modal .modal-content .modal-body .border-top{border-top:none}.airway-product-modal .modal-content .modal-body .b{font-weight:700}.airway-product-modal .modal-content .modal-body .margin{margin-top:15px}.airway-product-modal .modal-content .modal-body .float{float:right}.airway-product-modal .modal-content .modal-body .inclusive{font-weight:700;font-size:9px;margin-right:10px}.airway-product-modal .modal-content .modal-body hr{border-style:solid;border-color:#000;margin-top:10px;width:300px}.airway-product-modal .modal-content .modal-footer{padding:5px}</style>';
            html_container += '</head><body><div class="airway-product-modal"><div class="modal-content"><div class="modal-body">';

            var body = $("#print").html();
            var myWindow = window.open('', '', 'width=500,height=500,scrollbars=yes,location=no');
            myWindow.document.write(html_container);
            myWindow.document.write(body);
            myWindow.document.write("</div></div></div>");
            myWindow.document.write('<div class="airway-product-modal"><div class="modal-content"><div class="modal-body">');
            myWindow.document.write(body);
            myWindow.document.write("</div></div></div></body></html>");
            myWindow.document.close();

            setTimeout(function () {
                myWindow.focus();
                myWindow.print();
            }, 3000);
        }
    },

    computed: {
        discountedPrice: function discountedPrice() {
            var $discountPercentage = this.details.discount;
            var $discountDecimal = $discountPercentage / 100;
            var $originalPrice = this.details.price_per_item;
            var $discountAmount = $discountDecimal * $originalPrice;
            var $discountedPrice = $originalPrice - $discountAmount;
            $discountedPrice = $discountedPrice.toFixed(2);
            return $discountedPrice;
        },
        price_per_item: function price_per_item() {
            if (this.details.fee_included == 1) return parseFloat(this.details.price_per_item) + parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat);else return this.details.price_per_item;
        },
        discountedAmount: function discountedAmount() {
            var amount = 0;
            if (this.details.discount != null) var $amount = this.details.price_per_item - this.details.sale_price;
            if (this.details.discount != null || this.details.discount != 0) var $amount = this.details.sale_price;else var $amount = 0;
            $amount = amount.toFixed(2);
            //$amount = $amount.toFixed(2);
            return $amount;
        },
        discountPercentage: function discountPercentage() {
            if (this.details.discount != null) {
                if (this.details.fee_included == 1) {
                    var $amount = 100 - (parseFloat(this.details.price_per_item) + parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat) - parseFloat(this.details.discount)) / (parseFloat(this.details.price_per_item) + parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat)) * 100;
                    return $amount.toFixed(2);
                } else return this.details.discount;
            } else return 0;
        },
        now: function now() {
            return this.price_per_item - this.discountedAmount;
        },
        totalAmount: function totalAmount() {
            var total = parseFloat(this.details.subtotal) + parseFloat(this.details.shipping_fee);
            total = total.toFixed(2);
            return total;
        },
        shipping: function shipping() {
            if (this.details.fee_included != 1) {
                var total = parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat);
                total = total.toFixed(2);
            } else var total = 0.00;

            return total;
        },
        weight: function weight() {
            var DW = parseFloat(this.details.length) * parseFloat(this.details.width) * parseFloat(this.details.height) / 3500;
            if (this.details.weight > DW) return this.details.weight;
            if (DW > this.details.weight) return DW;
        }
    },

    created: function created() {
        var _this9 = this;

        this.getStores();
        this.getUsableEwalletBalance();
        Event.listen('order.in-transit', function (data) {
            return _this9.inTransit(data);
        });
        Event.listen('order.cancelled', function (data) {
            return _this9.cancelled(data);
        });

        axios.get('/translate/sales-report').then(function (result) {
            _this9.lang = result.data;
        });
    },
    mounted: function mounted() {
        var _this10 = this;

        $('#inTransitModal2').on('hidden.bs.modal', function (e) {
            if (!_this10.transitForm.submitted) {
                _this10.selectedOrder.status = _this10.selectedOrder.original_status;
            }
        });

        $('#cancelModal').on('hidden.bs.modal', function (e) {
            if (!_this10.cancelForm.submitted) {
                _this10.selectedOrder.status = _this10.selectedOrder.original_status;
            }
        });
    }
});

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['order'],
	data: function data() {
		return {};
	},

	computed: {
		price_per_item: function price_per_item() {
			if (this.order.fee_included == 1) return parseFloat(this.order.price_per_item) + parseFloat(this.order.shipping_fee) + parseFloat(this.order.charge) + parseFloat(this.order.vat);else return this.order.price_per_item;
		},
		discountedAmount: function discountedAmount() {
			if (this.order.discount != null || this.order.discount != 0) var $amount = this.order.sale_price;else var $amount = 0;
			//$amount = $amount.toFixed(2);
			return $amount;
		},
		now: function now() {
			return this.price_per_item - this.discountedAmount;
		},
		total: function total() {
			return this.order.quantity * this.order.price_per_item;
		},
		orderstatus: function orderstatus() {
			if (this.order.status == "In Transit") {
				return 'in-transit';
			}
			return this.order.status.toLowerCase();
		}
	},
	components: {
		Multiselect: window.VueMultiselect.default
	},
	methods: {
		viewAirwayBill: function viewAirwayBill(id) {
			this.$parent.$parent.showAirwayBill(id);
		},
		receivedBySeller: function receivedBySeller(id) {
			var _this = this;

			swal({
				title: this.$parent.$parent.lang.data['confirm'],
				html: this.$parent.$parent.lang.data['received-returned-msg-1'],
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: this.$parent.$parent.lang.data['yes']
			}).then(function () {
				axiosAPIv1.post('order-details/receive-returned-item', {
					//make api to return the money to the buyer and stocks and the current date for the returned_received_date column
					id: id
				}).then(function (result) {
					toastr.success(_this.$parent.$parent.lang.data['saved-msg']);
					_this.$parent.$parent.getStores();
				}).catch(function (error) {
					toastr.error(_this.$parent.$parent.lang.data['update-failed-msg']);
				});
			}).catch(function () {
				return _this.order.status = _this.order.original_status;
			});
		},
		viewDetails: function viewDetails(id) {
			this.$parent.$parent.showDetails(id);
		},
		updateStatus: function updateStatus(status) {
			var _this2 = this;

			this.order.status = status;

			if (status === 'In Transit') {
				Event.fire('order.in-transit', this.order);
			} else if (status === 'Cancelled') {
				Event.fire('order.cancelled', this.order);
			} else if (status === 'Pending') {
				swal({
					title: this.$parent.$parent.lang.data['confirm'],
					html: this.$parent.$parent.lang.data['mark-pending-msg'],
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: this.$parent.$parent.lang.data['yes']
				}).then(function () {
					axios.get('/user/sales-report/updateStatus/' + _this2.order.id + '/' + status).then(function (result) {
						toastr.success(_this2.$parent.$parent.lang.data['saved-msg']);
						_this2.$parent.$parent.getStores();
					}).catch(function (error) {
						toastr.error(_this2.$parent.$parent.lang.data['update-failed-msg']);
					});
				}).catch(function () {
					return _this2.order.status = _this2.order.original_status;
				});
			}
		},
		statusAction: function statusAction() {
			if (this.order.status === 'In Transit') {
				Event.fire('order.in-transit', this.order);
			} else if (this.order.status === 'Cancelled') {
				Event.fire('order.cancelled', this.order);
			}
		}
	},

	created: function created() {
		this.order.original_status = this.order.status;
	},
	beforeUpdate: function beforeUpdate() {
		if (!this.order.original_status) {
			this.order.original_status = this.order.status;
		}
	}
});

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__salesTable_vue__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__salesTable_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__salesTable_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['store', 'index'],
	components: {
		'orders': __WEBPACK_IMPORTED_MODULE_0__salesTable_vue___default.a
	},
	methods: {
		filterStatus: function filterStatus(id, filter) {
			if (filter == "Cancelled") {
				$('#cancelled_tbl_' + id).addClass('show');
				$('#all_tbl_' + id).removeClass('show');
				$('#delivered_tbl_' + id).removeClass('show');
				$('#pending_tbl_' + id).removeClass('show');
				$('#transit_tbl_' + id).removeClass('show');
				$('#pending_hdr' + id).removeClass('show');
				$('#all_hdr' + id).removeClass('show');
				$('#cancelled_hdr' + id).addClass('show');
				$('#delivered_hdr' + id).removeClass('show');
				$('#transit_hdr' + id).removeClass('show');
				$('#fl_Pending_' + id).removeClass('active');
				$('#fl_Cancelled_' + id).addClass('active');
				$('#fl_Delivered_' + id).removeClass('active');
				$('#fl_All_' + id).removeClass('active');
				$('#fl_Transit_' + id).removeClass('active');
			} else if (filter == "Pending") {
				$('#cancelled_tbl_' + id).removeClass('show');
				$('#all_tbl_' + id).removeClass('show');
				$('#delivered_tbl_' + id).removeClass('show');
				$('#pending_tbl_' + id).addClass('show');
				$('#transit_tbl_' + id).removeClass('show');
				$('#pending_hdr' + id).addClass('show');
				$('#all_hdr' + id).removeClass('show');
				$('#cancelled_hdr' + id).removeClass('show');
				$('#delivered_hdr' + id).removeClass('show');
				$('#transit_hdr' + id).removeClass('show');
				$('#fl_Pending_' + id).addClass('active');
				$('#fl_Cancelled_' + id).removeClass('active');
				$('#fl_Delivered_' + id).removeClass('active');
				$('#fl_All_' + id).removeClass('active');
				$('#fl_Transit_' + id).removeClass('active');
			} else if (filter == "Delivered") {
				$('#cancelled_tbl_' + id).removeClass('show');
				$('#all_tbl_' + id).removeClass('show');
				$('#delivered_tbl_' + id).addClass('show');
				$('#pending_tbl_' + id).removeClass('show');
				$('#transit_tbl_' + id).removeClass('show');
				$('#pending_hdr' + id).removeClass('show');
				$('#all_hdr' + id).removeClass('show');
				$('#cancelled_hdr' + id).removeClass('show');
				$('#delivered_hdr' + id).addClass('show');
				$('#transit_hdr' + id).removeClass('show');
				$('#fl_Pending_' + id).removeClass('active');
				$('#fl_Cancelled_' + id).removeClass('active');
				$('#fl_Delivered_' + id).addClass('active');
				$('#fl_All_' + id).removeClass('active');
				$('#fl_Transit_' + id).removeClass('active');
			} else if (filter == "All") {
				$('#cancelled_tbl_' + id).removeClass('show');
				$('#all_tbl_' + id).addClass('show');
				$('#delivered_tbl_' + id).removeClass('show');
				$('#pending_tbl_' + id).removeClass('show');
				$('#transit_tbl_' + id).removeClass('show');
				$('#pending_hdr' + id).removeClass('show');
				$('#all_hdr' + id).addClass('show');
				$('#cancelled_hdr' + id).removeClass('show');
				$('#delivered_hdr' + id).removeClass('show');
				$('#transit_hdr' + id).removeClass('show');
				$('#fl_Pending_' + id).removeClass('active');
				$('#fl_Cancelled_' + id).removeClass('active');
				$('#fl_Delivered_' + id).removeClass('active');
				$('#fl_All_' + id).addClass('active');
				$('#fl_Transit_' + id).removeClass('active');
			} else if (filter == "Transit") {
				$('#cancelled_tbl_' + id).removeClass('show');
				$('#all_tbl_' + id).removeClass('show');
				$('#delivered_tbl_' + id).removeClass('show');
				$('#pending_tbl_' + id).removeClass('show');
				$('#transit_tbl_' + id).addClass('show');
				$('#pending_hdr' + id).removeClass('show');
				$('#all_hdr' + id).removeClass('show');
				$('#cancelled_hdr' + id).removeClass('show');
				$('#delivered_hdr' + id).removeClass('show');
				$('#transit_hdr' + id).addClass('show');
				$('#fl_Pending_' + id).removeClass('active');
				$('#fl_Cancelled_' + id).removeClass('active');
				$('#fl_Delivered_' + id).removeClass('active');
				$('#fl_All_' + id).removeClass('active');
				$('#fl_Transit_' + id).addClass('active');
			}
		}
	}
});

$(document).ready(function () {
	$(document).on("click", ".pnl-store", function () {
		$('#cancelled_tbl_' + this.id).removeClass('show');
		$('#all_tbl_' + this.id).addClass('show');
		$('#delivered_tbl_' + this.id).removeClass('show');
		$('#pending_tbl_' + this.id).removeClass('show');
		$('#transit_tbl' + this.id).removeClass('show');
		$('#pending_hdr' + this.id).removeClass('show');
		$('#all_hdr' + this.id).addClass('show');
		$('#cancelled_hdr' + this.id).removeClass('show');
		$('#delivered_hdr' + this.id).removeClass('show');
		$('#fl_Pending_' + this.id).removeClass('active');
		$('#fl_Cancelled_' + this.id).removeClass('active');
		$('#fl_Delivered_' + this.id).removeClass('active');
		$('#fl_All_' + this.id).addClass('active');
		$('#fl_Transit_' + this.id).removeClass('active');
	});
});

/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(287),
  /* template */
  __webpack_require__(405),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\reports\\sales\\salesTable.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] salesTable.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e64b307e", Component.options)
  } else {
    hotAPI.reload("data-v-e64b307e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 353:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(288),
  /* template */
  __webpack_require__(397),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\reports\\sales\\storePanel.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] storePanel.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ad290a7c", Component.options)
  } else {
    hotAPI.reload("data-v-ad290a7c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-12"
  }, [_c('div', {
    staticClass: "panel-group store-panel"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-collapse collapse",
    class: { in: (_vm.index == 0)
    },
    attrs: {
      "id": 'store_' + _vm.store.id
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('ul', {
    staticClass: "nav nav-pills"
  }, [_c('li', {
    staticClass: "badge-details",
    attrs: {
      "id": 'fl_Pending_' + _vm.store.id
    }
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.filterStatus(_vm.store.id, 'Pending')
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang.data['pending-order']) + "\n\t\t\t\t\t\t\t\t  \t"), _c('span', {
    staticClass: "badge"
  }, [_vm._v(_vm._s(_vm.store.pending_order_count.length))])])]), _vm._v(" "), _c('li', {
    staticClass: "badge-details",
    attrs: {
      "id": 'fl_Delivered_' + _vm.store.id
    }
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.filterStatus(_vm.store.id, 'Delivered')
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang.data['delivered']) + "\n\t\t\t\t\t\t\t\t\t  "), _c('span', {
    staticClass: "badge"
  }, [_vm._v(_vm._s(_vm.store.delivered_order_count.length))])])]), _vm._v(" "), _c('li', {
    staticClass: "badge-details",
    attrs: {
      "id": 'fl_Cancelled_' + _vm.store.id
    }
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.filterStatus(_vm.store.id, 'Cancelled')
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang.data['cancelled-order']) + " \n\t\t\t\t\t\t\t\t  \t"), _c('span', {
    staticClass: "badge"
  }, [_vm._v(_vm._s(_vm.store.cancelled_order_count.length))])])]), _vm._v(" "), _c('li', {
    staticClass: "badge-details",
    attrs: {
      "id": 'fl_Transit_' + _vm.store.id
    }
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.filterStatus(_vm.store.id, 'Transit')
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang.data['in-transit']) + "\n\t\t\t\t\t\t\t\t  \t"), _c('span', {
    staticClass: "badge"
  }, [_vm._v(_vm._s(_vm.store.transit_order_count.length))])])]), _vm._v(" "), _c('li', {
    staticClass: "active badge-details",
    attrs: {
      "id": 'fl_All_' + _vm.store.id
    }
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.filterStatus(_vm.store.id, 'All')
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang.data['all']) + " \n\t\t\t\t\t\t\t\t  \t"), _c('span', {
    staticClass: "badge"
  }, [_vm._v(_vm._s(_vm.store.order_details.length))])])])]), _vm._v(" "), (_vm.store.order_details != '') ? _c('table', {
    staticClass: "table table-striped filter-table sales-tbl show",
    attrs: {
      "id": 'all_tbl_' + _vm.store.id
    }
  }, [_c('thead', [_c('tr', [_c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['transaction']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['product']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['qty']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['unit-price']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['total']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['status']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  }), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  })])]), _vm._v(" "), _c('tbody', _vm._l((_vm.store.order_details), function(order) {
    return _c('orders', {
      key: order.id,
      attrs: {
        "order": order
      }
    })
  }))]) : _vm._e(), _vm._v(" "), (_vm.store.pending_order_count != '') ? _c('table', {
    staticClass: "table table-striped filter-table sales-tbl",
    attrs: {
      "id": 'pending_tbl_' + _vm.store.id
    }
  }, [_c('thead', [_c('tr', [_c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['transaction']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['product']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['qty']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['unit-price']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['total']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['status']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  }), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  })])]), _vm._v(" "), _c('tbody', _vm._l((_vm.store.pending_order_count), function(order) {
    return _c('orders', {
      key: order.id,
      attrs: {
        "order": order
      }
    })
  }))]) : _vm._e(), _vm._v(" "), (_vm.store.delivered_order_count != '') ? _c('table', {
    staticClass: "table table-striped filter-table sales-tbl",
    attrs: {
      "id": 'delivered_tbl_' + _vm.store.id
    }
  }, [_c('thead', [_c('tr', [_c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['transaction']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['product']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['qty']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['unit-price']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['total']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['status']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  }), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  })])]), _vm._v(" "), _c('tbody', _vm._l((_vm.store.delivered_order_count), function(order) {
    return _c('orders', {
      key: order.id,
      attrs: {
        "order": order
      }
    })
  }))]) : _vm._e(), _vm._v(" "), (_vm.store.cancelled_order_count != '') ? _c('table', {
    staticClass: "table table-striped filter-table sales-tbl",
    attrs: {
      "id": 'cancelled_tbl_' + _vm.store.id
    }
  }, [_c('thead', [_c('tr', [_c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['transaction']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['product']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['qty']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['unit-price']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['total']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['status']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  }), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  })])]), _vm._v(" "), _c('tbody', _vm._l((_vm.store.cancelled_order_count), function(order) {
    return _c('orders', {
      key: order.id,
      attrs: {
        "order": order
      }
    })
  }))]) : _vm._e(), _vm._v(" "), (_vm.store.transit_order_count != '') ? _c('table', {
    staticClass: "table table-striped filter-table sales-tbl",
    attrs: {
      "id": 'transit_tbl_' + _vm.store.id
    }
  }, [_c('thead', [_c('tr', [_c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['transaction']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['product']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['qty']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['unit-price']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header hidden-xs"
  }, [_vm._v(_vm._s(this.$parent.lang.data['total']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-2 report-header"
  }, [_vm._v(_vm._s(this.$parent.lang.data['status']))]), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  }), _vm._v(" "), _c('th', {
    staticClass: "col-sm-1 report-header"
  })])]), _vm._v(" "), _c('tbody', _vm._l((_vm.store.transit_order_count), function(order) {
    return _c('orders', {
      key: order.id,
      attrs: {
        "order": order
      }
    })
  }))]) : _vm._e(), _vm._v(" "), (_vm.store.order_details == '') ? _c('div', {
    staticClass: "no-purchase show",
    attrs: {
      "id": 'all_hdr' + _vm.store.id
    }
  }, [_c('h4', [_vm._v(_vm._s(this.$parent.lang.data['no-purchases-msg']))])]) : _vm._e(), _vm._v(" "), (_vm.store.pending_order_count == '') ? _c('div', {
    staticClass: "no-purchase",
    attrs: {
      "id": 'pending_hdr' + _vm.store.id
    }
  }, [_c('h4', [_vm._v(_vm._s(this.$parent.lang.data['no-pending-order-msg']))])]) : _vm._e(), _vm._v(" "), (_vm.store.delivered_order_count == '') ? _c('div', {
    staticClass: "no-purchase",
    attrs: {
      "id": 'delivered_hdr' + _vm.store.id
    }
  }, [_c('h4', [_vm._v(_vm._s(this.$parent.lang.data['no-delivered-msg']))])]) : _vm._e(), _vm._v(" "), (_vm.store.cancelled_order_count == '') ? _c('div', {
    staticClass: "no-purchase",
    attrs: {
      "id": 'cancelled_hdr' + _vm.store.id
    }
  }, [_c('h4', [_vm._v(_vm._s(this.$parent.lang.data['no-cancelled-order-msg']))])]) : _vm._e(), _vm._v(" "), (_vm.store.transit_order_count == '') ? _c('div', {
    staticClass: "no-purchase",
    attrs: {
      "id": 'transit_hdr' + _vm.store.id
    }
  }, [_c('h4', [_vm._v(_vm._s(this.$parent.lang.data['no-intransit-msg']))])]) : _vm._e()])])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-ad290a7c", module.exports)
  }
}

/***/ }),

/***/ 405:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('tr', [_c('td', {
    staticClass: "report-header"
  }, [_c('p', [_c('b', [_vm._v(_vm._s(_vm.order.tracking_number))])]), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.order.created_at))])]), _vm._v(" "), _c('td', {
    staticClass: "report-header hidden-xs"
  }, [_vm._v(_vm._s(_vm.order.product.name))]), _vm._v(" "), _c('td', {
    staticClass: "report-header hidden-xs"
  }, [_vm._v(_vm._s(_vm.order.quantity))]), _vm._v(" "), _c('td', {
    staticClass: "report-header hidden-xs"
  }, [_vm._v(_vm._s(_vm._f("currency")(_vm.now)))]), _vm._v(" "), _c('td', {
    staticClass: "report-header hidden-xs"
  }, [_vm._v(_vm._s(_vm.order.subtotal))]), _vm._v(" "), _c('td', {
    staticClass: "report-header"
  }, [((_vm.order.status != 'Received') && (_vm.order.status != 'Delivered') && (_vm.order.status != 'Returned') && (_vm.order.status != 'In Transit') && (_vm.order.status != 'Cancelled')) ? _c('div', {
    staticClass: "btn-group"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-raised btn-sm",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.statusAction
    }
  }, [('In Transit' == _vm.order.status) ? _c('span', [_vm._v(_vm._s(this.$parent.$parent.lang.data['in-transit']))]) : _vm._e(), _vm._v(" "), ('Cancelled' == _vm.order.status) ? _c('span', [_vm._v(_vm._s(this.$parent.$parent.lang.data['cancelled']))]) : _vm._e(), _vm._v(" "), ('Pending' == _vm.order.status) ? _c('span', [_vm._v(_vm._s(this.$parent.$parent.lang.data['pending']))]) : _vm._e()]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('ul', {
    staticClass: "dropdown-menu"
  }, [('In Transit' != _vm.order.status) ? _c('li', [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.updateStatus('In Transit')
      }
    }
  }, [_vm._v(_vm._s(this.$parent.$parent.lang.data['in-transit']))])]) : _vm._e(), _vm._v(" "), ('Cancelled' != _vm.order.status) ? _c('li', [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.updateStatus('Cancelled')
      }
    }
  }, [_vm._v(_vm._s(this.$parent.$parent.lang.data['cancelled']))])]) : _vm._e(), _vm._v(" "), ('Pending' != _vm.order.status) ? _c('li', [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.updateStatus('Pending')
      }
    }
  }, [_vm._v(_vm._s(this.$parent.$parent.lang.data['pending']))])]) : _vm._e()])]) : _vm._e(), _vm._v(" "), ((_vm.order.status == 'Returned' && _vm.order.returned_received_date == null)) ? _c('button', {
    staticClass: "btn btn-xs btn-raised btn-warning",
    attrs: {
      "title": this.$parent.$parent.lang.data['received-returned-msg-2'],
      "data-toggle": "tooltip",
      "data-placement": "bottom"
    },
    on: {
      "click": function($event) {
        _vm.receivedBySeller(_vm.order.id)
      }
    }
  }, [_c('span', {
    staticClass: "fa fa-exclamation"
  })]) : _vm._e(), _vm._v(" "), ((_vm.order.status == 'In Transit') || (_vm.order.status == 'Received') || (_vm.order.status == 'Delivered') || (_vm.order.status == 'Returned') || (_vm.order.status == 'Cancelled')) ? _c('span', [_vm._v(_vm._s(this.$parent.$parent.lang.data[this.orderstatus]))]) : _vm._e()]), _vm._v(" "), _c('td', [_c('a', {
    staticStyle: {
      "cursor": "pointer"
    },
    on: {
      "click": function($event) {
        _vm.viewDetails(_vm.order.id)
      }
    }
  }, [_c('span', {
    staticClass: "hidden-xs hidden-sm"
  }, [_vm._v(_vm._s(this.$parent.$parent.lang.data['see-more']))]), _vm._v(" "), _vm._m(1)])]), _vm._v(" "), _c('td', [((_vm.order.status != 'Pending')) ? _c('a', {
    staticStyle: {
      "cursor": "pointer"
    },
    on: {
      "click": function($event) {
        _vm.viewAirwayBill(_vm.order.id)
      }
    }
  }, [_c('span', {
    staticClass: "hidden-xs hidden-sm"
  }, [_vm._v(_vm._s(this.$parent.$parent.lang.data['airway-bill']))]), _vm._v(" "), _vm._m(2)]) : _vm._e()])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "btn btn-primary btn-raised btn-sm dropdown-toggle",
    attrs: {
      "type": "button",
      "data-toggle": "dropdown",
      "aria-haspopup": "true",
      "aria-expanded": "false"
    }
  }, [_c('span', {
    staticClass: "caret"
  }), _vm._v(" "), _c('span', {
    staticClass: "sr-only"
  }, [_vm._v("Toggle Dropdown")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "hidden-lg hidden-md",
    attrs: {
      "data-toggle": "tooltip",
      "data-placement": "left",
      "title": "",
      "data-original-title": "See More"
    }
  }, [_c('i', {
    staticClass: "fa fa-eye",
    attrs: {
      "aria-hidden": "true"
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', {
    staticClass: "hidden-lg hidden-md",
    attrs: {
      "data-toggle": "tooltip",
      "data-placement": "left",
      "title": "",
      "data-original-title": "Airway Bill"
    }
  }, [_c('i', {
    staticClass: "fa fa-file-text",
    attrs: {
      "aria-hidden": "true"
    }
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-e64b307e", module.exports)
  }
}

/***/ }),

/***/ 450:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(200);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9zYWxlcy1yZXBvcnQuanMiLCJ3ZWJwYWNrOi8vL3NhbGVzVGFibGUudnVlIiwid2VicGFjazovLy9zdG9yZVBhbmVsLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcmVwb3J0cy9zYWxlcy9zYWxlc1RhYmxlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcmVwb3J0cy9zYWxlcy9zdG9yZVBhbmVsLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcmVwb3J0cy9zYWxlcy9zdG9yZVBhbmVsLnZ1ZT81MGI3Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9yZXBvcnRzL3NhbGVzL3NhbGVzVGFibGUudnVlPzU2ZGUiXSwibmFtZXMiOlsiJCIsImRvY3VtZW50IiwicmVhZHkiLCJjdHgiLCJteUNoYXJ0IiwiQ2hhcnQiLCJ0eXBlIiwiZGF0YSIsImxhYmVscyIsImRhdGFzZXRzIiwibGFiZWwiLCJiYWNrZ3JvdW5kQ29sb3IiLCJib3JkZXJDb2xvciIsImJvcmRlcldpZHRoIiwib3B0aW9ucyIsInNjYWxlcyIsInlBeGVzIiwidGlja3MiLCJiZWdpbkF0WmVybyIsInJlc3BvbnNpdmUiLCJtYWludGFpbkFzcGVjdFJhdGlvIiwiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImZpbHRlciIsInZhbHVlIiwibnVtYmVyV2l0aENvbW1hcyIsInBhcnNlRmxvYXQiLCJ0b0ZpeGVkIiwidm0iLCJlbCIsImxpc3RTdG9yZSIsImRldGFpbHMiLCJzZXJ2aWNlVHlwZSIsInVwZGF0ZVJlbWFya3MiLCJGb3JtIiwiaWQiLCJzZWxsZXJfbm90ZXMiLCJiYXNlVVJMIiwib3B0aW9uUGlja1VwIiwib3B0aW9uRHJvcCIsInNlbGVjdGVkT3JkZXIiLCJ1c2FibGUiLCJmZWUiLCJ0cmFuc2l0Rm9ybSIsInNoaXBtZW50X3R5cGUiLCJjb3VyaWVyIiwidHJhY2tpbmdfbnVtYmVyIiwiY2FuY2VsRm9ybSIsInJlYXNvbiIsImhhc0Rpc2NvdW50IiwibGFuZyIsImNvbXBvbmVudHMiLCJNdWx0aXNlbGVjdCIsIndpbmRvdyIsIlZ1ZU11bHRpc2VsZWN0IiwiZGVmYXVsdCIsIm1ldGhvZHMiLCJzaG93QWlyd2F5QmlsbCIsImF4aW9zIiwiZ2V0IiwidGhlbiIsInJlc3VsdCIsImRpc2NvdW50IiwibW9kYWwiLCJnZXRVc2FibGVFd2FsbGV0QmFsYW5jZSIsIkxhcmF2ZWwiLCJ1c2VyIiwiYXhpb3NBUEl2MSIsInNob3dEZXRhaWxzIiwic3VibWl0Tm90ZSIsInN1Ym1pdCIsInRvYXN0ciIsInN1Y2Nlc3MiLCJjYXRjaCIsImVycm9yIiwiZ2V0U3RvcmVzIiwic3RvcmVzIiwiY2hhbmdlT3B0aW9uIiwic3VibWl0VHJhY2tpbmdOdW1iZXIiLCJlcnJvcnMiLCJyZWNvcmQiLCJyZW1vdmVIYXNMb2FkaW5nIiwicG9zdCIsInN0YXR1cyIsIm9yaWdpbmFsX3N0YXR1cyIsInN1Ym1pdHRlZCIsInN1Ym1pdDJHbyIsImVub3VnaEJhbGFuY2UiLCJzd2FsIiwiaW5UcmFuc2l0Iiwic2hpcHBpbmdfZmVlIiwidmF0IiwiY2hhcmdlIiwiY2FuY2VsbGVkIiwic3VibWl0UmVhc29uIiwicmVtb3ZlSGFzTG9hZGluZ0Zyb21DYW5jZWxGb3JtIiwic2V0VGltZW91dCIsIiRidG4iLCIkY29udGVudCIsImZpbmQiLCIkbG9hZGVyIiwiZmFkZU91dCIsImZhZGVJbiIsInByaW50IiwiaHRtbF9jb250YWluZXIiLCJib2R5IiwiaHRtbCIsIm15V2luZG93Iiwib3BlbiIsIndyaXRlIiwiY2xvc2UiLCJmb2N1cyIsImNvbXB1dGVkIiwiZGlzY291bnRlZFByaWNlIiwiJGRpc2NvdW50UGVyY2VudGFnZSIsIiRkaXNjb3VudERlY2ltYWwiLCIkb3JpZ2luYWxQcmljZSIsInByaWNlX3Blcl9pdGVtIiwiJGRpc2NvdW50QW1vdW50IiwiJGRpc2NvdW50ZWRQcmljZSIsImZlZV9pbmNsdWRlZCIsImRpc2NvdW50ZWRBbW91bnQiLCJhbW91bnQiLCIkYW1vdW50Iiwic2FsZV9wcmljZSIsImRpc2NvdW50UGVyY2VudGFnZSIsIm5vdyIsInRvdGFsQW1vdW50IiwidG90YWwiLCJzdWJ0b3RhbCIsInNoaXBwaW5nIiwid2VpZ2h0IiwiRFciLCJsZW5ndGgiLCJ3aWR0aCIsImhlaWdodCIsImNyZWF0ZWQiLCJFdmVudCIsImxpc3RlbiIsIm1vdW50ZWQiLCJvbiJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBQSxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVTtBQUN4QkMsVUFBTUgsRUFBRSxjQUFGLENBQU47QUFDQSxRQUFJSSxVQUFVLElBQUlDLEtBQUosQ0FBVUYsR0FBVixFQUFlO0FBQ3pCRyxjQUFNLE1BRG1CO0FBRXpCQyxjQUFNO0FBQ0ZDLG9CQUFRLENBQ0ksU0FESixFQUNlLFVBRGYsRUFDMkIsT0FEM0IsRUFDb0MsT0FEcEMsRUFDNkMsS0FEN0MsRUFDb0QsTUFEcEQsRUFFSSxNQUZKLEVBRVksUUFGWixFQUVzQixXQUZ0QixFQUVtQyxTQUZuQyxFQUU4QyxVQUY5QyxFQUUwRCxVQUYxRCxDQUROO0FBS0ZDLHNCQUFVLENBQUM7QUFDUEMsdUJBQU8sWUFEQTtBQUVQSCxzQkFBTSxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsQ0FBVCxFQUFZLENBQVosRUFBZSxDQUFmLEVBQWtCLENBQWxCLEVBQXFCLEVBQXJCLEVBQXlCLEVBQXpCLEVBQTZCLEVBQTdCLEVBQWlDLENBQWpDLEVBQW9DLEVBQXBDLEVBQXdDLEVBQXhDLENBRkM7QUFHUEksaUNBQWlCLENBQ2IseUJBRGEsQ0FIVjtBQU1QQyw2QkFBYSxDQUNULG9CQURTLENBTk47QUFTUEMsNkJBQWE7QUFUTixhQUFELEVBV1Y7QUFDSUgsdUJBQU8sU0FEWDtBQUVJSCxzQkFBTSxDQUFDLENBQUQsRUFBSSxDQUFKLEVBQU8sRUFBUCxFQUFXLEVBQVgsRUFBZSxFQUFmLEVBQW1CLEVBQW5CLEVBQXVCLENBQXZCLEVBQTBCLENBQTFCLEVBQTZCLENBQTdCLEVBQWdDLEVBQWhDLEVBQW9DLENBQXBDLEVBQXVDLEVBQXZDLENBRlY7QUFHSUksaUNBQWlCLENBQ2IseUJBRGEsQ0FIckI7QUFNSUMsNkJBQWEsQ0FDVCx1QkFEUyxDQU5qQjtBQVNJQyw2QkFBYTtBQVRqQixhQVhVLEVBc0JWO0FBQ0lILHVCQUFPLFNBRFg7QUFFSUgsc0JBQU0sQ0FBQyxDQUFELEVBQUksQ0FBSixFQUFPLENBQVAsRUFBVSxDQUFWLEVBQWEsQ0FBYixFQUFnQixFQUFoQixFQUFvQixFQUFwQixFQUF3QixFQUF4QixFQUE0QixDQUE1QixFQUErQixFQUEvQixFQUFtQyxFQUFuQyxFQUF1QyxFQUF2QyxDQUZWO0FBR0lJLGlDQUFpQixDQUNiLHVCQURhLENBSHJCO0FBTUlDLDZCQUFhLENBQ1QscUJBRFMsQ0FOakI7QUFTSUMsNkJBQWE7QUFUakIsYUF0QlU7QUFMUixTQUZtQjtBQTBDekJDLGlCQUFTO0FBQ0xDLG9CQUFRO0FBQ0pDLHVCQUFPLENBQUM7QUFDSkMsMkJBQU87QUFDSEMscUNBQVk7QUFEVDtBQURILGlCQUFEO0FBREgsYUFESDtBQVFMQyx3QkFBVyxJQVJOO0FBU0xDLGlDQUFxQjtBQVRoQjtBQTFDZ0IsS0FBZixDQUFkO0FBc0RILENBeEREOztBQTJEQUMsSUFBSUMsU0FBSixDQUFjLGFBQWQsRUFBOEIsbUJBQUFDLENBQVEsR0FBUixDQUE5Qjs7QUFHQUYsSUFBSUcsTUFBSixDQUFXLFVBQVgsRUFBdUIsVUFBVUMsS0FBVixFQUFpQjtBQUNwQyxXQUFPQyxpQkFBaUJDLFdBQVdGLEtBQVgsRUFBa0JHLE9BQWxCLENBQTBCLENBQTFCLENBQWpCLENBQVA7QUFDSCxDQUZEOztBQUlBLElBQUlDLEtBQUssSUFBSVIsR0FBSixDQUFRO0FBQ2JTLFFBQUcseUJBRFU7QUFFYnZCLFVBQUs7QUFDRHdCLG1CQUFVLEVBRFQ7QUFFREMsaUJBQVEsRUFGUDtBQUdEQyxxQkFBWSxDQUNQLFVBRE8sRUFFUCxTQUZPLENBSFg7QUFPREMsdUJBQWUsSUFBSUMsSUFBSixDQUFTO0FBQ25CQyxnQkFBUyxFQURVO0FBRW5CQywwQkFBYztBQUZLLFNBQVQsRUFHYixFQUFFQyxTQUFTLG9CQUFYLEVBSGEsQ0FQZDtBQVdEQyxzQkFBYyxLQVhiO0FBWURDLG9CQUFZLEtBWlg7QUFhREMsdUJBQWUsSUFiZDtBQWNEQyxnQkFBUSxJQWRQO0FBZURDLGFBQUssSUFmSjtBQWdCREMscUJBQWEsSUFBSVQsSUFBSixDQUFTO0FBQ2xCVSwyQkFBZSxJQURHO0FBRWxCQyxxQkFBUyxJQUZTO0FBR2xCQyw2QkFBaUI7QUFIQyxTQUFULENBaEJaO0FBcUJEQyxvQkFBWSxJQUFJYixJQUFKLENBQVM7QUFDakJjLG9CQUFRO0FBRFMsU0FBVCxDQXJCWDtBQXdCREMscUJBQWEsS0F4Qlo7QUF5QkRDLGNBQUs7QUF6QkosS0FGUTs7QUE4QmJDLGdCQUFZO0FBQ1JDLHFCQUFhQyxPQUFPQyxjQUFQLENBQXNCQztBQUQzQixLQTlCQztBQWlDYkMsYUFBUTtBQUNKQyxzQkFESSwwQkFDV3RCLEVBRFgsRUFDYztBQUFBOztBQUNkLGlCQUFLSixPQUFMLEdBQWUsRUFBZjtBQUNBMkIsa0JBQU1DLEdBQU4sQ0FBVSxnQ0FBOEJ4QixFQUF4QyxFQUNHeUIsSUFESCxDQUNRLGtCQUFVO0FBQ2Qsc0JBQUs3QixPQUFMLEdBQWU4QixPQUFPdkQsSUFBdEI7QUFDQSxvQkFBR3VELE9BQU92RCxJQUFQLENBQVl3RCxRQUFaLElBQXdCLENBQTNCLEVBQTZCO0FBQ3pCLDBCQUFLYixXQUFMLEdBQW1CLElBQW5CO0FBQ0gsaUJBRkQsTUFFSztBQUNELDBCQUFLQSxXQUFMLEdBQW1CLEtBQW5CO0FBQ0g7QUFDSixhQVJEO0FBU0FsRCxjQUFFLGlCQUFGLEVBQXFCZ0UsS0FBckIsQ0FBMkIsUUFBM0I7QUFDSCxTQWJHO0FBY0pDLCtCQWRJLHFDQWNxQjtBQUFBOztBQUNyQixnQkFBR0MsUUFBUUMsSUFBWCxFQUFnQjtBQUNaQywyQkFDS1IsR0FETCxDQUNTLFlBRFQsRUFFS0MsSUFGTCxDQUVVLGtCQUFVO0FBQUcsMkJBQUtuQixNQUFMLEdBQWNmLFdBQVdtQyxPQUFPdkQsSUFBUCxDQUFZQSxJQUFaLENBQWlCbUMsTUFBNUIsQ0FBZDtBQUFrRCxpQkFGekU7QUFHSDtBQUNKLFNBcEJHO0FBcUJKMkIsbUJBckJJLHVCQXFCUWpDLEVBckJSLEVBcUJXO0FBQUE7O0FBQ1gsaUJBQUtKLE9BQUwsR0FBZSxFQUFmO0FBQ0EyQixrQkFBTUMsR0FBTixDQUFVLGdDQUE4QnhCLEVBQXhDLEVBQ0d5QixJQURILENBQ1Esa0JBQVU7QUFDZCx1QkFBSzdCLE9BQUwsR0FBZThCLE9BQU92RCxJQUF0QjtBQUNBLHVCQUFLMkIsYUFBTCxDQUFtQkcsWUFBbkIsR0FBa0N5QixPQUFPdkQsSUFBUCxDQUFZOEIsWUFBOUM7QUFDQSxvQkFBR3lCLE9BQU92RCxJQUFQLENBQVl3RCxRQUFaLElBQXdCLENBQTNCLEVBQTZCO0FBQ3pCLDJCQUFLYixXQUFMLEdBQW1CLElBQW5CO0FBQ0gsaUJBRkQsTUFFSztBQUNELDJCQUFLQSxXQUFMLEdBQW1CLEtBQW5CO0FBQ0g7QUFDRGxELGtCQUFFLGdCQUFGLEVBQW9CZ0UsS0FBcEIsQ0FBMEIsTUFBMUI7QUFDSCxhQVZEO0FBWUgsU0FuQ0c7O0FBb0NKTSxvQkFBVyxvQkFBVWxDLEVBQVYsRUFBYTtBQUFBOztBQUNwQixpQkFBS0YsYUFBTCxDQUFtQkUsRUFBbkIsR0FBd0JBLEVBQXhCO0FBQ0EsaUJBQUtGLGFBQUwsQ0FBbUJxQyxNQUFuQixDQUEwQixNQUExQixFQUFrQyxpQkFBbEMsRUFDS1YsSUFETCxDQUNVLGtCQUFVO0FBQ1pXLHVCQUFPQyxPQUFQLENBQWUsT0FBS3RCLElBQUwsQ0FBVTVDLElBQVYsQ0FBZSxrQkFBZixDQUFmO0FBRUgsYUFKTCxFQUtLbUUsS0FMTCxDQUtXLGlCQUFTO0FBQ1pGLHVCQUFPRyxLQUFQLENBQWEsT0FBS3hCLElBQUwsQ0FBVTVDLElBQVYsQ0FBZSxtQkFBZixDQUFiO0FBQ0gsYUFQTDtBQVNILFNBL0NHO0FBZ0RKcUUsaUJBaERJLHVCQWdETztBQUFBOztBQUNQakIsa0JBQU1DLEdBQU4sQ0FBVSw4QkFBVixFQUNHQyxJQURILENBQ1Esa0JBQVU7QUFDZCx1QkFBSzlCLFNBQUwsR0FBaUIrQixPQUFPdkQsSUFBUCxDQUFZc0UsTUFBN0I7QUFDSCxhQUhEO0FBSUgsU0FyREc7QUFzREpDLG9CQXRESSx3QkFzRFNyRCxLQXREVCxFQXNEZTtBQUNoQixnQkFBR0EsU0FBUyxTQUFaLEVBQXNCO0FBQ2pCLHFCQUFLYyxZQUFMLEdBQW9CLElBQXBCO0FBQ0EscUJBQUtDLFVBQUwsR0FBa0IsS0FBbEI7QUFDSixhQUhELE1BR00sSUFBR2YsU0FBUyxVQUFaLEVBQXVCO0FBQ3hCLHFCQUFLZSxVQUFMLEdBQWtCLElBQWxCO0FBQ0EscUJBQUtELFlBQUwsR0FBb0IsS0FBcEI7QUFDSjtBQUNILFNBOURHO0FBK0RKd0MsNEJBL0RJLGtDQStEbUI7QUFBQTs7QUFDbkIsZ0JBQUlDLFNBQVMsRUFBYjs7QUFFQSxnQkFBSSxDQUFFLEtBQUtwQyxXQUFMLENBQWlCQyxhQUFuQixJQUFvQyxDQUFFLEtBQUtELFdBQUwsQ0FBaUJFLE9BQXZELElBQWtFLENBQUUsS0FBS0YsV0FBTCxDQUFpQkcsZUFBekYsRUFBMEc7QUFDckcsaUJBQUUsS0FBS0gsV0FBTCxDQUFpQkUsT0FBcEIsR0FBK0JrQyxPQUFPLFNBQVAsSUFBb0IsSUFBbkQsR0FBMEQsS0FBSyxDQUEvRDtBQUNDLGlCQUFFLEtBQUtwQyxXQUFMLENBQWlCQyxhQUFwQixHQUFxQ21DLE9BQU8sZUFBUCxJQUEwQixJQUEvRCxHQUFzRSxLQUFLLENBQTNFO0FBQ0MsaUJBQUUsS0FBS3BDLFdBQUwsQ0FBaUJHLGVBQXBCLEdBQXVDaUMsT0FBTyxpQkFBUCxJQUE0QixJQUFuRSxHQUEwRSxLQUFLLENBQS9FOztBQUVBLHFCQUFLcEMsV0FBTCxDQUFpQm9DLE1BQWpCLENBQXdCQyxNQUF4QixDQUErQkQsTUFBL0I7QUFDQSxxQkFBS0UsZ0JBQUw7O0FBRUE7QUFDSDs7QUFFRHZCLGtCQUFNd0IsSUFBTixDQUFXLHNDQUFYLEVBQW1EO0FBQy9DL0Msb0JBQUksS0FBS1EsV0FBTCxDQUFpQlIsRUFEMEI7QUFFL0NnRCx3QkFBUSxLQUFLeEMsV0FBTCxDQUFpQndDLE1BRnNCO0FBRy9DdkMsK0JBQWUsS0FBS0QsV0FBTCxDQUFpQkMsYUFIZTtBQUkvQ0MseUJBQVMsS0FBS0YsV0FBTCxDQUFpQkUsT0FKcUI7QUFLL0NDLGlDQUFpQixLQUFLSCxXQUFMLENBQWlCRztBQUxhLGFBQW5ELEVBTUdjLElBTkgsQ0FNUSxrQkFBVTtBQUNkVyx1QkFBT0MsT0FBUCxDQUFlLE9BQUt0QixJQUFMLENBQVU1QyxJQUFWLENBQWUsV0FBZixDQUFmOztBQUVBLHVCQUFLa0MsYUFBTCxDQUFtQjRDLGVBQW5CLEdBQXFDLE9BQUs1QyxhQUFMLENBQW1CMkMsTUFBeEQ7QUFDQSx1QkFBSzNDLGFBQUwsQ0FBbUJJLGFBQW5CLEdBQW1DLE9BQUtELFdBQUwsQ0FBaUJDLGFBQXBEO0FBQ0EsdUJBQUtKLGFBQUwsQ0FBbUJNLGVBQW5CLEdBQXFDLE9BQUtILFdBQUwsQ0FBaUJHLGVBQXREO0FBQ0EsdUJBQUtILFdBQUwsQ0FBaUIwQyxTQUFqQixHQUE2QixJQUE3Qjs7QUFFQSx1QkFBS0osZ0JBQUw7O0FBRUEsdUJBQUtOLFNBQUw7O0FBRUE1RSxrQkFBRSxpQkFBRixFQUFxQmdFLEtBQXJCLENBQTJCLE1BQTNCO0FBQ0gsYUFuQkQsRUFvQkNVLEtBcEJELENBb0JPLGlCQUFTO0FBQ1pGLHVCQUFPRyxLQUFQLENBQWEsT0FBS3hCLElBQUwsQ0FBVTVDLElBQVYsQ0FBZSxtQkFBZixDQUFiOztBQUVBLHVCQUFLMkUsZ0JBQUw7QUFDSCxhQXhCRDtBQXlCSCxTQXRHRztBQXdHSkssaUJBeEdJLHVCQXdHUTtBQUFBOztBQUNSLGdCQUFJQyxnQkFBZ0IsS0FBSzlDLE1BQUwsR0FBYyxLQUFLQyxHQUF2QztBQUNBLGdCQUFHNkMsaUJBQWlCLENBQXBCLEVBQXNCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE3QixzQkFBTXdCLElBQU4sQ0FBVyxzQ0FBWCxFQUFtRDtBQUMvQy9DLHdCQUFJLEtBQUtRLFdBQUwsQ0FBaUJSLEVBRDBCO0FBRS9DZ0QsNEJBQVEsS0FBS3hDLFdBQUwsQ0FBaUJ3QyxNQUZzQjtBQUcvQ3ZDLG1DQUFlLFNBSGdDO0FBSS9DQyw2QkFBUyxLQUpzQztBQUsvQ0MscUNBQWlCO0FBTDhCLGlCQUFuRCxFQU1HYyxJQU5ILENBTVEsa0JBQVU7O0FBRWRXLDJCQUFPQyxPQUFQLENBQWUsT0FBS3RCLElBQUwsQ0FBVTVDLElBQVYsQ0FBZSxXQUFmLENBQWY7O0FBRUEsMkJBQUtrQyxhQUFMLENBQW1CNEMsZUFBbkIsR0FBcUMsT0FBSzVDLGFBQUwsQ0FBbUIyQyxNQUF4RDtBQUNBLDJCQUFLM0MsYUFBTCxDQUFtQkksYUFBbkIsR0FBbUMsT0FBS0QsV0FBTCxDQUFpQkMsYUFBcEQ7QUFDQSwyQkFBS0osYUFBTCxDQUFtQk0sZUFBbkIsR0FBcUNlLE9BQU9mLGVBQTVDO0FBQ0EsMkJBQUtILFdBQUwsQ0FBaUIwQyxTQUFqQixHQUE2QixJQUE3Qjs7QUFFQSwyQkFBS0osZ0JBQUw7O0FBRUEsMkJBQUtOLFNBQUw7O0FBRUE1RSxzQkFBRSxrQkFBRixFQUFzQmdFLEtBQXRCLENBQTRCLE1BQTVCO0FBQ0gsaUJBcEJELEVBcUJDVSxLQXJCRCxDQXFCTyxpQkFBUztBQUNaRiwyQkFBT0csS0FBUCxDQUFhLE9BQUt4QixJQUFMLENBQVU1QyxJQUFWLENBQWUsbUJBQWYsQ0FBYjs7QUFFQSwyQkFBSzJFLGdCQUFMO0FBQ0gsaUJBekJEO0FBMEJILGFBckZELE1Bc0ZJO0FBQ0FsRixrQkFBRSxrQkFBRixFQUFzQmdFLEtBQXRCLENBQTRCLE1BQTVCO0FBQ0F5QixxQkFDRSxTQURGLEVBRUUsaUZBQStFLHdEQUZqRixFQUdFLFNBSEY7QUFNSDtBQUNKLFNBek1HO0FBMk1KQyxpQkEzTUkscUJBMk1NbkYsSUEzTU4sRUEyTVk7QUFDWixpQkFBS2tDLGFBQUwsR0FBcUJsQyxJQUFyQjtBQUNBLGdCQUFJb0MsTUFBTWhCLFdBQVdwQixLQUFLb0YsWUFBaEIsSUFBZ0NoRSxXQUFXcEIsS0FBS3FGLEdBQWhCLENBQWhDLEdBQXVEakUsV0FBV3BCLEtBQUtzRixNQUFoQixDQUFqRTtBQUNBLGlCQUFLbEQsR0FBTCxHQUFXQSxJQUFJZixPQUFKLENBQVksQ0FBWixDQUFYO0FBQ0EsaUJBQUtnQixXQUFMLEdBQW1CLElBQUlULElBQUosQ0FBUzVCLElBQVQsQ0FBbkI7O0FBRUFQLGNBQUUsa0JBQUYsRUFBc0JnRSxLQUF0QixDQUE0QixNQUE1QjtBQUNILFNBbE5HO0FBb05KOEIsaUJBcE5JLHFCQW9OTXZGLElBcE5OLEVBb05ZO0FBQ1osaUJBQUtrQyxhQUFMLEdBQXFCbEMsSUFBckI7QUFDQSxpQkFBS3lDLFVBQUwsR0FBa0IsSUFBSWIsSUFBSixDQUFTNUIsSUFBVCxDQUFsQjs7QUFFQVAsY0FBRSxjQUFGLEVBQWtCZ0UsS0FBbEIsQ0FBd0IsTUFBeEI7QUFDSCxTQXpORztBQTJOSitCLG9CQTNOSSwwQkEyTlc7QUFBQTs7QUFDWCxnQkFBSWYsU0FBUyxFQUFiOztBQUVBLGdCQUFJLENBQUUsS0FBS2hDLFVBQUwsQ0FBZ0JDLE1BQXRCLEVBQThCO0FBQ3pCLGlCQUFFLEtBQUtELFVBQUwsQ0FBZ0JDLE1BQW5CLEdBQTZCK0IsT0FBTyxRQUFQLElBQW1CLElBQWhELEdBQXVELEtBQUssQ0FBNUQ7O0FBRUEscUJBQUtoQyxVQUFMLENBQWdCZ0MsTUFBaEIsQ0FBdUJDLE1BQXZCLENBQThCRCxNQUE5QjtBQUNBLHFCQUFLZ0IsOEJBQUw7O0FBRUE7QUFDSDs7QUFFRHJDLGtCQUFNd0IsSUFBTixDQUFXLGlDQUFYLEVBQThDO0FBQzFDL0Msb0JBQUksS0FBS1ksVUFBTCxDQUFnQlosRUFEc0I7QUFFMUNnRCx3QkFBUSxLQUFLcEMsVUFBTCxDQUFnQm9DLE1BRmtCO0FBRzFDbkMsd0JBQVEsS0FBS0QsVUFBTCxDQUFnQkM7QUFIa0IsYUFBOUMsRUFJR1ksSUFKSCxDQUlRLGtCQUFVO0FBQ2RXLHVCQUFPQyxPQUFQLENBQWUsT0FBS3RCLElBQUwsQ0FBVTVDLElBQVYsQ0FBZSxXQUFmLENBQWY7O0FBRUEsdUJBQUtrQyxhQUFMLENBQW1CNEMsZUFBbkIsR0FBcUMsT0FBSzVDLGFBQUwsQ0FBbUIyQyxNQUF4RDtBQUNBLHVCQUFLM0MsYUFBTCxDQUFtQlEsTUFBbkIsR0FBNEIsT0FBS0QsVUFBTCxDQUFnQkMsTUFBNUM7QUFDQSx1QkFBS0QsVUFBTCxDQUFnQnNDLFNBQWhCLEdBQTRCLElBQTVCOztBQUVBLHVCQUFLVSw4QkFBTDs7QUFFQSx1QkFBS3BCLFNBQUw7O0FBRUE1RSxrQkFBRSxjQUFGLEVBQWtCZ0UsS0FBbEIsQ0FBd0IsTUFBeEI7QUFDSCxhQWhCRCxFQWlCQ1UsS0FqQkQsQ0FpQk8saUJBQVM7QUFDWkYsdUJBQU9HLEtBQVAsQ0FBYSxPQUFLeEIsSUFBTCxDQUFVNUMsSUFBVixDQUFlLG1CQUFmLENBQWI7O0FBRUEsdUJBQUt5Riw4QkFBTDtBQUNILGFBckJEO0FBc0JILFNBN1BHO0FBK1BKZCx3QkEvUEksOEJBK1BlO0FBQ2ZlLHVCQUFXLFlBQU07QUFDYixvQkFBSUMsT0FBT2xHLEVBQUUscUJBQUYsQ0FBWDtBQUNBLG9CQUFJbUcsV0FBV0QsS0FBS0UsSUFBTCxDQUFVLGNBQVYsQ0FBZjtBQUNBLG9CQUFJQyxVQUFVSCxLQUFLRSxJQUFMLENBQVUsU0FBVixDQUFkOztBQUVBQyx3QkFBUUMsT0FBUixDQUFnQixHQUFoQixFQUFxQixZQUFZO0FBQzdCSCw2QkFBU0ksTUFBVCxDQUFnQixHQUFoQjtBQUNILGlCQUZEO0FBR0gsYUFSRCxFQVFHLEdBUkg7QUFTSCxTQXpRRztBQTJRSlAsc0NBM1FJLDRDQTJRNkI7QUFDN0JDLHVCQUFXLFlBQU07QUFDYixvQkFBSUMsT0FBT2xHLEVBQUUseUJBQUYsQ0FBWDtBQUNBLG9CQUFJbUcsV0FBV0QsS0FBS0UsSUFBTCxDQUFVLGNBQVYsQ0FBZjtBQUNBLG9CQUFJQyxVQUFVSCxLQUFLRSxJQUFMLENBQVUsU0FBVixDQUFkOztBQUVBQyx3QkFBUUMsT0FBUixDQUFnQixHQUFoQixFQUFxQixZQUFZO0FBQzdCSCw2QkFBU0ksTUFBVCxDQUFnQixHQUFoQjtBQUNILGlCQUZEO0FBR0gsYUFSRCxFQVFHLEdBUkg7QUFTSCxTQXJSRztBQXVSSEMsYUF2UkcsbUJBdVJJO0FBQ0osZ0JBQUlDLGlCQUFpQixFQUFyQjtBQUNBQSw2QkFBaUIsdUtBQWpCO0FBQ0FBLDhCQUFrQixvR0FBbEI7QUFDQUEsOEJBQWtCLG1OQUFsQjtBQUNBQSw4QkFBa0IscXREQUFsQjtBQUNBQSw4QkFBa0Isb0dBQWxCOztBQUVBLGdCQUFJQyxPQUFPMUcsRUFBRSxRQUFGLEVBQVkyRyxJQUFaLEVBQVg7QUFDQSxnQkFBSUMsV0FBU3RELE9BQU91RCxJQUFQLENBQVksRUFBWixFQUFlLEVBQWYsRUFBa0IsaURBQWxCLENBQWI7QUFDQUQscUJBQVMzRyxRQUFULENBQWtCNkcsS0FBbEIsQ0FBd0JMLGNBQXhCO0FBQ0FHLHFCQUFTM0csUUFBVCxDQUFrQjZHLEtBQWxCLENBQXdCSixJQUF4QjtBQUNBRSxxQkFBUzNHLFFBQVQsQ0FBa0I2RyxLQUFsQixDQUF3QixvQkFBeEI7QUFDQUYscUJBQVMzRyxRQUFULENBQWtCNkcsS0FBbEIsQ0FBd0IsdUZBQXhCO0FBQ0FGLHFCQUFTM0csUUFBVCxDQUFrQjZHLEtBQWxCLENBQXdCSixJQUF4QjtBQUNBRSxxQkFBUzNHLFFBQVQsQ0FBa0I2RyxLQUFsQixDQUF3QixrQ0FBeEI7QUFDQUYscUJBQVMzRyxRQUFULENBQWtCOEcsS0FBbEI7O0FBRUFkLHVCQUFXLFlBQVU7QUFDckJXLHlCQUFTSSxLQUFUO0FBQ0FKLHlCQUFTSixLQUFUO0FBQ0MsYUFIRCxFQUdHLElBSEg7QUFJSDtBQTdTRyxLQWpDSzs7QUFpVmJTLGNBQVM7QUFDTEMsdUJBREssNkJBQ1k7QUFDYixnQkFBSUMsc0JBQXNCLEtBQUtuRixPQUFMLENBQWErQixRQUF2QztBQUNBLGdCQUFJcUQsbUJBQW1CRCxzQkFBc0IsR0FBN0M7QUFDQSxnQkFBSUUsaUJBQWlCLEtBQUtyRixPQUFMLENBQWFzRixjQUFsQztBQUNBLGdCQUFJQyxrQkFBa0JILG1CQUFtQkMsY0FBekM7QUFDQSxnQkFBSUcsbUJBQW1CSCxpQkFBaUJFLGVBQXhDO0FBQ0FDLCtCQUFtQkEsaUJBQWlCNUYsT0FBakIsQ0FBeUIsQ0FBekIsQ0FBbkI7QUFDQSxtQkFBUTRGLGdCQUFSO0FBQ0gsU0FUSTtBQVdMRixzQkFYSyw0QkFXVztBQUNaLGdCQUFHLEtBQUt0RixPQUFMLENBQWF5RixZQUFiLElBQTJCLENBQTlCLEVBQ0ksT0FBTzlGLFdBQVcsS0FBS0ssT0FBTCxDQUFhc0YsY0FBeEIsSUFBMEMzRixXQUFXLEtBQUtLLE9BQUwsQ0FBYTJELFlBQXhCLENBQTFDLEdBQWtGaEUsV0FBVyxLQUFLSyxPQUFMLENBQWE2RCxNQUF4QixDQUFsRixHQUFvSGxFLFdBQVcsS0FBS0ssT0FBTCxDQUFhNEQsR0FBeEIsQ0FBM0gsQ0FESixLQUdJLE9BQU8sS0FBSzVELE9BQUwsQ0FBYXNGLGNBQXBCO0FBQ1AsU0FoQkk7QUFrQkxJLHdCQWxCSyw4QkFrQmE7QUFDZCxnQkFBSUMsU0FBUSxDQUFaO0FBQ0EsZ0JBQUcsS0FBSzNGLE9BQUwsQ0FBYStCLFFBQWIsSUFBdUIsSUFBMUIsRUFDSSxJQUFJNkQsVUFBVSxLQUFLNUYsT0FBTCxDQUFhc0YsY0FBYixHQUE4QixLQUFLdEYsT0FBTCxDQUFhNkYsVUFBekQ7QUFDSixnQkFBRyxLQUFLN0YsT0FBTCxDQUFhK0IsUUFBYixJQUF1QixJQUF2QixJQUErQixLQUFLL0IsT0FBTCxDQUFhK0IsUUFBYixJQUF1QixDQUF6RCxFQUNJLElBQUk2RCxVQUFVLEtBQUs1RixPQUFMLENBQWE2RixVQUEzQixDQURKLEtBR0ksSUFBSUQsVUFBVSxDQUFkO0FBQ0pBLHNCQUFVRCxPQUFPL0YsT0FBUCxDQUFlLENBQWYsQ0FBVjtBQUNBO0FBQ0EsbUJBQU9nRyxPQUFQO0FBQ0gsU0E3Qkk7QUErQkxFLDBCQS9CSyxnQ0ErQmU7QUFDaEIsZ0JBQUcsS0FBSzlGLE9BQUwsQ0FBYStCLFFBQWIsSUFBdUIsSUFBMUIsRUFBK0I7QUFDM0Isb0JBQUcsS0FBSy9CLE9BQUwsQ0FBYXlGLFlBQWIsSUFBMkIsQ0FBOUIsRUFBZ0M7QUFDM0Isd0JBQUlHLFVBQVUsTUFBSyxDQUFDakcsV0FBVyxLQUFLSyxPQUFMLENBQWFzRixjQUF4QixJQUEwQzNGLFdBQVcsS0FBS0ssT0FBTCxDQUFhMkQsWUFBeEIsQ0FBMUMsR0FBa0ZoRSxXQUFXLEtBQUtLLE9BQUwsQ0FBYTZELE1BQXhCLENBQWxGLEdBQW9IbEUsV0FBVyxLQUFLSyxPQUFMLENBQWE0RCxHQUF4QixDQUFwSCxHQUFtSmpFLFdBQVcsS0FBS0ssT0FBTCxDQUFhK0IsUUFBeEIsQ0FBcEosS0FBd0xwQyxXQUFXLEtBQUtLLE9BQUwsQ0FBYXNGLGNBQXhCLElBQTBDM0YsV0FBVyxLQUFLSyxPQUFMLENBQWEyRCxZQUF4QixDQUExQyxHQUFrRmhFLFdBQVcsS0FBS0ssT0FBTCxDQUFhNkQsTUFBeEIsQ0FBbEYsR0FBb0hsRSxXQUFXLEtBQUtLLE9BQUwsQ0FBYTRELEdBQXhCLENBQTVTLENBQUQsR0FBNFUsR0FBOVY7QUFDRCwyQkFBT2dDLFFBQVFoRyxPQUFSLENBQWdCLENBQWhCLENBQVA7QUFDSCxpQkFIRCxNQUlJLE9BQU8sS0FBS0ksT0FBTCxDQUFhK0IsUUFBcEI7QUFDUCxhQU5ELE1BT0ksT0FBTyxDQUFQO0FBQ1AsU0F4Q0k7QUEwQ0xnRSxXQTFDSyxpQkEwQ0E7QUFDRCxtQkFBTyxLQUFLVCxjQUFMLEdBQW9CLEtBQUtJLGdCQUFoQztBQUNILFNBNUNJO0FBOENMTSxtQkE5Q0sseUJBOENRO0FBQ1QsZ0JBQUlDLFFBQVF0RyxXQUFXLEtBQUtLLE9BQUwsQ0FBYWtHLFFBQXhCLElBQW9DdkcsV0FBVyxLQUFLSyxPQUFMLENBQWEyRCxZQUF4QixDQUFoRDtBQUNBc0Msb0JBQVFBLE1BQU1yRyxPQUFOLENBQWMsQ0FBZCxDQUFSO0FBQ0EsbUJBQU9xRyxLQUFQO0FBQ0gsU0FsREk7QUFvRExFLGdCQXBESyxzQkFvREs7QUFDTixnQkFBRyxLQUFLbkcsT0FBTCxDQUFheUYsWUFBYixJQUEyQixDQUE5QixFQUFnQztBQUM1QixvQkFBSVEsUUFBUXRHLFdBQVcsS0FBS0ssT0FBTCxDQUFhMkQsWUFBeEIsSUFBd0NoRSxXQUFXLEtBQUtLLE9BQUwsQ0FBYTZELE1BQXhCLENBQXhDLEdBQTBFbEUsV0FBVyxLQUFLSyxPQUFMLENBQWE0RCxHQUF4QixDQUF0RjtBQUNBcUMsd0JBQVFBLE1BQU1yRyxPQUFOLENBQWMsQ0FBZCxDQUFSO0FBQ0gsYUFIRCxNQUlJLElBQUlxRyxRQUFRLElBQVo7O0FBRUosbUJBQU9BLEtBQVA7QUFDSCxTQTVESTtBQThETEcsY0E5REssb0JBOERHO0FBQ0osZ0JBQUlDLEtBQU0xRyxXQUFXLEtBQUtLLE9BQUwsQ0FBYXNHLE1BQXhCLElBQWtDM0csV0FBVyxLQUFLSyxPQUFMLENBQWF1RyxLQUF4QixDQUFsQyxHQUFtRTVHLFdBQVcsS0FBS0ssT0FBTCxDQUFhd0csTUFBeEIsQ0FBcEUsR0FBcUcsSUFBOUc7QUFDQSxnQkFBRyxLQUFLeEcsT0FBTCxDQUFhb0csTUFBYixHQUFvQkMsRUFBdkIsRUFDSSxPQUFRLEtBQUtyRyxPQUFMLENBQWFvRyxNQUFyQjtBQUNKLGdCQUFHQyxLQUFHLEtBQUtyRyxPQUFMLENBQWFvRyxNQUFuQixFQUNJLE9BQU9DLEVBQVA7QUFDUDtBQXBFSSxLQWpWSTs7QUF3WmJJLFdBeFphLHFCQXdaSDtBQUFBOztBQUNOLGFBQUs3RCxTQUFMO0FBQ0EsYUFBS1gsdUJBQUw7QUFDQXlFLGNBQU1DLE1BQU4sQ0FBYSxrQkFBYixFQUFpQztBQUFBLG1CQUFRLE9BQUtqRCxTQUFMLENBQWVuRixJQUFmLENBQVI7QUFBQSxTQUFqQztBQUNBbUksY0FBTUMsTUFBTixDQUFhLGlCQUFiLEVBQWdDO0FBQUEsbUJBQVEsT0FBSzdDLFNBQUwsQ0FBZXZGLElBQWYsQ0FBUjtBQUFBLFNBQWhDOztBQUVBb0QsY0FBTUMsR0FBTixDQUFVLHlCQUFWLEVBQ0tDLElBREwsQ0FDVSxrQkFBVTtBQUNoQixtQkFBS1YsSUFBTCxHQUFZVyxPQUFPdkQsSUFBbkI7QUFDSCxTQUhEO0FBSUgsS0FsYVk7QUFvYWJxSSxXQXBhYSxxQkFvYUg7QUFBQTs7QUFDTjVJLFVBQUUsa0JBQUYsRUFBc0I2SSxFQUF0QixDQUF5QixpQkFBekIsRUFBNEMsYUFBSztBQUM3QyxnQkFBSSxDQUFFLFFBQUtqRyxXQUFMLENBQWlCMEMsU0FBdkIsRUFBa0M7QUFDOUIsd0JBQUs3QyxhQUFMLENBQW1CMkMsTUFBbkIsR0FBNEIsUUFBSzNDLGFBQUwsQ0FBbUI0QyxlQUEvQztBQUNIO0FBQ0osU0FKRDs7QUFNQXJGLFVBQUUsY0FBRixFQUFrQjZJLEVBQWxCLENBQXFCLGlCQUFyQixFQUF3QyxhQUFLO0FBQ3pDLGdCQUFJLENBQUUsUUFBSzdGLFVBQUwsQ0FBZ0JzQyxTQUF0QixFQUFpQztBQUM3Qix3QkFBSzdDLGFBQUwsQ0FBbUIyQyxNQUFuQixHQUE0QixRQUFLM0MsYUFBTCxDQUFtQjRDLGVBQS9DO0FBQ0g7QUFDSixTQUpEO0FBS0g7QUFoYlksQ0FBUixDQUFULEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNkQTtTQUVBO3VCQUNBO1NBR0E7QUFDQTs7OzRDQUVBO2tDQUNBLDhJQUVBLDRCQUNBO0FBQ0E7Z0RBQ0E7NkRBQ0EsNEJBRUEsOEJBQ0E7QUFDQTtVQUNBO0FBQ0E7c0JBQ0E7cUNBQ0E7QUFDQTswQkFDQTsyQ0FDQTtBQUNBO3NDQUNBOzBDQUNBO1dBQ0E7QUFDQTs0QkFDQTtBQUVBO0FBM0JBOztxQ0E4QkE7QUFGQTs7OENBSUE7dUNBQ0E7QUFDQTs7QUFDQTs7OzBDQUVBO3lDQUNBO1VBQ0E7c0JBQ0E7d0JBQ0E7dUJBQ0E7c0RBQ0E7QUFQQSx1QkFRQTs7QUFFQTtTQUVBO0FBSEEsOEJBSUE7b0RBQ0E7MkJBQ0E7QUFDQSw4QkFDQTtrREFDQTtBQUNBOzs0Q0FDQTs7QUFFQTt3Q0FDQTtvQ0FDQTtBQUVBOztBQUNBOzt1QkFFQTs7Z0NBQ0E7d0NBQ0E7c0NBQ0E7dUNBQ0E7b0NBQ0E7OzJDQUVBOzBDQUNBO1dBQ0E7dUJBQ0E7eUJBQ0E7d0JBQ0E7dURBQ0E7QUFQQSx3QkFRQTs0RUFDQSwrQkFDQTtzREFDQTs2QkFDQTtBQUNBLCtCQUNBO29EQUNBO0FBQ0E7OytDQUNBOztBQUNBO0FBRUE7d0NBQ0E7MkNBQ0E7d0NBQ0E7aURBQ0E7dUNBQ0E7QUFDQTtBQUdBO0FBckVBOzs2QkFzRUE7MENBQ0E7QUFFQTt1Q0FDQTttQ0FDQTsyQ0FDQTtBQUNBO0FBQ0E7QUFwSEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzhIQTtBQUNBO2tCQUVBOztZQUdBO0FBRkE7O2tEQUlBOzhCQUNBO3VDQUNBO29DQUNBOzBDQUNBO3dDQUNBO3dDQUNBO3VDQUNBO21DQUNBO3NDQUNBO3lDQUNBO3VDQUNBO3VDQUNBO3NDQUNBO3lDQUNBO21DQUNBO3VDQUVBO21DQUNBOzBDQUNBO29DQUNBOzBDQUNBO3FDQUNBO3dDQUNBO29DQUNBO21DQUNBO3lDQUNBO3lDQUNBO3VDQUNBO29DQUNBO3lDQUNBO3lDQUNBO21DQUNBO3VDQUVBO3FDQUNBOzBDQUNBO29DQUNBO3VDQUNBO3dDQUNBO3dDQUNBO3VDQUNBO21DQUNBO3lDQUNBO3NDQUNBO3VDQUNBO3VDQUNBO3lDQUNBO3NDQUNBO21DQUNBO3VDQUVBOytCQUNBOzBDQUNBO2lDQUNBOzBDQUNBO3dDQUNBO3dDQUNBO3VDQUNBO2dDQUNBO3lDQUNBO3lDQUNBO3VDQUNBO3VDQUNBO3lDQUNBO3lDQUNBO2dDQUNBO3VDQUVBO21DQUNBOzBDQUNBO29DQUNBOzBDQUNBO3dDQUNBO3FDQUNBO3VDQUNBO21DQUNBO3lDQUNBO3lDQUNBO29DQUNBO3VDQUNBO3lDQUNBO3lDQUNBO21DQUNBO29DQUVBO0FBQ0E7QUFFQTtBQXpGQTtBQUxBOztBQWlHQSw4QkFDQTttREFDQTs2Q0FDQTtvQ0FDQTs2Q0FDQTsyQ0FDQTswQ0FDQTswQ0FDQTttQ0FDQTs0Q0FDQTs0Q0FDQTswQ0FDQTs0Q0FDQTs0Q0FDQTttQ0FDQTswQ0FDQTtBQUNBOzs7Ozs7OztBQ3ZTQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQWtIO0FBQ2xIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQWtIO0FBQ2xIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxZQUFZO0FBQ1osS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDblJBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEMiLCJmaWxlIjoianMvcGFnZXMvc2FsZXMtcmVwb3J0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0NTApO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGY1ZmNmZjBlMWYwODAwOTdjZjQ2IiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUgMjYgMjciLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xyXG4gICAgY3R4ID0gJCgnI3RhYmxlX2NoYXJ0Jyk7XHJcbiAgICB2YXIgbXlDaGFydCA9IG5ldyBDaGFydChjdHgsIHtcclxuICAgICAgICB0eXBlOiAnbGluZScsXHJcbiAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICBsYWJlbHM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJKYW51YXJ5XCIsIFwiRmVicnVhcnlcIiwgXCJNYXJjaFwiLCBcIkFwcmlsXCIsIFwiTWF5XCIsIFwiSnVuZVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIkp1bHlcIiwgXCJBdWd1c3RcIiwgXCJTZXB0ZW1iZXJcIiwgXCJPY3RvYmVyXCIsIFwiTm92ZW1iZXJcIiwgXCJEZWNlbWJlclwiXHJcbiAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgZGF0YXNldHM6IFt7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0FsbCBTdG9yZXMnLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogWzEyLCAxOSwgMywgNSwgMiwgMywgMjQsIDIzLCAxMiwgNiwgMzQsIDEwXSxcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogW1xyXG4gICAgICAgICAgICAgICAgICAgICdyZ2JhKDI1NSwgOTksIDEzMiwgMC4yKScsXHJcbiAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgYm9yZGVyQ29sb3I6IFtcclxuICAgICAgICAgICAgICAgICAgICAncmdiYSgyNTUsOTksMTMyLDEpJyxcclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICBib3JkZXJXaWR0aDogM1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N0b3JlIDEnLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogWzEsIDksIDEzLCAyNSwgMzIsIDIxLCA0LCAyLCAyLCAxNiwgNCwgMTBdLFxyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgJ3JnYmEoNzUsIDE5MiwgMTkyLCAwLjIpJyxcclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICBib3JkZXJDb2xvcjogW1xyXG4gICAgICAgICAgICAgICAgICAgICdyZ2JhKDc1LCAxOTIsIDE5MiwgMSknLFxyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgIGJvcmRlcldpZHRoOiAxXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3RvcmUgMicsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiBbMCwgMywgMSwgNSwgMiwgMzMsIDQxLCAxMCwgMiwgMTYsIDEwLCAxMl0sXHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IFtcclxuICAgICAgICAgICAgICAgICAgICAncmdiYSg3NSwgMCwgMTkyLCAwLjIpJyxcclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICBib3JkZXJDb2xvcjogW1xyXG4gICAgICAgICAgICAgICAgICAgICdyZ2JhKDc1LCAwLCAxOTIsIDEpJyxcclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICBib3JkZXJXaWR0aDogMVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9LFxyXG4gICAgICAgIG9wdGlvbnM6IHtcclxuICAgICAgICAgICAgc2NhbGVzOiB7XHJcbiAgICAgICAgICAgICAgICB5QXhlczogW3tcclxuICAgICAgICAgICAgICAgICAgICB0aWNrczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiZWdpbkF0WmVybzp0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgcmVzcG9uc2l2ZTp0cnVlLFxyXG4gICAgICAgICAgICBtYWludGFpbkFzcGVjdFJhdGlvOiB0cnVlXHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbn0pXHJcblxyXG5cclxuVnVlLmNvbXBvbmVudCgnc3RvcmUtcGFuZWwnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9yZXBvcnRzL3NhbGVzL3N0b3JlUGFuZWwudnVlJykpO1xyXG5cclxuXHJcblZ1ZS5maWx0ZXIoJ2N1cnJlbmN5JywgZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICByZXR1cm4gbnVtYmVyV2l0aENvbW1hcyhwYXJzZUZsb2F0KHZhbHVlKS50b0ZpeGVkKDIpKTtcclxufSk7XHJcblxyXG52YXIgdm0gPSBuZXcgVnVlKHtcclxuICAgIGVsOicjc2FsZXNfcmVwb3J0X2NvbnRhaW5lcicsXHJcbiAgICBkYXRhOntcclxuICAgICAgICBsaXN0U3RvcmU6W10sXHJcbiAgICAgICAgZGV0YWlsczpbXSxcclxuICAgICAgICBzZXJ2aWNlVHlwZTpbXHJcbiAgICAgICAgICAgICAnRHJvcCBPZmYnXHJcbiAgICAgICAgICAgICwnUGljayBVcCdcclxuICAgICAgICBdLFxyXG4gICAgICAgIHVwZGF0ZVJlbWFya3M6IG5ldyBGb3JtKHtcclxuICAgICAgICAgICAgIGlkICAgICA6IFwiXCJcclxuICAgICAgICAgICAgLHNlbGxlcl9ub3RlczogXCJcIlxyXG4gICAgICAgIH0seyBiYXNlVVJMOiAnL3VzZXIvc2FsZXMtcmVwb3J0J30pLFxyXG4gICAgICAgIG9wdGlvblBpY2tVcDogZmFsc2UsXHJcbiAgICAgICAgb3B0aW9uRHJvcDogZmFsc2UsXHJcbiAgICAgICAgc2VsZWN0ZWRPcmRlcjogbnVsbCxcclxuICAgICAgICB1c2FibGU6IG51bGwsXHJcbiAgICAgICAgZmVlOiBudWxsLFxyXG4gICAgICAgIHRyYW5zaXRGb3JtOiBuZXcgRm9ybSh7XHJcbiAgICAgICAgICAgIHNoaXBtZW50X3R5cGU6IG51bGwsXHJcbiAgICAgICAgICAgIGNvdXJpZXI6IG51bGwsXHJcbiAgICAgICAgICAgIHRyYWNraW5nX251bWJlcjogbnVsbCxcclxuICAgICAgICB9KSxcclxuICAgICAgICBjYW5jZWxGb3JtOiBuZXcgRm9ybSh7XHJcbiAgICAgICAgICAgIHJlYXNvbjogbnVsbFxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGhhc0Rpc2NvdW50OiBmYWxzZSxcclxuICAgICAgICBsYW5nOltdXHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgICBNdWx0aXNlbGVjdDogd2luZG93LlZ1ZU11bHRpc2VsZWN0LmRlZmF1bHRcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOntcclxuICAgICAgICBzaG93QWlyd2F5QmlsbChpZCl7XHJcbiAgICAgICAgICAgIHRoaXMuZGV0YWlscyA9ICcnO1xyXG4gICAgICAgICAgICBheGlvcy5nZXQoJy91c2VyL3NhbGVzLXJlcG9ydC9kZXRhaWxzLycraWQpXHJcbiAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGV0YWlscyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgaWYocmVzdWx0LmRhdGEuZGlzY291bnQgIT0gMCl7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5oYXNEaXNjb3VudCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc0Rpc2NvdW50ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAkKCcjdmlld0FpcndheUJpbGwnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXRVc2FibGVFd2FsbGV0QmFsYW5jZSgpe1xyXG4gICAgICAgICAgICBpZihMYXJhdmVsLnVzZXIpe1xyXG4gICAgICAgICAgICAgICAgYXhpb3NBUEl2MVxyXG4gICAgICAgICAgICAgICAgICAgIC5nZXQoJy9nZXRVc2FibGUnKVxyXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7ICB0aGlzLnVzYWJsZSA9IHBhcnNlRmxvYXQocmVzdWx0LmRhdGEuZGF0YS51c2FibGUpfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNob3dEZXRhaWxzKGlkKXtcclxuICAgICAgICAgICAgdGhpcy5kZXRhaWxzID0gXCJcIjtcclxuICAgICAgICAgICAgYXhpb3MuZ2V0KCcvdXNlci9zYWxlcy1yZXBvcnQvZGV0YWlscy8nK2lkKVxyXG4gICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRldGFpbHMgPSByZXN1bHQuZGF0YTtcclxuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUmVtYXJrcy5zZWxsZXJfbm90ZXMgPSByZXN1bHQuZGF0YS5zZWxsZXJfbm90ZXM7XHJcbiAgICAgICAgICAgICAgICBpZihyZXN1bHQuZGF0YS5kaXNjb3VudCAhPSAwKXtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc0Rpc2NvdW50ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzRGlzY291bnQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICQoJyNvcmRlclByb2R1Y3RzJykubW9kYWwoJ3Nob3cnKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc3VibWl0Tm90ZTpmdW5jdGlvbiAoaWQpe1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVJlbWFya3MuaWQgPSBpZDtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVSZW1hcmtzLnN1Ym1pdCgncG9zdCcsICcvdXBkYXRlLXJlbWFya3MnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmcuZGF0YVsndXBkYXRlZC1ub3RlLW1zZyddKTtcclxuXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nLmRhdGFbJ3VwZGF0ZS1mYWlsZWQtbXNnJ10pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZ2V0U3RvcmVzKCl7XHJcbiAgICAgICAgICAgIGF4aW9zLmdldCgnL3VzZXIvc2FsZXMtcmVwb3J0L2dldC1zdG9yZScpXHJcbiAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubGlzdFN0b3JlID0gcmVzdWx0LmRhdGEuc3RvcmVzXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2hhbmdlT3B0aW9uKHZhbHVlKXtcclxuICAgICAgICAgICBpZih2YWx1ZSA9PSBcIlBpY2sgVXBcIil7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvblBpY2tVcCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbkRyb3AgPSBmYWxzZTtcclxuICAgICAgICAgICB9ZWxzZSBpZih2YWx1ZSA9PSBcIkRyb3AgT2ZmXCIpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vcHRpb25Ecm9wID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMub3B0aW9uUGlja1VwID0gZmFsc2U7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc3VibWl0VHJhY2tpbmdOdW1iZXIoKSB7XHJcbiAgICAgICAgICAgIGxldCBlcnJvcnMgPSB7fTtcclxuXHJcbiAgICAgICAgICAgIGlmICghIHRoaXMudHJhbnNpdEZvcm0uc2hpcG1lbnRfdHlwZSB8fCAhIHRoaXMudHJhbnNpdEZvcm0uY291cmllciB8fCAhIHRoaXMudHJhbnNpdEZvcm0udHJhY2tpbmdfbnVtYmVyKSB7XHJcbiAgICAgICAgICAgICAgICAoISB0aGlzLnRyYW5zaXRGb3JtLmNvdXJpZXIpID8gZXJyb3JzWydjb3VyaWVyJ10gPSB0cnVlIDogdm9pZCgwKTtcclxuICAgICAgICAgICAgICAgICghIHRoaXMudHJhbnNpdEZvcm0uc2hpcG1lbnRfdHlwZSkgPyBlcnJvcnNbJ3NoaXBtZW50X3R5cGUnXSA9IHRydWUgOiB2b2lkKDApO1xyXG4gICAgICAgICAgICAgICAgKCEgdGhpcy50cmFuc2l0Rm9ybS50cmFja2luZ19udW1iZXIpID8gZXJyb3JzWyd0cmFja2luZ19udW1iZXInXSA9IHRydWUgOiB2b2lkKDApO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMudHJhbnNpdEZvcm0uZXJyb3JzLnJlY29yZChlcnJvcnMpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVIYXNMb2FkaW5nKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBheGlvcy5wb3N0KCcvdXNlci9zYWxlcy1yZXBvcnQvdXBkYXRlLXRvLXRyYW5zaXQnLCB7XHJcbiAgICAgICAgICAgICAgICBpZDogdGhpcy50cmFuc2l0Rm9ybS5pZCxcclxuICAgICAgICAgICAgICAgIHN0YXR1czogdGhpcy50cmFuc2l0Rm9ybS5zdGF0dXMsXHJcbiAgICAgICAgICAgICAgICBzaGlwbWVudF90eXBlOiB0aGlzLnRyYW5zaXRGb3JtLnNoaXBtZW50X3R5cGUsXHJcbiAgICAgICAgICAgICAgICBjb3VyaWVyOiB0aGlzLnRyYW5zaXRGb3JtLmNvdXJpZXIsXHJcbiAgICAgICAgICAgICAgICB0cmFja2luZ19udW1iZXI6IHRoaXMudHJhbnNpdEZvcm0udHJhY2tpbmdfbnVtYmVyLFxyXG4gICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmcuZGF0YVsnc2F2ZWQtbXNnJ10pO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcmRlci5vcmlnaW5hbF9zdGF0dXMgPSB0aGlzLnNlbGVjdGVkT3JkZXIuc3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9yZGVyLnNoaXBtZW50X3R5cGUgPSB0aGlzLnRyYW5zaXRGb3JtLnNoaXBtZW50X3R5cGU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3JkZXIudHJhY2tpbmdfbnVtYmVyID0gdGhpcy50cmFuc2l0Rm9ybS50cmFja2luZ19udW1iZXI7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRyYW5zaXRGb3JtLnN1Ym1pdHRlZCA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVIYXNMb2FkaW5nKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRTdG9yZXMoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAkKCcjaW5UcmFuc2l0TW9kYWwnKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZy5kYXRhWyd1cGRhdGUtZmFpbGVkLW1zZyddKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlbW92ZUhhc0xvYWRpbmcoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc3VibWl0MkdvKCkge1xyXG4gICAgICAgICAgICBsZXQgZW5vdWdoQmFsYW5jZSA9IHRoaXMudXNhYmxlIC0gdGhpcy5mZWU7XHJcbiAgICAgICAgICAgIGlmKGVub3VnaEJhbGFuY2UgPj0gMCl7XHJcbiAgICAgICAgICAgICAgICAvLyB2YXIgcGFyY2VsMmdvID0ge1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIGQgOiB7fSxcclxuICAgICAgICAgICAgICAgIC8vIH07XHJcbiAgICAgICAgICAgICAgICAvLyB2YXIgZGF0YTIgPSB7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJpZF9cIjogXCJBUElVU0VSXCIsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJ0b2tlbl9cIjogXCJYczJTYWtKMU1cIixcclxuICAgICAgICAgICAgICAgIC8vICAgICBcImFjY291bnRfbm9cIjogXCJcIixcclxuICAgICAgICAgICAgICAgIC8vICAgICBcImJrZ19yZXF1ZXN0XCI6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIFwiYXdiX2NvZGVcIjogXCJBMDAwMDAwMUFcIixcclxuICAgICAgICAgICAgICAgIC8vICAgICBcIm9yaWdpbl9wb3J0XCI6IFwiTU5MXCIsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJwYXltb2RlIFwiOiBcIkNTXCIsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJjb2RfYW10XCI6IDUwMDAsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJpbnN0cnVjdGlvblwiOiBcIk5lYXIgdHJhaW4gc3RhdGlvblwiLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIFwiYXR0YWNobWVudFwiOiBcIkFCQy0xMzIxMzEyMzJcIixcclxuICAgICAgICAgICAgICAgIC8vICAgICBjbmVlX2R0bHM6IHt9LFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIHB4X2R0bHM6IFtdLFxyXG4gICAgICAgICAgICAgICAgLy8gfTsgIFxyXG5cclxuICAgICAgICAgICAgICAgIC8vIHZhciBkdGxzID0ge1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIFwibmFtZVwiOiBcIkpVQU4gREVMQSBDUlVaXCIsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJzdHJlZXRcIjogXCJMMTAgQjIwIFBVVEFUQU4gVklMTEFHRVwiLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIFwiYmFyYW5nYXlcIjogXCJQVVRBVEFOXCIsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJjaXR5XCI6IFwiTVVOVElOTFVQQSBDSVRZXCIsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJhcmVhX2NvZGVcIjogXCIxOTU4XCIsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJtb2JpbGVfbnVtYmVyXCI6IFwiMDkyODEyMzQ1NjdcIixcclxuICAgICAgICAgICAgICAgIC8vICAgICBcImVtYWlsX2FkZHJlc3NcIjogXCJqdWFuX2RlbGFjcnV6QHlhaG9vLmNvbS5waFwiXHJcbiAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgICAgICAvLyBkYXRhMi5jbmVlX2R0bHMgPSBkdGxzO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGRhdGEyLnB4X2R0bHMucHVzaCh7IFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIFwiaXRlbV9xdHlcIjogMSxcclxuICAgICAgICAgICAgICAgIC8vICAgICBcInBrZ19jb2RlXCI6IFwiQk9YXCIsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgXCJhY3R1YWxfd3RcIjogNSxcclxuICAgICAgICAgICAgICAgIC8vICAgICBcImxlbmd0aFwiOiA1LFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIFwid2lkdGhcIjogNSxcclxuICAgICAgICAgICAgICAgIC8vICAgICBcImhlaWdodFwiOiA1LFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIFwiZGVjbGFyZWRfdmFsdWVcIjogMTAwMCxcclxuICAgICAgICAgICAgICAgIC8vICAgICBcIml0ZW1fZGVzY3JpcHRpb25cIjogXCJDZWxsdWxhciBQaG9uZSBVbml0XCJcclxuICAgICAgICAgICAgICAgIC8vIH0pO1xyXG4gICAgICAgICAgICAgICAgLy8gcGFyY2VsMmdvLmQgPSBkYXRhMjtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgLy8gdmFyIGpzb250ZXN0ID0gSlNPTi5zdHJpbmdpZnkocGFyY2VsMmdvKTtcclxuICAgICAgICAgICAgICAgIC8vIC8vY29uc29sZS5sb2coanNvbnRlc3QpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGF4aW9zLnBvc3QoJ2h0dHBzOi8vZXhwcmVzc2FwcHMuMmdvLmNvbS5waC8yZ293ZWJkZXYvbW9kZWwvQVBJL3NoaXBtZW50X3JlcXVlc3QuYXNteCcsIHtcclxuICAgICAgICAgICAgICAgIC8vICAgICBkOiBwYXJjZWwyZ28sXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICAnQWNjZXNzLUNvbnRyb2wtQWxsb3ctT3JpZ2luJzogJyonLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAgICAgICAgIC8vICAgICAsXHJcbiAgICAgICAgICAgICAgICAvLyB9KS50aGVuKHJlc3VsdCA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gICAgIGNvbnNvbGUubG9nKHJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgICAvLyB9KVxyXG4gICAgICAgICAgICAgICAgLy8gLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIGF4aW9zLnBvc3QoJy91c2VyL3NhbGVzLXJlcG9ydC91cGRhdGUtdG8tdHJhbnNpdCcsIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogdGhpcy50cmFuc2l0Rm9ybS5pZCxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHRoaXMudHJhbnNpdEZvcm0uc3RhdHVzLFxyXG4gICAgICAgICAgICAgICAgICAgIHNoaXBtZW50X3R5cGU6ICdQaWNrIFVwJyxcclxuICAgICAgICAgICAgICAgICAgICBjb3VyaWVyOiAnMkdPJyxcclxuICAgICAgICAgICAgICAgICAgICB0cmFja2luZ19udW1iZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZy5kYXRhWydzYXZlZC1tc2cnXSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcmRlci5vcmlnaW5hbF9zdGF0dXMgPSB0aGlzLnNlbGVjdGVkT3JkZXIuc3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcmRlci5zaGlwbWVudF90eXBlID0gdGhpcy50cmFuc2l0Rm9ybS5zaGlwbWVudF90eXBlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcmRlci50cmFja2luZ19udW1iZXIgPSByZXN1bHQudHJhY2tpbmdfbnVtYmVyO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJhbnNpdEZvcm0uc3VibWl0dGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVIYXNMb2FkaW5nKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0U3RvcmVzKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICQoJyNpblRyYW5zaXRNb2RhbDInKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZy5kYXRhWyd1cGRhdGUtZmFpbGVkLW1zZyddKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVIYXNMb2FkaW5nKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgJCgnI2luVHJhbnNpdE1vZGFsMicpLm1vZGFsKCdoaWRlJyk7XHJcbiAgICAgICAgICAgICAgICBzd2FsKFxyXG4gICAgICAgICAgICAgICAgICAnT29wcy4uLicsXHJcbiAgICAgICAgICAgICAgICAgICdZb3VyIGUtd2FsbGV0IHVzYWJsZSBiYWxhbmNlIGlzIG5vdCBlbm91Z2ggZm9yIHRoaXMgdHJhbnNhY3Rpb24gdG8gcHJvY2VlZCEgJysnQ2hlY2sgeW91ciBlLXdhbGxldCA8YSBocmVmPVwiL3VzZXIvZXdhbGxldFwiPmhlcmUuPC9hPiAnLFxyXG4gICAgICAgICAgICAgICAgICAnd2FybmluZydcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaW5UcmFuc2l0KGRhdGEpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9yZGVyID0gZGF0YTtcclxuICAgICAgICAgICAgdmFyIGZlZSA9IHBhcnNlRmxvYXQoZGF0YS5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdChkYXRhLnZhdCkgKyBwYXJzZUZsb2F0KGRhdGEuY2hhcmdlKTtcclxuICAgICAgICAgICAgdGhpcy5mZWUgPSBmZWUudG9GaXhlZCgyKTtcclxuICAgICAgICAgICAgdGhpcy50cmFuc2l0Rm9ybSA9IG5ldyBGb3JtKGRhdGEpO1xyXG5cclxuICAgICAgICAgICAgJCgnI2luVHJhbnNpdE1vZGFsMicpLm1vZGFsKCdzaG93Jyk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY2FuY2VsbGVkKGRhdGEpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9yZGVyID0gZGF0YTtcclxuICAgICAgICAgICAgdGhpcy5jYW5jZWxGb3JtID0gbmV3IEZvcm0oZGF0YSk7XHJcblxyXG4gICAgICAgICAgICAkKCcjY2FuY2VsTW9kYWwnKS5tb2RhbCgnc2hvdycpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHN1Ym1pdFJlYXNvbigpIHtcclxuICAgICAgICAgICAgbGV0IGVycm9ycyA9IHt9O1xyXG5cclxuICAgICAgICAgICAgaWYgKCEgdGhpcy5jYW5jZWxGb3JtLnJlYXNvbikge1xyXG4gICAgICAgICAgICAgICAgKCEgdGhpcy5jYW5jZWxGb3JtLnJlYXNvbikgPyBlcnJvcnNbJ3JlYXNvbiddID0gdHJ1ZSA6IHZvaWQoMCk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5jYW5jZWxGb3JtLmVycm9ycy5yZWNvcmQoZXJyb3JzKTtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVtb3ZlSGFzTG9hZGluZ0Zyb21DYW5jZWxGb3JtKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBheGlvcy5wb3N0KCcvdXNlci9zYWxlcy1yZXBvcnQvY2FuY2VsLW9yZGVyJywge1xyXG4gICAgICAgICAgICAgICAgaWQ6IHRoaXMuY2FuY2VsRm9ybS5pZCxcclxuICAgICAgICAgICAgICAgIHN0YXR1czogdGhpcy5jYW5jZWxGb3JtLnN0YXR1cyxcclxuICAgICAgICAgICAgICAgIHJlYXNvbjogdGhpcy5jYW5jZWxGb3JtLnJlYXNvbixcclxuICAgICAgICAgICAgfSkudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nLmRhdGFbJ3NhdmVkLW1zZyddKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3JkZXIub3JpZ2luYWxfc3RhdHVzID0gdGhpcy5zZWxlY3RlZE9yZGVyLnN0YXR1cztcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcmRlci5yZWFzb24gPSB0aGlzLmNhbmNlbEZvcm0ucmVhc29uO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jYW5jZWxGb3JtLnN1Ym1pdHRlZCA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVIYXNMb2FkaW5nRnJvbUNhbmNlbEZvcm0oKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFN0b3JlcygpO1xyXG5cclxuICAgICAgICAgICAgICAgICQoJyNjYW5jZWxNb2RhbCcpLm1vZGFsKCdoaWRlJyk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nLmRhdGFbJ3VwZGF0ZS1mYWlsZWQtbXNnJ10pO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMucmVtb3ZlSGFzTG9hZGluZ0Zyb21DYW5jZWxGb3JtKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHJlbW92ZUhhc0xvYWRpbmcoKSB7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0ICRidG4gPSAkKCcuYnRuLXRyYW5zaXQtc3VibWl0Jyk7XHJcbiAgICAgICAgICAgICAgICBsZXQgJGNvbnRlbnQgPSAkYnRuLmZpbmQoJy5idG4tY29udGVudCcpO1xyXG4gICAgICAgICAgICAgICAgbGV0ICRsb2FkZXIgPSAkYnRuLmZpbmQoJy5sb2FkZXInKTtcclxuXHJcbiAgICAgICAgICAgICAgICAkbG9hZGVyLmZhZGVPdXQoMTAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJGNvbnRlbnQuZmFkZUluKDEwMCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSwgMzAwKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZW1vdmVIYXNMb2FkaW5nRnJvbUNhbmNlbEZvcm0oKSB7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0ICRidG4gPSAkKCcuYnRuLWNhbmNlbC1mb3JtLXN1Ym1pdCcpO1xyXG4gICAgICAgICAgICAgICAgbGV0ICRjb250ZW50ID0gJGJ0bi5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuICAgICAgICAgICAgICAgIGxldCAkbG9hZGVyID0gJGJ0bi5maW5kKCcubG9hZGVyJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgJGxvYWRlci5mYWRlT3V0KDEwMCwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICRjb250ZW50LmZhZGVJbigxMDApO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sIDMwMCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgIHByaW50KCl7XHJcbiAgICAgICAgICAgIHZhciBodG1sX2NvbnRhaW5lciA9ICcnO1xyXG4gICAgICAgICAgICBodG1sX2NvbnRhaW5lciA9ICc8IURPQ1RZUEUgaHRtbCBQVUJMSUMgXCItLy9XM0MvL0RURCBYSFRNTCAxLjAgVHJhbnNpdGlvbmFsLy9FTlwiIFwiaHR0cDovL3d3dy53My5vcmcvVFIveGh0bWwxL0RURC94aHRtbDEtdHJhbnNpdGlvbmFsLmR0ZFwiPjxodG1sIHhtbG5zOmZiPVwiaHR0cDovL29ncC5tZS9ucy9mYiNcIj48aGVhZD4nO1xyXG4gICAgICAgICAgICBodG1sX2NvbnRhaW5lciArPSAnPGxpbmsgcmVsPVwic3R5bGVzaGVldFwiIHR5cGU9XCJ0ZXh0L2Nzc1wiIGhyZWY9XCIvL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2ljb24/ZmFtaWx5PU1hdGVyaWFsK0ljb25zXCIgLz4nO1xyXG4gICAgICAgICAgICBodG1sX2NvbnRhaW5lciArPSAnPGxpbmsgcmVsPVwic3R5bGVzaGVldFwiIGhyZWY9XCJodHRwczovL21heGNkbi5ib290c3RyYXBjZG4uY29tL2Jvb3RzdHJhcC8zLjMuNy9jc3MvYm9vdHN0cmFwLm1pbi5jc3NcIiBpbnRlZ3JpdHk9XCJzaGEzODQtQlZZaWlTSUZlSzFkR21KUkFreWN1SEFIUmczMk9tVWN3dzdvbjNSWWRnNFZhK1BtU1Rzei9LNjh2YmRFamg0dVwiIGNyb3Nzb3JpZ2luPVwiYW5vbnltb3VzXCIvPic7XHJcbiAgICAgICAgICAgIGh0bWxfY29udGFpbmVyICs9ICc8c3R5bGU+LmFpcndheS1wcm9kdWN0LW1vZGFsIC5tb2RhbC1jb250ZW50e2JvcmRlcjpub25lO2ZvbnQtc2l6ZToxMXB4fS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keXtvdmVyZmxvdy15OmF1dG87cGFkZGluZzoyNHB4IDZweCFpbXBvcnRhbnR9LmFpcndheS1wcm9kdWN0LW1vZGFsIC5tb2RhbC1jb250ZW50IC5tb2RhbC1ib2R5IC5kZWxpdmVye2ZvbnQtZmFtaWx5OkNhbWJyaWE7Zm9udC1zaXplOjE1cHg7Zm9udC13ZWlnaHQ6NzAwfS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuYWRkcmVzc3t3aWR0aDo0MDBweH0uYWlyd2F5LXByb2R1Y3QtbW9kYWwgLm1vZGFsLWNvbnRlbnQgLm1vZGFsLWJvZHkgdGFibGUsLmFpcndheS1wcm9kdWN0LW1vZGFsIC5tb2RhbC1jb250ZW50IC5tb2RhbC1ib2R5IHRkLC5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSB0aHtib3JkZXI6MXB4IHNvbGlkICMwMDB9LmFpcndheS1wcm9kdWN0LW1vZGFsIC5tb2RhbC1jb250ZW50IC5tb2RhbC1ib2R5IHRhYmxle2JvcmRlci1jb2xsYXBzZTpjb2xsYXBzZTt3aWR0aDoxMDAlfS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSB0aHt0ZXh0LWFsaWduOmxlZnQ7Zm9udC13ZWlnaHQ6NzAwfS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuYXdie2Zsb2F0OnJpZ2h0O21hcmdpbi1yaWdodDoyMHB4O21hcmdpbi10b3A6LTEyMHB4fS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAucmVjZWl2ZWR7ZmxvYXQ6bGVmdDttYXJnaW4tbGVmdDoxNXB4O21hcmdpbi10b3A6NXB4fS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuYXR0ZW1wdHtmbG9hdDpyaWdodDttYXJnaW4tcmlnaHQ6MjBweDttYXJnaW4tdG9wOjVweH0uYWlyd2F5LXByb2R1Y3QtbW9kYWwgLm1vZGFsLWNvbnRlbnQgLm1vZGFsLWJvZHkgLnRleHQtYWxpZ257dGV4dC1hbGlnbjpjZW50ZXJ9LmFpcndheS1wcm9kdWN0LW1vZGFsIC5tb2RhbC1jb250ZW50IC5tb2RhbC1ib2R5IC5wYWRkaW5nLWxlZnR7cGFkZGluZy1sZWZ0OjEwcHh9LmFpcndheS1wcm9kdWN0LW1vZGFsIC5tb2RhbC1jb250ZW50IC5tb2RhbC1ib2R5IC5ib3JkZXItdG9we2JvcmRlci10b3A6bm9uZX0uYWlyd2F5LXByb2R1Y3QtbW9kYWwgLm1vZGFsLWNvbnRlbnQgLm1vZGFsLWJvZHkgLmJ7Zm9udC13ZWlnaHQ6NzAwfS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAubWFyZ2lue21hcmdpbi10b3A6MTVweH0uYWlyd2F5LXByb2R1Y3QtbW9kYWwgLm1vZGFsLWNvbnRlbnQgLm1vZGFsLWJvZHkgLmZsb2F0e2Zsb2F0OnJpZ2h0fS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuaW5jbHVzaXZle2ZvbnQtd2VpZ2h0OjcwMDtmb250LXNpemU6OXB4O21hcmdpbi1yaWdodDoxMHB4fS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtYm9keSBocntib3JkZXItc3R5bGU6c29saWQ7Ym9yZGVyLWNvbG9yOiMwMDA7bWFyZ2luLXRvcDoxMHB4O3dpZHRoOjMwMHB4fS5haXJ3YXktcHJvZHVjdC1tb2RhbCAubW9kYWwtY29udGVudCAubW9kYWwtZm9vdGVye3BhZGRpbmc6NXB4fTwvc3R5bGU+JztcclxuICAgICAgICAgICAgaHRtbF9jb250YWluZXIgKz0gJzwvaGVhZD48Ym9keT48ZGl2IGNsYXNzPVwiYWlyd2F5LXByb2R1Y3QtbW9kYWxcIj48ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPjxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+JztcclxuXHJcbiAgICAgICAgICAgIHZhciBib2R5ID0gJChcIiNwcmludFwiKS5odG1sKCk7XHJcbiAgICAgICAgICAgIHZhciBteVdpbmRvdz13aW5kb3cub3BlbignJywnJywnd2lkdGg9NTAwLGhlaWdodD01MDAsc2Nyb2xsYmFycz15ZXMsbG9jYXRpb249bm8nKTtcclxuICAgICAgICAgICAgbXlXaW5kb3cuZG9jdW1lbnQud3JpdGUoaHRtbF9jb250YWluZXIpO1xyXG4gICAgICAgICAgICBteVdpbmRvdy5kb2N1bWVudC53cml0ZShib2R5KTtcclxuICAgICAgICAgICAgbXlXaW5kb3cuZG9jdW1lbnQud3JpdGUoXCI8L2Rpdj48L2Rpdj48L2Rpdj5cIik7XHJcbiAgICAgICAgICAgIG15V2luZG93LmRvY3VtZW50LndyaXRlKCc8ZGl2IGNsYXNzPVwiYWlyd2F5LXByb2R1Y3QtbW9kYWxcIj48ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPjxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+Jyk7XHJcbiAgICAgICAgICAgIG15V2luZG93LmRvY3VtZW50LndyaXRlKGJvZHkpO1xyXG4gICAgICAgICAgICBteVdpbmRvdy5kb2N1bWVudC53cml0ZShcIjwvZGl2PjwvZGl2PjwvZGl2PjwvYm9keT48L2h0bWw+XCIpO1xyXG4gICAgICAgICAgICBteVdpbmRvdy5kb2N1bWVudC5jbG9zZSgpO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBteVdpbmRvdy5mb2N1cygpO1xyXG4gICAgICAgICAgICBteVdpbmRvdy5wcmludCgpOyAgICAgICBcclxuICAgICAgICAgICAgfSwgMzAwMCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIFxyXG4gICAgY29tcHV0ZWQ6e1xyXG4gICAgICAgIGRpc2NvdW50ZWRQcmljZSgpe1xyXG4gICAgICAgICAgICB2YXIgJGRpc2NvdW50UGVyY2VudGFnZSA9IHRoaXMuZGV0YWlscy5kaXNjb3VudDtcclxuICAgICAgICAgICAgdmFyICRkaXNjb3VudERlY2ltYWwgPSAkZGlzY291bnRQZXJjZW50YWdlIC8gMTAwO1xyXG4gICAgICAgICAgICB2YXIgJG9yaWdpbmFsUHJpY2UgPSB0aGlzLmRldGFpbHMucHJpY2VfcGVyX2l0ZW07XHJcbiAgICAgICAgICAgIHZhciAkZGlzY291bnRBbW91bnQgPSAkZGlzY291bnREZWNpbWFsICogJG9yaWdpbmFsUHJpY2U7XHJcbiAgICAgICAgICAgIHZhciAkZGlzY291bnRlZFByaWNlID0gJG9yaWdpbmFsUHJpY2UgLSAkZGlzY291bnRBbW91bnQ7XHJcbiAgICAgICAgICAgICRkaXNjb3VudGVkUHJpY2UgPSAkZGlzY291bnRlZFByaWNlLnRvRml4ZWQoMik7XHJcbiAgICAgICAgICAgIHJldHVybiAoJGRpc2NvdW50ZWRQcmljZSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgcHJpY2VfcGVyX2l0ZW0oKXtcclxuICAgICAgICAgICAgaWYodGhpcy5kZXRhaWxzLmZlZV9pbmNsdWRlZD09MSlcclxuICAgICAgICAgICAgICAgIHJldHVybiBwYXJzZUZsb2F0KHRoaXMuZGV0YWlscy5wcmljZV9wZXJfaXRlbSkgKyBwYXJzZUZsb2F0KHRoaXMuZGV0YWlscy5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMuY2hhcmdlKSArIHBhcnNlRmxvYXQodGhpcy5kZXRhaWxzLnZhdCk7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmRldGFpbHMucHJpY2VfcGVyX2l0ZW07XHJcbiAgICAgICAgfSxcclxuICAgICAgICBcclxuICAgICAgICBkaXNjb3VudGVkQW1vdW50KCl7XHJcbiAgICAgICAgICAgIHZhciBhbW91bnQgPTA7XHJcbiAgICAgICAgICAgIGlmKHRoaXMuZGV0YWlscy5kaXNjb3VudCE9bnVsbClcclxuICAgICAgICAgICAgICAgIHZhciAkYW1vdW50ID0gdGhpcy5kZXRhaWxzLnByaWNlX3Blcl9pdGVtIC0gdGhpcy5kZXRhaWxzLnNhbGVfcHJpY2VcclxuICAgICAgICAgICAgaWYodGhpcy5kZXRhaWxzLmRpc2NvdW50IT1udWxsIHx8IHRoaXMuZGV0YWlscy5kaXNjb3VudCE9MClcclxuICAgICAgICAgICAgICAgIHZhciAkYW1vdW50ID0gdGhpcy5kZXRhaWxzLnNhbGVfcHJpY2U7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHZhciAkYW1vdW50ID0gMDtcclxuICAgICAgICAgICAgJGFtb3VudCA9IGFtb3VudC50b0ZpeGVkKDIpO1xyXG4gICAgICAgICAgICAvLyRhbW91bnQgPSAkYW1vdW50LnRvRml4ZWQoMik7XHJcbiAgICAgICAgICAgIHJldHVybiAkYW1vdW50O1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGRpc2NvdW50UGVyY2VudGFnZSgpe1xyXG4gICAgICAgICAgICBpZih0aGlzLmRldGFpbHMuZGlzY291bnQhPW51bGwpe1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5kZXRhaWxzLmZlZV9pbmNsdWRlZD09MSl7XHJcbiAgICAgICAgICAgICAgICAgICAgIHZhciAkYW1vdW50ID0gMTAwLSgocGFyc2VGbG9hdCh0aGlzLmRldGFpbHMucHJpY2VfcGVyX2l0ZW0pICsgcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMuc2hpcHBpbmdfZmVlKSArIHBhcnNlRmxvYXQodGhpcy5kZXRhaWxzLmNoYXJnZSkgKyBwYXJzZUZsb2F0KHRoaXMuZGV0YWlscy52YXQpIC0gcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMuZGlzY291bnQpKS8ocGFyc2VGbG9hdCh0aGlzLmRldGFpbHMucHJpY2VfcGVyX2l0ZW0pICsgcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMuc2hpcHBpbmdfZmVlKSArIHBhcnNlRmxvYXQodGhpcy5kZXRhaWxzLmNoYXJnZSkgKyBwYXJzZUZsb2F0KHRoaXMuZGV0YWlscy52YXQpKSkqMTAwO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAkYW1vdW50LnRvRml4ZWQoMik7ICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9IGVsc2VcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5kZXRhaWxzLmRpc2NvdW50O1xyXG4gICAgICAgICAgICB9IGVsc2VcclxuICAgICAgICAgICAgICAgIHJldHVybiAwO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG5vdygpe1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wcmljZV9wZXJfaXRlbS10aGlzLmRpc2NvdW50ZWRBbW91bnQ7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdG90YWxBbW91bnQoKXtcclxuICAgICAgICAgICAgdmFyIHRvdGFsID0gcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMuc3VidG90YWwpICsgcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMuc2hpcHBpbmdfZmVlKTtcclxuICAgICAgICAgICAgdG90YWwgPSB0b3RhbC50b0ZpeGVkKDIpO1xyXG4gICAgICAgICAgICByZXR1cm4gdG90YWw7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2hpcHBpbmcoKXtcclxuICAgICAgICAgICAgaWYodGhpcy5kZXRhaWxzLmZlZV9pbmNsdWRlZCE9MSl7XHJcbiAgICAgICAgICAgICAgICB2YXIgdG90YWwgPSBwYXJzZUZsb2F0KHRoaXMuZGV0YWlscy5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMuY2hhcmdlKSArIHBhcnNlRmxvYXQodGhpcy5kZXRhaWxzLnZhdCk7XHJcbiAgICAgICAgICAgICAgICB0b3RhbCA9IHRvdGFsLnRvRml4ZWQoMik7XHJcbiAgICAgICAgICAgIH0gZWxzZSBcclxuICAgICAgICAgICAgICAgIHZhciB0b3RhbCA9IDAuMDA7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdG90YWw7ICAgICAgICAgIFxyXG4gICAgICAgIH0sICBcclxuXHJcbiAgICAgICAgd2VpZ2h0KCl7XHJcbiAgICAgICAgICAgIHZhciBEVyA9IChwYXJzZUZsb2F0KHRoaXMuZGV0YWlscy5sZW5ndGgpICogcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMud2lkdGgpICogcGFyc2VGbG9hdCh0aGlzLmRldGFpbHMuaGVpZ2h0KSkvMzUwMDtcclxuICAgICAgICAgICAgaWYodGhpcy5kZXRhaWxzLndlaWdodD5EVylcclxuICAgICAgICAgICAgICAgIHJldHVybiAodGhpcy5kZXRhaWxzLndlaWdodCk7XHJcbiAgICAgICAgICAgIGlmKERXPnRoaXMuZGV0YWlscy53ZWlnaHQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gRFc7XHJcbiAgICAgICAgfSAgICBcclxuICAgIH0sXHJcblxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgICB0aGlzLmdldFN0b3JlcygpO1xyXG4gICAgICAgIHRoaXMuZ2V0VXNhYmxlRXdhbGxldEJhbGFuY2UoKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ29yZGVyLmluLXRyYW5zaXQnLCBkYXRhID0+IHRoaXMuaW5UcmFuc2l0KGRhdGEpKTtcclxuICAgICAgICBFdmVudC5saXN0ZW4oJ29yZGVyLmNhbmNlbGxlZCcsIGRhdGEgPT4gdGhpcy5jYW5jZWxsZWQoZGF0YSkpO1xyXG5cclxuICAgICAgICBheGlvcy5nZXQoJy90cmFuc2xhdGUvc2FsZXMtcmVwb3J0JylcclxuICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sYW5nID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgJCgnI2luVHJhbnNpdE1vZGFsMicpLm9uKCdoaWRkZW4uYnMubW9kYWwnLCBlID0+IHtcclxuICAgICAgICAgICAgaWYgKCEgdGhpcy50cmFuc2l0Rm9ybS5zdWJtaXR0ZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcmRlci5zdGF0dXMgPSB0aGlzLnNlbGVjdGVkT3JkZXIub3JpZ2luYWxfc3RhdHVzO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICQoJyNjYW5jZWxNb2RhbCcpLm9uKCdoaWRkZW4uYnMubW9kYWwnLCBlID0+IHtcclxuICAgICAgICAgICAgaWYgKCEgdGhpcy5jYW5jZWxGb3JtLnN1Ym1pdHRlZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9yZGVyLnN0YXR1cyA9IHRoaXMuc2VsZWN0ZWRPcmRlci5vcmlnaW5hbF9zdGF0dXM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSlcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3NhbGVzLXJlcG9ydC5qcyIsIjx0ZW1wbGF0ZT5cclxuXHQ8dHI+XHJcblx0XHQ8dGQgY2xhc3M9XCJyZXBvcnQtaGVhZGVyXCI+XHJcblx0XHRcdDxwPjxiPnt7b3JkZXIudHJhY2tpbmdfbnVtYmVyfX08L2I+PC9wPlxyXG5cdFx0XHQ8cD57e29yZGVyLmNyZWF0ZWRfYXR9fTwvcD5cclxuXHRcdDwvdGQ+XHJcblx0XHQ8dGQgY2xhc3M9XCJyZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7b3JkZXIucHJvZHVjdC5uYW1lfX08L3RkPlxyXG5cdFx0PHRkIGNsYXNzPVwicmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e29yZGVyLnF1YW50aXR5fX08L3RkPlxyXG5cdFx0PHRkIGNsYXNzPVwicmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e25vdyB8IGN1cnJlbmN5fX08L3RkPlxyXG5cdFx0PHRkIGNsYXNzPVwicmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e29yZGVyLnN1YnRvdGFsfX08L3RkPlxyXG5cdFx0PHRkIGNsYXNzPVwicmVwb3J0LWhlYWRlclwiPlxyXG5cdFx0XHQ8ZGl2IHYtaWY9XCIob3JkZXIuc3RhdHVzICE9ICdSZWNlaXZlZCcpICYmIChvcmRlci5zdGF0dXMgIT0gJ0RlbGl2ZXJlZCcpICYmIChvcmRlci5zdGF0dXMgIT0gJ1JldHVybmVkJykgJiYgKG9yZGVyLnN0YXR1cyAhPSAnSW4gVHJhbnNpdCcpICYmIChvcmRlci5zdGF0dXMgIT0gJ0NhbmNlbGxlZCcpIFwiIGNsYXNzPVwiYnRuLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgXHQ8YnV0dG9uIEBjbGljaz1cInN0YXR1c0FjdGlvblwiIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBidG4tcmFpc2VkIGJ0bi1zbVwiPlxyXG4gICAgICAgICAgICAgIFx0PHNwYW4gdi1pZj1cIidJbiBUcmFuc2l0JyA9PSBvcmRlci5zdGF0dXNcIj57e3RoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsnaW4tdHJhbnNpdCddfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgXHQ8c3BhbiB2LWlmPVwiJ0NhbmNlbGxlZCcgPT0gb3JkZXIuc3RhdHVzXCI+e3t0aGlzLiRwYXJlbnQuJHBhcmVudC5sYW5nLmRhdGFbJ2NhbmNlbGxlZCddfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgXHQ8c3BhbiB2LWlmPVwiJ1BlbmRpbmcnID09IG9yZGVyLnN0YXR1c1wiPnt7dGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydwZW5kaW5nJ119fTwvc3Bhbj5cclxuICAgICAgICAgICAgICBcdDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIFx0PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLXJhaXNlZCBidG4tc20gZHJvcGRvd24tdG9nZ2xlXCIgZGF0YS10b2dnbGU9XCJkcm9wZG93blwiIGFyaWEtaGFzcG9wdXA9XCJ0cnVlXCIgYXJpYS1leHBhbmRlZD1cImZhbHNlXCI+XHJcbiAgICAgICAgICAgIFx0ICAgIDxzcGFuIGNsYXNzPVwiY2FyZXRcIj48L3NwYW4+XHJcbiAgICAgICAgICAgIFx0ICAgIDxzcGFuIGNsYXNzPVwic3Itb25seVwiPlRvZ2dsZSBEcm9wZG93bjwvc3Bhbj5cclxuICAgICAgICAgICAgICBcdDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIFx0PHVsIGNsYXNzPVwiZHJvcGRvd24tbWVudVwiPlxyXG5cdCAgICAgICAgICAgICAgICA8bGkgdi1pZj1cIidJbiBUcmFuc2l0JyAhPSBvcmRlci5zdGF0dXNcIj5cclxuXHQgICAgICAgICAgICAgICAgXHQ8YSBAY2xpY2s9XCJ1cGRhdGVTdGF0dXMoJ0luIFRyYW5zaXQnKVwiIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIj57e3RoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsnaW4tdHJhbnNpdCddfX08L2E+XHJcblx0ICAgICAgICAgICAgICAgIDwvbGk+XHJcblx0ICAgICAgICAgICAgICAgIDxsaSB2LWlmPVwiJ0NhbmNlbGxlZCcgIT0gb3JkZXIuc3RhdHVzXCI+XHJcblx0ICAgICAgICAgICAgICAgIFx0PGEgQGNsaWNrPVwidXBkYXRlU3RhdHVzKCdDYW5jZWxsZWQnKVwiIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIj57e3RoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsnY2FuY2VsbGVkJ119fTwvYT5cclxuXHQgICAgICAgICAgICAgICAgPC9saT5cclxuXHQgICAgICAgICAgICAgICAgPGxpIHYtaWY9XCInUGVuZGluZycgIT0gb3JkZXIuc3RhdHVzXCI+XHJcblx0ICAgICAgICAgICAgICAgIFx0PGEgQGNsaWNrPVwidXBkYXRlU3RhdHVzKCdQZW5kaW5nJylcIiBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCI+e3t0aGlzLiRwYXJlbnQuJHBhcmVudC5sYW5nLmRhdGFbJ3BlbmRpbmcnXX19PC9hPlxyXG5cdCAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgIFx0PC91bD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG5cdFx0XHQ8YnV0dG9uIEBjbGljaz0ncmVjZWl2ZWRCeVNlbGxlcihvcmRlci5pZCknIHYtaWY9XCIob3JkZXIuc3RhdHVzID09ICdSZXR1cm5lZCcgJiYgb3JkZXIucmV0dXJuZWRfcmVjZWl2ZWRfZGF0ZSA9PSBudWxsKSBcIiBjbGFzcz0nYnRuIGJ0bi14cyBidG4tcmFpc2VkIGJ0bi13YXJuaW5nJyA6dGl0bGU9XCJ0aGlzLiRwYXJlbnQuJHBhcmVudC5sYW5nLmRhdGFbJ3JlY2VpdmVkLXJldHVybmVkLW1zZy0yJ11cIiBkYXRhLXRvZ2dsZT0ndG9vbHRpcCcgZGF0YS1wbGFjZW1lbnQ9J2JvdHRvbSc+PHNwYW4gY2xhc3M9J2ZhIGZhLWV4Y2xhbWF0aW9uJz48L3NwYW4+PC9idXR0b24+XHJcblx0XHRcdDxzcGFuIHYtaWY9XCIob3JkZXIuc3RhdHVzID09ICdJbiBUcmFuc2l0JykgfHwgKG9yZGVyLnN0YXR1cyA9PSAnUmVjZWl2ZWQnKSB8fCAob3JkZXIuc3RhdHVzID09ICdEZWxpdmVyZWQnKSB8fCAob3JkZXIuc3RhdHVzID09ICdSZXR1cm5lZCcpIHx8IChvcmRlci5zdGF0dXMgPT0gJ0NhbmNlbGxlZCcpIFwiPnt7dGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhW3RoaXMub3JkZXJzdGF0dXNdfX08L3NwYW4+XHJcblx0XHQ8L3RkPlxyXG5cdFx0PHRkPlxyXG5cdFx0XHQ8YSBAY2xpY2s9XCJ2aWV3RGV0YWlscyhvcmRlci5pZClcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiPlxyXG5cdFx0XHRcdDxzcGFuIGNsYXNzPVwiaGlkZGVuLXhzIGhpZGRlbi1zbVwiPnt7dGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydzZWUtbW9yZSddfX08L3NwYW4+XHJcblx0XHRcdFx0PHNwYW4gY2xhc3M9XCJoaWRkZW4tbGcgaGlkZGVuLW1kXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJsZWZ0XCIgdGl0bGU9XCJcIiBkYXRhLW9yaWdpbmFsLXRpdGxlPVwiU2VlIE1vcmVcIj48aSBjbGFzcz1cImZhIGZhLWV5ZVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L3NwYW4+XHJcblx0XHRcdDwvYT5cclxuXHRcdDwvdGQ+XHJcblx0XHQ8dGQ+XHJcblx0XHRcdDxhIEBjbGljaz1cInZpZXdBaXJ3YXlCaWxsKG9yZGVyLmlkKVwiIHN0eWxlPVwiY3Vyc29yOnBvaW50ZXI7XCIgdi1pZj1cIihvcmRlci5zdGF0dXMgIT0gJ1BlbmRpbmcnKVwiPlxyXG5cdFx0XHRcdDxzcGFuIGNsYXNzPVwiaGlkZGVuLXhzIGhpZGRlbi1zbVwiPnt7dGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydhaXJ3YXktYmlsbCddfX08L3NwYW4+XHJcblx0XHRcdFx0PHNwYW4gY2xhc3M9XCJoaWRkZW4tbGcgaGlkZGVuLW1kXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJsZWZ0XCIgdGl0bGU9XCJcIiBkYXRhLW9yaWdpbmFsLXRpdGxlPVwiQWlyd2F5IEJpbGxcIj48aSBjbGFzcz1cImZhIGZhLWZpbGUtdGV4dFwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L3NwYW4+XHJcblx0XHRcdDwvYT5cclxuXHRcdDwvdGQ+XHJcblx0PC90cj5cclxuPC90ZW1wbGF0ZT5cclxuPHNjcmlwdD5cclxuXHRleHBvcnQgZGVmYXVsdHtcclxuXHRcdHByb3BzOlsnb3JkZXInXSxcclxuXHRcdGRhdGEgKCkge1xyXG5cdFx0ICAgIHJldHVybiB7XHJcblx0XHQgICAgXHRcclxuXHRcdFx0fVxyXG5cdCAgICB9LFxyXG5cdFx0Y29tcHV0ZWQ6e1xyXG5cdCAgICAgICAgcHJpY2VfcGVyX2l0ZW0oKXtcclxuXHQgICAgICAgICAgICBpZih0aGlzLm9yZGVyLmZlZV9pbmNsdWRlZD09MSlcclxuXHQgICAgICAgICAgICAgICAgcmV0dXJuIHBhcnNlRmxvYXQodGhpcy5vcmRlci5wcmljZV9wZXJfaXRlbSkgKyBwYXJzZUZsb2F0KHRoaXMub3JkZXIuc2hpcHBpbmdfZmVlKSArIHBhcnNlRmxvYXQodGhpcy5vcmRlci5jaGFyZ2UpICsgcGFyc2VGbG9hdCh0aGlzLm9yZGVyLnZhdCk7XHJcblx0ICAgICAgICAgICAgZWxzZVxyXG5cdCAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5vcmRlci5wcmljZV9wZXJfaXRlbTtcclxuXHQgICAgICAgIH0sXHJcblx0ICAgICAgICBkaXNjb3VudGVkQW1vdW50KCl7XHJcblx0ICAgICAgICAgICAgaWYodGhpcy5vcmRlci5kaXNjb3VudCE9bnVsbCB8fCB0aGlzLm9yZGVyLmRpc2NvdW50ICE9IDApXHJcblx0ICAgICAgICAgICAgICAgIHZhciAkYW1vdW50ID0gdGhpcy5vcmRlci5zYWxlX3ByaWNlO1xyXG5cdCAgICAgICAgICAgIGVsc2VcclxuXHQgICAgICAgICAgICAgICAgdmFyICRhbW91bnQgPSAwO1xyXG5cdCAgICAgICAgICAgIC8vJGFtb3VudCA9ICRhbW91bnQudG9GaXhlZCgyKTtcclxuXHQgICAgICAgICAgICByZXR1cm4gJGFtb3VudDtcclxuXHQgICAgICAgIH0sXHJcblx0ICAgICAgICBub3coKXtcclxuXHQgICAgICAgICAgICByZXR1cm4gdGhpcy5wcmljZV9wZXJfaXRlbS10aGlzLmRpc2NvdW50ZWRBbW91bnQ7XHJcblx0ICAgICAgICB9LFx0ICAgICAgICBcclxuXHRcdFx0dG90YWwoKXtcclxuXHRcdFx0XHRyZXR1cm4gKHRoaXMub3JkZXIucXVhbnRpdHkgKiB0aGlzLm9yZGVyLnByaWNlX3Blcl9pdGVtKVxyXG5cdFx0XHR9LFxyXG5cdFx0XHRvcmRlcnN0YXR1cygpe1xyXG5cdFx0XHRpZih0aGlzLm9yZGVyLnN0YXR1cyA9PSBcIkluIFRyYW5zaXRcIil7XHJcblx0XHRcdFx0cmV0dXJuICdpbi10cmFuc2l0JztcclxuXHRcdFx0fVxyXG5cdFx0ICAgICAgcmV0dXJuIHRoaXMub3JkZXIuc3RhdHVzLnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdH0sXHJcblx0XHR9LFxyXG5cdFx0Y29tcG9uZW50czoge1xyXG5cdFx0ICBcdE11bHRpc2VsZWN0OiB3aW5kb3cuVnVlTXVsdGlzZWxlY3QuZGVmYXVsdFxyXG5cdFx0fSxcclxuXHRcdG1ldGhvZHM6e1xyXG5cdFx0XHR2aWV3QWlyd2F5QmlsbChpZCl7XHJcblx0XHQgICAgICAgIHRoaXMuJHBhcmVudC4kcGFyZW50LnNob3dBaXJ3YXlCaWxsKGlkKTtcclxuXHRcdCAgICB9LFxyXG5cdFx0XHRyZWNlaXZlZEJ5U2VsbGVyKGlkKXtcclxuXHRcdFx0XHRzd2FsKHtcclxuXHRcdCAgIFx0XHRcdFx0dGl0bGU6IHRoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsnY29uZmlybSddLFxyXG5cdFx0ICAgXHRcdFx0XHRodG1sOiAgdGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydyZWNlaXZlZC1yZXR1cm5lZC1tc2ctMSddLFxyXG5cdFx0ICAgXHRcdFx0XHR0eXBlOiAnd2FybmluZycsXHJcblx0XHQgICBcdFx0XHRcdHNob3dDYW5jZWxCdXR0b246IHRydWUsXHJcblx0XHQgICBcdFx0XHRcdGNvbmZpcm1CdXR0b25Db2xvcjogJyMzMDg1ZDYnLFxyXG5cdFx0ICAgXHRcdFx0XHRjYW5jZWxCdXR0b25Db2xvcjogJyNkMzMnLFxyXG5cdFx0ICAgXHRcdFx0XHRjb25maXJtQnV0dG9uVGV4dDogdGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWyd5ZXMnXVxyXG5cdFx0ICAgXHRcdFx0fSkudGhlbigoKSA9PiB7XHJcblx0XHQgICBcdFx0XHRcdGF4aW9zQVBJdjEucG9zdCgnb3JkZXItZGV0YWlscy9yZWNlaXZlLXJldHVybmVkLWl0ZW0nICwge1xyXG5cdFx0ICAgXHRcdFx0XHRcdC8vbWFrZSBhcGkgdG8gcmV0dXJuIHRoZSBtb25leSB0byB0aGUgYnV5ZXIgYW5kIHN0b2NrcyBhbmQgdGhlIGN1cnJlbnQgZGF0ZSBmb3IgdGhlIHJldHVybmVkX3JlY2VpdmVkX2RhdGUgY29sdW1uXHJcblx0XHQgICBcdFx0XHRcdFx0aWQ6aWRcclxuXHRcdCAgIFx0XHRcdFx0fSApXHJcblx0XHRcdFx0XHRcdCAgICAudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdCAgICAgICAgICBcdFx0dG9hc3RyLnN1Y2Nlc3ModGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydzYXZlZC1tc2cnXSk7XHJcblx0XHRcdFx0XHRcdCAgICBcdHRoaXMuJHBhcmVudC4kcGFyZW50LmdldFN0b3JlcygpO1xyXG5cdFx0XHRcdFx0XHQgICAgfSlcclxuXHRcdFx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdCAgICB0b2FzdHIuZXJyb3IodGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWyd1cGRhdGUtZmFpbGVkLW1zZyddKTtcclxuXHRcdFx0XHRcdFx0XHR9KTtcclxuXHRcdCAgIFx0XHRcdH0pLmNhdGNoKCgpID0+IHRoaXMub3JkZXIuc3RhdHVzID0gdGhpcy5vcmRlci5vcmlnaW5hbF9zdGF0dXMpO1xyXG5cdFx0XHR9LFxyXG5cclxuXHRcdFx0dmlld0RldGFpbHMoaWQpe1xyXG5cdFx0ICAgICAgICB0aGlzLiRwYXJlbnQuJHBhcmVudC5zaG93RGV0YWlscyhpZCk7XHJcblx0XHQgICAgfSxcclxuXHJcblx0XHQgICBcdHVwZGF0ZVN0YXR1cyhzdGF0dXMpIHtcclxuXHRcdCAgIFx0XHR0aGlzLm9yZGVyLnN0YXR1cyA9IHN0YXR1cztcclxuXHJcblx0XHQgICBcdFx0aWYgKHN0YXR1cyA9PT0gJ0luIFRyYW5zaXQnKSB7XHJcblx0XHQgICBcdFx0XHRFdmVudC5maXJlKCdvcmRlci5pbi10cmFuc2l0JywgdGhpcy5vcmRlcik7XHJcblx0XHQgICBcdFx0fSBlbHNlIGlmIChzdGF0dXMgPT09ICdDYW5jZWxsZWQnKSB7XHJcblx0XHQgICBcdFx0XHRFdmVudC5maXJlKCdvcmRlci5jYW5jZWxsZWQnLCB0aGlzLm9yZGVyKTtcclxuXHRcdCAgIFx0XHR9IGVsc2UgaWYgKHN0YXR1cyA9PT0gJ1BlbmRpbmcnKSB7XHJcblx0XHQgICBcdFx0XHRzd2FsKHtcclxuXHRcdCAgIFx0XHRcdFx0dGl0bGU6IHRoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsnY29uZmlybSddLFxyXG5cdFx0ICAgXHRcdFx0XHRodG1sOiB0aGlzLiRwYXJlbnQuJHBhcmVudC5sYW5nLmRhdGFbJ21hcmstcGVuZGluZy1tc2cnXSxcclxuXHRcdCAgIFx0XHRcdFx0dHlwZTogJ3dhcm5pbmcnLFxyXG5cdFx0ICAgXHRcdFx0XHRzaG93Q2FuY2VsQnV0dG9uOiB0cnVlLFxyXG5cdFx0ICAgXHRcdFx0XHRjb25maXJtQnV0dG9uQ29sb3I6ICcjMzA4NWQ2JyxcclxuXHRcdCAgIFx0XHRcdFx0Y2FuY2VsQnV0dG9uQ29sb3I6ICcjZDMzJyxcclxuXHRcdCAgIFx0XHRcdFx0Y29uZmlybUJ1dHRvblRleHQ6IHRoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsneWVzJ11cclxuXHRcdCAgIFx0XHRcdH0pLnRoZW4oKCkgPT4ge1xyXG5cdFx0ICAgXHRcdFx0XHRheGlvcy5nZXQoJy91c2VyL3NhbGVzLXJlcG9ydC91cGRhdGVTdGF0dXMvJyArIHRoaXMub3JkZXIuaWQgKyAnLycgKyBzdGF0dXMpXHJcblx0XHRcdFx0XHRcdCAgICAudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdCAgICAgICAgICBcdFx0dG9hc3RyLnN1Y2Nlc3ModGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydzYXZlZC1tc2cnXSk7XHJcblx0XHRcdFx0XHRcdCAgICBcdHRoaXMuJHBhcmVudC4kcGFyZW50LmdldFN0b3JlcygpO1xyXG5cdFx0XHRcdFx0XHQgICAgfSlcclxuXHRcdFx0XHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdCAgICB0b2FzdHIuZXJyb3IodGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWyd1cGRhdGUtZmFpbGVkLW1zZyddKTtcclxuXHRcdFx0XHRcdFx0XHR9KTtcclxuXHRcdCAgIFx0XHRcdH0pLmNhdGNoKCgpID0+IHRoaXMub3JkZXIuc3RhdHVzID0gdGhpcy5vcmRlci5vcmlnaW5hbF9zdGF0dXMpO1xyXG5cdFx0ICAgXHRcdH1cclxuXHRcdCAgIFx0fSxcclxuXHJcblx0XHQgICBcdHN0YXR1c0FjdGlvbigpIHtcclxuXHRcdCAgIFx0XHRpZiAodGhpcy5vcmRlci5zdGF0dXMgPT09ICdJbiBUcmFuc2l0Jykge1xyXG5cdFx0ICAgXHRcdFx0RXZlbnQuZmlyZSgnb3JkZXIuaW4tdHJhbnNpdCcsIHRoaXMub3JkZXIpO1xyXG5cdFx0ICAgXHRcdH0gZWxzZSBpZiAodGhpcy5vcmRlci5zdGF0dXMgPT09ICdDYW5jZWxsZWQnKSB7XHJcblx0XHQgICBcdFx0XHRFdmVudC5maXJlKCdvcmRlci5jYW5jZWxsZWQnLCB0aGlzLm9yZGVyKTtcclxuXHRcdCAgIFx0XHR9XHJcblx0XHQgICBcdH1cclxuXHRcdH0sXHJcblxyXG5cdFx0Y3JlYXRlZCgpIHtcclxuXHRcdFx0dGhpcy5vcmRlci5vcmlnaW5hbF9zdGF0dXMgPSB0aGlzLm9yZGVyLnN0YXR1cztcclxuXHRcdH0sXHJcblxyXG5cdFx0YmVmb3JlVXBkYXRlKCkge1xyXG5cdFx0XHRpZiAoISB0aGlzLm9yZGVyLm9yaWdpbmFsX3N0YXR1cykge1xyXG5cdFx0XHRcdHRoaXMub3JkZXIub3JpZ2luYWxfc3RhdHVzID0gdGhpcy5vcmRlci5zdGF0dXM7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzYWxlc1RhYmxlLnZ1ZT81NmRjZTZlMyIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2IGNsYXNzPVwicm93XCI+XHJcblx0XHQ8ZGl2IGNsYXNzPVwiY29sLXNtLTEyXCI+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCJwYW5lbC1ncm91cCBzdG9yZS1wYW5lbFwiPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0XCI+XHJcblx0XHRcdFx0XHQ8IS0tIDxkaXYgY2xhc3M9XCJjb2wtc20tMTIgcGFuZWwtaGVhZGluZyBwbmwtc3RvcmVcIiBkYXRhLXRvZ2dsZT1cImNvbGxhcHNlXCIgOmhyZWY9XCInI3N0b3JlXycrc3RvcmUuaWRcIiA6aWQ9XCJzdG9yZS5pZFwiPlxyXG5cdFx0XHRcdFx0XHQ8c3Bhbj5cclxuXHRcdFx0XHRcdFx0XHQ8YT5cclxuXHRcdFx0XHRcdFx0XHRcdDxoNCBjbGFzcz1cInN0b3JlLW5hbWVcIj57e3N0b3JlLm5hbWV9fVxyXG5cdFx0XHRcdFx0XHRcdFx0PHNwYW4gdi1pZj1cInN0b3JlLnBlbmRpbmdfb3JkZXJfY291bnQubGVuZ3RoICE9ICcnXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxpIGNsYXNzPVwiZmEgZmEtZXhjbGFtYXRpb24tY2lyY2xlIHBlbmRpbmctbm90aWZpY2F0aW9uXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9zcGFuPlx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0XHQ8L2g0PlxyXG5cdFx0XHRcdFx0XHRcdFx0PHAgc3R5bGU9XCJmb250LXNpemU6IHNtYWxsO1wiPnt7c3RvcmUuY25fbmFtZX19PC9wPlxyXG5cdFx0XHRcdFx0XHRcdDwvYT5cclxuXHRcdFx0XHRcdFx0PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHQ8aHI+XHJcblx0XHRcdFx0XHQ8L2Rpdj4gLS0+PCEtLSBlbmQgcGFuZWwtaGVhZGluZy0tPlxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQ8ZGl2IDppZD1cIidzdG9yZV8nK3N0b3JlLmlkXCIgY2xhc3M9XCJwYW5lbC1jb2xsYXBzZSBjb2xsYXBzZVwiIDpjbGFzcz1cInsgaW46IChpbmRleCA9PSAwKSB9XCI+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLWxnLTEyXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dWwgY2xhc3M9XCJuYXYgbmF2LXBpbGxzXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQgIDxsaSA6aWQ9XCInZmxfUGVuZGluZ18nK3N0b3JlLmlkXCIgY2xhc3M9XCJiYWRnZS1kZXRhaWxzXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdCAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiICBAY2xpY2s9XCJmaWx0ZXJTdGF0dXMoc3RvcmUuaWQsICdQZW5kaW5nJylcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3BlbmRpbmctb3JkZXInXX19XHJcblx0XHRcdFx0XHRcdFx0XHRcdCAgXHQ8c3BhbiBjbGFzcz1cImJhZGdlXCI+e3tzdG9yZS5wZW5kaW5nX29yZGVyX2NvdW50Lmxlbmd0aH19PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQgIDwvYT5cclxuXHRcdFx0XHRcdFx0XHRcdCAgPC9saT5cclxuXHRcdFx0XHRcdFx0XHRcdCAgPGxpIDppZD1cIidmbF9EZWxpdmVyZWRfJytzdG9yZS5pZFwiICBjbGFzcz1cImJhZGdlLWRldGFpbHNcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0ICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgIEBjbGljaz1cImZpbHRlclN0YXR1cyhzdG9yZS5pZCwgJ0RlbGl2ZXJlZCcpXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydkZWxpdmVyZWQnXX19XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0ICA8c3BhbiBjbGFzcz1cImJhZGdlXCI+e3tzdG9yZS5kZWxpdmVyZWRfb3JkZXJfY291bnQubGVuZ3RofX08L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHRcdCAgPC9hPlxyXG5cdFx0XHRcdFx0XHRcdFx0ICA8L2xpPlxyXG5cdFx0XHRcdFx0XHRcdFx0ICA8bGkgOmlkPVwiJ2ZsX0NhbmNlbGxlZF8nK3N0b3JlLmlkXCIgIGNsYXNzPVwiYmFkZ2UtZGV0YWlsc1wiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiAgQGNsaWNrPVwiZmlsdGVyU3RhdHVzKHN0b3JlLmlkLCAnQ2FuY2VsbGVkJylcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ2NhbmNlbGxlZC1vcmRlciddfX0gXHJcblx0XHRcdFx0XHRcdFx0XHRcdCAgXHQ8c3BhbiBjbGFzcz1cImJhZGdlXCI+e3tzdG9yZS5jYW5jZWxsZWRfb3JkZXJfY291bnQubGVuZ3RofX08L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHRcdCAgPC9hPlxyXG5cdFx0XHRcdFx0XHRcdFx0ICA8L2xpPlxyXG5cdFx0XHRcdFx0XHRcdFx0ICA8bGkgOmlkPVwiJ2ZsX1RyYW5zaXRfJytzdG9yZS5pZFwiICBjbGFzcz1cImJhZGdlLWRldGFpbHNcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0ICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgQGNsaWNrPVwiZmlsdGVyU3RhdHVzKHN0b3JlLmlkLCAnVHJhbnNpdCcpXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydpbi10cmFuc2l0J119fVxyXG5cdFx0XHRcdFx0XHRcdFx0XHQgIFx0PHNwYW4gY2xhc3M9XCJiYWRnZVwiPnt7c3RvcmUudHJhbnNpdF9vcmRlcl9jb3VudC5sZW5ndGh9fTwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0ICA8L2E+XHJcblx0XHRcdFx0XHRcdFx0XHQgIDwvbGk+XHJcblx0XHRcdFx0XHRcdFx0XHQgIDxsaSA6aWQ9XCInZmxfQWxsXycrc3RvcmUuaWRcIiAgY2xhc3M9XCJhY3RpdmUgYmFkZ2UtZGV0YWlsc1wiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBAY2xpY2s9XCJmaWx0ZXJTdGF0dXMoc3RvcmUuaWQsICdBbGwnKVwiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnYWxsJ119fSBcclxuXHRcdFx0XHRcdFx0XHRcdFx0ICBcdDxzcGFuIGNsYXNzPVwiYmFkZ2VcIj57e3N0b3JlLm9yZGVyX2RldGFpbHMubGVuZ3RofX08L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHRcdCAgPC9hPlxyXG5cdFx0XHRcdFx0XHRcdFx0ICA8L2xpPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC91bD5cclxuXHRcdFx0XHRcdFx0XHRcdDx0YWJsZSBjbGFzcz0ndGFibGUgdGFibGUtc3RyaXBlZCBmaWx0ZXItdGFibGUgc2FsZXMtdGJsIHNob3cnIHYtaWY9XCJzdG9yZS5vcmRlcl9kZXRhaWxzICE9ICcnXCIgOmlkPVwiJ2FsbF90YmxfJytzdG9yZS5pZFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGhlYWQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndHJhbnNhY3Rpb24nXX19PC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0yIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydwcm9kdWN0J119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsncXR5J119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMSByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndW5pdC1wcmljZSddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RvdGFsJ119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydzdGF0dXMnXX19PC90aD5cdFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiPjwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMSByZXBvcnQtaGVhZGVyXCI+PC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L3RoZWFkPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGJvZHk+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PG9yZGVyc1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0di1mb3I9XCJvcmRlciBpbiBzdG9yZS5vcmRlcl9kZXRhaWxzXCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDpvcmRlciA9IFwib3JkZXJcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0OmtleSA9IFwib3JkZXIuaWRcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdD48L29yZGVycz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90Ym9keT5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdGFibGU+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dGFibGUgY2xhc3M9J3RhYmxlIHRhYmxlLXN0cmlwZWQgZmlsdGVyLXRhYmxlIHNhbGVzLXRibCcgdi1pZj1cInN0b3JlLnBlbmRpbmdfb3JkZXJfY291bnQgIT0gJydcIiA6aWQ9XCIncGVuZGluZ190YmxfJytzdG9yZS5pZFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGhlYWQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndHJhbnNhY3Rpb24nXX19PC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0yIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydwcm9kdWN0J119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsncXR5J119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMSByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndW5pdC1wcmljZSddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RvdGFsJ119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydzdGF0dXMnXX19PC90aD5cdFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiPjwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMSByZXBvcnQtaGVhZGVyXCI+PC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L3RoZWFkPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGJvZHk+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PG9yZGVyc1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0di1mb3I9XCJvcmRlciBpbiBzdG9yZS5wZW5kaW5nX29yZGVyX2NvdW50XCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDpvcmRlciA9IFwib3JkZXJcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0OmtleSA9IFwib3JkZXIuaWRcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdD48L29yZGVycz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90Ym9keT5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdGFibGU+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dGFibGUgY2xhc3M9J3RhYmxlIHRhYmxlLXN0cmlwZWQgZmlsdGVyLXRhYmxlIHNhbGVzLXRibCcgdi1pZj1cInN0b3JlLmRlbGl2ZXJlZF9vcmRlcl9jb3VudCAhPSAnJ1wiIDppZD1cIidkZWxpdmVyZWRfdGJsXycrc3RvcmUuaWRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRoZWFkPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDx0cj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0yIHJlcG9ydC1oZWFkZXJcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RyYW5zYWN0aW9uJ119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsncHJvZHVjdCddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTIgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3F0eSddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3VuaXQtcHJpY2UnXX19PC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0xIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWyd0b3RhbCddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnc3RhdHVzJ119fTwvdGg+XHRcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0xIHJlcG9ydC1oZWFkZXJcIj48L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiPjwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PC90cj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90aGVhZD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRib2R5PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxvcmRlcnNcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHYtZm9yPVwib3JkZXIgaW4gc3RvcmUuZGVsaXZlcmVkX29yZGVyX2NvdW50XCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDpvcmRlciA9IFwib3JkZXJcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0OmtleSA9IFwib3JkZXIuaWRcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdD48L29yZGVycz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90Ym9keT5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdGFibGU+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dGFibGUgY2xhc3M9J3RhYmxlIHRhYmxlLXN0cmlwZWQgZmlsdGVyLXRhYmxlIHNhbGVzLXRibCcgdi1pZj1cInN0b3JlLmNhbmNlbGxlZF9vcmRlcl9jb3VudCAhPSAnJ1wiIDppZD1cIidjYW5jZWxsZWRfdGJsXycrc3RvcmUuaWRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRoZWFkPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDx0cj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0yIHJlcG9ydC1oZWFkZXJcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RyYW5zYWN0aW9uJ119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsncHJvZHVjdCddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTIgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3F0eSddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3VuaXQtcHJpY2UnXX19PC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0xIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWyd0b3RhbCddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnc3RhdHVzJ119fTwvdGg+XHRcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0xIHJlcG9ydC1oZWFkZXJcIj48L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiPjwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PC90cj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90aGVhZD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRib2R5PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxvcmRlcnNcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHYtZm9yPVwib3JkZXIgaW4gc3RvcmUuY2FuY2VsbGVkX29yZGVyX2NvdW50XCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDpvcmRlciA9IFwib3JkZXJcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0OmtleSA9IFwib3JkZXIuaWRcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdD48L29yZGVycz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90Ym9keT5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdGFibGU+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dGFibGUgY2xhc3M9J3RhYmxlIHRhYmxlLXN0cmlwZWQgZmlsdGVyLXRhYmxlIHNhbGVzLXRibCcgdi1pZj1cInN0b3JlLnRyYW5zaXRfb3JkZXJfY291bnQgIT0gJydcIiA6aWQ9XCIndHJhbnNpdF90YmxfJytzdG9yZS5pZFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGhlYWQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndHJhbnNhY3Rpb24nXX19PC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImNvbC1zbS0yIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydwcm9kdWN0J119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsncXR5J119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMSByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiPnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndW5pdC1wcmljZSddfX08L3RoPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIj57e3RoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RvdGFsJ119fTwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCI+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydzdGF0dXMnXX19PC90aD5cdFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHRoIGNsYXNzPVwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiPjwvdGg+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJjb2wtc20tMSByZXBvcnQtaGVhZGVyXCI+PC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L3RoZWFkPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGJvZHk+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PG9yZGVyc1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0di1mb3I9XCJvcmRlciBpbiBzdG9yZS50cmFuc2l0X29yZGVyX2NvdW50XCJcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDpvcmRlciA9IFwib3JkZXJcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0OmtleSA9IFwib3JkZXIuaWRcIlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdD48L29yZGVycz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90Ym9keT5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdGFibGU+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibm8tcHVyY2hhc2Ugc2hvd1wiIDppZD1cIidhbGxfaGRyJytzdG9yZS5pZFwidi1pZj1cInN0b3JlLm9yZGVyX2RldGFpbHMgPT0gJydcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGg0Pnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnbm8tcHVyY2hhc2VzLW1zZyddfX08L2g0PlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibm8tcHVyY2hhc2VcIiA6aWQ9XCIncGVuZGluZ19oZHInK3N0b3JlLmlkXCJ2LWlmPVwic3RvcmUucGVuZGluZ19vcmRlcl9jb3VudCA9PSAnJ1wiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8aDQ+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWyduby1wZW5kaW5nLW9yZGVyLW1zZyddfX08L2g0PlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibm8tcHVyY2hhc2VcIiA6aWQ9XCInZGVsaXZlcmVkX2hkcicrc3RvcmUuaWRcInYtaWY9XCJzdG9yZS5kZWxpdmVyZWRfb3JkZXJfY291bnQgPT0gJydcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGg0Pnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnbm8tZGVsaXZlcmVkLW1zZyddfX08L2g0PlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibm8tcHVyY2hhc2VcIiA6aWQ9XCInY2FuY2VsbGVkX2hkcicrc3RvcmUuaWRcInYtaWY9XCJzdG9yZS5jYW5jZWxsZWRfb3JkZXJfY291bnQgPT0gJydcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGg0Pnt7dGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnbm8tY2FuY2VsbGVkLW9yZGVyLW1zZyddfX08L2g0PlxyXG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibm8tcHVyY2hhc2VcIiA6aWQ9XCIndHJhbnNpdF9oZHInK3N0b3JlLmlkXCJ2LWlmPVwic3RvcmUudHJhbnNpdF9vcmRlcl9jb3VudCA9PSAnJ1wiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8aDQ+e3t0aGlzLiRwYXJlbnQubGFuZy5kYXRhWyduby1pbnRyYW5zaXQtbXNnJ119fTwvaDQ+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8L2Rpdj48IS0tIGVuZCBwYW5lbC1ncm91cC0tPlxyXG5cdFx0XHRcdDwvZGl2PjwhLS0gZW5kIHBhbmVsLWdyb3VwLS0+XHJcblx0XHRcdDwvZGl2PjwhLS0gZW5kIHBhbmVsLWdyb3VwLS0+XHJcblx0XHQ8L2Rpdj5cclxuXHQ8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcblx0aW1wb3J0IG9yZGVyIGZyb20gJy4vc2FsZXNUYWJsZS52dWUnO1xyXG5cdGV4cG9ydCBkZWZhdWx0e1xyXG5cdFx0cHJvcHM6WydzdG9yZScsICdpbmRleCddLFxyXG5cdFx0Y29tcG9uZW50czoge1xyXG5cdFx0ICAgICdvcmRlcnMnOiBvcmRlclxyXG5cdFx0fSxcclxuXHRcdG1ldGhvZHM6e1xyXG5cdFx0XHRmaWx0ZXJTdGF0dXMoaWQsIGZpbHRlcil7XHJcblx0XHRcdFx0aWYoZmlsdGVyID09IFwiQ2FuY2VsbGVkXCIpe1xyXG5cdFx0XHRcdFx0JCgnI2NhbmNlbGxlZF90YmxfJytpZCkuYWRkQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNhbGxfdGJsXycraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjZGVsaXZlcmVkX3RibF8nK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI3BlbmRpbmdfdGJsXycraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjdHJhbnNpdF90YmxfJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNwZW5kaW5nX2hkcicraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjYWxsX2hkcicraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjY2FuY2VsbGVkX2hkcicraWQpLmFkZENsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjZGVsaXZlcmVkX2hkcicraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjdHJhbnNpdF9oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX1BlbmRpbmdfJytpZCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX0NhbmNlbGxlZF8nK2lkKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHQkKCcjZmxfRGVsaXZlcmVkXycraWQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9BbGxfJytpZCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX1RyYW5zaXRfJytpZCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cclxuXHRcdFx0XHR9ZWxzZSBpZihmaWx0ZXIgPT0gXCJQZW5kaW5nXCIpe1xyXG5cdFx0XHRcdFx0JCgnI2NhbmNlbGxlZF90YmxfJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNhbGxfdGJsXycraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjZGVsaXZlcmVkX3RibF8nK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI3BlbmRpbmdfdGJsXycraWQpLmFkZENsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjdHJhbnNpdF90YmxfJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNwZW5kaW5nX2hkcicraWQpLmFkZENsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjYWxsX2hkcicraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjY2FuY2VsbGVkX2hkcicraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjZGVsaXZlcmVkX2hkcicraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjdHJhbnNpdF9oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX1BlbmRpbmdfJytpZCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX0NhbmNlbGxlZF8nK2lkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHQkKCcjZmxfRGVsaXZlcmVkXycraWQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9BbGxfJytpZCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX1RyYW5zaXRfJytpZCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cclxuXHRcdFx0XHR9ZWxzZSBpZihmaWx0ZXIgPT0gXCJEZWxpdmVyZWRcIil7XHJcblx0XHRcdFx0XHQkKCcjY2FuY2VsbGVkX3RibF8nK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2FsbF90YmxfJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNkZWxpdmVyZWRfdGJsXycraWQpLmFkZENsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjcGVuZGluZ190YmxfJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyN0cmFuc2l0X3RibF8nK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI3BlbmRpbmdfaGRyJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNhbGxfaGRyJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNjYW5jZWxsZWRfaGRyJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNkZWxpdmVyZWRfaGRyJytpZCkuYWRkQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyN0cmFuc2l0X2hkcicraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjZmxfUGVuZGluZ18nK2lkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHQkKCcjZmxfQ2FuY2VsbGVkXycraWQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9EZWxpdmVyZWRfJytpZCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX0FsbF8nK2lkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHQkKCcjZmxfVHJhbnNpdF8nK2lkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblxyXG5cdFx0XHRcdH1lbHNlIGlmKGZpbHRlciA9PSBcIkFsbFwiKXtcclxuXHRcdFx0XHRcdCQoJyNjYW5jZWxsZWRfdGJsXycraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjYWxsX3RibF8nK2lkKS5hZGRDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2RlbGl2ZXJlZF90YmxfJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNwZW5kaW5nX3RibF8nK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI3RyYW5zaXRfdGJsXycraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjcGVuZGluZ19oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2FsbF9oZHInK2lkKS5hZGRDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2NhbmNlbGxlZF9oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2RlbGl2ZXJlZF9oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI3RyYW5zaXRfaGRyJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9QZW5kaW5nXycraWQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9DYW5jZWxsZWRfJytpZCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX0RlbGl2ZXJlZF8nK2lkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHQkKCcjZmxfQWxsXycraWQpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9UcmFuc2l0XycraWQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHJcblx0XHRcdFx0fWVsc2UgaWYoZmlsdGVyID09IFwiVHJhbnNpdFwiKXtcclxuXHRcdFx0XHRcdCQoJyNjYW5jZWxsZWRfdGJsXycraWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjYWxsX3RibF8nK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2RlbGl2ZXJlZF90YmxfJytpZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNwZW5kaW5nX3RibF8nK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI3RyYW5zaXRfdGJsXycraWQpLmFkZENsYXNzKCdzaG93Jyk7XHJcblx0XHRcdFx0XHQkKCcjcGVuZGluZ19oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2FsbF9oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2NhbmNlbGxlZF9oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI2RlbGl2ZXJlZF9oZHInK2lkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHRcdFx0JCgnI3RyYW5zaXRfaGRyJytpZCkuYWRkQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9QZW5kaW5nXycraWQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9DYW5jZWxsZWRfJytpZCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0JCgnI2ZsX0RlbGl2ZXJlZF8nK2lkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHQkKCcjZmxfQWxsXycraWQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdCQoJyNmbF9UcmFuc2l0XycraWQpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHJcblx0JChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcclxuXHRcdCQoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIiwgXCIucG5sLXN0b3JlXCIsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHQkKCcjY2FuY2VsbGVkX3RibF8nK3RoaXMuaWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdCQoJyNhbGxfdGJsXycrdGhpcy5pZCkuYWRkQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0JCgnI2RlbGl2ZXJlZF90YmxfJyt0aGlzLmlkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHQkKCcjcGVuZGluZ190YmxfJyt0aGlzLmlkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHQkKCcjdHJhbnNpdF90YmwnK3RoaXMuaWQpLnJlbW92ZUNsYXNzKCdzaG93Jyk7XHJcblx0XHRcdCQoJyNwZW5kaW5nX2hkcicrdGhpcy5pZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0JCgnI2FsbF9oZHInK3RoaXMuaWQpLmFkZENsYXNzKCdzaG93Jyk7XHJcblx0XHRcdCQoJyNjYW5jZWxsZWRfaGRyJyt0aGlzLmlkKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG5cdFx0XHQkKCcjZGVsaXZlcmVkX2hkcicrdGhpcy5pZCkucmVtb3ZlQ2xhc3MoJ3Nob3cnKTtcclxuXHRcdFx0JCgnI2ZsX1BlbmRpbmdfJyt0aGlzLmlkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdCQoJyNmbF9DYW5jZWxsZWRfJyt0aGlzLmlkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdCQoJyNmbF9EZWxpdmVyZWRfJyt0aGlzLmlkKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdCQoJyNmbF9BbGxfJyt0aGlzLmlkKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdCQoJyNmbF9UcmFuc2l0XycrdGhpcy5pZCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0fSk7XHJcblx0fSk7XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBzdG9yZVBhbmVsLnZ1ZT9hMmRkZTFhYyIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL3NhbGVzVGFibGUudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1lNjRiMzA3ZVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9zYWxlc1RhYmxlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxyZXBvcnRzXFxcXHNhbGVzXFxcXHNhbGVzVGFibGUudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gc2FsZXNUYWJsZS52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtZTY0YjMwN2VcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi1lNjRiMzA3ZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcmVwb3J0cy9zYWxlcy9zYWxlc1RhYmxlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzUyXG4vLyBtb2R1bGUgY2h1bmtzID0gMTEiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9zdG9yZVBhbmVsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtYWQyOTBhN2NcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vc3RvcmVQYW5lbC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxccmVwb3J0c1xcXFxzYWxlc1xcXFxzdG9yZVBhbmVsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIHN0b3JlUGFuZWwudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWFkMjkwYTdjXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtYWQyOTBhN2NcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3JlcG9ydHMvc2FsZXMvc3RvcmVQYW5lbC52dWVcbi8vIG1vZHVsZSBpZCA9IDM1M1xuLy8gbW9kdWxlIGNodW5rcyA9IDExIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEyXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtZ3JvdXAgc3RvcmUtcGFuZWxcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtY29sbGFwc2UgY29sbGFwc2VcIixcbiAgICBjbGFzczogeyBpbjogKF92bS5pbmRleCA9PSAwKVxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ3N0b3JlXycgKyBfdm0uc3RvcmUuaWRcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMlwiXG4gIH0sIFtfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibmF2IG5hdi1waWxsc1wiXG4gIH0sIFtfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYmFkZ2UtZGV0YWlsc1wiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6ICdmbF9QZW5kaW5nXycgKyBfdm0uc3RvcmUuaWRcbiAgICB9XG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uZmlsdGVyU3RhdHVzKF92bS5zdG9yZS5pZCwgJ1BlbmRpbmcnKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsncGVuZGluZy1vcmRlciddKSArIFwiXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICBcXHRcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJhZGdlXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLnN0b3JlLnBlbmRpbmdfb3JkZXJfY291bnQubGVuZ3RoKSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJiYWRnZS1kZXRhaWxzXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ2ZsX0RlbGl2ZXJlZF8nICsgX3ZtLnN0b3JlLmlkXG4gICAgfVxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZpbHRlclN0YXR1cyhfdm0uc3RvcmUuaWQsICdEZWxpdmVyZWQnKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnZGVsaXZlcmVkJ10pICsgXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYmFkZ2VcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uc3RvcmUuZGVsaXZlcmVkX29yZGVyX2NvdW50Lmxlbmd0aCkpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYmFkZ2UtZGV0YWlsc1wiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6ICdmbF9DYW5jZWxsZWRfJyArIF92bS5zdG9yZS5pZFxuICAgIH1cbiAgfSwgW19jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5maWx0ZXJTdGF0dXMoX3ZtLnN0b3JlLmlkLCAnQ2FuY2VsbGVkJylcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ2NhbmNlbGxlZC1vcmRlciddKSArIFwiIFxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdCAgXFx0XCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJiYWRnZVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5zdG9yZS5jYW5jZWxsZWRfb3JkZXJfY291bnQubGVuZ3RoKSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJiYWRnZS1kZXRhaWxzXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ2ZsX1RyYW5zaXRfJyArIF92bS5zdG9yZS5pZFxuICAgIH1cbiAgfSwgW19jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5maWx0ZXJTdGF0dXMoX3ZtLnN0b3JlLmlkLCAnVHJhbnNpdCcpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydpbi10cmFuc2l0J10pICsgXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQgIFxcdFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYmFkZ2VcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uc3RvcmUudHJhbnNpdF9vcmRlcl9jb3VudC5sZW5ndGgpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImFjdGl2ZSBiYWRnZS1kZXRhaWxzXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ2ZsX0FsbF8nICsgX3ZtLnN0b3JlLmlkXG4gICAgfVxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmZpbHRlclN0YXR1cyhfdm0uc3RvcmUuaWQsICdBbGwnKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnYWxsJ10pICsgXCIgXFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0ICBcXHRcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJhZGdlXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLnN0b3JlLm9yZGVyX2RldGFpbHMubGVuZ3RoKSldKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5zdG9yZS5vcmRlcl9kZXRhaWxzICE9ICcnKSA/IF9jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1zdHJpcGVkIGZpbHRlci10YWJsZSBzYWxlcy10Ymwgc2hvd1wiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6ICdhbGxfdGJsXycgKyBfdm0uc3RvcmUuaWRcbiAgICB9XG4gIH0sIFtfYygndGhlYWQnLCBbX2MoJ3RyJywgW19jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndHJhbnNhY3Rpb24nXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3Byb2R1Y3QnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3F0eSddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndW5pdC1wcmljZSddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndG90YWwnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnc3RhdHVzJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiXG4gIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0uc3RvcmUub3JkZXJfZGV0YWlscyksIGZ1bmN0aW9uKG9yZGVyKSB7XG4gICAgcmV0dXJuIF9jKCdvcmRlcnMnLCB7XG4gICAgICBrZXk6IG9yZGVyLmlkLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJvcmRlclwiOiBvcmRlclxuICAgICAgfVxuICAgIH0pXG4gIH0pKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uc3RvcmUucGVuZGluZ19vcmRlcl9jb3VudCAhPSAnJykgPyBfYygndGFibGUnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGFibGUgdGFibGUtc3RyaXBlZCBmaWx0ZXItdGFibGUgc2FsZXMtdGJsXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ3BlbmRpbmdfdGJsXycgKyBfdm0uc3RvcmUuaWRcbiAgICB9XG4gIH0sIFtfYygndGhlYWQnLCBbX2MoJ3RyJywgW19jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndHJhbnNhY3Rpb24nXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3Byb2R1Y3QnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3F0eSddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndW5pdC1wcmljZSddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndG90YWwnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnc3RhdHVzJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiXG4gIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0uc3RvcmUucGVuZGluZ19vcmRlcl9jb3VudCksIGZ1bmN0aW9uKG9yZGVyKSB7XG4gICAgcmV0dXJuIF9jKCdvcmRlcnMnLCB7XG4gICAgICBrZXk6IG9yZGVyLmlkLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJvcmRlclwiOiBvcmRlclxuICAgICAgfVxuICAgIH0pXG4gIH0pKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uc3RvcmUuZGVsaXZlcmVkX29yZGVyX2NvdW50ICE9ICcnKSA/IF9jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1zdHJpcGVkIGZpbHRlci10YWJsZSBzYWxlcy10YmxcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiAnZGVsaXZlcmVkX3RibF8nICsgX3ZtLnN0b3JlLmlkXG4gICAgfVxuICB9LCBbX2MoJ3RoZWFkJywgW19jKCd0cicsIFtfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RyYW5zYWN0aW9uJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIlxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydwcm9kdWN0J10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIlxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydxdHknXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMSByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3VuaXQtcHJpY2UnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMSByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RvdGFsJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3N0YXR1cyddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXJcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXJcIlxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0Ym9keScsIF92bS5fbCgoX3ZtLnN0b3JlLmRlbGl2ZXJlZF9vcmRlcl9jb3VudCksIGZ1bmN0aW9uKG9yZGVyKSB7XG4gICAgcmV0dXJuIF9jKCdvcmRlcnMnLCB7XG4gICAgICBrZXk6IG9yZGVyLmlkLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJvcmRlclwiOiBvcmRlclxuICAgICAgfVxuICAgIH0pXG4gIH0pKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uc3RvcmUuY2FuY2VsbGVkX29yZGVyX2NvdW50ICE9ICcnKSA/IF9jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1zdHJpcGVkIGZpbHRlci10YWJsZSBzYWxlcy10YmxcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiAnY2FuY2VsbGVkX3RibF8nICsgX3ZtLnN0b3JlLmlkXG4gICAgfVxuICB9LCBbX2MoJ3RoZWFkJywgW19jKCd0cicsIFtfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RyYW5zYWN0aW9uJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIlxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydwcm9kdWN0J10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgcmVwb3J0LWhlYWRlciBoaWRkZW4teHNcIlxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZy5kYXRhWydxdHknXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMSByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3VuaXQtcHJpY2UnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMSByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3RvdGFsJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTIgcmVwb3J0LWhlYWRlclwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3N0YXR1cyddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXJcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXJcIlxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0Ym9keScsIF92bS5fbCgoX3ZtLnN0b3JlLmNhbmNlbGxlZF9vcmRlcl9jb3VudCksIGZ1bmN0aW9uKG9yZGVyKSB7XG4gICAgcmV0dXJuIF9jKCdvcmRlcnMnLCB7XG4gICAgICBrZXk6IG9yZGVyLmlkLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJvcmRlclwiOiBvcmRlclxuICAgICAgfVxuICAgIH0pXG4gIH0pKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uc3RvcmUudHJhbnNpdF9vcmRlcl9jb3VudCAhPSAnJykgPyBfYygndGFibGUnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGFibGUgdGFibGUtc3RyaXBlZCBmaWx0ZXItdGFibGUgc2FsZXMtdGJsXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ3RyYW5zaXRfdGJsXycgKyBfdm0uc3RvcmUuaWRcbiAgICB9XG4gIH0sIFtfYygndGhlYWQnLCBbX2MoJ3RyJywgW19jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndHJhbnNhY3Rpb24nXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3Byb2R1Y3QnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nLmRhdGFbJ3F0eSddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndW5pdC1wcmljZSddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xIHJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsndG90YWwnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMiByZXBvcnQtaGVhZGVyXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnc3RhdHVzJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEgcmVwb3J0LWhlYWRlclwiXG4gIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0uc3RvcmUudHJhbnNpdF9vcmRlcl9jb3VudCksIGZ1bmN0aW9uKG9yZGVyKSB7XG4gICAgcmV0dXJuIF9jKCdvcmRlcnMnLCB7XG4gICAgICBrZXk6IG9yZGVyLmlkLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJvcmRlclwiOiBvcmRlclxuICAgICAgfVxuICAgIH0pXG4gIH0pKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uc3RvcmUub3JkZXJfZGV0YWlscyA9PSAnJykgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm5vLXB1cmNoYXNlIHNob3dcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiAnYWxsX2hkcicgKyBfdm0uc3RvcmUuaWRcbiAgICB9XG4gIH0sIFtfYygnaDQnLCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZy5kYXRhWyduby1wdXJjaGFzZXMtbXNnJ10pKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5zdG9yZS5wZW5kaW5nX29yZGVyX2NvdW50ID09ICcnKSA/IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibm8tcHVyY2hhc2VcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiAncGVuZGluZ19oZHInICsgX3ZtLnN0b3JlLmlkXG4gICAgfVxuICB9LCBbX2MoJ2g0JywgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnbm8tcGVuZGluZy1vcmRlci1tc2cnXSkpXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnN0b3JlLmRlbGl2ZXJlZF9vcmRlcl9jb3VudCA9PSAnJykgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm5vLXB1cmNoYXNlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ2RlbGl2ZXJlZF9oZHInICsgX3ZtLnN0b3JlLmlkXG4gICAgfVxuICB9LCBbX2MoJ2g0JywgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnbm8tZGVsaXZlcmVkLW1zZyddKSldKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uc3RvcmUuY2FuY2VsbGVkX29yZGVyX2NvdW50ID09ICcnKSA/IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibm8tcHVyY2hhc2VcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiAnY2FuY2VsbGVkX2hkcicgKyBfdm0uc3RvcmUuaWRcbiAgICB9XG4gIH0sIFtfYygnaDQnLCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZy5kYXRhWyduby1jYW5jZWxsZWQtb3JkZXItbXNnJ10pKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5zdG9yZS50cmFuc2l0X29yZGVyX2NvdW50ID09ICcnKSA/IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibm8tcHVyY2hhc2VcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiAndHJhbnNpdF9oZHInICsgX3ZtLnN0b3JlLmlkXG4gICAgfVxuICB9LCBbX2MoJ2g0JywgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmcuZGF0YVsnbm8taW50cmFuc2l0LW1zZyddKSldKV0pIDogX3ZtLl9lKCldKV0pXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1hZDI5MGE3Y1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWFkMjkwYTdjXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcmVwb3J0cy9zYWxlcy9zdG9yZVBhbmVsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzk3XG4vLyBtb2R1bGUgY2h1bmtzID0gMTEiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3RyJywgW19jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyZXBvcnQtaGVhZGVyXCJcbiAgfSwgW19jKCdwJywgW19jKCdiJywgW192bS5fdihfdm0uX3MoX3ZtLm9yZGVyLnRyYWNraW5nX251bWJlcikpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3AnLCBbX3ZtLl92KF92bS5fcyhfdm0ub3JkZXIuY3JlYXRlZF9hdCkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm9yZGVyLnByb2R1Y3QubmFtZSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5vcmRlci5xdWFudGl0eSkpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyZXBvcnQtaGVhZGVyIGhpZGRlbi14c1wiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKF92bS5ub3cpKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJlcG9ydC1oZWFkZXIgaGlkZGVuLXhzXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm9yZGVyLnN1YnRvdGFsKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJlcG9ydC1oZWFkZXJcIlxuICB9LCBbKChfdm0ub3JkZXIuc3RhdHVzICE9ICdSZWNlaXZlZCcpICYmIChfdm0ub3JkZXIuc3RhdHVzICE9ICdEZWxpdmVyZWQnKSAmJiAoX3ZtLm9yZGVyLnN0YXR1cyAhPSAnUmV0dXJuZWQnKSAmJiAoX3ZtLm9yZGVyLnN0YXR1cyAhPSAnSW4gVHJhbnNpdCcpICYmIChfdm0ub3JkZXIuc3RhdHVzICE9ICdDYW5jZWxsZWQnKSkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0bi1ncm91cFwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBidG4tcmFpc2VkIGJ0bi1zbVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogX3ZtLnN0YXR1c0FjdGlvblxuICAgIH1cbiAgfSwgWygnSW4gVHJhbnNpdCcgPT0gX3ZtLm9yZGVyLnN0YXR1cykgPyBfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsnaW4tdHJhbnNpdCddKSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoJ0NhbmNlbGxlZCcgPT0gX3ZtLm9yZGVyLnN0YXR1cykgPyBfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsnY2FuY2VsbGVkJ10pKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksICgnUGVuZGluZycgPT0gX3ZtLm9yZGVyLnN0YXR1cykgPyBfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsncGVuZGluZyddKSldKSA6IF92bS5fZSgpXSksIF92bS5fdihcIiBcIiksIF92bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3VsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLW1lbnVcIlxuICB9LCBbKCdJbiBUcmFuc2l0JyAhPSBfdm0ub3JkZXIuc3RhdHVzKSA/IF9jKCdsaScsIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0udXBkYXRlU3RhdHVzKCdJbiBUcmFuc2l0JylcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC4kcGFyZW50LmxhbmcuZGF0YVsnaW4tdHJhbnNpdCddKSldKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksICgnQ2FuY2VsbGVkJyAhPSBfdm0ub3JkZXIuc3RhdHVzKSA/IF9jKCdsaScsIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0udXBkYXRlU3RhdHVzKCdDYW5jZWxsZWQnKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydjYW5jZWxsZWQnXSkpXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoJ1BlbmRpbmcnICE9IF92bS5vcmRlci5zdGF0dXMpID8gX2MoJ2xpJywgW19jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS51cGRhdGVTdGF0dXMoJ1BlbmRpbmcnKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydwZW5kaW5nJ10pKV0pXSkgOiBfdm0uX2UoKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKChfdm0ub3JkZXIuc3RhdHVzID09ICdSZXR1cm5lZCcgJiYgX3ZtLm9yZGVyLnJldHVybmVkX3JlY2VpdmVkX2RhdGUgPT0gbnVsbCkpID8gX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXhzIGJ0bi1yYWlzZWQgYnRuLXdhcm5pbmdcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0aXRsZVwiOiB0aGlzLiRwYXJlbnQuJHBhcmVudC5sYW5nLmRhdGFbJ3JlY2VpdmVkLXJldHVybmVkLW1zZy0yJ10sXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnJlY2VpdmVkQnlTZWxsZXIoX3ZtLm9yZGVyLmlkKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWV4Y2xhbWF0aW9uXCJcbiAgfSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoKF92bS5vcmRlci5zdGF0dXMgPT0gJ0luIFRyYW5zaXQnKSB8fCAoX3ZtLm9yZGVyLnN0YXR1cyA9PSAnUmVjZWl2ZWQnKSB8fCAoX3ZtLm9yZGVyLnN0YXR1cyA9PSAnRGVsaXZlcmVkJykgfHwgKF92bS5vcmRlci5zdGF0dXMgPT0gJ1JldHVybmVkJykgfHwgKF92bS5vcmRlci5zdGF0dXMgPT0gJ0NhbmNlbGxlZCcpKSA/IF9jKCdzcGFuJywgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhW3RoaXMub3JkZXJzdGF0dXNdKSldKSA6IF92bS5fZSgpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnYScsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJjdXJzb3JcIjogXCJwb2ludGVyXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0udmlld0RldGFpbHMoX3ZtLm9yZGVyLmlkKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImhpZGRlbi14cyBoaWRkZW4tc21cIlxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQuJHBhcmVudC5sYW5nLmRhdGFbJ3NlZS1tb3JlJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX20oMSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbKChfdm0ub3JkZXIuc3RhdHVzICE9ICdQZW5kaW5nJykpID8gX2MoJ2EnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiY3Vyc29yXCI6IFwicG9pbnRlclwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnZpZXdBaXJ3YXlCaWxsKF92bS5vcmRlci5pZClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoaWRkZW4teHMgaGlkZGVuLXNtXCJcbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZy5kYXRhWydhaXJ3YXktYmlsbCddKSldKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl9tKDIpXSkgOiBfdm0uX2UoKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBidG4tcmFpc2VkIGJ0bi1zbSBkcm9wZG93bi10b2dnbGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJ0eXBlXCI6IFwiYnV0dG9uXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwiZHJvcGRvd25cIixcbiAgICAgIFwiYXJpYS1oYXNwb3B1cFwiOiBcInRydWVcIixcbiAgICAgIFwiYXJpYS1leHBhbmRlZFwiOiBcImZhbHNlXCJcbiAgICB9XG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjYXJldFwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzci1vbmx5XCJcbiAgfSwgW192bS5fdihcIlRvZ2dsZSBEcm9wZG93blwiKV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJoaWRkZW4tbGcgaGlkZGVuLW1kXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcImRhdGEtcGxhY2VtZW50XCI6IFwibGVmdFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIlwiLFxuICAgICAgXCJkYXRhLW9yaWdpbmFsLXRpdGxlXCI6IFwiU2VlIE1vcmVcIlxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWV5ZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImFyaWEtaGlkZGVuXCI6IFwidHJ1ZVwiXG4gICAgfVxuICB9KV0pXG59LGZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaGlkZGVuLWxnIGhpZGRlbi1tZFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcImxlZnRcIixcbiAgICAgIFwidGl0bGVcIjogXCJcIixcbiAgICAgIFwiZGF0YS1vcmlnaW5hbC10aXRsZVwiOiBcIkFpcndheSBCaWxsXCJcbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1maWxlLXRleHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJhcmlhLWhpZGRlblwiOiBcInRydWVcIlxuICAgIH1cbiAgfSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWU2NGIzMDdlXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtZTY0YjMwN2VcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9yZXBvcnRzL3NhbGVzL3NhbGVzVGFibGUudnVlXG4vLyBtb2R1bGUgaWQgPSA0MDVcbi8vIG1vZHVsZSBjaHVua3MgPSAxMSJdLCJzb3VyY2VSb290IjoiIn0=