/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 440);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['item'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    },
    addToCart: function addToCart() {
      this.$parent.addToCart(this.item.id, this.item.name);
    },
    addToCompare: function addToCompare() {
      this.$parent.addToCompare(this.item.id, this.item.name);
    },
    addToFavorites: function addToFavorites() {
      this.$parent.addToFavorites(this.item.id, this.item.name);
    }
  },
  computed: {
    price: function price() {
      if (this.item.fee_included == 1) return parseFloat(this.item.price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.price;
    },
    saleprice: function saleprice() {
      if (this.item.fee_included == 1) return parseFloat(this.item.sale_price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.sale_price;
    },
    namelength: function namelength() {
      var m = $("meta[name=locale-lang]");
      var lang = m.attr("content");
      var prodname = this.item.name;
      if (lang == "cn") {
        if (this.item.cn_name) {
          prodname = this.item.cn_name;
        } else {
          prodname = this.item.name;
        }
      }

      if (prodname.length > 38) {
        var name = prodname.substr(0, 38) + "...";
        return name;
      } else {
        return prodname;
      }
    },
    starRating: function starRating() {
      return "width:" + Math.round(this.item.rate / this.item.count * 20) + "%";
    },
    productRating: function productRating() {
      return "width:" + this.item.product_rating + "%";
    }
  }
});

/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('product-item', __webpack_require__(35));

Vue.filter('currency', function (value) {
				return numberWithCommas(value.toFixed(2));
});

Vue.filter('round', function (value) {
				return roundOff(value);
});

var app = new Vue({
				el: '#for_men',
				data: {
								items: [],
								selected: [],
								lang: []
				},
				created: function created() {
								var _this = this;

								function getTranslation() {
												return axios.get('/translate/cart');
								}
								function getProducts() {
												return axiosAPIv1.get('/formen');
								}
								axios.all([getTranslation(), getProducts()]).then(axios.spread(function (translation, response) {
												_this.lang = translation.data.data;

												if (response.data) {
																$('.flexhide1').remove();
																$('#for_men').show();
																_this.items = response.data;
												}

												if (_this.items.length > 10) {
																$('#see-all-for-men').removeClass('hide');
												} else {
																$('#see-all-for-men').addClass('hide');
												}
								})).catch($.noop);
				},
				updated: function updated() {
								// Fired every second, should always be true
								$('.product-slider').owlCarousel({
												items: 1,
												dots: false,
												nav: false,
												mouseDrag: false,
												touchDrag: false,
												pullDrag: false,
												freeDrag: false,
												navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
												responsive: {
																0: {
																				items: 2
																},
																480: {
																				items: 2
																},
																768: {
																				items: 2
																},
																992: {
																				items: 5
																},
																1200: {
																				items: 5
																}
												}
								});
				},

				methods: {
								selectItem: function selectItem(id) {
												this.selected.push(id);
								},
								addToCart: function addToCart(id, name) {
												var _this2 = this;

												axios.get(location.origin + '/cart/add', {
																params: {
																				prod_id: id,
																				qty: 1
																}
												}).then(function (result) {
																$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
																if (result.data.error_msg) {
																				if (result.data.error_msg == 'Redirect') window.location = '/product/' + result.data.slug;else toastr.error(result.data.error_msg, _this2.lang['not-added']);
																} else {
																				toastr.success(_this2.lang['added-cart'], name);
																}
												});
								},
								addToCompare: function addToCompare(id, name) {
												var _this3 = this;

												if (Laravel.isAuthenticated === true) {
																axiosAPIv1.get('/addtocompare/' + id).then(function (result) {
																				if (result.data.response == 1) {
																								var comp = parseInt($("#comp-count").text()) + 1;
																								$("#comp-count").text(comp).show();
																								toastr.success(_this3.lang['added-comparison'], name);
																				} else if (result.data.response == 3) {
																								toastr.error(_this3.lang['full']);
																				} else {
																								toastr.info(_this3.lang['already-compare'], name);
																				}
																});
												} else {
																toastr.error(this.lang['login-compare']);
												}
								},
								addToFavorites: function addToFavorites(id, name) {
												var _this4 = this;

												if (Laravel.isAuthenticated === true) {
																axiosAPIv1.get('/addtofavorites/' + id).then(function (result) {
																				if (result.data.response == 1) {
																								var fav = parseInt($("#fav-count").text()) + 1;
																								$("#fav-count").text(fav).show();
																								toastr.success(_this4.lang['added-favorites'], name);
																				} else if (result.data.response == 2) {
																								toastr.error(result.data.error_msg, _this4.lang['not-added-wishlist']);
																				} else {
																								toastr.info(_this4.lang['already-favorites'], name);
																				}
																});
												} else {
																toastr.error(this.lang['login-favorites']);
												}
								}
				}
});

var app = new Vue({
				el: '#for_women',
				data: {
								items: [],
								selected: [],
								lang: []
				},
				created: function created() {
								var _this5 = this;

								function getTranslation() {
												return axios.get('/translate/cart');
								}
								function getProducts() {
												return axiosAPIv1.get('/forwomen');
								}
								axios.all([getTranslation(), getProducts()]).then(axios.spread(function (translation, response) {
												_this5.lang = translation.data.data;

												if (response.data) {
																$('.flexhide2').remove();
																$('#for_women').show();
																_this5.items = response.data;
												}

												if (_this5.items.length > 10) {
																$('#see-all-for-women').removeClass('hide');
												} else {
																$('#see-all-for-women').addClass('hide');
												}
								})).catch($.noop);
				},
				updated: function updated() {
								// Fired every second, should always be true
								// console.log(this.items);
								$('.forwomen-slider').owlCarousel({
												items: 1,
												dots: false,
												nav: false,
												mouseDrag: false,
												touchDrag: false,
												pullDrag: false,
												freeDrag: false,
												navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
												responsive: {
																0: {
																				items: 2
																},
																480: {
																				items: 2
																},
																768: {
																				items: 2
																},
																992: {
																				items: 5
																},
																1200: {
																				items: 5
																}
												}
								});
				},


				methods: {
								selectItem: function selectItem(id) {
												this.selected.push(id);
								},
								addToCart: function addToCart(id, name) {
												var _this6 = this;

												axios.get(location.origin + '/cart/add', {
																params: {
																				prod_id: id,
																				qty: 1
																}
												}).then(function (result) {
																$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
																if (result.data.error_msg) {
																				if (result.data.error_msg == 'Redirect') window.location = '/product/' + result.data.slug;else toastr.error(result.data.error_msg, _this6.lang['not-added']);
																} else {
																				toastr.success(_this6.lang['added-cart'], name);
																}
												});
								},
								addToCompare: function addToCompare(id, name) {
												var _this7 = this;

												if (Laravel.isAuthenticated === true) {
																axiosAPIv1.get('/addtocompare/' + id).then(function (result) {
																				if (result.data.response == 1) {
																								var comp = parseInt($("#comp-count").text()) + 1;
																								$("#comp-count").text(comp).show();
																								toastr.success(_this7.lang['added-comparison'], name);
																				} else if (result.data.response == 3) {
																								toastr.error(_this7.lang['full']);
																				} else {
																								toastr.info(_this7.lang['already-compare'], name);
																				}
																});
												} else {
																toastr.error(this.lang['login-compare']);
												}
								},
								addToFavorites: function addToFavorites(id, name) {
												var _this8 = this;

												if (Laravel.isAuthenticated === true) {
																axiosAPIv1.get('/addtofavorites/' + id).then(function (result) {
																				if (result.data.response == 1) {
																								var fav = parseInt($("#fav-count").text()) + 1;
																								$("#fav-count").text(fav).show();
																								toastr.success(_this8.lang['added-favorites'], name);
																				} else if (result.data.response == 2) {
																								toastr.error(result.data.error_msg, _this8.lang['not-added-wishlist']);
																				} else {
																								toastr.info(_this8.lang['already-favorites'], name);
																				}
																});
												} else {
																toastr.error(this.lang['login-favorites']);
												}
								}
				}
});

var app = new Vue({
				el: '#for_cyber',
				data: {
								items: [],
								selected: [],
								lang: []
				},
				created: function created() {
								var _this9 = this;

								function getTranslation() {
												return axios.get('/translate/cart');
								}
								function getProducts() {
												return axiosAPIv1.get('/forcyber');
								}
								axios.all([getTranslation(), getProducts()]).then(axios.spread(function (translation, response) {
												_this9.lang = translation.data.data;

												if (response.data) {
																$('.flexhide3').remove();
																$('#for_cyber').show();
																_this9.items = response.data;
												}

												if (_this9.items.length > 10) {
																$('#see-all-cyber-zone').removeClass('hide');
												} else {
																$('#see-all-cyber-zone').addClass('hide');
												}
								})).catch($.noop);
				},
				updated: function updated() {
								// Fired every second, should always be true
								// console.log(this.items);
								$('.forcyber-slider').owlCarousel({
												items: 1,
												dots: false,
												nav: false,
												mouseDrag: false,
												touchDrag: false,
												pullDrag: false,
												freeDrag: false,
												navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
												responsive: {
																0: {
																				items: 2
																},
																480: {
																				items: 2
																},
																768: {
																				items: 2
																},
																992: {
																				items: 5
																},
																1200: {
																				items: 5
																}
												}
								});
				},

				methods: {
								selectItem: function selectItem(id) {
												this.selected.push(id);
								},
								addToCart: function addToCart(id, name) {
												var _this10 = this;

												axios.get(location.origin + '/cart/add', {
																params: {
																				prod_id: id,
																				qty: 1
																}
												}).then(function (result) {
																$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
																if (result.data.error_msg) {
																				if (result.data.error_msg == 'Redirect') window.location = '/product/' + result.data.slug;else toastr.error(result.data.error_msg, _this10.lang['not-added']);
																} else {
																				toastr.success(_this10.lang['added-cart'], name);
																}
												});
								},
								addToCompare: function addToCompare(id, name) {
												var _this11 = this;

												if (Laravel.isAuthenticated === true) {
																axiosAPIv1.get('/addtocompare/' + id).then(function (result) {
																				if (result.data.response == 1) {
																								var comp = parseInt($("#comp-count").text()) + 1;
																								$("#comp-count").text(comp).show();
																								toastr.success(_this11.lang['added-comparison'], name);
																				} else if (result.data.response == 3) {
																								toastr.error(_this11.lang['full']);
																				} else {
																								toastr.info(_this11.lang['already-compare'], name);
																				}
																});
												} else {
																toastr.error(this.lang['login-compare']);
												}
								},
								addToFavorites: function addToFavorites(id, name) {
												var _this12 = this;

												if (Laravel.isAuthenticated === true) {
																axiosAPIv1.get('/addtofavorites/' + id).then(function (result) {
																				if (result.data.response == 1) {
																								var fav = parseInt($("#fav-count").text()) + 1;
																								$("#fav-count").text(fav).show();
																								toastr.success(_this12.lang['added-favorites'], name);
																				} else if (result.data.response == 2) {
																								toastr.error(result.data.error_msg, _this12.lang['not-added-wishlist']);
																				} else {
																								toastr.info(_this12.lang['already-favorites'], name);
																				}
																});
												} else {
																toastr.error(this.lang['login-favorites']);
												}
								}
				}
});

function numberWithCommas(x) {
				return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
				return Math.round(v);
}

/***/ }),

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(19),
  /* template */
  __webpack_require__(39),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\common\\Product.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Product.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-503845d2", Component.options)
  } else {
    hotAPI.reload("data-v-503845d2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "box-product-outer",
    staticStyle: {
      "margin": "0 auto"
    }
  }, [_c('div', {
    staticClass: "box-product"
  }, [_c('div', {
    staticClass: "img-wrapper",
    staticStyle: {
      "height": "auto"
    }
  }, [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_c('img', {
    staticClass: "imgthumb",
    attrs: {
      "alt": "Product",
      "src": '/shopping/images/product/' + _vm.item.id + '/primary.jpg'
    }
  })]), _vm._v(" "), (_vm.item.sale_price) ? _c('div', {
    staticClass: "tags"
  }, [_c('span', {
    staticClass: "label-tags"
  }, [_c('span', {
    staticClass: "label label-danger arrowed"
  }, [_vm._v(_vm._s(_vm.item.discount) + "%")])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "option"
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "data-placement": "top",
      "title": "Add to Cart",
      "data-original-title": "Add to Cart"
    },
    on: {
      "click": function($event) {
        _vm.addToCart()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-shopping-cart"
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Compare"
    },
    on: {
      "click": function($event) {
        _vm.addToCompare()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-align-left"
  })]), _vm._v(" "), _c('a', {
    staticClass: "wishlist",
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Wishlist"
    },
    on: {
      "click": function($event) {
        _vm.addToFavorites()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-heart"
  })])])]), _vm._v(" "), _c('h6', [_c('a', {
    staticClass: "product-name",
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_vm._v("\n        " + _vm._s(_vm.namelength) + "\n      ")])]), _vm._v(" "), _c('div', {
    staticClass: "price",
    staticStyle: {
      "margin-top": "-5px",
      "font-size": "14px"
    }
  }, [_c('div', [(_vm.item.discount && _vm.item.discount > 0) ? _c('span', [_vm._v("P" + _vm._s(_vm._f("currency")(parseFloat("0" + _vm.saleprice))))]) : _vm._e(), _vm._v(" "), _c('div', {
    staticStyle: {
      "display": "block"
    }
  }, [_c('span', {
    class: (_vm.item.discount && _vm.item.discount > 0) ? 'price-old' : '',
    staticStyle: {
      "margin-left": "0px"
    }
  }, [_vm._v("\n            P" + _vm._s(_vm._f("currency")(parseFloat("0" + _vm.price))) + "\n          ")]), _vm._v(" "), (_vm.item.discount == 0) ? _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }) : _vm._e(), _vm._v(" "), (_vm.item.discount && _vm.item.discount > 0) ? _c('span', {
    staticStyle: {
      "font-size": "13px",
      "color": "#666"
    }
  }, [_vm._v("-" + _vm._s(_vm.item.discount) + "%")]) : _vm._e()])])]), _vm._v(" "), _c('div', {
    staticClass: "rating-block"
  }, [_c('div', {
    staticClass: "star-ratings-sprite-xsmall"
  }, [_c('span', {
    staticClass: "star-ratings-sprite-xsmall-rating",
    style: (_vm.productRating)
  })])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-503845d2", module.exports)
  }
}

/***/ }),

/***/ 440:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(190);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy9Qcm9kdWN0LnZ1ZT85MDVhKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3Byb2R1Y3QuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NvbW1vbi9Qcm9kdWN0LnZ1ZT8xYTQ0Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9jb21tb24vUHJvZHVjdC52dWU/NWE1MioiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImZpbHRlciIsInZhbHVlIiwibnVtYmVyV2l0aENvbW1hcyIsInRvRml4ZWQiLCJyb3VuZE9mZiIsImFwcCIsImVsIiwiZGF0YSIsIml0ZW1zIiwic2VsZWN0ZWQiLCJsYW5nIiwiY3JlYXRlZCIsImdldFRyYW5zbGF0aW9uIiwiYXhpb3MiLCJnZXQiLCJnZXRQcm9kdWN0cyIsImF4aW9zQVBJdjEiLCJhbGwiLCJ0aGVuIiwic3ByZWFkIiwidHJhbnNsYXRpb24iLCJyZXNwb25zZSIsIiQiLCJyZW1vdmUiLCJzaG93IiwibGVuZ3RoIiwicmVtb3ZlQ2xhc3MiLCJhZGRDbGFzcyIsImNhdGNoIiwibm9vcCIsInVwZGF0ZWQiLCJvd2xDYXJvdXNlbCIsImRvdHMiLCJuYXYiLCJtb3VzZURyYWciLCJ0b3VjaERyYWciLCJwdWxsRHJhZyIsImZyZWVEcmFnIiwibmF2VGV4dCIsInJlc3BvbnNpdmUiLCJtZXRob2RzIiwic2VsZWN0SXRlbSIsImlkIiwicHVzaCIsImFkZFRvQ2FydCIsIm5hbWUiLCJsb2NhdGlvbiIsIm9yaWdpbiIsInBhcmFtcyIsInByb2RfaWQiLCJxdHkiLCJ0ZXh0IiwicmVzdWx0IiwiY2FydF9pdGVtX2NvdW50IiwiZXJyb3JfbXNnIiwid2luZG93Iiwic2x1ZyIsInRvYXN0ciIsImVycm9yIiwic3VjY2VzcyIsImFkZFRvQ29tcGFyZSIsIkxhcmF2ZWwiLCJpc0F1dGhlbnRpY2F0ZWQiLCJjb21wIiwicGFyc2VJbnQiLCJpbmZvIiwiYWRkVG9GYXZvcml0ZXMiLCJmYXYiLCJ4IiwidG9TdHJpbmciLCJyZXBsYWNlIiwidiIsIk1hdGgiLCJyb3VuZCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1RBO1VBRUE7O2dDQUVBOzhCQUNBO0FBQ0E7b0NBQ0E7cURBQ0E7QUFDQTswQ0FDQTt3REFDQTtBQUNBOzhDQUNBOzBEQUNBO0FBRUE7QUFiQTs7NEJBZUE7b0NBQ0EsaUlBRUEsMkJBQ0E7QUFDQTtvQ0FDQTtvQ0FDQSxzSUFFQSwyQkFDQTtBQUNBO3NDQUNBO2dCQUNBO3dCQUNBOytCQUNBO3dCQUNBOytCQUNBOytCQUNBO0FBQ0EsZUFDQTsrQkFDQTtBQUNBO0FBRUE7O2dDQUNBOzRDQUNBO2VBQ0E7QUFDQSxhQUNBO2VBQ0E7QUFDQTtBQUNBO3NDQUNBOzRFQUNBO0FBQ0E7NENBQ0E7bURBQ0E7QUFHQTtBQXpDQTtBQWhCQSxHOzs7Ozs7O0FDMUNBQSxJQUFJQyxTQUFKLENBQWMsY0FBZCxFQUErQixtQkFBQUMsQ0FBUSxFQUFSLENBQS9COztBQUVBRixJQUFJRyxNQUFKLENBQVcsVUFBWCxFQUF1QixVQUFVQyxLQUFWLEVBQWlCO0FBQ3BDLFdBQVFDLGlCQUFpQkQsTUFBTUUsT0FBTixDQUFjLENBQWQsQ0FBakIsQ0FBUjtBQUNILENBRkQ7O0FBSUFOLElBQUlHLE1BQUosQ0FBVyxPQUFYLEVBQW9CLFVBQVVDLEtBQVYsRUFBaUI7QUFDakMsV0FBUUcsU0FBU0gsS0FBVCxDQUFSO0FBQ0gsQ0FGRDs7QUFJQSxJQUFJSSxNQUFNLElBQUlSLEdBQUosQ0FBUTtBQUNqQlMsUUFBSSxVQURhO0FBRWpCQyxVQUFNO0FBQ0xDLGVBQU0sRUFERDtBQUdMQyxrQkFBUyxFQUhKO0FBSUxDLGNBQUs7QUFKQSxLQUZXO0FBUWpCQyxXQVJpQixxQkFRUjtBQUFBOztBQUNSLGlCQUFTQyxjQUFULEdBQTBCO0FBQ2hCLG1CQUFPQyxNQUFNQyxHQUFOLENBQVUsaUJBQVYsQ0FBUDtBQUNIO0FBQ1AsaUJBQVNDLFdBQVQsR0FBdUI7QUFDdEIsbUJBQU9DLFdBQVdGLEdBQVgsQ0FBZSxTQUFmLENBQVA7QUFDQTtBQUNFRCxjQUFNSSxHQUFOLENBQVUsQ0FDVEwsZ0JBRFMsRUFFWkcsYUFGWSxDQUFWLEVBR0FHLElBSEEsQ0FHS0wsTUFBTU0sTUFBTixDQUNQLFVBQ0NDLFdBREQsRUFFQ0MsUUFGRCxFQUdLO0FBQ0osa0JBQUtYLElBQUwsR0FBWVUsWUFBWWIsSUFBWixDQUFpQkEsSUFBN0I7O0FBRUEsZ0JBQUdjLFNBQVNkLElBQVosRUFBaUI7QUFDaEJlLGtCQUFFLFlBQUYsRUFBZ0JDLE1BQWhCO0FBQ0FELGtCQUFFLFVBQUYsRUFBY0UsSUFBZDtBQUNBLHNCQUFLaEIsS0FBTCxHQUFhYSxTQUFTZCxJQUF0QjtBQUNBOztBQUVELGdCQUFHLE1BQUtDLEtBQUwsQ0FBV2lCLE1BQVgsR0FBb0IsRUFBdkIsRUFBMkI7QUFDMUJILGtCQUFFLGtCQUFGLEVBQXNCSSxXQUF0QixDQUFrQyxNQUFsQztBQUNBLGFBRkQsTUFFTztBQUNOSixrQkFBRSxrQkFBRixFQUFzQkssUUFBdEIsQ0FBK0IsTUFBL0I7QUFDQTtBQUNGLFNBbEJPLENBSEwsRUFzQkZDLEtBdEJFLENBc0JJTixFQUFFTyxJQXRCTjtBQXVCSCxLQXRDZ0I7QUF1Q2pCQyxXQXZDaUIscUJBdUNQO0FBQ047QUFDQVIsVUFBRSxpQkFBRixFQUFxQlMsV0FBckIsQ0FBaUM7QUFDL0J2QixtQkFBTSxDQUR5QjtBQUUvQndCLGtCQUFNLEtBRnlCO0FBRy9CQyxpQkFBSyxLQUgwQjtBQUkvQkMsdUJBQVcsS0FKb0I7QUFLL0JDLHVCQUFXLEtBTG9CO0FBTS9CQyxzQkFBVSxLQU5xQjtBQU8vQkMsc0JBQVUsS0FQcUI7QUFRL0JDLHFCQUFRLENBQUMsa0NBQUQsRUFBb0MsbUNBQXBDLENBUnVCO0FBUy9CQyx3QkFBVztBQUNQLG1CQUFFO0FBQ0EvQiwyQkFBTTtBQUROLGlCQURLO0FBSVAscUJBQUk7QUFDRkEsMkJBQU07QUFESixpQkFKRztBQU9QLHFCQUFJO0FBQ0ZBLDJCQUFNO0FBREosaUJBUEc7QUFVUCxxQkFBSTtBQUNGQSwyQkFBTTtBQURKLGlCQVZHO0FBYVAsc0JBQUs7QUFDSEEsMkJBQU07QUFESDtBQWJFO0FBVG9CLFNBQWpDO0FBMkJELEtBcEVjOztBQXFFakJnQyxhQUFRO0FBQ1BDLGtCQURPLHNCQUNJQyxFQURKLEVBQ087QUFDYixpQkFBS2pDLFFBQUwsQ0FBY2tDLElBQWQsQ0FBbUJELEVBQW5CO0FBQ0EsU0FITTtBQUlERSxpQkFKQyxxQkFJU0YsRUFKVCxFQUlZRyxJQUpaLEVBSWlCO0FBQUE7O0FBQ2pCaEMsa0JBQU1DLEdBQU4sQ0FBVWdDLFNBQVNDLE1BQVQsR0FBa0IsV0FBNUIsRUFBd0M7QUFDcENDLHdCQUFRO0FBQ0pDLDZCQUFRUCxFQURKO0FBRUpRLHlCQUFJO0FBRkE7QUFENEIsYUFBeEMsRUFNQ2hDLElBTkQsQ0FNTSxrQkFBVTtBQUNaSSxrQkFBRSxpQ0FBRixFQUFxQzZCLElBQXJDLENBQTBDQyxPQUFPN0MsSUFBUCxDQUFZOEMsZUFBdEQ7QUFDRyxvQkFBR0QsT0FBTzdDLElBQVAsQ0FBWStDLFNBQWYsRUFDQTtBQUNDLHdCQUFHRixPQUFPN0MsSUFBUCxDQUFZK0MsU0FBWixJQUF1QixVQUExQixFQUNDQyxPQUFPVCxRQUFQLEdBQWtCLGNBQWNNLE9BQU83QyxJQUFQLENBQVlpRCxJQUE1QyxDQURELEtBR0NDLE9BQU9DLEtBQVAsQ0FBYU4sT0FBTzdDLElBQVAsQ0FBWStDLFNBQXpCLEVBQW9DLE9BQUs1QyxJQUFMLENBQVUsV0FBVixDQUFwQztBQUNELGlCQU5ELE1BTU87QUFDTitDLDJCQUFPRSxPQUFQLENBQWUsT0FBS2pELElBQUwsQ0FBVSxZQUFWLENBQWYsRUFBd0NtQyxJQUF4QztBQUNBO0FBQ0osYUFqQko7QUFrQkEsU0F2QkE7QUF3QkRlLG9CQXhCQyx3QkF3QllsQixFQXhCWixFQXdCZUcsSUF4QmYsRUF3Qm9CO0FBQUE7O0FBQ2pCLGdCQUFHZ0IsUUFBUUMsZUFBUixLQUE0QixJQUEvQixFQUFvQztBQUNoQzlDLDJCQUFXRixHQUFYLENBQWUsbUJBQW1CNEIsRUFBbEMsRUFDQ3hCLElBREQsQ0FDTSxrQkFBVTtBQUNaLHdCQUFHa0MsT0FBTzdDLElBQVAsQ0FBWWMsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUM3Qiw0QkFBSTBDLE9BQU9DLFNBQVMxQyxFQUFFLGFBQUYsRUFBaUI2QixJQUFqQixFQUFULElBQWtDLENBQTdDO0FBQ0E3QiwwQkFBRSxhQUFGLEVBQWlCNkIsSUFBakIsQ0FBc0JZLElBQXRCLEVBQTRCdkMsSUFBNUI7QUFDQWlDLCtCQUFPRSxPQUFQLENBQWUsT0FBS2pELElBQUwsQ0FBVSxrQkFBVixDQUFmLEVBQThDbUMsSUFBOUM7QUFDSCxxQkFKSyxNQUlDLElBQUdPLE9BQU83QyxJQUFQLENBQVljLFFBQVosSUFBc0IsQ0FBekIsRUFBNEI7QUFDakNvQywrQkFBT0MsS0FBUCxDQUFhLE9BQUtoRCxJQUFMLENBQVUsTUFBVixDQUFiO0FBQ0sscUJBRkEsTUFFTTtBQUNOK0MsK0JBQU9RLElBQVAsQ0FBWSxPQUFLdkQsSUFBTCxDQUFVLGlCQUFWLENBQVosRUFBMENtQyxJQUExQztBQUNBO0FBQ0osaUJBWEQ7QUFZSCxhQWJELE1BYU87QUFDSFksdUJBQU9DLEtBQVAsQ0FBYSxLQUFLaEQsSUFBTCxDQUFVLGVBQVYsQ0FBYjtBQUNIO0FBQ0osU0F6Q0E7QUEwQ0R3RCxzQkExQ0MsMEJBMENjeEIsRUExQ2QsRUEwQ2lCRyxJQTFDakIsRUEwQ3NCO0FBQUE7O0FBQ25CLGdCQUFHZ0IsUUFBUUMsZUFBUixLQUE0QixJQUEvQixFQUFvQztBQUNoQzlDLDJCQUFXRixHQUFYLENBQWUscUJBQXFCNEIsRUFBcEMsRUFDQ3hCLElBREQsQ0FDTSxrQkFBVTtBQUNaLHdCQUFHa0MsT0FBTzdDLElBQVAsQ0FBWWMsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUM3Qiw0QkFBSThDLE1BQU1ILFNBQVMxQyxFQUFFLFlBQUYsRUFBZ0I2QixJQUFoQixFQUFULElBQWlDLENBQTNDO0FBQ0E3QiwwQkFBRSxZQUFGLEVBQWdCNkIsSUFBaEIsQ0FBcUJnQixHQUFyQixFQUEwQjNDLElBQTFCO0FBQ0FpQywrQkFBT0UsT0FBUCxDQUFlLE9BQUtqRCxJQUFMLENBQVUsaUJBQVYsQ0FBZixFQUE2Q21DLElBQTdDO0FBQ0gscUJBSkssTUFJQyxJQUFHTyxPQUFPN0MsSUFBUCxDQUFZYyxRQUFaLElBQXNCLENBQXpCLEVBQTJCO0FBQzNCb0MsK0JBQU9DLEtBQVAsQ0FBYU4sT0FBTzdDLElBQVAsQ0FBWStDLFNBQXpCLEVBQW9DLE9BQUs1QyxJQUFMLENBQVUsb0JBQVYsQ0FBcEM7QUFDQSxxQkFGQSxNQUVNO0FBQ04rQywrQkFBT1EsSUFBUCxDQUFZLE9BQUt2RCxJQUFMLENBQVUsbUJBQVYsQ0FBWixFQUE0Q21DLElBQTVDO0FBQ0E7QUFDSixpQkFYRDtBQVlILGFBYkQsTUFhTztBQUNIWSx1QkFBT0MsS0FBUCxDQUFhLEtBQUtoRCxJQUFMLENBQVUsaUJBQVYsQ0FBYjtBQUNIO0FBQ0o7QUEzREE7QUFyRVMsQ0FBUixDQUFWOztBQW9JQSxJQUFJTCxNQUFNLElBQUlSLEdBQUosQ0FBUTtBQUNqQlMsUUFBSSxZQURhO0FBRWpCQyxVQUFNO0FBQ0xDLGVBQU0sRUFERDtBQUdMQyxrQkFBUyxFQUhKO0FBSUxDLGNBQUs7QUFKQSxLQUZXO0FBUWpCQyxXQVJpQixxQkFRUjtBQUFBOztBQUNSLGlCQUFTQyxjQUFULEdBQTBCO0FBQ2hCLG1CQUFPQyxNQUFNQyxHQUFOLENBQVUsaUJBQVYsQ0FBUDtBQUNIO0FBQ1AsaUJBQVNDLFdBQVQsR0FBdUI7QUFDdEIsbUJBQU9DLFdBQVdGLEdBQVgsQ0FBZSxXQUFmLENBQVA7QUFDQTtBQUNFRCxjQUFNSSxHQUFOLENBQVUsQ0FDVEwsZ0JBRFMsRUFFWkcsYUFGWSxDQUFWLEVBR0FHLElBSEEsQ0FHS0wsTUFBTU0sTUFBTixDQUNQLFVBQ0NDLFdBREQsRUFFQ0MsUUFGRCxFQUdLO0FBQ0osbUJBQUtYLElBQUwsR0FBWVUsWUFBWWIsSUFBWixDQUFpQkEsSUFBN0I7O0FBRUEsZ0JBQUdjLFNBQVNkLElBQVosRUFBaUI7QUFDaEJlLGtCQUFFLFlBQUYsRUFBZ0JDLE1BQWhCO0FBQ0FELGtCQUFFLFlBQUYsRUFBZ0JFLElBQWhCO0FBQ0EsdUJBQUtoQixLQUFMLEdBQWFhLFNBQVNkLElBQXRCO0FBQ0E7O0FBRUQsZ0JBQUcsT0FBS0MsS0FBTCxDQUFXaUIsTUFBWCxHQUFvQixFQUF2QixFQUEyQjtBQUMxQkgsa0JBQUUsb0JBQUYsRUFBd0JJLFdBQXhCLENBQW9DLE1BQXBDO0FBQ0EsYUFGRCxNQUVPO0FBQ05KLGtCQUFFLG9CQUFGLEVBQXdCSyxRQUF4QixDQUFpQyxNQUFqQztBQUNBO0FBQ0YsU0FsQk8sQ0FITCxFQXNCRkMsS0F0QkUsQ0FzQklOLEVBQUVPLElBdEJOO0FBdUJILEtBdENnQjtBQXVDakJDLFdBdkNpQixxQkF1Q1A7QUFDTjtBQUNBO0FBQ0FSLFVBQUUsa0JBQUYsRUFBc0JTLFdBQXRCLENBQWtDO0FBQ2hDdkIsbUJBQU0sQ0FEMEI7QUFFaEN3QixrQkFBTSxLQUYwQjtBQUdoQ0MsaUJBQUssS0FIMkI7QUFJaENDLHVCQUFXLEtBSnFCO0FBS2hDQyx1QkFBVyxLQUxxQjtBQU1oQ0Msc0JBQVUsS0FOc0I7QUFPaENDLHNCQUFVLEtBUHNCO0FBUWhDQyxxQkFBUSxDQUFDLGtDQUFELEVBQW9DLG1DQUFwQyxDQVJ3QjtBQVNoQ0Msd0JBQVc7QUFDUCxtQkFBRTtBQUNBL0IsMkJBQU07QUFETixpQkFESztBQUlQLHFCQUFJO0FBQ0ZBLDJCQUFNO0FBREosaUJBSkc7QUFPUCxxQkFBSTtBQUNGQSwyQkFBTTtBQURKLGlCQVBHO0FBVVAscUJBQUk7QUFDRkEsMkJBQU07QUFESixpQkFWRztBQWFQLHNCQUFLO0FBQ0hBLDJCQUFNO0FBREg7QUFiRTtBQVRxQixTQUFsQztBQTJCRCxLQXJFYzs7O0FBdUVqQmdDLGFBQVE7QUFDUEMsa0JBRE8sc0JBQ0lDLEVBREosRUFDTztBQUNiLGlCQUFLakMsUUFBTCxDQUFja0MsSUFBZCxDQUFtQkQsRUFBbkI7QUFDQSxTQUhNO0FBSURFLGlCQUpDLHFCQUlTRixFQUpULEVBSVlHLElBSlosRUFJaUI7QUFBQTs7QUFDakJoQyxrQkFBTUMsR0FBTixDQUFVZ0MsU0FBU0MsTUFBVCxHQUFrQixXQUE1QixFQUF3QztBQUNwQ0Msd0JBQVE7QUFDSkMsNkJBQVFQLEVBREo7QUFFSlEseUJBQUk7QUFGQTtBQUQ0QixhQUF4QyxFQU1DaEMsSUFORCxDQU1NLGtCQUFVO0FBQ1pJLGtCQUFFLGlDQUFGLEVBQXFDNkIsSUFBckMsQ0FBMENDLE9BQU83QyxJQUFQLENBQVk4QyxlQUF0RDtBQUNHLG9CQUFHRCxPQUFPN0MsSUFBUCxDQUFZK0MsU0FBZixFQUNBO0FBQ0Msd0JBQUdGLE9BQU83QyxJQUFQLENBQVkrQyxTQUFaLElBQXVCLFVBQTFCLEVBQ0NDLE9BQU9ULFFBQVAsR0FBa0IsY0FBY00sT0FBTzdDLElBQVAsQ0FBWWlELElBQTVDLENBREQsS0FHQ0MsT0FBT0MsS0FBUCxDQUFhTixPQUFPN0MsSUFBUCxDQUFZK0MsU0FBekIsRUFBb0MsT0FBSzVDLElBQUwsQ0FBVSxXQUFWLENBQXBDO0FBQ0QsaUJBTkQsTUFNTztBQUNOK0MsMkJBQU9FLE9BQVAsQ0FBZSxPQUFLakQsSUFBTCxDQUFVLFlBQVYsQ0FBZixFQUF3Q21DLElBQXhDO0FBQ0E7QUFDSixhQWpCSjtBQWtCQSxTQXZCQTtBQXdCRGUsb0JBeEJDLHdCQXdCWWxCLEVBeEJaLEVBd0JlRyxJQXhCZixFQXdCb0I7QUFBQTs7QUFDakIsZ0JBQUdnQixRQUFRQyxlQUFSLEtBQTRCLElBQS9CLEVBQW9DO0FBQ2hDOUMsMkJBQVdGLEdBQVgsQ0FBZSxtQkFBbUI0QixFQUFsQyxFQUNDeEIsSUFERCxDQUNNLGtCQUFVO0FBQ1osd0JBQUdrQyxPQUFPN0MsSUFBUCxDQUFZYyxRQUFaLElBQXNCLENBQXpCLEVBQTJCO0FBQzdCLDRCQUFJMEMsT0FBT0MsU0FBUzFDLEVBQUUsYUFBRixFQUFpQjZCLElBQWpCLEVBQVQsSUFBa0MsQ0FBN0M7QUFDQTdCLDBCQUFFLGFBQUYsRUFBaUI2QixJQUFqQixDQUFzQlksSUFBdEIsRUFBNEJ2QyxJQUE1QjtBQUNBaUMsK0JBQU9FLE9BQVAsQ0FBZSxPQUFLakQsSUFBTCxDQUFVLGtCQUFWLENBQWYsRUFBOENtQyxJQUE5QztBQUNILHFCQUpLLE1BSUMsSUFBR08sT0FBTzdDLElBQVAsQ0FBWWMsUUFBWixJQUFzQixDQUF6QixFQUE0QjtBQUNqQ29DLCtCQUFPQyxLQUFQLENBQWEsT0FBS2hELElBQUwsQ0FBVSxNQUFWLENBQWI7QUFDSyxxQkFGQSxNQUVNO0FBQ04rQywrQkFBT1EsSUFBUCxDQUFZLE9BQUt2RCxJQUFMLENBQVUsaUJBQVYsQ0FBWixFQUEwQ21DLElBQTFDO0FBQ0E7QUFDSixpQkFYRDtBQVlILGFBYkQsTUFhTztBQUNIWSx1QkFBT0MsS0FBUCxDQUFhLEtBQUtoRCxJQUFMLENBQVUsZUFBVixDQUFiO0FBQ0g7QUFDSixTQXpDQTtBQTBDRHdELHNCQTFDQywwQkEwQ2N4QixFQTFDZCxFQTBDaUJHLElBMUNqQixFQTBDc0I7QUFBQTs7QUFDbkIsZ0JBQUdnQixRQUFRQyxlQUFSLEtBQTRCLElBQS9CLEVBQW9DO0FBQ2hDOUMsMkJBQVdGLEdBQVgsQ0FBZSxxQkFBcUI0QixFQUFwQyxFQUNDeEIsSUFERCxDQUNNLGtCQUFVO0FBQ1osd0JBQUdrQyxPQUFPN0MsSUFBUCxDQUFZYyxRQUFaLElBQXNCLENBQXpCLEVBQTJCO0FBQzdCLDRCQUFJOEMsTUFBTUgsU0FBUzFDLEVBQUUsWUFBRixFQUFnQjZCLElBQWhCLEVBQVQsSUFBaUMsQ0FBM0M7QUFDQTdCLDBCQUFFLFlBQUYsRUFBZ0I2QixJQUFoQixDQUFxQmdCLEdBQXJCLEVBQTBCM0MsSUFBMUI7QUFDQWlDLCtCQUFPRSxPQUFQLENBQWUsT0FBS2pELElBQUwsQ0FBVSxpQkFBVixDQUFmLEVBQTZDbUMsSUFBN0M7QUFDSCxxQkFKSyxNQUlDLElBQUdPLE9BQU83QyxJQUFQLENBQVljLFFBQVosSUFBc0IsQ0FBekIsRUFBMkI7QUFDM0JvQywrQkFBT0MsS0FBUCxDQUFhTixPQUFPN0MsSUFBUCxDQUFZK0MsU0FBekIsRUFBb0MsT0FBSzVDLElBQUwsQ0FBVSxvQkFBVixDQUFwQztBQUNBLHFCQUZBLE1BRU07QUFDTitDLCtCQUFPUSxJQUFQLENBQVksT0FBS3ZELElBQUwsQ0FBVSxtQkFBVixDQUFaLEVBQTRDbUMsSUFBNUM7QUFDQTtBQUNKLGlCQVhEO0FBWUgsYUFiRCxNQWFPO0FBQ0hZLHVCQUFPQyxLQUFQLENBQWEsS0FBS2hELElBQUwsQ0FBVSxpQkFBVixDQUFiO0FBQ0g7QUFDSjtBQTNEQTtBQXZFUyxDQUFSLENBQVY7O0FBdUlBLElBQUlMLE1BQU0sSUFBSVIsR0FBSixDQUFRO0FBQ2pCUyxRQUFJLFlBRGE7QUFFakJDLFVBQU07QUFDTEMsZUFBTSxFQUREO0FBR0xDLGtCQUFTLEVBSEo7QUFJTEMsY0FBSztBQUpBLEtBRlc7QUFRakJDLFdBUmlCLHFCQVFSO0FBQUE7O0FBQ1IsaUJBQVNDLGNBQVQsR0FBMEI7QUFDaEIsbUJBQU9DLE1BQU1DLEdBQU4sQ0FBVSxpQkFBVixDQUFQO0FBQ0g7QUFDUCxpQkFBU0MsV0FBVCxHQUF1QjtBQUN0QixtQkFBT0MsV0FBV0YsR0FBWCxDQUFlLFdBQWYsQ0FBUDtBQUNBO0FBQ0VELGNBQU1JLEdBQU4sQ0FBVSxDQUNUTCxnQkFEUyxFQUVaRyxhQUZZLENBQVYsRUFHQUcsSUFIQSxDQUdLTCxNQUFNTSxNQUFOLENBQ1AsVUFDQ0MsV0FERCxFQUVDQyxRQUZELEVBR0s7QUFDSixtQkFBS1gsSUFBTCxHQUFZVSxZQUFZYixJQUFaLENBQWlCQSxJQUE3Qjs7QUFFQSxnQkFBR2MsU0FBU2QsSUFBWixFQUFpQjtBQUNoQmUsa0JBQUUsWUFBRixFQUFnQkMsTUFBaEI7QUFDQUQsa0JBQUUsWUFBRixFQUFnQkUsSUFBaEI7QUFDQSx1QkFBS2hCLEtBQUwsR0FBYWEsU0FBU2QsSUFBdEI7QUFDQTs7QUFFRCxnQkFBRyxPQUFLQyxLQUFMLENBQVdpQixNQUFYLEdBQW9CLEVBQXZCLEVBQTJCO0FBQzFCSCxrQkFBRSxxQkFBRixFQUF5QkksV0FBekIsQ0FBcUMsTUFBckM7QUFDQSxhQUZELE1BRU87QUFDTkosa0JBQUUscUJBQUYsRUFBeUJLLFFBQXpCLENBQWtDLE1BQWxDO0FBQ0E7QUFDRixTQWxCTyxDQUhMLEVBc0JGQyxLQXRCRSxDQXNCSU4sRUFBRU8sSUF0Qk47QUF1QkgsS0F0Q2dCO0FBdUNqQkMsV0F2Q2lCLHFCQXVDUDtBQUNOO0FBQ0E7QUFDQVIsVUFBRSxrQkFBRixFQUFzQlMsV0FBdEIsQ0FBa0M7QUFDaEN2QixtQkFBTSxDQUQwQjtBQUVoQ3dCLGtCQUFNLEtBRjBCO0FBR2hDQyxpQkFBSyxLQUgyQjtBQUloQ0MsdUJBQVcsS0FKcUI7QUFLaENDLHVCQUFXLEtBTHFCO0FBTWhDQyxzQkFBVSxLQU5zQjtBQU9oQ0Msc0JBQVUsS0FQc0I7QUFRaENDLHFCQUFRLENBQUMsa0NBQUQsRUFBb0MsbUNBQXBDLENBUndCO0FBU2hDQyx3QkFBVztBQUNQLG1CQUFFO0FBQ0EvQiwyQkFBTTtBQUROLGlCQURLO0FBSVAscUJBQUk7QUFDRkEsMkJBQU07QUFESixpQkFKRztBQU9QLHFCQUFJO0FBQ0ZBLDJCQUFNO0FBREosaUJBUEc7QUFVUCxxQkFBSTtBQUNGQSwyQkFBTTtBQURKLGlCQVZHO0FBYVAsc0JBQUs7QUFDSEEsMkJBQU07QUFESDtBQWJFO0FBVHFCLFNBQWxDO0FBMkJELEtBckVjOztBQXNFakJnQyxhQUFRO0FBQ1BDLGtCQURPLHNCQUNJQyxFQURKLEVBQ087QUFDYixpQkFBS2pDLFFBQUwsQ0FBY2tDLElBQWQsQ0FBbUJELEVBQW5CO0FBQ0EsU0FITTtBQUlERSxpQkFKQyxxQkFJU0YsRUFKVCxFQUlZRyxJQUpaLEVBSWlCO0FBQUE7O0FBQ2pCaEMsa0JBQU1DLEdBQU4sQ0FBVWdDLFNBQVNDLE1BQVQsR0FBa0IsV0FBNUIsRUFBd0M7QUFDcENDLHdCQUFRO0FBQ0pDLDZCQUFRUCxFQURKO0FBRUpRLHlCQUFJO0FBRkE7QUFENEIsYUFBeEMsRUFNQ2hDLElBTkQsQ0FNTSxrQkFBVTtBQUNaSSxrQkFBRSxpQ0FBRixFQUFxQzZCLElBQXJDLENBQTBDQyxPQUFPN0MsSUFBUCxDQUFZOEMsZUFBdEQ7QUFDRyxvQkFBR0QsT0FBTzdDLElBQVAsQ0FBWStDLFNBQWYsRUFDQTtBQUNDLHdCQUFHRixPQUFPN0MsSUFBUCxDQUFZK0MsU0FBWixJQUF1QixVQUExQixFQUNDQyxPQUFPVCxRQUFQLEdBQWtCLGNBQWNNLE9BQU83QyxJQUFQLENBQVlpRCxJQUE1QyxDQURELEtBR0NDLE9BQU9DLEtBQVAsQ0FBYU4sT0FBTzdDLElBQVAsQ0FBWStDLFNBQXpCLEVBQW9DLFFBQUs1QyxJQUFMLENBQVUsV0FBVixDQUFwQztBQUNELGlCQU5ELE1BTU87QUFDTitDLDJCQUFPRSxPQUFQLENBQWUsUUFBS2pELElBQUwsQ0FBVSxZQUFWLENBQWYsRUFBd0NtQyxJQUF4QztBQUNBO0FBQ0osYUFqQko7QUFrQkEsU0F2QkE7QUF3QkRlLG9CQXhCQyx3QkF3QllsQixFQXhCWixFQXdCZUcsSUF4QmYsRUF3Qm9CO0FBQUE7O0FBQ2pCLGdCQUFHZ0IsUUFBUUMsZUFBUixLQUE0QixJQUEvQixFQUFvQztBQUNoQzlDLDJCQUFXRixHQUFYLENBQWUsbUJBQW1CNEIsRUFBbEMsRUFDQ3hCLElBREQsQ0FDTSxrQkFBVTtBQUNaLHdCQUFHa0MsT0FBTzdDLElBQVAsQ0FBWWMsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUM3Qiw0QkFBSTBDLE9BQU9DLFNBQVMxQyxFQUFFLGFBQUYsRUFBaUI2QixJQUFqQixFQUFULElBQWtDLENBQTdDO0FBQ0E3QiwwQkFBRSxhQUFGLEVBQWlCNkIsSUFBakIsQ0FBc0JZLElBQXRCLEVBQTRCdkMsSUFBNUI7QUFDQWlDLCtCQUFPRSxPQUFQLENBQWUsUUFBS2pELElBQUwsQ0FBVSxrQkFBVixDQUFmLEVBQThDbUMsSUFBOUM7QUFDSCxxQkFKSyxNQUlDLElBQUdPLE9BQU83QyxJQUFQLENBQVljLFFBQVosSUFBc0IsQ0FBekIsRUFBNEI7QUFDakNvQywrQkFBT0MsS0FBUCxDQUFhLFFBQUtoRCxJQUFMLENBQVUsTUFBVixDQUFiO0FBQ0sscUJBRkEsTUFFTTtBQUNOK0MsK0JBQU9RLElBQVAsQ0FBWSxRQUFLdkQsSUFBTCxDQUFVLGlCQUFWLENBQVosRUFBMENtQyxJQUExQztBQUNBO0FBQ0osaUJBWEQ7QUFZSCxhQWJELE1BYU87QUFDSFksdUJBQU9DLEtBQVAsQ0FBYSxLQUFLaEQsSUFBTCxDQUFVLGVBQVYsQ0FBYjtBQUNIO0FBQ0osU0F6Q0E7QUEwQ0R3RCxzQkExQ0MsMEJBMENjeEIsRUExQ2QsRUEwQ2lCRyxJQTFDakIsRUEwQ3NCO0FBQUE7O0FBQ25CLGdCQUFHZ0IsUUFBUUMsZUFBUixLQUE0QixJQUEvQixFQUFvQztBQUNoQzlDLDJCQUFXRixHQUFYLENBQWUscUJBQXFCNEIsRUFBcEMsRUFDQ3hCLElBREQsQ0FDTSxrQkFBVTtBQUNaLHdCQUFHa0MsT0FBTzdDLElBQVAsQ0FBWWMsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUM3Qiw0QkFBSThDLE1BQU1ILFNBQVMxQyxFQUFFLFlBQUYsRUFBZ0I2QixJQUFoQixFQUFULElBQWlDLENBQTNDO0FBQ0E3QiwwQkFBRSxZQUFGLEVBQWdCNkIsSUFBaEIsQ0FBcUJnQixHQUFyQixFQUEwQjNDLElBQTFCO0FBQ0FpQywrQkFBT0UsT0FBUCxDQUFlLFFBQUtqRCxJQUFMLENBQVUsaUJBQVYsQ0FBZixFQUE2Q21DLElBQTdDO0FBQ0gscUJBSkssTUFJQyxJQUFHTyxPQUFPN0MsSUFBUCxDQUFZYyxRQUFaLElBQXNCLENBQXpCLEVBQTJCO0FBQzNCb0MsK0JBQU9DLEtBQVAsQ0FBYU4sT0FBTzdDLElBQVAsQ0FBWStDLFNBQXpCLEVBQW9DLFFBQUs1QyxJQUFMLENBQVUsb0JBQVYsQ0FBcEM7QUFDQSxxQkFGQSxNQUVNO0FBQ04rQywrQkFBT1EsSUFBUCxDQUFZLFFBQUt2RCxJQUFMLENBQVUsbUJBQVYsQ0FBWixFQUE0Q21DLElBQTVDO0FBQ0E7QUFDSixpQkFYRDtBQVlILGFBYkQsTUFhTztBQUNIWSx1QkFBT0MsS0FBUCxDQUFhLEtBQUtoRCxJQUFMLENBQVUsaUJBQVYsQ0FBYjtBQUNIO0FBQ0o7QUEzREE7QUF0RVMsQ0FBUixDQUFWOztBQXFJQSxTQUFTUixnQkFBVCxDQUEwQmtFLENBQTFCLEVBQTZCO0FBQ3pCLFdBQU9BLEVBQUVDLFFBQUYsR0FBYUMsT0FBYixDQUFxQix1QkFBckIsRUFBOEMsR0FBOUMsQ0FBUDtBQUNIOztBQUVELFNBQVNsRSxRQUFULENBQWtCbUUsQ0FBbEIsRUFBcUI7QUFDakIsV0FBT0MsS0FBS0MsS0FBTCxDQUFXRixDQUFYLENBQVA7QUFDSCxDOzs7Ozs7O0FDaGFEO0FBQ0E7QUFDQSx3QkFBcUo7QUFDcko7QUFDQSx3QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3BhZ2VzL3Byb2R1Y3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ0MCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsIjx0ZW1wbGF0ZT5cclxuICAgIDxkaXYgY2xhc3M9XCJib3gtcHJvZHVjdC1vdXRlclwiIHN0eWxlPVwibWFyZ2luOjAgYXV0bztcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImJveC1wcm9kdWN0XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImltZy13cmFwcGVyXCIgc3R5bGU9XCJoZWlnaHQ6YXV0bztcIj5cclxuICAgICAgICAgIDxhIDpocmVmPVwiJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcIj5cclxuICAgICAgICAgIDxpbWcgYWx0PVwiUHJvZHVjdFwiIHYtYmluZDpzcmM9XCInL3Nob3BwaW5nL2ltYWdlcy9wcm9kdWN0LycraXRlbS5pZCsnL3ByaW1hcnkuanBnJ1wiIGNsYXNzPVwiaW1ndGh1bWJcIj5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0YWdzXCIgdi1pZj0naXRlbS5zYWxlX3ByaWNlJz5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJsYWJlbC10YWdzXCI+PHNwYW4gY2xhc3M9XCJsYWJlbCBsYWJlbC1kYW5nZXIgYXJyb3dlZFwiPnt7aXRlbS5kaXNjb3VudH19JTwvc3Bhbj48L3NwYW4+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJvcHRpb25cIj5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIGRhdGEtcGxhY2VtZW50PVwidG9wXCIgdGl0bGU9XCJBZGQgdG8gQ2FydFwiIGRhdGEtb3JpZ2luYWwtdGl0bGU9XCJBZGQgdG8gQ2FydFwiIEBjbGljaz1cImFkZFRvQ2FydCgpXCI+PGkgY2xhc3M9XCJmYSBmYS1zaG9wcGluZy1jYXJ0XCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiQWRkIHRvIENvbXBhcmVcIiBAY2xpY2s9XCJhZGRUb0NvbXBhcmUoKVwiPjxpIGNsYXNzPVwiZmEgZmEtYWxpZ24tbGVmdFwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiB0aXRsZT1cIkFkZCB0byBXaXNobGlzdFwiIGNsYXNzPVwid2lzaGxpc3RcIiBAY2xpY2s9XCJhZGRUb0Zhdm9yaXRlcygpXCI+PGkgY2xhc3M9XCJmYSBmYS1oZWFydFwiPjwvaT48L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8aDY+XHJcbiAgICAgICAgICA8YSBjbGFzcz1cInByb2R1Y3QtbmFtZVwiIDpocmVmPVwiJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcIj5cclxuICAgICAgICAgICAge3sgbmFtZWxlbmd0aCB9fVxyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgIDwvaDY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInByaWNlXCIgc3R5bGU9XCJtYXJnaW4tdG9wOi01cHg7IGZvbnQtc2l6ZToxNHB4O1wiPlxyXG4gICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgPHNwYW4gdi1pZj0naXRlbS5kaXNjb3VudCAmJiBpdGVtLmRpc2NvdW50ID4gMCc+UHt7cGFyc2VGbG9hdChcIjBcIitzYWxlcHJpY2UpIHwgY3VycmVuY3l9fTwvc3Bhbj5cclxuICAgICAgICAgICAgPGRpdiBzdHlsZT1cImRpc3BsYXk6YmxvY2s7XCI+XHJcbiAgICAgICAgICAgICAgPHNwYW4gOmNsYXNzPVwiKGl0ZW0uZGlzY291bnQgJiYgaXRlbS5kaXNjb3VudCA+IDApPydwcmljZS1vbGQnOicnXCIgc3R5bGU9XCJtYXJnaW4tbGVmdDowcHg7XCI+XHJcbiAgICAgICAgICAgICAgICBQe3twYXJzZUZsb2F0KFwiMFwiK3ByaWNlKSB8IGN1cnJlbmN5fX1cclxuICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPGRpdiB2LWlmPVwiaXRlbS5kaXNjb3VudCA9PSAwXCIgc3R5bGU9XCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cclxuICAgICAgICAgICAgICA8c3BhbiB2LWlmPSdpdGVtLmRpc2NvdW50ICYmIGl0ZW0uZGlzY291bnQgPiAwJyBzdHlsZT1cImZvbnQtc2l6ZToxM3B4OyBjb2xvcjojNjY2O1wiPi17eyBpdGVtLmRpc2NvdW50IH19JTwvc3Bhbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwicmF0aW5nLWJsb2NrXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXhzbWFsbFwiPjxzcGFuIDpzdHlsZT1cInByb2R1Y3RSYXRpbmdcIiBjbGFzcz1cInN0YXItcmF0aW5ncy1zcHJpdGUteHNtYWxsLXJhdGluZ1wiPjwvc3Bhbj48L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICBwcm9wczogWydpdGVtJ10sXHJcbiAgbWV0aG9kczp7XHJcbiAgICBzZWxlY3QoaWQpe1xyXG4gICAgICAgdGhpcy4kcGFyZW50LnNlbGVjdEl0ZW0oaWQpXHJcbiAgICB9LFxyXG4gICAgYWRkVG9DYXJ0KCl7XHJcbiAgICAgICB0aGlzLiRwYXJlbnQuYWRkVG9DYXJ0KHRoaXMuaXRlbS5pZCx0aGlzLml0ZW0ubmFtZSlcclxuICAgIH0sXHJcbiAgICBhZGRUb0NvbXBhcmUoKXtcclxuICAgICAgIHRoaXMuJHBhcmVudC5hZGRUb0NvbXBhcmUodGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5uYW1lKVxyXG4gICAgfSxcclxuICAgIGFkZFRvRmF2b3JpdGVzKCl7XHJcbiAgICAgICB0aGlzLiRwYXJlbnQuYWRkVG9GYXZvcml0ZXModGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5uYW1lKVxyXG4gICAgfVxyXG4gIH0sXHJcbiAgY29tcHV0ZWQ6e1xyXG4gICAgcHJpY2UoKXtcclxuICAgICAgaWYodGhpcy5pdGVtLmZlZV9pbmNsdWRlZD09MSlcclxuICAgICAgICByZXR1cm4gcGFyc2VGbG9hdCh0aGlzLml0ZW0ucHJpY2UpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0uc2hpcHBpbmdfZmVlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLmNoYXJnZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS52YXQpO1xyXG4gICAgICBlbHNlXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXRlbS5wcmljZTtcclxuICAgIH0sXHJcbiAgICBzYWxlcHJpY2UoKXtcclxuICAgICAgaWYodGhpcy5pdGVtLmZlZV9pbmNsdWRlZD09MSlcclxuICAgICAgICByZXR1cm4gcGFyc2VGbG9hdCh0aGlzLml0ZW0uc2FsZV9wcmljZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0uY2hhcmdlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLnZhdCk7XHJcbiAgICAgIGVsc2VcclxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtLnNhbGVfcHJpY2U7XHJcbiAgICB9LFxyXG4gICAgbmFtZWxlbmd0aCgpe1xyXG4gICAgICB2YXIgbSA9ICQoXCJtZXRhW25hbWU9bG9jYWxlLWxhbmddXCIpOyAgICBcclxuICAgICAgdmFyIGxhbmcgPSBtLmF0dHIoXCJjb250ZW50XCIpO1xyXG4gICAgICB2YXIgcHJvZG5hbWUgPSB0aGlzLml0ZW0ubmFtZTtcclxuICAgICAgaWYobGFuZyA9PSBcImNuXCIpe1xyXG4gICAgICAgIGlmKHRoaXMuaXRlbS5jbl9uYW1lKXtcclxuICAgICAgICAgIHByb2RuYW1lID0gdGhpcy5pdGVtLmNuX25hbWU7IFxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgcHJvZG5hbWUgPSB0aGlzLml0ZW0ubmFtZTsgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAocHJvZG5hbWUubGVuZ3RoID4gMzgpIHtcclxuICAgICAgICB2YXIgbmFtZSA9IHByb2RuYW1lLnN1YnN0cigwLCAzOCkrXCIuLi5cIjtcclxuICAgICAgICByZXR1cm4gbmFtZTtcclxuICAgICAgfVxyXG4gICAgICBlbHNle1xyXG4gICAgICAgIHJldHVybiBwcm9kbmFtZTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHN0YXJSYXRpbmcoKXtcclxuICAgICAgcmV0dXJuIFwid2lkdGg6XCIrTWF0aC5yb3VuZCgodGhpcy5pdGVtLnJhdGUvdGhpcy5pdGVtLmNvdW50KSoyMCkrXCIlXCI7XHJcbiAgICB9LFxyXG4gICAgcHJvZHVjdFJhdGluZygpe1xyXG4gICAgICByZXR1cm4gXCJ3aWR0aDpcIit0aGlzLml0ZW0ucHJvZHVjdF9yYXRpbmcrXCIlXCI7XHJcbiAgICB9LFxyXG5cclxuICB9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFByb2R1Y3QudnVlPzQ5NmRkYjIwIiwiVnVlLmNvbXBvbmVudCgncHJvZHVjdC1pdGVtJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvY29tbW9uL1Byb2R1Y3QudnVlJykpO1xyXG5cclxuVnVlLmZpbHRlcignY3VycmVuY3knLCBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgIHJldHVybiAgbnVtYmVyV2l0aENvbW1hcyh2YWx1ZS50b0ZpeGVkKDIpKTtcclxufSk7XHJcblxyXG5WdWUuZmlsdGVyKCdyb3VuZCcsIGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuICByb3VuZE9mZih2YWx1ZSk7XHJcbn0pO1xyXG5cclxudmFyIGFwcCA9IG5ldyBWdWUoe1xyXG5cdGVsOiAnI2Zvcl9tZW4nLFxyXG5cdGRhdGE6IHtcclxuXHRcdGl0ZW1zOltcclxuXHRcdF0sXHJcblx0XHRzZWxlY3RlZDpbXSxcclxuXHRcdGxhbmc6W11cclxuXHR9LFxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdGZ1bmN0aW9uIGdldFRyYW5zbGF0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2NhcnQnKTtcclxuICAgICAgICB9XHJcblx0XHRmdW5jdGlvbiBnZXRQcm9kdWN0cygpIHtcclxuXHRcdFx0cmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvZm9ybWVuJyk7XHJcblx0XHR9XHJcbiAgICBcdGF4aW9zLmFsbChbXHJcbiAgICBcdFx0Z2V0VHJhbnNsYXRpb24oKSxcclxuXHRcdFx0Z2V0UHJvZHVjdHMoKVxyXG5cdFx0XSkudGhlbihheGlvcy5zcHJlYWQoXHJcblx0XHRcdChcdFxyXG5cdFx0XHRcdHRyYW5zbGF0aW9uLFxyXG5cdFx0XHRcdHJlc3BvbnNlXHJcblx0XHRcdCkgPT4ge1xyXG5cdFx0XHRcdHRoaXMubGFuZyA9IHRyYW5zbGF0aW9uLmRhdGEuZGF0YTtcclxuXHJcblx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YSl7XHJcblx0XHRcdFx0XHQkKCcuZmxleGhpZGUxJykucmVtb3ZlKCk7XHJcblx0XHRcdFx0XHQkKCcjZm9yX21lbicpLnNob3coKTtcclxuXHRcdFx0XHRcdHRoaXMuaXRlbXMgPSByZXNwb25zZS5kYXRhO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYodGhpcy5pdGVtcy5sZW5ndGggPiAxMCkge1xyXG5cdFx0XHRcdFx0JCgnI3NlZS1hbGwtZm9yLW1lbicpLnJlbW92ZUNsYXNzKCdoaWRlJyk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdCQoJyNzZWUtYWxsLWZvci1tZW4nKS5hZGRDbGFzcygnaGlkZScpO1xyXG5cdFx0XHRcdH1cclxuXHRcdH0pKVxyXG5cdFx0LmNhdGNoKCQubm9vcCk7XHJcblx0fSxcclxuXHR1cGRhdGVkKCkge1xyXG5cdCAgICAvLyBGaXJlZCBldmVyeSBzZWNvbmQsIHNob3VsZCBhbHdheXMgYmUgdHJ1ZVxyXG5cdCAgICAkKCcucHJvZHVjdC1zbGlkZXInKS5vd2xDYXJvdXNlbCh7XHJcblx0ICAgICAgaXRlbXM6MSxcclxuXHQgICAgICBkb3RzOiBmYWxzZSxcclxuXHQgICAgICBuYXY6IGZhbHNlLFxyXG5cdCAgICAgIG1vdXNlRHJhZzogZmFsc2UsXHJcblx0ICAgICAgdG91Y2hEcmFnOiBmYWxzZSxcclxuXHQgICAgICBwdWxsRHJhZzogZmFsc2UsXHJcblx0ICAgICAgZnJlZURyYWc6IGZhbHNlLFxyXG5cdCAgICAgIG5hdlRleHQ6Wyc8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLWxlZnRcIj48L2k+JywnPGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1yaWdodFwiPjwvaT4nXSxcclxuXHQgICAgICByZXNwb25zaXZlOntcclxuXHQgICAgICAgICAgMDp7XHJcblx0ICAgICAgICAgICAgaXRlbXM6MixcclxuXHQgICAgICAgICAgfSxcclxuXHQgICAgICAgICAgNDgwOntcclxuXHQgICAgICAgICAgICBpdGVtczoyLFxyXG5cdCAgICAgICAgICB9LFxyXG5cdCAgICAgICAgICA3Njg6e1xyXG5cdCAgICAgICAgICAgIGl0ZW1zOjIsXHJcblx0ICAgICAgICAgIH0sXHJcblx0ICAgICAgICAgIDk5Mjp7XHJcblx0ICAgICAgICAgICAgaXRlbXM6NSxcclxuXHQgICAgICAgICAgfSxcclxuXHQgICAgICAgICAgMTIwMDp7XHJcblx0ICAgICAgICAgICAgaXRlbXM6NSxcclxuXHQgICAgICAgICAgfVxyXG5cdCAgICAgICAgfVxyXG5cdCAgICB9KTtcclxuXHQgIH0sXHJcblx0bWV0aG9kczp7XHJcblx0XHRzZWxlY3RJdGVtKGlkKXtcclxuXHRcdFx0dGhpcy5zZWxlY3RlZC5wdXNoKGlkKVxyXG5cdFx0fSxcclxuICAgICAgICBhZGRUb0NhcnQoaWQsbmFtZSl7XHJcblx0ICAgICAgICBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2FkZCcse1xyXG5cdCAgICAgICAgICAgIHBhcmFtczoge1xyXG5cdCAgICAgICAgICAgICAgICBwcm9kX2lkOmlkLFxyXG5cdCAgICAgICAgICAgICAgICBxdHk6MVxyXG5cdCAgICAgICAgICAgIH1cclxuXHQgICAgICAgIH0pXHJcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG5cdCAgICAgICAgICAgICQoXCIjY2FydC1jb3VudCwgI2NhcnQtY291bnQtbW9iaWxlXCIpLnRleHQocmVzdWx0LmRhdGEuY2FydF9pdGVtX2NvdW50KTtcclxuICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLmVycm9yX21zZylcclxuICAgICAgICAgICAgICAgIHtcdFxyXG4gICAgICAgICAgICAgICAgXHRpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2c9PSdSZWRpcmVjdCcpXHJcbiAgICAgICAgICAgICAgICBcdFx0d2luZG93LmxvY2F0aW9uID0gJy9wcm9kdWN0LycgKyByZXN1bHQuZGF0YS5zbHVnO1xyXG4gICAgICAgICAgICAgICAgXHRlbHNlXHJcblx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQnXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG5cdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmdbJ2FkZGVkLWNhcnQnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYWRkVG9Db21wYXJlKGlkLG5hbWUpe1xyXG4gICAgICAgICAgICBpZihMYXJhdmVsLmlzQXV0aGVudGljYXRlZCA9PT0gdHJ1ZSl7XHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2FkZHRvY29tcGFyZS8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuXHRcdCAgICAgICAgICAgICAgICB2YXIgY29tcCA9IHBhcnNlSW50KCQoXCIjY29tcC1jb3VudFwiKS50ZXh0KCkpKzE7XHJcblx0XHQgICAgICAgICAgICAgICAgJChcIiNjb21wLWNvdW50XCIpLnRleHQoY29tcCkuc2hvdygpO1xyXG5cdFx0ICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1snYWRkZWQtY29tcGFyaXNvbiddLCBuYW1lKTtcclxuXHRcdCAgICAgICAgICAgIH0gZWxzZSBpZihyZXN1bHQuZGF0YS5yZXNwb25zZT09Mykge1xyXG5cdFx0ICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHRoaXMubGFuZ1snZnVsbCddKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmluZm8odGhpcy5sYW5nWydhbHJlYWR5LWNvbXBhcmUnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydsb2dpbi1jb21wYXJlJ10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBhZGRUb0Zhdm9yaXRlcyhpZCxuYW1lKXtcclxuICAgICAgICAgICAgaWYoTGFyYXZlbC5pc0F1dGhlbnRpY2F0ZWQgPT09IHRydWUpe1xyXG4gICAgICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9hZGR0b2Zhdm9yaXRlcy8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuXHRcdCAgICAgICAgICAgICAgICB2YXIgZmF2ID0gcGFyc2VJbnQoJChcIiNmYXYtY291bnRcIikudGV4dCgpKSsxO1xyXG5cdFx0ICAgICAgICAgICAgICAgICQoXCIjZmF2LWNvdW50XCIpLnRleHQoZmF2KS5zaG93KCk7XHJcblx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nWydhZGRlZC1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcblx0XHQgICAgICAgICAgICB9IGVsc2UgaWYocmVzdWx0LmRhdGEucmVzcG9uc2U9PTIpe1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQtd2lzaGxpc3QnXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBcdHRvYXN0ci5pbmZvKHRoaXMubGFuZ1snYWxyZWFkeS1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydsb2dpbi1mYXZvcml0ZXMnXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblx0fVxyXG59KVxyXG5cclxudmFyIGFwcCA9IG5ldyBWdWUoe1xyXG5cdGVsOiAnI2Zvcl93b21lbicsXHJcblx0ZGF0YToge1xyXG5cdFx0aXRlbXM6W1xyXG5cdFx0XSxcclxuXHRcdHNlbGVjdGVkOltdLFxyXG5cdFx0bGFuZzpbXVxyXG5cdH0sXHJcblx0Y3JlYXRlZCgpe1xyXG5cdFx0ZnVuY3Rpb24gZ2V0VHJhbnNsYXRpb24oKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvcy5nZXQoJy90cmFuc2xhdGUvY2FydCcpO1xyXG4gICAgICAgIH1cclxuXHRcdGZ1bmN0aW9uIGdldFByb2R1Y3RzKCkge1xyXG5cdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9mb3J3b21lbicpO1xyXG5cdFx0fVxyXG4gICAgXHRheGlvcy5hbGwoW1xyXG4gICAgXHRcdGdldFRyYW5zbGF0aW9uKCksXHJcblx0XHRcdGdldFByb2R1Y3RzKClcclxuXHRcdF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG5cdFx0XHQoXHRcclxuXHRcdFx0XHR0cmFuc2xhdGlvbixcclxuXHRcdFx0XHRyZXNwb25zZVxyXG5cdFx0XHQpID0+IHtcclxuXHRcdFx0XHR0aGlzLmxhbmcgPSB0cmFuc2xhdGlvbi5kYXRhLmRhdGE7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YSl7XHJcblx0XHRcdFx0XHQkKCcuZmxleGhpZGUyJykucmVtb3ZlKCk7XHJcblx0XHRcdFx0XHQkKCcjZm9yX3dvbWVuJykuc2hvdygpO1xyXG5cdFx0XHRcdFx0dGhpcy5pdGVtcyA9IHJlc3BvbnNlLmRhdGE7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpZih0aGlzLml0ZW1zLmxlbmd0aCA+IDEwKSB7XHJcblx0XHRcdFx0XHQkKCcjc2VlLWFsbC1mb3Itd29tZW4nKS5yZW1vdmVDbGFzcygnaGlkZScpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHQkKCcjc2VlLWFsbC1mb3Itd29tZW4nKS5hZGRDbGFzcygnaGlkZScpO1xyXG5cdFx0XHRcdH1cclxuXHRcdH0pKVxyXG5cdFx0LmNhdGNoKCQubm9vcCk7XHJcblx0fSxcclxuXHR1cGRhdGVkKCkge1xyXG5cdCAgICAvLyBGaXJlZCBldmVyeSBzZWNvbmQsIHNob3VsZCBhbHdheXMgYmUgdHJ1ZVxyXG5cdCAgICAvLyBjb25zb2xlLmxvZyh0aGlzLml0ZW1zKTtcclxuXHQgICAgJCgnLmZvcndvbWVuLXNsaWRlcicpLm93bENhcm91c2VsKHtcclxuXHQgICAgICBpdGVtczoxLFxyXG5cdCAgICAgIGRvdHM6IGZhbHNlLFxyXG5cdCAgICAgIG5hdjogZmFsc2UsXHJcblx0ICAgICAgbW91c2VEcmFnOiBmYWxzZSxcclxuXHQgICAgICB0b3VjaERyYWc6IGZhbHNlLFxyXG5cdCAgICAgIHB1bGxEcmFnOiBmYWxzZSxcclxuXHQgICAgICBmcmVlRHJhZzogZmFsc2UsXHJcblx0ICAgICAgbmF2VGV4dDpbJzxpIGNsYXNzPVwiZmEgZmEtYW5nbGUtbGVmdFwiPjwvaT4nLCc8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPiddLFxyXG5cdCAgICAgIHJlc3BvbnNpdmU6e1xyXG5cdCAgICAgICAgICAwOntcclxuXHQgICAgICAgICAgICBpdGVtczoyLFxyXG5cdCAgICAgICAgICB9LFxyXG5cdCAgICAgICAgICA0ODA6e1xyXG5cdCAgICAgICAgICAgIGl0ZW1zOjIsXHJcblx0ICAgICAgICAgIH0sXHJcblx0ICAgICAgICAgIDc2ODp7XHJcblx0ICAgICAgICAgICAgaXRlbXM6MixcclxuXHQgICAgICAgICAgfSxcclxuXHQgICAgICAgICAgOTkyOntcclxuXHQgICAgICAgICAgICBpdGVtczo1LFxyXG5cdCAgICAgICAgICB9LFxyXG5cdCAgICAgICAgICAxMjAwOntcclxuXHQgICAgICAgICAgICBpdGVtczo1LFxyXG5cdCAgICAgICAgICB9XHJcblx0ICAgICAgICB9XHJcblx0ICAgIH0pO1xyXG5cdCAgfSxcclxuXHJcblx0bWV0aG9kczp7XHJcblx0XHRzZWxlY3RJdGVtKGlkKXtcclxuXHRcdFx0dGhpcy5zZWxlY3RlZC5wdXNoKGlkKVxyXG5cdFx0fSxcclxuICAgICAgICBhZGRUb0NhcnQoaWQsbmFtZSl7XHJcblx0ICAgICAgICBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2FkZCcse1xyXG5cdCAgICAgICAgICAgIHBhcmFtczoge1xyXG5cdCAgICAgICAgICAgICAgICBwcm9kX2lkOmlkLFxyXG5cdCAgICAgICAgICAgICAgICBxdHk6MVxyXG5cdCAgICAgICAgICAgIH1cclxuXHQgICAgICAgIH0pXHJcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG5cdCAgICAgICAgICAgICQoXCIjY2FydC1jb3VudCwgI2NhcnQtY291bnQtbW9iaWxlXCIpLnRleHQocmVzdWx0LmRhdGEuY2FydF9pdGVtX2NvdW50KTtcclxuICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLmVycm9yX21zZylcclxuICAgICAgICAgICAgICAgIHtcdFxyXG4gICAgICAgICAgICAgICAgXHRpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2c9PSdSZWRpcmVjdCcpXHJcbiAgICAgICAgICAgICAgICBcdFx0d2luZG93LmxvY2F0aW9uID0gJy9wcm9kdWN0LycgKyByZXN1bHQuZGF0YS5zbHVnO1xyXG4gICAgICAgICAgICAgICAgXHRlbHNlXHJcblx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQnXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG5cdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmdbJ2FkZGVkLWNhcnQnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYWRkVG9Db21wYXJlKGlkLG5hbWUpe1xyXG4gICAgICAgICAgICBpZihMYXJhdmVsLmlzQXV0aGVudGljYXRlZCA9PT0gdHJ1ZSl7XHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2FkZHRvY29tcGFyZS8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuXHRcdCAgICAgICAgICAgICAgICB2YXIgY29tcCA9IHBhcnNlSW50KCQoXCIjY29tcC1jb3VudFwiKS50ZXh0KCkpKzE7XHJcblx0XHQgICAgICAgICAgICAgICAgJChcIiNjb21wLWNvdW50XCIpLnRleHQoY29tcCkuc2hvdygpO1xyXG5cdFx0ICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1snYWRkZWQtY29tcGFyaXNvbiddLCBuYW1lKTtcclxuXHRcdCAgICAgICAgICAgIH0gZWxzZSBpZihyZXN1bHQuZGF0YS5yZXNwb25zZT09Mykge1xyXG5cdFx0ICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHRoaXMubGFuZ1snZnVsbCddKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmluZm8odGhpcy5sYW5nWydhbHJlYWR5LWNvbXBhcmUnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydsb2dpbi1jb21wYXJlJ10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBhZGRUb0Zhdm9yaXRlcyhpZCxuYW1lKXtcclxuICAgICAgICAgICAgaWYoTGFyYXZlbC5pc0F1dGhlbnRpY2F0ZWQgPT09IHRydWUpe1xyXG4gICAgICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9hZGR0b2Zhdm9yaXRlcy8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuXHRcdCAgICAgICAgICAgICAgICB2YXIgZmF2ID0gcGFyc2VJbnQoJChcIiNmYXYtY291bnRcIikudGV4dCgpKSsxO1xyXG5cdFx0ICAgICAgICAgICAgICAgICQoXCIjZmF2LWNvdW50XCIpLnRleHQoZmF2KS5zaG93KCk7XHJcblx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nWydhZGRlZC1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcblx0XHQgICAgICAgICAgICB9IGVsc2UgaWYocmVzdWx0LmRhdGEucmVzcG9uc2U9PTIpe1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQtd2lzaGxpc3QnXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBcdHRvYXN0ci5pbmZvKHRoaXMubGFuZ1snYWxyZWFkeS1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydsb2dpbi1mYXZvcml0ZXMnXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblx0fVxyXG59KVxyXG5cclxuXHJcbnZhciBhcHAgPSBuZXcgVnVlKHtcclxuXHRlbDogJyNmb3JfY3liZXInLFxyXG5cdGRhdGE6IHtcclxuXHRcdGl0ZW1zOltcclxuXHRcdF0sXHJcblx0XHRzZWxlY3RlZDpbXSxcclxuXHRcdGxhbmc6W11cclxuXHR9LFxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdGZ1bmN0aW9uIGdldFRyYW5zbGF0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2NhcnQnKTtcclxuICAgICAgICB9XHJcblx0XHRmdW5jdGlvbiBnZXRQcm9kdWN0cygpIHtcclxuXHRcdFx0cmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvZm9yY3liZXInKTtcclxuXHRcdH1cclxuICAgIFx0YXhpb3MuYWxsKFtcclxuICAgIFx0XHRnZXRUcmFuc2xhdGlvbigpLFxyXG5cdFx0XHRnZXRQcm9kdWN0cygpXHJcblx0XHRdKS50aGVuKGF4aW9zLnNwcmVhZChcclxuXHRcdFx0KFx0XHJcblx0XHRcdFx0dHJhbnNsYXRpb24sXHJcblx0XHRcdFx0cmVzcG9uc2VcclxuXHRcdFx0KSA9PiB7XHJcblx0XHRcdFx0dGhpcy5sYW5nID0gdHJhbnNsYXRpb24uZGF0YS5kYXRhO1xyXG5cclxuXHRcdFx0XHRpZihyZXNwb25zZS5kYXRhKXtcclxuXHRcdFx0XHRcdCQoJy5mbGV4aGlkZTMnKS5yZW1vdmUoKTtcclxuXHRcdFx0XHRcdCQoJyNmb3JfY3liZXInKS5zaG93KCk7XHJcblx0XHRcdFx0XHR0aGlzLml0ZW1zID0gcmVzcG9uc2UuZGF0YTtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGlmKHRoaXMuaXRlbXMubGVuZ3RoID4gMTApIHtcclxuXHRcdFx0XHRcdCQoJyNzZWUtYWxsLWN5YmVyLXpvbmUnKS5yZW1vdmVDbGFzcygnaGlkZScpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHQkKCcjc2VlLWFsbC1jeWJlci16b25lJykuYWRkQ2xhc3MoJ2hpZGUnKTtcclxuXHRcdFx0XHR9XHJcblx0XHR9KSlcclxuXHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cdH0sXHJcblx0dXBkYXRlZCgpIHtcclxuXHQgICAgLy8gRmlyZWQgZXZlcnkgc2Vjb25kLCBzaG91bGQgYWx3YXlzIGJlIHRydWVcclxuXHQgICAgLy8gY29uc29sZS5sb2codGhpcy5pdGVtcyk7XHJcblx0ICAgICQoJy5mb3JjeWJlci1zbGlkZXInKS5vd2xDYXJvdXNlbCh7XHJcblx0ICAgICAgaXRlbXM6MSxcclxuXHQgICAgICBkb3RzOiBmYWxzZSxcclxuXHQgICAgICBuYXY6IGZhbHNlLFxyXG5cdCAgICAgIG1vdXNlRHJhZzogZmFsc2UsXHJcblx0ICAgICAgdG91Y2hEcmFnOiBmYWxzZSxcclxuXHQgICAgICBwdWxsRHJhZzogZmFsc2UsXHJcblx0ICAgICAgZnJlZURyYWc6IGZhbHNlLFxyXG5cdCAgICAgIG5hdlRleHQ6Wyc8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLWxlZnRcIj48L2k+JywnPGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1yaWdodFwiPjwvaT4nXSxcclxuXHQgICAgICByZXNwb25zaXZlOntcclxuXHQgICAgICAgICAgMDp7XHJcblx0ICAgICAgICAgICAgaXRlbXM6MixcclxuXHQgICAgICAgICAgfSxcclxuXHQgICAgICAgICAgNDgwOntcclxuXHQgICAgICAgICAgICBpdGVtczoyLFxyXG5cdCAgICAgICAgICB9LFxyXG5cdCAgICAgICAgICA3Njg6e1xyXG5cdCAgICAgICAgICAgIGl0ZW1zOjIsXHJcblx0ICAgICAgICAgIH0sXHJcblx0ICAgICAgICAgIDk5Mjp7XHJcblx0ICAgICAgICAgICAgaXRlbXM6NSxcclxuXHQgICAgICAgICAgfSxcclxuXHQgICAgICAgICAgMTIwMDp7XHJcblx0ICAgICAgICAgICAgaXRlbXM6NSxcclxuXHQgICAgICAgICAgfVxyXG5cdCAgICAgICAgfVxyXG5cdCAgICB9KTtcclxuXHQgIH0sXHJcblx0bWV0aG9kczp7XHJcblx0XHRzZWxlY3RJdGVtKGlkKXtcclxuXHRcdFx0dGhpcy5zZWxlY3RlZC5wdXNoKGlkKVxyXG5cdFx0fSxcclxuICAgICAgICBhZGRUb0NhcnQoaWQsbmFtZSl7XHJcblx0ICAgICAgICBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2FkZCcse1xyXG5cdCAgICAgICAgICAgIHBhcmFtczoge1xyXG5cdCAgICAgICAgICAgICAgICBwcm9kX2lkOmlkLFxyXG5cdCAgICAgICAgICAgICAgICBxdHk6MVxyXG5cdCAgICAgICAgICAgIH1cclxuXHQgICAgICAgIH0pXHJcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG5cdCAgICAgICAgICAgICQoXCIjY2FydC1jb3VudCwgI2NhcnQtY291bnQtbW9iaWxlXCIpLnRleHQocmVzdWx0LmRhdGEuY2FydF9pdGVtX2NvdW50KTtcclxuICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLmVycm9yX21zZylcclxuICAgICAgICAgICAgICAgIHtcdFxyXG4gICAgICAgICAgICAgICAgXHRpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2c9PSdSZWRpcmVjdCcpXHJcbiAgICAgICAgICAgICAgICBcdFx0d2luZG93LmxvY2F0aW9uID0gJy9wcm9kdWN0LycgKyByZXN1bHQuZGF0YS5zbHVnO1xyXG4gICAgICAgICAgICAgICAgXHRlbHNlXHJcblx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQnXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG5cdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmdbJ2FkZGVkLWNhcnQnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYWRkVG9Db21wYXJlKGlkLG5hbWUpe1xyXG4gICAgICAgICAgICBpZihMYXJhdmVsLmlzQXV0aGVudGljYXRlZCA9PT0gdHJ1ZSl7XHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2FkZHRvY29tcGFyZS8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuXHRcdCAgICAgICAgICAgICAgICB2YXIgY29tcCA9IHBhcnNlSW50KCQoXCIjY29tcC1jb3VudFwiKS50ZXh0KCkpKzE7XHJcblx0XHQgICAgICAgICAgICAgICAgJChcIiNjb21wLWNvdW50XCIpLnRleHQoY29tcCkuc2hvdygpO1xyXG5cdFx0ICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1snYWRkZWQtY29tcGFyaXNvbiddLCBuYW1lKTtcclxuXHRcdCAgICAgICAgICAgIH0gZWxzZSBpZihyZXN1bHQuZGF0YS5yZXNwb25zZT09Mykge1xyXG5cdFx0ICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHRoaXMubGFuZ1snZnVsbCddKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmluZm8odGhpcy5sYW5nWydhbHJlYWR5LWNvbXBhcmUnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydsb2dpbi1jb21wYXJlJ10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBhZGRUb0Zhdm9yaXRlcyhpZCxuYW1lKXtcclxuICAgICAgICAgICAgaWYoTGFyYXZlbC5pc0F1dGhlbnRpY2F0ZWQgPT09IHRydWUpe1xyXG4gICAgICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9hZGR0b2Zhdm9yaXRlcy8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuXHRcdCAgICAgICAgICAgICAgICB2YXIgZmF2ID0gcGFyc2VJbnQoJChcIiNmYXYtY291bnRcIikudGV4dCgpKSsxO1xyXG5cdFx0ICAgICAgICAgICAgICAgICQoXCIjZmF2LWNvdW50XCIpLnRleHQoZmF2KS5zaG93KCk7XHJcblx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nWydhZGRlZC1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcblx0XHQgICAgICAgICAgICB9IGVsc2UgaWYocmVzdWx0LmRhdGEucmVzcG9uc2U9PTIpe1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQtd2lzaGxpc3QnXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBcdHRvYXN0ci5pbmZvKHRoaXMubGFuZ1snYWxyZWFkeS1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydsb2dpbi1mYXZvcml0ZXMnXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblx0fVxyXG59KVxyXG5cclxuZnVuY3Rpb24gbnVtYmVyV2l0aENvbW1hcyh4KSB7XHJcbiAgICByZXR1cm4geC50b1N0cmluZygpLnJlcGxhY2UoL1xcQig/PShcXGR7M30pKyg/IVxcZCkpL2csIFwiLFwiKTtcclxufVxyXG5cclxuZnVuY3Rpb24gcm91bmRPZmYodikge1xyXG4gICAgcmV0dXJuIE1hdGgucm91bmQodik7XHJcbn1cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9wcm9kdWN0LmpzIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vUHJvZHVjdC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTUwMzg0NWQyXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1Byb2R1Y3QudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXGNvbW1vblxcXFxQcm9kdWN0LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFByb2R1Y3QudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTUwMzg0NWQyXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNTAzODQ1ZDJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NvbW1vbi9Qcm9kdWN0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzVcbi8vIG1vZHVsZSBjaHVua3MgPSAyNSAyNiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJveC1wcm9kdWN0LW91dGVyXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luXCI6IFwiMCBhdXRvXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJveC1wcm9kdWN0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1nLXdyYXBwZXJcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCJhdXRvXCJcbiAgICB9XG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdC8nICsgX3ZtLml0ZW0uc2x1Z1xuICAgIH1cbiAgfSwgW19jKCdpbWcnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1ndGh1bWJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJhbHRcIjogXCJQcm9kdWN0XCIsXG4gICAgICBcInNyY1wiOiAnL3Nob3BwaW5nL2ltYWdlcy9wcm9kdWN0LycgKyBfdm0uaXRlbS5pZCArICcvcHJpbWFyeS5qcGcnXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLml0ZW0uc2FsZV9wcmljZSkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhZ3NcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWwtdGFnc1wiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1kYW5nZXIgYXJyb3dlZFwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLmRpc2NvdW50KSArIFwiJVwiKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm9wdGlvblwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkFkZCB0byBDYXJ0XCIsXG4gICAgICBcImRhdGEtb3JpZ2luYWwtdGl0bGVcIjogXCJBZGQgdG8gQ2FydFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmFkZFRvQ2FydCgpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtc2hvcHBpbmctY2FydFwiXG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiQWRkIHRvIENvbXBhcmVcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5hZGRUb0NvbXBhcmUoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWFsaWduLWxlZnRcIlxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ3aXNobGlzdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiQWRkIHRvIFdpc2hsaXN0XCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uYWRkVG9GYXZvcml0ZXMoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWhlYXJ0XCJcbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdoNicsIFtfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwcm9kdWN0LW5hbWVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdC8nICsgX3ZtLml0ZW0uc2x1Z1xuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLm5hbWVsZW5ndGgpICsgXCJcXG4gICAgICBcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInByaWNlXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luLXRvcFwiOiBcIi01cHhcIixcbiAgICAgIFwiZm9udC1zaXplXCI6IFwiMTRweFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIFsoX3ZtLml0ZW0uZGlzY291bnQgJiYgX3ZtLml0ZW0uZGlzY291bnQgPiAwKSA/IF9jKCdzcGFuJywgW192bS5fdihcIlBcIiArIF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShwYXJzZUZsb2F0KFwiMFwiICsgX3ZtLnNhbGVwcmljZSkpKSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImRpc3BsYXlcIjogXCJibG9ja1wiXG4gICAgfVxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgY2xhc3M6IChfdm0uaXRlbS5kaXNjb3VudCAmJiBfdm0uaXRlbS5kaXNjb3VudCA+IDApID8gJ3ByaWNlLW9sZCcgOiAnJyxcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJtYXJnaW4tbGVmdFwiOiBcIjBweFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgUFwiICsgX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHBhcnNlRmxvYXQoXCIwXCIgKyBfdm0ucHJpY2UpKSkgKyBcIlxcbiAgICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5pdGVtLmRpc2NvdW50ID09IDApID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCIyMHB4XCIsXG4gICAgICBcImNsZWFyXCI6IFwiYm90aFwiXG4gICAgfVxuICB9KSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLml0ZW0uZGlzY291bnQgJiYgX3ZtLml0ZW0uZGlzY291bnQgPiAwKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImZvbnQtc2l6ZVwiOiBcIjEzcHhcIixcbiAgICAgIFwiY29sb3JcIjogXCIjNjY2XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCItXCIgKyBfdm0uX3MoX3ZtLml0ZW0uZGlzY291bnQpICsgXCIlXCIpXSkgOiBfdm0uX2UoKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyYXRpbmctYmxvY2tcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXhzbWFsbFwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXhzbWFsbC1yYXRpbmdcIixcbiAgICBzdHlsZTogKF92bS5wcm9kdWN0UmF0aW5nKVxuICB9KV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTUwMzg0NWQyXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNTAzODQ1ZDJcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9jb21tb24vUHJvZHVjdC52dWVcbi8vIG1vZHVsZSBpZCA9IDM5XG4vLyBtb2R1bGUgY2h1bmtzID0gMjUgMjYiXSwic291cmNlUm9vdCI6IiJ9