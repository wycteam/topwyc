/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 429);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 179:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('mfeedback', __webpack_require__(332));
Vue.component('feedback', __webpack_require__(333));

var app = new Vue({
	el: '#feedbacks_container',
	data: {
		feedbacks: [],
		lang: [],
		form: new Form({
			content: '',
			imageTemp: ''
		}, { baseURL: 'http://' + Laravel.base_api_url }),
		commentForm: new Form({
			content: '',
			parent_id: 0
		}, { baseURL: 'http://' + Laravel.base_api_url })
	},
	methods: {
		onInfinite: function onInfinite() {
			var _this = this;

			axiosAPIv1.get('/feedbacks', {
				params: {
					count: 4,
					skip: this.feedbacks.length
				}
			}).then(function (res) {
				if (res.data.length) {
					_this.feedbacks = _this.feedbacks.concat(res.data);
					_this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
					// if (this.feedbacks.length / 20 === 1) {
					//   this.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
					// }
					//for unli pages
				} else {
					_this.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
				}
			});
		},
		submitFeedback: function submitFeedback() {
			var _this2 = this;

			this.form.submit('post', '/v1/feedbacks').then(function (result) {
				if (result.success) {
					_this2.form.content = '';
					_this2.form.imageTemp = '';
					toastr.success(_this2.lang["description2"], _this2.lang["success-msg-feedback"]);
				} else {
					toastr.error(_this2.lang['try-again']);
				}
			}).catch($.noop);
		},
		submitComment: function submitComment() {
			var _this3 = this;

			var index;
			for (var i = 0; i < this.feedbacks.length; i++) {
				if (this.feedbacks[i].id == this.commentForm.parent_id) {
					index = i;
					break;
				}
			}
			this.commentForm.submit('post', '/v1/feedbacks').then(function (result) {
				if (result.success) {
					_this3.feedbacks[index].comments.unshift(result.data);
					toastr.success('', _this3.lang["success-msg-content"]);
					$('#modalComments').modal('toggle');
				} else {
					toastr.error(_this3.lang['try-again']);
				}
			}).catch($.noop);
		},


		handleCmdEnter: function handleCmdEnter(e) {
			if ((e.metaKey || e.ctrlKey) && e.keyCode == 13) {
				this.submitFeedback();
			}
		}

	},

	created: function created() {
		var _this4 = this;

		Event.listen('Imgfileupload.submitFeedbackImage', function (data) {
			_this4.form.imageTemp = data;
		});

		axios.get('/translate/feedback').then(function (language) {
			_this4.lang = language.data.data;
		});
	}
});

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['comment']
});

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['feedback', 'likes'],
	data: function data() {
		return {
			user_id: Laravel.user.id,
			feedbacklike_id: false,
			liked: false,
			message: "Like"
		};
	},
	watch: {
		message: function message(newMsg) {
			$('#btnLike' + this.feedback).tooltip('hide').attr('data-original-title', newMsg).tooltip('fixTitle');
		}
	},
	created: function created() {
		var _this = this;

		var lang = this.$parent.$parent.lang;
		axiosAPIv1.get('/feedback-likes?user=' + this.user_id + '&feedback=' + this.feedback).then(function (res) {
			if (res.data.length) {
				_this.feedbacklike_id = res.data[0].id;
				_this.liked = true;
				if (_this.liked) {
					if (_this.likes == "1") _this.message = lang['you-liked-this'];else {
						var like = _this.likes - 1;
						_this.likes = like;
						_this.message = lang['you-and'] + " " + like + " " + lang['others-liked-this'];
					}
				} else {
					if (_this.likes == "0") _this.message = lang["like"];else {
						var like = _this.likes - 1;
						_this.likes = like;
						_this.message = like + " " + lang['others-liked-this'];
					}
				}
			} else {
				$('[data-toggle="tooltip"]').tooltip();
			}
		});
	},

	methods: {
		likeFeedback: function likeFeedback() {
			var _this2 = this;

			var lang = this.$parent.$parent.lang;
			if (this.liked) {
				//update or search
				axiosAPIv1.get('/feedback-likes?user=' + this.user_id + '&feedback=' + this.feedback).then(function (res) {
					if (res.data.length) {
						_this2.feedbacklike_id = res.data[0].id;
						_this2.liked = true;
						axiosAPIv1.delete('/feedback-likes/' + _this2.feedbacklike_id).then(function (res) {
							_this2.liked = false;
							toastr.success('', lang['unliked']);
							if (_this2.likes == "0") _this2.message = lang["like"];else _this2.message = _this2.likes + " " + lang['others-liked-this'];
						});
					}
				});
			} else {
				axiosAPIv1.post('/feedback-likes', {
					user_id: this.user_id,
					feedback_id: this.feedback
				}).then(function (res) {
					_this2.liked = true;
					toastr.success('', lang['liked']);
					if (_this2.likes == "0") _this2.message = lang['you-liked-this'];else _this2.message = lang['you-and'] + " " + _this2.likes + " " + lang['others-liked-this'];
				});
			}
		}
	}
});

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading__);



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      list: []
    };
  },

  methods: {},
  components: {
    InfiniteLoading: __WEBPACK_IMPORTED_MODULE_0_vue_infinite_loading___default.a
  }
});

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

Vue.component('like', __webpack_require__(331));
Vue.component('comment', __webpack_require__(330));

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['feedback'],
    data: function data() {
        return {
            commentsShown: 2
        };
    },
    methods: {
        openCommentsModal: function openCommentsModal(id) {
            $('#modalComments').modal('toggle');
            this.$parent.commentForm.parent_id = id;
        },
        viewMore: function viewMore() {
            this.commentsShown += this.feedback.comments.length - this.commentsShown > 2 ? 2 : this.feedback.comments.length - this.commentsShown;
        },
        viewLess: function viewLess() {
            if (this.commentsShown <= 2) return;
            this.commentsShown -= 2;
        }
    }

});

/***/ }),

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(265),
  /* template */
  __webpack_require__(392),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\feedback\\Comment.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Comment.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6a2f27be", Component.options)
  } else {
    hotAPI.reload("data-v-6a2f27be", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 331:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(266),
  /* template */
  __webpack_require__(383),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\feedback\\Like.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Like.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4ece8776", Component.options)
  } else {
    hotAPI.reload("data-v-4ece8776", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 332:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(267),
  /* template */
  null,
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\feedback\\MoreFeedback.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c568d9b0", Component.options)
  } else {
    hotAPI.reload("data-v-c568d9b0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(268),
  /* template */
  __webpack_require__(366),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\feedback\\feedback.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] feedback.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-169f7bda", Component.options)
  } else {
    hotAPI.reload("data-v-169f7bda", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 366:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('div', {
    staticClass: "row user-feedback"
  }, [(!_vm.feedback.comments.length) ? _c('div', {
    staticClass: "abs to-partial-right to-full-bottom"
  }, [_c('button', {
    staticClass: "btn btn-fab btn-ripple btn-info",
    attrs: {
      "id": "btnAddCommentModal"
    },
    on: {
      "click": function($event) {
        _vm.openCommentsModal(_vm.feedback.id)
      }
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("add")])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "col-lg-1 col-md-2 col-sm-2 col-xs-3 text-center  "
  }, [(_vm.feedback.user.avatar) ? _c('img', {
    staticClass: "img img-circle fb-user-image",
    attrs: {
      "src": _vm.feedback.user.avatar
    }
  }) : _c('div', {
    staticClass: "no-avatar"
  }, [_vm._v("\n                        " + _vm._s(_vm.feedback.user.initials) + "\n                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-11  col-md-10 col-sm-10 col-xs-9"
  }, [_c('div', {
    staticClass: "fb-holder"
  }, [(_vm.feedback.comments.length) ? _c('div', {
    staticClass: "abs to-right to-full-bottom"
  }, [_c('button', {
    staticClass: "btn btn-fab btn-ripple btn-info",
    attrs: {
      "id": "btnAddCommentModal"
    },
    on: {
      "click": function($event) {
        _vm.openCommentsModal(_vm.feedback.id)
      }
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("add")])])]) : _vm._e(), _vm._v(" "), _c('span', {
    staticClass: "fb-name popover-details",
    attrs: {
      "data-id": _vm.feedback.user.id
    }
  }, [_vm._v("\n                            " + _vm._s(_vm.feedback.user.full_name) + "\n                        ")]), _vm._v(" "), _c('div', {
    staticClass: "datetime"
  }, [_vm._v("\n                            " + _vm._s(_vm._f("friendly-date")(_vm.feedback.created_at)) + "\n                            "), _c('like', {
    attrs: {
      "feedback": _vm.feedback.id,
      "likes": _vm.feedback.likes_count
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "fb-content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [(_vm.feedback.image) ? _c('div', {
    staticClass: "col-lg-2 col-md-2 col-sm-2 col-xs-12"
  }, [_c('img', {
    staticClass: "img-responsive",
    attrs: {
      "src": '/' + _vm.feedback.image
    }
  })]) : _vm._e(), _vm._v(" "), _c('div', {
    class: _vm.feedback.image ? 'col-lg-10 col-md-10 col-sm-10 col-xs-12' : 'col-lg-12'
  }, [_vm._v("\n                                    " + _vm._s(_vm.feedback.content) + "\n                                ")])])])]), _vm._v(" "), _c('div', {
    staticClass: "fb-comments-holder"
  }, [_vm._l((_vm.feedback.comments), function(comment, $index) {
    return ($index < _vm.commentsShown) ? _c('comment', {
      key: comment.id,
      attrs: {
        "comment": comment
      }
    }) : _vm._e()
  }), _vm._v(" "), ((_vm.feedback.comments.length > _vm.commentsShown) || (_vm.commentsShown > 2)) ? _c('div', {
    staticClass: "col-lg-12 user-feedback text-right"
  }, [(_vm.feedback.comments.length > _vm.commentsShown) ? _c('button', {
    staticClass: "btn btn-info btn-raised btn-has-loading",
    on: {
      "click": _vm.viewMore
    }
  }, [_vm._v(_vm._s(this.$parent.lang["view-more-replies"]))]) : _vm._e(), _vm._v(" "), (_vm.commentsShown > 2) ? _c('button', {
    staticClass: "btn btn-info btn-raised btn-has-loading",
    on: {
      "click": _vm.viewLess
    }
  }, [_vm._v(_vm._s(this.$parent.lang["view-less-replies"]))]) : _vm._e()]) : _vm._e()], 2)])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-169f7bda", module.exports)
  }
}

/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('a', {
    staticClass: "btn btn-has-loading btn-like btn-fab btn-fab-mini ",
    class: _vm.liked ? "liked" : "not-liked",
    attrs: {
      "href": "#" + _vm.feedback,
      "data-toggle": "tooltip",
      "data-placement": "bottom",
      "data-title": _vm.message,
      "id": "btnLike" + _vm.feedback
    },
    on: {
      "click": _vm.likeFeedback
    }
  }, [_c('span', {
    staticClass: "fa fa-thumbs-up"
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4ece8776", module.exports)
  }
}

/***/ }),

/***/ 392:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-lg-12 user-feedback"
  }, [_c('div', {
    staticClass: "col-lg-1 text-center"
  }, [(_vm.comment.user.avatar) ? _c('img', {
    staticClass: "img img-circle fb-user-image",
    attrs: {
      "src": _vm.comment.user.avatar
    }
  }) : _c('div', {
    staticClass: "no-avatar"
  }, [_vm._v("\n                " + _vm._s(_vm.comment.user.initials) + "\n            ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-11"
  }, [_c('span', {
    staticClass: "fb-name  popover-details",
    attrs: {
      "data-id": _vm.comment.user.id
    }
  }, [_vm._v("\n\t            " + _vm._s(_vm.comment.user.full_name) + "\n\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "datetime"
  }, [_vm._v("\n\t            " + _vm._s(_vm.comment.created_at) + "\n\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "fb-content"
  }, [_vm._v("\n\t            " + _vm._s(_vm.comment.content) + "\n\t        ")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6a2f27be", module.exports)
  }
}

/***/ }),

/***/ 429:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(179);


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

!function(p,x){ true?module.exports=x():"function"==typeof define&&define.amd?define([],x):"object"==typeof exports?exports.VueInfiniteLoading=x():p.VueInfiniteLoading=x()}(this,function(){return function(p){function x(a){if(t[a])return t[a].exports;var e=t[a]={exports:{},id:a,loaded:!1};return p[a].call(e.exports,e,e.exports,x),e.loaded=!0,e.exports}var t={};return x.m=p,x.c=t,x.p="/",x(0)}([function(p,x,t){"use strict";function a(p){return p&&p.__esModule?p:{default:p}}Object.defineProperty(x,"__esModule",{value:!0});var e=t(4),o=a(e);x.default=o.default,"undefined"!=typeof window&&window.Vue&&window.Vue.component("infinite-loading",o.default)},function(p,x){"use strict";function t(p){return"BODY"===p.tagName?window:["scroll","auto"].indexOf(getComputedStyle(p).overflowY)>-1?p:p.hasAttribute("infinite-wrapper")||p.hasAttribute("data-infinite-wrapper")?p:t(p.parentNode)}function a(p,x,t){var a=void 0;if("top"===t)a=isNaN(p.scrollTop)?p.pageYOffset:p.scrollTop;else{var e=x.getBoundingClientRect().top,o=p===window?window.innerHeight:p.getBoundingClientRect().bottom;a=e-o}return a}Object.defineProperty(x,"__esModule",{value:!0});var e={bubbles:"loading-bubbles",circles:"loading-circles",default:"loading-default",spiral:"loading-spiral",waveDots:"loading-wave-dots"};x.default={data:function(){return{scrollParent:null,scrollHandler:null,isLoading:!1,isComplete:!1,isFirstLoad:!0}},computed:{spinnerType:function(){return e[this.spinner]||e.default}},props:{distance:{type:Number,default:100},onInfinite:Function,spinner:String,direction:{type:String,default:"bottom"}},mounted:function(){var p=this;this.scrollParent=t(this.$el),this.scrollHandler=function(){this.isLoading||this.attemptLoad()}.bind(this),setTimeout(this.scrollHandler,1),this.scrollParent.addEventListener("scroll",this.scrollHandler),this.$on("$InfiniteLoading:loaded",function(){p.isFirstLoad=!1,p.isLoading&&p.$nextTick(p.attemptLoad)}),this.$on("$InfiniteLoading:complete",function(){p.isLoading=!1,p.isComplete=!0,p.scrollParent.removeEventListener("scroll",p.scrollHandler)}),this.$on("$InfiniteLoading:reset",function(){p.isLoading=!1,p.isComplete=!1,p.isFirstLoad=!0,p.scrollParent.addEventListener("scroll",p.scrollHandler),setTimeout(p.scrollHandler,1)})},deactivated:function(){this.isLoading=!1,this.scrollParent.removeEventListener("scroll",this.scrollHandler)},activated:function(){this.scrollParent.addEventListener("scroll",this.scrollHandler)},methods:{attemptLoad:function(){var p=a(this.scrollParent,this.$el,this.direction);!this.isComplete&&p<=this.distance?(this.isLoading=!0,this.onInfinite.call()):this.isLoading=!1}},destroyed:function(){this.isComplete||this.scrollParent.removeEventListener("scroll",this.scrollHandler)}}},function(p,x,t){x=p.exports=t(3)(),x.push([p.id,'.loading-wave-dots[data-v-50793f02]{position:relative}.loading-wave-dots[data-v-50793f02]:before{content:"";position:absolute;top:50%;left:50%;margin-left:-4px;margin-top:-4px;width:8px;height:8px;background-color:#bbb;border-radius:50%;-webkit-animation:linear loading-wave-dots 2.8s infinite;animation:linear loading-wave-dots 2.8s infinite}@-webkit-keyframes loading-wave-dots{0%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}5%{box-shadow:-32px -4px 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}10%{box-shadow:-32px -6px 0 #999,-16px -4px 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}15%{box-shadow:-32px 2px 0 #bbb,-16px -2px 0 #999,16px 4px 0 #bbb,32px 4px 0 #bbb;-webkit-transform:translateY(-4px);transform:translateY(-4px);background-color:#bbb}20%{box-shadow:-32px 6px 0 #bbb,-16px 4px 0 #bbb,16px 2px 0 #bbb,32px 6px 0 #bbb;-webkit-transform:translateY(-6px);transform:translateY(-6px);background-color:#999}25%{box-shadow:-32px 2px 0 #bbb,-16px 2px 0 #bbb,16px -4px 0 #999,32px -2px 0 #bbb;-webkit-transform:translateY(-2px);transform:translateY(-2px);background-color:#bbb}30%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px -2px 0 #bbb,32px -6px 0 #999;-webkit-transform:translateY(0);transform:translateY(0)}35%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px -2px 0 #bbb}40%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}to{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}}@keyframes loading-wave-dots{0%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}5%{box-shadow:-32px -4px 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}10%{box-shadow:-32px -6px 0 #999,-16px -4px 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb;-webkit-transform:translateY(0);transform:translateY(0)}15%{box-shadow:-32px 2px 0 #bbb,-16px -2px 0 #999,16px 4px 0 #bbb,32px 4px 0 #bbb;-webkit-transform:translateY(-4px);transform:translateY(-4px);background-color:#bbb}20%{box-shadow:-32px 6px 0 #bbb,-16px 4px 0 #bbb,16px 2px 0 #bbb,32px 6px 0 #bbb;-webkit-transform:translateY(-6px);transform:translateY(-6px);background-color:#999}25%{box-shadow:-32px 2px 0 #bbb,-16px 2px 0 #bbb,16px -4px 0 #999,32px -2px 0 #bbb;-webkit-transform:translateY(-2px);transform:translateY(-2px);background-color:#bbb}30%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px -2px 0 #bbb,32px -6px 0 #999;-webkit-transform:translateY(0);transform:translateY(0)}35%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px -2px 0 #bbb}40%{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}to{box-shadow:-32px 0 0 #bbb,-16px 0 0 #bbb,16px 0 0 #bbb,32px 0 0 #bbb}}.loading-circles[data-v-50793f02]{position:relative}.loading-circles[data-v-50793f02]:before{content:"";position:absolute;left:50%;top:50%;margin-top:-2.5px;margin-left:-2.5px;width:5px;height:5px;border-radius:50%;-webkit-animation:linear loading-circles .75s infinite;animation:linear loading-circles .75s infinite}@-webkit-keyframes loading-circles{0%{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}12.5%{box-shadow:0 -12px 0 #dfdfdf,8.52px -8.52px 0 #505050,12px 0 0 #646464,8.52px 8.52px 0 #797979,0 12px 0 #8d8d8d,-8.52px 8.52px 0 #a2a2a2,-12px 0 0 #b6b6b6,-8.52px -8.52px 0 #cacaca}25%{box-shadow:0 -12px 0 #cacaca,8.52px -8.52px 0 #dfdfdf,12px 0 0 #505050,8.52px 8.52px 0 #646464,0 12px 0 #797979,-8.52px 8.52px 0 #8d8d8d,-12px 0 0 #a2a2a2,-8.52px -8.52px 0 #b6b6b6}37.5%{box-shadow:0 -12px 0 #b6b6b6,8.52px -8.52px 0 #cacaca,12px 0 0 #dfdfdf,8.52px 8.52px 0 #505050,0 12px 0 #646464,-8.52px 8.52px 0 #797979,-12px 0 0 #8d8d8d,-8.52px -8.52px 0 #a2a2a2}50%{box-shadow:0 -12px 0 #a2a2a2,8.52px -8.52px 0 #b6b6b6,12px 0 0 #cacaca,8.52px 8.52px 0 #dfdfdf,0 12px 0 #505050,-8.52px 8.52px 0 #646464,-12px 0 0 #797979,-8.52px -8.52px 0 #8d8d8d}62.5%{box-shadow:0 -12px 0 #8d8d8d,8.52px -8.52px 0 #a2a2a2,12px 0 0 #b6b6b6,8.52px 8.52px 0 #cacaca,0 12px 0 #dfdfdf,-8.52px 8.52px 0 #505050,-12px 0 0 #646464,-8.52px -8.52px 0 #797979}75%{box-shadow:0 -12px 0 #797979,8.52px -8.52px 0 #8d8d8d,12px 0 0 #a2a2a2,8.52px 8.52px 0 #b6b6b6,0 12px 0 #cacaca,-8.52px 8.52px 0 #dfdfdf,-12px 0 0 #505050,-8.52px -8.52px 0 #646464}87.5%{box-shadow:0 -12px 0 #646464,8.52px -8.52px 0 #797979,12px 0 0 #8d8d8d,8.52px 8.52px 0 #a2a2a2,0 12px 0 #b6b6b6,-8.52px 8.52px 0 #cacaca,-12px 0 0 #dfdfdf,-8.52px -8.52px 0 #505050}to{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}}@keyframes loading-circles{0%{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}12.5%{box-shadow:0 -12px 0 #dfdfdf,8.52px -8.52px 0 #505050,12px 0 0 #646464,8.52px 8.52px 0 #797979,0 12px 0 #8d8d8d,-8.52px 8.52px 0 #a2a2a2,-12px 0 0 #b6b6b6,-8.52px -8.52px 0 #cacaca}25%{box-shadow:0 -12px 0 #cacaca,8.52px -8.52px 0 #dfdfdf,12px 0 0 #505050,8.52px 8.52px 0 #646464,0 12px 0 #797979,-8.52px 8.52px 0 #8d8d8d,-12px 0 0 #a2a2a2,-8.52px -8.52px 0 #b6b6b6}37.5%{box-shadow:0 -12px 0 #b6b6b6,8.52px -8.52px 0 #cacaca,12px 0 0 #dfdfdf,8.52px 8.52px 0 #505050,0 12px 0 #646464,-8.52px 8.52px 0 #797979,-12px 0 0 #8d8d8d,-8.52px -8.52px 0 #a2a2a2}50%{box-shadow:0 -12px 0 #a2a2a2,8.52px -8.52px 0 #b6b6b6,12px 0 0 #cacaca,8.52px 8.52px 0 #dfdfdf,0 12px 0 #505050,-8.52px 8.52px 0 #646464,-12px 0 0 #797979,-8.52px -8.52px 0 #8d8d8d}62.5%{box-shadow:0 -12px 0 #8d8d8d,8.52px -8.52px 0 #a2a2a2,12px 0 0 #b6b6b6,8.52px 8.52px 0 #cacaca,0 12px 0 #dfdfdf,-8.52px 8.52px 0 #505050,-12px 0 0 #646464,-8.52px -8.52px 0 #797979}75%{box-shadow:0 -12px 0 #797979,8.52px -8.52px 0 #8d8d8d,12px 0 0 #a2a2a2,8.52px 8.52px 0 #b6b6b6,0 12px 0 #cacaca,-8.52px 8.52px 0 #dfdfdf,-12px 0 0 #505050,-8.52px -8.52px 0 #646464}87.5%{box-shadow:0 -12px 0 #646464,8.52px -8.52px 0 #797979,12px 0 0 #8d8d8d,8.52px 8.52px 0 #a2a2a2,0 12px 0 #b6b6b6,-8.52px 8.52px 0 #cacaca,-12px 0 0 #dfdfdf,-8.52px -8.52px 0 #505050}to{box-shadow:0 -12px 0 #505050,8.52px -8.52px 0 #646464,12px 0 0 #797979,8.52px 8.52px 0 #8d8d8d,0 12px 0 #a2a2a2,-8.52px 8.52px 0 #b6b6b6,-12px 0 0 #cacaca,-8.52px -8.52px 0 #dfdfdf}}.loading-bubbles[data-v-50793f02]{position:relative}.loading-bubbles[data-v-50793f02]:before{content:"";position:absolute;left:50%;top:50%;margin-top:-.5px;margin-left:-.5px;width:1px;height:1px;border-radius:50%;-webkit-animation:linear loading-bubbles .85s infinite;animation:linear loading-bubbles .85s infinite}@-webkit-keyframes loading-bubbles{0%{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}12.5%{box-shadow:0 -12px 0 3.2px #666,8.52px -8.52px 0 .4px #666,12px 0 0 .8px #666,8.52px 8.52px 0 1.2px #666,0 12px 0 1.6px #666,-8.52px 8.52px 0 2px #666,-12px 0 0 2.4px #666,-8.52px -8.52px 0 2.8px #666}25%{box-shadow:0 -12px 0 2.8px #666,8.52px -8.52px 0 3.2px #666,12px 0 0 .4px #666,8.52px 8.52px 0 .8px #666,0 12px 0 1.2px #666,-8.52px 8.52px 0 1.6px #666,-12px 0 0 2px #666,-8.52px -8.52px 0 2.4px #666}37.5%{box-shadow:0 -12px 0 2.4px #666,8.52px -8.52px 0 2.8px #666,12px 0 0 3.2px #666,8.52px 8.52px 0 .4px #666,0 12px 0 .8px #666,-8.52px 8.52px 0 1.2px #666,-12px 0 0 1.6px #666,-8.52px -8.52px 0 2px #666}50%{box-shadow:0 -12px 0 2px #666,8.52px -8.52px 0 2.4px #666,12px 0 0 2.8px #666,8.52px 8.52px 0 3.2px #666,0 12px 0 .4px #666,-8.52px 8.52px 0 .8px #666,-12px 0 0 1.2px #666,-8.52px -8.52px 0 1.6px #666}62.5%{box-shadow:0 -12px 0 1.6px #666,8.52px -8.52px 0 2px #666,12px 0 0 2.4px #666,8.52px 8.52px 0 2.8px #666,0 12px 0 3.2px #666,-8.52px 8.52px 0 .4px #666,-12px 0 0 .8px #666,-8.52px -8.52px 0 1.2px #666}75%{box-shadow:0 -12px 0 1.2px #666,8.52px -8.52px 0 1.6px #666,12px 0 0 2px #666,8.52px 8.52px 0 2.4px #666,0 12px 0 2.8px #666,-8.52px 8.52px 0 3.2px #666,-12px 0 0 .4px #666,-8.52px -8.52px 0 .8px #666}87.5%{box-shadow:0 -12px 0 .8px #666,8.52px -8.52px 0 1.2px #666,12px 0 0 1.6px #666,8.52px 8.52px 0 2px #666,0 12px 0 2.4px #666,-8.52px 8.52px 0 2.8px #666,-12px 0 0 3.2px #666,-8.52px -8.52px 0 .4px #666}to{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}}@keyframes loading-bubbles{0%{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}12.5%{box-shadow:0 -12px 0 3.2px #666,8.52px -8.52px 0 .4px #666,12px 0 0 .8px #666,8.52px 8.52px 0 1.2px #666,0 12px 0 1.6px #666,-8.52px 8.52px 0 2px #666,-12px 0 0 2.4px #666,-8.52px -8.52px 0 2.8px #666}25%{box-shadow:0 -12px 0 2.8px #666,8.52px -8.52px 0 3.2px #666,12px 0 0 .4px #666,8.52px 8.52px 0 .8px #666,0 12px 0 1.2px #666,-8.52px 8.52px 0 1.6px #666,-12px 0 0 2px #666,-8.52px -8.52px 0 2.4px #666}37.5%{box-shadow:0 -12px 0 2.4px #666,8.52px -8.52px 0 2.8px #666,12px 0 0 3.2px #666,8.52px 8.52px 0 .4px #666,0 12px 0 .8px #666,-8.52px 8.52px 0 1.2px #666,-12px 0 0 1.6px #666,-8.52px -8.52px 0 2px #666}50%{box-shadow:0 -12px 0 2px #666,8.52px -8.52px 0 2.4px #666,12px 0 0 2.8px #666,8.52px 8.52px 0 3.2px #666,0 12px 0 .4px #666,-8.52px 8.52px 0 .8px #666,-12px 0 0 1.2px #666,-8.52px -8.52px 0 1.6px #666}62.5%{box-shadow:0 -12px 0 1.6px #666,8.52px -8.52px 0 2px #666,12px 0 0 2.4px #666,8.52px 8.52px 0 2.8px #666,0 12px 0 3.2px #666,-8.52px 8.52px 0 .4px #666,-12px 0 0 .8px #666,-8.52px -8.52px 0 1.2px #666}75%{box-shadow:0 -12px 0 1.2px #666,8.52px -8.52px 0 1.6px #666,12px 0 0 2px #666,8.52px 8.52px 0 2.4px #666,0 12px 0 2.8px #666,-8.52px 8.52px 0 3.2px #666,-12px 0 0 .4px #666,-8.52px -8.52px 0 .8px #666}87.5%{box-shadow:0 -12px 0 .8px #666,8.52px -8.52px 0 1.2px #666,12px 0 0 1.6px #666,8.52px 8.52px 0 2px #666,0 12px 0 2.4px #666,-8.52px 8.52px 0 2.8px #666,-12px 0 0 3.2px #666,-8.52px -8.52px 0 .4px #666}to{box-shadow:0 -12px 0 .4px #666,8.52px -8.52px 0 .8px #666,12px 0 0 1.2px #666,8.52px 8.52px 0 1.6px #666,0 12px 0 2px #666,-8.52px 8.52px 0 2.4px #666,-12px 0 0 2.8px #666,-8.52px -8.52px 0 3.2px #666}}.loading-default[data-v-50793f02]{position:relative;border:1px solid #999;-webkit-animation:ease loading-rotating 1.5s infinite;animation:ease loading-rotating 1.5s infinite}.loading-default[data-v-50793f02]:before{content:"";position:absolute;display:block;top:0;left:50%;margin-top:-3px;margin-left:-3px;width:6px;height:6px;background-color:#999;border-radius:50%}.loading-spiral[data-v-50793f02]{border:2px solid #777;border-right-color:transparent;-webkit-animation:linear loading-rotating .85s infinite;animation:linear loading-rotating .85s infinite}@-webkit-keyframes loading-rotating{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes loading-rotating{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}.infinite-loading-container[data-v-50793f02]{clear:both;text-align:center}.infinite-loading-container [class^=loading-][data-v-50793f02]{display:inline-block;margin:15px 0;width:28px;height:28px;font-size:28px;line-height:28px;border-radius:50%}.infinite-status-prompt[data-v-50793f02]{color:#666;font-size:14px;text-align:center;padding:10px 0}',""])},function(p,x){p.exports=function(){var p=[];return p.toString=function(){for(var p=[],x=0;x<this.length;x++){var t=this[x];t[2]?p.push("@media "+t[2]+"{"+t[1]+"}"):p.push(t[1])}return p.join("")},p.i=function(x,t){"string"==typeof x&&(x=[[null,x,""]]);for(var a={},e=0;e<this.length;e++){var o=this[e][0];"number"==typeof o&&(a[o]=!0)}for(e=0;e<x.length;e++){var n=x[e];"number"==typeof n[0]&&a[n[0]]||(t&&!n[2]?n[2]=t:t&&(n[2]="("+n[2]+") and ("+t+")"),p.push(n))}},p}},function(p,x,t){var a,e;t(7),a=t(1);var o=t(5);e=a=a||{},"object"!=typeof a.default&&"function"!=typeof a.default||(e=a=a.default),"function"==typeof e&&(e=e.options),e.render=o.render,e.staticRenderFns=o.staticRenderFns,e._scopeId="data-v-50793f02",p.exports=a},function(p,x){p.exports={render:function(){var p=this,x=p.$createElement,t=p._self._c||x;return t("div",{staticClass:"infinite-loading-container"},[t("div",{directives:[{name:"show",rawName:"v-show",value:p.isLoading,expression:"isLoading"}]},[p._t("spinner",[t("i",{class:p.spinnerType})])],2),p._v(" "),t("div",{directives:[{name:"show",rawName:"v-show",value:!p.isLoading&&p.isComplete&&p.isFirstLoad,expression:"!isLoading && isComplete && isFirstLoad"}],staticClass:"infinite-status-prompt"},[p._t("no-results",[p._v("No results :(")])],2),p._v(" "),t("div",{directives:[{name:"show",rawName:"v-show",value:!p.isLoading&&p.isComplete&&!p.isFirstLoad,expression:"!isLoading && isComplete && !isFirstLoad"}],staticClass:"infinite-status-prompt"},[p._t("no-more",[p._v("No more data :)")])],2)])},staticRenderFns:[]}},function(p,x,t){function a(p,x){for(var t=0;t<p.length;t++){var a=p[t],e=d[a.id];if(e){e.refs++;for(var o=0;o<e.parts.length;o++)e.parts[o](a.parts[o]);for(;o<a.parts.length;o++)e.parts.push(r(a.parts[o],x))}else{for(var n=[],o=0;o<a.parts.length;o++)n.push(r(a.parts[o],x));d[a.id]={id:a.id,refs:1,parts:n}}}}function e(p){for(var x=[],t={},a=0;a<p.length;a++){var e=p[a],o=e[0],n=e[1],i=e[2],r=e[3],s={css:n,media:i,sourceMap:r};t[o]?t[o].parts.push(s):x.push(t[o]={id:o,parts:[s]})}return x}function o(p,x){var t=c(),a=m[m.length-1];if("top"===p.insertAt)a?a.nextSibling?t.insertBefore(x,a.nextSibling):t.appendChild(x):t.insertBefore(x,t.firstChild),m.push(x);else{if("bottom"!==p.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");t.appendChild(x)}}function n(p){p.parentNode.removeChild(p);var x=m.indexOf(p);x>=0&&m.splice(x,1)}function i(p){var x=document.createElement("style");return x.type="text/css",o(p,x),x}function r(p,x){var t,a,e;if(x.singleton){var o=h++;t=u||(u=i(x)),a=s.bind(null,t,o,!1),e=s.bind(null,t,o,!0)}else t=i(x),a=b.bind(null,t),e=function(){n(t)};return a(p),function(x){if(x){if(x.css===p.css&&x.media===p.media&&x.sourceMap===p.sourceMap)return;a(p=x)}else e()}}function s(p,x,t,a){var e=t?"":a.css;if(p.styleSheet)p.styleSheet.cssText=g(x,e);else{var o=document.createTextNode(e),n=p.childNodes;n[x]&&p.removeChild(n[x]),n.length?p.insertBefore(o,n[x]):p.appendChild(o)}}function b(p,x){var t=x.css,a=x.media,e=x.sourceMap;if(a&&p.setAttribute("media",a),e&&(t+="\n/*# sourceURL="+e.sources[0]+" */",t+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(e))))+" */"),p.styleSheet)p.styleSheet.cssText=t;else{for(;p.firstChild;)p.removeChild(p.firstChild);p.appendChild(document.createTextNode(t))}}var d={},l=function(p){var x;return function(){return"undefined"==typeof x&&(x=p.apply(this,arguments)),x}},f=l(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),c=l(function(){return document.head||document.getElementsByTagName("head")[0]}),u=null,h=0,m=[];p.exports=function(p,x){x=x||{},"undefined"==typeof x.singleton&&(x.singleton=f()),"undefined"==typeof x.insertAt&&(x.insertAt="bottom");var t=e(p);return a(t,x),function(p){for(var o=[],n=0;n<t.length;n++){var i=t[n],r=d[i.id];r.refs--,o.push(r)}if(p){var s=e(p);a(s,x)}for(var n=0;n<o.length;n++){var r=o[n];if(0===r.refs){for(var b=0;b<r.parts.length;b++)r.parts[b]();delete d[r.id]}}}};var g=function(){var p=[];return function(x,t){return p[x]=t,p.filter(Boolean).join("\n")}}()},function(p,x,t){var a=t(2);"string"==typeof a&&(a=[[p.id,a,""]]);t(6)(a,{});a.locals&&(p.exports=a.locals)}])});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9mZWVkYmFjay5qcyIsIndlYnBhY2s6Ly8vQ29tbWVudC52dWU/YjA2MCIsIndlYnBhY2s6Ly8vTGlrZS52dWU/OGU3OSIsIndlYnBhY2s6Ly8vTW9yZUZlZWRiYWNrLnZ1ZSIsIndlYnBhY2s6Ly8vZmVlZGJhY2sudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9mZWVkYmFjay9Db21tZW50LnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvZmVlZGJhY2svTGlrZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2ZlZWRiYWNrL01vcmVGZWVkYmFjay52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2ZlZWRiYWNrL2ZlZWRiYWNrLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvZmVlZGJhY2svZmVlZGJhY2sudnVlPzE2OTkiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2ZlZWRiYWNrL0xpa2UudnVlP2IzNGYiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2ZlZWRiYWNrL0NvbW1lbnQudnVlP2JhZGYiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtaW5maW5pdGUtbG9hZGluZy9kaXN0L3Z1ZS1pbmZpbml0ZS1sb2FkaW5nLmpzPzgyYzUiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImFwcCIsImVsIiwiZGF0YSIsImZlZWRiYWNrcyIsImxhbmciLCJmb3JtIiwiRm9ybSIsImNvbnRlbnQiLCJpbWFnZVRlbXAiLCJiYXNlVVJMIiwiTGFyYXZlbCIsImJhc2VfYXBpX3VybCIsImNvbW1lbnRGb3JtIiwicGFyZW50X2lkIiwibWV0aG9kcyIsIm9uSW5maW5pdGUiLCJheGlvc0FQSXYxIiwiZ2V0IiwicGFyYW1zIiwiY291bnQiLCJza2lwIiwibGVuZ3RoIiwidGhlbiIsInJlcyIsImNvbmNhdCIsIiRyZWZzIiwiaW5maW5pdGVMb2FkaW5nIiwiJGVtaXQiLCJzdWJtaXRGZWVkYmFjayIsInN1Ym1pdCIsInJlc3VsdCIsInN1Y2Nlc3MiLCJ0b2FzdHIiLCJlcnJvciIsImNhdGNoIiwiJCIsIm5vb3AiLCJzdWJtaXRDb21tZW50IiwiaW5kZXgiLCJpIiwiaWQiLCJjb21tZW50cyIsInVuc2hpZnQiLCJtb2RhbCIsImhhbmRsZUNtZEVudGVyIiwiZSIsIm1ldGFLZXkiLCJjdHJsS2V5Iiwia2V5Q29kZSIsImNyZWF0ZWQiLCJFdmVudCIsImxpc3RlbiIsImF4aW9zIiwibGFuZ3VhZ2UiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQUEsSUFBSUMsU0FBSixDQUFjLFdBQWQsRUFBNEIsbUJBQUFDLENBQVEsR0FBUixDQUE1QjtBQUNBRixJQUFJQyxTQUFKLENBQWMsVUFBZCxFQUEyQixtQkFBQUMsQ0FBUSxHQUFSLENBQTNCOztBQUVBLElBQUlDLE1BQU0sSUFBSUgsR0FBSixDQUFRO0FBQ2pCSSxLQUFJLHNCQURhO0FBRWpCQyxPQUFNO0FBQ0xDLGFBQVksRUFEUDtBQUVMQyxRQUFLLEVBRkE7QUFHTEMsUUFBTSxJQUFJQyxJQUFKLENBQVM7QUFDYkMsWUFBUSxFQURLO0FBRWJDLGNBQVc7QUFGRSxHQUFULEVBR0gsRUFBRUMsU0FBUyxZQUFVQyxRQUFRQyxZQUE3QixFQUhHLENBSEQ7QUFPTEMsZUFBYSxJQUFJTixJQUFKLENBQVM7QUFDckJDLFlBQVEsRUFEYTtBQUVyQk0sY0FBVTtBQUZXLEdBQVQsRUFHVixFQUFFSixTQUFTLFlBQVVDLFFBQVFDLFlBQTdCLEVBSFU7QUFQUixFQUZXO0FBY2pCRyxVQUFRO0FBQ05DLFlBRE0sd0JBQ087QUFBQTs7QUFDYkMsY0FBV0MsR0FBWCxDQUFlLFlBQWYsRUFBNEI7QUFDM0JDLFlBQVE7QUFDTkMsWUFBTSxDQURBO0FBRU5DLFdBQUssS0FBS2pCLFNBQUwsQ0FBZWtCO0FBRmQ7QUFEbUIsSUFBNUIsRUFLR0MsSUFMSCxDQUtRLFVBQUNDLEdBQUQsRUFBUztBQUNoQixRQUFJQSxJQUFJckIsSUFBSixDQUFTbUIsTUFBYixFQUFxQjtBQUNwQixXQUFLbEIsU0FBTCxHQUFpQixNQUFLQSxTQUFMLENBQWVxQixNQUFmLENBQXNCRCxJQUFJckIsSUFBMUIsQ0FBakI7QUFDQSxXQUFLdUIsS0FBTCxDQUFXQyxlQUFYLENBQTJCQyxLQUEzQixDQUFpQyx5QkFBakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBUEQsTUFPTztBQUNOLFdBQUtGLEtBQUwsQ0FBV0MsZUFBWCxDQUEyQkMsS0FBM0IsQ0FBaUMsMkJBQWpDO0FBQ0E7QUFDRCxJQWhCRDtBQWlCRyxHQW5CRztBQXFCUEMsZ0JBckJPLDRCQXFCUztBQUFBOztBQUNmLFFBQUt2QixJQUFMLENBQVV3QixNQUFWLENBQWlCLE1BQWpCLEVBQXlCLGVBQXpCLEVBQ2NQLElBRGQsQ0FDbUIsa0JBQVU7QUFDWixRQUFHUSxPQUFPQyxPQUFWLEVBQ0E7QUFDTCxZQUFLMUIsSUFBTCxDQUFVRSxPQUFWLEdBQW9CLEVBQXBCO0FBQ0EsWUFBS0YsSUFBTCxDQUFVRyxTQUFWLEdBQXNCLEVBQXRCO0FBQ1R3QixZQUFPRCxPQUFQLENBQWUsT0FBSzNCLElBQUwsQ0FBVSxjQUFWLENBQWYsRUFBeUMsT0FBS0EsSUFBTCxDQUFVLHNCQUFWLENBQXpDO0FBQ2UsS0FMRCxNQU1BO0FBQ0M0QixZQUFPQyxLQUFQLENBQWEsT0FBSzdCLElBQUwsQ0FBVSxXQUFWLENBQWI7QUFDQTtBQUNKLElBWGQsRUFZYzhCLEtBWmQsQ0FZb0JDLEVBQUVDLElBWnRCO0FBYUEsR0FuQ007QUFxQ1BDLGVBckNPLDJCQXFDUTtBQUFBOztBQUNkLE9BQUlDLEtBQUo7QUFDQSxRQUFJLElBQUlDLElBQUUsQ0FBVixFQUFhQSxJQUFFLEtBQUtwQyxTQUFMLENBQWVrQixNQUE5QixFQUFzQ2tCLEdBQXRDLEVBQ007QUFDQyxRQUFHLEtBQUtwQyxTQUFMLENBQWVvQyxDQUFmLEVBQWtCQyxFQUFsQixJQUF3QixLQUFLNUIsV0FBTCxDQUFpQkMsU0FBNUMsRUFDQTtBQUNDeUIsYUFBUUMsQ0FBUjtBQUNBO0FBQ0E7QUFDRDtBQUNQLFFBQUszQixXQUFMLENBQWlCaUIsTUFBakIsQ0FBd0IsTUFBeEIsRUFBZ0MsZUFBaEMsRUFDY1AsSUFEZCxDQUNtQixrQkFBVTtBQUNaLFFBQUdRLE9BQU9DLE9BQVYsRUFDQTtBQUNSLFlBQUs1QixTQUFMLENBQWVtQyxLQUFmLEVBQXNCRyxRQUF0QixDQUErQkMsT0FBL0IsQ0FBdUNaLE9BQU81QixJQUE5QztBQUNOOEIsWUFBT0QsT0FBUCxDQUFlLEVBQWYsRUFBa0IsT0FBSzNCLElBQUwsQ0FBVSxxQkFBVixDQUFsQjtBQUNBK0IsT0FBRSxnQkFBRixFQUFvQlEsS0FBcEIsQ0FBMEIsUUFBMUI7QUFDZSxLQUxELE1BTUE7QUFDQ1gsWUFBT0MsS0FBUCxDQUFhLE9BQUs3QixJQUFMLENBQVUsV0FBVixDQUFiO0FBQ0E7QUFFSixJQVpkLEVBYWM4QixLQWJkLENBYW9CQyxFQUFFQyxJQWJ0QjtBQWNBLEdBN0RNOzs7QUErRFBRLGtCQUFnQix3QkFBVUMsQ0FBVixFQUFhO0FBQ3pCLE9BQUksQ0FBQ0EsRUFBRUMsT0FBRixJQUFhRCxFQUFFRSxPQUFoQixLQUE0QkYsRUFBRUcsT0FBRixJQUFhLEVBQTdDLEVBQWlEO0FBQzdDLFNBQUtwQixjQUFMO0FBQ0g7QUFDSjs7QUFuRU0sRUFkUzs7QUFxRmpCcUIsUUFyRmlCLHFCQXFGUDtBQUFBOztBQUNUQyxRQUFNQyxNQUFOLENBQWEsbUNBQWIsRUFBa0QsVUFBQ2pELElBQUQsRUFBVTtBQUMzRCxVQUFLRyxJQUFMLENBQVVHLFNBQVYsR0FBc0JOLElBQXRCO0FBQ0EsR0FGRDs7QUFJQWtELFFBQU1uQyxHQUFOLENBQVUscUJBQVYsRUFDRUssSUFERixDQUNPLG9CQUFZO0FBQ2pCLFVBQUtsQixJQUFMLEdBQVlpRCxTQUFTbkQsSUFBVCxDQUFjQSxJQUExQjtBQUNBLEdBSEY7QUFLQTtBQS9GZ0IsQ0FBUixDQUFWLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3FCQTtTQUVBO0FBREEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNSQTtRQUVBLENBQ0EsWUFFQTt1QkFDQTs7eUJBRUE7b0JBQ0E7VUFDQTtZQUVBO0FBTEE7QUFNQTs7b0NBRUE7eUNBQ0Esb0NBQ0EsZ0JBQ0E7QUFFQTtBQU5BOztBQU9BOztrQ0FDQTs4RUFDQSw4QkFDQTtnQkFDQSxRQUNBO3dDQUNBO2tCQUNBO3FCQUNBO3dCQUNBLDBCQUNBLHVCQUNBOytCQUNBO29CQUNBO2dFQUNBO0FBQ0E7V0FDQTt3QkFDQSwwQkFDQSxhQUNBOytCQUNBO29CQUNBO3dDQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQ0E7aUNBQ0E7QUFDQTtBQUVBO0FBQ0E7Ozs7QUFFQTs7bUNBQ0E7WUFDQSxPQUNBO0FBQ0E7Z0ZBQ0EsOEJBQ0E7a0JBQ0EsUUFDQTsyQ0FDQTtxQkFDQTtvREFDQSxxQ0FDQTtzQkFDQTsrQkFDQTsyQkFDQSwyQkFFQSx3REFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBLFVBQ0E7O21CQUVBO3VCQUNBO0FBRkEsMkJBR0E7b0JBQ0E7NkJBQ0E7eUJBQ0EsMkJBRUEsMEZBQ0E7QUFFQTtBQUNBO0FBRUE7QUF4Q0E7QUFwREEsRzs7Ozs7Ozs7Ozs7O0FDakJBOztBQUVBO3dCQUVBOztZQUdBO0FBRkE7QUFHQTs7V0FHQTs7QUFHQTtBQUZBO0FBVEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ21EQTtBQUNBOztBQUVBO1lBRUE7MEJBQ0E7OzJCQUdBO0FBRkE7QUFHQTs7MERBRUE7c0NBQ0E7aURBQ0E7QUFDQTtzQ0FDQTtvSUFDQTtBQUNBO3NDQUNBO3NDQUNBLEdBQ0E7a0NBQ0E7QUFJQTtBQWZBOztBQVBBLEc7Ozs7Ozs7QUMzREE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJOztBQUVwSTtBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMxQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDdEdBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3hCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7Ozs7OztBQy9CQSxlQUFlLDZKQUF5TSxpQkFBaUIsbUJBQW1CLGNBQWMsNEJBQTRCLFlBQVksVUFBVSxpQkFBaUIsZ0VBQWdFLFNBQVMsZ0NBQWdDLGtCQUFrQixhQUFhLGNBQWMsMEJBQTBCLFdBQVcsc0NBQXNDLFNBQVMsRUFBRSxrQkFBa0IsK0dBQStHLGVBQWUsYUFBYSxjQUFjLDRMQUE0TCxrQkFBa0IsYUFBYSw0REFBNEQsS0FBSyxxR0FBcUcsTUFBTSxTQUFTLHNDQUFzQyxTQUFTLEVBQUUsT0FBTyxvSUFBb0ksV0FBVyxnQkFBZ0IsT0FBTyxnRkFBZ0YsV0FBVyx1QkFBdUIsbUNBQW1DLFFBQVEsVUFBVSx3QkFBd0IsK0NBQStDLDhCQUE4QixvQkFBb0IsV0FBVyw0REFBNEQsbUNBQW1DLDJKQUEySix5REFBeUQsa0RBQWtELDRGQUE0RiwrQ0FBK0Msd0lBQXdJLEVBQUUsd0JBQXdCLHFGQUFxRixzQkFBc0IsZ0VBQWdFLFVBQVUsdUJBQXVCLG1EQUFtRCxpR0FBaUcsc0JBQXNCLHNGQUFzRixpQkFBaUIscUVBQXFFLGtCQUFrQiwyQ0FBMkMsV0FBVyxrQkFBa0IsUUFBUSxTQUFTLGlCQUFpQixnQkFBZ0IsVUFBVSxXQUFXLHNCQUFzQixrQkFBa0IseURBQXlELGlEQUFpRCxxQ0FBcUMsR0FBRyxxRUFBcUUsR0FBRyx3RUFBd0UsZ0NBQWdDLHdCQUF3QixJQUFJLDJFQUEyRSxnQ0FBZ0Msd0JBQXdCLElBQUksOEVBQThFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksNkVBQTZFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksK0VBQStFLG1DQUFtQywyQkFBMkIsc0JBQXNCLElBQUksMkVBQTJFLGdDQUFnQyx3QkFBd0IsSUFBSSx3RUFBd0UsSUFBSSxxRUFBcUUsR0FBRyxzRUFBc0UsNkJBQTZCLEdBQUcscUVBQXFFLEdBQUcsd0VBQXdFLGdDQUFnQyx3QkFBd0IsSUFBSSwyRUFBMkUsZ0NBQWdDLHdCQUF3QixJQUFJLDhFQUE4RSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLDZFQUE2RSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLCtFQUErRSxtQ0FBbUMsMkJBQTJCLHNCQUFzQixJQUFJLDJFQUEyRSxnQ0FBZ0Msd0JBQXdCLElBQUksd0VBQXdFLElBQUkscUVBQXFFLEdBQUcsc0VBQXNFLGtDQUFrQyxrQkFBa0IseUNBQXlDLFdBQVcsa0JBQWtCLFNBQVMsUUFBUSxrQkFBa0IsbUJBQW1CLFVBQVUsV0FBVyxrQkFBa0IsdURBQXVELCtDQUErQyxtQ0FBbUMsR0FBRyxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsSUFBSSxxTEFBcUwsTUFBTSxxTEFBcUwsR0FBRyxzTEFBc0wsMkJBQTJCLEdBQUcscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLElBQUkscUxBQXFMLE1BQU0scUxBQXFMLEdBQUcsc0xBQXNMLGtDQUFrQyxrQkFBa0IseUNBQXlDLFdBQVcsa0JBQWtCLFNBQVMsUUFBUSxpQkFBaUIsa0JBQWtCLFVBQVUsV0FBVyxrQkFBa0IsdURBQXVELCtDQUErQyxtQ0FBbUMsR0FBRyx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sSUFBSSx5TUFBeU0sTUFBTSx5TUFBeU0sR0FBRywwTUFBME0sMkJBQTJCLEdBQUcseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLElBQUkseU1BQXlNLE1BQU0seU1BQXlNLEdBQUcsME1BQTBNLGtDQUFrQyxrQkFBa0Isc0JBQXNCLHNEQUFzRCw4Q0FBOEMseUNBQXlDLFdBQVcsa0JBQWtCLGNBQWMsTUFBTSxTQUFTLGdCQUFnQixpQkFBaUIsVUFBVSxXQUFXLHNCQUFzQixrQkFBa0IsaUNBQWlDLHNCQUFzQiwrQkFBK0Isd0RBQXdELGdEQUFnRCxvQ0FBb0MsR0FBRyw0QkFBNEIsb0JBQW9CLEdBQUcsZ0NBQWdDLHlCQUF5Qiw0QkFBNEIsR0FBRyw0QkFBNEIsb0JBQW9CLEdBQUcsZ0NBQWdDLHlCQUF5Qiw2Q0FBNkMsV0FBVyxrQkFBa0IsK0RBQStELHFCQUFxQixjQUFjLFdBQVcsWUFBWSxlQUFlLGlCQUFpQixrQkFBa0IseUNBQXlDLFdBQVcsZUFBZSxrQkFBa0IsZUFBZSxPQUFPLGVBQWUscUJBQXFCLFNBQVMsNkJBQTZCLGlCQUFpQixjQUFjLEtBQUssY0FBYyw2QkFBNkIsU0FBUyxnQkFBZ0Isa0JBQWtCLG1CQUFtQixzQ0FBc0MsWUFBWSxLQUFLLGNBQWMsS0FBSyxpQkFBaUIsOEJBQThCLFFBQVEsV0FBVyxLQUFLLFdBQVcsZ0dBQWdHLElBQUksaUJBQWlCLFFBQVEsWUFBWSxXQUFXLFNBQVMsOE1BQThNLGVBQWUsV0FBVyxrQkFBa0IsOENBQThDLGdCQUFnQix5Q0FBeUMsV0FBVyxhQUFhLHNFQUFzRSxFQUFFLHlCQUF5QixvQkFBb0IsMkJBQTJCLGFBQWEsa0lBQWtJLHVDQUF1QyxvRUFBb0UsYUFBYSxvSUFBb0ksdUNBQXVDLGtEQUFrRCxxQkFBcUIsaUJBQWlCLGdCQUFnQixZQUFZLFdBQVcsS0FBSyxxQkFBcUIsTUFBTSxTQUFTLFlBQVksaUJBQWlCLDJCQUEyQixLQUFLLGlCQUFpQixrQ0FBa0MsS0FBSyxpQkFBaUIsaUJBQWlCLDRCQUE0QixTQUFTLDBCQUEwQixjQUFjLGlCQUFpQixLQUFLLFdBQVcsS0FBSywwQ0FBMEMsMkJBQTJCLHFDQUFxQyxlQUFlLEVBQUUsU0FBUyxnQkFBZ0IsMEJBQTBCLGdJQUFnSSxLQUFLLCtHQUErRyxrQkFBa0IsY0FBYyw0QkFBNEIsbUJBQW1CLG9CQUFvQixjQUFjLHNDQUFzQyxrQ0FBa0MsZ0JBQWdCLFVBQVUsZ0JBQWdCLFVBQVUsMERBQTBELDBDQUEwQyxNQUFNLHdCQUF3QixNQUFNLHNFQUFzRSxPQUFPLFVBQVUsb0JBQW9CLGlCQUFpQiw0Q0FBNEMsS0FBSyxnREFBZ0QsNEVBQTRFLGdCQUFnQixvQ0FBb0MsOEhBQThILDBHQUEwRyxLQUFLLEtBQUssYUFBYSw2QkFBNkIsMkNBQTJDLFFBQVEsZUFBZSxNQUFNLGtCQUFrQiw0REFBNEQsZ0JBQWdCLG9FQUFvRSxpQkFBaUIsK0RBQStELGtCQUFrQix3QkFBd0IsT0FBTywwR0FBMEcsV0FBVywwQkFBMEIsaUJBQWlCLFdBQVcsS0FBSyxxQkFBcUIsbUJBQW1CLE1BQU0sV0FBVyxPQUFPLFlBQVksV0FBVyxLQUFLLFdBQVcsZUFBZSxZQUFZLGlCQUFpQixpQkFBaUIsbUJBQW1CLGlCQUFpQixTQUFTLHFCQUFxQiw0Q0FBNEMsR0FBRyxpQkFBaUIsV0FBVyxzQ0FBc0MsU0FBUyxFQUFFLCtCQUErQixHQUFHLEUiLCJmaWxlIjoianMvcGFnZXMvZmVlZGJhY2suanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQyOSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsIlZ1ZS5jb21wb25lbnQoJ21mZWVkYmFjaycsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL2ZlZWRiYWNrL01vcmVGZWVkYmFjay52dWUnKSk7XHJcblZ1ZS5jb21wb25lbnQoJ2ZlZWRiYWNrJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvZmVlZGJhY2svZmVlZGJhY2sudnVlJykpO1xyXG5cclxudmFyIGFwcCA9IG5ldyBWdWUoe1xyXG5cdGVsOiAnI2ZlZWRiYWNrc19jb250YWluZXInLFxyXG5cdGRhdGE6IHtcclxuXHRcdGZlZWRiYWNrcyA6IFtdLFxyXG5cdFx0bGFuZzpbXSxcclxuXHRcdGZvcm06IG5ldyBGb3JtKHtcclxuXHRcdFx0IGNvbnRlbnQ6JydcclxuXHRcdFx0LGltYWdlVGVtcDogJydcclxuXHRcdH0sIHsgYmFzZVVSTDogJ2h0dHA6Ly8nK0xhcmF2ZWwuYmFzZV9hcGlfdXJsIH0pLFxyXG5cdFx0Y29tbWVudEZvcm06IG5ldyBGb3JtKHtcclxuXHRcdFx0Y29udGVudDonJyxcclxuXHRcdFx0cGFyZW50X2lkOjBcclxuXHRcdH0sIHsgYmFzZVVSTDogJ2h0dHA6Ly8nK0xhcmF2ZWwuYmFzZV9hcGlfdXJsIH0pLFxyXG5cdH0sXHJcblx0bWV0aG9kczp7XHJcblx0XHQgb25JbmZpbml0ZSgpIHtcclxuXHRcdFx0YXhpb3NBUEl2MS5nZXQoJy9mZWVkYmFja3MnLHtcclxuXHRcdFx0XHRwYXJhbXM6IHtcclxuXHRcdFx0XHRcdCBjb3VudDo0XHJcblx0XHRcdFx0XHQsc2tpcDp0aGlzLmZlZWRiYWNrcy5sZW5ndGhcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHR9KS50aGVuKChyZXMpID0+IHtcclxuXHRcdFx0XHRpZiAocmVzLmRhdGEubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHR0aGlzLmZlZWRiYWNrcyA9IHRoaXMuZmVlZGJhY2tzLmNvbmNhdChyZXMuZGF0YSk7XHJcblx0XHRcdFx0XHR0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpsb2FkZWQnKTtcclxuXHRcdFx0XHRcdC8vIGlmICh0aGlzLmZlZWRiYWNrcy5sZW5ndGggLyAyMCA9PT0gMSkge1xyXG5cdFx0XHRcdFx0Ly8gICB0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZScpO1xyXG5cdFx0XHRcdFx0Ly8gfVxyXG5cdFx0XHRcdFx0Ly9mb3IgdW5saSBwYWdlc1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLiRyZWZzLmluZmluaXRlTG9hZGluZy4kZW1pdCgnJEluZmluaXRlTG9hZGluZzpjb21wbGV0ZScpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0ICAgIH0sXHJcblxyXG5cdFx0c3VibWl0RmVlZGJhY2soKXtcclxuXHRcdFx0dGhpcy5mb3JtLnN1Ym1pdCgncG9zdCcsICcvdjEvZmVlZGJhY2tzJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LnN1Y2Nlc3MpXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG5cdFx0XHQgICAgICAgICAgICB0aGlzLmZvcm0uY29udGVudCA9ICcnO1xyXG5cdFx0XHQgICAgICAgICAgICB0aGlzLmZvcm0uaW1hZ2VUZW1wID0gJyc7XHJcblx0XHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1tcImRlc2NyaXB0aW9uMlwiXSx0aGlzLmxhbmdbXCJzdWNjZXNzLW1zZy1mZWVkYmFja1wiXSlcclxuICAgICAgICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuXHQgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ3RyeS1hZ2FpbiddKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XHJcblx0XHR9LFxyXG5cclxuXHRcdHN1Ym1pdENvbW1lbnQoKXtcclxuXHRcdFx0dmFyIGluZGV4O1xyXG5cdFx0XHRmb3IodmFyIGk9MDsgaTx0aGlzLmZlZWRiYWNrcy5sZW5ndGg7IGkrKylcclxuICAgICAgICBcdHtcclxuICAgICAgICBcdFx0aWYodGhpcy5mZWVkYmFja3NbaV0uaWQgPT0gdGhpcy5jb21tZW50Rm9ybS5wYXJlbnRfaWQpXHJcbiAgICAgICAgXHRcdHtcclxuICAgICAgICBcdFx0XHRpbmRleCA9IGk7XHJcbiAgICAgICAgXHRcdFx0YnJlYWs7XHJcbiAgICAgICAgXHRcdH1cclxuICAgICAgICBcdH1cclxuXHRcdFx0dGhpcy5jb21tZW50Rm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3YxL2ZlZWRiYWNrcycpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5zdWNjZXNzKVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICBcdFx0XHRcdHRoaXMuZmVlZGJhY2tzW2luZGV4XS5jb21tZW50cy51bnNoaWZ0KHJlc3VsdC5kYXRhKTtcclxuXHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MoJycsdGhpcy5sYW5nW1wic3VjY2Vzcy1tc2ctY29udGVudFwiXSk7XHJcblx0XHRcdFx0XHRcdCQoJyNtb2RhbENvbW1lbnRzJykubW9kYWwoJ3RvZ2dsZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG5cdCAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZ1sndHJ5LWFnYWluJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XHJcblx0XHR9LFxyXG5cclxuXHRcdGhhbmRsZUNtZEVudGVyOiBmdW5jdGlvbiAoZSkge1xyXG5cdFx0ICAgIGlmICgoZS5tZXRhS2V5IHx8IGUuY3RybEtleSkgJiYgZS5rZXlDb2RlID09IDEzKSB7XHJcblx0XHQgICAgICAgIHRoaXMuc3VibWl0RmVlZGJhY2soKTtcclxuXHRcdCAgICB9XHJcblx0XHR9XHJcblxyXG5cdH0sXHJcblxyXG5cdGNyZWF0ZWQoKSB7XHJcblx0XHRFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQuc3VibWl0RmVlZGJhY2tJbWFnZScsIChkYXRhKSA9PiB7XHJcblx0XHRcdHRoaXMuZm9ybS5pbWFnZVRlbXAgPSBkYXRhO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0YXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2ZlZWRiYWNrJylcclxuXHRcdFx0LnRoZW4obGFuZ3VhZ2UgPT4ge1xyXG5cdFx0XHRcdHRoaXMubGFuZyA9IGxhbmd1YWdlLmRhdGEuZGF0YTtcclxuXHRcdFx0fSk7XHJcblx0XHRcclxuXHR9XHJcblx0XHJcbn0pO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL2ZlZWRiYWNrLmpzIiwiPHRlbXBsYXRlPlxyXG5cdDxkaXYgY2xhc3M9J2NvbC1sZy0xMiB1c2VyLWZlZWRiYWNrJz5cclxuXHQgICAgPGRpdiBjbGFzcz0nY29sLWxnLTEgdGV4dC1jZW50ZXInPlxyXG5cdCAgICAgICAgPGltZyB2LWlmPSdjb21tZW50LnVzZXIuYXZhdGFyJyA6c3JjPVwiY29tbWVudC51c2VyLmF2YXRhclwiIGNsYXNzPVwiaW1nIGltZy1jaXJjbGUgZmItdXNlci1pbWFnZVwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPSduby1hdmF0YXInIHYtZWxzZT5cclxuICAgICAgICAgICAgICAgIHt7Y29tbWVudC51c2VyLmluaXRpYWxzfX1cclxuICAgICAgICAgICAgPC9kaXY+XHJcblx0ICAgIDwvZGl2PlxyXG5cdCAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTExXCI+XHJcblx0ICAgICAgICA8c3BhbiBjbGFzcz0nZmItbmFtZSAgcG9wb3Zlci1kZXRhaWxzJyA6ZGF0YS1pZD0nY29tbWVudC51c2VyLmlkJz5cclxuXHQgICAgICAgICAgICB7e2NvbW1lbnQudXNlci5mdWxsX25hbWV9fVxyXG5cdCAgICAgICAgPC9zcGFuPlxyXG5cdCAgICAgICAgPGRpdiBjbGFzcz0nZGF0ZXRpbWUnPlxyXG5cdCAgICAgICAgICAgIHt7Y29tbWVudC5jcmVhdGVkX2F0fX1cclxuXHQgICAgICAgIDwvZGl2PlxyXG5cdCAgICAgICAgPGRpdiBjbGFzcz0nZmItY29udGVudCc+XHJcblx0ICAgICAgICAgICAge3tjb21tZW50LmNvbnRlbnR9fVxyXG5cdCAgICAgICAgPC9kaXY+XHJcblx0ICAgIDwvZGl2PlxyXG5cdDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRwcm9wczogWydjb21tZW50J10sXHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENvbW1lbnQudnVlPzZmNTliZDUyIiwiPHRlbXBsYXRlPlxyXG5cdDxhIFx0Y2xhc3M9J2J0biBidG4taGFzLWxvYWRpbmcgYnRuLWxpa2UgYnRuLWZhYiBidG4tZmFiLW1pbmkgJyBcclxuXHRcdDpjbGFzcz0nbGlrZWQ/XCJsaWtlZFwiOlwibm90LWxpa2VkXCInXHJcblx0XHQ6aHJlZj0nXCIjXCIrIGZlZWRiYWNrJyBcclxuXHRcdEBjbGljaz0nbGlrZUZlZWRiYWNrJ1xyXG5cdFx0ZGF0YS10b2dnbGU9J3Rvb2x0aXAnXHJcblx0XHRkYXRhLXBsYWNlbWVudD0nYm90dG9tJ1xyXG5cdFx0OmRhdGEtdGl0bGU9J21lc3NhZ2UnXHJcblx0XHQ6aWQ9J1wiYnRuTGlrZVwiK2ZlZWRiYWNrJ1xyXG5cdD5cclxuXHRcdDxzcGFuIGNsYXNzPSdmYSBmYS10aHVtYnMtdXAnPjwvc3Bhbj4gXHJcblx0PC9hPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHRcdFxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cdHByb3BzOiBbXHJcblx0XHQgJ2ZlZWRiYWNrJyxcclxuXHRcdCAnbGlrZXMnXHJcblx0XSxcclxuXHRkYXRhOmZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgdXNlcl9pZDpMYXJhdmVsLnVzZXIuaWRcclxuICAgICAgICAgICAgLGZlZWRiYWNrbGlrZV9pZDpmYWxzZVxyXG5cdFx0XHQsbGlrZWQ6ZmFsc2VcclxuXHRcdFx0LG1lc3NhZ2U6XCJMaWtlXCJcclxuICAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIHdhdGNoOntcclxuICAgIFx0bWVzc2FnZTpmdW5jdGlvbiAobmV3TXNnKXtcclxuICAgIFx0XHQkKCcjYnRuTGlrZScrdGhpcy5mZWVkYmFjaykudG9vbHRpcCgnaGlkZScpXHJcblx0ICAgICAgICAgIC5hdHRyKCdkYXRhLW9yaWdpbmFsLXRpdGxlJywgbmV3TXNnKVxyXG5cdCAgICAgICAgICAudG9vbHRpcCgnZml4VGl0bGUnKTtcclxuICAgIFx0fVxyXG4gICAgfSxcclxuICAgIGNyZWF0ZWQoKXtcclxuXHRcdHZhciBsYW5nID0gdGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZztcclxuICAgIFx0YXhpb3NBUEl2MS5nZXQoJy9mZWVkYmFjay1saWtlcz91c2VyPScrdGhpcy51c2VyX2lkKycmZmVlZGJhY2s9Jyt0aGlzLmZlZWRiYWNrKVxyXG5cdFx0LnRoZW4oKHJlcykgPT4ge1xyXG5cdFx0XHRpZihyZXMuZGF0YS5sZW5ndGgpXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0aGlzLmZlZWRiYWNrbGlrZV9pZCA9IHJlcy5kYXRhWzBdLmlkO1xyXG5cdFx0XHRcdHRoaXMubGlrZWQgPSB0cnVlO1xyXG5cdFx0XHRcdGlmKHRoaXMubGlrZWQpe1xyXG5cdFx0XHRcdFx0aWYodGhpcy5saWtlcz09XCIxXCIpXHJcblx0XHRcdFx0XHRcdHRoaXMubWVzc2FnZSA9IGxhbmdbJ3lvdS1saWtlZC10aGlzJ107XHJcblx0XHRcdFx0XHRlbHNle1xyXG5cdFx0XHRcdFx0XHR2YXIgbGlrZSA9IHRoaXMubGlrZXMgLSAxO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmxpa2VzID0gbGlrZTtcclxuXHRcdFx0XHRcdFx0dGhpcy5tZXNzYWdlID0gbGFuZ1sneW91LWFuZCddK1wiIFwiK2xpa2UrXCIgXCIrbGFuZ1snb3RoZXJzLWxpa2VkLXRoaXMnXTtcclxuXHRcdFx0XHRcdH0gIFxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRpZih0aGlzLmxpa2VzPT1cIjBcIilcclxuXHRcdFx0XHRcdFx0dGhpcy5tZXNzYWdlID0gbGFuZ1tcImxpa2VcIl07XHJcblx0XHRcdFx0XHRlbHNle1xyXG5cdFx0XHRcdFx0XHR2YXIgbGlrZSA9IHRoaXMubGlrZXMgLSAxO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmxpa2VzID0gbGlrZTtcclxuXHRcdFx0XHRcdFx0dGhpcy5tZXNzYWdlID0gbGlrZStcIiBcIitsYW5nWydvdGhlcnMtbGlrZWQtdGhpcyddO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fWVsc2VcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKClcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblxyXG4gICAgfSxcclxuXHRtZXRob2RzOntcclxuXHRcdGxpa2VGZWVkYmFjaygpe1xyXG5cdFx0XHR2YXIgbGFuZyA9IHRoaXMuJHBhcmVudC4kcGFyZW50LmxhbmdcclxuXHRcdFx0aWYodGhpcy5saWtlZClcclxuXHRcdFx0e1xyXG5cdFx0XHRcdC8vdXBkYXRlIG9yIHNlYXJjaFxyXG5cdFx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvZmVlZGJhY2stbGlrZXM/dXNlcj0nK3RoaXMudXNlcl9pZCsnJmZlZWRiYWNrPScrdGhpcy5mZWVkYmFjaylcclxuXHRcdFx0XHQudGhlbigocmVzKSA9PiB7XHJcblx0XHRcdFx0XHRpZihyZXMuZGF0YS5sZW5ndGgpXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRoaXMuZmVlZGJhY2tsaWtlX2lkID0gcmVzLmRhdGFbMF0uaWQ7XHJcblx0XHRcdFx0XHRcdHRoaXMubGlrZWQgPSB0cnVlO1xyXG5cdFx0XHRcdFx0XHRheGlvc0FQSXYxLmRlbGV0ZSgnL2ZlZWRiYWNrLWxpa2VzLycrdGhpcy5mZWVkYmFja2xpa2VfaWQpXHJcblx0XHRcdFx0XHRcdC50aGVuKChyZXMpID0+IHtcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmxpa2VkID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MoJycsbGFuZ1sndW5saWtlZCddKTtcclxuXHRcdFx0XHRcdFx0XHRpZih0aGlzLmxpa2VzPT1cIjBcIilcclxuXHRcdFx0XHRcdFx0XHRcdHRoaXMubWVzc2FnZSA9IGxhbmdbXCJsaWtlXCJdO1xyXG5cdFx0XHRcdFx0XHRcdGVsc2VcclxuXHRcdFx0XHRcdFx0XHRcdHRoaXMubWVzc2FnZSA9IHRoaXMubGlrZXMrXCIgXCIrbGFuZ1snb3RoZXJzLWxpa2VkLXRoaXMnXTtcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHR9ZWxzZVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0YXhpb3NBUEl2MS5wb3N0KCcvZmVlZGJhY2stbGlrZXMnLHtcclxuXHRcdFx0XHRcdFx0IHVzZXJfaWQ6dGhpcy51c2VyX2lkXHJcblx0XHRcdFx0XHRcdCxmZWVkYmFja19pZDp0aGlzLmZlZWRiYWNrXHJcblx0XHRcdFx0fSkudGhlbigocmVzKSA9PiB7XHJcblx0XHRcdFx0XHR0aGlzLmxpa2VkID0gdHJ1ZTtcclxuXHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKCcnLGxhbmdbJ2xpa2VkJ10pO1xyXG5cdFx0XHRcdFx0aWYodGhpcy5saWtlcz09XCIwXCIpXHJcblx0XHRcdFx0XHRcdHRoaXMubWVzc2FnZSA9IGxhbmdbJ3lvdS1saWtlZC10aGlzJ11cclxuXHRcdFx0XHRcdGVsc2VcclxuXHRcdFx0XHRcdFx0dGhpcy5tZXNzYWdlID0gbGFuZ1sneW91LWFuZCddK1wiIFwiK3RoaXMubGlrZXMrXCIgXCIrbGFuZ1snb3RoZXJzLWxpa2VkLXRoaXMnXTtcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIExpa2UudnVlPzg4M2RkMzVjIiwiPHNjcmlwdCB0eXBlPVwidGV4dC9qYXZhc2NyaXB0XCI+XHJcbmltcG9ydCBJbmZpbml0ZUxvYWRpbmcgZnJvbSAndnVlLWluZmluaXRlLWxvYWRpbmcnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG4gIGRhdGEoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBsaXN0OiBbXSxcclxuICAgIH07XHJcbiAgfSxcclxuICBtZXRob2RzOiB7XHJcbiAgICBcclxuICB9LFxyXG4gIGNvbXBvbmVudHM6IHtcclxuICAgIEluZmluaXRlTG9hZGluZyxcclxuICB9LFxyXG59O1xyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gTW9yZUZlZWRiYWNrLnZ1ZT8xNmQwMDIzYSIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbGctMTJcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyB1c2VyLWZlZWRiYWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImFicyB0by1wYXJ0aWFsLXJpZ2h0IHRvLWZ1bGwtYm90dG9tXCIgdi1pZj1cIiFmZWVkYmFjay5jb21tZW50cy5sZW5ndGhcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIEBjbGljaz1cIm9wZW5Db21tZW50c01vZGFsKGZlZWRiYWNrLmlkKVwiIGlkPVwiYnRuQWRkQ29tbWVudE1vZGFsXCIgY2xhc3M9XCJidG4gYnRuLWZhYiBidG4tcmlwcGxlIGJ0bi1pbmZvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPSdtYXRlcmlhbC1pY29ucyc+YWRkPC9pPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTEgY29sLW1kLTIgY29sLXNtLTIgY29sLXhzLTMgdGV4dC1jZW50ZXIgIFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgdi1pZj0nZmVlZGJhY2sudXNlci5hdmF0YXInIDpzcmM9XCJmZWVkYmFjay51c2VyLmF2YXRhclwiIGNsYXNzPVwiaW1nIGltZy1jaXJjbGUgZmItdXNlci1pbWFnZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9J25vLWF2YXRhcicgdi1lbHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7e2ZlZWRiYWNrLnVzZXIuaW5pdGlhbHN9fVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTExICBjb2wtbWQtMTAgY29sLXNtLTEwIGNvbC14cy05XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZiLWhvbGRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYWJzIHRvLXJpZ2h0IHRvLWZ1bGwtYm90dG9tXCIgdi1pZj1cImZlZWRiYWNrLmNvbW1lbnRzLmxlbmd0aFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBAY2xpY2s9XCJvcGVuQ29tbWVudHNNb2RhbChmZWVkYmFjay5pZClcIiBpZD1cImJ0bkFkZENvbW1lbnRNb2RhbFwiIGNsYXNzPVwiYnRuIGJ0bi1mYWIgYnRuLXJpcHBsZSBidG4taW5mb1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPSdtYXRlcmlhbC1pY29ucyc+YWRkPC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImZiLW5hbWUgcG9wb3Zlci1kZXRhaWxzXCIgOmRhdGEtaWQ9J2ZlZWRiYWNrLnVzZXIuaWQnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3tmZWVkYmFjay51c2VyLmZ1bGxfbmFtZX19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRhdGV0aW1lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7e2ZlZWRiYWNrLmNyZWF0ZWRfYXQgfCBmcmllbmRseS1kYXRlfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaWtlICA6ZmVlZGJhY2s9J2ZlZWRiYWNrLmlkJyA6bGlrZXM9J2ZlZWRiYWNrLmxpa2VzX2NvdW50Jz48L2xpa2U+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmItY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz0nY29udGFpbmVyLWZsdWlkJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTIgY29sLW1kLTIgY29sLXNtLTIgY29sLXhzLTEyXCIgdi1pZj0nZmVlZGJhY2suaW1hZ2UnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIDpzcmM9XCInLycrZmVlZGJhY2suaW1hZ2VcIiBjbGFzcz0naW1nLXJlc3BvbnNpdmUnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgOmNsYXNzPVwiZmVlZGJhY2suaW1hZ2U/ICdjb2wtbGctMTAgY29sLW1kLTEwIGNvbC1zbS0xMCBjb2wteHMtMTInOiAnY29sLWxnLTEyJ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7e2ZlZWRiYWNrLmNvbnRlbnR9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmYi1jb21tZW50cy1ob2xkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGNvbW1lbnQgdi1mb3I9XCIoY29tbWVudCwkaW5kZXgpIGluIGZlZWRiYWNrLmNvbW1lbnRzXCIgdi1pZj1cIiRpbmRleCA8IGNvbW1lbnRzU2hvd25cIiA6Y29tbWVudD1cImNvbW1lbnRcIiA6a2V5PVwiY29tbWVudC5pZFwiPjwvY29tbWVudD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1sZy0xMiB1c2VyLWZlZWRiYWNrIHRleHQtcmlnaHRcIiB2LWlmPVwiKGZlZWRiYWNrLmNvbW1lbnRzLmxlbmd0aCA+IGNvbW1lbnRzU2hvd24pfHwoY29tbWVudHNTaG93biA+IDIpXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uICB2LWlmPVwiZmVlZGJhY2suY29tbWVudHMubGVuZ3RoID4gY29tbWVudHNTaG93blwiICBjbGFzcz1cImJ0biBidG4taW5mbyBidG4tcmFpc2VkIGJ0bi1oYXMtbG9hZGluZ1wiIEBjbGljaz0ndmlld01vcmUnPnt7dGhpcy4kcGFyZW50LmxhbmdbXCJ2aWV3LW1vcmUtcmVwbGllc1wiXX19PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uICB2LWlmPVwiY29tbWVudHNTaG93biA+IDJcIiBjbGFzcz1cImJ0biBidG4taW5mbyBidG4tcmFpc2VkIGJ0bi1oYXMtbG9hZGluZ1wiIEBjbGljaz0ndmlld0xlc3MnPnt7dGhpcy4kcGFyZW50LmxhbmdbXCJ2aWV3LWxlc3MtcmVwbGllc1wiXX19PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG5cclxuPHNjcmlwdD5cclxuVnVlLmNvbXBvbmVudCgnbGlrZScsICByZXF1aXJlKCcuL0xpa2UudnVlJykpO1xyXG5WdWUuY29tcG9uZW50KCdjb21tZW50JywgIHJlcXVpcmUoJy4vQ29tbWVudC52dWUnKSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCB7XHJcblx0cHJvcHM6IFsnZmVlZGJhY2snXSxcclxuICAgIGRhdGE6ZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNvbW1lbnRzU2hvd246MixcclxuICAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6e1xyXG4gICAgICAgIG9wZW5Db21tZW50c01vZGFsKGlkKXtcclxuICAgICAgICAgICAgJCgnI21vZGFsQ29tbWVudHMnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgICAgIHRoaXMuJHBhcmVudC5jb21tZW50Rm9ybS5wYXJlbnRfaWQgPSBpZDtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHZpZXdNb3JlKCl7XHJcbiAgICAgICAgICAgIHRoaXMuY29tbWVudHNTaG93biArPSAodGhpcy5mZWVkYmFjay5jb21tZW50cy5sZW5ndGgtdGhpcy5jb21tZW50c1Nob3duPjIpPzI6dGhpcy5mZWVkYmFjay5jb21tZW50cy5sZW5ndGgtdGhpcy5jb21tZW50c1Nob3duO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdmlld0xlc3MoKXtcclxuICAgICAgICAgICAgaWYodGhpcy5jb21tZW50c1Nob3duIDw9IDIpXHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIHRoaXMuY29tbWVudHNTaG93biAtPSAyO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICBcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBmZWVkYmFjay52dWU/NWJhMWQ2ZTEiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Db21tZW50LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNmEyZjI3YmVcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQ29tbWVudC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxcZmVlZGJhY2tcXFxcQ29tbWVudC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBDb21tZW50LnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi02YTJmMjdiZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTZhMmYyN2JlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9mZWVkYmFjay9Db21tZW50LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzMwXG4vLyBtb2R1bGUgY2h1bmtzID0gOCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0xpa2UudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi00ZWNlODc3NlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9MaWtlLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxmZWVkYmFja1xcXFxMaWtlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIExpa2UudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTRlY2U4Nzc2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNGVjZTg3NzZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2ZlZWRiYWNrL0xpa2UudnVlXG4vLyBtb2R1bGUgaWQgPSAzMzFcbi8vIG1vZHVsZSBjaHVua3MgPSA4IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vTW9yZUZlZWRiYWNrLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgbnVsbCxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxmZWVkYmFja1xcXFxNb3JlRmVlZGJhY2sudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi1jNTY4ZDliMFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LWM1NjhkOWIwXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9mZWVkYmFjay9Nb3JlRmVlZGJhY2sudnVlXG4vLyBtb2R1bGUgaWQgPSAzMzJcbi8vIG1vZHVsZSBjaHVua3MgPSA4IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vZmVlZGJhY2sudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0xNjlmN2JkYVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9mZWVkYmFjay52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxcZmVlZGJhY2tcXFxcZmVlZGJhY2sudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gZmVlZGJhY2sudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTE2OWY3YmRhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMTY5ZjdiZGFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2ZlZWRiYWNrL2ZlZWRiYWNrLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzMzXG4vLyBtb2R1bGUgY2h1bmtzID0gOCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcmRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMTJcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3cgdXNlci1mZWVkYmFja1wiXG4gIH0sIFsoIV92bS5mZWVkYmFjay5jb21tZW50cy5sZW5ndGgpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJhYnMgdG8tcGFydGlhbC1yaWdodCB0by1mdWxsLWJvdHRvbVwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZmFiIGJ0bi1yaXBwbGUgYnRuLWluZm9cIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImJ0bkFkZENvbW1lbnRNb2RhbFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLm9wZW5Db21tZW50c01vZGFsKF92bS5mZWVkYmFjay5pZClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtYXRlcmlhbC1pY29uc1wiXG4gIH0sIFtfdm0uX3YoXCJhZGRcIildKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMSBjb2wtbWQtMiBjb2wtc20tMiBjb2wteHMtMyB0ZXh0LWNlbnRlciAgXCJcbiAgfSwgWyhfdm0uZmVlZGJhY2sudXNlci5hdmF0YXIpID8gX2MoJ2ltZycsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbWcgaW1nLWNpcmNsZSBmYi11c2VyLWltYWdlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5mZWVkYmFjay51c2VyLmF2YXRhclxuICAgIH1cbiAgfSkgOiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm5vLWF2YXRhclwiXG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uZmVlZGJhY2sudXNlci5pbml0aWFscykgKyBcIlxcbiAgICAgICAgICAgICAgICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMTEgIGNvbC1tZC0xMCBjb2wtc20tMTAgY29sLXhzLTlcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYi1ob2xkZXJcIlxuICB9LCBbKF92bS5mZWVkYmFjay5jb21tZW50cy5sZW5ndGgpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJhYnMgdG8tcmlnaHQgdG8tZnVsbC1ib3R0b21cIlxuICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWZhYiBidG4tcmlwcGxlIGJ0bi1pbmZvXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJidG5BZGRDb21tZW50TW9kYWxcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5vcGVuQ29tbWVudHNNb2RhbChfdm0uZmVlZGJhY2suaWQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibWF0ZXJpYWwtaWNvbnNcIlxuICB9LCBbX3ZtLl92KFwiYWRkXCIpXSldKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZiLW5hbWUgcG9wb3Zlci1kZXRhaWxzXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1pZFwiOiBfdm0uZmVlZGJhY2sudXNlci5pZFxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uZmVlZGJhY2sudXNlci5mdWxsX25hbWUpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJkYXRldGltZVwiXG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLl9mKFwiZnJpZW5kbHktZGF0ZVwiKShfdm0uZmVlZGJhY2suY3JlYXRlZF9hdCkpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIpLCBfYygnbGlrZScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJmZWVkYmFja1wiOiBfdm0uZmVlZGJhY2suaWQsXG4gICAgICBcImxpa2VzXCI6IF92bS5mZWVkYmFjay5saWtlc19jb3VudFxuICAgIH1cbiAgfSldLCAxKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYi1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29udGFpbmVyLWZsdWlkXCJcbiAgfSwgWyhfdm0uZmVlZGJhY2suaW1hZ2UpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMiBjb2wtbWQtMiBjb2wtc20tMiBjb2wteHMtMTJcIlxuICB9LCBbX2MoJ2ltZycsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInNyY1wiOiAnLycgKyBfdm0uZmVlZGJhY2suaW1hZ2VcbiAgICB9XG4gIH0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBjbGFzczogX3ZtLmZlZWRiYWNrLmltYWdlID8gJ2NvbC1sZy0xMCBjb2wtbWQtMTAgY29sLXNtLTEwIGNvbC14cy0xMicgOiAnY29sLWxnLTEyJ1xuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmZlZWRiYWNrLmNvbnRlbnQpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZiLWNvbW1lbnRzLWhvbGRlclwiXG4gIH0sIFtfdm0uX2woKF92bS5mZWVkYmFjay5jb21tZW50cyksIGZ1bmN0aW9uKGNvbW1lbnQsICRpbmRleCkge1xuICAgIHJldHVybiAoJGluZGV4IDwgX3ZtLmNvbW1lbnRzU2hvd24pID8gX2MoJ2NvbW1lbnQnLCB7XG4gICAgICBrZXk6IGNvbW1lbnQuaWQsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImNvbW1lbnRcIjogY29tbWVudFxuICAgICAgfVxuICAgIH0pIDogX3ZtLl9lKClcbiAgfSksIF92bS5fdihcIiBcIiksICgoX3ZtLmZlZWRiYWNrLmNvbW1lbnRzLmxlbmd0aCA+IF92bS5jb21tZW50c1Nob3duKSB8fCAoX3ZtLmNvbW1lbnRzU2hvd24gPiAyKSkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMiB1c2VyLWZlZWRiYWNrIHRleHQtcmlnaHRcIlxuICB9LCBbKF92bS5mZWVkYmFjay5jb21tZW50cy5sZW5ndGggPiBfdm0uY29tbWVudHNTaG93bikgPyBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4taW5mbyBidG4tcmFpc2VkIGJ0bi1oYXMtbG9hZGluZ1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS52aWV3TW9yZVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmdbXCJ2aWV3LW1vcmUtcmVwbGllc1wiXSkpXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5jb21tZW50c1Nob3duID4gMikgPyBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4taW5mbyBidG4tcmFpc2VkIGJ0bi1oYXMtbG9hZGluZ1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS52aWV3TGVzc1xuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmdbXCJ2aWV3LWxlc3MtcmVwbGllc1wiXSkpXSkgOiBfdm0uX2UoKV0pIDogX3ZtLl9lKCldLCAyKV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTE2OWY3YmRhXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMTY5ZjdiZGFcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9mZWVkYmFjay9mZWVkYmFjay52dWVcbi8vIG1vZHVsZSBpZCA9IDM2NlxuLy8gbW9kdWxlIGNodW5rcyA9IDgiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1oYXMtbG9hZGluZyBidG4tbGlrZSBidG4tZmFiIGJ0bi1mYWItbWluaSBcIixcbiAgICBjbGFzczogX3ZtLmxpa2VkID8gXCJsaWtlZFwiIDogXCJub3QtbGlrZWRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiI1wiICsgX3ZtLmZlZWRiYWNrLFxuICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcbiAgICAgIFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIixcbiAgICAgIFwiZGF0YS10aXRsZVwiOiBfdm0ubWVzc2FnZSxcbiAgICAgIFwiaWRcIjogXCJidG5MaWtlXCIgKyBfdm0uZmVlZGJhY2tcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS5saWtlRmVlZGJhY2tcbiAgICB9XG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS10aHVtYnMtdXBcIlxuICB9KV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTRlY2U4Nzc2XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNGVjZTg3NzZcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9mZWVkYmFjay9MaWtlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzgzXG4vLyBtb2R1bGUgY2h1bmtzID0gOCIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMiB1c2VyLWZlZWRiYWNrXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTEgdGV4dC1jZW50ZXJcIlxuICB9LCBbKF92bS5jb21tZW50LnVzZXIuYXZhdGFyKSA/IF9jKCdpbWcnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1nIGltZy1jaXJjbGUgZmItdXNlci1pbWFnZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInNyY1wiOiBfdm0uY29tbWVudC51c2VyLmF2YXRhclxuICAgIH1cbiAgfSkgOiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm5vLWF2YXRhclwiXG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmNvbW1lbnQudXNlci5pbml0aWFscykgKyBcIlxcbiAgICAgICAgICAgIFwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTExXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZiLW5hbWUgIHBvcG92ZXItZGV0YWlsc1wiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImRhdGEtaWRcIjogX3ZtLmNvbW1lbnQudXNlci5pZFxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcblxcdCAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5jb21tZW50LnVzZXIuZnVsbF9uYW1lKSArIFwiXFxuXFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJkYXRldGltZVwiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHQgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uY29tbWVudC5jcmVhdGVkX2F0KSArIFwiXFxuXFx0ICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYi1jb250ZW50XCJcbiAgfSwgW192bS5fdihcIlxcblxcdCAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5jb21tZW50LmNvbnRlbnQpICsgXCJcXG5cXHQgICAgICAgIFwiKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi02YTJmMjdiZVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTZhMmYyN2JlXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvZmVlZGJhY2svQ29tbWVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDM5MlxuLy8gbW9kdWxlIGNodW5rcyA9IDgiLCIhZnVuY3Rpb24ocCx4KXtcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cyYmXCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZT9tb2R1bGUuZXhwb3J0cz14KCk6XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXSx4KTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9leHBvcnRzLlZ1ZUluZmluaXRlTG9hZGluZz14KCk6cC5WdWVJbmZpbml0ZUxvYWRpbmc9eCgpfSh0aGlzLGZ1bmN0aW9uKCl7cmV0dXJuIGZ1bmN0aW9uKHApe2Z1bmN0aW9uIHgoYSl7aWYodFthXSlyZXR1cm4gdFthXS5leHBvcnRzO3ZhciBlPXRbYV09e2V4cG9ydHM6e30saWQ6YSxsb2FkZWQ6ITF9O3JldHVybiBwW2FdLmNhbGwoZS5leHBvcnRzLGUsZS5leHBvcnRzLHgpLGUubG9hZGVkPSEwLGUuZXhwb3J0c312YXIgdD17fTtyZXR1cm4geC5tPXAseC5jPXQseC5wPVwiL1wiLHgoMCl9KFtmdW5jdGlvbihwLHgsdCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gYShwKXtyZXR1cm4gcCYmcC5fX2VzTW9kdWxlP3A6e2RlZmF1bHQ6cH19T2JqZWN0LmRlZmluZVByb3BlcnR5KHgsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSk7dmFyIGU9dCg0KSxvPWEoZSk7eC5kZWZhdWx0PW8uZGVmYXVsdCxcInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93JiZ3aW5kb3cuVnVlJiZ3aW5kb3cuVnVlLmNvbXBvbmVudChcImluZmluaXRlLWxvYWRpbmdcIixvLmRlZmF1bHQpfSxmdW5jdGlvbihwLHgpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHQocCl7cmV0dXJuXCJCT0RZXCI9PT1wLnRhZ05hbWU/d2luZG93OltcInNjcm9sbFwiLFwiYXV0b1wiXS5pbmRleE9mKGdldENvbXB1dGVkU3R5bGUocCkub3ZlcmZsb3dZKT4tMT9wOnAuaGFzQXR0cmlidXRlKFwiaW5maW5pdGUtd3JhcHBlclwiKXx8cC5oYXNBdHRyaWJ1dGUoXCJkYXRhLWluZmluaXRlLXdyYXBwZXJcIik/cDp0KHAucGFyZW50Tm9kZSl9ZnVuY3Rpb24gYShwLHgsdCl7dmFyIGE9dm9pZCAwO2lmKFwidG9wXCI9PT10KWE9aXNOYU4ocC5zY3JvbGxUb3ApP3AucGFnZVlPZmZzZXQ6cC5zY3JvbGxUb3A7ZWxzZXt2YXIgZT14LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcCxvPXA9PT13aW5kb3c/d2luZG93LmlubmVySGVpZ2h0OnAuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuYm90dG9tO2E9ZS1vfXJldHVybiBhfU9iamVjdC5kZWZpbmVQcm9wZXJ0eSh4LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pO3ZhciBlPXtidWJibGVzOlwibG9hZGluZy1idWJibGVzXCIsY2lyY2xlczpcImxvYWRpbmctY2lyY2xlc1wiLGRlZmF1bHQ6XCJsb2FkaW5nLWRlZmF1bHRcIixzcGlyYWw6XCJsb2FkaW5nLXNwaXJhbFwiLHdhdmVEb3RzOlwibG9hZGluZy13YXZlLWRvdHNcIn07eC5kZWZhdWx0PXtkYXRhOmZ1bmN0aW9uKCl7cmV0dXJue3Njcm9sbFBhcmVudDpudWxsLHNjcm9sbEhhbmRsZXI6bnVsbCxpc0xvYWRpbmc6ITEsaXNDb21wbGV0ZTohMSxpc0ZpcnN0TG9hZDohMH19LGNvbXB1dGVkOntzcGlubmVyVHlwZTpmdW5jdGlvbigpe3JldHVybiBlW3RoaXMuc3Bpbm5lcl18fGUuZGVmYXVsdH19LHByb3BzOntkaXN0YW5jZTp7dHlwZTpOdW1iZXIsZGVmYXVsdDoxMDB9LG9uSW5maW5pdGU6RnVuY3Rpb24sc3Bpbm5lcjpTdHJpbmcsZGlyZWN0aW9uOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiYm90dG9tXCJ9fSxtb3VudGVkOmZ1bmN0aW9uKCl7dmFyIHA9dGhpczt0aGlzLnNjcm9sbFBhcmVudD10KHRoaXMuJGVsKSx0aGlzLnNjcm9sbEhhbmRsZXI9ZnVuY3Rpb24oKXt0aGlzLmlzTG9hZGluZ3x8dGhpcy5hdHRlbXB0TG9hZCgpfS5iaW5kKHRoaXMpLHNldFRpbWVvdXQodGhpcy5zY3JvbGxIYW5kbGVyLDEpLHRoaXMuc2Nyb2xsUGFyZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIix0aGlzLnNjcm9sbEhhbmRsZXIpLHRoaXMuJG9uKFwiJEluZmluaXRlTG9hZGluZzpsb2FkZWRcIixmdW5jdGlvbigpe3AuaXNGaXJzdExvYWQ9ITEscC5pc0xvYWRpbmcmJnAuJG5leHRUaWNrKHAuYXR0ZW1wdExvYWQpfSksdGhpcy4kb24oXCIkSW5maW5pdGVMb2FkaW5nOmNvbXBsZXRlXCIsZnVuY3Rpb24oKXtwLmlzTG9hZGluZz0hMSxwLmlzQ29tcGxldGU9ITAscC5zY3JvbGxQYXJlbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHAuc2Nyb2xsSGFuZGxlcil9KSx0aGlzLiRvbihcIiRJbmZpbml0ZUxvYWRpbmc6cmVzZXRcIixmdW5jdGlvbigpe3AuaXNMb2FkaW5nPSExLHAuaXNDb21wbGV0ZT0hMSxwLmlzRmlyc3RMb2FkPSEwLHAuc2Nyb2xsUGFyZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIixwLnNjcm9sbEhhbmRsZXIpLHNldFRpbWVvdXQocC5zY3JvbGxIYW5kbGVyLDEpfSl9LGRlYWN0aXZhdGVkOmZ1bmN0aW9uKCl7dGhpcy5pc0xvYWRpbmc9ITEsdGhpcy5zY3JvbGxQYXJlbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHRoaXMuc2Nyb2xsSGFuZGxlcil9LGFjdGl2YXRlZDpmdW5jdGlvbigpe3RoaXMuc2Nyb2xsUGFyZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIix0aGlzLnNjcm9sbEhhbmRsZXIpfSxtZXRob2RzOnthdHRlbXB0TG9hZDpmdW5jdGlvbigpe3ZhciBwPWEodGhpcy5zY3JvbGxQYXJlbnQsdGhpcy4kZWwsdGhpcy5kaXJlY3Rpb24pOyF0aGlzLmlzQ29tcGxldGUmJnA8PXRoaXMuZGlzdGFuY2U/KHRoaXMuaXNMb2FkaW5nPSEwLHRoaXMub25JbmZpbml0ZS5jYWxsKCkpOnRoaXMuaXNMb2FkaW5nPSExfX0sZGVzdHJveWVkOmZ1bmN0aW9uKCl7dGhpcy5pc0NvbXBsZXRlfHx0aGlzLnNjcm9sbFBhcmVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwic2Nyb2xsXCIsdGhpcy5zY3JvbGxIYW5kbGVyKX19fSxmdW5jdGlvbihwLHgsdCl7eD1wLmV4cG9ydHM9dCgzKSgpLHgucHVzaChbcC5pZCwnLmxvYWRpbmctd2F2ZS1kb3RzW2RhdGEtdi01MDc5M2YwMl17cG9zaXRpb246cmVsYXRpdmV9LmxvYWRpbmctd2F2ZS1kb3RzW2RhdGEtdi01MDc5M2YwMl06YmVmb3Jle2NvbnRlbnQ6XCJcIjtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6NTAlO2xlZnQ6NTAlO21hcmdpbi1sZWZ0Oi00cHg7bWFyZ2luLXRvcDotNHB4O3dpZHRoOjhweDtoZWlnaHQ6OHB4O2JhY2tncm91bmQtY29sb3I6I2JiYjtib3JkZXItcmFkaXVzOjUwJTstd2Via2l0LWFuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy13YXZlLWRvdHMgMi44cyBpbmZpbml0ZTthbmltYXRpb246bGluZWFyIGxvYWRpbmctd2F2ZS1kb3RzIDIuOHMgaW5maW5pdGV9QC13ZWJraXQta2V5ZnJhbWVzIGxvYWRpbmctd2F2ZS1kb3RzezAle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJifTUle2JveC1zaGFkb3c6LTMycHggLTRweCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MTAle2JveC1zaGFkb3c6LTMycHggLTZweCAwICM5OTksLTE2cHggLTRweCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IDAgMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MTUle2JveC1zaGFkb3c6LTMycHggMnB4IDAgI2JiYiwtMTZweCAtMnB4IDAgIzk5OSwxNnB4IDRweCAwICNiYmIsMzJweCA0cHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTRweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTRweCk7YmFja2dyb3VuZC1jb2xvcjojYmJifTIwJXtib3gtc2hhZG93Oi0zMnB4IDZweCAwICNiYmIsLTE2cHggNHB4IDAgI2JiYiwxNnB4IDJweCAwICNiYmIsMzJweCA2cHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTZweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTZweCk7YmFja2dyb3VuZC1jb2xvcjojOTk5fTI1JXtib3gtc2hhZG93Oi0zMnB4IDJweCAwICNiYmIsLTE2cHggMnB4IDAgI2JiYiwxNnB4IC00cHggMCAjOTk5LDMycHggLTJweCAwICNiYmI7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtMnB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWSgtMnB4KTtiYWNrZ3JvdW5kLWNvbG9yOiNiYmJ9MzAle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAtMnB4IDAgI2JiYiwzMnB4IC02cHggMCAjOTk5Oy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoMCl9MzUle2JveC1zaGFkb3c6LTMycHggMCAwICNiYmIsLTE2cHggMCAwICNiYmIsMTZweCAwIDAgI2JiYiwzMnB4IC0ycHggMCAjYmJifTQwJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn10b3tib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn19QGtleWZyYW1lcyBsb2FkaW5nLXdhdmUtZG90c3swJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYn01JXtib3gtc2hhZG93Oi0zMnB4IC00cHggMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTEwJXtib3gtc2hhZG93Oi0zMnB4IC02cHggMCAjOTk5LC0xNnB4IC00cHggMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAwIDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTE1JXtib3gtc2hhZG93Oi0zMnB4IDJweCAwICNiYmIsLTE2cHggLTJweCAwICM5OTksMTZweCA0cHggMCAjYmJiLDMycHggNHB4IDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC00cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC00cHgpO2JhY2tncm91bmQtY29sb3I6I2JiYn0yMCV7Ym94LXNoYWRvdzotMzJweCA2cHggMCAjYmJiLC0xNnB4IDRweCAwICNiYmIsMTZweCAycHggMCAjYmJiLDMycHggNnB4IDAgI2JiYjstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC02cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC02cHgpO2JhY2tncm91bmQtY29sb3I6Izk5OX0yNSV7Ym94LXNoYWRvdzotMzJweCAycHggMCAjYmJiLC0xNnB4IDJweCAwICNiYmIsMTZweCAtNHB4IDAgIzk5OSwzMnB4IC0ycHggMCAjYmJiOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTJweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTJweCk7YmFja2dyb3VuZC1jb2xvcjojYmJifTMwJXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggLTJweCAwICNiYmIsMzJweCAtNnB4IDAgIzk5OTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVZKDApfTM1JXtib3gtc2hhZG93Oi0zMnB4IDAgMCAjYmJiLC0xNnB4IDAgMCAjYmJiLDE2cHggMCAwICNiYmIsMzJweCAtMnB4IDAgI2JiYn00MCV7Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmJ9dG97Ym94LXNoYWRvdzotMzJweCAwIDAgI2JiYiwtMTZweCAwIDAgI2JiYiwxNnB4IDAgMCAjYmJiLDMycHggMCAwICNiYmJ9fS5sb2FkaW5nLWNpcmNsZXNbZGF0YS12LTUwNzkzZjAyXXtwb3NpdGlvbjpyZWxhdGl2ZX0ubG9hZGluZy1jaXJjbGVzW2RhdGEtdi01MDc5M2YwMl06YmVmb3Jle2NvbnRlbnQ6XCJcIjtwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjUwJTt0b3A6NTAlO21hcmdpbi10b3A6LTIuNXB4O21hcmdpbi1sZWZ0Oi0yLjVweDt3aWR0aDo1cHg7aGVpZ2h0OjVweDtib3JkZXItcmFkaXVzOjUwJTstd2Via2l0LWFuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1jaXJjbGVzIC43NXMgaW5maW5pdGU7YW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLWNpcmNsZXMgLjc1cyBpbmZpbml0ZX1ALXdlYmtpdC1rZXlmcmFtZXMgbG9hZGluZy1jaXJjbGVzezAle2JveC1zaGFkb3c6MCAtMTJweCAwICM1MDUwNTAsOC41MnB4IC04LjUycHggMCAjNjQ2NDY0LDEycHggMCAwICM3OTc5NzksOC41MnB4IDguNTJweCAwICM4ZDhkOGQsMCAxMnB4IDAgI2EyYTJhMiwtOC41MnB4IDguNTJweCAwICNiNmI2YjYsLTEycHggMCAwICNjYWNhY2EsLTguNTJweCAtOC41MnB4IDAgI2RmZGZkZn0xMi41JXtib3gtc2hhZG93OjAgLTEycHggMCAjZGZkZmRmLDguNTJweCAtOC41MnB4IDAgIzUwNTA1MCwxMnB4IDAgMCAjNjQ2NDY0LDguNTJweCA4LjUycHggMCAjNzk3OTc5LDAgMTJweCAwICM4ZDhkOGQsLTguNTJweCA4LjUycHggMCAjYTJhMmEyLC0xMnB4IDAgMCAjYjZiNmI2LC04LjUycHggLTguNTJweCAwICNjYWNhY2F9MjUle2JveC1zaGFkb3c6MCAtMTJweCAwICNjYWNhY2EsOC41MnB4IC04LjUycHggMCAjZGZkZmRmLDEycHggMCAwICM1MDUwNTAsOC41MnB4IDguNTJweCAwICM2NDY0NjQsMCAxMnB4IDAgIzc5Nzk3OSwtOC41MnB4IDguNTJweCAwICM4ZDhkOGQsLTEycHggMCAwICNhMmEyYTIsLTguNTJweCAtOC41MnB4IDAgI2I2YjZiNn0zNy41JXtib3gtc2hhZG93OjAgLTEycHggMCAjYjZiNmI2LDguNTJweCAtOC41MnB4IDAgI2NhY2FjYSwxMnB4IDAgMCAjZGZkZmRmLDguNTJweCA4LjUycHggMCAjNTA1MDUwLDAgMTJweCAwICM2NDY0NjQsLTguNTJweCA4LjUycHggMCAjNzk3OTc5LC0xMnB4IDAgMCAjOGQ4ZDhkLC04LjUycHggLTguNTJweCAwICNhMmEyYTJ9NTAle2JveC1zaGFkb3c6MCAtMTJweCAwICNhMmEyYTIsOC41MnB4IC04LjUycHggMCAjYjZiNmI2LDEycHggMCAwICNjYWNhY2EsOC41MnB4IDguNTJweCAwICNkZmRmZGYsMCAxMnB4IDAgIzUwNTA1MCwtOC41MnB4IDguNTJweCAwICM2NDY0NjQsLTEycHggMCAwICM3OTc5NzksLTguNTJweCAtOC41MnB4IDAgIzhkOGQ4ZH02Mi41JXtib3gtc2hhZG93OjAgLTEycHggMCAjOGQ4ZDhkLDguNTJweCAtOC41MnB4IDAgI2EyYTJhMiwxMnB4IDAgMCAjYjZiNmI2LDguNTJweCA4LjUycHggMCAjY2FjYWNhLDAgMTJweCAwICNkZmRmZGYsLTguNTJweCA4LjUycHggMCAjNTA1MDUwLC0xMnB4IDAgMCAjNjQ2NDY0LC04LjUycHggLTguNTJweCAwICM3OTc5Nzl9NzUle2JveC1zaGFkb3c6MCAtMTJweCAwICM3OTc5NzksOC41MnB4IC04LjUycHggMCAjOGQ4ZDhkLDEycHggMCAwICNhMmEyYTIsOC41MnB4IDguNTJweCAwICNiNmI2YjYsMCAxMnB4IDAgI2NhY2FjYSwtOC41MnB4IDguNTJweCAwICNkZmRmZGYsLTEycHggMCAwICM1MDUwNTAsLTguNTJweCAtOC41MnB4IDAgIzY0NjQ2NH04Ny41JXtib3gtc2hhZG93OjAgLTEycHggMCAjNjQ2NDY0LDguNTJweCAtOC41MnB4IDAgIzc5Nzk3OSwxMnB4IDAgMCAjOGQ4ZDhkLDguNTJweCA4LjUycHggMCAjYTJhMmEyLDAgMTJweCAwICNiNmI2YjYsLTguNTJweCA4LjUycHggMCAjY2FjYWNhLC0xMnB4IDAgMCAjZGZkZmRmLC04LjUycHggLTguNTJweCAwICM1MDUwNTB9dG97Ym94LXNoYWRvdzowIC0xMnB4IDAgIzUwNTA1MCw4LjUycHggLTguNTJweCAwICM2NDY0NjQsMTJweCAwIDAgIzc5Nzk3OSw4LjUycHggOC41MnB4IDAgIzhkOGQ4ZCwwIDEycHggMCAjYTJhMmEyLC04LjUycHggOC41MnB4IDAgI2I2YjZiNiwtMTJweCAwIDAgI2NhY2FjYSwtOC41MnB4IC04LjUycHggMCAjZGZkZmRmfX1Aa2V5ZnJhbWVzIGxvYWRpbmctY2lyY2xlc3swJXtib3gtc2hhZG93OjAgLTEycHggMCAjNTA1MDUwLDguNTJweCAtOC41MnB4IDAgIzY0NjQ2NCwxMnB4IDAgMCAjNzk3OTc5LDguNTJweCA4LjUycHggMCAjOGQ4ZDhkLDAgMTJweCAwICNhMmEyYTIsLTguNTJweCA4LjUycHggMCAjYjZiNmI2LC0xMnB4IDAgMCAjY2FjYWNhLC04LjUycHggLTguNTJweCAwICNkZmRmZGZ9MTIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2RmZGZkZiw4LjUycHggLTguNTJweCAwICM1MDUwNTAsMTJweCAwIDAgIzY0NjQ2NCw4LjUycHggOC41MnB4IDAgIzc5Nzk3OSwwIDEycHggMCAjOGQ4ZDhkLC04LjUycHggOC41MnB4IDAgI2EyYTJhMiwtMTJweCAwIDAgI2I2YjZiNiwtOC41MnB4IC04LjUycHggMCAjY2FjYWNhfTI1JXtib3gtc2hhZG93OjAgLTEycHggMCAjY2FjYWNhLDguNTJweCAtOC41MnB4IDAgI2RmZGZkZiwxMnB4IDAgMCAjNTA1MDUwLDguNTJweCA4LjUycHggMCAjNjQ2NDY0LDAgMTJweCAwICM3OTc5NzksLTguNTJweCA4LjUycHggMCAjOGQ4ZDhkLC0xMnB4IDAgMCAjYTJhMmEyLC04LjUycHggLTguNTJweCAwICNiNmI2YjZ9MzcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgI2I2YjZiNiw4LjUycHggLTguNTJweCAwICNjYWNhY2EsMTJweCAwIDAgI2RmZGZkZiw4LjUycHggOC41MnB4IDAgIzUwNTA1MCwwIDEycHggMCAjNjQ2NDY0LC04LjUycHggOC41MnB4IDAgIzc5Nzk3OSwtMTJweCAwIDAgIzhkOGQ4ZCwtOC41MnB4IC04LjUycHggMCAjYTJhMmEyfTUwJXtib3gtc2hhZG93OjAgLTEycHggMCAjYTJhMmEyLDguNTJweCAtOC41MnB4IDAgI2I2YjZiNiwxMnB4IDAgMCAjY2FjYWNhLDguNTJweCA4LjUycHggMCAjZGZkZmRmLDAgMTJweCAwICM1MDUwNTAsLTguNTJweCA4LjUycHggMCAjNjQ2NDY0LC0xMnB4IDAgMCAjNzk3OTc5LC04LjUycHggLTguNTJweCAwICM4ZDhkOGR9NjIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzhkOGQ4ZCw4LjUycHggLTguNTJweCAwICNhMmEyYTIsMTJweCAwIDAgI2I2YjZiNiw4LjUycHggOC41MnB4IDAgI2NhY2FjYSwwIDEycHggMCAjZGZkZmRmLC04LjUycHggOC41MnB4IDAgIzUwNTA1MCwtMTJweCAwIDAgIzY0NjQ2NCwtOC41MnB4IC04LjUycHggMCAjNzk3OTc5fTc1JXtib3gtc2hhZG93OjAgLTEycHggMCAjNzk3OTc5LDguNTJweCAtOC41MnB4IDAgIzhkOGQ4ZCwxMnB4IDAgMCAjYTJhMmEyLDguNTJweCA4LjUycHggMCAjYjZiNmI2LDAgMTJweCAwICNjYWNhY2EsLTguNTJweCA4LjUycHggMCAjZGZkZmRmLC0xMnB4IDAgMCAjNTA1MDUwLC04LjUycHggLTguNTJweCAwICM2NDY0NjR9ODcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgIzY0NjQ2NCw4LjUycHggLTguNTJweCAwICM3OTc5NzksMTJweCAwIDAgIzhkOGQ4ZCw4LjUycHggOC41MnB4IDAgI2EyYTJhMiwwIDEycHggMCAjYjZiNmI2LC04LjUycHggOC41MnB4IDAgI2NhY2FjYSwtMTJweCAwIDAgI2RmZGZkZiwtOC41MnB4IC04LjUycHggMCAjNTA1MDUwfXRve2JveC1zaGFkb3c6MCAtMTJweCAwICM1MDUwNTAsOC41MnB4IC04LjUycHggMCAjNjQ2NDY0LDEycHggMCAwICM3OTc5NzksOC41MnB4IDguNTJweCAwICM4ZDhkOGQsMCAxMnB4IDAgI2EyYTJhMiwtOC41MnB4IDguNTJweCAwICNiNmI2YjYsLTEycHggMCAwICNjYWNhY2EsLTguNTJweCAtOC41MnB4IDAgI2RmZGZkZn19LmxvYWRpbmctYnViYmxlc1tkYXRhLXYtNTA3OTNmMDJde3Bvc2l0aW9uOnJlbGF0aXZlfS5sb2FkaW5nLWJ1YmJsZXNbZGF0YS12LTUwNzkzZjAyXTpiZWZvcmV7Y29udGVudDpcIlwiO3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6NTAlO3RvcDo1MCU7bWFyZ2luLXRvcDotLjVweDttYXJnaW4tbGVmdDotLjVweDt3aWR0aDoxcHg7aGVpZ2h0OjFweDtib3JkZXItcmFkaXVzOjUwJTstd2Via2l0LWFuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1idWJibGVzIC44NXMgaW5maW5pdGU7YW5pbWF0aW9uOmxpbmVhciBsb2FkaW5nLWJ1YmJsZXMgLjg1cyBpbmZpbml0ZX1ALXdlYmtpdC1rZXlmcmFtZXMgbG9hZGluZy1idWJibGVzezAle2JveC1zaGFkb3c6MCAtMTJweCAwIC40cHggIzY2Niw4LjUycHggLTguNTJweCAwIC44cHggIzY2NiwxMnB4IDAgMCAxLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjZweCAjNjY2LDAgMTJweCAwIDJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwtMTJweCAwIDAgMi44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAzLjJweCAjNjY2fTEyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDMuMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuNHB4ICM2NjYsMTJweCAwIDAgLjhweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjJweCAjNjY2LDAgMTJweCAwIDEuNnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAycHggIzY2NiwtMTJweCAwIDAgMi40cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAyLjhweCAjNjY2fTI1JXtib3gtc2hhZG93OjAgLTEycHggMCAyLjhweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2NiwxMnB4IDAgMCAuNHB4ICM2NjYsOC41MnB4IDguNTJweCAwIC44cHggIzY2NiwwIDEycHggMCAxLjJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwtMTJweCAwIDAgMnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMi40cHggIzY2Nn0zNy41JXtib3gtc2hhZG93OjAgLTEycHggMCAyLjRweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMi44cHggIzY2NiwxMnB4IDAgMCAzLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAuNHB4ICM2NjYsMCAxMnB4IDAgLjhweCAjNjY2LC04LjUycHggOC41MnB4IDAgMS4ycHggIzY2NiwtMTJweCAwIDAgMS42cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAycHggIzY2Nn01MCV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAyLjRweCAjNjY2LDEycHggMCAwIDIuOHB4ICM2NjYsOC41MnB4IDguNTJweCAwIDMuMnB4ICM2NjYsMCAxMnB4IDAgLjRweCAjNjY2LC04LjUycHggOC41MnB4IDAgLjhweCAjNjY2LC0xMnB4IDAgMCAxLjJweCAjNjY2LC04LjUycHggLTguNTJweCAwIDEuNnB4ICM2NjZ9NjIuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMS42cHggIzY2Niw4LjUycHggLTguNTJweCAwIDJweCAjNjY2LDEycHggMCAwIDIuNHB4ICM2NjYsOC41MnB4IDguNTJweCAwIDIuOHB4ICM2NjYsMCAxMnB4IDAgMy4ycHggIzY2NiwtOC41MnB4IDguNTJweCAwIC40cHggIzY2NiwtMTJweCAwIDAgLjhweCAjNjY2LC04LjUycHggLTguNTJweCAwIDEuMnB4ICM2NjZ9NzUle2JveC1zaGFkb3c6MCAtMTJweCAwIDEuMnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAxLjZweCAjNjY2LDEycHggMCAwIDJweCAjNjY2LDguNTJweCA4LjUycHggMCAyLjRweCAjNjY2LDAgMTJweCAwIDIuOHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAzLjJweCAjNjY2LC0xMnB4IDAgMCAuNHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgLjhweCAjNjY2fTg3LjUle2JveC1zaGFkb3c6MCAtMTJweCAwIC44cHggIzY2Niw4LjUycHggLTguNTJweCAwIDEuMnB4ICM2NjYsMTJweCAwIDAgMS42cHggIzY2Niw4LjUycHggOC41MnB4IDAgMnB4ICM2NjYsMCAxMnB4IDAgMi40cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuOHB4ICM2NjYsLTEycHggMCAwIDMuMnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgLjRweCAjNjY2fXRve2JveC1zaGFkb3c6MCAtMTJweCAwIC40cHggIzY2Niw4LjUycHggLTguNTJweCAwIC44cHggIzY2NiwxMnB4IDAgMCAxLjJweCAjNjY2LDguNTJweCA4LjUycHggMCAxLjZweCAjNjY2LDAgMTJweCAwIDJweCAjNjY2LC04LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwtMTJweCAwIDAgMi44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAzLjJweCAjNjY2fX1Aa2V5ZnJhbWVzIGxvYWRpbmctYnViYmxlc3swJXtib3gtc2hhZG93OjAgLTEycHggMCAuNHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuOHB4ICM2NjYsMTJweCAwIDAgMS4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwwIDEycHggMCAycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuNHB4ICM2NjYsLTEycHggMCAwIDIuOHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2Nn0xMi41JXtib3gtc2hhZG93OjAgLTEycHggMCAzLjJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgLjRweCAjNjY2LDEycHggMCAwIC44cHggIzY2Niw4LjUycHggOC41MnB4IDAgMS4ycHggIzY2NiwwIDEycHggMCAxLjZweCAjNjY2LC04LjUycHggOC41MnB4IDAgMnB4ICM2NjYsLTEycHggMCAwIDIuNHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMi44cHggIzY2Nn0yNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMi44cHggIzY2Niw4LjUycHggLTguNTJweCAwIDMuMnB4ICM2NjYsMTJweCAwIDAgLjRweCAjNjY2LDguNTJweCA4LjUycHggMCAuOHB4ICM2NjYsMCAxMnB4IDAgMS4ycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDEuNnB4ICM2NjYsLTEycHggMCAwIDJweCAjNjY2LC04LjUycHggLTguNTJweCAwIDIuNHB4ICM2NjZ9MzcuNSV7Ym94LXNoYWRvdzowIC0xMnB4IDAgMi40cHggIzY2Niw4LjUycHggLTguNTJweCAwIDIuOHB4ICM2NjYsMTJweCAwIDAgMy4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgLjRweCAjNjY2LDAgMTJweCAwIC44cHggIzY2NiwtOC41MnB4IDguNTJweCAwIDEuMnB4ICM2NjYsLTEycHggMCAwIDEuNnB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMnB4ICM2NjZ9NTAle2JveC1zaGFkb3c6MCAtMTJweCAwIDJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMi40cHggIzY2NiwxMnB4IDAgMCAyLjhweCAjNjY2LDguNTJweCA4LjUycHggMCAzLjJweCAjNjY2LDAgMTJweCAwIC40cHggIzY2NiwtOC41MnB4IDguNTJweCAwIC44cHggIzY2NiwtMTJweCAwIDAgMS4ycHggIzY2NiwtOC41MnB4IC04LjUycHggMCAxLjZweCAjNjY2fTYyLjUle2JveC1zaGFkb3c6MCAtMTJweCAwIDEuNnB4ICM2NjYsOC41MnB4IC04LjUycHggMCAycHggIzY2NiwxMnB4IDAgMCAyLjRweCAjNjY2LDguNTJweCA4LjUycHggMCAyLjhweCAjNjY2LDAgMTJweCAwIDMuMnB4ICM2NjYsLTguNTJweCA4LjUycHggMCAuNHB4ICM2NjYsLTEycHggMCAwIC44cHggIzY2NiwtOC41MnB4IC04LjUycHggMCAxLjJweCAjNjY2fTc1JXtib3gtc2hhZG93OjAgLTEycHggMCAxLjJweCAjNjY2LDguNTJweCAtOC41MnB4IDAgMS42cHggIzY2NiwxMnB4IDAgMCAycHggIzY2Niw4LjUycHggOC41MnB4IDAgMi40cHggIzY2NiwwIDEycHggMCAyLjhweCAjNjY2LC04LjUycHggOC41MnB4IDAgMy4ycHggIzY2NiwtMTJweCAwIDAgLjRweCAjNjY2LC04LjUycHggLTguNTJweCAwIC44cHggIzY2Nn04Ny41JXtib3gtc2hhZG93OjAgLTEycHggMCAuOHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAxLjJweCAjNjY2LDEycHggMCAwIDEuNnB4ICM2NjYsOC41MnB4IDguNTJweCAwIDJweCAjNjY2LDAgMTJweCAwIDIuNHB4ICM2NjYsLTguNTJweCA4LjUycHggMCAyLjhweCAjNjY2LC0xMnB4IDAgMCAzLjJweCAjNjY2LC04LjUycHggLTguNTJweCAwIC40cHggIzY2Nn10b3tib3gtc2hhZG93OjAgLTEycHggMCAuNHB4ICM2NjYsOC41MnB4IC04LjUycHggMCAuOHB4ICM2NjYsMTJweCAwIDAgMS4ycHggIzY2Niw4LjUycHggOC41MnB4IDAgMS42cHggIzY2NiwwIDEycHggMCAycHggIzY2NiwtOC41MnB4IDguNTJweCAwIDIuNHB4ICM2NjYsLTEycHggMCAwIDIuOHB4ICM2NjYsLTguNTJweCAtOC41MnB4IDAgMy4ycHggIzY2Nn19LmxvYWRpbmctZGVmYXVsdFtkYXRhLXYtNTA3OTNmMDJde3Bvc2l0aW9uOnJlbGF0aXZlO2JvcmRlcjoxcHggc29saWQgIzk5OTstd2Via2l0LWFuaW1hdGlvbjplYXNlIGxvYWRpbmctcm90YXRpbmcgMS41cyBpbmZpbml0ZTthbmltYXRpb246ZWFzZSBsb2FkaW5nLXJvdGF0aW5nIDEuNXMgaW5maW5pdGV9LmxvYWRpbmctZGVmYXVsdFtkYXRhLXYtNTA3OTNmMDJdOmJlZm9yZXtjb250ZW50OlwiXCI7cG9zaXRpb246YWJzb2x1dGU7ZGlzcGxheTpibG9jazt0b3A6MDtsZWZ0OjUwJTttYXJnaW4tdG9wOi0zcHg7bWFyZ2luLWxlZnQ6LTNweDt3aWR0aDo2cHg7aGVpZ2h0OjZweDtiYWNrZ3JvdW5kLWNvbG9yOiM5OTk7Ym9yZGVyLXJhZGl1czo1MCV9LmxvYWRpbmctc3BpcmFsW2RhdGEtdi01MDc5M2YwMl17Ym9yZGVyOjJweCBzb2xpZCAjNzc3O2JvcmRlci1yaWdodC1jb2xvcjp0cmFuc3BhcmVudDstd2Via2l0LWFuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1yb3RhdGluZyAuODVzIGluZmluaXRlO2FuaW1hdGlvbjpsaW5lYXIgbG9hZGluZy1yb3RhdGluZyAuODVzIGluZmluaXRlfUAtd2Via2l0LWtleWZyYW1lcyBsb2FkaW5nLXJvdGF0aW5nezAley13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgwKTt0cmFuc2Zvcm06cm90YXRlKDApfXRvey13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgxdHVybik7dHJhbnNmb3JtOnJvdGF0ZSgxdHVybil9fUBrZXlmcmFtZXMgbG9hZGluZy1yb3RhdGluZ3swJXstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMCk7dHJhbnNmb3JtOnJvdGF0ZSgwKX10b3std2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMXR1cm4pO3RyYW5zZm9ybTpyb3RhdGUoMXR1cm4pfX0uaW5maW5pdGUtbG9hZGluZy1jb250YWluZXJbZGF0YS12LTUwNzkzZjAyXXtjbGVhcjpib3RoO3RleHQtYWxpZ246Y2VudGVyfS5pbmZpbml0ZS1sb2FkaW5nLWNvbnRhaW5lciBbY2xhc3NePWxvYWRpbmctXVtkYXRhLXYtNTA3OTNmMDJde2Rpc3BsYXk6aW5saW5lLWJsb2NrO21hcmdpbjoxNXB4IDA7d2lkdGg6MjhweDtoZWlnaHQ6MjhweDtmb250LXNpemU6MjhweDtsaW5lLWhlaWdodDoyOHB4O2JvcmRlci1yYWRpdXM6NTAlfS5pbmZpbml0ZS1zdGF0dXMtcHJvbXB0W2RhdGEtdi01MDc5M2YwMl17Y29sb3I6IzY2Njtmb250LXNpemU6MTRweDt0ZXh0LWFsaWduOmNlbnRlcjtwYWRkaW5nOjEwcHggMH0nLFwiXCJdKX0sZnVuY3Rpb24ocCx4KXtwLmV4cG9ydHM9ZnVuY3Rpb24oKXt2YXIgcD1bXTtyZXR1cm4gcC50b1N0cmluZz1mdW5jdGlvbigpe2Zvcih2YXIgcD1bXSx4PTA7eDx0aGlzLmxlbmd0aDt4Kyspe3ZhciB0PXRoaXNbeF07dFsyXT9wLnB1c2goXCJAbWVkaWEgXCIrdFsyXStcIntcIit0WzFdK1wifVwiKTpwLnB1c2godFsxXSl9cmV0dXJuIHAuam9pbihcIlwiKX0scC5pPWZ1bmN0aW9uKHgsdCl7XCJzdHJpbmdcIj09dHlwZW9mIHgmJih4PVtbbnVsbCx4LFwiXCJdXSk7Zm9yKHZhciBhPXt9LGU9MDtlPHRoaXMubGVuZ3RoO2UrKyl7dmFyIG89dGhpc1tlXVswXTtcIm51bWJlclwiPT10eXBlb2YgbyYmKGFbb109ITApfWZvcihlPTA7ZTx4Lmxlbmd0aDtlKyspe3ZhciBuPXhbZV07XCJudW1iZXJcIj09dHlwZW9mIG5bMF0mJmFbblswXV18fCh0JiYhblsyXT9uWzJdPXQ6dCYmKG5bMl09XCIoXCIrblsyXStcIikgYW5kIChcIit0K1wiKVwiKSxwLnB1c2gobikpfX0scH19LGZ1bmN0aW9uKHAseCx0KXt2YXIgYSxlO3QoNyksYT10KDEpO3ZhciBvPXQoNSk7ZT1hPWF8fHt9LFwib2JqZWN0XCIhPXR5cGVvZiBhLmRlZmF1bHQmJlwiZnVuY3Rpb25cIiE9dHlwZW9mIGEuZGVmYXVsdHx8KGU9YT1hLmRlZmF1bHQpLFwiZnVuY3Rpb25cIj09dHlwZW9mIGUmJihlPWUub3B0aW9ucyksZS5yZW5kZXI9by5yZW5kZXIsZS5zdGF0aWNSZW5kZXJGbnM9by5zdGF0aWNSZW5kZXJGbnMsZS5fc2NvcGVJZD1cImRhdGEtdi01MDc5M2YwMlwiLHAuZXhwb3J0cz1hfSxmdW5jdGlvbihwLHgpe3AuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uKCl7dmFyIHA9dGhpcyx4PXAuJGNyZWF0ZUVsZW1lbnQsdD1wLl9zZWxmLl9jfHx4O3JldHVybiB0KFwiZGl2XCIse3N0YXRpY0NsYXNzOlwiaW5maW5pdGUtbG9hZGluZy1jb250YWluZXJcIn0sW3QoXCJkaXZcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJzaG93XCIscmF3TmFtZTpcInYtc2hvd1wiLHZhbHVlOnAuaXNMb2FkaW5nLGV4cHJlc3Npb246XCJpc0xvYWRpbmdcIn1dfSxbcC5fdChcInNwaW5uZXJcIixbdChcImlcIix7Y2xhc3M6cC5zcGlubmVyVHlwZX0pXSldLDIpLHAuX3YoXCIgXCIpLHQoXCJkaXZcIix7ZGlyZWN0aXZlczpbe25hbWU6XCJzaG93XCIscmF3TmFtZTpcInYtc2hvd1wiLHZhbHVlOiFwLmlzTG9hZGluZyYmcC5pc0NvbXBsZXRlJiZwLmlzRmlyc3RMb2FkLGV4cHJlc3Npb246XCIhaXNMb2FkaW5nICYmIGlzQ29tcGxldGUgJiYgaXNGaXJzdExvYWRcIn1dLHN0YXRpY0NsYXNzOlwiaW5maW5pdGUtc3RhdHVzLXByb21wdFwifSxbcC5fdChcIm5vLXJlc3VsdHNcIixbcC5fdihcIk5vIHJlc3VsdHMgOihcIildKV0sMikscC5fdihcIiBcIiksdChcImRpdlwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6IXAuaXNMb2FkaW5nJiZwLmlzQ29tcGxldGUmJiFwLmlzRmlyc3RMb2FkLGV4cHJlc3Npb246XCIhaXNMb2FkaW5nICYmIGlzQ29tcGxldGUgJiYgIWlzRmlyc3RMb2FkXCJ9XSxzdGF0aWNDbGFzczpcImluZmluaXRlLXN0YXR1cy1wcm9tcHRcIn0sW3AuX3QoXCJuby1tb3JlXCIsW3AuX3YoXCJObyBtb3JlIGRhdGEgOilcIildKV0sMildKX0sc3RhdGljUmVuZGVyRm5zOltdfX0sZnVuY3Rpb24ocCx4LHQpe2Z1bmN0aW9uIGEocCx4KXtmb3IodmFyIHQ9MDt0PHAubGVuZ3RoO3QrKyl7dmFyIGE9cFt0XSxlPWRbYS5pZF07aWYoZSl7ZS5yZWZzKys7Zm9yKHZhciBvPTA7bzxlLnBhcnRzLmxlbmd0aDtvKyspZS5wYXJ0c1tvXShhLnBhcnRzW29dKTtmb3IoO288YS5wYXJ0cy5sZW5ndGg7bysrKWUucGFydHMucHVzaChyKGEucGFydHNbb10seCkpfWVsc2V7Zm9yKHZhciBuPVtdLG89MDtvPGEucGFydHMubGVuZ3RoO28rKyluLnB1c2gocihhLnBhcnRzW29dLHgpKTtkW2EuaWRdPXtpZDphLmlkLHJlZnM6MSxwYXJ0czpufX19fWZ1bmN0aW9uIGUocCl7Zm9yKHZhciB4PVtdLHQ9e30sYT0wO2E8cC5sZW5ndGg7YSsrKXt2YXIgZT1wW2FdLG89ZVswXSxuPWVbMV0saT1lWzJdLHI9ZVszXSxzPXtjc3M6bixtZWRpYTppLHNvdXJjZU1hcDpyfTt0W29dP3Rbb10ucGFydHMucHVzaChzKTp4LnB1c2godFtvXT17aWQ6byxwYXJ0czpbc119KX1yZXR1cm4geH1mdW5jdGlvbiBvKHAseCl7dmFyIHQ9YygpLGE9bVttLmxlbmd0aC0xXTtpZihcInRvcFwiPT09cC5pbnNlcnRBdClhP2EubmV4dFNpYmxpbmc/dC5pbnNlcnRCZWZvcmUoeCxhLm5leHRTaWJsaW5nKTp0LmFwcGVuZENoaWxkKHgpOnQuaW5zZXJ0QmVmb3JlKHgsdC5maXJzdENoaWxkKSxtLnB1c2goeCk7ZWxzZXtpZihcImJvdHRvbVwiIT09cC5pbnNlcnRBdCl0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIHZhbHVlIGZvciBwYXJhbWV0ZXIgJ2luc2VydEF0Jy4gTXVzdCBiZSAndG9wJyBvciAnYm90dG9tJy5cIik7dC5hcHBlbmRDaGlsZCh4KX19ZnVuY3Rpb24gbihwKXtwLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQocCk7dmFyIHg9bS5pbmRleE9mKHApO3g+PTAmJm0uc3BsaWNlKHgsMSl9ZnVuY3Rpb24gaShwKXt2YXIgeD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic3R5bGVcIik7cmV0dXJuIHgudHlwZT1cInRleHQvY3NzXCIsbyhwLHgpLHh9ZnVuY3Rpb24gcihwLHgpe3ZhciB0LGEsZTtpZih4LnNpbmdsZXRvbil7dmFyIG89aCsrO3Q9dXx8KHU9aSh4KSksYT1zLmJpbmQobnVsbCx0LG8sITEpLGU9cy5iaW5kKG51bGwsdCxvLCEwKX1lbHNlIHQ9aSh4KSxhPWIuYmluZChudWxsLHQpLGU9ZnVuY3Rpb24oKXtuKHQpfTtyZXR1cm4gYShwKSxmdW5jdGlvbih4KXtpZih4KXtpZih4LmNzcz09PXAuY3NzJiZ4Lm1lZGlhPT09cC5tZWRpYSYmeC5zb3VyY2VNYXA9PT1wLnNvdXJjZU1hcClyZXR1cm47YShwPXgpfWVsc2UgZSgpfX1mdW5jdGlvbiBzKHAseCx0LGEpe3ZhciBlPXQ/XCJcIjphLmNzcztpZihwLnN0eWxlU2hlZXQpcC5zdHlsZVNoZWV0LmNzc1RleHQ9Zyh4LGUpO2Vsc2V7dmFyIG89ZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoZSksbj1wLmNoaWxkTm9kZXM7blt4XSYmcC5yZW1vdmVDaGlsZChuW3hdKSxuLmxlbmd0aD9wLmluc2VydEJlZm9yZShvLG5beF0pOnAuYXBwZW5kQ2hpbGQobyl9fWZ1bmN0aW9uIGIocCx4KXt2YXIgdD14LmNzcyxhPXgubWVkaWEsZT14LnNvdXJjZU1hcDtpZihhJiZwLnNldEF0dHJpYnV0ZShcIm1lZGlhXCIsYSksZSYmKHQrPVwiXFxuLyojIHNvdXJjZVVSTD1cIitlLnNvdXJjZXNbMF0rXCIgKi9cIix0Kz1cIlxcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtiYXNlNjQsXCIrYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoZSkpKSkrXCIgKi9cIikscC5zdHlsZVNoZWV0KXAuc3R5bGVTaGVldC5jc3NUZXh0PXQ7ZWxzZXtmb3IoO3AuZmlyc3RDaGlsZDspcC5yZW1vdmVDaGlsZChwLmZpcnN0Q2hpbGQpO3AuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUodCkpfX12YXIgZD17fSxsPWZ1bmN0aW9uKHApe3ZhciB4O3JldHVybiBmdW5jdGlvbigpe3JldHVyblwidW5kZWZpbmVkXCI9PXR5cGVvZiB4JiYoeD1wLmFwcGx5KHRoaXMsYXJndW1lbnRzKSkseH19LGY9bChmdW5jdGlvbigpe3JldHVybi9tc2llIFs2LTldXFxiLy50ZXN0KHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkpfSksYz1sKGZ1bmN0aW9uKCl7cmV0dXJuIGRvY3VtZW50LmhlYWR8fGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaGVhZFwiKVswXX0pLHU9bnVsbCxoPTAsbT1bXTtwLmV4cG9ydHM9ZnVuY3Rpb24ocCx4KXt4PXh8fHt9LFwidW5kZWZpbmVkXCI9PXR5cGVvZiB4LnNpbmdsZXRvbiYmKHguc2luZ2xldG9uPWYoKSksXCJ1bmRlZmluZWRcIj09dHlwZW9mIHguaW5zZXJ0QXQmJih4Lmluc2VydEF0PVwiYm90dG9tXCIpO3ZhciB0PWUocCk7cmV0dXJuIGEodCx4KSxmdW5jdGlvbihwKXtmb3IodmFyIG89W10sbj0wO248dC5sZW5ndGg7bisrKXt2YXIgaT10W25dLHI9ZFtpLmlkXTtyLnJlZnMtLSxvLnB1c2gocil9aWYocCl7dmFyIHM9ZShwKTthKHMseCl9Zm9yKHZhciBuPTA7bjxvLmxlbmd0aDtuKyspe3ZhciByPW9bbl07aWYoMD09PXIucmVmcyl7Zm9yKHZhciBiPTA7YjxyLnBhcnRzLmxlbmd0aDtiKyspci5wYXJ0c1tiXSgpO2RlbGV0ZSBkW3IuaWRdfX19fTt2YXIgZz1mdW5jdGlvbigpe3ZhciBwPVtdO3JldHVybiBmdW5jdGlvbih4LHQpe3JldHVybiBwW3hdPXQscC5maWx0ZXIoQm9vbGVhbikuam9pbihcIlxcblwiKX19KCl9LGZ1bmN0aW9uKHAseCx0KXt2YXIgYT10KDIpO1wic3RyaW5nXCI9PXR5cGVvZiBhJiYoYT1bW3AuaWQsYSxcIlwiXV0pO3QoNikoYSx7fSk7YS5sb2NhbHMmJihwLmV4cG9ydHM9YS5sb2NhbHMpfV0pfSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1pbmZpbml0ZS1sb2FkaW5nL2Rpc3QvdnVlLWluZmluaXRlLWxvYWRpbmcuanNcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDggMTYiXSwic291cmNlUm9vdCI6IiJ9