/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 436);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 186:
/***/ (function(module, exports, __webpack_require__) {

var data = window.Laravel.data;
var usr = window.Laravel.user;

Vue.component('review', __webpack_require__(348));
Vue.component('product-item', __webpack_require__(36));
Vue.component('related-product-item', __webpack_require__(345));
Vue.component('star-rating', VueStarRating.default);

var current_product = window.Laravel.data.id;
var usrid = null;
var docsVerified = false;
if (usr) {
    usrid = usr.id;
    docsVerified = usr.is_docs_verified;
}
var app = new Vue({
    el: '#product-details-container',
    data: {
        owner: data.user_id,
        site_user: usrid,
        verified: docsVerified,
        colorId: 0,
        colorStock: 0,
        colorDesign: '',
        colorImage: '',
        form: new Form({
            content: '',
            imageTemp: '',
            product_id: current_product,
            rating_id: '',
            rating: 1,
            review: ''
        }, { baseURL: 'http://' + Laravel.base_api_url }, { resetExceptions: ['product_id'] }),
        commentForm: new Form({
            content: '',
            parent_id: 0,
            product_id: current_product,
            rating_id: 0
        }, { baseURL: 'http://' + Laravel.base_api_url }, { resetExceptions: ['product_id'] }),
        user: data.user,
        reviews: [],
        reviewCount: 0,
        items: [],
        cartlang: [],
        varcolor: data.product_color_variations,
        varsize: data.product_size_variations,
        description: data.description,
        chDescription: data.cn_description,
        url: 'http://' + Laravel.base_api_url + '/' + data.image_path_color
    },

    created: function created() {
        var _this = this;

        function getFeedbackTranslation() {
            return axios.get('/translate/feedback');
        }

        function getCartTranslation() {
            return axios.get('/translate/cart');
        }

        function getProductTranslation() {
            return axios.get('/translate/products');
        }

        function getRelatedItems() {
            return axiosAPIv1.post('/product/get-related-products', {
                product: current_product
            });
        }

        function getReviews() {
            return axiosAPIv1.get('/product-reviews?product=' + current_product + '&count=2');
        }

        function getReviewCount() {
            return axiosAPIv1.get('/product-review/count?product=' + current_product);
        }

        function getCart() {
            return axios.get(location.origin + '/cart/view');
        }

        axios.all([getFeedbackTranslation(), getCartTranslation(), getProductTranslation(), getReviews(), getReviewCount(), getCart(), getRelatedItems()]).then(axios.spread(function (feedbacktranslation, carttranslation, producttranslation, reviews, reviewCount, cart, related) {
            _this.lang = feedbacktranslation.data.data;
            _this.cartlang = carttranslation.data.data;
            _this.productlang = producttranslation.data.data;
            _this.reviews = reviews.status == 200 ? reviews.data : [];
            _this.reviewCount = reviewCount.status == 200 ? reviewCount.data : 0;
            _this.items = related.status == 200 ? related.data : [];

            if (_this.varcolor.length != 0) {
                _this.colorStock = _this.varcolor[0].stock;
                _this.colorDesign = _this.varcolor[0].label;
                _this.colorImage = _this.url + _this.varcolor[0].image_name + '.jpg';
            }
        })).catch($.noop);

        Event.listen('Imgfileupload.submitFeedbackImage', function (data) {
            _this.form.imageTemp = data;
        });
    },


    methods: {
        submitFeedback: function submitFeedback() {
            var _this2 = this;

            //this.form.rating_id = this.form.rating == 0 ? '':this.form.rating;
            this.form.rating_id = 1;
            this.form.submit('post', '/v1/product-reviews').then(function (result) {
                if (result.success) {
                    _this2.reviews.unshift(result.data);
                    toastr.success('', _this2.productlang['feedback']);
                    _this2.form.imageTemp = '';
                    //this.form.rating = 3;
                } else {
                    toastr.error(_this2.productlang['please-try']);
                }
            }).catch($.noop);
        },


        handleCmdEnter: function handleCmdEnter(e) {
            if ((e.metaKey || e.ctrlKey) && e.keyCode == 13) {
                this.submitFeedback();
            }
        },

        submitComment: function submitComment() {
            var _this3 = this;

            var index;
            for (var i = 0; i < this.reviews.length; i++) {
                if (this.reviews[i].id == this.commentForm.parent_id) {
                    index = i;
                    break;
                }
            }
            this.commentForm.rating_id = 0;

            this.commentForm.submit('post', '/v1/product-reviews').then(function (result) {
                if (result.success) {
                    _this3.reviews[index].comments.unshift(result.data);
                    toastr.success('', _this3.productlang['comment']);
                    $('#modalComments').modal('toggle');
                } else {
                    toastr.error(_this3.productlang['please-try']);
                }
            }).catch($.noop);
        },
        loadMoreReviews: function loadMoreReviews() {
            var _this4 = this;

            function getReviews(review_count) {
                return axiosAPIv1.get('/product-reviews?product=' + current_product + '&count=2&skip=' + review_count);
            }

            axios.all([getReviews(this.reviews.length)]).then(axios.spread(function (reviews) {
                if (reviews.status == 200) {
                    _this4.reviews = _this4.reviews.concat(reviews.data);
                }
            })).catch($.noop);
        },
        chooseColor: function chooseColor(varco) {
            $("#qty").val('1');
            this.colorId = varco.id;
            this.colorStock = varco.stock;
            this.colorDesign = varco.label;
            this.colorImage = this.url + varco.image_name + '.jpg';
        },
        AddToWishlist: function AddToWishlist(id) {
            var _this5 = this;

            if (Laravel.isAuthenticated === true) {
                axiosAPIv1.get('/addtofavorites/' + id).then(function (result) {
                    if (result.data.response == 1) {
                        var fav = parseInt($("#fav-count").text()) + 1;
                        $("#fav-count").text(fav).show();
                        toastr.success(_this5.cartlang['added-favorites'], name);
                    } else if (result.data.response == 2) {
                        toastr.error(result.data.error_msg, _this5.cartlang['not-added-wishlist']);
                    } else {
                        toastr.info(_this5.cartlang['already-favorites'], name);
                    }
                });
            } else {
                toastr.error(this.cartlang['login-favorites']);
            }
        },
        addToCart2: function addToCart2(id) {
            var _this6 = this;

            var qty = $('#qty').val();
            var selcolor = this.colorDesign;
            var size = $("#gSize option:selected").val();
            var selsize = $("#gSize option:selected").text();

            if (this.varcolor.length == 0 && this.varsize.length == 0) {

                axios.get(location.origin + '/cart/add2', {
                    params: {
                        prod_id: id,
                        qty: qty
                    }
                }).then(function (result) {
                    $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                    if (result.data.error_msg) toastr.error(result.data.error_msg, _this6.cartlang['not-added']);else toastr.success(_this6.cartlang['added-cart']);
                });

                setTimeout(function () {
                    axios.get(location.origin + '/cart/view').then(function (result) {
                        $('#cart-count, #cart-count-mobile').text(result.data.cart_item_count);
                    });
                }, 1000);
            } else {

                if (size == 'size') {
                    toastr.error(this.cartlang['select-size'], this.cartlang['not-added']);
                } else {

                    axios.get(location.origin + '/cart/add2', {
                        params: {
                            prod_id: id,
                            qty: qty,
                            color: selcolor,
                            colorId: this.colorId,
                            colorStock: this.colorStock,
                            colorImage: this.colorImage,
                            size: selsize
                        }
                    }).then(function (result) {
                        $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                        if (result.data.error_msg) toastr.error(result.data.error_msg, _this6.cartlang['not-added']);else toastr.success(_this6.cartlang['added-cart']);
                    });

                    setTimeout(function () {
                        axios.get(location.origin + '/cart/view').then(function (result) {
                            $('#cart-count, #cart-count-mobile').text(result.data.cart_item_count);
                        });
                    }, 1000);
                }
            }
        },
        buyNow: function buyNow(id) {
            var _this7 = this;

            var qty = $('#qty').val();
            var selcolor = this.colorDesign;
            var size = $("#gSize option:selected").val();
            var selsize = $("#gSize option:selected").text();

            axios.get(location.origin + '/cart/view').then(function (result) {
                var cart2 = $.map(result.data.cart, function (value, index) {
                    return [value];
                });

                var cart3 = cart2.find(function (item) {
                    return item.id == current_product;
                });

                if (_this7.varcolor.length == 0 && _this7.varsize.length == 0) {
                    if (cart3 == undefined) {
                        axios.get(location.origin + '/cart/add2', {
                            params: {
                                prod_id: id,
                                qty: qty

                            }
                        }).then(function (result) {
                            if (result.data.error_msg) {
                                toastr.error(result.data.error_msg, _this7.cartlang['cant-bought']);
                            } else {
                                window.location.assign("/cart");
                            }
                        });
                    } else {
                        axios.get(location.origin + '/cart/update', {
                            params: {
                                prod_id: id,
                                row_id: cart3.rowId,
                                qty: qty
                            }
                        });

                        setTimeout(function () {
                            window.location.assign("/cart");
                        }, 1000);
                    }
                } else {

                    if (size == 'size') {
                        toastr.error(_this7.cartlang['select-size'], _this7.cartlang['not-added']);
                    } else {
                        if (cart3 == undefined) {
                            axios.get(location.origin + '/cart/add2', {
                                params: {
                                    prod_id: id,
                                    qty: qty,
                                    color: selcolor,
                                    colorId: _this7.colorId,
                                    colorStock: _this7.colorStock,
                                    colorImage: _this7.colorImage,
                                    size: selsize
                                }
                            }).then(function (result) {
                                if (result.data.error_msg) {
                                    toastr.error(result.data.error_msg, _this7.cartlang['cant-bought']);
                                } else {
                                    window.location.assign("/cart");
                                }
                            });
                        } else {

                            var arr = $.grep(cart2, function (a) {
                                return a.id == current_product;
                            });

                            var arr2 = arr.find(function (item) {
                                return item.options.size == selsize;
                            });

                            if (selcolor != 'color' && size != 'size') var arr2 = arr.find(function (item) {
                                return item.options.color == selcolor && item.options.size == selsize;
                            });else if (selcolor != 'color') var arr2 = arr.find(function (item) {
                                return item.options.color == selcolor;
                            });else if (size != 'size') var arr2 = arr.find(function (item) {
                                return item.options.size == selsize;
                            });

                            if (arr2) {
                                axios.get(location.origin + '/cart/update', {
                                    params: {
                                        prod_id: id,
                                        row_id: arr2.rowId,
                                        qty: qty,
                                        color: selcolor,
                                        colorId: _this7.colorId,
                                        colorStock: _this7.colorStock,
                                        colorImage: _this7.colorImage,
                                        size: selsize
                                    }
                                });

                                setTimeout(function () {
                                    window.location.assign("/cart");
                                }, 1000);
                            } else {
                                axios.get(location.origin + '/cart/add2', {
                                    params: {
                                        prod_id: id,
                                        qty: qty,
                                        color: selcolor,
                                        colorId: _this7.colorId,
                                        colorStock: _this7.colorStock,
                                        colorImage: _this7.colorImage,
                                        size: selsize
                                    }
                                }).then(function (result) {
                                    $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                                    if (result.data.error_msg) toastr.error(result.data.error_msg, _this7.cartlang['not-added']);else {
                                        setTimeout(function () {
                                            window.location.assign("/cart");
                                        }, 1000);
                                    }
                                });
                            }
                        }
                    }
                }
            });
        },
        addToCart: function addToCart(id, name) {
            var _this8 = this;

            axios.get(location.origin + '/cart/add', {
                params: {
                    prod_id: id,
                    qty: 1
                }
            }).then(function (result) {
                $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                if (result.data.error_msg) {
                    if (result.data.error_msg == 'Redirect') window.location = '/product/' + result.data.slug;else toastr.error(result.data.error_msg, _this8.cartlang['not-added']);
                } else {
                    toastr.success(_this8.cartlang['added-cart'], name);
                }
            });
        },
        addToCompare: function addToCompare(id, name) {
            var _this9 = this;

            if (Laravel.isAuthenticated === true) {
                axiosAPIv1.get('/addtocompare/' + id).then(function (result) {
                    if (result.data.response == 1) {
                        var comp = parseInt($("#comp-count").text()) + 1;
                        $("#comp-count").text(comp).show();
                        toastr.success(_this9.cartlang['added-comparison'], name);
                    } else if (result.data.response == 3) {
                        toastr.error(_this9.cartlang['full']);
                    } else {
                        toastr.info(_this9.cartlang['already-compare'], name);
                    }
                });
            } else {
                toastr.error(this.cartlang['login-compare']);
            }
        },
        addToFavorites: function addToFavorites(id, name) {
            var _this10 = this;

            if (Laravel.isAuthenticated === true) {
                axiosAPIv1.get('/addtofavorites/' + id).then(function (result) {
                    if (result.data.response == 1) {
                        var fav = parseInt($("#fav-count").text()) + 1;
                        $("#fav-count").text(fav).show();
                        toastr.success(_this10.cartlang['added-favorites'], name);
                    } else {
                        toastr.info(_this10.cartlang['already-favorites'], name);
                    }
                });
            } else {
                toastr.error(this.cartlang['login-favorites']);
            }
        },
        contactMe: function contactMe() {

            var chat = [];
            $(".panel-chat-box").each(function () {
                chat.push($(this).attr('id'));
            });

            if ($.inArray('user' + this.user.id, chat) == '-1') Event.fire('chat-box.show', this.user);

            setTimeout(function () {
                $('.js-auto-size').textareaAutoSize();
            }, 5000);
        },
        notVerified: function notVerified() {
            toastr.error(this.productlang['product-owner']);
        },
        showReportModal: function showReportModal() {
            $('#modalProdReport').modal('toggle');
        },
        cantReport: function cantReport() {
            toastr.error(this.productlang['cant-report']);
        },
        needLogin: function needLogin() {
            toastr.error(this.productlang['login-first']);
        }
    },
    computed: {
        storename: function storename() {
            var name = window.Laravel.data.store.cn_name ? window.Laravel.data.store.cn_name : window.Laravel.data.store.name;
            var m = $("meta[name=locale-lang]");
            var langmode = m.attr("content");

            if (langmode == "cn") return name;else return window.Laravel.data.store.name;
        }
    }
});

$(function () {
    // owlcarosel (if items less than 4, hide nav, disable drag, hide touch)
    var products_slider_detail = $('.products-slider-detail');
    var item_count = $('.products-slider-detail a').length;
    products_slider_detail.owlCarousel({
        margin: 5,
        dots: false,
        nav: item_count < 5 ? false : true,
        mouseDrag: item_count < 5 ? false : true,
        touchDrag: item_count < 5 ? false : true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        responsive: {
            0: { items: 4 }
        }
    });
    $('.products-slider-detail a').click(function () {
        var src = $(this).find('img').attr('src');
        var zoom = $(this).find('img').attr('data-zoom-image');
        var detail = $(this).parent().parent().parent().parent().parent().find('.image-detail img');
        detail.attr('src', src);
        detail.attr('data-zoom-image', zoom);
        $('.zoomWindow').css('background-image', 'url("' + zoom + '")');
        return false;
    });

    $('#qty-mar .form-group').css("margin", "0");
});

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['item'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    },
    addToCart: function addToCart() {
      this.$parent.addToCart(this.item.id, this.item.name);
    },
    addToCompare: function addToCompare() {
      this.$parent.addToCompare(this.item.id, this.item.name);
    },
    addToFavorites: function addToFavorites() {
      this.$parent.addToFavorites(this.item.id, this.item.name);
    }
  },
  computed: {
    price: function price() {
      if (this.item.fee_included == 1) return parseFloat(this.item.price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.price;
    },
    saleprice: function saleprice() {
      if (this.item.fee_included == 1) return parseFloat(this.item.sale_price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.sale_price;
    },
    namelength: function namelength() {
      var name = this.item.cn_name ? this.item.cn_name : this.item.name;
      if (this.item.name.length > 45) {
        if (this.$parent.langmode == "cn") {
          return name.substr(0, 45) + "...";
        }
        return this.item.name.substr(0, 45) + "...";
      } else {
        if (this.$parent.langmode == "cn") {
          return name;
        }
        return this.item.name;
      }
    },
    starRating: function starRating() {
      return "width:" + Math.round(this.item.rate / this.item.count * 20) + "%";
    },
    productRating: function productRating() {
      return "width:" + this.item.product_rating + "%";
    }
  }
});

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['item'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    },
    addToCart: function addToCart() {
      this.$parent.addToCart(this.item.id, this.item.name);
    },
    addToCompare: function addToCompare() {
      this.$parent.addToCompare(this.item.id, this.item.name);
    },
    addToFavorites: function addToFavorites() {
      this.$parent.addToFavorites(this.item.id, this.item.name);
    }
  },
  computed: {
    price: function price() {
      if (this.item.fee_included == 1) return parseFloat(this.item.price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.price;
    },
    saleprice: function saleprice() {
      if (this.item.fee_included == 1) return parseFloat(this.item.sale_price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.sale_price;
    },
    namelength: function namelength() {
      var name = this.item.cn_name ? this.item.cn_name : this.item.name;
      if (this.item.name.length > 45) {
        if (this.$parent.langmode == "cn") {
          return name.substr(0, 45) + "...";
        }
        return this.item.name.substr(0, 45) + "...";
      } else {
        if (this.$parent.langmode == "cn") {
          return name;
        }
        return this.item.name;
      }
    },
    starRating: function starRating() {
      return "width:" + Math.round(this.item.rate / this.item.count * 20) + "%";
    },
    productRating: function productRating() {
      return "width:" + this.item.product_rating + "%";
    }
  }
});

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['comment']
});

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['feedback', 'likes'],
	data: function data() {
		return {
			user_id: Laravel.user.id,
			feedbacklike_id: false,
			liked: false,
			message: "Like"
		};
	},
	watch: {
		message: function message(newMsg) {
			$('#btnLike' + this.feedback).tooltip('hide').attr('data-original-title', newMsg).tooltip('fixTitle');
		}
	},
	created: function created() {
		var _this = this;

		var lang = this.$parent.$parent.lang;
		axiosAPIv1.get('/product-review-likes?user=' + this.user_id + '&product=' + this.feedback).then(function (res) {
			if (res.data.length) {
				_this.feedbacklike_id = res.data[0].id;
				_this.liked = true;
				if (_this.liked) {
					if (_this.likes == "1") _this.message = lang['you-liked-this'];else {
						var like = _this.likes - 1;
						_this.likes = like;
						_this.message = lang['you-and'] + " " + like + " " + lang['others-liked-this'];
					}
				} else {
					if (_this.likes == "0") _this.message = lang["like"];else {
						var like = _this.likes - 1;
						_this.likes = like;
						_this.message = like + " " + lang['others-liked-this'];
					}
				}
			} else {
				$('[data-toggle="tooltip"]').tooltip();
			}
		});
	},

	methods: {
		likeFeedback: function likeFeedback() {
			var _this2 = this;

			var lang = this.$parent.$parent.lang;
			if (this.liked) {
				//update or search
				axiosAPIv1.get('/product-review-likes?user=' + this.user_id + '&product=' + this.feedback).then(function (res) {
					if (res.data.length) {
						_this2.feedbacklike_id = res.data[0].id;
						_this2.liked = true;
						axiosAPIv1.delete('/product-review-likes/' + _this2.feedbacklike_id).then(function (res) {
							_this2.liked = false;
							toastr.success('', lang['unliked']);
							if (_this2.likes == "0") _this2.message = lang["like"];else _this2.message = _this2.likes + " " + lang['others-liked-this'];
						});
					}
				});
			} else {
				axiosAPIv1.post('/product-review-likes', {
					user_id: this.user_id,
					review_id: this.feedback
				}).then(function (res) {
					_this2.liked = true;
					toastr.success('', lang['liked']);
					if (_this2.likes == "0") _this2.message = lang['you-liked-this'];else _this2.message = lang['you-and'] + " " + _this2.likes + " " + lang['others-liked-this'];
				});
			}
		}
	}
});

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

Vue.component('like', __webpack_require__(347));
Vue.component('comment', __webpack_require__(346));

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['feedback'],
    data: function data() {
        return {
            commentsShown: 2
        };
    },
    methods: {
        openCommentsModal: function openCommentsModal(id) {
            if (this.$parent.owner == this.$parent.site_user) {
                if (this.$parent.verified == true) {
                    $('#modalComments').modal('toggle');
                } else {
                    toastr.error('Your account need to be verified first for you to post a reply to any product review.!');
                }
            } else {
                $('#modalComments').modal('toggle');
            }
            this.$parent.commentForm.parent_id = id;
        },
        viewMore: function viewMore() {
            this.commentsShown += this.feedback.comments.length - this.commentsShown > 2 ? 2 : this.feedback.comments.length - this.commentsShown;
        },
        viewLess: function viewLess() {
            if (this.commentsShown <= 2) return;
            this.commentsShown -= 2;
        }
    }

});

/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(280),
  /* template */
  __webpack_require__(381),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\product\\Related-Product.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Related-Product.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-43ec3731", Component.options)
  } else {
    hotAPI.reload("data-v-43ec3731", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(281),
  /* template */
  __webpack_require__(390),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\product\\review\\Comment.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Comment.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-64ff0d94", Component.options)
  } else {
    hotAPI.reload("data-v-64ff0d94", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(282),
  /* template */
  __webpack_require__(364),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\product\\review\\Like.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Like.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1458709c", Component.options)
  } else {
    hotAPI.reload("data-v-1458709c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 348:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(283),
  /* template */
  __webpack_require__(391),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\product\\review\\review.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] review.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-69e43613", Component.options)
  } else {
    hotAPI.reload("data-v-69e43613", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 36:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(20),
  /* template */
  __webpack_require__(42),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\product\\Product.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Product.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-778f75f3", Component.options)
  } else {
    hotAPI.reload("data-v-778f75f3", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 364:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('a', {
    staticClass: "btn btn-has-loading btn-like btn-fab btn-fab-mini btn-like ",
    class: _vm.liked ? "liked" : "not-liked",
    attrs: {
      "href": "#" + _vm.feedback,
      "data-toggle": "tooltip",
      "data-placement": "bottom",
      "data-title": _vm.message,
      "id": "btnLike" + _vm.feedback
    },
    on: {
      "click": _vm.likeFeedback
    }
  }, [_c('span', {
    staticClass: "fa fa-thumbs-up"
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-1458709c", module.exports)
  }
}

/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mix col-sm-3 col-xs-6"
  }, [_c('div', {
    staticClass: "box-product-outer"
  }, [_c('div', {
    staticClass: "box-product"
  }, [_c('div', {
    staticClass: "img-wrapper"
  }, [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_c('img', {
    attrs: {
      "alt": "Product",
      "src": _vm.item.image,
      "onError": "this.src = '/images/avatar/default-product.jpg'"
    }
  })]), _vm._v(" "), (_vm.item.sale_price) ? _c('div', {
    staticClass: "tags"
  }, [_c('span', {
    staticClass: "label-tags"
  }, [_c('span', {
    staticClass: "label label-danger arrowed"
  }, [_vm._v(_vm._s(_vm.item.discount) + "%")])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "option"
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "data-placement": "top",
      "title": "Add to Cart",
      "data-original-title": "Add to Cart"
    },
    on: {
      "click": function($event) {
        _vm.addToCart()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-shopping-cart"
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Compare"
    }
  }, [_c('i', {
    staticClass: "fa fa-align-left",
    on: {
      "click": function($event) {
        _vm.addToCompare()
      }
    }
  })]), _vm._v(" "), _c('a', {
    staticClass: "wishlist",
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Wishlist"
    },
    on: {
      "click": function($event) {
        _vm.addToFavorites()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-heart"
  })])])]), _vm._v(" "), _c('h6', [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_vm._v(_vm._s(_vm.namelength))])]), _vm._v(" "), _c('div', {
    staticClass: "price"
  }, [_c('div', [(_vm.item.sale_price) ? _c('span', [_vm._v(_vm._s(_vm._f("currency")(parseFloat("0" + _vm.saleprice))))]) : _vm._e(), _vm._v(" "), _c('span', {
    class: _vm.item.sale_price ? 'price-old' : ''
  }, [_vm._v(_vm._s(_vm._f("currency")(parseFloat("0" + _vm.price))))])])]), _vm._v(" "), _c('div', {
    staticClass: "rating-block"
  }, [_c('div', {
    staticClass: "star-ratings-sprite-small"
  }, [_c('span', {
    staticClass: "star-ratings-sprite-small-rating",
    style: (_vm.productRating)
  })])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-43ec3731", module.exports)
  }
}

/***/ }),

/***/ 390:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-lg-12 user-feedback"
  }, [_c('div', {
    staticClass: "col-lg-1 text-center"
  }, [(_vm.comment.user.avatar) ? _c('img', {
    staticClass: "img img-circle fb-user-image",
    attrs: {
      "src": _vm.comment.user.avatar
    }
  }) : _c('div', {
    staticClass: "no-avatar"
  }, [_vm._v("\n                " + _vm._s(_vm.comment.user.initials) + "\n            ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-11"
  }, [_c('span', {
    staticClass: "fb-name  popover-details",
    attrs: {
      "data-id": _vm.comment.user.id
    }
  }, [_vm._v("\n\t            " + _vm._s(_vm.comment.user.full_name) + "\n\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "datetime"
  }, [_vm._v("\n\t            " + _vm._s(_vm.comment.created_at) + "\n\t        ")]), _vm._v(" "), _c('div', {
    staticClass: "fb-content"
  }, [_vm._v("\n\t            " + _vm._s(_vm.comment.content) + "\n\t        ")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-64ff0d94", module.exports)
  }
}

/***/ }),

/***/ 391:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('div', {
    staticClass: "row user-feedback"
  }, [(!_vm.feedback.comments.length) ? _c('div', {
    staticClass: "abs to-partial-right to-full-bottom"
  }, [_c('button', {
    staticClass: "btn btn-fab btn-ripple btn-info",
    attrs: {
      "id": "btnAddCommentModal"
    },
    on: {
      "click": function($event) {
        _vm.openCommentsModal(_vm.feedback.id)
      }
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("add")])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "col-lg-1 col-md-2 col-sm-2 col-xs-3 text-center  "
  }, [(_vm.feedback.user.avatar) ? _c('img', {
    staticClass: "img img-circle fb-user-image",
    attrs: {
      "src": _vm.feedback.user.avatar
    }
  }) : _c('div', {
    staticClass: "no-avatar"
  }, [_vm._v("\n                        " + _vm._s(_vm.feedback.user.initials) + "\n                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-11  col-md-10 col-sm-10 col-xs-9"
  }, [_c('div', {
    staticClass: "fb-holder"
  }, [(_vm.feedback.comments.length) ? _c('div', {
    staticClass: "abs to-right to-full-bottom"
  }, [_c('button', {
    staticClass: "btn btn-fab btn-ripple btn-info",
    attrs: {
      "id": "btnAddCommentModal"
    },
    on: {
      "click": function($event) {
        _vm.openCommentsModal(_vm.feedback.id)
      }
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("add")])])]) : _vm._e(), _vm._v(" "), _c('span', {
    staticClass: "fb-name popover-details",
    attrs: {
      "data-id": _vm.feedback.user.id
    }
  }, [_vm._v("\n                            " + _vm._s(_vm.feedback.user.full_name) + " \n                        ")]), _vm._v(" "), _c('div', {
    staticClass: "datetime"
  }, [_vm._v("\n                            " + _vm._s(_vm._f("friendly-date")(_vm.feedback.created_at)) + " This is " + _vm._s(_vm.feedback.review) + " product.\n                    "), _c('like', {
    attrs: {
      "feedback": _vm.feedback.id,
      "likes": _vm.feedback.likes_count
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "fb-content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [(_vm.feedback.image) ? _c('div', {
    staticClass: "col-lg-2 col-md-2 col-sm-2 col-xs-12"
  }, [_c('img', {
    staticClass: "img-responsive",
    attrs: {
      "src": '/' + _vm.feedback.image
    }
  })]) : _vm._e(), _vm._v(" "), _c('div', {
    class: _vm.feedback.image ? 'col-lg-10 col-md-10 col-sm-10 col-xs-12' : 'col-lg-12'
  }, [_vm._v("\n                                    " + _vm._s(_vm.feedback.content) + "\n                                ")])])])]), _vm._v(" "), _c('div', {
    staticClass: "fb-comments-holder"
  }, [_vm._l((_vm.feedback.comments), function(comment, $index) {
    return ($index < _vm.commentsShown) ? _c('comment', {
      key: comment.id,
      attrs: {
        "comment": comment
      }
    }) : _vm._e()
  }), _vm._v(" "), ((_vm.feedback.comments.length > _vm.commentsShown) || (_vm.commentsShown > 2)) ? _c('div', {
    staticClass: "col-lg-12 user-feedback text-right"
  }, [(_vm.feedback.comments.length > _vm.commentsShown) ? _c('button', {
    staticClass: "btn btn-info btn-raised btn-has-loading",
    on: {
      "click": _vm.viewMore
    }
  }, [_vm._v("View More")]) : _vm._e(), _vm._v(" "), (_vm.commentsShown > 2) ? _c('button', {
    staticClass: "btn btn-info btn-raised btn-has-loading",
    on: {
      "click": _vm.viewLess
    }
  }, [_vm._v("View Less")]) : _vm._e()]) : _vm._e()], 2)])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-69e43613", module.exports)
  }
}

/***/ }),

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mix col-sm-3"
  }, [_c('div', {
    staticClass: "box-product-outer"
  }, [_c('div', {
    staticClass: "box-product"
  }, [_c('div', {
    staticClass: "img-wrapper"
  }, [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_c('img', {
    attrs: {
      "alt": "Product",
      "src": '/shopping/images/product/' + _vm.item.id + '/primary.jpg',
      "onError": "this.src = '/images/avatar/default-product.jpg'"
    }
  })]), _vm._v(" "), (_vm.item.sale_price) ? _c('div', {
    staticClass: "tags"
  }, [_c('span', {
    staticClass: "label-tags"
  }, [_c('span', {
    staticClass: "label label-danger arrowed"
  }, [_vm._v(_vm._s(_vm.item.discount) + "%")])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "option"
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "data-placement": "top",
      "title": "Add to Cart",
      "data-original-title": "Add to Cart"
    },
    on: {
      "click": function($event) {
        _vm.addToCart()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-shopping-cart"
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Compare"
    }
  }, [_c('i', {
    staticClass: "fa fa-align-left",
    on: {
      "click": function($event) {
        _vm.addToCompare()
      }
    }
  })]), _vm._v(" "), _c('a', {
    staticClass: "wishlist",
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Wishlist"
    },
    on: {
      "click": function($event) {
        _vm.addToFavorites()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-heart"
  })])])]), _vm._v(" "), _c('h6', [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_vm._v(_vm._s(_vm.namelength))])]), _vm._v(" "), _c('div', {
    staticClass: "price"
  }, [_c('div', [(_vm.item.sale_price) ? _c('span', [_vm._v(_vm._s(_vm._f("currency")(parseFloat("0" + _vm.saleprice))))]) : _vm._e(), _vm._v(" "), _c('span', {
    class: _vm.item.sale_price ? 'price-old' : ''
  }, [_vm._v(_vm._s(_vm._f("currency")(parseFloat("0" + _vm.price))))])])]), _vm._v(" "), _c('div', {
    staticClass: "rating-block"
  }, [_c('div', {
    staticClass: "star-ratings-sprite-small"
  }, [_c('span', {
    staticClass: "star-ratings-sprite-small-rating",
    style: (_vm.productRating)
  })])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-778f75f3", module.exports)
  }
}

/***/ }),

/***/ 436:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(186);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9wcm9kdWN0LWRldGFpbHMuanMiLCJ3ZWJwYWNrOi8vL1Byb2R1Y3QudnVlIiwid2VicGFjazovLy9SZWxhdGVkLVByb2R1Y3QudnVlIiwid2VicGFjazovLy9Db21tZW50LnZ1ZSIsIndlYnBhY2s6Ly8vTGlrZS52dWUiLCJ3ZWJwYWNrOi8vL3Jldmlldy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3Byb2R1Y3QvUmVsYXRlZC1Qcm9kdWN0LnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9yZXZpZXcvQ29tbWVudC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3Byb2R1Y3QvcmV2aWV3L0xpa2UudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L3Jldmlldy9yZXZpZXcudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L1Byb2R1Y3QudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L3Jldmlldy9MaWtlLnZ1ZT8xMTNjIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L1JlbGF0ZWQtUHJvZHVjdC52dWU/MWFhYSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9yZXZpZXcvQ29tbWVudC52dWU/NjU5MyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9yZXZpZXcvcmV2aWV3LnZ1ZT9hN2I5Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L1Byb2R1Y3QudnVlP2UxNzUiXSwibmFtZXMiOlsiZGF0YSIsIndpbmRvdyIsIkxhcmF2ZWwiLCJ1c3IiLCJ1c2VyIiwiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsIlZ1ZVN0YXJSYXRpbmciLCJkZWZhdWx0IiwiY3VycmVudF9wcm9kdWN0IiwiaWQiLCJ1c3JpZCIsImRvY3NWZXJpZmllZCIsImlzX2RvY3NfdmVyaWZpZWQiLCJhcHAiLCJlbCIsIm93bmVyIiwidXNlcl9pZCIsInNpdGVfdXNlciIsInZlcmlmaWVkIiwiY29sb3JJZCIsImNvbG9yU3RvY2siLCJjb2xvckRlc2lnbiIsImNvbG9ySW1hZ2UiLCJmb3JtIiwiRm9ybSIsImNvbnRlbnQiLCJpbWFnZVRlbXAiLCJwcm9kdWN0X2lkIiwicmF0aW5nX2lkIiwicmF0aW5nIiwicmV2aWV3IiwiYmFzZVVSTCIsImJhc2VfYXBpX3VybCIsInJlc2V0RXhjZXB0aW9ucyIsImNvbW1lbnRGb3JtIiwicGFyZW50X2lkIiwicmV2aWV3cyIsInJldmlld0NvdW50IiwiaXRlbXMiLCJjYXJ0bGFuZyIsInZhcmNvbG9yIiwicHJvZHVjdF9jb2xvcl92YXJpYXRpb25zIiwidmFyc2l6ZSIsInByb2R1Y3Rfc2l6ZV92YXJpYXRpb25zIiwiZGVzY3JpcHRpb24iLCJjaERlc2NyaXB0aW9uIiwiY25fZGVzY3JpcHRpb24iLCJ1cmwiLCJpbWFnZV9wYXRoX2NvbG9yIiwiY3JlYXRlZCIsImdldEZlZWRiYWNrVHJhbnNsYXRpb24iLCJheGlvcyIsImdldCIsImdldENhcnRUcmFuc2xhdGlvbiIsImdldFByb2R1Y3RUcmFuc2xhdGlvbiIsImdldFJlbGF0ZWRJdGVtcyIsImF4aW9zQVBJdjEiLCJwb3N0IiwicHJvZHVjdCIsImdldFJldmlld3MiLCJnZXRSZXZpZXdDb3VudCIsImdldENhcnQiLCJsb2NhdGlvbiIsIm9yaWdpbiIsImFsbCIsInRoZW4iLCJzcHJlYWQiLCJmZWVkYmFja3RyYW5zbGF0aW9uIiwiY2FydHRyYW5zbGF0aW9uIiwicHJvZHVjdHRyYW5zbGF0aW9uIiwiY2FydCIsInJlbGF0ZWQiLCJsYW5nIiwicHJvZHVjdGxhbmciLCJzdGF0dXMiLCJsZW5ndGgiLCJzdG9jayIsImxhYmVsIiwiaW1hZ2VfbmFtZSIsImNhdGNoIiwiJCIsIm5vb3AiLCJFdmVudCIsImxpc3RlbiIsIm1ldGhvZHMiLCJzdWJtaXRGZWVkYmFjayIsInN1Ym1pdCIsInJlc3VsdCIsInN1Y2Nlc3MiLCJ1bnNoaWZ0IiwidG9hc3RyIiwiZXJyb3IiLCJoYW5kbGVDbWRFbnRlciIsImUiLCJtZXRhS2V5IiwiY3RybEtleSIsImtleUNvZGUiLCJzdWJtaXRDb21tZW50IiwiaW5kZXgiLCJpIiwiY29tbWVudHMiLCJtb2RhbCIsImxvYWRNb3JlUmV2aWV3cyIsInJldmlld19jb3VudCIsImNvbmNhdCIsImNob29zZUNvbG9yIiwidmFyY28iLCJ2YWwiLCJBZGRUb1dpc2hsaXN0IiwiaXNBdXRoZW50aWNhdGVkIiwicmVzcG9uc2UiLCJmYXYiLCJwYXJzZUludCIsInRleHQiLCJzaG93IiwibmFtZSIsImVycm9yX21zZyIsImluZm8iLCJhZGRUb0NhcnQyIiwicXR5Iiwic2VsY29sb3IiLCJzaXplIiwic2Vsc2l6ZSIsInBhcmFtcyIsInByb2RfaWQiLCJjYXJ0X2l0ZW1fY291bnQiLCJzZXRUaW1lb3V0IiwiY29sb3IiLCJidXlOb3ciLCJjYXJ0MiIsIm1hcCIsInZhbHVlIiwiY2FydDMiLCJmaW5kIiwiaXRlbSIsInVuZGVmaW5lZCIsImFzc2lnbiIsInJvd19pZCIsInJvd0lkIiwiYXJyIiwiZ3JlcCIsImEiLCJhcnIyIiwib3B0aW9ucyIsImFkZFRvQ2FydCIsInNsdWciLCJhZGRUb0NvbXBhcmUiLCJjb21wIiwiYWRkVG9GYXZvcml0ZXMiLCJjb250YWN0TWUiLCJjaGF0IiwiZWFjaCIsInB1c2giLCJhdHRyIiwiaW5BcnJheSIsImZpcmUiLCJ0ZXh0YXJlYUF1dG9TaXplIiwibm90VmVyaWZpZWQiLCJzaG93UmVwb3J0TW9kYWwiLCJjYW50UmVwb3J0IiwibmVlZExvZ2luIiwiY29tcHV0ZWQiLCJzdG9yZW5hbWUiLCJzdG9yZSIsImNuX25hbWUiLCJtIiwibGFuZ21vZGUiLCJwcm9kdWN0c19zbGlkZXJfZGV0YWlsIiwiaXRlbV9jb3VudCIsIm93bENhcm91c2VsIiwibWFyZ2luIiwiZG90cyIsIm5hdiIsIm1vdXNlRHJhZyIsInRvdWNoRHJhZyIsIm5hdlRleHQiLCJyZXNwb25zaXZlIiwiY2xpY2siLCJzcmMiLCJ6b29tIiwiZGV0YWlsIiwicGFyZW50IiwiY3NzIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNsREEsSUFBSUEsT0FBT0MsT0FBT0MsT0FBUCxDQUFlRixJQUExQjtBQUNBLElBQUlHLE1BQU9GLE9BQU9DLE9BQVAsQ0FBZUUsSUFBMUI7O0FBRUFDLElBQUlDLFNBQUosQ0FBYyxRQUFkLEVBQXlCLG1CQUFBQyxDQUFRLEdBQVIsQ0FBekI7QUFDQUYsSUFBSUMsU0FBSixDQUFjLGNBQWQsRUFBOEIsbUJBQUFDLENBQVEsRUFBUixDQUE5QjtBQUNBRixJQUFJQyxTQUFKLENBQWMsc0JBQWQsRUFBc0MsbUJBQUFDLENBQVEsR0FBUixDQUF0QztBQUNBRixJQUFJQyxTQUFKLENBQWMsYUFBZCxFQUE2QkUsY0FBY0MsT0FBM0M7O0FBRUEsSUFBSUMsa0JBQWtCVCxPQUFPQyxPQUFQLENBQWVGLElBQWYsQ0FBb0JXLEVBQTFDO0FBQ0EsSUFBSUMsUUFBUSxJQUFaO0FBQ0EsSUFBSUMsZUFBZSxLQUFuQjtBQUNBLElBQUdWLEdBQUgsRUFBTztBQUNIUyxZQUFRVCxJQUFJUSxFQUFaO0FBQ0FFLG1CQUFlVixJQUFJVyxnQkFBbkI7QUFDSDtBQUNELElBQUlDLE1BQU0sSUFBSVYsR0FBSixDQUFRO0FBQ2RXLFFBQUksNEJBRFU7QUFFZGhCLFVBQU07QUFDRmlCLGVBQU9qQixLQUFLa0IsT0FEVjtBQUVGQyxtQkFBV1AsS0FGVDtBQUdGUSxrQkFBVVAsWUFIUjtBQUlGUSxpQkFBUSxDQUpOO0FBS0ZDLG9CQUFZLENBTFY7QUFNRkMscUJBQWEsRUFOWDtBQU9GQyxvQkFBWSxFQVBWO0FBUUZDLGNBQU0sSUFBSUMsSUFBSixDQUNGO0FBQ0tDLHFCQUFRLEVBRGI7QUFFS0MsdUJBQVUsRUFGZjtBQUdLQyx3QkFBV25CLGVBSGhCO0FBSUtvQix1QkFBVSxFQUpmO0FBS0tDLG9CQUFPLENBTFo7QUFNS0Msb0JBQU87QUFOWixTQURFLEVBU0QsRUFBRUMsU0FBUyxZQUFVL0IsUUFBUWdDLFlBQTdCLEVBVEMsRUFVRCxFQUFDQyxpQkFBZ0IsQ0FBQyxZQUFELENBQWpCLEVBVkMsQ0FSSjtBQW9CREMscUJBQWEsSUFBSVYsSUFBSixDQUNWO0FBQ0NDLHFCQUFRLEVBRFQ7QUFFQ1UsdUJBQVUsQ0FGWDtBQUdDUix3QkFBV25CLGVBSFo7QUFJQ29CLHVCQUFVO0FBSlgsU0FEVSxFQU9ULEVBQUVHLFNBQVMsWUFBVS9CLFFBQVFnQyxZQUE3QixFQVBTLEVBUVQsRUFBQ0MsaUJBQWdCLENBQUMsWUFBRCxDQUFqQixFQVJTLENBcEJaO0FBOEJEL0IsY0FBTUosS0FBS0ksSUE5QlY7QUErQkRrQyxpQkFBUSxFQS9CUDtBQWdDREMscUJBQVksQ0FoQ1g7QUFpQ0RDLGVBQU0sRUFqQ0w7QUFrQ0RDLGtCQUFTLEVBbENSO0FBbUNEQyxrQkFBVTFDLEtBQUsyQyx3QkFuQ2Q7QUFvQ0RDLGlCQUFTNUMsS0FBSzZDLHVCQXBDYjtBQXFDREMscUJBQWE5QyxLQUFLOEMsV0FyQ2pCO0FBc0NEQyx1QkFBZS9DLEtBQUtnRCxjQXRDbkI7QUF1Q0RDLGFBQUssWUFBVS9DLFFBQVFnQyxZQUFsQixHQUErQixHQUEvQixHQUFtQ2xDLEtBQUtrRDtBQXZDNUMsS0FGUTs7QUE0Q2RDLFdBNUNjLHFCQTRDTDtBQUFBOztBQUVMLGlCQUFTQyxzQkFBVCxHQUFrQztBQUM5QixtQkFBT0MsTUFBTUMsR0FBTixDQUFVLHFCQUFWLENBQVA7QUFDSDs7QUFFRCxpQkFBU0Msa0JBQVQsR0FBOEI7QUFDMUIsbUJBQU9GLE1BQU1DLEdBQU4sQ0FBVSxpQkFBVixDQUFQO0FBQ0g7O0FBRUQsaUJBQVNFLHFCQUFULEdBQWlDO0FBQzdCLG1CQUFPSCxNQUFNQyxHQUFOLENBQVUscUJBQVYsQ0FBUDtBQUNIOztBQUVELGlCQUFTRyxlQUFULEdBQTBCO0FBQ3RCLG1CQUFPQyxXQUFXQyxJQUFYLENBQWdCLCtCQUFoQixFQUFnRDtBQUNuREMseUJBQVFsRDtBQUQyQyxhQUFoRCxDQUFQO0FBR0g7O0FBRUQsaUJBQVNtRCxVQUFULEdBQXNCO0FBQ2xCLG1CQUFPSCxXQUFXSixHQUFYLENBQWUsOEJBQTRCNUMsZUFBNUIsR0FBNEMsVUFBM0QsQ0FBUDtBQUNIOztBQUVELGlCQUFTb0QsY0FBVCxHQUEwQjtBQUN0QixtQkFBT0osV0FBV0osR0FBWCxDQUFlLG1DQUFpQzVDLGVBQWhELENBQVA7QUFDSDs7QUFFRCxpQkFBU3FELE9BQVQsR0FBbUI7QUFDZixtQkFBT1YsTUFBTUMsR0FBTixDQUFVVSxTQUFTQyxNQUFULEdBQWtCLFlBQTVCLENBQVA7QUFDSDs7QUFFRFosY0FBTWEsR0FBTixDQUFVLENBQ05kLHdCQURNLEVBRUxHLG9CQUZLLEVBR0xDLHVCQUhLLEVBSUxLLFlBSkssRUFLTEMsZ0JBTEssRUFNTEMsU0FOSyxFQU9MTixpQkFQSyxDQUFWLEVBUUdVLElBUkgsQ0FRUWQsTUFBTWUsTUFBTixDQUNKLFVBQ0lDLG1CQURKLEVBRUtDLGVBRkwsRUFHS0Msa0JBSEwsRUFJS2pDLE9BSkwsRUFLS0MsV0FMTCxFQU1LaUMsSUFOTCxFQU9LQyxPQVBMLEVBUUs7QUFDRCxrQkFBS0MsSUFBTCxHQUFZTCxvQkFBb0JyRSxJQUFwQixDQUF5QkEsSUFBckM7QUFDQSxrQkFBS3lDLFFBQUwsR0FBZ0I2QixnQkFBZ0J0RSxJQUFoQixDQUFxQkEsSUFBckM7QUFDQSxrQkFBSzJFLFdBQUwsR0FBbUJKLG1CQUFtQnZFLElBQW5CLENBQXdCQSxJQUEzQztBQUNBLGtCQUFLc0MsT0FBTCxHQUFlQSxRQUFRc0MsTUFBUixJQUFrQixHQUFsQixHQUF1QnRDLFFBQVF0QyxJQUEvQixHQUFvQyxFQUFuRDtBQUNBLGtCQUFLdUMsV0FBTCxHQUFtQkEsWUFBWXFDLE1BQVosSUFBc0IsR0FBdEIsR0FBMkJyQyxZQUFZdkMsSUFBdkMsR0FBNEMsQ0FBL0Q7QUFDQSxrQkFBS3dDLEtBQUwsR0FBYWlDLFFBQVFHLE1BQVIsSUFBa0IsR0FBbEIsR0FBdUJILFFBQVF6RSxJQUEvQixHQUFvQyxFQUFqRDs7QUFFQSxnQkFBRyxNQUFLMEMsUUFBTCxDQUFjbUMsTUFBZCxJQUF3QixDQUEzQixFQUE2QjtBQUN6QixzQkFBS3ZELFVBQUwsR0FBa0IsTUFBS29CLFFBQUwsQ0FBYyxDQUFkLEVBQWlCb0MsS0FBbkM7QUFDQSxzQkFBS3ZELFdBQUwsR0FBbUIsTUFBS21CLFFBQUwsQ0FBYyxDQUFkLEVBQWlCcUMsS0FBcEM7QUFDQSxzQkFBS3ZELFVBQUwsR0FBa0IsTUFBS3lCLEdBQUwsR0FBVyxNQUFLUCxRQUFMLENBQWMsQ0FBZCxFQUFpQnNDLFVBQTVCLEdBQXlDLE1BQTNEO0FBQ0g7QUFFUixTQXZCTyxDQVJSLEVBZ0NDQyxLQWhDRCxDQWdDT0MsRUFBRUMsSUFoQ1Q7O0FBa0NBQyxjQUFNQyxNQUFOLENBQWEsbUNBQWIsRUFBa0QsVUFBQ3JGLElBQUQsRUFBVTtBQUN4RCxrQkFBS3lCLElBQUwsQ0FBVUcsU0FBVixHQUFzQjVCLElBQXRCO0FBQ0gsU0FGRDtBQUdILEtBakhhOzs7QUFtSGRzRixhQUFRO0FBQ0pDLHNCQURJLDRCQUNZO0FBQUE7O0FBQ1o7QUFDQSxpQkFBSzlELElBQUwsQ0FBVUssU0FBVixHQUFzQixDQUF0QjtBQUNBLGlCQUFLTCxJQUFMLENBQVUrRCxNQUFWLENBQWlCLE1BQWpCLEVBQXlCLHFCQUF6QixFQUNLckIsSUFETCxDQUNVLGtCQUFVO0FBQ1osb0JBQUdzQixPQUFPQyxPQUFWLEVBQ0E7QUFDSSwyQkFBS3BELE9BQUwsQ0FBYXFELE9BQWIsQ0FBcUJGLE9BQU96RixJQUE1QjtBQUNBNEYsMkJBQU9GLE9BQVAsQ0FBZSxFQUFmLEVBQWtCLE9BQUtmLFdBQUwsQ0FBaUIsVUFBakIsQ0FBbEI7QUFDQSwyQkFBS2xELElBQUwsQ0FBVUcsU0FBVixHQUFzQixFQUF0QjtBQUNBO0FBQ0gsaUJBTkQsTUFPQTtBQUNJZ0UsMkJBQU9DLEtBQVAsQ0FBYSxPQUFLbEIsV0FBTCxDQUFpQixZQUFqQixDQUFiO0FBQ0g7QUFDSixhQVpMLEVBYUtNLEtBYkwsQ0FhV0MsRUFBRUMsSUFiYjtBQWNILFNBbEJHOzs7QUFvQkpXLHdCQUFnQix3QkFBVUMsQ0FBVixFQUFhO0FBQ3pCLGdCQUFJLENBQUNBLEVBQUVDLE9BQUYsSUFBYUQsRUFBRUUsT0FBaEIsS0FBNEJGLEVBQUVHLE9BQUYsSUFBYSxFQUE3QyxFQUFpRDtBQUM3QyxxQkFBS1gsY0FBTDtBQUNIO0FBQ0osU0F4Qkc7O0FBMEJKWSxxQkExQkksMkJBMEJXO0FBQUE7O0FBQ1gsZ0JBQUlDLEtBQUo7QUFDQSxpQkFBSSxJQUFJQyxJQUFFLENBQVYsRUFBYUEsSUFBRSxLQUFLL0QsT0FBTCxDQUFhdUMsTUFBNUIsRUFBb0N3QixHQUFwQyxFQUNBO0FBQ0ksb0JBQUcsS0FBSy9ELE9BQUwsQ0FBYStELENBQWIsRUFBZ0IxRixFQUFoQixJQUFzQixLQUFLeUIsV0FBTCxDQUFpQkMsU0FBMUMsRUFDQTtBQUNJK0QsNEJBQVFDLENBQVI7QUFDQTtBQUNIO0FBQ0o7QUFDRCxpQkFBS2pFLFdBQUwsQ0FBaUJOLFNBQWpCLEdBQTZCLENBQTdCOztBQUVBLGlCQUFLTSxXQUFMLENBQWlCb0QsTUFBakIsQ0FBd0IsTUFBeEIsRUFBZ0MscUJBQWhDLEVBQ0tyQixJQURMLENBQ1Usa0JBQVU7QUFDWixvQkFBR3NCLE9BQU9DLE9BQVYsRUFDQTtBQUNJLDJCQUFLcEQsT0FBTCxDQUFhOEQsS0FBYixFQUFvQkUsUUFBcEIsQ0FBNkJYLE9BQTdCLENBQXFDRixPQUFPekYsSUFBNUM7QUFDQTRGLDJCQUFPRixPQUFQLENBQWUsRUFBZixFQUFrQixPQUFLZixXQUFMLENBQWlCLFNBQWpCLENBQWxCO0FBQ0FPLHNCQUFFLGdCQUFGLEVBQW9CcUIsS0FBcEIsQ0FBMEIsUUFBMUI7QUFDSCxpQkFMRCxNQU1BO0FBQ0lYLDJCQUFPQyxLQUFQLENBQWEsT0FBS2xCLFdBQUwsQ0FBaUIsWUFBakIsQ0FBYjtBQUNIO0FBQ0osYUFYTCxFQVlLTSxLQVpMLENBWVdDLEVBQUVDLElBWmI7QUFhSCxTQW5ERztBQXFESnFCLHVCQXJESSw2QkFxRGE7QUFBQTs7QUFDYixxQkFBUzNDLFVBQVQsQ0FBb0I0QyxZQUFwQixFQUFrQztBQUM5Qix1QkFBTy9DLFdBQVdKLEdBQVgsQ0FBZSw4QkFBNEI1QyxlQUE1QixHQUE0QyxnQkFBNUMsR0FBNkQrRixZQUE1RSxDQUFQO0FBQ0g7O0FBRURwRCxrQkFBTWEsR0FBTixDQUFVLENBQ0xMLFdBQVcsS0FBS3ZCLE9BQUwsQ0FBYXVDLE1BQXhCLENBREssQ0FBVixFQUVHVixJQUZILENBRVFkLE1BQU1lLE1BQU4sQ0FDSixVQUNLOUIsT0FETCxFQUVLO0FBQ0Qsb0JBQUdBLFFBQVFzQyxNQUFSLElBQWtCLEdBQXJCLEVBQ0E7QUFDSSwyQkFBS3RDLE9BQUwsR0FBZSxPQUFLQSxPQUFMLENBQWFvRSxNQUFiLENBQW9CcEUsUUFBUXRDLElBQTVCLENBQWY7QUFDSDtBQUNSLGFBUk8sQ0FGUixFQVdDaUYsS0FYRCxDQVdPQyxFQUFFQyxJQVhUO0FBWUgsU0F0RUc7QUF3RUp3QixtQkF4RUksdUJBd0VRQyxLQXhFUixFQXdFYztBQUNkMUIsY0FBRSxNQUFGLEVBQVUyQixHQUFWLENBQWMsR0FBZDtBQUNBLGlCQUFLeEYsT0FBTCxHQUFldUYsTUFBTWpHLEVBQXJCO0FBQ0EsaUJBQUtXLFVBQUwsR0FBa0JzRixNQUFNOUIsS0FBeEI7QUFDQSxpQkFBS3ZELFdBQUwsR0FBbUJxRixNQUFNN0IsS0FBekI7QUFDQSxpQkFBS3ZELFVBQUwsR0FBa0IsS0FBS3lCLEdBQUwsR0FBVzJELE1BQU01QixVQUFqQixHQUE4QixNQUFoRDtBQUNILFNBOUVHO0FBZ0ZKOEIscUJBaEZJLHlCQWdGVW5HLEVBaEZWLEVBZ0ZhO0FBQUE7O0FBQ2IsZ0JBQUdULFFBQVE2RyxlQUFSLEtBQTRCLElBQS9CLEVBQW9DO0FBQ2hDckQsMkJBQVdKLEdBQVgsQ0FBZSxxQkFBcUIzQyxFQUFwQyxFQUNDd0QsSUFERCxDQUNNLGtCQUFVO0FBQ1osd0JBQUdzQixPQUFPekYsSUFBUCxDQUFZZ0gsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUN2Qiw0QkFBSUMsTUFBTUMsU0FBU2hDLEVBQUUsWUFBRixFQUFnQmlDLElBQWhCLEVBQVQsSUFBaUMsQ0FBM0M7QUFDQWpDLDBCQUFFLFlBQUYsRUFBZ0JpQyxJQUFoQixDQUFxQkYsR0FBckIsRUFBMEJHLElBQTFCO0FBQ0F4QiwrQkFBT0YsT0FBUCxDQUFlLE9BQUtqRCxRQUFMLENBQWMsaUJBQWQsQ0FBZixFQUFpRDRFLElBQWpEO0FBQ0gscUJBSkQsTUFJTyxJQUFHNUIsT0FBT3pGLElBQVAsQ0FBWWdILFFBQVosSUFBc0IsQ0FBekIsRUFBMkI7QUFDOUJwQiwrQkFBT0MsS0FBUCxDQUFhSixPQUFPekYsSUFBUCxDQUFZc0gsU0FBekIsRUFBb0MsT0FBSzdFLFFBQUwsQ0FBYyxvQkFBZCxDQUFwQztBQUNILHFCQUZNLE1BRUE7QUFDSG1ELCtCQUFPMkIsSUFBUCxDQUFZLE9BQUs5RSxRQUFMLENBQWMsbUJBQWQsQ0FBWixFQUFnRDRFLElBQWhEO0FBQ0g7QUFDSixpQkFYRDtBQVlILGFBYkQsTUFhTztBQUNIekIsdUJBQU9DLEtBQVAsQ0FBYSxLQUFLcEQsUUFBTCxDQUFjLGlCQUFkLENBQWI7QUFDSDtBQUNKLFNBakdHO0FBbUdKK0Usa0JBbkdJLHNCQW1HTzdHLEVBbkdQLEVBbUdVO0FBQUE7O0FBRVYsZ0JBQUk4RyxNQUFNdkMsRUFBRSxNQUFGLEVBQVUyQixHQUFWLEVBQVY7QUFDQSxnQkFBSWEsV0FBVyxLQUFLbkcsV0FBcEI7QUFDQSxnQkFBSW9HLE9BQU96QyxFQUFFLHdCQUFGLEVBQTRCMkIsR0FBNUIsRUFBWDtBQUNBLGdCQUFJZSxVQUFVMUMsRUFBRSx3QkFBRixFQUE0QmlDLElBQTVCLEVBQWQ7O0FBRUEsZ0JBQUcsS0FBS3pFLFFBQUwsQ0FBY21DLE1BQWQsSUFBd0IsQ0FBeEIsSUFBNkIsS0FBS2pDLE9BQUwsQ0FBYWlDLE1BQWIsSUFBdUIsQ0FBdkQsRUFBeUQ7O0FBRXJEeEIsc0JBQU1DLEdBQU4sQ0FBVVUsU0FBU0MsTUFBVCxHQUFrQixZQUE1QixFQUF5QztBQUNyQzRELDRCQUFRO0FBQ0pDLGlDQUFRbkgsRUFESjtBQUVKOEcsNkJBQUlBO0FBRkE7QUFENkIsaUJBQXpDLEVBTUN0RCxJQU5ELENBTU0sa0JBQVU7QUFDWmUsc0JBQUUsaUNBQUYsRUFBcUNpQyxJQUFyQyxDQUEwQzFCLE9BQU96RixJQUFQLENBQVkrSCxlQUF0RDtBQUNBLHdCQUFHdEMsT0FBT3pGLElBQVAsQ0FBWXNILFNBQWYsRUFDSTFCLE9BQU9DLEtBQVAsQ0FBYUosT0FBT3pGLElBQVAsQ0FBWXNILFNBQXpCLEVBQW9DLE9BQUs3RSxRQUFMLENBQWMsV0FBZCxDQUFwQyxFQURKLEtBR0ltRCxPQUFPRixPQUFQLENBQWUsT0FBS2pELFFBQUwsQ0FBYyxZQUFkLENBQWY7QUFDUCxpQkFaRDs7QUFjQXVGLDJCQUFXLFlBQVU7QUFDckIzRSwwQkFBTUMsR0FBTixDQUFVVSxTQUFTQyxNQUFULEdBQWtCLFlBQTVCLEVBQ0NFLElBREQsQ0FDTSxrQkFBVTtBQUNaZSwwQkFBRSxpQ0FBRixFQUFxQ2lDLElBQXJDLENBQTBDMUIsT0FBT3pGLElBQVAsQ0FBWStILGVBQXREO0FBQ0gscUJBSEQ7QUFJQyxpQkFMRCxFQUtHLElBTEg7QUFPSCxhQXZCRCxNQXVCTzs7QUFFSCxvQkFBR0osUUFBUSxNQUFYLEVBQWtCO0FBQ2QvQiwyQkFBT0MsS0FBUCxDQUFhLEtBQUtwRCxRQUFMLENBQWMsYUFBZCxDQUFiLEVBQTJDLEtBQUtBLFFBQUwsQ0FBYyxXQUFkLENBQTNDO0FBQ0gsaUJBRkQsTUFFTzs7QUFFSFksMEJBQU1DLEdBQU4sQ0FBVVUsU0FBU0MsTUFBVCxHQUFrQixZQUE1QixFQUF5QztBQUNyQzRELGdDQUFRO0FBQ0pDLHFDQUFRbkgsRUFESjtBQUVKOEcsaUNBQUlBLEdBRkE7QUFHSlEsbUNBQU1QLFFBSEY7QUFJSnJHLHFDQUFRLEtBQUtBLE9BSlQ7QUFLSkMsd0NBQVcsS0FBS0EsVUFMWjtBQU1KRSx3Q0FBVyxLQUFLQSxVQU5aO0FBT0ptRyxrQ0FBS0M7QUFQRDtBQUQ2QixxQkFBekMsRUFXQ3pELElBWEQsQ0FXTSxrQkFBVTtBQUNaZSwwQkFBRSxpQ0FBRixFQUFxQ2lDLElBQXJDLENBQTBDMUIsT0FBT3pGLElBQVAsQ0FBWStILGVBQXREO0FBQ0EsNEJBQUd0QyxPQUFPekYsSUFBUCxDQUFZc0gsU0FBZixFQUNJMUIsT0FBT0MsS0FBUCxDQUFhSixPQUFPekYsSUFBUCxDQUFZc0gsU0FBekIsRUFBb0MsT0FBSzdFLFFBQUwsQ0FBYyxXQUFkLENBQXBDLEVBREosS0FHSW1ELE9BQU9GLE9BQVAsQ0FBZSxPQUFLakQsUUFBTCxDQUFjLFlBQWQsQ0FBZjtBQUNQLHFCQWpCRDs7QUFtQkF1RiwrQkFBVyxZQUFVO0FBQ3JCM0UsOEJBQU1DLEdBQU4sQ0FBVVUsU0FBU0MsTUFBVCxHQUFrQixZQUE1QixFQUNDRSxJQURELENBQ00sa0JBQVU7QUFDWmUsOEJBQUUsaUNBQUYsRUFBcUNpQyxJQUFyQyxDQUEwQzFCLE9BQU96RixJQUFQLENBQVkrSCxlQUF0RDtBQUNILHlCQUhEO0FBSUMscUJBTEQsRUFLRyxJQUxIO0FBTUg7QUFDSjtBQUVKLFNBbktHO0FBcUtKRyxjQXJLSSxrQkFxS0d2SCxFQXJLSCxFQXFLTTtBQUFBOztBQUVOLGdCQUFJOEcsTUFBTXZDLEVBQUUsTUFBRixFQUFVMkIsR0FBVixFQUFWO0FBQ0EsZ0JBQUlhLFdBQVcsS0FBS25HLFdBQXBCO0FBQ0EsZ0JBQUlvRyxPQUFPekMsRUFBRSx3QkFBRixFQUE0QjJCLEdBQTVCLEVBQVg7QUFDQSxnQkFBSWUsVUFBVTFDLEVBQUUsd0JBQUYsRUFBNEJpQyxJQUE1QixFQUFkOztBQUVBOUQsa0JBQU1DLEdBQU4sQ0FBVVUsU0FBU0MsTUFBVCxHQUFrQixZQUE1QixFQUNDRSxJQURELENBQ00sa0JBQVU7QUFDWixvQkFBSWdFLFFBQVFqRCxFQUFFa0QsR0FBRixDQUFNM0MsT0FBT3pGLElBQVAsQ0FBWXdFLElBQWxCLEVBQXdCLFVBQVM2RCxLQUFULEVBQWdCakMsS0FBaEIsRUFBdUI7QUFDdkQsMkJBQU8sQ0FBQ2lDLEtBQUQsQ0FBUDtBQUNILGlCQUZXLENBQVo7O0FBSUEsb0JBQUlDLFFBQVFILE1BQU1JLElBQU4sQ0FBVztBQUFBLDJCQUFRQyxLQUFLN0gsRUFBTCxJQUFXRCxlQUFuQjtBQUFBLGlCQUFYLENBQVo7O0FBRUEsb0JBQUcsT0FBS2dDLFFBQUwsQ0FBY21DLE1BQWQsSUFBd0IsQ0FBeEIsSUFBNkIsT0FBS2pDLE9BQUwsQ0FBYWlDLE1BQWIsSUFBdUIsQ0FBdkQsRUFBeUQ7QUFDckQsd0JBQUd5RCxTQUFTRyxTQUFaLEVBQXNCO0FBQ2xCcEYsOEJBQU1DLEdBQU4sQ0FBVVUsU0FBU0MsTUFBVCxHQUFrQixZQUE1QixFQUF5QztBQUNyQzRELG9DQUFRO0FBQ0pDLHlDQUFRbkgsRUFESjtBQUVKOEcscUNBQUlBOztBQUZBO0FBRDZCLHlCQUF6QyxFQU9DdEQsSUFQRCxDQU9NLGtCQUFVO0FBQ1osZ0NBQUdzQixPQUFPekYsSUFBUCxDQUFZc0gsU0FBZixFQUNBO0FBQ0kxQix1Q0FBT0MsS0FBUCxDQUFhSixPQUFPekYsSUFBUCxDQUFZc0gsU0FBekIsRUFBb0MsT0FBSzdFLFFBQUwsQ0FBYyxhQUFkLENBQXBDO0FBQ0gsNkJBSEQsTUFJQTtBQUNJeEMsdUNBQU8rRCxRQUFQLENBQWdCMEUsTUFBaEIsQ0FBdUIsT0FBdkI7QUFDSDtBQUNKLHlCQWZEO0FBZ0JILHFCQWpCRCxNQWlCTztBQUNIckYsOEJBQU1DLEdBQU4sQ0FBVVUsU0FBU0MsTUFBVCxHQUFrQixjQUE1QixFQUEyQztBQUN2QzRELG9DQUFRO0FBQ0pDLHlDQUFRbkgsRUFESjtBQUVKZ0ksd0NBQU9MLE1BQU1NLEtBRlQ7QUFHSm5CLHFDQUFJQTtBQUhBO0FBRCtCLHlCQUEzQzs7QUFRQU8sbUNBQVcsWUFBVTtBQUNqQi9ILG1DQUFPK0QsUUFBUCxDQUFnQjBFLE1BQWhCLENBQXVCLE9BQXZCO0FBQ0gseUJBRkQsRUFFRyxJQUZIO0FBR0g7QUFDSixpQkEvQkQsTUErQk87O0FBRUgsd0JBQUdmLFFBQVEsTUFBWCxFQUFrQjtBQUNkL0IsK0JBQU9DLEtBQVAsQ0FBYSxPQUFLcEQsUUFBTCxDQUFjLGFBQWQsQ0FBYixFQUEyQyxPQUFLQSxRQUFMLENBQWMsV0FBZCxDQUEzQztBQUNILHFCQUZELE1BRU87QUFDSCw0QkFBRzZGLFNBQVNHLFNBQVosRUFBc0I7QUFDbEJwRixrQ0FBTUMsR0FBTixDQUFVVSxTQUFTQyxNQUFULEdBQWtCLFlBQTVCLEVBQXlDO0FBQ3JDNEQsd0NBQVE7QUFDSkMsNkNBQVFuSCxFQURKO0FBRUo4Ryx5Q0FBSUEsR0FGQTtBQUdKUSwyQ0FBTVAsUUFIRjtBQUlKckcsNkNBQVEsT0FBS0EsT0FKVDtBQUtKQyxnREFBVyxPQUFLQSxVQUxaO0FBTUpFLGdEQUFXLE9BQUtBLFVBTlo7QUFPSm1HLDBDQUFLQztBQVBEO0FBRDZCLDZCQUF6QyxFQVdDekQsSUFYRCxDQVdNLGtCQUFVO0FBQ1osb0NBQUdzQixPQUFPekYsSUFBUCxDQUFZc0gsU0FBZixFQUNBO0FBQ0kxQiwyQ0FBT0MsS0FBUCxDQUFhSixPQUFPekYsSUFBUCxDQUFZc0gsU0FBekIsRUFBb0MsT0FBSzdFLFFBQUwsQ0FBYyxhQUFkLENBQXBDO0FBQ0gsaUNBSEQsTUFJQTtBQUNJeEMsMkNBQU8rRCxRQUFQLENBQWdCMEUsTUFBaEIsQ0FBdUIsT0FBdkI7QUFDSDtBQUNKLDZCQW5CRDtBQW9CSCx5QkFyQkQsTUFxQk87O0FBRUgsZ0NBQUlHLE1BQU0zRCxFQUFFNEQsSUFBRixDQUFPWCxLQUFQLEVBQWMsVUFBVVksQ0FBVixFQUFjO0FBQ3BDLHVDQUFPQSxFQUFFcEksRUFBRixJQUFRRCxlQUFmO0FBQ0QsNkJBRlMsQ0FBVjs7QUFJQSxnQ0FBSXNJLE9BQU9ILElBQUlOLElBQUosQ0FBUztBQUFBLHVDQUFRQyxLQUFLUyxPQUFMLENBQWF0QixJQUFiLElBQXFCQyxPQUE3QjtBQUFBLDZCQUFULENBQVg7O0FBRUEsZ0NBQUdGLFlBQVksT0FBWixJQUF1QkMsUUFBUSxNQUFsQyxFQUNJLElBQUlxQixPQUFPSCxJQUFJTixJQUFKLENBQVM7QUFBQSx1Q0FBUUMsS0FBS1MsT0FBTCxDQUFhaEIsS0FBYixJQUFzQlAsUUFBdEIsSUFBa0NjLEtBQUtTLE9BQUwsQ0FBYXRCLElBQWIsSUFBcUJDLE9BQS9EO0FBQUEsNkJBQVQsQ0FBWCxDQURKLEtBRUssSUFBR0YsWUFBWSxPQUFmLEVBQ0QsSUFBSXNCLE9BQU9ILElBQUlOLElBQUosQ0FBUztBQUFBLHVDQUFRQyxLQUFLUyxPQUFMLENBQWFoQixLQUFiLElBQXNCUCxRQUE5QjtBQUFBLDZCQUFULENBQVgsQ0FEQyxLQUVBLElBQUdDLFFBQVEsTUFBWCxFQUNELElBQUlxQixPQUFPSCxJQUFJTixJQUFKLENBQVM7QUFBQSx1Q0FBUUMsS0FBS1MsT0FBTCxDQUFhdEIsSUFBYixJQUFxQkMsT0FBN0I7QUFBQSw2QkFBVCxDQUFYOztBQUVKLGdDQUFHb0IsSUFBSCxFQUFRO0FBQ0ozRixzQ0FBTUMsR0FBTixDQUFVVSxTQUFTQyxNQUFULEdBQWtCLGNBQTVCLEVBQTJDO0FBQ3ZDNEQsNENBQVE7QUFDSkMsaURBQVFuSCxFQURKO0FBRUpnSSxnREFBT0ssS0FBS0osS0FGUjtBQUdKbkIsNkNBQUlBLEdBSEE7QUFJSlEsK0NBQU1QLFFBSkY7QUFLSnJHLGlEQUFRLE9BQUtBLE9BTFQ7QUFNSkMsb0RBQVcsT0FBS0EsVUFOWjtBQU9KRSxvREFBVyxPQUFLQSxVQVBaO0FBUUptRyw4Q0FBS0M7QUFSRDtBQUQrQixpQ0FBM0M7O0FBYUFJLDJDQUFXLFlBQVU7QUFDakIvSCwyQ0FBTytELFFBQVAsQ0FBZ0IwRSxNQUFoQixDQUF1QixPQUF2QjtBQUNILGlDQUZELEVBRUcsSUFGSDtBQUlILDZCQWxCRCxNQWtCTztBQUNIckYsc0NBQU1DLEdBQU4sQ0FBVVUsU0FBU0MsTUFBVCxHQUFrQixZQUE1QixFQUF5QztBQUNyQzRELDRDQUFRO0FBQ0pDLGlEQUFRbkgsRUFESjtBQUVKOEcsNkNBQUlBLEdBRkE7QUFHSlEsK0NBQU1QLFFBSEY7QUFJSnJHLGlEQUFRLE9BQUtBLE9BSlQ7QUFLSkMsb0RBQVcsT0FBS0EsVUFMWjtBQU1KRSxvREFBVyxPQUFLQSxVQU5aO0FBT0ptRyw4Q0FBS0M7QUFQRDtBQUQ2QixpQ0FBekMsRUFXQ3pELElBWEQsQ0FXTSxrQkFBVTtBQUNaZSxzQ0FBRSxpQ0FBRixFQUFxQ2lDLElBQXJDLENBQTBDMUIsT0FBT3pGLElBQVAsQ0FBWStILGVBQXREO0FBQ0Esd0NBQUd0QyxPQUFPekYsSUFBUCxDQUFZc0gsU0FBZixFQUNJMUIsT0FBT0MsS0FBUCxDQUFhSixPQUFPekYsSUFBUCxDQUFZc0gsU0FBekIsRUFBb0MsT0FBSzdFLFFBQUwsQ0FBYyxXQUFkLENBQXBDLEVBREosS0FFSTtBQUNBdUYsbURBQVcsWUFBVTtBQUNqQi9ILG1EQUFPK0QsUUFBUCxDQUFnQjBFLE1BQWhCLENBQXVCLE9BQXZCO0FBQ0gseUNBRkQsRUFFRyxJQUZIO0FBR0g7QUFDSixpQ0FwQkQ7QUFxQkg7QUFFSjtBQUNKO0FBQ0o7QUFFSixhQTlIRDtBQWdJSCxTQTVTRztBQThTSlEsaUJBOVNJLHFCQThTTXZJLEVBOVNOLEVBOFNTMEcsSUE5U1QsRUE4U2M7QUFBQTs7QUFDZGhFLGtCQUFNQyxHQUFOLENBQVVVLFNBQVNDLE1BQVQsR0FBa0IsV0FBNUIsRUFBd0M7QUFDcEM0RCx3QkFBUTtBQUNKQyw2QkFBUW5ILEVBREo7QUFFSjhHLHlCQUFJO0FBRkE7QUFENEIsYUFBeEMsRUFNQ3RELElBTkQsQ0FNTSxrQkFBVTtBQUNaZSxrQkFBRSxpQ0FBRixFQUFxQ2lDLElBQXJDLENBQTBDMUIsT0FBT3pGLElBQVAsQ0FBWStILGVBQXREO0FBQ0Esb0JBQUd0QyxPQUFPekYsSUFBUCxDQUFZc0gsU0FBZixFQUNBO0FBQ0ksd0JBQUc3QixPQUFPekYsSUFBUCxDQUFZc0gsU0FBWixJQUF1QixVQUExQixFQUNJckgsT0FBTytELFFBQVAsR0FBa0IsY0FBY3lCLE9BQU96RixJQUFQLENBQVltSixJQUE1QyxDQURKLEtBR0l2RCxPQUFPQyxLQUFQLENBQWFKLE9BQU96RixJQUFQLENBQVlzSCxTQUF6QixFQUFvQyxPQUFLN0UsUUFBTCxDQUFjLFdBQWQsQ0FBcEM7QUFDUCxpQkFORCxNQU1PO0FBQ0htRCwyQkFBT0YsT0FBUCxDQUFlLE9BQUtqRCxRQUFMLENBQWMsWUFBZCxDQUFmLEVBQTRDNEUsSUFBNUM7QUFDSDtBQUNKLGFBakJEO0FBa0JILFNBalVHO0FBa1VKK0Isb0JBbFVJLHdCQWtVU3pJLEVBbFVULEVBa1VZMEcsSUFsVVosRUFrVWlCO0FBQUE7O0FBQ2pCLGdCQUFHbkgsUUFBUTZHLGVBQVIsS0FBNEIsSUFBL0IsRUFBb0M7QUFDaENyRCwyQkFBV0osR0FBWCxDQUFlLG1CQUFtQjNDLEVBQWxDLEVBQ0N3RCxJQURELENBQ00sa0JBQVU7QUFDWix3QkFBR3NCLE9BQU96RixJQUFQLENBQVlnSCxRQUFaLElBQXNCLENBQXpCLEVBQTJCO0FBQ3ZCLDRCQUFJcUMsT0FBT25DLFNBQVNoQyxFQUFFLGFBQUYsRUFBaUJpQyxJQUFqQixFQUFULElBQWtDLENBQTdDO0FBQ0FqQywwQkFBRSxhQUFGLEVBQWlCaUMsSUFBakIsQ0FBc0JrQyxJQUF0QixFQUE0QmpDLElBQTVCO0FBQ0F4QiwrQkFBT0YsT0FBUCxDQUFlLE9BQUtqRCxRQUFMLENBQWMsa0JBQWQsQ0FBZixFQUFrRDRFLElBQWxEO0FBQ0gscUJBSkQsTUFJTyxJQUFHNUIsT0FBT3pGLElBQVAsQ0FBWWdILFFBQVosSUFBc0IsQ0FBekIsRUFBNEI7QUFDL0JwQiwrQkFBT0MsS0FBUCxDQUFhLE9BQUtwRCxRQUFMLENBQWMsTUFBZCxDQUFiO0FBQ0gscUJBRk0sTUFFQTtBQUNIbUQsK0JBQU8yQixJQUFQLENBQVksT0FBSzlFLFFBQUwsQ0FBYyxpQkFBZCxDQUFaLEVBQThDNEUsSUFBOUM7QUFDSDtBQUNKLGlCQVhEO0FBWUgsYUFiRCxNQWFPO0FBQ0h6Qix1QkFBT0MsS0FBUCxDQUFhLEtBQUtwRCxRQUFMLENBQWMsZUFBZCxDQUFiO0FBQ0g7QUFDSixTQW5WRztBQW9WSjZHLHNCQXBWSSwwQkFvVlczSSxFQXBWWCxFQW9WYzBHLElBcFZkLEVBb1ZtQjtBQUFBOztBQUNuQixnQkFBR25ILFFBQVE2RyxlQUFSLEtBQTRCLElBQS9CLEVBQW9DO0FBQ2hDckQsMkJBQVdKLEdBQVgsQ0FBZSxxQkFBcUIzQyxFQUFwQyxFQUNDd0QsSUFERCxDQUNNLGtCQUFVO0FBQ1osd0JBQUdzQixPQUFPekYsSUFBUCxDQUFZZ0gsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUN2Qiw0QkFBSUMsTUFBTUMsU0FBU2hDLEVBQUUsWUFBRixFQUFnQmlDLElBQWhCLEVBQVQsSUFBaUMsQ0FBM0M7QUFDQWpDLDBCQUFFLFlBQUYsRUFBZ0JpQyxJQUFoQixDQUFxQkYsR0FBckIsRUFBMEJHLElBQTFCO0FBQ0F4QiwrQkFBT0YsT0FBUCxDQUFlLFFBQUtqRCxRQUFMLENBQWMsaUJBQWQsQ0FBZixFQUFpRDRFLElBQWpEO0FBQ0gscUJBSkQsTUFJTztBQUNIekIsK0JBQU8yQixJQUFQLENBQVksUUFBSzlFLFFBQUwsQ0FBYyxtQkFBZCxDQUFaLEVBQWdENEUsSUFBaEQ7QUFDSDtBQUNKLGlCQVREO0FBVUgsYUFYRCxNQVdPO0FBQ0h6Qix1QkFBT0MsS0FBUCxDQUFhLEtBQUtwRCxRQUFMLENBQWMsaUJBQWQsQ0FBYjtBQUNIO0FBQ0osU0FuV0c7QUFxV0o4RyxpQkFyV0ksdUJBcVdROztBQUVSLGdCQUFJQyxPQUFPLEVBQVg7QUFDQXRFLGNBQUUsaUJBQUYsRUFBcUJ1RSxJQUFyQixDQUEwQixZQUFVO0FBQ2hDRCxxQkFBS0UsSUFBTCxDQUFVeEUsRUFBRSxJQUFGLEVBQVF5RSxJQUFSLENBQWEsSUFBYixDQUFWO0FBQ0gsYUFGRDs7QUFJQSxnQkFBR3pFLEVBQUUwRSxPQUFGLENBQVUsU0FBTyxLQUFLeEosSUFBTCxDQUFVTyxFQUEzQixFQUE4QjZJLElBQTlCLEtBQXFDLElBQXhDLEVBQ0lwRSxNQUFNeUUsSUFBTixDQUFXLGVBQVgsRUFBNEIsS0FBS3pKLElBQWpDOztBQUVKNEgsdUJBQVcsWUFBVTtBQUNuQjlDLGtCQUFFLGVBQUYsRUFBbUI0RSxnQkFBbkI7QUFDRCxhQUZELEVBRUcsSUFGSDtBQUdILFNBbFhHO0FBb1hKQyxtQkFwWEkseUJBb1hTO0FBQ1RuRSxtQkFBT0MsS0FBUCxDQUFhLEtBQUtsQixXQUFMLENBQWlCLGVBQWpCLENBQWI7QUFDSCxTQXRYRztBQXdYSnFGLHVCQXhYSSw2QkF3WGE7QUFDYjlFLGNBQUUsa0JBQUYsRUFBc0JxQixLQUF0QixDQUE0QixRQUE1QjtBQUNILFNBMVhHO0FBNFhKMEQsa0JBNVhJLHdCQTRYUTtBQUNSckUsbUJBQU9DLEtBQVAsQ0FBYSxLQUFLbEIsV0FBTCxDQUFpQixhQUFqQixDQUFiO0FBQ0gsU0E5WEc7QUFnWUp1RixpQkFoWUksdUJBZ1lPO0FBQ1B0RSxtQkFBT0MsS0FBUCxDQUFhLEtBQUtsQixXQUFMLENBQWlCLGFBQWpCLENBQWI7QUFDSDtBQWxZRyxLQW5ITTtBQXVmZHdGLGNBQVM7QUFDTEMsaUJBREssdUJBQ007QUFDVCxnQkFBSS9DLE9BQU9wSCxPQUFPQyxPQUFQLENBQWVGLElBQWYsQ0FBb0JxSyxLQUFwQixDQUEwQkMsT0FBMUIsR0FBb0NySyxPQUFPQyxPQUFQLENBQWVGLElBQWYsQ0FBb0JxSyxLQUFwQixDQUEwQkMsT0FBOUQsR0FBdUVySyxPQUFPQyxPQUFQLENBQWVGLElBQWYsQ0FBb0JxSyxLQUFwQixDQUEwQmhELElBQTVHO0FBQ0EsZ0JBQUlrRCxJQUFJckYsRUFBRSx3QkFBRixDQUFSO0FBQ0EsZ0JBQUlzRixXQUFXRCxFQUFFWixJQUFGLENBQU8sU0FBUCxDQUFmOztBQUVBLGdCQUFHYSxZQUFVLElBQWIsRUFDRSxPQUFPbkQsSUFBUCxDQURGLEtBR0UsT0FBT3BILE9BQU9DLE9BQVAsQ0FBZUYsSUFBZixDQUFvQnFLLEtBQXBCLENBQTBCaEQsSUFBakM7QUFDSDtBQVZJO0FBdmZLLENBQVIsQ0FBVjs7QUFzZ0JBbkMsRUFBRSxZQUFVO0FBQ1Y7QUFDQSxRQUFJdUYseUJBQXlCdkYsRUFBRSx5QkFBRixDQUE3QjtBQUNBLFFBQUl3RixhQUFheEYsRUFBRSwyQkFBRixFQUErQkwsTUFBaEQ7QUFDQTRGLDJCQUF1QkUsV0FBdkIsQ0FBbUM7QUFDakNDLGdCQUFPLENBRDBCO0FBRWpDQyxjQUFLLEtBRjRCO0FBR2pDQyxhQUFJSixhQUFhLENBQWIsR0FBaUIsS0FBakIsR0FBeUIsSUFISTtBQUlqQ0ssbUJBQVVMLGFBQWEsQ0FBYixHQUFpQixLQUFqQixHQUF5QixJQUpGO0FBS2pDTSxtQkFBVU4sYUFBYSxDQUFiLEdBQWlCLEtBQWpCLEdBQXlCLElBTEY7QUFNakNPLGlCQUFRLENBQUMsa0NBQUQsRUFBb0MsbUNBQXBDLENBTnlCO0FBT2pDQyxvQkFBVztBQUNQLGVBQUUsRUFBRTFJLE9BQU0sQ0FBUjtBQURLO0FBUHNCLEtBQW5DO0FBV0EwQyxNQUFFLDJCQUFGLEVBQStCaUcsS0FBL0IsQ0FBcUMsWUFBVTtBQUM3QyxZQUFJQyxNQUFNbEcsRUFBRSxJQUFGLEVBQVFxRCxJQUFSLENBQWEsS0FBYixFQUFvQm9CLElBQXBCLENBQXlCLEtBQXpCLENBQVY7QUFDQSxZQUFJMEIsT0FBT25HLEVBQUUsSUFBRixFQUFRcUQsSUFBUixDQUFhLEtBQWIsRUFBb0JvQixJQUFwQixDQUF5QixpQkFBekIsQ0FBWDtBQUNBLFlBQUkyQixTQUFTcEcsRUFBRSxJQUFGLEVBQVFxRyxNQUFSLEdBQWlCQSxNQUFqQixHQUEwQkEsTUFBMUIsR0FBbUNBLE1BQW5DLEdBQTRDQSxNQUE1QyxHQUFxRGhELElBQXJELENBQTBELG1CQUExRCxDQUFiO0FBQ0ErQyxlQUFPM0IsSUFBUCxDQUFZLEtBQVosRUFBa0J5QixHQUFsQjtBQUNBRSxlQUFPM0IsSUFBUCxDQUFZLGlCQUFaLEVBQThCMEIsSUFBOUI7QUFDQW5HLFVBQUUsYUFBRixFQUFpQnNHLEdBQWpCLENBQXFCLGtCQUFyQixFQUF5QyxVQUFVSCxJQUFWLEdBQWlCLElBQTFEO0FBQ0EsZUFBTyxLQUFQO0FBQ0QsS0FSRDs7QUFVQW5HLE1BQUUsc0JBQUYsRUFBMEJzRyxHQUExQixDQUE4QixRQUE5QixFQUF1QyxHQUF2QztBQUVELENBM0JELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pmQTtVQUVBOztnQ0FFQTs4QkFDQTtBQUNBO29DQUNBO3FEQUNBO0FBQ0E7MENBQ0E7d0RBQ0E7QUFDQTs4Q0FDQTswREFDQTtBQUVBO0FBYkE7OzRCQWVBO29DQUNBLGlJQUVBLDJCQUNBO0FBQ0E7b0NBQ0E7b0NBQ0Esc0lBRUEsMkJBQ0E7QUFDQTtzQ0FDQTttRUFDQTtzQ0FDQTsyQ0FDQTtzQ0FDQTtBQUNBOzhDQUNBO0FBQ0EsYUFDQTsyQ0FDQTtpQkFDQTtBQUNBO3lCQUNBO0FBQ0E7QUFDQTtzQ0FDQTs0RUFDQTtBQUNBOzRDQUNBO21EQUNBO0FBR0E7QUFuQ0E7QUFoQkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGQTtVQUVBOztnQ0FFQTs4QkFDQTtBQUNBO29DQUNBO3FEQUNBO0FBQ0E7MENBQ0E7d0RBQ0E7QUFDQTs4Q0FDQTswREFDQTtBQUVBO0FBYkE7OzRCQWVBO29DQUNBLGlJQUVBLDJCQUNBO0FBQ0E7b0NBQ0E7b0NBQ0Esc0lBRUEsMkJBQ0E7QUFDQTtzQ0FDQTttRUFDQTtzQ0FDQTsyQ0FDQTtzQ0FDQTtBQUNBOzhDQUNBO0FBQ0EsYUFDQTsyQ0FDQTtpQkFDQTtBQUNBO3lCQUNBO0FBQ0E7QUFDQTtzQ0FDQTs0RUFDQTtBQUNBOzRDQUNBO21EQUNBO0FBR0E7QUFuQ0E7QUFoQkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWkE7U0FFQTtBQURBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1BBO1FBRUEsQ0FDQSxZQUdBO3VCQUNBOzt5QkFFQTtvQkFDQTtVQUNBO1lBRUE7QUFMQTtBQU1BOztvQ0FFQTt5Q0FDQSxvQ0FDQSxnQkFDQTtBQUVBO0FBTkE7O0FBT0E7O2tDQUNBO21GQUNBLDhCQUNBO2dCQUNBLFFBQ0E7d0NBQ0E7a0JBQ0E7cUJBQ0E7d0JBQ0EsMEJBQ0EsdUJBQ0E7K0JBQ0E7b0JBQ0E7Z0VBQ0E7QUFDQTtXQUNBO3dCQUNBLDBCQUNBLGFBQ0E7K0JBQ0E7b0JBQ0E7d0NBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFDQTtpQ0FDQTtBQUNBO0FBRUE7QUFDQTs7OztBQUVBOzttQ0FDQTtZQUNBLE9BQ0E7QUFDQTtxRkFDQSw4QkFDQTtrQkFDQSxRQUNBOzJDQUNBO3FCQUNBOzBEQUNBLHFDQUNBO3NCQUNBOytCQUNBOzJCQUNBLDJCQUVBLHdEQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0EsVUFDQTs7bUJBRUE7cUJBQ0E7QUFGQSwyQkFHQTtvQkFDQTs2QkFDQTt5QkFDQSwyQkFFQSwwRkFDQTtBQUVBO0FBQ0E7QUFFQTtBQXhDQTtBQXJEQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN5Q0E7QUFDQTs7QUFFQTtZQUVBOzBCQUNBOzsyQkFHQTtBQUZBO0FBR0E7OzBEQUVBOzhEQUNBO21EQUNBOzhDQUNBO0FBQ0EsdUJBQ0E7aUNBQ0E7QUFDQTtBQUNBLG1CQUNBOzBDQUNBO0FBQ0E7aURBQ0E7QUFDQTtzQ0FDQTtvSUFDQTtBQUNBO3NDQUNBO3NDQUNBLEdBQ0E7a0NBQ0E7QUFJQTtBQXpCQTs7QUFQQSxHOzs7Ozs7O0FDaEVBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBa0g7QUFDbEg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBa0g7QUFDbEg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBa0g7QUFDbEg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx3QkFBcUo7QUFDcko7QUFDQSx3QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3hCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDNUZBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQy9CQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN0R0EsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9wYWdlcy9wcm9kdWN0LWRldGFpbHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQzNik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsImxldCBkYXRhID0gd2luZG93LkxhcmF2ZWwuZGF0YTtcclxubGV0IHVzciAgPSB3aW5kb3cuTGFyYXZlbC51c2VyO1xyXG5cclxuVnVlLmNvbXBvbmVudCgncmV2aWV3JywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvcHJvZHVjdC9yZXZpZXcvcmV2aWV3LnZ1ZScpKTtcclxuVnVlLmNvbXBvbmVudCgncHJvZHVjdC1pdGVtJywgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9wcm9kdWN0L1Byb2R1Y3QudnVlJykpO1xyXG5WdWUuY29tcG9uZW50KCdyZWxhdGVkLXByb2R1Y3QtaXRlbScsIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvcHJvZHVjdC9SZWxhdGVkLVByb2R1Y3QudnVlJykpO1xyXG5WdWUuY29tcG9uZW50KCdzdGFyLXJhdGluZycsIFZ1ZVN0YXJSYXRpbmcuZGVmYXVsdCk7XHJcblxyXG52YXIgY3VycmVudF9wcm9kdWN0ID0gd2luZG93LkxhcmF2ZWwuZGF0YS5pZDtcclxudmFyIHVzcmlkID0gbnVsbDtcclxudmFyIGRvY3NWZXJpZmllZCA9IGZhbHNlO1xyXG5pZih1c3Ipe1xyXG4gICAgdXNyaWQgPSB1c3IuaWQ7XHJcbiAgICBkb2NzVmVyaWZpZWQgPSB1c3IuaXNfZG9jc192ZXJpZmllZDtcclxufVxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcbiAgICBlbDogJyNwcm9kdWN0LWRldGFpbHMtY29udGFpbmVyJyxcclxuICAgIGRhdGE6IHtcclxuICAgICAgICBvd25lcjogZGF0YS51c2VyX2lkLFxyXG4gICAgICAgIHNpdGVfdXNlcjogdXNyaWQsXHJcbiAgICAgICAgdmVyaWZpZWQ6IGRvY3NWZXJpZmllZCxcclxuICAgICAgICBjb2xvcklkOjAsXHJcbiAgICAgICAgY29sb3JTdG9jazogMCxcclxuICAgICAgICBjb2xvckRlc2lnbjogJycsXHJcbiAgICAgICAgY29sb3JJbWFnZTogJycsXHJcbiAgICAgICAgZm9ybTogbmV3IEZvcm0oXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICBjb250ZW50OicnXHJcbiAgICAgICAgICAgICAgICAsaW1hZ2VUZW1wOicnXHJcbiAgICAgICAgICAgICAgICAscHJvZHVjdF9pZDpjdXJyZW50X3Byb2R1Y3QgXHJcbiAgICAgICAgICAgICAgICAscmF0aW5nX2lkOicnXHJcbiAgICAgICAgICAgICAgICAscmF0aW5nOjFcclxuICAgICAgICAgICAgICAgICxyZXZpZXc6JydcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAseyBiYXNlVVJMOiAnaHR0cDovLycrTGFyYXZlbC5iYXNlX2FwaV91cmwgfVxyXG4gICAgICAgICAgICAse3Jlc2V0RXhjZXB0aW9uczpbJ3Byb2R1Y3RfaWQnXX1cclxuICAgICAgICApXHJcbiAgICAgICAgLGNvbW1lbnRGb3JtOiBuZXcgRm9ybShcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgY29udGVudDonJ1xyXG4gICAgICAgICAgICAscGFyZW50X2lkOjBcclxuICAgICAgICAgICAgLHByb2R1Y3RfaWQ6Y3VycmVudF9wcm9kdWN0IFxyXG4gICAgICAgICAgICAscmF0aW5nX2lkOjBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAseyBiYXNlVVJMOiAnaHR0cDovLycrTGFyYXZlbC5iYXNlX2FwaV91cmwgfVxyXG4gICAgICAgICAgICAse3Jlc2V0RXhjZXB0aW9uczpbJ3Byb2R1Y3RfaWQnXX1cclxuICAgICAgICApXHJcbiAgICAgICAgLHVzZXI6IGRhdGEudXNlclxyXG4gICAgICAgICxyZXZpZXdzOltdXHJcbiAgICAgICAgLHJldmlld0NvdW50OjBcclxuICAgICAgICAsaXRlbXM6W11cclxuICAgICAgICAsY2FydGxhbmc6W11cclxuICAgICAgICAsdmFyY29sb3I6IGRhdGEucHJvZHVjdF9jb2xvcl92YXJpYXRpb25zXHJcbiAgICAgICAgLHZhcnNpemU6IGRhdGEucHJvZHVjdF9zaXplX3ZhcmlhdGlvbnNcclxuICAgICAgICAsZGVzY3JpcHRpb246IGRhdGEuZGVzY3JpcHRpb25cclxuICAgICAgICAsY2hEZXNjcmlwdGlvbjogZGF0YS5jbl9kZXNjcmlwdGlvblxyXG4gICAgICAgICx1cmw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCsnLycrZGF0YS5pbWFnZV9wYXRoX2NvbG9yXHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQoKXtcclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0RmVlZGJhY2tUcmFuc2xhdGlvbigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zLmdldCgnL3RyYW5zbGF0ZS9mZWVkYmFjaycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0Q2FydFRyYW5zbGF0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2NhcnQnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGdldFByb2R1Y3RUcmFuc2xhdGlvbigpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zLmdldCgnL3RyYW5zbGF0ZS9wcm9kdWN0cycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0UmVsYXRlZEl0ZW1zKCl7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLnBvc3QoJy9wcm9kdWN0L2dldC1yZWxhdGVkLXByb2R1Y3RzJyx7XHJcbiAgICAgICAgICAgICAgICBwcm9kdWN0OmN1cnJlbnRfcHJvZHVjdFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGdldFJldmlld3MoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL3Byb2R1Y3QtcmV2aWV3cz9wcm9kdWN0PScrY3VycmVudF9wcm9kdWN0KycmY291bnQ9MicpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0UmV2aWV3Q291bnQoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL3Byb2R1Y3QtcmV2aWV3L2NvdW50P3Byb2R1Y3Q9JytjdXJyZW50X3Byb2R1Y3QpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0Q2FydCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvdmlldycpOyBcclxuICAgICAgICB9ICAgICBcclxuXHJcbiAgICAgICAgYXhpb3MuYWxsKFtcclxuICAgICAgICAgICAgZ2V0RmVlZGJhY2tUcmFuc2xhdGlvbigpXHJcbiAgICAgICAgICAgICxnZXRDYXJ0VHJhbnNsYXRpb24oKVxyXG4gICAgICAgICAgICAsZ2V0UHJvZHVjdFRyYW5zbGF0aW9uKClcclxuICAgICAgICAgICAgLGdldFJldmlld3MoKVxyXG4gICAgICAgICAgICAsZ2V0UmV2aWV3Q291bnQoKVxyXG4gICAgICAgICAgICAsZ2V0Q2FydCgpXHJcbiAgICAgICAgICAgICxnZXRSZWxhdGVkSXRlbXMoKVxyXG4gICAgICAgIF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG4gICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICBmZWVkYmFja3RyYW5zbGF0aW9uXHJcbiAgICAgICAgICAgICAgICAsY2FydHRyYW5zbGF0aW9uXHJcbiAgICAgICAgICAgICAgICAscHJvZHVjdHRyYW5zbGF0aW9uXHJcbiAgICAgICAgICAgICAgICAscmV2aWV3c1xyXG4gICAgICAgICAgICAgICAgLHJldmlld0NvdW50XHJcbiAgICAgICAgICAgICAgICAsY2FydFxyXG4gICAgICAgICAgICAgICAgLHJlbGF0ZWRcclxuICAgICAgICAgICAgKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxhbmcgPSBmZWVkYmFja3RyYW5zbGF0aW9uLmRhdGEuZGF0YTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2FydGxhbmcgPSBjYXJ0dHJhbnNsYXRpb24uZGF0YS5kYXRhO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9kdWN0bGFuZyA9IHByb2R1Y3R0cmFuc2xhdGlvbi5kYXRhLmRhdGE7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJldmlld3MgPSByZXZpZXdzLnN0YXR1cyA9PSAyMDA/IHJldmlld3MuZGF0YTpbXTtcclxuICAgICAgICAgICAgICAgIHRoaXMucmV2aWV3Q291bnQgPSByZXZpZXdDb3VudC5zdGF0dXMgPT0gMjAwPyByZXZpZXdDb3VudC5kYXRhOjA7XHJcbiAgICAgICAgICAgICAgICB0aGlzLml0ZW1zID0gcmVsYXRlZC5zdGF0dXMgPT0gMjAwPyByZWxhdGVkLmRhdGE6W107XHJcblxyXG4gICAgICAgICAgICAgICAgaWYodGhpcy52YXJjb2xvci5sZW5ndGggIT0gMCl7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb2xvclN0b2NrID0gdGhpcy52YXJjb2xvclswXS5zdG9jaztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbG9yRGVzaWduID0gdGhpcy52YXJjb2xvclswXS5sYWJlbDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbG9ySW1hZ2UgPSB0aGlzLnVybCArIHRoaXMudmFyY29sb3JbMF0uaW1hZ2VfbmFtZSArICcuanBnJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIH0pKVxyXG4gICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG5cclxuICAgICAgICBFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQuc3VibWl0RmVlZGJhY2tJbWFnZScsIChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybS5pbWFnZVRlbXAgPSBkYXRhO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBtZXRob2RzOntcclxuICAgICAgICBzdWJtaXRGZWVkYmFjaygpe1xyXG4gICAgICAgICAgICAvL3RoaXMuZm9ybS5yYXRpbmdfaWQgPSB0aGlzLmZvcm0ucmF0aW5nID09IDAgPyAnJzp0aGlzLmZvcm0ucmF0aW5nO1xyXG4gICAgICAgICAgICB0aGlzLmZvcm0ucmF0aW5nX2lkID0gMTtcclxuICAgICAgICAgICAgdGhpcy5mb3JtLnN1Ym1pdCgncG9zdCcsICcvdjEvcHJvZHVjdC1yZXZpZXdzJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LnN1Y2Nlc3MpXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJldmlld3MudW5zaGlmdChyZXN1bHQuZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKCcnLHRoaXMucHJvZHVjdGxhbmdbJ2ZlZWRiYWNrJ10pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybS5pbWFnZVRlbXAgPSAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy90aGlzLmZvcm0ucmF0aW5nID0gMztcclxuICAgICAgICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMucHJvZHVjdGxhbmdbJ3BsZWFzZS10cnknXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGhhbmRsZUNtZEVudGVyOiBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBpZiAoKGUubWV0YUtleSB8fCBlLmN0cmxLZXkpICYmIGUua2V5Q29kZSA9PSAxMykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJtaXRGZWVkYmFjaygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc3VibWl0Q29tbWVudCgpe1xyXG4gICAgICAgICAgICB2YXIgaW5kZXg7XHJcbiAgICAgICAgICAgIGZvcih2YXIgaT0wOyBpPHRoaXMucmV2aWV3cy5sZW5ndGg7IGkrKylcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5yZXZpZXdzW2ldLmlkID09IHRoaXMuY29tbWVudEZvcm0ucGFyZW50X2lkKVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGluZGV4ID0gaTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRGb3JtLnJhdGluZ19pZCA9IDA7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRGb3JtLnN1Ym1pdCgncG9zdCcsICcvdjEvcHJvZHVjdC1yZXZpZXdzJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LnN1Y2Nlc3MpXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJldmlld3NbaW5kZXhdLmNvbW1lbnRzLnVuc2hpZnQocmVzdWx0LmRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2VzcygnJyx0aGlzLnByb2R1Y3RsYW5nWydjb21tZW50J10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcjbW9kYWxDb21tZW50cycpLm1vZGFsKCd0b2dnbGUnKTtcclxuICAgICAgICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMucHJvZHVjdGxhbmdbJ3BsZWFzZS10cnknXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGxvYWRNb3JlUmV2aWV3cygpe1xyXG4gICAgICAgICAgICBmdW5jdGlvbiBnZXRSZXZpZXdzKHJldmlld19jb3VudCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvcHJvZHVjdC1yZXZpZXdzP3Byb2R1Y3Q9JytjdXJyZW50X3Byb2R1Y3QrJyZjb3VudD0yJnNraXA9JytyZXZpZXdfY291bnQpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBheGlvcy5hbGwoW1xyXG4gICAgICAgICAgICAgICAgIGdldFJldmlld3ModGhpcy5yZXZpZXdzLmxlbmd0aClcclxuICAgICAgICAgICAgXSkudGhlbihheGlvcy5zcHJlYWQoXHJcbiAgICAgICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAgICAgIHJldmlld3NcclxuICAgICAgICAgICAgICAgICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJldmlld3Muc3RhdHVzID09IDIwMClcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmV2aWV3cyA9IHRoaXMucmV2aWV3cy5jb25jYXQocmV2aWV3cy5kYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pKVxyXG4gICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjaG9vc2VDb2xvcih2YXJjbyl7XHJcbiAgICAgICAgICAgICQoXCIjcXR5XCIpLnZhbCgnMScpO1xyXG4gICAgICAgICAgICB0aGlzLmNvbG9ySWQgPSB2YXJjby5pZDtcclxuICAgICAgICAgICAgdGhpcy5jb2xvclN0b2NrID0gdmFyY28uc3RvY2s7XHJcbiAgICAgICAgICAgIHRoaXMuY29sb3JEZXNpZ24gPSB2YXJjby5sYWJlbDtcclxuICAgICAgICAgICAgdGhpcy5jb2xvckltYWdlID0gdGhpcy51cmwgKyB2YXJjby5pbWFnZV9uYW1lICsgJy5qcGcnO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIEFkZFRvV2lzaGxpc3QoaWQpe1xyXG4gICAgICAgICAgICBpZihMYXJhdmVsLmlzQXV0aGVudGljYXRlZCA9PT0gdHJ1ZSl7XHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2FkZHRvZmF2b3JpdGVzLycgKyBpZClcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LmRhdGEucmVzcG9uc2U9PTEpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgZmF2ID0gcGFyc2VJbnQoJChcIiNmYXYtY291bnRcIikudGV4dCgpKSsxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2Zhdi1jb3VudFwiKS50ZXh0KGZhdikuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmNhcnRsYW5nWydhZGRlZC1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0yKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5jYXJ0bGFuZ1snbm90LWFkZGVkLXdpc2hsaXN0J10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5pbmZvKHRoaXMuY2FydGxhbmdbJ2FscmVhZHktZmF2b3JpdGVzJ10sIG5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmNhcnRsYW5nWydsb2dpbi1mYXZvcml0ZXMnXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBhZGRUb0NhcnQyKGlkKXtcclxuXHJcbiAgICAgICAgICAgIHZhciBxdHkgPSAkKCcjcXR5JykudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBzZWxjb2xvciA9IHRoaXMuY29sb3JEZXNpZ247XHJcbiAgICAgICAgICAgIHZhciBzaXplID0gJChcIiNnU2l6ZSBvcHRpb246c2VsZWN0ZWRcIikudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBzZWxzaXplID0gJChcIiNnU2l6ZSBvcHRpb246c2VsZWN0ZWRcIikudGV4dCgpO1xyXG5cclxuICAgICAgICAgICAgaWYodGhpcy52YXJjb2xvci5sZW5ndGggPT0gMCAmJiB0aGlzLnZhcnNpemUubGVuZ3RoID09IDApe1xyXG5cclxuICAgICAgICAgICAgICAgIGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvYWRkMicse1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9kX2lkOmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdHk6cXR5XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJChcIiNjYXJ0LWNvdW50LCAjY2FydC1jb3VudC1tb2JpbGVcIikudGV4dChyZXN1bHQuZGF0YS5jYXJ0X2l0ZW1fY291bnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLmVycm9yX21zZykgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcihyZXN1bHQuZGF0YS5lcnJvcl9tc2csIHRoaXMuY2FydGxhbmdbJ25vdC1hZGRlZCddKTtcclxuICAgICAgICAgICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMuY2FydGxhbmdbJ2FkZGVkLWNhcnQnXSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L3ZpZXcnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAkKCcjY2FydC1jb3VudCwgI2NhcnQtY291bnQtbW9iaWxlJykudGV4dChyZXN1bHQuZGF0YS5jYXJ0X2l0ZW1fY291bnQpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9LCAxMDAwKTtcclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYoc2l6ZSA9PSAnc2l6ZScpe1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmNhcnRsYW5nWydzZWxlY3Qtc2l6ZSddLCB0aGlzLmNhcnRsYW5nWydub3QtYWRkZWQnXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2FkZDInLHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kX2lkOmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcXR5OnF0eSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOnNlbGNvbG9yLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3JJZDp0aGlzLmNvbG9ySWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvclN0b2NrOnRoaXMuY29sb3JTdG9jayxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9ySW1hZ2U6dGhpcy5jb2xvckltYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZTpzZWxzaXplXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjY2FydC1jb3VudCwgI2NhcnQtY291bnQtbW9iaWxlXCIpLnRleHQocmVzdWx0LmRhdGEuY2FydF9pdGVtX2NvdW50KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LmRhdGEuZXJyb3JfbXNnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5jYXJ0bGFuZ1snbm90LWFkZGVkJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmNhcnRsYW5nWydhZGRlZC1jYXJ0J10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICAgICBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L3ZpZXcnKVxyXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJyNjYXJ0LWNvdW50LCAjY2FydC1jb3VudC1tb2JpbGUnKS50ZXh0KHJlc3VsdC5kYXRhLmNhcnRfaXRlbV9jb3VudCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgMTAwMCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgYnV5Tm93KGlkKXtcclxuXHJcbiAgICAgICAgICAgIHZhciBxdHkgPSAkKCcjcXR5JykudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBzZWxjb2xvciA9IHRoaXMuY29sb3JEZXNpZ247XHJcbiAgICAgICAgICAgIHZhciBzaXplID0gJChcIiNnU2l6ZSBvcHRpb246c2VsZWN0ZWRcIikudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBzZWxzaXplID0gJChcIiNnU2l6ZSBvcHRpb246c2VsZWN0ZWRcIikudGV4dCgpO1xyXG5cclxuICAgICAgICAgICAgYXhpb3MuZ2V0KGxvY2F0aW9uLm9yaWdpbiArICcvY2FydC92aWV3JylcclxuICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIHZhciBjYXJ0MiA9ICQubWFwKHJlc3VsdC5kYXRhLmNhcnQsIGZ1bmN0aW9uKHZhbHVlLCBpbmRleCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbdmFsdWVdO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIGNhcnQzID0gY2FydDIuZmluZChpdGVtID0+IGl0ZW0uaWQgPT0gY3VycmVudF9wcm9kdWN0KTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLnZhcmNvbG9yLmxlbmd0aCA9PSAwICYmIHRoaXMudmFyc2l6ZS5sZW5ndGggPT0gMCl7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoY2FydDMgPT0gdW5kZWZpbmVkKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXhpb3MuZ2V0KGxvY2F0aW9uLm9yaWdpbiArICcvY2FydC9hZGQyJyx7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kX2lkOmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF0eTpxdHlcclxuICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2cpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5jYXJ0bGFuZ1snY2FudC1ib3VnaHQnXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5hc3NpZ24oXCIvY2FydFwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXhpb3MuZ2V0KGxvY2F0aW9uLm9yaWdpbiArICcvY2FydC91cGRhdGUnLHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2RfaWQ6aWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93X2lkOmNhcnQzLnJvd0lkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF0eTpxdHlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmFzc2lnbihcIi9jYXJ0XCIpOyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgMTAwMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYoc2l6ZSA9PSAnc2l6ZScpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5jYXJ0bGFuZ1snc2VsZWN0LXNpemUnXSwgdGhpcy5jYXJ0bGFuZ1snbm90LWFkZGVkJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKGNhcnQzID09IHVuZGVmaW5lZCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2FkZDInLHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZF9pZDppZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcXR5OnF0eSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6c2VsY29sb3IsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9ySWQ6dGhpcy5jb2xvcklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvclN0b2NrOnRoaXMuY29sb3JTdG9jayxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3JJbWFnZTp0aGlzLmNvbG9ySW1hZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNpemU6c2Vsc2l6ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLmVycm9yX21zZylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcihyZXN1bHQuZGF0YS5lcnJvcl9tc2csIHRoaXMuY2FydGxhbmdbJ2NhbnQtYm91Z2h0J10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uYXNzaWduKFwiL2NhcnRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGFyciA9ICQuZ3JlcChjYXJ0MiwgZnVuY3Rpb24oIGEgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhLmlkID09IGN1cnJlbnRfcHJvZHVjdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcnIyID0gYXJyLmZpbmQoaXRlbSA9PiBpdGVtLm9wdGlvbnMuc2l6ZSA9PSBzZWxzaXplKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihzZWxjb2xvciAhPSAnY29sb3InICYmIHNpemUgIT0gJ3NpemUnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcnIyID0gYXJyLmZpbmQoaXRlbSA9PiBpdGVtLm9wdGlvbnMuY29sb3IgPT0gc2VsY29sb3IgJiYgaXRlbS5vcHRpb25zLnNpemUgPT0gc2Vsc2l6ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIGlmKHNlbGNvbG9yICE9ICdjb2xvcicpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGFycjIgPSBhcnIuZmluZChpdGVtID0+IGl0ZW0ub3B0aW9ucy5jb2xvciA9PSBzZWxjb2xvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIGlmKHNpemUgIT0gJ3NpemUnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcnIyID0gYXJyLmZpbmQoaXRlbSA9PiBpdGVtLm9wdGlvbnMuc2l6ZSA9PSBzZWxzaXplKTsgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGFycjIpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvdXBkYXRlJyx7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZF9pZDppZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd19pZDphcnIyLnJvd0lkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcXR5OnF0eSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOnNlbGNvbG9yLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3JJZDp0aGlzLmNvbG9ySWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvclN0b2NrOnRoaXMuY29sb3JTdG9jayxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9ySW1hZ2U6dGhpcy5jb2xvckltYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZTpzZWxzaXplXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpeyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmFzc2lnbihcIi9jYXJ0XCIpOyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAxMDAwKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvYWRkMicse1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2RfaWQ6aWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdHk6cXR5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6c2VsY29sb3IsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcklkOnRoaXMuY29sb3JJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yU3RvY2s6dGhpcy5jb2xvclN0b2NrLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3JJbWFnZTp0aGlzLmNvbG9ySW1hZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaXplOnNlbHNpemVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjYXJ0LWNvdW50LCAjY2FydC1jb3VudC1tb2JpbGVcIikudGV4dChyZXN1bHQuZGF0YS5jYXJ0X2l0ZW1fY291bnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2cpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IocmVzdWx0LmRhdGEuZXJyb3JfbXNnLCB0aGlzLmNhcnRsYW5nWydub3QtYWRkZWQnXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5hc3NpZ24oXCIvY2FydFwiKTsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAxMDAwKTsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBhZGRUb0NhcnQoaWQsbmFtZSl7XHJcbiAgICAgICAgICAgIGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvYWRkJyx7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBwcm9kX2lkOmlkLFxyXG4gICAgICAgICAgICAgICAgICAgIHF0eToxXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAkKFwiI2NhcnQtY291bnQsICNjYXJ0LWNvdW50LW1vYmlsZVwiKS50ZXh0KHJlc3VsdC5kYXRhLmNhcnRfaXRlbV9jb3VudCk7XHJcbiAgICAgICAgICAgICAgICBpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2cpXHJcbiAgICAgICAgICAgICAgICB7ICAgXHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LmRhdGEuZXJyb3JfbXNnPT0nUmVkaXJlY3QnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24gPSAnL3Byb2R1Y3QvJyArIHJlc3VsdC5kYXRhLnNsdWc7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IocmVzdWx0LmRhdGEuZXJyb3JfbXNnLCB0aGlzLmNhcnRsYW5nWydub3QtYWRkZWQnXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMuY2FydGxhbmdbJ2FkZGVkLWNhcnQnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYWRkVG9Db21wYXJlKGlkLG5hbWUpe1xyXG4gICAgICAgICAgICBpZihMYXJhdmVsLmlzQXV0aGVudGljYXRlZCA9PT0gdHJ1ZSl7XHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2FkZHRvY29tcGFyZS8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNvbXAgPSBwYXJzZUludCgkKFwiI2NvbXAtY291bnRcIikudGV4dCgpKSsxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NvbXAtY291bnRcIikudGV4dChjb21wKS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMuY2FydGxhbmdbJ2FkZGVkLWNvbXBhcmlzb24nXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmNhcnRsYW5nWydmdWxsJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5pbmZvKHRoaXMuY2FydGxhbmdbJ2FscmVhZHktY29tcGFyZSddLCBuYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmNhcnRsYW5nWydsb2dpbi1jb21wYXJlJ10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBhZGRUb0Zhdm9yaXRlcyhpZCxuYW1lKXtcclxuICAgICAgICAgICAgaWYoTGFyYXZlbC5pc0F1dGhlbnRpY2F0ZWQgPT09IHRydWUpe1xyXG4gICAgICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9hZGR0b2Zhdm9yaXRlcy8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGZhdiA9IHBhcnNlSW50KCQoXCIjZmF2LWNvdW50XCIpLnRleHQoKSkrMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNmYXYtY291bnRcIikudGV4dChmYXYpLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5jYXJ0bGFuZ1snYWRkZWQtZmF2b3JpdGVzJ10sIG5hbWUpOyBcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuaW5mbyh0aGlzLmNhcnRsYW5nWydhbHJlYWR5LWZhdm9yaXRlcyddLCBuYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmNhcnRsYW5nWydsb2dpbi1mYXZvcml0ZXMnXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjb250YWN0TWUoKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgY2hhdCA9IFtdO1xyXG4gICAgICAgICAgICAkKFwiLnBhbmVsLWNoYXQtYm94XCIpLmVhY2goZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgIGNoYXQucHVzaCgkKHRoaXMpLmF0dHIoJ2lkJykpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGlmKCQuaW5BcnJheSgndXNlcicrdGhpcy51c2VyLmlkLGNoYXQpPT0nLTEnKVxyXG4gICAgICAgICAgICAgICAgRXZlbnQuZmlyZSgnY2hhdC1ib3guc2hvdycsIHRoaXMudXNlcik7XHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgJCgnLmpzLWF1dG8tc2l6ZScpLnRleHRhcmVhQXV0b1NpemUoKTtcclxuICAgICAgICAgICAgfSwgNTAwMCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgbm90VmVyaWZpZWQoKXtcclxuICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMucHJvZHVjdGxhbmdbJ3Byb2R1Y3Qtb3duZXInXSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2hvd1JlcG9ydE1vZGFsKCl7XHJcbiAgICAgICAgICAgICQoJyNtb2RhbFByb2RSZXBvcnQnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY2FudFJlcG9ydCgpe1xyXG4gICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5wcm9kdWN0bGFuZ1snY2FudC1yZXBvcnQnXSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgbmVlZExvZ2luKCl7XHJcbiAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLnByb2R1Y3RsYW5nWydsb2dpbi1maXJzdCddKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6e1xyXG4gICAgICAgIHN0b3JlbmFtZSgpe1xyXG4gICAgICAgICAgdmFyIG5hbWUgPSB3aW5kb3cuTGFyYXZlbC5kYXRhLnN0b3JlLmNuX25hbWUgPyB3aW5kb3cuTGFyYXZlbC5kYXRhLnN0b3JlLmNuX25hbWU6IHdpbmRvdy5MYXJhdmVsLmRhdGEuc3RvcmUubmFtZTtcclxuICAgICAgICAgIHZhciBtID0gJChcIm1ldGFbbmFtZT1sb2NhbGUtbGFuZ11cIik7ICAgIFxyXG4gICAgICAgICAgdmFyIGxhbmdtb2RlID0gbS5hdHRyKFwiY29udGVudFwiKTtcclxuXHJcbiAgICAgICAgICBpZihsYW5nbW9kZT09XCJjblwiKVxyXG4gICAgICAgICAgICByZXR1cm4gbmFtZTtcclxuICAgICAgICAgIGVsc2UgXHJcbiAgICAgICAgICAgIHJldHVybiB3aW5kb3cuTGFyYXZlbC5kYXRhLnN0b3JlLm5hbWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTtcclxuXHJcblxyXG4kKGZ1bmN0aW9uKCl7XHJcbiAgLy8gb3dsY2Fyb3NlbCAoaWYgaXRlbXMgbGVzcyB0aGFuIDQsIGhpZGUgbmF2LCBkaXNhYmxlIGRyYWcsIGhpZGUgdG91Y2gpXHJcbiAgdmFyIHByb2R1Y3RzX3NsaWRlcl9kZXRhaWwgPSAkKCcucHJvZHVjdHMtc2xpZGVyLWRldGFpbCcpO1xyXG4gIHZhciBpdGVtX2NvdW50ID0gJCgnLnByb2R1Y3RzLXNsaWRlci1kZXRhaWwgYScpLmxlbmd0aDtcclxuICBwcm9kdWN0c19zbGlkZXJfZGV0YWlsLm93bENhcm91c2VsKHtcclxuICAgIG1hcmdpbjo1LFxyXG4gICAgZG90czpmYWxzZSxcclxuICAgIG5hdjppdGVtX2NvdW50IDwgNSA/IGZhbHNlIDogdHJ1ZSxcclxuICAgIG1vdXNlRHJhZzppdGVtX2NvdW50IDwgNSA/IGZhbHNlIDogdHJ1ZSxcclxuICAgIHRvdWNoRHJhZzppdGVtX2NvdW50IDwgNSA/IGZhbHNlIDogdHJ1ZSxcclxuICAgIG5hdlRleHQ6Wyc8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLWxlZnRcIj48L2k+JywnPGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1yaWdodFwiPjwvaT4nXSxcclxuICAgIHJlc3BvbnNpdmU6e1xyXG4gICAgICAgIDA6eyBpdGVtczo0LCB9XHJcbiAgICB9XHJcbiAgfSk7XHJcbiAgJCgnLnByb2R1Y3RzLXNsaWRlci1kZXRhaWwgYScpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICB2YXIgc3JjID0gJCh0aGlzKS5maW5kKCdpbWcnKS5hdHRyKCdzcmMnKTtcclxuICAgIHZhciB6b29tID0gJCh0aGlzKS5maW5kKCdpbWcnKS5hdHRyKCdkYXRhLXpvb20taW1hZ2UnKTtcclxuICAgIHZhciBkZXRhaWwgPSAkKHRoaXMpLnBhcmVudCgpLnBhcmVudCgpLnBhcmVudCgpLnBhcmVudCgpLnBhcmVudCgpLmZpbmQoJy5pbWFnZS1kZXRhaWwgaW1nJyk7XHJcbiAgICBkZXRhaWwuYXR0cignc3JjJyxzcmMpO1xyXG4gICAgZGV0YWlsLmF0dHIoJ2RhdGEtem9vbS1pbWFnZScsem9vbSk7XHJcbiAgICAkKCcuem9vbVdpbmRvdycpLmNzcygnYmFja2dyb3VuZC1pbWFnZScsICd1cmwoXCInICsgem9vbSArICdcIiknKTtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9KTtcclxuXHJcbiAgJCgnI3F0eS1tYXIgLmZvcm0tZ3JvdXAnKS5jc3MoXCJtYXJnaW5cIixcIjBcIik7XHJcblxyXG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3Byb2R1Y3QtZGV0YWlscy5qcyIsIjx0ZW1wbGF0ZT5cclxuICA8ZGl2IGNsYXNzPVwibWl4IGNvbC1zbS0zXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwiYm94LXByb2R1Y3Qtb3V0ZXJcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImJveC1wcm9kdWN0XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImltZy13cmFwcGVyXCI+XHJcbiAgICAgICAgICA8YSA6aHJlZj1cIicvcHJvZHVjdC8nICsgaXRlbS5zbHVnXCI+XHJcbiAgICAgICAgICAgIDxpbWcgYWx0PVwiUHJvZHVjdFwiIHYtYmluZDpzcmM9XCInL3Nob3BwaW5nL2ltYWdlcy9wcm9kdWN0LycraXRlbS5pZCsnL3ByaW1hcnkuanBnJ1wiIG9uRXJyb3I9XCJ0aGlzLnNyYyA9ICcvaW1hZ2VzL2F2YXRhci9kZWZhdWx0LXByb2R1Y3QuanBnJ1wiPlxyXG4gICAgICAgICAgICA8IS0tIDxpbWcgYWx0PVwiUHJvZHVjdFwiIHYtYmluZDpzcmM9XCJpdGVtLmltYWdlXCIgb25FcnJvcj1cInRoaXMuc3JjID0gJy9pbWFnZXMvYXZhdGFyL2RlZmF1bHQtcHJvZHVjdC5qcGcnXCI+IC0tPlxyXG4gICAgICAgICAgICA8IS0tIG9uRXJyb3I9XCJ0aGlzLnNyYyA9ICcvaW1hZ2VzL2F2YXRhci9kZWZhdWx0LXByb2R1Y3QuanBnJ1wiIC0tPlxyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cInRhZ3NcIiB2LWlmPSdpdGVtLnNhbGVfcHJpY2UnPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImxhYmVsLXRhZ3NcIj48c3BhbiBjbGFzcz1cImxhYmVsIGxhYmVsLWRhbmdlciBhcnJvd2VkXCI+e3tpdGVtLmRpc2NvdW50fX0lPC9zcGFuPjwvc3Bhbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cIm9wdGlvblwiPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIiB0aXRsZT1cIkFkZCB0byBDYXJ0XCIgZGF0YS1vcmlnaW5hbC10aXRsZT1cIkFkZCB0byBDYXJ0XCIgQGNsaWNrPVwiYWRkVG9DYXJ0KClcIj48aSBjbGFzcz1cImZhIGZhLXNob3BwaW5nLWNhcnRcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgdGl0bGU9XCJBZGQgdG8gQ29tcGFyZVwiPjxpIGNsYXNzPVwiZmEgZmEtYWxpZ24tbGVmdFwiIEBjbGljaz1cImFkZFRvQ29tcGFyZSgpXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiQWRkIHRvIFdpc2hsaXN0XCIgY2xhc3M9XCJ3aXNobGlzdFwiIEBjbGljaz1cImFkZFRvRmF2b3JpdGVzKClcIj48aSBjbGFzcz1cImZhIGZhLWhlYXJ0XCI+PC9pPjwvYT5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxoNj48YSA6aHJlZj1cIicvcHJvZHVjdC8nICsgaXRlbS5zbHVnXCI+e3tuYW1lbGVuZ3RofX08L2E+PC9oNj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwicHJpY2VcIj5cclxuICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgIDxzcGFuIHYtaWY9J2l0ZW0uc2FsZV9wcmljZSc+e3twYXJzZUZsb2F0KFwiMFwiK3NhbGVwcmljZSkgfCBjdXJyZW5jeSB9fTwvc3Bhbj4gXHJcbiAgICAgICAgICAgIDxzcGFuIDpjbGFzcz1cIml0ZW0uc2FsZV9wcmljZT8ncHJpY2Utb2xkJzonJ1wiPnt7cGFyc2VGbG9hdChcIjBcIitwcmljZSkgfCBjdXJyZW5jeX19PC9zcGFuPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJhdGluZy1ibG9ja1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3Rhci1yYXRpbmdzLXNwcml0ZS1zbWFsbFwiPjxzcGFuIDpzdHlsZT1cInByb2R1Y3RSYXRpbmdcIiBjbGFzcz1cInN0YXItcmF0aW5ncy1zcHJpdGUtc21hbGwtcmF0aW5nXCI+PC9zcGFuPjwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2Plx0XHJcbjwvdGVtcGxhdGU+XHJcblxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cdHByb3BzOiBbJ2l0ZW0nXSxcclxuXHRtZXRob2RzOntcclxuXHRcdHNlbGVjdChpZCl7XHJcblx0XHRcdCB0aGlzLiRwYXJlbnQuc2VsZWN0SXRlbShpZClcclxuXHRcdH0sXHJcbiAgICBhZGRUb0NhcnQoKXtcclxuICAgICAgIHRoaXMuJHBhcmVudC5hZGRUb0NhcnQodGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5uYW1lKVxyXG4gICAgfSxcclxuICAgIGFkZFRvQ29tcGFyZSgpe1xyXG4gICAgICAgdGhpcy4kcGFyZW50LmFkZFRvQ29tcGFyZSh0aGlzLml0ZW0uaWQsdGhpcy5pdGVtLm5hbWUpXHJcbiAgICB9LFxyXG4gICAgYWRkVG9GYXZvcml0ZXMoKXtcclxuICAgICAgIHRoaXMuJHBhcmVudC5hZGRUb0Zhdm9yaXRlcyh0aGlzLml0ZW0uaWQsdGhpcy5pdGVtLm5hbWUpXHJcbiAgICB9XHJcblx0fSxcclxuICBjb21wdXRlZDp7XHJcbiAgICBwcmljZSgpe1xyXG4gICAgICBpZih0aGlzLml0ZW0uZmVlX2luY2x1ZGVkPT0xKVxyXG4gICAgICAgIHJldHVybiBwYXJzZUZsb2F0KHRoaXMuaXRlbS5wcmljZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0uY2hhcmdlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLnZhdCk7XHJcbiAgICAgIGVsc2VcclxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtLnByaWNlO1xyXG4gICAgfSxcclxuICAgIHNhbGVwcmljZSgpe1xyXG4gICAgICBpZih0aGlzLml0ZW0uZmVlX2luY2x1ZGVkPT0xKVxyXG4gICAgICAgIHJldHVybiBwYXJzZUZsb2F0KHRoaXMuaXRlbS5zYWxlX3ByaWNlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLnNoaXBwaW5nX2ZlZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS5jaGFyZ2UpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0udmF0KTtcclxuICAgICAgZWxzZVxyXG4gICAgICAgIHJldHVybiB0aGlzLml0ZW0uc2FsZV9wcmljZTtcclxuICAgIH0sXHJcbiAgICBuYW1lbGVuZ3RoKCl7XHJcbiAgICAgIHZhciBuYW1lID0gdGhpcy5pdGVtLmNuX25hbWUgPyB0aGlzLml0ZW0uY25fbmFtZTogdGhpcy5pdGVtLm5hbWU7XHJcbiAgICAgIGlmICh0aGlzLml0ZW0ubmFtZS5sZW5ndGggPiA0NSkge1xyXG4gICAgICAgIGlmKHRoaXMuJHBhcmVudC5sYW5nbW9kZT09XCJjblwiKXtcclxuICAgICAgICAgICAgcmV0dXJuIG5hbWUuc3Vic3RyKDAsIDQ1KStcIi4uLlwiOyAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLml0ZW0ubmFtZS5zdWJzdHIoMCwgNDUpK1wiLi4uXCI7XHJcbiAgICAgIH1cclxuICAgICAgZWxzZXtcclxuICAgICAgICBpZih0aGlzLiRwYXJlbnQubGFuZ21vZGU9PVwiY25cIil7XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lOyAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLml0ZW0ubmFtZTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHN0YXJSYXRpbmcoKXtcclxuICAgICAgcmV0dXJuIFwid2lkdGg6XCIrTWF0aC5yb3VuZCgodGhpcy5pdGVtLnJhdGUvdGhpcy5pdGVtLmNvdW50KSoyMCkrXCIlXCI7XHJcbiAgICB9LFxyXG4gICAgcHJvZHVjdFJhdGluZygpe1xyXG4gICAgICByZXR1cm4gXCJ3aWR0aDpcIit0aGlzLml0ZW0ucHJvZHVjdF9yYXRpbmcrXCIlXCI7XHJcbiAgICB9LFxyXG5cclxuICB9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFByb2R1Y3QudnVlPzI2ZWNkNDg0IiwiPHRlbXBsYXRlPlxyXG4gIDxkaXYgY2xhc3M9XCJtaXggY29sLXNtLTMgY29sLXhzLTZcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJib3gtcHJvZHVjdC1vdXRlclwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiYm94LXByb2R1Y3RcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiaW1nLXdyYXBwZXJcIj5cclxuICAgICAgICAgIDxhIDpocmVmPVwiJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcIj5cclxuICAgICAgICAgICAgPGltZyBhbHQ9XCJQcm9kdWN0XCIgdi1iaW5kOnNyYz1cIml0ZW0uaW1hZ2VcIiBvbkVycm9yPVwidGhpcy5zcmMgPSAnL2ltYWdlcy9hdmF0YXIvZGVmYXVsdC1wcm9kdWN0LmpwZydcIj5cclxuICAgICAgICAgICAgPCEtLSBvbkVycm9yPVwidGhpcy5zcmMgPSAnL2ltYWdlcy9hdmF0YXIvZGVmYXVsdC1wcm9kdWN0LmpwZydcIiAtLT5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0YWdzXCIgdi1pZj0naXRlbS5zYWxlX3ByaWNlJz5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJsYWJlbC10YWdzXCI+PHNwYW4gY2xhc3M9XCJsYWJlbCBsYWJlbC1kYW5nZXIgYXJyb3dlZFwiPnt7aXRlbS5kaXNjb3VudH19JTwvc3Bhbj48L3NwYW4+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJvcHRpb25cIj5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIGRhdGEtcGxhY2VtZW50PVwidG9wXCIgdGl0bGU9XCJBZGQgdG8gQ2FydFwiIGRhdGEtb3JpZ2luYWwtdGl0bGU9XCJBZGQgdG8gQ2FydFwiIEBjbGljaz1cImFkZFRvQ2FydCgpXCI+PGkgY2xhc3M9XCJmYSBmYS1zaG9wcGluZy1jYXJ0XCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiQWRkIHRvIENvbXBhcmVcIj48aSBjbGFzcz1cImZhIGZhLWFsaWduLWxlZnRcIiBAY2xpY2s9XCJhZGRUb0NvbXBhcmUoKVwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiB0aXRsZT1cIkFkZCB0byBXaXNobGlzdFwiIGNsYXNzPVwid2lzaGxpc3RcIiBAY2xpY2s9XCJhZGRUb0Zhdm9yaXRlcygpXCI+PGkgY2xhc3M9XCJmYSBmYS1oZWFydFwiPjwvaT48L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8aDY+PGEgOmhyZWY9XCInL3Byb2R1Y3QvJyArIGl0ZW0uc2x1Z1wiPnt7bmFtZWxlbmd0aH19PC9hPjwvaDY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInByaWNlXCI+XHJcbiAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICA8c3BhbiB2LWlmPSdpdGVtLnNhbGVfcHJpY2UnPnt7cGFyc2VGbG9hdChcIjBcIitzYWxlcHJpY2UpIHwgY3VycmVuY3kgfX08L3NwYW4+IFxyXG4gICAgICAgICAgICA8c3BhbiA6Y2xhc3M9XCJpdGVtLnNhbGVfcHJpY2U/J3ByaWNlLW9sZCc6JydcIj57e3BhcnNlRmxvYXQoXCIwXCIrcHJpY2UpIHwgY3VycmVuY3l9fTwvc3Bhbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJyYXRpbmctYmxvY2tcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN0YXItcmF0aW5ncy1zcHJpdGUtc21hbGxcIj48c3BhbiA6c3R5bGU9XCJwcm9kdWN0UmF0aW5nXCIgY2xhc3M9XCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXNtYWxsLXJhdGluZ1wiPjwvc3Bhbj48L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj4gIFxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICBwcm9wczogWydpdGVtJ10sXHJcbiAgbWV0aG9kczp7XHJcbiAgICBzZWxlY3QoaWQpe1xyXG4gICAgICAgdGhpcy4kcGFyZW50LnNlbGVjdEl0ZW0oaWQpXHJcbiAgICB9LFxyXG4gICAgYWRkVG9DYXJ0KCl7XHJcbiAgICAgICB0aGlzLiRwYXJlbnQuYWRkVG9DYXJ0KHRoaXMuaXRlbS5pZCx0aGlzLml0ZW0ubmFtZSlcclxuICAgIH0sXHJcbiAgICBhZGRUb0NvbXBhcmUoKXtcclxuICAgICAgIHRoaXMuJHBhcmVudC5hZGRUb0NvbXBhcmUodGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5uYW1lKVxyXG4gICAgfSxcclxuICAgIGFkZFRvRmF2b3JpdGVzKCl7XHJcbiAgICAgICB0aGlzLiRwYXJlbnQuYWRkVG9GYXZvcml0ZXModGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5uYW1lKVxyXG4gICAgfVxyXG4gIH0sXHJcbiAgY29tcHV0ZWQ6e1xyXG4gICAgcHJpY2UoKXtcclxuICAgICAgaWYodGhpcy5pdGVtLmZlZV9pbmNsdWRlZD09MSlcclxuICAgICAgICByZXR1cm4gcGFyc2VGbG9hdCh0aGlzLml0ZW0ucHJpY2UpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0uc2hpcHBpbmdfZmVlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLmNoYXJnZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS52YXQpO1xyXG4gICAgICBlbHNlXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXRlbS5wcmljZTtcclxuICAgIH0sXHJcbiAgICBzYWxlcHJpY2UoKXtcclxuICAgICAgaWYodGhpcy5pdGVtLmZlZV9pbmNsdWRlZD09MSlcclxuICAgICAgICByZXR1cm4gcGFyc2VGbG9hdCh0aGlzLml0ZW0uc2FsZV9wcmljZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0uY2hhcmdlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLnZhdCk7XHJcbiAgICAgIGVsc2VcclxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtLnNhbGVfcHJpY2U7XHJcbiAgICB9LFxyXG4gICAgbmFtZWxlbmd0aCgpe1xyXG4gICAgICB2YXIgbmFtZSA9IHRoaXMuaXRlbS5jbl9uYW1lID8gdGhpcy5pdGVtLmNuX25hbWU6IHRoaXMuaXRlbS5uYW1lO1xyXG4gICAgICBpZiAodGhpcy5pdGVtLm5hbWUubGVuZ3RoID4gNDUpIHtcclxuICAgICAgICBpZih0aGlzLiRwYXJlbnQubGFuZ21vZGU9PVwiY25cIil7XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lLnN1YnN0cigwLCA0NSkrXCIuLi5cIjsgIFxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtLm5hbWUuc3Vic3RyKDAsIDQ1KStcIi4uLlwiO1xyXG4gICAgICB9XHJcbiAgICAgIGVsc2V7XHJcbiAgICAgICAgaWYodGhpcy4kcGFyZW50Lmxhbmdtb2RlPT1cImNuXCIpe1xyXG4gICAgICAgICAgICByZXR1cm4gbmFtZTsgIFxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtLm5hbWU7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBzdGFyUmF0aW5nKCl7XHJcbiAgICAgIHJldHVybiBcIndpZHRoOlwiK01hdGgucm91bmQoKHRoaXMuaXRlbS5yYXRlL3RoaXMuaXRlbS5jb3VudCkqMjApK1wiJVwiO1xyXG4gICAgfSxcclxuICAgIHByb2R1Y3RSYXRpbmcoKXtcclxuICAgICAgcmV0dXJuIFwid2lkdGg6XCIrdGhpcy5pdGVtLnByb2R1Y3RfcmF0aW5nK1wiJVwiO1xyXG4gICAgfSxcclxuXHJcbiAgfVxyXG59XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBSZWxhdGVkLVByb2R1Y3QudnVlPzliM2ZmODMyIiwiPHRlbXBsYXRlPlxyXG5cdDxkaXYgY2xhc3M9J2NvbC1sZy0xMiB1c2VyLWZlZWRiYWNrJz5cclxuXHQgICAgPGRpdiBjbGFzcz0nY29sLWxnLTEgdGV4dC1jZW50ZXInPlxyXG5cdCAgICAgICAgPGltZyB2LWlmPSdjb21tZW50LnVzZXIuYXZhdGFyJyA6c3JjPVwiY29tbWVudC51c2VyLmF2YXRhclwiIGNsYXNzPVwiaW1nIGltZy1jaXJjbGUgZmItdXNlci1pbWFnZVwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPSduby1hdmF0YXInIHYtZWxzZT5cclxuICAgICAgICAgICAgICAgIHt7Y29tbWVudC51c2VyLmluaXRpYWxzfX1cclxuICAgICAgICAgICAgPC9kaXY+XHJcblx0ICAgIDwvZGl2PlxyXG5cdCAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTExXCI+XHJcblx0ICAgICAgICA8c3BhbiBjbGFzcz0nZmItbmFtZSAgcG9wb3Zlci1kZXRhaWxzJyA6ZGF0YS1pZD0nY29tbWVudC51c2VyLmlkJz5cclxuXHQgICAgICAgICAgICB7e2NvbW1lbnQudXNlci5mdWxsX25hbWV9fVxyXG5cdCAgICAgICAgPC9zcGFuPlxyXG5cdCAgICAgICAgPGRpdiBjbGFzcz0nZGF0ZXRpbWUnPlxyXG5cdCAgICAgICAgICAgIHt7Y29tbWVudC5jcmVhdGVkX2F0fX1cclxuXHQgICAgICAgIDwvZGl2PlxyXG5cdCAgICAgICAgPGRpdiBjbGFzcz0nZmItY29udGVudCc+XHJcblx0ICAgICAgICAgICAge3tjb21tZW50LmNvbnRlbnR9fVxyXG5cdCAgICAgICAgPC9kaXY+XHJcblx0ICAgIDwvZGl2PlxyXG5cdDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRwcm9wczogWydjb21tZW50J10sXHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENvbW1lbnQudnVlPzZmNTliZDUyIiwiPHRlbXBsYXRlPlxyXG5cdDxhIFx0Y2xhc3M9J2J0biBidG4taGFzLWxvYWRpbmcgYnRuLWxpa2UgYnRuLWZhYiBidG4tZmFiLW1pbmkgYnRuLWxpa2UgJyBcclxuXHRcdDpjbGFzcz0nbGlrZWQ/XCJsaWtlZFwiOlwibm90LWxpa2VkXCInXHJcblx0XHQ6aHJlZj0nXCIjXCIrIGZlZWRiYWNrJyBcclxuXHRcdEBjbGljaz0nbGlrZUZlZWRiYWNrJ1xyXG5cdFx0ZGF0YS10b2dnbGU9J3Rvb2x0aXAnXHJcblx0XHRkYXRhLXBsYWNlbWVudD0nYm90dG9tJ1xyXG5cdFx0OmRhdGEtdGl0bGU9J21lc3NhZ2UnXHJcblx0XHQ6aWQ9J1wiYnRuTGlrZVwiK2ZlZWRiYWNrJ1xyXG5cdD5cclxuXHRcdDxzcGFuIGNsYXNzPSdmYSBmYS10aHVtYnMtdXAnPjwvc3Bhbj4gXHJcblx0PC9hPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHRcdDwhLS0gZGF0YS10b2dnbGU9J3Rvb2x0aXAnXHJcblx0XHQ6ZGF0YS10aXRsZT0nbGlrZWQ/IFwiWW91IGxpa2VkIHRoaXMgZmVlZGJhY2tcIjpcIkxpa2VcIicgLS0+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcblx0cHJvcHM6IFtcclxuXHRcdCAgJ2ZlZWRiYWNrJ1xyXG5cdFx0ICwnbGlrZXMnXHJcblxyXG5cdF0sXHJcblx0ZGF0YTpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgIHVzZXJfaWQ6TGFyYXZlbC51c2VyLmlkXHJcbiAgICAgICAgICAgICxmZWVkYmFja2xpa2VfaWQ6ZmFsc2VcclxuXHRcdFx0LGxpa2VkOmZhbHNlXHJcblx0XHRcdCxtZXNzYWdlOlwiTGlrZVwiXHJcbiAgICAgICAgIH1cclxuICAgIH0sXHJcbiAgICB3YXRjaDp7XHJcbiAgICBcdG1lc3NhZ2U6ZnVuY3Rpb24gKG5ld01zZyl7XHJcbiAgICBcdFx0JCgnI2J0bkxpa2UnK3RoaXMuZmVlZGJhY2spLnRvb2x0aXAoJ2hpZGUnKVxyXG5cdCAgICAgICAgICAuYXR0cignZGF0YS1vcmlnaW5hbC10aXRsZScsIG5ld01zZylcclxuXHQgICAgICAgICAgLnRvb2x0aXAoJ2ZpeFRpdGxlJyk7XHJcbiAgICBcdH1cclxuICAgIH0sXHJcbiAgICBjcmVhdGVkKCl7XHJcbiAgICBcdHZhciBsYW5nID0gdGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZztcclxuICAgIFx0YXhpb3NBUEl2MS5nZXQoJy9wcm9kdWN0LXJldmlldy1saWtlcz91c2VyPScrdGhpcy51c2VyX2lkKycmcHJvZHVjdD0nK3RoaXMuZmVlZGJhY2spXHJcblx0XHQudGhlbigocmVzKSA9PiB7XHJcblx0XHRcdGlmKHJlcy5kYXRhLmxlbmd0aClcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRoaXMuZmVlZGJhY2tsaWtlX2lkID0gcmVzLmRhdGFbMF0uaWQ7XHJcblx0XHRcdFx0dGhpcy5saWtlZCA9IHRydWU7XHJcblx0XHRcdFx0aWYodGhpcy5saWtlZCl7XHJcblx0XHRcdFx0XHRpZih0aGlzLmxpa2VzPT1cIjFcIilcclxuXHRcdFx0XHRcdFx0dGhpcy5tZXNzYWdlID0gbGFuZ1sneW91LWxpa2VkLXRoaXMnXTtcclxuXHRcdFx0XHRcdGVsc2V7XHJcblx0XHRcdFx0XHRcdHZhciBsaWtlID0gdGhpcy5saWtlcyAtIDE7XHJcblx0XHRcdFx0XHRcdHRoaXMubGlrZXMgPSBsaWtlO1xyXG5cdFx0XHRcdFx0XHR0aGlzLm1lc3NhZ2UgPSBsYW5nWyd5b3UtYW5kJ10rXCIgXCIrbGlrZStcIiBcIitsYW5nWydvdGhlcnMtbGlrZWQtdGhpcyddO1xyXG5cdFx0XHRcdFx0fSAgXHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGlmKHRoaXMubGlrZXM9PVwiMFwiKVxyXG5cdFx0XHRcdFx0XHR0aGlzLm1lc3NhZ2UgPSBsYW5nW1wibGlrZVwiXTtcclxuXHRcdFx0XHRcdGVsc2V7XHJcblx0XHRcdFx0XHRcdHZhciBsaWtlID0gdGhpcy5saWtlcyAtIDE7XHJcblx0XHRcdFx0XHRcdHRoaXMubGlrZXMgPSBsaWtlO1xyXG5cdFx0XHRcdFx0XHR0aGlzLm1lc3NhZ2UgPSBsaWtlK1wiIFwiK2xhbmdbJ290aGVycy1saWtlZC10aGlzJ107XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9ZWxzZVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0JCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoKVxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHJcbiAgICB9LFxyXG5cdG1ldGhvZHM6e1xyXG5cdFx0bGlrZUZlZWRiYWNrKCl7XHJcblx0XHRcdHZhciBsYW5nID0gdGhpcy4kcGFyZW50LiRwYXJlbnQubGFuZztcclxuXHRcdFx0aWYodGhpcy5saWtlZClcclxuXHRcdFx0e1xyXG5cdFx0XHRcdC8vdXBkYXRlIG9yIHNlYXJjaFxyXG5cdFx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvcHJvZHVjdC1yZXZpZXctbGlrZXM/dXNlcj0nK3RoaXMudXNlcl9pZCsnJnByb2R1Y3Q9Jyt0aGlzLmZlZWRiYWNrKVxyXG5cdFx0XHRcdC50aGVuKChyZXMpID0+IHtcclxuXHRcdFx0XHRcdGlmKHJlcy5kYXRhLmxlbmd0aClcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGhpcy5mZWVkYmFja2xpa2VfaWQgPSByZXMuZGF0YVswXS5pZDtcclxuXHRcdFx0XHRcdFx0dGhpcy5saWtlZCA9IHRydWU7XHJcblx0XHRcdFx0XHRcdGF4aW9zQVBJdjEuZGVsZXRlKCcvcHJvZHVjdC1yZXZpZXctbGlrZXMvJyt0aGlzLmZlZWRiYWNrbGlrZV9pZClcclxuXHRcdFx0XHRcdFx0LnRoZW4oKHJlcykgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdHRoaXMubGlrZWQgPSBmYWxzZTtcclxuXHRcdFx0XHRcdFx0XHR0b2FzdHIuc3VjY2VzcygnJyxsYW5nWyd1bmxpa2VkJ10pO1xyXG5cdFx0XHRcdFx0XHRcdGlmKHRoaXMubGlrZXM9PVwiMFwiKVxyXG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5tZXNzYWdlID0gbGFuZ1tcImxpa2VcIl07XHJcblx0XHRcdFx0XHRcdFx0ZWxzZVxyXG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5tZXNzYWdlID0gdGhpcy5saWtlcytcIiBcIitsYW5nWydvdGhlcnMtbGlrZWQtdGhpcyddO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdH1lbHNlXHJcblx0XHRcdHtcclxuXHRcdFx0XHRheGlvc0FQSXYxLnBvc3QoJy9wcm9kdWN0LXJldmlldy1saWtlcycse1xyXG5cdFx0XHRcdFx0XHQgdXNlcl9pZDp0aGlzLnVzZXJfaWRcclxuXHRcdFx0XHRcdFx0LHJldmlld19pZDp0aGlzLmZlZWRiYWNrXHJcblx0XHRcdFx0fSkudGhlbigocmVzKSA9PiB7XHJcblx0XHRcdFx0XHR0aGlzLmxpa2VkID0gdHJ1ZTtcclxuXHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKCcnLGxhbmdbJ2xpa2VkJ10pO1xyXG5cdFx0XHRcdFx0aWYodGhpcy5saWtlcz09XCIwXCIpXHJcblx0XHRcdFx0XHRcdHRoaXMubWVzc2FnZSA9IGxhbmdbJ3lvdS1saWtlZC10aGlzJ11cclxuXHRcdFx0XHRcdGVsc2VcclxuXHRcdFx0XHRcdFx0dGhpcy5tZXNzYWdlID0gbGFuZ1sneW91LWFuZCddK1wiIFwiK3RoaXMubGlrZXMrXCIgXCIrbGFuZ1snb3RoZXJzLWxpa2VkLXRoaXMnXTtcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIExpa2UudnVlPzNlNzBhY2QyIiwiXHJcbjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbGctMTJcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyB1c2VyLWZlZWRiYWNrXCI+XHJcbiAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImFicyB0by1wYXJ0aWFsLXJpZ2h0IHRvLWZ1bGwtYm90dG9tXCIgdi1pZj1cIiFmZWVkYmFjay5jb21tZW50cy5sZW5ndGhcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIEBjbGljaz1cIm9wZW5Db21tZW50c01vZGFsKGZlZWRiYWNrLmlkKVwiIGlkPVwiYnRuQWRkQ29tbWVudE1vZGFsXCIgY2xhc3M9XCJidG4gYnRuLWZhYiBidG4tcmlwcGxlIGJ0bi1pbmZvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPSdtYXRlcmlhbC1pY29ucyc+YWRkPC9pPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTEgY29sLW1kLTIgY29sLXNtLTIgY29sLXhzLTMgdGV4dC1jZW50ZXIgIFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgdi1pZj0nZmVlZGJhY2sudXNlci5hdmF0YXInIDpzcmM9XCJmZWVkYmFjay51c2VyLmF2YXRhclwiIGNsYXNzPVwiaW1nIGltZy1jaXJjbGUgZmItdXNlci1pbWFnZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9J25vLWF2YXRhcicgdi1lbHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7e2ZlZWRiYWNrLnVzZXIuaW5pdGlhbHN9fVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTExICBjb2wtbWQtMTAgY29sLXNtLTEwIGNvbC14cy05XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZiLWhvbGRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYWJzIHRvLXJpZ2h0IHRvLWZ1bGwtYm90dG9tXCIgdi1pZj1cImZlZWRiYWNrLmNvbW1lbnRzLmxlbmd0aFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBAY2xpY2s9XCJvcGVuQ29tbWVudHNNb2RhbChmZWVkYmFjay5pZClcIiBpZD1cImJ0bkFkZENvbW1lbnRNb2RhbFwiIGNsYXNzPVwiYnRuIGJ0bi1mYWIgYnRuLXJpcHBsZSBidG4taW5mb1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPSdtYXRlcmlhbC1pY29ucyc+YWRkPC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImZiLW5hbWUgcG9wb3Zlci1kZXRhaWxzXCIgOmRhdGEtaWQ9J2ZlZWRiYWNrLnVzZXIuaWQnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3tmZWVkYmFjay51c2VyLmZ1bGxfbmFtZX19IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkYXRldGltZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3tmZWVkYmFjay5jcmVhdGVkX2F0IHwgZnJpZW5kbHktZGF0ZX19IFRoaXMgaXMge3tmZWVkYmFjay5yZXZpZXd9fSBwcm9kdWN0LlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaWtlICA6ZmVlZGJhY2s9J2ZlZWRiYWNrLmlkJyA6bGlrZXM9J2ZlZWRiYWNrLmxpa2VzX2NvdW50Jz48L2xpa2U+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPCEtLSA8ZGl2IGNsYXNzPVwicmF0aW5nLWJsb2NrIHAtdC0yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3Rhci1yYXRpbmdzLXNwcml0ZS1zbWFsbFwiPjxzcGFuIDpzdHlsZT1cImZlZWRiYWNrLnN0YXJfcmF0aW5nXCIgY2xhc3M9XCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXNtYWxsLXJhdGluZ1wiPjwvc3Bhbj48L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IC0tPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmItY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz0nY29udGFpbmVyLWZsdWlkJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTIgY29sLW1kLTIgY29sLXNtLTIgY29sLXhzLTEyXCIgdi1pZj0nZmVlZGJhY2suaW1hZ2UnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIDpzcmM9XCInLycrZmVlZGJhY2suaW1hZ2VcIiBjbGFzcz0naW1nLXJlc3BvbnNpdmUnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgOmNsYXNzPVwiZmVlZGJhY2suaW1hZ2U/ICdjb2wtbGctMTAgY29sLW1kLTEwIGNvbC1zbS0xMCBjb2wteHMtMTInOiAnY29sLWxnLTEyJ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7e2ZlZWRiYWNrLmNvbnRlbnR9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmYi1jb21tZW50cy1ob2xkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGNvbW1lbnQgdi1mb3I9XCIoY29tbWVudCwkaW5kZXgpIGluIGZlZWRiYWNrLmNvbW1lbnRzXCIgdi1pZj1cIiRpbmRleCA8IGNvbW1lbnRzU2hvd25cIiA6Y29tbWVudD1cImNvbW1lbnRcIiA6a2V5PVwiY29tbWVudC5pZFwiPjwvY29tbWVudD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1sZy0xMiB1c2VyLWZlZWRiYWNrIHRleHQtcmlnaHRcIiB2LWlmPVwiKGZlZWRiYWNrLmNvbW1lbnRzLmxlbmd0aCA+IGNvbW1lbnRzU2hvd24pfHwoY29tbWVudHNTaG93biA+IDIpXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uICB2LWlmPVwiZmVlZGJhY2suY29tbWVudHMubGVuZ3RoID4gY29tbWVudHNTaG93blwiICBjbGFzcz1cImJ0biBidG4taW5mbyBidG4tcmFpc2VkIGJ0bi1oYXMtbG9hZGluZ1wiIEBjbGljaz0ndmlld01vcmUnPlZpZXcgTW9yZTwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiAgdi1pZj1cImNvbW1lbnRzU2hvd24gPiAyXCIgY2xhc3M9XCJidG4gYnRuLWluZm8gYnRuLXJhaXNlZCBidG4taGFzLWxvYWRpbmdcIiBAY2xpY2s9J3ZpZXdMZXNzJz5WaWV3IExlc3M8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcblxyXG48c2NyaXB0PlxyXG5WdWUuY29tcG9uZW50KCdsaWtlJywgIHJlcXVpcmUoJy4vTGlrZS52dWUnKSk7XHJcblZ1ZS5jb21wb25lbnQoJ2NvbW1lbnQnLCAgcmVxdWlyZSgnLi9Db21tZW50LnZ1ZScpKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRwcm9wczogWydmZWVkYmFjayddLFxyXG4gICAgZGF0YTpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgY29tbWVudHNTaG93bjoyLFxyXG4gICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczp7XHJcbiAgICAgICAgb3BlbkNvbW1lbnRzTW9kYWwoaWQpe1xyXG4gICAgICAgICAgICBpZih0aGlzLiRwYXJlbnQub3duZXIgPT0gdGhpcy4kcGFyZW50LnNpdGVfdXNlcil7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLiRwYXJlbnQudmVyaWZpZWQgPT0gdHJ1ZSl7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnI21vZGFsQ29tbWVudHMnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcignWW91ciBhY2NvdW50IG5lZWQgdG8gYmUgdmVyaWZpZWQgZmlyc3QgZm9yIHlvdSB0byBwb3N0IGEgcmVwbHkgdG8gYW55IHByb2R1Y3QgcmV2aWV3LiEnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgJCgnI21vZGFsQ29tbWVudHMnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy4kcGFyZW50LmNvbW1lbnRGb3JtLnBhcmVudF9pZCA9IGlkO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdmlld01vcmUoKXtcclxuICAgICAgICAgICAgdGhpcy5jb21tZW50c1Nob3duICs9ICh0aGlzLmZlZWRiYWNrLmNvbW1lbnRzLmxlbmd0aC10aGlzLmNvbW1lbnRzU2hvd24+Mik/Mjp0aGlzLmZlZWRiYWNrLmNvbW1lbnRzLmxlbmd0aC10aGlzLmNvbW1lbnRzU2hvd247XHJcbiAgICAgICAgfSxcclxuICAgICAgICB2aWV3TGVzcygpe1xyXG4gICAgICAgICAgICBpZih0aGlzLmNvbW1lbnRzU2hvd24gPD0gMilcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgdGhpcy5jb21tZW50c1Nob3duIC09IDI7XHJcbiAgICAgICAgfSxcclxuICAgICAgIFxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHJldmlldy52dWU/MDk2MGJjMGMiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9SZWxhdGVkLVByb2R1Y3QudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi00M2VjMzczMVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9SZWxhdGVkLVByb2R1Y3QudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXHByb2R1Y3RcXFxcUmVsYXRlZC1Qcm9kdWN0LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFJlbGF0ZWQtUHJvZHVjdC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNDNlYzM3MzFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi00M2VjMzczMVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9SZWxhdGVkLVByb2R1Y3QudnVlXG4vLyBtb2R1bGUgaWQgPSAzNDVcbi8vIG1vZHVsZSBjaHVua3MgPSA1IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQ29tbWVudC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTY0ZmYwZDk0XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0NvbW1lbnQudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXHByb2R1Y3RcXFxccmV2aWV3XFxcXENvbW1lbnQudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQ29tbWVudC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNjRmZjBkOTRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi02NGZmMGQ5NFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9yZXZpZXcvQ29tbWVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDM0NlxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9MaWtlLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMTQ1ODcwOWNcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vTGlrZS52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxccHJvZHVjdFxcXFxyZXZpZXdcXFxcTGlrZS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBMaWtlLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0xNDU4NzA5Y1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTE0NTg3MDljXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L3Jldmlldy9MaWtlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzQ3XG4vLyBtb2R1bGUgY2h1bmtzID0gNSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL3Jldmlldy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTY5ZTQzNjEzXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jldmlldy52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxccHJvZHVjdFxcXFxyZXZpZXdcXFxccmV2aWV3LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIHJldmlldy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNjllNDM2MTNcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi02OWU0MzYxM1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9yZXZpZXcvcmV2aWV3LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzQ4XG4vLyBtb2R1bGUgY2h1bmtzID0gNSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1Byb2R1Y3QudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03NzhmNzVmM1xcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Qcm9kdWN0LnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxwcm9kdWN0XFxcXFByb2R1Y3QudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gUHJvZHVjdC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNzc4Zjc1ZjNcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi03NzhmNzVmM1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9Qcm9kdWN0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzZcbi8vIG1vZHVsZSBjaHVua3MgPSA1IDIwIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4taGFzLWxvYWRpbmcgYnRuLWxpa2UgYnRuLWZhYiBidG4tZmFiLW1pbmkgYnRuLWxpa2UgXCIsXG4gICAgY2xhc3M6IF92bS5saWtlZCA/IFwibGlrZWRcIiA6IFwibm90LWxpa2VkXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIiNcIiArIF92bS5mZWVkYmFjayxcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG4gICAgICBcImRhdGEtdGl0bGVcIjogX3ZtLm1lc3NhZ2UsXG4gICAgICBcImlkXCI6IFwiYnRuTGlrZVwiICsgX3ZtLmZlZWRiYWNrXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0ubGlrZUZlZWRiYWNrXG4gICAgfVxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtdGh1bWJzLXVwXCJcbiAgfSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0xNDU4NzA5Y1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTE0NTg3MDljXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9yZXZpZXcvTGlrZS52dWVcbi8vIG1vZHVsZSBpZCA9IDM2NFxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtaXggY29sLXNtLTMgY29sLXhzLTZcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJib3gtcHJvZHVjdC1vdXRlclwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJveC1wcm9kdWN0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1nLXdyYXBwZXJcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiAnL3Byb2R1Y3QvJyArIF92bS5pdGVtLnNsdWdcbiAgICB9XG4gIH0sIFtfYygnaW1nJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImFsdFwiOiBcIlByb2R1Y3RcIixcbiAgICAgIFwic3JjXCI6IF92bS5pdGVtLmltYWdlLFxuICAgICAgXCJvbkVycm9yXCI6IFwidGhpcy5zcmMgPSAnL2ltYWdlcy9hdmF0YXIvZGVmYXVsdC1wcm9kdWN0LmpwZydcIlxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5pdGVtLnNhbGVfcHJpY2UpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWdzXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImxhYmVsLXRhZ3NcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWwgbGFiZWwtZGFuZ2VyIGFycm93ZWRcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uaXRlbS5kaXNjb3VudCkgKyBcIiVcIildKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJvcHRpb25cIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiLFxuICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcbiAgICAgIFwiZGF0YS1wbGFjZW1lbnRcIjogXCJ0b3BcIixcbiAgICAgIFwidGl0bGVcIjogXCJBZGQgdG8gQ2FydFwiLFxuICAgICAgXCJkYXRhLW9yaWdpbmFsLXRpdGxlXCI6IFwiQWRkIHRvIENhcnRcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5hZGRUb0NhcnQoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXNob3BwaW5nLWNhcnRcIlxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkFkZCB0byBDb21wYXJlXCJcbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1hbGlnbi1sZWZ0XCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5hZGRUb0NvbXBhcmUoKVxuICAgICAgfVxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwid2lzaGxpc3RcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkFkZCB0byBXaXNobGlzdFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmFkZFRvRmF2b3JpdGVzKClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1oZWFydFwiXG4gIH0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaDYnLCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiAnL3Byb2R1Y3QvJyArIF92bS5pdGVtLnNsdWdcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5uYW1lbGVuZ3RoKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInByaWNlXCJcbiAgfSwgW19jKCdkaXYnLCBbKF92bS5pdGVtLnNhbGVfcHJpY2UpID8gX2MoJ3NwYW4nLCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShwYXJzZUZsb2F0KFwiMFwiICsgX3ZtLnNhbGVwcmljZSkpKSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBjbGFzczogX3ZtLml0ZW0uc2FsZV9wcmljZSA/ICdwcmljZS1vbGQnIDogJydcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikocGFyc2VGbG9hdChcIjBcIiArIF92bS5wcmljZSkpKSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicmF0aW5nLWJsb2NrXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwic3Rhci1yYXRpbmdzLXNwcml0ZS1zbWFsbFwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXNtYWxsLXJhdGluZ1wiLFxuICAgIHN0eWxlOiAoX3ZtLnByb2R1Y3RSYXRpbmcpXG4gIH0pXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi00M2VjMzczMVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTQzZWMzNzMxXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9SZWxhdGVkLVByb2R1Y3QudnVlXG4vLyBtb2R1bGUgaWQgPSAzODFcbi8vIG1vZHVsZSBjaHVua3MgPSA1IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTEyIHVzZXItZmVlZGJhY2tcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMSB0ZXh0LWNlbnRlclwiXG4gIH0sIFsoX3ZtLmNvbW1lbnQudXNlci5hdmF0YXIpID8gX2MoJ2ltZycsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbWcgaW1nLWNpcmNsZSBmYi11c2VyLWltYWdlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5jb21tZW50LnVzZXIuYXZhdGFyXG4gICAgfVxuICB9KSA6IF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibm8tYXZhdGFyXCJcbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uY29tbWVudC51c2VyLmluaXRpYWxzKSArIFwiXFxuICAgICAgICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMTFcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmItbmFtZSAgcG9wb3Zlci1kZXRhaWxzXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1pZFwiOiBfdm0uY29tbWVudC51c2VyLmlkXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmNvbW1lbnQudXNlci5mdWxsX25hbWUpICsgXCJcXG5cXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImRhdGV0aW1lXCJcbiAgfSwgW192bS5fdihcIlxcblxcdCAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5jb21tZW50LmNyZWF0ZWRfYXQpICsgXCJcXG5cXHQgICAgICAgIFwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZiLWNvbnRlbnRcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0ICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmNvbW1lbnQuY29udGVudCkgKyBcIlxcblxcdCAgICAgICAgXCIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTY0ZmYwZDk0XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNjRmZjBkOTRcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L3Jldmlldy9Db21tZW50LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzkwXG4vLyBtb2R1bGUgY2h1bmtzID0gNSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcmRcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMTJcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyb3cgdXNlci1mZWVkYmFja1wiXG4gIH0sIFsoIV92bS5mZWVkYmFjay5jb21tZW50cy5sZW5ndGgpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJhYnMgdG8tcGFydGlhbC1yaWdodCB0by1mdWxsLWJvdHRvbVwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZmFiIGJ0bi1yaXBwbGUgYnRuLWluZm9cIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImJ0bkFkZENvbW1lbnRNb2RhbFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLm9wZW5Db21tZW50c01vZGFsKF92bS5mZWVkYmFjay5pZClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtYXRlcmlhbC1pY29uc1wiXG4gIH0sIFtfdm0uX3YoXCJhZGRcIildKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMSBjb2wtbWQtMiBjb2wtc20tMiBjb2wteHMtMyB0ZXh0LWNlbnRlciAgXCJcbiAgfSwgWyhfdm0uZmVlZGJhY2sudXNlci5hdmF0YXIpID8gX2MoJ2ltZycsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbWcgaW1nLWNpcmNsZSBmYi11c2VyLWltYWdlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5mZWVkYmFjay51c2VyLmF2YXRhclxuICAgIH1cbiAgfSkgOiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm5vLWF2YXRhclwiXG4gIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uZmVlZGJhY2sudXNlci5pbml0aWFscykgKyBcIlxcbiAgICAgICAgICAgICAgICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMTEgIGNvbC1tZC0xMCBjb2wtc20tMTAgY29sLXhzLTlcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYi1ob2xkZXJcIlxuICB9LCBbKF92bS5mZWVkYmFjay5jb21tZW50cy5sZW5ndGgpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJhYnMgdG8tcmlnaHQgdG8tZnVsbC1ib3R0b21cIlxuICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWZhYiBidG4tcmlwcGxlIGJ0bi1pbmZvXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJidG5BZGRDb21tZW50TW9kYWxcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5vcGVuQ29tbWVudHNNb2RhbChfdm0uZmVlZGJhY2suaWQpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibWF0ZXJpYWwtaWNvbnNcIlxuICB9LCBbX3ZtLl92KFwiYWRkXCIpXSldKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZiLW5hbWUgcG9wb3Zlci1kZXRhaWxzXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1pZFwiOiBfdm0uZmVlZGJhY2sudXNlci5pZFxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhfdm0uZmVlZGJhY2sudXNlci5mdWxsX25hbWUpICsgXCIgXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZGF0ZXRpbWVcIlxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5fZihcImZyaWVuZGx5LWRhdGVcIikoX3ZtLmZlZWRiYWNrLmNyZWF0ZWRfYXQpKSArIFwiIFRoaXMgaXMgXCIgKyBfdm0uX3MoX3ZtLmZlZWRiYWNrLnJldmlldykgKyBcIiBwcm9kdWN0LlxcbiAgICAgICAgICAgICAgICAgICAgXCIpLCBfYygnbGlrZScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJmZWVkYmFja1wiOiBfdm0uZmVlZGJhY2suaWQsXG4gICAgICBcImxpa2VzXCI6IF92bS5mZWVkYmFjay5saWtlc19jb3VudFxuICAgIH1cbiAgfSldLCAxKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYi1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29udGFpbmVyLWZsdWlkXCJcbiAgfSwgWyhfdm0uZmVlZGJhY2suaW1hZ2UpID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMiBjb2wtbWQtMiBjb2wtc20tMiBjb2wteHMtMTJcIlxuICB9LCBbX2MoJ2ltZycsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInNyY1wiOiAnLycgKyBfdm0uZmVlZGJhY2suaW1hZ2VcbiAgICB9XG4gIH0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBjbGFzczogX3ZtLmZlZWRiYWNrLmltYWdlID8gJ2NvbC1sZy0xMCBjb2wtbWQtMTAgY29sLXNtLTEwIGNvbC14cy0xMicgOiAnY29sLWxnLTEyJ1xuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmZlZWRiYWNrLmNvbnRlbnQpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiKV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZiLWNvbW1lbnRzLWhvbGRlclwiXG4gIH0sIFtfdm0uX2woKF92bS5mZWVkYmFjay5jb21tZW50cyksIGZ1bmN0aW9uKGNvbW1lbnQsICRpbmRleCkge1xuICAgIHJldHVybiAoJGluZGV4IDwgX3ZtLmNvbW1lbnRzU2hvd24pID8gX2MoJ2NvbW1lbnQnLCB7XG4gICAgICBrZXk6IGNvbW1lbnQuaWQsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImNvbW1lbnRcIjogY29tbWVudFxuICAgICAgfVxuICAgIH0pIDogX3ZtLl9lKClcbiAgfSksIF92bS5fdihcIiBcIiksICgoX3ZtLmZlZWRiYWNrLmNvbW1lbnRzLmxlbmd0aCA+IF92bS5jb21tZW50c1Nob3duKSB8fCAoX3ZtLmNvbW1lbnRzU2hvd24gPiAyKSkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMiB1c2VyLWZlZWRiYWNrIHRleHQtcmlnaHRcIlxuICB9LCBbKF92bS5mZWVkYmFjay5jb21tZW50cy5sZW5ndGggPiBfdm0uY29tbWVudHNTaG93bikgPyBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4taW5mbyBidG4tcmFpc2VkIGJ0bi1oYXMtbG9hZGluZ1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IF92bS52aWV3TW9yZVxuICAgIH1cbiAgfSwgW192bS5fdihcIlZpZXcgTW9yZVwiKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIChfdm0uY29tbWVudHNTaG93biA+IDIpID8gX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWluZm8gYnRuLXJhaXNlZCBidG4taGFzLWxvYWRpbmdcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBfdm0udmlld0xlc3NcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJWaWV3IExlc3NcIildKSA6IF92bS5fZSgpXSkgOiBfdm0uX2UoKV0sIDIpXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNjllNDM2MTNcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi02OWU0MzYxM1wifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3Byb2R1Y3QvcmV2aWV3L3Jldmlldy52dWVcbi8vIG1vZHVsZSBpZCA9IDM5MVxuLy8gbW9kdWxlIGNodW5rcyA9IDUiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtaXggY29sLXNtLTNcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJib3gtcHJvZHVjdC1vdXRlclwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJveC1wcm9kdWN0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1nLXdyYXBwZXJcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiAnL3Byb2R1Y3QvJyArIF92bS5pdGVtLnNsdWdcbiAgICB9XG4gIH0sIFtfYygnaW1nJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImFsdFwiOiBcIlByb2R1Y3RcIixcbiAgICAgIFwic3JjXCI6ICcvc2hvcHBpbmcvaW1hZ2VzL3Byb2R1Y3QvJyArIF92bS5pdGVtLmlkICsgJy9wcmltYXJ5LmpwZycsXG4gICAgICBcIm9uRXJyb3JcIjogXCJ0aGlzLnNyYyA9ICcvaW1hZ2VzL2F2YXRhci9kZWZhdWx0LXByb2R1Y3QuanBnJ1wiXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLml0ZW0uc2FsZV9wcmljZSkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhZ3NcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWwtdGFnc1wiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1kYW5nZXIgYXJyb3dlZFwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLmRpc2NvdW50KSArIFwiJVwiKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm9wdGlvblwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkFkZCB0byBDYXJ0XCIsXG4gICAgICBcImRhdGEtb3JpZ2luYWwtdGl0bGVcIjogXCJBZGQgdG8gQ2FydFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmFkZFRvQ2FydCgpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtc2hvcHBpbmctY2FydFwiXG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiQWRkIHRvIENvbXBhcmVcIlxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWFsaWduLWxlZnRcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmFkZFRvQ29tcGFyZSgpXG4gICAgICB9XG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ3aXNobGlzdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiQWRkIHRvIFdpc2hsaXN0XCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uYWRkVG9GYXZvcml0ZXMoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWhlYXJ0XCJcbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdoNicsIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdC8nICsgX3ZtLml0ZW0uc2x1Z1xuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm5hbWVsZW5ndGgpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHJpY2VcIlxuICB9LCBbX2MoJ2RpdicsIFsoX3ZtLml0ZW0uc2FsZV9wcmljZSkgPyBfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHBhcnNlRmxvYXQoXCIwXCIgKyBfdm0uc2FsZXByaWNlKSkpKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIGNsYXNzOiBfdm0uaXRlbS5zYWxlX3ByaWNlID8gJ3ByaWNlLW9sZCcgOiAnJ1xuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShwYXJzZUZsb2F0KFwiMFwiICsgX3ZtLnByaWNlKSkpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyYXRpbmctYmxvY2tcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXNtYWxsXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInN0YXItcmF0aW5ncy1zcHJpdGUtc21hbGwtcmF0aW5nXCIsXG4gICAgc3R5bGU6IChfdm0ucHJvZHVjdFJhdGluZylcbiAgfSldKV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTc3OGY3NWYzXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNzc4Zjc1ZjNcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L1Byb2R1Y3QudnVlXG4vLyBtb2R1bGUgaWQgPSA0MlxuLy8gbW9kdWxlIGNodW5rcyA9IDUgMjAiXSwic291cmNlUm9vdCI6IiJ9