/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 448);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['item'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    },
    addToCart: function addToCart() {
      this.$parent.addToCart(this.item.id, this.item.name);
    },
    addToCompare: function addToCompare() {
      this.$parent.addToCompare(this.item.id, this.item.name);
    },
    addToFavorites: function addToFavorites() {
      this.$parent.addToFavorites(this.item.id, this.item.name);
    }
  },
  computed: {
    price: function price() {
      if (this.item.fee_included == 1) return parseFloat(this.item.price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.price;
    },
    saleprice: function saleprice() {
      if (this.item.fee_included == 1) return parseFloat(this.item.sale_price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.sale_price;
    },
    namelength: function namelength() {
      var m = $("meta[name=locale-lang]");
      var lang = m.attr("content");
      var prodname = this.item.name;
      if (lang == "cn") {
        if (this.item.cn_name) {
          prodname = this.item.cn_name;
        } else {
          prodname = this.item.name;
        }
      }

      if (prodname.length > 38) {
        var name = prodname.substr(0, 38) + "...";
        return name;
      } else {
        return prodname;
      }
    },
    starRating: function starRating() {
      return "width:" + Math.round(this.item.rate / this.item.count * 20) + "%";
    },
    productRating: function productRating() {
      return "width:" + this.item.product_rating + "%";
    }
  }
});

/***/ }),

/***/ 198:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('product-item', __webpack_require__(35));

Vue.filter('currency', function (value) {
	return numberWithCommas(value.toFixed(2));
});

Vue.filter('round', function (value) {
	return roundOff(value);
});

var app = new Vue({
	el: '#related_products',
	data: {
		items: [{
			id: 1,
			name: 'PhosphorusGrey Melange Printed V Neck T-Shirt',
			image: '/img/demo/vneck1.jpg',
			price: 3000,
			sale_price: 2000
		}, {
			id: 2,
			name: 'United Colors of BenettonNavy Blue Solid V Neck T Shirt',
			image: '/img/demo/vneck2.jpg',
			price: 2000,
			sale_price: 1500
		}, {
			id: 3,
			name: 'WranglerBlack V Neck T Shirt',
			image: '/img/demo/vneck3.jpg',
			price: 1850,
			sale_price: 1500
		}, {
			id: 4,
			name: 'Tagd New YorkGrey Printed V Neck T-Shirts',
			image: '/img/demo/vneck4.jpg',
			price: 2050,
			sale_price: 1699
		}, {
			id: 5,
			name: 'Penshoppe Polo Shirt',
			image: '/img/demo/polo2.jpg',
			price: 1999,
			sale_price: 999
		}, {
			id: 6,
			name: 'Penshoppe Polo Shirt',
			image: '/img/demo/polo2.jpg',
			price: 1999,
			sale_price: 999
		}, {
			id: 7,
			name: 'Penshoppe Polo Shirt',
			image: '/img/demo/polo2.jpg',
			price: 1999,
			sale_price: 999
		}],
		selected: []
	},
	methods: {
		selectItem: function selectItem(id) {
			this.selected.push(id);
		}
	}
});

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
	return Math.round(v);
}

/***/ }),

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(19),
  /* template */
  __webpack_require__(39),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\common\\Product.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Product.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-503845d2", Component.options)
  } else {
    hotAPI.reload("data-v-503845d2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "box-product-outer",
    staticStyle: {
      "margin": "0 auto"
    }
  }, [_c('div', {
    staticClass: "box-product"
  }, [_c('div', {
    staticClass: "img-wrapper",
    staticStyle: {
      "height": "auto"
    }
  }, [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_c('img', {
    staticClass: "imgthumb",
    attrs: {
      "alt": "Product",
      "src": '/shopping/images/product/' + _vm.item.id + '/primary.jpg'
    }
  })]), _vm._v(" "), (_vm.item.sale_price) ? _c('div', {
    staticClass: "tags"
  }, [_c('span', {
    staticClass: "label-tags"
  }, [_c('span', {
    staticClass: "label label-danger arrowed"
  }, [_vm._v(_vm._s(_vm.item.discount) + "%")])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "option"
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "data-placement": "top",
      "title": "Add to Cart",
      "data-original-title": "Add to Cart"
    },
    on: {
      "click": function($event) {
        _vm.addToCart()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-shopping-cart"
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Compare"
    },
    on: {
      "click": function($event) {
        _vm.addToCompare()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-align-left"
  })]), _vm._v(" "), _c('a', {
    staticClass: "wishlist",
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Wishlist"
    },
    on: {
      "click": function($event) {
        _vm.addToFavorites()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-heart"
  })])])]), _vm._v(" "), _c('h6', [_c('a', {
    staticClass: "product-name",
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_vm._v("\n        " + _vm._s(_vm.namelength) + "\n      ")])]), _vm._v(" "), _c('div', {
    staticClass: "price",
    staticStyle: {
      "margin-top": "-5px",
      "font-size": "14px"
    }
  }, [_c('div', [(_vm.item.discount && _vm.item.discount > 0) ? _c('span', [_vm._v("P" + _vm._s(_vm._f("currency")(parseFloat("0" + _vm.saleprice))))]) : _vm._e(), _vm._v(" "), _c('div', {
    staticStyle: {
      "display": "block"
    }
  }, [_c('span', {
    class: (_vm.item.discount && _vm.item.discount > 0) ? 'price-old' : '',
    staticStyle: {
      "margin-left": "0px"
    }
  }, [_vm._v("\n            P" + _vm._s(_vm._f("currency")(parseFloat("0" + _vm.price))) + "\n          ")]), _vm._v(" "), (_vm.item.discount == 0) ? _c('div', {
    staticStyle: {
      "height": "20px",
      "clear": "both"
    }
  }) : _vm._e(), _vm._v(" "), (_vm.item.discount && _vm.item.discount > 0) ? _c('span', {
    staticStyle: {
      "font-size": "13px",
      "color": "#666"
    }
  }, [_vm._v("-" + _vm._s(_vm.item.discount) + "%")]) : _vm._e()])])]), _vm._v(" "), _c('div', {
    staticClass: "rating-block"
  }, [_c('div', {
    staticClass: "star-ratings-sprite-xsmall"
  }, [_c('span', {
    staticClass: "star-ratings-sprite-xsmall-rating",
    style: (_vm.productRating)
  })])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-503845d2", module.exports)
  }
}

/***/ }),

/***/ 448:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(198);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vUHJvZHVjdC52dWU/OTA1YSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3JlbGF0ZWQtcHJvZHVjdHMuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NvbW1vbi9Qcm9kdWN0LnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvY29tbW9uL1Byb2R1Y3QudnVlPzVhNTIiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImZpbHRlciIsInZhbHVlIiwibnVtYmVyV2l0aENvbW1hcyIsInRvRml4ZWQiLCJyb3VuZE9mZiIsImFwcCIsImVsIiwiZGF0YSIsIml0ZW1zIiwiaWQiLCJuYW1lIiwiaW1hZ2UiLCJwcmljZSIsInNhbGVfcHJpY2UiLCJzZWxlY3RlZCIsIm1ldGhvZHMiLCJzZWxlY3RJdGVtIiwicHVzaCIsIngiLCJ0b1N0cmluZyIsInJlcGxhY2UiLCJ2IiwiTWF0aCIsInJvdW5kIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVEE7VUFFQTs7Z0NBRUE7OEJBQ0E7QUFDQTtvQ0FDQTtxREFDQTtBQUNBOzBDQUNBO3dEQUNBO0FBQ0E7OENBQ0E7MERBQ0E7QUFFQTtBQWJBOzs0QkFlQTtvQ0FDQSxpSUFFQSwyQkFDQTtBQUNBO29DQUNBO29DQUNBLHNJQUVBLDJCQUNBO0FBQ0E7c0NBQ0E7Z0JBQ0E7d0JBQ0E7K0JBQ0E7d0JBQ0E7K0JBQ0E7K0JBQ0E7QUFDQSxlQUNBOytCQUNBO0FBQ0E7QUFFQTs7Z0NBQ0E7NENBQ0E7ZUFDQTtBQUNBLGFBQ0E7ZUFDQTtBQUNBO0FBQ0E7c0NBQ0E7NEVBQ0E7QUFDQTs0Q0FDQTttREFDQTtBQUdBO0FBekNBO0FBaEJBLEc7Ozs7Ozs7QUMxQ0FBLElBQUlDLFNBQUosQ0FBYyxjQUFkLEVBQStCLG1CQUFBQyxDQUFRLEVBQVIsQ0FBL0I7O0FBRUFGLElBQUlHLE1BQUosQ0FBVyxVQUFYLEVBQXVCLFVBQVVDLEtBQVYsRUFBaUI7QUFDcEMsUUFBUUMsaUJBQWlCRCxNQUFNRSxPQUFOLENBQWMsQ0FBZCxDQUFqQixDQUFSO0FBQ0gsQ0FGRDs7QUFJQU4sSUFBSUcsTUFBSixDQUFXLE9BQVgsRUFBb0IsVUFBVUMsS0FBVixFQUFpQjtBQUNqQyxRQUFRRyxTQUFTSCxLQUFULENBQVI7QUFDSCxDQUZEOztBQUlBLElBQUlJLE1BQU0sSUFBSVIsR0FBSixDQUFRO0FBQ2pCUyxLQUFJLG1CQURhO0FBRWpCQyxPQUFNO0FBQ0xDLFNBQU0sQ0FDTDtBQUNFQyxPQUFJLENBRE47QUFFRUMsU0FBTSwrQ0FGUjtBQUdFQyxVQUFPLHNCQUhUO0FBSUVDLFVBQU0sSUFKUjtBQUtFQyxlQUFXO0FBTGIsR0FESyxFQVFMO0FBQ0VKLE9BQUksQ0FETjtBQUVFQyxTQUFNLHlEQUZSO0FBR0VDLFVBQU8sc0JBSFQ7QUFJRUMsVUFBTSxJQUpSO0FBS0VDLGVBQVc7QUFMYixHQVJLLEVBZUw7QUFDRUosT0FBSSxDQUROO0FBRUVDLFNBQU0sOEJBRlI7QUFHRUMsVUFBTyxzQkFIVDtBQUlFQyxVQUFNLElBSlI7QUFLRUMsZUFBVztBQUxiLEdBZkssRUFzQkw7QUFDRUosT0FBSSxDQUROO0FBRUVDLFNBQU0sMkNBRlI7QUFHRUMsVUFBTyxzQkFIVDtBQUlFQyxVQUFNLElBSlI7QUFLRUMsZUFBVztBQUxiLEdBdEJLLEVBNkJMO0FBQ0VKLE9BQUksQ0FETjtBQUVFQyxTQUFNLHNCQUZSO0FBR0VDLFVBQU8scUJBSFQ7QUFJRUMsVUFBTSxJQUpSO0FBS0VDLGVBQVc7QUFMYixHQTdCSyxFQW9DTDtBQUNFSixPQUFJLENBRE47QUFFRUMsU0FBTSxzQkFGUjtBQUdFQyxVQUFPLHFCQUhUO0FBSUVDLFVBQU0sSUFKUjtBQUtFQyxlQUFXO0FBTGIsR0FwQ0ssRUEyQ0w7QUFDRUosT0FBSSxDQUROO0FBRUVDLFNBQU0sc0JBRlI7QUFHRUMsVUFBTyxxQkFIVDtBQUlFQyxVQUFNLElBSlI7QUFLRUMsZUFBVztBQUxiLEdBM0NLLENBREQ7QUFvRExDLFlBQVM7QUFwREosRUFGVztBQXdEakJDLFVBQVE7QUFDUEMsWUFETyxzQkFDSVAsRUFESixFQUNPO0FBQ2IsUUFBS0ssUUFBTCxDQUFjRyxJQUFkLENBQW1CUixFQUFuQjtBQUNBO0FBSE07QUF4RFMsQ0FBUixDQUFWOztBQStEQSxTQUFTUCxnQkFBVCxDQUEwQmdCLENBQTFCLEVBQTZCO0FBQ3pCLFFBQU9BLEVBQUVDLFFBQUYsR0FBYUMsT0FBYixDQUFxQix1QkFBckIsRUFBOEMsR0FBOUMsQ0FBUDtBQUNIOztBQUVELFNBQVNoQixRQUFULENBQWtCaUIsQ0FBbEIsRUFBcUI7QUFDakIsUUFBT0MsS0FBS0MsS0FBTCxDQUFXRixDQUFYLENBQVA7QUFDSCxDOzs7Ozs7O0FDL0VEO0FBQ0E7QUFDQSx3QkFBcUo7QUFDcko7QUFDQSx3QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3BhZ2VzL3JlbGF0ZWQtcHJvZHVjdHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ0OCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsIjx0ZW1wbGF0ZT5cclxuICAgIDxkaXYgY2xhc3M9XCJib3gtcHJvZHVjdC1vdXRlclwiIHN0eWxlPVwibWFyZ2luOjAgYXV0bztcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImJveC1wcm9kdWN0XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImltZy13cmFwcGVyXCIgc3R5bGU9XCJoZWlnaHQ6YXV0bztcIj5cclxuICAgICAgICAgIDxhIDpocmVmPVwiJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcIj5cclxuICAgICAgICAgIDxpbWcgYWx0PVwiUHJvZHVjdFwiIHYtYmluZDpzcmM9XCInL3Nob3BwaW5nL2ltYWdlcy9wcm9kdWN0LycraXRlbS5pZCsnL3ByaW1hcnkuanBnJ1wiIGNsYXNzPVwiaW1ndGh1bWJcIj5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0YWdzXCIgdi1pZj0naXRlbS5zYWxlX3ByaWNlJz5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJsYWJlbC10YWdzXCI+PHNwYW4gY2xhc3M9XCJsYWJlbCBsYWJlbC1kYW5nZXIgYXJyb3dlZFwiPnt7aXRlbS5kaXNjb3VudH19JTwvc3Bhbj48L3NwYW4+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJvcHRpb25cIj5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIGRhdGEtcGxhY2VtZW50PVwidG9wXCIgdGl0bGU9XCJBZGQgdG8gQ2FydFwiIGRhdGEtb3JpZ2luYWwtdGl0bGU9XCJBZGQgdG8gQ2FydFwiIEBjbGljaz1cImFkZFRvQ2FydCgpXCI+PGkgY2xhc3M9XCJmYSBmYS1zaG9wcGluZy1jYXJ0XCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiQWRkIHRvIENvbXBhcmVcIiBAY2xpY2s9XCJhZGRUb0NvbXBhcmUoKVwiPjxpIGNsYXNzPVwiZmEgZmEtYWxpZ24tbGVmdFwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiB0aXRsZT1cIkFkZCB0byBXaXNobGlzdFwiIGNsYXNzPVwid2lzaGxpc3RcIiBAY2xpY2s9XCJhZGRUb0Zhdm9yaXRlcygpXCI+PGkgY2xhc3M9XCJmYSBmYS1oZWFydFwiPjwvaT48L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8aDY+XHJcbiAgICAgICAgICA8YSBjbGFzcz1cInByb2R1Y3QtbmFtZVwiIDpocmVmPVwiJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcIj5cclxuICAgICAgICAgICAge3sgbmFtZWxlbmd0aCB9fVxyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgIDwvaDY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInByaWNlXCIgc3R5bGU9XCJtYXJnaW4tdG9wOi01cHg7IGZvbnQtc2l6ZToxNHB4O1wiPlxyXG4gICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgPHNwYW4gdi1pZj0naXRlbS5kaXNjb3VudCAmJiBpdGVtLmRpc2NvdW50ID4gMCc+UHt7cGFyc2VGbG9hdChcIjBcIitzYWxlcHJpY2UpIHwgY3VycmVuY3l9fTwvc3Bhbj5cclxuICAgICAgICAgICAgPGRpdiBzdHlsZT1cImRpc3BsYXk6YmxvY2s7XCI+XHJcbiAgICAgICAgICAgICAgPHNwYW4gOmNsYXNzPVwiKGl0ZW0uZGlzY291bnQgJiYgaXRlbS5kaXNjb3VudCA+IDApPydwcmljZS1vbGQnOicnXCIgc3R5bGU9XCJtYXJnaW4tbGVmdDowcHg7XCI+XHJcbiAgICAgICAgICAgICAgICBQe3twYXJzZUZsb2F0KFwiMFwiK3ByaWNlKSB8IGN1cnJlbmN5fX1cclxuICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPGRpdiB2LWlmPVwiaXRlbS5kaXNjb3VudCA9PSAwXCIgc3R5bGU9XCJoZWlnaHQ6MjBweDsgY2xlYXI6Ym90aDtcIj48L2Rpdj5cclxuICAgICAgICAgICAgICA8c3BhbiB2LWlmPSdpdGVtLmRpc2NvdW50ICYmIGl0ZW0uZGlzY291bnQgPiAwJyBzdHlsZT1cImZvbnQtc2l6ZToxM3B4OyBjb2xvcjojNjY2O1wiPi17eyBpdGVtLmRpc2NvdW50IH19JTwvc3Bhbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwicmF0aW5nLWJsb2NrXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXhzbWFsbFwiPjxzcGFuIDpzdHlsZT1cInByb2R1Y3RSYXRpbmdcIiBjbGFzcz1cInN0YXItcmF0aW5ncy1zcHJpdGUteHNtYWxsLXJhdGluZ1wiPjwvc3Bhbj48L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICBwcm9wczogWydpdGVtJ10sXHJcbiAgbWV0aG9kczp7XHJcbiAgICBzZWxlY3QoaWQpe1xyXG4gICAgICAgdGhpcy4kcGFyZW50LnNlbGVjdEl0ZW0oaWQpXHJcbiAgICB9LFxyXG4gICAgYWRkVG9DYXJ0KCl7XHJcbiAgICAgICB0aGlzLiRwYXJlbnQuYWRkVG9DYXJ0KHRoaXMuaXRlbS5pZCx0aGlzLml0ZW0ubmFtZSlcclxuICAgIH0sXHJcbiAgICBhZGRUb0NvbXBhcmUoKXtcclxuICAgICAgIHRoaXMuJHBhcmVudC5hZGRUb0NvbXBhcmUodGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5uYW1lKVxyXG4gICAgfSxcclxuICAgIGFkZFRvRmF2b3JpdGVzKCl7XHJcbiAgICAgICB0aGlzLiRwYXJlbnQuYWRkVG9GYXZvcml0ZXModGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5uYW1lKVxyXG4gICAgfVxyXG4gIH0sXHJcbiAgY29tcHV0ZWQ6e1xyXG4gICAgcHJpY2UoKXtcclxuICAgICAgaWYodGhpcy5pdGVtLmZlZV9pbmNsdWRlZD09MSlcclxuICAgICAgICByZXR1cm4gcGFyc2VGbG9hdCh0aGlzLml0ZW0ucHJpY2UpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0uc2hpcHBpbmdfZmVlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLmNoYXJnZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS52YXQpO1xyXG4gICAgICBlbHNlXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXRlbS5wcmljZTtcclxuICAgIH0sXHJcbiAgICBzYWxlcHJpY2UoKXtcclxuICAgICAgaWYodGhpcy5pdGVtLmZlZV9pbmNsdWRlZD09MSlcclxuICAgICAgICByZXR1cm4gcGFyc2VGbG9hdCh0aGlzLml0ZW0uc2FsZV9wcmljZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0uY2hhcmdlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLnZhdCk7XHJcbiAgICAgIGVsc2VcclxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtLnNhbGVfcHJpY2U7XHJcbiAgICB9LFxyXG4gICAgbmFtZWxlbmd0aCgpe1xyXG4gICAgICB2YXIgbSA9ICQoXCJtZXRhW25hbWU9bG9jYWxlLWxhbmddXCIpOyAgICBcclxuICAgICAgdmFyIGxhbmcgPSBtLmF0dHIoXCJjb250ZW50XCIpO1xyXG4gICAgICB2YXIgcHJvZG5hbWUgPSB0aGlzLml0ZW0ubmFtZTtcclxuICAgICAgaWYobGFuZyA9PSBcImNuXCIpe1xyXG4gICAgICAgIGlmKHRoaXMuaXRlbS5jbl9uYW1lKXtcclxuICAgICAgICAgIHByb2RuYW1lID0gdGhpcy5pdGVtLmNuX25hbWU7IFxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgcHJvZG5hbWUgPSB0aGlzLml0ZW0ubmFtZTsgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAocHJvZG5hbWUubGVuZ3RoID4gMzgpIHtcclxuICAgICAgICB2YXIgbmFtZSA9IHByb2RuYW1lLnN1YnN0cigwLCAzOCkrXCIuLi5cIjtcclxuICAgICAgICByZXR1cm4gbmFtZTtcclxuICAgICAgfVxyXG4gICAgICBlbHNle1xyXG4gICAgICAgIHJldHVybiBwcm9kbmFtZTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHN0YXJSYXRpbmcoKXtcclxuICAgICAgcmV0dXJuIFwid2lkdGg6XCIrTWF0aC5yb3VuZCgodGhpcy5pdGVtLnJhdGUvdGhpcy5pdGVtLmNvdW50KSoyMCkrXCIlXCI7XHJcbiAgICB9LFxyXG4gICAgcHJvZHVjdFJhdGluZygpe1xyXG4gICAgICByZXR1cm4gXCJ3aWR0aDpcIit0aGlzLml0ZW0ucHJvZHVjdF9yYXRpbmcrXCIlXCI7XHJcbiAgICB9LFxyXG5cclxuICB9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFByb2R1Y3QudnVlPzQ5NmRkYjIwIiwiVnVlLmNvbXBvbmVudCgncHJvZHVjdC1pdGVtJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvY29tbW9uL1Byb2R1Y3QudnVlJykpO1xyXG5cclxuVnVlLmZpbHRlcignY3VycmVuY3knLCBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgIHJldHVybiAgbnVtYmVyV2l0aENvbW1hcyh2YWx1ZS50b0ZpeGVkKDIpKTtcclxufSk7XHJcblxyXG5WdWUuZmlsdGVyKCdyb3VuZCcsIGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuICByb3VuZE9mZih2YWx1ZSk7XHJcbn0pO1xyXG5cclxudmFyIGFwcCA9IG5ldyBWdWUoe1xyXG5cdGVsOiAnI3JlbGF0ZWRfcHJvZHVjdHMnLFxyXG5cdGRhdGE6IHtcclxuXHRcdGl0ZW1zOltcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCBpZDogMVxyXG5cdFx0XHRcdCxuYW1lOiAnUGhvc3Bob3J1c0dyZXkgTWVsYW5nZSBQcmludGVkIFYgTmVjayBULVNoaXJ0J1xyXG5cdFx0XHRcdCxpbWFnZTogJy9pbWcvZGVtby92bmVjazEuanBnJ1xyXG5cdFx0XHRcdCxwcmljZTozMDAwXHJcblx0XHRcdFx0LHNhbGVfcHJpY2U6MjAwMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0IGlkOiAyXHJcblx0XHRcdFx0LG5hbWU6ICdVbml0ZWQgQ29sb3JzIG9mIEJlbmV0dG9uTmF2eSBCbHVlIFNvbGlkIFYgTmVjayBUIFNoaXJ0J1xyXG5cdFx0XHRcdCxpbWFnZTogJy9pbWcvZGVtby92bmVjazIuanBnJ1xyXG5cdFx0XHRcdCxwcmljZToyMDAwXHJcblx0XHRcdFx0LHNhbGVfcHJpY2U6MTUwMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0IGlkOiAzXHJcblx0XHRcdFx0LG5hbWU6ICdXcmFuZ2xlckJsYWNrIFYgTmVjayBUIFNoaXJ0J1xyXG5cdFx0XHRcdCxpbWFnZTogJy9pbWcvZGVtby92bmVjazMuanBnJ1xyXG5cdFx0XHRcdCxwcmljZToxODUwXHJcblx0XHRcdFx0LHNhbGVfcHJpY2U6MTUwMFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0IGlkOiA0XHJcblx0XHRcdFx0LG5hbWU6ICdUYWdkIE5ldyBZb3JrR3JleSBQcmludGVkIFYgTmVjayBULVNoaXJ0cydcclxuXHRcdFx0XHQsaW1hZ2U6ICcvaW1nL2RlbW8vdm5lY2s0LmpwZydcclxuXHRcdFx0XHQscHJpY2U6MjA1MFxyXG5cdFx0XHRcdCxzYWxlX3ByaWNlOjE2OTlcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCBpZDogNVxyXG5cdFx0XHRcdCxuYW1lOiAnUGVuc2hvcHBlIFBvbG8gU2hpcnQnXHJcblx0XHRcdFx0LGltYWdlOiAnL2ltZy9kZW1vL3BvbG8yLmpwZydcclxuXHRcdFx0XHQscHJpY2U6MTk5OVxyXG5cdFx0XHRcdCxzYWxlX3ByaWNlOjk5OVxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0IGlkOiA2XHJcblx0XHRcdFx0LG5hbWU6ICdQZW5zaG9wcGUgUG9sbyBTaGlydCdcclxuXHRcdFx0XHQsaW1hZ2U6ICcvaW1nL2RlbW8vcG9sbzIuanBnJ1xyXG5cdFx0XHRcdCxwcmljZToxOTk5XHJcblx0XHRcdFx0LHNhbGVfcHJpY2U6OTk5XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHQgaWQ6IDdcclxuXHRcdFx0XHQsbmFtZTogJ1BlbnNob3BwZSBQb2xvIFNoaXJ0J1xyXG5cdFx0XHRcdCxpbWFnZTogJy9pbWcvZGVtby9wb2xvMi5qcGcnXHJcblx0XHRcdFx0LHByaWNlOjE5OTlcclxuXHRcdFx0XHQsc2FsZV9wcmljZTo5OTlcclxuXHRcdFx0fSxcclxuXHRcdF0sXHJcblx0XHRzZWxlY3RlZDpbXVxyXG5cdH0sXHJcblx0bWV0aG9kczp7XHJcblx0XHRzZWxlY3RJdGVtKGlkKXtcclxuXHRcdFx0dGhpcy5zZWxlY3RlZC5wdXNoKGlkKVxyXG5cdFx0fVxyXG5cdH1cclxufSlcclxuXHJcbmZ1bmN0aW9uIG51bWJlcldpdGhDb21tYXMoeCkge1xyXG4gICAgcmV0dXJuIHgudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIixcIik7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJvdW5kT2ZmKHYpIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKHYpO1xyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9yZWxhdGVkLXByb2R1Y3RzLmpzIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vUHJvZHVjdC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTUwMzg0NWQyXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1Byb2R1Y3QudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXGNvbW1vblxcXFxQcm9kdWN0LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIFByb2R1Y3QudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTUwMzg0NWQyXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNTAzODQ1ZDJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NvbW1vbi9Qcm9kdWN0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzVcbi8vIG1vZHVsZSBjaHVua3MgPSAyNSAyNiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJveC1wcm9kdWN0LW91dGVyXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luXCI6IFwiMCBhdXRvXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJveC1wcm9kdWN0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1nLXdyYXBwZXJcIixcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCJhdXRvXCJcbiAgICB9XG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdC8nICsgX3ZtLml0ZW0uc2x1Z1xuICAgIH1cbiAgfSwgW19jKCdpbWcnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1ndGh1bWJcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJhbHRcIjogXCJQcm9kdWN0XCIsXG4gICAgICBcInNyY1wiOiAnL3Nob3BwaW5nL2ltYWdlcy9wcm9kdWN0LycgKyBfdm0uaXRlbS5pZCArICcvcHJpbWFyeS5qcGcnXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLml0ZW0uc2FsZV9wcmljZSkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhZ3NcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWwtdGFnc1wiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1kYW5nZXIgYXJyb3dlZFwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLmRpc2NvdW50KSArIFwiJVwiKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm9wdGlvblwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkFkZCB0byBDYXJ0XCIsXG4gICAgICBcImRhdGEtb3JpZ2luYWwtdGl0bGVcIjogXCJBZGQgdG8gQ2FydFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmFkZFRvQ2FydCgpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtc2hvcHBpbmctY2FydFwiXG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiQWRkIHRvIENvbXBhcmVcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5hZGRUb0NvbXBhcmUoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWFsaWduLWxlZnRcIlxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ3aXNobGlzdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiQWRkIHRvIFdpc2hsaXN0XCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uYWRkVG9GYXZvcml0ZXMoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWhlYXJ0XCJcbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdoNicsIFtfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwcm9kdWN0LW5hbWVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdC8nICsgX3ZtLml0ZW0uc2x1Z1xuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLm5hbWVsZW5ndGgpICsgXCJcXG4gICAgICBcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInByaWNlXCIsXG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwibWFyZ2luLXRvcFwiOiBcIi01cHhcIixcbiAgICAgIFwiZm9udC1zaXplXCI6IFwiMTRweFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIFsoX3ZtLml0ZW0uZGlzY291bnQgJiYgX3ZtLml0ZW0uZGlzY291bnQgPiAwKSA/IF9jKCdzcGFuJywgW192bS5fdihcIlBcIiArIF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShwYXJzZUZsb2F0KFwiMFwiICsgX3ZtLnNhbGVwcmljZSkpKSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImRpc3BsYXlcIjogXCJibG9ja1wiXG4gICAgfVxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgY2xhc3M6IChfdm0uaXRlbS5kaXNjb3VudCAmJiBfdm0uaXRlbS5kaXNjb3VudCA+IDApID8gJ3ByaWNlLW9sZCcgOiAnJyxcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJtYXJnaW4tbGVmdFwiOiBcIjBweFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgUFwiICsgX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHBhcnNlRmxvYXQoXCIwXCIgKyBfdm0ucHJpY2UpKSkgKyBcIlxcbiAgICAgICAgICBcIildKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5pdGVtLmRpc2NvdW50ID09IDApID8gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNTdHlsZToge1xuICAgICAgXCJoZWlnaHRcIjogXCIyMHB4XCIsXG4gICAgICBcImNsZWFyXCI6IFwiYm90aFwiXG4gICAgfVxuICB9KSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLml0ZW0uZGlzY291bnQgJiYgX3ZtLml0ZW0uZGlzY291bnQgPiAwKSA/IF9jKCdzcGFuJywge1xuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcImZvbnQtc2l6ZVwiOiBcIjEzcHhcIixcbiAgICAgIFwiY29sb3JcIjogXCIjNjY2XCJcbiAgICB9XG4gIH0sIFtfdm0uX3YoXCItXCIgKyBfdm0uX3MoX3ZtLml0ZW0uZGlzY291bnQpICsgXCIlXCIpXSkgOiBfdm0uX2UoKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyYXRpbmctYmxvY2tcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXhzbWFsbFwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXhzbWFsbC1yYXRpbmdcIixcbiAgICBzdHlsZTogKF92bS5wcm9kdWN0UmF0aW5nKVxuICB9KV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTUwMzg0NWQyXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNTAzODQ1ZDJcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9jb21tb24vUHJvZHVjdC52dWVcbi8vIG1vZHVsZSBpZCA9IDM5XG4vLyBtb2R1bGUgY2h1bmtzID0gMjUgMjYiXSwic291cmNlUm9vdCI6IiJ9