/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 441);
/******/ })
/************************************************************************/
/******/ ({

/***/ 191:
/***/ (function(module, exports) {

$(document).ready(function () {

    page_open = window.location.search.substr(6);

    $('.btnUpload').click(function () {
        $('.avatar-uploader input').click();
    }

    //individual
    );$('.btnNbiUpload').click(function () {
        $('.nbi-uploader input').click();
    });
    $('.btnBirthCertUpload').click(function () {
        $('.birthCert-uploader input').click();
    });
    $('.btnGovIdUpload').click(function () {
        $('.govID-uploader input').click();
    }

    //enterprise
    // COMMENTED THIS 10/12/17
    // $('.btnSecUpload').click(function(){
    //     $('.sec-uploader input').click();
    // })
    // $('.btnBirUpload').click(function(){
    //     $('.bir-uploader input').click();
    // })
    // $('.btnPermitUpload').click(function(){
    //     $('.permit-uploader input').click();
    // })

    // $('.btnNbiUpload').click(function(){
    //     $('.nbi-uploader input').click();
    // })


    );if (page_open) {
        if ($('#profile-settings-container #' + page_open).length) {

            $('#' + page_open).addClass('active');
            return;
        }
        $('.tab-pane:first').addClass('active');
    } else {
        $('.tab-pane:first').addClass('active');
    }

    $('#modalMenu').on('shown.bs.modal', function (e) {
        $('button[data-target="#modalMenu"] span:eq(0)').removeClass().addClass('fa fa-times');
    });

    $('#modalMenu').on('hidden.bs.modal', function (e) {
        $('button[data-target="#modalMenu"] span:eq(0)').removeClass().addClass('fa fa fa-bars');
    });

    $('#modalMenu table tr').on('click', function () {
        $('#modalMenu').modal('hide');
    });
});

var profileMenu = new Vue({
    el: '#profile-menu',
    data: {
        modalMenuShowing: false,
        friends: [],
        requests: [],
        recommended: [],
        selected: [],
        notifications: [],
        profileImage: '',
        is_docs_verified: '',
        lang: [],
        form: new Form({
            avatar: ''
        }, { baseURL: 'http://' + Laravel.base_api_url })

    },
    methods: {
        show: function show(event) {
            var elShow = event.target.name;
            if (elShow == 'show_product') {
                $('#show_product2').show();
                $('#show_stores2').hide();
                $('#show_friends2').hide();
            } else if (elShow == 'show_stores') {
                $('#show_product2').hide();
                $('#show_stores2').show();
                $('#show_friends2').hide();
            } else if (elShow == 'show_friends') {
                $('#show_product2').hide();
                $('#show_stores2').hide();
                $('#show_friends2').show();
            }
        },
        selectItem: function selectItem(id) {
            this.selected.push(id);
        },
        getFriendsData: function getFriendsData() {
            var _this = this;

            function getFriend() {
                return axiosAPIv1.get('/friends');
            }

            axios.all([getFriend()]).then(axios.spread(function (data) {
                _this.friends = data.data.friends;
                _this.requests = data.data.requests;
            })).catch($.noop);
        },
        getNotifications: function getNotifications() {
            var _this2 = this;

            function getFriend() {
                return axiosAPIv1.get('/friends');
            }

            axios.all([getFriend()]).then(axios.spread(function (data) {
                _this2.friends = data.data.friends;
                _this2.requests = data.data.requests;
            })).catch($.noop);
        }
    },
    created: function created() {
        var _this3 = this;

        $('.modal-body .menu-holder').find('tr').click(function () {
            $('#modalMenu').modal('toggle');
        });
        axios.get('/translate/profile-settings').then(function (result) {
            _this3.lang = result.data;
        });
        this.getFriendsData();

        this.profileImage = Laravel.user.avatar;
        this.is_docs_verified = Laravel.user.is_docs_verified;
        Event.listen('Imgfileupload.avatarUpload', function (img) {
            _this3.profileImage = img;
        });

        var title_status = this.is_docs_verified ? "Verified User" : "Not yet Verified";
        var class_status = this.is_docs_verified ? "tc-green fa-check-circle" : "text-warning fa-exclamation-circle";
        $('#avatar-container span').attr('data-original-title', title_status);
        $('#avatar-container span span').attr('class', "mouse-hand  fa fa-2x " + class_status);
    }
});

var dataContainer = new Vue({
    el: '#data-area',
    data: {
        ewallet: 0,
        storeCount: 0,
        wishCount: 0
    },
    mounted: function mounted() {
        var _this4 = this;

        function getEwallet() {
            return axiosAPIv1.get('/user/' + Laravel.user.id + '/ewallet');
        }
        function getStores() {
            return axiosAPIv1.get('/users/' + Laravel.user.id + '/stores');
        }
        function getWishlist() {
            return axiosAPIv1.get('/getfavoritestotal');
        }

        axios.all([getEwallet(), getStores(), getWishlist()]).then(axios.spread(function (ewallet, stores, wishlist) {

            _this4.ewallet = parseFloat(ewallet.data.data.amount);
            _this4.storeCount = stores.data.length;
            _this4.wishCount = wishlist.data;
        })).catch($.noop);
    }
});

// Vue.component('product-item',  require('../components/store/management/Product.vue'));


// Vue.filter('round', function (value) {
//     return  roundOff(value);
// });

// var app = new Vue({
//   el: '#profile-container',
//   data: {
//     friends:[
//       {
//         id:1,
//         name:'Anna',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:2,
//         name:'Annie',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:3,
//         name:'John',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:4,
//         name:'Johnnie',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       }
//     ],
//     requests:[
//       {
//         id:5,
//         name:'Juanito',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:6,
//         name:'Juanita',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       }
//     ],
//     recommended:[
//       {
//         id:7,
//         name:'Charles',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:8,
//         name:'Arianne',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:9,
//         name:'Aries',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:10,
//         name:'Carl',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:11,
//         name:'Kevin',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:12,
//         name:'Charlie',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       }
//     ],
//     showRequest:true,
//     showFriends:false,
//     showRecommended:false,
//     items:[
//       {
//         id: 1
//         ,name: 'PhosphorusGrey Melange Printed V Neck T-Shirt'
//         ,image: '/img/demo/vneck1.jpg'
//         ,price:3000
//         ,sale_price:2000
//       },
//       {
//         id: 2
//         ,name: 'United Colors of BenettonNavy Blue Solid V Neck T Shirt'
//         ,image: '/img/demo/vneck2.jpg'
//         ,price:2000
//         ,sale_price:1500
//       },
//       {
//         id: 3
//         ,name: 'WranglerBlack V Neck T Shirt'
//         ,image: '/img/demo/vneck3.jpg'
//         ,price:1850
//         ,sale_price:1500
//       },
//       {
//         id: 4
//         ,name: 'Tagd New YorkGrey Printed V Neck T-Shirts'
//         ,image: '/img/demo/vneck4.jpg'
//         ,price:2050
//         ,sale_price:1699
//       },
//       {
//          id: 5
//         ,name: 'Penshoppe Polo Shirt'
//         ,image: '/img/demo/polo2.jpg'
//         ,price:1999
//         ,sale_price:999
//       },
//     ],
//     selected:[]
//   },
//   methods:{
//     show:function(event){
//       var elShow = event.target.name;
//       if(elShow == 'show_product'){
//         $('#show_product2').show();
//         $('#show_stores2').hide();
//         $('#show_friends2').hide();
//       }else if(elShow == 'show_stores'){
//         $('#show_product2').hide();
//         $('#show_stores2').show();
//         $('#show_friends2').hide();
//       }else if(elShow == 'show_friends'){
//         $('#show_product2').hide();
//         $('#show_stores2').hide();
//         $('#show_friends2').show();
//       }
//     },
//     selectItem(id){
//       this.selected.push(id)
//     },

//     clkShowRequest: function(){
//       this.showRequest = true;
//       this.showFriends = false;
//       this.showRecommended = false;
//     },
//     clkShowFriends: function(){
//       this.showRequest = false;
//       this.showFriends = true;
//       this.showRecommended = false;
//     },
//     clkShowRecommended: function(){
//       this.showRequest = false;
//       this.showFriends = false;
//       this.showRecommended = true;
//     },
//   }
// })

// var friends = new Vue({
//   el: '#friends_container',
//   data: {},
//   methods:{}
// })


// function numberWithCommas(x) {
//     return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }

// function roundOff(v) {
//     return Math.round(v);
// }

/***/ }),

/***/ 441:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(191);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvcHJvZmlsZS5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsInBhZ2Vfb3BlbiIsIndpbmRvdyIsImxvY2F0aW9uIiwic2VhcmNoIiwic3Vic3RyIiwiY2xpY2siLCJsZW5ndGgiLCJhZGRDbGFzcyIsIm9uIiwiZSIsInJlbW92ZUNsYXNzIiwibW9kYWwiLCJwcm9maWxlTWVudSIsIlZ1ZSIsImVsIiwiZGF0YSIsIm1vZGFsTWVudVNob3dpbmciLCJmcmllbmRzIiwicmVxdWVzdHMiLCJyZWNvbW1lbmRlZCIsInNlbGVjdGVkIiwibm90aWZpY2F0aW9ucyIsInByb2ZpbGVJbWFnZSIsImlzX2RvY3NfdmVyaWZpZWQiLCJsYW5nIiwiZm9ybSIsIkZvcm0iLCJhdmF0YXIiLCJiYXNlVVJMIiwiTGFyYXZlbCIsImJhc2VfYXBpX3VybCIsIm1ldGhvZHMiLCJzaG93IiwiZXZlbnQiLCJlbFNob3ciLCJ0YXJnZXQiLCJuYW1lIiwiaGlkZSIsInNlbGVjdEl0ZW0iLCJpZCIsInB1c2giLCJnZXRGcmllbmRzRGF0YSIsImdldEZyaWVuZCIsImF4aW9zQVBJdjEiLCJnZXQiLCJheGlvcyIsImFsbCIsInRoZW4iLCJzcHJlYWQiLCJjYXRjaCIsIm5vb3AiLCJnZXROb3RpZmljYXRpb25zIiwiY3JlYXRlZCIsImZpbmQiLCJyZXN1bHQiLCJ1c2VyIiwiRXZlbnQiLCJsaXN0ZW4iLCJpbWciLCJ0aXRsZV9zdGF0dXMiLCJjbGFzc19zdGF0dXMiLCJhdHRyIiwiZGF0YUNvbnRhaW5lciIsImV3YWxsZXQiLCJzdG9yZUNvdW50Iiwid2lzaENvdW50IiwibW91bnRlZCIsImdldEV3YWxsZXQiLCJnZXRTdG9yZXMiLCJnZXRXaXNobGlzdCIsInN0b3JlcyIsIndpc2hsaXN0IiwicGFyc2VGbG9hdCIsImFtb3VudCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQUEsRUFBRUMsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVU7O0FBR3hCQyxnQkFBWUMsT0FBT0MsUUFBUCxDQUFnQkMsTUFBaEIsQ0FBdUJDLE1BQXZCLENBQThCLENBQTlCLENBQVo7O0FBRUFQLE1BQUUsWUFBRixFQUFnQlEsS0FBaEIsQ0FBc0IsWUFBVTtBQUM1QlIsVUFBRSx3QkFBRixFQUE0QlEsS0FBNUI7QUFDSDs7QUFFRDtBQUpBLE1BS0FSLEVBQUUsZUFBRixFQUFtQlEsS0FBbkIsQ0FBeUIsWUFBVTtBQUMvQlIsVUFBRSxxQkFBRixFQUF5QlEsS0FBekI7QUFDSCxLQUZEO0FBR0FSLE1BQUUscUJBQUYsRUFBeUJRLEtBQXpCLENBQStCLFlBQVU7QUFDckNSLFVBQUUsMkJBQUYsRUFBK0JRLEtBQS9CO0FBQ0gsS0FGRDtBQUdBUixNQUFFLGlCQUFGLEVBQXFCUSxLQUFyQixDQUEyQixZQUFVO0FBQ2pDUixVQUFFLHVCQUFGLEVBQTJCUSxLQUEzQjtBQUNIOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFsQkEsTUFzQkEsSUFBR0wsU0FBSCxFQUNBO0FBQ0ksWUFBR0gsRUFBRSxrQ0FBZ0NHLFNBQWxDLEVBQTZDTSxNQUFoRCxFQUNBOztBQUVJVCxjQUFFLE1BQUlHLFNBQU4sRUFBaUJPLFFBQWpCLENBQTBCLFFBQTFCO0FBQ0E7QUFDSDtBQUNEVixVQUFFLGlCQUFGLEVBQXFCVSxRQUFyQixDQUE4QixRQUE5QjtBQUNILEtBVEQsTUFVQTtBQUNJVixVQUFFLGlCQUFGLEVBQXFCVSxRQUFyQixDQUE4QixRQUE5QjtBQUNIOztBQUVEVixNQUFFLFlBQUYsRUFBZ0JXLEVBQWhCLENBQW1CLGdCQUFuQixFQUFxQyxVQUFVQyxDQUFWLEVBQWE7QUFDOUNaLFVBQUUsNkNBQUYsRUFBaURhLFdBQWpELEdBQStESCxRQUEvRCxDQUF3RSxhQUF4RTtBQUNILEtBRkQ7O0FBSUFWLE1BQUUsWUFBRixFQUFnQlcsRUFBaEIsQ0FBbUIsaUJBQW5CLEVBQXNDLFVBQVVDLENBQVYsRUFBYTtBQUMvQ1osVUFBRSw2Q0FBRixFQUFpRGEsV0FBakQsR0FBK0RILFFBQS9ELENBQXdFLGVBQXhFO0FBQ0gsS0FGRDs7QUFJQVYsTUFBRSxxQkFBRixFQUF5QlcsRUFBekIsQ0FBNEIsT0FBNUIsRUFBb0MsWUFBVztBQUMzQ1gsVUFBRSxZQUFGLEVBQWdCYyxLQUFoQixDQUFzQixNQUF0QjtBQUNILEtBRkQ7QUFJSCxDQWhFRDs7QUFtRUEsSUFBSUMsY0FBYyxJQUFJQyxHQUFKLENBQVE7QUFDdEJDLFFBQUksZUFEa0I7QUFFdEJDLFVBQUs7QUFDREMsMEJBQWlCLEtBRGhCO0FBRURDLGlCQUFRLEVBRlA7QUFHREMsa0JBQVMsRUFIUjtBQUlEQyxxQkFBWSxFQUpYO0FBS0RDLGtCQUFTLEVBTFI7QUFNREMsdUJBQWMsRUFOYjtBQU9EQyxzQkFBYSxFQVBaO0FBUURDLDBCQUFpQixFQVJoQjtBQVNEQyxjQUFLLEVBVEo7QUFVREMsY0FBTSxJQUFJQyxJQUFKLENBQVM7QUFDWkMsb0JBQU87QUFESyxTQUFULEVBRUgsRUFBRUMsU0FBUyxZQUFVQyxRQUFRQyxZQUE3QixFQUZHOztBQVZMLEtBRmlCO0FBbUJ0QkMsYUFBUTtBQUNKQyxjQUFLLGNBQVNDLEtBQVQsRUFBZTtBQUNoQixnQkFBSUMsU0FBU0QsTUFBTUUsTUFBTixDQUFhQyxJQUExQjtBQUNBLGdCQUFHRixVQUFVLGNBQWIsRUFBNEI7QUFDeEJyQyxrQkFBRSxnQkFBRixFQUFvQm1DLElBQXBCO0FBQ0FuQyxrQkFBRSxlQUFGLEVBQW1Cd0MsSUFBbkI7QUFDQXhDLGtCQUFFLGdCQUFGLEVBQW9Cd0MsSUFBcEI7QUFDSCxhQUpELE1BSU0sSUFBR0gsVUFBVSxhQUFiLEVBQTJCO0FBQzdCckMsa0JBQUUsZ0JBQUYsRUFBb0J3QyxJQUFwQjtBQUNBeEMsa0JBQUUsZUFBRixFQUFtQm1DLElBQW5CO0FBQ0FuQyxrQkFBRSxnQkFBRixFQUFvQndDLElBQXBCO0FBQ0gsYUFKSyxNQUlBLElBQUdILFVBQVUsY0FBYixFQUE0QjtBQUM5QnJDLGtCQUFFLGdCQUFGLEVBQW9Cd0MsSUFBcEI7QUFDQXhDLGtCQUFFLGVBQUYsRUFBbUJ3QyxJQUFuQjtBQUNBeEMsa0JBQUUsZ0JBQUYsRUFBb0JtQyxJQUFwQjtBQUNIO0FBQ0osU0FoQkc7QUFpQkpNLGtCQWpCSSxzQkFpQk9DLEVBakJQLEVBaUJVO0FBQ1YsaUJBQUtuQixRQUFMLENBQWNvQixJQUFkLENBQW1CRCxFQUFuQjtBQUNILFNBbkJHO0FBb0JKRSxzQkFwQkksNEJBb0JZO0FBQUE7O0FBQ1oscUJBQVNDLFNBQVQsR0FBcUI7QUFDakIsdUJBQU9DLFdBQVdDLEdBQVgsQ0FBZSxVQUFmLENBQVA7QUFDSDs7QUFFREMsa0JBQU1DLEdBQU4sQ0FBVSxDQUNMSixXQURLLENBQVYsRUFFR0ssSUFGSCxDQUVRRixNQUFNRyxNQUFOLENBQ0osVUFDS2pDLElBREwsRUFFSztBQUNMLHNCQUFLRSxPQUFMLEdBQWVGLEtBQUtBLElBQUwsQ0FBVUUsT0FBekI7QUFDQSxzQkFBS0MsUUFBTCxHQUFnQkgsS0FBS0EsSUFBTCxDQUFVRyxRQUExQjtBQUNILGFBTk8sQ0FGUixFQVNDK0IsS0FURCxDQVNPcEQsRUFBRXFELElBVFQ7QUFVSCxTQW5DRztBQW9DSkMsd0JBcENJLDhCQW9DYztBQUFBOztBQUNiLHFCQUFTVCxTQUFULEdBQXFCO0FBQ2xCLHVCQUFPQyxXQUFXQyxHQUFYLENBQWUsVUFBZixDQUFQO0FBQ0g7O0FBRURDLGtCQUFNQyxHQUFOLENBQVUsQ0FDTEosV0FESyxDQUFWLEVBRUdLLElBRkgsQ0FFUUYsTUFBTUcsTUFBTixDQUNKLFVBQ0tqQyxJQURMLEVBRUs7QUFDTCx1QkFBS0UsT0FBTCxHQUFlRixLQUFLQSxJQUFMLENBQVVFLE9BQXpCO0FBQ0EsdUJBQUtDLFFBQUwsR0FBZ0JILEtBQUtBLElBQUwsQ0FBVUcsUUFBMUI7QUFDSCxhQU5PLENBRlIsRUFTQytCLEtBVEQsQ0FTT3BELEVBQUVxRCxJQVRUO0FBVUg7QUFuREcsS0FuQmM7QUF3RXRCRSxXQXhFc0IscUJBd0ViO0FBQUE7O0FBQ0x2RCxVQUFFLDBCQUFGLEVBQThCd0QsSUFBOUIsQ0FBbUMsSUFBbkMsRUFBeUNoRCxLQUF6QyxDQUErQyxZQUFVO0FBQ3JEUixjQUFFLFlBQUYsRUFBZ0JjLEtBQWhCLENBQXNCLFFBQXRCO0FBQ0gsU0FGRDtBQUdBa0MsY0FBTUQsR0FBTixDQUFVLDZCQUFWLEVBQ0tHLElBREwsQ0FDVSxrQkFBVTtBQUNoQixtQkFBS3ZCLElBQUwsR0FBWThCLE9BQU92QyxJQUFuQjtBQUNILFNBSEQ7QUFJRCxhQUFLMEIsY0FBTDs7QUFFQSxhQUFLbkIsWUFBTCxHQUFvQk8sUUFBUTBCLElBQVIsQ0FBYTVCLE1BQWpDO0FBQ0EsYUFBS0osZ0JBQUwsR0FBd0JNLFFBQVEwQixJQUFSLENBQWFoQyxnQkFBckM7QUFDQWlDLGNBQU1DLE1BQU4sQ0FBYSw0QkFBYixFQUEyQyxVQUFDQyxHQUFELEVBQVM7QUFDM0MsbUJBQUtwQyxZQUFMLEdBQW9Cb0MsR0FBcEI7QUFFUCxTQUhGOztBQU1BLFlBQUlDLGVBQWUsS0FBS3BDLGdCQUFMLEdBQXNCLGVBQXRCLEdBQXNDLGtCQUF6RDtBQUNBLFlBQUlxQyxlQUFlLEtBQUtyQyxnQkFBTCxHQUFzQiwwQkFBdEIsR0FBaUQsb0NBQXBFO0FBQ0ExQixVQUFFLHdCQUFGLEVBQTRCZ0UsSUFBNUIsQ0FBaUMscUJBQWpDLEVBQXVERixZQUF2RDtBQUNBOUQsVUFBRSw2QkFBRixFQUFpQ2dFLElBQWpDLENBQXNDLE9BQXRDLEVBQThDLDBCQUF5QkQsWUFBdkU7QUFHRjtBQWhHcUIsQ0FBUixDQUFsQjs7QUFvR0EsSUFBSUUsZ0JBQWdCLElBQUlqRCxHQUFKLENBQVE7QUFDeEJDLFFBQUksWUFEb0I7QUFFeEJDLFVBQU07QUFDRmdELGlCQUFRLENBRE47QUFFRkMsb0JBQVcsQ0FGVDtBQUdGQyxtQkFBVTtBQUhSLEtBRmtCO0FBT3hCQyxXQVB3QixxQkFPZjtBQUFBOztBQUVMLGlCQUFTQyxVQUFULEdBQXNCO0FBQ2xCLG1CQUFPeEIsV0FBV0MsR0FBWCxDQUFlLFdBQVNmLFFBQVEwQixJQUFSLENBQWFoQixFQUF0QixHQUF5QixVQUF4QyxDQUFQO0FBQ0g7QUFDRCxpQkFBUzZCLFNBQVQsR0FBcUI7QUFDakIsbUJBQU96QixXQUFXQyxHQUFYLENBQWUsWUFBVWYsUUFBUTBCLElBQVIsQ0FBYWhCLEVBQXZCLEdBQTBCLFNBQXpDLENBQVA7QUFDSDtBQUNELGlCQUFTOEIsV0FBVCxHQUF1QjtBQUNuQixtQkFBTzFCLFdBQVdDLEdBQVgsQ0FBZSxvQkFBZixDQUFQO0FBQ0g7O0FBRURDLGNBQU1DLEdBQU4sQ0FBVSxDQUNMcUIsWUFESyxFQUVMQyxXQUZLLEVBR0xDLGFBSEssQ0FBVixFQUlHdEIsSUFKSCxDQUlRRixNQUFNRyxNQUFOLENBQ0osVUFDS2UsT0FETCxFQUVLTyxNQUZMLEVBR0tDLFFBSEwsRUFJSzs7QUFFTCxtQkFBS1IsT0FBTCxHQUFlUyxXQUFXVCxRQUFRaEQsSUFBUixDQUFhQSxJQUFiLENBQWtCMEQsTUFBN0IsQ0FBZjtBQUNBLG1CQUFLVCxVQUFMLEdBQWtCTSxPQUFPdkQsSUFBUCxDQUFZVCxNQUE5QjtBQUNBLG1CQUFLMkQsU0FBTCxHQUFpQk0sU0FBU3hELElBQTFCO0FBQ0gsU0FWTyxDQUpSLEVBZUNrQyxLQWZELENBZU9wRCxFQUFFcUQsSUFmVDtBQWdCSDtBQW5DdUIsQ0FBUixDQUFwQjs7QUF1Q0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBSUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJIiwiZmlsZSI6ImpzL3BhZ2VzL3Byb2ZpbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ0MSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xyXG5cclxuICAgIFxyXG4gICAgcGFnZV9vcGVuID0gd2luZG93LmxvY2F0aW9uLnNlYXJjaC5zdWJzdHIoNik7XHJcbiAgICBcclxuICAgICQoJy5idG5VcGxvYWQnKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJy5hdmF0YXItdXBsb2FkZXIgaW5wdXQnKS5jbGljaygpO1xyXG4gICAgfSlcclxuXHJcbiAgICAvL2luZGl2aWR1YWxcclxuICAgICQoJy5idG5OYmlVcGxvYWQnKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJy5uYmktdXBsb2FkZXIgaW5wdXQnKS5jbGljaygpO1xyXG4gICAgfSlcclxuICAgICQoJy5idG5CaXJ0aENlcnRVcGxvYWQnKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJy5iaXJ0aENlcnQtdXBsb2FkZXIgaW5wdXQnKS5jbGljaygpO1xyXG4gICAgfSlcclxuICAgICQoJy5idG5Hb3ZJZFVwbG9hZCcpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgJCgnLmdvdklELXVwbG9hZGVyIGlucHV0JykuY2xpY2soKTtcclxuICAgIH0pXHJcblxyXG4gICAgLy9lbnRlcnByaXNlXHJcbiAgICAvLyBDT01NRU5URUQgVEhJUyAxMC8xMi8xN1xyXG4gICAgLy8gJCgnLmJ0blNlY1VwbG9hZCcpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICAvLyAgICAgJCgnLnNlYy11cGxvYWRlciBpbnB1dCcpLmNsaWNrKCk7XHJcbiAgICAvLyB9KVxyXG4gICAgLy8gJCgnLmJ0bkJpclVwbG9hZCcpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICAvLyAgICAgJCgnLmJpci11cGxvYWRlciBpbnB1dCcpLmNsaWNrKCk7XHJcbiAgICAvLyB9KVxyXG4gICAgLy8gJCgnLmJ0blBlcm1pdFVwbG9hZCcpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICAvLyAgICAgJCgnLnBlcm1pdC11cGxvYWRlciBpbnB1dCcpLmNsaWNrKCk7XHJcbiAgICAvLyB9KVxyXG5cclxuICAgIC8vICQoJy5idG5OYmlVcGxvYWQnKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgLy8gICAgICQoJy5uYmktdXBsb2FkZXIgaW5wdXQnKS5jbGljaygpO1xyXG4gICAgLy8gfSlcclxuXHJcblxyXG5cclxuICAgIGlmKHBhZ2Vfb3BlbilcclxuICAgIHtcclxuICAgICAgICBpZigkKCcjcHJvZmlsZS1zZXR0aW5ncy1jb250YWluZXIgIycrcGFnZV9vcGVuKS5sZW5ndGgpXHJcbiAgICAgICAge1xyXG5cclxuICAgICAgICAgICAgJCgnIycrcGFnZV9vcGVuKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgJCgnLnRhYi1wYW5lOmZpcnN0JykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgfWVsc2VcclxuICAgIHtcclxuICAgICAgICAkKCcudGFiLXBhbmU6Zmlyc3QnKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICB9XHJcblxyXG4gICAgJCgnI21vZGFsTWVudScpLm9uKCdzaG93bi5icy5tb2RhbCcsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgJCgnYnV0dG9uW2RhdGEtdGFyZ2V0PVwiI21vZGFsTWVudVwiXSBzcGFuOmVxKDApJykucmVtb3ZlQ2xhc3MoKS5hZGRDbGFzcygnZmEgZmEtdGltZXMnKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoJyNtb2RhbE1lbnUnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAkKCdidXR0b25bZGF0YS10YXJnZXQ9XCIjbW9kYWxNZW51XCJdIHNwYW46ZXEoMCknKS5yZW1vdmVDbGFzcygpLmFkZENsYXNzKCdmYSBmYSBmYS1iYXJzJyk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCcjbW9kYWxNZW51IHRhYmxlIHRyJykub24oJ2NsaWNrJyxmdW5jdGlvbigpIHtcclxuICAgICAgICAkKCcjbW9kYWxNZW51JykubW9kYWwoJ2hpZGUnKTtcclxuICAgIH0pO1xyXG5cclxufSlcclxuXHJcblxyXG52YXIgcHJvZmlsZU1lbnUgPSBuZXcgVnVlKHtcclxuICAgIGVsOiAnI3Byb2ZpbGUtbWVudScsXHJcbiAgICBkYXRhOntcclxuICAgICAgICBtb2RhbE1lbnVTaG93aW5nOmZhbHNlLFxyXG4gICAgICAgIGZyaWVuZHM6W10sXHJcbiAgICAgICAgcmVxdWVzdHM6W10sXHJcbiAgICAgICAgcmVjb21tZW5kZWQ6W10sXHJcbiAgICAgICAgc2VsZWN0ZWQ6W10sXHJcbiAgICAgICAgbm90aWZpY2F0aW9uczpbXSxcclxuICAgICAgICBwcm9maWxlSW1hZ2U6JycsXHJcbiAgICAgICAgaXNfZG9jc192ZXJpZmllZDonJyxcclxuICAgICAgICBsYW5nOltdLFxyXG4gICAgICAgIGZvcm06IG5ldyBGb3JtKHtcclxuICAgICAgICAgICBhdmF0YXI6JydcclxuICAgICAgICB9LCB7IGJhc2VVUkw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCAgfSksXHJcbiAgICAgICBcclxuICAgICAgIFxyXG5cclxuICAgIH0sXHJcbiAgICBtZXRob2RzOntcclxuICAgICAgICBzaG93OmZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgICAgICAgdmFyIGVsU2hvdyA9IGV2ZW50LnRhcmdldC5uYW1lO1xyXG4gICAgICAgICAgICBpZihlbFNob3cgPT0gJ3Nob3dfcHJvZHVjdCcpe1xyXG4gICAgICAgICAgICAgICAgJCgnI3Nob3dfcHJvZHVjdDInKS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAkKCcjc2hvd19zdG9yZXMyJykuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgJCgnI3Nob3dfZnJpZW5kczInKS5oaWRlKCk7XHJcbiAgICAgICAgICAgIH1lbHNlIGlmKGVsU2hvdyA9PSAnc2hvd19zdG9yZXMnKXtcclxuICAgICAgICAgICAgICAgICQoJyNzaG93X3Byb2R1Y3QyJykuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgJCgnI3Nob3dfc3RvcmVzMicpLnNob3coKTtcclxuICAgICAgICAgICAgICAgICQoJyNzaG93X2ZyaWVuZHMyJykuaGlkZSgpO1xyXG4gICAgICAgICAgICB9ZWxzZSBpZihlbFNob3cgPT0gJ3Nob3dfZnJpZW5kcycpe1xyXG4gICAgICAgICAgICAgICAgJCgnI3Nob3dfcHJvZHVjdDInKS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAkKCcjc2hvd19zdG9yZXMyJykuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgJCgnI3Nob3dfZnJpZW5kczInKS5zaG93KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNlbGVjdEl0ZW0oaWQpe1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkLnB1c2goaWQpXHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXRGcmllbmRzRGF0YSgpe1xyXG4gICAgICAgICAgICBmdW5jdGlvbiBnZXRGcmllbmQoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9mcmllbmRzJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGF4aW9zLmFsbChbXHJcbiAgICAgICAgICAgICAgICAgZ2V0RnJpZW5kKClcclxuICAgICAgICAgICAgXSkudGhlbihheGlvcy5zcHJlYWQoXHJcbiAgICAgICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAgICAgIGRhdGFcclxuICAgICAgICAgICAgICAgICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mcmllbmRzID0gZGF0YS5kYXRhLmZyaWVuZHM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlcXVlc3RzID0gZGF0YS5kYXRhLnJlcXVlc3RzO1xyXG4gICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXROb3RpZmljYXRpb25zKCl7XHJcbiAgICAgICAgICAgICBmdW5jdGlvbiBnZXRGcmllbmQoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9mcmllbmRzJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGF4aW9zLmFsbChbXHJcbiAgICAgICAgICAgICAgICAgZ2V0RnJpZW5kKClcclxuICAgICAgICAgICAgXSkudGhlbihheGlvcy5zcHJlYWQoXHJcbiAgICAgICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAgICAgIGRhdGFcclxuICAgICAgICAgICAgICAgICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mcmllbmRzID0gZGF0YS5kYXRhLmZyaWVuZHM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlcXVlc3RzID0gZGF0YS5kYXRhLnJlcXVlc3RzO1xyXG4gICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNyZWF0ZWQoKXtcclxuICAgICAgICAkKCcubW9kYWwtYm9keSAubWVudS1ob2xkZXInKS5maW5kKCd0cicpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICQoJyNtb2RhbE1lbnUnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgfSlcclxuICAgICAgICBheGlvcy5nZXQoJy90cmFuc2xhdGUvcHJvZmlsZS1zZXR0aW5ncycpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubGFuZyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgdGhpcy5nZXRGcmllbmRzRGF0YSgpO1xyXG5cclxuICAgICAgIHRoaXMucHJvZmlsZUltYWdlID0gTGFyYXZlbC51c2VyLmF2YXRhcjtcclxuICAgICAgIHRoaXMuaXNfZG9jc192ZXJpZmllZCA9IExhcmF2ZWwudXNlci5pc19kb2NzX3ZlcmlmaWVkO1xyXG4gICAgICAgRXZlbnQubGlzdGVuKCdJbWdmaWxldXBsb2FkLmF2YXRhclVwbG9hZCcsIChpbWcpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZUltYWdlID0gaW1nO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgbGV0IHRpdGxlX3N0YXR1cyA9IHRoaXMuaXNfZG9jc192ZXJpZmllZD9cIlZlcmlmaWVkIFVzZXJcIjpcIk5vdCB5ZXQgVmVyaWZpZWRcIjtcclxuICAgICAgIGxldCBjbGFzc19zdGF0dXMgPSB0aGlzLmlzX2RvY3NfdmVyaWZpZWQ/XCJ0Yy1ncmVlbiBmYS1jaGVjay1jaXJjbGVcIjpcInRleHQtd2FybmluZyBmYS1leGNsYW1hdGlvbi1jaXJjbGVcIjtcclxuICAgICAgICQoJyNhdmF0YXItY29udGFpbmVyIHNwYW4nKS5hdHRyKCdkYXRhLW9yaWdpbmFsLXRpdGxlJyx0aXRsZV9zdGF0dXMpO1xyXG4gICAgICAgJCgnI2F2YXRhci1jb250YWluZXIgc3BhbiBzcGFuJykuYXR0cignY2xhc3MnLFwibW91c2UtaGFuZCAgZmEgZmEtMnggXCIrIGNsYXNzX3N0YXR1cyk7XHJcblxyXG4gICAgICAgXHJcbiAgICB9LFxyXG5cclxufSk7XHJcblxyXG52YXIgZGF0YUNvbnRhaW5lciA9IG5ldyBWdWUoe1xyXG4gICAgZWw6ICcjZGF0YS1hcmVhJyxcclxuICAgIGRhdGE6IHtcclxuICAgICAgICBld2FsbGV0OjAsXHJcbiAgICAgICAgc3RvcmVDb3VudDowLFxyXG4gICAgICAgIHdpc2hDb3VudDowXHJcbiAgICB9LFxyXG4gICAgbW91bnRlZCgpe1xyXG5cclxuICAgICAgICBmdW5jdGlvbiBnZXRFd2FsbGV0KCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy91c2VyLycrTGFyYXZlbC51c2VyLmlkKycvZXdhbGxldCcpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmdW5jdGlvbiBnZXRTdG9yZXMoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL3VzZXJzLycrTGFyYXZlbC51c2VyLmlkKycvc3RvcmVzJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZ1bmN0aW9uIGdldFdpc2hsaXN0KCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9nZXRmYXZvcml0ZXN0b3RhbCcpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYXhpb3MuYWxsKFtcclxuICAgICAgICAgICAgIGdldEV3YWxsZXQoKSxcclxuICAgICAgICAgICAgIGdldFN0b3JlcygpLFxyXG4gICAgICAgICAgICAgZ2V0V2lzaGxpc3QoKVxyXG4gICAgICAgIF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG4gICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAgZXdhbGxldCxcclxuICAgICAgICAgICAgICAgICBzdG9yZXMsXHJcbiAgICAgICAgICAgICAgICAgd2lzaGxpc3RcclxuICAgICAgICAgICAgKSA9PiB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmV3YWxsZXQgPSBwYXJzZUZsb2F0KGV3YWxsZXQuZGF0YS5kYXRhLmFtb3VudCk7XHJcbiAgICAgICAgICAgIHRoaXMuc3RvcmVDb3VudCA9IHN0b3Jlcy5kYXRhLmxlbmd0aDtcclxuICAgICAgICAgICAgdGhpcy53aXNoQ291bnQgPSB3aXNobGlzdC5kYXRhO1xyXG4gICAgICAgIH0pKVxyXG4gICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG4gICAgfVxyXG59KVxyXG5cclxuXHJcbi8vIFZ1ZS5jb21wb25lbnQoJ3Byb2R1Y3QtaXRlbScsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL3N0b3JlL21hbmFnZW1lbnQvUHJvZHVjdC52dWUnKSk7XHJcblxyXG5cclxuLy8gVnVlLmZpbHRlcigncm91bmQnLCBmdW5jdGlvbiAodmFsdWUpIHtcclxuLy8gICAgIHJldHVybiAgcm91bmRPZmYodmFsdWUpO1xyXG4vLyB9KTtcclxuXHJcbi8vIHZhciBhcHAgPSBuZXcgVnVlKHtcclxuLy8gICBlbDogJyNwcm9maWxlLWNvbnRhaW5lcicsXHJcbi8vICAgZGF0YToge1xyXG4vLyAgICAgZnJpZW5kczpbXHJcbi8vICAgICAgIHtcclxuLy8gICAgICAgICBpZDoxLFxyXG4vLyAgICAgICAgIG5hbWU6J0FubmEnLFxyXG4vLyAgICAgICAgIGltYWdlOidodHRwczovL2ltYWdlcy5nYW11cnMuY29tL3VzZXIvYXZhdGFyLzM0NmViODM0LWI2ZDMtNDU3NC1iMTFmLTZhNzZjNDRkMDUwZC5wbmcnXHJcbi8vICAgICAgIH0sXHJcbi8vICAgICAgIHtcclxuLy8gICAgICAgICBpZDoyLFxyXG4vLyAgICAgICAgIG5hbWU6J0FubmllJyxcclxuLy8gICAgICAgICBpbWFnZTonaHR0cHM6Ly9pbWFnZXMuZ2FtdXJzLmNvbS91c2VyL2F2YXRhci8zNDZlYjgzNC1iNmQzLTQ1NzQtYjExZi02YTc2YzQ0ZDA1MGQucG5nJ1xyXG4vLyAgICAgICB9LFxyXG4vLyAgICAgICB7XHJcbi8vICAgICAgICAgaWQ6MyxcclxuLy8gICAgICAgICBuYW1lOidKb2huJyxcclxuLy8gICAgICAgICBpbWFnZTonaHR0cHM6Ly9pbWFnZXMuZ2FtdXJzLmNvbS91c2VyL2F2YXRhci8zNDZlYjgzNC1iNmQzLTQ1NzQtYjExZi02YTc2YzQ0ZDA1MGQucG5nJ1xyXG4vLyAgICAgICB9LFxyXG4vLyAgICAgICB7XHJcbi8vICAgICAgICAgaWQ6NCxcclxuLy8gICAgICAgICBuYW1lOidKb2hubmllJyxcclxuLy8gICAgICAgICBpbWFnZTonaHR0cHM6Ly9pbWFnZXMuZ2FtdXJzLmNvbS91c2VyL2F2YXRhci8zNDZlYjgzNC1iNmQzLTQ1NzQtYjExZi02YTc2YzQ0ZDA1MGQucG5nJ1xyXG4vLyAgICAgICB9XHJcbi8vICAgICBdLFxyXG4vLyAgICAgcmVxdWVzdHM6W1xyXG4vLyAgICAgICB7XHJcbi8vICAgICAgICAgaWQ6NSxcclxuLy8gICAgICAgICBuYW1lOidKdWFuaXRvJyxcclxuLy8gICAgICAgICBpbWFnZTonaHR0cHM6Ly9pbWFnZXMuZ2FtdXJzLmNvbS91c2VyL2F2YXRhci8zNDZlYjgzNC1iNmQzLTQ1NzQtYjExZi02YTc2YzQ0ZDA1MGQucG5nJ1xyXG4vLyAgICAgICB9LFxyXG4vLyAgICAgICB7XHJcbi8vICAgICAgICAgaWQ6NixcclxuLy8gICAgICAgICBuYW1lOidKdWFuaXRhJyxcclxuLy8gICAgICAgICBpbWFnZTonaHR0cHM6Ly9pbWFnZXMuZ2FtdXJzLmNvbS91c2VyL2F2YXRhci8zNDZlYjgzNC1iNmQzLTQ1NzQtYjExZi02YTc2YzQ0ZDA1MGQucG5nJ1xyXG4vLyAgICAgICB9XHJcbi8vICAgICBdLFxyXG4vLyAgICAgcmVjb21tZW5kZWQ6W1xyXG4vLyAgICAgICB7XHJcbi8vICAgICAgICAgaWQ6NyxcclxuLy8gICAgICAgICBuYW1lOidDaGFybGVzJyxcclxuLy8gICAgICAgICBpbWFnZTonaHR0cHM6Ly9pbWFnZXMuZ2FtdXJzLmNvbS91c2VyL2F2YXRhci8zNDZlYjgzNC1iNmQzLTQ1NzQtYjExZi02YTc2YzQ0ZDA1MGQucG5nJ1xyXG4vLyAgICAgICB9LFxyXG4vLyAgICAgICB7XHJcbi8vICAgICAgICAgaWQ6OCxcclxuLy8gICAgICAgICBuYW1lOidBcmlhbm5lJyxcclxuLy8gICAgICAgICBpbWFnZTonaHR0cHM6Ly9pbWFnZXMuZ2FtdXJzLmNvbS91c2VyL2F2YXRhci8zNDZlYjgzNC1iNmQzLTQ1NzQtYjExZi02YTc2YzQ0ZDA1MGQucG5nJ1xyXG4vLyAgICAgICB9LFxyXG4vLyAgICAgICB7XHJcbi8vICAgICAgICAgaWQ6OSxcclxuLy8gICAgICAgICBuYW1lOidBcmllcycsXHJcbi8vICAgICAgICAgaW1hZ2U6J2h0dHBzOi8vaW1hZ2VzLmdhbXVycy5jb20vdXNlci9hdmF0YXIvMzQ2ZWI4MzQtYjZkMy00NTc0LWIxMWYtNmE3NmM0NGQwNTBkLnBuZydcclxuLy8gICAgICAgfSxcclxuLy8gICAgICAge1xyXG4vLyAgICAgICAgIGlkOjEwLFxyXG4vLyAgICAgICAgIG5hbWU6J0NhcmwnLFxyXG4vLyAgICAgICAgIGltYWdlOidodHRwczovL2ltYWdlcy5nYW11cnMuY29tL3VzZXIvYXZhdGFyLzM0NmViODM0LWI2ZDMtNDU3NC1iMTFmLTZhNzZjNDRkMDUwZC5wbmcnXHJcbi8vICAgICAgIH0sXHJcbi8vICAgICAgIHtcclxuLy8gICAgICAgICBpZDoxMSxcclxuLy8gICAgICAgICBuYW1lOidLZXZpbicsXHJcbi8vICAgICAgICAgaW1hZ2U6J2h0dHBzOi8vaW1hZ2VzLmdhbXVycy5jb20vdXNlci9hdmF0YXIvMzQ2ZWI4MzQtYjZkMy00NTc0LWIxMWYtNmE3NmM0NGQwNTBkLnBuZydcclxuLy8gICAgICAgfSxcclxuLy8gICAgICAge1xyXG4vLyAgICAgICAgIGlkOjEyLFxyXG4vLyAgICAgICAgIG5hbWU6J0NoYXJsaWUnLFxyXG4vLyAgICAgICAgIGltYWdlOidodHRwczovL2ltYWdlcy5nYW11cnMuY29tL3VzZXIvYXZhdGFyLzM0NmViODM0LWI2ZDMtNDU3NC1iMTFmLTZhNzZjNDRkMDUwZC5wbmcnXHJcbi8vICAgICAgIH1cclxuLy8gICAgIF0sXHJcbi8vICAgICBzaG93UmVxdWVzdDp0cnVlLFxyXG4vLyAgICAgc2hvd0ZyaWVuZHM6ZmFsc2UsXHJcbi8vICAgICBzaG93UmVjb21tZW5kZWQ6ZmFsc2UsXHJcbi8vICAgICBpdGVtczpbXHJcbi8vICAgICAgIHtcclxuLy8gICAgICAgICBpZDogMVxyXG4vLyAgICAgICAgICxuYW1lOiAnUGhvc3Bob3J1c0dyZXkgTWVsYW5nZSBQcmludGVkIFYgTmVjayBULVNoaXJ0J1xyXG4vLyAgICAgICAgICxpbWFnZTogJy9pbWcvZGVtby92bmVjazEuanBnJ1xyXG4vLyAgICAgICAgICxwcmljZTozMDAwXHJcbi8vICAgICAgICAgLHNhbGVfcHJpY2U6MjAwMFxyXG4vLyAgICAgICB9LFxyXG4vLyAgICAgICB7XHJcbi8vICAgICAgICAgaWQ6IDJcclxuLy8gICAgICAgICAsbmFtZTogJ1VuaXRlZCBDb2xvcnMgb2YgQmVuZXR0b25OYXZ5IEJsdWUgU29saWQgViBOZWNrIFQgU2hpcnQnXHJcbi8vICAgICAgICAgLGltYWdlOiAnL2ltZy9kZW1vL3ZuZWNrMi5qcGcnXHJcbi8vICAgICAgICAgLHByaWNlOjIwMDBcclxuLy8gICAgICAgICAsc2FsZV9wcmljZToxNTAwXHJcbi8vICAgICAgIH0sXHJcbi8vICAgICAgIHtcclxuLy8gICAgICAgICBpZDogM1xyXG4vLyAgICAgICAgICxuYW1lOiAnV3JhbmdsZXJCbGFjayBWIE5lY2sgVCBTaGlydCdcclxuLy8gICAgICAgICAsaW1hZ2U6ICcvaW1nL2RlbW8vdm5lY2szLmpwZydcclxuLy8gICAgICAgICAscHJpY2U6MTg1MFxyXG4vLyAgICAgICAgICxzYWxlX3ByaWNlOjE1MDBcclxuLy8gICAgICAgfSxcclxuLy8gICAgICAge1xyXG4vLyAgICAgICAgIGlkOiA0XHJcbi8vICAgICAgICAgLG5hbWU6ICdUYWdkIE5ldyBZb3JrR3JleSBQcmludGVkIFYgTmVjayBULVNoaXJ0cydcclxuLy8gICAgICAgICAsaW1hZ2U6ICcvaW1nL2RlbW8vdm5lY2s0LmpwZydcclxuLy8gICAgICAgICAscHJpY2U6MjA1MFxyXG4vLyAgICAgICAgICxzYWxlX3ByaWNlOjE2OTlcclxuLy8gICAgICAgfSxcclxuLy8gICAgICAge1xyXG4vLyAgICAgICAgICBpZDogNVxyXG4vLyAgICAgICAgICxuYW1lOiAnUGVuc2hvcHBlIFBvbG8gU2hpcnQnXHJcbi8vICAgICAgICAgLGltYWdlOiAnL2ltZy9kZW1vL3BvbG8yLmpwZydcclxuLy8gICAgICAgICAscHJpY2U6MTk5OVxyXG4vLyAgICAgICAgICxzYWxlX3ByaWNlOjk5OVxyXG4vLyAgICAgICB9LFxyXG4vLyAgICAgXSxcclxuLy8gICAgIHNlbGVjdGVkOltdXHJcbi8vICAgfSxcclxuLy8gICBtZXRob2RzOntcclxuLy8gICAgIHNob3c6ZnVuY3Rpb24oZXZlbnQpe1xyXG4vLyAgICAgICB2YXIgZWxTaG93ID0gZXZlbnQudGFyZ2V0Lm5hbWU7XHJcbi8vICAgICAgIGlmKGVsU2hvdyA9PSAnc2hvd19wcm9kdWN0Jyl7XHJcbi8vICAgICAgICAgJCgnI3Nob3dfcHJvZHVjdDInKS5zaG93KCk7XHJcbi8vICAgICAgICAgJCgnI3Nob3dfc3RvcmVzMicpLmhpZGUoKTtcclxuLy8gICAgICAgICAkKCcjc2hvd19mcmllbmRzMicpLmhpZGUoKTtcclxuLy8gICAgICAgfWVsc2UgaWYoZWxTaG93ID09ICdzaG93X3N0b3Jlcycpe1xyXG4vLyAgICAgICAgICQoJyNzaG93X3Byb2R1Y3QyJykuaGlkZSgpO1xyXG4vLyAgICAgICAgICQoJyNzaG93X3N0b3JlczInKS5zaG93KCk7XHJcbi8vICAgICAgICAgJCgnI3Nob3dfZnJpZW5kczInKS5oaWRlKCk7XHJcbi8vICAgICAgIH1lbHNlIGlmKGVsU2hvdyA9PSAnc2hvd19mcmllbmRzJyl7XHJcbi8vICAgICAgICAgJCgnI3Nob3dfcHJvZHVjdDInKS5oaWRlKCk7XHJcbi8vICAgICAgICAgJCgnI3Nob3dfc3RvcmVzMicpLmhpZGUoKTtcclxuLy8gICAgICAgICAkKCcjc2hvd19mcmllbmRzMicpLnNob3coKTtcclxuLy8gICAgICAgfVxyXG4vLyAgICAgfSxcclxuLy8gICAgIHNlbGVjdEl0ZW0oaWQpe1xyXG4vLyAgICAgICB0aGlzLnNlbGVjdGVkLnB1c2goaWQpXHJcbi8vICAgICB9LFxyXG5cclxuLy8gICAgIGNsa1Nob3dSZXF1ZXN0OiBmdW5jdGlvbigpe1xyXG4vLyAgICAgICB0aGlzLnNob3dSZXF1ZXN0ID0gdHJ1ZTtcclxuLy8gICAgICAgdGhpcy5zaG93RnJpZW5kcyA9IGZhbHNlO1xyXG4vLyAgICAgICB0aGlzLnNob3dSZWNvbW1lbmRlZCA9IGZhbHNlO1xyXG4vLyAgICAgfSxcclxuLy8gICAgIGNsa1Nob3dGcmllbmRzOiBmdW5jdGlvbigpe1xyXG4vLyAgICAgICB0aGlzLnNob3dSZXF1ZXN0ID0gZmFsc2U7XHJcbi8vICAgICAgIHRoaXMuc2hvd0ZyaWVuZHMgPSB0cnVlO1xyXG4vLyAgICAgICB0aGlzLnNob3dSZWNvbW1lbmRlZCA9IGZhbHNlO1xyXG4vLyAgICAgfSxcclxuLy8gICAgIGNsa1Nob3dSZWNvbW1lbmRlZDogZnVuY3Rpb24oKXtcclxuLy8gICAgICAgdGhpcy5zaG93UmVxdWVzdCA9IGZhbHNlO1xyXG4vLyAgICAgICB0aGlzLnNob3dGcmllbmRzID0gZmFsc2U7XHJcbi8vICAgICAgIHRoaXMuc2hvd1JlY29tbWVuZGVkID0gdHJ1ZTtcclxuLy8gICAgIH0sXHJcbi8vICAgfVxyXG4vLyB9KVxyXG5cclxuLy8gdmFyIGZyaWVuZHMgPSBuZXcgVnVlKHtcclxuLy8gICBlbDogJyNmcmllbmRzX2NvbnRhaW5lcicsXHJcbi8vICAgZGF0YToge30sXHJcbi8vICAgbWV0aG9kczp7fVxyXG4vLyB9KVxyXG5cclxuXHJcblxyXG4vLyBmdW5jdGlvbiBudW1iZXJXaXRoQ29tbWFzKHgpIHtcclxuLy8gICAgIHJldHVybiB4LnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIsXCIpO1xyXG4vLyB9XHJcblxyXG4vLyBmdW5jdGlvbiByb3VuZE9mZih2KSB7XHJcbi8vICAgICByZXR1cm4gTWF0aC5yb3VuZCh2KTtcclxuLy8gfVxyXG5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9wcm9maWxlLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==