/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 423);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(8),
  /* template */
  __webpack_require__(11),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\Modal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Modal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5c12ce5e", Component.options)
  } else {
    hotAPI.reload("data-v-5c12ce5e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    staticClass: "modal-dialog",
    class: _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_vm._t("extra-left"), _vm._v(" "), _c('div', {
    class: 'col-md-' + _vm.contentSize + ' modal-sm-' + _vm.contentSize
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_vm._m(0), _vm._v(" "), _c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer")], 2)]), _vm._v(" "), _vm._t("modal-right")], 2)])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "data-dismiss": "modal",
      "aria-label": "Close"
    }
  }, [_c('span', {
    attrs: {
      "aria-hidden": "true"
    }
  }, [_vm._v("×")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5c12ce5e", module.exports)
  }
}

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-653e7369", module.exports)
  }
}

/***/ }),

/***/ 173:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('cart-item', __webpack_require__(324));
Vue.component('cart-table', __webpack_require__(325));
Vue.component('order-table', __webpack_require__(326));
Vue.component('dialog-modal', __webpack_require__(9));
Vue.component('modal', __webpack_require__(10));

Vue.filter('currency', function (value) {
	return '₱' + numberWithCommas(parseFloat(value).toFixed(2));
});

var app = new Vue({
	el: '#cart_container',
	data: {
		items: [],
		selected: [],
		dup_items: [],
		prod_id: [],
		qty: [],
		showUndo: false,
		item_names: '',
		optionType: [],
		selectedType: 5,
		ewallet: 0,
		order_id: 0,
		name: '',
		address: '',
		shipping: 0,
		total: 0,
		orders: [],
		lang: [],

		//checkout
		current_step_name: 'Shipping Address',
		current_step: 1,
		reached_step: 1,
		address_set: false,
		form: new Form({
			type_id: '',
			type: null,
			name: null,
			number: null,
			address: null,
			barangay: null,
			city: null,
			province: null,
			zip_code: null,
			landmark: null,
			notes: null,
			alternate_number: null,
			subtotal: null,
			total: null,
			tax: null,
			shipping: null,
			charge: null
		}, { baseURL: '/cart' })
	},

	created: function created() {
		var _this = this;

		function getTranslation() {
			return axios.get('/translate/cart');
		}

		function getCart() {
			return axios.get(location.origin + '/cart/view');
		}

		function getType() {
			if (Laravel.user) return axios.get(location.origin + '/cart/getType');
		}
		function getActive() {
			if (Laravel.user) return axios.get(location.origin + '/cart/getActive');
		}
		function getUsableBalance() {
			if (Laravel.user) return axiosAPIv1.get('/getUsable');
		}

		axios.all([getTranslation(), getCart(), getType(), getActive(), getUsableBalance()]).then(axios.spread(function (translation, cart, type, active, balance) {
			_this.lang = translation.data.data;
			var cart2 = $.map(cart.data.cart, function (value, index) {
				return [value];
			});
			setTimeout(function () {
				$.material.init();
			}, 100);
			_this.items = cart2;

			var newtype = { "id": 0, "type": _this.lang['new-profile'], "is_active": false };
			var type = type.data;
			type.push(newtype);
			_this.optionType = type;

			if (active.data.length !== 0) {

				_this.selectedType = active.data[0].id;

				axios.get(location.origin + '/cart/getAddressDetails/' + _this.selectedType).then(function (result) {
					var address = result.data[0].address;
					_this.form.type_id = _this.selectedType;
					_this.form.type = result.data[0].type;
					_this.form.name = result.data[0].name_of_receiver;
					_this.form.number = result.data[0].contact_number;
					_this.form.address = address.replace(/(\r\n|\n|\r)/gm, "");
					_this.form.barangay = result.data[0].barangay;
					_this.form.city = result.data[0].city;
					_this.form.province = result.data[0].province;
					_this.form.zip_code = result.data[0].zip_code;
					_this.form.landmark = result.data[0].landmark;
					_this.form.notes = '';
					_this.form.alternate_number = result.data[0].alternate_contact_number;
				});
			} else {
				_this.selectedType = 0;
			}

			_this.ewallet = balance.data.success ? parseFloat(balance.data.data.usable) : 0;
		})).catch($.noop);
	},


	methods: {
		showRemoveDialog: function showRemoveDialog() {
			if (this.selected.length) {
				$('#dialogRemove').modal('toggle');
			}
		},
		showCheckoutModal: function showCheckoutModal() {
			if (Laravel.user) {
				if (this.items != 0) $('#modalCheckout').modal('toggle');
			} else {
				toastr.error(this.lang['you-need']);
			}
		},

		//checkout
		seeStepAddress: function seeStepAddress() {
			if (this.items != 0) {
				this.current_step = 1;
				this.current_step_name = 'Shipping Address';
			}
		},
		moveToWishlist: function moveToWishlist(id, rowId) {
			var _this2 = this;

			if (Laravel.isAuthenticated === true) {
				var index = this.items.findIndex(function (el) {
					return el.id === id;
				});
				axios.get(location.origin + '/cart/delete', {
					params: {
						row_id: rowId
					}
				}).then(function (result) {
					_this2.items.splice(index, 1);
					axiosAPIv1.get('/addtofavorites/' + id).then(function (result) {
						var fav = parseInt($("#fav-count").text()) + 1;
						$("#fav-count").text(fav).show();
						toastr.success(_this2.lang['added-favorites']);
					});
				});
			} else {
				toastr.error(this.lang['you-need']);
			}
		},
		removeItems: function removeItems(id, name, rowId) {
			var _this3 = this;

			var index = this.items.findIndex(function (el) {
				return el.id === id;
			});
			axios.get(location.origin + '/cart/delete', {
				params: {
					row_id: rowId
				}
			}).then(function (result) {
				_this3.items.splice(index, 1);
				toastr.success(_this3.lang['removed'], name);
			});
		},
		saveProfile: function saveProfile() {
			var _this4 = this;

			this.form.submit('post', '/saveProfile').then(function (result) {
				if (result.response != 0) {
					toastr.success(_this4.lang['profile-saved']);
					axios.get(location.origin + '/cart/getType').then(function (result) {
						var newtype = { "id": 0, "type": "New Profile", "is_active": false };
						var type = result.data;
						type.push(newtype);
						_this4.optionType = type;
						if ($("#address_profile").val() == 0) {
							_this4.form.type = '';
							_this4.form.name = '';
							_this4.form.number = '';
							_this4.form.address = '';
							_this4.form.barangay = '';
							_this4.form.city = '';
							_this4.form.province = '';
							_this4.form.zip_code = '';
							_this4.form.landmark = '';
							_this4.form.notes = '';
							_this4.form.alternate_number = '';
						}
					});
				} else {
					toastr.error(result.message);
				}
			}).catch(function (error) {
				toastr.error(_this4.lang['please']);
			});
		},
		deleteProfile: function deleteProfile() {
			var _this5 = this;

			this.form.submit('post', '/deleteProfile').then(function (result) {
				if (result.response != 0) {
					toastr.success(_this5.lang['profile-deleted']);
					axios.get(location.origin + '/cart/getType').then(function (result) {
						var newtype = { "id": 0, "type": "New Profile", "is_active": true };
						var type = result.data;
						type.push(newtype);
						_this5.optionType = type;
						_this5.selectedType = 0;
						_this5.form.type = '';
						_this5.form.name = '';
						_this5.form.number = '';
						_this5.form.address = '';
						_this5.form.barangay = '';
						_this5.form.city = '';
						_this5.form.province = '';
						_this5.form.zip_code = '';
						_this5.form.landmark = '';
						_this5.form.notes = '';
						_this5.form.alternate_number = '';
					});
				} else {
					toastr.error(result.message);
				}
			}).catch(function (error) {
				toastr.error(_this5.lang['currently'], _this5.lang['cant-delete']);
			});
		},
		placeOrder: function placeOrder() {
			var _this6 = this;

			if (this.items != 0) {
				this.form.submit('post', '/checkout').then(function (result) {
					if (result.response != 0) {
						_this6.ewallet = result.ewallet_bal;
						_this6.order_id = result.id;
						_this6.name = result.name;
						_this6.address = result.address;
						_this6.shipping = _this6.form.shipping;
						_this6.total = result.total;
						_this6.orders = _this6.items;
						_this6.current_step = 2;
						_this6.current_step_name = 'Order Summary';
						_this6.items = [];
						_this6.dup_items = [];
						toastr.success(_this6.lang['order-placed'], _this6.lang['order-no'] + _this6.order_id);
						$("#urf").hide();
					} else {
						$("#placeOrder").prop('disabled', false);
						toastr.error(result.message);
					}
				}).catch(function (error) {
					_this6.current_step = 1;
					_this6.current_step_name = 'Shipping Address';
					$("#placeOrder").prop('disabled', false);
					toastr.error(_this6.lang['please']);
				});
			}
		},
		updateQty: function updateQty(qty, rowId) {
			axios.get(location.origin + '/cart/updateQty', {
				params: {
					row_id: rowId,
					qty: qty
				}
			});
		},
		stock: function stock(id, qty, _stock, rowId) {
			if (parseInt(_stock) == 0) {
				axios.get(location.origin + '/cart/updateQty', {
					params: {
						row_id: rowId,
						qty: 1
					}
				});
			}
			if (parseInt(qty) > parseInt(_stock) && parseInt(_stock) != 0) {
				$('#q' + id).val(_stock);
				axios.get(location.origin + '/cart/updateQty', {
					params: {
						row_id: rowId,
						qty: _stock
					}
				});
			}
			if (parseInt(qty) <= parseInt(_stock)) {
				axios.get(location.origin + '/cart/updateQty', {
					params: {
						row_id: rowId,
						qty: qty
					}
				});
			}
			if (qty == '' || qty == '0') {
				$('#q' + id).val(1);
				axios.get(location.origin + '/cart/updateQty', {
					params: {
						row_id: rowId,
						qty: 1
					}
				});
			}
		},
		getAddressDetails: function getAddressDetails(event) {
			var _this7 = this;

			if (event.target.value === '0') {
				this.form.type_id = event.target.value;
				this.form.type = '';
				this.form.name = '';
				this.form.number = '';
				this.form.address = '';
				this.form.barangay = '';
				this.form.city = '';
				this.form.province = '';
				this.form.zip_code = '';
				this.form.landmark = '';
				this.form.notes = '';
				this.form.alternate_number = '';
			} else {
				axios.get(location.origin + '/cart/getAddressDetails/' + event.target.value).then(function (result) {
					var address = result.data[0].address;
					var number = '';
					if (Laravel.user.numbers.length !== 0) number = Laravel.user.numbers[0].number;else number = '';
					_this7.form.type_id = event.target.value;
					_this7.form.type = result.data[0].type;
					_this7.form.name = result.data[0].name_of_receiver;
					_this7.form.number = result.data[0].contact_number;
					_this7.form.address = address.replace(/(\r\n|\n|\r)/gm, "");
					_this7.form.barangay = result.data[0].barangay;
					_this7.form.city = result.data[0].city;
					_this7.form.province = result.data[0].province;
					_this7.form.zip_code = result.data[0].zip_code;
					_this7.form.landmark = result.data[0].landmark;
					_this7.form.notes = '';
					_this7.form.alternate_number = result.data[0].alternate_contact_number;
				});
			}
		},
		print: function print() {
			var html_container = '';
			html_container = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns:fb="http://ogp.me/ns/fb#"><head>';
			html_container += '<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons" />';
			html_container += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>';
			html_container += '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"><meta name="apple-mobile-web-app-capable" content="yes">';
			html_container += '<style>#cart_container .checkout-steps-container #total td{text-align:right;padding-left:35px}#cart_container .checkout-steps-container .e-wallet{text-align:center;background-color:#e6e6e6;width:150px;border:2px solid #009688;height:85px}#cart_container .checkout-steps-container .e-wallet .titles{margin-top:15px}#cart_container .checkout-steps-container .e-wallet .balance{font-weight:700}#cart_container .checkout-steps-container .float{float:right}</style>';
			html_container += '</head><body><div id="cart_container" class="container"><div class="checkout-steps-container checkout-step-summary"><img src="/shopping/img/general/new-logo.png">';

			var body = $("#print").html();
			myWindow = window.open('', '', 'width=500,height=500,scrollbars=yes,location=no');
			myWindow.document.write(html_container);
			myWindow.document.write(body);
			myWindow.document.write("</div></div></body></html>");
			myWindow.document.close();

			setTimeout(function () {
				myWindow.focus();
				myWindow.print();
			}, 3000);
		}
	},

	computed: {
		total_price: function total_price() {
			sum = 0;
			shipping = 0;
			charge = 0;
			shippingAndCharge = 0;
			vat = 0;
			for (i = 0; i < this.items.length; i++) {
				var salePrice = this.items[i].options.sale_price;
				sum += salePrice.replace(',', '') * this.items[i].qty;
				charge += this.items[i].options.charge * this.items[i].qty;
				shippingAndCharge += this.items[i].options.shipping_fee * this.items[i].qty + this.items[i].options.charge * this.items[i].qty;
				vat += this.items[i].options.vat * this.items[i].qty;
				shipping += this.items[i].options.shipping_fee * this.items[i].qty;
			}
			this.form.subtotal = sum;
			this.form.total = sum + shippingAndCharge + vat;
			this.form.tax = vat;
			this.form.shipping = shipping;
			this.form.charge = charge;

			return this.form.total;
		}
	}
});

$('#placeOrder').on('click', function () {
	$(this).prop('disabled', true);
});

$("#number, #zip_code, #alternate_number").keypress(function (e) {
	//if the letter is not digit then display error and don't type anything
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
	}
});

/*$('#number, #alternate_number').inputmask({
mask: '(9999) 999-9999'
})*/

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['item'],

	methods: {
		updateqty: function updateqty() {
			this.$parent.updateQty(this.item.qty, this.item.rowId);
		},
		stock: function stock() {
			if (parseInt(this.item.qty) > parseInt(this.item.options.stock)) {
				this.item.qty = this.item.options.stock;
			}
			if (parseInt(this.item.qty) <= parseInt(this.item.options.stock)) {
				this.item.qty = this.item.qty;
			}
			if (this.item.qty == '' || this.item.qty == '0') {
				this.item.qty = 1;
			}
			this.$parent.stock(this.item.id, this.item.qty, this.item.options.stock, this.item.rowId);
		},
		moveToWishlist: function moveToWishlist() {
			this.$parent.moveToWishlist(this.item.id, this.item.rowId);
		},
		removeItems: function removeItems() {
			this.$parent.removeItems(this.item.id, this.item.name, this.item.rowId);
		}
	},
	computed: {
		sale_price: function sale_price() {
			var salePrice = this.item.options.sale_price;
			return salePrice.replace(',', '');
		},
		itemPrice: function itemPrice() {
			var salePrice = this.item.options.sale_price;
			if (parseInt(this.item.options.stock) == 0) {
				return salePrice.replace(',', '');
			}
			if (parseInt(this.item.qty) > parseInt(this.item.options.stock) && parseInt(this.item.options.stock) != 0) {
				return this.item.options.stock * salePrice.replace(',', '');
			}
			if (parseInt(this.item.qty) <= parseInt(this.item.options.stock)) {
				return this.item.qty * salePrice.replace(',', '');
			}
			if (this.item.qty == '' || this.item.qty == '0') {
				return 1 * salePrice.replace(',', '');
			}
		},
		shipping: function shipping() {
			return this.item.options.shipping_fee * this.item.qty + this.item.options.charge * this.item.qty + this.item.options.vat * this.item.qty;
		},
		subTotal: function subTotal() {
			return this.itemPrice + this.shipping;
		}
	}
});

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['items'],
	computed: {
		sale_price: function sale_price() {
			var salePrice = this.item.options.sale_price;
			return salePrice.replace(',', '');
		}
	}
});

/***/ }),

/***/ 261:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['orders'],
	computed: {
		sale_price: function sale_price() {
			var salePrice = this.item.options.sale_price;
			return salePrice.replace(',', '');
		}
	}
});

/***/ }),

/***/ 324:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(259),
  /* template */
  __webpack_require__(371),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\cart\\CartItem.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] CartItem.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-22884846", Component.options)
  } else {
    hotAPI.reload("data-v-22884846", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 325:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(260),
  /* template */
  __webpack_require__(373),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\cart\\CartList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] CartList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2c8dc051", Component.options)
  } else {
    hotAPI.reload("data-v-2c8dc051", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 326:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(261),
  /* template */
  __webpack_require__(388),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\cart\\OrderList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] OrderList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-600fbfae", Component.options)
  } else {
    hotAPI.reload("data-v-600fbfae", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-lg-12 col-md-6 col-sm-6"
  }, [_c('a', {
    staticClass: "remove",
    attrs: {
      "href": "javascript:void(0)",
      "title": this.$parent.lang['remove']
    },
    on: {
      "click": function($event) {
        _vm.removeItems()
      }
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-remove",
    attrs: {
      "aria-hidden": "true"
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "card vcenter-lg",
    class: {
      'item-selected': this.$parent.selected.indexOf(_vm.item.id) > -1
    },
    attrs: {
      "data-id": _vm.item.id
    }
  }, [_c('div', {
    staticClass: "col-lg-5"
  }, [_c('div', {
    staticClass: "col-lg-5 col-md-12 col-sm-12"
  }, [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.options.slug
    }
  }, [_c('img', {
    staticClass: "img img-responsive",
    attrs: {
      "src": _vm.item.options.image
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-7 col-md-12 col-sm-12"
  }, [_c('div', {
    staticClass: "row text-sm-center"
  }, [_c('span', {
    staticClass: "cart-item-name"
  }, [_vm._v(_vm._s(_vm.item.name))]), _vm._v(" "), _c('a', {
    staticClass: "btn btn-sm btn-link",
    attrs: {
      "href": '/stores/' + _vm.item.options.store
    }
  }, [_c('span', {
    staticClass: "cart-item-store"
  }, [_vm._v(_vm._s(_vm.item.options.store))])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.item.options.color != ''),
      expression: "item.options.color != '' "
    }]
  }, [_vm._v(_vm._s(this.$parent.lang['color']) + ": " + _vm._s(_vm.item.options.color))]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.item.options.size != ''),
      expression: "item.options.size != '' "
    }]
  }, [_vm._v(_vm._s(this.$parent.lang['size']) + ": " + _vm._s(_vm.item.options.size))]), _vm._v(" "), _c('div', [_c('a', {
    attrs: {
      "id": "wishlist",
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.moveToWishlist()
      }
    }
  }, [_c('span', {
    staticClass: "glyphicon glyphicon-heart"
  }), _vm._v(" " + _vm._s(this.$parent.lang['move-to-wishlist']))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-7 text-right"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('div', {
    staticClass: "col-lg-2 col-md-12  form-group"
  }, [_c('div', {
    staticClass: "cart-header hidden-lg col-md-6 col-sm-6"
  }, [_vm._v(_vm._s(this.$parent.lang['quantity']))]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-12 col-md-6 col-sm-6"
  }, [(_vm.item.options.stock != '0') ? _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.item.qty),
      expression: "item.qty"
    }],
    staticClass: "form-control text-right",
    staticStyle: {
      "margin-left": "-14px"
    },
    attrs: {
      "id": 'q' + _vm.item.id,
      "type": "number",
      "max": _vm.item.options.stock,
      "min": "1"
    },
    domProps: {
      "value": (_vm.item.qty)
    },
    on: {
      "click": function($event) {
        _vm.updateqty()
      },
      "keyup": function($event) {
        _vm.stock()
      },
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.item.qty = $event.target.value
      }
    }
  }) : _vm._e(), _vm._v(" "), (_vm.item.options.stock == '0') ? _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.item.qty),
      expression: "item.qty"
    }],
    staticClass: "form-control text-right",
    staticStyle: {
      "margin-left": "-14px"
    },
    attrs: {
      "id": 'q' + _vm.item.id,
      "type": "number",
      "min": "1"
    },
    domProps: {
      "value": (_vm.item.qty)
    },
    on: {
      "click": function($event) {
        _vm.updateqty()
      },
      "keyup": function($event) {
        _vm.stock()
      },
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.item.qty = $event.target.value
      }
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-3 col-md-12 form-group"
  }, [_c('div', {
    staticClass: "cart-header hidden-lg col-md-6 col-sm-6"
  }, [_vm._v(_vm._s(this.$parent.lang['item-price']))]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-12 col-md-6 col-sm-6 cart-item-price"
  }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(_vm._f("currency")(_vm.sale_price)) + "\n\t\t\t\t\t")])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-3 col-md-12 form-group"
  }, [_c('div', {
    staticClass: "cart-header hidden-lg col-md-6 col-sm-6"
  }, [_vm._v(_vm._s(this.$parent.lang['shipping-fee']))]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-12 col-md-6 col-sm-6 cart-item-price shipping"
  }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(_vm._f("currency")(_vm.shipping)) + "\n\t\t\t\t\t")])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-4 col-md-12 form-group"
  }, [_c('div', {
    staticClass: "cart-header hidden-lg col-md-6 col-sm-6"
  }, [_vm._v(_vm._s(this.$parent.lang['subtotal']))]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-12 col-md-6 col-sm-6 cart-item-price"
  }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(_vm._f("currency")(_vm.subTotal)) + "\n\t\t\t\t\t")])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-22884846", module.exports)
  }
}

/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "table-responsive"
  }, [_c('table', {
    staticClass: "table table-hover"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.items), function(item) {
    return _c('tr', {
      key: item.id
    }, [_c('td', [_vm._v(_vm._s(item.name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(item.qty))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(_vm.sale_price)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(item.qty * _vm.sale_price)))])])
  }))]), _vm._v(" "), _vm._t("footer")], 2)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Item/s")]), _vm._v(" "), _c('th', [_vm._v("Qty")]), _vm._v(" "), _c('th', [_vm._v("Price")]), _vm._v(" "), _c('th', [_vm._v("Subtotal")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-2c8dc051", module.exports)
  }
}

/***/ }),

/***/ 388:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "table-responsive"
  }, [_c('table', {
    staticClass: "table table-hover"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.orders), function(order) {
    return _c('tr', {
      key: order.id
    }, [_c('td', [_vm._v(_vm._s(order.name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(order.qty))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(_vm.sale_price)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("currency")(order.qty * _vm.sale_price)))])])
  }))]), _vm._v(" "), _vm._t("footer")], 2)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Item/s")]), _vm._v(" "), _c('th', [_vm._v("Qty")]), _vm._v(" "), _c('th', [_vm._v("Price")]), _vm._v(" "), _c('th', [_vm._v("Subtotal")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-600fbfae", module.exports)
  }
}

/***/ }),

/***/ 423:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(173);


/***/ }),

/***/ 7:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-sm' }
    }
});

/***/ }),

/***/ 8:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'size': { default: 'modal-md' },
        'id': { required: true },
        'contentSize': { default: '12' }

    }
});

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(7),
  /* template */
  __webpack_require__(12),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-653e7369", Component.options)
  } else {
    hotAPI.reload("data-v-653e7369", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvTW9kYWwudnVlP2UwY2EiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL01vZGFsLnZ1ZT9iNGIzKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlPzA0MzIiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9jYXJ0LmpzIiwid2VicGFjazovLy9DYXJ0SXRlbS52dWUiLCJ3ZWJwYWNrOi8vL0NhcnRMaXN0LnZ1ZSIsIndlYnBhY2s6Ly8vT3JkZXJMaXN0LnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvY2FydC9DYXJ0SXRlbS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NhcnQvQ2FydExpc3QudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9jYXJ0L09yZGVyTGlzdC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NhcnQvQ2FydEl0ZW0udnVlPzkwNTEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NhcnQvQ2FydExpc3QudnVlPzVhNTciLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NhcnQvT3JkZXJMaXN0LnZ1ZT9hMjViIiwid2VicGFjazovLy9EaWFsb2dNb2RhbC52dWUiLCJ3ZWJwYWNrOi8vL01vZGFsLnZ1ZT80ZDg3Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWUiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImZpbHRlciIsInZhbHVlIiwibnVtYmVyV2l0aENvbW1hcyIsInBhcnNlRmxvYXQiLCJ0b0ZpeGVkIiwiYXBwIiwiZWwiLCJkYXRhIiwiaXRlbXMiLCJzZWxlY3RlZCIsImR1cF9pdGVtcyIsInByb2RfaWQiLCJxdHkiLCJzaG93VW5kbyIsIml0ZW1fbmFtZXMiLCJvcHRpb25UeXBlIiwic2VsZWN0ZWRUeXBlIiwiZXdhbGxldCIsIm9yZGVyX2lkIiwibmFtZSIsImFkZHJlc3MiLCJzaGlwcGluZyIsInRvdGFsIiwib3JkZXJzIiwibGFuZyIsImN1cnJlbnRfc3RlcF9uYW1lIiwiY3VycmVudF9zdGVwIiwicmVhY2hlZF9zdGVwIiwiYWRkcmVzc19zZXQiLCJmb3JtIiwiRm9ybSIsInR5cGVfaWQiLCJ0eXBlIiwibnVtYmVyIiwiYmFyYW5nYXkiLCJjaXR5IiwicHJvdmluY2UiLCJ6aXBfY29kZSIsImxhbmRtYXJrIiwibm90ZXMiLCJhbHRlcm5hdGVfbnVtYmVyIiwic3VidG90YWwiLCJ0YXgiLCJjaGFyZ2UiLCJiYXNlVVJMIiwiY3JlYXRlZCIsImdldFRyYW5zbGF0aW9uIiwiYXhpb3MiLCJnZXQiLCJnZXRDYXJ0IiwibG9jYXRpb24iLCJvcmlnaW4iLCJnZXRUeXBlIiwiTGFyYXZlbCIsInVzZXIiLCJnZXRBY3RpdmUiLCJnZXRVc2FibGVCYWxhbmNlIiwiYXhpb3NBUEl2MSIsImFsbCIsInRoZW4iLCJzcHJlYWQiLCJ0cmFuc2xhdGlvbiIsImNhcnQiLCJhY3RpdmUiLCJiYWxhbmNlIiwiY2FydDIiLCIkIiwibWFwIiwiaW5kZXgiLCJzZXRUaW1lb3V0IiwibWF0ZXJpYWwiLCJpbml0IiwibmV3dHlwZSIsInB1c2giLCJsZW5ndGgiLCJpZCIsInJlc3VsdCIsIm5hbWVfb2ZfcmVjZWl2ZXIiLCJjb250YWN0X251bWJlciIsInJlcGxhY2UiLCJhbHRlcm5hdGVfY29udGFjdF9udW1iZXIiLCJzdWNjZXNzIiwidXNhYmxlIiwiY2F0Y2giLCJub29wIiwibWV0aG9kcyIsInNob3dSZW1vdmVEaWFsb2ciLCJtb2RhbCIsInNob3dDaGVja291dE1vZGFsIiwidG9hc3RyIiwiZXJyb3IiLCJzZWVTdGVwQWRkcmVzcyIsIm1vdmVUb1dpc2hsaXN0Iiwicm93SWQiLCJpc0F1dGhlbnRpY2F0ZWQiLCJmaW5kSW5kZXgiLCJwYXJhbXMiLCJyb3dfaWQiLCJzcGxpY2UiLCJmYXYiLCJwYXJzZUludCIsInRleHQiLCJzaG93IiwicmVtb3ZlSXRlbXMiLCJzYXZlUHJvZmlsZSIsInN1Ym1pdCIsInJlc3BvbnNlIiwidmFsIiwibWVzc2FnZSIsImRlbGV0ZVByb2ZpbGUiLCJwbGFjZU9yZGVyIiwiZXdhbGxldF9iYWwiLCJoaWRlIiwicHJvcCIsInVwZGF0ZVF0eSIsInN0b2NrIiwiZ2V0QWRkcmVzc0RldGFpbHMiLCJldmVudCIsInRhcmdldCIsIm51bWJlcnMiLCJwcmludCIsImh0bWxfY29udGFpbmVyIiwiYm9keSIsImh0bWwiLCJteVdpbmRvdyIsIndpbmRvdyIsIm9wZW4iLCJkb2N1bWVudCIsIndyaXRlIiwiY2xvc2UiLCJmb2N1cyIsImNvbXB1dGVkIiwidG90YWxfcHJpY2UiLCJzdW0iLCJzaGlwcGluZ0FuZENoYXJnZSIsInZhdCIsImkiLCJzYWxlUHJpY2UiLCJvcHRpb25zIiwic2FsZV9wcmljZSIsInNoaXBwaW5nX2ZlZSIsIm9uIiwia2V5cHJlc3MiLCJlIiwid2hpY2giLCJ4IiwidG9TdHJpbmciXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQTtBQUNBO0FBQ0EsdUJBQXFKO0FBQ3JKO0FBQ0Esd0JBQTRHO0FBQzVHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUNyREEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN6Q0FBLElBQUlDLFNBQUosQ0FBYyxXQUFkLEVBQTRCLG1CQUFBQyxDQUFRLEdBQVIsQ0FBNUI7QUFDQUYsSUFBSUMsU0FBSixDQUFjLFlBQWQsRUFBNkIsbUJBQUFDLENBQVEsR0FBUixDQUE3QjtBQUNBRixJQUFJQyxTQUFKLENBQWMsYUFBZCxFQUE4QixtQkFBQUMsQ0FBUSxHQUFSLENBQTlCO0FBQ0FGLElBQUlDLFNBQUosQ0FBYyxjQUFkLEVBQStCLG1CQUFBQyxDQUFRLENBQVIsQ0FBL0I7QUFDQUYsSUFBSUMsU0FBSixDQUFjLE9BQWQsRUFBd0IsbUJBQUFDLENBQVEsRUFBUixDQUF4Qjs7QUFFQUYsSUFBSUcsTUFBSixDQUFXLFVBQVgsRUFBdUIsVUFBVUMsS0FBVixFQUFpQjtBQUNwQyxRQUFRLE1BQUlDLGlCQUFpQkMsV0FBV0YsS0FBWCxFQUFrQkcsT0FBbEIsQ0FBMEIsQ0FBMUIsQ0FBakIsQ0FBWjtBQUNILENBRkQ7O0FBS0EsSUFBSUMsTUFBTSxJQUFJUixHQUFKLENBQVE7QUFDakJTLEtBQUksaUJBRGE7QUFFakJDLE9BQU07QUFDTEMsU0FBTSxFQUREO0FBRUxDLFlBQVMsRUFGSjtBQUdMQyxhQUFVLEVBSEw7QUFJTEMsV0FBUSxFQUpIO0FBS0xDLE9BQUksRUFMQztBQU1MQyxZQUFTLEtBTko7QUFPTEMsY0FBVyxFQVBOO0FBUUxDLGNBQVcsRUFSTjtBQVNMQyxnQkFBYSxDQVRSO0FBVUxDLFdBQVEsQ0FWSDtBQVdMQyxZQUFTLENBWEo7QUFZTEMsUUFBSyxFQVpBO0FBYUxDLFdBQVEsRUFiSDtBQWNMQyxZQUFTLENBZEo7QUFlTEMsU0FBTSxDQWZEO0FBZ0JMQyxVQUFPLEVBaEJGO0FBaUJMQyxRQUFLLEVBakJBOztBQW1CTDtBQUNBQyxxQkFBa0Isa0JBcEJiO0FBcUJMQyxnQkFBYSxDQXJCUjtBQXNCTEMsZ0JBQWEsQ0F0QlI7QUF1QkxDLGVBQVksS0F2QlA7QUF3QkZDLFFBQU0sSUFBSUMsSUFBSixDQUFTO0FBQ1pDLFlBQVEsRUFESTtBQUVaQyxTQUFNLElBRk07QUFHWmIsU0FBTSxJQUhNO0FBSVpjLFdBQVEsSUFKSTtBQUtaYixZQUFTLElBTEc7QUFNWmMsYUFBVSxJQU5FO0FBT1pDLFNBQU0sSUFQTTtBQVFaQyxhQUFVLElBUkU7QUFTWkMsYUFBVSxJQVRFO0FBVVpDLGFBQVUsSUFWRTtBQVdaQyxVQUFPLElBWEs7QUFZWkMscUJBQWtCLElBWk47QUFhWkMsYUFBVSxJQWJFO0FBY1puQixVQUFPLElBZEs7QUFlWm9CLFFBQUssSUFmTztBQWdCWnJCLGFBQVUsSUFoQkU7QUFpQlpzQixXQUFRO0FBakJJLEdBQVQsRUFrQkosRUFBRUMsU0FBUyxPQUFYLEVBbEJJO0FBeEJKLEVBRlc7O0FBK0NqQkMsUUEvQ2lCLHFCQStDUjtBQUFBOztBQUNSLFdBQVNDLGNBQVQsR0FBMEI7QUFDaEIsVUFBT0MsTUFBTUMsR0FBTixDQUFVLGlCQUFWLENBQVA7QUFDSDs7QUFFRCxXQUFTQyxPQUFULEdBQW1CO0FBQ2YsVUFBT0YsTUFBTUMsR0FBTixDQUFVRSxTQUFTQyxNQUFULEdBQWtCLFlBQTVCLENBQVA7QUFDSDs7QUFFUCxXQUFTQyxPQUFULEdBQW1CO0FBQ2xCLE9BQUdDLFFBQVFDLElBQVgsRUFDQSxPQUFPUCxNQUFNQyxHQUFOLENBQVVFLFNBQVNDLE1BQVQsR0FBa0IsZUFBNUIsQ0FBUDtBQUNBO0FBQ0QsV0FBU0ksU0FBVCxHQUFxQjtBQUNwQixPQUFHRixRQUFRQyxJQUFYLEVBQ0EsT0FBT1AsTUFBTUMsR0FBTixDQUFVRSxTQUFTQyxNQUFULEdBQWtCLGlCQUE1QixDQUFQO0FBQ0E7QUFDRCxXQUFTSyxnQkFBVCxHQUE0QjtBQUMzQixPQUFHSCxRQUFRQyxJQUFYLEVBQ0EsT0FBT0csV0FBV1QsR0FBWCxDQUFlLFlBQWYsQ0FBUDtBQUNBOztBQUVERCxRQUFNVyxHQUFOLENBQVUsQ0FDVFosZ0JBRFMsRUFFVEcsU0FGUyxFQUdURyxTQUhTLEVBSVRHLFdBSlMsRUFLVEMsa0JBTFMsQ0FBVixFQU1HRyxJQU5ILENBTVFaLE1BQU1hLE1BQU4sQ0FDUCxVQUNDQyxXQURELEVBRUNDLElBRkQsRUFHQzlCLElBSEQsRUFJQytCLE1BSkQsRUFLQ0MsT0FMRCxFQU1LO0FBQ0osU0FBS3hDLElBQUwsR0FBWXFDLFlBQVl0RCxJQUFaLENBQWlCQSxJQUE3QjtBQUNBLE9BQUkwRCxRQUFRQyxFQUFFQyxHQUFGLENBQU1MLEtBQUt2RCxJQUFMLENBQVV1RCxJQUFoQixFQUFzQixVQUFTN0QsS0FBVCxFQUFnQm1FLEtBQWhCLEVBQXVCO0FBQ3JELFdBQU8sQ0FBQ25FLEtBQUQsQ0FBUDtBQUNILElBRlcsQ0FBWjtBQUdBb0UsY0FBVyxZQUFVO0FBQUNILE1BQUVJLFFBQUYsQ0FBV0MsSUFBWDtBQUFtQixJQUF6QyxFQUEyQyxHQUEzQztBQUNBLFNBQUsvRCxLQUFMLEdBQWF5RCxLQUFiOztBQUVBLE9BQUlPLFVBQVUsRUFBQyxNQUFNLENBQVAsRUFBVSxRQUFRLE1BQUtoRCxJQUFMLENBQVUsYUFBVixDQUFsQixFQUE0QyxhQUFhLEtBQXpELEVBQWQ7QUFDQSxPQUFJUSxPQUFPQSxLQUFLekIsSUFBaEI7QUFDQXlCLFFBQUt5QyxJQUFMLENBQVVELE9BQVY7QUFDQSxTQUFLekQsVUFBTCxHQUFrQmlCLElBQWxCOztBQUVBLE9BQUcrQixPQUFPeEQsSUFBUCxDQUFZbUUsTUFBWixLQUF1QixDQUExQixFQUE2Qjs7QUFFNUIsVUFBSzFELFlBQUwsR0FBb0IrQyxPQUFPeEQsSUFBUCxDQUFZLENBQVosRUFBZW9FLEVBQW5DOztBQUVBNUIsVUFBTUMsR0FBTixDQUFVRSxTQUFTQyxNQUFULEdBQWtCLDBCQUFsQixHQUErQyxNQUFLbkMsWUFBOUQsRUFDQzJDLElBREQsQ0FDTSxrQkFBVTtBQUNmLFNBQUl2QyxVQUFVd0QsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWVhLE9BQTdCO0FBQ0EsV0FBS1MsSUFBTCxDQUFVRSxPQUFWLEdBQW9CLE1BQUtmLFlBQXpCO0FBQ0EsV0FBS2EsSUFBTCxDQUFVRyxJQUFWLEdBQWlCNEMsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWV5QixJQUFoQztBQUNBLFdBQUtILElBQUwsQ0FBVVYsSUFBVixHQUFpQnlELE9BQU9yRSxJQUFQLENBQVksQ0FBWixFQUFlc0UsZ0JBQWhDO0FBQ0EsV0FBS2hELElBQUwsQ0FBVUksTUFBVixHQUFtQjJDLE9BQU9yRSxJQUFQLENBQVksQ0FBWixFQUFldUUsY0FBbEM7QUFDQSxXQUFLakQsSUFBTCxDQUFVVCxPQUFWLEdBQW9CQSxRQUFRMkQsT0FBUixDQUFnQixnQkFBaEIsRUFBaUMsRUFBakMsQ0FBcEI7QUFDQSxXQUFLbEQsSUFBTCxDQUFVSyxRQUFWLEdBQXFCMEMsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWUyQixRQUFwQztBQUNBLFdBQUtMLElBQUwsQ0FBVU0sSUFBVixHQUFpQnlDLE9BQU9yRSxJQUFQLENBQVksQ0FBWixFQUFlNEIsSUFBaEM7QUFDQSxXQUFLTixJQUFMLENBQVVPLFFBQVYsR0FBcUJ3QyxPQUFPckUsSUFBUCxDQUFZLENBQVosRUFBZTZCLFFBQXBDO0FBQ0EsV0FBS1AsSUFBTCxDQUFVUSxRQUFWLEdBQXFCdUMsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWU4QixRQUFwQztBQUNBLFdBQUtSLElBQUwsQ0FBVVMsUUFBVixHQUFxQnNDLE9BQU9yRSxJQUFQLENBQVksQ0FBWixFQUFlK0IsUUFBcEM7QUFDQSxXQUFLVCxJQUFMLENBQVVVLEtBQVYsR0FBa0IsRUFBbEI7QUFDQSxXQUFLVixJQUFMLENBQVVXLGdCQUFWLEdBQTZCb0MsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWV5RSx3QkFBNUM7QUFDTSxLQWZQO0FBaUJBLElBckJELE1BcUJPO0FBQ04sVUFBS2hFLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQTs7QUFFRCxTQUFLQyxPQUFMLEdBQWUrQyxRQUFRekQsSUFBUixDQUFhMEUsT0FBYixHQUFzQjlFLFdBQVc2RCxRQUFRekQsSUFBUixDQUFhQSxJQUFiLENBQWtCMkUsTUFBN0IsQ0FBdEIsR0FBMkQsQ0FBMUU7QUFDRCxHQTlDTyxDQU5SLEVBcURDQyxLQXJERCxDQXFET2pCLEVBQUVrQixJQXJEVDtBQXNEQSxFQTNIZ0I7OztBQTZIakJDLFVBQVE7QUFDUEMsa0JBRE8sOEJBQ1c7QUFDakIsT0FBRyxLQUFLN0UsUUFBTCxDQUFjaUUsTUFBakIsRUFDQTtBQUNDUixNQUFFLGVBQUYsRUFBbUJxQixLQUFuQixDQUF5QixRQUF6QjtBQUNBO0FBQ0QsR0FOTTtBQU9QQyxtQkFQTywrQkFPWTtBQUNsQixPQUFHbkMsUUFBUUMsSUFBWCxFQUFnQjtBQUNmLFFBQUcsS0FBSzlDLEtBQUwsSUFBYyxDQUFqQixFQUNBMEQsRUFBRSxnQkFBRixFQUFvQnFCLEtBQXBCLENBQTBCLFFBQTFCO0FBQ0EsSUFIRCxNQUdPO0FBQ0hFLFdBQU9DLEtBQVAsQ0FBYSxLQUFLbEUsSUFBTCxDQUFVLFVBQVYsQ0FBYjtBQUNIO0FBQ0QsR0FkTTs7QUFlUDtBQUNBbUUsZ0JBaEJPLDRCQWdCUztBQUNmLE9BQUcsS0FBS25GLEtBQUwsSUFBYyxDQUFqQixFQUFtQjtBQUNuQixTQUFLa0IsWUFBTCxHQUFvQixDQUFwQjtBQUNBLFNBQUtELGlCQUFMLEdBQXlCLGtCQUF6QjtBQUNDO0FBQ0QsR0FyQk07QUFzQlBtRSxnQkF0Qk8sMEJBc0JRakIsRUF0QlIsRUFzQldrQixLQXRCWCxFQXNCaUI7QUFBQTs7QUFDdkIsT0FBR3hDLFFBQVF5QyxlQUFSLEtBQTRCLElBQS9CLEVBQW9DO0FBQ3BDLFFBQUkxQixRQUFRLEtBQUs1RCxLQUFMLENBQVd1RixTQUFYLENBQXNCLFVBQUN6RixFQUFEO0FBQUEsWUFBUUEsR0FBR3FFLEVBQUgsS0FBVUEsRUFBbEI7QUFBQSxLQUF0QixDQUFaO0FBQ0E1QixVQUFNQyxHQUFOLENBQVVFLFNBQVNDLE1BQVQsR0FBa0IsY0FBNUIsRUFBMkM7QUFDdkM2QyxhQUFRO0FBQ1BDLGNBQVFKO0FBREQ7QUFEK0IsS0FBM0MsRUFLQ2xDLElBTEQsQ0FLTSxrQkFBVTtBQUNmLFlBQUtuRCxLQUFMLENBQVcwRixNQUFYLENBQWtCOUIsS0FBbEIsRUFBeUIsQ0FBekI7QUFDQVgsZ0JBQVdULEdBQVgsQ0FBZSxxQkFBcUIyQixFQUFwQyxFQUNhaEIsSUFEYixDQUNrQixrQkFBVTtBQUNmLFVBQUl3QyxNQUFNQyxTQUFTbEMsRUFBRSxZQUFGLEVBQWdCbUMsSUFBaEIsRUFBVCxJQUFpQyxDQUEzQztBQUNBbkMsUUFBRSxZQUFGLEVBQWdCbUMsSUFBaEIsQ0FBcUJGLEdBQXJCLEVBQTBCRyxJQUExQjtBQUNaYixhQUFPUixPQUFQLENBQWUsT0FBS3pELElBQUwsQ0FBVSxpQkFBVixDQUFmO0FBQ1ksTUFMYjtBQU1BLEtBYkQ7QUFjQyxJQWhCRCxNQWdCTztBQUNOaUUsV0FBT0MsS0FBUCxDQUFhLEtBQUtsRSxJQUFMLENBQVUsVUFBVixDQUFiO0FBQ0E7QUFDRCxHQTFDTTtBQTJDUCtFLGFBM0NPLHVCQTJDSzVCLEVBM0NMLEVBMkNReEQsSUEzQ1IsRUEyQ2EwRSxLQTNDYixFQTJDbUI7QUFBQTs7QUFDekIsT0FBSXpCLFFBQVEsS0FBSzVELEtBQUwsQ0FBV3VGLFNBQVgsQ0FBc0IsVUFBQ3pGLEVBQUQ7QUFBQSxXQUFRQSxHQUFHcUUsRUFBSCxLQUFVQSxFQUFsQjtBQUFBLElBQXRCLENBQVo7QUFDQTVCLFNBQU1DLEdBQU4sQ0FBVUUsU0FBU0MsTUFBVCxHQUFrQixjQUE1QixFQUEyQztBQUN2QzZDLFlBQVE7QUFDUEMsYUFBUUo7QUFERDtBQUQrQixJQUEzQyxFQUtDbEMsSUFMRCxDQUtNLGtCQUFVO0FBQ2YsV0FBS25ELEtBQUwsQ0FBVzBGLE1BQVgsQ0FBa0I5QixLQUFsQixFQUF5QixDQUF6QjtBQUNBcUIsV0FBT1IsT0FBUCxDQUFlLE9BQUt6RCxJQUFMLENBQVUsU0FBVixDQUFmLEVBQW9DTCxJQUFwQztBQUNBLElBUkQ7QUFTQSxHQXRETTtBQXVEUHFGLGFBdkRPLHlCQXVETTtBQUFBOztBQUNQLFFBQUszRSxJQUFMLENBQVU0RSxNQUFWLENBQWlCLE1BQWpCLEVBQXlCLGNBQXpCLEVBQ0k5QyxJQURKLENBQ1Msa0JBQVU7QUFDdkIsUUFBR2lCLE9BQU84QixRQUFQLElBQWlCLENBQXBCLEVBQXNCO0FBQ3JCakIsWUFBT1IsT0FBUCxDQUFlLE9BQUt6RCxJQUFMLENBQVUsZUFBVixDQUFmO0FBQ0F1QixXQUFNQyxHQUFOLENBQVVFLFNBQVNDLE1BQVQsR0FBa0IsZUFBNUIsRUFDQ1EsSUFERCxDQUNNLGtCQUFVO0FBQ2YsVUFBSWEsVUFBVSxFQUFDLE1BQU0sQ0FBUCxFQUFVLFFBQVEsYUFBbEIsRUFBaUMsYUFBYSxLQUE5QyxFQUFkO0FBQ0EsVUFBSXhDLE9BQU80QyxPQUFPckUsSUFBbEI7QUFDQXlCLFdBQUt5QyxJQUFMLENBQVVELE9BQVY7QUFDQSxhQUFLekQsVUFBTCxHQUFrQmlCLElBQWxCO0FBQ0EsVUFBR2tDLEVBQUUsa0JBQUYsRUFBc0J5QyxHQUF0QixNQUE2QixDQUFoQyxFQUFrQztBQUNqQyxjQUFLOUUsSUFBTCxDQUFVRyxJQUFWLEdBQWlCLEVBQWpCO0FBQ0EsY0FBS0gsSUFBTCxDQUFVVixJQUFWLEdBQWlCLEVBQWpCO0FBQ0EsY0FBS1UsSUFBTCxDQUFVSSxNQUFWLEdBQW1CLEVBQW5CO0FBQ0EsY0FBS0osSUFBTCxDQUFVVCxPQUFWLEdBQW9CLEVBQXBCO0FBQ0EsY0FBS1MsSUFBTCxDQUFVSyxRQUFWLEdBQXFCLEVBQXJCO0FBQ0EsY0FBS0wsSUFBTCxDQUFVTSxJQUFWLEdBQWlCLEVBQWpCO0FBQ0EsY0FBS04sSUFBTCxDQUFVTyxRQUFWLEdBQXFCLEVBQXJCO0FBQ0EsY0FBS1AsSUFBTCxDQUFVUSxRQUFWLEdBQXFCLEVBQXJCO0FBQ0EsY0FBS1IsSUFBTCxDQUFVUyxRQUFWLEdBQXFCLEVBQXJCO0FBQ0EsY0FBS1QsSUFBTCxDQUFVVSxLQUFWLEdBQWtCLEVBQWxCO0FBQ0EsY0FBS1YsSUFBTCxDQUFVVyxnQkFBVixHQUE2QixFQUE3QjtBQUNBO0FBQ0ssTUFuQlA7QUFvQkEsS0F0QkQsTUF1Qkk7QUFDTWlELFlBQU9DLEtBQVAsQ0FBYWQsT0FBT2dDLE9BQXBCO0FBQ0E7QUFDRixJQTVCSixFQTZCSXpCLEtBN0JKLENBNkJVLGlCQUFTO0FBQ2RNLFdBQU9DLEtBQVAsQ0FBYSxPQUFLbEUsSUFBTCxDQUFVLFFBQVYsQ0FBYjtBQUNELElBL0JKO0FBZ0NMLEdBeEZNO0FBMEZQcUYsZUExRk8sMkJBMEZRO0FBQUE7O0FBQ1QsUUFBS2hGLElBQUwsQ0FBVTRFLE1BQVYsQ0FBaUIsTUFBakIsRUFBeUIsZ0JBQXpCLEVBQ0k5QyxJQURKLENBQ1Msa0JBQVU7QUFDdkIsUUFBR2lCLE9BQU84QixRQUFQLElBQWlCLENBQXBCLEVBQXNCO0FBQ3JCakIsWUFBT1IsT0FBUCxDQUFlLE9BQUt6RCxJQUFMLENBQVUsaUJBQVYsQ0FBZjtBQUNBdUIsV0FBTUMsR0FBTixDQUFVRSxTQUFTQyxNQUFULEdBQWtCLGVBQTVCLEVBQ0NRLElBREQsQ0FDTSxrQkFBVTtBQUNmLFVBQUlhLFVBQVUsRUFBQyxNQUFNLENBQVAsRUFBVSxRQUFRLGFBQWxCLEVBQWlDLGFBQWEsSUFBOUMsRUFBZDtBQUNBLFVBQUl4QyxPQUFPNEMsT0FBT3JFLElBQWxCO0FBQ0F5QixXQUFLeUMsSUFBTCxDQUFVRCxPQUFWO0FBQ0EsYUFBS3pELFVBQUwsR0FBa0JpQixJQUFsQjtBQUNBLGFBQUtoQixZQUFMLEdBQW9CLENBQXBCO0FBQ0EsYUFBS2EsSUFBTCxDQUFVRyxJQUFWLEdBQWlCLEVBQWpCO0FBQ0EsYUFBS0gsSUFBTCxDQUFVVixJQUFWLEdBQWlCLEVBQWpCO0FBQ0EsYUFBS1UsSUFBTCxDQUFVSSxNQUFWLEdBQW1CLEVBQW5CO0FBQ0EsYUFBS0osSUFBTCxDQUFVVCxPQUFWLEdBQW9CLEVBQXBCO0FBQ0EsYUFBS1MsSUFBTCxDQUFVSyxRQUFWLEdBQXFCLEVBQXJCO0FBQ0EsYUFBS0wsSUFBTCxDQUFVTSxJQUFWLEdBQWlCLEVBQWpCO0FBQ0EsYUFBS04sSUFBTCxDQUFVTyxRQUFWLEdBQXFCLEVBQXJCO0FBQ0EsYUFBS1AsSUFBTCxDQUFVUSxRQUFWLEdBQXFCLEVBQXJCO0FBQ0EsYUFBS1IsSUFBTCxDQUFVUyxRQUFWLEdBQXFCLEVBQXJCO0FBQ0EsYUFBS1QsSUFBTCxDQUFVVSxLQUFWLEdBQWtCLEVBQWxCO0FBQ0EsYUFBS1YsSUFBTCxDQUFVVyxnQkFBVixHQUE2QixFQUE3QjtBQUNNLE1BbEJQO0FBbUJBLEtBckJELE1Bc0JJO0FBQ01pRCxZQUFPQyxLQUFQLENBQWFkLE9BQU9nQyxPQUFwQjtBQUNBO0FBQ0YsSUEzQkosRUE0Qkl6QixLQTVCSixDQTRCVSxpQkFBUztBQUNkTSxXQUFPQyxLQUFQLENBQWEsT0FBS2xFLElBQUwsQ0FBVSxXQUFWLENBQWIsRUFBb0MsT0FBS0EsSUFBTCxDQUFVLGFBQVYsQ0FBcEM7QUFDRCxJQTlCSjtBQStCTCxHQTFITTtBQTJIUHNGLFlBM0hPLHdCQTJISztBQUFBOztBQUNULE9BQUcsS0FBS3RHLEtBQUwsSUFBYyxDQUFqQixFQUFtQjtBQUNoQixTQUFLcUIsSUFBTCxDQUFVNEUsTUFBVixDQUFpQixNQUFqQixFQUF5QixXQUF6QixFQUNJOUMsSUFESixDQUNTLGtCQUFVO0FBQ3ZCLFNBQUdpQixPQUFPOEIsUUFBUCxJQUFpQixDQUFwQixFQUFzQjtBQUNyQixhQUFLekYsT0FBTCxHQUFlMkQsT0FBT21DLFdBQXRCO0FBQ0EsYUFBSzdGLFFBQUwsR0FBZ0IwRCxPQUFPRCxFQUF2QjtBQUNBLGFBQUt4RCxJQUFMLEdBQVl5RCxPQUFPekQsSUFBbkI7QUFDQSxhQUFLQyxPQUFMLEdBQWV3RCxPQUFPeEQsT0FBdEI7QUFDQSxhQUFLQyxRQUFMLEdBQWdCLE9BQUtRLElBQUwsQ0FBVVIsUUFBMUI7QUFDQSxhQUFLQyxLQUFMLEdBQWFzRCxPQUFPdEQsS0FBcEI7QUFDQSxhQUFLQyxNQUFMLEdBQWMsT0FBS2YsS0FBbkI7QUFDUyxhQUFLa0IsWUFBTCxHQUFvQixDQUFwQjtBQUNULGFBQUtELGlCQUFMLEdBQXlCLGVBQXpCO0FBQ0EsYUFBS2pCLEtBQUwsR0FBYSxFQUFiO0FBQ0EsYUFBS0UsU0FBTCxHQUFpQixFQUFqQjtBQUNBK0UsYUFBT1IsT0FBUCxDQUFlLE9BQUt6RCxJQUFMLENBQVUsY0FBVixDQUFmLEVBQTBDLE9BQUtBLElBQUwsQ0FBVSxVQUFWLElBQXdCLE9BQUtOLFFBQXZFO0FBQ0FnRCxRQUFFLE1BQUYsRUFBVThDLElBQVY7QUFDQSxNQWRELE1BZUk7QUFDSDlDLFFBQUUsYUFBRixFQUFpQitDLElBQWpCLENBQXNCLFVBQXRCLEVBQWtDLEtBQWxDO0FBQ1N4QixhQUFPQyxLQUFQLENBQWFkLE9BQU9nQyxPQUFwQjtBQUNBO0FBQ0YsS0FyQkosRUFzQkl6QixLQXRCSixDQXNCVSxpQkFBUztBQUN2QixZQUFLekQsWUFBTCxHQUFvQixDQUFwQjtBQUNBLFlBQUtELGlCQUFMLEdBQXlCLGtCQUF6QjtBQUNBeUMsT0FBRSxhQUFGLEVBQWlCK0MsSUFBakIsQ0FBc0IsVUFBdEIsRUFBa0MsS0FBbEM7QUFDU3hCLFlBQU9DLEtBQVAsQ0FBYSxPQUFLbEUsSUFBTCxDQUFVLFFBQVYsQ0FBYjtBQUNELEtBM0JKO0FBNEJDO0FBQ04sR0ExSk07QUEySlAwRixXQTNKTyxxQkEySkd0RyxHQTNKSCxFQTJKT2lGLEtBM0pQLEVBMkphO0FBQ25COUMsU0FBTUMsR0FBTixDQUFVRSxTQUFTQyxNQUFULEdBQWtCLGlCQUE1QixFQUE4QztBQUMxQzZDLFlBQVE7QUFDUEMsYUFBT0osS0FEQTtBQUVWakYsVUFBSUE7QUFGTTtBQURrQyxJQUE5QztBQU1BLEdBbEtNO0FBbUtQdUcsT0FuS08saUJBbUtEeEMsRUFuS0MsRUFtS0UvRCxHQW5LRixFQW1LTXVHLE1BbktOLEVBbUtZdEIsS0FuS1osRUFtS2tCO0FBQ3hCLE9BQUdPLFNBQVNlLE1BQVQsS0FBaUIsQ0FBcEIsRUFBc0I7QUFDckJwRSxVQUFNQyxHQUFOLENBQVVFLFNBQVNDLE1BQVQsR0FBa0IsaUJBQTVCLEVBQThDO0FBQzFDNkMsYUFBUTtBQUNQQyxjQUFPSixLQURBO0FBRVZqRixXQUFJO0FBRk07QUFEa0MsS0FBOUM7QUFNQTtBQUNELE9BQUd3RixTQUFTeEYsR0FBVCxJQUFjd0YsU0FBU2UsTUFBVCxDQUFkLElBQWlDZixTQUFTZSxNQUFULEtBQWlCLENBQXJELEVBQXVEO0FBQ3REakQsTUFBRSxPQUFLUyxFQUFQLEVBQVdnQyxHQUFYLENBQWVRLE1BQWY7QUFDQXBFLFVBQU1DLEdBQU4sQ0FBVUUsU0FBU0MsTUFBVCxHQUFrQixpQkFBNUIsRUFBOEM7QUFDMUM2QyxhQUFRO0FBQ1BDLGNBQU9KLEtBREE7QUFFVmpGLFdBQUl1RztBQUZNO0FBRGtDLEtBQTlDO0FBTUE7QUFDRCxPQUFHZixTQUFTeEYsR0FBVCxLQUFld0YsU0FBU2UsTUFBVCxDQUFsQixFQUFrQztBQUNqQ3BFLFVBQU1DLEdBQU4sQ0FBVUUsU0FBU0MsTUFBVCxHQUFrQixpQkFBNUIsRUFBOEM7QUFDMUM2QyxhQUFRO0FBQ1BDLGNBQU9KLEtBREE7QUFFVmpGLFdBQUlBO0FBRk07QUFEa0MsS0FBOUM7QUFNQTtBQUNELE9BQUdBLE9BQUssRUFBTCxJQUFXQSxPQUFLLEdBQW5CLEVBQXVCO0FBQ3RCc0QsTUFBRSxPQUFLUyxFQUFQLEVBQVdnQyxHQUFYLENBQWUsQ0FBZjtBQUNBNUQsVUFBTUMsR0FBTixDQUFVRSxTQUFTQyxNQUFULEdBQWtCLGlCQUE1QixFQUE4QztBQUMxQzZDLGFBQVE7QUFDUEMsY0FBT0osS0FEQTtBQUVWakYsV0FBSTtBQUZNO0FBRGtDLEtBQTlDO0FBTUE7QUFDRCxHQXRNTTtBQXdNUHdHLG1CQXhNTyw2QkF3TVdDLEtBeE1YLEVBd01pQjtBQUFBOztBQUN2QixPQUFHQSxNQUFNQyxNQUFOLENBQWFySCxLQUFiLEtBQXVCLEdBQTFCLEVBQThCO0FBQzdCLFNBQUs0QixJQUFMLENBQVVFLE9BQVYsR0FBb0JzRixNQUFNQyxNQUFOLENBQWFySCxLQUFqQztBQUNBLFNBQUs0QixJQUFMLENBQVVHLElBQVYsR0FBaUIsRUFBakI7QUFDQSxTQUFLSCxJQUFMLENBQVVWLElBQVYsR0FBaUIsRUFBakI7QUFDQSxTQUFLVSxJQUFMLENBQVVJLE1BQVYsR0FBbUIsRUFBbkI7QUFDQSxTQUFLSixJQUFMLENBQVVULE9BQVYsR0FBb0IsRUFBcEI7QUFDQSxTQUFLUyxJQUFMLENBQVVLLFFBQVYsR0FBcUIsRUFBckI7QUFDQSxTQUFLTCxJQUFMLENBQVVNLElBQVYsR0FBaUIsRUFBakI7QUFDQSxTQUFLTixJQUFMLENBQVVPLFFBQVYsR0FBcUIsRUFBckI7QUFDQSxTQUFLUCxJQUFMLENBQVVRLFFBQVYsR0FBcUIsRUFBckI7QUFDQSxTQUFLUixJQUFMLENBQVVTLFFBQVYsR0FBcUIsRUFBckI7QUFDQSxTQUFLVCxJQUFMLENBQVVVLEtBQVYsR0FBa0IsRUFBbEI7QUFDQSxTQUFLVixJQUFMLENBQVVXLGdCQUFWLEdBQTZCLEVBQTdCO0FBQ0EsSUFiRCxNQWFPO0FBQ1BPLFVBQU1DLEdBQU4sQ0FBVUUsU0FBU0MsTUFBVCxHQUFrQiwwQkFBbEIsR0FBK0NrRSxNQUFNQyxNQUFOLENBQWFySCxLQUF0RSxFQUNDMEQsSUFERCxDQUNNLGtCQUFVO0FBQ2YsU0FBSXZDLFVBQVV3RCxPQUFPckUsSUFBUCxDQUFZLENBQVosRUFBZWEsT0FBN0I7QUFDQSxTQUFJYSxTQUFTLEVBQWI7QUFDQSxTQUFHb0IsUUFBUUMsSUFBUixDQUFhaUUsT0FBYixDQUFxQjdDLE1BQXJCLEtBQWdDLENBQW5DLEVBQXNDekMsU0FBU29CLFFBQVFDLElBQVIsQ0FBYWlFLE9BQWIsQ0FBcUIsQ0FBckIsRUFBd0J0RixNQUFqQyxDQUF0QyxLQUNLQSxTQUFTLEVBQVQ7QUFDTCxZQUFLSixJQUFMLENBQVVFLE9BQVYsR0FBb0JzRixNQUFNQyxNQUFOLENBQWFySCxLQUFqQztBQUNBLFlBQUs0QixJQUFMLENBQVVHLElBQVYsR0FBaUI0QyxPQUFPckUsSUFBUCxDQUFZLENBQVosRUFBZXlCLElBQWhDO0FBQ0EsWUFBS0gsSUFBTCxDQUFVVixJQUFWLEdBQWlCeUQsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWVzRSxnQkFBaEM7QUFDQSxZQUFLaEQsSUFBTCxDQUFVSSxNQUFWLEdBQW1CMkMsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWV1RSxjQUFsQztBQUNBLFlBQUtqRCxJQUFMLENBQVVULE9BQVYsR0FBb0JBLFFBQVEyRCxPQUFSLENBQWdCLGdCQUFoQixFQUFpQyxFQUFqQyxDQUFwQjtBQUNBLFlBQUtsRCxJQUFMLENBQVVLLFFBQVYsR0FBcUIwQyxPQUFPckUsSUFBUCxDQUFZLENBQVosRUFBZTJCLFFBQXBDO0FBQ0EsWUFBS0wsSUFBTCxDQUFVTSxJQUFWLEdBQWlCeUMsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWU0QixJQUFoQztBQUNBLFlBQUtOLElBQUwsQ0FBVU8sUUFBVixHQUFxQndDLE9BQU9yRSxJQUFQLENBQVksQ0FBWixFQUFlNkIsUUFBcEM7QUFDQSxZQUFLUCxJQUFMLENBQVVRLFFBQVYsR0FBcUJ1QyxPQUFPckUsSUFBUCxDQUFZLENBQVosRUFBZThCLFFBQXBDO0FBQ0EsWUFBS1IsSUFBTCxDQUFVUyxRQUFWLEdBQXFCc0MsT0FBT3JFLElBQVAsQ0FBWSxDQUFaLEVBQWUrQixRQUFwQztBQUNBLFlBQUtULElBQUwsQ0FBVVUsS0FBVixHQUFrQixFQUFsQjtBQUNBLFlBQUtWLElBQUwsQ0FBVVcsZ0JBQVYsR0FBNkJvQyxPQUFPckUsSUFBUCxDQUFZLENBQVosRUFBZXlFLHdCQUE1QztBQUNNLEtBbEJQO0FBbUJDO0FBQ0QsR0EzT007QUE2T1B3QyxPQTdPTyxtQkE2T0E7QUFDTixPQUFJQyxpQkFBaUIsRUFBckI7QUFDQUEsb0JBQWlCLHVLQUFqQjtBQUNBQSxxQkFBa0Isb0dBQWxCO0FBQ0FBLHFCQUFrQixtTkFBbEI7QUFDQUEscUJBQWtCLHFLQUFsQjtBQUNBQSxxQkFBa0IsOGNBQWxCO0FBQ0FBLHFCQUFrQixvS0FBbEI7O0FBRUEsT0FBSUMsT0FBT3hELEVBQUUsUUFBRixFQUFZeUQsSUFBWixFQUFYO0FBQ0FDLGNBQVNDLE9BQU9DLElBQVAsQ0FBWSxFQUFaLEVBQWUsRUFBZixFQUFrQixpREFBbEIsQ0FBVDtBQUNBRixZQUFTRyxRQUFULENBQWtCQyxLQUFsQixDQUF3QlAsY0FBeEI7QUFDQUcsWUFBU0csUUFBVCxDQUFrQkMsS0FBbEIsQ0FBd0JOLElBQXhCO0FBQ0FFLFlBQVNHLFFBQVQsQ0FBa0JDLEtBQWxCLENBQXdCLDRCQUF4QjtBQUNBSixZQUFTRyxRQUFULENBQWtCRSxLQUFsQjs7QUFFQTVELGNBQVcsWUFBVTtBQUNyQnVELGFBQVNNLEtBQVQ7QUFDQU4sYUFBU0osS0FBVDtBQUNDLElBSEQsRUFHRyxJQUhIO0FBSUE7QUFqUU0sRUE3SFM7O0FBaVlqQlcsV0FBUztBQUNSQyxhQURRLHlCQUNLO0FBQ1pDLFNBQU0sQ0FBTjtBQUNBaEgsY0FBVyxDQUFYO0FBQ0FzQixZQUFTLENBQVQ7QUFDQTJGLHVCQUFvQixDQUFwQjtBQUNBQyxTQUFNLENBQU47QUFDQSxRQUFJQyxJQUFFLENBQU4sRUFBU0EsSUFBRSxLQUFLaEksS0FBTCxDQUFXa0UsTUFBdEIsRUFBOEI4RCxHQUE5QixFQUNBO0FBQ0MsUUFBSUMsWUFBWSxLQUFLakksS0FBTCxDQUFXZ0ksQ0FBWCxFQUFjRSxPQUFkLENBQXNCQyxVQUF0QztBQUNBTixXQUFPSSxVQUFVMUQsT0FBVixDQUFrQixHQUFsQixFQUF1QixFQUF2QixJQUE2QixLQUFLdkUsS0FBTCxDQUFXZ0ksQ0FBWCxFQUFjNUgsR0FBbEQ7QUFDQStCLGNBQVUsS0FBS25DLEtBQUwsQ0FBV2dJLENBQVgsRUFBY0UsT0FBZCxDQUFzQi9GLE1BQXRCLEdBQStCLEtBQUtuQyxLQUFMLENBQVdnSSxDQUFYLEVBQWM1SCxHQUF2RDtBQUNBMEgseUJBQXFCLEtBQUs5SCxLQUFMLENBQVdnSSxDQUFYLEVBQWNFLE9BQWQsQ0FBc0JFLFlBQXRCLEdBQXFDLEtBQUtwSSxLQUFMLENBQVdnSSxDQUFYLEVBQWM1SCxHQUFuRCxHQUF5RCxLQUFLSixLQUFMLENBQVdnSSxDQUFYLEVBQWNFLE9BQWQsQ0FBc0IvRixNQUF0QixHQUErQixLQUFLbkMsS0FBTCxDQUFXZ0ksQ0FBWCxFQUFjNUgsR0FBM0g7QUFDQTJILFdBQU8sS0FBSy9ILEtBQUwsQ0FBV2dJLENBQVgsRUFBY0UsT0FBZCxDQUFzQkgsR0FBdEIsR0FBNEIsS0FBSy9ILEtBQUwsQ0FBV2dJLENBQVgsRUFBYzVILEdBQWpEO0FBQ0FTLGdCQUFZLEtBQUtiLEtBQUwsQ0FBV2dJLENBQVgsRUFBY0UsT0FBZCxDQUFzQkUsWUFBdEIsR0FBcUMsS0FBS3BJLEtBQUwsQ0FBV2dJLENBQVgsRUFBYzVILEdBQS9EO0FBQ0E7QUFDRCxRQUFLaUIsSUFBTCxDQUFVWSxRQUFWLEdBQXFCNEYsR0FBckI7QUFDQSxRQUFLeEcsSUFBTCxDQUFVUCxLQUFWLEdBQWtCK0csTUFBTUMsaUJBQU4sR0FBMEJDLEdBQTVDO0FBQ0EsUUFBSzFHLElBQUwsQ0FBVWEsR0FBVixHQUFnQjZGLEdBQWhCO0FBQ0EsUUFBSzFHLElBQUwsQ0FBVVIsUUFBVixHQUFxQkEsUUFBckI7QUFDQSxRQUFLUSxJQUFMLENBQVVjLE1BQVYsR0FBbUJBLE1BQW5COztBQUVBLFVBQU8sS0FBS2QsSUFBTCxDQUFVUCxLQUFqQjtBQUNBO0FBdkJPO0FBallRLENBQVIsQ0FBVjs7QUE0WkE0QyxFQUFFLGFBQUYsRUFBaUIyRSxFQUFqQixDQUFvQixPQUFwQixFQUE2QixZQUFXO0FBQ3ZDM0UsR0FBRSxJQUFGLEVBQVErQyxJQUFSLENBQWEsVUFBYixFQUF5QixJQUF6QjtBQUNBLENBRkQ7O0FBSUEvQyxFQUFFLHVDQUFGLEVBQTJDNEUsUUFBM0MsQ0FBb0QsVUFBVUMsQ0FBVixFQUFhO0FBQ2hFO0FBQ0MsS0FBSUEsRUFBRUMsS0FBRixJQUFXLENBQVgsSUFBZ0JELEVBQUVDLEtBQUYsSUFBVyxDQUEzQixLQUFpQ0QsRUFBRUMsS0FBRixHQUFVLEVBQVYsSUFBZ0JELEVBQUVDLEtBQUYsR0FBVSxFQUEzRCxDQUFKLEVBQW9FO0FBQ2pFLFNBQU8sS0FBUDtBQUNIO0FBQ0QsQ0FMRDs7QUFPQTs7OztBQUlBLFNBQVM5SSxnQkFBVCxDQUEwQitJLENBQTFCLEVBQTZCO0FBQ3pCLFFBQU9BLEVBQUVDLFFBQUYsR0FBYW5FLE9BQWIsQ0FBcUIsdUJBQXJCLEVBQThDLEdBQTlDLENBQVA7QUFDSCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvWEQ7U0FHQTs7O2tDQUVBO21EQUNBO0FBQ0E7MEJBQ0E7b0VBQ0E7c0NBQ0E7QUFDQTtxRUFDQTs4QkFDQTtBQUNBO29EQUNBO29CQUNBO0FBQ0E7c0ZBQ0E7QUFDQTs0Q0FDQTt1REFDQTtBQUNBO3NDQUNBO29FQUNBO0FBRUE7QUF0QkE7O29DQXdCQTtxQ0FDQTtpQ0FDQTtBQUNBO2tDQUNBO3FDQUNBOytDQUNBO2tDQUNBO0FBQ0E7OEdBQ0E7NERBQ0E7QUFDQTtxRUFDQTtrREFDQTtBQUNBO29EQUNBO3NDQUNBO0FBQ0E7QUFDQTtnQ0FDQTt3SUFDQTtBQUNBO2dDQUNBO2dDQUNBO0FBRUE7QUExQkE7QUExQkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlCQTtTQUVBOztvQ0FFQTtxQ0FDQTtpQ0FDQTtBQUVBO0FBTEE7QUFGQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDREE7U0FFQTs7b0NBRUE7cUNBQ0E7aUNBQ0E7QUFFQTtBQUxBO0FBRkEsRzs7Ozs7OztBQzdCQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQStHO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQStHO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQStHO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ2xMQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ25CQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0gsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0lBOzswQkFHQTsyQkFFQTtBQUhBO0FBREEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTUE7OzJCQUdBOzBCQUNBO2tDQUdBOztBQUxBO0FBREEsRzs7Ozs7OztBQy9CQTtBQUNBO0FBQ0EsdUJBQXFKO0FBQ3JKO0FBQ0Esd0JBQTRHO0FBQzVHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEIiwiZmlsZSI6ImpzL3BhZ2VzL2NhcnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQyMyk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL01vZGFsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNWMxMmNlNWVcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vTW9kYWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXE1vZGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIE1vZGFsLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi01YzEyY2U1ZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTVjMTJjZTVlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9Nb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDEwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA2IDI3IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwgZmFkZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IF92bS5pZCxcbiAgICAgIFwidGFiaW5kZXhcIjogXCItMVwiLFxuICAgICAgXCJyb2xlXCI6IFwiZGlhbG9nXCIsXG4gICAgICBcImFyaWEtbGFiZWxsZWRieVwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWRpYWxvZ1wiLFxuICAgIGNsYXNzOiBfdm0uc2l6ZSxcbiAgICBhdHRyczoge1xuICAgICAgXCJyb2xlXCI6IFwiZG9jdW1lbnRcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfdm0uX3QoXCJleHRyYS1sZWZ0XCIpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIGNsYXNzOiAnY29sLW1kLScgKyBfdm0uY29udGVudFNpemUgKyAnIG1vZGFsLXNtLScgKyBfdm0uY29udGVudFNpemVcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtaGVhZGVyXCJcbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2g0Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLXRpdGxlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJtb2RhbC1sYWJlbFwiXG4gICAgfVxuICB9LCBbX3ZtLl90KFwibW9kYWwtdGl0bGVcIiwgW192bS5fdihcIk1vZGFsIFRpdGxlXCIpXSldLCAyKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWJvZHlcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtYm9keVwiLCBbX3ZtLl92KFwiTW9kYWwgQm9keVwiKV0pXSwgMiksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtZm9vdGVyXCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWZvb3RlclwiKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF92bS5fdChcIm1vZGFsLXJpZ2h0XCIpXSwgMildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNsb3NlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiLFxuICAgICAgXCJhcmlhLWxhYmVsXCI6IFwiQ2xvc2VcIlxuICAgIH1cbiAgfSwgW19jKCdzcGFuJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImFyaWEtaGlkZGVuXCI6IFwidHJ1ZVwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiw5dcIildKV0pXG59XX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNWMxMmNlNWVcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi01YzEyY2U1ZVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDYgMjciLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogX3ZtLmlkLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIi0xXCIsXG4gICAgICBcInJvbGVcIjogXCJkaWFsb2dcIixcbiAgICAgIFwiYXJpYS1sYWJlbGxlZGJ5XCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgY2xhc3M6ICdtb2RhbC1kaWFsb2cgJyArIF92bS5zaXplLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInJvbGVcIjogXCJkb2N1bWVudFwiXG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1jb250ZW50XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtaGVhZGVyXCJcbiAgfSwgW19jKCdoNCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC10aXRsZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwibW9kYWwtbGFiZWxcIlxuICAgIH1cbiAgfSwgW192bS5fdChcIm1vZGFsLXRpdGxlXCIsIFtfdm0uX3YoXCJNb2RhbCBUaXRsZVwiKV0pXSwgMildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1ib2R5XCJcbiAgfSwgW192bS5fdChcIm1vZGFsLWJvZHlcIiwgW192bS5fdihcIk1vZGFsIEJvZHlcIildKV0sIDIpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWZvb3RlclwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1mb290ZXJcIiwgW19jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5XCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiXG4gICAgfVxuICB9LCBbX3ZtLl92KFwiQ2xvc2VcIildKV0pXSwgMildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNjUzZTczNjlcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi02NTNlNzM2OVwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTJcbi8vIG1vZHVsZSBjaHVua3MgPSA2IDE0IDE1IiwiVnVlLmNvbXBvbmVudCgnY2FydC1pdGVtJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvY2FydC9DYXJ0SXRlbS52dWUnKSk7XHJcblZ1ZS5jb21wb25lbnQoJ2NhcnQtdGFibGUnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9jYXJ0L0NhcnRMaXN0LnZ1ZScpKTtcclxuVnVlLmNvbXBvbmVudCgnb3JkZXItdGFibGUnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9jYXJ0L09yZGVyTGlzdC52dWUnKSk7XHJcblZ1ZS5jb21wb25lbnQoJ2RpYWxvZy1tb2RhbCcsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZScpKTtcclxuVnVlLmNvbXBvbmVudCgnbW9kYWwnLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9Nb2RhbC52dWUnKSk7XHJcblxyXG5WdWUuZmlsdGVyKCdjdXJyZW5jeScsIGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuICAn4oKxJytudW1iZXJXaXRoQ29tbWFzKHBhcnNlRmxvYXQodmFsdWUpLnRvRml4ZWQoMikpO1xyXG59KTtcclxuXHJcblxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcblx0ZWw6ICcjY2FydF9jb250YWluZXInLFxyXG5cdGRhdGE6IHtcclxuXHRcdGl0ZW1zOltdLFxyXG5cdFx0c2VsZWN0ZWQ6W10sXHJcblx0XHRkdXBfaXRlbXM6W10sXHJcblx0XHRwcm9kX2lkOltdLFxyXG5cdFx0cXR5OltdLFxyXG5cdFx0c2hvd1VuZG86ZmFsc2UsXHJcblx0XHRpdGVtX25hbWVzOicnLFxyXG5cdFx0b3B0aW9uVHlwZTpbXSxcclxuXHRcdHNlbGVjdGVkVHlwZTo1LFxyXG5cdFx0ZXdhbGxldDowLFxyXG5cdFx0b3JkZXJfaWQ6MCxcclxuXHRcdG5hbWU6JycsXHJcblx0XHRhZGRyZXNzOicnLFxyXG5cdFx0c2hpcHBpbmc6MCxcclxuXHRcdHRvdGFsOjAsXHJcblx0XHRvcmRlcnM6W10sXHJcblx0XHRsYW5nOltdLFxyXG5cclxuXHRcdC8vY2hlY2tvdXRcclxuXHRcdGN1cnJlbnRfc3RlcF9uYW1lOidTaGlwcGluZyBBZGRyZXNzJyxcclxuXHRcdGN1cnJlbnRfc3RlcDoxLFxyXG5cdFx0cmVhY2hlZF9zdGVwOjEsXHJcblx0XHRhZGRyZXNzX3NldDpmYWxzZSxcclxuXHQgICAgZm9ybTogbmV3IEZvcm0oe1xyXG5cdCAgICAgICB0eXBlX2lkOicnLFxyXG5cdCAgICAgICB0eXBlOiBudWxsLFxyXG5cdCAgICAgICBuYW1lOiBudWxsLFxyXG5cdCAgICAgICBudW1iZXI6IG51bGwsXHJcblx0ICAgICAgIGFkZHJlc3M6IG51bGwsXHJcblx0ICAgICAgIGJhcmFuZ2F5OiBudWxsLFxyXG5cdCAgICAgICBjaXR5OiBudWxsLFxyXG5cdCAgICAgICBwcm92aW5jZTogbnVsbCxcclxuXHQgICAgICAgemlwX2NvZGU6IG51bGwsXHJcblx0ICAgICAgIGxhbmRtYXJrOiBudWxsLFxyXG5cdCAgICAgICBub3RlczogbnVsbCxcclxuXHQgICAgICAgYWx0ZXJuYXRlX251bWJlcjogbnVsbCxcclxuXHQgICAgICAgc3VidG90YWw6IG51bGwsXHJcblx0ICAgICAgIHRvdGFsOiBudWxsLFxyXG5cdCAgICAgICB0YXg6IG51bGwsXHQgICAgICAgXHJcblx0ICAgICAgIHNoaXBwaW5nOiBudWxsLFxyXG5cdCAgICAgICBjaGFyZ2U6IG51bGxcclxuXHQgICAgfSx7IGJhc2VVUkw6ICcvY2FydCd9KVxyXG5cdH0sXHJcblxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdGZ1bmN0aW9uIGdldFRyYW5zbGF0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2NhcnQnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGdldENhcnQoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L3ZpZXcnKTsgXHJcbiAgICAgICAgfVxyXG5cclxuXHRcdGZ1bmN0aW9uIGdldFR5cGUoKSB7XHJcblx0XHRcdGlmKExhcmF2ZWwudXNlcilcclxuXHRcdFx0cmV0dXJuIGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvZ2V0VHlwZScpO1xyXG5cdFx0fVxyXG5cdFx0ZnVuY3Rpb24gZ2V0QWN0aXZlKCkge1xyXG5cdFx0XHRpZihMYXJhdmVsLnVzZXIpXHJcblx0XHRcdHJldHVybiBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2dldEFjdGl2ZScpO1xyXG5cdFx0fVxyXG5cdFx0ZnVuY3Rpb24gZ2V0VXNhYmxlQmFsYW5jZSgpIHtcclxuXHRcdFx0aWYoTGFyYXZlbC51c2VyKVxyXG5cdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9nZXRVc2FibGUnKTtcclxuXHRcdH1cclxuXHJcblx0XHRheGlvcy5hbGwoW1xyXG5cdFx0XHRnZXRUcmFuc2xhdGlvbigpLFxyXG5cdFx0XHRnZXRDYXJ0KCksXHJcblx0XHRcdGdldFR5cGUoKSxcclxuXHRcdFx0Z2V0QWN0aXZlKCksXHJcblx0XHRcdGdldFVzYWJsZUJhbGFuY2UoKVxyXG5cdFx0XSkudGhlbihheGlvcy5zcHJlYWQoXHJcblx0XHRcdChcclxuXHRcdFx0XHR0cmFuc2xhdGlvbixcclxuXHRcdFx0XHRjYXJ0LFxyXG5cdFx0XHRcdHR5cGUsXHJcblx0XHRcdFx0YWN0aXZlLFxyXG5cdFx0XHRcdGJhbGFuY2VcclxuXHRcdFx0KSA9PiB7XHJcblx0XHRcdFx0dGhpcy5sYW5nID0gdHJhbnNsYXRpb24uZGF0YS5kYXRhO1xyXG5cdFx0XHRcdHZhciBjYXJ0MiA9ICQubWFwKGNhcnQuZGF0YS5jYXJ0LCBmdW5jdGlvbih2YWx1ZSwgaW5kZXgpIHtcclxuXHRcdFx0XHQgICAgcmV0dXJuIFt2YWx1ZV07XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpeyQubWF0ZXJpYWwuaW5pdCgpO30sIDEwMCk7XHJcblx0XHRcdFx0dGhpcy5pdGVtcyA9IGNhcnQyO1xyXG5cclxuXHRcdFx0XHR2YXIgbmV3dHlwZSA9IHtcImlkXCI6IDAsIFwidHlwZVwiOiB0aGlzLmxhbmdbJ25ldy1wcm9maWxlJ10sIFwiaXNfYWN0aXZlXCI6IGZhbHNlfTtcclxuXHRcdFx0XHR2YXIgdHlwZSA9IHR5cGUuZGF0YTtcclxuXHRcdFx0XHR0eXBlLnB1c2gobmV3dHlwZSk7XHJcblx0XHRcdFx0dGhpcy5vcHRpb25UeXBlID0gdHlwZTtcclxuXHJcblx0XHRcdFx0aWYoYWN0aXZlLmRhdGEubGVuZ3RoICE9PSAwKSB7XHJcblxyXG5cdFx0XHRcdFx0dGhpcy5zZWxlY3RlZFR5cGUgPSBhY3RpdmUuZGF0YVswXS5pZDtcclxuXHJcblx0XHRcdFx0XHRheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2dldEFkZHJlc3NEZXRhaWxzLycgKyB0aGlzLnNlbGVjdGVkVHlwZSlcclxuXHRcdFx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0XHRcdHZhciBhZGRyZXNzID0gcmVzdWx0LmRhdGFbMF0uYWRkcmVzcztcclxuXHRcdFx0XHRcdFx0dGhpcy5mb3JtLnR5cGVfaWQgPSB0aGlzLnNlbGVjdGVkVHlwZTtcclxuXHRcdFx0XHRcdFx0dGhpcy5mb3JtLnR5cGUgPSByZXN1bHQuZGF0YVswXS50eXBlO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmZvcm0ubmFtZSA9IHJlc3VsdC5kYXRhWzBdLm5hbWVfb2ZfcmVjZWl2ZXI7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5udW1iZXIgPSByZXN1bHQuZGF0YVswXS5jb250YWN0X251bWJlcjtcclxuXHRcdFx0XHRcdFx0dGhpcy5mb3JtLmFkZHJlc3MgPSBhZGRyZXNzLnJlcGxhY2UoLyhcXHJcXG58XFxufFxccikvZ20sXCJcIik7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5iYXJhbmdheSA9IHJlc3VsdC5kYXRhWzBdLmJhcmFuZ2F5O1xyXG5cdFx0XHRcdFx0XHR0aGlzLmZvcm0uY2l0eSA9IHJlc3VsdC5kYXRhWzBdLmNpdHk7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5wcm92aW5jZSA9IHJlc3VsdC5kYXRhWzBdLnByb3ZpbmNlO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmZvcm0uemlwX2NvZGUgPSByZXN1bHQuZGF0YVswXS56aXBfY29kZTtcclxuXHRcdFx0XHRcdFx0dGhpcy5mb3JtLmxhbmRtYXJrID0gcmVzdWx0LmRhdGFbMF0ubGFuZG1hcms7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5ub3RlcyA9ICcnO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmZvcm0uYWx0ZXJuYXRlX251bWJlciA9IHJlc3VsdC5kYXRhWzBdLmFsdGVybmF0ZV9jb250YWN0X251bWJlcjtcclxuXHRcdFx0ICAgICAgICB9KTtcdFx0XHJcblxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLnNlbGVjdGVkVHlwZSA9IDA7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHR0aGlzLmV3YWxsZXQgPSBiYWxhbmNlLmRhdGEuc3VjY2Vzcz8gcGFyc2VGbG9hdChiYWxhbmNlLmRhdGEuZGF0YS51c2FibGUpOjA7XHJcblx0XHR9KSlcclxuXHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cdH0sXHJcblxyXG5cdG1ldGhvZHM6e1xyXG5cdFx0c2hvd1JlbW92ZURpYWxvZygpe1xyXG5cdFx0XHRpZih0aGlzLnNlbGVjdGVkLmxlbmd0aClcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCQoJyNkaWFsb2dSZW1vdmUnKS5tb2RhbCgndG9nZ2xlJyk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblx0XHRzaG93Q2hlY2tvdXRNb2RhbCgpe1xyXG5cdFx0XHRpZihMYXJhdmVsLnVzZXIpe1xyXG5cdFx0XHRcdGlmKHRoaXMuaXRlbXMgIT0gMClcclxuXHRcdFx0XHQkKCcjbW9kYWxDaGVja291dCcpLm1vZGFsKCd0b2dnbGUnKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdCAgICBcdHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ3lvdS1uZWVkJ10pOyBcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcdC8vY2hlY2tvdXRcclxuXHRcdHNlZVN0ZXBBZGRyZXNzKCl7XHJcblx0XHRcdGlmKHRoaXMuaXRlbXMgIT0gMCl7XHJcblx0XHRcdHRoaXMuY3VycmVudF9zdGVwID0gMTtcclxuXHRcdFx0dGhpcy5jdXJyZW50X3N0ZXBfbmFtZSA9ICdTaGlwcGluZyBBZGRyZXNzJztcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcdG1vdmVUb1dpc2hsaXN0KGlkLHJvd0lkKXtcclxuXHRcdFx0aWYoTGFyYXZlbC5pc0F1dGhlbnRpY2F0ZWQgPT09IHRydWUpe1xyXG5cdFx0XHR2YXIgaW5kZXggPSB0aGlzLml0ZW1zLmZpbmRJbmRleCggKGVsKSA9PiBlbC5pZCA9PT0gaWQgKTtcclxuXHRcdFx0YXhpb3MuZ2V0KGxvY2F0aW9uLm9yaWdpbiArICcvY2FydC9kZWxldGUnLHtcclxuXHRcdFx0ICAgIHBhcmFtczoge1xyXG5cdFx0XHQgICAgXHRyb3dfaWQ6IHJvd0lkXHJcblx0XHRcdCAgICB9XHJcblx0XHRcdH0pXHJcblx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0dGhpcy5pdGVtcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG5cdFx0XHRcdGF4aW9zQVBJdjEuZ2V0KCcvYWRkdG9mYXZvcml0ZXMvJyArIGlkKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuXHQgICAgICAgICAgICAgICAgdmFyIGZhdiA9IHBhcnNlSW50KCQoXCIjZmF2LWNvdW50XCIpLnRleHQoKSkrMTtcclxuXHQgICAgICAgICAgICAgICAgJChcIiNmYXYtY291bnRcIikudGV4dChmYXYpLnNob3coKTtcclxuXHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1snYWRkZWQtZmF2b3JpdGVzJ10pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblx0XHRcdH0pO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ3lvdS1uZWVkJ10pO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFx0cmVtb3ZlSXRlbXMoaWQsbmFtZSxyb3dJZCl7XHJcblx0XHRcdHZhciBpbmRleCA9IHRoaXMuaXRlbXMuZmluZEluZGV4KCAoZWwpID0+IGVsLmlkID09PSBpZCApO1xyXG5cdFx0XHRheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2RlbGV0ZScse1xyXG5cdFx0XHQgICAgcGFyYW1zOiB7XHJcblx0XHRcdCAgICBcdHJvd19pZDogcm93SWRcclxuXHRcdFx0ICAgIH1cclxuXHRcdFx0fSlcclxuXHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHR0aGlzLml0ZW1zLnNwbGljZShpbmRleCwgMSk7XHJcblx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nWydyZW1vdmVkJ10sbmFtZSk7XHJcblx0XHRcdH0pO1xyXG5cdFx0fSxcclxuXHRcdHNhdmVQcm9maWxlKCl7XHJcblx0ICAgICAgIHRoaXMuZm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3NhdmVQcm9maWxlJylcclxuXHQgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRpZihyZXN1bHQucmVzcG9uc2UhPTApe1xyXG5cdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nWydwcm9maWxlLXNhdmVkJ10pO1xyXG5cdFx0XHRcdFx0YXhpb3MuZ2V0KGxvY2F0aW9uLm9yaWdpbiArICcvY2FydC9nZXRUeXBlJylcclxuXHRcdFx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0XHRcdHZhciBuZXd0eXBlID0ge1wiaWRcIjogMCwgXCJ0eXBlXCI6IFwiTmV3IFByb2ZpbGVcIiwgXCJpc19hY3RpdmVcIjogZmFsc2V9O1xyXG5cdFx0XHRcdFx0XHR2YXIgdHlwZSA9IHJlc3VsdC5kYXRhO1xyXG5cdFx0XHRcdFx0XHR0eXBlLnB1c2gobmV3dHlwZSk7XHJcblx0XHRcdFx0XHRcdHRoaXMub3B0aW9uVHlwZSA9IHR5cGU7XHJcblx0XHRcdFx0XHRcdGlmKCQoXCIjYWRkcmVzc19wcm9maWxlXCIpLnZhbCgpPT0wKXtcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmZvcm0udHlwZSA9ICcnO1xyXG5cdFx0XHRcdFx0XHRcdHRoaXMuZm9ybS5uYW1lID0gJyc7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5mb3JtLm51bWJlciA9ICcnO1xyXG5cdFx0XHRcdFx0XHRcdHRoaXMuZm9ybS5hZGRyZXNzID0gJyc7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5mb3JtLmJhcmFuZ2F5ID0gJyc7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5mb3JtLmNpdHkgPSAnJztcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmZvcm0ucHJvdmluY2UgPSAnJztcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmZvcm0uemlwX2NvZGUgPSAnJztcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmZvcm0ubGFuZG1hcmsgPSAnJztcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmZvcm0ubm90ZXMgPSAnJztcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmZvcm0uYWx0ZXJuYXRlX251bWJlciA9ICcnO1x0XHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0ICAgICAgICB9KTtcdFx0XHRcdFx0XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGVsc2V7XHJcblx0ICAgICAgICAgICBcdFx0dG9hc3RyLmVycm9yKHJlc3VsdC5tZXNzYWdlKTtcclxuXHQgICAgICAgICAgIFx0fVxyXG5cdCAgICAgICAgICB9KVxyXG5cdCAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG5cdCAgICAgICAgICAgXHR0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydwbGVhc2UnXSk7XHJcblx0ICAgICAgICAgIH0pO1xyXG5cdFx0fSxcclxuXHRcdFxyXG5cdFx0ZGVsZXRlUHJvZmlsZSgpe1xyXG5cdCAgICAgICB0aGlzLmZvcm0uc3VibWl0KCdwb3N0JywgJy9kZWxldGVQcm9maWxlJylcclxuXHQgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRpZihyZXN1bHQucmVzcG9uc2UhPTApe1xyXG5cdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nWydwcm9maWxlLWRlbGV0ZWQnXSk7XHJcblx0XHRcdFx0XHRheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2dldFR5cGUnKVxyXG5cdFx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRcdFx0dmFyIG5ld3R5cGUgPSB7XCJpZFwiOiAwLCBcInR5cGVcIjogXCJOZXcgUHJvZmlsZVwiLCBcImlzX2FjdGl2ZVwiOiB0cnVlfTtcclxuXHRcdFx0XHRcdFx0dmFyIHR5cGUgPSByZXN1bHQuZGF0YTtcclxuXHRcdFx0XHRcdFx0dHlwZS5wdXNoKG5ld3R5cGUpO1xyXG5cdFx0XHRcdFx0XHR0aGlzLm9wdGlvblR5cGUgPSB0eXBlO1xyXG5cdFx0XHRcdFx0XHR0aGlzLnNlbGVjdGVkVHlwZSA9IDA7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS50eXBlID0gJyc7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5uYW1lID0gJyc7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5udW1iZXIgPSAnJztcclxuXHRcdFx0XHRcdFx0dGhpcy5mb3JtLmFkZHJlc3MgPSAnJztcclxuXHRcdFx0XHRcdFx0dGhpcy5mb3JtLmJhcmFuZ2F5ID0gJyc7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5jaXR5ID0gJyc7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5wcm92aW5jZSA9ICcnO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmZvcm0uemlwX2NvZGUgPSAnJztcclxuXHRcdFx0XHRcdFx0dGhpcy5mb3JtLmxhbmRtYXJrID0gJyc7XHJcblx0XHRcdFx0XHRcdHRoaXMuZm9ybS5ub3RlcyA9ICcnO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmZvcm0uYWx0ZXJuYXRlX251bWJlciA9ICcnO1xyXG5cdFx0XHQgICAgICAgIH0pO1x0XHRcdFx0XHRcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0ZWxzZXtcclxuXHQgICAgICAgICAgIFx0XHR0b2FzdHIuZXJyb3IocmVzdWx0Lm1lc3NhZ2UpO1xyXG5cdCAgICAgICAgICAgXHR9XHJcblx0ICAgICAgICAgIH0pXHJcblx0ICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcblx0ICAgICAgICAgICBcdHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ2N1cnJlbnRseSddLHRoaXMubGFuZ1snY2FudC1kZWxldGUnXSk7XHRcclxuXHQgICAgICAgICAgfSk7XHJcblx0XHR9LFxyXG5cdFx0cGxhY2VPcmRlcigpeyAgXHJcblx0XHQgICBpZih0aGlzLml0ZW1zICE9IDApe1xyXG5cdCAgICAgICB0aGlzLmZvcm0uc3VibWl0KCdwb3N0JywgJy9jaGVja291dCcpXHJcblx0ICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0aWYocmVzdWx0LnJlc3BvbnNlIT0wKXtcclxuXHRcdFx0XHRcdHRoaXMuZXdhbGxldCA9IHJlc3VsdC5ld2FsbGV0X2JhbDtcclxuXHRcdFx0XHRcdHRoaXMub3JkZXJfaWQgPSByZXN1bHQuaWQ7XHJcblx0XHRcdFx0XHR0aGlzLm5hbWUgPSByZXN1bHQubmFtZTtcclxuXHRcdFx0XHRcdHRoaXMuYWRkcmVzcyA9IHJlc3VsdC5hZGRyZXNzO1xyXG5cdFx0XHRcdFx0dGhpcy5zaGlwcGluZyA9IHRoaXMuZm9ybS5zaGlwcGluZztcclxuXHRcdFx0XHRcdHRoaXMudG90YWwgPSByZXN1bHQudG90YWw7XHJcblx0XHRcdFx0XHR0aGlzLm9yZGVycyA9IHRoaXMuaXRlbXM7XHJcblx0ICAgICAgICAgICAgXHR0aGlzLmN1cnJlbnRfc3RlcCA9IDI7XHJcblx0XHRcdFx0XHR0aGlzLmN1cnJlbnRfc3RlcF9uYW1lID0gJ09yZGVyIFN1bW1hcnknO1x0XHJcblx0XHRcdFx0XHR0aGlzLml0ZW1zID0gW107XHJcblx0XHRcdFx0XHR0aGlzLmR1cF9pdGVtcyA9IFtdO1xyXG5cdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nWydvcmRlci1wbGFjZWQnXSwgdGhpcy5sYW5nWydvcmRlci1ubyddICsgdGhpcy5vcmRlcl9pZCk7XHJcblx0XHRcdFx0XHQkKFwiI3VyZlwiKS5oaWRlKCk7XHRcdFx0XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGVsc2V7XHJcblx0XHRcdFx0XHQkKFwiI3BsYWNlT3JkZXJcIikucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcblx0ICAgICAgICAgICBcdFx0dG9hc3RyLmVycm9yKHJlc3VsdC5tZXNzYWdlKTtcclxuXHQgICAgICAgICAgIFx0fVxyXG5cdCAgICAgICAgICB9KVxyXG5cdCAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG5cdFx0XHRcdHRoaXMuY3VycmVudF9zdGVwID0gMTtcclxuXHRcdFx0XHR0aGlzLmN1cnJlbnRfc3RlcF9uYW1lID0gJ1NoaXBwaW5nIEFkZHJlc3MnO1xyXG5cdFx0XHRcdCQoXCIjcGxhY2VPcmRlclwiKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcclxuXHQgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHRoaXMubGFuZ1sncGxlYXNlJ10pO1x0XHJcblx0ICAgICAgICAgIH0pO1xyXG5cdCAgICAgICB9XHJcblx0XHR9LFxyXG5cdFx0dXBkYXRlUXR5KHF0eSxyb3dJZCl7XHJcblx0XHRcdGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvdXBkYXRlUXR5Jyx7XHJcblx0XHRcdCAgICBwYXJhbXM6IHtcclxuXHRcdFx0ICAgIFx0cm93X2lkOnJvd0lkLFxyXG5cdFx0XHRcdFx0cXR5OnF0eVxyXG5cdFx0XHQgICAgfVxyXG5cdFx0XHR9KTtcclxuXHRcdH0sXHJcblx0XHRzdG9jayhpZCxxdHksc3RvY2sscm93SWQpe1xyXG5cdFx0XHRpZihwYXJzZUludChzdG9jayk9PTApe1xyXG5cdFx0XHRcdGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvdXBkYXRlUXR5Jyx7XHJcblx0XHRcdFx0ICAgIHBhcmFtczoge1xyXG5cdFx0XHRcdCAgICBcdHJvd19pZDpyb3dJZCxcclxuXHRcdFx0XHRcdFx0cXR5OjFcclxuXHRcdFx0XHQgICAgfVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmKHBhcnNlSW50KHF0eSk+cGFyc2VJbnQoc3RvY2spICYmIHBhcnNlSW50KHN0b2NrKSE9MCl7XHJcblx0XHRcdFx0JCgnI3EnK2lkKS52YWwoc3RvY2spO1xyXG5cdFx0XHRcdGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvdXBkYXRlUXR5Jyx7XHJcblx0XHRcdFx0ICAgIHBhcmFtczoge1xyXG5cdFx0XHRcdCAgICBcdHJvd19pZDpyb3dJZCxcclxuXHRcdFx0XHRcdFx0cXR5OnN0b2NrXHJcblx0XHRcdFx0ICAgIH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZihwYXJzZUludChxdHkpPD1wYXJzZUludChzdG9jaykpe1xyXG5cdFx0XHRcdGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvdXBkYXRlUXR5Jyx7XHJcblx0XHRcdFx0ICAgIHBhcmFtczoge1xyXG5cdFx0XHRcdCAgICBcdHJvd19pZDpyb3dJZCxcclxuXHRcdFx0XHRcdFx0cXR5OnF0eVxyXG5cdFx0XHRcdCAgICB9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYocXR5PT0nJyB8fCBxdHk9PScwJyl7XHJcblx0XHRcdFx0JCgnI3EnK2lkKS52YWwoMSk7XHJcblx0XHRcdFx0YXhpb3MuZ2V0KGxvY2F0aW9uLm9yaWdpbiArICcvY2FydC91cGRhdGVRdHknLHtcclxuXHRcdFx0XHQgICAgcGFyYW1zOiB7XHJcblx0XHRcdFx0ICAgIFx0cm93X2lkOnJvd0lkLFxyXG5cdFx0XHRcdFx0XHRxdHk6MVxyXG5cdFx0XHRcdCAgICB9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdH0sXHJcblxyXG5cdFx0Z2V0QWRkcmVzc0RldGFpbHMoZXZlbnQpe1xyXG5cdFx0XHRpZihldmVudC50YXJnZXQudmFsdWUgPT09ICcwJyl7XHJcblx0XHRcdFx0dGhpcy5mb3JtLnR5cGVfaWQgPSBldmVudC50YXJnZXQudmFsdWU7XHJcblx0XHRcdFx0dGhpcy5mb3JtLnR5cGUgPSAnJztcclxuXHRcdFx0XHR0aGlzLmZvcm0ubmFtZSA9ICcnO1xyXG5cdFx0XHRcdHRoaXMuZm9ybS5udW1iZXIgPSAnJztcclxuXHRcdFx0XHR0aGlzLmZvcm0uYWRkcmVzcyA9ICcnO1xyXG5cdFx0XHRcdHRoaXMuZm9ybS5iYXJhbmdheSA9ICcnO1xyXG5cdFx0XHRcdHRoaXMuZm9ybS5jaXR5ID0gJyc7XHJcblx0XHRcdFx0dGhpcy5mb3JtLnByb3ZpbmNlID0gJyc7XHJcblx0XHRcdFx0dGhpcy5mb3JtLnppcF9jb2RlID0gJyc7XHJcblx0XHRcdFx0dGhpcy5mb3JtLmxhbmRtYXJrID0gJyc7XHJcblx0XHRcdFx0dGhpcy5mb3JtLm5vdGVzID0gJyc7XHJcblx0XHRcdFx0dGhpcy5mb3JtLmFsdGVybmF0ZV9udW1iZXIgPSAnJztcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0YXhpb3MuZ2V0KGxvY2F0aW9uLm9yaWdpbiArICcvY2FydC9nZXRBZGRyZXNzRGV0YWlscy8nICsgZXZlbnQudGFyZ2V0LnZhbHVlKVxyXG5cdFx0XHQudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdHZhciBhZGRyZXNzID0gcmVzdWx0LmRhdGFbMF0uYWRkcmVzcztcclxuXHRcdFx0XHR2YXIgbnVtYmVyID0gJyc7XHJcblx0XHRcdFx0aWYoTGFyYXZlbC51c2VyLm51bWJlcnMubGVuZ3RoICE9PSAwKSBudW1iZXIgPSBMYXJhdmVsLnVzZXIubnVtYmVyc1swXS5udW1iZXIgXHJcblx0XHRcdFx0ZWxzZSBudW1iZXIgPSAnJztcclxuXHRcdFx0XHR0aGlzLmZvcm0udHlwZV9pZCA9IGV2ZW50LnRhcmdldC52YWx1ZTtcclxuXHRcdFx0XHR0aGlzLmZvcm0udHlwZSA9IHJlc3VsdC5kYXRhWzBdLnR5cGU7XHJcblx0XHRcdFx0dGhpcy5mb3JtLm5hbWUgPSByZXN1bHQuZGF0YVswXS5uYW1lX29mX3JlY2VpdmVyO1xyXG5cdFx0XHRcdHRoaXMuZm9ybS5udW1iZXIgPSByZXN1bHQuZGF0YVswXS5jb250YWN0X251bWJlcjtcclxuXHRcdFx0XHR0aGlzLmZvcm0uYWRkcmVzcyA9IGFkZHJlc3MucmVwbGFjZSgvKFxcclxcbnxcXG58XFxyKS9nbSxcIlwiKTtcclxuXHRcdFx0XHR0aGlzLmZvcm0uYmFyYW5nYXkgPSByZXN1bHQuZGF0YVswXS5iYXJhbmdheTtcclxuXHRcdFx0XHR0aGlzLmZvcm0uY2l0eSA9IHJlc3VsdC5kYXRhWzBdLmNpdHk7XHJcblx0XHRcdFx0dGhpcy5mb3JtLnByb3ZpbmNlID0gcmVzdWx0LmRhdGFbMF0ucHJvdmluY2U7XHJcblx0XHRcdFx0dGhpcy5mb3JtLnppcF9jb2RlID0gcmVzdWx0LmRhdGFbMF0uemlwX2NvZGU7XHJcblx0XHRcdFx0dGhpcy5mb3JtLmxhbmRtYXJrID0gcmVzdWx0LmRhdGFbMF0ubGFuZG1hcms7XHJcblx0XHRcdFx0dGhpcy5mb3JtLm5vdGVzID0gJyc7XHJcblx0XHRcdFx0dGhpcy5mb3JtLmFsdGVybmF0ZV9udW1iZXIgPSByZXN1bHQuZGF0YVswXS5hbHRlcm5hdGVfY29udGFjdF9udW1iZXI7XHJcblx0ICAgICAgICB9KTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHRwcmludCgpe1xyXG5cdFx0XHR2YXIgaHRtbF9jb250YWluZXIgPSAnJztcclxuXHRcdFx0aHRtbF9jb250YWluZXIgPSAnPCFET0NUWVBFIGh0bWwgUFVCTElDIFwiLS8vVzNDLy9EVEQgWEhUTUwgMS4wIFRyYW5zaXRpb25hbC8vRU5cIiBcImh0dHA6Ly93d3cudzMub3JnL1RSL3hodG1sMS9EVEQveGh0bWwxLXRyYW5zaXRpb25hbC5kdGRcIj48aHRtbCB4bWxuczpmYj1cImh0dHA6Ly9vZ3AubWUvbnMvZmIjXCI+PGhlYWQ+JztcclxuXHRcdFx0aHRtbF9jb250YWluZXIgKz0gJzxsaW5rIHJlbD1cInN0eWxlc2hlZXRcIiB0eXBlPVwidGV4dC9jc3NcIiBocmVmPVwiLy9mb250cy5nb29nbGVhcGlzLmNvbS9pY29uP2ZhbWlseT1NYXRlcmlhbCtJY29uc1wiIC8+JztcclxuXHRcdFx0aHRtbF9jb250YWluZXIgKz0gJzxsaW5rIHJlbD1cInN0eWxlc2hlZXRcIiBocmVmPVwiaHR0cHM6Ly9tYXhjZG4uYm9vdHN0cmFwY2RuLmNvbS9ib290c3RyYXAvMy4zLjcvY3NzL2Jvb3RzdHJhcC5taW4uY3NzXCIgaW50ZWdyaXR5PVwic2hhMzg0LUJWWWlpU0lGZUsxZEdtSlJBa3ljdUhBSFJnMzJPbVVjd3c3b24zUllkZzRWYStQbVNUc3ovSzY4dmJkRWpoNHVcIiBjcm9zc29yaWdpbj1cImFub255bW91c1wiLz4nO1x0XHJcblx0XHRcdGh0bWxfY29udGFpbmVyICs9ICc8bWV0YSBuYW1lPVwidmlld3BvcnRcIiBjb250ZW50PVwid2lkdGg9ZGV2aWNlLXdpZHRoLCBpbml0aWFsLXNjYWxlPTEuMCwgbWF4aW11bS1zY2FsZT0xLjAsIHVzZXItc2NhbGFibGU9bm9cIj48bWV0YSBuYW1lPVwiYXBwbGUtbW9iaWxlLXdlYi1hcHAtY2FwYWJsZVwiIGNvbnRlbnQ9XCJ5ZXNcIj4nO1xyXG5cdFx0XHRodG1sX2NvbnRhaW5lciArPSAnPHN0eWxlPiNjYXJ0X2NvbnRhaW5lciAuY2hlY2tvdXQtc3RlcHMtY29udGFpbmVyICN0b3RhbCB0ZHt0ZXh0LWFsaWduOnJpZ2h0O3BhZGRpbmctbGVmdDozNXB4fSNjYXJ0X2NvbnRhaW5lciAuY2hlY2tvdXQtc3RlcHMtY29udGFpbmVyIC5lLXdhbGxldHt0ZXh0LWFsaWduOmNlbnRlcjtiYWNrZ3JvdW5kLWNvbG9yOiNlNmU2ZTY7d2lkdGg6MTUwcHg7Ym9yZGVyOjJweCBzb2xpZCAjMDA5Njg4O2hlaWdodDo4NXB4fSNjYXJ0X2NvbnRhaW5lciAuY2hlY2tvdXQtc3RlcHMtY29udGFpbmVyIC5lLXdhbGxldCAudGl0bGVze21hcmdpbi10b3A6MTVweH0jY2FydF9jb250YWluZXIgLmNoZWNrb3V0LXN0ZXBzLWNvbnRhaW5lciAuZS13YWxsZXQgLmJhbGFuY2V7Zm9udC13ZWlnaHQ6NzAwfSNjYXJ0X2NvbnRhaW5lciAuY2hlY2tvdXQtc3RlcHMtY29udGFpbmVyIC5mbG9hdHtmbG9hdDpyaWdodH08L3N0eWxlPic7XHJcblx0XHRcdGh0bWxfY29udGFpbmVyICs9ICc8L2hlYWQ+PGJvZHk+PGRpdiBpZD1cImNhcnRfY29udGFpbmVyXCIgY2xhc3M9XCJjb250YWluZXJcIj48ZGl2IGNsYXNzPVwiY2hlY2tvdXQtc3RlcHMtY29udGFpbmVyIGNoZWNrb3V0LXN0ZXAtc3VtbWFyeVwiPjxpbWcgc3JjPVwiL3Nob3BwaW5nL2ltZy9nZW5lcmFsL25ldy1sb2dvLnBuZ1wiPic7XHJcblxyXG5cdFx0XHR2YXIgYm9keSA9ICQoXCIjcHJpbnRcIikuaHRtbCgpO1xyXG5cdFx0XHRteVdpbmRvdz13aW5kb3cub3BlbignJywnJywnd2lkdGg9NTAwLGhlaWdodD01MDAsc2Nyb2xsYmFycz15ZXMsbG9jYXRpb249bm8nKTtcclxuXHRcdFx0bXlXaW5kb3cuZG9jdW1lbnQud3JpdGUoaHRtbF9jb250YWluZXIpO1xyXG5cdFx0XHRteVdpbmRvdy5kb2N1bWVudC53cml0ZShib2R5KTtcclxuXHRcdFx0bXlXaW5kb3cuZG9jdW1lbnQud3JpdGUoXCI8L2Rpdj48L2Rpdj48L2JvZHk+PC9odG1sPlwiKTtcclxuXHRcdFx0bXlXaW5kb3cuZG9jdW1lbnQuY2xvc2UoKTtcclxuXHRcdFx0XHJcblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuXHRcdFx0bXlXaW5kb3cuZm9jdXMoKTtcclxuXHRcdFx0bXlXaW5kb3cucHJpbnQoKTtcdFx0XHJcblx0XHRcdH0sIDMwMDApO1xyXG5cdFx0fVxyXG5cdH0sXHJcblxyXG5cdGNvbXB1dGVkOntcclxuXHRcdHRvdGFsX3ByaWNlKCl7XHJcblx0XHRcdHN1bSA9IDA7XHJcblx0XHRcdHNoaXBwaW5nID0gMDtcclxuXHRcdFx0Y2hhcmdlID0gMDtcclxuXHRcdFx0c2hpcHBpbmdBbmRDaGFyZ2UgPSAwO1xyXG5cdFx0XHR2YXQgPSAwO1xyXG5cdFx0XHRmb3IoaT0wOyBpPHRoaXMuaXRlbXMubGVuZ3RoOyBpKyspXHJcblx0XHRcdHtcclxuXHRcdFx0XHR2YXIgc2FsZVByaWNlID0gdGhpcy5pdGVtc1tpXS5vcHRpb25zLnNhbGVfcHJpY2U7XHJcblx0XHRcdFx0c3VtICs9IHNhbGVQcmljZS5yZXBsYWNlKCcsJywgJycpICogdGhpcy5pdGVtc1tpXS5xdHk7XHJcblx0XHRcdFx0Y2hhcmdlICs9IHRoaXMuaXRlbXNbaV0ub3B0aW9ucy5jaGFyZ2UgKiB0aGlzLml0ZW1zW2ldLnF0eTtcclxuXHRcdFx0XHRzaGlwcGluZ0FuZENoYXJnZSArPSB0aGlzLml0ZW1zW2ldLm9wdGlvbnMuc2hpcHBpbmdfZmVlICogdGhpcy5pdGVtc1tpXS5xdHkgKyB0aGlzLml0ZW1zW2ldLm9wdGlvbnMuY2hhcmdlICogdGhpcy5pdGVtc1tpXS5xdHk7XHJcblx0XHRcdFx0dmF0ICs9IHRoaXMuaXRlbXNbaV0ub3B0aW9ucy52YXQgKiB0aGlzLml0ZW1zW2ldLnF0eTsgXHJcblx0XHRcdFx0c2hpcHBpbmcgKz0gdGhpcy5pdGVtc1tpXS5vcHRpb25zLnNoaXBwaW5nX2ZlZSAqIHRoaXMuaXRlbXNbaV0ucXR5O1xyXG5cdFx0XHR9XHJcblx0XHRcdHRoaXMuZm9ybS5zdWJ0b3RhbCA9IHN1bTtcclxuXHRcdFx0dGhpcy5mb3JtLnRvdGFsID0gc3VtICsgc2hpcHBpbmdBbmRDaGFyZ2UgKyB2YXQ7XHJcblx0XHRcdHRoaXMuZm9ybS50YXggPSB2YXQ7XHJcblx0XHRcdHRoaXMuZm9ybS5zaGlwcGluZyA9IHNoaXBwaW5nO1xyXG5cdFx0XHR0aGlzLmZvcm0uY2hhcmdlID0gY2hhcmdlO1xyXG5cclxuXHRcdFx0cmV0dXJuIHRoaXMuZm9ybS50b3RhbDtcclxuXHRcdH1cclxuXHR9XHJcbn0pO1xyXG5cclxuJCgnI3BsYWNlT3JkZXInKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuXHQkKHRoaXMpLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XHJcbn0pO1xyXG5cclxuJChcIiNudW1iZXIsICN6aXBfY29kZSwgI2FsdGVybmF0ZV9udW1iZXJcIikua2V5cHJlc3MoZnVuY3Rpb24gKGUpIHtcclxuIC8vaWYgdGhlIGxldHRlciBpcyBub3QgZGlnaXQgdGhlbiBkaXNwbGF5IGVycm9yIGFuZCBkb24ndCB0eXBlIGFueXRoaW5nXHJcbiBcdGlmIChlLndoaWNoICE9IDggJiYgZS53aGljaCAhPSAwICYmIChlLndoaWNoIDwgNDggfHwgZS53aGljaCA+IDU3KSkge1xyXG4gICAgXHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG59KTtcclxuXHJcbi8qJCgnI251bWJlciwgI2FsdGVybmF0ZV9udW1iZXInKS5pbnB1dG1hc2soe1xyXG5tYXNrOiAnKDk5OTkpIDk5OS05OTk5J1xyXG59KSovXHJcblxyXG5mdW5jdGlvbiBudW1iZXJXaXRoQ29tbWFzKHgpIHtcclxuICAgIHJldHVybiB4LnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIsXCIpO1xyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9jYXJ0LmpzIiwiPHRlbXBsYXRlPlxyXG5cclxuXHQ8ZGl2IGNsYXNzPSdjb2wtbGctMTIgY29sLW1kLTYgY29sLXNtLTYnPlxyXG5cdFx0PGEgY2xhc3M9XCJyZW1vdmVcIiBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgQGNsaWNrPVwicmVtb3ZlSXRlbXMoKVwiIDp0aXRsZT1cInRoaXMuJHBhcmVudC5sYW5nWydyZW1vdmUnXVwiXCI+PGkgY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLXJlbW92ZVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L2E+XHJcblx0XHQ8ZGl2IGNsYXNzPSdjYXJkIHZjZW50ZXItbGcnIFxyXG5cdFx0XHR2LWJpbmQ6ZGF0YS1pZD1cIml0ZW0uaWRcIiBcclxuXHRcdFx0di1iaW5kOmNsYXNzPVwieyAnaXRlbS1zZWxlY3RlZCc6IHRoaXMuJHBhcmVudC5zZWxlY3RlZC5pbmRleE9mKGl0ZW0uaWQpPi0xIH1cIiBcclxuXHRcdD5cclxuXHRcdFx0PGRpdiBjbGFzcz0nY29sLWxnLTUnPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3M9J2NvbC1sZy01IGNvbC1tZC0xMiBjb2wtc20tMTInPlxyXG5cdFx0XHRcdFx0PGEgOmhyZWY9XCInL3Byb2R1Y3QvJyArIGl0ZW0ub3B0aW9ucy5zbHVnXCI+PGltZyB2LWJpbmQ6c3JjPVwiaXRlbS5vcHRpb25zLmltYWdlXCIgY2xhc3M9J2ltZyBpbWctcmVzcG9uc2l2ZSc+PC9hPlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdDxkaXYgY2xhc3M9J2NvbC1sZy03IGNvbC1tZC0xMiBjb2wtc20tMTInPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz0ncm93IHRleHQtc20tY2VudGVyJz5cclxuXHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9J2NhcnQtaXRlbS1uYW1lJz57e2l0ZW0ubmFtZX19PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHQ8YSA6aHJlZj1cIicvc3RvcmVzLycgKyBpdGVtLm9wdGlvbnMuc3RvcmVcIiBjbGFzcz0nYnRuIGJ0bi1zbSBidG4tbGluayc+PHNwYW4gY2xhc3M9J2NhcnQtaXRlbS1zdG9yZSc+e3tpdGVtLm9wdGlvbnMuc3RvcmV9fTwvc3Bhbj48L2E+XHJcblx0XHRcdFx0XHRcdDxkaXYgdi1zaG93PVwiaXRlbS5vcHRpb25zLmNvbG9yICE9ICcnIFwiPnt7dGhpcy4kcGFyZW50LmxhbmdbJ2NvbG9yJ119fToge3tpdGVtLm9wdGlvbnMuY29sb3J9fTwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IHYtc2hvdz1cIml0ZW0ub3B0aW9ucy5zaXplICE9ICcnIFwiPnt7dGhpcy4kcGFyZW50LmxhbmdbJ3NpemUnXX19OiB7e2l0ZW0ub3B0aW9ucy5zaXplfX08L2Rpdj5cclxuXHRcdFx0XHRcdFx0PGRpdj48YSBpZD1cIndpc2hsaXN0XCIgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIEBjbGljaz1cIm1vdmVUb1dpc2hsaXN0KClcIj48c3BhbiBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24taGVhcnRcIj48L3NwYW4+IHt7dGhpcy4kcGFyZW50LmxhbmdbJ21vdmUtdG8td2lzaGxpc3QnXX19PC9hPjwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPSdjb2wtbGctNyB0ZXh0LXJpZ2h0Jz5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzPSdjb2wtbGctMTInPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz0nY29sLWxnLTIgY29sLW1kLTEyICBmb3JtLWdyb3VwJz5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz0nY2FydC1oZWFkZXIgaGlkZGVuLWxnIGNvbC1tZC02IGNvbC1zbS02Jz57e3RoaXMuJHBhcmVudC5sYW5nWydxdWFudGl0eSddfX08L2Rpdj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz0nY29sLWxnLTEyIGNvbC1tZC02IGNvbC1zbS02Jz5cclxuXHRcdFx0XHRcdFx0XHQ8aW5wdXQgdi1pZj1cIml0ZW0ub3B0aW9ucy5zdG9jayAhPSAnMCdcIiA6aWQ9XCIncScraXRlbS5pZFwiIHR5cGU9J251bWJlcicgOm1heD1cIml0ZW0ub3B0aW9ucy5zdG9ja1wiIG1pbj0nMScgY2xhc3M9J2Zvcm0tY29udHJvbCB0ZXh0LXJpZ2h0JyB2LW1vZGVsPVwiaXRlbS5xdHlcIiBAY2xpY2s9XCJ1cGRhdGVxdHkoKVwiIEBrZXl1cD1cInN0b2NrKClcIiBzdHlsZT1cIm1hcmdpbi1sZWZ0OiAtMTRweDtcIj5cclxuXHRcdFx0XHRcdFx0XHQ8aW5wdXQgdi1pZj1cIml0ZW0ub3B0aW9ucy5zdG9jayA9PSAnMCdcIiA6aWQ9XCIncScraXRlbS5pZFwiIHR5cGU9J251bWJlcicgbWluPScxJyBjbGFzcz0nZm9ybS1jb250cm9sIHRleHQtcmlnaHQnIHYtbW9kZWw9XCJpdGVtLnF0eVwiIEBjbGljaz1cInVwZGF0ZXF0eSgpXCIgQGtleXVwPVwic3RvY2soKVwiIHN0eWxlPVwibWFyZ2luLWxlZnQ6IC0xNHB4O1wiPlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz0nY29sLWxnLTMgY29sLW1kLTEyIGZvcm0tZ3JvdXAnPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPSdjYXJ0LWhlYWRlciBoaWRkZW4tbGcgY29sLW1kLTYgY29sLXNtLTYnPnt7dGhpcy4kcGFyZW50LmxhbmdbJ2l0ZW0tcHJpY2UnXX19PC9kaXY+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9J2NvbC1sZy0xMiBjb2wtbWQtNiBjb2wtc20tNiBjYXJ0LWl0ZW0tcHJpY2UnPlxyXG5cdFx0XHRcdFx0XHRcdHt7c2FsZV9wcmljZSB8IGN1cnJlbmN5fX1cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9J2NvbC1sZy0zIGNvbC1tZC0xMiBmb3JtLWdyb3VwJz5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzcz0nY2FydC1oZWFkZXIgaGlkZGVuLWxnIGNvbC1tZC02IGNvbC1zbS02Jz57e3RoaXMuJHBhcmVudC5sYW5nWydzaGlwcGluZy1mZWUnXX19PC9kaXY+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9J2NvbC1sZy0xMiBjb2wtbWQtNiBjb2wtc20tNiBjYXJ0LWl0ZW0tcHJpY2Ugc2hpcHBpbmcnPlxyXG5cdFx0XHRcdFx0XHRcdHt7c2hpcHBpbmcgfCBjdXJyZW5jeX19XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPSdjb2wtbGctNCBjb2wtbWQtMTIgZm9ybS1ncm91cCc+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9J2NhcnQtaGVhZGVyIGhpZGRlbi1sZyBjb2wtbWQtNiBjb2wtc20tNic+e3t0aGlzLiRwYXJlbnQubGFuZ1snc3VidG90YWwnXX19PC9kaXY+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9J2NvbC1sZy0xMiBjb2wtbWQtNiBjb2wtc20tNiBjYXJ0LWl0ZW0tcHJpY2UnPlxyXG5cdFx0XHRcdFx0XHRcdHt7c3ViVG90YWwgfCBjdXJyZW5jeX19XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0PC9kaXY+XHJcblx0PC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cdHByb3BzOiBbJ2l0ZW0nXSxcclxuXHRcclxuXHRtZXRob2RzOntcclxuXHRcdHVwZGF0ZXF0eSgpe1xyXG5cdFx0XHQgdGhpcy4kcGFyZW50LnVwZGF0ZVF0eSh0aGlzLml0ZW0ucXR5LHRoaXMuaXRlbS5yb3dJZCk7XHJcblx0XHR9LFxyXG5cdFx0c3RvY2soKXtcclxuXHRcdFx0aWYocGFyc2VJbnQodGhpcy5pdGVtLnF0eSk+cGFyc2VJbnQodGhpcy5pdGVtLm9wdGlvbnMuc3RvY2spKXtcclxuXHRcdFx0XHR0aGlzLml0ZW0ucXR5ID0gdGhpcy5pdGVtLm9wdGlvbnMuc3RvY2s7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYocGFyc2VJbnQodGhpcy5pdGVtLnF0eSk8PXBhcnNlSW50KHRoaXMuaXRlbS5vcHRpb25zLnN0b2NrKSl7XHJcblx0XHRcdFx0dGhpcy5pdGVtLnF0eSA9IHRoaXMuaXRlbS5xdHk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYodGhpcy5pdGVtLnF0eT09JycgfHwgdGhpcy5pdGVtLnF0eT09JzAnKXtcclxuXHRcdFx0XHR0aGlzLml0ZW0ucXR5ID0gMTtcclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLiRwYXJlbnQuc3RvY2sodGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5xdHksdGhpcy5pdGVtLm9wdGlvbnMuc3RvY2ssdGhpcy5pdGVtLnJvd0lkKTtcclxuXHRcdH0sXHJcblx0XHRtb3ZlVG9XaXNobGlzdCgpe1xyXG5cdFx0XHR0aGlzLiRwYXJlbnQubW92ZVRvV2lzaGxpc3QodGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5yb3dJZCk7XHJcblx0XHR9LFxyXG5cdFx0cmVtb3ZlSXRlbXMoKXtcclxuXHRcdFx0dGhpcy4kcGFyZW50LnJlbW92ZUl0ZW1zKHRoaXMuaXRlbS5pZCx0aGlzLml0ZW0ubmFtZSx0aGlzLml0ZW0ucm93SWQpO1xyXG5cdFx0fVxyXG5cdH0sXHJcblx0Y29tcHV0ZWQ6e1xyXG5cdFx0c2FsZV9wcmljZSgpe1xyXG5cdFx0XHR2YXIgc2FsZVByaWNlID0gdGhpcy5pdGVtLm9wdGlvbnMuc2FsZV9wcmljZTtcclxuXHRcdFx0cmV0dXJuIHNhbGVQcmljZS5yZXBsYWNlKCcsJywgJycpO1xyXG5cdFx0fSxcdFxyXG5cdFx0aXRlbVByaWNlKCl7XHJcblx0XHRcdHZhciBzYWxlUHJpY2UgPSB0aGlzLml0ZW0ub3B0aW9ucy5zYWxlX3ByaWNlO1xyXG5cdFx0XHRpZihwYXJzZUludCh0aGlzLml0ZW0ub3B0aW9ucy5zdG9jayk9PTApe1xyXG5cdFx0XHRcdHJldHVybiBzYWxlUHJpY2UucmVwbGFjZSgnLCcsICcnKTtcclxuXHRcdFx0fVx0XHRcdFxyXG5cdFx0XHRpZihwYXJzZUludCh0aGlzLml0ZW0ucXR5KT5wYXJzZUludCh0aGlzLml0ZW0ub3B0aW9ucy5zdG9jaykgJiYgcGFyc2VJbnQodGhpcy5pdGVtLm9wdGlvbnMuc3RvY2spIT0wKXtcclxuXHRcdFx0XHRyZXR1cm4gdGhpcy5pdGVtLm9wdGlvbnMuc3RvY2sgKiBzYWxlUHJpY2UucmVwbGFjZSgnLCcsICcnKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZihwYXJzZUludCh0aGlzLml0ZW0ucXR5KTw9cGFyc2VJbnQodGhpcy5pdGVtLm9wdGlvbnMuc3RvY2spKXtcclxuXHRcdFx0XHRyZXR1cm4gdGhpcy5pdGVtLnF0eSAqIHNhbGVQcmljZS5yZXBsYWNlKCcsJywgJycpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmKHRoaXMuaXRlbS5xdHk9PScnIHx8IHRoaXMuaXRlbS5xdHk9PScwJyl7XHJcblx0XHRcdFx0cmV0dXJuIDEgKiBzYWxlUHJpY2UucmVwbGFjZSgnLCcsICcnKTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcdHNoaXBwaW5nKCl7XHJcblx0XHRcdHJldHVybiB0aGlzLml0ZW0ub3B0aW9ucy5zaGlwcGluZ19mZWUqdGhpcy5pdGVtLnF0eSArIHRoaXMuaXRlbS5vcHRpb25zLmNoYXJnZSp0aGlzLml0ZW0ucXR5ICsgdGhpcy5pdGVtLm9wdGlvbnMudmF0KnRoaXMuaXRlbS5xdHk7XHJcblx0XHR9LFxyXG5cdFx0c3ViVG90YWwoKXtcclxuXHRcdFx0cmV0dXJuIHRoaXMuaXRlbVByaWNlICsgdGhpcy5zaGlwcGluZztcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENhcnRJdGVtLnZ1ZT80ZmM4MTU1NCIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2PlxyXG5cdFx0PGRpdiBjbGFzcz1cInRhYmxlLXJlc3BvbnNpdmVcIj5cclxuXHRcdFx0PHRhYmxlIGNsYXNzPSd0YWJsZSB0YWJsZS1ob3Zlcic+XHJcblx0XHRcdFx0PHRoZWFkPlxyXG5cdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHQ8dGg+SXRlbS9zPC90aD5cclxuXHRcdFx0XHRcdFx0PHRoPlF0eTwvdGg+XHJcblx0XHRcdFx0XHRcdDx0aD5QcmljZTwvdGg+XHJcblx0XHRcdFx0XHRcdDx0aD5TdWJ0b3RhbDwvdGg+XHJcblx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdDwvdGhlYWQ+XHJcblx0XHRcdFx0PHRib2R5PlxyXG5cdFx0XHRcdFx0PHRyIHYtZm9yPSdpdGVtIGluIGl0ZW1zJyA6a2V5PSdpdGVtLmlkJz5cdFxyXG5cdFx0XHRcdFx0XHQ8dGQ+e3tpdGVtLm5hbWV9fTwvdGQ+XHJcblx0XHRcdFx0XHRcdDx0ZD57e2l0ZW0ucXR5fX08L3RkPlxyXG5cdFx0XHRcdFx0XHQ8dGQ+e3tzYWxlX3ByaWNlIHwgY3VycmVuY3l9fTwvdGQ+XHJcblx0XHRcdFx0XHRcdDx0ZD57e2l0ZW0ucXR5ICogc2FsZV9wcmljZSB8IGN1cnJlbmN5fX08L3RkPlxyXG5cdFx0XHRcdFx0PC90cj5cclxuXHRcdFx0XHQ8L3Rib2R5PlxyXG5cdFx0XHQ8L3RhYmxlPlxyXG5cdFx0XHQ8c2xvdCBuYW1lPSdmb290ZXInPjwvc2xvdD5cclxuXHRcdDwvZGl2PlxyXG5cdDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRwcm9wczogWydpdGVtcyddLFxyXG5cdGNvbXB1dGVkOntcclxuXHRcdHNhbGVfcHJpY2UoKXtcclxuXHRcdFx0dmFyIHNhbGVQcmljZSA9IHRoaXMuaXRlbS5vcHRpb25zLnNhbGVfcHJpY2U7XHJcblx0XHRcdHJldHVybiBzYWxlUHJpY2UucmVwbGFjZSgnLCcsICcnKTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENhcnRMaXN0LnZ1ZT8yNzliOGYyYiIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2PlxyXG5cdFx0PGRpdiBjbGFzcz1cInRhYmxlLXJlc3BvbnNpdmVcIj5cclxuXHRcdFx0PHRhYmxlIGNsYXNzPSd0YWJsZSB0YWJsZS1ob3Zlcic+XHJcblx0XHRcdFx0PHRoZWFkPlxyXG5cdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHQ8dGg+SXRlbS9zPC90aD5cclxuXHRcdFx0XHRcdFx0PHRoPlF0eTwvdGg+XHJcblx0XHRcdFx0XHRcdDx0aD5QcmljZTwvdGg+XHJcblx0XHRcdFx0XHRcdDx0aD5TdWJ0b3RhbDwvdGg+XHJcblx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdDwvdGhlYWQ+XHJcblx0XHRcdFx0PHRib2R5PlxyXG5cdFx0XHRcdFx0PHRyIHYtZm9yPSdvcmRlciBpbiBvcmRlcnMnIDprZXk9J29yZGVyLmlkJz5cdFxyXG5cdFx0XHRcdFx0XHQ8dGQ+e3tvcmRlci5uYW1lfX08L3RkPlxyXG5cdFx0XHRcdFx0XHQ8dGQ+e3tvcmRlci5xdHl9fTwvdGQ+XHJcblx0XHRcdFx0XHRcdDx0ZD57e3NhbGVfcHJpY2UgfCBjdXJyZW5jeX19PC90ZD5cclxuXHRcdFx0XHRcdFx0PHRkPnt7b3JkZXIucXR5ICogc2FsZV9wcmljZSB8IGN1cnJlbmN5fX08L3RkPlxyXG5cdFx0XHRcdFx0PC90cj5cclxuXHRcdFx0XHQ8L3Rib2R5PlxyXG5cdFx0ICAgIDwvdGFibGU+XHJcblx0XHRcdDxzbG90IG5hbWU9J2Zvb3Rlcic+PC9zbG90PlxyXG5cdFx0PC9kaXY+XHJcblx0PC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cdHByb3BzOiBbJ29yZGVycyddLFxyXG5cdGNvbXB1dGVkOntcclxuXHRcdHNhbGVfcHJpY2UoKXtcclxuXHRcdFx0dmFyIHNhbGVQcmljZSA9IHRoaXMuaXRlbS5vcHRpb25zLnNhbGVfcHJpY2U7XHJcblx0XHRcdHJldHVybiBzYWxlUHJpY2UucmVwbGFjZSgnLCcsICcnKTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE9yZGVyTGlzdC52dWU/MGY5M2Y1MzIiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9DYXJ0SXRlbS52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTIyODg0ODQ2XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0NhcnRJdGVtLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxjYXJ0XFxcXENhcnRJdGVtLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIENhcnRJdGVtLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0yMjg4NDg0NlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTIyODg0ODQ2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9jYXJ0L0NhcnRJdGVtLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzI0XG4vLyBtb2R1bGUgY2h1bmtzID0gNiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NhcnRMaXN0LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMmM4ZGMwNTFcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQ2FydExpc3QudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXGNhcnRcXFxcQ2FydExpc3QudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQ2FydExpc3QudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTJjOGRjMDUxXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMmM4ZGMwNTFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NhcnQvQ2FydExpc3QudnVlXG4vLyBtb2R1bGUgaWQgPSAzMjVcbi8vIG1vZHVsZSBjaHVua3MgPSA2IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vT3JkZXJMaXN0LnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjAwZmJmYWVcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vT3JkZXJMaXN0LnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxjYXJ0XFxcXE9yZGVyTGlzdC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBPcmRlckxpc3QudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTYwMGZiZmFlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNjAwZmJmYWVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NhcnQvT3JkZXJMaXN0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzI2XG4vLyBtb2R1bGUgY2h1bmtzID0gNiIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMiBjb2wtbWQtNiBjb2wtc20tNlwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyZW1vdmVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcInRpdGxlXCI6IHRoaXMuJHBhcmVudC5sYW5nWydyZW1vdmUnXVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5yZW1vdmVJdGVtcygpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi1yZW1vdmVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJhcmlhLWhpZGRlblwiOiBcInRydWVcIlxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjYXJkIHZjZW50ZXItbGdcIixcbiAgICBjbGFzczoge1xuICAgICAgJ2l0ZW0tc2VsZWN0ZWQnOiB0aGlzLiRwYXJlbnQuc2VsZWN0ZWQuaW5kZXhPZihfdm0uaXRlbS5pZCkgPiAtMVxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiZGF0YS1pZFwiOiBfdm0uaXRlbS5pZFxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTVcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctNSBjb2wtbWQtMTIgY29sLXNtLTEyXCJcbiAgfSwgW19jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogJy9wcm9kdWN0LycgKyBfdm0uaXRlbS5vcHRpb25zLnNsdWdcbiAgICB9XG4gIH0sIFtfYygnaW1nJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImltZyBpbWctcmVzcG9uc2l2ZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInNyY1wiOiBfdm0uaXRlbS5vcHRpb25zLmltYWdlXG4gICAgfVxuICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTcgY29sLW1kLTEyIGNvbC1zbS0xMlwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvdyB0ZXh0LXNtLWNlbnRlclwiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjYXJ0LWl0ZW0tbmFtZVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLm5hbWUpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXNtIGJ0bi1saW5rXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiAnL3N0b3Jlcy8nICsgX3ZtLml0ZW0ub3B0aW9ucy5zdG9yZVxuICAgIH1cbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcnQtaXRlbS1zdG9yZVwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLm9wdGlvbnMuc3RvcmUpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgZGlyZWN0aXZlczogW3tcbiAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgIHZhbHVlOiAoX3ZtLml0ZW0ub3B0aW9ucy5jb2xvciAhPSAnJyksXG4gICAgICBleHByZXNzaW9uOiBcIml0ZW0ub3B0aW9ucy5jb2xvciAhPSAnJyBcIlxuICAgIH1dXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWydjb2xvciddKSArIFwiOiBcIiArIF92bS5fcyhfdm0uaXRlbS5vcHRpb25zLmNvbG9yKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uaXRlbS5vcHRpb25zLnNpemUgIT0gJycpLFxuICAgICAgZXhwcmVzc2lvbjogXCJpdGVtLm9wdGlvbnMuc2l6ZSAhPSAnJyBcIlxuICAgIH1dXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWydzaXplJ10pICsgXCI6IFwiICsgX3ZtLl9zKF92bS5pdGVtLm9wdGlvbnMuc2l6ZSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ3aXNobGlzdFwiLFxuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0ubW92ZVRvV2lzaGxpc3QoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImdseXBoaWNvbiBnbHlwaGljb24taGVhcnRcIlxuICB9KSwgX3ZtLl92KFwiIFwiICsgX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWydtb3ZlLXRvLXdpc2hsaXN0J10pKV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTcgdGV4dC1yaWdodFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMlwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0yIGNvbC1tZC0xMiAgZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcnQtaGVhZGVyIGhpZGRlbi1sZyBjb2wtbWQtNiBjb2wtc20tNlwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWydxdWFudGl0eSddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMTIgY29sLW1kLTYgY29sLXNtLTZcIlxuICB9LCBbKF92bS5pdGVtLm9wdGlvbnMuc3RvY2sgIT0gJzAnKSA/IF9jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5pdGVtLnF0eSksXG4gICAgICBleHByZXNzaW9uOiBcIml0ZW0ucXR5XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgdGV4dC1yaWdodFwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIm1hcmdpbi1sZWZ0XCI6IFwiLTE0cHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ3EnICsgX3ZtLml0ZW0uaWQsXG4gICAgICBcInR5cGVcIjogXCJudW1iZXJcIixcbiAgICAgIFwibWF4XCI6IF92bS5pdGVtLm9wdGlvbnMuc3RvY2ssXG4gICAgICBcIm1pblwiOiBcIjFcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5pdGVtLnF0eSlcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0udXBkYXRlcXR5KClcbiAgICAgIH0sXG4gICAgICBcImtleXVwXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uc3RvY2soKVxuICAgICAgfSxcbiAgICAgIFwiaW5wdXRcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykgeyByZXR1cm47IH1cbiAgICAgICAgX3ZtLml0ZW0ucXR5ID0gJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgfVxuICAgIH1cbiAgfSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5pdGVtLm9wdGlvbnMuc3RvY2sgPT0gJzAnKSA/IF9jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5pdGVtLnF0eSksXG4gICAgICBleHByZXNzaW9uOiBcIml0ZW0ucXR5XCJcbiAgICB9XSxcbiAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2wgdGV4dC1yaWdodFwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcIm1hcmdpbi1sZWZ0XCI6IFwiLTE0cHhcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogJ3EnICsgX3ZtLml0ZW0uaWQsXG4gICAgICBcInR5cGVcIjogXCJudW1iZXJcIixcbiAgICAgIFwibWluXCI6IFwiMVwiXG4gICAgfSxcbiAgICBkb21Qcm9wczoge1xuICAgICAgXCJ2YWx1ZVwiOiAoX3ZtLml0ZW0ucXR5KVxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS51cGRhdGVxdHkoKVxuICAgICAgfSxcbiAgICAgIFwia2V5dXBcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5zdG9jaygpXG4gICAgICB9LFxuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uaXRlbS5xdHkgPSAkZXZlbnQudGFyZ2V0LnZhbHVlXG4gICAgICB9XG4gICAgfVxuICB9KSA6IF92bS5fZSgpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMyBjb2wtbWQtMTIgZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcnQtaGVhZGVyIGhpZGRlbi1sZyBjb2wtbWQtNiBjb2wtc20tNlwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWydpdGVtLXByaWNlJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMiBjb2wtbWQtNiBjb2wtc20tNiBjYXJ0LWl0ZW0tcHJpY2VcIlxuICB9LCBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0XFx0XFx0XFx0XCIgKyBfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikoX3ZtLnNhbGVfcHJpY2UpKSArIFwiXFxuXFx0XFx0XFx0XFx0XFx0XCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMyBjb2wtbWQtMTIgZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcnQtaGVhZGVyIGhpZGRlbi1sZyBjb2wtbWQtNiBjb2wtc20tNlwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWydzaGlwcGluZy1mZWUnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTEyIGNvbC1tZC02IGNvbC1zbS02IGNhcnQtaXRlbS1wcmljZSBzaGlwcGluZ1wiXG4gIH0sIFtfdm0uX3YoXCJcXG5cXHRcXHRcXHRcXHRcXHRcXHRcIiArIF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShfdm0uc2hpcHBpbmcpKSArIFwiXFxuXFx0XFx0XFx0XFx0XFx0XCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctNCBjb2wtbWQtMTIgZm9ybS1ncm91cFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcnQtaGVhZGVyIGhpZGRlbi1sZyBjb2wtbWQtNiBjb2wtc20tNlwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWydzdWJ0b3RhbCddKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbGctMTIgY29sLW1kLTYgY29sLXNtLTYgY2FydC1pdGVtLXByaWNlXCJcbiAgfSwgW192bS5fdihcIlxcblxcdFxcdFxcdFxcdFxcdFxcdFwiICsgX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKF92bS5zdWJUb3RhbCkpICsgXCJcXG5cXHRcXHRcXHRcXHRcXHRcIildKV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMjI4ODQ4NDZcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0yMjg4NDg0NlwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NhcnQvQ2FydEl0ZW0udnVlXG4vLyBtb2R1bGUgaWQgPSAzNzFcbi8vIG1vZHVsZSBjaHVua3MgPSA2IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZS1yZXNwb25zaXZlXCJcbiAgfSwgW19jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1ob3ZlclwiXG4gIH0sIFtfdm0uX20oMCksIF92bS5fdihcIiBcIiksIF9jKCd0Ym9keScsIF92bS5fbCgoX3ZtLml0ZW1zKSwgZnVuY3Rpb24oaXRlbSkge1xuICAgIHJldHVybiBfYygndHInLCB7XG4gICAgICBrZXk6IGl0ZW0uaWRcbiAgICB9LCBbX2MoJ3RkJywgW192bS5fdihfdm0uX3MoaXRlbS5uYW1lKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoaXRlbS5xdHkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShfdm0uc2FsZV9wcmljZSkpKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShpdGVtLnF0eSAqIF92bS5zYWxlX3ByaWNlKSkpXSldKVxuICB9KSldKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl90KFwiZm9vdGVyXCIpXSwgMildKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCd0aGVhZCcsIFtfYygndHInLCBbX2MoJ3RoJywgW192bS5fdihcIkl0ZW0vc1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiUXR5XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJQcmljZVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCBbX3ZtLl92KFwiU3VidG90YWxcIildKV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0yYzhkYzA1MVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTJjOGRjMDUxXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvY2FydC9DYXJ0TGlzdC52dWVcbi8vIG1vZHVsZSBpZCA9IDM3M1xuLy8gbW9kdWxlIGNodW5rcyA9IDYiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYmxlLXJlc3BvbnNpdmVcIlxuICB9LCBbX2MoJ3RhYmxlJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYmxlIHRhYmxlLWhvdmVyXCJcbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0ub3JkZXJzKSwgZnVuY3Rpb24ob3JkZXIpIHtcbiAgICByZXR1cm4gX2MoJ3RyJywge1xuICAgICAga2V5OiBvcmRlci5pZFxuICAgIH0sIFtfYygndGQnLCBbX3ZtLl92KF92bS5fcyhvcmRlci5uYW1lKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3Mob3JkZXIucXR5KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikoX3ZtLnNhbGVfcHJpY2UpKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RkJywgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikob3JkZXIucXR5ICogX3ZtLnNhbGVfcHJpY2UpKSldKV0pXG4gIH0pKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX3QoXCJmb290ZXJcIildLCAyKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ3RoZWFkJywgW19jKCd0cicsIFtfYygndGgnLCBbX3ZtLl92KFwiSXRlbS9zXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJRdHlcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywgW192bS5fdihcIlByaWNlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIFtfdm0uX3YoXCJTdWJ0b3RhbFwiKV0pXSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTYwMGZiZmFlXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNjAwZmJmYWVcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9jYXJ0L09yZGVyTGlzdC52dWVcbi8vIG1vZHVsZSBpZCA9IDM4OFxuLy8gbW9kdWxlIGNodW5rcyA9IDYiLCI8dGVtcGxhdGU+XHJcbjxkaXYgY2xhc3M9XCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIgOmlkPVwiaWRcIiB0YWJpbmRleD1cIi0xXCIgcm9sZT1cImRpYWxvZ1wiIGFyaWEtbGFiZWxsZWRieT1cIm1vZGFsLWxhYmVsXCI+XHJcbiAgICA8ZGl2IDpjbGFzcz1cIidtb2RhbC1kaWFsb2cgJytzaXplXCIgcm9sZT1cImRvY3VtZW50XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwibW9kYWwtdGl0bGVcIiBpZD1cIm1vZGFsLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLXRpdGxlXCI+TW9kYWwgVGl0bGU8L3Nsb3Q+XHJcbiAgICAgICAgICAgICAgICA8L2g0PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1ib2R5XCI+TW9kYWwgQm9keTwvc2xvdD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cclxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1mb290ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvc2xvdD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgICdpZCc6e3JlcXVpcmVkOnRydWV9XHJcbiAgICAgICAgLCdzaXplJzoge2RlZmF1bHQ6J21vZGFsLXNtJ31cclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIERpYWxvZ01vZGFsLnZ1ZT8zYzk5MDhhNiIsIjx0ZW1wbGF0ZT5cclxuPGRpdiBjbGFzcz1cIm1vZGFsIGZhZGVcIiA6aWQ9XCJpZFwiIHRhYmluZGV4PVwiLTFcIiByb2xlPVwiZGlhbG9nXCIgYXJpYS1sYWJlbGxlZGJ5PVwibW9kYWwtbGFiZWxcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1kaWFsb2dcIiA6Y2xhc3M9XCJzaXplXCIgcm9sZT1cImRvY3VtZW50XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz0ncm93Jz5cclxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9J2V4dHJhLWxlZnQnPjwvc2xvdD5cclxuICAgICAgICAgICAgICAgIDxkaXYgOmNsYXNzPVwiJ2NvbC1tZC0nK2NvbnRlbnRTaXplKycgbW9kYWwtc20tJytjb250ZW50U2l6ZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjbG9zZVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCIgYXJpYS1sYWJlbD1cIkNsb3NlXCI+PHNwYW4gYXJpYS1oaWRkZW49XCJ0cnVlXCI+JnRpbWVzOzwvc3Bhbj48L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwibW9kYWwtdGl0bGVcIiBpZD1cIm1vZGFsLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtdGl0bGVcIj5Nb2RhbCBUaXRsZTwvc2xvdD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9oND5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtYm9keVwiPk1vZGFsIEJvZHk8L3Nsb3Q+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtZm9vdGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8IS0tIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1mbGF0XCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPiAtLT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9zbG90PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPSdtb2RhbC1yaWdodCc+PC9zbG90PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG48L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgICdzaXplJzoge2RlZmF1bHQ6J21vZGFsLW1kJ31cclxuICAgICAgICAsJ2lkJzoge3JlcXVpcmVkOnRydWV9XHJcbiAgICAgICAgLCdjb250ZW50U2l6ZSc6IHtkZWZhdWx0OicxMid9XHJcblxyXG4gICAgfVxyXG59XHJcbjwvc2NyaXB0PlxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gTW9kYWwudnVlPzMzM2Q4MTk1IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi02NTNlNzM2OVxcXCJ9IS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9EaWFsb2dNb2RhbC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxcRGlhbG9nTW9kYWwudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRGlhbG9nTW9kYWwudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTY1M2U3MzY5XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNjUzZTczNjlcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gOVxuLy8gbW9kdWxlIGNodW5rcyA9IDYgMTQgMTUiXSwic291cmNlUm9vdCI6IiJ9