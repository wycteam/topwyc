/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 426);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 176:
/***/ (function(module, exports, __webpack_require__) {


Vue.component('ewallet-button', __webpack_require__(329));

var app = new Vue({
	el: '#ewallet_container',
	components: {
		Multiselect: window.VueMultiselect.default
	},
	data: {
		transferTo: [],
		lang: [],
		btns: [{
			icon: 'account_balance_wallet',
			title: 'Load Money',
			subtitle: 'Offline or Online',
			modal: 'modalLoad'
		}, {
			icon: 'exit_to_app',
			title: 'Withdraw Funds',
			subtitle: 'Get your saved money',
			modal: 'modalWithdraw'
		}, {
			icon: 'compare_arrows',
			title: 'Transfer or Receive',
			subtitle: 'to/from another Account',
			modal: 'modalTransferReceive'
		}, {
			icon: 'account_balance',
			title: 'Bank Account',
			subtitle: 'Register your account',
			modal: 'modalAddBank'
		}],
		showLoadingOptions: true,
		showOnlineArea: false,
		showOfflineArea: false,
		showLoadingBack: false,

		showTransferReceiveOptions: true,
		showTransferArea: false,
		showReceiveArea: false,
		showTransferReceiveBack: false,

		user_id: null,
		ewallet_balance: 0,
		usable_balance: 0,
		bank_accounts: false,
		transactions: false,
		banks: false,
		fetchedAccounts: [],
		bankAccountForm: new Form({
			bank_id: '',
			account_name: null,
			account_number: null,
			reaccount_number: null
		}, { baseURL: 'http://' + Laravel.base_api_url }),
		bankAccountProcessing: null,
		bankAccountMode: true,

		loadOfflineForm: new Form({
			bank_id: '',
			amount: null
		}, { baseURL: 'http://' + Laravel.base_api_url }),
		withdrawOfflineForm: new Form({
			bank_id: '',
			amount: null,
			type: 'Withdraw'
		}, { baseURL: 'http://' + Laravel.base_api_url }),
		transferForm: new Form({
			bank_id: '',
			amount: null,
			type: 'Transfer',
			note: ''
		}, { baseURL: 'http://' + Laravel.base_api_url }),
		isFetchingAccounts: false,
		transferCode: '',

		current_transaction: '',
		current_transaction_image: '',
		current_transaction_status: '',
		current_transaction_reason: '',
		baseHeight: 0,
		baseWidth: 0,
		baseZoom: 1,
		showZoomable: false,

		maxDepositRequest: 700000,
		friends: []
	},

	mounted: function mounted() {
		var _this = this;

		$('#modalDepositUpload').on('shown.bs.modal', function () {
			_this.baseZoom = 1;
			img = $('<img id="shittyimage" class="center-block img-responsive" src="' + _this.current_transaction_image + '">').on('load', function () {
				var $modalImg = $('<img src="' + _this.current_transaction_image + '" class="center-block  img-responsive" >');
				$modalImg.appendTo('#doc-container');
				_this.baseHeight = $modalImg.height();
				_this.baseWidth = $modalImg.width();

				$modalImg.draggable();
			});
		});

		$('#modalDepositUpload').on('hide.bs.modal', function () {
			$('#doc-container').empty();
			_this.showZoomable = false;
		});
	},


	watch: {
		baseZoom: function baseZoom(newValue) {
			var changedHeightSize = this.baseHeight * newValue;
			var changedWidthSize = this.baseWidth * newValue;
			$('#doc-container').find('img').css('height', changedHeightSize);
			$('#doc-container').find('img').css('width', changedWidthSize);
			$('#doc-container').find('img').removeClass('img-responsive');
		},

		current_transaction_image: function current_transaction_image() {
			this.baseZoom = 1;
		},

		transactions: function transactions() {
			$('#ewallet_transaction_table').DataTable().destroy();

			setTimeout(function () {
				currentTable = $('#ewallet_transaction_table').DataTable({
					pageLength: 10,
					responsive: true
				});
			}, 800);
		}

	},

	created: function created() {
		var _this2 = this;

		Event.listen('Imgfileupload.depositUpload', function (result) {

			if (result) {
				toastr.success(_this2.lang['wait-verification'], _this2.lang['success-upload']);
			} else {
				toastr.error(_this2.lang['try-again'], _this2.lang['failed-upload']);
			}
			$('#modalDepositUpload').modal('toggle');
		});
		this.user_id = Laravel.user.id;
		// axios
		function getTranslation() {
			return axios.get('/translate/ewallet');
		}
		function getEwalletBalance() {
			return axiosAPIv1.get('/user/' + this.user_id + '/ewallet');
		}
		function getUsableBalance() {
			return axiosAPIv1.get('/getUsable');
		}
		function getBankAccounts() {
			return axiosAPIv1.get('/user/' + this.user_id + '/bank-accounts');
		}
		function getTransactions() {
			return axiosAPIv1.get('/ewallet-transaction');
		}
		function getPurchasedOrders() {
			return axiosAPIv1.get('/ewallet-transaction/get-purchased-orders');
		}
		function getCancelledOrders() {
			return axiosAPIv1.get('/ewallet-transaction/get-cancelled-orders');
		}
		function getSoldOrders() {
			return axiosAPIv1.get('/ewallet-transaction/get-sold-orders');
		}
		function getReturnedOrders() {
			return axiosAPIv1.get('/ewallet-transaction/get-buyer-returned-orders');
		}
		function getBanks() {
			return axiosAPIv1.get('/banks');
		}
		function getDocs() {
			return axiosAPIv1.get('/users/' + Laravel.user.id + '/documents');
		}

		axios.all([getTranslation(), getTransactions(), getEwalletBalance(), getUsableBalance(), getBankAccounts(), getBanks(), getDocs(), getPurchasedOrders(), getCancelledOrders(), getSoldOrders(), getReturnedOrders()]).then(axios.spread(function (language, transactions, balance, usable, bank_accounts, banks, documents, purchased_transactions, cancelled_transactions, sold_transactions, returned_transactions) {

			_this2.lang = language.data.data;
			_this2.btns[0].title = _this2.lang["load-money"];
			_this2.btns[1].title = _this2.lang["withdraw-funds"];
			_this2.btns[2].title = _this2.lang["transfer-or-receive"];
			_this2.btns[3].title = _this2.lang["bank-account"];

			_this2.btns[0].subtitle = _this2.lang["offline-or-online"];
			_this2.btns[1].subtitle = _this2.lang["withdraw-funds"];
			_this2.btns[2].subtitle = _this2.lang["get-saved-money"];
			_this2.btns[3].subtitle = _this2.lang["reg-account"];

			_this2.ewallet_balance = balance.data.success ? parseFloat(balance.data.data.amount) : 0;
			_this2.usable_balance = usable.data.success ? parseFloat(usable.data.data.usable) : 0;
			_this2.bank_accounts = bank_accounts.data.success ? bank_accounts.data.data.bank_accounts : false;
			_this2.banks = banks.data.success ? banks.data.data : false;
			_this2.transactions = transactions.status == 200 ? transactions.data : false;

			if (purchased_transactions.status == 200) {
				_this2.transactions = _this2.transactions.concat(purchased_transactions.data);
			}

			if (cancelled_transactions.status == 200) {
				_this2.transactions = _this2.transactions.concat(cancelled_transactions.data);
			}

			if (sold_transactions.status == 200) {
				_this2.transactions = _this2.transactions.concat(sold_transactions.data);
			}

			if (returned_transactions.status == 200) {
				_this2.transactions = _this2.transactions.concat(returned_transactions.data);
			}

			if (documents.status == 200) {
				if (documents.data.length) {
					var enterprise = 1;
					enterprise *= documents.data.filter(function (obj) {
						return obj.type === 'sec' && obj.status === 'Verified';
					}).length;

					enterprise *= documents.data.filter(function (obj) {
						return obj.type === 'bir' && obj.status === 'Verified';
					}).length;

					enterprise *= documents.data.filter(function (obj) {
						return obj.type === 'permit' && obj.status === 'Verified';
					}).length;
					if (enterprise) {
						_this2.maxDepositRequest = 7000000 - _this2.ewallet_balance;
					} else {
						_this2.maxDepositRequest = 1000000 - _this2.ewallet_balance;
					}
				}
			}

			Vue.nextTick(function () {
				setTimeout(function () {
					// $('[data-toggle="tooltip"]').attr('data-original-title',$(this).attr('data-temp-title'));
					$('#ewallet_transaction_table tr[data-toggle="tooltip"]').each(function () {
						$(this).attr('data-original-title', $(this).attr('data-temp-title'));
					});
					$('[data-toggle="tooltip"]').tooltip
					// $('[data-toggle="tooltip"]').tooltip('fixTitle');
					();$('#ewallet_transaction_table').dataTable();
				}, 500);
			});
		})).catch($.noop);
	},


	methods: {
		CreditOrDebit: function CreditOrDebit(type) {
			switch (type) {
				case 'Purchase':
				case 'Withdraw':
				case 'Transfer':
				case 'Product Returned by Buyer':
					return 'text-danger';
				case 'Deposit':
				case 'Received':
				case 'Cancelled Order':
				case 'Returned Order':
				case 'Sold':
					return 'text-success';

			}
		},
		tooltipForTransaction: function tooltipForTransaction(status, type, reason) {
			if (status == 'Cancelled') {
				return reason;
			} else if (status == "Pending" && type == "Deposit") {
				return "Please upload you deposit slip.";
			}
			return '';
		},
		fileChange: function fileChange(e) {
			var files = e.target.files || e.dataTransfer.files;
			if (!files.length) return;
			this.createImage(files[0]);
		},
		createImage: function createImage(file) {
			var _this3 = this;

			var image = new Image();
			var reader = new FileReader();
			var vm = this;

			reader.onload = function (e) {
				// this.current_transaction_image = e.target.result;
				$('#doc-container').empty();
				_this3.baseZoom = 1;
				img = $('<img id="shittyimage" class="center-block img-responsive" src="' + e.target.result + '">').on('load', function () {
					var $modalImg = $('<img src="' + e.target.result + '" class="center-block img-responsive" >');
					$modalImg.appendTo('#doc-container');
					_this3.baseHeight = $modalImg.height();
					_this3.baseWidth = $modalImg.width();
					_this3.showZoomable = true;

					$modalImg.draggable();
				});
			};
			reader.readAsDataURL(file);
		},


		removeImage: function removeImage(e) {
			this.image = '';
		},

		openModalUploadDeposit: function openModalUploadDeposit(transaction_id, status, reason, ref_number) {
			var _this4 = this;

			this.current_transaction = transaction_id;
			this.current_transaction_status = status;
			this.current_transaction_image = false;
			this.current_transaction_reason = reason;

			axios.get("/shopping/images/deposit-slip/" + ref_number + '.jpg').then(function (result) {
				_this4.current_transaction_image = "/shopping/images/deposit-slip/" + ref_number + '.jpg';
				_this4.showZoomable = true;
			}).catch($.noop);
			$('#modalDepositUpload').modal('toggle');
		},


		//loading of ewallet
		limitText: function limitText(count) {
			return 'and ' + count + ' other accounts';
		},


		chooseOnline: function chooseOnline() {
			this.showLoadingOptions = false;
			this.showOfflineArea = false;
			this.showOnlineArea = true;
			this.showLoadingBack = true;
		},

		chooseOffline: function chooseOffline() {
			this.showLoadingOptions = false;
			this.showOnlineArea = false;
			this.showOfflineArea = true;
			this.showLoadingBack = true;
		},
		//transfer/receive of ewallet
		chooseTransfer: function chooseTransfer() {
			var _this5 = this;

			axiosAPIv1.get('/friends').then(function (result) {
				var friends = [];
				for (var i = 0; i < result.data.friends.length; i++) {
					friends.push(result.data.friends[i].user);
				}
				_this5.friends = friends;
				_this5.fetchedAccounts = friends;
			});

			this.showTransferReceiveOptions = false;
			this.showReceiveArea = false;
			this.showTransferArea = true;
			this.showTransferReceiveBack = true;
		},

		chooseReceive: function chooseReceive() {
			this.showTransferReceiveOptions = false;
			this.showTransferArea = false;
			this.showReceiveArea = true;
			this.showTransferReceiveBack = true;
		},

		openModal: function openModal(modal) {
			resetModals();
			$('#' + modal).modal('toggle');
		},

		resetModals: function resetModals() {
			this.showLoadingOptions = true;
			this.showOnlineArea = false;
			this.showOfflineArea = false;
			this.showLoadingBack = false;

			this.showTransferReceiveOptions = true;
			this.showTransferArea = false;
			this.showReceiveArea = false;
			this.showTransferReceiveBack = false;

			this.bankAccountMode = true;
			this.bankAccountForm.bank_id = '';
			this.bankAccountForm.account_name = null;
			this.bankAccountForm.account_number = null;
			this.bankAccountForm.reaccount_number = null;

			this.bankAccountProcessing = null;

			this.current_transaction_reason = '';

			this.bankAccountForm = new Form({
				bank_id: '',
				account_name: null,
				account_number: null,
				reaccount_number: null
			}, { baseURL: 'http://' + Laravel.base_api_url });

			this.loadOfflineForm = new Form({
				bank_id: '',
				amount: null
			}, { baseURL: 'http://' + Laravel.base_api_url });

			this.withdrawOfflineForm = new Form({
				bank_id: '',
				amount: null,
				type: 'Withdraw'
			}, { baseURL: 'http://' + Laravel.base_api_url });

			this.transferForm = new Form({
				bank_id: '',
				amount: null,
				type: 'Transfer',
				note: ''
			}, { baseURL: 'http://' + Laravel.base_api_url });

			this.current_transaction_image = '';
		},

		nameWithEmail: function nameWithEmail(_ref) {
			var full_name = _ref.full_name,
			    email = _ref.email;

			return full_name + ' (' + email + ')';
		},
		submitNewBankAccount: function submitNewBankAccount() {
			var _this6 = this;

			this.bankAccountForm.submit('post', '/v1/user/' + this.user_id + '/bank-accounts').then(function (result) {
				if (result.success) {
					_this6.bank_accounts.unshift(result.data);
					toastr.success(_this6.lang["account-added"]);
					_this6.bankAccountForm.bank_id = '';
					_this6.bankAccountForm.account_name = null;
					_this6.bankAccountForm.account_number = null;
					_this6.bankAccountForm.reaccount_number = null;
				} else {
					toastr.error(_this6.lang['try-again']);
				}
			}).catch($.noop);
		},
		makeBankAccountModeAdd: function makeBankAccountModeAdd() {
			this.bankAccountMode = true;
			this.bankAccountForm.bank_id = '';
			this.bankAccountForm.account_name = null;
			this.bankAccountForm.account_number = null;
			this.bankAccountForm.reaccount_number = null;

			this.bankAccountProcessing = null;
		},
		updateBankAccount: function updateBankAccount() {
			var _this7 = this;

			function putBankAccounts() {
				return axiosAPIv1.put('/user/' + Laravel.user.id + '/bank-accounts/' + this.bankAccountProcessing);
			}

			this.bankAccountForm.submit('put', '/v1/user/' + Laravel.user.id + '/bank-accounts/' + this.bankAccountProcessing).then(function (result) {
				if (result.success) {
					for (i = 0; i < _this7.bank_accounts.length; i++) {
						if (_this7.bank_accounts[i].id == _this7.bankAccountProcessing) {
							_this7.bank_accounts[i] = result.data;
							break;
						}
					}
					toastr.success(_this7.lang["account-updated"]);
				} else {
					toastr.error(_this7.lang['try-again']);
				}
			}).catch($.noop);
		},
		validateEmail: function validateEmail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		},
		removeDuplicates: function removeDuplicates(originalArray, prop) {
			var newArray = [];
			var lookupObject = {};

			for (var i in originalArray) {
				lookupObject[originalArray[i][prop]] = originalArray[i];
			}

			for (i in lookupObject) {
				newArray.push(lookupObject[i]);
			}
			return newArray;
		},
		fetchAccountsByEmail: function fetchAccountsByEmail(name) {
			var _this8 = this;

			var fetched;

			var friends = null;
			//clone
			friends = $.extend(friends, this.friends);
			//convert to array for multiselect
			friends = $.map(friends, function (el) {
				return el;
			});

			if (this.validateEmail(name)) {
				this.isFetchingAccounts = true;
				//accept only email addresses for not friends
				this.fetchedAccounts = friends;

				axiosAPIv1.get('/users?searchByEmail=' + name).then(function (accounts) {
					if (accounts.data.length) {
						_this8.fetchedAccounts = _this8.fetchedAccounts.concat(accounts.data);
						_this8.fetchedAccounts = _this8.removeDuplicates(_this8.fetchedAccounts, 'email');
					} else {
						_this8.fetchedAccounts = friends;
					}
					_this8.isFetchingAccounts = false;
				}).catch(function (error) {
					_this8.fetchedAccounts = friends;
				});
			} else {
				this.fetchedAccounts = friends;
			}
		},
		showBank: function showBank(bank_account_id) {
			var _this9 = this;

			function getBankAccounts(id) {
				return axiosAPIv1.get('/user/' + Laravel.user.id + '/bank-accounts/' + id);
			}
			axios.all([getBankAccounts(bank_account_id)]).then(axios.spread(function (bank_account) {
				if (bank_account.data.success) {
					_this9.bankAccountProcessing = bank_account_id;
					_this9.bankAccountForm.bank_id = bank_account.data.data.bank_id;
					_this9.bankAccountForm.account_name = bank_account.data.data.account_name;
					_this9.bankAccountForm.account_number = bank_account.data.data.account_number;
					_this9.bankAccountForm.reaccount_number = bank_account.data.data.account_number;
					_this9.bankAccountMode = false;
				}
			})).catch($.noop);
		},
		submitLoadOffline: function submitLoadOffline() {
			var _this10 = this;

			this.loadOfflineForm.submit('post', '/v1/ewallet-transaction').then(function (result) {
				if (result.success) {
					_this10.transactions.push(result.data);
					$('#modalLoad').modal('toggle');
					toastr.info(_this10.lang['ref-no'] + result.data.ref_number + '<br>' + _this10.lang['deposit'] + '<br>' + _this10.lang['bank'] + ': ' + result.data.description, '', {
						timeOut: 0,
						extendedTimeOut: 0
					});
					toastr.success(_this10.lang["success-request"]);

					//show instruction modal
					$('#modalInstructions').modal('toggle');
				} else {
					toastr.error(_this10.lang['try-again']);
				}
			}).catch($.noop);
		},
		submitWithdrawOffline: function submitWithdrawOffline() {
			var _this11 = this;

			this.withdrawOfflineForm.submit('post', '/v1/ewallet-transaction').then(function (result) {
				if (result.success) {
					_this11.transactions.push(result.data);
					$('#modalWithdraw').modal('toggle');
					toastr.info(_this11.lang['ref-no'] + result.data.ref_number + '<br>' + _this11.lang['withdraw'] + '<br>' + _this11.lang['bank'] + ': ' + result.data.description, '', {
						timeOut: 0,
						extendedTimeOut: 0
					});
					toastr.success(_this11.lang["success-request"]);
				} else {
					toastr.error(_this11.lang['try-again']);
				}
			}).catch($.noop);
		},
		submitTransfer: function submitTransfer() {
			var _this12 = this;

			if (this.transferTo.length) {
				this.transferTo.forEach(function (transfer_id) {
					_this12.transferForm.bank_id = transfer_id.id;
					//submit form

					_this12.transferForm.submit('post', '/v1/ewallet-transaction').then(function (result) {

						_this12.fetchedAccounts = [];

						if (result.success) {
							_this12.transactions.push(result.data);
							toastr.info(_this12.lang['code'] + ': ' + result.data.ref_number + '<br>' + _this12.lang["transfer"] + '<br> ' + result.data.description, '', {
								timeOut: 0,
								extendedTimeOut: 0
							});
						} else {

							toastr.error(_this12.lang['try-again']);
						}
						_this12.fetchedAccounts = [];
					}).catch($.noop);
				});
				toastr.success(this.lang['give-receiver']);
				$('#btnTransferSubmit .loader').fadeOut(function () {
					$('#btnTransferSubmit .btn-content').fadeIn();
				}, 100);

				this.transferForm.amount = '';
				this.transferTo = [];

				$('#modalTransferReceive').modal('toggle');
			} else {
				this.transferForm.submit('post', '/v1/ewallet-transaction').then(function (result) {}).catch($.noop);
				$('#btnTransferSubmit .loader').fadeOut(function () {
					$('#btnTransferSubmit .btn-content').fadeIn();
				}, 100);
			}
		},
		submitReceive: function submitReceive() {
			var _this13 = this;

			if (this.transferCode == '') {
				toastr.error(this.lang['invalid-code']);
				$('#modalTransferReceive').modal('toggle');
			} else {
				axiosAPIv1.post('/ewallet-transaction/refnum', {
					refnum: this.transferCode
				}).then(function (result) {
					if (result.data) {
						_this13.transactions.push(result.data.data);
						axiosAPIv1.get('/user/' + _this13.user_id + '/ewallet').then(function (response) {
							_this13.ewallet_balance = parseFloat(response.data.data.amount);
							toastr.success(_this13.lang['ewallet-received']);
						});
						_this13.transferCode = '';
						$('#modalTransferReceive').modal('toggle');
					} else {
						toastr.error(_this13.lang['invalid-code']);
						$('#modalTransferReceive').modal('toggle');
					}
				});
			}
		}
	}
});

$('#uploadDepositSlip').click(function () {
	$('.deposit-uploader input').click();
});

$('#ewallet_container').on('hidden.bs.modal', function () {
	app.resetModals();
});

$(document).on('click', function (e) {
	$('[data-toggle="popover"],[data-original-title]').each(function () {
		//the 'is' for buttons that trigger popups
		//the 'has' for icons within a button that triggers a popup
		if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
			(($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false; // fix for BS 3.3.6
		}
	});
});

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['btn'],
	methods: {
		openModal: function openModal(modal) {
			$('#' + modal).modal('toggle');
		}
	}
});

/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(264),
  /* template */
  __webpack_require__(379),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\ewallet\\EwalletButton.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] EwalletButton.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3a23b2e3", Component.options)
  } else {
    hotAPI.reload("data-v-3a23b2e3", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 379:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-12 col-sm-3 col-xs-6"
  }, [_c('div', {
    staticClass: "card",
    on: {
      "click": function($event) {
        _vm.openModal(_vm.btn.modal)
      }
    }
  }, [_c('div', {
    staticClass: "col-md-4 text-center vertical-center"
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v(_vm._s(_vm.btn.icon))])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card-container"
  }, [_c('h4', [_c('b', [_vm._v(_vm._s(_vm.btn.title))])]), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.btn.subtitle))])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3a23b2e3", module.exports)
  }
}

/***/ }),

/***/ 426:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(176);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9ld2FsbGV0LmpzIiwid2VicGFjazovLy9Fd2FsbGV0QnV0dG9uLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvZXdhbGxldC9Fd2FsbGV0QnV0dG9uLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvZXdhbGxldC9Fd2FsbGV0QnV0dG9uLnZ1ZT9jZGY3Il0sIm5hbWVzIjpbIlZ1ZSIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJhcHAiLCJlbCIsImNvbXBvbmVudHMiLCJNdWx0aXNlbGVjdCIsIndpbmRvdyIsIlZ1ZU11bHRpc2VsZWN0IiwiZGVmYXVsdCIsImRhdGEiLCJ0cmFuc2ZlclRvIiwibGFuZyIsImJ0bnMiLCJpY29uIiwidGl0bGUiLCJzdWJ0aXRsZSIsIm1vZGFsIiwic2hvd0xvYWRpbmdPcHRpb25zIiwic2hvd09ubGluZUFyZWEiLCJzaG93T2ZmbGluZUFyZWEiLCJzaG93TG9hZGluZ0JhY2siLCJzaG93VHJhbnNmZXJSZWNlaXZlT3B0aW9ucyIsInNob3dUcmFuc2ZlckFyZWEiLCJzaG93UmVjZWl2ZUFyZWEiLCJzaG93VHJhbnNmZXJSZWNlaXZlQmFjayIsInVzZXJfaWQiLCJld2FsbGV0X2JhbGFuY2UiLCJ1c2FibGVfYmFsYW5jZSIsImJhbmtfYWNjb3VudHMiLCJ0cmFuc2FjdGlvbnMiLCJiYW5rcyIsImZldGNoZWRBY2NvdW50cyIsImJhbmtBY2NvdW50Rm9ybSIsIkZvcm0iLCJiYW5rX2lkIiwiYWNjb3VudF9uYW1lIiwiYWNjb3VudF9udW1iZXIiLCJyZWFjY291bnRfbnVtYmVyIiwiYmFzZVVSTCIsIkxhcmF2ZWwiLCJiYXNlX2FwaV91cmwiLCJiYW5rQWNjb3VudFByb2Nlc3NpbmciLCJiYW5rQWNjb3VudE1vZGUiLCJsb2FkT2ZmbGluZUZvcm0iLCJhbW91bnQiLCJ3aXRoZHJhd09mZmxpbmVGb3JtIiwidHlwZSIsInRyYW5zZmVyRm9ybSIsIm5vdGUiLCJpc0ZldGNoaW5nQWNjb3VudHMiLCJ0cmFuc2ZlckNvZGUiLCJjdXJyZW50X3RyYW5zYWN0aW9uIiwiY3VycmVudF90cmFuc2FjdGlvbl9pbWFnZSIsImN1cnJlbnRfdHJhbnNhY3Rpb25fc3RhdHVzIiwiY3VycmVudF90cmFuc2FjdGlvbl9yZWFzb24iLCJiYXNlSGVpZ2h0IiwiYmFzZVdpZHRoIiwiYmFzZVpvb20iLCJzaG93Wm9vbWFibGUiLCJtYXhEZXBvc2l0UmVxdWVzdCIsImZyaWVuZHMiLCJtb3VudGVkIiwiJCIsIm9uIiwiaW1nIiwiJG1vZGFsSW1nIiwiYXBwZW5kVG8iLCJoZWlnaHQiLCJ3aWR0aCIsImRyYWdnYWJsZSIsImVtcHR5Iiwid2F0Y2giLCJuZXdWYWx1ZSIsImNoYW5nZWRIZWlnaHRTaXplIiwiY2hhbmdlZFdpZHRoU2l6ZSIsImZpbmQiLCJjc3MiLCJyZW1vdmVDbGFzcyIsIkRhdGFUYWJsZSIsImRlc3Ryb3kiLCJzZXRUaW1lb3V0IiwiY3VycmVudFRhYmxlIiwicGFnZUxlbmd0aCIsInJlc3BvbnNpdmUiLCJjcmVhdGVkIiwiRXZlbnQiLCJsaXN0ZW4iLCJyZXN1bHQiLCJ0b2FzdHIiLCJzdWNjZXNzIiwiZXJyb3IiLCJ1c2VyIiwiaWQiLCJnZXRUcmFuc2xhdGlvbiIsImF4aW9zIiwiZ2V0IiwiZ2V0RXdhbGxldEJhbGFuY2UiLCJheGlvc0FQSXYxIiwiZ2V0VXNhYmxlQmFsYW5jZSIsImdldEJhbmtBY2NvdW50cyIsImdldFRyYW5zYWN0aW9ucyIsImdldFB1cmNoYXNlZE9yZGVycyIsImdldENhbmNlbGxlZE9yZGVycyIsImdldFNvbGRPcmRlcnMiLCJnZXRSZXR1cm5lZE9yZGVycyIsImdldEJhbmtzIiwiZ2V0RG9jcyIsImFsbCIsInRoZW4iLCJzcHJlYWQiLCJsYW5ndWFnZSIsImJhbGFuY2UiLCJ1c2FibGUiLCJkb2N1bWVudHMiLCJwdXJjaGFzZWRfdHJhbnNhY3Rpb25zIiwiY2FuY2VsbGVkX3RyYW5zYWN0aW9ucyIsInNvbGRfdHJhbnNhY3Rpb25zIiwicmV0dXJuZWRfdHJhbnNhY3Rpb25zIiwicGFyc2VGbG9hdCIsInN0YXR1cyIsImNvbmNhdCIsImxlbmd0aCIsImVudGVycHJpc2UiLCJmaWx0ZXIiLCJvYmoiLCJuZXh0VGljayIsImVhY2giLCJhdHRyIiwidG9vbHRpcCIsImRhdGFUYWJsZSIsImNhdGNoIiwibm9vcCIsIm1ldGhvZHMiLCJDcmVkaXRPckRlYml0IiwidG9vbHRpcEZvclRyYW5zYWN0aW9uIiwicmVhc29uIiwiZmlsZUNoYW5nZSIsImUiLCJmaWxlcyIsInRhcmdldCIsImRhdGFUcmFuc2ZlciIsImNyZWF0ZUltYWdlIiwiZmlsZSIsImltYWdlIiwiSW1hZ2UiLCJyZWFkZXIiLCJGaWxlUmVhZGVyIiwidm0iLCJvbmxvYWQiLCJyZWFkQXNEYXRhVVJMIiwicmVtb3ZlSW1hZ2UiLCJvcGVuTW9kYWxVcGxvYWREZXBvc2l0IiwidHJhbnNhY3Rpb25faWQiLCJyZWZfbnVtYmVyIiwibGltaXRUZXh0IiwiY291bnQiLCJjaG9vc2VPbmxpbmUiLCJjaG9vc2VPZmZsaW5lIiwiY2hvb3NlVHJhbnNmZXIiLCJpIiwicHVzaCIsImNob29zZVJlY2VpdmUiLCJvcGVuTW9kYWwiLCJyZXNldE1vZGFscyIsIm5hbWVXaXRoRW1haWwiLCJmdWxsX25hbWUiLCJlbWFpbCIsInN1Ym1pdE5ld0JhbmtBY2NvdW50Iiwic3VibWl0IiwidW5zaGlmdCIsIm1ha2VCYW5rQWNjb3VudE1vZGVBZGQiLCJ1cGRhdGVCYW5rQWNjb3VudCIsInB1dEJhbmtBY2NvdW50cyIsInB1dCIsInZhbGlkYXRlRW1haWwiLCJyZSIsInRlc3QiLCJyZW1vdmVEdXBsaWNhdGVzIiwib3JpZ2luYWxBcnJheSIsInByb3AiLCJuZXdBcnJheSIsImxvb2t1cE9iamVjdCIsImZldGNoQWNjb3VudHNCeUVtYWlsIiwibmFtZSIsImZldGNoZWQiLCJleHRlbmQiLCJtYXAiLCJhY2NvdW50cyIsInNob3dCYW5rIiwiYmFua19hY2NvdW50X2lkIiwiYmFua19hY2NvdW50Iiwic3VibWl0TG9hZE9mZmxpbmUiLCJpbmZvIiwiZGVzY3JpcHRpb24iLCJ0aW1lT3V0IiwiZXh0ZW5kZWRUaW1lT3V0Iiwic3VibWl0V2l0aGRyYXdPZmZsaW5lIiwic3VibWl0VHJhbnNmZXIiLCJmb3JFYWNoIiwidHJhbnNmZXJfaWQiLCJmYWRlT3V0IiwiZmFkZUluIiwic3VibWl0UmVjZWl2ZSIsInBvc3QiLCJyZWZudW0iLCJyZXNwb25zZSIsImNsaWNrIiwiZG9jdW1lbnQiLCJpcyIsImhhcyIsInBvcG92ZXIiLCJpblN0YXRlIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDakRBQSxJQUFJQyxTQUFKLENBQWMsZ0JBQWQsRUFBaUMsbUJBQUFDLENBQVEsR0FBUixDQUFqQzs7QUFFQSxJQUFJQyxNQUFNLElBQUlILEdBQUosQ0FBUTtBQUNoQkksS0FBSSxvQkFEWTtBQUVoQkMsYUFBWTtBQUNWQyxlQUFhQyxPQUFPQyxjQUFQLENBQXNCQztBQUR6QixFQUZJO0FBS2hCQyxPQUFNO0FBQ0xDLGNBQVcsRUFETjtBQUVMQyxRQUFLLEVBRkE7QUFHTkMsUUFBTyxDQUNOO0FBQ0VDLFNBQU0sd0JBRFI7QUFFRUMsVUFBTyxZQUZUO0FBR0VDLGFBQVUsbUJBSFo7QUFJRUMsVUFBTztBQUpULEdBRE0sRUFPTjtBQUNFSCxTQUFNLGFBRFI7QUFFRUMsVUFBTyxnQkFGVDtBQUdFQyxhQUFVLHNCQUhaO0FBSUVDLFVBQU87QUFKVCxHQVBNLEVBYU47QUFDRUgsU0FBTSxnQkFEUjtBQUVFQyxVQUFPLHFCQUZUO0FBR0VDLGFBQVUseUJBSFo7QUFJRUMsVUFBTztBQUpULEdBYk0sRUFtQk47QUFDRUgsU0FBTSxpQkFEUjtBQUVFQyxVQUFPLGNBRlQ7QUFHRUMsYUFBVSx1QkFIWjtBQUlFQyxVQUFPO0FBSlQsR0FuQk0sQ0FIRDtBQThCTkMsc0JBQW1CLElBOUJiO0FBK0JOQyxrQkFBZSxLQS9CVDtBQWdDTkMsbUJBQWdCLEtBaENWO0FBaUNOQyxtQkFBZ0IsS0FqQ1Y7O0FBbUNOQyw4QkFBNEIsSUFuQ3RCO0FBb0NOQyxvQkFBa0IsS0FwQ1o7QUFxQ05DLG1CQUFpQixLQXJDWDtBQXNDTkMsMkJBQXdCLEtBdENsQjs7QUF3Q05DLFdBQVEsSUF4Q0Y7QUF5Q05DLG1CQUFnQixDQXpDVjtBQTBDTkMsa0JBQWUsQ0ExQ1Q7QUEyQ05DLGlCQUFlLEtBM0NUO0FBNENOQyxnQkFBYSxLQTVDUDtBQTZDTkMsU0FBTSxLQTdDQTtBQThDTkMsbUJBQWdCLEVBOUNWO0FBK0NOQyxtQkFBZ0IsSUFBSUMsSUFBSixDQUFTO0FBQ3hCQyxZQUFRLEVBRGdCO0FBRXhCQyxpQkFBYSxJQUZXO0FBR3hCQyxtQkFBZSxJQUhTO0FBSXhCQyxxQkFBaUI7QUFKTyxHQUFULEVBS2IsRUFBRUMsU0FBUyxZQUFVQyxRQUFRQyxZQUE3QixFQUxhLENBL0NWO0FBcUROQyx5QkFBc0IsSUFyRGhCO0FBc0ROQyxtQkFBZ0IsSUF0RFY7O0FBd0ROQyxtQkFBZ0IsSUFBSVYsSUFBSixDQUFTO0FBQ3hCQyxZQUFRLEVBRGdCO0FBRXhCVSxXQUFPO0FBRmlCLEdBQVQsRUFHYixFQUFFTixTQUFTLFlBQVVDLFFBQVFDLFlBQTdCLEVBSGEsQ0F4RFY7QUE0RE5LLHVCQUFzQixJQUFJWixJQUFKLENBQVM7QUFDOUJDLFlBQVEsRUFEc0I7QUFFOUJVLFdBQU8sSUFGdUI7QUFHOUJFLFNBQUs7QUFIeUIsR0FBVCxFQUluQixFQUFFUixTQUFTLFlBQVVDLFFBQVFDLFlBQTdCLEVBSm1CLENBNURoQjtBQWlFTk8sZ0JBQWEsSUFBSWQsSUFBSixDQUFTO0FBQ3JCQyxZQUFRLEVBRGE7QUFFckJVLFdBQU8sSUFGYztBQUdyQkUsU0FBSyxVQUhnQjtBQUlyQkUsU0FBSztBQUpnQixHQUFULEVBS1YsRUFBRVYsU0FBUyxZQUFVQyxRQUFRQyxZQUE3QixFQUxVLENBakVQO0FBdUVOUyxzQkFBbUIsS0F2RWI7QUF3RU5DLGdCQUFhLEVBeEVQOztBQTBFTkMsdUJBQW9CLEVBMUVkO0FBMkVOQyw2QkFBMEIsRUEzRXBCO0FBNEVOQyw4QkFBMkIsRUE1RXJCO0FBNkVOQyw4QkFBMkIsRUE3RXJCO0FBOEVOQyxjQUFZLENBOUVOO0FBK0VIQyxhQUFXLENBL0VSO0FBZ0ZIQyxZQUFXLENBaEZSO0FBaUZOQyxnQkFBZSxLQWpGVDs7QUFtRk5DLHFCQUFrQixNQW5GWjtBQW9GTkMsV0FBUTtBQXBGRixFQUxVOztBQTRGakJDLFFBNUZpQixxQkE0RlA7QUFBQTs7QUFDTkMsSUFBRSxxQkFBRixFQUF5QkMsRUFBekIsQ0FBNEIsZ0JBQTVCLEVBQThDLFlBQU07QUFDaEQsU0FBS04sUUFBTCxHQUFnQixDQUFoQjtBQUNBTyxTQUFNRixFQUFFLG9FQUFtRSxNQUFLVix5QkFBeEUsR0FBbUcsSUFBckcsRUFBMkdXLEVBQTNHLENBQThHLE1BQTlHLEVBQXNILFlBQU07QUFDaEksUUFBSUUsWUFBWUgsRUFBRSxlQUFjLE1BQUtWLHlCQUFuQixHQUE4QywwQ0FBaEQsQ0FBaEI7QUFDQWEsY0FBVUMsUUFBVixDQUFtQixnQkFBbkI7QUFDQSxVQUFLWCxVQUFMLEdBQWtCVSxVQUFVRSxNQUFWLEVBQWxCO0FBQ0EsVUFBS1gsU0FBTCxHQUFpQlMsVUFBVUcsS0FBVixFQUFqQjs7QUFHQUgsY0FBVUksU0FBVjtBQUVELElBVEssQ0FBTjtBQVVILEdBWkQ7O0FBY0FQLElBQUUscUJBQUYsRUFBeUJDLEVBQXpCLENBQTRCLGVBQTVCLEVBQTZDLFlBQU07QUFDL0NELEtBQUUsZ0JBQUYsRUFBb0JRLEtBQXBCO0FBQ0EsU0FBS1osWUFBTCxHQUFvQixLQUFwQjtBQUNILEdBSEQ7QUFJRixFQS9HZTs7O0FBaUhqQmEsUUFBTTtBQUNMZCxZQUFTLGtCQUFTZSxRQUFULEVBQWtCO0FBQzFCLE9BQUlDLG9CQUFxQixLQUFLbEIsVUFBTCxHQUFrQmlCLFFBQTNDO0FBQ0EsT0FBSUUsbUJBQW1CLEtBQUtsQixTQUFMLEdBQWlCZ0IsUUFBeEM7QUFDQVYsS0FBRSxnQkFBRixFQUFvQmEsSUFBcEIsQ0FBeUIsS0FBekIsRUFBZ0NDLEdBQWhDLENBQW9DLFFBQXBDLEVBQTZDSCxpQkFBN0M7QUFDQVgsS0FBRSxnQkFBRixFQUFvQmEsSUFBcEIsQ0FBeUIsS0FBekIsRUFBZ0NDLEdBQWhDLENBQW9DLE9BQXBDLEVBQTRDRixnQkFBNUM7QUFDQVosS0FBRSxnQkFBRixFQUFvQmEsSUFBcEIsQ0FBeUIsS0FBekIsRUFBZ0NFLFdBQWhDLENBQTRDLGdCQUE1QztBQUNBLEdBUEk7O0FBU0x6Qiw2QkFBMEIscUNBQVU7QUFDbkMsUUFBS0ssUUFBTCxHQUFnQixDQUFoQjtBQUNBLEdBWEk7O0FBYUw1QixnQkFBYSx3QkFBVTtBQUN0QmlDLEtBQUUsNEJBQUYsRUFBZ0NnQixTQUFoQyxHQUE0Q0MsT0FBNUM7O0FBRUFDLGNBQVcsWUFBVTtBQUNwQkMsbUJBQWVuQixFQUFFLDRCQUFGLEVBQWdDZ0IsU0FBaEMsQ0FBMEM7QUFDbERJLGlCQUFZLEVBRHNDO0FBRWxEQyxpQkFBWTtBQUZzQyxLQUExQyxDQUFmO0FBSUEsSUFMRCxFQUtFLEdBTEY7QUFNQTs7QUF0QkksRUFqSFc7O0FBMklqQkMsUUEzSWlCLHFCQTJJUjtBQUFBOztBQUNSQyxRQUFNQyxNQUFOLENBQWEsNkJBQWIsRUFBMkMsVUFBQ0MsTUFBRCxFQUFZOztBQUV0RCxPQUFHQSxNQUFILEVBQ0E7QUFDQ0MsV0FBT0MsT0FBUCxDQUFlLE9BQUs5RSxJQUFMLENBQVUsbUJBQVYsQ0FBZixFQUE4QyxPQUFLQSxJQUFMLENBQVUsZ0JBQVYsQ0FBOUM7QUFDQSxJQUhELE1BSUE7QUFDQzZFLFdBQU9FLEtBQVAsQ0FBYSxPQUFLL0UsSUFBTCxDQUFVLFdBQVYsQ0FBYixFQUFvQyxPQUFLQSxJQUFMLENBQVUsZUFBVixDQUFwQztBQUNBO0FBQ0RtRCxLQUFFLHFCQUFGLEVBQXlCOUMsS0FBekIsQ0FBK0IsUUFBL0I7QUFDQSxHQVZEO0FBV0EsT0FBS1MsT0FBTCxHQUFlYyxRQUFRb0QsSUFBUixDQUFhQyxFQUE1QjtBQUNBO0FBQ0EsV0FBU0MsY0FBVCxHQUEwQjtBQUN6QixVQUFPQyxNQUFNQyxHQUFOLENBQVUsb0JBQVYsQ0FBUDtBQUNBO0FBQ0QsV0FBU0MsaUJBQVQsR0FBNkI7QUFDNUIsVUFBT0MsV0FBV0YsR0FBWCxDQUFlLFdBQVMsS0FBS3RFLE9BQWQsR0FBc0IsVUFBckMsQ0FBUDtBQUNBO0FBQ0QsV0FBU3lFLGdCQUFULEdBQTRCO0FBQzNCLFVBQU9ELFdBQVdGLEdBQVgsQ0FBZSxZQUFmLENBQVA7QUFDQTtBQUNELFdBQVNJLGVBQVQsR0FBMkI7QUFDMUIsVUFBT0YsV0FBV0YsR0FBWCxDQUFlLFdBQVMsS0FBS3RFLE9BQWQsR0FBc0IsZ0JBQXJDLENBQVA7QUFDQTtBQUNELFdBQVMyRSxlQUFULEdBQTJCO0FBQzFCLFVBQU9ILFdBQVdGLEdBQVgsQ0FBZSxzQkFBZixDQUFQO0FBQ0E7QUFDRCxXQUFTTSxrQkFBVCxHQUE4QjtBQUM3QixVQUFPSixXQUFXRixHQUFYLENBQWUsMkNBQWYsQ0FBUDtBQUNBO0FBQ0QsV0FBU08sa0JBQVQsR0FBOEI7QUFDN0IsVUFBT0wsV0FBV0YsR0FBWCxDQUFlLDJDQUFmLENBQVA7QUFDQTtBQUNELFdBQVNRLGFBQVQsR0FBeUI7QUFDeEIsVUFBT04sV0FBV0YsR0FBWCxDQUFlLHNDQUFmLENBQVA7QUFDQTtBQUNELFdBQVNTLGlCQUFULEdBQTZCO0FBQzVCLFVBQU9QLFdBQVdGLEdBQVgsQ0FBZSxnREFBZixDQUFQO0FBQ0E7QUFDRCxXQUFTVSxRQUFULEdBQW9CO0FBQ25CLFVBQU9SLFdBQVdGLEdBQVgsQ0FBZSxRQUFmLENBQVA7QUFDQTtBQUNELFdBQVNXLE9BQVQsR0FBbUI7QUFDbEIsVUFBT1QsV0FBV0YsR0FBWCxDQUFlLFlBQVV4RCxRQUFRb0QsSUFBUixDQUFhQyxFQUF2QixHQUEwQixZQUF6QyxDQUFQO0FBQ0E7O0FBRURFLFFBQU1hLEdBQU4sQ0FBVSxDQUNSZCxnQkFEUSxFQUVSTyxpQkFGUSxFQUdSSixtQkFIUSxFQUlSRSxrQkFKUSxFQUtSQyxpQkFMUSxFQU1STSxVQU5RLEVBT1JDLFNBUFEsRUFRUkwsb0JBUlEsRUFTUkMsb0JBVFEsRUFVUkMsZUFWUSxFQVdSQyxtQkFYUSxDQUFWLEVBWUdJLElBWkgsQ0FZUWQsTUFBTWUsTUFBTixDQUNQLFVBQ0VDLFFBREYsRUFFRWpGLFlBRkYsRUFHRWtGLE9BSEYsRUFJRUMsTUFKRixFQUtFcEYsYUFMRixFQU1FRSxLQU5GLEVBT0VtRixTQVBGLEVBUUVDLHNCQVJGLEVBU0VDLHNCQVRGLEVBVUVDLGlCQVZGLEVBV0VDLHFCQVhGLEVBWUs7O0FBRUwsVUFBSzFHLElBQUwsR0FBWW1HLFNBQVNyRyxJQUFULENBQWNBLElBQTFCO0FBQ0EsVUFBS0csSUFBTCxDQUFVLENBQVYsRUFBYUUsS0FBYixHQUFxQixPQUFLSCxJQUFMLENBQVUsWUFBVixDQUFyQjtBQUNBLFVBQUtDLElBQUwsQ0FBVSxDQUFWLEVBQWFFLEtBQWIsR0FBcUIsT0FBS0gsSUFBTCxDQUFVLGdCQUFWLENBQXJCO0FBQ0EsVUFBS0MsSUFBTCxDQUFVLENBQVYsRUFBYUUsS0FBYixHQUFxQixPQUFLSCxJQUFMLENBQVUscUJBQVYsQ0FBckI7QUFDQSxVQUFLQyxJQUFMLENBQVUsQ0FBVixFQUFhRSxLQUFiLEdBQXFCLE9BQUtILElBQUwsQ0FBVSxjQUFWLENBQXJCOztBQUVBLFVBQUtDLElBQUwsQ0FBVSxDQUFWLEVBQWFHLFFBQWIsR0FBd0IsT0FBS0osSUFBTCxDQUFVLG1CQUFWLENBQXhCO0FBQ0EsVUFBS0MsSUFBTCxDQUFVLENBQVYsRUFBYUcsUUFBYixHQUF3QixPQUFLSixJQUFMLENBQVUsZ0JBQVYsQ0FBeEI7QUFDQSxVQUFLQyxJQUFMLENBQVUsQ0FBVixFQUFhRyxRQUFiLEdBQXdCLE9BQUtKLElBQUwsQ0FBVSxpQkFBVixDQUF4QjtBQUNBLFVBQUtDLElBQUwsQ0FBVSxDQUFWLEVBQWFHLFFBQWIsR0FBd0IsT0FBS0osSUFBTCxDQUFVLGFBQVYsQ0FBeEI7O0FBRUEsVUFBS2UsZUFBTCxHQUF1QnFGLFFBQVF0RyxJQUFSLENBQWFnRixPQUFiLEdBQXNCNkIsV0FBV1AsUUFBUXRHLElBQVIsQ0FBYUEsSUFBYixDQUFrQm1DLE1BQTdCLENBQXRCLEdBQTJELENBQWxGO0FBQ0EsVUFBS2pCLGNBQUwsR0FBc0JxRixPQUFPdkcsSUFBUCxDQUFZZ0YsT0FBWixHQUFxQjZCLFdBQVdOLE9BQU92RyxJQUFQLENBQVlBLElBQVosQ0FBaUJ1RyxNQUE1QixDQUFyQixHQUF5RCxDQUEvRTtBQUNBLFVBQUtwRixhQUFMLEdBQXFCQSxjQUFjbkIsSUFBZCxDQUFtQmdGLE9BQW5CLEdBQTRCN0QsY0FBY25CLElBQWQsQ0FBbUJBLElBQW5CLENBQXdCbUIsYUFBcEQsR0FBa0UsS0FBdkY7QUFDQSxVQUFLRSxLQUFMLEdBQWFBLE1BQU1yQixJQUFOLENBQVdnRixPQUFYLEdBQW9CM0QsTUFBTXJCLElBQU4sQ0FBV0EsSUFBL0IsR0FBb0MsS0FBakQ7QUFDQSxVQUFLb0IsWUFBTCxHQUFvQkEsYUFBYTBGLE1BQWIsSUFBcUIsR0FBckIsR0FBMEIxRixhQUFhcEIsSUFBdkMsR0FBNEMsS0FBaEU7O0FBRUEsT0FBR3lHLHVCQUF1QkssTUFBdkIsSUFBaUMsR0FBcEMsRUFDQTtBQUNDLFdBQUsxRixZQUFMLEdBQW9CLE9BQUtBLFlBQUwsQ0FBa0IyRixNQUFsQixDQUF5Qk4sdUJBQXVCekcsSUFBaEQsQ0FBcEI7QUFDQTs7QUFFRCxPQUFHMEcsdUJBQXVCSSxNQUF2QixJQUFpQyxHQUFwQyxFQUNBO0FBQ0MsV0FBSzFGLFlBQUwsR0FBb0IsT0FBS0EsWUFBTCxDQUFrQjJGLE1BQWxCLENBQXlCTCx1QkFBdUIxRyxJQUFoRCxDQUFwQjtBQUNBOztBQUVELE9BQUcyRyxrQkFBa0JHLE1BQWxCLElBQTRCLEdBQS9CLEVBQ0E7QUFDQyxXQUFLMUYsWUFBTCxHQUFvQixPQUFLQSxZQUFMLENBQWtCMkYsTUFBbEIsQ0FBeUJKLGtCQUFrQjNHLElBQTNDLENBQXBCO0FBQ0E7O0FBRUQsT0FBRzRHLHNCQUFzQkUsTUFBdEIsSUFBZ0MsR0FBbkMsRUFDQTtBQUNDLFdBQUsxRixZQUFMLEdBQW9CLE9BQUtBLFlBQUwsQ0FBa0IyRixNQUFsQixDQUF5Qkgsc0JBQXNCNUcsSUFBL0MsQ0FBcEI7QUFDQTs7QUFFRCxPQUFHd0csVUFBVU0sTUFBVixJQUFrQixHQUFyQixFQUNBO0FBQ0MsUUFBR04sVUFBVXhHLElBQVYsQ0FBZWdILE1BQWxCLEVBQ0E7QUFDQyxTQUFJQyxhQUFhLENBQWpCO0FBQ1NBLG1CQUFpQlQsVUFBVXhHLElBQVYsQ0FBZWtILE1BQWYsQ0FBc0IsZUFBTztBQUFFLGFBQU9DLElBQUk5RSxJQUFKLEtBQWEsS0FBYixJQUFzQjhFLElBQUlMLE1BQUosS0FBZSxVQUE1QztBQUF3RCxNQUF2RixFQUF5RkUsTUFBMUc7O0FBRUFDLG1CQUFpQlQsVUFBVXhHLElBQVYsQ0FBZWtILE1BQWYsQ0FBc0IsZUFBTztBQUFFLGFBQU9DLElBQUk5RSxJQUFKLEtBQWEsS0FBYixJQUFzQjhFLElBQUlMLE1BQUosS0FBZSxVQUE1QztBQUF3RCxNQUF2RixFQUF5RkUsTUFBMUc7O0FBRUFDLG1CQUFpQlQsVUFBVXhHLElBQVYsQ0FBZWtILE1BQWYsQ0FBc0IsZUFBTztBQUFFLGFBQU9DLElBQUk5RSxJQUFKLEtBQWEsUUFBYixJQUF5QjhFLElBQUlMLE1BQUosS0FBZSxVQUEvQztBQUEyRCxNQUExRixFQUE0RkUsTUFBN0c7QUFDVCxTQUFHQyxVQUFILEVBQ0E7QUFDQyxhQUFLL0QsaUJBQUwsR0FBeUIsVUFBVSxPQUFLakMsZUFBeEM7QUFDQSxNQUhELE1BSUE7QUFDQyxhQUFLaUMsaUJBQUwsR0FBeUIsVUFBVSxPQUFLakMsZUFBeEM7QUFDQTtBQUVEO0FBQ0Q7O0FBRUQzQixPQUFJOEgsUUFBSixDQUFhLFlBQVk7QUFDeEI3QyxlQUFXLFlBQVU7QUFDWDtBQUNBbEIsT0FBRSxzREFBRixFQUEwRGdFLElBQTFELENBQStELFlBQVU7QUFDeEVoRSxRQUFFLElBQUYsRUFBUWlFLElBQVIsQ0FBYSxxQkFBYixFQUFtQ2pFLEVBQUUsSUFBRixFQUFRaUUsSUFBUixDQUFhLGlCQUFiLENBQW5DO0FBQ0EsTUFGRDtBQUdBakUsT0FBRSx5QkFBRixFQUE2QmtFO0FBQzdCO0FBREEsUUFFQWxFLEVBQUUsNEJBQUYsRUFBZ0NtRSxTQUFoQztBQUNULEtBUkQsRUFRRSxHQVJGO0FBU00sSUFWUDtBQVdBLEdBcEZPLENBWlIsRUFpR0NDLEtBakdELENBaUdPcEUsRUFBRXFFLElBakdUO0FBa0dBLEVBN1JnQjs7O0FBK1JqQkMsVUFBUztBQUNSQyxlQURRLHlCQUNNdkYsSUFETixFQUNXO0FBQ2xCLFdBQU9BLElBQVA7QUFFQyxTQUFLLFVBQUw7QUFDQSxTQUFLLFVBQUw7QUFDQSxTQUFLLFVBQUw7QUFDQSxTQUFLLDJCQUFMO0FBQ0MsWUFBTyxhQUFQO0FBQ0QsU0FBSyxTQUFMO0FBQ0EsU0FBSyxVQUFMO0FBQ0EsU0FBSyxpQkFBTDtBQUNBLFNBQUssZ0JBQUw7QUFDQSxTQUFLLE1BQUw7QUFDQyxZQUFPLGNBQVA7O0FBWkY7QUFlQSxHQWpCTztBQW1CUndGLHVCQW5CUSxpQ0FtQmNmLE1BbkJkLEVBbUJxQnpFLElBbkJyQixFQW1CMEJ5RixNQW5CMUIsRUFtQmlDO0FBQ3hDLE9BQUdoQixVQUFVLFdBQWIsRUFDQTtBQUNDLFdBQU9nQixNQUFQO0FBQ0EsSUFIRCxNQUdNLElBQUloQixVQUFTLFNBQVYsSUFBdUJ6RSxRQUFRLFNBQWxDLEVBQTZDO0FBQ2xELFdBQU8saUNBQVA7QUFDQTtBQUNELFVBQU8sRUFBUDtBQUNBLEdBM0JPO0FBNkJSMEYsWUE3QlEsc0JBNkJHQyxDQTdCSCxFQTZCTTtBQUNULE9BQUlDLFFBQVFELEVBQUVFLE1BQUYsQ0FBU0QsS0FBVCxJQUFrQkQsRUFBRUcsWUFBRixDQUFlRixLQUE3QztBQUNBLE9BQUksQ0FBQ0EsTUFBTWpCLE1BQVgsRUFDRTtBQUNGLFFBQUtvQixXQUFMLENBQWlCSCxNQUFNLENBQU4sQ0FBakI7QUFDRCxHQWxDSTtBQW9DTEcsYUFwQ0ssdUJBb0NPQyxJQXBDUCxFQW9DYTtBQUFBOztBQUNoQixPQUFJQyxRQUFRLElBQUlDLEtBQUosRUFBWjtBQUNBLE9BQUlDLFNBQVMsSUFBSUMsVUFBSixFQUFiO0FBQ0EsT0FBSUMsS0FBSyxJQUFUOztBQUVBRixVQUFPRyxNQUFQLEdBQWdCLFVBQUNYLENBQUQsRUFBTztBQUNyQjtBQUNBM0UsTUFBRSxnQkFBRixFQUFvQlEsS0FBcEI7QUFDQSxXQUFLYixRQUFMLEdBQWdCLENBQWhCO0FBQ0FPLFVBQU1GLEVBQUUsb0VBQW9FMkUsRUFBRUUsTUFBRixDQUFTcEQsTUFBN0UsR0FBcUYsSUFBdkYsRUFBNkZ4QixFQUE3RixDQUFnRyxNQUFoRyxFQUF3RyxZQUFNO0FBQ2xILFNBQUlFLFlBQVlILEVBQUUsZUFBZTJFLEVBQUVFLE1BQUYsQ0FBU3BELE1BQXhCLEdBQWdDLHlDQUFsQyxDQUFoQjtBQUNBdEIsZUFBVUMsUUFBVixDQUFtQixnQkFBbkI7QUFDQSxZQUFLWCxVQUFMLEdBQWtCVSxVQUFVRSxNQUFWLEVBQWxCO0FBQ0EsWUFBS1gsU0FBTCxHQUFpQlMsVUFBVUcsS0FBVixFQUFqQjtBQUNOLFlBQUtWLFlBQUwsR0FBb0IsSUFBcEI7O0FBRU1PLGVBQVVJLFNBQVY7QUFFRCxLQVRLLENBQU47QUFVRCxJQWREO0FBZUE0RSxVQUFPSSxhQUFQLENBQXFCUCxJQUFyQjtBQUNELEdBekRJOzs7QUEyRExRLGVBQWEscUJBQVViLENBQVYsRUFBYTtBQUN4QixRQUFLTSxLQUFMLEdBQWEsRUFBYjtBQUNELEdBN0RJOztBQStEUlEsd0JBL0RRLGtDQStEZUMsY0EvRGYsRUErRDhCakMsTUEvRDlCLEVBK0RxQ2dCLE1BL0RyQyxFQStENENrQixVQS9ENUMsRUErRHVEO0FBQUE7O0FBRTlELFFBQUt0RyxtQkFBTCxHQUEyQnFHLGNBQTNCO0FBQ0EsUUFBS25HLDBCQUFMLEdBQWtDa0UsTUFBbEM7QUFDQSxRQUFLbkUseUJBQUwsR0FBaUMsS0FBakM7QUFDQSxRQUFLRSwwQkFBTCxHQUFrQ2lGLE1BQWxDOztBQUVBekMsU0FBTUMsR0FBTixDQUFVLG1DQUFpQzBELFVBQWpDLEdBQTRDLE1BQXRELEVBQ0U3QyxJQURGLENBQ08sa0JBQVU7QUFDZixXQUFLeEQseUJBQUwsR0FBaUMsbUNBQWlDcUcsVUFBakMsR0FBNEMsTUFBN0U7QUFDQSxXQUFLL0YsWUFBTCxHQUFvQixJQUFwQjtBQUNBLElBSkYsRUFLRXdFLEtBTEYsQ0FLUXBFLEVBQUVxRSxJQUxWO0FBTUFyRSxLQUFFLHFCQUFGLEVBQXlCOUMsS0FBekIsQ0FBK0IsUUFBL0I7QUFDQSxHQTdFTzs7O0FBK0VSO0FBQ0EwSSxXQWhGUSxxQkFnRkdDLEtBaEZILEVBZ0ZVO0FBQ2IsbUJBQWNBLEtBQWQ7QUFDRCxHQWxGSTs7O0FBb0ZSQyxnQkFBYyx3QkFBVTtBQUN2QixRQUFLM0ksa0JBQUwsR0FBMEIsS0FBMUI7QUFDQSxRQUFLRSxlQUFMLEdBQXVCLEtBQXZCO0FBQ0EsUUFBS0QsY0FBTCxHQUFzQixJQUF0QjtBQUNBLFFBQUtFLGVBQUwsR0FBdUIsSUFBdkI7QUFFQSxHQTFGTzs7QUE0RlJ5SSxpQkFBZSx5QkFBVTtBQUN4QixRQUFLNUksa0JBQUwsR0FBMEIsS0FBMUI7QUFDQSxRQUFLQyxjQUFMLEdBQXNCLEtBQXRCO0FBQ0EsUUFBS0MsZUFBTCxHQUF1QixJQUF2QjtBQUNBLFFBQUtDLGVBQUwsR0FBdUIsSUFBdkI7QUFFQSxHQWxHTztBQW1HUjtBQUNBMEksa0JBQWdCLDBCQUFVO0FBQUE7O0FBQ3pCN0QsY0FDRUYsR0FERixDQUNNLFVBRE4sRUFFRWEsSUFGRixDQUVPLGtCQUFVO0FBQ2YsUUFBSWhELFVBQVUsRUFBZDtBQUNBLFNBQUksSUFBSW1HLElBQUUsQ0FBVixFQUFhQSxJQUFFeEUsT0FBTzlFLElBQVAsQ0FBWW1ELE9BQVosQ0FBb0I2RCxNQUFuQyxFQUEyQ3NDLEdBQTNDLEVBQ0E7QUFDQ25HLGFBQVFvRyxJQUFSLENBQWF6RSxPQUFPOUUsSUFBUCxDQUFZbUQsT0FBWixDQUFvQm1HLENBQXBCLEVBQXVCcEUsSUFBcEM7QUFDQTtBQUNELFdBQUsvQixPQUFMLEdBQWVBLE9BQWY7QUFDQSxXQUFLN0IsZUFBTCxHQUF1QjZCLE9BQXZCO0FBQ0EsSUFWRjs7QUFZQSxRQUFLdkMsMEJBQUwsR0FBa0MsS0FBbEM7QUFDQSxRQUFLRSxlQUFMLEdBQXVCLEtBQXZCO0FBQ0EsUUFBS0QsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDQSxRQUFLRSx1QkFBTCxHQUErQixJQUEvQjtBQUNBLEdBckhPOztBQXVIUnlJLGlCQUFlLHlCQUFVO0FBQ3hCLFFBQUs1SSwwQkFBTCxHQUFrQyxLQUFsQztBQUNBLFFBQUtDLGdCQUFMLEdBQXdCLEtBQXhCO0FBQ0EsUUFBS0MsZUFBTCxHQUF1QixJQUF2QjtBQUNBLFFBQUtDLHVCQUFMLEdBQStCLElBQS9CO0FBQ0EsR0E1SE87O0FBOEhMMEksYUFBVyxtQkFBVWxKLEtBQVYsRUFBaUI7QUFDOUJtSjtBQUNHckcsS0FBRSxNQUFJOUMsS0FBTixFQUFhQSxLQUFiLENBQW1CLFFBQW5CO0FBQ0EsR0FqSUk7O0FBbUlMbUosZUFBYSx1QkFBVTtBQUN0QixRQUFLbEosa0JBQUwsR0FBMEIsSUFBMUI7QUFDQSxRQUFLQyxjQUFMLEdBQXNCLEtBQXRCO0FBQ0EsUUFBS0MsZUFBTCxHQUF1QixLQUF2QjtBQUNILFFBQUtDLGVBQUwsR0FBdUIsS0FBdkI7O0FBRUcsUUFBS0MsMEJBQUwsR0FBa0MsSUFBbEM7QUFDSCxRQUFLQyxnQkFBTCxHQUF3QixLQUF4QjtBQUNBLFFBQUtDLGVBQUwsR0FBdUIsS0FBdkI7QUFDQSxRQUFLQyx1QkFBTCxHQUErQixLQUEvQjs7QUFFQSxRQUFLa0IsZUFBTCxHQUF1QixJQUF2QjtBQUNBLFFBQUtWLGVBQUwsQ0FBcUJFLE9BQXJCLEdBQTZCLEVBQTdCO0FBQ0EsUUFBS0YsZUFBTCxDQUFxQkcsWUFBckIsR0FBa0MsSUFBbEM7QUFDQSxRQUFLSCxlQUFMLENBQXFCSSxjQUFyQixHQUFvQyxJQUFwQztBQUNBLFFBQUtKLGVBQUwsQ0FBcUJLLGdCQUFyQixHQUFzQyxJQUF0Qzs7QUFFQSxRQUFLSSxxQkFBTCxHQUE2QixJQUE3Qjs7QUFFQSxRQUFLYSwwQkFBTCxHQUFrQyxFQUFsQzs7QUFFQSxRQUFLdEIsZUFBTCxHQUF1QixJQUFJQyxJQUFKLENBQVM7QUFDL0JDLGFBQVEsRUFEdUI7QUFFL0JDLGtCQUFhLElBRmtCO0FBRy9CQyxvQkFBZSxJQUhnQjtBQUkvQkMsc0JBQWlCO0FBSmMsSUFBVCxFQUtwQixFQUFFQyxTQUFTLFlBQVVDLFFBQVFDLFlBQTdCLEVBTG9CLENBQXZCOztBQU9BLFFBQUtHLGVBQUwsR0FBdUIsSUFBSVYsSUFBSixDQUFTO0FBQy9CQyxhQUFRLEVBRHVCO0FBRS9CVSxZQUFPO0FBRndCLElBQVQsRUFHcEIsRUFBRU4sU0FBUyxZQUFVQyxRQUFRQyxZQUE3QixFQUhvQixDQUF2Qjs7QUFLQSxRQUFLSyxtQkFBTCxHQUEyQixJQUFJWixJQUFKLENBQVM7QUFDbkNDLGFBQVEsRUFEMkI7QUFFbkNVLFlBQU8sSUFGNEI7QUFHbkNFLFVBQUs7QUFIOEIsSUFBVCxFQUl4QixFQUFFUixTQUFTLFlBQVVDLFFBQVFDLFlBQTdCLEVBSndCLENBQTNCOztBQU1BLFFBQUtPLFlBQUwsR0FBb0IsSUFBSWQsSUFBSixDQUFTO0FBQzVCQyxhQUFRLEVBRG9CO0FBRTVCVSxZQUFPLElBRnFCO0FBRzVCRSxVQUFLLFVBSHVCO0FBSTVCRSxVQUFLO0FBSnVCLElBQVQsRUFLakIsRUFBRVYsU0FBUyxZQUFVQyxRQUFRQyxZQUE3QixFQUxpQixDQUFwQjs7QUFPQSxRQUFLWSx5QkFBTCxHQUErQixFQUEvQjtBQUNHLEdBbExJOztBQW9MTGdILGVBcExLLCtCQW9MOEI7QUFBQSxPQUFuQkMsU0FBbUIsUUFBbkJBLFNBQW1CO0FBQUEsT0FBUkMsS0FBUSxRQUFSQSxLQUFROztBQUNsQyxVQUFVRCxTQUFWLFVBQXdCQyxLQUF4QjtBQUNBLEdBdExJO0FBd0xMQyxzQkF4TEssa0NBd0xpQjtBQUFBOztBQUNmLFFBQUt2SSxlQUFMLENBQXFCd0ksTUFBckIsQ0FBNEIsTUFBNUIsRUFBb0MsY0FBWSxLQUFLL0ksT0FBakIsR0FBeUIsZ0JBQTdELEVBQ0ttRixJQURMLENBQ1Usa0JBQVU7QUFDWixRQUFHckIsT0FBT0UsT0FBVixFQUNBO0FBQ0MsWUFBSzdELGFBQUwsQ0FBbUI2SSxPQUFuQixDQUEyQmxGLE9BQU85RSxJQUFsQztBQUNBK0UsWUFBT0MsT0FBUCxDQUFlLE9BQUs5RSxJQUFMLENBQVUsZUFBVixDQUFmO0FBQ0EsWUFBS3FCLGVBQUwsQ0FBcUJFLE9BQXJCLEdBQTZCLEVBQTdCO0FBQ2YsWUFBS0YsZUFBTCxDQUFxQkcsWUFBckIsR0FBa0MsSUFBbEM7QUFDQSxZQUFLSCxlQUFMLENBQXFCSSxjQUFyQixHQUFvQyxJQUFwQztBQUNBLFlBQUtKLGVBQUwsQ0FBcUJLLGdCQUFyQixHQUFzQyxJQUF0QztBQUNlLEtBUkQsTUFTQTtBQUNDbUQsWUFBT0UsS0FBUCxDQUFhLE9BQUsvRSxJQUFMLENBQVUsV0FBVixDQUFiO0FBQ0E7QUFDSixJQWRMLEVBZUt1SCxLQWZMLENBZVdwRSxFQUFFcUUsSUFmYjtBQWdCTixHQXpNSTtBQTJNTHVDLHdCQTNNSyxvQ0EyTW1CO0FBQ3ZCLFFBQUtoSSxlQUFMLEdBQXVCLElBQXZCO0FBQ0gsUUFBS1YsZUFBTCxDQUFxQkUsT0FBckIsR0FBNkIsRUFBN0I7QUFDQSxRQUFLRixlQUFMLENBQXFCRyxZQUFyQixHQUFrQyxJQUFsQztBQUNBLFFBQUtILGVBQUwsQ0FBcUJJLGNBQXJCLEdBQW9DLElBQXBDO0FBQ0EsUUFBS0osZUFBTCxDQUFxQkssZ0JBQXJCLEdBQXNDLElBQXRDOztBQUVBLFFBQUtJLHFCQUFMLEdBQTZCLElBQTdCO0FBQ0csR0FuTkk7QUFxTkxrSSxtQkFyTkssK0JBcU5jO0FBQUE7O0FBQ2xCLFlBQVNDLGVBQVQsR0FBMkI7QUFDN0IsV0FBTzNFLFdBQVc0RSxHQUFYLENBQWUsV0FBU3RJLFFBQVFvRCxJQUFSLENBQWFDLEVBQXRCLEdBQXlCLGlCQUF6QixHQUEyQyxLQUFLbkQscUJBQS9ELENBQVA7QUFDQTs7QUFFRCxRQUFLVCxlQUFMLENBQXFCd0ksTUFBckIsQ0FBNEIsS0FBNUIsRUFBbUMsY0FBWWpJLFFBQVFvRCxJQUFSLENBQWFDLEVBQXpCLEdBQTRCLGlCQUE1QixHQUE4QyxLQUFLbkQscUJBQXRGLEVBQ2NtRSxJQURkLENBQ21CLGtCQUFVO0FBQ1osUUFBR3JCLE9BQU9FLE9BQVYsRUFDQTtBQUNDLFVBQUlzRSxJQUFFLENBQU4sRUFBU0EsSUFBRyxPQUFLbkksYUFBTCxDQUFtQjZGLE1BQS9CLEVBQXVDc0MsR0FBdkMsRUFDQTtBQUNDLFVBQUcsT0FBS25JLGFBQUwsQ0FBbUJtSSxDQUFuQixFQUFzQm5FLEVBQXRCLElBQTRCLE9BQUtuRCxxQkFBcEMsRUFDQTtBQUNGLGNBQUtiLGFBQUwsQ0FBbUJtSSxDQUFuQixJQUF3QnhFLE9BQU85RSxJQUEvQjtBQUNHO0FBQ0E7QUFDRDtBQUNEK0UsWUFBT0MsT0FBUCxDQUFlLE9BQUs5RSxJQUFMLENBQVUsaUJBQVYsQ0FBZjtBQUNBLEtBWEQsTUFZQTtBQUNDNkUsWUFBT0UsS0FBUCxDQUFhLE9BQUsvRSxJQUFMLENBQVUsV0FBVixDQUFiO0FBQ0E7QUFDSixJQWpCZCxFQWtCY3VILEtBbEJkLENBa0JvQnBFLEVBQUVxRSxJQWxCdEI7QUFtQkcsR0E3T0k7QUErT0wyQyxlQS9PSyx5QkErT1NSLEtBL09ULEVBK09nQjtBQUNwQixPQUFJUyxLQUFLLHdKQUFUO0FBQ0EsVUFBT0EsR0FBR0MsSUFBSCxDQUFRVixLQUFSLENBQVA7QUFDSCxHQWxQTztBQW9QUlcsa0JBcFBRLDRCQW9QU0MsYUFwUFQsRUFvUHdCQyxJQXBQeEIsRUFvUDhCO0FBQ2pDLE9BQUlDLFdBQVcsRUFBZjtBQUNBLE9BQUlDLGVBQWdCLEVBQXBCOztBQUVBLFFBQUksSUFBSXRCLENBQVIsSUFBYW1CLGFBQWIsRUFBNEI7QUFDekJHLGlCQUFhSCxjQUFjbkIsQ0FBZCxFQUFpQm9CLElBQWpCLENBQWIsSUFBdUNELGNBQWNuQixDQUFkLENBQXZDO0FBQ0Y7O0FBRUQsUUFBSUEsQ0FBSixJQUFTc0IsWUFBVCxFQUF1QjtBQUNuQkQsYUFBU3BCLElBQVQsQ0FBY3FCLGFBQWF0QixDQUFiLENBQWQ7QUFDSDtBQUNBLFVBQU9xQixRQUFQO0FBQ0osR0FoUU07QUFrUUxFLHNCQWxRSyxnQ0FrUWdCQyxJQWxRaEIsRUFrUXFCO0FBQUE7O0FBQ3pCLE9BQUlDLE9BQUo7O0FBRUgsT0FBSTVILFVBQVUsSUFBZDtBQUNBO0FBQ0FBLGFBQVVFLEVBQUUySCxNQUFGLENBQVM3SCxPQUFULEVBQWlCLEtBQUtBLE9BQXRCLENBQVY7QUFDQTtBQUNBQSxhQUFVRSxFQUFFNEgsR0FBRixDQUFNOUgsT0FBTixFQUFlLFVBQVN6RCxFQUFULEVBQWE7QUFBRSxXQUFPQSxFQUFQO0FBQVcsSUFBekMsQ0FBVjs7QUFFRyxPQUFHLEtBQUsySyxhQUFMLENBQW1CUyxJQUFuQixDQUFILEVBQ0E7QUFDRixTQUFLdEksa0JBQUwsR0FBMEIsSUFBMUI7QUFDRztBQUNILFNBQUtsQixlQUFMLEdBQXVCNkIsT0FBdkI7O0FBR0FxQyxlQUNFRixHQURGLENBQ00sMEJBQXdCd0YsSUFEOUIsRUFFRTNFLElBRkYsQ0FFTyxvQkFBWTtBQUNqQixTQUFHK0UsU0FBU2xMLElBQVQsQ0FBY2dILE1BQWpCLEVBQ0E7QUFDQyxhQUFLMUYsZUFBTCxHQUF1QixPQUFLQSxlQUFMLENBQXFCeUYsTUFBckIsQ0FBNEJtRSxTQUFTbEwsSUFBckMsQ0FBdkI7QUFDQSxhQUFLc0IsZUFBTCxHQUF1QixPQUFLa0osZ0JBQUwsQ0FBc0IsT0FBS2xKLGVBQTNCLEVBQTJDLE9BQTNDLENBQXZCO0FBQ0EsTUFKRCxNQUtBO0FBQ0ksYUFBS0EsZUFBTCxHQUF1QjZCLE9BQXZCO0FBQ0g7QUFDRCxZQUFLWCxrQkFBTCxHQUEwQixLQUExQjtBQUNBLEtBWkYsRUFhRWlGLEtBYkYsQ0FhUSxpQkFBUztBQUNmLFlBQUtuRyxlQUFMLEdBQXVCNkIsT0FBdkI7QUFDQSxLQWZGO0FBaUJHLElBeEJELE1BeUJBO0FBQ0MsU0FBSzdCLGVBQUwsR0FBd0I2QixPQUF4QjtBQUNBO0FBQ0QsR0F2U0k7QUF5U0xnSSxVQXpTSyxvQkF5U0lDLGVBelNKLEVBeVNvQjtBQUFBOztBQUN4QixZQUFTMUYsZUFBVCxDQUF5QlAsRUFBekIsRUFBNkI7QUFDL0IsV0FBT0ssV0FBV0YsR0FBWCxDQUFlLFdBQVN4RCxRQUFRb0QsSUFBUixDQUFhQyxFQUF0QixHQUF5QixpQkFBekIsR0FBMkNBLEVBQTFELENBQVA7QUFDQTtBQUNFRSxTQUFNYSxHQUFOLENBQVUsQ0FDWlIsZ0JBQWdCMEYsZUFBaEIsQ0FEWSxDQUFWLEVBRUFqRixJQUZBLENBRUtkLE1BQU1lLE1BQU4sQ0FDUCxVQUNFaUYsWUFERixFQUVLO0FBQ0wsUUFBR0EsYUFBYXJMLElBQWIsQ0FBa0JnRixPQUFyQixFQUNBO0FBQ0MsWUFBS2hELHFCQUFMLEdBQTZCb0osZUFBN0I7QUFDQSxZQUFLN0osZUFBTCxDQUFxQkUsT0FBckIsR0FBK0I0SixhQUFhckwsSUFBYixDQUFrQkEsSUFBbEIsQ0FBdUJ5QixPQUF0RDtBQUNBLFlBQUtGLGVBQUwsQ0FBcUJHLFlBQXJCLEdBQW9DMkosYUFBYXJMLElBQWIsQ0FBa0JBLElBQWxCLENBQXVCMEIsWUFBM0Q7QUFDQSxZQUFLSCxlQUFMLENBQXFCSSxjQUFyQixHQUFzQzBKLGFBQWFyTCxJQUFiLENBQWtCQSxJQUFsQixDQUF1QjJCLGNBQTdEO0FBQ0EsWUFBS0osZUFBTCxDQUFxQkssZ0JBQXJCLEdBQXdDeUosYUFBYXJMLElBQWIsQ0FBa0JBLElBQWxCLENBQXVCMkIsY0FBL0Q7QUFDQSxZQUFLTSxlQUFMLEdBQXVCLEtBQXZCO0FBRUE7QUFDRCxJQWRPLENBRkwsRUFpQkZ3RixLQWpCRSxDQWlCSXBFLEVBQUVxRSxJQWpCTjtBQWtCQSxHQS9USTtBQWlVTDRELG1CQWpVSywrQkFpVWM7QUFBQTs7QUFDbEIsUUFBS3BKLGVBQUwsQ0FBcUI2SCxNQUFyQixDQUE0QixNQUE1QixFQUFvQyx5QkFBcEMsRUFDVzVELElBRFgsQ0FDZ0Isa0JBQVU7QUFDWixRQUFHckIsT0FBT0UsT0FBVixFQUNBO0FBQ0MsYUFBSzVELFlBQUwsQ0FBa0JtSSxJQUFsQixDQUF1QnpFLE9BQU85RSxJQUE5QjtBQUNBcUQsT0FBRSxZQUFGLEVBQWdCOUMsS0FBaEIsQ0FBc0IsUUFBdEI7QUFDQXdFLFlBQU93RyxJQUFQLENBQVksUUFBS3JMLElBQUwsQ0FBVSxRQUFWLElBQW9CNEUsT0FBTzlFLElBQVAsQ0FBWWdKLFVBQWhDLEdBQTJDLE1BQTNDLEdBQWtELFFBQUs5SSxJQUFMLENBQVUsU0FBVixDQUFsRCxHQUF1RSxNQUF2RSxHQUE4RSxRQUFLQSxJQUFMLENBQVUsTUFBVixDQUE5RSxHQUFnRyxJQUFoRyxHQUFxRzRFLE9BQU85RSxJQUFQLENBQVl3TCxXQUE3SCxFQUEwSSxFQUExSSxFQUE4STtBQUMzSkMsZUFBUyxDQURrSjtBQUUzSkMsdUJBQWlCO0FBRjBJLE1BQTlJO0FBSWYzRyxZQUFPQyxPQUFQLENBQWUsUUFBSzlFLElBQUwsQ0FBVSxpQkFBVixDQUFmOztBQUVBO0FBQ0FtRCxPQUFFLG9CQUFGLEVBQXdCOUMsS0FBeEIsQ0FBOEIsUUFBOUI7QUFDZSxLQVpELE1BYUE7QUFDQ3dFLFlBQU9FLEtBQVAsQ0FBYSxRQUFLL0UsSUFBTCxDQUFVLFdBQVYsQ0FBYjtBQUNBO0FBQ0osSUFsQlgsRUFtQld1SCxLQW5CWCxDQW1CaUJwRSxFQUFFcUUsSUFuQm5CO0FBb0JBLEdBdFZJO0FBd1ZMaUUsdUJBeFZLLG1DQXdWa0I7QUFBQTs7QUFDdEIsUUFBS3ZKLG1CQUFMLENBQXlCMkgsTUFBekIsQ0FBZ0MsTUFBaEMsRUFBd0MseUJBQXhDLEVBQ1c1RCxJQURYLENBQ2dCLGtCQUFVO0FBQ1osUUFBR3JCLE9BQU9FLE9BQVYsRUFDQTtBQUNDLGFBQUs1RCxZQUFMLENBQWtCbUksSUFBbEIsQ0FBdUJ6RSxPQUFPOUUsSUFBOUI7QUFDQXFELE9BQUUsZ0JBQUYsRUFBb0I5QyxLQUFwQixDQUEwQixRQUExQjtBQUNBd0UsWUFBT3dHLElBQVAsQ0FBWSxRQUFLckwsSUFBTCxDQUFVLFFBQVYsSUFBb0I0RSxPQUFPOUUsSUFBUCxDQUFZZ0osVUFBaEMsR0FBMkMsTUFBM0MsR0FBa0QsUUFBSzlJLElBQUwsQ0FBVSxVQUFWLENBQWxELEdBQXdFLE1BQXhFLEdBQStFLFFBQUtBLElBQUwsQ0FBVSxNQUFWLENBQS9FLEdBQWlHLElBQWpHLEdBQXNHNEUsT0FBTzlFLElBQVAsQ0FBWXdMLFdBQTlILEVBQTJJLEVBQTNJLEVBQStJO0FBQzVKQyxlQUFTLENBRG1KO0FBRTVKQyx1QkFBaUI7QUFGMkksTUFBL0k7QUFJZjNHLFlBQU9DLE9BQVAsQ0FBZSxRQUFLOUUsSUFBTCxDQUFVLGlCQUFWLENBQWY7QUFDZSxLQVRELE1BVUE7QUFDQzZFLFlBQU9FLEtBQVAsQ0FBYSxRQUFLL0UsSUFBTCxDQUFVLFdBQVYsQ0FBYjtBQUNBO0FBQ0osSUFmWCxFQWdCV3VILEtBaEJYLENBZ0JpQnBFLEVBQUVxRSxJQWhCbkI7QUFpQkEsR0ExV0k7QUE0V0xrRSxnQkE1V0ssNEJBNFdXO0FBQUE7O0FBQ2YsT0FBRyxLQUFLM0wsVUFBTCxDQUFnQitHLE1BQW5CLEVBQ0E7QUFDQyxTQUFLL0csVUFBTCxDQUFnQjRMLE9BQWhCLENBQXdCLFVBQUNDLFdBQUQsRUFBZ0I7QUFDdkMsYUFBS3hKLFlBQUwsQ0FBa0JiLE9BQWxCLEdBQTRCcUssWUFBWTNHLEVBQXhDO0FBQ0g7O0FBRUEsYUFBSzdDLFlBQUwsQ0FBa0J5SCxNQUFsQixDQUF5QixNQUF6QixFQUFpQyx5QkFBakMsRUFDYzVELElBRGQsQ0FDbUIsa0JBQVU7O0FBRVosY0FBSzdFLGVBQUwsR0FBdUIsRUFBdkI7O0FBRUEsVUFBR3dELE9BQU9FLE9BQVYsRUFDQTtBQUNDLGVBQUs1RCxZQUFMLENBQWtCbUksSUFBbEIsQ0FBdUJ6RSxPQUFPOUUsSUFBOUI7QUFDQStFLGNBQU93RyxJQUFQLENBQVksUUFBS3JMLElBQUwsQ0FBVSxNQUFWLElBQWtCLElBQWxCLEdBQXVCNEUsT0FBTzlFLElBQVAsQ0FBWWdKLFVBQW5DLEdBQThDLE1BQTlDLEdBQXFELFFBQUs5SSxJQUFMLENBQVUsVUFBVixDQUFyRCxHQUEyRSxPQUEzRSxHQUFtRjRFLE9BQU85RSxJQUFQLENBQVl3TCxXQUEzRyxFQUF3SCxFQUF4SCxFQUE0SDtBQUN6SUMsaUJBQVMsQ0FEZ0k7QUFFeklDLHlCQUFpQjtBQUZ3SCxRQUE1SDtBQUlBLE9BUEQsTUFRQTs7QUFFQzNHLGNBQU9FLEtBQVAsQ0FBYSxRQUFLL0UsSUFBTCxDQUFVLFdBQVYsQ0FBYjtBQUNBO0FBQ0QsY0FBS29CLGVBQUwsR0FBdUIsRUFBdkI7QUFDSCxNQWxCZCxFQW1CY21HLEtBbkJkLENBbUJvQnBFLEVBQUVxRSxJQW5CdEI7QUFvQkcsS0F4QkQ7QUF5QkgzQyxXQUFPQyxPQUFQLENBQWUsS0FBSzlFLElBQUwsQ0FBVSxlQUFWLENBQWY7QUFDR21ELE1BQUUsNEJBQUYsRUFBZ0MwSSxPQUFoQyxDQUF3QyxZQUFVO0FBQ2pEMUksT0FBRSxpQ0FBRixFQUFxQzJJLE1BQXJDO0FBQ0EsS0FGRCxFQUVFLEdBRkY7O0FBSUEsU0FBSzFKLFlBQUwsQ0FBa0JILE1BQWxCLEdBQTJCLEVBQTNCO0FBQ0EsU0FBS2xDLFVBQUwsR0FBa0IsRUFBbEI7O0FBRU1vRCxNQUFFLHVCQUFGLEVBQTJCOUMsS0FBM0IsQ0FBaUMsUUFBakM7QUFFTixJQXJDRCxNQXNDQTtBQUNGLFNBQUsrQixZQUFMLENBQWtCeUgsTUFBbEIsQ0FBeUIsTUFBekIsRUFBaUMseUJBQWpDLEVBQ0U1RCxJQURGLENBQ08sa0JBQVUsQ0FFZixDQUhGLEVBSVdzQixLQUpYLENBSWlCcEUsRUFBRXFFLElBSm5CO0FBS0FyRSxNQUFFLDRCQUFGLEVBQWdDMEksT0FBaEMsQ0FBd0MsWUFBVTtBQUM5QzFJLE9BQUUsaUNBQUYsRUFBcUMySSxNQUFyQztBQUNBLEtBRkosRUFFSyxHQUZMO0FBR0c7QUFDRCxHQTdaSTtBQThaSkMsZUE5WkksMkJBOFpXO0FBQUE7O0FBQ2YsT0FBRyxLQUFLeEosWUFBTCxJQUFxQixFQUF4QixFQUNBO0FBQ0ZzQyxXQUFPRSxLQUFQLENBQWEsS0FBSy9FLElBQUwsQ0FBVSxjQUFWLENBQWI7QUFDTW1ELE1BQUUsdUJBQUYsRUFBMkI5QyxLQUEzQixDQUFpQyxRQUFqQztBQUVILElBTEQsTUFNQTtBQUNDaUYsZUFDQzBHLElBREQsQ0FDTSw2QkFETixFQUNvQztBQUNuQ0MsYUFBTyxLQUFLMUo7QUFEdUIsS0FEcEMsRUFJQzBELElBSkQsQ0FJTSxrQkFBVTtBQUNmLFNBQUdyQixPQUFPOUUsSUFBVixFQUNBO0FBQ0YsY0FBS29CLFlBQUwsQ0FBa0JtSSxJQUFsQixDQUF1QnpFLE9BQU85RSxJQUFQLENBQVlBLElBQW5DO0FBQ0F3RixpQkFDRUYsR0FERixDQUNNLFdBQVMsUUFBS3RFLE9BQWQsR0FBc0IsVUFENUIsRUFFRW1GLElBRkYsQ0FFTyxvQkFBWTtBQUNqQixlQUFLbEYsZUFBTCxHQUF1QjRGLFdBQVd1RixTQUFTcE0sSUFBVCxDQUFjQSxJQUFkLENBQW1CbUMsTUFBOUIsQ0FBdkI7QUFDQTRDLGNBQU9DLE9BQVAsQ0FBZSxRQUFLOUUsSUFBTCxDQUFVLGtCQUFWLENBQWY7QUFDQSxPQUxGO0FBTUEsY0FBS3VDLFlBQUwsR0FBb0IsRUFBcEI7QUFDTVksUUFBRSx1QkFBRixFQUEyQjlDLEtBQTNCLENBQWlDLFFBQWpDO0FBQ0gsTUFYRCxNQVlBO0FBQ0Z3RSxhQUFPRSxLQUFQLENBQWEsUUFBSy9FLElBQUwsQ0FBVSxjQUFWLENBQWI7QUFDTW1ELFFBQUUsdUJBQUYsRUFBMkI5QyxLQUEzQixDQUFpQyxRQUFqQztBQUNIO0FBQ0QsS0FyQkQ7QUFzQkE7QUFFRDtBQTliSTtBQS9SUSxDQUFSLENBQVY7O0FBaXVCQThDLEVBQUUsb0JBQUYsRUFBd0JnSixLQUF4QixDQUE4QixZQUFVO0FBQ3ZDaEosR0FBRSx5QkFBRixFQUE2QmdKLEtBQTdCO0FBQ0EsQ0FGRDs7QUFJQWhKLEVBQUUsb0JBQUYsRUFBd0JDLEVBQXhCLENBQTJCLGlCQUEzQixFQUE4QyxZQUFXO0FBQ3hEN0QsS0FBSWlLLFdBQUo7QUFDQSxDQUZEOztBQUlBckcsRUFBRWlKLFFBQUYsRUFBWWhKLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFVBQVUwRSxDQUFWLEVBQWE7QUFDakMzRSxHQUFFLCtDQUFGLEVBQW1EZ0UsSUFBbkQsQ0FBd0QsWUFBWTtBQUNoRTtBQUNBO0FBQ0EsTUFBSSxDQUFDaEUsRUFBRSxJQUFGLEVBQVFrSixFQUFSLENBQVd2RSxFQUFFRSxNQUFiLENBQUQsSUFBeUI3RSxFQUFFLElBQUYsRUFBUW1KLEdBQVIsQ0FBWXhFLEVBQUVFLE1BQWQsRUFBc0JsQixNQUF0QixLQUFpQyxDQUExRCxJQUErRDNELEVBQUUsVUFBRixFQUFjbUosR0FBZCxDQUFrQnhFLEVBQUVFLE1BQXBCLEVBQTRCbEIsTUFBNUIsS0FBdUMsQ0FBMUcsRUFBNkc7QUFDekcsSUFBQyxDQUFDM0QsRUFBRSxJQUFGLEVBQVFvSixPQUFSLENBQWdCLE1BQWhCLEVBQXdCek0sSUFBeEIsQ0FBNkIsWUFBN0IsS0FBNEMsRUFBN0MsRUFBaUQwTSxPQUFqRCxJQUEwRCxFQUEzRCxFQUErREwsS0FBL0QsR0FBdUUsS0FBdkUsQ0FEeUcsQ0FDM0I7QUFDakY7QUFFSixFQVBEO0FBUUgsQ0FURCxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxdEJBO1NBRUE7O3VDQUVBO3dCQUNBO0FBRUE7QUFKQTtBQUZBLEc7Ozs7Ozs7QUNuQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9wYWdlcy9ld2FsbGV0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0MjYpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGY1ZmNmZjBlMWYwODAwOTdjZjQ2IiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUgMjYgMjciLCJcclxuVnVlLmNvbXBvbmVudCgnZXdhbGxldC1idXR0b24nLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9ld2FsbGV0L0V3YWxsZXRCdXR0b24udnVlJykpO1xyXG5cclxudmFyIGFwcCA9IG5ldyBWdWUoe1xyXG5cdCBlbDogJyNld2FsbGV0X2NvbnRhaW5lcicsXHJcblx0IGNvbXBvbmVudHM6IHtcclxuXHQgIFx0TXVsdGlzZWxlY3Q6IHdpbmRvdy5WdWVNdWx0aXNlbGVjdC5kZWZhdWx0XHJcblx0ICB9LFxyXG5cdCBkYXRhOiB7XHJcblx0IFx0dHJhbnNmZXJUbzpbXSxcclxuXHQgXHRsYW5nOltdLFxyXG5cdFx0YnRucyA6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCBpY29uOiAnYWNjb3VudF9iYWxhbmNlX3dhbGxldCdcclxuXHRcdFx0XHQsdGl0bGU6ICdMb2FkIE1vbmV5J1xyXG5cdFx0XHRcdCxzdWJ0aXRsZTogJ09mZmxpbmUgb3IgT25saW5lJ1xyXG5cdFx0XHRcdCxtb2RhbDogJ21vZGFsTG9hZCdcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCBpY29uOiAnZXhpdF90b19hcHAnXHJcblx0XHRcdFx0LHRpdGxlOiAnV2l0aGRyYXcgRnVuZHMnXHJcblx0XHRcdFx0LHN1YnRpdGxlOiAnR2V0IHlvdXIgc2F2ZWQgbW9uZXknXHJcblx0XHRcdFx0LG1vZGFsOiAnbW9kYWxXaXRoZHJhdydcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCBpY29uOiAnY29tcGFyZV9hcnJvd3MnXHJcblx0XHRcdFx0LHRpdGxlOiAnVHJhbnNmZXIgb3IgUmVjZWl2ZSdcclxuXHRcdFx0XHQsc3VidGl0bGU6ICd0by9mcm9tIGFub3RoZXIgQWNjb3VudCdcclxuXHRcdFx0XHQsbW9kYWw6ICdtb2RhbFRyYW5zZmVyUmVjZWl2ZSdcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCBpY29uOiAnYWNjb3VudF9iYWxhbmNlJ1xyXG5cdFx0XHRcdCx0aXRsZTogJ0JhbmsgQWNjb3VudCdcclxuXHRcdFx0XHQsc3VidGl0bGU6ICdSZWdpc3RlciB5b3VyIGFjY291bnQnXHJcblx0XHRcdFx0LG1vZGFsOiAnbW9kYWxBZGRCYW5rJ1xyXG5cdFx0XHR9LFxyXG5cclxuXHRcdF0sXHJcblx0XHRzaG93TG9hZGluZ09wdGlvbnM6dHJ1ZSxcclxuXHRcdHNob3dPbmxpbmVBcmVhOmZhbHNlLFxyXG5cdFx0c2hvd09mZmxpbmVBcmVhOmZhbHNlLFxyXG5cdFx0c2hvd0xvYWRpbmdCYWNrOmZhbHNlLFxyXG5cclxuXHRcdHNob3dUcmFuc2ZlclJlY2VpdmVPcHRpb25zOiB0cnVlLFxyXG5cdFx0c2hvd1RyYW5zZmVyQXJlYTogZmFsc2UsXHJcblx0XHRzaG93UmVjZWl2ZUFyZWE6IGZhbHNlLFxyXG5cdFx0c2hvd1RyYW5zZmVyUmVjZWl2ZUJhY2s6ZmFsc2UsXHJcblxyXG5cdFx0dXNlcl9pZDpudWxsLFxyXG5cdFx0ZXdhbGxldF9iYWxhbmNlOjAsXHJcblx0XHR1c2FibGVfYmFsYW5jZTowLFxyXG5cdFx0YmFua19hY2NvdW50czogZmFsc2UsXHJcblx0XHR0cmFuc2FjdGlvbnM6ZmFsc2UsXHJcblx0XHRiYW5rczpmYWxzZSxcclxuXHRcdGZldGNoZWRBY2NvdW50czpbXSxcclxuXHRcdGJhbmtBY2NvdW50Rm9ybTpuZXcgRm9ybSh7XHJcblx0XHRcdGJhbmtfaWQ6JycsXHJcblx0XHRcdGFjY291bnRfbmFtZTpudWxsLFxyXG5cdFx0XHRhY2NvdW50X251bWJlcjpudWxsLFxyXG5cdFx0XHRyZWFjY291bnRfbnVtYmVyOm51bGwsXHJcblx0XHR9LCB7IGJhc2VVUkw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCB9KSxcclxuXHRcdGJhbmtBY2NvdW50UHJvY2Vzc2luZzpudWxsLFxyXG5cdFx0YmFua0FjY291bnRNb2RlOnRydWUsXHJcblxyXG5cdFx0bG9hZE9mZmxpbmVGb3JtOm5ldyBGb3JtKHtcclxuXHRcdFx0YmFua19pZDonJyxcclxuXHRcdFx0YW1vdW50Om51bGwsXHJcblx0XHR9LCB7IGJhc2VVUkw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCB9KSxcclxuXHRcdHdpdGhkcmF3T2ZmbGluZUZvcm0gOiBuZXcgRm9ybSh7XHJcblx0XHRcdGJhbmtfaWQ6JycsXHJcblx0XHRcdGFtb3VudDpudWxsLFxyXG5cdFx0XHR0eXBlOidXaXRoZHJhdycsXHJcblx0XHR9LCB7IGJhc2VVUkw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCB9KSxcclxuXHRcdHRyYW5zZmVyRm9ybTpuZXcgRm9ybSh7XHJcblx0XHRcdGJhbmtfaWQ6JycsXHJcblx0XHRcdGFtb3VudDpudWxsLFxyXG5cdFx0XHR0eXBlOidUcmFuc2ZlcicsXHJcblx0XHRcdG5vdGU6JydcclxuXHRcdH0sIHsgYmFzZVVSTDogJ2h0dHA6Ly8nK0xhcmF2ZWwuYmFzZV9hcGlfdXJsIH0pLFxyXG5cdFx0aXNGZXRjaGluZ0FjY291bnRzOmZhbHNlLFxyXG5cdFx0dHJhbnNmZXJDb2RlOicnLFxyXG5cclxuXHRcdGN1cnJlbnRfdHJhbnNhY3Rpb246JycsXHJcblx0XHRjdXJyZW50X3RyYW5zYWN0aW9uX2ltYWdlOicnLFxyXG5cdFx0Y3VycmVudF90cmFuc2FjdGlvbl9zdGF0dXM6JycsXHJcblx0XHRjdXJyZW50X3RyYW5zYWN0aW9uX3JlYXNvbjonJyxcclxuXHRcdGJhc2VIZWlnaHQgOjAsXHJcblx0ICAgIGJhc2VXaWR0aCA6MCxcclxuXHQgICAgYmFzZVpvb20gOiAxLFxyXG5cdFx0c2hvd1pvb21hYmxlIDogZmFsc2UsXHJcblx0XHRcclxuXHRcdG1heERlcG9zaXRSZXF1ZXN0OjcwMDAwMCxcclxuXHRcdGZyaWVuZHM6W11cclxuXHR9LFxyXG5cclxuXHRtb3VudGVkKCkge1xyXG5cdCAgICAkKCcjbW9kYWxEZXBvc2l0VXBsb2FkJykub24oJ3Nob3duLmJzLm1vZGFsJywgKCkgPT4ge1xyXG5cdCAgICAgICAgdGhpcy5iYXNlWm9vbSA9IDE7XHJcblx0ICAgICAgICBpbWcgPSAkKCc8aW1nIGlkPVwic2hpdHR5aW1hZ2VcIiBjbGFzcz1cImNlbnRlci1ibG9jayBpbWctcmVzcG9uc2l2ZVwiIHNyYz1cIicrIHRoaXMuY3VycmVudF90cmFuc2FjdGlvbl9pbWFnZSArJ1wiPicpLm9uKCdsb2FkJywgKCkgPT4ge1xyXG5cdCAgICAgICAgICB2YXIgJG1vZGFsSW1nID0gJCgnPGltZyBzcmM9XCInKyB0aGlzLmN1cnJlbnRfdHJhbnNhY3Rpb25faW1hZ2UgKydcIiBjbGFzcz1cImNlbnRlci1ibG9jayAgaW1nLXJlc3BvbnNpdmVcIiA+Jyk7XHJcblx0ICAgICAgICAgICRtb2RhbEltZy5hcHBlbmRUbygnI2RvYy1jb250YWluZXInKTtcclxuXHQgICAgICAgICAgdGhpcy5iYXNlSGVpZ2h0ID0gJG1vZGFsSW1nLmhlaWdodCgpO1xyXG5cdCAgICAgICAgICB0aGlzLmJhc2VXaWR0aCA9ICRtb2RhbEltZy53aWR0aCgpO1xyXG5cdCAgICAgICAgICBcclxuXHJcblx0ICAgICAgICAgICRtb2RhbEltZy5kcmFnZ2FibGUoKTtcclxuXHQgICAgICAgICAgXHJcblx0ICAgICAgICB9KTtcclxuXHQgICAgfSk7XHJcblxyXG5cdCAgICAkKCcjbW9kYWxEZXBvc2l0VXBsb2FkJykub24oJ2hpZGUuYnMubW9kYWwnLCAoKSA9PiB7XHJcblx0ICAgICAgICAkKCcjZG9jLWNvbnRhaW5lcicpLmVtcHR5KCk7XHJcblx0ICAgICAgICB0aGlzLnNob3dab29tYWJsZSA9IGZhbHNlO1xyXG5cdCAgICB9KTtcclxuICB9LFxyXG5cclxuXHR3YXRjaDp7XHJcblx0XHRiYXNlWm9vbTpmdW5jdGlvbihuZXdWYWx1ZSl7XHJcblx0XHRcdHZhciBjaGFuZ2VkSGVpZ2h0U2l6ZSA9ICB0aGlzLmJhc2VIZWlnaHQgKiBuZXdWYWx1ZTtcclxuXHRcdFx0dmFyIGNoYW5nZWRXaWR0aFNpemUgPSB0aGlzLmJhc2VXaWR0aCAqIG5ld1ZhbHVlO1xyXG5cdFx0XHQkKCcjZG9jLWNvbnRhaW5lcicpLmZpbmQoJ2ltZycpLmNzcygnaGVpZ2h0JyxjaGFuZ2VkSGVpZ2h0U2l6ZSk7XHJcblx0XHRcdCQoJyNkb2MtY29udGFpbmVyJykuZmluZCgnaW1nJykuY3NzKCd3aWR0aCcsY2hhbmdlZFdpZHRoU2l6ZSk7XHJcblx0XHRcdCQoJyNkb2MtY29udGFpbmVyJykuZmluZCgnaW1nJykucmVtb3ZlQ2xhc3MoJ2ltZy1yZXNwb25zaXZlJyk7XHJcblx0XHR9LFxyXG5cclxuXHRcdGN1cnJlbnRfdHJhbnNhY3Rpb25faW1hZ2U6ZnVuY3Rpb24oKXtcclxuXHRcdFx0dGhpcy5iYXNlWm9vbSA9IDE7XHJcblx0XHR9LFxyXG5cclxuXHRcdHRyYW5zYWN0aW9uczpmdW5jdGlvbigpe1xyXG5cdFx0XHQkKCcjZXdhbGxldF90cmFuc2FjdGlvbl90YWJsZScpLkRhdGFUYWJsZSgpLmRlc3Ryb3koKTtcclxuXHJcblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuXHRcdFx0XHRjdXJyZW50VGFibGUgPSAkKCcjZXdhbGxldF90cmFuc2FjdGlvbl90YWJsZScpLkRhdGFUYWJsZSh7XHJcblx0XHRcdCAgICAgICAgcGFnZUxlbmd0aDogMTAsXHJcblx0XHRcdCAgICAgICAgcmVzcG9uc2l2ZTogdHJ1ZSxcclxuXHRcdFx0ICAgIH0pO1xyXG5cdFx0XHR9LDgwMCk7XHJcblx0XHR9XHJcblxyXG5cdH0sXHJcblxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdEV2ZW50Lmxpc3RlbignSW1nZmlsZXVwbG9hZC5kZXBvc2l0VXBsb2FkJywocmVzdWx0KSA9PiB7XHJcblxyXG5cdFx0XHRpZihyZXN1bHQpXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmdbJ3dhaXQtdmVyaWZpY2F0aW9uJ10sdGhpcy5sYW5nWydzdWNjZXNzLXVwbG9hZCddKTtcclxuXHRcdFx0fWVsc2VcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ3RyeS1hZ2FpbiddLHRoaXMubGFuZ1snZmFpbGVkLXVwbG9hZCddKVxyXG5cdFx0XHR9XHJcblx0XHRcdCQoJyNtb2RhbERlcG9zaXRVcGxvYWQnKS5tb2RhbCgndG9nZ2xlJyk7XHJcblx0XHR9KVxyXG5cdFx0dGhpcy51c2VyX2lkID0gTGFyYXZlbC51c2VyLmlkO1xyXG5cdFx0Ly8gYXhpb3NcclxuXHRcdGZ1bmN0aW9uIGdldFRyYW5zbGF0aW9uKCkge1xyXG5cdFx0XHRyZXR1cm4gYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2V3YWxsZXQnKTtcclxuXHRcdH1cclxuXHRcdGZ1bmN0aW9uIGdldEV3YWxsZXRCYWxhbmNlKCkge1xyXG5cdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy91c2VyLycrdGhpcy51c2VyX2lkKycvZXdhbGxldCcpO1xyXG5cdFx0fVxyXG5cdFx0ZnVuY3Rpb24gZ2V0VXNhYmxlQmFsYW5jZSgpIHtcclxuXHRcdFx0cmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvZ2V0VXNhYmxlJyk7XHJcblx0XHR9XHJcblx0XHRmdW5jdGlvbiBnZXRCYW5rQWNjb3VudHMoKSB7XHJcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL3VzZXIvJyt0aGlzLnVzZXJfaWQrJy9iYW5rLWFjY291bnRzJyk7XHJcblx0XHR9XHJcblx0XHRmdW5jdGlvbiBnZXRUcmFuc2FjdGlvbnMoKSB7XHJcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2V3YWxsZXQtdHJhbnNhY3Rpb24nKTtcclxuXHRcdH1cclxuXHRcdGZ1bmN0aW9uIGdldFB1cmNoYXNlZE9yZGVycygpIHtcclxuXHRcdFx0cmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvZXdhbGxldC10cmFuc2FjdGlvbi9nZXQtcHVyY2hhc2VkLW9yZGVycycpO1xyXG5cdFx0fVxyXG5cdFx0ZnVuY3Rpb24gZ2V0Q2FuY2VsbGVkT3JkZXJzKCkge1xyXG5cdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9ld2FsbGV0LXRyYW5zYWN0aW9uL2dldC1jYW5jZWxsZWQtb3JkZXJzJyk7XHJcblx0XHR9XHJcblx0XHRmdW5jdGlvbiBnZXRTb2xkT3JkZXJzKCkge1xyXG5cdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9ld2FsbGV0LXRyYW5zYWN0aW9uL2dldC1zb2xkLW9yZGVycycpO1xyXG5cdFx0fVxyXG5cdFx0ZnVuY3Rpb24gZ2V0UmV0dXJuZWRPcmRlcnMoKSB7XHJcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2V3YWxsZXQtdHJhbnNhY3Rpb24vZ2V0LWJ1eWVyLXJldHVybmVkLW9yZGVycycpO1xyXG5cdFx0fVxyXG5cdFx0ZnVuY3Rpb24gZ2V0QmFua3MoKSB7XHJcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2JhbmtzJyk7XHJcblx0XHR9XHJcblx0XHRmdW5jdGlvbiBnZXREb2NzKCkge1xyXG5cdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy91c2Vycy8nK0xhcmF2ZWwudXNlci5pZCsnL2RvY3VtZW50cycpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRheGlvcy5hbGwoW1xyXG5cdFx0XHQgZ2V0VHJhbnNsYXRpb24oKVxyXG5cdFx0XHQsZ2V0VHJhbnNhY3Rpb25zKClcclxuXHRcdFx0LGdldEV3YWxsZXRCYWxhbmNlKClcclxuXHRcdFx0LGdldFVzYWJsZUJhbGFuY2UoKVxyXG5cdFx0XHQsZ2V0QmFua0FjY291bnRzKClcclxuXHRcdFx0LGdldEJhbmtzKClcclxuXHRcdFx0LGdldERvY3MoKVxyXG5cdFx0XHQsZ2V0UHVyY2hhc2VkT3JkZXJzKClcclxuXHRcdFx0LGdldENhbmNlbGxlZE9yZGVycygpXHJcblx0XHRcdCxnZXRTb2xkT3JkZXJzKClcclxuXHRcdFx0LGdldFJldHVybmVkT3JkZXJzKClcclxuXHRcdF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG5cdFx0XHQoXHJcblx0XHRcdFx0IGxhbmd1YWdlXHJcblx0XHRcdFx0LHRyYW5zYWN0aW9uc1xyXG5cdFx0XHRcdCxiYWxhbmNlXHJcblx0XHRcdFx0LHVzYWJsZVxyXG5cdFx0XHRcdCxiYW5rX2FjY291bnRzXHJcblx0XHRcdFx0LGJhbmtzXHJcblx0XHRcdFx0LGRvY3VtZW50c1xyXG5cdFx0XHRcdCxwdXJjaGFzZWRfdHJhbnNhY3Rpb25zXHJcblx0XHRcdFx0LGNhbmNlbGxlZF90cmFuc2FjdGlvbnNcclxuXHRcdFx0XHQsc29sZF90cmFuc2FjdGlvbnNcclxuXHRcdFx0XHQscmV0dXJuZWRfdHJhbnNhY3Rpb25zXHJcblx0XHRcdCkgPT4ge1xyXG5cdFx0XHRcdFxyXG5cdFx0XHR0aGlzLmxhbmcgPSBsYW5ndWFnZS5kYXRhLmRhdGE7XHJcblx0XHRcdHRoaXMuYnRuc1swXS50aXRsZSA9IHRoaXMubGFuZ1tcImxvYWQtbW9uZXlcIl07XHJcblx0XHRcdHRoaXMuYnRuc1sxXS50aXRsZSA9IHRoaXMubGFuZ1tcIndpdGhkcmF3LWZ1bmRzXCJdO1xyXG5cdFx0XHR0aGlzLmJ0bnNbMl0udGl0bGUgPSB0aGlzLmxhbmdbXCJ0cmFuc2Zlci1vci1yZWNlaXZlXCJdO1xyXG5cdFx0XHR0aGlzLmJ0bnNbM10udGl0bGUgPSB0aGlzLmxhbmdbXCJiYW5rLWFjY291bnRcIl07XHJcblxyXG5cdFx0XHR0aGlzLmJ0bnNbMF0uc3VidGl0bGUgPSB0aGlzLmxhbmdbXCJvZmZsaW5lLW9yLW9ubGluZVwiXTtcclxuXHRcdFx0dGhpcy5idG5zWzFdLnN1YnRpdGxlID0gdGhpcy5sYW5nW1wid2l0aGRyYXctZnVuZHNcIl07XHJcblx0XHRcdHRoaXMuYnRuc1syXS5zdWJ0aXRsZSA9IHRoaXMubGFuZ1tcImdldC1zYXZlZC1tb25leVwiXTtcclxuXHRcdFx0dGhpcy5idG5zWzNdLnN1YnRpdGxlID0gdGhpcy5sYW5nW1wicmVnLWFjY291bnRcIl07XHJcblxyXG5cdFx0XHR0aGlzLmV3YWxsZXRfYmFsYW5jZSA9IGJhbGFuY2UuZGF0YS5zdWNjZXNzPyBwYXJzZUZsb2F0KGJhbGFuY2UuZGF0YS5kYXRhLmFtb3VudCk6MDtcclxuXHRcdFx0dGhpcy51c2FibGVfYmFsYW5jZSA9IHVzYWJsZS5kYXRhLnN1Y2Nlc3M/IHBhcnNlRmxvYXQodXNhYmxlLmRhdGEuZGF0YS51c2FibGUpOjA7XHJcblx0XHRcdHRoaXMuYmFua19hY2NvdW50cyA9IGJhbmtfYWNjb3VudHMuZGF0YS5zdWNjZXNzPyBiYW5rX2FjY291bnRzLmRhdGEuZGF0YS5iYW5rX2FjY291bnRzOmZhbHNlO1xyXG5cdFx0XHR0aGlzLmJhbmtzID0gYmFua3MuZGF0YS5zdWNjZXNzPyBiYW5rcy5kYXRhLmRhdGE6ZmFsc2U7XHJcblx0XHRcdHRoaXMudHJhbnNhY3Rpb25zID0gdHJhbnNhY3Rpb25zLnN0YXR1cz09MjAwPyB0cmFuc2FjdGlvbnMuZGF0YTpmYWxzZTtcclxuXHJcblx0XHRcdGlmKHB1cmNoYXNlZF90cmFuc2FjdGlvbnMuc3RhdHVzID09IDIwMClcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRoaXMudHJhbnNhY3Rpb25zID0gdGhpcy50cmFuc2FjdGlvbnMuY29uY2F0KHB1cmNoYXNlZF90cmFuc2FjdGlvbnMuZGF0YSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmKGNhbmNlbGxlZF90cmFuc2FjdGlvbnMuc3RhdHVzID09IDIwMClcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRoaXMudHJhbnNhY3Rpb25zID0gdGhpcy50cmFuc2FjdGlvbnMuY29uY2F0KGNhbmNlbGxlZF90cmFuc2FjdGlvbnMuZGF0YSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmKHNvbGRfdHJhbnNhY3Rpb25zLnN0YXR1cyA9PSAyMDApXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0aGlzLnRyYW5zYWN0aW9ucyA9IHRoaXMudHJhbnNhY3Rpb25zLmNvbmNhdChzb2xkX3RyYW5zYWN0aW9ucy5kYXRhKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYocmV0dXJuZWRfdHJhbnNhY3Rpb25zLnN0YXR1cyA9PSAyMDApXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0aGlzLnRyYW5zYWN0aW9ucyA9IHRoaXMudHJhbnNhY3Rpb25zLmNvbmNhdChyZXR1cm5lZF90cmFuc2FjdGlvbnMuZGF0YSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmKGRvY3VtZW50cy5zdGF0dXM9PTIwMClcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGlmKGRvY3VtZW50cy5kYXRhLmxlbmd0aClcclxuXHRcdFx0XHR7XHJcblx0XHRcdFx0XHR2YXIgZW50ZXJwcmlzZSA9IDE7XHJcblx0XHQgICAgICAgICAgICBlbnRlcnByaXNlICAgICo9IGRvY3VtZW50cy5kYXRhLmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLnR5cGUgPT09ICdzZWMnICYmIG9iai5zdGF0dXMgPT09ICdWZXJpZmllZCcgfSkubGVuZ3RoO1xyXG5cdFx0XHRcdFx0XHJcblx0XHQgICAgICAgICAgICBlbnRlcnByaXNlICAgICo9IGRvY3VtZW50cy5kYXRhLmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLnR5cGUgPT09ICdiaXInICYmIG9iai5zdGF0dXMgPT09ICdWZXJpZmllZCcgfSkubGVuZ3RoO1xyXG5cdFx0XHRcdFx0XHJcblx0XHQgICAgICAgICAgICBlbnRlcnByaXNlICAgICo9IGRvY3VtZW50cy5kYXRhLmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLnR5cGUgPT09ICdwZXJtaXQnICYmIG9iai5zdGF0dXMgPT09ICdWZXJpZmllZCcgfSkubGVuZ3RoO1xyXG5cdFx0XHRcdFx0aWYoZW50ZXJwcmlzZSlcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGhpcy5tYXhEZXBvc2l0UmVxdWVzdCA9IDcwMDAwMDAgLSB0aGlzLmV3YWxsZXRfYmFsYW5jZTtcclxuXHRcdFx0XHRcdH1lbHNlXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRoaXMubWF4RGVwb3NpdFJlcXVlc3QgPSAxMDAwMDAwIC0gdGhpcy5ld2FsbGV0X2JhbGFuY2U7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0VnVlLm5leHRUaWNrKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcblx0XHQgICAgICAgICAgICAvLyAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykuYXR0cignZGF0YS1vcmlnaW5hbC10aXRsZScsJCh0aGlzKS5hdHRyKCdkYXRhLXRlbXAtdGl0bGUnKSk7XHJcblx0XHQgICAgICAgICAgICAkKCcjZXdhbGxldF90cmFuc2FjdGlvbl90YWJsZSB0cltkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS5lYWNoKGZ1bmN0aW9uKCl7XHJcblx0XHQgICAgICAgICAgICBcdCQodGhpcykuYXR0cignZGF0YS1vcmlnaW5hbC10aXRsZScsJCh0aGlzKS5hdHRyKCdkYXRhLXRlbXAtdGl0bGUnKSk7XHJcblx0XHQgICAgICAgICAgICB9KTtcclxuXHRcdCAgICAgICAgICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKClcclxuXHRcdCAgICAgICAgICAgIC8vICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCdmaXhUaXRsZScpO1xyXG5cdFx0ICAgICAgICAgICAgJCgnI2V3YWxsZXRfdHJhbnNhY3Rpb25fdGFibGUnKS5kYXRhVGFibGUoKTtcclxuXHRcdFx0XHR9LDUwMCk7XHJcblx0ICAgICAgICB9KVxyXG5cdFx0fSkpXHJcblx0XHQuY2F0Y2goJC5ub29wKTtcclxuXHR9LFxyXG5cclxuXHRtZXRob2RzOiB7XHJcblx0XHRDcmVkaXRPckRlYml0KHR5cGUpe1xyXG5cdFx0XHRzd2l0Y2godHlwZSlcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGNhc2UgJ1B1cmNoYXNlJzpcclxuXHRcdFx0XHRjYXNlICdXaXRoZHJhdyc6XHJcblx0XHRcdFx0Y2FzZSAnVHJhbnNmZXInOlxyXG5cdFx0XHRcdGNhc2UgJ1Byb2R1Y3QgUmV0dXJuZWQgYnkgQnV5ZXInOlxyXG5cdFx0XHRcdFx0cmV0dXJuICd0ZXh0LWRhbmdlcic7XHJcblx0XHRcdFx0Y2FzZSAnRGVwb3NpdCc6XHJcblx0XHRcdFx0Y2FzZSAnUmVjZWl2ZWQnOlxyXG5cdFx0XHRcdGNhc2UgJ0NhbmNlbGxlZCBPcmRlcic6XHJcblx0XHRcdFx0Y2FzZSAnUmV0dXJuZWQgT3JkZXInOlxyXG5cdFx0XHRcdGNhc2UgJ1NvbGQnOlxyXG5cdFx0XHRcdFx0cmV0dXJuICd0ZXh0LXN1Y2Nlc3MnO1xyXG5cclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHR0b29sdGlwRm9yVHJhbnNhY3Rpb24oc3RhdHVzLHR5cGUscmVhc29uKXtcclxuXHRcdFx0aWYoc3RhdHVzID09ICdDYW5jZWxsZWQnIClcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHJldHVybiByZWFzb247XHJcblx0XHRcdH1lbHNlIGlmKChzdGF0dXMgPT1cIlBlbmRpbmdcIikmJih0eXBlID09IFwiRGVwb3NpdFwiKSl7XHJcblx0XHRcdFx0cmV0dXJuIFwiUGxlYXNlIHVwbG9hZCB5b3UgZGVwb3NpdCBzbGlwLlwiO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiAnJztcclxuXHRcdH0sXHJcblxyXG5cdFx0ZmlsZUNoYW5nZShlKSB7XHJcblx0ICAgICAgdmFyIGZpbGVzID0gZS50YXJnZXQuZmlsZXMgfHwgZS5kYXRhVHJhbnNmZXIuZmlsZXM7XHJcblx0ICAgICAgaWYgKCFmaWxlcy5sZW5ndGgpXHJcblx0ICAgICAgICByZXR1cm47XHJcblx0ICAgICAgdGhpcy5jcmVhdGVJbWFnZShmaWxlc1swXSk7XHJcblx0ICAgIH0sXHJcblxyXG5cdCAgICBjcmVhdGVJbWFnZShmaWxlKSB7XHJcblx0ICAgICAgdmFyIGltYWdlID0gbmV3IEltYWdlKCk7XHJcblx0ICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcblx0ICAgICAgdmFyIHZtID0gdGhpcztcclxuXHJcblx0ICAgICAgcmVhZGVyLm9ubG9hZCA9IChlKSA9PiB7XHJcblx0ICAgICAgICAvLyB0aGlzLmN1cnJlbnRfdHJhbnNhY3Rpb25faW1hZ2UgPSBlLnRhcmdldC5yZXN1bHQ7XHJcbiAgICAgICAgXHQkKCcjZG9jLWNvbnRhaW5lcicpLmVtcHR5KCk7XHJcblx0ICAgICAgICB0aGlzLmJhc2Vab29tID0gMTtcclxuXHQgICAgICAgIGltZyA9ICQoJzxpbWcgaWQ9XCJzaGl0dHlpbWFnZVwiIGNsYXNzPVwiY2VudGVyLWJsb2NrIGltZy1yZXNwb25zaXZlXCIgc3JjPVwiJysgIGUudGFyZ2V0LnJlc3VsdCArJ1wiPicpLm9uKCdsb2FkJywgKCkgPT4ge1xyXG5cdCAgICAgICAgICB2YXIgJG1vZGFsSW1nID0gJCgnPGltZyBzcmM9XCInKyAgZS50YXJnZXQucmVzdWx0ICsnXCIgY2xhc3M9XCJjZW50ZXItYmxvY2sgaW1nLXJlc3BvbnNpdmVcIiA+Jyk7XHJcblx0ICAgICAgICAgICRtb2RhbEltZy5hcHBlbmRUbygnI2RvYy1jb250YWluZXInKTtcclxuXHQgICAgICAgICAgdGhpcy5iYXNlSGVpZ2h0ID0gJG1vZGFsSW1nLmhlaWdodCgpO1xyXG5cdCAgICAgICAgICB0aGlzLmJhc2VXaWR0aCA9ICRtb2RhbEltZy53aWR0aCgpO1xyXG5cdFx0XHRcdFx0dGhpcy5zaG93Wm9vbWFibGUgPSB0cnVlO1xyXG5cclxuXHQgICAgICAgICAgJG1vZGFsSW1nLmRyYWdnYWJsZSgpO1xyXG5cdCAgICAgICAgICBcclxuXHQgICAgICAgIH0pO1xyXG5cdCAgICAgIH07XHJcblx0ICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XHJcblx0ICAgIH0sXHJcblxyXG5cdCAgICByZW1vdmVJbWFnZTogZnVuY3Rpb24gKGUpIHtcclxuXHQgICAgICB0aGlzLmltYWdlID0gJyc7XHJcblx0ICAgIH0sXHJcblxyXG5cdFx0b3Blbk1vZGFsVXBsb2FkRGVwb3NpdCh0cmFuc2FjdGlvbl9pZCxzdGF0dXMscmVhc29uLHJlZl9udW1iZXIpe1xyXG5cclxuXHRcdFx0dGhpcy5jdXJyZW50X3RyYW5zYWN0aW9uID0gdHJhbnNhY3Rpb25faWQ7XHJcblx0XHRcdHRoaXMuY3VycmVudF90cmFuc2FjdGlvbl9zdGF0dXMgPSBzdGF0dXM7XHJcblx0XHRcdHRoaXMuY3VycmVudF90cmFuc2FjdGlvbl9pbWFnZSA9IGZhbHNlO1xyXG5cdFx0XHR0aGlzLmN1cnJlbnRfdHJhbnNhY3Rpb25fcmVhc29uID0gcmVhc29uO1xyXG5cdFx0XHRcclxuXHRcdFx0YXhpb3MuZ2V0KFwiL3Nob3BwaW5nL2ltYWdlcy9kZXBvc2l0LXNsaXAvXCIrcmVmX251bWJlcisnLmpwZycpXHJcblx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRcdHRoaXMuY3VycmVudF90cmFuc2FjdGlvbl9pbWFnZSA9IFwiL3Nob3BwaW5nL2ltYWdlcy9kZXBvc2l0LXNsaXAvXCIrcmVmX251bWJlcisnLmpwZyc7XHJcblx0XHRcdFx0XHR0aGlzLnNob3dab29tYWJsZSA9IHRydWU7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQuY2F0Y2goJC5ub29wKTtcclxuXHRcdFx0JCgnI21vZGFsRGVwb3NpdFVwbG9hZCcpLm1vZGFsKCd0b2dnbGUnKTtcclxuXHRcdH0sXHJcblxyXG5cdFx0Ly9sb2FkaW5nIG9mIGV3YWxsZXRcclxuXHRcdGxpbWl0VGV4dCAoY291bnQpIHtcclxuXHQgICAgICByZXR1cm4gYGFuZCAke2NvdW50fSBvdGhlciBhY2NvdW50c2BcclxuXHQgICAgfSxcclxuXHJcblx0XHRjaG9vc2VPbmxpbmU6IGZ1bmN0aW9uKCl7XHJcblx0XHRcdHRoaXMuc2hvd0xvYWRpbmdPcHRpb25zID0gZmFsc2U7XHJcblx0XHRcdHRoaXMuc2hvd09mZmxpbmVBcmVhID0gZmFsc2U7XHJcblx0XHRcdHRoaXMuc2hvd09ubGluZUFyZWEgPSB0cnVlO1xyXG5cdFx0XHR0aGlzLnNob3dMb2FkaW5nQmFjayA9IHRydWU7XHJcblxyXG5cdFx0fSxcclxuXHJcblx0XHRjaG9vc2VPZmZsaW5lOiBmdW5jdGlvbigpe1xyXG5cdFx0XHR0aGlzLnNob3dMb2FkaW5nT3B0aW9ucyA9IGZhbHNlO1xyXG5cdFx0XHR0aGlzLnNob3dPbmxpbmVBcmVhID0gZmFsc2U7XHJcblx0XHRcdHRoaXMuc2hvd09mZmxpbmVBcmVhID0gdHJ1ZTtcclxuXHRcdFx0dGhpcy5zaG93TG9hZGluZ0JhY2sgPSB0cnVlO1xyXG5cclxuXHRcdH0sXHJcblx0XHQvL3RyYW5zZmVyL3JlY2VpdmUgb2YgZXdhbGxldFxyXG5cdFx0Y2hvb3NlVHJhbnNmZXI6IGZ1bmN0aW9uKCl7XHJcblx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHQuZ2V0KCcvZnJpZW5kcycpXHJcblx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRcdGxldCBmcmllbmRzID0gW107XHJcblx0XHRcdFx0XHRmb3IodmFyIGk9MDsgaTxyZXN1bHQuZGF0YS5mcmllbmRzLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRmcmllbmRzLnB1c2gocmVzdWx0LmRhdGEuZnJpZW5kc1tpXS51c2VyKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHRoaXMuZnJpZW5kcyA9IGZyaWVuZHM7XHJcblx0XHRcdFx0XHR0aGlzLmZldGNoZWRBY2NvdW50cyA9IGZyaWVuZHM7XHJcblx0XHRcdFx0fSlcclxuXHJcblx0XHRcdHRoaXMuc2hvd1RyYW5zZmVyUmVjZWl2ZU9wdGlvbnMgPSBmYWxzZTtcclxuXHRcdFx0dGhpcy5zaG93UmVjZWl2ZUFyZWEgPSBmYWxzZTtcclxuXHRcdFx0dGhpcy5zaG93VHJhbnNmZXJBcmVhID0gdHJ1ZTtcclxuXHRcdFx0dGhpcy5zaG93VHJhbnNmZXJSZWNlaXZlQmFjayA9IHRydWU7XHJcblx0XHR9LFxyXG5cclxuXHRcdGNob29zZVJlY2VpdmU6IGZ1bmN0aW9uKCl7XHJcblx0XHRcdHRoaXMuc2hvd1RyYW5zZmVyUmVjZWl2ZU9wdGlvbnMgPSBmYWxzZTtcclxuXHRcdFx0dGhpcy5zaG93VHJhbnNmZXJBcmVhID0gZmFsc2U7XHJcblx0XHRcdHRoaXMuc2hvd1JlY2VpdmVBcmVhID0gdHJ1ZTtcclxuXHRcdFx0dGhpcy5zaG93VHJhbnNmZXJSZWNlaXZlQmFjayA9IHRydWU7XHJcblx0XHR9LFxyXG5cclxuICAgIFx0b3Blbk1vZGFsOiBmdW5jdGlvbiAobW9kYWwpIHtcclxuXHRcdFx0cmVzZXRNb2RhbHMoKTtcclxuICAgIFx0XHQkKCcjJyttb2RhbCkubW9kYWwoJ3RvZ2dsZScpXHJcblx0ICAgIH0sXHJcblxyXG5cdCAgICByZXNldE1vZGFsczogZnVuY3Rpb24oKXtcclxuXHQgICAgXHR0aGlzLnNob3dMb2FkaW5nT3B0aW9ucyA9IHRydWU7XHJcblx0ICAgIFx0dGhpcy5zaG93T25saW5lQXJlYSA9IGZhbHNlO1xyXG5cdCAgICBcdHRoaXMuc2hvd09mZmxpbmVBcmVhID0gZmFsc2U7XHJcblx0XHRcdHRoaXMuc2hvd0xvYWRpbmdCYWNrID0gZmFsc2U7XHJcblxyXG5cdCAgICBcdHRoaXMuc2hvd1RyYW5zZmVyUmVjZWl2ZU9wdGlvbnMgPSB0cnVlO1xyXG5cdFx0XHR0aGlzLnNob3dUcmFuc2ZlckFyZWEgPSBmYWxzZTtcclxuXHRcdFx0dGhpcy5zaG93UmVjZWl2ZUFyZWEgPSBmYWxzZTtcclxuXHRcdFx0dGhpcy5zaG93VHJhbnNmZXJSZWNlaXZlQmFjayA9IGZhbHNlO1xyXG5cclxuXHRcdFx0dGhpcy5iYW5rQWNjb3VudE1vZGUgPSB0cnVlO1xyXG5cdFx0XHR0aGlzLmJhbmtBY2NvdW50Rm9ybS5iYW5rX2lkPScnO1xyXG5cdFx0XHR0aGlzLmJhbmtBY2NvdW50Rm9ybS5hY2NvdW50X25hbWU9bnVsbDtcclxuXHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0uYWNjb3VudF9udW1iZXI9bnVsbDtcclxuXHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0ucmVhY2NvdW50X251bWJlcj1udWxsO1xyXG5cclxuXHRcdFx0dGhpcy5iYW5rQWNjb3VudFByb2Nlc3NpbmcgPSBudWxsO1xyXG5cclxuXHRcdFx0dGhpcy5jdXJyZW50X3RyYW5zYWN0aW9uX3JlYXNvbiA9ICcnO1xyXG5cclxuXHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0gPSBuZXcgRm9ybSh7XHJcblx0XHRcdFx0YmFua19pZDonJyxcclxuXHRcdFx0XHRhY2NvdW50X25hbWU6bnVsbCxcclxuXHRcdFx0XHRhY2NvdW50X251bWJlcjpudWxsLFxyXG5cdFx0XHRcdHJlYWNjb3VudF9udW1iZXI6bnVsbCxcclxuXHRcdFx0fSwgeyBiYXNlVVJMOiAnaHR0cDovLycrTGFyYXZlbC5iYXNlX2FwaV91cmwgfSk7XHJcblxyXG5cdFx0XHR0aGlzLmxvYWRPZmZsaW5lRm9ybSA9IG5ldyBGb3JtKHtcclxuXHRcdFx0XHRiYW5rX2lkOicnLFxyXG5cdFx0XHRcdGFtb3VudDpudWxsLFxyXG5cdFx0XHR9LCB7IGJhc2VVUkw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCB9KVxyXG5cclxuXHRcdFx0dGhpcy53aXRoZHJhd09mZmxpbmVGb3JtID0gbmV3IEZvcm0oe1xyXG5cdFx0XHRcdGJhbmtfaWQ6JycsXHJcblx0XHRcdFx0YW1vdW50Om51bGwsXHJcblx0XHRcdFx0dHlwZTonV2l0aGRyYXcnLFxyXG5cdFx0XHR9LCB7IGJhc2VVUkw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCB9KVxyXG5cclxuXHRcdFx0dGhpcy50cmFuc2ZlckZvcm0gPSBuZXcgRm9ybSh7XHJcblx0XHRcdFx0YmFua19pZDonJyxcclxuXHRcdFx0XHRhbW91bnQ6bnVsbCxcclxuXHRcdFx0XHR0eXBlOidUcmFuc2ZlcicsXHJcblx0XHRcdFx0bm90ZTonJ1xyXG5cdFx0XHR9LCB7IGJhc2VVUkw6ICdodHRwOi8vJytMYXJhdmVsLmJhc2VfYXBpX3VybCB9KVxyXG5cclxuXHRcdFx0dGhpcy5jdXJyZW50X3RyYW5zYWN0aW9uX2ltYWdlPScnO1xyXG5cdCAgICB9LFxyXG5cclxuXHQgICAgbmFtZVdpdGhFbWFpbCh7IGZ1bGxfbmFtZSwgZW1haWwgfSl7XHJcblx0ICAgIFx0cmV0dXJuIGAke2Z1bGxfbmFtZX0gKCR7ZW1haWx9KWA7XHJcblx0ICAgIH0sXHJcblxyXG5cdCAgICBzdWJtaXROZXdCYW5rQWNjb3VudCgpe1xyXG4gICAgICAgICAgICB0aGlzLmJhbmtBY2NvdW50Rm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3YxL3VzZXIvJyt0aGlzLnVzZXJfaWQrJy9iYW5rLWFjY291bnRzJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LnN1Y2Nlc3MpXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dGhpcy5iYW5rX2FjY291bnRzLnVuc2hpZnQocmVzdWx0LmRhdGEpO1xyXG5cdCAgICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nW1wiYWNjb3VudC1hZGRlZFwiXSk7XHJcblx0ICAgICAgICAgICAgICAgICAgICB0aGlzLmJhbmtBY2NvdW50Rm9ybS5iYW5rX2lkPScnO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmJhbmtBY2NvdW50Rm9ybS5hY2NvdW50X25hbWU9bnVsbDtcclxuXHRcdFx0XHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0uYWNjb3VudF9udW1iZXI9bnVsbDtcclxuXHRcdFx0XHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0ucmVhY2NvdW50X251bWJlcj1udWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG5cdCAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZ1sndHJ5LWFnYWluJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuXHQgICAgfSxcclxuXHJcblx0ICAgIG1ha2VCYW5rQWNjb3VudE1vZGVBZGQoKXtcclxuXHQgICAgXHR0aGlzLmJhbmtBY2NvdW50TW9kZSA9IHRydWU7XHJcblx0XHRcdHRoaXMuYmFua0FjY291bnRGb3JtLmJhbmtfaWQ9Jyc7XHJcblx0XHRcdHRoaXMuYmFua0FjY291bnRGb3JtLmFjY291bnRfbmFtZT1udWxsO1xyXG5cdFx0XHR0aGlzLmJhbmtBY2NvdW50Rm9ybS5hY2NvdW50X251bWJlcj1udWxsO1xyXG5cdFx0XHR0aGlzLmJhbmtBY2NvdW50Rm9ybS5yZWFjY291bnRfbnVtYmVyPW51bGw7XHJcblxyXG5cdFx0XHR0aGlzLmJhbmtBY2NvdW50UHJvY2Vzc2luZyA9IG51bGw7XHJcblx0ICAgIH0sXHJcblxyXG5cdCAgICB1cGRhdGVCYW5rQWNjb3VudCgpe1xyXG5cdCAgICBcdGZ1bmN0aW9uIHB1dEJhbmtBY2NvdW50cygpIHtcclxuXHRcdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5wdXQoJy91c2VyLycrTGFyYXZlbC51c2VyLmlkKycvYmFuay1hY2NvdW50cy8nK3RoaXMuYmFua0FjY291bnRQcm9jZXNzaW5nKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0uc3VibWl0KCdwdXQnLCAnL3YxL3VzZXIvJytMYXJhdmVsLnVzZXIuaWQrJy9iYW5rLWFjY291bnRzLycrdGhpcy5iYW5rQWNjb3VudFByb2Nlc3NpbmcpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5zdWNjZXNzKVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBcdGZvcihpPTA7IGk8IHRoaXMuYmFua19hY2NvdW50cy5sZW5ndGg7IGkrKylcclxuICAgICAgICAgICAgICAgICAgICBcdHtcclxuICAgICAgICAgICAgICAgICAgICBcdFx0aWYodGhpcy5iYW5rX2FjY291bnRzW2ldLmlkID09IHRoaXMuYmFua0FjY291bnRQcm9jZXNzaW5nKVxyXG4gICAgICAgICAgICAgICAgICAgIFx0XHR7XHJcblx0XHRcdFx0ICAgICAgICAgICAgICAgIHRoaXMuYmFua19hY2NvdW50c1tpXSA9IHJlc3VsdC5kYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIFx0XHRcdGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIFx0XHR9XHJcbiAgICAgICAgICAgICAgICAgICAgXHR9XHJcblx0ICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmdbXCJhY2NvdW50LXVwZGF0ZWRcIl0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG5cdCAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZ1sndHJ5LWFnYWluJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuXHQgICAgfSxcclxuXHJcblx0ICAgIHZhbGlkYXRlRW1haWwoZW1haWwpIHtcclxuXHRcdCAgICB2YXIgcmUgPSAvXigoW148PigpXFxbXFxdXFxcXC4sOzpcXHNAXCJdKyhcXC5bXjw+KClcXFtcXF1cXFxcLiw7Olxcc0BcIl0rKSopfChcIi4rXCIpKUAoKFxcW1swLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31dKXwoKFthLXpBLVpcXC0wLTldK1xcLikrW2EtekEtWl17Mix9KSkkLztcclxuXHRcdCAgICByZXR1cm4gcmUudGVzdChlbWFpbCk7XHJcblx0XHR9LFxyXG5cclxuXHRcdHJlbW92ZUR1cGxpY2F0ZXMob3JpZ2luYWxBcnJheSwgcHJvcCkge1xyXG5cdFx0ICAgICB2YXIgbmV3QXJyYXkgPSBbXTtcclxuXHRcdCAgICAgdmFyIGxvb2t1cE9iamVjdCAgPSB7fTtcclxuXHJcblx0XHQgICAgIGZvcih2YXIgaSBpbiBvcmlnaW5hbEFycmF5KSB7XHJcblx0XHQgICAgICAgIGxvb2t1cE9iamVjdFtvcmlnaW5hbEFycmF5W2ldW3Byb3BdXSA9IG9yaWdpbmFsQXJyYXlbaV07XHJcblx0XHQgICAgIH1cclxuXHJcblx0XHQgICAgIGZvcihpIGluIGxvb2t1cE9iamVjdCkge1xyXG5cdFx0ICAgICAgICAgbmV3QXJyYXkucHVzaChsb29rdXBPYmplY3RbaV0pO1xyXG5cdFx0ICAgICB9XHJcblx0XHQgICAgICByZXR1cm4gbmV3QXJyYXk7XHJcblx0XHQgfSxcclxuXHJcblx0ICAgIGZldGNoQWNjb3VudHNCeUVtYWlsKG5hbWUpe1xyXG5cdCAgICBcdHZhciBmZXRjaGVkO1xyXG5cdFx0XHRcclxuXHRcdFx0dmFyIGZyaWVuZHMgPSBudWxsO1xyXG5cdFx0XHQvL2Nsb25lXHJcblx0XHRcdGZyaWVuZHMgPSAkLmV4dGVuZChmcmllbmRzLHRoaXMuZnJpZW5kcyk7XHJcblx0XHRcdC8vY29udmVydCB0byBhcnJheSBmb3IgbXVsdGlzZWxlY3RcclxuXHRcdFx0ZnJpZW5kcyA9ICQubWFwKGZyaWVuZHMsIGZ1bmN0aW9uKGVsKSB7IHJldHVybiBlbCB9KTtcclxuXHQgICAgXHRcclxuXHQgICAgXHRpZih0aGlzLnZhbGlkYXRlRW1haWwobmFtZSkpXHJcblx0ICAgIFx0e1xyXG5cdFx0XHRcdHRoaXMuaXNGZXRjaGluZ0FjY291bnRzID0gdHJ1ZTtcclxuXHRcdCAgICBcdC8vYWNjZXB0IG9ubHkgZW1haWwgYWRkcmVzc2VzIGZvciBub3QgZnJpZW5kc1xyXG5cdFx0XHRcdHRoaXMuZmV0Y2hlZEFjY291bnRzID0gZnJpZW5kcztcclxuXHJcblx0XHQgICAgXHRcclxuXHRcdFx0XHRheGlvc0FQSXYxXHJcblx0XHRcdFx0XHQuZ2V0KCcvdXNlcnM/c2VhcmNoQnlFbWFpbD0nK25hbWUpXHJcblx0XHRcdFx0XHQudGhlbihhY2NvdW50cyA9PiB7XHJcblx0XHRcdFx0XHRcdGlmKGFjY291bnRzLmRhdGEubGVuZ3RoKVxyXG5cdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5mZXRjaGVkQWNjb3VudHMgPSB0aGlzLmZldGNoZWRBY2NvdW50cy5jb25jYXQoYWNjb3VudHMuZGF0YSk7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5mZXRjaGVkQWNjb3VudHMgPSB0aGlzLnJlbW92ZUR1cGxpY2F0ZXModGhpcy5mZXRjaGVkQWNjb3VudHMsJ2VtYWlsJyk7XHJcblx0XHRcdFx0XHRcdH1lbHNlXHJcblx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHQgICAgXHRcdHRoaXMuZmV0Y2hlZEFjY291bnRzID0gZnJpZW5kcztcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR0aGlzLmlzRmV0Y2hpbmdBY2NvdW50cyA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdC5jYXRjaChlcnJvciA9PiB7XHJcblx0XHRcdFx0XHRcdHRoaXMuZmV0Y2hlZEFjY291bnRzID0gZnJpZW5kcztcclxuXHRcdFx0XHRcdH0pXHJcblxyXG5cdCAgICBcdH1lbHNlIFxyXG5cdCAgICBcdHtcclxuXHQgICAgXHRcdHRoaXMuZmV0Y2hlZEFjY291bnRzID0gIGZyaWVuZHM7XHJcblx0ICAgIFx0fVxyXG5cdCAgICB9LFxyXG5cdCAgXHJcblx0ICAgIHNob3dCYW5rKGJhbmtfYWNjb3VudF9pZCl7XHJcblx0ICAgIFx0ZnVuY3Rpb24gZ2V0QmFua0FjY291bnRzKGlkKSB7XHJcblx0XHRcdFx0cmV0dXJuIGF4aW9zQVBJdjEuZ2V0KCcvdXNlci8nK0xhcmF2ZWwudXNlci5pZCsnL2JhbmstYWNjb3VudHMvJytpZCk7XHJcblx0XHRcdH1cclxuXHQgICAgXHRheGlvcy5hbGwoW1xyXG5cdFx0XHRcdGdldEJhbmtBY2NvdW50cyhiYW5rX2FjY291bnRfaWQpXHJcblx0XHRcdF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG5cdFx0XHRcdChcclxuXHRcdFx0XHRcdCBiYW5rX2FjY291bnRcclxuXHRcdFx0XHQpID0+IHtcclxuXHRcdFx0XHRpZihiYW5rX2FjY291bnQuZGF0YS5zdWNjZXNzKVxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdHRoaXMuYmFua0FjY291bnRQcm9jZXNzaW5nID0gYmFua19hY2NvdW50X2lkO1xyXG5cdFx0XHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0uYmFua19pZCA9IGJhbmtfYWNjb3VudC5kYXRhLmRhdGEuYmFua19pZDtcclxuXHRcdFx0XHRcdHRoaXMuYmFua0FjY291bnRGb3JtLmFjY291bnRfbmFtZSA9IGJhbmtfYWNjb3VudC5kYXRhLmRhdGEuYWNjb3VudF9uYW1lO1xyXG5cdFx0XHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0uYWNjb3VudF9udW1iZXIgPSBiYW5rX2FjY291bnQuZGF0YS5kYXRhLmFjY291bnRfbnVtYmVyO1xyXG5cdFx0XHRcdFx0dGhpcy5iYW5rQWNjb3VudEZvcm0ucmVhY2NvdW50X251bWJlciA9IGJhbmtfYWNjb3VudC5kYXRhLmRhdGEuYWNjb3VudF9udW1iZXI7XHJcblx0XHRcdFx0XHR0aGlzLmJhbmtBY2NvdW50TW9kZSA9IGZhbHNlO1xyXG5cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pKVxyXG5cdFx0XHQuY2F0Y2goJC5ub29wKTtcclxuXHQgICAgfSxcclxuXHJcblx0ICAgIHN1Ym1pdExvYWRPZmZsaW5lKCl7XHJcblx0ICAgIFx0dGhpcy5sb2FkT2ZmbGluZUZvcm0uc3VibWl0KCdwb3N0JywgJy92MS9ld2FsbGV0LXRyYW5zYWN0aW9uJylcclxuICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LnN1Y2Nlc3MpXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dGhpcy50cmFuc2FjdGlvbnMucHVzaChyZXN1bHQuZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgXHQkKCcjbW9kYWxMb2FkJykubW9kYWwoJ3RvZ2dsZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmluZm8odGhpcy5sYW5nWydyZWYtbm8nXStyZXN1bHQuZGF0YS5yZWZfbnVtYmVyKyc8YnI+Jyt0aGlzLmxhbmdbJ2RlcG9zaXQnXSsnPGJyPicrdGhpcy5sYW5nWydiYW5rJ10rJzogJytyZXN1bHQuZGF0YS5kZXNjcmlwdGlvbiwgJycsIHtcclxuXHRcdFx0XHRcdFx0ICB0aW1lT3V0OiAwLFxyXG5cdFx0XHRcdFx0XHQgIGV4dGVuZGVkVGltZU91dDogMFxyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nW1wic3VjY2Vzcy1yZXF1ZXN0XCJdKTtcclxuXHJcblx0XHRcdFx0XHRcdC8vc2hvdyBpbnN0cnVjdGlvbiBtb2RhbFxyXG5cdFx0XHRcdFx0XHQkKCcjbW9kYWxJbnN0cnVjdGlvbnMnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAgICAgICAgICB7XHJcblx0ICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWyd0cnktYWdhaW4nXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG5cdCAgICB9LFxyXG5cclxuXHQgICAgc3VibWl0V2l0aGRyYXdPZmZsaW5lKCl7XHJcblx0ICAgIFx0dGhpcy53aXRoZHJhd09mZmxpbmVGb3JtLnN1Ym1pdCgncG9zdCcsICcvdjEvZXdhbGxldC10cmFuc2FjdGlvbicpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5zdWNjZXNzKVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBcdHRoaXMudHJhbnNhY3Rpb25zLnB1c2gocmVzdWx0LmRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIFx0JCgnI21vZGFsV2l0aGRyYXcnKS5tb2RhbCgndG9nZ2xlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgXHR0b2FzdHIuaW5mbyh0aGlzLmxhbmdbJ3JlZi1ubyddK3Jlc3VsdC5kYXRhLnJlZl9udW1iZXIrJzxicj4nK3RoaXMubGFuZ1snd2l0aGRyYXcnXSsnPGJyPicrdGhpcy5sYW5nWydiYW5rJ10rJzogJytyZXN1bHQuZGF0YS5kZXNjcmlwdGlvbiwgJycsIHtcclxuXHRcdFx0XHRcdFx0ICB0aW1lT3V0OiAwLFxyXG5cdFx0XHRcdFx0XHQgIGV4dGVuZGVkVGltZU91dDogMFxyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nW1wic3VjY2Vzcy1yZXF1ZXN0XCJdKTtcclxuICAgICAgICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuXHQgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ3RyeS1hZ2FpbiddKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XHJcblx0ICAgIH0sXHJcblxyXG5cdCAgICBzdWJtaXRUcmFuc2Zlcigpe1xyXG5cdCAgICBcdGlmKHRoaXMudHJhbnNmZXJUby5sZW5ndGgpXHJcblx0ICAgIFx0e1xyXG5cdCAgICBcdFx0dGhpcy50cmFuc2ZlclRvLmZvckVhY2goKHRyYW5zZmVyX2lkKSA9PntcclxuXHRcdCAgICBcdFx0dGhpcy50cmFuc2ZlckZvcm0uYmFua19pZCA9IHRyYW5zZmVyX2lkLmlkO1xyXG5cdFx0XHRcdFx0Ly9zdWJtaXQgZm9ybVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHR0aGlzLnRyYW5zZmVyRm9ybS5zdWJtaXQoJ3Bvc3QnLCAnL3YxL2V3YWxsZXQtdHJhbnNhY3Rpb24nKVxyXG5cdFx0ICAgICAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcblxyXG5cdFx0ICAgICAgICAgICAgICAgICAgICB0aGlzLmZldGNoZWRBY2NvdW50cyA9IFtdO1xyXG5cclxuXHRcdCAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LnN1Y2Nlc3MpXHJcblx0XHQgICAgICAgICAgICAgICAgICAgIHtcclxuXHRcdCAgICAgICAgICAgICAgICAgICAgXHR0aGlzLnRyYW5zYWN0aW9ucy5wdXNoKHJlc3VsdC5kYXRhKTtcclxuXHRcdCAgICAgICAgICAgICAgICAgICAgXHR0b2FzdHIuaW5mbyh0aGlzLmxhbmdbJ2NvZGUnXSsnOiAnK3Jlc3VsdC5kYXRhLnJlZl9udW1iZXIrJzxicj4nK3RoaXMubGFuZ1tcInRyYW5zZmVyXCJdKyc8YnI+ICcrcmVzdWx0LmRhdGEuZGVzY3JpcHRpb24sICcnLCB7XHJcblx0XHRcdFx0XHRcdFx0XHQgIHRpbWVPdXQ6IDAsXHJcblx0XHRcdFx0XHRcdFx0XHQgIGV4dGVuZGVkVGltZU91dDogMFxyXG5cdFx0XHRcdFx0XHRcdFx0fSk7XHJcblx0XHQgICAgICAgICAgICAgICAgICAgIH1lbHNlXHJcblx0XHQgICAgICAgICAgICAgICAgICAgIHtcclxuXHJcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZ1sndHJ5LWFnYWluJ10pO1xyXG5cdFx0ICAgICAgICAgICAgICAgICAgICB9XHJcblx0XHQgICAgICAgICAgICAgICAgICAgIHRoaXMuZmV0Y2hlZEFjY291bnRzID0gW107XHJcblx0XHQgICAgICAgICAgICAgICAgfSlcclxuXHRcdCAgICAgICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuXHQgICAgXHRcdH0pO1xyXG5cdFx0XHRcdHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1snZ2l2ZS1yZWNlaXZlciddKTtcclxuXHQgICAgXHRcdCQoJyNidG5UcmFuc2ZlclN1Ym1pdCAubG9hZGVyJykuZmFkZU91dChmdW5jdGlvbigpe1xyXG5cdCAgICBcdFx0XHQkKCcjYnRuVHJhbnNmZXJTdWJtaXQgLmJ0bi1jb250ZW50JykuZmFkZUluKCk7XHJcblx0ICAgIFx0XHR9LDEwMCk7XHJcblxyXG5cdCAgICBcdFx0dGhpcy50cmFuc2ZlckZvcm0uYW1vdW50ID0gJyc7XHJcblx0ICAgIFx0XHR0aGlzLnRyYW5zZmVyVG8gPSBbXTtcclxuICAgICAgICAgICAgXHRcclxuICAgICAgICAgICAgXHQkKCcjbW9kYWxUcmFuc2ZlclJlY2VpdmUnKS5tb2RhbCgndG9nZ2xlJyk7XHJcblxyXG5cdCAgICBcdH1lbHNlXHJcblx0ICAgIFx0e1xyXG5cdFx0XHRcdHRoaXMudHJhbnNmZXJGb3JtLnN1Ym1pdCgncG9zdCcsICcvdjEvZXdhbGxldC10cmFuc2FjdGlvbicpXHJcblx0XHRcdFx0XHQudGhlbihyZXN1bHQgPT4ge1xyXG5cclxuXHRcdFx0XHRcdH0pXHJcblx0XHQgICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuXHRcdFx0XHQkKCcjYnRuVHJhbnNmZXJTdWJtaXQgLmxvYWRlcicpLmZhZGVPdXQoZnVuY3Rpb24oKXtcclxuXHQgICAgXHRcdFx0JCgnI2J0blRyYW5zZmVyU3VibWl0IC5idG4tY29udGVudCcpLmZhZGVJbigpO1xyXG5cdCAgICBcdFx0fSwxMDApO1xyXG5cdCAgICBcdH1cclxuXHQgICAgfSxcclxuXHQgICAgIHN1Ym1pdFJlY2VpdmUoKXtcclxuXHQgICAgXHRpZih0aGlzLnRyYW5zZmVyQ29kZSA9PSAnJylcclxuXHQgICAgXHR7XHJcblx0XHRcdFx0dG9hc3RyLmVycm9yKHRoaXMubGFuZ1snaW52YWxpZC1jb2RlJ10pO1xyXG5cdCAgICAgICAgXHQkKCcjbW9kYWxUcmFuc2ZlclJlY2VpdmUnKS5tb2RhbCgndG9nZ2xlJyk7XHJcblx0XHRcdFx0XHJcblx0ICAgIFx0fWVsc2VcclxuXHQgICAgXHR7XHJcblx0XHQgICAgXHRheGlvc0FQSXYxXHJcblx0ICAgIFx0XHQucG9zdCgnL2V3YWxsZXQtdHJhbnNhY3Rpb24vcmVmbnVtJyx7XHJcblx0ICAgIFx0XHRcdHJlZm51bTp0aGlzLnRyYW5zZmVyQ29kZVxyXG5cdCAgICBcdFx0fSlcclxuXHQgICAgXHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0ICAgIFx0XHRcdGlmKHJlc3VsdC5kYXRhKVxyXG5cdCAgICBcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRoaXMudHJhbnNhY3Rpb25zLnB1c2gocmVzdWx0LmRhdGEuZGF0YSk7XHJcblx0XHRcdFx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHRcdFx0XHQuZ2V0KCcvdXNlci8nK3RoaXMudXNlcl9pZCsnL2V3YWxsZXQnKVxyXG5cdFx0XHRcdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcclxuXHRcdFx0XHRcdFx0XHRcdHRoaXMuZXdhbGxldF9iYWxhbmNlID0gcGFyc2VGbG9hdChyZXNwb25zZS5kYXRhLmRhdGEuYW1vdW50KTtcclxuXHRcdFx0XHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1snZXdhbGxldC1yZWNlaXZlZCddKTtcclxuXHRcdFx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdFx0XHR0aGlzLnRyYW5zZmVyQ29kZSA9ICcnO1xyXG5cdFx0XHQgICAgICAgIFx0JCgnI21vZGFsVHJhbnNmZXJSZWNlaXZlJykubW9kYWwoJ3RvZ2dsZScpO1xyXG5cdCAgICBcdFx0XHR9ZWxzZVxyXG5cdCAgICBcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ2ludmFsaWQtY29kZSddKTtcclxuXHRcdFx0ICAgICAgICBcdCQoJyNtb2RhbFRyYW5zZmVyUmVjZWl2ZScpLm1vZGFsKCd0b2dnbGUnKTtcclxuXHQgICAgXHRcdFx0fVxyXG5cdCAgICBcdFx0fSlcclxuXHQgICAgXHR9XHJcblxyXG5cdCAgICB9XHJcblx0fVxyXG59KVxyXG5cclxuJCgnI3VwbG9hZERlcG9zaXRTbGlwJykuY2xpY2soZnVuY3Rpb24oKXtcclxuXHQkKCcuZGVwb3NpdC11cGxvYWRlciBpbnB1dCcpLmNsaWNrKCk7XHJcbn0pXHJcblxyXG4kKCcjZXdhbGxldF9jb250YWluZXInKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24oKSB7XHJcblx0YXBwLnJlc2V0TW9kYWxzKCk7IFxyXG59KTtcclxuXHJcbiQoZG9jdW1lbnQpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICAkKCdbZGF0YS10b2dnbGU9XCJwb3BvdmVyXCJdLFtkYXRhLW9yaWdpbmFsLXRpdGxlXScpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIC8vdGhlICdpcycgZm9yIGJ1dHRvbnMgdGhhdCB0cmlnZ2VyIHBvcHVwc1xyXG4gICAgICAgIC8vdGhlICdoYXMnIGZvciBpY29ucyB3aXRoaW4gYSBidXR0b24gdGhhdCB0cmlnZ2VycyBhIHBvcHVwXHJcbiAgICAgICAgaWYgKCEkKHRoaXMpLmlzKGUudGFyZ2V0KSAmJiAkKHRoaXMpLmhhcyhlLnRhcmdldCkubGVuZ3RoID09PSAwICYmICQoJy5wb3BvdmVyJykuaGFzKGUudGFyZ2V0KS5sZW5ndGggPT09IDApIHsgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICgoJCh0aGlzKS5wb3BvdmVyKCdoaWRlJykuZGF0YSgnYnMucG9wb3ZlcicpfHx7fSkuaW5TdGF0ZXx8e30pLmNsaWNrID0gZmFsc2UgIC8vIGZpeCBmb3IgQlMgMy4zLjZcclxuICAgICAgICB9XHJcblxyXG4gICAgfSk7XHJcbn0pO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvZXdhbGxldC5qcyIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTEyIGNvbC1zbS0zIGNvbC14cy02XCIgPlxyXG5cdFx0PGRpdiBjbGFzcz1cImNhcmRcIiAgQGNsaWNrPSdvcGVuTW9kYWwoYnRuLm1vZGFsKSc+XHJcblx0ICAgICAgICA8ZGl2IGNsYXNzPSdjb2wtbWQtNCB0ZXh0LWNlbnRlciB2ZXJ0aWNhbC1jZW50ZXInPlxyXG5cdCAgICAgICAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj57e2J0bi5pY29ufX08L2k+XHJcblx0ICAgICAgICA8L2Rpdj5cclxuXHQgICAgICAgIDxkaXYgY2xhc3M9J2NvbC1tZC04Jz5cclxuXHQgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1jb250YWluZXJcIj5cclxuXHQgICAgICAgICAgICAgICAgPGg0PjxiPnt7IGJ0bi50aXRsZSB9fTwvYj48L2g0PiBcclxuXHQgICAgICAgICAgICAgICAgPHA+e3tidG4uc3VidGl0bGV9fTwvc2xvdD48L3A+IFxyXG5cdCAgICAgICAgICAgIDwvZGl2PlxyXG5cdCAgICAgICAgPC9kaXY+XHJcblx0XHQ8L2Rpdj5cclxuXHQ8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcblx0cHJvcHM6IFsnYnRuJ10sXHJcblx0bWV0aG9kczoge1xyXG4gICAgXHRvcGVuTW9kYWw6IGZ1bmN0aW9uIChtb2RhbCkge1xyXG4gICAgXHRcdCQoJyMnK21vZGFsKS5tb2RhbCgndG9nZ2xlJylcclxuXHQgICAgfVxyXG5cdH1cclxufVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRXdhbGxldEJ1dHRvbi52dWU/MzVjMWRmODAiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Fd2FsbGV0QnV0dG9uLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtM2EyM2IyZTNcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRXdhbGxldEJ1dHRvbi52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxcZXdhbGxldFxcXFxFd2FsbGV0QnV0dG9uLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEV3YWxsZXRCdXR0b24udnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTNhMjNiMmUzXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtM2EyM2IyZTNcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2V3YWxsZXQvRXdhbGxldEJ1dHRvbi52dWVcbi8vIG1vZHVsZSBpZCA9IDMyOVxuLy8gbW9kdWxlIGNodW5rcyA9IDI0IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEyIGNvbC1zbS0zIGNvbC14cy02XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2FyZFwiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0ub3Blbk1vZGFsKF92bS5idG4ubW9kYWwpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCB0ZXh0LWNlbnRlciB2ZXJ0aWNhbC1jZW50ZXJcIlxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibWF0ZXJpYWwtaWNvbnNcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uYnRuLmljb24pKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLThcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjYXJkLWNvbnRhaW5lclwiXG4gIH0sIFtfYygnaDQnLCBbX2MoJ2InLCBbX3ZtLl92KF92bS5fcyhfdm0uYnRuLnRpdGxlKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygncCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5idG4uc3VidGl0bGUpKV0pXSldKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtM2EyM2IyZTNcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zYTIzYjJlM1wifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2V3YWxsZXQvRXdhbGxldEJ1dHRvbi52dWVcbi8vIG1vZHVsZSBpZCA9IDM3OVxuLy8gbW9kdWxlIGNodW5rcyA9IDI0Il0sInNvdXJjZVJvb3QiOiIifQ==