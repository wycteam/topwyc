/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 335);
/******/ })
/************************************************************************/
/******/ ({

/***/ 155:
/***/ (function(module, exports) {

var app = new Vue({
  el: '.reset-password-container',
  data: {
    question: [],
    userid: 0,
    email: '',
    lang: [],
    form: new Form({
      primary_email: '',
      email2: '',
      answer: ''
    }, { baseURL: 'http://' + Laravel.base_api_url })
  },

  created: function created() {
    var _this = this;

    axios.get('/translate/passwords').then(function (lang) {
      _this.lang = lang.data.data;
    });
  },


  methods: {
    searhEmail: function searhEmail() {
      var _this2 = this;

      this.form.submit('post', '/v1/searchEmail').then(function (result) {
        if (result.response == 1) {
          $('#search, #email2-cont').hide();

          _this2.userid = result.id;
          _this2.email = $('#email2').val();
          _this2.form.email2 = '';
          _this2.form.primary_email = $('#email2').val();

          axiosAPIv1.get('/getQuestion/' + result.id).then(function (result) {
            _this2.question = result.data;
            if (_this2.question.length == 0) {
              $('#l-email').text(_this2.lang['alt-email']);
              $('#email2-cont, #search-alt').show();
            } else $('.answer-cont').show();
          });
        } else {
          toastr.error(_this2.lang['email-error-msg']);
        }
      });
    },
    searhAltEmail: function searhAltEmail() {
      var _this3 = this;

      this.form.submit('post', '/v1/searchAltEmail').then(function (result) {
        if (result.response == 1) {
          $('#question-cont').hide();
          $('#success-cont').show();
        } else if (result.response == 3) {
          toastr.error(_this3.lang['please-set-alt-email']);
        } else {
          toastr.error(_this3.lang['email-error-msg']);
        }
      });
    },
    answerQuestion: function answerQuestion() {
      var _this4 = this;

      var val = $("#question option:selected").val();

      if (val == 0) {
        toastr.error(this.lang['question-error-msg']);
      } else {
        axiosAPIv1.get('/answerQuestion/' + val + '/' + this.userid, {
          params: {
            answer: $('#answer').val()
          }
        }).then(function (result) {
          if (result.data.response == 1) {
            $('#question-cont').hide();
            $('#success-cont').show();
          } else {
            toastr.error(_this4.lang['answer-error-msg']);
          }
        });
      }
    }
  }
});

/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(155);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjYzYjM3MGU5M2QwM2U4MzhmMzQ/NjI1YioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvcmVzZXQtcGFzc3dvcmQuanMiXSwibmFtZXMiOlsiYXBwIiwiVnVlIiwiZWwiLCJkYXRhIiwicXVlc3Rpb24iLCJ1c2VyaWQiLCJlbWFpbCIsImxhbmciLCJmb3JtIiwiRm9ybSIsInByaW1hcnlfZW1haWwiLCJlbWFpbDIiLCJhbnN3ZXIiLCJiYXNlVVJMIiwiTGFyYXZlbCIsImJhc2VfYXBpX3VybCIsImNyZWF0ZWQiLCJheGlvcyIsImdldCIsInRoZW4iLCJtZXRob2RzIiwic2VhcmhFbWFpbCIsInN1Ym1pdCIsInJlc3VsdCIsInJlc3BvbnNlIiwiJCIsImhpZGUiLCJpZCIsInZhbCIsImF4aW9zQVBJdjEiLCJsZW5ndGgiLCJ0ZXh0Iiwic2hvdyIsInRvYXN0ciIsImVycm9yIiwic2VhcmhBbHRFbWFpbCIsImFuc3dlclF1ZXN0aW9uIiwicGFyYW1zIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBLElBQUlBLE1BQU0sSUFBSUMsR0FBSixDQUFRO0FBQ2hCQyxNQUFJLDJCQURZO0FBRWhCQyxRQUFNO0FBQ0pDLGNBQVMsRUFETDtBQUVKQyxZQUFPLENBRkg7QUFHSkMsV0FBTSxFQUhGO0FBSUpDLFVBQUssRUFKRDtBQUtKQyxVQUFNLElBQUlDLElBQUosQ0FBUztBQUNaQyxxQkFBYyxFQURGO0FBRVpDLGNBQU8sRUFGSztBQUdaQyxjQUFPO0FBSEssS0FBVCxFQUlKLEVBQUVDLFNBQVMsWUFBVUMsUUFBUUMsWUFBN0IsRUFKSTtBQUxGLEdBRlU7O0FBY2hCQyxTQWRnQixxQkFjUDtBQUFBOztBQUNQQyxVQUFNQyxHQUFOLENBQVUsc0JBQVYsRUFDR0MsSUFESCxDQUNRLGdCQUFRO0FBQ1osWUFBS1osSUFBTCxHQUFZQSxLQUFLSixJQUFMLENBQVVBLElBQXRCO0FBQ0QsS0FISDtBQUlELEdBbkJlOzs7QUFxQmhCaUIsV0FBUTtBQUNOQyxjQURNLHdCQUNNO0FBQUE7O0FBQ1YsV0FBS2IsSUFBTCxDQUFVYyxNQUFWLENBQWlCLE1BQWpCLEVBQXlCLGlCQUF6QixFQUNDSCxJQURELENBQ00sa0JBQVU7QUFDZCxZQUFHSSxPQUFPQyxRQUFQLElBQWlCLENBQXBCLEVBQXNCO0FBQ3BCQyxZQUFFLHVCQUFGLEVBQTJCQyxJQUEzQjs7QUFFQSxpQkFBS3JCLE1BQUwsR0FBY2tCLE9BQU9JLEVBQXJCO0FBQ0EsaUJBQUtyQixLQUFMLEdBQWFtQixFQUFFLFNBQUYsRUFBYUcsR0FBYixFQUFiO0FBQ0EsaUJBQUtwQixJQUFMLENBQVVHLE1BQVYsR0FBbUIsRUFBbkI7QUFDQSxpQkFBS0gsSUFBTCxDQUFVRSxhQUFWLEdBQTBCZSxFQUFFLFNBQUYsRUFBYUcsR0FBYixFQUExQjs7QUFFQUMscUJBQVdYLEdBQVgsQ0FBZSxrQkFBZ0JLLE9BQU9JLEVBQXRDLEVBQ0NSLElBREQsQ0FDTSxrQkFBVTtBQUNkLG1CQUFLZixRQUFMLEdBQWdCbUIsT0FBT3BCLElBQXZCO0FBQ0EsZ0JBQUcsT0FBS0MsUUFBTCxDQUFjMEIsTUFBZCxJQUFzQixDQUF6QixFQUEyQjtBQUN6QkwsZ0JBQUUsVUFBRixFQUFjTSxJQUFkLENBQW1CLE9BQUt4QixJQUFMLENBQVUsV0FBVixDQUFuQjtBQUNBa0IsZ0JBQUUsMkJBQUYsRUFBK0JPLElBQS9CO0FBQ0QsYUFIRCxNQUtFUCxFQUFFLGNBQUYsRUFBa0JPLElBQWxCO0FBQ0gsV0FURDtBQVdELFNBbkJELE1BbUJPO0FBQ0xDLGlCQUFPQyxLQUFQLENBQWEsT0FBSzNCLElBQUwsQ0FBVSxpQkFBVixDQUFiO0FBQ0Q7QUFDRixPQXhCRDtBQXlCRCxLQTNCSztBQTRCTjRCLGlCQTVCTSwyQkE0QlM7QUFBQTs7QUFDYixXQUFLM0IsSUFBTCxDQUFVYyxNQUFWLENBQWlCLE1BQWpCLEVBQXlCLG9CQUF6QixFQUNDSCxJQURELENBQ00sa0JBQVU7QUFDZCxZQUFHSSxPQUFPQyxRQUFQLElBQWlCLENBQXBCLEVBQXNCO0FBQ3BCQyxZQUFFLGdCQUFGLEVBQW9CQyxJQUFwQjtBQUNBRCxZQUFFLGVBQUYsRUFBbUJPLElBQW5CO0FBQ0QsU0FIRCxNQUdPLElBQUlULE9BQU9DLFFBQVAsSUFBaUIsQ0FBckIsRUFBdUI7QUFDNUJTLGlCQUFPQyxLQUFQLENBQWEsT0FBSzNCLElBQUwsQ0FBVSxzQkFBVixDQUFiO0FBQ0QsU0FGTSxNQUVBO0FBQ0wwQixpQkFBT0MsS0FBUCxDQUFhLE9BQUszQixJQUFMLENBQVUsaUJBQVYsQ0FBYjtBQUNEO0FBQ0YsT0FWRDtBQVdELEtBeENLO0FBeUNONkIsa0JBekNNLDRCQXlDVTtBQUFBOztBQUNkLFVBQUlSLE1BQU1ILEVBQUUsMkJBQUYsRUFBK0JHLEdBQS9CLEVBQVY7O0FBRUEsVUFBR0EsT0FBSyxDQUFSLEVBQVU7QUFDUkssZUFBT0MsS0FBUCxDQUFhLEtBQUszQixJQUFMLENBQVUsb0JBQVYsQ0FBYjtBQUNELE9BRkQsTUFFTztBQUNMc0IsbUJBQVdYLEdBQVgsQ0FBZSxxQkFBbUJVLEdBQW5CLEdBQXVCLEdBQXZCLEdBQTJCLEtBQUt2QixNQUEvQyxFQUFzRDtBQUNsRGdDLGtCQUFRO0FBQ056QixvQkFBT2EsRUFBRSxTQUFGLEVBQWFHLEdBQWI7QUFERDtBQUQwQyxTQUF0RCxFQUtDVCxJQUxELENBS00sa0JBQVU7QUFDZCxjQUFHSSxPQUFPcEIsSUFBUCxDQUFZcUIsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUN6QkMsY0FBRSxnQkFBRixFQUFvQkMsSUFBcEI7QUFDQUQsY0FBRSxlQUFGLEVBQW1CTyxJQUFuQjtBQUNELFdBSEQsTUFHTztBQUNMQyxtQkFBT0MsS0FBUCxDQUFhLE9BQUszQixJQUFMLENBQVUsa0JBQVYsQ0FBYjtBQUNEO0FBRUYsU0FiRDtBQWNEO0FBRUY7QUEvREs7QUFyQlEsQ0FBUixDQUFWLEMiLCJmaWxlIjoianMvcGFnZXMvcmVzZXQtcGFzc3dvcmQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDMzNSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjYzYjM3MGU5M2QwM2U4MzhmMzQiLCJ2YXIgYXBwID0gbmV3IFZ1ZSh7XG4gIGVsOiAnLnJlc2V0LXBhc3N3b3JkLWNvbnRhaW5lcicsXG4gIGRhdGE6IHtcbiAgICBxdWVzdGlvbjpbXSxcbiAgICB1c2VyaWQ6MCxcbiAgICBlbWFpbDonJyxcbiAgICBsYW5nOltdLFxuICAgIGZvcm06IG5ldyBGb3JtKHtcbiAgICAgICBwcmltYXJ5X2VtYWlsOicnLFxuICAgICAgIGVtYWlsMjonJyxcbiAgICAgICBhbnN3ZXI6JydcbiAgICB9LHsgYmFzZVVSTDogJ2h0dHA6Ly8nK0xhcmF2ZWwuYmFzZV9hcGlfdXJsIH0pXG4gIH0sXG5cbiAgY3JlYXRlZCgpe1xuICAgIGF4aW9zLmdldCgnL3RyYW5zbGF0ZS9wYXNzd29yZHMnKVxuICAgICAgLnRoZW4obGFuZyA9PiB7XG4gICAgICAgIHRoaXMubGFuZyA9IGxhbmcuZGF0YS5kYXRhO1xuICAgICAgfSlcbiAgfSxcblxuICBtZXRob2RzOntcbiAgICBzZWFyaEVtYWlsKCl7XG4gICAgICB0aGlzLmZvcm0uc3VibWl0KCdwb3N0JywgJy92MS9zZWFyY2hFbWFpbCcpXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICBpZihyZXN1bHQucmVzcG9uc2U9PTEpe1xuICAgICAgICAgICQoJyNzZWFyY2gsICNlbWFpbDItY29udCcpLmhpZGUoKTtcblxuICAgICAgICAgIHRoaXMudXNlcmlkID0gcmVzdWx0LmlkO1xuICAgICAgICAgIHRoaXMuZW1haWwgPSAkKCcjZW1haWwyJykudmFsKCk7XG4gICAgICAgICAgdGhpcy5mb3JtLmVtYWlsMiA9ICcnO1xuICAgICAgICAgIHRoaXMuZm9ybS5wcmltYXJ5X2VtYWlsID0gJCgnI2VtYWlsMicpLnZhbCgpO1xuXG4gICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9nZXRRdWVzdGlvbi8nK3Jlc3VsdC5pZClcbiAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgdGhpcy5xdWVzdGlvbiA9IHJlc3VsdC5kYXRhO1xuICAgICAgICAgICAgaWYodGhpcy5xdWVzdGlvbi5sZW5ndGg9PTApe1xuICAgICAgICAgICAgICAkKCcjbC1lbWFpbCcpLnRleHQodGhpcy5sYW5nWydhbHQtZW1haWwnXSk7XG4gICAgICAgICAgICAgICQoJyNlbWFpbDItY29udCwgI3NlYXJjaC1hbHQnKS5zaG93KCk7ICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgJCgnLmFuc3dlci1jb250Jykuc2hvdygpOyBcbiAgICAgICAgICB9KTtcblxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ2VtYWlsLWVycm9yLW1zZyddKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSxcbiAgICBzZWFyaEFsdEVtYWlsKCl7XG4gICAgICB0aGlzLmZvcm0uc3VibWl0KCdwb3N0JywgJy92MS9zZWFyY2hBbHRFbWFpbCcpXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICBpZihyZXN1bHQucmVzcG9uc2U9PTEpe1xuICAgICAgICAgICQoJyNxdWVzdGlvbi1jb250JykuaGlkZSgpO1xuICAgICAgICAgICQoJyNzdWNjZXNzLWNvbnQnKS5zaG93KCk7XG4gICAgICAgIH0gZWxzZSBpZiAocmVzdWx0LnJlc3BvbnNlPT0zKXtcbiAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydwbGVhc2Utc2V0LWFsdC1lbWFpbCddKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydlbWFpbC1lcnJvci1tc2cnXSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0sXG4gICAgYW5zd2VyUXVlc3Rpb24oKXtcbiAgICAgIHZhciB2YWwgPSAkKFwiI3F1ZXN0aW9uIG9wdGlvbjpzZWxlY3RlZFwiKS52YWwoKTtcbiAgICAgIFxuICAgICAgaWYodmFsPT0wKXtcbiAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZ1sncXVlc3Rpb24tZXJyb3ItbXNnJ10pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9hbnN3ZXJRdWVzdGlvbi8nK3ZhbCsnLycrdGhpcy51c2VyaWQse1xuICAgICAgICAgICAgcGFyYW1zOiB7XG4gICAgICAgICAgICAgIGFuc3dlcjokKCcjYW5zd2VyJykudmFsKClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICBpZihyZXN1bHQuZGF0YS5yZXNwb25zZT09MSl7XG4gICAgICAgICAgICAkKCcjcXVlc3Rpb24tY29udCcpLmhpZGUoKTtcbiAgICAgICAgICAgICQoJyNzdWNjZXNzLWNvbnQnKS5zaG93KCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmxhbmdbJ2Fuc3dlci1lcnJvci1tc2cnXSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIFxuICAgICAgICB9KTsgICAgICAgXG4gICAgICB9XG5cbiAgICB9XG4gIH1cbn0pO1xuXG5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvcmVzZXQtcGFzc3dvcmQuanMiXSwic291cmNlUm9vdCI6IiJ9