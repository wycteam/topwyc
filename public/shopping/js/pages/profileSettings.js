/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 442);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 192:
/***/ (function(module, exports, __webpack_require__) {

var user = window.Laravel.user;
var cpanelUrl = window.Laravel.cpanel_url;
var display_name = window.Laravel.user.display_name;
//checkbox value
var display_stat = "";
if (display_name == 'Real Name') {
    display_stat = false;
} else {
    display_stat = true;
}
var select = [];

Vue.component('que-answer', __webpack_require__(37));
Vue.component('notif-container', __webpack_require__(349));

var profileSettings = new Vue({
    el: '#profile-settings-container',

    data: {
        showSecurity: true,
        user: user,
        valueQue1: {},
        valueQue2: {},
        valueQue3: {},
        securityQuestions: [],
        securityAnswers: [],
        optionProv: [],
        optionCity: [],
        city: null,
        accInfo: new Form({
            secondary_email: user.secondary_email,
            birth_date: user.birth_date,
            nick_name: user.nick_name,
            gender: user.gender,
            is_display_name: user.is_display_name
        }, { baseURL: '/profile/update' }),

        addressInfo: new Form({
            address: '',
            barangay: '',
            city: '',
            province: '',
            zip_code: '',
            area_code: '',
            landmark: '',
            remarks: '',
            landline: '',
            mobile: ''
        }, { baseURL: '/profile/update' }),

        changePassword: new Form({
            old_password: '',
            new_password: '',
            new_password_confirmation: ''
        }, { baseURL: '/profile/update' }),

        securityQuestion: new Form({
            question: '',
            question1: '',
            question2: '',
            answer: '',
            answer1: '',
            answer2: ''
        }, { baseURL: '/profile/update' }),

        homeAddressInfo: new Form({
            address: '',
            barangay: '',
            city: '',
            province: '',
            zip_code: '',
            landmark: '',
            remarks: '',
            landline: '',
            area_code: '',
            mobile: ''
        }, { baseURL: '/profile/update' }),

        modalDocTitle: '',
        modalDocImage: '',
        notifications: [],
        securityLevel: 'progress-bar-danger low',

        nbi_status: null,
        birthCert_status: null,
        govID_status: null,
        sec_status: null,
        bir_status: null,
        permit_status: null,

        nbi_expiry: '',
        birthCert_expiry: '',
        govID_expiry: '',
        sec_expiry: '',
        bir_expiry: '',
        permit_expiry: '',

        nbi_file: '',
        birthCert_file: '',
        govID_file: '',
        sec_file: '',
        bir_file: '',
        permit_file: '',

        nbi_reason: '',
        birthCert_reason: '',
        govID_reason: '',
        sec_reason: '',
        bir_reason: '',
        permit_reason: '',

        docNbi: null,
        docBirthCert: null,
        docGovId: null,
        docSec: null,
        docBir: null,
        docPermit: null,

        baseHeight: 0,
        baseWidth: 0,
        baseZoom: 1,

        lang2: [],
        verified: 'ssd',
        notverified: 'ssss',
        optionGender: ['Male', 'Female']
    },

    ready: function ready() {
        this.$nextTick(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="tooltip"]').tooltip('fixTitle');
        });
    },

    components: {
        Multiselect: window.VueMultiselect.default
    },

    directives: {
        datepicker: {
            bind: function bind(el, binding, vnode) {
                $(el).datepicker({
                    format: 'yyyy-mm-dd'
                }).on('changeDate', function (e) {
                    profileSettings.$set(profileSettings.Info, 'birth_date', e.format('yyyy-mm-dd'));
                });
            }
        }
    },

    mounted: function mounted() {
        var _this = this;

        $('#modalDocsView').on('shown.bs.modal,show.bs.modal', function () {
            _this.baseZoom = 1;

            img = $('<img id="shittyimage" class="center-block" src="' + _this.modalDocImage + '">').on('load', function () {
                var $modalImg = $('<img src="' + _this.modalDocImage + '" class="center-block" >');
                $modalImg.appendTo('#doc-container');
                _this.baseHeight = $modalImg.height();
                _this.baseWidth = $modalImg.width();

                $modalImg.draggable();
            });
        });

        $('#modalDocsView').on('hide.bs.modal', function () {
            $('#doc-container').empty();
        });
    },


    watch: {
        baseZoom: function baseZoom(newValue) {
            var changedHeightSize = this.baseHeight * newValue;
            var changedWidthSize = this.baseWidth * newValue;
            $('#doc-container').find('img').css('height', changedHeightSize);
            $('#doc-container').find('img').css('width', changedWidthSize);
        },
        modalDocImage: function modalDocImage() {
            this.baseZoom = 1;
        }
    },

    methods: {
        lockUploadBtn: function lockUploadBtn(status, expire_date) {
            // if(status == 'Pending')
            // {
            //     return true;
            // }

            // if(status == 'Expired')
            // {
            //     return true;
            // }

            if (status == 'Verified') {
                var expiry_date = moment(expire_date);

                if (parseInt(expiry_date.diff(moment(), 'days')) <= 30) {
                    return false;
                } else {
                    return true;
                }
            }

            return false;
        },

        labelColor: function labelColor(status) {
            switch (status) {
                case 'Pending':
                    return 'label-default';

                    break;
                case 'Expired':
                    $('[data-toggle="tooltip"]').tooltip('fixTitle');
                case 'Denied':
                    return 'label-danger';

                    break;
                case 'Verified':
                    return 'label-success';
                    break;
            }
        },
        //show modals
        shipAddModal: function shipAddModal(event) {
            var elShow = event.target.name;
            $('#' + elShow + 2).modal('toggle');
        },
        openDocModal: function openDocModal(title, filename) {
            this.modalDocTitle = title.toUpperCase();

            this.modalDocImage = '/images/documents/' + user.id + '/' + filename;
            $('#modalDocsView').modal('toggle');
        },
        //save account info
        saveInfo: function saveInfo(event) {
            var _this2 = this;

            this.accInfo.submit('post', '/account-info').then(function (result) {
                _this2.accInfo = new Form({
                    secondary_email: result.secondary_email,
                    birth_date: result.birth_date,
                    nick_name: result.nick_name,
                    gender: result.gender,
                    is_display_name: result.is_display_name
                }, { baseURL: '/profile/update' });

                $('#nickname').text(result.nick_name);

                toastr.success(_this2.lang2.data['prof-updated-msg']);
            }).catch(function (error) {
                toastr.error(_this2.lang2.data['update-failed']);
            });
        },

        fnchangePassword: function fnchangePassword(event) {
            var _this3 = this;

            this.changePassword.submit('post', '/change-password').then(function (result) {
                toastr.success(_this3.lang2.data['pw-changed-msg']);
            }).catch(function (error) {
                toastr.error(_this3.lang2.data['update-failed']);
            });
        },

        saveSecurityQuestion: function saveSecurityQuestion(event) {
            var _this4 = this;

            this.securityQuestion.submit('post', '/answer-security-questions').then(function (result) {
                toastr.success(_this4.lang2.data['succ-upload']);
                _this4.getSecurityAnswers();
                _this4.getSecurityLevel();
            }).catch(function (error) {
                toastr.error(_this4.lang2.data['update-failed']);
            });
        },

        saveNewAddress: function saveNewAddress(event) {
            this.addressInfo.submit('post', '/address-info');
        },

        getSecurityAnswers: function getSecurityAnswers() {
            var _this5 = this;

            axios.get('/profile/security-answers').then(function (result) {
                _this5.securityAnswers = result.data.questions;
                if (_this5.securityAnswers.length) {
                    _this5.showSecurity = false;
                }
                _this5.$nextTick(function () {
                    this.getSecurityLevel();
                });
            });
        },


        getCity: function getCity(data) {
            var _this6 = this;

            this.homeAddressInfo.city = '';
            axios.get('/profile/city/' + data).then(function (result) {
                var array = [];
                for (var i = 0; i < result.data.length; i++) {

                    array.push(result.data[i]);
                }
                _this6.optionCity = array;
            });
        },

        getAreaCode: function getAreaCode(data) {
            this.homeAddressInfo.city = data;
            this.homeAddressInfo.area_code = this.padAreaCode(data.area_code);
        },

        saveHomeAddress: function saveHomeAddress(event) {
            var _this7 = this;

            this.homeAddressInfo.city = this.city.name;
            this.homeAddressInfo.submit('post', '/home-address-info', this.homeAddressInfo).then(function (result) {
                toastr.success(_this7.lang2.data['succ-upload']);
            }).catch(function (error) {
                toastr.error(_this7.lang2.data['update-failed']);
            });
        },

        changeSelect: function changeSelect(selectedOption, id) {
            if (id == "question") {
                select[0] = selectedOption;
            } else if (id == "question1") {
                select[1] = selectedOption;
            } else if (id == "question2") {
                select[2] = selectedOption;
            }
            var options = this.securityQuestions;
            options = options.map(function (question) {
                question.$isDisabled = false;
                return question;
            });

            $(select).each(function (i) {
                options = options.map(function (question) {
                    if (select[i].id == question.id) {
                        question.$isDisabled = true;
                    }
                    return question;
                });
            });
        },
        getDocuments: function getDocuments() {
            var _this8 = this;

            axiosAPIv1.get('/users/' + Laravel.user.id + '/documents').then(function (docsObj) {
                if (docsObj['status'] == 200) {
                    nbi_obj = docsObj.data.filter(function (obj) {
                        return obj.type === 'nbi';
                    });
                    birthCert_obj = docsObj.data.filter(function (obj) {
                        return obj.type === 'birthCert';
                    });
                    govID_obj = docsObj.data.filter(function (obj) {
                        return obj.type === 'govId';
                    });
                    sec_obj = docsObj.data.filter(function (obj) {
                        return obj.type === 'sec';
                    });
                    bir_obj = docsObj.data.filter(function (obj) {
                        return obj.type === 'bir';
                    });
                    permit_obj = docsObj.data.filter(function (obj) {
                        return obj.type === 'permit';
                    });

                    if (nbi_obj.length) {
                        _this8.nbi_status = nbi_obj[0].status;
                        _this8.nbi_expiry = nbi_obj[0].expires_at;
                        _this8.nbi_reason = nbi_obj[0].reason;
                        _this8.nbi_file = nbi_obj[0].name;
                        if (_this8.nbi_status == 'Expired') {
                            _this8.docNbi = false;
                        } else {
                            _this8.docNbi = true;
                        }
                    }

                    if (birthCert_obj.length) {
                        _this8.birthCert_status = birthCert_obj[0].status;
                        _this8.birthCert_expiry = birthCert_obj[0].expires_at;
                        _this8.birthCert_reason = birthCert_obj[0].reason;
                        _this8.birthCert_file = birthCert_obj[0].name;
                        _this8.docBirthCert = true;
                    }

                    if (govID_obj.length) {
                        _this8.govID_status = govID_obj[0].status;
                        _this8.govID_expiry = govID_obj[0].expires_at;
                        _this8.govID_reason = govID_obj[0].reason;
                        _this8.govID_file = govID_obj[0].name;
                        _this8.docGovId = true;
                    }

                    if (sec_obj.length) {
                        _this8.sec_status = sec_obj[0].status;
                        _this8.sec_expiry = sec_obj[0].expires_at;
                        _this8.sec_reason = sec_obj[0].reason;
                        _this8.sec_file = sec_obj[0].name;
                        _this8.docSec = true;
                    }

                    if (bir_obj.length) {
                        _this8.bir_status = bir_obj[0].status;
                        _this8.bir_expiry = bir_obj[0].expires_at;
                        _this8.bir_reason = bir_obj[0].reason;
                        _this8.bir_file = bir_obj[0].name;
                        _this8.docBir = true;
                    }

                    if (permit_obj.length) {
                        _this8.permit_status = permit_obj[0].status;
                        _this8.permit_expiry = permit_obj[0].expires_at;
                        _this8.permit_reason = permit_obj[0].reason;
                        _this8.permit_file = permit_obj[0].name;
                        _this8.docPermit = true;
                    }

                    _this8.getSecurityLevel();
                    _this8.$nextTick(function () {
                        $('[data-toggle="tooltip"]').tooltip('fixTitle');
                    });
                }
            }).catch(function (error) {
                // toastr.error('Updated Failed.');
            });
        },
        getSecurityLevel: function getSecurityLevel() {

            var is_docsSubmitted = true;

            is_docsSubmitted = (this.nbi_status == 'Verified') * (this.govID_status == 'Verified') || (this.sec_status == 'Verified') * (this.bir_status == 'Verified') * (this.permit_status == 'Verified');

            if (this.securityAnswers.length && is_docsSubmitted) {
                this.securityLevel = 'progress-bar-success high';
            } else if (this.securityAnswers.length || is_docsSubmitted) {
                this.securityLevel = 'progress-bar-warning med';
            } else {
                this.securityLevel = 'progress-bar-danger low';
            }
        },
        padAreaCode: function padAreaCode(n) {
            if (n) {
                return ('00000' + n).slice(-5);
            } else {
                return '';
            }
        }
    },

    created: function created() {
        var _this9 = this;

        axios.get('/translate/profile-settings').then(function (result) {
            _this9.lang2 = result.data;
        });
        //individual
        Event.listen('Imgfileupload.nbiUpload', function (data) {
            if (data) {
                axiosAPIv1.get('/users/' + Laravel.user.id + '/documents?doctype=nbi').then(function (docsObj) {
                    if (docsObj.data.length) {
                        _this9.docNbi = true;
                        _this9.nbi_status = docsObj.data[0].status;
                        _this9.nbi_file = docsObj.data[0].name;
                        toastr.success(_this9.lang2.data['upload'], _this9.lang2.data['succ-upload']);
                    } else {
                        toastr.error(_this9.lang2.data['upload'], _this9.lang2.data['try-again']);
                    }
                });
                content = $('.btnNbiUpload').find('.btn-content');
                loader = $('.btnNbiUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            } else {
                content = $('.btnNbiUpload').find('.btn-content');
                loader = $('.btnNbiUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            }
        });
        Event.listen('Imgfileupload.birthCertUpload', function (data) {
            if (data) {
                axiosAPIv1.get('/users/' + Laravel.user.id + '/documents?doctype=birthCert').then(function (docsObj) {
                    if (docsObj.data.length) {
                        _this9.docBirthCert = true;
                        _this9.birthCert_status = docsObj.data[0].status;
                        _this9.birthCert_file = docsObj.data[0].name;
                        toastr.success(_this9.lang2.data['upload'], _this9.lang2.data['succ-upload']);
                    } else {
                        toastr.error(_this9.lang2.data['upload'], _this9.lang2.data['try-again']);
                    }
                });
                content = $('.btnBirthCertUpload').find('.btn-content');
                loader = $('.btnBirthCertUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            } else {
                content = $('.btnBirthCertUpload').find('.btn-content');
                loader = $('.btnBirthCertUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            }
        });
        Event.listen('Imgfileupload.govIdUpload', function (data) {
            if (data) {
                axiosAPIv1.get('/users/' + Laravel.user.id + '/documents?doctype=govId').then(function (docsObj) {
                    console.log(docsObj);
                    if (docsObj.data.length) {
                        _this9.docGovId = true;
                        _this9.govID_status = docsObj.data[0].status;
                        _this9.govID_file = docsObj.data[0].name;
                        toastr.success(_this9.lang2.data['upload'], _this9.lang2.data['succ-upload']);
                    } else {
                        toastr.error(_this9.lang2.data['upload'], _this9.lang2.data['try-again']);
                    }
                });
                content = $('.btnGovIdUpload').find('.btn-content');
                loader = $('.btnGovIdUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            } else {
                content = $('.btnGovIdUpload').find('.btn-content');
                loader = $('.btnGovIdUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            }
        }
        // enterprise
        );Event.listen('Imgfileupload.secUpload', function (data) {
            if (data) {
                axiosAPIv1.get('/users/' + Laravel.user.id + '/documents?doctype=sec').then(function (docsObj) {
                    if (docsObj.data.length) {
                        _this9.docSec = true;
                        _this9.sec_status = docsObj.data[0].status;
                        _this9.sec_file = docsObj.data[0].name;
                        toastr.success(_this9.lang2.data['upload'], _this9.lang2.data['succ-upload']);
                    } else {
                        toastr.error(_this9.lang2.data['upload'], _this9.lang2.data['try-again']);
                    }
                });
                content = $('.btnSecUpload').find('.btn-content');
                loader = $('.btnSecUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            } else {
                content = $('.btnSecUpload').find('.btn-content');
                loader = $('.btnSecUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            }
        });
        Event.listen('Imgfileupload.birUpload', function (data) {
            if (data) {
                axiosAPIv1.get('/users/' + Laravel.user.id + '/documents?doctype=bir').then(function (docsObj) {
                    if (docsObj.data.length) {
                        _this9.docBir = true;
                        _this9.bir_status = docsObj.data[0].status;
                        _this9.bir_file = docsObj.data[0].name;
                        toastr.success(_this9.lang2.data['upload'], _this9.lang2.data['succ-upload']);
                    } else {
                        toastr.error(_this9.lang2.data['upload'], _this9.lang2.data['try-again']);
                    }
                });
                content = $('.btnBirUpload').find('.btn-content');
                loader = $('.btnBirUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            } else {
                content = $('.btnBirUpload').find('.btn-content');
                loader = $('.btnBirUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            }
        });
        Event.listen('Imgfileupload.permitUpload', function (data) {
            if (data) {
                axiosAPIv1.get('/users/' + Laravel.user.id + '/documents?doctype=permit').then(function (docsObj) {
                    if (docsObj.data.length) {
                        _this9.docPermit = true;
                        _this9.permit_status = docsObj.data[0].status;
                        _this9.permit_file = docsObj.data[0].name;
                        toastr.success(_this9.lang2.data['upload'], _this9.lang2.data['succ-upload']);
                    } else {
                        toastr.error(_this9.lang2.data['upload'], _this9.lang2.data['try-again']);
                    }
                });
                content = $('.btnPermitUpload').find('.btn-content');
                loader = $('.btnPermitUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            } else {
                content = $('.btnPermitUpload').find('.btn-content');
                loader = $('.btnPermitUpload').find('.loader');
                loader.fadeOut(100, function () {
                    content.fadeIn(100);
                });
            }
        });

        axios.get('/profile/notifications').then(function (result) {
            _this9.notifications = result.data;
        });

        axios.get('/profile/security-questions').then(function (result) {
            _this9.securityQuestions = result.data;
        });

        axios.get('/profile/get-home-address').then(function (result) {
            if (result.data.mobile != '' && result.data.landline != '' && result.data.residence != null) {
                _this9.homeAddressInfo = new Form($.extend({
                    mobile: result.data.mobile,
                    landline: result.data.landline,
                    area_code: result.data.area_code,
                    residence: result.data.residence
                }, result.data.residence), { baseURL: '/profile/update' });
                axios.get('/profile/city/' + _this9.homeAddressInfo.province).then(function (result) {
                    _this9.optionCity = result.data;
                    var cityObj = _this9.optionCity.filter(function (obj) {
                        return _this9.homeAddressInfo.city == obj.name;
                    });
                    _this9.city = cityObj[0];
                });
            } else {
                homeAddressInfo: new Form({
                    address: '',
                    barangay: '',
                    city: '',
                    province: '',
                    zip_code: '',
                    area_code: '',
                    landmark: '',
                    remarks: '',
                    landline: '',
                    mobile: ''
                }, { baseURL: '/profile/update' });
            }
        });

        this.getSecurityAnswers();

        axios.get('/profile/provinces').then(function (result) {
            var array = [];
            for (var i = 0; i < result.data.length; i++) {
                array.push(result.data[i].name);
            }
            _this9.optionProv = array;
        });
        this.getDocuments();
    },


    computed: {
        answerEmpty: function answerEmpty() {
            return jQuery.isEmptyObject(this.securityAnswers);
        }
    }

});

function removeLoading(input, container) {
    if ($(input).val() == '') {
        content = $(container).find('.btn-content');
        loader = $(container).find('.loader');
        loader.fadeOut(100, function () {
            content.fadeIn(100);
        });
    }
}

$(document).ready(function () {

    //code to go to tab using url with #
    var url = window.location.href;
    if (/#/.test(url)) {
        var activeTab = url.substring(url.indexOf("#") + 1);
        $(".tab-pane").removeClass("active in");
        $('a[href="#' + activeTab + '"]').tab('show');
    }
    //console.log(activeTab);


    $('.nbi-uploader input').on('click', function () {
        $('body').one('mousemove', function () {
            removeLoading('.nbi-uploader input', '.btnNbiUpload');
        });
    });
    $('.birthCert-uploader input').on('click', function () {
        $('body').one('mousemove', function () {
            removeLoading('.birthCert-uploader input', '.btnBirthCertUpload');
        });
    });
    $('.govID-uploader input').on('click', function () {
        $('body').one('mousemove', function () {
            removeLoading('.govID-uploader input', '.btnGovIdUpload');
        });
    });
    $('.sec-uploader input').on('click', function () {
        $('body').one('mousemove', function () {
            removeLoading('.sec-uploader input', '.btnSecUpload');
        });
    });
    $('.bir-uploader input').on('click', function () {
        $('body').one('mousemove', function () {
            removeLoading('.bir-uploader input', '.btnBirUpload');
        });
    });
    $('.permit-uploader input').on('click', function () {
        $('body').one('mousemove', function () {
            removeLoading('.permit-uploader input', '.btnPermitUpload');
        });
    });

    $('#txt-home-area-code').inputmask({
        mask: '99999'
    });

    $('#txt-home-landline').inputmask({
        mask: '999-9999'
    });

    $('#txt-home-mobile').inputmask({
        mask: '(9999) 999-9999'
    });

    $('#txt-ship-zc').inputmask({
        mask: '99999'
    });

    $('.tile'
    // tile mouse actions
    ).on('mouseover', function () {
        $(this).children('.photo').css({ 'transform': 'scale(' + $(this).attr('data-scale') + ')' });
    }).on('mouseout', function () {
        $(this).children('.photo').css({ 'transform': 'scale(1)' });
    }).on('mousemove', function (e) {
        $(this).children('.photo').css({
            'transform-origin': (e.pageX - $(this).offset().left) / $(this).width() * 100 + '% ' + (e.pageY - $(this).offset().top) / $(this).height() * 100 + '%'
        });
    });
});

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['item']
});

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['notif'],
	data: function data() {
		return {
			link: "",
			notif_type: '',
			icon: '',
			iconColor: ''
		};
	},
	created: function created() {
		this.notif_type = this.notif.type.replace("App\\Notifications\\", "");
		var cpanelUrl = window.Laravel.cpanel_url;
		switch (this.notif_type) {
			case "NewChatMessage":
				break;
			case "NewDocument":
				this.link = "http://" + cpanelUrl + '/documents';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "NewEwalletTransaction":
				this.link = "http://" + cpanelUrl + '/ewallet';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "NewIssue":
				this.link = "http://" + cpanelUrl + '/customer-service';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "ChangeEwalletTransactionStatus":
				this.link = 'user/ewallet';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "NewPurchase":
				this.link = '/user/sales-report';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "OrderStatusUpdate":
				var lnk = '';
				if (!!~this.notif.data.body.search('requested to return your item')) {
					lnk = '/user/sales-report';
				} else if (!!~this.notif.data.body.search('successfully received the item')) {
					lnk = '/user/sales-report';
				} else {
					lnk = '/user/purchase-report';
				}
				console.log(lnk);
				this.link = lnk;
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "DocumentApproved":
				this.link = '/profile#documents';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "NewFriendRequest":
				this.link = '/friends';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "FeedbackComment":
				this.link = '/feedback#';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
			case "ProductReviewComment":
				this.link = '/product/';
				this.icon = "fa-user";
				this.iconColor = "tc-green";
				break;
		}
	}
});

/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(284),
  /* template */
  __webpack_require__(401),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\profile\\NotificationContainer.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] NotificationContainer.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-cac896d8", Component.options)
  } else {
    hotAPI.reload("data-v-cac896d8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(21),
  /* template */
  __webpack_require__(41),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\profile\\SecurityQuestionAnswer.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] SecurityQuestionAnswer.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5feabad6", Component.options)
  } else {
    hotAPI.reload("data-v-5feabad6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 401:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.notif_type != "NewChatMessage") ? _c('li', {
    staticClass: "col-md-12 ",
    class: _vm.notif.read_at ? "" : "not-read"
  }, [_c('a', {
    attrs: {
      "href": _vm.link,
      "target": "_blank"
    }
  }, [_c('div', {
    staticClass: "image"
  }, [_c('img', {
    attrs: {
      "src": _vm.notif.data.image
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('span', [_c('strong', [_vm._v(_vm._s(_vm.notif.data.title))])]), _vm._v(" "), _c('br'), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.notif.data.body))]), _vm._v(" "), _c('br'), _vm._v(" "), _c('span', [_c('small', [_vm._v(_vm._s(_vm.notif.human_created_at))])])])])]) : _vm._e()
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-cac896d8", module.exports)
  }
}

/***/ }),

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-sm-12 security-question-set"
  }, [_c('div', {
    staticClass: "col-sm-8"
  }, [_c('label', {
    staticClass: "account-label question-label"
  }, [_vm._v(_vm._s(_vm.item.question))])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-4"
  }, [_c('label', {
    staticClass: "account-label answer-label"
  }, [_vm._v(_vm._s(this.$parent.lang2.data['ans-on-file']))]), _vm._v(" "), _c('p')])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5feabad6", module.exports)
  }
}

/***/ }),

/***/ 442:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(192);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3Byb2ZpbGVTZXR0aW5ncy5qcyIsIndlYnBhY2s6Ly8vU2VjdXJpdHlRdWVzdGlvbkFuc3dlci52dWUiLCJ3ZWJwYWNrOi8vL05vdGlmaWNhdGlvbkNvbnRhaW5lci52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3Byb2ZpbGUvTm90aWZpY2F0aW9uQ29udGFpbmVyLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZmlsZS9TZWN1cml0eVF1ZXN0aW9uQW5zd2VyLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZmlsZS9Ob3RpZmljYXRpb25Db250YWluZXIudnVlPzA1OWYiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3Byb2ZpbGUvU2VjdXJpdHlRdWVzdGlvbkFuc3dlci52dWU/YWQxNSJdLCJuYW1lcyI6WyJ1c2VyIiwid2luZG93IiwiTGFyYXZlbCIsImNwYW5lbFVybCIsImNwYW5lbF91cmwiLCJkaXNwbGF5X25hbWUiLCJkaXNwbGF5X3N0YXQiLCJzZWxlY3QiLCJWdWUiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwicHJvZmlsZVNldHRpbmdzIiwiZWwiLCJkYXRhIiwic2hvd1NlY3VyaXR5IiwidmFsdWVRdWUxIiwidmFsdWVRdWUyIiwidmFsdWVRdWUzIiwic2VjdXJpdHlRdWVzdGlvbnMiLCJzZWN1cml0eUFuc3dlcnMiLCJvcHRpb25Qcm92Iiwib3B0aW9uQ2l0eSIsImNpdHkiLCJhY2NJbmZvIiwiRm9ybSIsInNlY29uZGFyeV9lbWFpbCIsImJpcnRoX2RhdGUiLCJuaWNrX25hbWUiLCJnZW5kZXIiLCJpc19kaXNwbGF5X25hbWUiLCJiYXNlVVJMIiwiYWRkcmVzc0luZm8iLCJhZGRyZXNzIiwiYmFyYW5nYXkiLCJwcm92aW5jZSIsInppcF9jb2RlIiwiYXJlYV9jb2RlIiwibGFuZG1hcmsiLCJyZW1hcmtzIiwibGFuZGxpbmUiLCJtb2JpbGUiLCJjaGFuZ2VQYXNzd29yZCIsIm9sZF9wYXNzd29yZCIsIm5ld19wYXNzd29yZCIsIm5ld19wYXNzd29yZF9jb25maXJtYXRpb24iLCJzZWN1cml0eVF1ZXN0aW9uIiwicXVlc3Rpb24iLCJxdWVzdGlvbjEiLCJxdWVzdGlvbjIiLCJhbnN3ZXIiLCJhbnN3ZXIxIiwiYW5zd2VyMiIsImhvbWVBZGRyZXNzSW5mbyIsIm1vZGFsRG9jVGl0bGUiLCJtb2RhbERvY0ltYWdlIiwibm90aWZpY2F0aW9ucyIsInNlY3VyaXR5TGV2ZWwiLCJuYmlfc3RhdHVzIiwiYmlydGhDZXJ0X3N0YXR1cyIsImdvdklEX3N0YXR1cyIsInNlY19zdGF0dXMiLCJiaXJfc3RhdHVzIiwicGVybWl0X3N0YXR1cyIsIm5iaV9leHBpcnkiLCJiaXJ0aENlcnRfZXhwaXJ5IiwiZ292SURfZXhwaXJ5Iiwic2VjX2V4cGlyeSIsImJpcl9leHBpcnkiLCJwZXJtaXRfZXhwaXJ5IiwibmJpX2ZpbGUiLCJiaXJ0aENlcnRfZmlsZSIsImdvdklEX2ZpbGUiLCJzZWNfZmlsZSIsImJpcl9maWxlIiwicGVybWl0X2ZpbGUiLCJuYmlfcmVhc29uIiwiYmlydGhDZXJ0X3JlYXNvbiIsImdvdklEX3JlYXNvbiIsInNlY19yZWFzb24iLCJiaXJfcmVhc29uIiwicGVybWl0X3JlYXNvbiIsImRvY05iaSIsImRvY0JpcnRoQ2VydCIsImRvY0dvdklkIiwiZG9jU2VjIiwiZG9jQmlyIiwiZG9jUGVybWl0IiwiYmFzZUhlaWdodCIsImJhc2VXaWR0aCIsImJhc2Vab29tIiwibGFuZzIiLCJ2ZXJpZmllZCIsIm5vdHZlcmlmaWVkIiwib3B0aW9uR2VuZGVyIiwicmVhZHkiLCIkbmV4dFRpY2siLCIkIiwidG9vbHRpcCIsImNvbXBvbmVudHMiLCJNdWx0aXNlbGVjdCIsIlZ1ZU11bHRpc2VsZWN0IiwiZGVmYXVsdCIsImRpcmVjdGl2ZXMiLCJkYXRlcGlja2VyIiwiYmluZCIsImJpbmRpbmciLCJ2bm9kZSIsImZvcm1hdCIsIm9uIiwiZSIsIiRzZXQiLCJJbmZvIiwibW91bnRlZCIsImltZyIsIiRtb2RhbEltZyIsImFwcGVuZFRvIiwiaGVpZ2h0Iiwid2lkdGgiLCJkcmFnZ2FibGUiLCJlbXB0eSIsIndhdGNoIiwibmV3VmFsdWUiLCJjaGFuZ2VkSGVpZ2h0U2l6ZSIsImNoYW5nZWRXaWR0aFNpemUiLCJmaW5kIiwiY3NzIiwibWV0aG9kcyIsImxvY2tVcGxvYWRCdG4iLCJzdGF0dXMiLCJleHBpcmVfZGF0ZSIsImV4cGlyeV9kYXRlIiwibW9tZW50IiwicGFyc2VJbnQiLCJkaWZmIiwibGFiZWxDb2xvciIsInNoaXBBZGRNb2RhbCIsImV2ZW50IiwiZWxTaG93IiwidGFyZ2V0IiwibmFtZSIsIm1vZGFsIiwib3BlbkRvY01vZGFsIiwidGl0bGUiLCJmaWxlbmFtZSIsInRvVXBwZXJDYXNlIiwiaWQiLCJzYXZlSW5mbyIsInN1Ym1pdCIsInRoZW4iLCJyZXN1bHQiLCJ0ZXh0IiwidG9hc3RyIiwic3VjY2VzcyIsImNhdGNoIiwiZXJyb3IiLCJmbmNoYW5nZVBhc3N3b3JkIiwic2F2ZVNlY3VyaXR5UXVlc3Rpb24iLCJnZXRTZWN1cml0eUFuc3dlcnMiLCJnZXRTZWN1cml0eUxldmVsIiwic2F2ZU5ld0FkZHJlc3MiLCJheGlvcyIsImdldCIsInF1ZXN0aW9ucyIsImxlbmd0aCIsImdldENpdHkiLCJhcnJheSIsImkiLCJwdXNoIiwiZ2V0QXJlYUNvZGUiLCJwYWRBcmVhQ29kZSIsInNhdmVIb21lQWRkcmVzcyIsImNoYW5nZVNlbGVjdCIsInNlbGVjdGVkT3B0aW9uIiwib3B0aW9ucyIsIm1hcCIsIiRpc0Rpc2FibGVkIiwiZWFjaCIsImdldERvY3VtZW50cyIsImF4aW9zQVBJdjEiLCJkb2NzT2JqIiwibmJpX29iaiIsImZpbHRlciIsIm9iaiIsInR5cGUiLCJiaXJ0aENlcnRfb2JqIiwiZ292SURfb2JqIiwic2VjX29iaiIsImJpcl9vYmoiLCJwZXJtaXRfb2JqIiwiZXhwaXJlc19hdCIsInJlYXNvbiIsImlzX2RvY3NTdWJtaXR0ZWQiLCJuIiwic2xpY2UiLCJjcmVhdGVkIiwiRXZlbnQiLCJsaXN0ZW4iLCJjb250ZW50IiwibG9hZGVyIiwiZmFkZU91dCIsImZhZGVJbiIsImNvbnNvbGUiLCJsb2ciLCJyZXNpZGVuY2UiLCJleHRlbmQiLCJjaXR5T2JqIiwiY29tcHV0ZWQiLCJhbnN3ZXJFbXB0eSIsImpRdWVyeSIsImlzRW1wdHlPYmplY3QiLCJyZW1vdmVMb2FkaW5nIiwiaW5wdXQiLCJjb250YWluZXIiLCJ2YWwiLCJkb2N1bWVudCIsInVybCIsImxvY2F0aW9uIiwiaHJlZiIsInRlc3QiLCJhY3RpdmVUYWIiLCJzdWJzdHJpbmciLCJpbmRleE9mIiwicmVtb3ZlQ2xhc3MiLCJ0YWIiLCJvbmUiLCJpbnB1dG1hc2siLCJtYXNrIiwiY2hpbGRyZW4iLCJhdHRyIiwicGFnZVgiLCJvZmZzZXQiLCJsZWZ0IiwicGFnZVkiLCJ0b3AiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2xEQSxJQUFJQSxPQUFPQyxPQUFPQyxPQUFQLENBQWVGLElBQTFCO0FBQ0EsSUFBSUcsWUFBWUYsT0FBT0MsT0FBUCxDQUFlRSxVQUEvQjtBQUNBLElBQUlDLGVBQWVKLE9BQU9DLE9BQVAsQ0FBZUYsSUFBZixDQUFvQkssWUFBdkM7QUFDQTtBQUNBLElBQUlDLGVBQWUsRUFBbkI7QUFDQyxJQUFHRCxnQkFBZ0IsV0FBbkIsRUFBK0I7QUFDNUJDLG1CQUFlLEtBQWY7QUFDRixDQUZELE1BRUs7QUFDRkEsbUJBQWUsSUFBZjtBQUNGO0FBQ0YsSUFBSUMsU0FBUyxFQUFiOztBQUVBQyxJQUFJQyxTQUFKLENBQWMsWUFBZCxFQUE2QixtQkFBQUMsQ0FBUSxFQUFSLENBQTdCO0FBQ0FGLElBQUlDLFNBQUosQ0FBYyxpQkFBZCxFQUFrQyxtQkFBQUMsQ0FBUSxHQUFSLENBQWxDOztBQUlBLElBQUlDLGtCQUFrQixJQUFJSCxHQUFKLENBQVE7QUFDNUJJLFFBQUksNkJBRHdCOztBQUc1QkMsVUFBTTtBQUNKQyxzQkFBYyxJQURWO0FBRUpkLGNBQUtBLElBRkQ7QUFHSmUsbUJBQVUsRUFITjtBQUlKQyxtQkFBVSxFQUpOO0FBS0pDLG1CQUFVLEVBTE47QUFNSkMsMkJBQW1CLEVBTmY7QUFPSkMseUJBQWlCLEVBUGI7QUFRSkMsb0JBQVksRUFSUjtBQVNKQyxvQkFBWSxFQVRSO0FBVUpDLGNBQU0sSUFWRjtBQVdKQyxpQkFBUyxJQUFJQyxJQUFKLENBQVM7QUFDZkMsNkJBQWlCekIsS0FBS3lCLGVBRFA7QUFFZkMsd0JBQVkxQixLQUFLMEIsVUFGRjtBQUdmQyx1QkFBVzNCLEtBQUsyQixTQUhEO0FBSWZDLG9CQUFRNUIsS0FBSzRCLE1BSkU7QUFLZkMsNkJBQWlCN0IsS0FBSzZCO0FBTFAsU0FBVCxFQU1QLEVBQUVDLFNBQVMsaUJBQVgsRUFOTyxDQVhMOztBQW1CSkMscUJBQWEsSUFBSVAsSUFBSixDQUFTO0FBQ2xCUSxxQkFBYyxFQURJO0FBRWxCQyxzQkFBYyxFQUZJO0FBR2xCWCxrQkFBYyxFQUhJO0FBSWxCWSxzQkFBYyxFQUpJO0FBS2xCQyxzQkFBYyxFQUxJO0FBTWxCQyx1QkFBYyxFQU5JO0FBT2xCQyxzQkFBYyxFQVBJO0FBUWxCQyxxQkFBYyxFQVJJO0FBU2xCQyxzQkFBYyxFQVRJO0FBVWxCQyxvQkFBYztBQVZJLFNBQVQsRUFXWCxFQUFFVixTQUFXLGlCQUFiLEVBWFcsQ0FuQlQ7O0FBZ0NKVyx3QkFBZ0IsSUFBSWpCLElBQUosQ0FBUztBQUN0QmtCLDBCQUFlLEVBRE87QUFFdEJDLDBCQUFlLEVBRk87QUFHdEJDLHVDQUE0QjtBQUhOLFNBQVQsRUFJZCxFQUFFZCxTQUFRLGlCQUFWLEVBSmMsQ0FoQ1o7O0FBc0NKZSwwQkFBa0IsSUFBSXJCLElBQUosQ0FBUztBQUN4QnNCLHNCQUFZLEVBRFk7QUFFeEJDLHVCQUFZLEVBRlk7QUFHeEJDLHVCQUFZLEVBSFk7QUFJeEJDLG9CQUFZLEVBSlk7QUFLeEJDLHFCQUFZLEVBTFk7QUFNeEJDLHFCQUFZO0FBTlksU0FBVCxFQU9oQixFQUFFckIsU0FBUSxpQkFBVixFQVBnQixDQXRDZDs7QUErQ0pzQix5QkFBaUIsSUFBSTVCLElBQUosQ0FBUztBQUNwQlEscUJBQWMsRUFETTtBQUVwQkMsc0JBQWMsRUFGTTtBQUdwQlgsa0JBQWMsRUFITTtBQUlwQlksc0JBQWMsRUFKTTtBQUtwQkMsc0JBQWMsRUFMTTtBQU1wQkUsc0JBQWMsRUFOTTtBQU9wQkMscUJBQWMsRUFQTTtBQVFwQkMsc0JBQWMsRUFSTTtBQVNwQkgsdUJBQWMsRUFUTTtBQVVwQkksb0JBQWM7QUFWTSxTQUFULEVBV2YsRUFBRVYsU0FBVyxpQkFBYixFQVhlLENBL0NiOztBQTRESnVCLHVCQUFjLEVBNURWO0FBNkRKQyx1QkFBYyxFQTdEVjtBQThESkMsdUJBQWMsRUE5RFY7QUErREpDLHVCQUFjLHlCQS9EVjs7QUFpRUpDLG9CQUFpQixJQWpFYjtBQWtFSkMsMEJBQWlCLElBbEViO0FBbUVKQyxzQkFBaUIsSUFuRWI7QUFvRUpDLG9CQUFpQixJQXBFYjtBQXFFSkMsb0JBQWlCLElBckViO0FBc0VKQyx1QkFBaUIsSUF0RWI7O0FBd0VKQyxvQkFBaUIsRUF4RWI7QUF5RUpDLDBCQUFpQixFQXpFYjtBQTBFSkMsc0JBQWlCLEVBMUViO0FBMkVKQyxvQkFBaUIsRUEzRWI7QUE0RUpDLG9CQUFpQixFQTVFYjtBQTZFSkMsdUJBQWlCLEVBN0ViOztBQStFSkMsa0JBQWlCLEVBL0ViO0FBZ0ZKQyx3QkFBaUIsRUFoRmI7QUFpRkpDLG9CQUFpQixFQWpGYjtBQWtGSkMsa0JBQWlCLEVBbEZiO0FBbUZKQyxrQkFBaUIsRUFuRmI7QUFvRkpDLHFCQUFpQixFQXBGYjs7QUFzRkpDLG9CQUFpQixFQXRGYjtBQXVGSkMsMEJBQWlCLEVBdkZiO0FBd0ZKQyxzQkFBaUIsRUF4RmI7QUF5RkpDLG9CQUFpQixFQXpGYjtBQTBGSkMsb0JBQWlCLEVBMUZiO0FBMkZKQyx1QkFBaUIsRUEzRmI7O0FBNkZKQyxnQkFBaUIsSUE3RmI7QUE4RkpDLHNCQUFpQixJQTlGYjtBQStGSkMsa0JBQWlCLElBL0ZiO0FBZ0dKQyxnQkFBaUIsSUFoR2I7QUFpR0pDLGdCQUFpQixJQWpHYjtBQWtHSkMsbUJBQWlCLElBbEdiOztBQW9HSkMsb0JBQVksQ0FwR1I7QUFxR0pDLG1CQUFXLENBckdQO0FBc0dKQyxrQkFBVyxDQXRHUDs7QUF3R0pDLGVBQU0sRUF4R0Y7QUF5R0pDLGtCQUFTLEtBekdMO0FBMEdKQyxxQkFBWSxNQTFHUjtBQTJHSkMsc0JBQWMsQ0FBQyxNQUFELEVBQVEsUUFBUjtBQTNHVixLQUhzQjs7QUFrSDlCQyxXQUFPLGlCQUFXO0FBQ2hCLGFBQUtDLFNBQUwsQ0FBZSxZQUFZO0FBQ3pCQyxjQUFFLHlCQUFGLEVBQTZCQyxPQUE3QjtBQUNBRCxjQUFFLHlCQUFGLEVBQTZCQyxPQUE3QixDQUFxQyxVQUFyQztBQUNELFNBSEQ7QUFJRCxLQXZINkI7O0FBeUg1QkMsZ0JBQVk7QUFDWEMscUJBQWFsRyxPQUFPbUcsY0FBUCxDQUFzQkM7QUFEeEIsS0F6SGdCOztBQTZINUJDLGdCQUFZO0FBQ1ZDLG9CQUFZO0FBQ1ZDLGdCQURVLGdCQUNMNUYsRUFESyxFQUNENkYsT0FEQyxFQUNRQyxLQURSLEVBQ2U7QUFDdkJWLGtCQUFFcEYsRUFBRixFQUFNMkYsVUFBTixDQUFpQjtBQUNmSSw0QkFBUTtBQURPLGlCQUFqQixFQUVHQyxFQUZILENBRU0sWUFGTixFQUVvQixVQUFDQyxDQUFELEVBQU87QUFDekJsRyxvQ0FBZ0JtRyxJQUFoQixDQUFxQm5HLGdCQUFnQm9HLElBQXJDLEVBQTJDLFlBQTNDLEVBQXlERixFQUFFRixNQUFGLENBQVMsWUFBVCxDQUF6RDtBQUNELGlCQUpEO0FBS0Q7QUFQUztBQURGLEtBN0hnQjs7QUF5STVCSyxXQXpJNEIscUJBeUlsQjtBQUFBOztBQUNSaEIsVUFBRSxnQkFBRixFQUFvQlksRUFBcEIsQ0FBdUIsOEJBQXZCLEVBQXVELFlBQU07QUFDekQsa0JBQUtuQixRQUFMLEdBQWdCLENBQWhCOztBQUVBd0Isa0JBQU1qQixFQUFFLHFEQUFvRCxNQUFLMUMsYUFBekQsR0FBd0UsSUFBMUUsRUFBZ0ZzRCxFQUFoRixDQUFtRixNQUFuRixFQUEyRixZQUFNO0FBQ3JHLG9CQUFJTSxZQUFZbEIsRUFBRSxlQUFjLE1BQUsxQyxhQUFuQixHQUFrQywwQkFBcEMsQ0FBaEI7QUFDQTRELDBCQUFVQyxRQUFWLENBQW1CLGdCQUFuQjtBQUNBLHNCQUFLNUIsVUFBTCxHQUFrQjJCLFVBQVVFLE1BQVYsRUFBbEI7QUFDQSxzQkFBSzVCLFNBQUwsR0FBaUIwQixVQUFVRyxLQUFWLEVBQWpCOztBQUVBSCwwQkFBVUksU0FBVjtBQUVELGFBUkssQ0FBTjtBQVVILFNBYkQ7O0FBZUF0QixVQUFFLGdCQUFGLEVBQW9CWSxFQUFwQixDQUF1QixlQUF2QixFQUF3QyxZQUFNO0FBQzFDWixjQUFFLGdCQUFGLEVBQW9CdUIsS0FBcEI7QUFDSCxTQUZEO0FBSUQsS0E3SjJCOzs7QUErSjVCQyxXQUFNO0FBQ0ovQixrQkFBUyxrQkFBU2dDLFFBQVQsRUFBa0I7QUFDekIsZ0JBQUlDLG9CQUFxQixLQUFLbkMsVUFBTCxHQUFrQmtDLFFBQTNDO0FBQ0EsZ0JBQUlFLG1CQUFtQixLQUFLbkMsU0FBTCxHQUFpQmlDLFFBQXhDO0FBQ0F6QixjQUFFLGdCQUFGLEVBQW9CNEIsSUFBcEIsQ0FBeUIsS0FBekIsRUFBZ0NDLEdBQWhDLENBQW9DLFFBQXBDLEVBQTZDSCxpQkFBN0M7QUFDQTFCLGNBQUUsZ0JBQUYsRUFBb0I0QixJQUFwQixDQUF5QixLQUF6QixFQUFnQ0MsR0FBaEMsQ0FBb0MsT0FBcEMsRUFBNENGLGdCQUE1QztBQUNELFNBTkc7QUFPSnJFLHVCQUFjLHlCQUFVO0FBQ3BCLGlCQUFLbUMsUUFBTCxHQUFnQixDQUFoQjtBQUNIO0FBVEcsS0EvSnNCOztBQTJLNUJxQyxhQUFRO0FBQ05DLHVCQUFjLHVCQUFTQyxNQUFULEVBQWdCQyxXQUFoQixFQUE0QjtBQUN0QztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxnQkFBR0QsVUFBVSxVQUFiLEVBQ0E7QUFDSSxvQkFBSUUsY0FBY0MsT0FBT0YsV0FBUCxDQUFsQjs7QUFFQSxvQkFBSUcsU0FBU0YsWUFBWUcsSUFBWixDQUFpQkYsUUFBakIsRUFBMkIsTUFBM0IsQ0FBVCxLQUFnRCxFQUFwRCxFQUNBO0FBQ0ksMkJBQU8sS0FBUDtBQUNILGlCQUhELE1BSUE7QUFDSSwyQkFBTyxJQUFQO0FBQ0g7QUFDSjs7QUFFRCxtQkFBTyxLQUFQO0FBQ0gsU0ExQks7O0FBNEJORyxvQkFBVyxvQkFBU04sTUFBVCxFQUNYO0FBQ0Usb0JBQU9BLE1BQVA7QUFFRSxxQkFBSyxTQUFMO0FBQ0UsMkJBQU8sZUFBUDs7QUFFRjtBQUNBLHFCQUFLLFNBQUw7QUFDSWhDLHNCQUFFLHlCQUFGLEVBQTZCQyxPQUE3QixDQUFxQyxVQUFyQztBQUNKLHFCQUFLLFFBQUw7QUFDRSwyQkFBTyxjQUFQOztBQUVGO0FBQ0EscUJBQUssVUFBTDtBQUNFLDJCQUFPLGVBQVA7QUFDRjtBQWRGO0FBZ0JELFNBOUNLO0FBK0NOO0FBQ0FzQyxzQkFBYSxzQkFBVUMsS0FBVixFQUFnQjtBQUMxQixnQkFBSUMsU0FBU0QsTUFBTUUsTUFBTixDQUFhQyxJQUExQjtBQUNBM0MsY0FBRSxNQUFJeUMsTUFBSixHQUFXLENBQWIsRUFBZ0JHLEtBQWhCLENBQXNCLFFBQXRCO0FBQ0YsU0FuREs7QUFvRE5DLHNCQUFhLHNCQUFTQyxLQUFULEVBQWVDLFFBQWYsRUFBd0I7QUFDbkMsaUJBQUsxRixhQUFMLEdBQXFCeUYsTUFBTUUsV0FBTixFQUFyQjs7QUFHQSxpQkFBSzFGLGFBQUwsR0FBcUIsdUJBQXFCdEQsS0FBS2lKLEVBQTFCLEdBQTZCLEdBQTdCLEdBQWlDRixRQUF0RDtBQUNDL0MsY0FBRSxnQkFBRixFQUFvQjRDLEtBQXBCLENBQTBCLFFBQTFCO0FBQ0YsU0ExREs7QUEyRE47QUFDQU0sa0JBQVMsa0JBQVVWLEtBQVYsRUFBZ0I7QUFBQTs7QUFDdEIsaUJBQUtqSCxPQUFMLENBQWE0SCxNQUFiLENBQW9CLE1BQXBCLEVBQTRCLGVBQTVCLEVBQ0lDLElBREosQ0FDUyxrQkFBVTtBQUNkLHVCQUFLN0gsT0FBTCxHQUFlLElBQUlDLElBQUosQ0FBUztBQUNyQkMscUNBQWlCNEgsT0FBTzVILGVBREg7QUFFckJDLGdDQUFZMkgsT0FBTzNILFVBRkU7QUFHckJDLCtCQUFXMEgsT0FBTzFILFNBSEc7QUFJckJDLDRCQUFReUgsT0FBT3pILE1BSk07QUFLckJDLHFDQUFpQndILE9BQU94SDtBQUxILGlCQUFULEVBTWIsRUFBRUMsU0FBUyxpQkFBWCxFQU5hLENBQWY7O0FBUUFrRSxrQkFBRSxXQUFGLEVBQWVzRCxJQUFmLENBQW9CRCxPQUFPMUgsU0FBM0I7O0FBRUE0SCx1QkFBT0MsT0FBUCxDQUFlLE9BQUs5RCxLQUFMLENBQVc3RSxJQUFYLENBQWdCLGtCQUFoQixDQUFmO0FBQ0QsYUFiSixFQWNJNEksS0FkSixDQWNVLGlCQUFTO0FBQ2RGLHVCQUFPRyxLQUFQLENBQWEsT0FBS2hFLEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsZUFBaEIsQ0FBYjtBQUNELGFBaEJKO0FBaUJGLFNBOUVLOztBQWdGTjhJLDBCQUFpQiwwQkFBU25CLEtBQVQsRUFBZTtBQUFBOztBQUM5QixpQkFBSy9GLGNBQUwsQ0FBb0IwRyxNQUFwQixDQUEyQixNQUEzQixFQUFtQyxrQkFBbkMsRUFDQ0MsSUFERCxDQUNNLGtCQUFVO0FBQ1pHLHVCQUFPQyxPQUFQLENBQWUsT0FBSzlELEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsZ0JBQWhCLENBQWY7QUFDSCxhQUhELEVBSUM0SSxLQUpELENBSU8saUJBQVM7QUFDWkYsdUJBQU9HLEtBQVAsQ0FBYSxPQUFLaEUsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixlQUFoQixDQUFiO0FBQ0gsYUFORDtBQU9ELFNBeEZLOztBQTBGTitJLDhCQUFxQiw4QkFBU3BCLEtBQVQsRUFBZTtBQUFBOztBQUNsQyxpQkFBSzNGLGdCQUFMLENBQXNCc0csTUFBdEIsQ0FBNkIsTUFBN0IsRUFBcUMsNEJBQXJDLEVBQ0NDLElBREQsQ0FDTSxrQkFBVTtBQUNaRyx1QkFBT0MsT0FBUCxDQUFlLE9BQUs5RCxLQUFMLENBQVc3RSxJQUFYLENBQWdCLGFBQWhCLENBQWY7QUFDQSx1QkFBS2dKLGtCQUFMO0FBQ0EsdUJBQUtDLGdCQUFMO0FBQ0gsYUFMRCxFQU1DTCxLQU5ELENBTU8saUJBQVM7QUFDWkYsdUJBQU9HLEtBQVAsQ0FBYSxPQUFLaEUsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixlQUFoQixDQUFiO0FBQ0gsYUFSRDtBQVNELFNBcEdLOztBQXNHTmtKLHdCQUFlLHdCQUFTdkIsS0FBVCxFQUFlO0FBQzVCLGlCQUFLekcsV0FBTCxDQUFpQm9ILE1BQWpCLENBQXdCLE1BQXhCLEVBQWdDLGVBQWhDO0FBQ0QsU0F4R0s7O0FBMEdOVSwwQkExR00sZ0NBMEdlO0FBQUE7O0FBQ25CRyxrQkFBTUMsR0FBTixDQUFVLDJCQUFWLEVBQ0diLElBREgsQ0FDUSxrQkFBVTtBQUNkLHVCQUFLakksZUFBTCxHQUF1QmtJLE9BQU94SSxJQUFQLENBQVlxSixTQUFuQztBQUNBLG9CQUFJLE9BQUsvSSxlQUFMLENBQXFCZ0osTUFBekIsRUFBaUM7QUFDL0IsMkJBQUtySixZQUFMLEdBQW9CLEtBQXBCO0FBRUQ7QUFDRCx1QkFBS2lGLFNBQUwsQ0FBZSxZQUFVO0FBQ3ZCLHlCQUFLK0QsZ0JBQUw7QUFDRCxpQkFGRDtBQUdILGFBVkQ7QUFXRCxTQXRISzs7O0FBd0hOTSxpQkFBUSxpQkFBU3ZKLElBQVQsRUFBYztBQUFBOztBQUNwQixpQkFBS3VDLGVBQUwsQ0FBcUI5QixJQUFyQixHQUE0QixFQUE1QjtBQUNBMEksa0JBQU1DLEdBQU4sQ0FBVSxtQkFBaUJwSixJQUEzQixFQUNHdUksSUFESCxDQUNRLGtCQUFVO0FBQ2hCLG9CQUFJaUIsUUFBUSxFQUFaO0FBQ0EscUJBQUksSUFBSUMsSUFBSSxDQUFaLEVBQWVBLElBQUlqQixPQUFPeEksSUFBUCxDQUFZc0osTUFBL0IsRUFBdUNHLEdBQXZDLEVBQTJDOztBQUV6Q0QsMEJBQU1FLElBQU4sQ0FBV2xCLE9BQU94SSxJQUFQLENBQVl5SixDQUFaLENBQVg7QUFDRDtBQUNELHVCQUFLakosVUFBTCxHQUFrQmdKLEtBQWxCO0FBQ0QsYUFSRDtBQVNELFNBbklLOztBQXFJTkcscUJBQVkscUJBQVMzSixJQUFULEVBQWM7QUFDeEIsaUJBQUt1QyxlQUFMLENBQXFCOUIsSUFBckIsR0FBNEJULElBQTVCO0FBQ0EsaUJBQUt1QyxlQUFMLENBQXFCaEIsU0FBckIsR0FBaUMsS0FBS3FJLFdBQUwsQ0FBaUI1SixLQUFLdUIsU0FBdEIsQ0FBakM7QUFFRCxTQXpJSzs7QUEySU5zSSx5QkFBZ0IseUJBQVNsQyxLQUFULEVBQWU7QUFBQTs7QUFDN0IsaUJBQUtwRixlQUFMLENBQXFCOUIsSUFBckIsR0FBNEIsS0FBS0EsSUFBTCxDQUFVcUgsSUFBdEM7QUFDQSxpQkFBS3ZGLGVBQUwsQ0FBcUIrRixNQUFyQixDQUE0QixNQUE1QixFQUFvQyxvQkFBcEMsRUFBMEQsS0FBSy9GLGVBQS9ELEVBQ0NnRyxJQURELENBQ00sa0JBQVU7QUFDWkcsdUJBQU9DLE9BQVAsQ0FBZSxPQUFLOUQsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixhQUFoQixDQUFmO0FBQ0gsYUFIRCxFQUlDNEksS0FKRCxDQUlPLGlCQUFTO0FBQ1pGLHVCQUFPRyxLQUFQLENBQWEsT0FBS2hFLEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsZUFBaEIsQ0FBYjtBQUNILGFBTkQ7QUFPRCxTQXBKSzs7QUFzSk44SixvQkF0Sk0sd0JBc0pPQyxjQXRKUCxFQXNKdUIzQixFQXRKdkIsRUFzSjBCO0FBQzlCLGdCQUFHQSxNQUFNLFVBQVQsRUFBb0I7QUFDbEIxSSx1QkFBTyxDQUFQLElBQVlxSyxjQUFaO0FBQ0QsYUFGRCxNQUVNLElBQUczQixNQUFNLFdBQVQsRUFBcUI7QUFDekIxSSx1QkFBTyxDQUFQLElBQVlxSyxjQUFaO0FBQ0QsYUFGSyxNQUVBLElBQUczQixNQUFNLFdBQVQsRUFBcUI7QUFDekIxSSx1QkFBTyxDQUFQLElBQVlxSyxjQUFaO0FBQ0Q7QUFDRCxnQkFBSUMsVUFBVSxLQUFLM0osaUJBQW5CO0FBQ0EySixzQkFBVUEsUUFBUUMsR0FBUixDQUFZLFVBQVVoSSxRQUFWLEVBQW1CO0FBQ3ZDQSx5QkFBU2lJLFdBQVQsR0FBdUIsS0FBdkI7QUFDQSx1QkFBT2pJLFFBQVA7QUFDRCxhQUhTLENBQVY7O0FBS0FrRCxjQUFFekYsTUFBRixFQUFVeUssSUFBVixDQUFlLFVBQVNWLENBQVQsRUFBWTtBQUN6Qk8sMEJBQVVBLFFBQVFDLEdBQVIsQ0FBWSxVQUFVaEksUUFBVixFQUFtQjtBQUNyQyx3QkFBSXZDLE9BQU8rSixDQUFQLEVBQVVyQixFQUFWLElBQWdCbkcsU0FBU21HLEVBQTdCLEVBQWlDO0FBQy9CbkcsaUNBQVNpSSxXQUFULEdBQXVCLElBQXZCO0FBQ0Q7QUFDRCwyQkFBT2pJLFFBQVA7QUFDRCxpQkFMTyxDQUFWO0FBTUQsYUFQRDtBQVFELFNBNUtLO0FBOEtObUksb0JBOUtNLDBCQThLUTtBQUFBOztBQUNaQyx1QkFBV2pCLEdBQVgsQ0FBZSxZQUFVL0osUUFBUUYsSUFBUixDQUFhaUosRUFBdkIsR0FBMEIsWUFBekMsRUFDR0csSUFESCxDQUNRLG1CQUFXO0FBQ2Ysb0JBQUcrQixRQUFRLFFBQVIsS0FBcUIsR0FBeEIsRUFDQTtBQUNFQyw4QkFBZ0JELFFBQVF0SyxJQUFSLENBQWF3SyxNQUFiLENBQW9CLGVBQU87QUFBRSwrQkFBT0MsSUFBSUMsSUFBSixLQUFhLEtBQXBCO0FBQTJCLHFCQUF4RCxDQUFoQjtBQUNBQyxvQ0FBZ0JMLFFBQVF0SyxJQUFSLENBQWF3SyxNQUFiLENBQW9CLGVBQU87QUFBRSwrQkFBT0MsSUFBSUMsSUFBSixLQUFhLFdBQXBCO0FBQWlDLHFCQUE5RCxDQUFoQjtBQUNBRSxnQ0FBZ0JOLFFBQVF0SyxJQUFSLENBQWF3SyxNQUFiLENBQW9CLGVBQU87QUFBRSwrQkFBT0MsSUFBSUMsSUFBSixLQUFhLE9BQXBCO0FBQTZCLHFCQUExRCxDQUFoQjtBQUNBRyw4QkFBZ0JQLFFBQVF0SyxJQUFSLENBQWF3SyxNQUFiLENBQW9CLGVBQU87QUFBRSwrQkFBT0MsSUFBSUMsSUFBSixLQUFhLEtBQXBCO0FBQTJCLHFCQUF4RCxDQUFoQjtBQUNBSSw4QkFBZ0JSLFFBQVF0SyxJQUFSLENBQWF3SyxNQUFiLENBQW9CLGVBQU87QUFBRSwrQkFBT0MsSUFBSUMsSUFBSixLQUFhLEtBQXBCO0FBQTJCLHFCQUF4RCxDQUFoQjtBQUNBSyxpQ0FBZ0JULFFBQVF0SyxJQUFSLENBQWF3SyxNQUFiLENBQW9CLGVBQU87QUFBRSwrQkFBT0MsSUFBSUMsSUFBSixLQUFhLFFBQXBCO0FBQThCLHFCQUEzRCxDQUFoQjs7QUFFQSx3QkFBR0gsUUFBUWpCLE1BQVgsRUFDQTtBQUNJLCtCQUFLMUcsVUFBTCxHQUFtQjJILFFBQVEsQ0FBUixFQUFXcEQsTUFBOUI7QUFDQSwrQkFBS2pFLFVBQUwsR0FBbUJxSCxRQUFRLENBQVIsRUFBV1MsVUFBOUI7QUFDQSwrQkFBS2xILFVBQUwsR0FBbUJ5RyxRQUFRLENBQVIsRUFBV1UsTUFBOUI7QUFDQSwrQkFBS3pILFFBQUwsR0FBaUIrRyxRQUFRLENBQVIsRUFBV3pDLElBQTVCO0FBQ0EsNEJBQUcsT0FBS2xGLFVBQUwsSUFBbUIsU0FBdEIsRUFBZ0M7QUFDOUIsbUNBQUt3QixNQUFMLEdBQWMsS0FBZDtBQUNELHlCQUZELE1BR0k7QUFDRixtQ0FBS0EsTUFBTCxHQUFjLElBQWQ7QUFDRDtBQUlKOztBQUVELHdCQUFHdUcsY0FBY3JCLE1BQWpCLEVBQ0E7QUFDSSwrQkFBS3pHLGdCQUFMLEdBQXlCOEgsY0FBYyxDQUFkLEVBQWlCeEQsTUFBMUM7QUFDQSwrQkFBS2hFLGdCQUFMLEdBQXlCd0gsY0FBYyxDQUFkLEVBQWlCSyxVQUExQztBQUNBLCtCQUFLakgsZ0JBQUwsR0FBeUI0RyxjQUFjLENBQWQsRUFBaUJNLE1BQTFDO0FBQ0EsK0JBQUt4SCxjQUFMLEdBQXVCa0gsY0FBYyxDQUFkLEVBQWlCN0MsSUFBeEM7QUFDQSwrQkFBS3pELFlBQUwsR0FBb0IsSUFBcEI7QUFHSDs7QUFFRCx3QkFBR3VHLFVBQVV0QixNQUFiLEVBQ0E7QUFDSSwrQkFBS3hHLFlBQUwsR0FBcUI4SCxVQUFVLENBQVYsRUFBYXpELE1BQWxDO0FBQ0EsK0JBQUsvRCxZQUFMLEdBQXFCd0gsVUFBVSxDQUFWLEVBQWFJLFVBQWxDO0FBQ0EsK0JBQUtoSCxZQUFMLEdBQXFCNEcsVUFBVSxDQUFWLEVBQWFLLE1BQWxDO0FBQ0EsK0JBQUt2SCxVQUFMLEdBQW1Ca0gsVUFBVSxDQUFWLEVBQWE5QyxJQUFoQztBQUNBLCtCQUFLeEQsUUFBTCxHQUFnQixJQUFoQjtBQUlIOztBQUVELHdCQUFHdUcsUUFBUXZCLE1BQVgsRUFDQTtBQUNJLCtCQUFLdkcsVUFBTCxHQUFtQjhILFFBQVEsQ0FBUixFQUFXMUQsTUFBOUI7QUFDQSwrQkFBSzlELFVBQUwsR0FBbUJ3SCxRQUFRLENBQVIsRUFBV0csVUFBOUI7QUFDQSwrQkFBSy9HLFVBQUwsR0FBbUI0RyxRQUFRLENBQVIsRUFBV0ksTUFBOUI7QUFDQSwrQkFBS3RILFFBQUwsR0FBaUJrSCxRQUFRLENBQVIsRUFBVy9DLElBQTVCO0FBQ0EsK0JBQUt2RCxNQUFMLEdBQWMsSUFBZDtBQUdIOztBQUVELHdCQUFHdUcsUUFBUXhCLE1BQVgsRUFDQTtBQUNJLCtCQUFLdEcsVUFBTCxHQUFtQjhILFFBQVEsQ0FBUixFQUFXM0QsTUFBOUI7QUFDQSwrQkFBSzdELFVBQUwsR0FBbUJ3SCxRQUFRLENBQVIsRUFBV0UsVUFBOUI7QUFDQSwrQkFBSzlHLFVBQUwsR0FBbUI0RyxRQUFRLENBQVIsRUFBV0csTUFBOUI7QUFDQSwrQkFBS3JILFFBQUwsR0FBaUJrSCxRQUFRLENBQVIsRUFBV2hELElBQTVCO0FBQ0EsK0JBQUt0RCxNQUFMLEdBQWMsSUFBZDtBQUdIOztBQUVELHdCQUFHdUcsV0FBV3pCLE1BQWQsRUFDQTtBQUNJLCtCQUFLckcsYUFBTCxHQUFzQjhILFdBQVcsQ0FBWCxFQUFjNUQsTUFBcEM7QUFDQSwrQkFBSzVELGFBQUwsR0FBc0J3SCxXQUFXLENBQVgsRUFBY0MsVUFBcEM7QUFDQSwrQkFBSzdHLGFBQUwsR0FBc0I0RyxXQUFXLENBQVgsRUFBY0UsTUFBcEM7QUFDQSwrQkFBS3BILFdBQUwsR0FBb0JrSCxXQUFXLENBQVgsRUFBY2pELElBQWxDO0FBQ0EsK0JBQUtyRCxTQUFMLEdBQWlCLElBQWpCO0FBR0g7O0FBRUQsMkJBQUt3RSxnQkFBTDtBQUNBLDJCQUFLL0QsU0FBTCxDQUFlLFlBQVU7QUFDdkJDLDBCQUFFLHlCQUFGLEVBQTZCQyxPQUE3QixDQUFxQyxVQUFyQztBQUNELHFCQUZEO0FBR0Q7QUFFRixhQTFGSCxFQTJGR3dELEtBM0ZILENBMkZTLGlCQUFTO0FBQ2Q7QUFDSCxhQTdGRDtBQThGRCxTQTdRSztBQStRTkssd0JBL1FNLDhCQStRWTs7QUFFZCxnQkFBSWlDLG1CQUFtQixJQUF2Qjs7QUFFQUEsK0JBQ0MsQ0FBQyxLQUFLdEksVUFBTCxJQUFpQixVQUFsQixLQUErQixLQUFLRSxZQUFMLElBQW1CLFVBQWxELENBQUQsSUFFQyxDQUFDLEtBQUtDLFVBQUwsSUFBaUIsVUFBbEIsS0FBK0IsS0FBS0MsVUFBTCxJQUFpQixVQUFoRCxLQUE2RCxLQUFLQyxhQUFMLElBQW9CLFVBQWpGLENBSEQ7O0FBTUEsZ0JBQUcsS0FBSzNDLGVBQUwsQ0FBcUJnSixNQUFyQixJQUErQjRCLGdCQUFsQyxFQUNBO0FBQ0kscUJBQUt2SSxhQUFMLEdBQXFCLDJCQUFyQjtBQUNILGFBSEQsTUFHTSxJQUFHLEtBQUtyQyxlQUFMLENBQXFCZ0osTUFBckIsSUFBK0I0QixnQkFBbEMsRUFDTjtBQUNJLHFCQUFLdkksYUFBTCxHQUFxQiwwQkFBckI7QUFDSCxhQUhLLE1BSU47QUFDSSxxQkFBS0EsYUFBTCxHQUFxQix5QkFBckI7QUFDSDtBQUVKLFNBcFNLO0FBc1NOaUgsbUJBdFNNLHVCQXNTTXVCLENBdFNOLEVBc1NRO0FBQ1osZ0JBQUdBLENBQUgsRUFDQTtBQUNFLHVCQUFPLENBQUMsVUFBVUEsQ0FBWCxFQUFjQyxLQUFkLENBQW9CLENBQUMsQ0FBckIsQ0FBUDtBQUNELGFBSEQsTUFJQTtBQUNFLHVCQUFPLEVBQVA7QUFDRDtBQUNGO0FBOVNLLEtBM0tvQjs7QUE2ZDVCQyxXQTdkNEIscUJBNmRsQjtBQUFBOztBQUVSbEMsY0FBTUMsR0FBTixDQUFVLDZCQUFWLEVBQ1NiLElBRFQsQ0FDYyxrQkFBVTtBQUNoQixtQkFBSzFELEtBQUwsR0FBYTJELE9BQU94SSxJQUFwQjtBQUNQLFNBSEQ7QUFJQTtBQUNBc0wsY0FBTUMsTUFBTixDQUFhLHlCQUFiLEVBQXVDLFVBQUN2TCxJQUFELEVBQVE7QUFDM0MsZ0JBQUdBLElBQUgsRUFBUTtBQUNKcUssMkJBQVdqQixHQUFYLENBQWUsWUFBVS9KLFFBQVFGLElBQVIsQ0FBYWlKLEVBQXZCLEdBQTBCLHdCQUF6QyxFQUNLRyxJQURMLENBQ1UsbUJBQVc7QUFDYix3QkFBRytCLFFBQVF0SyxJQUFSLENBQWFzSixNQUFoQixFQUF1QjtBQUNuQiwrQkFBS2xGLE1BQUwsR0FBYyxJQUFkO0FBQ0EsK0JBQUt4QixVQUFMLEdBQWtCMEgsUUFBUXRLLElBQVIsQ0FBYSxDQUFiLEVBQWdCbUgsTUFBbEM7QUFDQSwrQkFBSzNELFFBQUwsR0FBZ0I4RyxRQUFRdEssSUFBUixDQUFhLENBQWIsRUFBZ0I4SCxJQUFoQztBQUNBWSwrQkFBT0MsT0FBUCxDQUFlLE9BQUs5RCxLQUFMLENBQVc3RSxJQUFYLENBQWdCLFFBQWhCLENBQWYsRUFBeUMsT0FBSzZFLEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsYUFBaEIsQ0FBekM7QUFFSCxxQkFORCxNQU9BO0FBQ0kwSSwrQkFBT0csS0FBUCxDQUFhLE9BQUtoRSxLQUFMLENBQVc3RSxJQUFYLENBQWdCLFFBQWhCLENBQWIsRUFBdUMsT0FBSzZFLEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsV0FBaEIsQ0FBdkM7QUFDSDtBQUNKLGlCQVpMO0FBYUZ3TCwwQkFBVXJHLEVBQUUsZUFBRixFQUFtQjRCLElBQW5CLENBQXdCLGNBQXhCLENBQVY7QUFDQTBFLHlCQUFTdEcsRUFBRSxlQUFGLEVBQW1CNEIsSUFBbkIsQ0FBd0IsU0FBeEIsQ0FBVDtBQUNBMEUsdUJBQU9DLE9BQVAsQ0FBZSxHQUFmLEVBQW1CLFlBQVU7QUFDM0JGLDRCQUFRRyxNQUFSLENBQWUsR0FBZjtBQUNELGlCQUZEO0FBR0QsYUFuQkQsTUFvQkE7QUFDSUgsMEJBQVVyRyxFQUFFLGVBQUYsRUFBbUI0QixJQUFuQixDQUF3QixjQUF4QixDQUFWO0FBQ0EwRSx5QkFBU3RHLEVBQUUsZUFBRixFQUFtQjRCLElBQW5CLENBQXdCLFNBQXhCLENBQVQ7QUFDQTBFLHVCQUFPQyxPQUFQLENBQWUsR0FBZixFQUFtQixZQUFVO0FBQzNCRiw0QkFBUUcsTUFBUixDQUFlLEdBQWY7QUFDRCxpQkFGRDtBQUdIO0FBRUosU0E3QkQ7QUE4QkFMLGNBQU1DLE1BQU4sQ0FBYSwrQkFBYixFQUE2QyxVQUFDdkwsSUFBRCxFQUFRO0FBQ2pELGdCQUFHQSxJQUFILEVBQVE7QUFDSnFLLDJCQUFXakIsR0FBWCxDQUFlLFlBQVUvSixRQUFRRixJQUFSLENBQWFpSixFQUF2QixHQUEwQiw4QkFBekMsRUFDS0csSUFETCxDQUNVLG1CQUFXO0FBQ2Isd0JBQUcrQixRQUFRdEssSUFBUixDQUFhc0osTUFBaEIsRUFBdUI7QUFDbkIsK0JBQUtqRixZQUFMLEdBQW9CLElBQXBCO0FBQ0EsK0JBQUt4QixnQkFBTCxHQUF3QnlILFFBQVF0SyxJQUFSLENBQWEsQ0FBYixFQUFnQm1ILE1BQXhDO0FBQ0EsK0JBQUsxRCxjQUFMLEdBQXNCNkcsUUFBUXRLLElBQVIsQ0FBYSxDQUFiLEVBQWdCOEgsSUFBdEM7QUFDQVksK0JBQU9DLE9BQVAsQ0FBZSxPQUFLOUQsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixRQUFoQixDQUFmLEVBQXlDLE9BQUs2RSxLQUFMLENBQVc3RSxJQUFYLENBQWdCLGFBQWhCLENBQXpDO0FBRUgscUJBTkQsTUFPQTtBQUNJMEksK0JBQU9HLEtBQVAsQ0FBYSxPQUFLaEUsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixRQUFoQixDQUFiLEVBQXVDLE9BQUs2RSxLQUFMLENBQVc3RSxJQUFYLENBQWdCLFdBQWhCLENBQXZDO0FBQ0g7QUFDSixpQkFaTDtBQWFGd0wsMEJBQVVyRyxFQUFFLHFCQUFGLEVBQXlCNEIsSUFBekIsQ0FBOEIsY0FBOUIsQ0FBVjtBQUNBMEUseUJBQVN0RyxFQUFFLHFCQUFGLEVBQXlCNEIsSUFBekIsQ0FBOEIsU0FBOUIsQ0FBVDtBQUNBMEUsdUJBQU9DLE9BQVAsQ0FBZSxHQUFmLEVBQW1CLFlBQVU7QUFDM0JGLDRCQUFRRyxNQUFSLENBQWUsR0FBZjtBQUNELGlCQUZEO0FBR0QsYUFuQkQsTUFvQkE7QUFDRUgsMEJBQVVyRyxFQUFFLHFCQUFGLEVBQXlCNEIsSUFBekIsQ0FBOEIsY0FBOUIsQ0FBVjtBQUNBMEUseUJBQVN0RyxFQUFFLHFCQUFGLEVBQXlCNEIsSUFBekIsQ0FBOEIsU0FBOUIsQ0FBVDtBQUNBMEUsdUJBQU9DLE9BQVAsQ0FBZSxHQUFmLEVBQW1CLFlBQVU7QUFDM0JGLDRCQUFRRyxNQUFSLENBQWUsR0FBZjtBQUNELGlCQUZEO0FBR0Q7QUFDSixTQTVCRDtBQTZCQUwsY0FBTUMsTUFBTixDQUFhLDJCQUFiLEVBQXlDLFVBQUN2TCxJQUFELEVBQVE7QUFDN0MsZ0JBQUdBLElBQUgsRUFBUTtBQUNKcUssMkJBQVdqQixHQUFYLENBQWUsWUFBVS9KLFFBQVFGLElBQVIsQ0FBYWlKLEVBQXZCLEdBQTBCLDBCQUF6QyxFQUNLRyxJQURMLENBQ1UsbUJBQVc7QUFDZnFELDRCQUFRQyxHQUFSLENBQVl2QixPQUFaO0FBQ0Usd0JBQUdBLFFBQVF0SyxJQUFSLENBQWFzSixNQUFoQixFQUF1QjtBQUNuQiwrQkFBS2hGLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSwrQkFBS3hCLFlBQUwsR0FBb0J3SCxRQUFRdEssSUFBUixDQUFhLENBQWIsRUFBZ0JtSCxNQUFwQztBQUNBLCtCQUFLekQsVUFBTCxHQUFrQjRHLFFBQVF0SyxJQUFSLENBQWEsQ0FBYixFQUFnQjhILElBQWxDO0FBQ0FZLCtCQUFPQyxPQUFQLENBQWUsT0FBSzlELEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsUUFBaEIsQ0FBZixFQUF5QyxPQUFLNkUsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixhQUFoQixDQUF6QztBQUVILHFCQU5ELE1BT0E7QUFDSTBJLCtCQUFPRyxLQUFQLENBQWEsT0FBS2hFLEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsUUFBaEIsQ0FBYixFQUF1QyxPQUFLNkUsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixXQUFoQixDQUF2QztBQUNIO0FBQ0osaUJBYkw7QUFjRndMLDBCQUFVckcsRUFBRSxpQkFBRixFQUFxQjRCLElBQXJCLENBQTBCLGNBQTFCLENBQVY7QUFDQTBFLHlCQUFTdEcsRUFBRSxpQkFBRixFQUFxQjRCLElBQXJCLENBQTBCLFNBQTFCLENBQVQ7QUFDQTBFLHVCQUFPQyxPQUFQLENBQWUsR0FBZixFQUFtQixZQUFVO0FBQzNCRiw0QkFBUUcsTUFBUixDQUFlLEdBQWY7QUFDRCxpQkFGRDtBQUdELGFBcEJELE1BcUJBO0FBQ0VILDBCQUFVckcsRUFBRSxpQkFBRixFQUFxQjRCLElBQXJCLENBQTBCLGNBQTFCLENBQVY7QUFDQTBFLHlCQUFTdEcsRUFBRSxpQkFBRixFQUFxQjRCLElBQXJCLENBQTBCLFNBQTFCLENBQVQ7QUFDQTBFLHVCQUFPQyxPQUFQLENBQWUsR0FBZixFQUFtQixZQUFVO0FBQzNCRiw0QkFBUUcsTUFBUixDQUFlLEdBQWY7QUFDRCxpQkFGRDtBQUdEO0FBQ0o7QUFDRDtBQTlCQSxVQStCQUwsTUFBTUMsTUFBTixDQUFhLHlCQUFiLEVBQXVDLFVBQUN2TCxJQUFELEVBQVE7QUFDM0MsZ0JBQUdBLElBQUgsRUFBUTtBQUNKcUssMkJBQVdqQixHQUFYLENBQWUsWUFBVS9KLFFBQVFGLElBQVIsQ0FBYWlKLEVBQXZCLEdBQTBCLHdCQUF6QyxFQUNLRyxJQURMLENBQ1UsbUJBQVc7QUFDYix3QkFBRytCLFFBQVF0SyxJQUFSLENBQWFzSixNQUFoQixFQUF1QjtBQUNuQiwrQkFBSy9FLE1BQUwsR0FBYyxJQUFkO0FBQ0EsK0JBQUt4QixVQUFMLEdBQWtCdUgsUUFBUXRLLElBQVIsQ0FBYSxDQUFiLEVBQWdCbUgsTUFBbEM7QUFDQSwrQkFBS3hELFFBQUwsR0FBZ0IyRyxRQUFRdEssSUFBUixDQUFhLENBQWIsRUFBZ0I4SCxJQUFoQztBQUNBWSwrQkFBT0MsT0FBUCxDQUFlLE9BQUs5RCxLQUFMLENBQVc3RSxJQUFYLENBQWdCLFFBQWhCLENBQWYsRUFBeUMsT0FBSzZFLEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsYUFBaEIsQ0FBekM7QUFFSCxxQkFORCxNQU9BO0FBQ0kwSSwrQkFBT0csS0FBUCxDQUFhLE9BQUtoRSxLQUFMLENBQVc3RSxJQUFYLENBQWdCLFFBQWhCLENBQWIsRUFBdUMsT0FBSzZFLEtBQUwsQ0FBVzdFLElBQVgsQ0FBZ0IsV0FBaEIsQ0FBdkM7QUFDSDtBQUNKLGlCQVpMO0FBYUZ3TCwwQkFBVXJHLEVBQUUsZUFBRixFQUFtQjRCLElBQW5CLENBQXdCLGNBQXhCLENBQVY7QUFDQTBFLHlCQUFTdEcsRUFBRSxlQUFGLEVBQW1CNEIsSUFBbkIsQ0FBd0IsU0FBeEIsQ0FBVDtBQUNBMEUsdUJBQU9DLE9BQVAsQ0FBZSxHQUFmLEVBQW1CLFlBQVU7QUFDM0JGLDRCQUFRRyxNQUFSLENBQWUsR0FBZjtBQUNELGlCQUZEO0FBR0QsYUFuQkQsTUFvQkE7QUFDRUgsMEJBQVVyRyxFQUFFLGVBQUYsRUFBbUI0QixJQUFuQixDQUF3QixjQUF4QixDQUFWO0FBQ0EwRSx5QkFBU3RHLEVBQUUsZUFBRixFQUFtQjRCLElBQW5CLENBQXdCLFNBQXhCLENBQVQ7QUFDQTBFLHVCQUFPQyxPQUFQLENBQWUsR0FBZixFQUFtQixZQUFVO0FBQzNCRiw0QkFBUUcsTUFBUixDQUFlLEdBQWY7QUFDRCxpQkFGRDtBQUdEO0FBQ0osU0E1QkQ7QUE2QkFMLGNBQU1DLE1BQU4sQ0FBYSx5QkFBYixFQUF1QyxVQUFDdkwsSUFBRCxFQUFRO0FBQzNDLGdCQUFHQSxJQUFILEVBQVE7QUFDSnFLLDJCQUFXakIsR0FBWCxDQUFlLFlBQVUvSixRQUFRRixJQUFSLENBQWFpSixFQUF2QixHQUEwQix3QkFBekMsRUFDS0csSUFETCxDQUNVLG1CQUFXO0FBQ2Isd0JBQUcrQixRQUFRdEssSUFBUixDQUFhc0osTUFBaEIsRUFBdUI7QUFDbkIsK0JBQUs5RSxNQUFMLEdBQWMsSUFBZDtBQUNBLCtCQUFLeEIsVUFBTCxHQUFrQnNILFFBQVF0SyxJQUFSLENBQWEsQ0FBYixFQUFnQm1ILE1BQWxDO0FBQ0EsK0JBQUt2RCxRQUFMLEdBQWdCMEcsUUFBUXRLLElBQVIsQ0FBYSxDQUFiLEVBQWdCOEgsSUFBaEM7QUFDQVksK0JBQU9DLE9BQVAsQ0FBZSxPQUFLOUQsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixRQUFoQixDQUFmLEVBQXlDLE9BQUs2RSxLQUFMLENBQVc3RSxJQUFYLENBQWdCLGFBQWhCLENBQXpDO0FBRUgscUJBTkQsTUFPQTtBQUNJMEksK0JBQU9HLEtBQVAsQ0FBYSxPQUFLaEUsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixRQUFoQixDQUFiLEVBQXVDLE9BQUs2RSxLQUFMLENBQVc3RSxJQUFYLENBQWdCLFdBQWhCLENBQXZDO0FBQ0g7QUFDSixpQkFaTDtBQWFGd0wsMEJBQVVyRyxFQUFFLGVBQUYsRUFBbUI0QixJQUFuQixDQUF3QixjQUF4QixDQUFWO0FBQ0EwRSx5QkFBU3RHLEVBQUUsZUFBRixFQUFtQjRCLElBQW5CLENBQXdCLFNBQXhCLENBQVQ7QUFDQTBFLHVCQUFPQyxPQUFQLENBQWUsR0FBZixFQUFtQixZQUFVO0FBQzNCRiw0QkFBUUcsTUFBUixDQUFlLEdBQWY7QUFDRCxpQkFGRDtBQUdELGFBbkJELE1Bb0JBO0FBQ0VILDBCQUFVckcsRUFBRSxlQUFGLEVBQW1CNEIsSUFBbkIsQ0FBd0IsY0FBeEIsQ0FBVjtBQUNBMEUseUJBQVN0RyxFQUFFLGVBQUYsRUFBbUI0QixJQUFuQixDQUF3QixTQUF4QixDQUFUO0FBQ0EwRSx1QkFBT0MsT0FBUCxDQUFlLEdBQWYsRUFBbUIsWUFBVTtBQUMzQkYsNEJBQVFHLE1BQVIsQ0FBZSxHQUFmO0FBQ0QsaUJBRkQ7QUFHRDtBQUNKLFNBNUJEO0FBNkJBTCxjQUFNQyxNQUFOLENBQWEsNEJBQWIsRUFBMEMsVUFBQ3ZMLElBQUQsRUFBUTtBQUM5QyxnQkFBR0EsSUFBSCxFQUFRO0FBQ0pxSywyQkFBV2pCLEdBQVgsQ0FBZSxZQUFVL0osUUFBUUYsSUFBUixDQUFhaUosRUFBdkIsR0FBMEIsMkJBQXpDLEVBQ0tHLElBREwsQ0FDVSxtQkFBVztBQUNiLHdCQUFHK0IsUUFBUXRLLElBQVIsQ0FBYXNKLE1BQWhCLEVBQXVCO0FBQ25CLCtCQUFLN0UsU0FBTCxHQUFpQixJQUFqQjtBQUNBLCtCQUFLeEIsYUFBTCxHQUFxQnFILFFBQVF0SyxJQUFSLENBQWEsQ0FBYixFQUFnQm1ILE1BQXJDO0FBQ0EsK0JBQUt0RCxXQUFMLEdBQW1CeUcsUUFBUXRLLElBQVIsQ0FBYSxDQUFiLEVBQWdCOEgsSUFBbkM7QUFDQVksK0JBQU9DLE9BQVAsQ0FBZSxPQUFLOUQsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixRQUFoQixDQUFmLEVBQXlDLE9BQUs2RSxLQUFMLENBQVc3RSxJQUFYLENBQWdCLGFBQWhCLENBQXpDO0FBRUgscUJBTkQsTUFPQTtBQUNJMEksK0JBQU9HLEtBQVAsQ0FBYSxPQUFLaEUsS0FBTCxDQUFXN0UsSUFBWCxDQUFnQixRQUFoQixDQUFiLEVBQXVDLE9BQUs2RSxLQUFMLENBQVc3RSxJQUFYLENBQWdCLFdBQWhCLENBQXZDO0FBQ0g7QUFDSixpQkFaTDtBQWFGd0wsMEJBQVVyRyxFQUFFLGtCQUFGLEVBQXNCNEIsSUFBdEIsQ0FBMkIsY0FBM0IsQ0FBVjtBQUNBMEUseUJBQVN0RyxFQUFFLGtCQUFGLEVBQXNCNEIsSUFBdEIsQ0FBMkIsU0FBM0IsQ0FBVDtBQUNBMEUsdUJBQU9DLE9BQVAsQ0FBZSxHQUFmLEVBQW1CLFlBQVU7QUFDM0JGLDRCQUFRRyxNQUFSLENBQWUsR0FBZjtBQUNELGlCQUZEO0FBR0QsYUFuQkQsTUFvQkE7QUFDRUgsMEJBQVVyRyxFQUFFLGtCQUFGLEVBQXNCNEIsSUFBdEIsQ0FBMkIsY0FBM0IsQ0FBVjtBQUNBMEUseUJBQVN0RyxFQUFFLGtCQUFGLEVBQXNCNEIsSUFBdEIsQ0FBMkIsU0FBM0IsQ0FBVDtBQUNBMEUsdUJBQU9DLE9BQVAsQ0FBZSxHQUFmLEVBQW1CLFlBQVU7QUFDM0JGLDRCQUFRRyxNQUFSLENBQWUsR0FBZjtBQUNELGlCQUZEO0FBR0Q7QUFDSixTQTVCRDs7QUErQkF4QyxjQUFNQyxHQUFOLENBQVUsd0JBQVYsRUFDR2IsSUFESCxDQUNRLGtCQUFVO0FBQ2QsbUJBQUs3RixhQUFMLEdBQXFCOEYsT0FBT3hJLElBQTVCO0FBQ0gsU0FIRDs7QUFLQW1KLGNBQU1DLEdBQU4sQ0FBVSw2QkFBVixFQUNHYixJQURILENBQ1Esa0JBQVU7QUFDZCxtQkFBS2xJLGlCQUFMLEdBQXlCbUksT0FBT3hJLElBQWhDO0FBQ0gsU0FIRDs7QUFLQW1KLGNBQU1DLEdBQU4sQ0FBVSwyQkFBVixFQUNHYixJQURILENBQ1Esa0JBQVU7QUFDZCxnQkFBR0MsT0FBT3hJLElBQVAsQ0FBWTJCLE1BQVosSUFBc0IsRUFBdEIsSUFBNEI2RyxPQUFPeEksSUFBUCxDQUFZMEIsUUFBWixJQUF3QixFQUFwRCxJQUEwRDhHLE9BQU94SSxJQUFQLENBQVk4TCxTQUFaLElBQXlCLElBQXRGLEVBQTJGO0FBQ3pGLHVCQUFLdkosZUFBTCxHQUF1QixJQUFJNUIsSUFBSixDQUFTd0UsRUFBRTRHLE1BQUYsQ0FBUztBQUNwQ3BLLDRCQUFXNkcsT0FBT3hJLElBQVAsQ0FBWTJCLE1BRGE7QUFFcENELDhCQUFXOEcsT0FBT3hJLElBQVAsQ0FBWTBCLFFBRmE7QUFHcENILCtCQUFZaUgsT0FBT3hJLElBQVAsQ0FBWXVCLFNBSFk7QUFJcEN1SywrQkFBV3RELE9BQU94SSxJQUFQLENBQVk4TDtBQUphLGlCQUFULEVBSzdCdEQsT0FBT3hJLElBQVAsQ0FBWThMLFNBTGlCLENBQVQsRUFLSSxFQUFFN0ssU0FBUyxpQkFBWCxFQUxKLENBQXZCO0FBTUFrSSxzQkFBTUMsR0FBTixDQUFVLG1CQUFpQixPQUFLN0csZUFBTCxDQUFxQmxCLFFBQWhELEVBQ0drSCxJQURILENBQ1Esa0JBQVU7QUFDaEIsMkJBQUsvSCxVQUFMLEdBQWtCZ0ksT0FBT3hJLElBQXpCO0FBQ0Esd0JBQUlnTSxVQUFVLE9BQUt4TCxVQUFMLENBQWdCZ0ssTUFBaEIsQ0FBdUIsZUFBTztBQUFFLCtCQUFPLE9BQUtqSSxlQUFMLENBQXFCOUIsSUFBckIsSUFBNkJnSyxJQUFJM0MsSUFBeEM7QUFBOEMscUJBQTlFLENBQWQ7QUFDQSwyQkFBS3JILElBQUwsR0FBWXVMLFFBQVEsQ0FBUixDQUFaO0FBRUQsaUJBTkQ7QUFPRCxhQWRELE1BY0s7QUFDSHpKLGlDQUFpQixJQUFJNUIsSUFBSixDQUFTO0FBQ3BCUSw2QkFBYyxFQURNO0FBRXBCQyw4QkFBYyxFQUZNO0FBR3BCWCwwQkFBYyxFQUhNO0FBSXBCWSw4QkFBYyxFQUpNO0FBS3BCQyw4QkFBYyxFQUxNO0FBTXBCQywrQkFBYyxFQU5NO0FBT3BCQyw4QkFBYyxFQVBNO0FBUXBCQyw2QkFBYyxFQVJNO0FBU3BCQyw4QkFBYyxFQVRNO0FBVXBCQyw0QkFBYztBQVZNLGlCQUFULEVBV2YsRUFBRVYsU0FBVyxpQkFBYixFQVhlO0FBWWxCO0FBR0osU0FoQ0Q7O0FBa0NBLGFBQUsrSCxrQkFBTDs7QUFFQUcsY0FBTUMsR0FBTixDQUFVLG9CQUFWLEVBQ0diLElBREgsQ0FDUSxrQkFBVTtBQUNkLGdCQUFJaUIsUUFBUSxFQUFaO0FBQ0EsaUJBQUksSUFBSUMsSUFBSSxDQUFaLEVBQWVBLElBQUlqQixPQUFPeEksSUFBUCxDQUFZc0osTUFBL0IsRUFBdUNHLEdBQXZDLEVBQTJDO0FBQ3pDRCxzQkFBTUUsSUFBTixDQUFXbEIsT0FBT3hJLElBQVAsQ0FBWXlKLENBQVosRUFBZTNCLElBQTFCO0FBQ0Q7QUFDRCxtQkFBS3ZILFVBQUwsR0FBa0JpSixLQUFsQjtBQUNILFNBUEQ7QUFRQSxhQUFLWSxZQUFMO0FBQ0QsS0E5c0IyQjs7O0FBZ3RCNUI2QixjQUFVO0FBQ1JDLHFCQUFhLHVCQUFZO0FBQ3ZCLG1CQUFPQyxPQUFPQyxhQUFQLENBQXFCLEtBQUs5TCxlQUExQixDQUFQO0FBQ0Q7QUFITzs7QUFodEJrQixDQUFSLENBQXRCOztBQXl0QkEsU0FBUytMLGFBQVQsQ0FBdUJDLEtBQXZCLEVBQThCQyxTQUE5QixFQUNBO0FBQ0ksUUFBR3BILEVBQUVtSCxLQUFGLEVBQVNFLEdBQVQsTUFBa0IsRUFBckIsRUFDQTtBQUNJaEIsa0JBQVVyRyxFQUFFb0gsU0FBRixFQUFheEYsSUFBYixDQUFrQixjQUFsQixDQUFWO0FBQ0EwRSxpQkFBU3RHLEVBQUVvSCxTQUFGLEVBQWF4RixJQUFiLENBQWtCLFNBQWxCLENBQVQ7QUFDQTBFLGVBQU9DLE9BQVAsQ0FBZSxHQUFmLEVBQW1CLFlBQVU7QUFDekJGLG9CQUFRRyxNQUFSLENBQWUsR0FBZjtBQUNILFNBRkQ7QUFHSDtBQUNKOztBQUVEeEcsRUFBRXNILFFBQUYsRUFBWXhILEtBQVosQ0FBa0IsWUFBVTs7QUFFeEI7QUFDQSxRQUFJeUgsTUFBTXROLE9BQU91TixRQUFQLENBQWdCQyxJQUExQjtBQUNBLFFBQUcsSUFBSUMsSUFBSixDQUFTSCxHQUFULENBQUgsRUFBaUI7QUFDZixZQUFJSSxZQUFZSixJQUFJSyxTQUFKLENBQWNMLElBQUlNLE9BQUosQ0FBWSxHQUFaLElBQW1CLENBQWpDLENBQWhCO0FBQ0E3SCxVQUFFLFdBQUYsRUFBZThILFdBQWYsQ0FBMkIsV0FBM0I7QUFDQTlILFVBQUUsY0FBYTJILFNBQWIsR0FBd0IsSUFBMUIsRUFBZ0NJLEdBQWhDLENBQW9DLE1BQXBDO0FBQ0Q7QUFDRDs7O0FBR0EvSCxNQUFFLHFCQUFGLEVBQXlCWSxFQUF6QixDQUE0QixPQUE1QixFQUFvQyxZQUFVO0FBQzFDWixVQUFFLE1BQUYsRUFBVWdJLEdBQVYsQ0FBYyxXQUFkLEVBQTBCLFlBQVU7QUFDaENkLDBCQUFjLHFCQUFkLEVBQW9DLGVBQXBDO0FBQ0gsU0FGRDtBQUdILEtBSkQ7QUFLQWxILE1BQUUsMkJBQUYsRUFBK0JZLEVBQS9CLENBQWtDLE9BQWxDLEVBQTBDLFlBQVU7QUFDaERaLFVBQUUsTUFBRixFQUFVZ0ksR0FBVixDQUFjLFdBQWQsRUFBMEIsWUFBVTtBQUNoQ2QsMEJBQWMsMkJBQWQsRUFBMEMscUJBQTFDO0FBQ0gsU0FGRDtBQUdILEtBSkQ7QUFLQWxILE1BQUUsdUJBQUYsRUFBMkJZLEVBQTNCLENBQThCLE9BQTlCLEVBQXNDLFlBQVU7QUFDNUNaLFVBQUUsTUFBRixFQUFVZ0ksR0FBVixDQUFjLFdBQWQsRUFBMEIsWUFBVTtBQUNoQ2QsMEJBQWMsdUJBQWQsRUFBc0MsaUJBQXRDO0FBQ0gsU0FGRDtBQUdILEtBSkQ7QUFLQWxILE1BQUUscUJBQUYsRUFBeUJZLEVBQXpCLENBQTRCLE9BQTVCLEVBQW9DLFlBQVU7QUFDMUNaLFVBQUUsTUFBRixFQUFVZ0ksR0FBVixDQUFjLFdBQWQsRUFBMEIsWUFBVTtBQUNoQ2QsMEJBQWMscUJBQWQsRUFBb0MsZUFBcEM7QUFDSCxTQUZEO0FBR0gsS0FKRDtBQUtBbEgsTUFBRSxxQkFBRixFQUF5QlksRUFBekIsQ0FBNEIsT0FBNUIsRUFBb0MsWUFBVTtBQUMxQ1osVUFBRSxNQUFGLEVBQVVnSSxHQUFWLENBQWMsV0FBZCxFQUEwQixZQUFVO0FBQ2hDZCwwQkFBYyxxQkFBZCxFQUFvQyxlQUFwQztBQUNILFNBRkQ7QUFHSCxLQUpEO0FBS0FsSCxNQUFFLHdCQUFGLEVBQTRCWSxFQUE1QixDQUErQixPQUEvQixFQUF1QyxZQUFVO0FBQzdDWixVQUFFLE1BQUYsRUFBVWdJLEdBQVYsQ0FBYyxXQUFkLEVBQTBCLFlBQVU7QUFDaENkLDBCQUFjLHdCQUFkLEVBQXVDLGtCQUF2QztBQUNILFNBRkQ7QUFHSCxLQUpEOztBQU1GbEgsTUFBRSxxQkFBRixFQUF5QmlJLFNBQXpCLENBQW1DO0FBQ2pDQyxjQUFNO0FBRDJCLEtBQW5DOztBQUlBbEksTUFBRSxvQkFBRixFQUF3QmlJLFNBQXhCLENBQWtDO0FBQ2hDQyxjQUFNO0FBRDBCLEtBQWxDOztBQUlBbEksTUFBRSxrQkFBRixFQUFzQmlJLFNBQXRCLENBQWdDO0FBQzlCQyxjQUFNO0FBRHdCLEtBQWhDOztBQUlBbEksTUFBRSxjQUFGLEVBQWtCaUksU0FBbEIsQ0FBNEI7QUFDMUJDLGNBQU07QUFEb0IsS0FBNUI7O0FBSUNsSSxNQUFFO0FBQ0Q7QUFERCxNQUVFWSxFQUZGLENBRUssV0FGTCxFQUVrQixZQUFVO0FBQ3pCWixVQUFFLElBQUYsRUFBUW1JLFFBQVIsQ0FBaUIsUUFBakIsRUFBMkJ0RyxHQUEzQixDQUErQixFQUFDLGFBQWEsV0FBVTdCLEVBQUUsSUFBRixFQUFRb0ksSUFBUixDQUFhLFlBQWIsQ0FBVixHQUFzQyxHQUFwRCxFQUEvQjtBQUNELEtBSkYsRUFLRXhILEVBTEYsQ0FLSyxVQUxMLEVBS2lCLFlBQVU7QUFDeEJaLFVBQUUsSUFBRixFQUFRbUksUUFBUixDQUFpQixRQUFqQixFQUEyQnRHLEdBQTNCLENBQStCLEVBQUMsYUFBYSxVQUFkLEVBQS9CO0FBQ0QsS0FQRixFQVFFakIsRUFSRixDQVFLLFdBUkwsRUFRa0IsVUFBU0MsQ0FBVCxFQUFXO0FBQzFCYixVQUFFLElBQUYsRUFBUW1JLFFBQVIsQ0FBaUIsUUFBakIsRUFBMkJ0RyxHQUEzQixDQUErQjtBQUMzQixnQ0FBcUIsQ0FBQ2hCLEVBQUV3SCxLQUFGLEdBQVVySSxFQUFFLElBQUYsRUFBUXNJLE1BQVIsR0FBaUJDLElBQTVCLElBQW9DdkksRUFBRSxJQUFGLEVBQVFxQixLQUFSLEVBQXJDLEdBQXdELEdBQXhELEdBQThELElBQTlELEdBQXNFLENBQUNSLEVBQUUySCxLQUFGLEdBQVV4SSxFQUFFLElBQUYsRUFBUXNJLE1BQVIsR0FBaUJHLEdBQTVCLElBQW1DekksRUFBRSxJQUFGLEVBQVFvQixNQUFSLEVBQXBDLEdBQXdELEdBQTdILEdBQWtJO0FBRDNILFNBQS9CO0FBR0QsS0FaRjtBQWFGLENBeEVELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeHVCQTtTQUVBO0FBREEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNPQTs7O1FBRUEsQ0FFQTt1QkFDQTs7U0FFQTtlQUNBO1NBQ0E7Y0FFQTtBQUxBO0FBTUE7NkJBQ0E7b0VBQ0E7aUNBQ0E7ZUFFQTtRQUNBO0FBQ0E7UUFDQTt3Q0FDQTtnQkFDQTtxQkFDQTtBQUNBO1FBQ0E7d0NBQ0E7Z0JBQ0E7cUJBQ0E7QUFDQTtRQUNBO3dDQUNBO2dCQUNBO3FCQUNBO0FBQ0E7UUFDQTtnQkFDQTtnQkFDQTtxQkFDQTtBQUNBO1FBQ0E7Z0JBQ0E7Z0JBQ0E7cUJBQ0E7QUFDQTtRQUNBO2NBQ0E7eUVBQ0E7V0FDQTtBQUNBLGlGQUNBO1dBQ0E7QUFDQSxXQUNBO1dBQ0E7QUFDQTtnQkFDQTtnQkFDQTtnQkFDQTtxQkFDQTtBQUNBO1FBQ0E7Z0JBQ0E7Z0JBQ0E7cUJBQ0E7QUFDQTtRQUNBO2dCQUNBO2dCQUNBO3FCQUNBO0FBQ0E7UUFDQTtnQkFDQTtnQkFDQTtxQkFDQTtBQUNBO1FBQ0E7Z0JBQ0E7Z0JBQ0E7cUJBQ0E7QUFFQTs7QUFFQTtBQWxGQSxHOzs7Ozs7O0FDdkJBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx3QkFBcUo7QUFDcko7QUFDQSx3QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQ3pCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEMiLCJmaWxlIjoianMvcGFnZXMvcHJvZmlsZVNldHRpbmdzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0NDIpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGY1ZmNmZjBlMWYwODAwOTdjZjQ2IiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUgMjYgMjciLCJ2YXIgdXNlciA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XHJcbnZhciBjcGFuZWxVcmwgPSB3aW5kb3cuTGFyYXZlbC5jcGFuZWxfdXJsO1xyXG52YXIgZGlzcGxheV9uYW1lID0gd2luZG93LkxhcmF2ZWwudXNlci5kaXNwbGF5X25hbWU7XHJcbi8vY2hlY2tib3ggdmFsdWVcclxudmFyIGRpc3BsYXlfc3RhdCA9IFwiXCI7XHJcbiBpZihkaXNwbGF5X25hbWUgPT0gJ1JlYWwgTmFtZScpe1xyXG4gICAgZGlzcGxheV9zdGF0ID0gZmFsc2VcclxuIH1lbHNle1xyXG4gICAgZGlzcGxheV9zdGF0ID0gdHJ1ZVxyXG4gfVxyXG52YXIgc2VsZWN0ID0gW107XHJcblxyXG5WdWUuY29tcG9uZW50KCdxdWUtYW5zd2VyJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvcHJvZmlsZS9TZWN1cml0eVF1ZXN0aW9uQW5zd2VyLnZ1ZScpKTtcclxuVnVlLmNvbXBvbmVudCgnbm90aWYtY29udGFpbmVyJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvcHJvZmlsZS9Ob3RpZmljYXRpb25Db250YWluZXIudnVlJykpO1xyXG5cclxuXHJcblxyXG52YXIgcHJvZmlsZVNldHRpbmdzID0gbmV3IFZ1ZSh7XHJcbiAgZWw6ICcjcHJvZmlsZS1zZXR0aW5ncy1jb250YWluZXInLFxyXG4gXHJcbiAgZGF0YToge1xyXG4gICAgc2hvd1NlY3VyaXR5OiB0cnVlLFxyXG4gICAgdXNlcjp1c2VyLFxyXG4gICAgdmFsdWVRdWUxOnt9LFxyXG4gICAgdmFsdWVRdWUyOnt9LFxyXG4gICAgdmFsdWVRdWUzOnt9LFxyXG4gICAgc2VjdXJpdHlRdWVzdGlvbnM6IFtdLFxyXG4gICAgc2VjdXJpdHlBbnN3ZXJzOiBbXSxcclxuICAgIG9wdGlvblByb3Y6IFtdLFxyXG4gICAgb3B0aW9uQ2l0eTogW10sXHJcbiAgICBjaXR5OiBudWxsLFxyXG4gICAgYWNjSW5mbzogbmV3IEZvcm0oe1xyXG4gICAgICAgc2Vjb25kYXJ5X2VtYWlsOiB1c2VyLnNlY29uZGFyeV9lbWFpbFxyXG4gICAgICAsYmlydGhfZGF0ZTogdXNlci5iaXJ0aF9kYXRlXHJcbiAgICAgICxuaWNrX25hbWU6IHVzZXIubmlja19uYW1lXHJcbiAgICAgICxnZW5kZXI6IHVzZXIuZ2VuZGVyXHJcbiAgICAgICxpc19kaXNwbGF5X25hbWU6IHVzZXIuaXNfZGlzcGxheV9uYW1lXHJcbiAgICB9LHsgYmFzZVVSTDogJy9wcm9maWxlL3VwZGF0ZSd9KSxcclxuXHJcbiAgICBhZGRyZXNzSW5mbzogbmV3IEZvcm0oe1xyXG4gICAgICAgIGFkZHJlc3MgICAgIDogJydcclxuICAgICAgICxiYXJhbmdheSAgICA6ICcnXHJcbiAgICAgICAsY2l0eSAgICAgICAgOiAnJ1xyXG4gICAgICAgLHByb3ZpbmNlICAgIDogJydcclxuICAgICAgICx6aXBfY29kZSAgICA6ICcnXHJcbiAgICAgICAsYXJlYV9jb2RlICAgOiAnJ1xyXG4gICAgICAgLGxhbmRtYXJrICAgIDogJydcclxuICAgICAgICxyZW1hcmtzICAgICA6ICcnICBcclxuICAgICAgICxsYW5kbGluZSAgICA6ICcnIFxyXG4gICAgICAgLG1vYmlsZSAgICAgIDogJydcclxuICAgIH0seyBiYXNlVVJMICA6ICcvcHJvZmlsZS91cGRhdGUnfSksXHJcblxyXG4gICAgY2hhbmdlUGFzc3dvcmQ6IG5ldyBGb3JtKHtcclxuICAgICAgIG9sZF9wYXNzd29yZCA6ICcnXHJcbiAgICAgICxuZXdfcGFzc3dvcmQgOiAnJ1xyXG4gICAgICAsbmV3X3Bhc3N3b3JkX2NvbmZpcm1hdGlvbiA6ICcnXHJcbiAgICB9LHsgYmFzZVVSTDonL3Byb2ZpbGUvdXBkYXRlJ30pLFxyXG5cclxuICAgIHNlY3VyaXR5UXVlc3Rpb246IG5ldyBGb3JtKHtcclxuICAgICAgIHF1ZXN0aW9uICA6ICcnXHJcbiAgICAgICxxdWVzdGlvbjEgOiAnJ1xyXG4gICAgICAscXVlc3Rpb24yIDogJydcclxuICAgICAgLGFuc3dlciAgICA6ICcnXHJcbiAgICAgICxhbnN3ZXIxICAgOiAnJ1xyXG4gICAgICAsYW5zd2VyMiAgIDogJydcclxuICAgIH0seyBiYXNlVVJMOicvcHJvZmlsZS91cGRhdGUnfSksXHJcblxyXG4gICAgaG9tZUFkZHJlc3NJbmZvOiBuZXcgRm9ybSh7XHJcbiAgICAgICAgICBhZGRyZXNzICAgICA6ICcnXHJcbiAgICAgICAgICxiYXJhbmdheSAgICA6ICcnXHJcbiAgICAgICAgICxjaXR5ICAgICAgICA6ICcnIFxyXG4gICAgICAgICAscHJvdmluY2UgICAgOiAnJ1xyXG4gICAgICAgICAsemlwX2NvZGUgICAgOiAnJ1xyXG4gICAgICAgICAsbGFuZG1hcmsgICAgOiAnJ1xyXG4gICAgICAgICAscmVtYXJrcyAgICAgOiAnJ1xyXG4gICAgICAgICAsbGFuZGxpbmUgICAgOiAnJ1xyXG4gICAgICAgICAsYXJlYV9jb2RlICAgOiAnJ1xyXG4gICAgICAgICAsbW9iaWxlICAgICAgOiAnJ1xyXG4gICAgfSx7IGJhc2VVUkwgIDogJy9wcm9maWxlL3VwZGF0ZSd9KSxcclxuXHJcbiAgICBtb2RhbERvY1RpdGxlOicnLFxyXG4gICAgbW9kYWxEb2NJbWFnZTonJyxcclxuICAgIG5vdGlmaWNhdGlvbnM6W10sXHJcbiAgICBzZWN1cml0eUxldmVsOidwcm9ncmVzcy1iYXItZGFuZ2VyIGxvdycsXHJcblxyXG4gICAgbmJpX3N0YXR1cyAgICAgIDpudWxsLFxyXG4gICAgYmlydGhDZXJ0X3N0YXR1czpudWxsLFxyXG4gICAgZ292SURfc3RhdHVzICAgIDpudWxsLFxyXG4gICAgc2VjX3N0YXR1cyAgICAgIDpudWxsLFxyXG4gICAgYmlyX3N0YXR1cyAgICAgIDpudWxsLFxyXG4gICAgcGVybWl0X3N0YXR1cyAgIDpudWxsLFxyXG5cclxuICAgIG5iaV9leHBpcnkgICAgICA6JycsXHJcbiAgICBiaXJ0aENlcnRfZXhwaXJ5OicnLFxyXG4gICAgZ292SURfZXhwaXJ5ICAgIDonJyxcclxuICAgIHNlY19leHBpcnkgICAgICA6JycsXHJcbiAgICBiaXJfZXhwaXJ5ICAgICAgOicnLFxyXG4gICAgcGVybWl0X2V4cGlyeSAgIDonJyxcclxuXHJcbiAgICBuYmlfZmlsZSAgICAgICAgOicnLFxyXG4gICAgYmlydGhDZXJ0X2ZpbGUgIDonJyxcclxuICAgIGdvdklEX2ZpbGUgICAgICA6JycsXHJcbiAgICBzZWNfZmlsZSAgICAgICAgOicnLFxyXG4gICAgYmlyX2ZpbGUgICAgICAgIDonJyxcclxuICAgIHBlcm1pdF9maWxlICAgICA6JycsXHJcblxyXG4gICAgbmJpX3JlYXNvbiAgICAgIDonJyxcclxuICAgIGJpcnRoQ2VydF9yZWFzb246JycsXHJcbiAgICBnb3ZJRF9yZWFzb24gICAgOicnLFxyXG4gICAgc2VjX3JlYXNvbiAgICAgIDonJyxcclxuICAgIGJpcl9yZWFzb24gICAgICA6JycsXHJcbiAgICBwZXJtaXRfcmVhc29uICAgOicnLFxyXG5cclxuICAgIGRvY05iaSAgICAgICAgICA6bnVsbCxcclxuICAgIGRvY0JpcnRoQ2VydCAgICA6bnVsbCxcclxuICAgIGRvY0dvdklkICAgICAgICA6bnVsbCxcclxuICAgIGRvY1NlYyAgICAgICAgICA6bnVsbCxcclxuICAgIGRvY0JpciAgICAgICAgICA6bnVsbCxcclxuICAgIGRvY1Blcm1pdCAgICAgICA6bnVsbCxcclxuXHJcbiAgICBiYXNlSGVpZ2h0IDowLFxyXG4gICAgYmFzZVdpZHRoIDowLFxyXG4gICAgYmFzZVpvb20gOiAxLFxyXG5cclxuICAgIGxhbmcyOltdLFxyXG4gICAgdmVyaWZpZWQ6J3NzZCcsXHJcbiAgICBub3R2ZXJpZmllZDonc3NzcycsICAgIFxyXG4gICAgb3B0aW9uR2VuZGVyOiBbJ01hbGUnLCdGZW1hbGUnXSxcclxuICB9LFxyXG5cclxuXHJcbnJlYWR5OiBmdW5jdGlvbigpIHtcclxuICB0aGlzLiRuZXh0VGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykudG9vbHRpcCgpO1xyXG4gICAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoJ2ZpeFRpdGxlJyk7XHJcbiAgfSk7XHJcbn0sXHJcblxyXG4gIGNvbXBvbmVudHM6IHtcclxuICBcdE11bHRpc2VsZWN0OiB3aW5kb3cuVnVlTXVsdGlzZWxlY3QuZGVmYXVsdFxyXG4gIH0sXHJcblxyXG4gIGRpcmVjdGl2ZXM6IHtcclxuICAgIGRhdGVwaWNrZXI6IHtcclxuICAgICAgYmluZChlbCwgYmluZGluZywgdm5vZGUpIHtcclxuICAgICAgICAkKGVsKS5kYXRlcGlja2VyKHtcclxuICAgICAgICAgIGZvcm1hdDogJ3l5eXktbW0tZGQnXHJcbiAgICAgICAgfSkub24oJ2NoYW5nZURhdGUnLCAoZSkgPT4ge1xyXG4gICAgICAgICAgcHJvZmlsZVNldHRpbmdzLiRzZXQocHJvZmlsZVNldHRpbmdzLkluZm8sICdiaXJ0aF9kYXRlJywgZS5mb3JtYXQoJ3l5eXktbW0tZGQnKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9LFxyXG5cclxuICBtb3VudGVkKCkge1xyXG4gICAgJCgnI21vZGFsRG9jc1ZpZXcnKS5vbignc2hvd24uYnMubW9kYWwsc2hvdy5icy5tb2RhbCcsICgpID0+IHtcclxuICAgICAgICB0aGlzLmJhc2Vab29tID0gMTtcclxuICAgICAgXHJcbiAgICAgICAgaW1nID0gJCgnPGltZyBpZD1cInNoaXR0eWltYWdlXCIgY2xhc3M9XCJjZW50ZXItYmxvY2tcIiBzcmM9XCInKyB0aGlzLm1vZGFsRG9jSW1hZ2UgKydcIj4nKS5vbignbG9hZCcsICgpID0+IHtcclxuICAgICAgICAgIHZhciAkbW9kYWxJbWcgPSAkKCc8aW1nIHNyYz1cIicrIHRoaXMubW9kYWxEb2NJbWFnZSArJ1wiIGNsYXNzPVwiY2VudGVyLWJsb2NrXCIgPicpO1xyXG4gICAgICAgICAgJG1vZGFsSW1nLmFwcGVuZFRvKCcjZG9jLWNvbnRhaW5lcicpO1xyXG4gICAgICAgICAgdGhpcy5iYXNlSGVpZ2h0ID0gJG1vZGFsSW1nLmhlaWdodCgpO1xyXG4gICAgICAgICAgdGhpcy5iYXNlV2lkdGggPSAkbW9kYWxJbWcud2lkdGgoKTtcclxuXHJcbiAgICAgICAgICAkbW9kYWxJbWcuZHJhZ2dhYmxlKCk7XHJcbiAgICAgICAgICBcclxuICAgICAgICB9KTtcclxuICAgICAgXHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCcjbW9kYWxEb2NzVmlldycpLm9uKCdoaWRlLmJzLm1vZGFsJywgKCkgPT4ge1xyXG4gICAgICAgICQoJyNkb2MtY29udGFpbmVyJykuZW1wdHkoKTtcclxuICAgIH0pO1xyXG5cclxuICB9LFxyXG5cclxuICB3YXRjaDp7XHJcbiAgICBiYXNlWm9vbTpmdW5jdGlvbihuZXdWYWx1ZSl7XHJcbiAgICAgIHZhciBjaGFuZ2VkSGVpZ2h0U2l6ZSA9ICB0aGlzLmJhc2VIZWlnaHQgKiBuZXdWYWx1ZTtcclxuICAgICAgdmFyIGNoYW5nZWRXaWR0aFNpemUgPSB0aGlzLmJhc2VXaWR0aCAqIG5ld1ZhbHVlO1xyXG4gICAgICAkKCcjZG9jLWNvbnRhaW5lcicpLmZpbmQoJ2ltZycpLmNzcygnaGVpZ2h0JyxjaGFuZ2VkSGVpZ2h0U2l6ZSk7XHJcbiAgICAgICQoJyNkb2MtY29udGFpbmVyJykuZmluZCgnaW1nJykuY3NzKCd3aWR0aCcsY2hhbmdlZFdpZHRoU2l6ZSk7XHJcbiAgICB9LFxyXG4gICAgbW9kYWxEb2NJbWFnZTpmdW5jdGlvbigpe1xyXG4gICAgICAgIHRoaXMuYmFzZVpvb20gPSAxO1xyXG4gICAgfSxcclxuICB9LFxyXG5cclxuICBtZXRob2RzOntcclxuICAgIGxvY2tVcGxvYWRCdG46ZnVuY3Rpb24oc3RhdHVzLGV4cGlyZV9kYXRlKXtcclxuICAgICAgICAvLyBpZihzdGF0dXMgPT0gJ1BlbmRpbmcnKVxyXG4gICAgICAgIC8vIHtcclxuICAgICAgICAvLyAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICAvLyBpZihzdGF0dXMgPT0gJ0V4cGlyZWQnKVxyXG4gICAgICAgIC8vIHtcclxuICAgICAgICAvLyAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICBpZihzdGF0dXMgPT0gJ1ZlcmlmaWVkJylcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHZhciBleHBpcnlfZGF0ZSA9IG1vbWVudChleHBpcmVfZGF0ZSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZiggcGFyc2VJbnQoZXhwaXJ5X2RhdGUuZGlmZihtb21lbnQoKSwgJ2RheXMnKSkgPD0gMzApXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0sXHJcblxyXG4gICAgbGFiZWxDb2xvcjpmdW5jdGlvbihzdGF0dXMpXHJcbiAgICB7XHJcbiAgICAgIHN3aXRjaChzdGF0dXMpXHJcbiAgICAgIHtcclxuICAgICAgICBjYXNlICdQZW5kaW5nJzpcclxuICAgICAgICAgIHJldHVybiAnbGFiZWwtZGVmYXVsdCc7XHJcblxyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGNhc2UgJ0V4cGlyZWQnOlxyXG4gICAgICAgICAgICAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykudG9vbHRpcCgnZml4VGl0bGUnKTtcclxuICAgICAgICBjYXNlICdEZW5pZWQnOlxyXG4gICAgICAgICAgcmV0dXJuICdsYWJlbC1kYW5nZXInO1xyXG5cclxuICAgICAgICBicmVhaztcclxuICAgICAgICBjYXNlICdWZXJpZmllZCc6XHJcbiAgICAgICAgICByZXR1cm4gJ2xhYmVsLXN1Y2Nlc3MnO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgLy9zaG93IG1vZGFsc1xyXG4gICAgc2hpcEFkZE1vZGFsOmZ1bmN0aW9uIChldmVudCl7XHJcbiAgICAgICB2YXIgZWxTaG93ID0gZXZlbnQudGFyZ2V0Lm5hbWU7XHJcbiAgICAgICAkKCcjJytlbFNob3crMikubW9kYWwoJ3RvZ2dsZScpO1xyXG4gICAgfSxcclxuICAgIG9wZW5Eb2NNb2RhbDpmdW5jdGlvbih0aXRsZSxmaWxlbmFtZSl7XHJcbiAgICAgIHRoaXMubW9kYWxEb2NUaXRsZSA9IHRpdGxlLnRvVXBwZXJDYXNlKCk7XHJcblxyXG5cclxuICAgICAgdGhpcy5tb2RhbERvY0ltYWdlID0gJy9pbWFnZXMvZG9jdW1lbnRzLycrdXNlci5pZCsnLycrZmlsZW5hbWU7XHJcbiAgICAgICAkKCcjbW9kYWxEb2NzVmlldycpLm1vZGFsKCd0b2dnbGUnKTtcclxuICAgIH0sXHJcbiAgICAvL3NhdmUgYWNjb3VudCBpbmZvXHJcbiAgICBzYXZlSW5mbzpmdW5jdGlvbiAoZXZlbnQpe1xyXG4gICAgICAgdGhpcy5hY2NJbmZvLnN1Ym1pdCgncG9zdCcsICcvYWNjb3VudC1pbmZvJylcclxuICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYWNjSW5mbyA9IG5ldyBGb3JtKHtcclxuICAgICAgICAgICAgICAgc2Vjb25kYXJ5X2VtYWlsOiByZXN1bHQuc2Vjb25kYXJ5X2VtYWlsXHJcbiAgICAgICAgICAgICAgLGJpcnRoX2RhdGU6IHJlc3VsdC5iaXJ0aF9kYXRlXHJcbiAgICAgICAgICAgICAgLG5pY2tfbmFtZTogcmVzdWx0Lm5pY2tfbmFtZVxyXG4gICAgICAgICAgICAgICxnZW5kZXI6IHJlc3VsdC5nZW5kZXJcclxuICAgICAgICAgICAgICAsaXNfZGlzcGxheV9uYW1lOiByZXN1bHQuaXNfZGlzcGxheV9uYW1lICAgIFxyXG4gICAgICAgICAgICB9LHsgYmFzZVVSTDogJy9wcm9maWxlL3VwZGF0ZSd9KTtcclxuXHJcbiAgICAgICAgICAgICQoJyNuaWNrbmFtZScpLnRleHQocmVzdWx0Lm5pY2tfbmFtZSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmcyLmRhdGFbJ3Byb2YtdXBkYXRlZC1tc2cnXSk7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZzIuZGF0YVsndXBkYXRlLWZhaWxlZCddKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBmbmNoYW5nZVBhc3N3b3JkOmZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgdGhpcy5jaGFuZ2VQYXNzd29yZC5zdWJtaXQoJ3Bvc3QnLCAnL2NoYW5nZS1wYXNzd29yZCcpXHJcbiAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmcyLmRhdGFbJ3B3LWNoYW5nZWQtbXNnJ10pO1xyXG4gICAgICB9KVxyXG4gICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZzIuZGF0YVsndXBkYXRlLWZhaWxlZCddKTtcclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIHNhdmVTZWN1cml0eVF1ZXN0aW9uOmZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgdGhpcy5zZWN1cml0eVF1ZXN0aW9uLnN1Ym1pdCgncG9zdCcsICcvYW5zd2VyLXNlY3VyaXR5LXF1ZXN0aW9ucycpXHJcbiAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmcyLmRhdGFbJ3N1Y2MtdXBsb2FkJ10pO1xyXG4gICAgICAgICAgdGhpcy5nZXRTZWN1cml0eUFuc3dlcnMoKTtcclxuICAgICAgICAgIHRoaXMuZ2V0U2VjdXJpdHlMZXZlbCgpXHJcbiAgICAgIH0pXHJcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nMi5kYXRhWyd1cGRhdGUtZmFpbGVkJ10pO1xyXG4gICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgc2F2ZU5ld0FkZHJlc3M6ZnVuY3Rpb24oZXZlbnQpe1xyXG4gICAgICB0aGlzLmFkZHJlc3NJbmZvLnN1Ym1pdCgncG9zdCcsICcvYWRkcmVzcy1pbmZvJyk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldFNlY3VyaXR5QW5zd2VycygpIHtcclxuICAgICAgYXhpb3MuZ2V0KCcvcHJvZmlsZS9zZWN1cml0eS1hbnN3ZXJzJylcclxuICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgdGhpcy5zZWN1cml0eUFuc3dlcnMgPSByZXN1bHQuZGF0YS5xdWVzdGlvbnM7XHJcbiAgICAgICAgICBpZiAodGhpcy5zZWN1cml0eUFuc3dlcnMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd1NlY3VyaXR5ID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy4kbmV4dFRpY2soZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgdGhpcy5nZXRTZWN1cml0eUxldmVsKCk7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0Q2l0eTpmdW5jdGlvbihkYXRhKXtcclxuICAgICAgdGhpcy5ob21lQWRkcmVzc0luZm8uY2l0eSA9ICcnO1xyXG4gICAgICBheGlvcy5nZXQoJy9wcm9maWxlL2NpdHkvJytkYXRhKVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgdmFyIGFycmF5ID0gW107XHJcbiAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IHJlc3VsdC5kYXRhLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICBcclxuICAgICAgICAgIGFycmF5LnB1c2gocmVzdWx0LmRhdGFbaV0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9wdGlvbkNpdHkgPSBhcnJheTtcclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldEFyZWFDb2RlOmZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgICB0aGlzLmhvbWVBZGRyZXNzSW5mby5jaXR5ID0gZGF0YTtcclxuICAgICAgdGhpcy5ob21lQWRkcmVzc0luZm8uYXJlYV9jb2RlID0gdGhpcy5wYWRBcmVhQ29kZShkYXRhLmFyZWFfY29kZSk7XHJcblxyXG4gICAgfSxcclxuXHJcbiAgICBzYXZlSG9tZUFkZHJlc3M6ZnVuY3Rpb24oZXZlbnQpe1xyXG4gICAgICB0aGlzLmhvbWVBZGRyZXNzSW5mby5jaXR5ID0gdGhpcy5jaXR5Lm5hbWU7XHJcbiAgICAgIHRoaXMuaG9tZUFkZHJlc3NJbmZvLnN1Ym1pdCgncG9zdCcsICcvaG9tZS1hZGRyZXNzLWluZm8nLCB0aGlzLmhvbWVBZGRyZXNzSW5mbylcclxuICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZzIuZGF0YVsnc3VjYy11cGxvYWQnXSk7XHJcbiAgICAgIH0pXHJcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nMi5kYXRhWyd1cGRhdGUtZmFpbGVkJ10pO1xyXG4gICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgY2hhbmdlU2VsZWN0KHNlbGVjdGVkT3B0aW9uLCBpZCl7XHJcbiAgICAgIGlmKGlkID09IFwicXVlc3Rpb25cIil7XHJcbiAgICAgICAgc2VsZWN0WzBdID0gc2VsZWN0ZWRPcHRpb247XHJcbiAgICAgIH1lbHNlIGlmKGlkID09IFwicXVlc3Rpb24xXCIpe1xyXG4gICAgICAgIHNlbGVjdFsxXSA9IHNlbGVjdGVkT3B0aW9uO1xyXG4gICAgICB9ZWxzZSBpZihpZCA9PSBcInF1ZXN0aW9uMlwiKXtcclxuICAgICAgICBzZWxlY3RbMl0gPSBzZWxlY3RlZE9wdGlvbjtcclxuICAgICAgfVxyXG4gICAgICB2YXIgb3B0aW9ucyA9IHRoaXMuc2VjdXJpdHlRdWVzdGlvbnM7XHJcbiAgICAgIG9wdGlvbnMgPSBvcHRpb25zLm1hcChmdW5jdGlvbiAocXVlc3Rpb24pe1xyXG4gICAgICAgIHF1ZXN0aW9uLiRpc0Rpc2FibGVkID0gZmFsc2U7XHJcbiAgICAgICAgcmV0dXJuIHF1ZXN0aW9uO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgICQoc2VsZWN0KS5lYWNoKGZ1bmN0aW9uKGkpIHtcclxuICAgICAgICBvcHRpb25zID0gb3B0aW9ucy5tYXAoZnVuY3Rpb24gKHF1ZXN0aW9uKXtcclxuICAgICAgICAgICAgaWYgKHNlbGVjdFtpXS5pZCA9PSBxdWVzdGlvbi5pZCkge1xyXG4gICAgICAgICAgICAgIHF1ZXN0aW9uLiRpc0Rpc2FibGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcXVlc3Rpb247XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGdldERvY3VtZW50cygpe1xyXG4gICAgICBheGlvc0FQSXYxLmdldCgnL3VzZXJzLycrTGFyYXZlbC51c2VyLmlkKycvZG9jdW1lbnRzJylcclxuICAgICAgICAudGhlbihkb2NzT2JqID0+IHtcclxuICAgICAgICAgIGlmKGRvY3NPYmpbJ3N0YXR1cyddID09IDIwMClcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgbmJpX29iaiAgICAgICA9IGRvY3NPYmouZGF0YS5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAnbmJpJyB9KSBcclxuICAgICAgICAgICAgYmlydGhDZXJ0X29iaiA9IGRvY3NPYmouZGF0YS5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAnYmlydGhDZXJ0JyB9KVxyXG4gICAgICAgICAgICBnb3ZJRF9vYmogICAgID0gZG9jc09iai5kYXRhLmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLnR5cGUgPT09ICdnb3ZJZCcgfSlcclxuICAgICAgICAgICAgc2VjX29iaiAgICAgICA9IGRvY3NPYmouZGF0YS5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAnc2VjJyB9KVxyXG4gICAgICAgICAgICBiaXJfb2JqICAgICAgID0gZG9jc09iai5kYXRhLmZpbHRlcihvYmogPT4geyByZXR1cm4gb2JqLnR5cGUgPT09ICdiaXInIH0pXHJcbiAgICAgICAgICAgIHBlcm1pdF9vYmogICAgPSBkb2NzT2JqLmRhdGEuZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoudHlwZSA9PT0gJ3Blcm1pdCcgfSlcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmKG5iaV9vYmoubGVuZ3RoKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5iaV9zdGF0dXMgPSAgbmJpX29ialswXS5zdGF0dXM7ICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHRoaXMubmJpX2V4cGlyeSA9ICBuYmlfb2JqWzBdLmV4cGlyZXNfYXQ7ICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHRoaXMubmJpX3JlYXNvbiA9ICBuYmlfb2JqWzBdLnJlYXNvbjsgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5uYmlfZmlsZSA9ICBuYmlfb2JqWzBdLm5hbWU7IFxyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5uYmlfc3RhdHVzID09ICdFeHBpcmVkJyl7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuZG9jTmJpID0gZmFsc2U7ICBcclxuICAgICAgICAgICAgICAgIH0gICBcclxuICAgICAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuZG9jTmJpID0gdHJ1ZTsgIFxyXG4gICAgICAgICAgICAgICAgfSAgICAgICAgICBcclxuICAgICAgICAgICAgXHJcblxyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYoYmlydGhDZXJ0X29iai5sZW5ndGgpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYmlydGhDZXJ0X3N0YXR1cyA9ICBiaXJ0aENlcnRfb2JqWzBdLnN0YXR1czsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5iaXJ0aENlcnRfZXhwaXJ5ID0gIGJpcnRoQ2VydF9vYmpbMF0uZXhwaXJlc19hdDsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5iaXJ0aENlcnRfcmVhc29uID0gIGJpcnRoQ2VydF9vYmpbMF0ucmVhc29uOyAgXHJcbiAgICAgICAgICAgICAgICB0aGlzLmJpcnRoQ2VydF9maWxlID0gIGJpcnRoQ2VydF9vYmpbMF0ubmFtZTsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5kb2NCaXJ0aENlcnQgPSB0cnVlOyAgXHJcbiAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYoZ292SURfb2JqLmxlbmd0aClcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nb3ZJRF9zdGF0dXMgPSAgZ292SURfb2JqWzBdLnN0YXR1czsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5nb3ZJRF9leHBpcnkgPSAgZ292SURfb2JqWzBdLmV4cGlyZXNfYXQ7ICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHRoaXMuZ292SURfcmVhc29uID0gIGdvdklEX29ialswXS5yZWFzb247ICBcclxuICAgICAgICAgICAgICAgIHRoaXMuZ292SURfZmlsZSA9ICBnb3ZJRF9vYmpbMF0ubmFtZTsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5kb2NHb3ZJZCA9IHRydWU7ICBcclxuICAgICAgICAgICAgXHJcblxyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYoc2VjX29iai5sZW5ndGgpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VjX3N0YXR1cyA9ICBzZWNfb2JqWzBdLnN0YXR1czsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWNfZXhwaXJ5ID0gIHNlY19vYmpbMF0uZXhwaXJlc19hdDsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWNfcmVhc29uID0gIHNlY19vYmpbMF0ucmVhc29uOyAgXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlY19maWxlID0gIHNlY19vYmpbMF0ubmFtZTsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5kb2NTZWMgPSB0cnVlOyAgXHJcbiAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYoYmlyX29iai5sZW5ndGgpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYmlyX3N0YXR1cyA9ICBiaXJfb2JqWzBdLnN0YXR1czsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5iaXJfZXhwaXJ5ID0gIGJpcl9vYmpbMF0uZXhwaXJlc19hdDsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5iaXJfcmVhc29uID0gIGJpcl9vYmpbMF0ucmVhc29uOyAgXHJcbiAgICAgICAgICAgICAgICB0aGlzLmJpcl9maWxlID0gIGJpcl9vYmpbMF0ubmFtZTsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5kb2NCaXIgPSB0cnVlOyAgXHJcbiAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYocGVybWl0X29iai5sZW5ndGgpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucGVybWl0X3N0YXR1cyA9ICBwZXJtaXRfb2JqWzBdLnN0YXR1czsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wZXJtaXRfZXhwaXJ5ID0gIHBlcm1pdF9vYmpbMF0uZXhwaXJlc19hdDsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wZXJtaXRfcmVhc29uID0gIHBlcm1pdF9vYmpbMF0ucmVhc29uOyAgXHJcbiAgICAgICAgICAgICAgICB0aGlzLnBlcm1pdF9maWxlID0gIHBlcm1pdF9vYmpbMF0ubmFtZTsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5kb2NQZXJtaXQgPSB0cnVlOyAgXHJcbiAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5nZXRTZWN1cml0eUxldmVsKCk7XHJcbiAgICAgICAgICAgIHRoaXMuJG5leHRUaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoJ2ZpeFRpdGxlJyk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgIC8vIHRvYXN0ci5lcnJvcignVXBkYXRlZCBGYWlsZWQuJyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRTZWN1cml0eUxldmVsKCl7XHJcblxyXG4gICAgICAgIHZhciBpc19kb2NzU3VibWl0dGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgaXNfZG9jc1N1Ym1pdHRlZCA9IFxyXG4gICAgICAgICgodGhpcy5uYmlfc3RhdHVzPT0nVmVyaWZpZWQnKSoodGhpcy5nb3ZJRF9zdGF0dXM9PSdWZXJpZmllZCcpKVxyXG4gICAgICAgICAgICB8fFxyXG4gICAgICAgICgodGhpcy5zZWNfc3RhdHVzPT0nVmVyaWZpZWQnKSoodGhpcy5iaXJfc3RhdHVzPT0nVmVyaWZpZWQnKSoodGhpcy5wZXJtaXRfc3RhdHVzPT0nVmVyaWZpZWQnKSk7XHJcblxyXG5cclxuICAgICAgICBpZih0aGlzLnNlY3VyaXR5QW5zd2Vycy5sZW5ndGggJiYgaXNfZG9jc1N1Ym1pdHRlZClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VjdXJpdHlMZXZlbCA9ICdwcm9ncmVzcy1iYXItc3VjY2VzcyBoaWdoJztcclxuICAgICAgICB9ZWxzZSBpZih0aGlzLnNlY3VyaXR5QW5zd2Vycy5sZW5ndGggfHwgaXNfZG9jc1N1Ym1pdHRlZClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VjdXJpdHlMZXZlbCA9ICdwcm9ncmVzcy1iYXItd2FybmluZyBtZWQnO1xyXG4gICAgICAgIH1lbHNlXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLnNlY3VyaXR5TGV2ZWwgPSAncHJvZ3Jlc3MtYmFyLWRhbmdlciBsb3cnO1xyXG4gICAgICAgIH1cclxuICAgICAgIFxyXG4gICAgfSxcclxuXHJcbiAgICBwYWRBcmVhQ29kZShuKXtcclxuICAgICAgaWYobilcclxuICAgICAge1xyXG4gICAgICAgIHJldHVybiAoJzAwMDAwJyArIG4pLnNsaWNlKC01KTsgXHJcbiAgICAgIH1lbHNlXHJcbiAgICAgIHtcclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgfSxcclxuXHJcbiAgY3JlYXRlZCgpIHtcclxuXHJcbiAgICBheGlvcy5nZXQoJy90cmFuc2xhdGUvcHJvZmlsZS1zZXR0aW5ncycpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubGFuZzIgPSByZXN1bHQuZGF0YTtcclxuICAgIH0pO1xyXG4gICAgLy9pbmRpdmlkdWFsXHJcbiAgICBFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQubmJpVXBsb2FkJywoZGF0YSk9PntcclxuICAgICAgICBpZihkYXRhKXtcclxuICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy91c2Vycy8nK0xhcmF2ZWwudXNlci5pZCsnL2RvY3VtZW50cz9kb2N0eXBlPW5iaScpXHJcbiAgICAgICAgICAgICAgICAudGhlbihkb2NzT2JqID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZihkb2NzT2JqLmRhdGEubGVuZ3RoKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kb2NOYmkgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5iaV9zdGF0dXMgPSBkb2NzT2JqLmRhdGFbMF0uc3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5iaV9maWxlID0gZG9jc09iai5kYXRhWzBdLm5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZzIuZGF0YVsndXBsb2FkJ10sdGhpcy5sYW5nMi5kYXRhWydzdWNjLXVwbG9hZCddKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmxhbmcyLmRhdGFbJ3VwbG9hZCddLHRoaXMubGFuZzIuZGF0YVsndHJ5LWFnYWluJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICBjb250ZW50ID0gJCgnLmJ0bk5iaVVwbG9hZCcpLmZpbmQoJy5idG4tY29udGVudCcpO1xyXG4gICAgICAgICAgbG9hZGVyID0gJCgnLmJ0bk5iaVVwbG9hZCcpLmZpbmQoJy5sb2FkZXInKTtcclxuICAgICAgICAgIGxvYWRlci5mYWRlT3V0KDEwMCxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBjb250ZW50LmZhZGVJbigxMDApO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfWVsc2VcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQgPSAkKCcuYnRuTmJpVXBsb2FkJykuZmluZCgnLmJ0bi1jb250ZW50Jyk7XHJcbiAgICAgICAgICAgIGxvYWRlciA9ICQoJy5idG5OYmlVcGxvYWQnKS5maW5kKCcubG9hZGVyJyk7XHJcbiAgICAgICAgICAgIGxvYWRlci5mYWRlT3V0KDEwMCxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgIGNvbnRlbnQuZmFkZUluKDEwMCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9KVxyXG4gICAgRXZlbnQubGlzdGVuKCdJbWdmaWxldXBsb2FkLmJpcnRoQ2VydFVwbG9hZCcsKGRhdGEpPT57XHJcbiAgICAgICAgaWYoZGF0YSl7XHJcbiAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvdXNlcnMvJytMYXJhdmVsLnVzZXIuaWQrJy9kb2N1bWVudHM/ZG9jdHlwZT1iaXJ0aENlcnQnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oZG9jc09iaiA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoZG9jc09iai5kYXRhLmxlbmd0aCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZG9jQmlydGhDZXJ0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iaXJ0aENlcnRfc3RhdHVzID0gZG9jc09iai5kYXRhWzBdLnN0YXR1cztcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iaXJ0aENlcnRfZmlsZSA9IGRvY3NPYmouZGF0YVswXS5uYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmcyLmRhdGFbJ3VwbG9hZCddLHRoaXMubGFuZzIuZGF0YVsnc3VjYy11cGxvYWQnXSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nMi5kYXRhWyd1cGxvYWQnXSx0aGlzLmxhbmcyLmRhdGFbJ3RyeS1hZ2FpbiddKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgY29udGVudCA9ICQoJy5idG5CaXJ0aENlcnRVcGxvYWQnKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuICAgICAgICAgIGxvYWRlciA9ICQoJy5idG5CaXJ0aENlcnRVcGxvYWQnKS5maW5kKCcubG9hZGVyJyk7XHJcbiAgICAgICAgICBsb2FkZXIuZmFkZU91dCgxMDAsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgY29udGVudC5mYWRlSW4oMTAwKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1lbHNlXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgY29udGVudCA9ICQoJy5idG5CaXJ0aENlcnRVcGxvYWQnKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuICAgICAgICAgIGxvYWRlciA9ICQoJy5idG5CaXJ0aENlcnRVcGxvYWQnKS5maW5kKCcubG9hZGVyJyk7XHJcbiAgICAgICAgICBsb2FkZXIuZmFkZU91dCgxMDAsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgY29udGVudC5mYWRlSW4oMTAwKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0pXHJcbiAgICBFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQuZ292SWRVcGxvYWQnLChkYXRhKT0+e1xyXG4gICAgICAgIGlmKGRhdGEpe1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL3VzZXJzLycrTGFyYXZlbC51c2VyLmlkKycvZG9jdW1lbnRzP2RvY3R5cGU9Z292SWQnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oZG9jc09iaiA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRvY3NPYmopO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRvY3NPYmouZGF0YS5sZW5ndGgpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRvY0dvdklkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nb3ZJRF9zdGF0dXMgPSBkb2NzT2JqLmRhdGFbMF0uc3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdvdklEX2ZpbGUgPSBkb2NzT2JqLmRhdGFbMF0ubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nMi5kYXRhWyd1cGxvYWQnXSx0aGlzLmxhbmcyLmRhdGFbJ3N1Y2MtdXBsb2FkJ10pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZzIuZGF0YVsndXBsb2FkJ10sdGhpcy5sYW5nMi5kYXRhWyd0cnktYWdhaW4nXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgIGNvbnRlbnQgPSAkKCcuYnRuR292SWRVcGxvYWQnKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuICAgICAgICAgIGxvYWRlciA9ICQoJy5idG5Hb3ZJZFVwbG9hZCcpLmZpbmQoJy5sb2FkZXInKTtcclxuICAgICAgICAgIGxvYWRlci5mYWRlT3V0KDEwMCxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBjb250ZW50LmZhZGVJbigxMDApO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfWVsc2VcclxuICAgICAgICB7XHJcbiAgICAgICAgICBjb250ZW50ID0gJCgnLmJ0bkdvdklkVXBsb2FkJykuZmluZCgnLmJ0bi1jb250ZW50Jyk7XHJcbiAgICAgICAgICBsb2FkZXIgPSAkKCcuYnRuR292SWRVcGxvYWQnKS5maW5kKCcubG9hZGVyJyk7XHJcbiAgICAgICAgICBsb2FkZXIuZmFkZU91dCgxMDAsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgY29udGVudC5mYWRlSW4oMTAwKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0pXHJcbiAgICAvLyBlbnRlcnByaXNlXHJcbiAgICBFdmVudC5saXN0ZW4oJ0ltZ2ZpbGV1cGxvYWQuc2VjVXBsb2FkJywoZGF0YSk9PntcclxuICAgICAgICBpZihkYXRhKXtcclxuICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy91c2Vycy8nK0xhcmF2ZWwudXNlci5pZCsnL2RvY3VtZW50cz9kb2N0eXBlPXNlYycpXHJcbiAgICAgICAgICAgICAgICAudGhlbihkb2NzT2JqID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZihkb2NzT2JqLmRhdGEubGVuZ3RoKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kb2NTZWMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlY19zdGF0dXMgPSBkb2NzT2JqLmRhdGFbMF0uc3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlY19maWxlID0gZG9jc09iai5kYXRhWzBdLm5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZzIuZGF0YVsndXBsb2FkJ10sdGhpcy5sYW5nMi5kYXRhWydzdWNjLXVwbG9hZCddKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmxhbmcyLmRhdGFbJ3VwbG9hZCddLHRoaXMubGFuZzIuZGF0YVsndHJ5LWFnYWluJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICBjb250ZW50ID0gJCgnLmJ0blNlY1VwbG9hZCcpLmZpbmQoJy5idG4tY29udGVudCcpO1xyXG4gICAgICAgICAgbG9hZGVyID0gJCgnLmJ0blNlY1VwbG9hZCcpLmZpbmQoJy5sb2FkZXInKTtcclxuICAgICAgICAgIGxvYWRlci5mYWRlT3V0KDEwMCxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBjb250ZW50LmZhZGVJbigxMDApO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfWVsc2VcclxuICAgICAgICB7XHJcbiAgICAgICAgICBjb250ZW50ID0gJCgnLmJ0blNlY1VwbG9hZCcpLmZpbmQoJy5idG4tY29udGVudCcpO1xyXG4gICAgICAgICAgbG9hZGVyID0gJCgnLmJ0blNlY1VwbG9hZCcpLmZpbmQoJy5sb2FkZXInKTtcclxuICAgICAgICAgIGxvYWRlci5mYWRlT3V0KDEwMCxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBjb250ZW50LmZhZGVJbigxMDApO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSlcclxuICAgIEV2ZW50Lmxpc3RlbignSW1nZmlsZXVwbG9hZC5iaXJVcGxvYWQnLChkYXRhKT0+e1xyXG4gICAgICAgIGlmKGRhdGEpe1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL3VzZXJzLycrTGFyYXZlbC51c2VyLmlkKycvZG9jdW1lbnRzP2RvY3R5cGU9YmlyJylcclxuICAgICAgICAgICAgICAgIC50aGVuKGRvY3NPYmogPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRvY3NPYmouZGF0YS5sZW5ndGgpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRvY0JpciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmlyX3N0YXR1cyA9IGRvY3NPYmouZGF0YVswXS5zdGF0dXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmlyX2ZpbGUgPSBkb2NzT2JqLmRhdGFbMF0ubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nMi5kYXRhWyd1cGxvYWQnXSx0aGlzLmxhbmcyLmRhdGFbJ3N1Y2MtdXBsb2FkJ10pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RyLmVycm9yKHRoaXMubGFuZzIuZGF0YVsndXBsb2FkJ10sdGhpcy5sYW5nMi5kYXRhWyd0cnktYWdhaW4nXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgIGNvbnRlbnQgPSAkKCcuYnRuQmlyVXBsb2FkJykuZmluZCgnLmJ0bi1jb250ZW50Jyk7XHJcbiAgICAgICAgICBsb2FkZXIgPSAkKCcuYnRuQmlyVXBsb2FkJykuZmluZCgnLmxvYWRlcicpO1xyXG4gICAgICAgICAgbG9hZGVyLmZhZGVPdXQoMTAwLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIGNvbnRlbnQuZmFkZUluKDEwMCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9ZWxzZVxyXG4gICAgICAgIHtcclxuICAgICAgICAgIGNvbnRlbnQgPSAkKCcuYnRuQmlyVXBsb2FkJykuZmluZCgnLmJ0bi1jb250ZW50Jyk7XHJcbiAgICAgICAgICBsb2FkZXIgPSAkKCcuYnRuQmlyVXBsb2FkJykuZmluZCgnLmxvYWRlcicpO1xyXG4gICAgICAgICAgbG9hZGVyLmZhZGVPdXQoMTAwLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIGNvbnRlbnQuZmFkZUluKDEwMCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9KVxyXG4gICAgRXZlbnQubGlzdGVuKCdJbWdmaWxldXBsb2FkLnBlcm1pdFVwbG9hZCcsKGRhdGEpPT57XHJcbiAgICAgICAgaWYoZGF0YSl7XHJcbiAgICAgICAgICAgIGF4aW9zQVBJdjEuZ2V0KCcvdXNlcnMvJytMYXJhdmVsLnVzZXIuaWQrJy9kb2N1bWVudHM/ZG9jdHlwZT1wZXJtaXQnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oZG9jc09iaiA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoZG9jc09iai5kYXRhLmxlbmd0aCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZG9jUGVybWl0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wZXJtaXRfc3RhdHVzID0gZG9jc09iai5kYXRhWzBdLnN0YXR1cztcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wZXJtaXRfZmlsZSA9IGRvY3NPYmouZGF0YVswXS5uYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmcyLmRhdGFbJ3VwbG9hZCddLHRoaXMubGFuZzIuZGF0YVsnc3VjYy11cGxvYWQnXSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nMi5kYXRhWyd1cGxvYWQnXSx0aGlzLmxhbmcyLmRhdGFbJ3RyeS1hZ2FpbiddKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgY29udGVudCA9ICQoJy5idG5QZXJtaXRVcGxvYWQnKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuICAgICAgICAgIGxvYWRlciA9ICQoJy5idG5QZXJtaXRVcGxvYWQnKS5maW5kKCcubG9hZGVyJyk7XHJcbiAgICAgICAgICBsb2FkZXIuZmFkZU91dCgxMDAsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgY29udGVudC5mYWRlSW4oMTAwKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1lbHNlXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgY29udGVudCA9ICQoJy5idG5QZXJtaXRVcGxvYWQnKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuICAgICAgICAgIGxvYWRlciA9ICQoJy5idG5QZXJtaXRVcGxvYWQnKS5maW5kKCcubG9hZGVyJyk7XHJcbiAgICAgICAgICBsb2FkZXIuZmFkZU91dCgxMDAsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgY29udGVudC5mYWRlSW4oMTAwKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0pXHJcblxyXG5cclxuICAgIGF4aW9zLmdldCgnL3Byb2ZpbGUvbm90aWZpY2F0aW9ucycpXHJcbiAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zID0gcmVzdWx0LmRhdGE7XHJcbiAgICB9KTtcclxuXHJcbiAgICBheGlvcy5nZXQoJy9wcm9maWxlL3NlY3VyaXR5LXF1ZXN0aW9ucycpXHJcbiAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgdGhpcy5zZWN1cml0eVF1ZXN0aW9ucyA9IHJlc3VsdC5kYXRhO1xyXG4gICAgfSk7XHJcblxyXG4gICAgYXhpb3MuZ2V0KCcvcHJvZmlsZS9nZXQtaG9tZS1hZGRyZXNzJylcclxuICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICBpZihyZXN1bHQuZGF0YS5tb2JpbGUgIT0gJycgJiYgcmVzdWx0LmRhdGEubGFuZGxpbmUgIT0gJycgJiYgcmVzdWx0LmRhdGEucmVzaWRlbmNlICE9IG51bGwpe1xyXG4gICAgICAgICAgdGhpcy5ob21lQWRkcmVzc0luZm8gPSBuZXcgRm9ybSgkLmV4dGVuZCh7XHJcbiAgICAgICAgICAgICAgIG1vYmlsZSAgIDogcmVzdWx0LmRhdGEubW9iaWxlXHJcbiAgICAgICAgICAgICAgLGxhbmRsaW5lIDogcmVzdWx0LmRhdGEubGFuZGxpbmVcclxuICAgICAgICAgICAgICAsYXJlYV9jb2RlIDogcmVzdWx0LmRhdGEuYXJlYV9jb2RlXHJcbiAgICAgICAgICAgICAgLHJlc2lkZW5jZTogcmVzdWx0LmRhdGEucmVzaWRlbmNlXHJcbiAgICAgICAgICB9LCByZXN1bHQuZGF0YS5yZXNpZGVuY2UpLCB7IGJhc2VVUkw6ICcvcHJvZmlsZS91cGRhdGUnfSk7XHJcbiAgICAgICAgICBheGlvcy5nZXQoJy9wcm9maWxlL2NpdHkvJyt0aGlzLmhvbWVBZGRyZXNzSW5mby5wcm92aW5jZSlcclxuICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25DaXR5ID0gcmVzdWx0LmRhdGE7XHJcbiAgICAgICAgICAgIHZhciBjaXR5T2JqID0gdGhpcy5vcHRpb25DaXR5LmZpbHRlcihvYmogPT4geyByZXR1cm4gdGhpcy5ob21lQWRkcmVzc0luZm8uY2l0eSA9PSBvYmoubmFtZSB9KVxyXG4gICAgICAgICAgICB0aGlzLmNpdHkgPSBjaXR5T2JqWzBdO1xyXG5cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgaG9tZUFkZHJlc3NJbmZvOiBuZXcgRm9ybSh7XHJcbiAgICAgICAgICAgICAgICBhZGRyZXNzICAgICA6ICcnXHJcbiAgICAgICAgICAgICAgICxiYXJhbmdheSAgICA6ICcnXHJcbiAgICAgICAgICAgICAgICxjaXR5ICAgICAgICA6ICcnIFxyXG4gICAgICAgICAgICAgICAscHJvdmluY2UgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsemlwX2NvZGUgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsYXJlYV9jb2RlICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsbGFuZG1hcmsgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAscmVtYXJrcyAgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsbGFuZGxpbmUgICAgOiAnJ1xyXG4gICAgICAgICAgICAgICAsbW9iaWxlICAgICAgOiAnJ1xyXG4gICAgICAgICAgfSx7IGJhc2VVUkwgIDogJy9wcm9maWxlL3VwZGF0ZSd9KVxyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5nZXRTZWN1cml0eUFuc3dlcnMoKTtcclxuICAgIFxyXG4gICAgYXhpb3MuZ2V0KCcvcHJvZmlsZS9wcm92aW5jZXMnKVxyXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgIHZhciBhcnJheSA9IFtdO1xyXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCByZXN1bHQuZGF0YS5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICBhcnJheS5wdXNoKHJlc3VsdC5kYXRhW2ldLm5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9wdGlvblByb3YgPSBhcnJheTtcclxuICAgIH0pO1xyXG4gICAgdGhpcy5nZXREb2N1bWVudHMoKTtcclxuICB9LFxyXG5cclxuICBjb21wdXRlZDoge1xyXG4gICAgYW5zd2VyRW1wdHk6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgcmV0dXJuIGpRdWVyeS5pc0VtcHR5T2JqZWN0KHRoaXMuc2VjdXJpdHlBbnN3ZXJzKTtcclxuICAgIH0sICBcclxuICB9LFxyXG5cclxuXHJcbn0pO1xyXG5cclxuZnVuY3Rpb24gcmVtb3ZlTG9hZGluZyhpbnB1dCwgY29udGFpbmVyKVxyXG57XHJcbiAgICBpZigkKGlucHV0KS52YWwoKSA9PSAnJylcclxuICAgIHtcclxuICAgICAgICBjb250ZW50ID0gJChjb250YWluZXIpLmZpbmQoJy5idG4tY29udGVudCcpO1xyXG4gICAgICAgIGxvYWRlciA9ICQoY29udGFpbmVyKS5maW5kKCcubG9hZGVyJyk7XHJcbiAgICAgICAgbG9hZGVyLmZhZGVPdXQoMTAwLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIGNvbnRlbnQuZmFkZUluKDEwMCk7XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxufVxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcclxuXHJcbiAgICAvL2NvZGUgdG8gZ28gdG8gdGFiIHVzaW5nIHVybCB3aXRoICNcclxuICAgIHZhciB1cmwgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcclxuICAgIGlmKC8jLy50ZXN0KHVybCkpe1xyXG4gICAgICB2YXIgYWN0aXZlVGFiID0gdXJsLnN1YnN0cmluZyh1cmwuaW5kZXhPZihcIiNcIikgKyAxKTtcclxuICAgICAgJChcIi50YWItcGFuZVwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZSBpblwiKTtcclxuICAgICAgJCgnYVtocmVmPVwiIycrIGFjdGl2ZVRhYiArJ1wiXScpLnRhYignc2hvdycpO1xyXG4gICAgfVxyXG4gICAgLy9jb25zb2xlLmxvZyhhY3RpdmVUYWIpO1xyXG5cclxuXHJcbiAgICAkKCcubmJpLXVwbG9hZGVyIGlucHV0Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJ2JvZHknKS5vbmUoJ21vdXNlbW92ZScsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgcmVtb3ZlTG9hZGluZygnLm5iaS11cGxvYWRlciBpbnB1dCcsJy5idG5OYmlVcGxvYWQnKTtcclxuICAgICAgICB9KVxyXG4gICAgfSlcclxuICAgICQoJy5iaXJ0aENlcnQtdXBsb2FkZXIgaW5wdXQnKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgJCgnYm9keScpLm9uZSgnbW91c2Vtb3ZlJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICByZW1vdmVMb2FkaW5nKCcuYmlydGhDZXJ0LXVwbG9hZGVyIGlucHV0JywnLmJ0bkJpcnRoQ2VydFVwbG9hZCcpO1xyXG4gICAgICAgIH0pXHJcbiAgICB9KVxyXG4gICAgJCgnLmdvdklELXVwbG9hZGVyIGlucHV0Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJ2JvZHknKS5vbmUoJ21vdXNlbW92ZScsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgcmVtb3ZlTG9hZGluZygnLmdvdklELXVwbG9hZGVyIGlucHV0JywnLmJ0bkdvdklkVXBsb2FkJyk7XHJcbiAgICAgICAgfSlcclxuICAgIH0pXHJcbiAgICAkKCcuc2VjLXVwbG9hZGVyIGlucHV0Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJ2JvZHknKS5vbmUoJ21vdXNlbW92ZScsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgcmVtb3ZlTG9hZGluZygnLnNlYy11cGxvYWRlciBpbnB1dCcsJy5idG5TZWNVcGxvYWQnKTtcclxuICAgICAgICB9KVxyXG4gICAgfSlcclxuICAgICQoJy5iaXItdXBsb2FkZXIgaW5wdXQnKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgJCgnYm9keScpLm9uZSgnbW91c2Vtb3ZlJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICByZW1vdmVMb2FkaW5nKCcuYmlyLXVwbG9hZGVyIGlucHV0JywnLmJ0bkJpclVwbG9hZCcpO1xyXG4gICAgICAgIH0pXHJcbiAgICB9KVxyXG4gICAgJCgnLnBlcm1pdC11cGxvYWRlciBpbnB1dCcpLm9uKCdjbGljaycsZnVuY3Rpb24oKXtcclxuICAgICAgICAkKCdib2R5Jykub25lKCdtb3VzZW1vdmUnLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHJlbW92ZUxvYWRpbmcoJy5wZXJtaXQtdXBsb2FkZXIgaW5wdXQnLCcuYnRuUGVybWl0VXBsb2FkJyk7XHJcbiAgICAgICAgfSlcclxuICAgIH0pXHJcblxyXG4gICQoJyN0eHQtaG9tZS1hcmVhLWNvZGUnKS5pbnB1dG1hc2soe1xyXG4gICAgbWFzazogJzk5OTk5J1xyXG4gIH0pXHJcblxyXG4gICQoJyN0eHQtaG9tZS1sYW5kbGluZScpLmlucHV0bWFzayh7XHJcbiAgICBtYXNrOiAnOTk5LTk5OTknXHJcbiAgfSlcclxuXHJcbiAgJCgnI3R4dC1ob21lLW1vYmlsZScpLmlucHV0bWFzayh7XHJcbiAgICBtYXNrOiAnKDk5OTkpIDk5OS05OTk5J1xyXG4gIH0pXHJcblxyXG4gICQoJyN0eHQtc2hpcC16YycpLmlucHV0bWFzayh7XHJcbiAgICBtYXNrOiAnOTk5OTknXHJcbiAgfSlcclxuXHJcbiAgICQoJy50aWxlJylcclxuICAgIC8vIHRpbGUgbW91c2UgYWN0aW9uc1xyXG4gICAgLm9uKCdtb3VzZW92ZXInLCBmdW5jdGlvbigpe1xyXG4gICAgICAkKHRoaXMpLmNoaWxkcmVuKCcucGhvdG8nKS5jc3Moeyd0cmFuc2Zvcm0nOiAnc2NhbGUoJysgJCh0aGlzKS5hdHRyKCdkYXRhLXNjYWxlJykgKycpJ30pO1xyXG4gICAgfSlcclxuICAgIC5vbignbW91c2VvdXQnLCBmdW5jdGlvbigpe1xyXG4gICAgICAkKHRoaXMpLmNoaWxkcmVuKCcucGhvdG8nKS5jc3Moeyd0cmFuc2Zvcm0nOiAnc2NhbGUoMSknfSk7XHJcbiAgICB9KVxyXG4gICAgLm9uKCdtb3VzZW1vdmUnLCBmdW5jdGlvbihlKXtcclxuICAgICAgJCh0aGlzKS5jaGlsZHJlbignLnBob3RvJykuY3NzKHtcclxuICAgICAgICAgICd0cmFuc2Zvcm0tb3JpZ2luJzogKChlLnBhZ2VYIC0gJCh0aGlzKS5vZmZzZXQoKS5sZWZ0KSAvICQodGhpcykud2lkdGgoKSkgKiAxMDAgKyAnJSAnICsgKChlLnBhZ2VZIC0gJCh0aGlzKS5vZmZzZXQoKS50b3ApIC8gJCh0aGlzKS5oZWlnaHQoKSkgKiAxMDAgKyclJ1xyXG4gICAgICB9KTtcclxuICAgIH0pXHJcbn0pXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9wcm9maWxlU2V0dGluZ3MuanMiLCI8dGVtcGxhdGU+XHJcblx0PGRpdiBjbGFzcz1cImNvbC1zbS0xMiBzZWN1cml0eS1xdWVzdGlvbi1zZXRcIj5cclxuXHRcdDxkaXYgY2xhc3M9XCJjb2wtc20tOFwiPlxyXG5cdFx0IFx0PGxhYmVsIGNsYXNzPVwiYWNjb3VudC1sYWJlbCBxdWVzdGlvbi1sYWJlbFwiPnt7aXRlbS5xdWVzdGlvbn19PC9sYWJlbD5cclxuXHRcdDwvZGl2PjwhLS0gZW5kIGNvbC1zbS04IC0tPlxyXG5cclxuXHRcdDxkaXYgY2xhc3M9XCJjb2wtc20tNFwiPlxyXG5cdFx0XHQ8bGFiZWwgY2xhc3M9XCJhY2NvdW50LWxhYmVsIGFuc3dlci1sYWJlbFwiPnt7dGhpcy4kcGFyZW50LmxhbmcyLmRhdGFbJ2Fucy1vbi1maWxlJ119fTwvbGFiZWw+XHJcblx0XHQgXHQ8cD48L3A+XHJcblx0XHQ8L2Rpdj48IS0tIGVuZCBjb2wtc20tNCAtLT5cclxuXHQ8L2Rpdj48IS0tIGVuZCBzZWN1cml0eS1xdWVzdGlvbi1zZXQgLS0+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5cdGV4cG9ydCBkZWZhdWx0e1xyXG5cdFx0cHJvcHM6WydpdGVtJ11cclxuXHR9XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBTZWN1cml0eVF1ZXN0aW9uQW5zd2VyLnZ1ZT85YjEyOTFhYyIsIjx0ZW1wbGF0ZT5cclxuXHQ8bGkgY2xhc3M9J2NvbC1tZC0xMiAnIDpjbGFzcz0nbm90aWYucmVhZF9hdD9cIlwiOlwibm90LXJlYWRcIicgdi1pZj0nbm90aWZfdHlwZSE9XCJOZXdDaGF0TWVzc2FnZVwiJz5cclxuXHRcdDxhIDpocmVmPSdsaW5rJyB0YXJnZXQ9XCJfYmxhbmtcIj5cclxuXHRcdFx0PGRpdiBjbGFzcz0naW1hZ2UnPlxyXG5cdFx0XHRcdDxpbWcgOnNyYz1cIm5vdGlmLmRhdGEuaW1hZ2VcIj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHRcdDxkaXYgY2xhc3M9J2NvbnRlbnQnID5cclxuXHRcdFx0XHQ8c3Bhbj48c3Ryb25nPnt7IG5vdGlmLmRhdGEudGl0bGUgfX08L3N0cm9uZz48L3NwYW4+XHJcblx0XHRcdFx0PGJyPlxyXG5cdFx0XHRcdDxzcGFuPnt7IG5vdGlmLmRhdGEuYm9keSB9fTwvc3Bhbj5cclxuXHRcdFx0XHQ8YnI+XHJcblx0XHRcdFx0PHNwYW4+PHNtYWxsPnt7IG5vdGlmLmh1bWFuX2NyZWF0ZWRfYXQgfX08L3NtYWxsPjwvc3Bhbj5cclxuXHRcdFx0XHQ8IS0tIDxwPlxyXG5cdFx0XHRcdFx0PHNwYW4gY2xhc3M9J2ZhICcgOmNsYXNzPVwiaWNvbisnICcraWNvbkNvbG9yXCI+PC9zcGFuPlxyXG5cdFx0XHRcdFx0PHNwYW4gY2xhc3M9J3RpbWVzdGFtcCcgdi10ZXh0PSdub3RpZi5odW1hbl9jcmVhdGVkX2F0Jz48L3NwYW4+XHJcblx0XHRcdFx0PC9wPiAtLT5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQ8L2E+XHJcblx0PC9saT5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcblx0ZXhwb3J0IGRlZmF1bHR7XHJcblx0XHRwcm9wczpbXHJcblx0XHRcdCdub3RpZidcclxuXHRcdF0sXHJcblx0XHRkYXRhOmZ1bmN0aW9uICgpIHtcclxuICAgICAgICBcdHJldHVybiB7XHJcbiAgICAgICAgICAgIFx0bGluazpcIlwiLFxyXG4gICAgICAgICAgICBcdG5vdGlmX3R5cGU6JycsXHJcbiAgICAgICAgICAgIFx0aWNvbjonJyxcclxuICAgICAgICAgICAgXHRpY29uQ29sb3I6JydcclxuICAgICAgICAgXHR9XHJcblx0ICAgIH0sXHJcblx0XHRjcmVhdGVkKCl7XHJcblx0XHRcdHRoaXMubm90aWZfdHlwZSA9IHRoaXMubm90aWYudHlwZS5yZXBsYWNlKFwiQXBwXFxcXE5vdGlmaWNhdGlvbnNcXFxcXCIsXCJcIik7XHJcblx0XHRcdHZhciBjcGFuZWxVcmwgPSB3aW5kb3cuTGFyYXZlbC5jcGFuZWxfdXJsO1xyXG5cdFx0XHRzd2l0Y2godGhpcy5ub3RpZl90eXBlKVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0Y2FzZSBcIk5ld0NoYXRNZXNzYWdlXCI6XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0Y2FzZSBcIk5ld0RvY3VtZW50XCI6XHJcblx0XHRcdFx0XHR0aGlzLmxpbmsgPSBcImh0dHA6Ly9cIitjcGFuZWxVcmwrJy9kb2N1bWVudHMnO1xyXG5cdFx0XHRcdFx0dGhpcy5pY29uID0gXCJmYS11c2VyXCI7XHJcblx0XHRcdFx0XHR0aGlzLmljb25Db2xvciA9IFwidGMtZ3JlZW5cIjtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlIFwiTmV3RXdhbGxldFRyYW5zYWN0aW9uXCI6XHJcblx0XHRcdFx0XHR0aGlzLmxpbmsgPSBcImh0dHA6Ly9cIitjcGFuZWxVcmwrJy9ld2FsbGV0JztcclxuXHRcdFx0XHRcdHRoaXMuaWNvbiA9IFwiZmEtdXNlclwiO1xyXG5cdFx0XHRcdFx0dGhpcy5pY29uQ29sb3IgPSBcInRjLWdyZWVuXCI7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0Y2FzZSBcIk5ld0lzc3VlXCI6XHJcblx0XHRcdFx0XHR0aGlzLmxpbmsgPSBcImh0dHA6Ly9cIitjcGFuZWxVcmwrJy9jdXN0b21lci1zZXJ2aWNlJztcclxuXHRcdFx0XHRcdHRoaXMuaWNvbiA9IFwiZmEtdXNlclwiO1xyXG5cdFx0XHRcdFx0dGhpcy5pY29uQ29sb3IgPSBcInRjLWdyZWVuXCI7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0Y2FzZSBcIkNoYW5nZUV3YWxsZXRUcmFuc2FjdGlvblN0YXR1c1wiOlxyXG5cdFx0XHRcdFx0dGhpcy5saW5rID0gJ3VzZXIvZXdhbGxldCc7XHJcblx0XHRcdFx0XHR0aGlzLmljb24gPSBcImZhLXVzZXJcIjtcclxuXHRcdFx0XHRcdHRoaXMuaWNvbkNvbG9yID0gXCJ0Yy1ncmVlblwiO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgXCJOZXdQdXJjaGFzZVwiOlxyXG5cdFx0XHRcdFx0dGhpcy5saW5rID0gJy91c2VyL3NhbGVzLXJlcG9ydCc7XHJcblx0XHRcdFx0XHR0aGlzLmljb24gPSBcImZhLXVzZXJcIjtcclxuXHRcdFx0XHRcdHRoaXMuaWNvbkNvbG9yID0gXCJ0Yy1ncmVlblwiO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgXCJPcmRlclN0YXR1c1VwZGF0ZVwiOlxyXG5cdFx0XHRcdFx0dmFyIGxuayA9ICcnO1xyXG5cdFx0XHRcdFx0aWYoISF+dGhpcy5ub3RpZi5kYXRhLmJvZHkuc2VhcmNoKCdyZXF1ZXN0ZWQgdG8gcmV0dXJuIHlvdXIgaXRlbScpKXtcclxuXHQgICAgICAgICAgICAgICAgICAgIGxuayA9ICcvdXNlci9zYWxlcy1yZXBvcnQnO1xyXG5cdCAgICAgICAgICAgICAgICB9XHJcblx0ICAgICAgICAgICAgICAgIGVsc2UgaWYoISF+dGhpcy5ub3RpZi5kYXRhLmJvZHkuc2VhcmNoKCdzdWNjZXNzZnVsbHkgcmVjZWl2ZWQgdGhlIGl0ZW0nKSl7XHJcblx0ICAgICAgICAgICAgICAgICAgICBsbmsgPSAnL3VzZXIvc2FsZXMtcmVwb3J0JztcclxuXHQgICAgICAgICAgICAgICAgfVxyXG5cdCAgICAgICAgICAgICAgICBlbHNle1xyXG5cdCAgICAgICAgICAgICAgICBsbmsgPSAnL3VzZXIvcHVyY2hhc2UtcmVwb3J0JztcclxuXHQgICAgICAgICAgICAgICAgfVxyXG5cdCAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhsbmspO1xyXG5cdFx0XHRcdFx0dGhpcy5saW5rID0gbG5rO1xyXG5cdFx0XHRcdFx0dGhpcy5pY29uID0gXCJmYS11c2VyXCI7XHJcblx0XHRcdFx0XHR0aGlzLmljb25Db2xvciA9IFwidGMtZ3JlZW5cIjtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlIFwiRG9jdW1lbnRBcHByb3ZlZFwiOlxyXG5cdFx0XHRcdFx0dGhpcy5saW5rID0gJy9wcm9maWxlI2RvY3VtZW50cyc7XHJcblx0XHRcdFx0XHR0aGlzLmljb24gPSBcImZhLXVzZXJcIjtcclxuXHRcdFx0XHRcdHRoaXMuaWNvbkNvbG9yID0gXCJ0Yy1ncmVlblwiO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgXCJOZXdGcmllbmRSZXF1ZXN0XCI6XHJcblx0XHRcdFx0XHR0aGlzLmxpbmsgPSAgJy9mcmllbmRzJztcclxuXHRcdFx0XHRcdHRoaXMuaWNvbiA9IFwiZmEtdXNlclwiO1xyXG5cdFx0XHRcdFx0dGhpcy5pY29uQ29sb3IgPSBcInRjLWdyZWVuXCI7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0Y2FzZSBcIkZlZWRiYWNrQ29tbWVudFwiOlxyXG5cdFx0XHRcdFx0dGhpcy5saW5rID0gICcvZmVlZGJhY2sjJztcclxuXHRcdFx0XHRcdHRoaXMuaWNvbiA9IFwiZmEtdXNlclwiO1xyXG5cdFx0XHRcdFx0dGhpcy5pY29uQ29sb3IgPSBcInRjLWdyZWVuXCI7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0Y2FzZSBcIlByb2R1Y3RSZXZpZXdDb21tZW50XCI6XHJcblx0XHRcdFx0XHR0aGlzLmxpbmsgPSAgJy9wcm9kdWN0Lyc7XHJcblx0XHRcdFx0XHR0aGlzLmljb24gPSBcImZhLXVzZXJcIjtcclxuXHRcdFx0XHRcdHRoaXMuaWNvbkNvbG9yID0gXCJ0Yy1ncmVlblwiO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdH1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE5vdGlmaWNhdGlvbkNvbnRhaW5lci52dWU/NzUwNDdhMDgiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Ob3RpZmljYXRpb25Db250YWluZXIudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1jYWM4OTZkOFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Ob3RpZmljYXRpb25Db250YWluZXIudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXHByb2ZpbGVcXFxcTm90aWZpY2F0aW9uQ29udGFpbmVyLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIE5vdGlmaWNhdGlvbkNvbnRhaW5lci52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtY2FjODk2ZDhcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi1jYWM4OTZkOFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZmlsZS9Ob3RpZmljYXRpb25Db250YWluZXIudnVlXG4vLyBtb2R1bGUgaWQgPSAzNDlcbi8vIG1vZHVsZSBjaHVua3MgPSAxMiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1NlY3VyaXR5UXVlc3Rpb25BbnN3ZXIudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi01ZmVhYmFkNlxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9TZWN1cml0eVF1ZXN0aW9uQW5zd2VyLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxwcm9maWxlXFxcXFNlY3VyaXR5UXVlc3Rpb25BbnN3ZXIudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gU2VjdXJpdHlRdWVzdGlvbkFuc3dlci52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNWZlYWJhZDZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi01ZmVhYmFkNlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZmlsZS9TZWN1cml0eVF1ZXN0aW9uQW5zd2VyLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzdcbi8vIG1vZHVsZSBjaHVua3MgPSAxMiAxOSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiAoX3ZtLm5vdGlmX3R5cGUgIT0gXCJOZXdDaGF0TWVzc2FnZVwiKSA/IF9jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTIgXCIsXG4gICAgY2xhc3M6IF92bS5ub3RpZi5yZWFkX2F0ID8gXCJcIiA6IFwibm90LXJlYWRcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBfdm0ubGluayxcbiAgICAgIFwidGFyZ2V0XCI6IFwiX2JsYW5rXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImltYWdlXCJcbiAgfSwgW19jKCdpbWcnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5ub3RpZi5kYXRhLmltYWdlXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbnRlbnRcIlxuICB9LCBbX2MoJ3NwYW4nLCBbX2MoJ3N0cm9uZycsIFtfdm0uX3YoX3ZtLl9zKF92bS5ub3RpZi5kYXRhLnRpdGxlKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnInKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3NwYW4nLCBbX3ZtLl92KF92bS5fcyhfdm0ubm90aWYuZGF0YS5ib2R5KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2JyJyksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywgW19jKCdzbWFsbCcsIFtfdm0uX3YoX3ZtLl9zKF92bS5ub3RpZi5odW1hbl9jcmVhdGVkX2F0KSldKV0pXSldKV0pIDogX3ZtLl9lKClcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtY2FjODk2ZDhcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi1jYWM4OTZkOFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3Byb2ZpbGUvTm90aWZpY2F0aW9uQ29udGFpbmVyLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMTIiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMTIgc2VjdXJpdHktcXVlc3Rpb24tc2V0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLThcIlxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImFjY291bnQtbGFiZWwgcXVlc3Rpb24tbGFiZWxcIlxuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uaXRlbS5xdWVzdGlvbikpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tNFwiXG4gIH0sIFtfYygnbGFiZWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYWNjb3VudC1sYWJlbCBhbnN3ZXItbGFiZWxcIlxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZzIuZGF0YVsnYW5zLW9uLWZpbGUnXSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdwJyldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTVmZWFiYWQ2XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNWZlYWJhZDZcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9maWxlL1NlY3VyaXR5UXVlc3Rpb25BbnN3ZXIudnVlXG4vLyBtb2R1bGUgaWQgPSA0MVxuLy8gbW9kdWxlIGNodW5rcyA9IDEyIDE5Il0sInNvdXJjZVJvb3QiOiIifQ==