/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 434);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 184:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('tabs', __webpack_require__(342));
Vue.component('tab', __webpack_require__(344));
Vue.component('tab-item', __webpack_require__(343));

var app = new Vue({

	el: '#notifications-tab'

});

var app = new Vue({

	el: '#notifications-item',
	data: {
		items: [{
			id: 'OrderUpdates',
			content: [{
				src: 'shopping/img/general/notifications/notification.jpg',
				title: 'More Frees Shipping & COD!',
				content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
				date: '31-05-2017 18:33',
				href: ''
			}, {
				src: 'shopping/img/general/notifications/notification.jpg',
				title: 'More Frees Shipping & COD!',
				content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
				date: '31-05-2017 18:33',
				href: ''
			}]
		}, {
			id: 'Promotions',
			content: [{
				src: 'shopping/img/general/notifications/notification.jpg',
				title: 'More Frees Shipping & COD!',
				content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
				date: '31-05-2017 18:33',
				href: ''
			}]
		}, {
			id: 'Activity',
			content: [{
				src: 'shopping/img/general/notifications/notification.jpg',
				title: 'More Frees Shipping & COD!',
				content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
				date: '31-05-2017 18:33',
				href: ''
			}, {
				src: 'shopping/img/general/notifications/notification.jpg',
				title: 'More Frees Shipping & COD!',
				content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
				date: '31-05-2017 18:33',
				href: ''
			}, {
				src: 'shopping/img/general/notifications/notification.jpg',
				title: 'More Frees Shipping & COD!',
				content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
				date: '31-05-2017 18:33',
				href: ''
			}]
		}]
	}

});

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return { tabs: [] };
	},
	created: function created() {
		this.tabs = this.$children;
	},


	methods: {
		selectTab: function selectTab(selectedTab) {
			this.tabs.forEach(function (tab) {
				tab.isActive = tab.name == selectedTab.name;

				var cont = tab.name;
				cont = cont.split(' ').join('');

				if (tab.isActive == true) $('#' + cont).show();else $('#' + cont).hide();
			});
		}
	}
});

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['item']
});

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: {
		name: { required: true },
		selected: { default: false }
	},

	data: function data() {
		return {
			isActive: false
		};
	},
	mounted: function mounted() {
		this.active = this.selected;
	}
});

/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(277),
  /* template */
  __webpack_require__(403),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\pages\\Notifications.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Notifications.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d58fe9fe", Component.options)
  } else {
    hotAPI.reload("data-v-d58fe9fe", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(278),
  /* template */
  __webpack_require__(380),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\pages\\NotificationsContent.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] NotificationsContent.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3aa48c30", Component.options)
  } else {
    hotAPI.reload("data-v-3aa48c30", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(279),
  /* template */
  __webpack_require__(395),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\pages\\NotificationsTab.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] NotificationsTab.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-79fc9a84", Component.options)
  } else {
    hotAPI.reload("data-v-79fc9a84", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 380:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      "display": "none"
    },
    attrs: {
      "id": _vm.item.id
    }
  }, _vm._l((_vm.item.content), function(content) {
    return _c('div', {
      staticClass: "col-xs-12 col-sm-6 col-md-12"
    }, [_c('div', {
      staticClass: "col-md-2"
    }, [_c('img', {
      staticClass: "img-child",
      attrs: {
        "src": content.src
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "content-container col-md-8"
    }, [_c('p', {
      staticClass: "ttle"
    }, [_vm._v(_vm._s(content.title))]), _vm._v(" "), _c('p', {
      staticClass: "content"
    }, [_vm._v(_vm._s(content.content))]), _vm._v(" "), _c('p', {
      staticClass: "date"
    }, [_vm._v(_vm._s(content.date))])]), _vm._v(" "), _c('div', {
      staticClass: "button-container col-md-2"
    }, [_c('a', {
      staticClass: "view",
      attrs: {
        "href": content.href
      }
    }, [_vm._v("View Details")])]), _vm._v(" "), _c('br')])
  }))
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3aa48c30", module.exports)
  }
}

/***/ }),

/***/ 395:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isActive),
      expression: "isActive"
    }]
  }, [_vm._t("default")], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-79fc9a84", module.exports)
  }
}

/***/ }),

/***/ 403:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('ul', {
    staticClass: "child"
  }, _vm._l((_vm.tabs), function(tab) {
    return _c('li', {
      staticClass: "li-child",
      class: {
        'is-active': tab.isActive
      },
      on: {
        "click": function($event) {
          _vm.selectTab(tab)
        }
      }
    }, [_c('img', {
      staticClass: "img-child",
      attrs: {
        "src": "shopping/img/general/notifications/small-circle.png"
      }
    }), _vm._v(" "), _c('a', {
      staticClass: "a-child"
    }, [_vm._v(_vm._s(tab.name))])])
  })), _vm._v(" "), _c('div', {
    staticClass: "tabs-details"
  }, [_vm._t("default")], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-d58fe9fe", module.exports)
  }
}

/***/ }),

/***/ 434:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(184);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL25vdGlmaWNhdGlvbnMuanMiLCJ3ZWJwYWNrOi8vL05vdGlmaWNhdGlvbnMudnVlIiwid2VicGFjazovLy9Ob3RpZmljYXRpb25zQ29udGVudC52dWUiLCJ3ZWJwYWNrOi8vL05vdGlmaWNhdGlvbnNUYWIudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wYWdlcy9Ob3RpZmljYXRpb25zLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvTm90aWZpY2F0aW9uc0NvbnRlbnQudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wYWdlcy9Ob3RpZmljYXRpb25zVGFiLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvTm90aWZpY2F0aW9uc0NvbnRlbnQudnVlP2EzZjAiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3BhZ2VzL05vdGlmaWNhdGlvbnNUYWIudnVlP2JhN2MiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3BhZ2VzL05vdGlmaWNhdGlvbnMudnVlPzEyYTMiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImFwcCIsImVsIiwiZGF0YSIsIml0ZW1zIiwiaWQiLCJjb250ZW50Iiwic3JjIiwidGl0bGUiLCJkYXRlIiwiaHJlZiJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBQSxJQUFJQyxTQUFKLENBQWMsTUFBZCxFQUF1QixtQkFBQUMsQ0FBUSxHQUFSLENBQXZCO0FBQ0FGLElBQUlDLFNBQUosQ0FBYyxLQUFkLEVBQXNCLG1CQUFBQyxDQUFRLEdBQVIsQ0FBdEI7QUFDQUYsSUFBSUMsU0FBSixDQUFjLFVBQWQsRUFBMkIsbUJBQUFDLENBQVEsR0FBUixDQUEzQjs7QUFHQSxJQUFJQyxNQUFNLElBQUlILEdBQUosQ0FBUTs7QUFFakJJLEtBQUk7O0FBRmEsQ0FBUixDQUFWOztBQU9BLElBQUlELE1BQU0sSUFBSUgsR0FBSixDQUFROztBQUVqQkksS0FBSSxxQkFGYTtBQUdqQkMsT0FBTTtBQUNMQyxTQUFNLENBQ0w7QUFDQ0MsT0FBSSxjQURMO0FBRUlDLFlBQVMsQ0FDUjtBQUNDQyxTQUFLLHFEQUROO0FBRURDLFdBQU8sNEJBRk47QUFHREYsYUFBUyxnU0FIUjtBQUlERyxVQUFNLGtCQUpMO0FBS0RDLFVBQU07QUFMTCxJQURRLEVBUVI7QUFDRUgsU0FBSyxxREFEUDtBQUVEQyxXQUFPLDRCQUZOO0FBR0RGLGFBQVMsZ1NBSFI7QUFJREcsVUFBTSxrQkFKTDtBQUtEQyxVQUFNO0FBTEwsSUFSUTtBQUZiLEdBREssRUFvQkw7QUFDQ0wsT0FBSSxZQURMO0FBRUlDLFlBQVMsQ0FDUjtBQUNFQyxTQUFLLHFEQURQO0FBRURDLFdBQU8sNEJBRk47QUFHREYsYUFBUyxnU0FIUjtBQUlERyxVQUFNLGtCQUpMO0FBS0RDLFVBQU07QUFMTCxJQURRO0FBRmIsR0FwQkssRUFnQ0w7QUFDQ0wsT0FBSSxVQURMO0FBRUlDLFlBQVMsQ0FDUjtBQUNFQyxTQUFLLHFEQURQO0FBRURDLFdBQU8sNEJBRk47QUFHREYsYUFBUyxnU0FIUjtBQUlERyxVQUFNLGtCQUpMO0FBS0RDLFVBQU07QUFMTCxJQURRLEVBUVI7QUFDRUgsU0FBSyxxREFEUDtBQUVEQyxXQUFPLDRCQUZOO0FBR0RGLGFBQVMsZ1NBSFI7QUFJREcsVUFBTSxrQkFKTDtBQUtEQyxVQUFNO0FBTEwsSUFSUSxFQWVSO0FBQ0VILFNBQUsscURBRFA7QUFFREMsV0FBTyw0QkFGTjtBQUdERixhQUFTLGdTQUhSO0FBSURHLFVBQU0sa0JBSkw7QUFLREMsVUFBTTtBQUxMLElBZlE7QUFGYixHQWhDSztBQUREOztBQUhXLENBQVIsQ0FBVixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNHQTt1QkFFQTtpQkFDQTtBQUVBOzZCQUNBO21CQUNBO0FBRUE7Ozs7NkNBRUE7b0NBQ0E7MkNBRUE7O21CQUNBO2dDQUVBOzt3QkFDQSxvQkFFQSwwQkFDQTtBQUNBO0FBR0E7QUFmQTtBQVRBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSUE7U0FFQTtBQURBLEc7Ozs7Ozs7Ozs7Ozs7O0FDaEJBOztvQkFHQTt1QkFHQTtBQUpBOzt1QkFLQTs7YUFHQTtBQUZBO0FBSUE7NkJBQ0E7cUJBQ0E7QUFDQTtBQWRBLEc7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQStHO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQStHO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQStHO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUMxQ0EsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDaEJBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9wYWdlcy9ub3RpZmljYXRpb25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA0MzQpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGY1ZmNmZjBlMWYwODAwOTdjZjQ2IiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMTggMTkgMjAgMjEgMjIgMjMgMjQgMjUgMjYgMjciLCJWdWUuY29tcG9uZW50KCd0YWJzJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvcGFnZXMvTm90aWZpY2F0aW9ucy52dWUnKSk7XHJcblZ1ZS5jb21wb25lbnQoJ3RhYicsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL3BhZ2VzL05vdGlmaWNhdGlvbnNUYWIudnVlJykpO1xyXG5WdWUuY29tcG9uZW50KCd0YWItaXRlbScsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL3BhZ2VzL05vdGlmaWNhdGlvbnNDb250ZW50LnZ1ZScpKTtcclxuXHJcblxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcblxyXG5cdGVsOiAnI25vdGlmaWNhdGlvbnMtdGFiJ1xyXG5cclxufSk7XHJcblxyXG5cclxudmFyIGFwcCA9IG5ldyBWdWUoe1xyXG5cclxuXHRlbDogJyNub3RpZmljYXRpb25zLWl0ZW0nLFxyXG5cdGRhdGE6IHtcclxuXHRcdGl0ZW1zOltcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGlkOiAnT3JkZXJVcGRhdGVzJyxcclxuXHRcdFx0ICAgIGNvbnRlbnQ6IFtcclxuXHRcdFx0XHQgICAge1xyXG5cdFx0XHRcdCAgICBcdHNyYzogJ3Nob3BwaW5nL2ltZy9nZW5lcmFsL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9uLmpwZycsXHJcblx0XHRcdFx0XHQgXHR0aXRsZTogJ01vcmUgRnJlZXMgU2hpcHBpbmcgJiBDT0QhJyxcclxuXHRcdFx0XHRcdCBcdGNvbnRlbnQ6ICdMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgaWxsdW0gZmFjaWxpc2kgZWkgdmlzLCB1dCByZXF1ZSBzY3JpcHRvcmVtIG5lYy4gU2l0IGRpc2NlcmUgYW5jaWxsYWUgYWQsIHBybyBxdW90IHBlcnNpdXMgbm8sIGluIGNldGVybyBkZWxpY2F0aXNzaW1pIHNlZC4gVmlkaXQgdmVuaWFtIGhpcyB1dCwgdmltIHRlIHZvY2lidXMgcGF0cmlvcXVlLiBObyBhdHF1aSBkaWNhbnQgZGljdGFzIGR1by4gSGFzIHBlcnNpdXMgb2ZmZW5kaXQgc2FkaXBzY2luZyBlaSwgdGUgYW1ldCBlZmZpY2lhbnR1ciB2aW0uJyxcclxuXHRcdFx0XHRcdCBcdGRhdGU6ICczMS0wNS0yMDE3IDE4OjMzJyxcclxuXHRcdFx0XHRcdCBcdGhyZWY6ICcnXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdCAgICB7XHJcblx0XHRcdFx0ICAgICBcdHNyYzogJ3Nob3BwaW5nL2ltZy9nZW5lcmFsL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9uLmpwZycsXHJcblx0XHRcdFx0XHQgXHR0aXRsZTogJ01vcmUgRnJlZXMgU2hpcHBpbmcgJiBDT0QhJyxcclxuXHRcdFx0XHRcdCBcdGNvbnRlbnQ6ICdMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgaWxsdW0gZmFjaWxpc2kgZWkgdmlzLCB1dCByZXF1ZSBzY3JpcHRvcmVtIG5lYy4gU2l0IGRpc2NlcmUgYW5jaWxsYWUgYWQsIHBybyBxdW90IHBlcnNpdXMgbm8sIGluIGNldGVybyBkZWxpY2F0aXNzaW1pIHNlZC4gVmlkaXQgdmVuaWFtIGhpcyB1dCwgdmltIHRlIHZvY2lidXMgcGF0cmlvcXVlLiBObyBhdHF1aSBkaWNhbnQgZGljdGFzIGR1by4gSGFzIHBlcnNpdXMgb2ZmZW5kaXQgc2FkaXBzY2luZyBlaSwgdGUgYW1ldCBlZmZpY2lhbnR1ciB2aW0uJyxcclxuXHRcdFx0XHRcdCBcdGRhdGU6ICczMS0wNS0yMDE3IDE4OjMzJyxcclxuXHRcdFx0XHRcdCBcdGhyZWY6ICcnXHJcblx0XHRcdFx0XHR9XHRcdFx0XHRcdCBcclxuXHRcdFx0XHRdIFx0XHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHRpZDogJ1Byb21vdGlvbnMnLFxyXG5cdFx0XHQgICAgY29udGVudDogW1xyXG5cdFx0XHRcdCAgICB7XHJcblx0XHRcdFx0ICAgICBcdHNyYzogJ3Nob3BwaW5nL2ltZy9nZW5lcmFsL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9uLmpwZycsXHJcblx0XHRcdFx0XHQgXHR0aXRsZTogJ01vcmUgRnJlZXMgU2hpcHBpbmcgJiBDT0QhJyxcclxuXHRcdFx0XHRcdCBcdGNvbnRlbnQ6ICdMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgaWxsdW0gZmFjaWxpc2kgZWkgdmlzLCB1dCByZXF1ZSBzY3JpcHRvcmVtIG5lYy4gU2l0IGRpc2NlcmUgYW5jaWxsYWUgYWQsIHBybyBxdW90IHBlcnNpdXMgbm8sIGluIGNldGVybyBkZWxpY2F0aXNzaW1pIHNlZC4gVmlkaXQgdmVuaWFtIGhpcyB1dCwgdmltIHRlIHZvY2lidXMgcGF0cmlvcXVlLiBObyBhdHF1aSBkaWNhbnQgZGljdGFzIGR1by4gSGFzIHBlcnNpdXMgb2ZmZW5kaXQgc2FkaXBzY2luZyBlaSwgdGUgYW1ldCBlZmZpY2lhbnR1ciB2aW0uJyxcclxuXHRcdFx0XHRcdCBcdGRhdGU6ICczMS0wNS0yMDE3IDE4OjMzJyxcclxuXHRcdFx0XHRcdCBcdGhyZWY6ICcnXHJcblx0XHRcdFx0XHR9XHRcdFx0XHQgXHJcblx0XHRcdFx0XSBcdFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0aWQ6ICdBY3Rpdml0eScsXHJcblx0XHRcdCAgICBjb250ZW50OiBbXHJcblx0XHRcdFx0ICAgIHtcclxuXHRcdFx0XHQgICAgIFx0c3JjOiAnc2hvcHBpbmcvaW1nL2dlbmVyYWwvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb24uanBnJyxcclxuXHRcdFx0XHRcdCBcdHRpdGxlOiAnTW9yZSBGcmVlcyBTaGlwcGluZyAmIENPRCEnLFxyXG5cdFx0XHRcdFx0IFx0Y29udGVudDogJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBpbGx1bSBmYWNpbGlzaSBlaSB2aXMsIHV0IHJlcXVlIHNjcmlwdG9yZW0gbmVjLiBTaXQgZGlzY2VyZSBhbmNpbGxhZSBhZCwgcHJvIHF1b3QgcGVyc2l1cyBubywgaW4gY2V0ZXJvIGRlbGljYXRpc3NpbWkgc2VkLiBWaWRpdCB2ZW5pYW0gaGlzIHV0LCB2aW0gdGUgdm9jaWJ1cyBwYXRyaW9xdWUuIE5vIGF0cXVpIGRpY2FudCBkaWN0YXMgZHVvLiBIYXMgcGVyc2l1cyBvZmZlbmRpdCBzYWRpcHNjaW5nIGVpLCB0ZSBhbWV0IGVmZmljaWFudHVyIHZpbS4nLFxyXG5cdFx0XHRcdFx0IFx0ZGF0ZTogJzMxLTA1LTIwMTcgMTg6MzMnLFxyXG5cdFx0XHRcdFx0IFx0aHJlZjogJydcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0ICAgIHtcclxuXHRcdFx0XHQgICAgIFx0c3JjOiAnc2hvcHBpbmcvaW1nL2dlbmVyYWwvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb24uanBnJyxcclxuXHRcdFx0XHRcdCBcdHRpdGxlOiAnTW9yZSBGcmVlcyBTaGlwcGluZyAmIENPRCEnLFxyXG5cdFx0XHRcdFx0IFx0Y29udGVudDogJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBpbGx1bSBmYWNpbGlzaSBlaSB2aXMsIHV0IHJlcXVlIHNjcmlwdG9yZW0gbmVjLiBTaXQgZGlzY2VyZSBhbmNpbGxhZSBhZCwgcHJvIHF1b3QgcGVyc2l1cyBubywgaW4gY2V0ZXJvIGRlbGljYXRpc3NpbWkgc2VkLiBWaWRpdCB2ZW5pYW0gaGlzIHV0LCB2aW0gdGUgdm9jaWJ1cyBwYXRyaW9xdWUuIE5vIGF0cXVpIGRpY2FudCBkaWN0YXMgZHVvLiBIYXMgcGVyc2l1cyBvZmZlbmRpdCBzYWRpcHNjaW5nIGVpLCB0ZSBhbWV0IGVmZmljaWFudHVyIHZpbS4nLFxyXG5cdFx0XHRcdFx0IFx0ZGF0ZTogJzMxLTA1LTIwMTcgMTg6MzMnLFxyXG5cdFx0XHRcdFx0IFx0aHJlZjogJydcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0ICAgIHtcclxuXHRcdFx0XHQgICAgIFx0c3JjOiAnc2hvcHBpbmcvaW1nL2dlbmVyYWwvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb24uanBnJyxcclxuXHRcdFx0XHRcdCBcdHRpdGxlOiAnTW9yZSBGcmVlcyBTaGlwcGluZyAmIENPRCEnLFxyXG5cdFx0XHRcdFx0IFx0Y29udGVudDogJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBpbGx1bSBmYWNpbGlzaSBlaSB2aXMsIHV0IHJlcXVlIHNjcmlwdG9yZW0gbmVjLiBTaXQgZGlzY2VyZSBhbmNpbGxhZSBhZCwgcHJvIHF1b3QgcGVyc2l1cyBubywgaW4gY2V0ZXJvIGRlbGljYXRpc3NpbWkgc2VkLiBWaWRpdCB2ZW5pYW0gaGlzIHV0LCB2aW0gdGUgdm9jaWJ1cyBwYXRyaW9xdWUuIE5vIGF0cXVpIGRpY2FudCBkaWN0YXMgZHVvLiBIYXMgcGVyc2l1cyBvZmZlbmRpdCBzYWRpcHNjaW5nIGVpLCB0ZSBhbWV0IGVmZmljaWFudHVyIHZpbS4nLFxyXG5cdFx0XHRcdFx0IFx0ZGF0ZTogJzMxLTA1LTIwMTcgMTg6MzMnLFxyXG5cdFx0XHRcdFx0IFx0aHJlZjogJydcclxuXHRcdFx0XHRcdH1cdFx0XHRcdFx0IFxyXG5cdFx0XHRcdF0gXHRcclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH1cclxuXHJcbn0pO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvbm90aWZpY2F0aW9ucy5qcyIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2PlxyXG5cdFx0PHVsIGNsYXNzPVwiY2hpbGRcIj5cclxuICAgIFx0XHQ8bGkgdi1mb3I9XCJ0YWIgaW4gdGFic1wiIEBjbGljaz1cInNlbGVjdFRhYih0YWIpXCIgOmNsYXNzPVwieydpcy1hY3RpdmUnOnRhYi5pc0FjdGl2ZX1cIiBjbGFzcz1cImxpLWNoaWxkXCI+PGltZyBjbGFzcz1cImltZy1jaGlsZFwiIHNyYz1cInNob3BwaW5nL2ltZy9nZW5lcmFsL25vdGlmaWNhdGlvbnMvc21hbGwtY2lyY2xlLnBuZ1wiIC8+XHJcbiAgICBcdFx0XHQ8YSBjbGFzcz1cImEtY2hpbGRcIj57eyB0YWIubmFtZSB9fTwvYT5cclxuICAgIFx0XHQ8L2xpPlxyXG4gICAgXHQ8L3VsPlxyXG5cclxuXHRcdDxkaXYgY2xhc3M9XCJ0YWJzLWRldGFpbHNcIj5cclxuXHRcdFx0PHNsb3Q+PC9zbG90PlxyXG5cdFx0PC9kaXY+XHJcblx0PC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcblx0ZGF0YSgpIHtcclxuXHRcdHJldHVybiB7IHRhYnM6IFtdIH07XHJcblx0fSxcclxuXHJcblx0Y3JlYXRlZCgpIHtcclxuXHRcdHRoaXMudGFicyA9IHRoaXMuJGNoaWxkcmVuO1xyXG5cdH0sXHJcblxyXG5cdG1ldGhvZHM6IHtcclxuXHRcdHNlbGVjdFRhYihzZWxlY3RlZFRhYikge1xyXG5cdFx0XHR0aGlzLnRhYnMuZm9yRWFjaCh0YWIgPT4geyBcclxuXHRcdFx0XHR0YWIuaXNBY3RpdmUgPSAodGFiLm5hbWUgPT0gc2VsZWN0ZWRUYWIubmFtZSlcclxuXHJcblx0XHRcdFx0dmFyIGNvbnQgPSB0YWIubmFtZTtcclxuXHRcdFx0XHRjb250ID0gY29udC5zcGxpdCgnICcpLmpvaW4oJycpO1xyXG5cclxuXHRcdFx0XHRpZih0YWIuaXNBY3RpdmU9PXRydWUpXHJcblx0XHRcdFx0ICQoJyMnK2NvbnQpLnNob3coKTtcclxuXHRcdFx0XHRlbHNlXHJcblx0XHRcdFx0ICQoJyMnK2NvbnQpLmhpZGUoKTtcdFxyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHJcblx0fVxyXG59XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBOb3RpZmljYXRpb25zLnZ1ZT9kNzNhZTI1NiIsIjx0ZW1wbGF0ZT5cclxuPGRpdiA6aWQ9XCJpdGVtLmlkXCIgc3R5bGU9XCJkaXNwbGF5Om5vbmVcIj5cclxuICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyIGNvbC1zbS02IGNvbC1tZC0xMlwiIHYtZm9yPVwiY29udGVudCBpbiBpdGVtLmNvbnRlbnRcIj5cclxuICA8ZGl2IGNsYXNzPVwiY29sLW1kLTJcIj5cclxuXHQ8aW1nIGNsYXNzPVwiaW1nLWNoaWxkXCIgOnNyYz1cImNvbnRlbnQuc3JjXCIgLz5cclxuICA8L2Rpdj5cclxuICA8ZGl2IGNsYXNzPVwiY29udGVudC1jb250YWluZXIgY29sLW1kLThcIj5cclxuICBcdDxwIGNsYXNzPVwidHRsZVwiPnt7Y29udGVudC50aXRsZX19PC9wPlxyXG4gIFx0PHAgY2xhc3M9XCJjb250ZW50XCI+e3tjb250ZW50LmNvbnRlbnR9fTwvcD5cclxuICBcdDxwIGNsYXNzPVwiZGF0ZVwiPnt7Y29udGVudC5kYXRlfX08L3A+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cImJ1dHRvbi1jb250YWluZXIgY29sLW1kLTJcIj5cclxuXHQ8YSA6aHJlZj1cImNvbnRlbnQuaHJlZlwiIGNsYXNzPVwidmlld1wiPlZpZXcgRGV0YWlsczwvYT5cclxuICA8L2Rpdj5cclxuICA8YnIvPlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRwcm9wczogWydpdGVtJ11cclxufVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gTm90aWZpY2F0aW9uc0NvbnRlbnQudnVlP2Q1ZjAxOGE0IiwiPHRlbXBsYXRlPlxyXG5cdDxkaXYgdi1zaG93PVwiaXNBY3RpdmVcIj48c2xvdD48L3Nsb3Q+PC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcblx0cHJvcHM6IHtcclxuXHRcdG5hbWU6IHsgcmVxdWlyZWQgOiB0cnVlfSxcclxuXHRcdHNlbGVjdGVkOiB7IGRlZmF1bHQgOiBmYWxzZX1cclxuXHR9LFxyXG5cclxuXHRkYXRhKCkge1xyXG5cdFx0cmV0dXJuIHtcclxuXHRcdFx0aXNBY3RpdmU6IGZhbHNlXHJcblx0XHR9O1xyXG5cdH0sXHJcblxyXG5cdG1vdW50ZWQoKSB7XHJcblx0XHR0aGlzLmFjdGl2ZSA9IHRoaXMuc2VsZWN0ZWQ7XHJcblx0fVxyXG59XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBOb3RpZmljYXRpb25zVGFiLnZ1ZT9hZmNhYmIwNCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL05vdGlmaWNhdGlvbnMudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1kNThmZTlmZVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Ob3RpZmljYXRpb25zLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxwYWdlc1xcXFxOb3RpZmljYXRpb25zLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIE5vdGlmaWNhdGlvbnMudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWQ1OGZlOWZlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtZDU4ZmU5ZmVcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3BhZ2VzL05vdGlmaWNhdGlvbnMudnVlXG4vLyBtb2R1bGUgaWQgPSAzNDJcbi8vIG1vZHVsZSBjaHVua3MgPSA5IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vTm90aWZpY2F0aW9uc0NvbnRlbnQudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zYWE0OGMzMFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Ob3RpZmljYXRpb25zQ29udGVudC52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxccGFnZXNcXFxcTm90aWZpY2F0aW9uc0NvbnRlbnQudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gTm90aWZpY2F0aW9uc0NvbnRlbnQudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTNhYTQ4YzMwXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtM2FhNDhjMzBcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3BhZ2VzL05vdGlmaWNhdGlvbnNDb250ZW50LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzQzXG4vLyBtb2R1bGUgY2h1bmtzID0gOSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL05vdGlmaWNhdGlvbnNUYWIudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi03OWZjOWE4NFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Ob3RpZmljYXRpb25zVGFiLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxwYWdlc1xcXFxOb3RpZmljYXRpb25zVGFiLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIE5vdGlmaWNhdGlvbnNUYWIudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTc5ZmM5YTg0XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtNzlmYzlhODRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3BhZ2VzL05vdGlmaWNhdGlvbnNUYWIudnVlXG4vLyBtb2R1bGUgaWQgPSAzNDRcbi8vIG1vZHVsZSBjaHVua3MgPSA5IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljU3R5bGU6IHtcbiAgICAgIFwiZGlzcGxheVwiOiBcIm5vbmVcIlxuICAgIH0sXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogX3ZtLml0ZW0uaWRcbiAgICB9XG4gIH0sIF92bS5fbCgoX3ZtLml0ZW0uY29udGVudCksIGZ1bmN0aW9uKGNvbnRlbnQpIHtcbiAgICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMiBjb2wtc20tNiBjb2wtbWQtMTJcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTJcIlxuICAgIH0sIFtfYygnaW1nJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLWNoaWxkXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInNyY1wiOiBjb250ZW50LnNyY1xuICAgICAgfVxuICAgIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb250ZW50LWNvbnRhaW5lciBjb2wtbWQtOFwiXG4gICAgfSwgW19jKCdwJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwidHRsZVwiXG4gICAgfSwgW192bS5fdihfdm0uX3MoY29udGVudC50aXRsZSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdwJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29udGVudFwiXG4gICAgfSwgW192bS5fdihfdm0uX3MoY29udGVudC5jb250ZW50KSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3AnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJkYXRlXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhjb250ZW50LmRhdGUpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJidXR0b24tY29udGFpbmVyIGNvbC1tZC0yXCJcbiAgICB9LCBbX2MoJ2EnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJ2aWV3XCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogY29udGVudC5ocmVmXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlZpZXcgRGV0YWlsc1wiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdicicpXSlcbiAgfSkpXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTNhYTQ4YzMwXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtM2FhNDhjMzBcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wYWdlcy9Ob3RpZmljYXRpb25zQ29udGVudC52dWVcbi8vIG1vZHVsZSBpZCA9IDM4MFxuLy8gbW9kdWxlIGNodW5rcyA9IDkiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgdmFsdWU6IChfdm0uaXNBY3RpdmUpLFxuICAgICAgZXhwcmVzc2lvbjogXCJpc0FjdGl2ZVwiXG4gICAgfV1cbiAgfSwgW192bS5fdChcImRlZmF1bHRcIildLCAyKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi03OWZjOWE4NFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTc5ZmM5YTg0XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvTm90aWZpY2F0aW9uc1RhYi52dWVcbi8vIG1vZHVsZSBpZCA9IDM5NVxuLy8gbW9kdWxlIGNodW5rcyA9IDkiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2hpbGRcIlxuICB9LCBfdm0uX2woKF92bS50YWJzKSwgZnVuY3Rpb24odGFiKSB7XG4gICAgcmV0dXJuIF9jKCdsaScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImxpLWNoaWxkXCIsXG4gICAgICBjbGFzczoge1xuICAgICAgICAnaXMtYWN0aXZlJzogdGFiLmlzQWN0aXZlXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBfdm0uc2VsZWN0VGFiKHRhYilcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIFtfYygnaW1nJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLWNoaWxkXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInNyY1wiOiBcInNob3BwaW5nL2ltZy9nZW5lcmFsL25vdGlmaWNhdGlvbnMvc21hbGwtY2lyY2xlLnBuZ1wiXG4gICAgICB9XG4gICAgfSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiYS1jaGlsZFwiXG4gICAgfSwgW192bS5fdihfdm0uX3ModGFiLm5hbWUpKV0pXSlcbiAgfSkpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhYnMtZGV0YWlsc1wiXG4gIH0sIFtfdm0uX3QoXCJkZWZhdWx0XCIpXSwgMildKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1kNThmZTlmZVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWQ1OGZlOWZlXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvTm90aWZpY2F0aW9ucy52dWVcbi8vIG1vZHVsZSBpZCA9IDQwM1xuLy8gbW9kdWxlIGNodW5rcyA9IDkiXSwic291cmNlUm9vdCI6IiJ9