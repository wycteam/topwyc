/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 428);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal md-modal fade",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "modal-label"
    }
  }, [_c('div', {
    class: 'modal-dialog ' + _vm.size,
    attrs: {
      "role": "document"
    }
  }, [_c('div', {
    staticClass: "modal-content"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-653e7369", module.exports)
  }
}

/***/ }),

/***/ 178:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('favorites-list', __webpack_require__(341));
Vue.component('fav-modal', __webpack_require__(9));

Vue.filter('currency', function (value) {
	return '₱' + numberWithCommas(parseFloat(value).toFixed(2));
});

var app = new Vue({

	el: '#favorites-content',
	data: {
		items: [],
		no_item: '',
		id: '',
		index: '',
		lang: []
	},
	created: function created() {
		var _this = this;

		function getTranslation() {
			return axios.get('/translate/favorites');
		}

		function getFavorites() {
			return axiosAPIv1.get('/favorites');
		}

		axios.all([getTranslation(), getFavorites()]).then(axios.spread(function (translation, favorites) {

			_this.lang = translation.data.data;

			if (favorites.data.data != 0) _this.items = favorites.status == 200 ? favorites.data : [];else $('.no-item').show(); //this.no_item = "There are no items in your wishlist";
		})).catch($.noop);
	},

	methods: {
		showRemoveDialog: function showRemoveDialog(id, index) {
			this.id = id;
			this.index = index;
			$('#dialogRemove').modal('toggle');
		},
		deleteFavorites: function deleteFavorites() {
			var _this2 = this;

			axiosAPIv1.get('/deletefavorites/' + this.id).then(function (result) {
				$("#fav-count").text(result.data);
				toastr.success(_this2.lang['removed'], _this2.items.data[_this2.index].name);
				_this2.items.data.splice(_this2.index, 1);
				if (_this2.items.data.length == 0) $('.no-item').show();
			});
		},
		addtocart: function addtocart(id, index) {
			var _this3 = this;

			axios.get(location.origin + '/cart/add', {
				params: {
					prod_id: $('#prod_id' + id).val(),
					qty: $('#qty' + id).val()
				}
			}).then(function (result) {
				$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
				if (result.data.error_msg) {
					if (result.data.error_msg == 'Redirect') window.location = '/product/' + result.data.slug;else toastr.error(result.data.error_msg, _this3.lang['not-added']);
				} else {
					toastr.success(_this3.lang['added'], _this3.items.data[index].name);
					_this3.items.data.splice(index, 1);
					if (_this3.items.data.length == 0) $('.no-item').show();
					axiosAPIv1.get('/deletefavorites/' + id).then(function (result) {
						$("#fav-count").text(result.data);
					});
				}
			});
		}
	}

});

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['items'],
	methods: {
		showRemoveDialog: function showRemoveDialog(id, index) {
			this.$parent.showRemoveDialog(id, index);
		},
		addtocart: function addtocart(id, index) {
			this.$parent.addtocart(id, index);
		}
	}
});

/***/ }),

/***/ 341:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(276),
  /* template */
  __webpack_require__(399),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\pages\\Favorites.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Favorites.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b3770d20", Component.options)
  } else {
    hotAPI.reload("data-v-b3770d20", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 399:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "table-responsive"
  }, [_c('table', {
    staticClass: "table cart-table wishlist-table text-center"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.items.data), function(item, index) {
    return _c('tr', {
      key: item.id,
      attrs: {
        "id": 'div' + item.id
      }
    }, [_c('td', [_c('a', {
      staticClass: "cart-pro-image",
      attrs: {
        "href": '/product/' + item.slug
      }
    }, [_c('img', {
      attrs: {
        "src": item.image,
        "alt": ""
      }
    })])]), _vm._v(" "), _c('td', [_c('a', {
      staticClass: "cart-pro-title",
      attrs: {
        "href": '/product/' + item.slug
      }
    }, [_vm._v(_vm._s(item.name))])]), _vm._v(" "), _c('td', [_c('p', {
      staticClass: "stock in-stock"
    }, [_vm._v(_vm._s(item.stock))])]), _vm._v(" "), _c('td', [_c('p', {
      staticClass: "cart-pro-price"
    }, [_vm._v(_vm._s(_vm._f("currency")(item.price)))])]), _vm._v(" "), _c('td', [_c('a', {
      staticClass: "btn btn-raised btn-primary add-cart-btn",
      attrs: {
        "href": "javascript:void(0)"
      },
      on: {
        "click": function($event) {
          _vm.addtocart(item.id, index)
        }
      }
    }, [_vm._v("ADD TO CART"), _c('div', {
      staticClass: "ripple-container"
    })])]), _vm._v(" "), _c('td', [_c('a', {
      attrs: {
        "id": 'btn' + item.id,
        "href": "javascript:void(0)"
      },
      on: {
        "click": function($event) {
          _vm.showRemoveDialog(item.id, index)
        }
      }
    }, [_c('i', {
      staticClass: "fa fa-trash-o cart-pro-remove",
      attrs: {
        "aria-hidden": "true"
      }
    })])]), _vm._v(" "), _c('input', {
      attrs: {
        "id": 'prod_id' + item.id,
        "name": 'prod_id' + item.id,
        "type": "hidden"
      },
      domProps: {
        "value": item.product_id
      }
    }), _vm._v(" "), _c('input', {
      attrs: {
        "id": 'qty' + item.id,
        "name": 'qty' + item.id,
        "type": "hidden",
        "value": "1"
      }
    })])
  }))])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', {
    staticClass: "image"
  }), _vm._v(" "), _c('th', {
    staticClass: "name"
  }, [_vm._v("PRODUCT NAME")]), _vm._v(" "), _c('th', {
    staticClass: "stock"
  }, [_vm._v("STOCK STATUS")]), _vm._v(" "), _c('th', {
    staticClass: "price"
  }, [_vm._v("PRICE")]), _vm._v(" "), _c('th', {
    staticClass: "add-cart"
  }), _vm._v(" "), _c('th', {
    staticClass: "remove"
  })])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-b3770d20", module.exports)
  }
}

/***/ }),

/***/ 428:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(178);


/***/ }),

/***/ 7:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        'id': { required: true },
        'size': { default: 'modal-sm' }
    }
});

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(7),
  /* template */
  __webpack_require__(12),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\DialogModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] DialogModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-653e7369", Component.options)
  } else {
    hotAPI.reload("data-v-653e7369", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL0RpYWxvZ01vZGFsLnZ1ZT8wNDMyKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL2Zhdm9yaXRlcy5qcyIsIndlYnBhY2s6Ly8vRmF2b3JpdGVzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvRmF2b3JpdGVzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvRmF2b3JpdGVzLnZ1ZT9jN2NjIiwid2VicGFjazovLy9EaWFsb2dNb2RhbC52dWU/MjZkMSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlPzMzY2EiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImZpbHRlciIsInZhbHVlIiwibnVtYmVyV2l0aENvbW1hcyIsInBhcnNlRmxvYXQiLCJ0b0ZpeGVkIiwiYXBwIiwiZWwiLCJkYXRhIiwiaXRlbXMiLCJub19pdGVtIiwiaWQiLCJpbmRleCIsImxhbmciLCJjcmVhdGVkIiwiZ2V0VHJhbnNsYXRpb24iLCJheGlvcyIsImdldCIsImdldEZhdm9yaXRlcyIsImF4aW9zQVBJdjEiLCJhbGwiLCJ0aGVuIiwic3ByZWFkIiwidHJhbnNsYXRpb24iLCJmYXZvcml0ZXMiLCJzdGF0dXMiLCIkIiwic2hvdyIsImNhdGNoIiwibm9vcCIsIm1ldGhvZHMiLCJzaG93UmVtb3ZlRGlhbG9nIiwibW9kYWwiLCJkZWxldGVGYXZvcml0ZXMiLCJ0ZXh0IiwicmVzdWx0IiwidG9hc3RyIiwic3VjY2VzcyIsIm5hbWUiLCJzcGxpY2UiLCJsZW5ndGgiLCJhZGR0b2NhcnQiLCJsb2NhdGlvbiIsIm9yaWdpbiIsInBhcmFtcyIsInByb2RfaWQiLCJ2YWwiLCJxdHkiLCJjYXJ0X2l0ZW1fY291bnQiLCJlcnJvcl9tc2ciLCJ3aW5kb3ciLCJzbHVnIiwiZXJyb3IiLCJ4IiwidG9TdHJpbmciLCJyZXBsYWNlIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNsREEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7QUN6Q0FBLElBQUlDLFNBQUosQ0FBYyxnQkFBZCxFQUFpQyxtQkFBQUMsQ0FBUSxHQUFSLENBQWpDO0FBQ0FGLElBQUlDLFNBQUosQ0FBYyxXQUFkLEVBQTRCLG1CQUFBQyxDQUFRLENBQVIsQ0FBNUI7O0FBRUFGLElBQUlHLE1BQUosQ0FBVyxVQUFYLEVBQXVCLFVBQVVDLEtBQVYsRUFBaUI7QUFDcEMsUUFBUSxNQUFJQyxpQkFBaUJDLFdBQVdGLEtBQVgsRUFBa0JHLE9BQWxCLENBQTBCLENBQTFCLENBQWpCLENBQVo7QUFDSCxDQUZEOztBQUlBLElBQUlDLE1BQU0sSUFBSVIsR0FBSixDQUFROztBQUVqQlMsS0FBSSxvQkFGYTtBQUdqQkMsT0FBTTtBQUNMQyxTQUFNLEVBREQ7QUFFTEMsV0FBUyxFQUZKO0FBR0xDLE1BQUcsRUFIRTtBQUlMQyxTQUFNLEVBSkQ7QUFLTEMsUUFBSztBQUxBLEVBSFc7QUFVakJDLFFBVmlCLHFCQVVSO0FBQUE7O0FBQ1IsV0FBU0MsY0FBVCxHQUEwQjtBQUNoQixVQUFPQyxNQUFNQyxHQUFOLENBQVUsc0JBQVYsQ0FBUDtBQUNIOztBQUVQLFdBQVNDLFlBQVQsR0FBd0I7QUFDdkIsVUFBT0MsV0FBV0YsR0FBWCxDQUFlLFlBQWYsQ0FBUDtBQUNBOztBQUVERCxRQUFNSSxHQUFOLENBQVUsQ0FDVEwsZ0JBRFMsRUFFVEcsY0FGUyxDQUFWLEVBR0dHLElBSEgsQ0FHUUwsTUFBTU0sTUFBTixDQUNQLFVBQ0NDLFdBREQsRUFFQ0MsU0FGRCxFQUdLOztBQUVMLFNBQUtYLElBQUwsR0FBWVUsWUFBWWYsSUFBWixDQUFpQkEsSUFBN0I7O0FBRUEsT0FBR2dCLFVBQVVoQixJQUFWLENBQWVBLElBQWYsSUFBdUIsQ0FBMUIsRUFDQyxNQUFLQyxLQUFMLEdBQWFlLFVBQVVDLE1BQVYsSUFBb0IsR0FBcEIsR0FBeUJELFVBQVVoQixJQUFuQyxHQUF3QyxFQUFyRCxDQURELEtBR0NrQixFQUFFLFVBQUYsRUFBY0MsSUFBZCxHQVBJLENBT2tCO0FBQ3ZCLEdBWk8sQ0FIUixFQWdCQ0MsS0FoQkQsQ0FnQk9GLEVBQUVHLElBaEJUO0FBaUJBLEVBcENnQjs7QUFxQ2pCQyxVQUFTO0FBQ1JDLGtCQURRLDRCQUNTcEIsRUFEVCxFQUNZQyxLQURaLEVBQ2tCO0FBQ3pCLFFBQUtELEVBQUwsR0FBVUEsRUFBVjtBQUNBLFFBQUtDLEtBQUwsR0FBYUEsS0FBYjtBQUNBYyxLQUFFLGVBQUYsRUFBbUJNLEtBQW5CLENBQXlCLFFBQXpCO0FBQ0EsR0FMTztBQU1SQyxpQkFOUSw2QkFNVztBQUFBOztBQUNsQmQsY0FBV0YsR0FBWCxDQUFlLHNCQUFzQixLQUFLTixFQUExQyxFQUNDVSxJQURELENBQ00sa0JBQVU7QUFDZkssTUFBRSxZQUFGLEVBQWdCUSxJQUFoQixDQUFxQkMsT0FBTzNCLElBQTVCO0FBQ0E0QixXQUFPQyxPQUFQLENBQWUsT0FBS3hCLElBQUwsQ0FBVSxTQUFWLENBQWYsRUFBb0MsT0FBS0osS0FBTCxDQUFXRCxJQUFYLENBQWdCLE9BQUtJLEtBQXJCLEVBQTRCMEIsSUFBaEU7QUFDQSxXQUFLN0IsS0FBTCxDQUFXRCxJQUFYLENBQWdCK0IsTUFBaEIsQ0FBdUIsT0FBSzNCLEtBQTVCLEVBQW1DLENBQW5DO0FBQ0EsUUFBRyxPQUFLSCxLQUFMLENBQVdELElBQVgsQ0FBZ0JnQyxNQUFoQixJQUEwQixDQUE3QixFQUNDZCxFQUFFLFVBQUYsRUFBY0MsSUFBZDtBQUNELElBUEQ7QUFRQSxHQWZPO0FBZ0JSYyxXQWhCUSxxQkFnQkc5QixFQWhCSCxFQWdCTUMsS0FoQk4sRUFnQmE7QUFBQTs7QUFDcEJJLFNBQU1DLEdBQU4sQ0FBVXlCLFNBQVNDLE1BQVQsR0FBa0IsV0FBNUIsRUFBd0M7QUFDcENDLFlBQVE7QUFDUEMsY0FBUW5CLEVBQUUsYUFBV2YsRUFBYixFQUFpQm1DLEdBQWpCLEVBREQ7QUFFVkMsVUFBSXJCLEVBQUUsU0FBT2YsRUFBVCxFQUFhbUMsR0FBYjtBQUZNO0FBRDRCLElBQXhDLEVBTUN6QixJQU5ELENBTU0sa0JBQVU7QUFDZkssTUFBRSxpQ0FBRixFQUFxQ1EsSUFBckMsQ0FBMENDLE9BQU8zQixJQUFQLENBQVl3QyxlQUF0RDtBQUNZLFFBQUdiLE9BQU8zQixJQUFQLENBQVl5QyxTQUFmLEVBQ0E7QUFDQyxTQUFHZCxPQUFPM0IsSUFBUCxDQUFZeUMsU0FBWixJQUF1QixVQUExQixFQUNDQyxPQUFPUixRQUFQLEdBQWtCLGNBQWNQLE9BQU8zQixJQUFQLENBQVkyQyxJQUE1QyxDQURELEtBR0NmLE9BQU9nQixLQUFQLENBQWFqQixPQUFPM0IsSUFBUCxDQUFZeUMsU0FBekIsRUFBb0MsT0FBS3BDLElBQUwsQ0FBVSxXQUFWLENBQXBDO0FBQ0QsS0FORCxNQU1PO0FBQ2xCdUIsWUFBT0MsT0FBUCxDQUFlLE9BQUt4QixJQUFMLENBQVUsT0FBVixDQUFmLEVBQW1DLE9BQUtKLEtBQUwsQ0FBV0QsSUFBWCxDQUFnQkksS0FBaEIsRUFBdUIwQixJQUExRDtBQUNBLFlBQUs3QixLQUFMLENBQVdELElBQVgsQ0FBZ0IrQixNQUFoQixDQUF1QjNCLEtBQXZCLEVBQThCLENBQTlCO0FBQ0EsU0FBRyxPQUFLSCxLQUFMLENBQVdELElBQVgsQ0FBZ0JnQyxNQUFoQixJQUEwQixDQUE3QixFQUNDZCxFQUFFLFVBQUYsRUFBY0MsSUFBZDtBQUNEUixnQkFBV0YsR0FBWCxDQUFlLHNCQUFzQk4sRUFBckMsRUFDQ1UsSUFERCxDQUNNLGtCQUFVO0FBQ2ZLLFFBQUUsWUFBRixFQUFnQlEsSUFBaEIsQ0FBcUJDLE9BQU8zQixJQUE1QjtBQUNBLE1BSEQ7QUFJWTtBQUNiLElBeEJEO0FBeUJBO0FBMUNPOztBQXJDUSxDQUFSLENBQVY7O0FBb0ZBLFNBQVNMLGdCQUFULENBQTBCa0QsQ0FBMUIsRUFBNkI7QUFDekIsUUFBT0EsRUFBRUMsUUFBRixHQUFhQyxPQUFiLENBQXFCLHVCQUFyQixFQUE4QyxHQUE5QyxDQUFQO0FBQ0gsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0REO1NBRUE7O3lEQUVBO3FDQUNBO0FBQ0E7MkNBQ0E7OEJBQ0E7QUFFQTtBQVBBO0FBRkEsRzs7Ozs7OztBQy9CQTtBQUNBO0FBQ0EseUJBQXFKO0FBQ3JKO0FBQ0EseUJBQStHO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekVBOzswQkFHQTsyQkFFQTtBQUhBO0FBREEsRzs7Ozs7OztBQ3hCQTtBQUNBO0FBQ0EsdUJBQXFKO0FBQ3JKO0FBQ0Esd0JBQTRHO0FBQzVHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEIiwiZmlsZSI6ImpzL3BhZ2VzL2Zhdm9yaXRlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNDI4KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBmNWZjZmYwZTFmMDgwMDk3Y2Y0NiIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDE4IDE5IDIwIDIxIDIyIDIzIDI0IDI1IDI2IDI3IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwgbWQtbW9kYWwgZmFkZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IF92bS5pZCxcbiAgICAgIFwidGFiaW5kZXhcIjogXCItMVwiLFxuICAgICAgXCJyb2xlXCI6IFwiZGlhbG9nXCIsXG4gICAgICBcImFyaWEtbGFiZWxsZWRieVwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIGNsYXNzOiAnbW9kYWwtZGlhbG9nICcgKyBfdm0uc2l6ZSxcbiAgICBhdHRyczoge1xuICAgICAgXCJyb2xlXCI6IFwiZG9jdW1lbnRcIlxuICAgIH1cbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtY29udGVudFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWhlYWRlclwiXG4gIH0sIFtfYygnaDQnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtdGl0bGVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcIm1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiLCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtYm9keVwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1ib2R5XCIsIFtfdm0uX3YoXCJNb2RhbCBCb2R5XCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtZm9vdGVyXCIsIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTY1M2U3MzY5XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNjUzZTczNjlcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDEyXG4vLyBtb2R1bGUgY2h1bmtzID0gNiAxNCAxNSIsIlZ1ZS5jb21wb25lbnQoJ2Zhdm9yaXRlcy1saXN0JywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvcGFnZXMvRmF2b3JpdGVzLnZ1ZScpKTtcclxuVnVlLmNvbXBvbmVudCgnZmF2LW1vZGFsJywgIHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvRGlhbG9nTW9kYWwudnVlJykpO1xyXG5cclxuVnVlLmZpbHRlcignY3VycmVuY3knLCBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgIHJldHVybiAgJ+KCsScrbnVtYmVyV2l0aENvbW1hcyhwYXJzZUZsb2F0KHZhbHVlKS50b0ZpeGVkKDIpKTtcclxufSk7XHJcblxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcblxyXG5cdGVsOiAnI2Zhdm9yaXRlcy1jb250ZW50JyxcclxuXHRkYXRhOiB7XHJcblx0XHRpdGVtczpbXSxcclxuXHRcdG5vX2l0ZW06ICcnLFxyXG5cdFx0aWQ6JycsXHJcblx0XHRpbmRleDonJyxcclxuXHRcdGxhbmc6W11cclxuXHR9LFxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdGZ1bmN0aW9uIGdldFRyYW5zbGF0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3MuZ2V0KCcvdHJhbnNsYXRlL2Zhdm9yaXRlcycpO1xyXG4gICAgICAgIH1cclxuXHJcblx0XHRmdW5jdGlvbiBnZXRGYXZvcml0ZXMoKSB7XHJcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2Zhdm9yaXRlcycpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRheGlvcy5hbGwoW1xyXG5cdFx0XHRnZXRUcmFuc2xhdGlvbigpLFxyXG5cdFx0XHRnZXRGYXZvcml0ZXMoKVxyXG5cdFx0XSkudGhlbihheGlvcy5zcHJlYWQoXHJcblx0XHRcdChcdFxyXG5cdFx0XHRcdHRyYW5zbGF0aW9uLFxyXG5cdFx0XHRcdGZhdm9yaXRlc1xyXG5cdFx0XHQpID0+IHtcclxuXHJcblx0XHRcdHRoaXMubGFuZyA9IHRyYW5zbGF0aW9uLmRhdGEuZGF0YTtcclxuXHJcblx0XHRcdGlmKGZhdm9yaXRlcy5kYXRhLmRhdGEgIT0gMClcclxuXHRcdFx0XHR0aGlzLml0ZW1zID0gZmF2b3JpdGVzLnN0YXR1cyA9PSAyMDA/IGZhdm9yaXRlcy5kYXRhOltdO1xyXG5cdFx0XHRlbHNlXHJcblx0XHRcdFx0JCgnLm5vLWl0ZW0nKS5zaG93KCk7IC8vdGhpcy5ub19pdGVtID0gXCJUaGVyZSBhcmUgbm8gaXRlbXMgaW4geW91ciB3aXNobGlzdFwiO1xyXG5cdFx0fSkpXHJcblx0XHQuY2F0Y2goJC5ub29wKTtcclxuXHR9LFxyXG5cdG1ldGhvZHM6IHtcclxuXHRcdHNob3dSZW1vdmVEaWFsb2coaWQsaW5kZXgpe1xyXG5cdFx0XHR0aGlzLmlkID0gaWQ7XHJcblx0XHRcdHRoaXMuaW5kZXggPSBpbmRleDtcclxuXHRcdFx0JCgnI2RpYWxvZ1JlbW92ZScpLm1vZGFsKCd0b2dnbGUnKTtcclxuXHRcdH0sXHJcblx0XHRkZWxldGVGYXZvcml0ZXMgKCkge1xyXG5cdFx0XHRheGlvc0FQSXYxLmdldCgnL2RlbGV0ZWZhdm9yaXRlcy8nICsgdGhpcy5pZClcdFx0XHRcclxuXHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHQkKFwiI2Zhdi1jb3VudFwiKS50ZXh0KHJlc3VsdC5kYXRhKTtcclxuXHRcdFx0XHR0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmdbJ3JlbW92ZWQnXSx0aGlzLml0ZW1zLmRhdGFbdGhpcy5pbmRleF0ubmFtZSk7XHJcblx0XHRcdFx0dGhpcy5pdGVtcy5kYXRhLnNwbGljZSh0aGlzLmluZGV4LCAxKTtcclxuXHRcdFx0XHRpZih0aGlzLml0ZW1zLmRhdGEubGVuZ3RoID09IDApXHJcblx0XHRcdFx0XHQkKCcubm8taXRlbScpLnNob3coKTtcclxuXHRcdFx0fSk7XHRcdFxyXG5cdFx0fSxcclxuXHRcdGFkZHRvY2FydCAoaWQsaW5kZXgpIHtcclxuXHRcdFx0YXhpb3MuZ2V0KGxvY2F0aW9uLm9yaWdpbiArICcvY2FydC9hZGQnLHtcclxuXHRcdFx0ICAgIHBhcmFtczoge1xyXG5cdFx0XHQgICAgXHRwcm9kX2lkOiQoJyNwcm9kX2lkJytpZCkudmFsKCksXHJcblx0XHRcdFx0XHRxdHk6JCgnI3F0eScraWQpLnZhbCgpXHJcblx0XHRcdCAgICB9XHJcblx0XHRcdH0pXHJcblx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0JChcIiNjYXJ0LWNvdW50LCAjY2FydC1jb3VudC1tb2JpbGVcIikudGV4dChyZXN1bHQuZGF0YS5jYXJ0X2l0ZW1fY291bnQpO1xyXG4gICAgICAgICAgICAgICAgaWYocmVzdWx0LmRhdGEuZXJyb3JfbXNnKVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXHRpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2c9PSdSZWRpcmVjdCcpXHJcbiAgICAgICAgICAgICAgICBcdFx0d2luZG93LmxvY2F0aW9uID0gJy9wcm9kdWN0LycgKyByZXN1bHQuZGF0YS5zbHVnO1xyXG4gICAgICAgICAgICAgICAgXHRlbHNlXHJcblx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQnXSk7ICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuXHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1snYWRkZWQnXSwgdGhpcy5pdGVtcy5kYXRhW2luZGV4XS5uYW1lKTtcclxuXHRcdFx0XHRcdHRoaXMuaXRlbXMuZGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG5cdFx0XHRcdFx0aWYodGhpcy5pdGVtcy5kYXRhLmxlbmd0aCA9PSAwKVxyXG5cdFx0XHRcdFx0XHQkKCcubm8taXRlbScpLnNob3coKTtcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRheGlvc0FQSXYxLmdldCgnL2RlbGV0ZWZhdm9yaXRlcy8nICsgaWQpXHJcblx0XHRcdFx0XHQudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdFx0XHQkKFwiI2Zhdi1jb3VudFwiKS50ZXh0KHJlc3VsdC5kYXRhKTtcclxuXHRcdFx0XHRcdH0pOyAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxufSk7XHJcblxyXG5mdW5jdGlvbiBudW1iZXJXaXRoQ29tbWFzKHgpIHtcclxuICAgIHJldHVybiB4LnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIsXCIpO1xyXG59XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvZmF2b3JpdGVzLmpzIiwiPHRlbXBsYXRlPlxyXG48ZGl2IGNsYXNzPVwidGFibGUtcmVzcG9uc2l2ZVwiPlxyXG5cdDx0YWJsZSBjbGFzcz1cInRhYmxlIGNhcnQtdGFibGUgd2lzaGxpc3QtdGFibGUgdGV4dC1jZW50ZXJcIj5cclxuXHRcdDx0aGVhZD5cclxuXHRcdFx0PHRyPlxyXG5cdFx0XHRcdDx0aCBjbGFzcz1cImltYWdlXCI+PC90aD5cclxuXHRcdFx0XHQ8dGggY2xhc3M9XCJuYW1lXCI+UFJPRFVDVCBOQU1FPC90aD5cclxuXHRcdFx0XHQ8dGggY2xhc3M9XCJzdG9ja1wiPlNUT0NLIFNUQVRVUzwvdGg+XHJcblx0XHRcdFx0PHRoIGNsYXNzPVwicHJpY2VcIj5QUklDRTwvdGg+XHJcblx0XHRcdFx0PHRoIGNsYXNzPVwiYWRkLWNhcnRcIj48L3RoPlxyXG5cdFx0XHRcdDx0aCBjbGFzcz1cInJlbW92ZVwiPjwvdGg+XHJcblx0XHRcdDwvdHI+XHJcblx0XHQ8L3RoZWFkPlxyXG5cdFx0PHRib2R5PlxyXG5cdFx0ICA8dHIgdi1mb3I9XCIoaXRlbSxpbmRleCkgaW4gaXRlbXMuZGF0YVwiIDprZXk9XCJpdGVtLmlkXCIgOmlkPVwiJ2RpdicraXRlbS5pZFwiPlxyXG5cdFx0ICAgIDx0ZD48YSA6aHJlZj1cIicvcHJvZHVjdC8nICsgaXRlbS5zbHVnXCIgY2xhc3M9XCJjYXJ0LXByby1pbWFnZVwiPjxpbWcgOnNyYz1cIml0ZW0uaW1hZ2VcIiBhbHQ9XCJcIiAvPjwvYT48L3RkPlxyXG5cdFx0ICAgIDx0ZD48YSA6aHJlZj1cIicvcHJvZHVjdC8nICsgaXRlbS5zbHVnXCIgY2xhc3M9XCJjYXJ0LXByby10aXRsZVwiPnt7aXRlbS5uYW1lfX08L2E+PC90ZD5cclxuXHRcdCAgICA8dGQ+PHAgY2xhc3M9XCJzdG9jayBpbi1zdG9ja1wiPnt7aXRlbS5zdG9ja319PC9wPjwvdGQ+XHJcblx0XHQgICAgPHRkPjxwIGNsYXNzPVwiY2FydC1wcm8tcHJpY2VcIj57e2l0ZW0ucHJpY2UgfCBjdXJyZW5jeX19PC9wPjwvdGQ+XHJcblx0XHQgICAgPHRkPjxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cImJ0biBidG4tcmFpc2VkIGJ0bi1wcmltYXJ5IGFkZC1jYXJ0LWJ0blwiIEBjbGljaz1cImFkZHRvY2FydChpdGVtLmlkLCBpbmRleClcIj5BREQgVE8gQ0FSVDxkaXYgY2xhc3M9XCJyaXBwbGUtY29udGFpbmVyXCI+PC9kaXY+PC9hPjwvdGQ+XHJcblx0XHQgICAgPHRkPjxhIDppZD1cIididG4nICsgaXRlbS5pZFwiIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBAY2xpY2s9XCJzaG93UmVtb3ZlRGlhbG9nKGl0ZW0uaWQsaW5kZXgpXCI+PGkgY2xhc3M9XCJmYSBmYS10cmFzaC1vIGNhcnQtcHJvLXJlbW92ZVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L2E+PC90ZD5cclxuXHRcdCAgICA8aW5wdXQgOmlkPVwiJ3Byb2RfaWQnICsgaXRlbS5pZFwiIDpuYW1lPVwiJ3Byb2RfaWQnICsgaXRlbS5pZFwiIHR5cGU9XCJoaWRkZW5cIiA6dmFsdWU9XCJpdGVtLnByb2R1Y3RfaWRcIj5cclxuXHRcdCAgICA8aW5wdXQgOmlkPVwiJ3F0eScgKyBpdGVtLmlkXCIgOm5hbWU9XCIncXR5JytpdGVtLmlkXCIgdHlwZT1cImhpZGRlblwiIHZhbHVlPVwiMVwiPlx0ICAgIFxyXG5cdFx0ICA8L3RyPlxyXG5cdFx0PC90Ym9keT5cclxuXHQ8L3RhYmxlPlxyXG48L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRwcm9wczogWydpdGVtcyddLFxyXG5cdG1ldGhvZHM6e1xyXG5cdFx0c2hvd1JlbW92ZURpYWxvZyhpZCxpbmRleCl7XHJcblx0XHRcdHRoaXMuJHBhcmVudC5zaG93UmVtb3ZlRGlhbG9nKGlkLGluZGV4KVxyXG5cdFx0fSxcclxuXHRcdGFkZHRvY2FydChpZCxpbmRleCl7XHJcblx0XHRcdHRoaXMuJHBhcmVudC5hZGR0b2NhcnQoaWQsaW5kZXgpXHJcblx0XHR9XHRcdFxyXG5cdH1cclxufVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRmF2b3JpdGVzLnZ1ZT84ZDYyMGE4OCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0Zhdm9yaXRlcy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LWIzNzcwZDIwXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0Zhdm9yaXRlcy52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxccGFnZXNcXFxcRmF2b3JpdGVzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIEZhdm9yaXRlcy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtYjM3NzBkMjBcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi1iMzc3MGQyMFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvRmF2b3JpdGVzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzQxXG4vLyBtb2R1bGUgY2h1bmtzID0gMTQiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZS1yZXNwb25zaXZlXCJcbiAgfSwgW19jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZSBjYXJ0LXRhYmxlIHdpc2hsaXN0LXRhYmxlIHRleHQtY2VudGVyXCJcbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3Rib2R5JywgX3ZtLl9sKChfdm0uaXRlbXMuZGF0YSksIGZ1bmN0aW9uKGl0ZW0sIGluZGV4KSB7XG4gICAgcmV0dXJuIF9jKCd0cicsIHtcbiAgICAgIGtleTogaXRlbS5pZCxcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaWRcIjogJ2RpdicgKyBpdGVtLmlkXG4gICAgICB9XG4gICAgfSwgW19jKCd0ZCcsIFtfYygnYScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNhcnQtcHJvLWltYWdlXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2ltZycsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwic3JjXCI6IGl0ZW0uaW1hZ2UsXG4gICAgICAgIFwiYWx0XCI6IFwiXCJcbiAgICAgIH1cbiAgICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnYScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNhcnQtcHJvLXRpdGxlXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogJy9wcm9kdWN0LycgKyBpdGVtLnNsdWdcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KF92bS5fcyhpdGVtLm5hbWUpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygncCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInN0b2NrIGluLXN0b2NrXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhpdGVtLnN0b2NrKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGQnLCBbX2MoJ3AnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjYXJ0LXByby1wcmljZVwiXG4gICAgfSwgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikoaXRlbS5wcmljZSkpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnYScsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcmFpc2VkIGJ0bi1wcmltYXJ5IGFkZC1jYXJ0LWJ0blwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICAgIH0sXG4gICAgICBvbjoge1xuICAgICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS5hZGR0b2NhcnQoaXRlbS5pZCwgaW5kZXgpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiQUREIFRPIENBUlRcIiksIF9jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJyaXBwbGUtY29udGFpbmVyXCJcbiAgICB9KV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZCcsIFtfYygnYScsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaWRcIjogJ2J0bicgKyBpdGVtLmlkLFxuICAgICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIlxuICAgICAgfSxcbiAgICAgIG9uOiB7XG4gICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgX3ZtLnNob3dSZW1vdmVEaWFsb2coaXRlbS5pZCwgaW5kZXgpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBbX2MoJ2knLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmYSBmYS10cmFzaC1vIGNhcnQtcHJvLXJlbW92ZVwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJhcmlhLWhpZGRlblwiOiBcInRydWVcIlxuICAgICAgfVxuICAgIH0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2lucHV0Jywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJpZFwiOiAncHJvZF9pZCcgKyBpdGVtLmlkLFxuICAgICAgICBcIm5hbWVcIjogJ3Byb2RfaWQnICsgaXRlbS5pZCxcbiAgICAgICAgXCJ0eXBlXCI6IFwiaGlkZGVuXCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcInZhbHVlXCI6IGl0ZW0ucHJvZHVjdF9pZFxuICAgICAgfVxuICAgIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaW5wdXQnLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcImlkXCI6ICdxdHknICsgaXRlbS5pZCxcbiAgICAgICAgXCJuYW1lXCI6ICdxdHknICsgaXRlbS5pZCxcbiAgICAgICAgXCJ0eXBlXCI6IFwiaGlkZGVuXCIsXG4gICAgICAgIFwidmFsdWVcIjogXCIxXCJcbiAgICAgIH1cbiAgICB9KV0pXG4gIH0pKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygndGhlYWQnLCBbX2MoJ3RyJywgW19jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJpbWFnZVwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibmFtZVwiXG4gIH0sIFtfdm0uX3YoXCJQUk9EVUNUIE5BTUVcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInN0b2NrXCJcbiAgfSwgW192bS5fdihcIlNUT0NLIFNUQVRVU1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndGgnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHJpY2VcIlxuICB9LCBbX3ZtLl92KFwiUFJJQ0VcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RoJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImFkZC1jYXJ0XCJcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCd0aCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyZW1vdmVcIlxuICB9KV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1iMzc3MGQyMFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWIzNzcwZDIwXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcGFnZXMvRmF2b3JpdGVzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzk5XG4vLyBtb2R1bGUgY2h1bmtzID0gMTQiLCI8dGVtcGxhdGU+XHJcbjxkaXYgY2xhc3M9XCJtb2RhbCBtZC1tb2RhbCBmYWRlXCIgOmlkPVwiaWRcIiB0YWJpbmRleD1cIi0xXCIgcm9sZT1cImRpYWxvZ1wiIGFyaWEtbGFiZWxsZWRieT1cIm1vZGFsLWxhYmVsXCI+XHJcbiAgICA8ZGl2IDpjbGFzcz1cIidtb2RhbC1kaWFsb2cgJytzaXplXCIgcm9sZT1cImRvY3VtZW50XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgPGg0IGNsYXNzPVwibW9kYWwtdGl0bGVcIiBpZD1cIm1vZGFsLWxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLXRpdGxlXCI+TW9kYWwgVGl0bGU8L3Nsb3Q+XHJcbiAgICAgICAgICAgICAgICA8L2g0PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1ib2R5XCI+TW9kYWwgQm9keTwvc2xvdD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cclxuICAgICAgICAgICAgICAgIDxzbG90IG5hbWU9XCJtb2RhbC1mb290ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+Q2xvc2U8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvc2xvdD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgICdpZCc6e3JlcXVpcmVkOnRydWV9XHJcbiAgICAgICAgLCdzaXplJzoge2RlZmF1bHQ6J21vZGFsLXNtJ31cclxuICAgIH1cclxufVxyXG48L3NjcmlwdD5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIERpYWxvZ01vZGFsLnZ1ZT8zYzk5MDhhNiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0RpYWxvZ01vZGFsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjUzZTczNjlcXFwifSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRGlhbG9nTW9kYWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXERpYWxvZ01vZGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIERpYWxvZ01vZGFsLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi02NTNlNzM2OVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTY1M2U3MzY5XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9EaWFsb2dNb2RhbC52dWVcbi8vIG1vZHVsZSBpZCA9IDlcbi8vIG1vZHVsZSBjaHVua3MgPSA2IDE0IDE1Il0sInNvdXJjZVJvb3QiOiIifQ==