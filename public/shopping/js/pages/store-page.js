/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 451);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 201:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('product-item', __webpack_require__(355));

Vue.filter('currency', function (value) {
  return numberWithCommas(value.toFixed(2));
});

Vue.filter('round', function (value) {
  return roundOff(value);
});

var app = new Vue({
  el: '#store-content',
  data: {
    items: [{
      id: 1,
      name: 'PhosphorusGrey Melange Printed V Neck T-Shirt',
      image: '/img/demo/vneck1.jpg',
      price: 3000,
      sale_price: 2000
    }, {
      id: 2,
      name: 'United Colors of BenettonNavy Blue Solid V Neck T Shirt',
      image: '/img/demo/vneck2.jpg',
      price: 2000,
      sale_price: 1500
    }, {
      id: 3,
      name: 'WranglerBlack V Neck T Shirt',
      image: '/img/demo/vneck3.jpg',
      price: 1850,
      sale_price: 1500
    }, {
      id: 4,
      name: 'Tagd New YorkGrey Printed V Neck T-Shirts',
      image: '/img/demo/vneck4.jpg',
      price: 2050,
      sale_price: 1699
    }, {
      id: 5,
      name: 'Penshoppe Polo Shirt',
      image: '/img/demo/polo2.jpg',
      price: 1999,
      sale_price: 999
    }]
  }
});

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
  return Math.round(v);
}

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	props: ['item'],
	methods: {
		select: function select(id) {
			this.$parent.selectItem(id);
		}
	},
	computed: {
		discount: function discount() {
			return (1 - this.item.sale_price / this.item.price) * 100;
		}
	}
});

/***/ }),

/***/ 355:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(290),
  /* template */
  __webpack_require__(378),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\store\\management\\Product.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Product.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-38b60b5d", Component.options)
  } else {
    hotAPI.reload("data-v-38b60b5d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 378:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "box-product-outer col-sm-4 col-lg-3"
  }, [_c('div', {
    staticClass: "box-product"
  }, [_c('div', {
    staticClass: "img-wrapper"
  }, [_c('a', {
    attrs: {
      "href": "#"
    }
  }, [_c('img', {
    attrs: {
      "alt": "Product",
      "src": _vm.item.image
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "tags"
  }, [_c('span', {
    staticClass: "label-tags"
  }, [_c('span', {
    staticClass: "label label-danger arrowed"
  }, [_vm._v(_vm._s(_vm._f("round")(_vm.discount)) + "%")])])]), _vm._v(" "), _vm._m(0)]), _vm._v(" "), _c('h6', [_c('a', {
    attrs: {
      "href": "#"
    }
  }, [_vm._v(_vm._s(_vm.item.name))])]), _vm._v(" "), _c('div', {
    staticClass: "price"
  }, [_c('div', [_vm._v(_vm._s(_vm._f("currency")(_vm.item.sale_price)) + " "), _c('span', {
    staticClass: "label-tags"
  }), _c('span', {
    staticClass: "price-old"
  }, [_vm._v(_vm._s(_vm._f("currency")(_vm.item.price)))])])]), _vm._v(" "), _vm._m(1)])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "option"
  }, [_c('a', {
    attrs: {
      "href": "#",
      "data-toggle": "tooltip",
      "title": "Edit Product"
    }
  }, [_c('i', {
    staticClass: "fa fa-edit"
  })]), _vm._v(" "), _c('a', {
    staticClass: "wishlist",
    attrs: {
      "href": "#",
      "data-toggle": "tooltip",
      "title": "Delete Product"
    }
  }, [_c('i', {
    staticClass: "fa fa-trash"
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "rating"
  }, [_c('i', {
    staticClass: "fa fa-star"
  }), _vm._v(" "), _c('i', {
    staticClass: "fa fa-star"
  }), _vm._v(" "), _c('i', {
    staticClass: "fa fa-star"
  }), _vm._v(" "), _c('i', {
    staticClass: "fa fa-star-half-o"
  }), _vm._v(" "), _c('i', {
    staticClass: "fa fa-star-o"
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "#"
    }
  }, [_vm._v("(5 reviews)")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-38b60b5d", module.exports)
  }
}

/***/ }),

/***/ 451:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(201);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3N0b3JlLXBhZ2UuanMiLCJ3ZWJwYWNrOi8vL1Byb2R1Y3QudnVlP2QwMTciLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3N0b3JlL21hbmFnZW1lbnQvUHJvZHVjdC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL3N0b3JlL21hbmFnZW1lbnQvUHJvZHVjdC52dWU/YmIyOCJdLCJuYW1lcyI6WyJWdWUiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwiZmlsdGVyIiwidmFsdWUiLCJudW1iZXJXaXRoQ29tbWFzIiwidG9GaXhlZCIsInJvdW5kT2ZmIiwiYXBwIiwiZWwiLCJkYXRhIiwiaXRlbXMiLCJpZCIsIm5hbWUiLCJpbWFnZSIsInByaWNlIiwic2FsZV9wcmljZSIsIngiLCJ0b1N0cmluZyIsInJlcGxhY2UiLCJ2IiwiTWF0aCIsInJvdW5kIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNsREFBLElBQUlDLFNBQUosQ0FBYyxjQUFkLEVBQStCLG1CQUFBQyxDQUFRLEdBQVIsQ0FBL0I7O0FBRUFGLElBQUlHLE1BQUosQ0FBVyxVQUFYLEVBQXVCLFVBQVVDLEtBQVYsRUFBaUI7QUFDcEMsU0FBUUMsaUJBQWlCRCxNQUFNRSxPQUFOLENBQWMsQ0FBZCxDQUFqQixDQUFSO0FBQ0gsQ0FGRDs7QUFJQU4sSUFBSUcsTUFBSixDQUFXLE9BQVgsRUFBb0IsVUFBVUMsS0FBVixFQUFpQjtBQUNqQyxTQUFRRyxTQUFTSCxLQUFULENBQVI7QUFDSCxDQUZEOztBQUlBLElBQUlJLE1BQU0sSUFBSVIsR0FBSixDQUFRO0FBQ2hCUyxNQUFJLGdCQURZO0FBRWhCQyxRQUFNO0FBQ0pDLFdBQU0sQ0FDSjtBQUNFQyxVQUFJLENBRE47QUFFR0MsWUFBTSwrQ0FGVDtBQUdHQyxhQUFPLHNCQUhWO0FBSUdDLGFBQU0sSUFKVDtBQUtHQyxrQkFBVztBQUxkLEtBREksRUFRSjtBQUNFSixVQUFJLENBRE47QUFFR0MsWUFBTSx5REFGVDtBQUdHQyxhQUFPLHNCQUhWO0FBSUdDLGFBQU0sSUFKVDtBQUtHQyxrQkFBVztBQUxkLEtBUkksRUFlSjtBQUNFSixVQUFJLENBRE47QUFFR0MsWUFBTSw4QkFGVDtBQUdHQyxhQUFPLHNCQUhWO0FBSUdDLGFBQU0sSUFKVDtBQUtHQyxrQkFBVztBQUxkLEtBZkksRUFzQko7QUFDRUosVUFBSSxDQUROO0FBRUdDLFlBQU0sMkNBRlQ7QUFHR0MsYUFBTyxzQkFIVjtBQUlHQyxhQUFNLElBSlQ7QUFLR0Msa0JBQVc7QUFMZCxLQXRCSSxFQTZCSjtBQUNHSixVQUFJLENBRFA7QUFFR0MsWUFBTSxzQkFGVDtBQUdHQyxhQUFPLHFCQUhWO0FBSUdDLGFBQU0sSUFKVDtBQUtHQyxrQkFBVztBQUxkLEtBN0JJO0FBREY7QUFGVSxDQUFSLENBQVY7O0FBNENBLFNBQVNYLGdCQUFULENBQTBCWSxDQUExQixFQUE2QjtBQUN6QixTQUFPQSxFQUFFQyxRQUFGLEdBQWFDLE9BQWIsQ0FBcUIsdUJBQXJCLEVBQThDLEdBQTlDLENBQVA7QUFDSDs7QUFFRCxTQUFTWixRQUFULENBQWtCYSxDQUFsQixFQUFxQjtBQUNqQixTQUFPQyxLQUFLQyxLQUFMLENBQVdGLENBQVgsQ0FBUDtBQUNILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxQkQ7U0FFQTs7OEJBRUE7MkJBQ0E7QUFFQTtBQUpBOztnQ0FNQTt5REFDQTtBQUVBO0FBSkE7QUFQQSxHOzs7Ozs7O0FDbkNBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBa0g7QUFDbEg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsQ0FBQyxhQUFhLGFBQWEsMEJBQTBCO0FBQ3JEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3BhZ2VzL3N0b3JlLXBhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQ1MSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsIlZ1ZS5jb21wb25lbnQoJ3Byb2R1Y3QtaXRlbScsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL3N0b3JlL21hbmFnZW1lbnQvUHJvZHVjdC52dWUnKSk7XHJcblxyXG5WdWUuZmlsdGVyKCdjdXJyZW5jeScsIGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuICBudW1iZXJXaXRoQ29tbWFzKHZhbHVlLnRvRml4ZWQoMikpO1xyXG59KTtcclxuXHJcblZ1ZS5maWx0ZXIoJ3JvdW5kJywgZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICByZXR1cm4gIHJvdW5kT2ZmKHZhbHVlKTtcclxufSk7XHJcblxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcbiAgZWw6ICcjc3RvcmUtY29udGVudCcsXHJcbiAgZGF0YToge1xyXG4gICAgaXRlbXM6W1xyXG4gICAgICB7XHJcbiAgICAgICAgaWQ6IDFcclxuICAgICAgICAsbmFtZTogJ1Bob3NwaG9ydXNHcmV5IE1lbGFuZ2UgUHJpbnRlZCBWIE5lY2sgVC1TaGlydCdcclxuICAgICAgICAsaW1hZ2U6ICcvaW1nL2RlbW8vdm5lY2sxLmpwZydcclxuICAgICAgICAscHJpY2U6MzAwMFxyXG4gICAgICAgICxzYWxlX3ByaWNlOjIwMDBcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGlkOiAyXHJcbiAgICAgICAgLG5hbWU6ICdVbml0ZWQgQ29sb3JzIG9mIEJlbmV0dG9uTmF2eSBCbHVlIFNvbGlkIFYgTmVjayBUIFNoaXJ0J1xyXG4gICAgICAgICxpbWFnZTogJy9pbWcvZGVtby92bmVjazIuanBnJ1xyXG4gICAgICAgICxwcmljZToyMDAwXHJcbiAgICAgICAgLHNhbGVfcHJpY2U6MTUwMFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgaWQ6IDNcclxuICAgICAgICAsbmFtZTogJ1dyYW5nbGVyQmxhY2sgViBOZWNrIFQgU2hpcnQnXHJcbiAgICAgICAgLGltYWdlOiAnL2ltZy9kZW1vL3ZuZWNrMy5qcGcnXHJcbiAgICAgICAgLHByaWNlOjE4NTBcclxuICAgICAgICAsc2FsZV9wcmljZToxNTAwXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBpZDogNFxyXG4gICAgICAgICxuYW1lOiAnVGFnZCBOZXcgWW9ya0dyZXkgUHJpbnRlZCBWIE5lY2sgVC1TaGlydHMnXHJcbiAgICAgICAgLGltYWdlOiAnL2ltZy9kZW1vL3ZuZWNrNC5qcGcnXHJcbiAgICAgICAgLHByaWNlOjIwNTBcclxuICAgICAgICAsc2FsZV9wcmljZToxNjk5XHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICAgaWQ6IDVcclxuICAgICAgICAsbmFtZTogJ1BlbnNob3BwZSBQb2xvIFNoaXJ0J1xyXG4gICAgICAgICxpbWFnZTogJy9pbWcvZGVtby9wb2xvMi5qcGcnXHJcbiAgICAgICAgLHByaWNlOjE5OTlcclxuICAgICAgICAsc2FsZV9wcmljZTo5OTlcclxuICAgICAgfSxcclxuICAgIF0sXHJcbiAgfVxyXG59KVxyXG5cclxuXHJcbmZ1bmN0aW9uIG51bWJlcldpdGhDb21tYXMoeCkge1xyXG4gICAgcmV0dXJuIHgudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIixcIik7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJvdW5kT2ZmKHYpIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKHYpO1xyXG59XHJcblxyXG5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9wYWdlcy9zdG9yZS1wYWdlLmpzIiwiPHRlbXBsYXRlPlxyXG4gICAgPGRpdiBjbGFzcz1cImJveC1wcm9kdWN0LW91dGVyIGNvbC1zbS00IGNvbC1sZy0zXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJib3gtcHJvZHVjdFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJpbWctd3JhcHBlclwiPlxyXG4gICAgICAgICAgPGEgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgPGltZyBhbHQ9XCJQcm9kdWN0XCIgdi1iaW5kOnNyYz1cIml0ZW0uaW1hZ2VcIj5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0YWdzXCI+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwibGFiZWwtdGFnc1wiPjxzcGFuIGNsYXNzPVwibGFiZWwgbGFiZWwtZGFuZ2VyIGFycm93ZWRcIj57e2Rpc2NvdW50IHwgcm91bmR9fSU8L3NwYW4+PC9zcGFuPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwib3B0aW9uXCI+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgdGl0bGU9XCJFZGl0IFByb2R1Y3RcIj48aSBjbGFzcz1cImZhIGZhLWVkaXRcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiI1wiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiRGVsZXRlIFByb2R1Y3RcIiBjbGFzcz1cIndpc2hsaXN0XCI+PGkgY2xhc3M9XCJmYSBmYS10cmFzaFwiPjwvaT48L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8aDY+PGEgaHJlZj1cIiNcIj57e2l0ZW0ubmFtZX19PC9hPjwvaDY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInByaWNlXCI+XHJcbiAgICAgICAgICA8ZGl2Pnt7aXRlbS5zYWxlX3ByaWNlIHwgY3VycmVuY3l9fSA8c3BhbiBjbGFzcz1cImxhYmVsLXRhZ3NcIj48L3NwYW4+PHNwYW4gY2xhc3M9XCJwcmljZS1vbGRcIj57e2l0ZW0ucHJpY2UgfCBjdXJyZW5jeX19PC9zcGFuPjwvZGl2PlxyXG4gICAgICAgICAgXHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJhdGluZ1wiPlxyXG4gICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPlxyXG4gICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPlxyXG4gICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1zdGFyXCI+PC9pPlxyXG4gICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1zdGFyLWhhbGYtb1wiPjwvaT5cclxuICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtc3Rhci1vXCI+PC9pPlxyXG4gICAgICAgICAgPGEgaHJlZj1cIiNcIj4oNSByZXZpZXdzKTwvYT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cdFxyXG48L3RlbXBsYXRlPlxyXG5cclxuXHJcbjxzY3JpcHQ+XHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRwcm9wczogWydpdGVtJ10sXHJcblx0bWV0aG9kczp7XHJcblx0XHRzZWxlY3QoaWQpe1xyXG5cdFx0XHQgdGhpcy4kcGFyZW50LnNlbGVjdEl0ZW0oaWQpXHJcblx0XHR9XHJcblx0fSxcclxuXHRjb21wdXRlZDp7XHJcblx0XHRkaXNjb3VudCgpe1xyXG5cdFx0XHRyZXR1cm4gKDEtKHRoaXMuaXRlbS5zYWxlX3ByaWNlIC8gdGhpcy5pdGVtLnByaWNlKSkqMTAwXHJcblx0XHR9XHJcblx0fVxyXG59XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBQcm9kdWN0LnZ1ZT83YjE0MDJkNCIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL1Byb2R1Y3QudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0zOGI2MGI1ZFxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Qcm9kdWN0LnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiQzpcXFxceGFtcHBcXFxcaHRkb2NzXFxcXHd5YzA2XFxcXHJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcc2hvcHBpbmdcXFxcY29tcG9uZW50c1xcXFxzdG9yZVxcXFxtYW5hZ2VtZW50XFxcXFByb2R1Y3QudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gUHJvZHVjdC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMzhiNjBiNWRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0zOGI2MGI1ZFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvc3RvcmUvbWFuYWdlbWVudC9Qcm9kdWN0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzU1XG4vLyBtb2R1bGUgY2h1bmtzID0gMTciLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJib3gtcHJvZHVjdC1vdXRlciBjb2wtc20tNCBjb2wtbGctM1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJveC1wcm9kdWN0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1nLXdyYXBwZXJcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIiNcIlxuICAgIH1cbiAgfSwgW19jKCdpbWcnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiYWx0XCI6IFwiUHJvZHVjdFwiLFxuICAgICAgXCJzcmNcIjogX3ZtLml0ZW0uaW1hZ2VcbiAgICB9XG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwidGFnc1wiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbC10YWdzXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImxhYmVsIGxhYmVsLWRhbmdlciBhcnJvd2VkXCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwicm91bmRcIikoX3ZtLmRpc2NvdW50KSkgKyBcIiVcIildKV0pXSksIF92bS5fdihcIiBcIiksIF92bS5fbSgwKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaDYnLCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIiNcIlxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLml0ZW0ubmFtZSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJwcmljZVwiXG4gIH0sIFtfYygnZGl2JywgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikoX3ZtLml0ZW0uc2FsZV9wcmljZSkpICsgXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbC10YWdzXCJcbiAgfSksIF9jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInByaWNlLW9sZFwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKF92bS5pdGVtLnByaWNlKSkpXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX20oMSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJvcHRpb25cIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIiNcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiRWRpdCBQcm9kdWN0XCJcbiAgICB9XG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1lZGl0XCJcbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwid2lzaGxpc3RcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiI1wiLFxuICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcbiAgICAgIFwidGl0bGVcIjogXCJEZWxldGUgUHJvZHVjdFwiXG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtdHJhc2hcIlxuICB9KV0pXSlcbn0sZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJhdGluZ1wiXG4gIH0sIFtfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1zdGFyXCJcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLXN0YXJcIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtc3RhclwiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1zdGFyLWhhbGYtb1wiXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1zdGFyLW9cIlxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiBcIiNcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIig1IHJldmlld3MpXCIpXSldKVxufV19XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTM4YjYwYjVkXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMzhiNjBiNWRcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9zdG9yZS9tYW5hZ2VtZW50L1Byb2R1Y3QudnVlXG4vLyBtb2R1bGUgaWQgPSAzNzhcbi8vIG1vZHVsZSBjaHVua3MgPSAxNyJdLCJzb3VyY2VSb290IjoiIn0=