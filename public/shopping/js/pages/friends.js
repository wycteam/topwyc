/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 430);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 180:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('friend-container', __webpack_require__(334));

window.timeout = null;

var friendApp = new Vue({
    el: '#relationships_container',

    data: {
        friends: [],
        requests: [],
        recommended: [],
        searched: [],
        keyword: '',

        lang: []
    },

    watch: {
        // whenever question changes, this function will run
        keyword: function keyword(newKeyword) {
            var _this = this;

            clearTimeout(window.timeout);
            window.timeout = setTimeout(function () {
                if (newKeyword == '') {
                    _this.searched = [];
                } else {
                    axiosAPIv1.get('/users?search=' + newKeyword).then(function (accounts) {

                        temp_recommended = [];
                        temp_searched = [];
                        for (var i = 0; i < accounts.data.length; i++) {
                            temp = [];
                            temp.user = accounts.data[i];
                            temp_recommended.push(temp);

                            if (accounts.data[i].id != window.Laravel.user.id) {
                                temp_searched.push(temp);
                            }
                        }

                        _this.searched = temp_searched;
                    }).catch($.noop);
                }
            }, 500);
        }
    },

    methods: {
        getFriendsData: function getFriendsData() {
            var _this2 = this;

            function getFriend() {
                return axiosAPIv1.get('/friends');
            }
            function getRecommend() {
                return axiosAPIv1.get('/users?takeNotFriend=4');
            }

            axios.all([getFriend(), getRecommend()]).then(axios.spread(function (friends, recommended) {
                _this2.friends = friends.data.friends;
                // this.requests = friends.data.requests;
                for (var i = 0; i < friends.data.requests.length; i++) {
                    if (!friends.data.requests[i].requestor) {
                        _this2.requests.push(friends.data.requests[i]);
                    }
                }
                temp_recommended = [];
                for (var i = 0; i < recommended.data.length; i++) {
                    temp = [];
                    temp.user = recommended.data[i];
                    temp_recommended.push(temp);
                }
                _this2.recommended = temp_recommended;
            })).catch($.noop);
        },
        select: function select(user) {
            this.toggleOffcanvas();

            var chat = [];
            $(".panel-chat-box").each(function () {
                chat.push($(this).attr('id'));
            });

            if ($.inArray('user' + user.id, chat) == '-1') Event.fire('chat-box.show', user);

            setTimeout(function () {
                $('.js-auto-size').textareaAutoSize();
            }, 5000);
        },
        toggleOffcanvas: function toggleOffcanvas() {
            $(this.$el).toggleClass('expanded');
        }
    },
    created: function created() {
        var _this3 = this;

        this.getFriendsData();
        axios.get('/translate/friends').then(function (language) {
            _this3.lang = language.data.data;
        });
    }
});

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			notBlocked: true

		};
	},

	props: ['data', 'type'],
	methods: {
		btnFn: function btnFn(id, type) {
			var _this = this;

			switch (type) {
				case 'add':
					axiosAPIv1.post('/friends', {
						user_id2: id
					}).then(function (response) {
						if (response.status == 201) {
							for (var j = 0; j < _this.$root.recommended.length; j++) {
								if (_this.$root.recommended[j].user.id == id) {
									//remove
									_this.$root.recommended.splice(j, 1);
									_this.$root.getFriendsData();
								}
							}
							toastr.success('', 'Friend request sent!');
						}
					});
					break;
				case 'request':
					axiosAPIv1.post('/friend/find', {
						user_id: Laravel.user.id,
						user_id2: id
					}).then(function (response) {
						if (response.status == 200) {
							axiosAPIv1.put('/friends/' + response.data, {
								status: 'Friend'
							}).then(function (data) {
								if (data.status == 200) {
									for (var j = 0; j < _this.$root.requests.length; j++) {
										if (_this.$root.requests[j].user.id == id) {
											_this.$root.friends.push(_this.$root.requests[j]);
											//remove
											_this.$root.requests.splice(j, 1);
										}
									}
									toastr.success('', 'Friend Accepted!');
								}
							}).catch($.noop);
						}
					});
					break;
				case 'reject':
					axiosAPIv1.post('/friend/find', {
						user_id: Laravel.user.id,
						user_id2: id
					}).then(function (response) {
						if (response.status == 200) {
							axiosAPIv1.delete('/friends/' + response.data).then(function (data) {
								if (data.status == 200) {
									for (var j = 0; j < _this.$root.requests.length; j++) {
										if (_this.$root.requests[j].user.id == id) {
											//remove
											_this.$root.requests.splice(j, 1);
										}
									}
									toastr.success('', 'Rejected!');
								}
							}).catch($.noop);
						}
					});
					break;
				case 'unfriend':
				case 'cancel_request':
					axiosAPIv1.post('/friend/find', {
						user_id: Laravel.user.id,
						user_id2: id
					}).then(function (response) {
						if (response.status == 200) {
							axiosAPIv1.delete('/friends/' + response.data).then(function (data) {
								if (data.status == 200) {
									for (var j = 0; j < _this.$root.friends.length; j++) {
										if (_this.$root.friends[j].user.id == id) {
											//remove
											_this.$root.friends.splice(j, 1);
										}
									}
									toastr.success('', 'Successful!');
								}
							}).catch($.noop);
						}
					});
					break;
			}
		},
		select: function select(user) {
			this.$parent.select(user);
		}
	},

	computed: {
		button: function button() {
			var data = {};
			data.type = 'add';
			data.content = 'Add Friend';
			data.style = 'btn-success';

			if (this.data.user.initiator) {
				switch (this.data.user.rel_status) {
					case 'Request':
						data.type = 'cancel_request';
						data.content = 'Cancel Request';
						data.style = 'btn-danger';
						break;

					case 'Friend':
						data.type = 'unfriend';
						data.content = 'Unfriend';
						data.style = 'btn-danger';
						break;

					case 'Blocked':
						data.type = 'cancel_request';
						data.content = 'Unblock';
						data.style = 'btn-info';
						break;

					case 'Restricted':
						data.type = 'cancel_request';
						data.content = 'Unrestrict';
						data.style = 'btn-info';
						break;

					default:

				}
			} else {
				switch (this.data.user.rel_status) {
					case 'Request':
						this.type = 'request';
						break;

					case 'Friend':
						data.type = 'unfriend';
						data.content = 'Unfriend';
						data.style = 'btn-danger';
						break;

					case 'Blocked':
						this.notBlocked = false;
						break;
					case 'Restricted':
						this.type = 'restricted';
						break;
					default:

				}
			}
			return data;
		}
	},

	created: function created() {
		console.log(this.$parent.lang);
	}
});

/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(269),
  /* template */
  __webpack_require__(387),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\friends\\FriendContainer.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] FriendContainer.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5fd8b42d", Component.options)
  } else {
    hotAPI.reload("data-v-5fd8b42d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 387:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.notBlocked) ? _c('div', {
    staticClass: "col-lg-3 col-md-3 col-sm-6 col-xs-12 friend-container"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "col-lg-12 text-center"
  }, [_c('img', {
    staticClass: "friend-image img-circle img-responsive",
    attrs: {
      "src": _vm.data.user.avatar
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "friend-name"
  }, [_c('a', {
    attrs: {
      "href": '/users/' + _vm.data.user.id
    }
  }, [_vm._v(_vm._s(_vm._f("truncate")((_vm.data.user.first_name + " " + _vm.data.user.last_name), 25)))]), _vm._v(" "), _c('br'), _vm._v(" "), _vm._t("date", [_c('small', {
    staticClass: "tc-grey"
  }, [_vm._v(_vm._s(_vm.data.date))])])], 2)]), _vm._v(" "), (_vm.type == "request") ? _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center"
  }, [_c('button', {
    staticClass: "btn btn-link btn-danger btn-has-loading",
    on: {
      "click": function($event) {
        _vm.btnFn(_vm.data.user.id, "reject")
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang['reject']))])]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center"
  }, [_c('button', {
    staticClass: "btn btn-link btn-success btn-has-loading",
    on: {
      "click": function($event) {
        _vm.btnFn(_vm.data.user.id, "request")
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang['accept']))])])]) : _vm._e(), _vm._v(" "), (_vm.type == "friend") ? _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center margin"
  }, [_c('button', {
    staticClass: "btn btn-link btn-danger btn-has-loading",
    on: {
      "click": function($event) {
        _vm.select(_vm.data.user)
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang['send-message']))])]), _vm._v(" "), _c('div', {
    staticClass: "col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center"
  }, [_c('button', {
    staticClass: "btn btn-link btn-danger btn-has-loading",
    on: {
      "click": function($event) {
        _vm.btnFn(_vm.data.user.id, "unfriend")
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang['unfriend']))])])]) : _vm._e(), _vm._v(" "), (_vm.type == "recommended") ? _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 text-center"
  }, [_c('button', {
    staticClass: "btn btn-link btn-success btn-has-loading",
    attrs: {
      "onclick": "this.innerText='Cancel Request'; this.style.color='red';"
    },
    on: {
      "click": function($event) {
        _vm.btnFn(_vm.data.user.id, "add")
      }
    }
  }, [_vm._v(_vm._s(this.$parent.lang['add']))])])]) : _vm._e(), _vm._v(" "), (_vm.type == "searched") ? _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12 text-center"
  }, [_c('button', {
    staticClass: "btn btn-link btn-has-loading",
    class: _vm.button.style,
    on: {
      "click": function($event) {
        _vm.btnFn(_vm.data.user.id, _vm.button.type)
      }
    }
  }, [_vm._v(_vm._s(_vm.button.content))])])]) : _vm._e()])]) : _vm._e()
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5fd8b42d", module.exports)
  }
}

/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(180);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvZnJpZW5kcy5qcyIsIndlYnBhY2s6Ly8vRnJpZW5kQ29udGFpbmVyLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvZnJpZW5kcy9GcmllbmRDb250YWluZXIudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9mcmllbmRzL0ZyaWVuZENvbnRhaW5lci52dWU/ZDdkZCJdLCJuYW1lcyI6WyJWdWUiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwid2luZG93IiwidGltZW91dCIsImZyaWVuZEFwcCIsImVsIiwiZGF0YSIsImZyaWVuZHMiLCJyZXF1ZXN0cyIsInJlY29tbWVuZGVkIiwic2VhcmNoZWQiLCJrZXl3b3JkIiwibGFuZyIsIndhdGNoIiwibmV3S2V5d29yZCIsImNsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJheGlvc0FQSXYxIiwiZ2V0IiwidGhlbiIsInRlbXBfcmVjb21tZW5kZWQiLCJ0ZW1wX3NlYXJjaGVkIiwiaSIsImFjY291bnRzIiwibGVuZ3RoIiwidGVtcCIsInVzZXIiLCJwdXNoIiwiaWQiLCJMYXJhdmVsIiwiY2F0Y2giLCIkIiwibm9vcCIsIm1ldGhvZHMiLCJnZXRGcmllbmRzRGF0YSIsImdldEZyaWVuZCIsImdldFJlY29tbWVuZCIsImF4aW9zIiwiYWxsIiwic3ByZWFkIiwicmVxdWVzdG9yIiwic2VsZWN0IiwidG9nZ2xlT2ZmY2FudmFzIiwiY2hhdCIsImVhY2giLCJhdHRyIiwiaW5BcnJheSIsIkV2ZW50IiwiZmlyZSIsInRleHRhcmVhQXV0b1NpemUiLCIkZWwiLCJ0b2dnbGVDbGFzcyIsImNyZWF0ZWQiLCJsYW5ndWFnZSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBQSxJQUFJQyxTQUFKLENBQWMsa0JBQWQsRUFBbUMsbUJBQUFDLENBQVEsR0FBUixDQUFuQzs7QUFFQUMsT0FBT0MsT0FBUCxHQUFpQixJQUFqQjs7QUFFQSxJQUFJQyxZQUFZLElBQUlMLEdBQUosQ0FBUTtBQUNwQk0sUUFBSSwwQkFEZ0I7O0FBR3BCQyxVQUFNO0FBQ0ZDLGlCQUFjLEVBRFo7QUFFRkMsa0JBQWMsRUFGWjtBQUdGQyxxQkFBYyxFQUhaO0FBSUZDLGtCQUFjLEVBSlo7QUFLRkMsaUJBQWMsRUFMWjs7QUFPRkMsY0FBYTtBQVBYLEtBSGM7O0FBYXBCQyxXQUFPO0FBQ0g7QUFDQUYsaUJBQVMsaUJBQVVHLFVBQVYsRUFBc0I7QUFBQTs7QUFDM0JDLHlCQUFhYixPQUFPQyxPQUFwQjtBQUNBRCxtQkFBT0MsT0FBUCxHQUFpQmEsV0FBVyxZQUFNO0FBQzlCLG9CQUFHRixjQUFjLEVBQWpCLEVBQ0E7QUFDSSwwQkFBS0osUUFBTCxHQUFnQixFQUFoQjtBQUNILGlCQUhELE1BSUE7QUFDS08sK0JBQ0lDLEdBREosQ0FDUSxtQkFBaUJKLFVBRHpCLEVBRUlLLElBRkosQ0FFUyxvQkFBWTs7QUFFZEMsMkNBQW1CLEVBQW5CO0FBQ0FDLHdDQUFnQixFQUFoQjtBQUNBLDZCQUFJLElBQUlDLElBQUUsQ0FBVixFQUFhQSxJQUFFQyxTQUFTakIsSUFBVCxDQUFja0IsTUFBN0IsRUFBcUNGLEdBQXJDLEVBQ0E7QUFDSUcsbUNBQU8sRUFBUDtBQUNBQSxpQ0FBS0MsSUFBTCxHQUFZSCxTQUFTakIsSUFBVCxDQUFjZ0IsQ0FBZCxDQUFaO0FBQ0FGLDZDQUFpQk8sSUFBakIsQ0FBc0JGLElBQXRCOztBQUVBLGdDQUFHRixTQUFTakIsSUFBVCxDQUFjZ0IsQ0FBZCxFQUFpQk0sRUFBakIsSUFBdUIxQixPQUFPMkIsT0FBUCxDQUFlSCxJQUFmLENBQW9CRSxFQUE5QyxFQUFrRDtBQUM5Q1AsOENBQWNNLElBQWQsQ0FBbUJGLElBQW5CO0FBQ0g7QUFDSjs7QUFFRCw4QkFBS2YsUUFBTCxHQUFnQlcsYUFBaEI7QUFFSCxxQkFuQkosRUFvQklTLEtBcEJKLENBb0JVQyxFQUFFQyxJQXBCWjtBQXFCSjtBQUNKLGFBNUJnQixFQTRCZCxHQTVCYyxDQUFqQjtBQTZCSDtBQWpDRSxLQWJhOztBQWlEcEJDLGFBQVE7QUFDSkMsc0JBREksNEJBQ1k7QUFBQTs7QUFDWixxQkFBU0MsU0FBVCxHQUFxQjtBQUNqQix1QkFBT2xCLFdBQVdDLEdBQVgsQ0FBZSxVQUFmLENBQVA7QUFDSDtBQUNELHFCQUFTa0IsWUFBVCxHQUF3QjtBQUNwQix1QkFBT25CLFdBQVdDLEdBQVgsQ0FBZSx3QkFBZixDQUFQO0FBQ0g7O0FBRURtQixrQkFBTUMsR0FBTixDQUFVLENBQ0xILFdBREssRUFFTEMsY0FGSyxDQUFWLEVBR0dqQixJQUhILENBR1FrQixNQUFNRSxNQUFOLENBQ0osVUFDS2hDLE9BREwsRUFFS0UsV0FGTCxFQUdLO0FBQ0wsdUJBQUtGLE9BQUwsR0FBZUEsUUFBUUQsSUFBUixDQUFhQyxPQUE1QjtBQUNBO0FBQ0EscUJBQUksSUFBSWUsSUFBRSxDQUFWLEVBQWFBLElBQUdmLFFBQVFELElBQVIsQ0FBYUUsUUFBYixDQUFzQmdCLE1BQXRDLEVBQThDRixHQUE5QyxFQUFrRDtBQUM5Qyx3QkFBRyxDQUFDZixRQUFRRCxJQUFSLENBQWFFLFFBQWIsQ0FBc0JjLENBQXRCLEVBQXlCa0IsU0FBN0IsRUFDQTtBQUNJLCtCQUFLaEMsUUFBTCxDQUFjbUIsSUFBZCxDQUFtQnBCLFFBQVFELElBQVIsQ0FBYUUsUUFBYixDQUFzQmMsQ0FBdEIsQ0FBbkI7QUFDSDtBQUNKO0FBQ0RGLG1DQUFtQixFQUFuQjtBQUNBLHFCQUFJLElBQUlFLElBQUUsQ0FBVixFQUFhQSxJQUFFYixZQUFZSCxJQUFaLENBQWlCa0IsTUFBaEMsRUFBd0NGLEdBQXhDLEVBQ0E7QUFDSUcsMkJBQU8sRUFBUDtBQUNBQSx5QkFBS0MsSUFBTCxHQUFZakIsWUFBWUgsSUFBWixDQUFpQmdCLENBQWpCLENBQVo7QUFDQUYscUNBQWlCTyxJQUFqQixDQUFzQkYsSUFBdEI7QUFDSDtBQUNELHVCQUFLaEIsV0FBTCxHQUFtQlcsZ0JBQW5CO0FBQ0gsYUFyQk8sQ0FIUixFQXlCQ1UsS0F6QkQsQ0F5Qk9DLEVBQUVDLElBekJUO0FBMEJILFNBbkNHO0FBcUNKUyxjQXJDSSxrQkFxQ0dmLElBckNILEVBcUNTO0FBQ1QsaUJBQUtnQixlQUFMOztBQUVBLGdCQUFJQyxPQUFPLEVBQVg7QUFDQVosY0FBRSxpQkFBRixFQUFxQmEsSUFBckIsQ0FBMEIsWUFBVTtBQUNoQ0QscUJBQUtoQixJQUFMLENBQVVJLEVBQUUsSUFBRixFQUFRYyxJQUFSLENBQWEsSUFBYixDQUFWO0FBQ0gsYUFGRDs7QUFJQSxnQkFBR2QsRUFBRWUsT0FBRixDQUFVLFNBQU9wQixLQUFLRSxFQUF0QixFQUF5QmUsSUFBekIsS0FBZ0MsSUFBbkMsRUFDSUksTUFBTUMsSUFBTixDQUFXLGVBQVgsRUFBNEJ0QixJQUE1Qjs7QUFFSlYsdUJBQVcsWUFBVTtBQUNuQmUsa0JBQUUsZUFBRixFQUFtQmtCLGdCQUFuQjtBQUNELGFBRkQsRUFFRyxJQUZIO0FBR0gsU0FuREc7QUFxREpQLHVCQXJESSw2QkFxRGM7QUFDZFgsY0FBRSxLQUFLbUIsR0FBUCxFQUFZQyxXQUFaLENBQXdCLFVBQXhCO0FBQ0g7QUF2REcsS0FqRFk7QUEwR3BCQyxXQTFHb0IscUJBMEdWO0FBQUE7O0FBQ04sYUFBS2xCLGNBQUw7QUFDQUcsY0FBTW5CLEdBQU4sQ0FBVSxvQkFBVixFQUNLQyxJQURMLENBQ1Usb0JBQVk7QUFDZCxtQkFBS1AsSUFBTCxHQUFZeUMsU0FBUy9DLElBQVQsQ0FBY0EsSUFBMUI7QUFDSCxTQUhMO0FBSUg7QUFoSG1CLENBQVIsQ0FBaEIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3NDQTs7O3VCQUVBOztlQUlBOztBQUhBO0FBSUE7O1FBQ0EsQ0FDQSxRQUVBOzs0QkFFQTtBQUNBOztXQUVBO1NBQ0E7QUFDQTtnQkFHQTtBQUZBLGlDQUdBOzZCQUNBLEtBQ0E7MkRBQ0EsS0FDQTtrREFDQSxJQUNBO0FBQ0E7MkNBQ0E7cUJBQ0E7QUFDQTtBQUNBOzBCQUNBO0FBQ0E7QUFDQTtBQUNBO1NBQ0E7QUFDQTs0QkFFQTtnQkFFQTtBQUhBLGlDQUlBOzZCQUNBLEtBQ0E7QUFDQTtnQkFHQTtBQUZBLCtCQUdBOzJCQUNBLEtBQ0E7MERBQ0EsS0FDQTtpREFDQSxJQUNBO3lEQUNBO0FBQ0E7MENBQ0E7QUFDQTtBQUNBOzRCQUNBO0FBQ0E7QUFDQSxrQkFDQTtBQUNBO0FBQ0E7QUFDQTtTQUNBO0FBQ0E7NEJBRUE7Z0JBRUE7QUFIQSxpQ0FJQTs2QkFDQSxLQUNBO0FBQ0EsZ0RBQ0EsMkJBQ0E7MkJBQ0EsS0FDQTswREFDQSxLQUNBO2lEQUNBLElBQ0E7QUFDQTswQ0FDQTtBQUNBO0FBQ0E7NEJBQ0E7QUFDQTtBQUNBLGtCQUNBO0FBQ0E7QUFDQTtBQUNBO1NBQ0E7U0FDQTtBQUNBOzRCQUVBO2dCQUVBO0FBSEEsaUNBSUE7NkJBQ0EsS0FDQTtBQUNBLGdEQUNBLDJCQUNBOzJCQUNBLEtBQ0E7eURBQ0EsS0FDQTtnREFDQSxJQUNBO0FBQ0E7eUNBQ0E7QUFDQTtBQUNBOzRCQUNBO0FBQ0E7QUFDQSxrQkFDQTtBQUNBO0FBQ0E7QUFFQTs7QUFFQTtnQ0FDQTt1QkFDQTtBQUdBO0FBNUhBOzs7NEJBOEhBO2NBQ0E7ZUFDQTtrQkFDQTtnQkFFQTs7c0JBQ0EsV0FDQTsyQkFFQTtVQUNBO2tCQUNBO3FCQUNBO21CQUNBO0FBRUE7O1VBQ0E7a0JBQ0E7cUJBQ0E7bUJBQ0E7QUFFQTs7VUFDQTtrQkFDQTtxQkFDQTttQkFDQTtBQUVBOztVQUNBO2tCQUNBO3FCQUNBO21CQUNBO0FBRUE7O0FBSUE7OztBQUNBLFVBQ0E7MkJBRUE7VUFDQTtrQkFDQTtBQUVBOztVQUNBO2tCQUNBO3FCQUNBO21CQUNBO0FBRUE7O1VBQ0E7d0JBQ0E7QUFDQTtVQUNBO2tCQUNBO0FBQ0E7QUFHQTs7O0FBQ0E7VUFDQTtBQUdBO0FBbEVBOzs2QkFtRUE7MkJBQ0E7QUFFQTtBQTlNQSxHOzs7Ozs7O0FDM0NBO0FBQ0E7QUFDQSx5QkFBcUo7QUFDcko7QUFDQSx5QkFBK0c7QUFDL0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxrREFBa0Qsd0JBQXdCO0FBQzFFLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL3BhZ2VzL2ZyaWVuZHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQzMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsIlZ1ZS5jb21wb25lbnQoJ2ZyaWVuZC1jb250YWluZXInLCAgcmVxdWlyZSgnLi4vY29tcG9uZW50cy9mcmllbmRzL0ZyaWVuZENvbnRhaW5lci52dWUnKSk7XHJcblxyXG53aW5kb3cudGltZW91dCA9IG51bGw7XHJcblxyXG52YXIgZnJpZW5kQXBwID0gbmV3IFZ1ZSh7XHJcbiAgICBlbDogJyNyZWxhdGlvbnNoaXBzX2NvbnRhaW5lcicsXHJcblxyXG4gICAgZGF0YToge1xyXG4gICAgICAgIGZyaWVuZHMgICAgIDogW10sXHJcbiAgICAgICAgcmVxdWVzdHMgICAgOiBbXSxcclxuICAgICAgICByZWNvbW1lbmRlZCA6IFtdLFxyXG4gICAgICAgIHNlYXJjaGVkICAgIDogW10sXHJcbiAgICAgICAga2V5d29yZCAgICAgOiAnJyxcclxuXHJcbiAgICAgICAgbGFuZyAgICAgICAgOltdXHJcbiAgICB9LFxyXG5cclxuICAgIHdhdGNoOiB7XHJcbiAgICAgICAgLy8gd2hlbmV2ZXIgcXVlc3Rpb24gY2hhbmdlcywgdGhpcyBmdW5jdGlvbiB3aWxsIHJ1blxyXG4gICAgICAgIGtleXdvcmQ6IGZ1bmN0aW9uIChuZXdLZXl3b3JkKSB7XHJcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh3aW5kb3cudGltZW91dCk7XHJcbiAgICAgICAgICAgIHdpbmRvdy50aW1lb3V0ID0gc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZihuZXdLZXl3b3JkID09ICcnKVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoZWQgPSBbXTtcclxuICAgICAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGF4aW9zQVBJdjFcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmdldCgnL3VzZXJzP3NlYXJjaD0nK25ld0tleXdvcmQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGFjY291bnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcF9yZWNvbW1lbmRlZCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcF9zZWFyY2hlZCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yKHZhciBpPTA7IGk8YWNjb3VudHMuZGF0YS5sZW5ndGg7IGkrKylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcC51c2VyID0gYWNjb3VudHMuZGF0YVtpXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wX3JlY29tbWVuZGVkLnB1c2godGVtcCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGFjY291bnRzLmRhdGFbaV0uaWQgIT0gd2luZG93LkxhcmF2ZWwudXNlci5pZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wX3NlYXJjaGVkLnB1c2godGVtcCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoZWQgPSB0ZW1wX3NlYXJjaGVkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCA1MDApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgICxcclxuICAgIG1ldGhvZHM6e1xyXG4gICAgICAgIGdldEZyaWVuZHNEYXRhKCl7XHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIGdldEZyaWVuZCgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBheGlvc0FQSXYxLmdldCgnL2ZyaWVuZHMnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmdW5jdGlvbiBnZXRSZWNvbW1lbmQoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy91c2Vycz90YWtlTm90RnJpZW5kPTQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgYXhpb3MuYWxsKFtcclxuICAgICAgICAgICAgICAgICBnZXRGcmllbmQoKSxcclxuICAgICAgICAgICAgICAgICBnZXRSZWNvbW1lbmQoKVxyXG4gICAgICAgICAgICBdKS50aGVuKGF4aW9zLnNwcmVhZChcclxuICAgICAgICAgICAgICAgIChcclxuICAgICAgICAgICAgICAgICAgICAgZnJpZW5kcyxcclxuICAgICAgICAgICAgICAgICAgICAgcmVjb21tZW5kZWRcclxuICAgICAgICAgICAgICAgICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mcmllbmRzID0gZnJpZW5kcy5kYXRhLmZyaWVuZHM7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLnJlcXVlc3RzID0gZnJpZW5kcy5kYXRhLnJlcXVlc3RzO1xyXG4gICAgICAgICAgICAgICAgZm9yKHZhciBpPTA7IGk8IGZyaWVuZHMuZGF0YS5yZXF1ZXN0cy5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoIWZyaWVuZHMuZGF0YS5yZXF1ZXN0c1tpXS5yZXF1ZXN0b3IpXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlcXVlc3RzLnB1c2goZnJpZW5kcy5kYXRhLnJlcXVlc3RzW2ldKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0ZW1wX3JlY29tbWVuZGVkID0gW107XHJcbiAgICAgICAgICAgICAgICBmb3IodmFyIGk9MDsgaTxyZWNvbW1lbmRlZC5kYXRhLmxlbmd0aDsgaSsrKVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHRlbXAgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB0ZW1wLnVzZXIgPSByZWNvbW1lbmRlZC5kYXRhW2ldO1xyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBfcmVjb21tZW5kZWQucHVzaCh0ZW1wKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5yZWNvbW1lbmRlZCA9IHRlbXBfcmVjb21tZW5kZWQ7XHJcbiAgICAgICAgICAgIH0pKVxyXG4gICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzZWxlY3QodXNlcikge1xyXG4gICAgICAgICAgICB0aGlzLnRvZ2dsZU9mZmNhbnZhcygpO1xyXG5cclxuICAgICAgICAgICAgdmFyIGNoYXQgPSBbXTtcclxuICAgICAgICAgICAgJChcIi5wYW5lbC1jaGF0LWJveFwiKS5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBjaGF0LnB1c2goJCh0aGlzKS5hdHRyKCdpZCcpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZigkLmluQXJyYXkoJ3VzZXInK3VzZXIuaWQsY2hhdCk9PSctMScpXHJcbiAgICAgICAgICAgICAgICBFdmVudC5maXJlKCdjaGF0LWJveC5zaG93JywgdXNlcik7XHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgJCgnLmpzLWF1dG8tc2l6ZScpLnRleHRhcmVhQXV0b1NpemUoKTtcclxuICAgICAgICAgICAgfSwgNTAwMCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdG9nZ2xlT2ZmY2FudmFzKCkge1xyXG4gICAgICAgICAgICAkKHRoaXMuJGVsKS50b2dnbGVDbGFzcygnZXhwYW5kZWQnKTtcclxuICAgICAgICB9LFxyXG4gICAgfSxcclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5nZXRGcmllbmRzRGF0YSgpO1xyXG4gICAgICAgIGF4aW9zLmdldCgnL3RyYW5zbGF0ZS9mcmllbmRzJylcclxuICAgICAgICAgICAgLnRoZW4obGFuZ3VhZ2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sYW5nID0gbGFuZ3VhZ2UuZGF0YS5kYXRhO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH0sXHJcbiAgIFxyXG59KTtcclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvcGFnZXMvZnJpZW5kcy5qcyIsIjx0ZW1wbGF0ZT5cclxuXHQ8ZGl2IGNsYXNzPVwiY29sLWxnLTMgY29sLW1kLTMgY29sLXNtLTYgY29sLXhzLTEyIGZyaWVuZC1jb250YWluZXJcIiB2LWlmPSdub3RCbG9ja2VkJz5cclxuXHRcdDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCJjb2wtbGctMTIgdGV4dC1jZW50ZXJcIj5cclxuXHRcdFx0XHQ8aW1nIGNsYXNzPVwiZnJpZW5kLWltYWdlIGltZy1jaXJjbGUgaW1nLXJlc3BvbnNpdmVcIiA6c3JjPSdkYXRhLnVzZXIuYXZhdGFyJz5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzPSdmcmllbmQtbmFtZSc+PGEgOmhyZWY9XCInL3VzZXJzLycrZGF0YS51c2VyLmlkXCI+e3sgKGRhdGEudXNlci5maXJzdF9uYW1lICtcIiBcIitkYXRhLnVzZXIubGFzdF9uYW1lKSB8IHRydW5jYXRlKDI1KSB9fTwvYT5cclxuXHRcdFx0XHRcdDxicj5cclxuICAgICAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwiZGF0ZVwiPjxzbWFsbCBjbGFzcz0ndGMtZ3JleSc+e3tkYXRhLmRhdGV9fTwvc21hbGw+PC9zbG90PlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PGRpdiBjbGFzcz1cInJvd1wiIHYtaWY9J3R5cGU9PVwicmVxdWVzdFwiJz5cclxuXHRcdFx0PCEtLSBhc2RhZCAtLT5cclxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtNiBjb2wtc20tNiBjb2wtbWQtNiBjb2wtbGctNiB0ZXh0LWNlbnRlclwiPlxyXG5cdFx0XHQgICAgICAgIDxidXR0b24gQGNsaWNrPSdidG5GbihkYXRhLnVzZXIuaWQsXCJyZWplY3RcIiknIGNsYXNzPSdidG4gYnRuLWxpbmsgYnRuLWRhbmdlciBidG4taGFzLWxvYWRpbmcnID57e3RoaXMuJHBhcmVudC5sYW5nWydyZWplY3QnXX19PC9idXR0b24+XHJcblx0XHRcdCAgICA8L2Rpdj5cclxuXHRcdFx0ICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtNiBjb2wtc20tNiBjb2wtbWQtNiBjb2wtbGctNiB0ZXh0LWNlbnRlclwiPlxyXG5cdFx0XHQgICAgICAgIDxidXR0b24gQGNsaWNrPSdidG5GbihkYXRhLnVzZXIuaWQsXCJyZXF1ZXN0XCIpJyBjbGFzcz0nYnRuIGJ0bi1saW5rIGJ0bi1zdWNjZXNzIGJ0bi1oYXMtbG9hZGluZycgPnt7dGhpcy4kcGFyZW50LmxhbmdbJ2FjY2VwdCddfX08L2J1dHRvbj5cclxuXHRcdFx0ICAgIDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PGRpdiBjbGFzcz1cInJvd1wiIHYtaWY9J3R5cGU9PVwiZnJpZW5kXCInPlxyXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC14cy02IGNvbC1zbS02IGNvbC1tZC02IGNvbC1sZy02IHRleHQtY2VudGVyIG1hcmdpblwiPlxyXG5cdFx0XHQgICAgICAgIDxidXR0b24gQGNsaWNrPSdzZWxlY3QoZGF0YS51c2VyKScgY2xhc3M9J2J0biBidG4tbGluayBidG4tZGFuZ2VyIGJ0bi1oYXMtbG9hZGluZycgPnt7dGhpcy4kcGFyZW50LmxhbmdbJ3NlbmQtbWVzc2FnZSddfX08L2J1dHRvbj5cclxuXHRcdFx0ICAgIDwvZGl2PlxyXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC14cy02IGNvbC1zbS02IGNvbC1tZC02IGNvbC1sZy02IHRleHQtY2VudGVyXCI+XHJcblx0XHRcdCAgICAgICAgPGJ1dHRvbiBAY2xpY2s9J2J0bkZuKGRhdGEudXNlci5pZCxcInVuZnJpZW5kXCIpJyBjbGFzcz0nYnRuIGJ0bi1saW5rIGJ0bi1kYW5nZXIgYnRuLWhhcy1sb2FkaW5nJyA+e3t0aGlzLiRwYXJlbnQubGFuZ1sndW5mcmllbmQnXX19PC9idXR0b24+XHJcblx0XHRcdCAgICA8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCJyb3dcIiB2LWlmPSd0eXBlPT1cInJlY29tbWVuZGVkXCInPlxyXG5cdFx0XHQgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMiB0ZXh0LWNlbnRlclwiPlxyXG5cdFx0XHQgICAgICAgIDxidXR0b24gQGNsaWNrPSdidG5GbihkYXRhLnVzZXIuaWQsXCJhZGRcIiknIGNsYXNzPSdidG4gYnRuLWxpbmsgYnRuLXN1Y2Nlc3MgYnRuLWhhcy1sb2FkaW5nJyBvbmNsaWNrPVwidGhpcy5pbm5lclRleHQ9J0NhbmNlbCBSZXF1ZXN0JzsgdGhpcy5zdHlsZS5jb2xvcj0ncmVkJztcIj57e3RoaXMuJHBhcmVudC5sYW5nWydhZGQnXX19PC9idXR0b24+XHJcblx0XHRcdCAgICA8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCJyb3dcIiB2LWlmPSd0eXBlID09IFwic2VhcmNoZWRcIic+XHJcblx0XHRcdFx0PGRpdiBjbGFzcz1cImNvbC14cy0xMiB0ZXh0LWNlbnRlclwiPlxyXG5cdFx0XHQgICAgICAgIDxidXR0b24gQGNsaWNrPSdidG5GbihkYXRhLnVzZXIuaWQsYnV0dG9uLnR5cGUpJyBjbGFzcz0nYnRuIGJ0bi1saW5rIGJ0bi1oYXMtbG9hZGluZycgOmNsYXNzPSdidXR0b24uc3R5bGUnPnt7YnV0dG9uLmNvbnRlbnR9fTwvYnV0dG9uPlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdDwvZGl2PlxyXG5cdDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuXHRleHBvcnQgZGVmYXVsdHtcclxuXHRcdGRhdGEoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcblx0XHRcdFx0bm90QmxvY2tlZDp0cnVlXHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHRcdHByb3BzOltcclxuXHRcdFx0J2RhdGEnLFxyXG5cdFx0XHQndHlwZSdcclxuXHRcdF0sXHJcblx0XHRtZXRob2RzOntcclxuXHRcdFx0YnRuRm4oaWQsIHR5cGUpXHJcblx0XHRcdHtcclxuXHRcdFx0XHRzd2l0Y2godHlwZSlcclxuXHRcdFx0XHR7XHJcblx0XHRcdFx0XHRjYXNlICdhZGQnOlxyXG5cdFx0XHRcdFx0XHRheGlvc0FQSXYxXHJcblx0XHRcdFx0XHRcdFx0LnBvc3QoJy9mcmllbmRzJyx7XHJcblx0XHRcdFx0XHRcdFx0XHR1c2VyX2lkMjppZFxyXG5cdFx0XHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0XHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdFx0aWYocmVzcG9uc2Uuc3RhdHVzID09IDIwMSlcclxuXHRcdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0Zm9yKHZhciBqPTA7IGo8dGhpcy4kcm9vdC5yZWNvbW1lbmRlZC5sZW5ndGg7IGorKylcclxuXHRcdFx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGlmKHRoaXMuJHJvb3QucmVjb21tZW5kZWRbal0udXNlci5pZCA9PSBpZClcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQvL3JlbW92ZVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0dGhpcy4kcm9vdC5yZWNvbW1lbmRlZC5zcGxpY2UoaiwxKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuJHJvb3QuZ2V0RnJpZW5kc0RhdGEoKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MoJycsJ0ZyaWVuZCByZXF1ZXN0IHNlbnQhJyk7XHJcblx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSAncmVxdWVzdCc6XHJcblx0XHRcdFx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHRcdFx0XHQucG9zdCgnL2ZyaWVuZC9maW5kJyx7XHJcblx0XHRcdFx0XHRcdFx0XHR1c2VyX2lkOkxhcmF2ZWwudXNlci5pZCxcclxuXHRcdFx0XHRcdFx0XHRcdHVzZXJfaWQyOmlkXHJcblx0XHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XHJcblx0XHRcdFx0XHRcdFx0XHRpZihyZXNwb25zZS5zdGF0dXMgPT0gMjAwKVxyXG5cdFx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRheGlvc0FQSXYxXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0LnB1dCgnL2ZyaWVuZHMvJytyZXNwb25zZS5kYXRhLHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHN0YXR1czonRnJpZW5kJ1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0LnRoZW4oZGF0YSA9PiB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpZihkYXRhLnN0YXR1cyA9PSAyMDApXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGZvcih2YXIgaj0wOyBqPHRoaXMuJHJvb3QucmVxdWVzdHMubGVuZ3RoOyBqKyspXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpZih0aGlzLiRyb290LnJlcXVlc3RzW2pdLnVzZXIuaWQgPT0gaWQpXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0dGhpcy4kcm9vdC5mcmllbmRzLnB1c2godGhpcy4kcm9vdC5yZXF1ZXN0c1tqXSk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQvL3JlbW92ZVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0dGhpcy4kcm9vdC5yZXF1ZXN0cy5zcGxpY2UoaiwxKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0dG9hc3RyLnN1Y2Nlc3MoJycsJ0ZyaWVuZCBBY2NlcHRlZCEnKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdGNhc2UgJ3JlamVjdCc6XHJcblx0XHRcdFx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHRcdFx0XHQucG9zdCgnL2ZyaWVuZC9maW5kJyx7XHJcblx0XHRcdFx0XHRcdFx0XHR1c2VyX2lkOkxhcmF2ZWwudXNlci5pZCxcclxuXHRcdFx0XHRcdFx0XHRcdHVzZXJfaWQyOmlkXHJcblx0XHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XHJcblx0XHRcdFx0XHRcdFx0XHRpZihyZXNwb25zZS5zdGF0dXMgPT0gMjAwKVxyXG5cdFx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRheGlvc0FQSXYxXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0LmRlbGV0ZSgnL2ZyaWVuZHMvJytyZXNwb25zZS5kYXRhKVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdC50aGVuKGRhdGEgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aWYoZGF0YS5zdGF0dXMgPT0gMjAwKVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRmb3IodmFyIGo9MDsgajx0aGlzLiRyb290LnJlcXVlc3RzLmxlbmd0aDsgaisrKVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0aWYodGhpcy4kcm9vdC5yZXF1ZXN0c1tqXS51c2VyLmlkID09IGlkKVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdC8vcmVtb3ZlXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR0aGlzLiRyb290LnJlcXVlc3RzLnNwbGljZShqLDEpO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR0b2FzdHIuc3VjY2VzcygnJywnUmVqZWN0ZWQhJyk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQuY2F0Y2goJC5ub29wKTtcclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRjYXNlICd1bmZyaWVuZCc6XHJcblx0XHRcdFx0XHRjYXNlICdjYW5jZWxfcmVxdWVzdCc6XHJcblx0XHRcdFx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHRcdFx0XHQucG9zdCgnL2ZyaWVuZC9maW5kJyx7XHJcblx0XHRcdFx0XHRcdFx0XHR1c2VyX2lkOkxhcmF2ZWwudXNlci5pZCxcclxuXHRcdFx0XHRcdFx0XHRcdHVzZXJfaWQyOmlkXHJcblx0XHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XHJcblx0XHRcdFx0XHRcdFx0XHRpZihyZXNwb25zZS5zdGF0dXMgPT0gMjAwKVxyXG5cdFx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRheGlvc0FQSXYxXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0LmRlbGV0ZSgnL2ZyaWVuZHMvJytyZXNwb25zZS5kYXRhKVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdC50aGVuKGRhdGEgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aWYoZGF0YS5zdGF0dXMgPT0gMjAwKVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRmb3IodmFyIGo9MDsgajx0aGlzLiRyb290LmZyaWVuZHMubGVuZ3RoOyBqKyspXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpZih0aGlzLiRyb290LmZyaWVuZHNbal0udXNlci5pZCA9PSBpZClcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQvL3JlbW92ZVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0dGhpcy4kcm9vdC5mcmllbmRzLnNwbGljZShqLDEpO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR0b2FzdHIuc3VjY2VzcygnJywnU3VjY2Vzc2Z1bCEnKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sXHJcblxyXG5cdFx0XHRzZWxlY3QodXNlcil7XHJcblx0XHRcdFx0dGhpcy4kcGFyZW50LnNlbGVjdCh1c2VyKTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHRjb21wdXRlZDoge1xyXG5cdFx0XHRidXR0b246ZnVuY3Rpb24oKXtcclxuXHRcdFx0XHR2YXIgZGF0YSA9IHt9O1xyXG5cdFx0XHRcdGRhdGEudHlwZSA9ICdhZGQnO1xyXG5cdFx0XHRcdGRhdGEuY29udGVudCA9ICdBZGQgRnJpZW5kJztcclxuXHRcdFx0XHRkYXRhLnN0eWxlID0gJ2J0bi1zdWNjZXNzJztcclxuXHJcblx0XHRcdFx0aWYodGhpcy5kYXRhLnVzZXIuaW5pdGlhdG9yKVxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdHN3aXRjaCh0aGlzLmRhdGEudXNlci5yZWxfc3RhdHVzKVxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRjYXNlICdSZXF1ZXN0JzpcclxuXHRcdFx0XHRcdFx0XHRkYXRhLnR5cGUgPSAnY2FuY2VsX3JlcXVlc3QnO1xyXG5cdFx0XHRcdFx0XHRcdGRhdGEuY29udGVudCA9ICdDYW5jZWwgUmVxdWVzdCc7XHJcblx0XHRcdFx0XHRcdFx0ZGF0YS5zdHlsZSA9ICdidG4tZGFuZ2VyJztcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHJcblx0XHRcdFx0XHRcdGNhc2UgJ0ZyaWVuZCc6XHJcblx0XHRcdFx0XHRcdFx0ZGF0YS50eXBlID0gJ3VuZnJpZW5kJztcclxuXHRcdFx0XHRcdFx0XHRkYXRhLmNvbnRlbnQgPSAnVW5mcmllbmQnO1xyXG5cdFx0XHRcdFx0XHRcdGRhdGEuc3R5bGUgPSAnYnRuLWRhbmdlcic7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0XHRcdFx0XHRjYXNlICdCbG9ja2VkJzpcclxuXHRcdFx0XHRcdFx0XHRkYXRhLnR5cGUgPSAnY2FuY2VsX3JlcXVlc3QnO1xyXG5cdFx0XHRcdFx0XHRcdGRhdGEuY29udGVudCA9ICdVbmJsb2NrJztcclxuXHRcdFx0XHRcdFx0XHRkYXRhLnN0eWxlID0gJ2J0bi1pbmZvJztcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHJcblx0XHRcdFx0XHRcdGNhc2UgJ1Jlc3RyaWN0ZWQnOlxyXG5cdFx0XHRcdFx0XHRcdGRhdGEudHlwZSA9ICdjYW5jZWxfcmVxdWVzdCc7XHJcblx0XHRcdFx0XHRcdFx0ZGF0YS5jb250ZW50ID0gJ1VucmVzdHJpY3QnO1xyXG5cdFx0XHRcdFx0XHRcdGRhdGEuc3R5bGUgPSAnYnRuLWluZm8nO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdFx0XHRcdFx0ZGVmYXVsdDpcclxuXHJcblx0XHRcdFx0XHR9XHJcbiBcclxuXHRcdFx0XHR9ZWxzZVxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdHN3aXRjaCh0aGlzLmRhdGEudXNlci5yZWxfc3RhdHVzKVxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRjYXNlICdSZXF1ZXN0JzpcclxuXHRcdFx0XHRcdFx0XHR0aGlzLnR5cGUgPSAncmVxdWVzdCc7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0XHRcdFx0XHRjYXNlICdGcmllbmQnOlxyXG5cdFx0XHRcdFx0XHRcdGRhdGEudHlwZSA9ICd1bmZyaWVuZCc7XHJcblx0XHRcdFx0XHRcdFx0ZGF0YS5jb250ZW50ID0gJ1VuZnJpZW5kJztcclxuXHRcdFx0XHRcdFx0XHRkYXRhLnN0eWxlID0gJ2J0bi1kYW5nZXInO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdFx0XHRcdFx0Y2FzZSAnQmxvY2tlZCc6XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5ub3RCbG9ja2VkID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdGNhc2UgJ1Jlc3RyaWN0ZWQnOlxyXG5cdFx0XHRcdFx0XHRcdHRoaXMudHlwZSA9ICdyZXN0cmljdGVkJztcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0ZGVmYXVsdDpcclxuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHJldHVybiBkYXRhO1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cclxuXHRcdGNyZWF0ZWQoKXtcclxuXHRcdFx0Y29uc29sZS5sb2codGhpcy4kcGFyZW50LmxhbmcpXHJcblx0XHR9LCBcclxuXHJcblx0fVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRnJpZW5kQ29udGFpbmVyLnZ1ZT8wZDE2MGEwNiIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0ZyaWVuZENvbnRhaW5lci52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTVmZDhiNDJkXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0ZyaWVuZENvbnRhaW5lci52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxcZnJpZW5kc1xcXFxGcmllbmRDb250YWluZXIudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRnJpZW5kQ29udGFpbmVyLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi01ZmQ4YjQyZFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTVmZDhiNDJkXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9mcmllbmRzL0ZyaWVuZENvbnRhaW5lci52dWVcbi8vIG1vZHVsZSBpZCA9IDMzNFxuLy8gbW9kdWxlIGNodW5rcyA9IDIzIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIChfdm0ubm90QmxvY2tlZCkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0zIGNvbC1tZC0zIGNvbC1zbS02IGNvbC14cy0xMiBmcmllbmQtY29udGFpbmVyXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2FyZFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMiB0ZXh0LWNlbnRlclwiXG4gIH0sIFtfYygnaW1nJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZyaWVuZC1pbWFnZSBpbWctY2lyY2xlIGltZy1yZXNwb25zaXZlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5kYXRhLnVzZXIuYXZhdGFyXG4gICAgfVxuICB9KSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJmcmllbmQtbmFtZVwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6ICcvdXNlcnMvJyArIF92bS5kYXRhLnVzZXIuaWRcbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcInRydW5jYXRlXCIpKChfdm0uZGF0YS51c2VyLmZpcnN0X25hbWUgKyBcIiBcIiArIF92bS5kYXRhLnVzZXIubGFzdF9uYW1lKSwgMjUpKSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2JyJyksIF92bS5fdihcIiBcIiksIF92bS5fdChcImRhdGVcIiwgW19jKCdzbWFsbCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0Yy1ncmV5XCJcbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLmRhdGEuZGF0ZSkpXSldKV0sIDIpXSksIF92bS5fdihcIiBcIiksIChfdm0udHlwZSA9PSBcInJlcXVlc3RcIikgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy02IGNvbC1zbS02IGNvbC1tZC02IGNvbC1sZy02IHRleHQtY2VudGVyXCJcbiAgfSwgW19jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1saW5rIGJ0bi1kYW5nZXIgYnRuLWhhcy1sb2FkaW5nXCIsXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5idG5Gbihfdm0uZGF0YS51c2VyLmlkLCBcInJlamVjdFwiKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmdbJ3JlamVjdCddKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy02IGNvbC1zbS02IGNvbC1tZC02IGNvbC1sZy02IHRleHQtY2VudGVyXCJcbiAgfSwgW19jKCdidXR0b24nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1saW5rIGJ0bi1zdWNjZXNzIGJ0bi1oYXMtbG9hZGluZ1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uYnRuRm4oX3ZtLmRhdGEudXNlci5pZCwgXCJyZXF1ZXN0XCIpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZ1snYWNjZXB0J10pKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLnR5cGUgPT0gXCJmcmllbmRcIikgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy02IGNvbC1zbS02IGNvbC1tZC02IGNvbC1sZy02IHRleHQtY2VudGVyIG1hcmdpblwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tbGluayBidG4tZGFuZ2VyIGJ0bi1oYXMtbG9hZGluZ1wiLFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uc2VsZWN0KF92bS5kYXRhLnVzZXIpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KF92bS5fcyh0aGlzLiRwYXJlbnQubGFuZ1snc2VuZC1tZXNzYWdlJ10pKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTYgY29sLXNtLTYgY29sLW1kLTYgY29sLWxnLTYgdGV4dC1jZW50ZXJcIlxuICB9LCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWxpbmsgYnRuLWRhbmdlciBidG4taGFzLWxvYWRpbmdcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmJ0bkZuKF92bS5kYXRhLnVzZXIuaWQsIFwidW5mcmllbmRcIilcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKHRoaXMuJHBhcmVudC5sYW5nWyd1bmZyaWVuZCddKSldKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS50eXBlID09IFwicmVjb21tZW5kZWRcIikgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMiB0ZXh0LWNlbnRlclwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tbGluayBidG4tc3VjY2VzcyBidG4taGFzLWxvYWRpbmdcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJvbmNsaWNrXCI6IFwidGhpcy5pbm5lclRleHQ9J0NhbmNlbCBSZXF1ZXN0JzsgdGhpcy5zdHlsZS5jb2xvcj0ncmVkJztcIlxuICAgIH0sXG4gICAgb246IHtcbiAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgIF92bS5idG5Gbihfdm0uZGF0YS51c2VyLmlkLCBcImFkZFwiKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3ModGhpcy4kcGFyZW50LmxhbmdbJ2FkZCddKSldKV0pXSkgOiBfdm0uX2UoKSwgX3ZtLl92KFwiIFwiKSwgKF92bS50eXBlID09IFwic2VhcmNoZWRcIikgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMiB0ZXh0LWNlbnRlclwiXG4gIH0sIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tbGluayBidG4taGFzLWxvYWRpbmdcIixcbiAgICBjbGFzczogX3ZtLmJ1dHRvbi5zdHlsZSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmJ0bkZuKF92bS5kYXRhLnVzZXIuaWQsIF92bS5idXR0b24udHlwZSlcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5idXR0b24uY29udGVudCkpXSldKV0pIDogX3ZtLl9lKCldKV0pIDogX3ZtLl9lKClcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNWZkOGI0MmRcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi01ZmQ4YjQyZFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2ZyaWVuZHMvRnJpZW5kQ29udGFpbmVyLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzg3XG4vLyBtb2R1bGUgY2h1bmtzID0gMjMiXSwic291cmNlUm9vdCI6IiJ9