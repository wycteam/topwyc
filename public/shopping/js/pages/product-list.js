/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 438);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 188:
/***/ (function(module, exports, __webpack_require__) {

Vue.component('product-item', __webpack_require__(36));

Vue.filter('currency', function (value) {
	return numberWithCommas(value.toFixed(2));
});

Vue.filter('round', function (value) {
	return roundOff(value);
});

var app = new Vue({
	el: '#product-list',
	data: {
		pagination: {
			total: 0,
			per_page: 7,
			from: 1,
			to: 0,
			current_page: 1
		},
		offset: 4, // left and right padding from the pagination <span>,just change it to see effects
		items: [],
		selected: [],
		pName: true,
		lang: [],
		keyword: '',
		langmode: null
	},
	created: function created() {
		var _this = this;

		var m = $("meta[name=locale-lang]");
		this.langmode = m.attr("content");
		//check if there is a search query
		if (window.location.search.substr().indexOf('search') > -1) {
			var getTranslation = function getTranslation() {
				return axios.get('/translate/cart');
			};

			var getProducts = function getProducts() {
				return axiosAPIv1.post('/search/product', {
					keyword: getURLParameter('search'),
					sort: getURLParameter('price'),
					prod_name: getURLParameter('prod_name'),
					minPrice: getURLParameter('min'),
					maxPrice: getURLParameter('max'),
					brands: getURLParameter('brands')
				});
			};

			axios.all([getTranslation(), getProducts()]).then(axios.spread(function (translation, response) {

				setPriceRangeMax(response.data.maxPrice);

				_this.lang = translation.data.data;

				if (response.data) {
					$('.loading').remove();
					_this.items = response.data.data.data;
				}

				if (response.data.pagination.total > 20) {
					_this.pagination = response.data.pagination;
				}

				//show no found result 
				if (_this.items == '') {
					$('.cd-fail-message').show();
				}

				if (response.data.queryString != '') {
					$('#resultMessage').removeClass('hide');
					$('#resultMessage #queryString').text('"' + response.data.queryString + '"');
				} else {
					$('#resultMessage').addClass('hide');
					$('#resultMessage #queryString').text('');
				}
			})).catch($.noop);

			var price = getURLParameter('price');

			if (price) {
				if (price === 'desc') $('#radio2').attr('checked', 'checked');

				if (price === 'asc') $('#radio3').attr('checked', 'checked');
			} else {
				$('#radio1').attr('checked', 'checked');
			}
		}
	},

	computed: {
		isActived: function isActived() {
			return this.pagination.current_page;
		},
		pagesNumber: function pagesNumber() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + this.offset * 2;
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
		}
	},
	methods: {
		selectItem: function selectItem(id) {
			this.selected.push(id);
		},
		sortBy: function sortBy(val) {
			var url = removeURLParam(window.location.href, 'price');
			if (val == "desc") {
				//var url = window.location.href.replace('&price=asc','')
				window.location = url + '&price=desc';
			}

			if (val == "asc") {
				//var url = window.location.href.replace('&price=desc','')
				window.location = url + '&price=asc';
			}

			if (val == "rel") {
				var url = window.location.href;
				window.location = this.removeURLParameter(url, 'price');
			}
		},
		sortByBrand: function sortByBrand(e) {
			var url = window.location.href;
			var brands = getURLParameter('brands');

			if (brands == undefined) {
				window.location = url + '&brands=' + e.target.value;
			} else {
				if (e.target.checked) {
					var url = removeURLParam(url, 'brands');

					window.location = url + '&brands=' + brands + '--' + e.target.value;
				} else {
					if (url.indexOf('--' + e.target.value) != -1) {
						var url = url.replace('--' + e.target.value, "");
					} else {
						var url = url.replace(e.target.value, "");
					}

					window.location = url;
				}
			}
		},
		removeURLParameter: function removeURLParameter(url, parameter) {
			//prefer to use l.search if you have a location/link object
			var urlparts = url.split('?');
			if (urlparts.length >= 2) {

				var prefix = encodeURIComponent(parameter) + '=';
				var pars = urlparts[1].split(/[&;]/g);

				//reverse iteration as may be destructive
				for (var i = pars.length; i-- > 0;) {
					//idiom for string.startsWith
					if (pars[i].lastIndexOf(prefix, 0) !== -1) {
						pars.splice(i, 1);
					}
				}

				url = urlparts[0] + '?' + pars.join('&');
				return url;
			} else {
				return url;
			}
		},

		handleEnter: function handleEnter(e) {
			if (e.keyCode == 13) {
				var url = removeURLParam(window.location.href, 'prod_name');
				window.location = url + '&prod_name=' + this.keyword;
			}
		},
		addToCart: function addToCart(id, name) {
			var _this2 = this;

			axios.get(location.origin + '/cart/add', {
				params: {
					prod_id: id,
					qty: 1
				}
			}).then(function (result) {
				$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
				if (result.data.error_msg) {
					if (result.data.error_msg == 'Redirect') window.location = '/product/' + result.data.slug;else toastr.error(result.data.error_msg, _this2.lang['not-added']);
				} else {
					toastr.success(_this2.lang['added-cart'], name);
				}
			});
		},
		addToCompare: function addToCompare(id, name) {
			var _this3 = this;

			if (Laravel.isAuthenticated === true) {
				axiosAPIv1.get('/addtocompare/' + id).then(function (result) {
					if (result.data.response == 1) {
						var comp = parseInt($("#comp-count").text()) + 1;
						$("#comp-count").text(comp).show();
						toastr.success(_this3.lang['added-comparison'], name);
					} else if (result.data.response == 3) {
						toastr.error(_this3.lang['full']);
					} else {
						toastr.info(_this3.lang['already-compare'], name);
					}
				});
			} else {
				toastr.error(this.lang['login-compare']);
			}
		},
		addToFavorites: function addToFavorites(id, name) {
			var _this4 = this;

			if (Laravel.isAuthenticated === true) {
				axiosAPIv1.get('/addtofavorites/' + id).then(function (result) {
					if (result.data.response == 1) {
						var fav = parseInt($("#fav-count").text()) + 1;
						$("#fav-count").text(fav).show();
						toastr.success(_this4.lang['added-favorites'], name);
					} else if (result.data.response == 2) {
						toastr.error(result.data.error_msg, _this4.lang['not-added-wishlist']);
					} else {
						toastr.info(_this4.lang['already-favorites'], name);
					}
				});
			} else {
				toastr.error(this.lang['login-favorites']);
			}
		},
		fetchItems: function fetchItems(page) {
			var _this5 = this;

			axiosAPIv1.post('/search/product', {
				page: page,
				keyword: getURLParameter('search'),
				sort: getURLParameter('price'),
				prod_name: getURLParameter('prod_name'),
				minPrice: getURLParameter('min'),
				maxPrice: getURLParameter('max'),
				brands: getURLParameter('brands')
			}).then(function (result) {
				setPriceRangeMax(result.data.maxPrice);

				_this5.items = result.data.data.data;
				_this5.pagination = result.data.pagination;
			});
		},
		changePage: function changePage(page) {
			this.pagination.current_page = page;
			this.fetchItems(page);
		},
		showPagination: function showPagination() {
			$('#pagination').show();
		},
		hidePagination: function hidePagination() {
			$('#pagination').hide();
		}
	},
	watch: {
		item: function item(key) {
			$('.cd-gallery ul').mixItUp({
				controls: {
					enable: false
				},
				callbacks: {
					onMixStart: function onMixStart() {
						$('.cd-fail-message').fadeOut(200);
					},
					onMixFail: function onMixFail() {
						$('.cd-fail-message').fadeIn(200);
					}
				}
			});
		}
	},
	updated: function updated() {
		// $('.cd-gallery ul').mixItUp({
		// 	    controls: {
		// 	    	enable: false
		// 	    },
		// 	    callbacks: {
		// 	    	onMixStart: function(){
		// 	    		$('.cd-fail-message').hide();
		// 	    	},
		// 	      	onMixFail: function(){
		// 	      		$('.cd-fail-message').show();
		// 	    	}
		// 	    }
		// 	});
	}
});

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
	return Math.round(v);
}

function getURLParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

function removeURLParam(url, param) {
	var urlparts = url.split('?');
	if (urlparts.length >= 2) {
		var prefix = encodeURIComponent(param) + '=';
		var pars = urlparts[1].split(/[&;]/g);
		for (var i = pars.length; i-- > 0;) {
			if (pars[i].indexOf(prefix, 0) == 0) pars.splice(i, 1);
		}if (pars.length > 0) return urlparts[0] + '?' + pars.join('&');else return urlparts[0];
	} else return url;
}

function setPriceRangeMax(maxPrice) {
	window.slider.update({
		min: 0,
		from: 0,

		max: maxPrice,
		to: maxPrice
	});
}

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['item'],
  methods: {
    select: function select(id) {
      this.$parent.selectItem(id);
    },
    addToCart: function addToCart() {
      this.$parent.addToCart(this.item.id, this.item.name);
    },
    addToCompare: function addToCompare() {
      this.$parent.addToCompare(this.item.id, this.item.name);
    },
    addToFavorites: function addToFavorites() {
      this.$parent.addToFavorites(this.item.id, this.item.name);
    }
  },
  computed: {
    price: function price() {
      if (this.item.fee_included == 1) return parseFloat(this.item.price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.price;
    },
    saleprice: function saleprice() {
      if (this.item.fee_included == 1) return parseFloat(this.item.sale_price) + parseFloat(this.item.shipping_fee) + parseFloat(this.item.charge) + parseFloat(this.item.vat);else return this.item.sale_price;
    },
    namelength: function namelength() {
      var name = this.item.cn_name ? this.item.cn_name : this.item.name;
      if (this.item.name.length > 45) {
        if (this.$parent.langmode == "cn") {
          return name.substr(0, 45) + "...";
        }
        return this.item.name.substr(0, 45) + "...";
      } else {
        if (this.$parent.langmode == "cn") {
          return name;
        }
        return this.item.name;
      }
    },
    starRating: function starRating() {
      return "width:" + Math.round(this.item.rate / this.item.count * 20) + "%";
    },
    productRating: function productRating() {
      return "width:" + this.item.product_rating + "%";
    }
  }
});

/***/ }),

/***/ 36:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(20),
  /* template */
  __webpack_require__(42),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\product\\Product.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Product.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-778f75f3", Component.options)
  } else {
    hotAPI.reload("data-v-778f75f3", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mix col-sm-3"
  }, [_c('div', {
    staticClass: "box-product-outer"
  }, [_c('div', {
    staticClass: "box-product"
  }, [_c('div', {
    staticClass: "img-wrapper"
  }, [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_c('img', {
    attrs: {
      "alt": "Product",
      "src": '/shopping/images/product/' + _vm.item.id + '/primary.jpg',
      "onError": "this.src = '/images/avatar/default-product.jpg'"
    }
  })]), _vm._v(" "), (_vm.item.sale_price) ? _c('div', {
    staticClass: "tags"
  }, [_c('span', {
    staticClass: "label-tags"
  }, [_c('span', {
    staticClass: "label label-danger arrowed"
  }, [_vm._v(_vm._s(_vm.item.discount) + "%")])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "option"
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "data-placement": "top",
      "title": "Add to Cart",
      "data-original-title": "Add to Cart"
    },
    on: {
      "click": function($event) {
        _vm.addToCart()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-shopping-cart"
  })]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Compare"
    }
  }, [_c('i', {
    staticClass: "fa fa-align-left",
    on: {
      "click": function($event) {
        _vm.addToCompare()
      }
    }
  })]), _vm._v(" "), _c('a', {
    staticClass: "wishlist",
    attrs: {
      "href": "javascript:void(0)",
      "data-toggle": "tooltip",
      "title": "Add to Wishlist"
    },
    on: {
      "click": function($event) {
        _vm.addToFavorites()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-heart"
  })])])]), _vm._v(" "), _c('h6', [_c('a', {
    attrs: {
      "href": '/product/' + _vm.item.slug
    }
  }, [_vm._v(_vm._s(_vm.namelength))])]), _vm._v(" "), _c('div', {
    staticClass: "price"
  }, [_c('div', [(_vm.item.sale_price) ? _c('span', [_vm._v(_vm._s(_vm._f("currency")(parseFloat("0" + _vm.saleprice))))]) : _vm._e(), _vm._v(" "), _c('span', {
    class: _vm.item.sale_price ? 'price-old' : ''
  }, [_vm._v(_vm._s(_vm._f("currency")(parseFloat("0" + _vm.price))))])])]), _vm._v(" "), _c('div', {
    staticClass: "rating-block"
  }, [_c('div', {
    staticClass: "star-ratings-sprite-small"
  }, [_c('span', {
    staticClass: "star-ratings-sprite-small-rating",
    style: (_vm.productRating)
  })])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-778f75f3", module.exports)
  }
}

/***/ }),

/***/ 438:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(188);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioqKioqKioqIiwid2VicGFjazovLy8uL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanM/ZDRmMyoqKioqKioqKioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3Byb2R1Y3QtbGlzdC5qcyIsIndlYnBhY2s6Ly8vUHJvZHVjdC52dWU/N2E1YSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvcHJvZHVjdC9Qcm9kdWN0LnZ1ZT85ZWY1Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L1Byb2R1Y3QudnVlP2UxNzUqIl0sIm5hbWVzIjpbIlZ1ZSIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJmaWx0ZXIiLCJ2YWx1ZSIsIm51bWJlcldpdGhDb21tYXMiLCJ0b0ZpeGVkIiwicm91bmRPZmYiLCJhcHAiLCJlbCIsImRhdGEiLCJwYWdpbmF0aW9uIiwidG90YWwiLCJwZXJfcGFnZSIsImZyb20iLCJ0byIsImN1cnJlbnRfcGFnZSIsIm9mZnNldCIsIml0ZW1zIiwic2VsZWN0ZWQiLCJwTmFtZSIsImxhbmciLCJrZXl3b3JkIiwibGFuZ21vZGUiLCJjcmVhdGVkIiwibSIsIiQiLCJhdHRyIiwid2luZG93IiwibG9jYXRpb24iLCJzZWFyY2giLCJzdWJzdHIiLCJpbmRleE9mIiwiZ2V0VHJhbnNsYXRpb24iLCJheGlvcyIsImdldCIsImdldFByb2R1Y3RzIiwiYXhpb3NBUEl2MSIsInBvc3QiLCJnZXRVUkxQYXJhbWV0ZXIiLCJzb3J0IiwicHJvZF9uYW1lIiwibWluUHJpY2UiLCJtYXhQcmljZSIsImJyYW5kcyIsImFsbCIsInRoZW4iLCJzcHJlYWQiLCJ0cmFuc2xhdGlvbiIsInJlc3BvbnNlIiwic2V0UHJpY2VSYW5nZU1heCIsInJlbW92ZSIsInNob3ciLCJxdWVyeVN0cmluZyIsInJlbW92ZUNsYXNzIiwidGV4dCIsImFkZENsYXNzIiwiY2F0Y2giLCJub29wIiwicHJpY2UiLCJjb21wdXRlZCIsImlzQWN0aXZlZCIsInBhZ2VzTnVtYmVyIiwibGFzdF9wYWdlIiwicGFnZXNBcnJheSIsInB1c2giLCJtZXRob2RzIiwic2VsZWN0SXRlbSIsImlkIiwic29ydEJ5IiwidmFsIiwidXJsIiwicmVtb3ZlVVJMUGFyYW0iLCJocmVmIiwicmVtb3ZlVVJMUGFyYW1ldGVyIiwic29ydEJ5QnJhbmQiLCJlIiwidW5kZWZpbmVkIiwidGFyZ2V0IiwiY2hlY2tlZCIsInJlcGxhY2UiLCJwYXJhbWV0ZXIiLCJ1cmxwYXJ0cyIsInNwbGl0IiwibGVuZ3RoIiwicHJlZml4IiwiZW5jb2RlVVJJQ29tcG9uZW50IiwicGFycyIsImkiLCJsYXN0SW5kZXhPZiIsInNwbGljZSIsImpvaW4iLCJoYW5kbGVFbnRlciIsImtleUNvZGUiLCJhZGRUb0NhcnQiLCJuYW1lIiwib3JpZ2luIiwicGFyYW1zIiwicHJvZF9pZCIsInF0eSIsInJlc3VsdCIsImNhcnRfaXRlbV9jb3VudCIsImVycm9yX21zZyIsInNsdWciLCJ0b2FzdHIiLCJlcnJvciIsInN1Y2Nlc3MiLCJhZGRUb0NvbXBhcmUiLCJMYXJhdmVsIiwiaXNBdXRoZW50aWNhdGVkIiwiY29tcCIsInBhcnNlSW50IiwiaW5mbyIsImFkZFRvRmF2b3JpdGVzIiwiZmF2IiwiZmV0Y2hJdGVtcyIsInBhZ2UiLCJjaGFuZ2VQYWdlIiwic2hvd1BhZ2luYXRpb24iLCJoaWRlUGFnaW5hdGlvbiIsImhpZGUiLCJ3YXRjaCIsIml0ZW0iLCJrZXkiLCJtaXhJdFVwIiwiY29udHJvbHMiLCJlbmFibGUiLCJjYWxsYmFja3MiLCJvbk1peFN0YXJ0IiwiZmFkZU91dCIsIm9uTWl4RmFpbCIsImZhZGVJbiIsInVwZGF0ZWQiLCJ4IiwidG9TdHJpbmciLCJ2IiwiTWF0aCIsInJvdW5kIiwic1BhcmFtIiwic1BhZ2VVUkwiLCJzdWJzdHJpbmciLCJzVVJMVmFyaWFibGVzIiwic1BhcmFtZXRlck5hbWUiLCJwYXJhbSIsInNsaWRlciIsInVwZGF0ZSIsIm1pbiIsIm1heCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDbERBQSxJQUFJQyxTQUFKLENBQWMsY0FBZCxFQUErQixtQkFBQUMsQ0FBUSxFQUFSLENBQS9COztBQUVBRixJQUFJRyxNQUFKLENBQVcsVUFBWCxFQUF1QixVQUFVQyxLQUFWLEVBQWlCO0FBQ3BDLFFBQVFDLGlCQUFpQkQsTUFBTUUsT0FBTixDQUFjLENBQWQsQ0FBakIsQ0FBUjtBQUNILENBRkQ7O0FBSUFOLElBQUlHLE1BQUosQ0FBVyxPQUFYLEVBQW9CLFVBQVVDLEtBQVYsRUFBaUI7QUFDakMsUUFBUUcsU0FBU0gsS0FBVCxDQUFSO0FBQ0gsQ0FGRDs7QUFJQSxJQUFJSSxNQUFNLElBQUlSLEdBQUosQ0FBUTtBQUNqQlMsS0FBSSxlQURhO0FBRWpCQyxPQUFNO0FBQ0NDLGNBQVk7QUFDUkMsVUFBTyxDQURDO0FBRVJDLGFBQVUsQ0FGRjtBQUdSQyxTQUFNLENBSEU7QUFJUkMsT0FBSSxDQUpJO0FBS1JDLGlCQUFjO0FBTE4sR0FEYjtBQVFDQyxVQUFRLENBUlQsRUFRVztBQUNoQkMsU0FBTSxFQVREO0FBVUxDLFlBQVMsRUFWSjtBQVdMQyxTQUFNLElBWEQ7QUFZTEMsUUFBSyxFQVpBO0FBYUxDLFdBQVEsRUFiSDtBQWNMQyxZQUFTO0FBZEosRUFGVztBQWtCakJDLFFBbEJpQixxQkFrQlI7QUFBQTs7QUFDUixNQUFJQyxJQUFJQyxFQUFFLHdCQUFGLENBQVI7QUFDQSxPQUFLSCxRQUFMLEdBQWdCRSxFQUFFRSxJQUFGLENBQU8sU0FBUCxDQUFoQjtBQUNBO0FBQ0EsTUFBR0MsT0FBT0MsUUFBUCxDQUFnQkMsTUFBaEIsQ0FBdUJDLE1BQXZCLEdBQWdDQyxPQUFoQyxDQUF3QyxRQUF4QyxJQUFvRCxDQUFDLENBQXhELEVBQ0E7QUFBQSxPQUNVQyxjQURWLEdBQ0MsU0FBU0EsY0FBVCxHQUEwQjtBQUNoQixXQUFPQyxNQUFNQyxHQUFOLENBQVUsaUJBQVYsQ0FBUDtBQUNILElBSFI7O0FBQUEsT0FJVUMsV0FKVixHQUlDLFNBQVNBLFdBQVQsR0FBdUI7QUFDdEIsV0FBT0MsV0FBV0MsSUFBWCxDQUFnQixpQkFBaEIsRUFBa0M7QUFDeENoQixjQUFRaUIsZ0JBQWdCLFFBQWhCLENBRGdDO0FBRXhDQyxXQUFLRCxnQkFBZ0IsT0FBaEIsQ0FGbUM7QUFHeENFLGdCQUFVRixnQkFBZ0IsV0FBaEIsQ0FIOEI7QUFJeENHLGVBQVVILGdCQUFnQixLQUFoQixDQUo4QjtBQUt4Q0ksZUFBVUosZ0JBQWdCLEtBQWhCLENBTDhCO0FBTXhDSyxhQUFRTCxnQkFBZ0IsUUFBaEI7QUFOZ0MsS0FBbEMsQ0FBUDtBQVFBLElBYkY7O0FBZUlMLFNBQU1XLEdBQU4sQ0FBVSxDQUNUWixnQkFEUyxFQUVaRyxhQUZZLENBQVYsRUFHQVUsSUFIQSxDQUdLWixNQUFNYSxNQUFOLENBQ1AsVUFDQ0MsV0FERCxFQUVDQyxRQUZELEVBR0s7O0FBRUpDLHFCQUFpQkQsU0FBU3ZDLElBQVQsQ0FBY2lDLFFBQS9COztBQUVBLFVBQUt0QixJQUFMLEdBQVkyQixZQUFZdEMsSUFBWixDQUFpQkEsSUFBN0I7O0FBRUEsUUFBR3VDLFNBQVN2QyxJQUFaLEVBQWlCO0FBQ2hCZ0IsT0FBRSxVQUFGLEVBQWN5QixNQUFkO0FBQ0EsV0FBS2pDLEtBQUwsR0FBYStCLFNBQVN2QyxJQUFULENBQWNBLElBQWQsQ0FBbUJBLElBQWhDO0FBQ0E7O0FBRUQsUUFBR3VDLFNBQVN2QyxJQUFULENBQWNDLFVBQWQsQ0FBeUJDLEtBQXpCLEdBQWlDLEVBQXBDLEVBQXVDO0FBQ3RDLFdBQUtELFVBQUwsR0FBa0JzQyxTQUFTdkMsSUFBVCxDQUFjQyxVQUFoQztBQUNBOztBQUVEO0FBQ0EsUUFBRyxNQUFLTyxLQUFMLElBQWMsRUFBakIsRUFBb0I7QUFDZlEsT0FBRSxrQkFBRixFQUFzQjBCLElBQXRCO0FBQ0o7O0FBRUQsUUFBR0gsU0FBU3ZDLElBQVQsQ0FBYzJDLFdBQWQsSUFBNkIsRUFBaEMsRUFBb0M7QUFDbkMzQixPQUFFLGdCQUFGLEVBQW9CNEIsV0FBcEIsQ0FBZ0MsTUFBaEM7QUFDQTVCLE9BQUUsNkJBQUYsRUFBaUM2QixJQUFqQyxDQUFzQyxNQUFNTixTQUFTdkMsSUFBVCxDQUFjMkMsV0FBcEIsR0FBa0MsR0FBeEU7QUFDQSxLQUhELE1BR087QUFDTjNCLE9BQUUsZ0JBQUYsRUFBb0I4QixRQUFwQixDQUE2QixNQUE3QjtBQUNBOUIsT0FBRSw2QkFBRixFQUFpQzZCLElBQWpDLENBQXNDLEVBQXRDO0FBQ0E7QUFFRixJQWhDTyxDQUhMLEVBb0NGRSxLQXBDRSxDQW9DSS9CLEVBQUVnQyxJQXBDTjs7QUFzQ0gsT0FBSUMsUUFBUXBCLGdCQUFnQixPQUFoQixDQUFaOztBQUVBLE9BQUdvQixLQUFILEVBQVM7QUFDUixRQUFHQSxVQUFRLE1BQVgsRUFDQ2pDLEVBQUUsU0FBRixFQUFhQyxJQUFiLENBQWtCLFNBQWxCLEVBQTRCLFNBQTVCOztBQUVELFFBQUdnQyxVQUFRLEtBQVgsRUFDQ2pDLEVBQUUsU0FBRixFQUFhQyxJQUFiLENBQWtCLFNBQWxCLEVBQTRCLFNBQTVCO0FBQ0QsSUFORCxNQU9JO0FBQ0hELE1BQUUsU0FBRixFQUFhQyxJQUFiLENBQWtCLFNBQWxCLEVBQTRCLFNBQTVCO0FBQ0E7QUFDRDtBQUNELEVBekZnQjs7QUEwRmRpQyxXQUFVO0FBQ05DLFdBRE0sdUJBQ007QUFDUixVQUFPLEtBQUtsRCxVQUFMLENBQWdCSyxZQUF2QjtBQUNILEdBSEs7QUFJTjhDLGFBSk0seUJBSVE7QUFDVixPQUFJLENBQUMsS0FBS25ELFVBQUwsQ0FBZ0JJLEVBQXJCLEVBQXlCO0FBQ3JCLFdBQU8sRUFBUDtBQUNIO0FBQ0QsT0FBSUQsT0FBTyxLQUFLSCxVQUFMLENBQWdCSyxZQUFoQixHQUErQixLQUFLQyxNQUEvQztBQUNBLE9BQUlILE9BQU8sQ0FBWCxFQUFjO0FBQ1ZBLFdBQU8sQ0FBUDtBQUNIO0FBQ0QsT0FBSUMsS0FBS0QsT0FBUSxLQUFLRyxNQUFMLEdBQWMsQ0FBL0I7QUFDQSxPQUFJRixNQUFNLEtBQUtKLFVBQUwsQ0FBZ0JvRCxTQUExQixFQUFxQztBQUNqQ2hELFNBQUssS0FBS0osVUFBTCxDQUFnQm9ELFNBQXJCO0FBQ0g7QUFDRCxPQUFJQyxhQUFhLEVBQWpCO0FBQ0EsVUFBT2xELFFBQVFDLEVBQWYsRUFBbUI7QUFDZmlELGVBQVdDLElBQVgsQ0FBZ0JuRCxJQUFoQjtBQUNBQTtBQUNIO0FBQ0QsVUFBT2tELFVBQVA7QUFDSDtBQXRCSyxFQTFGSTtBQW1IakJFLFVBQVE7QUFDUEMsWUFETyxzQkFDSUMsRUFESixFQUNPO0FBQ2IsUUFBS2pELFFBQUwsQ0FBYzhDLElBQWQsQ0FBbUJHLEVBQW5CO0FBQ0EsR0FITTtBQUlQQyxRQUpPLGtCQUlBQyxHQUpBLEVBSUk7QUFDVixPQUFJQyxNQUFNQyxlQUFlNUMsT0FBT0MsUUFBUCxDQUFnQjRDLElBQS9CLEVBQXFDLE9BQXJDLENBQVY7QUFDQSxPQUFHSCxPQUFLLE1BQVIsRUFBZTtBQUNkO0FBQ0ExQyxXQUFPQyxRQUFQLEdBQWtCMEMsTUFBTSxhQUF4QjtBQUNBOztBQUVELE9BQUdELE9BQUssS0FBUixFQUFjO0FBQ2I7QUFDQTFDLFdBQU9DLFFBQVAsR0FBa0IwQyxNQUFNLFlBQXhCO0FBQ0E7O0FBRUQsT0FBR0QsT0FBSyxLQUFSLEVBQWM7QUFDYixRQUFJQyxNQUFNM0MsT0FBT0MsUUFBUCxDQUFnQjRDLElBQTFCO0FBQ0E3QyxXQUFPQyxRQUFQLEdBQWtCLEtBQUs2QyxrQkFBTCxDQUF3QkgsR0FBeEIsRUFBNEIsT0FBNUIsQ0FBbEI7QUFDQTtBQUNELEdBcEJNO0FBcUJQSSxhQXJCTyx1QkFxQktDLENBckJMLEVBcUJRO0FBQ2QsT0FBSUwsTUFBTTNDLE9BQU9DLFFBQVAsQ0FBZ0I0QyxJQUExQjtBQUNBLE9BQUk3QixTQUFTTCxnQkFBZ0IsUUFBaEIsQ0FBYjs7QUFFQSxPQUFHSyxVQUFVaUMsU0FBYixFQUF3QjtBQUN2QmpELFdBQU9DLFFBQVAsR0FBa0IwQyxNQUFNLFVBQU4sR0FBbUJLLEVBQUVFLE1BQUYsQ0FBUzFFLEtBQTlDO0FBQ0EsSUFGRCxNQUVPO0FBQ04sUUFBR3dFLEVBQUVFLE1BQUYsQ0FBU0MsT0FBWixFQUFxQjtBQUNwQixTQUFJUixNQUFNQyxlQUFlRCxHQUFmLEVBQW1CLFFBQW5CLENBQVY7O0FBRUEzQyxZQUFPQyxRQUFQLEdBQWtCMEMsTUFBTSxVQUFOLEdBQW1CM0IsTUFBbkIsR0FBNEIsSUFBNUIsR0FBbUNnQyxFQUFFRSxNQUFGLENBQVMxRSxLQUE5RDtBQUNBLEtBSkQsTUFJTztBQUNOLFNBQUdtRSxJQUFJdkMsT0FBSixDQUFZLE9BQUs0QyxFQUFFRSxNQUFGLENBQVMxRSxLQUExQixLQUFvQyxDQUFDLENBQXhDLEVBQTJDO0FBQzFDLFVBQUltRSxNQUFNQSxJQUFJUyxPQUFKLENBQVksT0FBS0osRUFBRUUsTUFBRixDQUFTMUUsS0FBMUIsRUFBaUMsRUFBakMsQ0FBVjtBQUNBLE1BRkQsTUFFTztBQUNOLFVBQUltRSxNQUFNQSxJQUFJUyxPQUFKLENBQVlKLEVBQUVFLE1BQUYsQ0FBUzFFLEtBQXJCLEVBQTRCLEVBQTVCLENBQVY7QUFDQTs7QUFFRHdCLFlBQU9DLFFBQVAsR0FBa0IwQyxHQUFsQjtBQUNBO0FBQ0Q7QUFDRCxHQTFDTTtBQTJDUEcsb0JBM0NPLDhCQTJDWUgsR0EzQ1osRUEyQ2lCVSxTQTNDakIsRUEyQzRCO0FBQy9CO0FBQ0EsT0FBSUMsV0FBVVgsSUFBSVksS0FBSixDQUFVLEdBQVYsQ0FBZDtBQUNBLE9BQUlELFNBQVNFLE1BQVQsSUFBaUIsQ0FBckIsRUFBd0I7O0FBRXBCLFFBQUlDLFNBQVFDLG1CQUFtQkwsU0FBbkIsSUFBOEIsR0FBMUM7QUFDQSxRQUFJTSxPQUFNTCxTQUFTLENBQVQsRUFBWUMsS0FBWixDQUFrQixPQUFsQixDQUFWOztBQUVBO0FBQ0EsU0FBSyxJQUFJSyxJQUFHRCxLQUFLSCxNQUFqQixFQUF5QkksTUFBTSxDQUEvQixHQUFtQztBQUMvQjtBQUNBLFNBQUlELEtBQUtDLENBQUwsRUFBUUMsV0FBUixDQUFvQkosTUFBcEIsRUFBNEIsQ0FBNUIsTUFBbUMsQ0FBQyxDQUF4QyxFQUEyQztBQUN2Q0UsV0FBS0csTUFBTCxDQUFZRixDQUFaLEVBQWUsQ0FBZjtBQUNIO0FBQ0o7O0FBRURqQixVQUFLVyxTQUFTLENBQVQsSUFBWSxHQUFaLEdBQWdCSyxLQUFLSSxJQUFMLENBQVUsR0FBVixDQUFyQjtBQUNBLFdBQU9wQixHQUFQO0FBQ0gsSUFmRCxNQWVPO0FBQ0gsV0FBT0EsR0FBUDtBQUNIO0FBQ0osR0FoRU07O0FBaUVKcUIsZUFBYSxxQkFBVWhCLENBQVYsRUFBYTtBQUN6QixPQUFJQSxFQUFFaUIsT0FBRixJQUFhLEVBQWpCLEVBQXFCO0FBQ3BCLFFBQUl0QixNQUFNQyxlQUFlNUMsT0FBT0MsUUFBUCxDQUFnQjRDLElBQS9CLEVBQXFDLFdBQXJDLENBQVY7QUFDSDdDLFdBQU9DLFFBQVAsR0FBbUIwQyxNQUFNLGFBQU4sR0FBc0IsS0FBS2pELE9BQTlDO0FBQ0E7QUFDRCxHQXRFTTtBQXVFRHdFLFdBdkVDLHFCQXVFUzFCLEVBdkVULEVBdUVZMkIsSUF2RVosRUF1RWlCO0FBQUE7O0FBQ2pCN0QsU0FBTUMsR0FBTixDQUFVTixTQUFTbUUsTUFBVCxHQUFrQixXQUE1QixFQUF3QztBQUNwQ0MsWUFBUTtBQUNKQyxjQUFROUIsRUFESjtBQUVKK0IsVUFBSTtBQUZBO0FBRDRCLElBQXhDLEVBTUNyRCxJQU5ELENBTU0sa0JBQVU7QUFDWnBCLE1BQUUsaUNBQUYsRUFBcUM2QixJQUFyQyxDQUEwQzZDLE9BQU8xRixJQUFQLENBQVkyRixlQUF0RDtBQUNHLFFBQUdELE9BQU8xRixJQUFQLENBQVk0RixTQUFmLEVBQ0E7QUFDQyxTQUFHRixPQUFPMUYsSUFBUCxDQUFZNEYsU0FBWixJQUF1QixVQUExQixFQUNDMUUsT0FBT0MsUUFBUCxHQUFrQixjQUFjdUUsT0FBTzFGLElBQVAsQ0FBWTZGLElBQTVDLENBREQsS0FHQ0MsT0FBT0MsS0FBUCxDQUFhTCxPQUFPMUYsSUFBUCxDQUFZNEYsU0FBekIsRUFBb0MsT0FBS2pGLElBQUwsQ0FBVSxXQUFWLENBQXBDO0FBQ0QsS0FORCxNQU1PO0FBQ05tRixZQUFPRSxPQUFQLENBQWUsT0FBS3JGLElBQUwsQ0FBVSxZQUFWLENBQWYsRUFBd0MwRSxJQUF4QztBQUNBO0FBQ0osSUFqQko7QUFrQkEsR0ExRkE7QUEyRkRZLGNBM0ZDLHdCQTJGWXZDLEVBM0ZaLEVBMkZlMkIsSUEzRmYsRUEyRm9CO0FBQUE7O0FBQ2pCLE9BQUdhLFFBQVFDLGVBQVIsS0FBNEIsSUFBL0IsRUFBb0M7QUFDaEN4RSxlQUFXRixHQUFYLENBQWUsbUJBQW1CaUMsRUFBbEMsRUFDQ3RCLElBREQsQ0FDTSxrQkFBVTtBQUNaLFNBQUdzRCxPQUFPMUYsSUFBUCxDQUFZdUMsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUM3QixVQUFJNkQsT0FBT0MsU0FBU3JGLEVBQUUsYUFBRixFQUFpQjZCLElBQWpCLEVBQVQsSUFBa0MsQ0FBN0M7QUFDQTdCLFFBQUUsYUFBRixFQUFpQjZCLElBQWpCLENBQXNCdUQsSUFBdEIsRUFBNEIxRCxJQUE1QjtBQUNBb0QsYUFBT0UsT0FBUCxDQUFlLE9BQUtyRixJQUFMLENBQVUsa0JBQVYsQ0FBZixFQUE4QzBFLElBQTlDO0FBQ0gsTUFKSyxNQUlDLElBQUdLLE9BQU8xRixJQUFQLENBQVl1QyxRQUFaLElBQXNCLENBQXpCLEVBQTRCO0FBQ2pDdUQsYUFBT0MsS0FBUCxDQUFhLE9BQUtwRixJQUFMLENBQVUsTUFBVixDQUFiO0FBQ0ssTUFGQSxNQUVNO0FBQ05tRixhQUFPUSxJQUFQLENBQVksT0FBSzNGLElBQUwsQ0FBVSxpQkFBVixDQUFaLEVBQTBDMEUsSUFBMUM7QUFDQTtBQUNKLEtBWEQ7QUFZSCxJQWJELE1BYU87QUFDSFMsV0FBT0MsS0FBUCxDQUFhLEtBQUtwRixJQUFMLENBQVUsZUFBVixDQUFiO0FBQ0g7QUFDSixHQTVHQTtBQTZHRDRGLGdCQTdHQywwQkE2R2M3QyxFQTdHZCxFQTZHaUIyQixJQTdHakIsRUE2R3NCO0FBQUE7O0FBQ25CLE9BQUdhLFFBQVFDLGVBQVIsS0FBNEIsSUFBL0IsRUFBb0M7QUFDaEN4RSxlQUFXRixHQUFYLENBQWUscUJBQXFCaUMsRUFBcEMsRUFDQ3RCLElBREQsQ0FDTSxrQkFBVTtBQUNaLFNBQUdzRCxPQUFPMUYsSUFBUCxDQUFZdUMsUUFBWixJQUFzQixDQUF6QixFQUEyQjtBQUM3QixVQUFJaUUsTUFBTUgsU0FBU3JGLEVBQUUsWUFBRixFQUFnQjZCLElBQWhCLEVBQVQsSUFBaUMsQ0FBM0M7QUFDQTdCLFFBQUUsWUFBRixFQUFnQjZCLElBQWhCLENBQXFCMkQsR0FBckIsRUFBMEI5RCxJQUExQjtBQUNBb0QsYUFBT0UsT0FBUCxDQUFlLE9BQUtyRixJQUFMLENBQVUsaUJBQVYsQ0FBZixFQUE2QzBFLElBQTdDO0FBQ0gsTUFKSyxNQUlDLElBQUdLLE9BQU8xRixJQUFQLENBQVl1QyxRQUFaLElBQXNCLENBQXpCLEVBQTJCO0FBQzNCdUQsYUFBT0MsS0FBUCxDQUFhTCxPQUFPMUYsSUFBUCxDQUFZNEYsU0FBekIsRUFBb0MsT0FBS2pGLElBQUwsQ0FBVSxvQkFBVixDQUFwQztBQUNBLE1BRkEsTUFFTTtBQUNObUYsYUFBT1EsSUFBUCxDQUFZLE9BQUszRixJQUFMLENBQVUsbUJBQVYsQ0FBWixFQUE0QzBFLElBQTVDO0FBQ0E7QUFDSixLQVhEO0FBWUgsSUFiRCxNQWFPO0FBQ0hTLFdBQU9DLEtBQVAsQ0FBYSxLQUFLcEYsSUFBTCxDQUFVLGlCQUFWLENBQWI7QUFDSDtBQUNKLEdBOUhBO0FBK0hEOEYsWUEvSEMsc0JBK0hVQyxJQS9IVixFQStIZ0I7QUFBQTs7QUFDdEIvRSxjQUFXQyxJQUFYLENBQWdCLGlCQUFoQixFQUFrQztBQUNqQzhFLFVBQU1BLElBRDJCO0FBRWpDOUYsYUFBUWlCLGdCQUFnQixRQUFoQixDQUZ5QjtBQUdqQ0MsVUFBS0QsZ0JBQWdCLE9BQWhCLENBSDRCO0FBSWpDRSxlQUFVRixnQkFBZ0IsV0FBaEIsQ0FKdUI7QUFLakNHLGNBQVVILGdCQUFnQixLQUFoQixDQUx1QjtBQU1qQ0ksY0FBVUosZ0JBQWdCLEtBQWhCLENBTnVCO0FBT2pDSyxZQUFRTCxnQkFBZ0IsUUFBaEI7QUFQeUIsSUFBbEMsRUFTQ08sSUFURCxDQVNNLGtCQUFVO0FBQ2ZJLHFCQUFpQmtELE9BQU8xRixJQUFQLENBQVlpQyxRQUE3Qjs7QUFFQSxXQUFLekIsS0FBTCxHQUFha0YsT0FBTzFGLElBQVAsQ0FBWUEsSUFBWixDQUFpQkEsSUFBOUI7QUFDQSxXQUFLQyxVQUFMLEdBQWtCeUYsT0FBTzFGLElBQVAsQ0FBWUMsVUFBOUI7QUFDTSxJQWRQO0FBZU0sR0EvSUE7QUFnSkowRyxZQWhKSSxzQkFnSk9ELElBaEpQLEVBZ0phO0FBQ2IsUUFBS3pHLFVBQUwsQ0FBZ0JLLFlBQWhCLEdBQStCb0csSUFBL0I7QUFDQSxRQUFLRCxVQUFMLENBQWdCQyxJQUFoQjtBQUNILEdBbkpHO0FBb0pKRSxnQkFwSkksNEJBb0pZO0FBQ2Q1RixLQUFFLGFBQUYsRUFBaUIwQixJQUFqQjtBQUNELEdBdEpHO0FBdUpKbUUsZ0JBdkpJLDRCQXVKWTtBQUNkN0YsS0FBRSxhQUFGLEVBQWlCOEYsSUFBakI7QUFDRDtBQXpKRyxFQW5IUztBQStRakJDLFFBQU87QUFDTkMsUUFBTSxjQUFVQyxHQUFWLEVBQWU7QUFDcEJqRyxLQUFFLGdCQUFGLEVBQW9Ca0csT0FBcEIsQ0FBNEI7QUFDeEJDLGNBQVU7QUFDVEMsYUFBUTtBQURDLEtBRGM7QUFJeEJDLGVBQVc7QUFDVkMsaUJBQVksc0JBQVU7QUFDckJ0RyxRQUFFLGtCQUFGLEVBQXNCdUcsT0FBdEIsQ0FBOEIsR0FBOUI7QUFDQSxNQUhTO0FBSVJDLGdCQUFXLHFCQUFVO0FBQ3BCeEcsUUFBRSxrQkFBRixFQUFzQnlHLE1BQXRCLENBQTZCLEdBQTdCO0FBQ0Y7QUFOUztBQUphLElBQTVCO0FBYUE7QUFmSyxFQS9RVTtBQWdTakJDLFFBaFNpQixxQkFnU1A7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBOVNnQixDQUFSLENBQVY7O0FBaVRBLFNBQVMvSCxnQkFBVCxDQUEwQmdJLENBQTFCLEVBQTZCO0FBQ3pCLFFBQU9BLEVBQUVDLFFBQUYsR0FBYXRELE9BQWIsQ0FBcUIsdUJBQXJCLEVBQThDLEdBQTlDLENBQVA7QUFDSDs7QUFFRCxTQUFTekUsUUFBVCxDQUFrQmdJLENBQWxCLEVBQXFCO0FBQ2pCLFFBQU9DLEtBQUtDLEtBQUwsQ0FBV0YsQ0FBWCxDQUFQO0FBQ0g7O0FBRUQsU0FBU2hHLGVBQVQsQ0FBeUJtRyxNQUF6QixFQUNBO0FBQ0ksS0FBSUMsV0FBVy9HLE9BQU9DLFFBQVAsQ0FBZ0JDLE1BQWhCLENBQXVCOEcsU0FBdkIsQ0FBaUMsQ0FBakMsQ0FBZjtBQUNBLEtBQUlDLGdCQUFnQkYsU0FBU3hELEtBQVQsQ0FBZSxHQUFmLENBQXBCO0FBQ0EsTUFBSyxJQUFJSyxJQUFJLENBQWIsRUFBZ0JBLElBQUlxRCxjQUFjekQsTUFBbEMsRUFBMENJLEdBQTFDLEVBQ0E7QUFDSSxNQUFJc0QsaUJBQWlCRCxjQUFjckQsQ0FBZCxFQUFpQkwsS0FBakIsQ0FBdUIsR0FBdkIsQ0FBckI7QUFDQSxNQUFJMkQsZUFBZSxDQUFmLEtBQXFCSixNQUF6QixFQUNBO0FBQ0ksVUFBT0ksZUFBZSxDQUFmLENBQVA7QUFDSDtBQUNKO0FBQ0o7O0FBRUQsU0FBU3RFLGNBQVQsQ0FBd0JELEdBQXhCLEVBQTZCd0UsS0FBN0IsRUFDQTtBQUNFLEtBQUk3RCxXQUFVWCxJQUFJWSxLQUFKLENBQVUsR0FBVixDQUFkO0FBQ0EsS0FBSUQsU0FBU0UsTUFBVCxJQUFpQixDQUFyQixFQUNBO0FBQ0MsTUFBSUMsU0FBUUMsbUJBQW1CeUQsS0FBbkIsSUFBMEIsR0FBdEM7QUFDQSxNQUFJeEQsT0FBTUwsU0FBUyxDQUFULEVBQVlDLEtBQVosQ0FBa0IsT0FBbEIsQ0FBVjtBQUNBLE9BQUssSUFBSUssSUFBRUQsS0FBS0gsTUFBaEIsRUFBd0JJLE1BQU0sQ0FBOUI7QUFDQyxPQUFJRCxLQUFLQyxDQUFMLEVBQVF4RCxPQUFSLENBQWdCcUQsTUFBaEIsRUFBd0IsQ0FBeEIsS0FBNEIsQ0FBaEMsRUFDQ0UsS0FBS0csTUFBTCxDQUFZRixDQUFaLEVBQWUsQ0FBZjtBQUZGLEdBR0EsSUFBSUQsS0FBS0gsTUFBTCxHQUFjLENBQWxCLEVBQ0MsT0FBT0YsU0FBUyxDQUFULElBQVksR0FBWixHQUFnQkssS0FBS0ksSUFBTCxDQUFVLEdBQVYsQ0FBdkIsQ0FERCxLQUdDLE9BQU9ULFNBQVMsQ0FBVCxDQUFQO0FBQ0QsRUFYRCxNQWFDLE9BQU9YLEdBQVA7QUFDRjs7QUFFRCxTQUFTckIsZ0JBQVQsQ0FBMEJQLFFBQTFCLEVBQW9DO0FBQ25DZixRQUFPb0gsTUFBUCxDQUFjQyxNQUFkLENBQXFCO0FBQ3BCQyxPQUFLLENBRGU7QUFFcEJwSSxRQUFNLENBRmM7O0FBSXBCcUksT0FBS3hHLFFBSmU7QUFLcEI1QixNQUFJNEI7QUFMZ0IsRUFBckI7QUFPQSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4VUQ7VUFFQTs7Z0NBRUE7OEJBQ0E7QUFDQTtvQ0FDQTtxREFDQTtBQUNBOzBDQUNBO3dEQUNBO0FBQ0E7OENBQ0E7MERBQ0E7QUFFQTtBQWJBOzs0QkFlQTtvQ0FDQSxpSUFFQSwyQkFDQTtBQUNBO29DQUNBO29DQUNBLHNJQUVBLDJCQUNBO0FBQ0E7c0NBQ0E7bUVBQ0E7c0NBQ0E7MkNBQ0E7c0NBQ0E7QUFDQTs4Q0FDQTtBQUNBLGFBQ0E7MkNBQ0E7aUJBQ0E7QUFDQTt5QkFDQTtBQUNBO0FBQ0E7c0NBQ0E7NEVBQ0E7QUFDQTs0Q0FDQTttREFDQTtBQUdBO0FBbkNBO0FBaEJBLEc7Ozs7Ozs7QUNyQ0E7QUFDQTtBQUNBLHdCQUFxSjtBQUNySjtBQUNBLHdCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9wYWdlcy9wcm9kdWN0LWxpc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQzOCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsIlZ1ZS5jb21wb25lbnQoJ3Byb2R1Y3QtaXRlbScsICByZXF1aXJlKCcuLi9jb21wb25lbnRzL3Byb2R1Y3QvUHJvZHVjdC52dWUnKSk7XHJcblxyXG5WdWUuZmlsdGVyKCdjdXJyZW5jeScsIGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuICBudW1iZXJXaXRoQ29tbWFzKHZhbHVlLnRvRml4ZWQoMikpO1xyXG59KTtcclxuXHJcblZ1ZS5maWx0ZXIoJ3JvdW5kJywgZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICByZXR1cm4gIHJvdW5kT2ZmKHZhbHVlKTtcclxufSk7XHJcblxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcblx0ZWw6ICcjcHJvZHVjdC1saXN0JyxcclxuXHRkYXRhOiB7XHJcbiAgICAgICAgcGFnaW5hdGlvbjoge1xyXG4gICAgICAgICAgICB0b3RhbDogMCxcclxuICAgICAgICAgICAgcGVyX3BhZ2U6IDcsXHJcbiAgICAgICAgICAgIGZyb206IDEsXHJcbiAgICAgICAgICAgIHRvOiAwLFxyXG4gICAgICAgICAgICBjdXJyZW50X3BhZ2U6IDFcclxuICAgICAgICB9LFxyXG4gICAgICAgIG9mZnNldDogNCwvLyBsZWZ0IGFuZCByaWdodCBwYWRkaW5nIGZyb20gdGhlIHBhZ2luYXRpb24gPHNwYW4+LGp1c3QgY2hhbmdlIGl0IHRvIHNlZSBlZmZlY3RzXHJcblx0XHRpdGVtczpbXSxcclxuXHRcdHNlbGVjdGVkOltdLFxyXG5cdFx0cE5hbWU6dHJ1ZSxcclxuXHRcdGxhbmc6W10sXHJcblx0XHRrZXl3b3JkOicnLFxyXG5cdFx0bGFuZ21vZGU6bnVsbFxyXG5cdH0sXHJcblx0Y3JlYXRlZCgpe1xyXG5cdFx0dmFyIG0gPSAkKFwibWV0YVtuYW1lPWxvY2FsZS1sYW5nXVwiKTsgICAgXHJcblx0XHR0aGlzLmxhbmdtb2RlID0gbS5hdHRyKFwiY29udGVudFwiKTtcclxuXHRcdC8vY2hlY2sgaWYgdGhlcmUgaXMgYSBzZWFyY2ggcXVlcnlcclxuXHRcdGlmKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc3Vic3RyKCkuaW5kZXhPZignc2VhcmNoJykgPiAtMSlcclxuXHRcdHtcclxuXHRcdFx0ZnVuY3Rpb24gZ2V0VHJhbnNsYXRpb24oKSB7XHJcbiAgICAgICAgICAgIFx0cmV0dXJuIGF4aW9zLmdldCgnL3RyYW5zbGF0ZS9jYXJ0Jyk7XHJcbiAgICAgICAgXHR9XHJcblx0XHRcdGZ1bmN0aW9uIGdldFByb2R1Y3RzKCkge1xyXG5cdFx0XHRcdHJldHVybiBheGlvc0FQSXYxLnBvc3QoJy9zZWFyY2gvcHJvZHVjdCcse1xyXG5cdFx0XHRcdFx0a2V5d29yZDpnZXRVUkxQYXJhbWV0ZXIoJ3NlYXJjaCcpLFxyXG5cdFx0XHRcdFx0c29ydDpnZXRVUkxQYXJhbWV0ZXIoJ3ByaWNlJyksXHJcblx0XHRcdFx0XHRwcm9kX25hbWU6Z2V0VVJMUGFyYW1ldGVyKCdwcm9kX25hbWUnKSxcclxuXHRcdFx0XHRcdG1pblByaWNlOiBnZXRVUkxQYXJhbWV0ZXIoJ21pbicpLFxyXG5cdFx0XHRcdFx0bWF4UHJpY2U6IGdldFVSTFBhcmFtZXRlcignbWF4JyksXHJcblx0XHRcdFx0XHRicmFuZHM6IGdldFVSTFBhcmFtZXRlcignYnJhbmRzJylcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHQgICAgXHRheGlvcy5hbGwoW1xyXG5cdCAgICBcdFx0Z2V0VHJhbnNsYXRpb24oKSxcclxuXHRcdFx0XHRnZXRQcm9kdWN0cygpXHJcblx0XHRcdF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG5cdFx0XHRcdChcclxuXHRcdFx0XHRcdHRyYW5zbGF0aW9uLFxyXG5cdFx0XHRcdFx0cmVzcG9uc2VcclxuXHRcdFx0XHQpID0+IHtcclxuXHJcblx0XHRcdFx0XHRzZXRQcmljZVJhbmdlTWF4KHJlc3BvbnNlLmRhdGEubWF4UHJpY2UpO1xyXG5cclxuXHRcdFx0XHRcdHRoaXMubGFuZyA9IHRyYW5zbGF0aW9uLmRhdGEuZGF0YTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YSl7XHJcblx0XHRcdFx0XHRcdCQoJy5sb2FkaW5nJykucmVtb3ZlKCk7XHJcblx0XHRcdFx0XHRcdHRoaXMuaXRlbXMgPSByZXNwb25zZS5kYXRhLmRhdGEuZGF0YTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YS5wYWdpbmF0aW9uLnRvdGFsID4gMjApe1xyXG5cdFx0XHRcdFx0XHR0aGlzLnBhZ2luYXRpb24gPSByZXNwb25zZS5kYXRhLnBhZ2luYXRpb247XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0Ly9zaG93IG5vIGZvdW5kIHJlc3VsdCBcclxuXHRcdFx0XHRcdGlmKHRoaXMuaXRlbXMgPT0gJycpe1xyXG5cdFx0XHRcdFx0ICAgICAkKCcuY2QtZmFpbC1tZXNzYWdlJykuc2hvdygpO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdGlmKHJlc3BvbnNlLmRhdGEucXVlcnlTdHJpbmcgIT0gJycpIHtcclxuXHRcdFx0XHRcdFx0JCgnI3Jlc3VsdE1lc3NhZ2UnKS5yZW1vdmVDbGFzcygnaGlkZScpO1xyXG5cdFx0XHRcdFx0XHQkKCcjcmVzdWx0TWVzc2FnZSAjcXVlcnlTdHJpbmcnKS50ZXh0KCdcIicgKyByZXNwb25zZS5kYXRhLnF1ZXJ5U3RyaW5nICsgJ1wiJyk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHQkKCcjcmVzdWx0TWVzc2FnZScpLmFkZENsYXNzKCdoaWRlJyk7XHJcblx0XHRcdFx0XHRcdCQoJyNyZXN1bHRNZXNzYWdlICNxdWVyeVN0cmluZycpLnRleHQoJycpO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fSkpXHJcblx0XHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cclxuXHRcdFx0dmFyIHByaWNlID0gZ2V0VVJMUGFyYW1ldGVyKCdwcmljZScpO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYocHJpY2Upe1xyXG5cdFx0XHRcdGlmKHByaWNlPT09J2Rlc2MnKVxyXG5cdFx0XHRcdFx0JCgnI3JhZGlvMicpLmF0dHIoJ2NoZWNrZWQnLCdjaGVja2VkJyk7XHJcblxyXG5cdFx0XHRcdGlmKHByaWNlPT09J2FzYycpXHJcblx0XHRcdFx0XHQkKCcjcmFkaW8zJykuYXR0cignY2hlY2tlZCcsJ2NoZWNrZWQnKTtcdFxyXG5cdFx0XHR9XHJcblx0XHRcdGVsc2V7XHJcblx0XHRcdFx0JCgnI3JhZGlvMScpLmF0dHIoJ2NoZWNrZWQnLCdjaGVja2VkJyk7XHRcdFx0XHRcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGlzQWN0aXZlZCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucGFnaW5hdGlvbi5jdXJyZW50X3BhZ2U7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBwYWdlc051bWJlcigpIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnBhZ2luYXRpb24udG8pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBbXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgZnJvbSA9IHRoaXMucGFnaW5hdGlvbi5jdXJyZW50X3BhZ2UgLSB0aGlzLm9mZnNldDtcclxuICAgICAgICAgICAgaWYgKGZyb20gPCAxKSB7XHJcbiAgICAgICAgICAgICAgICBmcm9tID0gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgdG8gPSBmcm9tICsgKHRoaXMub2Zmc2V0ICogMik7XHJcbiAgICAgICAgICAgIGlmICh0byA+PSB0aGlzLnBhZ2luYXRpb24ubGFzdF9wYWdlKSB7XHJcbiAgICAgICAgICAgICAgICB0byA9IHRoaXMucGFnaW5hdGlvbi5sYXN0X3BhZ2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIHBhZ2VzQXJyYXkgPSBbXTtcclxuICAgICAgICAgICAgd2hpbGUgKGZyb20gPD0gdG8pIHtcclxuICAgICAgICAgICAgICAgIHBhZ2VzQXJyYXkucHVzaChmcm9tKTtcclxuICAgICAgICAgICAgICAgIGZyb20rKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcGFnZXNBcnJheTtcclxuICAgICAgICB9XHJcbiAgICAgICBcclxuICAgIH0sXHJcblx0bWV0aG9kczp7XHJcblx0XHRzZWxlY3RJdGVtKGlkKXtcclxuXHRcdFx0dGhpcy5zZWxlY3RlZC5wdXNoKGlkKVxyXG5cdFx0fSxcclxuXHRcdHNvcnRCeSh2YWwpe1xyXG5cdFx0XHR2YXIgdXJsID0gcmVtb3ZlVVJMUGFyYW0od2luZG93LmxvY2F0aW9uLmhyZWYsICdwcmljZScpO1xyXG5cdFx0XHRpZih2YWw9PVwiZGVzY1wiKXtcclxuXHRcdFx0XHQvL3ZhciB1cmwgPSB3aW5kb3cubG9jYXRpb24uaHJlZi5yZXBsYWNlKCcmcHJpY2U9YXNjJywnJylcclxuXHRcdFx0XHR3aW5kb3cubG9jYXRpb24gPSB1cmwgKyAnJnByaWNlPWRlc2MnO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZih2YWw9PVwiYXNjXCIpe1xyXG5cdFx0XHRcdC8vdmFyIHVybCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmLnJlcGxhY2UoJyZwcmljZT1kZXNjJywnJylcclxuXHRcdFx0XHR3aW5kb3cubG9jYXRpb24gPSB1cmwgKyAnJnByaWNlPWFzYyc7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmKHZhbD09XCJyZWxcIil7XHJcblx0XHRcdFx0dmFyIHVybCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xyXG5cdFx0XHRcdHdpbmRvdy5sb2NhdGlvbiA9IHRoaXMucmVtb3ZlVVJMUGFyYW1ldGVyKHVybCwncHJpY2UnKTtcclxuXHRcdFx0fVx0XHRcdFxyXG5cdFx0fSxcclxuXHRcdHNvcnRCeUJyYW5kKGUpIHtcclxuXHRcdFx0dmFyIHVybCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xyXG5cdFx0XHR2YXIgYnJhbmRzID0gZ2V0VVJMUGFyYW1ldGVyKCdicmFuZHMnKTtcclxuXHJcblx0XHRcdGlmKGJyYW5kcyA9PSB1bmRlZmluZWQpIHtcclxuXHRcdFx0XHR3aW5kb3cubG9jYXRpb24gPSB1cmwgKyAnJmJyYW5kcz0nICsgZS50YXJnZXQudmFsdWU7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aWYoZS50YXJnZXQuY2hlY2tlZCkge1xyXG5cdFx0XHRcdFx0dmFyIHVybCA9IHJlbW92ZVVSTFBhcmFtKHVybCwnYnJhbmRzJyk7XHJcblxyXG5cdFx0XHRcdFx0d2luZG93LmxvY2F0aW9uID0gdXJsICsgJyZicmFuZHM9JyArIGJyYW5kcyArICctLScgKyBlLnRhcmdldC52YWx1ZTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0aWYodXJsLmluZGV4T2YoJy0tJytlLnRhcmdldC52YWx1ZSkgIT0gLTEpIHtcclxuXHRcdFx0XHRcdFx0dmFyIHVybCA9IHVybC5yZXBsYWNlKCctLScrZS50YXJnZXQudmFsdWUsIFwiXCIpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0dmFyIHVybCA9IHVybC5yZXBsYWNlKGUudGFyZ2V0LnZhbHVlLCBcIlwiKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0d2luZG93LmxvY2F0aW9uID0gdXJsO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcdHJlbW92ZVVSTFBhcmFtZXRlcih1cmwsIHBhcmFtZXRlcikge1xyXG5cdFx0ICAgIC8vcHJlZmVyIHRvIHVzZSBsLnNlYXJjaCBpZiB5b3UgaGF2ZSBhIGxvY2F0aW9uL2xpbmsgb2JqZWN0XHJcblx0XHQgICAgdmFyIHVybHBhcnRzPSB1cmwuc3BsaXQoJz8nKTsgICBcclxuXHRcdCAgICBpZiAodXJscGFydHMubGVuZ3RoPj0yKSB7XHJcblxyXG5cdFx0ICAgICAgICB2YXIgcHJlZml4PSBlbmNvZGVVUklDb21wb25lbnQocGFyYW1ldGVyKSsnPSc7XHJcblx0XHQgICAgICAgIHZhciBwYXJzPSB1cmxwYXJ0c1sxXS5zcGxpdCgvWyY7XS9nKTtcclxuXHJcblx0XHQgICAgICAgIC8vcmV2ZXJzZSBpdGVyYXRpb24gYXMgbWF5IGJlIGRlc3RydWN0aXZlXHJcblx0XHQgICAgICAgIGZvciAodmFyIGk9IHBhcnMubGVuZ3RoOyBpLS0gPiAwOykgeyAgICBcclxuXHRcdCAgICAgICAgICAgIC8vaWRpb20gZm9yIHN0cmluZy5zdGFydHNXaXRoXHJcblx0XHQgICAgICAgICAgICBpZiAocGFyc1tpXS5sYXN0SW5kZXhPZihwcmVmaXgsIDApICE9PSAtMSkgeyAgXHJcblx0XHQgICAgICAgICAgICAgICAgcGFycy5zcGxpY2UoaSwgMSk7XHJcblx0XHQgICAgICAgICAgICB9XHJcblx0XHQgICAgICAgIH1cclxuXHJcblx0XHQgICAgICAgIHVybD0gdXJscGFydHNbMF0rJz8nK3BhcnMuam9pbignJicpO1xyXG5cdFx0ICAgICAgICByZXR1cm4gdXJsO1xyXG5cdFx0ICAgIH0gZWxzZSB7XHJcblx0XHQgICAgICAgIHJldHVybiB1cmw7XHJcblx0XHQgICAgfVxyXG5cdFx0fSxcclxuXHQgICAgaGFuZGxlRW50ZXI6IGZ1bmN0aW9uIChlKSB7XHJcblx0XHQgICAgaWYgKGUua2V5Q29kZSA9PSAxMykge1xyXG5cdFx0ICAgIFx0dmFyIHVybCA9IHJlbW92ZVVSTFBhcmFtKHdpbmRvdy5sb2NhdGlvbi5ocmVmLCAncHJvZF9uYW1lJyk7XHJcblx0XHRcdFx0d2luZG93LmxvY2F0aW9uID0gIHVybCArICcmcHJvZF9uYW1lPScgKyB0aGlzLmtleXdvcmQ7XHRcdCAgICBcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuICAgICAgICBhZGRUb0NhcnQoaWQsbmFtZSl7XHJcblx0ICAgICAgICBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L2FkZCcse1xyXG5cdCAgICAgICAgICAgIHBhcmFtczoge1xyXG5cdCAgICAgICAgICAgICAgICBwcm9kX2lkOmlkLFxyXG5cdCAgICAgICAgICAgICAgICBxdHk6MVxyXG5cdCAgICAgICAgICAgIH1cclxuXHQgICAgICAgIH0pXHJcblx0ICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG5cdCAgICAgICAgICAgICQoXCIjY2FydC1jb3VudCwgI2NhcnQtY291bnQtbW9iaWxlXCIpLnRleHQocmVzdWx0LmRhdGEuY2FydF9pdGVtX2NvdW50KTtcclxuICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLmVycm9yX21zZylcclxuICAgICAgICAgICAgICAgIHtcdFxyXG4gICAgICAgICAgICAgICAgXHRpZihyZXN1bHQuZGF0YS5lcnJvcl9tc2c9PSdSZWRpcmVjdCcpXHJcbiAgICAgICAgICAgICAgICBcdFx0d2luZG93LmxvY2F0aW9uID0gJy9wcm9kdWN0LycgKyByZXN1bHQuZGF0YS5zbHVnO1xyXG4gICAgICAgICAgICAgICAgXHRlbHNlXHJcblx0ICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQnXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG5cdCAgICAgICAgICAgICAgICB0b2FzdHIuc3VjY2Vzcyh0aGlzLmxhbmdbJ2FkZGVkLWNhcnQnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYWRkVG9Db21wYXJlKGlkLG5hbWUpe1xyXG4gICAgICAgICAgICBpZihMYXJhdmVsLmlzQXV0aGVudGljYXRlZCA9PT0gdHJ1ZSl7XHJcbiAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2FkZHRvY29tcGFyZS8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuXHRcdCAgICAgICAgICAgICAgICB2YXIgY29tcCA9IHBhcnNlSW50KCQoXCIjY29tcC1jb3VudFwiKS50ZXh0KCkpKzE7XHJcblx0XHQgICAgICAgICAgICAgICAgJChcIiNjb21wLWNvdW50XCIpLnRleHQoY29tcCkuc2hvdygpO1xyXG5cdFx0ICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMubGFuZ1snYWRkZWQtY29tcGFyaXNvbiddLCBuYW1lKTtcclxuXHRcdCAgICAgICAgICAgIH0gZWxzZSBpZihyZXN1bHQuZGF0YS5yZXNwb25zZT09Mykge1xyXG5cdFx0ICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHRoaXMubGFuZ1snZnVsbCddKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmluZm8odGhpcy5sYW5nWydhbHJlYWR5LWNvbXBhcmUnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydsb2dpbi1jb21wYXJlJ10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBhZGRUb0Zhdm9yaXRlcyhpZCxuYW1lKXtcclxuICAgICAgICAgICAgaWYoTGFyYXZlbC5pc0F1dGhlbnRpY2F0ZWQgPT09IHRydWUpe1xyXG4gICAgICAgICAgICAgICAgYXhpb3NBUEl2MS5nZXQoJy9hZGR0b2Zhdm9yaXRlcy8nICsgaWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0xKXtcclxuXHRcdCAgICAgICAgICAgICAgICB2YXIgZmF2ID0gcGFyc2VJbnQoJChcIiNmYXYtY291bnRcIikudGV4dCgpKSsxO1xyXG5cdFx0ICAgICAgICAgICAgICAgICQoXCIjZmF2LWNvdW50XCIpLnRleHQoZmF2KS5zaG93KCk7XHJcblx0XHQgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3ModGhpcy5sYW5nWydhZGRlZC1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcblx0XHQgICAgICAgICAgICB9IGVsc2UgaWYocmVzdWx0LmRhdGEucmVzcG9uc2U9PTIpe1xyXG4gICAgICAgICAgICAgICAgICAgIFx0dG9hc3RyLmVycm9yKHJlc3VsdC5kYXRhLmVycm9yX21zZywgdGhpcy5sYW5nWydub3QtYWRkZWQtd2lzaGxpc3QnXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBcdHRvYXN0ci5pbmZvKHRoaXMubGFuZ1snYWxyZWFkeS1mYXZvcml0ZXMnXSwgbmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0b2FzdHIuZXJyb3IodGhpcy5sYW5nWydsb2dpbi1mYXZvcml0ZXMnXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGZldGNoSXRlbXMocGFnZSkge1xyXG5cdFx0XHRheGlvc0FQSXYxLnBvc3QoJy9zZWFyY2gvcHJvZHVjdCcse1xyXG5cdFx0XHRcdHBhZ2U6IHBhZ2UsXHJcblx0XHRcdFx0a2V5d29yZDpnZXRVUkxQYXJhbWV0ZXIoJ3NlYXJjaCcpLFxyXG5cdFx0XHRcdHNvcnQ6Z2V0VVJMUGFyYW1ldGVyKCdwcmljZScpLFxyXG5cdFx0XHRcdHByb2RfbmFtZTpnZXRVUkxQYXJhbWV0ZXIoJ3Byb2RfbmFtZScpLFxyXG5cdFx0XHRcdG1pblByaWNlOiBnZXRVUkxQYXJhbWV0ZXIoJ21pbicpLFxyXG5cdFx0XHRcdG1heFByaWNlOiBnZXRVUkxQYXJhbWV0ZXIoJ21heCcpLFxyXG5cdFx0XHRcdGJyYW5kczogZ2V0VVJMUGFyYW1ldGVyKCdicmFuZHMnKVxyXG5cdFx0XHR9KSAgICAgIFxyXG5cdFx0XHQudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdHNldFByaWNlUmFuZ2VNYXgocmVzdWx0LmRhdGEubWF4UHJpY2UpO1xyXG5cclxuXHRcdFx0XHR0aGlzLml0ZW1zID0gcmVzdWx0LmRhdGEuZGF0YS5kYXRhO1xyXG5cdFx0XHRcdHRoaXMucGFnaW5hdGlvbiA9IHJlc3VsdC5kYXRhLnBhZ2luYXRpb247XHJcblx0ICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cdCAgICBjaGFuZ2VQYWdlKHBhZ2UpIHtcclxuXHQgICAgICAgIHRoaXMucGFnaW5hdGlvbi5jdXJyZW50X3BhZ2UgPSBwYWdlO1xyXG5cdCAgICAgICAgdGhpcy5mZXRjaEl0ZW1zKHBhZ2UpO1xyXG5cdCAgICB9LFxyXG5cdCAgICBzaG93UGFnaW5hdGlvbigpe1xyXG5cdCAgICBcdCAkKCcjcGFnaW5hdGlvbicpLnNob3coKTtcclxuXHQgICAgfSxcclxuXHQgICAgaGlkZVBhZ2luYXRpb24oKXtcclxuXHQgICAgXHQgJCgnI3BhZ2luYXRpb24nKS5oaWRlKCk7XHJcblx0ICAgIH1cclxuXHJcblx0fSxcclxuXHR3YXRjaDoge1xyXG5cdFx0aXRlbTogZnVuY3Rpb24gKGtleSkge1xyXG5cdFx0XHQkKCcuY2QtZ2FsbGVyeSB1bCcpLm1peEl0VXAoe1xyXG5cdFx0XHQgICAgY29udHJvbHM6IHtcclxuXHRcdFx0ICAgIFx0ZW5hYmxlOiBmYWxzZVxyXG5cdFx0XHQgICAgfSxcclxuXHRcdFx0ICAgIGNhbGxiYWNrczoge1xyXG5cdFx0XHQgICAgXHRvbk1peFN0YXJ0OiBmdW5jdGlvbigpe1xyXG5cdFx0XHQgICAgXHRcdCQoJy5jZC1mYWlsLW1lc3NhZ2UnKS5mYWRlT3V0KDIwMCk7XHJcblx0XHRcdCAgICBcdH0sXHJcblx0XHRcdCAgICAgIFx0b25NaXhGYWlsOiBmdW5jdGlvbigpe1xyXG5cdFx0XHQgICAgICBcdFx0JCgnLmNkLWZhaWwtbWVzc2FnZScpLmZhZGVJbigyMDApO1xyXG5cdFx0XHQgICAgXHR9XHJcblx0XHRcdCAgICB9XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH0sXHJcblx0dXBkYXRlZCgpIHtcclxuXHRcdC8vICQoJy5jZC1nYWxsZXJ5IHVsJykubWl4SXRVcCh7XHJcblx0XHQvLyBcdCAgICBjb250cm9sczoge1xyXG5cdFx0Ly8gXHQgICAgXHRlbmFibGU6IGZhbHNlXHJcblx0XHQvLyBcdCAgICB9LFxyXG5cdFx0Ly8gXHQgICAgY2FsbGJhY2tzOiB7XHJcblx0XHQvLyBcdCAgICBcdG9uTWl4U3RhcnQ6IGZ1bmN0aW9uKCl7XHJcblx0XHQvLyBcdCAgICBcdFx0JCgnLmNkLWZhaWwtbWVzc2FnZScpLmhpZGUoKTtcclxuXHRcdC8vIFx0ICAgIFx0fSxcclxuXHRcdC8vIFx0ICAgICAgXHRvbk1peEZhaWw6IGZ1bmN0aW9uKCl7XHJcblx0XHQvLyBcdCAgICAgIFx0XHQkKCcuY2QtZmFpbC1tZXNzYWdlJykuc2hvdygpO1xyXG5cdFx0Ly8gXHQgICAgXHR9XHJcblx0XHQvLyBcdCAgICB9XHJcblx0XHQvLyBcdH0pO1xyXG5cdH1cclxufSlcclxuXHJcbmZ1bmN0aW9uIG51bWJlcldpdGhDb21tYXMoeCkge1xyXG4gICAgcmV0dXJuIHgudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIixcIik7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJvdW5kT2ZmKHYpIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKHYpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRVUkxQYXJhbWV0ZXIoc1BhcmFtKVxyXG57XHJcbiAgICB2YXIgc1BhZ2VVUkwgPSB3aW5kb3cubG9jYXRpb24uc2VhcmNoLnN1YnN0cmluZygxKTtcclxuICAgIHZhciBzVVJMVmFyaWFibGVzID0gc1BhZ2VVUkwuc3BsaXQoJyYnKTtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc1VSTFZhcmlhYmxlcy5sZW5ndGg7IGkrKykgXHJcbiAgICB7XHJcbiAgICAgICAgdmFyIHNQYXJhbWV0ZXJOYW1lID0gc1VSTFZhcmlhYmxlc1tpXS5zcGxpdCgnPScpO1xyXG4gICAgICAgIGlmIChzUGFyYW1ldGVyTmFtZVswXSA9PSBzUGFyYW0pIFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcmV0dXJuIHNQYXJhbWV0ZXJOYW1lWzFdO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZnVuY3Rpb24gcmVtb3ZlVVJMUGFyYW0odXJsLCBwYXJhbSlcclxue1xyXG5cdCB2YXIgdXJscGFydHM9IHVybC5zcGxpdCgnPycpO1xyXG5cdCBpZiAodXJscGFydHMubGVuZ3RoPj0yKVxyXG5cdCB7XHJcblx0ICB2YXIgcHJlZml4PSBlbmNvZGVVUklDb21wb25lbnQocGFyYW0pKyc9JztcclxuXHQgIHZhciBwYXJzPSB1cmxwYXJ0c1sxXS5zcGxpdCgvWyY7XS9nKTtcclxuXHQgIGZvciAodmFyIGk9cGFycy5sZW5ndGg7IGktLSA+IDA7KVxyXG5cdCAgIGlmIChwYXJzW2ldLmluZGV4T2YocHJlZml4LCAwKT09MClcclxuXHQgICAgcGFycy5zcGxpY2UoaSwgMSk7XHJcblx0ICBpZiAocGFycy5sZW5ndGggPiAwKVxyXG5cdCAgIHJldHVybiB1cmxwYXJ0c1swXSsnPycrcGFycy5qb2luKCcmJyk7XHJcblx0ICBlbHNlXHJcblx0ICAgcmV0dXJuIHVybHBhcnRzWzBdO1xyXG5cdCB9XHJcblx0IGVsc2VcclxuXHQgIHJldHVybiB1cmw7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNldFByaWNlUmFuZ2VNYXgobWF4UHJpY2UpIHtcclxuXHR3aW5kb3cuc2xpZGVyLnVwZGF0ZSh7XHJcblx0XHRtaW46IDAsXHJcblx0XHRmcm9tOiAwLFxyXG5cclxuXHRcdG1heDogbWF4UHJpY2UsXHJcblx0XHR0bzogbWF4UHJpY2VcclxuXHR9KTtcclxufVxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3BhZ2VzL3Byb2R1Y3QtbGlzdC5qcyIsIjx0ZW1wbGF0ZT5cclxuICA8ZGl2IGNsYXNzPVwibWl4IGNvbC1zbS0zXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwiYm94LXByb2R1Y3Qtb3V0ZXJcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImJveC1wcm9kdWN0XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImltZy13cmFwcGVyXCI+XHJcbiAgICAgICAgICA8YSA6aHJlZj1cIicvcHJvZHVjdC8nICsgaXRlbS5zbHVnXCI+XHJcbiAgICAgICAgICAgIDxpbWcgYWx0PVwiUHJvZHVjdFwiIHYtYmluZDpzcmM9XCInL3Nob3BwaW5nL2ltYWdlcy9wcm9kdWN0LycraXRlbS5pZCsnL3ByaW1hcnkuanBnJ1wiIG9uRXJyb3I9XCJ0aGlzLnNyYyA9ICcvaW1hZ2VzL2F2YXRhci9kZWZhdWx0LXByb2R1Y3QuanBnJ1wiPlxyXG4gICAgICAgICAgICA8IS0tIDxpbWcgYWx0PVwiUHJvZHVjdFwiIHYtYmluZDpzcmM9XCJpdGVtLmltYWdlXCIgb25FcnJvcj1cInRoaXMuc3JjID0gJy9pbWFnZXMvYXZhdGFyL2RlZmF1bHQtcHJvZHVjdC5qcGcnXCI+IC0tPlxyXG4gICAgICAgICAgICA8IS0tIG9uRXJyb3I9XCJ0aGlzLnNyYyA9ICcvaW1hZ2VzL2F2YXRhci9kZWZhdWx0LXByb2R1Y3QuanBnJ1wiIC0tPlxyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cInRhZ3NcIiB2LWlmPSdpdGVtLnNhbGVfcHJpY2UnPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImxhYmVsLXRhZ3NcIj48c3BhbiBjbGFzcz1cImxhYmVsIGxhYmVsLWRhbmdlciBhcnJvd2VkXCI+e3tpdGVtLmRpc2NvdW50fX0lPC9zcGFuPjwvc3Bhbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cIm9wdGlvblwiPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIiB0aXRsZT1cIkFkZCB0byBDYXJ0XCIgZGF0YS1vcmlnaW5hbC10aXRsZT1cIkFkZCB0byBDYXJ0XCIgQGNsaWNrPVwiYWRkVG9DYXJ0KClcIj48aSBjbGFzcz1cImZhIGZhLXNob3BwaW5nLWNhcnRcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgdGl0bGU9XCJBZGQgdG8gQ29tcGFyZVwiPjxpIGNsYXNzPVwiZmEgZmEtYWxpZ24tbGVmdFwiIEBjbGljaz1cImFkZFRvQ29tcGFyZSgpXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIHRpdGxlPVwiQWRkIHRvIFdpc2hsaXN0XCIgY2xhc3M9XCJ3aXNobGlzdFwiIEBjbGljaz1cImFkZFRvRmF2b3JpdGVzKClcIj48aSBjbGFzcz1cImZhIGZhLWhlYXJ0XCI+PC9pPjwvYT5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxoNj48YSA6aHJlZj1cIicvcHJvZHVjdC8nICsgaXRlbS5zbHVnXCI+e3tuYW1lbGVuZ3RofX08L2E+PC9oNj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwicHJpY2VcIj5cclxuICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgIDxzcGFuIHYtaWY9J2l0ZW0uc2FsZV9wcmljZSc+e3twYXJzZUZsb2F0KFwiMFwiK3NhbGVwcmljZSkgfCBjdXJyZW5jeSB9fTwvc3Bhbj4gXHJcbiAgICAgICAgICAgIDxzcGFuIDpjbGFzcz1cIml0ZW0uc2FsZV9wcmljZT8ncHJpY2Utb2xkJzonJ1wiPnt7cGFyc2VGbG9hdChcIjBcIitwcmljZSkgfCBjdXJyZW5jeX19PC9zcGFuPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJhdGluZy1ibG9ja1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3Rhci1yYXRpbmdzLXNwcml0ZS1zbWFsbFwiPjxzcGFuIDpzdHlsZT1cInByb2R1Y3RSYXRpbmdcIiBjbGFzcz1cInN0YXItcmF0aW5ncy1zcHJpdGUtc21hbGwtcmF0aW5nXCI+PC9zcGFuPjwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2Plx0XHJcbjwvdGVtcGxhdGU+XHJcblxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cdHByb3BzOiBbJ2l0ZW0nXSxcclxuXHRtZXRob2RzOntcclxuXHRcdHNlbGVjdChpZCl7XHJcblx0XHRcdCB0aGlzLiRwYXJlbnQuc2VsZWN0SXRlbShpZClcclxuXHRcdH0sXHJcbiAgICBhZGRUb0NhcnQoKXtcclxuICAgICAgIHRoaXMuJHBhcmVudC5hZGRUb0NhcnQodGhpcy5pdGVtLmlkLHRoaXMuaXRlbS5uYW1lKVxyXG4gICAgfSxcclxuICAgIGFkZFRvQ29tcGFyZSgpe1xyXG4gICAgICAgdGhpcy4kcGFyZW50LmFkZFRvQ29tcGFyZSh0aGlzLml0ZW0uaWQsdGhpcy5pdGVtLm5hbWUpXHJcbiAgICB9LFxyXG4gICAgYWRkVG9GYXZvcml0ZXMoKXtcclxuICAgICAgIHRoaXMuJHBhcmVudC5hZGRUb0Zhdm9yaXRlcyh0aGlzLml0ZW0uaWQsdGhpcy5pdGVtLm5hbWUpXHJcbiAgICB9XHJcblx0fSxcclxuICBjb21wdXRlZDp7XHJcbiAgICBwcmljZSgpe1xyXG4gICAgICBpZih0aGlzLml0ZW0uZmVlX2luY2x1ZGVkPT0xKVxyXG4gICAgICAgIHJldHVybiBwYXJzZUZsb2F0KHRoaXMuaXRlbS5wcmljZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0uY2hhcmdlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLnZhdCk7XHJcbiAgICAgIGVsc2VcclxuICAgICAgICByZXR1cm4gdGhpcy5pdGVtLnByaWNlO1xyXG4gICAgfSxcclxuICAgIHNhbGVwcmljZSgpe1xyXG4gICAgICBpZih0aGlzLml0ZW0uZmVlX2luY2x1ZGVkPT0xKVxyXG4gICAgICAgIHJldHVybiBwYXJzZUZsb2F0KHRoaXMuaXRlbS5zYWxlX3ByaWNlKSArIHBhcnNlRmxvYXQodGhpcy5pdGVtLnNoaXBwaW5nX2ZlZSkgKyBwYXJzZUZsb2F0KHRoaXMuaXRlbS5jaGFyZ2UpICsgcGFyc2VGbG9hdCh0aGlzLml0ZW0udmF0KTtcclxuICAgICAgZWxzZVxyXG4gICAgICAgIHJldHVybiB0aGlzLml0ZW0uc2FsZV9wcmljZTtcclxuICAgIH0sXHJcbiAgICBuYW1lbGVuZ3RoKCl7XHJcbiAgICAgIHZhciBuYW1lID0gdGhpcy5pdGVtLmNuX25hbWUgPyB0aGlzLml0ZW0uY25fbmFtZTogdGhpcy5pdGVtLm5hbWU7XHJcbiAgICAgIGlmICh0aGlzLml0ZW0ubmFtZS5sZW5ndGggPiA0NSkge1xyXG4gICAgICAgIGlmKHRoaXMuJHBhcmVudC5sYW5nbW9kZT09XCJjblwiKXtcclxuICAgICAgICAgICAgcmV0dXJuIG5hbWUuc3Vic3RyKDAsIDQ1KStcIi4uLlwiOyAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLml0ZW0ubmFtZS5zdWJzdHIoMCwgNDUpK1wiLi4uXCI7XHJcbiAgICAgIH1cclxuICAgICAgZWxzZXtcclxuICAgICAgICBpZih0aGlzLiRwYXJlbnQubGFuZ21vZGU9PVwiY25cIil7XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lOyAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLml0ZW0ubmFtZTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHN0YXJSYXRpbmcoKXtcclxuICAgICAgcmV0dXJuIFwid2lkdGg6XCIrTWF0aC5yb3VuZCgodGhpcy5pdGVtLnJhdGUvdGhpcy5pdGVtLmNvdW50KSoyMCkrXCIlXCI7XHJcbiAgICB9LFxyXG4gICAgcHJvZHVjdFJhdGluZygpe1xyXG4gICAgICByZXR1cm4gXCJ3aWR0aDpcIit0aGlzLml0ZW0ucHJvZHVjdF9yYXRpbmcrXCIlXCI7XHJcbiAgICB9LFxyXG5cclxuICB9XHJcbn1cclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFByb2R1Y3QudnVlPzI2ZWNkNDg0IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vUHJvZHVjdC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTc3OGY3NWYzXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1Byb2R1Y3QudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXHByb2R1Y3RcXFxcUHJvZHVjdC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBQcm9kdWN0LnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi03NzhmNzVmM1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTc3OGY3NWYzXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L1Byb2R1Y3QudnVlXG4vLyBtb2R1bGUgaWQgPSAzNlxuLy8gbW9kdWxlIGNodW5rcyA9IDUgMjAiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtaXggY29sLXNtLTNcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJib3gtcHJvZHVjdC1vdXRlclwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJveC1wcm9kdWN0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiaW1nLXdyYXBwZXJcIlxuICB9LCBbX2MoJ2EnLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaHJlZlwiOiAnL3Byb2R1Y3QvJyArIF92bS5pdGVtLnNsdWdcbiAgICB9XG4gIH0sIFtfYygnaW1nJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImFsdFwiOiBcIlByb2R1Y3RcIixcbiAgICAgIFwic3JjXCI6ICcvc2hvcHBpbmcvaW1hZ2VzL3Byb2R1Y3QvJyArIF92bS5pdGVtLmlkICsgJy9wcmltYXJ5LmpwZycsXG4gICAgICBcIm9uRXJyb3JcIjogXCJ0aGlzLnNyYyA9ICcvaW1hZ2VzL2F2YXRhci9kZWZhdWx0LXByb2R1Y3QuanBnJ1wiXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLml0ZW0uc2FsZV9wcmljZSkgPyBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInRhZ3NcIlxuICB9LCBbX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGFiZWwtdGFnc1wiXG4gIH0sIFtfYygnc3BhbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsYWJlbCBsYWJlbC1kYW5nZXIgYXJyb3dlZFwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5pdGVtLmRpc2NvdW50KSArIFwiJVwiKV0pXSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm9wdGlvblwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuICAgICAgXCJkYXRhLXBsYWNlbWVudFwiOiBcInRvcFwiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkFkZCB0byBDYXJ0XCIsXG4gICAgICBcImRhdGEtb3JpZ2luYWwtdGl0bGVcIjogXCJBZGQgdG8gQ2FydFwiXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmFkZFRvQ2FydCgpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX2MoJ2knLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtc2hvcHBpbmctY2FydFwiXG4gIH0pXSksIF92bS5fdihcIiBcIiksIF9jKCdhJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiQWRkIHRvIENvbXBhcmVcIlxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWFsaWduLWxlZnRcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLmFkZFRvQ29tcGFyZSgpXG4gICAgICB9XG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ3aXNobGlzdFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIixcbiAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG4gICAgICBcInRpdGxlXCI6IFwiQWRkIHRvIFdpc2hsaXN0XCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uYWRkVG9GYXZvcml0ZXMoKVxuICAgICAgfVxuICAgIH1cbiAgfSwgW19jKCdpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImZhIGZhLWhlYXJ0XCJcbiAgfSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdoNicsIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdC8nICsgX3ZtLml0ZW0uc2x1Z1xuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLm5hbWVsZW5ndGgpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicHJpY2VcIlxuICB9LCBbX2MoJ2RpdicsIFsoX3ZtLml0ZW0uc2FsZV9wcmljZSkgPyBfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHBhcnNlRmxvYXQoXCIwXCIgKyBfdm0uc2FsZXByaWNlKSkpKV0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdzcGFuJywge1xuICAgIGNsYXNzOiBfdm0uaXRlbS5zYWxlX3ByaWNlID8gJ3ByaWNlLW9sZCcgOiAnJ1xuICB9LCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShwYXJzZUZsb2F0KFwiMFwiICsgX3ZtLnByaWNlKSkpKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJyYXRpbmctYmxvY2tcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJzdGFyLXJhdGluZ3Mtc3ByaXRlLXNtYWxsXCJcbiAgfSwgW19jKCdzcGFuJywge1xuICAgIHN0YXRpY0NsYXNzOiBcInN0YXItcmF0aW5ncy1zcHJpdGUtc21hbGwtcmF0aW5nXCIsXG4gICAgc3R5bGU6IChfdm0ucHJvZHVjdFJhdGluZylcbiAgfSldKV0pXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTc3OGY3NWYzXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNzc4Zjc1ZjNcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9wcm9kdWN0L1Byb2R1Y3QudnVlXG4vLyBtb2R1bGUgaWQgPSA0MlxuLy8gbW9kdWxlIGNodW5rcyA9IDUgMjAiXSwic291cmNlUm9vdCI6IiJ9