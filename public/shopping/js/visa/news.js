/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 345);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 159:
/***/ (function(module, exports, __webpack_require__) {



Vue.component('news-articles', __webpack_require__(288));
Vue.component('news-articles-featured', __webpack_require__(287));

new Vue({
  el: '#newsLoader'
});

new Vue({
  el: '#newsLoader2'

});

Vue.filter('formatDate', function (time) {
  var date = new Date((time || "").replace(/-/g, "/").replace(/[TZ]/g, " ")),
      diff = (new Date().getTime() - date.getTime()) / 1000,
      day_diff = Math.floor(diff / 86400);

  if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31) return;

  return day_diff == 0 && (diff < 60 && "just now" || diff < 120 && "1 minute ago" || diff < 3600 && Math.floor(diff / 60) + " minutes ago" || diff < 7200 && "1 hour ago" || diff < 86400 && Math.floor(diff / 3600) + " hours ago") || day_diff == 1 && "Yesterday" || day_diff < 7 && day_diff + " days ago" || day_diff < 31 && Math.ceil(day_diff / 7) + " weeks ago";
});

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var user = window.Laravel.user;

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: user,
      news_item: [],
      most_latest: []
    };
  },

  methods: {
    fetchMessages: function fetchMessages() {
      var _this = this;

      window.axios.get('news').then(function (res) {
        _this.news_item = res.data;

        //console.log(res.data);
      });
      window.axios.get('news/latest').then(function (res) {
        _this.most_latest = res.data;

        //console.log(res.data);
      });
    },
    truncate: function truncate(text, length, suffix) {
      if (text.replace(/(<([^>]+)>)/ig, "").length < length) {
        return text.replace(/(<([^>]+)>)/ig, "").substring(0, length);
      } else {
        return text.replace(/(<([^>]+)>)/ig, "").substring(0, length) + suffix;
      }
    }

  },

  computed: {
    countResults: function countResults() {
      return this.news_item.length;
    },
    isEmpty: function isEmpty() {
      return jQuery.isEmptyObject(this.user);
    }

  },

  created: function created() {
    this.fetchMessages();
  }
});

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var user = window.Laravel.user;

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: user,
      news_item: []
    };
  },

  methods: {
    fetchMessages: function fetchMessages() {
      var _this = this;

      window.axios.get('news/featured').then(function (res) {
        _this.news_item = res.data;

        //console.log(res.data);
      });
    },
    truncate: function truncate(text, length, suffix) {
      if (text.replace(/(<([^>]+)>)/ig, "").length < length) {
        return text.replace(/(<([^>]+)>)/ig, "").substring(0, length);
      } else {
        return text.replace(/(<([^>]+)>)/ig, "").substring(0, length) + suffix;
      }
    }

  },

  computed: {
    countResults: function countResults() {
      return this.news_item.length;
    },
    isEmpty: function isEmpty() {
      return jQuery.isEmptyObject(this.user);
    }

  },

  created: function created() {
    this.fetchMessages();
  }
});

/***/ }),

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(237),
  /* template */
  __webpack_require__(310),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/shopping/visa/components/news/Featured_news.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Featured_news.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7d72f43e", Component.options)
  } else {
    hotAPI.reload("data-v-7d72f43e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(238),
  /* template */
  __webpack_require__(296),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/shopping/visa/components/news/News.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] News.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-09f8ad74", Component.options)
  } else {
    hotAPI.reload("data-v-09f8ad74", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 296:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', _vm._l((_vm.news_item), function(news_items) {
    return _c('div', [_c('div', {
      staticClass: "media"
    }, [_c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "col-xs-12 col-sm-6 col-md-4"
    }, [_c('img', {
      staticClass: "img-fluid img-responsive",
      attrs: {
        "src": '../../images/news/' + news_items.image
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "col-xs-12 col-sm-6 col-md-8"
    }, [_c('div', {
      staticClass: "media-body"
    }, [_c('div', {
      staticClass: "choice_text",
      staticStyle: {
        "margin-top": "0 !important"
      }
    }, [_c('div', {
      staticClass: "date"
    }, [_c('a', {
      attrs: {
        "href": "#"
      }
    }, [_c('i', {
      staticClass: "fa fa-calendar",
      attrs: {
        "aria-hidden": "true"
      }
    }), _vm._v(_vm._s(_vm._f("formatDate")(news_items.created_at)))])]), _vm._v(" "), _c('a', {
      attrs: {
        "href": '/news/' + news_items.news_id
      }
    }, [_c('h4', {
      staticStyle: {
        "font-size": "15px !important"
      }
    }, [_vm._v(_vm._s(news_items.header))])]), _vm._v(" "), _c('span', {
      staticClass: "more",
      staticStyle: {
        "font-size": "12px !important"
      },
      domProps: {
        "innerHTML": _vm._s(_vm.truncate(news_items.content, 300, '...'))
      }
    })])])])])])])
  }), 0)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-09f8ad74", module.exports)
  }
}

/***/ }),

/***/ 310:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_vm._l((_vm.most_latest), function(most_latest) {
    return _c('div', [_c('div', {
      staticClass: "choice_item"
    }, [_c('img', {
      staticClass: "img-fluid",
      attrs: {
        "src": '../../images/news/' + most_latest.image,
        "alt": ""
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "choice_text"
    }, [_c('div', {
      staticClass: "date"
    }, [_c('a', {
      attrs: {
        "href": "#"
      }
    }, [_c('i', {
      staticClass: "fa fa-calendar",
      attrs: {
        "aria-hidden": "true"
      }
    }), _vm._v(_vm._s(_vm._f("formatDate")(most_latest.created_at)))])]), _vm._v(" "), _c('a', {
      attrs: {
        "href": '/news/' + most_latest.news_id
      }
    }, [_c('h4', [_vm._v(_vm._s(most_latest.header))])]), _vm._v(" "), _c('span', {
      staticClass: "more",
      staticStyle: {
        "font-size": "12px !important"
      },
      domProps: {
        "innerHTML": _vm._s(_vm.truncate(most_latest.content, 200, '...'))
      }
    })])])])
  }), _vm._v(" "), _c('div', {
    staticClass: "product_list p-left"
  }, _vm._l((_vm.news_item), function(news_item) {
    return _c('div', [_c('div', {
      staticClass: "media"
    }, [_c('div', {
      staticClass: "d-flex"
    }, [_c('img', {
      staticStyle: {
        "width": "150px !important",
        "padding": "5px"
      },
      attrs: {
        "src": '../../images/news/' + news_item.image
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "media-body"
    }, [_c('div', {
      staticClass: "choice_text"
    }, [_c('a', {
      attrs: {
        "href": '/news/' + news_item.news_id
      }
    }, [_c('h4', {
      staticStyle: {
        "font-size": "12px"
      }
    }, [_vm._v(_vm._s(news_item.header))])]), _vm._v(" "), _c('div', {
      staticClass: "date"
    }, [_c('a', {
      attrs: {
        "href": "#"
      }
    }, [_c('i', {
      staticClass: "fa fa-calendar",
      attrs: {
        "aria-hidden": "true"
      }
    }), _vm._v(_vm._s(_vm._f("formatDate")(news_item.created_at)))])])])])])])
  }), 0)], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7d72f43e", module.exports)
  }
}

/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(159);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZDFhMWZiOWUyZTI5MTVjMWQ1ZDY/MGUzMCoqKioqKiIsIndlYnBhY2s6Ly8vLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzP2Q0ZjMqKioqKiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3Zpc2EvbmV3cy5qcyIsIndlYnBhY2s6Ly8vRmVhdHVyZWRfbmV3cy52dWUiLCJ3ZWJwYWNrOi8vL05ld3MudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvdmlzYS9jb21wb25lbnRzL25ld3MvRmVhdHVyZWRfbmV3cy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy92aXNhL2NvbXBvbmVudHMvbmV3cy9OZXdzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3Zpc2EvY29tcG9uZW50cy9uZXdzL05ld3MudnVlPzRmNDEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy92aXNhL2NvbXBvbmVudHMvbmV3cy9GZWF0dXJlZF9uZXdzLnZ1ZT8yMzgxIl0sIm5hbWVzIjpbIlZ1ZSIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJlbCIsImZpbHRlciIsInRpbWUiLCJkYXRlIiwiRGF0ZSIsInJlcGxhY2UiLCJkaWZmIiwiZ2V0VGltZSIsImRheV9kaWZmIiwiTWF0aCIsImZsb29yIiwiaXNOYU4iLCJjZWlsIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQ2hEQUEsSUFBSUMsU0FBSixDQUFjLGVBQWQsRUFBK0JDLG1CQUFPQSxDQUFDLEdBQVIsQ0FBL0I7QUFDQUYsSUFBSUMsU0FBSixDQUFjLHdCQUFkLEVBQXdDQyxtQkFBT0EsQ0FBQyxHQUFSLENBQXhDOztBQUVBLElBQUlGLEdBQUosQ0FBUTtBQUNORyxNQUFJO0FBREUsQ0FBUjs7QUFJQSxJQUFJSCxHQUFKLENBQVE7QUFDTkcsTUFBSTs7QUFERSxDQUFSOztBQUtBSCxJQUFJSSxNQUFKLENBQVcsWUFBWCxFQUF5QixVQUFTQyxJQUFULEVBQWM7QUFDckMsTUFBSUMsT0FBTyxJQUFJQyxJQUFKLENBQVMsQ0FBQ0YsUUFBUSxFQUFULEVBQWFHLE9BQWIsQ0FBcUIsSUFBckIsRUFBMEIsR0FBMUIsRUFBK0JBLE9BQS9CLENBQXVDLE9BQXZDLEVBQStDLEdBQS9DLENBQVQsQ0FBWDtBQUFBLE1BQ0VDLE9BQVEsQ0FBRSxJQUFJRixJQUFKLEVBQUQsQ0FBYUcsT0FBYixLQUF5QkosS0FBS0ksT0FBTCxFQUExQixJQUE0QyxJQUR0RDtBQUFBLE1BRUVDLFdBQVdDLEtBQUtDLEtBQUwsQ0FBV0osT0FBTyxLQUFsQixDQUZiOztBQUlGLE1BQUtLLE1BQU1ILFFBQU4sS0FBbUJBLFdBQVcsQ0FBOUIsSUFBbUNBLFlBQVksRUFBcEQsRUFFSTs7QUFFSixTQUFPQSxZQUFZLENBQVosS0FDQ0YsT0FBTyxFQUFQLElBQWEsVUFBYixJQUNBQSxPQUFPLEdBQVAsSUFBYyxjQURkLElBRUFBLE9BQU8sSUFBUCxJQUFlRyxLQUFLQyxLQUFMLENBQVlKLE9BQU8sRUFBbkIsSUFBMEIsY0FGekMsSUFHQUEsT0FBTyxJQUFQLElBQWUsWUFIZixJQUlBQSxPQUFPLEtBQVAsSUFBZ0JHLEtBQUtDLEtBQUwsQ0FBWUosT0FBTyxJQUFuQixJQUE0QixZQUw3QyxLQU1IRSxZQUFZLENBQVosSUFBaUIsV0FOZCxJQU9IQSxXQUFXLENBQVgsSUFBZ0JBLFdBQVcsV0FQeEIsSUFRSEEsV0FBVyxFQUFYLElBQWlCQyxLQUFLRyxJQUFMLENBQVdKLFdBQVcsQ0FBdEIsSUFBNEIsWUFSakQ7QUFTQyxDQWxCRCxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN5QkE7O0FBRUE7QUFDQSxNQURBLGtCQUNBO0FBQ0E7QUFDQSxnQkFEQTtBQUVBLG1CQUZBO0FBR0E7QUFIQTtBQUtBLEdBUEE7O0FBUUE7QUFDQTtBQUFBOztBQUNBO0FBQ0E7O0FBRUE7QUFDQSxPQUpBO0FBS0E7QUFDQTs7QUFFQTtBQUNBLE9BSkE7QUFLQSxLQVpBO0FBYUE7QUFDQTtBQUNBO0FBQ0EsT0FGQSxNQUVBO0FBQ0E7QUFDQTtBQUVBOztBQXBCQSxHQVJBOztBQWdDQTtBQUNBO0FBQ0E7QUFDQSxLQUhBO0FBSUE7QUFDQTtBQUNBOztBQU5BLEdBaENBOztBQTBDQSxTQTFDQSxxQkEwQ0E7QUFDQTtBQUNBO0FBNUNBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7O0FBRUE7QUFDQSxNQURBLGtCQUNBO0FBQ0E7QUFDQSxnQkFEQTtBQUVBO0FBRkE7QUFJQSxHQU5BOztBQU9BO0FBQ0E7QUFBQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0EsT0FKQTtBQUtBLEtBUEE7QUFRQTtBQUNBO0FBQ0E7QUFDQSxPQUZBLE1BRUE7QUFDQTtBQUNBO0FBRUE7O0FBZkEsR0FQQTs7QUEwQkE7QUFDQTtBQUNBO0FBQ0EsS0FIQTtBQUlBO0FBQ0E7QUFDQTs7QUFOQSxHQTFCQTs7QUFvQ0EsU0FwQ0EscUJBb0NBO0FBQ0E7QUFDQTtBQXRDQSxHOzs7Ozs7O0FDM0NBLGdCQUFnQixtQkFBTyxDQUFDLENBQXdFO0FBQ2hHO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQThPO0FBQ3hQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTZNO0FBQ3ZOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQU8sQ0FBQyxDQUF3RTtBQUNoRztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFxTztBQUMvTztBQUNBLEVBQUUsbUJBQU8sQ0FBQyxHQUFvTTtBQUM5TTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLElBQUksS0FBVSxHQUFHO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0EsSUFBSSxLQUFVO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDMURBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBLElBQUksS0FBVTtBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy92aXNhL25ld3MuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDM0NSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZDFhMWZiOWUyZTI5MTVjMWQ1ZDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiIsIlxuXG5WdWUuY29tcG9uZW50KCduZXdzLWFydGljbGVzJywgcmVxdWlyZSgnLi4vdmlzYS9jb21wb25lbnRzL25ld3MvTmV3cy52dWUnKSk7XG5WdWUuY29tcG9uZW50KCduZXdzLWFydGljbGVzLWZlYXR1cmVkJywgcmVxdWlyZSgnLi4vdmlzYS9jb21wb25lbnRzL25ld3MvRmVhdHVyZWRfbmV3cy52dWUnKSk7XG5cbm5ldyBWdWUoe1xuICBlbDogJyNuZXdzTG9hZGVyJyxcbn0pO1xuXG5uZXcgVnVlKHtcbiAgZWw6ICcjbmV3c0xvYWRlcjInLFxuICBcbn0pO1xuXG5WdWUuZmlsdGVyKCdmb3JtYXREYXRlJywgZnVuY3Rpb24odGltZSl7XG4gIHZhciBkYXRlID0gbmV3IERhdGUoKHRpbWUgfHwgXCJcIikucmVwbGFjZSgvLS9nLFwiL1wiKS5yZXBsYWNlKC9bVFpdL2csXCIgXCIpKSxcbiAgICBkaWZmID0gKCgobmV3IERhdGUoKSkuZ2V0VGltZSgpIC0gZGF0ZS5nZXRUaW1lKCkpIC8gMTAwMCksXG4gICAgZGF5X2RpZmYgPSBNYXRoLmZsb29yKGRpZmYgLyA4NjQwMCk7XG5cbmlmICggaXNOYU4oZGF5X2RpZmYpIHx8IGRheV9kaWZmIDwgMCB8fCBkYXlfZGlmZiA+PSAzMSApXG5cbiAgICByZXR1cm47XG5cbnJldHVybiBkYXlfZGlmZiA9PSAwICYmIChcbiAgICAgICAgZGlmZiA8IDYwICYmIFwianVzdCBub3dcIiB8fFxuICAgICAgICBkaWZmIDwgMTIwICYmIFwiMSBtaW51dGUgYWdvXCIgfHxcbiAgICAgICAgZGlmZiA8IDM2MDAgJiYgTWF0aC5mbG9vciggZGlmZiAvIDYwICkgKyBcIiBtaW51dGVzIGFnb1wiIHx8XG4gICAgICAgIGRpZmYgPCA3MjAwICYmIFwiMSBob3VyIGFnb1wiIHx8XG4gICAgICAgIGRpZmYgPCA4NjQwMCAmJiBNYXRoLmZsb29yKCBkaWZmIC8gMzYwMCApICsgXCIgaG91cnMgYWdvXCIpIHx8XG4gICAgZGF5X2RpZmYgPT0gMSAmJiBcIlllc3RlcmRheVwiIHx8XG4gICAgZGF5X2RpZmYgPCA3ICYmIGRheV9kaWZmICsgXCIgZGF5cyBhZ29cIiB8fFxuICAgIGRheV9kaWZmIDwgMzEgJiYgTWF0aC5jZWlsKCBkYXlfZGlmZiAvIDcgKSArIFwiIHdlZWtzIGFnb1wiO1xufSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3Zpc2EvbmV3cy5qcyIsIjx0ZW1wbGF0ZT5cbjxkaXY+XG4gIDxkaXYgdi1mb3I9XCJtb3N0X2xhdGVzdCBpbiBtb3N0X2xhdGVzdFwiID5cbiAgPGRpdiBjbGFzcz1cImNob2ljZV9pdGVtXCI+XG4gICAgPGltZyBjbGFzcz1cImltZy1mbHVpZFwiIDpzcmM9XCInLi4vLi4vaW1hZ2VzL25ld3MvJyttb3N0X2xhdGVzdC5pbWFnZVwiIGFsdD1cIlwiPlxuICAgIDxkaXYgY2xhc3M9XCJjaG9pY2VfdGV4dFwiPlxuICAgICAgPGRpdiBjbGFzcz1cImRhdGVcIj5cbiAgICAgICAgPGEgaHJlZj1cIiNcIj48aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPnt7bW9zdF9sYXRlc3QuY3JlYXRlZF9hdHxmb3JtYXREYXRlfX08L2E+XG4gICAgICAgIDwhLS08YSBocmVmPVwiI1wiPjxpIGNsYXNzPVwiZmEgZmEtY29tbWVudHMtb1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT4wNTwvYT4tLT5cbiAgICAgIDwvZGl2PlxuICAgICAgPGEgOmhyZWY9XCInL25ld3MvJyArIG1vc3RfbGF0ZXN0Lm5ld3NfaWRcIj48aDQ+e3ttb3N0X2xhdGVzdC5oZWFkZXJ9fTwvaDQ+PC9hPlxuICAgICAgPHNwYW4gY2xhc3M9XCJtb3JlXCIgdi1odG1sPVwidHJ1bmNhdGUobW9zdF9sYXRlc3QuY29udGVudCwyMDAsICcuLi4nKVwiIHN0eWxlPVwiZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnRcIj48L3NwYW4+XG4gICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gIDwvZGl2PlxuICA8ZGl2IGNsYXNzPVwicHJvZHVjdF9saXN0IHAtbGVmdFwiPlxuICAgIDxkaXYgdi1mb3I9XCJuZXdzX2l0ZW0gaW4gbmV3c19pdGVtXCIgPlxuICAgICAgPGRpdiBjbGFzcz1cIm1lZGlhXCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJkLWZsZXhcIj5cbiAgICAgICAgICA8aW1nIDpzcmM9XCInLi4vLi4vaW1hZ2VzL25ld3MvJytuZXdzX2l0ZW0uaW1hZ2VcIiAgc3R5bGU9XCJ3aWR0aDoxNTBweCAhaW1wb3J0YW50OyBwYWRkaW5nOjVweDtcIj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJtZWRpYS1ib2R5XCI+XG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNob2ljZV90ZXh0XCI+XG4gICAgICAgICAgICA8YSA6aHJlZj1cIicvbmV3cy8nICsgbmV3c19pdGVtLm5ld3NfaWRcIj48aDQgc3R5bGU9XCJmb250LXNpemU6MTJweDtcIj57e25ld3NfaXRlbS5oZWFkZXJ9fTwvaDQ+PC9hPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImRhdGVcIj5cbiAgICAgICAgICAgICAgPGEgaHJlZj1cIiNcIj48aSBjbGFzcz1cImZhIGZhLWNhbGVuZGFyXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPnt7bmV3c19pdGVtLmNyZWF0ZWRfYXR8Zm9ybWF0RGF0ZX19PC9hPlxuICAgICAgICAgICAgICA8IS0tPGEgaHJlZj1cIiNcIj48aSBjbGFzcz1cImZhIGZhLWNvbW1lbnRzLW9cIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+MDU8L2E+LS0+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cblxuICAgIDwvZGl2PlxuICA8L2Rpdj5cblxuPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxubGV0IHVzZXIgPSB3aW5kb3cuTGFyYXZlbC51c2VyO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIGRhdGEoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHVzZXI6IHVzZXIsXG4gICAgICBuZXdzX2l0ZW06IFtdLFxuICAgICAgbW9zdF9sYXRlc3Q6IFtdXG4gICAgfVxuICB9LFxuICBtZXRob2RzOiB7XG4gICAgZmV0Y2hNZXNzYWdlczogZnVuY3Rpb24oKSB7XG4gICAgICAgIHdpbmRvdy5heGlvcy5nZXQoJ25ld3MnKS50aGVuKChyZXMpID0+IHtcbiAgICAgICAgICAgIHRoaXMubmV3c19pdGVtID0gcmVzLmRhdGE7XG5cbiAgICAgICAgICAgIC8vY29uc29sZS5sb2cocmVzLmRhdGEpO1xuICAgICAgICB9KTtcbiAgICAgICAgd2luZG93LmF4aW9zLmdldCgnbmV3cy9sYXRlc3QnKS50aGVuKChyZXMpID0+IHtcbiAgICAgICAgICAgIHRoaXMubW9zdF9sYXRlc3QgPSByZXMuZGF0YTtcblxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhyZXMuZGF0YSk7XG4gICAgICAgIH0pO1xuICAgIH0sXG4gICAgdHJ1bmNhdGU6IGZ1bmN0aW9uICh0ZXh0LCBsZW5ndGgsIHN1ZmZpeCkge1xuICAgICAgaWYodGV4dC5yZXBsYWNlKC8oPChbXj5dKyk+KS9pZyxcIlwiKS5sZW5ndGg8bGVuZ3RoKXtcbiAgICAgICAgcmV0dXJuIHRleHQucmVwbGFjZSgvKDwoW14+XSspPikvaWcsXCJcIikuc3Vic3RyaW5nKDAsIGxlbmd0aCk7XG4gICAgICB9ZWxzZXtcbiAgICAgICAgcmV0dXJuIHRleHQucmVwbGFjZSgvKDwoW14+XSspPikvaWcsXCJcIikuc3Vic3RyaW5nKDAsIGxlbmd0aCkgKyBzdWZmaXg7XG4gICAgICB9XG5cbiAgICB9LFxuXG4gfSxcblxuIGNvbXB1dGVkOntcbiAgIGNvdW50UmVzdWx0czogZnVuY3Rpb24oKXtcbiAgICAgICAgICByZXR1cm4gdGhpcy5uZXdzX2l0ZW0ubGVuZ3RoO1xuICAgIH0sXG4gICAgaXNFbXB0eTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiBqUXVlcnkuaXNFbXB0eU9iamVjdCh0aGlzLnVzZXIpXG4gICAgfSxcblxuIH0sXG5cbiBjcmVhdGVkKCkge1xuICAgICAgdGhpcy5mZXRjaE1lc3NhZ2VzKCk7XG4gIH0sXG5cblxufVxuXG48L3NjcmlwdD5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBGZWF0dXJlZF9uZXdzLnZ1ZT8xMmU5ZDc1MiIsIjx0ZW1wbGF0ZT5cblxuICA8ZGl2PlxuICAgIDxkaXYgdi1mb3I9XCJuZXdzX2l0ZW1zIGluIG5ld3NfaXRlbVwiID5cbiAgICAgIDxkaXYgY2xhc3M9XCJtZWRpYVwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMTIgY29sLXNtLTYgY29sLW1kLTRcIj5cbiAgICAgICAgICA8IS0tPGRpdiBjbGFzcz1cImQtZmxleFwiPi0tPlxuICAgICAgICAgICAgPGltZyBjbGFzcz1cImltZy1mbHVpZCBpbWctcmVzcG9uc2l2ZVwiIDpzcmM9XCInLi4vLi4vaW1hZ2VzL25ld3MvJytuZXdzX2l0ZW1zLmltYWdlXCIgPlxuICAgICAgICAgIDwhLS08L2Rpdj4tLT5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMTIgY29sLXNtLTYgY29sLW1kLThcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwibWVkaWEtYm9keVwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNob2ljZV90ZXh0XCIgc3R5bGU9XCJtYXJnaW4tdG9wOjAgIWltcG9ydGFudFwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZGF0ZVwiPlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCI+PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT57e25ld3NfaXRlbXMuY3JlYXRlZF9hdHxmb3JtYXREYXRlfX08L2E+XG4gICAgICAgICAgICAgICAgPCEtLTxhIGhyZWY9XCIjXCI+PGkgY2xhc3M9XCJmYSBmYS1jb21tZW50cy1vXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPnt7Y291bnRSZXN1bHRzfX08L2E+LS0+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8YSA6aHJlZj1cIicvbmV3cy8nICsgbmV3c19pdGVtcy5uZXdzX2lkXCIgPjxoNCBzdHlsZT1cImZvbnQtc2l6ZTogMTVweCAhaW1wb3J0YW50XCI+e3tuZXdzX2l0ZW1zLmhlYWRlcn19PC9oND48L2E+XG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwibW9yZVwiIHYtaHRtbD1cInRydW5jYXRlKG5ld3NfaXRlbXMuY29udGVudCwzMDAsICcuLi4nKVwiIHN0eWxlPVwiZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnRcIj5cbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG5cblxuICAgIDwvZGl2PlxuICA8L2Rpdj5cblxuXG48L3RlbXBsYXRlPlxuXG5AcHVzaCgnc2NyaXB0cycpXG57ISEgSHRtbDo6c2NyaXB0KG1peCgnanMvcGFnZXMvc3VtbWVybm90ZS5qcycsJ3Nob3BwaW5nJykpICEhfVxuQGVuZHB1c2hcblxuPHNjcmlwdD5cbmxldCB1c2VyID0gd2luZG93LkxhcmF2ZWwudXNlcjtcblxuZXhwb3J0IGRlZmF1bHQge1xuICBkYXRhKCkge1xuICAgIHJldHVybiB7XG4gICAgICB1c2VyOiB1c2VyLFxuICAgICAgbmV3c19pdGVtOiBbXVxuICAgIH1cbiAgfSxcbiAgbWV0aG9kczoge1xuICAgIGZldGNoTWVzc2FnZXM6IGZ1bmN0aW9uKCkge1xuICAgICAgICB3aW5kb3cuYXhpb3MuZ2V0KCduZXdzL2ZlYXR1cmVkJykudGhlbigocmVzKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm5ld3NfaXRlbSA9IHJlcy5kYXRhO1xuXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKHJlcy5kYXRhKTtcbiAgICAgICAgfSk7XG4gICAgfSxcbiAgICB0cnVuY2F0ZTogZnVuY3Rpb24gKHRleHQsIGxlbmd0aCwgc3VmZml4KSB7XG4gICAgICBpZih0ZXh0LnJlcGxhY2UoLyg8KFtePl0rKT4pL2lnLFwiXCIpLmxlbmd0aDxsZW5ndGgpe1xuICAgICAgICByZXR1cm4gdGV4dC5yZXBsYWNlKC8oPChbXj5dKyk+KS9pZyxcIlwiKS5zdWJzdHJpbmcoMCwgbGVuZ3RoKTtcbiAgICAgIH1lbHNle1xuICAgICAgICByZXR1cm4gdGV4dC5yZXBsYWNlKC8oPChbXj5dKyk+KS9pZyxcIlwiKS5zdWJzdHJpbmcoMCwgbGVuZ3RoKSArIHN1ZmZpeDtcbiAgICAgIH1cblxuICAgIH0sXG5cbiB9LFxuXG4gY29tcHV0ZWQ6e1xuICAgY291bnRSZXN1bHRzOiBmdW5jdGlvbigpe1xuICAgICAgICAgIHJldHVybiB0aGlzLm5ld3NfaXRlbS5sZW5ndGg7XG4gICAgfSxcbiAgICBpc0VtcHR5OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuIGpRdWVyeS5pc0VtcHR5T2JqZWN0KHRoaXMudXNlcilcbiAgICB9LFxuXG4gfSxcblxuIGNyZWF0ZWQoKSB7XG4gICAgICB0aGlzLmZldGNoTWVzc2FnZXMoKTtcbiAgfSxcblxuXG59XG5cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE5ld3MudnVlPzRmNzMxNjJjIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRmVhdHVyZWRfbmV3cy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTdkNzJmNDNlXFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0ZlYXR1cmVkX25ld3MudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3Zpc2EvY29tcG9uZW50cy9uZXdzL0ZlYXR1cmVkX25ld3MudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRmVhdHVyZWRfbmV3cy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtN2Q3MmY0M2VcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi03ZDcyZjQzZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3Zpc2EvY29tcG9uZW50cy9uZXdzL0ZlYXR1cmVkX25ld3MudnVlXG4vLyBtb2R1bGUgaWQgPSAyODdcbi8vIG1vZHVsZSBjaHVua3MgPSA2IiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vTmV3cy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTA5ZjhhZDc0XFxcIn0hLi4vLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL05ld3MudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL3RvcHd5Yy9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3Zpc2EvY29tcG9uZW50cy9uZXdzL05ld3MudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gTmV3cy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMDlmOGFkNzRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0wOWY4YWQ3NFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3Zpc2EvY29tcG9uZW50cy9uZXdzL05ld3MudnVlXG4vLyBtb2R1bGUgaWQgPSAyODhcbi8vIG1vZHVsZSBjaHVua3MgPSA2IiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCBfdm0uX2woKF92bS5uZXdzX2l0ZW0pLCBmdW5jdGlvbihuZXdzX2l0ZW1zKSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcIm1lZGlhXCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMTIgY29sLXNtLTYgY29sLW1kLTRcIlxuICAgIH0sIFtfYygnaW1nJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLWZsdWlkIGltZy1yZXNwb25zaXZlXCIsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInNyY1wiOiAnLi4vLi4vaW1hZ2VzL25ld3MvJyArIG5ld3NfaXRlbXMuaW1hZ2VcbiAgICAgIH1cbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY29sLXhzLTEyIGNvbC1zbS02IGNvbC1tZC04XCJcbiAgICB9LCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcIm1lZGlhLWJvZHlcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY2hvaWNlX3RleHRcIixcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwibWFyZ2luLXRvcFwiOiBcIjAgIWltcG9ydGFudFwiXG4gICAgICB9XG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJkYXRlXCJcbiAgICB9LCBbX2MoJ2EnLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogXCIjXCJcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2knLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1jYWxlbmRhclwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJhcmlhLWhpZGRlblwiOiBcInRydWVcIlxuICAgICAgfVxuICAgIH0pLCBfdm0uX3YoX3ZtLl9zKF92bS5fZihcImZvcm1hdERhdGVcIikobmV3c19pdGVtcy5jcmVhdGVkX2F0KSkpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2EnLCB7XG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogJy9uZXdzLycgKyBuZXdzX2l0ZW1zLm5ld3NfaWRcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2g0Jywge1xuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJmb250LXNpemVcIjogXCIxNXB4ICFpbXBvcnRhbnRcIlxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoX3ZtLl9zKG5ld3NfaXRlbXMuaGVhZGVyKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcIm1vcmVcIixcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwiZm9udC1zaXplXCI6IFwiMTJweCAhaW1wb3J0YW50XCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcImlubmVySFRNTFwiOiBfdm0uX3MoX3ZtLnRydW5jYXRlKG5ld3NfaXRlbXMuY29udGVudCwgMzAwLCAnLi4uJykpXG4gICAgICB9XG4gICAgfSldKV0pXSldKV0pXSlcbiAgfSksIDApXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTA5ZjhhZDc0XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMDlmOGFkNzRcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvdmlzYS9jb21wb25lbnRzL25ld3MvTmV3cy52dWVcbi8vIG1vZHVsZSBpZCA9IDI5NlxuLy8gbW9kdWxlIGNodW5rcyA9IDYiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfdm0uX2woKF92bS5tb3N0X2xhdGVzdCksIGZ1bmN0aW9uKG1vc3RfbGF0ZXN0KSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNob2ljZV9pdGVtXCJcbiAgICB9LCBbX2MoJ2ltZycsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1mbHVpZFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJzcmNcIjogJy4uLy4uL2ltYWdlcy9uZXdzLycgKyBtb3N0X2xhdGVzdC5pbWFnZSxcbiAgICAgICAgXCJhbHRcIjogXCJcIlxuICAgICAgfVxuICAgIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY2hvaWNlX3RleHRcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZGF0ZVwiXG4gICAgfSwgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgICB9XG4gICAgfSwgW19jKCdpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiYXJpYS1oaWRkZW5cIjogXCJ0cnVlXCJcbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJmb3JtYXREYXRlXCIpKG1vc3RfbGF0ZXN0LmNyZWF0ZWRfYXQpKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiAnL25ld3MvJyArIG1vc3RfbGF0ZXN0Lm5ld3NfaWRcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2g0JywgW192bS5fdihfdm0uX3MobW9zdF9sYXRlc3QuaGVhZGVyKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcIm1vcmVcIixcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwiZm9udC1zaXplXCI6IFwiMTJweCAhaW1wb3J0YW50XCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcImlubmVySFRNTFwiOiBfdm0uX3MoX3ZtLnRydW5jYXRlKG1vc3RfbGF0ZXN0LmNvbnRlbnQsIDIwMCwgJy4uLicpKVxuICAgICAgfVxuICAgIH0pXSldKV0pXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInByb2R1Y3RfbGlzdCBwLWxlZnRcIlxuICB9LCBfdm0uX2woKF92bS5uZXdzX2l0ZW0pLCBmdW5jdGlvbihuZXdzX2l0ZW0pIHtcbiAgICByZXR1cm4gX2MoJ2RpdicsIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibWVkaWFcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZC1mbGV4XCJcbiAgICB9LCBbX2MoJ2ltZycsIHtcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwid2lkdGhcIjogXCIxNTBweCAhaW1wb3J0YW50XCIsXG4gICAgICAgIFwicGFkZGluZ1wiOiBcIjVweFwiXG4gICAgICB9LFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJzcmNcIjogJy4uLy4uL2ltYWdlcy9uZXdzLycgKyBuZXdzX2l0ZW0uaW1hZ2VcbiAgICAgIH1cbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibWVkaWEtYm9keVwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjaG9pY2VfdGV4dFwiXG4gICAgfSwgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvbmV3cy8nICsgbmV3c19pdGVtLm5ld3NfaWRcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2g0Jywge1xuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJmb250LXNpemVcIjogXCIxMnB4XCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KF92bS5fcyhuZXdzX2l0ZW0uaGVhZGVyKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZGF0ZVwiXG4gICAgfSwgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgICB9XG4gICAgfSwgW19jKCdpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiYXJpYS1oaWRkZW5cIjogXCJ0cnVlXCJcbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJmb3JtYXREYXRlXCIpKG5ld3NfaXRlbS5jcmVhdGVkX2F0KSkpXSldKV0pXSldKV0pXG4gIH0pLCAwKV0sIDIpXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LTdkNzJmNDNlXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtN2Q3MmY0M2VcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvdmlzYS9jb21wb25lbnRzL25ld3MvRmVhdHVyZWRfbmV3cy52dWVcbi8vIG1vZHVsZSBpZCA9IDMxMFxuLy8gbW9kdWxlIGNodW5rcyA9IDYiXSwic291cmNlUm9vdCI6IiJ9