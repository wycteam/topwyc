/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 346);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 160:
/***/ (function(module, exports, __webpack_require__) {



Vue.component('news-articles-featured', __webpack_require__(289));

new Vue({
  el: '#newsLoader2'
});

Vue.filter('formatDate', function (time) {
  var date = new Date((time || "").replace(/-/g, "/").replace(/[TZ]/g, " ")),
      diff = (new Date().getTime() - date.getTime()) / 1000,
      day_diff = Math.floor(diff / 86400);

  if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31) return;

  return day_diff == 0 && (diff < 60 && "just now" || diff < 120 && "1 minute ago" || diff < 3600 && Math.floor(diff / 60) + " minutes ago" || diff < 7200 && "1 hour ago" || diff < 86400 && Math.floor(diff / 3600) + " hours ago") || day_diff == 1 && "Yesterday" || day_diff < 7 && day_diff + " days ago" || day_diff < 31 && Math.ceil(day_diff / 7) + " weeks ago";
});

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var user = window.Laravel.user;

/* harmony default export */ __webpack_exports__["default"] = ({
  components: { Comment: Comment },
  data: function data() {
    return {
      user: user,
      news_item: [],
      most_latest: []
    };
  },

  methods: {
    fetchMessages: function fetchMessages() {
      var _this = this;

      window.axios.get('/news').then(function (res) {
        _this.news_item = res.data;
        //console.log(res.data);
      });
      window.axios.get('/news/latest').then(function (res) {
        _this.most_latest = res.data;

        //console.log(res.data);
      });
    },
    truncate: function truncate(text, length, suffix) {
      if (text.replace(/(<([^>]+)>)/ig, "").length < length) {
        return text.replace(/(<([^>]+)>)/ig, "").substring(0, length);
      } else {
        return text.replace(/(<([^>]+)>)/ig, "").substring(0, length) + suffix;
      }
    }

  },

  computed: {
    countResults: function countResults() {
      return this.news_item.length;
    },
    isEmpty: function isEmpty() {
      return jQuery.isEmptyObject(this.user);
    }

  },

  created: function created() {
    this.fetchMessages();
  }
});

/***/ }),

/***/ 289:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(239),
  /* template */
  __webpack_require__(318),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Applications/XAMPP/xamppfiles/htdocs/topwyc/resources/assets/js/shopping/visa/components/news/innerFeatured.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] innerFeatured.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-be77ab3e", Component.options)
  } else {
    hotAPI.reload("data-v-be77ab3e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 318:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_vm._l((_vm.most_latest), function(most_latest) {
    return _c('div', [_c('div', {
      staticClass: "choice_item"
    }, [_c('img', {
      staticClass: "img-fluid",
      attrs: {
        "src": '../../../images/news/' + most_latest.image,
        "alt": ""
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "choice_text"
    }, [_c('div', {
      staticClass: "date"
    }, [_c('a', {
      attrs: {
        "href": "#"
      }
    }, [_c('i', {
      staticClass: "fa fa-calendar",
      attrs: {
        "aria-hidden": "true"
      }
    }), _vm._v(_vm._s(_vm._f("formatDate")(most_latest.created_at)))])]), _vm._v(" "), _c('a', {
      attrs: {
        "href": '/news/' + most_latest.news_id
      }
    }, [_c('h4', [_vm._v(_vm._s(most_latest.header))])]), _vm._v(" "), _c('span', {
      staticClass: "more",
      staticStyle: {
        "font-size": "12px !important"
      },
      domProps: {
        "innerHTML": _vm._s(_vm.truncate(most_latest.content, 200, '...'))
      }
    })])])])
  }), _vm._v(" "), _c('div', {
    staticClass: "product_list p-left"
  }, _vm._l((_vm.news_item), function(news_item) {
    return _c('div', [_c('div', {
      staticClass: "media"
    }, [_c('div', {
      staticClass: "d-flex"
    }, [_c('img', {
      staticStyle: {
        "width": "150px !important",
        "padding": "5px"
      },
      attrs: {
        "src": '../../../images/news/' + news_item.image
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "media-body"
    }, [_c('div', {
      staticClass: "choice_text"
    }, [_c('a', {
      attrs: {
        "href": '/news/' + news_item.news_id
      }
    }, [_c('h4', {
      staticStyle: {
        "font-size": "12px"
      }
    }, [_vm._v(_vm._s(news_item.header))])]), _vm._v(" "), _c('div', {
      staticClass: "date"
    }, [_c('a', {
      attrs: {
        "href": "#"
      }
    }, [_c('i', {
      staticClass: "fa fa-calendar",
      attrs: {
        "aria-hidden": "true"
      }
    }), _vm._v(_vm._s(_vm._f("formatDate")(news_item.created_at)))])])])])])])
  }), 0)], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-be77ab3e", module.exports)
  }
}

/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(160);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZDFhMWZiOWUyZTI5MTVjMWQ1ZDY/MGUzMCoqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvdmlzYS9uZXdzMi5qcyIsIndlYnBhY2s6Ly8vaW5uZXJGZWF0dXJlZC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy92aXNhL2NvbXBvbmVudHMvbmV3cy9pbm5lckZlYXR1cmVkLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL3Zpc2EvY29tcG9uZW50cy9uZXdzL2lubmVyRmVhdHVyZWQudnVlPzYzM2EiXSwibmFtZXMiOlsiVnVlIiwiY29tcG9uZW50IiwicmVxdWlyZSIsImVsIiwiZmlsdGVyIiwidGltZSIsImRhdGUiLCJEYXRlIiwicmVwbGFjZSIsImRpZmYiLCJnZXRUaW1lIiwiZGF5X2RpZmYiLCJNYXRoIiwiZmxvb3IiLCJpc05hTiIsImNlaWwiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUNoRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7O0FDL0NBQSxJQUFJQyxTQUFKLENBQWMsd0JBQWQsRUFBd0NDLG1CQUFPQSxDQUFDLEdBQVIsQ0FBeEM7O0FBR0EsSUFBSUYsR0FBSixDQUFRO0FBQ05HLE1BQUk7QUFERSxDQUFSOztBQUlBSCxJQUFJSSxNQUFKLENBQVcsWUFBWCxFQUF5QixVQUFTQyxJQUFULEVBQWM7QUFDckMsTUFBSUMsT0FBTyxJQUFJQyxJQUFKLENBQVMsQ0FBQ0YsUUFBUSxFQUFULEVBQWFHLE9BQWIsQ0FBcUIsSUFBckIsRUFBMEIsR0FBMUIsRUFBK0JBLE9BQS9CLENBQXVDLE9BQXZDLEVBQStDLEdBQS9DLENBQVQsQ0FBWDtBQUFBLE1BQ0VDLE9BQVEsQ0FBRSxJQUFJRixJQUFKLEVBQUQsQ0FBYUcsT0FBYixLQUF5QkosS0FBS0ksT0FBTCxFQUExQixJQUE0QyxJQUR0RDtBQUFBLE1BRUVDLFdBQVdDLEtBQUtDLEtBQUwsQ0FBV0osT0FBTyxLQUFsQixDQUZiOztBQUlGLE1BQUtLLE1BQU1ILFFBQU4sS0FBbUJBLFdBQVcsQ0FBOUIsSUFBbUNBLFlBQVksRUFBcEQsRUFFSTs7QUFFSixTQUFPQSxZQUFZLENBQVosS0FDQ0YsT0FBTyxFQUFQLElBQWEsVUFBYixJQUNBQSxPQUFPLEdBQVAsSUFBYyxjQURkLElBRUFBLE9BQU8sSUFBUCxJQUFlRyxLQUFLQyxLQUFMLENBQVlKLE9BQU8sRUFBbkIsSUFBMEIsY0FGekMsSUFHQUEsT0FBTyxJQUFQLElBQWUsWUFIZixJQUlBQSxPQUFPLEtBQVAsSUFBZ0JHLEtBQUtDLEtBQUwsQ0FBWUosT0FBTyxJQUFuQixJQUE0QixZQUw3QyxLQU1IRSxZQUFZLENBQVosSUFBaUIsV0FOZCxJQU9IQSxXQUFXLENBQVgsSUFBZ0JBLFdBQVcsV0FQeEIsSUFRSEEsV0FBVyxFQUFYLElBQWlCQyxLQUFLRyxJQUFMLENBQVdKLFdBQVcsQ0FBdEIsSUFBNEIsWUFSakQ7QUFTQyxDQWxCRCxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNtQ0E7O0FBRUE7QUFDQSxrQ0FEQTtBQUVBLE1BRkEsa0JBRUE7QUFDQTtBQUNBLGdCQURBO0FBRUEsbUJBRkE7QUFHQTtBQUhBO0FBS0EsR0FSQTs7QUFTQTtBQUNBO0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FIQTtBQUlBO0FBQ0E7O0FBRUE7QUFDQSxPQUpBO0FBS0EsS0FYQTtBQVlBO0FBQ0E7QUFDQTtBQUNBLE9BRkEsTUFFQTtBQUNBO0FBQ0E7QUFFQTs7QUFuQkEsR0FUQTs7QUFnQ0E7QUFDQTtBQUNBO0FBQ0EsS0FIQTtBQUlBO0FBQ0E7QUFDQTs7QUFOQSxHQWhDQTs7QUEwQ0EsU0ExQ0EscUJBMENBO0FBQ0E7QUFDQTtBQTVDQSxHOzs7Ozs7O0FDL0NBLGdCQUFnQixtQkFBTyxDQUFDLENBQXdFO0FBQ2hHO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQThPO0FBQ3hQO0FBQ0EsRUFBRSxtQkFBTyxDQUFDLEdBQTZNO0FBQ3ZOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsSUFBSSxLQUFVLEdBQUc7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7OztBQzNCQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQSxJQUFJLEtBQVU7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEMiLCJmaWxlIjoianMvdmlzYS9uZXdzMi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMzQ2KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBkMWExZmI5ZTJlMjkxNWMxZDVkNiIsIi8vIHRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIHNjb3BlSWQsXG4gIGNzc01vZHVsZXNcbikge1xuICB2YXIgZXNNb2R1bGVcbiAgdmFyIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyB8fCB7fVxuXG4gIC8vIEVTNiBtb2R1bGVzIGludGVyb3BcbiAgdmFyIHR5cGUgPSB0eXBlb2YgcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIGlmICh0eXBlID09PSAnb2JqZWN0JyB8fCB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgZXNNb2R1bGUgPSByYXdTY3JpcHRFeHBvcnRzXG4gICAgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICB9XG5cbiAgLy8gVnVlLmV4dGVuZCBjb25zdHJ1Y3RvciBleHBvcnQgaW50ZXJvcFxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHRFeHBvcnRzID09PSAnZnVuY3Rpb24nXG4gICAgPyBzY3JpcHRFeHBvcnRzLm9wdGlvbnNcbiAgICA6IHNjcmlwdEV4cG9ydHNcblxuICAvLyByZW5kZXIgZnVuY3Rpb25zXG4gIGlmIChjb21waWxlZFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSBjb21waWxlZFRlbXBsYXRlLnJlbmRlclxuICAgIG9wdGlvbnMuc3RhdGljUmVuZGVyRm5zID0gY29tcGlsZWRUZW1wbGF0ZS5zdGF0aWNSZW5kZXJGbnNcbiAgfVxuXG4gIC8vIHNjb3BlZElkXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWRcbiAgfVxuXG4gIC8vIGluamVjdCBjc3NNb2R1bGVzXG4gIGlmIChjc3NNb2R1bGVzKSB7XG4gICAgdmFyIGNvbXB1dGVkID0gT2JqZWN0LmNyZWF0ZShvcHRpb25zLmNvbXB1dGVkIHx8IG51bGwpXG4gICAgT2JqZWN0LmtleXMoY3NzTW9kdWxlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgbW9kdWxlID0gY3NzTW9kdWxlc1trZXldXG4gICAgICBjb21wdXRlZFtrZXldID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gbW9kdWxlIH1cbiAgICB9KVxuICAgIG9wdGlvbnMuY29tcHV0ZWQgPSBjb21wdXRlZFxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBlc01vZHVsZTogZXNNb2R1bGUsXG4gICAgZXhwb3J0czogc2NyaXB0RXhwb3J0cyxcbiAgICBvcHRpb25zOiBvcHRpb25zXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIiwiXG5cblxuVnVlLmNvbXBvbmVudCgnbmV3cy1hcnRpY2xlcy1mZWF0dXJlZCcsIHJlcXVpcmUoJy4uL3Zpc2EvY29tcG9uZW50cy9uZXdzL2lubmVyRmVhdHVyZWQudnVlJykpO1xuXG5cbm5ldyBWdWUoe1xuICBlbDogJyNuZXdzTG9hZGVyMicsXG59KTtcblxuVnVlLmZpbHRlcignZm9ybWF0RGF0ZScsIGZ1bmN0aW9uKHRpbWUpe1xuICB2YXIgZGF0ZSA9IG5ldyBEYXRlKCh0aW1lIHx8IFwiXCIpLnJlcGxhY2UoLy0vZyxcIi9cIikucmVwbGFjZSgvW1RaXS9nLFwiIFwiKSksXG4gICAgZGlmZiA9ICgoKG5ldyBEYXRlKCkpLmdldFRpbWUoKSAtIGRhdGUuZ2V0VGltZSgpKSAvIDEwMDApLFxuICAgIGRheV9kaWZmID0gTWF0aC5mbG9vcihkaWZmIC8gODY0MDApO1xuXG5pZiAoIGlzTmFOKGRheV9kaWZmKSB8fCBkYXlfZGlmZiA8IDAgfHwgZGF5X2RpZmYgPj0gMzEgKVxuXG4gICAgcmV0dXJuO1xuXG5yZXR1cm4gZGF5X2RpZmYgPT0gMCAmJiAoXG4gICAgICAgIGRpZmYgPCA2MCAmJiBcImp1c3Qgbm93XCIgfHxcbiAgICAgICAgZGlmZiA8IDEyMCAmJiBcIjEgbWludXRlIGFnb1wiIHx8XG4gICAgICAgIGRpZmYgPCAzNjAwICYmIE1hdGguZmxvb3IoIGRpZmYgLyA2MCApICsgXCIgbWludXRlcyBhZ29cIiB8fFxuICAgICAgICBkaWZmIDwgNzIwMCAmJiBcIjEgaG91ciBhZ29cIiB8fFxuICAgICAgICBkaWZmIDwgODY0MDAgJiYgTWF0aC5mbG9vciggZGlmZiAvIDM2MDAgKSArIFwiIGhvdXJzIGFnb1wiKSB8fFxuICAgIGRheV9kaWZmID09IDEgJiYgXCJZZXN0ZXJkYXlcIiB8fFxuICAgIGRheV9kaWZmIDwgNyAmJiBkYXlfZGlmZiArIFwiIGRheXMgYWdvXCIgfHxcbiAgICBkYXlfZGlmZiA8IDMxICYmIE1hdGguY2VpbCggZGF5X2RpZmYgLyA3ICkgKyBcIiB3ZWVrcyBhZ29cIjtcbn0pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy92aXNhL25ld3MyLmpzIiwiPHRlbXBsYXRlPlxuPGRpdj5cblxuICA8ZGl2IHYtZm9yPVwibW9zdF9sYXRlc3QgaW4gbW9zdF9sYXRlc3RcIiA+XG4gIDxkaXYgY2xhc3M9XCJjaG9pY2VfaXRlbVwiPlxuICAgIDxpbWcgY2xhc3M9XCJpbWctZmx1aWRcIiA6c3JjPVwiJy4uLy4uLy4uL2ltYWdlcy9uZXdzLycrbW9zdF9sYXRlc3QuaW1hZ2VcIiBhbHQ9XCJcIj5cbiAgICA8ZGl2IGNsYXNzPVwiY2hvaWNlX3RleHRcIj5cbiAgICAgIDxkaXYgY2xhc3M9XCJkYXRlXCI+XG4gICAgICAgIDxhIGhyZWY9XCIjXCI+PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT57e21vc3RfbGF0ZXN0LmNyZWF0ZWRfYXR8Zm9ybWF0RGF0ZX19PC9hPlxuICAgICAgICA8IS0tPGEgaHJlZj1cIiNcIj48aSBjbGFzcz1cImZhIGZhLWNvbW1lbnRzLW9cIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+MDU8L2E+LS0+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxhIDpocmVmPVwiJy9uZXdzLycgKyBtb3N0X2xhdGVzdC5uZXdzX2lkXCI+PGg0Pnt7bW9zdF9sYXRlc3QuaGVhZGVyfX08L2g0PjwvYT5cbiAgICAgIDxzcGFuIGNsYXNzPVwibW9yZVwiIHYtaHRtbD1cInRydW5jYXRlKG1vc3RfbGF0ZXN0LmNvbnRlbnQsMjAwLCAnLi4uJylcIiBzdHlsZT1cImZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50XCI+PC9zcGFuPlxuICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICA8L2Rpdj5cbiAgPGRpdiBjbGFzcz1cInByb2R1Y3RfbGlzdCBwLWxlZnRcIj5cbiAgICA8ZGl2IHYtZm9yPVwibmV3c19pdGVtIGluIG5ld3NfaXRlbVwiID5cbiAgICAgIDxkaXYgY2xhc3M9XCJtZWRpYVwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwiZC1mbGV4XCI+XG4gICAgICAgICAgPGltZyA6c3JjPVwiJy4uLy4uLy4uL2ltYWdlcy9uZXdzLycrbmV3c19pdGVtLmltYWdlXCIgIHN0eWxlPVwid2lkdGg6MTUwcHggIWltcG9ydGFudDsgcGFkZGluZzo1cHg7XCI+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzPVwibWVkaWEtYm9keVwiPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaG9pY2VfdGV4dFwiPlxuICAgICAgICAgICAgPGEgOmhyZWY9XCInL25ld3MvJyArIG5ld3NfaXRlbS5uZXdzX2lkXCI+PGg0IHN0eWxlPVwiZm9udC1zaXplOjEycHg7XCI+e3tuZXdzX2l0ZW0uaGVhZGVyfX08L2g0PjwvYT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkYXRlXCI+XG4gICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCI+PGkgY2xhc3M9XCJmYSBmYS1jYWxlbmRhclwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT57e25ld3NfaXRlbS5jcmVhdGVkX2F0fGZvcm1hdERhdGV9fTwvYT5cbiAgICAgICAgICAgICAgPCEtLTxhIGhyZWY9XCIjXCI+PGkgY2xhc3M9XCJmYSBmYS1jb21tZW50cy1vXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPjA1PC9hPi0tPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG5cbiAgICA8L2Rpdj5cbiAgPC9kaXY+XG4gIDwhLS1cbiAgPGRpdiBjbGFzcz1cInJvd1wiPlxuICAgICAgPENvbW1lbnQgOmNvbW1lbnRVcmw9XCIxOVwiIDp1c2VyPVwidXNlclwiPiAgPC9Db21tZW50PlxuICA8L2Rpdj5cbi0tPlxuPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG5sZXQgdXNlciA9IHdpbmRvdy5MYXJhdmVsLnVzZXI7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgY29tcG9uZW50czogeyBDb21tZW50OiBDb21tZW50fSxcbiAgZGF0YSgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgdXNlcjogdXNlcixcbiAgICAgIG5ld3NfaXRlbTogW10sXG4gICAgICBtb3N0X2xhdGVzdDogW10sXG4gICAgfVxuICB9LFxuICBtZXRob2RzOiB7XG4gICAgZmV0Y2hNZXNzYWdlczogZnVuY3Rpb24oKSB7XG4gICAgICAgIHdpbmRvdy5heGlvcy5nZXQoJy9uZXdzJykudGhlbigocmVzKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm5ld3NfaXRlbSA9IHJlcy5kYXRhO1xuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhyZXMuZGF0YSk7XG4gICAgICAgIH0pO1xuICAgICAgICB3aW5kb3cuYXhpb3MuZ2V0KCcvbmV3cy9sYXRlc3QnKS50aGVuKChyZXMpID0+IHtcbiAgICAgICAgICAgIHRoaXMubW9zdF9sYXRlc3QgPSByZXMuZGF0YTtcblxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhyZXMuZGF0YSk7XG4gICAgICAgIH0pO1xuICAgIH0sXG4gICAgdHJ1bmNhdGU6IGZ1bmN0aW9uICh0ZXh0LCBsZW5ndGgsIHN1ZmZpeCkge1xuICAgICAgaWYodGV4dC5yZXBsYWNlKC8oPChbXj5dKyk+KS9pZyxcIlwiKS5sZW5ndGg8bGVuZ3RoKXtcbiAgICAgICAgcmV0dXJuIHRleHQucmVwbGFjZSgvKDwoW14+XSspPikvaWcsXCJcIikuc3Vic3RyaW5nKDAsIGxlbmd0aCk7XG4gICAgICB9ZWxzZXtcbiAgICAgICAgcmV0dXJuIHRleHQucmVwbGFjZSgvKDwoW14+XSspPikvaWcsXCJcIikuc3Vic3RyaW5nKDAsIGxlbmd0aCkgKyBzdWZmaXg7XG4gICAgICB9XG5cbiAgICB9LFxuXG4gfSxcblxuIGNvbXB1dGVkOntcbiAgIGNvdW50UmVzdWx0czogZnVuY3Rpb24oKXtcbiAgICAgICAgICByZXR1cm4gdGhpcy5uZXdzX2l0ZW0ubGVuZ3RoO1xuICAgIH0sXG4gICAgaXNFbXB0eTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiBqUXVlcnkuaXNFbXB0eU9iamVjdCh0aGlzLnVzZXIpXG4gICAgfSxcblxuIH0sXG5cbiBjcmVhdGVkKCkge1xuICAgICAgdGhpcy5mZXRjaE1lc3NhZ2VzKCk7XG4gIH0sXG5cblxufVxuXG48L3NjcmlwdD5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBpbm5lckZlYXR1cmVkLnZ1ZT80ZjhiY2MyZSIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL2lubmVyRmVhdHVyZWQudnVlXCIpLFxuICAvKiB0ZW1wbGF0ZSAqL1xuICByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi1iZTc3YWIzZVxcXCJ9IS4uLy4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9pbm5lckZlYXR1cmVkLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy90b3B3eWMvcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy92aXNhL2NvbXBvbmVudHMvbmV3cy9pbm5lckZlYXR1cmVkLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5ICE9PSBcIl9fZXNNb2R1bGVcIn0pKSB7Y29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuaWYgKENvbXBvbmVudC5vcHRpb25zLmZ1bmN0aW9uYWwpIHtjb25zb2xlLmVycm9yKFwiW3Z1ZS1sb2FkZXJdIGlubmVyRmVhdHVyZWQudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWJlNzdhYjNlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtYmU3N2FiM2VcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy92aXNhL2NvbXBvbmVudHMvbmV3cy9pbm5lckZlYXR1cmVkLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMjg5XG4vLyBtb2R1bGUgY2h1bmtzID0gMTEiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIFtfdm0uX2woKF92bS5tb3N0X2xhdGVzdCksIGZ1bmN0aW9uKG1vc3RfbGF0ZXN0KSB7XG4gICAgcmV0dXJuIF9jKCdkaXYnLCBbX2MoJ2RpdicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImNob2ljZV9pdGVtXCJcbiAgICB9LCBbX2MoJ2ltZycsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1mbHVpZFwiLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJzcmNcIjogJy4uLy4uLy4uL2ltYWdlcy9uZXdzLycgKyBtb3N0X2xhdGVzdC5pbWFnZSxcbiAgICAgICAgXCJhbHRcIjogXCJcIlxuICAgICAgfVxuICAgIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiY2hvaWNlX3RleHRcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZGF0ZVwiXG4gICAgfSwgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgICB9XG4gICAgfSwgW19jKCdpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiYXJpYS1oaWRkZW5cIjogXCJ0cnVlXCJcbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJmb3JtYXREYXRlXCIpKG1vc3RfbGF0ZXN0LmNyZWF0ZWRfYXQpKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYScsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiAnL25ld3MvJyArIG1vc3RfbGF0ZXN0Lm5ld3NfaWRcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2g0JywgW192bS5fdihfdm0uX3MobW9zdF9sYXRlc3QuaGVhZGVyKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnc3BhbicsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcIm1vcmVcIixcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwiZm9udC1zaXplXCI6IFwiMTJweCAhaW1wb3J0YW50XCJcbiAgICAgIH0sXG4gICAgICBkb21Qcm9wczoge1xuICAgICAgICBcImlubmVySFRNTFwiOiBfdm0uX3MoX3ZtLnRydW5jYXRlKG1vc3RfbGF0ZXN0LmNvbnRlbnQsIDIwMCwgJy4uLicpKVxuICAgICAgfVxuICAgIH0pXSldKV0pXG4gIH0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInByb2R1Y3RfbGlzdCBwLWxlZnRcIlxuICB9LCBfdm0uX2woKF92bS5uZXdzX2l0ZW0pLCBmdW5jdGlvbihuZXdzX2l0ZW0pIHtcbiAgICByZXR1cm4gX2MoJ2RpdicsIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibWVkaWFcIlxuICAgIH0sIFtfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZC1mbGV4XCJcbiAgICB9LCBbX2MoJ2ltZycsIHtcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwid2lkdGhcIjogXCIxNTBweCAhaW1wb3J0YW50XCIsXG4gICAgICAgIFwicGFkZGluZ1wiOiBcIjVweFwiXG4gICAgICB9LFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJzcmNcIjogJy4uLy4uLy4uL2ltYWdlcy9uZXdzLycgKyBuZXdzX2l0ZW0uaW1hZ2VcbiAgICAgIH1cbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwibWVkaWEtYm9keVwiXG4gICAgfSwgW19jKCdkaXYnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJjaG9pY2VfdGV4dFwiXG4gICAgfSwgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvbmV3cy8nICsgbmV3c19pdGVtLm5ld3NfaWRcbiAgICAgIH1cbiAgICB9LCBbX2MoJ2g0Jywge1xuICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgXCJmb250LXNpemVcIjogXCIxMnB4XCJcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KF92bS5fcyhuZXdzX2l0ZW0uaGVhZGVyKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZGF0ZVwiXG4gICAgfSwgW19jKCdhJywge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6IFwiI1wiXG4gICAgICB9XG4gICAgfSwgW19jKCdpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZmEgZmEtY2FsZW5kYXJcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiYXJpYS1oaWRkZW5cIjogXCJ0cnVlXCJcbiAgICAgIH1cbiAgICB9KSwgX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJmb3JtYXREYXRlXCIpKG5ld3NfaXRlbS5jcmVhdGVkX2F0KSkpXSldKV0pXSldKV0pXG4gIH0pLCAwKV0sIDIpXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWJlNzdhYjNlXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtYmU3N2FiM2VcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvdmlzYS9jb21wb25lbnRzL25ld3MvaW5uZXJGZWF0dXJlZC52dWVcbi8vIG1vZHVsZSBpZCA9IDMxOFxuLy8gbW9kdWxlIGNodW5rcyA9IDExIl0sInNvdXJjZVJvb3QiOiIifQ==