/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 420);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 170:
/***/ (function(module, exports, __webpack_require__) {

$(document).ready(function () {
	//material-tooltip
	//make next and previous as symbols
	$.fn.dataTable.ext.errMode = 'none';
	$.extend(true, $.fn.dataTable.defaults, {

		"language": {
			"paginate": {
				"previous": "<",
				"next": ">"
			}
		}
	});

	//language dropdown on header init
	headerColor = $('.top-header').css('background-color');
	//plugin
	var _ddslickLoaded = false;
	$('#language').ddslick({
		width: 'min-content',
		background: headerColor,
		onSelected: function onSelected(data) {
			if (_ddslickLoaded) {
				currentValue = $('#language .dd-selected-value').val();
				$.ajax({
					url: '/locale/' + currentValue,
					type: 'GET'
				}).then(function (result) {
					window.location.reload();
				});
			} else {
				_ddslickLoaded = true;
			}
		}
	}

	//from php
	);selectedLanguage = window.Laravel.language;
	languageIndex = 0;
	switch (selectedLanguage) {
		case 'en':
			languageIndex = 0;break;
		case 'cn':
			languageIndex = 1;break;
		default:
			languageIndex = 0;break;
	}

	$('#language').ddslick('select', { index: languageIndex });
	$('#language .dd-pointer-down').css('display', 'none');
	$('#language .dd-select').css({
		border: 'none',
		borderRadius: '0px'
	});
	$('#language').addClass('pull-left');
	//end of language scripts

	$.material.options.autofill = true;
	$.material.init();

	toastr.options = {
		debug: false,
		positionClass: "toast-bottom-right",
		onclick: null,
		fadeIn: 300,
		fadeOut: 1000,
		timeOut: 5000,
		extendedTimeOut: 1000
	};

	runTimeFunctions();

	//for searching : hiding the results
	$('html').click(function () {
		$('#searchContainer table').hide();
	});

	$('#searchContainer').click(function (event) {
		event.stopPropagation();
		$('#searchContainer table').show();
	});

	//hovering users
	$(document).on('mouseenter', '.popover-details', function (e) {
		content_height = $('#popover-content').height();
		content_width = $(this).width() - 20;
		var top = $(this).offset().top - window.scrollY - content_height;
		var left = $(this).offset().left - window.scrollX + content_width;

		$("#popover-content").css({ left: left + 'px', top: top + 'px' });
		$("#popover-content").stop(true, true).fadeIn(500);
		id = $(this).data('id');
		$('#popover-content').data('id', id);

		$('#btn_loading').show();
		$('#popover-content button').hide();
		$('#btn_container').hide();

		axiosAPIv1.get('/friends').then(function (response) {
			if (id == Laravel.user.id) {
				$('#popover-content').find('#btnViewProfile').show();
				$('#popover-content').find('#btnAddFriend').hide();
				$('#btn_loading').hide();
				$('#btn_container').show();
			} else {
				$('#popover-content').find('#btnViewProfile').hide();

				found = 0;
				for (var i = 0; i < response.data.friends.length; i++) {
					if (response.data.friends[i].user.id == id) {
						found = 'friends';
						break;
					}
				}
				for (var i = 0; i < response.data.requests.length; i++) {
					if (found != 0) {
						break;
					}
					if (response.data.requests[i].user.id == id) {
						found = 'requests';
						break;
					}
				}
				for (var i = 0; i < response.data.restricted.length; i++) {
					if (found != 0) {
						break;
					}
					if (response.data.restricted[i].user.id == id) {
						found = 'restricted';
						break;
					}
				}
				for (var i = 0; i < response.data.blocked.length; i++) {
					if (found != 0) {
						break;
					}
					if (response.data.blocked[i].user.id == id) {
						found = 'blocked';
						break;
					}
				}

				switch (found) {
					case 'friends':
						// $('#popover-content #btnMessageUser').show();
						$('#popover-content #btnBlockUser').show();
						break;

					case 'requests':
						// $('#popover-content #btnMessageUser').show();
						$('#popover-content #btnCancelRequest').show();
						// $('#popover-content #btnBlockUser').show();
						break;

					case 'restricted':
						$('#popover-content #btnBlockUser').show();
						// $('#popover-content #btnRemoveRestriction').show();
						break;

					case 'blocked':
						$('#popover-content #btnUnblockUser').show();
						break;

					default:
						$('#popover-content #btnAddFriend').show();
						// $('#popover-content #btnMessageUser').show();
						$('#popover-content #btnBlockUser').show();
						break;
				}

				$('#btn_loading').hide();
				$('#btn_container').show();
			}
		}).catch($.noop);
	}).on('mouseleave', '.popover-details', function () {
		if ($('#popover-content:hover').length) {
			$('#popover-content').stop(true, true).fadeIn(500);
		} else {
			$('#popover-content').stop(true, true).fadeOut(500);
		}
	});

	$('body').on('mouseleave', '#popover-content', function () {
		$(this).stop(true, true).fadeOut(500);
		loader = $(this).find('.loader').filter(':visible');
		content = loader.next();
		loader.stop(true, true).fadeOut(100, function () {
			content.stop(true, true).fadeIn(100);
		});
	});

	$('#popover-content #btnAddFriend').click(function () {
		axiosAPIv1.post('/friends', {
			user_id2: $('#popover-content').data('id')
		}).then(function (response) {
			if (response.status == 201) {
				toastr.success('', 'Friend request sent!');
				$('#popover-content').hide();
			}
		});
	});

	$('#popover-content #btnMessageUser').click(function () {
		console.log($('#popover-content').data('id'));
		console.log('show message pop up');
	});

	$('#popover-content #btnBlockUser').click(function () {
		axiosAPIv1.post('/friends/block', {
			user_id2: $('#popover-content').data('id')
		}).then(function (response) {
			toastr.success('', 'User Blocked!');
			$('#popover-content').hide();
		});
	});

	$('#popover-content #btnUnblockUser,#popover-content #btnCancelRequest').click(function () {
		axiosAPIv1.post('/friend/find', {
			user_id: Laravel.user.id,
			user_id2: $('#popover-content').data('id')
		}).then(function (response) {
			if (response.status == 200) {

				axiosAPIv1.delete('/friends/' + response.data).then(function (response) {
					if (response.status == 200) {
						toastr.success('', 'Success');
						$('#popover-content').hide();
					}
				}).catch($.noop);
			}
		});
	}

	//end of hovering users


	);
}); //eof

window.isChinese = function () {
	return $('meta[name="locale-lang"]').attr('content') === 'cn';
};

window.runTimeFunctions = function () {

	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover({ html: true });

	loading = '<div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="7" stroke-miterlimit="10"/></svg></div>';
	$('.btn-has-loading').wrapInner("<div class='btn-content'></div>");
	$('.btn-has-loading').prepend(loading);

	$('.btn-has-loading').click(function () {
		content = $(this).find('.btn-content');
		loader = $(this).find('.loader');
		content.stop(true, true).fadeOut(100, function () {
			loader.stop(true, true).fadeIn(100);
		});
	});

	$(document).ajaxSend(function (a, b, c) {
		// if(exempted_links.indexOf(c.url) == -1)
		// {
		// $( ".loader" ).show();
		// }
	});
	$(document).ajaxComplete(function () {
		$(".loader").hide();
		content = $('.btn-has-loading').find('.btn-content');
		loader = $('.btn-has-loading').find('.loader');
		loader.fadeOut(100, function () {
			content.fadeIn(100);
		});
	});
	$(document).ajaxStop(function () {
		// $( ".loader" ).hide();
		content = $('.btn-has-loading').find('.btn-content');
		loader = $('.btn-has-loading').find('.loader');
		loader.fadeOut(100, function () {
			content.fadeIn(100);
		});
	});
	$(document).ajaxSuccess(function () {
		//   $( ".loader" ).hide();
		content = $('.btn-has-loading').find('.btn-content');
		loader = $('.btn-has-loading').find('.loader');
		loader.fadeOut(100, function () {
			content.fadeIn(100);
		});
	});
	$(document).ajaxError(function () {
		// $( ".loader" ).hide();
		content = $('.btn-has-loading').find('.btn-content');
		loader = $('.btn-has-loading').find('.loader');
		loader.fadeOut(100, function () {
			content.fadeIn(100);
		});
	});
}

/*!
 * jQuery Textarea AutoSize plugin
 * Author: Javier Julio
 * Licensed under the MIT license
 */
;(function ($, window, document, undefined) {

	var pluginName = "textareaAutoSize";
	var pluginDataName = "plugin_" + pluginName;

	var containsText = function containsText(value) {
		return value.replace(/\s/g, '').length > 0;
	};

	function Plugin(element, options) {
		this.element = element;
		this.$element = $(element);
		this.init();
	}

	Plugin.prototype = {
		init: function init() {
			var height = this.$element.outerHeight();
			var diff = parseInt(this.$element.css('paddingBottom')) + parseInt(this.$element.css('paddingTop')) || 0;

			if (containsText(this.element.value)) {
				this.$element.height(this.element.scrollHeight - diff);
				console.log(this.element.scrollHeight - diff);
			}

			// keyup is required for IE to properly reset height when deleting text
			this.$element.on('input keyup', function (event) {
				var $window = $(window);
				var currentScrollPosition = $window.scrollTop();

				$(this).height(0).height(this.scrollHeight - diff);

				var id = $(this).attr('data-chat');
				var h = this.scrollHeight - diff;

				if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
					if (h == 40) $('#' + id).css('height', '250');else if (h == 51) $('#' + id).css('height', '229');else if (h == 69) $('#' + id).css('height', '221');else if (h == 86) $('#' + id).css('height', '204');else if (h == 103) $('#' + id).css('height', '196');
				}

				if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
					if (h == 36) $('#' + id).css('height', '250');else if (h == 51) $('#' + id).css('height', '235');else if (h == 68) $('#' + id).css('height', '218');else if (h == 85) $('#' + id).css('height', '201');else if (h == 102) $('#' + id).css('height', '196');
				}

				$window.scrollTop(currentScrollPosition);
			});
		}
	};

	$.fn[pluginName] = function (options) {
		this.each(function () {
			if (!$.data(this, pluginDataName)) {
				$.data(this, pluginDataName, new Plugin(this, options));
			}
		});
		return this;
	};
})(jQuery, window, document);

// Session (flash) message - Toastr
if (window.Laravel.message) {
	toastr.success(window.Laravel.message, null, {
		positionClass: 'toast-top-right'
	});
}

window.numberWithCommas = function (x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

Vue.component('friends-text', __webpack_require__(328));

var appFriends = new Vue({
	el: '#friends-box',

	data: {
		requests: []
	},

	created: function created() {
		var _this = this;

		function getFriend() {
			return axiosAPIv1.get('/friends');
		}

		axios.all([getFriend()]).then(axios.spread(function (friends) {
			for (var i = 0; i < friends.data.requests.length; i++) {
				if (!friends.data.requests[i].requestor) {
					_this.requests.push(friends.data.requests[i]);
				}
			}
		})).catch($.noop);
	}
});

var app = new Vue({
	el: '#searchContainer',
	components: {
		Multiselect: window.VueMultiselect.default
	},
	data: {
		keyword: '',
		products_fetched: [],
		isOnSearching: false
	},
	watch: {
		// whenever question changes, this function will run
		// keyword: function (key) {
		// 	if(key.length > 1)
		// 	{
		// 	  	function getProducts() {
		// 			return axiosAPIv1.post('/search/product',{
		// 				keyword:key
		// 			});
		// 		}
		//     	axios.all([
		// 			getProducts()
		// 		]).then(axios.spread(
		// 			(
		// 				 response
		// 			) => {

		// 			this.products_fetched = response.data;

		// 		}))
		// 		.catch($.noop);
		// 	}
		// }
	},
	methods: {
		nameWithStore: function nameWithStore(_ref) {
			var name = _ref.name,
			    store_name = _ref.store_name;

			return name + " (" + store_name + ")";
		},
		limitText: function limitText(count) {
			return "and " + count + " other results";
		},
		gotopage: function gotopage(slug) {
			window.location = '/product?search=' + slug;
		},

		handleEnter: function handleEnter(e) {
			if (e.keyCode == 13) {
				window.location = '/product?search=' + this.keyword;
			}
		}
	}
});

var app = new Vue({
	el: '#favorites',
	data: {
		item: []
	},
	created: function created() {
		var _this2 = this;

		function getfavoritestotal() {
			if (Laravel.user) return axiosAPIv1.get('/getfavoritestotal');
		}
		axios.all([getfavoritestotal()]).then(axios.spread(function (count) {
			_this2.item = count;
		})).catch($.noop);
	}
});

var app = new Vue({
	el: '#compare',
	data: {
		item: []
	},
	created: function created() {
		var _this3 = this;

		function getcomparetotal() {
			if (Laravel.user) return axiosAPIv1.get('/getcomparetotal');
		}
		axios.all([getcomparetotal()]).then(axios.spread(function (count) {
			//this.item = (count > 0) ? count : '';
			_this3.item = count;
		})).catch($.noop);
	}
});

var app = new Vue({
	el: '#cart',
	data: {
		items: [],
		count: '',
		item_names: ''
	},
	created: function created() {
		var _this4 = this;

		function getCart() {
			return axios.get(location.origin + '/cart/view');
		}
		axios.all([getCart()]).then(axios.spread(function (cart) {
			_this4.count = cart.data.cart_item_count;
			var cart = $.map(cart.data.cart, function (value, index) {
				return [value];
			});
			_this4.items = cart;
		})).catch($.noop);
	},

	methods: {
		resfreshItems: function resfreshItems() {
			var _this5 = this;

			axios.get(location.origin + '/cart/view').then(function (result) {
				var cart = $.map(result.data.cart, function (value, index) {
					return [value];
				});
				_this5.items = cart;
			});
		},
		removeItems: function removeItems(id) {
			axios.get(location.origin + '/cart/delete', {
				params: {
					row_id: this.items[id].rowId
				}
			}).then(function (result) {
				$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
			});
			toastr.success('Removed from Shopping Cart!', this.items[id].name);
			this.items.splice(id, 1);
		}
	},
	computed: {
		total_price: function total_price() {
			sum = 0;
			for (i = 0; i < this.items.length; i++) {
				var salePrice = this.items[i].options.sale_price;
				sum += salePrice.replace(',', '') * this.items[i].qty;
			}
			return sum;
		}
	}
});

var app = new Vue({
	el: '#cart-mobile',
	data: {
		items: [],
		count: ''
	},
	created: function created() {
		var _this6 = this;

		function getCart() {
			return axios.get(location.origin + '/cart/view');
		}
		axios.all([getCart()]).then(axios.spread(function (cart) {
			_this6.count = cart.data.cart_item_count;
			var cart = $.map(cart.data.cart, function (value, index) {
				return [value];
			});
			_this6.items = cart;
		})).catch($.noop);
	}
});

$("#dropdown").mouseleave(function () {
	$("#dropdown-cart").click();
});

// $(window).scroll(function() {
// 	if ($(window).scrollTop() + $(window).height() == $(document).height()) {
// 		Event.fire('scrolled-to-bottom');
// 	}
// });

var docsCheckApp = new Vue({
	el: '#docsCheckApp',

	data: {
		sample: 1,
		sample2: 2
	},

	methods: {
		showExpiringMessage: function showExpiringMessage(doctype, expiryDate) {
			//show warning message when the expiry date is within 30 days
			var expiry_date = moment(expiryDate);

			if (parseInt(expiry_date.diff(moment(), 'days')) <= 30) {
				if (parseInt(expiry_date.diff(moment(), 'days')) > 0) {
					toastr.warning('in ' + expiry_date.diff(moment(), 'days') + ' days.', doctype + ' expiring..', {
						timeOut: 0,
						extendedTimeOut: 0,
						onclick: function onclick() {
							window.location = '/profile?open=documents';
						}
					});
				}
				if (parseInt(expiry_date.diff(moment(), 'days')) < 0) {
					toastr.warning('', doctype + ' was expired, click here to upload new documents', {
						timeOut: 0,
						extendedTimeOut: 0,
						onclick: function onclick() {
							window.location = '/profile?open=documents';
						}
					});
				}
			}
		}
	},

	created: function created() {
		var _this7 = this;

		if (Laravel.user) {
			axiosAPIv1.get('/users/' + Laravel.user.id + '/documents').then(function (docsObj) {
				if (docsObj['status'] == 200) {
					nbi_obj = docsObj.data.filter(function (obj) {
						return obj.type === 'nbi';
					});
					birthCert_obj = docsObj.data.filter(function (obj) {
						return obj.type === 'birthCert';
					});
					govID_obj = docsObj.data.filter(function (obj) {
						return obj.type === 'govId';
					});
					sec_obj = docsObj.data.filter(function (obj) {
						return obj.type === 'sec';
					});
					bir_obj = docsObj.data.filter(function (obj) {
						return obj.type === 'bir';
					});
					permit_obj = docsObj.data.filter(function (obj) {
						return obj.type === 'permit';
					});

					if (nbi_obj.length) {
						if (nbi_obj[0].status == 'Verified' || nbi_obj[0].status == 'Expired') {
							_this7.showExpiringMessage('NBI', nbi_obj[0].expires_at);
						}
					}

					if (birthCert_obj.length) {
						if (birthCert_obj[0].status == 'Verified') {
							_this7.showExpiringMessage('BIRTH CERTIFICATE', birthCert_obj[0].expires_at);
						}
					}

					if (govID_obj.length) {
						if (govID_obj[0].status == 'Verified') {
							_this7.showExpiringMessage('GOVERNMENT ID', govID_obj[0].expires_at);
						}
					}

					if (sec_obj.length) {
						if (sec_obj[0].status == 'Verified') {
							_this7.showExpiringMessage('SEC', sec_obj[0].expires_at);
						}
					}

					if (bir_obj.length) {
						if (bir_obj[0].status == 'Verified') {
							_this7.showExpiringMessage('BIR', bir_obj[0].expires_at);
						}
					}

					if (permit_obj.length) {
						if (permit_obj[0].status == 'Verified') {
							_this7.showExpiringMessage('PERMIT', permit_obj[0].expires_at);
						}
					}
				}
			}).catch(function (error) {
				// toastr.error('Updated Failed.');
			});
		}
	}
});

Vue.component('grand-parent-category', __webpack_require__(327));

var app = new Vue({
	el: '#main_menu',

	created: function created() {
		var _this8 = this;

		axiosAPIv1.get('/getcategories').then(function (response) {
			_this8.categories = response.data;
		});
	},


	data: {
		categories: []
	}
});

var app = new Vue({
	el: '#dropdownUserMenu',
	data: {
		ewallet_balance: 0,
		usable_balance: 0,
		cnt_msg: '',
		cnt_fr: '',
		cnt_not: ''

	},

	methods: {
		getLatestEwalletBalance: function getLatestEwalletBalance() {
			var _this9 = this;

			if (Laravel.user) {
				axiosAPIv1.get('/user/' + Laravel.user.id + '/ewallet').then(function (result) {
					_this9.ewallet_balance = parseFloat(result.data.data.amount);
				});
			}
			if (Laravel.user) {
				axiosAPIv1.get('/getUsable').then(function (result) {
					_this9.usable_balance = result.data.success ? parseFloat(result.data.data.usable) : 0;
				});
			}
		},
		getUsableBalance: function getUsableBalance() {
			var _this10 = this;

			if (Laravel.user) {
				axiosAPIv1.get('/getUsable').then(function (result) {
					_this10.usable_balance = result.data.success ? parseFloat(result.data.data.usable) : 0;
				});
			}
		},
		getMessages: function getMessages() {
			var _this11 = this;

			if (Laravel.user) {
				axiosAPIv1.get('/messages/notifications2').then(function (result) {
					_this11.cnt_msg = result.data;
				});
			}
		},
		getFriends: function getFriends() {
			var _this12 = this;

			if (Laravel.user) {
				axiosAPIv1.get('/friends/notifications').then(function (result) {
					_this12.cnt_fr = result.data;
				});
			}
		},
		getNoti: function getNoti() {
			var _this13 = this;

			if (Laravel.user) {
				axiosAPIv1.get('/notifications').then(function (result) {
					_this13.cnt_not = result.data;
				});
			}
		}
	},
	computed: {
		msgcount: function msgcount() {
			var count = 0;

			if (this.cnt_msg) {
				this.cnt_msg.forEach(function (notif) {
					if (notif.user_id != window.Laravel.user.id) {
						if (!notif.seen_at) {
							++count;
						}
					}
				});
			}

			if (count < 100) {
				return count;
			} else if (count >= 100) {
				return '99+';
			}
		},
		frcount: function frcount() {
			if (this.cnt_fr.notseen_count < 100) {
				return this.cnt_fr.notseen_count;
			} else if (this.cnt_fr.notseen_count >= 100) {
				return '99+';
			}
		},
		noticount: function noticount() {
			var count = 0;

			if (this.cnt_not) {
				this.cnt_not.forEach(function (notif) {
					if (!notif.seen_at) {
						++count;
					}
				});
			}

			if (count < 100) {
				return count;
			} else if (count >= 100) {
				return '99+';
			}
		}
	},

	created: function created() {
		this.getLatestEwalletBalance();
		this.getUsableBalance();
		this.getMessages();
		this.getFriends();
		this.getNoti();
	}
});

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    mounted: function mounted() {
        $('.carousel').carousel({
            interval: 3000 //changes the speed
        });
    },


    props: ['category'],
    data: function data() {
        return {
            image: '',
            perColumn: 0,
            products: [],
            fixedCategories: [],
            hlang: [],
            clang: [],
            firstColumn: [],
            secondColumn: [],
            thirdColumn: [],
            fourthColumn: []
        };
    },
    created: function created() {
        var _this = this;

        function getHeaderTranslation() {
            return axios.get('/translate/header');
        }
        function getCartTranslation() {
            return axios.get('/translate/cart');
        }

        axios.all([getHeaderTranslation(), getCartTranslation()]).then(axios.spread(function (htranslation, ctranslation) {

            _this.hlang = htranslation.data.data;
            _this.clang = ctranslation.data.data;
        })).catch($.noop);

        this.products = this.category.products;

        for (var j = 0; j < this.category.children.length; j++) {
            this.category.children[j].isParent = true;
            this.fixedCategories.push(this.category.children[j]);

            this.products = this.products.concat(this.category.children[j].products);
            for (var k = 0; k < this.category.children[j].children.length; k++) {
                this.category.children[j].children[k].isParent = false;
                this.fixedCategories.push(this.category.children[j].children[k]);
                this.products = this.products.concat(this.category.children[j].children[k].products);
            }
        }

        $('.carousel').carousel({
            interval: 3000 //changes the speed
        });

        for (var i = 0; i < this.fixedCategories.length; i++) {
            if (this.firstColumn.length < 6) {
                this.firstColumn.push(this.fixedCategories[i]);
            } else if (this.secondColumn.length < 6) {
                this.secondColumn.push(this.fixedCategories[i]);
            } else if (this.thirdColumn.length < 6) {
                this.thirdColumn.push(this.fixedCategories[i]);
            } else if (this.fourthColumn.length < 6) {
                this.fourthColumn.push(this.fixedCategories[i]);
            }
        }
    },


    methods: {
        addToFavorites: function addToFavorites(id, name) {
            var _this2 = this;

            if (Laravel.isAuthenticated === true) {
                axiosAPIv1.get('/addtofavorites/' + id).then(function (result) {
                    if (result.data.response == 1) {
                        var fav = parseInt($("#fav-count").text()) + 1;
                        $("#fav-count").text(fav).show();
                        toastr.success(_this2.clang['added-favorites'], name);
                    } else if (result.data.response == 2) {
                        toastr.error(result.data.error_msg, _this2.clang['not-added-wishlist']);
                    } else {
                        toastr.info(_this2.clang['already-favorites'], name);
                    }
                });
            } else {
                toastr.error(this.clang['login-favorites']);
            }
        },
        hasAddedRow: function hasAddedRow(isParent, minimum) {
            if (isParent) {
                return minimum - 1;
            } else {
                return minimum;
            }
        },
        maximumLastColumn: function maximumLastColumn() {
            if (this.fixedCategories[6].isParent) {} else {}
        }
    },

    computed: {
        getTitle: function getTitle() {
            if (this.lang == "cn") {
                if (this.category.name_cn) {
                    return this.category.name_cn.replace("'s Fashion", "");
                }
                return this.category.name.replace("'s Fashion", "");
            }
            return this.category.name.replace("'s Fashion", "");
        },
        getParent: function getParent() {
            if (this.lang == "cn") {
                if (this.fixedCategories[6].name_cn) {
                    return this.fixedCategories[6].name_cn.replace("'s Fashion", "");
                }
                return this.fixedCategories[6].name.replace("'s Fashion", "");
            }
            return this.fixedCategories[6].name.replace("'s Fashion", "");
        },
        getParent2: function getParent2() {
            if (this.lang == "cn") {
                if (this.fixedCategories[this.hasAddedRow(this.fixedCategories[6].isParent, 13)].name_cn) {
                    return this.fixedCategories[this.hasAddedRow(this.fixedCategories[6].isParent, 13)].name_cn;
                }
                return this.fixedCategories[this.hasAddedRow(this.fixedCategories[6].isParent, 13)].name;
            }
            return this.fixedCategories[this.hasAddedRow(this.fixedCategories[6].isParent, 13)].name;
        },
        lang: function lang() {
            var m = $("meta[name=locale-lang]");
            return m.attr("content");
        }
    }
});

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['friend'],

  methods: {
    acceptFriend: function acceptFriend(id) {
      var _this = this;

      axiosAPIv1.post('/friend/find', {
        user_id: Laravel.user.id,
        user_id2: id
      }).then(function (response) {
        if (response.status == 200) {
          axiosAPIv1.put('/friends/' + response.data, {
            status: 'Friend'
          }).then(function (data) {
            if (data.status == 200) {
              for (var j = 0; j < _this.$root.requests.length; j++) {
                if (_this.$root.requests[j].user.id == id) {
                  _this.$root.requests.splice(j, 1);
                }
              }
              toastr.success('', 'Friend Accepted!');
            }
          }).catch($.noop);
        }
      });
    },
    rejectFriend: function rejectFriend(id) {
      var _this2 = this;

      axiosAPIv1.post('/friend/find', {
        user_id: Laravel.user.id,
        user_id2: id
      }).then(function (response) {
        if (response.status == 200) {
          axiosAPIv1.delete('/friends/' + response.data).then(function (data) {
            if (data.status == 200) {
              for (var j = 0; j < _this2.$root.requests.length; j++) {
                if (_this2.$root.requests[j].user.id == id) {
                  _this2.$root.requests.splice(j, 1);
                }
              }
              toastr.success('', 'Request Rejected!');
            }
          }).catch($.noop);
        }
      });
    }
  }

});

/***/ }),

/***/ 327:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(262),
  /* template */
  __webpack_require__(374),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\common\\CategoryHeader.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] CategoryHeader.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2e013b33", Component.options)
  } else {
    hotAPI.reload("data-v-2e013b33", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(263),
  /* template */
  __webpack_require__(406),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\xampp\\htdocs\\wyc06\\resources\\assets\\js\\shopping\\components\\common\\Friends.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Friends.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f6d2d286", Component.options)
  } else {
    hotAPI.reload("data-v-f6d2d286", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 374:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', {
    staticClass: "dropdown mega-menu"
  }, [_c('a', {
    staticClass: "dropdown-toggle",
    attrs: {
      "href": "#",
      "data-toggle": "dropdown",
      "role": "button",
      "aria-haspopup": "true",
      "aria-expanded": "false"
    }
  }, [_vm._v("\n        " + _vm._s(_vm.getTitle) + " "), _c('span', {
    staticClass: "caret"
  })]), _vm._v(" "), _c('ul', {
    staticClass: "dropdown-menu mega-dropdown-menu row"
  }, [_c('li', {
    staticClass: "col-sm-4",
    staticStyle: {
      "text-align": "center",
      "padding-left": "40px",
      "padding-right": "40px"
    }
  }, [_c('ul', [(_vm.products.length) ? _c('div', [_c('li', {
    staticClass: "dropdown-header"
  }, [_vm._v(_vm._s(_vm.hlang['new-products']))]), _vm._v(" "), _c('div', {
    staticClass: "carousel slide",
    attrs: {
      "id": "myCarousel",
      "data-ride": "carousel"
    }
  }, [_c('div', {
    staticClass: "carousel-inner"
  }, _vm._l((_vm.products), function(product, key) {
    return (key < 3) ? _c('div', {
      key: product.id,
      staticClass: "item",
      class: key == 0 ? 'active' : ''
    }, [_c('a', {
      staticClass: "block-center",
      attrs: {
        "href": 'product/' + product.slug
      }
    }, [_c('img', {
      staticClass: "block-center",
      staticStyle: {
        "height": "150px",
        "margin-left": "auto",
        "margin-right": "auto"
      },
      attrs: {
        "src": product.main_image_thumbnail,
        "alt": product.slug
      }
    })]), _vm._v(" "), (_vm.lang == 'cn') ? _c('h4', {
      staticClass: "header-category-product-slider-name"
    }, [(product.cn_name) ? _c('small', [_vm._v(_vm._s(product.cn_name))]) : _c('small', [_vm._v(_vm._s(product.name))])]) : _c('h4', {
      staticClass: "header-category-product-slider-name"
    }, [_vm._v(_vm._s(product.name))]), _vm._v(" "), (product.sale_price != null) ? _c('span', {
      staticClass: "text-primary"
    }, [(product.fee_included == 1) ? _c('span', [_vm._v(_vm._s(_vm._f("currency")(parseFloat(product.sale_price) + parseFloat(product.shipping_fee) + parseFloat(product.charge) + parseFloat(product.vat))))]) : _c('span', [_vm._v(_vm._s(_vm._f("currency")(parseFloat(product.sale_price))))])]) : _c('span', {
      staticClass: "text-primary"
    }, [(product.fee_included == 1) ? _c('span', [_vm._v(_vm._s(_vm._f("currency")(parseFloat(product.price) + parseFloat(product.shipping_fee) + parseFloat(product.charge) + parseFloat(product.vat))))]) : _c('span', [_vm._v(_vm._s(_vm._f("currency")(parseFloat(product.price))))])]), _vm._v(" "), _c('button', {
      staticClass: "btn btn-default",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.addToFavorites(product.id, product.name)
        }
      }
    }, [_c('span', {
      staticClass: "glyphicon glyphicon-heart"
    }), _vm._v(" " + _vm._s(_vm.clang['add-to-wishlist']) + "\n                                ")])]) : _vm._e()
  }))]), _vm._v(" "), _c('li', {
    staticClass: "divider"
  })]) : _vm._e(), _vm._v(" "), _c('li', [_c('a', {
    staticClass: "btn btn-raised btn-primary",
    attrs: {
      "href": "/categories"
    }
  }, [_vm._v(" " + _vm._s(_vm.hlang['view-all-cat']) + " "), _c('span', {
    staticClass: "glyphicon glyphicon-chevron-right pull-right"
  })])])])]), _vm._v(" "), _c('li', {
    staticClass: "col-sm-2"
  }, [_c('ul', {
    staticClass: "list-unstyled"
  }, [_c('li', {
    staticClass: "dropdown-header grandparent"
  }, [_c('a', {
    attrs: {
      "href": '/product?search=' + this.category.slug
    }
  }, [_vm._v(_vm._s(_vm.getTitle))])]), _vm._v(" "), _vm._l((_vm.firstColumn), function(data) {
    return _c('span', [(_vm.lang == 'cn') ? _c('li', {
      class: data.isParent ? 'dropdown-header secparent' : ''
    }, [(data.name_cn) ? _c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name_cn) + "\n                        ")]) : _c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name) + "\n                        ")])]) : _c('li', {
      class: data.isParent ? 'dropdown-header secparent' : ''
    }, [_c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name) + "\n                        ")])]), _vm._v(" "), _c('li', {
      staticClass: "divider"
    })])
  })], 2)]), _vm._v(" "), _c('li', {
    staticClass: "col-sm-2"
  }, [_c('ul', {
    staticClass: "list-unstyled"
  }, [_c('li', {
    staticClass: "dropdown-header grandparent"
  }, [_vm._v(" ")]), _vm._v(" "), _vm._l((_vm.secondColumn), function(data) {
    return _c('span', [(_vm.lang == 'cn') ? _c('li', {
      class: data.isParent ? 'dropdown-header secparent' : ''
    }, [(data.name_cn) ? _c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name_cn) + "\n                        ")]) : _c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name) + "\n                        ")])]) : _c('li', {
      class: data.isParent ? 'dropdown-header secparent' : ''
    }, [_c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name) + "\n                        ")])]), _vm._v(" "), _c('li', {
      staticClass: "divider"
    })])
  })], 2)]), _vm._v(" "), _c('li', {
    staticClass: "col-sm-2"
  }, [_c('ul', {
    staticClass: "list-unstyled"
  }, [_c('li', {
    staticClass: "dropdown-header grandparent"
  }, [_vm._v(" ")]), _vm._v(" "), _vm._l((_vm.thirdColumn), function(data) {
    return _c('span', [(_vm.lang == 'cn') ? _c('li', {
      class: data.isParent ? 'dropdown-header secparent' : ''
    }, [(data.name_cn) ? _c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name_cn) + "\n                        ")]) : _c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name) + "\n                        ")])]) : _c('li', {
      class: data.isParent ? 'dropdown-header secparent' : ''
    }, [_c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name) + "\n                        ")])]), _vm._v(" "), _c('li', {
      staticClass: "divider"
    })])
  })], 2)]), _vm._v(" "), _c('li', {
    staticClass: "col-sm-2"
  }, [_c('ul', {
    staticClass: "list-unstyled"
  }, [_c('li', {
    staticClass: "dropdown-header grandparent"
  }, [_vm._v(" ")]), _vm._v(" "), _vm._l((_vm.fourthColumn), function(data) {
    return _c('span', [(_vm.lang == 'cn') ? _c('li', {
      class: data.isParent ? 'dropdown-header secparent' : ''
    }, [(data.name_cn) ? _c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name_cn) + "\n                        ")]) : _c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name) + "\n                        ")])]) : _c('li', {
      class: data.isParent ? 'dropdown-header secparent' : ''
    }, [_c('a', {
      class: !data.isParent ? 'r-arrow down' : '',
      attrs: {
        "href": '/product?search=' + data.slug
      }
    }, [_vm._v("\n                            " + _vm._s(data.name) + "\n                        ")])]), _vm._v(" "), _c('li', {
      staticClass: "divider"
    })])
  })], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-2e013b33", module.exports)
  }
}

/***/ }),

/***/ 406:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-12 notif-box"
  }, [_c('div', {
    staticClass: "col-sm-2"
  }, [_c('img', {
    staticClass: "notif-img img-circle",
    attrs: {
      "src": _vm.friend.user.avatar,
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-10 msg-context"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-12"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-6 notif-name"
  }, [_c('p', [_vm._v("\r\n                      " + _vm._s(_vm.friend.user.first_name) + " " + _vm._s(_vm.friend.user.last_name) + "\r\n                   ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-6 fr-btn-set"
  }, [_c('div', {
    staticClass: "col-sm-6 fr-button"
  }, [_c('a', {
    staticClass: "btn btn-raised btn-tangerine",
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.acceptFriend(_vm.friend.user.id)
      }
    }
  }, [_vm._v("Accept")])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-6 fr-button"
  }, [_c('a', {
    staticClass: "btn btn-default",
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        _vm.rejectFriend(_vm.friend.user.id)
      }
    }
  }, [_vm._v("Reject")])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-12 msg-content"
  }, [_c('p', [_vm._v("\r\n                  " + _vm._s(_vm.friend.date) + "\r\n                ")])])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-f6d2d286", module.exports)
  }
}

/***/ }),

/***/ 420:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(170);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDY/NTAwOSoqKioqKioqKioqKioiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcz9kNGYzKioqKioqKioqKioqIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvbWFpbi5qcyIsIndlYnBhY2s6Ly8vQ2F0ZWdvcnlIZWFkZXIudnVlIiwid2VicGFjazovLy9GcmllbmRzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvY29tbW9uL0NhdGVnb3J5SGVhZGVyLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvY29tbW9uL0ZyaWVuZHMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvc2hvcHBpbmcvY29tcG9uZW50cy9jb21tb24vQ2F0ZWdvcnlIZWFkZXIudnVlPzM4OTIiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NvbW1vbi9GcmllbmRzLnZ1ZT83ZDY2Il0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5IiwiZm4iLCJkYXRhVGFibGUiLCJleHQiLCJlcnJNb2RlIiwiZXh0ZW5kIiwiZGVmYXVsdHMiLCJoZWFkZXJDb2xvciIsImNzcyIsIl9kZHNsaWNrTG9hZGVkIiwiZGRzbGljayIsIndpZHRoIiwiYmFja2dyb3VuZCIsIm9uU2VsZWN0ZWQiLCJkYXRhIiwiY3VycmVudFZhbHVlIiwidmFsIiwiYWpheCIsInVybCIsInR5cGUiLCJ0aGVuIiwicmVzdWx0Iiwid2luZG93IiwibG9jYXRpb24iLCJyZWxvYWQiLCJzZWxlY3RlZExhbmd1YWdlIiwiTGFyYXZlbCIsImxhbmd1YWdlIiwibGFuZ3VhZ2VJbmRleCIsImluZGV4IiwiYm9yZGVyIiwiYm9yZGVyUmFkaXVzIiwiYWRkQ2xhc3MiLCJtYXRlcmlhbCIsIm9wdGlvbnMiLCJhdXRvZmlsbCIsImluaXQiLCJ0b2FzdHIiLCJkZWJ1ZyIsInBvc2l0aW9uQ2xhc3MiLCJvbmNsaWNrIiwiZmFkZUluIiwiZmFkZU91dCIsInRpbWVPdXQiLCJleHRlbmRlZFRpbWVPdXQiLCJydW5UaW1lRnVuY3Rpb25zIiwiY2xpY2siLCJoaWRlIiwiZXZlbnQiLCJzdG9wUHJvcGFnYXRpb24iLCJzaG93Iiwib24iLCJlIiwiY29udGVudF9oZWlnaHQiLCJoZWlnaHQiLCJjb250ZW50X3dpZHRoIiwidG9wIiwib2Zmc2V0Iiwic2Nyb2xsWSIsImxlZnQiLCJzY3JvbGxYIiwic3RvcCIsImlkIiwiYXhpb3NBUEl2MSIsImdldCIsInVzZXIiLCJmaW5kIiwiZm91bmQiLCJpIiwicmVzcG9uc2UiLCJmcmllbmRzIiwibGVuZ3RoIiwicmVxdWVzdHMiLCJyZXN0cmljdGVkIiwiYmxvY2tlZCIsImNhdGNoIiwibm9vcCIsImxvYWRlciIsImZpbHRlciIsImNvbnRlbnQiLCJuZXh0IiwicG9zdCIsInVzZXJfaWQyIiwic3RhdHVzIiwic3VjY2VzcyIsImNvbnNvbGUiLCJsb2ciLCJ1c2VyX2lkIiwiZGVsZXRlIiwiaXNDaGluZXNlIiwiYXR0ciIsInRvb2x0aXAiLCJwb3BvdmVyIiwiaHRtbCIsImxvYWRpbmciLCJ3cmFwSW5uZXIiLCJwcmVwZW5kIiwiYWpheFNlbmQiLCJhIiwiYiIsImMiLCJhamF4Q29tcGxldGUiLCJhamF4U3RvcCIsImFqYXhTdWNjZXNzIiwiYWpheEVycm9yIiwidW5kZWZpbmVkIiwicGx1Z2luTmFtZSIsInBsdWdpbkRhdGFOYW1lIiwiY29udGFpbnNUZXh0IiwidmFsdWUiLCJyZXBsYWNlIiwiUGx1Z2luIiwiZWxlbWVudCIsIiRlbGVtZW50IiwicHJvdG90eXBlIiwib3V0ZXJIZWlnaHQiLCJkaWZmIiwicGFyc2VJbnQiLCJzY3JvbGxIZWlnaHQiLCIkd2luZG93IiwiY3VycmVudFNjcm9sbFBvc2l0aW9uIiwic2Nyb2xsVG9wIiwiaCIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsInRvTG93ZXJDYXNlIiwiaW5kZXhPZiIsImVhY2giLCJqUXVlcnkiLCJtZXNzYWdlIiwibnVtYmVyV2l0aENvbW1hcyIsIngiLCJ0b1N0cmluZyIsIlZ1ZSIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJhcHBGcmllbmRzIiwiZWwiLCJjcmVhdGVkIiwiZ2V0RnJpZW5kIiwiYXhpb3MiLCJhbGwiLCJzcHJlYWQiLCJyZXF1ZXN0b3IiLCJwdXNoIiwiYXBwIiwiY29tcG9uZW50cyIsIk11bHRpc2VsZWN0IiwiVnVlTXVsdGlzZWxlY3QiLCJkZWZhdWx0Iiwia2V5d29yZCIsInByb2R1Y3RzX2ZldGNoZWQiLCJpc09uU2VhcmNoaW5nIiwid2F0Y2giLCJtZXRob2RzIiwibmFtZVdpdGhTdG9yZSIsIm5hbWUiLCJzdG9yZV9uYW1lIiwibGltaXRUZXh0IiwiY291bnQiLCJnb3RvcGFnZSIsInNsdWciLCJoYW5kbGVFbnRlciIsImtleUNvZGUiLCJpdGVtIiwiZ2V0ZmF2b3JpdGVzdG90YWwiLCJnZXRjb21wYXJldG90YWwiLCJpdGVtcyIsIml0ZW1fbmFtZXMiLCJnZXRDYXJ0Iiwib3JpZ2luIiwiY2FydCIsImNhcnRfaXRlbV9jb3VudCIsIm1hcCIsInJlc2ZyZXNoSXRlbXMiLCJyZW1vdmVJdGVtcyIsInBhcmFtcyIsInJvd19pZCIsInJvd0lkIiwidGV4dCIsInNwbGljZSIsImNvbXB1dGVkIiwidG90YWxfcHJpY2UiLCJzdW0iLCJzYWxlUHJpY2UiLCJzYWxlX3ByaWNlIiwicXR5IiwibW91c2VsZWF2ZSIsImRvY3NDaGVja0FwcCIsInNhbXBsZSIsInNhbXBsZTIiLCJzaG93RXhwaXJpbmdNZXNzYWdlIiwiZG9jdHlwZSIsImV4cGlyeURhdGUiLCJleHBpcnlfZGF0ZSIsIm1vbWVudCIsIndhcm5pbmciLCJkb2NzT2JqIiwibmJpX29iaiIsIm9iaiIsImJpcnRoQ2VydF9vYmoiLCJnb3ZJRF9vYmoiLCJzZWNfb2JqIiwiYmlyX29iaiIsInBlcm1pdF9vYmoiLCJleHBpcmVzX2F0IiwiY2F0ZWdvcmllcyIsImV3YWxsZXRfYmFsYW5jZSIsInVzYWJsZV9iYWxhbmNlIiwiY250X21zZyIsImNudF9mciIsImNudF9ub3QiLCJnZXRMYXRlc3RFd2FsbGV0QmFsYW5jZSIsInBhcnNlRmxvYXQiLCJhbW91bnQiLCJ1c2FibGUiLCJnZXRVc2FibGVCYWxhbmNlIiwiZ2V0TWVzc2FnZXMiLCJnZXRGcmllbmRzIiwiZ2V0Tm90aSIsIm1zZ2NvdW50IiwiZm9yRWFjaCIsIm5vdGlmIiwic2Vlbl9hdCIsImZyY291bnQiLCJub3RzZWVuX2NvdW50Iiwibm90aWNvdW50Il0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNsREFBLEVBQUVDLFFBQUYsRUFBWUMsS0FBWixDQUFrQixZQUFZO0FBQzdCO0FBQ0E7QUFDQUYsR0FBRUcsRUFBRixDQUFLQyxTQUFMLENBQWVDLEdBQWYsQ0FBbUJDLE9BQW5CLEdBQTZCLE1BQTdCO0FBQ0FOLEdBQUVPLE1BQUYsQ0FBVSxJQUFWLEVBQWdCUCxFQUFFRyxFQUFGLENBQUtDLFNBQUwsQ0FBZUksUUFBL0IsRUFBeUM7O0FBRXJDLGNBQVk7QUFDWCxlQUFZO0FBQ1YsZ0JBQVksR0FERjtBQUVWLFlBQVE7QUFGRTtBQUREO0FBRnlCLEVBQXpDOztBQVdBO0FBQ0FDLGVBQWNULEVBQUUsYUFBRixFQUFpQlUsR0FBakIsQ0FBcUIsa0JBQXJCLENBQWQ7QUFDQztBQUNELEtBQUlDLGlCQUFpQixLQUFyQjtBQUNBWCxHQUFFLFdBQUYsRUFBZVksT0FBZixDQUF1QjtBQUN0QkMsU0FBTSxhQURnQjtBQUV0QkMsY0FBV0wsV0FGVztBQUd0Qk0sY0FBWSxvQkFBU0MsSUFBVCxFQUFjO0FBQ3pCLE9BQUdMLGNBQUgsRUFBa0I7QUFDakJNLG1CQUFlakIsRUFBRSw4QkFBRixFQUFrQ2tCLEdBQWxDLEVBQWY7QUFDQ2xCLE1BQUVtQixJQUFGLENBQU87QUFDRUMsVUFBSyxhQUFXSCxZQURsQjtBQUVFSSxXQUFNO0FBRlIsS0FBUCxFQUdRQyxJQUhSLENBR2EsVUFBVUMsTUFBVixFQUFrQjtBQUN0QkMsWUFBT0MsUUFBUCxDQUFnQkMsTUFBaEI7QUFDSCxLQUxOO0FBTUQsSUFSRCxNQVNBO0FBQ0NmLHFCQUFpQixJQUFqQjtBQUNBO0FBRUU7QUFqQmtCOztBQW9CdkI7QUFwQkEsR0FxQkFnQixtQkFBbUJILE9BQU9JLE9BQVAsQ0FBZUMsUUFBbEM7QUFDQUMsaUJBQWdCLENBQWhCO0FBQ0EsU0FBT0gsZ0JBQVA7QUFDQyxPQUFLLElBQUw7QUFBV0csbUJBQWdCLENBQWhCLENBQW1CO0FBQzlCLE9BQUssSUFBTDtBQUFXQSxtQkFBZ0IsQ0FBaEIsQ0FBbUI7QUFDOUI7QUFBU0EsbUJBQWdCLENBQWhCLENBQW1CO0FBSDdCOztBQU1BOUIsR0FBRSxXQUFGLEVBQWVZLE9BQWYsQ0FBdUIsUUFBdkIsRUFBaUMsRUFBQ21CLE9BQU9ELGFBQVIsRUFBakM7QUFDQTlCLEdBQUUsNEJBQUYsRUFBZ0NVLEdBQWhDLENBQW9DLFNBQXBDLEVBQThDLE1BQTlDO0FBQ0FWLEdBQUUsc0JBQUYsRUFBMEJVLEdBQTFCLENBQThCO0FBQzdCc0IsVUFBTyxNQURzQjtBQUU3QkMsZ0JBQWE7QUFGZ0IsRUFBOUI7QUFJQWpDLEdBQUUsV0FBRixFQUFla0MsUUFBZixDQUF3QixXQUF4QjtBQUNBOztBQUVHbEMsR0FBRW1DLFFBQUYsQ0FBV0MsT0FBWCxDQUFtQkMsUUFBbkIsR0FBOEIsSUFBOUI7QUFDSHJDLEdBQUVtQyxRQUFGLENBQVdHLElBQVg7O0FBRUdDLFFBQU9ILE9BQVAsR0FBaUI7QUFDbkJJLFNBQU8sS0FEWTtBQUVuQkMsaUJBQWUsb0JBRkk7QUFHbkJDLFdBQVMsSUFIVTtBQUluQkMsVUFBUSxHQUpXO0FBS25CQyxXQUFTLElBTFU7QUFNbkJDLFdBQVMsSUFOVTtBQU9uQkMsbUJBQWlCO0FBUEUsRUFBakI7O0FBVUhDOztBQUVBO0FBQ0EvQyxHQUFFLE1BQUYsRUFBVWdELEtBQVYsQ0FBZ0IsWUFBVztBQUMxQmhELElBQUUsd0JBQUYsRUFBNEJpRCxJQUE1QjtBQUNBLEVBRkQ7O0FBSUFqRCxHQUFFLGtCQUFGLEVBQXNCZ0QsS0FBdEIsQ0FBNEIsVUFBU0UsS0FBVCxFQUFlO0FBQzFDQSxRQUFNQyxlQUFOO0FBQ0FuRCxJQUFFLHdCQUFGLEVBQTRCb0QsSUFBNUI7QUFDQSxFQUhEOztBQU1BO0FBQ0FwRCxHQUFFQyxRQUFGLEVBQVlvRCxFQUFaLENBQWUsWUFBZixFQUE0QixrQkFBNUIsRUFBK0MsVUFBU0MsQ0FBVCxFQUFXO0FBQ3pEQyxtQkFBaUJ2RCxFQUFFLGtCQUFGLEVBQXNCd0QsTUFBdEIsRUFBakI7QUFDQUMsa0JBQWdCekQsRUFBRSxJQUFGLEVBQVFhLEtBQVIsS0FBa0IsRUFBbEM7QUFDQSxNQUFJNkMsTUFBTzFELEVBQUUsSUFBRixFQUFRMkQsTUFBUixHQUFpQkQsR0FBakIsR0FBdUJsQyxPQUFPb0MsT0FBL0IsR0FBMENMLGNBQXBEO0FBQ0EsTUFBSU0sT0FBUTdELEVBQUUsSUFBRixFQUFRMkQsTUFBUixHQUFpQkUsSUFBakIsR0FBd0JyQyxPQUFPc0MsT0FBaEMsR0FBMkNMLGFBQXREOztBQUVBekQsSUFBRSxrQkFBRixFQUFzQlUsR0FBdEIsQ0FBMEIsRUFBQ21ELE1BQU1BLE9BQU0sSUFBYixFQUFtQkgsS0FBSUEsTUFBSSxJQUEzQixFQUExQjtBQUNBMUQsSUFBRSxrQkFBRixFQUFzQitELElBQXRCLENBQTJCLElBQTNCLEVBQWdDLElBQWhDLEVBQXNDcEIsTUFBdEMsQ0FBNkMsR0FBN0M7QUFDQXFCLE9BQUtoRSxFQUFFLElBQUYsRUFBUWdCLElBQVIsQ0FBYSxJQUFiLENBQUw7QUFDQWhCLElBQUUsa0JBQUYsRUFBc0JnQixJQUF0QixDQUEyQixJQUEzQixFQUFnQ2dELEVBQWhDOztBQUVBaEUsSUFBRSxjQUFGLEVBQWtCb0QsSUFBbEI7QUFDQXBELElBQUUseUJBQUYsRUFBNkJpRCxJQUE3QjtBQUNBakQsSUFBRSxnQkFBRixFQUFvQmlELElBQXBCOztBQUVBZ0IsYUFDRUMsR0FERixDQUNNLFVBRE4sRUFFRTVDLElBRkYsQ0FFUSxvQkFBWTtBQUNsQixPQUFHMEMsTUFBTXBDLFFBQVF1QyxJQUFSLENBQWFILEVBQXRCLEVBQ0E7QUFDQ2hFLE1BQUUsa0JBQUYsRUFBc0JvRSxJQUF0QixDQUEyQixpQkFBM0IsRUFBOENoQixJQUE5QztBQUNBcEQsTUFBRSxrQkFBRixFQUFzQm9FLElBQXRCLENBQTJCLGVBQTNCLEVBQTRDbkIsSUFBNUM7QUFDQWpELE1BQUUsY0FBRixFQUFrQmlELElBQWxCO0FBQ0FqRCxNQUFFLGdCQUFGLEVBQW9Cb0QsSUFBcEI7QUFDQSxJQU5ELE1BT0E7QUFDQ3BELE1BQUUsa0JBQUYsRUFBc0JvRSxJQUF0QixDQUEyQixpQkFBM0IsRUFBOENuQixJQUE5Qzs7QUFFQW9CLFlBQVEsQ0FBUjtBQUNBLFNBQUksSUFBSUMsSUFBRSxDQUFWLEVBQWFBLElBQUdDLFNBQVN2RCxJQUFULENBQWN3RCxPQUFkLENBQXNCQyxNQUF0QyxFQUE4Q0gsR0FBOUMsRUFDQTtBQUNDLFNBQUdDLFNBQVN2RCxJQUFULENBQWN3RCxPQUFkLENBQXNCRixDQUF0QixFQUF5QkgsSUFBekIsQ0FBOEJILEVBQTlCLElBQW9DQSxFQUF2QyxFQUNBO0FBQ0NLLGNBQVEsU0FBUjtBQUNBO0FBQ0E7QUFDRDtBQUNELFNBQUksSUFBSUMsSUFBRSxDQUFWLEVBQWFBLElBQUdDLFNBQVN2RCxJQUFULENBQWMwRCxRQUFkLENBQXVCRCxNQUF2QyxFQUErQ0gsR0FBL0MsRUFDQTtBQUNDLFNBQUdELFNBQVMsQ0FBWixFQUNBO0FBQ0M7QUFDQTtBQUNELFNBQUdFLFNBQVN2RCxJQUFULENBQWMwRCxRQUFkLENBQXVCSixDQUF2QixFQUEwQkgsSUFBMUIsQ0FBK0JILEVBQS9CLElBQXFDQSxFQUF4QyxFQUNBO0FBQ0NLLGNBQVEsVUFBUjtBQUNBO0FBQ0E7QUFDRDtBQUNELFNBQUksSUFBSUMsSUFBRSxDQUFWLEVBQWFBLElBQUdDLFNBQVN2RCxJQUFULENBQWMyRCxVQUFkLENBQXlCRixNQUF6QyxFQUFpREgsR0FBakQsRUFDQTtBQUNDLFNBQUdELFNBQVMsQ0FBWixFQUNBO0FBQ0M7QUFDQTtBQUNELFNBQUdFLFNBQVN2RCxJQUFULENBQWMyRCxVQUFkLENBQXlCTCxDQUF6QixFQUE0QkgsSUFBNUIsQ0FBaUNILEVBQWpDLElBQXVDQSxFQUExQyxFQUNBO0FBQ0NLLGNBQVEsWUFBUjtBQUNBO0FBQ0E7QUFDRDtBQUNELFNBQUksSUFBSUMsSUFBRSxDQUFWLEVBQWFBLElBQUdDLFNBQVN2RCxJQUFULENBQWM0RCxPQUFkLENBQXNCSCxNQUF0QyxFQUE4Q0gsR0FBOUMsRUFDQTtBQUNDLFNBQUdELFNBQVMsQ0FBWixFQUNBO0FBQ0M7QUFDQTtBQUNELFNBQUdFLFNBQVN2RCxJQUFULENBQWM0RCxPQUFkLENBQXNCTixDQUF0QixFQUF5QkgsSUFBekIsQ0FBOEJILEVBQTlCLElBQW9DQSxFQUF2QyxFQUNBO0FBQ0NLLGNBQVEsU0FBUjtBQUNBO0FBQ0E7QUFDRDs7QUFFRCxZQUFPQSxLQUFQO0FBRUMsVUFBSyxTQUFMO0FBQ0M7QUFDQXJFLFFBQUUsZ0NBQUYsRUFBb0NvRCxJQUFwQztBQUNBOztBQUVELFVBQUssVUFBTDtBQUNDO0FBQ0FwRCxRQUFFLG9DQUFGLEVBQXdDb0QsSUFBeEM7QUFDQTtBQUNBOztBQUVELFVBQUssWUFBTDtBQUNDcEQsUUFBRSxnQ0FBRixFQUFvQ29ELElBQXBDO0FBQ0E7QUFDQTs7QUFFRCxVQUFLLFNBQUw7QUFDQ3BELFFBQUUsa0NBQUYsRUFBc0NvRCxJQUF0QztBQUNBOztBQUVEO0FBQ0NwRCxRQUFFLGdDQUFGLEVBQW9Db0QsSUFBcEM7QUFDQTtBQUNBcEQsUUFBRSxnQ0FBRixFQUFvQ29ELElBQXBDO0FBQ0E7QUExQkY7O0FBNkJBcEQsTUFBRSxjQUFGLEVBQWtCaUQsSUFBbEI7QUFDQWpELE1BQUUsZ0JBQUYsRUFBb0JvRCxJQUFwQjtBQUNBO0FBQ0QsR0EzRkYsRUE0RkV5QixLQTVGRixDQTRGUTdFLEVBQUU4RSxJQTVGVjtBQThGQSxFQTdHRCxFQTZHR3pCLEVBN0dILENBNkdNLFlBN0dOLEVBNkdtQixrQkE3R25CLEVBNkdzQyxZQUFVO0FBQy9DLE1BQUlyRCxFQUFFLHdCQUFGLEVBQTRCeUUsTUFBaEMsRUFDQTtBQUNDekUsS0FBRSxrQkFBRixFQUFzQitELElBQXRCLENBQTJCLElBQTNCLEVBQWdDLElBQWhDLEVBQXNDcEIsTUFBdEMsQ0FBNkMsR0FBN0M7QUFFQSxHQUpELE1BS0E7QUFDQzNDLEtBQUUsa0JBQUYsRUFBc0IrRCxJQUF0QixDQUEyQixJQUEzQixFQUFnQyxJQUFoQyxFQUFzQ25CLE9BQXRDLENBQThDLEdBQTlDO0FBQ0E7QUFDRCxFQXRIRDs7QUF3SEE1QyxHQUFFLE1BQUYsRUFBVXFELEVBQVYsQ0FBYSxZQUFiLEVBQTBCLGtCQUExQixFQUE2QyxZQUFVO0FBQ3REckQsSUFBRSxJQUFGLEVBQVErRCxJQUFSLENBQWEsSUFBYixFQUFrQixJQUFsQixFQUF3Qm5CLE9BQXhCLENBQWdDLEdBQWhDO0FBQ0FtQyxXQUFTL0UsRUFBRSxJQUFGLEVBQVFvRSxJQUFSLENBQWEsU0FBYixFQUF3QlksTUFBeEIsQ0FBK0IsVUFBL0IsQ0FBVDtBQUNBQyxZQUFVRixPQUFPRyxJQUFQLEVBQVY7QUFDQUgsU0FBT2hCLElBQVAsQ0FBWSxJQUFaLEVBQWlCLElBQWpCLEVBQXVCbkIsT0FBdkIsQ0FBK0IsR0FBL0IsRUFBbUMsWUFBVTtBQUM1Q3FDLFdBQVFsQixJQUFSLENBQWEsSUFBYixFQUFrQixJQUFsQixFQUF3QnBCLE1BQXhCLENBQStCLEdBQS9CO0FBQ0EsR0FGRDtBQUdBLEVBUEQ7O0FBU0EzQyxHQUFFLGdDQUFGLEVBQW9DZ0QsS0FBcEMsQ0FBMEMsWUFBVTtBQUNuRGlCLGFBQ0VrQixJQURGLENBQ08sVUFEUCxFQUNrQjtBQUNoQkMsYUFBU3BGLEVBQUUsa0JBQUYsRUFBc0JnQixJQUF0QixDQUEyQixJQUEzQjtBQURPLEdBRGxCLEVBSUVNLElBSkYsQ0FJTyxvQkFBWTtBQUNqQixPQUFHaUQsU0FBU2MsTUFBVCxJQUFtQixHQUF0QixFQUNBO0FBQ0M5QyxXQUFPK0MsT0FBUCxDQUFlLEVBQWYsRUFBa0Isc0JBQWxCO0FBQ0F0RixNQUFFLGtCQUFGLEVBQXNCaUQsSUFBdEI7QUFDQTtBQUNELEdBVkY7QUFXQSxFQVpEOztBQWNBakQsR0FBRSxrQ0FBRixFQUFzQ2dELEtBQXRDLENBQTRDLFlBQVU7QUFDckR1QyxVQUFRQyxHQUFSLENBQVl4RixFQUFFLGtCQUFGLEVBQXNCZ0IsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBWjtBQUNBdUUsVUFBUUMsR0FBUixDQUFZLHFCQUFaO0FBQ0EsRUFIRDs7QUFLQXhGLEdBQUUsZ0NBQUYsRUFBb0NnRCxLQUFwQyxDQUEwQyxZQUFVO0FBQ25EaUIsYUFDRWtCLElBREYsQ0FDTyxnQkFEUCxFQUN3QjtBQUN0QkMsYUFBVXBGLEVBQUUsa0JBQUYsRUFBc0JnQixJQUF0QixDQUEyQixJQUEzQjtBQURZLEdBRHhCLEVBSUVNLElBSkYsQ0FJTyxvQkFBWTtBQUNqQmlCLFVBQU8rQyxPQUFQLENBQWUsRUFBZixFQUFrQixlQUFsQjtBQUNBdEYsS0FBRSxrQkFBRixFQUFzQmlELElBQXRCO0FBQ0EsR0FQRjtBQVFBLEVBVEQ7O0FBV0FqRCxHQUFFLHFFQUFGLEVBQXlFZ0QsS0FBekUsQ0FBK0UsWUFBVTtBQUN4RmlCLGFBQ0VrQixJQURGLENBQ08sY0FEUCxFQUNzQjtBQUNwQk0sWUFBUTdELFFBQVF1QyxJQUFSLENBQWFILEVBREQ7QUFFcEJvQixhQUFTcEYsRUFBRSxrQkFBRixFQUFzQmdCLElBQXRCLENBQTJCLElBQTNCO0FBRlcsR0FEdEIsRUFLRU0sSUFMRixDQUtPLG9CQUFZO0FBQ2pCLE9BQUdpRCxTQUFTYyxNQUFULElBQW1CLEdBQXRCLEVBQ0E7O0FBRUNwQixlQUNFeUIsTUFERixDQUNTLGNBQVluQixTQUFTdkQsSUFEOUIsRUFFRU0sSUFGRixDQUVPLG9CQUFZO0FBQ2pCLFNBQUdpRCxTQUFTYyxNQUFULElBQW1CLEdBQXRCLEVBQ0E7QUFDQzlDLGFBQU8rQyxPQUFQLENBQWUsRUFBZixFQUFrQixTQUFsQjtBQUNBdEYsUUFBRSxrQkFBRixFQUFzQmlELElBQXRCO0FBQ0E7QUFDRCxLQVJGLEVBU0U0QixLQVRGLENBU1E3RSxFQUFFOEUsSUFUVjtBQVVBO0FBQ0QsR0FwQkY7QUFxQkE7O0FBRUQ7OztBQXhCQTtBQStCQSxDQWxSRCxFLENBa1JHOztBQUVIdEQsT0FBT21FLFNBQVAsR0FBbUIsWUFBVztBQUM3QixRQUFPM0YsRUFBRSwwQkFBRixFQUE4QjRGLElBQTlCLENBQW1DLFNBQW5DLE1BQWdELElBQXZEO0FBQ0EsQ0FGRDs7QUFJQXBFLE9BQU91QixnQkFBUCxHQUEwQixZQUFVOztBQUVuQy9DLEdBQUUseUJBQUYsRUFBNkI2RixPQUE3QjtBQUNBN0YsR0FBRSx5QkFBRixFQUE2QjhGLE9BQTdCLENBQXFDLEVBQUNDLE1BQUssSUFBTixFQUFyQzs7QUFFQUMsV0FBVSwrS0FBVjtBQUNBaEcsR0FBRSxrQkFBRixFQUFzQmlHLFNBQXRCLENBQWlDLGlDQUFqQztBQUNBakcsR0FBRSxrQkFBRixFQUFzQmtHLE9BQXRCLENBQThCRixPQUE5Qjs7QUFFR2hHLEdBQUUsa0JBQUYsRUFBc0JnRCxLQUF0QixDQUE0QixZQUFVO0FBQ3JDaUMsWUFBVWpGLEVBQUUsSUFBRixFQUFRb0UsSUFBUixDQUFhLGNBQWIsQ0FBVjtBQUNBVyxXQUFTL0UsRUFBRSxJQUFGLEVBQVFvRSxJQUFSLENBQWEsU0FBYixDQUFUO0FBQ0FhLFVBQVFsQixJQUFSLENBQWEsSUFBYixFQUFrQixJQUFsQixFQUF3Qm5CLE9BQXhCLENBQWdDLEdBQWhDLEVBQW9DLFlBQVU7QUFDN0NtQyxVQUFPaEIsSUFBUCxDQUFZLElBQVosRUFBaUIsSUFBakIsRUFBdUJwQixNQUF2QixDQUE4QixHQUE5QjtBQUNBLEdBRkQ7QUFHQSxFQU5EOztBQVFBM0MsR0FBR0MsUUFBSCxFQUFja0csUUFBZCxDQUF1QixVQUFTQyxDQUFULEVBQVdDLENBQVgsRUFBYUMsQ0FBYixFQUFnQjtBQUN0QztBQUNHO0FBQ0M7QUFDRDtBQUNILEVBTEQ7QUFNQXRHLEdBQUdDLFFBQUgsRUFBY3NHLFlBQWQsQ0FBMkIsWUFBVztBQUNwQ3ZHLElBQUcsU0FBSCxFQUFlaUQsSUFBZjtBQUNKZ0MsWUFBVWpGLEVBQUUsa0JBQUYsRUFBc0JvRSxJQUF0QixDQUEyQixjQUEzQixDQUFWO0FBQ0FXLFdBQVMvRSxFQUFFLGtCQUFGLEVBQXNCb0UsSUFBdEIsQ0FBMkIsU0FBM0IsQ0FBVDtBQUNBVyxTQUFPbkMsT0FBUCxDQUFlLEdBQWYsRUFBbUIsWUFBVTtBQUM1QnFDLFdBQVF0QyxNQUFSLENBQWUsR0FBZjtBQUNBLEdBRkQ7QUFHRyxFQVBEO0FBUUEzQyxHQUFHQyxRQUFILEVBQWN1RyxRQUFkLENBQXVCLFlBQVc7QUFDaEM7QUFDQXZCLFlBQVVqRixFQUFFLGtCQUFGLEVBQXNCb0UsSUFBdEIsQ0FBMkIsY0FBM0IsQ0FBVjtBQUNKVyxXQUFTL0UsRUFBRSxrQkFBRixFQUFzQm9FLElBQXRCLENBQTJCLFNBQTNCLENBQVQ7QUFDQVcsU0FBT25DLE9BQVAsQ0FBZSxHQUFmLEVBQW1CLFlBQVU7QUFDNUJxQyxXQUFRdEMsTUFBUixDQUFlLEdBQWY7QUFDQSxHQUZEO0FBSUcsRUFSRDtBQVNDM0MsR0FBR0MsUUFBSCxFQUFjd0csV0FBZCxDQUEwQixZQUFXO0FBQ3RDO0FBQ0F4QixZQUFVakYsRUFBRSxrQkFBRixFQUFzQm9FLElBQXRCLENBQTJCLGNBQTNCLENBQVY7QUFDRlcsV0FBUy9FLEVBQUUsa0JBQUYsRUFBc0JvRSxJQUF0QixDQUEyQixTQUEzQixDQUFUO0FBQ0FXLFNBQU9uQyxPQUFQLENBQWUsR0FBZixFQUFtQixZQUFVO0FBQzVCcUMsV0FBUXRDLE1BQVIsQ0FBZSxHQUFmO0FBQ0EsR0FGRDtBQUdHLEVBUEE7QUFRRDNDLEdBQUdDLFFBQUgsRUFBY3lHLFNBQWQsQ0FBd0IsWUFBVztBQUNqQztBQUNBekIsWUFBVWpGLEVBQUUsa0JBQUYsRUFBc0JvRSxJQUF0QixDQUEyQixjQUEzQixDQUFWO0FBQ0pXLFdBQVMvRSxFQUFFLGtCQUFGLEVBQXNCb0UsSUFBdEIsQ0FBMkIsU0FBM0IsQ0FBVDtBQUNBVyxTQUFPbkMsT0FBUCxDQUFlLEdBQWYsRUFBbUIsWUFBVTtBQUM1QnFDLFdBQVF0QyxNQUFSLENBQWUsR0FBZjtBQUNBLEdBRkQ7QUFHRyxFQVBEO0FBVUg7O0FBRUQ7Ozs7O0FBNURBLENBaUVDLENBQUMsVUFBVTNDLENBQVYsRUFBYXdCLE1BQWIsRUFBcUJ2QixRQUFyQixFQUErQjBHLFNBQS9CLEVBQTBDOztBQUUxQyxLQUFJQyxhQUFhLGtCQUFqQjtBQUNBLEtBQUlDLGlCQUFpQixZQUFZRCxVQUFqQzs7QUFFQSxLQUFJRSxlQUFlLFNBQWZBLFlBQWUsQ0FBVUMsS0FBVixFQUFpQjtBQUNsQyxTQUFRQSxNQUFNQyxPQUFOLENBQWMsS0FBZCxFQUFxQixFQUFyQixFQUF5QnZDLE1BQXpCLEdBQWtDLENBQTFDO0FBQ0QsRUFGRDs7QUFJQSxVQUFTd0MsTUFBVCxDQUFnQkMsT0FBaEIsRUFBeUI5RSxPQUF6QixFQUFrQztBQUNoQyxPQUFLOEUsT0FBTCxHQUFlQSxPQUFmO0FBQ0EsT0FBS0MsUUFBTCxHQUFnQm5ILEVBQUVrSCxPQUFGLENBQWhCO0FBQ0EsT0FBSzVFLElBQUw7QUFDRDs7QUFFRDJFLFFBQU9HLFNBQVAsR0FBbUI7QUFDakI5RSxRQUFNLGdCQUFXO0FBQ2YsT0FBSWtCLFNBQVMsS0FBSzJELFFBQUwsQ0FBY0UsV0FBZCxFQUFiO0FBQ0EsT0FBSUMsT0FBT0MsU0FBUyxLQUFLSixRQUFMLENBQWN6RyxHQUFkLENBQWtCLGVBQWxCLENBQVQsSUFDQTZHLFNBQVMsS0FBS0osUUFBTCxDQUFjekcsR0FBZCxDQUFrQixZQUFsQixDQUFULENBREEsSUFDNkMsQ0FEeEQ7O0FBR0EsT0FBSW9HLGFBQWEsS0FBS0ksT0FBTCxDQUFhSCxLQUExQixDQUFKLEVBQXNDO0FBQ3BDLFNBQUtJLFFBQUwsQ0FBYzNELE1BQWQsQ0FBcUIsS0FBSzBELE9BQUwsQ0FBYU0sWUFBYixHQUE0QkYsSUFBakQ7QUFDQS9CLFlBQVFDLEdBQVIsQ0FBWSxLQUFLMEIsT0FBTCxDQUFhTSxZQUFiLEdBQTRCRixJQUF4QztBQUNEOztBQUVEO0FBQ0EsUUFBS0gsUUFBTCxDQUFjOUQsRUFBZCxDQUFpQixhQUFqQixFQUFnQyxVQUFTSCxLQUFULEVBQWdCO0FBQzlDLFFBQUl1RSxVQUFVekgsRUFBRXdCLE1BQUYsQ0FBZDtBQUNBLFFBQUlrRyx3QkFBd0JELFFBQVFFLFNBQVIsRUFBNUI7O0FBRUEzSCxNQUFFLElBQUYsRUFDR3dELE1BREgsQ0FDVSxDQURWLEVBRUdBLE1BRkgsQ0FFVSxLQUFLZ0UsWUFBTCxHQUFvQkYsSUFGOUI7O0FBSUEsUUFBSXRELEtBQUtoRSxFQUFFLElBQUYsRUFBUTRGLElBQVIsQ0FBYSxXQUFiLENBQVQ7QUFDQSxRQUFJZ0MsSUFBSSxLQUFLSixZQUFMLEdBQW9CRixJQUE1Qjs7QUFFTixRQUFJTyxVQUFVQyxTQUFWLENBQW9CQyxXQUFwQixHQUFrQ0MsT0FBbEMsQ0FBMEMsU0FBMUMsSUFBdUQsQ0FBQyxDQUE1RCxFQUErRDtBQUN4RCxTQUFHSixLQUFHLEVBQU4sRUFDRTVILEVBQUUsTUFBSWdFLEVBQU4sRUFBVXRELEdBQVYsQ0FBYyxRQUFkLEVBQXVCLEtBQXZCLEVBREYsS0FFSyxJQUFHa0gsS0FBRyxFQUFOLEVBQ0g1SCxFQUFFLE1BQUlnRSxFQUFOLEVBQVV0RCxHQUFWLENBQWMsUUFBZCxFQUF1QixLQUF2QixFQURHLEtBRUEsSUFBR2tILEtBQUcsRUFBTixFQUNINUgsRUFBRSxNQUFJZ0UsRUFBTixFQUFVdEQsR0FBVixDQUFjLFFBQWQsRUFBdUIsS0FBdkIsRUFERyxLQUVBLElBQUdrSCxLQUFHLEVBQU4sRUFDSDVILEVBQUUsTUFBSWdFLEVBQU4sRUFBVXRELEdBQVYsQ0FBYyxRQUFkLEVBQXVCLEtBQXZCLEVBREcsS0FFQSxJQUFHa0gsS0FBRyxHQUFOLEVBQ0g1SCxFQUFFLE1BQUlnRSxFQUFOLEVBQVV0RCxHQUFWLENBQWMsUUFBZCxFQUF1QixLQUF2QjtBQUNSOztBQUVELFFBQUltSCxVQUFVQyxTQUFWLENBQW9CQyxXQUFwQixHQUFrQ0MsT0FBbEMsQ0FBMEMsUUFBMUMsSUFBc0QsQ0FBQyxDQUEzRCxFQUE4RDtBQUN2RCxTQUFHSixLQUFHLEVBQU4sRUFDRTVILEVBQUUsTUFBSWdFLEVBQU4sRUFBVXRELEdBQVYsQ0FBYyxRQUFkLEVBQXVCLEtBQXZCLEVBREYsS0FFSyxJQUFHa0gsS0FBRyxFQUFOLEVBQ0g1SCxFQUFFLE1BQUlnRSxFQUFOLEVBQVV0RCxHQUFWLENBQWMsUUFBZCxFQUF1QixLQUF2QixFQURHLEtBRUEsSUFBR2tILEtBQUcsRUFBTixFQUNINUgsRUFBRSxNQUFJZ0UsRUFBTixFQUFVdEQsR0FBVixDQUFjLFFBQWQsRUFBdUIsS0FBdkIsRUFERyxLQUVBLElBQUdrSCxLQUFHLEVBQU4sRUFDSDVILEVBQUUsTUFBSWdFLEVBQU4sRUFBVXRELEdBQVYsQ0FBYyxRQUFkLEVBQXVCLEtBQXZCLEVBREcsS0FFQSxJQUFHa0gsS0FBRyxHQUFOLEVBQ0g1SCxFQUFFLE1BQUlnRSxFQUFOLEVBQVV0RCxHQUFWLENBQWMsUUFBZCxFQUF1QixLQUF2QjtBQUNSOztBQUVLK0csWUFBUUUsU0FBUixDQUFrQkQscUJBQWxCO0FBQ0QsSUF0Q0Q7QUF1Q0Q7QUFuRGdCLEVBQW5COztBQXNEQTFILEdBQUVHLEVBQUYsQ0FBS3lHLFVBQUwsSUFBbUIsVUFBVXhFLE9BQVYsRUFBbUI7QUFDcEMsT0FBSzZGLElBQUwsQ0FBVSxZQUFXO0FBQ25CLE9BQUksQ0FBQ2pJLEVBQUVnQixJQUFGLENBQU8sSUFBUCxFQUFhNkYsY0FBYixDQUFMLEVBQW1DO0FBQ2pDN0csTUFBRWdCLElBQUYsQ0FBTyxJQUFQLEVBQWE2RixjQUFiLEVBQTZCLElBQUlJLE1BQUosQ0FBVyxJQUFYLEVBQWlCN0UsT0FBakIsQ0FBN0I7QUFDRDtBQUNGLEdBSkQ7QUFLQSxTQUFPLElBQVA7QUFDRCxFQVBEO0FBU0QsQ0E5RUEsRUE4RUU4RixNQTlFRixFQThFVTFHLE1BOUVWLEVBOEVrQnZCLFFBOUVsQjs7QUFnRkc7QUFDQSxJQUFJdUIsT0FBT0ksT0FBUCxDQUFldUcsT0FBbkIsRUFBNEI7QUFDeEI1RixRQUFPK0MsT0FBUCxDQUFlOUQsT0FBT0ksT0FBUCxDQUFldUcsT0FBOUIsRUFBdUMsSUFBdkMsRUFBNkM7QUFDekMxRixpQkFBZTtBQUQwQixFQUE3QztBQUdIOztBQUVMakIsT0FBTzRHLGdCQUFQLEdBQTBCLFVBQVNDLENBQVQsRUFBWTtBQUNsQyxRQUFPQSxFQUFFQyxRQUFGLEdBQWF0QixPQUFiLENBQXFCLHVCQUFyQixFQUE4QyxHQUE5QyxDQUFQO0FBQ0gsQ0FGRDs7QUFLQXVCLElBQUlDLFNBQUosQ0FBYyxjQUFkLEVBQStCLG1CQUFBQyxDQUFRLEdBQVIsQ0FBL0I7O0FBR0EsSUFBSUMsYUFBYSxJQUFJSCxHQUFKLENBQVE7QUFDeEJJLEtBQUksY0FEb0I7O0FBR3hCM0gsT0FBSztBQUNKMEQsWUFBUztBQURMLEVBSG1COztBQVF4QmtFLFFBUndCLHFCQVFmO0FBQUE7O0FBQ1IsV0FBU0MsU0FBVCxHQUFxQjtBQUNYLFVBQU81RSxXQUFXQyxHQUFYLENBQWUsVUFBZixDQUFQO0FBQ0g7O0FBRUQ0RSxRQUFNQyxHQUFOLENBQVUsQ0FDTEYsV0FESyxDQUFWLEVBRUd2SCxJQUZILENBRVF3SCxNQUFNRSxNQUFOLENBQ0osVUFDS3hFLE9BREwsRUFFSztBQUNMLFFBQUksSUFBSUYsSUFBRSxDQUFWLEVBQWFBLElBQUdFLFFBQVF4RCxJQUFSLENBQWEwRCxRQUFiLENBQXNCRCxNQUF0QyxFQUE4Q0gsR0FBOUMsRUFBa0Q7QUFDOUMsUUFBRyxDQUFDRSxRQUFReEQsSUFBUixDQUFhMEQsUUFBYixDQUFzQkosQ0FBdEIsRUFBeUIyRSxTQUE3QixFQUNBO0FBQ0ksV0FBS3ZFLFFBQUwsQ0FBY3dFLElBQWQsQ0FBbUIxRSxRQUFReEQsSUFBUixDQUFhMEQsUUFBYixDQUFzQkosQ0FBdEIsQ0FBbkI7QUFDSDtBQUNKO0FBRUosR0FYTyxDQUZSLEVBY0NPLEtBZEQsQ0FjTzdFLEVBQUU4RSxJQWRUO0FBZU47QUE1QnVCLENBQVIsQ0FBakI7O0FBK0JBLElBQUlxRSxNQUFNLElBQUlaLEdBQUosQ0FBUTtBQUNqQkksS0FBSSxrQkFEYTtBQUVqQlMsYUFBWTtBQUNYQyxlQUFhN0gsT0FBTzhILGNBQVAsQ0FBc0JDO0FBRHhCLEVBRks7QUFLakJ2SSxPQUFLO0FBQ0p3SSxXQUFRLEVBREo7QUFFSkMsb0JBQWlCLEVBRmI7QUFHSkMsaUJBQWM7QUFIVixFQUxZO0FBVWpCQyxRQUFPO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBdEJNLEVBVlU7QUFrQ2pCQyxVQUFRO0FBQ1BDLGVBRE8sK0JBQzRCO0FBQUEsT0FBbkJDLElBQW1CLFFBQW5CQSxJQUFtQjtBQUFBLE9BQWJDLFVBQWEsUUFBYkEsVUFBYTs7QUFDL0IsVUFBVUQsSUFBVixVQUFtQkMsVUFBbkI7QUFDQSxHQUhHO0FBSVBDLFdBSk8scUJBSUlDLEtBSkosRUFJVztBQUNiLG1CQUFjQSxLQUFkO0FBQ0QsR0FORztBQU9KQyxVQVBJLG9CQU9LQyxJQVBMLEVBT1U7QUFDaEIzSSxVQUFPQyxRQUFQLEdBQWtCLHFCQUFtQjBJLElBQXJDO0FBQ0csR0FURzs7QUFVSkMsZUFBYSxxQkFBVTlHLENBQVYsRUFBYTtBQUN6QixPQUFJQSxFQUFFK0csT0FBRixJQUFhLEVBQWpCLEVBQXFCO0FBQ3ZCN0ksV0FBT0MsUUFBUCxHQUFrQixxQkFBbUIsS0FBSytILE9BQTFDO0FBQ0c7QUFDSjtBQWRNO0FBbENTLENBQVIsQ0FBVjs7QUFvREEsSUFBSUwsTUFBTSxJQUFJWixHQUFKLENBQVE7QUFDakJJLEtBQUksWUFEYTtBQUVqQjNILE9BQU07QUFDTHNKLFFBQUs7QUFEQSxFQUZXO0FBS2pCMUIsUUFMaUIscUJBS1I7QUFBQTs7QUFDUixXQUFTMkIsaUJBQVQsR0FBNkI7QUFDNUIsT0FBRzNJLFFBQVF1QyxJQUFYLEVBQ0EsT0FBT0YsV0FBV0MsR0FBWCxDQUFlLG9CQUFmLENBQVA7QUFDQTtBQUNENEUsUUFBTUMsR0FBTixDQUFVLENBQ1R3QixtQkFEUyxDQUFWLEVBRUdqSixJQUZILENBRVF3SCxNQUFNRSxNQUFOLENBQ1AsVUFDRWlCLEtBREYsRUFFSztBQUNKLFVBQUtLLElBQUwsR0FBWUwsS0FBWjtBQUNELEdBTE8sQ0FGUixFQVFDcEYsS0FSRCxDQVFPN0UsRUFBRThFLElBUlQ7QUFTQTtBQW5CZ0IsQ0FBUixDQUFWOztBQXNCQSxJQUFJcUUsTUFBTSxJQUFJWixHQUFKLENBQVE7QUFDakJJLEtBQUksVUFEYTtBQUVqQjNILE9BQU07QUFDTHNKLFFBQUs7QUFEQSxFQUZXO0FBS2pCMUIsUUFMaUIscUJBS1I7QUFBQTs7QUFDUixXQUFTNEIsZUFBVCxHQUEyQjtBQUMxQixPQUFHNUksUUFBUXVDLElBQVgsRUFDQSxPQUFPRixXQUFXQyxHQUFYLENBQWUsa0JBQWYsQ0FBUDtBQUNBO0FBQ0Q0RSxRQUFNQyxHQUFOLENBQVUsQ0FDVHlCLGlCQURTLENBQVYsRUFFR2xKLElBRkgsQ0FFUXdILE1BQU1FLE1BQU4sQ0FDUCxVQUNFaUIsS0FERixFQUVLO0FBQ0o7QUFDQSxVQUFLSyxJQUFMLEdBQVlMLEtBQVo7QUFDRCxHQU5PLENBRlIsRUFTQ3BGLEtBVEQsQ0FTTzdFLEVBQUU4RSxJQVRUO0FBVUE7QUFwQmdCLENBQVIsQ0FBVjs7QUF3QkEsSUFBSXFFLE1BQU0sSUFBSVosR0FBSixDQUFRO0FBQ2pCSSxLQUFJLE9BRGE7QUFFakIzSCxPQUFNO0FBQ0x5SixTQUFNLEVBREQ7QUFFTFIsU0FBTSxFQUZEO0FBR0xTLGNBQVc7QUFITixFQUZXO0FBT2pCOUIsUUFQaUIscUJBT1I7QUFBQTs7QUFDUixXQUFTK0IsT0FBVCxHQUFtQjtBQUNsQixVQUFPN0IsTUFBTTVFLEdBQU4sQ0FBVXpDLFNBQVNtSixNQUFULEdBQWtCLFlBQTVCLENBQVA7QUFDQTtBQUNEOUIsUUFBTUMsR0FBTixDQUFVLENBQ1Q0QixTQURTLENBQVYsRUFFR3JKLElBRkgsQ0FFUXdILE1BQU1FLE1BQU4sQ0FDUCxVQUNFNkIsSUFERixFQUVLO0FBQ0osVUFBS1osS0FBTCxHQUFhWSxLQUFLN0osSUFBTCxDQUFVOEosZUFBdkI7QUFDQSxPQUFJRCxPQUFPN0ssRUFBRStLLEdBQUYsQ0FBTUYsS0FBSzdKLElBQUwsQ0FBVTZKLElBQWhCLEVBQXNCLFVBQVM5RCxLQUFULEVBQWdCaEYsS0FBaEIsRUFBdUI7QUFDcEQsV0FBTyxDQUFDZ0YsS0FBRCxDQUFQO0FBQ0gsSUFGVSxDQUFYO0FBR0EsVUFBSzBELEtBQUwsR0FBYUksSUFBYjtBQUVELEdBVk8sQ0FGUixFQWFDaEcsS0FiRCxDQWFPN0UsRUFBRThFLElBYlQ7QUFjQSxFQXpCZ0I7O0FBMEJqQjhFLFVBQVE7QUFDUG9CLGVBRE8sMkJBQ1E7QUFBQTs7QUFDZGxDLFNBQU01RSxHQUFOLENBQVV6QyxTQUFTbUosTUFBVCxHQUFrQixZQUE1QixFQUNDdEosSUFERCxDQUNNLGtCQUFVO0FBQ2YsUUFBSXVKLE9BQU83SyxFQUFFK0ssR0FBRixDQUFNeEosT0FBT1AsSUFBUCxDQUFZNkosSUFBbEIsRUFBd0IsVUFBUzlELEtBQVQsRUFBZ0JoRixLQUFoQixFQUF1QjtBQUN0RCxZQUFPLENBQUNnRixLQUFELENBQVA7QUFDSCxLQUZVLENBQVg7QUFHQSxXQUFLMEQsS0FBTCxHQUFhSSxJQUFiO0FBQ00sSUFOUDtBQU9BLEdBVE07QUFVUEksYUFWTyx1QkFVS2pILEVBVkwsRUFVUTtBQUNkOEUsU0FBTTVFLEdBQU4sQ0FBVXpDLFNBQVNtSixNQUFULEdBQWtCLGNBQTVCLEVBQTJDO0FBQ3ZDTSxZQUFRO0FBQ1BDLGFBQVEsS0FBS1YsS0FBTCxDQUFXekcsRUFBWCxFQUFlb0g7QUFEaEI7QUFEK0IsSUFBM0MsRUFLQzlKLElBTEQsQ0FLTSxrQkFBVTtBQUNmdEIsTUFBRSxpQ0FBRixFQUFxQ3FMLElBQXJDLENBQTBDOUosT0FBT1AsSUFBUCxDQUFZOEosZUFBdEQ7QUFDQSxJQVBEO0FBUUF2SSxVQUFPK0MsT0FBUCxDQUFlLDZCQUFmLEVBQThDLEtBQUttRixLQUFMLENBQVd6RyxFQUFYLEVBQWU4RixJQUE3RDtBQUNBLFFBQUtXLEtBQUwsQ0FBV2EsTUFBWCxDQUFrQnRILEVBQWxCLEVBQXFCLENBQXJCO0FBQ0E7QUFyQk0sRUExQlM7QUFrRGpCdUgsV0FBUztBQUNSQyxhQURRLHlCQUNLO0FBQ1pDLFNBQU0sQ0FBTjtBQUNBLFFBQUluSCxJQUFFLENBQU4sRUFBU0EsSUFBRSxLQUFLbUcsS0FBTCxDQUFXaEcsTUFBdEIsRUFBOEJILEdBQTlCLEVBQ0E7QUFDQyxRQUFJb0gsWUFBWSxLQUFLakIsS0FBTCxDQUFXbkcsQ0FBWCxFQUFjbEMsT0FBZCxDQUFzQnVKLFVBQXRDO0FBQ0FGLFdBQVFDLFVBQVUxRSxPQUFWLENBQWtCLEdBQWxCLEVBQXVCLEVBQXZCLElBQTZCLEtBQUt5RCxLQUFMLENBQVduRyxDQUFYLEVBQWNzSCxHQUFuRDtBQUNBO0FBQ0QsVUFBT0gsR0FBUDtBQUNBO0FBVE87QUFsRFEsQ0FBUixDQUFWOztBQWdFQSxJQUFJdEMsTUFBTSxJQUFJWixHQUFKLENBQVE7QUFDakJJLEtBQUksY0FEYTtBQUVqQjNILE9BQU07QUFDTHlKLFNBQU0sRUFERDtBQUVMUixTQUFNO0FBRkQsRUFGVztBQU1qQnJCLFFBTmlCLHFCQU1SO0FBQUE7O0FBQ1IsV0FBUytCLE9BQVQsR0FBbUI7QUFDbEIsVUFBTzdCLE1BQU01RSxHQUFOLENBQVV6QyxTQUFTbUosTUFBVCxHQUFrQixZQUE1QixDQUFQO0FBQ0E7QUFDRDlCLFFBQU1DLEdBQU4sQ0FBVSxDQUNUNEIsU0FEUyxDQUFWLEVBRUdySixJQUZILENBRVF3SCxNQUFNRSxNQUFOLENBQ1AsVUFDRTZCLElBREYsRUFFSztBQUNKLFVBQUtaLEtBQUwsR0FBYVksS0FBSzdKLElBQUwsQ0FBVThKLGVBQXZCO0FBQ0EsT0FBSUQsT0FBTzdLLEVBQUUrSyxHQUFGLENBQU1GLEtBQUs3SixJQUFMLENBQVU2SixJQUFoQixFQUFzQixVQUFTOUQsS0FBVCxFQUFnQmhGLEtBQWhCLEVBQXVCO0FBQ3BELFdBQU8sQ0FBQ2dGLEtBQUQsQ0FBUDtBQUNILElBRlUsQ0FBWDtBQUdBLFVBQUswRCxLQUFMLEdBQWFJLElBQWI7QUFDRCxHQVRPLENBRlIsRUFZQ2hHLEtBWkQsQ0FZTzdFLEVBQUU4RSxJQVpUO0FBYUE7QUF2QmdCLENBQVIsQ0FBVjs7QUEyQkE5RSxFQUFFLFdBQUYsRUFBZTZMLFVBQWYsQ0FBMEIsWUFBVztBQUNqQzdMLEdBQUUsZ0JBQUYsRUFBb0JnRCxLQUFwQjtBQUNILENBRkQ7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxJQUFJOEksZUFBZSxJQUFJdkQsR0FBSixDQUFRO0FBQzFCSSxLQUFJLGVBRHNCOztBQUcxQjNILE9BQUs7QUFDSitLLFVBQU8sQ0FESDtBQUVKQyxXQUFRO0FBRkosRUFIcUI7O0FBUTFCcEMsVUFBUTtBQUNQcUMscUJBRE8sK0JBQ2FDLE9BRGIsRUFDc0JDLFVBRHRCLEVBQ2lDO0FBQ25DO0FBQ0EsT0FBSUMsY0FBY0MsT0FBT0YsVUFBUCxDQUFsQjs7QUFFQSxPQUFJNUUsU0FBUzZFLFlBQVk5RSxJQUFaLENBQWlCK0UsUUFBakIsRUFBMkIsTUFBM0IsQ0FBVCxLQUFnRCxFQUFwRCxFQUNBO0FBQ0MsUUFBRzlFLFNBQVM2RSxZQUFZOUUsSUFBWixDQUFpQitFLFFBQWpCLEVBQTJCLE1BQTNCLENBQVQsSUFBK0MsQ0FBbEQsRUFBb0Q7QUFDbEQ5SixZQUFPK0osT0FBUCxDQUFlLFFBQU9GLFlBQVk5RSxJQUFaLENBQWlCK0UsUUFBakIsRUFBMkIsTUFBM0IsQ0FBUCxHQUEyQyxRQUExRCxFQUFxRUgsVUFBUSxhQUE3RSxFQUE0RjtBQUMxRnJKLGVBQVMsQ0FEaUY7QUFFMUZDLHVCQUFpQixDQUZ5RTtBQUcxRkosZUFBUyxtQkFBWTtBQUFFbEIsY0FBT0MsUUFBUCxHQUFrQix5QkFBbEI7QUFBNkM7QUFIc0IsTUFBNUY7QUFLRDtBQUNELFFBQUc4RixTQUFTNkUsWUFBWTlFLElBQVosQ0FBaUIrRSxRQUFqQixFQUEyQixNQUEzQixDQUFULElBQStDLENBQWxELEVBQW9EO0FBQ25EOUosWUFBTytKLE9BQVAsQ0FBZSxFQUFmLEVBQW9CSixVQUFRLGtEQUE1QixFQUFnRjtBQUM3RXJKLGVBQVMsQ0FEb0U7QUFFN0VDLHVCQUFpQixDQUY0RDtBQUc3RUosZUFBUyxtQkFBWTtBQUFFbEIsY0FBT0MsUUFBUCxHQUFrQix5QkFBbEI7QUFBNkM7QUFIUyxNQUFoRjtBQUtBO0FBQ0Q7QUFDRjtBQXRCRyxFQVJrQjs7QUFrQzFCbUgsUUFsQzBCLHFCQWtDakI7QUFBQTs7QUFDUixNQUFHaEgsUUFBUXVDLElBQVgsRUFBZ0I7QUFDaEJGLGNBQVdDLEdBQVgsQ0FBZSxZQUFVdEMsUUFBUXVDLElBQVIsQ0FBYUgsRUFBdkIsR0FBMEIsWUFBekMsRUFDUTFDLElBRFIsQ0FDYSxtQkFBVztBQUNmLFFBQUdpTCxRQUFRLFFBQVIsS0FBcUIsR0FBeEIsRUFDQTtBQUNHQyxlQUFnQkQsUUFBUXZMLElBQVIsQ0FBYWdFLE1BQWIsQ0FBb0IsZUFBTztBQUFFLGFBQU95SCxJQUFJcEwsSUFBSixLQUFhLEtBQXBCO0FBQTJCLE1BQXhELENBQWhCO0FBQ0FxTCxxQkFBZ0JILFFBQVF2TCxJQUFSLENBQWFnRSxNQUFiLENBQW9CLGVBQU87QUFBRSxhQUFPeUgsSUFBSXBMLElBQUosS0FBYSxXQUFwQjtBQUFpQyxNQUE5RCxDQUFoQjtBQUNBc0wsaUJBQWdCSixRQUFRdkwsSUFBUixDQUFhZ0UsTUFBYixDQUFvQixlQUFPO0FBQUUsYUFBT3lILElBQUlwTCxJQUFKLEtBQWEsT0FBcEI7QUFBNkIsTUFBMUQsQ0FBaEI7QUFDQXVMLGVBQWdCTCxRQUFRdkwsSUFBUixDQUFhZ0UsTUFBYixDQUFvQixlQUFPO0FBQUUsYUFBT3lILElBQUlwTCxJQUFKLEtBQWEsS0FBcEI7QUFBMkIsTUFBeEQsQ0FBaEI7QUFDQXdMLGVBQWdCTixRQUFRdkwsSUFBUixDQUFhZ0UsTUFBYixDQUFvQixlQUFPO0FBQUUsYUFBT3lILElBQUlwTCxJQUFKLEtBQWEsS0FBcEI7QUFBMkIsTUFBeEQsQ0FBaEI7QUFDQXlMLGtCQUFnQlAsUUFBUXZMLElBQVIsQ0FBYWdFLE1BQWIsQ0FBb0IsZUFBTztBQUFFLGFBQU95SCxJQUFJcEwsSUFBSixLQUFhLFFBQXBCO0FBQThCLE1BQTNELENBQWhCOztBQUVBLFNBQUdtTCxRQUFRL0gsTUFBWCxFQUNBO0FBQ0MsVUFBRytILFFBQVEsQ0FBUixFQUFXbkgsTUFBWCxJQUFxQixVQUFyQixJQUFtQ21ILFFBQVEsQ0FBUixFQUFXbkgsTUFBWCxJQUFxQixTQUEzRCxFQUNBO0FBQ0ksY0FBSzRHLG1CQUFMLENBQXlCLEtBQXpCLEVBQWdDTyxRQUFRLENBQVIsRUFBV08sVUFBM0M7QUFDSDtBQUNEOztBQUVELFNBQUdMLGNBQWNqSSxNQUFqQixFQUNBO0FBQ0MsVUFBR2lJLGNBQWMsQ0FBZCxFQUFpQnJILE1BQWpCLElBQTJCLFVBQTlCLEVBQ0E7QUFDSSxjQUFLNEcsbUJBQUwsQ0FBeUIsbUJBQXpCLEVBQThDUyxjQUFjLENBQWQsRUFBaUJLLFVBQS9EO0FBQ0g7QUFDRDs7QUFFRCxTQUFHSixVQUFVbEksTUFBYixFQUNBO0FBQ0MsVUFBR2tJLFVBQVUsQ0FBVixFQUFhdEgsTUFBYixJQUF1QixVQUExQixFQUNBO0FBQ0ksY0FBSzRHLG1CQUFMLENBQXlCLGVBQXpCLEVBQTBDVSxVQUFVLENBQVYsRUFBYUksVUFBdkQ7QUFDSDtBQUNEOztBQUVELFNBQUdILFFBQVFuSSxNQUFYLEVBQ0E7QUFDQyxVQUFHbUksUUFBUSxDQUFSLEVBQVd2SCxNQUFYLElBQXFCLFVBQXhCLEVBQ0E7QUFDSSxjQUFLNEcsbUJBQUwsQ0FBeUIsS0FBekIsRUFBZ0NXLFFBQVEsQ0FBUixFQUFXRyxVQUEzQztBQUNIO0FBQ0Q7O0FBRUQsU0FBR0YsUUFBUXBJLE1BQVgsRUFDQTtBQUNDLFVBQUdvSSxRQUFRLENBQVIsRUFBV3hILE1BQVgsSUFBcUIsVUFBeEIsRUFDQTtBQUNJLGNBQUs0RyxtQkFBTCxDQUF5QixLQUF6QixFQUFnQ1ksUUFBUSxDQUFSLEVBQVdFLFVBQTNDO0FBQ0g7QUFDRDs7QUFFRCxTQUFHRCxXQUFXckksTUFBZCxFQUNBO0FBQ0MsVUFBR3FJLFdBQVcsQ0FBWCxFQUFjekgsTUFBZCxJQUF3QixVQUEzQixFQUNBO0FBQ0ksY0FBSzRHLG1CQUFMLENBQXlCLFFBQXpCLEVBQW1DYSxXQUFXLENBQVgsRUFBY0MsVUFBakQ7QUFDSDtBQUNEO0FBRUg7QUFFRixJQTdEUixFQThEUWxJLEtBOURSLENBOERjLGlCQUFTO0FBQ2Q7QUFDSCxJQWhFTjtBQWlFQztBQUVEO0FBdkd5QixDQUFSLENBQW5COztBQTJHQTBELElBQUlDLFNBQUosQ0FBYyx1QkFBZCxFQUF3QyxtQkFBQUMsQ0FBUSxHQUFSLENBQXhDOztBQUVBLElBQUlVLE1BQU0sSUFBSVosR0FBSixDQUFRO0FBQ2hCSSxLQUFLLFlBRFc7O0FBR2hCQyxRQUhnQixxQkFHUDtBQUFBOztBQUNQM0UsYUFDRUMsR0FERixDQUNNLGdCQUROLEVBRUk1QyxJQUZKLENBRVMsb0JBQVk7QUFDaEIsVUFBSzBMLFVBQUwsR0FBa0J6SSxTQUFTdkQsSUFBM0I7QUFDTixHQUpDO0FBS0QsRUFUZTs7O0FBV2hCQSxPQUFLO0FBQ0hnTSxjQUFXO0FBRFI7QUFYVyxDQUFSLENBQVY7O0FBZ0JBLElBQUk3RCxNQUFNLElBQUlaLEdBQUosQ0FBUTtBQUNqQkksS0FBSSxtQkFEYTtBQUVqQjNILE9BQU07QUFDTGlNLG1CQUFnQixDQURYO0FBRUxDLGtCQUFlLENBRlY7QUFHTEMsV0FBUSxFQUhIO0FBSUxDLFVBQU8sRUFKRjtBQUtMQyxXQUFROztBQUxILEVBRlc7O0FBV2pCekQsVUFBUTtBQUNQMEQseUJBRE8scUNBQ2tCO0FBQUE7O0FBQ3hCLE9BQUcxTCxRQUFRdUMsSUFBWCxFQUFnQjtBQUNmRixlQUNFQyxHQURGLENBQ00sV0FBU3RDLFFBQVF1QyxJQUFSLENBQWFILEVBQXRCLEdBQXlCLFVBRC9CLEVBRUUxQyxJQUZGLENBRU8sa0JBQVU7QUFBRyxZQUFLMkwsZUFBTCxHQUF1Qk0sV0FBV2hNLE9BQU9QLElBQVAsQ0FBWUEsSUFBWixDQUFpQndNLE1BQTVCLENBQXZCO0FBQTJELEtBRi9FO0FBR0E7QUFDRCxPQUFHNUwsUUFBUXVDLElBQVgsRUFBZ0I7QUFDZkYsZUFDRUMsR0FERixDQUNNLFlBRE4sRUFFRTVDLElBRkYsQ0FFTyxrQkFBVTtBQUFHLFlBQUs0TCxjQUFMLEdBQXNCM0wsT0FBT1AsSUFBUCxDQUFZc0UsT0FBWixHQUFxQmlJLFdBQVdoTSxPQUFPUCxJQUFQLENBQVlBLElBQVosQ0FBaUJ5TSxNQUE1QixDQUFyQixHQUF5RCxDQUEvRTtBQUFpRixLQUZyRztBQUdBO0FBQ0QsR0FaTTtBQWFQQyxrQkFiTyw4QkFhWTtBQUFBOztBQUNsQixPQUFHOUwsUUFBUXVDLElBQVgsRUFBZ0I7QUFDZkYsZUFDRUMsR0FERixDQUNNLFlBRE4sRUFFRTVDLElBRkYsQ0FFTyxrQkFBVTtBQUFHLGFBQUs0TCxjQUFMLEdBQXNCM0wsT0FBT1AsSUFBUCxDQUFZc0UsT0FBWixHQUFxQmlJLFdBQVdoTSxPQUFPUCxJQUFQLENBQVlBLElBQVosQ0FBaUJ5TSxNQUE1QixDQUFyQixHQUF5RCxDQUEvRTtBQUFpRixLQUZyRztBQUdBO0FBQ0QsR0FuQk07QUFvQlBFLGFBcEJPLHlCQW9CTztBQUFBOztBQUNiLE9BQUcvTCxRQUFRdUMsSUFBWCxFQUFnQjtBQUNmRixlQUNFQyxHQURGLENBQ00sMEJBRE4sRUFFRTVDLElBRkYsQ0FFTyxrQkFBVTtBQUNmLGFBQUs2TCxPQUFMLEdBQWU1TCxPQUFPUCxJQUF0QjtBQUNBLEtBSkY7QUFLQTtBQUNLLEdBNUJBO0FBNkJENE0sWUE3QkMsd0JBNkJZO0FBQUE7O0FBQ2xCLE9BQUdoTSxRQUFRdUMsSUFBWCxFQUFnQjtBQUNmRixlQUNFQyxHQURGLENBQ00sd0JBRE4sRUFFRTVDLElBRkYsQ0FFTyxrQkFBVTtBQUNmLGFBQUs4TCxNQUFMLEdBQWM3TCxPQUFPUCxJQUFyQjtBQUNBLEtBSkY7QUFLQTtBQUNLLEdBckNBO0FBc0NENk0sU0F0Q0MscUJBc0NTO0FBQUE7O0FBQ2YsT0FBR2pNLFFBQVF1QyxJQUFYLEVBQWdCO0FBQ2ZGLGVBQ0VDLEdBREYsQ0FDTSxnQkFETixFQUVFNUMsSUFGRixDQUVPLGtCQUFVO0FBQ2YsYUFBSytMLE9BQUwsR0FBZTlMLE9BQU9QLElBQXRCO0FBQ0EsS0FKRjtBQUtBO0FBQ0s7QUE5Q0EsRUFYUztBQTJEakJ1SyxXQUFVO0FBQ0h1QyxVQURHLHNCQUNRO0FBQ1AsT0FBSTdELFFBQVEsQ0FBWjs7QUFFQSxPQUFJLEtBQUtrRCxPQUFULEVBQWtCO0FBQ2QsU0FBS0EsT0FBTCxDQUFhWSxPQUFiLENBQXFCLFVBQVVDLEtBQVYsRUFBaUI7QUFDbEMsU0FBR0EsTUFBTXZJLE9BQU4sSUFBaUJqRSxPQUFPSSxPQUFQLENBQWV1QyxJQUFmLENBQW9CSCxFQUF4QyxFQUEyQztBQUN2QyxVQUFJLENBQUVnSyxNQUFNQyxPQUFaLEVBQXFCO0FBQ2pCLFNBQUVoRSxLQUFGO0FBQ0g7QUFDSjtBQUNKLEtBTkQ7QUFPSDs7QUFFRCxPQUFJQSxRQUFRLEdBQVosRUFBZ0I7QUFDZCxXQUFPQSxLQUFQO0FBQ1YsSUFGUSxNQUVILElBQUdBLFNBQVMsR0FBWixFQUFnQjtBQUNyQixXQUFPLEtBQVA7QUFDQTtBQUNLLEdBbkJFO0FBb0JIaUUsU0FwQkcscUJBb0JPO0FBQ1QsT0FBSSxLQUFLZCxNQUFMLENBQVllLGFBQVosR0FBNEIsR0FBaEMsRUFBb0M7QUFDaEMsV0FBTyxLQUFLZixNQUFMLENBQVllLGFBQW5CO0FBQ1QsSUFGSyxNQUVBLElBQUcsS0FBS2YsTUFBTCxDQUFZZSxhQUFaLElBQTZCLEdBQWhDLEVBQW9DO0FBQ3pDLFdBQU8sS0FBUDtBQUNBO0FBRUssR0EzQkU7QUE0QkhDLFdBNUJHLHVCQTRCUztBQUNSLE9BQUluRSxRQUFRLENBQVo7O0FBRUEsT0FBSSxLQUFLb0QsT0FBVCxFQUFrQjtBQUNkLFNBQUtBLE9BQUwsQ0FBYVUsT0FBYixDQUFxQixVQUFVQyxLQUFWLEVBQWlCO0FBQ2xDLFNBQUksQ0FBRUEsTUFBTUMsT0FBWixFQUFxQjtBQUNqQixRQUFFaEUsS0FBRjtBQUNIO0FBQ0osS0FKRDtBQUtIOztBQUVELE9BQUlBLFFBQVEsR0FBWixFQUFnQjtBQUNkLFdBQU9BLEtBQVA7QUFDVixJQUZRLE1BRUgsSUFBR0EsU0FBUyxHQUFaLEVBQWdCO0FBQ3JCLFdBQU8sS0FBUDtBQUNBO0FBQ0s7QUE1Q0UsRUEzRE87O0FBMEdqQnJCLFFBMUdpQixxQkEwR1I7QUFDUixPQUFLMEUsdUJBQUw7QUFDQSxPQUFLSSxnQkFBTDtBQUNBLE9BQUtDLFdBQUw7QUFDQSxPQUFLQyxVQUFMO0FBQ0EsT0FBS0MsT0FBTDtBQUNBO0FBaEhnQixDQUFSLENBQVYsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pwQkE7Z0NBRUE7OzJCQUdBO0FBRkE7QUFJQTs7O1lBQ0E7MEJBQ0E7O21CQUVBO3VCQUNBO3NCQUNBOzZCQUNBO21CQUNBO21CQUNBO3lCQUNBOzBCQUNBO3lCQUNBOzBCQUVBO0FBWEE7QUFZQTs7QUFDQTs7d0NBQ0E7NkJBQ0E7QUFDQTtzQ0FDQTs2QkFDQTtBQUVBOztrQkFDQSxDQUNBLHdCQUNBLGtDQUNBLE9BQ0EsVUFDQSxjQUNBLGNBRUE7OzRDQUNBOzRDQUVBO0FBQ0Esb0JBRUE7O3NDQUVBOzsyREFDQSxLQUNBO2lEQUNBOzZEQUVBOzsyRUFDQTsyRUFDQSxLQUNBO2lFQUNBOzZFQUNBOzJGQUNBO0FBQ0E7QUFFQTs7OzJCQUlBO0FBSEE7OzhEQUlBOzZDQUNBOzJEQUNBO3FEQUNBOzREQUNBO29EQUNBOzJEQUNBO3FEQUNBOzREQUNBO0FBQ0E7QUFFQTtBQUVBOzs7OztBQUVBOztrREFDQTtvREFDQSwyQkFDQTttREFDQTtxRUFDQTtrREFDQTt3RUFDQTswREFDQTt5RUFDQTsyQkFDQTt1RUFDQTtBQUNBO0FBRUE7bUJBQ0E7d0NBQ0E7QUFDQTtBQUNBOzZEQUNBO2dCQUNBLFVBQ0E7aUNBQ0E7QUFDQSxtQkFDQTt1QkFDQTtBQUNBO0FBRUE7d0RBQ0E7d0NBQ0EsVUFFQSxDQUNBLE9BRUEsQ0FDQTtBQUdBO0FBeENBOzs7c0NBMENBO21DQUNBOzJDQUNBO3VFQUNBO0FBQ0E7Z0VBQ0E7QUFDQTs0REFDQTtBQUNBO3dDQUNBO21DQUNBO3FEQUNBO2lGQUNBO0FBQ0E7MEVBQ0E7QUFDQTtzRUFDQTtBQUNBOzBDQUNBO21DQUNBOzBHQUNBO3dHQUNBO0FBQ0E7b0dBQ0E7QUFDQTtnR0FDQTtBQUNBOzhCQUNBO3NCQUNBOzBCQUNBO0FBR0E7QUFqQ0E7QUF4SEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxRkE7VUFHQTs7OztBQUVBOztBQUNBOzhCQUVBO2tCQUVBO0FBSEEsa0NBSUE7K0JBQ0EsS0FDQTtBQUNBO29CQUdBO0FBRkEsa0NBR0E7K0JBQ0EsS0FDQTsrREFDQSxLQUNBO3VEQUNBLElBQ0E7aURBQ0E7QUFDQTtBQUNBO2lDQUNBO0FBQ0E7QUFDQSxxQkFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTs7QUFDQTs4QkFFQTtrQkFFQTtBQUhBLGtDQUlBOytCQUNBLEtBQ0E7QUFDQSxtREFDQSwyQkFDQTsrQkFDQSxLQUNBO2dFQUNBLEtBQ0E7d0RBQ0EsSUFDQTtrREFDQTtBQUNBO0FBQ0E7aUNBQ0E7QUFDQTtBQUNBLHFCQUNBO0FBQ0E7QUFDQTtBQUdBO0FBNURBOztBQUhBLEc7Ozs7Ozs7QUMxQ0E7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHlCQUFxSjtBQUNySjtBQUNBLHlCQUErRztBQUMvRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUMzQkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7O0FDcE5BLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6ImpzL21haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDQyMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjVmY2ZmMGUxZjA4MDA5N2NmNDYiLCIvLyB0aGlzIG1vZHVsZSBpcyBhIHJ1bnRpbWUgdXRpbGl0eSBmb3IgY2xlYW5lciBjb21wb25lbnQgbW9kdWxlIG91dHB1dCBhbmQgd2lsbFxuLy8gYmUgaW5jbHVkZWQgaW4gdGhlIGZpbmFsIHdlYnBhY2sgdXNlciBidW5kbGVcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQgKFxuICByYXdTY3JpcHRFeHBvcnRzLFxuICBjb21waWxlZFRlbXBsYXRlLFxuICBzY29wZUlkLFxuICBjc3NNb2R1bGVzXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICAvLyBpbmplY3QgY3NzTW9kdWxlc1xuICBpZiAoY3NzTW9kdWxlcykge1xuICAgIHZhciBjb21wdXRlZCA9IE9iamVjdC5jcmVhdGUob3B0aW9ucy5jb21wdXRlZCB8fCBudWxsKVxuICAgIE9iamVjdC5rZXlzKGNzc01vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIG1vZHVsZSA9IGNzc01vZHVsZXNba2V5XVxuICAgICAgY29tcHV0ZWRba2V5XSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIG1vZHVsZSB9XG4gICAgfSlcbiAgICBvcHRpb25zLmNvbXB1dGVkID0gY29tcHV0ZWRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAxOCAxOSAyMCAyMSAyMiAyMyAyNCAyNSAyNiAyNyIsIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcclxuXHQvL21hdGVyaWFsLXRvb2x0aXBcclxuXHQvL21ha2UgbmV4dCBhbmQgcHJldmlvdXMgYXMgc3ltYm9sc1xyXG5cdCQuZm4uZGF0YVRhYmxlLmV4dC5lcnJNb2RlID0gJ25vbmUnO1xyXG5cdCQuZXh0ZW5kKCB0cnVlLCAkLmZuLmRhdGFUYWJsZS5kZWZhdWx0cywge1xyXG5cdCAgIFxyXG5cdCAgICBcImxhbmd1YWdlXCI6IHtcclxuXHRcdCAgICBcInBhZ2luYXRlXCI6IHtcclxuXHRcdCAgICAgIFwicHJldmlvdXNcIjogXCI8XCIsXHJcblx0XHQgICAgICBcIm5leHRcIjogXCI+XCIsXHJcblx0XHQgICAgfVxyXG5cdFx0ICB9XHJcblx0fSApO1xyXG5cclxuXHJcblx0Ly9sYW5ndWFnZSBkcm9wZG93biBvbiBoZWFkZXIgaW5pdFxyXG5cdGhlYWRlckNvbG9yID0gJCgnLnRvcC1oZWFkZXInKS5jc3MoJ2JhY2tncm91bmQtY29sb3InKTtcclxuXHRcdC8vcGx1Z2luXHJcblx0dmFyIF9kZHNsaWNrTG9hZGVkID0gZmFsc2U7XHJcblx0JCgnI2xhbmd1YWdlJykuZGRzbGljayh7XHJcblx0XHR3aWR0aDonbWluLWNvbnRlbnQnLFxyXG5cdFx0YmFja2dyb3VuZDpoZWFkZXJDb2xvcixcclxuXHRcdG9uU2VsZWN0ZWQ6IGZ1bmN0aW9uKGRhdGEpe1xyXG5cdFx0XHRpZihfZGRzbGlja0xvYWRlZCl7XHJcblx0XHRcdFx0Y3VycmVudFZhbHVlID0gJCgnI2xhbmd1YWdlIC5kZC1zZWxlY3RlZC12YWx1ZScpLnZhbCgpO1xyXG5cdFx0XHRcdCAkLmFqYXgoe1xyXG5cdFx0ICAgICAgICAgICAgdXJsOiAnL2xvY2FsZS8nK2N1cnJlbnRWYWx1ZSxcclxuXHRcdCAgICAgICAgICAgIHR5cGU6ICdHRVQnXHJcblx0XHQgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xyXG5cdFx0ICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xyXG5cdFx0ICAgICAgICB9KTtcclxuXHRcdFx0fWVsc2VcclxuXHRcdFx0e1xyXG5cdFx0XHRcdF9kZHNsaWNrTG9hZGVkID0gdHJ1ZTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHQgICAgfSAgXHJcblx0fSlcclxuXHJcblx0Ly9mcm9tIHBocFxyXG5cdHNlbGVjdGVkTGFuZ3VhZ2UgPSB3aW5kb3cuTGFyYXZlbC5sYW5ndWFnZTtcclxuXHRsYW5ndWFnZUluZGV4ID0gMDtcclxuXHRzd2l0Y2goc2VsZWN0ZWRMYW5ndWFnZSl7XHJcblx0XHRjYXNlICdlbic6IGxhbmd1YWdlSW5kZXggPSAwOyBicmVhaztcclxuXHRcdGNhc2UgJ2NuJzogbGFuZ3VhZ2VJbmRleCA9IDE7IGJyZWFrO1xyXG5cdFx0ZGVmYXVsdDogbGFuZ3VhZ2VJbmRleCA9IDA7IGJyZWFrO1xyXG5cdH1cclxuXHJcblx0JCgnI2xhbmd1YWdlJykuZGRzbGljaygnc2VsZWN0Jywge2luZGV4OiBsYW5ndWFnZUluZGV4IH0pO1xyXG5cdCQoJyNsYW5ndWFnZSAuZGQtcG9pbnRlci1kb3duJykuY3NzKCdkaXNwbGF5Jywnbm9uZScpO1xyXG5cdCQoJyNsYW5ndWFnZSAuZGQtc2VsZWN0JykuY3NzKHtcclxuXHRcdGJvcmRlcjonbm9uZScsXHJcblx0XHRib3JkZXJSYWRpdXM6JzBweCdcclxuXHR9KTtcclxuXHQkKCcjbGFuZ3VhZ2UnKS5hZGRDbGFzcygncHVsbC1sZWZ0Jyk7XHJcblx0Ly9lbmQgb2YgbGFuZ3VhZ2Ugc2NyaXB0c1xyXG5cclxuICAgICQubWF0ZXJpYWwub3B0aW9ucy5hdXRvZmlsbCA9IHRydWU7XHJcblx0JC5tYXRlcmlhbC5pbml0KCk7XHJcblxyXG4gICAgdG9hc3RyLm9wdGlvbnMgPSB7XHJcblx0XHRkZWJ1ZzogZmFsc2UsXHJcblx0XHRwb3NpdGlvbkNsYXNzOiBcInRvYXN0LWJvdHRvbS1yaWdodFwiLFxyXG5cdFx0b25jbGljazogbnVsbCxcclxuXHRcdGZhZGVJbjogMzAwLFxyXG5cdFx0ZmFkZU91dDogMTAwMCxcclxuXHRcdHRpbWVPdXQ6IDUwMDAsXHJcblx0XHRleHRlbmRlZFRpbWVPdXQ6IDEwMDBcclxuXHR9O1xyXG5cclxuXHRydW5UaW1lRnVuY3Rpb25zKCk7XHJcblxyXG5cdC8vZm9yIHNlYXJjaGluZyA6IGhpZGluZyB0aGUgcmVzdWx0c1xyXG5cdCQoJ2h0bWwnKS5jbGljayhmdW5jdGlvbigpIHtcclxuXHRcdCQoJyNzZWFyY2hDb250YWluZXIgdGFibGUnKS5oaWRlKCk7XHJcblx0fSk7XHJcblxyXG5cdCQoJyNzZWFyY2hDb250YWluZXInKS5jbGljayhmdW5jdGlvbihldmVudCl7XHJcblx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHRcdCQoJyNzZWFyY2hDb250YWluZXIgdGFibGUnKS5zaG93KCk7XHJcblx0fSk7XHJcblxyXG5cclxuXHQvL2hvdmVyaW5nIHVzZXJzXHJcblx0JChkb2N1bWVudCkub24oJ21vdXNlZW50ZXInLCcucG9wb3Zlci1kZXRhaWxzJyxmdW5jdGlvbihlKXtcclxuXHRcdGNvbnRlbnRfaGVpZ2h0ID0gJCgnI3BvcG92ZXItY29udGVudCcpLmhlaWdodCgpO1xyXG5cdFx0Y29udGVudF93aWR0aCA9ICQodGhpcykud2lkdGgoKSAtIDIwO1xyXG5cdFx0dmFyIHRvcCA9ICgkKHRoaXMpLm9mZnNldCgpLnRvcCAtIHdpbmRvdy5zY3JvbGxZKSAtIGNvbnRlbnRfaGVpZ2h0O1xyXG5cdFx0dmFyIGxlZnQgPSAoJCh0aGlzKS5vZmZzZXQoKS5sZWZ0IC0gd2luZG93LnNjcm9sbFgpICsgY29udGVudF93aWR0aDtcclxuXHJcblx0XHQkKFwiI3BvcG92ZXItY29udGVudFwiKS5jc3Moe2xlZnQ6IGxlZnQgKydweCcsIHRvcDp0b3ArJ3B4J30pO1xyXG5cdFx0JChcIiNwb3BvdmVyLWNvbnRlbnRcIikuc3RvcCh0cnVlLHRydWUpLmZhZGVJbig1MDApO1xyXG5cdFx0aWQgPSAkKHRoaXMpLmRhdGEoJ2lkJyk7XHJcblx0XHQkKCcjcG9wb3Zlci1jb250ZW50JykuZGF0YSgnaWQnLGlkKTtcclxuXHJcblx0XHQkKCcjYnRuX2xvYWRpbmcnKS5zaG93KCk7XHJcblx0XHQkKCcjcG9wb3Zlci1jb250ZW50IGJ1dHRvbicpLmhpZGUoKTtcclxuXHRcdCQoJyNidG5fY29udGFpbmVyJykuaGlkZSgpO1xyXG5cclxuXHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0LmdldCgnL2ZyaWVuZHMnKVxyXG5cdFx0XHQudGhlbiggcmVzcG9uc2UgPT4ge1xyXG5cdFx0XHRcdGlmKGlkID09IExhcmF2ZWwudXNlci5pZClcclxuXHRcdFx0XHR7XHJcblx0XHRcdFx0XHQkKCcjcG9wb3Zlci1jb250ZW50JykuZmluZCgnI2J0blZpZXdQcm9maWxlJykuc2hvdygpO1xyXG5cdFx0XHRcdFx0JCgnI3BvcG92ZXItY29udGVudCcpLmZpbmQoJyNidG5BZGRGcmllbmQnKS5oaWRlKCk7XHJcblx0XHRcdFx0XHQkKCcjYnRuX2xvYWRpbmcnKS5oaWRlKCk7XHJcblx0XHRcdFx0XHQkKCcjYnRuX2NvbnRhaW5lcicpLnNob3coKTtcclxuXHRcdFx0XHR9ZWxzZVxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdCQoJyNwb3BvdmVyLWNvbnRlbnQnKS5maW5kKCcjYnRuVmlld1Byb2ZpbGUnKS5oaWRlKCk7XHJcblxyXG5cdFx0XHRcdFx0Zm91bmQgPSAwO1xyXG5cdFx0XHRcdFx0Zm9yKHZhciBpPTA7IGk8IHJlc3BvbnNlLmRhdGEuZnJpZW5kcy5sZW5ndGg7IGkrKylcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YS5mcmllbmRzW2ldLnVzZXIuaWQgPT0gaWQpXHJcblx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRmb3VuZCA9ICdmcmllbmRzJztcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0Zm9yKHZhciBpPTA7IGk8IHJlc3BvbnNlLmRhdGEucmVxdWVzdHMubGVuZ3RoOyBpKyspXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGlmKGZvdW5kICE9IDApXHJcblx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRpZihyZXNwb25zZS5kYXRhLnJlcXVlc3RzW2ldLnVzZXIuaWQgPT0gaWQpXHJcblx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRmb3VuZCA9ICdyZXF1ZXN0cyc7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGZvcih2YXIgaT0wOyBpPCByZXNwb25zZS5kYXRhLnJlc3RyaWN0ZWQubGVuZ3RoOyBpKyspXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGlmKGZvdW5kICE9IDApXHJcblx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRpZihyZXNwb25zZS5kYXRhLnJlc3RyaWN0ZWRbaV0udXNlci5pZCA9PSBpZClcclxuXHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdGZvdW5kID0gJ3Jlc3RyaWN0ZWQnO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRmb3IodmFyIGk9MDsgaTwgcmVzcG9uc2UuZGF0YS5ibG9ja2VkLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRpZihmb3VuZCAhPSAwKVxyXG5cdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0aWYocmVzcG9uc2UuZGF0YS5ibG9ja2VkW2ldLnVzZXIuaWQgPT0gaWQpXHJcblx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRmb3VuZCA9ICdibG9ja2VkJztcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdHN3aXRjaChmb3VuZClcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0Y2FzZSAnZnJpZW5kcyc6XHJcblx0XHRcdFx0XHRcdFx0Ly8gJCgnI3BvcG92ZXItY29udGVudCAjYnRuTWVzc2FnZVVzZXInKS5zaG93KCk7XHJcblx0XHRcdFx0XHRcdFx0JCgnI3BvcG92ZXItY29udGVudCAjYnRuQmxvY2tVc2VyJykuc2hvdygpO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdFx0XHRcdFx0Y2FzZSAncmVxdWVzdHMnOlxyXG5cdFx0XHRcdFx0XHRcdC8vICQoJyNwb3BvdmVyLWNvbnRlbnQgI2J0bk1lc3NhZ2VVc2VyJykuc2hvdygpO1xyXG5cdFx0XHRcdFx0XHRcdCQoJyNwb3BvdmVyLWNvbnRlbnQgI2J0bkNhbmNlbFJlcXVlc3QnKS5zaG93KCk7XHJcblx0XHRcdFx0XHRcdFx0Ly8gJCgnI3BvcG92ZXItY29udGVudCAjYnRuQmxvY2tVc2VyJykuc2hvdygpO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdFx0XHRcdFx0Y2FzZSAncmVzdHJpY3RlZCc6XHJcblx0XHRcdFx0XHRcdFx0JCgnI3BvcG92ZXItY29udGVudCAjYnRuQmxvY2tVc2VyJykuc2hvdygpO1xyXG5cdFx0XHRcdFx0XHRcdC8vICQoJyNwb3BvdmVyLWNvbnRlbnQgI2J0blJlbW92ZVJlc3RyaWN0aW9uJykuc2hvdygpO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdFx0XHRcdFx0Y2FzZSAnYmxvY2tlZCc6XHJcblx0XHRcdFx0XHRcdFx0JCgnI3BvcG92ZXItY29udGVudCAjYnRuVW5ibG9ja1VzZXInKS5zaG93KCk7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0XHRcdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFx0XHRcdCQoJyNwb3BvdmVyLWNvbnRlbnQgI2J0bkFkZEZyaWVuZCcpLnNob3coKTtcclxuXHRcdFx0XHRcdFx0XHQvLyAkKCcjcG9wb3Zlci1jb250ZW50ICNidG5NZXNzYWdlVXNlcicpLnNob3coKTtcclxuXHRcdFx0XHRcdFx0XHQkKCcjcG9wb3Zlci1jb250ZW50ICNidG5CbG9ja1VzZXInKS5zaG93KCk7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0JCgnI2J0bl9sb2FkaW5nJykuaGlkZSgpO1xyXG5cdFx0XHRcdFx0JCgnI2J0bl9jb250YWluZXInKS5zaG93KCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdFx0XHQuY2F0Y2goJC5ub29wKTtcclxuXHJcblx0fSkub24oJ21vdXNlbGVhdmUnLCcucG9wb3Zlci1kZXRhaWxzJyxmdW5jdGlvbigpe1xyXG5cdFx0aWYoICQoJyNwb3BvdmVyLWNvbnRlbnQ6aG92ZXInKS5sZW5ndGggIClcclxuXHRcdHtcclxuXHRcdFx0JCgnI3BvcG92ZXItY29udGVudCcpLnN0b3AodHJ1ZSx0cnVlKS5mYWRlSW4oNTAwKTtcclxuXHJcblx0XHR9ZWxzZVxyXG5cdFx0e1xyXG5cdFx0XHQkKCcjcG9wb3Zlci1jb250ZW50Jykuc3RvcCh0cnVlLHRydWUpLmZhZGVPdXQoNTAwKTtcclxuXHRcdH1cclxuXHR9KTtcclxuXHJcblx0JCgnYm9keScpLm9uKCdtb3VzZWxlYXZlJywnI3BvcG92ZXItY29udGVudCcsZnVuY3Rpb24oKXtcclxuXHRcdCQodGhpcykuc3RvcCh0cnVlLHRydWUpLmZhZGVPdXQoNTAwKTtcclxuXHRcdGxvYWRlciA9ICQodGhpcykuZmluZCgnLmxvYWRlcicpLmZpbHRlcignOnZpc2libGUnKTtcclxuXHRcdGNvbnRlbnQgPSBsb2FkZXIubmV4dCgpO1xyXG5cdFx0bG9hZGVyLnN0b3AodHJ1ZSx0cnVlKS5mYWRlT3V0KDEwMCxmdW5jdGlvbigpe1xyXG5cdFx0XHRjb250ZW50LnN0b3AodHJ1ZSx0cnVlKS5mYWRlSW4oMTAwKTtcclxuXHRcdH0pO1xyXG5cdH0pXHJcblxyXG5cdCQoJyNwb3BvdmVyLWNvbnRlbnQgI2J0bkFkZEZyaWVuZCcpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcblx0XHRheGlvc0FQSXYxXHJcblx0XHRcdC5wb3N0KCcvZnJpZW5kcycse1xyXG5cdFx0XHRcdHVzZXJfaWQyOiQoJyNwb3BvdmVyLWNvbnRlbnQnKS5kYXRhKCdpZCcpXHJcblx0XHRcdH0pXHJcblx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcclxuXHRcdFx0XHRpZihyZXNwb25zZS5zdGF0dXMgPT0gMjAxKVxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKCcnLCdGcmllbmQgcmVxdWVzdCBzZW50IScpO1xyXG5cdFx0XHRcdFx0JCgnI3BvcG92ZXItY29udGVudCcpLmhpZGUoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0fSlcclxuXHJcblx0JCgnI3BvcG92ZXItY29udGVudCAjYnRuTWVzc2FnZVVzZXInKS5jbGljayhmdW5jdGlvbigpe1xyXG5cdFx0Y29uc29sZS5sb2coJCgnI3BvcG92ZXItY29udGVudCcpLmRhdGEoJ2lkJykpO1xyXG5cdFx0Y29uc29sZS5sb2coJ3Nob3cgbWVzc2FnZSBwb3AgdXAnKTtcclxuXHR9KVxyXG5cclxuXHQkKCcjcG9wb3Zlci1jb250ZW50ICNidG5CbG9ja1VzZXInKS5jbGljayhmdW5jdGlvbigpe1xyXG5cdFx0YXhpb3NBUEl2MVxyXG5cdFx0XHQucG9zdCgnL2ZyaWVuZHMvYmxvY2snLHtcclxuXHRcdFx0XHR1c2VyX2lkMjogJCgnI3BvcG92ZXItY29udGVudCcpLmRhdGEoJ2lkJyksXHJcblx0XHRcdH0pXHJcblx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcclxuXHRcdFx0XHR0b2FzdHIuc3VjY2VzcygnJywnVXNlciBCbG9ja2VkIScpO1xyXG5cdFx0XHRcdCQoJyNwb3BvdmVyLWNvbnRlbnQnKS5oaWRlKCk7XHJcblx0XHRcdH0pXHJcblx0fSlcclxuXHJcblx0JCgnI3BvcG92ZXItY29udGVudCAjYnRuVW5ibG9ja1VzZXIsI3BvcG92ZXItY29udGVudCAjYnRuQ2FuY2VsUmVxdWVzdCcpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcblx0XHRheGlvc0FQSXYxXHJcblx0XHRcdC5wb3N0KCcvZnJpZW5kL2ZpbmQnLHtcclxuXHRcdFx0XHR1c2VyX2lkOkxhcmF2ZWwudXNlci5pZCxcclxuXHRcdFx0XHR1c2VyX2lkMjokKCcjcG9wb3Zlci1jb250ZW50JykuZGF0YSgnaWQnKVxyXG5cdFx0XHR9KVxyXG5cdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XHJcblx0XHRcdFx0aWYocmVzcG9uc2Uuc3RhdHVzID09IDIwMClcclxuXHRcdFx0XHR7XHJcblxyXG5cdFx0XHRcdFx0YXhpb3NBUEl2MVxyXG5cdFx0XHRcdFx0XHQuZGVsZXRlKCcvZnJpZW5kcy8nK3Jlc3BvbnNlLmRhdGEpXHJcblx0XHRcdFx0XHRcdC50aGVuKHJlc3BvbnNlID0+IHtcclxuXHRcdFx0XHRcdFx0XHRpZihyZXNwb25zZS5zdGF0dXMgPT0gMjAwKVxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdHRvYXN0ci5zdWNjZXNzKCcnLCdTdWNjZXNzJyk7XHJcblx0XHRcdFx0XHRcdFx0XHQkKCcjcG9wb3Zlci1jb250ZW50JykuaGlkZSgpO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdFx0LmNhdGNoKCQubm9vcCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdH0pXHJcblxyXG5cdC8vZW5kIG9mIGhvdmVyaW5nIHVzZXJzXHJcblxyXG5cclxuXHRcclxuXHJcblx0XHJcblxyXG59KTsvL2VvZlxyXG5cclxud2luZG93LmlzQ2hpbmVzZSA9IGZ1bmN0aW9uICgpe1xyXG5cdHJldHVybiAkKCdtZXRhW25hbWU9XCJsb2NhbGUtbGFuZ1wiXScpLmF0dHIoJ2NvbnRlbnQnKT09PSdjbic7XHJcbn1cclxuXHJcbndpbmRvdy5ydW5UaW1lRnVuY3Rpb25zID0gZnVuY3Rpb24oKXtcclxuXHJcblx0JCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoKTtcclxuXHQkKCdbZGF0YS10b2dnbGU9XCJwb3BvdmVyXCJdJykucG9wb3Zlcih7aHRtbDp0cnVlfSk7XHJcblxyXG5cdGxvYWRpbmcgPSAnPGRpdiBjbGFzcz1cImxvYWRlclwiPjxzdmcgY2xhc3M9XCJjaXJjdWxhclwiIHZpZXdCb3g9XCIyNSAyNSA1MCA1MFwiPjxjaXJjbGUgY2xhc3M9XCJwYXRoXCIgY3g9XCI1MFwiIGN5PVwiNTBcIiByPVwiMjBcIiBmaWxsPVwibm9uZVwiIHN0cm9rZS13aWR0aD1cIjdcIiBzdHJva2UtbWl0ZXJsaW1pdD1cIjEwXCIvPjwvc3ZnPjwvZGl2Pic7XHJcblx0JCgnLmJ0bi1oYXMtbG9hZGluZycpLndyYXBJbm5lciggXCI8ZGl2IGNsYXNzPSdidG4tY29udGVudCc+PC9kaXY+XCIpO1xyXG5cdCQoJy5idG4taGFzLWxvYWRpbmcnKS5wcmVwZW5kKGxvYWRpbmcpO1xyXG5cclxuICAgICQoJy5idG4taGFzLWxvYWRpbmcnKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgXHRjb250ZW50ID0gJCh0aGlzKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuICAgIFx0bG9hZGVyID0gJCh0aGlzKS5maW5kKCcubG9hZGVyJyk7XHJcbiAgICBcdGNvbnRlbnQuc3RvcCh0cnVlLHRydWUpLmZhZGVPdXQoMTAwLGZ1bmN0aW9uKCl7XHJcbiAgICBcdFx0bG9hZGVyLnN0b3AodHJ1ZSx0cnVlKS5mYWRlSW4oMTAwKTtcclxuICAgIFx0fSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCBkb2N1bWVudCApLmFqYXhTZW5kKGZ1bmN0aW9uKGEsYixjKSB7XHJcbiAgICBcdC8vIGlmKGV4ZW1wdGVkX2xpbmtzLmluZGV4T2YoYy51cmwpID09IC0xKVxyXG4gICAgICAgIC8vIHtcclxuICAgICAgICBcdC8vICQoIFwiLmxvYWRlclwiICkuc2hvdygpO1xyXG4gICAgICAgIC8vIH1cclxuICAgIH0pO1xyXG4gICAgJCggZG9jdW1lbnQgKS5hamF4Q29tcGxldGUoZnVuY3Rpb24oKSB7XHJcbiAgICAgICQoIFwiLmxvYWRlclwiICkuaGlkZSgpO1xyXG5cdFx0Y29udGVudCA9ICQoJy5idG4taGFzLWxvYWRpbmcnKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuXHRcdGxvYWRlciA9ICQoJy5idG4taGFzLWxvYWRpbmcnKS5maW5kKCcubG9hZGVyJyk7XHJcblx0XHRsb2FkZXIuZmFkZU91dCgxMDAsZnVuY3Rpb24oKXtcclxuXHRcdFx0Y29udGVudC5mYWRlSW4oMTAwKTtcclxuXHRcdH0pO1xyXG4gICAgfSk7XHJcbiAgICAkKCBkb2N1bWVudCApLmFqYXhTdG9wKGZ1bmN0aW9uKCkge1xyXG4gICAgICAvLyAkKCBcIi5sb2FkZXJcIiApLmhpZGUoKTtcclxuICAgICAgY29udGVudCA9ICQoJy5idG4taGFzLWxvYWRpbmcnKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuXHRcdGxvYWRlciA9ICQoJy5idG4taGFzLWxvYWRpbmcnKS5maW5kKCcubG9hZGVyJyk7XHJcblx0XHRsb2FkZXIuZmFkZU91dCgxMDAsZnVuY3Rpb24oKXtcclxuXHRcdFx0Y29udGVudC5mYWRlSW4oMTAwKTtcclxuXHRcdH0pO1xyXG5cclxuICAgIH0pO1xyXG4gICAgICQoIGRvY3VtZW50ICkuYWpheFN1Y2Nlc3MoZnVuY3Rpb24oKSB7XHJcbiAgICAvLyAgICQoIFwiLmxvYWRlclwiICkuaGlkZSgpO1xyXG4gICAgY29udGVudCA9ICQoJy5idG4taGFzLWxvYWRpbmcnKS5maW5kKCcuYnRuLWNvbnRlbnQnKTtcclxuXHRcdGxvYWRlciA9ICQoJy5idG4taGFzLWxvYWRpbmcnKS5maW5kKCcubG9hZGVyJyk7XHJcblx0XHRsb2FkZXIuZmFkZU91dCgxMDAsZnVuY3Rpb24oKXtcclxuXHRcdFx0Y29udGVudC5mYWRlSW4oMTAwKTtcclxuXHRcdH0pO1xyXG4gICAgfSk7XHJcbiAgICAkKCBkb2N1bWVudCApLmFqYXhFcnJvcihmdW5jdGlvbigpIHtcclxuICAgICAgLy8gJCggXCIubG9hZGVyXCIgKS5oaWRlKCk7XHJcbiAgICAgIGNvbnRlbnQgPSAkKCcuYnRuLWhhcy1sb2FkaW5nJykuZmluZCgnLmJ0bi1jb250ZW50Jyk7XHJcblx0XHRsb2FkZXIgPSAkKCcuYnRuLWhhcy1sb2FkaW5nJykuZmluZCgnLmxvYWRlcicpO1xyXG5cdFx0bG9hZGVyLmZhZGVPdXQoMTAwLGZ1bmN0aW9uKCl7XHJcblx0XHRcdGNvbnRlbnQuZmFkZUluKDEwMCk7XHJcblx0XHR9KTtcclxuICAgIH0pO1xyXG5cclxuXHJcbn1cclxuXHJcbi8qIVxyXG4gKiBqUXVlcnkgVGV4dGFyZWEgQXV0b1NpemUgcGx1Z2luXHJcbiAqIEF1dGhvcjogSmF2aWVyIEp1bGlvXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxyXG4gKi9cclxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XHJcblxyXG4gIHZhciBwbHVnaW5OYW1lID0gXCJ0ZXh0YXJlYUF1dG9TaXplXCI7XHJcbiAgdmFyIHBsdWdpbkRhdGFOYW1lID0gXCJwbHVnaW5fXCIgKyBwbHVnaW5OYW1lO1xyXG5cclxuICB2YXIgY29udGFpbnNUZXh0ID0gZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICByZXR1cm4gKHZhbHVlLnJlcGxhY2UoL1xccy9nLCAnJykubGVuZ3RoID4gMCk7XHJcbiAgfTtcclxuXHJcbiAgZnVuY3Rpb24gUGx1Z2luKGVsZW1lbnQsIG9wdGlvbnMpIHtcclxuICAgIHRoaXMuZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICB0aGlzLiRlbGVtZW50ID0gJChlbGVtZW50KTtcclxuICAgIHRoaXMuaW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgUGx1Z2luLnByb3RvdHlwZSA9IHtcclxuICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgaGVpZ2h0ID0gdGhpcy4kZWxlbWVudC5vdXRlckhlaWdodCgpO1xyXG4gICAgICB2YXIgZGlmZiA9IHBhcnNlSW50KHRoaXMuJGVsZW1lbnQuY3NzKCdwYWRkaW5nQm90dG9tJykpICtcclxuICAgICAgICAgICAgICAgICBwYXJzZUludCh0aGlzLiRlbGVtZW50LmNzcygncGFkZGluZ1RvcCcpKSB8fCAwO1xyXG5cclxuICAgICAgaWYgKGNvbnRhaW5zVGV4dCh0aGlzLmVsZW1lbnQudmFsdWUpKSB7XHJcbiAgICAgICAgdGhpcy4kZWxlbWVudC5oZWlnaHQodGhpcy5lbGVtZW50LnNjcm9sbEhlaWdodCAtIGRpZmYpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZWxlbWVudC5zY3JvbGxIZWlnaHQgLSBkaWZmKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8ga2V5dXAgaXMgcmVxdWlyZWQgZm9yIElFIHRvIHByb3Blcmx5IHJlc2V0IGhlaWdodCB3aGVuIGRlbGV0aW5nIHRleHRcclxuICAgICAgdGhpcy4kZWxlbWVudC5vbignaW5wdXQga2V5dXAnLCBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgIHZhciAkd2luZG93ID0gJCh3aW5kb3cpO1xyXG4gICAgICAgIHZhciBjdXJyZW50U2Nyb2xsUG9zaXRpb24gPSAkd2luZG93LnNjcm9sbFRvcCgpO1xyXG5cclxuICAgICAgICAkKHRoaXMpXHJcbiAgICAgICAgICAuaGVpZ2h0KDApXHJcbiAgICAgICAgICAuaGVpZ2h0KHRoaXMuc2Nyb2xsSGVpZ2h0IC0gZGlmZik7XHJcblxyXG4gICAgICAgIHZhciBpZCA9ICQodGhpcykuYXR0cignZGF0YS1jaGF0Jyk7XHJcbiAgICAgICAgdmFyIGggPSB0aGlzLnNjcm9sbEhlaWdodCAtIGRpZmY7XHJcblxyXG5cdFx0aWYoIG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5pbmRleE9mKCdmaXJlZm94JykgPiAtMSApe1xyXG5cdCAgICAgICAgaWYoaD09NDApXHJcblx0ICAgICAgICAgICQoJyMnK2lkKS5jc3MoJ2hlaWdodCcsJzI1MCcpO1xyXG5cdCAgICAgICAgZWxzZSBpZihoPT01MSlcclxuXHQgICAgICAgICAgJCgnIycraWQpLmNzcygnaGVpZ2h0JywnMjI5Jyk7XHJcblx0ICAgICAgICBlbHNlIGlmKGg9PTY5KVxyXG5cdCAgICAgICAgICAkKCcjJytpZCkuY3NzKCdoZWlnaHQnLCcyMjEnKTtcclxuXHQgICAgICAgIGVsc2UgaWYoaD09ODYpXHJcblx0ICAgICAgICAgICQoJyMnK2lkKS5jc3MoJ2hlaWdodCcsJzIwNCcpO1xyXG5cdCAgICAgICAgZWxzZSBpZihoPT0xMDMpXHJcblx0ICAgICAgICAgICQoJyMnK2lkKS5jc3MoJ2hlaWdodCcsJzE5NicpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmKCBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZignY2hyb21lJykgPiAtMSApe1xyXG5cdCAgICAgICAgaWYoaD09MzYpXHJcblx0ICAgICAgICAgICQoJyMnK2lkKS5jc3MoJ2hlaWdodCcsJzI1MCcpO1xyXG5cdCAgICAgICAgZWxzZSBpZihoPT01MSlcclxuXHQgICAgICAgICAgJCgnIycraWQpLmNzcygnaGVpZ2h0JywnMjM1Jyk7XHJcblx0ICAgICAgICBlbHNlIGlmKGg9PTY4KVxyXG5cdCAgICAgICAgICAkKCcjJytpZCkuY3NzKCdoZWlnaHQnLCcyMTgnKTtcclxuXHQgICAgICAgIGVsc2UgaWYoaD09ODUpXHJcblx0ICAgICAgICAgICQoJyMnK2lkKS5jc3MoJ2hlaWdodCcsJzIwMScpO1xyXG5cdCAgICAgICAgZWxzZSBpZihoPT0xMDIpXHJcblx0ICAgICAgICAgICQoJyMnK2lkKS5jc3MoJ2hlaWdodCcsJzE5NicpO1xyXG5cdFx0fVxyXG5cclxuICAgICAgICAkd2luZG93LnNjcm9sbFRvcChjdXJyZW50U2Nyb2xsUG9zaXRpb24pO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICAkLmZuW3BsdWdpbk5hbWVdID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcclxuICAgIHRoaXMuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgaWYgKCEkLmRhdGEodGhpcywgcGx1Z2luRGF0YU5hbWUpKSB7XHJcbiAgICAgICAgJC5kYXRhKHRoaXMsIHBsdWdpbkRhdGFOYW1lLCBuZXcgUGx1Z2luKHRoaXMsIG9wdGlvbnMpKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9O1xyXG5cclxufSkoalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50KTtcclxuXHJcbiAgICAvLyBTZXNzaW9uIChmbGFzaCkgbWVzc2FnZSAtIFRvYXN0clxyXG4gICAgaWYgKHdpbmRvdy5MYXJhdmVsLm1lc3NhZ2UpIHtcclxuICAgICAgICB0b2FzdHIuc3VjY2Vzcyh3aW5kb3cuTGFyYXZlbC5tZXNzYWdlLCBudWxsLCB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uQ2xhc3M6ICd0b2FzdC10b3AtcmlnaHQnXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG53aW5kb3cubnVtYmVyV2l0aENvbW1hcyA9IGZ1bmN0aW9uKHgpIHtcclxuICAgIHJldHVybiB4LnRvU3RyaW5nKCkucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIsXCIpO1xyXG59XHJcblxyXG5cclxuVnVlLmNvbXBvbmVudCgnZnJpZW5kcy10ZXh0JywgIHJlcXVpcmUoJy4vY29tcG9uZW50cy9jb21tb24vRnJpZW5kcy52dWUnKSk7XHJcblxyXG5cclxudmFyIGFwcEZyaWVuZHMgPSBuZXcgVnVlKHtcclxuXHRlbDogJyNmcmllbmRzLWJveCcsXHJcblxyXG5cdGRhdGE6e1xyXG5cdFx0cmVxdWVzdHM6W11cclxuXHR9LFxyXG5cclxuXHJcblx0Y3JlYXRlZCgpe1xyXG5cdFx0ZnVuY3Rpb24gZ2V0RnJpZW5kKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9mcmllbmRzJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBheGlvcy5hbGwoW1xyXG4gICAgICAgICAgICAgZ2V0RnJpZW5kKCksXHJcbiAgICAgICAgXSkudGhlbihheGlvcy5zcHJlYWQoXHJcbiAgICAgICAgICAgIChcclxuICAgICAgICAgICAgICAgICBmcmllbmRzXHJcbiAgICAgICAgICAgICkgPT4ge1xyXG4gICAgICAgICAgICBmb3IodmFyIGk9MDsgaTwgZnJpZW5kcy5kYXRhLnJlcXVlc3RzLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgICAgICAgIGlmKCFmcmllbmRzLmRhdGEucmVxdWVzdHNbaV0ucmVxdWVzdG9yKVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVxdWVzdHMucHVzaChmcmllbmRzLmRhdGEucmVxdWVzdHNbaV0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0pKVxyXG4gICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG5cdH1cclxufSlcclxuXHJcbnZhciBhcHAgPSBuZXcgVnVlKHtcclxuXHRlbDogJyNzZWFyY2hDb250YWluZXInLFxyXG5cdGNvbXBvbmVudHM6IHtcclxuXHRcdE11bHRpc2VsZWN0OiB3aW5kb3cuVnVlTXVsdGlzZWxlY3QuZGVmYXVsdFxyXG5cdH0sXHJcblx0ZGF0YTp7XHJcblx0XHRrZXl3b3JkOicnLFxyXG5cdFx0cHJvZHVjdHNfZmV0Y2hlZDpbXSxcclxuXHRcdGlzT25TZWFyY2hpbmc6ZmFsc2VcclxuXHR9LFxyXG5cdHdhdGNoOiB7XHJcblx0XHQvLyB3aGVuZXZlciBxdWVzdGlvbiBjaGFuZ2VzLCB0aGlzIGZ1bmN0aW9uIHdpbGwgcnVuXHJcblx0XHQvLyBrZXl3b3JkOiBmdW5jdGlvbiAoa2V5KSB7XHJcblx0XHQvLyBcdGlmKGtleS5sZW5ndGggPiAxKVxyXG5cdFx0Ly8gXHR7XHJcblx0XHQvLyBcdCAgXHRmdW5jdGlvbiBnZXRQcm9kdWN0cygpIHtcclxuXHRcdC8vIFx0XHRcdHJldHVybiBheGlvc0FQSXYxLnBvc3QoJy9zZWFyY2gvcHJvZHVjdCcse1xyXG5cdFx0Ly8gXHRcdFx0XHRrZXl3b3JkOmtleVxyXG5cdFx0Ly8gXHRcdFx0fSk7XHJcblx0XHQvLyBcdFx0fVxyXG5cdFx0Ly8gICAgIFx0YXhpb3MuYWxsKFtcclxuXHRcdC8vIFx0XHRcdGdldFByb2R1Y3RzKClcclxuXHRcdC8vIFx0XHRdKS50aGVuKGF4aW9zLnNwcmVhZChcclxuXHRcdC8vIFx0XHRcdChcclxuXHRcdC8vIFx0XHRcdFx0IHJlc3BvbnNlXHJcblx0XHQvLyBcdFx0XHQpID0+IHtcclxuXHJcblx0XHQvLyBcdFx0XHR0aGlzLnByb2R1Y3RzX2ZldGNoZWQgPSByZXNwb25zZS5kYXRhO1xyXG5cclxuXHRcdC8vIFx0XHR9KSlcclxuXHRcdC8vIFx0XHQuY2F0Y2goJC5ub29wKTtcclxuXHRcdC8vIFx0fVxyXG5cdFx0Ly8gfVxyXG5cdH0sXHJcblx0bWV0aG9kczp7XHJcblx0XHRuYW1lV2l0aFN0b3JlKHsgbmFtZSwgc3RvcmVfbmFtZSB9KXtcclxuXHQgICAgXHRyZXR1cm4gYCR7bmFtZX0gKCR7c3RvcmVfbmFtZX0pYDtcclxuXHQgICAgfSxcclxuXHRcdGxpbWl0VGV4dCAoY291bnQpIHtcclxuXHQgICAgICByZXR1cm4gYGFuZCAke2NvdW50fSBvdGhlciByZXN1bHRzYFxyXG5cdCAgICB9LFxyXG5cdCAgICBnb3RvcGFnZShzbHVnKXtcclxuXHRcdFx0d2luZG93LmxvY2F0aW9uID0gJy9wcm9kdWN0P3NlYXJjaD0nK3NsdWc7XHJcblx0ICAgIH0sXHJcblx0ICAgIGhhbmRsZUVudGVyOiBmdW5jdGlvbiAoZSkge1xyXG5cdFx0ICAgIGlmIChlLmtleUNvZGUgPT0gMTMpIHtcclxuXHRcdFx0XHR3aW5kb3cubG9jYXRpb24gPSAnL3Byb2R1Y3Q/c2VhcmNoPScrdGhpcy5rZXl3b3JkO1xyXG5cdFx0ICAgIH1cclxuXHRcdH1cclxuXHR9XHJcbn0pXHJcblxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcblx0ZWw6ICcjZmF2b3JpdGVzJyxcclxuXHRkYXRhOiB7XHJcblx0XHRpdGVtOltdXHJcblx0fSxcclxuXHRjcmVhdGVkKCl7XHJcblx0XHRmdW5jdGlvbiBnZXRmYXZvcml0ZXN0b3RhbCgpIHtcclxuXHRcdFx0aWYoTGFyYXZlbC51c2VyKVxyXG5cdFx0XHRyZXR1cm4gYXhpb3NBUEl2MS5nZXQoJy9nZXRmYXZvcml0ZXN0b3RhbCcpO1xyXG5cdFx0fVxyXG5cdFx0YXhpb3MuYWxsKFtcclxuXHRcdFx0Z2V0ZmF2b3JpdGVzdG90YWwoKVxyXG5cdFx0XSkudGhlbihheGlvcy5zcHJlYWQoXHJcblx0XHRcdChcclxuXHRcdFx0XHQgY291bnRcclxuXHRcdFx0KSA9PiB7XHJcblx0XHRcdFx0dGhpcy5pdGVtID0gY291bnQ7XHJcblx0XHR9KSlcclxuXHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cdH1cclxufSk7XHJcblxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcblx0ZWw6ICcjY29tcGFyZScsXHJcblx0ZGF0YToge1xyXG5cdFx0aXRlbTpbXVxyXG5cdH0sXHJcblx0Y3JlYXRlZCgpe1xyXG5cdFx0ZnVuY3Rpb24gZ2V0Y29tcGFyZXRvdGFsKCkge1xyXG5cdFx0XHRpZihMYXJhdmVsLnVzZXIpXHJcblx0XHRcdHJldHVybiBheGlvc0FQSXYxLmdldCgnL2dldGNvbXBhcmV0b3RhbCcpO1xyXG5cdFx0fVxyXG5cdFx0YXhpb3MuYWxsKFtcclxuXHRcdFx0Z2V0Y29tcGFyZXRvdGFsKClcclxuXHRcdF0pLnRoZW4oYXhpb3Muc3ByZWFkKFxyXG5cdFx0XHQoXHJcblx0XHRcdFx0IGNvdW50XHJcblx0XHRcdCkgPT4ge1xyXG5cdFx0XHRcdC8vdGhpcy5pdGVtID0gKGNvdW50ID4gMCkgPyBjb3VudCA6ICcnO1xyXG5cdFx0XHRcdHRoaXMuaXRlbSA9IGNvdW50O1xyXG5cdFx0fSkpXHJcblx0XHQuY2F0Y2goJC5ub29wKTtcclxuXHR9XHJcbn0pO1xyXG5cclxuXHJcbnZhciBhcHAgPSBuZXcgVnVlKHtcclxuXHRlbDogJyNjYXJ0JyxcclxuXHRkYXRhOiB7XHJcblx0XHRpdGVtczpbXSxcclxuXHRcdGNvdW50OicnLFxyXG5cdFx0aXRlbV9uYW1lczonJ1xyXG5cdH0sXHJcblx0Y3JlYXRlZCgpe1xyXG5cdFx0ZnVuY3Rpb24gZ2V0Q2FydCgpIHtcclxuXHRcdFx0cmV0dXJuIGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvdmlldycpO1xyXG5cdFx0fVxyXG5cdFx0YXhpb3MuYWxsKFtcclxuXHRcdFx0Z2V0Q2FydCgpXHJcblx0XHRdKS50aGVuKGF4aW9zLnNwcmVhZChcclxuXHRcdFx0KFxyXG5cdFx0XHRcdCBjYXJ0XHJcblx0XHRcdCkgPT4ge1xyXG5cdFx0XHRcdHRoaXMuY291bnQgPSBjYXJ0LmRhdGEuY2FydF9pdGVtX2NvdW50O1xyXG5cdFx0XHRcdHZhciBjYXJ0ID0gJC5tYXAoY2FydC5kYXRhLmNhcnQsIGZ1bmN0aW9uKHZhbHVlLCBpbmRleCkge1xyXG5cdFx0XHRcdCAgICByZXR1cm4gW3ZhbHVlXTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHR0aGlzLml0ZW1zID0gY2FydDtcclxuXHJcblx0XHR9KSlcclxuXHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cdH0sXHJcblx0bWV0aG9kczp7XHJcblx0XHRyZXNmcmVzaEl0ZW1zKCl7XHJcblx0XHRcdGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvdmlldycpXHJcblx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0dmFyIGNhcnQgPSAkLm1hcChyZXN1bHQuZGF0YS5jYXJ0LCBmdW5jdGlvbih2YWx1ZSwgaW5kZXgpIHtcclxuXHRcdFx0XHQgICAgcmV0dXJuIFt2YWx1ZV07XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0dGhpcy5pdGVtcyA9IGNhcnQ7XHJcblx0ICAgICAgICB9KTtcclxuXHRcdH0sXHJcblx0XHRyZW1vdmVJdGVtcyhpZCl7XHJcblx0XHRcdGF4aW9zLmdldChsb2NhdGlvbi5vcmlnaW4gKyAnL2NhcnQvZGVsZXRlJyx7XHJcblx0XHRcdCAgICBwYXJhbXM6IHtcclxuXHRcdFx0ICAgIFx0cm93X2lkOiB0aGlzLml0ZW1zW2lkXS5yb3dJZFxyXG5cdFx0XHQgICAgfVxyXG5cdFx0XHR9KVxyXG5cdFx0XHQudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdCQoXCIjY2FydC1jb3VudCwgI2NhcnQtY291bnQtbW9iaWxlXCIpLnRleHQocmVzdWx0LmRhdGEuY2FydF9pdGVtX2NvdW50KTtcclxuXHRcdFx0fSk7XHJcblx0XHRcdHRvYXN0ci5zdWNjZXNzKCdSZW1vdmVkIGZyb20gU2hvcHBpbmcgQ2FydCEnLCB0aGlzLml0ZW1zW2lkXS5uYW1lKTtcclxuXHRcdFx0dGhpcy5pdGVtcy5zcGxpY2UoaWQsMSk7XHJcblx0XHR9XHJcblxyXG5cdH0sXHJcblx0Y29tcHV0ZWQ6e1xyXG5cdFx0dG90YWxfcHJpY2UoKXtcclxuXHRcdFx0c3VtID0gMDtcclxuXHRcdFx0Zm9yKGk9MDsgaTx0aGlzLml0ZW1zLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHR7XHRcclxuXHRcdFx0XHR2YXIgc2FsZVByaWNlID0gdGhpcy5pdGVtc1tpXS5vcHRpb25zLnNhbGVfcHJpY2U7XHJcblx0XHRcdFx0c3VtICs9ICBzYWxlUHJpY2UucmVwbGFjZSgnLCcsICcnKSAqIHRoaXMuaXRlbXNbaV0ucXR5O1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiBzdW07XHJcblx0XHR9XHJcblx0fVxyXG59KTtcclxuXHJcblxyXG52YXIgYXBwID0gbmV3IFZ1ZSh7XHJcblx0ZWw6ICcjY2FydC1tb2JpbGUnLFxyXG5cdGRhdGE6IHtcclxuXHRcdGl0ZW1zOltdLFxyXG5cdFx0Y291bnQ6JydcclxuXHR9LFxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdGZ1bmN0aW9uIGdldENhcnQoKSB7XHJcblx0XHRcdHJldHVybiBheGlvcy5nZXQobG9jYXRpb24ub3JpZ2luICsgJy9jYXJ0L3ZpZXcnKTtcclxuXHRcdH1cclxuXHRcdGF4aW9zLmFsbChbXHJcblx0XHRcdGdldENhcnQoKVxyXG5cdFx0XSkudGhlbihheGlvcy5zcHJlYWQoXHJcblx0XHRcdChcclxuXHRcdFx0XHQgY2FydFxyXG5cdFx0XHQpID0+IHtcclxuXHRcdFx0XHR0aGlzLmNvdW50ID0gY2FydC5kYXRhLmNhcnRfaXRlbV9jb3VudDtcclxuXHRcdFx0XHR2YXIgY2FydCA9ICQubWFwKGNhcnQuZGF0YS5jYXJ0LCBmdW5jdGlvbih2YWx1ZSwgaW5kZXgpIHtcclxuXHRcdFx0XHQgICAgcmV0dXJuIFt2YWx1ZV07XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0dGhpcy5pdGVtcyA9IGNhcnQ7XHJcblx0XHR9KSlcclxuXHRcdC5jYXRjaCgkLm5vb3ApO1xyXG5cdH1cclxufSk7XHJcblxyXG5cclxuJChcIiNkcm9wZG93blwiKS5tb3VzZWxlYXZlKGZ1bmN0aW9uKCkge1xyXG4gICAgJChcIiNkcm9wZG93bi1jYXJ0XCIpLmNsaWNrKCk7XHJcbn0pO1xyXG5cclxuLy8gJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpIHtcclxuLy8gXHRpZiAoJCh3aW5kb3cpLnNjcm9sbFRvcCgpICsgJCh3aW5kb3cpLmhlaWdodCgpID09ICQoZG9jdW1lbnQpLmhlaWdodCgpKSB7XHJcbi8vIFx0XHRFdmVudC5maXJlKCdzY3JvbGxlZC10by1ib3R0b20nKTtcclxuLy8gXHR9XHJcbi8vIH0pO1xyXG5cclxudmFyIGRvY3NDaGVja0FwcCA9IG5ldyBWdWUoe1xyXG5cdGVsOiAnI2RvY3NDaGVja0FwcCcsXHJcblxyXG5cdGRhdGE6e1xyXG5cdFx0c2FtcGxlOjEsXHJcblx0XHRzYW1wbGUyOjIsXHJcblx0fSxcclxuXHJcblx0bWV0aG9kczp7XHJcblx0XHRzaG93RXhwaXJpbmdNZXNzYWdlKGRvY3R5cGUsIGV4cGlyeURhdGUpe1xyXG5cdCAgICAgIC8vc2hvdyB3YXJuaW5nIG1lc3NhZ2Ugd2hlbiB0aGUgZXhwaXJ5IGRhdGUgaXMgd2l0aGluIDMwIGRheXNcclxuXHQgICAgICB2YXIgZXhwaXJ5X2RhdGUgPSBtb21lbnQoZXhwaXJ5RGF0ZSk7XHJcblxyXG5cdCAgICAgIGlmKCBwYXJzZUludChleHBpcnlfZGF0ZS5kaWZmKG1vbWVudCgpLCAnZGF5cycpKSA8PSAzMCApXHJcblx0ICAgICAge1xyXG5cdCAgICAgIFx0aWYocGFyc2VJbnQoZXhwaXJ5X2RhdGUuZGlmZihtb21lbnQoKSwgJ2RheXMnKSkgPiAwKXtcclxuXHRcdCAgICAgICAgdG9hc3RyLndhcm5pbmcoJ2luICcrIGV4cGlyeV9kYXRlLmRpZmYobW9tZW50KCksICdkYXlzJykgKycgZGF5cy4nICwgZG9jdHlwZSsnIGV4cGlyaW5nLi4nLCB7XHJcblx0XHQgICAgICAgICAgdGltZU91dDogMCxcclxuXHRcdCAgICAgICAgICBleHRlbmRlZFRpbWVPdXQ6IDAsXHJcblx0XHQgICAgICAgICAgb25jbGljazogZnVuY3Rpb24gKCkgeyB3aW5kb3cubG9jYXRpb24gPSAnL3Byb2ZpbGU/b3Blbj1kb2N1bWVudHMnIH1cclxuXHRcdCAgICAgICAgfSk7XHJcblx0ICAgICAgXHR9XHJcblx0ICAgICAgXHRpZihwYXJzZUludChleHBpcnlfZGF0ZS5kaWZmKG1vbWVudCgpLCAnZGF5cycpKSA8IDApe1xyXG5cdCAgICAgIFx0XHR0b2FzdHIud2FybmluZygnJyAsIGRvY3R5cGUrJyB3YXMgZXhwaXJlZCwgY2xpY2sgaGVyZSB0byB1cGxvYWQgbmV3IGRvY3VtZW50cycsIHtcclxuXHRcdCAgICAgICAgICB0aW1lT3V0OiAwLFxyXG5cdFx0ICAgICAgICAgIGV4dGVuZGVkVGltZU91dDogMCxcclxuXHRcdCAgICAgICAgICBvbmNsaWNrOiBmdW5jdGlvbiAoKSB7IHdpbmRvdy5sb2NhdGlvbiA9ICcvcHJvZmlsZT9vcGVuPWRvY3VtZW50cycgfVxyXG5cdFx0ICAgICAgICB9KTtcclxuXHQgICAgICBcdH1cclxuXHQgICAgICB9XHJcblx0ICAgIH0sXHJcblx0fSxcclxuXHJcblxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdGlmKExhcmF2ZWwudXNlcil7XHJcblx0XHRheGlvc0FQSXYxLmdldCgnL3VzZXJzLycrTGFyYXZlbC51c2VyLmlkKycvZG9jdW1lbnRzJylcclxuXHQgICAgICAgIC50aGVuKGRvY3NPYmogPT4ge1xyXG5cdCAgICAgICAgICBpZihkb2NzT2JqWydzdGF0dXMnXSA9PSAyMDApXHJcblx0ICAgICAgICAgIHtcclxuXHRcdCAgICAgICAgICAgIG5iaV9vYmogICAgICAgPSBkb2NzT2JqLmRhdGEuZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoudHlwZSA9PT0gJ25iaScgfSlcclxuXHRcdCAgICAgICAgICAgIGJpcnRoQ2VydF9vYmogPSBkb2NzT2JqLmRhdGEuZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoudHlwZSA9PT0gJ2JpcnRoQ2VydCcgfSlcclxuXHRcdCAgICAgICAgICAgIGdvdklEX29iaiAgICAgPSBkb2NzT2JqLmRhdGEuZmlsdGVyKG9iaiA9PiB7IHJldHVybiBvYmoudHlwZSA9PT0gJ2dvdklkJyB9KVxyXG5cdFx0ICAgICAgICAgICAgc2VjX29iaiAgICAgICA9IGRvY3NPYmouZGF0YS5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAnc2VjJyB9KVxyXG5cdFx0ICAgICAgICAgICAgYmlyX29iaiAgICAgICA9IGRvY3NPYmouZGF0YS5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAnYmlyJyB9KVxyXG5cdFx0ICAgICAgICAgICAgcGVybWl0X29iaiAgICA9IGRvY3NPYmouZGF0YS5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai50eXBlID09PSAncGVybWl0JyB9KVxyXG5cclxuXHRcdCAgICAgICAgICAgIGlmKG5iaV9vYmoubGVuZ3RoKVxyXG5cdFx0ICAgICAgICAgICAge1xyXG5cdFx0ICAgICAgICAgICAgXHRpZihuYmlfb2JqWzBdLnN0YXR1cyA9PSAnVmVyaWZpZWQnIHx8IG5iaV9vYmpbMF0uc3RhdHVzID09ICdFeHBpcmVkJylcclxuXHRcdCAgICAgICAgICAgIFx0e1xyXG5cdFx0XHQgICAgICAgICAgICAgICAgdGhpcy5zaG93RXhwaXJpbmdNZXNzYWdlKCdOQkknLCBuYmlfb2JqWzBdLmV4cGlyZXNfYXQpO1xyXG5cdFx0ICAgICAgICAgICAgXHR9XHJcblx0XHQgICAgICAgICAgICB9XHJcblxyXG5cdFx0ICAgICAgICAgICAgaWYoYmlydGhDZXJ0X29iai5sZW5ndGgpXHJcblx0XHQgICAgICAgICAgICB7XHJcblx0XHQgICAgICAgICAgICBcdGlmKGJpcnRoQ2VydF9vYmpbMF0uc3RhdHVzID09ICdWZXJpZmllZCcpXHJcblx0XHQgICAgICAgICAgICBcdHtcclxuXHRcdFx0ICAgICAgICAgICAgICAgIHRoaXMuc2hvd0V4cGlyaW5nTWVzc2FnZSgnQklSVEggQ0VSVElGSUNBVEUnLCBiaXJ0aENlcnRfb2JqWzBdLmV4cGlyZXNfYXQpO1xyXG5cdFx0ICAgICAgICAgICAgXHR9XHJcblx0XHQgICAgICAgICAgICB9XHJcblxyXG5cdFx0ICAgICAgICAgICAgaWYoZ292SURfb2JqLmxlbmd0aClcclxuXHRcdCAgICAgICAgICAgIHtcclxuXHRcdCAgICAgICAgICAgIFx0aWYoZ292SURfb2JqWzBdLnN0YXR1cyA9PSAnVmVyaWZpZWQnKVxyXG5cdFx0ICAgICAgICAgICAgXHR7XHJcblx0XHRcdCAgICAgICAgICAgICAgICB0aGlzLnNob3dFeHBpcmluZ01lc3NhZ2UoJ0dPVkVSTk1FTlQgSUQnLCBnb3ZJRF9vYmpbMF0uZXhwaXJlc19hdCk7XHJcblx0XHQgICAgICAgICAgICBcdH1cclxuXHRcdCAgICAgICAgICAgIH1cclxuXHJcblx0XHQgICAgICAgICAgICBpZihzZWNfb2JqLmxlbmd0aClcclxuXHRcdCAgICAgICAgICAgIHtcclxuXHRcdCAgICAgICAgICAgIFx0aWYoc2VjX29ialswXS5zdGF0dXMgPT0gJ1ZlcmlmaWVkJylcclxuXHRcdCAgICAgICAgICAgIFx0e1xyXG5cdFx0XHQgICAgICAgICAgICAgICAgdGhpcy5zaG93RXhwaXJpbmdNZXNzYWdlKCdTRUMnLCBzZWNfb2JqWzBdLmV4cGlyZXNfYXQpO1xyXG5cdFx0ICAgICAgICAgICAgXHR9XHJcblx0XHQgICAgICAgICAgICB9XHJcblxyXG5cdFx0ICAgICAgICAgICAgaWYoYmlyX29iai5sZW5ndGgpXHJcblx0XHQgICAgICAgICAgICB7XHJcblx0XHQgICAgICAgICAgICBcdGlmKGJpcl9vYmpbMF0uc3RhdHVzID09ICdWZXJpZmllZCcpXHJcblx0XHQgICAgICAgICAgICBcdHtcclxuXHRcdFx0ICAgICAgICAgICAgICAgIHRoaXMuc2hvd0V4cGlyaW5nTWVzc2FnZSgnQklSJywgYmlyX29ialswXS5leHBpcmVzX2F0KTtcclxuXHRcdCAgICAgICAgICAgIFx0fVxyXG5cdFx0ICAgICAgICAgICAgfVxyXG5cclxuXHRcdCAgICAgICAgICAgIGlmKHBlcm1pdF9vYmoubGVuZ3RoKVxyXG5cdFx0ICAgICAgICAgICAge1xyXG5cdFx0ICAgICAgICAgICAgXHRpZihwZXJtaXRfb2JqWzBdLnN0YXR1cyA9PSAnVmVyaWZpZWQnKVxyXG5cdFx0ICAgICAgICAgICAgXHR7XHJcblx0XHRcdCAgICAgICAgICAgICAgICB0aGlzLnNob3dFeHBpcmluZ01lc3NhZ2UoJ1BFUk1JVCcsIHBlcm1pdF9vYmpbMF0uZXhwaXJlc19hdCk7XHJcblx0XHQgICAgICAgICAgICBcdH1cclxuXHRcdCAgICAgICAgICAgIH1cclxuXHJcblx0ICAgICAgICAgIH1cclxuXHJcblx0ICAgICAgICB9KVxyXG5cdCAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuXHQgICAgICAgICAgLy8gdG9hc3RyLmVycm9yKCdVcGRhdGVkIEZhaWxlZC4nKTtcclxuXHQgICAgICB9KTsgICBcdFx0XHRcclxuXHRcdH1cclxuXHJcblx0fVxyXG5cclxufSlcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2dyYW5kLXBhcmVudC1jYXRlZ29yeScsICByZXF1aXJlKCcuL2NvbXBvbmVudHMvY29tbW9uL0NhdGVnb3J5SGVhZGVyLnZ1ZScpKTtcclxuXHJcbnZhciBhcHAgPSBuZXcgVnVlKHtcclxuICBlbCA6ICcjbWFpbl9tZW51JyxcclxuICBcclxuICBjcmVhdGVkKCl7XHJcbiAgICBheGlvc0FQSXYxXHJcbiAgICBcdC5nZXQoJy9nZXRjYXRlZ29yaWVzJylcclxuICAgICAgXHQudGhlbihyZXNwb25zZSA9PiB7XHJcblx0ICAgICAgICB0aGlzLmNhdGVnb3JpZXMgPSByZXNwb25zZS5kYXRhO1xyXG5cdFx0fSlcclxuICB9LFxyXG5cclxuICBkYXRhOntcclxuICAgIGNhdGVnb3JpZXM6W11cclxuICB9LFxyXG59KTtcclxuXHJcbnZhciBhcHAgPSBuZXcgVnVlKHtcclxuXHRlbDogJyNkcm9wZG93blVzZXJNZW51JyxcclxuXHRkYXRhOiB7XHJcblx0XHRld2FsbGV0X2JhbGFuY2U6MCxcclxuXHRcdHVzYWJsZV9iYWxhbmNlOjAsXHJcblx0XHRjbnRfbXNnOicnLFxyXG5cdFx0Y250X2ZyOicnLFxyXG5cdFx0Y250X25vdDonJyxcclxuXHJcblx0fSxcclxuXHJcblx0bWV0aG9kczp7XHJcblx0XHRnZXRMYXRlc3RFd2FsbGV0QmFsYW5jZSgpe1xyXG5cdFx0XHRpZihMYXJhdmVsLnVzZXIpe1xyXG5cdFx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHRcdC5nZXQoJy91c2VyLycrTGFyYXZlbC51c2VyLmlkKycvZXdhbGxldCcpXHJcblx0XHRcdFx0XHQudGhlbihyZXN1bHQgPT4geyAgdGhpcy5ld2FsbGV0X2JhbGFuY2UgPSBwYXJzZUZsb2F0KHJlc3VsdC5kYXRhLmRhdGEuYW1vdW50KX0pO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmKExhcmF2ZWwudXNlcil7XHJcblx0XHRcdFx0YXhpb3NBUEl2MVxyXG5cdFx0XHRcdFx0LmdldCgnL2dldFVzYWJsZScpXHJcblx0XHRcdFx0XHQudGhlbihyZXN1bHQgPT4geyAgdGhpcy51c2FibGVfYmFsYW5jZSA9IHJlc3VsdC5kYXRhLnN1Y2Nlc3M/IHBhcnNlRmxvYXQocmVzdWx0LmRhdGEuZGF0YS51c2FibGUpOjB9KTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcdGdldFVzYWJsZUJhbGFuY2UoKSB7XHJcblx0XHRcdGlmKExhcmF2ZWwudXNlcil7XHJcblx0XHRcdFx0YXhpb3NBUEl2MVxyXG5cdFx0XHRcdFx0LmdldCgnL2dldFVzYWJsZScpXHJcblx0XHRcdFx0XHQudGhlbihyZXN1bHQgPT4geyAgdGhpcy51c2FibGVfYmFsYW5jZSA9IHJlc3VsdC5kYXRhLnN1Y2Nlc3M/IHBhcnNlRmxvYXQocmVzdWx0LmRhdGEuZGF0YS51c2FibGUpOjB9KTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHRcdGdldE1lc3NhZ2VzKCkge1xyXG5cdFx0XHRpZihMYXJhdmVsLnVzZXIpe1xyXG5cdFx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHRcdC5nZXQoJy9tZXNzYWdlcy9ub3RpZmljYXRpb25zMicpXHJcblx0XHRcdFx0XHQudGhlbihyZXN1bHQgPT4ge1xyXG5cdFx0XHRcdFx0XHR0aGlzLmNudF9tc2cgPSByZXN1bHQuZGF0YTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHR9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXRGcmllbmRzKCkge1xyXG5cdFx0XHRpZihMYXJhdmVsLnVzZXIpe1xyXG5cdFx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHRcdC5nZXQoJy9mcmllbmRzL25vdGlmaWNhdGlvbnMnKVxyXG5cdFx0XHRcdFx0LnRoZW4ocmVzdWx0ID0+IHtcclxuXHRcdFx0XHRcdFx0dGhpcy5jbnRfZnIgPSByZXN1bHQuZGF0YTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHR9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXROb3RpKCkge1xyXG5cdFx0XHRpZihMYXJhdmVsLnVzZXIpe1xyXG5cdFx0XHRcdGF4aW9zQVBJdjFcclxuXHRcdFx0XHRcdC5nZXQoJy9ub3RpZmljYXRpb25zJylcclxuXHRcdFx0XHRcdC50aGVuKHJlc3VsdCA9PiB7XHJcblx0XHRcdFx0XHRcdHRoaXMuY250X25vdCA9IHJlc3VsdC5kYXRhO1xyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuICAgICAgICB9LFxyXG5cdH0sXHJcblx0Y29tcHV0ZWQ6IHtcclxuICAgICAgICBtc2djb3VudCgpIHtcclxuICAgICAgICAgICAgbGV0IGNvdW50ID0gMDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNudF9tc2cpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY250X21zZy5mb3JFYWNoKGZ1bmN0aW9uIChub3RpZikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKG5vdGlmLnVzZXJfaWQgIT0gd2luZG93LkxhcmF2ZWwudXNlci5pZCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghIG5vdGlmLnNlZW5fYXQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICsrY291bnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGNvdW50IDwgMTAwKXtcclxuICAgICAgICAgICAgXHQgcmV0dXJuIGNvdW50O1xyXG5cdFx0XHR9ZWxzZSBpZihjb3VudCA+PSAxMDApe1xyXG5cdFx0XHRcdHJldHVybiAnOTkrJztcclxuXHRcdFx0fVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZnJjb3VudCgpIHtcclxuICAgICAgICBcdGlmICh0aGlzLmNudF9mci5ub3RzZWVuX2NvdW50IDwgMTAwKXtcclxuICAgICAgICAgICAgXHRyZXR1cm4gdGhpcy5jbnRfZnIubm90c2Vlbl9jb3VudDtcclxuXHRcdFx0fWVsc2UgaWYodGhpcy5jbnRfZnIubm90c2Vlbl9jb3VudCA+PSAxMDApe1xyXG5cdFx0XHRcdHJldHVybiAnOTkrJztcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuICAgICAgICB9LFxyXG4gICAgICAgIG5vdGljb3VudCgpIHtcclxuICAgICAgICAgICAgbGV0IGNvdW50ID0gMDtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNudF9ub3QpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY250X25vdC5mb3JFYWNoKGZ1bmN0aW9uIChub3RpZikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghIG5vdGlmLnNlZW5fYXQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgKytjb3VudDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGNvdW50IDwgMTAwKXtcclxuICAgICAgICAgICAgXHQgcmV0dXJuIGNvdW50O1xyXG5cdFx0XHR9ZWxzZSBpZihjb3VudCA+PSAxMDApe1xyXG5cdFx0XHRcdHJldHVybiAnOTkrJztcclxuXHRcdFx0fVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG5cdGNyZWF0ZWQoKXtcclxuXHRcdHRoaXMuZ2V0TGF0ZXN0RXdhbGxldEJhbGFuY2UoKTtcclxuXHRcdHRoaXMuZ2V0VXNhYmxlQmFsYW5jZSgpO1xyXG5cdFx0dGhpcy5nZXRNZXNzYWdlcygpO1xyXG5cdFx0dGhpcy5nZXRGcmllbmRzKCk7XHJcblx0XHR0aGlzLmdldE5vdGkoKTtcclxuXHR9LFxyXG5cclxufSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9tYWluLmpzIiwiXHJcbjx0ZW1wbGF0ZT5cclxuICAgIDxsaSBjbGFzcz1cImRyb3Bkb3duIG1lZ2EtbWVudVwiPlxyXG4gICAgICAgIDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJkcm9wZG93bi10b2dnbGVcIiBkYXRhLXRvZ2dsZT1cImRyb3Bkb3duXCIgcm9sZT1cImJ1dHRvblwiIGFyaWEtaGFzcG9wdXA9XCJ0cnVlXCIgYXJpYS1leHBhbmRlZD1cImZhbHNlXCI+XHJcbiAgICAgICAgICAgIHt7Z2V0VGl0bGV9fSA8c3BhbiBjbGFzcz1cImNhcmV0XCI+PC9zcGFuPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgICA8dWwgY2xhc3M9XCJkcm9wZG93bi1tZW51IG1lZ2EtZHJvcGRvd24tbWVudSByb3dcIj5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwiY29sLXNtLTRcIiBzdHlsZT1cInRleHQtYWxpZ246Y2VudGVyOyBwYWRkaW5nLWxlZnQ6NDBweDsgcGFkZGluZy1yaWdodDo0MHB4O1wiPlxyXG4gICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgdi1pZj1cInByb2R1Y3RzLmxlbmd0aFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJkcm9wZG93bi1oZWFkZXJcIj57eyBobGFuZ1snbmV3LXByb2R1Y3RzJ10gfX08L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPVwibXlDYXJvdXNlbFwiIGNsYXNzPVwiY2Fyb3VzZWwgc2xpZGVcIiBkYXRhLXJpZGU9XCJjYXJvdXNlbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNhcm91c2VsLWlubmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIml0ZW1cIiA6Y2xhc3M9XCJrZXk9PTA/J2FjdGl2ZSc6JydcIiB2LWZvcj0ncHJvZHVjdCxrZXkgaW4gcHJvZHVjdHMnIHYtaWY9J2tleSA8IDMnIDprZXk9J3Byb2R1Y3QuaWQnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSA6aHJlZj1cIidwcm9kdWN0LycrcHJvZHVjdC5zbHVnXCIgY2xhc3M9J2Jsb2NrLWNlbnRlcic+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIDpzcmM9XCJwcm9kdWN0Lm1haW5faW1hZ2VfdGh1bWJuYWlsXCIgY2xhc3M9XCJibG9jay1jZW50ZXJcIiBzdHlsZT1cImhlaWdodDogMTUwcHg7bWFyZ2luLWxlZnQ6IGF1dG87bWFyZ2luLXJpZ2h0OiBhdXRvO1wiIDphbHQ9XCJwcm9kdWN0LnNsdWdcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgdi1pZj1cImxhbmc9PSdjbidcIiBjbGFzcz1cImhlYWRlci1jYXRlZ29yeS1wcm9kdWN0LXNsaWRlci1uYW1lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c21hbGwgdi1pZj1cInByb2R1Y3QuY25fbmFtZVwiPnt7cHJvZHVjdC5jbl9uYW1lfX08L3NtYWxsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNtYWxsIHYtZWxzZT57e3Byb2R1Y3QubmFtZX19PC9zbWFsbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9oND5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IHYtZWxzZSBjbGFzcz1cImhlYWRlci1jYXRlZ29yeS1wcm9kdWN0LXNsaWRlci1uYW1lXCI+e3twcm9kdWN0Lm5hbWV9fTwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIHYtaWY9XCJwcm9kdWN0LnNhbGVfcHJpY2UhPW51bGxcIiBjbGFzcz1cInRleHQtcHJpbWFyeVwiID48c3BhbiB2LWlmPVwicHJvZHVjdC5mZWVfaW5jbHVkZWQ9PTFcIj57eyBwYXJzZUZsb2F0KHByb2R1Y3Quc2FsZV9wcmljZSkrcGFyc2VGbG9hdChwcm9kdWN0LnNoaXBwaW5nX2ZlZSkrcGFyc2VGbG9hdChwcm9kdWN0LmNoYXJnZSkrcGFyc2VGbG9hdChwcm9kdWN0LnZhdCkgfCBjdXJyZW5jeX19PC9zcGFuPjxzcGFuIHYtZWxzZT57eyBwYXJzZUZsb2F0KHByb2R1Y3Quc2FsZV9wcmljZSkgfCBjdXJyZW5jeX19PC9zcGFuPjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gdi1lbHNlIGNsYXNzPVwidGV4dC1wcmltYXJ5XCIgPjxzcGFuIHYtaWY9XCJwcm9kdWN0LmZlZV9pbmNsdWRlZD09MVwiPnt7IHBhcnNlRmxvYXQocHJvZHVjdC5wcmljZSkrcGFyc2VGbG9hdChwcm9kdWN0LnNoaXBwaW5nX2ZlZSkrcGFyc2VGbG9hdChwcm9kdWN0LmNoYXJnZSkrcGFyc2VGbG9hdChwcm9kdWN0LnZhdCkgfCBjdXJyZW5jeX19PC9zcGFuPjxzcGFuIHYtZWxzZT57eyBwYXJzZUZsb2F0KHByb2R1Y3QucHJpY2UpIHwgY3VycmVuY3l9fTwvc3Bhbj48L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gQGNsaWNrPSdhZGRUb0Zhdm9yaXRlcyhwcm9kdWN0LmlkLHByb2R1Y3QubmFtZSknIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0XCIgdHlwZT1cImJ1dHRvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLWhlYXJ0XCI+PC9zcGFuPiB7eyBjbGFuZ1snYWRkLXRvLXdpc2hsaXN0J10gfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8IS0tIEVuZCBJdGVtIC0tPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8IS0tIEVuZCBDYXJvdXNlbCBJbm5lciAtLT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwhLS0gLy5jYXJvdXNlbCAtLT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwiZGl2aWRlclwiPjwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPjxhIGhyZWY9XCIvY2F0ZWdvcmllc1wiIGNsYXNzPVwiYnRuIGJ0bi1yYWlzZWQgYnRuLXByaW1hcnlcIj4ge3sgaGxhbmdbJ3ZpZXctYWxsLWNhdCddIH19IDxzcGFuIGNsYXNzPVwiZ2x5cGhpY29uIGdseXBoaWNvbi1jaGV2cm9uLXJpZ2h0IHB1bGwtcmlnaHRcIj48L3NwYW4+PC9hPjwvbGk+XHJcbiAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICA8bGkgY2xhc3M9XCJjb2wtc20tMlwiPlxyXG4gICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwibGlzdC11bnN0eWxlZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cImRyb3Bkb3duLWhlYWRlciBncmFuZHBhcmVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSA6aHJlZj1cIicvcHJvZHVjdD9zZWFyY2g9Jyt0aGlzLmNhdGVnb3J5LnNsdWdcIj57e2dldFRpdGxlfX08L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gdi1mb3I9XCJkYXRhIGluIGZpcnN0Q29sdW1uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaSA6Y2xhc3M9XCJkYXRhLmlzUGFyZW50ID8gJ2Ryb3Bkb3duLWhlYWRlciBzZWNwYXJlbnQnIDogJydcIiB2LWlmPVwibGFuZz09J2NuJ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCIgdi1pZj1cImRhdGEubmFtZV9jblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGRhdGEubmFtZV9jbiB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCIgdi1lbHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGRhdGEubmFtZSB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgOmNsYXNzPVwiZGF0YS5pc1BhcmVudCA/ICdkcm9wZG93bi1oZWFkZXIgc2VjcGFyZW50JyA6ICcnXCIgdi1lbHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3sgZGF0YS5uYW1lIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cImRpdmlkZXJcIj48L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDxsaSBjbGFzcz1cImNvbC1zbS0yXCI+XHJcbiAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJsaXN0LXVuc3R5bGVkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwiZHJvcGRvd24taGVhZGVyIGdyYW5kcGFyZW50XCI+Jm5ic3A7PC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiB2LWZvcj1cImRhdGEgaW4gc2Vjb25kQ29sdW1uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaSA6Y2xhc3M9XCJkYXRhLmlzUGFyZW50ID8gJ2Ryb3Bkb3duLWhlYWRlciBzZWNwYXJlbnQnIDogJydcIiB2LWlmPVwibGFuZz09J2NuJ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCIgdi1pZj1cImRhdGEubmFtZV9jblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGRhdGEubmFtZV9jbiB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCIgdi1lbHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGRhdGEubmFtZSB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgOmNsYXNzPVwiZGF0YS5pc1BhcmVudCA/ICdkcm9wZG93bi1oZWFkZXIgc2VjcGFyZW50JyA6ICcnXCIgdi1lbHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3sgZGF0YS5uYW1lIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cImRpdmlkZXJcIj48L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDxsaSBjbGFzcz1cImNvbC1zbS0yXCI+XHJcbiAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJsaXN0LXVuc3R5bGVkXCIgPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cImRyb3Bkb3duLWhlYWRlciBncmFuZHBhcmVudFwiPiZuYnNwOzwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gdi1mb3I9XCJkYXRhIGluIHRoaXJkQ29sdW1uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaSA6Y2xhc3M9XCJkYXRhLmlzUGFyZW50ID8gJ2Ryb3Bkb3duLWhlYWRlciBzZWNwYXJlbnQnIDogJydcIiB2LWlmPVwibGFuZz09J2NuJ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCIgdi1pZj1cImRhdGEubmFtZV9jblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGRhdGEubmFtZV9jbiB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCIgdi1lbHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGRhdGEubmFtZSB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgOmNsYXNzPVwiZGF0YS5pc1BhcmVudCA/ICdkcm9wZG93bi1oZWFkZXIgc2VjcGFyZW50JyA6ICcnXCIgdi1lbHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgOmhyZWY9XCInL3Byb2R1Y3Q/c2VhcmNoPScrZGF0YS5zbHVnXCIgOmNsYXNzPVwiIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3sgZGF0YS5uYW1lIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cImRpdmlkZXJcIj48L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDxsaSBjbGFzcz1cImNvbC1zbS0yXCI+XHJcbiAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJsaXN0LXVuc3R5bGVkXCIgPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz1cImRyb3Bkb3duLWhlYWRlciBncmFuZHBhcmVudFwiPiZuYnNwOzwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gdi1mb3I9XCJkYXRhIGluIGZvdXJ0aENvbHVtblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgOmNsYXNzPVwiZGF0YS5pc1BhcmVudCA/ICdkcm9wZG93bi1oZWFkZXIgc2VjcGFyZW50JyA6ICcnXCIgdi1pZj1cImxhbmc9PSdjbidcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIDpocmVmPVwiJy9wcm9kdWN0P3NlYXJjaD0nK2RhdGEuc2x1Z1wiIDpjbGFzcz1cIiFkYXRhLmlzUGFyZW50ID8gJ3ItYXJyb3cgZG93bicgOiAnJ1wiIHYtaWY9XCJkYXRhLm5hbWVfY25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7eyBkYXRhLm5hbWVfY24gfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIDpocmVmPVwiJy9wcm9kdWN0P3NlYXJjaD0nK2RhdGEuc2x1Z1wiIDpjbGFzcz1cIiFkYXRhLmlzUGFyZW50ID8gJ3ItYXJyb3cgZG93bicgOiAnJ1wiIHYtZWxzZT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7eyBkYXRhLm5hbWUgfX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpIDpjbGFzcz1cImRhdGEuaXNQYXJlbnQgPyAnZHJvcGRvd24taGVhZGVyIHNlY3BhcmVudCcgOiAnJ1wiIHYtZWxzZT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIDpocmVmPVwiJy9wcm9kdWN0P3NlYXJjaD0nK2RhdGEuc2x1Z1wiIDpjbGFzcz1cIiFkYXRhLmlzUGFyZW50ID8gJ3ItYXJyb3cgZG93bicgOiAnJ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7IGRhdGEubmFtZSB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJkaXZpZGVyXCI+PC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgIDwvdWw+XHJcbiAgICA8L2xpPlxyXG48L3RlbXBsYXRlPlxyXG48c2NyaXB0PlxyXG4gICAgZXhwb3J0IGRlZmF1bHR7XHJcbiAgICAgICAgbW91bnRlZCgpe1xyXG4gICAgICAgICAgICAkKCcuY2Fyb3VzZWwnKS5jYXJvdXNlbCh7XHJcbiAgICAgICAgICAgICAgICBpbnRlcnZhbDogMzAwMCAvL2NoYW5nZXMgdGhlIHNwZWVkXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHByb3BzOiBbJ2NhdGVnb3J5J10sXHJcbiAgICAgICAgZGF0YSgpe1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgaW1hZ2U6ICcnLFxyXG4gICAgICAgICAgICAgICAgcGVyQ29sdW1uOjAsXHJcbiAgICAgICAgICAgICAgICBwcm9kdWN0czpbXSxcclxuICAgICAgICAgICAgICAgIGZpeGVkQ2F0ZWdvcmllczpbXSxcclxuICAgICAgICAgICAgICAgIGhsYW5nOltdLFxyXG4gICAgICAgICAgICAgICAgY2xhbmc6W10sXHJcbiAgICAgICAgICAgICAgICBmaXJzdENvbHVtbjpbXSxcclxuICAgICAgICAgICAgICAgIHNlY29uZENvbHVtbjpbXSxcclxuICAgICAgICAgICAgICAgIHRoaXJkQ29sdW1uOltdLFxyXG4gICAgICAgICAgICAgICAgZm91cnRoQ29sdW1uOltdLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgICAgICBmdW5jdGlvbiBnZXRIZWFkZXJUcmFuc2xhdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBheGlvcy5nZXQoJy90cmFuc2xhdGUvaGVhZGVyJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZnVuY3Rpb24gZ2V0Q2FydFRyYW5zbGF0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGF4aW9zLmdldCgnL3RyYW5zbGF0ZS9jYXJ0Jyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGF4aW9zLmFsbChbXHJcbiAgICAgICAgICAgICAgICBnZXRIZWFkZXJUcmFuc2xhdGlvbigpLFxyXG4gICAgICAgICAgICAgICAgZ2V0Q2FydFRyYW5zbGF0aW9uKClcclxuICAgICAgICAgICAgXSkudGhlbihheGlvcy5zcHJlYWQoXHJcbiAgICAgICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICBodHJhbnNsYXRpb24sXHJcbiAgICAgICAgICAgICAgICBjdHJhbnNsYXRpb25cclxuICAgICAgICAgICAgICAgICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5obGFuZyA9IGh0cmFuc2xhdGlvbi5kYXRhLmRhdGE7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNsYW5nID0gY3RyYW5zbGF0aW9uLmRhdGEuZGF0YTtcclxuXHJcbiAgICAgICAgICAgIH0pKVxyXG4gICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdHMgPSB0aGlzLmNhdGVnb3J5LnByb2R1Y3RzO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgZm9yKHZhciBqPTA7IGogPCB0aGlzLmNhdGVnb3J5LmNoaWxkcmVuLmxlbmd0aDsgaisrKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNhdGVnb3J5LmNoaWxkcmVuW2pdLmlzUGFyZW50ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZml4ZWRDYXRlZ29yaWVzLnB1c2godGhpcy5jYXRlZ29yeS5jaGlsZHJlbltqXSlcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9kdWN0cyA9IHRoaXMucHJvZHVjdHMuY29uY2F0KHRoaXMuY2F0ZWdvcnkuY2hpbGRyZW5bal0ucHJvZHVjdHMpO1xyXG4gICAgICAgICAgICAgICAgZm9yKHZhciBrPTA7IGsgPCB0aGlzLmNhdGVnb3J5LmNoaWxkcmVuW2pdLmNoaWxkcmVuLmxlbmd0aDsgaysrKVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2F0ZWdvcnkuY2hpbGRyZW5bal0uY2hpbGRyZW5ba10uaXNQYXJlbnQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpeGVkQ2F0ZWdvcmllcy5wdXNoKHRoaXMuY2F0ZWdvcnkuY2hpbGRyZW5bal0uY2hpbGRyZW5ba10pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHMgPSB0aGlzLnByb2R1Y3RzLmNvbmNhdCh0aGlzLmNhdGVnb3J5LmNoaWxkcmVuW2pdLmNoaWxkcmVuW2tdLnByb2R1Y3RzKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgJCgnLmNhcm91c2VsJykuY2Fyb3VzZWwoe1xyXG4gICAgICAgICAgICAgICAgaW50ZXJ2YWw6IDMwMDAgLy9jaGFuZ2VzIHRoZSBzcGVlZFxyXG4gICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgICAgZm9yKHZhciBpPTA7IGk8dGhpcy5maXhlZENhdGVnb3JpZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMuZmlyc3RDb2x1bW4ubGVuZ3RoIDwgNikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmlyc3RDb2x1bW4ucHVzaCh0aGlzLmZpeGVkQ2F0ZWdvcmllc1tpXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYodGhpcy5zZWNvbmRDb2x1bW4ubGVuZ3RoIDwgNikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2Vjb25kQ29sdW1uLnB1c2godGhpcy5maXhlZENhdGVnb3JpZXNbaV0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmKHRoaXMudGhpcmRDb2x1bW4ubGVuZ3RoIDwgNikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGhpcmRDb2x1bW4ucHVzaCh0aGlzLmZpeGVkQ2F0ZWdvcmllc1tpXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYodGhpcy5mb3VydGhDb2x1bW4ubGVuZ3RoIDwgNikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm91cnRoQ29sdW1uLnB1c2godGhpcy5maXhlZENhdGVnb3JpZXNbaV0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG1ldGhvZHM6e1xyXG4gICAgICAgICAgICBhZGRUb0Zhdm9yaXRlcyhpZCxuYW1lKXtcclxuICAgICAgICAgICAgICAgIGlmKExhcmF2ZWwuaXNBdXRoZW50aWNhdGVkID09PSB0cnVlKXtcclxuICAgICAgICAgICAgICAgICAgICBheGlvc0FQSXYxLmdldCgnL2FkZHRvZmF2b3JpdGVzLycgKyBpZClcclxuICAgICAgICAgICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihyZXN1bHQuZGF0YS5yZXNwb25zZT09MSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgZmF2ID0gcGFyc2VJbnQoJChcIiNmYXYtY291bnRcIikudGV4dCgpKSsxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNmYXYtY291bnRcIikudGV4dChmYXYpLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5zdWNjZXNzKHRoaXMuY2xhbmdbJ2FkZGVkLWZhdm9yaXRlcyddLCBuYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmKHJlc3VsdC5kYXRhLnJlc3BvbnNlPT0yKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcihyZXN1bHQuZGF0YS5lcnJvcl9tc2csIHRoaXMuY2xhbmdbJ25vdC1hZGRlZC13aXNobGlzdCddKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ci5pbmZvKHRoaXMuY2xhbmdbJ2FscmVhZHktZmF2b3JpdGVzJ10sIG5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgfSBlbHNle1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0ci5lcnJvcih0aGlzLmNsYW5nWydsb2dpbi1mYXZvcml0ZXMnXSk7ICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgaGFzQWRkZWRSb3coaXNQYXJlbnQsbWluaW11bSl7XHJcbiAgICAgICAgICAgICAgICBpZihpc1BhcmVudClcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbWluaW11bS0xO1xyXG4gICAgICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbWluaW11bTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIG1heGltdW1MYXN0Q29sdW1uKCl7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLmZpeGVkQ2F0ZWdvcmllc1s2XS5pc1BhcmVudClcclxuICAgICAgICAgICAgICAgIHtcclxuXHJcbiAgICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgICAge1xyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNvbXB1dGVkOntcclxuICAgICAgICAgICAgZ2V0VGl0bGUoKXtcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMubGFuZyA9PSBcImNuXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuY2F0ZWdvcnkubmFtZV9jbil7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmNhdGVnb3J5Lm5hbWVfY24ucmVwbGFjZShcIidzIEZhc2hpb25cIixcIlwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuY2F0ZWdvcnkubmFtZS5yZXBsYWNlKFwiJ3MgRmFzaGlvblwiLFwiXCIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuY2F0ZWdvcnkubmFtZS5yZXBsYWNlKFwiJ3MgRmFzaGlvblwiLFwiXCIpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBnZXRQYXJlbnQoKXtcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMubGFuZyA9PSBcImNuXCIpe1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuZml4ZWRDYXRlZ29yaWVzWzZdLm5hbWVfY24pe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5maXhlZENhdGVnb3JpZXNbNl0ubmFtZV9jbi5yZXBsYWNlKFwiJ3MgRmFzaGlvblwiLFwiXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5maXhlZENhdGVnb3JpZXNbNl0ubmFtZS5yZXBsYWNlKFwiJ3MgRmFzaGlvblwiLFwiXCIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZml4ZWRDYXRlZ29yaWVzWzZdLm5hbWUucmVwbGFjZShcIidzIEZhc2hpb25cIixcIlwiKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZ2V0UGFyZW50Migpe1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5sYW5nID09IFwiY25cIil7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5maXhlZENhdGVnb3JpZXNbdGhpcy5oYXNBZGRlZFJvdyh0aGlzLmZpeGVkQ2F0ZWdvcmllc1s2XS5pc1BhcmVudCwxMyldLm5hbWVfY24pe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5maXhlZENhdGVnb3JpZXNbdGhpcy5oYXNBZGRlZFJvdyh0aGlzLmZpeGVkQ2F0ZWdvcmllc1s2XS5pc1BhcmVudCwxMyldLm5hbWVfY247XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZpeGVkQ2F0ZWdvcmllc1t0aGlzLmhhc0FkZGVkUm93KHRoaXMuZml4ZWRDYXRlZ29yaWVzWzZdLmlzUGFyZW50LDEzKV0ubmFtZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZpeGVkQ2F0ZWdvcmllc1t0aGlzLmhhc0FkZGVkUm93KHRoaXMuZml4ZWRDYXRlZ29yaWVzWzZdLmlzUGFyZW50LDEzKV0ubmFtZTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgbGFuZygpe1xyXG4gICAgICAgICAgICAgICAgdmFyIG0gPSAkKFwibWV0YVtuYW1lPWxvY2FsZS1sYW5nXVwiKTsgICAgXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbS5hdHRyKFwiY29udGVudFwiKTtcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gQ2F0ZWdvcnlIZWFkZXIudnVlPzlkMjZiODBhIiwiPHRlbXBsYXRlPlxyXG48bGk+XHJcbiAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMiBub3RpZi1ib3hcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMlwiPlxyXG4gICAgICAgICAgICA8aW1nIGNsYXNzPVwibm90aWYtaW1nIGltZy1jaXJjbGVcIiB2LWJpbmQ6c3JjPVwiZnJpZW5kLnVzZXIuYXZhdGFyXCIgYWx0PVwiXCI+XHJcbiAgICAgICAgICA8L2Rpdj48IS0tIEVuZCBjb2wtc20tMiAtLT5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMTAgbXNnLWNvbnRleHRcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMTJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTYgbm90aWYtbmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICAgICAgICB7e2ZyaWVuZC51c2VyLmZpcnN0X25hbWV9fSB7e2ZyaWVuZC51c2VyLmxhc3RfbmFtZX19XHJcbiAgICAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICAgPC9kaXY+PCEtLSBFbmQgY29sLXNtLTYgbXNnLW5hbWUgLS0+XHJcbiAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS02IGZyLWJ0bi1zZXRcIiA+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS02IGZyLWJ1dHRvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6dm9pZCgwKVwiIEBjbGljaz0nYWNjZXB0RnJpZW5kKGZyaWVuZC51c2VyLmlkKScgY2xhc3M9XCJidG4gYnRuLXJhaXNlZCBidG4tdGFuZ2VyaW5lXCI+QWNjZXB0PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tNiBmci1idXR0b25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBAY2xpY2s9J3JlamVjdEZyaWVuZChmcmllbmQudXNlci5pZCknIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0XCI+UmVqZWN0PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgIDwvZGl2PjwhLS0gRW5kIGNvbC1zbS02IG1zZy10aW1lIC0tPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+PCEtLSBFbmQgcm93IC0tPlxyXG4gICAgICAgICAgICAgIDwvZGl2PjwhLS0gRW5kIGNvbC1zbS0xMiAtLT5cclxuICAgICAgICAgICAgPC9kaXY+PCEtLSBFbmQgcm93IC0tPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMiBtc2ctY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICAgIHt7ZnJpZW5kLmRhdGV9fVxyXG4gICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgIDwvZGl2PjwhLS0gRW5kIGNvbC1zbS0xMiAtLT5cclxuICAgICAgICAgICAgPC9kaXY+PCEtLSBFbmQgcm93IC0tPlxyXG4gICAgICAgICAgPC9kaXY+PCEtLSBFbmQgbXNnLWNvbnRleHQgLS0+XHJcbiAgICAgICAgPC9kaXY+PCEtLSBFbmQgbm90aWYtYm94IC0tPlxyXG4gICAgPC9kaXY+PCEtLSBFbmQgcm93IC0tPlxyXG48L2xpPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cdHByb3BzOiBbJ2ZyaWVuZCddLFxyXG5cclxuICBtZXRob2RzOntcclxuICAgIGFjY2VwdEZyaWVuZChpZCl7XHJcbiAgICAgIGF4aW9zQVBJdjFcclxuICAgICAgICAucG9zdCgnL2ZyaWVuZC9maW5kJyx7XHJcbiAgICAgICAgICB1c2VyX2lkOkxhcmF2ZWwudXNlci5pZCxcclxuICAgICAgICAgIHVzZXJfaWQyOmlkXHJcbiAgICAgICAgfSlcclxuICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBpZihyZXNwb25zZS5zdGF0dXMgPT0gMjAwKVxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICBheGlvc0FQSXYxXHJcbiAgICAgICAgICAgICAgLnB1dCgnL2ZyaWVuZHMvJytyZXNwb25zZS5kYXRhLHtcclxuICAgICAgICAgICAgICAgIHN0YXR1czonRnJpZW5kJ1xyXG4gICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgLnRoZW4oZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZihkYXRhLnN0YXR1cyA9PSAyMDApXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgIGZvcih2YXIgaj0wOyBqPHRoaXMuJHJvb3QucmVxdWVzdHMubGVuZ3RoOyBqKyspXHJcbiAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZih0aGlzLiRyb290LnJlcXVlc3RzW2pdLnVzZXIuaWQgPT0gaWQpXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5yZXF1ZXN0cy5zcGxpY2UoaiwxKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJycsJ0ZyaWVuZCBBY2NlcHRlZCEnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgIC5jYXRjaCgkLm5vb3ApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgcmVqZWN0RnJpZW5kKGlkKXtcclxuICAgICAgYXhpb3NBUEl2MVxyXG4gICAgICAgIC5wb3N0KCcvZnJpZW5kL2ZpbmQnLHtcclxuICAgICAgICAgIHVzZXJfaWQ6TGFyYXZlbC51c2VyLmlkLFxyXG4gICAgICAgICAgdXNlcl9pZDI6aWRcclxuICAgICAgICB9KVxyXG4gICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIGlmKHJlc3BvbnNlLnN0YXR1cyA9PSAyMDApXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGF4aW9zQVBJdjFcclxuICAgICAgICAgICAgICAuZGVsZXRlKCcvZnJpZW5kcy8nK3Jlc3BvbnNlLmRhdGEpXHJcbiAgICAgICAgICAgICAgLnRoZW4oZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZihkYXRhLnN0YXR1cyA9PSAyMDApXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgIGZvcih2YXIgaj0wOyBqPHRoaXMuJHJvb3QucmVxdWVzdHMubGVuZ3RoOyBqKyspXHJcbiAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZih0aGlzLiRyb290LnJlcXVlc3RzW2pdLnVzZXIuaWQgPT0gaWQpXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy4kcm9vdC5yZXF1ZXN0cy5zcGxpY2UoaiwxKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgdG9hc3RyLnN1Y2Nlc3MoJycsJ1JlcXVlc3QgUmVqZWN0ZWQhJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAuY2F0Y2goJC5ub29wKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgfVxyXG4gIH0sXHJcblxyXG59XHJcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBGcmllbmRzLnZ1ZT9iMTk3M2ZjYyIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL0NhdGVnb3J5SGVhZGVyLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMmUwMTNiMzNcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQ2F0ZWdvcnlIZWFkZXIudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJDOlxcXFx4YW1wcFxcXFxodGRvY3NcXFxcd3ljMDZcXFxccmVzb3VyY2VzXFxcXGFzc2V0c1xcXFxqc1xcXFxzaG9wcGluZ1xcXFxjb21wb25lbnRzXFxcXGNvbW1vblxcXFxDYXRlZ29yeUhlYWRlci52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBDYXRlZ29yeUhlYWRlci52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMmUwMTNiMzNcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0yZTAxM2IzM1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvY29tbW9uL0NhdGVnb3J5SGVhZGVyLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMzI3XG4vLyBtb2R1bGUgY2h1bmtzID0gMTMiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9GcmllbmRzLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZjZkMmQyODZcXFwifSEuLi8uLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vRnJpZW5kcy52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIkM6XFxcXHhhbXBwXFxcXGh0ZG9jc1xcXFx3eWMwNlxcXFxyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXHNob3BwaW5nXFxcXGNvbXBvbmVudHNcXFxcY29tbW9uXFxcXEZyaWVuZHMudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gRnJpZW5kcy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtZjZkMmQyODZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi1mNmQyZDI4NlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvY29tbW9uL0ZyaWVuZHMudnVlXG4vLyBtb2R1bGUgaWQgPSAzMjhcbi8vIG1vZHVsZSBjaHVua3MgPSAxMyIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZHJvcGRvd24gbWVnYS1tZW51XCJcbiAgfSwgW19jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLXRvZ2dsZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImhyZWZcIjogXCIjXCIsXG4gICAgICBcImRhdGEtdG9nZ2xlXCI6IFwiZHJvcGRvd25cIixcbiAgICAgIFwicm9sZVwiOiBcImJ1dHRvblwiLFxuICAgICAgXCJhcmlhLWhhc3BvcHVwXCI6IFwidHJ1ZVwiLFxuICAgICAgXCJhcmlhLWV4cGFuZGVkXCI6IFwiZmFsc2VcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIlxcbiAgICAgICAgXCIgKyBfdm0uX3MoX3ZtLmdldFRpdGxlKSArIFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2FyZXRcIlxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZHJvcGRvd24tbWVudSBtZWdhLWRyb3Bkb3duLW1lbnUgcm93XCJcbiAgfSwgW19jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tNFwiLFxuICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICBcInRleHQtYWxpZ25cIjogXCJjZW50ZXJcIixcbiAgICAgIFwicGFkZGluZy1sZWZ0XCI6IFwiNDBweFwiLFxuICAgICAgXCJwYWRkaW5nLXJpZ2h0XCI6IFwiNDBweFwiXG4gICAgfVxuICB9LCBbX2MoJ3VsJywgWyhfdm0ucHJvZHVjdHMubGVuZ3RoKSA/IF9jKCdkaXYnLCBbX2MoJ2xpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLWhlYWRlclwiXG4gIH0sIFtfdm0uX3YoX3ZtLl9zKF92bS5obGFuZ1snbmV3LXByb2R1Y3RzJ10pKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcm91c2VsIHNsaWRlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJteUNhcm91c2VsXCIsXG4gICAgICBcImRhdGEtcmlkZVwiOiBcImNhcm91c2VsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNhcm91c2VsLWlubmVyXCJcbiAgfSwgX3ZtLl9sKChfdm0ucHJvZHVjdHMpLCBmdW5jdGlvbihwcm9kdWN0LCBrZXkpIHtcbiAgICByZXR1cm4gKGtleSA8IDMpID8gX2MoJ2RpdicsIHtcbiAgICAgIGtleTogcHJvZHVjdC5pZCxcbiAgICAgIHN0YXRpY0NsYXNzOiBcIml0ZW1cIixcbiAgICAgIGNsYXNzOiBrZXkgPT0gMCA/ICdhY3RpdmUnIDogJydcbiAgICB9LCBbX2MoJ2EnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJibG9jay1jZW50ZXJcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiAncHJvZHVjdC8nICsgcHJvZHVjdC5zbHVnXG4gICAgICB9XG4gICAgfSwgW19jKCdpbWcnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJibG9jay1jZW50ZXJcIixcbiAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgIFwiaGVpZ2h0XCI6IFwiMTUwcHhcIixcbiAgICAgICAgXCJtYXJnaW4tbGVmdFwiOiBcImF1dG9cIixcbiAgICAgICAgXCJtYXJnaW4tcmlnaHRcIjogXCJhdXRvXCJcbiAgICAgIH0sXG4gICAgICBhdHRyczoge1xuICAgICAgICBcInNyY1wiOiBwcm9kdWN0Lm1haW5faW1hZ2VfdGh1bWJuYWlsLFxuICAgICAgICBcImFsdFwiOiBwcm9kdWN0LnNsdWdcbiAgICAgIH1cbiAgICB9KV0pLCBfdm0uX3YoXCIgXCIpLCAoX3ZtLmxhbmcgPT0gJ2NuJykgPyBfYygnaDQnLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJoZWFkZXItY2F0ZWdvcnktcHJvZHVjdC1zbGlkZXItbmFtZVwiXG4gICAgfSwgWyhwcm9kdWN0LmNuX25hbWUpID8gX2MoJ3NtYWxsJywgW192bS5fdihfdm0uX3MocHJvZHVjdC5jbl9uYW1lKSldKSA6IF9jKCdzbWFsbCcsIFtfdm0uX3YoX3ZtLl9zKHByb2R1Y3QubmFtZSkpXSldKSA6IF9jKCdoNCcsIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImhlYWRlci1jYXRlZ29yeS1wcm9kdWN0LXNsaWRlci1uYW1lXCJcbiAgICB9LCBbX3ZtLl92KF92bS5fcyhwcm9kdWN0Lm5hbWUpKV0pLCBfdm0uX3YoXCIgXCIpLCAocHJvZHVjdC5zYWxlX3ByaWNlICE9IG51bGwpID8gX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LXByaW1hcnlcIlxuICAgIH0sIFsocHJvZHVjdC5mZWVfaW5jbHVkZWQgPT0gMSkgPyBfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHBhcnNlRmxvYXQocHJvZHVjdC5zYWxlX3ByaWNlKSArIHBhcnNlRmxvYXQocHJvZHVjdC5zaGlwcGluZ19mZWUpICsgcGFyc2VGbG9hdChwcm9kdWN0LmNoYXJnZSkgKyBwYXJzZUZsb2F0KHByb2R1Y3QudmF0KSkpKV0pIDogX2MoJ3NwYW4nLCBbX3ZtLl92KF92bS5fcyhfdm0uX2YoXCJjdXJyZW5jeVwiKShwYXJzZUZsb2F0KHByb2R1Y3Quc2FsZV9wcmljZSkpKSldKV0pIDogX2MoJ3NwYW4nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LXByaW1hcnlcIlxuICAgIH0sIFsocHJvZHVjdC5mZWVfaW5jbHVkZWQgPT0gMSkgPyBfYygnc3BhbicsIFtfdm0uX3YoX3ZtLl9zKF92bS5fZihcImN1cnJlbmN5XCIpKHBhcnNlRmxvYXQocHJvZHVjdC5wcmljZSkgKyBwYXJzZUZsb2F0KHByb2R1Y3Quc2hpcHBpbmdfZmVlKSArIHBhcnNlRmxvYXQocHJvZHVjdC5jaGFyZ2UpICsgcGFyc2VGbG9hdChwcm9kdWN0LnZhdCkpKSldKSA6IF9jKCdzcGFuJywgW192bS5fdihfdm0uX3MoX3ZtLl9mKFwiY3VycmVuY3lcIikocGFyc2VGbG9hdChwcm9kdWN0LnByaWNlKSkpKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdidXR0b24nLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHRcIixcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwidHlwZVwiOiBcImJ1dHRvblwiXG4gICAgICB9LFxuICAgICAgb246IHtcbiAgICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICBfdm0uYWRkVG9GYXZvcml0ZXMocHJvZHVjdC5pZCwgcHJvZHVjdC5uYW1lKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgW19jKCdzcGFuJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi1oZWFydFwiXG4gICAgfSksIF92bS5fdihcIiBcIiArIF92bS5fcyhfdm0uY2xhbmdbJ2FkZC10by13aXNobGlzdCddKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIildKV0pIDogX3ZtLl9lKClcbiAgfSkpXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJkaXZpZGVyXCJcbiAgfSldKSA6IF92bS5fZSgpLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCBbX2MoJ2EnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1yYWlzZWQgYnRuLXByaW1hcnlcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiL2NhdGVnb3JpZXNcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIiBcIiArIF92bS5fcyhfdm0uaGxhbmdbJ3ZpZXctYWxsLWNhdCddKSArIFwiIFwiKSwgX2MoJ3NwYW4nLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi1jaGV2cm9uLXJpZ2h0IHB1bGwtcmlnaHRcIlxuICB9KV0pXSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTJcIlxuICB9LCBbX2MoJ3VsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImxpc3QtdW5zdHlsZWRcIlxuICB9LCBbX2MoJ2xpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLWhlYWRlciBncmFuZHBhcmVudFwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdD9zZWFyY2g9JyArIHRoaXMuY2F0ZWdvcnkuc2x1Z1xuICAgIH1cbiAgfSwgW192bS5fdihfdm0uX3MoX3ZtLmdldFRpdGxlKSldKV0pLCBfdm0uX3YoXCIgXCIpLCBfdm0uX2woKF92bS5maXJzdENvbHVtbiksIGZ1bmN0aW9uKGRhdGEpIHtcbiAgICByZXR1cm4gX2MoJ3NwYW4nLCBbKF92bS5sYW5nID09ICdjbicpID8gX2MoJ2xpJywge1xuICAgICAgY2xhc3M6IGRhdGEuaXNQYXJlbnQgPyAnZHJvcGRvd24taGVhZGVyIHNlY3BhcmVudCcgOiAnJ1xuICAgIH0sIFsoZGF0YS5uYW1lX2NuKSA/IF9jKCdhJywge1xuICAgICAgY2xhc3M6ICFkYXRhLmlzUGFyZW50ID8gJ3ItYXJyb3cgZG93bicgOiAnJyxcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiAnL3Byb2R1Y3Q/c2VhcmNoPScgKyBkYXRhLnNsdWdcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGRhdGEubmFtZV9jbikgKyBcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiKV0pIDogX2MoJ2EnLCB7XG4gICAgICBjbGFzczogIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdD9zZWFyY2g9JyArIGRhdGEuc2x1Z1xuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoZGF0YS5uYW1lKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIpXSldKSA6IF9jKCdsaScsIHtcbiAgICAgIGNsYXNzOiBkYXRhLmlzUGFyZW50ID8gJ2Ryb3Bkb3duLWhlYWRlciBzZWNwYXJlbnQnIDogJydcbiAgICB9LCBbX2MoJ2EnLCB7XG4gICAgICBjbGFzczogIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdD9zZWFyY2g9JyArIGRhdGEuc2x1Z1xuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoZGF0YS5uYW1lKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZGl2aWRlclwiXG4gICAgfSldKVxuICB9KV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMlwiXG4gIH0sIFtfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGlzdC11bnN0eWxlZFwiXG4gIH0sIFtfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZHJvcGRvd24taGVhZGVyIGdyYW5kcGFyZW50XCJcbiAgfSwgW192bS5fdihcIsKgXCIpXSksIF92bS5fdihcIiBcIiksIF92bS5fbCgoX3ZtLnNlY29uZENvbHVtbiksIGZ1bmN0aW9uKGRhdGEpIHtcbiAgICByZXR1cm4gX2MoJ3NwYW4nLCBbKF92bS5sYW5nID09ICdjbicpID8gX2MoJ2xpJywge1xuICAgICAgY2xhc3M6IGRhdGEuaXNQYXJlbnQgPyAnZHJvcGRvd24taGVhZGVyIHNlY3BhcmVudCcgOiAnJ1xuICAgIH0sIFsoZGF0YS5uYW1lX2NuKSA/IF9jKCdhJywge1xuICAgICAgY2xhc3M6ICFkYXRhLmlzUGFyZW50ID8gJ3ItYXJyb3cgZG93bicgOiAnJyxcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIFwiaHJlZlwiOiAnL3Byb2R1Y3Q/c2VhcmNoPScgKyBkYXRhLnNsdWdcbiAgICAgIH1cbiAgICB9LCBbX3ZtLl92KFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKGRhdGEubmFtZV9jbikgKyBcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiKV0pIDogX2MoJ2EnLCB7XG4gICAgICBjbGFzczogIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdD9zZWFyY2g9JyArIGRhdGEuc2x1Z1xuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoZGF0YS5uYW1lKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIpXSldKSA6IF9jKCdsaScsIHtcbiAgICAgIGNsYXNzOiBkYXRhLmlzUGFyZW50ID8gJ2Ryb3Bkb3duLWhlYWRlciBzZWNwYXJlbnQnIDogJydcbiAgICB9LCBbX2MoJ2EnLCB7XG4gICAgICBjbGFzczogIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdD9zZWFyY2g9JyArIGRhdGEuc2x1Z1xuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoZGF0YS5uYW1lKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiZGl2aWRlclwiXG4gICAgfSldKVxuICB9KV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMlwiXG4gIH0sIFtfYygndWwnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibGlzdC11bnN0eWxlZFwiXG4gIH0sIFtfYygnbGknLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZHJvcGRvd24taGVhZGVyIGdyYW5kcGFyZW50XCJcbiAgfSwgW192bS5fdihcIsKgXCIpXSksIF92bS5fdihcIiBcIiksIF92bS5fbCgoX3ZtLnRoaXJkQ29sdW1uKSwgZnVuY3Rpb24oZGF0YSkge1xuICAgIHJldHVybiBfYygnc3BhbicsIFsoX3ZtLmxhbmcgPT0gJ2NuJykgPyBfYygnbGknLCB7XG4gICAgICBjbGFzczogZGF0YS5pc1BhcmVudCA/ICdkcm9wZG93bi1oZWFkZXIgc2VjcGFyZW50JyA6ICcnXG4gICAgfSwgWyhkYXRhLm5hbWVfY24pID8gX2MoJ2EnLCB7XG4gICAgICBjbGFzczogIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdD9zZWFyY2g9JyArIGRhdGEuc2x1Z1xuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoZGF0YS5uYW1lX2NuKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIpXSkgOiBfYygnYScsIHtcbiAgICAgIGNsYXNzOiAhZGF0YS5pc1BhcmVudCA/ICdyLWFycm93IGRvd24nIDogJycsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogJy9wcm9kdWN0P3NlYXJjaD0nICsgZGF0YS5zbHVnXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhkYXRhLm5hbWUpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIildKV0pIDogX2MoJ2xpJywge1xuICAgICAgY2xhc3M6IGRhdGEuaXNQYXJlbnQgPyAnZHJvcGRvd24taGVhZGVyIHNlY3BhcmVudCcgOiAnJ1xuICAgIH0sIFtfYygnYScsIHtcbiAgICAgIGNsYXNzOiAhZGF0YS5pc1BhcmVudCA/ICdyLWFycm93IGRvd24nIDogJycsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogJy9wcm9kdWN0P3NlYXJjaD0nICsgZGF0YS5zbHVnXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhkYXRhLm5hbWUpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJkaXZpZGVyXCJcbiAgICB9KV0pXG4gIH0pXSwgMildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2xpJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0yXCJcbiAgfSwgW19jKCd1bCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJsaXN0LXVuc3R5bGVkXCJcbiAgfSwgW19jKCdsaScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJkcm9wZG93bi1oZWFkZXIgZ3JhbmRwYXJlbnRcIlxuICB9LCBbX3ZtLl92KFwiwqBcIildKSwgX3ZtLl92KFwiIFwiKSwgX3ZtLl9sKChfdm0uZm91cnRoQ29sdW1uKSwgZnVuY3Rpb24oZGF0YSkge1xuICAgIHJldHVybiBfYygnc3BhbicsIFsoX3ZtLmxhbmcgPT0gJ2NuJykgPyBfYygnbGknLCB7XG4gICAgICBjbGFzczogZGF0YS5pc1BhcmVudCA/ICdkcm9wZG93bi1oZWFkZXIgc2VjcGFyZW50JyA6ICcnXG4gICAgfSwgWyhkYXRhLm5hbWVfY24pID8gX2MoJ2EnLCB7XG4gICAgICBjbGFzczogIWRhdGEuaXNQYXJlbnQgPyAnci1hcnJvdyBkb3duJyA6ICcnLFxuICAgICAgYXR0cnM6IHtcbiAgICAgICAgXCJocmVmXCI6ICcvcHJvZHVjdD9zZWFyY2g9JyArIGRhdGEuc2x1Z1xuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCIgKyBfdm0uX3MoZGF0YS5uYW1lX2NuKSArIFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgXCIpXSkgOiBfYygnYScsIHtcbiAgICAgIGNsYXNzOiAhZGF0YS5pc1BhcmVudCA/ICdyLWFycm93IGRvd24nIDogJycsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogJy9wcm9kdWN0P3NlYXJjaD0nICsgZGF0YS5zbHVnXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhkYXRhLm5hbWUpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIildKV0pIDogX2MoJ2xpJywge1xuICAgICAgY2xhc3M6IGRhdGEuaXNQYXJlbnQgPyAnZHJvcGRvd24taGVhZGVyIHNlY3BhcmVudCcgOiAnJ1xuICAgIH0sIFtfYygnYScsIHtcbiAgICAgIGNsYXNzOiAhZGF0YS5pc1BhcmVudCA/ICdyLWFycm93IGRvd24nIDogJycsXG4gICAgICBhdHRyczoge1xuICAgICAgICBcImhyZWZcIjogJy9wcm9kdWN0P3NlYXJjaD0nICsgZGF0YS5zbHVnXG4gICAgICB9XG4gICAgfSwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIiArIF92bS5fcyhkYXRhLm5hbWUpICsgXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnbGknLCB7XG4gICAgICBzdGF0aWNDbGFzczogXCJkaXZpZGVyXCJcbiAgICB9KV0pXG4gIH0pXSwgMildKV0pXSlcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMmUwMTNiMzNcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0yZTAxM2IzM1wifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9zaG9wcGluZy9jb21wb25lbnRzL2NvbW1vbi9DYXRlZ29yeUhlYWRlci52dWVcbi8vIG1vZHVsZSBpZCA9IDM3NFxuLy8gbW9kdWxlIGNodW5rcyA9IDEzIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdsaScsIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xMiBub3RpZi1ib3hcIlxuICB9LCBbX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tMlwiXG4gIH0sIFtfYygnaW1nJywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm5vdGlmLWltZyBpbWctY2lyY2xlXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwic3JjXCI6IF92bS5mcmllbmQudXNlci5hdmF0YXIsXG4gICAgICBcImFsdFwiOiBcIlwiXG4gICAgfVxuICB9KV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xMCBtc2ctY29udGV4dFwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0xMlwiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcInJvd1wiXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS02IG5vdGlmLW5hbWVcIlxuICB9LCBbX2MoJ3AnLCBbX3ZtLl92KFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5mcmllbmQudXNlci5maXJzdF9uYW1lKSArIFwiIFwiICsgX3ZtLl9zKF92bS5mcmllbmQudXNlci5sYXN0X25hbWUpICsgXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wtc20tNiBmci1idG4tc2V0XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTYgZnItYnV0dG9uXCJcbiAgfSwgW19jKCdhJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcmFpc2VkIGJ0bi10YW5nZXJpbmVcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0uYWNjZXB0RnJpZW5kKF92bS5mcmllbmQudXNlci5pZClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJBY2NlcHRcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS02IGZyLWJ1dHRvblwiXG4gIH0sIFtfYygnYScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHRcIixcbiAgICBhdHRyczoge1xuICAgICAgXCJocmVmXCI6IFwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICB9LFxuICAgIG9uOiB7XG4gICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICBfdm0ucmVqZWN0RnJpZW5kKF92bS5mcmllbmQudXNlci5pZClcbiAgICAgIH1cbiAgICB9XG4gIH0sIFtfdm0uX3YoXCJSZWplY3RcIildKV0pXSldKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwicm93XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTEyIG1zZy1jb250ZW50XCJcbiAgfSwgW19jKCdwJywgW192bS5fdihcIlxcclxcbiAgICAgICAgICAgICAgICAgIFwiICsgX3ZtLl9zKF92bS5mcmllbmQuZGF0ZSkgKyBcIlxcclxcbiAgICAgICAgICAgICAgICBcIildKV0pXSldKV0pXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtdfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi1mNmQyZDI4NlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LWY2ZDJkMjg2XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Nob3BwaW5nL2NvbXBvbmVudHMvY29tbW9uL0ZyaWVuZHMudnVlXG4vLyBtb2R1bGUgaWQgPSA0MDZcbi8vIG1vZHVsZSBjaHVua3MgPSAxMyJdLCJzb3VyY2VSb290IjoiIn0=