@extends('cpanel.layouts.master')

@section('title', 'Categories')

@push('scripts')
    {!! Html::script(mix('js/category.js','cpanel')) !!}
@endpush

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Categories</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Categories</strong>
                </li>
            </ol>
        </div>
    </div>

<div id="category-root">
	<div class="wrapper wrapper-content" >
		<div class="row">
			<div class="col-md-12">
				<div class="tab-content">
					<div class="ibox float-e-margins">
		                <div class="ibox-title">
		                    <h5>Categories</h5>
		                     <div class="ibox-tools">
		                     	<a @click='showModal(0)'>
	                                <i class="fa fa-plus"></i>
	                            </a>
	                            <a class="collapse-link">
	                                <i class="fa fa-chevron-up"></i>
	                            </a>
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
	                                <i class="fa fa-filter"></i>
	                            </a>
	                            <div class="dropdown-menu dropdown-user p-sm">
	                            	<h3 class='text-center'>Filters</h3>
	                            	<b>Type :</b>
	                            		<div class="i-checks"><label> <input v-model='filterType'  type="radio" value="all" name="filter-type"> <i></i> All </label></div>
										<div class="i-checks"><label> <input v-model='filterType'  type="radio" value="GrandParent" name="filter-type"> <i></i> GrandParent </label></div>
										<div class="i-checks"><label> <input v-model='filterType' type="radio" value="Parent" name="filter-type"> <i></i> Parent </label></div>
										<div class="i-checks"><label> <input v-model='filterType' type="radio" value="Child" name="filter-type"> <i></i> Child </label></div>
	                            </div>
	                            <a class="fullscreen-link">
	                                <i class="fa fa-expand"></i>
	                            </a>
	                        </div>

		                </div>
		                <div class="ibox-content p-sm">
				                <table class="table table-bordered table-hover dt-category htFixTable" >
				                    <thead>
				                        <tr>
				                            <th>Category</th>
				                            <th>Type</th>
				                            <th>Status</th>
				                        </tr>
				                    </thead>
				                    <tbody>
				                        <tr 
				                        	v-if='showFilterType(getType(category.tree_parent))' 
				                        	v-for='category in categories' 
				                        	:key='category.id' 
				                        	class='mouse-cursor'
				                        	@click='showModal(category.id)'
				                        >
				                           <td v-cloak>@{{category.name}}</td>
				                           <td v-cloak>@{{getType(category.tree_parent)}}</td>
				                           <td v-cloak>
				                           		<span class="label label-success" v-if='parseInt(category.active)'>Active</span>
				                           		<span class="label label-info"  v-if='parseInt(category.featured)'>Featured</span>
				                           </td>
				                        </tr>
				                    </tbody>
				                </table>
		                </div>
		            </div>
				</div>


			</div>
		</div>
	</div>
	<!-- Edit Modal -->
	<modal size="modal-sm" id='categoryModal'>
	    <template slot="modal-title">
	        <h3>
	            Category
	        </h3>
	    </template>
	    <template slot="modal-body">
	        <div class="form-group">
	        	<label for="">Name</label>
	        	<input type="text" v-model='catName' placeholder="Category Name" class="form-control">
	        </div>
	        <div class="form-group">
	        	<label for="">Parent</label>
	        	<select id="" class="form-control" v-model='catParent'>
	        		<option value=''>No Parent</option>
	        		<option v-for="parent in categories" v-if='parent.tree_parent.indexOf("-") == -1 && parent.id != catId' :value='parent.id'>
	        			@{{parent.name}} (@{{getType(parent.tree_parent)}})
	        		</option>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label for=""><input v-model='catActivated' type="checkbox"> Activate</label>
        	</div>
	        <div class="form-group">
	        	<label for=""><input v-model='catFeatured' type="checkbox"> Feature</label>
	        </div>
	    </template>
	    <template slot="modal-footer">
	        <div class="form-group text-center">
	            <button class='btn btn-primary' @click='updateCategory()'>Submit</button>
	        </div>
	    </template>
	</modal>
</div>
@endsection
