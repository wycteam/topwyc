@extends('cpanel.layouts.master')

@section('title', 'Feedbacks')

@push('styles')

@endpush

@push('scripts')
    {!! Html::script(mix('js/feedback.js','cpanel')) !!}
@endpush

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Feedback</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Feedback</strong>
                </li>
            </ol>
        </div>
    </div>

<div class="wrapper wrapper-content animated fadeInRight" id='feedback-root'>
	<div class="row">
		<div class="col-md-12">
			<h1 class="pull-right m-r-lg m-t-lg dt-title"><b>Feedbacks</b></h1>
			<div class="col-md-2">
				<a data-toggle="tab" href="#fb-pending">
                    <div class="ibox-title">
                        <h3>Pending
	                        <span class="pull-right label label-warning">
			                    @{{pending_count}}
	                        </span>
                        </h3>
                    </div>
                </a>
			</div>
			<div class="col-md-2">
				<a data-toggle="tab" href="#fb-approved">
                    <div class="ibox-title">
                        <h3>Approved
	                        <span class="pull-right label label-info">
			                    @{{approved_count}}
	                        </span>
                        </h3>
                    </div>
                </a>
			</div>
			<div class="col-md-2">
				<a data-toggle="tab" href="#fb-rejected">
                    <div class="ibox-title">
                        <h3>Rejected
	                        <span class="pull-right label label-danger" v-cloak>
			                    @{{rejected_count}}
	                        </span>
                        </h3>
                    </div>
                </a>
			</div>
		</div>
		<div class="col-md-12 tab-content">
			
			<div id='fb-pending' class="ibox float-e-margins tab-pane fade in active">
				<div class="ibox-title  yellow-bg">
					<h5>Pending Feedbacks </h5>
					<div class="ibox-tools">
						<!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="fa fa-refresh"></i>
						</a> -->
					</div>
				</div>
				<div class="ibox-content">

					<div>
						<div class="feed-activity-list">
							<feedback v-for='feedback in pending' :feedback='feedback'></feedback>
							<h3 v-if='!pending.length'>No Pending Feedbacks.</h3>

							<div class='pagination-container text-center ' v-cloak>
								<a class='btn btn-sm btn-success' v-for='page in pending_page' @click='showPage(0, page)'>@{{page}}</a>							
							</div>
						</div>
					</div>

				</div>
			</div>

			
			<div id='fb-approved' class="ibox float-e-margins tab-pane fade ">
				<div class="ibox-title lazur-bg">
					<h5>Approved Feedbacks </h5>
					<div class="ibox-tools">
						<!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="fa fa-refresh"></i>
						</a> -->
					</div>
				</div>
				<div class="ibox-content">

					<div>
						<div class="feed-activity-list">
							<feedback v-for='feedback in approved' :feedback='feedback'></feedback>		
							<h3 v-if='!approved.length'>No Approved Feedbacks.</h3>
							<div class='pagination-container text-center ' v-cloak>
								<a class='btn btn-sm btn-success' v-for='page in approved_page' @click='showPage(1, page)'>@{{page}}</a>							
							</div>					
						</div>
					</div>

				</div>
			</div>

			
			<div id='fb-rejected' class="ibox float-e-margins tab-pane fade ">
				<div class="ibox-title red-bg">
					<h5>Rejected Feedbacks </h5>
					<div class="ibox-tools">
						<!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="fa fa-refresh"></i>
						</a> -->
					</div>
				</div>
				<div class="ibox-content">

					<div>
						<div class="feed-activity-list">
							<feedback v-for='feedback in rejected' :feedback='feedback'></feedback>	
							<h3 v-if='!rejected.length'>No Rejected Feedbacks.</h3>

							<div class='pagination-container text-center ' v-cloak>
								<a class='btn btn-sm btn-success' v-for='page in rejected_page' @click='showPage(2, page)'>@{{page}}</a>							
							</div>						
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>
@endsection
