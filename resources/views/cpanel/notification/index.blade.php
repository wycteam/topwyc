@extends('cpanel.layouts.master')

@section('title', 'Notifications')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Notifications</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Notifications</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Notification List</h5>
                </div>
                <div class="ibox-content">
                    @if ($notifications->count())
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Body</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($notifications as $notif)
                            <tr>
                                <td><img src="{{ $notif->data['image'] }}" width="24px" height="24px"></td>
                                <td>{{ $notif->data['title'] }}</td>
                                <td>{{ $notif->data['body'] }}</td>
                                <td>{{ $notif->created_at->format('F j, Y - g:i:s A') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                    <h4 class="text-center">You have no notification</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
