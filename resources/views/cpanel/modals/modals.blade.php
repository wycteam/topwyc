
<modal id='add-new-service-modal' size="modal-md">

	<template slot="modal-title">
        <h3>@lang('cpanel/clients-page.add-new-service')</h3>
    </template>

    <template slot="modal-body">

    	<add-new-service :tracking="tracking_selected" :client_id="usr_id" ref="addnewserviceref"></add-new-service>

    </template>

</modal>

<modal id='edit-service-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/clients-page.edit-service')</h3>
    </template>

    <template slot="modal-body">

        <edit-service :tracking="tracking_selected" :client_id="usr_id" ref="editserviceref"></edit-service>

    </template>

</modal>

<modal id='add-quick-report-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/clients-page.add-quick-report')</h3>
    </template>

    <template slot="modal-body">

        <!-- <multiple-quick-report :groupreport="false" :groupid="0" :clientsid="quickReportClientsId" :clientservices="quickReportClientServicesId" :trackings="quickReportTrackings" ref="multiplequickreportref"></multiple-quick-report> -->

        <multiple-quick-report-v2 ref="multiplequickreportv2ref" :groupreport="false" :groupid="0" :clientsid="quickReportClientsId" :clientservices="quickReportClientServicesId" :trackings="quickReportTrackings"></multiple-quick-report-v2>

    </template>

</modal>


<modal id='docs-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/clients-page.docs')</h3>
    </template>

    <template slot="modal-body">

        <docs ref="docsref"></docs>

    </template>

</modal>

<modal id='addDocumentsModal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/clients-page.add-documents')</h3>
    </template>

    <template slot="modal-body">

        <client-documents ref="clientdocuments"></client-documents>

    </template>

</modal>

<modal id='view-report-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/clients-page.reports')</h3>
    </template>

    <template slot="modal-body">

        <view-report :report_view="report_view" :report_id="report_id" ref="viewreportref"></view-report>

    </template>

</modal>