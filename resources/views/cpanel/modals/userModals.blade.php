<modals id='modalOrderDetails'  size="modal-lg" content-size='9'>
	<template slot="modal-title">
        <h3>
            @{{ or_num }}
        </h3>
    </template>
    <template slot="modal-body">
		<div class="wrapper wrapper-content ">
            <div class="ibox-content p-xl">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Name : <label class="text-navy">@{{ rcvr_name }}</label></h5>
                            <address>
                                <strong>@{{ address }}</strong><br>
                                <abbr title="Phone">Contact Number:</abbr> @{{ rcvr_contact }}
                            </address>
                            <p>
                                <span><strong>Date Ordered:</strong> @{{ rcvr_orderDate | invoiceFormat }}</span>
                            </p>
                        </div>
                    </div>

                    <div class="table-responsive m-t">
                        <table class="table invoice-table">
                            <thead>
                            <tr>
                                <th>Item List</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Total Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="prod in order_prods.data" :item="prod" :key="prod.id">
                                <td><div><strong>@{{ prod.product.name }}</strong></div>
                                    <small>@{{ prod.product.description }}</small>
                                    <br><span class="label label_success">@{{ prod.status }}</span>
                                </td>
                                <td><p class="text-center">@{{ prod.quantity }}</p></td>
                                <td>@{{ prod.price_per_item | currency }}</td>
                                <td>
                                	@{{ (prod.price_per_item * prod.quantity) | currency }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div><!-- /table-responsive -->

                    <table class="table invoice-total">
                        <tbody>
                        <tr>
                            <td><strong>Sub Total :</strong></td>
                            <td>@{{subtotal | currency}}</td>
                        </tr>
                        <tr>
                            <td><strong>Shipping Fee :</strong></td>
                            <td>@{{shipping_fee | currency}}</td>
                        </tr>
                        <tr>
                            <td><strong>TAX :</strong></td>
                            <td>@{{tax | currency}}</td>
                        </tr>
                        <tr>
                            <td><strong>TOTAL :</strong></td>
                            <td><b>@{{total_price | currency}}</b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
        </div>
    </template>
    <template slot="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
    </template>
</modals>

<div id="deletePackagePop" class="hide">
    <div id="validset" style="width: 300px;">
        @lang('cpanel/clients-page.prompt-del-package')
        <br>
        <br>
        <label for="reason">@lang('cpanel/clients-page.reason-delete'):</label>
        <textarea
            class="form-control autoExpand"
            rows="1"
            data-min-rows='1'
            id="reason"
            name="reason"
            required
            style="resize:none;"
         ></textarea>

        <button type="button" class="btn btn-w-m btn-primary pull-right del-pack m-b-2" style="margin-top: 5px;">
            @lang('cpanel/clients-page.save')
        </button>
    </div>
</div>

<div id="addFundsPop" class="hide">
    <div id="fundset" style="width: 300px;">
    <br/>
        <br/>
        <div class="form-group" >
            <label class="col-sm-4 control-label">@lang('cpanel/clients-page.type'):</label>
            <div class="col-sm-8">
                <select class="form-control" id="fund_type" name="fund_type">
                    <option value="deposit">@lang('cpanel/clients-page.deposit')</option>
                    <option value="payment">@lang('cpanel/clients-page.payment')</option>
                    <option value="refund">@lang('cpanel/clients-page.refund')</option>
                    <option value="discount">@lang('cpanel/clients-page.disc')</option>
                    <option value="transfer">@lang('cpanel/clients-page.bal-trans')</option>
                </select>
            </div>
        </div>

        <div class="form-group branch_type" style="margin-top:10px;clear:both;">
            <br />
            <label class="col-sm-4 control-label">Branch:</label>
            <div class="col-sm-8">
                <select class="form-control" id="branch_id" name="branch_id">
                    <option value="1">Manila</option>
                    <option value="2">Cebu</option>
                </select>
            </div>
        </div>
        <div class="form-group storage_type" style="margin-top:7px;clear:both;">
        
        <br/>
            <label class="col-sm-4 control-label">@lang('cpanel/clients-page.storage'):</label>
            <div class="col-sm-8">
                <select class="form-control" id="fund_storage" name="fund_storage">
                    <option value="cash">@lang('cpanel/clients-page.cash')</option>
                    <option value="bank">@lang('cpanel/clients-page.bank')</option>
                    <option class="alipay" value="alipay">Alipay</option>
                </select>
            </div>
        </div>
       
        <div class="form-group bank_type" style="margin-top:10px;clear:both;">
            <br/>

            <label class="col-sm-4 control-label">Bank:</label>
            <div class="col-sm-8">
                <select class="form-control" id="bank_id" name="bank_id">
                    <option class="metrobank" value="metrobank">MetroBank</option>
                    <option class="securitybank" value="securitybank">Security Bank</option>
                    <option class="aub" value="aub">AUB</option>
                    <option class="eastwest" value="eastwest">Eastwest</option>
                </select>
            </div>
        </div>
        
        <div class="form-group alipay_reference" style="margin-top:10px;clear:both;">
            <br />
 
            <label class="col-sm-4 control-label">Reference:</label>
            <div class="col-sm-8">
               <input class="form-control" type="text" id="alipay_reference" name="alipay_reference" />
            </div>
        </div>
        <br/>
        <br/>
        <div class="form-group" >
            <label class="col-sm-4 control-label">@lang('cpanel/clients-page.amt'):</label>
            <div class="col-sm-8">
                <button class="btn btn-white" id="fullBalance" data-toggle="tooltip" data-placement="left" title="Full Balance"><i class="fa fa-exchange"></i> </button>
                <input type="number" class="form-control" placeholder="..." min="0" id="fund_amount" name="fund_amount" style="width: 130px;display: inline; float: right;">
                <div class="clearbal">
                    <input type="checkbox" name="clearbalance" id="clearbalance">for balance clearing
                </div>
            </div>
        </div>

        <div class="form-group transferTo" style="margin-top:55px;clear:both;">
            <label class="col-sm-4 control-label">@lang('cpanel/clients-page.transfer-to'):</label>
            <div class="col-sm-8">
                <select class="form-control" id="transfer_type" name="transfer_type">
                    <option value="client">@lang('cpanel/clients-page.client')</option>
                    <option value="group">@lang('cpanel/clients-page.groups')</option>
                </select>
            </div>
        </div>

        <div class="form-group transferTo transClient" id="transClient" style="margin-top:10px;clear:both;">
            <label class="col-sm-4 control-label">@lang('cpanel/clients-page.client'):</label>
            <div class="col-sm-8">
                <div id="search" class="navbar-form-custom">
                    <div class="form-group">
                        <input type="text" placeholder="Search Client" class="form-control typeahead" name="transfer-search" id="transfer-search" style="width: 170px;height: 35px;margin-top: 5px;border: 1px solid #e5e6e7;border-radius: 1px;" autocomplete="off"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group transferTo transGroup" id="transGroup" style="margin-top:10px;clear:both;">
            <label class="col-sm-4 control-label">@lang('cpanel/clients-page.groups'):</label>
            <div class="col-sm-8">
                <div id="search" class="navbar-form-custom">
                    <div class="form-group">
                        <input type="text" placeholder="Search Group" class="form-control grouptypeahead" name="transfer-search" id="transfer-search" style="width: 170px;height: 35px;margin-top: 5px;border: 1px solid #e5e6e7;border-radius: 1px;" autocomplete="off"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group class freason" style="margin-top:10px;clear:both;">
            <label class="col-sm-4 control-label">@lang('cpanel/clients-page.reason'):</label>
            <div class="col-sm-11 m-l-2 txareawidth">
                <textarea
                    class="form-control autoExpand "
                    rows="1"
                    data-min-rows='1'
                    id="fund_reason"
                    name="fund_reason"
                    required
                    style="resize:none;"
                 ></textarea>
             </div>
         </div>

        <button type="button" class="btn btn-w-m btn-primary pull-right funds-add m-b-2" style="margin-top: 5px;">
            @lang('cpanel/clients-page.save')
        </button>
    </div>
</div>

<!-- AUTHORIZER -->
<dialog-modal id='authorizerModal' size="modal-sm" data-backdrop="static">
    <template slot="modal-title">
        @lang('cpanel/clients-page.authorization')
    </template>
    <template slot="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="control-label">@lang('cpanel/clients-page.authorizer'):</label>
                </div>
                <div class="form-group">
                    <multiselect class="custom-select" id="authorizer" :options="authorizers" v-model="authorizerFrm.authorizer" :close-on-select="true" label="fullname" track-by="id"></multiselect>
                </div>
                <div class="form-group">
                    <label class="control-label">@lang('cpanel/clients-page.password'):</label>
                </div>
                <div class="form-group">
                    <input type="password" id="password" v-model="authorizerFrm.password" class="form-control" required>
                </div>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-primary' @click="saveAuthorizer" data-dismiss="modal">@lang('cpanel/clients-page.proceed')</button>
        <button type="button" class="btn btn-white" data-dismiss="modal">@lang('cpanel/clients-page.cancel')</button>
    </template>
</dialog-modal>


<!-- EXTENSION FORM -->
<dialog-modal id='extensionform' size="modal-lg" data-backdrop="static">
    <template slot="modal-title">
        @lang('cpanel/clients-page.consolidated-general-application-form')
    </template>
    <template slot="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <h3>I. @lang('cpanel/clients-page.application-information')</h3>
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.months'):</label>
                            <input class="form-control m-b" id="months_req" v-model="extensionFrm.months_req" readonly name="months_req">
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.accreditation-number'):</label>
                            <input class="form-control m-b" id="acc_num" v-model="extensionFrm.acc_num" name="acc_num" readonly>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.authorized-representative'):</label>

                            <input class="form-control m-b" id="auth_rep" v-model="extensionFrm.auth_rep" name="auth_rep" readonly v-if="(is_rush) || (branch == 2)">

                            <select class="form-control" v-model="extensionFrm.auth_rep" id="auth_rep2" name="auth_rep" v-if="(!is_rush) && ((branch == 1) || (branch == 3))">
                                <option selected="selected" value="BORBON, MORIELLE G.">BORBON, MORIELLE G.</option>
                                <option value="RANADA, RICHARD B.">RANADA, RICHARD B.</option>
                            </select>
                        </div>
                    </div>
<!--                     <div class="col-lg-7">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">Reason:</label>
                                     <select class="form-control" v-model="extensionFrm.reason_type" id="reason_type" name="reason_type">
                                        <option selected value="pleasure">Pleasure</option>
                                        <option value="health">Health</option>
                                        <option value="business">Business</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label v-if="extensionFrm.reason_type == 'pleasure' || extensionFrm.reason_type == 'health'"> <input  type="checkbox" id="vs_1"> With Valid Special Study Permit </label>
                                <label v-else-if="extensionFrm.reason_type == 'business'"> <input type="checkbox" id="vs_3"> With Valid Provisional Work Permit </label>
                                <label v-else-if="extensionFrm.reason_type == 'others'"> <input type="checkbox" id="vs_4"> With Valid Limited Work Permit </label>
                            </div>
                        </div>
                    </div> -->
                </div>
<!--                 <div class="row">
                    <div v-if="extensionFrm.reason_type == 'others'" class="col-lg-12">
                        <label class="control-label">If Others, please specify:</label>
                        <input class="form-control m-b" id="others" type="text" v-model="extensionFrm.others" name="others">
                    </div>

                </div> -->

                <div class="row">

<!--                     <div class="col-lg-7">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">Reason:</label>
                                     <select class="form-control" v-model="extensionFrm.reason_type" id="reason_type" name="reason_type">
                                        <option selected value="pleasure">Pleasure</option>
                                        <option value="health">Health</option>
                                        <option value="business">Business</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label v-if="extensionFrm.reason_type == 'pleasure' || extensionFrm.reason_type == 'health'"> <input  type="checkbox" id="vs_1"> With Valid Special Study Permit </label>
                                <label v-else-if="extensionFrm.reason_type == 'business'"> <input type="checkbox" id="vs_3"> With Valid Provisional Work Permit </label>
                                <label v-else-if="extensionFrm.reason_type == 'others'"> <input type="checkbox" id="vs_4"> With Valid Limited Work Permit </label>
                            </div>
                        </div>
                    </div> -->
                </div>
                <h3>II. @lang('cpanel/clients-page.personal-info')</h3>
                <div class="row">
                    <div class="col-lg-4">
                        <div id="frm_lname" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.lname'):</label>
                            <input class="form-control m-b" id="lname" readonly type="text" v-model="extensionFrm.lname" name="lname">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div id="frm_fname" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.fname'):</label>
                            <input class="form-control m-b" id="fname" readonly type="text" v-model="extensionFrm.fname" name="fname">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div id="frm_mname" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.mname'):</label>
                            <input class="form-control m-b" id="mname" readonly type="text" v-model="extensionFrm.mname" name="mname">
                        </div>
                    </div>
<!--                     <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Alias:</label>
                            <input class="form-control m-b" id="alias" type="text" v-model="extensionFrm.alias" name="alias">
                        </div>
                    </div> -->
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div id="frm_nationality" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.citizenship'):</label>
                            <input class="form-control m-b" id="nationality" type="text" v-model="extensionFrm.nationality" readonly name="nationality">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div id="frm_birthcountry" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.country-of-birth'):</label>
                            <input class="form-control m-b" id="birthcountry" type="text" v-model="extensionFrm.birthcountry" readonly name="birthcountry">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div id="frm_bday" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.date-of-birth'):</label>
                            <input class="form-control m-b" id="bday" type="text" readonly v-model="extensionFrm.bday" name="bday">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div id="frm_gender" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.gender'):</label>
                            <input class="form-control m-b" id="gender" type="text" readonly v-model="extensionFrm.gender" name="gender">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div id="frm_cstatus" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.cstatus'):</label>
                            <input class="form-control m-b" id="cstatus" type="text" readonly v-model="extensionFrm.cstatus" name="cstatus">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div id="frm_height" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.height'):</label>
                            <input class="form-control m-b" id="height" type="text" readonly v-model="extensionFrm.height" name="height">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div id="frm_weight" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.weight'):</label>
                            <input class="form-control m-b" id="weight" type="text" readonly v-model="extensionFrm.weight" name="weight">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div id="frm_address" class="form-group">
                            <label class="control-label">@lang('cpanel/clients-page.add'):</label>                          
                            <input class="form-control m-b" readonly id="address" type="text" v-model="extensionFrm.address" name="address" readonly>
                        </div>
                    </div>
                </div>
<!--           <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="control-label">Mobile Number:</label>
                            <input class="form-control m-b" id="mobile_no" type="number" v-model="extensionFrm.mobile_no" name="mobile_no">
                        </div>
                    </div>
                </div> -->

                

                <h3>TRAVEL INFORMATION</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <label class="control-label">@lang('cpanel/clients-page.passport-num'):</label>
                        <div id="frm_passport" class="form-group">
                            <input class="form-control m-b" readonly id="passport" type="text" readonly v-model="extensionFrm.passport" name="passport">
                        </div>
                    </div>
                     <div class="col-lg-6">
                        <label class="control-label">@lang('cpanel/clients-page.passport') (@lang('cpanel/clients-page.valid-until')):</label>
                        <div id="frm_passport_exp" class="form-group">
                            <div class="form-group">
                                <the-mask  id="passport_exp" readonly v-model="extensionFrm.passport_exp" class="input-sm form-control" name="passport_exp"  placeholder="YYYY-MM-DD"  mask="####-##-##">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <label class="control-label">@lang('cpanel/clients-page.date-of-latest-arrival'):</label>
                        <div id="frm_latest_arrival" class="form-group">
                            <div class="form-group">
                                <the-mask  id="latest_arrival" v-model="extensionFrm.latest_arrival" class="input-sm form-control" name="latest_arrival"  placeholder="YYYY-MM-DD"  mask="####-##-##">
                            </div>
                        </div>
                    </div>
                     <div class="col-lg-6">
                        <label class="control-label">@lang('cpanel/clients-page.latest-tourist-ext') (@lang('cpanel/clients-page.valid-until')):</label>
                        <div id="frm_ext_valid_date" class="form-group">
                            <div class="form-group">
                                <the-mask  id="ext_valid_date" v-model="extensionFrm.ext_valid_date" class="input-sm form-control" name="ext_valid_date"  placeholder="YYYY-MM-DD"  mask="####-##-##">
                            </div>
                        </div>
                    </div>
                </div>

                <h3>III. @lang('cpanel/clients-page.mode-of-transaction')</h3>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <input class="form-control m-b" readonly id="express_regular" type="text" v-model="extensionFrm.express_regular" name="express_regular" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2 align-right">
                        <label class="control-label">@lang('cpanel/clients-page.issue-date'):</label>
                    </div>
                    <div class="col-lg-3">
                        <div id="frm_issue_date" class="form-group">
                            <div class="form-group">
                                <the-mask  id="issue_date" v-model="extensionFrm.issue_date" class="input-sm form-control" name="issue_date"  placeholder="YYYY-MM-DD"  mask="####-##-##">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-primary' @click="exportPdf">@lang('cpanel/clients-page.export')</button>
        <button type="button" class="btn btn-white" data-dismiss="modal">@lang('cpanel/clients-page.cancel')</button>
    </template>
</dialog-modal>

<modal id='dialogRemoveDocs' size="modal-sm">

    <template slot="modal-title">
        <h3> @lang('cpanel/clients-page.delete-document') </h3>
    </template>

    <template slot="modal-body">

        @lang('cpanel/clients-page.prompt-del-document')

    </template>

    <template slot="modal-footer">
        <button class='btn btn-default' data-dismiss="modal">@lang('cpanel/clients-page.cancel')</button>
        <button class='btn btn-primary' data-dismiss="modal" @click="deleteDoc">@lang('cpanel/clients-page.proceed')</button>
    </template>

</modal>