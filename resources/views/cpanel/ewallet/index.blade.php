@extends('cpanel.layouts.master')

@section('title', 'E-wallet')

@push('styles')
	{!! Html::style('cpanel/plugins/iCheck/custom.css') !!}
	{!! Html::style('cpanel/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}
	{!! Html::style(mix('css/ewallet.css','cpanel')) !!}
	{!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.css") !!}
	{!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css") !!}
	{!! Html::style("cpanel/plugins/nouslider/jquery.nouislider.css") !!}

@endpush

@push('scripts')
   	{!! Html::script('cpanel/plugins/iCheck/icheck.min.js')!!}
	{!! Html::script("cpanel/plugins/jquery-ui/jui-draggable.min.js") !!}
    {!! Html::script(mix('js/ewallet.js','cpanel')) !!}
@endpush

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>E-wallet</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>E-wallet</strong>
                </li>
            </ol>
        </div>
    </div>

<div id="ewallet-root">
	@include('cpanel.ewallet.modals')
	<div class="wrapper wrapper-content" >
		<div class="row">
	        <h1 class="pull-right m-r-lg m-t-lg dt-title"><b>Ewallet</b></h1>
	        <div class="col-md-2">
	            <a data-toggle="tab" href="#etransaction-pending" @click='showTransactionByStatus("Pending")'>
	                <div class="ibox-title">
	                    <h3>Pending
	                        <span class="pull-right label label-warning"  v-cloak>
	                            @{{pending_count}}
	                        </span>
	                    </h3>
	                </div>
	            </a>
	        </div>
	        <div class="col-md-2">
	            <a data-toggle="tab" href="#etransaction-approved"  @click='showTransactionByStatus("Processing")'>
	                <div class="ibox-title">
	                    <h3>Processing
	                        <span class="pull-right label label-info"  v-cloak>
	                            @{{processing_count}}
	                        </span>
	                    </h3>
	                </div>
	            </a>
	        </div>
	        <div class="col-md-2">
	            <a data-toggle="tab" href="#etransaction-rejected"  @click='showTransactionByStatus("Cancelled")'>
	                <div class="ibox-title">
	                    <h3>Cancelled
	                        <span class="pull-right label label-danger" v-cloak>
	                            @{{cancelled_count}}
	                        </span>
	                    </h3>
	                </div>
	            </a>
	        </div>
	        <div class="col-md-2">
	            <a data-toggle="tab" href="#etransaction-rejected"  @click='showTransactionByStatus("Completed")'>
	                <div class="ibox-title">
	                    <h3>Completed
	                        <span class="pull-right label label-default" v-cloak>
	                            @{{completed_count}}
	                        </span>
	                    </h3>
	                </div>
	            </a>
	        </div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="tab-content">
					<div class="ibox float-e-margins">
		                <div class="ibox-title">
		                    <h5>@{{status_seen}} Ewallet Transactions </h5>
		                     <div class="ibox-tools">
	                            <a class="collapse-link">
	                                <i class="fa fa-chevron-up"></i>
	                            </a>
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
	                                <i class="fa fa-filter"></i>
	                            </a>
	                            <div class="dropdown-menu dropdown-user p-sm">
	                            	<h3 class='text-center'>Filters</h3>
	                            	<b>Type :</b>
										<div class="i-checks"><label> <input v-model='filterType'  type="radio" value="all" name="filter-type"> <i></i> All </label></div>
										<div class="i-checks"><label> <input v-model='filterType' type="radio" value="deposit" name="filter-type"> <i></i> Deposit </label></div>
										<div class="i-checks"><label> <input v-model='filterType' type="radio" value="withdraw" name="filter-type"> <i></i> Withdraw </label></div>
										<div class="i-checks"><label> <input v-model='filterType' type="radio" value="transfer" name="filter-type"> <i></i> Transfer </label></div>
									<!-- <b>Status :</b>
										<div class="i-checks"><label> <input v-model='filterStatus'  type="radio" value="all" name="filter-status"> <i></i> All </label></div>
										<div class="i-checks"><label> <input v-model='filterStatus' type="radio" value="pending" name="filter-status"> <i></i> Pending </label></div>
										<div class="i-checks"><label> <input v-model='filterStatus' type="radio" value="processing" name="filter-status"> <i></i> On Process </label></div>
										<div class="i-checks"><label> <input v-model='filterStatus' type="radio" value="cancelled" name="filter-status"> <i></i> Cancelled </label></div>
										<div class="i-checks"><label> <input v-model='filterStatus' type="radio" value="completed" name="filter-status"> <i></i> Completed </label></div> -->
	                            </div>
	                            <a class="fullscreen-link">
	                                <i class="fa fa-expand"></i>
	                            </a>
	                        </div>

		                </div>
		                <div class="ibox-content p-sm">
		                	<div class="table-responsive">
		                		
				                <table class="table table-bordered table-hover dt-ewallet"  data-order='[[ 0, "desc" ]]'>
				                    <thead>
				                        <tr>
				                            <th>Date and Time</th>
				                            <th>Ref. No.</th>
				                            <th>Type</th>
				                            <th>Amount</th>
				                            <th>Description</th>
				                            <th>User - Processor</th>
				                        </tr>
				                    </thead>
				                    <tbody>
				                        <tr 
				                        	v-if='showFilterType(transaction.type)' 
				                        	v-for='transaction in transactions' 
				                        	:key='transaction.id' 
				                        	class='mouse-cursor'
				                        	:class='textColor(transaction.status)'
				                        	
				                        >
				                            <td v-cloak @click='showModal(transaction.id)'>@{{transaction.updated_at}} 
				                            	<!-- <span 
				                            		class="label" 
				                            		:class='statusColor(transaction.status)'
													data-toggle='tooltip'
													:title='transaction.status=="Cancelled"?transaction.reason:""'
				                            		>
				                            		@{{transaction.status}}
												</span> -->
											</td>
				                            <td @click='showModal(transaction.id)' v-cloak>@{{transaction.ref_number}}</td>
				                            <td @click='showModal(transaction.id)' v-cloak>@{{transaction.type}}</td>
				                            <td @click='showModal(transaction.id)' v-cloak>@{{transaction.amount | currency}}</td>
				                            <td @click='showModal(transaction.id)' v-cloak>@{{transaction.description}}</td>
				                            <td v-cloak>
				                            	<a :href='"/users/"+transaction.user.id'>
				                            		@{{transaction.user.first_name}} @{{transaction.user.last_name}}
				                            	</a>
				                            	<br> 
				                            	<span class='badge badge-primary' v-if='transaction.processor'>Processor: @{{transaction.processor.first_name}} @{{transaction.processor.last_name}}
				                            	</span>
			                            	</td>
				                        </tr>
				                    </tbody>
				                </table>
		                	</div>
				               
		                </div>
		            </div>
				</div>


			</div>
		</div>
	</div>
</div>
@endsection
