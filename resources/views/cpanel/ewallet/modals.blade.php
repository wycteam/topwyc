<!-- Upload Modal -->
<modal :size="imgDS?'modal-md':'modal-sm'" id='depositModal'>
    <template slot="modal-title">
        <h3>
            Deposit Slip
        </h3>
    </template>
    <template slot="modal-body">
        <div class='container-fluid text-center'>
            <div class="col-lg-12">
                <div id="doc-container">
            
                </div>
                <div class="range-slider" v-show='showZoomable'>
                  <input class="range-slider__range" type="range"  step='0.1' v-model='baseZoom' min="1" max="5">
                </div>
            </div>
           <!--  <div class="col-xs-6 ">
                <img v-if='imgDS' :src="imgDS" class="img-responsive">
            </div> -->
            <h4 v-if='!imgDS'>No Deposit Slip Uploaded</h4>
            
        </div>
    </template>
    <template slot="modal-footer">
        <div class="form-group text-center">
            <select class='form-control form-group form-control-auto-width' v-model='newStatus'>
                <option>Processing</option>
                <option>Completed</option>
                <option>Cancelled</option>
            </select>
            <div class="form-group" v-if='newStatus == "Cancelled"'>
                <label>Reason:</label>
                <textarea type="text" v-model='reason' class='form-control ' placeholder="Reason" ></textarea>
            </div>
            <button class='btn btn-primary' @click='changeStatus()'>Change Status</button>
        </div>
    </template>
</modal>

<!-- Upload Modal -->
<modal size="modal-sm" id='decisionModal'>
    <template slot="modal-title">
        <h3>
            Update Status?
        </h3>
    </template>
    <template slot="modal-body">
        <div class="container-fluid text-center">
            <select class='form-control form-group' v-model='newStatus'>
                <option v-if='status!="Processing"'>Processing</option>
                <option>Completed</option>
                <option>Cancelled</option>
            </select>
            <div class="form-group"  v-if='newStatus == "Cancelled"'>
                <label>Reason:</label>
                <textarea type="text" v-model='reason' class='form-control ' placeholder="Reason" v-if='newStatus == "Cancelled"'></textarea>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
        <div class="form-group text-center">
            <button class='btn btn-primary' @click='changeStatus()'>Change Status</button>
        </div>
    </template>
</modal>