@extends('cpanel.layouts.master')

@section('title', 'E-wallet Pool')

@push('styles')
	{!! Html::style('cpanel/plugins/iCheck/custom.css') !!}
	{!! Html::style('cpanel/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}
	{!! Html::style(mix('css/ewallet.css','cpanel')) !!}
	{!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.css") !!}
	{!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css") !!}
	{!! Html::style("cpanel/plugins/nouslider/jquery.nouislider.css") !!}

@endpush

@push('scripts')
   	{!! Html::script('cpanel/plugins/iCheck/icheck.min.js')!!}
	{!! Html::script("cpanel/plugins/jquery-ui/jui-draggable.min.js") !!}
@endpush

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>E-wallet Pool</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>E-wallet Pool</strong>
                </li>
            </ol>
        </div>
    </div>

<div id="ewallet-root">
	@include('cpanel.ewallet.modals')
	<div class="wrapper wrapper-content" >
		<div class="row">
			<div class="col-md-12">
				<div class="tab-content">
					<div class="ibox float-e-margins">
		                <div class="ibox-title">
		                    <h5>Ewallet Pool</h5>

		                    <div class="ibox-tools">
	                            <a class="collapse-link">
	                                <i class="fa fa-chevron-up"></i>
	                            </a>
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
	                                <i class="fa fa-filter"></i>
	                            </a>
	                            <div class="dropdown-menu dropdown-user p-sm">
	                            	<h3 class='text-center'>Filters</h3>
	                            	<b>Type :</b>
										<div class="i-checks"><label> <input v-model='filterType'  type="radio" value="all" name="filter-type"> <i></i> All </label></div>
										<div class="i-checks"><label> <input v-model='filterType' type="radio" value="deposit" name="filter-type"> <i></i> Deposit </label></div>
										<div class="i-checks"><label> <input v-model='filterType' type="radio" value="withdraw" name="filter-type"> <i></i> Withdraw </label></div>
										<div class="i-checks"><label> <input v-model='filterType' type="radio" value="transfer" name="filter-type"> <i></i> Transfer </label></div>
									<b>Status :</b>
										<div class="i-checks"><label> <input v-model='filterStatus'  type="radio" value="all" name="filter-status"> <i></i> All </label></div>
										<div class="i-checks"><label> <input v-model='filterStatus' type="radio" value="pending" name="filter-status"> <i></i> Pending </label></div>
										<div class="i-checks"><label> <input v-model='filterStatus' type="radio" value="processing" name="filter-status"> <i></i> On Process </label></div>
										<div class="i-checks"><label> <input v-model='filterStatus' type="radio" value="cancelled" name="filter-status"> <i></i> Cancelled </label></div>
										<div class="i-checks"><label> <input v-model='filterStatus' type="radio" value="completed" name="filter-status"> <i></i> Completed </label></div>
	                            </div>
	                            <a class="fullscreen-link">
	                                <i class="fa fa-expand"></i>
	                            </a>
	                        </div>

				            <h1 class='text-center fa-4x'>P {{number_format($total_pool,2)}}</h1>
		                </div>
		                <div class="ibox-content p-sm">
		                	<div class="table-responsive">
		                		
				                <table class="table table-bordered table-hover dt-ewallet"  data-order='[[ 0, "desc" ]]'>
				                    <thead>
				                        <tr>
				                            <th>Date and Time</th>
				                            <th>Tracking No.</th>
				                            <th>On-hold(Seller)</th>
				                            <th>Amount</th>
				                            <th>Seller</th>
				                            <th>Buyer</th>
				                            <th>Status</th>
				                        </tr>
				                    </thead>
				                    <tbody>
										@foreach($unfinished_orders as $order)
										<tr>
											<td>@if($order->shipped_date)
												Until : <b>{{$order->shipped_date->addDays(7)}}</b>
												@endif
											</td>
											<td>{{$order->tracking_number}}</td>
				                            <td>
				                            	@if($order->status == 'In Transit' ||($order->status == 'Returned' && $order->returned_received_date == ''))
													P{{ $order->vat + $order->shipping_fee + $order->charge }}
				                            	@endif
				                            </td>
				                            <td>P{{$order->subtotal}}</td>
				                            <td>{{$order->product->user->full_name}}</td>
				                            <td>{{$order->order->user->full_name}}</td>
				                            <td>{{$order->status}}</td>
										</tr>
										@endforeach
				                    </tbody>
				                    <!-- <tbody>
				                        <tr 
				                        	v-if='showFilterStatus(transaction.status) && showFilterType(transaction.type)' 
				                        	v-for='transaction in transactions' 
				                        	:key='transaction.id' 
				                        	class='mouse-cursor'
				                        	:class='textColor(transaction.status)'
				                        	@click='showModal(transaction.id)'
				                        >
				                            <td v-cloak>@{{transaction.updated_at}} 
				                            	<span 
				                            		class="label" 
				                            		:class='statusColor(transaction.status)'
													data-toggle='tooltip'
													:title='transaction.status=="Cancelled"?transaction.reason:""'
				                            		>
				                            		@{{transaction.status}}
												</span>
											</td>
				                            <td v-cloak>@{{transaction.ref_number}}</td>
				                            <td v-cloak>@{{transaction.type}}</td>
				                            <td v-cloak>@{{transaction.amount | currency}}</td>
				                            <td v-cloak>@{{transaction.description}}</td>
				                            <td v-cloak>
				                            	<a :href='"/users/"+transaction.user.id'>@{{transaction.user.first_name}} @{{transaction.user.last_name}}</a> 
				                            	<span class='badge badge-primary' v-if='transaction.processor'>@{{transaction.processor.first_name}} @{{transaction.processor.last_name}}</span>
			                            	</td>
				                        </tr>
				                    </tbody> -->
				                </table>
		                	</div>
				               
		                </div>
		            </div>
				</div>


			</div>
		</div>
	</div>
</div>
@endsection
