@extends('cpanel.layouts.master')

@section('title', 'Shipments List')

@push('styles')
    {!! Html::style(mix('css/sales.css','cpanel')) !!}
    {!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Shipments</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Shipments</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content" id="shipmentpage">
        <div class="row">
            <div class="col-lg-12">
                <div>
                    <div class="tabs-container">
                        <h1 class="pull-right m-r-lg m-t-lg dt-title"><b>List of Shipments</b></h1>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1">
                                    <div class="ibox-title">
                                        <span class="label label-success">All</span>
                                        <h3>Shipments</h3>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content ">
                            <div id="tab-1" class="tab-pane active ibox">
                                 <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">

                                        <div class="filter-container well">
                                            <h4>Filter By:</h4>

                                            <form class="form-inline">
                                                <div class="form-check form-check-inline form-group">
                                                    <input type="radio" class="form-check-input" name="filter" checked @click="getproduct">
                                                    <label class="form-check-label">All Shipments</label>
                                                </div>

                                                <div class="form-check form-check-inline form-group margin-20">
                                                    <input type="radio" class="form-check-input" name="filter" @click="filter = 'Daily'">
                                                    <label class="form-check-label">Daily</label>
                                                </div>

                                                <div class="form-check form-check-inline form-group margin-20">
                                                    <input type="radio" class="form-check-input" name="filter" @click="filter = 'Monthly'">
                                                    <label class="form-check-label">Monthly</label>
                                                </div>

                                                <div class="form-check form-check-inline form-group margin-20">
                                                    <input type="radio" class="form-check-input" name="filter" @click="filter = 'DateRange'">
                                                    <label class="form-check-label">Date Range</label>
                                                </div>
                                            </form>

                                            <div v-if="filter == 'Daily'" class="filter-form">
                                                <form id="daily-form" class="form-inline" @submit.prevent="submitFilter">
                                                    <div class="form-group">
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="date">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary">
                                                            <strong>Filter</strong>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                            <div v-if="filter == 'Monthly'" class="filter-form">
                                                <form id="monthly-form" class="form-inline" @submit.prevent="submitFilter">
                                                    <div class="form-group">
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="date">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary">
                                                            <strong>Filter</strong>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                            <div v-if="filter == 'DateRange'" class="filter-form">
                                                <form id="date-range-form" class="form-inline" @submit.prevent="submitFilter">
                                                    <div class="form-group">
                                                        <div class="input-daterange input-group" id="datepicker">
                                                            <input type="text" class="input-sm form-control" name="start" />
                                                            <span class="input-group-addon">to</span>
                                                            <input type="text" class="input-sm form-control" name="end" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary">
                                                            <strong>Filter</strong>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-shipments" >
                                                <thead>
                                                    <tr>
                                                        <th>Ref No.</th>
                                                        <th>Order No.</th>
                                                        <th>Total Price</th>
                                                        <th>WATAF Cost</th>
                                                        <th>2GO Cost</th>
                                                        <!-- <th>Discrepancy</th> -->
                                                        <th>Profit</th>
                                                        <!-- <th>Actual Profit</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @include('cpanel.includes.shipments.modals')
                                                    <tr v-for="item in orders.data"
                                                        :item="item" 
                                                        :key="item.id"
                                                        is="orderlist"
                                                        v-if="item.awb_id != ''">
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="5" style="text-align:right">
                                                            Total:
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    {!! Html::script(mix('js/shipments.js','cpanel')) !!}
    {!! Html::script('cpanel/plugins/dataTables/datatables.min.js') !!}
    {!! Html::script('cpanel/plugins/datapicker/bootstrap-datepicker.js') !!}
@endpush
