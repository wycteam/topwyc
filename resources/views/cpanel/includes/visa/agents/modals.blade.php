<modal id='add-new-agent-modal' size="modal-md">

	<template slot="modal-title">
        <h3>@{{(translation) ? translation['add-new-shop'] : 'Add New Shop'}}</h3>
    </template>

    <template slot="modal-body">

    	<add-new-agent ref="addnewagent"></add-new-agent>

    </template>

</modal>

<modal id='edit-agent-modal' size="modal-md">

	<template slot="modal-title">
        <h3>@{{(translation) ? translation['edit-shop'] : 'Edit Shop'}}</h3>
    </template>

    <template slot="modal-body">

    	<edit-agent ref="editagent"></edit-agent>

    </template>

</modal>