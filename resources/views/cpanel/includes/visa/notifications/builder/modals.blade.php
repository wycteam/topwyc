<modal id='add-report-type-modal' size="modal-md">

	<template slot="modal-title">
        <h3>Add Report Type</h3>
    </template>

    <template slot="modal-body">

    	<add-report-type ref="addreporttyperef"></add-report-type>

    </template>

</modal>

<modal id='update-report-type-modal' size="modal-md">

	<template slot="modal-title">
        <h3>Update Report Type</h3>
    </template>

    <template slot="modal-body">

    	<update-report-type ref="updatereporttyperef" :reporttypeid="selectedReportTypeId"></update-report-type>

    </template>

</modal>

<modal id='delete-report-type-modal' size="modal-md">

	<template slot="modal-title">
        <h3>Delete Report Type</h3>
    </template>

    <template slot="modal-body">

    	<delete-report-type ref="deletereporttyperef" :reporttypeid="selectedReportTypeId"></delete-report-type>

    </template>

</modal>

<modal id='add-document-modal' size="modal-md">

	<template slot="modal-title">
        <h3>Add Document</h3>
    </template>

    <template slot="modal-body">

    	<add-document ref="adddocumentref"></add-document>

    </template>

</modal>

<modal id='update-document-modal' size="modal-md">

	<template slot="modal-title">
        <h3>Update Document</h3>
    </template>

    <template slot="modal-body">

    	<update-document ref="updatedocumentref" :documentid="selectedDocumentId"></update-document>

    </template>

</modal>

<modal id='delete-document-modal' size="modal-md">

	<template slot="modal-title">
        <h3>Delete Document</h3>
    </template>

    <template slot="modal-body">

    	<delete-document ref="deletedocumentref" :documentid="selectedDocumentId"></delete-document>

    </template>

</modal>