<modal id='export-excel-modal' size="modal-md">

    <template slot="modal-title">
        <h3>Export Excel</h3>
    </template>

    <template slot="modal-body">

        <export-excel :groupid="groupId" :radiovalue="radioValue" ref="exportexcelref"></export-excel>

    </template>

</modal>