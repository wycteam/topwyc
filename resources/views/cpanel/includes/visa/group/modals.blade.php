<modal id='edit-address-modal' size="modal-md">

	<template slot="modal-title">
        <h3>@lang('cpanel/groups-page.edit-address')</h3>
    </template>

    <template slot="modal-body">

    	<edit-address :groupid="groupId" ref="editaddressref"></edit-address>

    </template>

</modal>

<modal id='add-new-member-modal' size="modal-md">

	<template slot="modal-title">
        <h3>@lang('cpanel/groups-page.new-member')</h3>
    </template>

    <template slot="modal-body">

    	<add-new-member :groupid="groupId" :branchid="branchId" ref="addnewmemberref"></add-new-member>

    </template>

</modal>

<modal id='add-new-service-modal' size="modal-lg">

	<template slot="modal-title">
        <h3>@lang('cpanel/groups-page.new-service')</h3>
    </template>

    <template slot="modal-body">

    	<add-new-service :groupid="groupId" ref="addnewserviceref"></add-new-service>

    </template>

</modal>

<modal id='add-funds-modal' size="modal-md">

	<template slot="modal-title">
        <h3>@lang('cpanel/groups-page.add-funds')</h3>
    </template>

    <template slot="modal-body">

    	<add-fund :groupid="groupId" ref="addfundref"></add-fund>

    </template>

</modal>

<modal id='delete-member-modal' size="modal-md">

	<template slot="modal-title">
        <h3>@lang('cpanel/groups-page.delete-member')</h3>
    </template>

    <template slot="modal-body">

    	<delete-member :groupid="groupId" ref="deletememberref"></delete-member>

    </template>

</modal>

<modal id='transfer-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/groups-page.transfer')</h3>
    </template>

    <template slot="modal-body">

        <transfer :groupid="groupId" ref="transferref"></transfer>

    </template>

</modal>

<modal id='edit-service-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/groups-page.edit-service')</h3>
    </template>

    <template slot="modal-body">

        <edit-service :groupid="groupId" ref="editserviceref"></edit-service>

    </template>

</modal>

<modal id='add-quick-report-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/groups-page.add-quick-report')</h3>
    </template>

    <template slot="modal-body">

        <!-- <multiple-quick-report :groupreport="true" :groupid="groupId" :clientsid="quickReportClientsId" :clientservices="quickReportClientServicesId" :trackings="quickReportTrackings" ref="multiplequickreportref"></multiple-quick-report> -->

        <multiple-quick-report-v2 ref="multiplequickreportv2ref" :groupreport="true" :groupid="groupId" :clientsid="quickReportClientsId" :clientservices="quickReportClientServicesId" :trackings="quickReportTrackings"></multiple-quick-report-v2>

    </template>

</modal>

<modal id='client-services-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/groups-page.client-services')</h3>
    </template>

    <template slot="modal-body">

        <client-services :groupid="groupId" :members="members" ref="clientservicesref"></client-services>

    </template>

</modal>

<!-- Change vice leader to a main leader or a member -->
<modal id='vice-leader-modal' size="modal-md">

    <template slot="modal-title">
        <h3>@lang('cpanel/groups-page.change-member-type')</h3>
    </template>

    <template slot="modal-body">

        <member-type :groupid="groupId" ref="changetyperef"></member-type>

    </template>

</modal>

<modal id='view-report-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/groups-page.reports')</h3>
    </template>

    <template slot="modal-body">

        <view-report :report_view="report_view" :report_id="report_id" ref="viewreportref"></view-report>

    </template>

</modal>