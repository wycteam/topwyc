<div class="table-responsive">

    <table class="table table-bordered table-striped table-hover dt-transactions">
        <thead>
            <tr>
                <th>@lang('cpanel/groups-page.amount')</th>
                <th>@lang('cpanel/groups-page.date-time')</th>
                <th>@lang('cpanel/groups-page.type')</th>

            </tr>
        </thead>
        <tbody>

        	<tr v-for="trans in allTransactions">
        		<td>@{{ trans.amount }}</td>
        		<td><span style="display: none;">@{{ trans.id }}</span>@{{ trans.log_date }}</td>
                <td>@{{ trans.type }}</td>
        	</tr>

        </tbody>
    </table>

        <button v-if="limit>0" type="button" class="btn btn-w-m btn-primary" v-cloak @click="fetchTransactions(0)">@lang('cpanel/groups-page.load-all')</button>
        <div v-show="loading" class="ibox">
            <div class="ibox-content">
                <div class="spiner-example">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                </div>
            </div>
        </div>

</div>