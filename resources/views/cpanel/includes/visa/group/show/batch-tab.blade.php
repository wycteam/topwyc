<div class="table-responsive vld-parent" >

	<div class="row" v-if="details">
		<div class="col-md-12" v-if="details.group.check_shop < 1 || details.group.is_shop > 0">
			<button type="button" class="btn btn-link btn-link-primary" data-toggle="modal" data-target="#add-new-member-modal" v-show="selpermissions.newMember==1 || setting == 1">
				@lang('cpanel/groups-page.new-member')
			</button>
			<button type="button" class="btn btn-link btn-link-primary" data-toggle="modal" data-target="#add-new-service-modal" v-show="selpermissions.addNewService==1 || setting == 1">
				@lang('cpanel/groups-page.add-new-service')
			</button>
			<button type="button" class="btn btn-link btn-link-primary" data-toggle="modal" data-target="#add-funds-modal" v-show="selpermissions.addFunds==1 || setting == 1">
				@lang('cpanel/groups-page.add-funds')
			</button>
			<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#client-services-modal" v-show="selpermissions.writeReport==1 || setting == 1" @click="fetchMembers">
				@lang('cpanel/groups-page.write-report')
			</button>
			<button v-show="!service_editor && page_wizard==1" @click="checkEdit" type="button" class="btn btn-link btn-link-primary" v-show="selpermissions.addFunds==1 || setting == 1">
				@lang('cpanel/groups-page.edit-service')
			</button>

			<button v-show="service_editor && page_wizard==1" style="color:orange !important" type="button" @click="checkEdit" class="btn btn-link " v-show="selpermissions.addFunds==1 || setting == 1">
				@lang('cpanel/groups-page.cancel-edit')
			</button>
		</div>
	</div>
	<div style="height:20px;clear:both;"></div>
    <!--<table class="table table-bordered table-striped table-hover dt-gbatch">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>@lang('cpanel/groups-page.date')</th>
                <th>@lang('cpanel/groups-page.total-service-cost')</th>
            </tr>
        </thead>
	</table>
	-->
	
	<batch-service ref="batchservices" :loading="loading" :translation="translation" :page_wizard="page_wizard" :group_id="groupId">

	</batch-service>
</div>