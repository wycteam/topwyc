<div class="table-responsive vld-parent">

	<div class="row">
		<div class="col-md-12">
			<button type="button" class="btn btn-link btn-link-primary" data-toggle="modal" data-target="#add-new-member-modal" v-show="selpermissions.newMember==1 || setting == 1">
				@lang('cpanel/groups-page.new-member')
			</button>
			<button type="button" class="btn btn-link btn-link-primary" data-toggle="modal" data-target="#add-new-service-modal" v-show="selpermissions.addNewService==1 || setting == 1">
				@lang('cpanel/groups-page.add-new-service')
			</button>
			<button type="button" class="btn btn-link btn-link-primary" data-toggle="modal" data-target="#add-funds-modal" v-show="selpermissions.addFunds==1 || setting == 1">
				@lang('cpanel/groups-page.add-funds')
			</button>

			<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#client-services-modal" v-show="selpermissions.writeReport==1 || setting == 1" @click="fetchMembers">
				@lang('cpanel/groups-page.write-report')
			</button>
			
			<button v-show="!service_editor && page_wizard==1" @click="checkEdit" type="button" class="btn btn-link btn-link-primary" v-show="selpermissions.addFunds==1 || setting == 1">
				@lang('cpanel/groups-page.edit-service')
			</button>

			<button v-show="service_editor && page_wizard==1" style="color:orange !important" type="button" @click="checkEdit" class="btn btn-link " v-show="selpermissions.addFunds==1 || setting == 1">
				@lang('cpanel/groups-page.cancel-edit')
			</button>
		</div>
	</div>
	<group-service ref="groupservices" :loading="loading" :translation="translation" :page_wizard="page_wizard" :group_id="groupId">

	</group-service>
	<!--
    <table class="table table-bordered table-striped table-hover dt-gservice">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>@lang('cpanel/groups-page.service-name')</th>
                <th>@lang('cpanel/groups-page.latest-date')</th>
                <th>@lang('cpanel/groups-page.total-service-cost')</th>
            </tr>
        </thead>
	</table>
	-->
</div>