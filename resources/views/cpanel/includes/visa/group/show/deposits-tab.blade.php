<div class="table-responsive">

    <table class="table table-bordered table-striped table-hover dt-deposits">
        <thead>
            <tr>
                <th>@lang('cpanel/groups-page.amount')</th>
                <th>@lang('cpanel/groups-page.date-time')</th>
            </tr>
        </thead>
        <tbody>

        	<tr v-for="deposit in deposits">
        		<td>@{{ deposit.amount }}</td>
        		<td><span style="display: none;">@{{ deposit.id }}</span>@{{ deposit.log_date }}</td>
        	</tr>

        </tbody>
    </table>

        <button v-if="limit>0" type="button" class="btn btn-w-m btn-primary" v-cloak @click="fetchDeposits(0)">Load all</button>
        <div v-show="loading" class="ibox">
            <div class="ibox-content">
                <div class="spiner-example">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                </div>
            </div>
        </div>

</div>