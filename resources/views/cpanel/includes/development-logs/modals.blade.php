<!--editUpdatesModal -->
<dialog-modal size="modal-lg" id='editUpdatesModal'>
    <template slot="modal-title">
        <h3>@{{modalTitle}}</h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group label-floating" :class="{ 'has-error': logForm.errors.has('title') }">
                    <label for="text">Title:</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        required
                        id="title" 
                        name="title"
                        v-model="logForm.title">
                    <p class="help-block" v-if="logForm.errors.has('title')" v-text="logForm.errors.get('title')"></p>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <label for="details">Body:</label>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <quill-editor
                    v-model="logForm.body">
                </quill-editor> 
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-lg-6">
                <div class="form-group label-floating" :class="{ 'has-error': logForm.errors.has('date') }">
                    <label for="date">Update Date:</label>
                    <form id="daily-form" class="form-inline">
                        <div class="form-group">
                            <div class="input-group date">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" id="date" @focus="getDate()" class="form-control" name="date">
                            </div>
                        </div>
                    </form>
                    <p class="help-block" v-if="logForm.errors.has('date')" v-text="logForm.errors.get('date')"></p>
                </div>
            </div>
            <div class="col-lg-6"> 
                <div class="form-group label-floating" :class="{ 'has-error': logForm.errors.has('version') }">
                    <label for="version">Version:</label>
                     <input 
                        type="text" 
                        class="form-control" 
                        required
                        id="version" 
                        name="version"
                        v-model="logForm.version"
                    >
                    <p class="help-block" v-if="logForm.errors.has('version')" v-text="logForm.errors.get('version')"></p>
                </div>
            </div>
        </div>
    </template><!-- end of modal-body -->
    <template slot="modal-footer">
        <button type="button" class="btn btn-w-m btn-primary" @click="doFunction()">Save</button>
        <button type="button" data-dismiss="modal" class="btn btn-w-m">Cancel</button>
    </template><!-- end of modal-body -->
</dialog-modal><!-- end of editUpdatesModal -->

<!-- deleteLogPrompt Modal -->
<dialog-modal size="modal-sm" id='deleteLogPrompt'>
    <template slot="modal-title">
        <h3>Delete Log</h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body">
        <p>Are you sure you want to delete this?</p>

    </template><!-- end of modal-body -->
    <template slot="modal-footer">
        <button type="button" class="btn btn-w-m btn-primary" @click="deleteLog()">Yes</button>
        <button type="button" data-dismiss="modal" class="btn btn-w-m btn-primary" >Cancel</button>
    </template>
</dialog-modal><!-- end of deleteLogPrompt Modal -->