<!-- View Document Modal -->
<modal size="modal-md" class="report-product-modal" id='viewDocument'>
    <template slot="modal-title">
        <h3>Review Document</h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body" class="product-details-set">
    	<div id="doc-container">
    		
    	</div>
    	<div class="range-slider">
		  <input class="range-slider__range" type="range"  step='0.1' v-model='baseZoom' min="1" max="5">
		</div>
  		 <!-- <div class="tiles">
	        <div data-scale="2"  :data-image="'/images/documents/'+selected.documentable_id+'/'+selected.name" class="tile">
	        	<div class="photo" :style='"background-image: url(/images/documents/"+selected.documentable_id+"/"+selected.name+");"'>
	        		
	        	</div>
	        </div>
  		</div> -->

    </template><!-- end of modal-body -->
</modal><!-- end of Product Order Modal -->

<!-- Expiration Date Modal -->
<modal size="modal-md" class="report-product-modal" id='setValidity'>
 	<template slot="modal-title">
        <h3></h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body" class="product-details-set">
    	<div class="form-group">
    		<label class="col-lg-3 control-label">Valid Until</label>
	        <div class="col-lg-9">
	        	<input 
	        	type="text" 
	        	class="form-control" 
	        	v-datepicker required 
	        	id="expires_at" 
	        	name="expires_at" 
	        	v-model="validity.expires_at"
	        	>
	        </div>
        </div>
    </template><!-- end of modal-body -->
    <template slot="modal-footer" class="product-details-set">
  		<button type="button" class="btn btn-w-m btn-primary" @click="saveValidity">Save</button>
    </template><!-- end of modal-body -->
</modal><!-- end of Product Order Modal -->

<div id="frmValidUntil" class="hide">
    <div id="validset">
        <label for="expires_at">Valid Until:</label>
        <input 
	        type="text" 
	        class="form-control validdate" 
	        v-datepicker required 
	        id="expires_at" 
	        name="expires_at"
	        data-mask="9999-99-99"
	        placeholder="yyyy-mm-dd"
	    >
	    <button type="button" class="btn btn-w-m btn-primary pull-right save-verification" style="margin-top: 5px;">Save</button>
    </div>
</div>

<div id="frmReason" class="hide">
    <div id="reasonset">
        <label for="reason">Reason:</label>
	    <textarea
		    class="form-control autoExpand"
		    rows="1"
		    data-min-rows='1'  
		    id="reason" 
		    name="reason"
		    required
		    style="resize:none;"
	     ></textarea>
	    <button type="button" class="btn btn-w-m btn-primary pull-right save-denied" style="margin-top: 5px;">Save</button>
    </div>
</div>

       