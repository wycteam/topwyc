<!-- Add FAQ Modal -->
<modal size="modal-lg" class="custom-chosen report-product-modal" id='addQuestion'>
    <template slot="modal-title">
        <h3 id="title">Add Question</h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body" class="product-details-set">
    	<div class="form-group">
            <div class="row">
                <div class="col-sm-9">
                    <div class="form-group">
                        <input type="text" class="form-control" v-model="save.question" placeholder="Question in English">
                    </div>
                    <div class="form-group" v-if="save.questionCn">
                        <input type="text" class="form-control" v-model="save.questionCn" placeholder="Question in Chinese">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <select class="form-control m-b" name="type" v-model="save.type">
                            <option value="Type" disabled="">Type</option>
                            <option value="Account">Account</option>
                            <option value="Product">Product</option>
                            <option value="Payment">Payment</option>
                            <option value="Chat">Chat</option>
                            <option value="Store">Store</option>
                            <option value="Order">Order</option>
                            <option value="E-wallet">E-wallet</option>
                        </select>
                    </div>
                </div>
            </div>
    	</div>
    	<div class="form-group">
            <div class="row">
                <div class="col-sm-12">
                    <h3>Answer in English</h3>
                    <quill-editor
                        v-model="save.answer"
                        :options="editorOptions">
                    </quill-editor>
                </div>
                <div class="col-sm-12" v-if="save.answerCn">
                    <h3>Answer in Chinese</h3>
                    <quill-editor
                        v-model="save.answerCn"
                        :options="editorOptions">
                    </quill-editor>
                </div>
            </div>
    	</div>

	</template><!-- end of modal-body -->
	<template slot="modal-footer">
		<button type="button" class="btn btn-w-m btn-primary" v-if="!save.id"@click="saveQuestion" style="margin-top: 10px;">Save</button>
		<button type="button" class="btn btn-w-m btn-primary" v-if="save.id" @click="editQuestion(save.id)" style="margin-top: 10px;">Update</button>

	</template>
</modal><!-- end of Product Order Modal -->

<!-- Edit FAQ Modal -->
<modal size="modal-sm" class="custom-chosen report-product-modal" id='deleteQuestion'>
    <template slot="modal-title">
        <h3>Delete Question</h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body" class="product-details-set">
    	<p>Are you sure you want to delete this?</p>

	</template><!-- end of modal-body -->
	<template slot="modal-footer">
		<button type="button" class="btn btn-w-m btn-primary" @click="deleteQuestion">Yes</button>
		<button type="button" class="btn btn-w-m btn-primary" >Cancel</button>
	</template>
</modal><!-- end of Product Order Modal -->