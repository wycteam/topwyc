<modal id='shipdetails' size="modal-lg" class="sales-product-modal orderdetails-modal" >
    <template slot="modal-title">
        <h3>
            Order No. &nbsp; <span>@{{selected.tracking_number}}</span>
            <!-- shipment status -->
            <!-- <span class="label label-info">Pending</span>
            <span class="label label-primary">In Transit</span>
            <span class="label label-success">Delivered</span>
            <span class="label label-warning">Returned</span>
            <span class="label label-danger">Cancelled</span>     -->      
        </h3>
    </template>
    <template slot="modal-body" v-if="selected != ''">
        <div class="row">
            <div class="col-sm-12">
                <div class='product-card'>
                    <div class="col-lg-12 product-set">
                        <div class="col-lg-12 order-details">
                            <div class="delivery-info">
                                <h4 class="title">DELIVERY INFORMATION</h4>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Shipped To:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{selected.user.full_name}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Address:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>#2 pasong balite marulas</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Nearest Landmark:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{selected.address.landmark}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Contact No.:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{selected.address.contact_number}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Alternate Contact No.:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{selected.address.alternate_contact_number}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row" v-if="selected.address.remarks != null">
                                    <div class="col-sm-4">
                                        Buyer Remarks:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{selected.address.remarks}}</span>
                                    </div>
                                </div>
                                <hr v-if="selected.address.remarks != null">
                            </div> <!-- delivery-info -->

                            <div style="height:20px;clear:both;"></div> <!-- spacer -->

                            <div class="products-info">
                                <h4 class="title">PRODUCTS</h4>
                                <div class="row">
                                    <div class="col-sm-12 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Tracking No.</th>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                    <th>Details</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="item in selected.details"
                                                        :item="item" 
                                                        :key="item.id"
                                                        is="productlist">
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- products-info -->

                            <div style="height:20px;clear:both;"></div> <!-- spacer -->

                            <div class="payment-info">
                                <h4 class="title">TOTAL SUMMARY</h4>
                                <div class="col-lg-6 col-lg-offset-6 summary-right">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            Subtotal:
                                        </div>
                                        <div class="col-sm-6">
                                            @{{totalsub | currency}}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            Discount:
                                        </div>
                                        <div class="col-sm-6">
                                            @{{totaldiscount | currency}}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Total Price including VAT:<strong>
                                        </div>
                                        <div class="col-sm-6">
                                            <strong>@{{priceAfterDiscount | currency}}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- payment-info -->
                        </div>
                        <div style="height:10px;clear:both;"></div> <!-- spacer -->
                    </div>
                </div>
            </div>            
        </div>
    </template>
    <template slot="modal-footer">
    </template>
</modal>