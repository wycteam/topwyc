<modal id='orderdetails-modal' size="modal-lg" class="sales-product-modal orderdetails-modal">
    <template slot="modal-title">
        <h3>
            Tracking No. &nbsp; <span>@{{orderDetails.tracking_number}}</span>
            <span class="label label-info" v-if="orderDetails.status === 'Pending'">@{{orderDetails.status}}</span>
            <span class="label label-primary" v-if="orderDetails.status === 'In Transit'">@{{orderDetails.status}}</span>
            <span class="label label-success" v-if="orderDetails.status == 'Received' || orderDetails.status == 'Delivered'">@{{orderDetails.status}}</span>
            <span class="label label-warning" v-if="orderDetails.status === 'Returned'">@{{orderDetails.status}}</span>
            <span class="label label-danger" v-if="orderDetails.status === 'Cancelled'">@{{orderDetails.status}}</span>          
        </h3>
    </template>
    <template slot="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class='product-card'>
                    <div class="col-lg-12 product-set">
                        <!-- <div class="col-lg-4">
                            <div class="product-img-set">
                                <img  class="product-img" :src="orderDetails.product_main_image">
                            </div>
                            <div class="productDetails">
                                <a :href="'http://' +base_url+'/product/'+orderDetails.product_slug">
                                    <p class="productName">@{{orderDetails.product_name}}</p>
                                </a>
                                <p>By <span class="storeName"><a :href="'http://' +base_url+'/stores/'+orderDetails.store_slug">@{{orderDetails.store_name}}</a></span></p>

                                <p class="sku">
                                    <span class="label label-info" v-if="orderDetails.status === 'Pending'">@{{orderDetails.status}}</span>                           
                                    <span class="label label-primary" v-if="orderDetails.status === 'In Transit'">@{{orderDetails.status}} on @{{orderDetails.shipped_date}}</span>
                                    <span class="label label-success" v-if="orderDetails.status == 'Received' || orderDetails.status == 'Delivered'">@{{orderDetails.status}} on @{{orderDetails.received_date}}</span>
                                    <span class="label label-danger" v-if="orderDetails.status === 'Cancelled'">@{{orderDetails.status}}</span>
                                </p>

                                <div class="qualification-container">
                                    <h5 class="title qualification">QUALIFICATIONS</h5>
                                    <p v-if="orderDetails.product_boxContent != null">What's in the box: <span>@{{orderDetails.product_boxContent}}</span>
                                    </p>
                                    <p v-if="orderDetails.product_brand != null">Brand: <span>@{{orderDetails.product_brand}}</span></p>
                                    <p v-if="orderDetails.color != ''">Color: <span>@{{orderDetails.color}}</span></p>
                                    <p v-if="orderDetails.size != ''">Size: <span>@{{orderDetails.size}}</span></p>
                                    <p v-if="orderDetails.product_weight != null">Weight: <span>@{{orderDetails.product_weight}}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 order-details">
                            <div class="delivery-info">
                                <h4 class="title">DELIVERY INFORMATION</h4>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Shipped To:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.order_receiver}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Address:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.full_address}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row" v-if="orderDetails.order_landmark != null">
                                    <div class="col-sm-4">
                                        Nearest Landmark:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.order_landmark}}</span>
                                    </div>
                                </div>
                                <hr v-if="orderDetails.order_landmark != null">
                                <div class="row">
                                    <div class="col-sm-4">
                                        Contact No.:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.order_contact}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Alternate Contact No.:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.order_alternate}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row" v-if="orderDetails.remarks != null">
                                    <div class="col-sm-4">
                                        Buyer Remarks:
                                    </div>
                                    <div class="col-sm-8">
                                        <span v-if="orderDetails.remarks != null">@{{orderDetails.remarks}}</span>
                                    </div>
                                </div>
                                <hr v-if="orderDetails.remarks != null">
                            </div>
                            <h4 class="title">TOTAL SUMMARY</h4>
                            <div class="row">
                                <div class="col-sm-4">
                                        Unit Price:
                                </div>
                                <div class="col-sm-8">
                                    <span>@{{price_per_item | currency}}</span>
                                </div>
                            </div>
                            <div class="row" v-if="hasDiscount">
                                <div class="col-sm-4">
                                        You save:
                                </div>
                                <div class="col-sm-8">
                                    <span>@{{discountedAmount}}</span>(<span>@{{orderDetails.discount}}</span>%)
                                </div>
                            </div>
                            <div class="row" v-if="hasDiscount">
                                <div class="col-sm-4">
                                        Now:
                                </div>
                                <div class="col-sm-8">
                                    <span>@{{now | currency}}</span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    Quantity:
                                </div>
                                <div class="col-sm-8">
                                    <span>@{{orderDetails.quantity}}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-4">
                                    Shipping Fee
                                </div>
                                <div class="col-sm-8">
                                    <span>@{{shipping | currency}}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    Total:
                                </div>
                                <div class="col-sm-8">
                                    <span>@{{orderDetails.subtotal | currency}}</span>
                                </div>
                            </div>
                        </div>
 -->
                        <div class="col-lg-12 order-details">
                            <div class="delivery-info">
                                <h4 class="title">DELIVERY INFORMATION</h4>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Shipped To:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.order_receiver}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Address:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.full_address}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row" v-if="orderDetails.order_landmark != null">
                                    <div class="col-sm-4">
                                        Nearest Landmark:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.order_landmark}}</span>
                                    </div>
                                </div>
                                <hr v-if="orderDetails.order_landmark != null">
                                <div class="row">
                                    <div class="col-sm-4">
                                        Contact No.:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.order_contact}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        Alternate Contact No.:
                                    </div>
                                    <div class="col-sm-8">
                                        <span>@{{orderDetails.order_alternate}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row" v-if="orderDetails.remarks != null">
                                    <div class="col-sm-4">
                                        Buyer Remarks:
                                    </div>
                                    <div class="col-sm-8">
                                        <span v-if="orderDetails.remarks != null">@{{orderDetails.remarks}}</span>
                                    </div>
                                </div>
                                <hr v-if="orderDetails.remarks != null">
                            </div> <!-- delivery-info -->

                            <div style="height:20px;clear:both;"></div> <!-- spacer -->

                            <div class="products-info">
                                <h4 class="title">PRODUCTS</h4>
                                <div class="row">
                                    <div class="col-sm-12 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                    <th>Color</th>
                                                    <th>Size</th>
                                                    <th>Weight</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="detail in orderProducts">
                                                    <td>
                                                        <img class="img-thumbnail img-responsive" :src="detail.image"> 
                                                    </td>
                                                    <td>@{{ detail.product.name }}</td>
                                                    <td>
                                                        <span class="label">
                                                            @{{ detail.status }}
                                                        </span>
                                                    </td>
                                                    <td>@{{ (detail.color) ? detail.color : 'N/A' }}</td>
                                                    <td>@{{ (detail.size) ? detail.size : 'N/A' }}</td>
                                                    <td>@{{ detail.product.weight }}</td>
                                                    <td>@{{ detail.quantity }}</td>
                                                    <td>
                                                        <template v-if="detail.sale_price == 0">
                                                            @{{ ((parseFloat(detail.price_per_item) + (parseFloat(detail.shipping_fee) + parseFloat(detail.charge) + parseFloat(detail.vat))) * detail.quantity) | currency }}
                                                        </template>
                                                        <template v-else>
                                                            @{{ ((parseFloat(detail.sale_price) + (parseFloat(detail.shipping_fee) + parseFloat(detail.charge) + parseFloat(detail.vat))) * detail.quantity) | currency }}
                                                        </template>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- products-info -->

                            <div style="height:20px;clear:both;"></div> <!-- spacer -->

                            <div class="payment-info">
                                <h4 class="title">TOTAL SUMMARY</h4>
                                <div class="col-lg-6 col-lg-offset-6">
                                    <div class="row" v-if="totaldiscount > 0">
                                        <div class="col-sm-6">
                                            Subtotal:
                                        </div>
                                        <div class="col-sm-6">
                                            @{{ totalsub | currency }}
                                        </div>
                                    </div>
                                    <hr v-if="totaldiscount > 0">
                                    <div class="row" v-if="totaldiscount > 0">
                                        <div class="col-sm-6">
                                            Discount:
                                        </div>
                                        <div class="col-sm-6">
                                            @{{ totaldiscount | currency }}
                                        </div>
                                    </div>
                                    <hr v-if="totaldiscount > 0">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Total Price including VAT:<strong>
                                        </div>
                                        <div class="col-sm-6">
                                            <strong>@{{ priceAfterDiscount | currency }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- payment-info -->
                        </div>

                        <div v-if="orderDetails.reason">
                            <div class="col-sm-12">
                                <br/>
                                <div class="alert alert-dismissible alert-danger returned-info return-details">
                                    <span>This order has been returned on @{{orderDetails.returned_date}} with Case No. <strong>@{{orderDetails.case_number}}</strong>.</span><br/>
                                    <span>Reason for Returning : <strong>'@{{orderDetails.reason}}'</strong>.</span>
                                </div>
                            </div>

                            <div class="col-sm-12" id="photoEvidence">
                                <h4 class="title return-details">
                                    IMAGES FROM THE BUYER
                                </h4>
                                <div class="row">
                                    <div class="col-sm-4" v-for='(item, index) in orderDetails.file_image' :key='item.id' style="display:inline-block; float:none; margin-top:10px;">
                                        <a :href="'/storage/'+item.file" :data-lightbox="'image-'+index" :data-title="orderDetails.reason">
                                            <img :src="'/storage/'+item.file" class="img-thumbnail">
                                        </a>
                                    </div>
                                </div>
                            </div> <!-- photoEvidence -->

                            <div class="col-sm-12" id="videoEvidence">
                                <h4 class="title">
                                    VIDEOS FROM THE BUYER
                                </h4>

                                <div class="row">
                                    <div class="col-sm-4" v-for='(item, index) in orderDetails.file_video' :key='item.id' style="display:inline-block; float:none; margin-top:10px;">
                                        <a :href="'/storage/'+item.file" data-toggle="lightbox" data-gallery="video-evidence" data-type="video" data-wrapping="false" :id="'video-evidence-' + index" @click="showVideoEvidenceModal($event, index, 'orderdetails-modal')">
                                            <video>
                                                <source :src="'/storage/'+item.file">
                                            </video>
                                        </a>
                                    </div>
                                </div>
                            </div> <!-- videoEvidence -->
                        </div>

                        <div style="height:10px;clear:both;"></div> <!-- spacer -->
                     
                        <div class="col-sm-12 productChat" v-if="orderDetails.status === 'Returned' && !orderDetails.returned_received_date">
                            <h4 class="title">ADDITIONAL ACTIONS</h4>
                            <div class="row">
                                <!-- force ewallet return to the buyer -->
                                <div class="col-sm-12">
                                    <button class='btn btn-outline btn-success' @click='btnReturnBuyerEwallet(orderDetails.id)'>
                                        <b class='fa fa-3x fa-undo'></b><br>
                                        Return Buyer's Ewallet
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 productChat" v-if="orderDetails.status === 'Returned' && !orderDetails.returned_received_date">
                            <p>
                                <i class="fa fa-user fa-5" aria-hidden="true"></i>
                                <a :href="'/messages#/u/'+user_id">Chat Buyer</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </template>
    <template slot="modal-footer">
    </template>
</modal>