<div id="group-details" class="panel-body">

    @if(count($groups) == 0)
        <h2>@lang('cpanel/clients-page.not-included-in-any-group')</h2>
    @else
        @foreach ($groups as $group)
            @if($group->group_detail)
                <div class="col-lg-4">
                    <div class="widget navy-bg p-sm">
                            <a href="/visa/group/{{$group->group_detail['id']}}" style="color:#fff !important;" target='_blank'><h2>{{$group->group_detail['name']}}</h2></a>
                        <ul class="list-unstyled m-t-md">
                            <li>
                                <span class="fa fa-star m-r-xs"></span>
                                <label>@lang('cpanel/clients-page.group-leader'):</label>
                                <a href="/visa/client/{{$group->group_detail->user['id']}}" style="color:#fff !important;" target='_blank'>{{$group->group_detail->user['full_name']}}</a>
                            </li>
                            <li>
                                <span class="fa fa-phone m-r-xs"></span>
                                <label>@lang('cpanel/clients-page.contact'):</label>
                                {{ $group->group_detail->user->contact_number }}
                                {{ $group->group_detail->user->alternate_contact_number }}
                            </li>
                        </ul>
                    </div>
                </div>
            @endif
        @endforeach
    @endif
</div>