<div id="access-control" class="panel-body">
    <form id="roles-permissions" action="/users/{{ $selectedUser->id }}/roles-permissions" method="POST">
        {{ csrf_field() }}
        <h4>@lang('cpanel/clients-page.roles')</h4>
        <select id="roles" name="roles[]" class="form-control dual_select" multiple>
            @foreach ($roles as $role)
            <option {{ $assignedRoles->contains('id', $role->id) ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->label }}</option>
            @endforeach
        </select>

        <hr>

        <h4>@lang('cpanel/clients-page.permissions')</h4>
        <select id="permissions" name="permissions[]" class="form-control dual_select" multiple>
            @foreach ($permissions as $permission)
            <option {{ $assignedPermissions->contains('id', $permission->id) ? 'selected' : '' }} value="{{ $permission->id }}">{{ $permission->type.' '.$permission->label }}</option>
            @endforeach
        </select>
        <div class="row">
            <div class="col-xs-12 text-right">
                <button class="btn btn-primary m-t" type="submit">@lang('cpanel/clients-page.save')</button>
            </div>
        </div>
    </form>
</div>