<div id="frmValidUntil" class="hide">
    <div id="validset">
        <label for="expires_at">Valid Until:</label>
        <input 
	        type="text" 
	        class="form-control validdate" 
	        v-datepicker required 
	        id="expires_at" 
	        name="expires_at"
	        data-mask="9999-99-99"
	        placeholder="yyyy-mm-dd"
	    >
	    <button type="button" class="btn btn-w-m btn-primary pull-right save-verification" style="margin-top: 5px;">Save</button>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
	            <div class="container-fluid">
					<div class="row"><!-- start row -->
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  9g I-Card</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload 9g I-Card">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
									<span class='label label-info' v-if='ninegicard_compare==false && ninegicard_expiry!=null' v-text='"Expires at "+ninegicard_expiry'></span>
									<span class='label label-danger' v-if='ninegicard_compare==true && ninegicard_expiry!=null' v-text='"Expires on "+ninegicard_expiry'></span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btn9gICardUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="ninegICard-uploader hide">
					              		<imgfileupload data-event="ninegICardUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=9gICard"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="doc9gICard">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+ninegicard_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="9G I-CARD">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_9gicard'
											:href		='"/images/documents/{{$selectedUser->id}}/" + ninegicard_file '
											class		='btn btn-xs'
											:class		='doc9gICard?"btn-success":""'
											:disabled	='!doc9gICard'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>

										<a :id='doc9gICardId' href='javascript:void(0)' class="btn btn-xs btn-success add-verification" data-placement="bottom" data-docs="9gICard">
											<span class="hidden-xs">Expiration</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</a></a>

				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->	            	
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  9g Order</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload 9g Order">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
									<span class='label label-info' v-if='ninegorder_compare==false && ninegorder_expiry!=null' v-text='"Expires at "+ninegorder_expiry'></span>
									<span class='label label-danger' v-if='ninegorder_compare==true && ninegorder_expiry!=null' v-text='"Expires on "+ninegorder_expiry'></span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btn9gOrderUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="ninegOrder-uploader hide">
					              		<imgfileupload data-event="ninegOrderUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=9gOrder"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="doc9gOrder">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+ninegorder_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="9G ORDER">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_9gorder'
											:href		='"/images/documents/{{$selectedUser->id}}/" + ninegorder_file '
											class		='btn btn-xs'
											:class		='doc9gOrder?"btn-success":""'
											:disabled	='!doc9gOrder'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>

										<a :id='doc9gOrderId' href='javascript:void(0)' class="btn btn-xs btn-success add-verification" data-placement="bottom" data-docs="9gOrder"> 
											<span class="hidden-xs">Expiration</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</a></a>

				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->
					</div><!-- end row -->
					<div class="row"><!-- start row -->
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  PRV Card</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload PRV Card">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
									<span class='label label-info' v-if='prvcard_compare==false && prvcard_expiry!=null' v-text='"Expires at "+prvcard_expiry'></span>
									<span class='label label-danger' v-if='prvcard_compare==true && prvcard_expiry!=null' v-text='"Expires on "+prvcard_expiry'></span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnPrvCardUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="prvCard-uploader hide">
					              		<imgfileupload data-event="prvCardUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=prvCard"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docPrvCard">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+prvcard_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="PRV CARD">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_prvcard'
											:href		='"/images/documents/{{$selectedUser->id}}/" + prvcard_file '
											class		='btn btn-xs'
											:class		='docPrvCard?"btn-success":""'
											:disabled	='!docPrvCard'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>

										<a :id='docPrvCardId' href='javascript:void(0)' class="btn btn-xs btn-success add-verification" data-placement="bottom" data-docs="prvCard">
											<span class="hidden-xs">Expiration</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</a></a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->	            	
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  PRV Order</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload PRV Order">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
									<span class='label label-info' v-if='prvorder_compare==false && prvorder_expiry!=null' v-text='"Expires at "+prvorder_expiry'></span>
									<span class='label label-danger' v-if='prvorder_compare==true && prvorder_expiry!=null' v-text='"Expires on "+prvorder_expiry'></span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnPrvOrderUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="prvOrder-uploader hide">
					              		<imgfileupload data-event="prvOrderUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=prvOrder"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docPrvOrder">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+prvorder_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="PRV ORDER">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_prvorder'
											:href		='"/images/documents/{{$selectedUser->id}}/" + prvorder_file '
											class		='btn btn-xs'
											:class		='docPrvOrder?"btn-success":""'
											:disabled	='!docPrvOrder'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>

										<a :id='docPrvOrderId' href='javascript:void(0)' class="btn btn-xs btn-success add-verification" data-placement="bottom" data-docs="prvOrder">
											<span class="hidden-xs">Expiration</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</a></a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->
					</div><!-- end row -->
					<div class="row"><!-- start row -->
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  SRRV Card</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload SRRV Card">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
									<span class='label label-info' v-if='srrvcard_compare==false && srrvcard_expiry!=null' v-text='"Expires at "+srrvcard_expiry'></span>
									<span class='label label-danger' v-if='srrvcard_compare==true && srrvcard_expiry!=null' v-text='"Expires on "+srrvcard_expiry'></span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnSrrvCardUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="srrvCard-uploader hide">
					              		<imgfileupload data-event="srrvCardUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=srrvCard"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docSrrvCard">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+srrvcard_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="SRRV CARD">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_srrvcard'
											:href		='"/images/documents/{{$selectedUser->id}}/" + srrvcard_file '
											class		='btn btn-xs'
											:class		='docSrrvCard?"btn-success":""'
											:disabled	='!docSrrvCard'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>

										<a :id='docSrrvCardId' href='javascript:void(0)' class="btn btn-xs btn-success add-verification" data-placement="bottom" data-docs="srrvCard">
											<span class="hidden-xs">Expiration</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</a></a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->	            	
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  SRRV Visa</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload SRRV Visa">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
									<span class='label label-info' v-if='srrvvisa_compare==false && srrvvisa_expiry!=null' v-text='"Expires at "+srrvvisa_expiry'></span>
									<span class='label label-danger' v-if='srrvvisa_compare==true && srrvvisa_expiry!=null' v-text='"Expires on "+srrvvisa_expiry'></span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnSrrvVisaUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="srrvVisa-uploader hide">
					              		<imgfileupload data-event="srrvVisaUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=srrvVisa"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docSrrvVisa">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+srrvvisa_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="SRRV VISA">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_srrvvisa'
											:href		='"/images/documents/{{$selectedUser->id}}/" + srrvvisa_file '
											class		='btn btn-xs'
											:class		='docSrrvVisa?"btn-success":""'
											:disabled	='!docSrrvVisa'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>

										<a :id='docSrrvVisaId' href='javascript:void(0)' class="btn btn-xs btn-success add-verification" data-placement="bottom" data-docs="srrvVisa">
											<span class="hidden-xs">Expiration</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</a></a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->
					</div><!-- end row -->
					<div class="row"><!-- start row -->
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  Marriage Cert.</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload Marriage Cert">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnMarriageCertUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="marriageCert-uploader hide">
					              		<imgfileupload data-event="marriageCertUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=marriageCert"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docMarriageCert">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+marriagecert_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="MARRIAGE CERT.">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_marriagecert'
											:href		='"/images/documents/{{$selectedUser->id}}/" + marriagecert_file '
											class		='btn btn-xs'
											:class		='docMarriageCert?"btn-success":""'
											:disabled	='!docMarriageCert'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->	            	
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  AEP</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload AEP">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
									<span class='label label-info' v-if='aep_compare==false && aep_expiry!=null' v-text='"Expires at "+aep_expiry'></span>
									<span class='label label-danger' v-if='aep_compare==true && aep_expiry!=null' v-text='"Expires on "+aep_expiry'></span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnAepUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="aep-uploader hide">
					              		<imgfileupload data-event="aepUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=aep"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docAep">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+aep_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="AEP">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_aep'
											:href		='"/images/documents/{{$selectedUser->id}}/" + aep_file '
											class		='btn btn-xs'
											:class		='docAep?"btn-success":""'
											:disabled	='!docAep'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>

										<a :id='docAepId' href='javascript:void(0)' class="btn btn-xs btn-success add-verification" data-placement="bottom" data-docs="aep">
											<span class="hidden-xs">Expiration</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</a></a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->
					</div><!-- end row -->
					<div class="row"><!-- start row -->
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  NBI</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload valid NBI">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
									<span class='label label-info' v-if='nbi_compare==false && nbi_expiry!=null' v-text='"Expires at "+nbi_expiry'></span>
									<span class='label label-danger' v-if='nbi_compare==true && nbi_expiry!=null' v-text='"Expires on "+nbi_expiry'></span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnNbiUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="nbi-uploader hide">
					              		<imgfileupload data-event="nbiUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=nbi"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docNbi">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+nbi_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="NBI">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_nbi'
											:href		='"/images/documents/{{$selectedUser->id}}/" + nbi_file '
											class		='btn btn-xs'
											:class		='docNbi?"btn-success":""'
											:disabled	='!docNbi'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>

										<a :id='docNbiId' href='javascript:void(0)' class="btn btn-xs btn-success add-verification" data-placement="bottom" data-docs="nbi">
											<span class="hidden-xs">Expiration</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-calendar" aria-hidden="true"></i>
										</a></a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->	            	
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  Goverment ID</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload valid Goverment ID">
										<span class="fa fa-question-circle-o icon-color" ></span>										
									</span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnGovIdUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="govId-uploader hide">
					              		<imgfileupload data-event="govIdUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=govId"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docGovId">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+govid_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="Goverment ID">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_govid'
											:href		='"/images/documents/{{$selectedUser->id}}/" + govid_file '
											class		='btn btn-xs'
											:class		='docGovId?"btn-success":""'
											:disabled	='!docGovId'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->
					</div><!-- end row -->
					<div class="row"><!-- start row -->
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  Birth Cert.</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload Birth Cert.">
										<span class="fa fa-question-circle-o icon-color" ></span>
									</span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnBirthCertUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="birthCert-uploader hide">
					              		<imgfileupload data-event="birthCertUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=birthCert"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docBirthCert">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+birthcert_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="BIRTH CERT.">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_birthcert'
											:href		='"/images/documents/{{$selectedUser->id}}/" + birthcert_file '
											class		='btn btn-xs'
											:class		='docBirthCert?"btn-success":""'
											:disabled	='!docBirthCert'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->	            	
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  SEC</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload valid SEC">
										<span class="fa fa-question-circle-o icon-color" ></span>										
									</span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnSecUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="sec-uploader hide">
					              		<imgfileupload data-event="secUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=sec"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docSec">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+sec_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="SEC">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_sec'
											:href		='"/images/documents/{{$selectedUser->id}}/" + sec_file '
											class		='btn btn-xs'
											:class		='docSec?"btn-success":""'
											:disabled	='!docSec'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->
					</div><!-- end row -->
					<div class="row"><!-- start row -->           	
						<div class="col-lg-6"> <!-- start document-set -->
							<div class="col-sm-12 document-set list-group-item">
								<div class="col-sm-6">
									<span><span  class="fa fa-asterisk"></span>  BIR</span>
									<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload valid BIR">
										<span class="fa fa-question-circle-o icon-color" ></span>										
									</span>
								</div>
								<div class="col-sm-6 doc-btn-set">		    				
				                    <button v-cloak class="btnBirUpload btn btn-warning btn-raised btn-xs">             
				                    	<span class="hidden-xs">Upload</span>
										<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
				                	</button>

				              		<div class="bir-uploader hide">
					              		<imgfileupload data-event="birUpload" data-url="/visa/client/upload-docs/{{$selectedUser->id}}?width=800&height=800&type=bir"></imgfileupload>
				              		</div>

				              		<div class="doc-action-btns" v-if="docBir">
										<a :href='"http://"+cpanel_url+"/images/documents/{{$selectedUser->id}}/"+bir_file' class="btn btn-xs btn-success" data-toggle="lightbox" data-gallery="example-gallery" data-title="BIR">
											<span class="hidden-xs">View</span>
											<span class="hidden-sm hidden-md hidden-lg">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>

										<a 	id			='download_bir'
											:href		='"/images/documents/{{$selectedUser->id}}/" + bir_file '
											class		='btn btn-xs'
											:class		='docBir?"btn-success":""'
											:disabled	='!docBir'
											download	=''>
											<span class="hidden-xs">Download</span>
											<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
										
										</a>
				              		</div>
									
								</div>
							</div>
						</div><!-- end document-set -->
					</div><!-- end row -->					
	            </div>
            </div>
        </div>

    </div>
</div>