
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                    <thead>
                        <tr>

                           <th>Date </th>
                           <th>Ref No. </th>
                           <th>Type    </th>
                           <th>Amount  </th>
                           <th>Description </th>
                           <th>Status  </th>
                           <th>Balance</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="etransaction in etransactions"  :key="etransaction.updated_at">
                            <td v-cloak>@{{etransaction.updated_at}} </td>
                            <td v-cloak>@{{etransaction.ref_number}} </td>
                            <td v-cloak>@{{etransaction.type}} </td>
                            <td v-cloak>@{{etransaction.amount | currency}} </td>
                            <td v-cloak>@{{etransaction.description}} </td>
                            <td v-cloak>@{{etransaction.status}} </td>
                            <td v-cloak>@{{etransaction.balance | currency}} </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>