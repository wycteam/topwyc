@extends('cpanel.layouts.master')

@section('title', 'Attendance')

@push('styles')
    <style>
        #fixTable-wrapper {
            overflow-x: scroll;
        }
        #fixTable {
            width: 1800px !important;
        }
        #fixTable tr th, #fixTable tr td {
            min-width: 200px;
            vertical-align: middle;
            padding: 10px;
        }
        #fixTable tr th + th,  #fixTable tr td + td {
           text-align: center;
        }
        .employee-name {
            background-color: #1ab394;
        }
        .employee-name a, .employee-name a:hover {
            color: #ffffff;
        }
        .dept-admin {
            background-color: #014e3f;
        }
        .dept-legal {
            background-color: #057f68;
        }
        .dept-processing {
            background-color: #1ab394;
        }
        .dept-purchasing {
            background-color: #4f7d74;
        }
        .dept-it {
            background-color: #6eb5a7;
        }
        #filter-btn {
            margin-top: 5px;
        }
        th.current, td.current {
            background-color: #1ab394 !important;
            color: #fff;
        }
        td.weekends {
            background-color: #999 !important;
            color: #fff;
        }
        .label.label-def {
            background-color: #2a2c2e;
            font-weight: bold;
            color: #fff;
        }
        .label.label-late {
            background-color: #ece401;
            font-weight: bold;
            color: #000;
        }
        .label.label-slate {
            background-color: #f3a010;
            font-weight: bold;
            color: #000;
        }
        .label.label-earlyout {
            background-color: #ece401;
            font-weight: bold;
            color: #000;
        }
        .label.label-absent {
            background-color: #c30000;
            font-weight: bold;
            color: #fff;
        }
    </style>
@endpush


@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Attendance Monitoring</h2>
        </div>
    </div>

    <div id="attendance_monitoring" class="wrapper wrapper-content animated">
        <div class="row">
            <div class="col-lg-12">

                <div style="height:10px; clear:both;"></div>

                <div v-show="!loading" class="ibox float-e-margins">
                    <div class="ibox-content">
      
                        <center>
                            <h1>@{{ selectMonths[filterMonth-1].text + ' ' + filterYear }}</h1>

                            <form class="form-inline">
                                <div class="form-group">
                                    <label>Month</label>
                                    <select class="form-control" v-model="filterMonth" @change="setNumberOfDays">
                                        <option :value="month.value" v-for="month in selectMonths">
                                            @{{ month.text }}
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Year</label>
                                    <select class="form-control" v-model="filterYear" @change="setNumberOfDays">
                                        <option :value="year" v-for="year in selectYears">
                                            @{{ year }}
                                        </option>
                                    </select>
                                </div>
                                <button id="filter-btn" type="button" class="btn btn-primary" @click="filter">Filter</button>
                            </form>
                        </center>

                        <div class="row">
                            <div class="col-md-6">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input type="text" class="form-control" v-model="search" @keyup="refreshTableHeadFixer">
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="pull-right" style="margin-top: 10px;">
                                    Legend:
                                    <span class="label label-def">DEFAULT</span>
                                    <span class="label label-late">LATE</span>
                                    <span class="label label-slate">SUPER LATE</span>
                                    <span class="label label-earlyout">EARLY OUT</span>
                                    <span class="label label-absent">ABSENT</span>
                                </div>
                            </div>
                        </div>

                        <div style="height:10px;clear:both;"></div>

                        <div id="fixTable-wrapper">
                            <table id="fixTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Employee</th>
                                        <th v-for="day in numberOfDays" :class="{
                                            'current' : current && day == new Date().getDate()
                                        }">
                                            <strong>@{{ day }}</strong>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="employee in filteredEmployees">
                                        <td class="employee-name" :class="{
                                                        'dept-admin' : employee.department == '1',
                                                        'dept-legal' : employee.department == '2',
                                                        'dept-purchasing' : employee.department == '4',
                                                        'dept-it' : employee.department == '5'}">
                                            <a :href="'/emp/' + employee.id" target="_blank">
                                                <strong>@{{ employee.last_name }}</strong>, 
                                                @{{ employee.first_name }} <!-- @{{ employee.department }} -->
                                            </a>
                                        </td>
                                        <td v-for="attendance in employee.attendance" :class="{
                                            'current' : current && attendance.day == new Date().getDate(),
                                            'weekends' : (attendance.time_in_status == 'WEEKENDS') && (attendance.time_out_status == 'WEEKENDS')
                                        }">
                                            <template v-if="attendance.has_data">
                                                <template v-if="attendance.time_in_status == 'ABSENT' && attendance.time_out_status == 'ABSENT'">
                                                    <span class="label label-absent">ABSENT</span>
                                                </template>
                                                <template v-else>
                                                    <span v-if="attendance.time_in" class="label label-def" :class="{
                                                        'label-late' : attendance.time_in_status == 'LATE',
                                                        'label-slate' : attendance.time_in_status == 'SLATE'
                                                    }">
                                                        @{{ attendance.time_in }}
                                                    </span>
                                                    &nbsp;&nbsp;
                                                    <span v-if="attendance.time_out" class="label label-def" :class="{
                                                        'label-earlyout' : attendance.time_out_status == 'EARLYOUT'
                                                    }">
                                                        @{{ attendance.time_out }}
                                                    </span>
                                                </template>
                                            </template>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <div v-show="loading" class="ibox">
                    <div class="ibox-content">
                        <div class="spiner-example">
                            <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {!! Html::script('cpanel/plugins/tableHeadFixer/tableHeadFixer.js') !!}
    {!! Html::script(mix('js/employee/attendance/index.js','cpanel')) !!}
@endpush
