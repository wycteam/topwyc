@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('cpanel/plugins/dualListbox/bootstrap-duallistbox.min.css') !!}
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>{{ $title }}</h2>
        </div>
    </div>

    <div id="editAccount" class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Account Form</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                       @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{ session()->get('message') }}
                        </div>
                        @endif
                        <form role="form" method="POST" action="{{ route('cpanel.visa.cpanel-accounts-update',$account[0]->id) }}">
                        {!! Form::token() !!}

                        <div class="row">
                            <div class="col-xs-12">

                            <legend>Personal Information</legend>
                            <div class="row">
                                <div class="col-md-2 avatar">
                                    <img :src="accountForm.avatar" class='img-responsive img-circle btnUpload center-block' data-toggle='tooltip' title="Upload Display Picture" style="cursor: pointer; max-width: 130px;">
                                    <div class="avatar-uploader hide">
                                      <imgfileupload 
                                        data-event="avatarUpload" 
                                        data-url="/visa/cpanel-accounts/update-avatar/{{$account[0]->id}}?width=300&height=300&type=avatar&resize=true"></imgfileupload>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                <label>First Name</label>
                                                <input class="form-control" id="first_name" name="first_name" type="text" placeholder="Enter first name" v-model="accountForm.first_name">

                                                @if ($errors->has('first_name'))
                                                <span class="help-block">
                                                    {{ $errors->first('first_name') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Middle Name</label>
                                                <input class="form-control" id="middle_name" name="middle_name" type="text" placeholder="Enter middle name" v-model="accountForm.middle_name">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                <label>Last Name</label>
                                                <input class="form-control" id="last_name" name="last_name" type="text" placeholder="Enter last name" v-model="accountForm.last_name">

                                                @if ($errors->has('last_name'))
                                                <span class="help-block">
                                                    {{ $errors->first('last_name') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                                                <label>Date of Birth</label>
                                                <input class="form-control" id="birth_date" name="birth_date" v-model="accountForm.birth_date" v-datepicker type="text" >

                                                @if ($errors->has('birth_date'))
                                                <span class="help-block">
                                                    {{ $errors->first('birth_date') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                                <label>Gender</label>
                                                <select class="form-control" id="gender" name="gender" v-model="accountForm.gender">
                                                    <option value="0">Select</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>

                                                @if ($errors->has('gender'))
                                                <span class="help-block">
                                                    {{ $errors->first('gender') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group{{ $errors->has('civil_status') ? ' has-error' : '' }}">
                                                <label>Civil Status</label>

                                                <select class="form-control" id="civil_status" name="civil_status" v-model="accountForm.civil_status">
                                                    <option value="0">Select</option>
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Widowed">Widowed</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Separated">Separated</option>
                                                </select>

                                                @if ($errors->has('civil_status'))
                                                <span class="help-block">
                                                    {{ $errors->first('civil_status') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Height</label>
                                                <input class="form-control" id="height" name="height" type="text" placeholder="Enter height" v-model="accountForm.height">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Weight</label>
                                                <input class="form-control" id="weight" name="weight" type="text" placeholder="Enter weight" v-model="accountForm.weight">

                                            </div>
                                        </div>
                                    </div>                                
                                </div>
                            </div>


                            <legend class="m-t">Contact Information</legend>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group{{ $errors->has('address.address') ? ' has-error' : '' }}">
                                        <label>Address</label>
                                        <input name="address[id]" type="hidden" v-model="accountForm.address.id">
                                        <input class="form-control" id="address" name="address[address]" type="text" placeholder="Enter address" v-model="accountForm.address.address">

                                        @if ($errors->has('address.address'))
                                        <span class="help-block">
                                            {{ $errors->first('address.address') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Barangay</label>
                                        <input class="form-control" id="barangay" name="address[barangay]" type="text" placeholder="Enter barangay" v-model="accountForm.address.barangay">                                        
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('address.city') ? ' has-error' : '' }}">
                                        <label>City</label>
                                        <input class="form-control" id="city" name="address[city]" type="text" placeholder="Enter city" v-model="accountForm.address.city">                                        
                                        @if ($errors->has('address.city'))
                                        <span class="help-block">
                                            {{ $errors->first('address.city') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('address.province') ? ' has-error' : '' }}">
                                        <label>Province</label>
                                        <input class="form-control" id="province" name="address[province]" type="text" placeholder="Enter province" v-model="accountForm.address.province">                                        

                                        @if ($errors->has('address.province'))
                                        <span class="help-block">
                                            {{ $errors->first('address.province') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Zip Code</label>
                                        <input class="form-control" id="zip_code" name="address[zip_code]" type="text" placeholder="Enter zip_code" v-model="accountForm.address.zip_code">                                        
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('address.contact_number') ? ' has-error' : '' }}">
                                        <label>Contact Number 1</label>
                                        <input class="form-control" id="contact_number" name="address[contact_number]" type="text" placeholder="Enter contact number" v-model="accountForm.address.contact_number">

                                        @if ($errors->has('address.contact_number'))
                                        <span class="help-block">
                                            {{ $errors->first('address.contact_number') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Contact Number 2</label>
                                        <input class="form-control" id="alternate_contact_number" name="address[alternate_contact_number]" type="text" placeholder="Enter contact number" v-model="accountForm.address.alternate_contact_number">                                        
                                    </div>
                                </div>
                            </div>

                            <legend class="m-t">Account Information</legend>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>Email</label>
                                        <input class="form-control" id="email" name="email" type="email" placeholder="Enter email" v-model="accountForm.email" autocomplete="off">

                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            {{ $errors->first('email') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label>Password</label>
                                        {!! Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Enter password']) !!}

                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                            {{ $errors->first('password') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        {!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => 'Retype password']) !!}
                                    </div>
                                </div>
                            </div>

                            <legend class="m-t">Roles & Permissions</legend>

                            <h4>Roles</h4>

                            <select id="roles" name="roles[]" class="form-control dual_select" multiple>
                                @foreach ($roles as $role)
                                <option {{ $account[0]->roles->contains('id', $role->id) ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->label }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('roles'))
                            <div class="text-danger m-t-xs">
                                {{ $errors->first('roles') }}
                            </div>
                            @endif

                            <hr>

                            <h4>Permissions</h4>

                            <select id="permissions" name="permissions[]" class="form-control dual_select" multiple>
                                @foreach ($permissions as $permission)
                                <option {{ $account[0]->permissions->contains('id', $permission->id) ? 'selected' : '' }} value="{{ $permission->id }}">{{ $permission->type.' '.$permission->label }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('permissions'))
                            <div class="text-danger m-t-xs">
                                {{ $errors->first('permissions') }}
                            </div>
                            @endif

                            <button type="submit" class="btn btn-sm btn-primary pull-right m-t">
                                <strong>Save</strong>
                            </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
   
    {!! Html::script('cpanel/plugins/dualListbox/jquery.bootstrap-duallistbox.js') !!}    
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}  
    {!! Html::script(mix('js/visa/accounts/edit.js', 'cpanel')) !!}

<script>
    $(document).ready(function () {
        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160,
        });
    });
</script>     

@endpush