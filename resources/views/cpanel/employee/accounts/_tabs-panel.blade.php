 <div class="col-lg-12 m-t-md">
    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <!-- Track your own attendance and tardiness -->
            <li class="active"><a data-toggle="tab" href="#tab-attendance">Attendance</a></li>
            <!-- Electronic Version of Payslip 
            <li class=""><a data-toggle="tab" href="#tab-payroll">Payroll</a></li>-->
            <!-- THIS TAB IS WHERE EMPLOYEES FILE THEIR LEAVES 
            <li class=""><a data-toggle="tab" href="#tab-leave">File Leave</a></li>-->
            <li class=""><a data-toggle="tab" href="#tab-loan">Loans</a></li>
            <li class=""><a data-toggle="tab" href="#tab-penalty">Penalties</a></li>
            <!-- THIS TAB TO SHOW COMPANY EVENTS, SUBORDINATES APPROVED LEAVES AND OWN LEAVES 
            <li><a data-toggle="tab" href="#tab-events">Calendar of Events</a></li>-->
            <!-- THIS TAB IS FOR CEO, HR and DEPT HEAD to where they approve leaves & absences of their subordinates -->
            <li class=""><a data-toggle="tab" href="#tab-approval"><span class="label label-danger">2</span> For Approval </a></li> 
        </ul>
        
        <div class="tab-content">
            <!-- Track your own attendance and tardiness -->
            <div id="tab-attendance" class="tab-pane active">
                @include('cpanel.employee.accounts.show-tabs-content.attendance-tab')
            </div>

            <!-- Electronic Version of Payslip -->
            <div id="tab-payroll" class="tab-pane">
                @include('cpanel.employee.accounts.show-tabs-content.payroll-tab')
            </div>

            <!-- THIS TAB IS WHERE EMPLOYEES FILE THEIR LEAVES -->
            <div id="tab-leave" class="tab-pane">
                @include('cpanel.employee.accounts.show-tabs-content.file-leave-tab')
            </div>

            <!-- THIS TAB TO SHOW COMPANY EVENTS, SUBORDINATES APPROVED LEAVES AND OWN LEAVES -->
            <div id="tab-events" class="tab-pane">
                @include('cpanel.employee.accounts.show-tabs-content.calendar-events')
            </div>
            <div id="tab-loan" class="tab-pane">
                @include('cpanel.employee.accounts.show-tabs-content.loan-tab')
            </div>
            <div id="tab-penalty" class="tab-pane">
                @include('cpanel.employee.accounts.show-tabs-content.penalty-tab')
            </div>
            <!-- THIS TAB IS FOR CEO, HR and DEPT HEAD to where they approve leaves & absences of their subordinates -->
            <div id="tab-approval" class="tab-pane">
                @include('cpanel.employee.accounts.show-tabs-content.for-approval-tab')
            </div>

            <div style="height:20px;clear:both;"></div>
            
        </div>
    </div>
</div>