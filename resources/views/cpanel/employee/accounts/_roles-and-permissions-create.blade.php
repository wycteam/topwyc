<legend class="m-t">Roles & Permissions</legend>

<h4>Roles</h4>

<select id="roles" name="roles[]" class="form-control dual_select" multiple>
    @foreach ($roles as $role)
    <option value="{{ $role->id }}">{{ $role->label }}</option>
    @endforeach
</select>

@if ($errors->has('roles'))
<div class="text-danger m-t-xs">
    {{ $errors->first('roles') }}
</div>
@endif

<hr>

<h4>Permissions</h4>

<select id="permissions" name="permissions[]" class="form-control dual_select" multiple>
    @foreach ($permissions as $permission)
    <option value="{{ $permission->id }}">{{ $permission->type.' '.$permission->label }}</option>
    @endforeach
</select>

@if ($errors->has('permissions'))
<div class="text-danger m-t-xs">
    {{ $errors->first('permissions') }}
</div>
@endif