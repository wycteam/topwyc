@extends('cpanel.layouts.master')

@section('title', 'Profile')

@push('styles')
    {!! Html::style('cpanel/plugins/dualListbox/bootstrap-duallistbox.min.css') !!}
    {!! Html::style('cpanel/plugins/blueimp/css/blueimp-gallery.min.css') !!}
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
    {!! Html::style('//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css') !!}
    {!! Html::style('js/owl/owl.carousel.css') !!}
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    {!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
    {!! Html::style('js/owl/owl.theme.css') !!}  
    {!! Html::style(mix('css/visa/clients/profile.css', 'cpanel')) !!}
@endpush

@section('content')

	<div class="wrapper wrapper-content" id="show-employee" v-if="employee">
		<div class="row">
            @include('cpanel.employee.accounts.modals-show')
            <!-- TOP PANEL -->
            <div class="col-sm-12 no-side-padding fff-bg">
                <div class="col-sm-3 no-side-padding">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="tab-content">                            
                                <div id="contact-1" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-lg-4 text-center no-side-padding">
                                            <div class="profile-image">
                                                <img :src="employee.avatar" class="img-circle circle-border m-b-md" alt="profile">
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <h3 class="no-margins m-t-2">
                                                <b v-cloak>@{{ employee.full_name }}</b>
                                            </h3>
                                            <h4>
                                            	<p>
                                                    <span v-cloak>@{{ employee.id }}</span>
                                                </p>
                                            </h4>
                                            <a :href="'/visa/cpanel-accounts/'+employee.id+'/edit'" target="_blank" class="btn btn-primary btn-sm">
                                            	<i class="fa fa-edit fa-fw"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 no-side-padding">
                    <div class="ibox no-b-margin">
                        <div class="ibox-content no-tb-padding no-side-padding">
                            <div class="tab-content">
                                <div id="contact-1" class="tab-pane active">
                                    <div class="full-height-scroll details-size">
                                        <strong><h2>Basic Information</h2></strong>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <b>Gender :</b>
                                            </div>
                                            <div class="col-sm-8 no-side-padding">
                                                <span class="totals" v-cloak v-if="employee.gender">
                                                	@{{ employee.gender }}
                                                </span>
                                                <span class=" label label-danger" v-else v-cloak>Empty Gender</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <b>Birthday :</b>
                                            </div>
                                            <div class="col-sm-8 no-side-padding">
                                                <span class="totals m-r-2" v-cloak v-if="employee.birth_date">
                                                	@{{ employee.birth_date | invoiceFormat }}
                                                </span>
                                                <span class="totals m-r-2 label label-danger" v-else v-cloak>
                                                	Empty Birthday
                                               	</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <b>Age :</b>
                                            </div>
                                            <div class="col-sm-8 no-side-padding">
                                                <span class="totals" v-cloak>@{{ employee.birth_date | age }}</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <b>Status :</b>
                                            </div>
                                            <div class="col-sm-8 no-side-padding">
                                                <span class="totals" v-cloak>@{{ employee.civil_status }}</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <b>Address :</b>
                                            </div>
                                            <div class="col-sm-8 no-side-padding">
                                                <template v-if="employee.address">
                                                	<template v-if="employee.address.city">
                                                		<span class="totals" v-cloak>
                                                			@{{ employee.address.address + ", " + employee.address.city }}
                                                		</span>
                                                	</template>
                                                	<template v-else>
                                                		<span class="totals" v-cloak>@{{ employee.address.address }}</span>
                                                	</template>
                                                </template>
                                                <template v-else>
                                                	<span class="totals m-r-2 label label-danger" v-cloak>
		                                                Empty Address
		                                            </span>
                                                </template>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 no-side-padding">
                    <div class="ibox no-b-margin">
                        <div class="ibox-content no-tb-padding no-side-padding">
                            <div class="tab-content">
                                <div id="contact-1" class="tab-pane active">
                                	<div class="full-height-scroll details-size">
                                        <strong><h2>Schedule Information</h2></strong>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <b>Type :</b>
                                            </div>
                                            <div class="col-sm-8 no-side-padding">
                                                <span class="totals" v-cloak v-if="employee.schedule">
                                                	@{{ employee.schedule.schedule_type.name }}
                                                </span>
                                                <span class=" label label-danger" v-else v-cloak>Not Available</span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <b>Details :</b>
                                            </div>
                                            <div class="col-sm-8 no-side-padding">
                                            	<template v-if="employee.schedule">
                                            		<span class="totals" v-cloak>
	                                            		<template v-if="employee.schedule.schedule_type_id == 1">
	                                            			Time In: @{{ employee.schedule.time_in }}
						                                    <br />
						                                    Time Out: @{{ employee.schedule.time_out }}
	                                            		</template>
	                                            		<template v-else-if="employee.schedule.schedule_type_id == 2">
	                                            			Time In Range: 
	                                            			<br>
	                                            			@{{ employee.schedule.time_in_from }} 
	                                            			- 
	                                            			@{{ employee.schedule.time_in_to }}
	                                            		</template>
	                                            		<template v-else-if="employee.schedule.schedule_type_id == 3">
	                                            			Not Available
	                                            		</template>
                                            		</span>
                                            	</template>
                                            	<template v-else>
                                            		<span class="label label-danger" v-cloak>
                                            			Not Available
                                            		</span>
                                            	</template>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- col-sm-12 -->
            <!-- END OF TOP PANEL -->

            <!-- TABS PANEL -->
            <div class="col-sm-12 no-side-padding fff-bg">
                
                 @include('cpanel.employee.accounts._tabs-panel')
               
            </div>
            <!-- END OF TABS PANEL -->

        </div>
	</div> <!-- wrapper -->

    <div style="height:20px;clear:both;"></div>

@stop

@push('scripts')
    {!! Html::script('cpanel/plugins/tableHeadFixer/tableHeadFixer.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js') !!}
    {!! $calendar->script() !!}
    {!! Html::script(mix('js/employee/accounts/show.js','cpanel')) !!}
@endpush

