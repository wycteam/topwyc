@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('cpanel/plugins/dualListbox/bootstrap-duallistbox.min.css') !!}
@endpush

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>{{ $title }}</h2>
        </div>
    </div>

    <div id="userlist" class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Account Form</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {!! Form::open(['id'=>'form-client','route' => 'cpanel.visa.cpanel-accounts-store', 'method' => 'post']) !!}
                        <div class="row">
                            <div class="col-xs-12">

                                @include('cpanel.visa.accounts._personal-info')

                                @include('cpanel.visa.accounts._contact-info')

                                @include('cpanel.visa.accounts._account-info')

                                @include('cpanel.visa.accounts._roles-and-permissions-create')

                                <button type="submit" class="btn btn-sm btn-primary pull-right m-t">
                                    <strong>Save</strong>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
	
	{!! Html::script(mix('js/visa/accounts/index.js', 'cpanel')) !!}
    {!! Html::script('cpanel/plugins/dualListbox/jquery.bootstrap-duallistbox.js') !!}

<script>
    $(document).ready(function () {
        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160,
        });
    });
</script>    

@endpush
