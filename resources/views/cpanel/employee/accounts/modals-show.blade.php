<modal size="modal-md" id='fileLeaveModal'>
    <template slot="modal-title">
        File a Leave
    </template>
    <template slot="modal-body">
    	<file-leave></file-leave>
    </template>
</modal>