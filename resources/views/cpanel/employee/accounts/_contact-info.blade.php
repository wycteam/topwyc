<legend class="m-t">Contact Information</legend>

<div class="row">
    <div class="col-md-8">
        <div class="form-group{{ $errors->has('address.address') ? ' has-error' : '' }}">
            <label>Address</label>
            {!! Form::hidden('address[id]', null) !!}
            {!! Form::text('address[address]', null, ['id' => 'address', 'class' => 'form-control', 'placeholder' => 'Enter address']) !!}

            @if ($errors->has('address.address'))
            <span class="help-block">
                {{ $errors->first('address.address') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Barangay</label>
            {!! Form::text('address[barangay]', null, ['id' => 'barangay', 'class' => 'form-control', 'placeholder' => 'Enter barangay']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('address.city') ? ' has-error' : '' }}">
            <label>City</label>
            {!! Form::text('address[city]', null, ['id' => 'city', 'class' => 'form-control', 'placeholder' => 'Enter city']) !!}

            @if ($errors->has('address.city'))
            <span class="help-block">
                {{ $errors->first('address.city') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('address.province') ? ' has-error' : '' }}">
            <label>Province</label>
            {!! Form::text('address[province]', null, ['id' => 'province', 'class' => 'form-control', 'placeholder' => 'Enter province']) !!}

            @if ($errors->has('address.province'))
            <span class="help-block">
                {{ $errors->first('address.province') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Zip Code</label>
            {!! Form::text('address[zip_code]', null, ['id' => 'zip_code', 'class' => 'form-control', 'placeholder' => 'Enter zip code']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('address.contact_number') ? ' has-error' : '' }}">
            <label>Contact Number 1</label>
            {!! Form::text('address[contact_number]', null, ['id' => 'contact_number', 'class' => 'form-control', 'placeholder' => 'Enter contact number']) !!}

            @if ($errors->has('address.contact_number'))
            <span class="help-block">
                {{ $errors->first('address.contact_number') }}
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Contact Number 2</label>
            {!! Form::text('address[alternate_contact_number]', null, ['id' => 'alternate_contact_number', 'class' => 'form-control', 'placeholder' => 'Enter contact number']) !!}
        </div>
    </div>
</div>

