<td class="project-status text-center" width="5%">
    @{{stores.id}}
</td>
<td class="project-title text-left" width="30%">
    <a :href="main_url+/stores/+stores.slug" target="_blank">@{{stores.name}}</a>
    <br/>
    <small>Created @{{stores.created_at | invoiceFormat}}</small>
</td>
<td class="project-title text-left" width="25%">
    <a :href="/users/+stores.id" target="_blank">@{{stores.first_name}} @{{stores.last_name}}</a>
    <br/>
    <span class="label label-primary" v-if="stores.is_verified">verified</span>
    <span class="label label-primary" v-else>not verified</span>
</td>
<td class="text-center" width="5%">
   @{{ stores.products_count }}
</td>