<td class="project-status text-center" width="5%">
    @{{store.id}}
</td>
<td class="project-title text-left" width="30%">
    <a :href="main_url+/stores/+store.slug" target="_blank">@{{store.name}}</a>
    <br/>
    <small>Created @{{store.created_at.date | invoiceFormat}}</small>
</td>
<td class="project-title text-left" width="25%">
    <a :href="/users/+store.user_id" target="_blank">@{{store.first_name}} @{{store.last_name}}</a>
    <br/>
    <span class="label label-primary" v-if="store.is_verified">verified</span>
    <span class="label label-primary" v-else>not verified</span>
</td>
<td class="text-center" width="5%">
   @{{ store.products_count }}
</td>