<dialog-modal id='dialogRemove'>
	<template slot="modal-title">
        Are you sure you want to delete this store?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selStore.profile_image" width="50"></i>
            <h2>
                <b>@{{selStore.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='deleteStore(selStore.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogDeac'>
	<template slot="modal-title">
        Are you sure you want to deactivate this store?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selStore.profile_image" width="50"></i>
            <h2>
                <b>@{{selStore.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='deacStore(selStore.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogDemote'>
	<template slot="modal-title">
        Are you sure you want to demote this store?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selStore.profile_image" width="50"></i>
            <h2>
                <b>@{{selStore.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='demoteStore(selStore.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogRecycle'>
	<template slot="modal-title">
        Are you sure you want to restore this store?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selStore.profile_image" width="50"></i>
            <h2>
                <b>@{{selStore.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='recycleStore(selStore.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogPermaDelete'>
	<template slot="modal-title">
        Are you sure you want to delete this store permanently?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selStore.profile_image" width="50"></i>
            <h2>
                <b>@{{selStore.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='PermaDeletetore(selStore.id)'>Proceed</button>
    </template>
</dialog-modal>