@extends('cpanel.layouts.master')

@section('title', 'Stores List')

@push('styles')
   <!--  {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/dataTables.bootstrap.css') !!} -->
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Stores</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Stores</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content" id="storepage">
        @include('cpanel.store.modals')
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <div class="tabs-container">
                        <h1 class="pull-right m-r-lg m-t-lg dt-title"><b>List of Stores</b></h1>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1">
                                    <div class="ibox-title">
                                        <span class="label label-success pull-right">Active</span>
                                        <h3>Stores</h3>
                                    </div>
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-2">
                                    <div class="ibox-title">
                                        <span class="label label-warning pull-right">Deactivated</span>
                                        <h3>Stores</h3>
                                    </div>
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-3">
                                    <div class="ibox-title">
                                        <span class="label label-primary pull-right">Featured</span>
                                        <h3>Stores</h3>
                                    </div>
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-4">
                                    <div class="ibox-title">
                                        <span class="label label-danger pull-right">Deleted</span>
                                        <h3>Stores</h3>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content ">
                            <div id="tab-1" class="tab-pane active ibox">
                                 <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-stores" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Store Name</th>
                                                        <th>Store Owner</th>
                                                        <th>Products</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="store in stores" :item="store" :key="store.id" v-if="store.is_active==1 && store.deleted_at==null && store.status == null">
                                                        @include('cpanel.store.include')
                                                        <td>
                                                            <a href="#" class="btn btn-success btn-outline btn-sm" @click='upStore(store.id,"promote")'><i class="fa fa-certificate" ></i> Promote </a>
                                                            <a href="#" class="btn btn-warning btn-outline btn-sm" @click='showDeacDialog(store)'><i class="fa fa-toggle-on"></i> Deactivate </a>
                                                            <a href="#" class="btn btn-danger btn-outline btn-sm" @click='showRemoveDialog(store)'><i class="fa fa-trash" ></i> Delete </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane ibox">
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-stores" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Store Name</th>
                                                        <th>Store Owner</th>
                                                        <th>Products</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="store in stores" :item="store" :key="store.id" v-if="store.is_active==0 && store.deleted_at==null">
                                                        @include('cpanel.store.include')
                                                        <td>
                                                            <a href="#" class="btn btn-success btn-outline btn-sm" @click='upStore(store.id,"activate")'><i class="fa fa-toggle-on"></i> Activate </a>
                                                            <a href="#" class="btn btn-danger btn-outline btn-sm" @click='showRemoveDialog(store)'><i class="fa fa-trash"></i> Delete </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane ibox">
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-stores" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Store Name</th>
                                                        <th>Store Owner</th>
                                                        <th>Products</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="store in stores" :item="store" :key="store.id" v-if="store.is_active==1 && store.status=='featured'">
                                                        @include('cpanel.store.include')
                                                        <td>
                                                            <a href="#" class="btn btn-success btn-outline btn-sm" @click='showDemoteDialog(store)'><i class="fa fa-bullhorn"></i> Remove Promotion </a>
                                                            <a href="#" class="btn btn-danger btn-outline btn-sm" @click='showRemoveDialog(store)'><i class="fa fa-trash" ></i> Delete </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-content" style="display:none">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-stores" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Store Name</th>
                                                        <th>Store Owner</th>
                                                        <th>Products</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="stores in featuredstores" :item="stores" :key="stores.id">
                                                        @include('cpanel.store.featured')
                                                        <td>
                                                            <a href="#" class="btn btn-success btn-outline btn-sm" @click='showDemoteDialog(store)'><i class="fa fa-bullhorn"></i> Remove Promotion </a>
                                                            <a href="#" class="btn btn-danger btn-outline btn-sm" @click='showRemoveDialog(store)'><i class="fa fa-trash" ></i> Delete </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-pane ibox">
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-stores" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Store Name</th>
                                                        <th>Store Owner</th>
                                                        <th>Products</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="store, index in stores" :item="store" :key="store.id" v-if="store.deleted_at!=null">
                                                        @include('cpanel.store.include')
                                                        <td width="35%">
                                                            <a href="#" class="btn btn-success btn-outline btn-sm" @click='showRecycleDialog(store)'><i class="fa fa-recycle"></i> Restore </a>
                                                            <a href="#" class="btn btn-danger btn-outline btn-sm" @click='showPermaDialog(store,index)'><i class="fa fa-trash"></i> Delete Permanently</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {!! Html::script(mix('js/stores.js','cpanel')) !!}
    <!-- {!! Html::script('cpanel/plugins/dataTables/datatables.min.js') !!} -->
@endpush
