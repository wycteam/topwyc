@extends('cpanel.layouts.master')

@section('title', 'List of Users')



@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-9">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Users</strong>
                </li>
            </ol>
        </div>
        <div class="col-md-3">
            <a href="{{ route('cpanel.users.create') }}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-user-plus fa-fw" ></i> Add New User</a>
        </div>
    </div>

    <div class="wrapper wrapper-content animated" id="userlist">
        @include('cpanel.user.modals')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>User List</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example htFixTable" >
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Ewallet Bal.</th>
                        <th>Gender</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr  v-for="user in users" :item="user" :key="user.id">
                        <td>@{{ user.id }}</td>
                        <td>@{{ user.first_name }} @{{ user.last_name }}</td>
                        <td>@{{ user.email }}</td>
                        <td>@{{ user.ewallet.amount | currency}}</td>
                        <td class="text-center">@{{ user.gender }}</td>
                        <td class="text-center">
                            <span class="label label-primary" v-if="user.is_docs_verified">verified</span>
                            <span class="label label-warning" v-else>not verified</span>
                        </td>
                        <td class="text-center">
                            <a href="#" class="btn btn-danger btn-outline btn-xs" @click='showRemoveDialog(user)'>Delete</a>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
    </div>
@endsection

@push('scripts')
    {!! Html::script(mix('js/user.js','cpanel')) !!}
<script>
    $(document).ready(function(){
        // $('.dataTables-example').DataTable({
        //     pageLength: 10,
        //     responsive: true,
        //     dom: '<"top"lf>rt<"bottom"ip><"clear">'

        // });

    });

</script>
@endpush
