@extends('cpanel.layouts.master')

@section('title', 'User Form')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Create New User</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/users">Users</a>
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div id="userlist" class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>User Form</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(['route' => 'cpanel.users.store', 'method' => 'post']) !!}
                    <div class="row">
                        <div class="col-xs-12">
                            @if (session()->has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{ session()->get('message') }}
                            </div>
                            @endif

                            @include('cpanel.user._personal-info')

                            @include('cpanel.user._contact-info')

                            @include('cpanel.user._account-info')

                            @include('cpanel.user._roles-and-permissions-create')

                            <button type="submit" class="btn btn-sm btn-primary pull-right m-t">
                                <strong>Save</strong>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
{!! Html::style('cpanel/plugins/dualListbox/bootstrap-duallistbox.min.css') !!}
@endpush

@push('scripts')
{!! Html::script('cpanel/plugins/dualListbox/jquery.bootstrap-duallistbox.js') !!}

<script>
    $(document).ready(function () {
        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160,
        });
    });
</script>
@endpush
