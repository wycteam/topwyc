<!-- Create Account Modal -->
<modal size="modal-lg" class="custom-chosen report-product-modal" id='addQuestion'>
    <template slot="modal-title">
        <h3 id="title"></h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body" class="product-details-set">
    	
	</template>
</modal>

<dialog-modal id='dialogRemove'>
	<template slot="modal-title">
        Are you sure you want to delete this user?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="seluser.avatar" width="50"></i>
            <h2>
                <b>@{{seluser.first_name}} @{{seluser.last_name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{seluser.created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click="deleteUser">Proceed</button>
    </template>
</dialog-modal>