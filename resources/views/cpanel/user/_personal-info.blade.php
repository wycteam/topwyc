<legend>Personal Information</legend>

<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            <label>First Name</label>
            {!! Form::text('first_name', null, ['id' => 'first_name', 'class' => 'form-control', 'placeholder' => 'Enter first name']) !!}

            @if ($errors->has('first_name'))
            <span class="help-block">
                {{ $errors->first('first_name') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Middle Name</label>
            {!! Form::text('middle_name', null, ['id' => 'middle_name', 'class' => 'form-control', 'placeholder' => 'Enter middle name']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
            <label>Last Name</label>
            {!! Form::text('last_name', null, ['id' => 'last_name', 'class' => 'form-control', 'placeholder' => 'Enter last name']) !!}

            @if ($errors->has('last_name'))
            <span class="help-block">
                {{ $errors->first('last_name') }}
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
            <label>Date of Birth</label>
            {!! Form::date('birth_date', null, ['id' => 'birth_date', 'class' => 'form-control', 'placeholder' => 'Enter date of birth']) !!}

            @if ($errors->has('birth_date'))
            <span class="help-block">
                {{ $errors->first('birth_date') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            <label>Gender</label>
            {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null, ['class' => 'form-control', 'placeholder' => 'Select gender']) !!}

            @if ($errors->has('gender'))
            <span class="help-block">
                {{ $errors->first('gender') }}
            </span>
            @endif
        </div>
    </div>
</div>