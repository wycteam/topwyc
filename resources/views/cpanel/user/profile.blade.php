@extends('cpanel.layouts.master')

@section('title', 'User Profile')

@push('styles')
    {!! Html::style('cpanel/plugins/dualListbox/bootstrap-duallistbox.min.css') !!}
    {!! Html::style('cpanel/plugins/blueimp/css/blueimp-gallery.min.css') !!}
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}

    <style type="text/css">
        .scrollable-content{
            max-height: 565px;
            overflow-x: hidden;
            overflow-y: auto;
            border: 1px solid #ccc;
            padding: 10px;
        }
        .edit-input{
            border: 0px !important;
            width: 100px;
            padding-bottom: 20px;
        }
        .edit-textarea{
            border: 0px !important;
            width: 100%;
            padding-bottom: 20px;
        }
    </style>
@endpush

@push('scripts')
    {!! Html::script('cpanel/plugins/dualListbox/jquery.bootstrap-duallistbox.js') !!}
    {!! Html::script('cpanel/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script(mix('js/userprofile.js','cpanel')) !!}
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-9">
            <h2>User Profile</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/users">Users</a>
                </li>
                <li class="active">
                    <strong>Profile</strong>
                </li>
            </ol>
        </div>
        <div class="col-md-3">
            <a href="{{ route('cpanel.users.edit', $selectedUser->id) }}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-edit fa-fw" ></i> Edit</a>
        </div>
    </div>

    <div class="wrapper wrapper-content" id="userprofile">
            @include('cpanel.modals.userModals')
            <div class="row">

                <div class="col-sm-4">
                    <div class="ibox ">

                        <div class="ibox-content">
                            <div class="tab-content">
                                <div id="contact-1" class="tab-pane active">
                                    <div class="row m-b-lg">
                                        <div class="col-lg-4 text-center">
                                        <div class="profile-image">
                                            <img :src="avatar" class="img-circle circle-border m-b-md" alt="profile">
                                        </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <h2 class="no-margins">
                                                @{{ fullname }}
                                            </h2>
                                            <h4><p>
                                                <span class="label label-primary" v-if="is_docs_verified">Verified</span>
                                                <span class="label label-danger" v-else>Not Verified</span>
                                            </p></h4>
                                            <i>@{{ email }}</i>

                                        </div>
                                    </div>
                                    <div class="client-detail">
                                    <div class="full-height-scroll">

                                        <strong><h4>Basic Information</h4></strong>

                                        <ul class="list-group clear-list">
                                            <li class="list-group-item first-item">
                                                <b>Ewallet Balance</b>
                                                <span class="pull-right m-r-sm"> @{{ewallet_balance | currency}} </span>
                                            </li>
                                            <li class="list-group-item ">
                                                <span class="pull-right m-r-sm"> @{{bday | age}} </span>
                                                <b>Age</b>
                                            </li>
                                            <li class="list-group-item">
                                                <!-- <span class="pull-right"> @{{ bday }} </span> -->
                                                <span class="pull-right">
                                                <input v-model="bday" class="form-control edit-input" id="birth_date" type="text" name="birth_date" data-date-format="yyyy-mm-dd" >
                                                <input type="hidden" id="hiddendate" v-model="bday">
                                                </span>
                                                <b><a href="#"></a> Birth Date </b>
                                            </li>
                                            <li class="list-group-item">
                                                <b><a href="#"></a> Address </b><br>
                                                <span><input v-if="address != null" v-model="address" class="form-control edit-textarea" id="address" type="text" name="address" @blur="saveAddress()"></span>
                                            </li>
                                        </ul>
                                        <hr/>
                                        <strong>Comment History</strong>
                                        <div id="vertical-timeline" class="vertical-container dark-timeline">
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon gray-bg">
                                                    <i class="fa fa-coffee"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <p>Conference on the sales results for the previous year.
                                                    </p>
                                                    <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014 </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon gray-bg">
                                                    <i class="fa fa-briefcase"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <p>Many desktop publishing packages and web page editors now use Lorem.
                                                    </p>
                                                    <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon gray-bg">
                                                    <i class="fa fa-bolt"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <p>There are many variations of passages of Lorem Ipsum available.
                                                    </p>
                                                    <span class="vertical-date small text-muted"> 06:10 pm - 11.03.2014 </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-warning"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <p>The generated Lorem Ipsum is therefore.
                                                    </p>
                                                    <span class="vertical-date small text-muted"> 02:50 pm - 03.10.2014 </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon gray-bg">
                                                    <i class="fa fa-coffee"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <p>Conference on the sales results for the previous year.
                                                    </p>
                                                    <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014 </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon gray-bg">
                                                    <i class="fa fa-briefcase"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <p>Many desktop publishing packages and web page editors now use Lorem.
                                                    </p>
                                                    <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="col-sm-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
<!--                                 <span class="label label-success pull-right">Annual</span>
 -->                                <h5>Stores</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">@{{stores.length}}</h1>
<!--                                 <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
 -->                                <small>
                                        <p class="no-margins">Active Stores</p>
                                        <p>@{{activestores.length}}</p>
                                    </small>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
<!--                                 <span class="label label-success pull-right">Annual</span>
 -->                                <h5>Products</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">@{{products.length}}</h1>
<!--                                 <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
 -->                                <small>
                                        <p class="no-margins">Active Products</p>
                                        <p>@{{activeprods.length}}</p>
                                    </small>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
<!--                                 <span class="label label-info pull-right">Annual</span>
 -->                                <h5>Sales</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">@{{countSold}}</h1>
<!--                                 <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
 -->                                <small>
                                        <p class="no-margins">Complete Transactions</p>
                                        <p>@{{sales.length}}</p>
                                    </small>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
<!--                                 <span class="label label-info pull-right">Annual</span>
 -->                                <h5>Purchases</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">@{{orders.length}}</h1>
<!--                                 <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
 -->                                <small>
                                        <p class="no-margins">Complete Orders</p>
                                        <p>@{{comporders.length}}</p>
                                    </small>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-store"> Stores</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-products">Products</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-orders">Orders</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-ewallet">Ewallet</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-documents">Documents</a></li>
                            @if(Auth::user()->hasRole('master') || Auth::user()->hasRole('vice-master'))
                                <li class=""><a data-toggle="tab" href="#tab-access-control">Access Control</a></li>
                            @endif
                               <!--  <li class=""><a data-toggle="tab" href="#tab-comments">Comments</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-reviews">Reviews</a></li> -->
                            </ul>
                            <div class="tab-content">
                                <div id="tab-store" class="tab-pane active">
                                    <div class="panel-body">
                                        <user-stores
                                            v-for="store in stores"
                                            :item="store"
                                            :key="store.id"
                                          >
                                        </user-stores>
                                    </div>
                                </div>
                                <div id="tab-products" class="tab-pane">
                                    <div class="panel-body">
                                          <user-products
                                            v-for="product in products"
                                            :item="product"
                                            :key="product.id"
                                          >
                                          </user-products>
                                    </div>
                                </div>
                                <div id="tab-orders" class="tab-pane">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox">
                                                <div class="ibox-content">
                                                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                                        <thead>
                                                        <tr>

                                                            <th>Order ID</th>
                                                            <th data-hide="phone">Status</th>
                                                            <th data-hide="phone">Customer</th>
                                                            <th data-hide="phone">Amount</th>
                                                            <th data-hide="phone">Tracking #</th>
                                                            <th data-hide="phone,tablet" >Date Ordered</th>
                                                            <th class="text-right">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr v-for="order in orders" :item="order" :key="order.id">
                                                                <td>
                                                                   @{{ order.id }}
                                                                </td>
                                                                <td>
                                                                    <span class="label" :class="order.status =='Incomplete'?'label-warning':'label-primary'">@{{ order.status }}</span>
                                                                </td>
                                                                <td>
                                                                    @{{ order.name_of_receiver }}
                                                                </td>
                                                                <td>
                                                                    P@{{ order.total }}
                                                                </td>
                                                                <td>
                                                                    @{{ order.tracking_number }}
                                                                </td>
                                                                <td>
                                                                    @{{ order.created_at | formatDate }}
                                                                </td>
                                                                <td class="text-right">
                                                                    <div class="btn-group">
                                                                        <button type="button" class="btn-white btn btn-xs" :id="order.id" @click='showOrderDetails(order.id,order.tracking_number,order.contact_number,order.name_of_receiver,order.created_at)'>View</button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-ewallet" class="tab-pane">
                                     @include('cpanel.includes.user.ewallet-content')
                                </div>

                                <div id="tab-documents" class="tab-pane">
                                     @include('cpanel.includes.user.doc-content')
                                </div>

                            @if(Auth::user()->hasRole('master') || Auth::user()->hasRole('vice-master') || Auth::user()->hasRole('cpanel-admin'))
                                <div id="tab-access-control" class="tab-pane">
                            @endif
                                    @include('cpanel.includes.user.access-control')
                                </div>

                                <div id="tab-comments" class="tab-pane">
                                </div>

                                <div id="tab-reviews" class="tab-pane">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <!-- end col-sm-8 -->
            </div>
    </div>

@endsection

@push('styles')
@endpush

@push('scripts')
<script>
    // $(function(){
    //   $("#birth_date" ).val($('#hiddendate').val());
    //     $('#birth_date').datepicker().on('changeDate', function (ev) {
    //         $("#hiddendate" ).val($('#birth_date').val());
    //     });
    // });
</script>
@endpush


