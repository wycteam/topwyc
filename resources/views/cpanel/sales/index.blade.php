@extends('cpanel.layouts.master')

@section('title', 'Sales/Purchase')

@push('styles')
    {!! Html::style(mix('css/sales.css','cpanel')) !!}
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
    {!! Html::style('components/sweetalert2/dist/sweetalert2.min.css') !!}
@endpush

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Sales/Purchase</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Sales/Purchase</strong>
                </li>
            </ol>
            <a href="/export-daily-orders" target="_blank" class="btn btn-primary pull-right" style="margin-top:18px;">Export Daily Orders (.xlsx)</a>
        </div>
    </div>
    
<div id="orders-content" class="wrapper wrapper-content">
    @include('cpanel.includes.sales.modals')
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <div class="tabs-container">
                    <h1 class="pull-right m-r-lg m-t-lg dt-title"><b>List of Sales/Purchase</b></h1>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#tab-1">
                                <div class="ibox-title">
                                    <span class="label label-info">Pending</span>
                                </div>
                            </a>
                        </li>                     
                        <li class="">
                            <a data-toggle="tab" href="#tab-2">
                                <div class="ibox-title">
                                    <span class="label label-primary">In Transit</span>
                                </div>
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#tab-3">
                                <div class="ibox-title">
                                    <span class="label label-success">Completed</span>
                                </div>
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#tab-4">
                                <div class="ibox-title">
                                    <span class="label label-warning">Returned</span>
                                </div>
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#tab-5">
                                <div class="ibox-title">
                                    <span class="label label-danger">Cancelled</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content ">
                        <div id="tab-1" class="tab-pane active ibox">
                             <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wandering-cubes">
                                    <div class="sk-cube1"></div>
                                    <div class="sk-cube2"></div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover dt-sales">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Tracking No.</th>
                                                    <th class="text-center">Product</th>
                                                    <th class="text-center">Buyer</th>
                                                    <th class="text-center">Seller</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="sale in sales" :item="sale" :key="sale.id" v-if="sale.status=='Pending' ">
                                                    @include('cpanel.sales.include')
                                                    <td>
                                                        <a class="btn btn-info btn-outline btn-sm" @click="openModal(sale.id)"><i class="fa fa-eye"></i> View </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane ibox">
                             <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wandering-cubes">
                                    <div class="sk-cube1"></div>
                                    <div class="sk-cube2"></div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover dt-sales">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Tracking No.</th>
                                                    <th class="text-center">Product</th>
                                                    <th class="text-center">Buyer</th>
                                                    <th class="text-center">Seller</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="sale in sales" :item="sale" :key="sale.id" v-if="sale.status=='In Transit' ">
                                                    @include('cpanel.sales.include')
                                                    <td>
                                                        <a class="btn btn-info btn-outline btn-sm" @click="openModal(sale.id)"><i class="fa fa-eye"></i> View </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane ibox">
                             <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wandering-cubes">
                                    <div class="sk-cube1"></div>
                                    <div class="sk-cube2"></div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover dt-sales" >
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Tracking No.</th>
                                                    <th class="text-center">Product</th>
                                                    <th class="text-center">Buyer</th>
                                                    <th class="text-center">Seller</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="sale in sales" :item="sale" :key="sale.id" v-if="sale.status=='Delivered' || sale.status=='Received' ">
                                                    @include('cpanel.sales.include')
                                                    <td>
                                                        <a class="btn btn-info btn-outline btn-sm" @click="openModal(sale.id)"><i class="fa fa-eye"></i> View </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane ibox">
                             <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wandering-cubes">
                                    <div class="sk-cube1"></div>
                                    <div class="sk-cube2"></div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover dt-sales" >
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Tracking No.</th>
                                                    <th class="text-center">Product</th>
                                                    <th class="text-center">Buyer</th>
                                                    <th class="text-center">Seller</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="sale in sales" :item="sale" :key="sale.id" v-if="sale.status=='Returned' ">
                                                    @include('cpanel.sales.include')
                                                    <td>
                                                        <a class="btn btn-info btn-outline btn-sm" @click="openModal(sale.id)"><i class="fa fa-eye"></i> View </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-5" class="tab-pane ibox">
                             <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wandering-cubes">
                                    <div class="sk-cube1"></div>
                                    <div class="sk-cube2"></div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover dt-sales" >
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Tracking No.</th>
                                                    <th class="text-center">Product</th>
                                                    <th class="text-center">Buyer</th>
                                                    <th class="text-center">Seller</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="sale in sales" :item="sale" :key="sale.id" v-if="sale.status=='Cancelled' ">
                                                    @include('cpanel.sales.include')
                                                    <td>
                                                        <a class="btn btn-info btn-outline btn-sm" @click="openModal(sale.id)"><i class="fa fa-eye"></i> View </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    {!! Html::script('cpanel/plugins/dataTables/datatables.min.js') !!}
    {!! Html::script('components/sweetalert2/dist/sweetalert2.min.js') !!}
    {!! Html::script(mix('js/pages/sales.js', 'cpanel')) !!}
@endpush
