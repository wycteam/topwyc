@extends('cpanel.layouts.master')

@section('title', 'FAQ')

@push('scripts')
    {!! Html::script(mix('js/faq.js','cpanel')) !!}
@endpush

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-9">
        <h2>FAQ</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>FAQ</strong>
            </li>
        </ol>
    </div>
    <div class="col-md-3">
        <a class="btn btn-primary btn-sm pull-right" @click="openModal"><i class="fa fa-plus" ></i> Add question</a>
    </div>
</div>

<div class="wrapper wrapper-content animated" id="faq-manager">
     @include('cpanel.includes.faq.modals')
     <div class="row">
        <div class="col-lg-12">
            <div class="wrapper animated">
                <faq-panel
                v-for="item in faq"
                :item="item"
                :key="item.id"
                >
                </faq-panel>
            </div>
        </div>
    </div>
</div>
@endsection
