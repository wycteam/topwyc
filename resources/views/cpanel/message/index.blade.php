@extends('cpanel.layouts.master')

@section('title', 'Messages')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Messages</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Messages</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div id="chatbox" class="ibox chatbox">
                <div class="ibox-title">
                    <h5>Chatbox</h5>
                    {{-- <h5>@{{ fullName }}</h5> --}}
                    <div class="ibox-tools">
                        <wyc-user-search target=".new-message" panel-class="messages-user-search"></wyc-user-search>
                        <a @click.prevent="newMessage" href="#" class="new-message">
                            <i class="fa fa-edit"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-3 left-pane">
                                <ul v-if="userLatestMessages.length" id="user-message">
                                    <li is="wyc-message"
                                        v-for="message in userLatestMessages"
                                        :message="message"
                                        :key="message.id">
                                    </li>
                                </ul>
                                <h4 v-else class="text-center">You have no messages</h4>
                            </div>
                            <div class="col-xs-9 right-pane">
                                <router-view></router-view>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
{!! Html::style('components/jQuery-contextMenu/dist/jquery.contextMenu.min.css') !!}
{!! Html::style(mix('css/pages/messages.css', 'cpanel')) !!}
@endpush

@push('scripts')
{!! Html::script('components/jQuery-contextMenu/dist/jquery.ui.position.min.js') !!}
{!! Html::script('components/jQuery-contextMenu/dist/jquery.contextMenu.min.js') !!}
{!! Html::script(mix('js/pages/messages.js', 'cpanel')) !!}
@endpush
