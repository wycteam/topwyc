<!-- @extends('cpanel.layouts.master') -->

@section('title', 'Account Verification')

@push('styles')
  {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
  {!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.css") !!}
  {!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css") !!}
  {!! Html::style("cpanel/plugins/nouslider/jquery.nouislider.css") !!}
  {!! Html::style(mix('css/pages/profile.css', 'cpanel')) !!}
@endpush
@push('scripts')
  {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
  {!! Html::script("cpanel/plugins/jquery-ui/jui-draggable.min.js") !!}
  {!! Html::script('cpanel/plugins/jasny/jasny-bootstrap.min.js') !!}
  {!! Html::script(mix('js/documents.js','cpanel')) !!}
@endpush

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Account Verification</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Account Verification</strong>
                </li>
            </ol>
        </div>
    </div>

<div class="wrapper wrapper-content animated" id="documents-manager">
    @include('cpanel.includes.documents.modals')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">     
            <div class="row">
                <div class="col-md-6">

                </div>

               <div class="col-md-6 text-right" style="margin-top: 20px;">
                <button type="button" :disabled="disableBtns" class="btn btn-outline btn-default" @click="changeView('all')">All </button>
                <button type="button" :disabled="disableBtns" class="btn btn-outline btn-warning" @click="changeView('pending')">Pending </button>
                <button type="button" :disabled="disableBtns" class="btn btn-outline btn-primary" @click="changeView('verified')">Verified</button>
                <button type="button" :disabled="disableBtns" class="btn btn-outline btn-danger"  @click="changeView('denied')">Denied</button>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table id="documentslist" class="table table-bordered table-hover dt-stores " >
                                                <thead>
                                                   <tr>
                                                    <th>Id</th>
                                                    <th class="col-sm-1">Name</th>
                                                    <th class="col-sm-1">Type</th>
                                                    <th class="col-sm-1">Status</th>
                                                    <th class="col-sm-2">Upload Date</th>
                                                    <th class="col-sm-2">Valid Until</th>
                                                    <th class="col-sm-3">Reason</th>
                                                    <th class="col-sm-3">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   <tr
                                                   is="documentlist"
                                                   v-for="docs in showlist"
                                                   :docs = "docs"
                                                   :key = "docs.id"
                                                   ></tr>
                                               </tbody>
                                            </table>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
