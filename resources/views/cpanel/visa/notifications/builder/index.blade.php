@extends('cpanel.layouts.master')

@section('title', 'Notification Builder')

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Notification Builder</h2>
        </div>
    </div>

    <div id="notification-builder" class="wrapper wrapper-content" v-cloak>

    	@include('cpanel.includes.visa.notifications.builder.modals')

    	<div class="row">
    		<div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1"> Report Types</a></li>
                    <li class=""><a data-toggle="tab" href="#tab-2">Docs</a></li>
                </ul>
                <div class="tab-content">

                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">

                        	<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add-report-type-modal">Add Report Type</button>

                        	<div style="height: 10px;clear:both;"></div>

                        	<table id="report-types-table" class="table table-striped table-bordered table-hover">
                        		<thead>
                        			<tr>
                        				<th>Title</th>
                        				<th>Title CN</th>
                        				<th>Services</th>
                        				<th>With Docs</th>
                        				<th class="text-center">Action</th>
                        				<th class="text-center">Action</th>
                        			</tr>
                        		</thead>
                        		<tbody>
                        			<tr v-for="reportType in reportTypes">
                        				<td>@{{ reportType.title }}</td>
                        				<td>@{{ reportType.title_cn }}</td>
                        				<td>
                        					<ul>
                        						<li v-for="serviceName in reportType.services_name">
                        							@{{ serviceName }}
                        						</li>
                        					</ul>
                        				</td>
                        				<td>@{{ (reportType.with_docs == 1) ? 'YES' : 'NO' }}</td>
                        				<td class="text-center">
                        					<a href="#" data-toggle="modal" data-target="#update-report-type-modal" @click="setSelectedReportTypeId(reportType.id) + setSelectedReportType(reportType)">Update</a>
                        				</td>
                        				<td class="text-center">
                        					<a href="#" data-toggle="modal" data-target="#delete-report-type-modal" @click="setSelectedReportTypeId(reportType.id)">Delete</a>
                        				</td>
                        			</tr>
                        		</tbody>
                        	</table>
                        </div>
                    </div> <!-- tab-1 -->

                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">

                            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add-document-modal">Add Document</button>

                        	<div style="height: 10px;clear:both;"></div>

                        	<table id="docs-table" class="table table-striped table-bordered table-hover">
                        		<thead>
                        			<tr>
                        				<th>Title</th>
                        				<th>Title CN</th>
                        				<th>Document Types</th>
                        				<th class="text-center">Action</th>
                        				<th class="text-center">Action</th>
                        			</tr>
                        		</thead>
                        		<tbody>
                        			<tr v-for="doc in docs">
                        				<td>@{{ doc.title }}</td>
                        				<td>@{{ doc.title_cn }}</td>
                        				<td>
                        					<ul>
                        						<li v-for="doc_type in doc.doc_types">@{{ doc_type.title }}</li>
                        					</ul>
                        				</td>
                        				<td class="text-center">
                        					<a href="#" data-toggle="modal" data-target="#update-document-modal" @click="setSelectedDocumentId(doc.id) + setSelectedDocument(doc)">Update</a>
                        				</td>
                        				<td class="text-center">
                        					<a href="#" data-toggle="modal" data-target="#delete-document-modal" @click="setSelectedDocumentId(doc.id)">Delete</a>
                        				</td>
                        			</tr>
                        		</tbody>
                        	</table>

                        </div>
                    </div> <!-- tab-2 -->

                </div>
            </div>
        </div>

    </div> <!-- notification-builder -->

@endsection

@push('styles')

    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    
@endpush

@push('scripts')

	{!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
	{!! Html::script('cpanel/plugins/typehead/bootstrap3-typeahead.min.js') !!}
    {!! Html::script(mix('js/visa/notifications/builder.js', 'cpanel')) !!}

@endpush