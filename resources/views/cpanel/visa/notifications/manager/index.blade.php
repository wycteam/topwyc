@extends('cpanel.layouts.master')

@section('title', 'Manager')

@push('styles')
    {!! Html::style('cpanel/plugins/select2/select2.min.css') !!}
@endpush

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Notification Manager</h2>
        </div>
    </div>

	<div id="manager" class="wrapper wrapper-content animated" v-cloak>
        <div class="row">
        	<div class="col-lg-12">

	        	<div style="height:10px; clear:both;"></div>

	        	<div class="ibox float-e-margins">
                    <div class="ibox-content">

					<div class="row">
					    <div class="col-md-4">
					        <div class="form-group">
					            <label>Report Type:</label>
					            <select id="reportType" name="reportType" class="form-control m-b select2">
					            	<option></option>
					                <option :value="item.id" v-for="item in reportType">
					                    @{{ item.title }}
					                </option>
					            </select>
					        </div>
					    </div>
					</div>

                    	<div class="table-responsive">
		                    <table id="lists" class="table table-striped table-bordered table-hover dataTables-example">
			                    <thead>
				                    <tr>
				                        <th>ID</th>
				                        <th>Name</th>
				                        <th>Cost</th>
				                        <th>Birthdate</th>
                        				<th class="no-sort"></th>
				                    </tr>
			                    </thead>
					                <tbody>
				                    </tbod>
			                </table>
			            </div>

						<a href="javascript:void(0)" id="selService" cl-ids="" class="pull-right" @click="addClient"><b>Continue</b></a>

			            <h3 style="font-weight: bold;">Add Reports to List of Clients Below</h3>

                    	<div class="table-responsive">
		                    <table id="reports" class="table table-striped table-bordered table-hover">
			                    <thead>
				                    <tr>
				                        <th style="width:20%">ID</th>
				                        <th>Name</th>
				                        <th style="width:20%">Action</th>
				                    </tr>
			                    </thead>
			                    <tbody>
			                    	
			                    </tbody>
			                    
			                </table>
			            </div>

                    </div>
                </div>

	        </div>

        </div>
    </div>

<script>
	function addClient(id,name){
      var clids = $("#selService").attr("cl-ids");
      if(clids!=""){
         clids += id;
         clids += ";";
      }
      else{
        clids = id+";";
      }
      $("#selService").attr("cl-ids",clids);
      var elem  = '<tr id="tr'+id+'">';
          elem += '<td class="text-center">'+id+'</td>';
          elem += '<td class="text-center">'+name+'</td>';
          elem += '<td class="text-center"><a href="javascript:void(0)" style="font-size:9px !important" onclick="removeClient(\''+id+'\')" class="btn btn-danger btn-xs" id="'+id+'"><span class="fa fa-cross"></span>Remove</a></td>';
          elem += '</tr>';
          $("#reports").append(elem);
          $("#add"+id).hide();
  	}
	function removeClient(id){
	          $("#tr"+id).remove();
	          $("#add"+id).show();
	          var ids = $("#selService").attr("cl-ids");
	          ids = ids.replace(id+";",'');
	          $("#selService").attr("cl-ids",ids);

	}
</script>

@endsection

@push('scripts')
    {!! Html::script('cpanel/plugins/select2/select2.full.min.js') !!}
	{!! Html::script(mix('js/visa/notifications/manager.js', 'cpanel')) !!}
@endpush