@extends('cpanel.layouts.master')

@section('title', 'Manager')

@push('styles')
    {!! Html::style(mix('css/visa/notifications/index.css', 'cpanel')) !!}
@endpush

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Notification Manager</h2>
        </div>
    </div>

	<div id="manager" class="wrapper wrapper-content animated">
        <div class="row">
        	<div class="col-lg-12">

	        	<div style="height:10px; clear:both;"></div>

	        	<div class="ibox float-e-margins">
                    <div class="ibox-content">
		                    	<div class="table-responsive">
				                    <table id="clientlist" class="table table-striped table-bordered table-hover">
				                    	<!-- HEADER TEXTS & COLUMNS MAY VARY -->
					                    <thead>
						                    <tr>
						                        <th>Client ID</th>
						                        <th>Name</th>
						                        <th>Action</th>
						                    </tr>
					                    </thead>
	                                    <tbody>
	                                    	<template v-for="client in clients">
	                                        <tr @click="showServices(client.id, $event)" class="clickable">
	                                       		<td>@{{ client.id }}</td>
	                                            <td>@{{ client.first_name }} @{{ client.last_name }} </td>
	                                        	<td class="text-center toggle-row">
	                                        		<i :id="'fa-'+client.id" class="fa fa-arrow-right cursor"></i>
	                                        	</td>
	                                        </tr>
	                                        <tr :id="'client-'+client.id" class="hide">
												<td colspan="5">

													<table class="table table-bordered services-tbl" >
														<thead>
															<tr>
																<td></td>
																<td>Date</td>
																<td>Package</td>
																<td>Details</td>
																<td>Cost</td>
																<td>Charge</td>
																<td>Tip</td>
																<td>Discount</td>
																<td></td>
															</tr>
														</thead>
														<tbody>
															<tr v-for="service in client.services" :class="{'hide': hideService(service)<=0, 'strikethrough': service.active == 0}">
																<td>
                                                                <i v-if="service.status.toUpperCase()=='PENDING'" class="fa fa-exclamation-circle" aria-hidden="true"></i>
																<i v-if="service.report.length!=0" class="fa fa-commenting" aria-hidden="true"></i>
																<i v-if="service.status.toUpperCase()!='PENDING' && service.report.length==0" class="fa fa-check-circle" aria-hidden="true"></i>
																</td>
																<td>@{{ service.service_date }}</td>
																<td>@{{ service.tracking }}</td>
																<td>@{{ service.detail }}</td>
																<td>@{{ service.cost | currency }}</td>
																<td>@{{ service.charge | currency }}</td>
																<td>@{{ service.tip | currency }}</td>					
						                        				<td><span v-if="service.discount!=null">@{{ service.discount.discount_amount | currency }}</span><span v-else>0</span></td>
						                        				<td><input @click="selectServices(service)" type="checkbox" :disabled="service.active == 0"></td>
															</tr>
														</tbody>
													</table>

												</td>
	                                        </tr>
	                                      	</template>
	                                    </tbody>

					                </table>
					            </div><!-- end table-responsive -->
					            <div v-if="selectedReportType.with_docs==1">
									<div id="docsContainer" v-cloak> 
										<div class="title-docs">Documents</div>
										<div>  
											<ul v-for="doc in docs">
												<li><input @click="selectDocs(doc)" type="checkbox" :disabled="doc.doc_types.length != 0"> @{{doc.title }}
													<ul v-for="doctype in doc.doc_types">
														<li><input @click="selectDocs(doctype)" type="checkbox"> @{{doctype.title }}</li>
													</ul>
												</li>
											</ul>
										</div> 
									</div>
					            </div>
					            <br><br><br>
					            <div class="btn-generate">
									<a href="javascript:void(0)" class="btn btn-success" @click="saveReport"><b>Generate Report</b></a>
								</div>

                    </div>
                </div>

	        </div>

        </div>
    </div>

@endsection

@push('scripts')
	
	{!! Html::script(mix('js/visa/notifications/manager.js', 'cpanel')) !!}

@endpush