@extends('cpanel.layouts.master')

@section('title', 'Advertisement')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-12">
        <h2>Advertisement</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">@lang('cpanel/news-page.home')</a>
            </li>
            <li class="active">
                <strong>Advertisement</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <ads-content id="adsContents" :translation="translation">

    </ads-content>

</div>
@endsection
@push('scripts')
	{!! Html::script(mix('js/visa/ads/ads.js', 'cpanel')) !!}
@endpush
