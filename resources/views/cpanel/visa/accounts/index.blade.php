@extends('cpanel.layouts.master')

@section('title', 'List of Accounts')



@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Accounts</h2>
            <a href="{{ url('/visa/cpanel-accounts/create') }}" class="m-l-3 p-l-1"><i class="fa fa-user-plus fa-fw" ></i> Add New Account</a>
        </div>
    </div>

    <div id="userlist" class="wrapper wrapper-content animated">
        @include('cpanel.visa.accounts.modals')
        <div class="row">
            <div class="col-lg-12">

                <div style="height:10px; clear:both;"></div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table id="lists" class="table table-striped table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>User Type</th>
                        <th class="text-center">Schedule Type</th>
                        <th class="text-center">Schedule Details</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr  v-for="user in users" :item="user" :key="user.id" v-cloak>
                        <td>@{{ user.id }}</td>
                        <td>@{{ user.first_name }} @{{ user.last_name }}</td>
                        <td>@{{ user.email }}</td>
                        <td>
                            <span v-for="(role, index) in user.roles">
                                @{{ role.label }}<span v-if="user.roles.length != index+1">,</span> 
                            </span>
                        </td>
                        <td class="text-center">
                            <span v-if="user.schedule" class="label label-primary">
                                @{{ user.schedule.schedule_type.name }}
                            </span>
                            <span v-else class="label label-danger">Not available</span>
                        </td>
                        <td class="text-center">
                            <template v-if="user.schedule">
                                <template v-if="user.schedule.schedule_type.name == 'Fixed'">
                                    Time In: @{{ parseMoment(user.schedule.time_in) }}
                                    <br />
                                    Time Out: @{{ parseMoment(user.schedule.time_out) }}
                                </template>
                                <template v-else-if="user.schedule.schedule_type.name == 'Flexi'">
                                    Time In Range: 
                                    <br>
                                    @{{ parseMoment(user.schedule.time_in_from) }} 
                                    - @{{ parseMoment(user.schedule.time_in_to) }}
                                </template>
                            </template>
                            <template v-else>
                                Not Available
                            </template>
                        </td>
                        <td class="text-center">
                            <template v-if="(user.roles.filter(role => role.label == 'Employee')).length == 1">
                                <a :href="'/emp/' + user.id" data-toggle="tooltip" title="View Employee" target="_blank"><i class="fa fa-eye"></i></a>
                                &nbsp;
                                <a href="javascript:void(0)" @click="editSchedule(user)" data-toggle="tooltip" title="Edit Schedule"><i class="fa fa-clock-o"></i></a>
                                &nbsp;
                            </template>

                            <a :href="'/visa/cpanel-accounts/'+user.id+'/edit'" title="Edit Account" target="_blank"><i class="fa fa-arrow-right"></i></a>
                            &nbsp;

                            <a href="javascript:void(0)" @click="showRemoveDialog(user)" title="Delete Account"><i class="fa fa-trash" style="color: red;"></i></a>

                            &nbsp;
                            <template v-if="(user.roles.filter(role => role.label == 'Agent')).length == 1">
                                <a :href="'/visa/access-control/downloadQR/'+user.id" target="_blank"  title="Generate QR Code"><i class="fa fa-qrcode" style="color: blue;"></i></a>
                            </template>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
    </div>
@endsection

@push('styles')
    {!! Html::style('components/bootstrap-timepicker/bootstrap-timepicker.min.css') !!}
@endpush

@push('scripts')
    {!! Html::script('components/bootstrap-timepicker/bootstrap-timepicker.min.js') !!}
    {!! Html::script(mix('js/visa/accounts/index.js','cpanel')) !!}
@endpush
