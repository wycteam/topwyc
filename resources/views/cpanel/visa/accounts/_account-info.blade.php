<legend class="m-t">Account Information</legend>
<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label>Email</label>
            {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Enter email']) !!}

            @if ($errors->has('email'))
            <span class="help-block">
                {{ $errors->first('email') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label>Password</label>
            {!! Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Enter password']) !!}

            @if ($errors->has('password'))
            <span class="help-block">
                {{ $errors->first('password') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Confirm Password</label>
            {!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => 'Retype password']) !!}
        </div>
    </div>
</div>