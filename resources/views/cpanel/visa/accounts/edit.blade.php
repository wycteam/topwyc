@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('cpanel/plugins/dualListbox/bootstrap-duallistbox.min.css') !!}
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>{{ $title }}</h2>
        </div>
    </div>

    <div id="editAccount" class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Account Form</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                       @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{ session()->get('message') }}
                        </div>
                        @endif
                        <form role="form" method="POST" action="{{ route('cpanel.visa.cpanel-accounts-update',$account[0]->id) }}">
                        {!! Form::token() !!}
                        @include('cpanel.visa.accounts.edit-modals')
                        <div class="row">
                            <div class="col-xs-12">

                            <legend>Personal Information</legend>
                            <div class="row">
                                <div class="col-md-2 avatar">
                                    <img :src="accountForm.avatar" class='img-responsive img-circle btnUpload center-block' data-toggle='tooltip' title="Upload Display Picture" style="cursor: pointer; max-width: 130px;">
                                    <div class="avatar-uploader hide">
                                      <imgfileupload 
                                        data-event="avatarUpload" 
                                        data-url="/visa/cpanel-accounts/update-avatar/{{$account[0]->id}}?width=300&height=300&type=avatar&resize=true"></imgfileupload>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                <label>First Name</label>
                                                <input class="form-control" id="first_name" name="first_name" type="text" placeholder="Enter first name" v-model="accountForm.first_name">

                                                @if ($errors->has('first_name'))
                                                <span class="help-block">
                                                    {{ $errors->first('first_name') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Middle Name</label>
                                                <input class="form-control" id="middle_name" name="middle_name" type="text" placeholder="Enter middle name" v-model="accountForm.middle_name">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                <label>Last Name</label>
                                                <input class="form-control" id="last_name" name="last_name" type="text" placeholder="Enter last name" v-model="accountForm.last_name">

                                                @if ($errors->has('last_name'))
                                                <span class="help-block">
                                                    {{ $errors->first('last_name') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                                                <label>Date of Birth</label>
                                                <input class="form-control" id="birth_date" name="birth_date" v-model="accountForm.birth_date" v-datepicker type="text" >

                                                @if ($errors->has('birth_date'))
                                                <span class="help-block">
                                                    {{ $errors->first('birth_date') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                                <label>Gender</label>
                                                <select class="form-control" id="gender" name="gender" v-model="accountForm.gender">
                                                    <option value="0">Select</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>

                                                @if ($errors->has('gender'))
                                                <span class="help-block">
                                                    {{ $errors->first('gender') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group{{ $errors->has('civil_status') ? ' has-error' : '' }}">
                                                <label>Civil Status</label>

                                                <select class="form-control" id="civil_status" name="civil_status" v-model="accountForm.civil_status">
                                                    <option value="0">Select</option>
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Widowed">Widowed</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Separated">Separated</option>
                                                </select>

                                                @if ($errors->has('civil_status'))
                                                <span class="help-block">
                                                    {{ $errors->first('civil_status') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Height</label>
                                                <input class="form-control" id="height" name="height" type="text" placeholder="Enter height" v-model="accountForm.height">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Weight</label>
                                                <input class="form-control" id="weight" name="weight" type="text" placeholder="Enter weight" v-model="accountForm.weight">

                                            </div>
                                        </div>
                                    </div>                                
                                </div>
                            </div>


                            <legend class="m-t">Contact Information</legend>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label>Address</label>
                                        <input class="form-control" id="address" name="address" type="text" placeholder="Enter address" v-model="accountForm.address">

                                        @if ($errors->has('address'))
                                        <span class="help-block">
                                            {{ $errors->first('address') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
                                        <label>Contact Number 1</label>
                                        <input class="form-control" id="contact_number" name="contact_number" type="text" placeholder="Enter contact number" v-model="accountForm.contact_number">

                                        @if ($errors->has('contact_number'))
                                        <span class="help-block">
                                            {{ $errors->first('contact_number') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Contact Number 2</label>
                                        <input class="form-control" id="alternate_contact_number" name="alternate_contact_number" type="text" placeholder="Enter contact number" v-model="accountForm.alternate_contact_number">                                        
                                    </div>
                                </div>
                            </div>

                            <legend class="m-t">Account Information</legend>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>Email</label>
                                        <input class="form-control" id="email" name="email" type="email" placeholder="Enter email" v-model="accountForm.email" autocomplete="off">

                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            {{ $errors->first('email') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label>Password</label>
                                        {!! Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Enter password']) !!}

                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                            {{ $errors->first('password') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        {!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => 'Retype password']) !!}
                                    </div>
                                </div>
                            </div>
                            <legend>Payroll Settings</legend>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group"><label>Date of Employment</label> <the-mask  class="input-sm form-control"  placeholder="YYYY-MM-DD"  mask="####-##-##" v-model="contribution[0]._employment_date" @blur.native="updateContribution($event,'employment_date')"></div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group"><label>Basic Pay</label> <input type="text" class="input-sm form-control"  placeholder="0" v-model="contribution[0]._basic_pay" @blur="updateContribution($event,'basic_pay')"></div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group"><label>Bonus</label> <input type="text" class="input-sm form-control"  placeholder="0" v-model="contribution[0]._bonus" @blur="updateContribution($event,'bonus')"></div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group"><label>Allowance</label> <input type="text" class="input-sm form-control"  placeholder="0" v-model="contribution[0]._allowance" @blur="updateContribution($event,'allowance')"></div>
                                </div>
                            </div>
                             <div class="row">
                                
                                <div class="col-lg-4">
                                        <div class="form-group"><label>SSS EE</label> <input type="text" placeholder="0" class="form-control" v-model="contribution[0]._sss" @blur="updateContribution($event,'sss')"></div>
                                        <div class="form-group"><label>SSS ER</label> <input type="text" placeholder="0" class="form-control" v-model="contribution[0]._sss_es" @blur="updateContribution($event,'sss_es')"></div>   
                                </div>
                                <div class="col-lg-4">
                                        <div class="form-group"><label>Philhealth EE</label> <input type="text" placeholder="0" class="form-control" v-model="contribution[0]._phil_health" @blur="updateContribution($event,'philhealth')"></div>
                                        <div class="form-group"><label>Philhealth ER</label> <input type="text" placeholder="0" class="form-control" v-model="contribution[0]._phil_health_es" @blur="updateContribution($event,'philhealth_es')"></div>
                                </div>
                                <div class="col-lg-4">
                                        <div class="form-group"><label>Pag-ibig EE</label> <input type="text" placeholder="0" class="form-control" v-model="contribution[0]._pag_ibig" @blur="updateContribution($event,'pagibig')"></div>
                                        <div class="form-group"><label>Pag-ibig ER</label> <input type="text" placeholder="0" class="form-control" v-model="contribution[0]._pag_ibig_es" @blur="updateContribution($event,'pagibig_es')"></div>
                                </div>
                            <!---
                                <div class="col-lg-4">
                                        <div class="form-group"><label>How many working days?</label> <input type="text" placeholder="0" class="form-control" v-model="contribution[0]._working_days" @blur="updateContribution($event,'working_days')"></div>
                                </div>
                                <div class="col-lg-4">
                                        <div class="form-group"><label>How many working hours?</label> <input type="text" placeholder="0" class="form-control" v-model="contribution[0]._working_hours" @blur="updateContribution($event,'working_hours')"></div>
                                   
                                </div>
                            -->
                             </div>
                            <template v-if="(roles.filter(role => role.label == 'Employee')).length == 1">
                                <legend class="m-t">Schedule Information</legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group 
                                            {{ ($errors->has('scheduleType') || $errors->has('timeIn') || $errors->has('timeOut') || $errors->has('timeInFrom') || $errors->has('timeInTo')) ? 'has-error' : '' }}
                                        ">
                                            <label>Schedule Type</label>
                                            <select class="form-control" name="scheduleType" v-model="accountForm.scheduleType">
                                                <option v-for="scheduleType in scheduleTypes" :value="scheduleType.id">
                                                    @{{ scheduleType.name }}
                                                </option>
                                            </select>
                                            @if ($errors->has('scheduleType'))
                                                <span class="help-block">
                                                    {{ $errors->first('scheduleType') }}
                                                </span>
                                            @endif
                                            @if ($errors->has('timeIn'))
                                                <span class="help-block">
                                                    {{ $errors->first('timeIn') }}
                                                </span>
                                            @endif
                                            @if ($errors->has('timeOut'))
                                                <span class="help-block">
                                                    {{ $errors->first('timeOut') }}
                                                </span>
                                            @endif
                                            @if ($errors->has('timeInFrom'))
                                                <span class="help-block">
                                                    {{ $errors->first('timeInFrom') }}
                                                </span>
                                            @endif
                                            @if ($errors->has('timeInTo'))
                                                <span class="help-block">
                                                    {{ $errors->first('timeInTo') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Working Hours</label>
                                        <div v-show="accountForm.scheduleType == 1 ||  accountForm.scheduleType == 2" class="form-group">
                                            <div v-show="accountForm.scheduleType == 1">
                                                <div class="input-group">
                                                    <div class="bootstrap-timepicker timepicker">
                                                        <input id="timepicker1" name="timeIn" type="text" class="form-control input-small timepicker-input" v-model="accountForm.timeIn" autocomplete="off">
                                                    </div>
                                                    <span class="input-group-addon">to</span>
                                                    <div class="bootstrap-timepicker timepicker">
                                                        <input id="timepicker2" name="timeOut" type="text" class="form-control input-small timepicker-input" v-model="accountForm.timeOut" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>

                                            <div v-show="accountForm.scheduleType == 2">
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="width:90px;text-align:right;">Start</span>
                                                    <div class="bootstrap-timepicker timepicker">
                                                        <input  style="width:280px;" id="timepicker3" name="timeInFrom" type="text" class="form-control input-small timepicker-input" v-model="accountForm.timeInFrom" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div style="height:10px; clear:both;"></div>
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="width:90px;text-align:right;">End</span>
                                                    <div class="bootstrap-timepicker timepicker">
                                                        <input style="width:280px;"  id="timepicker4" name="timeInTo" type="text" class="form-control input-small timepicker-input" v-model="accountForm.timeInTo" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <label>Working Days</label>
                                        <div class="input-group">
                                            <span class="input-group-addon" style="width:90px;text-align:right;" >Start</span>
                                            <div class="">
                                                <select class="form-control" style="width:280px" v-model="contribution[0]._day_from" @change="updateContribution($event,'day_from')">
                                                    <option value="MONDAY">Monday</option>
                                                    <option value="TUESDAY">Tuesday</option>
                                                    <option value="WEDNESDAY">Wednesday</option>
                                                    <option value="THURSDAY">Thursday</option>
                                                    <option value="FRIDAY">Friday</option>
                                                    <option value="SATURDAY">Saturday</option>
                                                    <option value="SUNDAY">Sunday</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div style="height:10px; clear:both;"></div>
                                        <div class="input-group">
                                            <span class="input-group-addon" style="width:90px;text-align:right;" >End</span>
                                            <div class="">
                                                <select class="form-control" style="width:280px" v-model="contribution[0]._day_to" @change="updateContribution($event,'day_to')">
                                                    <option value="MONDAY" selected>Monday</option>
                                                    <option value="TUESDAY">Tuesday</option>
                                                    <option value="WEDNESDAY">Wednesday</option>
                                                    <option value="THURSDAY">Thursday</option>
                                                    <option value="FRIDAY">Friday</option>
                                                    <option value="SATURDAY">Saturday</option>
                                                    <option value="SUNDAY">Sunday</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </template>
                            <template v-else>
                                <input type="hidden" name="scheduleType" value="0">
                            </template>

                            <legend class="m-t">Roles & Permissions</legend>

                            <h4>Roles</h4>

                            <select id="roles" name="roles[]" class="form-control dual_select" multiple>
                                @foreach ($roles as $role)
                                <option {{ $account[0]->roles->contains('id', $role->id) ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->label }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('roles'))
                            <div class="text-danger m-t-xs">
                                {{ $errors->first('roles') }}
                            </div>
                            @endif

                            <hr>

                            <h4>Permissions</h4>

                            <select id="permissions" name="permissions[]" class="form-control" multiple> 
                                <option :value="data.id" v-for="data in permissions" selected>@{{ data.type }} - @{{ data.label }}</option>
                            </select>

                            <button type="button" class="btn btn-sm btn-primary m-t hide" @click="showPermissions">
                                    <strong>Edit Permissions</strong>
                            </button>

                            @if ($errors->has('permissions'))
                            <div class="text-danger m-t-xs">
                                {{ $errors->first('permissions') }}
                            </div>
                            @endif

                            <button type="submit" class="btn btn-sm btn-primary pull-right m-t">
                                <strong>Save</strong>
                            </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('styles')
    {!! Html::style('components/bootstrap-timepicker/bootstrap-timepicker.min.css') !!}
@endpush

@push('scripts')
   

    {!! Html::script('cpanel/plugins/dualListbox/jquery.bootstrap-duallistbox.js') !!}
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}  
    {!! Html::script('components/bootstrap-timepicker/bootstrap-timepicker.min.js') !!}
    {!! Html::script(mix('js/visa/accounts/edit.js', 'cpanel')) !!}

<script>
    $(document).ready(function () {
        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160,
        });
    });
</script>     

@endpush