<legend class="m-t">Contact Information</legend>

<div class="row">
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
            <label>Address</label>
            {!! Form::text('address', null, ['id' => 'address', 'class' => 'form-control', 'placeholder' => 'Enter address']) !!}

            @if ($errors->has('address'))
            <span class="help-block">
                {{ $errors->first('address') }}
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
            <label>Contact Number 1</label>
            {!! Form::text('contact_number', null, ['id' => 'contact_number', 'class' => 'form-control', 'placeholder' => 'Enter contact number']) !!}

            @if ($errors->has('contact_number'))
            <span class="help-block">
                {{ $errors->first('contact_number') }}
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Contact Number 2</label>
            {!! Form::text('alternate_contact_number', null, ['id' => 'alternate_contact_number', 'class' => 'form-control', 'placeholder' => 'Enter contact number']) !!}
        </div>
    </div>
</div>

