<dialog-modal id='dialogPermissions' size="modal-lg">
	<template slot="modal-title">
        Add Permissions
    </template>
    <template slot="modal-body">
        <div>
            <form method="POST" id="form">
                <template v-for="data in permissiontype">
                    <div class="ibox collapsed panel panel-info" style="border-color: #23c6c8 !important;">
                        <div class="ibox-title panel-heading">
                            <h5>@{{ data.type }}</h5>
                            <div class="ibox-tools">
                                <a @click="toggle(data.id, $event)" data-toggle="collapse" :href="'#content'+data.id" aria-expanded="false" aria-controls="content" class="toggle-row">
                                    <i :id="'fa-'+data.id" class="fa fa-arrow-up"></i>
                                </a>
                            </div>
                        </div>
                        <div :id="'content'+data.id" class="collapse ibox-content-permission">
                            <p>
                                <span v-for="(data2,index) in getpermissions">
                                    <span v-if="data2.type==data.type">
                                    <input type="checkbox" :value="data2.id" @click="selectItem(data2)">@{{data2.name}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                                    </span>    
                                </span>
                            </p>
                        </div>
                    </div>
                </template>
            </form>
        </div>

    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Close</button>
    </template>
</dialog-modal>