<legend class="m-t">Roles & Permissions</legend>

<h4>Roles</h4>

<select id="roles" name="roles[]" class="form-control dual_select" multiple>
    @foreach ($roles as $role)
    <option value="{{ $role->id }}" {{ (in_array($role->id, (old('roles')) ? old('roles') : [] )) ? 'selected' : '' }}>{{ $role->label }}</option>
    @endforeach
</select>

@if ($errors->has('roles'))
<div class="text-danger m-t-xs">
    {{ $errors->first('roles') }}
</div>
@endif

<hr>

<h4>Permissions</h4>

<!-- <input id="permissions" name="permissions[]" class="form-control"  type="file" v-model="permissions" multiple>
 -->
<select id="permissions" name="permissions[]" class="form-control" multiple v-cloak> 
    <option :value="data.id" v-for="data in permissions" selected>@{{ data.type }} - @{{ data.label }}</option>
</select>

<button type="button" class="btn btn-sm btn-primary m-t hide" @click="showPermissions">
        <strong>Add Permissions</strong>
</button>
@if ($errors->has('permissions'))
<div class="text-danger m-t-xs">
    {{ $errors->first('permissions') }}
</div>
@endif