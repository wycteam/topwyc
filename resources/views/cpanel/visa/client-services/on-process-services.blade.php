@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    {!! Html::style(mix('css/visa/dashboard/index.css', 'cpanel')) !!}
@endpush

@section('content')
    <div id="client-services" class="wrapper wrapper-content animated">
        <div class="row ibox center-box">
            <div class="panel-body">

              @include('cpanel.visa.dashboard.modals')
              <div class="ibox-content">
                <div class="table-responsive">
                    <table id="onprocessLists" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>@lang('cpanel/dashboard.date')</th>
                                <th>@lang('cpanel/dashboard.name')</th>
                                <th>@lang('cpanel/dashboard.id')</th>
                                <th>@lang('cpanel/dashboard.pckg')</th>
                                <th style="width:30% !important">@lang('cpanel/dashboard.detail')</th>
                                <th>@lang('cpanel/dashboard.cost')</th>
                                <th>@lang('cpanel/dashboard.charge')</th>
                				<!-- <th>Tip</th> -->
                				<th style="width:5%">@lang('cpanel/dashboard.action')</th>
                            </tr>
                        </thead>
                            <tbody>
                				<tr v-for="service in onprocessServices" v-cloak>
                					<td>@{{ service.service_date | serviceDate }}</td>
                					<td><span>@{{ service.first_name }} @{{ service.last_name }}</span></td>
                					<td><span>@{{ service.client_id }}</span></td>
                					<td>@{{ service.tracking }}</td>
                					<td>
                						<a href="#" v-if="service.remarks!=''" data-toggle="popover" data-placement="right" data-container="body" :data-content="service.remarks" data-trigger="hover" >
                			              @{{ service.detail }}
                			            </a>
                			            <span v-else>@{{ service.detail }}</span>
                			        </td>
                					<td>@{{ totalCost(service.cost,service.tip) }}</td>
                					<td>@{{ service.charge }}</td>
                					<!-- <td>@{{ service.tip }}</td> -->
                					<td class="text-center">
                						<a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Mark as pending" @click="openPendingService(service)" v-cloak><i class="fa fa-times" style="color: red;"></i></a>
                						<a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Mark as on process and remove from the list" @click="openProcessService(service)" v-cloak><i class="fa fa-check-square-o"></i></a>
                						<a :href="'/visa/client/'+service.client_id" target="_blank" data-toggle="tooltip" data-placement="left" title="View client profile"><i class="fa fa-arrow-right"></i></a>
                					</td>
                				</tr>
                            </tbody>
                    </table>
                </div>
               </div>  
              </div>
          </div> <!-- row -->
      </div>
  @endsection

  @push('scripts')
      {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
      {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
      {!! Html::script(mix('js/visa/client-services/onprocess.js', 'cpanel')) !!}
  @endpush
