@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    {!! Html::style(mix('css/visa/dashboard/index.css', 'cpanel')) !!}
@endpush

@section('content')
    <div id="dashboard" class="wrapper wrapper-content animated">
        <div class="row ibox center-box">

            <div v-show="setting == 1" v-cloak>
                <div class="col-sm-2 rem-padding bordered ">
                    <div class="small-box bg-g">
                        <div class="inner">
                            <h3 class="bgc-1">{{ $total_client }}</h3>
                        </div>
                        <a href="javascript:void(0)" class="small-box-footer bgc-1-1">
                            @lang('cpanel/dashboard.total-client')
                        </a>
                    </div>
                </div>
                <div class="col-sm-2 rem-padding bordered">
                    <div class="small-box bg-g">
                        <div class="inner">
                            <h3 class="bgc-2">{{ $total_service}}</h3>
                        </div>
                        <a href="javascript:void(0)" class="small-box-footer bgc-2-2">
                            @lang('cpanel/dashboard.total-serv')
                        </a>
                    </div>
                </div>
                <div class="col-sm-2 rem-padding bordered">
                    <div class="small-box bg-g">
                        <div class="inner">
                            <h3 class="bgc-3">{{ $total_service_new }}</h3>
                            <h5>{{ $total_today_services_amount }}</h5>
                            <h6>@lang('cpanel/dashboard.total-today-serv-cost')</h6>
                        </div>

                       <a  href="{{ action('Cpanel\ClientServicesController@getTodayServices', ['mode' => 'today']) }}" class="small-box-footer bgc-3-3">
                            @lang('cpanel/dashboard.today-serv')
                        </a>
                    </div>
                </div>

                <div class="col-sm-2 rem-padding bordered">
                    <div class="small-box bg-g">
                        <div class="inner">
                            <h3 class="bgc-4">{{ $total_service_yesterday }}</h3>
                            <h5 >{{ $total_yesterday_services_amount }}</h5>
                            <h6>@lang('cpanel/dashboard.total-yesterday-serv-cost')</h6>
                        </div>
                        <a  href="{{ action('Cpanel\ClientServicesController@getTodayServices', ['mode' => 'yesterday'])}}" class="small-box-footer bgc-4-4">
                            @lang('cpanel/dashboard.yesterday-serv')
                        </a>
                    </div>
                </div>


            </div>
        </div>

        <div class="row" v-cloak>
            <div class="ibox-content">

                  <div>
                       <service-list columns="{{ $oldest_pending_services }}" title = "@lang('cpanel/dashboard.pending-serv')"
                          nodata = "@lang('cpanel/dashboard.no-data-avail-tbl')"
                          viewmore = "{{ action('Cpanel\ClientServicesController@getPendingServices') }}">

                      </service-list>
                  </div>

                  <div>
                      <service-list columns="{{ $oldest_on_process_services }}" title = "@lang('cpanel/dashboard.on-process-serv')"
                         nodata = "@lang('cpanel/dashboard.no-data-avail-tbl')"
                         viewmore = "{{ action('Cpanel\ClientServicesController@getOnProcessServices') }}">
                      </service-list>
                  </div>

                  <div>
                        <tasks-list columns="{{ $today_tasks }}" title = "@lang('cpanel/dashboard.today-task')" nodata = "@lang('cpanel/dashboard.no-data-avail-tbl')"></tasks-list>
                  </div>
                  <div align="right" class="amount-footer">
                      <div>
                          <h5>@lang('cpanel/dashboard.total-estimated-cost-for-today'): <b>{{$total_estimated_cost_for_today}}</b></h5>
                      </div>
                  </div>

                  <div>
                      <tasks-list columns="{{ $reminders }}" title = "@lang('cpanel/dashboard.reminders-for-tom')" nodata = "@lang('cpanel/dashboard.no-data-avail-tbl')"></tasks-list>
                  </div>

                  <div align="right" class="amount-footer">
                      <div>
                          <h5>@lang('cpanel/dashboard.total-estimated-cost-for-tomorrow'): <b>{{$total_estimated_cost_for_yesterday}}</b></h5>
                      </div>
                  </div>


            </div>
        </div> <!-- row -->
    </div>
@endsection

@push('scripts')
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    {!! Html::script(mix('js/visa/dashboard/index.js', 'cpanel')) !!}
@endpush
