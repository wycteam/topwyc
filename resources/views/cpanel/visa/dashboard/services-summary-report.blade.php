<table class="lists table table-striped table-bordered">
        <thead>
            <tr>
                <th colspan="1">@lang('cpanel/dashboard.pending-serv')</th>
            </tr>
        </thead>
  				<tr v-for="(item, index) in summaryReports.oldest_pending_services" v-cloak>
    					<td>
    			       <span><a :href="'/visa/client/' + item.client_id" target="_blank" >@{{(index + 1) + ".)"}} @{{  item.service_date | serviceDate }} : @{{ item.detail }}<span class="label" v-html="item.remarks">@{{ item.remarks }}</span></a></span>

    					</td>
  				</tr>
        </tbody>
</table>
