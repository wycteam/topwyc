@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    {!! Html::style(mix('css/visa/dashboard/index.css', 'cpanel')) !!}
@endpush

@section('content')
    <div id="dashboard" class="wrapper wrapper-content animated">
        <div class="row ibox center-box">

        <div class="panel-body">
           <todays-tasks ref="todaystasksref"></todays-tasks>
        </div>


        </div> <!-- row -->
    </div>
@endsection

@push('scripts')
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    {!! Html::script(mix('js/visa/dashboard/index.js', 'cpanel')) !!}
@endpush
