<dialog-modal id='dialogReason' size="modal-md">

    <template slot="modal-title">
        @lang('cpanel/dashboard.please-put-a-reason')
    </template>
    <template slot="modal-body" class="text-center">
    <form role="form" method="POST">
      <div class="form-group" :class="{ 'has-error': form.errors.has('reason') }">
        <div class="row">
        	<div class="col-lg-12" style="overflow:auto">
                <div class="col-md-2">@lang('cpanel/dashboard.reason'):</div>
                <div class="col-md-10">
                    <input v-model="form.reason" class="form-control" id="reason" type="text">
                    <span class="help-block" v-if="form.errors.has('reason')" v-text="form.errors.get('reason')"></span>
                </div>
			</div>
		</div>
      </div>
    </form>
    </template>
    <template slot="modal-footer">
        <button type="button" class='btn btn-default' data-dismiss="modal">@lang('cpanel/dashboard.cancel')</button>
        <button type="submit" class='btn btn-primary' @click="markAsPending">@lang('cpanel/dashboard.save')</button>
    </template>


</dialog-modal>

<dialog-modal id='dialogRemove'>
    <template slot="modal-title">
        @lang('cpanel/dashboard.remove-this-service-from-the-list')
    </template>
    <template slot="modal-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>@lang('cpanel/dashboard.date')</th>
                        <th>@lang('cpanel/dashboard.name')</th>
                        <th>@lang('cpanel/dashboard.id')</th>
                        <th>@lang('cpanel/dashboard.pckg')</th>
                        <th>@lang('cpanel/dashboard.detail')</th>
                        <th>@lang('cpanel/dashboard.cost')</th>
                        <th>@lang('cpanel/dashboard.charge')</th>
                        <th>@lang('cpanel/dashboard.tip')</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-cloak>
                        <td>@{{ service.service_date | serviceDate }}</td>
                        <td><span v-if="service.user!=null">@{{ service.user.full_name }}</span></td>
                        <td><span v-if="service.user!=null">@{{ service.user.id }}</span></td>
                        <td>@{{ service.tracking }}</td>
                        <td>@{{ service.detail }}</td>
                        <td>@{{ service.cost }}</td>
                        <td>@{{ service.charge }}</td>
                        <td>@{{ service.tip }}</td>  
                    </tr>
                </tbody>
            </table>
        </div>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">@lang('cpanel/dashboard.cancel')</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click="removeService">@lang('cpanel/dashboard.proceed')</button>
    </template>
</dialog-modal>

<modal id='editServiceModal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/dashboard.edit-service')</h3>
    </template>

    <template slot="modal-body">

        <edit-service-dashboard ref="editservicedashboardref"></edit-service-dashboard> 

    </template>

</modal>

<modal id='addScheduleModal' size="modal-sm">
    <template slot="modal-title">
        <h3>@lang('cpanel/dashboard.add-new-delivery-schedule')</h3>
    </template>

    <template slot="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <form method="post" id="frm_deliverysched">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- item name -->
                            <div class="form-group" :class="{ 'has-error': addSchedule.errors.has('item') }">
                                <label class="control-label">@lang('cpanel/dashboard.description'):</label>
                                <input class="form-control m-b" id="item" type="text" v-model="addSchedule.item" name="item">
                                <p class="help-block" v-if="addSchedule.errors.has('item')" v-text="addSchedule.errors.get('item')"></p>
                            </div><!-- end label-floating -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <!-- location -->
                            <div class="form-group" :class="{ 'has-error': addSchedule.errors.has('location') }">
                                <label class="control-label">@lang('cpanel/dashboard.loc'):</label>
                                <input class="form-control m-b" id="detail" type="text" v-model="addSchedule.location" name="location">
                                <p class="help-block" v-if="addSchedule.errors.has('location')" v-text="addSchedule.errors.get('location')"></p>
                            </div><!-- end label-floating -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <!-- date -->
          <!--                    <div class="form-group" :class="{ 'has-error': addSchedule.errors.has('scheduled_date') }">
                                    <label class="control-label">Date</label>
                                    <input class="form-control m-b" id="scheduled_date" type="date" name="date">
                                    <p class="help-block" v-if="addSchedule.errors.has('scheduled_date2')" v-text="addSchedule.errors.get('scheduled_date')"></p>
                            </div> --><!-- end label-floating -->
                        </div>
                    </div>
<!--                     <div class="row">
                         <div class="col-lg-12">
                                <div class="form-group" :class="{ 'has-error': addSchedule.errors.has('scheduled_date') }">
                                    <label class="control-label">Date</label>
                                    <div class="input-daterange input-group" id="datepicker_ds">
                                        <input type="text" class="input-sm input form-control" id="date" name="date" autocomplete="off" @focus="getDate()"/>
                                    </div>
                                     <p class="help-block" v-if="addSchedule.errors.has('scheduled_date')" v-text="addSchedule.errors.get('scheduled_date')"></p>
                                </div>
                         </div>
                    </div> -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group" :class="{ 'has-error': addSchedule.errors.has('scheduled_date') }">
                                <label class="control-label">@lang('cpanel/dashboard.date'):</label>
                                <div class="input-group registration-date-time">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                    <input class="form-control" name="registration_date" id="scheduled_date" type="date" v-model="addSchedule.scheduled_date">
                                    <p class="help-block" v-if="addSchedule.errors.has('scheduled_date')" v-text="addSchedule.errors.get('scheduled_date')"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-lg-12">
                            <div class="form-group" :class="{ 'has-error': addSchedule.errors.has('scheduled_time') }">
                                    <label class="control-label">@lang('cpanel/dashboard.time'):</label>
                                    <div class="input-group registration-date-time">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></span>
                                    <input class="form-control" type="number" id="scheduled_hour" min="0" max="23" style="width: 55px;padding: 0px;padding-left: 16px;" v-model="scheduled_hour">
                                    <input class="form-control" type="number" id="scheduled_minute" min="0" max="59" style="width: 55px;padding: 0px;padding-left: 16px;"  v-model="scheduled_minute">   
                                    <p class="help-block" v-if="addSchedule.errors.has('scheduled_time')" v-text="addSchedule.errors.get('scheduled_time')"></p>
                                </div>
                            </div><!-- end label-floating -->
                        </div>
                    </div>

<!--                     <div class="row">
                         <div class="col-lg-12">
                            <div class="form-group" :class="{ 'has-error': addSchedule.errors.has('scheduled_time') }">
                                    <label class="control-label">Time</label>
                                    <div class="input-group registration-date-time">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></span>
                                    <input class="form-control m-b" id="scheduled_time" type="time" v-model="addSchedule.scheduled_time" name="scheduled_time">   
                                    <p class="help-block" v-if="addSchedule.errors.has('scheduled_time')" v-text="addSchedule.errors.get('scheduled_time')"></p>
                                </div>
                            </div>
                        </div>
                    </div>  -->

                    <div class="row">
                        <div class="col-lg-12">
                            <!-- rider -->
                            <div class="form-group" :class="{ 'has-error': addSchedule.errors.has('rider_id') }">
                                    <label class="control-label">@lang('cpanel/dashboard.rider'):</label>
                                    <select class="form-control m-b" v-model="addSchedule.rider_id" id="rider_id" name="rider_id">
                                        <option v-for="option in companyCouriers" v-bind:value="option.user_id">
                                            @{{ option.user.full_name }}
                                        </option>
                                    </select>
                                    <p class="help-block" v-if="addSchedule.errors.has('rider')" v-text="addSchedule.errors.get('rider')"></p>
                            </div><!-- end label-floating -->
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </template>

    <template slot="modal-footer">
        <button class='btn btn-default' data-dismiss="modal">@lang('cpanel/dashboard.cancel')</button>
        <button class='btn btn-primary' @click="saveDeliverSchedule()">@lang('cpanel/dashboard.save')</button>
    </template>

</modal>

<!-- deleteLogPrompt Modal -->
<modal size="modal-sm" id='deleteSchedPrompt'>
    <template slot="modal-title">
        <h3>@lang('cpanel/dashboard.delete-log')</h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body">
        <p>@lang('cpanel/dashboard.are-you-sure-you-want-to-delete-this-schedule')</p>

    </template><!-- end of modal-body -->
    <template slot="modal-footer">
        <button type="button" class="btn btn-w-m btn-primary" @click="deleteSchedule(selId)">@lang('cpanel/dashboard.yes')</button>
        <button type="button" data-dismiss="modal" class="btn btn-w-m btn-primary">@lang('cpanel/dashboard.cancel')</button>
    </template>
</modal><!-- end of deleteLogPrompt Modal -->

<modal id='editWhoModal' size="modal-md">

    <template slot="modal-title">
        <h3>@lang('cpanel/dashboard.edit')</h3>
    </template>

    <template slot="modal-body">

        <edit-who ref="editwhoref"></edit-who> 

    </template>

</modal>