

@extends('cpanel.layouts.master-plain')
<title>Group Summary</title>
@push('styles')
    <!-- {!! Html::style(mix('css/visa/financing/financing.css', 'cpanel')) !!} -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endpush
@section('content')

    <div class="container" id="groupSummary">
        <div class="row wrapper border-bottom white-bg page-heading">
<!--             <div class="col-md-12">
                <h2>Group Summary</h2>
            </div> -->
        </div>
        <div class="row">

            @foreach ($data['members'] as $value)
            <tr>
                <td>{{ $value['title'] }}</td>
            </tr>
            @endforeach
        </div>
    </div>

@endsection
@push('scripts')
    {!! Html::script(mix('js/visa/groups/summary.js', 'cpanel')) !!}
@endpush