@extends('cpanel.layouts.master')

@section('title', $title)

@section('content')
    <div id="app"  v-cloak>
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <div class="col-md-3">
                <h2>{{ $title }}</h2><br>
                <!-- <b v-if="details">Branch : <small style="color:black;">@{{ details.group.branch }}</small></b> -->
            </div>
            <div class="col-md-9">
                
                <div class="col-md-3 col-md-offset-2">
                    <div v-show="client_commission.client_id==0" class="clients_select" style="margin-top: 16px; margin-right: 10px;" v-if="userauth == 1193 || userauth == 2352 || userauth == 1" v-cloak>
                        <label>@lang('cpanel/groups-page.client-commi'):</label>
                        <!--<select id="client_com" class="form-control m-b" v-model="client_com" @change="changeCommission('client')" >
                            <option value="0" disabled>No Client Selected</option>
                            <option v-for="c in clients" :value="c.client_id">@{{c.client.full_name}}</option>
                        </select>-->
                        <select data-placeholder="Select clients commission" class="chosen-select-for-clients2" style="width:100%;" multiple>
                        <option v-for="client in clients" :value="client.id">
                                @{{ client.full_name }}
                        </option>
                    </select>
                    </div>
                    <div v-show="client_commission.client_id>0" style="margin-top: 16px;" v-if="userauth == 1193 || userauth == 2352 || userauth == 1" v-cloak>
                        <div class="row">
                            
                            <label>@lang('cpanel/groups-page.client-commi'):</label>
                            
                        </div>
                        <div class="row">
                            <!--<div class="com-select">@{{client_commission.full_name}}<a href="#" class="close2" @click="removeCommission('client')"></a></div>-->
                            <span class="multiselect__tag mr-top" v-show="client_commission.full_name!=''"><span >@{{client_commission.full_name}}</span> <a aria-hidden="true" tabindex="1" class="multiselect__tag-icon" @click="removeCommission('client')"></a></span>
                        </div>
                    </div>
                </div>
               <div class="col-md-3">
                    <div v-show="agent_commission.client_id==0" class="agents_select" style="margin-top: 16px; margin-right: 10px;" v-if="userauth == 1193 || userauth == 2352 || userauth == 1" v-cloak>
                            <label>@lang('cpanel/groups-page.agent-commi'):</label>
                            <select data-placeholder="Select agent commission" class="chosen-select-for-clients3" style="width:100%;" multiple>
                                <option v-for="client in agents" :value="client.id">
                                    @{{ client.full_name }}
                                </option>
                            </select>
                        <!--<select id="agent_com" class="form-control m-b"  v-model="agent_com" @change="changeCommission('agent')">
                                <option value="0" disabled>No Agent Selected</option>
                                <option v-for="client in agents" :value="client.id">@{{client.full_name}}</option>
                            </select>-->
                        </div>
                        <div v-show="agent_commission.client_id>0" style="margin-top: 16px; " v-if="userauth == 1193 || userauth == 2352 || userauth == 1" v-cloak>
                            <div class="row">
                                <label>@lang('cpanel/groups-page.agent-commi'):</label>
                            </div>
                            <div class="row">
                                <!--<p class="com-select">@{{agent_commission.full_name}}<a href="#" class="close2" @click="removeCommission('agent')"></a></p>-->
                                <span class="multiselect__tag mr-top" v-show="agent_commission.full_name!=''"><span>@{{agent_commission.full_name}}</span> <a aria-hidden="true" tabindex="1" class="multiselect__tag-icon" @click="removeCommission('agent')"></a></span>
                            </div>
                            
                            
                        </div>
               </div>
               <div class="col-md-2">
                    <div style="margin-top: 16px; margin-right: 10px;" v-if="userauth == 1193 || userauth == 2352 || userauth == 1" v-cloak>
                        <label>@lang('cpanel/groups-page.branch') :</label>
                        <select class="form-control m-b" id="branchSelect" name="branchSelect" v-model="branchId" @change="switchBranch">
                            <option v-for="s in batches" :value="s.id">@{{s.name}}</option>
                        </select>
                    </div>
                </div>
               <div class="col-md-2">
                    <div style=" margin-top: 16px;" v-if="userauth == 1193 || userauth == 2352 || userauth == 1" v-cloak>
                        <label>@lang('cpanel/groups-page.service-charge') :</label>
                        <select class="form-control m-b" id="profileSelect" name="profileSelect" v-model="pselected" @change="switchCost">
                            <option value="0">@lang('cpanel/groups-page.regular')</option>
                            <option v-for="s in service_profiles" :value="s.id">@{{s.name}}</option>
                        </select>
                    </div>
             </div>
            </div>
        </div>
    </div>

    <div  class="wrapper wrapper-content">

    	<div class="row ibox" v-if="details!=null">

    		@include('cpanel.includes.visa.group.modals')

    		<div class="ibox-content">
	    		<div class="col-lg-5">
	    			<div class="row">
		    			<div class="col-lg-5 text-center font-bold text-info">
                            <h2 v-cloak v-if="payment_status=='High-Risk'"><i><b style="color: red">@{{ details.group.name }}</b></i></h2>
		    				<h2 v-cloak v-else><i><b>@{{ details.group.name }}</b></i></h2>
                            <!-- <h3>
                                <p>
                                    <span v-cloak>
                                        <a href="javascript:void(0)" @click="viewReport(details.group.id)" data-toggle="popover" data-placement="top" data-container="body" data-content="View Report" data-trigger="hover">
                                            @{{ details.group.id }}
                                        </a>
                                    </span>
                                </p>
                            </h3> -->
                            <h4><i style="color:black;">(@{{ details.group.user.full_name }})</i></h4>
                            <p style="color:black;">@{{ details.group.tracking }}</p>
                            <a data-toggle="modal" data-target="#edit-address-modal" @click="$refs.editaddressref.setGroupAddress()" v-show="selpermissions.editAddress==1 || setting == 1">@lang('cpanel/groups-page.edit') <i class="fa fa-edit cursor" ></i></a>
		    			</div>
		    			<div class="col-lg-7">
                            <h4 style="padding-top:8px;">@lang('cpanel/groups-page.branch'): 
                                <span style="font-weight: 400;font-size: 13px;">@{{ details.group.branch }}</span>
                            </h4>
                            <h4>@lang('cpanel/groups-page.address'): 
                                <span style="font-weight: 400;font-size: 13px;">@{{ details.group.address }}</span>
                            </h4>
                            <h4>@lang('cpanel/groups-page.contact'): 
                                <span style="font-weight: 400;font-size: 13px;">@{{ details.group.user.contact_number }} </span>
                                <span v-if="details.group.user.alternate_contact_number != null">@{{ "/"+details.group.user.alternate_contact_number }}</span>
                            </h4>
		    			</div>
	    			</div>
	    		</div>
	    		<div class="col-lg-7">
	    			<div class="row">
		    			<div class="col-lg-6">
		    				<table class="table table-hover no-margins">
                                <tr>
                                    <td><b>@lang('cpanel/groups-page.total-complete-cost'):</b></td>
                                    <td>@{{ (details.cost != null) ? parseFloat(details.completeCost).toFixed(2) : '0.00' }}</td>
                                </tr>
		    					<tr>
		    						<td><b>@lang('cpanel/groups-page.total-cost'):</b></td>
		    						<td>@{{ (details.cost != null) ? parseFloat(details.cost).toFixed(2) : '0.00' }}</td>
		    					</tr>
                                <tr>
                                    <td><b>@lang('cpanel/groups-page.total-payment'):</b></td>
                                    <td>@{{ (parseFloat((details.deposit != null) ? parseFloat(details.deposit).toFixed(2) : '0.00') + parseFloat((details.payment != null) ? parseFloat(details.payment).toFixed(2) : '0.00')).toFixed(2) }}</td>
                                </tr>
                                <tr style="display: none">
                                    <td><b>@lang('cpanel/groups-page.total-client-commission'):</b></td>
                                    <td>@{{ (details.clientcom != null) ? parseFloat(details.clientcom).toFixed(2) : '0.00' }}</td>
                                </tr>
                                <tr style="display: none">
                                    <td><b>@lang('cpanel/groups-page.total-agent-commission'):</b></td>
                                    <td >@{{ (details.agentcom != null) ? parseFloat(details.agentcom).toFixed(2) : '0.00' }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-hover no-margins">
                                <tr>
                                    <td><b>@lang('cpanel/groups-page.total-discount'):</b></td>
                                    <td>@{{ (details.discount != null) ? parseFloat(details.discount).toFixed(2) : '0.00' }}</td>
                                </tr>
		    					<tr>
		    						<td><b>@lang('cpanel/groups-page.total-refund'):</b></td>
		    						<td>@{{ (details.refund != null) ? parseFloat(details.refund).toFixed(2) : '0.00' }}</td>
		    					</tr>
                                <tr>
                                    <td><b>@lang('cpanel/groups-page.total-balance'):</b></td>
                                    <td>@{{ (details.balance != null) ? parseFloat(details.balance).toFixed(2) : '0.00' }}</td>
                                </tr>
                                <tr>
                                    <td><b>@lang('cpanel/groups-page.total-collectables'):</b></td>
                                    <td>@{{ (details.collectables < 0 ) ? parseFloat(details.collectables).toFixed(2) : '0.00' }}</td>
                                </tr>
                                
		    				</table>
		    			</div>
		    		</div>
	    		</div>

	    		<div class="spacer-1"></div>
    		</div>

    	</div> <!-- row -->

    	<div class="row">
    		<div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1b" @click="fetchGBatch"> @lang('cpanel/groups-page.batch')</a></li>
                    <li class=""><a data-toggle="tab" href="#tab-1" @click="getGroupMembers"> @lang('cpanel/groups-page.members')</a></li>
                    <li class=""><a data-toggle="tab" href="#tab-1a" @click="fetchGServices"> @lang('cpanel/groups-page.services')</a></li>
                    <li class=""><a data-toggle="tab" href="#tab-8" @click="fetchTransactions(20)">@lang('cpanel/groups-page.funds')</a></li>
                    <li class="" v-show="selpermissions.actionLogs==1 || setting == 1"><a data-toggle="tab" href="#tab-2" @click="fetchActionLogs(20)">@lang('cpanel/groups-page.action-logs')</a></li>
                    <li class="" v-show="selpermissions.transactionLogs==1 || setting == 1"><a data-toggle="tab" href="#tab-3" @click="fetchTransactionLogs(20)">@lang('cpanel/groups-page.transaction-logs')</a></li>
                    <li class="" v-show="selpermissions.transactionLogs==1 || setting == 1"><a data-toggle="tab" href="#tab-4" @click="fetchCommissionLogs(20)">@lang('cpanel/groups-page.commission-logs')</a></li>
<!--                     <li class="" v-show="selpermissions.deposits==1 || setting == 1"><a data-toggle="tab" href="#tab-4" @click="fetchDeposits(20)">@lang('cpanel/groups-page.deposits')</a></li>
 -->                    
                   <!--  <li class="" v-show="selpermissions.payments==1 || setting == 1"><a data-toggle="tab" href="#tab-5" @click="fetchDepositsPayments(20)">@lang('cpanel/groups-page.payments')</a></li>
                    <li class="" v-show="selpermissions.refunds==1 || setting == 1"><a data-toggle="tab" href="#tab-6" @click="fetchRefunds(20)">@lang('cpanel/groups-page.refunds')</a></li>
                    <li class="" v-show="selpermissions.discounts==1 || setting == 1"><a data-toggle="tab" href="#tab-7" @click="fetchDiscounts(20)">@lang('cpanel/groups-page.discounts')</a></li> -->


                    
                </ul>
                <div class="tab-content">
                    <div id="tab-1b" class="tab-pane active">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.batch-tab')
                        </div>
                    </div> <!-- tab-1 -->
                    
                    <div id="tab-1" class="tab-pane ">
                        <div class="panel-body">
                        	@include('cpanel.includes.visa.group.show.member-tab')
                        </div>
                    </div> <!-- tab-1 -->

                    <div id="tab-1a" class="tab-pane">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.service-tab')
                        </div>
                    </div> <!-- tab-1 -->


                    <div id="tab-2" class="tab-pane" v-show="selpermissions.actionLogs==1 || setting == 1">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.action-logs-tab')
                        </div>
                    </div> <!-- tab-2 -->

                    <div id="tab-3" class="tab-pane" v-show="selpermissions.transactionLogs==1 || setting == 1">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.transaction-logs-tab')
                        </div>
                    </div> <!-- tab-3 -->

                    <div id="tab-4" class="tab-pane" v-show="selpermissions.transactionLogs==1 || setting == 1">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.commission-logs-tab')
                        </div>
                    </div> <!-- tab-4 -->

<!--                     <div id="tab-4" class="tab-pane" v-show="selpermissions.deposits==1 || setting == 1">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.deposits-tab')
                        </div>
                    </div>  -->
                    <!-- tab-4 -->

<!--                     <div id="tab-5" class="tab-pane" v-show="selpermissions.payments==1 || setting == 1">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.payments-tab')
                        </div>
                    </div>  -->
                    <!-- tab-5 -->

<!--                     <div id="tab-6" class="tab-pane" v-show="selpermissions.refunds==1 || setting == 1">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.refunds-tab')
                        </div>
                    </div>  -->
                    <!-- tab-6 -->
<!-- 
                    <div id="tab-7" class="tab-pane" v-show="selpermissions.discounts==1 || setting == 1">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.discounts-tab')
                        </div>
                    </div>  -->
                    <!-- tab-7-->

                    <div id="tab-8" class="tab-pane">
                        <div class="panel-body">
                            @include('cpanel.includes.visa.group.show.transactions-tab')
                        </div>
                    </div>
                    <!-- tab-8-->


                </div> <!-- tab-content -->
            </div> <!-- tabs-container -->
    	</div> <!-- row -->

    </div>
    </div> <!-- app -->

@endsection

@push('styles')

    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    {!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
    {!! Html::style(mix('css/visa/groups/show.css', 'cpanel')) !!}
    
@endpush

@push('scripts')

    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
	{!! Html::script('cpanel/plugins/typehead/bootstrap3-typeahead.min.js') !!}
    {!! Html::script('cpanel/plugins/datapicker/bootstrap-datepicker.js') !!}
    {!! Html::script('cpanel/plugins/inputmask/jquery.inputmask.bundle.js') !!}
    {!! Html::script(mix('js/visa/groups/show.js', 'cpanel')) !!}

@endpush
