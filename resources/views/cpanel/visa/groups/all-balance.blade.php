@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
{!! Html::style('cpanel/plugins/textSpinners/spinners.css') !!}
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
{!! Html::style('cpanel/plugins/iCheck/custom.css') !!}


<style>
.ui-widget.success-dialog {
    font-family: Verdana,Arial,sans-serif;
    font-size: 1em;
}

.ui-widget-overlay {
    background-color: transparent !important;
    }

.ui-widget-content.success-dialog {
    background: #F9F9F9;
    border: 1px solid #1ab394;
    color: #222222;
}

.ui-dialog.success-dialog {
    left: 0;
    outline: 0 none;
    padding: 0 !important;
    position: absolute;
    top: 0;
}

.ui-dialog.success-dialog .ui-dialog-content {
    background: none repeat scroll 0 0 transparent;
    border: 0 none;
    overflow: auto;
    position: relative;
    padding: 2px !important;
    margin: 0;
}

.ui-dialog.success-dialog .ui-widget-header {
    background: #1ab394;
    border: 0;
    color: #000;
    font-weight: normal;
}

.ui-dialog-titlebar .ui-button{
    float: right !important;
    font-size: 0.8em;
}
.ui-dialog.success-dialog .ui-dialog-titlebar {
    padding: 0.1em .5em;
    position: relative;
    font-size: 1em;
}

.ui-datepicker-calendar {
    display: none;
}

.control-label{
    padding-top: 8px;
    font-size: 15px;
}

.ibox{
    margin-bottom: 70px;
}
</style>
<!-- <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/ui-lightness/jquery-ui.css"> -->
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>All Balance</h2>
        </div>
    </div>

    <div id="app" class="wrapper wrapper-content" v-cloak>
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-payments">Payments</a></li>
                <li><a data-toggle="tab" href="#tab-list"> List of Initial Balances</a></li>
            </ul>
            <div class="tab-content">

                <div id="tab-payments" class="tab-pane active">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="spacer-10"></div>

                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">

                                        <div style="height:10px; clear:both;"></div>

                                        <div v-if="loading">
                                            <div class="sk-spinner sk-spinner-wave" style="margin-top:20px;">
                                                <div class="sk-rect1"></div>
                                                <div class="sk-rect2"></div>
                                                <div class="sk-rect3"></div>
                                                <div class="sk-rect4"></div>
                                                <div class="sk-rect5"></div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 pull-left">
                                            <span><b>Total Initial Balance : <span id="total-balance">@{{total_temp_balance}}</span> </b></span><br>
                                            <span><b>Total Balance Collected : <span id="total-collectables">@{{total_distribute}}</span> </b></span><br>
                                            <span><b>Total Remaining Balance : <span id="total-distribute">@{{ (total_temp_balance + total_distribute).toFixed(2)}}</span> </b></span>
                                        </div>
                                        <div v-show="!loading" class="table-responsive">
                                            <div class="m-t-2">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="table-responsive no-side-padding">
                                                            <table id="paymentlists" class="table table-striped table-bordered table-hover dataTables-example">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Date</th>
                                                                        <th>Initial</th>
                                                                        <th>Payment </th>
                                                                        <th>Balance </th>
                                                                        <th>Total Collected </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr is="payment-item"
                                                                      v-for="pay in allpayment"
                                                                      :pay="pay"
                                                                      :key="pay.id"
                                                                    >
                                                                    </tr>                              
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div id="tab-list" class="tab-pane">
                    <div class="panel-body">
                         <div class="row">
                            <div class="col-lg-12">

                                <div class="spacer-10"></div>

                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">

                                        <div style="height:10px; clear:both;"></div>

                                        <div v-if="loading">
                                            <div class="sk-spinner sk-spinner-wave" style="margin-top:20px;">
                                                <div class="sk-rect1"></div>
                                                <div class="sk-rect2"></div>
                                                <div class="sk-rect3"></div>
                                                <div class="sk-rect4"></div>
                                                <div class="sk-rect5"></div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 pull-left">
                                            <span><b>@lang('cpanel/groups-page.total-balance'): <span id="total-balance">@{{total_temp_balance}}</span> </b></span><br>
                                            <span><b>@lang('cpanel/groups-page.total-collectables'): <span id="total-collectables">@{{total_temp_col}}</span> </b></span>
                                        </div>

                                        <div v-show="!loading" class="table-responsive">
                                            <div class="m-t-2">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="table-responsive no-side-padding">
                                                            <table id="balancelists" class="table table-striped table-bordered table-hover dataTables-example">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Balance</th>
                                                                        <th>Collectables</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr is="balance-item"
                                                                      v-for="bal in allbalance"
                                                                      :bal="bal"
                                                                      :key="bal.id"
                                                                    >
                                                                    </tr>                              
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- app -->

@endsection

@push('styles')
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}

    {!! Html::style(mix('css/visa/groups/index.css', 'cpanel')) !!}

@endpush

@push('scripts')
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    {!! Html::script('cpanel/plugins/typehead/bootstrap3-typeahead.min.js') !!}
    {!! Html::script('cpanel/plugins/iCheck/icheck.min.js') !!}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {!! Html::script('shopping/plugins/fileDownload/filedownload.js') !!}
    {!! Html::script(mix('js/visa/groups/index.js', 'cpanel')) !!}

@endpush
