@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
{!! Html::style('cpanel/plugins/textSpinners/spinners.css') !!}
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
{!! Html::style('cpanel/plugins/iCheck/custom.css') !!}


<style>
.ui-widget.success-dialog {
    font-family: Verdana,Arial,sans-serif;
    font-size: 1em;
}

.ui-widget-overlay {
    background-color: transparent !important;
    }

.ui-widget-content.success-dialog {
    background: #F9F9F9;
    border: 1px solid #1ab394;
    color: #222222;
}

.ui-dialog.success-dialog {
    left: 0;
    outline: 0 none;
    padding: 0 !important;
    position: absolute;
    top: 0;
}

.ui-dialog.success-dialog .ui-dialog-content {
    background: none repeat scroll 0 0 transparent;
    border: 0 none;
    overflow: auto;
    position: relative;
    padding: 2px !important;
    margin: 0;
}

.ui-dialog.success-dialog .ui-widget-header {
    background: #1ab394;
    border: 0;
    color: #000;
    font-weight: normal;
}

.ui-dialog-titlebar .ui-button{
    float: right !important;
    font-size: 0.8em;
}
.ui-dialog.success-dialog .ui-dialog-titlebar {
    padding: 0.1em .5em;
    position: relative;
    font-size: 1em;
}

.ui-datepicker-calendar {
    display: none;
}

.control-label{
    padding-top: 8px;
    font-size: 15px;
}

.ibox{
    margin-bottom: 70px;
}
</style>
<!-- <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/ui-lightness/jquery-ui.css"> -->
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/groups-page.groups')</h2>
        </div>
    </div>

    <div id="app" class="wrapper wrapper-content" v-cloak>
        <div class="row">
            @include('cpanel.includes.visa.group.modals2')
        	<div class="col-lg-12">

                <a href="#" data-toggle="modal" data-target="#addNewGroupModal" class="btn btn-primary btn-sm pull-right" v-show="selpermissions.addNewGroup==1 || setting == 1"><i class="fa fa-user-plus fa-fw" ></i> @lang('cpanel/groups-page.add-new-group')</a>

                <a href="/visa/group/all-balance" target="_blank" class="btn btn-primary btn-sm pull-right" style="margin-right: 10px;"><i class="fa fa-user-plus fa-fw" ></i> Check all balance</a>

                <div class="spacer-10"></div>

	        	<div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <form class="form-inline pull-right" v-if="is_master" v-cloak>
                            <div class="form-group">
                                <label for="exampleInputName2"><strong>@lang('cpanel/groups-page.branch'):</strong></label>
                                <select class="form-control" name="branch" v-model="selectedBranch" @change="loadGroup">
                                    <option value="0">@lang('cpanel/groups-page.all')</option>
                                    <option v-for="branch in branches" :value="branch.id">@{{ branch.name }}</option>
                                </select>
                            </div>
                        </form>

                        <div style="height:10px; clear:both;"></div>

                        <div v-if="loading">
                            <div class="sk-spinner sk-spinner-wave" style="margin-top:20px;">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                        </div>

                    	<div v-show="!loading" class="table-responsive">
		                    <table id="lists" class="table table-striped table-bordered table-hover dataTables-example">
			                    <thead>
				                    <tr>
				                        <th>@lang('cpanel/groups-page.group-name')</th>
                                        <th style="width:10%">@lang('cpanel/groups-page.balance')</th>
				                        <th style="width:10%">@lang('cpanel/groups-page.collectables')</th>
				                        <th style="width:15%">@lang('cpanel/groups-page.leader')</th>
                                        <th style="width:20%">@lang('cpanel/groups-page.latest-package')</th>
				                        <th style="width:15%">@lang('cpanel/groups-page.latest-service')</th>
				                        <th style="width:10%">@lang('cpanel/groups-page.action')</th>
				                    </tr>
			                    </thead>

			                </table>
                            <div class="col-lg-12 pull-left">
                                <span><b>@lang('cpanel/groups-page.total-balance'): <span id="total-balance">{{$balance}}</span> </b></span><br>
                                <span><b>@lang('cpanel/groups-page.total-collectables'): <span id="total-collectables">{{$collect}}</span> </b></span>
                            </div>
			            </div>

                    </div>
                </div>

	        </div>
        </div>

        <!-- Add New Group Modal -->
        <div class="modal fade" id="addNewGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <form class="form-horizontal" @submit.prevent="addNewGroup">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">@lang('cpanel/groups-page.add-new-group')</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group" :class="{ 'has-error': form.errors.has('groupName') }">
                                <label class="col-sm-2 control-label">
                                    @lang('cpanel/groups-page.group-name')
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control m-top-0" v-model="form.groupName" name="groupName" placeholder="@lang('cpanel/groups-page.group-name')" >
                                    <span class="help-block" v-if="form.errors.has('groupName')" v-text="form.errors.get('groupName')"></span>
                                </div>
                            </div>

                            <div class="form-group" :class="{ 'has-error': form.errors.has('branch') }" v-if="is_master" v-cloak>
                                <label class="col-sm-2 control-label">
                                    @lang('cpanel/groups-page.branch')
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="branch" v-model="form.branch" @change="clearLeader">
                                        <option v-for="branch in branches" :value="branch.id">@{{ branch.name }}</option>
                                    </select>
                                    <span class="help-block" v-if="form.errors.has('branch')" v-text="form.errors.get('branch')"></span>
                                </div>
                            </div>

                            <div class="form-group" :class="{ 'has-error': form.errors.has('leader') }">
                                <label class="col-sm-2 control-label">
                                    @lang('cpanel/groups-page.leader')
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="col-sm-10">
                                    <input v-show="leader == ''" type="text" placeholder="[@lang('cpanel/groups-page.select-leader')]" class="leader_typeahead form-control m-top-0" name="leader" />

                                    <div v-show="leader != ''" class="badge badge-primary badge-leader">
                                        <span class="badge-leader-text">@{{ leader }}</span>
                                        <button type="button" class="close" data-dismiss="badge" aria-label="Close"><span @click="clearLeader" aria-hidden="true">&times;</span></button>
                                    </div>

                                    <span class="help-block" v-if="form.errors.has('leader')" v-text="form.errors.get('leader')"></span>
                                </div>
                            </div>

                            <!-- <div class="hr-line-dashed"></div>

                            <div class="form-group" :class="{ 'has-error': form.errors.has('contact') }">
                                <label class="col-sm-2 control-label">
                                    Contact
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="col-sm-10">
                                    <select class="form-control" v-model="form.contact" name="contact">
                                        <option v-for="contact in contacts" :value="contact">
                                            @{{ contact }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group" :class="{ 'has-error': form.errors.has('address') }">
                                <label class="col-sm-2 control-label">
                                    Address
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="col-sm-10">
                                    <label class="radio-inline">
                                        <input type="radio" v-model="addressOptions" value="current address" checked>
                                        <span class="m-left-60">Use Current Address</span>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" v-model="addressOptions" value="different address">
                                        <span class="m-left-60">Use Different Address</span>
                                    </label>

                                    <div class="spacer-10"></div>

                                    <input type="text" class="form-control m-top-0" name="address" v-model="form.address" :readonly="addressOptions == 'current address'">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Barangay</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control m-top-0" v-model="form.barangay" :readonly="addressOptions == 'current address'" />
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group" :class="{ 'has-error': form.errors.has('city') }">
                                <label class="col-sm-2 control-label">
                                    City
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control m-top-0" name="city" v-model="form.city" :readonly="addressOptions == 'current address'">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group" :class="{ 'has-error': form.errors.has('province') }">
                                <label class="col-sm-2 control-label">
                                    Province
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control m-top-0" name="province" v-model="form.province" :readonly="addressOptions == 'current address'">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Zip Code</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control m-top-0" v-model="form.zipCode" :readonly="addressOptions == 'current address'">
                                </div>
                            </div> -->

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">@lang('cpanel/groups-page.close')</button>
                            <button type="submit" class="btn btn-primary">@lang('cpanel/groups-page.add')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div> <!-- app -->

@endsection

@push('styles')
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}

    {!! Html::style(mix('css/visa/groups/index.css', 'cpanel')) !!}

@endpush

@push('scripts')
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    {!! Html::script('cpanel/plugins/typehead/bootstrap3-typeahead.min.js') !!}
    {!! Html::script('cpanel/plugins/iCheck/icheck.min.js') !!}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {!! Html::script('shopping/plugins/fileDownload/filedownload.js') !!}
    {!! Html::script(mix('js/visa/groups/index.js', 'cpanel')) !!}

@endpush
