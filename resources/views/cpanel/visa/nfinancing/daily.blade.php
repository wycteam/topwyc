@extends('cpanel.layouts.master')

@section('title', $title)
@push('styles')
  {!! Html::style('cpanel/plugins/daterangepicker/daterangepicker-bs3.css') !!}
  {!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/daily-cost-page.daily-cost')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">@lang('cpanel/daily-cost-page.home')</a>
                </li>
                <li class="active">
                    <strong>@lang('cpanel/daily-cost-page.daily-cost')</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <daily-content id="dailyCost" :translation="translation">

        </daily-content>
    </div>



@endsection
@push('scripts')
{!! Html::script(mix('js/visa/nfinancing/dailycost.js', 'cpanel')) !!}
{!! Html::script('cpanel/plugins/fullcalendar/moment.min.js') !!}
{!! Html::script('cpanel/plugins/inputmask/jquery.inputmask.bundle.js') !!}
@endpush
