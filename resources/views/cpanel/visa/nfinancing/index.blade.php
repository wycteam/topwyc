@extends('cpanel.layouts.master')

@section('title', 'Financial Monitor')
@push('styles')
    {!! Html::style(mix('css/visa/financing/financing.css', 'cpanel')) !!}
    {!! Html::style('https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css') !!}
    {!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
    {!! Html::style('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css') !!}
    {!! Html::style('https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css') !!}

@endpush
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Financial Monitor</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li class="active">
                    <strong>Financing</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <finance-content id="nFinancings">

        </finance-content>
    </div>



@endsection
@push('scripts')
<!-- {!! Html::script('https://code.jquery.com/jquery-3.3.1.js') !!} -->
{!! Html::script('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') !!}
{!! Html::script('https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js') !!}
{!! Html::script('https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js') !!}

  {!! Html::script('https://unpkg.com/popper.js') !!}
  {!! Html::script('https://unpkg.com/vue/dist/vue.js') !!}
  <!-- {!! Html::script('https://unpkg.com/v-tooltip') !!} -->
  {!! Html::script('https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js') !!}
  <!-- {!! Html::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js') !!} -->
  {!! Html::script('https://code.jquery.com/ui/1.12.1/jquery-ui.js') !!}
  {!! Html::script('cpanel/plugins/datapicker/bootstrap-datepicker.js') !!}
	{!! Html::script(mix('js/visa/nfinancing/financing.js', 'cpanel')) !!}

@endpush

