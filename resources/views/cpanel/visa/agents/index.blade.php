@extends('cpanel.layouts.master')

@section('title', $title)

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>{{ $title }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">{{ $home }}</a>
                </li>
                <li class="active">
                    <strong>{{ $title }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated service-box" id="agents" v-cloak>
        @include('cpanel.includes.visa.agents.modals')

        <div class="row">
            <div class="col-lg-12">

                <a class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#add-new-agent-modal" @click="$refs.addnewagent.resetAddNewAgentForm()">
                    <i class="fa fa-plus fa-fw"></i> @{{(translation) ? translation['add-new-shop'] : 'Add New Shop'}}
                </a>

                <div style="height:10px; clear:both;"></div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>@{{(translation) ? translation['list-shops'] : 'List of Shops'}}</h3>
                    </div>

                    <div class="ibox-content">

                        <div v-show="loading">
                            <div class="spiner-example">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                            </div>
                        </div> <!-- loading -->

                        <div v-show="!loading" class="table-responsive">
                            <table id="agents-datatable" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">@{{(translation) ? translation['id'] : 'ID'}}</th>
                                        <th>@{{(translation) ? translation['name'] : 'Name'}}</th>
                                        <th class="text-center">@{{(translation) ? translation['no-ref-clients'] : 'No. of Referred Clients'}}</th>
                                        <th>@{{(translation) ? translation['agent'] : 'Agent'}}</th>
                                        <th class="text-center"> @{{(translation) ? translation['cashflow'] : 'Cashflow'}}</th>
                                        <th class="text-center"> @{{(translation) ? translation['action'] : 'Action'}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="agent in agents">
                                        <td class="text-center">@{{ agent.user_id }}</td>
                                        <td>
                                            <a :href="'/visa/agents/'+agent.user_id" data-toggle="tooltip" :title="translation['view-agent']" target="_blank">
                                                @{{ agent.user.full_name }}
                                            </a>
                                        </td>
                                        <td class="text-center">@{{ agent.ref.length }}</td>
                                        <td>
                                            <span v-if="agent.midman">
                                                <a :href="'/visa/agents/'+agent.user_id+'/middleman/'+agent.midman.id" data-toggle="tooltip" :title="translation['view-midman-prof']" target="_blank">
                                                @{{ agent.midman.full_name }}
                                                </a>
                                            </span>
                                            <span v-else>@{{translation['not-applicable-short']}}</span>
                                        </a>
                                        </td>
                                        <td class="text-center">@{{ agent.total_cashflow }}</td>
                                        <td class="text-center">
                                            <a :href="'/visa/access-control/downloadQR/'+agent.user_id" target="_blank"  :title="translation['gen-qr-code']"><i class="fa fa-qrcode"></i></a>

                                            &nbsp;&nbsp;

                                            <a :href="'/visa/agents/'+agent.user_id" data-toggle="tooltip" :title="translation['view-agent']" target="_blank">
                                                <i class="fa fa-arrow-right fa-1x"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('styles')
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
	{!! Html::style(mix('css/visa/agents/index.css', 'cpanel')) !!}
@endpush

@push('scripts')
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    {!! Html::script(mix('js/visa/agents/index.js', 'cpanel')) !!}
@endpush

