@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnify/2.3.2/css/magnify.min.css">
    {!! Html::style('cpanel/plugins/lightbox/css/lightbox.css') !!}
    {!! Html::style('cpanel/plugins/dualListbox/bootstrap-duallistbox.min.css') !!}
    {!! Html::style('cpanel/plugins/blueimp/css/blueimp-gallery.min.css') !!}
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
    {!! Html::style('js/owl/owl.carousel.css') !!}
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    {!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
    {!! Html::style('js/owl/owl.theme.css') !!}
    {!! Html::style(mix('css/visa/clients/profile.css', 'cpanel')) !!}
    {!! Html::style(mix('css/visa/agents/index.css', 'cpanel')) !!}
    <style>
        tr.strikethrough td { 
            text-decoration: line-through !important;
        }
    </style>
@endpush

@section('content')
    <div class="wrapper wrapper-content" id="middleman" v-if="middleman != null">
            <a href="#"  target="_blank" class="load-pdf"></a>
            <div class="row">
                <div class="col-sm-12 no-side-padding fff-bg">
                    <div class="col-sm-3 no-side-padding">
                        <div class="ibox ">

                            <div class="ibox-content">
                                <div class="tab-content">
                                    <div id="contact-1" class="tab-pane active">
                                        <div class="row">   
                                            <div class="col-lg-4 text-center  no-side-padding">
                                                <div class="profile-image">
                                                    <img :src="middleman.avatar" class="img-circle circle-border m-b-md" alt="profile">
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <h3 class="no-margins m-t-2">
                                                    <b>@{{middleman.full_name}}</b>
                                                </h3>
                                                <div style="margin-top: 8px;">
                                                    <a id="editor" href="" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw" ></i> @{{(translation) ? translation['edit'] : 'Edit'}}</a>
                                                </div>                                                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 no-side-padding">
                        <div class="ibox no-b-margin">
                            <div class="ibox-content no-tb-padding no-side-padding">
                                <div class="tab-content">
                                    <div id="contact-1" class="tab-pane active">
                                        <div class="full-height-scroll details-size">

                                            <strong><h2>@{{(translation) ? translation['basic-info'] : 'Basic Information'}}</h2></strong>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <b>@{{(translation) ? translation['gender'] : 'Gender'}}:</b>
                                                </div>
                                                <div class="col-sm-8 no-side-padding">
                                                
                                                    <span class="m-l-sm totals" v-if="middleman.gender">@{{middleman.gender}}</span>
                                             
                                                    <span class="m-l-sm label label-danger" v-if="!middleman.gender">@{{(translation) ? translation['empty']+' '+translation['gender'] : 'Empty Gender'}}</span>
                                    
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <b>@{{(translation) ? translation['bday'] : 'Birthday'}}:</b>
                                                </div>
                                                <div class="col-sm-8 no-side-padding">
                                                 
                                                    <span class="m-l-sm totals m-r-2" v-if="middleman.birth_date">@{{middleman.birth_date}}</span>
                                                   
                                                    <span class="m-l-sm totals m-r-2 label label-danger" v-if="!middleman.birth_date">@{{(translation) ? translation['empty']+' '+translation['bday'] : 'Empty Birthday'}}</span>
                                                    
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <b>@{{(translation) ? translation['age'] : 'Age'}}:</b>
                                                </div>
                                                <div class="col-sm-8 no-side-padding">
                                                    <span class="m-l-sm totals" v-if="middleman.birth_date">@{{middleman.birth_date | age}}</span>
                                                  
                                                    <span class="m-l-sm totals m-r-2 label label-danger" v-if="!middleman.birth_date">@{{(translation) ? translation['empty']+' '+translation['bday'] : 'Empty Birthday'}}</span>
                                                   
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <b>@{{(translation) ? translation['address'] : 'Address'}}:</b>
                                                </div>
                                                <div class="col-sm-8 no-side-padding">
                                                    <span class="m-l-sm totals">@{{middleman.address}}</span>
                                                    <span class="m-l-sm totals m-r-2 label label-danger" v-if="!middleman.address">@{{(translation) ? translation['empty']+' '+translation['address'] : 'Empty Address'}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 no-side-padding">
                        <div class="ibox no-b-margin">
                            <div class="ibox-content no-tb-padding">
                                <div class="tab-content">
                                    <div id="contact-1" class="tab-pane active">
                                        <div class="full-height-scroll details-size">

                                            <strong><h2></h2></strong>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <b></b>
                                                </div>
                                                <div class="col-sm-7 no-side-padding">
                                                    <span class="m-l-sm totals" v-cloak></span>
                                                </div>
                                            </div>
     
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 no-side-padding">
                        <div class="ibox no-b-margin">
                            <div class="ibox-content no-tb-padding">
                                <div class="tab-content">
                                    <div id="contact-1" class="tab-pane active">
                                        <div class="full-height-scroll details-size">

                                            <strong><h2>@{{(translation) ? translation['stats'] : 'Statistics'}}</h2></strong>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <b>@{{(translation) ? translation['commi'] : 'Commission'}}:</b>
                                                </div>
                                                <div class="col-sm-7 no-side-padding">
                                                    <span class="m-l-sm totals" v-cloak> @{{commi}} </span>
                                                </div>
                                            </div>
     
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                    <div class="col-lg-12 m-t-md">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-store">@{{(translation) ? translation['agents'] : 'Agents'}}/@{{(translation) ? translation['shops'] : 'Shops'}}</a></li>
                            </ul>


                            <div class="tab-content">
                                <div id="tab-store" class="tab-pane active">
                                    <div class="panel-body no-side-padding" style="min-height: 600px;">
                                         @include('cpanel.visa.agents.middle-man.list-of-agents')
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col-sm-8 -->
    </div>

    
@endsection

@push('scripts')
    <!-- {!! Html::script('cpanel/plugins/jasny/jasny-bootstrap.min.js') !!} -->
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnify/2.3.2/js/jquery.magnify.min.js"></script>
    {!! Html::script('cpanel/plugins/lightbox/js/lightbox.js') !!}
    {!! Html::script('cpanel/plugins/dualListbox/jquery.bootstrap-duallistbox.js') !!}
    {!! Html::script('cpanel/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('/js/owl/owl.carousel.js') !!}
    {!! Html::script('cpanel/plugins/typehead/bootstrap3-typeahead.min.js') !!}
    {!! Html::script('cpanel/plugins/datapicker/bootstrap-datepicker.js') !!}
    {!! Html::script('cpanel/plugins/inputmask/jquery.inputmask.bundle.js') !!}
    {!! Html::script(mix('js/visa/agents/index.js','cpanel')) !!}

@endpush

@push('scripts')
<script>
    $(document).ready(function(){
      var owl = $(".trackings");
            owl.owlCarousel({
                items : 4,
                lazyLoad : true,
                navigation : false,
                pagination :false,
                rewindNav:false
            });
            $(".next").click(function(){
                owl.trigger('owl.next');
            });
            $(".prev").click(function(){
                owl.trigger('owl.prev');
            });
    });
        
</script>
    
@endpush
