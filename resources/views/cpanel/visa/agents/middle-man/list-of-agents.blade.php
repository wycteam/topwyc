 <div class="container m-t-2">
    <div class="row">
        <div class="col-sm-12">
            <div class="span12 newspan">
                <div class="customNavigation item-package">
                    <a class="btn prev pull-left" style="background-color: transparent;">{{ HTML::image('js/owl/arrow left.png') }}</a>
                    <a class="btn next pull-right" style="background-color: transparent;">{{ HTML::image('js/owl/arrow right.png') }}</a>
                </div>
                <div id="owl-demo" class="trackings owl-carousel">
                    <agents-list
                      v-for="a in agents"
                      :a="a"
                      :key="a.id"
                    >
                    </agents-list>
                </div>
            </div>
        </div>

        <div style="height:10px; clear:both;"></div>
        
        <middleman-referred-clients ref="middlemanreferredclients"></middleman-referred-clients>
    </div>
</div>