@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
<style>
    .custom-tbl-footer{
        position: fixed;
        bottom: 35px;
        width: 1127px;
        margin-right: auto;
        margin-left: auto;
    }

    #tab-1 .table-responsive{
        max-height: 456px !important;
    }

    

</style>
@endpush

@section('content')

    <div id="app">
        <div class="row wrapper border-bottom white-bg page-heading" v-if="agent != null" v-cloak>
            <div class="col-md-3">
                <h2>@{{ (translation) ? translation['shop'] : 'Shop' }}::@{{ agent.first_name }}</h2>
            </div>

            <div class="col-md-7" style="margin-top: 20px">
                 @include('cpanel.includes.visa.agents.modals')
                <div class="">
                    <div class="row">
                        <div class="col-md-3">
                            <span class="text-muted">@{{ (translation) ? translation['agent'] : 'Agent' }}:</span>
                            <span v-if="middleman">
                                <a :href="'/visa/agents/' + agent.id + '/middleman/' + middleman.id" target="_blank">
                                     @{{ middleman.first_name + ' ' + middleman.last_name }}
                                </a>
                            </span>
                            <span v-else>@{{ (translation) ? translation['not-applicable-short'] : 'N/A' }}</span>
                        </div>

                        <div class="col-md-4">
                            <span class="text-muted">@{{ (translation) ? translation['no-ref-clients'] : 'No. of Referred Clients' }}:</span>
                           <span> @{{ $refs.referredclients.referredClients.length }} </span>
                        </div>
                        <div class="col-md-2">
                            <span class="text-muted">Rate :</span>
                            <span>@{{ agent.current_rate }}</span>
                        </div>
                        <div class="col-md-3">
                            <span class="text-muted">@{{ (translation) ? translation['points'] : 'Points' }}:</span>
                            <span>@{{ agent.commission }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2" style="margin-top: 20px">
                <div class="">
                    <div class="row">
                         <div class="col-md-6">
                             <a href="#">@{{ (translation) ? translation['add-funds'] : 'Add Funds' }}</a>
                         </div>
                         <div class="col-md-6">
                             <a href="#" data-toggle="modal" data-target="#edit-agent-modal" @click="setAgent">
                                @{{ (translation) ? translation['edit'] : 'Edit' }} <i class="fa fa-edit cursor"></i>
                            </a>
                         </div>
                    </div>
                    
                </div>     
            </div>
        </div> <!-- wrapper -->

        <div class="wrapper wrapper-content">

            <div class="row ibox" v-if="agent != null">
                 
                <!-- <div class="ibox-content">
                    <div class="col-lg-7">
                        <div class="row">
                            <div class="col-lg-5 text-center font-bold text-info">
                                <h2><i><b>@{{ agent.first_name }}</b></i></h2>
                                <p style="color:black;">
                                    <span class="text-muted">@{{ (translation) ? translation['midman'] : 'Middleman' }}:</span> 
                                    <span v-if="middleman">
                                        <a :href="'/visa/agents/' + agent.id + '/middleman/' + middleman.id" target="_blank">
                                            @{{ middleman.first_name + ' ' + middleman.last_name }}
                                        </a>
                                    </span>
                                    <span v-else>@{{ (translation) ? translation['not-applicable-short'] : 'N/A' }}</span>
                                </p>
                                <p style="color:black;">
                                    <span class="text-muted">@{{ (translation) ? translation['no-ref-clients'] : 'No. of Referred Clients' }}:</span> @{{ $refs.referredclients.referredClients.length }}
                                </p>
                                <a href="#" data-toggle="modal" data-target="#edit-agent-modal" @click="setAgent">
                                    @{{ (translation) ? translation['edit'] : 'Edit' }} <i class="fa fa-edit cursor"></i>
                                </a>
                            </div>
                            <div class="col-lg-7">
                                <h4>@{{ (translation) ? translation['email'] : 'Email' }}: 
                                    <span style="font-weight: 400;font-size: 13px;">
                                        @{{ agent.email }}
                                    </span>
                                </h4>
                                <h4>@{{ (translation) ? translation['address'] : 'Address' }}: 
                                    <span style="font-weight: 400;font-size: 13px;">
                                        @{{ agent.address }}
                                    </span>
                                </h4>
                                <h4>@{{ (translation) ? translation['cont-no'] : 'Contact Number' }}: 
                                    <span style="font-weight: 400;font-size: 13px;">
                                        @{{ agent.contact_number }}
                                    </span>
                                </h4>
                                <h4>@{{ (translation) ? translation['alt-cont-no'] : 'Alternate Contact Number' }}: 
                                    <span style="font-weight: 400;font-size: 13px;">
                                        @{{ agent.alternate_contact_number }}
                                    </span>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <table class="table table-hover no-margins">
                            <tr>
                                <td><b>@{{ (translation) ? translation['commi'] : 'Commission' }}:</b></td>
                                <td>@{{ agent.commission }}</td>
                            </tr>
                        </table>
                    </div>

                    <div style="height:1px;clear:both;"></div>
                </div> -->
            </div> <!-- row -->

            <div class="row" v-cloak>
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> @{{(translation) ? translation['ref-client'] : 'Referred Clients'}}</a></li>
                    </ul>

                    <div class="tab-content">

                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body" style="height: 570px;">

                                <div class="table-responsive">
                                    <referred-clients ref="referredclients"></referred-clients>
                                </div>

                            </div>
                        </div> <!-- tab-1 -->

                    </div>
                </div>
            </div> <!-- row -->

        </div><!-- wrapper -->

    </div> <!-- app -->

@endsection

@push('styles')
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    <style>
        tr.strikethrough td { 
            text-decoration: line-through !important;
        }
    </style>
@endpush

@push('scripts')
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    {!! Html::script(mix('js/visa/agents/show.js', 'cpanel')) !!}
@endpush