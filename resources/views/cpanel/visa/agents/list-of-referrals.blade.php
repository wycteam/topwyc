<div class="m-t-2">
    <div class="row">
        <div class="col-sm-12" style="padding-left: 3%; padding-right: 5%;">
            <div><a href='#' @click="getAllReferrals()">@{{(translation) ? translation['show-ref'] : 'Show all Referrals'}}</a></div>
            <div class="table-responsive no-side-padding">
                <table id="lists" class="table table-striped table-bordered table-hover dataTables-example" style="    margin: 3.5em 0 !important;">
                    <thead>
                        <tr>
                            <th class="text-center"></th>
                            <th class="text-center">@{{(translation) ? translation['ID'] : 'ID'}}</th>
                            <th class="text-center">@{{(translation) ? translation['name'] : 'Name'}}</th>
                            <th class="text-center">@{{(translation) ? translation['total-cost'] : 'Total Cost'}}</th>
                            <th class="text-center">@{{(translation) ? translation['points-earned'] : 'Points Earned'}}</th>
                        </tr>
                    </thead>
                    <tbody>
                         <tr is="referral-list"
                                  v-for="refer in referrals"
                                  :refer="refer"
                                  :key="refer.id"
                                >
                                </tr>                     
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>