@extends('cpanel.layouts.master')

@section('title', $title)

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>{{ $title }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li class="active">
                    <strong>{{ $title }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
        	<div class="col-md-8 col-md-offset-2">

        		<div class="ibox float-e-margins">
                    <div class="ibox-content">

                    	@foreach($services as $service)
                    		<h2 class="font-bold">{{ $service->detail }}</h2>

                    		<ul class="list-group">
                    			@foreach($service->children as $child)
                    				<li class="list-group-item">
                    					<span class="badge badge-primary">{{ $child->cost + $child->charge + $child->tip + $child->com_client + $child->com_agent }}</span>
                    					{{ $child->detail }}
                    				</li>
                    			@endforeach
                    		</ul>

                    		<div class="hr-line-dashed"></div>
                    	@endforeach

                    </div>
                </div>

        	</div>
        </div>
    </div>

@endsection