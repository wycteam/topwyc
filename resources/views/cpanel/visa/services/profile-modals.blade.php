


<modal size="modal-sm" id='addProfile'>
    <template slot="modal-title">
        <h3 id="title"></h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <form method="get" class="form-horizontal">
            <!-- english service detail -->
            <div class="form-group" :class="{ 'has-error': addProfFrm.errors.has('name') }">
                <label class="control-label">@lang('cpanel/service-profile-page.profile-name')</label>
                <input class="form-control m-b" id="name" type="text" v-model="addProfFrm.name" name="name" required>
                <input  type="hidden" v-model="addProfFrm.id" name="id">
                <p class="help-block" v-if="addProfFrm.errors.has('name')" v-text="addProfFrm.errors.get('name')"></p>
            </div><!-- end label-floating -->

            <div class="form-group">
                <label class="col-md-3 control-label">
                    @lang('cpanel/service-profile-page.status'):
                </label>
                <div class="col-md-9">
                    <div style="height:5px;clear:both;"></div>
                    <label class=""> <input type="radio" value="0" name="active" v-model="addProfFrm.is_active"> <i></i> @lang('cpanel/service-profile-page.disabled') </label>

                    <label class="m-l-2"> <input type="radio" value="1" name="active" v-model="addProfFrm.is_active"> <i></i> @lang('cpanel/service-profile-page.enabled') </label>
                </div>
            </div>
        </form>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal" @click="emptyform()">@lang('cpanel/service-profile-page.cancel')</button>
        <button class='btn btn-primary'  @click="saveform()">@lang('cpanel/service-profile-page.save')</button>
    </template>
</modal>


<modal size="modal-sm" id='editProfile'>
    <template slot="modal-title">
        <h3 id="title"></h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <form method="get" class="form-horizontal">
            <!-- english service detail -->
            <div class="form-group">
                <label class="control-label">@{{ editCostFrm.service }}</label>
                <input  type="hidden" v-model="editCostFrm.id" name="id">
            </div><!-- end label-floating -->
            <br>
            <div class="form-group">
                <label for="charge" class="col-sm-3 control-label">Charge: </label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="charge" placeholder="Charge" v-model="editCostFrm.charge">
                </div>
            </div>

            <div class="form-group">
                <label for="tip" class="col-sm-3 control-label">Tip: </label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="tip" placeholder="Tip" v-model="editCostFrm.tip">
                </div>
            </div>

            <div class="form-group">
                <label for="cost" class="col-sm-3 control-label">Cost: </label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="cost" placeholder="Cost" v-model="editCostFrm.cost">
                </div>
            </div>

            <div class="form-group">
                <label for="agent" class="col-sm-3 control-label">Agent: </label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="agent" placeholder="Agent's Commission" v-model="editCostFrm.com_agent">
                </div>
            </div>

            <div class="form-group">
                <label for="agent" class="col-sm-3 control-label">Client: </label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="agent" placeholder="Client's Commission" v-model="editCostFrm.com_client">
                </div>
            </div>

        </form>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal" @click="emptyEditCostform()">@lang('cpanel/service-profile-page.cancel')</button>
        <button class='btn btn-primary'  @click="saveform()">@lang('cpanel/service-profile-page.save')</button>
    </template>
</modal>
