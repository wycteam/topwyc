@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style(mix('css/visa/services/index.css', 'cpanel')) !!}
    <style>
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
          -webkit-appearance: none; 
          margin: 0; 
        }

        .form-group {
             margin-bottom: 0px !important; 
        }

        table.dataTable {
            margin-top: 35px !important;
        }
    </style>
@endpush

@section('content')

        <div class="wrapper wrapper-content  service-box" id="services" v-cloak>
             @include('cpanel.visa.services.modals')
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 v-html="page_title"></h5>
                            <div class="ibox-tools">
                                <template v-if="addEditDeleteAuth">
                                  
                                    <a v-if="pageWizard==5" class="btn btn-primary btn-sm pull-right" @click="addServiceProfile" style="margin-top:-8px">
                                        <i class="fa fa-plus fa-fw"></i>
                                         Add Service Profile
                                    </a>
                                    <a v-else class="btn btn-primary btn-sm pull-right" @click="showWizard(5,'')" style="margin-top:-8px">
                                        <i class="fa fa-eye fa-fw"></i>
                                         Service Profile
                                    </a>

                                    <a class="btn btn-primary btn-sm pull-right" @click="showWizard(2,'add')" v-show="selpermissions.addNewService==1 || setting == 1" style="margin-top:-8px">
                                        <i class="fa fa-plus fa-fw"></i>
                                        @lang('cpanel/list-of-services-page.add-new-service')
                                    </a>
                                    <!--
                                    <a class="btn btn-primary btn-sm pull-right" @click="addServiceProfile()">
                                        <i class="fa fa-plus fa-fw"></i>
                                        Add Service Profile
                                   
                                   
                                    <a class="btn btn-primary btn-sm pull-right" @click="addOrEdit('add')" v-show="selpermissions.addNewService==1 || setting == 1">
                                        <i class="fa fa-plus fa-fw"></i>
                                        @lang('cpanel/list-of-services-page.add-new-service')
                                    </a>
                                     </a> 
                                     -->
                                   
                                </template>
                            </div>
                        </div>
                        <div class="ibox-content" v-show="!loading">

                            <div class="table-responsive" v-show="pageWizard==1">
                                <table class="table table-striped table-bordered table-hover dataTables-example" id="lists" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Service</th>
                                            <th>Total Service Charge</th>
                                           <!-- <th>Cost</th>
                                            <th>Charge</th>
                                            <th>Tip</th>-->
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="service in services"
                                            :item="service" 
                                            :key="service.id"
                                            is="service-list">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="wrapper wrapper-content" style="padding: 20px 10px 20px !important;" v-show="pageWizard==2">
                      
                                <div class="ibox float-e-margins">
                                  
                                    <div class="ibox-content" style="border-style:none !important">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                    <template v-if="addorEditFunction == 'add'">
                                                        <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('type') }" style="margin-right: 10px">
                                                                <label class="control-label">@lang('cpanel/list-of-services-page.service-type')</label>
                                                                <select class="form-control m-b" v-model="addServFrm.type" id="type" @change="changeServiceType()" name="type">
                                                                    <option value='Parent'>
                                                                        @lang('cpanel/list-of-services-page.parent')
                                                                    </option>
                                                                    <option value='Child'>
                                                                        @lang('cpanel/list-of-services-page.child')
                                                                    </option>
                                                                </select>
                                                               
                                                        </div><!-- end label-floating -->
                                                    </template>
                                                    <template v-else>
                                                        <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('type') }" style="margin-right: 10px">
                                                                <label class="control-label">@lang('cpanel/list-of-services-page.service-type')</label>
                                                                <select class="form-control m-b" v-model="addServFrm.type" id="type" name="type" disabled>
                                                                    <option value='Parent'>
                                                                        @lang('cpanel/list-of-services-page.parent')
                                                                    </option>
                                                                    <option value='Child'>
                                                                        @lang('cpanel/list-of-services-page.child')
                                                                    </option>
                                                                </select>
                                                               
                                                        </div>
                                                    </template>
                                                </div>
                                                <div class="form-group col-sm-4" :class="{ 'has-error': addServFrm.errors.has('parent_id') }" v-if="showChildOptions">
                                                    <label class="control-label">@lang('cpanel/list-of-services-page.service-parent')</label>
                                                    <select class="form-control m-b" v-model="addServFrm.parent_id" id="parent_id" name="parent_id" >
                                                        <option v-for="option in serviceParents" v-bind:value="option.id" v-if="activeId != option.id">
                                                            <p>@{{ option.detail }}</p>
                                                        </option>
                                                    </select>
                                                    
                                                </div><!-- end label-floating -->
                                        </div>
                                        <div class="row">
                                           
                                            <div class="col-sm-6 b-r">
                                               
                                                    <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('detail') }" >
                                                        <label class="control-label">@lang('cpanel/list-of-services-page.service-name')</label> 
                                                        <input type="text" placeholder="Service Name" class="form-control" id="detail" v-model="addServFrm.detail" name="detail">
                                                    </div>
                                                    <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('description') }">
                                                        <label>@lang('cpanel/list-of-services-page.description')</label> 
                                                        <textarea rows="2" style ='resize:none' class="form-control m-b" placeholder="Description" v-model="addServFrm.description" id="description" name="description"></textarea>   
                                                        
                                                    </div>
                                                    
                                                   
                                               
                                            </div>
                                            <div class="col-sm-6"> 
                                                    <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('detail_cn') }">
                                                        <label>@lang('cpanel/list-of-services-page.service-name-chinese')</label> 
                                                        <input type="text" placeholder="Service Name  (Chinese)" class="form-control" id="detail" v-model="addServFrm.detail_cn" name="detail_cn">
                                                       
                                                    </div>
                                                    <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('description_cn') }">
                                                        <label>@lang('cpanel/list-of-services-page.description-chinese')</label> 
                                                        <textarea rows="2" style ='resize:none' class="form-control m-b" placeholder="Description (Chinese)" v-model="addServFrm.description_cn" id="description_cn" name="description_cn"></textarea>
                                                        
                                                    </div>
                                                   
                                            </div>
                                            
                                           
                                            <div class="col-sm-4 col-sm-offset-8 " style="margin-top:20px;" v-if="!showChildOptions" >
                                                <vue-button-spinner :is-loading="isLoading" :disabled="isLoading" :status="status" class='btn btn-success pull-right'  v-on:click.native="saveForm()" style="height:34px;margin:2px"><span style="font-size: 16px;padding: 2px 8px;">@lang('cpanel/list-of-services-page.save')</span></vue-button-spinner>
                                                <!--<button style="margin:2px" v-if="showChildOptions" class="btn btn-success pull-right" @click="nxtWizard('n')">Next</button> -->
                                                <button style="margin:2px" class="btn btn-success pull-right" @click="showWizard(1,'')">Back</button>
                                               
                                            </div>
                                            
                                       
                                        </div>
                                <div class="row"  v-show="addServFrm.type=='Child'">
                                    <div class="col-sm-10">
                                        <h1 style="margin:5px 5px 5px 220px;text-align:center" >Pricing</h1>
                              
                                    </div>
                                    <div class="col-sm-2">
                                                <select v-if="addorEditFunction=='edit'"  @change="selectProfile2" v-model="serviceProfile" class="form-control m-b pull-right">
                                                    <option :value="0">Regular Rate</option>
                                                    <option v-for="p in profiles" :value="p.id">@{{p.name}}</option>
                                                    
                                                </select>
                                    </div>
                                    
                                                
                                                <template >
                                                
                                                        <template v-for="b in addServFrm.feeArray">
                                                            <div class="col-sm-12" >
                                                                <div class="ibox float-e-margins">
                                                                    <div class="ibox-title" style="border:none">
                                                                        <h3 style="margin:0 auto;text-align:center !important;color:#696969;">@{{b.branch}}</h3>
                                                                    </div>
                                                                    <div class="ibox-content" style="border-bottom:1px dashed #e5e6e7;border-top:none">
                                                                        <div class="row" style="padding-bottom:5px">
                                                                            <div class="col-sm-2">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-2 control-label">Cost</label>
                                                                                    <div class="col-sm-10">
                                                                                     <input type="text" class="form-control" v-model="b.cost">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-3 control-label">Charge</label>
                                                                                    <div class="col-sm-9">
                                                                                     <input type="text" class="form-control" v-model="b.charge">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-2 control-label">Tip</label>
                                                                                    <div class="col-sm-10">
                                                                                     <input type="text" class="form-control" v-model="b.tip">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-6 control-label" >Agent's Commission</label>
                                                                                    <div class="col-sm-6">
                                                                                     <input type="text" class="form-control" v-model="b.com_agent" :disabled="serviceProfile == 0">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-6 control-label">Client's Commission</label>
                                                                                    <div class="col-sm-6">
                                                                                     <input type="text" class="form-control" v-model="b.com_client" :disabled="serviceProfile == 0">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                       <!-- <table class="table table-bordered">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Cost</th>
                                                                                <th>Charge</th>
                                                                                <th>Tip</th>
                                                                                <th>Agent's Commission</th>
                                                                                <th>Client's Commission</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Cost</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.cost"></td>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Charge</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.charge"></td>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Tip</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.tip"></td>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Agent's Commission</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.com_agent"></td>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Client's Commission</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.com_client"></td>
                                                                            </tr>
                                                                            
                                                                            </tbody>
                                                                        </table>-->

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </template>
                                                    
                                                    </template>
                                                    <div class="col-sm-4 col-sm-offset-8 " style="margin-top:20px;">
                                                    <vue-button-spinner :is-loading="isLoading" :disabled="isLoading" :status="status" class='btn btn-success pull-right'  v-on:click.native="saveForm()" style="height:34px;margin:2px"><span style="font-size: 16px;padding: 2px 8px;">@lang('cpanel/list-of-services-page.save')</span></vue-button-spinner>
                         
                                                    <button style="margin:2px" class="btn btn-success pull-right" @click="nxtWizard('b')">Back</button>
                                                    </div>
                                                </div>

                                    </div>
                                </div>
                           
                            </div>
                            <!--
                            <div class="wrapper wrapper-content" style="padding: 20px 10px 20px !important;" v-show="pageWizard==3">
                             <div class="ibox float-e-margins">
                             <div class="ibox-content" style="border-style:none !important">
                             <div class="row">
                                    <div class="col-sm-10">
                                        <h1 style="margin:5px 5px 5px 220px;text-align:center" >Pricing</h1>
                              
                                    </div>
                                    <div class="col-sm-2">
                                                <select v-if="addorEditFunction=='edit'"  @change="selectProfile2" v-model="serviceProfile" class="form-control m-b pull-right">
                                                    <option :value="0">Regular Rate</option>
                                                    <option v-for="p in profiles" :value="p.id">@{{p.name}}</option>
                                                    
                                                </select>
                                    </div>
                                    
                                                
                                                <template >
                                                
                                                        <template v-for="b in addServFrm.feeArray">
                                                            <div class="col-sm-12" >
                                                                <div class="ibox float-e-margins">
                                                                    <div class="ibox-title" style="border:none">
                                                                        <h3 style="margin:0 auto;text-align:center !important;color:#696969;">@{{b.branch}}</h3>
                                                                    </div>
                                                                    <div class="ibox-content" style="border-bottom:1px dashed #e5e6e7;border-top:none">
                                                                        <div class="row" style="padding-bottom:5px">
                                                                            <div class="col-sm-2">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-2 control-label">Cost</label>
                                                                                    <div class="col-sm-10">
                                                                                     <input type="text" class="form-control" v-model="b.cost">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-3 control-label">Charge</label>
                                                                                    <div class="col-sm-9">
                                                                                     <input type="text" class="form-control" v-model="b.charge">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-2 control-label">Tip</label>
                                                                                    <div class="col-sm-10">
                                                                                     <input type="text" class="form-control" v-model="b.tip">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-6 control-label">Agent's Commission</label>
                                                                                    <div class="col-sm-6">
                                                                                     <input type="text" class="form-control" v-model="b.com_agent">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class="form-group">
                                                                                    <label style="vertical-align:middle;margin-top: 5px;padding:0px" class="col-sm-6 control-label">Client's Commission</label>
                                                                                    <div class="col-sm-6">
                                                                                     <input type="text" class="form-control" v-model="b.com_client">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>-->
                                                                       <!-- <table class="table table-bordered">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Cost</th>
                                                                                <th>Charge</th>
                                                                                <th>Tip</th>
                                                                                <th>Agent's Commission</th>
                                                                                <th>Client's Commission</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Cost</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.cost"></td>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Charge</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.charge"></td>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Tip</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.tip"></td>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Agent's Commission</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.com_agent"></td>
                                                                                <td style="width:150px;text-align:right;vertical-align: middle;"><span>Client's Commission</span></td>
                                                                                <td><input type="text" class="form-control" v-model="b.com_client"></td>
                                                                            </tr>
                                                                            
                                                                            </tbody>
                                                                        </table>-->
                                                <!--
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </template>
                                                    
                                                    </template>
                                                    <div class="col-sm-4 col-sm-offset-8 " style="margin-top:20px;">
                                                    <vue-button-spinner :is-loading="isLoading" :disabled="isLoading" :status="status" class='btn btn-success pull-right'  v-on:click.native="saveForm()" style="height:34px;margin:2px"><span style="font-size: 16px;padding: 2px 8px;">@lang('cpanel/list-of-services-page.save')</span></vue-button-spinner>
                         
                                                    <button style="margin:2px" class="btn btn-success pull-right" @click="nxtWizard('b')">Back</button>
                                                    </div>
                                                </div>
                                     </div>
                                    
                                </div>
                            </div>-->
                            <div class="wrapper wrapper-content" style="padding: 20px 10px 20px !important;" v-show="pageWizard==4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content" style="border-style:none !important">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="control-label">@lang('cpanel/list-of-services-page.required-documents')</label>
                                                    </div>
                                                    <multiselect class="custom-select" id="req" :options="serviceDocuments" v-model="addServFrm.docs_needed" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                                                    <div class="form-group">
                                                        <label class="control-label">@lang('cpanel/list-of-services-page.optional-documents')</label>
                                                    </div>
                                                    <multiselect class="custom-select" id="opt" :options="serviceDocuments" v-model="addServFrm.docs_optional" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                                                    <div class="form-group">
                                                        <label class="control-label">@lang('cpanel/list-of-services-page.docs-released')</label>
                                                    </div>
                                                    <multiselect class="custom-select" id="released" :options="serviceDocuments" v-model="addServFrm.docs_released" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                                                    <div class="form-group">
                                                        <label class="control-label">@lang('cpanel/list-of-services-page.optional-docs-released')</label>
                                                    </div>
                                                    <multiselect class="custom-select" id="optional_released" :options="serviceDocuments" v-model="addServFrm.docs_released_optional" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                                            </div>
                                            <div class="col-sm-4 col-sm-offset-8 " style="margin-top:20px;">
                                                        <vue-button-spinner :is-loading="isLoading" :disabled="isLoading" :status="status" class='btn btn-success pull-right'  v-on:click.native="saveForm()" style="height:34px;margin:2px"><span style="font-size: 16px;padding: 2px 8px;">@lang('cpanel/list-of-services-page.save')</span></vue-button-spinner>
                            
                                                        <button style="margin:2px" class="btn btn-success pull-right" @click="showWizard(1,'')">Back</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                             <div class="wrapper wrapper-content" style="padding: 20px 10px 20px !important;" v-show="pageWizard==5">
                             <div class="ibox float-e-margins">
                                <div class="ibox-content" style="border-style:none !important">
                                    <div class="row">
                                        <div class="col-sm-12">
                                        
                                               <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>Profile Name</th>
                                                            <th>Actions</th>
                                                   
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr v-for="p in profiles2">
                                                            <td style="width:95%">
                                                                <span v-if="p.is_active==1">@{{p.name}}</span>
                                                                <span style="text-decoration: line-through;" v-else>@{{p.name}}</span>
                                                            </td>
                                                            <td style="width:5%;text-align:center">
                                                                
                                                                <a   style="margin-right:3px;" data-toggle="tooltip" title="Edit"  @click="editServiceProfile(p.id,p.name)">
                                                                    <i class="fa fa-pencil fa-1x"></i>
                                                                </a>
                                                                <a v-if="p.is_active==1" @click="disableProfile(p.id,'disable')"  style="margin-right:3px;"  data-toggle="tooltip" title="Disable" >
                                                                    <i class="fa fa-ban fa-1x"></i>
                                                                </a>
                                                                <a v-else @click="disableProfile(p.id,'active')"  style="margin-right:3px;"  data-toggle="tooltip" title="Enable" >
                                                                    <i class="fa fa-check fa-1x"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        
                                                        </tbody>
                                                    </table>
                                        </div>
                                        <div class="col-sm-2 col-sm-offset-10">
                                        <button style="margin:2px" class="btn btn-success pull-right" @click="showWizard(1,'')">Back</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div v-show="loading" class="ibox">
                            <div class="ibox-content">
                                <div class="spiner-example">
                                    <div class="sk-spinner sk-spinner-wave">
                                        <div class="sk-rect1"></div>
                                        <div class="sk-rect2"></div>
                                        <div class="sk-rect3"></div>
                                        <div class="sk-rect4"></div>
                                        <div class="sk-rect5"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

@push('scripts')
    {!! Html::script(mix('js/visa/services/index.js', 'cpanel')) !!}
@endpush


