@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style(mix('css/visa/services/index.css', 'cpanel')) !!}
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12"> 
            <h2>@lang('cpanel/documents-page.documents')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">@lang('cpanel/documents-page.home')</a>
                </li>
                <li class="active">
                    <strong>@lang('cpanel/documents-page.documents')</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated service-box" id="documents" v-cloak>
        @include('cpanel.visa.services.document-modals')
        <div class="row">
            <div class="col-lg-12">
                <a class="btn btn-primary btn-sm pull-right" @click="actionDoc('add', '')" v-show="selpermissions.addNewDocument==1 || setting == 1"><i class="fa fa-plus fa-fw"></i>@lang('cpanel/documents-page.add-new-document')</a>
                <div style="height:10px; clear:both;"></div>


                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>@lang('cpanel/documents-page.list-of-documents')</h3>
                    </div>
                    <div class="ibox-content">
                        <!-- SHOW IF DOCUMENTS ARE EMPTY -->
                        <div class="no-result" v-if="documents.length == 0">
                            <h3>@lang('cpanel/documents-page.no-service-documents-to-show')</h3>
                        </div>

                        <!-- HIDE TABLE IF EMPTY -->
                        <div class="table-responsive" v-if="documents.length != 0">
                            <table class="table table-striped table-bordered table-hover dataTables-example service-documents-table">
                                <thead>
                                    <tr>
                                        <th class="col-lg-5">@lang('cpanel/documents-page.document-name')</th>
                                        <th class="col-lg-1">@lang('cpanel/documents-page.action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      <tr v-for="docs in documents"
                                        :item="docs" 
                                        :key="docs.id"
                                        is="docs-list">
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- end table-responsive -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {!! Html::script(mix('js/visa/services/docs.js', 'cpanel')) !!}
@endpush


