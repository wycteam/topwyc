<!-- Add/Edit Service Modal -->
<modal size="modal-md" class="custom-chosen" id='addService'>
    <template slot="modal-title">
        <h3 id="title">@{{ addOrEditModalTitle }}</h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <form method="get" class="form-horizontal">
            <!-- english service detail -->
            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('detail') }">
                <label class="control-label">@lang('cpanel/list-of-services-page.service-name')</label>
                <input class="form-control m-b" id="detail" type="text" v-model="addServFrm.detail" name="detail">
                <p class="help-block" v-if="addServFrm.errors.has('detail')" v-text="addServFrm.errors.get('detail')"></p>
            </div><!-- end label-floating -->

            <!-- chinese service detail -->
            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('detail_cn') }">
                <label class="control-label">@lang('cpanel/list-of-services-page.service-name-chinese')</label>
                <input class="form-control m-b" id="detail" type="text" v-model="addServFrm.detail_cn" name="detail_cn">
                <p class="help-block" v-if="addServFrm.errors.has('detail_cn')" v-text="addServFrm.errors.get('detail_cn')"></p>
            </div><!-- end label-floating -->

            <!-- english service desc -->
            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('description') }">
                    <label class="control-label">@lang('cpanel/list-of-services-page.description')</label>
                    <textarea rows="4" style ='resize:none' class="form-control m-b" v-model="addServFrm.description" id="description" name="description"></textarea>   
                    <p class="help-block" v-if="addServFrm.errors.has('description')" v-text="addServFrm.errors.get('description')"></p>
            </div><!-- end label-floating -->
           
            <!-- chinese service desc -->
             <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('description_cn') }">
                    <label class="control-label">@lang('cpanel/list-of-services-page.description-chinese')</label>
                    <textarea rows="4" style ='resize:none' class="form-control m-b" v-model="addServFrm.description_cn" id="description_cn" name="description_cn"></textarea>
                    <p class="help-block" v-if="addServFrm.errors.has('description_cn')" v-text="addServFrm.errors.get('description_cn')"></p>
            </div><!-- end label-floating -->

            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('type') }">
                    <label class="control-label">@lang('cpanel/list-of-services-page.service-type')</label>
                    <select class="form-control m-b" v-model="addServFrm.type" id="type" @change="changeServiceType()" name="type">
                        <option value='Parent'>
                            @lang('cpanel/list-of-services-page.parent')
                        </option>
                        <option value='Child'>
                            @lang('cpanel/list-of-services-page.child')
                        </option>
                    </select>
                    <p class="help-block" v-if="addServFrm.errors.has('type')" v-text="addServFrm.errors.get('type')"></p>
            </div><!-- end label-floating -->
           

            <!-- SHOW ONLY IF CHILD WAS CHOSEN AS TYPE -->
            <div v-if="showChildOptions">
                <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('parent_id') }">
                    <label class="control-label">@lang('cpanel/list-of-services-page.service-parent')</label>
                    <select class="form-control m-b" v-model="addServFrm.parent_id" id="parent_id" name="parent_id" >
                        <option v-for="option in serviceParents" v-bind:value="option.id" v-if="activeId != option.id">
                            <p>@{{ option.detail }}</p>
                        </option>
                    </select>
                      <p class="help-block" v-if="addServFrm.errors.has('parent_id')" v-text="addServFrm.errors.get('parent_id')"></p>
                </div><!-- end label-floating -->

                <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('cost') }">
                    <label class="control-label">@lang('cpanel/list-of-services-page.cost')</label>
                      <!-- <input class="form-control m-b" placeholder="Service Charge"  id="cost" type="number" v-model="addServFrm.cost" name="cost">
                      <p class="help-block" v-if="addServFrm.errors.has('cost')" v-text="addServFrm.errors.get('cost')"></p> -->

                    <cost-component ref="costcomponent" :branchcosts="branchCosts"></cost-component>
                </div><!-- end label-floating -->

                <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('charge') }">
                    <label class="control-label">@lang('cpanel/list-of-services-page.charge')</label>
                      <!-- <input class="form-control m-b" placeholder="Service Charge"  id="charge" type="number" v-model="addServFrm.charge" name="charge">
                      <p class="help-block" v-if="addServFrm.errors.has('charge')" v-text="addServFrm.errors.get('charge')"></p> -->

                    <charge-component ref="chargecomponent" :branchcosts="branchCosts"></charge-component>
                </div><!-- end label-floating -->

                <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('tip') }">
                    <label class="control-label">@lang('cpanel/list-of-services-page.tip')</label>
                      <!-- <input class="form-control m-b" placeholder="Service Charge"  id="tip" type="number" v-model="addServFrm.tip" name="tip">
                      <p class="help-block" v-if="addServFrm.errors.has('tip')" v-text="addServFrm.errors.get('tip')"></p> -->

                    <tip-component ref="tipcomponent" :branchcosts="branchCosts"></tip-component>
                </div><!-- end label-floating -->


                <!-- commissionable amt -->
                 <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('commissionable_amt') }">
                        <label class="control-label">@lang('cpanel/list-of-services-page.com-amt')</label>
                        <input class="form-control m-b" id="commissionable_amt" type="number" v-model="addServFrm.commissionable_amt" name="commissionable_amt">
                        <p class="help-block" v-if="addServFrm.errors.has('commissionable_amt')" v-text="addServFrm.errors.get('commissionable_amt')"></p>
                </div><!-- end label-floating -->

                <!-- insert required documents here -->
                <div class="form-group">
                    <label class="control-label">@lang('cpanel/list-of-services-page.required-documents')</label>
                </div>
                <multiselect class="custom-select" id="req" :options="serviceDocuments" v-model="addServFrm.docs_needed" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>
                
                <div class="form-group">
                    <label class="control-label">@lang('cpanel/list-of-services-page.optional-documents')</label>
                </div>
                <multiselect class="custom-select" id="opt" :options="serviceDocuments" v-model="addServFrm.docs_optional" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                <div class="form-group">
                    <label class="control-label">@lang('cpanel/list-of-services-page.docs-released')</label>
                </div>
                <multiselect class="custom-select" id="released" :options="serviceDocuments" v-model="addServFrm.docs_released" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                <div class="form-group">
                    <label class="control-label">@lang('cpanel/list-of-services-page.optional-docs-released')</label>
                </div>
                <multiselect class="custom-select" id="optional_released" :options="serviceDocuments" v-model="addServFrm.docs_released_optional" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                <!-- 9A/9G/TRV/CEZA -->
                <div v-if="addServFrm.parent_id == 63 || addServFrm.parent_id == 70 || addServFrm.parent_id == 148 || addServFrm.parent_id == 238" class="form-group">
                    <label class="control-label">@lang('cpanel/list-of-services-page.months-required')</label>
                    <input class="form-control m-b" id="months_required" type="number" min="0" v-model="addServFrm.months_required" name="months_required">
                </div>
            </div>

        </form>
	</template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal" @click="commonFunctionReload()">@lang('cpanel/list-of-services-page.cancel')</button>
        <button class='btn btn-primary'  @click="saveForm()">@lang('cpanel/list-of-services-page.save')</button>
    </template>
</modal>


<!-- Add/Edit Service Modal -->
<modal size="modal-lg" class="custom-chosen" id='editService'>
    <template slot="modal-title">
        <h3 id="title">Edit Service</h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <form method="get" class="form-horizontal">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-info"> Info</a></li>
                    <li><a data-toggle="tab" href="#tab-pricing" v-if="showChildOptions">Pricing</a></li>
                    <li><a data-toggle="tab" href="#tab-docs" v-if="showChildOptions">Docs</a></li>
                </ul>
            <div class="tab-content">
                <div id="tab-info" class="tab-pane active">
                    <div class="panel-body">
                        <div v-show="!loadingmodal">
                        <!-- english service detail -->
                        <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('detail') }">
                            <label class="control-label">@lang('cpanel/list-of-services-page.service-name')</label>
                            <input class="form-control m-b" id="detail" type="text" v-model="addServFrm.detail" name="detail">
                            <p class="help-block" v-if="addServFrm.errors.has('detail')" v-text="addServFrm.errors.get('detail')"></p>
                        </div><!-- end label-floating -->

                        <!-- english service desc -->
                        <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('description') }">
                                <label class="control-label">@lang('cpanel/list-of-services-page.description')</label>
                                <textarea rows="2" style ='resize:none' class="form-control m-b" v-model="addServFrm.description" id="description" name="description"></textarea>   
                                <p class="help-block" v-if="addServFrm.errors.has('description')" v-text="addServFrm.errors.get('description')"></p>
                        </div><!-- end label-floating -->

                        <!-- chinese service detail -->
                        <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('detail_cn') }">
                            <label class="control-label">@lang('cpanel/list-of-services-page.service-name-chinese')</label>
                            <input class="form-control m-b" id="detail" type="text" v-model="addServFrm.detail_cn" name="detail_cn">
                            <p class="help-block" v-if="addServFrm.errors.has('detail_cn')" v-text="addServFrm.errors.get('detail_cn')"></p>
                        </div><!-- end label-floating -->
                       
                        <!-- chinese service desc -->
                         <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('description_cn') }">
                                <label class="control-label">@lang('cpanel/list-of-services-page.description-chinese')</label>
                                <textarea rows="2" style ='resize:none' class="form-control m-b" v-model="addServFrm.description_cn" id="description_cn" name="description_cn"></textarea>
                                <p class="help-block" v-if="addServFrm.errors.has('description_cn')" v-text="addServFrm.errors.get('description_cn')"></p>
                        </div><!-- end label-floating -->
                    <div class="row">
                        <template v-if="addorEditFunction == 'add'">
                            <div class="form-group col-lg-4" :class="{ 'has-error': addServFrm.errors.has('type') }" style="margin-right: 10px">
                                    <label class="control-label">@lang('cpanel/list-of-services-page.service-type')</label>
                                    <select class="form-control m-b" v-model="addServFrm.type" id="type" @change="changeServiceType()" name="type">
                                        <option value='Parent'>
                                            @lang('cpanel/list-of-services-page.parent')
                                        </option>
                                        <option value='Child'>
                                            @lang('cpanel/list-of-services-page.child')
                                        </option>
                                    </select>
                                    <p class="help-block" v-if="addServFrm.errors.has('type')" v-text="addServFrm.errors.get('type')"></p>
                            </div><!-- end label-floating -->
                        </template>

                        <div class="form-group col-lg-5" :class="{ 'has-error': addServFrm.errors.has('parent_id') }" v-if="showChildOptions">
                            <label class="control-label">@lang('cpanel/list-of-services-page.service-parent')</label>
                            <select class="form-control m-b" v-model="addServFrm.parent_id" id="parent_id" name="parent_id" >
                                <option v-for="option in serviceParents" v-bind:value="option.id" v-if="activeId != option.id">
                                    <p>@{{ option.detail }}</p>
                                </option>
                            </select>
                              <p class="help-block" v-if="addServFrm.errors.has('parent_id')" v-text="addServFrm.errors.get('parent_id')"></p>
                        </div><!-- end label-floating -->

                        <!-- 9A/9G/TRV/CEZA -->
                        <template v-if="addorEditFunction == 'add'">
                            <div v-if="addServFrm.parent_id == 63 || addServFrm.parent_id == 70 || addServFrm.parent_id == 148 || addServFrm.parent_id == 238" class="form-group col-lg-3"  style="margin-left: 10px">
                                <label class="control-label">@lang('cpanel/list-of-services-page.months-required')</label>
                                <input class="form-control m-b" id="months_required" type="number" min="0" v-model="addServFrm.months_required" name="months_required">
                            </div>
                        </template>
                    </div>
                        </div>

                            <div v-show="loadingmodal" class="ibox">
                                <div class="ibox-content">
                                    <div class="spiner-example">
                                        <div class="sk-spinner sk-spinner-wave">
                                            <div class="sk-rect1"></div>
                                            <div class="sk-rect2"></div>
                                            <div class="sk-rect3"></div>
                                            <div class="sk-rect4"></div>
                                            <div class="sk-rect5"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
  
                    </div>
                </div>
                <div id="tab-pricing" class="tab-pane" v-if="showChildOptions">
                    <div class="panel-body">

                        <div class="ibox float-e-margins">
                            <div class="ibox-title" style="border-color: #fff;">
                                <div class="ibox-tools">
                                    <b>@{{ profileCost.name | toTitleCase }} </b>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #18a689;" v-show="addServFrm.id > 0">
                                        <i class="fa fa-wrench" style="margin-left: 5px;"></i> Change
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li class="prof active"><a href="#" p-id='0'>Regular Rates</a>
                                        </li>
                                        <li class="prof" v-for="p in profiles">
                                            <a href="#" :p-id="p.id">@{{ p.name | toTitleCase }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div v-show="profileCost.profile_id < 1">@{{profileCost.profile_id}}
                            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('cost') }" >
                                <label class="control-label">@lang('cpanel/list-of-services-page.cost')</label>
                                <cost-component ref="costcomponent" :branchcosts="branchCosts"></cost-component>
                            </div><!-- end label-floating -->

                            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('charge') }">
                                <label class="control-label">@lang('cpanel/list-of-services-page.charge')</label>
                                <charge-component ref="chargecomponent" :branchcosts="branchCosts"></charge-component>
                            </div><!-- end label-floating -->

                            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('tip') }">
                                <label class="control-label">@lang('cpanel/list-of-services-page.tip')</label>
                                <tip-component ref="tipcomponent" :branchcosts="branchCosts"></tip-component>
                            </div><!-- end label-floating -->

                            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('com_agent') }">
                                <label class="control-label">Agent's Commission</label>
                                <comagent-component ref="comagentcomponent" :branchcosts="branchCosts"></comagent-component>
                            </div><!-- end label-floating -->

                            <div class="form-group" :class="{ 'has-error': addServFrm.errors.has('com_client') }">
                                <label class="control-label">Client's Commission</label>
                                <comclient-component ref="comclientcomponent" :branchcosts="branchCosts"></comclient-component>
                            </div><!-- end label-floating -->
                        </div>

                        <template v-if="profileCost.profile_id != ''">
                            <div class="form-group" >
                                <label class="control-label">@lang('cpanel/list-of-services-page.cost')</label>
                                <cost-component ref="costcomponent2" :branchcosts="profileCosts"></cost-component>
                            </div><!-- end label-floating -->

                            <div class="form-group" >
                                <label class="control-label">@lang('cpanel/list-of-services-page.charge')</label>
                                <charge-component ref="chargecomponent2" :branchcosts="profileCosts"></charge-component>
                            </div><!-- end label-floating -->

                            <div class="form-group" >
                                <label class="control-label">@lang('cpanel/list-of-services-page.tip')</label>
                                <tip-component ref="tipcomponent2" :branchcosts="profileCosts"></tip-component>
                            </div><!-- end label-floating -->

                            <div class="form-group" >
                                <label class="control-label">Agent's Commission</label>
                                <comagent-component ref="comagentcomponent2" :branchcosts="profileCosts"></comagent-component>
                            </div><!-- end label-floating -->

                            <div class="form-group" >
                                <label class="control-label">Client's Commission</label>
                                <comclient-component ref="comclientcomponent2" :branchcosts="profileCosts"></comclient-component>
                            </div><!-- end label-floating -->
                        </template>

<!--                         <div class="row">    
                            <div class="form-group col-lg-4" :class="{ 'has-error': addServFrm.errors.has('com_agent') }">
                                <label class="control-label">Agent's Commission</label>
                                <input class="form-control m-b" id="com_agent" type="text" v-model="addServFrm.com_agent" name="com_agent">
                                <p class="help-block" v-if="addServFrm.errors.has('com_agent')" v-text="addServFrm.errors.get('com_agent')"></p>
                            </div>

                            <div class="form-group col-lg-4" :class="{ 'has-error': addServFrm.errors.has('com_client') }" style="margin-left: 10px;">
                                <label class="control-label">Client's Commission</label>
                                <input class="form-control m-b" id="com_client" type="text" v-model="addServFrm.com_client" name="com_client">
                                <p class="help-block" v-if="addServFrm.errors.has('com_client')" v-text="addServFrm.errors.get('com_client')"></p>
                            </div>
                        </div> -->
                        
                    </div>
                </div>
                <div id="tab-docs" class="tab-pane" v-if="showChildOptions">
                    <div class="panel-body">

                        <!-- insert required documents here -->
                        <div class="form-group">
                            <label class="control-label">@lang('cpanel/list-of-services-page.required-documents')</label>
                        </div>
                        <multiselect class="custom-select" id="req" :options="serviceDocuments" v-model="addServFrm.docs_needed" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>
                        
                        <div class="form-group">
                            <label class="control-label">@lang('cpanel/list-of-services-page.optional-documents')</label>
                        </div>
                        <multiselect class="custom-select" id="opt" :options="serviceDocuments" v-model="addServFrm.docs_optional" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                        <div class="form-group">
                            <label class="control-label">@lang('cpanel/list-of-services-page.docs-released')</label>
                        </div>
                        <multiselect class="custom-select" id="released" :options="serviceDocuments" v-model="addServFrm.docs_released" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                        <div class="form-group">
                            <label class="control-label">@lang('cpanel/list-of-services-page.optional-docs-released')</label>
                        </div>
                        <multiselect class="custom-select" id="optional_released" :options="serviceDocuments" v-model="addServFrm.docs_released_optional" :multiple="true" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" label="title" track-by="id"></multiselect>

                    </div>
                </div>
            </div>
        </div>

        </form>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal" >@lang('cpanel/list-of-services-page.cancel')</button>
        <!-- <button class='btn btn-primary'  @click="saveForm()">@lang('cpanel/list-of-services-page.save')</button> -->
        <vue-button-spinner :is-loading="isLoading" :disabled="isLoading" :status="status" class='btn btn-primary'  v-on:click.native="saveForm()" style="height:   34px;"><span style="font-size: 16px;padding: 2px 8px;">@lang('cpanel/list-of-services-page.save')</span></vue-button-spinner>
    </template>
</modal>


<modal size="modal-sm" id='addProfile'>
    <template slot="modal-title">
        <h3 id="title"></h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <form method="get" class="form-horizontal">
            <!-- english service detail -->
            <div class="form-group" :class="{ 'has-error': addProfFrm.errors.has('name') }">
                <label class="control-label">@lang('cpanel/service-profile-page.profile-name')</label>
                <input class="form-control m-b" id="name" type="text" v-model="addProfFrm.name" name="name" required>
                <input  type="hidden" v-model="addProfFrm.id" name="id">
                <p class="help-block" v-if="addProfFrm.errors.has('name')" v-text="addProfFrm.errors.get('name')"></p>
            </div><!-- end label-floating -->

            <div class="form-group">
                <label class="col-md-3 control-label">
                    @lang('cpanel/service-profile-page.status'):
                </label>
                <div class="col-md-9">
                    <div style="height:5px;clear:both;"></div>
                    <label class=""> <input type="radio" value="0" name="active" v-model="addProfFrm.is_active"> <i></i> @lang('cpanel/service-profile-page.disabled') </label>

                    <label class="m-l-2"> <input type="radio" value="1" name="active" v-model="addProfFrm.is_active"> <i></i> @lang('cpanel/service-profile-page.enabled') </label>
                </div>
            </div>
        </form>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal" @click="emptyform()">@lang('cpanel/list-of-services-page.cancel')</button>
        <button class='btn btn-primary'  @click="saveProfileForm()">@lang('cpanel/list-of-services-page.save')</button>
    </template>
</modal>

<modal size="modal-sm" id='editProfile'>
    <template slot="modal-title">
        <h3 id="title"></h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <form method="get" class="form-horizontal">
            <!-- english service detail -->
                <label class="control-label">@lang('cpanel/service-profile-page.profile-name')</label>
                <input class="form-control" type="text" v-model="profile_name" >
            </div><!-- end label-floating -->
        </form>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">@lang('cpanel/list-of-services-page.cancel')</button>
        <button class='btn btn-primary'  @click="updateProfile">@lang('cpanel/list-of-services-page.save')</button>
    </template>
</modal>

<dialog-modal size="modal-sm" id='dialogRemoveService'>
	<template slot="modal-title">
       @lang('cpanel/list-of-services-page.delete-service')
    </template>
    <template slot="modal-body" class="text-center">
        @lang('cpanel/list-of-services-page.are-you-sure-you-want-to-delete-this-service')
        <br>
        @lang('cpanel/list-of-services-page.once-deleted-it-will-be-irreversible')
    </template>
    <template slot="modal-footer">
        <button class='btn btn-primary'  data-dismiss="modal" @click="deleteService()">@lang('cpanel/list-of-services-page.yes')</button>
        <button class='btn btn-default'  data-dismiss="modal">@lang('cpanel/list-of-services-page.cancel')</button>
    </template>
</dialog-modal>

<modal size="modal-md" id='serviceDetailModal'>
    <template slot="modal-title">
        <h3 id="title">Service Detail</h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">

        <table class="table table-bordered" v-if="selectedService">
            <thead>
                <tr>
                    <th colspan="2" class="text-center text-bold">@{{ selectedService.detail }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width:30%"><strong>Service Name (Chinese)</strong></td>
                    <td style="width:70%">@{{ selectedService.detail_cn }}</td>
                </tr>
                <tr>
                    <td><strong>Description</strong></td>
                    <td>@{{ selectedService.description }}</td>
                </tr>
                <tr>
                    <td><strong>Description (Chinese)</strong></td>
                    <td>@{{ selectedService.description_cn }}</td>
                </tr>
                <tr>
                    <td><strong>Service Type</strong></td>
                    <td>@{{ (selectedService.parent_id == 0) ? 'Parent' : 'Child' }}</td>
                </tr>
                    <tr v-if="selectedService.parent_id != 0">
                        <td><strong>Service Parent</strong></td>
                        <td>@{{ selectedService.parent.detail }}</td>
                    </tr>
                    <tr v-if="selectedService.parent_id != 0">
                        <td><strong>Cost</strong></td>
                        <td>
                            <table class="table table-bordered">
                                <tr v-for="bc in selectedService.branch_costs">
                                    <td style="width:40%"><i>@{{ bc.branch }}</i></td>
                                    <td style="width:60%">@{{ bc.cost }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr v-if="selectedService.parent_id != 0">
                        <td><strong>Charge</strong></td>
                        <td>
                            <table class="table table-bordered">
                                <tr v-for="bc in selectedService.branch_costs">
                                    <td style="width:40%"><i>@{{ bc.branch }}</i></td>
                                    <td style="width:60%">@{{ bc.charge }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr v-if="selectedService.parent_id != 0">
                        <td><strong>Tip</strong></td>
                        <td>
                            <table class="table table-bordered">
                                <tr v-for="bc in selectedService.branch_costs">
                                    <td style="width:40%"><i>@{{ bc.branch }}</i></td>
                                    <td style="width:60%">@{{ bc.tip }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr v-if="selectedService.parent_id != 0">
                        <td><strong>Required Documents</strong></td>
                        <td>
                            <ul>
                                <li v-for="docs in selectedService.select_docs_needed">
                                    @{{ docs[0].title }}
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr v-if="selectedService.parent_id != 0">
                        <td><strong>Optional Documents</strong></td>
                        <td>
                            <ul>
                                <li v-for="docs in selectedService.select_docs_optional">
                                    @{{ docs[0].title }}
                                </li>
                            </ul>
                        </td>
                    </tr>
            </tbody>
        </table>

    </template>
    <template slot="modal-footer">
        <button class='btn btn-default' data-dismiss="modal">Close</button>
    </template>
</modal>
