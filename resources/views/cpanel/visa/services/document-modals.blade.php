<dialog-modal size="modal-sm" id='dialogRemoveDocument'>
	<template slot="modal-title">
       @lang('cpanel/documents-page.delete-service-document')
    </template>
    <template slot="modal-body" class="text-center">
        @lang('cpanel/documents-page.are-you-sure-you-want-to-delete-this-service-document')
        <br>
        @lang('cpanel/documents-page.once-deleted-it-will-be-irreversible')
    </template>
    <template slot="modal-footer">
        <button class='btn btn-primary' data-dismiss="modal" @click="saveDocs(_action)">@lang('cpanel/documents-page.yes')</button>
        <button class='btn btn-default' data-dismiss="modal">@lang('cpanel/documents-page.cancel')</button>
    </template>
</dialog-modal>

<dialog-modal size="modal-md" id='dialogAddDocument'>
    <template slot="modal-title">
       @{{ act }} @lang('cpanel/documents-page.service-document')
    </template>
    <template slot="modal-body" class="text-center">
       <!-- english service detail -->
        <div class="form-group" :class="{ 'has-error': docsFrm.errors.has('title') }">
            <label class="control-label">@lang('cpanel/documents-page.title')</label>
            <input class="form-control m-b" id="title" type="text" v-model="docsFrm.title" name="title">
            <p class="help-block" v-if="docsFrm.errors.has('title')" v-text="docsFrm.errors.get('title')"></p>
        </div><!-- end label-floating -->

        <!-- chinese service detail -->
        <div class="form-group">
            <label class="control-label">@lang('cpanel/documents-page.title-chinese')</label>
            <input class="form-control m-b" id="title_cn" type="text" v-model="docsFrm.title_cn" name="title_cn">
        </div><!-- end label-floating -->

        <div class="form-group">
            <label class="control-label">Unique</label>

            <div>
                <label class="radio-inline">
                    <input type="radio" value="1" name="is_unique" v-model="docsFrm.is_unique">
                    Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" value="0" name="is_unique" v-model="docsFrm.is_unique">
                    No
                </label>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-primary'  @click="saveDocs(_action)">@lang('cpanel/documents-page.save')</button>
        <button class='btn btn-default'  @click="reloadAll" data-dismiss="modal">@lang('cpanel/documents-page.cancel')</button>
    </template>
</dialog-modal>