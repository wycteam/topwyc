@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style(mix('css/visa/services/index.css', 'cpanel')) !!}

    <style>
        #fixTable-wrapper {
            overflow-x: scroll;
        }
        #fixTable {
            /*width: 600px !important;*/
        }
        #fixTable tr th, #fixTable tr td {
            min-width: 200px;
            vertical-align: middle;
            padding: 10px;
        }
        #fixTable tr th:first-child, #fixTable tr td:first-child {
            min-width: 400px;
            vertical-align: middle;
            padding: 10px;
        }
        #fixTable tr th + th,  #fixTable tr td + td {
           text-align: center;
        }
        .parent-name {
            background-color: #1ab394;
        }
        .parent-name a, .parent-name a:hover {
            color: #ffffff;
        }
        .child-indent {
            padding-left: 15px;
        }
        .child-name {
            background-color: #86c1b9;
        }
        .child-name a, .child-name a:hover {
            color: #ffffff;
        }
        #addProfile .modal-content{
        	width: 300px;
        	margin-left: auto;
			margin-right: auto;
        }
        .strCost, .strProf{
        	cursor: pointer;
        }

        input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button { 
		  -webkit-appearance: none; 
		  margin: 0; 
		}
    </style>

@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/service-profile-page.service-profiles')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">@lang('cpanel/service-profile-page.home')</a>
                </li>
                <li class="active">
                    <strong>@lang('cpanel/service-profile-page.service-profiles')</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated service-box" id="service_profiles">
        @include('cpanel.visa.services.profile-modals')
        <div class="row">
            <div class="col-lg-12">
                <a class="btn btn-primary btn-sm pull-right" @click="addServiceProfile()" ><i class="fa fa-plus fa-fw"></i>@lang('cpanel/service-profile-page.add-service-profile')</a>
                <div class="panel-body no-side-padding" style="min-height: 600px;">

	    <div id="attendance_monitoring" class="wrapper wrapper-content animated">
	        <div class="row">
	            <div class="col-lg-12">
	                <div style="height:10px; clear:both;"></div>

	                <div v-show="!loading" class="ibox float-e-margins">
	                    <div class="ibox-content">
	                        <div class="row">
	                            <div class="col-md-6">
	                                <form class="form-inline">
	                                    <div class="form-group">
	                                        <label> @lang('cpanel/service-profile-page.service-name'):</label>
	                                        <input type="text" class="form-control" v-model="search" @keyup="refreshTableHeadFixer">
	                                    </div>
	                                </form>
	                            </div>
	                        </div>

	                        <div style="height:10px;clear:both;"></div>

	                        <div id="fixTable-wrapper">
	                            <table id="fixTable" class="table table-bordered table-striped">
	                                <thead>
	                                    <tr>
	                                        <th>@lang('cpanel/service-profile-page.service')</th>
	                                        <th>@lang('cpanel/service-profile-page.regular')</th>
	                                        <th v-for="p in profiles">
	                                            <strong @click="editServiceProfile(p.id,p.name,p.is_active)" class="strProf" data-toggle="popover" data-placement="top" data-container="body" data-content="click to edit profile" data-trigger="hover">@{{ p.name }}</strong>
	                                        </th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                    <tr v-for="serv in filteredServices">
	                                        <td class="parent-name" :class="{'child-name' : serv.parent_id > '0'}">
	                                            <a>
	                                                <strong :class="{'child-indent' : serv.parent_id > '0'}">@{{ serv.detail }}</strong>
	                                            </a>
	                                        </td>
	                                        <td >
	                                                <strong><a href="#" data-toggle="popover" data-placement="left" data-container="body" :data-content="'<b>Charge : </b>'+ serv.charge + '</br><b>Tip :</b>'+ serv.tip + '</br><b>Cost : </b>'+ serv.cost" data-trigger="hover">@{{ parseFloat(serv.charge) + parseFloat(serv.tip) + parseFloat(serv.cost) }}</a></strong>
	                                        </td>
											<td v-for="pc in serv.costs" @click="editServiceCost(pc.id,serv.detail, pc.charge, pc.tip, pc.cost)">
												<!-- <span :p-id="pc.profile_id" :s-id="pc.service_id" @dblclick="editProfileCost(pc.cost,pc.profile_id,pc.service_id)" style="padding: 10px 0px; "> -->
												<span :p-id="pc.profile_id" :s-id="pc.service_id" style="padding: 10px 0px; ">
	                                                <strong :id="'str_'+pc.service_id+'_'+pc.profile_id" class="strCost" data-toggle="popover" data-placement="top" data-container="body" data-content="click to edit" data-trigger="hover">@{{ parseFloat(pc.cost) + parseFloat(pc.tip) +  parseFloat(pc.charge)}}</strong>
	                                                <center><input type="number" :id="'pc_'+pc.service_id+'_'+pc.profile_id" class="inputCost" style="outline: none; border: 0; border-bottom: 1px solid #ccc;text-align: center; " @blur="updateProfileCost" @keyup.enter="updateProfileCost"></center>
	                                            </span>
	                                        </td>
	                                    </tr>
	                                </tbody>
	                            </table>
	                        </div>

	                    </div>
	                </div>

	                <div v-show="loading" class="ibox">
	                    <div class="ibox-content">
	                        <div class="spiner-example">
	                            <div class="sk-spinner sk-spinner-wave">
	                                <div class="sk-rect1"></div>
	                                <div class="sk-rect2"></div>
	                                <div class="sk-rect3"></div>
	                                <div class="sk-rect4"></div>
	                                <div class="sk-rect5"></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>

	            </div>
	        </div>
	    </div>

	</div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {!! Html::script('cpanel/plugins/tableHeadFixer/tableHeadFixer.js') !!}
    {!! Html::script(mix('js/visa/services/profiles.js', 'cpanel')) !!}
@endpush


