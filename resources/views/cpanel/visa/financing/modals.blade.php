<dialog-modal id='dialogPermissions' size="modal-lg">
	<template slot="modal-title">
        @{{ rolename }} - Permissions
    </template>
    <template slot="modal-body">
        <div>
            <form method="POST" id="form">
                <template v-for="data in permissiontype">
                    <div class="ibox collapsed panel panel-info" style="border-color: #23c6c8 !important;">
                        <div class="ibox-title panel-heading">
                            <h5>@{{ data.type }}</h5>
                            <div class="ibox-tools">
                                <a @click="toggle(data.id, $event)" data-toggle="collapse" :href="'#content'+data.id" aria-expanded="false" aria-controls="content" class="toggle-row">
                                    <i :id="'fa-'+data.id" class="fa fa-arrow-up"></i>
                                </a>
                            </div>
                        </div>
                        <div :id="'content'+data.id" class="collapse ibox-content-permission">
                            <p>
                                <span v-for="(data2,index) in permissions">
                                    <span v-if="data2.type==data.type">
                                    <span v-if="check(data2)==1" ><input type="checkbox" :value="data2.id" @click="selectItem(data2,index)" checked></span>
                                    <span v-else><input type="checkbox" :value="data2.id" @click="selectItem(data2,index)"></span> @{{data2.name}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                                    </span>    
                                </span>
                            </p>
                        </div>
                    </div>
                </template>
            </form>
        </div>

    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary' @click="saveAccessControl">Save</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogRole' size="modal-md">
    <template slot="modal-title">
        Add New Role
    </template>
    <template slot="modal-body">

        <form class="form-horizontal">
                    
            <div class="form-group" :class="{ 'has-error': roleForm.errors.has('role') }">
                <label for="role" class="col-sm-3 control-label">Role:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="role" v-model="roleForm.role">
                    <span class="help-block" v-if="roleForm.errors.has('role')" v-text="roleForm.errors.get('role')"></span>
                </div>
            </div>
        </form>

    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary' @click="addRole">Save</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogEditRole' size="modal-md">
    <template slot="modal-title">
        Edit Role
    </template>
    <template slot="modal-body">
                    
        <div class="form-group" :class="{ 'has-error': roleForm.errors.has('role') }">
            <label for="role" class="col-sm-3 control-label">Role:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="role" v-model="roleForm.role">
                <span class="help-block" v-if="roleForm.errors.has('role')" v-text="roleForm.errors.get('role')"></span>
            </div>
        </div>

    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary' @click="saveRole">Save</button>
    </template>
</dialog-modal>