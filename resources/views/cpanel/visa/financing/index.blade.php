@extends('cpanel.layouts.master')

@section('title', 'Financial Monitor')

@push('styles')
    {!! Html::style('cpanel/plugins/jexcel/css/jquery.jexcel.css') !!}
    {!! Html::style('cpanel/plugins/jexcel/css/jquery.jcalendar.css') !!}
@endpush

@section('content')
    <div id="access-control">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Financial Monitor</h2>
            <a href="javascript:void(0)" @click="showRoleDialog" class="m-l-3 p-l-1"><i class="fa fa-user-plus fa-fw" ></i> Add Row</a>
        </div>
    </div>

    <div id="access-control" class="wrapper wrapper-content">
        @include('cpanel.visa.financing.modals')
        <div class="row">
            <div class="col-lg-12">

                <div style="height:10px; clear:both;"></div>

                <div class="wrapper wrapper-content" id="userprofile" v-cloak>
                    <div class="row">
                        <div class="col-sm-12 no-side-padding fff-bg">
                            <div class="col-sm-7 no-side-padding">
                                <div class="ibox no-b-margin">
                                    <div class="ibox-content no-tb-padding">
                                        <div class="tab-content">
                                            <div id="contact-1" class="tab-pane active">
                                                <div class="full-height-scroll details-size">

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Process Output :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals"> <?php echo ($stats[17] == '' ) ? 0:$stats[17]; ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>How much we received from client :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals"> <?php echo ($stats[18] == '' ) ? 0:$stats[18]; ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>How much we received after we spent for client :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals"> <?php echo ($stats[19] == '' ) ? 0:$stats[18]; ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Admin Total Cost :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" ><?php echo ($stats[19] == '' ) ? 0:$stats[18]; ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 no-side-padding">
                                <div class="ibox no-b-margin">
                                    <div class="ibox-content no-tb-padding">
                                        <div class="tab-content">
                                            <div id="contact-1" class="tab-pane active">
                                                <div class="full-height-scroll details-size">

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Initial Cash :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals"> <?php echo ($ini['allcashInitial'] == '' ) ? 0:$ini['allcashInitial']; ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Initial Bank :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals"> <?php echo ($ini['allbankInitial'] == '' ) ? 0:$ini['allbankInitial']; ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Cash Balance :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals"> <?php echo ($stats[21] == '' ) ? 0:$stats[21]; ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Bank Balance :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" ><?php echo ($stats[22] == '' ) ? 0:$stats[22]; ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Total :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" ><?php echo ($stats[23] == '' ) ? 0:$stats[23]; ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Profit :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" ><?php echo ($stats[24] == '' ) ? 0:$stats[24]; ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        
                    <div class="chart tab-pane" style="position: relative; height:96% !important;margin-left:10px !important;">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active" style="width:80px"><a href="#view1" data-toggle="tab"><strong style="margin-left:15px">All</strong></a></li>
                                <li><a href="#view2" data-toggle="tab"><strong>Jason</strong></a></li>
                                <li><a href="#view3" data-toggle="tab"><strong>Jerry</strong></a></li>
<!--                                 <li><a href="#view4" data-toggle="tab"><strong>Pending</strong></a></li>
                                <li><a href="#view5" data-toggle="tab"><strong>Processing</strong></a></li>
                                <li><a href="#view6" data-toggle="tab"><strong>Action Log</strong></a></li> -->
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="active tab-pane" id="view1">
                                <div class="container" style="width:100% !important;">        
                                    <div class="table-responsive">
                                        <div id="finance"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="view2">
                                <div id="jasonTable" style="overflow-y: auto; height:100%;"></div>
                            </div><!--end tab-pane-->

                            <div class="tab-pane" id="view3">
                                 <div id="jerryTable" style="overflow-y: auto; height:100%;"></div>
                            </div><!--end tab-pane-->
                        </div>
                    </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>   

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.71/jquery.csv-0.71.min.js"></script>
    {!! Html::script('cpanel/plugins/jexcel/js/jquery.jexcel.js') !!}
    {!! Html::script('cpanel/plugins/jexcel/js/jquery.jcalendar.js') !!}
    {!! Html::script(mix('js/visa/financing/index.js','cpanel')) !!}
<script>
      $(function () {
            $('#finance').jexcel({
                csv:'/storage/data/VisaFinancingReverse.csv',
                csvHeaders:false,
                  colHeaders: [' ',' ','Date', 'Time', 'Transaction Description', 'Category Operation Type','Category Storage','CASH - Client Deposit & Payment','CASH - Client Refund','CASH - Client process budget return','CASH - Client Processing Cost','Borrowed Processing Cost','CASH - Admin budget return etc','Borrowed Admin Cost','CASH - Admin Cost','Bank - Client Deposit & Payment','Bank - Cost','<b>Process Output</b>','<b>How much we received from client</b>','<b>How much we received after we spent for client</b>','<b>Admin Total Cost</b>','<b>Cash Balance</b>','<b>Bank Balance</b>','Total','Profit','Postdated Checks'],
                  colWidths: [ 30,30,80, 80, 300, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,100 ],
                tableOverflow:true,
            });

            $('#jasonTable').jexcel({
              csv:'/storage/data/JasonReverse.csv',
              csvHeaders:false,
              colHeaders: ['Date','Time','Excel', 'Transaction Description','Credit','Debit','<b>Cash Balance</b>'],
              colWidths: [ 100,100,100,300,  150, 150, 150],
            });

            $('#jerryTable').jexcel({
              csv:'/storage/data/JerryReverse.csv',
              csvHeaders:false,
              colHeaders: ['Date','Time','Excel', 'Transaction Description','Credit','Debit','<b>Cash Balance</b>'],
              colWidths: [ 100,100,100,300,  150, 150, 150],
            });
    });
</script>
@endpush
