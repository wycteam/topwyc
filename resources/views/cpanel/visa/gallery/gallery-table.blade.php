@extends('cpanel.layouts.master')

@section('title', 'Gallery')

@push('styles')
    {!! Html::style(mix('css/visa/services/index.css', 'cpanel')) !!}
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/gallery-page.gallery')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">@lang('cpanel/gallery-page.home')</a>
                </li>
                <li class="active">
                    <strong>@lang('cpanel/gallery-page.gallery')</strong>
                </li>
            </ol>
        </div>
    </div>
    <new-list class="wrapper wrapper-content animated service-box" id="albums2">


    </new-list>

    <!--
    <album-list class="wrapper wrapper-content animated service-box" id="albums">


    </album-list>
    -->
@endsection

@push('scripts')
  {!! Html::script(mix('js/visa/gallery/album.js', 'cpanel')) !!}
@endpush
