
    <div id="dialogAddAlbum" class="modal fade" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12"><h3 class="m-t-none m-b">Add new album</h3>

                            <form role="form" action="#" method="POST">
                                <div class="form-group"><input type="text" placeholder="Title" class="form-control"></div>

                                <div class="col-sm-6 col-sm-offset-6">
                                        <button class="btn btn-primary pull-right" v-if="!edit" type="button">Save Changes</button>
                                        <button class="btn btn-primary pull-right" v-if="edit" type="button">Update Changes</button>
                                        <button class="btn btn-white pull-right" type="button">Cancel</button>
                                    </div>
                            </form>
                        </div>

                </div>
            </div>
            </div>
        </div>
</div>
