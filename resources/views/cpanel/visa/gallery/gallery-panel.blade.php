@extends('cpanel.layouts.master')
@section('title', 'Gallery')
@push('styles')
{!! Html::style('cpanel/plugins/summernote/summernote-bs3.css') !!}
{!! Html::style('cpanel/plugins/summernote/summernote.css') !!}
{!! Html::style('cpanel/plugins/dropzone/dropzone.css') !!}
@endpush
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-12">
        <h2>@lang('cpanel/gallery-detail-page.gallery')</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">@lang('cpanel/gallery-detail-page.home')</a>
            </li>
            <li>
                <a href="{{ url('/visa/gallery') }}">@lang('cpanel/gallery-detail-page.gallery')</a>
            </li>
            <li class="active">
                <strong>@lang('cpanel/gallery-detail-page.album')</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <gallery-content id="galleryContents">

    </gallery-editor>
</div>
@endsection
@push('scripts')
    {!! Html::script('cpanel/plugins/dropzone/dropzone.js') !!}
    {!! Html::script('cpanel/plugins/summernote/summernote.min.js') !!}
	{!! Html::script(mix('js/visa/gallery/gallery.js', 'cpanel')) !!}
@endpush
