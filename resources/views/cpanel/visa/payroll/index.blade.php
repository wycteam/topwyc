@extends('cpanel.layouts.master')

@section('title', 'Payroll')
@push('styles')
{!! Html::style('cpanel/plugins/footable/footable.core.css') !!}
@endpush
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Payroll Control</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li class="active">
                    <strong>Payroll</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <payroll-content id="payroll_view">

        </payroll-content>
    </div>



@endsection
@push('scripts')
    {!! Html::script(mix('js/visa/payroll/payroll.js', 'cpanel')) !!}  
    {!! Html::script('cpanel/plugins/slimscroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('cpanel/plugins/footable/footable.all.min.js') !!}
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}  
@endpush
