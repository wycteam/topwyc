@extends('cpanel.layouts.master')

@section('title', 'Payroll')
@push('styles')
{!! Html::style('cpanel/plugins/footable/footable.core.css') !!}
{!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
@endpush
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Payroll Profile</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <strong>Payroll Profile</strong>
                </li>
                <li class="active">
                    <strong>{{$full_name}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <payroll-content id="payroll_list" :payroll_list="'{{$payroll_list}}'" :user_profile="'{{$profile}}'" >

        </payroll-content>
    </div>



@endsection
@push('scripts')
    {!! Html::script(mix('js/visa/payroll/payroll_list.js', 'cpanel')) !!}  
    {!! Html::script('cpanel/plugins/datapicker/bootstrap-datepicker.js') !!}
@endpush
