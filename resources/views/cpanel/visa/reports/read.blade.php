@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
    <style>
    .popover-content {
	    /*color: black;*/
	    font-size: 12px;
	}
	.wrapok {
	    white-space:nowrap
	}
/*	
	td.wrapok {
	    white-space:normal
	}*/
	div.dataTables_wrapper {
        margin: 0 auto;
    }
/*    .dataTables_scroll
	{
	    overflow:auto;
	}*/

	.dtchildrow{
		margin-bottom: 	0px !important;
	}
</style>
@endpush

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/read-report-page.list-of-reports')</h2>
        </div>
    </div>

	<div id="read-report" class="wrapper wrapper-content animated">
        <div class="row">
        	<div class="col-lg-12">

	        	<div style="height:10px; clear:both;"></div>

	        	<div class="ibox float-e-margins">
                    <div class="ibox-content">
                    	@include('cpanel.visa.reports.tracking')
			            <div class="filter-form" v-show="selpermissions.filter==1 || setting == 1" v-cloak>
			                <form id="date-range-form" class="form-inline" @submit.prevent="submitFilter">
			                    <div class="form-group">
			                        <div class="input-daterange input-group" id="datepicker">
			                            <input type="text" class="input-sm form-control" name="start" />
			                            <span class="input-group-addon">@lang('cpanel/read-report-page.to')</span>
			                            <input type="text" class="input-sm form-control" name="end" />
			                        </div>
			                    </div>
			                    <div class="form-group m-l-2">
			                        <div class="input-group" id="client_filter">
			                            <span class="input-group-addon"><b>@lang('cpanel/read-report-page.client-id') : </b></span>

			                            <input type="text" class="input-sm form-control" name="client_id" />
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <button type="submit" class="btn btn-primary" style="line-height: 1.2; margin-top: 5px;">
			                            <strong>@lang('cpanel/read-report-page.filter')</strong>
			                        </button>
			                    </div>
			                <button id="btn-show-all-children" class="btn btn-primary" style="float:right;line-height: 1.2; margin-top: 5px;" type="button">@lang('cpanel/read-report-page.expand-all')</button>
							<button id="btn-hide-all-children" class="btn btn-primary" style="float:right;line-height: 1.2; margin-top: 5px; margin-right: 2px;" type="button">@lang('cpanel/read-report-page.collapse-all')</button>
			                </form>
			            </div>
                    	<div class="table-responsive">
		                    <table id="lists" cellspacing="5" class="dataTables_scroll table table-striped table-hover dataTables-example " style="table-layout: fixed;word-wrap:break-word;border-collapse: collapse;">
			                    <thead>
				                    <tr>
<!-- 				                        <th>Tracking No.</th> -->
				                        <!-- <th></th> -->
				                        <th>@lang('cpanel/read-report-page.service')</th>
				                       <!--  <th>Processor</th> -->
				                        <th>@lang('cpanel/read-report-page.date-time')</th>
                        				<th>@lang('cpanel/read-report-page.processor')</th>
                        				<th>@lang('cpanel/read-report-page.client-names')</th>
				                    </tr>
			                    </thead>
					                <tbody>
										<!-- <tr v-for="report in reports" v-cloak>
											<td><p v-for="client in report.client" v-cloak><a :href="'/visa/client/'+client.client_id" data-toggle="popover" data-placement="top" data-container="body" :data-content="client.fullname" data-trigger="hover"></i>@{{ client.client_id }} </a> - 
												<span  data-toggle="popover" data-placement="top" data-container="body" :data-content="client.remarks" data-trigger="hover" v-if="client.remarks"><b>@{{ client.service }}</b></span>
												<span v-else>@{{ client.service }}</span></p></td>
											<td>@{{ report.processor }}</td>
											<td>@{{ report.log_date }}</td>
											<td>@{{ report.detail }}</td>				
										</tr>	 -->		                
				                    </tbody>                  
			                </table>
			            </div>

                    </div>
                </div>

	        </div>

        </div>
    </div>

@endsection

@push('scripts')
	
    {!! Html::script('cpanel/plugins/datapicker/bootstrap-datepicker.js') !!}
	{!! Html::script(mix('js/visa/reports/read.js', 'cpanel')) !!}

@endpush