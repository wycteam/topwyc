<dialog-modal id='reports' size="modal-md">

    <template slot="modal-title">
       @lang('cpanel/write-report-page.write-report')
    </template>
    <template slot="modal-body" class="text-center">
    <form role="form" method="POST">
      <div class="form-group" :class="{ 'has-error': reportForm.errors.has('message') }">
        <div class="row">
        	<div class="col-lg-12" style="overflow:auto">      	
				<textarea rows="20" cols="80" name="message" placeholder="Enter report here..." v-model="reportForm.message"></textarea>
                <span class="help-block" v-if="reportForm.errors.has('message')" v-text="reportForm.errors.get('message')"></span>
			</div>
		</div>
      </div>
    </form>
    </template>
    <template slot="modal-footer">
        <button type="button" class='btn btn-default' data-dismiss="modal">@lang('cpanel/write-report-page.cancel')</button>
        <button type="submit" class='btn btn-primary' @click="saveReport">@lang('cpanel/write-report-page.save')</button>
    </template>


</dialog-modal>

<modal id='add-quick-report-modal' size="modal-lg">

    <template slot="modal-title">
        <h3>@lang('cpanel/write-report-page.add-quick-report')</h3>
    </template>

    <template slot="modal-body">

        <!-- <multiple-quick-report :groupreport="false" :groupid="0" :clientsid="quickReportClientsId" :clientservices="quickReportClientServicesId" :trackings="quickReportTrackings" :iswritereportpage="true" ref="multiplequickreportref"></multiple-quick-report> -->

        <multiple-quick-report-v2 ref="multiplequickreportv2ref" :groupreport="false" :groupid="0" :clientsid="quickReportClientsId" :clientservices="quickReportClientServicesId" :trackings="quickReportTrackings" :iswritereportpage="true"></multiple-quick-report-v2>

    </template>

</modal>