<dialog-modal id='tracking' size="modal-lg">

    <template slot="modal-title">
       @lang('cpanel/read-report-page.tracking-no'). @{{ trackingno }}
    </template>
    <template slot="modal-body" class="text-center">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>@lang('cpanel/read-report-page.date')</th>
                        <th>@lang('cpanel/read-report-page.service-detail')</th>
                        <th>@lang('cpanel/read-report-page.status')</th>
                        <th>@lang('cpanel/read-report-page.total-cost')</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="service in services" :key="service.id">
                        <td>@{{ service.service_date }}</td>
                        <td>@{{ service.detail }}</td>
                        <td>@{{ service.status }}</td>
                        <td>@{{ parseFloat(service.cost) + parseFloat(service.charge) + parseFloat(service.tip) | currency}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </template>

</dialog-modal>