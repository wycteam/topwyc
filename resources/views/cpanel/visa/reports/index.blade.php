@extends('cpanel.layouts.master')

@section('title', $title)

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/write-report-page.write-report')</h2>
        </div>
    </div>

	<div id="write-report" class="wrapper wrapper-content animated">
        <div class="row">
        	<div class="col-lg-12">

	        	<div style="height:10px; clear:both;"></div>

	        	<div class="ibox float-e-margins">
                    <div class="ibox-content">

                    	<div class="table-responsive">
		                    <table id="lists" class="table table-striped table-bordered table-hover dataTables-example">
			                    <thead>
				                    <tr>
				                        <th>@lang('cpanel/write-report-page.id')</th>
				                        <th>@lang('cpanel/write-report-page.name')</th>
				                        <th>@lang('cpanel/write-report-page.cost')</th>
				                        <th>@lang('cpanel/write-report-page.birthdate')</th>
                        				<th class="no-sort"></th>
				                    </tr>
			                    </thead>
					                <tbody>
				                    </tbody>			                    
			                </table>
			            </div>

						<a href="javascript:void(0)" id="selService" cl-ids="" class="pull-right" @click="addClient" v-show="selpermissions.continue==1 || setting == 1" v-cloak><b>@lang('cpanel/write-report-page.continue')</b></a>

			            <h3 style="font-weight: bold;">@lang('cpanel/write-report-page.add-reports-to-list-of-clients-below')</h3>

                    	<div class="table-responsive">
		                    <table id="reports" class="table table-striped table-bordered table-hover">
			                    <thead>
				                    <tr>
				                        <th style="width:20%">@lang('cpanel/write-report-page.id')</th>
				                        <th>@lang('cpanel/write-report-page.name')</th>
				                        <th style="width:20%">@lang('cpanel/write-report-page.action')</th>
				                    </tr>
			                    </thead>
			                    <tbody>
			                    	
			                    </tbody>
			                    
			                </table>
			            </div>

                    </div>
                </div>

	        </div>

        </div>
    </div>

<script>
	function addClient(id,name){
      var clids = $("#selService").attr("cl-ids");
      if(clids!=""){
         clids += id;
         clids += ";";
      }
      else{
        clids = id+";";
      }
      $("#selService").attr("cl-ids",clids);

      let removeText = (window.translation) ? window.translation.remove : 'Remove';

      var elem  = '<tr id="tr'+id+'">';
          elem += '<td class="text-center">'+id+'</td>';
          elem += '<td class="text-center">'+name+'</td>';
          elem += '<td class="text-center"><a href="javascript:void(0)" style="font-size:9px !important" onclick="removeClient(\''+id+'\')" class="btn btn-danger btn-xs" id="'+id+'"><span class="fa fa-cross"></span>'+removeText+'</a></td>';
          elem += '</tr>';
          $("#reports").append(elem);
          $("#add"+id).hide();
  	}
	function removeClient(id){
	          $("#tr"+id).remove();
	          $("#add"+id).show();
	          var ids = $("#selService").attr("cl-ids");
	          ids = ids.replace(id+";",'');
	          $("#selService").attr("cl-ids",ids);

	}
</script>

@endsection

@push('styles')

    {!! Html::style(mix('css/visa/reports/write-report.css', 'cpanel')) !!}
    
@endpush

@push('scripts')
	
	{!! Html::script(mix('js/visa/reports/index.js', 'cpanel')) !!}

@endpush