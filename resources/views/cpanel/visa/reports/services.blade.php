@extends('cpanel.layouts.master')

@section('title', 'Write Report')

@push('styles')
	{!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
	{!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
    {!! Html::style(mix('css/visa/reports/index.css', 'cpanel')) !!}
@endpush

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/write-report-page.write-report')</h2>
        </div>
    </div>

	<div id="write-report" class="wrapper wrapper-content animated">
        <div class="row">
        	<div class="col-lg-12">

	        	<div style="height:10px; clear:both;"></div>

	        	<div class="ibox float-e-margins">
                    <div class="ibox-content">
                    	@include('cpanel.visa.reports.modals')
		                    	<div class="table-responsive">
				                    <table id="clientlist" class="table table-striped table-bordered table-hover">
				                    	<!-- HEADER TEXTS & COLUMNS MAY VARY -->
					                    <thead>
						                    <tr>
						                        <th>@lang('cpanel/write-report-page.client-id')</th>
						                        <th>@lang('cpanel/write-report-page.name')</th>
						                        <th>@lang('cpanel/write-report-page.action')</th>
						                    </tr>
					                    </thead>
	                                    <tbody>
	                                    	<template v-for="client in clients">
	                                        <tr @click="showServices(client.id, $event)" class="clickable">
	                                       		<td>@{{ client.id }}</td>
	                                            <td>@{{ client.first_name }} @{{ client.last_name }} </td>
	                                        	<td class="text-center toggle-row">
	                                        		<i :id="'fa-'+client.id" class="fa fa-arrow-right cursor"></i>
	                                        	</td>
	                                        </tr>
	                                        <tr :id="'client-'+client.id" class="hide">
												<td colspan="5">

													<table class="table table-bordered services-tbl" >
														<thead>
															<tr>
																<td></td>
																<td>@lang('cpanel/write-report-page.date')</td>
																<td>@lang('cpanel/write-report-page.package')</td>
																<td>@lang('cpanel/write-report-page.details')</td>
																<td>@lang('cpanel/write-report-page.cost')</td>
																<td>@lang('cpanel/write-report-page.charge')</td>
																<td>@lang('cpanel/write-report-page.tip')</td>
																<td>@lang('cpanel/write-report-page.discount')</td>
																<td></td>
															</tr>
														</thead>
														<tbody>
															<tr :class="{'strikethrough': service.active == 0}" v-for="service in client.services" v-if="service.active == 1 && service.status != 'pending'">
																<td>
                                                                <i v-if="service.status.toUpperCase()=='PENDING'" class="fa fa-exclamation-circle" aria-hidden="true"></i>
																<i v-if="service.report.length!=0" class="fa fa-commenting" aria-hidden="true"></i>
																<i v-if="service.status.toUpperCase()!='PENDING' && service.report.length==0" class="fa fa-check-circle" aria-hidden="true"></i>
																</td>
																<td>@{{ service.service_date }}</td>
																<td>@{{ service.tracking }}</td>
																<td>@{{ service.detail }}</td>
																<td>@{{ service.cost | currency }}</td>
																<td>@{{ service.charge | currency }}</td>
																<td>@{{ service.tip | currency }}</td>					
						                        				<td><span v-if="service.discount!=null">@{{ service.discount.discount_amount | currency }}</span><span v-else>0</span></td>
						                        				<td><input @click="selectServices(service)" type="checkbox" :disabled="service.active == 0"></td>
															</tr>
														</tbody>
													</table>

												</td>
	                                        </tr>
	                                      	</template>
	                                    </tbody>

					                </table>
					            </div><!-- end table-responsive -->
					            <br><br><br>
								<a href="javascript:void(0)" class="pull-right" @click="showReport"><b>@lang('cpanel/write-report-page.continue')</b></a>

                    </div>
                </div>

	        </div>

        </div>
    </div>

@endsection

@push('scripts')
	
	{!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
	{!! Html::script('cpanel/plugins/datapicker/bootstrap-datepicker.js') !!}
	{!! Html::script('cpanel/plugins/inputmask/jquery.inputmask.bundle.js') !!}
	{!! Html::script(mix('js/visa/reports/index.js', 'cpanel')) !!}

@endpush