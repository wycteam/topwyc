@extends('cpanel.layouts.master')

@section('title', 'Latest News')

@push('styles')
{!! Html::style('cpanel/plugins/summernote/summernote-bs3.css') !!}
{!! Html::style('cpanel/plugins/summernote/summernote.css') !!}
@endpush

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-12">
        <h2>@lang('cpanel/news-page.news-and-events')</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">@lang('cpanel/news-page.home')</a>
            </li>
            <li class="active">
                <strong>@lang('cpanel/news-page.news')</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <news-content id="newsContents" :translation="translation">

    </news-content>

</div>
@endsection
@push('scripts')
    {!! Html::script('cpanel/plugins/summernote/summernote.min.js') !!}
	{!! Html::script(mix('js/visa/news/news.js', 'cpanel')) !!}
@endpush
