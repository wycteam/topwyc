@extends('cpanel.layouts.master')

@section('title', 'Tracking')

@push('styles')
    {!! Html::style(mix('css/visa/tracking/index.css', 'cpanel')) !!}
@endpush

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Package & Group Tracking</h2>
        </div>
    </div>
    <div class="wrapper wrapper-content animated tracking-box">
        <div id="tracking" class="row">
        	<div class="col-lg-12">
	        	<div class="ibox float-e-margins">
                    <div class="ibox-title">
                	    <div class="form-group">
                	    	<div class="row">
                	    		<div class="col-md-6">
	                	    		<div class="input-group">
		                        		<input type="text" class="form-control custom-txt" placeholder="Tracking #" @keydown.enter="searchEnter" v-model="keyword">
		                        		<span class="input-group-btn">
		                        			<!-- DOES NOTHING WHEN INPUT IS EMPTY -->
		                        			<button type="button" class="btn btn-primary" @click="searchBtn">
		                        				<i class="fa fa-search"></i>
		                        			</button>
		                                </span>
		                            </div><!-- end input-group -->
	                	    	</div><!-- end col-lg-6 -->
                	    	</div>
                		</div>
                    </div>
                    <div class="ibox-content">
                    	@include('cpanel.visa.tracking.modals')
                    	<!-- SHOW IF RESULTS ARE EMPTY EITHER CLIENT OR TRACKING-->
                    	<div class="no-result" v-if="count==0" v-cloak>
                    		<h3>No results to show.</h3>
                    	</div>

                    	<div v-else v-cloak>
                    	<!-- HIDE TABLE IF EMPTY OR TRACKING IS BEING SEARCHED -->
	                    	<div class="client-result" v-if="check==0">
		                    	<div id="client-title"><span class="tracking-no">@{{ services.tracking }}</span> <span class="client-name">@{{ services.client }}</span></div>
		                    	<div class="table-responsive">
				                    <table id="clientlist" class="table table-striped table-bordered table-hover">
				                    	<!-- HEADER TEXTS & COLUMNS MAY VARY -->
					                    <thead>
						                    <tr>
						                        <th>Date</th>
						                        <th>Detail</th>
						                        <th>Cost</th>
						                        <th>Charge</th>
						                        <th>Tip</th>
						                        <th>Discount</th>
						                        <th>Total</th>
						                        <th>Status</th>
						                    </tr>
					                    </thead>
						                <tbody>
						                    <tr :class="{'strikethrough': service.active == 0}" v-for="service in services.services" :key="service.id">
						                        <td>@{{ service.service_date }}</td>
						                        <td @click="report(service.id)" :class="[service.report.length!=0 ? process : '']">@{{ service.detail }}</td>
						                        <td>@{{ service.cost | currency }}</td>
						                        <td>@{{ service.charge | currency }}</td>
						                        <td>@{{ service.tip | currency }}</td>
						                        <td><span v-if="service.discount!=null"><a href="#" data-toggle="popover" data-placement="right" data-container="body" :data-content="service.discount.reason" data-trigger="hover">@{{ service.discount.discount_amount | currency }}</a></span><span v-else>0</span></td>
						                        <td>@{{ parseFloat(service.cost) + parseFloat(service.charge) + parseFloat(service.tip) | currency}}</td>
						                        <td>@{{ service.status }}</td>
						                    </tr>
					                    </tbody>
					                </table>
					            </div><!-- end table-responsive -->
				        		<hr>
					            <div class="computed" v-cloak>
					                <table>
					                    <tr>
					                        <td class="align-right"> <span>Package Cost: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ services.package_cost | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Initial Deposit: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ services.package_deposit | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Payment Done: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ services.package_payment | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Refund: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ services.package_refund | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Discount: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ services.package_discount | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Available Balance: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ balance | currency }}</span></td>
					                    <tr>
					                </table>
					            </div>
	                    	</div><!-- end client-result -->

	                    	<!-- HIDE TABLE IF EMPTY OR CLIENT IS BEING SEARCHED -->
	                    	<div class="group-result" v-else>
	                    		<div class="tracking-number">
		                    		<h3>@{{ groups.tracking }}</h3>
		                    		<h5 class="group-name">@{{ groups.name }}</h5>
		                    	</div>
		                    	<div class="table-responsive">
				                    <table id="grouplist" class="table table-striped table-bordered table-hover">
				                    	<!-- HEADER TEXTS & COLUMNS MAY VARY -->
					                    <thead>
						                    <tr>
						                        <th>Name</th>
						                        <th>Client #</th>
						                        <th>Total Service Cost</th>
						                        <th>&nbsp;</th>
						                    </tr>
					                    </thead>
	                                    <tbody>
	                                    	<template v-for="(group, index) in groups.groupmember">
	                                        <tr @click="showServices(group.client_id, $event)" class="clickable">
	                                            <td>@{{ group.client.first_name }} @{{ group.client.last_name }} <span class="leader" v-if="group.leader==1">(Leader)</span></td>
	                                            <td>@{{ group.client_id }}</td>
	                                            <td>@{{ serviceCost[index] | currency}}</td>
	                                        	<td class="text-center toggle-row">
	                                        		<i :id="'fa-'+group.client_id" class="fa fa-arrow-right cursor"></i>
	                                        	</td>
	                                        </tr>
	                                        <tr :id="'client-'+group.client_id" class="hide">
												<td colspan="5">

													<table class="table table-bordered services-tbl" >
														<thead>
															<tr>
																<td></td>
																<td>Date</td>
																<td>Package</td>
																<td>Details</td>
																<td>Cost</td>
																<td>Charge</td>
																<td>Tip</td>
																<td>Discount</td>
															</tr>
														</thead>
														<tbody>
															<tr :class="{'strikethrough': service.active == 0}" v-for="service in group.services" v-if="service.group_id!=0">
																<td>
                                                                <i v-if="service.status.toUpperCase()=='PENDING'" class="fa fa-exclamation-circle" aria-hidden="true"></i>
																<i v-if="service.report.length!=0" class="fa fa-commenting" aria-hidden="true"></i>
																<i v-if="service.status.toUpperCase()!='PENDING' && service.report.length==0" class="fa fa-check-circle" aria-hidden="true"></i>
																</td>
																<td>@{{ service.service_date }}</td>
																<td>@{{ service.tracking }}</td>
                                                                <td @click="report(service.id)" :class="[service.report.length!=0 ? process : '']">@{{ service.detail }}</td>
																<td>@{{ service.cost | currency }}</td>
																<td>@{{ service.charge | currency }}</td>
																<td>@{{ service.tip | currency }}</td>
						                        				<td><span v-if="service.discount!=null">@{{ service.discount.discount_amount | currency }}</span><span v-else>0</span></td>
															</tr>
														</tbody>
													</table>

												</td>
	                                        </tr>
	                                      	</template>
	                                    </tbody>

					                </table>
					            </div><!-- end table-responsive -->
					            <hr>
					            <div class="computed" v-cloak>
					                <table>
					                    <tr>
					                        <td class="align-right"> <span>Total Service Cost: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ compute.total_cost | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Initial Deposit: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ compute.package_deposit | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Payment Done: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ compute.package_payment | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Refund: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ compute.package_refund | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Discount: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ compute.package_discount | currency }}</span></td>
					                    <tr>
					                    <tr>
					                        <td class="align-right"> <span>Available Balance: </span></td>
					                        <td class="align-left"><span class="color-font">@{{ balance | currency }}</span></td>
					                    <tr>
					                </table>
					            </div>

	                    	</div><!-- end tracking-result -->

                    	</div>

                    </div>
                </div>
	        </div>
        </div>

    </div>
@endsection

@push('scripts')
   	{!! Html::script(mix('js/visa/tracking/index.js', 'cpanel')) !!}
@endpush
