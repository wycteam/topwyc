<dialog-modal id='reports' size="modal-md">

    <template slot="modal-title">
       Reports
    </template>
    <template slot="modal-body" class="text-center">
		<div class="table-responsive">
	        <table id="reportlist" class="table table-striped table-bordered table-hover">
	            <thead>
	                <tr>
	                    <th>Details</th>
	                    <th>Date & Time</th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr v-for="report in reports" :key="report.id">
	                    <td>@{{ report.detail }}</td>
	                    <td>@{{ report.log_date }}</td>
	                </tr>
	            </tbody>
	        </table>
	    </div>        
    </template>
    <template slot="modal-footer">
        <button class='btn btn-primary'  data-dismiss="modal">Close</button>
    </template>


</dialog-modal>