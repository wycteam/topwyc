@extends('cpanel.layouts.master')

@section('title', 'Access Control')



@section('content')
    <div id="access-control">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>Access Control</h2>
            <a href="javascript:void(0)" @click="showRoleDialog" class="m-l-3 p-l-1"><i class="fa fa-user-plus fa-fw" ></i> Add Role</a>
        </div>
    </div>

    <div id="access-control" class="wrapper wrapper-content">
        @include('cpanel.visa.access-control.modals')
        <div class="row">
            <div class="col-lg-12">

                <div style="height:10px; clear:both;"></div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table id="roles" class="table table-striped table-bordered table-hover" >
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>User Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr  v-for="role in roles" :key="role.id" v-cloak>
                                    <td>@{{ role.id }}</td>
                                    <td>@{{ role.label }}</td>
                                    <td class="text-center">
                                        <a href="javascript:void(0)" @click="showEditRoleDialog(role)"><i class="fa fa-pencil-square-o" data-toggle="popover" data-placement="top" data-container="body" data-content="Edit Role" data-trigger="hover"></i></a>
                                        <a href="javascript:void(0)" @click="showPermissionDialog(role)"><i class="fa fa-pencil-square-o" data-toggle="popover" data-placement="top" data-container="body" data-content="Edit Permission" data-trigger="hover"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>   

@endsection

@push('scripts')
    {!! Html::script(mix('js/visa/access-control/index.js','cpanel')) !!}
@endpush
