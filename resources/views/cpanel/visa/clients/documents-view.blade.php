@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('cpanel/plugins/lightbox/css/lightbox.css') !!}
    {!! Html::style(mix('css/visa/services/index.css', 'cpanel')) !!}
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12"> 
            <h2>{{ $title }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">@lang('cpanel/document-type-page.home')</a>
                </li>
                <li class="active">
                    <strong>{{ $title }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated service-box" id="clientDocuments" v-cloak>
        @include('cpanel.visa.services.document-modals')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>@lang('cpanel/document-type-page.client-documents')</h3>
                    </div>
                    <div class="ibox-content">
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div
                                        :is="dz"
                                        id="returnedItemFiles"
                                        ref="returnedItemFiles"
                                        :url="'/visa/service-manager/client-documents/'+typeid+'/upload-files'"
                                        :show-remove-link="false"
                                        accepted-file-types="image/jpeg,image/png"
                                        :max-file-size-in-m-b="50"
                                        :max-number-of-files="50"
                                        @vdropzone-success="dropzoneUploadSuccess"
                                        @vdropzone-file-added="dropzoneFileAdded">
                                        <input type="hidden" name="token" value="xxx">
                                        <div class="dz-message" data-dz-message><span>@lang('cpanel/document-type-page.click-to-choose-or-drag-images-to-upload')</span></div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div style="height:10px;clear:both;"></div>

                        <a @click="submit" href="javascript:void(0)" class="btn btn-return-item-submit btn-has-loading btn-success pull-right">@lang('cpanel/document-type-page.submit')</a>

                        <div style="height:20px;clear:both;"></div>

                        <div class="row">
                            <div v-if="files.length == 0" class="text-center">
                                <h1>@lang('cpanel/document-type-page.no-documents-to-show')</h1>
                            </div>

                            <div v-else v-for="(file, index) in files" class="col-lg-3 col-md-4">

                                <div class="media">
                                    <div class="media-left">
                                        <a :href="'/storage/' + file.file_path" data-lightbox="documents" :data-title="'Client ID: ' + file.user_id + '<br>' + 'Issued At: ' + file.issue_at + '<br>' + 'Expired At: ' + file.expires_at">
                                            <img :src="'/storage/' + file.file_path" class="media-object img-thumbnail img-responsive lightbox-image" />
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <strong>@lang('cpanel/document-type-page.client-id'):</strong> @{{ file.user_id }}
                                            </li>
                                            <li class="list-group-item">
                                                <strong>@lang('cpanel/document-type-page.issued-at'):</strong> @{{ file.issue_at }}
                                            </li>
                                            <li class="list-group-item">
                                                <strong>@lang('cpanel/document-type-page.expired-at'):</strong> @{{ file.expires_at }}
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {!! Html::script('cpanel/plugins/lightbox/js/lightbox.js') !!}
    {!! Html::script(mix('js/visa/clients/document.js', 'cpanel')) !!}
@endpush


