<div>
    <div id="timeline" class="m-l-md">
        <transaction-logs
            v-for="log2 in transactionlogs"
            :log2="log2"
            :key="log2.log_id"
        >
        </transaction-logs>
        <div v-show="loading" class="ibox">
            <div class="ibox-content">
                <div class="spiner-example">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

