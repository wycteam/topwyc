<a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addDocumentsModal" v-show="!loading">
    <i class="fa fa-plus"></i> @lang('cpanel/clients-page.add-documents')
</a>


<div style="height:20px;clear:both;"></div>

<div class="row" v-show="!loading">
    <div v-if="files.length == 0" class="text-center">
        <h1>@lang('cpanel/clients-page.no-documents-to-show')</h1>
    </div>

<!--     <div v-else v-for="(file, index) in files" class="col-lg-3 col-md-4">

        <div class="media">
            <div class="media-left">
                <a :href="'/storage/' + file.file_path" data-lightbox="documents" :data-title="'Client ID: ' + file.user_id + '<br>' + 'Issued At: ' + file.issue_at + '<br>' + 'Expired At: ' + file.expires_at">
                    <img :src="'/storage/' + file.file_path" class="media-object img-thumbnail img-responsive lightbox-image" />
                </a>
            </div>
           <div class="media-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Type:</strong> @{{ file.type }}
                    </li>
                    <li class="list-group-item">
                        <strong>Issued At:</strong> @{{ file.issue_at }}
                    </li>
                    <li class="list-group-item">
                        <strong>Expired At:</strong> @{{ file.expires_at }}
                    </li>
                </ul>
            </div>
        </div>

    </div> -->



            <div v-else class="col-lg-3 col-md-4 col-xs-6 thumb thumbnail" v-for="(file, index) in files">
                <br>
                    
                    <div class="row caption">
                        <div class="col-lg-10">
                            <ul class="list-group" style="margin-bottom: 6px">
                                <li class="list-group-item" style="border: 0px;padding-bottom: 0px;padding-top: 0px;margin-bottom: -2px;min-height: 40px;">
                                    <b>@{{ file.type }} </b> 
                                </li>
                                <li class="list-group-item" style="border: 0px;padding-bottom: 0px;padding-top: 0px;margin-bottom: -2px;">
                                    <small>
                                        <strong>@lang('cpanel/clients-page.issued-at'):</strong> 
                                        @{{ file.issue_at }}
                                    </small>
                                </li>
                                <li class="list-group-item" style="border: 0px;padding-bottom: 0px;padding-top: 0px;margin-bottom: -2px;">
                                    <small>
                                        <strong>@lang('cpanel/clients-page.expired-at'):</strong> 
                                        <span v-if="file.expires_at == null">-</span>
                                        <span else>@{{ file.expires_at }}</span>
                                    </small>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-2">
                            <a class="btn btn-danger btn-sm pull-right" @click="deletePrompt(file.id, file.file_path)"><i class="fa fa-trash fa-fw"></i></a>
                        </div>

<!--     <div v-else class="col-lg-3 col-md-4 col-xs-6 thumb thumbnail" v-for="(file, index) in files">
        <br>
            
            <div class="row caption">
                <div class="col-lg-10">
                    <ul class="list-group" style="margin-bottom: 6px">
                        <li class="list-group-item" style="border: 0px;padding-bottom: 0px;padding-top: 0px;margin-bottom: -2px;">
                            <b>@{{ file.type }} </b> 
                        </li>
                        <li class="list-group-item" style="border: 0px;padding-bottom: 0px;padding-top: 0px;margin-bottom: -2px;">
                            <small><strong>Issued At:</strong> @{{ file.issue_at }}</small>
                        </li>
                        <li class="list-group-item" style="border: 0px;padding-bottom: 0px;padding-top: 0px;margin-bottom: -2px;">
                            <small><strong>Expired At:</strong> 
                                <span v-if="file.expires_at == null">-</span>
                                <span else>@{{ file.expires_at }}</span>
                            </small>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2">
                    <a class="btn btn-danger btn-sm pull-right" @click="deletePrompt(file.id, file.file_path)"><i class="fa fa-trash fa-fw"></i></a>
                </div>
            </div> -->
                <a :href="'/storage/' + file.file_path" data-lightbox="documents" :data-title="'Client ID: ' + file.user_id + '<br>' + 'Issued At: ' + file.issue_at + '<br>' + 'Expired At: ' + file.expires_at" @click="magnify('/storage/' + file.file_path)">
                    <img class="img-thumbnail img-responsive"
                         :src="'/storage/' + file.file_path"
                         alt="Another alt text" style="height: 200px;max-height: 200px;">
                </a>
    </div>
</div>
</div>
<div v-show="loading" class="ibox">
    <div class="ibox-content">
        <div class="spiner-example">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
        </div>
    </div>
</div>
