<dialog-modal size="modal-md" id='dialogAddDocument'>
    <template slot="modal-title">
       @{{ act }}@lang('cpanel/document-type-page.client-document-type')
    </template>
    <template slot="modal-body" class="text-center">
       <!-- english service detail -->
        <div class="form-group" :class="{ 'has-error': docsFrm.errors.has('name') }">
            <label class="control-label">@lang('cpanel/document-type-page.name')</label>
            <input class="form-control m-b" id="name" type="text" v-model="docsFrm.name" name="name">
            <p class="help-block" v-if="docsFrm.errors.has('name')" v-text="docsFrm.errors.get('name')"></p>
        </div><!-- end label-floating -->
    </template>
    <template slot="modal-footer">
        <button class='btn btn-primary'  @click="saveDocs(act)">@lang('cpanel/document-type-page.save')</button>
        <button class='btn btn-default'  @click="reloadAll" data-dismiss="modal">@lang('cpanel/document-type-page.cancel')</button>
    </template>
</dialog-modal>
