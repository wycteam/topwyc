<legend class="m-t">@lang('cpanel/clients-page.acct-info')</legend>
<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.email')</label>
            <input class="form-control" id="email" name="email" type="email" placeholder="{{ (($lang)? $lang['enter-email'] : 'Enter Email')}}" @keyup="changeUrl()" autocomplete="off">

            @if ($errors->has('email'))
            <span class="help-block">
                {{ $errors->first('email') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.passport-num')</label>
            {!! Form::text('passport', null, ['id' => 'passport', 'class' => 'form-control', 'placeholder' => (($lang)? $lang['enter-passport'] : 'Enter Passport')]) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
          <label>@lang('cpanel/clients-page.passport') @lang('cpanel/clients-page.valid-until')</label>
          <the-mask id="passport_exp_date" class="input-sm form-control" name="passport_exp_date" placeholder="YYYY-MM-DD" mask="####-##-##"/>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
          <label>@lang('cpanel/clients-page.group')</label>
          <select id="group_id" name="group_id" class="form-control m-b select2">
              <option value="0">@lang('cpanel/clients-page.no-group')</option>
              <option :value="option.id" v-for="option in groups">
                  @{{ option.name }}
              </option>
          </select>
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
      <div class="form-group">
          <label>@lang('cpanel/clients-page.visa-type')</label>
          <select  v-model="v_type" id="visa_type" name="visa_type" class="form-control">
              <option value="0">@lang('cpanel/clients-page.select-type')</option>
              <option value="9A">9A</option>
              <option value="9G">9G</option>
              <option value="TRV">TRV</option>
              <option value="CWV">CWV</option>
          </select>
      </div>
    </div>
    <template v-if="v_type=='9A'">
      <div class="col-md-2">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.arrival')</label>
          <the-mask v-model="arrs" id="arrival_date"  class="input-sm form-control" name="arrival_date"  placeholder="YYYY-MM-DD"  mask="####-##-##" @input="firstExp">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.first-exp-date')</label>
          <the-mask  v-model="frstExp" id="first_exp_date"  class="input-sm form-control" name="first_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.ext-exp-date')</label>
          <the-mask  id="service_exp_date"  class="input-sm form-control" name="service_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##"/>
        </div>
      </div>
    </template>
<template v-if="v_type=='9G' || v_type=='TRV' || v_type=='CWV'">

    <div class="col-md-4">
      <div class="form-group">
          <label>@lang('cpanel/clients-page.exp-date')</label>
          <the-mask  id="service_exp_date"  class="input-sm form-control" name="service_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##"/>
      </div>
    </div>
</template>
</div>

<template v-if="v_type=='9G' || v_type=='TRV'">
<div class="row">
  <!--
  <div class="col-md-4">
    <div class="form-group">
        <label>ICard</label>
        <input type="text" id="icard" name="icard" class="form-control" placeholder="Icard"/>
    </div>
  </div>
  -->
  <div class="col-md-4">
    <div class="form-group">
        <label>@lang('cpanel/clients-page.icard-issue-date')</label>
        <the-mask  id="issue_date"  class="input-sm form-control" name="issue_date"  placeholder="YYYY-MM-DD"  mask="####-##-##"/>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
        <label>@lang('cpanel/clients-page.exp-date')</label>
        <the-mask  id="icard_exp_date"  class="input-sm form-control" name="icard_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##"/>
    </div>
  </div>
</div>
</template>
