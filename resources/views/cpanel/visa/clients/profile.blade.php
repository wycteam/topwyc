@extends('cpanel.layouts.master')

@section('title', 'User Profile')

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnify/2.3.2/css/magnify.min.css">
    {!! Html::style('cpanel/plugins/lightbox/css/lightbox.css') !!}
    {!! Html::style('cpanel/plugins/dualListbox/bootstrap-duallistbox.min.css') !!}
    {!! Html::style('cpanel/plugins/blueimp/css/blueimp-gallery.min.css') !!}
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
    {!! Html::style('js/owl/owl.carousel.css') !!}
    {!! Html::style('cpanel/plugins/chosen/bootstrap-chosen.css') !!}
    {!! Html::style('cpanel/plugins/datapicker/datepicker3.css') !!}
    {!! Html::style('js/owl/owl.theme.css') !!}
    {!! Html::style(mix('css/visa/clients/profile.css', 'cpanel')) !!}

@endpush


@section('content')
<!--     <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-9">
            <h2>User Profile</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/users">Users</a>
                </li>
                <li class="active">
                    <strong>Profile</strong>
                </li>
            </ol>
        </div>
        <div class="col-md-3">
            <a href="{{ route('cpanel.users.edit', $selectedUser->id) }}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-edit fa-fw" ></i> Edit</a>
        </div>
    </div> -->

    <div class="wrapper wrapper-content" id="userprofile" v-cloak>
            @include('cpanel.modals.userModals')
            @include('cpanel.modals.modals')

            <a href="#"  target="_blank" class="load-pdf"></a>

            <div class="row">
                <div class="col-sm-12 no-side-padding fff-bg">
                    <div class="col-sm-2 no-side-padding">
                        <div class="ibox ">
                            <div class="ibox-content">
                                <div class="tab-content">
                                    @if (session()->has('message'))
                                    <div class="alert alert-success alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        {{ session()->get('message') }}
                                    </div>
                                    @endif
                                    <div id="contact-1" class="tab-pane active">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="profile-image" style="margin-bottom: -21px;">
                                                    <img :src="avatar" class="img-circle circle-border m-b-md" alt="profile">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3 class="no-margins m-t-2 client-name">
                                                    <b v-cloak v-if="payment_status == 'High-Risk'"><span style="color: red">@{{ fullname }}</span></b>
                                                    <b v-cloak v-else>@{{ fullname }}</b>
                                                </h3>
                                                <h4><p>
                                                    <span v-cloak>
                                                        <a href="javascript:void(0)" @click="viewReport(usr_id)" data-toggle="popover" data-placement="top" data-container="body" data-content="View Report" data-trigger="hover">@{{ usr_id }}
                                                        </a>
                                                        <small><b>@{{ client_branch }}</b></small>
                                                    </span>
                                                </p></h4>
                                                <a id="editor" href="{{ route('cpanel.visa.client-edit', $selectedUser->id) }}" class="btn btn-primary btn-sm" v-show="selpermissions.edit==1 || setting == 1"><i class="fa fa-edit fa-fw" ></i> @lang('cpanel/clients-page.edit')</a>
                                                <h4><p>
                                                    <a href="#" data-toggle="popover" data-placement="top" data-container="body" :data-content="((translation) ? translation['app-installed-client'] : 'App Installed By Client')" data-trigger="hover"  v-if="binded">
                                                        <b class="fa fa-mobile app-icon m-l-1"></b>
                                                    </a>
                                                    <a href="#" data-toggle="popover" data-placement="top" data-container="body" :data-content="group_binded" data-trigger="hover" v-if="group_binded != ''">
                                                        <b class="fa fa-user-circle group-icon m-l-1"></b>
                                                    </a>
                                                    <a href="#" data-toggle="popover" data-placement="top" data-container="body" :data-content="((translation) ? translation['unread-notif'] : 'Unread Notifications')+': '+unread_notif" data-trigger="hover"  v-if="unread_notif > 0">
                                                        <b class="fa fa-envelope unread-icon m-l-1"></b>
                                                    </a>

                                                </p></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10 no-side-padding">
                        <div class="row">
                            <div class="col-sm-4 no-side-padding">
                                <div class="ibox no-b-margin">
                                    <div class="ibox-content no-tb-padding">
                                        <div class="tab-content">
                                            <div id="contact-1" class="tab-pane active">
                                                <div class="details-size">

                                                    <strong><h2>@lang('cpanel/clients-page.statistics')</h2></strong>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/clients-page.total-pts-earned') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="discount>=0" v-cloak> @{{points | currency}} </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/clients-page.total-complete-serv-cost') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="service_cost>=0" v-cloak> @{{complete_service_cost | currency}} </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/clients-page.total-serv-cost') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="service_cost>=0" v-cloak> @{{service_cost | currency}} </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/clients-page.total-pay') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="payment>=0" v-cloak>  @{{ (deposit + payment) | currency}} </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/clients-page.total-ref') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="refund>=0" v-cloak>@{{refund | currency}} </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/clients-page.total-disc') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="discount>=0" v-cloak>@{{discount | currency}} </span>
                                                        </div>
                                                    </div>

                                                    <!-- <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>Total Transfer :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="discount>=0" v-cloak> @{{transfer | currency}} </span>
                                                        </div>
                                                    </div> -->

                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/clients-page.total-bal') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="discount>=0" v-cloak> @{{balance | currency}} </span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/clients-page.collectables') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-if="discount>=0" v-cloak> @{{ collectables < 0 ? collectables : 0 | currency}} </span>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="display: none">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/groups-page.total-client-commission') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals"  v-cloak> @{{ (total_client_com) ? total_client_com : 0 | currency}} </span>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="display: none">
                                                        <div class="col-sm-8">
                                                            <b>@lang('cpanel/groups-page.total-agent-commission') :</b>
                                                        </div>
                                                        <div class="col-sm-4 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak> @{{ (total_agent_com) ? total_agent_com : 0 | currency}} </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 no-side-padding">
                                <div class="ibox no-b-margin">

                                    <div class="ibox-content no-tb-padding no-side-padding">
                                        <div class="tab-content">
                                            <div id="contact-1" class="tab-pane active">
                                                <div class="details-size">

                                                    <strong><h2>@lang('cpanel/clients-page.basic-info')</h2></strong>

                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <b>@lang('cpanel/clients-page.gender') :</b>
                                                        </div>
                                                        <div class="col-sm-8 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="gender"> @{{gender}} </span>
                                                            <span class="m-l-sm label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty-gender')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <b>@lang('cpanel/clients-page.nationality'):</b>
                                                        </div>
                                                        <div class="col-sm-8 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="nationality"> @{{nationality}} </span>
                                                            <span class="m-l-sm label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <b>@lang('cpanel/clients-page.birth-country'):</b>
                                                        </div>
                                                        <div class="col-sm-8 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="birthcountry"> @{{birthcountry}} </span>
                                                            <span class="m-l-sm label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <b>@lang('cpanel/clients-page.age') :</b>
                                                        </div>
                                                        <div class="col-sm-8 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="bday"> @{{bday | age}} </span>
                                                            <span class="m-l-sm label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <b>@lang('cpanel/clients-page.birthday') :</b>
                                                        </div>
                                                        <div class="col-sm-8 no-side-padding">
                                                            <span class="m-l-sm totals m-r-2" v-cloak v-if="bday"> @{{bday | invoiceFormat}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty-birthday')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <b>@lang('cpanel/clients-page.status') :</b>
                                                        </div>
                                                        <div class="col-sm-8 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak> @{{cstatus}} </span>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-right: 0px;margin-left: -7px;">
                                                        <div class="col-sm-4">
                                                            <b style="margin-left: -8px;">@lang('cpanel/clients-page.add') :</b>
                                                        </div>
                                                        <div class="col-sm-8 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="add"> @{{add}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="ibox no-b-margin">
                                    <div class="ibox-content no-tb-padding">
                                        <div class="tab-content">
                                            <div id="contact-1" class="tab-pane active">
                                                <div class="details-size">

                                                    <strong><h2>@lang('cpanel/clients-page.personal-info')</h2></strong>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.passport-num') :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="passport"> @{{passport}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.passport') @lang('cpanel/clients-page.valid-until') :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="passport_exp_date"> @{{passport_exp_date}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.weight') :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals m-r-2" v-cloak v-if="weight"> @{{weight}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.height') :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="height"> @{{height}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.cnum-abbr') :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="number"> @{{number}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.visa-type') :</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="visa.visa_type"> @{{visa.visa_type}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.not-applicable')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <template v-if="visa.visa_type=='9G' || visa.visa_type=='TRV'">
                                                      <div class="row">
                                                          <div class="col-sm-7">
                                                              <b>@{{visa.visa_type}} @lang('cpanel/clients-page.expiry'):</b>
                                                          </div>
                                                          <div class="col-sm-5 no-side-padding">
                                                              <span class="m-l-sm totals" v-cloak v-if="visa.exp_date"> @{{visa.exp_date}} </span>
                                                              <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                          </div>
                                                      </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.icard-expiration-date'):</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="visa.icard_exp_date"> @{{visa.icard_exp_date}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.issue-date'):</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="visa.issue_date"> @{{visa.issue_date}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                  </template>
                                                  <template v-else-if="visa.visa_type=='9A'">
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.arrival-date'):</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="visa.arrival_date"> @{{visa.arrival_date}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.first-expiry'):</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding" >
                                                            <span class="m-l-sm totals" v-cloak v-if="visa.first_exp_date"> @{{visa.first_exp_date}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@lang('cpanel/clients-page.ext-exp-date'):</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="visa.exp_date"> @{{visa.exp_date}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                  </template>
                                                  <template v-else>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <b>@{{visa.visa_type}} @lang('cpanel/clients-page.expiry'):</b>
                                                        </div>
                                                        <div class="col-sm-5 no-side-padding">
                                                            <span class="m-l-sm totals" v-cloak v-if="visa.exp_date"> @{{visa.exp_date}} </span>
                                                            <span class="m-l-sm m-r-2 label label-danger" v-else v-cloak>
                                                                @lang('cpanel/clients-page.empty')
                                                            </span>
                                                        </div>
                                                    </div>
                                                  </template>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="col-lg-12 m-t-md">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-store"> @lang('cpanel/clients-page.services')</a></li>
                                <li><a data-toggle="tab" href="#tab-action"  @click="getActionLogs">@lang('cpanel/clients-page.action-logs')</a></li>
                                <li><a data-toggle="tab" href="#tab-transaction" @click="getTransactionLogs">@lang('cpanel/clients-page.trans-logs')</a></li>
                                <li><a data-toggle="tab" href="#tab-documents-2" @click="getDocumentLogs">@lang('cpanel/clients-page.docs')</a></li>
                                <li><a data-toggle="tab" href="#tab-documents" @click="getFiles">@lang('cpanel/clients-page.uploads')</a></li>
<!--                             @if(Auth::user()->hasRole('master') || Auth::user()->hasRole('vice-master'))
                                <li class="" v-show="selpermissions.accessControl==1 || setting == 1"><a data-toggle="tab" href="#tab-access-control">@lang('cpanel/clients-page.access-control')</a></li>
                            @endif -->
                                <li class=""><a data-toggle="tab" href="#tab-group-details">@lang('cpanel/clients-page.groups')</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-group-commission" @click="getCommissionLogs">@lang('cpanel/clients-page.commission')</a></li>
                               <!--  <li class=""><a data-toggle="tab" href="#tab-comments">Comments</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-reviews">Reviews</a></li> -->
                            </ul>
                            <div class="tab-content">
                                <div id="tab-store" class="tab-pane active">
                                    <div class="panel-body no-side-padding" style="min-height: 600px;">
                                        @include('cpanel.visa.clients.list-of-tracking')
                                        <div v-if="tracking_selected==null"></div>
                                        <div class="col-sm-12 clientlinks" v-else>
                                            <h4 class="underlined box-title pull-right">
                                                <b v-cloak>@lang('cpanel/clients-page.package-no') : @{{ tracking_selected }}</b></h4>

                                        <template v-if="number == null">
                                            <h4 class="box-title2 " v-if="!group_package" @click="noNumber"><a>@lang('cpanel/clients-page.add-service')</a></h4>
                                        </template>
                                        <template v-else>
                                            <h4 class="box-title2 " v-if="!group_package" v-show="selpermissions.addService==1 || setting == 1"><a data-toggle="modal" data-target="#add-new-service-modal">@lang('cpanel/clients-page.add-service')</a></h4>
                                        </template>

                                            <h4 class="box-title2 " v-if="!group_package" v-show="selpermissions.addFunds==1 || setting == 1">
                                                <div class="ibox-tools">
                                                    <a href="javascript:void(0)" class="blue-link add-funds" data-placement="right">
                                                        <i class="fa fa-archive"></i>@lang('cpanel/clients-page.add-funds')
                                                    </a>
                                                </div>
                                            </h4>
<!--                                             <h4 class="box-title2 "><a>Delete This Package</a></h4> -->
                                            <h4 class="box-title2 " v-if="!group_package" v-show="selpermissions.deleteThisPackage==1 || setting == 1">
                                                <div class="ibox-tools ">
                                                    <a href="javascript:void(0)" class="blue-link delete-package" data-placement="bottom">
                                                        <i class="fa fa-trash"></i>@lang('cpanel/clients-page.delete-package')
                                                    </a>
                                                </div>
                                            </h4>
                                            <h4 class="box-title2 ">
                                                <a href="javascript:void(0)" v-on:click="showAllServices">@lang('cpanel/clients-page.show-all-services')</a>
                                            </h4>
                                        </div>
                                        <!-- <h5 v-if="rcvs.length">@lang('cpanel/clients-page.doc-received')</h5>
                                        <ul class="tag-list" style="padding: 0">
                                            <li v-for="r in rcvs">
                                                <a style="cursor:default;"><i class="fa fa-tag"></i> @{{r}}</a>
                                            </li>
                                        </ul> -->
                                        @include('cpanel.visa.clients.list-of-services')
                                        <br><br>
                                        <div v-if="tracking_selected" class="m-l-sm">
                                            <h3>@lang('cpanel/clients-page.package-cost') : @{{package_cost}}</h3>
                                            <template v-if="pack_date">
                                                <h3>@lang('cpanel/clients-page.initial-dep') : @{{package_deposit}}</h3>
                                                <h3>@lang('cpanel/clients-page.payment-done') : @{{package_payment}}</h3>
                                                <h3>@lang('cpanel/clients-page.refund')  : @{{package_refund}}</h3>
                                                <h3>@lang('cpanel/clients-page.disc') : @{{package_discount}}</h3>
                                            </template>
                                            <h3>@lang('cpanel/clients-page.avail-bal') : @{{package_balance}}</h3>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-action" class="tab-pane">
                                    <div class="panel-body">
                                        @include('cpanel.visa.clients.action-logs')
                                    </div>
                                </div>
                                <div id="tab-transaction" class="tab-pane">
                                    <div class="panel-body">
                                        @include('cpanel.visa.clients.transaction-logs')
                                    </div>
                                </div>

                                <div id="tab-documents-2" class="tab-pane">
                                    <div class="panel-body">
                                        @include('cpanel.visa.clients.document-logs')
                                    </div>
                                </div>

                                <div id="tab-documents" class="tab-pane">
                                    <div class="panel-body">
                                        @include('cpanel.visa.clients.documents')
                                    </div>
                                </div>

                            @if(Auth::user()->hasRole('master') || Auth::user()->hasRole('vice-master') || Auth::user()->hasRole('cpanel-admin'))
                                <div id="tab-access-control" class="tab-pane" v-show="selpermissions.accessControl==1 || setting == 1">
                                    @include('cpanel.includes.user.access-control')
                                </div>
                            @endif

                                <div id="tab-group-details" class="tab-pane">
                                    @include('cpanel.includes.user.group-details')

                                </div>
                                <div id="tab-group-commission" class="tab-pane">
                                    @include('cpanel.visa.clients.commission-logs')

                                </div>

                                <div id="tab-comments" class="tab-pane">
                                </div>

                                <div id="tab-reviews" class="tab-pane">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <!-- end col-sm-8 -->
    </div>


@endsection

@push('scripts')
    <!-- {!! Html::script('cpanel/plugins/jasny/jasny-bootstrap.min.js') !!} -->
    {!! Html::script('cpanel/plugins/chosen/chosen.jquery.js') !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnify/2.3.2/js/jquery.magnify.min.js"></script>
    {!! Html::script('cpanel/plugins/lightbox/js/lightbox.js') !!}
    {!! Html::script('cpanel/plugins/dualListbox/jquery.bootstrap-duallistbox.js') !!}
    {!! Html::script('cpanel/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('/js/owl/owl.carousel.js') !!}
    {!! Html::script('cpanel/plugins/typehead/bootstrap3-typeahead.min.js') !!}
    {!! Html::script('cpanel/plugins/datapicker/bootstrap-datepicker.js') !!}
    {!! Html::script('cpanel/plugins/inputmask/jquery.inputmask.bundle.js') !!}
    {!! Html::script(mix('js/userprofile.js','cpanel')) !!}

@endpush

@push('scripts')
<script>
    // $(function(){
    //   $("#birth_date" ).val($('#hiddendate').val());
    //     $('#birth_date').datepicker().on('changeDate', function (ev) {
    //         $("#hiddendate" ).val($('#birth_date').val());
    //     });
    // });
</script>
@endpush
