<legend>@lang('cpanel/clients-page.personal-info')</legend>

<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.fname')</label>

            {!! Form::text('first_name', null, ['id' => 'first_name', 'class' => 'form-control', 'placeholder' => (($lang)? $lang['enter-fname'] : 'Enter First Name')]) !!}

            @if ($errors->has('first_name'))
            <span class="help-block">
                {{ $errors->first('first_name') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.mname')</label>
            {!! Form::text('middle_name', null, ['id' => 'middle_name', 'class' => 'form-control', 'placeholder' => (($lang)? $lang['enter-mname'] : 'Enter Middle Name')]) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.lname')</label>
            {!! Form::text('last_name', null, ['id' => 'last_name', 'class' => 'form-control', 'placeholder' => (($lang)? $lang['enter-lname'] : 'Enter Last Name')]) !!}

            @if ($errors->has('last_name'))
            <span class="help-block">
                {{ $errors->first('last_name') }}
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.date-of-birth')</label>
            {!! Form::date('birth_date', null, ['id' => 'birth_date', 'class' => 'form-control', 'placeholder' => (($lang)? $lang['enter-bday'] : 'Enter Birthday'), 'maxlength' => 10]) !!}

            @if ($errors->has('birth_date'))
            <span class="help-block">
                {{ $errors->first('birth_date') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.gender')</label>
            {!! Form::select('gender', ['Male' => $lang['male'], 'Female' => $lang['female']], null, ['class' => 'form-control', 'placeholder' => (($lang)? $lang['select'] : 'Select')]) !!}

            @if ($errors->has('gender'))
            <span class="help-block">
                {{ $errors->first('gender') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('civil_status') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.cstatus')</label>
            {!! Form::select('civil_status', [
            'Single' => $lang['single'], 
            'Married' => $lang['married'], 
            'Widowed' => $lang['widowed'], 
            'Divorced' => $lang['divorced'], 
            'Separated' => $lang['separated']], 
            null, ['class' => 'form-control', 'placeholder' => (($lang)? $lang['select'] : 'Select')]) !!}

            @if ($errors->has('civil_status'))
            <span class="help-block">
                {{ $errors->first('civil_status') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.height')</label>
            {!! Form::text('height', null, ['id' => 'height', 'class' => 'form-control', 'placeholder' =>(($lang)? $lang['enter-height'] : 'Enter Height')]) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.weight')</label>
            {!! Form::text('weight', null, ['id' => 'weight', 'class' => 'form-control', 'placeholder' =>(($lang)? $lang['enter-weight'] : 'Enter Weight')]) !!}
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-4">
      <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
          <label>@lang('cpanel/clients-page.nationality')</label>
          {!! Form::select('nationality', 
          [
          'American'      => (($lang)? $lang['american'] : 'American'),
          'British'       => (($lang)? $lang['british'] : 'British'),
          'Canadian'      => (($lang)? $lang['canadian'] : 'Canadian'),
          'Chinese'       => (($lang)? $lang['chinese'] : 'Chinese'), 
          'Costarricense' => (($lang)? $lang['costarricense'] : 'Costarricense'),
          'Cypriot'       => (($lang)? $lang['cypriot'] : 'Cypriot'),
          'Filipino'      => (($lang)? $lang['filipino'] : 'Filipino'),
          'French'        => (($lang)? $lang['french'] : 'French'),
          'Indian'        => (($lang)? $lang['indian'] : 'Indian'),
          'Indonesian'    => (($lang)? $lang['indonesian'] : 'Indonesian'),
          'Israeli'       => (($lang)? $lang['israeli'] : 'Israeli'),
          'Italian'       => (($lang)? $lang['italian'] : 'Italian'),
          'Irish'         => (($lang)? $lang['irish'] : 'Irish'),
          'Japanese'      => (($lang)? $lang['japanese'] : 'Japanese'),
          'Korean'        => (($lang)? $lang['korean'] : 'Korean'),
          'Latvian'       => (($lang)? $lang['latvian'] : 'Latvian'),
          'Malaysian'     => (($lang)? $lang['malaysian'] : 'Malaysian'),
          'Salvadoran'    => (($lang)? $lang['salvadoran'] : 'Salvadoran'),
          'Swedish'       => (($lang)? $lang['swedish'] : 'Swedish'),
          'Taiwanese'     => (($lang)? $lang['taiwanese'] : 'Taiwanese'),
          'Thai'          => (($lang)? $lang['thai'] : 'Thai'),
          'Vanuatu'       => (($lang)? $lang['ni-vanuatu'] : 'Vanuatu'),
          'Vietnamese'    => (($lang)? $lang['vietnamese'] : 'Vietnamese'),
          ], null, ['id'=>'nationality','class' => 'form-control', 'placeholder' => (($lang)? $lang['select'] : 'Select')]) !!}
          @if ($errors->has('nationality'))
          <span class="help-block">
              {{ $errors->first('nationality') }}
          </span>
          @endif
      </div>
  </div>
  <div class="col-md-4">
      <div class="form-group{{ $errors->has('birth_country') ? ' has-error' : '' }}">
          <label>@lang('cpanel/clients-page.birth-country')</label>
          {!! Form::select('birth_country', 
          [
          'America'         => (($lang)? $lang['america'] : 'America'),
          'Canadian'        => (($lang)? $lang['canada'] : 'Canada'),
          'China'           => (($lang)? $lang['china'] : 'Chinese'), 
          'Costa Rica'      => (($lang)? $lang['costa-rica'] : 'Costa Rica'),
          'Cyprus'          => (($lang)? $lang['cyprus'] : 'Cyprus'),
          'El Salvador'     => (($lang)? $lang['el-salvador'] : 'El Salvador'),
          'France'          => (($lang)? $lang['france'] : 'France'),
          'India'           => (($lang)? $lang['india'] : 'India'),
          'Indonesia'       => (($lang)? $lang['indonesia'] : 'Indonesia'),
          'Israel'          => (($lang)? $lang['israel'] : 'Israel'),
          'Italy'           => (($lang)? $lang['italy'] : 'Italy'),
          'Ireland'         => (($lang)? $lang['ireland'] : 'Ireland'),
          'Japan'           => (($lang)? $lang['japan'] : 'Japan'),
          'Korea'           => (($lang)? $lang['korea'] : 'Korea'),
          'Latvia'          => (($lang)? $lang['latvia'] : 'Latvia'),
          'Malaysia'        => (($lang)? $lang['malaysia'] : 'Malaysia'),
          'Philippines'     => (($lang)? $lang['philippines'] : 'Philippines'), 
          'Sweden'          => (($lang)? $lang['sweden'] : 'Sweden'),
          'Taiwan'          => (($lang)? $lang['taiwan'] : 'Taiwan'),
          'Thailand'        => (($lang)? $lang['thailand'] : 'Thailand'),
          'United Kingdom'  => (($lang)? $lang['united-kingdom'] : 'United Kingdom'),
          'Vanuatu'         => (($lang)? $lang['vanuatu'] : 'Vanuatu'),
          'Vietnam'         => (($lang)? $lang['vietnam'] : 'Vietnam'),
          ], null, ['id'=>'birth_country','class' => 'form-control', 'placeholder' => (($lang)? $lang['select'] : 'Select')]) !!}
          @if ($errors->has('birth_country'))
          <span class="help-block">
              {{ $errors->first('birth_country') }}
          </span>
          @endif
      </div>
  </div>
</div>
