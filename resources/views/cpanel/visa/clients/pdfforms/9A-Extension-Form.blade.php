<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="style.css" media="all" />
    <style type="text/css">
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #001028;
  text-decoration: none;
}

body {
  font-family: 'Helvetica', 'Arial';
  position: relative;
  width: 17cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-size: 14px; 
}

p {
  font-weight: normal;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 30px;
}

table td .left{
  text-align: left;
}
table td .right{
  text-align: right;
}

table .service,
table .desc {
  text-align: left;
}

#details {
  margin-bottom: 30px;
}

/*NEW*/

.frm-title{
  font-size: 12px;
  letter-spacing: 2px;
}

.frm-title-2{
  font-size: 14px;
}

.small-notes {
  font-size: 10px;
}

.head-1{
  font-size: 13px;
}

.head-2{
  font-size: 9px;
  letter-spacing: 1px;
}

.head-3{
  font-size: 7px;
}

.head-4{
  font-size: 10px;
  letter-spacing:5px;
}

.info-box{
  width: 12px;
  height: 12px;
  margin: 0;
   
}

.info-box-small{
  width: 10px;
  height: 10px;
}

.info-box-large{
  width: 12px;
  height: 15px;
}

.info-box-med{
  width: 12px;
  height: 17px;
}

.first-box{
  border: 1px solid gray;
}


.next-box{
  border: 1px solid gray;
  border-left: none;
}

div.inline { 
  float:left;   
}

.hide{
  display: none;
}

.x-select{
   font-weight: bold;
   font-size: 14px;
}

.input-text{
  letter-spacing: 2px;
  font-size: 10px;
  text-transform: uppercase;
}

.underline {
  width: 300px;
  border-bottom: 1px solid;
}

.dash{
  width: 4px;
  border-bottom: 1px solid;
}

.sign{
  width: 150px;
  border-bottom: 1px solid;
}

.date{
  width: 100px;
  border-bottom: 1px solid;
}

.address-text{
  font-size: 10px;
  text-transform: uppercase;
}


  </style>

</head>
  <body>
    <main>

      <div style="position:absolute;left:33%;margin-left:-306px;top:-60px;width:780px;height:936px;overflow:visible">

        <div style="position:absolute;left:60px;top:30px">
          <img class="immig" weight="55" height="55" src="{{ URL::to('/') }}/cpanel/img/PDF Forms/Bureau-of-immigration.png">
        </div>
        <div style="position:absolute;left:125px;top:35px">
          <span class="frm-title-2">BI FORM NO.IRD04.QF.001 Rev 02</b</span>
        </div>
        <div style="position:absolute;right:-10px;top:37px">
          <span class="small-notes">This document may be reproduced and is</span>
          <span class="small-notes">NOT for sale</b></span>
        </div>
        <div style="position:absolute;left:125px;top:48px">
          <span class="frm-title-2">CONSOLIDATED GENERAL APPLICATION FORM</b></span>
        </div>

        <div style="position:absolute;left:125px;top:62px">
          <span class="frm-title-2">FOR TOURIST VISA EXTENSION</span>
        </div>

        <div style="position:absolute;right:170px;top:66px">
          <span class="head-1"><b>Method of Application:</b></span>
        </div>

        <div style="position:absolute;left:60px;top:90px">
          <span class="head-1"><b>I. APPLICATION INFORMATION</b></span>

        </div>

        <!-- PERSONAL -->
        <div>
          <div style="position:absolute;right:298px;top:80px">
            <div class="info-box-small first-box"></div>
          </div>

          <div style="position:absolute;right:245px;top:79px">
            <span class="head-1">Personal</span>
          </div>

          <!-- SHOW IF PERSONAL IS SELECTED -NOT IN USE -->
      <!--     <div style="position:absolute;right:305px;top:117px">
            <span class="head-1 x-select"><b>X</b></span>
          </div> -->
          <!-- END OF SHOW IF PERSONAL IS SELECTED -->
        </div>


        <!-- AUTHORIZED REPRESENTATIVE -->
        <div>
          <div style="position:absolute;right:200px;top:80px">
            <div class="info-box-small first-box"></div>
          </div>

          <div style="position:absolute;right:46px;top:79px">
            <span class="head-1">Authorized Representative</span>
          </div>

          @if($data['auth_rep'] != "")
           <!-- SHOW IF AUTH REP IS SELECTED -->
          <div style="position:absolute;right:202px;top:76px">
            <span class="head-1 x-select"><b>x</b></span>
          </div>
          <!-- END OF SHOW IF AUTH REP IS SELECTED -->
          @endif
        </div>

        <!-- MONTHS REQUESTED -->
        <div>
          <div style="position:absolute;left:60px;top:105px">
            <span class="head-1">Number of Months Requested</span>
          </div>

          <!-- BOXES FOR NUMBER OF MONTHS REQUIRED -->
          <div style="position:absolute;left:60px;top:120px">
            <div class="info-box inline first-box"></div>
            <div class="info-box inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NUMBER OF MONTHS REQUIRED -->

          <!-- NUMBER OF MONTHS REQUIRED -->
          <div style="position:absolute;left:40px;top:110px;">
              <p class="input-text">{{$data['months_req']}}</p>
          </div>
        
        </div>

        <!-- ACCREDITATION NUMBER -->
        <div>
          <div style="position:absolute;left:473px;top:105px">
            <span class="head-1">Accreditation Number</span>
          </div>
          <!-- BOXES FOR ACCREDITATION NUMBER -->
          <div style="position:absolute;left:465px;top:120px">
            <!--  1 --><div class="info-box-large inline first-box"></div>
            <!--  2 --><div class="info-box-large inline next-box"></div>
            <!--  3 --><div class="info-box-large inline next-box"></div>
            <!--  4 --><div class="info-box-large inline next-box"></div>
            <!--  5 --><div class="info-box-large inline next-box"></div>
            <!--  6 --><div class="info-box-large inline next-box"></div>
            <!--  7 --><div class="info-box-large inline next-box"></div>
            <!--  8 --><div class="info-box-large inline next-box"></div>
            <!--  9 --><div class="info-box-large inline next-box"></div>
            <!-- 10 --><div class="info-box-large inline next-box"></div>
            <!-- 11 --><div class="info-box-large inline next-box"></div>
            <!-- 12 --><div class="info-box-large inline next-box"></div>
            <!-- 13 --><div class="info-box-large inline next-box"></div>
            <!-- 14 --><div class="info-box-large inline next-box"></div>
            <!-- 15 --><div class="info-box-large inline next-box"></div>
            <!-- 16 --><div class="info-box-large inline next-box"></div>
            <!-- 17 --><div class="info-box-large inline next-box"></div>
            <!-- 18 --><div class="info-box-large inline next-box"></div>
            <!-- 19 --><div class="info-box-large inline next-box"></div>
            <!-- 20 --><div class="info-box-large inline next-box"></div>
            <!-- 21 --><div class="info-box-large inline next-box"></div>
            <!-- 22 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR ACCREDITATION NUMBER -->

          <!-- INPUT FOR ACCREDITATION NUMBER -->
          <div style="position:absolute;left:182px;top:112px;">
            <p class="input-text">{{$data['acc_num']}}</p>
          </div>
        </div>
        <!-- END OF ACCREDITATION NUMBER -->

        <!-- Authorized Representative -->
        <div>
          @if($data['auth_rep'] != "")
          <div style="position:absolute;right:100px;top:135px">
            <span class="head-1">Name of Authorized Representative</span>
          </div>
          @elseif($data['auth_rep'] == "")
          <div style="position:absolute;right:210px;top:135px">
            <span class="head-1">Name of Authorized Representative</span>
          </div>
          @endif
          <div style="position:absolute;right:153px;top:150px">
            <span class="head-2">(Last Name, Given Name, MI)</span>
          </div>
          <!-- BOXES FOR Authorized Representative -->
          <div style="position:absolute;left:465px;top:160px">
            <!--  1 --><div class="info-box-large inline first-box"></div>
            <!--  2 --><div class="info-box-large inline next-box"></div>
            <!--  3 --><div class="info-box-large inline next-box"></div>
            <!--  4 --><div class="info-box-large inline next-box"></div>
            <!--  5 --><div class="info-box-large inline next-box"></div>
            <!--  6 --><div class="info-box-large inline next-box"></div>
            <!--  7 --><div class="info-box-large inline next-box"></div>
            <!--  8 --><div class="info-box-large inline next-box"></div>
            <!--  9 --><div class="info-box-large inline next-box"></div>
            <!-- 10 --><div class="info-box-large inline next-box"></div>
            <!-- 11 --><div class="info-box-large inline next-box"></div>
            <!-- 12 --><div class="info-box-large inline next-box"></div>
            <!-- 13 --><div class="info-box-large inline next-box"></div>
            <!-- 14 --><div class="info-box-large inline next-box"></div>
            <!-- 15 --><div class="info-box-large inline next-box"></div>
            <!-- 16 --><div class="info-box-large inline next-box"></div>
            <!-- 17 --><div class="info-box-large inline next-box"></div>
            <!-- 18 --><div class="info-box-large inline next-box"></div>
            <!-- 19 --><div class="info-box-large inline next-box"></div>
            <!-- 20 --><div class="info-box-large inline next-box"></div>
            <!-- 21 --><div class="info-box-large inline next-box"></div>
            <!-- 22 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR Authorized Representative -->
          <!-- INPUT FOR Authorized Representative -->
          <div style="position:absolute;left:182px;top:162px;">
            <span class="input-text">{{$data['auth_rep']}}</span>
          </div>
          <!-- BOXES FOR Authorized Representative -->
          <div style="position:absolute;left:465px;top:190px">
            <!--  1 --><div class="info-box-large inline first-box"></div>
            <!--  2 --><div class="info-box-large inline next-box"></div>
            <!--  3 --><div class="info-box-large inline next-box"></div>
            <!--  4 --><div class="info-box-large inline next-box"></div>
            <!--  5 --><div class="info-box-large inline next-box"></div>
            <!--  6 --><div class="info-box-large inline next-box"></div>
            <!--  7 --><div class="info-box-large inline next-box"></div>
            <!--  8 --><div class="info-box-large inline next-box"></div>
            <!--  9 --><div class="info-box-large inline next-box"></div>
            <!-- 10 --><div class="info-box-large inline next-box"></div>
            <!-- 11 --><div class="info-box-large inline next-box"></div>
            <!-- 12 --><div class="info-box-large inline next-box"></div>
            <!-- 13 --><div class="info-box-large inline next-box"></div>
            <!-- 14 --><div class="info-box-large inline next-box"></div>
            <!-- 15 --><div class="info-box-large inline next-box"></div>
            <!-- 16 --><div class="info-box-large inline next-box"></div>
            <!-- 17 --><div class="info-box-large inline next-box"></div>
            <!-- 18 --><div class="info-box-large inline next-box"></div>
            <!-- 19 --><div class="info-box-large inline next-box"></div>
            <!-- 20 --><div class="info-box-large inline next-box"></div>
            <!-- 21 --><div class="info-box-large inline next-box"></div>
            <!-- 22 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR ACCREDITATION NUMBER -->

          <!-- BOXES FOR ACCREDITATION NUMBER -->
          <div style="position:absolute;left:465px;top:220px">
            <!--  1 --><div class="info-box-large inline first-box"></div>
            <!--  2 --><div class="info-box-large inline next-box"></div>
            <!--  3 --><div class="info-box-large inline next-box"></div>
            <!--  4 --><div class="info-box-large inline next-box"></div>
            <!--  5 --><div class="info-box-large inline next-box"></div>
            <!--  6 --><div class="info-box-large inline next-box"></div>
            <!--  7 --><div class="info-box-large inline next-box"></div>
            <!--  8 --><div class="info-box-large inline next-box"></div>
            <!--  9 --><div class="info-box-large inline next-box"></div>
            <!-- 10 --><div class="info-box-large inline next-box"></div>
            <!-- 11 --><div class="info-box-large inline next-box"></div>
            <!-- 12 --><div class="info-box-large inline next-box"></div>
            <!-- 13 --><div class="info-box-large inline next-box"></div>
            <!-- 14 --><div class="info-box-large inline next-box"></div>
            <!-- 15 --><div class="info-box-large inline next-box"></div>
            <!-- 16 --><div class="info-box-large inline next-box"></div>
            <!-- 17 --><div class="info-box-large inline next-box"></div>
            <!-- 18 --><div class="info-box-large inline next-box"></div>
            <!-- 19 --><div class="info-box-large inline next-box"></div>
            <!-- 20 --><div class="info-box-large inline next-box"></div>
            <!-- 21 --><div class="info-box-large inline next-box"></div>
            <!-- 22 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR ACCREDITATION NUMBER -->
        </div>
        <!-- END OF ACCREDITATION NUMBER -->


        <div style="position:absolute;left:-215px;top:143px">
          <span class="head-1"><b>Reason:</b></span>
        </div>
 
        <!-- REASON-PLEASURE -->
        <div>
          <!-- BOXES FOR REASON-PLEASURE -->
          <div style="position:absolute;left:-215px;top:157px">
            <div class="info-box-small inline first-box"></div>
          </div>

          <!-- IF REASON-PLEASURE IS SELECTED - NOT BEING USED AS PER JONAH-->
<!--           <div style="position:absolute;left:-260px;top:200px">
            <p class="head-1"><b>X</b></p>
          </div> -->

          <div style="position:absolute;left:-213px;top:157px">
            <span class="head-1">Pleasure</span>
          </div>
          <!-- END OF BOXES FOR REASON-PLEASURE -->
        </div>

        <!-- REASON-PLEASURE-PERMIT -->
        <div>
          <!-- BOXES FOR REASON-PLEASURE-PERMIT -->
          <div style="position:absolute;left:-75px;top:157px">
            <div class="info-box-small inline first-box"></div>
          </div>

          <!-- IF REASON-PLEASURE-PERMIT IS CHECKED - NOT BEING USED AS PER JONAH-->
<!--           <div style="position:absolute;left:-113px;top:210px">
            <span class="head-1"><b>X</b></span>
          </div> -->

          <div style="position:absolute;left:-71px;top:157px">
            <span class="head-1">With Valid Special Study Permit</span><br>  
          </div>
          <!-- END OF BOXES FOR REASON-PLEASURE-PERMIT -->
        </div>


        <!-- REASON-HEALTH -->
        <div>
          <!-- BOXES FOR REASON-HEALTH -->
          <div style="position:absolute;left:-215px;top:180px">
            <div class="info-box-small inline first-box"></div>
          </div>

          <!-- IF REASON-HEALTH IS SELECTED - NOT BEING USED AS PER JONAH-->
<!--           <div style="position:absolute;left:-260px;top:225px">
            <p class="head-1"><b>X</b></p>
          </div> -->

          <div style="position:absolute;left:-213px;top:180px">
            <span class="head-1">Health</span>
          </div>
          <!-- END OF BOXES FOR REASON-HEALTH -->
        </div>

        <!-- REASON-HEALTH-PERMIT -->
        <div>
          <!-- BOXES FOR REASON-HEALTH-PERMIT -->
          <div style="position:absolute;left:-75px;top:180px">
            <div class="info-box-small inline first-box"></div>
          </div>

          <!-- IF REASON-HEALTH-PERMIT IS CHECKED - NOT BEING USED AS PER JONAH-->
<!--           <div style="position:absolute;left:-113px;top:235px">
            <span class="head-1"><b>X</b></span>
          </div> -->

          <div style="position:absolute;left:-71px;top:180px">
            <span class="head-1">With Valid Special Study Permit</span><br>  
          </div>
          <!-- END OF BOXES FOR REASON-HEALTH-PERMIT -->
        </div>

        <!-- REASON-BUSINESS -->
        <div>
          <!-- BOXES FOR REASON-BUSINESS -->
          <div style="position:absolute;left:-215px;top:200px">
            <div class="info-box-small inline first-box"></div>
          </div>

          <!-- IF REASON-BUSINESS IS SELECTED - NOT BEING USED AS PER JONAH-->
<!--           <div style="position:absolute;left:-260px;top:249px">
            <p class="head-1"><b>X</b></p>
          </div> -->

          <div style="position:absolute;left:-213px;top:200px">
            <span class="head-1">Business</span>
          </div>
          <!-- END OF BOXES FOR REASON-BUSINESS -->
        </div>

        <!-- REASON-BUSINESS-PERMIT -->
        <div>
          <!-- BOXES FOR REASON-BUSINESS-PERMIT -->
          <div style="position:absolute;left:-75px;top:200px">
            <div class="info-box-small inline first-box"></div>
          </div>

          <!-- IF REASON-HEALTH-BUSINESS IS CHECKED - NOT BEING USED AS PER JONAH-->
<!--           <div style="position:absolute;left:210px;top:260px">
            <span class="head-1"><b>X</b></span>
          </div> -->

          <div style="position:absolute;left:-71px;top:200px">
            <span class="head-1">With Valid Provisional Work Permit</span><br>  
          </div>
          <!-- END OF BOXES FOR REASON-BUSINESS-PERMIT -->
        </div>

        <!-- REASON-OTHERS -->
        <div>
          <!-- BOXES FOR REASON-OTHERS -->
          <div style="position:absolute;left:-215px;top:225px">
            <div class="info-box-small inline first-box"></div>
          </div>

          <!-- IF REASON-OTHERS IS SELECTED - NOT BEING USED AS PER JONAH-->
<!--           <div style="position:absolute;left:63px;top:275px">
            <p class="head-1"><b>X</b></p>
          </div>
 -->
          <div style="position:absolute;left:-213px;top:220px">
            <span class="head-1">Others,</span><br><br>
          </div>
          <!-- END OF BOXES FOR REASON-OTHERS -->
          
        </div>

        <!-- REASON-Please specify -->
        <div>
          <div style="position:absolute;left:60px;top:240px">
            <span class="head-1">Please specify:</span>
          </div>
          <div style="position:absolute;left:150px;top:255px">
            <div class="underline inline"></div>
          </div>
        </div>

        <!-- REASON-Please specify -->

        <!-- END REASON-OTHERS-PERMIT -->

        <div>
          <!-- BOXES FOR REASON-OTHERS-PERMIT -->
          <div style="position:absolute;left:-89px;top:225px">
            <div class="info-box-small inline first-box"></div>
          </div>

          <!-- IF REASON-HEALTH-OTHERS IS CHECKED - NOT BEING USED AS PER JONAH-->
<!--           <div style="position:absolute;left:210px;top:260px">
            <span class="head-1"><b>X</b></span>  
          </div> -->

          <div style="position:absolute;left:-85px;top:220px">
            <span class="head-1">With Valid Limited Work Permit</span><br>  
          </div>
          <!-- END OF BOXES FOR REASON-OTHERS-PERMIT -->
        </div>

        <!-- PART II. PERSONAL INFO -->
        <div style="position:absolute;left:60px;top:260px">
          <span class="head-1"><b>II. PERSONAL INFORMATION</b></span>
        </div>

        <!-- NAME -->
        <div>
          <div style="position:absolute;left:60px;top:278px">
            <span class="head-2">Last Name, Given Name, Middle Name, Other name/ALIAS</span>
          </div>
          <div>
            <div style="position:absolute;left:473px;top:305px">
              <span class="head-2"><b>Residential Address in the Philippines</b></span>
            </div>
            <div style="position:absolute;left:473px;top:315px">
              <span class="head-2">Number & Street</span>
            </div>
            <div style="position:absolute;left:600px;top:315px">
              <span class="head-2">Subdivision / Village </span>
            </div>
            <div style="position:absolute;left:473px;top:342px">
              <span class="head-2">Barangay, Municipality, City</span>
            </div>

            <div style="position:absolute;left:473px;top:370px">
              <span class="head-2">Province, Zip Code</span>
            </div>

            <div style="position:absolute;left:473px;top:398px">
              <span class="head-2">Mobile Number</span>
            </div>

          </div>


          <!-- INPUT FOR NAME -->
          <div style="position:absolute;left:62px;top:290px;">
            <span class="input-text">{{ $data['lname'] }}, {{ $data['fname'] }} {{ $data['mname'] }}</span>
          </div>
          <!-- BOXES FOR NAME -->
          <div style="position:absolute;left:60px;top:288px">
 <!--  1 --><div class="info-box-med inline first-box"></div>
 <!--  2 --><div class="info-box-med inline next-box"></div>
 <!--  3 --><div class="info-box-med inline next-box"></div>
 <!--  4 --><div class="info-box-med inline next-box"></div>
 <!--  5 --><div class="info-box-med inline next-box"></div>
 <!--  6 --><div class="info-box-med inline next-box"></div>
 <!--  7 --><div class="info-box-med inline next-box"></div>
 <!--  8 --><div class="info-box-med inline next-box"></div>
 <!--  9 --><div class="info-box-med inline next-box"></div>
 <!-- 10 --><div class="info-box-med inline next-box"></div>

 <!-- 11 --><div class="info-box-med inline next-box"></div>
 <!-- 12 --><div class="info-box-med inline next-box"></div>
 <!-- 13 --><div class="info-box-med inline next-box"></div>
 <!-- 14 --><div class="info-box-med inline next-box"></div>
 <!-- 15 --><div class="info-box-med inline next-box"></div>
 <!-- 16 --><div class="info-box-med inline next-box"></div>
 <!-- 17 --><div class="info-box-med inline next-box"></div>
 <!-- 18 --><div class="info-box-med inline next-box"></div>
 <!-- 19 --><div class="info-box-med inline next-box"></div>
 <!-- 20 --><div class="info-box-med inline next-box"></div>

 <!-- 21 --><div class="info-box-med inline next-box"></div>
 <!-- 22 --><div class="info-box-med inline next-box"></div>
 <!-- 23 --><div class="info-box-med inline next-box"></div>
 <!-- 24 --><div class="info-box-med inline next-box"></div>
 <!-- 25 --><div class="info-box-med inline next-box"></div>
 <!-- 27 --><div class="info-box-med inline next-box"></div>
 <!-- 27 --><div class="info-box-med inline next-box"></div>
 <!-- 28 --><div class="info-box-med inline next-box"></div>
 <!-- 29 --><div class="info-box-med inline next-box"></div>
 <!-- 30 --><div class="info-box-med inline next-box"></div>

 <!-- 31 --><div class="info-box-med inline next-box"></div>
 <!-- 32 --><div class="info-box-med inline next-box"></div>
 <!-- 33 --><div class="info-box-med inline next-box"></div>
 <!-- 34 --><div class="info-box-med inline next-box"></div>
 <!-- 35 --><div class="info-box-med inline next-box"></div>
 <!-- 37 --><div class="info-box-med inline next-box"></div>
 <!-- 37 --><div class="info-box-med inline next-box"></div>
 <!-- 38 --><div class="info-box-med inline next-box"></div>
 <!-- 39 --><div class="info-box-med inline next-box"></div>
 <!-- 40 --><div class="info-box-med inline next-box"></div>

 <!-- 41 --><div class="info-box-med inline next-box"></div>
 <!-- 42 --><div class="info-box-med inline next-box"></div>
 <!-- 43 --><div class="info-box-med inline next-box"></div>
 <!-- 44 --><div class="info-box-med inline next-box"></div>
 <!-- 45 --><div class="info-box-med inline next-box"></div>
 <!-- 46 --><div class="info-box-med inline next-box"></div>
 <!-- 47 --><div class="info-box-med inline next-box"></div>
 <!-- 48 --><div class="info-box-med inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NAME -->
        </div>

        <!-- NATIONALITY -->
        <div>
          <div style="position:absolute;left:60px;top:310px">
            <span class="head-2">Citizenship / Nationality:</span>
          </div>

          <!-- INPUT FOR NATIONALITY -->
          <div style="position:absolute;left:62px;top:323px;">
            <span class="input-text">{{ $data['nationality'] }}</span>
          </div>
          <!-- BOXES FOR NATIONALITY -->
          <div style="position:absolute;left:60px;top:320px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
 <!--  5 --><div class="info-box-large inline next-box"></div>
 <!--  6 --><div class="info-box-large inline next-box"></div>
 <!--  7 --><div class="info-box-large inline next-box"></div>
 <!--  8 --><div class="info-box-large inline next-box"></div>
 <!--  9 --><div class="info-box-large inline next-box"></div>
 <!-- 10 --><div class="info-box-large inline next-box"></div>

 <!-- 11 --><div class="info-box-large inline next-box"></div>
 <!-- 12 --><div class="info-box-large inline next-box"></div>
 <!-- 13 --><div class="info-box-large inline next-box"></div>
 <!-- 14 --><div class="info-box-large inline next-box"></div>
 <!-- 15 --><div class="info-box-large inline next-box"></div>
 <!-- 16 --><div class="info-box-large inline next-box"></div>
 <!-- 17 --><div class="info-box-large inline next-box"></div>
 <!-- 18 --><div class="info-box-large inline next-box"></div>
 <!-- 19 --><div class="info-box-large inline next-box"></div>
 <!-- 20 --><div class="info-box-large inline next-box"></div>

 <!-- 21 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NATIONALITY -->
        </div>
        <!-- ADDRESS -->
        <!-- INPUT FOR ADDRESS -->
        <div>
          <div style="position:absolute;left:198px;top:327px;">
            <span class="address-text">{{ $data['address_1'] }}</span>
          </div>
        </div>
        <div>
          <!-- BOXES FOR ADDRESS -->
          <div style="position:absolute;left:195px;top:325px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
 <!--  5 --><div class="info-box-large inline next-box"></div>
 <!--  6 --><div class="info-box-large inline next-box"></div>
 <!--  7 --><div class="info-box-large inline next-box"></div>
 <!--  8 --><div class="info-box-large inline next-box"></div>
 <!--  9 --><div class="info-box-large inline next-box"></div>
 <!-- 10 --><div class="info-box-large inline next-box"></div>

 <!-- 11 --><div class="info-box-large inline next-box"></div>
 <!-- 12 --><div class="info-box-large inline next-box"></div>
 <!-- 13 --><div class="info-box-large inline next-box"></div>
 <!-- 14 --><div class="info-box-large inline next-box"></div>
 <!-- 15 --><div class="info-box-large inline next-box"></div>
 <!-- 16 --><div class="info-box-large inline next-box"></div>
 <!-- 17 --><div class="info-box-large inline next-box"></div>
 <!-- 18 --><div class="info-box-large inline next-box"></div>
 <!-- 19 --><div class="info-box-large inline next-box"></div>
 <!-- 20 --><div class="info-box-large inline next-box"></div>

 <!-- 21 --><div class="info-box-large inline next-box"></div>
 <!-- 22 --><div class="info-box-large inline next-box"></div>
          </div>
          
        </div>

        <!-- COUNTRY OF BIRTH -->
        <div>
          <div style="position:absolute;left:-215px;top:340px">
            <span class="head-2">Country of Birth:</span>
          </div>

          <!-- INPUT FOR NATIONALITY -->
          <div style="position:absolute;left:62px;top:353px;">
            <span class="input-text">{{ $data['birthcountry'] }}</span>
          </div>
          <!-- BOXES FOR NATIONALITY -->
          <div style="position:absolute;left:60px;top:350px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
 <!--  5 --><div class="info-box-large inline next-box"></div>
 <!--  6 --><div class="info-box-large inline next-box"></div>
 <!--  7 --><div class="info-box-large inline next-box"></div>
 <!--  8 --><div class="info-box-large inline next-box"></div>
 <!--  9 --><div class="info-box-large inline next-box"></div>
 <!-- 10 --><div class="info-box-large inline next-box"></div>

 <!-- 11 --><div class="info-box-large inline next-box"></div>
 <!-- 12 --><div class="info-box-large inline next-box"></div>
 <!-- 13 --><div class="info-box-large inline next-box"></div>
 <!-- 14 --><div class="info-box-large inline next-box"></div>
 <!-- 15 --><div class="info-box-large inline next-box"></div>
 <!-- 16 --><div class="info-box-large inline next-box"></div>
 <!-- 17 --><div class="info-box-large inline next-box"></div>
 <!-- 18 --><div class="info-box-large inline next-box"></div>
 <!-- 19 --><div class="info-box-large inline next-box"></div>
 <!-- 20 --><div class="info-box-large inline next-box"></div>

 <!-- 21 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NATIONALITY -->
        </div>


        <!-- ADDRESS  2-->
        <div>
         
          <!-- INPUT FOR ADDRESS  2 -->
          <div style="position:absolute;left:202px;top:355px;">
            <span class="address-text">{{ $data['address_2'] }}</span>

          </div>
          <!-- BOXES FOR ADDRESS  2 -->
          <div style="position:absolute;left:195px;top:352px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
 <!--  5 --><div class="info-box-large inline next-box"></div>
 <!--  6 --><div class="info-box-large inline next-box"></div>
 <!--  7 --><div class="info-box-large inline next-box"></div>
 <!--  8 --><div class="info-box-large inline next-box"></div>
 <!--  9 --><div class="info-box-large inline next-box"></div>
 <!-- 10 --><div class="info-box-large inline next-box"></div>

 <!-- 11 --><div class="info-box-large inline next-box"></div>
 <!-- 12 --><div class="info-box-large inline next-box"></div>
 <!-- 13 --><div class="info-box-large inline next-box"></div>
 <!-- 14 --><div class="info-box-large inline next-box"></div>
 <!-- 15 --><div class="info-box-large inline next-box"></div>
 <!-- 16 --><div class="info-box-large inline next-box"></div>
 <!-- 17 --><div class="info-box-large inline next-box"></div>
 <!-- 18 --><div class="info-box-large inline next-box"></div>
 <!-- 19 --><div class="info-box-large inline next-box"></div>
 <!-- 20 --><div class="info-box-large inline next-box"></div>

 <!-- 21 --><div class="info-box-large inline next-box"></div>
 <!-- 22 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NUMBER OF MONTHS REQUIRED -->
        </div>

        <!-- DATE OF BIRTH -->
        <div>
          <div style="position:absolute;left:-215px;top:368px">
            <span class="head-2">Date of Birth [DD-MMM-YYYY e.g. 01 JAN 1990]:</span>
          </div>

          <!-- INPUT FOR DAY -->
          <div style="position:absolute;left:65px;top:381px;">
            <span class="input-text">{{ $data['bd'] }}</span>
          </div>
          <!-- BOXES FOR DAY -->
          <div style="position:absolute;left:60px;top:378px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
          </div>

          <div style="position:absolute;left:95px;top:388px">
            <div class="dash"></div>
          </div>
          

          <!-- INPUT FOR MONTH -->
          <div style="position:absolute;left:88px;top:381px;">
            <span class="input-text">{{ $data['bmonth'] }}</span>
          </div>

          <!-- BOXES FOR MONTH -->
          <div style="position:absolute;left:85px;top:378px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
          </div>

           <div style="position:absolute;left:160px;top:388px">
            <div class="dash"></div>
          </div>
          
          <!-- INPUT FOR YEAR -->
          <div style="position:absolute;left:108px;top:381px;">
              <span class="input-text">{{ $data['byear'] }}</span>
          </div>

          <!-- BOXES FOR MONTH -->
          <div style="position:absolute;left:105px;top:378px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NATIONALITY -->
        </div>

        <!-- GENDER -->
        <div>
          <div style="position:absolute;left:225px;top:368px">
            <span class="head-2">Gender:</span>
          </div>

          <!-- INPUT FOR MALE -->
          <div style="position:absolute;left:223px;top:381px;">
            @if($data['gender'] == "Male")
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOX FOR MALE -->
          <div style="position:absolute;left:237px;top:378px">
            <span class="head-1">M</span>
          </div>

          <div style="position:absolute;left:220px;top:378px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
          </div>

          <!-- INPUT FOR FEMALE -->
          <div style="position:absolute;left:243px;top:381px;">
            @if($data['gender'] == "Female")
            <span class="input-text">x</span>
            @endif
          </div>

          <!-- BOX FOR FEMALE -->
          <div style="position:absolute;left:260px;top:378px">
            <span class="head-1">F</span>
          </div>
          <!-- END OF BOXES FOR NATIONALITY -->

          <div style="position:absolute;left:240px;top:378px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
          </div>

        </div>

        <!-- ADDRESS 3 -->
        <div>
          <!-- INPUT FOR province -->
          <div style="position:absolute;left:322px;top:380px;">
            <span class="address-text"></span>
          </div>
          <!-- BOXES FOR province -->
          <div style="position:absolute;left:322px;top:380px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
 <!--  5 --><div class="info-box-large inline next-box"></div>
 <!--  6 --><div class="info-box-large inline next-box"></div>
 <!--  7 --><div class="info-box-large inline next-box"></div>
 <!--  8 --><div class="info-box-large inline next-box"></div>
 <!--  9 --><div class="info-box-large inline next-box"></div>
 <!-- 10 --><div class="info-box-large inline next-box"></div>

 <!-- 11 --><div class="info-box-large inline next-box"></div>
 <!-- 12 --><div class="info-box-large inline next-box"></div>
 <!-- 13 --><div class="info-box-large inline next-box"></div>
 <!-- 14 --><div class="info-box-large inline next-box"></div>
 <!-- 15 --><div class="info-box-large inline next-box"></div>
 <!-- 16 --><div class="info-box-large inline next-box"></div>
 <!-- 17 --><div class="info-box-large inline next-box"></div>
 <!-- 18 --><div class="info-box-large inline next-box"></div>
 <!-- 19 --><div class="info-box-large inline next-box"></div>
 <!-- 20 --><div class="info-box-large inline next-box"></div>

 <!-- 21 --><div class="info-box-large inline next-box"></div>
 <!-- 22 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NUMBER OF MONTHS REQUIRED -->
        </div>

                <!-- CIVIL STATUS -->
        <div>
          <div style="position:absolute;left:60px;top:398px">
            <span class="head-2">Civil Status</span>
          </div>

          <!-- INPUT FOR SINGLE -->
          <div style="position:absolute;left:63px;top:410px;">
            @if($data['cstatus'] == "Single")
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOXES FOR SINGLE -->
          <div style="position:absolute;left:60px;top:410px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
            &nbsp;
            <span class="head-2">Single</span>
          </div>

          <!-- INPUT FOR MARRIED -->
          <div style="position:absolute;left:63px;top:430px;">
            @if($data['cstatus'] == "Married")
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOXES FOR MARRIED -->
          <div style="position:absolute;left:60px;top:430px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
            &nbsp;
            <span class="head-2">Married</span>
          </div>

          <!-- INPUT FOR SEPARATED -->
          <div style="position:absolute;left:112px;top:410px;">
            @if($data['cstatus'] == "Separated")
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOXES FOR SEPARATED -->
          <div style="position:absolute;left:110px;top:410px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
            &nbsp;
            <span class="head-2">Separated</span>
          </div>

          <!-- INPUT FOR WIDOWED -->
          <div style="position:absolute;left:112px;top:430px;">
            @if($data['cstatus'] == "Widowed")
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOXES FOR WIDOWED -->
          <div style="position:absolute;left:110px;top:430px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
            &nbsp;
            <span class="head-2">Widowed</span>
          </div>

          <!-- INPUT FOR ANNULLED -->
          <div style="position:absolute;left:172px;top:410px;">
            @if($data['cstatus'] == "Annulled")
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOXES FOR ANNULLED -->
          <div style="position:absolute;left:170px;top:410px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
            &nbsp;
            <span class="head-2">Annulled</span>
          </div>

          <!-- INPUT FOR DIVORCED -->
          <div style="position:absolute;left:172px;top:430px;">
            @if($data['cstatus'] == "Divorced")
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOXES FOR DIVORCED -->
          <div style="position:absolute;left:170px;top:430px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
            &nbsp;
            <span class="head-2">Divorced</span>
          </div>

          <div style="position:absolute;left:250px;top:410px">
            <span class="head-2">Height</span>
          </div>

          <!-- INPUT FOR HEIGHT -->
          <div style="position:absolute;left:286px;top:410px;">
            <span class="input-text">{{$data['height']}}</span>
          </div>

           <div style="position:absolute;left:284px;top:410px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
 <!--  2 --><div class="info-box-small inline next-box"></div>
 <!--  3 --><div class="info-box-small inline next-box"></div>
          </div>

          <div style="position:absolute;left:290px;top:410px">
            <span class="head-2">cm</span>
          </div>

          <div style="position:absolute;left:250px;top:430px">
            <span class="head-2">Weight</span>

          </div>


          <!-- INPUT FOR WEIGHT -->
          <div style="position:absolute;left:286px;top:430px;">
            <span class="input-text">{{$data['weight']}}</span>
          </div>

           <div style="position:absolute;left:284px;top:430px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
 <!--  2 --><div class="info-box-small inline next-box"></div>
 <!--  3 --><div class="info-box-small inline next-box"></div>
          </div>

          <div style="position:absolute;left:290px;top:430px">
            <span class="head-2">kg</span>
          </div>
        </div>

        <!-- MOBILE  -->
        <div>
          <!-- INPUT FOR MOBILE -->
          <div style="position:absolute;left:400px;top:410px;">
            <span class="address-text"></span>
          </div>
          <!-- BOXES FOR MOBILE -->
          <div style="position:absolute;left:400px;top:410px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
 <!--  5 --><div class="info-box-large inline next-box"></div>
 <!--  6 --><div class="info-box-large inline next-box"></div>
 <!--  7 --><div class="info-box-large inline next-box"></div>
 <!--  8 --><div class="info-box-large inline next-box"></div>
 <!--  9 --><div class="info-box-large inline next-box"></div>
 <!-- 10 --><div class="info-box-large inline next-box"></div>

 <!-- 11 --><div class="info-box-large inline next-box"></div>
 <!-- 12 --><div class="info-box-large inline next-box"></div>
 <!-- 13 --><div class="info-box-large inline next-box"></div>
 <!-- 14 --><div class="info-box-large inline next-box"></div>
 <!-- 15 --><div class="info-box-large inline next-box"></div>
 <!-- 16 --><div class="info-box-large inline next-box"></div>
 <!-- 17 --><div class="info-box-large inline next-box"></div>
 <!-- 18 --><div class="info-box-large inline next-box"></div>
 <!-- 19 --><div class="info-box-large inline next-box"></div>
 <!-- 20 --><div class="info-box-large inline next-box"></div>

 <!-- 21 --><div class="info-box-large inline next-box"></div>
 <!-- 22 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NUMBER OF MONTHS REQUIRED -->
        </div>

        <!-- TRAVEL INFORMATION -->
        <div style="position:absolute;left:60px;top:445px">
          <span class="head-1"><b>TRAVEL INFORMATION:</b></span>
        </div>
        <div style="position:absolute;left:60px;top:452px">
          <span class="head-2">TRAVEL DOCUMENT/PASSPORT NUMBER:VALID UNTIL</span>
          <span class="head-3">(DD-MMM-YYYY e.g. 01 JAN 1990):</span>
        </div>

        <div>
          <!-- INPUT FOR PASSPORT NUMBER -->
          <div style="position:absolute;left:65px;top:472px;">
            <span class="input-text">{{ $data['passport'] }}</span>
          </div>
          <!-- BOXES FOR PASSPORT NUMBER -->
          <div style="position:absolute;left:60px;top:470px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
 <!--  5 --><div class="info-box-large inline next-box"></div>
 <!--  6 --><div class="info-box-large inline next-box"></div>
 <!--  7 --><div class="info-box-large inline next-box"></div>
 <!--  8 --><div class="info-box-large inline next-box"></div>
 <!--  9 --><div class="info-box-large inline next-box"></div>
 <!-- 10 --><div class="info-box-large inline next-box"></div>

 <!-- 11 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NUMBER OF MONTHS REQUIRED -->

          <div>
            <div style="position:absolute;left:95px;top:473px;">
              <span class="input-text">{{ $data['pd'] }}</span>
            </div>
            <!-- BOXES FOR DAY -->
            <div style="position:absolute;left:90px;top:470px">
    <!--  1 --><div class="info-box-large inline first-box"></div>
    <!--  2 --><div class="info-box-large inline next-box"></div>
            </div>

            <div style="position:absolute;left:270px;top:480px">
              <div class="dash"></div>
            </div>
            
            <!-- INPUT FOR MONTH -->
            <div style="position:absolute;left:120px;top:473px;">
              <span class="input-text">{{ $data['pmonth'] }}</span>
            </div>

            <!-- BOXES FOR MONTH -->
            <div style="position:absolute;left:115px;top:470px">
    <!--  1 --><div class="info-box-large inline first-box"></div>
    <!--  2 --><div class="info-box-large inline next-box"></div>
    <!--  3 --><div class="info-box-large inline next-box"></div>
            </div>

            <div style="position:absolute;left:330px;top:480px">
              <div class="dash"></div>
            </div>

            <!-- INPUT FOR YEAR -->
            <div style="position:absolute;left:133px;top:473px;">
                <span class="input-text">{{ $data['pyear'] }}</span>
            </div>

            <!-- BOXES FOR MONTH -->
            <div style="position:absolute;left:130px;top:470px">
    <!--  1 --><div class="info-box-large inline first-box"></div>
    <!--  2 --><div class="info-box-large inline next-box"></div>
    <!--  3 --><div class="info-box-large inline next-box"></div>
    <!--  4 --><div class="info-box-large inline next-box"></div>
            </div>
          </div>
        </div>


        <div style="position:absolute;left:60px;top:488px">
          <span class="head-2">Date of Latest Arrival (DD-MMM-YYYY e.g. 01 JAN 1990)</span>
        </div>

        <!-- DATE OF latest arrival -->
        <div>
          <!-- INPUT FOR DAY -->
          <div style="position:absolute;left:65px;top:505px;">
            <span class="input-text">{{ $data['ld'] }}</span>
          </div>
          <!-- BOXES FOR DAY -->
          <div style="position:absolute;left:60px;top:503px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
          </div>

          <div style="position:absolute;left:95px;top:513px">
            <div class="dash"></div>
          </div>
          

          <!-- INPUT FOR MONTH -->
          <div style="position:absolute;left:88px;top:505px;">
            <span class="input-text">{{ $data['lmonth'] }}</span>
          </div>

          <!-- BOXES FOR MONTH -->
          <div style="position:absolute;left:85px;top:503px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
          </div>

           <div style="position:absolute;left:160px;top:513px">
            <div class="dash"></div>
          </div>
          
          <!-- INPUT FOR YEAR -->
          <div style="position:absolute;left:108px;top:505px;">
              <span class="input-text">{{ $data['lyear'] }}</span>
          </div>

          <!-- BOXES FOR MONTH -->
          <div style="position:absolute;left:105px;top:503px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NATIONALITY -->
        </div>

        <div style="position:absolute;left:60px;top:523px">
          <span class="head-1"><b>LATEST TOURIST VISA EXTENSION:</b></span>
        </div>
        <div style="position:absolute;left:60px;top:535px">
          <span class="head-2">Valid Until (DD-MMM-YYYY e.g. 01 JAN 1990)</span>
        </div>

                <!-- DATE OF latest visa ext -->
        <div>
          <!-- INPUT FOR DAY -->
          <div style="position:absolute;left:65px;top:548px;">
            <span class="input-text">{{ $data['ed'] }}</span>
          </div>
          <!-- BOXES FOR DAY -->
          <div style="position:absolute;left:60px;top:545px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
          </div>

          <div style="position:absolute;left:95px;top:555px">
            <div class="dash"></div>
          </div>
          

          <!-- INPUT FOR MONTH -->
          <div style="position:absolute;left:88px;top:548px;">
            <span class="input-text">{{ $data['emonth'] }}</span>
          </div>

          <!-- BOXES FOR MONTH -->
          <div style="position:absolute;left:85px;top:545px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
          </div>

           <div style="position:absolute;left:160px;top:555px">
            <div class="dash"></div>
          </div>
          
          <!-- INPUT FOR YEAR -->
          <div style="position:absolute;left:108px;top:548px;">
              <span class="input-text">{{ $data['eyear'] }}</span>
          </div>

          <!-- BOXES FOR MONTH -->
          <div style="position:absolute;left:105px;top:545px">
 <!--  1 --><div class="info-box-large inline first-box"></div>
 <!--  2 --><div class="info-box-large inline next-box"></div>
 <!--  3 --><div class="info-box-large inline next-box"></div>
 <!--  4 --><div class="info-box-large inline next-box"></div>
          </div>
          <!-- END OF BOXES FOR NATIONALITY -->
        </div>


        <!-- III. MODE OF TRANSACTION -->
          <div style="position:absolute;left:60px;top:563px">
            <span class="head-1"><b>III. MODE OF TRANSACTION</b></span>
          </div>

          <!-- INPUT FOR EXPRESS LANE -->
          <div style="position:absolute;left:63px;top:578px;">
            @if($data['express_regular'] == 'Express')
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOXES FOR EXPRESS LANE -->
          <div style="position:absolute;left:60px;top:578px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
            &nbsp;
            <span class="head-2">Express Lane </span>
          </div>


          <!-- INPUT FOR REGULAR LANE -->
          <div style="position:absolute;left:153px;top:578px;">
            @if($data['express_regular'] == 'Regular')
            <span class="input-text">x</span>
            @endif
          </div>
          <!-- BOXES FOR REGULAR LANE -->
          <div style="position:absolute;left:150px;top:578px">
 <!--  1 --><div class="info-box-small inline first-box"></div>
            &nbsp;
            <span class="head-2">Regular Lane</span>
          </div>
          
        <div>
          <div style="position:absolute;left:500px;top:430px;">
            <span class="head-4"><b> IV. UNDERTAKING</b></span>
          </div>
          <div style="position:absolute;left:450px;top:440px;width:300px;">
            <p class="" style="text-align:justify;font-size:  8px">
              I/We certify that (1) All the information in the application is
              truthful, complete and correct; (2) All documents are
              authentic and were legally obtained from the corresponding
              government agencies or private entities; (3) I/We understand
              that my/our application may be summarily denied if: (a) Any
              statement is false; (b) Any document submitted is falsified; or
              (c) I/We fail to comply with all the BI requirements without
              prejudice to whatever action the BI may take; (4) I/We have not filed this or any similar application before any office of the
              Bureau; and I agree to the type of transaction chosen.
            </p>
          </div>

          <div style="position:absolute;left:450px;top:540px;">
              <div class="address-text">{{ $data['lname'] }}, {{ $data['fname'] }} {{ $data['mname'] }}</div>
          </div>

          <div style="position:absolute;left:653px;top:540px;">
              <div class="address-text">{{$data['issue_date']}}</div>
          </div>


           <!-- SIGNATURE -->
          <div style="position:absolute;left:450px;top:560px;">
            <div class="sign"></div>
          </div>
          
          <!-- SIGNATURE -->
          <div style="position:absolute;left:440px;top:565px;">
            <div class="head-3">Applicant's Signature over Printed Name</div>
          </div>


          <!-- DATE -->
          <div style="position:absolute;left:650px;top:560px;">
            <div class="date"></div>
          </div>

          <div style="position:absolute;left:665px;top:565px;">
            <div class="head-3">Date</div>
          </div>




          <div style="position:absolute;left:551px;top:578px;width:320px;">
            <p style="font-size:9px"><b>IRD04.QF.001 Rev 02 effective 12 July 2019</b></p>
          </div>
        </div>

    </main>
  </body>
</html>