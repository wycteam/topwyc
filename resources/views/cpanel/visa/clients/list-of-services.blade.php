        <div class="m-t-2">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive no-side-padding">
                        <table id="lists" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th class="text-center">@lang('cpanel/clients-page.date')</th>
                                    <th class="text-center">@lang('cpanel/clients-page.detail')</th>
                                    <!-- <th class="text-center">Cost</th> -->
                                    <th class="text-center">@lang('cpanel/clients-page.charge')</th>
                                    <!-- <th class="text-center">Tip</th> -->
                                    <th class="text-center">@lang('cpanel/clients-page.disc')</th>
                                    <th class="text-center">@lang('cpanel/clients-page.status')</th>
                                    <th class="text-center">@lang('cpanel/clients-page.tracking')</th>
                                    <th class="text-center">@lang('cpanel/clients-page.group')</th>
                                    <th class="no-sort"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr is="service-item"
                                  v-for="serv in services"
                                  :serv="serv"
                                  :key="serv.id"
                                >
                                </tr>                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>