@extends('cpanel.layouts.master')

@section('title', $title)

@section('content')

<div id="app" v-cloak>

	<div class="row wrapper border-bottom white-bg page-heading" v-cloak>
        <div class="col-md-12">
            <h2>@lang('cpanel/clients-page.clients')</h2>
            <a href="{{ url('/visa/client/create') }}" class="m-l-3 p-l-1" v-show="selpermissions.addNewClient==1 || setting == 1"><i class="fa fa-user-plus fa-fw" ></i> @lang('cpanel/clients-page.add-new-client')</a>
            <a href="#" data-toggle="modal" data-target="#addTemporaryClientModal" class="m-l-3 p-l-1" v-show="selpermissions.addTemporaryClient==1 || setting == 1">
            	<i class="fa fa-user-plus fa-fw" ></i> 
            	@lang('cpanel/clients-page.add-temp-client')
            </a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated">
        <div class="row">
        	<div class="col-lg-12">

	        	<div style="height:10px; clear:both;"></div>

	        	<div class="ibox float-e-margins">
                    <div class="ibox-content">

                    	<form class="form-inline pull-right" v-cloak v-if="is_master">
                    		<div class="form-group">
						    	<label for="exampleInputName2"><strong>@lang('cpanel/clients-page.branch'):</strong></label>
						    	<select class="form-control" name="branch" v-model="selectedBranch" @change="loadClient">
						    		<option value="0">@lang('cpanel/clients-page.all')</option>
						    		<option v-for="branch in branches" :value="branch.id">@{{ branch.name }}</option>
						    	</select>
						  	</div>
                    	</form>

                    	<div style="height:10px; clear:both;"></div>

                    	<div v-if="loading">
                    		<div class="sk-spinner sk-spinner-wave" style="margin-top:20px;">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                    	</div>

                    	<div v-show="!loading" class="table-responsive">
		                    <table id="lists" class="table table-striped table-bordered table-hover dataTables-example">
			                    <thead>
				                    <tr>
				                        <th style="width:5%">@lang('cpanel/clients-page.id')</th>
				                        <th style="width:30%">@lang('cpanel/clients-page.name')</th>
				                        <!-- <th>Cost</th>
				                        <th>Deposit</th>
				                        <th>Paid</th>
				                        <th>Refund</th>
				                        <th>Discount</th> -->
				                        <th style="width:12%">@lang('cpanel/clients-page.balance')</th>
				                        <th style="width:13%">@lang('cpanel/clients-page.collectables')</th>
				                        <th style="width:20%">@lang('cpanel/clients-page.latest-package')</th>
				                        <th style="width:20%">@lang('cpanel/clients-page.latest-service')</th>
                        				<th style="width:5%" class="no-sort"></th>
				                    </tr>
			                    </thead>
			                    
			                </table>
			        <div class="col-lg-12 pull-left">
                        <span><b>@lang('cpanel/clients-page.total-collectables'): <span id="total-collectables">{{$collect}}</span> </b></span>
                    </div>
			            </div>

                    </div>
                </div>
	        </div>
        </div>

        <modal id='addTemporaryClientModal' size="modal-lg">
			<template slot="modal-title">
		        <h3>@lang('cpanel/clients-page.add-temp-client')</h3>
		    </template>

		    <template slot="modal-body">

		    	<add-temporary-client ref="addtemporaryclientref"></add-temporary-client>

		    </template>
		</modal>

		<modal id='newlyAddedTemporaryClientModal' size="modal-md">
		    <template slot="modal-title">
		        <h3 id="title">@lang('cpanel/clients-page.newly-added-temporary-clients')</h3>
		    </template>

		    <template slot="modal-body">
		        <ul class="list-group">
		            <li class="list-group-item" v-for="temporaryClientId in temporaryClientIds">
		                @lang('cpanel/clients-page.client-id') @{{ temporaryClientId }}
		                <a :href="'/visa/client/' + temporaryClientId" class="btn btn-primary btn-sm pull-right" style="margin-top:-6px;">view-profile
		                    @lang('cpanel/clients-page.view-profile')
		                </a>
		            </li>
		        </ul>
		    </template>
		</modal>
	</div>
</div> <!-- app -->

@endsection

@push('styles')
	{!! Html::style(mix('css/visa/clients/index.css', 'cpanel')) !!}
@endpush

@push('scripts')
	{!! Html::script('components/sweetalert2/dist/sweetalert2.min.js') !!}
    {!! Html::script('cpanel/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cpanel/plugins/inputmask/jquery.inputmask.bundle.js') !!}
	{!! Html::script(mix('js/visa/clients/index.js', 'cpanel')) !!}
@endpush