<legend class="m-t">@lang('cpanel/clients-page.contact-info')</legend>

<div class="row">
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('address.address') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.add')</label>
            {!! Form::hidden('address[id]', null) !!}
            {!! Form::text('address[address]', null, ['id' => 'address', 'class' => 'form-control', 'placeholder' => (($lang)? $lang['enter-add'] : 'Enter Address')]) !!}

            @if ($errors->has('address.address'))
            <span class="help-block">
                {{ $errors->first('address.address') }}
            </span>
            @endif
        </div>
    </div>
<!--     <div class="col-md-4">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.brgy')</label>
            {!! Form::text('address[barangay]', null, ['id' => 'barangay', 'class' => 'form-control', 'placeholder' => 'Enter barangay']) !!}
        </div>
    </div> -->
</div>

<!-- <div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('address.city') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.city')</label>
            {!! Form::text('address[city]', null, ['id' => 'city', 'class' => 'form-control', 'placeholder' => 'Enter city']) !!}

            @if ($errors->has('address.city'))
            <span class="help-block">
                {{ $errors->first('address.city') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('address.province') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.province')</label>
            {!! Form::text('address[province]', null, ['id' => 'province', 'class' => 'form-control', 'placeholder' => 'Enter province']) !!}

            @if ($errors->has('address.province'))
            <span class="help-block">
                {{ $errors->first('address.province') }}
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.zip-code')</label>
            {!! Form::text('address[zip_code]', null, ['id' => 'zip_code', 'class' => 'form-control', 'placeholder' => 'Enter zip code']) !!}
        </div>
    </div>
</div> -->

<div class="row">
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('address.contact_number') ? ' has-error' : '' }}">
            <label>@lang('cpanel/clients-page.cnum-1')</label>
            {!! Form::text('address[contact_number]', null, ['id' => 'contact_number', 'class' => 'form-control', 'placeholder' => (($lang)? $lang['enter-cnum'] : 'Enter Contact Number'), 'maxlength' => '11']) !!}

            @if ($errors->has('address.contact_number'))
            <span class="help-block">   
                {{ $errors->first('address.contact_number') }}
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.cnum-2')</label>
            {!! Form::text('address[alternate_contact_number]', null, ['id' => 'alternate_contact_number', 'class' => 'form-control', 'placeholder' => (($lang)? $lang['enter-cnum'] : 'Enter Contact Number'), 'maxlength' => '11']) !!}
        </div>
    </div>


    <div class="col-md-4">
        <div class="form-group">
            <label>@lang('cpanel/clients-page.branch')</label>
            <select v-model="branch_id" id="branch_id" name="branch_id" class="form-control m-b select-branch select2" v-cloak>
<!--                 <option value="0">No Branch</option> -->
                <option :value="br.id" v-for="br in branches">
                    @{{ br.name }}
                </option>
            </select>
        </div>
    </div>

</div>
