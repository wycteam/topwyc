<div>
	<template v-if="documentLogsDetailsLoading && documentLogsLoading">
		<center>
			<div class="spiner-example">
			    <div class="sk-spinner sk-spinner-wave">
			        <div class="sk-rect1"></div>
			        <div class="sk-rect2"></div>
			        <div class="sk-rect3"></div>
			        <div class="sk-rect4"></div>
			        <div class="sk-rect5"></div>
			    </div>
			</div>
		</center>
	</template>
	<template v-else-if="!documentLogsDetailsLoading && documentLogsDetails.length == 0 && !documentLogsLoading && documentLogs.length == 0">
		<center>
			<h2>@lang('cpanel/clients-page.no-data-available')</h2>
		</center>
	</template>
	<template v-else>
		<document-logs-details :documentlogsdetails="documentLogsDetails"></document-logs-details>

		<div style="height:30px;clear:both;"></div>

		<div id="timeline" class="m-l-md">
		    <document-logs
		        v-for="(log3, index) in documentLogs"
		        :log3="log3"
		        :index="index"
		    >
		    </document-logs>
		</div>
	</template>
</div>