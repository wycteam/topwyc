@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}
    {!! Html::style('cpanel/plugins/select2/select2.min.css') !!}
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/clients-page.edit-client')</h2>
        </div>
    </div>

    <div id="client" class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>@lang('cpanel/clients-page.client-form')</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                       @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{ session()->get('message') }}
                        </div>
                        @endif
                        <form id="form-client" role="form" method="POST" action="{{ route('cpanel.visa.client-update',$client[0]->id) }}">
                        {!! Form::token() !!}
                        <input id="client-id" type="hidden" value="{{$client[0]->id}}">
                        <div class="row">
                            <div class="col-xs-12">

                            <legend>@lang('cpanel/clients-page.personal-info')</legend>
                            <div class="row">
                                <div class="col-md-2 avatar">
                                    <img :src="clientForm.avatar" class='img-responsive img-circle btnUpload center-block' data-toggle='tooltip' title="Upload Display Picture" style="cursor: pointer; max-width: 130px;">
                                    <div class="avatar-uploader hide">
                                      <imgfileupload
                                        data-event="avatarUpload"
                                        data-url="/visa/client/update-avatar/{{$client[0]->id}}?width=300&height=300&type=avatar&resize=true"></imgfileupload>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                <label>@lang('cpanel/clients-page.fname')</label>
                                                <input class="form-control" id="first_name" name="first_name" type="text" placeholder="{{$lang ? $lang['enter-fname']: 'Enter First Name' }}" v-model="clientForm.first_name">

                                                @if ($errors->has('first_name'))
                                                <span class="help-block">
                                                    {{ $errors->first('first_name') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>@lang('cpanel/clients-page.mname')</label>
                                                <input class="form-control" id="middle_name" name="middle_name" type="text" placeholder="{{$lang ? $lang['enter-mname']: 'Enter Middle Name' }}" v-model="clientForm.middle_name">
                                            </div>


                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                <label>@lang('cpanel/clients-page.lname')</label>
                                                <input class="form-control" id="last_name" name="last_name" type="text" placeholder="{{$lang ? $lang['enter-lname']: 'Enter Last Name' }}" v-model="clientForm.last_name">

                                                @if ($errors->has('last_name'))
                                                <span class="help-block">
                                                    {{ $errors->first('last_name') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                                                <label>@lang('cpanel/clients-page.date-of-birth')</label>
                                                <input class="form-control" id="birth_date" name="birth_date" v-model="clientForm.birth_date" placeholder="{{$lang ? $lang['enter-bday']: 'Enter Birthday' }}"v-datepicker type="text"  onkeyup="
												var v = this.value;
												if (v.match(/^\d{4}$/) !== null) {
													this.value = v + '-';
												} else if (v.match(/^\d{4}\-\d{2}$/) !== null) {
													this.value = v + '-';
												}" maxlength="10">

                                                @if ($errors->has('birth_date'))
                                                <span class="help-block">
                                                    {{ $errors->first('birth_date') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                                <label>@lang('cpanel/clients-page.gender')</label>
                                                <select class="form-control" id="gender" name="gender" v-model="clientForm.gender">
                                                    <option value="0">@lang('cpanel/clients-page.select')</option>
                                                    <option value="Male">@lang('cpanel/clients-page.male')</option>
                                                    <option value="Female">@lang('cpanel/clients-page.female')</option>
                                                </select>

                                                @if ($errors->has('gender'))
                                                <span class="help-block">
                                                    {{ $errors->first('gender') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group{{ $errors->has('civil_status') ? ' has-error' : '' }}">
                                                <label>@lang('cpanel/clients-page.cstatus')</label>

                                                <select class="form-control" id="civil_status" name="civil_status" v-model="clientForm.civil_status">
                                                    <option value="0">@lang('cpanel/clients-page.select')</option>
                                                    <option value="Single">@lang('cpanel/clients-page.single')</option>
                                                    <option value="Married">@lang('cpanel/clients-page.married')</option>
                                                    <option value="Widowed">@lang('cpanel/clients-page.widowed')</option>
                                                    <option value="Divorced">@lang('cpanel/clients-page.divorced')</option>
                                                    <option value="Separated">@lang('cpanel/clients-page.separated')</option>
                                                </select>

                                                @if ($errors->has('civil_status'))
                                                <span class="help-block">
                                                    {{ $errors->first('civil_status') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>@lang('cpanel/clients-page.height')</label>
                                                <input class="form-control" id="height" name="height" type="text" placeholder="{{$lang ? $lang['enter-height']: 'Enter Height' }}" v-model="clientForm.height">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>@lang('cpanel/clients-page.weight')</label>
                                                <input class="form-control" id="weight" name="weight" type="text" placeholder="{{$lang ? $lang['enter-weight']: 'Enter Weight' }}" v-model="clientForm.weight">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-4">
                                          <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                                              <label>@lang('cpanel/clients-page.nationality')</label>

                                              <select class="form-control" id="nationality" name="nationality" v-model="clientForm.nationality">
                                                  <option value="American">@lang('cpanel/clients-page.american')</option>
                                                  <option value="British">@lang('cpanel/clients-page.british')</option>
                                                  <option value="Canadian">@lang('cpanel/clients-page.canadian')</option>
                                                  <option value="Chinese">@lang('cpanel/clients-page.chinese')</option>
                                                  <option value="Costarricense">@lang('cpanel/clients-page.costarricense')</option>
                                                  <option value="Cyprus">@lang('cpanel/clients-page.cyprus')</option>
                                                  <option value="Filipino">@lang('cpanel/clients-page.filipino')</option>
                                                  <option value="French">@lang('cpanel/clients-page.french')</option>
                                                  <option value="Indian">@lang('cpanel/clients-page.indian')</option>
                                                  <option value="Indonesian">@lang('cpanel/clients-page.indonesian')</option>
                                                  <option value="Israeli">@lang('cpanel/clients-page.israeli')</option>
                                                  <option value="Irish">@lang('cpanel/clients-page.irish')</option>
                                                  <option value="Italian">@lang('cpanel/clients-page.italian')</option>
                                                  <option value="Japanese">@lang('cpanel/clients-page.japanese')</option>
                                                  <option value="Korean">@lang('cpanel/clients-page.korean')</option>
                                                  <option value="Latvian">@lang('cpanel/clients-page.latvian')</option>
                                                  <option value="Malaysian">@lang('cpanel/clients-page.malaysian')</option>
                                                  <option value="Salvadoran">@lang('cpanel/clients-page.salvadoran')</option>
                                                  <option value="Swedish">@lang('cpanel/clients-page.swedish')</option>
                                                  <option value="Taiwanese">@lang('cpanel/clients-page.taiwanese')</option>
                                                  <option value="Thai">@lang('cpanel/clients-page.thai')</option>
                                                  <option value="Vanuatu">@lang('cpanel/clients-page.vanuatu')</option>    <option value="Vietnamese">@lang('cpanel/clients-page.vietnamese')</option>
                                              </select>
                                              @if ($errors->has('nationality'))
                                              <span class="help-block">
                                                  {{ $errors->first('nationality') }}
                                              </span>
                                              @endif
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group{{ $errors->has('birth_country') ? ' has-error' : '' }}">
                                              <label>@lang('cpanel/clients-page.birth-country')</label>
                                              <select class="form-control" id="birth_country" name="birth_country" v-model="clientForm.birth_country">
                                                  <option value="America">@lang('cpanel/clients-page.america')</option>
                                                  <option value="Canada">@lang('cpanel/clients-page.canada')</option>
                                                  <option value="China">@lang('cpanel/clients-page.china')</option>
                                                  <option value="Costa Rica">@lang('cpanel/clients-page.costa-rica')</option>
                                                  <option value="Cyprus">@lang('cpanel/clients-page.cyprus')</option>
                                                  <option value="El Salvador">@lang('cpanel/clients-page.el-salvador')</option>
                                                  <option value="France">@lang('cpanel/clients-page.france')</option>
                                                  <option value="India">@lang('cpanel/clients-page.india')</option>
                                                  <option value="Indonesia">@lang('cpanel/clients-page.indonesia')</option>
                                                  <option value="Israel">@lang('cpanel/clients-page.israel')</option>
                                                  <option value="Ireland">@lang('cpanel/clients-page.ireland')</option>
                                                  <option value="Italy">@lang('cpanel/clients-page.italian')</option>
                                                  <option value="Japan">@lang('cpanel/clients-page.japan')</option>
                                                  <option value="Korea">@lang('cpanel/clients-page.korea')</option>
                                                  <option value="Latvia">@lang('cpanel/clients-page.latvia')</option>
                                                  <option value="Malaysia">@lang('cpanel/clients-page.malaysia')</option>
                                                  <option value="Philippines">@lang('cpanel/clients-page.philippines')</option>
                                                  <option value="Sweden">@lang('cpanel/clients-page.sweden')</option>
                                                  <option value="Taiwan">@lang('cpanel/clients-page.taiwan')</option>
                                                  <option value="Thailand">@lang('cpanel/clients-page.thailand')</option>
                                                  <option value="United Kingdom">@lang('cpanel/clients-page.united-kingdom')</option>
                                                  <option value="Vanuatu">@lang('cpanel/clients-page.vanuatu')</option>
                                                  <option value="Vietnam">@lang('cpanel/clients-page.vietnam')</option>
                                              </select>
                                              @if ($errors->has('birth_country'))
                                              <span class="help-block">
                                                  {{ $errors->first('birth_country') }}
                                              </span>
                                              @endif
                                          </div>
                                      </div>
                                    </div>
                                </div>
                            </div>


                            <legend class="m-t">@lang('cpanel/clients-page.contact-info')</legend>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label>@lang('cpanel/clients-page.add')</label>
                                        <!-- <input name="address[id]" type="hidden" v-model="clientForm.address.id"> -->
                                        <input class="form-control" id="address" name="address" type="text" placeholder="{{$lang ? $lang['enter-add']: 'Enter Address' }}" v-model="clientForm.address">

                                        @if ($errors->has('address'))
                                        <span class="help-block">
                                            {{ $errors->first('address') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>

                            </div>



                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
                                        <label>@lang('cpanel/clients-page.cnum-1')</label>
                                        <input class="form-control" id="contact_number" name="contact_number" type="text" placeholder="{{$lang ? $lang['enter-cnum']: 'Enter Contact Number' }}" v-model="clientForm.contact_number" maxlength="11">

                                        @if ($errors->has('contact_number'))
                                        <span class="help-block">
                                            {{ $errors->first('contact_number') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('cpanel/clients-page.cnum-2')</label>
                                        <input class="form-control" id="alternate_contact_number" name="alternate_contact_number" type="text" placeholder="{{$lang ? $lang['enter-cnum']: 'Enter Contact Number' }}" v-model="clientForm.alternate_contact_number" maxlength="11">
                                    </div>
                                </div>
                                <div class="col-md-4"  >
                                    <div class="form-group">
                                        <label>@lang('cpanel/clients-page.branch')</label>
                                        <select v-model="clientForm.branch_id" id="branch_id" name="branch_id" class="form-control m-b select-branch" v-cloak>
                            <!--                 <option value="0">No Branch</option> -->
                                            <option :value="br.id" v-for="br in branch">
                                                @{{ br.name }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <legend class="m-t">@lang('cpanel/clients-page.acct-info')</legend>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>@lang('cpanel/clients-page.email')</label>
                                        <input class="form-control" id="email" name="email" type="text" placeholder="{{$lang ? $lang['enter-email']: 'Enter Email' }}" v-model="clientForm.email" @keyup="changeUrl()" autocomplete="off">

                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            {{ $errors->first('email') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('cpanel/clients-page.passport-num')</label>
                                        <input class="form-control" id="passport" name="passport" type="text" placeholder="{{$lang ? $lang['enter-passport']: 'Enter Passport' }}" v-model="clientForm.passport">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('cpanel/clients-page.passport') @lang('cpanel/clients-page.valid-until')</label>
                                        <the-mask v-model="clientForm.passport_exp_date"  id="passport_exp_date"  class="input-sm form-control" name="passport_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('cpanel/clients-page.group')</label>
                                        <select class="form-control select2" id="group_id" name="group_id" v-model="clientForm.group_id">
                                            <option value="0">@lang('cpanel/clients-page.no-group')</option>
                                            <option :value="option.id" v-for="option in groups">
                                                @{{ option.name }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                  <div class="form-group">
                                      <label>@lang('cpanel/clients-page.visa-type')</label>
                                      <select  v-model="clientForm.visa_id" id="visa_type" name="visa_type" class="form-control" @change="checkVisa">
                                          <option value="0">@lang('cpanel/clients-page.select-type')</option>
                                          <option value="9A">9A</option>
                                          <option value="9G">9G</option>
                                          <option value="TRV">TRV</option>
                                          <option value="CWV">CWV</option>
                                      </select>
                                  </div>
                                </div>
                                <template v-if="clientForm.visa_id=='9A'">
                                  <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('cpanel/clients-page.arrival')</label>
                                      <the-mask v-model="clientForm.arrival_date"  id="arrival_date"  class="input-sm form-control" name="arrival_date"  placeholder="YYYY-MM-DD"  mask="####-##-##" @input="firstExp">
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('cpanel/clients-page.first-exp-date')</label>
                                      <the-mask v-model="clientForm.first_exp_date" id="first_exp_date"  class="input-sm form-control" name="first_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##"/>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('cpanel/clients-page.ext-exp-date')</label>
                                      <the-mask  id="service_exp_date" v-model="clientForm.exp_date" class="input-sm form-control" name="service_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##"/>
                                    </div>
                                  </div>
                                </template>
                            <template v-if="clientForm.visa_id=='9G' || clientForm.visa_id=='TRV' || clientForm.visa_id=='CWV'">

                                <div class="col-md-4">
                                  <div class="form-group">
                                      <label>@lang('cpanel/clients-page.exp-date')</label>
                                      <the-mask  id="service_exp_date" v-model="clientForm.exp_date" class="input-sm form-control" name="service_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##"/>
                                  </div>
                                </div>
                            </template>
                            </div>

                            <template v-if="clientForm.visa_id=='9G' || clientForm.visa_id=='TRV'">
                            <div class="row">
                              <!--
                              <div class="col-md-4">
                                <div class="form-group">
                                    <label>ICard</label>
                                    <input type="text" v-model="clientForm.icard" id="icard" name="icard" class="form-control" placeholder="Icard"/>
                                </div>
                              </div>
                            -->
                              <div class="col-md-4">
                                <div class="form-group">
                                    <label>@lang('cpanel/clients-page.icard-issue-date')</label>
                                    <the-mask  id="issue_date" v-model="clientForm.issue_date"  class="input-sm form-control" name="issue_date" placeholder="YYYY-MM-DD"  mask="####-##-##"/>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <label>@lang('cpanel/clients-page.icard-expiration-date')</label>
                                    <the-mask  id="icard_exp_date" v-model="clientForm.icard_exp_date" class="input-sm form-control" name="icard_exp_date"  placeholder="YYYY-MM-DD"  mask="####-##-##">
                                </div>
                              </div>
                            </div>
                            </template>

                            <button type="submit" class="btn btn-sm btn-primary pull-right m-t">
                                <strong>@lang('cpanel/clients-page.save')</strong>
                            </button>
                            <a href="{{ action('Cpanel\ClientsController@client_profile', ['id' => $client[0]->id]) }}" class="btn btn-sm btn-primary pull-right m-t" style="margin-right:10px;">
                                <strong>@lang('cpanel/clients-page.back')</strong>
                            </a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Duplicate Clients List Modal -->
        <div class="modal fade" id="duplicateClientsListModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@lang('cpanel/clients-page.suggested-clients')</h4>
              </div>
              <div class="modal-body">

                <div class="full-height-scroll">
                    <div class="table-responsive">
                        <p class="text-danger">@lang('cpanel/clients-page.sorry-it-looks-like')</p>
                        <table class="table table-striped table-hover">
                            <tbody>
                                <tr v-for="client in clients">
                                    <td class="client-avatar">
                                        <img :alt="client.first_name + ' ' + client.last_name" :src="client.avatar" style="width:40px; height:40px;">
                                    </td>
                                    <td>
                                        <a :href="'/visa/client/' + client.id" class="client-link" target="_blank" style="font-size:15px; display:block; margin-top:8px;">
                                            @{{ client.first_name + ' ' + client.last_name }}
                                        </a>
                                    </td>
                                    <td class="client-status">
                                        <a :href="'/visa/client/' + client.id" class="btn btn-primary btn-sm pull-right" target="_blank" style="margin-top:5px;">
                                            @lang('cpanel/clients-page.see-profile')
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('cpanel/clients-page.close')</button>
              </div>
            </div>
          </div>
        </div> <!-- duplicateClientsListModal -->

    </div>


@endsection

@push('scripts')
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('cpanel/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cpanel/plugins/inputmask/jquery.inputmask.bundle.js') !!}
    {!! Html::script(mix('js/visa/clients/edit.js', 'cpanel')) !!}
@endpush
