<div>
<div v-if="commissionlogs.length>0">
    <div id="timeline" class="m-l-md panel-body">   
        <commission-logs
                    v-for="log3 in commissionlogs"
                    :log3="log3"
                    :key="log3.id"
                >
        </commission-logs>
 

        <div v-show="loading" class="ibox">
            <div class="ibox-content">
                <div class="spiner-example">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div v-else>
        <div class="wrapper wrapper-content panel-body" v-show="!loading">
<!--                 <div class="text-center">
                    <h3 class="font-bold">No commission yet</h3>
              
                </div> -->
            <center>
                <h2>@lang('cpanel/clients-page.no-data-available')</h2>
            </center>
        </div>
        <div v-show="loading" class="ibox">
            <div class="ibox-content">
                <div class="spiner-example">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>

