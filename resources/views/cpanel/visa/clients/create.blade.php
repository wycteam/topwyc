@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('cpanel/plugins/select2/select2.min.css') !!}
@endpush

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-md-12">
            <h2>@lang('cpanel/clients-page.create-client')</h2>
        </div>
    </div>

    <div id="client" class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>@lang('cpanel/clients-page.client-form')</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {!! Form::open(['id'=>'form-client','route' => 'cpanel.visa.client-store', 'method' => 'post']) !!}
                        <div class="row">
                            <div class="col-xs-12">

                                @include('cpanel.visa.clients._personal-info')

                                @include('cpanel.visa.clients._contact-info')

                                @include('cpanel.visa.clients._account-info')

                                <button type="submit" class="btn btn-sm btn-primary pull-right m-t">
                                    <strong>@lang('cpanel/clients-page.save')</strong>
                                </button>

                                <a href="{{ url('/visa/client') }}" class="btn btn-sm btn-primary pull-right m-t" style="margin-right: 10px;">@lang('cpanel/clients-page.back')</a>


                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <!-- Duplicate Clients List Modal -->
        <div class="modal fade" id="duplicateClientsListModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@lang('cpanel/clients-page.suggested-clients')</h4>
              </div>
              <div class="modal-body">

                <div class="full-height-scroll">
                    <div class="table-responsive">
                        <p class="text-danger">@lang('cpanel/clients-page.sorry-it-looks-like')</p>
                        <table class="table table-striped table-hover">
                            <tbody>
                                <tr v-for="client in clients">
                                    <td class="client-avatar">
                                        <img :alt="client.first_name + ' ' + client.last_name" :src="client.avatar" style="width:40px; height:40px;">
                                    </td>
                                    <td>
                                        <a :href="'/visa/client/' + client.id" class="client-link" target="_blank" style="font-size:15px; display:block; margin-top:8px;">
                                            @{{ client.first_name + ' ' + client.last_name }}
                                        </a>
                                    </td>
                                    <td class="client-status">
                                        <a :href="'/visa/client/' + client.id" class="btn btn-primary btn-sm pull-right" target="_blank" style="margin-top:5px;">
                                            @lang('cpanel/clients-page.see-profile')
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('cpanel/clients-page.close')</button>
              </div>
            </div>
          </div>
        </div>

    </div>


@endsection

@push('scripts')
    {!! Html::script('cpanel/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cpanel/plugins/inputmask/jquery.inputmask.bundle.js') !!}
	   {!! Html::script(mix('js/visa/clients/index.js', 'cpanel')) !!}
@endpush
