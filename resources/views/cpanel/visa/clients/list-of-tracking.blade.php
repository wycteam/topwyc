        <div class="container m-t-2">
            <div class="row">
                <div class="col-sm-12">
                    <div class="span12 newspan">

                        <div class="customNavigation item-package">
                            <a class="btn prev pull-left" style="background-color: transparent;">{{ HTML::image('js/owl/arrow left.png') }}</a>
                            <a class="btn next pull-right" style="background-color: transparent;">{{ HTML::image('js/owl/arrow right.png') }}</a>
                        </div>
                        <div id="owl-demo" class="trackings owl-carousel">
                            <div class="item item-fix-width">
                                <a href="javascript:void(0)" v-on:click="addNewPackage" class="item-clickable">
                                    <div class="info-box" style="background-color: #e3e3e3;">
                                        <li class="fa fa-plus fa-4" aria-hidden="true"></li>
                                    </div>
                                </a>
                            </div>
                            <package-item
                              v-for="pack in packages"
                              :pack="pack"
                              :translation="packageTranslation"
                              :key="pack.id"
                            >
                            </package-item>
                        </div>
                    </div>
                </div>
            </div>
        </div>