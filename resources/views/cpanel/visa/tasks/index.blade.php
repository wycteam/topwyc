@extends('cpanel.layouts.master')

@section('title', $title)

@push('styles')
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
@endpush

@section('content')

<div id="app">

	<div class="wrapper wrapper-content animated">
        <div class="row">

        	@include('cpanel.visa.tasks.modals')

            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>@lang('cpanel/todays-tasks-page.reminders')</h5>
                    </div>
                    <div class="ibox-content">

                        <reminders :translation="translation"></reminders>

                    </div>
                </div>
            </div> <!-- col-lg-3 -->

        	<div class="col-lg-9">
	        	<div class="ibox float-e-margins">
                    <div class="ibox-content">
                    	
                        <todays-tasks-v2 ref="todaystasksv2" :translation="translation" :employees="employees"></todays-tasks-v2>

                    </div>
                </div>
	        </div> <!-- col-lg-9 -->

        </div>
	</div>

</div> <!-- app -->

@endsection

@push('scripts')
	{!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
	{!! Html::script(mix('js/visa/tasks/index.js', 'cpanel')) !!}
@endpush