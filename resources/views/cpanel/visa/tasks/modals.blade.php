<modal id='editWhoModal' size="modal-md">

    <template slot="modal-title">
        <h3>Edit</h3>
    </template>

    <template slot="modal-body">

        <edit-who ref="editwhoref" :employees="employees"></edit-who> 

    </template>

</modal>

<modal id='taskHistoryModal' size="modal-md">

    <template slot="modal-title">
        <h3 id="task-history-title"></h3>
    </template>

    <template slot="modal-body">

        <task-history ref="taskhistoryref"></task-history> 

    </template>

</modal>