<!-- Add/Edit Service Modal -->
<modal size="modal-md" class="custom-chosen" id='addEditBranch'>
    <template slot="modal-title">
        <h3 id="title">@{{modalTitle}}</h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <form method="post" class="form-horizontal">
            <div class="form-group" :class="{ 'has-error': frmBranch.errors.has('name') }">
                <label class="control-label">@lang('cpanel/branch-manager-page.branch-name')</label>
                <input class="form-control m-b" id="detail" type="text" v-model="frmBranch.name" name="name" placeholder="{{$enterBranchName}}">
                <p class="help-block" v-if="frmBranch.errors.has('name')" v-text="frmBranch.errors.get('name')"></p>
            </div>
        </form>
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">@lang('cpanel/branch-manager-page.cancel')</button>
        <button class='btn btn-primary' @click="saveAddEditFrm()">@lang('cpanel/branch-manager-page.save')</button>
    </template>
</modal>

<!-- deleteLogPrompt Modal -->
<modal size="modal-sm" id='deleteBranchPrompt'>
    <template slot="modal-title">
        <h3>@lang('cpanel/branch-manager-page.delete-branch')</h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body">
        <p>@lang('cpanel/branch-manager-page.are-you-sure-you-want-to-delete-this-branch')</p>

    </template><!-- end of modal-body -->
    <template slot="modal-footer">
        <button type="button" class="btn btn-w-m btn-primary" @click="deleteBranch()">@lang('cpanel/branch-manager-page.yes')</button>
        <button type="button" data-dismiss="modal" class="btn btn-w-m btn-primary">@lang('cpanel/branch-manager-page.cancel')</button>
    </template>
</modal><!-- end of deleteLogPrompt Modal -->