@extends('cpanel.layouts.master')

@section('title', $title)

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-12">
        <h2>@lang('cpanel/branch-manager-page.branch-manager')</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">@lang('cpanel/branch-manager-page.home')</a>
            </li>
            <li class="active">
                <strong>@lang('cpanel/branch-manager-page.branch-manager')</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated service-box" id="branches">
     @include('cpanel.visa.branches.modals')
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary btn-sm pull-right" @click="changeMode('Add','')"><i class="fa fa-plus fa-fw"></i>@lang('cpanel/branch-manager-page.add-new-branch')</a>
            <div style="height:10px; clear:both;"></div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>@lang('cpanel/branch-manager-page.list-of-branches')</h3>
                </div>
                <div class="ibox-content">
                    <!-- SHOW IF SERVICES ARE EMPTY -->
        <!--             <div class="no-result">
                        <h3>No agents to show.</h3>
                    </div> -->
                    <!-- HIDE TABLE IF EMPTY -->

                    <div class="table-responsive">
                        <table id="lists" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th class="col-lg-1">@lang('cpanel/branch-manager-page.id')</th>
                                    <th class="col-lg-3">@lang('cpanel/branch-manager-page.branch-name')</th>
                                    <th class="col-lg-3">@lang('cpanel/branch-manager-page.no-of-clients')</th>
                                    <th class="col-lg-3">@lang('cpanel/branch-manager-page.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            	
                                    <tr v-for="branch in branches">
                                        <td>@{{branch.id}}</td>
                                        <td>@{{branch.name}}</td>
                                        <td>@{{branch.count_clients}}</td>
                                        <td>
                                            <a @click="changeMode('Edit',branch.id)"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                                            &nbsp;
                                            <a @click="changeMode('Delete',branch.id)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            
                                        </td>
                                    </tr>
                                
                            </tbody>
                        </table>
                    </div><!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    {!! Html::script(mix('js/visa/branches/index.js', 'cpanel')) !!}
@endpush


