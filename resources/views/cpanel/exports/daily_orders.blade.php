<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>WATAF - Daily Orders</title>
	<style>
		table tr td {
			text-align: left;
		}
		thead {
			background-color: #009688;
			color: #ffffff;
			vertical-align: middle;
		}
		tr.buyer-details-header {
			background-color: #e37222;
			color: #fff;
			font-weight: bold;
		}
		tr.order-details-header td:nth-child(n+2) {
			background-color: #fabf8f;
			color: #333;
			font-weight: bold;#fabf8f
		}
	</style>
</head>
<body>

	<table>
		<thead>
		    <tr>
		        <th colspan="27" height="50">WATAF Daily Orders {{ $dateTimeRange }}</th>
		    </tr>
	    </thead>
	    <tbody>
	    	<tr>&nbsp;</tr>
	    	<tr class="buyer-details-header">
	    		<td>ID</td>
	    		<td>BUYER NAME</td>
	    		<td>BUYER ADDRESS</td>
	    		<td>BARANGAY</td>
	    		<td>CITY</td>
	    		<td>PROVINCE</td>
	    		<td>LANDMARK</td>
	    		<td>ZIP CODE</td>
	    		<td>COUNTRY</td>
	    		<td>NAME OF RECEIVER</td>
	    		<td>CONTACT NUMBER</td>
	    		<td>ALTERNATE CONTACT NUMBER</td>
	    		<td colspan="15">&nbsp;</td>
	    	</tr>
	    	@foreach($rows as $row)
	    		<tr>
					<td>{{ $row->user_id }}</td>
					<td>{{ $row->user->full_name }}</td>
					<td>{{ $row->address->address }}</td>
					<td>{{ $row->address->barangay }}</td>
					<td>{{ $row->address->city }}</td>
					<td>{{ $row->address->province }}</td>
					<td>{{ $row->address->landmark }}</td>
					<td>{{ $row->address->zip_code }}</td>
					<td>{{ $row->address->country }}</td>
					<td>{{ $row->address->name_of_receiver }}</td>
					<td>{{ $row->address->contact_number }}</td>
					<td>{{ $row->address->alternate_contact_number }}</td>
					<td colspan="15">&nbsp;</td>
				</tr>

				<tr class="order-details-header">
					<td colspan="2">&nbsp;</td>
					<td>PRODUCT ID</td>
					<td>PRODUCT NAME</td>
					<td>TRACKING NUMBER</td>
					<td>PRICE</td>
					<td>QUANTITY</td>
					<td>WEIGHT</td>
					<td>LENGTH</td>
					<td>WIDTH</td>
					<td>HEIGHT</td>
					<td>SHIPPING FEE</td>
					<td>CHARGE</td>
					<td>VAT</td>
					<td>SUBTOTAL</td>
					<td>COLOR</td>
					<td>SIZE</td>
					<td>STATUS</td>
					<td>SHIPMENT TYPE</td>
					<td>COURIER</td>
					<td>STORE ADDRESS</td>
		    		<td>BARANGAY</td>
		    		<td>CITY</td>
		    		<td>PROVINCE</td>
		    		<td>LANDMARK</td>
		    		<td>ZIP CODE</td>
		    		<td>COUNTRY</td>
				</tr>
				@foreach($row->details as $detail)
					<tr>
						<td colspan="2">&nbsp;</td>
						<td>{{ $detail->product_id }}</td>
						<td>{{ $detail->product->name }}</td>
						<td>{{ $detail->tracking_number }}</td>
						<td>
							{{ ($detail->sale_price > 0) 
								? $detail->sale_price
								: $detail->price_per_item
							}}
						</td>
						<td>{{ $detail->quantity }}</td>
						<td>{{ $detail->product->weight }}</td>
						<td>{{ $detail->product->length }}</td>
						<td>{{ $detail->product->width }}</td>
						<td>{{ $detail->product->height }}</td>
						<td>{{ $detail->shipping_fee }}</td>
						<td>{{ $detail->charge }}</td>
						<td>{{ $detail->vat }}</td>
						<td>{{ $detail->subtotal }}</td>
						<td>{{ $detail->color }}</td>
						<td>{{ $detail->size }}</td>
						<td>{{ $detail->status }}</td>
						<td>{{ $detail->shipment_type }}</td>
						<td>{{ $detail->courier }}</td>
						<td>
							{{ (count($detail->product->store->addresses)) ? $detail->product->store->addresses[0]->address : '' }}
						</td>
						<td>
							{{ (count($detail->product->store->addresses)) ? $detail->product->store->addresses[0]->barangay : '' }}
						</td>
						<td>
							{{ (count($detail->product->store->addresses)) ? $detail->product->store->addresses[0]->city : '' }}
						</td>
						<td>
							{{ (count($detail->product->store->addresses)) ? $detail->product->store->addresses[0]->province : '' }}
						</td>
						<td>
							{{ (count($detail->product->store->addresses)) ? $detail->product->store->addresses[0]->landmark : '' }}
						</td>
						<td>
							{{ (count($detail->product->store->addresses)) ? $detail->product->store->addresses[0]->zip_code : '' }}
						</td>
						<td>
							{{ (count($detail->product->store->addresses)) ? $detail->product->store->addresses[0]->country : '' }}
						</td>
					</tr>
				@endforeach

				<tr>&nbsp;</tr>
				
	    	@endforeach
	    </tbody>
	</table>

</body>
</html>