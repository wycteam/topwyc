@extends('cpanel.layouts.master')

@section('title', 'Minor page')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Stores</h2>
            <ol class="breadcrumb">
                <li class="active">
                    <strong>Active</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-t-lg">
                    <h1>
                        Simple example of second view
                    </h1>
                    <small>Writen in minor.blade.php file.</small>
                </div>
            </div>
        </div>
    </div>
@endsection
