<!-- @extends('cpanel.layouts.master') -->

@section('title', 'Development Logs')

@push('styles')
  {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
  
  {!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.css") !!}
  {!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css") !!}
  {!! Html::style("cpanel/plugins/nouslider/jquery.nouislider.css") !!}
@endpush

@section('content')
<div id="devlogs">
  <div class="row wrapper border-bottom white-bg page-heading">
    @include('cpanel.includes.development-logs.modals')
      <div class="col-lg-9">
          <h2>Development Logs</h2>
          <ol class="breadcrumb">
              <li>
                  <a href="/">Home</a>
              </li>
              <li class="active">
                  <strong>Development Logs</strong>
              </li>
          </ol>
      </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="ibox">
              <div class="ibox-content">
                <div class="row">
              <div class="col-lg-9">
                  <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3><i class="fa fa-info-circle"></i> What's New</h3>
                                    </div>
                                    <div class="panel-body" style="min-height: 211px;">
                                      <h3 v-if="Object.keys(latestUpdate).length != 0">@{{latestUpdate.title}}</h3>
                                      <div v-if="Object.keys(latestUpdate).length != 0"><p v-html="latestUpdate.body"></div>
                      <h3 v-if="Object.keys(latestUpdate).length === 0">No Updates to Show.</h3>
                                    </div>
                                </div>
              </div> 
              <div class="col-lg-3">
                <div class="widget style1 navy-bg" v-if="Object.keys(latestUpdate).length != 0">
                              <div class="row">
                                  <div class="col-xs-3 text-center">
                                      <i class="fa fa-refresh fa-5x"></i>
                                  </div>
                                  <div class="col-xs-9 text-right">
                                      <span> Updated </span>
                                      <h3 class="font-bold">@{{latestUpdate.date}}</h3>
                                  </div>
                              </div>
                        </div>
                        <div class="widget style1 yellow-bg" v-if="Object.keys(latestUpdate).length != 0">
                              <div class="row">
                                  <div class="col-xs-3 text-center">
                                      <i class="fa fa-th-large  fa-5x"></i>
                                  </div>
                                  <div class="col-xs-9 text-right">
                                      <span> Version </span>
                                      <h2 class="font-bold">@{{latestUpdate.version}}</h2>
                                  </div>
                              </div>
                        </div>
                        <div class="row">
                          <div col-lg-12>
                             <div class="btn-group">
                                      <button class="btn btn-primary" type="button"  @click="switchMode('Add','')">Add Update</button>
                                      <button class="btn btn-white" type="button"   v-if="Object.keys(latestUpdate).length != 0" @click="switchMode('Edit',latestUpdate.id)">Edit Log</button>
                                      <button class="btn btn-white" type="button"   v-if="Object.keys(latestUpdate).length != 0" @click="switchMode('Delete',latestUpdate.id)">Delete Log</button>
                                  </div>
                          </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12">
                <div class="ibox collapsed">
                    <div class="ibox-title">
                        <h5 >View Older Updates</h5>
                        <div class="ibox-tools" style="text-align: left !important;">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="scroll_content">
                            <div id="vertical-timeline" class="vertical-container dark-timeline">
                              <!-- insert here -->
                              <old-update-list v-for="item in oldUpdates" :item="item" :key="item.id"></old-update-list>
                          </div>
                        </div>
                    </div>
                  </div>
                </div> 
              </div>
          </div>
        </div>
      </div>
    </div> 
  </div>
</div>      
@endsection
@push('scripts')
  {!! Html::script(mix('js/visa/development-logs/index.js','cpanel')) !!}
  {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
  {!! Html::script("cpanel/plugins/jquery-ui/jui-draggable.min.js") !!}
  {!! Html::script('cpanel/plugins/jasny/jasny-bootstrap.min.js') !!}
@endpush
