<!DOCTYPE html>
<html>
<head>
    @stack('styles')
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'accessToken' => access_token(),
            'isAuthenticated' => Auth::check(),
            'user' => Auth::check() ? Auth::user() : null,
            'message' => session('message') ?: null,
            'data' => session('data') ?: null,
            'base_api_url' => config('app.wataf_api_subdomain'),
            'cpanel_url' => config('app.wataf_cpanel_subdomain'),
            'base_url' => config('app.wataf_domain'),
        ]) !!};
    </script>

    <script src="http://topwyc.test:6001/socket.io/socket.io.js"></script>

</head>
<body>
    <!-- Wrapper-->
    <div id="wrapper">

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Main view  -->
            @yield('content')

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    {!! Html::script('components/toastr/toastr.min.js') !!}
    {!! Html::script('components/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('components/datatables.net-zf/js/dataTables.foundation.min.js') !!}


    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js') !!}
    {!! Html::script('cpanel/plugins/metisMenu/jquery.metisMenu.js') !!}
    {!! Html::script('cpanel/plugins/slimscroll/jquery.slimscroll.min.js') !!}


    {!! Html::script(mix('js/inspinia.js', 'cpanel')) !!}
    {!! Html::script(mix('js/app.js', 'cpanel')) !!}
    <!-- {!! Html::script(mix('js/main.js', 'cpanel')) !!} -->

    @stack('scripts')
</body>
</html>
