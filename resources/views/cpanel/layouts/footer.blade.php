<div class="footer">
    <div class="pull-right">
        @lang('cpanel/footer.version') 1.0
    </div>
    <div>
        <strong>@lang('cpanel/footer.copyright')</strong> WATAF &copy; 2014-2017
    </div>
</div>
