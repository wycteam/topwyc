
<nav id="navigation" class="navbar-default navbar-static-side" role="navigation" v-cloak>
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">

                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" class="img-circle" src="{{ $user->avatar }}" />
                    </span>

                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ $user->full_name }}</strong>

                            </span> <span class="text-muted text-xs block">CEO <b class="caret"></b></span>

                        </span>
                    </a>

                    {{-- @foreach ($user->roles as $r)
                        @if ($r->name == 'master')
                            <span class="text-muted text-xs font-bold">Access Control </span>
                            <input id="toggle-access-control" :value="setting" type="checkbox" data-toggle="toggle" data-size="mini">
                        @endif
                    @endforeach --}}

                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="profile.html">@lang('cpanel/navigation.profile')</a></li>
                        <li><a href="contacts.html">@lang('cpanel/navigation.contacts')</a></li>
                        <li><a href="mailbox.html">@lang('cpanel/navigation.mailbox')</a></li>
                        <li class="divider"></li>
<!--                         <li>
                            <form id="form-logout" action="/logout" method="POST">
                                {{ csrf_field() }}
                                <a onclick="document.getElementById('form-logout').submit()" href="javascript:void(0)"><i class="fa fa-sign-out"></i> Logout</a>
                            </form>
                        </li> -->
                    </ul>
                </div>
                <div class="logo-element">
                    Wataf
                </div>
            </li>

            <li class="{{ isActiveRoute('cpanel.home') }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-th-large"></i> <span class="nav-label">@lang('cpanel/navigation.dashboard')</span>
                </a>
            </li>

            <li class="{{ isActiveRoute('cpanel.visa.tracking') }}" v-if="selpermissions.search == 1">
                <a href="{{ route('cpanel.visa.tracking') }}">
                    <i class="fa fa-search"></i> <span class="nav-label"> @lang('cpanel/navigation.tracking')</span>
                </a>
            </li>

            <li class="{{ isActiveRoute('cpanel.visa.client') }}">
                <a href="{{ route('cpanel.visa.client') }}">
                    <i class="fa fa-user"></i> <span class="nav-label"> @lang('cpanel/navigation.manage-clients')</span>
                </a>
            </li>

            <li class="{{ isActiveRoute('cpanel.visa.group') }}">
                <a href="{{ route('cpanel.visa.group') }}">
                    <i class="fa fa-users"></i> <span class="nav-label"> @lang('cpanel/navigation.manage-groups')</span>
                </a>
            </li>


            <li class="{{ isActiveRoute('cpanel.visa.group') }}">
                <a href="#">
                    <i class="fa fa-folder-open"></i> <span class="nav-label">@lang('cpanel/navigation.client-services')</span>
                    <span class="fa fa-angle-right pull-right" style="margin-top: 5px;"></span>
                </a>
                <ul class="nav nav-second-level collapse {{
                    isInRoute(['cpanel.visa.today-services', 'cpanel.visa.on-process-services'])
                }}">
                <li>
                    <a href="{{ route('cpanel.visa.today-services', ['mode' => 'today'])}}">
                        <i class="fa fa-file"></i> <span class="nav-label">@lang('cpanel/navigation.today-serv')</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cpanel.visa.on-process-services') }}">
                        <i class="fa fa-file"></i> <span class="nav-label">@lang('cpanel/navigation.on-process-serv')</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cpanel.visa.pending-services') }}">
                        <i class="fa fa-file"></i> <span class="nav-label">@lang('cpanel/navigation.pending-serv')</span>
                    </a>
                </li>
                </ul>
            </li>


            <li class="{{ isActiveRoute('cpanel.visa.today-tasks') }}">
                <a href="{{ route('cpanel.visa.today-tasks') }}">
                    <i class="fa fa-file"></i> <span class="nav-label"> @lang('cpanel/navigation.today-task')</span>
                </a>
            </li>


            <li>
                <a href="{{ route('cpanel.visa.events') }}">
                    <i class="fa fa-calendar"></i> <span class="nav-label"> @lang('cpanel/navigation.event-calendar')</span>
                </a>
            </li>


            <li class="{{
                isActiveRoute(['cpanel.visa.report.write-report', 'cpanel.visa.report.read-reports'])
            }}">
                <a href="#">
                    <i class="fa fa-file-pdf-o"></i> <span class="nav-label">@lang('cpanel/navigation.reports')</span>
                    <span class="fa fa-angle-right pull-right" style="margin-top: 5px;"></span>
                </a>
                <ul class="nav nav-second-level collapse {{
                    isInRoute(['cpanel.visa.report.write-report', 'cpanel.visa.report.read-reports'])
                }}">
                <li>
                    <a href="{{ route('cpanel.visa.write-report') }}">
                        <i class="fa fa-edit"></i> <span class="nav-label">@lang('cpanel/navigation.write-reports')</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cpanel.visa.read-reports') }}">
                        <i class="fa fa-eye"></i> <span class="nav-label">@lang('cpanel/navigation.read-reports')</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cpanel.visa.get-daily') }}">
                        <i class="fa fa-calendar-check-o"></i> <span class="nav-label">@lang('cpanel/navigation.daily-cost')</span>
                    </a>
                </li>
                </ul>
            </li>

            <li >
                <a href="#">
                    <i class="fa fa-cubes"></i> <span class="nav-label">@lang('cpanel/navigation.news-events')</span>
                    <span class="fa fa-angle-right pull-right" style="margin-top: 5px;"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                <li>
                    <a href="{{ route('cpanel.visa.news-and-events') }}">
                        <i class="fa fa-paper-plane-o"></i> <span class="nav-label"> @lang('cpanel/navigation.news')</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cpanel.visa.ads') }}">
                        <i class="fa fa-star"></i> <span class="nav-label"> @lang('cpanel/navigation.advertisements')</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cpanel.visa.album') }}">
                        <i class="fa fa-camera"></i> <span class="nav-label">@lang('cpanel/navigation.gallery')</span>
                    </a>
                </li>
                </ul>
            </li>



<!--
       <li>
                <a href="{{ route('cpanel.visa.news-and-events') }}">
                    <i class="fa fa-newspaper-o"></i> <span class="nav-label"> @lang('cpanel/navigation.news')</span>
                </a>
            </li>
            <li>
                <a href="{{ route('cpanel.visa.ads') }}">
                    <i class="fa fa-star"></i> <span class="nav-label"> Advertisements</span>
                </a>
            </li>
            <li>
                <a href="{{ route('cpanel.visa.album') }}">
                    <i class="fa fa-image"></i> <span class="nav-label"> @lang('cpanel/navigation.gallery')</span>
                </a>
            </li>

     <li class="{{
                isActiveRoute(['cpanel.visa.notification.builder', 'cpanel.visa.notification.manager'])
            }}">
                <a href="#">
                    <i class="fa fa-bullhorn"></i> <span class="nav-label">Notifications</span>
                    <span class="fa fa-angle-right pull-right" style="margin-top: 5px;"></span>
                </a>
                <ul class="nav nav-second-level collapse {{
                    isInRoute(['cpanel.visa.notification.builder', 'cpanel.visa.notification.manager'])
                }}">
                <li>
                    <a href="{{ route('cpanel.visa.notification-builder') }}">
                        <i class="fa fa-wrench"></i> <span class="nav-label">Builder</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cpanel.visa.notification-manager') }}">
                        <i class="fa fa-share-alt"></i> <span class="nav-label">Manager</span>
                    </a>
                </li>
                </ul>
            </li> -->
            <li class="{{ isActiveRoute('cpanel.visa.service-manager') }}">
                <a href="#">
                    <i class="fa fa-cog"></i> <span class="nav-label">@lang('cpanel/navigation.service-mngr')</span>
                    <span class="fa fa-angle-right pull-right" style="margin-top: 5px;"></span>
                </a>
                <ul class="nav nav-second-level collapse {{ isActiveRoute('cpanel.visa.service-manager') }}">
                <li>
                    <a href="{{ route('cpanel.visa.service-manager') }}">
                        <i class="fa fa-list"></i> <span class="nav-label">@lang('cpanel/navigation.list-services')</span>
                    </a>
                </li>

                <li v-if="uauth == 1193 || uauth == 1">
                    <a href="{{ route('cpanel.visa.service-profiles') }}">
                        <i class="fa fa-cogs"></i> <span class="nav-label">@lang('cpanel/navigation.service-profile')</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('cpanel.visa.service-documents') }}">
                        <i class="fa fa-file-text"></i> <span class="nav-label">@lang('cpanel/navigation.docs')</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cpanel.visa.client-documents') }}">
                        <i class="fa fa-upload"></i> <span class="nav-label">@lang('cpanel/navigation.up-docs')</span>
                    </a>
                </li>
                </ul>
            </li>

             <li class="{{ isActiveRoute('cpanel.visa.branch-mngr') }}">
                <a href="{{ route('cpanel.visa.branch-mngr') }}">
                    <i class="fa fa-sitemap"></i> <span class="nav-label"> @lang('cpanel/navigation.branch-mngr')</span>
                </a>
            </li>


<!--             @if($user->hasPermission('CPanel Accounts') || $user->hasRole('Master'))
            <li class="{{ isActiveRoute('cpanel.visa.cpanel-accounts') }}" v-if="selpermissions.cPanelAccounts == 1">
                <a href="{{ route('cpanel.visa.cpanel-accounts') }}/"><i class="fa fa-user-circle-o"></i> <span class="nav-label">CPanel Accounts</span></a>
            </li>
            @endif -->





            <li class="{{ isActiveRoute('cpanel.visa.cpanel-accounts')}} ">
                <a href="#">
                    <i class="fa fa-universal-access"></i> <span class="nav-label">@lang('cpanel/navigation.accts-mngr')</span>
                    <span class="fa fa-angle-right pull-right" style="margin-top: 5px;"></span>
                </a>
                <ul class="nav nav-second-level collapse {{ isActiveRoute('cpanel.visa.cpanel-accounts') }}">

                    <li v-if="is_master  || uauth == 7">
                        <a href="{{ route('cpanel.visa.cpanel-accounts') }}">
                            <i class="fa fa-user-circle-o"></i> <span class="nav-label">@lang('cpanel/navigation.cpanel-accts')</span>
                        </a>
                    </li>


<!--                     <li class="{{
                        isActiveRoute(['cpanel.visa.notification.builder', 'cpanel.visa.notification.manager'])
                    }}" v-if="selpermissions.employees == 1">
                            <a href="{{ route('cpanel.emp.emp-attendance') }}">
                                <i class="fa fa-calendar"></i> <span class="nav-label">@lang('cpanel/navigation.attendance')</span>
                            </a>
                    </li> -->

                    <li>
                        <a href="{{ route('cpanel.visa.agents') }}">
                            <i class="fa fa-handshake-o"></i> <span class="nav-label">@lang('cpanel/navigation.shops')</span>
                        </a>
                    </li>
                    <li class="{{ isActiveRoute('cpanel.visa.access-control') }}" v-if="selpermissions.accessControl == 1 || setting == 1">
                        <a href="{{ route('cpanel.visa.access-control') }}/"><i class="fa fa-key"></i> <span class="nav-label">@lang('cpanel/navigation.access-control')</span></a>
                    </li>
                </ul>
            </li>

            <!--
            <li class="{{ isActiveRoute('cpanel.visa.financing') }}" >
                <a href="{{ route('cpanel.visa.financing') }}/"><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Financing</span></a>
            </li>
            -->
            <li class="{{ isActiveRoute('cpanel.visa.nfinancing') }}" v-if="is_master  || uauth == 7 || uauth == 2 || uauth == 10552">
                <a target="_blank" href="{{ route('cpanel.visa.nfinancing') }}/"><i class="fa fa-rub"></i> <span class="nav-label">@lang('cpanel/navigation.financing')</span></a>
            </li>
            <li class="{{ isActiveRoute('cpanel.visa.payroll') }}" v-if="is_master || uauth == 7">
                <a target="_blank" href="{{ route('cpanel.visa.payroll') }}/"><i class="fa fa-suitcase"></i> <span class="nav-label">Payroll</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.visa.dev-logs') }}" v-if="selpermissions.developmentLogs == 1 || setting == 1">
                <a href="{{ route('cpanel.visa.dev-logs') }}/"><i class="fa fa-list-alt"></i> <span class="nav-label">@lang('cpanel/navigation.dev-logs')</span></a>
            </li>

            <li class="{{isActiveRoute(['cpanel.visa.notification.builder', 'cpanel.visa.notification.manager'])}}" v-if="is_master  || uauth == 7">
                <a href="{{ route('cpanel.emp.emp-attendance') }}"><i class="fa fa-calendar"></i> <span class="nav-label">@lang('cpanel/navigation.attendance')</span></a>
            </li>

            <!-- <li class="{{ isActiveRoute('cpanel.messages.index') }} m-t-2 p-t-2" >
                <a href="{{ 'http://'.config('app.wataf_domain').'/apk/wycgrouptracker.apk' }}" target="_blank" style="text-align: center">Android Test APK<img src="{{ 'http://'.config('app.wataf_domain').'/images/android.png' }}" width="165px"></a> -->



            <!-- <li class="{{ isActiveRoute('cpanel.messages.index') }}">
                <a href="{{ route('cpanel.messages.index') }}/"><i class="fa fa-envelope"></i> <span class="nav-label">Messages</span></a>

            </li>

            <!-- <li class="{{ isActiveRoute('cpanel.stores') }}">
                <a href="{{ url('/stores') }}"><i class="fa fa-institution"></i> <span class="nav-label">Stores</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.users.index') }}">
                <a href="{{ route('cpanel.users.index') }}"><i class="fa fa-users"></i> <span class="nav-label">Users</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.products') }}">
                <a href="{{ url('/products') }}"><i class="fa fa-cubes"></i> <span class="nav-label">Products</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.categories') }}">
                <a href="{{ url('/categories') }}"><i class="fa fa-sitemap"></i> <span class="nav-label">Categories</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.sales') }}">
                <a href="{{ url('/sales') }}"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Sales/Purchase</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.shipments') }}">
                <a href="{{ url('/shipments') }}"><i class="fa fa-truck"></i> <span class="nav-label">Shipments</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.feedback') }}">
                <a href="{{ url('/feedback') }}"><i class="fa fa-files-o"></i> <span class="nav-label">Feedback</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.notifications.index') }}">
                <a href="{{ route('cpanel.notifications.index') }}"><i class="fa fa-globe"></i> <span class="nav-label">Notifications</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.ewallet') }}">
                <a href="{{ url('/ewallet') }}"><i class="fa fa-google-wallet"></i> <span class="nav-label">E-wallet</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.pool') }}">
                <a href="{{ url('/ewallet-pool') }}"><i class="fa fa-gg-circle"></i> <span class="nav-label">E-wallet Pool</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.documents') }}">
                <a href="{{ url('/documents') }}"><i class="fa fa-database"></i> <span class="nav-label">Account Verification</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.customer-service.index') }}">
                <a href="{{ route('cpanel.customer-service.index') }}"><i class="fa fa-phone"></i> <span class="nav-label">Customer Service</span></a>
            </li>

            <li class="{{ isActiveRoute('cpanel.faq') }}">
                <a href="{{ url('/faq') }}"><i class="fa fa-question"></i> <span class="nav-label">FAQ</span></a>
            </li> -->
        </ul>

    </div>
</nav>
