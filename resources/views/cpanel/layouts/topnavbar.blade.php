<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header" style="display:block">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <div class="navbar-header" style="width: 300px;">
          <div id="search" class="navbar-form-custom">
              <div class="form-group">
                  <form onsubmit="return false;" autocomplete="off">
                      <input style="border-right: 1px solid #e6e4e4;width: 300px;" type="text" autocomplete="off" placeholder="@lang('cpanel/header.enter-client-name-cp')" class="form-control typeahead" name="top-search" id="top-search" />
                  </form>
              </div>
          </div>
        </div>
        <div class="navbar-header" style="width: 300px;">
            <div id="searchgroup" class="navbar-form-custom">
                <div class="form-group" >
                    <form onsubmit="return false;" autocomplete="off">
                        <input style="border-right: 1px solid #e6e4e4;width: 300px;" type="text" autocomplete="off" placeholder="@lang('cpanel/header.enter-group-name-leader-name-cp')" class="form-control typeahead" name="group-search" id="group-search" />
                    </form>
                </div>
            </div>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">@lang('cpanel/header.welcome-to-visa-cpanel')</span>
            </li>
            <li id="inbox" class="dropdown">
                <a @click="markAllAsSeen" class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i> <span class="label label-primary" v-cloak>@{{ count }}</span>
                </a>
                <div class="dropdown-menu panel panel-notification">
                    <div class="panel-heading">
                        <strong>@lang('cpanel/header.messages')</strong>
                    </div>
                    <div class="panel-body">
                        <ul v-if="notifications.length">
                            <li is="wyc-inbox"
                                v-for="notification in notifications"
                                :notification="notification"
                                :key="notification.id">
                            </li>
                        </ul>
                        <h5 v-else class="text-center">@lang('cpanel/header.no-new-messages')</h5>
                    </div>
                    <div class="panel-footer text-center">
                        <a href="/messages/">
                            <strong>@lang('cpanel/header.view-all-messages') <i class="fa fa-angle-right"></i></strong>
                        </a>
                    </div>
                </div>
            </li>
            <li id="notifications" class="dropdown">
                <a @click="markAllAsSeen" class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i> <span class="label label-primary" v-cloak>@{{ count }}</span>
                </a>
                <div class="dropdown-menu panel panel-notification">
                    <div class="panel-heading">
                        <strong>@lang('cpanel/header.notifications')</strong>
                    </div>
                    <div class="panel-body">
                        <ul v-if="notifications.length">
                            <li is="wyc-notification"
                                v-for="notification in notifications"
                                :notification="notification"
                                :key="notification.id">
                            </li>
                        </ul>
                        <h5 v-else class="text-center">@lang('cpanel/header.no-new-notifications')</h5>
                    </div>
                    <div class="panel-footer text-center">
                        <a href="/notifications">
                            <strong>@lang('cpanel/header.view-all-notifications') <i class="fa fa-angle-right"></i></strong>
                        </a>
                    </div>
                </div>
            </li>
            <li id="locale">
                <locale></locale>
            </li>
            <li>
                <form id="form-logout" action="/logout" method="POST">
                    {{ csrf_field() }}
                    <a onclick="document.getElementById('form-logout').submit()" href="javascript:void(0)"><i class="fa fa-sign-out"></i> @lang('cpanel/header.logout')</a>
                </form>
            </li>

            {{-- <li>
                <a class="right-sidebar-toggle">
                    <i class="fa fa-tasks"></i>
                </a>
            </li> --}}
        </ul>
    </nav>
</div>
