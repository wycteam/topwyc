<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta id="token"  name="csrf-token" content="{{ csrf_token() }}">
    <meta name="locale-lang" content="{{ Cookie::get('locale') }}">
    
    <title>@yield('title')</title>

    {{-- Vendor --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" integrity="sha256-j+P6EZJVrbXgwSR5Mx+eCS6FvP9Wq27MBRC/ogVriY0=" crossorigin="anonymous" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    {{-- /Vendor --}}

    {!! Html::style('cpanel/plugins/dropzone/dropzone.css') !!}

    {!! Html::style('components/toastr/toastr.min.css') !!}
    {!! Html::style(mix('css/inspinia.css', 'cpanel')) !!}
    {!! Html::style(mix('css/main.css', 'cpanel')) !!}
    {!! Html::style('components/datatables.net-zf/css/dataTables.foundation.min.css') !!}
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css') !!}
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css') !!}
    {!! Html::style('https://unpkg.com/vue-multiselect@2.0.0/dist/vue-multiselect.min.css') !!}
    {!! Html::style('https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css') !!}

    <!-- Sweet Alert -->
    {!! Html::style('components/sweetalert2/dist/sweetalert2.min.css') !!}


    @stack('styles')

    <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        'accessToken' => access_token(),
        'isAuthenticated' => Auth::check(),
        'user' => Auth::check() ? Auth::user() : null,
        'message' => session('message') ?: null,
        'data' => session('data') ?: null,
        // 'rates' => session('rates') ?: null,
        'base_api_url' => config('app.wataf_api_subdomain'),
        'cpanel_url' => config('app.wataf_cpanel_subdomain'),
        'base_url' => config('app.wataf_domain'),
        'language' => config('app.locale'),
    ]) !!};




    </script>

    @if (Auth::check())
    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
    @endif
</head>
<body>
    <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('cpanel.layouts.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('cpanel.layouts.topnavbar')

            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('cpanel.layouts.footer')

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

    {{-- Vendor --}}
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    {{-- /Vendor --}}
    <!-- {!! Html::script('cpanel/plugins/summernote/summernote.min.js') !!} -->

    {!! Html::script('components/toastr/toastr.min.js') !!}
    {!! Html::script('components/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('components/datatables.net-zf/js/dataTables.foundation.min.js') !!}

    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js') !!}

    {!! Html::script('cpanel/plugins/metisMenu/jquery.metisMenu.js') !!}
    {!! Html::script('cpanel/plugins/slimscroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('cpanel/plugins/pace/pace.min.js') !!}
    {!! Html::script('cpanel/plugins/footable/footable.all.min.js') !!}

    {!! Html::script('https://unpkg.com/vue-multiselect@2.0.0') !!}
    <!-- Sweet Alert -->
    {!! Html::script('components/sweetalert2/dist/sweetalert2.min.js') !!}
    {!! Html::script('https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js') !!}


    {!! Html::script(mix('js/inspinia.js', 'cpanel')) !!}
    {!! Html::script(mix('js/app.js', 'cpanel')) !!}
    {!! Html::script(mix('js/main.js', 'cpanel')) !!}

    <!-- {!! Html::script(mix('js/notification.js', 'cpanel')) !!} -->
    {!! Html::script(mix('js/inbox.js', 'cpanel')) !!}
    
    @stack('scripts')
</body>
</html>
