@extends('cpanel.layouts.master')

@section('title', 'Customer Service')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Customer Service</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="active">
                <strong>Customer Service</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div id="customer-service" class="ibox">
                <div class="ibox-title">
                    <h5>Customer Issues</h5>
                </div>
                <div class="ibox-content">
                    <div class="row m-b-sm m-t-sm">
                        <div class="col-md-1">
                            <button @click="getIssues" type="button" id="loading-example-btn" class="btn btn-white btn-sm"><i class="fa fa-refresh"></i> Refresh</button>
                        </div>
                        <div class="col-md-11">
                            <div class="input-group">
                                <input v-model="search" type="text" placeholder="Search" class="input-sm form-control">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-sm btn-primary"> Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <paginate class="pull-right"
                                :page-count="pageCount"
                                :container-class="'pagination'"
                                :click-handler="clickCallback">
                            </paginate>
                        </div>
                    </div>

                    <div class="project-list">
                        <table class="table table-hover">
                            <tbody>
                                <tr is="wyc-issue"
                                    v-for="issue in issues"
                                    :issue="issue"
                                    :key="issue.id">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<style>
    .project-people > img {
        margin-left: 5px;
    }
</style>
@endpush

@push('scripts')
{!! Html::script(mix('js/pages/customer-service/index.js', 'cpanel')) !!}
@endpush
