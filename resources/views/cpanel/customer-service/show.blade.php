@extends('cpanel.layouts.master')

@section('title', 'Customer Service')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Customer Service</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/customer-service">Customer Service</a>
            </li>
            <li class="active">
                <strong>{{ $issue->subject }}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div id="customer-service" class="ibox">
                <div class="ibox-content">

                    <div class="row">
                        <div class="col-lg-5" id="cluster_info">
                            <div class="m-b-md">
                            <a @click.prevent="edit" href="#" class="btn btn-warning btn-xs pull-right">Edit</a>
                                <!-- <h2>{{ $issue->subject }}</h2> -->
                            </div>
                            <div style="height:10px; clear:both;"></div>
                            <div class="table-responsive">
                                <table class="table convo-info">
                                    <tr>
                                        <td class="text-right">Subject :</td>
                                        <td><b>{{ ucfirst($issue->subject) }}</b></td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Status:</td>
                                        <td>{{ ucfirst($issue->status) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Ticket Number:</td>
                                        <td>{{ $issue->issue_number }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Created by:</td>
                                        <td>{{ $issue->user->full_name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Messages:</td>
                                        <td>{{ $issue->chat_room ? $issue->chat_room->messages->count() : 0 }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Created on:</td>
                                        <td>{{ $issue->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Participants:</td>
                                        <td class="project-people">
                                            @if ($issue->chat_room)
                                                @foreach ($issue->chat_room->users as $user)
                                                    <a href="#" class="pull-left">
                                                        <img alt="image" class="img-circle" src="{{ $user->avatar }}" data-toggle="tooltip" title="{{ $user->first_name.' '.$user->last_name  }}">
                                                    </a>
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Body:</td>
                                        <td>{{ $issue->body }}</td>
                                    </tr>
                                    <tr v-if="details != null && details.file_image.length > 0">
                                        <td class="text-right">Photos:</td>
                                        <td class="row">
                                            <div class="col-md-6" v-for="(image, index) in details.file_image">
                                                <a :href="'/storage/'+image.file" :data-lightbox="'image-'+index">
                                                    <img :src="'/storage/'+image.file" class="img-thumbnail img-responsive">
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr v-if="details != null && details.file_video.length > 0">
                                        <td class="text-right">Videos:</td>
                                        <td class="row">
                                            <div class="col-md-6" v-for="(video, index) in details.file_video">
                                                <a :href="'/storage/'+video.file" data-toggle="lightbox" data-gallery="video-evidence" data-type="video" data-wrapping="false" :id="'video-evidence-' + index" @click="showVideoEvidenceModal($event, index)">
                                                    <video>
                                                        <source :src="'/storage/'+video.file">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        @if (!$issue->chat_room)
                            <div><button  @click="createConvo" class="btn btn-info " type="button"><i class="fa fa-comments-o"></i> Create Conversation</button></div>
                        @endif
                        </div> 
                        <!-- end col-lg-5 -->

                        <div class="col-lg-7" style="border-left: 1px solid rgb(191, 191, 191); min-height: 300px;">
                            <div class="panel blank-panel">
                                <div class="panel-heading">
                                    <div class="panel-options">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab-1" data-toggle="tab">Conversation</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab-1">
                                            <div class="feed-activity-list">
                                            @if ($issue->chat_room && $issue->chat_room->messages->count())
                                                <?php $userId = 0; ?>

                                                @foreach ($issue->chat_room->messages as $message)
                                                    <div class="feed-element media" style="border-bottom: none; padding-bottom:0px; {{ 
                                                            ($userId != $message->user->id && $userId != 0) 
                                                            ? 'border-top:1px solid #e7eaec; padding-top: 15px;' 
                                                            : (($userId == 0) 
                                                                ? 'margin-top:10px;' 
                                                                : 'margin-top:-30px;') 
                                                        }}">
                                                        
                                                        <div class="media-left">
                                                            <a href="#" class="pull-left" style="{{ ($userId == $message->user->id) ? 'opacity:0;' : '' }}">
                                                                <img alt="image" class="img-circle" src="{{ $message->user->avatar }}">
                                                            </a>
                                                        </div>

                                                        <div class="media-body">
                                                            @if($userId != $message->user->id)
                                                                <strong>{{ $message->user->first_name.' '.$message->user->last_name }}</strong>
                                                                <!-- <br>
                                                                <small class="text-muted">{{ $message->created_at }}</small> -->
                                                            @else
                                                                <br>
                                                            @endif
                                                            <div class="well wellmsgs" data-placement="left" data-toggle="tooltip" title="{{ $message->created_at }}">
                                                                {{ $message->body }}
                                                                <!-- <br>
                                                                <small class="text-muted">
                                                                    {{ $message->created_at }}
                                                                </small> -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php  $userId = $message->user->id; ?>
                                                @endforeach


                                            @else
                                                <h4 class="text-center">No conversation</h4>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- end row  -->
                </div>

                <wyc-modal id="issue-modal">
                    <template slot="modal-title">
                        <i class="fa fa-ticket"></i> {{ $issue->issue_number }}
                    </template>

                    <template slot="modal-body">
                        <form method="POST" action="/customer-service" @submit.prevent="submit">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select v-model="form.status" class="form-control">
                                            <option value="open">Open</option>
                                            <option value="closed">Closed</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Subject</label>
                                <input v-model="form.subject" type="email" class="form-control" placeholder="Enter subject">
                            </div>

                            <div class="form-group">
                                <label>Body</label>
                                <textarea v-model="form.body" class="form-control" placeholder="Enter something..." rows="3"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Add to Conversation</label>

                                <multiselect
                                    v-model="selectedSupports"
                                    :options="supportUsers"
                                    :multiple="true"
                                    {{-- :close-on-select="false" --}}
                                    :clear-on-select="false"
                                    :hide-selected="true"
                                    placeholder="Select a user"
                                    label="name_if_support"
                                    track-by="id">
                                </multiselect>
                            </div>
                        </form>
                    </template>

                    <template slot="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button @click="submit" type="button" class="btn btn-primary">Save</button>
                    </template>
                </wyc-modal>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
{!! Html::style('https://unpkg.com/vue-multiselect@2.0.0/dist/vue-multiselect.min.css') !!}

<style>
    .project-people > a {
        margin-right: 5px;
    }
    .wellmsgs{
        margin-bottom: 0px !important;
        padding: 6px 10px !important;
    }
</style>
@endpush

@push('scripts')
{!! Html::script(mix('js/pages/customer-service/show.js', 'cpanel')) !!}
@endpush
