@extends('cpanel.layouts.master')

@section('title', 'Products List')

@push('styles')

@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Products</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Products</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content" id="productpage">
        @include('cpanel.product.modals')
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <div class="tabs-container">
                        <h1 class="pull-right m-r-lg m-t-lg dt-title"><b>List of Products</b></h1>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1">
                                    <div class="ibox-title">
                                        <span class="label label-success">Active</span>
                                        <h3>Products</h3>
                                    </div>
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-2">
                                    <div class="ibox-title">
                                        <span class="label label-warning">Deactivated</span>
                                        <h3>Products</h3>
                                    </div>
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-3">
                                    <div class="ibox-title">
                                        <span class="label label-primary">Not Searchable</span>
                                        <h3>Products</h3>
                                    </div>
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-4">
                                    <div class="ibox-title">
                                        <span class="label label-info">Drafts</span>
                                        <h3>Products</h3>
                                    </div>
                                </a>
                            </li>                            
                            <li class="">
                                <a data-toggle="tab" href="#tab-5">
                                    <div class="ibox-title">
                                        <span class="label label-danger">Deleted</span>
                                        <h3>Products</h3>
                                    </div>
                                </a>
                            </li>                        
                        </ul>
                        <div class="tab-content ">
                            <div id="tab-1" class="tab-pane active ibox">
                                 <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-products" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Product Name</th>
                                                        <th>Product Owner</th>
                                                        <th>Stocks</th>
                                                        <th>Sold Count</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="product in products" :item="product" :key="product.id" v-if="product.is_active==1 && product.deleted_at==null && product.is_draft == 0">
                                                        @include('cpanel.product.include')
                                                        <td width="25%">
                                                            <!--<a href="#" class="btn btn-success btn-outline btn-sm" @click='upproduct(product.id,"promote")'><i class="fa fa-certificate" ></i> Promote </a> -->
                                                            <a href="#" class="btn btn-warning btn-outline btn-sm" @click='showDeacDialog(product)'><i class="fa fa-toggle-on"></i> Deactivate </a>
                                                            <a href="#" class="btn btn-danger btn-outline btn-sm" @click='showRemoveDialog(product)'><i class="fa fa-trash" ></i> Delete </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane ibox">
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-products" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Product Name</th>
                                                        <th>Product Owner</th>
                                                        <th>Stocks</th>
                                                        <th>Sold Count</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="product in products" :item="product" :key="product.id" v-if="product.is_active==0 && product.deleted_at==null">
                                                        @include('cpanel.product.include')
                                                        <td width="25%">
                                                            <a href="#" class="btn btn-success btn-outline btn-sm" @click='upproduct(product.id,"activate")'><i class="fa fa-toggle-on"></i> Activate </a>
                                                            <a href="#" class="btn btn-danger btn-outline btn-sm" @click='showRemoveDialog(product)'><i class="fa fa-trash"></i> Delete </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="tab-3" class="tab-pane ibox">
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-products" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Product Name</th>
                                                        <th>Product Owner</th>
                                                        <th>Stocks</th>
                                                        <th>Sold Count</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="product, index in products" :item="product" :key="product.id" v-if="product.is_searchable == 0">
                                                        @include('cpanel.product.include')
                                                        <td width="25%">
                                                            <a href="#" class="btn btn-success btn-outline btn-sm" @click='showRestoreDialog(product)'><i class="fa fa-recycle"></i> Restore </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            <div id="tab-4" class="tab-pane ibox">
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-products" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Product Name</th>
                                                        <th>Product Owner</th>
                                                        <th>Stocks</th>
                                                        <th>Sold Count</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="product, index in products" :item="product" :key="product.id" v-if="product.deleted_at!=null">
                                                        @include('cpanel.product.include')
                                                        <td width="25%">
                                                            <a href="#" class="btn btn-success btn-outline btn-sm" @click='showRecycleDialog(product)'><i class="fa fa-recycle"></i> Restore </a>
                                                            <a href="#" class="btn btn-danger btn-outline btn-sm" @click='showPermaDialog(product,index)'><i class="fa fa-trash"></i> Delete Permanently</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            <div id="tab-5" class="tab-pane ibox">
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dt-products" >
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Product Name</th>
                                                        <th>Product Owner</th>
                                                        <th>Stocks</th>
                                                        <th>Sold Count</th>
                                                        <!-- <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="product, index in products" :item="product" :key="product.id" v-if="product.is_draft==1">
                                                        @include('cpanel.product.include')
                                                        
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {!! Html::script(mix('js/products.js','cpanel')) !!}
    {!! Html::script('cpanel/plugins/dataTables/datatables.min.js') !!}
@endpush
