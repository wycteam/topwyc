<td class="project-status text-center" width="5%">
    @{{product.id}}
</td>
<td class="project-title text-left" width="40%">
    <a :href="main_url+/product/+product.slug" target="_blank">@{{product.name}}</a>
    <br/>
    <small>Created @{{product.created_at.date | invoiceFormat}}</small>
</td>
<td class="project-title text-left" width="20%">
    <a :href="/users/+product.user_id" target="_blank">@{{product.first_name}} @{{product.last_name}}</a>
    <br/>
    <span class="label label-primary" v-if="product.is_verified">verified</span>
    <span class="label label-primary" v-else>not verified</span>
</td>
<td class="text-center" width="5%">
   @{{ product.stock }}
</td>
<td class="text-center" width="5%">
   @{{ product.sold_count }}
</td>