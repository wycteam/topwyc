<dialog-modal id='dialogRemove'>
	<template slot="modal-title">
        Are you sure you want to delete this product?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selproduct.main_image" width="50"></i>
            <h2>
                <b>@{{selproduct.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='deleteproduct(selproduct.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogDeac'>
	<template slot="modal-title">
        Are you sure you want to deactivate this product?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selproduct.main_image" width="50"></i>
            <h2>
                <b>@{{selproduct.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='deacproduct(selproduct.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogDemote'>
	<template slot="modal-title">
        Are you sure you want to demote this product?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selproduct.main_image" width="50"></i>
            <h2>
                <b>@{{selproduct.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='demoteproduct(selproduct.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogRestore'>
    <template slot="modal-title">
        Are you sure you want this product be searchable again?
    </template>
    <template slot="modal-body">
        <div class="payment-card">
            <i><img :src="selproduct.main_image" width="50"></i>
            <h2>
                <b>@{{selproduct.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
        
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='restoreproduct(selproduct.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogRecycle'>
	<template slot="modal-title">
        Are you sure you want to restore this product?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selproduct.main_image" width="50"></i>
            <h2>
                <b>@{{selproduct.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='recycleproduct(selproduct.id)'>Proceed</button>
    </template>
</dialog-modal>

<dialog-modal id='dialogPermaDelete'>
	<template slot="modal-title">
        Are you sure you want to delete this product permanently?
    </template>
    <template slot="modal-body">
    	<div class="payment-card">
            <i><img :src="selproduct.main_image" width="50"></i>
            <h2>
                <b>@{{selproduct.name}}</b>
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <small>
                        <strong>Date Created:</strong> @{{created_at | invoiceFormat}}
                    </small>
                </div>
            </div>
        </div>
    	
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">Cancel</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='PermaDeletetore(selproduct.id)'>Proceed</button>
    </template>
</dialog-modal>