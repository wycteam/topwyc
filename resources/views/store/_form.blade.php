@include('store.form._images')
@include('store.form._details')

<div class="row">
  <div class="col-xs-12 text-right">
    <button id="storecreate" type="submit" class="btn btn-tangerine">{{$lang['save']}}</button>
  </div>
</div>
