@extends('layouts.master')

@section('title', 'Edit Store')

@section('content')
<div id="store-create" class="container stores-form-container">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="title m-b-2"><span>{{$lang['edit-store']}}</span></div>

      <div class="row">
        <div class="col-xs-12">
          <form id="store-form" role="form" method="PUT" action="{{ route('shopping.stores.update', $store->slug) }}" @submit.prevent="submit" novalidate>
          @include('store._form')
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('styles')
{!! Html::style('https://unpkg.com/vue-multiselect@2.0.0-beta.14/dist/vue-multiselect.min.css') !!}
{!! Html::style('components/flag-icon-css/css/flag-icon.min.css') !!}
{!! Html::style('components/Croppie/croppie.css') !!}
{!! Html::style('components/sweetalert2/dist/sweetalert2.min.css') !!}
{!! Html::style(mix('css/pages/store/form.css', 'shopping')) !!}
@endpush

@push('scripts')
{!! Html::script('https://unpkg.com/vue-multiselect@2.0.0-beta.14') !!}
{!! Html::script('components/sweetalert2/dist/sweetalert2.min.js') !!}
{!! Html::script('components/Croppie/croppie.min.js') !!}
{!! Html::script('cpanel/plugins/jasny/jasny-bootstrap.min.js') !!}
{!! Html::script(mix('js/pages/store/edit.js', 'shopping')) !!}
@endpush
