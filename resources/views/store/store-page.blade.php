<!--  OPEN STORE PAGE -->
@extends('layouts.master')

@section('title', 'Store Name')

@push('styles')
   {!! Html::style(mix('css/pages/store.css','shopping')) !!}
   {!! Html::style(mix('css/pages/product/details.css','shopping')) !!}
   {!! Html::style(mix('css/pages/home.css', 'shopping')) !!}
@endpush

@section('content')
<div class="full-content custom-chosen" id="store-content">
  <div class="container">
      <div class="big-card hovercard">
        <div class="cardheader">

        </div><!-- end cardheader -->

        <div class="avatar">
            <img alt="" src="/shopping/img/sample pictures/sampledp-redgirl.jpg">
        </div><!-- end avatar -->

        <div class="info">
          <div class="col-sm-12 store-info-set">
            <div class="col-sm-3 store-info">
              <div class="title">
                <a target="_blank" href="#">
                  <h3>STORE NAME</h3>
                </a>
                <a target="_blank" href="#">
                  <h4>商店名称</h4>
                </a>
              </div><!-- end title -->
              <div class="title">
                <div class="desc">Jewelle Santos</div>
                <div class="desc">Valenzuela City</div>
                <div class="desc">0925-343-9420</div>
                <div class="desc">gen.tadz@gmail.com</div>
                <div class="desc">Joined since <span id="joined-year">2017</span></div>
              </div><!-- end title -->
              <div class="title">
                <p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Erit enim mecum, si tecum erit. Idem iste, inquam, de voluptate quid sentit? Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum. Sin laboramus, quis est, qui alienae modum statuat industriae? Duo Reges: constructio interrete. Te enim iudicem aequum puto, modo quae dicat ille bene noris.</p>
              </div><!-- end title -->
              <div class="col-sm-12">
                <a href="javascript:void(0)" class="btn btn-raised btn-block btn-tangerine btn-sm"><i class="material-icons">&#xE145;</i>&nbsp;&nbsp;Follow</a>
                <a href="javascript:void(0)" class="btn btn-raised btn-block btn-tangerine btn-sm"><i class="material-icons">&#xE0C9;</i>&nbsp;&nbsp;Chat</a>
              </div>
            </div><!-- end store-info -->

            <div class="col-sm-9">
              <!-- BADGES -->
              <div class="row">
                @include('includes.store.store-page.badges')
              </div><!-- end row -->

              <!--  PRODUCTS -->
              <div class="row">
                @include('includes.store.store-page.products')
              </div><!-- end row-->

            </div><!-- end col-sm-10 -->
          </div>
        </div>
    </div>
  </div><!-- end container -->
</div><!-- end store-content -->

@endsection

@push('scripts')
  {!! Html::script(mix('js/pages/home.js','shopping')) !!}
  {!! Html::script(mix('js/pages/store-page.js','shopping')) !!}
@endpush
