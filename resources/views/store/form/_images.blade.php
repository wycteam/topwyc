<div class="big-card hovercard">
    <div class="cardheader" id="upload-demo"  :style='form.cover?"background-image: url("+form.cover+")":""'>
        <!-- <img class="store-cover-photo" :src="form.cover" v-if="form.cover"> -->

        <div class="cover-uploader ">
            <p class='pull-right'>{{$lang['up-cover-photo']}}</p>
            <input type="file" id="upload" value="Choose a file"  @change='readFile' accept="image/*" />
            <!-- <imgfileupload data-event="coverOnLoad" data-loading-event="coverLoading" data-url="/common/resize-image?width=1140&height=300"></imgfileupload> -->
            <beat-loader :loading="coverLoading" class="v-spinner-cover" size="24px"></beat-loader>
        </div>
    </div>

    <div class="avatar"  data-toggle='modal' data-target='#modalPrimary'>
        <img  class="store-profile-photo img-circle img-responsive" :src="form.profile" v-if="form.profile">
        <div class="avatar-uploader">
            <div class="avatar-upload-text">{{$lang['upload']}}</div>
            <!-- <imgfileupload data-event="profileOnLoad" data-loading-event="profileLoading" data-url="/common/resize-image?width=300&height=300"></imgfileupload> -->
            <beat-loader :loading="profileLoading" class="v-spinner-profile" size="14px"></beat-loader>
        </div>
    </div>
</div>
<div class="col-md-12 text-right" v-if='tempCover'>
    <button type="button" @click='tempCover = false;' class="btn btn-default btn-raised upload-result">{{$lang['cancel']}}</button>
    <button type="button" @click='submitCover' class="btn btn-danger btn-raised upload-result">{{$lang['sub-cover']}}</button>
</div>


<!-- Instructions Modal -->
<modal size="modal-md" id='modalPrimary'>
    <template slot="modal-title">
        <h3>
            {{$lang['primary-pic']}}
        </h3>
    </template>
    <template slot="modal-body">
        <div class='container-fluid'>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div id='upload-primary'></div>
                    <input type="file" value="Choose a file"  @change='readFilePrimary' accept="image/*" hidden />
                </div>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
        <div class="col-md-12 text-center">
            <button v-if='!tempPrimary' type="button" class='btn btn-raised btn-default' @click='triggerUploadPrimary'>{{$lang['pick-img']}}</button>
            <span v-if='tempPrimary'>
                <button type="button" @click='tempPrimary = false;' class="btn btn-danger btn-raised upload-result">{{$lang['cancel']}}</button>
                <button type="button" @click='submitPrimary' class="btn btn-default btn-raised upload-result">{{$lang['sub-prof-img']}}</button>
            </span>
        </div>
    </template>
</modal>