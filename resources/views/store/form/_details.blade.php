<div class="row custom-chosen">
	{{-- Store Details --}}
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group label-floating">
				  	<label class="control-label" for="name">
				  		<span class="flag-icon flag-icon-us"></span> {{$lang['store-name']}} <small>{{$lang['en']}}</small>
					</label>
				  	<input v-model="form.name" id="name" name="name" class="form-control" type="text" autofocus>
				  	<small class="text-danger" v-if="form.errors.has('name')">{{$lang['store-req']}}</small>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group label-floating">
			  	  	<label class="control-label" for="cn_name">
			  	  		<span class="flag-icon flag-icon-cn"></span> {{$lang['store-name']}} <small>{{$lang['cn']}}</small>
			  		</label>
				  	<input v-model="form.cn_name" id="cn_name" name="cn_name" type="text" class="form-control">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
				  	<label class="control-label" for="description">
				  		<span class="flag-icon flag-icon-us"></span> {{$lang['desc']}} <small>{{$lang['en']}}</small>
					</label>
				    <input v-model="form.description" class="form-control" id="description" name="description" type="text">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
				  	<label class="control-label" for="cn_description">
				  		<span class="flag-icon flag-icon-cn"></span> {{$lang['desc']}} <small>{{$lang['cn']}}</small>
					</label>
				    <input v-model="form.cn_description" class="form-control" id="cn_description" name="cn_description" type="text">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
			  		<multiselect 
			  			v-model="form.categories" 
			  			name="categories" 
			  			class="wyc-multiselect" 
			  			:options="categories" 
			  			:multiple="true" 
			  			:max="4" 
			  			:close-on-select="false" 
			  			:clear-on-select="false" 
			  			:hide-selected="true" 
			  			:placeholder="categoryPlaceholder" 
			  			:label="getCategoryName" 
			  			track-by="id"></multiselect>
			  		<small class="text-danger" v-if="form.errors.has('categories')" v-text="form.errors.get('categories')"></small>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
				    <label class="control-label" for="address">{{$lang['add']}}</label>
				    <input v-model="form.address.address" id="address" name="address.address" type="text" class="form-control">
				    <small class="text-danger" v-if="form.errors.has('address.address')">{{$lang['add-req']}}</small>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
					<!-- <label class="control-label" for="province">{{$lang['prov']}}</label> -->
					<!-- <input v-model="form.address.province" id="province" name="address.province" type="text" class="form-control">
					<small class="text-danger" v-if="form.errors.has('address.province')">{{$lang['prov-req']}}</small> -->
					<div class="form-group" :class="{ 'has-error': form.errors.has('address.province') }">
						<multiselect class="custom-select" 
			            :options="optionProv" 
			            placeholder="{{$lang['province']}}" 
			            @input="getCity" 
			            v-model="form.address.province"
			            :selected="form.address.province"
			            deselect-label="{{$lang['field-required']}}"
			            :allow-empty="false"></multiselect>
			            <p class="help-block" v-if="form.errors.has('address.province')" v-text="form.errors.get('address.province')"></p>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
					<!-- <label class="control-label" for="city">{{$lang['city']}}</label> -->
					<!-- <input v-model="form.address.city" id="city" name="address.city" type="text" class="form-control">
					<small class="text-danger" v-if="form.errors.has('address.city')">{{$lang['city-req']}}</small> -->
					<div class="form-group" :class="{ 'has-error': form.errors.has('address.city') }">
						<multiselect class="custom-select" 
				        :options="optionCity" 
				        :allow-empty="false"
				        :selected="form.address.city"
				        placeholder="{{$lang['city']}}"  
				        label="name"
				        track-by="name"
				        v-model="form.address.city"
				       	deselect-label="{{$lang['field-required']}}"
				        ></multiselect>		
				        <p class="help-block" v-if="form.errors.has('address.city')" v-text="form.errors.get('address.city')"></p>		
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
				    <label class="control-label" for="landmark">{{$lang['land']}}</label>
				    <input v-model="form.address.landmark" id="landmark" name="address.landmark" type="text" class="form-control">
				    <small class="text-danger" v-if="form.errors.has('address.landmark')">{{$lang['land-req']}}</small>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group label-floating">
				    <label class="control-label" for="email">{{$lang['email']}}</label>
				    <input v-model="form.email" id="email" name="email" type="email" class="form-control">
				    <small class="text-danger" v-if="form.errors.has('email')" v-text="form.errors.get('email')"></small>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group label-floating">
				    <label class="control-label" for="number">{{$lang['mobile-no']}}</label>
				    <input v-model="form.number.number" @keydown.69.prevent id="number" name="number.number" maxlength="11" type="text" class="form-control cell">
				    <small class="text-danger" v-if="form.errors.has('number.number')">{{$lang['mobile-req']}}</small>
				</div>
			</div>
		</div>
	</div>

	{{-- Store Contact Details --}}
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
				    <label class="control-label" for="company_name">{{$lang['company-name']}}</label>
				    <input v-model="form.company.name" id="company_name" name="company.name" type="text" class="form-control">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
					<label class="control-label" for="company_address">{{$lang['company-add']}}</label>
					<input v-model="form.company.address" id="company_address" name="company.address" type="text" class="form-control">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
				    <label class="control-label" for="facebook">{{$lang['fb']}}</label>
				    <input v-model="form.company.facebook" id="facebook" name="company.facebook" type="facebook" class="form-control">
				    <small class="text-danger" v-if="form.errors.has('company.facebook')" v-text="form.errors.get('company.facebook')"></small>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
				    <label class="control-label" for="twitter">{{$lang['twitter']}}</label>
				    <input v-model="form.company.twitter" id="twitter" name="company.twitter" type="twitter" class="form-control">
				    <small class="text-danger" v-if="form.errors.has('company.twitter')" v-text="form.errors.get('company.twitter')"></small>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group label-floating">
				    <label class="control-label" for="instagram">{{$lang['ig']}}</label>
				    <input v-model="form.company.instagram" id="instagram" name="company.instagram" type="instagram" class="form-control">
				    <small class="text-danger" v-if="form.errors.has('company.instagram')" v-text="form.errors.get('company.instagram')"></small>
				</div>
			</div>
		</div>
	</div>
</div>
