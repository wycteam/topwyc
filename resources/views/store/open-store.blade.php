<!--  OPEN STORE PAGE -->
@extends('layouts.master')

@section('title', 'Create Your Store')

@push('styles')
   {!! Html::style(mix('css/pages/store.css','shopping') !!}
   {!! Html::style('https://unpkg.com/vue-multiselect@2.0.0-beta.14/dist/vue-multiselect.min.css') !!}
@endpush

@section('content')
<div class="full-content custom-chosen" id="open-store-content">
  <div class="container">
    <div class="main-header-container">
      <h3 class="main-header">CREATE YOUR STORE</h3>
    </div><!-- end  main-header-container-->
    <div class="col-sm-12">
        <div class="alert alert-info" role="alert">
          All of the context should not contain any offensive or
          seductive words like sex or other sensitive words. <br>
          (We had set a vocabulary to detect sensitive words,
          and subject for approval the store.)
        </div>
      <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs store-nav" role="tablist">
          <li role="presentation" class="active">
            <a data-toggle="pill" href="#step1" class="store-pill-header">
              <div class='store-pill-content'>
                <img src="/shopping/img/store/store-steps-icon/pencil.png">
                <span>
                  1. FILL UP FORM
                </span>
              </div><!-- end store-pill-content-->
            </a>
          </li>

          <li role="presentation">
            <a data-toggle="pill" href="#step2">
              <div class='store-pill-content'>
                <img src="/shopping/img/store/store-steps-icon/upload.png">
                <span>
                  2. UPLOAD PHOTOS
                </span>
              </div><!-- end store-pill-content-->
            </a>
          </li>
        </ul>

          <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="step1">
            @include('includes.store.store-information')
          </div>
          <div role="tabpanel" class="tab-pane fade" id="step2">
            @include('includes.store.store-upload-pics')
          </div>
        </div>

        <div class="col-sm-12 store-input-set">
          <div class="col-sm-12">
            {!!Form::Button('Save', ['id' => '', 'class' => 'btn btn-tangerine btn-raised pull-right'])!!}
          </div>
        </div><!-- end account-input-set -->
        {!! Form::close() !!}
      </div><!-- end  Nav tabs -->
    </div><!-- end col-sm-12 -->
  </div><!-- end container -->
</div><!-- end open-store-content -->

@endsection

@push('scripts')
  {!! Html::script('https://unpkg.com/vue-multiselect@2.0.0-beta.14') !!}
  {!! Html::script(mix('js/pages/store.js','shopping')) !!}
@endpush
