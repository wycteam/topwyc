@extends('layouts.master')

@section('title', $store->name)

@section('content')
<div class="container stores-show-container">
  <div class="panel panel-default panel-stores-show">
    <div class="panel-heading">
      @if ($store->cover_image)
      <img class="store-cover" src="{{ $store->cover_image }}">
      @endif
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-3">
          <div class="row">
            <div class="col-xs-12">
              <div class="panel panel-default panel-stores-show-profile hidden-sm">
                <div class="panel-heading col-md-10 col-md-offset-1 col-xs-6 col-xs-offset-3">
                  <img class="store-profile img-circle img-responsive"  src="{{ $store->profile_image }}">
                </div>
                <div class="panel-body">
                  <div style="height:1px; clear:both;"></div> <!-- spacer -->
                  <h3 class="text-center">
                    <b>{{ $store->name }}</b>
                    @if (! empty($store->cn_name))
                    <br>
                    <b>{{ $store->cn_name }}</b>
                    @endif
                  </h3>
                @if($user)
                  @if($user->id == $store->user_id)
                  <div class="btn-group-vertical btn-block">
                    <a href="{{ route('shopping.product.create') }}" class="btn btn-raised btn-primary btn-block">
                      {{$lang['add-prod']}}
                    </a>
                    <a href="{{ route('shopping.stores.edit', $store->slug) }}" class="btn btn-raised btn-primary btn-block">
                      {{$lang['edit-store']}}
                    </a>
                  </div>
 
                  @if($enableDeleteButton)
                    {!! Form::open(['route' => ['shopping.stores.destroy', $store->slug], 'method' => 'delete']) !!}
                      <button type="submit" class="btn btn-raised btn-danger btn-block">
                        {{$lang['del-store']}}
                      </button>
                    {!! Form::close() !!}
                  @else
                    <button type="button" class="btn btn-raised btn-danger btn-block" data-toggle="modal" data-target="#deleteStoreNotAllowedModal">
                        {{$lang['del-store']}}
                    </button>
                  @endif
                  
                  @endif
                @endif
                </div>
                <div class="panel-footer">
                  <dl>
                    <dt>
                      {{$lang['desc']}}
                    </dt>
                    <dd>{{ $store->description }}</dd>

                    @if (! empty($store->cn_description))
                    <dt>{{$lang['desc']}} <small>{{$lang['cn']}}</small></dt>
                    <dd>{{ $store->cn_description }}</dd>
                    @endif

                    <dt>{{$lang['add']}}</dt>
                    <dd>{{ $store->full_address }}</dd>
                    <dt>{{$lang['contact-num']}} </dt>
                    <dd>{{ $store->contact_number }}</dd>

                    @if ($store->company)
                      @if ($store->company->name)

                      <dt>{{$lang['company-name']}}</dt>

                      <dd>{{ $store->company->name }}</dd>
                      @endif

                      @if ($store->company->address)
                      <dt>{{$lang['company-add']}} </dt>

                      <dd>{{ $store->company->address }}</dd>
                      @endif
                
                      @if ($store->company->facebook)
                      <dt>{{$lang['fb']}}</dt>

                      <dd class="ellipsis"><a href="{{ $store->company->facebook }}">{{ $store->company->facebook }}</a></dd>
                      @endif

                      @if ($store->company->twitter)
                      <dt>{{$lang['twitter']}}</dt>
                      <dd class="ellipsis"><a href="{{ $store->company->twitter }}">{{ $store->company->twitter }}</a></dd>
                      @endif

                      @if ($store->company->instagram)
                      <dt>{{$lang['ig']}}</dt>

                      <dd class="ellipsis"><a href="{{ $store->company->instagram }}">{{ $store->company->instagram }}</a></dd>
                      @endif
                    @endif
                  </dl>
                  <dt>{{$lang['category']}}</dt>
                  @foreach ($store->categories as $category)
                  <span class="label label-info m-r-1 store-categories">
                    @if(isset($_COOKIE['locale']))
                      @if($_COOKIE['locale'] == 'cn')
                        {{$category->name_cn?:$category->name}}
                      @else
                        {{$category->name}}
                      @endif
                    @else
                      {{$category->name}}
                    @endif
                  </span>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-4">
              <div class="wyc-widget style1 bg-orange-8">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-star fa-4x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <small>{{$lang['rating']}}</small>

                    <h2>4.4/5</h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="wyc-widget style1 bg-cyan-8">
                 <a href="javascript:void(0)" id="allProducts" class="productShow">
                   <div class="row">
                    <div class="col-xs-3">
                      <i class="fa fa-cubes fa-4x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                      <small>{{$lang['product']}}</small>
                      <h2>{{ $store->products->count() }}</h2>
                    </div>
                  </div>
                 </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="wyc-widget style1 bg-red-500">
                <a href="javascript:void(0)" id="suspendedItems" class="productShow">
                  <div class="row">
                    <div class="col-xs-3">
                      <i class="fa fa-exclamation-circle fa-4x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                      <small>Suspended Items</small>
                      <h2>{{ $store->getSuspendedProducts->count() }}</h2>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>

          <div class="row p-top-lg">
            <div class="col-xs-12">
               <div class="tab-content">
                  <div id="allProducts1" class="tab-pane active fade in ">
                    <div class="title text-center">
                      @if($store->products->count() != 0)
                      <span>All Products</span>
                      @elseif($store->products->count() == 0)
                      <span>You have no products yet.</span>
                      @endif
                    </div>
                    <div class="products-grid" style="width: 100% !important;">
                    @foreach ($store->products as $product)
                      <div class="grid-item">
                        @if ($product->is_draft)
                        <div class="draft">{{$lang['draft']}}</div>
                        @endif
                        <img src="/shopping/images/product/{{$product->id}}/primary.jpg" style="width: 190px; height: 166px;" class="img-responsive">
                        <div class="content">
                          <h4 class="product-name ellipsis" data-toggle="tooltip" title="{{ $product->name }}">
                            <a href="{{ route('shopping.product.show', $product->slug) }}">
                              @if(isset($_COOKIE['locale']))
                                @if($_COOKIE['locale'] == 'cn')
                                  {{$product->cn_name?: $product->name}}
                                @else
                                  {{$product->name}}
                                @endif
                              @else
                                {{$product->name}}
                              @endif
                            </a>
                          </h4>
                          <div class="row">
                            <div class="col-md-6 text-left">
                              <h4 class="font-helvetica">
                                <strong class="text-danger">
                                  @if($product->fee_included)
                                    @if($product->sale_price != null || $product->sale_price != 0)
                                      &#8369;{{ $product->sale_price + $product->charge + $product->shipping_fee + $product->vat }}
                                    @else
                                      &#8369;{{ $product->price + $product->charge + $product->shipping_fee + $product->vat }}
                                    @endif
                                  @else
                                    &#8369;{{ $product->sale_price? : $product->price }}
                                  @endif
                                </strong> 
                              </h4>
                            </div>
                            @if ($product->sale_price != null)
                            <div class="col-md-6 text-right">
                              <h5 class="font-helvetica strike-through">
                                  @if($product->fee_included)
                                    @if($product->sale_price != null)
                                      &#8369;{{ $product->price + $product->charge + $product->shipping_fee + $product->vat }}
                                    @endif
                                  @else
                                    &#8369;{{ $product->sale_price? $product->price:'' }}
                                  @endif
                              </h5>
                            </div>
                            @endif
                          </div>

                          {{-- <p>{{$lang['you-save']}} <strong class="font-helvetica">&#8369; 201</strong></p> --}}

                          {{-- <div class="discount">-30%</div> --}}

                          {{-- <p>{{$lang['review']}}: {{ $product->ratings->count() }}</p> --}}

                          {{-- <div class="rate"
                            data-rateyo-rating="{{ $product->product_rating }}"
                            data-rateyo-read-only="true">
                          </div>--}}
                          <div class="rating-block" style="margin-top: 0px">
                              <div class="star-ratings-sprite-small"><span style="width:{{ round($product->productRating) }}%" class="star-ratings-sprite-small-rating"></span></div>
                          </div>
                        @if($user)
                          @if($user->id == $store->user_id)
                          <a href="{{ route('shopping.product.edit', $product->slug) }}" class="btn btn-raised btn-danger btn-block">
                            {{$lang['edit']}}
                          </a>

                          {!! Form::open(['route' => ['shopping.product.destroy', $product->slug], 'method' => 'delete']) !!}
                          <button type="submit" class="btn btn-raised btn-danger btn-block">
                            {{$lang['del']}}
                          </button>
                          {!! Form::close() !!}
                          @endif
                        @endif
                        </div>
                      </div>
                    @endforeach
                    </div>
                  </div>

                  @if($user->id == $store->user_id)
                    <div id="suspendedItems1" class="tab-pane fade in ">
                      <div class="title text-center">
                        @if($store->getSuspendedProducts->count() != 0)
                        <span>Suspended Items</span>
                        @elseif($store->getSuspendedProducts->count() == 0)
                        <span>You don't have any suspended items.</span>
                        @endif
                      </div>

                      <div class="products-grid">
                      @foreach ($store->getSuspendedProducts as $product)
                        <div class="grid-item">
                          @if ($product->is_draft)
                          <div class="draft">{{$lang['draft']}}</div>
                          @endif
                          <img src="{{ $product->main_image_thumbnail }}" class="img-responsive">
                          <div class="content">
                            <h4 class="product-name ellipsis" data-toggle="tooltip" title="{{ $product->name }}">
                              <a href="{{ route('shopping.product.show', $product->slug) }}">
                                @if(isset($_COOKIE['locale']))
                                  @if($_COOKIE['locale'] == 'cn')
                                    {{$product->cn_name?: $product->name}}
                                  @else
                                    {{$product->name}}
                                  @endif
                                @else
                                  {{$product->name}}
                                @endif
                              </a>
                            </h4>
                            <div class="row">
                              <div class="col-md-6 text-left">
                                <h4 class="font-helvetica">
                                  <strong class="text-danger">
                                    &#8369;{{ $product->sale_price? : $product->price }}
                                  </strong> 
                                </h4>
                              </div>
                              @if ($product->sale_price != null)
                              <div class="col-md-6 text-right">
                                <h5 class="font-helvetica strike-through">
                                    &#8369;{{ $product->sale_price? $product->price:'' }}
                                </h5>
                              </div>
                              @endif
                            </div>

                            {{-- <p>{{$lang['you-save']}} <strong class="font-helvetica">&#8369; 201</strong></p> --}}

                            {{-- <div class="discount">-30%</div> --}}

                            {{-- <p>{{$lang['review']}}: {{ $product->ratings->count() }}</p> --}}

                            {{-- <div class="rate"
                              data-rateyo-rating="{{ $product->averageRating | 0 }}"
                              data-rateyo-read-only="true">
                            </div> --}}
                            @if($user)
                              @if($user->id == $store->user_id)
                              <a href="{{ route('shopping.product.edit', $product->slug) }}" class="btn btn-raised btn-danger btn-block">
                                {{$lang['edit']}}
                              </a>

                              {!! Form::open(['route' => ['shopping.product.destroy', $product->slug], 'method' => 'delete']) !!}
                              <button type="submit" class="btn btn-raised btn-danger btn-block">
                                {{$lang['del']}}
                              </button>
                              {!! Form::close() !!}

                              <a href="" class="btn btn-raised btn-primary btn-block btn-cs">
                                Contact Support
                              </a>
                              @endif
                            @endif
                          </div>
                        </div>
                      @endforeach
                      </div> <!-- products-grid -->
                    </div> <!-- suspendedItems1 -->
                  @endif

                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <!-- Delete Store Not Allowed Modal -->
   <div class="modal fade" id="deleteStoreNotAllowedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Delete Store</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-25px;">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <div role="alert" class="alert alert-danger">
            Cannot delete store. Make sure you don't have any pending transactions in your store.
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('common.close')</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Delete Store Not Allowed Modal -->

@endsection

@push('styles')
{!! Html::style('components/rateYo/min/jquery.rateyo.min.css') !!}
{!! Html::style(mix('css/pages/store/show.css', 'shopping')) !!}
@endpush

@push('scripts')
{!! Html::script('components/masonry/dist/masonry.pkgd.min.js') !!}
{!! Html::script('components/rateYo/min/jquery.rateyo.min.js') !!}
{!! Html::script(mix('js/pages/store/show.js', 'shopping')) !!}
@endpush
