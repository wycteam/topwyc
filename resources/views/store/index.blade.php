@extends('layouts.master')

@section('title', $lang['store-mgnt-title'])

@section('content')
<div class="container stores-container">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="title m-b-2"><span>{{$lang['store-mgnt-title']}}</span></div>

      <div class="row">
        <div class="col-md-3">

          <div class="panel panel-default panel-store-profile visible-sm-block">
            <div class="panel-body">
              <div class="row">
                <div class="col-xs-6">
                  <img class="user-avatar img-responsive" src="{{ $user->avatar }}" alt="Profile Picture">
                </div>
                <div class="col-xs-6">
                  <h3 class="full-name"><b>{{ $user->full_name }}</b></h3>
                  <p class="text-muted">
                    <b>{{ $user->stores->count() }}</b> {{$lang['store']}}<br>
                    <b>{{ $user->products->count() }}</b> {{$lang['product']}}
                  </p>
                  <a href="{{ route('shopping.stores.create') }}" class="btn btn-raised btn-primary btn-block">{{$lang['create-store']}}</a>
                </div>
              </div>
            </div>
          </div>

          {{-- Hide on small devices --}}
          <div class="panel panel-default panel-store-profile hidden-sm">
            <div class="row">
              <div class="panel-heading col-md-10 col-md-offset-1 col-xs-6 col-xs-offset-3">
                <h3 class="panel-title">
                  <img class="user-avatar img-responsive" src="{{ $user->avatar }}" alt="Profile Picture">
                </h3>
              </div>
              <div class="panel-body col-xs-10 col-xs-offset-1">
                <h3 class="full-name text-center"><b>{{ $user->full_name }}</b></h3>
                <a href="{{ route('shopping.stores.create') }}" class="btn btn-raised btn-primary btn-block">{{$lang['create-store']}}</a>
              </div>
              <div class="panel-footer col-xs-10 col-xs-offset-1">
                <span class="text-muted"><b>{{ $user->stores->count() }}</b> {{$lang['store']}}</span>
                <span class="text-muted pull-right"><b>{{ $user->products->count() }}</b> {{$lang['product']}}</span>
              </div>
            </div>
          </div>

        </div>

        <div class="col-md-9">
          <div class="row">
            <div class="col-xs-12">
              <div class="owl-carousel owl-theme">
              @foreach ($stores as $store)
                <div class="panel panel-default panel-store">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                      <img class="img-responsive" src="{{ $store->cover_image }}" alt="Store Image">
                    </h3>
                  </div>
                  <div class="panel-body">
                    <h3><a class="store-name" href="{{ route('shopping.stores.show', $store->slug) }}">
                        @if(isset($_COOKIE['locale']))
                          @if($_COOKIE['locale']=='cn')
                            {{$store->cn_name? : $store->name}}
                          @else
                            {{$store->name}}
                          @endif
                        @else
                          {{$store->name}}
                        @endif
                    </a></h3>
                    <p class="store-description">
                        @if(isset($_COOKIE['locale']))
                          @if($_COOKIE['locale']=='cn')
                            {{$store->cn_description? : $store->description}}
                          @else
                            {{$store->description}}
                          @endif
                        @else
                          {{$store->description}}
                        @endif
                    </p>

                    <!-- <div class="rate"
                      data-rateyo-rating="5"
                      data-rateyo-full-star="true"
                      data-rateyo-read-only="true">
                    </div> -->

                    <a href="{{ route('shopping.product.create') }}" class="btn btn-warning btn-raised btn-block">{{$lang['add-prod']}}</a>
                  </div>
                </div>
              @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('styles')
{!! Html::style('components/owl.carousel/dist/assets/owl.carousel.min.css') !!}
{!! Html::style('components/owl.carousel/dist/assets/owl.theme.default.min.css') !!}
{!! Html::style('components/rateYo/min/jquery.rateyo.min.css') !!}
{!! Html::style(mix('css/pages/store/index.css', 'shopping')) !!}
@endpush

@push('scripts')
{!! Html::script('components/owl.carousel/dist/owl.carousel.min.js') !!}
{!! Html::script('components/masonry/dist/masonry.pkgd.min.js') !!}
{!! Html::script('components/rateYo/min/jquery.rateyo.min.js') !!}
{!! Html::script('components/jq.ellipsis/jquery.ellipsis.min.js') !!}
{!! Html::script(mix('js/pages/store/index.js', 'shopping')) !!}
@endpush
