<!--  FINISH OPEN STORE PAGE -->
@extends('layouts.master')

@section('title', 'Create Your Store')

@push('styles')
   {!! Html::style('shopping/css/pages/store.css') !!}
@endpush

@section('content')
<div class="full-content" id="store-finish-content">
	<div class="full-container">
	    <div class="full-header-banner">
	      <h1>Congratulations! We are done creating your store.</h1>
	      <p>
	        &nbsp;Your store is now online. Start growing your business now.
	      </p>
	    </div><!-- end about-header-banner -->
  	</div><!-- end about-container -->

	<div class="container store-container">
<!-- Note: Place condition if user's account is NOT yet verified, please show this -->
	@include('includes.store.finish.store-unverified')

<!-- Note: Place condition if user's account is verified, please show this -->
	@include('includes.store.finish.store-options')

  	</div><!-- end  container about-context-->
</div><!-- end  about-content-->
@endsection

@push('scripts')
    {!! Html::script(mix('js/pages/store-finish.js','shopping')) !!}
@endpush