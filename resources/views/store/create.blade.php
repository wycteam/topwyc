@extends('layouts.master')

@section('title', 'Create Store')

@section('content')
<div id="store-form" class="container stores-form-container">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="title m-b-2"><span>{{$lang['create-store']}}</span></div>

      <div class="row">
        <div class="col-xs-12">
          <div class="alert alert-danger" role="alert" v-if="!docs_verified">
            <b>{{$lang['acct-not-ver-1']}}</b>
            <br>{{$lang['acct-not-ver-2']}}
            <br>{{$lang['acct-not-ver-3']}}
            <br>{{$lang['acct-not-ver-4']}}
            <br><br>{{$lang['acct-not-ver-5']}}<a href="/profile" target="_blank" class="link-alert"><b>{{$lang['acct-not-ver-6']}}</b></a>
          </div>

          <form id="store-form" role="form" method="POST" action="{{ route('shopping.stores.index') }}" @submit.prevent="submit" @keydown="form.errors.clear($event.target.name)" novalidate>
          @include('store._form')
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('styles')
{!! Html::style('https://unpkg.com/vue-multiselect@2.0.0-beta.14/dist/vue-multiselect.min.css') !!}
{!! Html::style('components/flag-icon-css/css/flag-icon.min.css') !!}
    {!! Html::style('components/sweetalert2/dist/sweetalert2.min.css') !!}
{!! Html::style('components/Croppie/croppie.css') !!}
{!! Html::style(mix('css/pages/store/form.css', 'shopping')) !!}
@endpush


@push('scripts')
{!! Html::script('components/sweetalert2/dist/sweetalert2.min.js') !!}
{!! Html::script('https://unpkg.com/vue-multiselect@2.0.0-beta.14') !!}
{!! Html::script('components/Croppie/croppie.min.js') !!}
{!! Html::script('cpanel/plugins/jasny/jasny-bootstrap.min.js') !!}
{!! Html::script(mix('js/pages/store/create.js', 'shopping')) !!}
@endpush
