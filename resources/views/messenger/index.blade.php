<!--  MESSAGES PAGE -->
@extends('layouts.master')

@section('title', 'Messenger')

@push('styles')
 	{!! Html::style('css/pages/messenger.css') !!}
@endpush

@section('content')
<div class="full-content" id="messages-content">
	<div class="row">
		<div class="col-sm-12 messenger-container">
			<div class="mail-box">
                  	<aside class="sm-side">
	                    @include('includes.messenger.left-pane')
                  	</aside>
                  	<aside class="lg-side">
						@include('includes.messenger.right-pane')
					</aside>
                </div>                
            </div>
		</div>
	</div>
</div><!-- end messages-content-->
@endsection
@push('scripts')
 	{!! Html::script(mix('js/pages/messenger.js','shopping')) !!}
@endpush