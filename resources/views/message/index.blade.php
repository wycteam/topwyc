@extends('layouts.master')

@section('title', 'Messages')

@section('content')
<div class="container messages-container">
  <div class="panel panel-default panel-messages">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-3 left-pane">
          <div class="row">
            <div class="col-xs-12 header">
              <h5>
                {{$lang['messages']}}
                <a @click.prevent="newMessage" href="#" class="new-message float-right" 
                data-title="Compose New Message"
                data-toggle='tooltip'
                data-placement='right'>
                  <i class="fa fa-edit fa-lg"></i>
                </a>

                <user-search target=".new-message" panel-class="messages-user-search"></user-search>
              </h5>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 content user-latest-messages-container">
              <user-latest-message
                v-for="message in userLatestMessages"
                :message="message"
                :key="message.id">
              </user-latest-message>
            </div>
          </div>
        </div>
        <div class="col-md-9 right-pane">
          <router-view></router-view>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('styles')
{!! Html::style('components/jQuery-contextMenu/dist/jquery.contextMenu.min.css') !!}
{!! Html::style(mix('css/pages/message/index.css', 'shopping')) !!}
@endpush

@push('scripts')
{!! Html::script('components/jq.ellipsis/jquery.ellipsis.min.js') !!}
{!! Html::script('components/jQuery-contextMenu/dist/jquery.ui.position.min.js') !!}
{!! Html::script('components/jQuery-contextMenu/dist/jquery.contextMenu.min.js') !!}
{!! Html::script(mix('js/pages/message/index.js', 'shopping')) !!}
@endpush
