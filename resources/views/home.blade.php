@extends('layouts.master')

@section('title', 'Home')

@push('styles')
{!! Html::style(mix('css/pages/news.css', 'shopping')) !!}
{!! Html::style(mix('css/pages/home.css', 'shopping')) !!}
{!! Html::style(mix('css/pages/gallery.css', 'shopping')) !!}
{!! Html::style('shopping/plugins/slick/slick.css') !!}
{!! Html::style('shopping/plugins/slick/slick-theme.css') !!}
@endpush

@section('content')


<div id="myModal1" class="modal1">
  <div class="div11" >
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
   <!-- Indicators -->


   <!-- Wrapper for slides -->
   <div  class="carousel-inner" id="imgAppender">

     <div class="item active">
       <img id= "img01"  src="/images/gallery/1549502838405_37.jpg" alt="Los Angeles" style="width:100%;">
     </div>

     <div class="item">
       <img id= "img01" src="/images/gallery/1549502837718_37.jpg" alt="Chicago" style="width:100%;">
     </div>

     <div class="item">
       <img id= "img01" src="/images/gallery/1549502837548_37.jpg" alt="New york" style="width:100%;">
     </div>

   </div>

   <!-- Left and right controls -->
   <a id="lll" class="left carousel-control" href="#myCarousel" data-slide="prev">
     <span id="llll" class="glyphicon glyphicon-chevron-left"></span>
     <span class="sr-only">Previous</span>
   </a>
   <a id="llr"  class="right carousel-control" href="#myCarousel" data-slide="next">
     <span id="lllr"  class="glyphicon glyphicon-chevron-right"></span>
     <span class="sr-only">Next</span>
   </a>
 </div>
    <!--
    <span class="close">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
  -->
  </div>
</div>

<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <h1 style="font-size: 30px;">{{$lang['tagline-1']}}<br></h1>
                    <p>{{$lang['tagline-1-desc']}}<br>{{$lang['tagline-1-desc2']}}</p>
                    <p>
                        <a class="btn btn-lg btn-primary" href="/services" role="button">{{$lang['tagline-1-btn']}}</a>
                    </p>
                </div>
                <div class="carousel-image wow zoomIn">
                    <img src="/images/laptop.png" alt="laptop"/>
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back one"></div>`

        </div>
        <div class="item">
            <div class="container">
                <div class="carousel-caption">
                    <h1>{{$lang['tagline-2']}}</h1>
                    <p>{{$lang['tagline-2-desc']}}</p>
                    <p><a class="btn btn-lg btn-primary" href="/services" role="button">{{$lang['tagline-2-btn']}}</a></p>
                </div>
                <div class="carousel-image wow zoomIn">
                    <img src="/images/laptop.png" alt="laptop"/>
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back two"></div>
        </div>
    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

@if($newsCount>=5)
<section id="newsArticles" class="features wow fadeInRight">
     @include('includes.home.company-news')
</section>
@endif
<section class="features wow fadeInRight">
     @include('includes.home.company-gallery')
</section>

<section class="features">
    @include('includes.home.company-values')
</section>

<section  class="container features wow fadeInLeft">
    @include('includes.home.services-offered')
</section>

<section id="appvisa "class="features">
    @include('includes.home.mobile-app')
</section>


<section id="contact" class="gray-section contact wow fadeInDown">
     @include('includes.home.contact-section')
</section>



@endsection
@push('scripts')
{!! Html::script(mix('js/pages/home.js','shopping')) !!}
{!! Html::script(mix('js/visa/tracking/index.js', 'shopping')) !!}
{!! Html::script(mix('js/visa/news.js','shopping')) !!}
{!! Html::script('shopping/plugins/slick/slick.min.js') !!}
{!! Html::script(mix('js/visa/gallery.js','shopping')) !!}
@endpush
