<!--  ABOUT PAGE -->
@extends('layouts.master')

@section('title', 'About WATAF')

@push('styles')
 	{!! Html::style(mix('css/pages/about.css','shopping')) !!}
@endpush

@section('content')
<div class="full-content" id="about-content">

  <div class="full-container">
    <div class="full-header-banner">
      <h1>{{$lang['header-1']}}</h1>
      <p> 
        &nbsp;{{$lang['subheader-1']}}
      </p>
    </div><!-- end about-header-banner -->
  </div><!-- end about-container -->

  <div class="container about-context">
    <div class="col-sm-12">
      <div class="col-sm-5">
        <div class="about-text-header-container">
          <h4 class="text-header">{{$lang['about-wataf']}}</h4>
        </div><!-- end about-header-container-->

        <p class="about-description">
          <span>{{$lang['wataf-shop']}}</span>  {{$lang['desc-1']}}{{$lang['desc-2']}}
        
        </p>
      </div><!-- end col-sm-5 -->

      <div class="col-sm-7">
        <div class="about-text-header-container orange">
          <h4 class="text-header">{{$lang['header-2']}}</h4>
        </div><!-- end about-header-container-->

        <div class="col-sm-12 choose-set">
          <div class="col-sm-3 about-image">
            <img src="/shopping/img/general/about/about-social.jpeg">
          </div><!-- end about-image -->
          <div class="col-sm-9 about-text">
            <h4 class="sub-header orange">{{$lang['social-head']}}</h4>
            <hr>
             <p>
              {{$lang['social-desc']}}
            </p>
          </div><!-- end about-text -->
        </div><!-- end  col-sm-12-->

        <div class="col-sm-12 choose-set">
          <div class="col-sm-3 about-image">
            <img src="/shopping/img/general/about/about-integrity.jpeg">
          </div><!-- end about-image -->
          <div class="col-sm-9 about-text">
            <h4 class="sub-header orange">{{$lang['integ-head']}}</h4>
            <hr>
             <p>
              {{$lang['integ-desc']}}
            </p>
          </div><!-- end about-text -->
        </div><!-- end  col-sm-12-->

        <div class="col-sm-12 choose-set">
          <div class="col-sm-3 about-image">
            <img src="/shopping/img/general/about/about-convenience.jpeg">
          </div><!-- end about-image -->
          <div class="col-sm-9 about-text">
            <h4 class="sub-header orange">{{$lang['conv-head']}}</h4>
            <hr>
             <p>
              {{$lang['conv-desc']}}
            </p>
          </div><!-- end about-text -->
        </div><!-- end  col-sm-12-->
      </div><!-- end  col-sm-7-->

    </div><!-- end  col-sm-12-->
  </div><!-- end  container about-context-->
</div><!-- end  about-content-->

@endsection
