<!--  FAQ PAGE -->
@extends('layouts.master')

@section('title', 'WATAF Help Center')

@push('styles')
    {!! Html::style(mix('css/pages/faq.css','shopping')) !!}
@endpush

@section('content')
<div class="full-content" id="faq-content">
  <div class="container">
    <div class="main-header-container">
      <h3 class="main-header">WATAF TUTORIAL CENTER</h3>
    </div><!-- end  main-header-container-->

    <div class="row faq-container">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 faq-card">
        <a href="/faq/contents/?help=account">
          <div class="card">
            <div class="col-lg-12 text-center">
              <div class="icon-wrapper"><i class="fa fa-user-o custom-icon account bgc-indigo"><span class="fix-editor">&nbsp;</span></i></div>
              <div class="faq-name">
                <span>ACCOUNT</span>
                <br> 
                <small class="tc-grey">Do you have questions about creating and editing your WATAF Account? Check this out.</small>
              </div>
            </div> <!--end text-center--> 
          </div> <!--end card-->
        </a>
      </div><!-- end faq-card-->

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 faq-card">
        <a href="/faq/contents/?help=store">
          <div class="card">
            <div class="col-lg-12 text-center">
              <div class="icon-wrapper"><i class="fa fa-archive custom-icon store bgc-blue"><span class="fix-editor">&nbsp;</span></i></div>
              <div class="faq-name">
                <span>STORE</span> 
                <br> 
                <small class="tc-grey">Trouble managing your store? This is the right place to seek help.</small>
              </div>
            </div> <!--end text-center--> 
          </div> <!--end card-->
        </a>
      </div><!-- end faq-card-->

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 faq-card">
        <a href="/faq/contents/?help=product">
          <div class="card">
            <div class="col-lg-12 text-center">
              <div class="icon-wrapper"><i class="fa fa-cubes custom-icon product bgc-light-blue"><span class="fix-editor">&nbsp;</span></i></div>
              <div class="faq-name">
                <span>PRODUCT</span> 
                <br> 
                <small class="tc-grey">Common questions and answers from creating, editing up to managing your products.</small>
              </div>
            </div> <!--end text-center--> 
          </div> <!--end card-->
        </a>
      </div><!-- end faq-card-->

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 faq-card">
        <a href="/faq/contents/?help=order">
          <div class="card">
            <div class="col-lg-12 text-center">
              <div class="icon-wrapper"><i class="fa fa-shopping-cart custom-icon order bgc-cyan"><span class="fix-editor">&nbsp;</span></i></div>
              <div class="faq-name">
                <span>ORDER</span> 
                <br> 
                <small class="tc-grey">Do you have concern on placing an order or delivery? Here are the top FAQs.</small>
              </div>
            </div> <!--end text-center--> 
          </div> <!--end card-->
        </a>
      </div><!-- end faq-card-->

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 faq-card">
        <a href="/faq/contents/?help=friends">
          <div class="card">
            <div class="col-lg-12 text-center">
              <div class="icon-wrapper"><i class="fa fa-users custom-icon  friends bgc-green"><span class="fix-editor">&nbsp;</span></i></div>
              <div class="faq-name">
                <span>FRIENDS</span> 
                <br> 
                <small class="tc-grey">Here are the top FAQs about Adding Friends and many more ...</small>
              </div>
            </div> <!--end text-center--> 
          </div> <!--end card-->
        </a>
      </div><!-- end faq-card-->

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 faq-card">
        <a href="/faq/contents/?help=ewallet">
          <div class="card">
            <div class="col-lg-12 text-center">
              <div class="icon-wrapper"><i class="fa fa-credit-card custom-icon ewallet bgc-light-green"><span class="fix-editor">&nbsp;</span></i></div>
              <div class="faq-name">
                <span>E-WALLET</span> 
                <br> 
                <small class="tc-grey">Learn about the E-Wallet adn the convenience it offers.</small>
              </div>
            </div> <!--end text-center--> 
          </div> <!--end card-->
        </a>
      </div><!-- end faq-card-->

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 faq-card">
        <a href="/faq/contents/?help=chat">
          <div class="card">
            <div class="col-lg-12 text-center">
              <div class="icon-wrapper"><i class="fa fa-comments-o custom-icon chat bgc-lime"><span class="fix-editor">&nbsp;</span></i></div>
              <div class="faq-name">
                <span>CHAT</span> 
                <br> 
                <small class="tc-grey">Here are some questions and answers about chat and more...</small>
              </div>
            </div> <!--end text-center--> 
          </div> <!--end card-->
        </a>
      </div><!-- end faq-card-->

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 faq-card">
        <a href="/faq/contents/?help=feedback">
          <div class="card">
            <div class="col-lg-12 text-center">
              <div class="icon-wrapper"><i class="fa fa-commenting-o custom-icon feedback bgc-yellow"><span class="fix-editor">&nbsp;</span></i></div>
              <div class="faq-name">
                <span>FEEDBACK</span> 
                <br> 
                <small class="tc-grey">Need to know how to sumbit a feedback? Check this out.</small>
              </div>
            </div> <!--end text-center--> 
          </div> <!--end card-->
        </a>
      </div><!-- end faq-card-->


    </div><!-- end faq-container-->  
  </div><!-- end container-->
</div><!-- end faq-content-->

@endsection
