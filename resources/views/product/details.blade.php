@extends('layouts.master')

@section('title', 'Product Details')

@push('styles')
 {!! Html::style('shopping/plugins/carousel/owl.carousel.css') !!}
 {!! Html::style('shopping/plugins/carousel/owl.theme.default.css') !!}
 {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css') !!}
 {!! Html::style(mix('css/pages/home.css', 'shopping')) !!}
 {!! Html::style(mix('css/pages/product/details.css', 'shopping')) !!}
 {!! Html::style(mix('css/pages/feedback.css', 'shopping')) !!}
 {!! Html::style('components/flag-icon-css/css/flag-icon.min.css') !!}
@endpush

@section('content')
<header class="full-content" id='product-details-container'>
    <!-- Main Content -->
    <div class="maindiv">
    <div class="container m-t-3 p-t-3">
      <div class="row">

        <!-- Image List -->
        <div class="col-sm-4 p-l-3">
          <div class="jqzoom">
              <img  id="zoom_03" src="{{ $product->main_image }}" data-zoom-image="{{ $product->main_image }}"  />

                <div id="gallery_01" class="m-t-2">
                    <a  class="elevatezoom-gallery active" data-update="" data-image="{{ $product->main_image }}" data-zoom-image="{{ $product->main_image }}">
                        <img src="{{ $product->main_image }}" width="51" height="51"  />
                    </a>

                    @if($product->main_image2)
                    <a  class="elevatezoom-gallery" data-update="" data-image="{{ $product->main_image2 }}" data-zoom-image="{{ $product->main_image2 }}">
                        <img src="{{ $product->main_image2 }}" width="51"  height="51"/>
                    </a>
                    @endif

                    @if($product->main_image3)
                    <a  class="elevatezoom-gallery" data-update="" data-image="{{ $product->main_image3 }}" data-zoom-image="{{ $product->main_image3 }}">
                        <img src="{{ $product->main_image3 }}" width="51"  height="51"/>
                    </a>
                    @endif

                    @if($product->main_image4)
                    <a  class="elevatezoom-gallery" data-update="" data-image="{{ $product->main_image4 }}" data-zoom-image="{{ $product->main_image4 }}">
                        <img src="{{ $product->main_image4 }}" width="51"  height="51"/>
                    </a>
                    @endif

                    @if($product->main_image5)
                    <a  class="elevatezoom-gallery" data-update="" data-image="{{ $product->main_image5 }}" data-zoom-image="{{ $product->main_image5 }}">
                        <img src="{{ $product->main_image5 }}" width="51"  height="51"/>
                    </a>
                    @endif

                    @if($product->main_image6)
                    <a  class="elevatezoom-gallery" data-update="" data-image="{{ $product->main_image6 }}" data-zoom-image="{{ $product->main_image6 }}">
                        <img src="{{ $product->main_image6 }}" width="51"  height="51"/>
                    </a>
                    @endif
                </div>
          </div>
        </div>
        <!-- End Image List -->

        <div class="col-sm-6 mobile">
          <div class="title-detail">
              {{ $product->name }}
              <br>
              @if($user)
                  @if($user->id != $product->user_id)
                      <small class="fa fa-flag btnsmall" @click="showReportModal()"> {{$lang['report']}}</small>
                  @else
                      <small class="fa fa-flag btnsmall" @click="cantReport()"> {{$lang['report']}}</small>
                  @endif
              @else
                  <small class="fa fa-flag btnsmall" @click="needLogin()"> {{$lang['report']}}</small>
              @endif
              <a v-cloak v-show="checkfav==1 && checkfav!=''" class="wishlist" href="javascript:void(0)" @click="AddToWishlist({{$product->id}})"><span class="glyphicon glyphicon-heart"></span> {{$lang['add-to-wishlist']}}</a>
              <a v-cloak v-show="checkfav==0 && checkfav!=''" class="wishlist" href="javascript:void(0)" @click="RemoveFromWishlist()"><span class="glyphicon glyphicon-heart"></span>
              {{$lang['remove-from-wishlist']}}</a>
              <div class="rating-block">
                <div class="star-ratings-sprite-small"><span style="width:{{ round($product->productRating) }}%" class="star-ratings-sprite-rating-small"></span></div>
              </div>
              <div class="table-detail">
                @if($product->sale_price)
                  <div class="price p-t-2">
                    @if($product->fee_included)
                        <div><span class="price-old">₱{{ number_format(($product->price+ $product->vat + $product->charge + $product->shipping_fee),2) }}</span>
                        ₱{{ number_format(($product->sale_price + $product->vat + $product->charge + $product->shipping_fee),2) }} <br><span class="ysv">(You save {{ $product->discount }}%)</span></div>
                    @else
                        <div><span class="price-old">₱{{ number_format(($product->price),2) }}</span>
                          ₱{{ number_format($product->sale_price,2) }} <span class="ysv">(You save {{ $product->discount }}%)</span></div>
                        
                    @endif
                    
                  </div>
                  @else
                  <div class="price p-t-2">
                    @if($product->fee_included)
                      <div>₱{{ number_format(($product->price+ $product->vat + $product->charge + $product->shipping_fee),2) }}</div>
                    @else
                      <div>₱{{ number_format($product->price,2) }}</div>
                    @endif
                  </div>
                  @endif
              </div>
          </div>

          @foreach($product->product_cats as $cats)
            <span class="label label-warning arrowed" style="font-size: 10px">{{$cats->name}} </span>
          @endforeach
          
          <table class="table table-detail m-t-2">
            <tbody>
              @if($product->cn_name)
              <tr>
                <td class="fxWd"><span class="flag-icon flag-icon-cn"></span> {{$lang['product-name']}}</td>
                <td  class="p-l-2">
                    {{ $product->cn_name }}
                </td>
              </tr>
              @endif
              <!-- <tr>
                <td class="fxWd">{{$lang['price']}}</td>
                <td class="p-l-2">
                  @if($product->sale_price)
                  <div class="price">
                    @if($product->fee_included)
                        <div>P{{ number_format(($product->sale_price + $product->vat + $product->charge + $product->shipping_fee),2) }} <span class="label label-default arrowed m-l-2">-{{ $product->discount }}%</span></div>
                        <span class="price-old">P{{ number_format(($product->price+ $product->vat + $product->charge + $product->shipping_fee),2) }}</span>
                    @else
                        <div>P{{ number_format($product->sale_price,2) }} <span class="label label-default arrowed m-l-2">-{{ $product->discount }}%</span></div>
                        <span class="price-old">P{{ number_format(($product->price),2) }}</span>
                    @endif
                    
                  </div>
                  @else
                  <div class="price">
                    @if($product->fee_included)
                      <div>P{{ number_format(($product->price+ $product->vat + $product->charge + $product->shipping_fee),2) }}</div>
                    @else
                      <div>P{{ number_format($product->price,2) }}</div>
                    @endif
                  </div>
                  @endif
                </td>
              </tr> -->
<!--               <tr>
                <td class="fxWd">Shipping Fee</td>
                <td class="p-l-2">
                  @if($product->fee_included)
                   <div><small><b>Shipping Fee Included </b></small></div>
                  @else
                  <div class="shipprice"><b>  
                    ₱{{ number_format(($product->vat + $product->charge + $product->shipping_fee),2) }}</b>
                  </div>
                  @endif
                </td>
              </tr> -->
              <tr>
                <td class="fxWd">{{$lang['availability']}}</td>
                <td class="p-l-2">
                  @if($product->stock > 0)
                    <!-- <span class="label label-success arrowed p-x-2" >{{$lang['in-stock']}} </span> -->
                    <span id="remain"><small><b>{{$lang['remaining-stock']}}: {{$product->stock}}</b></small></span>
                  @else
                    <span>{{$lang['out-stock']}}</span>
                  @endif
                </td>
              </tr>
              <tr v-if="varcolor != 0" v-cloak>
                <td>{{$lang['choose-color']}}</td>
                <td>
                 <div id="gallery_02">
                      <a  v-for="(varco,index) in varcolor" class="elevatezoom-gallery padding" :class="{ 'active': index==0}" data-update="" :data-image="url+varco.image_name+'.jpg'" :data-zoom-image="url+varco.image_name+'.jpg'" @click="chooseColor(varco)" >
                          <img :src="url+varco.image_name+'.jpg'" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="varco.label" :data-original-title="varco.label" data-selector="true"/>
                      </a>
                  </div>
                </td>
              </tr>
              <tr v-if="varsize != 0" v-cloak>
                <td>{{$lang['choose-size']}}</td>
                <td>
                  <select id="gSize" @change="chooseSize($event.target.value)">
                    <!-- <option value="size">{{$lang['size']}}</option> -->
                    <option :value="varsi.id + '-' + varsi.stock" v-for="varsi in varsize2">@{{varsi.label}}</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="p-t-2 fxWd">{{$lang['quantity']}}</td>
                <td v-cloak>
                  <div id="qty-mar" class="input-qty col-sm-2">
                    <input v-if="varcolor == 0" id="qty" type='number' max="{{ $product->stock }}" min='1' value="1" class='form-control text-center m-x-1'>
                    <input v-else id="qty" type='number' :max="colorStock" min='1' value="1" class='form-control text-center m-x-1'>
                  </div>
                  <div v-show="varcolor != 0" class="color-stock"><small><b>{{$lang['pieces']}} (@{{ colorStock }} {{$lang['in-stock']}})</small></b></div>
              </tr>
        <!--  <tr>
                <td></td>
                <td class="buy-now-cont">
                  @if($product->is_searchable)
                    <a id="buy-now" href="javascript:void(0)" class="btn btn-raised btn-primary btn-lg" @click="buyNow({{$product->id}})"><i class="fa fa-shopping-bag"></i> {{$lang['buy-now']}}</a>
                    <a id="add-to-cart" href="javascript:void(0)" class="btn btn-raised btn-primary btn-lg" @click="addToCart2({{$product->id}})"><i class="fa fa-shopping-cart"></i> {{$lang['add-to-cart']}}</a>
                  @endif
                </td>
              </tr> -->
            </tbody>
          </table>
                  @if($product->is_searchable)
                    @if($user->id != $product->user_id)
                      <a id="buy-now" href="javascript:void(0)" class="btn btn-raised btn-primary btn-lg" @click="buyNow({{$product->id}})"><i class="fa fa-shopping-bag"></i> {{$lang['buy-now']}}</a>
                      <a id="add-to-cart" href="javascript:void(0)" class="btn btn-raised btn-primary btn-lg" @click="addToCart2({{$product->id}})"><i class="fa fa-shopping-cart"></i> {{$lang['add-to-cart']}}</a>
                    @else
                      @if($product->is_draft)
                        <div class="text-danger">
                          <small>* Your product is currently marked as DRAFT. Edit to Activate.</small>
                        </div>
                      @endif
              
                      <a id="edit-product" href="/product/{{$product->slug}}/edit" class="btn btn-raised btn-primary btn-lg"><i class="fa fa-shopping-cart"></i> Edit Product</a>
                    @endif
                  @endif
        </div>
        <div class="col-sm-2 hidden-xs">
          <div class="store-details" v-cloak>
              <h5>{{$lang['store-details']}}</h5>
              <span><a href="/stores/{{$product->store->slug}}" class="storename" data-container="body" data-toggle="popover" data-placement="left" data-content="You will receive a discount if you bought multiple items from this store in single transaction.">@{{ storename }}</a></span>
              <div class="stars">
                <i class="levels diamonds"></i>
                <i class="levels diamonds"></i>
                <i class="levels diamonds"></i>
                <i class="levels diamonds"></i>
                <i class="levels diamonds"></i>
              </div>
              <p class="store-desc">
                @if(isset($_COOKIE['locale']))
                  @if($_COOKIE['locale']=='cn')
                    {{ mb_strimwidth($product->store->cn_description, 0, 200, "...") }}
                  @else
                    {{ mb_strimwidth($product->store->description, 0, 200, "...") }}
                  @endif
                @else
                    {{ mb_strimwidth($product->store->description, 0, 200, "...") }}
                @endif
              </p>
              @if($user)
              @if($user->id != $product->user_id)
                <span class="store-btn">{{$lang['contact-me']}}: 
                @if($product->user->is_online)
                  <span class="green">{{$lang['im-online']}}</span>
                @else
                  <span class="red">{{$lang['im-not-online']}}</span>
                @endif
                @if ($product->user->is_docs_verified)
                <a @click="contactMe" href="javascript:void(0)" class="btn btn-raised btn-default btn-block fa fa-envelope font-size">
                  @if($product->user->is_online)
                    <span class="font-size">{{$lang['chat-me']}}</span>
                  @else
                    <span class="font-size">{{$lang['leave-message']}}</span>
                  @endif
                </a></span>
                @else
                <a @click="notVerified" href="javascript:void(0)" class="btn btn-raised btn-default btn-block fa fa-envelope">
                  @if($product->user->is_online)
                    <span class="font-size">{{$lang['chat-me']}}</span>
                  @else
                    <span class="font-size">{{$lang['leave-message']}}</span>
                  @endif
                </a></span>
                @endif
              @endif
              @endif
              <a href="/stores/{{$product->store->slug}}" class="btn btn-raised btn-default btn-block btnmobile"> {{$lang['view-store']}} </a>
<!--               <div class="row badges-container">
                <div class="col-sm-3 badges-img">
                  <img src="/images/icons-cash_on_delivery.png">
                </div>
                <div class="col-sm-9 badge-text">
                  <span class="badge-text">{{$lang['cash-on']}}</span>
                </div>
              </div> -->
              <div class="row badges-container">
                <div class="col-sm-3 badges-img">
                  <img src="/images/ecommerce-security.png">
                </div>
                <div class="col-sm-9 badge-text">
                  <span class="badge-text">100% {{$lang['buyer-protection']}}</span>
                </div>
              </div>
          </div>
        </div>
        
        <div class="row break">
        <div class="col-md-12">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#desc" aria-controls="desc" role="tab" data-toggle="tab">{{$lang['description']}}</a></li>
            <li role="presentation"><a href="#review" aria-controls="review" role="tab" data-toggle="tab" v-cloak>{{$lang['reviews']}} (@{{reviewCount}})</a></li>
          </ul>
          <!-- End Nav tabs -->

          <!-- Tab panes -->
          <div class="tab-content tab-content-detail">

              <!-- Description Tab Content -->
              <div role="tabpanel" class="tab-pane active" id="desc">
                <div class="well">

                  <div class="store-subhead m-b-2 m-t-1">
                    <h4 class="sub-header">{{$lang['product-specifications']}} </h4>
                    <hr>
                  </div>

                  <table class="table specsTable">
                    <tbody>
                      @if($product->boxContent)
                      <tr>
                        <td class="specsTitle" width="200">{{$lang['what-box']}} :</td>
                        <td>{{ $product->boxContent }}</td>
                      </tr>
                      @endif
                      @if($product->brand)
                      <tr>
                        <td class="specsTitle">{{$lang['brand']}} :</td>
                        <td>{{ $product->brand }}</td>
                      </tr>
                      @endif
                      @if($product->stock)
                      <tr>
                        <td class="specsTitle">{{$lang['remaining-stock']}}</td>
                        <td>{{ $product->stock }}</td>
                      </tr>
                      @endif
                      @if($product->wPeriod)
                      <tr>
                        <td class="specsTitle">Warranty :</td>
                        <td>{{ $product->wPeriod }}</td>
                      </tr>
                      @endif
                      @if($product->weight)
                      <tr>
                        <td class="specsTitle">{{$lang['weight']}} :</td>
                        <td>{{ $product->weight }}</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>

                  <div class="store-subhead m-b-2 m-t-3">
                    <h4 class="sub-header">{{$lang['product-description']}} </h4>
                    <hr>
                  </div>
                  <table class="table specsTable">
                    <tbody>
                      @if($product->description)
                      <tr>
                        <td class="specsTitle" width="200"><span class="flag-icon flag-icon-us"></span> {{$lang['description']}} :</td>
                        <td v-html="description"></td>
                      </tr>
                      <tr>
                        
                      </tr>
                      @endif
                      @if($product->cn_description)
                      <tr>
                        <td class="specsTitle" width="200"><span class="flag-icon flag-icon-cn"></span> {{$lang['description']}} :</td>
                        <td v-html="chDescription"></td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                    <div class="row">

                      @if($product->desc_image)
                        <div class="col-sm-12 p-x-5 m-b-3">
                          <img class="img-responsive" src="{{ $product->desc_image }}">
                        </div>
                      @endif

                      @if($product->desc_image2)
                        <div class="col-sm-12 p-x-5 m-b-3">
                          <img class="img-responsive" src="{{ $product->desc_image2 }}">
                        </div>
                      @endif

                      @if($product->desc_image3)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image3 }}">
                        </div>
                      @endif

                      @if($product->desc_image4)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image4 }}">
                        </div>
                      @endif

                      @if($product->desc_image5)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image5 }}">
                        </div>
                      @endif

                      @if($product->desc_image6)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image6 }}">
                        </div>
                      @endif

                      @if($product->desc_image7)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image7 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image8)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image8 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image9)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image9 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image10)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image10 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image11)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image11 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image12)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image12 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image13)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image13 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image14)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image14 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image15)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image15 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image16)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image16 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image17)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image17 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image18)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image18 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image19)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image19 }}">
                        </div>
                      @endif
            
                      @if($product->desc_image20)
                        <div class="col-sm-12 p-x-5 m-b-3">
                            <img class="img-responsive" src="{{ $product->desc_image20 }}">
                        </div>
                      @endif
                      
                    </div>



                </div>
              </div>
              <!-- End Description Tab Content -->

              <!-- Review Tab Content -->
              @include('product.tab-review')
              <!-- End Review Tab Content -->

          </div>
          <!-- End Tab panes -->

        </div>
        </div>
      </div>

      <!-- Related Products -->
      <div class="row m-t-3" v-if='items.length'>
        <div class="col-xs-12">
          <div class="title"><span>{{$lang['related-products']}}</span></div>
          <div class="related-product-slider owl-controls-top-offset">
            <related-product-item
              v-for="item in items"
              :item="item"
              :key="item.id"
            >
            </related-product-item>
          </div>
        </div>
      </div>
      <!-- End Related Products -->

    </div>
    </div>
    <!-- End Main Content -->
</header>
<div id="product-report">
    @include('includes.product.modals')
</div>
@endsection

@push('scripts')
 {!! Html::script('https://unpkg.com/vue-star-rating/dist/star-rating.min.js') !!}
 {!! Html::script(mix('js/pages/product-report.js','shopping')) !!}
 {!! Html::script(mix('js/pages/product-details.js','shopping')) !!}
 {!! Html::script('shopping/plugins/jqzoom/elevatezoom.js') !!}

  <script type="text/javascript">
      var zoomConfig = {cursor: 'crosshair', zoomType: "lens", lensShape : "round" };
      var image = $('#gallery_01 a, #gallery_02 a');
      var zoomImage = $('img#zoom_03');

      zoomImage.elevateZoom(zoomConfig);//initialize zoom

      image.on('click', function(){
          image.removeClass("active");
          $(this).addClass("active");
          // Remove old instance
          $('.zoomContainer').remove();
          zoomImage.removeData('elevateZoom');
          // Update source for images
          zoomImage.attr('src', $(this).data('image'));
          zoomImage.data('zoom-image', $(this).data('zoom-image'));
          // Reinitialize EZ
          zoomImage.elevateZoom(zoomConfig);
      });

          $('.storename').popover({}).popover('show');
  </script>
@endpush


