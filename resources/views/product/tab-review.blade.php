<div role="tabpanel" class="tab-pane" id="review">

    <div class="well" id='feedbacks_container'>
        @if($user && ($user->id != $product->user_id))
        <div class='card'>
            <div class="col-lg-12">
                <div class='row user-feedback'>
                    <div class='col-lg-1 text-center'>
                        @if($user->avatar)
                            <img src="{{$user->avatar}}" class="img img-circle fb-user-image">
                        @else
                            <div class='no-avatar' >
                                {{$user->initials}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-11">
                        <div class='fb-holder'>
                            <div class='fb-name'>
                                {{$user->full_name}}
                            </div>
                            <div class='fb-content'>
                                <div class="col-lg-2 col-md-2" v-if='form.imageTemp'>
                                    <img :src="form.imageTemp" class='img-responsive' >
                                </div>

                                <div :class="form.imageTemp? 'col-lg-10 col-md-10':'col-lg-12 col-md-12'">
                                    <div class="form-group"  :class="{ 'has-error': form.errors.has('content') }">
                                        <label class="control-label" for="focusedInput1">{{$lang['tell-others']}}</label>
                                        <textarea v-model='form.content' v-on:keyup="handleCmdEnter($event)" id='feedback_textarea' class='form-control' rows='2'></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 cd-filter-block" style="margin-top: -38px;">
                                <ul class="cd-filter-content cd-filters list review-options">
                                    <div class="form-group"  :class="{ 'has-error': form.errors.has('review') }">
                                        <label class="control-label" for="focusedInput1">Please answer</label>
                                    </div>                                
                                    <li class="margin">
                                        <input class="filter radio-review" data-filter=".radio1" type="radio" value="good" v-model="form.review" name="review" id="good">
                                        <label class="radio-label label-review" for="radio1">Is it Good?</label>
                                    </li>
                                    <li class="margin">
                                        <input class="filter radio-review" data-filter=".radio3" type="radio" value="bad" v-model="form.review" name="review" id="bad">
                                        <label class="radio-label label-review" for="radio3">Is it Bad?</label>
                                    </li>
                                    <div class="form-group"  :class="{ 'has-error': form.errors.has('review') }">
                                        <p class="help-block" v-if="form.errors.has('review')" v-text="form.errors.get('review')"></p>
                                    </div> 
                                </ul>
                            </div>
                            <div class='col-sm-4 pull-right text-right'>
                                <a class='upload-container btn btn-default btn-raised'>
                                    <imgfileupload data-event="submitFeedbackImage" data-url="/common/resize-whole-image?width=250&height=250">
                                    </imgfileupload>
                                    <span class='fa fa-image'></span>
                                </a>
                                <button id="submitReview" @click='submitFeedback' class='btn btn-raised btn-success btn-ripple btn-has-loading'>{{$lang['submit']}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="card alert alert-info  text-center">
                    <div><span class="fa fa-2x fa-info-circle"></span> </div>
                    <div>
                        @if($user)
                            @if($user->id == $product->user_id)
                                {{$lang['cannot-make']}}
                            @endif
                        @else
                            {{$lang['to-make']}}
                        @endif
                    </div>
            </div>
        @endif
        <modal id='modalComments'>
            <template slot='modal-title'>
                {{$lang['add-comment']}}
            </template>
            <template slot='modal-body'>
                <div class="form-group"   :class="{ 'has-error': commentForm.errors.has('content') }">
                    <label class="control-label" for="focusedInput1">{{$lang['say-something']}}</label>
                    <textarea v-model='commentForm.content'  id='feedback_textarea' class='form-control' rows='3'></textarea>
                    <span  v-if="commentForm.errors.has('content')" v-text="commentForm.errors.get('content')"></span>
                </div>
            </template>
            <template slot='modal-footer'>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{$lang['close']}}</button>
                <button type="button" class="btn btn-success btn-has-loading" @click='submitComment'>
                {{$lang['submit']}}</button>
            </template>
        </modal>
        <review v-for='review in reviews' :key='review.id' :feedback='review' ></review>
        <div class="row" v-if='(reviewCount > 2) && reviews.length < reviewCount'>
            <div  class="col-lg-12 col-md-12 text-center">
                <button class="btn btn-has-loading btn-default btn-raised" @click='loadMoreReviews'>
                    {{$lang['view-more']}}
                </button>
            </div>
        </div>
    </div>
</div>
               