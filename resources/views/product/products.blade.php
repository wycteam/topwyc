@extends('layouts.master')

@section('title', 'Products')

@push('styles')
 {!! Html::style('shopping/plugins/carousel/owl.carousel.css') !!}
 {!! Html::style('shopping/plugins/carousel/owl.theme.default.css') !!}
 {!! Html::style('shopping/plugins/mixitup/css/style.css') !!}
<!--  {!! Html::style('https://cdn.jsdelivr.net/jpages/0.7/css/jPages.css') !!} -->
 {!! Html::style(mix('css/pages/home.css', 'shopping')) !!}
@endpush

@section('content')

      <main class="cd-main-content" id="product-list">
        <div class="cd-tab-filter-wrapper">
          <div class="cd-tab-filter">
            <ul class="cd-filters">
              <li class="placeholder">
                <a data-type="all" href="javascript:void(0)">{{$lang['all']}}</a> <!-- selected option on mobile -->
              </li>
<!--               <li class="filter"><a class="selected" href="#0" data-type="all" id="allitems">All</a></li>
              <li class="filter" data-filter=".color-1"><a href="#0" data-type="color-1" id="onsale" >On Sale</a></li> -->
              <li class="filter"><a class="selected" href="javascript:void(0)" data-type="all" id="allitems">{{$lang['all']}}</a></li>
              <li class="filter" data-filter=".color-1"><a href="javascript:void(0)" data-type="color-1">{{$lang['on-sale']}}</a></li>

              <!-- <li class="filter" data-filter=".color-2"><a href="#0" data-type="color-2">Color 2</a></li> -->
            </ul> <!-- cd-filters -->
          </div> <!-- cd-tab-filter -->
        </div> <!-- cd-tab-filter-wrapper -->

      <section class="cd-gallery" id="cdgallery">

        <div id="resultMessage" class="text-center hide">
          <h3 style="color:#009688;">
            {{ __('products.we-were-unable-to-find-results-for') }} 
            <span id="queryString"></span>
          </h3>
          <p>{{ __('products.we-will-display-all-of-our-products-instead') }}</p>
        </div>

        <ul class="product-list">
          <?php for ($x = 0; $x < 20; $x++) { ?>
              <div class="mix col-sm-3">
                  <div class="box-product-outer loading">
                    <div class="box-product" style="width: 241.77px;">
                      <div class="img-wrapper" style="background-color: #d4d4d4;">
                        <div class="option">
                        </div>
                      </div>
                      <h6><a>Loading...</a></h6>
                      <div class="price">
                        <div>
                          <span>Loading...</span> 
                        </div>
                      </div>
                      <div class="rating-block">
                          <div class="star-ratings-sprite-small"><span></span></div>
                      </div>
                    </div>
                  </div>
              </div>
            <?php }?>
            <product-item
              v-for="item in items"
                :item="item"
                :key="item.id"
              >
            </product-item>
          <li class="gap"></li>
          <li class="gap"></li>
          <li class="gap"></li>
        </ul>
        <div class="cd-fail-message">
          <img src="/shopping/img/general/no-results-found.jpg">
        </div>
      </section> <!-- cd-gallery -->
       <div id="pagination" class="pagination">
          <nav>
              <ul class="pagination">
                  <li v-if="pagination.current_page > 1">
                      <a href="javascript:void(0)" aria-label="Previous"
                         @click='changePage(pagination.current_page - 1)'>
                          <span aria-hidden="true">&laquo;</span>
                      </a>
                  </li>
                  <li v-for="page in pagesNumber"
                      v-bind:class="[ page == isActived ? 'active' : '']">
                      <a href="javascript:void(0)"
                         @click='changePage(page)'>@{{ page }}</a>
                  </li>
                  <li v-if="pagination.current_page < pagination.last_page">
                      <a href="javascript:void(0)" aria-label="Next"
                         @click='changePage(pagination.current_page + 1)'>
                          <span aria-hidden="true">&raquo;</span>
                      </a>
                  </li>
              </ul>
          </nav>
      </div>     
      <div class="cd-filter">
        <form>
          <div class="cd-filter-block hidden">
            <h4>Search</h4>

            <div class="cd-filter-content">
              <input type="text" placeholder="Product Name to Filter" @keyup='handleEnter' v-model='keyword' >
            </div> <!-- cd-filter-content -->
          </div> <!-- cd-filter-block -->

          <div class="cd-filter-block hidden">
            <h4>Browse by Category</h4>

            <ul class="cd-filter-content cd-filters list">
              <li>
                <input class="filter" data-filter=".check1" type="checkbox" id="checkbox1">
                  <label class="checkbox-label" for="checkbox1">Option 1</label>
              </li>

              <li>
                <input class="filter" data-filter=".check2" type="checkbox" id="checkbox2">
                <label class="checkbox-label" for="checkbox2">Option 2</label>
              </li>

              <li>
                <input class="filter" data-filter=".check3" type="checkbox" id="checkbox3">
                <label class="checkbox-label" for="checkbox3">Option 3</label>
              </li>
            </ul> <!-- cd-filter-content -->
          </div> <!-- cd-filter-block -->


          <div class="cd-filter-block hidden">
            <h4>Delivery & Payment</h4>

            <div class="cd-filter-content">
              <div class="cd-select cd-filters">
                <select class="filter" name="selectThis" id="selectThis">
                  <option value="">Choose an option</option>
                  <option value=".option1">Option 1</option>
                  <option value=".option2">Option 2</option>
                  <option value=".option3">Option 3</option>
                  <option value=".option4">Option 4</option>
                </select>
              </div> <!-- cd-select -->
            </div> <!-- cd-filter-content -->
          </div> <!-- cd-filter-block -->

          <div class="cd-filter-block">
            <h4>@lang('products.filter-by-category')</h4>

            <ul class="cd-filter-content list">
              @foreach($categoriesArray as $category)
                <li>
                  <a href="/product?search={{ $category['slug'] }}&type=filterByCategory" class="text-muted"
                    style="{{ (app('request')->input('type') 
                          && app('request')->input('type') == 'filterByCategory'
                          && app('request')->input('search') == $category['slug'] 
                          ) 
                          ? 'text-decoration:underline;color:#333333' 
                          : '' 
                    }}"
                  >
                    {{ (\App::getLocale() == 'en') ? $category['name'] : $category['cnName'] }} 
                    ({{ $category['count'] }})
                  </a>
                </li>
              @endforeach
            </ul>
          </div>

          <div class="cd-filter-block">
            <h4>{{$lang['sort-by']}}</h4>

            <ul class="cd-filter-content cd-filters list">
              <li>
                <input class="filter" data-filter=".radio1" type="radio" name="radioButton" id="radio1" @click="sortBy('rel')">
                <label class="radio-label" for="radio1">{{$lang['relevance']}}</label>
              </li>

              <li>
                <input class="filter" data-filter=".radio2" type="radio" name="radioButton" id="radio2" @click="sortBy('desc')" 
                {{ (app('request')->input('price') && app('request')->input('price') == 'desc') ? 'checked' : '' }}
                >
                <label class="radio-label" for="radio2">{{$lang['high-to-low']}}</label>
              </li>

              <li>
                <input class="filter" data-filter=".radio3" type="radio" name="radioButton" id="radio3" @click="sortBy('asc')"
                {{ (app('request')->input('price') && app('request')->input('price') == 'asc') ? 'checked' : '' }}
                >
                <label class="radio-label" for="radio3">{{$lang['low-to-high']}}</label>
              </li>
            </ul> <!-- cd-filter-content -->
          </div> <!-- cd-filter-block -->

          <div class="cd-filter-block">
            <h4>@lang('products.price-range')</h4>

            <div class="cd-filter-content">
              <div id="range-slider">
                <input type="text" id="range" value="" name="range" />
              </div>

              <button type="button" id="filterByPriceBtn" class="btn btn-raised btn-wyc-global-orange pull-right btn-sm">@lang('common.filter')</button>
            </div>
          </div> <!-- cd-filter-block -->

          <!-- <div class="cd-filter-block" style="clear:both;">
            <h4>@lang('products.filter-by-brand')</h4>

            <div class="cd-filter-content">
                @if(count($brands))

                  <div style="height:200px;overflow-y:scroll;">
                    @foreach($brands as $brand)
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" value="{{ $brand }}" @click="sortByBrand"
                          @foreach($selectedBrands as $selectedBrand)
                            @if($brand == $selectedBrand)
                              checked
                            @endif
                          @endforeach
                          > 
                            {{ $brand }}
                        </label>
                      </div>
                    @endforeach
                  </div>

                @else
                  <div class="text-danger text-center">-- @lang('products.no-brands-to-display') --</div>
                @endif
            </div>
          </div> --> <!-- cd-filter-block -->

        </form>

        <a href="javascript:void(0)" class="cd-close">{{$lang['close']}}</a>
      </div> <!-- cd-filter -->

      <a href="javascript:void(0)" class="cd-filter-trigger"><i class="fa fa-exchange"></i> {{$lang['filters']}}</a>
    </main> <!-- cd-main-content -->


@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('css/range-slider/normalize.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/range-slider/ion.rangeSlider.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/range-slider/ion.rangeSlider.skinFlat.css') }}" />
@endpush

@push('scripts')
 {!! Html::script('shopping/plugins/carousel/jquery.ez-plus.js') !!}
 {!! Html::script('shopping/plugins/mixitup/js/jquery.mixitup.min.js') !!}
 {!! Html::script(mix('js/pages/product-list.js','shopping')) !!}
 {!! Html::script('shopping/plugins/mixitup/js/main.js') !!}
<!--  {!! Html::script('https://cdn.jsdelivr.net/jpages/0.7/js/jPages.min.js') !!} -->

<!-- <script>
  var pagination = $('.pagination');

function setPagination(){
   pagination.jPages({
     containerID: 'product-list',
     perPage: 16,
     startPage: 1,
     startRange: 1,
     midRange: 3,
     endRange: 1,
     first: false,
     last: false
   });
}

function destroyPagination() {
  pagination.jPages('destroy');
};

setPagination();

$('#product-list').mixItUp({
  callbacks: {
    onMixLoad: function(state,futureState ){
      console.log('mix Loaded');
      //setPagination();
    },
    onMixStart: function(state,futureState ){
      destroyPagination();
      $('.cd-fail-message').fadeOut(200);
    },
    onMixEnd: function(state, futureState){
      console.log('mix End');
      setPagination();
    },
    onMixFail: function(){
      $('.cd-fail-message').fadeIn(200);
    }
  }
});
</script> -->

<script src="{{ asset('js/range-slider/ion.rangeSlider.js') }}"></script>
<script>

    function removeURLParam(url, param) {
       var urlparts= url.split('?');
       if (urlparts.length>=2)
       {
        var prefix= encodeURIComponent(param)+'=';
        var pars= urlparts[1].split(/[&;]/g);
        for (var i=pars.length; i-- > 0;)
         if (pars[i].indexOf(prefix, 0)==0)
          pars.splice(i, 1);
        if (pars.length > 0)
         return urlparts[0]+'?'+pars.join('&');
        else
         return urlparts[0];
       }
       else
        return url;
    }

    $(function () {

        $("#range").ionRangeSlider({
            type: 'double',
            grid: true,
            min: 0,
            from: 0
        });

        window.slider = $("#range").data("ionRangeSlider");

        $('#filterByPriceBtn').on('click', function() {
            var min = window.slider.result.from;
            var max = window.slider.result.to;
            
            var url = removeURLParam(removeURLParam(window.location.href, 'min'), 'max');

            window.location = url + '&min='+min+'&max='+max;
        });

    });
</script>
@endpush
