<!--  CREATE PRODUCT PAGE -->
@extends('layouts.master')

@section('title', 'Edit Your Product')

@push('styles')
   {!! Html::style(mix('css/pages/store.css','shopping')) !!}
   {!! Html::style(mix('css/pages/product/product-create.css', 'shopping')) !!}
   {!! Html::style(mix('css/pages/home.css', 'shopping')) !!}
   {!! Html::style(mix('css/pages/product/details.css', 'shopping')) !!}
   {!! Html::style('https://unpkg.com/vue-multiselect@2.0.0-beta.14/dist/vue-multiselect.min.css') !!}
   {!! Html::style('components/flag-icon-css/css/flag-icon.min.css') !!}

@endpush

@section('content')
  @include('product.create.product-form')
@endsection
@push('scripts')
  {!! Html::script('https://unpkg.com/vue-multiselect@2.0.0-beta.14') !!}
  {!! Html::script(mix('js/pages/product-edit.js','shopping')) !!}

  <script type="text/javascript">
      //var zoomConfig = {cursor: 'crosshair', zoomType: "inner" }; 
      var image = $('#gallery_01 a');
      var zoomImage = $('img#zoom_03');

      //zoomImage.elevateZoom(zoomConfig);//initialize zoom

      image.on('click', function(){
          // Remove old instance
          $('.zoomContainer').remove();
          zoomImage.removeData('elevateZoom');
          // Update source for images
          zoomImage.attr('src', $(this).data('image'));
          zoomImage.data('zoom-image', $(this).data('zoom-image'));
          // Reinitialize EZ
          //zoomImage.elevateZoom(zoomConfig);
      });
$('#sampleModal').on('shown.bs.modal', function (e) {
      var zoomConfig = {cursor: 'crosshair', zoomType: "inner" }; 
      var image2 = $('#sampleModal #gallery_02 a, #sampleModal a.color-variation');
      var zoomImage2 = $('#sampleModal img#zoom_03');

      zoomImage2.elevateZoom(zoomConfig);//initialize zoom

      image2.on('click', function(){
          image2.removeClass("active");
          $(this).addClass("active");
          // Remove old instance
          $('.zoomContainer').remove();
          zoomImage2.removeData('elevateZoom');
          // Update source for images
          zoomImage2.attr('src', $(this).data('image'));
          zoomImage2.data('zoom-image', $(this).data('zoom-image'));
          // Reinitialize EZ
          zoomImage2.elevateZoom(zoomConfig);
      });
})

$('#sampleModal').on('hidden.bs.modal', function (e) {
  $('.zoomContainer').remove();
})

    $('#previewModal').on('shown.bs.modal', function (e) {
        var zoomConfig = {cursor: 'crosshair', zoomType: "inner" }; 
        var image = $('#previewModal a.elevatezoom-gallery');
        var zoomImage = $('#previewModal img#zoom_03');

        image.on('click', function(){
          image.removeClass("active");
          $(this).addClass("active");
          // Update source for images
          zoomImage.attr('src', $(this).data('image'));
      });
    });

  </script>
@endpush