<modal id='previewModal'  size="modal-lg" content-size='12'>
	<template slot="modal-title">
        <h3>
            {{$lang['product-preview']}}
            <br>
            <small></small>
        </h3>
    </template>
    <template slot="modal-body">
    	<header class="full-content" id='product-details-container' style="padding-bottom:0px;">
    <!-- Main Content -->
    <div class="maindiv">
    <div class="">
      <div class="row">

        <!-- Image List -->
        <div class="col-sm-5 p-l-3 m-b-3">
          <div class="jqzoom">
              <img  id="zoom_03" :src="productForm.image1" :data-zoom-image="productForm.image1" width="320" height="280"  />

                <div id="gallery_02" class="m-t-2">
                    <a  class="elevatezoom-gallery active" data-update="" :data-image="productForm.image1" :data-zoom-image="productForm.image1">
                        <img :src="productForm.image1" width="47" height="47" />
                    </a>

                    <a  v-if="productForm.image2 !=''" class="elevatezoom-gallery active" data-update="" :data-image="productForm.image2" :data-zoom-image="productForm.image2">
                        <img :src="productForm.image2" width="47" height="47" />
                    </a>

                    <a  v-if="productForm.image3 !=''" class="elevatezoom-gallery active" data-update="" :data-image="productForm.image3" :data-zoom-image="productForm.image3">
                        <img :src="productForm.image3" width="47" height="47" />
                    </a>     

                    <a  v-if="productForm.image4 !=''" class="elevatezoom-gallery active" data-update="" :data-image="productForm.image4" :data-zoom-image="productForm.image4">
                        <img :src="productForm.image4" width="47" height="47" />
                    </a>

                    <a  v-if="productForm.image5 !=''" class="elevatezoom-gallery active" data-update="" :data-image="productForm.image5" :data-zoom-image="productForm.image5">
                        <img :src="productForm.image5" width="47" height="47" />
                    </a>

                    <a  v-if="productForm.image6 !=''" class="elevatezoom-gallery active" data-update="" :data-image="productForm.image6" :data-zoom-image="productForm.image6">
                        <img :src="productForm.image6" width="47" height="47" />
                    </a>             
                </div>
          </div>
        </div>
        <!-- End Image List -->


        <div class="col-sm-7"> 
          <div class="title-detail">
            @{{productForm.name}} 

            <div class="table-detail">
              <div class="price p-t-2" v-if="productForm.sale_price">
                <div v-if="productForm.fee_included">
                  <div>
                    <span class="price-old">
                      ₱@{{ formatPrice(productForm.price + productForm.vat + productForm.charge + productForm.shipping_fee) }}
                    </span>
                    ₱@{{ formatPrice(productForm.sale_price + productForm.vat + productForm.charge + productForm.shipping_fee) }}
                    <br>
                    <span class="ysv">(You save @{{ productForm.discount }}%)</span>
                  </div>
                </div>
                <div v-else>
                  <div>
                    <span class="price-old">₱@{{ formatPrice(parseFloat(productForm.price)) }}</span>
                    ₱@{{ formatPrice(parseFloat(productForm.sale_price)) }}
                    <span class="ysv">(You save @{{ productForm.discount }}%)</span>
                  </div>
                </div>
              </div>
              <div class="price p-t-2" v-else>
                <div>
                  <span v-if="productForm.fee_included">
                    ₱@{{ formatPrice(productForm.price + productForm.vat + productForm.charge + productForm.shipping_fee) }}
                  </span>
                  <span v-else>
                    ₱@{{ formatPrice(parseFloat(productForm.price)) }}
                  </span> 
                </div>
              </div>
            </div> <!-- table-detail -->


          </div> <!-- title-detail -->

          <span class="label label-warning arrowed" style="font-size: 10px" v-for="cat in productForm.category_id">
            @{{ cat.name }}
          </span>

          <table class="table table-detail m-t-2">
            <tbody>
              <tr v-if="productForm.cn_name">
                <td class="fxWd"><span class="flag-icon flag-icon-cn"></span> {{$lang['product-name']}}</td>
                <td  class="p-l-2">@{{ productForm.cn_name }}</td>
              </tr>
              <tr>
                <td class="fxWd">Shipping Fee</td>
                <td class="p-l-2">
                  <div v-if="productForm.fee_included">
                    <div><small><b>Shipping Fee Included </b></small></div>
                  </div>
                  <div v-else>
                    <div class="shipprice">
                      <b>₱@{{ formatPrice(parseFloat(productForm.vat + productForm.charge + productForm.shipping_fee)) }}</b>  
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>{{$lang['availability']}}</td>
                <td class="p-l-2" v-if="productForm.stock > 0">
                  <small><b>{{$lang['remaining-stock']}}: @{{productForm.stock}}</b></small>
                </td>
                <td class="p-l-2" v-else>
                  <span class="label label-danger arrowed p-x-2" >{{$lang['out-stock']}}</span>
                </td>
              </tr>
              <tr v-if="productForm.colorImage1 != '' || productForm.colorImage2 != '' || productForm.colorImage3 != '' || 
              productForm.colorImage4 != '' || productForm.colorImage5 != '' || productForm.colorImage6 != '' || productForm.colorImage7 != '' || productForm.colorImage8 != '' || productForm.colorImage9 != '' || productForm.colorImage10 != ''">
                <td>{{$lang['choose-color']}}</td>
                <td>
                  
                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage1 != ''" :data-image="productForm.colorImage1">
                    <img :src="productForm.colorImage1" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[0].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage2 != ''" :data-image="productForm.colorImage2">
                    <img :src="productForm.colorImage2" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[1].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage3 != ''" :data-image="productForm.colorImage3">
                    <img :src="productForm.colorImage3" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[2].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage4 != ''" :data-image="productForm.colorImage4">
                    <img :src="productForm.colorImage4" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[3].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage5 != ''" :data-image="productForm.colorImage5">
                    <img :src="productForm.colorImage5" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[4].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage6 != ''" :data-image="productForm.colorImage6">
                    <img :src="productForm.colorImage6" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[5].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage7 != ''" :data-image="productForm.colorImage7">
                    <img :src="productForm.colorImage7" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[6].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage8 != ''" :data-image="productForm.colorImage8">
                    <img :src="productForm.colorImage8" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[7].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage9 != ''" :data-image="productForm.colorImage9">
                    <img :src="productForm.colorImage9" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[8].label" data-selector="true"/>
                  </a>

                  <a class="elevatezoom-gallery color-variation" v-if="productForm.colorImage10 != ''" :data-image="productForm.colorImage10">
                    <img :src="productForm.colorImage10" width="36" height="36" data-toggle='tooltip' data-placement='bottom' :data-original-title="productForm.varcolor[9].label" data-selector="true"/>
                  </a>

                </td>
              </tr>
              <tr v-if="productForm.varsize != 0">
                <td>{{$lang['choose-size']}}</td>
                <td>
                  <select class="choose-size">
                    <option>{{$lang['size']}}</option>
                    <option v-for="varsi in productForm.varsize">@{{varsi.label}}</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="p-t-2">{{$lang['quantity']}}</td>
                <td>
                  <div class="input-qty">
                    <input id="qty" type='number' :max="productForm.stock" min='1' value="1" class=' text-right m-x-1'>
                    <span class="color-stock"><small><b>{{$lang['pieces']}} (@{{ productForm.stock }} {{$lang['in-stock']}})</small></b></span>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>

          <a id="buy-now" href="javascript:void(0)" class="btn btn-raised btn-primary btn-lg" ><i class="fa fa-shopping-bag"></i> {{$lang['buy-now']}}</a>                                   
          <a id="add-to-cart" href="javascript:void(0)" class="btn btn-raised btn-primary btn-lg"><i class="fa fa-shopping-cart"></i> {{$lang['add-to-cart']}}</a>

        </div>
        <!-- <div class="col-sm-2">
          <div class="store-details">
              <h5>Store Details</h5>
              <span><a href="" class="storename"></a></span>
              <p class="store-desc"></p>
              <span class="store-btn">Contact me: <a href="javascript:void(0)" class="btn btn-raised btn-default fa fa-envelope"> </a></span>
          </div>
        </div> -->

        <div class="row break">
        <div class="col-md-12">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#desc" aria-controls="desc" role="tab" data-toggle="tab">{{$lang['description']}}</a></li>
          </ul>
          <!-- End Nav tabs -->

          <!-- Tab panes -->
          <div class="tab-content tab-content-detail">

              <!-- Description Tab Content -->
              <div role="tabpanel" class="tab-pane active" id="desc">
                <div class="well">

                  <div class="store-subhead m-b-2 m-t-1">
                    <h4 class="sub-header">{{$lang['product-specifications']}} </h4>
                    <hr>
                  </div>

                  <table class="table specsTable">
                    <tbody>
                      <tr v-if="productForm.boxContent">
                        <td class="specsTitle" width="200">{{$lang['what-box']}} :</td>
                        <td>@{{productForm.boxContent}}</td>
                      </tr>
                      <tr v-if="productForm.brand">
                        <td class="specsTitle">{{$lang['brand']}} :</td>
                        <td>@{{productForm.brand}}</td>
                      </tr>
                      <tr v-if="productForm.weight">
                        <td class="specsTitle">{{$lang['weight']}} :</td>
                        <td>@{{productForm.weight}}</td>
                      </tr>
                      <tr v-if="productForm.wPeriod">
                        <td class="specsTitle">{{$lang['warranty-period']}} :</td>
                        <td>@{{productForm.wPeriod}}</td>
                      </tr>
                      <tr v-if="productForm.stock">
                        <td class="specsTitle">{{$lang['remaining-stock']}}</td>
                        <td>@{{productForm.stock}}</td>
                      </tr>
                    </tbody>
                  </table>

                  <div class="store-subhead m-b-2 m-t-3">
                    <h4 class="sub-header">{{$lang['product-description']}} </h4>
                    <hr>
                  </div>
                    <p>
                      <span v-html="productForm.description"></span>
                      <br><br><br>

                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage1">
                        <img :src="productForm.descimage1" class="img-responsive">
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage2">
                        <img :src="productForm.descimage2" class="img-responsive">
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage3">
                        <img :src="productForm.descimage3" class="img-responsive">
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage4">
                        <img :src="productForm.descimage4" class="img-responsive">
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage5">
                        <img :src="productForm.descimage5" class="img-responsive" >
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage6">
                        <img :src="productForm.descimage6" class="img-responsive">
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage7">
                        <img :src="productForm.descimage7" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage8">
                        <img :src="productForm.descimage8" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage9">
                        <img :src="productForm.descimage9" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage10">
                        <img :src="productForm.descimage10" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage11">
                        <img :src="productForm.descimage11" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage12">
                        <img :src="productForm.descimage12" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage13">
                        <img :src="productForm.descimage13" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage14">
                        <img :src="productForm.descimage14" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage15">
                        <img :src="productForm.descimage15" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage16">
                        <img :src="productForm.descimage16" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage17">
                        <img :src="productForm.descimage17" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage18">
                        <img :src="productForm.descimage18" class="img-responsive">
                      </div>
        
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage19">
                        <img :src="productForm.descimage19" class="img-responsive">
                      </div>
            
                      <div class="col-sm-12 p-x-5 m-b-3" v-if="productForm.descimage20">
                        <img :src="productForm.descimage20" class="img-responsive">
                      </div>

                      <div style="height:1px;clear:both;"></div>

                    </p>                  
                </div>
              </div>
          </div>
          <!-- End Tab panes -->

        </div>
        </div>
      </div>

    </div>
    </div>
    <!-- End Main Content -->
</header>
    </template>
    <template slot="modal-footer">
	     <div class='col-md-12'  data-dismiss="modal" aria-label="Close">
			<button class='btn btn-default'>{{$lang['close']}}</button>
	    </div>
    </template>
</modal>


<!-- Sample Product Details--> 
<modal id='sampleModal'  size="modal-lg" content-size='12'>
	<template slot="modal-title">
        <h3>
            {{$lang['sample-product']}}
            <br>
            <small></small>
        </h3>
    </template>
    <template slot="modal-body">
    	<header class="full-content" id='product-details-container' style="padding-bottom:0px;">
    <!-- Main Content -->
    <div class="maindiv">
    <div class="">
      <div class="row">

        <!-- Image List -->
        <div class="col-sm-5 p-l-3 m-b-3">
          <div class="jqzoom">
              <img  id="zoom_03" src="/images/product_preview/a.jpg" width="320" height="280"  />

                <div id="gallery_02" class="m-t-2">
                    <a  class="elevatezoom-gallery active" data-update="" data-image="/images/product_preview/a.jpg" data-zoom-image="/images/product_preview/a.jpg">
                        <img src="/images/product_preview/a.jpg" width="47" height="47"  />
                    </a>   

                    <a  class="elevatezoom-gallery" data-update="" data-image="/images/product_preview/b.jpg" data-zoom-image="/images/product_preview/b.jpg">
                        <img src="/images/product_preview/b.jpg" width="47"  height="47"/>
                    </a>  

                    <a  class="elevatezoom-gallery" data-update="" data-image="/images/product_preview/c.jpg" data-zoom-image="/images/product_preview/c.jpg">
                        <img src="/images/product_preview/c.jpg" width="47"  height="47"/>
                    </a>

                    <a  class="elevatezoom-gallery" data-update="" data-image="/images/product_preview/e.jpg" data-zoom-image="/images/product_preview/d.jpg">
                        <img src="/images/product_preview/e.jpg" width="47"  height="47"/>
                    </a> 

                    <a  class="elevatezoom-gallery" data-update="" data-image="/images/product_preview/f.jpg" data-zoom-image="/images/product_preview/e.jpg">
                        <img src="/images/product_preview/f.jpg" width="47"  height="47"/>
                    </a> 

                    <a  class="elevatezoom-gallery" data-update="" data-image="/images/product_preview/g.jpg" data-zoom-image="/images/product_preview/f.jpg">
                        <img src="/images/product_preview/g.jpg" width="47"  height="47"/>
                    </a>                
                </div>
          </div>
        </div>
        <!-- End Image List -->

        <div class="col-sm-7"> 
          <div class="title-detail">
            Iphone 7 Plus 128gb 

            <div class="table-detail">
              <div class="price p-t-2">
                <div>
                  <span class="price-old">
                    ₱43999.00
                  </span>
                  ₱39599.10
                  <br>
                  <span class="ysv">(You save 10%)</span>
                </div>
              </div>
            </div> <!-- table-detail -->
          </div> <!-- title-detail -->

          <span class="label label-warning arrowed" style="font-size: 10px">
            Electronics
          </span>

          <span class="label label-warning arrowed" style="font-size: 10px">
            Mobiles & Tablets
          </span>

          <span class="label label-warning arrowed" style="font-size: 10px">
            Smart Phones
          </span>

          <table class="table table-detail m-t-2">
            <tbody>
                <tr>
                    <td class="fxWd">Shipping Fee</td>
                    <td class="p-l-2">
                        <div>
                            <div class="shipprice"><b>₱100.00</b></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Availability</td>
                    <td class="p-l-2"><small><b>Remaining Stock: 50</b></small></td>
                </tr>
                <tr>
                    <td>Choose Color</td>
                    <td>
                        <a data-image="/images/product_preview/JetBlack.png" class="elevatezoom-gallery color-variation"><img src="/images/product_preview/JetBlack.png" width="36" height="36" data-toggle="tooltip" data-placement="bottom" data-original-title="Jet Black" data-selector="true"></a>

                        <a data-image="/images/product_preview/Black.png" class="elevatezoom-gallery color-variation"><img src="/images/product_preview/Black.png" width="36" height="36" data-toggle="tooltip" data-placement="bottom" data-original-title="Black" data-selector="true"></a>

                        <a data-image="/images/product_preview/Silver.png" class="elevatezoom-gallery color-variation"><img src="/images/product_preview/Silver.png" width="36" height="36" data-toggle="tooltip" data-placement="bottom" data-original-title="Silver" data-selector="true"></a>

                        <a data-image="/images/product_preview/Gold.png" class="elevatezoom-gallery color-variation"><img src="/images/product_preview/Gold.png" width="36" height="36" data-toggle="tooltip" data-placement="bottom" data-original-title="Gold" data-selector="true"></a>

                        <a data-image="/images/product_preview/RoseGold.png" class="elevatezoom-gallery color-variation"><img src="/images/product_preview/RoseGold.png" width="36" height="36" data-toggle="tooltip" data-placement="bottom" data-original-title="Rose Gold" data-selector="true"></a>
                    </td>
                </tr>
                <!-- <tr>
                    <td>Choose Size</td>
                    <td>
                        <select class="choose-size">
                            <option>Size</option>
                            <option>XL</option>
                            <option>L</option>
                            <option>M</option>
                            <option>S</option>
                        </select>
                    </td>
                </tr> -->
                <tr>
                    <td class="p-t-2">Quantity</td>
                    <td>
                        <div class="input-qty">
                            <input id="qty" type="number" max="50" min="1" value="1" class=" text-right m-x-1"> <span class="color-stock"><small><b>Pieces (50 in stock)</b></small></span></div>
                    </td>
                </tr>
            </tbody>
          </table>

          <a id="buy-now" href="javascript:void(0)" class="btn btn-raised btn-primary btn-lg" ><i class="fa fa-shopping-bag"></i> {{$lang['buy-now']}}</a>                                   
          <a id="add-to-cart" href="javascript:void(0)" class="btn btn-raised btn-primary btn-lg"><i class="fa fa-shopping-cart"></i> {{$lang['add-to-cart']}}</a>

        </div>
        <!-- <div class="col-sm-2">
          <div class="store-details">
              <h5>Store Details</h5>
              <span><a href="" class="storename"></a></span>
              <p class="store-desc"></p>
              <span class="store-btn">Contact me: <a href="javascript:void(0)" class="btn btn-raised btn-default fa fa-envelope"> </a></span>
          </div>
        </div> -->

        <div class="row break">
        <div class="col-md-12">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#desc" aria-controls="desc" role="tab" data-toggle="tab">{{$lang['description']}}</a></li>
          </ul>
          <!-- End Nav tabs -->

          <!-- Tab panes -->
          <div class="tab-content tab-content-detail">

              <!-- Description Tab Content -->
              <div role="tabpanel" class="tab-pane active" id="desc">
                <div class="well">

                  <div class="store-subhead m-b-2 m-t-1">
                    <h4 class="sub-header">{{$lang['product-specifications']}} </h4>
                    <hr>
                  </div>

                  <table class="table specsTable">
                    <tbody>
                      <tr>
                        <td class="specsTitle" width="200">{{$lang['what-box']}} :</td>
                        <td>{{$lang['1xiphone7']}}</td>
                      </tr>
                      <tr>
                        <td class="specsTitle">{{$lang['brand']}} :</td>
                        <td>{{$lang['apple']}}</td>
                      </tr>
                      <tr>
                        <td class="specsTitle">{{$lang['weight']}} :</td>
                        <td>188g</td>
                      </tr>
                      <tr>
                        <td class="specsTitle">{{$lang['warranty-desc']}} :</td>
                        <td>{{$lang['2years']}}</td>
                      </tr>
                      <tr>
                        <td class="specsTitle">{{$lang['remaining-stock']}}</td>
                        <td>100</td>
                      </tr>
                    </tbody>
                  </table>

                  <div class="store-subhead m-b-2 m-t-3">
                    <h4 class="sub-header">{{$lang['product-description']}}</h4>
                    <hr>
                  </div>
                    <p>
                      {{$lang['apples-iphone-7']}}
                      <br><br><br>
                      
                      <div class="col-sm-12 p-x-5 m-b-3">
                        <img src="/images/product_preview/desc/1.jpg" class="img-responsive" >
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3">
                        <img src="/images/product_preview/desc/2.jpg" class="img-responsive" >
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3">
                        <img src="/images/product_preview/desc/3.jpg" class="img-responsive" >
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3">
                        <img src="/images/product_preview/desc/4.jpg" class="img-responsive" >
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3">
                        <img src="/images/product_preview/desc/5.jpg" class="img-responsive" >
                      </div>

                      <div class="col-sm-12 p-x-5 m-b-3">
                        <img src="/images/product_preview/desc/6.jpg" class="img-responsive" >
                      </div>

                      <div style="height:1px;clear:both;"></div>


                    </p>


                  
                </div>
              </div>
          </div>
          <!-- End Tab panes -->

        </div>
        </div>
      </div>

    </div>
    </div>
    <!-- End Main Content -->
</header>
    </template>
    <template slot="modal-footer">
	     <div class='col-md-12'  data-dismiss="modal" aria-label="Close">
			<button class='btn btn-default'>{{$lang['close']}}</button>
	    </div>
    </template>
</modal>