<div class="full-content custom-chosen" id="productCreate" v-cloak>
  @include('product.create.modals')
<!--   <form id="product-form" role="form" method="POST" action="{{ route('shopping.product.index') }}"
@submit.prevent="submitProduct" novalidate> -->
    <form id="product-form" role="form" method="PUT" action="{{ route('shopping.product.update', $product->slug) }}" @submit.prevent="submitProduct" novalidate>
  <div class="container" id="product1">
    <div class="title m-t-2"><span>{{$lang['create-product']}}</span><a class="btn-raised btn-link pull-right m-b-2" data-toggle="modal" href="#sampleModal">{{$lang['click']}}</a></div>
      <div id="primaryDetails" class="p-t-3 m-t-3">
        <div class="col-sm-7">
            <div class="col-sm-12 store-input-set">
              <div class="store-subhead">
                <h4 class="sub-header">{{$lang['primary-details']}}</h4>
                <hr>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('name') }">
                  <label class="control-label" for="name">
                    <span class="flag-icon flag-icon-us"></span> {{$lang['product-name']}} <small>{{$lang['english']}}</small>
                  </label>
                  <input class="form-control" id="name" type="text" v-model="productForm.name">
                  <p class="help-block">{{$lang['please-indicate1']}}</p>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating">
                  <label class="control-label" for="cn_name">
                    <span class="flag-icon flag-icon-cn"></span> {{$lang['product-name']}} <small>{{$lang['chinese']}}</small>
                  </label>
                  <input class="form-control" id="cn_name" type="text" v-model="productForm.cn_name">
                  <p class="help-block">{{$lang['please-indicate2']}}</p>
                </div>
              </div>

              <div class="col-sm-12">
                  <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('store_list') }">
                      <!-- <label class="control-label" for="store_id">Store Name</label> -->
                      <input type="hidden" id="store_id" v-model="productForm.store_id">
                      <multiselect id="store_id" v-model="productForm.store_list" deselect-label="{{$lang['remove']}}" track-by="id" label="name" placeholder="{{$lang['select-store']}}" :options="productForm.storeOption" :searchable="false" :allow-empty="true"></multiselect>
                      <p class="help-block">{{$lang['please-select-store']}}</p>
                  </div>
              </div>

              <div class="col-sm-12">
                  <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('category_id') }">
                      <multiselect class="custom-select" v-model="productForm.category_id" id="category_id" :options="productForm.catOption" :multiple="true" :max="4" :close-on-select="true" :clear-on-select="false" :allow-empty="true" :hide-selected="true" :placeholder="productForm.cat_holder" label="name" track-by="name" @input="checkMulti()"></multiselect>
                      <p class="help-block">{{$lang['you-need-choose']}}</p>
                  </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('price') }">
                    <label class="control-label" for="price">{{$lang['price']}}</label>
                    <input class="form-control" id="price" type="text" v-model.number="productForm.price" >
                    <p class="help-block">{{$lang['this-product-orig']}}</p>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label" for="sale_price">{{$lang['discounted-price']}}</label>
                    <input class="form-control" id="sale_price" type="text" v-model.number="productForm.sale_price" @blur="greaterThan()">
                    <p class="help-block">{{$lang['this-product-price']}}</p>
                </div>
              </div>

              <div class="col-sm-12" style="z-index: 0">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('primaryImage') }">
                  <div class="input-group">
                    <label class="control-label" for="primaryImage">{{$lang['primary-image']}}</label>
                    <input type="text" id="primaryImage" class="form-control" disabled="" v-model="productForm.primaryImage">
                    <p class="help-block">{{$lang['this-product-always']}}</p>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-raised btn-upload btn-has-loading" type="button">{{$lang['upload']}}</button>
                        <div class="avatar-uploader">
                          <imgfileupload data-event="profileOnLoad" data-loading-event="primaryLoading" data-url="/common/resize-whole-image?width=518&height=454&type=primary"></imgfileupload>
                          <upload-loader :loading="primaryLoading" class="v-spinner-primary" :progress="progress"></upload-loader>
                          <!-- <beat-loader :loading="primaryLoading" class="v-spinner-primary" size="16px"></beat-loader> -->
                        </div>
                    </span>
                  </div>
                </div>
              </div>
            </div><!-- end store-input-set -->

        </div><!-- end col-sm-7 -->
        <div class="col-sm-1"></div>
        <div class="col-sm-4">
          <div class="col-sm-12">
                <div class="box">
                    <div class="box-icon">
                        <span class="fa fa-2x fa-eercast"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-center">{{$lang['product-preview']}}</h4>
                        <div class="box-product-outer">
                          <div class="box-product">
                            <div class="img-wrapper">
                                <div class="avatar">
                                  <a :href="productForm.image" v-if="productForm.image" data-lightbox="image-1">
                                    <img class="product-profile-photo" :src="productForm.image" v-if="productForm.image">
                                  </a>
<!--                                   <div class="avatar-uploader">
                                    <div class="avatar-upload-text">Upload</div>
                                    <imgfileupload data-event="profileOnLoad" data-url="/common/resize-image?width=240&height=200"></imgfileupload>
                                  </div> -->
                                </div>
                              <div id="tags" class="tags">
                                <span class="label-tags"><span class="label label-danger arrowed" id="pDiscount">@{{productDiscount}}</span></span>
                              </div>
                            </div>
                            <h6><a href="#" id="pName">@{{productName}}</a></h6>
                            <div class="price">
                              <div><b id="pPrice">@{{productPrice}}</b><i class="price-old" id="pPriceOld">@{{productPriceOld}}</i></div>

                            </div>
<!--                             <div class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star-half-o"></i>
                              <i class="fa fa-star-o"></i>
                              <a href="#">(5 reviews)</a>
                            </div> -->
                          </div>
                        </div>
                    </div>
                </div>
          </div>
        </div>
      </div> <!-- end primary details -->

      <div id="shipping">
        <div class="col-sm-12 p-t-3 m-t-3">
            <div class="col-sm-12 store-input-set">
              <div class="store-subhead">
                <h4 class="sub-header">{{$lang['shipping-details']}}</h4>
                <hr>
                <h5>{{$lang['pack']}}</h5>
              </div>
            <div class="col-sm-7 m-t-1">

                <div class="">
                  <div class="form-group label-floating col-sm-3">
                      <label class="control-label" for="weight">{{$lang['weight']}} (kg)</label>
                      <input class="form-control" id="weight" v-model.number="productForm.weight">
                      <small class="text-danger " v-if="productForm.errors.has('weight')">{{$lang['actual-weight']}}</small>
                  </div>                
                  <div class="form-group label-floating col-sm-3">
                      <label class="control-label" for="length">{{$lang['length']}}</label>
                      <input class="form-control" id="length" v-model.number="productForm.length">
                      <small class="text-danger " v-if="productForm.errors.has('length')">{{$lang['package-lenght']}}</small>
                  </div>
                  <div class="form-group label-floating col-sm-3">
                      <label class="control-label" for="width">{{$lang['width']}} (cm)</label>
                      <input class="form-control" id="width" v-model.number="productForm.width">
                      <small class="text-danger " v-if="productForm.errors.has('width')">{{$lang['package-width']}}</small>
                  </div>
                  <div class="form-group label-floating col-sm-3">
                      <label class="control-label" for="height">{{$lang['height']}} (cm)</label>
                      <input class="form-control" id="height" v-model.number="productForm.height">
                      <small class="text-danger " v-if="productForm.errors.has('height')">{{$lang['package-height']}}</small>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" 
                             id="chk_display"
                             v-model="insurance"> {{$lang['insurance']}}
                    </label>
                  </div>
<!--                   <div class="checkbox">
                    <label>
                      <input type="checkbox" 
                             id="chk_include"
                             v-model="productForm.fee_included"> {{$lang['include-ship']}}
                    </label>
                  </div> -->
                </div>
            </div>
              <div class="col-sm-5">
                  <div class="col-sm-3">
                    
                  </div>
                  <div class="col-sm-9 form-group jqzoom">
                    <div class="col-sm-12">
                      <span class="fee-label">{{$lang['actual-weight']}} (kg): <b class="pull-right">@{{ formatPrice(productForm.weight) }}</b></span><br>
                      <span class="fee-label">{{$lang['volume']}} (kg): <b class="pull-right">@{{ formatPrice(volume) }}</b></span><br>
                      <span class="fee-label">{{$lang['charge']}} : <b class="pull-right">@{{ formatPrice(valuationCharge) }}</b></span><br>
                      <span class="fee-label">{{$lang['vat']}} : <b class="pull-right">@{{ formatPrice(tax) }}</b></span><br><br>
                      <span class="fee-label"><b>{{$lang['total-fee']}} : </b><b class="pull-right">@{{ formatPrice(totalfee) }}</b></span><br>
                      <span class="fee-label"><b>{{$lang['total-price']}} : </b><b class="pull-right">@{{ formatPrice(totalprice) }}</b></span><br>
                    </div>
                    <!-- <div class="col-sm-6">
                     <span class="fee-label">{{$lang['charge']}}  : <b>@{{ formatPrice(valuationCharge) }}</b></span><br><br>
                     <span class="fee-label">{{$lang['vat']}}  : <b>@{{ formatPrice(tax) }}</b></span><br><br>
                     <span class="fee-label"><b>{{$lang['total-fee']}}  : @{{ formatPrice(totalfee) }}</b></span><br><br>
                     <span class="fee-label"><b>{{$lang['total-price']}}  : @{{ formatPrice(totalprice) }}</b></span><br><br>
                    </div> -->
                  </div>
              </div>

            </div>
        </div>
      </div>

      <div id="specs">
        <div class="col-sm-12 p-t-3 m-t-3">
            <div class="col-sm-12 store-input-set">
              <div class="store-subhead">
                <h4 class="sub-header">{{$lang['product-desc-specs']}}</h4>
                <hr>
              </div>
              <div class="col-sm-12">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('boxContent') }">
                  <label class="control-label" for="boxContent">{{$lang['what-box']}}</label>
                  <input class="form-control" id="boxContent" type="text" v-model="productForm.boxContent">
                  <p class="help-block">{{$lang['importan-understand']}}</p>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group" :class="{ 'has-error': productForm.errors.has('description') }">
                  <label class="control-label" for="description">
                    <span class="flag-icon flag-icon-us"></span> {{$lang['product-description']}} <small>{{$lang['english']}}</small>
                  </label>
                  <quill-editor v-model="productForm.description" :options="editorOptions"> </quill-editor>
                  <p class="help-block">{{$lang['give-a-brief']}}</p>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group" >
                  <label class="control-label" for="cn_description">
                    <span class="flag-icon flag-icon-cn"></span> {{$lang['product-description']}} <small>{{$lang['chinese']}}</small>
                  </label>
                  <quill-editor v-model="productForm.cn_description" :options="editorOptions"> </quill-editor>
                  <p class="help-block">{{$lang['give-a-chinese']}}</p>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('brand') }">
                    <label class="control-label" for="brand">{{$lang['brand']}}</label>
                    <input class="form-control" id="brand" type="text" v-model="productForm.brand">
                    <p class="help-block">{{$lang['please-brand']}}</p>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('stock') }">
                    <label class="control-label" for="stock">{{$lang['stock']}}</label>
                    <input class="form-control" id="stock" type="number" v-model="productForm.stock" min="0">
                    <p class="help-block">{{$lang['please-stocks']}}</p>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label" for="wPeriod">{{$lang['warranty-desc']}}</label>
                    <input class="form-control" id="wPeriod" type="text" v-model="productForm.wPeriod">
                    <p class="help-block">{{$lang['please-describe']}}</p>
                </div>
              </div>

<!--              <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label" for="weight">{{$lang['weight']}}</label>
                    <input class="form-control" id="weight" type="text" v-model="productForm.weight">
                    <p class="help-block">{{$lang['please-weight']}}</p>
                </div>
              </div> -->

<!--             <div class="row">
              <div class="col-md-2">
                <a href="javascript:void(0)" class="btn btn-default btn-raised" id="btn-add-item-spec"><i class="fa fa-plus"></i> Add Item Spec</a>
              </div>
            </div> -->
            </div><!-- end store-input-set -->

        </div><!-- end col-sm-12 -->
      </div> <!-- end specs -->

      <div id="variations">
        <div class="col-sm-12 p-t-3 m-t-3">
            <div class="col-sm-12 store-input-set">
              <div class="store-subhead">
                <h4 class="sub-header">{{$lang['product-variations']}}</h4>
                <hr>
              </div>

              <div>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-raised btn-var" type="button" @click="addColor"><i class="fa fa-plus"></i>  {{$lang['add-color']}}</button>
                    </span>
                  </div>
                </div>
              </div>

              <div class="item-spec-container-1">
                <div class="row border-row">
                <div id="colorDiv1" class="col-sm-6 border-right" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[0].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[0].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[0].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage1" v-if="productForm.colorImage1" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage1" v-if="productForm.colorImage1"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad1'" :data-loading-event="'colorLoading1'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading1" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize1"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize1">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock1" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize1(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                
                <div id="colorDiv2" class="col-sm-6 border-left" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[1].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[1].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                          
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>
                    </div>                 
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[1].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage2" v-if="productForm.colorImage2" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage2" v-if="productForm.colorImage2"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad2'" :data-loading-event="'colorLoading2'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading2" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize2"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize2">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock2" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize2(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>

                <div class="row border-row">
                <div id="colorDiv3" class="col-sm-6 border-right" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[2].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[2].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                         
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>                        
                    </div>                  
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[2].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage3" v-if="productForm.colorImage3" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage3" v-if="productForm.colorImage3"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad3'" :data-loading-event="'colorLoading3'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading3" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize3"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize3">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock3" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize3(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <div id="colorDiv4" class="col-sm-6 border-left" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[3].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[3].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                        
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>                          
                    </div>
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[3].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage4" v-if="productForm.colorImage4" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage4" v-if="productForm.colorImage4"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad4'" :data-loading-event="'colorLoading4'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading4" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize4"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize4">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock4" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize4(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>

                <div class="row border-row">
                <div id="colorDiv5" class="col-sm-6 border-right" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[4].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[4].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                         
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>                        
                    </div> 
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[4].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage5" v-if="productForm.colorImage5" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage5" v-if="productForm.colorImage5"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad5'" :data-loading-event="'colorLoading5'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading5" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize5"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize5">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock5" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize5(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <div id="colorDiv6" class="col-sm-6 border-left" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[5].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[5].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                          
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>                      
                    </div>                    
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[5].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage6" v-if="productForm.colorImage6" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage6" v-if="productForm.colorImage6"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad6'" :data-loading-event="'colorLoading6'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading6" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize6"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize6">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock6" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize6(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>

                <div class="row border-row">
                <div id="colorDiv7" class="col-sm-6 border-right" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[6].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[6].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                         
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>                       
                    </div>
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[6].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage7" v-if="productForm.colorImage7" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage7" v-if="productForm.colorImage7"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad7'" :data-loading-event="'colorLoading7'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading7" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize7"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize7">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock7" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize7(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <div id="colorDiv8" class="col-sm-6 border-left" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[7].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[7].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                          
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>
                    </div>                  
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[7].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage8" v-if="productForm.colorImage8" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage8" v-if="productForm.colorImage8"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad8'" :data-loading-event="'colorLoading8'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading8" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize8"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize8">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock8" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize8(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>

                <div class="row border-row">
                <div id="colorDiv9" class="col-sm-6 border-right" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[8].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[8].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                         
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>
                    </div>                   
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[8].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage9" v-if="productForm.colorImage9" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage9" v-if="productForm.colorImage9"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad9'" :data-loading-event="'colorLoading9'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading9" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize9"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize9">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock9" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize9(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <div id="colorDiv10" class="col-sm-6 border-left" style="display: none">
                  <div class="col-sm-4 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['color']}}/{{$lang['design-name']}}</label>
                        <input class="form-control color colorHide colorRequired" type="text" v-model="productForm.varcolor[9].label">
                        <a class="delete-spec" href="javascript:void(0)" @click="deleteColor(productForm.varcolor[9].temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>                        
                        <p class="help-block">{{$lang['please-color']}}/{{$lang['design-name']}}</p>
                    </div>                    
                  </div>
                  <div class="col-sm-2 margin">
                    <div class="form-group label-floating">
                        <label class="control-label">{{$lang['stock']}}</label>
                        <input class="form-control stock colorRequired" type="number" min="0" v-model="productForm.varcolor[9].stock">
                        <p class="help-block">{{$lang['please-stocks']}}</p>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <div class="add-desc colorImageRequired">
                          <a :href="productForm.colorImage10" v-if="productForm.colorImage10" data-lightbox="image-1">
                          <img class="add-desc-photo" :src="productForm.colorImage10" v-if="productForm.colorImage10"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'colorOnLoad10'" :data-loading-event="'colorLoading10'" data-url="/common/resize-whole-image?width=518&height=454"></imgfileupload>
                              <upload-loader :loading="colorLoading10" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-var" type="button" @click="addSize10"><i class="fa fa-plus"></i>  {{$lang['add-size']}}</button>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="item-spec-container-2" >
                    <div class="row">
                      <div class="margin" v-for="varsi in productForm.varsize10">
                        <div class="form-group label-floating col-sm-3">
                            <label class="control-label">{{$lang['size']}}</label>
                            <input class="form-control requiredsize" type="text" v-model="varsi.label">
                            <p class="help-block">{{$lang['please-size']}}</p>
                        </div>
                        <div class="form-group label-floating col-sm-2">
                            <label class="control-label">{{$lang['stock']}}</label>
                            <input class="form-control stock10" type="number" min="0" v-model="varsi.stock">
                            <p class="help-block">{{$lang['please-stocks']}}</p>
                            <a class="delete-spec2" href="javascript:void(0)" @click="deleteSize10(varsi.id,varsi.temp_id)" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>

              </div>

            </div>
        </div>
      </div>

      <div id="main-photos">
        <div class="col-sm-12 p-t-3 m-t-3">
            <div class="col-sm-12 store-input-set">
              <div class="store-subhead">
                <h4 class="sub-header">{{$lang['product-main']}} <i class="inote">{{$lang['you-main']}}</i></h4>
                <hr>
              </div>

          <div class="jqzoom m-t-3 col-sm-4">
              <img  id="zoom_03" :src="productForm.image1" v-if="productForm.image1" :data-zoom-image="productForm.image1" width="320" height="280"/>

                <div id="gallery_01">

                    <a  class="elevatezoom-gallery active" data-update="" :data-image="productForm.image1" :data-zoom-image="productForm.image1">
                        <img :src="productForm.image1" width="54" height="54" />
                    </a>

                    <a  class="elevatezoom-gallery active" data-update="" :data-image="productForm.image2" :data-zoom-image="productForm.image2">
                        <img :src="productForm.image2" width="54" height="54" />
                    </a>

                    <a  class="elevatezoom-gallery active" data-update="" :data-image="productForm.image3" :data-zoom-image="productForm.image3">
                        <img :src="productForm.image3" width="54" height="54" />
                    </a>     

                    <a  class="elevatezoom-gallery active" data-update="" :data-image="productForm.image4" :data-zoom-image="productForm.image4">
                        <img :src="productForm.image4" width="54" height="54" />
                    </a>

                    <a  class="elevatezoom-gallery active" data-update="" :data-image="productForm.image5" :data-zoom-image="productForm.image5">
                        <img :src="productForm.image5" width="54" height="54" />
                    </a>

                    <a  class="elevatezoom-gallery active" data-update="" :data-image="productForm.image6" :data-zoom-image="productForm.image6">
                        <img :src="productForm.image6" width="54" height="54" />
                    </a> 
                </div>
          </div>

          <div class="col-sm-8 m-t-3">
              <div class="col-sm-12 margin">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('primaryImage2') }">
                  <div class="input-group">
                    <label class="control-label" for="primaryImage2">{{$lang['primary-image']}} #2</label>
                    <input type="text" id="primaryImage2" class="form-control" disabled="" v-model="productForm.primaryImage2">
                    <p class="help-block">{{$lang['this-product-always']}}</p>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-raised btn-upload" type="button"> <i class="img2 fa fa-spinner fa-spin hide"></i> {{$lang['upload']}}</button>
                        <div class="avatar-uploader">
                          <imgfileupload data-event="profileOnLoad2" data-loading-event="primaryLoading2" data-url="/common/resize-whole-image?width=518&height=454&type=primary"></imgfileupload>
                          <upload-loader :loading="primaryLoading2" class="v-spinner-primary" :progress="progress"></upload-loader>
                          <!-- <beat-loader :loading="primaryLoading2" class="v-spinner-primary" size="16px"></beat-loader> -->
                        </div>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('primaryImage3') }">
                  <div class="input-group">
                    <label class="control-label" for="primaryImage3">{{$lang['primary-image']}} #3</label>
                    <input type="text" id="primaryImage3" class="form-control" disabled="" v-model="productForm.primaryImage3">
                    <p class="help-block">{{$lang['this-product-always']}}</p>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-raised btn-upload" type="button"><i class="img3 fa fa-spinner fa-spin hide"></i> {{$lang['upload']}}</button>
                        <div class="avatar-uploader">
                          <imgfileupload data-event="profileOnLoad3" data-loading-event="primaryLoading3" data-url="/common/resize-whole-image?width=518&height=454&type=primary"></imgfileupload>
                          <upload-loader :loading="primaryLoading3" class="v-spinner-primary" :progress="progress"></upload-loader>
                          <!-- <beat-loader :loading="primaryLoading3" class="v-spinner-primary" size="16px"></beat-loader> -->
                        </div>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('primaryImage4') }">
                  <div class="input-group">
                    <label class="control-label" for="primaryImage4">{{$lang['primary-image']}} #4</label>
                    <input type="text" id="primaryImage4" class="form-control" disabled="" v-model="productForm.primaryImage4">
                    <p class="help-block">{{$lang['this-product-always']}}</p>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-raised btn-upload" type="button"><i class="img4 fa fa-spinner fa-spin hide"></i> {{$lang['upload']}}</button>
                        <div class="avatar-uploader">
                          <imgfileupload data-event="profileOnLoad4"  data-loading-event="primaryLoading4" data-url="/common/resize-whole-image?width=518&height=454&type=primary"></imgfileupload>
                          <upload-loader :loading="primaryLoading4" class="v-spinner-primary" :progress="progress"></upload-loader>
                          <!-- <beat-loader :loading="primaryLoading4" class="v-spinner-primary" size="16px"></beat-loader> -->
                        </div>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('primaryImage5') }">
                  <div class="input-group">
                    <label class="control-label" for="primaryImage5">{{$lang['primary-image']}} #5</label>
                    <input type="text" id="primaryImage5" class="form-control" disabled="" v-model="productForm.primaryImage5">
                    <p class="help-block">{{$lang['this-product-always']}}</p>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-raised btn-upload" type="button"><i class="img5 fa fa-spinner fa-spin"></i> {{$lang['upload']}}</button>
                        <div class="avatar-uploader">
                          <imgfileupload data-event="profileOnLoad11" data-loading-event="primaryLoading5" data-url="/common/resize-whole-image?width=998&height=874&type=primary"></imgfileupload>
                          <upload-loader :loading="primaryLoading5" class="v-spinner-primary" :progress="progress"></upload-loader>
                        </div>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group label-floating" :class="{ 'has-error': productForm.errors.has('primaryImage6') }">
                  <div class="input-group">
                    <label class="control-label" for="primaryImage6">{{$lang['primary-image']}} #6</label>
                    <input type="text" id="primaryImage6" class="form-control" disabled="" v-model="productForm.primaryImage6">
                    <p class="help-block">{{$lang['this-product-always']}}</p>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-raised btn-upload" type="button"><i class="img6 fa fa-spinner fa-spin"></i> {{$lang['upload']}}</button>
                        <div class="avatar-uploader">
                          <imgfileupload data-event="profileOnLoad12"  data-loading-event="primaryLoading6" data-url="/common/resize-whole-image?width=998&height=874&type=primary"></imgfileupload>
                          <upload-loader :loading="primaryLoading6" class="v-spinner-primary" :progress="progress"></upload-loader>
                        </div>
                    </span>
                  </div>
                </div>
              </div>
          </div>

    </div><!-- end col-sm-12 -->
  </div><!-- end col-sm-12 -->
</div><!-- end main-photos -->

      <div id="desc-photos">
        <div class="col-sm-12 p-t-3 m-t-3">
            <div class="col-sm-12 store-input-set">
              <div class="store-subhead">
                <h4 class="sub-header">{{$lang['product-desc-photos']}} <i class="inote">{{$lang['you-desc']}}</i></h4>
                <hr>
              </div>

              <div id="descs" class="col-sm-12">
              <div class="row">
              
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#1</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage1=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage1" v-if="productForm.descimage1" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage1" v-if="productForm.descimage1"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad5'" :data-loading-event="'descLoading1'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading1" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#2</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage2=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage2" v-if="productForm.descimage2" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage2" v-if="productForm.descimage2"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad6'" :data-loading-event="'descLoading2'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading2" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#3</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage3=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage3" v-if="productForm.descimage3" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage3" v-if="productForm.descimage3"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad7'" :data-loading-event="'descLoading3'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading3" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#4</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage4=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage4" v-if="productForm.descimage4" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage4" v-if="productForm.descimage4"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad8'" :data-loading-event="'descLoading4'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading4" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#5</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage5=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage5" v-if="productForm.descimage5" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage5" v-if="productForm.descimage5"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad9'" :data-loading-event="'descLoading5'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading5" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#6</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage6=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage6" v-if="productForm.descimage6" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage6" v-if="productForm.descimage6"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad10'" :data-loading-event="'descLoading6'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading6" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#7</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage7=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage7" v-if="productForm.descimage7" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage7" v-if="productForm.descimage7"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad13'" :data-loading-event="'descLoading7'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading7" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#8</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage8=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage8" v-if="productForm.descimage8" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage8" v-if="productForm.descimage8"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad14'" :data-loading-event="'descLoading8'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading8" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#9</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage9=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage9" v-if="productForm.descimage9" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage9" v-if="productForm.descimage9"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad15'" :data-loading-event="'descLoading9'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading9" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#10</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage10=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage10" v-if="productForm.descimage10" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage10" v-if="productForm.descimage10"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad16'" :data-loading-event="'descLoading10'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading10" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#11</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage11=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage11" v-if="productForm.descimage11" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage11" v-if="productForm.descimage11"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad17'" :data-loading-event="'descLoading11'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading11" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#12</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage12=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage12" v-if="productForm.descimage12" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage12" v-if="productForm.descimage12"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad18'" :data-loading-event="'descLoading12'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading12" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#13</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage13=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage13" v-if="productForm.descimage13" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage13" v-if="productForm.descimage13"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad19'" :data-loading-event="'descLoading13'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading13" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#14</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage14=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage14" v-if="productForm.descimage14" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage14" v-if="productForm.descimage14"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad20'" :data-loading-event="'descLoading14'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading14" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#15</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage15=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage15" v-if="productForm.descimage15" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage15" v-if="productForm.descimage15"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad21'" :data-loading-event="'descLoading15'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading15" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#16</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage16=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage16" v-if="productForm.descimage16" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage16" v-if="productForm.descimage16"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad22'" :data-loading-event="'descLoading16'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading16" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#17</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage17=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage17" v-if="productForm.descimage17" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage17" v-if="productForm.descimage17"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad23'" :data-loading-event="'descLoading17'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading17" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#18</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage18=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage18" v-if="productForm.descimage18" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage18" v-if="productForm.descimage18"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad24'" :data-loading-event="'descLoading18'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading18" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#19</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage19=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage19" v-if="productForm.descimage19" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage19" v-if="productForm.descimage19"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad25'" :data-loading-event="'descLoading19'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading19" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group label-floating">
                      <div class="input-group">
                        <label class="control-label number">#20</label>
                        <a class="delete-spec" href="javascript:void(0)" @click="productForm.descimage20=''" title="{{$lang['remove']}}"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                        <div class="add-desc">
                          <a :href="productForm.descimage20" v-if="productForm.descimage20" data-lightbox="image-2">
                          <img class="add-desc-photo" :src="productForm.descimage20" v-if="productForm.descimage20"></a>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-raised btn-upload" type="button">{{$lang['upload']}}</button>
                            <div class="avatar-uploader">
                              <imgfileupload :data-event="'profileOnLoad26'" :data-loading-event="'descLoading20'" data-url="/common/resize-whole-image?width=518&height=454&type=description"></imgfileupload>
                              <upload-loader :loading="descLoading20" class="v-spinner-primary top" :progress="progress"></upload-loader>
                            </div>
                        </span>
                      </div>
                    </div>
                  </div>

              </div>
              </div>
            </div>

      <div class="col-sm-12 store-input-set">
          <div class="col-lg-12 col-sm-6 pull-right">
              <button type="submit" class="btnFinishProduct btn btn-has-loading btn-tangerine pull-right" @click="submitProduct()"></i>{{$lang['finish']}}</button>
              <button type="button" class="btnSaveDraft btn btn-has-loading btn-tangerine pull-right m-r-2" @click="submitAsDraft()"></i> {{$lang['save-as-draft']}}</button>
              <button type="button" class="btn btn-tangerine pull-right m-r-2" @click="productPreview()">{{$lang['product-preview']}}</button>
          </div>
      </div>

  </div><!-- end col-sm-12 -->
</div><!-- end desc-photos -->

  </form>
</div><!-- end open-product-content -->
