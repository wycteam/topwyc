<!--  ABOUT PAGE -->
@extends('layouts.master2')

@section('title', 'Registration Page')

@push('styles')
 	{!! Html::style(mix('css/pages/home.css','shopping')) !!}
 	{!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.css') !!}
 	<style>
/* 	#wizard, #signup {
 		display: none;
 	}*/

/* For mobile phones: */
	@media only screen and (max-width: 768px) {
	  /* For mobile phones: */
	  .datepicker.datepicker-dropdown{
			top: 175px !important;
		}
	}

	@media only screen and (min-width: 769px){
		
		.datepicker.datepicker-dropdown {
		    top: 232px !important;
		}

	}


	/*xs screen*/
	@media only screen and (min-width: 497px){
						
		.open-reg-btn{
			margin-left: 25%;
		}

		.open-reg-btn2{
			margin-left: 25%;
		}

	}

	@media only screen and (min-width: 600px) {

		.open-reg-btn{
			margin-left: 34%;
		}

		.open-reg-btn2{
			margin-left: 34%;
		}
	}


	/*sm screen */
	@media only screen and (min-width: 768px){
		
		.open-reg-btn{
			margin-left: 45px;
		}

		.open-reg-btn2{
			margin-left: 45px;
		}

	}



	/*md screen */
	@media only screen and (min-width: 992px){
		
		.open-reg-btn{
			margin-left: 50px;
		}

		.open-reg-btn2{
			margin-left: 50px;
		}

	}

	/*lg screen */
	@media only screen and (min-width: 1200px){
		
		.open-reg-btn{
			margin-left: 45px;
		}

	}

/* END mobile phones: */

	#loading {
	    background: #f4f4f2 url("img/page-bg.png") repeat scroll 0 0;
	    height: 100%;
/*	    left: 0;*/
	    margin: auto;
	    position: fixed;
	    top: 0;
	    width: 100%;
	}
	.bokeh {
	    border: 0.01em solid rgba(150, 150, 150, 0.1);
	    border-radius: 50%;
	    font-size: 100px;
	    height: 1em;
	    list-style: outside none none;
	    margin: 0 auto;
	    position: relative;
	    top: 35%;
	    width: 1em;
	    z-index: 2147483647;
	}
	.bokeh li {
	    border-radius: 50%;
	    height: 0.2em;
	    position: absolute;
	    width: 0.2em;
	}
	.bokeh li:nth-child(1) {
	    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
	    background: #00c176 none repeat scroll 0 0;
	    left: 50%;
	    margin: 0 0 0 -0.1em;
	    top: 0;
	    transform-origin: 50% 250% 0;
	}
	.bokeh li:nth-child(2) {
	    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
	    background: #ff003c none repeat scroll 0 0;
	    margin: -0.1em 0 0;
	    right: 0;
	    top: 50%;
	    transform-origin: -150% 50% 0;
	}
	.bokeh li:nth-child(3) {
	    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
	    background: #fabe28 none repeat scroll 0 0;
	    bottom: 0;
	    left: 50%;
	    margin: 0 0 0 -0.1em;
	    transform-origin: 50% -150% 0;
	}
	.bokeh li:nth-child(4) {
	    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
	    background: #88c100 none repeat scroll 0 0;
	    margin: -0.1em 0 0;
	    top: 50%;
	    transform-origin: 250% 50% 0;
	}


	@keyframes opa {
	12% {
	    opacity: 0.8;
	}
	19.5% {
	    opacity: 0.88;
	}
	37.2% {
	    opacity: 0.64;
	}
	40.5% {
	    opacity: 0.52;
	}
	52.7% {
	    opacity: 0.69;
	}
	60.2% {
	    opacity: 0.6;
	}
	66.6% {
	    opacity: 0.52;
	}
	70% {
	    opacity: 0.63;
	}
	79.9% {
	    opacity: 0.6;
	}
	84.2% {
	    opacity: 0.75;
	}
	91% {
	    opacity: 0.87;
	}
	}

	@keyframes rota {
	100% {
	    transform: rotate(360deg);
	}
	}

	


img {
  max-width: 100%; }

ul {
  padding-left: 0;
  margin-bottom: 0; }

a {
  text-decoration: none;
  color: #ff9a9c;
  transition: all 0.3s ease; }
  a:hover {
    text-decoration: none;
    color: #fe4447; }

:focus {
  outline: none; }





 	</style>
@endpush

@section('content')
<div class="full-content" id="about-content">

  <section class="gray-section" id="androidPage">
     <div class="container wow">
		<div class="row" id="signup">
        	<div class="col-lg-12 text-center">
	            <p> </p>
	        </div>
	    </div>
		<div class="wrapper" >
                    <div class="inner" style="width: 100%;">
                        <div class="image-holder">
                            <img src="/images/form-wizard-1.jpg" alt="">
                        </div>
                        <form style="padding-left: 	0px; padding-right: 	0px;">
                        <div class="col-sm-12">
                                    <div class="panel panel-success">
                                        <div class="panel-heading" style="background-color: #ff9a9c;">
                                            <b>{{$lang['successfully-registered']}}</b>
                                        </div>
                                        <div class="panel-body">
                                        	<p> <b>{{$lang['you-can-search-wechat']}}</b></p>
                                        	<a href="#" id="wechatLink"><img src="/images/wechat.png"></a>
                                        	<br><br>

                                        	<p> <b>{{$lang['scan-qr-code']}}</b></p>
                                        	<center><a><img src="/images/userQR.jpg"></a></center>
                                        	 <!-- <br><br>
                                        	<br><br>
                                            <p> <b>Please download our app, you can login using your mobile number as username and password.</b></p>
                                            <p><br> <b>*</b> 
                                            	If you are a Huawei user, please download our app from <a href="https://appstore.huawei.com/app/C100859187"><b>Huawei store.</b></a> <br>  
                                            	<a href="https://appstore.huawei.com/app/C100859187"><img src="/images/huawei_logo.png"></a>
                                            </p>

                                            <p><br> <b>*</b> 
                                            	If you are non-Huawei user, please download our app from <a href="https://play.google.com/store/apps/details?id=com.wyc.visatrackerapp"><b>Google store</b></a> or <a href="http://topwyc.com/apk/wycgrouptracker.apk"><b>download apk file.</b></a> <br>
  
                                            	<a href="https://play.google.com/store/apps/details?id=com.wyc.visatrackerapp"><img src="/images/google_logo.png"></a>
                                            	<a href="http://topwyc.com/apk/wycgrouptracker.apk" target="_blank"><img src="/images/apk_logo.png"></a>
                                            </p>    -->                                      
                                        </div>
                                    </div>
                                </div>

                        </form>
                    </div>
        </div>

	</div>
  </section>

</div>

@endsection

@push('scripts')
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js') !!}
<!-- {!! Html::script(mix('js/visa/open.js','shopping')) !!}-->
{!! Html::script(mix('js/pages/home.js','shopping')) !!}
@endpush

