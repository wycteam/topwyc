@extends('layouts.master')

@section('title', 'Categories')

@push('styles')
 {!! Html::style(mix('css/pages/home.css', 'shopping')) !!}
@endpush

@section('content')
  <main class="cd-main-content" id="categories-list">
    <div class="row hidden-sm hidden-xs" >
      <div class="categories">
            <ul type="none">
              <li v-for='parent in categorylist' >
                <div class="main-header-container parent">
                  <a :href="'/product?search='+ parent.name">
                    <h3 class="main-header">@{{parent.name}}</h3>
                  </a>
                </div>
                <ul type="none" v-if="parent.children.length != 0" v-for='child in parent.children'>
                  <li>
                    <a :href="'/product?search='+ child.name"><h5 class="child">@{{child.name}}</h5></a>
                    <ul type="none" v-if="child.children.length != 0" v-for='grandchild in child.children'>
                      <li>
                        <a :href="'/product?search='+ grandchild.name"><h6 class="grandchild">@{{grandchild.name}}</h6></a>
                      </li>
                    </ul> 
                  </li>
                </ul>             
              </li>
            </ul> 
   
      </div>
    </div>

    <div class="row hidden-lg hidden-md">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-12 categories-mob">
            <ul type="none">
              <li v-for='parent in categorylist' >
                <div class="main-header-container parent">
                  <a :href="'/product?search='+ parent.name">
                    <h3 class="main-header">@{{parent.name}}</h3>
                  </a>
                </div>
                <ul type="none" v-if="parent.children.length != 0" v-for='child in parent.children'>
                  <li>
                    <a :href="'/product?search='+ child.name"><h5 class="child">@{{child.name}}</h5></a>
                    <ul type="none" v-if="child.children.length != 0" v-for='grandchild in child.children'>
                      <li>
                        <a :href="'/product?search='+ grandchild.name"><h6 class="grandchild">@{{grandchild.name}}</h6></a>
                      </li>
                    </ul> 
                  </li>
                </ul>             
              </li>
            </ul> 
          </div>
        </div>
      </div>
    </div>

  </main> <!-- cd-main-content -->
@endsection

@push('scripts')
  {!! Html::script('shopping/js/pages/categories.js') !!}
  {!! Html::script('shopping/plugins/mixitup/js/main.js') !!}
  
@endpush
