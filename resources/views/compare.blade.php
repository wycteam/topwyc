<!--  FAVORITES PAGE -->
@extends('layouts.master')

@section('title', $lang['title'])

@push('styles')
    {!! Html::style(mix('css/pages/compare.css','shopping')) !!}
@endpush

@section('content')
<div class="full-content" id="compare-content">
  @include('includes.compare.modals')
  <div class="container">
  	<div class="main-header-container">
      <h3 class="main-header">{{$lang['title']}}</h3>
    </div>
	  <div class="col-md-12">
      <div class="row">
        <div>
          <div class="col-md-4 col-sm-6" v-for="(item,index) in items.data" :key="item.id" :id="'div'+item.id">
            <div class="card" v-cloak>
              <a :href="'/product/' + item.slug"><img class="card-img-top" :src="item.image" alt=""></a>
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a :href="'/product/' + item.slug" class="comp-name" v-cloak>@{{item.name}}</a></li>
                <li class="list-group-item" v-cloak><span v-if="item.fee_included==1">@{{ parseFloat(item.price)+parseFloat(item.shipping_fee)+parseFloat(item.charge)+parseFloat(item.vat) | currency}}</span><span v-else>@{{ parseFloat(item.price) | currency}}</span></li>     
                <li class="list-group-item margin1" v-cloak><span class="font">{{$lang['stock']}}:</span> @{{item.stock}}</li>
                <li class="list-group-item">
                  <div class="rating-block">
                      <div class="star-ratings-sprite"><span :style="item.rating" class="star-ratings-sprite-rating"></span></div>
                    </div>
                  </li>
                  <li class="list-group-item margin2" v-cloak><span class="font">{{$lang['description']}}:</span> <span v-html="item.desc"></span></li>
                <li class="list-group-item"><a href="javascript:void(0)" class="btn btn-raised btn-primary comp-btn" @click="addtocart(item.id, index)">{{$lang['add-cart']}}<div class="ripple-container"></div></a></li>
                <li class="list-group-item"><a href="javascript:void(0)" class="btn btn-raised btn-primary comp-btn" @click="showRemoveDialog(item.id, index)">{{$lang['remove']}}<div class="ripple-container"></div></a></li>
              <input :id="'prod_id' + item.id" :name="'prod_id' + item.id" type="hidden" :value="item.prod_id">
              <input :id="'qty' + item.id" :name="'qty'+item.id" type="hidden" value="1"> 
              </ul>       
            </div>
          </div>
        </div>
        <p id="no-item" style="display:none">{{$lang['no-item']}}</p>
      </div>
	  </div>

  </div><!-- end  container-->
</div><!-- end  compare-content-->
@endsection

@push('scripts')
    {!! Html::script(mix('js/pages/compare.js','shopping')) !!}
@endpush
