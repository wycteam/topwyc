@extends('layouts.master')

@section('title', $lang['title'])

@push('styles')
    {!! Html::style(mix('css/pages/feedback.css','shopping')) !!}
@endpush

@push('scripts')
    {!! Html::script(mix('js/pages/feedback.js','shopping')) !!}
@endpush

@section('content')
<!--  EWALLET PAGE -->
<header class="full-content">
    <div class='container' id='feedbacks_container'>
        <div class="main-header-container">
          <h3 class="main-header">{{$lang['title']}}</h3>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class='card'>
                    <div class="col-lg-12">
                        <div class='row user-feedback'>
                            <div class='col-lg-1 text-center'>
                                @if($user->avatar)
                                    <img src="{{$user->avatar}}" class="img img-circle fb-user-image">
                                @else
                                    <div class='no-avatar' >
                                        {{$user->initials}}
                                    </div>
                                @endif
                            </div>
                            <div class="col-lg-11">
                                <div class='fb-holder'>
                                    <div class='fb-name'>
                                        {{$user->full_name}}
                                    </div>
                                    <div class='fb-content'>
    
                                        <div class="col-lg-2 col-md-2" v-if='form.imageTemp'>
                                            <img :src="form.imageTemp" class='img-responsive' >
                                        </div>

                                        <div :class="form.imageTemp? 'col-lg-10 col-md-10':'col-lg-12 col-md-12'">
                                            <div class="form-group"  :class="{ 'has-error': form.errors.has('content') }">
                                                <label class="control-label" for="focusedInput1">{{$lang['description1']}}</label>
                                                <textarea v-model='form.content' v-on:keyup="handleCmdEnter($event)" id='feedback_textarea' class='form-control' rows='2'></textarea>
                                            </div>
                                        </div>

                                        <span  v-if="form.errors.has('content')" v-text="form.errors.get('content')"></span>
                                    </div>
                                    <div class='text-right'>
                                        <a class='upload-container btn btn-default btn-raised'>
                                            <imgfileupload data-event="submitFeedbackImage" data-url="/common/resize-whole-image?width=250&height=250">
                                            </imgfileupload>
                                            <span class='fa fa-image'></span>
                                        </a>
                                        <button @click='submitFeedback' class='btn btn-raised btn-success  btn-has-loading'>{{$lang['send-feedback']}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <feedback v-for='feedback in feedbacks' :key='feedback.id' :feedback='feedback' ></feedback>
                <infinite-loading :on-infinite="onInfinite" ref="infiniteLoading">
                    <span slot="no-more">
                        {{$lang['no-more-feedbacks']}}
                    </span>
                </infinite-loading>
            </div>
        </div>
        <modal id='modalComments'>
            <template slot='modal-title'>
                {{$lang['add-comment']}}
            </template>
            <template slot='modal-body'>
                <div class="form-group"   :class="{ 'has-error': commentForm.errors.has('content') }">
                    <label class="control-label" for="focusedInput1">{{$lang['say-comment']}}</label>
                    <textarea v-model='commentForm.content'  id='feedback_textarea' class='form-control' rows='3'></textarea>
                    <span  v-if="commentForm.errors.has('content')" v-text="commentForm.errors.get('content')"></span>
                </div>
            </template>
            <template slot='modal-footer'>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{$lang['close']}}</button>
                <button type="button" class="btn btn-success" @click='submitComment'>
                    {{$lang['submit']}}
                </button>
            </template>
        </modal>
    </div>
</header>
@endsection