@extends('layouts.master')

@section('title', $lang['purchase-report'])

@push('styles')
    {!! Html::style('components/sweetalert2/dist/sweetalert2.min.css') !!}
	{!! Html::style(mix('css/pages/home.css', 'shopping')) !!}
	{!! Html::style(mix('css/pages/purchase-report.css','shopping')) !!}
@endpush

@push('scripts')
    {!! Html::script('components/sweetalert2/dist/sweetalert2.min.js') !!}
	{!! Html::script(mix('js/pages/purchase-report.js','shopping')) !!}
@endpush

@section('content')
<!--  SALES REPORT PAGE -->
<header class="full-content">
    <div class='container' id='purchase_report_container'>
    	@include('includes.reports.purchase.modals')
      	@include('includes.reports.purchase.airway')
		<div class="main-header-container">
			<h3 class="main-header">{{$lang['purchase-report']}}</h3>
        </div>
        <div class="row" >
        	<!-- products bought -->
        	<div class="col-lg-12">
        		<div class="card" v-if="ordersList == ''">
        			<div class="col-lg-12">
		        		<h4 style="text-align: center;">{{$lang['no-purchase-msg']}}</h4>
    				</div>
        		</div>
        		<div class="card" v-if="orderedProduct != ''">
        			<div class="col-lg-12">
		        		<h4>{{$lang['recently-bought']}}</h4>
		        		<div class="row">
		        			<div class='col-lg-3'
					             v-for="item in orderedProduct"
								:key="item.id"
		        				>
		        				<product
					              :item="item"
					            ></product>
		        			</div>
        				</div>
    				</div>
        		</div>
        	</div>
        	<!-- table of products bought -->
        	<div class="col-lg-12" v-if="ordersList != ''">
        		<!-- <div class='card'>
					<div class="col-lg-12">
						<table class="table table-striped purchase-tbl">
							<thead>
								<tr>
									<th class="col-sm-2 report-header">{{$lang['tracking-number']}}</th>
									<th class="col-sm-2 report-header hidden-xs">{{$lang['product']}}</th>
									<th class="col-sm-2 report-header hidden-xs">{{$lang['placed-on']}}</th>
									<th class="col-sm-1 report-header hidden-xs">{{$lang['total']}}</th>
									<th class="col-sm-3 report-header">{{$lang['status']}}</th>
									<th class="col-sm-1 report-header"></th>
									<th class="col-sm-1 report-header"></th>
								</tr>
							</thead>
							<tbody>
								<tr is="orderList"
									v-for="order in ordersList"
									:order = "order"
									:key = "order.id"
								></tr>
							</tbody>
						</table>
					</div>
        		</div> -->

        		<div class="card">
        			<div class="col-lg-12">
        				<table class='table table-striped orders-tbl'>
							<thead>
								<tr>
									<th class="text-center"><b>Tracking Number</b></th>
									<th class="text-center"><b>Placed On</b></th>
									<th class="text-center"><b>Total</b></th>
									<th class="text-center"><b>Status</b></th>
									<th class="text-center">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<template v-for="my_order in my_orders_list">
									<tr @click="showOrderDetail(my_order.id, $event)" class="clickable">
										<td>
											<a><strong>@{{ my_order.tracking_number}}</strong></a></td>
										<td>@{{ my_order.created_at }}</td>
										<td>
											<!-- @{{ getDiscounted(my_order.details[0].id) }} @{{my_order.details[0].id }} -->
											Php@{{ my_order.total }}
										</td>
										<td>
											@{{ my_order.status }}
										</td>
										<td>
											<a @click.stop="showDetails(my_order.details[0].id)" style="cursor:pointer;">
												See More
											</a>
										</td>
									</tr>

									<tr :id="'order-row-'+my_order.id" class="hide">
										<td colspan="5">

											<table class="table table-bordered order-details-tbl">
												<thead>
													<tr>
														<td colspan="5">
															<h4><strong>Order Details</strong></h4>
														</td>
													</tr>
													<tr>
														<td class="text-center" style="width:10%;">Image</td>
														<td>Name</td>
														<td>Quantity</td>
														<td>Status</td>
													</tr>
												</thead>
												<tbody>
													<tr v-for="_detail in my_order.details">
														<td class="text-center">
															<img :src="_detail.image" class="img-thumbnail">
														</td>
														<td><b>@{{ _detail.product.name }}</b></td>
														<td><b>@{{ _detail.quantity }}</b></td>
														<td>
															<div v-if="_detail.status == 'In Transit'" class="btn-group">
																<button type="button" class="btn btn-primary btn-raised btn-sm">
																	   <span>In Transit</span>
																</button>
																<button type="button" class="btn btn-primary btn-raised btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<span class="caret"></span>
																	<span class="sr-only">Toggle Dropdown</span>
																</button>
																<ul class="dropdown-menu">
																	<li>
																		<a @click="markAsReceived(_detail.id)" href="javascript:void(0)" style="color:black">I received the item</a>
																	</li>
																	<li>
																		<a @click="showReturnItemModal(_detail)" href="javascript:void(0)" style="color:black">I want to return this item</a>
																	</li>
																</ul>
															</div>
															<div v-else>
																<b>@{{ _detail.status }}</b>
															</div>
														</td>
													</tr>
												</tbody>
											</table>

										</td>
									</tr>
								</template>
							</tbody>
						</table>
        			</div>
        		</div> <!-- card -->
        	</div>
        </div>
    </div>
</header>

@endsection