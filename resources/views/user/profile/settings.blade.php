<!--  PROFILE SETTINGS PAGE -->
@extends('layouts.master')

@section('title', 'Profile Settings')

@push('styles')
 	{!! Html::style('shopping/css/pages/profile.css') !!}
  {!! Html::style('https://unpkg.com/vue-multiselect@2.0.0-beta.14/dist/vue-multiselect.min.css') !!}
  {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
@endpush

@section('content')
<header class="full-content custom-chosen">
  <div class="container" id="profile-settings-container">
    @include('includes.profile.modals.profile-settings-modals')
    <div class="main-header-container">
      <h3 class="main-header">PROFILE SETTINGS</h3>
    </div><!-- end  main-header-container-->
  	<div class="col-sm-12">
  		@include('includes.profile.settings.accountInformation')
  	</div><!-- end  col-sm-12-->
    <div class="col-sm-12">
      @include('includes.profile.settings.contactDetails')
    </div><!-- end  col-sm-12-->
  	<div class="col-sm-12">
  		@include('includes.profile.settings.privacySettings')
  	</div><!-- end  col-sm-12-->
  	<div class="col-sm-12">
  		@include('includes.profile.settings.securitySettings')
  	</div><!-- end  col-sm-12-->
  </div><!-- end  container-->
</header>
@endsection

@push('scripts')
  {!! Html::script('components/jquery-mask-plugin/dist/jquery.mask.min.js') !!}
	{!! Html::script('https://unpkg.com/vue-multiselect@2.0.0-beta.14') !!}
  {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
  {!! Html::script('shopping/js/pages/profileSettings.js') !!}
@endpush
