<!--  PROFILE PAGE -->
@extends('layouts.master')

@section('title', $lang['profile'])

@push('styles')
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
    {!! Html::style('cpanel/plugins/jasny/jasny-bootstrap.min.css') !!}

    {!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.css") !!}
    {!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css") !!}
    {!! Html::style("cpanel/plugins/nouslider/jquery.nouislider.css") !!}

    {!! Html::style(mix('css/pages/profile.css', 'shopping')) !!}
@endpush

@section('content')
<div class="full-content custom-chosen" id="profile-container">
    <div class="container">
        <!-- profile picture and tabs -->
        <span  id='profile-menu'>
            
            <div class="col-lg-3 col-md-3">
                @include('includes.profile.image-name')
                <div class='card hidden-sm hidden-xs' id='subnav'>
                    @include('includes.profile.content.menu')
                </div>
            </div>
            <!-- contains elements for mobile viewing: menu and modal for menu -->
            @include('includes.profile.content.mobile')
            
        </span>
        

        <!-- boxes and content of tabs -->
        <div class="col-lg-9 col-md-9">
            <!-- boxes -->
            <div class="row hidden-sm hidden-xs" id='data-area'>
                <div class="col-lg-3 col-md-3">
                    <a href='/stores'>
                        <div class='card text-center '> 
                            <div class='count-display tc-purple' v-cloak>@{{storeCount}}</div>                   
                            <div class='description-display bgc-purple'>{{$lang['store']}}</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3">
                    <a href="/favorites">
                        <div class='card text-center '>   
                            <div class='count-display tc-pink' v-cloak>@{{wishCount}}</div>                   
                            <div class='description-display bgc-pink'>{{$lang['wishlist']}}</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6">
                    <a href="/user/ewallet">
                        <div class='card text-center' data-toggle='modal' data-target='#modalFriends'> 
                            <div class='count-display tc-red'  v-cloak>@{{ewallet | currency}}</div>                   
                            <div class='description-display bgc-red'>{{$lang['ewallet']}}</div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- content of tabs -->
            
            <div class="row" >
                <div class="col-lg-12 col-md-12">
                    <div class='card'>
                        <div class="col-lg-12 col-md-12">
                            <div class="tab-content" id='profile-settings-container'>
                                <div id="account-info" v-show="param=='account'" style="display:none">
                                    @include('includes.profile.content.account_information')
                                </div>
                                <div id="contact-details" v-show="param=='contact'" style="display:none">
                                    @include('includes.profile.content.contact_details')
                                </div>
                                <div id="security-settings" v-show="param=='security'" style="display:none">
                                    @include('includes.profile.content.security_settings')
                                </div>
                                <div id="documents" v-show="param=='documents'" style="display:none">
                                    @include('includes.profile.content.documents')
                                </div>
                                <div id="notifications" v-show="param=='notifications'" style="display:none">
                                    @include('includes.profile.content.notifications')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
    {!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('cpanel/plugins/jasny/jasny-bootstrap.min.js') !!}
    {!! Html::script(mix('js/pages/profile.js','shopping')) !!}
    {!! Html::script(mix('js/pages/profileSettings.js','shopping')) !!}
   
    {!! Html::script("cpanel/plugins/jquery-ui/jquery-ui-draggable.min.js") !!}
    <!-- {!! Html::script('shopping/js/pages/profile/account-info.js') !!} -->
    <!-- {!! Html::script('shopping/js/pages/profile/contact-details.js') !!} -->
    <!-- {!! Html::script('shopping/js/pages/profile/security-settings.js') !!} -->
@endpush
