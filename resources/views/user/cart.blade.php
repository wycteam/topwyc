@extends('layouts.master')

@section('title', $lang['title'])

@push('styles')
	{!! Html::style(mix('css/pages/cart.css', 'shopping')) !!}
@endpush

@section('content')
<!--  CART PAGE -->
<header class="full-content" >
    <div class='container'  id='cart_container'>
		@include('includes.cart.modals')
        <div class='col-lg-12'>
			<div class="main-header-container">
	          	<h3 class="main-header">{{$lang['title']}}</h3>
	        </div>
			<div class='col-lg-12 cart-items-container'>
				<div class='row'>
					<div class="col-lg-5 col-md-5 col-sm-5">
						<span class='cart-header' v-cloak>{{$lang['you-have']}} @{{items.length}} {{$lang['items-in']}}</span>
					</div>

					<div class="col-lg-7 col-md-7 col-sm-7 visible-lg hidden-md hidden-sm hidden-xs" v-if="items.length!=0">
						<div class="col-md-3 col-sm-3 text-center">
							<span class='cart-header'>{{$lang['quantity']}}</span>
						</div>
						<div class="ccol-md-4 col-sm-4  text-center">
							<span class='cart-header'>{{$lang['item-price']}}</span>
						</div>
						<div class="col-md-3 col-sm-3  text-center hide">
							<span class='cart-header'>{{$lang['shipping-fee']}}</span>
						</div>
						<div class="col-md-5 col-sm-5  text-center">
							<span class='cart-header' style="margin-left: -60px;">{{$lang['sub-total']}}</span>
						</div>
					</div>
				</div>
				<div class='row '>
					<cart-item
						v-for="(item,index) in items"
						:index="index"
						:item="item"
						:key="item.id"
					>
					</cart-item>
				</div>
			</div>
        	<div class='col-lg-12 text-right' v-cloak>
        		<h4 class="font-weight" v-if="discounts!=0">{{$lang['sub-total']}}: @{{total_price | currency}}</h4>
        		<h4 class="font-weight" v-if="discounts!=0">{{$lang['discount']}}: @{{discounts | currency}}</h4>
				<button
					@click='showCheckoutModal()'
					class='btn btn-success btn-raised btn-modify' v-if="items.length!=0"
				><span class='fa fa-check'></span> {{$lang['checkout']}} <b>(@{{discounted_price | currency}})</b></button>
			</div>
        </div>
    </div>
</header>

@endsection

@push('scripts')
	{!! Html::script('cpanel/plugins/jasny/jasny-bootstrap.min.js') !!}
	{!! Html::script(mix('js/pages/cart.js', 'shopping')) !!}
@endpush