@extends('layouts.master')

@section('title', $viewuser->full_name)

@section('content')
<div class="container stores-container">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="title m-b-2"><span></span></div>

      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default panel-store-profile visible-sm-block">
            <div class="panel-body">
              <div class="row">
                <div class="col-xs-6">
                  <img class="user-avatar img-responsive img-thumbnail" src="{{ $viewuser->avatar }}" alt="Profile Picture">
                </div>
                <div class="col-xs-6">
                  <h3 class="full-name"><b>{{ $viewuser->full_name }}</b></h3>
                  <p class="text-muted">
                    <b>{{ $viewuser->stores->count() }}</b> {{$lang['store']}}<br>
                    <b>{{ $viewuser->products->count() }}</b> {{$lang['product']}}
                  </p>
                @if($user->id == $viewuser->id)
                  <a href="{{ route('shopping.stores.create') }}" class="btn btn-raised btn-primary btn-block">{{$lang['create-store']}}</a>
                @endif
                </div>
              </div>
            </div>
          </div>

          {{-- Hide on small devices --}}
          <div class="panel panel-default panel-store-profile hidden-sm">
            <div class="panel-heading">
              <h3 class="panel-title">
                <img class="user-avatar img-responsive img-thumbnail" src="{{ $viewuser->avatar }}" alt="Profile Picture">
              </h3>
            </div>
            <div class="panel-body">
              <h3 class="full-name text-center"><b>{{ $viewuser->first_name }} {{ $viewuser->last_name }}</b></h3>
         <!-- <div class="card card-with-border card-centred-text black-grey">
                <div class="card-content text-white">
                  <h4 class="card-title">{{ $viewuser->ewallet->amount }}</h4>
                  <h6 class="card-description"><b>Ewallet</b></h6>
                </div>
              </div> -->
              @if($user->id == $viewuser->id)
                  <a href="{{ route('shopping.stores.create') }}" class="btn btn-raised btn-primary btn-block">{{$lang['create-store']}}</a>
              
              @endif
            </div>
            <div class="panel-footer">
              <span class="text-muted"><b>{{ $viewuser->stores->count() }}</b> {{$lang['store']}}</span>
              <span class="text-muted pull-right"><b>{{ $viewuser->products->count() }}</b> {{$lang['product']}}</span>
            </div>
          </div>
        </div>

        <div class="col-md-9">
          <div class="row">
            <div class="col-xs-12">
              <div class="owl-carousel owl-theme">
              @foreach ($stores as $store)
                <div class="panel panel-default panel-store">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                      <img class="img-responsive" src="{{ $store->cover_image }}" alt="Store Image">
                    </h3>
                  </div>
                  <div class="panel-body">
                    <h3><a class="store-name" href="{{ route('shopping.stores.show', $store->slug) }}">
                        @if(isset($_COOKIE['locale']))
                          @if($_COOKIE['locale']=='cn')
                            {{$store->cn_name? : $store->name}}
                          @else
                            {{$store->name}}
                          @endif
                        @else
                          {{$store->name}}
                        @endif
                    </a></h3>
                    <p class="store-description">
                        @if(isset($_COOKIE['locale']))
                          @if($_COOKIE['locale']=='cn')
                            {{$store->cn_description? : $store->description}}
                          @else
                            {{$store->description}}
                          @endif
                        @else
                          {{$store->description}}
                        @endif
                    </p>

                    <!-- <div class="rate"
                      data-rateyo-rating="5"
                      data-rateyo-full-star="true"
                      data-rateyo-read-only="true">
                    </div> -->
                  @if($user->id == $viewuser->id)
                    <a href="{{ route('shopping.product.create') }}" class="btn btn-warning btn-raised btn-block">{{$lang['add-prod']}}</a>
                  @endif
                  </div>
                </div>
              @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('styles')
{!! Html::style('components/owl.carousel/dist/assets/owl.carousel.min.css') !!}
{!! Html::style('components/owl.carousel/dist/assets/owl.theme.default.min.css') !!}
{!! Html::style('components/rateYo/min/jquery.rateyo.min.css') !!}
{!! Html::style(mix('css/pages/store/index.css', 'shopping')) !!}
@endpush

@push('scripts')
{!! Html::script('components/owl.carousel/dist/owl.carousel.min.js') !!}
{!! Html::script('components/masonry/dist/masonry.pkgd.min.js') !!}
{!! Html::script('components/rateYo/min/jquery.rateyo.min.js') !!}
{!! Html::script('components/jq.ellipsis/jquery.ellipsis.min.js') !!}
{!! Html::script(mix('js/pages/store/index.js', 'shopping')) !!}
@endpush
