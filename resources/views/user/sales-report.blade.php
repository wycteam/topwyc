@extends('layouts.master')

@section('title', $lang['sales-report-title'])

@push('styles')
    {!! Html::style('components/sweetalert2/dist/sweetalert2.min.css') !!}
    {!! Html::style(mix('css/pages/sales-report.css','shopping')) !!}
@endpush

@push('scripts')
    {!! Html::script('components/sweetalert2/dist/sweetalert2.min.js') !!}
    {!! Html::script('components/chart.js/dist/Chart.bundle.js') !!}
    {!! Html::script(mix('js/pages/sales-report.js','shopping')) !!}
@endpush

@section('content')
<!--  SALES REPORT PAGE -->


<header class="full-content custom-chosen">
    <div class='container' id='sales_report_container'>
    	@include('includes.reports.sales.modals')
		<div class="main-header-container">
			<h3 class="main-header">{{$lang['sales-report-title']}}</h3>
        </div>
        <div class="row">
			<div class="col-lg-12">

				@if(count($stores) == 0)
					<div class="col-lg-12">
						<h4 style="text-align:center;">
							{{ $lang['no-stores-msg'] }}
						</h4>
					</di>
				@endif

				<div class="owl-carousel owl-theme owlbg">
				  <?php $ctr = 1; ?>
	              @foreach ($stores as $store)
	                <div data-id="_panel-{{ $store->id }}" class="panel panel-default panel-store" style="opacity: {{ ($ctr == $param || $store->id == $param) ? '1' : '0.5' }};">
	                  <div class="panel-heading">
	                    <h3 class="panel-title">
	                      <img class="img-responsive" src="{{ $store->cover_image }}" alt="Store Image">
	                    </h3>
	                  </div>
	                  <div class="panel-body">
	                    <h3><a data-id="{{ $store->id }}" class="store-name" data-toggle="collapse" href="#store_{{ $store->id }}">
	                        @if(isset($_COOKIE['locale']))
	                          @if($_COOKIE['locale']=='cn')
	                            {{$store->cn_name? : $store->name}}
	                          @else
	                            {{$store->name}}
	                          @endif
	                        @else
	                          {{$store->name}}
	                        @endif
	                    </a></h3>
	                    <p class="store-description">
	                        @if(isset($_COOKIE['locale']))
	                          @if($_COOKIE['locale']=='cn')
	                            {{$store->cn_description? : $store->description}}
	                          @else
	                            {{$store->description}}
	                          @endif
	                        @else
	                          {{$store->description}}
	                        @endif
	                    </p>
	                    <p>{{$store->pending_orders}} {{$lang['pending-orders']}}</p>
	                  </div>
	                </div>

	                <?php $ctr++; ?>
	              @endforeach
              </div> <!-- owl-carousel -->

				<div class="col-lg-12">
					<store-panel
		                v-for="(store,index) in listStore"
		                :store="store"
		                :key='store.id'
		                :index='index'
		                :param="param"
		            >
                  	</store-panel>
				</div>


			</div>
        </div>
    </div>
    <input type="hidden" id="discPrice" value="">
</header>
@endsection

@push('styles')
{!! Html::style('components/owl.carousel/dist/assets/owl.carousel.min.css') !!}
{!! Html::style('components/owl.carousel/dist/assets/owl.theme.default.min.css') !!}
{!! Html::style('components/rateYo/min/jquery.rateyo.min.css') !!}
{!! Html::style(mix('css/pages/store/index.css', 'shopping')) !!}
@endpush

@push('scripts')
{!! Html::script('components/owl.carousel/dist/owl.carousel.min.js') !!}
{!! Html::script('components/masonry/dist/masonry.pkgd.min.js') !!}
{!! Html::script('components/rateYo/min/jquery.rateyo.min.js') !!}
{!! Html::script('components/jq.ellipsis/jquery.ellipsis.min.js') !!}
{!! Html::script(mix('js/pages/store/index.js', 'shopping')) !!}

<script>
	$(function() {
		$('a.store-name').click( function(e) {
		    $('.collapse').collapse('hide');

		    $('div.panel-store').css('opacity', '0.5');
		    var id = $(this).attr('data-id');
		    $('div[data-id=_panel-'+id+']').css('opacity', '1');
		});

		// $('a.store-name:eq(0)').click();

	});
</script>
@endpush