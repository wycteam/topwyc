@extends('layouts.master')

@section('title', $lang['title'])

@push('styles')
    {!! Html::style(mix('css/pages/ewallet.css','shopping')) !!}
    {!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.css") !!}
    {!! Html::style("cpanel/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css") !!}
    {!! Html::style("cpanel/plugins/nouslider/jquery.nouislider.css") !!}
    {!! Html::style('components/datatables.net-zf/css/dataTables.foundation.min.css') !!}
    <style type="text/css">
        /*.dataTables_paginate{
            position: absolute !important;
            right: 10px  !important;
        }*/
    </style>
@endpush

@push('scripts')
    {!! Html::script("cpanel/plugins/jquery-ui/jquery-ui-draggable.min.js") !!}
    {!! Html::script(mix('js/pages/ewallet.js','shopping')) !!}
    {!! Html::script('components/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('components/datatables.net-zf/js/dataTables.foundation.min.js') !!}
@endpush

@section('content')

<!--  EWALLET PAGE -->
<header class="full-content custom-chosen">
    <div class='container-fluid' id='ewallet_container'>
        @include('includes.ewallet.modals')
        <div class="main-header-container">
            <h3 class="main-header">{{$lang['title']}} 
                <a href='#' 
                    class='fa fa-question text-warning' 
                    title="{{$lang['title']}}" 
                    data-container="#ewallet_container"
                    data-toggle="popover" 
                    data-placement="right" 
                    data-content="
                        <p>
                            {{$lang['ewallet-faq-1']}}
                        </p>
                        <p>
                            {{$lang['ewallet-faq-2']}}
                        </p>
                        
                        <p>
                            {{$lang['ewallet-faq-3']}}
                        </p>
                        <p>
                            <center>
                                <a href='/faq/contents?help=ewallet' class='center-block btn btn-raised btn-default' style='padding:10px;'>{{$lang['read-more']}}</a>
                            </center>
                        </p>
                    "
                ></a>
            </h3>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12 menu">
                <div class='col-md-12 breakdown'>
                    <b>{{$lang['available-balance']}}</b>
                    <p class='bigger-text'><span v-cloak>@{{ewallet_balance | currency}}</span></p>
                    <b>{{ __('ewallet.usable') }} <a href='#' 
                    id="breakdown"
                    class='text-warning' 
                    title="Breakdown" 
                    data-toggle="popover" 
                    data-placement="right" 
                    data-content="{{ $breakdown }}
                    "
                >{{ __('ewallet.show-details') }}</a>
            </b>
                    <p class='bigger-text'><span id="usableAmt" v-cloak>@{{usable_balance | currency}}</span></p>
                </div>
                <ewallet-button
                    v-for="btn in btns"
                    :btn='btn'
                    :key="btn.id">
                </ewallet-button>
            </div>
            <div class="col-md-9 col-sm-12">
                <div style="height:1px; clear:both;"></div> <!-- spacer -->
                <p class='medium-text'>{{$lang['transactions']}}</p>
                <hr>
                <div class="col-md-12 table-responsive">
                    <table class='table table-striped' id='ewallet_transaction_table'  data-order='[[ 0, "desc" ]]'>
                        <thead>
                            <tr>
                                <th>{{$lang['datetime']}}</th>
                                <th>{{$lang['ref-no-short']}}</th>
                                <th>{{$lang['type']}}</th>
                                <th>{{$lang['amount']}}</th>
                                <th>{{$lang['description']}}</th>
                                <th>{{$lang['status']}}</th>
                                <th>{{$lang['balance']}}</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr
                                data-toggle='tooltip'
                                :data-temp-title='tooltipForTransaction(transaction.status,transaction.type,transaction.reason)'
                                @click='(transaction.status=="Completed")||(transaction.type != "Deposit")?"":openModalUploadDeposit(transaction.id,transaction.status,transaction.reason,transaction.ref_number)' 
                                v-for='transaction in transactions' 
                                :key='transaction.id'
                                :class='(transaction.status=="Completed")||(transaction.type != "Deposit")?"":"mouse-hand"'
                                v-if='transaction.user_id == {{$user->id}}'
                                data-placement='bottom'
                             >
                                <td v-cloak>@{{transaction.updated_at}}</td>
                                <td v-cloak>@{{transaction.ref_number}}</td>
                                <td v-cloak>@{{transaction.type}}</td>
                                <td :class='CreditOrDebit(transaction.type)' v-cloak>@{{parseFloat(transaction.amount) | currency}}</td>
                                <td v-cloak>@{{transaction.description}}</td>
                                <td v-cloak>@{{transaction.status}}</td>
                                <td v-cloak>@{{parseFloat(transaction.balance) | currency}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
               

            </div>

        </div>
    </div>
</header>
@endsection

