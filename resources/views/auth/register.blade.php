@extends('layouts.master')

@section('title', $lang['register'])

@section('content')
<div class="container register-container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{$lang['register']}}</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="first_name" class="control-label">{{$lang['first-name']}}</label>
                                    {!! Form::text('first_name', null, ['id' => 'first_name', 'class' => 'form-control']) !!}

                                    @if ($errors->has('first_name'))
                                    <small class="text-danger">
                                        {{ $errors->first('first_name') }}
                                    </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="last_name" class="control-label">{{$lang['last-name']}}</label>
                                    {!! Form::text('last_name', null, ['id' => 'last_name', 'class' => 'form-control']) !!}

                                    @if ($errors->has('last_name'))
                                    <small class="text-danger">
                                        {{ $errors->first('last_name') }}
                                    </small>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="email" class="control-label">{{$lang['email']}}</label>
                                    {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control']) !!}

                                    @if ($errors->has('email'))
                                    <small class="text-danger">
                                        {{ $errors->first('email') }}
                                    </small>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password" class="control-label">{{$lang['password']}}</label>
                                    {!! Form::password('password', ['id' => 'password', 'class' => 'form-control']) !!}

                                    @if ($errors->has('password'))
                                    <small class="text-danger">
                                        {{ $errors->first('password') }}
                                    </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password_confirmation" class="control-label">{{$lang['confirm-password']}}</label>
                                    {!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="birth_date" class="control-label">{{$lang['date-of-birth']}}</label>
                                    {!! Form::text('birth_date', null, [
                                        'id' => 'birth_date',
                                        'class' => 'form-control',
                                        'placeholder' => 'YYYY-MM-DD',
                                    ]) !!}

                                    @if ($errors->has('birth_date'))
                                    <small class="text-danger">
                                        {{ $errors->first('birth_date') }}
                                    </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="gender" class="control-label">{{$lang['gender']}}</label>
                                    {!! 
                                        Form::select(
                                            'gender', 
                                            ['Male' => $lang['male'], 'Female' => $lang['female']], 
                                            null, 
                                            ['class' => 'form-control', 'placeholder' => $lang['ph-gender']]
                                        )
                                    !!}

                                    @if ($errors->has('gender'))
                                    <small class="text-danger">
                                        {{ $errors->first('gender') }}
                                    </small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-sm-8">
                                <p><small>{{$lang['by-agreement']}} <a href="/terms-and-conditions">{{$lang['terms-conditions']}}</a>.</small></p>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group text-right m-b-2 p-b-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{$lang['register']}}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
{!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') !!}
{!! Html::style(mix('css/pages/register.css', 'shopping')) !!}
@endpush

@push('scripts')
{!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}
{!! Html::script('components/jquery-mask-plugin/dist/jquery.mask.min.js') !!}
{!! Html::script(mix('js/pages/register.js', 'shopping')) !!}
@endpush
