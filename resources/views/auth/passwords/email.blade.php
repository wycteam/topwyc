@extends('layouts.master')

@section('title', $lang['forgot-password'])

@push('styles')
{!! Html::style(mix('css/pages/reset-password.css', 'shopping')) !!}
@endpush

@section('content')
<div class="container reset-password-container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{$lang['reset-password']}}</div>

                <div id="success-cont" class="panel-body" style="display:none">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{$lang['email-address']}}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" :value = email readonly="">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{$lang['send-pw-link']}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="question-cont" class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif               
                    <div id="email2-cont" class="form-group" :class="{ 'has-error': form.errors.has('email2') }" style="padding-bottom: 35px;">
                        <label id="l-email" for="email2" class="col-md-4 control-label">{{$lang['email-address']}}</label>

                        <div class="col-md-7">
                            <input id="email2" type="email" v-model="form.email2" class="form-control" name="email2" required>
                            <input id="primary-email" type="hidden" v-model="form.primary_email" class="form-control" name="primary_email" required>
                            <span class="help-block" v-if="form.errors.has('email2')" v-text="form.errors.get('email2')"></span>
                        </div>
                    </div>

                    <div class="form-group answer-cont" style="display:none">
                        <label for="question" class="col-md-2 control-label">{{$lang['question']}}</label>

                        <div class="col-md-10">
                            <select id='question' class='form-control' required style="font-size: 12px">
                                <option value="0">{{$lang['select-question']}}...</option>
                                <option :value=item.question.id v-for="item in question">@{{item.question.question}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group answer-cont" :class="{ 'has-error': form.errors.has('answer') }" style="display:none">
                        <label for="answer" class="col-md-2 control-label">{{$lang['answer']}}</label>

                        <div class="col-md-10">
                            <input id="answer" type="text" v-model="form.answer" class="form-control" name="answer" required>
                            <span class="help-block" v-if="form.errors.has('answer')" v-text="form.errors.get('answer')"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button id="search" class="btn btn-primary" @click='searhEmail()'>
                                {{$lang['search-email']}}
                            </button>
                        </div>
                    </div>

                    <div id="search-alt" class="form-group" style="display:none">
                        <div class="col-md-6 col-md-offset-4">
                            <button class="btn btn-primary" @click='searhAltEmail()'>
                                {{$lang['search-email']}}
                            </button>
                        </div>
                    </div>

                    <div class="form-group answer-cont" style="display:none">
                        <div class="col-md-6 col-md-offset-4">
                            <button id="answer" class="btn btn-primary" @click='answerQuestion()'>
                                {{$lang['submit']}}
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
{!! Html::script(mix('js/pages/reset-password.js','shopping')) !!}
@endpush
