<form id="login-form" role="form" method="POST" action="{{ route('login') }}" @submit.prevent="login" @keydown="form.errors.clear($event.currentTarget.name)" novalidate>
  <div class="form-group label-floating">
    <label for="email" class="control-label">@lang('header.email')</label>
    <input v-model="form.email" id="email" type="text" class="form-control" name="email" autofocus>
    <small class="text-danger" v-if="form.errors.has('email')" v-text="form.errors.get('email')"></small>
  </div>

  <div class="form-group label-floating">
    <label for="password" class="control-label">@lang('header.password')</label>
    <input v-model="form.password" id="password" type="password" class="form-control" name="password">
    <small class="text-danger" v-if="form.errors.has('password')" v-text="form.errors.get('password')"></small>
  </div>

  <div class="form-group">
    <div class="checkbox">
      <label>
        <input v-model="form.remember" type="checkbox" name="remember"> @lang('header.remember')
      </label>
    </div>
  </div>

  <p><a class="link-forgot-password" href="{{ route('password.request') }}">@lang('header.forgot')</a></p>
  <p>@lang('header.dont-have-account') <a class="link-forgot-password" href="{{ route('register') }}">@lang('header.sign-up')</a></p>

  <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-right"></i> @lang('header.login')</button>
</form>