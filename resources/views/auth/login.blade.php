<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Login | WATAF</title>

    {{-- Vendor --}}
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" integrity="sha256-j+P6EZJVrbXgwSR5Mx+eCS6FvP9Wq27MBRC/ogVriY0=" crossorigin="anonymous" />
    {{-- /Vendor --}}

    {!! Html::style(mix('css/inspinia.css', 'cpanel')) !!}
</head>

<body class="gray-bg">
    <!-- <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            {{-- <div>
                <h1 class="logo-name">WA+</h1>
            </div> --}}

            <h3>Welcome to WATAF</h3>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

            <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Don't have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Create an account</a>
            </form>
            <p class="m-t"> <small>WATAF &copy; 2017</small> </p>
        </div>
    </div> -->

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-3">
                <div id="iosBlurBg">
                    <div id="whiteBg"></div>
                </div>
                <div id="bottomEnter"></div>
                <div id="bottomBlurBg"></div>
                <!-- Login Form -->
                <div class="loginForm">
                    <div class="title">
                        <p>WYC GROUP<br><span>CPANEL</span></p>
                        <hr>
                        <hr class="short">
                    </div>
                    <form id="clogin" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="col-3">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input class="effect-2 form-control" type="email" name="email" placeholder="Email..." value="{{ old('email') }}" required autofocus>
                                <span class="focus-border"></span>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                <input class="effect-2 form-control" type="password" name="password" placeholder="Password..." required>
                                <span class="focus-border"></span>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

<!--                             <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div> -->
                        </div>

                        <div class="forget">
                            <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>
                        </div>
                        <button type="submit" class="hidden"></button>
            <p class="m-t"> <small>VISA &copy; 2017</small> </p>

                    </form>
                </div>
                <a href="#">
                    <div class="enterButton">
                        <i class="fa fa-lock fa-2x text-white"></i><br>
                        <a href="#" onclick="document.getElementById('clogin').submit();"><span class="enterText text-white">Enter</span></a>
                    </div>
                </a>
            </div>
        </div>
    </div>

    {{-- Vendor --}}
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    {{-- /Vendor --}}
</body>
</html>
