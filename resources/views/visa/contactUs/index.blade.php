@extends('layouts.master')

@section('title', 'Contact Us')

@push('styles')
{!! Html::style(mix('css/pages/visa/contactUs/index.css', 'shopping')) !!}
@endpush

@section('content')
<section class="contact-us">
	<div class="full-container">
	  <div class="full-header-banner">
	    <h1>{{$lang['tagline-1']}}</h1>
	    <div><p> 
	      {{$lang['tagline-1-desc']}}
	      <br>
	      {{$lang['tagline-1-desc2']}}
	      <br>
	      {{$lang['tagline-1-desc3']}}
	    </p></div>
	    
	    <a href="/services" class="btn btn-primary">{{$lang['view-services']}}</a>
	  </div><!-- end about-header-banner -->

	</div><!-- end about-container -->
</section>
<section class="contact-us">
	<div class="container contact-container">
        <div class="row m-b-lg ">
            <div class="col-lg-8">
                <div class="ibox-content">
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.6885403920514!2d120.99019531489459!3d14.559795181978695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c97c89c205a5%3A0x3301c2caabfdc8d9!2sWyc+Business+Consultancy+Inc.!5e0!3m2!1sfil!2sph!4v1454467293306" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>	
            <div class="col-lg-4">
                <address>
                    <h3><strong><i class="fa fa-map-marker" aria-hidden="true"></i><span class="navy"> WYC CONSULTANCY INC.</span></strong></h3>
                    Unit 110-111. Balagtas Street<br>Balagtas Villas, Barangay 15 <br>Pasay City 1306<br>
                    <i class="fa fa-phone" aria-hidden="true"></i> (02) 354 80 21
                </address>
                <div class="row m-b-lg">
                	<div class="col-lg-5">
                		<img src="/shopping/images/qr_ronnie.jpg" class="qr-code">
                		<h5><span class="navy">Ronnie (中英文)</span></h5>
                		<p>(+63) 917-561-6069</p>
                	</div>
                	<div class="col-lg-5">
                		<img src="/shopping/images/qr_red.jpg" class="qr-code">
                		<h5><span class="navy">Red (中文福建话)</span></h5>
                		<p>(+63) 917-111-1105</p>
                	</div>
                </div>
                <h3><strong><i class="fa fa-briefcase" aria-hidden="true"></i> <span class="navy">{{$lang['operating-hrs']}}</span></strong></h3>
                <p>{{$lang['sched']}}</p>
            </div>
        </div>
        <br><br>
    </div>
</section>
<br>
<br>
@endsection