@extends('layouts.master')

@section('title', 'Services')

@push('styles')
{!! Html::style(mix('css/pages/visa/services/index.css', 'shopping')) !!}

@endpush

@section('content')
<section class="service">
	<div class="full-container">
	  <div class="full-header-banner">
	    <h1>{{$data['lang']['tagline-1']}}</h1>
	    <div><p> 
	      {{$data['lang']['tagline-1-desc']}}
	      <br>
	      {{$data['lang']['tagline-1-desc2']}}
	      <br>
	      {{$data['lang']['tagline-1-desc3']}}
	    </p></div>
	    
	    <a href="/contact_us" class="btn btn-primary">{{$data['lang']['tagline-1-desc3']}}</a>
	  </div><!-- end about-header-banner -->

	</div><!-- end about-container -->
</section>
<section>
	<div class="wrapper wrapper-content container visa" id="services">
	    <div class="row">
	    	@foreach($data['services'] as $service)
	    	<div class="col-lg-4">
		        <div class="row">
		            <div class="col-lg-12 wow pulse ">
		                <div class="ibox collapsed">
		                    <div class="ibox-title">
		                        <h5>{{$service['detail']}}
		                        </h5>
		                        <div class="ibox-tools">
		                            <a class="collapse-link">
		                                <i class="fa fa-chevron-up"></i>
		                            </a>
		                        </div>
		                    </div>
		                    <div class="ibox-content">
		                        <p>{{$service['description']}}</p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
	    </div>
	</div>
</section>
<br>
<br>
@endsection
                                                                                                                     