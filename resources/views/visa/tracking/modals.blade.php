<dialog-modal size="modal-md" id="reportModal">
	<template slot="modal-title">
        <h4 id="modal-label" class="modal-title">{{$data['lang']['reports']}}</h4>
    </template>
    <template slot="modal-body" class="text-center">
        <table class="table table-stripped">
            <thead>
                <tr>
                    <th>{{$data['lang']['details']}}</th>
                    <th>{{$data['lang']['date_time']}}</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="reports in activeReport"
                    :item="reports" 
                    :key="reports.id"
                    is="report-list">
                </tr>
            </tbody>
        </table>
    </template>
    <template slot="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary">{{$data['lang']['close']}}</button>
    </template>
</dialog-modal>