<div class="row" >
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-lg-9">
                        @if($data['no_results'])
                        <h2 class="result-package">{{$data['lang']['no-result']}}</h2>
                        @else
                        <h2 class="result-package">{{$data['tracking']}}</h2>
                        <h4 class="result-name">{{$data['client']}} <span class="client-id">({{$data['client_id']}})</span></h4>
                        @endif
                    </div>
                    <div class="col-lg-3 pull-right">
                        <div class="form-group tracking-txt">
                            <input type="text" class="form-control" placeholder="{{$data['lang']['tracking_no']}}" @keyup.enter='search()'  v-model="searchTrack.tracking">
                        </div>
                    </div>
                </div>
            </div>
            @if(!$data['no_results'])
            <div class="ibox-content">
                <table class="footable table table-stripped toggle-arrow-tiny">
                    <thead>
                    <tr>
                        <th></th>
                        <th data-toggle="true">{{$data['lang']['service_detail']}}</th>
                        <th>{{$data['lang']['service_status']}}</th>
                        <th>{{$data['lang']['date_created']}}</th>
                        <th>{{$data['lang']['service_cost']}}</th>
                        <th>{{$data['lang']['addtl_charge']}}</th>
                        <th>{{$data['lang']['service_fee']}}</th>
                        <th>{{$data['lang']['total_cost']}}</th>
                    </tr>
                    </thead>
                    <tbody>
                         @foreach ($data['services'] as $service)
                         @if($service->active == 1)
                        <tr>
                            <td>
                                @if(count($service->report) == 0)
                                <button type="button" class="btn btn-outline btn-link btn-report" data-toggle="tooltip" data-placement="right" title="{{$data['lang']['no_report']}}">
                                    <i  class="fa fa-check-circle no-report" aria-hidden="true"></i>
                                </button>
                                @elseif(count($service->report) != 0)
                                <button type="button" class="btn btn-outline btn-link btn-report" @click="showReportModal({{$service->id}})">
                                    <i class="fa fa-commenting has-report" aria-hidden="true"></i>
                                </button>
                                @endif
                            </td>
                            <td>{{$service->detail}}</td>
                            <td>
                                @if(($service->status == 'Complete')||($service->status == 'complete'))
                                <p><span class="badge badge-success">{{$data['lang']['complete']}}</span></p>
                                @elseif (($service->status == 'On Process')||($service->status == 'on process'))
                                <p><span class="badge badge-primary">{{$data['lang']['on_process']}}</span></p>
                                @elseif (($service->status == 'Empty')||($service->status == 'empty'))
                                <p><span class="badge badge-warning">{{$data['lang']['empty']}}</span></p>
                                @endif
                            </td>
                            <td>{{$service->service_date}}</td>
                            <td>&#8369;@money_format($service->cost)</td>
                            <td>&#8369;@money_format($service->charge)</td>
                            <td>&#8369;@money_format($service->tip)</td>
                            <td>&#8369;@money_format($service->total_service_cost)</td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="8">
                            <ul class="pagination pull-right"></ul>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>           
            @endif
        </div>
    </div>
</div>
@if(!$data['no_results'])
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-8"></div>
        <div class="col-lg-4 pull-right">
            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['total_package_cost']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_cost'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['initial_deposit']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_deposit'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['total_payment']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_payment'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['total_refund']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_refund'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['total_discount']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_discount'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right available-bal-txt">
                    {{$data['lang']['available_balance']}}
                </div>
                <div class="col-lg-4 text-left total-amount available-bal-txt">
                    &#8369;@money_format($data['available_balance'])
                </div>
            </div>
        </div>
    </div>
</div>
@endif