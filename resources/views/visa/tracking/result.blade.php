@extends('layouts.master')

@section('title', 'WYC Tracker')

@push('styles')
	{!! Html::style(mix('css/pages/visa/tracking/index.css', 'shopping')) !!}
    {!! Html::style('cpanel/plugins/footable/footable.core.css') !!}
    {!! Html::style('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') !!}
@endpush

@section('content')
<section class="visa tracking result" id="result">
    <div class="container">
        <div class="wrapper wrapper-content animated result-container">
            @include('visa.tracking.modals')
            @if($data['type'] == 'package')
            <!-- PACKAGE RESULTS-->
                @include('visa.tracking.packages')
            @elseif ($data['type'] == 'group')
            <!-- GROUP RESULTS -->
                @include('visa.tracking.groups')
            @endif
        </div>
    </div>
</section>
@endsection

@push('scripts')
    {!! Html::script(mix('js/visa/tracking/index.js', 'shopping')) !!}
@endpush