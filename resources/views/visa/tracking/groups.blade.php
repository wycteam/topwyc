<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-lg-9">
                        <h2 class="result-package">{{$data['tracking']}}</h2>
                        <h4 class="result-name">{{$data['group_name']}}</h4>
                    </div>
                    <div class="col-lg-3 pull-right">
                        <div class="form-group tracking-txt">
                            <input type="text" class="form-control" placeholder="{{$data['lang']['tracking_no']}}" @keyup.enter='search()'  v-model="searchTrack.tracking">
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-container">
                    <table class="footable table-responsive table table-stripped toggle-arrow-tiny">
                        <thead>
                        <tr>
                            <th data-toggle="true">{{$data['lang']['name']}}</th>
                            <th>{{$data['lang']['client_number']}}</th>
                            <th>{{$data['lang']['total_service_cost']}}</th>
                            <th data-hide="all"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['groupmembers'] as $member)
                            <tr>
                                <td>{{$member->client->full_name}} 
                                    @if($data['group_leader'] == $member->client->id)
                                    <button type="button" class="btn btn-outline btn-link btn-report" data-toggle="tooltip" data-placement="right" title="{{$data['lang']['group_leader']}}">
                                        <i class="fa fa-star leader-icon" aria-hidden="true"></i>
                                    </button>
                        
                                    @endif
                                </td>
                                <td>{{$member->client->id}}</td>
                                <td>&#8369;@money_format($member['total_service_cost'])</td>
                                
                                <td colspan="7">
                                    @if($member['total_services'] != 0)
                                    <table class="table table-stripped services-tbl">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="tbl-child-header">{{$data['lang']['date']}}</th>
                                                <th class="tbl-child-header">{{$data['lang']['packages']}}</th>
                                                <th class="tbl-child-header">{{$data['lang']['detail']}}</th>
                                                <th class="tbl-child-header">{{$data['lang']['cost']}}</th>
                                                <th class="tbl-child-header">{{$data['lang']['charge']}}</th>
                                                <th class="tbl-child-header">{{$data['lang']['tip']}}</th>
                                                <th class="tbl-child-header">{{$data['lang']['discount']}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($member['services'] as $service)
                                            @if($service->group_id != 0)
                                            @if($service->active == 1)
                                            <tr>
                                                <td>
                                                    @if(count($service->report) == 0)
                                                    <button type="button" class="btn btn-outline btn-link btn-report" data-toggle="tooltip" data-placement="right" title="{{$data['lang']['no_report']}}">
                                                        <i  class="fa fa-check-circle no-report" aria-hidden="true"></i>
                                                    </button>
                                            
                                                    @elseif(count($service->report) != 0)
                                                    <button type="button" class="btn btn-outline btn-link btn-report" @click="showReportModal({{$service->id}})">
                                                        <i class="fa fa-commenting has-report" aria-hidden="true"></i>
                                                    </button>
                                                    @endif
                                                </td>
                                                <td>{{$service->service_date}}</td>
                                                <td>{{$service->tracking}}</td>
                                                <td>{{$service->detail}}</td>
                                                <td>&#8369;@money_format($service->cost)</td>
                                                <td>&#8369;@money_format($service->charge)</td>
                                                <td>&#8369;@money_format($service->tip)</td>
                                                @if($service-> discount == '[]')
                                                <td>&#8369;0.00</td>
                                                @elseif($service->discount != '[]')
                                                <td>&#8369;@money_format($service->discount)</td>
                                                @endif
                                            </tr>
                                            @endif
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    <h3>{{$data['lang']['no_active_service']}}</h3>
                                    @endif
                                </td>
                               
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>   
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-8"></div>
        <div class="col-lg-4 pull-right">
            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['total_package_cost']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['total_cost'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['initial_deposit']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_deposit'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['total_payment']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_payment'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['total_refund']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_refund'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right">
                    {{$data['lang']['total_discount']}}
                </div>
                <div class="col-lg-4 text-left total-amount">
                    &#8369;@money_format($data['package_discount'])
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 text-right available-bal-txt">
                    {{$data['lang']['available_balance']}}
                </div>
                <div class="col-lg-4 text-left total-amount available-bal-txt">
                    &#8369;@money_format($data['available_balance'])
                </div>
            </div>
        </div>
    </div>
</div>