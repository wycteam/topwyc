@extends('layouts.master')

@section('title', 'WYC Tracker')

@push('styles')
	{!! Html::style(mix('css/pages/visa/tracking/index.css', 'shopping')) !!}
@endpush

@section('content')
<section class="visa tracking result" id="result">
    <div class="container">
        <div class="wrapper wrapper-content animated result-container">
            <div class="row" >
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <div class="row">
                                <div class="col-lg-9">
                                    <h2 class="result-package">{{$lang['no-result']}}</h2>
                                </div>
                                <div class="col-lg-3 pull-right">
                                    <div class="form-group tracking-txt">
                                        <input type="text" class="form-control" placeholder="{{$lang['tracking_no']}}" @keyup.enter='search()'  v-model="searchTrack.tracking">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    {!! Html::script(mix('js/visa/tracking/index.js', 'shopping')) !!}
@endpush