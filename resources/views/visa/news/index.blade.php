@extends('layouts.master')

@section('title', 'Services')

@push('styles')
{!! Html::style(mix('css/pages/visa/services/index.css', 'shopping')) !!}
{!! Html::style(mix('css/pages/news.css', 'shopping')) !!}
{!! Html::style(mix('css/pages/comment.css', 'shopping')) !!}
@endpush

@section('content')
<section class="service">
	<div class="full-container">

	  <div class="full-header-banner">

	    <h1>{{$data['lang']['tagline-1']}}</h1>
	    <div><p>
	      {{$data['lang']['tagline-1-desc']}}
	      <br>
	      {{$data['lang']['tagline-1-desc2']}}
	      <br>
	      {{$data['lang']['tagline-1-desc3']}}
	    </p></div>

	    <a href="/contact_us" class="btn btn-primary">{{$data['lang']['tagline-1-desc3']}}</a>
	  </div><!-- end about-header-banner -->

	</div><!-- end about-container -->
</section>
<section class="container">
    <div class="col-md-12">
      <div class="row">
					<div class="col-md-8">
							<div class="row main-contents">
								@if(!empty($data['news'][0]['image']))
								<img src='../../images/news/large/{{$data['news'][0]['image']}}'>
								@endif
								<br><br>
								<h2>{!!$data['news'][0]['header']!!}</h2>
								<br><br>
								{!!base64_decode($data['news'][0]['content'])!!}
							</div>
							@if($data['news'][0]['allow_comment']==1)
							<div class="row">
									<comment id="newsComments">  </comment>
							</div>
							@endif
					</div>
        <div class="col-md-4">
          <news-articles-featured id="newsLoader2">

          </news-articles-featured>
        </div>
      </div>
    </div>

</section>
<br>
<br>
@endsection

@push('scripts')
{!! Html::script(mix('js/visa/news2.js','shopping')) !!}
{!! Html::script(mix('js/visa/comment.js','shopping')) !!}
@endpush
