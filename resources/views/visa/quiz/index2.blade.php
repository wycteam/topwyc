@extends('layouts.master')

@section('title', 'WYC Quiz')

@push('styles')
	{!! Html::style(mix('css/pages/visa/tracking/index.css', 'shopping')) !!}
@endpush

<style>
.table-header-rotated {
  border-collapse: collapse;
}
.csstransforms .table-header-rotated td {
  width: 30px;
}
.no-csstransforms .table-header-rotated th {
  padding: 5px 10px;
}
.table-header-rotated td {
  text-align: center;
  padding: 10px 5px;
  border: 1px solid #ccc;
}
.csstransforms .table-header-rotated th.rotate {
  height: 140px;
  white-space: nowrap;
}
.csstransforms .table-header-rotated th.rotate > div {
  -webkit-transform: translate(25px, 51px) rotate(315deg);
          transform: translate(25px, 51px) rotate(315deg);
  width: 30px;
}
.csstransforms .table-header-rotated th.rotate > div > span {
  border-bottom: 1px solid #ccc;
  padding: 5px 10px;
}
.table-header-rotated th.row-header {
  padding: 0 10px;
  border-bottom: 1px solid #ccc;
}
.scores{
    width: 180px;display: inline-block;
}
</style>

@section('content')
<section class="visa tracking result" id="result">
    <div class="container">
        <div class="wrapper wrapper-content animated result-container">
            <div class="row" style="background-color: #FFF;">
                <table class="table table-header-rotated" >
                  <thead>
                    <tr>
                      <!-- First column header is not rotated -->
                      <th></th>
                      <!-- Following headers are rotated -->
                        <th class="rotate" width="13%"><div><span><center>没有</center></span></div></th>
                        <th class="rotate" width="13%"><div><span><center>很轻</center></span></div></th>
                        <th class="rotate" width="13%"><div><span><center>中等</center></span></div></th>
                        <th class="rotate" width="13%"><div><span><center>偏重</center></span></div></th>
                        <th class="rotate" width="13%"><div><span><center>严重</center></span></div></th>
                    </tr> 
                  </thead>
                  <tbody>
                    <tr v-for="q in q2" class="quiz2">
                      <th class="row-header" v-cloak>@{{ q.id }}. @{{ q.question }}</th>
                      <td><input :name="'row'+q.id" type="radio" value="0" style="width : 60px; height: 15px;"></td>
                      <td><input checked :name="'row'+q.id" type="radio" value="1" style="width : 60px; height: 15px;"></td>
                      <td><input :name="'row'+q.id" type="radio" value="2" style="width : 60px; height: 15px;"></td>
                      <td><input :name="'row'+q.id" type="radio" value="3" style="width : 60px; height: 15px;"></td>
                      <td><input :name="'row'+q.id" type="radio" value="4" style="width : 60px; height: 15px;"></td>
                    </tr>
                  </tbody>
                </table>

                <button class="button btn btn-primary pull-right" @click="submitQ2()" style="margin-right: 10px;">submit</button>
                <br><br>
                <br><br>
                <div class="row">
                    <div v-cloak class="col-sm-5"  style="margin-left: 15px; margin-bottom: 15px;">
                        <h3>Scores</h3>
                        <br>
                        <span class="scores">Total somatization : </span> <b >@{{ somatization }}</b><br>
                        <span class="scores">Compulsive symptoms : </span> <b>@{{ compulsive }}</b><br>
                        <span class="scores">Interpersonal Sensitivity : </span> <b>@{{ interpersonal }}</b><br>
                        <span class="scores">Total Depression : </span> <b>@{{ depression }}</b><br>
                        <span class="scores">Total Anxiety  : </span> <b>@{{ anxiety }}</b><br>
                        <span class="scores">Total Hostile   : </span> <b>@{{ hostile }}</b><br>
                        <span class="scores">Total Horror   : </span> <b>@{{ horror }}</b><br>
                        <span class="scores">Total Paranoid   : </span> <b>@{{ paranoid }}</b><br>
                        <span class="scores">Total Psychiatric    : </span> <b>@{{ psychiatric }}</b><br>
                        <span class="scores">Total score of other items    : </span> <b>@{{ others }}</b><br>
                        <span class="scores">Total Score    : </span> <b>@{{ totalScore }}</b><br>
                    </div>
                    <div  v-cloak class="col-sm-5"  style="margin-left: 15px; margin-bottom: 15px;">
                        <h3>&nbsp;</h3>
                        <br>
                        <span class="scores">Total Symptom Index : </span> <b >@{{ averageScore }}</b><br>
                        <span class="scores">Number of positive items : </span> <b>@{{ positives }}</b><br>
                        <span class="scores">Number of negative items : </span> <b>@{{ negatives }}</b><br>
                        <span class="scores">Positive symptom level : </span> <b>@{{ (isNaN(totalScore/positives) ? 0 : totalScore/positives) }}</b><br>
                        <!-- <span class="scores">Factor score : </span> <b>@{{ interpersonal }}</b><br> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    {!! Html::script(mix('js/visa/tracking/index.js', 'shopping')) !!}
@endpush