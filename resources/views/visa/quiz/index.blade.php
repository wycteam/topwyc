@extends('layouts.master')

@section('title', 'WYC Quiz')

@push('styles')
	{!! Html::style(mix('css/pages/visa/tracking/index.css', 'shopping')) !!}
@endpush

<style>
.table-header-rotated {
  border-collapse: collapse;
}
.csstransforms .table-header-rotated td {
  width: 30px;
}
.no-csstransforms .table-header-rotated th {
  padding: 5px 10px;
}
.table-header-rotated td {
  text-align: center;
  padding: 10px 5px;
  border: 1px solid #ccc;
}
.csstransforms .table-header-rotated th.rotate {
  height: 140px;
  white-space: nowrap;
}
.csstransforms .table-header-rotated th.rotate > div {
  -webkit-transform: translate(25px, 51px) rotate(315deg);
          transform: translate(25px, 51px) rotate(315deg);
  width: 30px;
}
.csstransforms .table-header-rotated th.rotate > div > span {
  border-bottom: 1px solid #ccc;
  padding: 5px 10px;
}
.table-header-rotated th.row-header {
  padding: 0 10px;
  border-bottom: 1px solid #ccc;
}
</style>

@section('content')
<section class="visa tracking result" id="result">
    <div class="container">
        <div class="wrapper wrapper-content animated result-container">
            <div class="row" style="background-color: #FFF;">
                <table class="table table-header-rotated" >
                  <thead>
                    <tr>
                      <!-- First column header is not rotated -->
                      <th></th>
                      <!-- Following headers are rotated -->
                        <th class="rotate" width="15%"><div><span><center>是</center></span></div></th>
                        <th class="rotate" width="15%"><div><span><center>否</center></span></div></th>
                    </tr> 
                  </thead>
                  <tbody>
                    <tr v-for="q in q1" class="quiz1">
                      <th class="row-header" v-cloak>@{{ q.id }}. @{{ q.question }}</th>
                      <td><input checked :name="'row'+q.id" type="radio" value="yes" style="width : 60px; height: 15px;"></td>
                      <td><input :name="'row'+q.id" type="radio" value="no"></td>
                    </tr>
                  </tbody>
                </table>

                <button class="button btn btn-primary pull-right" @click="submitQ1()" style="margin-right: 10px;">submit</button>
                <br><br>
                <div style="margin-left: 15px; margin-bottom: 15px;" v-cloak>
                    <br><br><h3>Scores</h3>
                    <br>
                    <span>A. Problem : </span> <b >@{{ scoreA }}</b><br>
                    <span>B. Self-blame : </span> <b>@{{ scoreB }}</b><br>
                    <span>C. Help : </span> <b>@{{ scoreC }}</b><br>
                    <span>D. Fantasy : </span> <b>@{{ scoreD }}</b><br>
                    <span>E. Avoidance  : </span> <b>@{{ scoreE }}</b><br>
                    <span>F. Rationalization   : </span> <b>@{{ scoreF }}</b><br>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    {!! Html::script(mix('js/visa/tracking/index.js', 'shopping')) !!}
@endpush