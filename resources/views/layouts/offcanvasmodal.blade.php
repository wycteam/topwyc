<modal id='modalHelp'  size="modal-md" content-size='12'>
	<template slot="modal-title">
        <h3 style="margin-top:0px">
            How can we help you?
        </h3>
        <h5>Ask us any question at all!</h5>
    </template>
    <template slot="modal-body">
	    		<div class='row'>
		    		<div class='col-md-12'>
						<div class="form-group" :class="{ 'has-error': form.errors.has('subject') }" style="margin-top: 0px">
						  <label class="control-label" for="subject">Subject</label>
						  <input v-model="form.subject" class="form-control" id="subject" type="text" autofocus>
						  <span class="help-block" v-if="form.errors.has('subject')" v-text="form.errors.get('subject')"></span>
						</div>
		    		</div>
	    		</div>
	    		<div class='row '>
		    		<div class="col-md-12">
						<div class="form-group" :class="{ 'has-error': form.errors.has('body') }"  style="margin-top: 0px">
						  <label class="control-label" for="body">Describe the problem you're having in great detail.</label>
						  <textarea v-model="form.body" name="" id="body" required="" rows="1" class='form-control' autofocus required ></textarea>
						  <span class="help-block" v-if="form.errors.has('body')" v-text="form.errors.get('body')"></span>
						</div>
					</div>
	    		</div>
    </template>
    <template slot="modal-footer">
	     <div class='col-md-12' >
			<button id="submitConcern" class='btn btn-raised btn-success' @click='submitConcern()' :disabled="disabled"> Submit Concern</button>
	    </div>
	    <div class="col-md-12"></div>
<!-- 	     <div class='col-md-12' data-dismiss="modal" aria-label="Close">
			<button class='btn btn-default'>Close</button>
	    </div> -->
    </template>
</modal>