<footer class="footer">
  <!-- DESKTOP -->
  <div class="col-sm-12">
  	<div class="col-sm-4"></div>
  	<div class="col-sm-4 footer-copyright">
        <p>WYC GROUP <span>&copy;</span> 2018. @lang('footer.all-rights')</p>
  	</div>
  	<div class="col-sm-4"></div>
  </div>
  <!-- end of DESKTOP -->
</footer>
