<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="Xylena Derez | Joshua Dolor | Jeffrey Gatpandan | Immanuel Ocampo">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="locale-lang" content="{{ Cookie::get('locale') }}">

  <title>@yield('title') - {{ config('app.name') }}</title>


  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
  <link rel="manifest" href="/images/favicon/manifest.json">
  <link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#5bbad5">

  <meta name="theme-color" content="#ffffff">
  <!-- Material Design fonts -->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

  <!-- components -->
  {!! Html::style('components/PACE/themes/orange/pace-theme-flash.css') !!}
  {!! Html::script('cpanel/plugins/pace/pace.min.js') !!}
  {!! Html::style('components/toastr/toastr.min.css') !!}

  {!! Html::style('components/perfect-scrollbar/css/perfect-scrollbar.min.css') !!}

  {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css') !!}
  {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css') !!}
  {!! Html::style('https://unpkg.com/vue-multiselect@2.0.0/dist/vue-multiselect.min.css') !!}

  {!! Html::style(mix('css/app.css', 'shopping')) !!}
  <!-- {!! Html::style(mix('css/main.css', 'shopping')) !!} -->
 
  <!-- Sweet Alert -->
  {!! Html::style('components/sweetalert2/dist/sweetalert2.min.css') !!}

  {!! Html::style('shopping/plugins/mixitup/css/style.css') !!}

  @stack('styles')

  <script>
    window.Laravel = {!! json_encode([
      'csrfToken' => csrf_token(),
      'accessToken' => access_token(),
      'isAuthenticated' => Auth::check(),
      'user' => Auth::check() ? Auth::user()->load(['settings']) : null,
      'message' => session('message') ?: null,
      'data' => session('data') ?: null,
      // 'rates' => session('rates') ?: null,
      'base_api_url' => config('app.wataf_api_subdomain'),
      'cpanel_url' => config('app.wataf_cpanel_subdomain'),
      'language' => config('app.locale'),
    ]) !!};
  </script>

  @if (Auth::check())
  <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
  @endif
</head>
<body  id="page-top" class="landing-page no-skin-config">



  <div id="app">

    @yield('content')

  </div>




  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.7/metisMenu.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <!-- Components / Plugins -->
  {!! Html::script('components/toastr/toastr.min.js') !!}
  
  {!! Html::script('shopping/plugins/ddslick.min.js') !!}
  
  {!! Html::script('components/datatables.net/js/jquery.dataTables.min.js') !!}
  {!! Html::script('components/datatables.net-zf/js/dataTables.foundation.min.js') !!}

  {!! Html::script('components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
  {!! Html::script('components/jq.ellipsis/jquery.ellipsis.min.js') !!}
  {!! Html::script('shopping/plugins/carousel/owl.carousel.js') !!}
  {!! Html::script('shopping/plugins/jqzoom/elevatezoom.js') !!}
  {!! Html::script('cpanel/plugins/wow/wow.min.js') !!}
  {!! Html::script('cpanel/plugins/slimscroll/jquery.slimscroll.min.js') !!}
  <!-- {!! Html::script('cpanel/plugins/pace/pace.min.js') !!} -->

  {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js') !!}
  {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js') !!}
  {!! Html::script('https://unpkg.com/vue-multiselect@2.0.0') !!}

    <!-- Sweet Alert -->

    {!! Html::script('components/sweetalert2/dist/sweetalert2.min.js') !!}
    {!! Html::script('components/jquery-date-dropdowns/dist/jquery.date-dropdowns.min.js') !!}
  {!! Html::script(mix('js/app.js', 'shopping')) !!}
  {!! Html::script(mix('js/main.js', 'shopping')) !!}
  {!! Html::script(mix('js/inspinia.js', 'cpanel')) !!}
  {!! Html::script('cpanel/plugins/footable/footable.all.min.js') !!}

  @if (! Auth::check())
  {!! Html::script(mix('js/login.js', 'shopping')) !!}
  @endif

  @stack('scripts')
<script type="text/javascript">
$(document).ready(function(){
        $('#birth_date').datepicker();
          $("#language-container").click(function(){
           $("#lang li").not(':first').slideToggle();
        });

        $("#lang li").click(function() {
            var url = location.href = "index.php?lang=" + $(this).find('a').attr("data-val");
           location.href =url;
        });

        $('.top-header li[data-toggle="tooltip"]').on('click', function() {
          var id = $(this).attr('aria-describedby');

          $('div#' + id).hide();
        });

         //$("#lightgallery").lightGallery();
         $("#bday").dateDropdowns({
              displayFormat: "mdy"
          });

        $('.date-dropdowns').addClass('form-group');
        $('.month').addClass('form-control');
        $('.day').addClass('form-control');
        $('.year').addClass('form-control');

        $('.month').wrap('<div class="col-sm-4 col-xs-4" style="padding-right: 0px;    margin-top: -6px; -webkit-order:   2;-moz-order:  2;-ms-order:  2;-o-order:   2;order:   2;"></div>');
        $('.day').wrap('<div class="col-sm-4 col-xs-4" style="padding-right: 0px;    margin-top: -6px; -webkit-order:   1;-moz-order:  1;-ms-order:  1;-o-order:   1;order:   1;"></div>');
        $('.year').wrap('<div class="col-sm-4 col-xs-4" style="    margin-top: -6px; -webkit-order:   3;-moz-order:  3;-ms-order:  3;-o-order:   3;order:   3;"></div>');


        $('.day').css("padding-right", "0px");
         // $(".month").css({ width: "40%" });
         // $(".day").css({ width: "30%" });
         // $(".year").css({ width: "30%" });

})
</script>
</body>
</html>
