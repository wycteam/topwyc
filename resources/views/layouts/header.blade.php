<!-- Top Header -->
<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top topwyc" role="navigation" id="topHeader">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="width: auto;!important">
                    <i class="fa fa-bars" aria-hidden="true" style="color:#009688; font-size: xx-large;"></i>
                </button>
                <a class="navbar-brand" href="/">@lang('header.wyc-group')</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="page-scroll" href="/">@lang('header.home')</a></li>
                    <li><a class="page-scroll" href="/services">@lang('header.services')</a></li>
                    <li><a class="page-scroll" href="/tracking">@lang('header.tracking')</a></li>
                    <li><a class="page-scroll" href="/contact_us">@lang('header.contact-us')</a></li>
                    <!--<li><a class="page-scroll" href="/gallery">Gallery</a></li>-->
                    <li class="trans-switch">
                        <a class="btn btn-outline btn-link btn-lang" data-toggle="tooltip" data-placement="right" title="@lang('header.change-language')">
                            <select id='language' class="select-lang">
                                <option value="en" data-imagesrc="/shopping/img/general/flag/en.jpg"></option>
                                <option value="cn" data-imagesrc="/shopping/img/general/flag/cn.jpg"></option>
                            </select>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
