<div id="chat-bar" class="panel panel-chat-bar hidden-xs">
  <div class="panel-body">
    <chat-box v-for="user in users" :user="user" :key="user.id"></chat-box>
  </div>
</div>