<div id="customer-service" class="panel panel-customer-service hidden-xs">
  <div @click="toggleVisibility" class="panel-heading">
    @lang('customer-service.customer-service')
  </div>
  <div class="content">
    <div class="panel-body">
      <form method="POST" action="/customer-service" @submit.prevent="submit">
        <div class="form-group label-floating" :class="{ 'vee-error': errors.has('subject') }">
          <label class="control-label" for="subject">@lang('customer-service.subject')</label>
          <input v-model="form.subject" v-validate="'required'" id="subject" name="subject" type="text" class="form-control">
          <small class="text-danger" v-if="errors.has('subject')">@lang('customer-service.please-subject')</small>
        </div>

        <div class="form-group label-floating" :class="{ 'vee-error': errors.has('body') }">
          <label class="control-label" for="body">@lang('customer-service.body')</label>
          <textarea v-model="form.body" v-validate="'required'" id="body" name="body" class="form-control" rows="3"></textarea>
          <small class="text-danger" v-if="errors.has('body')">@lang('customer-service.give-a-concern')</small>
        </div>

        <div class="row">
          <div class="col-xs-12 text-right">
            <button type="submit" class="btn btn-tangerine">@lang('common.save')</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>