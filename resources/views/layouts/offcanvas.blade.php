<div id="offcanvas" class="panel panel-offcanvas hidden-xs">
  <div class="panel-heading">
    <input v-model="search" @keyup="searchUser" type="text" name="user-search" placeholder="@lang('customer-support.search-user')">
  </div>
  <div class="panel-body">
    <div class="text-center">
      <beat-loader :loading="loading" size="14px"></beat-loader>
    </div>
    <ul class="user-list">
      <li class="liTitle"><b>@lang('customer-support.customer-support')</b></li>
      <li @click="contactSupport(support)" v-for="support in supports" :user="support" :key="support.id">
        <img :src="avatar(support)" class="img-circle">
        <span v-text="suppName(support)"></span>
        <span>
          <i class="dot isonline fa fa-circle" v-if="support.is_online"></i>
          <i class="dot fa fa-circle" v-else></i>
        </span>
      </li>
      <li class="liTitle"><b><hr></b></li>
      <li @click="select(user)" v-for="user in users" :user="user" :key="user.id">
        <img :src="avatar(user)" class="img-circle">
        <span v-text="fullName(user)"></span>
        <span>
          <i class="dot isonline fa fa-circle" v-if="user.is_online"></i>
          <i class="dot fa fa-circle" v-else></i>
        </span>
      </li>
    </ul>
  </div>
  <a @click.prevent="toggleOffcanvas" class="btn btn-default btn-toggle-offcanvas" href="#" role="button">
    <i class="material-icons">people</i>
  </a>
  @include('layouts.offcanvasmodal')
</div>

