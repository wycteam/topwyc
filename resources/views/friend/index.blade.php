@extends('layouts.master')

@section('title', 'Friends')

@push('styles')
{!! Html::style('components/perfect-scrollbar/css/perfect-scrollbar.min.css') !!}
{!! Html::style(mix('css/pages/friends.css', 'shopping')) !!}
@endpush

@push('scripts')
{!! Html::script('components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script(mix('js/pages/friends.js', 'shopping')) !!}
@endpush

@section('content')
<header class="full-content">
    <div class='container' id='relationships_container'>
        <div class="main-header-container">
          <h3 class="main-header">{{$lang['friends']}}</h3>
        </div>
        <div class='row' >

           <div class="col-lg-12" id='friends_holder'>
                <ul class="nav nav-tabs">
                    <li class="{{ 
                        (
                            app('request')->input('tab') == 'friend-requests' || 
                            !app('request')->input('tab') ||
                            (app('request')->input('tab') != 'friends' && app('request')->input('tab') != 'find-people')
                        )   
                            ? 'active' : ''
                    }}"><a data-toggle="tab" href="#requests-content">{{$lang['requests']}}</a></li>
                    <li class="{{ (app('request')->input('tab') == 'friends') ? 'active' : '' }}"><a data-toggle="tab" href="#friends-content">{{$lang['friends']}}</a></li>
                    <li class="{{ (app('request')->input('tab') == 'find-people') ? 'active' : '' }}"><a data-toggle="tab" href="#find-people-content">{{$lang['find-people']}}</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane {{ 
                        (
                            app('request')->input('tab') == 'friend-requests' || 
                            !app('request')->input('tab') ||
                            (app('request')->input('tab') != 'friends' && app('request')->input('tab') != 'find-people')
                        )   
                            ? 'active' : ''
                    }}" id='requests-content'>
                        <friend-container
                            v-for='friend in requests'
                            :data='friend'
                            type='request'
                            :key='friend.user.id'
                            v-if='!friend.requestor'
                        >
                        </friend-container>
                        <div class="col-lg-12 no-records" v-if='!requests.length'>
                                {{$lang['no-request']}}..
                        </div>
                    </div>
                    <div class="tab-pane {{ (app('request')->input('tab') == 'friends') ? 'active' : '' }}"  id='friends-content'>
                        <friend-container
                            v-for='friend in friends'
                            :data='friend'
                            :key='friend.user.id'
                            type='friend'
                        >
                        </friend-container>
                        <div class="col-lg-12 no-records"  v-if='!friends.length'>
                                {{$lang['no-friends']}}..
                        </div>
                    </div>
                    <div class="tab-pane {{ (app('request')->input('tab') == 'find-people') ? 'active' : '' }}" id='find-people-content'>
                        <div class="col-lg-offset-8 col-md-offset-8 col-lg-4 col-md-4">
                            <div class="form-group label-floating">
                                <input class="form-control" id="searchUser" type="text"  v-model='keyword' placeholder="{{$lang['search']}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3 v-if='searched.length'>{{$lang['result']}}</h3>
                                <friend-container
                                    v-for='friend in searched'
                                    :data='friend'
                                    type='searched'
                                    :key='friend.user.id'
                                >
                                    <template slot="date"><small class='tc-grey'>{{$lang['registered']}} @{{friend.user.created_at}}</small></template>
                                </friend-container>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>
                                    {{$lang['recommended']}}
                                </h3>
                                <friend-container
                                    v-for='friend in recommended'
                                    :data='friend'
                                    type='recommended'
                                    :key='friend.user.id'
                                >
                                    <template slot="date"><small class='tc-grey'>{{$lang['registered']}} @{{friend.user.created_at}}</small></template>
                                </friend-container>
                                <div class="col-lg-12 no-records"  v-if='!recommended.length'>
                                    {{$lang['no-recommendations']}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>
@endsection

