<!--  SITE MAP PAGE -->
@extends('layouts.master')

@section('title', 'Site Map')

@push('styles')
 	 {!! Html::style(mix('css/pages/home.css', 'shopping')) !!}
@endpush

@section('content')
<div class="full-content" id="sitemapWataf">
  <div class="container sitemap-context">
    <div class="col-sm-12">
      <div class="title-row">
          <div class="main-header-container">
              <h3 class="main-header">WATAF Site Map</h3>
          </div>
      </div>
      <!-- site-row-1 -->
      <div class="site-row-1 row">
       
        <div class="col-lg-3">
          <!-- ABOUT WATAF -->
          <div>
            <p class="section-title">About WATAF</p>
            <div class="link-set">
              <a href="{{route('shopping.about')}}"><p>Our Company</p></a>
              <a href="{{route('shopping.termsCondition')}}"><p>Terms & Conditions</p></a>
              <a href="{{route('shopping.privacyPolicy')}}"><p>Privacy Policy</p></a>
              <a href=""><p>Careers</p></a>
            </div>
          </div>
          <!-- end ABOUT WATAF -->

          <!-- OUR VALUES -->
          <div>
            <p  class="section-title">Our Values</p>
            <div class="link-set">
              <a href="">Social Responsibility<p></p></a>
              <a href=""><p>Integrity</p></a>
              <a href=""><p>Convenience</p></a>
            </div>
          </div>
          <!-- end OUR VALUES -->
        </div>
       

        <div class="col-lg-3">
          <!-- CUSTOMER SERVICE -->
          <div>
            <p class="section-title">Customer Service</p>
            <div class="link-set">
              <a href="{{route('shopping.faq')}}"><p>Frequently Asked Questions</p></a>
              <a href=""><p>Payment</p></a>
              <a href="/feedback"><p>Customer Feedback</p></a>
              <a href="/issues"><p>Report Issues</p></a>
            </div>
          </div>
          <!-- end CUSTOMER SERVICE -->
          <!-- ACCOUNT -->
          <div>
            <p class="section-title">Account</p>
            <div class="link-set">
              <a href="/register"><p>Create your Account</p></a>
              <a href="/profile"><p>Manage your Profile</p></a>
              <a href="/friends"><p>Manage your Contacts</p></a>
            </div>
          </div>
          <!-- end ACCOUNT -->
        </div>

        <div class="col-lg-3">
          <!-- STORE -->
          <div>
            <p class="section-title">Store</p>
            <div class="link-set">
              <a href="/stores/create"><p>Open a Store</p></a>
              <a href="/stores"><p>Manage your Stores</p></a>
              <a href=""><p>Search a store</p></a>
              <a href=""><p>Stores by Category</p></a>
            </div>
          </div>
          <!-- end STORE -->
          <!-- PRODUCT -->
          <div>
            <p class="section-title">Product</p>
            <div class="link-set">
              <a href="/product/create"><p>Sell your Product</p></a>
              <a href="/stores"><p>Manage your Products</p></a>
              <a href="/faq/contents?help=order"><p>How to Buy</p></a>
              <a href="/faq/contents?help=order"><p>Shipping & Delivery</p></a>
              <a href="/faq/contents?help=order"><p>How to Return</p></a>
              <a href=""><p>Search a Product</p></a>
              <a href="/categories"><p>Product by Category</p></a>
            </div>
          </div>
          <!-- end PRODUCT -->
        </div>

        <div class="col-lg-3">
          <!-- CONNECT WITH US -->
          <div>
            <p class="section-title">Connect With Us</p>
             <div class="link-set">
              <a href=""><p>Like Us on Facebook</p></a>
              <a href=""><p>Follow Us on Twitter</p></a>
            </div>
          </div>
          <!-- end CONNECT WITH US -->
        </div>
      </div>
      <!-- end site-row-1 -->
    </div><!-- end  col-sm-12-->
  </div><!-- end  container sitemap-context-->
</div><!-- end  sitemapWataf-->
@endsection

@push('scripts')
  {!! Html::script('shopping/js/pages/categories.js') !!}  
@endpush
