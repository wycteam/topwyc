<!--  PRIVACY POLICY PAGE -->
@extends('layouts.master')

@section('title', 'Privacy Policy')

@push('styles')
 	
@endpush

@section('content')
<div class="full-content" id="privacy-policy-content">
  <div class="container"><br><br>

    <div class="main-header-container">
      <h3 class="main-header">PRIVACY POLICY</h3>
    </div><!-- end  main-header-container-->
    <div class="col-sm-12">
    	<p>This Privacy Policy discloses the privacy practices for the WYC website. By availing for any of the services offered by WYC you are agreeing to the terms of this Privacy Policy. This policy is a legally binding agreement between you as the client (referred to as “you” or “your”) and WYC Business Consultancy Inc. and its affiliates, including but not limited to topwyc.com (referred to as “we”, “our”, “us” or “WYC”). If we add any new services or any changes to any services, they will also be subject to this policy. </p>

		<p>
		This Privacy Policy may be available in languages other than English. To the extent of any inconsistencies or conflicts between this English Privacy Policy and our Privacy Policy available in another language, the most current English version will prevail. 
		</p>

		<p><b>PLEASE MAKE SURE TO READ THIS PRIVACY POLICY CAREFULLY BY ACCESSING OR USING WYC WEBSITE/APP , YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTAND, AND AGREE TO BE BOUND TO ALL THE TERMS OF THIS PRIVACY POLICY. IF YOU DO NOT AGREE TO THESE TERMS, DO NOT ACCESS OR USE THE WEBSITE/APP. </b></p>

		<p><b>1. Changes to Privacy Policy</b></p>
		<p>Since technology and privacy laws are always changing, we may occasionally update this policy, we will post those changes to this privacy statement, the homepage, or other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. We reserve the right to modify this privacy statement at any time, so please review it frequently. If you continue to use WYC website/app after these changes are posted, you agree to the revised policy. </p>

		<p><b>2. Informations Collected and Uses of Collected Information</b></p>
		<p>We will keep your Personal Information accurate, current and complete and up-to-date with the information that you have provided with us. If you request access to your Personal Information, we will inform you of the existence, use and disclosure of your Personal Information as allowed by law, and provide you access to that information.</p>

		<ul>
			<li>
				<p><b>What information do we collect from clients and why? </b></p>
				<p>We collect your name, address, email address, phone number(s) and date of birth. We need this information to provide you with our services, for example, to confirm your identity, contact you, and invoice or pay you. </p>
				<p>We will also use Personal Information in other cases where you have given us your expressed permission. </p>

				<ul>
					<li>
						<p><b>When do we collect this information?</b></p>
						<p>We collect Personal Information when you avail for any of our Services, when you access our Services or otherwise provide us with the information. </p>
					</li>
				</ul>
			</li>
		</ul>

		<p><b>3. Release of Personally Identifiable Information</b></p>
		<p>We will not share, sell, rent, or trade your Personally Identifiable Information with other parties except as provided below: </p>

		<ul>
			<li>
				<p><b>We may share Personal Information with Third Party Service Providers. </b></p>
				<p>WYC may work with third parties to help provide you with our Services and we may share Personal Information with them to support these efforts. In certain limited circumstances, we may also be required by law to share information with third parties. We may also receive Personal Information from our partners and third parties. <br><br>

				Personal information may be shared with third parties to prevent, investigate, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our Terms and Conditions or any other agreement related to the Services, or as otherwise required by law.  <br><br>

				We undertake not to disclose any confidential information provided to us in the course of our work for clients, unless explicity authorized to do so or required by governments or other legal authorities. All WYC employees are contractually obliged to respect this undertaking on all internal matters.  <br><br>

				WYC remains responsible for Personal Information that is transferred to a third party for processing or to support our efforts. Any Personal Information transferred to a third party for data processing is subject, by law, to a comparable level of protection as that provided by WYC.  <br><br>

				A “comparable level of protection” means a level of protection generally equivalent to that provided by WYC. 
				</p>
			</li>

			<li>
				<p><b>We may share your information for our Protection and the Protection of Others.</b></p>
				<p>We reserve the right to disclose your personal information as required by law and when we believe that disclosure is necessary to protect our rights and/or comply with a judicial proceeding, court order, or legal process served on our website; enforce or apply this Privacy Policy, our website Terms and Conditions or other agreements; or protect the rights, property or safety of the website, its users or others.</p>
			</li>

			<li>
				<p><b>We may share your information in a Business Transfer.</b></p>
				<p>As with any other business, we could merge with, or be acquired by another company, whether through bankruptcy, dissolution, reorganization, or other similar transaction or proceedings. The successor company would acquire the informations we have maintained, including Personal Information. If this happens, we will post a notice on our home page. However, Personal Information would remain subject to this Privacy Policy.</p>
			</li>
		</ul>

		<p><b>4. Updating and Correcting Information</b></p>
		<p>Wataf takes reasonable steps to allow you to correct, amend or delete personal information that is shown to be inaccurate or incomplete. You may change some of your Personal Information by logging into your account and accessing the "Profile Settings" section of the website, or by contacting us through our Customer Support link.<br><br>

		We encourage you to update your Personal Information if it changes. You may also ask to have the information on your account be deleted or removed; however, because we keep track of past transactions, you cannot delete information associated with past transactions on the Wataf website. In addition, it may be impossible for us to completely delete all of your information because we periodically backup stored informations.</p>

		<p><b>5. What do we do with your Personal Information when you terminate your relationship with us?</b></p>
		<p>We will continue to store archived copies of your Personal Information for us to track past transactions, for legitimate business purposes and to comply with the law. </p>

		<p><b>6. What we don’t do with your Personal Information</b></p>
		<p>We do not and will never share, disclose, sell, rent, or otherwise provide Personal Information to other companies for the marketing of their own products or services. </p>

		<p><b>7. How do we keep your Personal Information secured?</b></p>
		<p>We follow industry standards on information security management to safeguard sensitive information, such as financial information, intellectual property and any other Personal Information entrusted to us. Our information security systems apply to people, processes and information technology systems on a risk management basis. </p>

		<p><b>8. How do we protect your information across borders?</b></p>
		<p>We may transmit Personal Information outside of your jurisdiction of residence. WYC remains responsible for Personal Information that is transferred to a third party abroad for processing or to support our efforts. Any Personal Information transferred to a third party for data processing is subject, by law, to a comparable level of protection as that provided by WYC. <br><br>

		A “comparable level of protection” means a level of protection generally equivalent to that provided by WYC. </p>

		<p><b>9. General</b></p>
		<p>Availing services on WYC requires trust on your part. We value your trust very highly and pledge to our clients, that we will work hard to protect the security and privacy of any personal information you provide to us and that your personal information will only be used as set forth in this Policy. This includes your name, address, phone number, email address or checking account information, in addition to any other personal information that can be linked to you, personally. <br><br>

		WYC may provide links to certain third party websites. This Privacy Policy applies only to activities conducted and personal information collected on WYC website. Other web sites may have their own policies regarding privacy and security. We encourage you to review the privacy policies on these sites before you use and access them. You are solely responsible for your use and access of other websites. <br><br>

		Terms which have not been defined or stipulated in this Agreement shall be interpreted in accordance with the definition(s) or provision(s) of the Privacy policy of WYC. </p>

		<p><b>&copy;&nbsp;WYC Business Consultancy Inc. </b></p>

    </div>
  </div><!-- end  container-->
</div><!-- end  privacy-policy-content-->
@endsection
