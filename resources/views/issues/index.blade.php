@extends('layouts.master')

@section('title', 'Issues')

@section('content')
  <div id="issues-container" class="container">
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
              <div class="col-sm-9">
                <div class="btn-group" role="group" aria-label="Basic example">
                  <a href="javascript:void(0)" @click="toggle('1')" class="btn btn-raised" :class="(pageTitle == '1') ? 'btn-wyc-global-orange' : ''">@lang('customer-service.all-tickets')</a>
                  <a href="javascript:void(0)" @click="toggle('open')" class="btn btn-raised" :class="(pageTitle == 'open') ? 'btn-wyc-global-orange' : ''">@lang('customer-service.open-tickets')</a>
                  <a href="javascript:void(0)" @click="toggle('closed')" class="btn btn-raised" :class="(pageTitle == 'closed') ? 'btn-wyc-global-orange' : ''">@lang('customer-service.closed-tickets')</a>
                </div>
              </div>

              <div class="col-sm-3">
                <button type="button" class="btn btn-raised btn-wyc-global-orange pull-right" data-toggle="modal" data-target="#createTicketModal" @click="createTicketModal">@lang('customer-service.create-new-ticket')</button>
              </div> <!-- col-md-6 -->

              @if(!$openTicketsCount)
                <div id="no-ticket-wrapper" class="col-md-12">
                  <img src="{{ asset('images/Open-Ticket-EmptyState-01.svg') }}" class="img-responsive">
                  <p>@lang('customer-service.you-dont-have-any-open-tickets')</p>
                </div> <!-- col-md-12 -->
              @endif

              <div style="height:1px; clear:both;"></div> <!-- spacer -->

                <div class="col-md-12 ibox">

                  <div class="table-responsive ibox-content" v-cloak>
                    <table id="issuesTable" class="table table-bordered table-striped table-hover dt-issues htFixTable">
                      <thead>
                        <tr>
                          <th>@lang('customer-service.issue-number')</th>
                          <th>@lang('customer-service.subject')</th>
                          <th>@lang('customer-service.date-created')</th>
                          <th class="text-center">@lang('customer-service.status')</th>
                        </tr>
                      </thead>
                      <tbody>
                            <tr v-for="issue in items">
                              <td>
                              
                              <a v-if="issue.chat_room && issue.status == 'closed'" href="#conversationModal" data-toggle="modal" :data-id="issue.chat_room.id" :data-subject="issue.subject" @click="getConversations">@{{ issue.issue_number }}</a>
                              <a v-else-if="issue.chat_room && issue.status == 'open'" :href="'/messages#/cr/'+issue.chat_room.id" target="_blank">@{{ issue.issue_number }}</a>
                              <span v-else>@{{ issue.issue_number }}</span>
                              </td>
                              <td>@{{ issue.subject }}</td>
                              <td>@{{ issue.created_at }}</td>
                              <td class="text-center">
                                <span v-if="issue.status == 'open'" class="label label-success">@lang('customer-service.open')</span>
                                <span v-if="issue.status == 'closed'" class="label label-default">@lang('customer-service.closed')</span>
                              </td>
                            </tr>
                      </tbody>
                    </table>
                  </div>
                </div> <!-- col-md-12 -->

              

            </div> <!-- row -->

          </div>
        </div>

    <!-- Create Ticket Modal -->
   <div class="modal fade" id="createTicketModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">@lang('customer-service.customer-service')</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-25px;">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form method="POST" @submit.prevent="submit">

          <div class="form-group label-floating" :class="{ 'vee-error': errors.has('subject') }">
            <label class="control-label" for="subject">@lang('customer-service.subject')</label>
            <input v-model="form.subject" v-validate="'required'" id="subject" name="subject" type="text" class="form-control">
            <small class="text-danger" v-if="errors.has('subject')" v-text="errors.first('subject')"></small>
          </div>

          <div class="form-group label-floating" :class="{ 'vee-error': errors.has('body') }">
            <label class="control-label" for="body">@lang('customer-service.body')</label>
            <textarea v-model="form.body" v-validate="'required'" id="body" name="body" class="form-control" rows="3"></textarea>
            <small class="text-danger" v-if="errors.has('body')" v-text="errors.first('body')"></small>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('common.close')</button>
            <button type="submit" class="btn btn-wyc-global-orange" :disabled="disabled">@lang('common.save')</button>
          </div>

        </form>

      </div>
    </div>
  </div>
  <!-- Create Ticket Modal -->

  <!-- Conversation Modal -->
  <div class="modal fade" id="conversationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">@lang('customer-service.message-thread')</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-25px;">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">

          <div style="max-height:500px; overflow-y: scroll;">
            <ul style="list-style: none; padding: 0;">
              <li v-for="conversation in conversations">
                <div class="message" :class="{ 'message-right': isMe(conversation), 'message-left': !isMe(conversation) }">@{{ conversation.body }}</div>
              </li>
            </ul>
          </div>

          <div class="alert alert-grey" role="alert" style="margin-top:20px;padding:30px;">
            <strong>@lang('customer-service.this-ticket-is-already-closed')</strong>
            @lang('customer-service.if-you-wish-to-reopen-this-issue-a-new-ticket-will-be-created')
            <button type="button" class="btn btn-raised btn-wyc-global-orange pull-right btn-sm" @click="reopenTicketModal">
              @lang('customer-service.reopen-ticket')
            </button>
          </div>

        </div> <!-- modal-body -->

      </div>
    </div>
  </div>
  <!-- Conversation Modal -->


    </div> <!-- issues-container -->

@stop

@push('styles')

  {!! Html::style('components/jQuery-contextMenu/dist/jquery.contextMenu.min.css') !!}
  {!! Html::style(mix('css/pages/message/index.css', 'shopping')) !!}

  <style>
    #issues-container { margin-top: 30px; padding-bottom: 80px; }
    #issues-container #no-ticket-wrapper { border: solid thin #ccc; text-align: center; width: 97%; margin-left: 1.5%; clear:both; padding: 40px 0px; margin-bottom: 10px;}
    #issues-container #no-ticket-wrapper p { font-weight: bold; color: #999; margin-top: 40px;}
    #issues-container #no-ticket-wrapper img { width: 30%; height:30%; margin: 0 auto;}
    #issues-container #issuesTable { margin-top: 20px; }
    #issues-container div#createTicketModal form { padding: 20px; }
    #issues-container #issuesTable_filter { float: right; display: inline; }
    #issues-container #issuesTable_length { display: inline; }
    #issues-container #issuesTable_wrapper .top { margin-top: 20px !important; }
    .message { background-color: #EF6C00; border-radius: 5px; padding:6px; clear:both; margin-bottom:10px;}
    .message-left { float:left; background-color: #D6D8D9; color: #464a4e; }
    .message-right { float:right; background-color: #EF6C00; color: #ffffff; }
    .alert-grey { background-color: #D3D3D3; color: #fff; }
  </style>

@endpush

@push('scripts')

  {!! Html::script('components/jq.ellipsis/jquery.ellipsis.min.js') !!}
  {!! Html::script('components/jQuery-contextMenu/dist/jquery.ui.position.min.js') !!}
  {!! Html::script('components/jQuery-contextMenu/dist/jquery.contextMenu.min.js') !!}
  {!! Html::script(mix('js/pages/message/index.js', 'shopping')) !!}

  <script>
    new Vue({
      el: '#issues-container',

      data: {
        items: [],
        $header: null,
        $chatBox: null,
        pageTitle: '1',
        form: new Form({
            subject: null,
            body: null,
            chatRoomId: null
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        newConvoForm: new Form({
            supportIds: [],
            flag: true
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        notif : [],
        conversations: [],
        disabled: false
      },
      created(){
          this.getSupports();
          this.getIssues('1');
      },
      methods: {
        //3 default support
        getSupports() {
            return axiosAPIv1.get('/getSupports')
                  .then(result => {
                      this.supports = result.data;
                      this.callDataTables();                
                  });
        },

        getIssues(status){
            return axiosAPIv1.get('/getIssues/'+status)
                  .then(result => {     
                    this.items = result.data;
                  });
        },

        submit() {
            this.$validator.validateAll();

            if (! this.errors.any()) {
                this.disabled = true;
                this.form.submit('post', '/issues')
                    .then(result => {
                      this.$validator.clean();

                      var offline = 0;
                      for(var i=0; i<this.supports.length; i++)
                      {
                          if(!this.supports[i].is_online)
                              offline = offline + 1;
                      }

                      var supp = 0;
                      if(offline!=this.supports.length){
                          for(var i=0; i<this.supports.length; i++)
                          {
                              if(this.supports[i].is_online)
                                  var supp = this.supports[i].id;
                          }
                      }

                      let casenum = result.data.issue_number;

                      //if(offline!=this.supports.length)
                      if(supp==0) supp = 2; // mam cha's account
                      this.newConvoForm.supportIds.push(supp);
                      this.newConvoForm.submit('put', '/customer-service/' + result.data.id)
                      .then(result => {
                          //alert(result.data.chat_room.name);
                        this.notif = result.data;
                        this.form.subject = null;
                        this.form.body = null;
                        this.form.chatRoomId = null;
                        this.newConvoForm.supportIds = [];

                          //open chat box with case number as title for the conversation
                          var chat = [];
                          $(".panel-chat-box").each(function(){
                              chat.push($(this).attr('id'));
                          });

                          if (casenum) {
                              if($.inArray('user'+casenum,chat)=='-1'){
                                  Event.fire('chat-box.show', this.notif[0]);
                                  this.getIssues(this.pageTitle);
                                  this.disabled = false;
                              }
                          }

                          setTimeout(function(){
                            $('.js-auto-size').textareaAutoSize();
                          }, 5000);
                      })
                      .catch($.noop);


                      $('#createTicketModal').modal('hide');

                      toastr.success((window.isChinese()) ? '你已经成功地创建了一张票。' : 'You have successfully created a ticket.', (window.isChinese()) ? 'Wataf购物' : 'Wataf Shopping', {timeOut:3000});
                    })
                    .catch($.noop);
            }
        },

        toggle(status){
            this.pageTitle = status;
            this.getIssues(status);
        },

        getConversations(e) {
          //this.form.subject = e.target.getAttribute('data-subject');
          this.form.chatRoomId = e.target.getAttribute('data-id');

          axiosAPIv1.get('/messages/chat-room/' + this.form.chatRoomId)
            .then(result => { 
              this.conversations = (result.data.data).slice().reverse(); 
            });
        },

        isMe(message) {
          return window.Laravel.user.id == message.user_id;
        },

        createTicketModal() {
          this.form.subject = null;
          this.form.body = null;
          this.form.chatRoomId = null;
        },

        reopenTicketModal() {
          this.form.body = null;
          $('#conversationModal').modal('hide');
          $('#createTicketModal').modal('show');
        },

        callDataTables(){

          setTimeout(function(){
            $('#issuesTable').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"top"lf>r<"clear">t<"bottom"p>',
                order: [[ 2, "desc" ]]
                });
           },500);
           
        },

      },
    watch:{
      items:function(){
          $('.dt-issues').DataTable().destroy();
          this.callDataTables();
      }
    },
    updated() {
      $('.ibox').children('.ibox-content').toggleClass('sk-loading');
    }
    });
  </script>

@endpush

