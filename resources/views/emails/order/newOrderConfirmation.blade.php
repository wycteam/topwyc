@component('mail::message')

Thanks for shopping with WATAF!<br>

Hi {{ $toUser->full_name }}, we are pleased to receive your order and are getting it ready to be shipped. We will notify you when it's on its way!<br>

@component('mail::button', ['url' => url('user/purchase-report')])
VIEW ORDER >
@endcomponent

<br>


@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            | Seller           |
| ----------------------------------------------  |:----------------:|:----------------:|:----------------:|------------:|
@for($i=0; $i<count($orderDetails); $i++)
| <img class="thumbs" src="{{ url($orderDetails[$i]->image) }}"> | {{ $products[$i]->name }} | <strong>{{ $orderDetails[$i]->quantity }}</strong>| ₱{{$orderDetails[$i]->subtotal }} |{{ $products[$i]->store->name }} |
@endfor
@endcomponent

<br>

@if($discount!=0)
<strong>Subtotal -</strong> ₱{{number_format($total,2)}}
<br>
<strong>Discount -</strong> ₱{{number_format($discount,2)}}
<br>
@endif
<strong>Total -</strong> ₱{{number_format($total-$discount,2)}}

<br>

<strong>Deliver To:</strong> {{ $order->name_of_receiver }} <br>
<strong>Contact Number:</strong> {{ $order->contact_number }}

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/purchase-report') }}</small>
@endcomponent