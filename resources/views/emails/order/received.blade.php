@component('mail::message')
# {{$fromUser->full_name}} successfully received your item {{$orderDetail->product->name}}

Dear {{ $orderDetail->order->user->full_name }},<br>
@if (empty($orderDetail->tracking_number))
Part of your
@else
Your
@endif
&nbsp;item <strong>#{{ $orderDetail->combined_tracking_number }}</strong> was received by {{$fromUser->full_name}}.  Thank you!<br>

@component('mail::button', ['url' => url('user/sales-report')])
Sales Report
@endcomponent

@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            | Seller           |
| ----------------------------------------------  |:----------------:|:----------------:|:----------------:|------------:|
| <img class="thumbs" src="{{ url($orderDetail->image) }}"> | {{ $orderDetail->product->name }} | <strong>{{ $orderDetail->quantity }}</strong>      | <strong>₱{{ ($orderDetail->sale_price > 0 ? $orderDetail->sale_price : $orderDetail->price_per_item) + $orderDetail->disc_fee }}</strong> |{{ $orderDetail->product->store->name }} |
@endcomponent

<br>
Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/sales-report') }}</small>
@endcomponent