@component('mail::message')
# New Purchase from {{$fromUser->full_name}}

Dear {{ $seller->full_name }},<br>

New Purchase from {{$fromUser->full_name}}. If you want to check all orders or you already want to ship this item, please click the link below. Thank you!<br>

@component('mail::button', ['url' => url('user/sales-report')])
Sales Report
@endcomponent

@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            | Seller           |
| ----------------------------------------------  |:----------------:|:----------------:|:----------------:|------------:|
@for($i=0; $i<count($details); $i++)
| <img class="thumbs" src="{{ url($details[$i]->image) }}"> | {{ $products[$i]->name}} | <strong>{{ $details[$i]->quantity }}</strong> | ₱{{ $details[$i]->subtotal }} |{{ $products[$i]->store->name }} |
@endfor
@endcomponent

<br>

@if($discount!=0)
<strong>Subtotal -</strong> ₱{{number_format($total,2)}}
<br>
<strong>Discount -</strong> ₱{{number_format($discount,2)}}
<br>
@endif
<strong>Total -</strong> ₱{{number_format($total-$discount,2)}}

<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/sales-report') }}</small>
@endcomponent