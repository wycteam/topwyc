@component('mail::message')
# Your order has been successfully returned to seller

Dear {{ $orderDetail->order->user->full_name }},<br>
@if (empty($orderDetail->tracking_number))
Part of your
@else
Your
@endif
&nbsp;order <strong>#{{ $orderDetail->combined_tracking_number }}</strong> that you returned was received by the seller. Your money will go back to your e-wallet. Please see below details for more information regarding the item.<br>

@component('mail::button', ['url' => url('user/purchase-report')])
Order Details
@endcomponent

<strong>Item Details</strong><br>


@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            | Seller           |
| ----------------------------------------------  |:----------------:|:----------------:|:----------------:|------------:|

| <img class="thumbs" src="{{ url($orderDetail->image) }}"> | {{ $orderDetail->product->name }} | <strong>{{ $orderDetail->quantity }}</strong>      | @if ($orderDetail->sale_price>0)
₱{{ ($orderDetail->sale_price * $orderDetail->quantity) + $orderDetail->disc_fee }}
@else
₱{{ ($orderDetail->sale_price * $orderDetail->quantity) + $orderDetail->disc_fee }}
@endif
 |{{ $orderDetail->product->store->name }} |

@endcomponent


<br>
@if($orderDetail->disc_fee!=NULL)
<strong>Discount -</strong> {{(($orderDetail->shipping_fee + $orderDetail->charge + $orderDetail->vat)*$orderDetail->quantity) - $orderDetail->disc_fee }}
@endif
<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/purchase-report') }}</small>
@endcomponent
