@component('mail::message')
# Your order has been delivered

Dear {{ $orderDetail->order->user->full_name }}, <br>
Thanks for shopping with us! We are glad to inform you that
@if (empty($orderDetail->tracking_number))
&nbsp;part of
@endif
&nbsp;your order <strong>#{{ $orderDetail->combined_tracking_number }}</strong> has been delivered, please see below details for more information regarding the item.<br>

@component('mail::button', ['url' => url('user/purchase-report')])
Order Details
@endcomponent

@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            |
| ----------------------------------------------  |:----------------:|:----------------:| ------------:|
@for($i=0; $i<count($sameStore); $i++)
| <img class="thumbs" src="{{ url($sameStore[$i]->image) }}"> | {{ $sameStore[$i]->product->name }} | <strong>{{ $sameStore[$i]->quantity }}</strong>      | ₱{{ $sameStore[$i]->subtotal }}				|
@endfor
@endcomponent
Total PHP incl. VAT and Discount : <strong>₱{{ number_format($fee,2) }}</strong><br>



<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/purchase-report') }}</small>
@endcomponent
