@component('mail::message')
# {{$fromUser->full_name}} requested to return your item {{$orderDetail->product->name}}

Dear {{ $orderDetail->order->user->full_name }},<br>
@if (empty($orderDetail->tracking_number))
Part of your
@else
Your
@endif
&nbsp;item <strong>#{{ $orderDetail->combined_tracking_number }}</strong> was returned by {{$fromUser->full_name}}. Reason: '{{$orderDetail->reason}}'. Customer Support will contact you to settle this. Thank you!<br>

@component('mail::button', ['url' => url('user/sales-report')])
Sales Report
@endcomponent



@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            | Seller           |
| ----------------------------------------------  |:----------------:|:----------------:|:----------------:|------------:|
@for($i=0; $i<count($orderDetail); $i++)
| <img class="thumbs" src="{{ url($orderDetail->image) }}"> | {{ $orderDetail->product->name }} | <strong>{{ $orderDetail->quantity }}</strong>      |@if ($orderDetail->sale_price>0)
₱{{ ($orderDetail->sale_price * $orderDetail->quantity) + $orderDetail->disc_fee }}
@else
₱{{ ($orderDetail->sale_price * $orderDetail->quantity) + $orderDetail->disc_fee }}
@endif  |{{ $orderDetail->product->store->name }} |
@endfor
@endcomponent
<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/sales-report') }}</small>
@endcomponent