@component('mail::message')
# Your order has been cancelled

Dear {{ $orderDetail->order->user->full_name }},<br>
@if (empty($orderDetail->tracking_number))
Part of your
@else
Your
@endif
&nbsp;order <strong>#{{ $orderDetail->combined_tracking_number }}</strong> has been cancelled, please see below details for more information regarding the item.<br>

@component('mail::button', ['url' => url('user/purchase-report')])
Order Details
@endcomponent

@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            | Seller           |
| ----------------------------------------------  |:----------------:|:----------------:|:----------------:|------------:|
| <img class="thumbs" src="{{ url($orderDetail->image) }}"> | {{ $orderDetail->product->name }} | <strong>{{ $orderDetail->quantity }}</strong>      | ₱{{ $orderDetail->subtotal }} |{{ $orderDetail->product->store->name }} |
@endcomponent

<br>

@if($orderDetail->disc_fee!=NULL)
<strong>Discount -</strong> ₱{{number_format((($orderDetail->shipping_fee + $orderDetail->charge + $orderDetail->vat)*$orderDetail->quantity) - $orderDetail->disc_fee,2) }}
@endif

<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/purchase-report') }}</small>
@endcomponent
