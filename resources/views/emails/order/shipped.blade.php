@component('mail::message')
# Your item has been shipped

Dear {{ $orderDetail->order->user->full_name }}, <br>
@if (empty($orderDetail->tracking_number))
Part of your order <strong>#{{ $orderDetail->combined_tracking_number }}</strong> has been shipped through <strong>{{ $orderDetail->courier }}</strong>!
@else
Your order <strong>#{{ $orderDetail->combined_tracking_number }}</strong> has been shipped through <strong>{{ $orderDetail->courier }}</strong>!
@endif

@component('mail::button', ['url' => url('user/purchase-report')])
Order Details
@endcomponent

Your order will be delivered to: <strong>{{ $orderDetail->order->name_of_receiver }}</strong><br>
Contact Number: <strong>#{{ $orderDetail->order->contact_number }}</strong>

@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            |
| ----------------------------------------------  |:----------------:|:----------------:| ------------:|
@for($i=0; $i<count($sameStore); $i++)
| <img class="thumbs" src="{{ url($sameStore[$i]->image) }}"> | {{ $sameStore[$i]->product->name }} | <strong>{{ $sameStore[$i]->quantity }}</strong>      | ₱{{ $sameStore[$i]->subtotal }}				|
@endfor
@endcomponent
Total PHP incl. VAT and Discount : <strong>₱{{ number_format($fee,2) }}</strong><br>



<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/purchase-report') }}</small>
@endcomponent
