@component('mail::message')
# Your order has been marked as pending

Dear {{ $orderDetail->order->user->full_name }},<br>
@if (empty($orderDetail->tracking_number))
Part of your
@else
Your
@endif
&nbsp;order <strong>#{{ $orderDetail->combined_tracking_number }}</strong> has been marked as pending, please see below details for more information regarding the item.<br>

@component('mail::button', ['url' => url('user/purchase-report')])
Order Details
@endcomponent

@component('mail::table')
| Product Image   | Product Name	              | Qty              | Price            | Seller           |
| ----------------------------------------------  |:----------------:|:----------------:|:----------------:|------------:|
@for($i=0; $i<count($orderDetail); $i++)
| <img class="thumbs" src="{{ url($orderDetail[$i]->image) }}"> | {{ $orderDetail[$i]->product->name }} | <strong>{{ $orderDetail[$i]->quantity }}</strong>      | ₱{{ $orderDetail[$i]->subtotal }} |{{ $orderDetail[$i]->product->store->name }} |
@endfor
@endcomponent

<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/purchase-report') }}</small>
@endcomponent
