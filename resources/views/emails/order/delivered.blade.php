@component('mail::message')
# Your order has been delivered

Dear {{ $orderDetail->order->user->full_name }},<br>
Thanks for shopping with us! We are glad to inform you that
@if (empty($orderDetail->tracking_number))
&nbsp;part of
@endif
&nbsp;your order <strong>#{{ $orderDetail->combined_tracking_number }}</strong> has been delivered, please see below details for more information regarding the item.<br>

@component('mail::button', ['url' => url('user/purchase-report')])
Order Details
@endcomponent

<strong>Item Details</strong><br>
Product Name: <strong>{{ $orderDetail->product->name }}</strong><br>
Price: <strong>₱{{ $orderDetail->subtotal }}</strong><br>
Quantity: <strong>{{ $orderDetail->quantity }}</strong><br>
Seller: <strong>{{ $orderDetail->product->store->name }}</strong><br>

<img src="{{ url($orderDetail->image) }}">

<br>

@if($orderDetail->disc_fee!=NULL)
<strong>Discount -</strong> ₱{{number_format((($orderDetail->shipping_fee + $orderDetail->charge + $orderDetail->vat)*$orderDetail->quantity) - $orderDetail->disc_fee,2) }}
@endif

<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ url('user/purchase-report') }}</small>
@endcomponent
