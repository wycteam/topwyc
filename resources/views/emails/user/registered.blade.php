@component('mail::message')
# Hello, {{ $toUser->full_name }}!

Please verify your email by clicking on the link or the button below.

@component('mail::button', ['url' => route('auth.confirm-email', ['token' => $toUser->verification_token])])
Verify Email
@endcomponent

If you did not register for an account on our website, please disregard this email.

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ route('auth.confirm-email', ['token' => $toUser->verification_token]) }}</small>
@endcomponent
