@component('mail::message')
# Hello, {{ $toUser->full_name }}!

Welcome to WATAF Shopping! Start shopping and get big discounts! Sell and double your sales. For assistance please send us a message.

If you did not register for an account on our website, please disregard this email.

Thanks,<br>
{{ config('app.name') }}

<hr>
@endcomponent