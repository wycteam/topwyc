@component('mail::message')

{{ $fromUser->full_name }} has reviewed your product {{ $review->products->name }}.

<br>
@if($review->image_path)
<img src="{{ url($review->image_path) }}/review.jpg">
@endif
<br>

{{ $review->content }}

<br>
@component('mail::button', ['url' => route('shopping.product.show', ['product' => $review->products->slug])])
VIEW PRODUCT >
@endcomponent


@endcomponent