@component('mail::message')
# Your withdraw request was approved.

Dear {{ $transaction->user->first_name }},<br>

Your withdraw request worth <i>P{{ $transaction->amount }}</i> <strong>(Transaction #{{ $transaction->ref_number }}</strong> was approved by the wataf admin. Expect the transfer within 2-3 banking days excluding holidays.<br>

@component('mail::button', ['url' => url('user/ewallet')])
View your ewallet transactions
@endcomponent

<br>
<br>

Thanks,<br>
{{ config('app.name') }}

<hr>

<small>If the above button does not work, please copy and paste the link below to your browser's address bar: {{ 'http://wataf.ph/user/ewallet'</small>
@endcomponent