<!--  TERMS AND CONDITIONS PAGE -->
@extends('layouts.master')

@section('title', 'Notifications')

@push('styles')
 	{!! Html::style(mix('css/pages/notifications.css','shopping')) !!}
@endpush

@section('content')
<div class="full-content" id="notifications-content">
  <div class="container">
	<div class="row">
	  <div class="col-md-3">
	  	<div class="left-pane">
            <img src="shopping/img/general/notifications/account.jpg" />
			<div class="tabs" id="notifications-tab">
			  <ul class="parent">
			    <li><img class="img-parent" src="shopping/img/general/notifications/circle.png" /><a class="a-parent">My Purchase</a></li>
			    <li><img class="img-parent" src="shopping/img/general/notifications/circle.png" /><a class="a-parent">Notifications</a></li>
				<tabs>
					<tab name="Order Updates" :selected="true"></tab>
					<tab name="Promotions"></tab>
					<tab name="Activity"></tab>
					<tab name="Wallet Updates"></tab>
					<tab name="Shopee Updates"></tab>
				</tabs>
			    <li><img class="img-parent" src="shopping/img/general/notifications/circle.png" /><a class="a-parent">MY Shopee Coins</a></li>
			  </ul>
			</div>
	  	</div>	
	  </div>
	  <div id="notifications-item" class="col-xs-12 col-md-9">
	  	<div class="right-pane">
	  	  <div class="col-md-12">
		  	  <div class="cont">
		  	  	<a id="#" class="mark-all">Mark all as read</a>
		  	  </div>
	  	  </div>
	  	     <tab-item
              v-for="item in items"
              :item="item"
              :key="item.id"
            >
            </tab-item>
	  	  <div id="WalletUpdates" style="display:none">
		  	  <div class="col-md-12">
		  	    <div class="center">
		  	  	<img src="shopping/img/general/notifications/no-updates.png" />
		  	  	<br/>
		  	  	<br/>
		  	  	<p>No Wallet Updates Yet</p>
		  	  	</div>
		  	  <br/>
		  	  </div>
		  </div>
		  <div id="ShopeeUpdates" style="display:none">
		  	  <div class="col-md-12">
		  	    <div class="center">
		  	  	<img src="shopping/img/general/notifications/no-updates.png" />
		  	  	<br/>
		  	  	<br/>
		  	  	<p>No Shopee Updates Yet</p>
		  	  	</div>
		  	  <br/>
		  	  </div>
		  </div>
	  	</div>
	  </div>
	</div>
  </div><!-- end  container-->
</div><!-- end  notifications-content-->
@endsection

@push('scripts')
  {!! Html::script(mix('js/pages/notifications.js','shopping')) !!}
@endpush
