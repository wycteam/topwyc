<!--  FAVORITES PAGE -->
@extends('layouts.master')

@section('title', $lang['title'])

@push('styles')
    {!! Html::style(mix('css/pages/favorites.css', 'shopping')) !!}
@endpush

@section('content')
<div class="full-content" id="favorites-content">
  @include('includes.favorites.modals')
  <div class="container">
  	<div class="main-header-container">
      <h3 class="main-header">{{$lang['title']}}</h3>
    </div>
  	<div class="row desktop">
  	  <div class="col-xs-12">
        <div class="table-responsive">
          <table class="table cart-table wishlist-table text-center">
            <thead>
              <tr>
                <th class="image"></th>
                <th class="name">{{$lang['product-name']}}</th>
                <th class="stock">{{$lang['stock-status']}}</th>
                <th class="price">{{$lang['price']}}</th>
                <th class="add-cart"></th>
                <th class="remove"></th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(item,index) in items.data" :key="item.id" :id="'div'+item.id">
                <td><a :href="'/product/' + item.slug" class="cart-pro-image"><img :src="item.image" alt="" /></a></td>
                <td><a :href="'/product/' + item.slug" class="cart-pro-title" v-cloak>@{{item.name}}</a></td>
                <td><p class="stock in-stock" v-cloak>@{{item.stock}}</p></td>
                <td><p class="cart-pro-price" v-cloak><span v-if="item.fee_included==1">@{{ parseFloat(item.price)+parseFloat(item.shipping_fee)+parseFloat(item.charge)+parseFloat(item.vat) | currency}}</span><span v-else>@{{ parseFloat(item.price) | currency}}</span></p></td>
                <td v-cloak><a href="javascript:void(0)" class="btn btn-raised btn-primary add-cart-btn" @click="addtocart(item.id, index)">{{$lang['add-cart']}}<div class="ripple-container"></div></a></td>
                <td v-cloak><a :id="'btn' + item.id" href="javascript:void(0)" @click="showRemoveDialog(item.id,index)"><i class="fa fa-trash-o cart-pro-remove" aria-hidden="true"></i></a></td>
                <input :id="'prod_id' + item.id" :name="'prod_id' + item.id" type="hidden" :value="item.product_id">
                <input :id="'qty' + item.id" :name="'qty'+item.id" type="hidden" value="1">     
              </tr>
            </tbody>
          </table>
        </div>
        <p class="no-item" style="display:none">{{$lang['no-item']}}</p>
  	  </div>
  	</div>
    <div class="col-md-12 mobile">
      <div class="row">
        <div>
          <div class="col-md-4 col-sm-6" v-for="(item,index) in items.data" :key="item.id" :id="'div'+item.id">
            <div class="card" v-cloak>
              <a :href="'/product/' + item.slug"><img class="card-img-top" :src="item.image" alt=""></a>
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a :href="'/product/' + item.slug" class="comp-name" v-cloak>@{{item.name}}</a></li>
                <li class="list-group-item" v-cloak><span v-if="item.fee_included==1">@{{ parseFloat(item.price)+parseFloat(item.shipping_fee)+parseFloat(item.charge)+parseFloat(item.vat) | currency}}</span><span v-else>@{{ parseFloat(item.price) | currency}}</span></li>     
                <li class="list-group-item margin1" v-cloak><span class="font">{{$lang['stock-status']}}:</span> @{{item.stock}}</li>
                <li class="list-group-item"><a href="javascript:void(0)" class="btn btn-raised btn-primary comp-btn" @click="addtocart(item.id, index)">{{$lang['add-cart']}}<div class="ripple-container"></div></a></li>
                <li class="list-group-item"><a href="javascript:void(0)" class="btn btn-raised btn-primary comp-btn" @click="showRemoveDialog(item.id, index)">{{$lang['remove']}}<div class="ripple-container"></div></a></li>
              <input :id="'prod_id' + item.id" :name="'prod_id' + item.id" type="hidden" :value="item.prod_id">
              <input :id="'qty' + item.id" :name="'qty'+item.id" type="hidden" value="1"> 
              </ul>       
            </div>
          </div>
        </div>
        <p class="no-item" style="display:none">{{$lang['no-item']}}</p>
      </div>
    </div>
  </div><!-- end  container-->
</div><!-- end  favorites-content-->
@endsection

@push('scripts')
    {!! Html::script(mix('js/pages/favorites.js', 'shopping')) !!}
@endpush
