<!--  FAQ PAGE -->
@extends('layouts.master')

@section('title', 'WATAF Help Center - Account')

@push('styles')
    {!! Html::style('shopping/css/pages/faq.css') !!}
@endpush

@section('content')
<div class="full-content" id="faq-container-content">
  <div class="container">
    <div class="main-header-container">
      <h3 class="main-header">WATAF HELP CENTER</h3>
    </div><!-- end  main-header-container-->

    <div class="row faq-container">
      <div class="col-sm-12">
        <div class="col-sm-2">
          <div class='card hidden-sm hidden-xs' id='subnav'>
          @include('includes.faq.faq-sidebar')
          </div>
        </div><!-- end col-sm-3-->
        <div class="mobile-menu-btn hidden-lg hidden-md">
          @include('includes.faq.modals')
        </div>
        <div class="col-sm-10">
          <div id="faq-account" v-show="param=='account'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="account">ACCOUNT</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='Account'"></faq-list>
                  </div>
              </div>
            </div>
          </div>

          <div id="faq-store" v-show="param=='store'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="shop">STORE</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='Store'"></faq-list>
                  </div>
              </div>
            </div>
          </div>

          <div id="faq-product" v-show="param=='product'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="shop">PRODUCT</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='Product'"></faq-list>
                  </div>
              </div>
            </div>
          </div>

          <div id="faq-order" v-show="param=='order'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="shop">ORDER</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='Order'"></faq-list>
                  </div>
              </div>
            </div>
          </div>

          <div id="faq-payment" v-show="param=='payment'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="shop">PAYMENT</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='Payment'"></faq-list>
                  </div>
              </div>
            </div>
          </div>

          <div id="faq-ewallet" v-show="param=='ewallet'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="shop">E-WALLET</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='E-wallet'"></faq-list>
                  </div>
              </div>
            </div>
          </div>

          <div id="faq-chat" v-show="param=='chat'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="shop">CHAT</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='Chat'"></faq-list>
                  </div>
              </div>
            </div>
          </div>


          <div id="faq-friends" v-show="param=='friends'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="shop">FRIENDS</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='Friends'"></faq-list>
                  </div>
              </div>
            </div>
          </div>


          <div id="faq-feedback" v-show="param=='feedback'" style="display:none">
            <div class="main-header-container">
                <h3 class="main-header" id="shop">FEEDBACK</h3>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="wrapper wrapper-content animated fadeInRight">
                    <faq-list v-for="item in items.data" :item="item" :key="item.id" v-if="item.type=='Feedback'"></faq-list>
                  </div>
              </div>
            </div>
          </div>

        </div><!-- end col-sm-9-->
      </div><!-- end col-sm-12-->
    </div><!-- end faq-container-->
  </div><!-- end container-->
</div><!-- end faq-content-->

@endsection

@push('scripts')
    {!! Html::script(mix('js/pages/faq.js','shopping')) !!}
@endpush