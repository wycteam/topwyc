
	<table class='table menu-holder'>
	    <tr class='btn-default btn-raised' onclick='window.location="/faq"'>
	        <td>
	            <div class="icon-wrapper"><i class="fa fa-home custom-icon bgc-deep-purple"><span class="fix-editor">&nbsp;</span></i></div>
	        </td>
	        <td>Home</td>                        
	    </tr>
	    <tr class='btn-default btn-raised' onclick='window.location="/faq/contents/?help=account"' :class="{'active': param=='account'}">
		    <td>
		        <div class="icon-wrapper"><i class="fa fa-user-o custom-icon bgc-indigo"><span class="fix-editor">&nbsp;</span></i></div>
		    </td>
		    <td>Account</td>        
	    </tr>
	    <tr class='btn-default btn-raised' onclick='window.location="/faq/contents/?help=store"' :class="{'active': param=='store'}">
	        <td>
	            <div class="icon-wrapper"><i class="fa fa-archive custom-icon bgc-blue"><span class="fix-editor">&nbsp;</span></i></div>
	        </td>
	        <td>Store</td>                        
	    </tr>
	    <tr class='btn-default btn-raised' onclick='window.location="/faq/contents/?help=product"' :class="{'active': param=='product'}">
	        <td>
	            <div class="icon-wrapper"><i class="fa fa-cubes custom-icon bgc-light-blue"><span class="fix-editor">&nbsp;</span></i></div>
	        </td>
	        <td>Product</td>                        
	    </tr>
	    <tr class='btn-default btn-raised' onclick='window.location="/faq/contents/?help=order"' :class="{'active': param=='order'}">
	        <td>
	            <div class="icon-wrapper"><i class="fa fa-shopping-cart custom-icon bgc-cyan"><span class="fix-editor">&nbsp;</span></i></div>
	        </td>
	        <td>Order</td>                        
	    </tr>
	    <tr class='btn-default btn-raised'>
	        <td>
	            <div class="icon-wrapper"><i class="fa fa-users custom-icon bgc-green"><span class="fix-editor">&nbsp;</span></i></div>
	        </td>
	        <td>Friends</td>                        
	    </tr>
	    <tr class='btn-default btn-raised' onclick='window.location="/faq/contents/?help=ewallet"' :class="{'active': param=='ewallet'}">
	        <td>
	            <div class="icon-wrapper"><i class="fa fa-credit-card custom-icon bgc-light-green"><span class="fix-editor">&nbsp;</span></i></div>
	        </td>
	        <td>E-Wallet</td>                        
	    </tr>
	    <tr class='btn-default btn-raised' onclick='window.location="/faq/contents/?help=chat"' :class="{'active': param=='chat'}">
	        <td>
	            <div class="icon-wrapper"><i class="fa fa-comments-o custom-icon bgc-lime"><span class="fix-editor">&nbsp;</span></i></div>
	        </td>
	        <td>Chat</td>                        
	    </tr>
	    <tr class='btn-default btn-raised'>
	        <td>
	            <div class="icon-wrapper"><i class="fa fa-commenting-o custom-icon bgc-yellow"><span class="fix-editor">&nbsp;</span></i></div>
	        </td>
	        <td>Feedback</td>                        
	    </tr>
	</table>
