<button data-toggle='modal' data-target='#faqModalMenu' 
@click='modalMenuShowing = !modalMenuShowing' class="btn btn-fab btn-raised btn-info modalBtn">
    <span class="fa" :class='modalMenuShowing?"fa-times":"fa-bars"'></span>
</button>
<modal size="modal-sm" id='faqModalMenu'>
    <template slot="modal-title">
        <h3>WATAF Help Center</h3>
    </template>
    <template slot="modal-body">
        <div class='container-fluid'>
            @include('includes.faq.faq-sidebar')
        </div>
    </template>
    <template slot="modal-footer"></template>
</modal>