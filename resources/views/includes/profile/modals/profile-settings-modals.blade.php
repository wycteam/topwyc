<!-- Select Shipping Address Modal -->
<modal size="modal-lg" id='changeShipAdd2'>
    <template slot="modal-title">
        <h3>
            Shipping Address
        </h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <table class="table table-striped table-hover ">
          <thead>
              <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Contact No.</th>
                <th>Default</th>
              </tr>
          </thead>

          <tbody>
              <tr>
                <td>Home</td>
                <td>7114 Kundiman Street, Sampaloc</td>
                <td>276-337-698</td>
                <td>
                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="1">
                </td>
              </tr>
              <tr>
                <td>Work</td>
                <td>75 P. Domingo Street, Carmona, Makati City</td>
                <td>215-676-412</td>    
                <td>
                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="2">
                </td>   
              </tr>
          </tbody>
        </table>
    </template><!-- end of modal-body -->
    <template slot="modal-footer">
        <a href="javascript:void(0)" @click="shipAddModal" name="addShipAdd" class="btn btn-raised btn-ship-add btn-tangerine">
            Add New Profile
            <div class="ripple-container"></div>
        </a>
    </template><!-- end of modal-footer -->
</modal>
<!-- end of Shipping Address Modal -->

<!-- Add Address Modal -->
<modal size="modal-md" id='addShipAdd2'>
    <template slot="modal-title">
        <h3>
            <button class='btn btn-default btn-fab btn-fab-mini' data-dismiss="modal" aria-label="Close">
                <span class='material-icons'>arrow_back</span>
            </button>
            Add New Profile
        </h3>
    </template><!-- end of modal-title -->
    <template slot="modal-body">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label" for="txt-ship-fn">Profile Name</label>
                    <input class="form-control" id="txt-ship-fn" type="text" required="">
                    <p class="help-block">Example: Office Address, Home Province</p>
                </div>
            </div><!-- end of col-sm-8 -->
        </div><!-- end of col-sm-12 -->

        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label" for="txt-ship-fn">Landline</label>
                    <input class="form-control phone_us" id="txt-ship-fn" type="text" v-model="addressInfo.landline">
                    <p class="help-block"></p>
            </div>
            </div><!-- end of col-sm-8 -->
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label" for="txt-ship-cn">Mobile</label>
                    <input class="form-control cell" id="txt-ship-cn" type="text" required="" v-model="addressInfo.mobile">
                    <p class="help-block"></p>
            </div>
            </div><!-- end of col-sm-2 -->
        </div><!-- end of col-sm-12 -->

        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label" for="txt-ship-ca">Complete Address</label>
                    <input class="form-control" id="txt-ship-ca" type="text" required="" v-model="addressInfo.address">
                    <p class="help-block"></p>
            </div>
            </div><!-- end of col-sm-12 -->
        </div><!-- end of col-sm-12 -->

        <div class="col-sm-12">
            <div class="col-sm-6">
                <multiselect class="custom-select" :options="optionProv" placeholder="Province" label="name" track-by="id" @input="getCity" v-model="addressInfo.province"></multiselect>
            </div><!-- end of col-sm-6-->

            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label" for="txt-ship-ba">Barangay</label>
                    <input class="form-control" id="txt-ship-ba" type="text" required="" v-model="addressInfo.barangay">
                    <p class="help-block"></p>
            </div>
            </div><!-- end of col-sm-6-->
        </div><!-- end of col-sm-12 -->

        <div class="col-sm-12">
            <div class="col-sm-6">
               <multiselect class="custom-select" :options="optionCity" placeholder="City" label="name" track-by="id" v-model="addressInfo.city"></multiselect>
            </div><!-- end of col-sm-6-->

            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label" for="txt-ship-zc">Zip Code</label>
                    <input class="form-control" id="txt-ship-zc" type="text" v-model="addressInfo.zip_code">
                    <p class="help-block"></p>
                </div>
            </div><!-- end of col-sm-6 -->
        </div><!-- end of col-sm-12 -->

        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label" for="txt-ship-nl">Nearest Landmark</label>
                    <input class="
                    form-control" id="txt-ship-nl" type="text" v-model="addressInfo.landmark">
                    <p class="help-block"></p>
            </div>
            </div><!-- end of col-sm-12 -->
        </div><!-- end of col-sm-12 -->

        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="form-group label-floating">
                    <label class="control-label" for="txt-ship-notes">Notes</label>
                    <textarea class="form-control" rows="3" id="txt-ship-notes" v-model="addressInfo.remarks"></textarea>
                    <span class="help-block"></span>
                </div>
            </div><!-- end of col-sm-12 -->
        </div><!-- end of col-sm-12 -->
        
    </template><!-- end of modal-body -->
    <template slot="modal-footer">
        <a href="javascript:void(0)" class="btn btn-raised btn-ship-add btn-tangerine" @click="saveNewAddress">
            Save
            <div class="ripple-container"></div>
        </a>
    </template><!-- end of modal-footer -->
</modal>
<!-- end of Add Address Modal -->