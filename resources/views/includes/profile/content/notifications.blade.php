
<div class="row">
	<div class="col-lg-12">
		<h4 class="title m-t-3 profile-title">{{$lang['noti']}}</h4>
	</div>
	<div class=" col-lg-12 account-input-set">
		<ul class='notification-list'>
			<li v-if='!notifications'>
				<div class="col-lg-12 text-center">
					{{$lang['no-noti']}}
				</div>
			</li>
			<notif-container v-for='notif in notifications' :notif='notif' :key='notif.id'></notif-container>
					
		</ul>
	</div>
</div>