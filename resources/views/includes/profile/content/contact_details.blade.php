 <!-- PROFILE SETTINGS > CONTACT DETAILS-->

<div class="row">
	<div class="col-lg-12">
		<h4 class="title m-t-3 profile-title">{{$lang['contact-details']}}</h4>
	</div>
	
	<div class="col-lg-12">
		<div class="col-lg-2 account-input-right">
            <div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('zip_code') }">
                <label class="control-label" for="txt-ship-zc">{{$lang['zip-code']}}</label>
                <input 
                	class="form-control" 
                	id="txt-ship-zc" 
                	type="text" 
                	v-model="homeAddressInfo.zip_code" 
                 	@keyup='homeAddressInfo.zip_code = $event.target.value'
                	required="">
                <p class="help-block" v-if="homeAddressInfo.errors.has('zip_code')" v-text="homeAddressInfo.errors.get('zip_code')"></p>
        	</div>
	    </div><!-- end of col-lg-6 -->

		<div class="col-lg-10 account-input-set">
			<div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('address') }">
				<label class="control-label" for="txt-home-address">{{$lang['com-add']}}</label>
				<input v-model="homeAddressInfo.address" class="form-control" id="txt-home-address" type="text" name="full_address" required="">
				<p class="help-block" v-if="homeAddressInfo.errors.has('address')" v-text="homeAddressInfo.errors.get('address')"></p>
			</div>
		</div><!-- end account-input-set -->
	</div>

	<div class="col-lg-12 account-input-set">
		<div class="col-lg-12">
			<div class="col-lg-6 account-input-left">
				<div class="form-group" :class="{ 'has-error': homeAddressInfo.errors.has('province') }">
					<multiselect class="custom-select" 
		            :options="optionProv" 
		            placeholder="{{$lang['province']}}" 
		            @input="getCity" 
		            v-model="homeAddressInfo.province"
		            :selected="homeAddressInfo.province"
		            deselect-label="{{$lang['field-required']}}"
		            :allow-empty="false"></multiselect>
		            <p class="help-block" v-if="homeAddressInfo.errors.has('province')" v-text="homeAddressInfo.errors.get('province')"></p>
				</div>
	        </div><!-- end of col-lg-6 -->

			<div class="col-lg-6 account-input-left">
				<div class="form-group" :class="{ 'has-error': homeAddressInfo.errors.has('city') }">
					<multiselect class="custom-select" 
			        :options="optionCity" 
			        :allow-empty="false"
			        :selected="homeAddressInfo.city"
			        placeholder="{{$lang['city']}}"  
			        label="name"
			        track-by="name"
			        v-model="city"
			       	deselect-label="{{$lang['field-required']}}"
			        ></multiselect>		
			        <p class="help-block" v-if="homeAddressInfo.errors.has('city')" v-text="homeAddressInfo.errors.get('city')"></p>		
				</div>
	        </div><!-- end of col-lg-6 -->

<!-- 	        <div class="col-lg-6 account-input-right">
	            <div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('barangay') }">
	                <label class="control-label" for="txt-home-barangay">{{$lang['brgy']}}</label>
	                <input class="form-control" id="txt-home-barangay" type="text" required="" v-model="homeAddressInfo.barangay">
	                <p class="help-block" v-if="homeAddressInfo.errors.has('barangay')" v-text="homeAddressInfo.errors.get('barangay')"></p>
	        	</div>
	        </div> -->
	        <!-- end of col-lg-6 -->
		</div>
	</div><!-- end account-input-set -->

	<div class="col-lg-12 account-input-set">
		<div class="col-lg-12">
			
		</div>
	</div><!-- end account-input-set -->

	<div class=" col-lg-12 account-input-set">
			<div class="col-lg-6 account-input-set">
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group label-floating">
						    <label class="control-label" for="txt-home-landline">{{$lang['area-code']}}</label>
						     <input 
			                	class="form-control"
			                 	id="txt-home-area-code"
			                 	type="text" 
			                 	name="area_code"
			                 	v-model="homeAddressInfo.area_code" 
			                 	@keyup='homeAddressInfo.area_code = $event.target.value'
			        			>
			        			<p class="help-block">Please put a '0' in the beginning if your Area Code has 4-digits only.</p>
						  </div>
					</div>
					<div class="col-lg-8">
						<div class="form-group label-floating">
						    <label class="control-label" for="txt-home-landline">{{$lang['landline']}}</label>
						     <input 
			                	class="form-control"
			                 	id="txt-home-landline"
			                 	type="text" 
			                 	name="landline"
			                 	v-model="homeAddressInfo.landline" 
			                 	@keyup='homeAddressInfo.landline = $event.target.value'
			                 	>
						  </div>
					</div>
				</div>
		         
	        </div><!-- end of col-lg-6 -->


	        <div class="col-lg-6 account-input-set">
	            <div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('mobile') }">
	                <label class="control-label" for="txt-home-mobile">{{$lang['mobile']}}</label>
	                <input 
	                	class="form-control" 
	                	id="txt-home-mobile" 
	                	type="text" 
	                	name="mobile" 
	                	v-model="homeAddressInfo.mobile" 
	                 	@keyup='homeAddressInfo.mobile = $event.target.value'
	                	required="">
	                <p class="help-block" v-if="homeAddressInfo.errors.has('mobile')" v-text="homeAddressInfo.errors.get('mobile')"></p>
	            </div>
	        </div><!-- end of col-lg-6 -->
	</div><!-- end account-input-set -->

	<div class="col-lg-12">
		<div class="col-lg-12 account-input-set">
			<div class="form-group label-floating">
	            <label class="control-label" for="txt-home-landmark">{{$lang['landmark']}}</label>
	            <input class="form-control" id="txt-home-landmark" type="text" name="landmark" v-model="homeAddressInfo.landmark">
	        </div>
		</div><!-- end account-input-set -->
	</div>
	<div class="col-lg-12 account-input-set">
		<div class="col-lg-12 text-right">
			<button @click="saveHomeAddress" id ="acct_save_password" class="btn btn-raised btn-tangerine">{{$lang['save']}}</button>
		</div>
	</div><!-- end account-input-set -->
</div><!-- end col-lg-9 -->