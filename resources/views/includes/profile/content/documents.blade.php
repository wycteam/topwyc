<!-- viewing Modal -->
<modal size="modal-lg" id='modalDocsView'>
    <template slot="modal-title">
        <h3 v-text='modalDocTitle'></h3>
    </template>
    <template slot="modal-body">
       <!--  <div class="tiles">
	        <div data-scale="2"  :data-image="modalDocImage" class="tile">
	        	<div class="photo" :style='"background-image: url("+modalDocImage+");"'>
	        		
	        	</div>
	        </div>
  		</div> -->
  		<div id='doc-container' >
  		</div>
  		<div class="range-slider">
		  <input class="range-slider__range" type="range"  step='0.1' v-model='baseZoom' min="1" max="5">
		</div>
    </template>
    <template slot="modal-footer">
    </template>
</modal>


<div class="row">
	<div class="col-lg-12">
		<h4 class="title m-t-3 profile-title">{{$lang['docs']}}</h4>
	</div>


	<div class="col-lg-12" v-if="!user.is_docs_verified">
		<div class="alert alert-danger" role="alert">
	      <b>{{$lang['acct-not-ver-1']}}</b>
	      <br>{{$lang['acct-not-ver-2']}}
	      <br>{{$lang['acct-not-ver-3']}}
	      <br>{{$lang['acct-not-ver-4']}}
	      <br><b>{{$lang['acct-not-ver-7']}}</b>
	    </div>
	</div>
	<div class="col-lg-12" v-else>
		<div class="alert alert-danger" role="alert">
	      <b>{{$lang['acct-not-ver-7']}}</b>
	    </div>
	</div>	

	<!-- UPLOAD DOCUMENTS -->
	<div class="col-sm-12 p-b-2">
		<button class='btn btn-xs btn-raised btn-info col-lg-12' data-toggle="collapse" data-target="#individual">
			<h5>{{$lang['ind-acct']}}</h5>
		</button>
		<div id="individual" class="collapse in">
			<div class="col-sm-12 document-set list-group-item">
				<div class="col-sm-7">
					<span><span  class="fa fa-asterisk"></span>  {{$lang['nbi']}}</span>
					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="{{$lang['upload-nbi-tp']}}">
						<span class="fa fa-question-circle-o icon-color" ></span>
					</span>
					<span class="label"  :class='labelColor(nbi_status)'  data-toggle='tooltip' v-if="nbi_status == 'Pending'">{{$lang['pending']}}</span>
					<span class="label"  :class='labelColor(nbi_status)'  data-toggle='tooltip' v-if="nbi_status == 'Verified'">{{$lang['verified']}}</span>
					<span class="label"  :class='labelColor(nbi_status)'  data-toggle='tooltip' title="{{$nbi_reason}}" v-if="nbi_status == 'Denied'">{{$lang['denied']}}</span>
					<br>
					<span class='label label-info' v-if='nbi_status == "Verified"' v-text='lang2.data["exp-at"]+" "+nbi_expiry'></span>
					<span class='label label-danger' v-if='nbi_status == "Expired"' v-text='lang2.data["exp-on"]+" "+nbi_expiry'></span>
				</div><!-- end col-sm-6 -->
				<div class="col-sm-5 doc-btn-set">		    				
                    <button v-cloak class="btnNbiUpload btn btn-tangerine btn-has-loading btn-raised btn-xs" :disabled='lockUploadBtn(nbi_status,nbi_expiry)'>             
                    	<span class="hidden-xs">{{$lang['upload']}}</span>
						<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
                	</button>

              		<div class="nbi-uploader hide">
	              		<imgfileupload data-event="nbiUpload" data-url="/profile/upload-docs?width=400&height=400&type=nbi"></imgfileupload>
              		</div>

              		<div class="doc-action-btns" v-if="docNbi">
              			<button
						id 			= 'view_Nbi'
						class 		=  'btn btn-xs'
						:class 		=  'docNbi?"btn-success":""'
						@click 		= "openDocModal('nbi',nbi_file)"
						:disabled	=  '!docNbi'>
						<span class="hidden-xs">{{$lang['view']}}</span>
						<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-eye" aria-hidden="true"></i></span>
					</button>

					<a 	id			='download_nbi'
						:href		='"/images/documents/{{$user->id}}/" + nbi_file '
						class		='btn btn-xs'
						:class		='docNbi?"btn-success":""'
						:disabled	='!docNbi'
						download	=''>
						<span class="hidden-xs">{{$lang['download']}}</span>
						<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span>
					
				</a>
              		</div>
					
				</div><!-- end col-sm-6 -->
			</div><!-- end document-set -->
			<!-- <div class="col-sm-12 document-set list-group-item">
				<div class="col-sm-7">
					<span><span  class="fa fa-asterisk"></span> {{$lang['birth-cert']}}</span>
					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="{{$lang['upload-bc-tp']}}"><span class="fa fa-question-circle-o icon-color" ></span></span>
					
					<span class="label"  :class='labelColor(birthCert_status)'  data-toggle='tooltip' v-if="birthCert_status == 'Pending'">{{$lang['pending']}}</span>
					<span class="label"  :class='labelColor(birthCert_status)'  data-toggle='tooltip' v-if="birthCert_status == 'Verified'">{{$lang['verified']}}</span>
					<span class="label"  :class='labelColor(birthCert_status)'  data-toggle='tooltip' title="{{$birthCert_reason}}" v-if="birthCert_status == 'Denied'">{{$lang['denied']}}</span>
					<br>
			
					<span class='label label-info' v-if='birthCert_status == "Verified"' v-text='lang2.data["exp-at"]+" "+birthCert_expiry'></span>
				</div>
			
				<div class="col-sm-5 doc-btn-set">
					<button v-cloak class="btnBirthCertUpload btn-has-loading btn btn-tangerine btn-raised btn-xs"   :disabled='lockUploadBtn(birthCert_status,birthCert_expiry)'>
						{{$lang['upload']}}
					</button>
			              		<div class="birthCert-uploader hide">
				              		<imgfileupload data-event="birthCertUpload" data-url="/profile/upload-docs?width=400&height=400&type=birthCert"></imgfileupload>
			              		</div>
			
			              		<div class="doc-action-btns" v-if="docBirthCert">
			              			<button
						id 			= 'view_BirthCert'
						class 		=  'btn btn-xs'
						:class 		=  'docBirthCert?"btn-success":""'
						@click 		= "openDocModal('birthCert',birthCert_file)"
						:disabled	=  '!docBirthCert'>{{$lang['view']}}</button>
						
					<a 	id			='download_BirthCert'
						:href		='"/images/documents/{{$user->id}}/" + birthCert_file '
						class		='btn btn-xs'
						:class		='docBirthCert?"btn-success":""'
						:disabled	='!docBirthCert'
						download	=''>{{$lang['download']}}</a>
			              		</div>
			
				</div>
			</div> -->
			<!-- end document-set -->

			<div class="col-sm-12 document-set list-group-item">
				<div class="col-sm-7">
					<span><span  class="fa fa-asterisk"></span> {{$lang['gov-id']}}</span>
					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="{{$lang['upload-govid-tp']}}"><span class="fa fa-question-circle-o icon-color" ></span></span>

					<span class="label"  :class='labelColor(govID_status)'  data-toggle='tooltip' v-if="govID_status == 'Pending'">{{$lang['pending']}}</span>
					<span class="label"  :class='labelColor(govID_status)'  data-toggle='tooltip' v-if="govID_status == 'Verified'">{{$lang['verified']}}</span>
					<span class="label"  :class='labelColor(govID_status)'  data-toggle='tooltip' title="{{$govID_reason}}" v-if="govID_status == 'Denied'">{{$lang['denied']}}</span>

					<br>
					<span class='label label-info' v-if='govID_status == "Verified"' v-text='lang2.data["exp-at"]+" "+govID_expiry'></span>
					<span class='label label-danger' v-if='govID_status == "Expired"' v-text='lang2.data["exp-on"]+" "+govID_expiry'></span>
					
				</div><!-- end col-sm-6 -->
				<div class="col-sm-5 doc-btn-set">

					<button v-cloak class="btnGovIdUpload btn-has-loading btn btn-tangerine btn-raised btn-xs"  :disabled='lockUploadBtn(govID_status,govID_expiry)'>
						<span class="hidden-xs">{{$lang['upload']}}</span>
						<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-upload" aria-hidden="true"></i></span>
					</button>
              		<div class="govID-uploader hide">
	              		<imgfileupload data-event="govIdUpload" data-url="/profile/upload-docs?width=400&height=400&type=govId"></imgfileupload>
              		</div>

              		<div class="doc-action-btns" v-if="docGovId">
              			<button
						id 			= 'view_GovId'
						class 		=  'btn btn-xs'
						:class 		=  'docGovId?"btn-success":""'
						@click 		= "openDocModal('govId',govID_file)"
						:disabled	=  '!docGovId'>
							<span class="hidden-xs">{{$lang['view']}}</span>
							<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-eye" aria-hidden="true"></i></span>
						</button>
						
					<a 	id			='download_GovId'
						:href		='"/images/documents/{{$user->id}}/" + govID_file '
						class		='btn btn-xs'
						:class		='docGovId?"btn-success":""'
						:disabled	='!docGovId'
						download	=''><span class="hidden-xs">{{$lang['download']}}</span>
						<span class="hidden-sm hidden-md hidden-lg"><i class="fa fa-download" aria-hidden="true"></i></span></a>
              		</div>

				</div><!-- end col-sm-6 -->
			</div><!-- end document-set -->
		</div>
		<!-- enterprise documents -->
		<!-- <button class='btn btn-xs btn-raised btn-info col-lg-12' data-toggle="collapse" data-target="#enterprise">
			<h5>{{$lang['ent-acct']}}</h5>
		</button>
		<div id="enterprise" class="collapse in ">
			<div class="col-sm-12 document-set list-group-item">
				<div class="col-sm-7">
					<span><span  class="fa fa-asterisk"></span> {{$lang['sec']}}</span>
					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="{{$lang['upload-sec-tp']}}"><span class="fa fa-question-circle-o icon-color" ></span></span>
				
					<span class="label"  :class='labelColor(sec_status)'  data-toggle='tooltip' v-if="sec_status == 'Pending'">{{$lang['pending']}}</span>
					<span class="label"  :class='labelColor(sec_status)'  data-toggle='tooltip' v-if="sec_status == 'Verified'">{{$lang['verified']}}</span>
					<span class="label"  :class='labelColor(sec_status)'  data-toggle='tooltip' title="{{$sec_reason}}" v-if="sec_status == 'Denied'">{{$lang['denied']}}</span>

					<br>
					<span class='label label-info' v-if='sec_status == "Verified"' v-text='lang2.data["exp-at"]+" "+sec_expiry'></span>
				</div>
				<div class="col-sm-5 doc-btn-set">
					<button v-cloak class="btnSecUpload btn-has-loading btn btn-tangerine btn-raised btn-xs" :disabled='lockUploadBtn(sec_status,sec_expiry)'>
						{{$lang['upload']}}
					</button>
              		<div class="sec-uploader hide">
	              		<imgfileupload data-event="secUpload" data-url="/profile/upload-docs?width=400&height=400&type=sec"></imgfileupload>
              		</div>
					<button
						id 			= 'view_Sec'
						class 		=  'btn btn-xs'
						:class 		=  'docSec?"btn-success":""'
						@click 		= "openDocModal('sec',sec_file)"
						:disabled	=  '!docSec'>{{$lang['view']}}</button>
						
					<a 	id			='download_Sec'
						:href		='"/images/documents/{{$user->id}}/" + sec_file '
						class		='btn btn-xs'
						:class		='docSec?"btn-success":""'
						:disabled	='!docSec'
						download	=''>{{$lang['download']}}</a>
				</div>
			</div>
			<div class="col-sm-12 document-set list-group-item">
				<div class="col-sm-7">
					<span><span  class="fa fa-asterisk"></span> {{$lang['bir']}}</span>
					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="{{$lang['upload-bir-tp']}}"><span class="fa fa-question-circle-o icon-color" ></span></span>

					<span class="label"  :class='labelColor(bir_status)'  data-toggle='tooltip' v-if="bir_status == 'Pending'">{{$lang['pending']}}</span>
					<span class="label"  :class='labelColor(bir_status)'  data-toggle='tooltip' v-if="bir_status == 'Verified'">{{$lang['verified']}}</span>
					<span class="label"  :class='labelColor(bir_status)'  data-toggle='tooltip' title="{{$bir_reason}}" v-if="bir_status == 'Denied'">{{$lang['denied']}}</span>

					<br>
					<span class='label label-info' v-if='bir_status == "Verified"' v-text='lang2.data["exp-at"]+" "+bir_expiry'></span>
				</div>
				<div class="col-sm-5 doc-btn-set">
					<button v-cloak class="btnBirUpload btn-has-loading btn btn-tangerine btn-raised btn-xs" :disabled='lockUploadBtn(bir_status,bir_expiry)'>
						{{$lang['upload']}}
					</button>
              		<div class="bir-uploader hide">
	              		<imgfileupload data-event="birUpload" data-url="/profile/upload-docs?width=400&height=400&type=bir"></imgfileupload>
              		</div>
					<button
						id 			= 'view_Bir'
						class 		=  'btn btn-xs'
						:class 		=  'docBir?"btn-success":""'
						@click 		= "openDocModal('bir',bir_file)"
						:disabled	=  '!docBir'>{{$lang['view']}}</button>
						
					<a 	id			='download_Bir'
						:href		='"/images/documents/{{$user->id}}/" + bir_file '
						class		='btn btn-xs'
						:class		='docBir?"btn-success":""'
						:disabled	='!docBir'
						download	=''>{{$lang['download']}}</a>
				</div>
			</div>
			<div class="col-sm-12 document-set list-group-item">
				<div class="col-sm-7">
					<span><span  class="fa fa-asterisk"></span>{{$lang['bp']}}</span>
					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="{{$lang['upload-bp-tp']}}"><span class="fa fa-question-circle-o icon-color" ></span></span>

					<span class="label"  :class='labelColor(permit_status)'  data-toggle='tooltip' v-if="permit_status == 'Pending'">{{$lang['pending']}}</span>
					<span class="label"  :class='labelColor(permit_status)'  data-toggle='tooltip' v-if="permit_status == 'Verified'">{{$lang['verified']}}</span>
					<span class="label"  :class='labelColor(permit_status)'  data-toggle='tooltip' title="{{$permit_reason}}" v-if="permit_status == 'Denied'">{{$lang['denied']}}</span>
					<br>
					<span class='label label-info' v-if='permit_status == "Verified"' v-text='lang2.data["exp-at"]+" "+permit_expiry'></span>
				</div>
				<div class="col-sm-5 doc-btn-set">
					<button v-cloak class="btnPermitUpload btn-has-loading btn btn-tangerine btn-raised btn-xs" :disabled='lockUploadBtn(permit_status,permit_expiry)'>
						{{$lang['upload']}}
					</button>
              		<div class="permit-uploader hide">
	              		<imgfileupload data-event="permitUpload" data-url="/profile/upload-docs?width=400&height=400&type=permit"></imgfileupload>
              		</div>
					<button
						id 			= 'view_Permit'
						class 		=  'btn btn-xs'
						:class 		=  'docPermit?"btn-success":""'
						@click 		= "openDocModal('permit',permit_file)"
						:disabled	=  '!docPermit'>{{$lang['view']}}</button>
						
					<a 	id			='download_Permit'
						:href		='"/images/documents/{{$user->id}}/" + permit_file '
						class		='btn btn-xs'
						:class		='docPermit?"btn-success":""'
						:disabled	='!docPermit'
						download	=''>{{$lang['download']}}</a>
				</div>
			</div>
		</div> -->
	</div>	
</div>