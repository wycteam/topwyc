<!--  PROFILE SETTINGS > SECURITY SETTINGS PANEL -->

<div class="row p-b-2">
	<div class="col-lg-12">
		<h4 class="title m-t-3 profile-title">{{$lang['sec-settings']}}</h4>
	</div>

	<!-- 
		COLOR CODING
		** LOW > RED
		** MEDIUM > YELLOW
		** HIGH > GREEN

	 -->
	<div class="col-lg-12">
		<div id="reminder-context" class="col-sm-12 security-settings-set bg-danger" v-if='securityLevel.indexOf("low")>-1'>
			<span class="reminder-low"></span>
			<h5 class="msg-context">
				<!-- CHANGE ICON & COLOR: change class 'reminder-high' to 'reminder-low' & 'reminder-med'-->
				{{$lang['sec-doc-msg-1']}}{{$lang['sec-doc-msg-2']}}{{$lang['sec-doc-msg-3']}}<a href='/profile?v=documents'>{{$lang['sec-doc-msg-4']}}</a>{{$lang['sec-doc-msg-5']}}
			</h5>
		</div><!-- end reminder-context -->
		<div id="reminder-context" class="col-sm-12 security-settings-set bg-warning"  v-if='securityLevel.indexOf("med")>-1'>
			<span class="reminder-med"></span>
			<h5 class="msg-context">
				<!-- CHANGE ICON & COLOR: change class 'reminder-high' to 'reminder-low' & 'reminder-med'-->
				{{$lang['sec-doc-msg-1']}}
				<span v-if='!securityAnswers.length'>{{$lang['sec-doc-msg-2']}}</span>
				<a href='/profile?v=documents' v-else>{{$lang['sec-doc-msg-4']}}  </a>{{$lang['sec-doc-msg-5']}}
			</h5>
		</div><!-- end reminder-context -->
		<div id="reminder-context" class="col-sm-12 security-settings-set bg-success"  v-if='securityLevel.indexOf("high")>-1'>
			<span class="reminder-high"></span>
			<h5 class="msg-context">
				<!-- CHANGE ICON & COLOR: change class 'reminder-high' to 'reminder-low' & 'reminder-med'-->
				{{$lang['acct-sec-msg']}}
			</h5>
		</div><!-- end reminder-context -->
		
	</div>
	<!-- REMINDER CONTEXT-->
	<!-- CHANGE COLOR: change class 'bg-success' to 'bg-danger' & 'bg-warning' to manipulate contextual backgrounds -->

	<!-- PROGRESS BAR -->
	<div class="col-sm-12 security-settings-set progress-bar-set">
		<div class="col-sm-2 prof-label">
            <span>{{$lang['sec-lvl']}}</span>
        </div><!-- end col-sm-2 -->

        <div class="col-sm-9 prof-label">
            <div class="col-xs-12 progressbar-main ">
                <div class="progress progressbar-body">
                	<!-- CHANGE LEVEL: change class 'high' to 'low' & 'med' to manipulate progressbar level -->
                	<!-- CHANGE COLOR: change class 'progress-bar-success' to 'progress-bar-warning' & 'progress-bar-danger' to manipulate progressbar color 
					progress-bar-success HIGH
                	-->
                	<div class="progress-bar progressbar-color " :class='securityLevel'></div>
                </div><!-- end progressbar-body -->
            </div><!-- end progressbar-main -->
        </div><!-- end col-sm-2 -->

        <div class="col-sm-1 prof-label">
 			<span v-cloak v-if="securityLevel.split(' ')[1] == 'low'" class="profile-title">{{$lang['low']}}</span>
 			<span v-cloak v-if="securityLevel.split(' ')[1] == 'med'" class="profile-title">{{$lang['med']}}</span>
 			<span v-cloak v-if="securityLevel.split(' ')[1] == 'high'" class="profile-title">{{$lang['high']}}</span>
        </div><!-- end col-sm-2 -->
	</div><!-- end account-input-set -->

	<!-- CHANGE PASSWORD -->
	<div class="col-sm-12 security-settings-set m-b-2">
		@include('includes.profile.settings.changePassword')
	</div><!-- end security-settings-set -->

	<!-- SECURITY QUESTION -->
	<div class="col-sm-12 security-settings-set">
		@include('includes.profile.settings.securityQuestion')
	</div>

	
</div>