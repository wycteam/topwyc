 <!-- PROFILE SETTINGS > ACCOUNT INFORMATION PANEL > DATA -->

<div class="row">
<!-- {!! Form::open()!!} -->
	<div class="col-lg-12">
		<h4 class="title m-t-3 profile-title">{{$lang['acct-info']}}</h4>
	</div>
	<div class="col-lg-12 account-input-set">
		<div class="col-lg-12">
			<div class="form-group label-floating">
				<label class="control-label" for="txt-email">{{$lang['email']}}</label>
				<input v-model="user.email" class="form-control" id="txt-email" type="text" readonly="" name="email" >
				<p class="help-block"
				>{{$lang['email-msg']}}</p>
			</div>
		</div><!-- end label-floating -->
	</div><!-- end account-input-set -->

	<div class="col-lg-12 account-input-set">
		<div class="col-lg-12">
			<div class="form-group label-floating" :class="{ 'has-error': accInfo.errors.has('secondary_email') }">
				<label class="control-label" for="txt-sec-email">{{$lang['sec-email']}}</label>
				<input class="form-control" id="txt-sec-email" type="text" v-model="accInfo.secondary_email" name="secondary_email">
				<p class="help-block" v-if="accInfo.errors.has('secondary_email')" v-text="accInfo.errors.get('secondary_email')"></p>
			</div>
		</div><!-- end label-floating -->
	</div><!-- end account-input-set -->

	<div class="col-lg-12 account-input-set">
		<div class="col-lg-6">
			<div class="form-group label-floating" :class="{ 'has-error': accInfo.errors.has('birth_date') }">
				<label class="control-label" for="txt-bday">{{$lang['bday']}}</label>
				<input v-model="accInfo.birth_date" v-datepicker class="form-control" id="birth_date" type="text" required name="birth_date">
				<p class="help-block" v-if="accInfo.errors.has('birth_date')" v-text="accInfo.errors.get('birth_date')"></p>
			</div><!-- end label-floating -->
		</div>
	
		<div class="col-lg-6">
			<multiselect
			  v-model="accInfo.gender"
			  :options="optionGender"
			  deselect-label="This field is required."
			  :allow-empty="false">
			</multiselect>
		</div>
	</div><!-- end account-input-set -->

	<div class="col-lg-12 account-input-set">
		<div class="col-lg-12">
			<div class="form-group label-floating">
			  <label class="control-label" for="txt-nickname">{{$lang['nickname']}}</label>
			  <input class="form-control" id="txt-nickname" type="text" v-model="accInfo.nick_name" name="nick_name">
	<!-- 		  <p class="help-block">You should really write something here</p>
	 -->	</div><!-- end label-floating -->	
		</div>
	</div><!-- end account-input-set -->

	<div class="col-lg-12 account-input-set">
		<div class="col-lg-12">
			<div class="checkbox">
	          <label>
	            <input type="checkbox" 
	              id="chk_display"
	              v-model="accInfo.is_display_name"
	              :disabled="accInfo.nick_name == null || accInfo.nick_name == ''"> 
	              {{$lang['set-nickname']}}
	          </label>
	        </div>
		</div>
	</div><!-- end account-input-set -->

	<div class="col-lg-12 account-input-set">
		<div class="col-lg-12 text-right">
			<button  @click="saveInfo" id ="acct_save_password" class="btn btn-raised btn-tangerine">{{$lang['save']}}</button>
		</div>
	</div><!-- end account-input-set -->

</div><!-- end col-lg-12 -->
