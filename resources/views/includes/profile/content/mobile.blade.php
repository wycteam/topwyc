<div class="mobile-menu-btn hidden-lg hidden-md">
    <button data-toggle='modal' data-target='#modalMenu' @click='modalMenuShowing = !modalMenuShowing' class="btn btn-fab btn-raised btn-info">
        <span class="fa" :class='modalMenuShowing?"fa-times":"fa-bars"'></span>
    </button>
</div>
<modal size="modal-sm" id='modalMenu'>
    <template slot="modal-title">
        <h3>{{$lang['menu']}}</h3>
    </template>
    <template slot="modal-body">
        <div class='container-fluid'>
            @include('includes.profile.content.menu')
        </div>
    </template>
    <template slot="modal-footer">
        
    </template>
</modal>