<table class='table menu-holder'>
    <tr class='btn-default btn-raised' data-toggle="tab" onclick='window.location="/profile/?v=account"' :class="{'active': param=='account'}">
        <td>
            <div class="icon-wrapper"><i class="fa fa-user-o custom-icon bgc-deep-purple"><span class="fix-editor">&nbsp;</span></i></div>
        </td>
        <td>{{$lang['acct-info']}}</td>                        
    </tr>
    <tr class='btn-default btn-raised' data-toggle="tab" onclick='window.location="/profile/?v=contact"' :class="{'active': param=='contact'}">
        <td>
            <div class="icon-wrapper"><i class="fa fa-phone custom-icon bgc-indigo"><span class="fix-editor">&nbsp;</span></i></div>
        </td>
        <td>{{$lang['contact-details']}}</td>                        
    </tr>
    <tr class='btn-default btn-raised' data-toggle="tab" onclick='window.location="/profile/?v=security"' :class="{'active': param=='security'}">
        <td>
            <div class="icon-wrapper"><i class="fa fa-shield custom-icon bgc-blue"><span class="fix-editor">&nbsp;</span></i></div>
        </td>
        <td>{{$lang['sec-settings']}}</td>                        
    </tr>
    <tr class='btn-default btn-raised' data-toggle="tab" onclick='window.location="/profile/?v=documents"' :class="{'active': param=='documents'}">
        <td>
            <div class="icon-wrapper"><i class="fa fa-book custom-icon bgc-light-blue"><span class="fix-editor">&nbsp;</span></i></div>
        </td>
        <td>{{$lang['docs']}}</td>                        
    </tr>
    <tr class='btn-default btn-raised' onclick='window.location="/stores"' @click='modalMenuShowing = false'>
        <td>
            <div class="icon-wrapper"><i class="fa fa-archive custom-icon bgc-cyan"><span class="fix-editor">&nbsp;</span></i></div>
        </td>
        <td>{{$lang['store-mgmt']}}</td>                        
    </tr>
    <tr class='btn-default btn-raised' onclick='window.location="/user/purchase-report"' @click='modalMenuShowing = false'>
        <td>
            <div class="icon-wrapper"><i class="fa fa-shopping-cart custom-icon bgc-green"><span class="fix-editor">&nbsp;</span></i></div>
        </td>
        <td>{{$lang['my-purchase']}}</td>                        
    </tr>
    <tr class='btn-default btn-raised' onclick='window.location="/user/sales-report"' @click='modalMenuShowing = false'>
        <td>
            <div class="icon-wrapper"><i class="fa fa-list custom-icon bgc-light-green"><span class="fix-editor">&nbsp;</span></i></div>
        </td>
        <td>{{$lang['my-sales']}}</td>                        
    </tr>
    <tr class='btn-default btn-raised' data-toggle="tab" onclick='window.location="/profile/?v=notifications"' :class="{'active': param=='notifications'}">
        <td>
            <div class="icon-wrapper"><i class="fa fa-asterisk custom-icon bgc-lime"><span class="fix-editor">&nbsp;</span></i></div>
        </td>
        <td>
            {{$lang['noti']}}
        </td>                        
    </tr>
</table>