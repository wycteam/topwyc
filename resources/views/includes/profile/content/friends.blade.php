<div class='container-fluid'>
   <div class="col-lg-12" id='friends_holder'>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#requests-content">Requests</a></li>
            <li><a data-toggle="tab" href="#friends-content">Friends</a></li>
            <li><a data-toggle="tab" href="#find-people-content">Find People</a></li>
        </ul>
        <div class="tab-content">
            <div class='tab-pane active' id='requests-content'>
                <friend-container 
                    v-for='friend in requests' 
                    :data='friend' 
                    :type='friend.requestor?"cancel-request":"request"'
                    :key='friend.id'
                    btn_id='btnAcceptFriend'
                >
                    <template slot='button-name' >@{{friend.requestor?"Cancel Request":"Accept"}}</template>
                </friend-container>
                <div class="col-lg-12 no-records" v-if='!requests.length'>
                        No Friend Requests..
                </div>
            </div>
            <div class='tab-pane'  id='friends-content'>
                <friend-container 
                    v-for='friend in friends' 
                    :data='friend' 
                    :key='friend.id'
                    type='unfriend'
                    btn_id='btnUnfriend'
                >
                    <template slot='button-name'>Unfriend</template>
                </friend-container>
                <div class="col-lg-12 no-records"  v-if='!friends.length'>
                        You have no friends! Add Someone..
                </div>
            </div>
            <div class='tab-pane' id='find-people-content'>
                <div class="col-lg-offset-8 col-md-offset-8 col-lg-4 col-md-4">
                    <div class="form-group label-floating">
                        <label class="control-label" for="txt-name">Search..</label>
                        <input class="form-control" id="txt-name" type="text">
                    </div>
                </div>
                <friend-container 
                    v-for='friend in recommended' 
                    :data='friend' 
                    type='add'
                    :key='friend.id'
                    btn_id='btnUnfriend'
                >
                    <template slot='button-name'>Add Friend</template>
                </friend-container>
            </div>
        </div>
    </div>
</div>