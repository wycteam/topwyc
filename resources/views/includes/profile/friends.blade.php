<div class="col-lg-12" id='friends_holder'>
	<div class='col-lg-12'>
		<div class='md-tabs'>
			<span @click='clkShowRequest' class='btn-ripple' :class='showRequest?"active":""'>Requests</span>
			<span @click='clkShowFriends'  class='btn-ripple'  :class='showFriends?"active":""'>Friends</span>
			<span @click='clkShowRecommended'  class='btn-ripple'  :class='showRecommended?"active":""'>Find People</span>
		</div>
	</div>
	<div class='col-lg-12' v-show='showRequest'>
		<friend-container 
			v-for='friend in requests' 
			:data='friend' 
			type='request'
			:key='friend.id'
			btn_id='btnAcceptFriend'
		>
			<template slot='button-name'>Accept Request</template>
		</friend-container>
	</div>
	<div class='col-lg-12'  v-show='showFriends'>
		<friend-container 
			v-for='friend in friends' 
			:data='friend' 
			:key='friend.id'
			type='unfriend'
			btn_id='btnUnfriend'
		>
			<template slot='button-name'>Unfriend</template>
		</friend-container>
	</div>
	<div class='col-lg-12' v-show='showRecommended'>
		<div class="col-lg-offset-8 col-lg-4">
			<div class="field-container ">  
				<input type="text" class="field"  placeholder="Search.." required/>
				<label class="floating-label">Search..</label> 
				<div class="field-underline"></div>
			</div>
		</div>
		<friend-container 
			v-for='friend in recommended' 
			:data='friend' 
			type='add'
			:key='friend.id'
			btn_id='btnUnfriend'
		>
			<template slot='button-name'>Add Friend</template>
		</friend-container>
	</div>
</div>