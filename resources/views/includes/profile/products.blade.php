<div class="col-lg-12" id='products_container'>
    <div class="row">
      	<div >
          	<product-item
            	 v-for="item in items"
            	:item="item"
             	:key="item.id"
           	>
           	</product-item>
       	</div>
   	</div>
</div>
