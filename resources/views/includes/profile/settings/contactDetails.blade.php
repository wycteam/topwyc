 <!-- PROFILE SETTINGS > CONTACT DETAILS-->

<div class="col-sm-12">
	<h4 class="sub-header">CONTACT DETAILS</h4>
	<hr>

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-6 ">
            <div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('landline') }">
                <label class="control-label" for="txt-home-landline">Landline</label>
                <input class="form-control" id="txt-home-landline" type="text" name="landline" v-model="homeAddressInfo.landline" required="">
                <p class="help-block" v-if="homeAddressInfo.errors.has('landline')" v-text="homeAddressInfo.errors.get('landline')"></p>
            </div>
        </div><!-- end of col-sm-6 -->

        <div class="col-sm-6 ">
            <div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('mobile') }">
                <label class="control-label" for="txt-home-mobile">Mobile</label>
                <input class="form-control" id="txt-home-mobile" type="text" name="mobile" v-model="homeAddressInfo.mobile" required="">
                <p class="help-block" v-if="homeAddressInfo.errors.has('mobile')" v-text="homeAddressInfo.errors.get('mobile')"></p>
            </div>
        </div><!-- end of col-sm-6 -->
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('address') }">
			<label class="control-label" for="txt-home-address">Complete Address</label>
			<input v-model="homeAddressInfo.address" class="form-control" id="txt-home-address" type="text" name="full_address" required="">
			<p class="help-block" v-if="homeAddressInfo.errors.has('address')" v-text="homeAddressInfo.errors.get('address')"></p>
		</div>
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-6 account-input-left">
			<div class="form-group" :class="{ 'has-error': homeAddressInfo.errors.has('province') }">
				<multiselect class="custom-select" 
	            :options="optionProv" 
	            placeholder="Province" 
	            @input="getCity" 
	            v-model="homeAddressInfo.province"
	            :selected="homeAddressInfo.province"
	            deselect-label="This field is required"
	            :allow-empty="false"></multiselect>
	            <p class="help-block" v-if="homeAddressInfo.errors.has('province')" v-text="homeAddressInfo.errors.get('province')"></p>
			</div>
            
        </div><!-- end of col-sm-6 -->

        <div class="col-sm-6 account-input-right">
            <div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('barangay') }">
                <label class="control-label" for="txt-home-barangay">Barangay</label>
                <input class="form-control" id="txt-home-barangay" type="text" required="" v-model="homeAddressInfo.barangay">
                <p class="help-block" v-if="homeAddressInfo.errors.has('barangay')" v-text="homeAddressInfo.errors.get('barangay')"></p>
        	</div>
        </div><!-- end of col-sm-6 -->
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-6 account-input-left">
			<div class="form-group" :class="{ 'has-error': homeAddressInfo.errors.has('city') }">
				<multiselect class="custom-select" 
		        :options="optionCity" 
		        placeholder="City"  
		        v-model="homeAddressInfo.city"
		        :allow-empty="false"
		        deselect-label="This field is required"
		        :selected="homeAddressInfo.city"
		        ></multiselect>		
		        <p class="help-block" v-if="homeAddressInfo.errors.has('city')" v-text="homeAddressInfo.errors.get('city')"></p>		
			</div>
        </div><!-- end of col-sm-6 -->

        <div class="col-sm-6 account-input-right">
            <div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('zip_code') }">
                <label class="control-label" for="txt-ship-zc">Zip Code</label>
                <input class="form-control" id="txt-ship-zc" type="text" v-model="homeAddressInfo.zip_code" required="">
                <p class="help-block" v-if="homeAddressInfo.errors.has('zip_code')" v-text="homeAddressInfo.errors.get('zip_code')"></p>
        	</div>
        </div><!-- end of col-sm-6 -->
	</div><!-- end account-input-set -->


	<div class="col-sm-12 account-input-set">
		<div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('landmark') }">
            <label class="control-label" for="txt-home-landmark">Nearest Landmark</label>
            <input class="form-control" id="txt-home-landmark" type="text" name="landmark" v-model="homeAddressInfo.landmark" required="">
            <p class="help-block" v-if="homeAddressInfo.errors.has('landmark')" v-text="homeAddressInfo.errors.get('landmark')"></p>
        </div>
	</div><!-- end account-input-set -->
		
	<div class="col-sm-12 account-input-set">
        <div class="form-group label-floating" :class="{ 'has-error': homeAddressInfo.errors.has('remarks') }">
            <label class="control-label" for="txt-home-notes">Notes</label>
            <textarea class="form-control" rows="2" id="txt-home-notes" name="remarks" v-model="homeAddressInfo.remarks"></textarea>
            <span class="help-block" v-if="homeAddressInfo.errors.has('remarks')" v-text="homeAddressInfo.errors.get('remarks')"></span>
        </div>
	</div><!-- end account-input-set -->


	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<a href="javascript:void(0)" @click="saveHomeAddress" id ="acct_save_password" class="btn btn-raised btn-tangerine">Save</a>
		</div>
	</div><!-- end account-input-set -->
</div><!-- end col-sm-9 -->