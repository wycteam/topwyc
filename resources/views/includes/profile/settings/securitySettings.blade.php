<!--  PROFILE SETTINGS > SECURITY SETTINGS PANEL -->

<div class="col-sm-12 last-panel">
	<h4 class="sub-header">SECURITY SETTINGS</h4>
	<hr>
	<!-- 
		COLOR CODING
		** LOW > RED
		** MEDIUM > YELLOW
		** HIGH > GREEN

	 -->

	<!-- REMINDER CONTEXT-->
	<!-- CHANGE COLOR: change class 'bg-success' to 'bg-danger' & 'bg-warning' to manipulate contextual backgrounds -->
	<div id="reminder-context" class="col-sm-12 security-settings-set bg-success">
		<span class="reminder-high"></span>
		<h5 class="msg-context">
			<!-- CHANGE ICON & COLOR: change class 'reminder-high' to 'reminder-low' & 'reminder-med'-->
			
			Congratulations! Your account is well secured.
		</h5>
	</div><!-- end reminder-context -->

	<!-- PROGRESS BAR -->
	<div class="col-sm-12 security-settings-set progress-bar-set">
		<div class="col-sm-2 prof-label">
            <span>Security Level</span>
        </div><!-- end col-sm-2 -->

        <div class="col-sm-9 prof-label">
            <div class="col-xs-12 progressbar-main">
                <div class="progress progressbar-body">
                	<!-- CHANGE LEVEL: change class 'high' to 'low' & 'med' to manipulate progressbar level -->
                	<!-- CHANGE COLOR: change class 'progress-bar-success' to 'progress-bar-warning' & 'progress-bar-danger' to manipulate progressbar color -->
                	<div class="progress-bar progress-bar-success progressbar-color high"></div>
                </div><!-- end progressbar-body -->
            </div><!-- end progressbar-main -->
        </div><!-- end col-sm-2 -->

        <div class="col-sm-1 prof-label">
            <span>High</span>
        </div><!-- end col-sm-2 -->
	</div><!-- end account-input-set -->

	<!-- CHANGE PASSWORD -->
	<div class="col-sm-12 security-settings-set">
	@include('includes.profile.settings.changePassword')
	</div><!-- end security-settings-set -->

	<!-- SECURITY QUESTION -->
	<div class="col-sm-12 security-settings-set">
	@include('includes.profile.settings.securityQuestion')
	</div>

	<!-- UPLOAD DOCUMENTS -->
	<div class="col-sm-12 security-settings-set">
	@include('includes.profile.settings.uploadDocuments')
	</div>
</div>