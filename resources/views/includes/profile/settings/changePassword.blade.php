<div class="panel-group security-panel">
	<div class="panel panel-default">

		<div class="col-sm-12 panel-heading">
			<div class="col-sm-1">
				<span class="fa fa-key sec-settings-icon"></span>
			</div><!-- end col-sm-1-->

			<div class="col-sm-2">
				<p class="sec-settings-title">{{$lang['login-pw']}}</p>
				<p class="sec-settings-minitext">{{$lang['is-set']}}</p>
			</div><!-- end col-sm-2 -->

			<div class="col-sm-9">
				<p class="sec-settings-notes">
					{{$lang['strong-pw-msg']}}
				</p>

				<div class="text-right">
					<a class="btn btn-raised btn-success" data-toggle="collapse" href="#change-password">{{$lang['change']}}</a>
				</div>
			</div><!-- end col-sm-8 -->
		</div><!-- end panel-heading-->

		<div id="change-password" class="panel-collapse collapse">
		    <div class="panel-body account-input-set">
		    	<div class="col-sm-12 ">

			    	<div class="col-sm-4">
			    		<div class="form-group label-floating" :class="{ 'has-error': changePassword.errors.has('old_password') }">
							  <label class="control-label" for="txt-old-password">{{$lang['old-pw']}}</label>
							  <input class="form-control" id="txt-old-password" type="password" required v-model="changePassword.old_password" name="old_password">
							  <p class="help-block" v-if="changePassword.errors.has('old_password')" v-text="changePassword.errors.get('old_password')"></p>
						</div><!-- end label-floating -->
			    	</div><!-- end col-sm-4 -->

			    	<div class="col-sm-4">
			    		<div class="form-group label-floating" :class="{ 'has-error': changePassword.errors.has('new_password') }">
							  <label class="control-label" for="txt-new-password">{{$lang['new-pw']}}</label>
							  <input class="form-control" id="txt-new-password" type="password" required v-model="changePassword.new_password" name="new_password">
							  <p class="help-block" v-if="changePassword.errors.has('new_password')" v-text="changePassword.errors.get('new_password')"></p>
						</div><!-- end label-floating -->
			    	</div><!-- end col-sm-4 -->

			    	<div class="col-sm-4">
			    		<div class="form-group label-floating" :class="{ 'has-error': changePassword.errors.has('new_password_confirmation') }">
							  <label class="control-label" for="txt-confirm-password">{{$lang['conf-pw']}}</label>
							  <input class="form-control" id="txt-confirm-password" type="password" required v-model="changePassword.new_password_confirmation" name="new_password_confirmation">
							  <p class="help-block" v-if="changePassword.errors.has('new_password_confirmation')" v-text="changePassword.errors.get('new_password_confirmation')"></p>
						</div><!-- end label-floating -->
						<div class="text-right">
					    	<a href="#" id ="acct_save_password" class="btn btn-raised btn-tangerine btn-has-loading" @click="fnchangePassword">{{$lang['save']}}</a>
						</div>
			    	</div><!-- end col-sm-4 -->

		    	</div><!-- end col-sm-12 -->
		    </div><!-- end panel-body -->

		    <div class="panel-footer">
		    </div><!-- end panel-footer -->
		</div><!-- end panel-group-->
	</div><!-- end panel-group-->
</div><!-- end panel-group-->