<!--  PROFILE SETTINGS > PRIVACY SETTINGS PANEL-->

<div class="col-sm-12 next-panel">
	<h4 class="sub-header">PRIVACY SETTINGS</h4>
	<hr>

	<div class="col-sm-12 account-input-set">
		<div class="form-group">
	       <label class="col-md-2 control-label custom-label">Who can chat me</label>
		    <div class="radio radio-primary">
	          <label>
	            <input type="radio" name="optionsRadios" id="everybody" value="Everybody" checked="">
	            Everybody 
	          </label>
	          <label>
	            <input type="radio" name="optionsRadios" id="off" value="Off" >
	            Off
	          </label>
	        </div>
		</div>
	</div><!-- end account-input-set -->
</div>

