<div class="panel-group security-panel" id="security-question">
	<div class="panel panel-default">

		<div class="col-sm-12 panel-heading">
			<div class="col-sm-1">
				<span class="fa fa-shield sec-settings-icon"></span>
			</div><!-- end col-sm-1-->
			<div class="col-sm-2">
				<p class="sec-settings-title">{{$lang['sec-que']}}</p>
				<p class="sec-settings-minitext" v-if="securityAnswers != ''">{{$lang['is-set']}}</p>
				<p class="sec-settings-minitext critical" v-if="securityAnswers == ''">{{$lang['not-set']}}</p>
			</div><!-- end col-sm-2 -->
			<div class="col-sm-9">
				<p class="sec-settings-notes">
					{{$lang['sec-que-msg']}}
				</p>
				<div class="text-right">
					<a class="btn btn-success btn-raised " data-toggle="collapse" href="#security-questions" v-if="showSecurity">{{$lang['set-btn']}}</a>
					<a class="btn btn-success btn-raised " data-toggle="collapse" href="#security-questions" v-if="!showSecurity">{{$lang['see-que']}}</a>
				</div>
				
			</div><!-- end col-sm-8 -->
		</div><!-- end panel-heading-->

		<div id="security-questions" class="panel-collapse collapse" >
		<!-- You need to hide this portion if the user has set security questions -->

		    <div id="set-security-que" class="panel-body account-input-set" :class="{ active: answerEmpty }" v-if="showSecurity">
		    	<div class="col-sm-12 security-question-set" >
		    		<div class="col-sm-8">
		    			<div class="form-group label-floating" :class="{ 'has-error': securityQuestion.errors.has('question') }" >
		    				<multiselect 
		    				:options="securityQuestions" 
		    				name="question" 
		    				v-model="securityQuestion.question" 
		    				placeholder="{{$lang['select-que']}}" 
		    				label="question" 
		    				track-by="id" 
		    				select-label = "" 
		    				selected-label = "" 
		    				deselect-label = ""
		    				class="select-que"
		    				id="question"
		    				:searchable="false"
		    				@select="changeSelect"
		    				></multiselect>
		    				<p class="help-block" v-if="securityQuestion.errors.has('question')" v-text="securityQuestion.errors.get('question')"></p>
		    			</div>
		    		</div><!-- end col-sm-6 -->
		    		<div class="col-sm-4">
		    			<div class="form-group label-floating" :class="{ 'has-error': securityQuestion.errors.has('answer') }">
						  <label class="control-label" for="answer">{{$lang['ans']}}</label>
						  <input class="form-control" type="text" name="answer"  v-model="securityQuestion.answer">
						  <p class="help-block" v-if="securityQuestion.errors.has('answer')" v-text="securityQuestion.errors.get('answer')"></p>
					</div><!-- end label-floating -->		    				
		    		</div><!-- end col-sm-6 -->
		    	</div><!-- end security-question-set -->
		    	<div class="col-sm-12 security-question-set">
		    		<div class="col-sm-8">
		    			<div class="form-group label-floating" :class="{ 'has-error': securityQuestion.errors.has('question1') }">
		    				<multiselect 
		    				:options="securityQuestions" 
		    				name="question1" 
		    				v-model="securityQuestion.question1" 
		    				placeholder="{{$lang['select-que']}}"  
		    				label="question" 
		    				track-by="id" 
		    				select-label = "" 
		    				selected-label = "" 
		    				deselect-label = ""
		    				class="select-que"
		    				id="question1"
		    				:searchable="false"
		    				@select="changeSelect"
		    				></multiselect>
		    				<p class="help-block" v-if="securityQuestion.errors.has('question1')" v-text="securityQuestion.errors.get('question1')"></p>

		    			</div>
		    		</div><!-- end col-sm-6 -->
		    		<div class="col-sm-4">
		    			<div class="form-group label-floating" :class="{ 'has-error': securityQuestion.errors.has('answer1') }">
						  <label class="control-label" for="answer1">{{$lang['ans']}}</label>
						  <input class="form-control" type="text" name="answer1"  v-model="securityQuestion.answer1">
						  <p class="help-block" v-if="securityQuestion.errors.has('answer1')" v-text="securityQuestion.errors.get('answer1')"></p>
					</div><!-- end label-floating -->
		    		</div><!-- end col-sm-6 -->
		    	</div><!-- end security-question-set -->
		    	<div class="col-sm-12 security-question-set">
		    		<div class="col-sm-8">
		    			<div class="form-group label-floating" :class="{ 'has-error': securityQuestion.errors.has('question2') }">
		    				<multiselect 
		    				:options="securityQuestions" 
		    				name="question2" 
		    				v-model="securityQuestion.question2" 
		    				placeholder="{{$lang['select-que']}}" 
		    				label="question" 
		    				track-by="id" 
		    				select-label = "" 
		    				selected-label = "" 
		    				deselect-label = ""
		    				class="select-que"
		    				id="question2"
		    				:searchable="false"
		    				@select="changeSelect"
		    				></multiselect>
		    				<p class="help-block" v-if="securityQuestion.errors.has('question2')" v-text="securityQuestion.errors.get('question2')"></p>
		    			</div>
		    		</div><!-- end col-sm-6 -->
		    		<div class="col-sm-4">
		    			<div class="form-group label-floating" :class="{ 'has-error': securityQuestion.errors.has('answer2') }">
						  <label class="control-label" for="txt-sec-que-2">{{$lang['ans']}}</label>
						  <input class="form-control" type="text" name="answer2" v-model="securityQuestion.answer2">
						  <p class="help-block" v-if="securityQuestion.errors.has('answer2')" v-text="securityQuestion.errors.get('answer2')"></p>
					</div><!-- end label-floating -->
		    		</div><!-- end col-sm-6 -->
		    	</div><!-- end security-question-set -->
		    <!-- You need to hide this portion if the user has set security questions -->
		    	<div class="col-lg-12 text-right" v-if="showSecurity">
			    	<a href="javascript:void(0)" id="btn_sec_que" @click="saveSecurityQuestion" class="btn btn-raised btn-tangerine" >{{$lang['save']}}</a>
			    </div>
		    </div><!-- end set-security-que -->

		<!-- You need to hide this portion if the user hasn't set security questions -->
		   	<div id="view-security-que" class="panel-body account-input-set" :class="{ active: !answerEmpty }">
		 		 <que-answer v-if="!showSecurity"
                      v-for="item in securityAnswers"
                      :item="item"
                      :key='item.id'
                    >
                  </que-answer>
		    </div><!-- end view-security-que -->

		   
		    
		</div><!-- end panel-group-->
	</div><!-- end panel-group-->
</div><!-- end panel-group-->