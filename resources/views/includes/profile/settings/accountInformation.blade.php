 <!-- PROFILE SETTINGS > ACCOUNT INFORMATION PANEL > DATA -->

<div class="col-sm-9">
<!-- {!! Form::open()!!} -->
	<h4 class="sub-header">ACCOUNT INFORMATION</h4>
	<hr>
	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<div class="form-group label-floating">
			  <label class="control-label" for="txt-name">Full Name</label>
			  <input v-model="user.full_name" class="form-control" id="txt-name" type="text" readonly="" name="full_name" >
			  <p class="help-block">You need to contact our Customer Service to change your full name.</p>
		</div>
		</div>
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<div class="form-group label-floating">
			  <label class="control-label" for="txt-email">E-Mail Address</label>
			  <input v-model="user.email" class="form-control" id="txt-email" type="text" readonly="" name="email" >
			  <p class="help-block">You cannot change your E-mail Address.</p>
		</div><!-- end label-floating -->
		</div>
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<div class="form-group label-floating" :class="{ 'has-error': accInfo.errors.has('secondary_email') }">
			  <label class="control-label" for="txt-sec-email">Secondary E-Mail</label>
			  <input class="form-control" id="txt-sec-email" type="text" v-model="accInfo.secondary_email" name="secondary_email">
			  <p class="help-block" v-if="accInfo.errors.has('secondary_email')" v-text="accInfo.errors.get('secondary_email')"></p>
		</div><!-- end label-floating -->
		</div>
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<div class="form-group label-floating" :class="{ 'has-error': accInfo.errors.has('birth_date') }">
			  <label class="control-label" for="txt-bday">Birthday</label>
			  <input v-model="accInfo.birth_date" v-datepicker class="form-control" id="birth_date" type="text" required name="birth_date">
			  <p class="help-block" v-if="accInfo.errors.has('birth_date')" v-text="accInfo.errors.get('birth_date')"></p>
		</div><!-- end label-floating -->
		</div>
	</div><!-- end account-input-set -->
	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<multiselect
			  v-model="accInfo.gender"
			  :options="optionGender"
			  deselect-label="This field is required."
			  :allow-empty="false">
			</multiselect>
		</div>
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<div class="form-group label-floating">
			  <label class="control-label" for="txt-nickname">Nickname</label>
			  <input class="form-control" id="txt-nickname" type="text" v-model="accInfo.nick_name" name="nick_name">
	<!-- 		  <p class="help-block">You should really write something here</p>
	 -->	</div><!-- end label-floating -->	
		</div>
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<div class="checkbox">
	          <label>
	            <input type="checkbox" 
	              id="chk_display"
	              v-model="accInfo.display_name"> 
	              Set as Display Name
	          </label>
	        </div>
		</div>
	</div><!-- end account-input-set -->

	<div class="col-sm-12 account-input-set">
		<div class="col-sm-12">
			<a href="javascript:void(0)" @click="saveInfo" id ="acct_save_password" class="btn btn-raised btn-tangerine">Save</a>
		</div>
	</div><!-- end account-input-set -->

</div><!-- end col-sm-9 -->

<!--  PROFILE SETTINGS > ACCOUNT INFORMATION PANEL > PROFILE PICTURE-->
<div class="col-sm-3 account-picture-set">
	<!-- IF USER IS A GIRL -->
<!-- 	{!! HTML::image('shopping/img/profile/icons/girl-avatar.png', 'Upload Profile Picture', ['class' => 'img-responsive']) !!}
 -->	<!-- IF USER IS A BOY -->
	{!! HTML::image('shopping/img/profile/icons/boy-avatar.png', 'Upload Profile Picture', ['class' => 'img-responsive']) !!}

	<div class="avatar-uploader">
		<div class="edit">Upload Picture</div> 
		<input type="file">
	</div>
</div><!-- end col-sm-3