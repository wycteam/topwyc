<div class="panel-group security-panel">
	<div class="panel panel-default">

		<div class="col-sm-12 panel-heading">
			<div class="col-sm-1">
				<span class="fa fa-upload sec-settings-icon"></span>
			</div><!-- end col-sm-1-->
			<div class="col-sm-2">
				<p class="sec-settings-title">Upload Documents</p>
				<p class="sec-settings-minitext">6 file/s have been uploaded</p>
				<p class="sec-settings-minitext">Your account is verified.</p>
			</div><!-- end col-sm-2 -->
			<div class="col-sm-8">
				<p class="sec-settings-notes">
					By uploading identification documents it will make your account more secure and unique, 
					and your account will become verified account.
				</p>
			</div><!-- end col-sm-8 -->
			<div class="col-sm-1">
				<a class="sec-settings-link" data-toggle="collapse" href="#upload-documents">Upload</a>
			</div><!-- end col-sm-1-->
		</div><!-- end panel-heading-->

		<div id="upload-documents" class="panel-collapse collapse">
		<!-- {!! Form::open()!!} -->
		    <div class="panel-body account-input-set">
		    	<div class="">
		    		<div class="col-sm-12 individual-docs">
		    			<div class="list-group">
							<div class="list-group-item active">
							   <h5 class="upload-doc-subhead">For Individual Account</h5>
							   <hr>
							</div><!-- end active -->
						 	<div class="col-sm-12 document-set list-group-item">
			    				<div class="col-sm-7">
			    					<span><span  class="fa fa-asterisk"></span> NBI</span>
			    					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload your valid NBI"><span class="fa fa-question-circle-o icon-color" ></span></span>
			    					<span class="label label-default">Pending</span>
			    				</div><!-- end col-sm-6 -->
			    				<div class="col-sm-5 doc-btn-set">		    				
			    					<div class="fileUpload btn btn-tangerine btn-raised btn-xs">
										<span>Upload</span>
										<input type="file" class="upload" />
									</div>
			    					{!!Form::button('View',['id' =>'view_nbi', 'class' => 'btn btn-xs btn-default'])!!}
			    					{!!Form::button('Download',['id' =>'download_nbi', 'class' => 'btn btn-xs btn-default'])!!}
			    				</div><!-- end col-sm-6 -->
		    				</div><!-- end document-set -->
		    				<div class="col-sm-12 document-set list-group-item">
			    			 	<div class="col-sm-7">
			    					<span><span  class="fa fa-asterisk"></span> Birth Certificate</span>
			    					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload your valid Birth Certificate"><span class="fa fa-question-circle-o icon-color" ></span></span>
			    					<span class="label label-success">Verified</span>
			    				</div><!-- end col-sm-6 -->
			    				<div class="col-sm-5 doc-btn-set">
			    					<div class="fileUpload btn btn-tangerine btn-raised btn-xs">
										<span>Upload</span>
										<input type="file" class="upload" />
									</div>
			    					{!!Form::button('View',['id' =>'view_bc', 'class' => 'btn btn-xs btn-default'])!!}
			    					{!!Form::button('Download',['id' =>'download_bc', 'class' => 'btn btn-xs btn-default'])!!}
			    				</div><!-- end col-sm-6 -->
			    			</div><!-- end document-set -->
			    			<div class="col-sm-12 document-set list-group-item">
			    			 	<div class="col-sm-7">
			    					<span><span  class="fa fa-asterisk"></span> Government ID</span>
			    					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload any ID issued by the Philippine Government"><span class="fa fa-question-circle-o icon-color" ></span></span>
			    					<span class="label label-danger">Denied</span>
			    				</div><!-- end col-sm-6 -->
			    				<div class="col-sm-5 doc-btn-set">
			    					<div class="fileUpload btn btn-tangerine btn-raised btn-xs">
										<span>Upload</span>
										<input type="file" class="upload" />
									</div>
			    					{!!Form::button('View',['id' =>'view_gov', 'class' => 'btn btn-xs btn-default'])!!}
			    					{!!Form::button('Download',['id' =>'download_gov', 'class' => 'btn btn-xs btn-default'])!!}
			    				</div><!-- end col-sm-6 -->
			    			</div><!-- end document-set -->
						</div><!-- end list-group -->
		    		</div><!-- end individual-docs -->

		    		<div class="col-sm-12 enterprise-docs">
		    			<div class="list-group">
							<div class="list-group-item active">
							   <h5 class="upload-doc-subhead">For Enterprise Account</h5>
							   <hr>
							</div><!-- end active -->
						 	<div class="col-sm-12 document-set list-group-item">
			    				<div class="col-sm-7">
			    					<span><span  class="fa fa-asterisk"></span> SEC Registration</span>
			    					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload your valid SEC Registration"><span class="fa fa-question-circle-o icon-color" ></span></span>
			    					<span class="label label-danger">Denied</span>
			    				</div><!-- end col-sm-6 -->
			    				<div class="col-sm-5 doc-btn-set">
									<div class="fileUpload btn btn-tangerine btn-raised btn-xs">
										<span>Upload</span>
										<input type="file" class="upload" />
									</div>
			    					{!!Form::button('View',['id' =>'view_sec', 'class' => 'btn btn-xs btn-default'])!!}
			    					{!!Form::button('Download',['id' =>'download_sec', 'class' => 'btn btn-xs btn-default'])!!}
			    				</div><!-- end col-sm-6 -->
		    				</div><!-- end document-set -->
		    				<div class="col-sm-12 document-set list-group-item">
			    			 	<div class="col-sm-7">
			    					<span><span  class="fa fa-asterisk"></span> BIR Registation</span>
			    					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload your valid BIR Registration"><span class="fa fa-question-circle-o icon-color" ></span></span>
			    					<span class="label label-success">Verified</span>
			    				</div><!-- end col-sm-6 -->
			    				<div class="col-sm-5 doc-btn-set">
			    					<div class="fileUpload btn btn-tangerine btn-raised btn-xs">
										<span>Upload</span>
										<input type="file" class="upload" />
									</div>
			    					{!!Form::button('View',['id' =>'view_bir', 'class' => 'btn btn-xs btn-default'])!!}
			    					{!!Form::button('Download',['id' =>'download_bir', 'class' => 'btn btn-xs btn-default'])!!}
			    				</div><!-- end col-sm-6 -->
			    			</div><!-- end document-set -->
			    			<div class="col-sm-12 document-set list-group-item">
			    			 	<div class="col-sm-7">
			    					<span><span  class="fa fa-asterisk"></span> Business Permit</span>
			    					<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload your valid Business Permit "><span class="fa fa-question-circle-o icon-color" ></span></span>
			    					<span class="label label-danger">Denied</span>
			    				</div><!-- end col-sm-6 -->
			    				<div class="col-sm-5 doc-btn-set">
			    					<div class="fileUpload btn btn-tangerine btn-raised btn-xs">
										<span>Upload</span>
										<input type="file" class="upload" />
									</div>
			    					{!!Form::button('View',['id' =>'view_bus', 'class' => 'btn btn-xs btn-default'])!!}
			    					{!!Form::button('Download',['id' =>'download_bus', 'class' => 'btn btn-xs btn-default'])!!}
			    				</div><!-- end col-sm-6 -->
			    			</div><!-- end document-set -->
						</div><!-- end list-group -->
		    		</div><!-- end individual-docs -->
		    	</div><!-- end col-sm-12 -->
		    </div><!-- end panel-body -->
		    
		    <div class="panel-footer">
		    </div><!-- end panel-footer -->
		<!-- {!! Form::close()!!} -->
		</div><!-- end panel-group-->
	</div><!-- end panel-group-->
</div><!-- end panel-group-->