<div class='card text-center'>
    <div class="col-lg-12 col-md-12 " id='avatar-container'>
        <br>
        <div class="avatar">
            <img :src="profileImage" class='img-responsive img-circle btnUpload center-block' data-toggle='tooltip' title="{{$lang['upload-dp']}}">
            <div class="avatar-uploader hide">
              <imgfileupload 
                data-event="avatarUpload" 
                data-url="/profile/update-avatar?width=300&height=300&type=avatar&resize=true"></imgfileupload>
            </div>
          </div>
            <span 
                v-if='true' 
                class='abs mouse-hand to-top to-right  m-t-2 m-r-2' 
                data-toggle='tooltip'
                data-placement='right' 
                title=''
            >
                <span 
                    class='mouse-hand  fa fa-2x' 
                    data-toggle="tab" href='#documents'></span>
            </span>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <a href='/users/{{$user->id}}'>
                <h2 
                    data-toggle='tooltip'
                    data-placement="bottom"
                    title="{{$user->nick_name? : ''}}"
                >
                    {{$user->first_name}} {{$user->last_name}}
                </h2>
            </a>
        </div>
    </div>
</div>