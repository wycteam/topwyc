<!-- top-header mobile version-->
  <div class="wataf-mobile">
    <div class="wataf-mobile-hdr col-xs-12 hidden-lg hidden-md hidden-sm hidden-xs">
      <!-- Left -->
      @if ($isAuthenticated)
      <div class="col-xs-7 pull-left header-left">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
          <span>
            <i class="fa fa-bars" aria-hidden="true"></i>
            <span class="wataf-title-hdr">&nbsp; WATAF Shopping</span>
          </span> 
        </a>
      </div>
      @endif
      <!-- Right -->
      <div class="col-xs-5 pull-right header-right">
        <ul class="list-inline pull-right">
          @if (! $isAuthenticated)
          <li>
            <div class="dropdown">
              <button class="btn dropdown-toggle" type="button" id="dropdownLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                @lang('header.login') <span class="caret"></span>
              </button>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-login" aria-labelledby="dropdownLogin" data-dropdown-in="zoomIn" data-dropdown-out="fadeOut">
                @include('auth._login')
              </div>
            </div>
          </li>
          @else
          <li>
            <div class="dropdown">
              <a>
                <i class="notif-icon fa fa-shopping-cart" aria-hidden="true"></i>
              </a>
              <span class="label label-danger counter" v-cloak>1</span>
            </div>
          </li>
          <li>
            <div class="dropdown">
              <a>
                <i class="notif-icon fa fa-envelope" aria-hidden="true"></i>
              </a>
              <span class="label label-danger counter" v-cloak>1</span>
            </div>
          </li>
          <li>
            <div class="dropdown">
              <a >
                <i class="notif-icon fa fa-bell" aria-hidden="true"></i>
              </a>
              <span class="label label-danger counter" v-cloak>1</span>
            </div>
          </li>
          @endif
        </ul>
      </div>
    </div>
  </div>
<!-- End of top-header mobile version-->