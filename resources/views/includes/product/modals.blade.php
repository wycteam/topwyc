<modal id='modalProdReport'  size="modal-md" content-size='12'>
	<template slot="modal-title">
        <h3>
            {{$lang['report-product']}}
        </h3>
    </template>
    <template slot="modal-body">
	    		<div class='row'>
		    		<div class='col-md-12'>
						<div class="form-group" :class="{ 'has-error': form.errors.has('subject') }">
						  <label class="control-label" for="subject">{{$lang['report-title']}}</label>
						  <input v-model="form.subject" class="form-control" id="subject" type="text" autofocus disabled="">
						</div>
		    		</div>
	    		</div>
	    		<div class='row '>
		    		<div class="col-md-12">
						<div class="form-group" :class="{ 'has-error': form.errors.has('body') }">
						  <label class="control-label" for="body">{{$lang['description']}}</label>
						  <textarea v-model="form.body" name="" id="body" required="" rows="1" class='form-control' autofocus required></textarea>
						  <span class="help-block" v-if="form.errors.has('body')" v-text="form.errors.get('body')"></span>
						</div>
					</div>
	    		</div>
    </template>
    <template slot="modal-footer">
	     <div class='col-md-12' >
			<button id="submitReport" class='btn btn-raised btn-success ' @click='submitReport()'> {{$lang['submit-report']}}</button>
	    </div>
	     <div class='col-md-12'data-dismiss="modal" aria-label="Close">
			<button class='btn btn-default'>{{$lang['close']}}</button>
	    </div>
    </template>
</modal>