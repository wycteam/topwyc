<div class="container wow zoomIn" id="result">
    <div class="row m-b-lg">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>{{$lang['tracking']}}</h1>
            <p>{{$lang['track-1-desc']}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4" style="text-align:center">
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-12 tracking-txt">
                        <input type="text" class="form-control text-center" v-model="searchTrack.tracking">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12"><a class="btn btn-lg btn-primary" @click.prevent="searchEnter()">{{$lang['track-btn']}}</a></div>
            </div>
        </div>
        <div class="col-sm-4"></div>
    </div>
</div>