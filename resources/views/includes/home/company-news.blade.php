
<div class="wow zoomIn" style="padding:20px;">

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="row m-b-lg">
            <div class="text-center">
                  <div class="navy-line"></div>
                  <!---
                  <h1>{{$lang['featured-news']}}</h1>
                    -->
                    <h1>{{$lang['latest-news']}}</h1>
            </div>
          </div>

                <news-articles class="latest_news" id="newsLoader" >


                </news-articles>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12" style="text-align:center">

              <!---
              <div class="row m-b-lg">
                <div class="text-center">
                      <div class="navy-line"></div>

                      <h1>{{$lang['latest-news']}}</h1>
                </div>
              </div>
            -->
              <news-articles-featured id="newsLoader2">

              </news-articles-featured>


        </div>



    </div>
</div>
