<div class="container wow pulse services" style="padding-top:0px !important">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h3>{{$lang['tagline-3']}}</h3>
            <h4>{{$lang['tagline-3-desc']}}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-lg-offset-1 features-text">
            <small>{{$lang['value-1-sub']}}</small>
            <h2>{{$lang['value-1']}}</h2>
            <i class="fa fa fa-eye big-icon pull-right"></i>
            <p>{{$lang['value-1-long-desc']}}
            <br><br>
            {{$lang['value-1-long-desc2']}}</p>
        </div>
        <div class="col-lg-5 features-text">
            <small>{{$lang['value-2-sub']}}</small>
            <h2>{{$lang['value-2']}}</h2>
            <i class="fa fa-heart big-icon pull-right"></i>
            <p> {{$lang['value-3-long-desc']}}
            <br><br>
            {{$lang['value-3-long-desc2']}}{{$lang['value-3-long-desc3']}}</p>

            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-lg-offset-1 features-text">
            <small>{{$lang['value-3-sub']}}</small>
            <h2>{{$lang['value-3']}}</h2>
            <i class="fa fa-lock big-icon pull-right"></i>
            <p> {{$lang['value-2-long-desc']}}
            <br><br>
            {{$lang['value-2-long-desc2']}}{{$lang['value-2-long-desc3']}}
        </div>
        <div class="col-lg-5 features-text">
            <small>{{$lang['value-4-sub']}}</small>
            <h2>{{$lang['value-4']}}</h2>
            <i class="fa fa-handshake-o big-icon pull-right"></i>
            <p>{{$lang['value-4-long-desc']}}</p>
        </div>
    </div>
</div>
