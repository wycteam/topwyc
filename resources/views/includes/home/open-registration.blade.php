<div class="container wow">
    <div class="row" id="signup" v-show="!verify">
        <div class="col-lg-12 text-center">
            <!-- <div class="navy-line"></div> -->
            <!-- <h2>{{$lang['msg-fill-details']}}<br/> <span class="navy"></span> </h2> -->
            <!-- <h2> <span class="navy"></span> </h2> -->
            <p> </p>
        </div>
    </div>

        <div class="row" id="verify" v-if="verify" v-cloak>
            <div class="col-lg-12 text-center">
                <!-- <div class="navy-line"></div> -->
                <!-- <h2>{{$lang['msg-pin-verify']}}<br/> <span class="navy"></span> </h2>
                <p >{{$lang['msg-pin-verify-long']}}</p> -->
                <!-- <h2> <span class="navy"></span> </h2> -->
                <p> </p>
            </div>
        </div>
                <div class="wrapper" v-cloak>
                    <div class="inner">
                        <div class="image-holder">
                            <img src="/images/form-wizard-1.jpg" alt="">
                        </div>
                        <form method="POST" action="{{ route('shopping.save-registration') }}" @submit.prevent="submit" @keydown="form.errors.clear($event.target.name)" novalidate autocomplete="off" >
                        <div v-show="!verify">
                            <div class="form-holder active">
                                <input type="text" placeholder="{{$lang['fname']}}" v-model="form.first_name" class="form-control" :class="{
                                                        'error-required' : form.errors.has('first_name')}">
                                <!-- <small class="text-danger" v-if="form.errors.has('fname')">First name is required.</small> -->
                            </div>
                            <div class="form-holder">
                                <input type="text" placeholder="{{$lang['lname']}}" v-model="form.last_name" class="form-control" :class="{
                                                        'error-required' : form.errors.has('last_name')}">
                            </div>
                            <div class="form-holder hide">
                                <input type="text" placeholder="{{$lang['email-opt']}}" v-model="form.email" class="form-control" :class="{
                                                        'error-required' : form.errors.has('email')}">
                            </div>
                            <div class="form-holder">
                                <input type="text" placeholder="{{$lang['mob-num']}}" id="phone" v-model="form.cp_num" class="form-control" style="font-size: 13px;" :class="{'error-required' : form.errors.has('cp_num')}" maxlength='11'>
                            </div>

                            <small style="color: #ababab;"><b>{{$lang['birth-date']}}</b></small>
                            <div class="form-holder" class="custom-date-dropdowns">
                                <div class="row">
                                    <input type="hidden" id="bday" name="bday" class="form-control">          
                               <!--  <input type="text" placeholder="{{$lang['bday']}}" name="birth_date" id="birth_date" v-model="form.bday" class="datepicker form-control" style="font-size: 15px;" :class="{'error-required' : form.errors.has('bday')}">
 -->
                                <!-- <the-mask mask="##/##/####" value="" type="text" masked="true" placeholder="Date of Birth (m/D/Y)" name="birth_date" id="birth_date" v-model="form.bday" class="form-control" autocomplete="false" :class="{'error-required' : form.errors.has('bday')}" style="font-size: 15px;"></the-mask> -->
                                </div>
                            </div>

     


                                   
        
                           
   <!--                          <div class="form-holder">
                                <input type="text" id="referral" placeholder="{{$lang['msg-who-recommended']}}" v-model="form.referral" class="form-control" style="font-size: 15px;" @focus="showpopover('referral')" @blur="hidepopover('referral')" >
                            </div> -->
                            <div class="form-holder" style="margin-top: 5px;">
                                <input type="radio" id="male" value="Male" v-model="form.gender">
                                <label for="one">{{$lang['male']}}</label>
                                <input type="radio" id="female" value="Female" v-model="form.gender" style="margin-left: 8px; ">
                                <label for="two">{{$lang['female']}}</label>
                            </div>
                        </div>
                            <small v-show="verify"><b>{{$lang['msg-pin-verify']}}. </b>{{$lang['msg-pin-verify-long']}}</small>
                            <div class="form-holder" v-if="verify" v-cloak style="margin-top: 12px;">
                                <input type="text" placeholder="{{$lang['msg-enter-pin']}}" id="verification" v-model="form2.verification" class="form-control" style="font-size: 15px;" :class="{'error-required' : form2.errors.has('verification')}" v-cloak>
                            </div>
                            <div class="form-login" style="margin-bottom: 10px;">
                                <button id="regsubmit" type="submit" v-show="!verify" class="open-reg-btn">{{$lang['sign-up']}}</button>
                                <button class="open-reg-btn2" id="regverify" type="button" v-if="verify" v-cloak @click="submitVerification" v-cloak>{{$lang['continue']}}</button>
                                <p class="hide">{{$lang['already-reg']}} <a href="#"><br>{{$lang['msg-verify-here']}}</a></p>
                            </div>
                                <center><a href="#" id="resendcode" type="button" v-show="verify" v-on:click="resendCode"><b>{{$lang['resend']}}</b></a></center>
                        </form>
                    </div>
        </div>

</div>


