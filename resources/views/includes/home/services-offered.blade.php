<div class="row">
    <div class="col-lg-12 text-center">
        <div class="navy-line"></div>
        <h1>{{$lang['services']}}<br/> <span class="navy"> {{$lang['offered']}}</span> </h1>

    </div>
</div>
<div class="row">
    <div class="col-md-3 text-center wow fadeInLeft">
        <div>
            <i class="fa fa-plane features-icon"></i>
            <h2>{{$lang['service-1-name']}}</h2>
            <p>{{$lang['service-1-desc']}}</p>
        </div>
        <div class="m-t-lg">
            <i class="fa fa-check features-icon"></i>
            <h2>{{$lang['service-3-name']}}</h2>
            <p>{{$lang['service-3-desc']}}</p>
        </div>
    </div>
    <div class="col-md-6 text-center  wow zoomIn">
        <img src="/images/perspective.png" alt="dashboard" class="img-responsive">
    </div>
    <div class="col-md-3 text-center wow fadeInRight">
        <div>
            <i class="fa fa-briefcase features-icon"></i>
            <h2>{{$lang['service-2-name']}}</h2>
            <p>{{$lang['service-2-desc']}}</p>
        </div>
        <div class="m-t-lg">
            <i class="fa fa-user-plus features-icon"></i>
            <h2>{{$lang['service-4-name']}}</h2>
            <p>{{$lang['service-4-desc']}}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <a class="btn btn-lg btn-primary" href="/services" role="button">{{$lang['service-view-btn']}}</a>
    </div>
</div>