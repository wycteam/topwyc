<div class="container">
    <div class="row m-b-lg">
        <div class="navy-line"></div>
    </div>
    <div class="row m-b-lg">
        <div class="col-lg-2 col-lg-offset-2 text-center">
            <address>
                <strong><i class="fa fa-map-marker" aria-hidden="true"></i> <span class="navy"> WYC BUSINESS CONSULTANCY INC.</span></strong><br/>
                Unit 110-111. Balagtas St<br>Balagtas Villas, Brgy 15 <br>Pasay City 1306<br>
                <i class="fa fa-phone" aria-hidden="true"></i> (02) 354 80 21
            </address>
        </div>
        <div class="col-lg-4 text-center">
            <div class="col-lg-12 text-center">
                
                    <h1>{{$lang['tagline-4']}}</h1>
                    <p>{{$lang['tagline-4-desc']}}</p>
                </div>
            </div>
        <div class="col-lg-4 text-center">
            <!-- <div class="col-lg-6 text-center">
                <a href="">
                    <img src="/shopping/images/android.png" class="android-img-btn">
                </a>
                <a href="">
                    <img src="/shopping/images/apple.png" class="android-img-btn">
                </a>
            </div>
            <div class="col-lg-6 text-center">
                <a href="">
                    <img src="/shopping/images/qrapp.png" class="android-img-btn">
                </a>
            </div> -->
            <div class="col-lg-12 text-center">
                <p class="text-color">
                    {{$lang['tagline-4-desc-2']}}
                </p>
            </div>
            
        </div>
    </div>
    <br><br>
</div>