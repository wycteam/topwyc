<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>{{$lang['mobile-app']}}</h1>
            <p>{{$lang['mobile-msg-1']}}</p>
        </div>
    </div>
    <div class="row features-block">
        <div class="col-lg-3 features-text wow fadeInLeft">
            <small>{{$lang['apple-store']}}</small>
            <h2>{{$lang['for-ios']}}</h2>
            <p><a href="https://itunes.apple.com/ph/app/wyc-group-tracker/id1360927056?mt=8" target="_blank">
                <img src="/shopping/images/apple.png" style="width: 300px; margin-left: -26px;">
            </a></p>
<!--             <a href="https://itunes.apple.com/ph/app/wyc-group-tracker/id1360927056?mt=8" target="_blank" class="btn btn-primary">Learn more</a>
 -->        </div>
        <div class="col-lg-6 text-right m-t-n-lg wow zoomIn">
            <a href="/apk/wycgrouptracker.apk" target="_blank">
                @if(config('app.locale')=='en')
                <img src="shopping/images/qrapp3.png" class="img-responsive" alt="qr">
                @endif
                @if(config('app.locale')=='cn')
                <img src="shopping/images/qrapp3-cn.png" class="img-responsive" alt="qr">
                @endif
            </a>
        </div>
        <div class="col-lg-3 features-text text-right wow fadeInRight">
            <small>{{$lang['google-play']}}</small>
            <h2>{{$lang['for-android']}}</h2>
            <p><a href="https://play.google.com/store/apps/details?id=com.wyc.visatrackerapp&hl=en_US"  target="_blank">
                <img src="/shopping/images/android.png" style="width: 300px;">
            </a></p>
<!--             <a href="https://play.google.com/store/apps/details?id=com.wyc.visatrackerapp&hl=en_US"  target="_blank" class="btn btn-primary">Learn more</a>
 -->        </div>
    </div>
</div>
<br>