<dialog-modal id='dialogRemove'>
	<template slot="modal-title">
        {{$lang['remove-items']}}
    </template>
    <template slot="modal-body">
        <!-- Let Google help apps determine location. This means this is a random and gibberish string that is needed for testing. -->
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">@lang('common.cancel')</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='removeItems()'>@lang('common.proceed')</button>
    </template>
</dialog-modal>

<modal id='modalCheckout'  size="modal-lg" content-size='12' class="custom-chosen">
	<template slot="modal-title">
        <h3>
            {{$lang['checkout']}} (<span v-if="selected.length!=0">@{{selected.length}}</span><span v-if="orders.length!=0">@{{orders.length}}</span> {{$lang['item']}})
        </h3>
    </template>
    <template slot="modal-body">
    	<div class='checkout-steps-container checkout-step-address' v-show='current_step == 1'>
    		<div class='col-md-12'>
	    		<div class='row col-md-12'>
		    		<div class='col-md-6'>
		    			<h3 class="mar-step1"><small><b>{{$lang['shipping-address']}}</b></small></h3>
		    			<div>
		    				<a href="javascript:void(0)" id="saveProfile" @click='saveProfile()'>{{$lang['save-profile']}}</a> <span v-show="form.type_id != 0">|</span>
		    				<a href="javascript:void(0)" id="deleteProfile" @click='deleteProfile()' v-show="form.type_id != 0">{{$lang['delete-profile']}}</a>			     
					    </div>
		    		</div>
		    		<div class='col-md-6'>
						<div class="e-wallet float">
							<p class="titles">{{$lang['e-wallet-balance']}}</p>
							<p class="balance">@{{ewallet | currency}}</p>
						</div>
		    		</div>
	    		</div>
	    		<div class='row col-md-12'>
		    		<div class='col-md-6'>
		    			<div class="form-group">
						  <label class="control-label" for="address_profile">{{$lang['profile']}}</label>
						  	<select v-model="selectedType" class='form-control' id='address_profile' required  @click="getAddressDetails($event)">
								<option :value=item.id v-for="item in optionType">@{{item.type}}</option>
							</select>
						</div>
		    		</div>
		    		<div class='col-md-6' v-show="form.type_id == 0">
		    			<div class="form-group" :class="{ 'has-error': form.errors.has('type') }">
						  <label class="control-label" for="type">{{$lang['new-profile']}}</label>
						  <input v-model="form.type" class="form-control" id="type" type="text" placeholder="{{$lang['ex-home-work']}}" type="text" name="type" autofocus required>
						  <span class="help-block" v-if="form.errors.has('type')">{{$lang['please-new-profile']}}</span>
						</div>
		    		</div>
	    		</div>
	    		<div class='row col-md-12'>
		    		<div class='col-md-7'>
						<div class="form-group" :class="{ 'has-error': form.errors.has('name') }">
						  <label class="control-label" for="name">{{$lang['name-of-receiver']}}</label>
						  <input v-model="form.name" class="form-control" id="name" type="text" autofocus required>
						  <span class="help-block" v-if="form.errors.has('name')">{{$lang['please-receiver']}}</span>
						</div>
		    		</div>
		    		<div class="col-md-5">
						<div class="form-group" :class="{ 'has-error': form.errors.has('number') }">
						  <label class="control-label" for="number">{{$lang['contact-number']}}</label>
						  <input v-model="form.number" class="form-control" id="number" type="text" maxlength="11" autofocus required>
						  <span class="help-block" v-if="form.errors.has('number')">{{$lang['please-number']}}</span>
						</div>
		    		</div>
	    		</div>
	    		<div class='row col-md-12'>
	    			<div class="col-md-3">
						<div class="form-group">
						  <label class="control-label" for="zip_code">{{$lang['zip-code']}}</label>
						  <input v-model="form.zip_code" class="form-control" id="zip_code" type="text" autofocus>
						</div>
					</div>
		    		<div class="col-md-9">
						<div class="form-group" :class="{ 'has-error': form.errors.has('address') }">
						  <label class="control-label" for="address">{{$lang['complete-address']}}</label>
						  <textarea v-model="form.address" name="" id="address" required="" rows="1" class='form-control' autofocus required></textarea>
						  <span class="help-block" v-if="form.errors.has('address')">{{$lang['please-address']}}</span>
						</div>
					</div>
	    		</div>
	    		<div class='row col-md-12'>
	    			<!-- <div class="col-md-6">
						<div class="form-group" :class="{ 'has-error': form.errors.has('province') }">
						  <label class="control-label" for="province">{{$lang['province']}}</label>
						  <input v-model="form.province" class="form-control" id="province" type="text" autofocus required>
						  <span class="help-block" v-if="form.errors.has('province')">{{$lang['please-province']}}</span>
						</div>
					</div> -->
					<div class="col-lg-6">
						<div class="form-group" :class="{ 'has-error': form.errors.has('province') }">
							<multiselect class="custom-select" 
				            :options="optionProv" 
				            placeholder="{{$lang['province']}}" 
				            @input="getCity" 
				            v-model="form.province"
				            :selected="form.province"
				            deselect-label="{{$lang['field-required']}}"
				            :allow-empty="false"></multiselect>
				            <p class="help-block" v-if="form.errors.has('province')" v-text="form.errors.get('province')"></p>
						</div>
			        </div><!-- end of col-lg-6 -->

					<div class="col-lg-6">
						<div class="form-group" :class="{ 'has-error': form.errors.has('city') }">
							<multiselect class="custom-select" 
					        :options="optionCity" 
					        :allow-empty="false"
					        :selected="form.city"
					        placeholder="{{$lang['city']}}"  
					        label="name"
					        track-by="name"
					        v-model="city"
					       	deselect-label="{{$lang['field-required']}}"
					        ></multiselect>		
					        <p class="help-block" v-if="form.errors.has('city')" v-text="form.errors.get('city')"></p>		
						</div>
			        </div><!-- end of col-lg-6 -->


	    		</div>
	    		<div class='row col-md-12'>
		    		<div class="col-md-12">
						<div class="form-group">
						  <label class="control-label" for="landmark">{{$lang['nearest-landmark']}}</label>
						  <textarea v-model="form.landmark" name="" id="landmark" required="" rows="1" class='form-control' autofocus></textarea>
						</div>
					</div>
	    		</div>
	    		<div class='row col-md-12'>
					<div class="col-md-12">
						<div class="form-group">
						  <label class="control-label" for="notes">{{$lang['notes']}}</label>
						  <textarea v-model="form.notes" name="" id="notes" required="" rows="2" class='form-control' autofocus></textarea>
						</div>
					</div>
	    		</div>

	    		<div class='row col-md-12'>
					<div class="col-md-5">
						<div class="form-group">
							<label class="control-label" for="alternate_number">{{$lang['alternate-contact-number']}}</label>
							<input v-model="form.alternate_number" class="form-control" id="alternate_number" type="text" maxlength="11" autofocus>
						</div>
					</div>
					<input type="hidden" name="total" v-model="form.total">
					<input type="hidden" name="discount" v-model="form.discount">
					<input type="hidden" name="subtotal" v-model="form.discountedPrice">
					<input type="hidden" name="tax" v-model="form.tax">					
					<input type="hidden" name="shipping" v-model="form.shipping">
					<input type="hidden" name="charge" v-model="form.charge">
					<input type="hidden" name="type_id" v-model="form.type_id">
	    		</div>
	    		<div class='row col-md-12'>
	    			<div class='col-md-6'>
	    				<h3 class="mar-step2"><small><b>{{$lang['purchase-summary']}}</b></small></h3>
	    			</div>
		    	</div>
		    	<div class='row col-md-12'>
			    	<div class='checkout-steps-container checkout-step-summary'>
						<div class="table-responsive">
							<table class='table table-hover'>
								<thead>
									<tr>
										<th>{{$lang['item']}}</th>
										<th class="qty">{{$lang['qty']}}</th>
										<th>{{$lang['price']}}</th>
										<th class="hide">{{$lang['shipping-fee']}}</th>
										<th>{{$lang['subtotal']}}</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for='item in selected' :key='item.id'>	
										<td v-cloak>@{{item.name}} <span v-show="item.options.color != '' ">(@{{item.options.color}})</span> <span v-show="item.options.size != '' ">(@{{item.options.size}})</span></td>
										<td v-cloak>@{{item.qty}}</td>
										<td v-cloak>@{{(item.options.sale_price).replace(',', '') | currency}}</td>
										<td class="hide" v-cloak>@{{item.qty * item.options.shipping_fee + item.qty * item.options.charge + item.qty * item.options.vat | currency}}</td>
										<td v-cloak>@{{item.qty * (item.options.sale_price).replace(',', '') + item.qty * item.options.shipping_fee + item.qty * item.options.charge + item.qty * item.options.vat | currency}}</td>
									</tr>
								</tbody>
							</table>
						</div>
						<table id="total" class="float">
						  <tr v-if="discounts!=0">
						    <td>{{$lang['sub-total']}} :</td>
						    <td>@{{form.total | currency}}</td>
						  </tr>
						  <tr v-if="discounts!=0">
						    <td>{{$lang['discount']}} :</td>
						    <td>@{{form.discount | currency}}</td>
						  </tr>
						  <tr>
						    <td>{{$lang['total']}} :</td>
						    <td>@{{form.discountedPrice | currency}}</td>
						  </tr>
						</table>
			    	</div>
		    	</div>
    		</div>
    	</div>
    	<div id="print" class='checkout-steps-container checkout-step-finish'  v-show='current_step == 2'>
			<div>{{$lang['order']}} #: @{{order_id}}</div>
			<br>
			<div>@{{name}}</div>
			<div>@{{address}}</div>
			<br>
			<div class="table-responsive">
				<table class='table table-hover'>
					<thead>
						<tr>
							<th>{{$lang['item']}} </th>
							<th class="qty">{{$lang['qty']}} </th>
							<th>{{$lang['price']}} </th>
							<th class="hide">{{$lang['shipping-fee']}}</th>
							<th>{{$lang['subtotal']}} </th>
						</tr>
					</thead>
					<tbody>
						<tr v-for='order in orders' :key='order.id'>	
							<td>@{{order.name}} <span v-show="order.options.color != '' ">(@{{order.options.color}})</span> <span v-show="order.options.size != '' ">(@{{order.options.size}})</span></td>
							<td>@{{order.qty}}</td>
							<td>@{{(order.options.sale_price).replace(',', '') | currency}}</td>
							<td class="hide">@{{order.qty * order.options.shipping_fee + order.qty * order.options.charge + order.qty * order.options.vat | currency}}</td>
							<td>@{{order.qty * (order.options.sale_price).replace(',', '') + order.qty * order.options.shipping_fee + order.qty * order.options.charge + order.qty * order.options.vat | currency}}</td>
						</tr>
					</tbody>
			    </table>
			</div>
			<table id="total" class="float">
			  <tr v-if="discount!=0">
			    <td>{{$lang['sub-total']}} :</td>
			    <td>@{{total | currency}}</td>
			  </tr>
			  <tr v-if="discount!=0">
			    <td>{{$lang['discount']}} :</td>
			    <td>@{{discount | currency}}</td>
			  </tr>
			  <tr>
			    <td>{{$lang['total']}} :</td>
			    <td>@{{discountedPrice | currency}}</td>
			  </tr>
			</table>
			<br>
			<br>
			<br>
			<div class="e-wallet">
				<p class="titles">{{$lang['e-wallet-balance']}}</p>
				<p class="balance">@{{ewallet | currency}}</p>
			</div>
    	</div>
    </template>
    <template slot="modal-footer">
	     <div class='col-md-12' v-show='current_step == 1'>
			<button id="placeOrder" class='btn btn-has-loading btn-raised btn-success ' @click='placeOrder()'> {{$lang['place-order']}}</button>
	    </div>
	     <div class='col-md-12' v-show='current_step == 2'>
	     	<button class='btn btn-raised btn-info ' @click='print()'> {{$lang['print']}}</button>
			<button class='btn btn-default' data-dismiss="modal" aria-label="Close" @click="reset()">{{$lang['close']}}</button>
	    </div>
    </template>
</modal>