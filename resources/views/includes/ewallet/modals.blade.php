<!-- Instructions Modal -->
<modal size="modal-md" id='modalInstructions'>
    <template slot="modal-title">
        <h3>
            {{$lang['instructions']}}
        </h3>
    </template>
    <template slot="modal-body">
        <div class=''>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-xs-12 col-lg-5 col-md-5 col-sm-5 text-center">
                        <img class='img-responsive' src='/shopping/images/sample-deposit-slip.jpg' align="center"> 
                    </div>
                    <div class="col-xs-12 col-lg-7 col-md-7 col-sm-7">
                        <ul>
                            <li style="margin-bottom: 15px;">{{$lang['depslip-ins-1']}}</li>
                            <li style="margin-bottom: 15px;">{{$lang['depslip-ins-2']}}</li>
                            <li style="margin-bottom: 15px;">{{$lang['depslip-ins-3']}}</li>
                            <li style="margin-bottom: 15px;">{{$lang['depslip-ins-4']}}</li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
    </template>
</modal>

<!-- Upload Modal -->
<modal size="modal-md" id='modalDepositUpload'>
    <template slot="modal-title">
        <h3>
            {{$lang['upload-deposit-slip']}}
        </h3>
    </template>
    <template slot="modal-body">
        <div class='container-fluid'>
            <div class="col-xs-12 text-center">
                <div id="doc-container">
                </div>
                <div class="range-slider" v-show='showZoomable'>
                  <input class="range-slider__range" type="range"  step='0.1' v-model='baseZoom' min="1" max="5">
                </div>
                <form action='/user/ewallet/upload-deposit' enctype='multipart/form-data' method="post">
                    <div class="deposit-uploader hide">
                        <input type="file" name='image' id='img_upload' @change='fileChange' @click='fileChange' required>
                        <input name='id' v-model="current_transaction" hidden>
                    </div>
                        {{Form::token() }}
                        <button class='btn btn-lg btn-raised btn-warning' id='uploadDepositSlip' type="button">{{$lang['upload']}}</button>
                        <button class='btn btn-lg btn-raised btn-info' id='' @click='checkSubmitPic'>

                            {{$lang['submit']}}
                        </button>
                    <span v-if='current_transaction_status != "Cancelled"'>
                    </span>
                </form>
            </div>
            <div class='col-xs-12 text-center' v-if='current_transaction_status == "Cancelled"'>
                "@{{current_transaction_reason}}"
            </div>
        </div>
    </template>
    <template slot="modal-footer">
        
    </template>
</modal>

<!-- Load Modal -->
<modal size="modal-md" id='modalLoad'>
    <template slot="modal-title">
        <h3>
            <button class='btn btn-default btn-link btn-info ' v-show='showLoadingBack' @click='resetModals'>
                <span class='material-icons'>arrow_back</span>
            </button> 
            {{$lang['load-money']}}
        </h3>
    </template>
    <template slot="modal-body">
        <div class='container-fluid'>
            <div class='row ' id='selectionLoad' v-show='showLoadingOptions'>
                <div class='col-md-6'>
                    <div class='card text-center' @click='chooseOnline'>
                        <i class="fa fa-paypal"></i>
                        <h4><b>{{$lang['online']}}</b></h4> 
                    </div>
                </div>
                <div class='col-md-6'>
                    <div class='card text-center'  @click='chooseOffline'>
                        <i class="fa fa-money"></i>
                        <h4><b>{{$lang['offline']}}</b></h4>
                    </div>
                </div>
                <div class='col-md-offset-6 col-md-6 text-right'>
                    <button class='btn btn-flat btn-default '  data-dismiss="modal" aria-label="Close">
                        {{$lang['close']}}
                    </button>
                </div>
            </div>
           
            <div class='row' id='onlineLoadingArea'  v-show='showOnlineArea'>
                <div class='col-md-12'>
                    <h2><small>
                        {{$lang['load-online-soon']}}
                    </small></h2>
                   
                </div>
                <div class='col-md-offset-6 col-md-6 text-right'>
                    <button class='btn btn-flat btn-default'  data-dismiss="modal" aria-label="Close">
                        {{$lang['close']}}
                    </button>
                </div>
            </div>
            <div class='row' id='offlineLoadingArea'  v-show='showOfflineArea'>
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'  :class="{ 'has-error': loadOfflineForm.errors.has('bank_id') }">
                        <label class='col-lg-2 text-right control-label'>{{$lang['bank']}}</label>
                        <div class='col-lg-10'>
                            <select class='form-control global-label' v-model='loadOfflineForm.bank_id'>
                                <option value="" class="small-text">{{$lang['choose-bank']}}</option>
                                <option 
                                    :key='bank.id'
                                    v-for='bank in banks' 
                                    :value='bank.id'
                                    v-text='bank.name_number'
                                    class="small-text"
                                >
                                </option>
                            </select>
                            <span class="help-block" v-if="loadOfflineForm.errors.has('bank_id')" v-text="loadOfflineForm.errors.get('bank_id')"></span>
                        </div>
                    </div>
                </div>
               <!--  <div class="col-lg-12 col-md-12">
                    <div class='form-group'  :class="{ 'has-error': loadOfflineForm.errors.has('bank_id') }">
                        <label class='col-lg-2 text-right control-label'>Account</label>
                        <div class='col-lg-10'>
                            {!! 
                                Form::text('txt-mobile','This account\'s Email', [
                                     'class' => 'form-control '
                                    ,'disabled' => 'true'
                                ]);
                            !!}
                            <span class="help-block" v-if="loadOfflineForm.errors.has('bank_id')" v-text="loadOfflineForm.errors.get('bank_id')"></span>
                        </div>
                    </div>
                </div> -->
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'  :class="{ 'has-error': loadOfflineForm.errors.has('amount') }">
                        <label class='col-lg-2 text-right control-label'>{{$lang['amount']}}</label>
                        <div class='col-lg-10'>
                            {!! 
                                Form::number('amount','', [
                                     'class'        => 'form-control '
                                    ,'placeholder'  => '(PHP) 0.00'
                                    ,'v-model'      => 'loadOfflineForm.amount'
                                ]);
                            !!}
                            <span class="help-block" v-if="loadOfflineForm.errors.has('amount')" v-text="loadOfflineForm.errors.get('amount')"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'>
                        <div class='col-md-offset-6 col-md-6 text-right'>
                            <button class='btn btn-default'  data-dismiss="modal" aria-label="Close">
                                {{$lang['close']}}
                            </button>
                            <button class='btn btn-has-loading  btn-success btn-raised'  @click='submitLoadOffline'>
                                {{$lang['submit']}}
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <a data-toggle='modal' data-target='#modalInstructions'>{{$lang['see-instructions']}}</a>
                    </div>
                </div>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
    </template>
</modal>
<!-- end of Load Modal -->


<!-- Withdraw Modal -->
<modal size="modal-md" id='modalWithdraw'>
    <template slot="modal-title">
        <h3>{{$lang['withdraw-funds']}}</h3>
    </template>
    <template slot="modal-body">
        <div class='container-fluid'>
            <div class='row'  >
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'  :class="{ 'has-error': withdrawOfflineForm.errors.has('bank_id') }">
                        <label class='col-lg-2 text-right control-label'>{{$lang['bank-account']}}</label>
                        <div class='col-lg-10'>
                            <select class='form-control global-label' v-model='withdrawOfflineForm.bank_id'>
                                <option value="" class="small-text">{{$lang['choose-bank']}}</option>
                                <option 
                                    class="small-text"
                                   :key='bank_account.id'
                                    v-for='bank_account in bank_accounts' 
                                    :value='bank_account.id'
                                    v-text='bank_account.account_name + " ("+bank_account.account_number+" : "+bank_account.bank.shortname +")"'
                                    
                                >
                                </option>
                            </select>
                            <span class="help-block" v-if="withdrawOfflineForm.errors.has('bank_id')" v-text="withdrawOfflineForm.errors.get('bank_id')"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'  :class="{ 'has-error': withdrawOfflineForm.errors.has('amount') }">
                        <label class='col-lg-2 text-right control-label'>{{$lang['amount']}}</label>
                        <div class='col-lg-10'>
                            {!! 
                                Form::number('amount','', [
                                     'class' => 'form-control '
                                    ,'placeholder' => '(PHP) 0.00'
                                    ,'v-model' => 'withdrawOfflineForm.amount'
                                ]);
                            !!}
                            <span class="help-block" v-if="withdrawOfflineForm.errors.has('amount')" v-text="withdrawOfflineForm.errors.get('amount')"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-offset-6 col-md-6 text-right'>
                <button class='btn btn-default'  data-dismiss="modal" aria-label="Close">
                    {{$lang['close']}}
                </button>
                <button class='btn btn-success btn-raised btn-has-loading' @click='submitWithdrawOffline'  >
                    {{$lang['submit']}}
                </button>
            </div>
        </div>
        
    </template>
    <template slot="modal-footer">
        
    </template>
</modal>
<!-- end of Withdraw Modal -->



<!-- Transfer Receive Modal -->
<modal size="modal-md" id='modalTransferReceive'>
    <template slot="modal-title">
        <h3>
            <button class='btn btn-default btn-link btn-info' v-show='showTransferReceiveBack' @click='resetModals' style="padding: 0px !important;">
                <span class='material-icons'>arrow_back</span>
            </button> 
            {{$lang['transfer-or-receive']}}
        </h3>
    </template>

    <template slot="modal-body">
        <div class='container-fluid'>
            <div class='row ' id='' v-show='showTransferReceiveOptions'>
                <div class='col-md-6' v-if="usable_balance != 0">
                    <div class='card text-center' @click='chooseTransfer'>
                        <i class="fa fa-long-arrow-right"></i>
                        <h4><b>{{$lang['transfer']}}</b></h4> 
                    </div>
                </div>
                <div :class="usable_balance != 0? 'col-lg-6':'col-lg-12'">
                    <div class='card text-center'  @click='chooseReceive'>
                        <i class="fa fa-paper-plane-o"></i>
                        <h4><b>{{$lang['receive']}}</b></h4> 
                    </div>
                </div>
                <div class='col-md-offset-6 col-md-6 text-right'>
                    <button class='btn btn-flat btn-default'  data-dismiss="modal" aria-label="Close">
                        {{$lang['close']}}
                    </button>
                </div>
            </div>
            <div class="row" v-show='showReceiveArea'>
                <div class='row form-group'>
                    <div class="col-md-3">
                        {!!
                            Form::label('lbl-mobile', $lang['code'], [
                                'class' => 'global-label'
                            ]);
                        !!}
                    </div>
                    <div class="col-md-9 account-input-right">
                        {!! 
                            Form::text('transfer_to',null, [
                                 'class' => 'form-control global-textbox'
                                ,'placeholder' => $lang['ph-code']
                                ,'v-model' => 'transferCode'
                            ]);
                        !!}
                    </div><!-- end col-md-9 -->
                </div>
                <div class='row'>
                    <div class='col-md-offset-6 col-md-6 text-right'>
                        <button class='btn btn-flat btn-default'  data-dismiss="modal" aria-label="Close">
                            {{$lang['close']}}
                        </button>
                        <button class='btn btn-success'  @click='submitReceive'>
                            {{$lang['submit']}}
                        </button>
                    </div>
                </div>
            </div>
            <div class='row'   v-show='showTransferArea'>
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'  :class="{ 'has-error': transferForm.errors.has('bank_id') }">
                        <label class='col-lg-2 text-right control-label'>{{$lang['receiver']}}</label>
                        <div class='col-lg-10'>
                            <multiselect 
                                class="receiver_multiselect"
                                v-model="transferTo" 
                                id="ajax" 
                                label="name"
                                :custom-label="nameWithEmail" 
                                track-by="id" 
                                placeholder="{{$lang['ph-email']}}" 
                                :options="fetchedAccounts" 
                                :multiple="true" 
                                :searchable="true" 
                                :loading="isFetchingAccounts" 
                                :internal-search="true" 
                                :clear-on-select="true" 
                                :close-on-select="true" 
                                :options-limit="100" 
                                :limit="4" 
                                :limit-text="limitText" 
                                @search-change="fetchAccountsByEmail">
                                <span slot="noResult">{{$lang['no-user-found']}}</span>
                            </multiselect>
                            <small><i>{{$lang['ph-email-help']}}</i></small>
                            <span class="help-block" v-if="transferForm.errors.has('bank_id')" v-text="transferForm.errors.get('bank_id')"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'  :class="{ 'has-error': transferForm.errors.has('amount') }">
                        <label class='col-lg-2 text-right control-label'>{{$lang['amount']}}</label>
                        <div class='col-lg-10'>
                            {!! 
                                Form::number('amount','', [
                                     'class' => 'form-control '
                                    ,'placeholder' => '(PHP) 0.00'
                                    ,'v-model' => 'transferForm.amount'
                                ]);
                            !!}
                            <span class="help-block" v-if="transferForm.errors.has('amount')" v-text="transferForm.errors.get('amount')"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'  :class="{ 'has-error': transferForm.errors.has('note') }">
                        <label class='col-lg-2 text-right control-label'>{{$lang['notes']}}</label>
                        <div class='col-lg-10'>
                            {!! 
                                Form::text('note','', [
                                     'class' => 'form-control '
                                    ,'placeholder' => $lang['leave-msg']
                                    ,'v-model' => 'transferForm.note'
                                ]);
                            !!}
                            <span class="help-block">*{{$lang['leave-msg']}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class='form-group'>
                        <div class='col-md-offset-6 col-md-6 text-right'>
                            <button class='btn btn-default'  data-dismiss="modal" aria-label="Close">
                                {{$lang['close']}}
                            </button>
                            <button class='btn btn-has-loading btn-raised btn-success' id='btnTransferSubmit' @click='submitTransfer' >
                                {{$lang['submit']}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </template>
    <template slot="modal-footer">
    </template>
</modal>
<!-- end of transfer /receive Modal -->


<!-- Withdraw Modal -->
<modal size="modal-lg" id='modalAddBank'>
    <template slot="modal-title">
        <h3>{{$lang['bank-account']}}</h3>
    </template>
    <template slot="modal-body">
        <div class='container-fluid'>
            <div class='row' id='' >
                <div :class="bank_accounts.length? 'col-lg-6':'col-lg-3'" class="table-responsive">
                    <table class='table table-striped'  v-if='bank_accounts.length'>
                        <thead>
                            <tr>
                                <th>{{$lang['bank']}}</th>
                                <th>{{$lang['account-name']}}</th>
                                <th>{{$lang['account-no']}}</th>
                                <th>{{$lang['status']}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if='!bank_accounts'><td colspan="4">{{$lang['msg-no-account']}}</td></tr>
                            <tr 
                                @click='showBank(bank_account.id)' 
                                v-for='bank_account in bank_accounts' 
                                :key='bank_account.id'
                                style= "cursor:pointer;"
                            >
                                <td v-cloak>@{{bank_account.bank.shortname}}</td>
                                <td v-cloak>@{{bank_account.account_name}}</td>
                                <td v-cloak>@{{bank_account.account_number}}</td>
                                <td v-cloak>@{{bank_account.status}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div :class="bank_accounts.length? 'col-lg-6':'col-lg-12'">
                    <div class="row">
                        <div class="col-xs-12">
                            <button data-toggle='tooltip' title='{{$lang["add-bank-account"]}}' v-if='!bankAccountMode' @click='makeBankAccountModeAdd' class='btn btn-small pull-right' style="background-color: rgba(153, 153, 153, 0.2) !important; color: rgba(0,0,0, 0.87) !important; border-color: #adadad !important;">
                                + Add Bank Account
                            </button>  
                        </div>
                        <div class="col-xs-12">
                            <h4 class="bank-subheader">
                                {{bankAccountMode?'<?=$lang['add']?>':'<?=$lang['edit']?>'}} {{$lang['your-bank-account']}}
                            </h4>
                        </div>  
                    </div>
                    <div class='col-lg-12 col-md-12' >
                        <div class="form-group" :class="{ 'has-error': bankAccountForm.errors.has('bank_id') }">
                            <label for="inputEmail" class="col-md-3 control-label">{{$lang['bank']}}</label>
                            <div class="col-md-9">
                                <select v-model="bankAccountForm.bank_id" class='form-control global-label' id='bank' name='bank'>
                                    <option value="" class="small-text">{{$lang['choose-bank']}}</option>
                                    <option 
                                        :key='bank.id'
                                        v-for='bank in banks' 
                                        :value='bank.id'
                                        v-text='bank.name'
                                        class="small-text"
                                    ></option>
                                </select>
                                 <span class="help-block" v-if="bankAccountForm.errors.has('bank_id')" v-text="bankAccountForm.errors.get('bank_id')"></span>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-12 col-md-12' >
                        <div class="form-group" :class="{ 'has-error': bankAccountForm.errors.has('account_name') }">
                            <label for="inputEmail" class="col-md-3 control-label">{{$lang['account-name']}}</label>
                            <div class="col-md-9">
                                {!! 
                                    Form::text('account_name','', [
                                         'class'        => 'form-control'
                                        ,'placeholder'  => $lang['account-name']
                                        ,'id'           => 'account_name'
                                        ,'v-model'      => 'bankAccountForm.account_name'
                                    ]);
                                !!}
                                 <span class="help-block" v-if="bankAccountForm.errors.has('account_name')" v-text="bankAccountForm.errors.get('account_name')"></span>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-12 col-md-12' >
                        <div class="form-group" :class="{ 'has-error': bankAccountForm.errors.has('account_number') }">
                            <label for="inputEmail" class="col-md-3 control-label">{{$lang['account-no']}}</label>
                            <div class="col-md-9">
                                {!! 
                                    Form::number('account_number','', [
                                         'class'        => 'form-control'
                                        ,'placeholder'  => $lang['account-no']
                                        ,'id'           => 'account_number'
                                        ,'v-model'      => 'bankAccountForm.account_number'
                                    ]);
                                !!}
                                 <span class="help-block" v-if="bankAccountForm.errors.has('account_number')" v-text="bankAccountForm.errors.get('account_number')"></span>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-12 col-md-12' >
                        <div class="form-group" :class="{ 'has-error': bankAccountForm.errors.has('reaccount_number') }">
                            <label for="inputEmail" class="col-md-3 control-label">{{$lang['reenter-account-no']}}</label>
                            <div class="col-md-9">
                                {!! 
                                    Form::number('reaccount_number','', [
                                         'class'        => 'form-control'
                                        ,'placeholder'  => $lang['reenter-account-no']
                                        ,'id'           => 'reaccount_number'
                                        ,'v-model'      => 'bankAccountForm.reaccount_number'
                                    ]);
                                !!}
                                 <span class="help-block" v-if="bankAccountForm.errors.has('reaccount_number')" v-text="bankAccountForm.errors.get('reaccount_number')"></span>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-12 col-md-12 text-right'>
                        <div class='form-group'>
                            <button class='btn btn-default'  data-dismiss="modal" aria-label="Close">
                                {{$lang['close']}}
                            </button>

                            <button 
                                class='btn btn-raised btn-has-loading btn-success'
                                @click='submitNewBankAccount'
                                v-if='bankAccountMode'
                            >
                                {{$lang['submit']}}
                            </button>
                            <button 
                                class='btn btn-raised btn-has-loading btn-warning'
                                @click='updateBankAccount'
                                v-if='!bankAccountMode'
                            >
                                {{$lang['update']}}
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </template>
    <template slot="modal-footer">
    </template>
</modal>
<!-- end of Withdraw Modal -->
