<div class="col-sm-12 store-input-set">
	<div class="store-subhead">
		<h4 class="sub-header">STORE DETAILS</h4>
		<hr>
	</div>
	<div class="col-sm-6">
		<div class="form-group label-floating">
		  	<label class="control-label" for="txt-store-name-en">Store Name
			  	<span>
					<img src="/shopping/img/general/flag/en.jpg">
				</span>
			</label>
		  <input class="form-control" id="txt-store-name-en" type="text" required="required" v-model="storeNameEn">
		  <p class="help-block">Your Store Name in English</p>
		</div>
	</div>

	<div class="col-sm-6">
		<div class="form-group label-floating">
		  	<label class="control-label" for="txt-store-name-cn">Store Name
			  	<span>
					<img src="/shopping/img/general/flag/cn.jpg">
				</span>
			</label>
		  	<input class="form-control" id="txt-store-name-cn" type="text" v-model="storeNameCn">
		  	<p class="help-block">Your Store Name in Chinese</p>
		</div>
	</div>
</div><!-- end store-input-set -->


<div class="col-sm-12 store-input-set">
	<div class="col-sm-6">
		<multiselect class="custom-select" :options="optionCat" v-model="valueCat" :multiple="true" :max="4" :close-on-select="false" :clear-on-select="false" :allow-empty="false" :hide-selected="true" placeholder="Select Category" label="name" track-by="name"></multiselect>
		<p class="multiselect-note">You need to choose at least one category and maximum of 4.</p>
	</div><!-- end col-sm-6 -->
	<div class="col-sm-6">
		<div class="form-group label-floating">
		  <label class="control-label" for="txt-custom-category">Custom Category</label>
		  <input class="form-control" id="txt-custom-category" type="text">
		  <p class="help-block">Can't find the right category? Try asking for it.</p>
		</div>
	</div><!-- end col-sm-6 -->
</div><!-- end store-input-set -->


<div class="col-sm-12 store-input-set">
	<div class="col-sm-12">
		<div class="form-group label-floating">
			<label class="control-label" for="txt-desc">Store Description</label>
	        <textarea class="form-control" rows="3" id="txt-desc" v-model="desc"></textarea>
	        <span class="help-block">Tell something about your store</span>
      	</div>
	</div>
</div><!-- end store-input-set -->

<div class="col-sm-12 store-input-set">
	<div class="store-subhead">
		<h4 class="sub-header">CONTACT DETAILS</h4>
		<hr>
	</div>
	<div class="col-sm-6">
		<multiselect class="custom-select" :options="optionProv" v-model="valueProv" placeholder="Province" label="name" track-by="name"></multiselect>
	</div>

	<div class="col-sm-6">
		<multiselect class="custom-select" :options="optionCity" v-model="valueCity" placeholder="City " label="name" track-by="name"></multiselect>
		</div>
</div><!-- end store-input-set -->

<div class="col-sm-12 store-input-set">
	<div class="col-sm-12">
		<div class="form-group label-floating">
			<label class="control-label" for="txt-address">Complete Address</label>
	        <input class="form-control" id="txt-address" type="text">
<!-- 	        <span class="help-block">Tell something about your store</span>
 -->    </div>
	</div>
</div><!-- end store-input-set -->


<div class="col-sm-12 store-input-set">
	<div class="col-sm-4">
		<div class="form-group label-floating">
			<label class="control-label" for="txt-mobile">Mobile No.</label>
	        <input class="form-control cell" id="txt-mobile" v-model="mobile" type="text" required = "">
<!-- 	        <span class="help-block">Tell something about your store</span>
 -->    </div>
	</div>

	<div class="col-sm-4">
		<div class="form-group label-floating">
			<label class="control-label" for="txt-email">Email</label>
	        <input class="form-control" id="txt-email" v-model="email" type="text" required = "">
<!-- 	        <span class="help-block">Tell something about your store</span>
 -->    </div>
	</div>

	<div class="col-sm-4">
		<div class="form-group label-floating">
			<label class="control-label" for="txt-facebook">Facebook</label>
	        <input class="form-control" id="txt-facebook" type="text" required = "">
<!-- 	        <span class="help-block">Tell something about your store</span>
 -->    </div>
	</div>
</div><!-- end store-input-set -->

