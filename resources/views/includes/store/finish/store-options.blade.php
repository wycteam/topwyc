<div class="col-sm-12 store-options">
	<div class="store-text-header-container">
	   <h4 class="text-header">WHAT DO YOU WANT TO DO NOW?</h4>
	 </div><!-- end about-header-container-->

	<div class="col-sm-12 store-finish-buttons">
		<div class="col-sm-3">
		    <a href="{{URL::Route('home')}}">
		        <img src="/shopping/img/store/finish/house.png">
		        <h4>Go back Home</h4>
		    </a>
		</div><!-- end col-sm-3 home-->

		<div class="col-sm-3">
		    <a href="{{URL::Route('store-management')}}">
		        <img src="/shopping/img/store/finish/online-shop.png">
		        <h4>Store Management</h4>
		    </a>
		</div><!-- end col-sm-3 Store Management-->

		<div class="col-sm-3">
		    <a href="">
		        <img src="/shopping/img/store/finish/warehouse.png">
		        <h4>Add Product</h4>
		    </a>
		</div><!-- end col-sm-3 Add Product-->

		<div class="col-sm-3">
		    <a href="">
		        <img src="/shopping/img/store/finish/presentation.png">
		        <h4>Marketing Center</h4>
		    </a>
		</div><!-- end col-sm-3 Marketing-->

	</div><!-- end col-sm-12-->
</div><!-- end  store-options -->