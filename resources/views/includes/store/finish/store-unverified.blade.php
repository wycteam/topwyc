<div class="col-sm-12 store-options">
	<div class="store-text-header-container red">  
	   <h4 class="text-header">YOUR ACCOUNT IS NOT VERIFIED</h4>
	 </div><!-- end about-header-container-->

	<div class="col-sm-12">
		<div class="ver-subhead">
			<h4>
				Please submit your personal or enterprise identification documents to verify your account in profile management page.<br> Otherwise, the following functions in your store is locked: 
			</h4>
		</div>
		<div class="ver-notes">
			<ol>
				<li v-for="item in conditions" v-text="item"><li>
			</ol>
		</div>
		<div class="home-btn-set">
			<a href="{{URL::Route('home')}}" class ='btn btn-tangerine btn-raised'>Go Back Home</a>
		</div>
	</div><!-- end col-sm-12-->
</div><!-- end  store-options -->