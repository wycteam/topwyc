              <div class="col-sm-12">
                <div class="col-sm-3">
                  <div class="card store-badge">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="col-md-4 text-center vertical-center">
                          <img src="/shopping/img/store/store-badge-icons/shopping-bag.png" class="store-badge-icon">
                        </div><!-- end col-md-4 -->
                        <div class="col-md-8">
                          <div class="card-container">
                            <h4>
                              <b>6</b>
                            </h4>
                            <p>Products</p>
                          </div><!-- end card-container -->
                        </div><!-- end col-md-8 -->
                      </div><!-- end col-sm-12 -->
                    </div><!-- end row -->
                  </div><!-- end store-badge -->
                </div><!-- end col-sm-3 -->

                <div class="col-sm-3">
                  <div class="card store-badge">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="col-md-4 text-center vertical-center">
                          <img src="/shopping/img/store/store-badge-icons/like.png" class="store-badge-icon">
                        </div><!-- end col-md-4 -->
                        <div class="col-md-8">
                          <div class="card-container">
                            <h4>
                              <strong><span>4.3</span>/5</strong>
                            </h4>
                            <p>Ratings</p>
                          </div><!-- end card-container -->
                        </div><!-- end col-md-8 -->
                      </div><!-- end col-sm-12 -->
                    </div><!-- end row -->
                  </div><!-- end store-badge -->
                </div><!-- end col-sm-3 -->

                <div class="col-sm-3">
                  <div class="card store-badge">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="col-md-4 text-center vertical-center">
                          <img src="/shopping/img/store/store-badge-icons/group.png" class="store-badge-icon">
                        </div><!-- end col-md-4 -->
                        <div class="col-md-8">
                          <div class="card-container">
                            <h4>
                              <b>1253</b>
                            </h4>
                            <p>Followers</p>
                          </div><!-- end card-container -->
                        </div><!-- end col-md-8 -->
                      </div><!-- end col-sm-12 -->
                    </div><!-- end row -->
                  </div><!-- end store-badge -->
                </div><!-- end col-sm-3 -->

                <div class="col-sm-3">
                  <div class="card store-badge">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="col-md-4 text-center vertical-center">
                          <img src="/shopping/img/store/store-badge-icons/user-heart.png" class="store-badge-icon">
                        </div><!-- end col-md-4 -->
                        <div class="col-md-8">
                          <div class="card-container">
                            <h4>
                              <b>5</b>
                            </h4>
                            <p>Following</p>
                          </div><!-- end card-container -->
                        </div><!-- end col-md-8 -->
                      </div><!-- end col-sm-12 -->
                    </div><!-- end row -->
                  </div><!-- end store-badge -->
                </div><!-- end col-sm-3 -->
              </div><!-- end col-sm-12 -->