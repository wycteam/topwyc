<div class="col-sm-12 store-input-set">
	<div class="store-subhead">
		<h4 class="sub-header">UPLOAD PICTURES</h4>
		<hr>
	</div>
	<div class="alert alert-success alert-set" role="alert">
        This serves as a preview of your store.
   	</div>
	<div class="big-card hovercard">
        <div class="cardheader">
        	<div class="cover-uploader">
        		Edit Cover Photo
	        	<input type="file">
        	</div>
        </div>
        <div class="avatar">
            <img alt="" src="/shopping/img/sample pictures/sampledp-redgirl.jpg">
            <div class="avatar-uploader">
            	<div class="edit">Edit</div>
	            <input type="file">
            </div>
        </div>
      
        <div class="info">
  			<div class="col-sm-12 store-info-set">
	            <div class="col-sm-3 store-info">
	              <div class="title">
	                <a target="_blank" href="#">
	                  <h3>@{{storeNameEn}}</h3>
	                </a>
	                <a target="_blank" href="#">
	                  <h4>@{{storeNameCn}}</h4>
	                </a>
	              </div><!-- end title -->
	              <div class="title">
	                <div class="desc">Store Owner</div>
	                <div class="desc">@{{valueCity.text}}</div>
	                <div class="desc">@{{mobile}}</div>
	                <div class="desc">@{{email}}</div>
	                <div class="desc">Joined since <span id="joined-year"></span></div>
	              </div><!-- end title -->
	              <div class="title">
                <p class="desc">@{{desc}}</p>
	              </div><!-- end title -->
	              <div class="col-sm-12">
	                <a href="javascript:void(0)" class="btn btn-raised btn-block btn-tangerine btn-sm"><i class="material-icons">&#xE145;</i>&nbsp;&nbsp;Follow</a>
	                <a href="javascript:void(0)" class="btn btn-raised btn-block btn-tangerine btn-sm"><i class="material-icons">&#xE0C9;</i>&nbsp;&nbsp;Chat</a>
	              </div>
	            </div><!-- end store-info -->

            <div class="col-sm-9">
              <!-- BADGES -->
              <div class="row">
                @include('includes.store.store-page.badges')
              </div><!-- end row -->

              <!--  PRODUCTS -->
              <div class="row">
              
              </div><!-- end row-->
            </div><!-- end col-sm-10 -->
          </div>
        </div>
    </div>
</div><!-- end store-input-set -->