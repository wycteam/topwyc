<!-- Product Order Modal -->
<modal size="modal-lg" class="airway-product-modal" id='viewAirwayBill'>
    <template slot="modal-title">
            
    </template><!-- end of modal-title -->

    <template slot="modal-body" class="product-details-set">
    	<div id="print" class='product-card'>
    		<div class="col-lg-12 product-set">
    			<div class="row">
    				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
	    				<div class="deliver">{{$lang['deliver-to']}}</div>
					    <div>@{{orderDetails.order_receiver}}</div>
					    <div>@{{orderDetails.full_address}}</div>
					    <br>
					    <div>@{{orderDetails.city}} , @{{orderDetails.province}} , @{{orderDetails.zip_code}}</div>
					    <div>T: @{{orderDetails.order_contact}}</div>
					    <div>{{$lang['area-code']}}: <b>@{{orderDetails.province}}</b></div>
					    <!-- <div><b>{{$lang['collected']}}:</b> @{{orderDetails.subtotal}} PHP</div> -->
	    			</div>
	    			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
	    			</div>
	    			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		    			<div><b>{{$lang['awb']}} :</b> @{{orderDetails.awb_id}}</div>
		    			<div><b>{{$lang['weight']}} (kgs) :</b> @{{weight | currency}}</b></div>
		    			<div><b>{{$lang['order-date']}} :</b> @{{orderDetails.created_at}}</div>
		    			<div><b>{{$lang['pieces']}} :</b> @{{orderDetails.quantity}}</div>
		    			<div><b>{{$lang['package-type']}} :</b> @{{orderDetails.product_package_type}}</div>
	    			</div>		
    			</div>
    			<div class="row">
    				<div class="table-responsive col-lg-12">
	    				<br>    			
						<table>
							<thead>
								<tr>
									<th class="text-align">{{$lang['sr']}}</th>
									<th class="padding-left">{{$lang['item-code']}}</th>
									<th class="padding-left">{{$lang['item-desc']}}n</th>
									<th class="text-align">{{$lang['quantity']}}</th>
									<th class="text-align">{{$lang['value']}}</th>
									<th class="text-align">{{$lang['shipping-fee']}}</th>
									<th class="text-align">{{$lang['total']}}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								    <td class="b text-align">1</td>
								    <td class="padding-left">@{{orderDetails.product_code}}-@{{orderDetails.product_id}}</td>
								    <td class="padding-left">@{{orderDetails.product_name}} <span v-show="orderDetails.color != '' ">(@{{orderDetails.color}})</span> <span v-show="orderDetails.size != '' ">(@{{orderDetails.size}})</span></td>
								    <td class="text-align">@{{orderDetails.quantity}}</td>
								    <td class="text-align">@{{now | currency}}</td>
								    <td class="text-align" width="15%">@{{shipping | currency}}</td>
								    <td class="text-align" width="10%">@{{orderDetails.subtotal}}</td>									
								</tr>
								<tr>
								    <td class="b text-align">2</td>
								    <td></td>
								    <td></td>
								    <td></td>
								    <td></td>
								    <td width="15%"></td>
								    <td width="10%"></td>					
								</tr>
								<tr>
								    <td class="b text-align">3</td>
								    <td></td>
								    <td></td>
								    <td></td>
								    <td></td>
								    <td width="15%"></td>
								    <td width="10%"></td>									
								</tr>
							</tbody>
						</table>
						<table class="border-top">
							<tbody>
								<tr>
								    <td class="border-top" width="90%"><span class="float"><b>{{$lang['total']}}</b> <span class="inclusive">({{$lang['inclusive']}})</span></span></td>
								    <td class="border-top text-align" width="10%">@{{orderDetails.subtotal}}</td>								
								</tr>
							</tbody>
						</table>
					</div>
    			</div>
    			<div class="row">
	    			<div class="col-sm-6 bottom-info">
	    				<div class="b">{{$lang['received-by']}}:</div>
	    				<hr>
	    				<div class="b margin">{{$lang['printed']}}</div>
	    				<hr>
	    				<div class="b margin">{{$lang['relationship']}}</div>
	    				<hr>
	    				<div class="b margin">{{$lang['date-received']}}</div>
	    				<hr>
	    			</div>
	    			<div class="col-sm-6 bottom-info">
	    				<div class="b">{{$lang['attempt']}}:</div>
	    				<div class="b margin">{{$lang['1st']}}</div><hr>
	    				<div class="b margin">{{$lang['2nd']}}</div><hr>
	    				<div class="b margin">{{$lang['3rd']}}</div><hr>
	    			</div>
    			</div>
	    	</div>
	    </div>
    </template><!-- end of modal-body -->

    <template slot="modal-footer">
    	<div class='col-md-12'>
    		<button class='btn btn-raised btn-info ' @click='print()'> {{$lang['print']}}</button>
    		<button class='btn btn-default' data-dismiss="modal" aria-label="Close">{{$lang['close']}}</button>
    	</div>
    </template>
</modal><!-- end of Product Order Modal -->
