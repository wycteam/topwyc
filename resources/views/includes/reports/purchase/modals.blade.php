<!-- Product Order Modal -->
<modal size="modal-lg" class="report-product-modal" id='viewProduct'>
    <template slot="modal-title">
        <h3>
            <button class='btn btn-default btn-fab btn-fab-mini' data-dismiss="modal" aria-label="Close">
                <span class='material-icons'>arrow_back</span>
            </button>
            <span v-if="orderDetails.tracking_number != null">{{$lang['tracking-number']}} &nbsp; <span>@{{orderDetails.tracking_number}}</span></span>
            
    </template><!-- end of modal-title -->

    <template slot="modal-body" class="product-details-set">
    	<div id="printPurchase" class='product-card'>
    		<div class="col-lg-12 product-set">
    			<div class="col-lg-12 order-details">
    				<div class="delivery-info">
                		<h4 class="title"><b>{{$lang['delivery-info']}}</b></h4>
	    				<div class="row">
							<div class="col-sm-4">
								{{$lang['shipped-to']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{orderDetails.order_receiver}}</span>
							</div>
						</div>
						<hr>
	    				<div class="row">
							<div class="col-sm-4">
								{{$lang['address']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{orderDetails.full_address}}</span>
							</div>
						</div>
                        <hr>
                        <div class="row" v-if="orderDetails.order_landmark != null">
                            <div class="col-sm-4">
								{{$lang['nearest-landmark']}}:
							</div>
                            <div class="col-sm-8">
                                <span>@{{orderDetails.order_landmark}}</span>
                            </div>
                        </div>
						<hr v-if="orderDetails.order_landmark != null">
						<div class="row">
							<div class="col-sm-4">
								{{$lang['contact-no']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{orderDetails.order_contact}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['alt-contact-no']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{orderDetails.order_alternate}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
                            <div class="col-sm-4">
								{{$lang['notes']}}:
							</div>
                            <div class="col-sm-8">
                                <span v-if="orderDetails.remarks != null">@{{orderDetails.remarks}}</span>
                                <span v-if="orderDetails.remarks == null">{{$lang['no-notes-msg']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
							<div class="col-sm-4">
								{{$lang['awb']}}:
							</div>
							<div class="col-sm-8">
								@{{orderDetails.awb_id}}
							</div>
						</div>
                        <hr v-if="orderDetails.remarks != null">
                	</div> <!-- delivery-info -->

    				<!-- <div style="height:20px;clear:both;"></div> --> 
    				<!-- spacer -->

<!--     				<div class="airway-bill-info">
    					<h4 class="title">Airway Bill</h4>
    					<div class="row">
							<div class="col-sm-4">
								{{$lang['deliver-to']}}:
							</div>
							<div class="col-sm-8">
								@{{orderDetails.order_realname}}
								<br>
								@{{orderDetails.order_address}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								T:
							</div>
							<div class="col-sm-8">
								@{{orderDetails.order_contact}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['area-code']}}:
							</div>
							<div class="col-sm-8">
								@{{orderDetails.province}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['collected']}}:
							</div>
							<div class="col-sm-8">
								@{{orderDetails.subtotal}} PHP
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['awb']}}:
							</div>
							<div class="col-sm-8">
								@{{orderDetails.awb_id}}
							</div>
						</div>
						<hr>
					</div> <!-- airway-bill-info --> 


    				<div style="height:20px;clear:both;"></div> <!-- spacer -->

    				<div class="products-info">
    					<h4 class="title"><b style="text-transform: uppercase;">{{$lang['products']}}</b></h4>
    					<div class="row">
							<div class="col-sm-12 table-responsive">
								<table class="table">
									<thead>
										<tr>
											<!-- <th>&nbsp;</th> -->
											<th>Name</th>
											<th>Status</th>
											<th>Color</th>
											<th>Size</th>
											<th>Weight</th>
											<th>Quantity</th>
											<th>Unit Price</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="detail in orderProducts">
											<!-- <td> 
												<img class="img-thumbnail img-responsive" :src="detail.image"> 

												@{{ detail.product.img_main_image }}
											</td> -->
											<td>@{{ detail.product.name }}</td>
											<td>
												<span class="label" :class="{
													'label-success': detail.status == 'In Transit',
													'label-default': detail.status == 'Cancelled',
													'label-warning': detail.status == 'Pending',
													'label-primary': detail.status == 'Received',
													'label-info': detail.status == 'Delivered',
													'label-default': detail.status == 'Returned'
												}">
													@{{ detail.status }}
												</span>
											</td>
											<td>@{{ (detail.color) ? detail.color : 'N/A' }}</td>
											<td>@{{ (detail.size) ? detail.size : 'N/A' }}</td>
											<td>@{{ detail.product.weight }}</td>
											<td>@{{ detail.quantity }}</td>
											<td>
												&#8369;
												<template v-if="detail.sale_price == 0">
													@{{ ((parseFloat(detail.price_per_item) + (parseFloat(detail.shipping_fee) + parseFloat(detail.charge) + parseFloat(detail.vat))) * detail.quantity) | currency }}
												</template>
												<template v-else>
													@{{ ((parseFloat(detail.sale_price) + (parseFloat(detail.shipping_fee) + parseFloat(detail.charge) + parseFloat(detail.vat))) * detail.quantity) | currency }}
												</template>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div> <!-- products-info -->

					<div style="height:20px;clear:both;"></div> <!-- spacer -->

					<div class="payment-info">
						<h4 class="title">{{$lang['total-summary']}}</h4>
						<div class="col-lg-6 col-lg-offset-6">
							<div class="row" v-if="totaldiscount > 0">
								<div class="col-sm-6">
									{{$lang['subtotal']}}:
								</div>
								<div class="col-sm-6">
									&#8369; @{{ totalsub | currency }}
								</div>
							</div>
							<hr v-if="totaldiscount > 0">
							<div class="row" v-if="totaldiscount > 0">
								<div class="col-sm-6">
									{{$lang['discount']}}:
								</div>
								<div class="col-sm-6">
									&#8369; @{{ totaldiscount | currency }}
								</div>
							</div>
							<hr v-if="totaldiscount > 0">
							<div class="row">
								<div class="col-sm-6">
									<strong>{{$lang['total-price-including-vat']}}:<strong>
								</div>
								<div class="col-sm-6">
									<strong>&#8369; @{{ priceAfterDiscount | currency }}</strong>
								</div>
							</div>
						</div>
					</div> <!-- payment-info -->
    			</div>

    			<!-- <div class="col-lg-4">
    				<div class="product-img-set">
    					<img  class="product-img" :src="orderDetails.product_main_image">
    				</div>
    				<div class="productDetails">
	                    <a :href="'/product/'+orderDetails.product_slug">
	                        <p class="productName" v-if="langmode=='en'">@{{orderDetails.product_name}}</p>
	                        <p class="productName" v-else>@{{orderDetails.product_cn_name}}</p>
	                    </a>
	                    <p>{{$lang['by']}} <span class="storeName"><a :href="'/stores/'+orderDetails.store_slug">@{{orderDetails.store_name}}</a></span></p>

	                    <p class="sku">
	                        <span class="label label-success" v-if="orderDetails.status === 'In Transit'">{{$lang['in-transit-on-1']}} @{{orderDetails.shipped_date}}{{$lang['in-transit-on-2']}} </span>
		    				<span class="label label-default" v-if="orderDetails.status === 'Cancelled'">{{$lang['cancelled']}}</span>
		    				<span class="label label-warning" v-if="orderDetails.status === 'Pending'">{{$lang['pending']}}</span>
		    				<span class="label label-primary" v-if="orderDetails.status === 'Received'">{{$lang['received-on-1']}}@{{orderDetails.received_date}}{{$lang['received-on-2']}}</span>
		    				<span class="label label-primary" v-if="orderDetails.status === 'Delivered'">{{$lang['delivered-on']}} @{{orderDetails.received_date}}</span>

	                    </p>

	                    <div class="qualification-container">	                      
							<h5 class="title qualification">{{$lang['qualifications']}}</h5>
	    					<p v-if="orderDetails.product_boxContent != null">{{$lang['what-in-box']}}:<span>@{{orderDetails.product_boxContent}}</span></p>
		                    <p v-if="orderDetails.product_brand != null">{{$lang['brand']}}: <span>@{{orderDetails.product_brand}}</span></p>
		                    <p v-if="orderDetails.color != ''">{{$lang['color']}}: <span>@{{orderDetails.color}}</span></p>
                            <p v-if="orderDetails.size != ''">{{$lang['size']}}: <span>@{{orderDetails.size}}</span></p>
	                		<p v-if="orderDetails.product_weight != null">{{$lang['weight']}}: <span>@{{orderDetails.product_weight}}</span></p>
	                    </div>
                	</div>
                </div>
                <div class="col-lg-8 order-details">
                	<div class="delivery-info">
                		<h4 class="title">{{$lang['delivery-info']}}</h4>
	    				<div class="row">
							<div class="col-sm-4">
								{{$lang['shipped-to']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{orderDetails.order_receiver}}</span>
							</div>
						</div>
						<hr>
	    				<div class="row">
							<div class="col-sm-4">
								{{$lang['address']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{orderDetails.full_address}}</span>
							</div>
						</div>
                        <hr>
                        <div class="row" v-if="orderDetails.order_landmark != null">
                            <div class="col-sm-4">
								{{$lang['nearest-landmark']}}:
							</div>
                            <div class="col-sm-8">
                                <span>@{{orderDetails.order_landmark}}</span>
                            </div>
                        </div>
						<hr v-if="orderDetails.order_landmark != null">
						<div class="row">
							<div class="col-sm-4">
								{{$lang['contact-no']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{orderDetails.order_contact}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['alt-contact-no']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{orderDetails.order_alternate}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
                            <div class="col-sm-4">
								{{$lang['notes']}}:
							</div>
                            <div class="col-sm-8">
                                <span v-if="orderDetails.remarks != null">@{{orderDetails.remarks}}</span>
                                <span v-if="orderDetails.remarks == null">{{$lang['no-notes-msg']}}</span>
                            </div>
                        </div>
                        <hr v-if="orderDetails.remarks != null">
                	</div>
                	<h4 class="title">{{$lang['total-summary']}}</h4>
                	<div class="row">
						<div class="col-sm-4">
								{{$lang['unit-price']}}:
						</div>
						<div class="col-sm-8">
							&#8369; <span>@{{price_per_item | currency}}</span>
						</div>
					</div>
					<div class="row" v-if="hasDiscount">
						<div class="col-sm-4">
								{{$lang['you-save']}}:
						</div>
						<div class="col-sm-8">
							&#8369; <span>@{{discountedAmount}}</span>(<span>@{{orderDetails.discount}}</span>%)
						</div>
					</div>
					<div class="row" v-if="hasDiscount">
						<div class="col-sm-4">
								{{$lang['now']}}:
						</div>
						<div class="col-sm-8">
							&#8369; <span>@{{now | currency}}</span>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							{{$lang['qty']}}:
						</div>
						<div class="col-sm-8">
							<span>@{{orderDetails.quantity}}</span>
						</div>
					</div>
					<hr>
                    <div class="row">
                        <div class="col-sm-4">
                            {{$lang['shipping-fee']}}:
                        </div>
                        <div class="col-sm-8">
                            &#8369; <span>@{{shipping | currency}}</span>
                        </div>
                    </div>
					<div class="row">
						<div class="col-sm-4">
							{{$lang['total']}}:
						</div>
						<div class="col-sm-8">
							&#8369; <span>@{{orderDetails.subtotal}}</span>
						</div>
					</div>
    			</div> -->
	    	</div>

	    	<div style="height:10px;clear:both;"></div> <!-- spacer -->
	    	<div class="reasonCont col-sm-12" v-if="orderDetails.reason">
	    	    	<br/>
	    	    	<div class="alert alert-dismissible alert-danger returned-info return-details">
						<span>{{$lang['return-msg-1']}} @{{orderDetails.returned_date}} {{$lang['return-msg-2']}}<strong>@{{orderDetails.case_number}}</strong>.</span><br/>

						<p>
							{{$lang['reason-return']}} :
							<ul>
								<li v-for="_detail in orderDetails.order.details" v-if="_detail.reason != null">
									@{{ _detail.product.name }} : 
									<strong>'@{{ _detail.reason }}'</strong>
								</li>
							</ul>
						</p>

						<p>{{$lang['contact-customer-support-1']}} 
							<a href="">{{$lang['contact-customer-support-2']}}</a> {{$lang['contact-customer-support-3']}}
						</p>
					</div>
	    			<div class="col-sm-12" id="photoEvidence">
	    				<h4 class="title return-details">
	    					{{$lang['photo-evidence']}}
	    					<small class="pull-right"><a style="cursor:pointer;" @click="showAddMoreEvidencesModal($event, 'Add more photos')">Add more photos</a></small>
	    				</h4>

	    				<div class="row">
	    					<template v-for="_detail in orderDetails.order.details">
				    			<div class="col-sm-4" v-for='(item, index) in _detail.file_image' :key='item.id' style="display:inline-block; float:none; margin-top:10px;">
				                    <a :href="'/storage/'+item.file" :data-lightbox="'image-'+index" :data-title="orderDetails.reason">
				                       	<img :src="'/storage/'+item.file" class="img-thumbnail">
				                    </a>
				                </div>
			                </template>
	                    </div>
	    			</div>
	    			<div class="col-sm-12" id="videoEvidence">
	    				<h4 class="title">
	    					{{$lang['video-evidence']}}
	    					<small class="pull-right"><a style="cursor:pointer;" @click="showAddMoreEvidencesModal($event, 'Add more videos')">Add more videos</a></small>
	    				</h4>

	    				<div class="row">
	    					<template v-for="_detail in orderDetails.order.details">
		                        <div class="col-sm-4" v-for='(item, index) in _detail.file_video' :key='item.id' style="display:inline-block; float:none; margin-top:10px;">
		                            <a :href="'/storage/'+item.file" data-toggle="lightbox" data-gallery="video-evidence" data-type="video" data-wrapping="false" :id="'video-evidence-' + index" @click="showVideoEvidenceModal($event, index, 'viewProduct')">
			                            <video>
			                                <source :src="'/storage/'+item.file">
			                                {{$lang['browser-no-html-support']}}
			                            </video>
			                        </a>
		                        </div>
	                        </template>
	                    </div>
	    			</div>
	    	</div>
	    </div>
    </template><!-- end of modal-body -->

    <template slot="modal-footer">
    	<button class='btn btn-raised btn-info ' @click='printPurchase()'> @lang('cart.print')</button>
    </template>
</modal><!-- end of Product Order Modal -->

<!-- Return Item Modal -->
<modal size="modal-md" class="return-item-modal" id='returnItemModal'>
    <template slot="modal-title">
        <h3>{{$lang['return-product']}}</h3>
    </template>

    <template slot="modal-body">
    	<form>
    		<div class="row">
    			<div class="col-md-12">
    				<p>Please indicate your reasons on why you want to return this item, you can also gather photos and videos and upload it here.</p>
    			</div>	
    		</div>

    		<div class="row">
    		    <div class="col-md-12">
    		        <div class="form-group">
    		        	<input v-model="returnItemForm.title" class="form-control" placeholder="Title">
    		            <small v-if="returnItemForm.errors.has('title')" class="text-danger">{{$lang['title-return-err']}}</small>
    		        </div>
    		    </div>
    		</div>

    		<div class="row">
    		    <div class="col-md-12">
    		        <div class="form-group">
    		        	<textarea v-model="returnItemForm.reason" class="form-control" placeholder="Body"></textarea>
    		            <small v-if="returnItemForm.errors.has('reason')" class="text-danger">{{$lang['reason-return-err']}}</small>
    		        </div>
    		    </div>
    		</div>

    		<div class="row">
    			<div class="col-md-12">
    				<div
    					:is="dz"
    					id="returnedItemFiles"
    					ref="returnedItemFiles"
    					:url="'/user/purchase-report/' + selectedOrder.id + '/upload-files'"
    					:show-remove-link="false"
    					accepted-file-types="image/*,video/*"
    					:max-file-size-in-m-b="50"
    					:max-number-of-files="null"
    					@vdropzone-success="dropzoneUploadSuccess"
    					@vdropzone-queue-complete="dropzoneQueueComplete">
    					<input type="hidden" name="token" value="xxx">
    					<div class="dz-message" data-dz-message><span>Click to choose or drag images/videos to upload.</span></div>
    				</div>
    				<!-- <small v-if="! returnItemForm.submitted" class="return-item-mask text-danger text-center">{{$lang['submit-return-msg-err']}}</small> -->
    			</div>
    		</div>
    	</form>
    </template>

    <template slot="modal-footer">
        <a @click="submit" href="javascript:void(0)" class="btn btn-return-item-submit btn-has-loading btn-success">{{$lang['submit']}}</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">{{$lang['close']}}</button>
    </template>
</modal>

<modal size="modal-lg" id='add-more-evidences-modal'>
    <template slot="modal-title">
        <h3></h3>
    </template>

    <template slot="modal-body">
    	<div
    		:is="dz2"
    		id="returnedItemFiles2"
    		ref="returnedItemFiles2"
    		:url="'/user/purchase-report/' + orderDetails.id + '/upload-files'"
    		:show-remove-link="false"
    		accepted-file-types="image/*,video/*"
    		:max-file-size-in-m-b="50"
    		:max-number-of-files="null"
    		@vdropzone-success="dropzoneUploadSuccess"
    		@vdropzone-error="dropzoneUploadError">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    	</div>
    </template>
</modal>
