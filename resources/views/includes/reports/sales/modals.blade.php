<!-- Product Order Modal -->
<modal size="modal-lg" class="report-product-modal" id='orderProducts'>
    <template slot="modal-title">
        <h3>
            <button class='btn btn-default btn-fab btn-fab-mini' data-dismiss="modal" aria-label="Close">
                <span class='material-icons'>arrow_back</span>
            </button>
            <span v-if="details.tracking_number != null">{{$lang['pending-order']}} &nbsp; <span>@{{details.tracking_number}}</span></span>
        </h3>
    </template><!-- end of modal-title -->

    <template slot="modal-body" class="product-details-set">
    	<div id="printSales" class='product-card'>
    		<div class="col-lg-12 product-set">

    			<div class="col-lg-12 order-details">
    				<div class="delivery-info">
    					<h4 class="title"><b>{{$lang['order-details']}}</b></h4>
    					<div class="row">
							<div class="col-sm-4">
								{{$lang['placed-by']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_fullname}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['placed-on']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.created_at}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['shipped-to']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_receiver}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['ship-address']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_address}}</span>
							</div>
						</div>
						<hr>

						<div class="row" v-if="details.order_landmark != null">
							<div class="col-sm-4">
								{{$lang['nearest-landmark']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_landmark}}</span>
							</div>
						</div>
						<hr v-if="details.order_landmark != null">
						<div class="row">
							<div class="col-sm-4">
								{{$lang['contact-no']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_contact}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['alt-contact-no']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_alternate}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['buyer-remarks']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.remarks}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['awb']}}:
							</div>
							<div class="col-sm-8">
								@{{details.awb_id}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['owner']}}:
							</div>
							<div class="col-sm-8">
								@{{owner}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['store-name']}}:
							</div>
							<div class="col-sm-8">
								@{{details.store_name}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['store-address']}}:
							</div>
							<div class="col-sm-8">
								@{{details.store_address}}
							</div>
						</div>
						<hr>
    				</div> <!-- delivery-info -->

    				<div style="height:20px;clear:both;"></div> <!-- spacer -->

<!--     				<div class="airway-bill-info">
    					<h4 class="title">Airway Bill</h4>
    					<div class="row">
							<div class="col-sm-4">
								{{$lang['deliver-to']}}:
							</div>
							<div class="col-sm-8">
								@{{details.order_realname}}
								<br>
								@{{details.order_address}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								T:
							</div>
							<div class="col-sm-8">
								@{{details.order_contact}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['area-code']}}:
							</div>
							<div class="col-sm-8">
								@{{details.province}}
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['collected']}}:
							</div>
							<div class="col-sm-8">
								@{{details.subtotal}} PHP
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['awb']}}:
							</div>
							<div class="col-sm-8">
								@{{details.awb_id}}
							</div>
						</div>
						<hr>
					</div>  -->
					<!-- airway-bill-info -->

    				<div style="height:20px;clear:both;"></div> <!-- spacer -->

    				<div class="products-info">
    					<h4 class="title"><b style="text-transform: uppercase;">{{$lang['products']}}</b></h4>
    					<div class="row">
							<div class="col-sm-12 table-responsive">
								<table class="table">
									<thead>
										<tr>
											<!-- <th>&nbsp;</th> -->
											<th>Product Name</th>
											<th>Status</th>
											<th>Color</th>
											<th>Size</th>
											<th>Weight</th>
											<th>Quantity</th>
											<th>Unit Price</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="detail in orderProducts" v-if="idProds.includes(detail.product.id)">
											<!-- <td>
												<img class="img-thumbnail img-responsive" :src="detail.image"> 
												@{{ detail.product.img_main_image }}
											</td> -->
											<td>@{{ detail.product.name }}</td>
											<td>
												<span class="label" :class="{
													'label-success': detail.status == 'In Transit',
													'label-default': detail.status == 'Cancelled',
													'label-warning': detail.status == 'Pending',
													'label-primary': detail.status == 'Received',
													'label-info': detail.status == 'Delivered',
													'label-default': detail.status == 'Returned'
												}">
													@{{ detail.status }}
												</span>
											</td>
											<td>@{{ (detail.color) ? detail.color : 'N/A' }}</td>
											<td>@{{ (detail.size) ? detail.size : 'N/A' }}</td>
											<td>@{{ detail.product.weight }}</td>
											<td>@{{ detail.quantity }}</td>
											<td>
												<!-- &#8369; | currency -->
												&#8369;
												<template v-if="detail.sale_price == 0">
													@{{ ((parseFloat(detail.price_per_item) + (parseFloat(detail.shipping_fee) + parseFloat(detail.charge) + parseFloat(detail.vat))) * detail.quantity) | currency }}
												</template>
												<template v-else>
													@{{ ((parseFloat(detail.sale_price) + (parseFloat(detail.shipping_fee) + parseFloat(detail.charge) + parseFloat(detail.vat))) * detail.quantity) | currency }}
												</template>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div> <!-- products-info -->

					<div style="height:20px;clear:both;"></div> <!-- spacer -->

					<div class="payment-info">
						<h4 class="title"><b>{{$lang['total-summary']}}</b></h4>
						<div class="col-lg-6 col-lg-offset-6">
							<div class="row" v-if="totaldiscount > 0">
								<div class="col-sm-6">
									{{$lang['subtotal']}}:
								</div>
								<div class="col-sm-6">
									&#8369; @{{ totalsub | currency }}
								</div>
							</div>
							<hr v-if="totaldiscount > 0">
							<div class="row" v-if="totaldiscount > 0">
								<div class="col-sm-6">
									{{$lang['discount']}}:
								</div>
								<div class="col-sm-6">
									&#8369; @{{ totaldiscount | currency }}
								</div>
							</div>
							<hr v-if="totaldiscount > 0">
							<div class="row">
								<div class="col-sm-6">
									<strong>{{$lang['total-price-including-vat']}}:<strong>
								</div>
								<div class="col-sm-6">
									<strong>&#8369; @{{ priceAfterDiscount | currency }}</strong>
								</div>
							</div>
						</div>
					</div> <!-- payment-info -->
    			</div> <!-- col-lg-12 -->

    			<!-- <div class="col-lg-4">
    				<div class="product-img-set">
	    				<img  class="product-img" :src="details.img_main_image">
	    			</div>
	    			<div class="productDetails">
	    				<a :href="'/product/'+ details.product_slug">
	    					<p class="productName">@{{details.product_name}}</p>
	    				</a>
	    				<span class="label label-success" v-if="details.status === 'In Transit'">{{$lang['in-transit-on-1']}} @{{details.shipped_date}}{{$lang['in-transit-on-2']}} </span>
	    				<span class="label label-default" v-if="details.status === 'Cancelled'">{{$lang['cancelled']}}</span>
	    				<span class="label label-warning" v-if="details.status === 'Pending'">{{$lang['pending']}}</span>
	    				<span class="label label-primary" v-if="details.status === 'Received'">{{$lang['received-on-1']}}@{{details.received_date}}{{$lang['received-on-2']}}</span>
	    				<span class="label label-primary" v-if="details.status === 'Delivered'">{{$lang['delivered-on']}} @{{details.received_date}}</span>
	    			</div>
	    			<div class="qualification-container">
    					<h5 class="title qualification">{{$lang['qualifications']}}</h5>
    					<p v-if="details.product_boxContent != null">{{$lang['what-in-box']}}:<span>@{{details.product_boxContent}}</span></p>
	                    <p v-if="details.product_brand != null">{{$lang['brand']}}: <span>@{{details.product_brand}}</span></p>
	                    <p v-if="details.color != ''">{{$lang['color']}}: <span>@{{details.color}}</span></p>
                        <p v-if="details.size != ''">{{$lang['size']}}: <span>@{{details.size}}</span></p>
                		<p v-if="details.product_weight != null">{{$lang['weight']}}: <span>@{{details.product_weight}}</span></p>
    				</div>
    			</div>
    			<div class="col-lg-8 order-details">
    				<div class="delivery-info">
    					<h4 class="title">{{$lang['order-details']}}</h4>
    					<div class="row">
							<div class="col-sm-4">
								{{$lang['placed-by']}}:
							</div>
							<div class="col-sm-8">
								<a href="">@{{details.order_fullname}}</a>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['placed-on']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.created_at}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['shipped-to']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_receiver}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['ship-address']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_address}}</span>
							</div>
						</div>
						<hr>

						<div class="row" v-if="details.order_landmark != null">
							<div class="col-sm-4">
								{{$lang['nearest-landmark']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_landmark}}</span>
							</div>
						</div>
						<hr v-if="details.order_landmark != null">
						<div class="row">
							<div class="col-sm-4">
								{{$lang['contact-no']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_contact}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['alt-contact-no']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.order_alternate}}</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['buyer-remarks']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.remarks}}</span>
							</div>
						</div>
						<hr>
    				</div>
    				<div>
    					<h4 class="title">{{$lang['total-summary']}}</h4>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['unit-price']}}:
							</div>
							<div class="col-sm-8">
								&#8369; <span>@{{price_per_item | currency}}</span>
							</div>
						</div>
						<div class="row" v-if="hasDiscount">
							<div class="col-sm-4">
								{{$lang['discount']}}:
							</div>
							<div class="col-sm-8">
								&#8369; <span>@{{discountedAmount}}</span>(<span>@{{details.discount}}</span>%)
							</div>
						</div>
						<div class="row" v-if="hasDiscount">
							<div class="col-sm-4">
							{{$lang['now']}}:
							</div>
							<div class="col-sm-8">
								&#8369; <span>@{{now | currency}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
							{{$lang['qty']}}:
							</div>
							<div class="col-sm-8">
								<span>@{{details.quantity}}</span>
							</div>
						</div>
						<hr>
	                    <div class="row">
	                        <div class="col-sm-4">
	                            {{$lang['shipping-fee']}}:
	                        </div>
	                        <div class="col-sm-8">
	                            &#8369; <span>@{{shipping | currency}}</span>
	                        </div>
	                    </div>
						<div class="row">
							<div class="col-sm-4">
								{{$lang['total']}}:
							</div>
							<div class="col-sm-8">
								&#8369; <span>@{{details.subtotal | currency}}</span>
							</div>
						</div>
    				</div>
    			</div> -->
    		</div>

    		<div style="height:10px;clear:both;"></div> <!-- spacer -->

    		<div class="col-sm-12" v-if="details.reason">
	    	    	<br/>
	    	    	<div class="alert alert-dismissible alert-danger returned-info">
						<span>{{$lang['return-msg-1']}} @{{details.returned_date}} {{$lang['return-msg-2']}}<strong>@{{details.case_number}}</strong>.</span>
						<br/>

						<p>
							{{$lang['reason-return']}} :
							<ul>
								<li v-for="_detail in details.order.details" v-if="_detail.reason != null && _detail.product.my_product">
									@{{ _detail.product.name }} : 
									<strong>'@{{ _detail.reason }}'</strong>
								</li>
							</ul>
						</p>

						<p>{{$lang['contact-customer-support-1']}} <a href="">{{$lang['contact-customer-support-2']}}</a> {{$lang['contact-customer-support-3']}}</p>
					</div>
	    			<div class="col-sm-12" v-if="details.order.details.file_image != ''" id="photoEvidence">
	    				<h4 class="title return-details">{{$lang['return-image']}}</h4>
                        <div class="row">
                        	<template v-for="_detail in details.order.details" v-if="_detail.product.my_product">
	                            <div class="col-sm-4" v-for='(item,index) in _detail.file_image' :key='item.id' style="display:inline-block; float:none; margin-top:10px;">
	                                <a :href="'/storage/'+item.file" :data-lightbox="'image-'+index" :data-title="details.reason">
	                                    <img :src="'/storage/'+item.file" class="img-thumbnail">
	                                </a>
	                            </div>
	                        </template>
                        </div>
	    			</div>
	    			<div class="col-sm-12" v-if="details.order.details.file_video != ''" id="videoEvidence">
	    				<h4 class="title">{{$lang['return-videos']}}</h4>
	                    <div class="row">
	                    	<template v-for="_detail in details.order.details" v-if="_detail.product.my_product">
	                            <div class="col-sm-4" v-for='(item, index) in _detail.file_video' :key='item.id' style="display:inline-block; float:none; margin-top:10px;">
		                            <a :href="'/storage/'+item.file" data-toggle="lightbox" data-gallery="video-evidence" data-type="video" data-wrapping="false" :id="'video-evidence-' + index" @click="showVideoEvidenceModal($event, index, 'orderProducts')">
		                            	<video>
		                            		<source :src="'/storage/'+item.file">
		                                	{{$lang['browser-no-html-support']}}
		                            	</video>
		                            </a>
	                            </div>
	                        </template>
                        </div>
	    			</div>
	    		</div>
    	</div>
    </template><!-- end of modal-body -->

    <template slot="modal-footer">
    	<div class='card total-card'>
    		<div class="col-sm-12 order-notes">
				<h4 class="title">{{$lang['order-notes']}}</h4>
				<div class="row">
					<div class="col-sm-12">
						<textarea class="form-control order-notes" rows="1" id="txt-ordernotes" v-model="updateRemarks.seller_notes" @blur="submitNote(details.id)"></textarea>
        				<span class="help-block">{{$lang['order-notes-msg']}}</span>
					</div>
				</div>
			</div>
			<button class='btn btn-raised btn-info ' @click='printSales()'> @lang('cart.print')</button>
		</div>
    </template>
</modal><!-- end of Product Order Modal-->

<!-- In Transit Modal -->
<modal size="modal-md" class="intransit-modal" id='inTransitModal'>
    <template slot="modal-title">
        <h3>
          {{$lang['ship-your-item']}}
        </h3>
    </template><!-- end of modal-title -->

    <!--  modal-body -->
    <template slot="modal-body" class="product-details-set">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <select v-model="transitForm.shipment_type" class="form-control">
                        <option disabled value="null">{{$lang['shipment-type']}}</option>
                        <option value="Drop Off">{{$lang['drop-off']}}</option>
                        <option value="Pick Up">{{$lang['pick-up']}}</option>
                    </select>
                    <small v-if="transitForm.errors.has('shipment_type')" class="text-danger">{{$lang['required-shipment']}}</small>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <select v-model="transitForm.courier" class="form-control">
                        <option disabled value="null">{{$lang['select-courier']}}</option>
                        <option value="LBC">LBC</option>
                        <option value="2GO">2GO</option>
                    </select>
                    <small v-if="transitForm.errors.has('courier')" class="text-danger">{{$lang['required-courier']}}</small>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input v-model="transitForm.tracking_number" type="text" class="form-control" id="tracking_number" placeholder="Tracking Number">
                    <small v-if="transitForm.errors.has('tracking_number')" class="text-danger">{{$lang['required-track-no']}}</small>
                </div>
            </div>
        </div>
    </template><!-- end of modal-body -->

    <!--  modal-footer -->
    <template slot="modal-footer">
        <a @click="submitTrackingNumber" href="javascript:void(0)" class="btn btn-transit-submit btn-has-loading btn-raised btn-success">{{$lang['submit']}}</a>
    </template><!-- end of modal-body -->

</modal>

<!-- In Transit Modal -->
<modal size="modal-lg" class="intransit-modal" id='inTransitModal2'>
    <template slot="modal-title">
        <h3><b>
          {{$lang['ship-your-item']}}
        </b></h3>
    </template><!-- end of modal-title -->

    <!--  modal-body -->
    <template slot="modal-body" class="product-details-set">
        <div class="row">
            <div class="col-lg-8">
                    <h4 class="txt-cont">To ship your item, we require a shipment security deposit in case there is a problem with your product and  return happens. This amount will be credited back to your e-wallet once transaction is successful and the buyer receives non faulty and undamaged product.
                    <br> 
                    Check the <a href="/faq" target="_blank">FAQ page</a> for more information. We will also generate a tracking number & <b>2GO</b> pickup request.  Please check <a href="#">DELIVERY POLICY</a> for more info.</h4>
                    <div class="col-lg-5 ewallet-info">
<!--                     	<span>Total E-wallet : </span><br> -->
                    	
                    </div>

					<div class="card">
					  <div class="card-body">
					    <h3 class="card-title">
                    		<i aria-hidden="true" class="fa fa-exclamation-triangle" style="color:#009688;"></i>
                    		Reminder
					    </h3>
					    <p class="card-text">
					    	Your item/s will be picked up <strong>2 days ({{ \Carbon\Carbon::today()->addDays(2)->format('F d, Y') }})</strong> from now. Make sure that your item/s are ready.
					    </p>
					  </div>
					</div>

                    <div>
						<a @click="submit2Go" href="javascript:void(0)" class="btn btn-transit-submit btn-has-loading btn-raised btn-success  pull-right">{{$lang['submit']}}</a>
                    </div>
            </div>
            <div class="col-lg-4 prlist">
            	<h4><b>Product/s Included</b></h4>
            	<div class="lsprod">
					<div class="catlist" v-for='item in listProd.data'>
						<dl>
							<dt>
								<img :src="item.image" alt="Product image" width="90" height="78" />
								<a>@{{(item.product.name).length > 40 ? (item.product.name).substring(0,40)+"...":item.product.name}}</a>
							</dt>
							<dd>
<!-- 								<span v-if="item.product.sale_price > 0"><b>Price : @{{item.product.sale_price}}</b></span>
								<span v-else><b>Price : @{{item.product.price}}</b></span> -->
								<span><b>Price : @{{item.subtotal}}</b></span>
								<br>
								<span><b>Qty : @{{item.quantity}}</b></span>
							</dd>
						</dl>
					</div>
				</div>
				<br>
				<span><b>Usable E-wallet : P{{$usable_ewallet}}</b></span><br>
				<span><b>Security Deposit : P@{{totalShippingFee}}</b></span>
            </div>
        </div>
    </template><!-- end of modal-body -->

    <!--  modal-footer -->
    <template slot="modal-footer">

    </template><!-- end of modal-body -->

</modal>

<modal size="modal-md" id="cancelModal">
    <template slot="modal-title">
        <h3><b>{{$lang['cancel-order']}}</b></h3>
    </template>

    <template slot="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group" style="margin-top:0px;">
                	<h4 class="txt-cont"><i>Note :</i> &nbsp; This product will temporarily be supended if you continue to cancel this transaction. Please contact <b>Customer Support</b> if you have questions about product cancellation or you can check <a href="/faq" target="_blank">FAQ page</a> for more info.<br>
                	If you still want to proceed with the cancellation, please indicate the reason below:</h4>
                    <textarea v-model="cancelForm.reason" class="form-control" placeholder="Enter reason"></textarea>
                    <small v-if="cancelForm.errors.has('reason')" class="text-danger">{{$lang['required-reason']}}</small>
                </div>
            </div>
        </div>
    </template>

    <template slot="modal-footer">
        <a @click="submitReason" href="javascript:void(0)" class="btn btn-cancel-form-submit btn-has-loading btn-raised btn-success">{{$lang['submit']}}</a>
    </template>
</modal>

<!-- Airway Bill Modal -->
<modal size="modal-lg" class="airway-product-modal" id='viewAirwayBill'>
    <template slot="modal-title">
            
    </template><!-- end of modal-title -->

    <template slot="modal-body" class="product-details-set">
    	<div id="print" class='product-card'>
    		<div class="col-lg-12 product-set">
    			<div class="row">
    				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
	    				<div class="deliver">{{$lang['deliver-to']}}</div>
					    <div>@{{details.order_realname}}</div>
					    <div>@{{details.order_address}}</div>
					    <br>
					    <div>T: @{{details.order_contact}}</div>
					    <div>{{$lang['area-code']}}: <b>@{{details.province}}</b></div>
					    <div><b>{{$lang['collected']}}:</b> @{{details.subtotal}} PHP</div>
	    			</div>
	    			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
	    			</div>
	    			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
	    				<div><b>{{$lang['awb']}} :</b> @{{details.awb_id}}</div>
	    				<div><b>{{$lang['weight']}} (kgs) :</b> @{{weight | currency}}</div>
	    				<div><b>{{$lang['dimensions']}} : @{{details.dimension}}</b></div>
	    				<div><b>{{$lang['order-date']}} :</b> @{{details.created_at}}</div>
	    				<div><b>{{$lang['pieces']}} :</b> @{{details.quantity}}</div>
	    				<div><b>{{$lang['package-type']}} :</b> @{{details.package_type}}</div>
	    			</div>
    			</div>

    			<div class="row">
	    			<div class="table-responsive col-lg-12">
	    				<br>    			
						<table>
							<thead>
								<tr>
									<th class="text-align">{{$lang['sr']}}</th>
									<th class="padding-left">{{$lang['item-code']}}</th>
									<th class="padding-left">{{$lang['item-desc']}}n</th>
									<th class="text-align">{{$lang['quantity']}}</th>
									<th class="text-align">{{$lang['value']}}</th>
									<th class="text-align">{{$lang['shipping-fee']}}</th>
									<th class="text-align">{{$lang['total']}}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								    <td class="b text-align">1</td>
								    <td class="padding-left">@{{details.product_code}}-@{{details.product_id}}</td>
								    <td class="padding-left">@{{details.product_name}} <span v-show="details.color != '' ">(@{{details.color}})</span> <span v-show="details.size != '' ">(@{{details.size}})</span></td>
								    <td class="text-align">@{{details.quantity}}</td>
								    <td class="text-align">@{{now | currency}}</td>
								    <td class="text-align" width="15%">@{{shipping | currency}}</td>
								    <td class="text-align" width="10%">@{{details.subtotal}}</td>									
								</tr>
								<tr>
								    <td class="b text-align">2</td>
								    <td></td>
								    <td></td>
								    <td></td>
								    <td></td>
								    <td width="15%"></td>
								    <td width="10%"></td>					
								</tr>
								<tr>
								    <td class="b text-align">3</td>
								    <td></td>
								    <td></td>
								    <td></td>
								    <td></td>
								    <td width="15%"></td>
								    <td width="10%"></td>									
								</tr>
							</tbody>
						</table>
						<table class="border-top">
							<tbody>
								<tr>
								    <td class="border-top" width="90%"><span class="float"><b>{{$lang['total']}}</b> <span class="inclusive">({{$lang['inclusive']}})</span></span></td>
								    <td class="border-top text-align" width="10%">@{{details.subtotal}}</td>								
								</tr>
							</tbody>
						</table>
					</div>
    			</div>
    			<div class="row">
    				<div class="col-sm-6 bottom-info">
	    				<div class="b">{{$lang['received-by']}}:</div>
	    				<hr>
	    				<div class="b margin">{{$lang['printed']}}</div>
	    				<hr>
	    				<div class="b margin">{{$lang['relationship']}}</div>
	    				<hr>
	    				<div class="b margin">{{$lang['date-received']}}</div>
	    				<hr>
	    			</div>
	    			<div class="col-sm-6 bottom-info">
	    				<div class="b">{{$lang['attempt']}}:</div>
	    				<div class="b margin">{{$lang['1st']}}</div><hr>
	    				<div class="b margin">{{$lang['2nd']}}</div><hr>
	    				<div class="b margin">{{$lang['3rd']}}</div><hr>
	    			</div>
    			</div>
	    	</div>
	    </div>
    </template><!-- end of modal-body -->

    <template slot="modal-footer">
    	<div class='col-md-12'>
    		<button class='btn btn-raised btn-info ' @click='print()'> {{$lang['print']}}</button>
    		<button class='btn btn-default' data-dismiss="modal" aria-label="Close">{{$lang['close']}}</button>
    	</div>
    </template>
</modal><!-- end of Product Order Modal -->
