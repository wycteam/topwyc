<div class="user-head">
	<button class="btn btn-tangerine btn-msg-btn btn-raised" type="button">
		New Message                  	
	</button>
</div>

<div class="inbox-body">
	<div class="form-group custom-search">
		<input type="text" class="form-control" id="txt-search-msg" placeholder="Search Messenger ...">
	</div>
</div>

<ul class="nav nav-pills nav-stacked labels-info ">
	<msg-contact
		v-for="frchat in frchat"
		:frchat="frchat"
		:key="frchat.id"
	>
	</msg-contact>
</ul><!-- ./labels-info -->