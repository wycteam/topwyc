<comp-modal id='dialogRemove'>
	<template slot="modal-title">
        {{$lang['are-you-sure']}}
    </template>
    <template slot="modal-body">
        <!-- Let Google help apps determine location. This means this is a random and gibberish string that is needed for testing. -->
    </template>
    <template slot="modal-footer">
        <button class='btn btn-default'  data-dismiss="modal">@lang('common.cancel')</button>
        <button class='btn btn-primary'  data-dismiss="modal" @click='deleteCompare()'>@lang('common.proceed')</button>
    </template>
</comp-modal>