
@extends('layouts.master')
@section('title', '404 | Page Not Found')

@push('styles')
<style type="text/css">
  #fof{display:block; width:100%; line-height:1.6em; text-align:center;}
  #fof h1{font-size:80px; text-transform:uppercase;}
  #fof img{margin-bottom:25px;}
  #fof p{display:block; margin:0 0 25px 0; padding:0; font-size:16px;}
  #fof #respond input{width:200px; padding:5px; border:1px solid #CCCCCC;}
  #fof #respond #submit{width:auto;}
</style>
@endpush

@section('content')
<div class="full-content custom-chosen" id="profile-container">
    <div class="container">


<div class="wrapper row2">
  <div id="container" class="clear">
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <section id="fof" class="clear">
      <!-- ####################################################################################################### -->
      <h1>WHOOPS!</h1>
      <img src="/images/404.png" alt="">
      <p>The Page You Requested Could Not Be Found On Our Server</p>
      <p>Go back to the <a href="javascript:history.go(-1)">previous page</a> or visit our <a href="/">homepage</a></p>
     
      <!-- ####################################################################################################### -->
    </section>
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
  </div>
</div>

</div>
</div>
@endsection