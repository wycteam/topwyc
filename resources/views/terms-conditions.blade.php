<!--  TERMS AND CONDITIONS PAGE -->
@extends('layouts.master')

@section('title', 'Terms and Conditions')

@push('styles')
 	{!! Html::style(mix('css/pages/terms-conditions.css','shopping')) !!}
@endpush

@section('content')
<div class="full-content" id="terms-conditions-content">
  <div class="container">

    <div class="main-header-container">
      <h3 class="main-header">TERMS AND CONDITIONS</h3>
    </div><!-- end  main-header-container-->

    <span class="label label-warning">Last updated on: December 8, 2017</span>
    <div class="col-sm-12">
    	<p>By signing up for any of the products or services offered by Wataf (together, the “Services”), or dealing with a Seller using Wataf Services, you are agreeing to be bound by the following Wataf Terms and Conditions. The Services offered by Wataf under the Terms and Conditions includes various products and services to help you create and manage your store or shop with our Sellers or both.This document is a legally binding agreement (the “Agreement”) between you as the user(s) of Wataf website (referred to as “you”, “your”, “Seller”, “Customer” or “User” hereinafter) and www.wataf.ph. If you have any questions about our Terms and Conditions, please contact us through our Customer Support link.
		</p>
		<p>Any new features or tools which are added to the current Service shall be also subject to the Terms and Conditions. You can review the current version at any time at https://www.wataf.com/legal/terms. Wataf website reserves the right to update and change the Terms and Conditions by posting updates and changes to the Wataf website. You are advised to check the Terms and Conditions from time to time for any updates or changes that may impact you.
		</p>
		<p>If there is any conflict between the English version and another language version of this Agreement, the English version shall prevail.
		</p>

		<p><b>You must read, agree with and accept all of the Terms and Conditions contained in this Terms and Conditions agreement and Wataf.ph’s Privacy Policy before you may become a Wataf website user.</b></p>

		<p>Please review the following basic rules that govern your use and access of Wataf (the “<b>Agreement</b>”). Please also note that your use or access of Wataf website constitutes your unconditional agreement to follow and be bound by this Agreement, although you may “bookmark” a particular portion of this website and thereby bypass this Agreement. If you do not accept all of the terms of this Agreement, please do not use or access this website.
		</p>

		<p><b>1. Account Terms</b></p>
		<p>a.) The Wataf Service as offered on the website is not available to minors under the age of 15 or to any users suspended or removed from the system by Wataf for any reason. If you do not qualify, you may not use Wataf Services.
		</p>
		<p>b.)You must be 18 years or older or at least the age of majority in the jurisdiction where you reside or from which you use this Service.
		</p>
		<p>c.) You must register for a Wataf user account (“Account”) by providing your full legal name, current address, phone number, a valid email address, and any other information indicated as required to access and use the Services, . Each User shall be solely responsible for the information and content posted on Wataf website. Providing untruthful or inaccurate information constitutes a breach of these Terms and Conditions. Wataf has the right to suspend or terminate your account and refuse any and all current or future use of the website (or any portion thereof).
		</p>
		<p>d.) You acknowledge that Wataf will use the email address you provide as the primary method for communication.
		</p>
		<p>e.) You are responsible for keeping your password secure. Wataf cannot and will not be liable for any loss or damage from your failure to maintain the security of your Account and password.
		</p>
		<p>f.) You are responsible for all activity and content such as data, graphics, photos and links that is uploaded under your Wataf User Account (“Store Content”). You must not transmit any worms or viruses or any code of a destructive nature.
		</p>
		<p>g.) A breach or violation of any term in the Terms and Conditions as determined in the sole discretion of Wataf will result in an immediate termination of your services.
		</p>

		<p><b>WHICH MEANS:</b></p>
		<p>Don’t use Wataf for anything illegal or transmit any harmful code. Remember that with any violation of these terms we will cancel your service. If we need to reach you, we will send you an email.
		</p>

		<p><b>2. Account Activation</b></p>
		<p>a.) The person signing up for the Service will be the contracting party (“Account Owner”) for the purposes of our Terms and Conditions and will be the person who is authorized to use any corresponding account we may provide to the Account Owner in connection with the Service.
		</p>
		<p>b.) If you are signing up for the Service on behalf of your employer, your employer shall be the Account Owner. If you are signing up for the Service on behalf of your employer, then you represent and warrant that you have the authority to bind your employer to our Terms of Service.
		</p>
		<p>c.) A set of User ID and Password is unique to a single account. Each Registered User shall be solely responsible for maintaining the confidentiality and security of your User ID and Password and for all activities that occur under your Wataf Account.
		</p>

		<p><b>3. General Conditions</b></p>
		<p>You must read, agree with and accept all of the terms and conditions contained in these Terms and Conditions and the Privacy Policy before you may become a member of Wataf.
		</p>
		<p>a.) You acknowledge and agree that Wataf may amend these Terms and Conditions at any time by posting the relevant amended and restated Terms and Conditions on Wataf’s website, available at https://www.wataf.com/termscondition and such amendments to the Terms and Conditions are effective as of the date of posting. Your continued use of the Services after the amended Terms and Conditions are posted to Wataf’s website constitutes your agreement to, and acceptance of, the amended Terms and Conditions. If you do not agree to any changes to the Terms and Conditions, do not continue to use our Service.
		</p>
		<p>b.) You are prohibited to use the Wataf Service for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws), the laws applicable to you in your customer’s jurisdiction. You will comply with all applicable laws, rules and regulations in your use of the Service.
		</p>
		<p>c.) You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service without the expressed written permission by Wataf.
		</p>
		<p>d.) You shall not purchase search engine or other pay per click keywords, or domain names that use Wataf or Wataf.ph trademarks and/or variations and misspellings thereof.
		</p>
		<p>e.) Questions about the Terms and Conditions should be sent to support@wataf.com.
		</p>
		<p>f.) You agree and acknowledge that your use of the Service, including information transmitted to or stored by Wataf, is governed by its privacy policy at https://www.wataf.com/privacypolicy.
		</p>
		<p>g.) The Terms and Conditions may be available in languages other than English. To the extent of any inconsistencies or conflicts between these English Terms of Service and Wataf’s Terms of Service available in another language, the most current English version of the Terms of Service at https://www.wataf.com/termscondition will prevail.
		</p>

		<p><b>4. Wataf Rights</b></p>
		<p>a.) We reserve the right to modify or terminate the Service for any reason, without notice at any time.
		</p>
		<p>b.) We reserve the right to refuse service to anyone for any reason at any time.
		</p>
		<p>c.) We may, but have no obligation to remove Store Content and Accounts containing content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms and Conditions.
		</p>
		<p>d.) Any kind of verbal or written abuse (including threats of abuse or retribution) of any Wataf customer, employee, member, or officer will result in immediate Account termination.
		</p>
		<p>e.) Wataf does not pre-screen Store Content and it is in our sole discretion to refuse or remove any Store Content that is available via the Service.
		</p>
		<p>f.) We reserve the right to provide our services to your competitors and make no promise of exclusivity in any particular market segment. You further acknowledge and agree that Wataf  employees and contractors may also be Wataf customers/sellers and that they may compete with you, although they may not use your confidential information in doing so.
		</p>
		<p>g.) In the event of a dispute regarding Account ownership, we reserve the right to request documentation to determine or confirm Account ownership. Documentation may include, but is not limited to, a scanned copy of your business license, government issued photo ID, the last four digits of the credit card on file, etc.
		</p>
		<p>h.) Wataf retains the right to determine, in our sole judgment, rightful Account ownership and transfer an Account to the rightful owner. If we are unable to reasonably determine the rightful Account owner, Wataf reserves the right to temporarily disable an Account until resolution has been determined between the disputing parties.
		</p>

		<p><b>5. Limitation of Liability</b></p>
		<p>a.) You expressly understand and agree that Wataf shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses resulting from the use of or inability to use the service.
		</p>
		<p>b.) In no event shall Wataf or our suppliers be liable for lost profits or any special, incidental or consequential damages arising out of or in connection with our site, our services or these Terms and Conditions (however arising including negligence). You agree to defend, indemnify and hold harmless Wataf and (as applicable) our subsidiaries, affiliates, partners, officers, directors, agents, employees, and suppliers from any claim or demand, including reasonable attorneys’ fees, made by any third party due to or arising out of your breach of these Terms and Conditions or the documents it incorporates by reference, or your violation of any law or the rights of a third party.
		</p>
		<p>c.) Your use of Wataf Service is at your sole risk. The Service is provided on an “as is” and “as available” and “with all the faults” basis without any warranty or condition, express, implied or statutory.
		</p>
		<p>d.)You are solely responsible for your seller and other users interactions to the website. To the extent permitted under applicable laws, you hereby release Wataf, it’s directors, officers, employees and agents from any and all claims or liability related to any seller service or product, seller inaction or unlawful action, seller’s failure to comply with applicable law and/or failure to abide the Terms and Conditions, and any conduct or speech whether online or offline, of any other user.
		</p>
		<p>e.) In addition, in no event shall Wataf be liable for damages stemming from any disputes related to goods, services, or information purchased or obtained from a seller or a third-party via the website, including, but not limited to, disputes about quality, safety, warranty, lawfulness or availability of such goods, services or information, no matter if it is special, direct, indirect, punitive, incidental or consequential damages, or related to contract, negligence, or tort.
		</p>
		<p>f.) Wataf does not warrant that the Service will be uninterrupted, timely, secure, or error-free.
		</p>
		<p>g.) Wataf does not warrant that the results that may be obtained from the use of the Service will be accurate or reliable.
		</p>
		<p>h.) Wataf does not guarantee that the quality of any products, services, information, or other material purchased or obtained by you through the Service will meet your expectations, or that any errors in the Service will be corrected.
		</p>

		<p><b>WHICH MEANS:</b></p>
		<p>We are not responsible if you break the law, breach this agreement or go against the rights of a third party, especially if you get sued. Service is “as is” so it may have errors or interruptions and we provide no warranties.</p>

		<p><b>6. Waiver and Complete Agreement</b></p>
		<p>The failure of Wataf to exercise or enforce any right or provision of the Terms and Conditions shall not constitute a waiver of such right or provision. The Terms and Conditions constitutes the entire agreement between you and Wataf and govern your use of the Service, superseding any prior agreements between you and Wataf (including, but not limited to, any prior versions of the Terms and Conditions).
		</p>
		<p>These Terms and Conditions make up the agreement that applies to you. This means that any previous agreements between you and Wataf doesn't apply if they conflict with these terms.
		</p>

		<p><b>7. Intellectual Property and Customer Content</b></p>
		<p>a.) We do not claim any intellectual property rights over the material you provide to the Wataf service. All material you upload remains yours. You can’t directly remove your Wataf store however, you can request to our Customer Support for the deactivation of your store. Wataf support will check and confirm for any pending transaction with your customer before we proceed to approve your request to deactivate your store.
		</p>
		<p>b.) By uploading Store Content, you agree:
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.1) to allow other internet users to view your Store Content; </p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.2) to allow Wataf to display and store your Store Content; and</p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.3) that Wataf can, at any time, review all the Store Content submitted by you to its Service.</p>
		</p>
		<p>c.) You retain ownership over all Store Content that you upload to a Wataf store; however, by making your store public, you agree to allow others to view your Store Content. You are responsible for compliance of Store Content with any applicable laws or regulations.
		</p>
		<p>d.) We will not disclose your confidential information to third parties, except as required in the course of providing our services. Confidential information includes any materials or information provided by you to us which is not publicly known. Confidential information does not include information that:
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d.1) was in the public domain at the time we received it;</p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d.2) comes into the public domain after we received it through no fault of ours;</p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d.3) we received from someone other than you without breach of our or their confidentiality obligations; or</p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d.4) we are required by law to disclose.</p>
		</p>
		<p>e.)Wataf shall have the non-exclusive right and license to use the names, trademarks, service marks and logos associated with your store to promote the Service.
		</p>

		<p><b>WHICH MEANS:</b></p>
		<p>Anything you upload remains yours and your responsibility.</p>

		<p><b>8. Independent Third Party Content and Services</b></p>
		<p>a.) Wataf is a distributor (and not a publisher) of content supplied by users and third parties. Accordingly, Wataf Shopping has no editorial control over such content. Any opinions, advice, statements, services, offers, or other information or content expressed or made available by third parties, including information providers, or any other users are those of the respective author(s) or distributor(s) and not of Wataf.
		</p>
		<p>b.) Wataf may contain links to third party websites maintained by other content providers. These links are provided solely as a convenience to you and not as an endorsement by Wataf of the contents on such third party sites, and Wataf hereby expressly disclaims any representations regarding the content or accuracy of materials on such third party websites.
		</p>
		<p>c.)  Wataf does not endorse any independent third-party and takes no responsibility for any work performed by them or failure to fulfill a work order. Links to websites of these independent third parties, announcements about services or offers, and responses to email inquiries, are provided solely for informational purposes at the discretion of Wataf and shall not be construed or imply permission, or an affiliation, position regarding any issue in controversy, authentication, appraisal, sponsorship, nor a recommendation or endorsement of any website, product, service, activity, business, organization, or person, and any offers, products, services, statements, opinions, content or information on any linked third-party website.
		</p>
		<p>d.) Under no circumstances shall Wataf be liable for any direct, indirect, incidental, special, consequential, exemplary or other damages whatsoever, that result from any contractual relationship between you and Wataf independent third parties. These limitations shall apply even if Wataf has been advised of the possibility of such damages. The foregoing limitations shall apply to the fullest extent permitted by law.
		</p>

		<p><b>WHICH MEANS:</b></p>
		<p>Any third party linked at Wataf website are not employees of Wataf shopping and we are not responsible for them.</p>
		<p>We are not responsible for third party services so use them at your own risk. If you use any third party services on the Wataf platform, you permit us to send your data to those services. If you use them you agree that we do not provide a warranty, so get advice beforehand.</p>

		<p><b>9. Cancellation and Termination</b></p>
		<p>a.) You may cancel your Account at anytime by contacting us through our Customer Support link and then following the specific instructions indicated to you in Wataf Support's response.</p>
		<p>b.) If at the date of termination of the Service, there are any outstanding Fees owing by you, you will receive one final invoice via email. Once that invoice has been paid in full, you will not be charged again.</p>
		<p>c.) We reserve the right to modify or terminate the Wataf Service or your Account for any reason, without notice at any time.</p>
		<p>d.) Fraud: Without limiting any other remedies, Wataf may suspend or terminate your Account if we suspect that you (by conviction, settlement, insurance or escrow investigation, or otherwise) have engaged in fraudulent activity in connection with the website.</p>

		<p><b>WHICH MEANS:</b></p>
		<p>To initiate a cancellation, contact our Customer Support. Wataf Support will respond with specific information regarding the cancellation process for your account. Once cancellation is confirmed, your Wataf account will no longer be active. If you cancel in the middle of your billing cycle, you’ll have one last email invoice before confirming your cancellation.</p>
		<p>We may change or cancel your account at any time. Any fraud and we will suspend or cancel your account..</p>

		<p><b>10. Modifications to the Service and Prices</b></p>
		<p>a.) Wataf reserves the right at any time, and from time to time, to modify or discontinue, the Service (or any part thereof) as we deem necessary or desirable with or without notice.</p>
		<p>b.) Wataf shall not be liable to you or to any third party for any modification, price change, suspension or discontinuance of the Service..</p>
		<p>c.) It is your sole responsibility to check the website from time to time to see any changes in the Agreement. Your use of the website after any modifications to the Agreement indicates that you agree to such modified Agreement.</p>

		<p><b>11. International Terms</b></p>
		<p>The Wataf website may be accessed from some countries around the world. You understand that some or all products or services provided on Wataf may not be available for purchase to persons residing in certain jurisdictions or geographic areas. Wataf reserves the right, in its sole discretion, to exclude or otherwise limit the provision of any product or service to a person residing in any jurisdiction or geographical area. Wataf does not represent or warrant that any product or service promoted on the website will be available for purchase by any particular person. Unless otherwise stated in the Product description or required by law, the following terms apply to all Products and/or Wataf Services:</p>
		<p>a.)Neither Wataf nor the Seller is responsible for lost or stolen products.</p>
		<p>b.)Duplicate use, reproduction, sale or trade of a product is prohibited.</p>

		<p><b>WHICH MEANS:</b></p>
		<p>The availability of the service or products may be limited to some countries.</p>
		<p>The Service is provided on an “as is” and “as available” and “with all the faults” basis without any warranty or condition, express, implied or statutory.</p>
		<p>We are not liable if you break the law.</p>

		<p><b>12. Seller Responsibility</b></p>
		<p>The Seller is obligated to honor the purchase order of the customer in compliance with the law.</p>
		<p>The Seller is obligated to provide honest product descriptions and real items. Such fraud is subject to investigation and Wataf will not be deemed liable of such fraud acts if the customer takes legal actions against the Seller.</p>
		<p>You waive and release Wataf and its officers, directors, employees and agents from any claim, liabilities, damages or injury arising from or related to any act or omission of a seller in connection with the Products/Services of the seller provided and/or as it relates to compliance with applicable unclaimed products and other laws relating to the delivery of the products or any portion thereof.</p>

		<p><b>WHICH MEANS:</b></p>
		<p>Wataf will not be held liable for legal actions taken by the customer against a fraudulent seller.</p>
	
		<p><b>13. Buyer/Customer Responsibility</b></p>
		<p>Wataf offers a platform for products/services with participating third party sellers in accordance with the terms and conditions of the website.</p>
		<p>Customers must always read the product description of every product that you want to purchase. Wataf will not be responsible for wrong orders.</p>
		<p>Communicating with the seller outside the Wataf platform is not encouraged but is not prohibited.</p>
		<p>You waive and release Wataf Shopping and its officers, directors, employees and agents from any claim, liabilities, damages or injury arising from or related to any act or omission of a seller in connection with the Products/Services of the seller provided and/or as it relates to compliance with applicable unclaimed products and other laws relating to the delivery of the products or any portion thereof.</p>

		<p><b>14. Refund, Return and Exchange Policy</b></p>
		<p>You may contact our Customer Support and provide evidence to return the item and to retrieve your full refund if you have done the returning procedure within 7 days payment confirmation period.</p>
		<p>The refund procedure shall be as follows:
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.) You must provide the following informations to our Customer Support 
		with the Subject line “Refund/Return Request”
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.1) Identification, product description and photo of the subject product, the seller whom you sought to redeem your refund/return your product.</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.2) Statement of the date, time and circumstances in which the product was discovered to be defective, wrong or damaged.</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.3) Statement, under penalty of perjury, that the product was defective, wrong, or damaged.</p>
			</p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.) We will investigate and process your request.</p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c.) We will respond to the request upon verification of the following:
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c.1) The evidence you have provided proves that your claims are true.</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c.2) We have informed and verified with the seller of your request.</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c.3) If your request is granted.</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c.4) If your request is denied and the details arising from the conclusion.</p>
			</p>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d.) We shall give your full refund or credit your Wataf Account with the full amount of the price paid for the subject product, or give the replacement within seven to fourteen (7-14) working days from your receipt of our response granting your request.</p>
		</p>
		<p><b>&copy;&nbsp;Wataf Information and Technology Inc.</b></p>
    </div><!-- end  col-sm-12-->
  </div><!-- end  container-->
</div><!-- end  terms-conditions-content-->
@endsection
