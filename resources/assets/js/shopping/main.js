$(document).ready(function () {

        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 80
        });

        // Page scrolling feature
        $('a.page-scroll').bind('click', function(event) {
            var link = $(this);
            $('html, body').stop().animate({
                scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
            $("#navbar").collapse('hide');
        });
    });

    var cbpAnimatedHeader = (function() {
        var docElem = document.documentElement,
                header = document.querySelector( '.navbar-default' ),
                didScroll = false,
                changeHeaderOn = 200;
        function init() {
            window.addEventListener( 'scroll', function( event ) {
                if( !didScroll ) {
                    didScroll = true;
                    setTimeout( scrollPage, 250 );
                }
            }, false );
        }
        function scrollPage() {
            var sy = scrollY();
            if ( sy >= changeHeaderOn ) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }
        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        init();

    })();

    // Activate WOW.js plugin for animation on scrol

$(document).ready(function () {
    new WOW().init();
	
	//material-tooltip
	//make next and previous as symbols
	$.fn.dataTable.ext.errMode = 'none';
	$.extend( true, $.fn.dataTable.defaults, {
	   
	    "language": {
		    "paginate": {
		      "previous": "<",
		      "next": ">",
		    }
		  }
	} );


	//language dropdown on header init
	headerColor = $('.top-header').css('background-color');
		//plugin
	var _ddslickLoaded = false;
	$('#language').ddslick({
		width:'min-content',
		background:headerColor,
		onSelected: function(data){
			if(_ddslickLoaded){
				currentValue = $('#language .dd-selected-value').val();
				 $.ajax({
		            url: '/locale/'+currentValue,
		            type: 'GET'
		        }).then(function (result) {
		            window.location.reload();
		        });
			}else
			{
				_ddslickLoaded = true;
			}
			
	    }  
	})

	//from php
	selectedLanguage = window.Laravel.language;
	languageIndex = 0;
	switch(selectedLanguage){
		case 'en': languageIndex = 0; break;
		case 'cn': languageIndex = 1; break;
		default: languageIndex = 0; break;
	}

	$('#language').ddslick('select', {index: languageIndex });
	$('#language .dd-pointer-down').css('display','none');
	$('#language .dd-select').css({
		border:'none',
		borderRadius:'0px'
	});
	$('#language').addClass('pull-left');
	//end of language scripts

    $.material.options.autofill = true;
	$.material.init();

    toastr.options = {
		debug: false,
		positionClass: "toast-bottom-right",
		onclick: null,
		fadeIn: 300,
		fadeOut: 1000,
		timeOut: 5000,
		extendedTimeOut: 1000
	};

	runTimeFunctions();

	//for searching : hiding the results
	$('html').click(function() {
		$('#searchContainer table').hide();
	});

	$('#searchContainer').click(function(event){
		event.stopPropagation();
		$('#searchContainer table').show();
	});


	//hovering users
	$(document).on('mouseenter','.popover-details',function(e){
		content_height = $('#popover-content').height();
		content_width = $(this).width() - 20;
		var top = ($(this).offset().top - window.scrollY) - content_height;
		var left = ($(this).offset().left - window.scrollX) + content_width;

		$("#popover-content").css({left: left +'px', top:top+'px'});
		$("#popover-content").stop(true,true).fadeIn(500);
		id = $(this).data('id');
		$('#popover-content').data('id',id);

		$('#btn_loading').show();
		$('#popover-content button').hide();
		$('#btn_container').hide();

		axiosAPIv1
			.get('/friends')
			.then( response => {
				if(id == Laravel.user.id)
				{
					$('#popover-content').find('#btnViewProfile').show();
					$('#popover-content').find('#btnAddFriend').hide();
					$('#btn_loading').hide();
					$('#btn_container').show();
				}else
				{
					$('#popover-content').find('#btnViewProfile').hide();

					found = 0;
					for(var i=0; i< response.data.friends.length; i++)
					{
						if(response.data.friends[i].user.id == id)
						{
							found = 'friends';
							break;
						}
					}
					for(var i=0; i< response.data.requests.length; i++)
					{
						if(found != 0)
						{
							break;
						}
						if(response.data.requests[i].user.id == id)
						{
							found = 'requests';
							break;
						}
					}
					for(var i=0; i< response.data.restricted.length; i++)
					{
						if(found != 0)
						{
							break;
						}
						if(response.data.restricted[i].user.id == id)
						{
							found = 'restricted';
							break;
						}
					}
					for(var i=0; i< response.data.blocked.length; i++)
					{
						if(found != 0)
						{
							break;
						}
						if(response.data.blocked[i].user.id == id)
						{
							found = 'blocked';
							break;
						}
					}

					switch(found)
					{
						case 'friends':
							// $('#popover-content #btnMessageUser').show();
							$('#popover-content #btnBlockUser').show();
							break;

						case 'requests':
							// $('#popover-content #btnMessageUser').show();
							$('#popover-content #btnCancelRequest').show();
							// $('#popover-content #btnBlockUser').show();
							break;

						case 'restricted':
							$('#popover-content #btnBlockUser').show();
							// $('#popover-content #btnRemoveRestriction').show();
							break;

						case 'blocked':
							$('#popover-content #btnUnblockUser').show();
							break;

						default:
							$('#popover-content #btnAddFriend').show();
							// $('#popover-content #btnMessageUser').show();
							$('#popover-content #btnBlockUser').show();
							break;
					}

					$('#btn_loading').hide();
					$('#btn_container').show();
				}
			})
			.catch($.noop);

	}).on('mouseleave','.popover-details',function(){
		if( $('#popover-content:hover').length  )
		{
			$('#popover-content').stop(true,true).fadeIn(500);

		}else
		{
			$('#popover-content').stop(true,true).fadeOut(500);
		}
	});

	$('body').on('mouseleave','#popover-content',function(){
		$(this).stop(true,true).fadeOut(500);
		loader = $(this).find('.loader').filter(':visible');
		content = loader.next();
		loader.stop(true,true).fadeOut(100,function(){
			content.stop(true,true).fadeIn(100);
		});
	})

	// $('#popover-content #btnAddFriend').click(function(){
	// 	axiosAPIv1
	// 		.post('/friends',{
	// 			user_id2:$('#popover-content').data('id')
	// 		})
	// 		.then(response => {
	// 			if(response.status == 201)
	// 			{
	// 				toastr.success('','Friend request sent!');
	// 				$('#popover-content').hide();
	// 			}
	// 		})
	// })

	// $('#popover-content #btnMessageUser').click(function(){
	// 	//console.log($('#popover-content').data('id'));
	// 	//console.log('show message pop up');
	// })

	// $('#popover-content #btnBlockUser').click(function(){
	// 	axiosAPIv1
	// 		.post('/friends/block',{
	// 			user_id2: $('#popover-content').data('id'),
	// 		})
	// 		.then(response => {
	// 			toastr.success('','User Blocked!');
	// 			$('#popover-content').hide();
	// 		})
	// })

	// $('#popover-content #btnUnblockUser,#popover-content #btnCancelRequest').click(function(){
	// 	axiosAPIv1
	// 		.post('/friend/find',{
	// 			user_id:Laravel.user.id,
	// 			user_id2:$('#popover-content').data('id')
	// 		})
	// 		.then(response => {
	// 			if(response.status == 200)
	// 			{

	// 				axiosAPIv1
	// 					.delete('/friends/'+response.data)
	// 					.then(response => {
	// 						if(response.status == 200)
	// 						{
	// 							toastr.success('','Success');
	// 							$('#popover-content').hide();
	// 						}
	// 					})
	// 					.catch($.noop);
	// 			}
	// 		})
	// })

	//end of hovering users

	// $('body').on('mouseover', 'span.custom-tooltip-placement', function(e) {
	// 	$('.tooltip').css({'left' : $(this).position().right, 'top' : e.pageY-20});
	// });

	

});//eof

window.isChinese = function (){
	return $('meta[name="locale-lang"]').attr('content')==='cn';
}

window.runTimeFunctions = function(){

	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover({html:true});

	loading = '<div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="7" stroke-miterlimit="10"/></svg></div>';
	$('.btn-has-loading').wrapInner( "<div class='btn-content'></div>");
	$('.btn-has-loading').prepend(loading);

    $('.btn-has-loading').click(function(){
    	content = $(this).find('.btn-content');
    	loader = $(this).find('.loader');
    	content.stop(true,true).fadeOut(100,function(){
    		loader.stop(true,true).fadeIn(100);
    	});
    });

    $( document ).ajaxSend(function(a,b,c) {
    	// if(exempted_links.indexOf(c.url) == -1)
        // {
        	// $( ".loader" ).show();
        // }
    });
    $( document ).ajaxComplete(function() {
      $( ".loader" ).hide();
		content = $('.btn-has-loading').find('.btn-content');
		loader = $('.btn-has-loading').find('.loader');
		loader.fadeOut(100,function(){
			content.fadeIn(100);
		});
    });
    $( document ).ajaxStop(function() {
      // $( ".loader" ).hide();
      content = $('.btn-has-loading').find('.btn-content');
		loader = $('.btn-has-loading').find('.loader');
		loader.fadeOut(100,function(){
			content.fadeIn(100);
		});

    });
     $( document ).ajaxSuccess(function() {
    //   $( ".loader" ).hide();
    content = $('.btn-has-loading').find('.btn-content');
		loader = $('.btn-has-loading').find('.loader');
		loader.fadeOut(100,function(){
			content.fadeIn(100);
		});
    });
    $( document ).ajaxError(function() {
      // $( ".loader" ).hide();
      content = $('.btn-has-loading').find('.btn-content');
		loader = $('.btn-has-loading').find('.loader');
		loader.fadeOut(100,function(){
			content.fadeIn(100);
		});
    });


}

/*!
 * jQuery Textarea AutoSize plugin
 * Author: Javier Julio
 * Licensed under the MIT license
 */
;(function ($, window, document, undefined) {

  var pluginName = "textareaAutoSize";
  var pluginDataName = "plugin_" + pluginName;

  var containsText = function (value) {
    return (value.replace(/\s/g, '').length > 0);
  };

  function Plugin(element, options) {
    this.element = element;
    this.$element = $(element);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var height = this.$element.outerHeight();
      var diff = parseInt(this.$element.css('paddingBottom')) +
                 parseInt(this.$element.css('paddingTop')) || 0;

      if (containsText(this.element.value)) {
        this.$element.height(this.element.scrollHeight - diff);
        //console.log(this.element.scrollHeight - diff);
      }

      // keyup is required for IE to properly reset height when deleting text
      this.$element.on('input keyup', function(event) {
        var $window = $(window);
        var currentScrollPosition = $window.scrollTop();

        $(this)
          .height(0)
          .height(this.scrollHeight - diff);

        var id = $(this).attr('data-chat');
        var h = this.scrollHeight - diff;

		if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ){
	        if(h==40)
	          $('#'+id).css('height','250');
	        else if(h==51)
	          $('#'+id).css('height','229');
	        else if(h==69)
	          $('#'+id).css('height','221');
	        else if(h==86)
	          $('#'+id).css('height','204');
	        else if(h==103)
	          $('#'+id).css('height','196');
		}

		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
	        if(h==36)
	          $('#'+id).css('height','250');
	        else if(h==51)
	          $('#'+id).css('height','235');
	        else if(h==68)
	          $('#'+id).css('height','218');
	        else if(h==85)
	          $('#'+id).css('height','201');
	        else if(h==102)
	          $('#'+id).css('height','196');
		}

        $window.scrollTop(currentScrollPosition);
      });
    }
  };

  $.fn[pluginName] = function (options) {
    this.each(function() {
      if (!$.data(this, pluginDataName)) {
        $.data(this, pluginDataName, new Plugin(this, options));
      }
    });
    return this;
  };

})(jQuery, window, document);

    // Session (flash) message - Toastr
    if (window.Laravel.message) {
        toastr.success(window.Laravel.message, null, {
            positionClass: 'toast-top-right'
        });
    }

window.numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}







