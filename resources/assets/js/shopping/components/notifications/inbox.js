window.WycNotifInbox = new Vue({
    el: '#inbox',

    data: {
        notifications: [],
        $messageContainer: null,
    },

    watch: {
        notifications(val) {
            setTimeout(() => {
                this.$messageContainer.perfectScrollbar('update');
            }, 1);
        }
    },

    methods: {
        getNotifications() {
            return axiosAPIv1.get('/messages/notifications2')
                .then(result => {
                    this.notifications = result.data;
                });
        },

        getNotifications2() {
            return axiosAPIv1.get('/messages/notifications2')
                .then(result => {
                    this.notifications = result.data;

                    if (this.notifications) {
                        var chat = [];
                        $(".panel-chat-box").each(function(){
                            chat.push($(this).attr('id'));
                        });
                        var oxp = false;
                        this.notifications.forEach(function (notif) {
                            if(notif.user_id != window.Laravel.user.id){
                                if (! notif.seen_at && oxp == false) {
                                    if (notif.chat_room.name) {
                                        if($.inArray('user'+notif.chat_room.name,chat)=='-1')
                                            Event.fire('chat-box.show', notif);

                                    } else {
                                         if($.inArray('user'+notif.chat_room.users[0].id,chat)=='-1')
                                            Event.fire('chat-box.show', notif.chat_room.users[0]);
                                   
                                    }
                                    oxp = true;
                                }
                            }
                        });
                    }
                    //notif sound
                    var audio = new Audio('/sound/wycnotif.wav');
                    audio.play();
                });
        },


        markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (! notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });

            this.getNotifications();

            return axiosAPIv1.post('/messages/mark-all-as-seen2');
        }
    },

    computed: {
        count() {
            let count = 0;
            
            if (this.notifications) {
                this.notifications.forEach(function (notif) {
                    if(notif.user_id != window.Laravel.user.id){
                        if (! notif.seen_at) {
                            ++count;
                        }
                    }
                });
            }

            return count || '';
        }
    },

    components: {
        'inbox-message': require('./InboxMessage.vue')
    },

    created() {
        this.getNotifications();

        Event.listen('new-chat-message', this.getNotifications2);
    },

    mounted() {
        this.$messageContainer = $(this.$el).find('.panel-body');
        this.$messageContainer.perfectScrollbar();
    }
});
