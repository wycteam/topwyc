window.WycNotifGeneral = new Vue({
    el: '#notification',

    data: {
        notifications: [],
        $container: null,
    },

    watch: {
        notifications(val) {
            setTimeout(() => {
                this.$container.perfectScrollbar('update');
            }, 1);
        }
    },

    methods: {
        getNotifications() {
            return axiosAPIv1.get('/notifications')
                .then(result => {
                    this.notifications = result.data;
                });
        },

        //upcoming notif with sound
        getNotifications2() {
            return axiosAPIv1.get('/notifications')
                .then(result => {
                    this.notifications = result.data;
                    var audio = new Audio('/sound/wycnotif.wav');
                    audio.play();
                });
        },

        markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (! notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });

            return axiosAPIv1.post('/notifications/mark-all-as-seen');
        },

        removeNotif(data) {
            var index = this.notifications.indexOf(data);

            this.notifications.splice(index, 1);
        }
    },

    computed: {
        count() {
            let count = 0;

            if (this.notifications) {
                this.notifications.forEach(function (notif) {
                    if (! notif.seen_at) {
                        ++count;
                    }
                });
            }

            return count || '';
        }
    },

    components: {
        notification: require('./Notification.vue')
    },

    created() {
        this.getNotifications();

        Event.listen('friend-request-accepted', this.getNotifications2);
        Event.listen('feedback-approved', this.getNotifications2);
        Event.listen('document-approved', this.getNotifications2);
        Event.listen('document-denied', this.getNotifications2);
        Event.listen('expired-document', this.getNotifications2);
        Event.listen('new-purchase', this.getNotifications2);
        Event.listen('order-status-update', this.getNotifications2);
        Event.listen('change-ewallet-transaction-status', this.getNotifications2);
        Event.listen('new-product-review', this.getNotifications2);
        Event.listen('new-product-review-comment', this.getNotifications2);
        Event.listen('new-ewallet-transaction', this.getNotifications2);
    },

    mounted() {
        this.$container = $(this.$el).find('.panel-body');
        this.$container.perfectScrollbar();

        $('[data-toggle="tooltip"]').tooltip();

        $('.dropdown-general-notif').on('shown.bs.dropdown', function () {
            $('.notif-body', this).ellipsis({
                lines: 2,
                responsive: true
            });
        });
    }
});
