window.WycNotifFriendRequest = new Vue({
    el: '#friend-request',

    data: {
        notifications: [],
        notSeenCount: null,
        $container: null,
    },

    watch: {
        notifications(val) {
            setTimeout(() => {
                this.$container.perfectScrollbar('update');
            }, 1);
        }
    },

    methods: {
        getNotifications() {
            return axiosAPIv1.get('/friends/notifications')
                .then(result => {
                    this.notifications = result.data.notifications;
                    this.notSeenCount = result.data.notseen_count;
                });
        },

        markAllAsSeen() {
            return axiosAPIv1.post('/friends/mark-all-as-seen')
                .then(result => {
                    this.notSeenCount = null;
                });
        },

        accepted(data) {
            var index = this.notifications.indexOf(data);

            this.getNotifications();
        },

        deleted(data) {
            var index = this.notifications.indexOf(data);

            this.getNotifications();
        }
    },

    computed: {
        count() {
            return this.notSeenCount ? this.notSeenCount : '';
        }
    },

    components: {
        'request': require('./Request.vue')
    },

    created() {
        this.getNotifications();

        Event.listen('new-friend-request', this.getNotifications);
        Event.listen('friend-request.accepted', this.accepted);
        Event.listen('friend-request.deleted', this.deleted);
    },

    mounted() {
        this.$container = $(this.$el).find('.panel-body');
        this.$container.perfectScrollbar();
    }
});

