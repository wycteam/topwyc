

Vue.component('news-articles', require('../visa/components/news/News.vue'));
Vue.component('news-articles-featured', require('../visa/components/news/Featured_news.vue'));

new Vue({
  el: '#newsLoader',
});

new Vue({
  el: '#newsLoader2',

});

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format("YYYY-MM-DD");
  }
});
