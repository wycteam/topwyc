import {TheMask} from 'vue-the-mask'

window.openReg = new Vue({
  el: '#openRegister',

   data: {
   		translation:null,
   		loading : true,
   		verify : false,
   		verify_data : [],
   		refcode : null,
   		found : [],
   		language : '',
        form: new Form({
            first_name: '',
            last_name: '',
            gender: 'Male',
            email: '',
            cp_num: null,
            // password: null,
            // confirm_password: null,
            bday: null,
            // month: '',
            // day:'',
            // year:'',
            referral: null,
            referral_id: null,
        }, { baseURL: '/' }),

        form2: new Form({
        	verification: '',
        	user_id: ''
        }, { baseURL: '/' })
    },

	methods:{
		getTranslation(){
			axios.get('/open/get-translation')
			        .then(response => {
			          	this.translation = response.data.translation;
			          	this.translateMonths();
			        });
		},
		translateMonths(){
				$('#openRegister .day > option:first-child').text((this.translation)?this.translation['bday']:"Birth Day");
				$('#openRegister .month > option:first-child').text((this.translation)?this.translation['month']:"Month");
				$('#openRegister .year > option:first-child').text((this.translation)?this.translation['year']:"Year");

				$('#openRegister .month > option:nth-child(2)').text((this.translation)?this.translation['january']:"January");//january
				$('#openRegister .month > option:nth-child(3)').text((this.translation)?this.translation['february']:"February");//february
				$('#openRegister .month > option:nth-child(4)').text((this.translation)?this.translation['march']:"March");//march
				$('#openRegister .month > option:nth-child(5)').text((this.translation)?this.translation['april']:"April");//april
				$('#openRegister .month > option:nth-child(6)').text((this.translation)?this.translation['may']:"May");//may
				$('#openRegister .month > option:nth-child(7)').text((this.translation)?this.translation['june']:"June");//june
				$('#openRegister .month > option:nth-child(8)').text((this.translation)?this.translation['july']:"July");//july
				$('#openRegister .month > option:nth-child(9)').text((this.translation)?this.translation['august']:"August");//august
				$('#openRegister .month > option:nth-child(10)').text((this.translation)?this.translation['september']:"September");//september
				$('#openRegister .month > option:nth-child(11)').text((this.translation)?this.translation['october']:"October");//october
				$('#openRegister .month > option:nth-child(12)').text((this.translation)?this.translation['november']:"November");//november
				$('#openRegister .month > option:nth-child(13)').text((this.translation)?this.translation['december']:"December");//december

		},
		init() {
			let pathArray = window.location.pathname.split( '/' );
			if(pathArray[2]){
				this.refcode = pathArray[2];
				this.getDecodedReferral(this.refcode);
			}
		},

      	getDecodedReferral(refcode) {
	        axios.get('/open/get-decoded-referral/'+refcode)
	              .then(response => {
	                  let data = response.data;
	                  this.found = response.data;
	                  if(data.found){
	                  	this.form.referral = 'Promo Code : '+data.id;
	                  	this.form.referral_id = data.id;
	                  	$("#referral").prop('disabled', true);
	                  }
	                  //this.project = data;
	              })
	              .catch(errors => {
	                  console.log(errors);
	              });
	     },

	    ChangeInputType(input){
	    	// alert($('#'+input).val());
	    	if($('#'+input).val() == ''){
	    		$('#'+input).attr('type','text');
	    	}
	    },

	    submit(e) {
            $("#regsubmit").prop('disabled', true);
            this.form.bday = $('#bday').val();
            if(this.form.bday == ''){
            	$('.month').addClass("error-required");
            	$('.day').addClass("error-required");
            	$('.year').addClass("error-required");
            }else{
            	$('.month').removeClass("error-required");
            	$('.day').removeClass("error-required");
            	$('.year').removeClass("error-required");
            }
            this.form.submit('post', e.currentTarget.action)
                .then(result => {
                    // window.location.href = '/open-register/' ;
                    if(result.success == false){
                    	swal({
		                    title: (this.translation)?this.translation['msg-user-registered']:"User already registered.",
		                    type: 'info',
		                    confirmButtonText:(this.translation)?this.translation['ok']:"Okay",
		                });
                    	$("#regsubmit").prop('disabled', false);
                    }
                    this.verify = result.success;
                    this.form2.user_id = result.id;
                    this.verify_data = result;
                    
                })
              .catch(error => {

                setTimeout(function(){
                $("#regsubmit").prop('disabled', false);
                },1000);
              });
        },

		resendCode() {
            this.form2.submit('post', 'open/resend-code')
                .then(result => {
                if(result.success == true){
                	swal({
		                    title: (this.translation)?this.translation['msg-code-sent'] : "Code successfully sent. Please check the code we've sent to your mobile number.",
		                    type: 'info',
		                    confirmButtonText:(this.translation)?this.translation['ok']:"Okay",
		                });
	              } 
                })
              .catch(error => {
              });
        },

        submitVerification() {
            $("#regverify").prop('disabled', true);
            this.form2.submit('post', 'open/save-verification')
                .then(result => {
                if(result.success == true){
                	window.onbeforeunload = null;
                	var isMobile = {
					    Android: function() {
					        return navigator.userAgent.match(/Android/i);
					    },
					    BlackBerry: function() {
					        return navigator.userAgent.match(/BlackBerry/i);
					    },
					    iOS: function() {
					        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
					    },
					    Opera: function() {
					        return navigator.userAgent.match(/Opera Mini/i);
					    },
					    Windows: function() {
					        return navigator.userAgent.match(/IEMobile/i);
					    },
					    any: function() {
					        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
					    }
					};


					var url = '/';
					if(isMobile.Android()){
						window.location.href = '/android-page' ;
						// url = 'https://play.google.com/store/apps/details?id=com.wyc.visatrackerapp&hl=en_US';
						//     swal({
						// 		title: "Successfully registered, Please download our app, you can login using your mobile number as username and password.",
					 //            type: 'info',
					 //            allowOutsideClick: false,
					 //            showCloseButton: true,
						// 		html: "" +
						//             "<br>" +
						//             '<a href="https://play.google.com/store/apps/details?id=com.wyc.visatrackerapp" class=" btn btn-success btn-lg">' + ' Google Play' + '</a>' +
						//             '<a href="https://appstore.huawei.com/app/C100859187" class=" btn btn-success btn-lg m-l-xs">' + ' Huawei Store' + '</a>' +
						//             '<a href="http://topwyc.com/apk/wycgrouptracker.apk" class=" btn btn-success btn-lg m-l-xs">' + ' Download APK' + '</a>',
						//         showCancelButton: false,
						//         showConfirmButton: false
						// 	});
					}
					else if(isMobile.iOS()){
						window.location.href = '/android-page' ;
						// url = 'https://itunes.apple.com/ph/app/wyc-group-tracker/id1360927056?mt=8';
						// swal({
		    //                 title: "Successfully registered, Please download our app, you can login using your mobile number as username and password.",
		    //                 type: 'success',
		    //                 showCancelButton: false,
		    //                 confirmButtonColor: '#3085d6',
		    //                 confirmButtonText: 'Proceed'
		    //             })
		    //             .then(function(response) {
		    //                window.location.href = url ;
		    //             })
		    //             .catch(function(response) {

		    //             });
					}
					else{	
						window.location.href = '/android-page' ;
		                // swal({
		                //     title: "Successfully registered, Please download our app, you can login using your mobile number as username and password.",
		                //     type: 'success',
		                //     showCancelButton: false,
		                //     confirmButtonColor: '#3085d6',
		                //     confirmButtonText: 'Proceed'
		                // })
		                // .then(function(response) {
		                //    window.location.href = url ;
		                // })
		                // .catch(function(response) {

		                // });
					}

                    this.verify = result.success;
                    this.form2.user_id = result.id;
                    this.verify_data = result;
	              } 
	              else{
	              	var msg = '';
	              	if(this.form2.verification == ''){
	              		msg = (this.translation)?this.translation['msg-four-digit-sent']:"A 4-digit code will be sent to your cellphone number to ensure you have access to the cellphone number you entered. Please enter the code to proceed!"
	              	}
	              	else{
	              		msg = (this.translation)?this.translation['msg-enter-code-proceed']:"Please enter the correct code to proceed.";
	        	    }
	              		swal({
		                    title: msg,
		                    type: 'info',
		                    confirmButtonText:(this.translation)?this.translation['ok']:"Okay",
			                });
		                $("#regverify").prop('disabled', false);
	              } 
                })
              .catch(error => {
                setTimeout(function(){
                $("#regverify").prop('disabled', false);
                },1000);
              });
        },
	 },
    created() {
    	this.getTranslation();
    	this.init();
    	
    	
  // 		 setTimeout(() => {
		// 	this.loading = false;
		// }, 1000);
    },
    mounted() {

    	$("#birth_date").datepicker().on(
     		"changeDate", () => {
     			this.form.bday = $('#birth_date').val();
     		}
		);
		$('input#birth_date').on('change', ()=>{
			this.form.bday = $('#birth_date').val();
		});



    },
    components: {
    	TheMask
    },
});


// $(document).ready(function() {
// 	var language = navigator.language;
//     	//if(window.Laravel.language != language){
//     		// alert(language);	
//     	    	if(language == 'zh-CN'){
// 		    		language = 'cn';
// 		    	}
// 		    	else{
// 		    		'en';
// 		    	}	

// 	    	$.ajax({
// 	            url: '/locale/'+language,
// 	            type: 'GET'
// 	        }).then(function (result) {
// 	            if(!window.location.hash) {
// 			        window.location = window.location + '#loaded';
// 			        window.openReg.init();
// 			    }
// 	        });
//     	//}
//     })
$(function(){
		var language = navigator.language;
    	//if(window.Laravel.language != language){
    		// alert(language);	
    	    	if(language == 'zh-CN'){
		    		language = 'cn';
		    	}
		    	else{
		    		'en';
		    	}	

	    	$.ajax({
	            url: '/locale/'+language,
	            type: 'GET'
	        }).then(function (result) {
	            if(!window.location.hash) {
			        window.location = window.location + '#loaded';
			        window.openReg.init();
			    }
	        });


	$('.form-holder').focusin(function(){
		$('.form-holder').removeClass("active");
		$(this).addClass("active");
	})

    window.onbeforeunload = function(event)
    {
        return confirm("Confirm refresh");
    };



$("#phone").keypress(function (e) {
	//if the letter is not digit then display error and don't type anything
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
	}
});
	// $('#referral').popover({title:'Please Enter Mobile Number',placement:'top'});
})


