


Vue.component('news-articles-featured', require('../visa/components/news/innerFeatured.vue'));


new Vue({
  el: '#newsLoader2',
});

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format("YYYY-MM-DD");
  }
});
