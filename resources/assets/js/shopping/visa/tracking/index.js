Vue.component('dialog-modal',  require('../../components/DialogModal.vue'));
Vue.component('report-list',  require('../components/tracking/Reports.vue'));
var Result = new Vue({
	el:'#result',
    data:{
        activeReport:[],
        searchTrack: new Form({
            tracking    : '',
        },{ baseURL     : ''}),

        scoreA : 0,
        scoreB : 0,
        scoreC : 0,
        scoreD : 0,
        scoreE : 0,
        scoreF : 0,

        q1 : [
          {
            id: 1,
            question : '能理智地应付困境',
          },
          {
            id: 2,
            question : '善于从失败中吸取经验',
          },
          {
            id: 3,
            question: '制定一些克服困难的计划并按计划去做',
          },
          {
            id: 4,
            question: '常希望自己已经解决了面临的困难',
          },
          {
            id: 5,
            question: '对自己取得成功的能力充满信心',
          },
          {
            id: 6,
            question: '认为“人生经历就是磨难”',
          },
          {
            id: 7,
            question: '常感叹生活的艰难',
          },
          {
            id: 8,
            question: '专心于工作或者学习以忘却不快',
          },
          {
            id: 9,
            question: '常认为“生死有命，富贵在天”',
          },
          {
            id: 10,
            question: '喜欢找人聊天以减轻烦恼',
          },
          {
            id: 11,
            question: '请求别人帮助自己克服困难',
          },
          {
            id: 12,
            question: '常只按自己想的做，且不考虑后果',
          },
          {
            id: 13,
            question: '不愿过多思考影响自己的情绪的问题',
          },
          {
            id: 14,
            question: '投身其他社会活动，寻找新的寄托',
          },
          {
            id: 15,
            question: '常自暴自弃',
          },
          {
            id: 16,
            question: '常以无所谓的态度来掩饰内心的感受',
          },
          {
            id: 17,
            question: '常想“这不是真的就好了”',
          },
          {
            id: 18,
            question: '认为自己的失败多是外因所致',
          },
          {
            id: 19,
            question: '对困难等待观望，任其发展的态度',
          },
          {
            id: 20,
            question: '与人冲突，多是对方性格怪异引起',
          },
          {
            id: 21,
            question: '常向引起问题的人和事发脾气',
          },
          {
            id: 22,
            question: '常幻想自己有克服困难的超人本领',
          },
          {
            id: 23,
            question: '常自我责备',
          },
          {
            id: 24,
            question: '常用睡觉的方式来逃避痛苦',
          },
          {
            id: 25,
            question: '常借娱乐活动来消除烦恼',
          },
          {
            id: 26,
            question: '常想些高兴的事自我安慰',
          },
          {
            id: 27,
            question: '避开困难以求心中宁静',
          },
          {
            id: 28,
            question: '为不能回避困难而懊恼',
          },
          {
            id: 29,
            question: '常用两种以上的办法解决问题',
          },
          {
            id: 30,
            question: '常认为没有必要那么费力去争取成败',
          },
          {
            id: 31,
            question: '努力去改变现状，使情况向好的一面转化',
          },
          {
            id: 32,
            question: '借烟或借酒消愁',
          },
          {
            id: 33,
            question: '常责怪他人',
          },
          {
            id: 34,
            question: '对困难常采用回避的态度',
          },
          {
            id: 35,
            question: '认为“退一步自然宽”',
          },
          {
            id: 36,
            question: '把不愉快的事埋在心里',
          },
          {
            id: 37,
            question: '常自卑自怜',
          },
          {
            id: 38,
            question: '常认为这是生活对自己不公平的表现',
          },
          {
            id: 39,
            question: '常压抑内心的愤怒与不满',
          },
          {
            id: 40,
            question: '吸取自己或他人的经验去应付困难',
          },
          {
            id: 41,
            question: '常不相信那些对自己不利的事',
          },
          {
            id: 42,
            question: '为了自尊，常不愿意让人知道自己的遭遇',
          },
          {
            id: 43,
            question: '常与同事，朋友一起讨论解决问题的办法',
          },
          {
            id: 44,
            question: '常告诫自己“能忍者自安”',
          },
          {
            id: 45,
            question: '常祈祷神灵保佑',
          },
          {
            id: 46,
            question: '常用幽默或玩笑的方式缓解冲突或不快',
          },
          {
            id: 47,
            question: '自己能力有限，只有忍耐',
          },
          {
            id: 48,
            question: '常怪自己没出息',
          },
          {
            id: 49,
            question: '常爱幻想一些不现实的事来消除烦恼',
          },
          {
            id: 50,
            question: '常抱怨自己无能',
          },
          {
            id: 51,
            question: '常能看到坏事中有好的一面',
          },
          {
            id: 52,
            question: '自感挫折是对自己的考验',
          },
          {
            id: 53,
            question: '向有经验的亲友, 师长求教解决问题解决的方法',
          },
          {
            id: 54,
            question: '平心静气淡化烦恼',
          },
          {
            id: 55,
            question: '努力寻找解决问题的办法',
          },
          {
            id: 56,
            question: '选择职业不当，是自己常遇挫折的主要原因',
          },
          {
            id: 57,
            question: '总怪自己不好',
          },
          {
            id: 58,
            question: '经常看破红尘，不在乎自己的不幸遭遇',
          },
          {
            id: 59,
            question: '常自感运气不好',
          },
          {
            id: 60,
            question: '向他人诉说心中的烦恼',
          },
          {
            id: 61,
            question: '常自感无所作为而任其自然',
          },
          {
            id: 62,
            question: '寻求别人的理解和同情',
          },
        ],

        q2 : [
           {
            id:1,
            question : '头痛。',
          },
          {
            id: 2,
            question : '神经过敏，心中不踏实。',
          },
          {
            id: 3,
            question : '头脑中有不必要的想法或字句盘旋。',
          },
          {
            id: 4,
            question : '头昏或昏倒。',
          },
          {
            id: 5,
            question : '对异性的兴趣减退。',
          },
          {
            id: 6,
            question : '对旁人责备求全。',
          },
          {
            id: 7,
            question : '感到别人能控制您的思想',
          },
          {
            id: 8,
            question : '责怪别人制造麻烦。',
          },
          {
            id: 9,
            question : '忘性大。',
          },
          {
            id: 10,
            question : '担心自己的衣饰整齐及仪态的端正。',
          },
          {
            id: 11,
            question : '容易烦恼和激动。',
          },
          {
            id: 12,
            question : '胸痛。',
          },
          {
            id: 13,
            question : '害怕空旷的场所或街道。',
          },
          {
            id: 14,
            question : '感到自己的精力下降，活动减慢。',
          },
          {
            id: 15,
            question : '想结束自己的生命。',
          },
          {
            id: 16,
            question : '听到旁人听不到的声音。',
          },
          {
            id: 17,
            question : '发抖。',
          },
          {
            id: 18,
            question : '感到大多数人都不可信任。',
          },
          {
            id: 19,
            question : '胃口不好。',
          },
          {
            id: 20,
            question : '容易哭泣。',
          },
          {
            id: 21,
            question : '同异性相处时感到害羞，不自在。',
          },
          {
            id: 22,
            question : '感到受骗、中了圈套或有人想抓住你。',
          },
          {
            id: 23,
            question : '无缘无故地突然感到害怕。',
          },
          {
            id: 24,
            question : '自己不能控制地大发脾气。',
          },
          {
            id: 25,
            question : '怕单独出门。',
          },
          {
            id: 26,
                question : '经常责怪自己。',
          },
          {
            id: 27,
                question : '腰痛。',
          },
          {
            id: 28,
                question : '感到难以完成任务。',
          },
          {
            id: 29,
                question : '感到孤独。',
          },
          {
            id: 30,
                question : '感到苦闷。',
          },
          {
            id: 31,
                question : '过分担忧。',
          },
          {
            id: 32,
                question : '对事物不感兴趣。',
          },
          {
            id: 33,
                question : '感到害怕。',
          },
          {
            id: 34,
                question : '你的感情容易受到伤害。',
          },
          {
            id: 35,
                question : '旁人能知道你的私下想法。',
          },
          {
            id: 36,
                question : '感到别人不理解你、不同情你。',
          },
          {
            id: 37,
                question : '感到人们对你不友好、不喜欢你。',
          },
          {
            id: 38,
                question : '做事必须做得很慢以保证做得正确。',
          },
          {
            id: 39,
                question : '心跳得很厉害。',
          },
          {
            id: 40,
                question : '恶心或胃部不舒服。',
          },
          {
            id: 41,
                question : '感到比不上他人。',
          },
          {
            id: 42,
                question : '肌肉酸痛。',
          },
          {
            id: 43,
                question : '感到有人在监视你、谈论你。',
          },
          {
            id: 44,
                question : '难以入睡。',
          },
          {
            id: 45,
                question : '做事必须反复检查。',
          },
          {
            id: 46,
                question : '难以作出决定。',
          },
          {
            id: 47,
                question : '怕乘电车、公共汽车、地铁或火车之类的。',
          },
          {
            id: 48,
                question : '呼吸有困难。',
          },
          {
            id: 49,
                question : '一阵阵发冷或发热。',
          },
          {
            id: 50,
                question : '因为感到害怕而避开某些东西、场合或活动',
          },
          {
            id: 51,
                question : '脑子变空了。',
          },
          {
            id: 52,
                question : '身体发麻或刺痛。',
          },
          {
            id: 53,
                question : '喉咙有梗塞感。',
          },
          {
            id: 54,
                question : '感到前途没有希望。',
          },
          {
            id: 55,
                question : '不能集中注意力。',
          },
          {
            id: 56,
                question : '感到身体的某一部分软弱无力。',
          },
          {
            id: 57,
                question : '感到紧张或容易紧张。',
          },
          {
            id: 58,
                question : '感到手或脚发重。',
          },
          {
            id: 59,
                question : '想到死亡的事。',
          },
          {
            id: 60,
                question : '吃得太多。',
          },
          {
            id: 61,
                question : '当别人看着你或谈论你时感到不自在。',
          },
          {
            id: 62,
                question : '有一些不属于你自己的想法。',
          },
          {
            id: 63,
                question : '有想打人或伤害他人的冲动。',
          },
          {
            id: 64,
                question : '醒得太早。',
          },
          {
            id: 65,
                question : '必须反复洗手、点数目或触摸某些东西。',
          },
          {
            id: 66,
                question : '睡得不稳不深。',
          },
          {
            id: 67,
                question : '有想摔坏或破坏东西的冲动。',
          },
          {
            id: 68,
                question : '有一些别人没有的想法或念头。',
          },
          {
            id: 69,
                question : '感到对别人神经过敏。',
          },
          {
            id: 70,
                question : '在商店或电影院等人多的地方感到不自在。',
          },
          {
            id: 71,
                question : '感到任何事情都很困难。',
          },
          {
            id: 72,
                question : '一阵阵恐惧或惊恐。',
          },
          {
            id: 73,
                question : '感到在公共场合吃东西很不舒服。',
          },
          {
            id: 74,
                question : '经常与人争论。',
          },
          {
            id: 75,
                question : '单独一人时神经很紧张。',
          },
          {
            id: 76,
                question : '别人对你的成绩没有作出恰当的评价。',
          },
          {
            id: 77,
                question : '即使和别人在一起也感到孤单。',
          },
          {
            id: 78,
                question : '感到坐立不安、心神不定。',
          },
          {
            id: 79,
                question : '感到自己没有什么价值。',
          },
          {
            id: 80,
                question : '感到熟悉的东西变得陌生或不像是真的了。',
          },
          {
            id: 81,
                question : '大叫或摔东西。',
          },
          {
            id: 82,
                question : '害怕会在公共场合昏倒。',
          },
          {
            id: 83,
                question : '感到别人想占你的便宜。',
          },
          {
            id: 84,
                question : '为一些有关“性”的想法而很苦恼。',
          },
          {
            id: 85,
            question : '你认为应该因为自己的过错而受到惩罚。',
          },
          {
            id: 86,
                question : '感到要赶快把事情做完。',
          },
          {
            id: 87,
                question : '感到自己的身体有严重问题。',
          },
          {
            id: 88,
            question : '从未感到和其他人很亲近。',
          },
          {
            id: 89,
            question : '感到自己有罪。',
          },
          {
            id: 90,
            question : '感到自己的脑子有毛病。',
          },
        ],

        somatization : 0,
        compulsive : 0,
        interpersonal : 0,
        depression : 0,
        anxiety : 0,
        hostile : 0,
        horror : 0,
        paranoid : 0,
        psychiatric : 0,
        others : 0,
        totalScore : 0,
        averageScore : 0,
        positives : 0,
        negatives : 0,
    },
    methods:{
    	showReportModal(i){
            axios.get('/tracking/reports/'+i)
            .then(result => {
              this.activeReport = result.data;
            });
           $('#reportModal').modal('toggle');
    	},
        searchEnter: function (e) {
            this.searchTrack.submit('get', '/tracking/result/'+this.searchTrack.tracking)
            .then(result => {
                var url = "/tracking/result/"+this.searchTrack.tracking;
                window.open(url,'_blank');
                this.searchTrack.tracking = "";
            })
        },
        search(){
             this.searchTrack.submit('get', '/tracking/result/'+this.searchTrack.tracking)
            .then(result => {
                window.location = "/tracking/result/"+this.searchTrack.tracking;
            })
        },

        submitQ2(){
            this.somatization = 0;
            this.compulsive = 0;
            this.interpersonal = 0;
            this.depression = 0;
            this.anxiety = 0;
            this.hostile = 0;
            this.horror = 0;
            this.paranoid = 0;
            this.psychiatric = 0;
            this.others = 0;
            this.totalScore = 0;
            this.averageScore = 0;
            this.positives = 0;
            this.negatives = 0;

            var $questions = $(".quiz2");
            // alert($questions.find("input:radio:checked").length+" "+$questions.length);
            if($questions.find("input:radio:checked").length === $questions.length) {
                var somatization = [1, 4, 12, 27, 40, 42, 48, 49, 52, 53, 56, 58];
                var compulsive = [3, 9, 10, 28, 38, 45, 46, 51, 55, 65];
                var interpersonal = [6, 21, 34, 36, 37, 41, 61, 69 ,73];
                var depression = [5, 14, 15, 20, 22, 26, 29, 30, 31, 32, 54, 71, 79];
                var anxiety = [2,17,23,33,39,57,72,78,80,86];
                var hostile = [11, 24, 63, 67, 74, 81];
                var horror = [13, 25, 47, 50, 70, 75, 82];
                var paranoid = [8,18,43,68,76,83];
                var psychiatric = [7, 16, 35, 62, 77, 84, 85, 87, 88, 90];
                var others = [19,44,59,60,64,66,89];


                for(var i = 1;i<=90; i++){
                    // for computation of somatization
                    if(jQuery.inArray(i, somatization) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.somatization+= parseInt(ans);
                    }
                    //end somatization

                    // for computation of compulsive
                    if(jQuery.inArray(i, compulsive) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.compulsive+= parseInt(ans);
                    }
                    //end somatization

                    // for computation of interpersonal
                    if(jQuery.inArray(i, interpersonal) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.interpersonal+= parseInt(ans);
                    }
                    //end somatization

                    // for computation of depression
                    if(jQuery.inArray(i, depression) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.depression+= parseInt(ans);
                    }
                    //end depression

                    // for computation of anxiety
                    if(jQuery.inArray(i, anxiety) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.anxiety+= parseInt(ans);
                    }
                    //end anxiety

                    // for computation of hostile
                    if(jQuery.inArray(i, hostile) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.hostile+= parseInt(ans);
                    }
                    //end hostile

                    // for computation of horror
                    if(jQuery.inArray(i, horror) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.horror+= parseInt(ans);
                    }
                    //end horror

                    // for computation of paranoid
                    if(jQuery.inArray(i, paranoid) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.paranoid+= parseInt(ans);
                    }
                    //end paranoid

                    // for computation of psychiatric
                    if(jQuery.inArray(i, psychiatric) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.psychiatric+= parseInt(ans);
                    }
                    //end psychiatric

                    // for computation of others
                    if(jQuery.inArray(i, others) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        this.others+= parseInt(ans);
                    }
                    //end others

                    this.totalScore = this.somatization + this.compulsive + this.interpersonal + this.depression + this.anxiety 
                                    + this.hostile + this.horror + this.paranoid + this.psychiatric + this.others;

                    this.averageScore = this.totalScore/90;

                    var ans = $("input[name='row"+i+"']:checked").val();
                    if(parseInt(ans)> 0){
                        this.positives += parseInt(ans);
                    }
                    if(parseInt(ans)== 0){
                        this.negatives += parseInt(ans);
                    }
                }
            }
            else{
                swal({
                    title: "You need to answer all questions.",
                    type: 'info',
                    confirmButtonText:"Okay",
                });
            }
        },

        submitQ1(){
            this.scoreA = 0;
            this.scoreB = 0;
            this.scoreC = 0;
            this.scoreD = 0;
            this.scoreE = 0;
            this.scoreF = 0;
            let vm = this;
            var $questions = $(".quiz1");
            // alert($questions.find("input:radio:checked").length+" "+$questions.length);
            if($questions.find("input:radio:checked").length === $questions.length) {
                var totalA = [1, 2, 3, 5, 8, 29, 31, 40, 46, 51, 55];
                var totalB = [15, 23, 25, 37, 48, 50, 56, 57, 59];
                var totalC = [10, 11, 14, 43, 53, 60, 62];
                var totalD = [4, 12, 17, 21, 22, 26, 28, 41, 45, 49];
                var totalE = [7, 13, 16, 24, 27, 32, 34, 35, 44, 47];
                var totalF = [6, 9, 18, 20, 30, 33, 38, 52, 54, 58, 61];

                for(var i = 1;i<=62; i++){
                    // for computation of A
                    if(jQuery.inArray(i, totalA) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'yes'){
                            this.scoreA++;
                        }
                    }
                    if(i==19){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'no'){
                            this.scoreA++;
                        }
                    }
                    //end A

                    // for computation of B
                    if(jQuery.inArray(i, totalB) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'yes'){
                            this.scoreB++;
                        }
                    }
                    if(i==39){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'no'){
                            this.scoreB++;
                        }
                    }
                    //end B

                    // for computation of C
                    if(jQuery.inArray(i, totalC) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'yes'){
                            this.scoreC++;
                        }
                    }
                    if(i==39 || i == 36 || i == 42){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'no'){
                            this.scoreC++;
                        }
                    }
                    //end C

                    // for computation of D
                    if(jQuery.inArray(i, totalD) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'yes'){
                            this.scoreD++;
                        }
                    }
                    //end D

                    // for computation of E
                    if(jQuery.inArray(i, totalE) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'yes'){
                            this.scoreE++;
                        }
                    }
                    if(i==19){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'no'){
                            this.scoreE++;
                        }
                    }
                    //end E

                    // for computation of D
                    if(jQuery.inArray(i, totalF) !== -1){
                        var ans = $("input[name='row"+i+"']:checked").val();
                        if(ans == 'yes'){
                            this.scoreF++;
                        }
                    }
                    //end D
                }
                // All Checked
            }
            else{
                swal({
                    title: "You need to answer all questions.",
                    type: 'info',
                    confirmButtonText:"Okay",
                });
            }

        },

    } 
})



$(document).ready(function() {
	$('.footable').footable();
});