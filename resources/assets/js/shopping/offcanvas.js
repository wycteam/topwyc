new Vue({
    el: '#offcanvas',

    data: {
        search: null,
        users: [],
        tmpUsers: [],
        supports: [],
        timeout: null,
        loading: false,
        $usersContainer: null,
        form: new Form({
            subject: null,
            body: null,
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        suppClick : null,
        newConvoForm: new Form({
            supportIds: [],
            flag: true
        },{ baseURL: axiosAPIv1.defaults.baseURL }),
        notif : [],
        disabled: false
    },

    watch: {
        search(val) {
            if (val != null && val.length === 0) {
                this.users = this.tmpUsers;
            }
        },

        users(val) {
            if (val.length) {
                setTimeout(function () {
                    this.$usersContainer.perfectScrollbar('update');
                }.bind(this), 1);
            }
        }
    },

    methods: {
        getFriends() {
            return axiosAPIv1.get('/friends2')
                .then(result => {
                    this.users = result.data;
                    this.tmpUsers = result.data;
                });
        },

        //3 default support
        getSupports() {
            return axiosAPIv1.get('/getSupports')
                .then(result => {
                    this.supports = result.data;
                });
        },
        searchUser(e) {
            if (! (((e.which <= 90) && (e.which >= 48)) || e.which === 8)) {
                return;
            }

            clearTimeout(this.timeout);

            this.timeout = setTimeout(function () {
                if (this.search && (this.search.length >= 2)) {
                    this.loading = true;

                    axiosAPIv1.get('/users', {
                        params: {
                            search: this.search
                        }
                    }).then(result => {
                        this.users = result.data;

                        this.loading = false;
                    });
                }
            }.bind(this), 200);
        },
        contactSupport(user) {
            //save selected support
            this.suppClick = user;
            $('#modalHelp').modal('toggle');
        },
        select(user) {
            this.toggleOffcanvas();

            var chat = [];
            $(".panel-chat-box").each(function(){
                chat.push($(this).attr('id'));
            });
            
            if($.inArray('user'+user.id,chat)=='-1')
                Event.fire('chat-box.show', user);

            setTimeout(function(){
              $('.js-auto-size').textareaAutoSize();
            }, 5000);
        },

        toggleOffcanvas() {
            $(this.$el).toggleClass('expanded');
        },

        avatar(user) {
            return user.avatar
                ? user.avatar
                : '/images/avatar/' + ((user.gender === 'Male') ? 'boy' : 'girl') + '-avatar.png';
        },

        suppName(user) {
            //return user.first_name + ' ' + user.last_name;
            return 'Support :'+user.first_name;
        },

        fullName(user) {
            return user.first_name + ' ' + user.last_name;
        },
        //submit concern
        submitConcern() {
            this.$validator.validateAll();

            if($('#modalHelp #subject').val() != '' &&  $('#modalHelp #body').val() != '') {
                this.disabled = true;
            }

            if (! this.errors.any()) {
                this.form.submit('post', '/issues')
                    .then(result => {
                        this.$validator.clean();
                        //let supp = this.suppClick.id; //get id of clicked supp
                        //let supp = 2; // mam cha's account

                        var offline = 0;
                        for(var i=0; i<this.supports.length; i++)
                        {
                            if(!this.supports[i].is_online)
                                offline = offline + 1;
                        }
                                                
                        var supp = 0;
                        if(this.suppClick.is_online)
                          var supp = this.suppClick.id;
                        else {
                            if(offline!=this.supports.length){
                                for(var i=0; i<this.supports.length; i++)
                                {
                                    if(this.supports[i].is_online)
                                        var supp = this.supports[i].id;
                                }
                            }
                        }

                         // mam cha's account
                        let issuer = Laravel.user.id;
                        let casenum = result.data.issue_number;
                        //create new chat-room and in chat room user include selected support and the user
                        //if(offline!=this.supports.length)
                        if(supp==0) supp = 2; // mam cha's account
                        this.newConvoForm.supportIds.push(supp);
                        this.newConvoForm.submit('put', '/customer-service/' + result.data.id)
                        .then(result => {
                            //alert(result.data.chat_room.name);
                            this.notif = result.data;
                            this.form.subject = null;
                            this.form.body = null;
                            $('#modalHelp').modal('toggle');
                            // Allow user to send another ticket
                            this.disabled = false;
                            toastr.success('Successfully Submitted!');
                            this.newConvoForm.supportIds = [];
                            //open chat box with case number as title for the conversation
                            this.toggleOffcanvas();
                            var chat = [];
                            $(".panel-chat-box").each(function(){
                                chat.push($(this).attr('id'));
                            });

                            if (casenum) {
                                if($.inArray('user'+casenum,chat)=='-1'){
                                    Event.fire('chat-box.show', this.notif[0]);
                                }
                            }

                            setTimeout(function(){
                              $('.js-auto-size').textareaAutoSize();
                            }, 5000);
                        })
                        .catch($.noop);
                        
                    })
                    .catch($.noop);
            }
        },
    },

    created() {
        this.getFriends();
        this.getSupports();
    },

    mounted() {
        this.$usersContainer = $(this.$el).find('.user-list');
        this.$usersContainer.perfectScrollbar();
    }
});