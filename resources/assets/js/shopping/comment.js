
import VueResource from 'vue-resource';
Vue.use(VueResource);

Vue.component('comment', require('./components/Comments.vue'));

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    next();
});

const app = new Vue({
  el: '#newsComments',
});
