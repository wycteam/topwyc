new Vue({
    el: '#login-form',

    data: {
        form: new Form({
            email: null,
            password: null,
            remember: false,
        }, { baseURL: '/' })
    },

    methods: {
        submit() {
            this.$validator.validateAll();

            if (! this.errors.any()) {
                this.login();
            }
        },

        login() {
            this.form.submit('post', '/login')
                .then(result => {
                    window.location.reload();
                })
                .catch($.noop);
        }
    }
});
