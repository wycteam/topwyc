/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
window.Event = require('../core/event.js').default;
window.Errors = require('../core/errors.js').default;
window.Form = require('../core/form.js').default;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('beat-loader', require('vue-spinner/src/BeatLoader.vue'));
Vue.component('passport-clients', require('../components/passport/Clients.vue'));
Vue.component('passport-authorized-clients', require('../components/passport/AuthorizedClients.vue'));
Vue.component('passport-personal-access-tokens', require('../components/passport/PersonalAccessTokens.vue'));
Vue.component('imgfileupload', require('../components/Imgfileupload.vue'));
Vue.component('upload-loader', require('../components/UploadLoader.vue'));
Vue.component('modal', require('../shopping/components/Modal.vue'));

//currency??
Vue.filter('currency', function (value) {
    return  '₱'+numberWithCommas(value.toFixed(2));
});
//Jan 1, 2017 10:00am
Vue.filter('friendly-date', function (value) {
    return  value;
});

//for ellipsis
Vue.filter('truncate', function(value, length) {
  if(value.length < length) {
    return value;
  }
  length = length - 3;
  return value.substring(0, length) + '...';
});

// const app = new Vue({
//     el: '#app'
// });

if (window.Laravel.isAuthenticated) {
    // Notification
    Echo.private('User.' + window.Laravel.user.id)
        .notification((notification) => {
            let arrType = notification.type.split('\\');
            let type = arrType[arrType.length - 1].replace(/([a-z](?=[A-Z]))/g, '$1-').toLowerCase();

            Event.fire(type, notification);
        });

    // Privte Channel
    // Echo.private('chat.1')
    //     .listen('NewChatMessage', (e) => {
    //         console.log(e);
    //     });
}
