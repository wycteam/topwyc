new Vue({
    el: '#customer-service',

    data: {
        $header: null,
        $chatBox: null,
        form: new Form({
            subject: null,
            body: null,
        }, { baseURL: axiosAPIv1.defaults.baseURL })
    },

    methods: {
        submit() {
            this.$validator.validateAll();

            if (! this.errors.any()) {
                this.form.submit('post', '/issues')
                    .then(result => {
                        this.form.subject = null;
                        this.form.body = null;

                        this.$validator.clean();

                        this.toggleVisibility();
                    })
                    .catch($.noop);
            }
        },

        toggleVisibility() {
            this.$chatBox.slideToggle(300);
        }
    },

    mounted() {
        var $el = $(this.$el);

        this.$header = $el.find('.panel-heading');
        this.$chatBox = $el.find('.content');
    }
});