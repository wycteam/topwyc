Vue.component('product-item',  require('../components/store/management/Product.vue'));

Vue.filter('currency', function (value) {
    return numberWithCommas(value.toFixed(2));
});

Vue.filter('round', function (value) {
    return roundOff(value);
});

var app = new Vue({
	el: '#sProd_list',
	data: {
		items:[
			{
				 id: 1
				,name: 'PhosphorusGrey Melange Printed V Neck T-Shirt'
				,image: '/shopping/img/demo/vneck1.jpg'
				,price:3000
				,sale_price:2000
			},
			{
				 id: 2
				,name: 'United Colors of BenettonNavy Blue Solid V Neck T Shirt'
				,image: '/shopping/img/demo/vneck2.jpg'
				,price:2000
				,sale_price:1500
			},
			{
				 id: 3
				,name: 'WranglerBlack V Neck T Shirt'
				,image: '/shopping/img/demo/vneck3.jpg'
				,price:1850
				,sale_price:1500
			},
			{
				 id: 4
				,name: 'Tagd New YorkGrey Printed V Neck T-Shirts'
				,image: '/shopping/img/demo/vneck4.jpg'
				,price:2050
				,sale_price:1699
			},
			{
				 id: 5
				,name: 'Penshoppe Polo Shirt'
				,image: '/shopping/img/demo/polo2.jpg'
				,price:1999
				,sale_price:999
			},
		],
		selected:[]
	},
	methods:{
		selectItem(id){
			this.selected.push(id)
		}
	}
})

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
    return Math.round(v);
}