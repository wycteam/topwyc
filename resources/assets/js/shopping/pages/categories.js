var app = new Vue({
	 el: '#categories-list',
	 data:{
	 	categorylist:[]
	 },
	 created(){
	 	axiosAPIv1.get('/getallcategories')
	 	  .then(result => {
            this.categorylist = result.data;
        });
	 }
})