window.storeCreate = new Vue({
     el: '#store-form',

    data: {
        cities: [],
        provinces: [],
        categories: [],
        coverLoading: false,
        profileLoading: false,
        tempCover:null,
        tempPrimary:null,
        docs_verified: Laravel.user.is_docs_verified,
        lang:[],
        optionProv: [],
        Prov: [],
        optionCity: [],
        form: new Form({
            id: null,
            cover: '',
            profile: '',
            name: null,
            cn_name: null,
            description: null,
            cn_description: null,
            email: null,
            categories: [],
            address: {
                id: null,
                address: null,
                city: null,
                province: null,
                landmark: null
            },
            number: {
                id: null,
                number: null
            },
            company: {
                name: null,
                address: null,
                facebook: null,
                twitter: null,
                instagram: null,
                wechat: null,
            }
        }, { baseURL: '/' })
    },

    watch:{
        tempCover:function(val){
            if(!val){
                 $('#upload-demo').removeClass('ready');
                $('#upload').val(''); // this will clear the input val.
                $uploadCrop.croppie('bind', {
                    url : ''
                }).then(function () {
                    console.log($this.lang['reset-complete']);
                });

                $('#upload-demo .cr-slider-wrap').hide();
                // $('#upload-demo .cr-boundary').hide();
                
            }else
            {
                $('#upload-demo .cr-slider-wrap').show();
            }
        },
        tempPrimary:function(val){
            if(!val){
                 $('#upload-primary').removeClass('ready');
                $('#modalPrimary input[type="file"]').val(''); // this will clear the input val.
                $uploadPrimaryPic.croppie('bind', {
                    url : ''
                }).then(function () {
                    console.log(this.lang['reset-complete']);
                });

                $('#modalPrimary .cr-slider-wrap').hide();
                // $('#upload-demo .cr-boundary').hide();
                
            }else
            {
                $('#modalPrimary .cr-slider-wrap').show();
            }
        }
    },

    methods: {
        getCategories() {
            axiosAPIv1.get('categories')
                .then(result => {
                    this.categories = result.data;
                });
        },

        submit(e) {
            $("#storecreate").prop('disabled', true);
            this.form.submit('post', e.currentTarget.action)
                .then(result => {
                    window.location.href = '/stores/' + result.slug;
                })
              .catch(error => {
                setTimeout(function(){
                $("#storecreate").prop('disabled', false);
                },1000);
              });
        },

        triggerUploadPrimary(){
            $('#modalPrimary input[type="file"]').click();
        },

        readFile(e) {

          var files = e.target.files || e.dataTransfer.files;


          if (!files.length)
            return;
          this.createImage(files);
        },

        readFilePrimary(e) {

          var files = e.target.files || e.dataTransfer.files;


          if (!files.length)
            return;
          this.createPrimaryImage(files);
        },

        createImage(input) {
            var reader = new FileReader();
            
            reader.onload = (e) => {
                $('#upload-demo').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(() => {
                    this.tempCover = true;
                });
            }
                    reader.readAsDataURL(input[0]);
        },
        createPrimaryImage(input) {
            var reader = new FileReader();
            
            reader.onload = (e) => {
                $('#upload-primary').addClass('ready');
                $uploadPrimaryPic.croppie('bind', {
                    url: e.target.result
                }).then(() => {
                    this.tempPrimary = true;
                });
            }
                    reader.readAsDataURL(input[0]);
        },

        submitCover(){
            this.coverLoading = true;
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then( (resp) => {
                axios.post('/common/resize-image?width=1140&height=300',{
                    image:resp
                }).then(result => {
                    this.form.cover = result.data;

                    setTimeout(()=>{
                        $('#upload-demo').css('background-image',"url('" + result.data + "')");
                    },1000)

                    this.tempCover = null;
                    this.coverLoading = false;

                }).catch(error => {
                    this.coverLoading = false;
                });
            });
        },

        submitPrimary(){
            this.profileLoading = true;
            $uploadPrimaryPic.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then( (resp) => {
                axios.post('/common/resize-image?width=300&height=300',{
                    image:resp
                }).then(result => {
                    this.form.profile = result.data;

                    // $('#upload-demo').css('background-image',"url('" + result.data + "')");
                    this.tempPrimary = null;
                    this.profileLoading = false;
                    $('#modalPrimary').modal('toggle');
                    toastr.success(this.lang.data['prof-pic-set-msg'])

                }).catch(error => {
                    this.profileLoading = false;
                });
            });
        },

        getCity:function(data){
            axios.get('/profile/city/'+data)
                .then(result => {
                    var array = [];
                    for(var i = 0; i < result.data.length; i++){
                        array.push(result.data[i]);
                    }
                    this.optionCity = array;
                });
        }
    },

    computed: {
        categoryPlaceholder() {
            if (this.form.categories.length) {
                return '';
            
            return window.isChinese()? '选择类别':'Select Category';
            }
        },
        getCategoryName(){
            return window.isChinese()? 'name_cn':'name';
        }
    },

    components: {
        Multiselect: window.VueMultiselect.default
    },

    created() {
        this.getCategories();

        Event.listen('Imgfileupload.coverLoading', () => {
            this.coverLoading = true;
        });

        Event.listen('Imgfileupload.coverOnLoad', (img) => {
            this.form.cover = img;
            this.coverLoading = false;
        });

        Event.listen('Imgfileupload.profileLoading', () => {
            this.profileLoading = true;
        });

        Event.listen('Imgfileupload.profileOnLoad', (img) => {
            this.form.profile = img;
            this.profileLoading = false;
        });

        axios.get('/translate/store-management')
            .then(result => {
            this.lang = result.data;
            this.catPlaceholder = result.data.data['sel-cat'];
        });

        axios.get('/profile/provinces')
            .then(result => {
                var array = [];
                var array2 = [];

                for(var i = 0; i < result.data.length; i++){
                    array.push(result.data[i].name);
                    array2.push(result.data[i]);
                }

                this.optionProv = array;
                this.Prov = array2;
            });

    },
});
   



$uploadCrop = $('#upload-demo').croppie({
    url: 'https://dummyimage.com/1140x300/b0b0b0/f5f5f5&text=1140px+x+300px' ,
    viewport: {
        width: 1140,
        height: 300,
    },
    // enableExif: true
});

$uploadPrimaryPic = $('#upload-primary').croppie({
    url: '/images/avatar/default.jpg' ,
    viewport: {
        width: 300,
        height: 300,
        type:'circle'
    },
});

// $('#upload').on('change', function () { readFile(this); });

/*$('#number').inputmask({
    mask: '(9999) 999-9999'
})*/

$("#number").keypress(function (e) {
 //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});

    
function popupResult(result) {
    var html;
   
    if (result.src) {
        html = '<img src="' + result.src + '" />';
    }
    return;
    swal({
        title: '',
        html: true,
        text: html,
        allowOutsideClick: true
    });
    setTimeout(function(){
        $('.sweet-alert').css('margin', function() {
            var top = -1 * ($(this).height() / 2),
                left = -1 * ($(this).width() / 2);

            return top + 'px 0 0 ' + left + 'px';
        });
    }, 1);
}
