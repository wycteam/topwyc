$(document).ready(function(){
     $('.productShow').click(function(){
       var id= this.id;

       if(id == "allProducts"){
            $('#allProducts1').addClass('active');
            $('#suspendedItems1').removeClass('active');
         
        }else if(id == "suspendedItems"){
            $('#allProducts1').removeClass('active');
            $('#suspendedItems1').addClass('active');
        }
    })
})

$(document).ready(() => {
    $('.rate').rateYo({
        starWidth: '16px',
        starSvg: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 6.76l1.379 4.246h4.465l-3.612 2.625 1.379 4.246-3.611-2.625-3.612 2.625 1.379-4.246-3.612-2.625h4.465l1.38-4.246zm0-6.472l-2.833 8.718h-9.167l7.416 5.389-2.833 8.718 7.417-5.388 7.416 5.388-2.833-8.718 7.417-5.389h-9.167l-2.833-8.718z"></path></svg>'
    });

    window.$productsGrid = $('.products-grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-item',
        gutter: 15,
        horizontalOrder: true,
        fitWidth: true,
        stagger: 30
    });

    $('[data-toggle="tooltip"]').tooltip();
});