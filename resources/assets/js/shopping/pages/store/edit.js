let data = window.Laravel.data;

window.storeEdit = new Vue({
    el: '#store-form',

    data: {
        cities: [],
        provinces: [],
        categories: [],
        coverLoading: false,
        profileLoading: false,
        tempCover:null,
        tempPrimary:null,
        form: {},
        lang:[],
        optionProv: [],
        Prov: [],
        optionCity: []
    },

     watch:{
        tempCover:function(val){
            if(!val){
                 $('#upload-demo').removeClass('ready');
                $('#upload').val(''); // this will clear the input val.
                $uploadCrop.croppie('bind', {
                    url : ''
                }).then(function () {
                    console.log('reset complete');
                });

                $('#upload-demo .cr-slider-wrap').hide();
                // $('#upload-demo .cr-boundary').hide();
                
            }else
            {
                $('#upload-demo .cr-slider-wrap').show();
            }
        },
        tempPrimary:function(val){
            if(!val){
                 $('#upload-primary').removeClass('ready');
                $('#modalPrimary input[type="file"]').val(''); // this will clear the input val.
                $uploadPrimaryPic.croppie('bind', {
                    url : ''
                }).then(function () {
                    console.log('reset complete');
                });

                $('#modalPrimary .cr-slider-wrap').hide();
                // $('#upload-demo .cr-boundary').hide();
                
            }else
            {
                $('#modalPrimary .cr-slider-wrap').show();
            }
        }
    },

    methods: {
        getCategories() {
            axiosAPIv1.get('categories')
                .then(result => {
                    this.categories = result.data;
                });
        },

        submit(e) {
            this.form.submit('put', e.currentTarget.action)
                .then(result => {
                    // console.log(result);
                    window.location.href = '/stores/' + result.slug;
                })
                .catch($.noop);
        },

        triggerUploadPrimary(){
            $('#modalPrimary input[type="file"]').click();
        },

        readFile(e) {

          var files = e.target.files || e.dataTransfer.files;


          if (!files.length)
            return;
          this.createImage(files);
        },

        readFilePrimary(e) {

          var files = e.target.files || e.dataTransfer.files;


          if (!files.length)
            return;
          this.createPrimaryImage(files);
        },

        createImage(input) {
            var reader = new FileReader();
            
            reader.onload = (e) => {
                $('#upload-demo').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(() => {
                    this.tempCover = true;
                });
            }
                    reader.readAsDataURL(input[0]);
        },
        createPrimaryImage(input) {
            var reader = new FileReader();
            
            reader.onload = (e) => {
                $('#upload-primary').addClass('ready');
                $uploadPrimaryPic.croppie('bind', {
                    url: e.target.result
                }).then(() => {
                    this.tempPrimary = true;
                });
            }
                    reader.readAsDataURL(input[0]);
        },

        submitCover(){
            this.coverLoading = true;
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then( (resp) => {
                axios.post('/common/resize-image?width=1140&height=300',{
                    image:resp
                }).then(result => {
                    this.form.cover = result.data;

                    setTimeout(()=>{
                        $('#upload-demo').css('background-image',"url('" + result.data + "')");
                    },500)
                    
                    this.tempCover = null;
                    this.coverLoading = false;

                }).catch(error => {
                    this.coverLoading = false;
                });
            });
        },

        submitPrimary(){
            this.profileLoading = true;
            $uploadPrimaryPic.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then( (resp) => {
                axios.post('/common/resize-image?width=300&height=300',{
                    image:resp
                }).then(result => {
                    this.form.profile = result.data;

                    // $('#upload-demo').css('background-image',"url('" + result.data + "')");
                    this.tempPrimary = null;
                    this.profileLoading = false;
                    $('#modalPrimary').modal('toggle');
                    toastr.success(this.lang.data['prof-pic-set-msg'])

                }).catch(error => {
                    this.profileLoading = false;
                });
            });
        },

        getCity:function(data){
            axios.get('/profile/city/'+data)
                .then(result => {
                    var array = [];
                    for(var i = 0; i < result.data.length; i++){
                        array.push(result.data[i]);
                    }
                    this.optionCity = array;
                });
        }

    },

    computed: {
        categoryPlaceholder() {
            if (this.form.categories && this.form.categories.length) {
                return '';
            }

            return window.isChinese()? '选择类别':'Select Category';
        },

        getCategoryName(){
            return window.isChinese()? 'name_cn':'name';
        }
    },

    components: {
        Multiselect: window.VueMultiselect.default
    },

    created() {
        axios.all([this.getCategories()]);

        axiosAPIv1.get('/countorder/'+this.form.id)
        .then(result => {
            console.log(result.data.length+'-'+this.form.id);
            if(result.data.length > 0){
                $('#address, #city, #province, #landmark').attr("disabled", "true");
            }
        });

        this.form = new Form($.extend(data, {
            cover: data.cover_image,
            profile: data.profile_image,
        }), { baseURL: '/' });

        this.form.address.city = {
            id: this.form.address.city_id,
            province_id: this.form.address.province_id,
            name: this.form.address.city
        };

        Event.listen('Imgfileupload.coverLoading', () => {
            this.coverLoading = true;
        });

        Event.listen('Imgfileupload.coverOnLoad', (img) => {
            this.form.cover = img;
            this.coverLoading = false;
        });

        Event.listen('Imgfileupload.profileLoading', () => {
            this.profileLoading = true;
        });

        Event.listen('Imgfileupload.profileOnLoad', (img) => {
            this.form.profile = img;
            this.profileLoading = false;
        });

        axios.get('/translate/store-management')
            .then(result => {
            this.lang = result.data;
        });

        axios.get('/profile/provinces')
            .then(result => {
                var array = [];
                var array2 = [];

                for(var i = 0; i < result.data.length; i++){
                    array.push(result.data[i].name);
                    array2.push(result.data[i]);
                }

                this.optionProv = array;
                this.Prov = array2;
            });

        this.getCity(this.form.address.province);
    }
});


$uploadCrop = $('#upload-demo').croppie({
    url: 'https://dummyimage.com/1140x300/b0b0b0/f5f5f5&text=1140px+x+300px' ,
    viewport: {
        width: 1140,
        height: 300,
    },
    // enableExif: true
});

$uploadPrimaryPic = $('#upload-primary').croppie({
    url: '/images/avatar/default.jpg' ,
    viewport: {
        width: 300,
        height: 300,
        type:'circle'
    },
});

/*$('#number').inputmask({
    mask: '(9999) 999-9999'
})*/

$("#number").keypress(function (e) {
 //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});