Vue.component('mfeedback',  require('../components/feedback/MoreFeedback.vue'));
Vue.component('feedback',  require('../components/feedback/feedback.vue'));

var app = new Vue({
	el: '#feedbacks_container',
	data: {
		feedbacks : [],
		lang:[],
		form: new Form({
			 content:''
			,imageTemp: ''
		}, { baseURL: 'http://'+Laravel.base_api_url }),
		commentForm: new Form({
			content:'',
			parent_id:0
		}, { baseURL: 'http://'+Laravel.base_api_url }),
	},
	methods:{
		 onInfinite() {
			axiosAPIv1.get('/feedbacks',{
				params: {
					 count:4
					,skip:this.feedbacks.length
				},
			}).then((res) => {
				var data = res.data.filter(feedback => feedback.approved == 1);
				if (data.length) {
					this.feedbacks = this.feedbacks.concat(data);
					this.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded');
					// if (this.feedbacks.length / 20 === 1) {
					//   this.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
					// }
					//for unli pages
				} else {
					this.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
				}
			});
	    },

		submitFeedback(){
			this.form.submit('post', '/v1/feedbacks')
                .then(result => {
                    if(result.success)
                    {
			            this.form.content = '';
			            this.form.imageTemp = '';
						toastr.success(this.lang["description2"],this.lang["success-msg-feedback"])
                    }else
                    {
	                    toastr.error(this.lang['try-again']);
                    }
                })
                .catch($.noop);
		},

		submitComment(){
			var index;
			for(var i=0; i<this.feedbacks.length; i++)
        	{
        		if(this.feedbacks[i].id == this.commentForm.parent_id)
        		{
        			index = i;
        			break;
        		}
        	}
			this.commentForm.submit('post', '/v1/feedbacks')
                .then(result => {
                    if(result.success)
                    {
                    	this.commentForm.content = '';
                    	this.commentForm.parent_id = 0;
        				this.feedbacks[index].comments.unshift(result.data);
						toastr.success('',this.lang["success-msg-comment"]);
						$('#modalComments').modal('toggle');
                    }else
                    {
	                    toastr.error(this.lang['try-again']);
                    }

                })
                .catch($.noop);
		},

		handleCmdEnter: function (e) {
		    if ((e.metaKey || e.ctrlKey) && e.keyCode == 13) {
		        this.submitFeedback();
		    }
		}

	},

	created() {
		Event.listen('Imgfileupload.submitFeedbackImage', (data) => {
			this.form.imageTemp = data;
		});

		axios.get('/translate/feedback')
			.then(language => {
				this.lang = language.data.data;
			});
		
	}
	
});
