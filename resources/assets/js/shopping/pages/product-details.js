let data = window.Laravel.data;
let usr  = window.Laravel.user;

Vue.component('review',  require('../components/product/review/review.vue'));
Vue.component('product-item', require('../components/product/Product.vue'));
Vue.component('related-product-item', require('../components/product/Related-Product.vue'));
Vue.component('star-rating', VueStarRating.default);

var current_product = window.Laravel.data.id;
var usrid = null;
var docsVerified = false;
if(usr){
    usrid = usr.id;
    docsVerified = usr.is_docs_verified;
}
var app = new Vue({
    el: '#product-details-container',
    data: {
        owner: data.user_id,
        site_user: usrid,
        verified: docsVerified,
        colorId:0,
        colorStock: 0,
        colorDesign: '',
        colorImage: '',
        sizeId:0,
        form: new Form(
            {
                 content:''
                ,imageTemp:''
                ,product_id:current_product 
                ,rating_id:''
                ,rating:1
                ,review:''
            }
            ,{ baseURL: 'http://'+Laravel.base_api_url }
            ,{resetExceptions:['product_id']}
        )
        ,commentForm: new Form(
            {
             content:''
            ,parent_id:0
            ,product_id:current_product 
            ,rating_id:0
            ,review: 'neutral'
            }
            ,{ baseURL: 'http://'+Laravel.base_api_url }
            ,{resetExceptions:['product_id']}
        )
        ,user: data.user
        ,reviews:[]
        ,reviewCount:0
        ,items:[]
        ,cartlang:[]
        ,varcolor: data.product_color_variations
        ,varsize: data.product_size_variations
        ,varsize2:[]
        ,description: data.description
        ,chDescription: data.cn_description
        ,url: 'http://'+Laravel.base_api_url+'/'+data.image_path_color
        ,checkfav: ''
        ,favid:''
    },

    created(){

        function getFeedbackTranslation() {
            return axios.get('/translate/feedback');
        }

        function getCartTranslation() {
            return axios.get('/translate/cart');
        }

        function getProductTranslation() {
            return axios.get('/translate/products');
        }

        function getRelatedItems(){
            return axiosAPIv1.post('/product/get-related-products',{
                product:current_product
            });
        }

        function getReviews() {
            return axiosAPIv1.get('/product-reviews?product='+current_product+'&count=2');
        }

        function getReviewCount() {
            return axiosAPIv1.get('/product-review/count?product='+current_product);
        }

        function getCart() {
            return axios.get(location.origin + '/cart/view'); 
        } 

        axios.all([
            getFeedbackTranslation()
            ,getCartTranslation()
            ,getProductTranslation()
            ,getReviews()
            ,getReviewCount()
            ,getCart()
            ,getRelatedItems()
        ]).then(axios.spread(
            (
                feedbacktranslation
                ,carttranslation
                ,producttranslation
                ,reviews
                ,reviewCount
                ,cart
                ,related
            ) => {
                this.lang = feedbacktranslation.data.data;
                this.cartlang = carttranslation.data.data;
                this.productlang = producttranslation.data.data;
                this.reviews = reviews.status == 200? reviews.data:[];
                this.reviewCount = reviewCount.status == 200? reviewCount.data:0;
                this.items = related.status == 200? related.data:[];

                this.colorId = this.varcolor[0].id;
                this.colorDesign = this.varcolor[0].label;
                this.colorImage = this.url + this.varcolor[0].image_name + '.jpg';                               
                
                if(this.varcolor.length != 0 && this.varsize.length == 0){
                    this.colorStock = this.varcolor[0].stock;
                } else {
                    if(this.varcolor[0].image_name.substring(5) == 1){
                      this.sizeId = data.product_size_variations1[0].id;
                      this.varsize2 = data.product_size_variations1;
                      this.colorStock = data.product_size_variations1[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 2){
                      this.sizeId = data.product_size_variations2[0].id;
                      this.varsize2 = data.product_size_variations2;
                      this.colorStock = data.product_size_variations2[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 3){
                      this.sizeId = data.product_size_variations3[0].id;
                      this.varsize2 = data.product_size_variations3;
                      this.colorStock = data.product_size_variations3[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 4){
                      this.sizeId = data.product_size_variations4[0].id;
                      this.varsize2 = data.product_size_variations4;
                      this.colorStock = data.product_size_variations4[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 5){
                      this.sizeId = data.product_size_variations5[0].id;
                      this.varsize2 = data.product_size_variations5;
                      this.colorStock = data.product_size_variations5[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 6){
                      this.sizeId = data.product_size_variations6[0].id;
                      this.varsize2 = data.product_size_variations6;
                      this.colorStock = data.product_size_variations6[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 7){
                      this.sizeId = data.product_size_variations7[0].id;
                      this.varsize2 = data.product_size_variations7;
                      this.colorStock = data.product_size_variations7[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 8){
                      this.sizeId = data.product_size_variations8[0].id;
                      this.varsize2 = data.product_size_variations8;
                      this.colorStock = data.product_size_variations8[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 9){
                      this.sizeId = data.product_size_variations9[0].id;
                      this.varsize2 = data.product_size_variations9;
                      this.colorStock = data.product_size_variations9[0].stock;
                    }
                    if(this.varcolor[0].image_name.substring(5) == 10){
                      this.sizeId = data.product_size_variations10[0].id;
                      this.varsize2 = data.product_size_variations10;
                      this.colorStock = data.product_size_variations10[0].stock;
                    }
                }
                
        }))
        .catch($.noop);


        axiosAPIv1.get('/checkfavorites/' + current_product)
        .then(result => {
            console.log(result);
            if(result.data.response==1){
                this.checkfav = 1;
                this.favid = 0;
            } else {
                this.checkfav = 0;
                this.favid = result.data.fav_id;
            }                   
        });

        Event.listen('Imgfileupload.submitFeedbackImage', (data) => {
            this.form.imageTemp = data;
        });
    },

    methods:{
        submitFeedback(){
            //this.form.rating_id = this.form.rating == 0 ? '':this.form.rating;
            this.form.rating_id = 1;
            this.form.submit('post', '/v1/product-reviews')
                .then(result => {
                    if(result.success)
                    {
                        this.reviews.unshift(result.data);
                        toastr.success('',this.productlang['feedback'])
                        this.form.imageTemp = '';
                        //this.form.rating = 3;
                    }else
                    {
                        toastr.error(this.productlang['please-try']);
                    }
                    $("#submitReview").prop('disabled', false);
                })
                .catch(error => {
                    $("#submitReview").prop('disabled', false)
                });
        },

        handleCmdEnter: function (e) {
            if ((e.metaKey || e.ctrlKey) && e.keyCode == 13) {
                this.submitFeedback();
            }
        },

        submitComment(){
            var index;
            for(var i=0; i<this.reviews.length; i++)
            {
                if(this.reviews[i].id == this.commentForm.parent_id)
                {
                    index = i;
                    break;
                }
            }

            this.commentForm.review = 'neutral';
            
            this.commentForm.submit('post', '/v1/product-reviews')
                .then(result => {
                    if(result.success)
                    {
                        this.reviews[index].comments.unshift(result.data);
                        toastr.success('',this.productlang['comment']);
                        $('#modalComments').modal('toggle');
                    }else
                    {
                        toastr.error(this.productlang['please-try']);
                    }
                })
                .catch($.noop);
        },

        loadMoreReviews(){
            function getReviews(review_count) {
                return axiosAPIv1.get('/product-reviews?product='+current_product+'&count=2&skip='+review_count);
            }

            axios.all([
                 getReviews(this.reviews.length)
            ]).then(axios.spread(
                (
                     reviews
                ) => {
                    if(reviews.status == 200)
                    {
                        this.reviews = this.reviews.concat(reviews.data);
                    }
            }))
            .catch($.noop);
        },

        chooseColor(varco){
            $("#qty").val('1');
            this.colorId = varco.id;
            this.colorDesign = varco.label;
            this.colorImage = this.url + varco.image_name + '.jpg';

            if(this.varcolor.length != 0 && this.varsize.length == 0){
                this.colorStock = varco.stock;
            } else {
                if(varco.image_name.substring(5) == 1){
                  this.varsize2 = data.product_size_variations1;
                  this.colorStock = data.product_size_variations1[0].stock;
                }
                if(varco.image_name.substring(5) == 2){
                  this.varsize2 = data.product_size_variations2;
                  this.colorStock = data.product_size_variations2[0].stock;
                }
                if(varco.image_name.substring(5) == 3){
                  this.varsize2 = data.product_size_variations3;
                  this.colorStock = data.product_size_variations3[0].stock;
                }
                if(varco.image_name.substring(5) == 4){
                  this.varsize2 = data.product_size_variations4;
                  this.colorStock = data.product_size_variations4[0].stock;
                }
                if(varco.image_name.substring(5) == 5){
                  this.varsize2 = data.product_size_variations5;
                  this.colorStock = data.product_size_variations5[0].stock;
                }
                if(varco.image_name.substring(5) == 6){
                  this.varsize2 = data.product_size_variations6;
                  this.colorStock = data.product_size_variations6[0].stock;
                }
                if(varco.image_name.substring(5) == 7){
                  this.varsize2 = data.product_size_variations7;
                  this.colorStock = data.product_size_variations7[0].stock;
                }
                if(varco.image_name.substring(5) == 8){
                  this.varsize2 = data.product_size_variations8;
                  this.colorStock = data.product_size_variations8[0].stock;
                }
                if(varco.image_name.substring(5) == 9){
                  this.varsize2 = data.product_size_variations9;
                  this.colorStock = data.product_size_variations9[0].stock;
                }
                if(varco.image_name.substring(5) == 10){
                  this.varsize2 = data.product_size_variations10;
                  this.colorStock = data.product_size_variations10[0].stock;
                }
                $('#gSize').prop('selectedIndex',0);
            }
        },

        chooseSize(varsi){
            var size = varsi;
            var arr = size.split("-");
            this.sizeId = arr[0];
            this.colorStock = arr[1];
        },

        AddToWishlist(id){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtofavorites/' + id)
                .then(result => {
                    if(result.data.response==1){
                        var fav = parseInt($("#fav-count").text())+1;
                        $("#fav-count").text(fav).show();
                        toastr.success(this.cartlang['added-favorites']);
                    } else if(result.data.response==2){
                        toastr.error(result.data.error_msg, this.cartlang['not-added-wishlist']);
                    } else {
                        toastr.info(this.cartlang['already-favorites']);
                    }                   
                });
            } else {
                toastr.error(this.cartlang['login-favorites']);
            }
        },

        RemoveFromWishlist(){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/deletefavorites/' + this.favid)
                .then(result => {
                    $("#fav-count").text(result.data);
                    toastr.success(this.cartlang['removed-from-wishlist']);            
                });
            } else {
                toastr.error(this.cartlang['login-favorites']);
            }
        },

        addToCart2(id){

            var qty = $('#qty').val();
            var selcolor = this.colorDesign;
            var size = $("#gSize option:selected").val();
            var selsize = $("#gSize option:selected").text();

            if(this.varcolor.length == 0 && this.varsize.length == 0){

                axios.get(location.origin + '/cart/add2',{
                    params: {
                        prod_id:id,
                        qty:qty
                    }
                })
                .then(result => {
                    $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                    if(result.data.error_msg) 
                        toastr.error(result.data.error_msg, this.cartlang['not-added']);
                    else
                        toastr.success(this.cartlang['added-cart']);
                });

                setTimeout(function(){
                axios.get(location.origin + '/cart/view')
                .then(result => {
                    $('#cart-count, #cart-count-mobile').text(result.data.cart_item_count);
                });
                }, 1000);

            } else {

                if((this.varcolor.length != 0 && this.varsize.length != 0) && this.sizeId == 0){
                    toastr.error(this.cartlang['select-size'], this.cartlang['not-added']);
                } else {

                    axios.get(location.origin + '/cart/add2',{
                        params: {
                            prod_id:id,
                            qty:qty,
                            color:selcolor,
                            colorId:this.colorId,
                            colorStock:this.colorStock,
                            colorImage:this.colorImage,
                            sizeId:this.sizeId,
                            size:selsize
                        }
                    })
                    .then(result => {
                        $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                        if(result.data.error_msg)
                            toastr.error(result.data.error_msg, this.cartlang['not-added']);
                        else
                            toastr.success(this.cartlang['added-cart']);
                    });
                    
                    setTimeout(function(){
                    axios.get(location.origin + '/cart/view')
                    .then(result => {
                        $('#cart-count, #cart-count-mobile').text(result.data.cart_item_count);
                    });
                    }, 3000);
                }
            }

        },

        buyNow(id){

            var qty = $('#qty').val();
            var selcolor = this.colorDesign;
            var size = $("#gSize option:selected").val();
            var selsize = $("#gSize option:selected").text();

            axios.get(location.origin + '/cart/view')
            .then(result => {
                var cart2 = $.map(result.data.cart, function(value, index) {
                    return [value];
                });

                var cart3 = cart2.find(item => item.id == current_product);

                if(this.varcolor.length == 0 && this.varsize.length == 0){
                    if(cart3 == undefined){
                        axios.get(location.origin + '/cart/add2',{
                            params: {
                                prod_id:id,
                                qty:qty
                        
                            }
                        })
                        .then(result => {
                            if(result.data.error_msg)
                            {
                                toastr.error(result.data.error_msg, this.cartlang['cant-bought']);
                            }else
                            {
                                window.location.assign("/cart");
                            }
                        });
                    } else {
                        axios.get(location.origin + '/cart/update',{
                            params: {
                                prod_id:id,
                                row_id:cart3.rowId,
                                qty:qty
                            }
                        });

                        setTimeout(function(){ 
                            window.location.assign("/cart"); 
                        }, 1000);
                    }
                } else {

                    if((this.varcolor.length != 0 && this.varsize.length != 0) && this.sizeId == 0){
                        toastr.error(this.cartlang['select-size'], this.cartlang['not-added']);
                    } else {
                        if(cart3 == undefined){
                            axios.get(location.origin + '/cart/add2',{
                                params: {
                                    prod_id:id,
                                    qty:qty,
                                    color:selcolor,
                                    colorId:this.colorId,
                                    colorStock:this.colorStock,
                                    colorImage:this.colorImage,
                                    sizeId:this.sizeId,
                                    size:selsize
                                }
                            })
                            .then(result => {
                                if(result.data.error_msg)
                                {
                                    toastr.error(result.data.error_msg, this.cartlang['cant-bought']);
                                }else
                                {
                                    window.location.assign("/cart");
                                }
                            });
                        } else {

                            var arr = $.grep(cart2, function( a ) {
                              return a.id == current_product;
                            });

                            var arr2 = arr.find(item => item.options.size == selsize);

                            if(selcolor != 'color' && size != 'size')
                                var arr2 = arr.find(item => item.options.color == selcolor && item.options.size == selsize);
                            else if(selcolor != 'color')
                                var arr2 = arr.find(item => item.options.color == selcolor);
                            else if(size != 'size')
                                var arr2 = arr.find(item => item.options.size == selsize);                    

                            if(arr2){
                                axios.get(location.origin + '/cart/update',{
                                    params: {
                                        prod_id:id,
                                        row_id:arr2.rowId,
                                        qty:qty,
                                        color:selcolor,
                                        colorId:this.colorId,
                                        colorStock:this.colorStock,
                                        colorImage:this.colorImage,
                                        sizeId:this.sizeId,
                                        size:selsize
                                    }
                                });

                                setTimeout(function(){ 
                                    window.location.assign("/cart"); 
                                }, 1000);

                            } else {
                                axios.get(location.origin + '/cart/add2',{
                                    params: {
                                        prod_id:id,
                                        qty:qty,
                                        color:selcolor,
                                        colorId:this.colorId,
                                        colorStock:this.colorStock,
                                        colorImage:this.colorImage,
                                        sizeId:this.sizeId,
                                        size:selsize
                                    }
                                })
                                .then(result => {
                                    $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                                    if(result.data.error_msg)
                                        toastr.error(result.data.error_msg, this.cartlang['not-added']);
                                    else{
                                        setTimeout(function(){ 
                                            window.location.assign("/cart"); 
                                        }, 1000);                                    
                                    }
                                });
                            }

                        }
                    }
                }

            });

        },

        addToCart(id,name){
            axios.get(location.origin + '/cart/add',{
                params: {
                    prod_id:id,
                    qty:1
                }
            })
            .then(result => {
                $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                if(result.data.error_msg)
                {   
                    if(result.data.error_msg=='Redirect')
                        window.location = '/product/' + result.data.slug;
                    else
                        toastr.error(result.data.error_msg, this.cartlang['not-added']);
                } else {
                    toastr.success(this.cartlang['added-cart'], name);
                }
            });
        },
        addToCompare(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtocompare/' + id)
                .then(result => {
                    if(result.data.response==1){
                        var comp = parseInt($("#comp-count").text())+1;
                        $("#comp-count").text(comp).show();
                        toastr.success(this.cartlang['added-comparison'], name);
                    } else if(result.data.response==3) {
                        toastr.error(this.cartlang['full']);
                    } else {
                        toastr.info(this.cartlang['already-compare'], name);
                    }
                });
            } else {
                toastr.error(this.cartlang['login-compare']);
            }
        },
        addToFavorites(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtofavorites/' + id)
                .then(result => {
                    if(result.data.response==1){
                        var fav = parseInt($("#fav-count").text())+1;
                        $("#fav-count").text(fav).show();
                        toastr.success(this.cartlang['added-favorites'], name); 
                    } else {
                        toastr.info(this.cartlang['already-favorites'], name);
                    }
                });
            } else {
                toastr.error(this.cartlang['login-favorites']);
            }
        },

        contactMe() {

            var chat = [];
            $(".panel-chat-box").each(function(){
                chat.push($(this).attr('id'));
            });

            if($.inArray('user'+this.user.id,chat)=='-1')
                Event.fire('chat-box.show', this.user);

            setTimeout(function(){
              $('.js-auto-size').textareaAutoSize();
            }, 5000);
        },

        notVerified(){
            toastr.error(this.productlang['product-owner']);
        },

        showReportModal(){
            $('#modalProdReport').modal('toggle');
        },

        cantReport(){
            toastr.error(this.productlang['cant-report']);
        },

        needLogin(){
            toastr.error(this.productlang['login-first']);
        }
    },
    computed:{
        storename(){
          var name = window.Laravel.data.store.cn_name ? window.Laravel.data.store.cn_name: window.Laravel.data.store.name;
          var m = $("meta[name=locale-lang]");    
          var langmode = m.attr("content");

          if(langmode=="cn")
            return name;
          else 
            return window.Laravel.data.store.name;
        }
    }
});


$(function(){
  // owlcarosel (if items less than 4, hide nav, disable drag, hide touch)
  var products_slider_detail = $('.products-slider-detail');
  var item_count = $('.products-slider-detail a').length;
  products_slider_detail.owlCarousel({
    margin:5,
    dots:false,
    nav:item_count < 5 ? false : true,
    mouseDrag:item_count < 5 ? false : true,
    touchDrag:item_count < 5 ? false : true,
    navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    responsive:{
        0:{ items:4, }
    }
  });
  $('.products-slider-detail a').click(function(){
    var src = $(this).find('img').attr('src');
    var zoom = $(this).find('img').attr('data-zoom-image');
    var detail = $(this).parent().parent().parent().parent().parent().find('.image-detail img');
    detail.attr('src',src);
    detail.attr('data-zoom-image',zoom);
    $('.zoomWindow').css('background-image', 'url("' + zoom + '")');
    return false;
  });

  $('#qty-mar .form-group').css("margin","0");

  $(document).on('click', 'div.popover.fade.left.in', function() {
    $('div.popover.fade.left.in').fadeOut();
  });

  $('#submitReview').on('click', function() {
    $(this).prop('disabled', true);
  });

});