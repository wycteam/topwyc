Vue.component('product-item',  require('../components/common/Product.vue'));

Vue.filter('currency', function (value) {
    return  numberWithCommas(value.toFixed(2));
});

Vue.filter('round', function (value) {
    return  roundOff(value);
});

var app = new Vue({
	el: '#for_men',
	data: {
		items:[
		],
		selected:[],
		lang:[]
	},
	created(){
		function getTranslation() {
            return axios.get('/translate/cart');
        }
		function getProducts() {
			return axiosAPIv1.get('/formen');
		}
    	axios.all([
    		getTranslation(),
			getProducts()
		]).then(axios.spread(
			(	
				translation,
				response
			) => {
				this.lang = translation.data.data;

				if(response.data){
					$('.flexhide1').remove();
					$('#for_men').show();
					this.items = response.data;
				}

				if(this.items.length > 10) {
					$('#see-all-for-men').removeClass('hide');
				} else {
					$('#see-all-for-men').addClass('hide');
				}
		}))
		.catch($.noop);
	},
	updated() {
	    // Fired every second, should always be true
	    $('.product-slider').owlCarousel({
	      items:1,
	      dots: false,
	      nav: false,
	      mouseDrag: false,
	      touchDrag: false,
	      pullDrag: false,
	      freeDrag: false,
	      navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	      responsive:{
	          0:{
	            items:2,
	          },
	          480:{
	            items:2,
	          },
	          768:{
	            items:2,
	          },
	          992:{
	            items:5,
	          },
	          1200:{
	            items:5,
	          }
	        }
	    });
	  },
	methods:{
		selectItem(id){
			this.selected.push(id)
		},
        addToCart(id,name){
	        axios.get(location.origin + '/cart/add',{
	            params: {
	                prod_id:id,
	                qty:1
	            }
	        })
	        .then(result => {
	            $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                if(result.data.error_msg)
                {	
                	if(result.data.error_msg=='Redirect')
                		window.location = '/product/' + result.data.slug;
                	else
	                	toastr.error(result.data.error_msg, this.lang['not-added']);
                } else {
	                toastr.success(this.lang['added-cart'], name);
                }
            });
        },
        addToCompare(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtocompare/' + id)
                .then(result => {
                    if(result.data.response==1){
		                var comp = parseInt($("#comp-count").text())+1;
		                $("#comp-count").text(comp).show();
		                toastr.success(this.lang['added-comparison'], name);
		            } else if(result.data.response==3) {
		             	toastr.error(this.lang['full']);
                    } else {
                    	toastr.info(this.lang['already-compare'], name);
                    }
                });
            } else {
                toastr.error(this.lang['login-compare']);
            }
        },
        addToFavorites(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtofavorites/' + id)
                .then(result => {
                    if(result.data.response==1){
		                var fav = parseInt($("#fav-count").text())+1;
		                $("#fav-count").text(fav).show();
		                toastr.success(this.lang['added-favorites'], name);
		            } else if(result.data.response==2){
                    	toastr.error(result.data.error_msg, this.lang['not-added-wishlist']);
                    } else {
                    	toastr.info(this.lang['already-favorites'], name);
                    }
                });
            } else {
                toastr.error(this.lang['login-favorites']);
            }
        }
	}
})

var app = new Vue({
	el: '#for_women',
	data: {
		items:[
		],
		selected:[],
		lang:[]
	},
	created(){
		function getTranslation() {
            return axios.get('/translate/cart');
        }
		function getProducts() {
			return axiosAPIv1.get('/forwomen');
		}
    	axios.all([
    		getTranslation(),
			getProducts()
		]).then(axios.spread(
			(	
				translation,
				response
			) => {
				this.lang = translation.data.data;
				
				if(response.data){
					$('.flexhide2').remove();
					$('#for_women').show();
					this.items = response.data;
				}

				if(this.items.length > 10) {
					$('#see-all-for-women').removeClass('hide');
				} else {
					$('#see-all-for-women').addClass('hide');
				}
		}))
		.catch($.noop);
	},
	updated() {
	    // Fired every second, should always be true
	    // console.log(this.items);
	    $('.forwomen-slider').owlCarousel({
	      items:1,
	      dots: false,
	      nav: false,
	      mouseDrag: false,
	      touchDrag: false,
	      pullDrag: false,
	      freeDrag: false,
	      navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	      responsive:{
	          0:{
	            items:2,
	          },
	          480:{
	            items:2,
	          },
	          768:{
	            items:2,
	          },
	          992:{
	            items:5,
	          },
	          1200:{
	            items:5,
	          }
	        }
	    });
	  },

	methods:{
		selectItem(id){
			this.selected.push(id)
		},
        addToCart(id,name){
	        axios.get(location.origin + '/cart/add',{
	            params: {
	                prod_id:id,
	                qty:1
	            }
	        })
	        .then(result => {
	            $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                if(result.data.error_msg)
                {	
                	if(result.data.error_msg=='Redirect')
                		window.location = '/product/' + result.data.slug;
                	else
	                	toastr.error(result.data.error_msg, this.lang['not-added']);
                } else {
	                toastr.success(this.lang['added-cart'], name);
                }
            });
        },
        addToCompare(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtocompare/' + id)
                .then(result => {
                    if(result.data.response==1){
		                var comp = parseInt($("#comp-count").text())+1;
		                $("#comp-count").text(comp).show();
		                toastr.success(this.lang['added-comparison'], name);
		            } else if(result.data.response==3) {
		             	toastr.error(this.lang['full']);
                    } else {
                    	toastr.info(this.lang['already-compare'], name);
                    }
                });
            } else {
                toastr.error(this.lang['login-compare']);
            }
        },
        addToFavorites(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtofavorites/' + id)
                .then(result => {
                    if(result.data.response==1){
		                var fav = parseInt($("#fav-count").text())+1;
		                $("#fav-count").text(fav).show();
		                toastr.success(this.lang['added-favorites'], name);
		            } else if(result.data.response==2){
                    	toastr.error(result.data.error_msg, this.lang['not-added-wishlist']);
                    } else {
                    	toastr.info(this.lang['already-favorites'], name);
                    }
                });
            } else {
                toastr.error(this.lang['login-favorites']);
            }
        }
	}
})


var app = new Vue({
	el: '#for_cyber',
	data: {
		items:[
		],
		selected:[],
		lang:[]
	},
	created(){
		function getTranslation() {
            return axios.get('/translate/cart');
        }
		function getProducts() {
			return axiosAPIv1.get('/forcyber');
		}
    	axios.all([
    		getTranslation(),
			getProducts()
		]).then(axios.spread(
			(	
				translation,
				response
			) => {
				this.lang = translation.data.data;

				if(response.data){
					$('.flexhide3').remove();
					$('#for_cyber').show();
					this.items = response.data;
				}

				if(this.items.length > 10) {
					$('#see-all-cyber-zone').removeClass('hide');
				} else {
					$('#see-all-cyber-zone').addClass('hide');
				}
		}))
		.catch($.noop);
	},
	updated() {
	    // Fired every second, should always be true
	    // console.log(this.items);
	    $('.forcyber-slider').owlCarousel({
	      items:1,
	      dots: false,
	      nav: false,
	      mouseDrag: false,
	      touchDrag: false,
	      pullDrag: false,
	      freeDrag: false,
	      navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	      responsive:{
	          0:{
	            items:2,
	          },
	          480:{
	            items:2,
	          },
	          768:{
	            items:2,
	          },
	          992:{
	            items:5,
	          },
	          1200:{
	            items:5,
	          }
	        }
	    });
	  },
	methods:{
		selectItem(id){
			this.selected.push(id)
		},
        addToCart(id,name){
	        axios.get(location.origin + '/cart/add',{
	            params: {
	                prod_id:id,
	                qty:1
	            }
	        })
	        .then(result => {
	            $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                if(result.data.error_msg)
                {	
                	if(result.data.error_msg=='Redirect')
                		window.location = '/product/' + result.data.slug;
                	else
	                	toastr.error(result.data.error_msg, this.lang['not-added']);
                } else {
	                toastr.success(this.lang['added-cart'], name);
                }
            });
        },
        addToCompare(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtocompare/' + id)
                .then(result => {
                    if(result.data.response==1){
		                var comp = parseInt($("#comp-count").text())+1;
		                $("#comp-count").text(comp).show();
		                toastr.success(this.lang['added-comparison'], name);
		            } else if(result.data.response==3) {
		             	toastr.error(this.lang['full']);
                    } else {
                    	toastr.info(this.lang['already-compare'], name);
                    }
                });
            } else {
                toastr.error(this.lang['login-compare']);
            }
        },
        addToFavorites(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtofavorites/' + id)
                .then(result => {
                    if(result.data.response==1){
		                var fav = parseInt($("#fav-count").text())+1;
		                $("#fav-count").text(fav).show();
		                toastr.success(this.lang['added-favorites'], name);
		            } else if(result.data.response==2){
                    	toastr.error(result.data.error_msg, this.lang['not-added-wishlist']);
                    } else {
                    	toastr.info(this.lang['already-favorites'], name);
                    }
                });
            } else {
                toastr.error(this.lang['login-favorites']);
            }
        }
	}
})

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
    return Math.round(v);
}
