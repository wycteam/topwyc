
Vue.component('ewallet-button',  require('../components/ewallet/EwalletButton.vue'));

var app = new Vue({
	 el: '#ewallet_container',
	 components: {
	  	Multiselect: window.VueMultiselect.default
	  },
	 data: {
	 	transferTo:[],
	 	lang:[],
		btns : [
			{
				 icon: 'account_balance_wallet'
				,title: 'Load Money'
				,subtitle: 'Offline or Online'
				,modal: 'modalLoad'
			},
			{
				 icon: 'exit_to_app'
				,title: 'Withdraw Funds'
				,subtitle: 'Get your saved money'
				,modal: 'modalWithdraw'
			},
			{
				 icon: 'compare_arrows'
				,title: 'Transfer or Receive'
				,subtitle: 'to/from another Account'
				,modal: 'modalTransferReceive'
			},
			{
				 icon: 'account_balance'
				,title: 'Bank Account'
				,subtitle: 'Register your account'
				,modal: 'modalAddBank'
			},

		],
		showLoadingOptions:true,
		showOnlineArea:false,
		showOfflineArea:false,
		showLoadingBack:false,

		showTransferReceiveOptions: true,
		showTransferArea: false,
		showReceiveArea: false,
		showTransferReceiveBack:false,

		user_id:null,
		ewallet_balance:0,
		usable_balance:0,
		bank_accounts: false,
		transactions:false,
		banks:false,
		fetchedAccounts:[],
		bankAccountForm:new Form({
			bank_id:'',
			account_name:null,
			account_number:null,
			reaccount_number:null,
		}, { baseURL: 'http://'+Laravel.base_api_url }),
		bankAccountProcessing:null,
		bankAccountMode:true,

		loadOfflineForm:new Form({
			bank_id:'',
			amount:null,
		}, { baseURL: 'http://'+Laravel.base_api_url }),
		withdrawOfflineForm : new Form({
			bank_id:'',
			amount:null,
			type:'Withdraw',
		}, { baseURL: 'http://'+Laravel.base_api_url }),
		transferForm:new Form({
			bank_id:'',
			amount:null,
			type:'Transfer',
			note:''
		}, { baseURL: 'http://'+Laravel.base_api_url }),
		isFetchingAccounts:false,
		transferCode:'',

		current_transaction:'',
		current_transaction_image:'',
		current_transaction_status:'',
		current_transaction_reason:'',
		baseHeight :0,
	    baseWidth :0,
	    baseZoom : 1,
		showZoomable : false,
		
		maxDepositRequest:700000,
		friends:[]
	},

	mounted() {
	    $('#modalDepositUpload').on('shown.bs.modal', () => {
	        this.baseZoom = 1;
	        img = $('<img id="shittyimage" class="center-block img-responsive" src="'+ this.current_transaction_image +'">').on('load', () => {
	          var $modalImg = $('<img src="'+ this.current_transaction_image +'" class="center-block  img-responsive" >');
	          $modalImg.appendTo('#doc-container');
	          this.baseHeight = $modalImg.height();
	          this.baseWidth = $modalImg.width();
	          

	          $modalImg.draggable();
	          
	        });
	    });

	    $('#modalDepositUpload').on('hide.bs.modal', () => {
	        $('#doc-container').empty();
	        this.showZoomable = false;
	    });
  },

	watch:{
		baseZoom:function(newValue){
			var changedHeightSize =  this.baseHeight * newValue;
			var changedWidthSize = this.baseWidth * newValue;
			$('#doc-container').find('img').css('height',changedHeightSize);
			$('#doc-container').find('img').css('width',changedWidthSize);
			$('#doc-container').find('img').removeClass('img-responsive');
		},

		current_transaction_image:function(){
			this.baseZoom = 1;
		},

		transactions:function(){
			$('#ewallet_transaction_table').DataTable().destroy();

			setTimeout(function(){
				currentTable = $('#ewallet_transaction_table').DataTable({
			        pageLength: 10,
			        responsive: true,
			    });
			},800);
		}

	},

	created(){
		Event.listen('Imgfileupload.depositUpload',(result) => {

			if(result)
			{
				toastr.success(this.lang['wait-verification'],this.lang['success-upload']);
			}else
			{
				toastr.error(this.lang['try-again'],this.lang['failed-upload'])
			}
			$('#modalDepositUpload').modal('toggle');
		})
		this.user_id = Laravel.user.id;
		// axios
		function getTranslation() {
			return axios.get('/translate/ewallet');
		}
		function getEwalletBalance() {
			return axiosAPIv1.get('/user/'+this.user_id+'/ewallet');
		}
		function getUsableBalance() {
			return axiosAPIv1.get('/getUsable');
		}
		function getBankAccounts() {
			return axiosAPIv1.get('/user/'+this.user_id+'/bank-accounts');
		}
		function getTransactions() {
			return axiosAPIv1.get('/ewallet-transaction');
		}
		function getPurchasedOrders() {
			return axiosAPIv1.get('/ewallet-transaction/get-purchased-orders');
		}
		function getCancelledOrders() {
			return axiosAPIv1.get('/ewallet-transaction/get-cancelled-orders');
		}
		function getSoldOrders() {
			return axiosAPIv1.get('/ewallet-transaction/get-sold-orders');
		}
		function getReturnedOrders() {
			return axiosAPIv1.get('/ewallet-transaction/get-buyer-returned-orders');
		}
		function getBanks() {
			return axiosAPIv1.get('/banks');
		}
		function getDocs() {
			return axiosAPIv1.get('/users/'+Laravel.user.id+'/documents');
		}
		
		axios.all([
			 getTranslation()
			,getTransactions()
			,getEwalletBalance()
			,getUsableBalance()
			,getBankAccounts()
			,getBanks()
			,getDocs()
			,getPurchasedOrders()
			,getCancelledOrders()
			,getSoldOrders()
			,getReturnedOrders()
		]).then(axios.spread(
			(
				 language
				,transactions
				,balance
				,usable
				,bank_accounts
				,banks
				,documents
				,purchased_transactions
				,cancelled_transactions
				,sold_transactions
				,returned_transactions
			) => {
				
			this.lang = language.data.data;
			this.btns[0].title = this.lang["load-money"];
			this.btns[1].title = this.lang["withdraw-funds"];
			this.btns[2].title = this.lang["transfer-or-receive"];
			this.btns[3].title = this.lang["bank-account"];

			this.btns[0].subtitle = this.lang["offline-or-online"];
			this.btns[1].subtitle = this.lang["withdraw-funds"];
			this.btns[2].subtitle = this.lang["get-saved-money"];
			this.btns[3].subtitle = this.lang["reg-account"];

			this.ewallet_balance = balance.data.success? parseFloat(balance.data.data.amount):0;
			this.usable_balance = usable.data.success? parseFloat(usable.data.data.usable):0;
			this.bank_accounts = bank_accounts.data.success? bank_accounts.data.data.bank_accounts:false;
			this.banks = banks.data.success? banks.data.data:false;
			this.transactions = transactions.status==200? transactions.data:false;

			if(purchased_transactions.status == 200)
			{
				this.transactions = this.transactions.concat(purchased_transactions.data);
			}

			if(cancelled_transactions.status == 200)
			{
				this.transactions = this.transactions.concat(cancelled_transactions.data);
			}

			if(sold_transactions.status == 200)
			{
				this.transactions = this.transactions.concat(sold_transactions.data);
			}

			if(returned_transactions.status == 200)
			{
				this.transactions = this.transactions.concat(returned_transactions.data);
			}

			if(documents.status==200)
			{
				if(documents.data.length)
				{
					var enterprise = 1;
		            enterprise    *= documents.data.filter(obj => { return obj.type === 'sec' && obj.status === 'Verified' }).length;
					
		            enterprise    *= documents.data.filter(obj => { return obj.type === 'bir' && obj.status === 'Verified' }).length;
					
		            enterprise    *= documents.data.filter(obj => { return obj.type === 'permit' && obj.status === 'Verified' }).length;
					if(enterprise)
					{
						this.maxDepositRequest = 7000000 - this.ewallet_balance;
					}else
					{
						this.maxDepositRequest = 1000000 - this.ewallet_balance;
					}

				}
			}

			Vue.nextTick(function () {
				setTimeout(function(){
		            // $('[data-toggle="tooltip"]').attr('data-original-title',$(this).attr('data-temp-title'));
		            $('#ewallet_transaction_table tr[data-toggle="tooltip"]').each(function(){
		            	$(this).attr('data-original-title',$(this).attr('data-temp-title'));
		            });
		            $('[data-toggle="tooltip"]').tooltip()
		            // $('[data-toggle="tooltip"]').tooltip('fixTitle');
		            $('#ewallet_transaction_table').dataTable();
				},500);
	        })
		}))
		.catch($.noop);
	},

	methods: {
		getEwalletBalance() {
			return axiosAPIv1.get('/user/'+Laravel.user.id+'/ewallet');
		},
		getUsableBalance() {
			return axiosAPIv1.get('/getUsable');
		},
		updateBreakdown() {
			return axiosAPIv1.get('/ewallet-transaction/update-breakdown');
		},
		checkSubmitPic(){
			if($("#img_upload").val() == ''){
		       toastr.error('Please upload your deposit slip.');
		    }
		},
		CreditOrDebit(type){
			switch(type)
			{
				case 'Purchase':
				case 'Withdraw':
				case 'Transfer':
				case 'Product Returned by Buyer':
					return 'text-danger';
				case 'Deposit':
				case 'Received':
				case 'Cancelled Order':
				case 'Returned Order':
				case 'Sold':
					return 'text-success';

			}
		},

		tooltipForTransaction(status,type,reason){
			if(status == 'Cancelled' )
			{
				return reason;
			}else if((status =="Pending")&&(type == "Deposit")){
				return "Please upload you deposit slip.";
			}
			return '';
		},

		fileChange(e) {
	      var files = e.target.files || e.dataTransfer.files;
	      if (!files.length)
	        return;
	      this.createImage(files[0]);

	    },

	    createImage(file) {
	      var image = new Image();
	      var reader = new FileReader();
	      var vm = this;

	      reader.onload = (e) => {
	        // this.current_transaction_image = e.target.result;
        	$('#doc-container').empty();
	        this.baseZoom = 1;
	        img = $('<img id="shittyimage" class="center-block img-responsive" src="'+  e.target.result +'">').on('load', () => {
	          var $modalImg = $('<img src="'+  e.target.result +'" class="center-block img-responsive" >');
	          $modalImg.appendTo('#doc-container');
	          this.baseHeight = $modalImg.height();
	          this.baseWidth = $modalImg.width();
					this.showZoomable = true;

	          $modalImg.draggable();
	          
	        });
	      };
	      reader.readAsDataURL(file);
	    },

	    removeImage: function (e) {
	      this.image = '';
	    },

		openModalUploadDeposit(transaction_id,status,reason,ref_number){

			this.current_transaction = transaction_id;
			this.current_transaction_status = status;
			this.current_transaction_image = false;
			this.current_transaction_reason = reason;
			
			axios.get("/shopping/images/deposit-slip/"+ref_number+'.jpg')
				.then(result => {
					this.current_transaction_image = "/shopping/images/deposit-slip/"+ref_number+'.jpg';
					this.showZoomable = true;
				})
				.catch($.noop);
			$('#modalDepositUpload').modal('toggle');
		},

		//loading of ewallet
		limitText (count) {
	      return `and ${count} other accounts`
	    },

		chooseOnline: function(){
			this.showLoadingOptions = false;
			this.showOfflineArea = false;
			this.showOnlineArea = true;
			this.showLoadingBack = true;

		},

		chooseOffline: function(){
			this.showLoadingOptions = false;
			this.showOnlineArea = false;
			this.showOfflineArea = true;
			this.showLoadingBack = true;

		},
		//transfer/receive of ewallet
		chooseTransfer: function(){
			axiosAPIv1
				.get('/friends')
				.then(result => {
					let friends = [];
					for(var i=0; i<result.data.friends.length; i++)
					{
						friends.push(result.data.friends[i].user);
					}
					this.friends = friends;
					this.fetchedAccounts = friends;
				})

			this.showTransferReceiveOptions = false;
			this.showReceiveArea = false;
			this.showTransferArea = true;
			this.showTransferReceiveBack = true;
		},

		chooseReceive: function(){
			this.showTransferReceiveOptions = false;
			this.showTransferArea = false;
			this.showReceiveArea = true;
			this.showTransferReceiveBack = true;
		},

    	openModal: function (modal) {
			resetModals();
    		$('#'+modal).modal('toggle')
	    },

	    resetModals: function(){
	    	this.showLoadingOptions = true;
	    	this.showOnlineArea = false;
	    	this.showOfflineArea = false;
			this.showLoadingBack = false;

	    	this.showTransferReceiveOptions = true;
			this.showTransferArea = false;
			this.showReceiveArea = false;
			this.showTransferReceiveBack = false;

			this.bankAccountMode = true;
			this.bankAccountForm.bank_id='';
			this.bankAccountForm.account_name=null;
			this.bankAccountForm.account_number=null;
			this.bankAccountForm.reaccount_number=null;

			this.bankAccountProcessing = null;

			this.current_transaction_reason = '';

			this.bankAccountForm = new Form({
				bank_id:'',
				account_name:null,
				account_number:null,
				reaccount_number:null,
				status:''
			}, { baseURL: 'http://'+Laravel.base_api_url });

			this.loadOfflineForm = new Form({
				bank_id:'',
				amount:null,
			}, { baseURL: 'http://'+Laravel.base_api_url })

			this.withdrawOfflineForm = new Form({
				bank_id:'',
				amount:null,
				type:'Withdraw',
			}, { baseURL: 'http://'+Laravel.base_api_url })

			this.transferForm = new Form({
				bank_id:'',
				amount:null,
				type:'Transfer',
				note:''
			}, { baseURL: 'http://'+Laravel.base_api_url })

			this.current_transaction_image='';
	    },

	    nameWithEmail({ full_name, email }){
	    	return `${full_name} (${email})`;
	    },

	    submitNewBankAccount(){
	    	this.bankAccountForm.status = "Pending";
            this.bankAccountForm.submit('post', '/v1/user/'+this.user_id+'/bank-accounts')
                .then(result => {
                    if(result.success)
                    {
                    	this.bank_accounts.unshift(result.data);
	                    toastr.success(this.lang["account-added"]);
	                    this.bankAccountForm.bank_id='';
						this.bankAccountForm.account_name=null;
						this.bankAccountForm.account_number=null;
						this.bankAccountForm.reaccount_number=null;
                    }else
                    {
	                    toastr.error(this.lang['try-again']);
                    }
                })
                .catch($.noop);
	    },

	    makeBankAccountModeAdd(){
	    	this.bankAccountMode = true;
			this.bankAccountForm.bank_id='';
			this.bankAccountForm.account_name=null;
			this.bankAccountForm.account_number=null;
			this.bankAccountForm.reaccount_number=null;

			this.bankAccountProcessing = null;
	    },

	    updateBankAccount(){
	    	function putBankAccounts() {
				return axiosAPIv1.put('/user/'+Laravel.user.id+'/bank-accounts/'+this.bankAccountProcessing);
			}

			this.bankAccountForm.submit('put', '/v1/user/'+Laravel.user.id+'/bank-accounts/'+this.bankAccountProcessing)
                .then(result => {
                    if(result.success)
                    {
                    	for(i=0; i< this.bank_accounts.length; i++)
                    	{
                    		if(this.bank_accounts[i].id == this.bankAccountProcessing)
                    		{
				                this.bank_accounts[i] = result.data;
                    		}
                    	}
                    	this.makeBankAccountModeAdd();
	                    toastr.success(this.lang["account-updated"]);
                    }else
                    {
	                    toastr.error(this.lang['try-again']);
                    }
                })
                .catch($.noop);
	    },

	    validateEmail(email) {
		    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    return re.test(email);
		},

		removeDuplicates(originalArray, prop) {
		     var newArray = [];
		     var lookupObject  = {};

		     for(var i in originalArray) {
		        lookupObject[originalArray[i][prop]] = originalArray[i];
		     }

		     for(i in lookupObject) {
		         newArray.push(lookupObject[i]);
		     }
		      return newArray;
		 },

	    fetchAccountsByEmail(name){
	    	var fetched;
			
			var friends = null;
			//clone
			friends = $.extend(friends,this.friends);
			//convert to array for multiselect
			friends = $.map(friends, function(el) { return el });
	    	
	    	if(this.validateEmail(name))
	    	{
				this.isFetchingAccounts = true;
		    	//accept only email addresses for not friends
				this.fetchedAccounts = friends;

		    	
				axiosAPIv1
					.get('/users?searchByEmail='+name)
					.then(accounts => {
						if(accounts.data.length)
						{
							this.fetchedAccounts = this.fetchedAccounts.concat(accounts.data);
							this.fetchedAccounts = this.removeDuplicates(this.fetchedAccounts,'email');
						}else
						{
				    		this.fetchedAccounts = friends;
						}
						this.isFetchingAccounts = false;
					})
					.catch(error => {
						this.fetchedAccounts = friends;
					})

	    	}else 
	    	{
	    		this.fetchedAccounts =  friends;
	    	}
	    },
	  
	    showBank(bank_account_id){
	    	function getBankAccounts(id) {
				return axiosAPIv1.get('/user/'+Laravel.user.id+'/bank-accounts/'+id);
			}
	    	axios.all([
				getBankAccounts(bank_account_id)
			]).then(axios.spread(
				(
					 bank_account
				) => {
				if(bank_account.data.success)
				{
					this.bankAccountProcessing = bank_account_id;
					this.bankAccountForm.bank_id = bank_account.data.data.bank_id;
					this.bankAccountForm.account_name = bank_account.data.data.account_name;
					this.bankAccountForm.account_number = bank_account.data.data.account_number;
					this.bankAccountForm.reaccount_number = bank_account.data.data.account_number;
					this.bankAccountForm.status = bank_account.data.data.status;
					this.bankAccountMode = false;

				}
			}))
			.catch($.noop);
	    },

	    submitLoadOffline(){
	    	this.loadOfflineForm.submit('post', '/v1/ewallet-transaction')
                .then(result => {
                    if(result.success)
                    {
                    	this.transactions.push(result.data);
                    	$('#modalLoad').modal('toggle');
                    	toastr.info(this.lang['ref-no']+result.data.ref_number+'<br>'+this.lang['deposit']+'<br>'+this.lang['bank']+': '+result.data.description, '', {
						  timeOut: 0,
						  extendedTimeOut: 0
						});
						toastr.success(this.lang["success-request"]);

						//show instruction modal
						$('#modalInstructions').modal('toggle');
                    }else
                    {
	                    toastr.error(this.lang['try-again']);
                    }
                })
                .catch($.noop);
	    },

	    submitWithdrawOffline(){
	    	this.withdrawOfflineForm.submit('post', '/v1/ewallet-transaction')
                .then(result => {
                    if(result.success)
                    {
                    	this.transactions.push(result.data);
                    	$('#modalWithdraw').modal('toggle');
                    	toastr.info(this.lang['ref-no']+result.data.ref_number+'<br>'+this.lang['withdraw']+'<br>'+this.lang['bank']+': '+result.data.description, '', {
						  timeOut: 0,
						  extendedTimeOut: 0
						});
						toastr.success(this.lang["success-request"]);
                    }else
                    {
	                    toastr.error(this.lang['try-again']);
                    }
                })
                .catch($.noop);
	    },

	    submitTransfer(){
	    	if(this.transferTo.length)
	    	{
	    		this.transferTo.forEach((transfer_id) =>{
		    		this.transferForm.bank_id = transfer_id.id;
					//submit form
					
					if(this.transferForm.amount>99){
						this.transferForm.submit('post', '/v1/ewallet-transaction')
			                .then(result => {

			                    this.fetchedAccounts = [];

			                    if(result.success)
			                    {
			                    	this.transactions.push(result.data);

									toastr.success(this.lang['give-receiver']);
						    		$('#btnTransferSubmit .loader').fadeOut(function(){
						    			$('#btnTransferSubmit .btn-content').fadeIn();
						    		},100);

						    		this.transferForm.amount = '';
						    		this.transferTo = [];
					            	
					            	$('#modalTransferReceive').modal('toggle');

			                    	toastr.info(this.lang['code']+': '+result.data.ref_number+'<br>'+this.lang["transfer"]+'<br> '+result.data.description, '', {
									  timeOut: 0,
									  extendedTimeOut: 0
									});

			                    	var self = this;

									this.getEwalletBalance()
										.then((balance) => {
											self.ewallet_balance = balance.data.success ? parseFloat(balance.data.data.amount) : 0;
										});

									this.getUsableBalance()
										.then((usable) => {
											self.usable_balance = usable.data.success ? parseFloat(usable.data.data.usable) : 0;
										});

									this.updateBreakdown()
										.then((response) => {
											var content = response.data.breakdown;
;
											$('a#breakdown').attr('data-content', content);						
										});

			                    }else
			                    {

				                    toastr.error(this.lang['try-again']);
			                    }
			                    this.fetchedAccounts = [];
			                })
			                .catch($.noop);
					} else {
						this.transferForm.submit('post', '/v1/ewallet-transaction')
							.then(result => {

							})
				            .catch($.noop);
						$('#btnTransferSubmit .loader').fadeOut(function(){
			    			$('#btnTransferSubmit .btn-content').fadeIn();
			    		},100);
						toastr.error(this.lang['mininum-amount']);
					}
	    		});

	    	}else
	    	{
				this.transferForm.submit('post', '/v1/ewallet-transaction')
					.then(result => {

					})
		            .catch($.noop);
				$('#btnTransferSubmit .loader').fadeOut(function(){
	    			$('#btnTransferSubmit .btn-content').fadeIn();
	    		},100);
	    	}
	    },
	     submitReceive(){
	    	if(this.transferCode == '')
	    	{
				toastr.error(this.lang['invalid-code']);
	        	$('#modalTransferReceive').modal('toggle');
				
	    	}else
	    	{
		    	axiosAPIv1
	    		.post('/ewallet-transaction/refnum',{
	    			refnum:this.transferCode
	    		})
	    		.then(result => {
	    			if(result.data)
	    			{
						this.transactions.push(result.data.data);
						axiosAPIv1
							.get('/user/'+this.user_id+'/ewallet')
							.then(response => {
								this.usable_balance += (parseFloat(response.data.data.amount)-this.ewallet_balance);
								this.ewallet_balance = parseFloat(response.data.data.amount);
								toastr.success(this.lang['ewallet-received']);
							})
						this.transferCode = '';
			        	$('#modalTransferReceive').modal('toggle');
	    			}else
	    			{
						toastr.error(this.lang['invalid-code']);
			        	$('#modalTransferReceive').modal('toggle');
	    			}
	    		})
	    	}

	    }
	}
})

$('#uploadDepositSlip').click(function(){
	$('.deposit-uploader input').click();
	$('#submitDepositSlip').prop('disabled', false);
});

$('#modalDepositUpload').on('hidden.bs.modal', function() {
	$('#submitDepositSlip').prop('disabled', true);
});

$('#ewallet_container').on('hidden.bs.modal', function() {
	app.resetModals(); 
});

$(document).on('click', function (e) {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
        }

    });
});