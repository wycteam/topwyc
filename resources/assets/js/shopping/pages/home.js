
// Vue.component('news-articles', require('../visa/components/news/News.vue'));
//
// new Vue({
//   el: '#newsArticles',
//   data() {
//     return {
//       items: []
//     }
//   },
//   methods: {
//     fetchMessages: function() {
//         window.axios.get('news').then((res) => {
//             this.items = res.data;
//             //console.log(res.data);
//         });
//     }
//  },
//  created() {
//       this.fetchMessages();
//   },
// });

// Function to check element is exist
$.fn.exist = function(){ return $(this).length > 0; }

// Function to get window width

function get_width() {
  return $(window).width();
}



$(function(){

  $(document).on("click","#wechatLink",function() {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val('JerryChua').select();
    document.execCommand("copy");
    $temp.remove();
    window.location.href= 'weixin://';
  })

  //open registration
    // Select Dropdown
    $('html').click(function() {
        $('.select .dropdown').hide();
    });
    $('.select').click(function(event){
        event.stopPropagation();
    });
    $('.select .select-control').click(function(){
        $(this).parent().next().toggle();
    })
    $('.select .dropdown li').click(function(){
        $(this).parent().toggle();
        var text = $(this).attr('rel');
        $(this).parent().prev().find('div').text(text);
    })

  // open navigation dropdown on hover (only when width >= 768px)
  $('ul.nav li.dropdown').hover(function() {
    if (get_width() >= 767) {
      $(this).addClass('open');
    }
  }, function() {
    if (get_width() >= 767) {
      $(this).removeClass('open');
    }
  });

  // Navigation submenu
  $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
    event.preventDefault();
    event.stopPropagation();
    $(this).parent().siblings().removeClass('open');
    $(this).parent().toggleClass('open');
  });

  // owlCarousel for Home Slider
  if ($('.home-slider').exist()) {
    $('.home-slider').owlCarousel({
      items:1,
      autoplay:true,
      autoplayHoverPause:true,
      dots:false,
      nav:true,
      stagePadding:-5,
      navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    });
  }

  // owlCarousel for Widget Slider
  if ($('.widget-slider').exist()) {
    var widget_slider = $('.widget-slider');
    widget_slider.owlCarousel({
      items:1,
      dots: false,
      nav: true,
      navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
      responsive:{
        0:{
          items:1,
        },
        480:{
          items:2,
        },
        768:{
          items:3,
        },
        992:{
          items:1,
        }
      }
    });
  }

  // owlCarousel for Product Slider
  if ($('.product-slider').exist()) {
    // var product_slider = $('.product-slider')
    // product_slider.owlCarousel({
    //   items:1,
    //   dots: false,
    //   nav: true,
    //   navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    //   responsive:{
    //       0:{
    //         items:1,
    //       },
    //       480:{
    //         items:2,
    //       },
    //       768:{
    //         items:3,
    //       },
    //       992:{
    //         items:3,
    //       },
    //       1200:{
    //         items:4,
    //       }
    //     }
    // });
  }

   // owlCarousel for Related Product Slider
  if ($('.related-product-slider').exist()) {
    var related_product_slider = $('.related-product-slider')
    related_product_slider.owlCarousel({
      dots: false,
      nav: true,
      navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
      responsive:{
          0:{
            items:1,
          },
          480:{
            items:2,
          },
          768:{
            items:3,
          },
          992:{
            items:5,
          },
          1200:{
            items:6,
          }
        }
    });
  }

  // owlCarousel for Brand Slider
  if ($('.brand-slider').exist()) {
    var brand_slider = $('.brand-slider');
    brand_slider.owlCarousel({
      dots:false,
      nav:true,
      navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
      responsive:{
        0:{
          items:1,
        },
        480:{
          items:2,
          margin:15
        },
        768:{
          items:3,
          margin:15
        },
        992:{
          items:4,
          margin: 30
        },
        1200:{
          items:6,
          margin: 30
        }
      }
    });
  }

  // Back top Top
    $(window).scroll(function(){
    if ($(this).scrollTop()>70) {
      $('.back-top').fadeIn();
    } else {
      $('.back-top').fadeOut();
    }
  });

    var toggleAds = '<div class="toggle-ads"><i class="fa fa-eye-slash" aria-hidden="true"></i></div>';
    $('div.home-slider-row').prepend(toggleAds);

    $('.toggle-ads').on('click', function() {
      $('div.home-slider').toggleClass('hide');

      $('.toggle-ads i').toggleClass('fa-eye-slash').toggleClass('fa-eye');

      if(!$('div.home-slider').is(':visible')) {
        $('.toggle-ads').css('margin-top', '2px');
      } else {
        $('.toggle-ads').css('margin-top', '35px');
      }
    });


});

new WOW().init();
