Vue.component('faq-list',  require('../components/pages/Faq.vue'));

var app = new Vue({
  el: '#faq-container-content',
  data: {
  	modalMenuShowing:false,
	items:[],
	param:'',

	},
	created(){
		
		this.param = location.search.split('help=')[1];

		function getFaq() {
			return axiosAPIv1.get('/faq');
		}
		
		axios.all([
			getFaq()
		]).then(axios.spread(
			(
				faq
			) => {
			if(faq.data.data != 0)
				this.items = faq.status == 200? faq.data:[];
		}))
		.catch($.noop);

	},
  	methods:{
  	}
});

$('#faqModalMenu').on('shown.bs.modal', function (e) {
    $('button[data-target="#faqModalMenu"] span:eq(0)').removeClass().addClass('fa fa-times');
});

$('#faqModalMenu').on('hidden.bs.modal', function (e) {
    $('button[data-target="#faqModalMenu"] span:eq(0)').removeClass().addClass('fa fa fa-bars');
});