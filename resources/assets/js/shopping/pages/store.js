Vue.component('feat-store',  require('../components/store/Featured.vue'));

var app = new Vue({
	el: '.featured_stores',
	data: {
		items:[
		],
		selected:[]
	},
	created(){
			function getProducts() {
				return axiosAPIv1.get('/featured');
			}
	    	axios.all([
				getProducts()
			]).then(axios.spread(
				(
					 response
				) => {
				this.items = response.data;
			}))
			.catch($.noop);

	},
	updated() {
	    // Fired every second, should always be true
	    console.log(this.items);
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover();

	  },
	methods:{
		selectItem(id){
			this.selected.push(id)
		}
	}
})

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
    return Math.round(v);
}
