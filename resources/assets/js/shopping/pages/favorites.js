Vue.component('favorites-list',  require('../components/pages/Favorites.vue'));
Vue.component('fav-modal',  require('../components/DialogModal.vue'));

Vue.filter('currency', function (value) {
    return  '₱'+numberWithCommas(parseFloat(value).toFixed(2));
});

var app = new Vue({

	el: '#favorites-content',
	data: {
		items:[],
		no_item: '',
		id:'',
		index:'',
		lang:[]
	},
	created(){
		function getTranslation() {
            return axios.get('/translate/favorites');
        }

		function getFavorites() {
			return axiosAPIv1.get('/favorites');
		}
		
		axios.all([
			getTranslation(),
			getFavorites()
		]).then(axios.spread(
			(	
				translation,
				favorites
			) => {

			this.lang = translation.data.data;

			if(favorites.data.data != 0)
				this.items = favorites.status == 200? favorites.data:[];
			else
				$('.no-item').show(); //this.no_item = "There are no items in your wishlist";
		}))
		.catch($.noop);
	},
	methods: {
		showRemoveDialog(id,index){
			this.id = id;
			this.index = index;
			$('#dialogRemove').modal('toggle');
		},
		deleteFavorites () {
			axiosAPIv1.get('/deletefavorites/' + this.id)			
			.then(result => {
				$("#fav-count").text(result.data);
				toastr.success(this.lang['removed'],this.items.data[this.index].name);
				this.items.data.splice(this.index, 1);
				if(this.items.data.length == 0)
					$('.no-item').show();
			});		
		},
		addtocart (id,index) {
			axios.get(location.origin + '/cart/add',{
			    params: {
			    	prod_id:$('#prod_id'+id).val(),
					qty:$('#qty'+id).val()
			    }
			})
			.then(result => {
				$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                if(result.data.error_msg)
                {
                	if(result.data.error_msg=='Redirect')
                		window.location = '/product/' + result.data.slug;
                	else
	                	toastr.error(result.data.error_msg, this.lang['not-added']);             
                } else {
					toastr.success(this.lang['added'], this.items.data[index].name);
					this.items.data.splice(index, 1);
					if(this.items.data.length == 0)
						$('.no-item').show();					
					axiosAPIv1.get('/deletefavorites/' + id)
					.then(result => {
						$("#fav-count").text(result.data);
					});          
                }
			});
		}
	}

});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
