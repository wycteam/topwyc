new Vue({
    el: '.messages-container',

    data: {
        userLatestMessages: [],
        selected: null,

        lang: []
    },

    router: new VueRouter({
        routes: [
            {
                path: '/u/:id',
                name: 'user',
                component: require('./ChatBox.vue')
            },
            {
                path: '/cr/:id',
                name: 'chat-room',
                component: require('./ChatBoxRoom.vue')
            }
        ]
    }),

    watch: {
        userLatestMessages(val) {
            setTimeout(() => {
                $('.user-list-container').perfectScrollbar('update');
            }, 1);
        }
    },

    methods: {
        getLatestMessages() {
            axiosAPIv1.get('/messages')
                .then(result => {
                    this.userLatestMessages = result.data;
                });
        },

        loadSelectedUser(data) {
            this.selected = data;

            this.$router.push({
                name: 'user',
                params: {
                    id: data.id
                }
            });
        },

        loadSelectedChatRoom(data) {
            this.selected = data;

            this.$router.push({
                name: 'chat-room',
                params: {
                    id: data.id
                }
            });
        },

        newMessage() {
            Event.fire('user-search.show');
        }
    },

    components: {
        'user-latest-message': require('./UserLatestMessage.vue'),
        'user-chat-room-message': require('./UserChatRoomMessage.vue'),
        'user-search': require('./UserSearch.vue')
    },

    created() {
        this.getLatestMessages();
        $('.user-latest-messages-container').perfectScrollbar('update');
        Event.listen('user-search.selected', this.loadSelectedUser);
        Event.listen('chat-room.selected', this.loadSelectedChatRoom);
        Event.listen('chat-box.send', this.getLatestMessages);
        Event.listen('chat-box.marked-as-read', this.getLatestMessages);
        Event.listen('new-chat-message', this.getLatestMessages);

        axios.get('/translate/messages')
            .then(language => {
                this.lang = language.data.data;
                toastr.info(this.lang['alert-msg'], this.lang['reminder'], {
                  timeOut: 0,
                  extendedTimeOut: 0
                });
        });

        setTimeout(function () {
        $('#toast-container').hide();
        },5000);
    },

    // mounted() {
    //     $('.user-list-container').perfectScrollbar();
    //     $('.conversation').perfectScrollbar();
    // }
});