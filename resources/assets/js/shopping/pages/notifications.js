Vue.component('tabs',  require('../components/pages/Notifications.vue'));
Vue.component('tab',  require('../components/pages/NotificationsTab.vue'));
Vue.component('tab-item',  require('../components/pages/NotificationsContent.vue'));


var app = new Vue({

	el: '#notifications-tab'

});


var app = new Vue({

	el: '#notifications-item',
	data: {
		items:[
			{
				id: 'OrderUpdates',
			    content: [
				    {
				    	src: 'shopping/img/general/notifications/notification.jpg',
					 	title: 'More Frees Shipping & COD!',
					 	content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
					 	date: '31-05-2017 18:33',
					 	href: ''
					},
				    {
				     	src: 'shopping/img/general/notifications/notification.jpg',
					 	title: 'More Frees Shipping & COD!',
					 	content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
					 	date: '31-05-2017 18:33',
					 	href: ''
					}					 
				] 	
			},
			{
				id: 'Promotions',
			    content: [
				    {
				     	src: 'shopping/img/general/notifications/notification.jpg',
					 	title: 'More Frees Shipping & COD!',
					 	content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
					 	date: '31-05-2017 18:33',
					 	href: ''
					}				 
				] 	
			},
			{
				id: 'Activity',
			    content: [
				    {
				     	src: 'shopping/img/general/notifications/notification.jpg',
					 	title: 'More Frees Shipping & COD!',
					 	content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
					 	date: '31-05-2017 18:33',
					 	href: ''
					},
				    {
				     	src: 'shopping/img/general/notifications/notification.jpg',
					 	title: 'More Frees Shipping & COD!',
					 	content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
					 	date: '31-05-2017 18:33',
					 	href: ''
					},
				    {
				     	src: 'shopping/img/general/notifications/notification.jpg',
					 	title: 'More Frees Shipping & COD!',
					 	content: 'Lorem ipsum dolor sit amet, illum facilisi ei vis, ut reque scriptorem nec. Sit discere ancillae ad, pro quot persius no, in cetero delicatissimi sed. Vidit veniam his ut, vim te vocibus patrioque. No atqui dicant dictas duo. Has persius offendit sadipscing ei, te amet efficiantur vim.',
					 	date: '31-05-2017 18:33',
					 	href: ''
					}					 
				] 	
			}
		]
	}

});