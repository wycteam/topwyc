Vue.component('friend-container',  require('../components/friends/FriendContainer.vue'));

window.timeout = null;

var friendApp = new Vue({
    el: '#relationships_container',

    data: {
        friends     : [],
        requests    : [],
        recommended : [],
        searched    : [],
        keyword     : '',

        lang        :[]
    },

    watch: {
        // whenever question changes, this function will run
        keyword: function (newKeyword) {
            clearTimeout(window.timeout);
            window.timeout = setTimeout(() => {
                if(newKeyword == '')
                {
                    this.searched = [];
                }else
                {
                     axiosAPIv1
                        .get('/users?search='+newKeyword)
                        .then(accounts => {
                            
                            temp_recommended = [];
                            temp_searched = [];
                            for(var i=0; i<accounts.data.length; i++)
                            {
                                temp = [];
                                temp.user = accounts.data[i];
                                temp_recommended.push(temp);

                                if(accounts.data[i].id != window.Laravel.user.id) {
                                    temp_searched.push(temp);
                                }
                            }

                            this.searched = temp_searched;
                            
                        })
                        .catch($.noop);
                }
            }, 500);
        }
    }
    ,
    methods:{
        getFriendsData(){
            function getFriend() {
                return axiosAPIv1.get('/friends');
            }
            function getRecommend() {
                return axiosAPIv1.get('/users?takeNotFriend=4');
            }
            
            axios.all([
                 getFriend(),
                 getRecommend()
            ]).then(axios.spread(
                (
                     friends,
                     recommended
                ) => {
                this.friends = friends.data.friends;
                // this.requests = friends.data.requests;
                for(var i=0; i< friends.data.requests.length; i++){
                    if(!friends.data.requests[i].requestor)
                    {
                        this.requests.push(friends.data.requests[i]);
                    }
                }
                temp_recommended = [];
                for(var i=0; i<recommended.data.length; i++)
                {
                    temp = [];
                    temp.user = recommended.data[i];
                    temp_recommended.push(temp)
                }
                this.recommended = temp_recommended;
            }))
            .catch($.noop);
        },

        select(user) {
            this.toggleOffcanvas();

            var chat = [];
            $(".panel-chat-box").each(function(){
                chat.push($(this).attr('id'));
            });
            
            if($.inArray('user'+user.id,chat)=='-1')
                Event.fire('chat-box.show', user);

            setTimeout(function(){
              $('.js-auto-size').textareaAutoSize();
            }, 5000);
        },

        toggleOffcanvas() {
            $(this.$el).toggleClass('expanded');
        },
    },
    created() {
        this.getFriendsData();
        axios.get('/translate/friends')
            .then(language => {
                this.lang = language.data.data;
            });
    },
   
});

