$(document).ready(function(){
    ctx = $('#table_chart');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [
                        "January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                    ],
            datasets: [{
                label: 'All Stores',
                data: [12, 19, 3, 5, 2, 3, 24, 23, 12, 6, 34, 10],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                ],
                borderWidth: 3
            },
            {
                label: 'Store 1',
                data: [1, 9, 13, 25, 32, 21, 4, 2, 2, 16, 4, 10],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            },
            {
                label: 'Store 2',
                data: [0, 3, 1, 5, 2, 33, 41, 10, 2, 16, 10, 12],
                backgroundColor: [
                    'rgba(75, 0, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(75, 0, 192, 1)',
                ],
                borderWidth: 1
            }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            },
            responsive:true,
            maintainAspectRatio: true
        }
    });
})


Vue.component('store-panel',  require('../components/reports/sales/storePanel.vue'));


Vue.filter('currency', function (value) {
    return numberWithCommas(parseFloat(value).toFixed(2));
});

let rates = window.Laravel.rates;
let pp = null;
var vm = new Vue({
    el:'#sales_report_container',
    data:{
        listStore:[],
        listProd:[],
        listProd2:[],
        totalP:0,
        totalsub:0,
        totaldiscount:0,
        priceAfterDiscount:0,
        details:[],
        orderProducts: [],
        serviceType:[
             'Drop Off'
            ,'Pick Up'
        ],
        updateRemarks: new Form({
             id     : ""
            ,seller_notes: ""
        },{ baseURL: '/user/sales-report'}),
        optionPickUp: false,
        optionDrop: false,
        selectedOrder: null,
        usable: null,
        fee: null,
        transitForm: new Form({
            shipment_type: null,
            courier: null,
            tracking_number: null,
        }),
        cancelForm: new Form({
            reason: null
        }),
        hasDiscount: false,
        lang:[],
        price_per_item:0,
        discountedAmount:0,
        now:0,
        shipping:0,
        weight:'',
        param:'',
        idProds:[],
        owner:'',
        totaldiscountedprice:0,
        watafcost:0,
        twogocost:0
    },

    components: {
        Multiselect: window.VueMultiselect.default
    },
    methods:{
        getUsableEwalletBalance(){
            if(Laravel.user){
                axiosAPIv1
                    .get('/getUsable')
                    .then(result => {  this.usable = parseFloat(result.data.data.usable)});
            }
        },
        showDetails(order_id,store_id){
          //alert(store_id);
            this.details = "";
            axios.get('/user/sales-report/detailsByStore/'+order_id+'/'+store_id)
              .then(result1 => {
                var det_id = parseFloat(result1.data);
            axios.get('/user/sales-report/details/'+det_id)
              .then(result => {
                this.orderProducts = result.data.order.details;

                axios.get('/user/sales-report/getProductsFromSameStore/'+result.data.product_id+'/'+result.data.order_id)
                  .then(prods => {
                    this.listProd = prods;
                    this.totalShippingFee;
                });

                axios.get('/user/sales-report/getProductIdsFromSameStore/'+result.data.product_id+'/'+result.data.order_id)
                  .then(prods => {
                    this.idProds = prods.data;
                });

                this.details = result.data;
                this.owner = window.Laravel.user.full_name;
                this.updateRemarks.seller_notes = result.data.seller_notes;

                //Price Per Item
                if(this.details.fee_included==1)
                    this.price_per_item = parseFloat(this.details.price_per_item) + parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat);
                else
                    this.price_per_item = this.details.price_per_item;

                //Dicounted Amount
                if(this.details.discount==0)
                    var price = this.details.price_per_item;
                else 
                    var price = this.details.sale_price;
                
                if(this.details.fee_included==1)
                    this.now = parseFloat(price) + parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat);
                else
                    this.now = price;

                this.discountedAmount = this.now * (this.details.discount/100);

                //Shipping Fee
                if(this.details.fee_included!=1){
                    var total = parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat);
                    this.shipping = total.toFixed(2);
                } else 
                    this.shipping = 0.00;

                //Weight
                var DW = (parseFloat(this.details.product_length) * parseFloat(this.details.product_width) * parseFloat(this.details.product_height))/3500;
                if(this.details.product_weight>DW)
                    this.weight = this.details.product_weight;
                if(DW>this.details.product_weight)
                    this.weight  = DW;

                if(result.data.discount != 0){
                    this.hasDiscount = true;
                }else{
                    this.hasDiscount = false;
                }
                $('#orderProducts').modal('show');
            });
          });

        },
        getDiscounted1(order_id,store_id) {
            // var product_id = null;
            // var order_id = null;
            var discPrice = null;
            var sFee = null;
            var twoGoFee = null;
            // $.ajax({
            //     url: '/user/sales-report/details/' + id,
            //     //url: './console',
            //     type: 'GET',
            //     dataType: 'json',
            //     async: false,
            //     data: {
            //         _token: window.Laravel.csrfToken,
            //     },
            //     beforeSend: function() {
            //         //$( "#console" ).html($( "#console" ).html()+"\n> Computing Client's Age");
            //     },
            //     success:function(result){
            //         //console.log(result.product_id);
            //         product_id = result.product_id;
            //         order_id = result.order_id;
                    $.ajax({
                        url: '/user/sales-report/getProductsFromSameStore2/' + order_id + '/' + store_id,
                        //url: './console',
                        type: 'GET',
                        dataType: 'json',
                        async: false,
                        data: {
                            _token: window.Laravel.csrfToken,
                        },
                        beforeSend: function() {
                            //$( "#console" ).html($( "#console" ).html()+"\n> Computing Client's Age");
                        },
                        success:function(prods){
                            var groupArray = require('group-array');
                            var group = groupArray(prods, 'product.store.id');
                            //console.log(group);
                            var weight = 0;
                            var volume = 0;
                            var totalChargeWeight = 0;
                            var productPrice = 0;
                            var salePrice = 0;
                            var totalWeight = 0;
                            var chargeWeight = 0;
                            var total = 0;
                            var totalSP = 0;
                            var discount = 0;
                            var totaldiscount = 0;
                            var t = 0;
                            var disc = [];
                            var vm = this;
                            $.each(group, function(key, value) {
                             // console.log("Loop:"+value.length);
                             if (value.length > 0) {
                              //console.log(value);
                              totalChargeWeight = 0;
                              productPrice = 0;
                              salePrice = 0;
                              var totalSalePrice = 0;
                              discount = 0;
                              for (var i = 0; i < value.length; i++) {
                               //console.log(value[i].product.weight);
                               if(value[i].status != "Cancelled"){
                                 weight = value[i].product.weight;
                                 volume = (value[i].product.length * value[i].product.width * value[i].product.height) / 3500;
                                 if (weight > volume) chargeWeight = weight;
                                 if (volume > weight) chargeWeight = volume;
                                 totalChargeWeight += (parseFloat(chargeWeight) * parseFloat(value[i].quantity));
                                 productPrice += ((value[i].sale_price > 0 ? parseFloat(value[i].sale_price) : parseFloat(value[i].price_per_item)) * parseFloat(value[i].quantity));
                                 var sp = value[i].subtotal;
                                 salePrice += (parseFloat(sp.replace(',', '')));
                               }
                              }

                              //console.log("total Price :"+productPrice);
                              totalWeight = totalChargeWeight * 1.2; //add 20% to total weight
                              //console.log("twt :"+totalWeight);
                              var rateCat = "SAMM";
                              var halfkg = 0;
                              var fkg = 0;
                              var excess = 0;
                              var rate = 0;
                              // rate computation
                              //console.log(rates);
                              $.each(rates, function(i, item) {
                               if (rates[i].rate_category == rateCat) {
                                halfkg = rates[i].halfkg;
                                fkg = rates[i].fkg;
                                excess = rates[i].excess;
                               }
                              });

                              if (totalWeight <= 0.5) {
                               rate = halfkg;
                              } else if (totalWeight > 0.5 && totalWeight <= 1) {
                               rate = fkg;
                              } else {
                               totalWeight -= 1;
                               rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
                              }
                              //console.log('rate b4 '+rate);
                              rate = parseFloat(rate) + 20; // add 20 for markup
                              //console.log("rate -> "+Math.ceil(rate));

                              //valuation charge computation
                              var vc = productPrice * 0.01;
                              //console.log("vc -> "+Math.ceil(vc));

                              //tax computation - docu stamp
                              var vtx = 0;
                              if (productPrice > 0 && productPrice <= 1000) {
                               vtx = ((rate - 1) + vc) * .12;
                              } else {
                               vtx = ((rate - 10) + vc) * .12;
                              }
                              //console.log("tax -> "+Math.ceil(vtx));

                              //WATAF Cost
                              sFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);

                              //console.log("total fee :"+sFee);

                              total = productPrice + sFee;
                              t += total;

                              totalSalePrice += salePrice;
                              totalSP += salePrice;
                              discount = totalSalePrice - total;
                              totaldiscount += discount;
                              //console.log('discount-'+discount);
                              discPrice = totalSP - totaldiscount;
                             }

                            });
                        },
                        complete:function(){}
                    });
                // },
                // complete:function(){}
            // });
            return discPrice;
},

        getDiscounted2(order_id,store_id) {
            //var product_id = null;
            //var order_id = null;
            var discPrice = null;
            var sFee = null;
            var twoGoFee = null;
            // $.ajax({
            //     url: '/user/sales-report/details/' + id,
            //     //url: './console',
            //     type: 'GET',
            //     dataType: 'json',
            //     async: false,
            //     data: {
            //         _token: window.Laravel.csrfToken,
            //     },
            //     beforeSend: function() {
            //         //$( "#console" ).html($( "#console" ).html()+"\n> Computing Client's Age");
            //     },
            //     success:function(result){
            //         //console.log(result.product_id);
            //         product_id = result.product_id;
            //         order_id = result.order_id;
                    $.ajax({
                        url: '/user/sales-report/getProductsFromSameStore2/' + order_id + '/' + store_id,
                        //url: './console',
                        type: 'GET',
                        dataType: 'json',
                        async: false,
                        data: {
                            _token: window.Laravel.csrfToken,
                        },
                        beforeSend: function() {
                            //$( "#console" ).html($( "#console" ).html()+"\n> Computing Client's Age");
                        },
                        success:function(prods){
                            var groupArray = require('group-array');
                            var group = groupArray(prods, 'product.store.id');
                            //console.log(group);
                            var weight = 0;
                            var volume = 0;
                            var totalChargeWeight = 0;
                            var productPrice = 0;
                            var salePrice = 0;
                            var totalWeight = 0;
                            var chargeWeight = 0;
                            var total = 0;
                            var totalSP = 0;
                            var discount = 0;
                            var totaldiscount = 0;
                            var t = 0;
                            var disc = [];
                            var vm = this;
                            $.each(group, function(key, value) {
                             // console.log("Loop:"+value.length);
                             if (value.length > 0) {
                              //console.log(value);
                              totalChargeWeight = 0;
                              productPrice = 0;
                              salePrice = 0;
                              var totalSalePrice = 0;
                              discount = 0;
                              for (var i = 0; i < value.length; i++) {
                               //console.log(value[i].product.weight);
                               if(value[i].status != "Cancelled"){
                                 weight = value[i].product.weight;
                                 volume = (value[i].product.length * value[i].product.width * value[i].product.height) / 3500;
                                 if (weight > volume) chargeWeight = weight;
                                 if (volume > weight) chargeWeight = volume;
                                 totalChargeWeight += (parseFloat(chargeWeight) * parseFloat(value[i].quantity));
                                 productPrice += ((value[i].sale_price > 0 ? parseFloat(value[i].sale_price) : parseFloat(value[i].price_per_item)) * parseFloat(value[i].quantity));
                                 var sp = value[i].subtotal;
                                 salePrice += (parseFloat(sp.replace(',', '')));
                               }
                              }

                              totalWeight = totalChargeWeight; //add 20% to total weight
                              //console.log("twt :"+totalWeight);
                              var rateCat = "SAMM";
                              var halfkg = 0;
                              var fkg = 0;
                              var excess = 0;
                              var rate = 0;
                              // rate computation
                              //console.log(rates);
                              $.each(rates, function(i, item) {
                               if (rates[i].rate_category == rateCat) {
                                halfkg = rates[i].halfkg;
                                fkg = rates[i].fkg;
                                excess = rates[i].excess;
                               }
                              });

                              if (totalWeight <= 0.5) {
                               rate = halfkg;
                              } else if (totalWeight > 0.5 && totalWeight <= 1) {
                               rate = fkg;
                              } else {
                               totalWeight -= 1;
                               rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
                              }
                              //console.log('rate b4 '+rate);
                              rate = parseFloat(rate);
                              //console.log("rate -> "+Math.ceil(rate));

                              //valuation charge computation
                              var vc = productPrice * 0.01;
                              //console.log("vc -> "+Math.ceil(vc));

                              //tax computation - docu stamp
                              var vtx = 0;
                              if (productPrice > 0 && productPrice <= 1000) {
                               vtx = ((rate - 1) + vc) * .12;
                              } else {
                               vtx = ((rate - 10) + vc) * .12;
                              }
                              //console.log("tax -> "+Math.ceil(vtx));

                              //2GO Cost
                              twoGoFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);



                              //console.log("total Price :"+productPrice);
                              totalWeight = totalChargeWeight * 1.2; //add 20% to total weight
                              //console.log("twt :"+totalWeight);
                              var rateCat = "SAMM";
                              var halfkg = 0;
                              var fkg = 0;
                              var excess = 0;
                              var rate = 0;
                              // rate computation
                              //console.log(rates);
                              $.each(rates, function(i, item) {
                               if (rates[i].rate_category == rateCat) {
                                halfkg = rates[i].halfkg;
                                fkg = rates[i].fkg;
                                excess = rates[i].excess;
                               }
                              });

                              if (totalWeight <= 0.5) {
                               rate = halfkg;
                              } else if (totalWeight > 0.5 && totalWeight <= 1) {
                               rate = fkg;
                              } else {
                               totalWeight -= 1;
                               rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
                              }
                              //console.log('rate b4 '+rate);
                              rate = parseFloat(rate) + 20; // add 20 for markup
                              //console.log("rate -> "+Math.ceil(rate));

                              //valuation charge computation
                              var vc = productPrice * 0.01;
                              //console.log("vc -> "+Math.ceil(vc));

                              //tax computation - docu stamp
                              var vtx = 0;
                              if (productPrice > 0 && productPrice <= 1000) {
                               vtx = ((rate - 1) + vc) * .12;
                              } else {
                               vtx = ((rate - 10) + vc) * .12;
                              }
                              //console.log("tax -> "+Math.ceil(vtx));

                              //WATAF Cost
                              sFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);

                              //console.log("total fee :"+sFee);

                              total = productPrice + sFee;
                              t += total;

                              totalSalePrice += salePrice;
                              totalSP += salePrice;
                              discount = totalSalePrice - total;
                              totaldiscount += discount;
                              //console.log('discount-'+discount);
                              discPrice = totalSP - totaldiscount;
                             }

                            });
                        },
                        complete:function(){}
                    });
            //     },
            //     complete:function(){}
            // });
            return discPrice + '-' + sFee + '-' + twoGoFee;
},

        showAirwayBill(id){
            this.details = '';
            axios.get('/user/sales-report/airway/'+id)
              .then(result => {
                this.details = result.data;

                //Price Per Item
                if(this.details.fee_included==1)
                    this.price_per_item = parseFloat(this.details.price_per_item) + parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat);
                else
                    this.price_per_item = this.details.price_per_item;

                //Dicounted Amount
                if(this.details.discount==0)
                    var price = this.details.price_per_item;
                else 
                    var price = this.details.sale_price;
                
                if(this.details.fee_included==1)
                    this.now = parseFloat(price) + parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat);
                else
                    this.now = price;

                this.discountedAmount = this.now * (this.details.discount/100);

                //Shipping Fee
                if(this.details.fee_included!=1){
                    var total = parseFloat(this.details.shipping_fee) + parseFloat(this.details.charge) + parseFloat(this.details.vat);
                    this.shipping = total.toFixed(2);
                } else 
                    this.shipping = 0.00;

                //Weight
                var DW = (parseFloat(this.details.length) * parseFloat(this.details.width) * parseFloat(this.details.height))/3500;
                if(this.details.weight>DW)
                    this.weight = this.details.weight;
                if(DW>this.details.weight)
                    this.weight  = DW;

                if(result.data.discount != 0){
                    this.hasDiscount = true;
                }else{
                    this.hasDiscount = false;
                }
            });
            $('#viewAirwayBill').modal('toggle');
        },
        submitNote:function (id){
            this.updateRemarks.id = id;
            this.updateRemarks.submit('post', '/update-remarks')
                .then(result => {
                    toastr.success(this.lang.data['updated-note-msg']);

                })
                .catch(error => {
                    toastr.error(this.lang.data['update-failed-msg']);
                });

        },
        getStores(){
            axios.get('/user/sales-report/get-store')
              .then(result => {
                this.listStore = result.data.stores
                //console.log(this.listStore);
                //console.log(this.totalPriceComputation);
            });
        },
        changeOption(value){
           if(value == "Pick Up"){
                this.optionPickUp = true;
                this.optionDrop = false;
           }else if(value == "Drop Off"){
                this.optionDrop = true;
                this.optionPickUp = false;
           }
        },
        submitTrackingNumber() {
            let errors = {};

            if (! this.transitForm.shipment_type || ! this.transitForm.courier || ! this.transitForm.tracking_number) {
                (! this.transitForm.courier) ? errors['courier'] = true : void(0);
                (! this.transitForm.shipment_type) ? errors['shipment_type'] = true : void(0);
                (! this.transitForm.tracking_number) ? errors['tracking_number'] = true : void(0);

                this.transitForm.errors.record(errors);
                this.removeHasLoading();

                return;
            }

            axios.post('/user/sales-report/update-to-transit', {
                id: this.transitForm.id,
                status: this.transitForm.status,
                shipment_type: this.transitForm.shipment_type,
                courier: this.transitForm.courier,
                tracking_number: this.transitForm.tracking_number,
                totaldiscountedprice: this.totaldiscountedprice,
                watafcost: this.watafcost,
                twogocost: this.twogocost
            }).then(result => {
                toastr.success(this.lang.data['saved-msg']);

                this.selectedOrder.original_status = this.selectedOrder.status;
                this.selectedOrder.shipment_type = this.transitForm.shipment_type;
                this.selectedOrder.tracking_number = this.transitForm.tracking_number;
                this.transitForm.submitted = true;

                this.removeHasLoading();

                this.getStores();

                $('#inTransitModal').modal('hide');
            })
            .catch(error => {
                toastr.error(this.lang.data['update-failed-msg']);

                this.removeHasLoading();
            });
        },

        submit2Go() {
            let enoughBalance = this.usable - this.fee;
            if(enoughBalance >= 0){
                // var parcel2go = {
                //     d : {},
                // };
                // var data2 = {
                //     "id_": "WATAF",
                //     "token_": "9Hs2JL21z",
                //     "account_no": "9602032400",
                //     "bkg_request": false,
                //     "awb_code": "W1000198B",
                //     "origin_port": "MNL",
                //     "paymode ": "CS",
                //     "cod_amt": 0,
                //     "instruction": "Near train station",
                //     "attachment": "MDS-132131232",
                //     cnee_dtls: {},
                //     px_dtls: [],
                // };  

                // var dtls = {
                //     "name": "JUAN DELA CRUZ",
                //     "street": "L10 B20 PUTATAN VILLAGE",
                //     "barangay": "PUTATAN",
                //     "city": "MUNTINLUPA CITY",
                //     "area_code": "1958",
                //     "mobile_number": "09068946526",
                //     "email_address": "jeffrey.gatpandan656@gmail.com"
                // }
                // data2.cnee_dtls = dtls;

                // data2.px_dtls.push({ 
                //     "item_qty": 1,
                //     "pkg_code": "BOX",
                //     "actual_wt": 1,
                //     "length": 5,
                //     "width": 5,
                //     "height": 5,
                //     "declared_value": 1000,
                //     "item_description": "Cellular Phone Unit"
                // });
                // parcel2go.d = data2;
                
                // var jsontest = JSON.stringify(parcel2go);
                // console.log(jsontest);

                // var config = {
                //   headers: {
                //     'Access-Control-Allow-Origin': '*',
                //     'Content-Type': 'application/json;charset=UTF-8',
                //     'Accept': 'application/json',
                //     }
                // };
                // axios.post('https://expressapps.2go.com.ph/2gowebdev/model/API/shipment_request.asmx/create', {
                //     d: jsontest
                // },
                // config 
                // ).then(result => {

                //     console.log(result);
                // })
                // .catch(error => {
                // });

                axios.post('/user/sales-report/update-to-transit', {
                    id: this.transitForm.id,
                    order_id: this.transitForm.order_id,
                    product_id: this.transitForm.product_id,
                    security_deposit: this.totalShippingFee,
                    fee: this.fee,
                    status: this.transitForm.status,
                    shipment_type: 'Pick Up',
                    courier: '2GO',
                    tracking_number: null,
                    totaldiscountedprice: this.totaldiscountedprice,
                    watafcost: this.watafcost,
                    twogocost: this.twogocost
                }).then(result => {

                    toastr.success(this.lang.data['saved-msg']);

                    this.selectedOrder.original_status = this.selectedOrder.status;
                    this.selectedOrder.shipment_type = this.transitForm.shipment_type;
                    this.selectedOrder.tracking_number = result.tracking_number;
                    this.transitForm.submitted = true;

                    this.removeHasLoading();

                    this.getStores();

                    $('#inTransitModal2').modal('hide');
                })
                .catch(error => {
                    toastr.error(this.lang.data['update-failed-msg']);

                    this.removeHasLoading();
                });
            }
            else{
                $('#inTransitModal2').modal('hide');
                swal(
                  'Oops...',
                  'Your e-wallet usable balance is not enough for this transaction to proceed! '+'Check your e-wallet <a href="/user/ewallet">here.</a> ',
                  'warning'
                );
            
            }
        },

        inTransit(order_id,store_id,total) {
            //this.selectedOrder = data;

            axios.get('/user/sales-report/wholedetailsByStore/'+order_id+'/'+store_id)
              .then(result1 => {
                this.selectedOrder = result1.data;
              
            var arr = total.split("-");
            this.totaldiscountedprice = arr[0];
            this.watafcost = arr[1];
            this.twogocost = arr[2];

            axios.get('/user/sales-report/getProductsFromSameStore/'+result1.data.product_id+'/'+order_id)
              .then(result => {
                this.listProd = result;
            });
            //alert(data.order_id);
            //var fee = parseFloat(data.shipping_fee) + parseFloat(data.vat) + parseFloat(data.charge);
            //this.fee = fee.toFixed(2);
            this.transitForm = new Form(result1.data);

            $('#inTransitModal2').modal('show');
          });
        },

        cancelled(data) {
            this.selectedOrder = data;
            this.cancelForm = new Form(data);

            $('#cancelModal').modal('show');
        },

        submitReason() {
            let errors = {};

            if (! this.cancelForm.reason) {
                (! this.cancelForm.reason) ? errors['reason'] = true : void(0);

                this.cancelForm.errors.record(errors);
                this.removeHasLoadingFromCancelForm();

                return;
            }

            axios.post('/user/sales-report/cancel-order', {
                id: this.cancelForm.id,
                status: this.cancelForm.status,
                reason: this.cancelForm.reason,
            }).then(result => {
                toastr.success(this.lang.data['saved-msg']);

                this.selectedOrder.original_status = this.selectedOrder.status;
                this.selectedOrder.reason = this.cancelForm.reason;
                this.cancelForm.submitted = true;

                this.removeHasLoadingFromCancelForm();

                this.getStores();

                $('#cancelModal').modal('hide');
            })
            .catch(error => {
                toastr.error(this.lang.data['update-failed-msg']);

                this.removeHasLoadingFromCancelForm();
            });
        },

        removeHasLoading() {
            setTimeout(() => {
                let $btn = $('.btn-transit-submit');
                let $content = $btn.find('.btn-content');
                let $loader = $btn.find('.loader');

                $loader.fadeOut(100, function () {
                    $content.fadeIn(100);
                });
            }, 300);
        },

        removeHasLoadingFromCancelForm() {
            setTimeout(() => {
                let $btn = $('.btn-cancel-form-submit');
                let $content = $btn.find('.btn-content');
                let $loader = $btn.find('.loader');

                $loader.fadeOut(100, function () {
                    $content.fadeIn(100);
                });
            }, 300);
        },

        print(){
            var html_container = '';
            html_container = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns:fb="http://ogp.me/ns/fb#"><head>';
            html_container += '<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons" />';
            html_container += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>';
            html_container += '<style>.airway-product-modal .modal-content{border:none;font-size:11px}.airway-product-modal .modal-content .modal-body{overflow-y:auto;padding:24px 6px!important}.airway-product-modal .modal-content .modal-body .deliver{font-family:Cambria;font-size:15px;font-weight:700}.airway-product-modal .modal-content .modal-body .address{width:400px}.airway-product-modal .modal-content .modal-body table,.airway-product-modal .modal-content .modal-body td,.airway-product-modal .modal-content .modal-body th{border:1px solid #000}.airway-product-modal .modal-content .modal-body table{border-collapse:collapse;width:100%}.airway-product-modal .modal-content .modal-body th{text-align:left;font-weight:700}.airway-product-modal .modal-content .modal-body .awb{float:right;margin-right:20px;margin-top:-120px}.airway-product-modal .modal-content .modal-body .received{float:left;margin-left:15px;margin-top:5px}.airway-product-modal .modal-content .modal-body .attempt{float:right;margin-right:20px;margin-top:5px}.airway-product-modal .modal-content .modal-body .text-align{text-align:center}.airway-product-modal .modal-content .modal-body .padding-left{padding-left:10px}.airway-product-modal .modal-content .modal-body .border-top{border-top:none}.airway-product-modal .modal-content .modal-body .b{font-weight:700}.airway-product-modal .modal-content .modal-body .margin{margin-top:15px}.airway-product-modal .modal-content .modal-body .float{float:right}.airway-product-modal .modal-content .modal-body .inclusive{font-weight:700;font-size:9px;margin-right:10px}.airway-product-modal .modal-content .modal-body hr{border-style:solid;border-color:#000;margin-top:10px;width:300px}.airway-product-modal .modal-content .modal-footer{padding:5px}</style>';
            html_container += '</head><body><div class="airway-product-modal"><div class="modal-content"><div class="modal-body">';

            var body = $("#print").html();
            var myWindow=window.open('','','width=500,height=500,scrollbars=yes,location=no');
            myWindow.document.write(html_container);
            myWindow.document.write(body);
            myWindow.document.write("</div></div></div>");
            myWindow.document.write('<div class="airway-product-modal"><div class="modal-content"><div class="modal-body">');
            myWindow.document.write(body);
            myWindow.document.write("</div></div></div></body></html>");
            myWindow.document.close();
            
            setTimeout(function(){
            myWindow.focus();
            myWindow.print();       
            }, 3000);
        },

        printSales(){
          var html_container = '';
          html_container = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns:fb="http://ogp.me/ns/fb#"><head>';
          html_container += '<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons" />';
          html_container += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>';  
          html_container += '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"><meta name="apple-mobile-web-app-capable" content="yes">';
          html_container += '<style>.sales-report-modal .title {font-size: 22px;line-height: 1;margin: 0 4px 10px;padding: 0;border-bottom: 3px solid #80cbc4;} hr {border-color: #009688;border-style: dotted;margin: 0;} .payment-info .col-sm-6 {text-align: right;} .label {border-color: #fff !important; color: #fff !important; font-size: 12px;font-weight: 400;line-height: 1.25;display: inline-block;height: 20px;border-radius: 0;} @media print {.reasonCont {display:none} .label-success {background-color: #4caf50 !important;} .label-default {background-color: #9e9e9e !important;} .label-warning {background-color: #ff5722 !important;} .label-primary {background-color: #009688 !important;} .label-info {background-color: #03a9f4 !important;}}</style>';    
          html_container += '</head><body style="-webkit-print-color-adjust: exact !important;"><div class="sales-report-modal">';

          var body = $("#printSales").html();
          var myWindow = window.open('','','width=500,height=500,scrollbars=yes,location=no');
          myWindow.document.write(html_container);
          myWindow.document.write(body);
          myWindow.document.write("</div></body></html>");
          myWindow.document.close();
          
          setTimeout(function(){
          myWindow.focus();
          myWindow.print();
          }, 3000);
        },

        showVideoEvidenceModal(e, index, modal) {
            e.preventDefault();

            $('#' + modal).modal('hide');

            setTimeout(function() {
                $('a#video-evidence-'+index).ekkoLightbox({ 
                    wrapping: false, 
                    alwaysShowClose: true,
                    onHidden: function(direction, itemIndex) {
                        $('#' + modal).modal('show');
                    } 
                });
            }, 1000);
            
        },

        receivedBySeller(id){
            swal({
                title: this.lang.data['confirm'],
                html:  this.lang.data['received-returned-msg-1'],
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: this.lang.data['yes']
            }).then(() => {
                axiosAPIv1.post('order-details/receive-returned-item' , {
                    //make api to return the money to the buyer and stocks and the current date for the returned_received_date column
                    id:id
                } )
                .then(result => {
                    toastr.success(this.lang.data['saved-msg']);
                    this.getStores();
                })
                .catch(error => {
                    toastr.error(this.lang.data['update-failed-msg']);
                });
            });
        }
    },

    created() {
        this.getStores();
        this.getUsableEwalletBalance();
        Event.listen('order.in-transit', data => this.inTransit(data));
        Event.listen('order.cancelled', data => this.cancelled(data));

        axios.get('/translate/sales-report')
            .then(result => {
            this.lang = result.data;
        });

        this.param = location.search.split('storeid=')[1];

        if(this.param == '' || this.param == undefined)
            this.param = 0;
        else
            this.param;
    },

    computed: {
        totalShippingFee() { 
            var groupArray = require('group-array');
            var group = groupArray(this.listProd.data, 'product.store.id');
            //console.log(group);
            var weight = 0;
            var volume = 0;
            var charge = 0;
            var totalChargeWeight = 0;
            var productPrice = 0;
            var salePrice = 0;
            var totalWeight = 0;
            var chargeWeight = 0;
            var total = 0;
            var totalSP = 0;
            var discount = 0;
            var totaldiscount = 0;
            var t = 0;
            var disc = [];
            var vm = this;
            $.each( group, function( key, value ) {
                //console.log("Loop:"+value.length);
                if(value.length>0){
                    //console.log(value);
                    totalChargeWeight = 0;
                    productPrice = 0;
                    salePrice = 0;
                    var totalSalePrice = 0;
                    discount = 0;
                    for(var i=0; i<value.length; i++){
                        //console.log(value[i].product.weight);
                      if(value[i].status != "Cancelled"){
                        weight = value[i].product.weight;
                        volume = (value[i].product.length*value[i].product.width*value[i].product.height)/3500;
                        charge += parseFloat(value[i].charge);
                        if(weight>volume) chargeWeight = weight;
                        if(volume>weight) chargeWeight = volume;
                        totalChargeWeight+= (parseFloat(chargeWeight)*parseFloat(value[i].quantity));
                        productPrice += ((value[i].sale_price > 0 ? parseFloat(value[i].sale_price) : parseFloat(value[i].price_per_item))*parseFloat(value[i].quantity));
                        var sp = value[i].subtotal;
                        salePrice += (parseFloat(sp.replace(',', '')));
                      }
                    }
                    //console.log("total Price :"+productPrice);
                    totalWeight = totalChargeWeight * 1.2; //add 20% to total weight
                    //console.log("twt :"+totalWeight);
                    let rateCat = "SAMM";
                    let halfkg = 0;
                    let fkg = 0;
                    let excess = 0;
                    let rate = 0;
                    // rate computation
                    //console.log(rates);
                    $.each(rates, function(i, item) {
                      if(rates[i].rate_category == rateCat){
                        halfkg = rates[i].halfkg;
                        fkg = rates[i].fkg;
                        excess = rates[i].excess;
                      }
                    });

                    if(totalWeight <= 0.5){
                      rate = halfkg;
                    }
                    else if(totalWeight >0.5 && totalWeight <=1){
                      rate = fkg;
                    }
                    else{
                      totalWeight -=1;
                      rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
                    }
                    //console.log('rate b4 '+rate);
                    rate = parseFloat(rate) + 20; // add 20 for markup
                    //console.log("rate -> "+Math.ceil(rate));

                    //valuation charge computation
                    let vc = (charge > 0 ? productPrice * 0.01 : 0); 
                    //console.log("vc -> "+Math.ceil(vc));

                    //tax computation - docu stamp
                    let vtx = 0;
                    if(productPrice > 0 && productPrice <=1000){
                        vtx = ((rate -1)+vc) * .12;
                      }
                    else{
                        vtx = ((rate -10)+vc) * .12;
                    }
                    //console.log("tax -> "+Math.ceil(vtx));

                    //total fee
                    let sFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);        

                    //console.log("total fee :"+sFee);

                    total = productPrice + sFee;
                    t += total;

                    totalSalePrice += salePrice;
                    totalSP += salePrice;
                    discount = totalSalePrice - total;
                    totaldiscount += discount;
                    //console.log('discount-'+discount);
                    
                } 

            });

            this.totalsub = totalSP;
            this.totaldiscount = totaldiscount;
            this.priceAfterDiscount = totalSP - totaldiscount;

            let FinalFee = t;
            return this.priceAfterDiscount - productPrice ;
        },
    },

    mounted() {
        $('#inTransitModal2').on('hidden.bs.modal', e => {
            if (! this.transitForm.submitted) {
                // this.selectedOrder.status = this.selectedOrder.original_status;
            }
        });

        $('#cancelModal').on('hidden.bs.modal', e => {
            if (! this.cancelForm.submitted) {
                // this.selectedOrder.status = this.selectedOrder.original_status;
            }
        });
    }
})