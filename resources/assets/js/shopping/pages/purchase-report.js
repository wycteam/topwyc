import Dropzone from 'vue2-dropzone'

Vue.filter('round', function (value) {
    return  roundOff(value);
});

Vue.filter('currency', function (value) {
    return  numberWithCommas(parseFloat(value).toFixed(2));
});

let rates = window.Laravel.rates;

var vm = new Vue ({
    el: '#purchase_report_container',
    data:{
        ordersList: [],
        orderDetails:[],
        orderedProduct:[],
        selectedOrder: {},
        returnItemForm: new Form(),
        dz: '',
        headers: {
            'X-CSRF-TOKEN': window.Laravel.csrfToken,
        },
        hasDiscount: false,
        issueform: new Form({
            subject: null,
            body: null,
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        newConvoForm: new Form({
            supportIds: [],
            flag: true
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        notif : [],
        issue_id: null,
        lang:[],
        lang2:[],
        langmode:null,
        ewallet_balance:0,
        price_per_item:0,
        discountedAmount:0,
        now:0,
        shipping:0,
        weight:'',

        orderProducts: [],
        listProd:[],
        totalsub:0,
        totaldiscount:0,
        priceAfterDiscount:0,

        dz2: '',

        my_orders_list: []

    },
    components: {
        'orderList': require('../components/reports/purchase/Order.vue'),
        'product': require('../components/reports/purchase/Product.vue'),
        Dropzone
    },

    created(){
        var m = $("meta[name=locale-lang]");    
        this.langmode = m.attr("content");

        this.getOrders();

        Event.listen('order.mark-as-returned', this.showReturnItemModal);

        axios.get('/user/purchase-report/recent-products')
            .then(result => {
            this.orderedProduct = result.data;
        });

        axios.get('/translate/purchase-report')
            .then(result => {
            this.lang = result.data;
            this.lang2 = result.data.data;
        });

        axiosAPIv1.get('/getUsable')
            .then(result => {  
            this.ewallet_balance = parseFloat(result.data.data.usable)
        });

        axiosAPIv1.get('/getSupports')
            .then(result => {
            this.supports = result.data;                  
        });
    },

    mounted() {
        $('#returnItemModal').on('show.bs.modal', e => {
            this.dz = 'dropzone';

            this.$nextTick(() => {
                this.$refs.returnedItemFiles.$on('vdropzone-removedFile', this.dropzoneFileRemoved.bind(this));
                this.$refs.returnedItemFiles.dropzone.options.headers = this.headers;
                this.$refs.returnedItemFiles.dropzone.options.autoProcessQueue = false;
                this.$refs.returnedItemFiles.dropzone.options.parallelUploads = 20;
            });

            $('#returnItemModal a.btn-return-item-submit').removeClass('disabled');
        });

        $('#returnItemModal').on('hidden.bs.modal', e => {
            this.dz = '';
            this.selectedOrder = {};
            this.returnItemForm = new Form();
        });
    },


    methods:{
        showDetails(id){
            this.orderDetails = '';
            axios.get('/user/purchase-report/get-orders/'+id)
              .then(result => {
                this.orderDetails = result.data;

                this.orderProducts = result.data.order.details;

                axios.get('/user/sales-report/checkIfSameStore/'+result.data.product_id+'/'+result.data.order_id)
                  .then(prods => {
                    this.listProd = prods;
                    this.totalShippingFee;
                });

                //Price Per Item
                if(this.orderDetails.fee_included==1)
                    this.price_per_item = parseFloat(this.orderDetails.price_per_item) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
                else
                    this.price_per_item = this.orderDetails.price_per_item;

                //Dicounted Amount
                if(this.orderDetails.discount==0)
                    var price = this.orderDetails.price_per_item;
                else 
                    var price = this.orderDetails.sale_price;
                
                if(this.orderDetails.fee_included==1)
                    this.now = parseFloat(price) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
                else
                    this.now = price;

                this.discountedAmount = this.now * (this.orderDetails.discount/100);

                //Shipping Fee
                if(this.orderDetails.fee_included!=1){
                    var total = parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
                    this.shipping = total.toFixed(2);
                } else 
                    this.shipping = 0.00;

                //Weight
                var DW = (parseFloat(this.orderDetails.product_length) * parseFloat(this.orderDetails.product_width) * parseFloat(this.orderDetails.product_height))/3500;
                if(this.orderDetails.product_weight>DW)
                    this.weight = this.orderDetails.product_weight;
                if(DW>this.orderDetails.product_weight)
                    this.weight  = DW;

                if(result.data.discount != 0){
                    this.hasDiscount = true;
                }else{
                    this.hasDiscount = false;
                }
            });
            $('#viewProduct').modal('toggle');
        },

        getDiscounted(id) {
            var product_id = null;
            var order_id = null;
            var discPrice = null;
            $.ajax({
                url: '/user/purchase-report/get-orders/' + id,
                //url: './console',
                type: 'GET',
                dataType: 'json',
                async: false,
                data: {
                    _token: window.Laravel.csrfToken,
                },
                beforeSend: function() {
                    //$( "#console" ).html($( "#console" ).html()+"\n> Computing Client's Age");
                },
                success:function(result){
                    //console.log(result.product_id);
                    product_id = result.product_id;
                    order_id = result.order_id;
                    $.ajax({
                        url: '/user/sales-report/checkIfSameStore/' + product_id + '/' + order_id,
                        //url: './console',
                        type: 'GET',
                        dataType: 'json',
                        async: false,
                        data: {
                            _token: window.Laravel.csrfToken,
                        },
                        beforeSend: function() {
                            //$( "#console" ).html($( "#console" ).html()+"\n> Computing Client's Age");
                        },
                        success:function(prods){
                            var groupArray = require('group-array');
                            var group = groupArray(prods, 'product.store.id');
                            //console.log(group);
                            var weight = 0;
                            var volume = 0;
                            var charge = 0;
                            var totalChargeWeight = 0;
                            var productPrice = 0;
                            var salePrice = 0;
                            var totalWeight = 0;
                            var chargeWeight = 0;
                            var total = 0;
                            var totalSP = 0;
                            var discount = 0;
                            var totaldiscount = 0;
                            var t = 0;
                            var disc = [];
                            var vm = this;
                            $.each(group, function(key, value) {
                             // console.log("Loop:"+value.length);
                             if (value.length > 0) {
                              //console.log(value);
                              totalChargeWeight = 0;
                              productPrice = 0;
                              salePrice = 0;
                              var totalSalePrice = 0;
                              discount = 0;
                              for (var i = 0; i < value.length; i++) {
                               //console.log(value[i].product.weight);
                               if(value[i].status != "Cancelled"){
                                 weight = value[i].product.weight;
                                 volume = (value[i].product.length * value[i].product.width * value[i].product.height) / 3500;
                                 charge += parseFloat(value[i].charge);
                                 if (weight > volume) chargeWeight = weight;
                                 if (volume > weight) chargeWeight = volume;
                                 totalChargeWeight += (parseFloat(chargeWeight) * parseFloat(value[i].quantity));
                                 productPrice += ((value[i].sale_price > 0 ? parseFloat(value[i].sale_price) : parseFloat(value[i].price_per_item)) * parseFloat(value[i].quantity));
                                 var sp = value[i].subtotal;
                                 salePrice += (parseFloat(sp.replace(',', '')));
                               }
                              }
                              //console.log("total Price :"+productPrice);
                              totalWeight = totalChargeWeight * 1.2; //add 20% to total weight
                              //console.log("twt :"+totalWeight);
                              let rateCat = "SAMM";
                              let halfkg = 0;
                              let fkg = 0;
                              let excess = 0;
                              let rate = 0;
                              // rate computation
                              //console.log(rates);
                              $.each(rates, function(i, item) {
                               if (rates[i].rate_category == rateCat) {
                                halfkg = rates[i].halfkg;
                                fkg = rates[i].fkg;
                                excess = rates[i].excess;
                               }
                              });

                              if (totalWeight <= 0.5) {
                               rate = halfkg;
                              } else if (totalWeight > 0.5 && totalWeight <= 1) {
                               rate = fkg;
                              } else {
                               totalWeight -= 1;
                               rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
                              }
                              //console.log('rate b4 '+rate);
                              rate = parseFloat(rate) + 20; // add 20 for markup
                              //console.log("rate -> "+Math.ceil(rate));

                              //valuation charge computation
                              let vc = (charge > 0 ? productPrice * 0.01 : 0);
                              //console.log("vc -> "+Math.ceil(vc));

                              //tax computation - docu stamp
                              let vtx = 0;
                              if (productPrice > 0 && productPrice <= 1000) {
                               vtx = ((rate - 1) + vc) * .12;
                              } else {
                               vtx = ((rate - 10) + vc) * .12;
                              }
                              //console.log("tax -> "+Math.ceil(vtx));

                              //total fee
                              let sFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);

                              //console.log("total fee :"+sFee);

                              total = productPrice + sFee;
                              t += total;

                              totalSalePrice += salePrice;
                              totalSP += salePrice;
                              discount = totalSalePrice - total;
                              totaldiscount += discount;
                              //console.log('discount-'+discount);
                              discPrice = totalSP - totaldiscount;
                             }

                            });
                        },
                        complete:function(){}
                    });
                },
                complete:function(){}
            });
            return discPrice;
        },
        showAirwayBill(id){
            this.orderDetails = '';
            axios.get('/user/purchase-report/get-orders/'+id)
              .then(result => {
                this.orderDetails = result.data;

                //Price Per Item
                if(this.orderDetails.fee_included==1)
                    this.price_per_item = parseFloat(this.orderDetails.price_per_item) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
                else
                    this.price_per_item = this.orderDetails.price_per_item;

                //Dicounted Amount
                if(this.orderDetails.discount==0)
                    var price = this.orderDetails.price_per_item;
                else 
                    var price = this.orderDetails.sale_price;
                
                if(this.orderDetails.fee_included==1)
                    this.now = parseFloat(price) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
                else
                    this.now = price;

                this.discountedAmount = this.now * (this.orderDetails.discount/100);

                //Shipping Fee
                if(this.orderDetails.fee_included!=1){
                    var total = parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
                    this.shipping = total.toFixed(2);
                } else 
                    this.shipping = 0.00;

                //Weight
                var DW = (parseFloat(this.orderDetails.product_length) * parseFloat(this.orderDetails.product_width) * parseFloat(this.orderDetails.product_height))/3500;
                if(this.orderDetails.product_weight>DW)
                    this.weight = this.orderDetails.product_weight;
                if(DW>this.orderDetails.product_weight)
                    this.weight  = DW;
                
                if(result.data.discount != 0){
                    this.hasDiscount = true;
                }else{
                    this.hasDiscount = false;
                }
            });
            $('#viewAirwayBill').modal('toggle');
        },
        getOrders(){
            axios.get('/user/purchase-report/get-orders')
              .then(result => {
                this.my_orders_list = result.data;

                var $details = [];
                $.each(result.data, function( index, value ) {
                  $.each(result.data[index].details, function( index, value ) {
                      $details.push(value);
                   });
                });
                this.ordersList = $details;
            });
        },

        showReturnItemModal(data) {
            this.selectedOrder = data;
            this.returnItemForm = new Form($.extend(data, {submitted: false}));
            $('#returnItemModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        },

        dropzoneUploadSuccess: function (file, response) {
            //
        },

        dropzoneFileRemoved(file, error, xhr) {
            //
        },

        dropzoneUploadError(file, message, xhr) {
            //
        },

        dropzoneQueueComplete(files) {
            $('#returnItemModal').modal('hide');

            toastr.success('Successfully saved.');

            this.selectedOrder.status = 'Returned';
            this.returnItemForm.submitted = true;
            this.returnItemForm.errors.clear();
                                            
            this.removeHasLoading();
        },

        submit() {

            let errors = {};
            var fee = this.returnItemForm.shipping_fee + this.returnItemForm.vat + this.returnItemForm.charge;
            // if(this.ewallet_balance < fee){
            //     toastr.error(this.lang2['not-enough'] + ' <br> <a href="/user/ewallet">' + this.lang2['add-balance'] +'</a>');
            // } 
            // else {

                if( ! this.returnItemForm.title ) {
                    (! this.returnItemForm.title) ? errors['title'] = true : void(0);

                    this.returnItemForm.errors.record(errors);
                    this.removeHasLoading();

                    return;
                }

                if (! this.returnItemForm.reason) {
                    (! this.returnItemForm.reason) ? errors['reason'] = true : void(0);

                    this.returnItemForm.errors.record(errors);
                    this.removeHasLoading();

                    return;
                }  

                this.$validator.validateAll();

                if (! this.errors.any()) {
                    //save also as incident report
                    this.issueform.subject = "Title: " + this.returnItemForm.title;
                    this.issueform.body = "Body: " + this.returnItemForm.reason;
                    if(this.returnItemForm.case_number==null){
                        this.issueform.submit('post', '/issues')
                            .then(result => {
                                $('#returnItemModal a.btn-return-item-submit').addClass('disabled');

                                this.$validator.clean();
                                this.returnItemForm.case_number = result.data.issue_number;
                                this.issue_id = result.data.id;

                                var offline = 0;
                                for(var i=0; i<this.supports.length; i++)
                                {
                                    if(!this.supports[i].is_online)
                                        offline = offline + 1;
                                }

                                var supp = 0;
                                if(offline!=this.supports.length){
                                    for(var i=0; i<this.supports.length; i++)
                                    {
                                        if(this.supports[i].is_online)
                                            var supp = this.supports[i].id;
                                    }
                                }

                                let casenum = result.data.issue_number;

                                //if(offline!=this.supports.length)
                                if(supp==0) supp = 2; // mam cha's account
                                this.newConvoForm.supportIds.push(supp);
                                this.newConvoForm.submit('put', '/customer-service/' + result.data.id)
                                .then(result => {
                                    //alert(result.data.chat_room.name);
                                    this.notif = result.data;
                                    this.issueform.subject = null;
                                    this.issueform.body = null;
                                    this.newConvoForm.supportIds = [];

                                    //open chat box with case number as title for the conversation
                                    var chat = [];
                                    $(".panel-chat-box").each(function(){
                                        chat.push($(this).attr('id'));
                                    });

                                    if (casenum) {
                                        if($.inArray('user'+casenum,chat)=='-1'){
                                            Event.fire('chat-box.show', this.notif[0]);
                                        }
                                    }

                                    setTimeout(function(){
                                      $('.js-auto-size').textareaAutoSize();
                                    }, 5000);
                                })
                                .catch($.noop);

                                //update item
                                axios.post('/user/purchase-report/return-item', {
                                    id: this.returnItemForm.id,
                                    reason: this.returnItemForm.reason,
                                    case_number: result.data.issue_number,
                                    //shipping: this.returnItemForm.shipping_fee
                                }).then(result => {
                                    if(this.$refs.returnedItemFiles.dropzone.getAcceptedFiles().length > 0) {
                                        this.$refs.returnedItemFiles.dropzone.processQueue();
                                    } else {
                                        $('#returnItemModal').modal('hide');
                                        toastr.success('Successfully saved.');

                                        this.selectedOrder.status = 'Returned';
                                        this.returnItemForm.submitted = true;
                                        this.returnItemForm.errors.clear();
                                            
                                        this.removeHasLoading();
                                    }

                                    // this.getOrders();
                                })
                                .catch(error => {
                                    toastr.error('Updated Failed.');

                                    this.removeHasLoading();
                                });

                            })
                            .catch($.noop);
                    }
                    else{
                        axiosAPIv1.put('/issues/'+this.issue_id, {
                                    subject: "Title: " + this.returnItemForm.title,
                                    body: "Body: " + this.returnItemForm.reason,
                                    case_number: this.returnItemForm.case_number,
                                }).then(result => {
                                $('#returnItemModal a.btn-return-item-submit').addClass('disabled');
                                this.$validator.clean();

                                //update item
                                axios.post('/user/purchase-report/return-item', {
                                    id: this.returnItemForm.id,
                                    reason: this.returnItemForm.reason,
                                    case_number: this.returnItemForm.case_number,
                                    shipping: this.returnItemForm.shipping_fee
                                }).then(result => {
                                    if(this.$refs.returnedItemFiles.dropzone.getAcceptedFiles().length > 0) {
                                        this.$refs.returnedItemFiles.dropzone.processQueue();
                                    } else {
                                        $('#returnItemModal').modal('hide');
                                        toastr.success('Successfully saved.');

                                        this.selectedOrder.status = 'Returned';
                                        this.returnItemForm.submitted = true;
                                        this.returnItemForm.errors.clear();

                                        this.removeHasLoading();
                                    }
                                    
                                    // this.getOrders();
                                })
                                .catch(error => {
                                    toastr.error('Updated Failed.');

                                    this.removeHasLoading();
                                });

                            })
                            .catch($.noop);

                    }
                }

            //}
        },

        removeHasLoading() {
            setTimeout(() => {
                let $btn = $('.btn-return-item-submit');
                let $content = $btn.find('.btn-content');
                let $loader = $btn.find('.loader');

                $loader.fadeOut(100, function () {
                    $content.fadeIn(100);
                });
            }, 300);
        },

        print(){
            var html_container = '';
            html_container = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns:fb="http://ogp.me/ns/fb#"><head>';
            html_container += '<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons" />';
            html_container += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>';
            html_container += '<style>.airway-product-modal .modal-content{border:none;font-size:11px}.airway-product-modal .modal-content .modal-body{overflow-y:auto;padding:24px 6px!important}.airway-product-modal .modal-content .modal-body .deliver{font-family:Cambria;font-size:15px;font-weight:700}.airway-product-modal .modal-content .modal-body .address{width:400px}.airway-product-modal .modal-content .modal-body table,.airway-product-modal .modal-content .modal-body td,.airway-product-modal .modal-content .modal-body th{border:1px solid #000}.airway-product-modal .modal-content .modal-body table{border-collapse:collapse;width:100%}.airway-product-modal .modal-content .modal-body th{text-align:left;font-weight:700}.airway-product-modal .modal-content .modal-body .awb{float:right;margin-right:20px;margin-top:-120px}.airway-product-modal .modal-content .modal-body .received{float:left;margin-left:15px;margin-top:5px}.airway-product-modal .modal-content .modal-body .attempt{float:right;margin-right:20px;margin-top:5px}.airway-product-modal .modal-content .modal-body .text-align{text-align:center}.airway-product-modal .modal-content .modal-body .padding-left{padding-left:10px}.airway-product-modal .modal-content .modal-body .border-top{border-top:none}.airway-product-modal .modal-content .modal-body .b{font-weight:700}.airway-product-modal .modal-content .modal-body .margin{margin-top:15px}.airway-product-modal .modal-content .modal-body .float{float:right}.airway-product-modal .modal-content .modal-body .inclusive{font-weight:700;font-size:9px;margin-right:10px}.airway-product-modal .modal-content .modal-body hr{border-style:solid;border-color:#000;margin-top:10px;width:300px}.airway-product-modal .modal-content .modal-footer{padding:5px}</style>';
            html_container += '</head><body><div class="airway-product-modal"><div class="modal-content"><div class="modal-body">';

            var body = $("#print").html();
            var myWindow = window.open('','','width=500,height=500,scrollbars=yes,location=no');
            myWindow.document.write(html_container);
            myWindow.document.write(body);
            myWindow.document.write("</div></div></div>");
            myWindow.document.write('<div class="airway-product-modal"><div class="modal-content"><div class="modal-body">');
            myWindow.document.write(body);
            myWindow.document.write("</div></div></div></body></html>");
            myWindow.document.close();
            
            setTimeout(function(){
            myWindow.focus();
            myWindow.print();       
            }, 3000);
        },

        printPurchase(){
          var html_container = '';
          html_container = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns:fb="http://ogp.me/ns/fb#"><head>';
          html_container += '<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons" />';
          html_container += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>';  
          html_container += '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"><meta name="apple-mobile-web-app-capable" content="yes">';
          html_container += '<style>.purchase-report-modal .title {font-size: 22px;line-height: 1;margin: 0 4px 10px;padding: 0;border-bottom: 3px solid #80cbc4;} hr {border-color: #009688;border-style: dotted;margin: 0;} .payment-info .col-sm-6 {text-align: right;} .label {border-color: #fff !important; color: #fff !important; font-size: 12px;font-weight: 400;line-height: 1.25;display: inline-block;height: 20px;border-radius: 0;} @media print {.reasonCont {display:none} .label-success {background-color: #4caf50 !important;} .label-default {background-color: #9e9e9e !important;} .label-warning {background-color: #ff5722 !important;} .label-primary {background-color: #009688 !important;} .label-info {background-color: #03a9f4 !important;}}</style>';    
          html_container += '</head><body style="-webkit-print-color-adjust: exact !important;"><div class="purchase-report-modal">';

          var body = $("#printPurchase").html();
          var myWindow = window.open('','','width=500,height=500,scrollbars=yes,location=no');
          myWindow.document.write(html_container);
          myWindow.document.write(body);
          myWindow.document.write("</div></body></html>");
          myWindow.document.close();
          
          setTimeout(function(){
          myWindow.focus();
          myWindow.print();
          }, 3000);
        },

        showVideoEvidenceModal(e, index, modal) {
            e.preventDefault();

            $('#' + modal).modal('hide');

            setTimeout(function() {
                $('a#video-evidence-'+index).ekkoLightbox({ 
                    wrapping: false, 
                    alwaysShowClose: true,
                    onHidden: function(direction, itemIndex) {
                        $('#' + modal).modal('show');
                    } 
                });
            }, 1000);

        },

        showAddMoreEvidencesModal(e, title) {
            e.preventDefault();

            this.dz2 = 'dropzone';

            $('#viewProduct').modal('hide');

            $('#add-more-evidences-modal h3').text(title);
            $('#add-more-evidences-modal').modal('show');      
        },

        showOrderDetail(id, e) {
            if(e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#order-row-' + id).toggleClass('hide');
            }
        },

        markAsReceived(orderDetailId) {
            var areyousure = this.lang.data['you-sure-msg'];
            swal({
                title: areyousure,
                html: this.lang.data['mark-receive-msg-1']+'<a href="/faq" target="_blank"> '+this.lang.data['mark-receive-msg-2']+'</a>.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: this.lang.data['yes']
            }).then(() => {
                axios.post('purchase-report/update-receive/' + orderDetailId)
                    .then(result => {
                        swal(this.lang.data['received'], this.lang.data['mark-received-msg'], 'success');

                        this.getOrders();
                    })
                    .catch(error => {
                        toastr.error(this.lang.data['update-failed-msg']);
                    });
            });
        }
    },

    computed: {
        totalShippingFee() { 
            var groupArray = require('group-array');
            var group = groupArray(this.listProd.data, 'product.store.id');
            //console.log(group);
            var weight = 0;
            var volume = 0;
            var totalChargeWeight = 0;
            var productPrice = 0;
            var salePrice = 0;
            var totalWeight = 0;
            var chargeWeight = 0;
            var total = 0;
            var totalSP = 0;
            var discount = 0;
            var totaldiscount = 0;
            var t = 0;
            var disc = [];
            var vm = this;
            $.each( group, function( key, value ) {
                //console.log("Loop:"+value.length);
                if(value.length>0){
                    //console.log(value);
                    totalChargeWeight = 0;
                    productPrice = 0;
                    salePrice = 0;
                    var totalSalePrice = 0;
                    discount = 0;
                    for(var i=0; i<value.length; i++){
                        //console.log(value[i].product.weight);
                        if(value[i].status != "Cancelled"){
                          weight = value[i].product.weight;
                          volume = (value[i].product.length*value[i].product.width*value[i].product.height)/3500;
                          if(weight>volume) chargeWeight = weight;
                          if(volume>weight) chargeWeight = volume;
                          totalChargeWeight+= (parseFloat(chargeWeight)*parseFloat(value[i].quantity));
                          productPrice += ((value[i].sale_price > 0 ? parseFloat(value[i].sale_price) : parseFloat(value[i].price_per_item))*parseFloat(value[i].quantity));
                          var sp = value[i].subtotal;
                          salePrice += (parseFloat(sp.replace(',', '')));
                        }
                    }
                    //console.log("total Price :"+productPrice);
                    totalWeight = totalChargeWeight * 1.2; //add 20% to total weight
                    //console.log("twt :"+totalWeight);
                    let rateCat = "SAMM";
                    let halfkg = 0;
                    let fkg = 0;
                    let excess = 0;
                    let rate = 0;
                    // rate computation
                    //console.log(rates);
                    $.each(rates, function(i, item) {
                      if(rates[i].rate_category == rateCat){
                        halfkg = rates[i].halfkg;
                        fkg = rates[i].fkg;
                        excess = rates[i].excess;
                      }
                    });

                    if(totalWeight <= 0.5){
                      rate = halfkg;
                    }
                    else if(totalWeight >0.5 && totalWeight <=1){
                      rate = fkg;
                    }
                    else{
                      totalWeight -=1;
                      rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
                    }
                    //console.log('rate b4 '+rate);
                    rate = parseFloat(rate) + 20; // add 20 for markup
                    //console.log("rate -> "+Math.ceil(rate));

                    //valuation charge computation
                    let vc = productPrice * 0.01; 
                    //console.log("vc -> "+Math.ceil(vc));

                    //tax computation - docu stamp
                    let vtx = 0;
                    if(productPrice > 0 && productPrice <=1000){
                        vtx = ((rate -1)+vc) * .12;
                      }
                    else{
                        vtx = ((rate -10)+vc) * .12;
                    }
                    //console.log("tax -> "+Math.ceil(vtx));

                    //total fee
                    let sFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);        

                    //console.log("total fee :"+sFee);

                    total = productPrice + sFee;
                    t += total;

                    totalSalePrice += salePrice;
                    totalSP += salePrice;
                    discount = totalSalePrice - total;
                    totaldiscount += discount;
                    //console.log('discount-'+discount);
                    
                } 

            });

            this.totalsub = totalSP;
            this.totaldiscount = totaldiscount;
            this.priceAfterDiscount = totalSP - totaldiscount;

            let FinalFee = t;
            //console.log(FinalFee);
            return this.priceAfterDiscount;
        },

    }
})

function roundOff(v) {
    return Math.round(v);
}

$(function() {

    $('#add-more-evidences-modal').on('hidden.bs.modal', function (e) {
        vm.dz2 = '';

        vm.showDetails(vm.orderDetails.id);

        setTimeout(function() {
            $('#viewProduct').modal('show');
        }, 500);
    });

});