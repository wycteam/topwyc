//NOT USED! 

var user = window.Laravel.user;
var display_name = window.Laravel.user.display_name;
//checkbox value
var display_stat = "";
 if(display_name == 'Real Name'){
    display_stat = false
 }else{
    display_stat = true
 }
var accountInfo = new Vue({
    el: '#account-info',
    data: {
        user:user,
        optionGender: ['Male', 'Female'],

        accInfo: new Form({
            secondary_email: user.secondary_email
            ,birth_date: user.birth_date
            ,nick_name: user.nick_name
            ,gender: user.gender
            ,display_name: display_stat 
        },{ baseURL: '/profile/update'}),
    },
    directives: {
        datepicker: {
            bind(el, binding, vnode) {
                $(el).datepicker({
                    format: 'yyyy-mm-dd'
                }).on('changeDate', (e) => {
                    accountInfo.$set(accountInfo.accInfo, 'birth_date', e.format('yyyy-mm-dd'));
                });
            }
        }
    },
    methods:{
        //save account info
        saveInfo:function (event){
            this.accInfo.submit('post', '/account-info')
            .then(result => {
                this.accInfo = new Form({
                    secondary_email: result.secondary_email
                    ,birth_date: result.birth_date
                    ,nick_name: result.nick_name
                    ,gender: result.gender
                    ,display_name: display_stat    
                },{ baseURL: '/profile/update'});
                toastr.success('Your profile has been updated.');
            })
            .catch(error => {
                toastr.error('Update failed.');
            });
        }
    }


});

