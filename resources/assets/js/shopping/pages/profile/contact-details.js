//NOT USED! 

var user = window.Laravel.user;

var contactDetails = new Vue({
  el: '#contact-details',
  data: {
    user:user,
    optionProv: [],
    optionCity: [],
    addressInfo: new Form({
        address     : ''
       ,barangay    : ''
       ,city        : ''
       ,province    : ''
       ,zip_code    : ''
       ,landmark    : ''
       ,remarks     : ''  
       ,landline    : ''
       ,mobile      : ''
    },{ baseURL  : '/profile/update'}),


    homeAddressInfo: new Form({
          address     : ''
         ,barangay    : ''
         ,city        : '' 
         ,province    : ''
         ,zip_code    : ''
         ,landmark    : ''
         ,remarks     : ''
         ,landline    : ''
         ,mobile      : ''
    },{ baseURL  : '/profile/update'})
  },

  components: {
  	Multiselect: window.VueMultiselect.default
  },

  methods:{
    getSecurityAnswers() {
      axios.get('/profile/security-answers')
        .then(result => {
          this.securityAnswers = result.data.questions;
          if (this.securityAnswers.length) {
            this.showSecurity = false;
          }
      });
    },

    getCity:function(data){
      this.homeAddressInfo.city = '';
      axios.get('/profile/city/'+data)
        .then(result => {
        var array = [];
        for(var i = 0; i < result.data.length; i++){
          array.push(result.data[i].name);
        }
        this.optionCity = array;
      });
    },

    saveHomeAddress:function(event){
      this.homeAddressInfo.submit('post', '/home-address-info')
      .then(result => {
          toastr.success('Successfully saved.');
      })
      .catch(error => {
          toastr.error('Updated Failed.');
      });
    },
  },

  created() {
    

    axios.get('/profile/get-home-address')
      .then(result => {
        if(result.data.mobile != '' && result.data.landline != '' && result.data.residence != null){
          this.homeAddressInfo = new Form($.extend({
               mobile   : result.data.mobile
              ,landline : result.data.landline
              ,residence: result.data.residence
          }, result.data.residence), { baseURL: '/profile/update'});
          axios.get('/profile/city/'+this.homeAddressInfo.province)
            .then(result => {
            this.optionCity = result.data;
          });
        }else{
          homeAddressInfo: new Form({
                address     : ''
               ,barangay    : ''
               ,city        : '' 
               ,province    : ''
               ,zip_code    : ''
               ,landmark    : ''
               ,remarks     : ''
               ,landline    : ''
               ,mobile      : ''
          },{ baseURL  : '/profile/update'})
        }
    });

    
    axios.get('/profile/provinces')
      .then(result => {
        var array = [];
        for(var i = 0; i < result.data.length; i++){
          array.push(result.data[i].name);
        }
        this.optionProv = array;
    });
  },

  
});

