//NOT USED! 

var user = window.Laravel.user;
var display_name = window.Laravel.user.display_name;
//checkbox value
var display_stat = "";
 if(display_name == 'Real Name'){
    display_stat = false
 }else{
    display_stat = true
 }

Vue.component('que-answer',  require('../../components/profile/SecurityQuestionAnswer.vue'));

var securitySettings = new Vue({
  el: '#security-settings',
  data: {
    showSecurity: true,
    user:user,
    valueQue1:{},
    valueQue2:{},
    valueQue3:{},
    securityQuestions: [],
    securityAnswers: [],
  
    changePassword: new Form({
       old_password : ''
      ,new_password : ''
      ,new_password_confirmation : ''
    },{ baseURL:'/profile/update'}),

    securityQuestion: new Form({
       question  : ''
      ,question1 : ''
      ,question2 : ''
      ,answer    : ''
      ,answer1   : ''
      ,answer2   : ''
    },{ baseURL:'/profile/update'}),

  },
  methods:{
    //show modals
    shipAddModal:function (event){
       var elShow = event.target.name;
       $('#'+elShow+2).modal('toggle');
    },
  
    fnchangePassword:function(event){
      this.changePassword.submit('post', '/change-password')
      .then(result => {
          toastr.success('Your password has been changed.');
      })
      .catch(error => {
          toastr.error('Update failed.');
      });
    },

    saveSecurityQuestion:function(event){
      this.securityQuestion.submit('post', '/answer-security-questions')
      .then(result => {
          toastr.success('Successfully saved.');
          this.getSecurityAnswers();
      })
      .catch(error => {
          toastr.error('Updated Failed.');
      });
    },

    saveNewAddress:function(event){
      this.addressInfo.submit('post', '/address-info');
    },

    getSecurityAnswers() {
      axios.get('/profile/security-answers')
        .then(result => {
          this.securityAnswers = result.data.questions;
          if (this.securityAnswers.length) {
            this.showSecurity = false;
          }
      });
    },
    
  },

  created() {
    axios.get('/profile/security-questions')
      .then(result => {
        this.securityQuestions = result.data;
    });
   
    this.getSecurityAnswers();
  },

  computed: {
    answerEmpty: function () {
      return $.isEmptyObject(this.securityAnswers);
    }
  }
});

