Vue.component('compare-list',  require('../components/pages/Compare.vue'));
Vue.component('comp-modal',  require('../components/DialogModal.vue'));

Vue.filter('currency', function (value) {
    return  '₱'+numberWithCommas(parseFloat(value).toFixed(2));
});

var app = new Vue({

	el: '#compare-content',
	data: {
		items:[],
		no_item: '',
		id:'',
		index:'',
		lang:[]
	},
	created(){
		function getTranslation() {
            return axios.get('/translate/compare');
        }

		function getCompare() {
			return axiosAPIv1.get('/compare');
		}
		
		axios.all([
			getTranslation(),
			getCompare()
		]).then(axios.spread(
			(
				translation,
				compare
			) => {

			this.lang = translation.data.data;
			
			if(compare.data.data != 0)
				this.items = compare.status == 200? compare.data:[];
			else
				$('#no-item').show(); //this.no_item = "There are no items in your comparison";
		}))
		.catch($.noop);
	},
	methods: {
		showRemoveDialog(id,index){
			this.id = id;
			this.index = index;
			$('#dialogRemove').modal('toggle');
		},
		deleteCompare () {
			axiosAPIv1.get('/deletecompare/' + this.id)			
			.then(result => {
				$("#comp-count").text(result.data);
				toastr.success(this.lang['removed'],this.items.data[this.index].name);
				this.items.data.splice(this.index, 1);
				if(this.items.data.length == 0)
					$('#no-item').show();					
			});
		},
		addtocart (id,index) {
			axios.get(location.origin + '/cart/add',{
			    params: {
			    	prod_id:$('#prod_id'+id).val(),
					qty:$('#qty'+id).val()
			    }
			})
			.then(result => {
				$("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                if(result.data.error_msg)
                {
                	if(result.data.error_msg=='Redirect')
                		window.location = '/product/' + result.data.slug;
                	else
	                	toastr.error(result.data.error_msg, this.lang['not-added']);            
                } else {
					toastr.success(this.lang['added'], this.items.data[index].name);
					this.items.data.splice(index, 1);
					if(this.items.data.length == 0)
						$('#no-item').show();					
					axiosAPIv1.get('/deletecompare/' + id)
					.then(result => {
						$("#comp-count").text(result.data);
					});          
                }
			});	
		}
	}

});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
