$(function () {
    $('#birth_date').datepicker({format: 'yyyy-mm-dd'})
        .mask('0000-00-00');

	$('#first_name, #last_name').keypress(function(event){
		var inputValue = event.charCode;
		//alert(inputValue);
		if(!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123)||(inputValue==32) || (inputValue==0))){
		event.preventDefault();
		}
	});

});
