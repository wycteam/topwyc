Vue.component('modals',  require('../components/ProductModal.vue'));
import { quillEditor } from 'vue-quill-editor';

let data = window.Laravel.data;
let rates = window.Laravel.rates;

var app = new Vue({
  el: '#productCreate',
  data: {
    progress: 0,
    inc: 0,
    primaryLoading: false,
    primaryLoading2: false,
    primaryLoading3: false,
    primaryLoading4: false,
    primaryLoading5: false,
    primaryLoading6: false,
    colorLoading1: false,
    colorLoading2: false,
    colorLoading3: false,
    colorLoading4: false,
    colorLoading5: false,
    colorLoading6: false,
    colorLoading7: false,
    colorLoading8: false,
    colorLoading9: false,
    colorLoading10: false,
    descLoading1: false,
    descLoading2: false,
    descLoading3: false,
    descLoading4: false,
    descLoading5: false,
    descLoading6: false,
    descLoading7: false,
    descLoading8: false,
    descLoading9: false,
    descLoading10: false,
    descLoading11: false,
    descLoading12: false,
    descLoading13: false,
    descLoading14: false,
    descLoading15: false,
    descLoading16: false,
    descLoading17: false,
    descLoading18: false,
    descLoading19: false,
    descLoading20: false,
    countcolor:data.product_color_variations.length + 1,
    countsize1:1,
    countsize2:1,
    countsize3:1,
    countsize4:1,
    countsize5:1,
    countsize6:1,
    countsize7:1,
    countsize8:1,
    countsize9:1,
    countsize10:1,
    lang:[],
    optionPackage: [],
    insurance: false,
    editorOptions: {},
    productForm:new Form({
      user_id: data.user_id,
      id: data.id,
      name: data.name,
      cn_name: data.cn_name,
      image: data.main_image,
      primaryImage: '',
      price: data.price,
      discount: data.discount,
      sale_price: data.sale_price,
      boxContent: data.boxContent,
      description: data.description,
      cn_description: data.cn_description,
      is_draft: data.is_draft,
      is_active: data.is_active,
      stock: data.stock,
      store_id:data.store_id,
      store_list:[],
      storeOption: [],
      category_id:[],
      catOption: [],
      wPeriod: data.wPeriod,
      brand: data.brand,
      weight: data.weight,
      package_type: data.package_type,
      image1: data.main_image,
      image2: data.main_image2,
      image3: data.main_image3,
      image4: data.main_image4,
      image5: data.main_image5,
      image6: data.main_image6,
      descimage1: data.desc_image,
      descimage2: data.desc_image2,
      descimage3: data.desc_image3,
      descimage4: data.desc_image4,
      descimage5: data.desc_image5,
      descimage6: data.desc_image6,
      descimage7: data.desc_image7,
      descimage8: data.desc_image8,
      descimage9: data.desc_image9,
      descimage10: data.desc_image10,
      descimage11: data.desc_image11,
      descimage12: data.desc_image12,
      descimage13: data.desc_image13,
      descimage14: data.desc_image14,
      descimage15: data.desc_image15,
      descimage16: data.desc_image16,
      descimage17: data.desc_image17,
      descimage18: data.desc_image18,
      descimage19: data.desc_image19,
      descimage20: data.desc_image20,
      colorImage1: '',
      colorImage2: '',
      colorImage3: '',
      colorImage4: '',
      colorImage5: '',
      colorImage6: '',
      colorImage7: '',
      colorImage8: '',
      colorImage9: '',
      colorImage10: '',
      cat_holder: '',
      varcolor: [
        {id:'', temp_id:1, label: '', stock: 0, image_name: 'color1'},
        {id:'', temp_id:2, label: '', stock: 0, image_name: 'color2'},
        {id:'', temp_id:3, label: '', stock: 0, image_name: 'color3'},
        {id:'', temp_id:4, label: '', stock: 0, image_name: 'color4'},
        {id:'', temp_id:5, label: '', stock: 0, image_name: 'color5'},
        {id:'', temp_id:6, label: '', stock: 0, image_name: 'color6'},
        {id:'', temp_id:7, label: '', stock: 0, image_name: 'color7'},
        {id:'', temp_id:8, label: '', stock: 0, image_name: 'color8'},
        {id:'', temp_id:9, label: '', stock: 0, image_name: 'color9'},
        {id:'', temp_id:10, label: '', stock: 0, image_name: 'color10'}
      ],
      varsize1: data.product_size_variations1,
      varsize2: data.product_size_variations2,
      varsize3: data.product_size_variations3,
      varsize4: data.product_size_variations4,
      varsize5: data.product_size_variations5,
      varsize6: data.product_size_variations6,
      varsize7: data.product_size_variations7,
      varsize8: data.product_size_variations8,
      varsize9: data.product_size_variations9,
      varsize10: data.product_size_variations10,
      length: data.length,
      width: data.width,
      height: data.height,
      shipping_fee: data.shipping_fee,
      shipping_fee_2go: data.shipping_fee_2go,
      vat: data.vat,
      vat_2go: data.vat_2go,
      charge: false,
      fee_included: true,
    }, { baseURL: 'http://'+Laravel.base_api_url }),

  },
  components: {
    quillEditor,
  	Multiselect: window.VueMultiselect.default
  },
  computed:{
    productName: function () {
      let pname = this.lang['sample-prod-name'];
      return pname = this.productForm.name == ""? pname:this.productForm.name;
    },

    productPrice: function () {
      let pprice = '';

      if(this.productForm.sale_price == null || this.productForm.sale_price == ''){
        pprice = this.productForm.price == ''? pprice:this.productForm.price;
      }
      else{
        pprice = this.productForm.sale_price ;
      }

      if(this.productForm.fee_included){
          pprice = pprice < 1 ? 0:pprice;
          pprice = parseFloat(pprice)  + parseFloat(this.totalfee);
      }
      return pprice < 1 ? '':"P"+pprice;
    },

    productPriceOld: function () {
      let pprice = '';
      if(this.productForm.sale_price > 0 ){
        pprice = this.productForm.price;
        if(this.productForm.fee_included){
            if(pprice < 1){
              pprice = 0;
            }
            else{
              pprice = parseFloat(pprice)  + parseFloat(this.totalfee);
            }
        }
      }
      return pprice < 1 ? '':"P"+pprice;
    },

    productDiscount: function () {
      let pprice = '0';
      if(this.productForm.fee_included){
        pprice = this.productForm.sale_price < 1? pprice : this.discountWithFee;
        this.productForm.discount = pprice;
        return pprice+"%";
      }
      else{
        pprice =  this.productForm.sale_price < 1? pprice:this.getdiscount;
        this.productForm.discount = pprice;
        return pprice+"%";
      }
    },

    getdiscount: function (){
      return Math.round((1-(this.productForm.sale_price / this.productForm.price))*100);
    },
    discountWithFee: function (){
      var a = parseFloat(this.productForm.sale_price) + parseFloat(this.totalfee);
      var b = parseFloat(this.productForm.price) + parseFloat(this.totalfee);
      return Math.round((1-(a / b))*100);
    },
    volume: function () {
      let vol = '';
      if(this.productForm.length > 0 && this.productForm.width > 0 && this.productForm.height > 0 ){
        vol = ((this.productForm.length * this.productForm.width * this.productForm.height)/3500);
      }
      return vol;
    },  
    valuationCharge: function () {
      if(!this.insurance){
        this.productForm.charge = 0;
        return 0;
      }
      let charge = 0;
      let pprice = this.productForm.price;
      pprice = this.productForm.sale_price < 1? pprice:this.productForm.sale_price;
      if(pprice > 0 ){
        charge = (pprice * .01);
      }
      this.productForm.charge = Math.ceil(charge);
      return Math.ceil(charge);
    }, 
    shippingFee: function () {
      let rateCat = "SAMM";
      let halfkg = 0;
      let fkg = 0;
      let excess = 0;
      //let fkg3 = 0;
      //let excess2 = 0;
      $.each(rates, function(i, item) {
          if(rates[i].rate_category == rateCat){
            halfkg = rates[i].halfkg;
            fkg = rates[i].fkg;
            excess = rates[i].excess;
            //fkg3 = rates[i].fkg3;
            //excess2 = rates[i].excess2;
          }
      });

      let rate = 0;
      let wycrate = 0;
      let greaterWeight = 0;
      if(this.volume > 0 && this.productForm.weight > 0){
        //console.log('vol->'+this.volume);
        //console.log('w->'+this.productForm.weight);
        let greaterWeight = this.productForm.weight > this.volume ? this.productForm.weight :this.volume;
        let greaterWeight2 = greaterWeight;

        //add 20% to the weight to be used in computation
        //console.log('wt->'+greaterWeight);

        greaterWeight = (parseFloat(greaterWeight));
        //console.log('wt->'+greaterWeight);
        //vat = greaterWeight;
        if(greaterWeight <= 0.5){
          rate = halfkg;
          //wycrate = fkg3;
        }
        else if(greaterWeight >0.5 && greaterWeight <=1){
          rate = fkg;
          //wycrate = fkg3;
        }
        else{
          greaterWeight -=1;
          rate = (greaterWeight * parseFloat(excess)) + parseFloat(fkg);
          //greaterWeight -=2;
          //wycrate = (greaterWeight * parseFloat(excess2)) + parseFloat(fkg3);
        }

        //add 20% to the weight to be used in computation
        greaterWeight = (parseFloat(greaterWeight2)*1.2);
        //vat = greaterWeight;
        if(greaterWeight <= 0.5){
          wycrate = halfkg;
        }
        else if(greaterWeight >0.5 && greaterWeight <=1){
          wycrate = fkg;
        }
        else{
          greaterWeight -=1;
          wycrate = (greaterWeight * parseFloat(excess)) + parseFloat(fkg);
        }
      }
      this.productForm.shipping_fee_2go = rate; // rate for 2go
      //let publishRate = wycrate;
      //wycrate = (parseFloat(wycrate) - parseFloat(rate))/1.5;
      //just add 20 pesos to 2go rates
      if(this.volume > 0 && this.productForm.weight > 0){
        wycrate = parseFloat(wycrate) + 20;
      }
      this.productForm.shipping_fee = Math.ceil(wycrate);
      //console.log(' rate->'+wycrate);
      return Math.ceil(wycrate);
    }, 
    tax: function () {
      let tx = 0;
      let pprice = this.productForm.price;
      pprice = this.productForm.sale_price < 1? pprice:this.productForm.sale_price;
      var rate = this.productForm.shipping_fee_2go;

      //documentation stamp
      if(rate>0){
        if(pprice > 0 && pprice <=1000){
          rate = parseFloat(rate)-1;
        }
        else{
          rate = parseFloat(rate)-10;
        }
      }

      let charge = rate + this.valuationCharge;
      tx = (parseFloat(charge) * .12);

      var wycrate = this.shippingFee;
      var wycrate = this.shippingFee;
      if(wycrate>0){
        if(pprice > 0 && pprice <=1000){
          wycrate = parseFloat(wycrate)-1;
        }
        else{
          wycrate = parseFloat(wycrate)-10;
        }
      }
      let wyccharge = wycrate + parseFloat(this.valuationCharge);
      let wyctx = (parseFloat(wyccharge) * .12);
      this.productForm.vat_2go = tx; //tax for 2go
      this.productForm.vat = Math.ceil(wyctx);

      //check difference of 2go and wyc markup
      // let r1 = tx + charge;
      // let r2 = wyctx + wyccharge;
      // let sumdif = r2 - r1;
      // if(sumdif<25){
        
      // }
      return Math.ceil(wyctx);
    },

    totalfee: function () {
      let pprice = this.productForm.price;
      pprice = this.productForm.sale_price < 1? pprice:this.productForm.sale_price;
      let total = parseFloat(this.tax) + parseFloat(this.shippingFee) + parseFloat(this.valuationCharge);
      if(pprice){
        return total;
      }
      return 0;
    },

    totalprice: function () {
      let pprice = this.productForm.price;
      pprice = this.productForm.sale_price < 1? pprice:this.productForm.sale_price;
      if(pprice){
        return parseFloat(this.totalfee) + parseFloat(pprice);
      }
      return 0;
    },

  },

  methods:{

    incrementProgress(time) {
      setTimeout(() => {
        var prod = this.progress;
        if(time>20)
          this.progress = prod+1;
        else
          this.progress = prod+5;
        if (this.progress <= 100)
          this.incrementProgress(time);
      }, time)
    },

    formatPrice(value) {
        let val = (value/1).toFixed(2);
        return val;
    },

    submitProduct(e){
      this.productForm.is_draft = false;
      this.productForm.is_active = true;

      var stock1 = 0;
      $('.stock1').each(function(){
        stock1 = parseInt($(this).val()) + stock1;
      });
      if(stock1!=0)
        this.productForm.varcolor[0].stock = stock1

      var stock2 = 0;
      $('.stock2').each(function(){
        stock2 = parseInt($(this).val()) + stock2;
      });
      if(stock2!=0)
        this.productForm.varcolor[1].stock = stock2

      var stock3 = 0;
      $('.stock3').each(function(){
        stock3 = parseInt($(this).val()) + stock3;
      });
      if(stock3!=0)
        this.productForm.varcolor[2].stock = stock3

      var stock4 = 0;
      $('.stock4').each(function(){
        stock4 = parseInt($(this).val()) + stock4;
      });
      if(stock4!=0)
        this.productForm.varcolor[3].stock = stock4

      var stock5 = 0;
      $('.stock5').each(function(){
        stock5 = parseInt($(this).val()) + stock5;
      });
      if(stock5!=0)
        this.productForm.varcolor[4].stock = stock5

      var stock6 = 0;
      $('.stock6').each(function(){
        stock6 = parseInt($(this).val()) + stock6;
      });
      if(stock6!=0)
        this.productForm.varcolor[5].stock = stock6

      var stock7 = 0;
      $('.stock7').each(function(){
        stock7 = parseInt($(this).val()) + stock7;
      });
      if(stock7!=0)
        this.productForm.varcolor[6].stock = stock7

      var stock8 = 0;
      $('.stock8').each(function(){
        stock8 = parseInt($(this).val()) + stock8;
      });
      if(stock8!=0)
        this.productForm.varcolor[7].stock = stock8

      var stock9 = 0;
      $('.stock9').each(function(){
        stock9 = parseInt($(this).val()) + stock9;
      });
      if(stock9!=0)
        this.productForm.varcolor[8].stock = stock9

      var stock10 = 0;
      $('.stock10').each(function(){
        stock10 = parseInt($(this).val()) + stock10;
      });
      if(stock10!=0)
        this.productForm.varcolor[9].stock = stock10

      var stock = 0;
      for(var i=0; i<this.productForm.varcolor.length; i++)
      {
        stock = parseInt(this.productForm.varcolor[i].stock) + stock;

      }
      if(stock!=0)
        this.productForm.stock = stock;

      if(this.productForm.store_list)
      {
        this.productForm.store_id = this.productForm.store_list.id;
      }

      $('.colorShow').removeClass('duplicate-color');
      $('.required, .requiredsize').removeClass('empty');

      $(".required, .requiredsize").filter(function () {
          return $.trim($(this).val()).length == 0
      }).addClass('empty');

      var inputs = $('.colorShow');

      inputs.filter(function(i,el){
        return inputs.not(this).filter(function() {
          return this.value.toLowerCase() === el.value.toLowerCase();
        }).length !== 0;
      }).addClass('duplicate-color');


      if($(".colorShow").hasClass("duplicate-color")==true){
        toastr.error(this.lang['duplicate-color']);
        this.incomplete2();
      } else if($(".required").hasClass("empty")==true){
        toastr.error(this.lang['color-is-empty']);
        this.incomplete2();
      } else if ($(".colorImageRequired").hasClass("imagerequired")==true){
        toastr.error(this.lang['please-upload-color-image']);
        this.incomplete2();
      } else if ($(".requiredsize").hasClass("empty")==true){
          toastr.error(this.lang['size-is-empty']);
          this.incomplete2();
      } else if (this.productForm.weight<=0){
        toastr.error(this.lang['weight-should']);
        this.productForm.weight = "";
        this.incomplete2();
      } else{
        this.productForm.submit('put', $('#product-form').prop('action'))
        .then(result => {
            if(result)
            {
              $('.btnSaveDraft .btn-content').css('margin-top','0px');
              toastr.success(this.lang['product-update']);
              window.location.href = '/product/' + result.slug;
            }else
            {
              toastr.error(this.lang['please-try']);
              setTimeout(function(){
                $('.btnFinishProduct').prop('disabled', false);
              },500);
            }
        })
        .catch(error => {
          toastr.error(this.lang['please-complete']);
          setTimeout(function(){
            $('.btnFinishProduct').prop('disabled', false);
          },500);
        });
      }

    },

    submitAsDraft(e) {
      this.productForm.is_draft = true;
      this.productForm.is_active = false;

      if (this.productForm.store_list) {
        this.productForm.store_id = this.productForm.store_list.id;
      }

      var stock1 = 0;
      $('.stock1').each(function(){
        stock1 = parseInt($(this).val()) + stock1;
      });
      if(stock1!=0)
        this.productForm.varcolor[0].stock = stock1

      var stock2 = 0;
      $('.stock2').each(function(){
        stock2 = parseInt($(this).val()) + stock2;
      });
      if(stock2!=0)
        this.productForm.varcolor[1].stock = stock2

      var stock3 = 0;
      $('.stock3').each(function(){
        stock3 = parseInt($(this).val()) + stock3;
      });
      if(stock3!=0)
        this.productForm.varcolor[2].stock = stock3

      var stock4 = 0;
      $('.stock4').each(function(){
        stock4 = parseInt($(this).val()) + stock4;
      });
      if(stock4!=0)
        this.productForm.varcolor[3].stock = stock4

      var stock5 = 0;
      $('.stock5').each(function(){
        stock5 = parseInt($(this).val()) + stock5;
      });
      if(stock5!=0)
        this.productForm.varcolor[4].stock = stock5

      var stock6 = 0;
      $('.stock6').each(function(){
        stock6 = parseInt($(this).val()) + stock6;
      });
      if(stock6!=0)
        this.productForm.varcolor[5].stock = stock6

      var stock7 = 0;
      $('.stock7').each(function(){
        stock7 = parseInt($(this).val()) + stock7;
      });
      if(stock7!=0)
        this.productForm.varcolor[6].stock = stock7

      var stock8 = 0;
      $('.stock8').each(function(){
        stock8 = parseInt($(this).val()) + stock8;
      });
      if(stock8!=0)
        this.productForm.varcolor[7].stock = stock8

      var stock9 = 0;
      $('.stock9').each(function(){
        stock9 = parseInt($(this).val()) + stock9;
      });
      if(stock9!=0)
        this.productForm.varcolor[8].stock = stock9

      var stock10 = 0;
      $('.stock10').each(function(){
        stock10 = parseInt($(this).val()) + stock10;
      });
      if(stock10!=0)
        this.productForm.varcolor[9].stock = stock10

      var stock = 0;
      for(var i=0; i<this.productForm.varcolor.length; i++)
      {
        stock = parseInt(this.productForm.varcolor[i].stock) + stock;

      }
      if(stock!=0)
        this.productForm.stock = stock;

      $('.colorShow').removeClass('duplicate-color');
      $('.required, .requiredsize').removeClass('empty');

      $(".required, .requiredsize").filter(function () {
          return $.trim($(this).val()).length == 0
      }).addClass('empty');

      var inputs = $('.colorShow');

      inputs.filter(function(i,el){
        return inputs.not(this).filter(function() {
          return this.value.toLowerCase() === el.value.toLowerCase();
        }).length !== 0;
      }).addClass('duplicate-color');

      if($(".colorShow").hasClass("duplicate-color")==true){
        toastr.error(this.lang['duplicate-color']);
        this.incomplete();
      } else if($(".required").hasClass("empty")==true){ 
        toastr.error(this.lang['color-is-empty']);
        this.incomplete();
      } else if ($(".colorImageRequired").hasClass("imagerequired")==true){
        toastr.error(this.lang['please-upload-color-image']);
        this.incomplete();
      } else if ($(".requiredsize").hasClass("empty")==true){
          toastr.error(this.lang['size-is-empty']);
          this.incomplete();
      } else if (this.productForm.weight<=0){
        toastr.error(this.lang['weight-should']);
        this.productForm.weight = "";
        this.incomplete();
      } else{
        this.productForm.submit('put', $('#product-form').prop('action'))
        .then(result => {
            if(result)
            {
              toastr.success(this.lang['product-update']);
              window.location.href = '/product/' + result.slug;
            }else
            {
              toastr.error(this.lang['please-try']);
              setTimeout(function(){
                $('.btnSaveDraft').prop('disabled', false);
              },500);
            }
        })
        .catch(error => {
          toastr.error(this.lang['please-complete']);
          setTimeout(function(){
            $('.btnSaveDraft').prop('disabled', false);
          },500);
        });
      }

    },

    incomplete(){
      $('.btnSaveDraft .circular').show();
      setTimeout(function(){
        $('.btnSaveDraft').prop('disabled', false);
        $('.btnSaveDraft').css('height','36px');
        $('.btnSaveDraft .circular').hide();
        $('.btnSaveDraft .btn-content').css('margin-top','-20px');
        $('.btn-content').show();
      },500);      
    },

    incomplete2(){
      $('.btnFinishProduct .circular').show();
      setTimeout(function(){
        $('.btnFinishProduct').prop('disabled', false);
        $('.btnFinishProduct').css('height','36px');
        $('.btnFinishProduct .circular').hide();
        $('.btnFinishProduct .btn-content').css('margin-top','-20px');
        $('.btn-content').show();
      },500);     
    },


    productPreview(){
      $('#previewModal').modal('toggle');
    },

    greaterThan(){
      if(this.productForm.sale_price > this.productForm.price){
        toastr.error(this.lang['sale-price-cannot']);
        this.productForm.sale_price = '';
      }
      if(this.productForm.sale_price < 1){
        this.productForm.sale_price = '';
      }
    },

    checkMulti(){
      let catid = this.productForm.category_id;
      if(catid != ''){
        this.productForm.cat_holder = '';
      }
      else{
        this.productForm.cat_holder = this.lang['select-cat'];
      }
    },

    getCategories() {
        axiosAPIv1.get('/productcategories/'+Laravel.data.id)
            .then(result => {
                this.productForm.category_id = result.data.data;
            });
    },

    getStore() {
        axiosAPIv1.get('/productstore/'+Laravel.data.store_id)
            .then(result => {
                this.productForm.store_list = result.data.data;
            });
    },

    addColor(){
      if(this.countcolor<11){
        $('#colorDiv'+this.countcolor).show();
        $('#colorDiv'+this.countcolor+' .color').addClass('colorShow');
        $('#colorDiv'+this.countcolor+' .color').removeClass('colorHide');
        $('#colorDiv'+this.countcolor+' .colorRequired').addClass('required');
        $('#colorDiv'+this.countcolor+' .colorImageRequired').addClass('imagerequired');       
      }
      else {
        
      }
      this.countcolor++;
    },

    addSize1(){
      this.productForm.varsize1.push({id:'', temp_id:this.countsize1, label: '', stock: 0});
      this.countsize1++;
    },

    addSize2(){
      this.productForm.varsize2.push({id:'', temp_id:this.countsize2, label: '', stock: 0});
      this.countsize2++;
    },

    addSize3(){
      this.productForm.varsize3.push({id:'', temp_id:this.countsize3, label: '', stock: 0});
      this.countsize3++;
    },

    addSize4(){
      this.productForm.varsize4.push({id:'', temp_id:this.countsize4, label: '', stock: 0});
      this.countsize4++;
    },

    addSize5(){
      this.productForm.varsize5.push({id:'', temp_id:this.countsize5, label: '', stock: 0});
      this.countsize5++;
    },

    addSize6(){
      this.productForm.varsize6.push({id:'', temp_id:this.countsize6, label: '', stock: 0});
      this.countsize6++;
    },

    addSize7(){
      this.productForm.varsize7.push({id:'', temp_id:this.countsize7, label: '', stock: 0});
      this.countsize7++;
    },

    addSize8(){
      this.productForm.varsize8.push({id:'', temp_id:this.countsize8, label: '', stock: 0});
      this.countsize8++;
    },

    addSize9(){
      this.productForm.varsize9.push({id:'', temp_id:this.countsize9, label: '', stock: 0});
      this.countsize9++;
    },

    addSize10(){
      this.productForm.varsize10.push({id:'', temp_id:this.countsize10, label: '', stock: 0});
      this.countsize10++;
    },

    deleteColor(id){
      $('#colorDiv'+id).hide();
      $('#colorDiv'+id+' .color').addClass('colorHide');
      $('#colorDiv'+id+' .color').removeClass('colorShow');
      $('#colorDiv'+id+' .colorRequired').removeClass('required');
      $('#colorDiv'+id+' .colorImageRequired').removeClass('imagerequired');
      $('#colorDiv'+id+' input').removeClass('requiredsize');
      $('#colorDiv'+id+' input').removeClass('empty'); 
      
      var id = id - 1;
      this.productForm.varcolor[id].label = '';
      this.productForm.varcolor[id].stock = 0;
  
      if(id == 0)
        this.productForm.colorImage1 = '';
      else if(id == 1)
        this.productForm.colorImage2 = '';
      else if(id == 2)
        this.productForm.colorImage3 = '';
      else if(id == 3)
        this.productForm.colorImage4 = '';
      else if(id == 4)
        this.productForm.colorImage5 = '';
      else if(id == 5)
        this.productForm.colorImage6 = '';
      else if(id == 6)
        this.productForm.colorImage7 = '';
      else if(id == 7)
        this.productForm.colorImage8 = '';
      else if(id == 8)
        this.productForm.colorImage9 = '';
      else
        this.productForm.colorImage10 = ''; 

    },

    deleteSize1(id,tempid){
      for(var i=0;i<this.productForm.varsize1.length;i++){
          if(id != ""){
            if(this.productForm.varsize1[i].id == id){
                this.productForm.varsize1.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize1[i].temp_id == tempid){
                this.productForm.varsize1.splice(i, 1);
            }
          }
      }
    },

    deleteSize2(id,tempid){
      for(var i=0;i<this.productForm.varsize2.length;i++){
          if(id != ""){
            if(this.productForm.varsize2[i].id == id){
                this.productForm.varsize2.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize2[i].temp_id == tempid){
                this.productForm.varsize2.splice(i, 1);
            }
          }
      }
    },

    deleteSize3(id,tempid){
      for(var i=0;i<this.productForm.varsize3.length;i++){
          if(id != ""){
            if(this.productForm.varsize3[i].id == id){
                this.productForm.varsize3.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize3[i].temp_id == tempid){
                this.productForm.varsize3.splice(i, 1);
            }
          }
      }
    },

    deleteSize4(id,tempid){
      for(var i=0;i<this.productForm.varsize4.length;i++){
          if(id != ""){
            if(this.productForm.varsize4[i].id == id){
                this.productForm.varsize4.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize4[i].temp_id == tempid){
                this.productForm.varsize4.splice(i, 1);
            }
          }
      }
    },

    deleteSize5(id,tempid){
      for(var i=0;i<this.productForm.varsize5.length;i++){
          if(id != ""){
            if(this.productForm.varsize5[i].id == id){
                this.productForm.varsize5.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize5[i].temp_id == tempid){
                this.productForm.varsize5.splice(i, 1);
            }
          }
      }
    },

    deleteSize6(id,tempid){
      for(var i=0;i<this.productForm.varsize6.length;i++){
          if(id != ""){
            if(this.productForm.varsize6[i].id == id){
                this.productForm.varsize6.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize6[i].temp_id == tempid){
                this.productForm.varsize6.splice(i, 1);
            }
          }
      }
    },

    deleteSize7(id,tempid){
      for(var i=0;i<this.productForm.varsize7.length;i++){
          if(id != ""){
            if(this.productForm.varsize7[i].id == id){
                this.productForm.varsize7.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize7[i].temp_id == tempid){
                this.productForm.varsize7.splice(i, 1);
            }
          }
      }
    },

    deleteSize8(id,tempid){
      for(var i=0;i<this.productForm.varsize8.length;i++){
          if(id != ""){
            if(this.productForm.varsize8[i].id == id){
                this.productForm.varsize8.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize8[i].temp_id == tempid){
                this.productForm.varsize8.splice(i, 1);
            }
          }
      }
    },

    deleteSize9(id,tempid){
      for(var i=0;i<this.productForm.varsize9.length;i++){
          if(id != ""){
            if(this.productForm.varsize9[i].id == id){
                this.productForm.varsize9.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize9[i].temp_id == tempid){
                this.productForm.varsize9.splice(i, 1);
            }
          }
      }
    },

    deleteSize10(id,tempid){
      for(var i=0;i<this.productForm.varsize10.length;i++){
          if(id != ""){
            if(this.productForm.varsize10[i].id == id){
                this.productForm.varsize10.splice(i, 1);
            }
          } else {
            if(this.productForm.varsize10[i].temp_id == tempid){
                this.productForm.varsize10.splice(i, 1);
            }
          }
      }
    }


  },
    created(){
      this.productForm.fee_included = true;
      this.user_id = Laravel.user.id;
      axios.all([this.getCategories()]);
      axios.all([this.getStore()]);

      this.productForm.primaryImage = data.main_image != 'http://wyc06.dev/images/avatar/default-product.jpg'? 'primary.jpg':null;
      this.productForm.primaryImage2 = data.main_image2 != ''? 'primary2.jpg':null;
      this.productForm.primaryImage3 = data.main_image3 != ''? 'primary3.jpg':null;
      this.productForm.primaryImage4 = data.main_image4 != ''? 'primary4.jpg':null;
      this.productForm.primaryImage5 = data.main_image5 != ''? 'primary5.jpg':null;
      this.productForm.primaryImage6 = data.main_image6 != ''? 'primary6.jpg':null;

      for(var a=0;a<data.product_color_variations.length;a++){
        var b = a + 1;
        $('#colorDiv'+b).show();
        $('#colorDiv'+b+' .color').addClass('colorShow');
        $('#colorDiv'+b+' .color').removeClass('colorHide');
        $('#colorDiv'+b+' .colorRequired').addClass('required');
        //$('#colorDiv'+b+' .colorImageRequired').removeClass('imagerequired');
        this.productForm.varcolor[a].label = data.product_color_variations[a].label;
        this.productForm.varcolor[a].stock = data.product_color_variations[a].stock;
        this.productForm.varcolor[a].stock = data.product_color_variations[a].stock;

        if(a == 0)
          this.productForm.colorImage1 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else if(a == 1)
          this.productForm.colorImage2 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else if(a == 2)
          this.productForm.colorImage3 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else if(a == 3)
          this.productForm.colorImage4 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else if(a == 4)
          this.productForm.colorImage5 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else if(a == 5)
          this.productForm.colorImage6 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else if(a == 6)
          this.productForm.colorImage7 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else if(a == 7)
          this.productForm.colorImage8 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else if(a == 8)
          this.productForm.colorImage9 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';
        else
          this.productForm.colorImage10 = 'http://'+Laravel.base_api_url+'/'+data.image_path_color+data.product_color_variations[a].image_name+'.jpg';      
      }


      if(data.charge < 1){
        this.insurance = false;
      }
      else{
        this.insurance = true;
      }

      if(data.fee_included < 1){
        this.productForm.fee_included = true;
      }
      else{
        this.productForm.fee_included = true;
      }
      // get translation
      function getTranslation() {
          return axios.get('/translate/products');
      }

      // get categories
      function getCategories() {
        return axiosAPIv1.get('/category');
      }

      //get all stores
      function getStores() {
        return axiosAPIv1.get('/getUserStores');
      }

      axios.all([
         getTranslation(),
         getCategories(),
         getStores()
      ]).then(axios.spread(
        (
           translation,
           cats,
           stores,
        ) => {
        this.lang = translation.data.data;
        this.productForm.catOption = cats.data.success? cats.data.data.cats:null;
        this.productForm.storeOption = stores.data.success? stores.data.data.stores:null;
        this.productForm.store_id = data.store_id;
        var pack = [];
        pack.push(this.lang['pouch']);
        pack.push(this.lang['box']);
        this.optionPackage = pack;
      }))
      .catch($.noop);

    Event.listen('Imgfileupload.primaryLoading', () => {
        this.incrementProgress(this.inc);
        this.primaryLoading = true;
    });
    Event.listen('Imgfileupload.profileOnLoad', (img) => {
        this.productForm.primaryImage = 'primary.jpg';
        this.productForm.image = img;
        this.productForm.image1 = img;
        this.primaryLoading = false;
    });

    Event.listen('Imgfileupload.primaryLoading2', () => {
        this.incrementProgress(this.inc);
        this.primaryLoading2 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad2', (img) => {
        this.productForm.primaryImage2 = 'primary2.jpg';
        this.productForm.image2 = img;
        this.primaryLoading2 = false;
    });

    Event.listen('Imgfileupload.primaryLoading3', () => {
        this.incrementProgress(this.inc);
        this.primaryLoading3 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad3', (img) => {
        this.productForm.primaryImage3 = 'primary3.jpg';
        this.productForm.image3 = img;
        this.primaryLoading3 = false;
    });

    Event.listen('Imgfileupload.primaryLoading4', () => {
        this.incrementProgress(this.inc);
        this.primaryLoading4 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad4', (img) => {
        this.productForm.primaryImage4 = 'primary4.jpg';
        this.productForm.image4 = img;
        this.primaryLoading4 = false;
    });

    Event.listen('Imgfileupload.primaryLoading5', () => {
        this.incrementProgress(this.inc);
        this.primaryLoading5 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad11', (img) => {
        this.productForm.primaryImage5 = 'primary5.jpg';
        this.productForm.image5 = img;
        this.primaryLoading5 = false;
    });

    Event.listen('Imgfileupload.primaryLoading6', () => {
        this.incrementProgress(this.inc);
        this.primaryLoading6 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad12', (img) => {
        this.productForm.primaryImage6 = 'primary6.jpg';
        this.productForm.image6 = img;
        this.primaryLoading6 = false;
    });

    Event.listen('Imgfileupload.descLoading1', () => {
        this.incrementProgress(this.inc);
        this.descLoading1 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad5', (img) => {
        this.productForm.descimage1 = img;
        this.descLoading1 = false;
    });

    Event.listen('Imgfileupload.descLoading2', () => {
        this.incrementProgress(this.inc);
        this.descLoading2 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad6', (img) => {
        this.productForm.descimage2 = img;
        this.descLoading2 = false;
    });

    Event.listen('Imgfileupload.descLoading3', () => {
        this.incrementProgress(this.inc);
        this.descLoading3 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad7', (img) => {
        this.productForm.descimage3 = img;
        this.descLoading3 = false;
    });

    Event.listen('Imgfileupload.descLoading4', () => {
        this.incrementProgress(this.inc);
        this.descLoading4 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad8', (img) => {
        this.productForm.descimage4 = img;
        this.descLoading4 = false;
    });

    Event.listen('Imgfileupload.descLoading5', () => {
        this.incrementProgress(this.inc);
        this.descLoading5 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad9', (img) => {
        this.productForm.descimage5 = img;
        this.descLoading5 = false;
    });

    Event.listen('Imgfileupload.descLoading6', () => {
        this.incrementProgress(this.inc);
        this.descLoading6 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad10', (img) => {
        this.productForm.descimage6 = img;
        this.descLoading6 = false;
    });

    Event.listen('Imgfileupload.descLoading7', () => {
        this.incrementProgress(this.inc);
        this.descLoading7 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad13', (img) => {
        this.productForm.descimage7 = img;
        this.descLoading7 = false;
    });
  
    Event.listen('Imgfileupload.descLoading8', () => {
        this.incrementProgress(this.inc);
        this.descLoading8 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad14', (img) => {
        this.productForm.descimage8 = img;
        this.descLoading8 = false;
    });
  
    Event.listen('Imgfileupload.descLoading9', () => {
        this.incrementProgress(this.inc);
        this.descLoading9 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad15', (img) => {
        this.productForm.descimage9 = img;
        this.descLoading9 = false;
    });
  
    Event.listen('Imgfileupload.descLoading10', () => {
        this.incrementProgress(this.inc);
        this.descLoading10 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad16', (img) => {
        this.productForm.descimage10 = img;
        this.descLoading10 = false;
    });
  
    Event.listen('Imgfileupload.descLoading11', () => {
        this.incrementProgress(this.inc);
        this.descLoading11 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad17', (img) => {
        this.productForm.descimage11 = img;
        this.descLoading11 = false;
    });
  
    Event.listen('Imgfileupload.descLoading12', () => {
        this.incrementProgress(this.inc);
        this.descLoading12 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad18', (img) => {
        this.productForm.descimage12 = img;
        this.descLoading12 = false;
    });
  
    Event.listen('Imgfileupload.descLoading13', () => {
        this.incrementProgress(this.inc);
        this.descLoading13 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad19', (img) => {
        this.productForm.descimage13 = img;
        this.descLoading13 = false;
    });
  
    Event.listen('Imgfileupload.descLoading14', () => {
        this.incrementProgress(this.inc);
        this.descLoading14 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad20', (img) => {
        this.productForm.descimage14 = img;
        this.descLoading14 = false;
    });
  
    Event.listen('Imgfileupload.descLoading15', () => {
        this.incrementProgress(this.inc);
        this.descLoading15 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad21', (img) => {
        this.productForm.descimage15 = img;
        this.descLoading15 = false;
    });
  
    Event.listen('Imgfileupload.descLoading16', () => {
        this.incrementProgress(this.inc);
        this.descLoading16 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad22', (img) => {
        this.productForm.descimage16 = img;
        this.descLoading16 = false;
    });
  
    Event.listen('Imgfileupload.descLoading17', () => {
        this.incrementProgress(this.inc);
        this.descLoading17 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad23', (img) => {
        this.productForm.descimage17 = img;
        this.descLoading17 = false;
    });
  
    Event.listen('Imgfileupload.descLoading18', () => {
        this.incrementProgress(this.inc);
        this.descLoading18 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad24', (img) => {
        this.productForm.descimage18 = img;
        this.descLoading18 = false;
    });
  
    Event.listen('Imgfileupload.descLoading19', () => {
        this.incrementProgress(this.inc);
        this.descLoading19 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad25', (img) => {
        this.productForm.descimage19 = img;
        this.descLoading19 = false;
    });
  
    Event.listen('Imgfileupload.descLoading20', () => {
        this.incrementProgress(this.inc);
        this.descLoading20 = true;
    });
    Event.listen('Imgfileupload.profileOnLoad26', (img) => {
        this.productForm.descimage20 = img;
        this.descLoading20 = false;
    });

    Event.listen('Imgfileupload.colorLoading1', () => {
        this.incrementProgress(this.inc);
        this.colorLoading1 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad1', (img) => {
        this.productForm.colorImage1 = img;
        this.colorLoading1 = false;
        $('#colorDiv1 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading2', () => {
        this.incrementProgress(this.inc);
        this.colorLoading2 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad2', (img) => {
        this.productForm.colorImage2 = img;
        this.colorLoading2 = false;
        $('#colorDiv2 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading3', () => {
        this.incrementProgress(this.inc);
        this.colorLoading3 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad3', (img) => {
        this.productForm.colorImage3 = img;
        this.colorLoading3 = false;
        $('#colorDiv3 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading4', () => {
        this.incrementProgress(this.inc);
        this.colorLoading4 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad4', (img) => {
        this.productForm.colorImage4 = img;
        this.colorLoading4 = false;
        $('#colorDiv4 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading5', () => {
        this.incrementProgress(this.inc);
        this.colorLoading5 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad5', (img) => {
        this.productForm.colorImage5 = img;
        this.colorLoading5 = false;
        $('#colorDiv5 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading6', () => {
        this.incrementProgress(this.inc);
        this.colorLoading6 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad6', (img) => {
        this.productForm.colorImage6 = img;
        this.colorLoading6 = false;
        $('#colorDiv6 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading7', () => {
        this.incrementProgress(this.inc);
        this.colorLoading7 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad7', (img) => {
        this.productForm.colorImage7 = img;
        this.colorLoading7 = false;
        $('#colorDiv7 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading8', () => {
        this.incrementProgress(this.inc);
        this.colorLoading8 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad8', (img) => {
        this.productForm.colorImage8 = img;
        this.colorLoading8 = false;
        $('#colorDiv8 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading9', () => {
        this.incrementProgress(this.inc);
        this.colorLoading9 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad9', (img) => {
        this.productForm.colorImage9 = img;
        this.colorLoading9 = false;
        $('#colorDiv9 .colorImageRequired').removeClass('imagerequired');
    });

    Event.listen('Imgfileupload.colorLoading10', () => {
        this.incrementProgress(this.inc);
        this.colorLoading10 = true;
    });
    Event.listen('Imgfileupload.colorOnLoad10', (img) => {
        this.productForm.colorImage10 = img;
        this.colorLoading10 = false;
        $('#colorDiv10 .colorImageRequired').removeClass('imagerequired');
    });
  }
})


$(document).ready(function(){
    $('.img1, .img2, .img3, .img4, .img5, .img6').hide();
    $('.createSpinner').hide();
    $('.draftSpinner').hide();
    $('.ql-script, .ql-image, .ql-video').hide();
});

$('.btnFinishProduct').on('click', function() {
  $(this).prop('disabled', true);
});

$('.btnSaveDraft').on('click', function() {
  $(this).prop('disabled', true);
});

$("#price, #sale_price, #weight, #length, #width, #height").keypress(function (e) {
 //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46)  {
        return false;
    }
});