$(document).ready(function(){

    
    page_open = window.location.search.substr(6);
    
    $('.btnUpload').click(function(){
        $('.avatar-uploader input').click();
    })

    //individual
    $('.btnNbiUpload').click(function(){
        $('.nbi-uploader input').click();
    })
    $('.btnBirthCertUpload').click(function(){
        $('.birthCert-uploader input').click();
    })
    $('.btnGovIdUpload').click(function(){
        $('.govID-uploader input').click();
    })

    //enterprise
    // COMMENTED THIS 10/12/17
    // $('.btnSecUpload').click(function(){
    //     $('.sec-uploader input').click();
    // })
    // $('.btnBirUpload').click(function(){
    //     $('.bir-uploader input').click();
    // })
    // $('.btnPermitUpload').click(function(){
    //     $('.permit-uploader input').click();
    // })

    // $('.btnNbiUpload').click(function(){
    //     $('.nbi-uploader input').click();
    // })



    if(page_open)
    {
        if($('#profile-settings-container #'+page_open).length)
        {

            $('#'+page_open).addClass('active');
            return;
        }
        $('.tab-pane:first').addClass('active');
    }else
    {
        $('.tab-pane:first').addClass('active');
    }

    $('#modalMenu').on('shown.bs.modal', function (e) {
        $('button[data-target="#modalMenu"] span:eq(0)').removeClass().addClass('fa fa-times');
    });

    $('#modalMenu').on('hidden.bs.modal', function (e) {
        $('button[data-target="#modalMenu"] span:eq(0)').removeClass().addClass('fa fa fa-bars');
    });

    $('#modalMenu table tr').on('click',function() {
        $('#modalMenu').modal('hide');
    });

})


var profileMenu = new Vue({
    el: '#profile-menu',
    data:{
        modalMenuShowing:false,
        friends:[],
        requests:[],
        recommended:[],
        selected:[],
        notifications:[],
        profileImage:'',
        is_docs_verified:'',
        lang:[],
        form: new Form({
           avatar:''
        }, { baseURL: 'http://'+Laravel.base_api_url  }),
        param:''     
       

    },
    methods:{
        show:function(event){
            var elShow = event.target.name;
            if(elShow == 'show_product'){
                $('#show_product2').show();
                $('#show_stores2').hide();
                $('#show_friends2').hide();
            }else if(elShow == 'show_stores'){
                $('#show_product2').hide();
                $('#show_stores2').show();
                $('#show_friends2').hide();
            }else if(elShow == 'show_friends'){
                $('#show_product2').hide();
                $('#show_stores2').hide();
                $('#show_friends2').show();
            }
        },
        selectItem(id){
            this.selected.push(id)
        },
        getFriendsData(){
            function getFriend() {
                return axiosAPIv1.get('/friends');
            }
            
            axios.all([
                 getFriend()
            ]).then(axios.spread(
                (
                     data
                ) => {
                this.friends = data.data.friends;
                this.requests = data.data.requests;
            }))
            .catch($.noop);
        },
        getNotifications(){
             function getFriend() {
                return axiosAPIv1.get('/friends');
            }
            
            axios.all([
                 getFriend()
            ]).then(axios.spread(
                (
                     data
                ) => {
                this.friends = data.data.friends;
                this.requests = data.data.requests;
            }))
            .catch($.noop);
        }
    },
    created(){

        this.param = location.search.split('v=')[1];

        $('.modal-body .menu-holder').find('tr').click(function(){
            $('#modalMenu').modal('toggle');
        })
        axios.get('/translate/profile-settings')
            .then(result => {
            this.lang = result.data;
        });
       this.getFriendsData();

       this.profileImage = Laravel.user.avatar;
       this.is_docs_verified = Laravel.user.is_docs_verified;
       Event.listen('Imgfileupload.avatarUpload', (img) => {
                this.profileImage = img;
            
        });


       let title_status = this.is_docs_verified?"Verified User":"Not yet Verified";
       let class_status = this.is_docs_verified?"tc-green fa-check-circle":"text-warning fa-exclamation-circle";
       $('#avatar-container span').attr('data-original-title',title_status);
       $('#avatar-container span span').attr('class',"mouse-hand  fa fa-2x "+ class_status);

       
    },

});

var dataContainer = new Vue({
    el: '#data-area',
    data: {
        ewallet:0,
        storeCount:0,
        wishCount:0
    },
    mounted(){

        function getEwallet() {
            return axiosAPIv1.get('/user/'+Laravel.user.id+'/ewallet');
        }
        function getStores() {
            return axiosAPIv1.get('/users/'+Laravel.user.id+'/stores');
        }
        function getWishlist() {
            return axiosAPIv1.get('/getfavoritestotal');
        }

        axios.all([
             getEwallet(),
             getStores(),
             getWishlist()
        ]).then(axios.spread(
            (
                 ewallet,
                 stores,
                 wishlist
            ) => {

            this.ewallet = parseFloat(ewallet.data.data.amount);
            this.storeCount = stores.data.length;
            this.wishCount = wishlist.data;
        }))
        .catch($.noop);
    }
})


// Vue.component('product-item',  require('../components/store/management/Product.vue'));


// Vue.filter('round', function (value) {
//     return  roundOff(value);
// });

// var app = new Vue({
//   el: '#profile-container',
//   data: {
//     friends:[
//       {
//         id:1,
//         name:'Anna',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:2,
//         name:'Annie',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:3,
//         name:'John',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:4,
//         name:'Johnnie',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       }
//     ],
//     requests:[
//       {
//         id:5,
//         name:'Juanito',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:6,
//         name:'Juanita',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       }
//     ],
//     recommended:[
//       {
//         id:7,
//         name:'Charles',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:8,
//         name:'Arianne',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:9,
//         name:'Aries',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:10,
//         name:'Carl',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:11,
//         name:'Kevin',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       },
//       {
//         id:12,
//         name:'Charlie',
//         image:'https://images.gamurs.com/user/avatar/346eb834-b6d3-4574-b11f-6a76c44d050d.png'
//       }
//     ],
//     showRequest:true,
//     showFriends:false,
//     showRecommended:false,
//     items:[
//       {
//         id: 1
//         ,name: 'PhosphorusGrey Melange Printed V Neck T-Shirt'
//         ,image: '/img/demo/vneck1.jpg'
//         ,price:3000
//         ,sale_price:2000
//       },
//       {
//         id: 2
//         ,name: 'United Colors of BenettonNavy Blue Solid V Neck T Shirt'
//         ,image: '/img/demo/vneck2.jpg'
//         ,price:2000
//         ,sale_price:1500
//       },
//       {
//         id: 3
//         ,name: 'WranglerBlack V Neck T Shirt'
//         ,image: '/img/demo/vneck3.jpg'
//         ,price:1850
//         ,sale_price:1500
//       },
//       {
//         id: 4
//         ,name: 'Tagd New YorkGrey Printed V Neck T-Shirts'
//         ,image: '/img/demo/vneck4.jpg'
//         ,price:2050
//         ,sale_price:1699
//       },
//       {
//          id: 5
//         ,name: 'Penshoppe Polo Shirt'
//         ,image: '/img/demo/polo2.jpg'
//         ,price:1999
//         ,sale_price:999
//       },
//     ],
//     selected:[]
//   },
//   methods:{
//     show:function(event){
//       var elShow = event.target.name;
//       if(elShow == 'show_product'){
//         $('#show_product2').show();
//         $('#show_stores2').hide();
//         $('#show_friends2').hide();
//       }else if(elShow == 'show_stores'){
//         $('#show_product2').hide();
//         $('#show_stores2').show();
//         $('#show_friends2').hide();
//       }else if(elShow == 'show_friends'){
//         $('#show_product2').hide();
//         $('#show_stores2').hide();
//         $('#show_friends2').show();
//       }
//     },
//     selectItem(id){
//       this.selected.push(id)
//     },

//     clkShowRequest: function(){
//       this.showRequest = true;
//       this.showFriends = false;
//       this.showRecommended = false;
//     },
//     clkShowFriends: function(){
//       this.showRequest = false;
//       this.showFriends = true;
//       this.showRecommended = false;
//     },
//     clkShowRecommended: function(){
//       this.showRequest = false;
//       this.showFriends = false;
//       this.showRecommended = true;
//     },
//   }
// })

// var friends = new Vue({
//   el: '#friends_container',
//   data: {},
//   methods:{}
// })



// function numberWithCommas(x) {
//     return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }

// function roundOff(v) {
//     return Math.round(v);
// }

