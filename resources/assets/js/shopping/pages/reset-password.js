var app = new Vue({
  el: '.reset-password-container',
  data: {
    question:[],
    userid:0,
    email:'',
    lang:[],
    form: new Form({
       primary_email:'',
       email2:'',
       answer:''
    },{ baseURL: 'http://'+Laravel.base_api_url })
  },

  created(){
    axios.get('/translate/passwords')
      .then(lang => {
        this.lang = lang.data.data;
      })
  },

  methods:{
    searhEmail(){
      this.form.submit('post', '/v1/searchEmail')
      .then(result => {
        if(result.response==1){
          $('#search, #email2-cont').hide();

          this.userid = result.id;
          this.email = $('#email2').val();
          this.form.email2 = '';
          this.form.primary_email = $('#email2').val();

          axiosAPIv1.get('/getQuestion/'+result.id)
          .then(result => {
            this.question = result.data;
            if(this.question.length==0){
              $('#l-email').text(this.lang['alt-email']);
              $('#email2-cont, #search-alt').show();           
            }
            else
              $('.answer-cont').show(); 
          });

        } else {
          toastr.error(this.lang['email-error-msg']);
        }
      });
    },
    searhAltEmail(){
      this.form.submit('post', '/v1/searchAltEmail')
      .then(result => {
        if(result.response==1){
          $('#question-cont').hide();
          $('#success-cont').show();
        } else if (result.response==3){
          toastr.error(this.lang['please-set-alt-email']);
        } else {
          toastr.error(this.lang['email-error-msg']);
        }
      });
    },
    answerQuestion(){
      var val = $("#question option:selected").val();
      
      if(val==0){
        toastr.error(this.lang['question-error-msg']);
      } else {
        axiosAPIv1.get('/answerQuestion/'+val+'/'+this.userid,{
            params: {
              answer:$('#answer').val()
            }
        })
        .then(result => {
          if(result.data.response==1){
            $('#question-cont').hide();
            $('#success-cont').show();
          } else {
            toastr.error(this.lang['answer-error-msg']);
          }
          
        });       
      }

    }
  }
});


