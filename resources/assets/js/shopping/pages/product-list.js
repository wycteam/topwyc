Vue.component('product-item',  require('../components/product/Product.vue'));

Vue.filter('currency', function (value) {
    return  numberWithCommas(value.toFixed(2));
});

Vue.filter('round', function (value) {
    return  roundOff(value);
});

var app = new Vue({
	el: '#product-list',
	data: {
        pagination: {
            total: 0,
            per_page: 7,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,// left and right padding from the pagination <span>,just change it to see effects
		items:[],
		selected:[],
		pName:true,
		lang:[],
		keyword:'',
		langmode:null
	},
	created(){
		var m = $("meta[name=locale-lang]");    
		this.langmode = m.attr("content");
		//check if there is a search query
		if(window.location.search.substr().indexOf('search') > -1)
		{
			function getTranslation() {
            	return axios.get('/translate/cart');
        	}
			function getProducts() {
				return axiosAPIv1.post('/search/product',{
					keyword:getURLParameter('search'),
					sort:getURLParameter('price'),
					prod_name:getURLParameter('prod_name'),
					minPrice: getURLParameter('min'),
					maxPrice: getURLParameter('max'),
					brands: getURLParameter('brands'),
					type: getURLParameter('type')
				});
			}
			
	    	axios.all([
	    		getTranslation(),
				getProducts()
			]).then(axios.spread(
				(
					translation,
					response
				) => {

					setPriceRangeMax(response.data.maxPrice);

					this.lang = translation.data.data;
					
					if(response.data){
						$('.loading').remove();
						this.items = response.data.data.data;
					}
					
					if(response.data.pagination.total > 20){
						this.pagination = response.data.pagination;
					}

					//show no found result 
					if(this.items == ''){
					     $('.cd-fail-message').show();
					}

					if(response.data.queryString != '') {
						$('#resultMessage').removeClass('hide');
						$('#resultMessage #queryString').text('"' + response.data.queryString + '"');
					} else {
						$('#resultMessage').addClass('hide');
						$('#resultMessage #queryString').text('');
					}

			}))
			.catch($.noop);

			var price = getURLParameter('price');
			
			if(price){
				if(price==='desc')
					$('#radio2').attr('checked','checked');

				if(price==='asc')
					$('#radio3').attr('checked','checked');	
			}
			else{
				$('#radio1').attr('checked','checked');				
			}
		}

		// check if results is filtered
		this.checkIfResultsIsFiltered();
	},
    computed: {
        isActived() {
            return this.pagination.current_page;
        },
        pagesNumber() {
            if (!this.pagination.to) {
                return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
       
    },
	methods:{
		selectItem(id){
			this.selected.push(id)
		},
		sortBy(val){
			var url = removeURLParam(window.location.href, 'price');
			if(val=="desc"){
				//var url = window.location.href.replace('&price=asc','')
				window.location = url + '&price=desc';
			}

			if(val=="asc"){
				//var url = window.location.href.replace('&price=desc','')
				window.location = url + '&price=asc';
			}

			if(val=="rel"){
				var url = window.location.href;
				window.location = this.removeURLParameter(url,'price');
			}			
		},
		sortByBrand(e) {
			var url = window.location.href;
			var brands = getURLParameter('brands');

			if(brands == undefined) {
				window.location = url + '&brands=' + e.target.value;
			} else {
				if(e.target.checked) {
					var url = removeURLParam(url,'brands');

					window.location = url + '&brands=' + brands + '--' + e.target.value;
				} else {
					if(url.indexOf('--'+e.target.value) != -1) {
						var url = url.replace('--'+e.target.value, "");
					} else {
						var url = url.replace(e.target.value, "");
					}
					
					window.location = url;
				}
			}
		},
		removeURLParameter(url, parameter) {
		    //prefer to use l.search if you have a location/link object
		    var urlparts= url.split('?');   
		    if (urlparts.length>=2) {

		        var prefix= encodeURIComponent(parameter)+'=';
		        var pars= urlparts[1].split(/[&;]/g);

		        //reverse iteration as may be destructive
		        for (var i= pars.length; i-- > 0;) {    
		            //idiom for string.startsWith
		            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
		                pars.splice(i, 1);
		            }
		        }

		        url= urlparts[0]+'?'+pars.join('&');
		        return url;
		    } else {
		        return url;
		    }
		},
	    handleEnter: function (e) {
		    if (e.keyCode == 13) {
		    	var url = removeURLParam(window.location.href, 'prod_name');
				window.location =  url + '&prod_name=' + this.keyword;		    
			}
		},
        addToCart(id,name){
	        axios.get(location.origin + '/cart/add',{
	            params: {
	                prod_id:id,
	                qty:1
	            }
	        })
	        .then(result => {
	            $("#cart-count, #cart-count-mobile").text(result.data.cart_item_count);
                if(result.data.error_msg)
                {	
                	if(result.data.error_msg=='Redirect')
                		window.location = '/product/' + result.data.slug;
                	else
	                	toastr.error(result.data.error_msg, this.lang['not-added']);
                } else {
	                toastr.success(this.lang['added-cart'], name);
                }
            });
        },
        addToCompare(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtocompare/' + id)
                .then(result => {
                    if(result.data.response==1){
		                var comp = parseInt($("#comp-count").text())+1;
		                $("#comp-count").text(comp).show();
		                toastr.success(this.lang['added-comparison'], name);
		            } else if(result.data.response==3) {
		             	toastr.error(this.lang['full']);
                    } else {
                    	toastr.info(this.lang['already-compare'], name);
                    }
                });
            } else {
                toastr.error(this.lang['login-compare']);
            }
        },
        addToFavorites(id,name){
            if(Laravel.isAuthenticated === true){
                axiosAPIv1.get('/addtofavorites/' + id)
                .then(result => {
                    if(result.data.response==1){
		                var fav = parseInt($("#fav-count").text())+1;
		                $("#fav-count").text(fav).show();
		                toastr.success(this.lang['added-favorites'], name);
		            } else if(result.data.response==2){
                    	toastr.error(result.data.error_msg, this.lang['not-added-wishlist']);
                    } else {
                    	toastr.info(this.lang['already-favorites'], name);
                    }
                });
            } else {
                toastr.error(this.lang['login-favorites']);
            }
        },
        fetchItems(page) {
			axiosAPIv1.post('/search/product',{
				page: page,
				keyword:getURLParameter('search'),
				sort:getURLParameter('price'),
				prod_name:getURLParameter('prod_name'),
				minPrice: getURLParameter('min'),
				maxPrice: getURLParameter('max'),
				brands: getURLParameter('brands')
			})      
			.then(result => {
				setPriceRangeMax(result.data.maxPrice);

				this.items = result.data.data.data;
				this.pagination = result.data.pagination;
	        });
        },
	    changePage(page) {
	        this.pagination.current_page = page;
	        this.fetchItems(page);
	    },
	    showPagination(){
	    	 $('#pagination').show();
	    },
	    hidePagination(){
	    	 $('#pagination').hide();
	    },
	    checkIfResultsIsFiltered() {
	    	var search = getURLParameter('search');
	    	var type = getURLParameter('type');

	    	var price = getURLParameter('price');

	    	var min = getURLParameter('min');
	    	var max = getURLParameter('max');

	    	if(
	    		(search != undefined && type != undefined)
	    		|| price != undefined
	    		|| (min != undefined && max != undefined)
	    	) {
	    		var elementsToTrigger = $([$('.cd-filter-trigger'), $('.cd-filter'), $('.cd-tab-filter'), $('.cd-gallery')]);
				elementsToTrigger.each(function(){
					$(this).toggleClass('filter-is-visible', true);
				});		    	
	    	}
	    }
	},
	watch: {
		item: function (key) {
			$('.cd-gallery ul').mixItUp({
			    controls: {
			    	enable: false
			    },
			    callbacks: {
			    	onMixStart: function(){
			    		$('.cd-fail-message').fadeOut(200);
			    	},
			      	onMixFail: function(){
			      		$('.cd-fail-message').fadeIn(200);
			    	}
			    }
			});
		}
	},
	updated() {
		// $('.cd-gallery ul').mixItUp({
		// 	    controls: {
		// 	    	enable: false
		// 	    },
		// 	    callbacks: {
		// 	    	onMixStart: function(){
		// 	    		$('.cd-fail-message').hide();
		// 	    	},
		// 	      	onMixFail: function(){
		// 	      		$('.cd-fail-message').show();
		// 	    	}
		// 	    }
		// 	});
	}
})

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
    return Math.round(v);
}

function getURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

function removeURLParam(url, param)
{
	 var urlparts= url.split('?');
	 if (urlparts.length>=2)
	 {
	  var prefix= encodeURIComponent(param)+'=';
	  var pars= urlparts[1].split(/[&;]/g);
	  for (var i=pars.length; i-- > 0;)
	   if (pars[i].indexOf(prefix, 0)==0)
	    pars.splice(i, 1);
	  if (pars.length > 0)
	   return urlparts[0]+'?'+pars.join('&');
	  else
	   return urlparts[0];
	 }
	 else
	  return url;
}

function setPriceRangeMax(maxPrice) {	
	var url_min = getURLParameter('min');
	var url_max = getURLParameter('max');

	if(url_min != undefined && url_max != undefined) {
		from = url_min;
		to = url_max;
	} else {
		from = 0;
		to = maxPrice;
	}

	window.slider.update({
		min: 0,
		from: from,

		max: maxPrice,
		to: to
	});
}
