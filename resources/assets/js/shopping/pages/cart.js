Vue.component('cart-item',  require('../components/cart/CartItem.vue'));
Vue.component('cart-table',  require('../components/cart/CartList.vue'));
Vue.component('order-table',  require('../components/cart/OrderList.vue'));
Vue.component('dialog-modal',  require('../components/DialogModal.vue'));
Vue.component('modal',  require('../components/Modal.vue'));

Vue.filter('currency', function (value) {
    return  '₱'+numberWithCommas(parseFloat(value).toFixed(2));
});

var app = new Vue({
	el: '#cart_container',
	data: {
		items:[],
		selected:[],
		select: '',
		rowId:[],
		dup_items:[],
		prod_id:[],
		qty:[],
		showUndo:false,
		item_names:'',
		optionType:[],
		selectedType:5,
		ewallet:0,
		order_id:0,
		name:'',
		address:'',
		shipping:0,
		discountedPrice:0,
		discount:0,
		total:0,
		orders:[],
		lang:[],
		rates:[],
		Prov: [],
		optionProv: [],
		optionCity: [],
		city: null,
		//checkout
		current_step_name:'Shipping Address',
		current_step:1,
		reached_step:1,
		address_set:false,
	    form: new Form({
	       type_id:'',
	       type: null,
	       name: null,
	       number: null,
	       address: null,
	       //barangay: null,
	       city: '',
	       city_id: null,
	       province: null,
	       province_id: null,
	       zip_code: null,
	       landmark: null,
	       notes: null,
	       alternate_number: null,
	       subtotal: 0,
	       total: 0,
		   discountedPrice:0,
		   discount:0,
	       tax: 0,	       
	       shipping: 0,
	       charge: 0,
	       discountPerSeller:[]
	    },{ baseURL: '/cart'}),

	},

	components: {
	  	Multiselect: window.VueMultiselect.default
	},

	created(){
		function getTranslation() {
            return axios.get('/translate/cart');
        }

        function getCart() {
            return axios.get(location.origin + '/cart/view'); 
        }

		function getType() {
			if(Laravel.user)
			return axios.get(location.origin + '/cart/getType');
		}
		
		function getActive() {
			if(Laravel.user)
			return axios.get(location.origin + '/cart/getActive');
		}

		function getUsableBalance() {
			if(Laravel.user)
			return axiosAPIv1.get('/getUsable');
		}

		function getRates() {
            return axios.get(location.origin + '/getRates');
        }

		axios.all([
			getTranslation(),
			getCart(),
			getType(),
			getActive(),
			getUsableBalance(),
			getRates()
		]).then(axios.spread(
			(
				translation,
				cart,
				type,
				active,
				balance,
				rates
			) => {
				this.rates = rates;
				this.lang = translation.data.data;
				var cart2 = $.map(cart.data.cart, function(value, index) {
				    return [value];
				});
				setTimeout(function(){$.material.init();}, 100);
				this.items = cart2;

				var newtype = {"id": 0, "type": this.lang['new-profile'], "is_active": false};
				var type = type.data;
				type.push(newtype);
				this.optionType = type;

				if(active.data.length !== 0) {

					this.selectedType = active.data[0].id;

					axios.get(location.origin + '/cart/getAddressDetails/' + this.selectedType)
					.then(result => {
						var address = result.data[0].address;
						this.form.type_id = this.selectedType;
						this.form.type = result.data[0].type;
						this.form.name = result.data[0].name_of_receiver;
						this.form.number = result.data[0].contact_number;
						this.form.address = address.replace(/(\r\n|\n|\r)/gm,"");
						//this.form.barangay = result.data[0].barangay;
						this.form.city = result.data[0].city;
						//console.log(result.data[0].province);
						this.form.province = result.data[0].province;
						this.form.zip_code = result.data[0].zip_code;
						this.form.landmark = result.data[0].landmark;
						this.form.notes = '';
						this.form.alternate_number = result.data[0].alternate_contact_number;

						axios.get('/profile/city/'+this.form.province)
				            .then(result => {
				            this.optionCity = result.data;
				            var cityObj = this.optionCity.filter(obj => { return this.form.city == obj.name })
				            this.city = cityObj[0];

				        });
			        });		


				} else {
					this.selectedType = 0;
				}
				axios.get('/profile/provinces')
			      .then(result => {
			        var array = [];
			        var array2 = [];
			        for(var i = 0; i < result.data.length; i++){
			          array.push(result.data[i].name);
			          array2.push(result.data[i]);
			        }
			        this.optionProv = array;
			        this.Prov = array2;
			    });
				this.ewallet = balance.data.success? parseFloat(balance.data.data.usable):0;
		}))
		.catch($.noop);
	},

	methods:{
        selectItem(index,item){
            if(this.selected.indexOf(item)>-1)
            {
                indx = this.selected.indexOf(item); // remove from selected
                this.selected.splice(indx, 1);
                //this.select = (this.select).replace(index+"-", "");
            }else
            {
                this.selected.push(item); // add to selected
                //this.select = this.select + index + '-';
            }
        },
		showRemoveDialog(){
			if(this.selected.length)
			{
				$('#dialogRemove').modal('toggle');
			}
		},
		showCheckoutModal(){
			if(Laravel.user){
				if(this.selected != 0)
					$('#modalCheckout').modal('toggle');
				else
					toastr.error(this.lang['product-buy']);
			} else {
		    	toastr.error(this.lang['you-need']);
			}
		},
		//checkout
		seeStepAddress(){
			if(this.items != 0){
			this.current_step = 1;
			this.current_step_name = 'Shipping Address';
			}
		},
		moveToWishlist(id,rowId){
			if(Laravel.isAuthenticated === true){
			var index = this.items.findIndex( (el) => el.id === id );
			axios.get(location.origin + '/cart/delete',{
			    params: {
			    	row_id: rowId
			    }
			})
			.then(result => {
				this.items.splice(index, 1);
				axiosAPIv1.get('/addtofavorites/' + id)
                .then(result => {
	                var fav = parseInt($("#fav-count").text())+1;
	                $("#fav-count").text(fav).show();
					toastr.success(this.lang['added-favorites']);
                });
			});
			} else {
				toastr.error(this.lang['you-need']);
			}
		},
		removeItems(id,name,rowId){
			var index = this.items.findIndex( (el) => el.id === id );
			axios.get(location.origin + '/cart/delete',{
			    params: {
			    	row_id: rowId
			    }
			})
			.then(result => {
				this.items.splice(index, 1);
				toastr.success(this.lang['removed'],name);
			});
		},
		saveProfile(){
	       this.form.submit('post', '/saveProfile')
	          .then(result => {
				if(result.response!=0){
					toastr.success(this.lang['profile-saved']);
					axios.get(location.origin + '/cart/getType')
					.then(result => {
						var newtype = {"id": 0, "type": "New Profile", "is_active": false};
						var type = result.data;
						type.push(newtype);
						this.optionType = type;
						if($("#address_profile").val()==0){
							this.form.type = '';
							this.form.name = '';
							this.form.number = '';
							this.form.address = '';
							//this.form.barangay = '';
							this.form.city = '';
							this.form.province = '';
							this.form.zip_code = '';
							this.form.landmark = '';
							this.form.notes = '';
							this.form.alternate_number = '';							
						}
			        });					
				}
				else{
	           		toastr.error(result.message);
	           	}
	          })
	          .catch(error => {
	           	toastr.error(this.lang['please']);
	          });
		},
		
		deleteProfile(){
	       this.form.submit('post', '/deleteProfile')
	          .then(result => {
				if(result.response!=0){
					toastr.success(this.lang['profile-deleted']);
					axios.get(location.origin + '/cart/getType')
					.then(result => {
						var newtype = {"id": 0, "type": "New Profile", "is_active": true};
						var type = result.data;
						type.push(newtype);
						this.optionType = type;
						this.selectedType = 0;
						this.form.type = '';
						this.form.name = '';
						this.form.number = '';
						this.form.address = '';
						//this.form.barangay = '';
						this.form.city = '';
						this.form.province = '';
						this.form.zip_code = '';
						this.form.landmark = '';
						this.form.notes = '';
						this.form.alternate_number = '';
			        });					
				}
				else{
	           		toastr.error(result.message);
	           	}
	          })
	          .catch(error => {
	           	toastr.error(this.lang['currently'],this.lang['cant-delete']);	
	          });
		},
		placeOrder(){  
		   if(this.selected != 0){
		   this.form.province_id = this.getProvinceID
      	   this.form.city = this.city.name;
           this.form.city_id = this.city.id;
	       this.form.submit('post', '/checkout')
	          .then(result => {
				if(result.response!=0){
					this.ewallet = result.ewallet_bal;
					this.order_id = result.id;
					this.name = result.name;
					this.address = result.address;
					this.shipping = this.form.shipping;
					this.total = result.subtotal;
					this.discount = result.discount;
					this.discountedPrice = result.total;
					this.orders = this.selected;
	            	this.current_step = 2;
					this.current_step_name = 'Order Summary';	
					this.selected = [];
					this.dup_items = [];

					// var arr = this.select.split("-");
					// for(var i=0; i<arr.length; i++)
					// {	
					// 	if(arr[i]!="")
					// 		this.items.splice(arr[i], 1);
					// }

		            for(i=0; i<this.orders.length; i++)
		            {    
		                for(j=0; j<this.items.length; j++)
		                {
		                    if(this.items[j].rowId == this.orders[i].rowId)
		                    {
		                        this.rowId.push(this.items[j].rowId);
		                        this.items.splice(j,1);                        
		                    }
		                }
		            }


		            axios.get(location.origin + '/cart/delete',{
		                params: {
		                    row_id: this.rowId
		                }
		            });

		            this.rowId = [];
		        	this.select = [];
		        	$('input:checkbox').prop('checked',false);

					toastr.success(this.lang['order-placed'], this.lang['order-no'] + this.order_id);
					$("#urf").hide();			
				}
				else{
					$("#placeOrder").prop('disabled', false);
	           		toastr.error(result.message);
	           	}
	          })
	          .catch(error => {
				this.current_step = 1;
				this.current_step_name = 'Shipping Address';
				$("#placeOrder").prop('disabled', false);
	           	toastr.error(this.lang['please']);	
	          });
	       }
		},
		updateQty(qty,rowId){
			axios.get(location.origin + '/cart/updateQty',{
			    params: {
			    	row_id:rowId,
					qty:qty
			    }
			});
		},
		stock(id,qty,stock,rowId){
			if(parseInt(stock)==0){
				axios.get(location.origin + '/cart/updateQty',{
				    params: {
				    	row_id:rowId,
						qty:1
				    }
				});
			}
			if(parseInt(qty)>parseInt(stock) && parseInt(stock)!=0){
				$('#q'+id).val(stock);
				axios.get(location.origin + '/cart/updateQty',{
				    params: {
				    	row_id:rowId,
						qty:stock
				    }
				});
			}
			if(parseInt(qty)<=parseInt(stock)){
				axios.get(location.origin + '/cart/updateQty',{
				    params: {
				    	row_id:rowId,
						qty:qty
				    }
				});
			}
			if(qty=='' || qty=='0'){
				$('#q'+id).val(1);
				axios.get(location.origin + '/cart/updateQty',{
				    params: {
				    	row_id:rowId,
						qty:1
				    }
				});
			}
		},

		getAddressDetails(event){
			if(event.target.value === '0'){
				this.form.type_id = event.target.value;
				this.form.type = '';
				this.form.name = '';
				this.form.number = '';
				this.form.address = '';
				this.form.barangay = '';
				this.form.city = '';
				this.form.province = '';
				this.form.zip_code = '';
				this.form.landmark = '';
				this.form.notes = '';
				this.form.alternate_number = '';
			} else {
			axios.get(location.origin + '/cart/getAddressDetails/' + event.target.value)
			.then(result => {
				var address = result.data[0].address;
				var number = '';
				if(Laravel.user.numbers.length !== 0) number = Laravel.user.numbers[0].number 
				else number = '';
				this.form.type_id = event.target.value;
				this.form.type = result.data[0].type;
				this.form.name = result.data[0].name_of_receiver;
				this.form.number = result.data[0].contact_number;
				this.form.address = address.replace(/(\r\n|\n|\r)/gm,"");
				this.form.barangay = result.data[0].barangay;
				this.form.city = result.data[0].city;
				this.form.province = result.data[0].province;
				this.form.zip_code = result.data[0].zip_code;
				this.form.landmark = result.data[0].landmark;
				this.form.notes = '';
				this.form.alternate_number = result.data[0].alternate_contact_number;
	        });
			}
		},

	    getCity:function(data){
	      this.form.city = '';
	      axios.get('/profile/city/'+data)
	        .then(result => {
	        var array = [];
	        for(var i = 0; i < result.data.length; i++){
	        
	          array.push(result.data[i]);
	        }
	        this.optionCity = array;
	      });
	    },

		print(){
			var html_container = '';
			html_container = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns:fb="http://ogp.me/ns/fb#"><head>';
			html_container += '<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons" />';
			html_container += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>';	
			html_container += '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"><meta name="apple-mobile-web-app-capable" content="yes">';
			html_container += '<style>#cart_container .checkout-steps-container #total td{text-align:right;padding-left:35px}#cart_container .checkout-steps-container .e-wallet{text-align:center;background-color:#e6e6e6;width:150px;border:2px solid #009688;height:85px}#cart_container .checkout-steps-container .e-wallet .titles{margin-top:15px}#cart_container .checkout-steps-container .e-wallet .balance{font-weight:700}#cart_container .checkout-steps-container .float{float:right}</style>';
			html_container += '</head><body><div id="cart_container" class="container"><div class="checkout-steps-container checkout-step-summary"><img src="/shopping/img/general/new-logo.png">';

			var body = $("#print").html();
			var myWindow = window.open('','','width=500,height=500,scrollbars=yes,location=no');
			myWindow.document.write(html_container);
			myWindow.document.write(body);
			myWindow.document.write("</div></div></body></html>");
			myWindow.document.close();
			
			setTimeout(function(){
			myWindow.focus();
			myWindow.print();		
			}, 3000);
		},

		reset(){
			this.current_step = 1;
			this.orders = [];
			$('#placeOrder').removeAttr("disabled");
		}
	},

	computed:{
		total_price(){
			sum = 0;
			shipping = 0;
			charge = 0;
			shippingAndCharge = 0;
			vat = 0;
			shipping2 = 0;
			charge2 = 0;
			vat2 = 0;			
			for(var i=0; i<this.selected.length; i++)
			{
				var salePrice = this.selected[i].options.sale_price;
				sum += salePrice.replace(',', '') * this.selected[i].qty;
				charge += this.selected[i].options.charge * this.selected[i].qty;
				shippingAndCharge += this.selected[i].options.shipping_fee * this.selected[i].qty + this.selected[i].options.charge * this.selected[i].qty;
				vat += this.selected[i].options.vat * this.selected[i].qty; 
				shipping += this.selected[i].options.shipping_fee * this.selected[i].qty;
				charge2 += this.selected[i].options.charge2 * this.selected[i].qty;
				vat2 += this.selected[i].options.vat2 * this.selected[i].qty;
				shipping2 += this.selected[i].options.shipping_fee2 * this.selected[i].qty; 
			}
			this.form.subtotal = sum;
			this.form.total = sum + shippingAndCharge + vat;
			this.form.tax = vat2;
			this.form.shipping = shipping2;
			this.form.charge = charge2;

			return this.form.total;
		},

		discounted_price(){
			var groupArray = require('group-array');
			var group = groupArray(this.selected, 'options.store');
			//console.log(this.items);
			var weight = 0;
			var volume = 0;
			var charge = 0;
			var totalChargeWeight = 0;
			var productPrice = 0;
			var salePrice = 0;
			var totalWeight = 0;
			var chargeWeight = 0;
			var total = 0;
			var totalSalePrice = 0;
			var discount = 0;
			var t = 0;
			var disc = [];
			var vm = this;
			$.each( group, function( key, value ) {
				//console.log(value.length);
				if(value.length>0){
					//console.log(value);
					totalChargeWeight = 0;
					productPrice = 0;
					salePrice = 0;
					totalSalePrice = 0;
					discount = 0;
					for(var i=0; i<value.length; i++){
						weight = value[i].options.weight;
						volume = (value[i].options.length*value[i].options.width*value[i].options.height)/3500;
						charge += parseFloat(value[i].options.charge2);
						if(weight>volume) chargeWeight = weight;
						if(volume>weight) chargeWeight = volume;
						totalChargeWeight+= (parseFloat(chargeWeight)*parseFloat(value[i].qty));
						productPrice += ((value[i].options.sale_price2 != null ? parseFloat(value[i].options.sale_price2) : parseFloat(value[i].price))*parseFloat(value[i].qty));
						var sp = value[i].options.sale_price;
						salePrice += ((parseFloat(sp.replace(',', '')))*parseFloat(value[i].qty));
					}
					//console.log(productPrice);
					totalWeight = totalChargeWeight * 1.2; //add 20% to total weight
					//console.log(totalWeight);
					let rateCat = "SAMM";
					let halfkg = 0;
					let fkg = 0;
					let excess = 0;
					let rate = 0;
					// rate computation
					var rates = vm.rates.data.rates;
					//console.log(rates);
					$.each(rates, function(i, item) {
					  if(rates[i].rate_category == rateCat){
					    halfkg = rates[i].halfkg;
					    fkg = rates[i].fkg;
					    excess = rates[i].excess;
					  }
					});

	                if(totalWeight <= 0.5){
	                  rate = halfkg;
	                }
	                else if(totalWeight >0.5 && totalWeight <=1){
	                  rate = fkg;
	                }
	                else{
	                  totalWeight -=1;
	                  rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
	                }
	                //console.log('rate b4 '+rate);
					rate = parseFloat(rate) + 20; // add 20 for markup
		            //console.log("rate -> "+Math.ceil(rate));

		            //valuation charge computation
		            let vc = (charge > 0 ? productPrice * 0.01 : 0); 
		            //console.log("vc -> "+Math.ceil(vc));

		            //tax computation - docu stamp
		            let vtx = 0;
		            if(productPrice > 0 && productPrice <=1000){
		                vtx = ((rate -1)+vc) * .12;
		              }
		            else{
		                vtx = ((rate -10)+vc) * .12;
		            }
		            //console.log("tax -> "+Math.ceil(vtx));

		            //total fee
		            let sFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);		

					//console.log(sFee);

					total = productPrice + sFee;
					t += total;

					totalSalePrice += salePrice;
					discount = totalSalePrice - total;
					//console.log('discount-'+discount);

					var seller = {"seller_id": value[0].options.seller_id, "amount": discount};
					disc.push(seller);
					vm.form.discountPerSeller = disc;
					
				} 
				// else {

				// 	for(var i=0; i<value.length; i++){
				// 		var sp = parseFloat(value[i].options.sale_price.replace(',', '')) * parseFloat(value[i].qty);
				// 		t += parseFloat(sp);
				// 	}
					 
				// }

			});
			//console.log(t);
			//console.log(this.form.discountPerSeller);
			this.form.discountedPrice = t;

			return this.form.discountedPrice;
		},

		discounts(){

			this.form.discount = this.total_price-this.discounted_price;

			return this.form.discount;
		},

		getProvinceID: function(){
	        let array = this.Prov;
	        let key = 'name';
	        let value = this.form.province;
	        for (var i = 0; i < array.length; i++) {
	                if (array[i][key] === value) {
	                    return array[i]['id'];
	                }
	            }
	        return null;
	    },

	},

	mounted(){
		$('#modalCheckout').on("hidden.bs.modal", this.reset)
	}
});

$('#placeOrder').on('click', function() {
	$(this).prop('disabled', true);
});

$("#number, #zip_code, #alternate_number").keypress(function (e) {
 //if the letter is not digit then display error and don't type anything
 	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    	return false;
	}
});

/*$('#number, #alternate_number').inputmask({
mask: '(9999) 999-9999'
})*/

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}