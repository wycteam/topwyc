let data = window.Laravel.data;
var current_product = 'Report product: '+data.name;

Vue.component('modal',  require('../components/Modal.vue'));

new Vue({
    el: '#product-report',

    data: {
        lang:[],
        form: new Form({
            subject: current_product,
            body: null
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        pform: new Form({
            issue_id: null,
            product_id: data.id,
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        newConvoForm: new Form({
            supportIds: [],
            flag: true
        }, { baseURL: axiosAPIv1.defaults.baseURL }),
        notif : []
    },
    created(){
        this.translate();
        this.getSupports();
    },
    methods: {
        translate() {
            return axios.get('/translate/products')
              .then(lang => {
                this.lang = lang.data.data;
            });
        },

        //3 default support
        getSupports() {
            return axiosAPIv1.get('/getSupports')
                .then(result => {
                    this.supports = result.data;                  
                });
        },

        submitReport() {
            this.$validator.validateAll();

            if (! this.errors.any()) {
                this.form.submit('post', '/issues')
                    .then(result => {
                        this.$validator.clean();

                        this.pform.issue_id = result.data.id;

                        var offline = 0;
                        for(var i=0; i<this.supports.length; i++)
                        {
                            if(!this.supports[i].is_online)
                                offline = offline + 1;
                        }

                        var supp = 0;
                        if(offline!=this.supports.length){
                            for(var i=0; i<this.supports.length; i++)
                            {
                                if(this.supports[i].is_online)
                                    var supp = this.supports[i].id;
                            }
                        }

                        let casenum = result.data.issue_number;

                        //if(offline!=this.supports.length)
                        if(supp==0) supp = 2; // mam cha's account
                        this.newConvoForm.supportIds.push(supp);
                        this.newConvoForm.submit('put', '/customer-service/' + result.data.id)
                        .then(result => {
                            //alert(result.data.chat_room.name);
                            this.notif = result.data;
                            this.form.body = null;
                            this.newConvoForm.supportIds = [];

                            //open chat box with case number as title for the conversation
                            var chat = [];
                            $(".panel-chat-box").each(function(){
                                chat.push($(this).attr('id'));
                            });

                            if (casenum) {
                                if($.inArray('user'+casenum,chat)=='-1'){
                                    Event.fire('chat-box.show', this.notif[0]);
                                }
                            }

                            setTimeout(function(){
                              $('.js-auto-size').textareaAutoSize();
                            }, 5000);
                        })
                        .catch($.noop);

                        this.pform.submit('post', '/product-report')
                        .then(result => {
                            $('#modalProdReport').modal('toggle');
                            toastr.success(this.lang['product-reported']);
                        })
                        .catch($.noop);

                    })
                    .catch($.noop);
            }
        },


    },
});