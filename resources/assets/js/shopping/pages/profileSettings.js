var user = window.Laravel.user;
var cpanelUrl = window.Laravel.cpanel_url;
var display_name = window.Laravel.user.display_name;
//checkbox value
var display_stat = "";
 if(display_name == 'Real Name'){
    display_stat = false
 }else{
    display_stat = true
 }
var select = [];

Vue.component('que-answer',  require('../components/profile/SecurityQuestionAnswer.vue'));
Vue.component('notif-container',  require('../components/profile/NotificationContainer.vue'));



var profileSettings = new Vue({
  el: '#profile-settings-container',
 
  data: {
    showSecurity: true,
    user:user,
    valueQue1:{},
    valueQue2:{},
    valueQue3:{},
    securityQuestions: [],
    securityAnswers: [],
    Prov: [],
    optionProv: [],
    optionCity: [],
    city: null,
    accInfo: new Form({
       secondary_email: user.secondary_email
      ,birth_date: user.birth_date
      ,nick_name: user.nick_name
      ,gender: user.gender
      ,is_display_name: user.is_display_name
    },{ baseURL: '/profile/update'}),

    addressInfo: new Form({
        address     : ''
       ,barangay    : ''
       ,city        : ''
       ,province    : ''
       ,zip_code    : ''
       ,area_code   : ''
       ,landmark    : ''
       ,remarks     : ''  
       ,landline    : '' 
       ,mobile      : ''
    },{ baseURL  : '/profile/update'}),

    changePassword: new Form({
       old_password : ''
      ,new_password : ''
      ,new_password_confirmation : ''
    },{ baseURL:'/profile/update'}),

    securityQuestion: new Form({
       question  : ''
      ,question1 : ''
      ,question2 : ''
      ,answer    : ''
      ,answer1   : ''
      ,answer2   : ''
    },{ baseURL:'/profile/update'}),

    homeAddressInfo: new Form({
          address     : ''
         ,barangay    : ''
         ,city        : '' 
         ,city_id     : '' 
         ,province    : ''
         ,province_id : ''
         ,zip_code    : ''
         ,landmark    : ''
         ,remarks     : ''
         ,landline    : ''
         ,area_code   : ''
         ,mobile      : ''
    },{ baseURL  : '/profile/update'}),

    modalDocTitle:'',
    modalDocImage:'',
    notifications:[],
    securityLevel:'progress-bar-danger low',

    nbi_status      :null,
    birthCert_status:null,
    govID_status    :null,
    sec_status      :null,
    bir_status      :null,
    permit_status   :null,

    nbi_expiry      :'',
    birthCert_expiry:'',
    govID_expiry    :'',
    sec_expiry      :'',
    bir_expiry      :'',
    permit_expiry   :'',

    nbi_file        :'',
    birthCert_file  :'',
    govID_file      :'',
    sec_file        :'',
    bir_file        :'',
    permit_file     :'',

    nbi_reason      :'',
    birthCert_reason:'',
    govID_reason    :'',
    sec_reason      :'',
    bir_reason      :'',
    permit_reason   :'',

    docNbi          :null,
    docBirthCert    :null,
    docGovId        :null,
    docSec          :null,
    docBir          :null,
    docPermit       :null,

    baseHeight :0,
    baseWidth :0,
    baseZoom : 1,

    lang2:[],
    verified:'ssd',
    notverified:'ssss',    
    optionGender: ['Male','Female'],
    param: ''
  },


ready: function() {
  this.$nextTick(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="tooltip"]').tooltip('fixTitle');
  });
},

  components: {
  	Multiselect: window.VueMultiselect.default
  },

  directives: {
    datepicker: {
      bind(el, binding, vnode) {
        $(el).datepicker({
          format: 'yyyy-mm-dd'
        }).on('changeDate', (e) => {
          profileSettings.$set(profileSettings.Info, 'birth_date', e.format('yyyy-mm-dd'));
        });
      }
    }
  },

  mounted() {
    $('#modalDocsView').on('shown.bs.modal,show.bs.modal', () => {
        this.baseZoom = 1;
      
        img = $('<img id="shittyimage" class="center-block" src="'+ this.modalDocImage +'">').on('load', () => {
          var $modalImg = $('<img src="'+ this.modalDocImage +'" class="center-block" >');
          $modalImg.appendTo('#doc-container');
          this.baseHeight = $modalImg.height();
          this.baseWidth = $modalImg.width();

          $modalImg.draggable();
          
        });
      
    });

    $('#modalDocsView').on('hide.bs.modal', () => {
        $('#doc-container').empty();
    });

  },

  watch:{
    baseZoom:function(newValue){
      var changedHeightSize =  this.baseHeight * newValue;
      var changedWidthSize = this.baseWidth * newValue;
      $('#doc-container').find('img').css('height',changedHeightSize);
      $('#doc-container').find('img').css('width',changedWidthSize);
    },
    modalDocImage:function(){
        this.baseZoom = 1;
    },
  },

  methods:{
    lockUploadBtn:function(status,expire_date){
        // if(status == 'Pending')
        // {
        //     return true;
        // }

        // if(status == 'Expired')
        // {
        //     return true;
        // }

        if(status == 'Verified')
        {
            var expiry_date = moment(expire_date);
            
            if( parseInt(expiry_date.diff(moment(), 'days')) <= 30)
            {
                return false;
            }else
            {
                return true;
            }
        }

        return false;
    },

    labelColor:function(status)
    {
      switch(status)
      {
        case 'Pending':
          return 'label-default';

        break;
        case 'Expired':
            $('[data-toggle="tooltip"]').tooltip('fixTitle');
        case 'Denied':
          return 'label-danger';

        break;
        case 'Verified':
          return 'label-success';
        break;
      }
    },
    //show modals
    shipAddModal:function (event){
       var elShow = event.target.name;
       $('#'+elShow+2).modal('toggle');
    },
    openDocModal:function(title,filename){
      this.modalDocTitle = title.toUpperCase();


      this.modalDocImage = '/images/documents/'+user.id+'/'+filename;
       $('#modalDocsView').modal('toggle');
    },
    //save account info
    saveInfo:function (event){
       this.accInfo.submit('post', '/account-info')
          .then(result => {
            this.accInfo = new Form({
               secondary_email: result.secondary_email
              ,birth_date: result.birth_date
              ,nick_name: result.nick_name
              ,gender: result.gender
              ,is_display_name: result.is_display_name    
            },{ baseURL: '/profile/update'});

            if(this.accInfo.is_display_name) { // checked
              if(result.nick_name != null) {
                $('#nickname').text(result.nick_name);
              } else {
                $('#nickname').text(result.first_name + ' ' + result.last_name);
              }
            } else { // unchecked
              $('#nickname').text(result.first_name + ' ' + result.last_name);
            }
            
            toastr.success(this.lang2.data['prof-updated-msg']);
          })
          .catch(error => {
            toastr.error(this.lang2.data['update-failed']);
          });
    },

    fnchangePassword:function(event){
      this.changePassword.submit('post', '/change-password')
      .then(result => {
          toastr.success(this.lang2.data['pw-changed-msg']);
      })
      .catch(error => {
          toastr.error(this.lang2.data['update-failed']);
      });
    },

    saveSecurityQuestion:function(event){
      this.securityQuestion.submit('post', '/answer-security-questions')
      .then(result => {
          toastr.success(this.lang2.data['succ-upload']);
          this.getSecurityAnswers();
          this.getSecurityLevel()
      })
      .catch(error => {
          toastr.error(this.lang2.data['update-failed']);
      });
    },

    saveNewAddress:function(event){
      this.addressInfo.submit('post', '/address-info');
    },

    getSecurityAnswers() {
      axios.get('/profile/security-answers')
        .then(result => {
          this.securityAnswers = result.data.questions;
          if (this.securityAnswers.length) {
            this.showSecurity = false;

          }
          this.$nextTick(function(){
            this.getSecurityLevel();
          })
      });
    },

    getCity:function(data){
      this.homeAddressInfo.city = '';
      axios.get('/profile/city/'+data)
        .then(result => {
        var array = [];
        for(var i = 0; i < result.data.length; i++){
        
          array.push(result.data[i]);
        }
        this.optionCity = array;
      });
    },

    getAreaCode:function(data){
      this.homeAddressInfo.city = data;
      this.homeAddressInfo.area_code = this.padAreaCode(data.area_code);

    },

    saveHomeAddress:function(event){
      if (this.city!=null){
        this.homeAddressInfo.province_id = this.getProvinceID
        this.homeAddressInfo.city = this.city.name;
        this.homeAddressInfo.city_id = this.city.id;
      }
      
      this.homeAddressInfo.submit('post', '/home-address-info', this.homeAddressInfo)
      .then(result => {
          toastr.success(this.lang2.data['prof-updated-msg']);
      })
      .catch(error => {
          toastr.error(this.lang2.data['update-failed']);
      });
       
    },

    changeSelect(selectedOption, id){
      if(id == "question"){
        select[0] = selectedOption;
      }else if(id == "question1"){
        select[1] = selectedOption;
      }else if(id == "question2"){
        select[2] = selectedOption;
      }
      var options = this.securityQuestions;
      options = options.map(function (question){
        question.$isDisabled = false;
        return question;
      });

      $(select).each(function(i) {
        options = options.map(function (question){
            if (select[i].id == question.id) {
              question.$isDisabled = true;
            }
            return question;
          });
      });
    },

    getDocuments(){
      axiosAPIv1.get('/users/'+Laravel.user.id+'/documents')
        .then(docsObj => {
          if(docsObj['status'] == 200)
          {
            nbi_obj       = docsObj.data.filter(obj => { return obj.type === 'nbi' }) 
            birthCert_obj = docsObj.data.filter(obj => { return obj.type === 'birthCert' })
            govID_obj     = docsObj.data.filter(obj => { return obj.type === 'govId' })
            sec_obj       = docsObj.data.filter(obj => { return obj.type === 'sec' })
            bir_obj       = docsObj.data.filter(obj => { return obj.type === 'bir' })
            permit_obj    = docsObj.data.filter(obj => { return obj.type === 'permit' })
            
            if(nbi_obj.length)
            {
                this.nbi_status =  nbi_obj[0].status;              
                this.nbi_expiry =  nbi_obj[0].expires_at;              
                this.nbi_reason =  nbi_obj[0].reason;  
                this.nbi_file =  nbi_obj[0].name; 
                if(this.nbi_status == 'Expired'){
                  this.docNbi = false;  
                }   
                else{
                  this.docNbi = true;  
                }          
            


            }

            if(birthCert_obj.length)
            {
                this.birthCert_status =  birthCert_obj[0].status;              
                this.birthCert_expiry =  birthCert_obj[0].expires_at;              
                this.birthCert_reason =  birthCert_obj[0].reason;  
                this.birthCert_file =  birthCert_obj[0].name;              
                this.docBirthCert = true;  
            

            }

            if(govID_obj.length)
            {
                this.govID_status =  govID_obj[0].status;              
                this.govID_expiry =  govID_obj[0].expires_at;              
                this.govID_reason =  govID_obj[0].reason;  
                this.govID_file =  govID_obj[0].name;              
                this.docGovId = true;  
            


            }

            if(sec_obj.length)
            {
                this.sec_status =  sec_obj[0].status;              
                this.sec_expiry =  sec_obj[0].expires_at;              
                this.sec_reason =  sec_obj[0].reason;  
                this.sec_file =  sec_obj[0].name;              
                this.docSec = true;  
            

            }

            if(bir_obj.length)
            {
                this.bir_status =  bir_obj[0].status;              
                this.bir_expiry =  bir_obj[0].expires_at;              
                this.bir_reason =  bir_obj[0].reason;  
                this.bir_file =  bir_obj[0].name;              
                this.docBir = true;  
            

            }

            if(permit_obj.length)
            {
                this.permit_status =  permit_obj[0].status;              
                this.permit_expiry =  permit_obj[0].expires_at;              
                this.permit_reason =  permit_obj[0].reason;  
                this.permit_file =  permit_obj[0].name;              
                this.docPermit = true;  
            

            }

            this.getSecurityLevel();
            this.$nextTick(function(){
              $('[data-toggle="tooltip"]').tooltip('fixTitle');
            })
          }

        })
        .catch(error => {
          // toastr.error('Updated Failed.');
      });
    },

    getSecurityLevel(){

        var is_docsSubmitted = true;

        is_docsSubmitted = 
        ((this.nbi_status=='Verified')*(this.govID_status=='Verified'))
            ||
        ((this.sec_status=='Verified')*(this.bir_status=='Verified')*(this.permit_status=='Verified'));


        if(this.securityAnswers.length && is_docsSubmitted)
        {
            this.securityLevel = 'progress-bar-success high';
        }else if(this.securityAnswers.length || is_docsSubmitted)
        {
            this.securityLevel = 'progress-bar-warning med';
        }else
        {
            this.securityLevel = 'progress-bar-danger low';
        }
       
    },

    padAreaCode(n){
      if(n)
      {
        return ('00000' + n).slice(-5); 
      }else
      {
        return '';
      }
    }

  },

  created() {

    this.param = location.search.split('v=')[1];

    axios.get('/translate/profile-settings')
            .then(result => {
            this.lang2 = result.data;
    });
    //individual
    Event.listen('Imgfileupload.nbiUpload',(data)=>{
        if(data){
            axiosAPIv1.get('/users/'+Laravel.user.id+'/documents?doctype=nbi')
                .then(docsObj => {
                    if(docsObj.data.length){
                        this.docNbi = true;
                        this.nbi_status = docsObj.data[0].status;
                        this.nbi_file = docsObj.data[0].name;
                        toastr.success(this.lang2.data['upload'],this.lang2.data['succ-upload']);

                    }else
                    {
                        toastr.error(this.lang2.data['upload'],this.lang2.data['try-again']);
                    }
                })
          content = $('.btnNbiUpload').find('.btn-content');
          loader = $('.btnNbiUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }else
        {
            content = $('.btnNbiUpload').find('.btn-content');
            loader = $('.btnNbiUpload').find('.loader');
            loader.fadeOut(100,function(){
              content.fadeIn(100);
            });
        }

    })
    Event.listen('Imgfileupload.birthCertUpload',(data)=>{
        if(data){
            axiosAPIv1.get('/users/'+Laravel.user.id+'/documents?doctype=birthCert')
                .then(docsObj => {
                    if(docsObj.data.length){
                        this.docBirthCert = true;
                        this.birthCert_status = docsObj.data[0].status;
                        this.birthCert_file = docsObj.data[0].name;
                        toastr.success(this.lang2.data['upload'],this.lang2.data['succ-upload']);

                    }else
                    {
                        toastr.error(this.lang2.data['upload'],this.lang2.data['try-again']);
                    }
                })
          content = $('.btnBirthCertUpload').find('.btn-content');
          loader = $('.btnBirthCertUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }else
        {
          content = $('.btnBirthCertUpload').find('.btn-content');
          loader = $('.btnBirthCertUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }
    })
    Event.listen('Imgfileupload.govIdUpload',(data)=>{
        if(data){
            axiosAPIv1.get('/users/'+Laravel.user.id+'/documents?doctype=govId')
                .then(docsObj => {
                  console.log(docsObj);
                    if(docsObj.data.length){
                        this.docGovId = true;
                        this.govID_status = docsObj.data[0].status;
                        this.govID_file = docsObj.data[0].name;
                        toastr.success(this.lang2.data['upload'],this.lang2.data['succ-upload']);

                    }else
                    {
                        toastr.error(this.lang2.data['upload'],this.lang2.data['try-again']);
                    }
                })
          content = $('.btnGovIdUpload').find('.btn-content');
          loader = $('.btnGovIdUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }else
        {
          content = $('.btnGovIdUpload').find('.btn-content');
          loader = $('.btnGovIdUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }
    })
    // enterprise
    Event.listen('Imgfileupload.secUpload',(data)=>{
        if(data){
            axiosAPIv1.get('/users/'+Laravel.user.id+'/documents?doctype=sec')
                .then(docsObj => {
                    if(docsObj.data.length){
                        this.docSec = true;
                        this.sec_status = docsObj.data[0].status;
                        this.sec_file = docsObj.data[0].name;
                        toastr.success(this.lang2.data['upload'],this.lang2.data['succ-upload']);

                    }else
                    {
                        toastr.error(this.lang2.data['upload'],this.lang2.data['try-again']);
                    }
                })
          content = $('.btnSecUpload').find('.btn-content');
          loader = $('.btnSecUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }else
        {
          content = $('.btnSecUpload').find('.btn-content');
          loader = $('.btnSecUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }
    })
    Event.listen('Imgfileupload.birUpload',(data)=>{
        if(data){
            axiosAPIv1.get('/users/'+Laravel.user.id+'/documents?doctype=bir')
                .then(docsObj => {
                    if(docsObj.data.length){
                        this.docBir = true;
                        this.bir_status = docsObj.data[0].status;
                        this.bir_file = docsObj.data[0].name;
                        toastr.success(this.lang2.data['upload'],this.lang2.data['succ-upload']);

                    }else
                    {
                        toastr.error(this.lang2.data['upload'],this.lang2.data['try-again']);
                    }
                })
          content = $('.btnBirUpload').find('.btn-content');
          loader = $('.btnBirUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }else
        {
          content = $('.btnBirUpload').find('.btn-content');
          loader = $('.btnBirUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }
    })
    Event.listen('Imgfileupload.permitUpload',(data)=>{
        if(data){
            axiosAPIv1.get('/users/'+Laravel.user.id+'/documents?doctype=permit')
                .then(docsObj => {
                    if(docsObj.data.length){
                        this.docPermit = true;
                        this.permit_status = docsObj.data[0].status;
                        this.permit_file = docsObj.data[0].name;
                        toastr.success(this.lang2.data['upload'],this.lang2.data['succ-upload']);

                    }else
                    {
                        toastr.error(this.lang2.data['upload'],this.lang2.data['try-again']);
                    }
                })
          content = $('.btnPermitUpload').find('.btn-content');
          loader = $('.btnPermitUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }else
        {
          content = $('.btnPermitUpload').find('.btn-content');
          loader = $('.btnPermitUpload').find('.loader');
          loader.fadeOut(100,function(){
            content.fadeIn(100);
          });
        }
    })


    axios.get('/profile/notifications')
      .then(result => {
        this.notifications = result.data;
    });

    axios.get('/profile/security-questions')
      .then(result => {
        this.securityQuestions = result.data;
    });

    axios.get('/profile/get-home-address')
      .then(result => {
        if(result.data.mobile != '' && result.data.landline != '' && result.data.residence != null){
          this.homeAddressInfo = new Form($.extend({
               mobile   : result.data.mobile
              ,landline : result.data.landline
              ,area_code : result.data.area_code
              ,residence: result.data.residence
          }, result.data.residence), { baseURL: '/profile/update'});
          axios.get('/profile/city/'+this.homeAddressInfo.province)
            .then(result => {
            this.optionCity = result.data;
            var cityObj = this.optionCity.filter(obj => { return this.homeAddressInfo.city == obj.name })
            this.city = cityObj[0];

          });
        }else{
          homeAddressInfo: new Form({
                address     : ''
               ,barangay    : ''
               ,city        : '' 
               ,province    : ''
               ,zip_code    : ''
               ,area_code   : ''
               ,landmark    : ''
               ,remarks     : ''
               ,landline    : ''
               ,mobile      : ''
          },{ baseURL  : '/profile/update'})
        }


    });

    this.getSecurityAnswers();
    
    axios.get('/profile/provinces')
      .then(result => {
        var array = [];
        var array2 = [];
        for(var i = 0; i < result.data.length; i++){
          array.push(result.data[i].name);
          array2.push(result.data[i]);
        }
        this.optionProv = array;
        this.Prov = array2;
    });
    this.getDocuments();
  },

  computed: {
    answerEmpty: function () {
      return jQuery.isEmptyObject(this.securityAnswers);
    },  
    getProvinceID: function(){
        let array = this.Prov;
        let key = 'name';
        let value = this.homeAddressInfo.province;
        for (var i = 0; i < array.length; i++) {
                if (array[i][key] === value) {
                    return array[i]['id'];
                }
            }
        return null;
    },
  },


});

function removeLoading(input, container)
{
    if($(input).val() == '')
    {
        content = $(container).find('.btn-content');
        loader = $(container).find('.loader');
        loader.fadeOut(100,function(){
            content.fadeIn(100);
        })
    }
}

$(document).ready(function(){

    //code to go to tab using url with #
    var url = window.location.href;
    if(/#/.test(url)){
      var activeTab = url.substring(url.indexOf("#") + 1);
      $(".tab-pane").removeClass("active in");
      $('a[href="#'+ activeTab +'"]').tab('show');
    }
    //console.log(activeTab);


    $('.nbi-uploader input').on('click',function(){
        $('body').one('mousemove',function(){
            removeLoading('.nbi-uploader input','.btnNbiUpload');
        })
    })
    $('.birthCert-uploader input').on('click',function(){
        $('body').one('mousemove',function(){
            removeLoading('.birthCert-uploader input','.btnBirthCertUpload');
        })
    })
    $('.govID-uploader input').on('click',function(){
        $('body').one('mousemove',function(){
            removeLoading('.govID-uploader input','.btnGovIdUpload');
        })
    })
    $('.sec-uploader input').on('click',function(){
        $('body').one('mousemove',function(){
            removeLoading('.sec-uploader input','.btnSecUpload');
        })
    })
    $('.bir-uploader input').on('click',function(){
        $('body').one('mousemove',function(){
            removeLoading('.bir-uploader input','.btnBirUpload');
        })
    })
    $('.permit-uploader input').on('click',function(){
        $('body').one('mousemove',function(){
            removeLoading('.permit-uploader input','.btnPermitUpload');
        })
    })

  $('#txt-home-area-code').inputmask({
    mask: '99999'
  })

  $('#txt-home-landline').inputmask({
    mask: '999-9999'
  })

  $('#txt-home-mobile').inputmask({
    mask: '(9999) 999-9999'
  })

  $('#txt-ship-zc').inputmask({
    mask: '99999'
  })

   $('.tile')
    // tile mouse actions
    .on('mouseover', function(){
      $(this).children('.photo').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
    })
    .on('mouseout', function(){
      $(this).children('.photo').css({'transform': 'scale(1)'});
    })
    .on('mousemove', function(e){
      $(this).children('.photo').css({
          'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'
      });
    })

  $('#txt-nickname').keypress(function(event){
    var inputValue = event.charCode;
    if(!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123)||(inputValue==32) || (inputValue==0))){
    event.preventDefault();
    }
  })
})