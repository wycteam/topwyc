Vue.component('product-item',  require('../components/product/Product.vue'));

Vue.filter('currency', function (value) {
    return  numberWithCommas(value.toFixed(2));
});

Vue.filter('round', function (value) {
    return  roundOff(value);
});

var app = new Vue({
	el: '#product-list',
	data: {
		items:[
			
		],
		selected:[],
		pName:true,
	},
	created(){
		if(window.location.search.substr(8)) // remove ?search=
		{
			function getProducts() {
				return axiosAPIv1.post('/search/product',{
					keyword:window.location.search.substr(8).replace(/%20/g, " ")
				});
			}
	    	axios.all([
				getProducts()
			]).then(axios.spread(
				(
					 response
				) => {
				this.items = response.data;
				
				setTimeout(function () {
			        $('#allitems').trigger('click');
			    }, 2000);

			}))
			.catch($.noop);
		}
	},
	methods:{
		selectItem(id){
			this.selected.push(id)
		}
	}
})

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function roundOff(v) {
    return Math.round(v);
}
