import ChatBox from './ChatBox.vue'

new Vue({
    el: '#chat-bar',

    data: {
        users: [],
        notifications: [],
    },

    methods: {
        showChatBox(user) {
            if (!!~this.users.indexOf(user)) {
                return;
            }

            this.users.push(user);
        },

        closeChatBox(user) {
            var index = this.users.findIndex(function (el) {
                return el.id === user.id;
            });

            this.users.splice(index, 1);
        },

        fireNewMessage(data) {
            data.message.user = this.users[0];
            Event.fire('chat-room-' + data.message.chat_room_id, data.message);

            var title = "New Message";
            var speed = 300;
            var seperator = "|";

            MarqueeTitle.start(title, speed, seperator);
        },

        getNotifications() {
            return axiosAPIv1.get('/messages/notifications2')
                .then(result => {
                    this.notifications = result.data;
                });
        },

        markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (! notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });
            $(".cntmsg").html('');
            return axiosAPIv1.post('/messages/mark-all-as-seen2');
        }
    },

    components: {
        ChatBox
    },

    created() {
        this.getNotifications();
        Event.listen('chat-box.show', this.showChatBox);
        Event.listen('chat-box.close', this.closeChatBox);
        Event.listen('new-chat-message', this.fireNewMessage);
    }
});


setTimeout(function(){
  $('.js-auto-size').textareaAutoSize();
}, 5000);
    
/* MarqueeTitle v2.0pre | Cameron Samuels License | git.io/vQZbs */
var MarqueeTitle = {}; MarqueeTitle.chars = [], MarqueeTitle.title = "",
MarqueeTitle.start = function(title, speed, separator) {
    MarqueeTitle.separator = separator || MarqueeTitle.separator || " ";
    title = (title || MarqueeTitle.title || document.title) + " " + MarqueeTitle.separator + " ";
    MarqueeTitle.chars = title.split('');
    MarqueeTitle.speed = speed || MarqueeTitle.speed || 250;
    clearInterval(MarqueeTitle.interval);
    MarqueeTitle.interval = setInterval(MarqueeTitle.cycle, MarqueeTitle.speed);
    MarqueeTitle.title = title.replace(" " + MarqueeTitle.separator + " ", "");
},
MarqueeTitle.stop = function() {
    clearInterval(MarqueeTitle.interval);
    document.title = MarqueeTitle.title;
},
MarqueeTitle.loop = function(times, title, speed, separator) {
    times = times || 1;
    MarqueeTitle.separator = separator || MarqueeTitle.separator || " ";
    title = (title || MarqueeTitle.title || document.title) + " " + MarqueeTitle.separator + " ";
    MarqueeTitle.chars = title.split('');
    MarqueeTitle.speed = speed || MarqueeTitle.speed || 250;
    setTimeout(MarqueeTitle.stop, MarqueeTitle.speed * MarqueeTitle.title.length * times);
    MarqueeTitle.interval = setInterval(MarqueeTitle.cycle, MarqueeTitle.speed);
    MarqueeTitle.title = title.replace(" " + MarqueeTitle.separator + " ", "");
},
MarqueeTitle.pause = function() { clearInterval(MarqueeTitle.interval) },
MarqueeTitle.resume = function() { MarqueeTitle.interval = setInterval(MarqueeTitle.cycle, MarqueeTitle.speed) },
MarqueeTitle.cycle = function(times) {
    times = times || MarqueeTitle.shifts || 1;
    do {
        if (MarqueeTitle.direction) MarqueeTitle.chars.unshift(MarqueeTitle.chars[MarqueeTitle.chars.length-1]), MarqueeTitle.chars.pop();
        else MarqueeTitle.chars.push(MarqueeTitle.chars[0]), MarqueeTitle.chars.shift();    
        document.title = MarqueeTitle.chars.join("");
        times--;
    } while (times > 0);
},
MarqueeTitle.reverse = function() { MarqueeTitle.direction = MarqueeTitle.direction?0:1 },
MarqueeTitle.separate = function(separator) {
    MarqueeTitle.separator = separator;
    MarqueeTitle.start();
}