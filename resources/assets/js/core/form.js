class Form {
    constructor(data = {}, axiosConfig = {}, config = {}) {
        this.originalData = data;
        this.errors = new Errors();
        this.axios = $.isEmptyObject(axiosConfig) ? axios : axios.create(axiosConfig);
        this.config = config;

        for (var field in data) {
            this[field] = data[field];
        }
    }

    data() {
        var data = {};

        for (var property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }

    reset() {
        for (var property in this.originalData) {
            if (this.config.hasOwnProperty('resetExceptions') && !~this.config.resetExceptions.indexOf(property)) {
                this[property] = '';
            }
        }

        this.errors.clear();
    }

    submit(requestType, url, data) {
        return new Promise((resolve, reject) => {
            this.axios[requestType](url, data || this.data())
                .then(response => {
                    this._onSuccess(response.data);

                    resolve(response.data);
                })
                .catch(error => {
                    this._onFail(error.response.data);
                    reject(error.response.data);
                });
        });
    }

    _onSuccess(data) {
        this.removeHasLoading()
        this.reset();
    }

    _onFail(errors) {
        this.removeHasLoading()
        console.log('fail');

        this.errors.record(errors);
    }

    removeHasLoading(){
        var content = $('body').find('.btn-content');
        var loader = $('body').find('.loader');
        if(loader)
        {
            loader.fadeOut(100,function(){
                content.fadeIn(100);
            });
        }
    }
}

export default Form;