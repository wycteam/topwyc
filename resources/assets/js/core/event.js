import Vue from 'vue';

let Event = new Vue({
    methods: {
        fire(event, data) {
            this.$emit(event, data);
        },

        listen(event, callback) {
            this.$on(event, callback);
        },

        unlisten(event, callback) {
            this.$off(event, callback);
        }
    }
});

export default Event;