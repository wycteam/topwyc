Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

let data = window.Laravel.data;

var FeedbackApp = new Vue({
	el:'#productpage',
	data:{
		products: [],
		selproduct : '',
		created_at: '',
		index : '',
		main_url : "http://"+window.Laravel.base_url,
	},
	created(){
		this.getproduct();
	},
	methods:{
		upproduct(product_id,req){
			axiosAPIv1.put('/products/'+product_id,{
					req:req						
				})
				.then(response => {
					if(response.status == 200)
					{
						$('.ibox').children('.ibox-content').toggleClass('sk-loading');
						this.products.filter(obj => {
							if(obj.id == product_id)
							{
								switch(req){
									case 'promote':
										obj.status = 'featured';
										toastr.success('product Promoted!');
										$('.dt-products').DataTable().destroy();
										this.callDataTables();
										break;
									case 'demote':
										obj.status = null;
										toastr.success('product Demoted!');
										$('.dt-products').DataTable().destroy();
										this.callDataTables();
										break;
									case 'delete':
										obj.deleted_at = 0;
										toastr.success('product Deleted!');
										$('.dt-products').DataTable().destroy();
										this.callDataTables();
										break;
									case 'deactivate':
										obj.is_active = 0;
										toastr.success('product Deactivated!');
										$('.dt-products').DataTable().destroy();
										this.callDataTables();
										break;
									case 'activate':
										obj.is_active = 1;
										toastr.success('product Activated!');
										$('.dt-products').DataTable().destroy();
										this.callDataTables();
										break;
									case 'searchable':
										obj.is_searchable = 1;
										toastr.success('product Searchable again!');
										$('.dt-products').DataTable().destroy();
										this.callDataTables();
										break;
									case 'restore':
										obj.deleted_at = null;
										toastr.success('product Reproducted!');
										$('.dt-products').DataTable().destroy();
										this.callDataTables();
										break;
									case 'permanent':
										this.$delete(this.products, this.index);
										toastr.success('product Remove from the database!');
										$('.ibox').children('.ibox-content').toggleClass('sk-loading');
										break;
									default:
										toastr.error('Try Again!');


								}
							}
						})
					}
				})
		},
		//remove functions
		showRemoveDialog(product){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selproduct = product;
			this.created_at = product.created_at.date;
			$('#dialogRemove').modal('toggle');
		},
		deleteproduct(id){
			let req = 'delete';
			this.upproduct(id,req);
		},
		//deactivate functions
		showDeacDialog(product){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selproduct = product;
			this.created_at = product.created_at.date;
			$('#dialogDeac').modal('toggle');
		},
		deacproduct(id){
			let req = 'deactivate';
			this.upproduct(id,req);
		},
		//demote functions
		showDemoteDialog(product){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selproduct = product;
			this.created_at = product.created_at.date;
			$('#dialogDemote').modal('toggle');
		},
		demoteproduct(id){
			let req = 'demote';
			this.upproduct(id,req);
		},
		showRestoreDialog(product){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selproduct = product;
			this.created_at = product.created_at.date;
			$('#dialogRestore').modal('toggle');
		},
		//reproduct functions
		showRecycleDialog(product){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selproduct = product;
			this.created_at = product.created_at.date;
			$('#dialogRecycle').modal('toggle');
		},
		restoreproduct(id){
			let req = 'searchable';
			this.upproduct(id,req);
		},
		recycleproduct(id){
			let req = 'restore';
			this.upproduct(id,req);
		},
		//delete perma  functions
		showPermaDialog(product,index){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selproduct = product;
			this.created_at = product.created_at.date;
			this.index = index;
			$('#dialogPermaDelete').modal('toggle');
		},
		PermaDeletetore(id){
			let req = 'permanent';
			this.upproduct(id,req);
		},

		//get all products
		getproduct(){
			function getAllproducts() {
				$('.ibox').children('.ibox-content').toggleClass('sk-loading');
				return axiosAPIv1.get('/products');
			}
	    	axios.all([
				getAllproducts()
			]).then(axios.spread(
				(
					 products
				) => {
				this.products = products.data;
			}))
			.catch($.noop);
			this.callDataTables();

		},

		callDataTables(){
			// if ( $.fn.DataTable.isDataTable('.dt-products') ) {
			//   $('.dt-products').DataTable().destroy();
			// }
			setTimeout(function(){
				$('.dt-products').DataTable({
					"destroy": true,
		            pageLength: 10,
		            responsive: true,
		            dom: '<"top"lf>r<"clear">t<"bottom"p>',
		            order: [[ 0, "desc" ]],
					scrollY: "500px",
					autoWidth: false,
					// scrollCollapse: true,
					// paging: false
		        });
			},500);

		},
	},
	watch:{
		products:function(){
	  		$('.dt-products').DataTable().destroy();
			this.callDataTables();
		},
	},
	updated() {
		$('.ibox').children('.ibox-content').toggleClass('sk-loading');
	}

});
