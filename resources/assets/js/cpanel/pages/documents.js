var pendocs = null;

var vm = new Vue ({
    el:'#documents-manager',
    data:{
        countlist:[],
        showlist:[],
        selected:{},
        activeStat:"all",
        validity: new Form({
            'expires_at' : "",
            'status':"",
            'id':"",
            'month':"",
            'day':""
        },{ baseURL  : '/documents/update'}),
        denied: new Form({
            'reason': "",
            'status': "",
            'id':""
        },{ baseURL  : '/documents/update'}),
        activeId:{},

        baseZoom:1,
        baseHeight:0,
        baseWidth:0,

        disableBtns:false
    },
    components: {
        'documentlist': require('../components/Documents/List.vue')
    },

     watch:{
        baseZoom:function(newValue){
          var changedHeightSize =  this.baseHeight * newValue;
          var changedWidthSize = this.baseWidth * newValue;
          $('#doc-container').find('img').css('height',changedHeightSize);
          $('#doc-container').find('img').css('width',changedWidthSize);
        },
      },

    created(){
         axios
            .get('documents/get-pending')
            .then(result =>{
                this.updateDocumentLists(result.data,"pending");
            });
    },
    updated(){
        // pendocs =  $('#documentslist').dataTable( {
        //     paging: true,
        //     searching: true,
        //     order: [[ 0, "desc" ]]
        // });
    },
    directives: {
        datepicker: {
            bind(el, binding, vnode) {
                $(el).datepicker({
                    format: 'yyyy-mm-dd'
                }).on('changeDate', (e) => {
                    vm.$set(vm.validity, 'expires_at', e.format('yyyy-mm-dd'));
                });
            }
        }
    },

    mounted(){
         $('#viewDocument').on('shown.bs.modal,show.bs.modal', () => {
            this.baseZoom = 1;
            
            img = $('<img id="shittyimage" class="center-block" src="/images/documents/'+this.selected.documentable_id+'/'+this.selected.name+'">')
                .on('load', () => {
              var $modalImg = $('<img src="/images/documents/'+this.selected.documentable_id+'/'+this.selected.name+'" class="center-block" >');
              // $modalImg.appendTo('#doc-container');
              $('#doc-container').html($modalImg);
              this.baseHeight = $modalImg.height();
              this.baseWidth = $modalImg.width();

              $modalImg.draggable();

            });


        });


    },

    ready: function() {
        this.$nextTick(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="tooltip"]').tooltip('fixTitle');
        });
    },

    methods:{

        changeView:function(status){
            this.disableBtns = true;

            if(status ==  'all'){
                axios
                    .get('documents/get-documents')
                    .then(result =>{
                        this.updateDocumentLists(result.data, status);
                    });

            }else if(status == 'pending'){

                axios
                    .get('documents/get-pending')
                    .then(result => {
                        this.updateDocumentLists(result.data, status);
                    });

            }else if(status == 'verified'){

                axios
                    .get('documents/get-approved')
                    .then(result =>{
                        this.updateDocumentLists(result.data, status);
                    });

            }else if(status == 'denied'){

                axios
                    .get('documents/get-declined')
                    .then(result =>{
                        this.updateDocumentLists(result.data, status);
                    });
            }
        },

        updateDocumentLists:function(data,activeBtn){
            this.showlist = data;
            this.activeStat = activeBtn;

            this.disableBtns = false;
            $('#documentslist').DataTable().destroy();
            setTimeout(function(){
                pendocs =  $('#documentslist').dataTable( {
                    paging: true,
                    searching: true,
                    order: [[ 0, "desc" ]],
                    scrollY: "500px",
                    autoWidth: false
                });
            },200);

        },
        countDocs(){
            axios.get('documents/get-count')
                .then(result =>{
                this.countlist = result.data;
            });
        },
        initDocs(){
            axios.get('documents/get-documents')
                .then(result =>{
                this.showlist = result.data;
            });
        },
        saveValidity(id, dateSel){
            $(".verify").popover('hide');
            this.validity.id = id;
            this.validity.status = "Verified";
            this.validity.expires_at = dateSel;
            var month = parseInt(dateSel.substr(5,2));
            var day = parseInt(dateSel.substr(8,2));
             $('#documentslist').DataTable().destroy();

            if (month != 0){
               if(day != 0){
                    this.validity.submit('post', '/verify')
                    .then(result => {
                        var stat = this.activeStat;
                         if(stat == "all"){
                             axios.get('documents/get-documents')
                             .then(result =>{
                                this.updateDocumentLists(result.data, stat);
                            });
                         }else if(stat == "pending"){
                             axios.get('documents/get-pending')
                             .then(result =>{
                                this.updateDocumentLists(result.data, stat);
                            });
                         }else if(stat == "verified"){
                             axios.get('documents/get-approved')
                             .then(result =>{
                                this.updateDocumentLists(result.data, stat);
                            });
                         }else if(stat == "denied"){
                            axios.get('documents/get-declined')
                            .then(result =>{
                                this.updateDocumentLists(result.data, stat);
                            });
                        }
                        toastr.success('Successfully saved.');
                    })
                    .catch(error => {
                         if(error.expires_at){
                            $('.popover #validset').addClass('has-error');
                            toastr.error("Please use valid date of expiry.");
                        }else{
                           toastr.error('Update failed.');
                        }
                        $("#ver"+id).popover('show');
                         pendocs =  $('#documentslist').dataTable( {
                            paging: true,
                            searching: true,
                            order: [[ 0, "desc" ]],
                            scrollY: "500px",
                            autoWidth: false
                        });
                    });
               }else{
                    toastr.error('Please use valid expiration date');
               }
            }else{
                toastr.error('Please use valid expiration date');
            }
            $('#documentslist').DataTable().destroy();
        },
        saveReason(id, reason){

            $(".deny").popover('hide');
            this.denied.id = id;
            this.denied.status = "Denied";
            this.denied.reason = reason;
            this.denied.submit('post','/deny')
            .then(result => {
                var stat = this.activeStat;
                 if(stat == "all"){
                     axios.get('documents/get-documents')
                     .then(result =>{
                        this.updateDocumentLists(result.data, stat);
                    });
                 }else if(stat == "pending"){
                     axios.get('documents/get-pending')
                     .then(result =>{
                        this.updateDocumentLists(result.data, stat);
                    });
                 }else if(stat == "verified"){
                     axios.get('documents/get-approved')
                     .then(result =>{
                        this.updateDocumentLists(result.data, stat);
                    });
                 }else if(stat == "denied"){
                    axios.get('documents/get-declined')
                    .then(result =>{
                        this.updateDocumentLists(result.data, stat);
                    });
                }
                toastr.success('Successfully saved.');
            })
            .catch(error =>{
                if(error.reason){
                    $('.popover #reasonset').addClass('has-error');
                    toastr.error("Please indicate reason to deny.");
                }else{
                   toastr.error('Update failed.');
                }
                $("#den"+id).popover('show');
            });
        }
    }
})

$(document).ready(function(){

   $('.tile')
    // tile mouse actions
    .on('mouseover', function(){
      $(this).children('.photo').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
    })
    .on('mouseout', function(){
      $(this).children('.photo').css({'transform': 'scale(1)'});
    })
    .on('mousemove', function(e){
      $(this).children('.photo').css({
          'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'
      });
    })
})
$(document).on("click", ".save-verification", function() {
    var id = vm.activeId;
    var dateSel = $('.popover #expires_at').val();
    vm.saveValidity(id, dateSel);
});

$(document).on("click", ".save-denied", function() {
    var id = vm.activeId;
    var reason = $('.popover #reason').val();
    vm.saveReason(id, reason);
});

$(document).on('click', function (e) {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
        }
    });
});

$(document).on('focus.autoExpand', 'textarea.autoExpand', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input.autoExpand', 'textarea.autoExpand', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 17);
        this.rows = minRows + rows;
    });