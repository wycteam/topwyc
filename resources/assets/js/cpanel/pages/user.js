Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

var UserApp = new Vue({
	el:'#userlist',
	data:{
		users:[],

		seluser : ''
	},
	methods: {
		showRemoveDialog(user) {
			this.seluser = user;

			$('#dialogRemove').modal('toggle');
		},
		deleteUser() {
			axiosAPIv1.delete('/users/'+this.seluser.id)
				.then(response => {
					toastr.success('User Deleted!');

					var self = this;
					axiosAPIv1.get('/users?withEwallet')
						.then(response => {
							self.users = response.data;
						});
				});
		}
	},
	created(){
		function getUsers() {
			return axiosAPIv1.get('/users?withEwallet');
		}
    	axios.all([
			getUsers()
		]).then(axios.spread(
			(
				 users,
			) => {
			this.users = users.data;
		}))
		.catch($.noop);
	},
	updated() {
		$('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"top"lf>rt<"bottom"ip><"clear">'

        });
	}
});
