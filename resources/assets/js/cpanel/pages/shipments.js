
let rates = window.Laravel.rates;

var app = new Vue({
	el:'#shipmentpage',
	data:{
		orders: [],
		selected:[],
		totalsub:0,
        totaldiscount:0,
        priceAfterDiscount:0,
        filter: 'All'
	},
	components: {
        'orderlist': require('../components/Shipments/List.vue'),
        'productlist': require('../components/Shipments/ProductList.vue')
    },
	created(){
		this.getproduct();
	},
	methods:{
		//get all orders
		getproduct(){
            this.filter = 'All';

			function getAllOrders() {
				$('.ibox').children('.ibox-content').toggleClass('sk-loading');
				return axiosAPIv1.get('/intransitorders');
			}
	    	axios.all([
				getAllOrders()
			]).then(axios.spread(
				(
					 orders
				) => {
				this.orders = orders.data;
			}))
			.catch($.noop);

			this.callDataTables();
		},

		callDataTables(){
			setTimeout(function(){
				$('.dt-shipments').DataTable({
		            pageLength: 10,
		            responsive: true,
		            order: [[ 0, "desc" ]],
					scrollY: "500px",
					autoWidth: false,
					// scrollCollapse: true,
					// paging: false

                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;
             
                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };
             
                        // Total over all pages
                        total = api
                            .column(5)
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
        
             
                        // Update footer
                        $( api.column( 5 ).footer() ).html(
                            'P'+ total.toFixed(2)
                        );
                    }
		        });
			},500);
		},

        submitFilter() {
            if(this.filter == 'Daily') {
                params = {
                    filter: 'Daily',
                    date: $('form#daily-form input[name=date]').val()
                };
            } else if(this.filter == 'Monthly') {
                params = {
                    filter: 'Monthly',
                    date: $('form#monthly-form input[name=date]').val()
                };
            } else if(this.filter == 'DateRange') {
                params = {
                    filter: 'DateRange',
                    start: $('form#date-range-form input[name=start]').val(),
                    end: $('form#date-range-form input[name=end]').val()
                };
            }

            var self = this;
            toastr.options.positionClass = 'toast-bottom-right';

            axiosAPIv1.get('/shipment-filter', {
                params: params
            })
            .then(function (response) {
                if(response.data.hasOwnProperty('errors')) {
                    var errors = response.data.errors;

                    $.each(errors, function( key, value ) {
                        toastr.error(value[0]);
                    });
                } else {
                    self.orders = response.data;

                    $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                }
            })
            .catch(function (error) {
                toastr.error(error);
            });

        }

	},
	watch:{
		orders:function(){
	  		$('.dt-shipments').DataTable().destroy();
			this.callDataTables();

            $('.ibox').children('.ibox-content').toggleClass('sk-loading');
		},
	},
	updated() {
		// $('.ibox').children('.ibox-content').toggleClass('sk-loading');
	},
	computed:{
		totalShippingFee() { 
            var groupArray = require('group-array');
            var group = groupArray(this.selected.details, 'product.store.id');
            var weight = 0;
            var volume = 0;
            var totalChargeWeight = 0;
            var productPrice = 0;
            var salePrice = 0;
            var totalWeight = 0;
            var chargeWeight = 0;
            var total = 0;
            var totalSP = 0;
            var discount = 0;
            var totaldiscount = 0;
            var t = 0;
            var disc = [];
            var vm = this;
            $.each( group, function( key, value ) {
                //console.log("Loop:"+value.length);
                if(value.length>0){
                    //console.log(value);
                    totalChargeWeight = 0;
                    productPrice = 0;
                    salePrice = 0;
                    var totalSalePrice = 0;
                    discount = 0;
                    for(var i=0; i<value.length; i++){
                        //console.log(value[i].product.weight);

                        weight = value[i].product.weight;
                        volume = (value[i].product.length*value[i].product.width*value[i].product.height)/3500;
                        if(weight>volume) chargeWeight = weight;
                        if(volume>weight) chargeWeight = volume;
                        totalChargeWeight+= (parseFloat(chargeWeight)*parseFloat(value[i].quantity));
                        productPrice += ((value[i].sale_price > 0 ? parseFloat(value[i].sale_price) : parseFloat(value[i].price_per_item))*parseFloat(value[i].quantity));
                        var sp = value[i].subtotal;
                        salePrice += ((parseFloat(sp.replace(',', ''))));
                    }
                    //console.log("total Price :"+productPrice);
                    totalWeight = totalChargeWeight * 1.2; //add 20% to total weight
                    //console.log("twt :"+totalWeight);
                    let rateCat = "SAMM";
                    let halfkg = 0;
                    let fkg = 0;
                    let excess = 0;
                    let rate = 0;
                    // rate computation
                    //console.log(rates);
                    $.each(rates, function(i, item) {
                      if(rates[i].rate_category == rateCat){
                        halfkg = rates[i].halfkg;
                        fkg = rates[i].fkg;
                        excess = rates[i].excess;
                      }
                    });

                    if(totalWeight <= 0.5){
                      rate = halfkg;
                    }
                    else if(totalWeight >0.5 && totalWeight <=1){
                      rate = fkg;
                    }
                    else{
                      totalWeight -=1;
                      rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
                    }
                    //console.log('rate b4 '+rate);
                    rate = parseFloat(rate) + 20; // add 20 for markup
                    //console.log("rate -> "+Math.ceil(rate));

                    //valuation charge computation
                    let vc = productPrice * 0.01; 
                    //console.log("vc -> "+Math.ceil(vc));

                    //tax computation - docu stamp
                    let vtx = 0;
                    if(productPrice > 0 && productPrice <=1000){
                        vtx = ((rate -1)+vc) * .12;
                      }
                    else{
                        vtx = ((rate -10)+vc) * .12;
                    }
                    //console.log("tax -> "+Math.ceil(vtx));

                    //total fee
                    let sFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);        

                    //console.log("total fee :"+sFee);

                    total = productPrice + sFee;
                    t += total;

                    totalSalePrice += salePrice;
                    totalSP += salePrice;
                    discount = totalSalePrice - total;
                    totaldiscount += discount;
                    //console.log('discount-'+discount);
                    
                } 

            });

            this.totalsub = totalSP;
            this.totaldiscount = totaldiscount;
            this.priceAfterDiscount = totalSP - totaldiscount;

            let FinalFee = t;
            //console.log(FinalFee);
            return this.priceAfterDiscount;
        },
	},
	mounted() {
        $('#shipdetails').on('hidden.bs.modal', e => {
           $('.dt-shipments').DataTable().destroy();
		   this.callDataTables();
        });

        $(document).on('focus', 'form#daily-form .input-group.date', function(){
            $(this).datepicker({
                format:'yyyy/mm/dd',
                todayHighlight: true
            });
        });

        $(document).on('focus', 'form#monthly-form .input-group.date', function(){
            $(this).datepicker({
                format: "yyyy/mm",
                viewMode: "months", 
                minViewMode: "months",
                todayHighlight: true
            });
        });

        $(document).on('focus', 'form#date-range-form .input-daterange', function(){
            $(this).datepicker({
                format:'yyyy/mm/dd',
                todayHighlight: true
            });
        });
    }
});