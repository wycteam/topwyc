let rates = window.Laravel.rates;

var app = new Vue({

    el: '#orders-content',
    data: {
        sales:[],
        orderDetails:[],
        base_url:window.Laravel.base_url,
        user_id:'',
        hasDiscount: false,

        orderProducts: [],
        listProd:[],

        totalsub:0,
        totaldiscount:0,
        priceAfterDiscount:0,
        idProds:[],
    },
    created(){
        this.getsales();
    },
    methods:{
        btnReturnBuyerEwallet(id){
            swal({
                title: 'Confirm...',
                html: 'Return Ewallet to the Buyer?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(() => {
                axiosAPIv1.post('order-details/receive-returned-item' , {
                    id:id
                })
                .then(result => {
                    toastr.success('Ewallet Amount returned to the Buyer');
                    this.getsales();
                    $("#orderdetails-modal").modal('toggle');
                })
                .catch(error => {
                    toastr.error('Updated Failed.');
                });

            }).catch(() => {
                toastr.success('Please try again.');
            });
        },

        openModal(id){
            $('.ibox').children('.ibox-content').toggleClass('sk-loading');
            axiosAPIv1.get('/getOrderDetails/' + id)
            .then(result => {
                this.orderDetails = result.data.data;

                this.orderProducts = result.data.data.order.details;

                axiosAPIv1.get('/user/sales-report/checkIfSameStore/'+result.data.data.product_id+'/'+result.data.data.order_id)
                  .then(prods => {
                    this.listProd = prods;
                    this.totalShippingFee;
                });

                this.user_id = result.data.data.order.user_id;
                if(result.data.data.discount != 0){
                    this.hasDiscount = true;
                }else{
                    this.hasDiscount = false;
                }

                $("#orderdetails-modal").modal('toggle');

                $('.ibox').children('.ibox-content').toggleClass('sk-loading');
            });
        },

        showVideoEvidenceModal(e, index, modal) {
            e.preventDefault();

            $('#' + modal).modal('hide');

            setTimeout(function() {
                $('a#video-evidence-'+index).ekkoLightbox({ 
                    wrapping: false, 
                    alwaysShowClose: true,
                    onHidden: function(direction, itemIndex) {
                        $('#' + modal).modal('show');
                    } 
                });
            }, 1000);
        },

        //get all sales
        getsales(){
            function getAllsales() {
                $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                return axiosAPIv1.get('/getOrders');
            }
            axios.all([
                getAllsales()
            ]).then(axios.spread(
                (
                     sales
                ) => {
                this.sales = sales.data.data;
            }))
            .catch($.noop);
            this.callDataTables();

        },

        callDataTables(){
            setTimeout(function(){
                $('.dt-sales').DataTable({
                    pageLength: 10,
                    responsive: true,
                    dom: '<"top"lf>r<"clear">t<"bottom"p>',
                    scrollY: "500px",
                });
            },500);

        },
    },
    watch:{
        sales:function(){
            $('.dt-sales').DataTable().destroy();
            this.callDataTables();
        },
    },
    updated() {
        $('.ibox').children('.ibox-content').toggleClass('sk-loading');
    },
    computed:{
        discountedPrice(){
            var $discountDecimal = this.orderDetails.discount / 100;
            var $originalPrice = this.orderDetails.price_per_item;
            var $discountAmount = $discountDecimal * $originalPrice;
            var $discountedPrice = $originalPrice - $discountAmount;
            $discountedPrice = $discountedPrice.toFixed(2);
            return ($discountedPrice);
        },

        price_per_item(){
            if(this.orderDetails.fee_included==1)
                return parseFloat(this.orderDetails.price_per_item) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
            else
                return this.orderDetails.price_per_item;
        },
        
        discountedAmount(){
            if(this.orderDetails.discount!=null || this.orderDetails.discount!=0)
                var $amount = this.orderDetails.sale_price;
            else
                var $amount = 0;

            $amount = parseFloat($amount).toFixed(2);
            return $amount;
        },

        discountPercentage(){
            if(this.orderDetails.discount!=null){
                if(this.orderDetails.fee_included==1){
                     var $amount = 100-((parseFloat(this.orderDetails.price_per_item) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat) - parseFloat(this.orderDetails.discount))/(parseFloat(this.orderDetails.price_per_item) + parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat)))*100;
                    return $amount.toFixed(2);               
                } else
                    return this.orderDetails.discount;
            } else
                return 0;
        },

        now(){
            return this.price_per_item-this.discountedAmount;
        },

        totalAmount(){
            var total = parseFloat(this.orderDetails.subtotal) + parseFloat(this.orderDetails.shipping_fee);
            total = total.toFixed(2);
            return total;
        },

        shipping(){
            if(this.orderDetails.fee_included!=1){
                var total = parseFloat(this.orderDetails.shipping_fee) + parseFloat(this.orderDetails.charge) + parseFloat(this.orderDetails.vat);
                total = total.toFixed(2);
            } else 
                var total = 0.00;

            return total;          
        },

        totalShippingFee() { 
            var groupArray = require('group-array');
            console.log(this.listProd.data);
            var group = groupArray(this.listProd.data, 'product.store.id');
            console.log(group);
            var weight = 0;
            var volume = 0;
            var totalChargeWeight = 0;
            var productPrice = 0;
            var salePrice = 0;
            var totalWeight = 0;
            var chargeWeight = 0;
            var total = 0;
            var totalSP = 0;
            var discount = 0;
            var totaldiscount = 0;
            var t = 0;
            var disc = [];
            var vm = this;
            $.each( group, function( key, value ) {
                //console.log("Loop:"+value.length);
                if(value.length>0){
                    //console.log(value);
                    totalChargeWeight = 0;
                    productPrice = 0;
                    salePrice = 0;
                    var totalSalePrice = 0;
                    discount = 0;
                    for(var i=0; i<value.length; i++){
                        //console.log(value[i].product.weight);
                        if(value[i].status != "Cancelled"){
                            weight = value[i].product.weight;
                            volume = (value[i].product.length*value[i].product.width*value[i].product.height)/3500;
                            if(weight>volume) chargeWeight = weight;
                            if(volume>weight) chargeWeight = volume;
                            totalChargeWeight+= (parseFloat(chargeWeight)*parseFloat(value[i].quantity));
                            productPrice += ((value[i].sale_price > 0 ? parseFloat(value[i].sale_price) : parseFloat(value[i].price_per_item))*parseFloat(value[i].quantity));
                            var sp = value[i].subtotal;
                            salePrice += (parseFloat(sp.replace(',', '')));
                        }
                    }
                    //console.log("total Price :"+productPrice);
                    totalWeight = totalChargeWeight * 1.2; //add 20% to total weight
                    //console.log("twt :"+totalWeight);
                    let rateCat = "SAMM";
                    let halfkg = 0;
                    let fkg = 0;
                    let excess = 0;
                    let rate = 0;
                    // rate computation
                    //console.log(rates);
                    $.each(rates, function(i, item) {
                      if(rates[i].rate_category == rateCat){
                        halfkg = rates[i].halfkg;
                        fkg = rates[i].fkg;
                        excess = rates[i].excess;
                      }
                    });

                    if(totalWeight <= 0.5){
                      rate = halfkg;
                    }
                    else if(totalWeight >0.5 && totalWeight <=1){
                      rate = fkg;
                    }
                    else{
                      totalWeight -=1;
                      rate = (totalWeight * parseFloat(excess)) + parseFloat(fkg);
                    }
                    //console.log('rate b4 '+rate);
                    rate = parseFloat(rate) + 20; // add 20 for markup
                    //console.log("rate -> "+Math.ceil(rate));

                    //valuation charge computation
                    let vc = productPrice * 0.01; 
                    //console.log("vc -> "+Math.ceil(vc));

                    //tax computation - docu stamp
                    let vtx = 0;
                    if(productPrice > 0 && productPrice <=1000){
                        vtx = ((rate -1)+vc) * .12;
                      }
                    else{
                        vtx = ((rate -10)+vc) * .12;
                    }
                    //console.log("tax -> "+Math.ceil(vtx));

                    //total fee
                    let sFee = Math.ceil(rate) + Math.ceil(vtx) + Math.ceil(vc);        

                    //console.log("total fee :"+sFee);

                    total = productPrice + sFee;
                    t += total;

                    totalSalePrice += salePrice;
                    totalSP += salePrice;
                    discount = totalSalePrice - total;
                    totaldiscount += discount;
                    //console.log('discount-'+discount);
                    
                } 

            });

            this.totalsub = totalSP;
            this.totaldiscount = totaldiscount;
            this.priceAfterDiscount = totalSP - totaldiscount;

            let FinalFee = t;
            //console.log(FinalFee);
            return this.priceAfterDiscount;
        },
    }

});
