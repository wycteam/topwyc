var currentTable = null;
var currentTransaction = null;
var EwalletApp = new Vue({
	el: '#ewallet-root',
	
	data:{
		transactions:[],
		filterType:'all',
		filterStatus:'all',
		fileDepositSlip:'',
		filename:'',
		status:'',
		reason:'',
		newStatus:'',

		baseHeight :0,
	    baseWidth :0,
	    baseZoom : 1,
		showZoomable : false,

		pending_count:0,
		processing_count:0,
		cancelled_count:0,
		completed_count:0,
		status_seen:'Pending'
	},

	watch:{
		transactions:function(){
	  		$('.dt-ewallet').DataTable().destroy();

			setTimeout(function(){
				currentTable = $('.dt-ewallet').DataTable({
			        pageLength: 10,
			        responsive: true,
			        scrollY: "500px",
			        autoWidth: false
			    });
			},500);
		},

		filterType: function(){
			$('.dt-ewallet').DataTable().destroy();

			setTimeout(function(){
				currentTable = $('.dt-ewallet').DataTable({
			        pageLength: 10,
			        responsive: true,
			        scrollY: "500px",
			        autoWidth: false
			    });
			},500);
		},
		
		baseZoom:function(newValue){
			var changedHeightSize =  this.baseHeight * newValue;
			var changedWidthSize = this.baseWidth * newValue;
			$('#doc-container').find('img').css('height',changedHeightSize);
			$('#doc-container').find('img').css('width',changedWidthSize);
			$('#doc-container').find('img').removeClass('img-responsive');
		},
		// current_transaction_image:function(){
		// 	this.baseZoom = 1;
		// },
	},



	methods:{
		
		changeStatus(){
			if(this.newStatus == 'Cancelled')
			{
				if(this.reason == '' || this.reason == null)
				{
					toastr.error('','Reason is required for cancelling Ewallet Requests');
					return;
				}
			}else
			{
				this.reason = '';
			}
			axiosAPIv1
				.put('/ewallet-transaction/'+currentTransaction,{
					status:this.newStatus,
					reason:this.reason						
				})
				.then(response => {
					if(response.status == 200)
					{
						this.transactions.filter(obj => {
							if(obj.id == currentTransaction)
							{
								obj.status = this.newStatus;
								obj.updated_at = response.data.data.updated_at;
								modal=obj.type=='Deposit'?'depositModal':'decisionModal';
								toastr.success('Status Updated!');
								
								$('.dt-ewallet').DataTable().destroy();

								this.showTransactionByStatus(this.status_seen);

								setTimeout(function(){
									currentTable = $('.dt-ewallet').DataTable({
								        pageLength: 10,
								        responsive: true,
								        scrollY: "500px",
								    });
								},500);

								$('#'+modal).modal('toggle');

							}
						})
					}
				})
		},

		statusColor(status){
			switch(status){
				case 'Pending': return 'label-warning'; break;
				case 'Processing': return 'label-default'; break;
				case 'Completed': return 'label-primary'; break;
				case 'Cancelled': return 'label-danger'; break;
			}
		},

		showFilterStatus(status){
			if(this.filterStatus != 'all')
			{
				return this.filterStatus == status.toLowerCase();
			}
			return true;
		},

		showFilterType(type){
			if(this.filterType != 'all')
			{
				return this.filterType === type.toLowerCase();
			}
			return true;
		},

		showModal(id){
			currentTransaction = id;
			var transaction = this.transactions.filter(obj => {
				return obj.id == id;
			})

			if(transaction.length)
			{
				transaction = transaction[0];
				// if(transaction.status == 'Completed' || transaction.status == 'Cancelled' || transaction.type == 'Transfer')
				// {
				// 	return true;
				// }
				this.reason = transaction.reason;
				modal=transaction.type=='Deposit'?'depositModal':'decisionModal';
				if(transaction.deposit_slip)
				{
					this.filename = transaction.deposit_slip;
				}
				this.newStatus = transaction.status;

				$('#'+modal).modal('toggle');
			}
		},

		textColor(status){
			switch(status)
			{
				case 'Pending': return 'row-pending'; break;
				case 'Completed': return 'row-completed'; break;
				case 'Processing': return 'row-processing'; break;
				case 'Cancelled': return 'row-cancelled'; break;
			}
		},

		getTransactionsByStatus(status)
		{
			axiosAPIv1.get('/deposit-transaction')
			.then(response => {
				if(response.status == 200)
				{
					if(status != 'Processing')
					{
						this.transactions = response.data.filter(obj => {return obj.status == status});
					}else
					{
						this.transactions = response.data.filter(obj => {return obj.status == 'Processing' && obj.processor_id == Laravel.user.id});
					}

					this.pending_count = response.data.filter(obj => {return obj.status == 'Pending'}).length;
					this.processing_count = response.data.filter(obj => {return obj.status == 'Processing'  && obj.processor_id == Laravel.user.id}).length;
					this.cancelled_count = response.data.filter(obj => {return obj.status == 'Cancelled'}).length;
					this.completed_count = response.data.filter(obj => {return obj.status == 'Completed'}).length;


				}
			})
		},

		showTransactionByStatus(status)
		{
			this.status_seen = status;
			// $('.dt-ewallet').DataTable().destroy();
			this.getTransactionsByStatus(status);

		},



	},

	created(){
		this.getTransactionsByStatus('Pending');
	},

	mounted(){
		 $('#depositModal').on('shown.bs.modal', () => {
	        this.baseZoom = 1;
	        img = $('<img id="shittyimage" class="center-block img-responsive" src="'+ this.imgDS +'">').on('load', () => {
	          var $modalImg = $('<img src="'+ this.imgDS +'" class="center-block img-responsive" >');
	          $modalImg.appendTo('#doc-container');
	          this.baseHeight = $modalImg.height();
	          this.baseWidth = $modalImg.width();

	          $modalImg.draggable();
	          
	        });
	    });

	    $('#depositModal').on('hide.bs.modal', () => {
	        $('#doc-container').empty();
	        this.showZoomable = false;
	        this.filename = false;
	    });
	},

	computed:{
		imgDS() {
			if (this.filename) {
				this.showZoomable = true;
				return '/shopping/images/deposit-slip/' + this.filename;
			}
			this.showZoomable = false;
			return false;	
		},
	},

	
})