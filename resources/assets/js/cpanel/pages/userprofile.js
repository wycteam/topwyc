import Multiselect from 'vue-multiselect'
import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

Vue.component('modals',  require('../components/Modal.vue'));
Vue.component('dialog-modal',  require('../components/DialogModal.vue'));
// Vue.component('row-document',  require('../components/Documents/DocumentContainer.vue'));
Vue.component('package-item',  require('../visa/clients/Package.vue'));
Vue.component('service-item',  require('../visa/clients/Service.vue'));
Vue.component('action-logs',  require('../visa/clients/ActionLogs.vue'));
Vue.component('transaction-logs',  require('../visa/clients/TransactionLogs.vue'));
Vue.component('commission-logs',  require('../visa/clients/CommissionLogs.vue'));
Vue.component('document-logs',  require('../visa/clients/DocumentLogs.vue'));
Vue.component('document-logs-details',  require('../visa/clients/DocumentLogsDetails.vue'));
Vue.component('add-new-service',  require('../visa/clients/AddService.vue'));
Vue.component('edit-service',  require('../visa/clients/EditService.vue'));
Vue.component('view-report',  require('../visa/clients/ViewReport.vue'));


let data = window.Laravel.data.data;

let add = data.address !=null? data.address : null;
let numb = data.contact_number !=null? data.contact_number : null;
var client_id = data.id;
var user = window.Laravel.user;
var ClientApp = new Vue({
	el:'#userprofile',
	data:{
		is_rush:true,
		translation: null,
		packageTranslation: null,
		transactionLogsTranslation: null,
		clients: [],
		processor: user,
		is_auth: false,
		authorizers:[],
		usr_id: data.id,
		branch: data.branch_id,
		client_branch: data.branch,
		payment_status: data.payment_status,
		rcvs: data.document_receive,
		fullname: data.full_name,
		first_name: data.first_name,
		last_name: data.last_name,
		avatar: data.avatar,
		ewallet_balance:0,
		email: data.email,
		bday: data.birth_date,
		gender: data.gender,
		cstatus: data.civil_status,
		passport: data.passport,
		passport_exp_date: data.passport_exp_date,
		height: data.height,
		weight: data.weight,
		unread_notif: data.unread_notif,
		group_binded: data.group_binded,
		binded: data.binded,
		address: data.addresss,
		add: add,
		number: numb,
		is_verified: data.is_verified,
		is_docs_verified: data.is_docs_verified,
		visa:data,
		complete_service_cost: parseFloat(data.total_complete_cost),
		service_cost: parseFloat(data.total_cost),
		deposit: parseFloat(data.total_deposit),
		payment: parseFloat(data.total_payment),
		refund: parseFloat(data.total_refund),
		discount: parseFloat(data.total_discount),
		balance: parseFloat(data.total_balance),
		collectables: parseFloat(data.collectable),
		total_client_com:parseFloat(data.total_client_com),
		total_agent_com:parseFloat(data.total_agent_com),
		runningBalance: null,
		curBalance: null,
		points: null,
		transfer: null,
		packages: [],
		services: [],
		package_date: null,
		package_cost: null,
		package_deposit: null,
		package_payment: null,
		package_refund: null,
		package_discount: null,
		package_balance: null,
		tracking_selected:null,
		group_package:false,
		tracking_status:null,
		nationality: data.nationality,
		birthcountry: data.birth_country,

		actionlogs: [],
		current_year: null,
		current_month: null,
		current_day: null,
		c_year: null,
		c_month: null,
		c_day: null,
		s_count: 1,
		log_count: 0,

		transactionlogs: [],
		commissionlogs: [],
		current_year2: null,
		current_month2: null,
		current_day2: null,
		c_year2: null,
		c_month2: null,
		c_day2: null,
		s_count2: 1,

		current_year3: null,
		current_month3: null,
		current_day3: null,
		c_year3: null,
		c_month3: null,
		c_day3: null,
		s_count3: 1,

		documentLogs: [],
		documentLogsLoading: false,
		documentLogsDetails: [],
		documentLogsDetailsLoading: false,

		loading: false,
		editloading: false,

		products: [],
		activeprods: [],
		stores: [],
		activestores: [],
		orders: [],
		comporders:[],
		order_prods: [],
		etransactions: [],
		documents: [],
		document_logs:[],
		sales: [],

		or_num: '',
		rcvr_name: '',
		rcvr_contact: '',
		rcvr_orderDate: '',
		total_price : '0',
		tax : '0',
		shipping_fee: '0',
		subtotal: '0',

		currentShoppingUser:'',

		countSold:0,

		delPack: new Form({
            'reason': "",
            'tracking':""
        },{ baseURL  : '/visa/client/'}),

        fundPack: new Form({
            'type': "",
            'reason': "",
            'amount':"",
            'tracking':"",
            'client_id':"",
            'selected_client':"",
            'selected_group':"",
            'authorizer':"",
            'auth_name':"",
			'storage':"",
			'transfer_type':"",
			'branch_id':'',
			'bank_type':'',
			'alipay_reference':'',
			'clearbalance': false
        },{ baseURL  : '/visa/client/'}),


		editServ: new Form({
			'cost': 0,
			'tip': 0,
			'charge': 0,
			'status':"",
			'prev_status':'',
			'active':0,
			'discount':0,
			'reason': "",
			'note': "",
			'id' : null,
			'reporting' : '',
			'extend':'',
			'rcv_docs':'',
			'rcv_docs_released':'',
			'detail':'',
			'exp_date':data.exp_date,
			'arrival_date':data.arrival_date,
			'first_exp_date':data.first_exp_date,
			'icard':data.icard,
			'issue_date':data.issue_date,
			'icard_exp_date':data.icard_exp_date,
			'parent_id':0,
			
			'received_docs': [],
			'docs_needed_array': [],
			'any_acr_icard_docs_needed_array': [],
			'docs_optional_array': [],
			'any_acr_icard_docs_optional_array': []
		},{ baseURL  : '/visa/client/'}),

		'rcvDocsOnHand' : [],
		'rcvDocsReleasedOnHand' : [],

		'required_docs' : [],
		'any_acr_icard_required_docs_array' : [],
		'optional_docs_array' : [],
		'any_acr_icard_optional_docs_array' : [],
		'outcome_docs' : [],
		'outcome_optional_docs' : [],

		cpanel_url : window.Laravel.base_url,
		date_now: new Date(),
		doctype:'',

		validity: new Form({
			'expires_at' : "",
			'status':"",
				'id':"",
				'month':"",
			'day':""
		},{ baseURL  : '/visa/client'}),

		quickReportClientsId: [],
		quickReportClientServicesId: [],
		quickReportTrackings: [],

		dt:'',

		authorizerFrm: new Form({
			'authorizer': '',
			'password': '',
		},{ baseURL  : '/visa/client/'}),

		deleteDocument: new Form({
			'id': '',
		},{ baseURL  : '/visa/service-manager/client-documents/'}),

		setting: user.access_control[0].setting,
		permissions: user.permissions,
		selpermissions: [{
			edit:0,
			actionLogs:0,
			transactionLogs:0,
			documents:0,
			accessControl:0,
			addService:0,
			addFunds:0,
			deleteThisPackage:0,
			editService:0,
			addQuickReport:0
		}],

		files: [],

		report_view : '',
		report_id : '',
		extensionFrm: new Form({
			'form_type'				:'',
			'months_req'			:'',
			'reason_type'			:'',
			'reason_chbx'			:'',
			'others'				:'',
			'acc_num'				:'',
			'auth_rep'				:'',
			'personal_authrep'		:'',
			'lname'					:data.last_name,
			'fname'					:data.first_name,
			'mname'					:data.middle_name,
			'alias'					:'',
			'nationality'			:data.nationality,
			'birthcountry'			:data.birth_country,
			'bday'					:data.birth_date,
			'gender'				:data.gender,
			'cstatus'				:data.civil_status,
			'address'				:data.address,	
			'mobile_no'				:'',
			'express_regular'		:'',
			'height'				:data.height,
			'weight'				:data.weight,
			'cid'                  	:data.id,
            'service_name'          :'',
            'issue_date'			:'',
            'passport'				:data.passport,
            'passport_exp'			:data.passport_exp_date,
            'latest_arrival'		:data.arrival_date,
            'ext_valid_date'		:data.exp_date
		},{ baseURL  : '/visa/client/'}),
		//docrcv: [],
		
	},

	created(){
		// this.loadCollectables();
		// this.getPackageTranslation();
		if(data.branch_id!=3){
			$('.branch_type').hide();
		}
		this.getTransactionLogsTranslation();

		//console.log(this.is_auth);
		if(this.visa.visa_type=='' || this.visa.visa_type==null){
			var url = $(location).attr("href")+'/edit';
			toastr.warning('Visa type is empty. Please fill out the visa type for this client. <br><br> <a href="'+url+'" >click here to update</a>');
		}

		let user_id = data.id; // change to session data
		this.currentShoppingUser = user_id;
		//console.log(data);

		var vm = this;

		// function getServiceCost() {
		// 	return axios.get('/visa/client/clientServiceCost/'+user_id);
		// }

		// function getCompleteServiceCost() {
		// 	return axios.get('/visa/client/clientCompleteServiceCost/'+user_id);
		// }

		// function getDeposit() {
		// 	return axios.get('/visa/client/clientDeposit/'+user_id);
		// }

		// function getPayment() {
		// 	return axios.get('/visa/client/clientPayment/'+user_id);
		// }

		// function getRefund() {
		// 	return axios.get('/visa/client/clientRefund/'+user_id);
		// }

		// function getDiscount() {
		// 	return axios.get('/visa/client/clientDiscount/'+user_id);
		// }

		// function getBalance() {
		// 	return axios.get('/visa/client/clientBalance/'+user_id);
		// }

		// function getCollectables() {
		// 	return axios.get('/visa/client/clientCollectables/'+user_id);
		// }

		function getPoints() {
			return axios.get('/visa/client/clientPointsEarned/'+user_id);
		}

		// function getTransfer() {
		// 	return axios.get('/visa/client/clientTransfer/'+user_id);
		// }

		function getPackages() {
			return axios.get('/visa/client/clientPackages/'+user_id);
		}

		function getServices(tracking) {
			return axios.get('/visa/client/clientServices/'+user_id+'/'+tracking);
		}

		// function getActionLogs() {
		// 	return axios.get('/visa/client/clientActionLogs/'+user_id);
		// }

		// function getTransactionLogs() {
		// 	return axios.get('/visa/client/clientTransactionLogs/'+user_id);
		// }

		// function _getDocumentLogs() {
		// 	return axios.get('/visa/client/clientDocumentLogs/'+user_id);
		// }

		// function _getDocumentLogsDetails() {
		// 	vm.documentLogsDetailsLoading = true;
		// 	return axios.get('/visa/client/clientDocumentLogsDetails/'+user_id);
		// }

		// function getDocuments(){
		// 	return axiosAPIv1.get('/users/'+user_id+'/documents');
		// }

		// function getDocumentLogs(){
		// 	return axiosAPIv1.get('/doclog?user='+user_id);
		// }

		// function getAuthorizers(){
		// 	return axios.get('/visa/client/get-authorizers');
		// }

		// function getFiles(){
		// 	let clientId = user_id;
		// 	return axios.get('/visa/service-manager/client-documents/client-id/'+ clientId);
		// }

		function getTranslation() {
			if(window.Laravel.language == 'cn'){
		    	return axios.get('/visa/client/translation');
		    }
		    else{
		    	return null;
		    }
		}

		function getPackageTranslation() {
			if(window.Laravel.language == 'cn'){
		    	return axios.get('/visa/client/translation/package');
		    }
		    else{
		    	return null;
		    }
		}

		// function getDocumentReceive(){
		// 	let clientId = window.Laravel.data.id;
		// 	return axios.get('/visa/service-manager/client-documents/received/'+ clientId);
		// }

		axios.all([
			getPackages()
			,getServices(0)
			//,getServiceCost()
			//,getCompleteServiceCost()
			//,getDeposit()
			//,getPayment()
			// ,getRefund()
			// ,getDiscount()
			// ,getBalance()
			// ,getCollectables()
			,getPoints()
			//,getTransfer()
			//,getActionLogs()
			//,getTransactionLogs()
			// ,_getDocumentLogs()
			// ,_getDocumentLogsDetails()
			//,getDocuments()
			//,getDocumentLogs()
			// ,getAuthorizers()
			//,getFiles()
			,getTranslation()
			,getPackageTranslation()
		   // ,getDocumentReceive()
		]).then(axios.spread(
			(
				packages,
				services,
				//service_cost,
				//complete_service_cost,
				//deposit,
				//payment,
				// refund,
				// discount,
				// balance,
				//collectables,
				points,
				//transfer,
				//actionlogs,
				//transactionlogs,
				// _documentLogs,
				// _getDocumentLogsDetails,
				//docs,
				//doclogs,
				// authors,
				//clientfiles,
				translation,
				packageTranslation,
				//docrcv,
			) => {
			this.packages 		= packages.data;
			this.services 		= services.data.services;
			this.package_date 		= services.data.package_date;
			this.package_cost 		= services.data.package_cost;
			this.package_deposit 		= services.data.package_deposit;
			this.package_payment 		= services.data.package_payment;
			this.package_refund 		= services.data.package_refund;
			this.package_discount 		= services.data.package_discount;
			this.package_balance 		= services.data.package_balance;
			//this.service_cost 	= service_cost.data;
			//this.complete_service_cost 	= complete_service_cost.data;
			//this.deposit 		= deposit.data;
			//this.payment 		= payment.data;
			// this.refund 		= refund.data;
			// this.discount 		= discount.data;
			// this.balance 		= balance.data;
			// this.collectables 	= collectables.data;
			//this.runningBalance = (deposit.data+payment.data+discount.data)-(refund.data+complete_service_cost.data);
			this.points 		= points.data;
			//this.transfer 		= transfer.data;
			//this.actionlogs 	= actionlogs.data;
			//this.transactionlogs = transactionlogs.data;
			// this.documentLogs = _documentLogs.data;
			// this.documentLogsDetailsLoading = false;
			// this.documentLogsDetails = _getDocumentLogsDetails.data;
			//this.documents 		= docs.data;
			//this.document_logs 	= doclogs.data;
			// this.authorizers 	= authors.data;
			//this.files 	= clientfiles.data;
			this.translation 	= translation.data.translation;
			this.packageTranslation 	= packageTranslation.data.translation;
			//this.docrcv 	= docrcv.data;

		}))
		.catch($.noop);

	},
	methods:{
		// getAuthorizers(){
		// 	axios.get('/visa/client/get-authorizers')
		// 		.then(response => {
		// 			this.authorizers = response.data;
		// 		})
		// },

		getFiles() {
			this.loading = true;
			let user_id = data.id;
			this.files = [];

			axios.get('/visa/service-manager/client-documents/client-id/'+ user_id)
				.then(response =>  {
					this.files = response.data;
					this.loading = false;
					});
		},

		getActionLogs() {
			this.loading = true;
			let user_id = data.id;

			axios.get('/visa/client/clientActionLogs/'+user_id)
				.then(response =>  {
					this.actionlogs = response.data
					this.loading = false;
					});
		},

		getTransactionLogs() {
			this.loading = true;
			let user_id = data.id;
			this.resetTransactionLogsComponentData();

			axios.get('/visa/client/clientTransactionLogs/'+user_id)
				.then(response =>  {
					this.transactionlogs = response.data
					this.loading = false;
					console.log(response.data);
					});
		},

		getCommissionLogs() {
			this.loading = true;
			let user_id = data.id;
			this.resetCommissionLogsComponentData();

			axios.get('/visa/client/clientCommissionLogs/'+user_id)
				.then(response =>  {
					this.commissionlogs = response.data;
					this.loading = false;
					console.log(response.data);
					});
		},

		resetTransactionLogsComponentData() {
			this.transactionlogs = [];
			this.current_year2 = null;
			this.current_month2 = null;
			this.current_day2 = null;
			this.c_year2 = null;
			this.c_month2 = null;
			this.c_day2 = null;
			this.s_count2 = 1;
		},

		resetCommissionLogsComponentData() {
			this.commissionlogs = [];
			this.current_year3 = null;
			this.current_month3 = null;
			this.current_day3 = null;
			this.c_year3 = null;
			this.c_month3 = null;
			this.c_day3 = null;
			this.s_count3 = 1;
		},

		getDocumentLogs() {
			let user_id = data.id;
			
			this.getDocumentLogsDetails();

			this.documentLogsLoading = true;

			axios.get('/visa/client/clientDocumentLogs/'+user_id)
				.then(response => {
					this.documentLogsLoading = false;

					this.documentLogs = response.data;
				});
		},

		getDocumentLogsDetails() {
			let user_id = data.id;

			this.documentLogsDetailsLoading = true;

			axios.get('/visa/client/clientDocumentLogsDetails/'+user_id)
				.then(response => {
					this.documentLogsDetailsLoading = false;

					this.documentLogsDetails = response.data;
				});
		},

		getPackageTranslation() {
			if(window.Laravel.language == 'cn'){
			    axios.get('/visa/client/translation/package')
		        .then(response => {
		          this.packageTranslation = response.data.translation;
		        });
		    }
		},

		// getTranslation() {
		//     axios.get('/visa/client/translation')
		//         .then(response => {
		//           	this.translation = response.data.translation;
		//         });
		// },

		getTransactionLogsTranslation() {
			if(window.Laravel.language == 'cn'){
				axios.get('/visa/client/translation/transaction-logs')
			        .then(response => {
			          	this.transactionLogsTranslation = response.data.translation;
			        });
			    }
		},

		noNumber() {
			var title = (this.translation)
				? this.translation['cannot-add-service-please-fill-up-clients-contact-number']
				: 'Cannot add service, Please fill up client\'s contact number';

			var btnText = (this.translation) ? this.translation['edit-client-profile'] : 'Edit Client Profile';

			swal({
				title: title,
	            type: 'info',
	            allowOutsideClick: false,
	            showCloseButton: true,
				html: "" +
		            "<br>" +
		            '<a href="/visa/client/'+data.id+'/edit" class="btn btn-success btn-lg">' + btnText + '</a>',

				showCancelButton: false,
		        showConfirmButton: false
			});
		},

		fetchClients(q) {
			if(q == data.id){
				$('.popover #fundset').addClass('has-error');
				toastr.error( (this.translation) ? this.translation['cannot-transfer-to-same-client-id'] : 'Cannot transfer to same client id' );
			}
			axiosAPIv1.get('/visa/client/get-visa-clients', {
				params: {
					q: q
				}
			})
			.then(response => {
				if(response.data.length > 0){
					this.clients.push({
					   id:response.data[0].id,
					   name:response.data[0].name });
				}

				setTimeout(() => {
					var my_val = $(".chosen-select-for-clients").val();
					$(".chosen-select-for-clients").val(my_val).trigger("chosen:updated");
				}, 1000);
			});
		},

		// getFiles() {
		// 	let clientId = window.Laravel.data.id;

		// 	axios.get('/visa/service-manager/client-documents/client-id/'+ clientId)
		// 		.then(response => {
		// 			this.files = response.data;
		// 		});
		// },

		saveAuthorizer(){
			this.authorizerFrm.submit('post','/saveAuthorizer')
			.then(result => {
				if(result.status == 'failed'){
					toastr.error( (this.translation) ? this.translation['wrong-password'] : 'Wrong password' );
				}else if(result.status == 'success'){
					this.fundPack.authorizer = this.authorizerFrm.authorizer.id;
					this.fundPack.auth_name = this.authorizerFrm.authorizer.fullname;
					this.fundPack.submit('post','/clientAddFunds')
					.then(result => {
						this.reloadComputation();
						this.showServicesUnder(data.id, this.tracking_selected);
						toastr.success( (this.translation) ? this.translation['successfully-saved'] : 'Successfully saved' );
					})
					.catch(error =>{
						toastr.error( (this.translation) ? this.translation['failed'] : 'Failed' );
					});
					$('#authorizerModal').modal('toggle');
				}
			})
			.catch(error =>{
				toastr.error('Failed');
			});
		},
		getDate(){
			this.dt = $('form#daily-form input[name=date]').val();
			this.editServ.extend = this.dt;
		},
		showServicesUnder(client_id,tracking){
			axios.get('/visa/client/clientServices/'+client_id+'/'+tracking)
				.then(response => {
					this.services = response.data.services;
					this.package_date 		= response.data.package_date;
					this.package_cost 		= response.data.package_cost;
					this.package_deposit 		= response.data.package_deposit;
					this.package_payment 		= response.data.package_payment;
					this.package_refund 		= response.data.package_refund;
					this.package_discount 		= response.data.package_discount;
					this.package_balance 		= response.data.package_balance;
					this.tracking_selected = tracking;
					this.group_package = false;
					if(tracking!=null){					
						if(tracking.slice(0,1) == 'G'){
							this.group_package = true;
						}
					}
					this.tracking_status   = response.data.status;
					this.popOver();
				})
		},

	   showAllServices(){
			var client_id = data.id;
			axios.get('/visa/client/clientServices/'+client_id+'/'+0)
				.then(response => {
					this.services = response.data.services;
					this.package_date         = response.data.package_date;
					this.package_cost         = response.data.package_cost;
					this.package_deposit         = response.data.package_deposit;
					this.package_payment 		= response.data.package_payment;
					this.package_refund 		= response.data.package_refund;
					this.package_discount 		= response.data.package_discount;
					this.package_balance 		= response.data.package_balance;
					this.tracking_selected = null;
					this.group_package = false;
					this.tracking_status = null;
				})
		},

		updateTrackingList(client_id){
			axios.get('/visa/client/clientPackages/'+client_id)
				.then(response => {
					this.packages = null;
					this.packages = response.data;
				})
		},

		addNewPackage(){
			var client_id = data.id;
			axios.get('/visa/client/clientAddPackage/'+client_id)
				.then(response => {
					var track = response.data.tracking;
					this.showServicesUnder(client_id,track);
					this.updateTrackingList(client_id);
					var owl = $("#owl-demo");
					owl.data('owlCarousel').destroy();
				})
		},

        deletePackage(reason){
			let client_id = data.id;
            $(".delete-package").popover('hide');
            this.delPack.tracking = this.tracking_selected;
            if(reason == ''){
            	$('.popover #validset').addClass('has-error');
                toastr.error( (this.translation) ? this.translation['please-indicate-reason-to-delete'] : 'Please indicate reason to delete' );
            }
            else{
            	this.delPack.reason = reason;
            	this.delPack.submit('post','/clientDeletePackage')
            	.then(response => {
            		if(response.status == 'success') {
		                toastr.success( (this.translation) ? this.translation['successfully-saved'] : 'Successfully saved.' );
		                this.showAllServices();
		                this.updateTrackingList(client_id);
		                var owl = $("#owl-demo");
						owl.data('owlCarousel').destroy();
					}
					else{
						toastr.error(response.log);
					}
				})
				.catch(error =>{
					if(error.reason){
						$('.popover #validset').addClass('has-error');
						toastr.error( (this.translation) ? this.translation['please-indicate-reason-to-delete'] : 'Please indicate reason to delete' );
					}else{
					   toastr.error( (this.translation) ? this.translation['update-failed'] : 'Update failed' );
					}
	            })
	            .catch(error =>{
	                if(error.reason){
	                    $('.popover #validset').addClass('has-error');
	                    toastr.error( (this.translation) ? this.translation['please-indicate-reason-to-delete'] : 'Please indicate reason to delete' );
	                }else{
	                   toastr.error( (this.translation) ? this.translation['update-failed'] : 'Update failed' );
	                }
	            });
            }
        },

        loadDeposit() {
			let client_id = data.id;
			axios.get('/visa/client/clientDeposit/'+client_id)
				.then(response => {
					this.deposit = response.data;
				})
		},

		loadRefund() {
			let client_id = data.id;
			axios.get('/visa/client/clientRefund/'+client_id)
				.then(response => {
					this.refund = response.data;
				})
		},

		loadPayment() {
			let client_id = data.id;
			axios.get('/visa/client/clientPayment/'+client_id)
				.then(response => {
					this.payment = response.data;
				})
		},

		loadDiscount() {
			let client_id = data.id;
			axios.get('/visa/client/clientDiscount/'+client_id)
				.then(response => {
					this.discount = response.data;

				})
		},

		loadCost() {
			let client_id = data.id;
			axios.get('/visa/client/clientServiceCost/'+client_id)
				.then(response => {
					this.service_cost = response.data;

				})
		},

		loadCompleteCost() {
			let client_id = data.id;
			axios.get('/visa/client/clientCompleteServiceCost/'+client_id)
				.then(response => {
					this.complete_service_cost = response.data;

				})
		},

		loadBalance() {
			let client_id = data.id;
			axios.get('/visa/client/clientBalance/'+client_id)
				.then(response => {
					this.balance = response.data;
				})
		},
		loadCommissions(){
			let client_id = data.id;
			axios.get('/visa/client/clientCommission/'+client_id)
				.then(response => {
					this.total_client_com = response.data.total_client_com;
					this.total_agent_com = response.data.total_agent_com;
				})
		},

		loadCollectables() {
			let client_id = data.id;
			axios.get('/visa/client/clientCollectables/'+client_id)
				.then(response => {
					this.runningBalance = response.data;
					this.collectables = response.data;
				})
		},

		loadPointsEarned() {
			let client_id = data.id;
			axios.get('/visa/client/clientPointsEarned/'+client_id)
				.then(response => {
					this.points = response.data;
				})
		},

		loadTransfer() {
			let client_id = data.id;
			axios.get('/visa/client/clientTransfer/'+client_id)
				.then(response => {
					this.transfer = response.data;
				})
		},

		editService(service_id) {
			this.editServ.id = service_id;
			axios.get('/visa/client/clientGetService/'+service_id)
				.then(response => {
					var sf_com = parseInt(response.data.charge) + parseInt(response.data.com_client) + parseInt(response.data.com_agent);
					this.editServ.cost = response.data.cost;
					this.editServ.tip = response.data.tip;
					this.editServ.charge = sf_com;
					this.editServ.parent_id = response.data.parent_id;
					if(response.data.deleted_at != null) {
						this.editServ.discount = '';
						this.editServ.reason = '';
					} else {
						this.editServ.discount = response.data.amount;
						this.editServ.reason = response.data.reason;
					}
					this.editServ.status = response.data.status;
					this.editServ.prev_status = response.data.status;
					this.editServ.active = response.data.active;
					this.editServ.extend = response.data.extend;
					this.editServ.detail = response.data.detail;
					// this.editServ.extend = response.data.remarks;

					this.editServ.received_docs = response.data.rcv_docs;
					this.editServ.docs_needed_array = response.data.docs_needed_array;
					this.editServ.any_acr_icard_docs_needed_array = response.data.any_acr_icard_docs_needed_array;
					this.editServ.docs_optional_array = response.data.docs_optional_array;
					this.editServ.any_acr_icard_docs_optional_array = response.data.any_acr_icard_docs_optional_array;
				
					this.rcvDocsOnHand = response.data.rcv_docs;
					this.rcvDocsReleasedOnHand = response.data.rcv_docs_released;

					this.required_docs = response.data.required_docs;
					this.any_acr_icard_required_docs_array = response.data.any_acr_icard_required_docs_array;
					this.optional_docs_array = response.data.optional_docs_array;
					this.any_acr_icard_optional_docs_array = response.data.any_acr_icard_optional_docs_array;
					this.outcome_docs = response.data.outcome_docs;
					this.outcome_optional_docs = response.data.outcome_optional_docs;

					this.editloading = true;
				});
			$('#edit-service-modal').modal('show');
		},

		viewReport(report_id) {
			this.report_id = report_id;
			this.report_view = 'client';
			this.$refs.viewreportref.getReports();
			$('#view-report-modal').modal('show');
		},

		viewReportByService(service_id) {
			this.report_id = service_id;
			this.report_view = 'service';
			this.$refs.viewreportref.getReports();
			$('#view-report-modal').modal('show');
		},

		reloadTransactionLogs() {
			let user_id = data.id;

			axios.get('/visa/client/clientTransactionLogs/'+user_id)
				.then(response => this.transactionlogs = response.data);
		},

        addFunds(type,amount,reason, storage,transfer_type,branch_id, bank_type, alipay_reference,clearbalance){
			// this.authorization();
			// console.log(this.is_auth);
	
			if(type == 'discount' && (user.id != '1193' && user.id != '1' && user.id != '2352')) {
				var title = (this.translation)
					? this.translation['please-contact-sir-jason-to-give-discount-to-client']
					: 'Please contact sir Jason to give discount to client';

				swal({
					title: title,
					type: 'info',
					allowOutsideClick: false,
					showCloseButton: true,
					showCancelButton: false,
					showConfirmButton: false
				});
			}
			else if((type == 'refund' || type == 'deposit' || type == 'payment') && (user.id != '1193' && user.id != '9' && user.id != '1' && user.id != '2' && user.id != '10552' && user.id != '2352')){
						swal({
							title: "Please contact Maam Cha to give "+type+" to client.",
							type: 'info',
							allowOutsideClick: false,
							showCloseButton: true,
							showCancelButton: false,
							showConfirmButton: false
						});
			}
			else{
				
				if ((type == 'refund' || type == 'discount' || type == 'transfer')&& reason.trim() == '') {
						$('.popover #fundset').addClass('has-error');
	            		toastr.error( (this.translation) ? this.translation['please-input-reason'] : 'Please input reason' );
	        	}

	        	else if(amount == ''){
	            	$('.popover #fundset').addClass('has-error');
	                toastr.error( (this.translation) ? this.translation['please-input-amount-to-add'] : 'Please input amount to add' );
	            }
	            else if(type == 'transfer' && this.fundPack.selected_client == '' && transfer_type == 'client'){
		            	$('.popover #fundset').addClass('has-error');
		                toastr.error( (this.translation) ? this.translation['please-select-client-to-proceed-transfer'] : 'Please select client to proceed transfer' );
	            }
	            else if(type == 'transfer' && this.fundPack.selected_group == '' && transfer_type == 'group'){
						$('.popover #fundset').addClass('has-error');
		                toastr.error( (this.translation) ? this.translation['please-select-group-to-proceed-transfer'] : 'Please select group to proceed transfer' );
	            }
	            else{
					if(storage=='alipay' && (alipay_reference=='' || alipay_reference==null)){
						$('.popover #fundset').addClass('has-error');
		                toastr.error('Reference number cannot be empty');
					}else{

						this.fundPack.type = type;
						this.fundPack.amount = amount;
						this.fundPack.tracking = this.tracking_selected;
						this.fundPack.client_id = data.id;
						this.fundPack.reason = reason;
						this.fundPack.storage = storage;
						this.fundPack.transfer_type = transfer_type;
						this.fundPack.branch_id = branch_id;
						this.fundPack.bank_type = bank_type;
						this.fundPack.alipay_reference = alipay_reference;
						this.fundPack.clearbalance = clearbalance;
						// this.fundPack.selected_client = selected_client;

						$(".add-funds").popover('hide');
							this.fundPack.submit('post','/clientAddFunds')
								.then(result => {
									if(result.status == 'success') {
										this.reloadTransactionLogs();
										this.reloadComputation();
										this.reloadTransactionLogs();
										this.showServicesUnder(data.id, this.tracking_selected);
										toastr.success(result.log);
									}
									else{
										toastr.error(result.log);
									}

									//console.log(result);
								})
								.catch(error =>{});
					}	
	            }
		    }
        },

        authorization(){

			var roles = this.processor.roles;
			var is_auth = false;
			roles.map(function(e) {
				console.log('ID:'+e.id);
				console.log('NAME:'+e.name);
				if(e.name == 'authorizer'){
					is_auth = true;
				}
			});
			this.is_auth = is_auth;
			console.log(this.is_auth);
		},

		reloadComputation(){
			this.loadCost();
			this.loadCompleteCost();
			this.loadDeposit();
			this.loadDiscount();
			this.loadRefund();
			this.loadPayment();
			this.loadBalance();
			this.loadTransfer();
			this.loadPointsEarned();
			this.loadCollectables();
			this.loadCommissions();
		},

		popOver(){
		    $(".delete-package")
            .popover({
              html:true,
              container: 'body',
              content: function () {
                return $('#deletePackagePop').html();
              },
            });

      var $fundsPopOver = $(".add-funds")
		    .popover({
		      html:true,
		      container: 'body',
		      content: function () {
		        return $('#addFundsPop').html();
		      },
		    });

		          // on show popover
	 		$fundsPopOver.on("shown.bs.popover", function(e) {
	 			$("body").tooltip({ selector: '[data-toggle=tooltip]' });
	 		    $('input.typeahead').typeahead({
		              source:  function (query, process) {
		              	if(query == data.id){
							// $('.popover #fundset').addClass('has-error');
							toastr.error("Cannot transfer to same client id.");
						}
						else{
							return $.get('/visa/client/search/transfer-balance/'+data.branch_id+'/'+data.id, { query: query }, function (data) {
									data = $.parseJSON(data);
									return process(data);
								});
							}
						},
					  minLength: 2,
					  fitToElement: true,
					  matcher: function(item) {
							return true;
					  },
					  updater:function (item) {
						  ClientApp.fundPack.selected_client =item.id;
						  return item;
					  }
				});


				$('input.grouptypeahead').typeahead({
		              source:  function (query, process) {
		    //           	if(query == data.id){
						// 	$('.popover #fundset').addClass('has-error');
						// 	toastr.error("Cannot transfer to same client id.");
						// }
						// else{
							return $.get('/visa/client/search/group-transfer-balance/'+data.branch_id+'/0', { query: query }, function (data) {
									data = $.parseJSON(data);
									return process(data);
								});
							// }
						},
					  minLength: 2,
					  fitToElement: true,
					  matcher: function(item) {
							return true;
					  },
					  updater:function (item) {
						  ClientApp.fundPack.selected_group =item.id;
						  return item;
					  }
				});
	  //   	$('.chosen-select-for-clients').chosen({
			// 	width: "100%",
			// 	no_results_text: "Searching Client # : ",
			// 	search_contains: true,
			// 	// max_selected_options: 1
			// });

	  //   	// when using chosen inside popover, call first popover class
			// $('body').on('keyup', '.chosen-container input[type=text]', (e) => {
			// 	let q = $('.popover .chosen-container input[type=text]').val();
			// 	if( q.length >= 4 ){
			// 		ClientApp.fetchClients(q);
			// 	}
			// });
		});

		$fundsPopOver.on('hidden.bs.popover', function() {
			$('.freason').hide();
			$('.transferTo').hide();
			$('.transGroup').hide();
			$('.bank_type').hide();
			$('.alipay_reference').hide();
			$('.clearbal').hide();
			$('.chosen-select-for-clients').chosen('destroy');
			
		});

			$(".add-verification")
			.popover({
			  html:true,
			  container: 'body',
			  content: function () {
				ClientApp.validity.id = $(this).attr('id');
				ClientApp.doctype = $(this).attr('data-docs');
				return $('#frmValidUntil').html();
			  },
			});
		},

		saveValidity(dateSel){

			$(".verify").popover('hide');
			this.validity.status = "Verified";
			this.validity.expires_at = dateSel;
			var month = parseInt(dateSel.substr(5,2));
			var day = parseInt(dateSel.substr(8,2));

			if (month != 0){
			   if(day != 0){
					this.validity.submit('post', '/verify-docs')
					.then(result => {

						if(this.doctype=='9gICard'){
							this.ninegicard_expiry = dateSel;
							var d = new Date(this.ninegicard_expiry);
							this.ninegicard_compare = d < this.date_now;
						}

						if(this.doctype=='9gOrder'){
							this.ninegorder_expiry = dateSel;
							var d = new Date(this.ninegorder_expiry);
							this.ninegorder_compare = d < this.date_now;
						}

						if(this.doctype=='prvCard'){
							this.prvcard_expiry = dateSel;
							var d = new Date(this.prvcard_expiry);
							this.prvcard_compare = d < this.date_now;
						}

						if(this.doctype=='prvOrder'){
							this.prvorder_expiry = dateSel;
							var d = new Date(this.prvorder_expiry);
							this.prvorder_compare = d < this.date_now;
						}

						if(this.doctype=='srrvCard'){
							this.srrvcard_expiry = dateSel;
							var d = new Date(this.srrvcard_expiry);
							this.srrvcard_compare = d < this.date_now;
						}

						if(this.doctype=='srrvVisa'){
							this.srrvvisa_expiry = dateSel;
							var d = new Date(this.srrvvisa_expiry);
							this.srrvvisa_compare = d < this.date_now;
						}

						if(this.doctype=='aep'){
							this.aep_expiry = dateSel;
							var d = new Date(this.aep_expiry);
							this.aep_compare = d < this.date_now;
						}

						if(this.doctype=='nbi'){
							this.nbi_expiry = dateSel;
							var d = new Date(this.nbi_expiry);
							this.nbi_compare = d < this.date_now;
						}


						$("#"+this.validity.id).popover('hide');
						toastr.success( (this.translation) ? this.translation['successfully-saved'] : 'Successfully saved' );
					})
					.catch(error => {
						 if(error.expires_at){
							$('.popover #validset').addClass('has-error');
							toastr.error( (this.translation) ? this.translation['please-use-valid-date-of-expiry'] : 'Please use valid date of expiry' );
						}else{
						   toastr.error( (this.translation) ? this.translation['update-failed'] : 'Update failed' );
						}
						$("#"+this.validity.id).popover('show');
					});
			   }else{
					toastr.error( (this.translation) ? this.translation['please-use-valid-date-of-expiry'] : 'Please use valid date of expiry' );
			   }
			}else{
				toastr.error( (this.translation) ? this.translation['please-use-valid-date-of-expiry'] : 'Please use valid date of expiry' );
			}
		},

		fetchDocs(id) {
			// this.$refs.multiplequickreportref.fetchDocs(id);
		},

		fetchDocs2(id) {
			this.$refs.editserviceref.fetchDocs(id);
		},

		fetchDocs3(service_id,needed,optional) {
			this.$refs.addnewserviceref.fetchDocs(service_id,needed,optional);
		},

		deletePrompt(id){
			this.deleteDocument.id = id;
			$('#dialogRemoveDocs').modal('toggle');
		},

		deleteDoc(){
			var id = this.deleteDocument.id;
			this.deleteDocument.submit('post', '/delete-docs/' + id)
	        .then(response => {
	       	 	let { success, message } = response;
	        	if(success) {
					toastr.success(message);
				}else{
					toastr.error(messsage);
				}
				this.getFiles();

	        })
	        .catch(error => {
			    toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete failed' );
			});
		},

		exportPdf(){
			this.extensionFrm.issue_date = $('input#issue_date').val();

            if ((this.extensionFrm.lname == '') || (this.extensionFrm.fname == '') ||
            	(this.extensionFrm.nationality == '') || (this.extensionFrm.nationality == null) ||
            	(this.extensionFrm.birthcountry == '') || (this.extensionFrm.birthcountry == null) ||
            	(this.extensionFrm.bday == '') || (this.extensionFrm.bday == null)||
            	(this.extensionFrm.address == '') || (this.extensionFrm.height == '') ||
            	(this.extensionFrm.weight == '') || (this.extensionFrm.issue_date == '') ||
            	(this.extensionFrm.gender == '') || (this.extensionFrm.cstatus == '') || 
            	(this.extensionFrm.passport == '') || (this.extensionFrm.passport == null) ||
            	(this.extensionFrm.passport_exp == '') || (this.extensionFrm.latest_arrival == '') || 
            	(this.extensionFrm.passport_exp == null) || (this.extensionFrm.ext_valid_date == ''))
            {
            	if(this.extensionFrm.lname == ''){
	            	$('#frm_lname').addClass("has-error");
	            }else{
	            	$('#frm_lname').removeClass("has-error");
	            }

	            if(this.extensionFrm.fname == ''){
	            	$('#frm_fname').addClass("has-error");
	            }else{
	            	$('#frm_lname').removeClass("has-error");
	            }

	            if((this.extensionFrm.nationality == '' ) || (this.extensionFrm.nationality == null)){
	            	$('#frm_nationality').addClass("has-error");
	            }else{
	            	$('#frm_nationality').removeClass("has-error");
	            }

	            if((this.extensionFrm.birthcountry == '') || (this.extensionFrm.birthcountry == null)){
	            	$('#frm_birthcountry').addClass("has-error");
	            }else{
	            	$('#frm_birthcountry').removeClass("has-error");
	            }

	           	if((this.extensionFrm.bday == '') || (this.extensionFrm.bday == null)){
	            	$('#frm_bday').addClass("has-error");
	            }else{
	            	$('#frm_bday').removeClass("has-error");
	            }

	           	if(this.extensionFrm.gender == ''){
	            	$('#frm_gender').addClass("has-error");
	            }else{
	            	$('#frm_gender').removeClass("has-error");
	            }

	            if(this.extensionFrm.cstatus == ''){
	            	$('#frm_cstatus').addClass("has-error");
	            }else{
	            	$('#frm_cstatus').removeClass("has-error");
	            }

	            if(this.extensionFrm.address == ''){
	            	$('#frm_address').addClass("has-error");
	            }else{
	            	$('#frm_address').removeClass("has-error");
	            }

	            if(this.extensionFrm.height == ''){
	            	$('#frm_height').addClass("has-error");
	            }else{
	            	$('#frm_height').removeClass("has-error");
	            }

	            if(this.extensionFrm.weight == ''){
	            	$('#frm_weight').addClass("has-error");
	            }else{
	            	$('#frm_weight').removeClass("has-error");
	            }

	            if(this.extensionFrm.issue_date == ''){
	            	$('#frm_issue_date').addClass("has-error");
	            }else{
	            	$('#frm_issue_date').removeClass("has-error");
	            }
 
	            if(this.extensionFrm.passport == '' || this.extensionFrm.passport == null){
	            	$('#frm_passport').addClass("has-error");
	            }else{
	            	$('#frm_passport').removeClass("has-error");
	            }

	            if(this.extensionFrm.passport_exp == '' || this.extensionFrm.passport_exp == null){
	            	$('#frm_passport_exp').addClass("has-error");
	            }else{
	            	$('#frm_passport_exp').removeClass("has-error");
	            }

	            if(this.extensionFrm.latest_arrival == '' || this.extensionFrm.latest_arrival == null){
	            	$('#frm_latest_arrival').addClass("has-error");
	            }else{
	            	$('#frm_latest_arrival').removeClass("has-error");
	            }

	            if(this.extensionFrm.ext_valid_date == '' || this.extensionFrm.ext_valid_date == null ){
	            	$('#frm_ext_valid_date').addClass("has-error");
	            }else{
	            	$('#frm_ext_valid_date').removeClass("has-error");
	            }

	            toastr.info('Please update client\'s information on Edit Profile.');
            }else{

				this.extensionFrm.submit('post', '/export-pdf')
				.then(result => {
					let { success, filename } = result;

					if(success) {
	                    let href = window.location.origin + '/' + filename;
	                    $('.load-pdf').attr('href', href);
	                    $('.load-pdf')[0].click();
	                    $('#extensionform').modal('hide');
	                }
				})
				.catch(error => {
					console.log(error);
					toastr.error( (this.translation) ? this.translation['failed'] : 'Failed' );
				});
            }
		},
		setAuthorizedReps(id){
			if((id == 355 || id == 349 || id == 350)){
				this.extensionFrm.auth_rep = "";
				this.extensionFrm.acc_num = "";
				this.is_rush = true;
			}else if (id == 419 || id == 418){
				this.is_rush = false;
				if(this.branch == 1 || this.branch == 3){
					var sel = $('#auth_rep2').val("BORBON, MORIELLE G.");
					this.extensionFrm.auth_rep = "BORBON, MORIELLE G.";
				}else if(this.branch == 2){
					this.extensionFrm.auth_rep = "MALIWAT, SARAH JANE";
				}
				this.extensionFrm.acc_num = "CA-2019-058-A";
			}
		},

		magnify(path) {
			//$('img.lb-image').addClass("zoom");
			//$('img.lb-image').attr("src", path);
     		//$('img.lb-image').attr("data-magnify-src", path);

			$('div.lb-nav').remove();

		 //    setTimeout(() => {
			// 	$('.zoom').magnify();

			// 	setTimeout(() => {
			// 		$('div.magnify-lens').css('display', 'inline');
			// 		$('.magnify').css('overflow','hidden');
			// 	}, 200);
			// }, 1000);
		}

	},
	 mounted () {
		var vm = this;

		$('body').popover({ // Ok
			html:true,
			trigger: 'hover',
			selector: '[data-toggle="popover"]'
		});

		// $('.chosen-select-for-clients').chosen({
		// 	width: "100%",
		// 	no_results_text: "Searching Client # : ",
		// 	search_contains: true
		// });

		$('.chosen-select-for-clients').on('change', () => {
			let clientIds = $('.chosen-select-for-clients').val();

			//this.addNewMemberForm.clientIds = clientIds;
		});

		$('#blueimp-gallery')
		 	.on('open', function (event) {
		        // Gallery open event handler
		    })

	},
	computed:{
		pack_date(){
			var c = moment(String('2017-01-01')).format("Y-m-d");
			var d = moment(String(this.package_date)).format("Y-m-d");
			return (c < d ? true : false);
		},
		isDisabled(){
	      return (this.serv.active > 0 ? "":"text-decoration: line-through;");
	    },
	},
	watch: {
		// whenever bday changes, this function will run
		bday: function (newVal) {
		  this.upStore(newVal,'bday');
		}

	},
	updated() {
		this.popOver();

		// Fired every second, should always be true
		var owl = $(".trackings");
		owl.owlCarousel({
			items : 4,
			lazyLoad : true,
			navigation : false,
			pagination :false,
			rewindNav:false
		});
		$(".next").click(function(){
			owl.trigger('owl.next');
		});
		$(".prev").click(function(){
			owl.trigger('owl.prev');
		});
	 //    $('#lists').DataTable({
		//     responsive: true,
		//     "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		//     "iDisplayLength": 25
		// });

	},
	directives: {
		datepicker: {
			bind(el, binding, vnode) {
				$(el).datepicker({
					format: 'yyyy-mm-dd'
				}).on('changeDate', (e) => {
					ClientApp.$set(ClientApp.validity, 'expires_at', e.format('yyyy-mm-dd'));
				});

			}
		}
	},
	components: {
		Multiselect
		// ,'multiple-quick-report': require('../components/Visa/MultipleQuickReport.vue')
		,'multiple-quick-report-v2': require('../components/Visa/MultipleQuickReportV2.vue')
		,'client-documents': require('../components/Visa/ClientDocuments.vue')
	}

});

$(document).ready(function () {
	
	document.title = ClientApp.fullname;

	$('#birth_date').datepicker();
	$('.underlined').hide();
	$('.freason').hide();
	$('.transferTo').hide();
	$('.transGroup').hide();
	$('.bank_type').hide();
	$('.alipay_reference').hide();
	$('.clearbal').hide();


	if($('.popover #branch_id :selected').val()==1){
		$('.eastwest').hide();
	}else{
		$('.aub').hide();
		$('.metrobank').hide();
		$('.securitybank').hide();
	}

	$('.dual_select').bootstrapDualListbox({
		selectorMinimalHeight: 160,
	});

	$(".se-pre-con2").fadeOut("slow");

	$(".delete-package")
	.popover({
	  html:true,
	  container: 'body',
	  content: function () {
		return $('#deletePackagePop').html();
	  },
	});

	$(".add-funds")
	.popover({
	  html:true,
	  container: 'body',
	  content: function () {
		return $('#addFundsPop').html();
	  },
	});

	$('#view-report-modal').on('hidden.bs.modal', function () {
	   ClientApp.reports = [];
	})

    $("#lightbox .lb-container").mousemove(function(){
      //console.log('hovering');

      //var src = $("img.lb-image").attr("src");

       //$('img.lb-image').addClass("zoom");
       //$('img.lb-image').attr("data-magnify-src", src);

    //    setTimeout(() => {
    //     $('.zoom').magnify();

    //     $('div.magnify-lens').css('display', 'block');
    //   $('.magnify').css('overflow', 'hidden');
    // }, 500);
    });
});

$(document).on("change", ".popover #branch_id", function() {
	if($(this).val()==1){
		if($('.popover #fund_storage').val()=='alipay'){
			$('.eastwest').hide();
			$('.metrobank').hide();
			$('.securitybank').hide();
			$('.aub').show();
			$('.popover #bank_id').val('aub');
		}else{
			$('.eastwest').hide();
			$('.metrobank').show();
			$('.securitybank').show();
			$('.aub').show();
			$('.popover #bank_id').val('metrobank');
		}
	}else{
		
		if($('.popover #fund_storage').val()=='alipay'){
			$('.eastwest').show();
			$('.metrobank').hide();
			$('.securitybank').hide();
			$('.aub').hide();
			$('.popover #bank_id').val('eastwest');
		}else{
			$('.eastwest').show();
			$('.metrobank').hide();
			$('.securitybank').hide();
			$('.aub').hide();
			$('.popover #bank_id').val('eastwest');
		}
	}
	
});

$(document).on("click", ".del-pack", function() {
	var reason = $('.popover #reason').val();
	ClientApp.deletePackage(reason);
});

$(document).on("change", "#fund_type", function() {
	var type = $('.popover #fund_type').val();
	$('.alipay').hide();
	$('.storage_type').show();

	if(type == "discount"){
		$('.clearbal').show();
	}
	else{
		$('.clearbal').hide();
	}

	if(type == "discount" || type == "refund"){
		$('.freason').show();
		$('.transferTo').hide();
		$('.popover #fund_storage').val('cash');
		$('.popover #fund_storage').trigger("change");
	}
	else if(type== "transfer"){
		$('.freason').show();
		$('.transferTo').show();
		$('.transGroup').hide();
		$('.fund_storage').hide();
		$('.storage_type').hide();
		$('.bank_type').hide();
		$('.alipay_reference').hide();
	}
	else{
		if(type== "deposit" || type== "payment"){
			$('.alipay').show();
		}
		$('.freason').hide();
		$('.transferTo').hide();
		$('.popover #fund_reason').val('');
	}

	// For showing/hiding of branch type field
	if(type == "discount" || type== "transfer") $('.branch_type').hide();
	else $('.branch_type').show();
});

$(document).on("change", "#fund_storage", function() {
	
	if($('.popover #branch_id :selected').val()==1){
		$('.eastwest').hide();
		$('.metrobank').show();
		$('.securitybank').show();
		$('.aub').show();
		$('.alipay_reference').hide();
		var fstorage = $('.popover #fund_storage').val();
		if(fstorage=='bank'){
			$('.bank_type').show();
		}else if(fstorage=='alipay'){
			$('.bank_type').show();
			$('.metrobank').hide();
			$('.securitybank').hide();
			$('.aub').show();
			$('.popover #bank_id').val('aub');
			$('.alipay_reference').show();
		}else{
			$('.bank_type').hide();
		}
	}else{
		$('.popover #bank_id').val('eastwest');
		$('.metrobank').hide();
		$('.securitybank').hide();
		$('.aub').hide();
		$('.eastwest').show();
		$('.alipay_reference').hide();
		var fstorage = $('.popover #fund_storage').val();
		if(fstorage=='bank'){
			$('.bank_type').show();
		}else if(fstorage=='alipay'){
			$('.bank_type').show();
			$('.metrobank').hide();
			$('.securitybank').hide();
			$('.aub').hide();
			$('.popover #bank_id').val('eastwest');
			$('.alipay_reference').show();
		}else{
			$('.bank_type').hide();
		}
	}
});



$(document).on("change", "#transfer_type", function() {
	var transTo = $('.popover #transfer_type').val();
		if(transTo == 'client'){
			$('.transGroup').hide();
			$('.transClient').show();
		}
		else if(transTo == 'group'){
			$('.transGroup').show();
			$('.transClient').hide();
		}
});

$(document).on("click", ".funds-add", function() {
	var branch_id = ((data.branch_id!=3) ? data.branch_id : $('.popover #branch_id :selected').val());
    var reason = $('.popover #fund_reason').val();
    var amount = $('.popover #fund_amount').val();
    var type = $('.popover #fund_type').val();
	var storage = $('.popover #fund_storage').val();
	var transfer_type = $('.popover #transfer_type').val();
	var bank_type = $('.popover #bank_id').val();
	var alipay_reference = $('.popover #alipay_reference').val();
	var alipay_reference = $('.popover #alipay_reference').val();
	var clearbalance = 0;
	if($("input[name='clearbalance']:checked:visible").val() == 'on'){
		var clearbalance = 1;
	}

     //alert(reason+" "+amount+" "+type+" "+storage);
    ClientApp.addFunds(type,amount,reason, storage,transfer_type,branch_id, bank_type, alipay_reference,clearbalance);
});

$(document).on("click", "#fullBalance", function() {
	$('.popover #fund_amount').val(Math.abs(ClientApp.balance));
});

$(document).on("click", ".save-verification", function() {
	var dateSel = $('.popover #expires_at').val();
	ClientApp.saveValidity(dateSel);
});

$(document).on('click', function (e) {
	$('[data-toggle="popover"],[data-original-title]').each(function () {
		if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
			(($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
		}
	});
});



$(document).on('focus.autoExpand', 'textarea.autoExpand', function(){
		var savedValue = this.value;
		this.value = '';
		this.baseScrollHeight = this.scrollHeight;
		this.value = savedValue;
	})
	.on('input.autoExpand', 'textarea.autoExpand', function(){
		var minRows = this.getAttribute('data-min-rows')|0, rows;
		this.rows = minRows;
		rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 17);
		this.rows = minRows + rows;
	});

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
	event.preventDefault();
	$(this).ekkoLightbox();

});

$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});
