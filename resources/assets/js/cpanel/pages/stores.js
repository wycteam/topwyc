Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

let data = window.Laravel.data;

var FeedbackApp = new Vue({
	el:'#storepage',
	data:{
		stores: [],
		featuredstores: [],
		selStore : '',
		created_at: '',
		index : '',
		main_url : "http://"+window.Laravel.base_url,
		count: 0,

	},
	created(){
		this.getStore();
	},
	methods:{
		upStore(store_id,req){
			axiosAPIv1.put('/stores/'+store_id,{
					req:req						
				})
				.then(response => {
					if(response.status == 200)
					{
						$('.ibox').children('.ibox-content').toggleClass('sk-loading');
						this.stores.filter(obj => {
							if(obj.id == store_id)
							{
								switch(req){
									case 'promote':
										obj.status = 'featured';
										toastr.success('Store Promoted!');
										$('.dt-stores').DataTable().destroy();
										this.callDataTables();
										break;
									case 'demote':
										obj.status = null;
										toastr.success('Store Demoted!');
										$('.dt-stores').DataTable().destroy();
										this.callDataTables();
										break;
									case 'delete':
										obj.deleted_at = 0;
										toastr.success('Store Deleted!');
										$('.dt-stores').DataTable().destroy();
										this.callDataTables();
										break;
									case 'deactivate':
										obj.is_active = 0;
										toastr.success('Store Deactivated!');
										$('.dt-stores').DataTable().destroy();
										this.callDataTables();
										break;
									case 'activate':
										obj.is_active = 1;
										toastr.success('Store Activated!');
										$('.dt-stores').DataTable().destroy();
										this.callDataTables();
										break;
									case 'restore':
										obj.deleted_at = null;
										toastr.success('Store Restored!');
										$('.dt-stores').DataTable().destroy();
										this.callDataTables();
										break;
									case 'permanent':
										this.$delete(this.stores, this.index);
										toastr.success('Store Remove from the database!');
										break;
									default:
										toastr.danger('Try Again!');


								}
							}
						})
					}
				})
		},
		//remove functions
		showRemoveDialog(store){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selStore = store;
			this.created_at = store.created_at.date;
			$('#dialogRemove').modal('toggle');
		},
		deleteStore(id){
			let req = 'delete';
			this.upStore(id,req);
		},
		//deactivate functions
		showDeacDialog(store){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selStore = store;
			this.created_at = store.created_at.date;
			$('#dialogDeac').modal('toggle');
		},
		deacStore(id){
			let req = 'deactivate';
			this.upStore(id,req);
		},
		//demote functions
		showDemoteDialog(store){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selStore = store;
			this.created_at = store.created_at.date;
			$('#dialogDemote').modal('toggle');
		},
		demoteStore(id){
			let req = 'demote';
			this.upStore(id,req);
		},
		//restore functions
		showRecycleDialog(store){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selStore = store;
			this.created_at = store.created_at.date;
			$('#dialogRecycle').modal('toggle');
		},
		recycleStore(id){
			let req = 'restore';
			this.upStore(id,req);
		},
		//delete perma  functions
		showPermaDialog(store,index){
			$('.ibox').children('.ibox-content').toggleClass('sk-loading');
			this.selStore = store;
			this.created_at = store.created_at.date;
			this.index = index;
			$('#dialogPermaDelete').modal('toggle');
		},
		PermaDeletetore(id){
			let req = 'permanent';
			this.upStore(id,req);
		},

		//get all stores
		getStore(){
			function getAllStores() {
				$('.ibox').children('.ibox-content').toggleClass('sk-loading');
				return axiosAPIv1.get('/stores');
			}
			function getFeaturedStores() {
				return axiosAPIv1.get('/featuredstores');
			}
	    	axios.all([
				getAllStores(),
				getFeaturedStores()
			]).then(axios.spread(
				(
					 stores,
					 featuredstores
				) => {
				this.stores = stores.data;
				this.featuredstores = featuredstores.data;

            	let count = 0;
                this.stores.forEach(function (store) {
                    if(store.is_active==1 && stores.status == 'featured'){
                        ++count;
                    }
                });
                this.count = count
        
			}))
			.catch($.noop);
			this.callDataTables();

		},

		callDataTables(){
			// if ( $.fn.DataTable.isDataTable('.dt-stores') ) {
			//   $('.dt-stores').DataTable().destroy();
			// }
			setTimeout(function(){
				$('.dt-stores').DataTable({
		            pageLength: 10,
		            responsive: true,
		            dom: '<"top"lf>r<"clear">t<"bottom"p>',
		            order: [[ 0, "desc" ]],
		            scrollY: "500px",
		            autoWidth: false,
		        });
			 },500);
			 
		},
	},
	watch:{
		stores:function(){
	  		$('.dt-stores').DataTable().destroy();
			this.callDataTables();
		},
	},
	updated() {
		$('.ibox').children('.ibox-content').toggleClass('sk-loading');
	}

});


// $(document).ready( function () {
//   var table = $('.dt-stores').DataTable({
//     "order": [[ 0, "desc" ]]
//   });
// } );