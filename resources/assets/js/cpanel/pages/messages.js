import WycMessage from '../components/WycMessage.vue'
import WycUserSearch from '../components/WycUserSearch.vue'

new Vue({
    el: '#chatbox',

    data: {
        userLatestMessages: [],
        selected: null
    },

    router: new VueRouter({
        routes: [
            {
                path: '/u/:id',
                name: 'user',
                component: require('../components/WycChatbox.vue')
            },
            {
                path: '/cr/:id',
                name: 'chat-room',
                component: require('../components/WycChatboxRoom.vue')
            }
        ]
    }),

    watch: {
        userLatestMessages(val) {
            setTimeout(() => {
                $('#user-message').slimScroll({
                    height: '100%'
                });
            }, 1);
        }
    },

    methods: {
        getLatestMessages() {
            axiosAPIv1.get('/messages')
                .then(result => {
                    console.log(result);
                    this.userLatestMessages = result.data;
                });
        },

        loadSelectedUser(data) {
            this.selected = data;

            this.$router.push({
                name: 'user',
                params: {
                    id: data.id
                }
            });
        },

        loadSelectedChatRoom(data) {
            this.selected = data;

            this.$router.push({
                name: 'chat-room',
                params: {
                    id: data.id
                }
            });
        },

        newMessage() {
            Event.fire('user-search.show');
        }
    },

    components: {
        WycMessage,
        WycUserSearch
    },

    created() {
        this.getLatestMessages();

        Event.listen('user-search.selected', this.loadSelectedUser);
        Event.listen('chat-room.selected', this.loadSelectedChatRoom);
        Event.listen('chat-box.send', this.getLatestMessages);
        Event.listen('chat-box.marked-as-read', this.getLatestMessages);
        Event.listen('new-chat-message', this.getLatestMessages);
    }
});
