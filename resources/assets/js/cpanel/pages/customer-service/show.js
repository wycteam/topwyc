import Multiselect from 'vue-multiselect'
import WycModal from '../../../components/Modal.vue'

let data = window.Laravel.data;

new Vue({
    el: '#customer-service',

    data: {
        supportUsers: data.all,
        issue: data.issue,
        form: new Form({
            id: data.issue.id,
            subject: data.issue.subject,
            body: data.issue.body,
            status: data.issue.status,
            supportIds: [],
        }, { baseURL: '/' }),
        newConvoForm: new Form({
            supportIds: [],
        },{ baseURL: axiosAPIv1.defaults.baseURL }),
        issuer : data.issuer,
        details: null
    },

    methods: {
        edit() {
            $('#issue-modal').modal({
                backdrop: 'static'
            });

            // $('#wyc-modal').modal('hide');
        },

        getSupportUsers() {
            let chatRoom = this.issue.chat_room;

            if (chatRoom) {
                return chatRoom.users.filter(user => {
                    return user.id != this.issue.user_id;
                });
            }

            return [];
        },

        getIds(users) {
            return users.map(user => {
                return user.id;
            });
        },

        submit() {
            this.form.submit('put', '/customer-service/' + this.form.id)
                .then(result => {
                    window.location.reload();
                })
                .catch($.noop);
        },

        createConvo() {
            if(this.issuer.id!=Laravel.user.id){
                this.newConvoForm.supportIds.push(Laravel.user.id);
                this.newConvoForm.submit('put', '/customer-service/' + data.issue.id)
                .then(result => {
                    toastr.success('Successfully Submitted!');
                    this.newConvoForm.supportIds = [];
                    location.reload();
                })
                .catch($.noop);
            }
            else{
                toastr.error('You are also the issuer!');
            }

        },

        getDetails(issueNumber) {
            axiosAPIv1.get('/get-details/' + issueNumber)
            .then(response => {
                this.details = response.data.data;
            });
        },

        showVideoEvidenceModal(e, index) {
            e.preventDefault();

            setTimeout(function() {
                $('a#video-evidence-'+index).ekkoLightbox({ 
                    wrapping: false, 
                    alwaysShowClose: true
                });
            }, 1000);
        }
    },

    computed: {
        selectedSupports: {
            get() {
                return this.getSupportUsers();
            },

            set(newValue) {
                this.form.supportIds = this.getIds(newValue);
            }
        }
    },

    components: {
        Multiselect,
        WycModal
    },

    created() {
        this.getDetails(this.issue.issue_number);

        this.form.supportIds = this.getIds(this.getSupportUsers());
    },

    mounted() {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    }
});
