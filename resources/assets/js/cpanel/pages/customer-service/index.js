import Paginate from 'vuejs-paginate'
import WycIssue from '../../components/WycIssue.vue'

new Vue({
    el: '#customer-service',

    data: {
        issues: [],
        page: 1,
        pageCount: 1,
        search: null,
        timeout: null
    },

    watch: {
        search(val) {
            clearTimeout(this.timeout);

            this.page = 1;
            this.timeout = setTimeout(this.getIssues, 200);
        }
    },

    methods: {
        getIssues() {
            return axiosAPIv1.get('/issues', {
                params: {
                    page: this.page,
                    search: this.search
                }
            }).then(result => {
                this.issues = result.data.data;
                this.page = result.data.current_page;
                this.pageCount = result.data.last_page;
            });
        },

        clickCallback(page) {
            this.page = page;

            this.getIssues();
        }
    },

    components: {
        Paginate,
        WycIssue
    },

    created() {
        this.getIssues();
    }
});
