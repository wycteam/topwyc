import { quillEditor } from 'vue-quill-editor'

var vm = new Vue({
	el:'#faq-manager',
	data:{
		faq:{},
		content: '<h2>Test</h2>',
		editorOptions: {},
		select:{},
		changefunction: false,
		save: new Form ({
			type	:'Type',
			question:'',
			questionCn:'',
			answer	: '',
			answerCn: ''
		},{ baseURL: '/faq'}),
	},

	created(){
		 this.showAll();
	},
	
	methods:{
		openModal:function(){
			this.save = new Form ({
				type	:'Type',
				question:'',
				questionCn:'',
				answer	: '',
				answerCn: ''
			},{ baseURL: '/faq'}),
			$('#addQuestion').modal('toggle');
		},
		saveQuestion:function(){
				this.save.submit('post', '/create')
				.then(result => {
		          	this.showAll();
		          	$('#addQuestion').modal('toggle');
		          	toastr.success('Successfully Added');
		      	})
		      	.catch(error => {
		      	    toastr.error('Saving Failed.');
		      	});				
		},
		editQuestion:function(id){
			this.save.submit('post', '/edit/'+id)
				.then(result => {
		          	this.showAll();
		          	$('#addQuestion').modal('toggle');
		          	toastr.success('Successfully Updated');
		      	})
		      	.catch(error => {
		      	    toastr.error('Updated Failed.');
		      	});
		},
		showAll(){
			axios.get('/faq/show')
	          .then(result => {
	            this.faq = result.data
	        });
		},
		deleteQuestion(){
			axios.post('/faq/delete/'+this.select)
	          .then(result => {
	          	this.showAll();
	          	$('#deleteQuestion').modal('toggle');
	          	toastr.success('Successfully Deleted');
	      	})
	      	.catch(error => {
	      	    toastr.error('Deletion Failed.');
	      	});
		},
		showEditModal(id){
			axios.post('/faq/getQuestion/'+id) 
				.then(result => {
		          	this.save = new Form ({
						type	:result.data.type,
						question:result.data.question,
						questionCn:result.data.question_cn,						
						answer	:result.data.answer,
						answerCn:result.data.answer_cn,	
						id		:result.data.id
					},{ baseURL: '/faq'});
					this.changefunction = true;
					$('#addQuestion').modal('toggle');
		      	});
	    }
	},
	components: {
		quillEditor
		,'faq-panel' : require('../components/Faq/Panel.vue')
	}
})