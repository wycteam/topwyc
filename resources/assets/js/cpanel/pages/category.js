var currentTable=null;
var currentCategory=null;

var CategoryApp = new Vue({
	el: '#category-root',
	
	data:{
		categories:[],
		filterType:'all',
		filterStatus:'',

		catId:'',
		catName:'',
		catParent:'',
		catActivated:false,
		catFeatured:false,
	},

	watch:{
		categories:function(){
	  		$('.dt-category').DataTable().destroy();

			setTimeout(function(){
				currentTable = $('.dt-category').DataTable({
			        pageLength: 10,
			        responsive: true,
			    });
			},500);
		},
		filterType:function(){
	  		$('.dt-category').DataTable().destroy();

			setTimeout(function(){
				currentTable = $('.dt-category').DataTable({
			        pageLength: 10,
			        responsive: true,
			    });
			},500);
		},
		filterStatus:function(){
	  		$('.dt-category').DataTable().destroy();

			setTimeout(function(){
				currentTable = $('.dt-category').DataTable({
			        pageLength: 10,
			        responsive: true,
			    });
			},500);
		}
	},

	methods:{
		default(){
			this.catName = '';
			this.catParent = '';
			this.catActivated = false;
			this.catFeatured = false;
		},

		showModal(id){
			var category = this.categories.filter(obj => {
				return obj.id == id;
			})
			this.default();
			if(category.length)
			{
				category = category[0];
				
				this.catId = id;
				this.catName = category.name;
				this.catParent = category.parent_id;
				this.catActivated = parseInt(category.active);
				this.catFeatured = parseInt(category.featured);

			}
				$('#categoryModal').modal('toggle');
		},

		getType(treelevel){
			if(treelevel.indexOf('-') > -1)
			{
				return 'Child';
			}else if(treelevel == '')
			{
				return 'Grandparent';
			}else
			{
				return 'Parent';
			}
		},

		updateCategory(){
			if(this.catName != '')
			{
				if(this.catId == 0)
				{
					axiosAPIv1.post('/categories',{
						name 		:this.catName,
						parent_id	:this.catParent,
						active 		:this.catActivated?1:0,
						featured 	:this.catFeatured?1:0,						
					})
					.then(response => {
						if(response.status == 201)
						{
							
							$('.dt-category').DataTable().destroy();
							this.categories.push(response.data.data);
							toastr.success('Category Added!');
							

							setTimeout(function(){
								currentTable = $('.dt-category').DataTable({
							        pageLength: 10,
							        responsive: true,
							    });
							},500);

							$('#categoryModal').modal('toggle');

						}
							
					})
				}else
				{
					axiosAPIv1.put('/categories/'+this.catId,{
						name 		:this.catName,
						parent_id	:this.catParent,
						active 		:this.catActivated?1:0,
						featured 	:this.catFeatured?1:0,						
					})
					.then(response => {
						if(response.status == 200)
						{
							this.categories.filter(obj => {
								if(obj.id == this.catId)
								{
									obj.name 		= response.data.data.name;
									obj.parent_id	= response.data.data.parent_id;
									obj.active 		= response.data.data.active;
									obj.featured 	= response.data.data.featured;
									toastr.success('Category Updated!');
									
									$('.dt-category').DataTable().destroy();

									setTimeout(function(){
										currentTable = $('.dt-category').DataTable({
									        pageLength: 10,
									        responsive: true,
									    });
									},500);

									$('#categoryModal').modal('toggle');

								}
							})
						}
					})
				}
			}else{
				toastr.error('','Category Name is required.');
			}
		},

		statusColor(status){
			switch(status){
				case 'Pending': return 'label-warning'; break;
				case 'Processing': return 'label-default'; break;
				case 'Completed': return 'label-primary'; break;
				case 'Cancelled': return 'label-danger'; break;
			}
		},

		showFilterStatus(status){
			// if(this.filterStatus != 'all')
			// {
			// 	return this.filterStatus == status.toLowerCase();
			// }
			// return true;
		},

		showFilterType(type){
			if(this.filterType != 'all')
			{
				return this.filterType.toLowerCase() == type.toLowerCase();
			}
			return true;
		},


		textColor(status){
			switch(status)
			{
				case 'Pending': return 'row-pending'; break;
				case 'Completed': return 'row-completed'; break;
				case 'Processing': return 'row-processing'; break;
				case 'Cancelled': return 'row-cancelled'; break;
			}
		},

	},

	created(){
		axiosAPIv1.get('/category')
			.then(response => {
				if(response.status == 200)
				{
					this.categories = response.data.data.cats;
				}
			})
	},

	mounted(){

	},

})