/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Event = require('../core/event.js').default;
window.Errors = require('../core/errors.js').default;
window.Form = require('../core/form.js').default;


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('modal', require('../shopping/components/Modal.vue'));
Vue.component('imgfileupload', require('../components/Imgfileupload.vue'));
Vue.component('dialog-modal', require('./components/DialogModal.vue'));
 
//currency??
Vue.filter('currency', function (value) {
  if(value!=null){
    if(value%1 != 0){
       return  numberWithCommas(parseFloat(value).toFixed(2));
    }
    else{
       return  numberWithCommas(parseInt(value));
    }
  }
  else{
    return 0;
  }
});

Vue.filter('toTitleCase', function (str) {  
    return str.split(' ').map(function (item) {
        return item.charAt(0).toUpperCase() + item.substring(1);
    }).join(' ');
});

window.numberWithCommas = function(x) {

    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

}
//Jan 1, 2017 10:00am
Vue.filter('friendly-date', function (value) {
    return  value;
});


Vue.filter('formatDate', function(value) {
  if (value) {
    return moment.utc(String(value)).fromNow();
  }
});


Vue.filter('invoiceFormat', function(value) {
  if (value) {
    return moment(String(value)).format("YYYY, MMMM Do");
  }
});


Vue.filter('commonDate', function(value) {
  if (value) {
    return moment(String(value)).format("MMM DD,YYYY");
  }
});


Vue.filter('serviceDate', function(value) {
  if (value) {
    return moment(String(value)).format("YYYY/MM/DD");
  }
});

Vue.filter('age', function(value) {
  if (value) {
    return moment().diff(value, 'years');
  }
});

// const app = new Vue({
//     el: '#app'
// });

if (window.Laravel.isAuthenticated) {
    Echo.private('User.' + window.Laravel.user.id)
        .notification((notification) => {
            let arrType = notification.type.split('\\');
            let type = arrType[arrType.length - 1].replace(/([a-z](?=[A-Z]))/g, '$1-').toLowerCase();

            Event.fire(type, notification);
        });
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
