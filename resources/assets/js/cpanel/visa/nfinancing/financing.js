import VueWorker from 'vue-worker';
import VTooltip from 'v-tooltip'

Vue.use(VTooltip);
Vue.use(VueWorker);
Vue.component('finance-content', require('../components/NewFinancing/Projects.vue'));

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    next();
});
const app = new Vue({
  el: '#nFinancings',
});
localStorage.setItem("versionControl", 0);
var toggler = document.getElementsByClassName("carets");
var processLocator = document.getElementById("processLocator");
var i;
var sel;
for (i = 0; i < toggler.length; i++) {
toggler[i].addEventListener("click", function() {
  this.parentElement.querySelector(".nested").classList.toggle("active");
  this.classList.toggle("carets-down");
  // if($.trim(processLocator.innerHTML).length == 0){
  //     processLocator.append(this.innerHTML);
  // }else{
  //     processLocator.append(" > "+this.innerHTML);
  // }

});
}

var toggler2 = document.getElementsByClassName("root");

for (i = 0; i < toggler2.length; i++) {
  toggler2[i].addEventListener("click", function() {
    $(sel).removeClass('rootSelected');
    sel = this;
    var inputForm =  document.getElementById("inputForm");
    $(inputForm).removeClass("inputForm");
    $(inputForm).addClass('inputForm2');
    $(sel).addClass('rootSelected');
    var p = this;
    while($(p).parent('.carets').length){
      p = $(p).parent('.carets');
      console.log($(p).parent('.carets').html());
    }
    //$(processLocator).html($(this).parents().parentsUntil('#myUL').html().replace(/(<([^>]+)>)/ig,"").replace( /\s\s+/g, '|'));
    //console.log($(this).parentsUntil('.liParent').closest('span').html())
    //$(this).parents().parentsUntil('li .liParent').css("background-color","black");
  })

}


//dragElement(document.getElementById("myModal5"));
$( function() {
   $( "#myModal5" ).draggable();

 } );
// function dragElement(elmnt) {
//   var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
//   if (document.getElementById("myModal5header")) {
//     // if present, the header is where you move the DIV from:
//     document.getElementById("myModal5header").onmousedown = dragMouseDown;
//   } else {
//     // otherwise, move the DIV from anywhere inside the DIV:
//     elmnt.onmousedown = dragMouseDown;
//   }
//
//   function dragMouseDown(e) {
//     e = e || window.event;
//     e.preventDefault();
//     // get the mouse cursor position at startup:
//     pos3 = e.clientX;
//     pos4 = e.clientY;
//     document.onmouseup = closeDragElement;
//     // call a function whenever the cursor moves:
//     document.onmousemove = elementDrag;
//   }
//
//   function elementDrag(e) {
//     e = e || window.event;
//     e.preventDefault();
//     // calculate the new cursor position:
//     pos1 = pos3 - e.clientX;
//     pos2 = pos4 - e.clientY;
//     pos3 = e.clientX;
//     pos4 = e.clientY;
//     // set the element's new position:
//     elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
//     elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
//   }
//
//   function closeDragElement() {
//     // stop moving when mouse button is released:
//     document.onmouseup = null;
//     document.onmousemove = null;
//   }
// }

$(function () {
    $('.footer-fixed').on('scroll', function (e) {
        //alert();
        $('#ftable tbody').scrollLeft($('.footer-fixed').scrollLeft());
    });
    // $('#ftable tbody').on('scroll', function (e) {
    //     $('.sidebar-fixed').scrollTop($('#ftable tbody').scrollTop());
    // });
    // $('.sidebar-fixed').on('scroll', function (e) {
    //     $('#ftable tbody').scrollTop($('.sidebar-fixed').scrollTop());
    // });

});
$(window).on('load', function (e) {
  //alert($('FinanceTable').prop('scrollHeight'));
    $('.div11').width($('#ftable').width()+500);
    $('.div22').width($('#ftable').width());

    //console.log($('#ftable tr')[0].height());
    //$("#total-height").text(outerHeight + 'px');
    //alert(outerHeight);


  $('#ftable tbody').scroll(function(e) { //detect a scroll event on the tbody
   /*
   Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
   of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.
   */
   $('#ftable thead').css("left", -$("#ftable tbody").scrollLeft()); //fix the thead relative to the body scrolling
   for(var i =1;i<=5;i++){
     $('#ftable thead th:nth-child('+i+')').css("left", $("#ftable tbody").scrollLeft()); //fix the first cell of the header
     $('#ftable tbody td:nth-child('+i+')').css("left", $("#ftable tbody").scrollLeft()); //fix the first column of tdbody
   }



 });

});
