
Vue.component('daily-content', require('../components/DailyCost/Projects.vue'));
import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)
// Vue.http.interceptors.push((request, next) => {
//     request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);
//
//     next();
// });
const app = new Vue({
  el: '#dailyCost',
  data: {
    translation: null
  },
  methods: {
    getTranslation() {
        axios.get('/visa/nfinancing/translation')
          .then(response => {
            this.translation = response.data.translation;
          });
      }
  },
  created() {
    this.getTranslation();
  }
});
$(document).ready(function(){
  // $('#data_5 .input-daterange').datepicker({
  //     keyboardNavigation: true,
  //     forceParse: false,
  //     autoclose: true,
  //     format: "yyyy/mm/dd",
  //
  // });
  //$('#df').inputmask({"mask": "9999/99/99"});
  $('#dt').inputmask({"mask": "9999/99/99"});
  var d =  moment();
  $('input[name="start"]').val(d.format("YYYY/MM/DD"));
  $('input[name="end"]').val(d.format("YYYY/MM/DD"));
});
