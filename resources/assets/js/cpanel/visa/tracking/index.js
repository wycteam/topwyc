
Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

var Tracking = new Vue({
    el:'#tracking',
    data:{
        keyword:'',
        check:0,
        count:0,
        services:[],
        groups:[],
        reports:[],
        process:'',
        serviceCost:[],
        compute:[]
    },
    methods: {
        searchEnter(e){
            if(this.keyword!=""){
                var key = this.keyword;
                var upper = key.toUpperCase();
                var keyword = upper.substring(0, 2);
                // alert(keyword);
                if (e.keyCode === 13) {
                    if(keyword=='GL'){
                        this.check = 1;

                        axiosAPIv1.get('/visa/tracking/group/'+this.keyword)
                        .then(response => {
                            this.groups = response.data.groups[0];
                            this.compute = response.data;
                            this.count = response.data.groups.length;
                            this.computeServiceCost(this.groups.groupmember);
                            this.balance = (((parseFloat(this.compute.package_deposit)+parseFloat(this.compute.package_payment)+parseFloat(this.compute.package_discount))-parseFloat(this.compute.package_refund) )-parseFloat(this.compute.total_cost));
                        });
                    } else {
                        this.check = 0;

                        axiosAPIv1.get('/visa/tracking/service/'+this.keyword)
                        .then(result => {
                            this.services = result.data;
                            this.count = this.services.services.length;
                            this.balance = (((parseFloat(this.services.package_deposit)+parseFloat(this.services.package_payment)+parseFloat(this.services.package_discount))-parseFloat(this.services.package_refund) )-parseFloat(this.services.package_cost));
                        });
                    }
                }
            } else {
                this.count = 0;
            }
        },

        searchBtn(){
            if(this.keyword!=""){
                var key = this.keyword;
                var upper = key.toUpperCase();
                var keyword = upper.substring(0, 2);
                if(keyword=='GL'){
                    this.check = 1;

                    axiosAPIv1.get('/visa/tracking/group/'+this.keyword)
                    .then(response => {
                        this.groups = response.data.groups[0];
                        this.compute = response.data;
                        this.count = response.data.groups.length;
                        this.computeServiceCost(this.groups.groupmember);
                        this.balance = (((parseFloat(this.compute.package_deposit)+parseFloat(this.compute.package_payment)+parseFloat(this.compute.package_discount))-parseFloat(this.compute.package_refund) )-parseFloat(this.compute.total_cost));
                    });
                } else {
                    this.check = 0;

                    axiosAPIv1.get('/visa/tracking/service/'+this.keyword)
                    .then(result => {
                        this.services = result.data;
                        this.count = this.services.services.length;
                        this.balance = (((parseFloat(this.services.package_deposit)+parseFloat(this.services.package_payment)+parseFloat(this.services.package_discount))-parseFloat(this.services.package_refund) )-parseFloat(this.services.package_cost));
                    });
                }
            } else {
                this.count = 0;
            }
        },

        showServices(id, e) {
            if(e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#client-' + id).toggleClass('hide');
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },

        computeServiceCost(members,) {
            this.serviceCost = members.map(member => {
                let cost = 0;
                let charge = 0;
                let tip = 0;

                member.services.map(service => {
                    if(service.active == 1 && service.group_id == this.groups.id){
                        cost += parseFloat(service.cost);
                        charge += parseFloat(service.charge);
                        tip += parseFloat(service.tip);
                    }
                });

                return (cost + charge + tip);

            });
        },

        report(id){
            axiosAPIv1.get('/visa/tracking/report/'+id)
            .then(response => {
                this.reports = response.data;
                if(this.reports.length!=0)
                    $('#reports').modal('toggle');
            });
        }
    },
    created(){
        this.process = 'process';
        $('body').popover({ // Ok
            html:true,
            trigger: 'hover',
            selector: '[data-toggle="popover"]'
        });
    }
});
