Vue.component('ads-content', require('../components/Ads/Projects.vue'));
Vue.http.interceptors.push((request, next) => {
  request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

  next();
});
const app = new Vue({
  el: '#adsContents',
  data: {
    translation: null
  },
  methods: {
    getTranslation() {
      axios.get('/visa/news/translation')
          .then(response => {
            this.translation = response.data.translation;
          });
    }
  },
  created() {
    this.getTranslation();
  }
});
