new Vue({
	el: '#agents',

	data: {
		loading: true,
		agents: [],
		translation: null,
	},

	methods: {
		getTranslation() {
		    axios.get('/visa/agents/translation')
		        .then(response => {
		          	this.translation = response.data.translation;
		        });
		},
		getAgents() {
			this.loading = true;

			axios.get('/visa/agents/get-agents')
				.then(response => {
					this.loading = false;

					this.agents = response.data;

					this.callDataTables('agents-datatable');
				});
		},

		callDataTables(className){
			$('#'+className).DataTable().destroy();

			setTimeout(() => {
				$('#'+className).DataTable({
		            pageLength: 10,
		            responsive: true,
		            dom: '<"top"lf>r<"clear">t<"bottom"p>',
		            order: [[ 0, "asc" ]]
		        });
			}, 500);
		}
	},

	created() {
		this.getAgents();
		this.getTranslation();
	},

	components: {
		'add-new-agent' : require('../components/agents/AddNewAgent.vue'),
		'edit-agent' : require('../components/agents/EditAgent.vue')
	}
});

let data = window.Laravel.data;
Vue.component('agents-list',  require('../components/agents/ListofAgents.vue'));
Vue.component('referral-list',  require('../components/agents/ListofReferrals.vue'));

new Vue({
	el: '#middleman',

	data: {
		middleman: (data) ? data.midman : null,
		agents: (data) ? data.agents : null,
		commi: (data) ? data.commi : null,
		referrals:[],
		translation:null
	},

	methods: {
		getTranslation() {
		    axios.get('/visa/agents/translation')
		        .then(response => {
		          	this.translation = response.data.translation;
		        });
		},
		getReferrals(id){
			axios.get('/visa/agents/getReferrals/' + id) 
				.then(response => {
					this.loading = false;
					this.referrals = response.data
					this.callDataTables('lists');
			});
		},
		getAllReferrals(){
			axios.get('/visa/agents/all-referrals/' + ((this.middleman) ? this.middleman.id : null) ) 
				.then(response => {
					this.loading = false;
					this.referrals = response.data
					this.callDataTables('lists');
			});
		},	

		callDataTables(className){
			$('#'+className).DataTable().destroy();

			setTimeout(() => {
				$('#'+className).DataTable({
		            pageLength: 10,
		            responsive: true,
		            dom: '<"top"lf>r<"clear">t<"bottom"p>',
		            order: [[ 0, "asc" ]]
		        });
			}, 500);

		},

	},

	created() {
		this.getAllReferrals();
		this.callDataTables('lists');
		this.getTranslation();
	},

	components: {
		'add-new-agent' : require('../components/agents/AddNewAgent.vue'),
		'middleman-referred-clients' : require('../components/agents/MiddlemanReferredClients.vue')
	}

});