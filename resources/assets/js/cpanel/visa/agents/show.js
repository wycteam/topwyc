new Vue({
	el: '#app',

	data: {
		agent: null,
		middleman: null,
		translation: null,
	},

	methods: {
		getTranslation() {
		    axios.get('/visa/agents/translation')
		        .then(response => {
		          	this.translation = response.data.translation;
		        });
		},

		setAgent() {
			this.$refs.editagent.setAgent(this.agent);
			
		},

		getAgentId() {
			let pathArray = window.location.pathname.split( '/' );
			return pathArray[3];
		},

		getAgent() {
			axios.get('/visa/agents/' + this.getAgentId() + '/get-agent')
				.then(response => {
					this.agent = response.data;
				});
		},

		getMiddleMan() {
			axios.get('/visa/agents/' + this.getAgentId() + '/get-middleman')
				.then(response => {
					this.middleman = response.data;
					this.getMiddlemen(this.middleman);
				});
		},

		getMiddlemen(middleman) {
			if(middleman) {
				axios.get('/visa/agents/get-middlemen', {
					params: {
						q: middleman.id
				 	}
				})
				.then(response => {
					this.$refs.editagent.initMiddlemen(response.data);
				});
			} else {
				this.$refs.editagent.initMiddlemen([]);
			}
		},

		callDataTables(className){
			$('#'+className).DataTable().destroy();

			setTimeout(() => {
				$('#'+className).DataTable({
		            pageLength: 10,
		            responsive: true,
		            dom: '<"top"lf>r<"clear">t<"bottom"p>',
		            order: [[ 0, "asc" ]]
		        });
			}, 500);
		}
	},

	created() {
		this.getAgent();
		this.getMiddleMan();
		this.getTranslation();
	},

	components: {
		'add-new-agent' : require('../components/agents/AddNewAgent.vue'),
		'edit-agent' : require('../components/agents/EditAgent.vue'),
		'referred-clients' : require('../components/agents/ReferredClients')
	}
});