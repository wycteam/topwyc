import Dropzone from 'vue2-dropzone';

Vue.component('docs-list',  require('../components/services/ClientDocumentList.vue'));

let data = window.Laravel.user;
var url  = window.location.href;
var tid = url.substring(url.lastIndexOf('/') + 1);

var app = new Vue({
	el:'#clientDocuments',
	data:{
		translation: null,

		dz: '',
        headers: {
            'X-CSRF-TOKEN': window.Laravel.csrfToken,
        },
        typeid: tid,
		documents:[],
		act:'',
		sel_id:'',

		files: [],

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            addNewDocument:0,
            editDocument:0,
            deleteDocument:0
        }],

		docsFrm: new Form({
	       	id 		: '',
		    name 	: '',
	    },{ baseURL	: '/visa/service-manager/client-documents'})
	},

	mounted () {
		$('#dialogAddDocument').on('show.bs.modal', function (e) {
			app.docsFrm.errors.clear();
		});

		if(this.typeid != 'list') {
			this.getFiles();

			var vm = this;

	        this.dz = 'dropzone';

	        this.$nextTick(() => {
	            this.$refs.returnedItemFiles.$on('vdropzone-removedFile', this.dropzoneFileRemoved.bind(this));
	            this.$refs.returnedItemFiles.dropzone.options.headers = this.headers;
	            this.$refs.returnedItemFiles.dropzone.options.autoProcessQueue = false;
	            this.$refs.returnedItemFiles.dropzone.options.parallelUploads = 100;
	            // parallelUploads defines how many files should be uploaded to the server at once.
				// autoProcessQueue tells Dropzone that it should not start the queue automatically, not initially, and not after files finished uploading.
				// If you want autoProcessQueue to be true after the first upload, then just listen to the processing event, and set this.options.autoProcessQueue = true; inside.
	        });

	        $('#multipleUploadModal').on('hidden.bs.modal', e => {
	            this.dz = '';
	            this.selectedOrder = {};
	            this.returnItemForm = new Form();
	        });
		}
	},
	methods:{
		getTranslation() {
      		axios.get('/visa/service-manager/client-documents/translation')
          		.then(response => {
            		this.translation = response.data.translation;
          		});
		},

		getFiles() {
			axios.get('/visa/service-manager/client-documents/type/' + this.typeid)
	        .then(result => {
	          	this.files = result.data;
	    	});
		},

		loadDocs(){
			axios.get('/visa/service-manager/client-documents')
	        .then(result => {
	          this.documents = result.data;

	          this.reloadDataTable();
	    	});
		},

		dropzoneUploadSuccess: function (file, response) {
            let _response = JSON.parse(response);

            let { success, message, path } = _response;

            if(success) {
            	toastr.success(message);

            	this.$refs.returnedItemFiles.dropzone.removeAllFiles();

            	this.getFiles();
            }
        },

        dropzoneFileRemoved(file, error, xhr) {
            //
        },

        dropzoneUploadError(file, message, xhr) {

        },

        dropzoneFileAdded(file) {
        	let { name, type } = file;
        	let filename = name.substr(0, name.lastIndexOf('.'));
			let f = filename.split("&&");

			let success = true;
			let message = '';

        	if(type != 'image/jpeg' && type != 'image/png') {
        		success = false;
        		message = (this.translation) ? this.translation['you-cant-upload-files-of-this-type'] : 'You can\'t upload files of this type.';
        	} else if(f.length != 3) {
        		success = false;
        		message = (this.translation) ? this.translation['incorrect-filename-format'] : 'Incorrect filename format';
        	} else if(isNaN(f[0])) {
        		success = false;
        		message =  (this.translation) ? this.translation['incorrect-filename-format'] : 'Incorrect filename format';
        	} else if(!moment(f[1], 'YYYY-MM-DD').isValid()) {
        		success = false;
        		message =  (this.translation) ? this.translation['incorrect-filename-format'] : 'Incorrect filename format';
        	} else if(!moment(f[2], 'YYYY-MM-DD').isValid()) {
        		success = false;
        		message =  (this.translation) ? this.translation['incorrect-filename-format'] : 'Incorrect filename format';
        	}

        	if(!success) {
        		this.$refs.returnedItemFiles.dropzone.removeFile(file);
        		toastr.error(message, name);
        	}
        },

        dropzoneQueueComplete(files) {
            // $('#multipleUploadModal').modal('hide');

            // toastr.success('Successfully saved.');

            // this.$refs.returnedItemFiles.dropzone.removeAllFiles();

            //this.selectedOrder.status = 'Returned';
            // this.returnItemForm.submitted = true;
            // this.returnItemForm.errors.clear();
        },

        submit() {
            if(this.$refs.returnedItemFiles.dropzone.getAcceptedFiles().length > 0) {
                this.$refs.returnedItemFiles.dropzone.processQueue();
            } else {
                toastr.error( (this.translation) ? this.translation['images-field-is-required'] : 'Images field is required.');
            }
        },

		reloadDataTable(){
			setTimeout(function(){
				$('#lists').DataTable({
		            responsive: true,
		        });
			},1000);
		},
		saveDocs(e){
		   	if(e == "Add"){
				this.docsFrm.submit('post', '/add')
			    .then(result => {
			    	let { success, message } = result;

			    	if(success) {
			    		toastr.success(message);
			        	$('#dialogAddDocument').modal('toggle');
			        	this.reloadAll();
			    	}
			    })
			    .catch(error => {
			    	for(var key in error) {
					    if(error.hasOwnProperty(key)) {
					    	toastr.error(error[key]);
					    }
					}
			    });
			}else if (e == "Edit"){
				this.docsFrm.submit('post', '/edit')
			    .then(result => {
			    	let { success, message } = result;

			    	if(success) {
			    		toastr.success(message);
			        	$('#dialogAddDocument').modal('toggle');
			        	this.reloadAll();
			    	}
			    })
			    .catch(error => {
			        for(var key in error) {
					    if(error.hasOwnProperty(key)) {
					    	toastr.error(error[key]);
					    }
					}
			    });
			}else if (e == "Delete"){
				axios.post('/visa/service-manager/service-documents/delete/'+ this.sel_id)
			    .then(result => {
			        toastr.success( (this.translation) ? this.translation['service-document-deleted'] : 'Service Document Deleted');
			        this.reloadAll();
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete Failed.');
			    });
			}
		},

		actionDoc(e, id){
			if(e == "add"){
				$('#dialogAddDocument').modal('toggle');
				this.act = "Add";
			}else if (e == "edit"){
				this.act = "Edit";
				axios.get('/visa/service-manager/client-documents/info/'+ id)
					.then(result =>{
					this.docsFrm.id = result.data.id;
					this.docsFrm.name = result.data.name;
				})
				$('#dialogAddDocument').modal('toggle');
			}else if (e == "delete"){
				this.act = "Delete";
				this.sel_id = id;
				$('#dialogRemoveDocument').modal('toggle');
			}
		},

		resetDocFrm(){
			this.docsFrm = new Form({
				id 		: '',
		        name 	: '',
		    },{ baseURL	: '/visa/service-manager/client-documents'})
		},
		reloadAll(){
			$('#lists').DataTable().destroy();
			this.resetDocFrm();
			this.loadDocs();
		}
	},
	created(){
		this.getTranslation();

		this.loadDocs();
	},

	components: {
		Dropzone
	}
});


$(document).ready(function() {
	//for editing of child
	// $(document).on("click",".editServ",function() {
	// 	var id = $(this).attr('id')
	// 	var id = id.split("_");
 //        app.changeActiveId(id[1]);
 //        app.addOrEdit('edit');
 //    });

	// //for deleting of child
 //    $(document).on("click",".deleteServ",function() {
	// 	var id = $(this).attr('id')
	// 	var id = id.split("_");
 //        app.changeActiveId(id[1]);
 //        app.showRemoveServiceDialog();
 //    });
});
