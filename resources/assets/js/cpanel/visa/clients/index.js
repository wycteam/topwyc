import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)
let data = window.Laravel.user;

var Clients = new Vue({
	el:'#client',
	data:{
		branch_id:data.branch_id,
		translation: null,
		arrs:'',
		frstExp:'',
		groups:[],
		branches:[],
		visa_type:[],
		clients: [],
		v_type:0,
		branchselected: 1,
		is_master : false,
		user_roles: data.roles,
	},
	methods: {
		getTranslation() {
			axios.get('/visa/client/translation')
               	.then(response => {
                  	this.translation = response.data.translation;

                  	window.translation = this.translation;
           		});
		},

		firstExp(){
			try {
				var d = moment(this.arrs,'YYYY-MM-DD').add(59, 'days');
				this.frstExp=  moment(d,'YYYY-MM-DD').format('YYYY-MM-DD');
			}
			catch(err) {console.log(err)}
		},
		changeUrl(){
			var cpanelUrl = window.Laravel.cpanel_url;
			if($('#email').val()==''){
		    	$('#form-client').attr('action','http://'+cpanelUrl+'/visa/client/store');
			} else {
		    	$('#form-client').attr('action','http://'+cpanelUrl+'/visa/client/store2');
			}
		},
		changeStatus(clid,stat){
			axios.get('/visa/client/changeStatus/'+clid+'/'+stat)
			.then(result => {
				$('#cs_'+clid).attr('client-status',stat);
				var fcolor = '#676a6c';
				if(stat == 'High-Risk'){
					fcolor = 'red';
				}
				$('#cl_'+clid).css('color',fcolor);
	          	swal({
                      title: (this.translation) ? this.translation['successful'] : "Successful!",
                      text:  (this.translation)
                      	? (this.translation['status-has-been-changed-to'] + ' ' + stat)
                      	: ('Status has been changed to ' + stat),
                      type: "success"
                  });
	    	});
		},

	    changeBranch(e) {
	    	alert();
	    	// if(e.target.value != this.current_cost){
	     //  		this.changeCostLevel(e.target.value);
	    	// }
	    },

	    getGroups(branch_id){
	    	axios.get('/visa/client/showGroup/'+branch_id)
	        .then(result => {
	          	this.groups = result.data;

	            $(".select2").select2({
	                placeholder: "Select group",
	                allowClear: true
	            });

	            $(".select2-selection").css('height','34px');
	    	});
	    }
	},
	created(){
		this.getTranslation();

		this.getGroups(data.branch_id); // manila branch default

		var master = this.user_roles.filter(obj => {
	      if(obj.name == 'master' || obj.name == 'vice-master'){
	        return true;
	      }
	    });

	    if(master.length > 0){
	        this.is_master = true;
	    }

    	axios.get('/visa/client/showBranch')
        .then(result => {
          	this.branches = result.data;

            $(".select-branch").select2({
                placeholder: "Select branch",
                allowClear: true
            });

            $(".select2-selection").css('height','34px');
    	});

	}

});

new Vue({

	el: '#app',

	data: {
		loading: false,

		branches: [],
		selectedBranch: 0,

		temporaryClientIds: [],

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            addNewClient:0,
            addTemporaryClient:0,
            balance:0,
            collectables:0,
            viewAction:0
        }],
        is_master : false,
		user_roles: data.roles,
	},

	methods: {

		getBranches() {
    		axios.get('/visa/get-branches')
         		.then(result => this.branches = result.data);
    	},

    	numberWithCommas(value) {
    		return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    	},

    	loadClient() {
    		this.loading = true;

    		axios.get('/visa/client/load-client-list/' + this.selectedBranch)
		        .then(result => {
		        	$('#total-collectables').text( this.numberWithCommas(result.data[1]) );

		        	this.__datatable__();
		        });
    	},

    	__datatable__() {
    		if(this.selpermissions.balance == 1 && this.selpermissions.collectables == 1 && this.selpermissions.viewAction == 1){
				this.callDatatable();
			}

			if(this.selpermissions.balance == 0 && this.selpermissions.collectables == 1 && this.selpermissions.viewAction == 1){
				this.callDatatable1(2);
			}

			if(this.selpermissions.balance == 1 && this.selpermissions.collectables == 0 && this.selpermissions.viewAction == 1){
				this.callDatatable1(3);
			}

			if(this.selpermissions.balance == 1 && this.selpermissions.collectables == 1 && this.selpermissions.viewAction == 0){
				this.callDatatable1(6);
			}

			if(this.selpermissions.balance == 0 && this.selpermissions.collectables == 0 && this.selpermissions.viewAction == 1){
				this.callDatatable2(2,3);
			}

			if(this.selpermissions.balance == 0 && this.selpermissions.collectables == 1 && this.selpermissions.viewAction == 0){
				this.callDatatable2(2,6);
			}

			if(this.selpermissions.balance == 1 && this.selpermissions.collectables == 0 && this.selpermissions.viewAction == 0){
				this.callDatatable2(3,6);
			}

			if(this.selpermissions.balance == 0 && this.selpermissions.collectables == 0 && this.selpermissions.viewAction == 0){
				this.callDatatable3(2,3,6);
			}
    	},

		callDatatable() {
			$('#lists').DataTable().destroy();

			setTimeout(() => {
				$('#lists').DataTable({
				    "ajax": "/storage/data/Clients.txt",
				    responsive: true,
				    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				    "iDisplayLength": 25,
				    columnDefs: [
				    {type: 'non-empty-string', targets: [4,5]}
				    ]
				});

				this.loading = false;
			}, 1000);
		},
		callDatatable1(a) {
			$('#lists').DataTable().destroy();

			setTimeout(() => {
				$('#lists').DataTable({
				    "ajax": "/storage/data/Clients.txt",
				    responsive: true,
				    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				    "iDisplayLength": 25,
				    columnDefs: [
				    {type: 'non-empty-string', targets: [4,5]},
				    {"targets": [ a ],"visible": false,"searchable": false}
				    ]
				});

				this.loading = false;
			}, 1000);
		},
		callDatatable2(a,b) {
			$('#lists').DataTable().destroy();

			setTimeout(() => {
				$('#lists').DataTable({
				    "ajax": "/storage/data/Clients.txt",
				    responsive: true,
				    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				    "iDisplayLength": 25,
				    columnDefs: [
				    {type: 'non-empty-string', targets: [4,5]},
				    {"targets": [ a ],"visible": false,"searchable": false},
				    {"targets": [ b ],"visible": false,"searchable": false}
				    ]
				});

				this.loading = false;
			}, 1000);
		},
		callDatatable3(a,b,c) {
			$('#lists').DataTable().destroy();

			setTimeout(() => {
				$('#lists').DataTable({
				    "ajax": "/storage/data/Clients.txt",
				    responsive: true,
				    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				    "iDisplayLength": 25,
				    columnDefs: [
				    {type: 'non-empty-string', targets: [4,5]},
				    {"targets": [ a ],"visible": false,"searchable": false},
				    {"targets": [ b ],"visible": false,"searchable": false},
				    {"targets": [ c ],"visible": false,"searchable": false}
				    ]
				});

				this.loading = false;
			}, 1000);
		}
	},

	created(){
		this.getBranches();

		var master = this.user_roles.filter(obj => {
	      if(obj.name == 'master' || obj.name == 'vice-master'){
	        return true;
	      }
	    });

	    if(master.length > 0){
	        this.is_master = true;
	    }
	    else{
	    	this.selectedBranch = data.branch_id;
	    	this.loadClient();
	    }

        var x = this.permissions.filter(obj => { return obj.type === 'Manage Clients'});

        var y = x.filter(obj => { return obj.name === 'Add New Client'});

        if(y.length==0) this.selpermissions.addNewClient = 0;
        else this.selpermissions.addNewClient = 1;

        var y = x.filter(obj => { return obj.name === 'Add Temporary Client'});

        if(y.length==0) this.selpermissions.addTemporaryClient = 0;
        else this.selpermissions.addTemporaryClient = 1;

        var y = x.filter(obj => { return obj.name === 'Balance'});

        if(y.length==0) this.selpermissions.balance = 0;
        else this.selpermissions.balance = 1;

        var y = x.filter(obj => { return obj.name === 'Collectables'});

        if(y.length==0) this.selpermissions.collectables = 0;
        else this.selpermissions.collectables = 1;

        var y = x.filter(obj => { return obj.name === 'View Action'});

        if(y.length==0) this.selpermissions.viewAction = 0;
        else this.selpermissions.viewAction = 1;

        this.__datatable__();
	},

	components: {
		'add-temporary-client': require('../../components/Visa/AddTemporaryClient.vue'),
	},

	mounted() {
		$('#newlyAddedTemporaryClientModal').on('hidden.bs.modal', function (e) {
			toastr.success('Temporary client successfully added.', null, {
		        timeOut: 1000,
		        onHidden: function () {
					window.location.reload();
				}
		    });
		});
	}

});

$(document).ready(function() {
	//when theres a change in branch, change also the groups in the list
	var $eventSelect = $(".select-branch");
	$eventSelect.on("change", function (e) {
		var $br = $('#branch_id').val();
		Clients.branch_id = $('#branch_id').val();
		Clients.getGroups($br);
	});

	$(document).on("click",".changeStatus",function() {
		var clid = $(this).attr("client-id");
		var stat = $(this).attr("client-status");
		var ht = '';
		if(stat=='Regular'){
			ht = '<a client-status="High-Risk" client-id="'+clid+'" class="payment-status btn btn-danger btn-lg">to High Risk </a>';
		}
		else{
			ht = '<a client-status="Regular" client-id="'+clid+'" class="payment-status btn btn-success btn-lg">to Regular </a>';
		}
      	swal({
			title: "Change Client Status?",
            type: 'info',
            allowOutsideClick: false,
            showCloseButton: true,
			html: "" +"<br>" + ht,
	        showCancelButton: false,
	        showConfirmButton: false
		});
    });

    $(document).on("click", ".payment-status", function() {
	    swal.close();
        var clid = $(this).attr("client-id");
		var stat = $(this).attr("client-status");
		Clients.changeStatus(clid,stat);
    });

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "non-empty-string-asc": function (str1, str2) {
        if(str1 == '<span style="display:none;">--</span>')
            return 1;
        if(str2 == '<span style="display:none;">--</span>')
            return -1;
        return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
    },

    "non-empty-string-desc": function (str1, str2) {
        if(str1 == '<span style="display:none;">--</span>')
            return 1;
        if(str2 == '<span style="display:none;">--</span>')
            return -1;
        return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
    }
});

	$('#first_name, #middle_name, #last_name').keypress(function(event){
		var inputValue = event.charCode;
		//alert(inputValue);
		if(!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123)||(inputValue==32) || (inputValue==0))){
		event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
	 //if the letter is not digit then display error and don't type anything
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        return false;
	    }
	});

	// $('#group_id').on('change', function() {
	//     alert($(this).val());
	// });

	$('form#form-client').on('submit', function(e) {
		var firstName = $('input#first_name').val();
		var lastName = $('input#last_name').val();
		var civil_status = $('select[name=civil_status]').val();
		var gender = $('select[name=gender]').val();
		var birthdate = $('input#birth_date').val();
		var contactNumber = $('input#contact_number').val();
		var contactNumber2 = $('input#alternate_contact_number').val();
		var passport = $('input#passport').val();
		// var passport_exp_date = $('#passport_exp_date').val();
		var group_id = $('#group_id').val();
		var branch_id = Clients.branch_id;
		var visa_type = $('#visa_type').val();
		//var icard = $('#icard').val();
		var arrival_date = $('#arrival_date').val();
		var issue_date = $('#issue_date').val();
		var exp_date = $('#exp_date').val();
		var service_exp_date = $('#service_exp_date').val();
		var icard_exp_date = $('#icard_exp_date').val();
		var first_exp_date = $('#first_exp_date').val();
		var nationality = $('#nationality').val();
		var birth_country = $('#birth_country').val();
		var address = $('#address').val();
		var city = $('#city').val();
		var province = $('#province').val();
		var email = $('#email').val();
		$.ajax({
			url: 'validationBeforeSubmit',
      type: 'get',
      data: {
        		firstName:firstName,
				lastName: lastName,
				gender: gender,
				civil_status: civil_status,
				birthdate: birthdate,
				contactNumber: contactNumber,
				contactNumber2: contactNumber2,
				passport: passport,
				// passport_exp_date: passport_exp_date,
				group_id: group_id,
				branch_id: branch_id,
				visa_type: visa_type,
				//icard:icard,
				exp_date:exp_date,
				first_exp_date:first_exp_date,
				issue_date:issue_date,
				arrival_date:arrival_date,
				service_exp_date:service_exp_date,
				icard_exp_date:icard_exp_date,
				birth_country:birth_country,
				nationality:nationality,
				address:address,
				city:city,
				province:province,
				email:email,
      },
            async: false,
            success: function(data) {
            	var data = JSON.parse(data);

            	Clients.clients = data.users;

							if(data.binded) {
								toastr.error((window.translation) ? window.translation['contact-number-is-already-binded-in-the-visa-app'] : 'Contact number is already binded in the visa app.');
								e.preventDefault();
							} else if(data.duplicateEntry) {
									$('#duplicateClientsListModal').modal('show');
									e.preventDefault();
							}else if(data.errorList.length>0){
								data.errorList.forEach(function(item) {
										toastr.error(item + ((window.translation) ? (' ' + window.translation['is-required']) : ' is required'));
								});
								e.preventDefault();
							}else if(data.dsave) {
									toastr.error( (window.translation) ? window.translation['visa-type-is-required'] : 'Visa Type is Required');
									e.preventDefault();
							} else if(data.invalidDate) {
									toastr.error( (window.translation) ? window.translation['please-make-sure-the-all-dates-are-filled'] : 'Please make sure the all dates are filled');
									e.preventDefault();
							}else if(data.dateVal) {
									toastr.error( (window.translation) ? window.translation['please-make-sure-the-all-required-dates-are-valid'] : 'Please make sure the all required dates are valid');
									e.preventDefault();
							}else if(data.arrExpGT) {
									toastr.error( (window.translation) ? window.translation['arrival-expiration-date-cannot-be-future-date'] : 'Arrival expiration date cannot be future date');
									e.preventDefault();
							}else if(data.frstExpGT) {
									toastr.error( (window.translation) ? window.translation['arrival-date-cannot-be-ahead-of-first-expiration-date'] : 'Arrival date cannot be ahead of First expiration date');
									e.preventDefault();
							}else if(data.extExpGT) {
									toastr.error( (window.translation) ? window.translation['first-expiration-date-cannot-be-ahead-of-extended-expiration-date'] : 'First expiration date cannot be ahead of Extended expiration date');
									e.preventDefault();
							}else if(data.defArrFrst) {
									toastr.error( (window.translation) ? window.translation['first-expiration-date-must-be-at-least-forteen-days-from-arrival-date'] : 'First expiration date must be atleast 13 days from Arrival date');
									e.preventDefault();
							}else if(data.defFrstExt) {
									toastr.error( (window.translation) ? window.translation['extended-expiration-date-must-be-at-least-twenty-eight-days-from-first-date'] : 'Extended expiration date must be atleast 28 days from First date');
									e.preventDefault();
							}else if(data.dupEmail) {
									toastr.error( (window.translation) ? window.translation['the-email-you-used-is-already-used'] : 'The email you used is already used');
									e.preventDefault();
							}else if(data.emptyContact) {
								toastr.error( (window.translation) ? window.translation['contact-number-cannot-be-empty'] : 'Contact number cannot be empty');
							e.preventDefault();
							}
								// else if(data.invalidDate) {
                //     toastr.error('Already Expired');
                //     e.preventDefault();
                // }

            }
		});
//e.preventDefault();
	});

} );
