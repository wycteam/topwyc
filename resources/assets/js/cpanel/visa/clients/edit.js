import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

let data = window.Laravel.data.data;
let user = window.Laravel.user;

var Clients = new Vue({
	el:'#client',
	data:{
		translation: null,
		clients: [],
		visa_type:[],
		groups:[],
		branch:[],
		tmpVisa_id:data.visa_type,
		h1:data.arrival_date,
		h2:data.first_exp_date,
		h3:data.exp_date,
		h4:data.issue_date,
		h5:data.icard_exp_date,
		user_roles: user.roles,
		is_master : false,
	    clientForm:new Form({
	      id: data.id,
	      avatar: data.avatar,
	      first_name: data.first_name,
	      middle_name: data.middle_name,
	      last_name: data.last_name,
	      birth_date: data.birth_date,
	      gender: data.gender,
	      civil_status: data.civil_status,
	      height: data.height,
	      weight: data.weight,
	      address: data.address,
	      contact_number: data.contact_number,
	      alternate_contact_number: data.alternate_contact_number,
	      email: data.email,
	      passport: data.passport,
	      passport_exp_date:data.passport_exp_date,
	      group_id: data.group_id,
				visa_id:(data.visa_type!='' || data.visa_type!=null ? data.visa_type:0),
				//icard: visa.icard,
				exp_date:data.exp_date,
				first_exp_date:data.first_exp_date,
				issue_date:data.issue_date,
				arrival_date:data.arrival_date,
				icard_exp_date:data.icard_exp_date,
				branch_id:data.branch_id,
				nationality:data.nationality,
				birth_country:data.birth_country
	    }, { baseURL: 'http://'+window.Laravel.cpanel_url })
	},

	directives: {
		datepicker: {
		  bind(el, binding, vnode) {
		    $(el).datepicker({
		      format: 'yyyy-mm-dd'
		    }).on('changeDate', (e) => {
		      Clients.$set(Clients.clientForm, 'birth_date', e.format('yyyy-mm-dd'));
		    });
		  }
		}
	},
	methods: {
		getTranslation() {
			axios.get('/visa/client/translation')
               	.then(response => {
                  	this.translation = response.data.translation;

                  	window.translation = this.translation;
           		});
		},
		firstExp(){
			try {
			  var d = moment(this.clientForm.arrival_date,'YYYY-MM-DD').add(59, 'days');
				this.clientForm.first_exp_date =  moment(d,'YYYY-MM-DD').format('YYYY-MM-DD');
			}
			catch(err) {console.log(err)}
		},
		checkVisa(){
			//alert(this.clientForm.visa_id+'-'+this.tmpVisa_id);
			 localStorage.setItem("visa_type", this.clientForm.visa_id);
			if(this.clientForm.visa_id!=this.tmpVisa_id){
					//alert(nVisa);
				if(this.clientForm.visa_id=='9A'){
					this.clientForm.arrival_date='';
					this.clientForm.first_exp_date='';
					this.clientForm.exp_date='';
				}else if(this.clientForm.visa_id=='9G'||this.clientForm.visa_id=='TRV'){
					this.clientForm.icard_exp_date='';
					this.clientForm.issue_date='';
					this.clientForm.exp_date='';
				}else{
					this.clientForm.exp_date='';
				}
			}else{
				if(this.tmpVisa_id=='9A'){
					this.clientForm.arrival_date = this.h1;
					this.clientForm.first_exp_date = this.h2;
					this.clientForm.exp_date = this.h3;
				}else if(this.tmpVisa_id=='9G' || this.tmpVisa_id=='TRV'){
					this.clientForm.icard_exp_date = this.h5;
					this.clientForm.issue_date = this.h4;
					this.clientForm.exp_date = this.h3;
				}else{
					this.clientForm.exp_date = this.h3;
				}
			}
		},
		changeUrl(){
			var cpanelUrl = window.Laravel.cpanel_url;
			var clientId = $('#client-id').val();
			if($('#email').val()==''){
		    	$('#form-client').attr('action','http://'+cpanelUrl+'/visa/client/update/'+clientId);
			} else {
		    	$('#form-client').attr('action','http://'+cpanelUrl+'/visa/client/updatewithemail/'+clientId);
			}
		},
		formatDate(date){

        return moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY');
    },
	},
	created(){
		this.getTranslation();
		//console.log(this.clientForm.branch_id);
		var master = this.user_roles.filter(obj => {
	      if(obj.name == 'master' || obj.name == 'vice-master'){
	        return true;
	      }
	    });

	    if(master.length > 0){
	        this.is_master = true;
	    }

		axios.get('/visa/client/showGroup')
        .then(result => {
            this.groups = result.data;

            setTimeout(function(){
	            $(".select2").select2({
	                placeholder: "Select group",
	                allowClear: true
	            });
	            $(".select2-selection").css('height','34px');
        	}, 500);
    	});
			axios.get('/visa/client/showBranch')
	        .then(result => {
	            this.branch = result.data;
	    	});



    	var cpanelUrl = window.Laravel.cpanel_url;
		var clientId = $('#client-id').val();
		if(data.email==null){
	    	$('#form-client').attr('action','http://'+cpanelUrl+'/visa/client/update/'+clientId);
		} else {
	    	$('#form-client').attr('action','http://'+cpanelUrl+'/visa/client/updatewithemail/'+clientId);
		}

       Event.listen('Imgfileupload.avatarUpload', (img) => {
                this.clientForm.avatar = img;

        });
				//console.log(this.clientForm);
	},
	mounted() {
		$('select#group_id').on("change", function() {
		    let value = $("select#group_id option:selected").val();
		    this.clientForm.group_id = value;
		}.bind(this));
	},

});

$(document).ready(function() {

    $('.btnUpload').click(function(){
        $('.avatar-uploader input').click();
    });

	$('#first_name, #middle_name, #last_name').keypress(function(event){
		var inputValue = event.charCode;
		//alert(inputValue);
		if(!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123)||(inputValue==32) || (inputValue==0))){
		event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
	 //if the letter is not digit then display error and don't type anything
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        return false;
	    }
	});

	$('form#form-client').on('submit', function(e) {
		var group_id = $('#group_id').val();
		var firstName = $('input#first_name').val();
		var lastName = $('input#last_name').val();
		var civil_status = $('select[name=civil_status]').val();
		var gender = $('select[name=gender]').val();
		var birthdate = $('input#birth_date').val();
		var contactNumber = $('input#contact_number').val();
		var contactNumber2 = $('input#alternate_contact_number').val();
		var passport = $('input#passport').val();
		// var passport_exp_date = $('#passport_exp_date').val();
		var visa_type = $('#visa_type').val();
		//var icard = $('#icard').val();
		var branch_id = Clients.clientForm.branch_id;
		var arrival_date = $('#arrival_date').val();
		var issue_date = $('#issue_date').val();
		var exp_date = $('#exp_date').val();
		var service_exp_date = $('#service_exp_date').val();
		var icard_exp_date = $('#icard_exp_date').val();
		var first_exp_date = $('#first_exp_date').val();
		var nationality = $('#nationality').val();
		var birth_country = $('#birth_country').val();
		var address = $('#address').val();
		var city = $('#city').val();
		var province = $('#province').val();
		var email = $('#email').val();

		$.ajax({
			url: '/visa/client/validationBeforeSubmit',
			type: 'get',
            data: {
            	id: $('#client-id').val(),
            	firstName:firstName,
							lastName: lastName,
							civil_status: civil_status,
							gender: gender,
							birthdate: birthdate,
							contactNumber: contactNumber,
							contactNumber2: contactNumber2,
							passport: passport,
							// passport_exp_date: passport_exp_date,
							visa_type: visa_type,
							group_id: group_id,
							branch_id: branch_id,
							//icard:icard,
							exp_date:exp_date,
							first_exp_date:first_exp_date,
							issue_date:issue_date,
							arrival_date:arrival_date,
							service_exp_date:service_exp_date,
							icard_exp_date:icard_exp_date,
							nationality:nationality,
							birth_country:birth_country,
							address:address,
							city:city,
							province:province,
							email:email,
            },
            async: false,
            success: function(data) {

            	var data = JSON.parse(data);

            	Clients.clients = data.users;
								//alert(data.invalidDate);
								if(data.binded) {
									toastr.error( (window.translation) ? window.translation['contact-number-is-already-binded-in-the-visa-app'] : 'Contact number is already binded in the visa app.');
									e.preventDefault();
								}else if(data.errorList.length>0){
									data.errorList.forEach(function(item) {
									    toastr.error(item + ((window.translation) ? (' ' + window.translation['is-required']) : ' is required'));
									});
									e.preventDefault();
								// }else if(data.duplicateEntry) {
								// 		$('#duplicateClientsListModal').modal('show');
								// 		e.preventDefault();
								}else if(data.dsave) {
										toastr.error( (window.translation) ? window.translation['visa-type-is-required'] : 'Visa Type is Required');
										e.preventDefault();
								}else if(data.invalidDate) {
										toastr.error( (window.translation) ? window.translation['please-make-sure-the-all-dates-are-filled'] : 'Please make sure the all dates are filled');
										e.preventDefault();
								}else if(data.dateVal) {
										toastr.error( (window.translation) ? window.translation['please-make-sure-the-all-required-dates-are-valid'] : 'Please make sure that all dates are valid');
										e.preventDefault();
								}else if(data.arrExpGT) {
										toastr.error( (window.translation) ? window.translation['arrival-expiration-date-cannot-be-future-date'] : 'Arrival expiration date cannot be future date');
										e.preventDefault();
								}else if(data.frstExpGT) {
										toastr.error( (window.translation) ? window.translation['arrival-date-cannot-be-ahead-of-first-expiration-date'] : 'Arrival date cannot be ahead of First expiration date');
										e.preventDefault();
								}else if(data.extExpGT) {
										toastr.error( (window.translation) ? window.translation['first-expiration-date-cannot-be-ahead-of-extended-expiration-date'] : 'First expiration date cannot be ahead of Extended expiration date');
										e.preventDefault();
								}else if(data.defArrFrst) {
										toastr.error( (window.translation) ? window.translation['first-expiration-date-must-be-at-least-forteen-days-from-arrival-date'] : 'First expiration date must be at least 14 days from Arrival date');
										e.preventDefault();
								}else if(data.defFrstExt) {
										toastr.error( (window.translation) ? window.translation['extended-expiration-date-must-be-at-least-twenty-eight-days-from-first-date'] : 'Extended expiration date must be at least 28 days from First date');
										e.preventDefault();
								}else if(data.dupEmail) {
										toastr.error( (window.translation) ? window.translation['the-email-you-used-is-already-used'] : 'The email you used is already used');
										e.preventDefault();
								}else if(data.emptyContact) {
										toastr.error( (window.translation) ? window.translation['contact-number-cannot-be-empty'] : 'Contact number cannot be empty');
										e.preventDefault();
								}

            }
		});
		//e.preventDefault();
	});

} );

