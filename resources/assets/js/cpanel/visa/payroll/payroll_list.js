
import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)
Vue.component('payroll-content', require('../components/Payroll/List.vue'));
Vue.http.interceptors.push((request, next) => {
  request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

  next();
});
new Vue({
  el: '#payroll_list',
});



