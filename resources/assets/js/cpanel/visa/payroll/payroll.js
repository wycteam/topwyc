

Vue.component('payroll-content', require('../components/Payroll/Projects.vue'));
Vue.http.interceptors.push((request, next) => {
  request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

  next();
});
const app = new Vue({
  el: '#payroll_view',
});



