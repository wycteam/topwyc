
new Vue({
	el:'#service_profiles',

	data:{
		translation: null,

		loading: true,

		search: '',

		current: true,

		services:[],
		profiles:[],
		pcount:0,
		pid:0,
		sid:0,

		// ADD & EDIT PROFILE FORM
		addProfFrm: new Form({
	        id 		: '',
	        name 		: '',
	        is_active		: 1,
	    },{ baseURL			: '/visa/service-manager'}),
	},

	methods: {
		getTranslation() {
      		axios.get('/visa/service-manager/service-profiles/translation')
          		.then(response => {
            		this.translation = response.data.translation;
          		});
		},

		addServiceProfile(){
			this.emptyform();
			$('#addProfile').modal('show');
		},
		editServiceProfile(id,name,status){
			this.addProfFrm.id = id;
			this.addProfFrm.name = name;
			this.addProfFrm.status = status;
			$('#addProfile').modal('show');
		},
		//reset add/edit service form
		emptyform(){
			this.addProfFrm = new Form({
		       	id 			: '',
		       	name 			: '',
		        is_active		: 1,

	    	},{ baseURL			: '/visa/service-manager'})
		},

		saveform(){
			if(this.addProfFrm.id == ""){
				this.addProfFrm.submit('post', '/saveProfile')
			    .then(result => {
			        toastr.success( (this.translation) ? this.translation['new-profile-added'] : 'New Profile Added.');
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			    });
			    $('#addProfile').modal('hide');
			    location.reload();
			}
			else{
				this.addProfFrm.submit('post', '/saveProfile')
			    .then(result => {
			        toastr.success( (this.translation) ? this.translation['profile-updated'] : 'Profile Updated.');
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			    });
				location.reload();
			}
		},

		getServiceProfiles() {
			axios.get('/visa/service-manager/servProfiles')
		        .then(result => {
		          this.profiles = result.data.profiles;
		          this.pcount = result.data.count;
		          	let count = this.pcount;
				    count = count * 200;
				    let w = 600 + count;
					$("#fixTable").css("width", w+"px");
		    	});
		},
		editProfileCost(cost,profile_id,service_id) {
		  $('.inputCost').hide();
		  $('.strCost').show();
		  var pcost = parseInt(cost);
		  this.pid = profile_id;
		  this.sid = service_id;
		  // alert('#pc_'+service_id+'_'+profile_id);
		  $('#pc_'+service_id+'_'+profile_id).val(pcost);
		  $('#str_'+service_id+'_'+profile_id).hide();
		  $('#pc_'+service_id+'_'+profile_id).show();
		  $('#pc_'+service_id+'_'+profile_id).focus();
		},

		updateProfileCost() {
		  var service_id = this.sid;
		  var profile_id = this.pid;
		  var pval = $('#pc_'+service_id+'_'+profile_id).val();

		  axios.post('/visa/service-manager/updateProfileCost',{
					service_id : service_id,
					profile_id : profile_id,
					cost : pval,
				})
				.then(response => {
					$('#str_'+service_id+'_'+profile_id).text( ((pval.length > 0) ? pval : '0') +'.00');
					$('#str_'+service_id+'_'+profile_id).show();
					$('#pc_'+service_id+'_'+profile_id).hide();
					toastr.success('', (this.translation) ? this.translation['updated-successfully'] : 'Updated Successfully')
				})
				.catch(error => console.log(error));

		},

		refreshTableHeadFixer() {
			setTimeout(() => {
		  		$("#fixTable").tableHeadFixer({"head" : false, "left" : 1});
		  	}, 1000);
		},
		getServiceDetails() {
			this.loading = true;
			axios.get('/visa/service-manager/sortedServices')
		        .then(result => {
		          this.services = result.data;
		          setTimeout(() => {
		  				this.loading = false;

		  				$("#fixTable").tableHeadFixer({"head" : false, "left" : 1});

		  				let position = 0;
		  				$("#fixTable-wrapper").animate({scrollLeft: position}, 100);
		  			}, 1000);
		    	});
		},
		filter() {
			this.getServiceDetails();
		}
	},

	created() {
		this.getTranslation();

		this.getServiceProfiles();

		this.getServiceDetails();
		$('.inputCost').hide();
		$('body').popover({ // Ok
		    html:true,
			trigger: 'hover',
		    selector: '[data-toggle="popover"]'
		});
	},

	computed: {
	    filteredServices() {
	      	return this.services.filter(service => {
	         	return (service.detail.toLowerCase().indexOf(this.search.toLowerCase()) > -1);
	      	});
	    }
	}
});
