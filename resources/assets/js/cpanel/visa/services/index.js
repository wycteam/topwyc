import Multiselect from 'vue-multiselect'
import VueButtonSpinner from 'vue-button-spinner';

Vue.component('dialog-modal',  require('../components/DialogModal.vue'));
Vue.component('service-panel',  require('../components/services/ServicePanel.vue'));
Vue.component('service-list',  require('../components/services/NewServices.vue'));
Vue.component('docs-list',  require('../components/services/DocumentList.vue'));

let data = window.Laravel.user;

var app = new Vue({
	el:'#services',
	data:{
		defaultCost:[],
		serviceProfile: 0,
		profile_name:'',
		temp_profile_id:'',
		branches:[],
		translation: null,
		loading	: true,
		loadingmodal	: true,
		addEditDeleteAuth: false,
		pageWizard:1,
		page_title:"List of Services",
		selectedService: null,
		authuser : data.id,
		services:[],// list of Existing Services -REMOVE THIS
		serviceParents:[],// list all parents
		servicetype:['Parent', 'Child'],
		addOrEditModalTitle:'',
	   	addorEditFunction: '',
	   	showChildOptions: false,
	   	activeId:'', //selected ID on the table
	   	countChild:'',
	   	serviceDocuments:[], //list of all service documents
	   	selectedDocsNeeded:[],
	    selectedDocsOpt:[],
	    selectedDocsReleased:[],
	    selectedDocsReleasedOptional:[],

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            addNewService:0,
            editService:0,
            deleteService:0
        }],

        branchCosts: null,
        profileCosts: null,
        profiles: [],
		profiles2: [],
		// ADD & EDIT SERVICE FORM
		addServFrm: new Form({
			profile_id: 0,
	        detail 			: '',
	        detail_cn		: '',
	        description 	: '',
	        description_cn	: '',
	        type   			: 'Parent', //parent or child
	        parent_id		: '',
	        cost 			: '',
	        charge 			: '',
	        tip 			: '',
	        com_client 		: '',
	        com_agent 		: '',
	        id	   			: '',
	        is_active		: '',
	        docs_needed		: [],
	        docs_optional	: [],
	        docs_released   : [],
	        docs_released_optional : [],
			branch_id: data.branch_id,
			costArray		: [],
			chargeArray		: [],
			tipArray		: [],
			comAgentArray		: [],
			comClientArray		: [],
			costBranchIdArray: [],
			chargeBranchIdArray: [],
			tipBranchIdArray: [],
			comAgentBranchIdArray: [],
			comClientBranchIdArray: [],
			feeArray:[],
			months_required: 0,
			commissionable_amt: ''
	    },{ baseURL			: '/visa/service-manager'}),

	    // ADD & EDIT PROFILE FORM
		addProfFrm: new Form({
	        id 			: '',
	        name 		: '',
	        is_active	: 1,
	    },{ baseURL		: '/visa/service-manager'}),


		// ADD & EDIT PROFILE COST
		profileCost: new Form({
	        //id 			: '',
	        name 		: 'Regular Rates',
	        service_id 	: '',
	        profile_id 	: '',
	        // branch_id 	: '',
	        costArray				: [],
			chargeArray				: [],
			tipArray				: [],
			comAgentArray			: [],
			comClientArray			: [],
			costBranchIdArray		: [],
			chargeBranchIdArray		: [],
			tipBranchIdArray		: [],
			comAgentBranchIdArray	: [],
			comClientBranchIdArray	: [],
			feeArray:[],
	    },{ baseURL		: '/visa/service-manager'}),

	    isLoading: false,
	    status: '',

	},
	components: {
		Multiselect,
  		VueButtonSpinner,
  		'cost-component': require('../components/services/CostComponent.vue'),
  		'charge-component': require('../components/services/ChargeComponent.vue'),
  		'tip-component': require('../components/services/TipComponent.vue'),
  		'comagent-component': require('../components/services/ComAgentComponent.vue'),
  		'comclient-component': require('../components/services/ComClientComponent.vue')
  	},

	mounted() {
	    $('#editService').on('hide.bs.modal', () => {
	    	let tab = "tab-info";
	        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
	  //       this.loading = true;
			// this.emptyform();
	  //       this.emptyProfileCost();
	  //      	this.loadService();
	  //      	this.loadParents();
	    });
	   	$('#dialogRemoveService').on('hide.bs.modal', () => {
	        //this.commonFunctionReload();
		});
		
	},

	methods: {
		nxtWizard(val){
			var flag = true;
			if(val=='n'){
				if(this.pageWizard==2){
					if(this.addServFrm.type=='Child'){
						if(this.addServFrm.detail==''){
							toastr.error('Service name cannot be empty');
							flag = false;
						}
						if(this.addServFrm.parent_id==''){
							toastr.error('Please choose a service parent');
							flag = false;
						}
					}
				}else if(this.pageWizard==3){
					$.each( this.addServFrm.feeArray, function( key, value ) {
						$.each( value, function( key, value ) {
							if(key!='branch'){
								if(!$.isNumeric(value) || value===''){
									flag=false;
								}
							}

						});
					});
					if(!flag){
						toastr.error('Please make sure all value are numeric and not empty');
					}
					
				}
				if(flag)
				this.pageWizard++;

				// if(this.pageWizard>2)
				// 	this.dataLoader();
			}else{
				this.pageWizard--;
				this.serviceProfile = 0;
				this.addServFrm.profile_id = 0;
			}
		},
		showWizard(num, val){
			this.pageWizard = num;
			switch(this.pageWizard){
				case 1 :
					this.page_title="List of Services";
					this.serviceProfile = 0;
					this.addServFrm.type='Parent';
					this.addServFrm.profile_id = 0;
				break;
				case 2 : 
				
					if(val=='add'){
						this.page_title="Add new Service";
						this.addServFrm.type='Parent';	
					}else{
						this.page_title="Edit Service";
					}
					//this.page_title= (val=='add') ? "Add new Service" : "Edit Service";
					this.addorEditFunction = val;
					
					this.addOrEdit(this.addorEditFunction);
				break;
				case 4 : 
					this.page_title="Edit Documents"
					this.addorEditFunction = val;
					this.addOrEdit(this.addorEditFunction);
				break;
				case 5 :
					this.page_title="Service Profiles";
				break;
			}
		},
		disableProfile(id,val){
			
			swal({
                title: 'Warning Message',
                html: val=='disable' ? '<p>Are you sure you want to disable this profile? <br> All costs link to this profile will be change to normal rates</p> <br> Completed services will not be affected' : '<p>All cost previously linked to this profile will be affected</p> <br> Completed services will not be affected',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(() => {
				axios.post('/visa/service-manager/updateProfile', {
					id: id,
					action: val
				  })
				.then(result => {
					this.getAllProfile();
					
				});
            });
		},
		updateProfile(){
			axios.post('/visa/service-manager/updateProfile', {
				id: this.temp_profile_id,
				name: this.profile_name,
				action: 'update'
			  })
			.then(result => {
				if(result.data=='failed'){
					toastr.error('Profile name is already exist');
				}else{
					this.getAllProfile();
					$('#editProfile').modal('hide');
					this.temp_profile_id = null;
					this.profile_name = null;
				}
				
			});
		},
		checkAddEditDeleteAuth() {
			let uId = window.Laravel.user.id;
			
			this.addEditDeleteAuth = (uId == 1193 || uId == 1 || uId == 2352) ? true : false;
		},

		getTranslation() {
			axios.get('/visa/service-manager/translation')
          		.then(response => {
            		this.translation = response.data.translation;
          		});
		},

		showRemoveServiceDialog() {
			$('#dialogRemoveService').modal('toggle');
		},

		showServiceDetailModal() {
			axios.get('/visa/service-manager/getService/'+ this.activeId)
				.then(response => {
					this.selectedService = response.data;

					$('#serviceDetailModal').modal('show');
				});
		},

		// getProfileCost() {
		// 	axios.get('/visa/service-manager/profile-cost')
		// 		.then(result => {
		// 			this.profileCosts = result.data.profileCost;
		// 		});
		// },

		getAllProfile() {
			
			axios.get('/visa/service-manager/getAllProfile')
				.then(result => {
					this.profiles = result.data.profiles;
					this.profiles2 = result.data.profiles2;
				});
		},

		emptyProfileCost() {
			this.profileCost = new Form({
		        //id 			: '',
		        name 		: 'Regular Rates',
		        service_id 	: '',
		        profile_id 	: '',
		        // branch_id 	: '',
		        costArray				: [],
				chargeArray				: [],
				tipArray				: [],
				comAgentArray			: [],
				comClientArray			: [],
				costBranchIdArray		: [],
				chargeBranchIdArray		: [],
				tipBranchIdArray		: [],
				comAgentBranchIdArray	: [],
				comClientBranchIdArray	: [],
				feeArray:this.branchCosts,
		    },{ baseURL		: '/visa/service-manager'})
		},
		selectProfile2(){
			var sid = this.addServFrm.id;
			var pid = this.serviceProfile;
			if(pid > 0){			
				axios.get('/visa/service-manager/service-profile-cost2/'+pid+'/'+sid)
					.then(result => {

						this.profileCosts = result.data.profile_costs;
						this.addServFrm.feeArray = this.profileCosts;
						this.addServFrm.profile_id = pid;
						//console.log(this.addServFrm.feeArray );
					});
			}
			else{
				
				this.addServFrm.feeArray =this.defaultCost;
				this.addServFrm.profile_id = 0;
				// this.profileCost.profile_id = '';
				// this.profileCost.service_id = this.addServFrm.id;
			}
		},
		selectProfile(pid, pname) {
			var sid = this.addServFrm.id;

			// save before changing of profile data
			// var last_profile_id = this.profileCost.profile_id;
			// if(last_profile_id > 0){
			// 	this.setProfileCost();
			// 	this.profileCost.submit('post', '/editProfileCost')
			//     .then(result => {
			//         toastr.success('Profile Price Updated');
			//     })
			//     .catch(error => {
			//         toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			//     });
			// }
			
			if(pid > 0){			
				axios.get('/visa/service-manager/service-profile-cost/'+pid+'/'+sid)
					.then(result => {
						this.profileCosts = result.data.profile_costs;
						
						this.profileCost.profile_id = pid;
						this.profileCost.name = pname;
					});
			}
			else{
				// this.emptyProfileCost();
				this.profileCost.profile_id = '';
				this.profileCost.name = 'Regular Rates';
				this.profileCost.service_id = this.addServFrm.id;
			}
		},

		setProfileCost() {
			this.profileCost.costArray = [];
			this.profileCost.chargeArray = [];
			this.profileCost.tipArray = [];
			this.profileCost.comAgentArray = [];
			this.profileCost.comClientArray = [];
			this.profileCost.costBranchIdArray = [];
			this.profileCost.chargeBranchIdArray = [];
			this.profileCost.tipBranchIdArray = [];
			this.profileCost.comAgentBranchIdArray = [];
			this.profileCost.comClientBranchIdArray = [];

			this.profileCosts.map((bc, index) => {
				let costRef = 'cost-' + index;
				let chargeRef = 'charge-' + index;
				let tipRef = 'tip-' + index;
				let comagentRef = 'comagent-' + index;
				let comclientRef = 'comclient-' + index;

				let cost = this.$refs['costcomponent2'].$refs[costRef][0].value;
				let charge = this.$refs['chargecomponent2'].$refs[chargeRef][0].value;
				let tip = this.$refs['tipcomponent2'].$refs[tipRef][0].value;
				let comagent = this.$refs['comagentcomponent2'].$refs[comagentRef][0].value;
				let comclient = this.$refs['comclientcomponent2'].$refs[comclientRef][0].value;

				this.profileCost.costArray.push(cost);
				this.profileCost.chargeArray.push(charge);
				this.profileCost.tipArray.push(tip);
				this.profileCost.comAgentArray.push(comagent);
				this.profileCost.comClientArray.push(comclient);

				/////////////////////////////////////////////////

				let costBranchIdRef = 'cost-branch-id-' + index;
				let chargeBranchIdRef = 'charge-branch-id-' + index;
				let tipBranchIdRef = 'tip-branch-id-' + index;
				let comAgentBranchIdRef = 'comagent-branch-id-' + index;
				let comClientBranchIdRef = 'comclient-branch-id-' + index;

				let costBranchId = this.$refs['costcomponent2'].$refs[costBranchIdRef][0].value;
				let chargeBranchId = this.$refs['chargecomponent2'].$refs[chargeBranchIdRef][0].value;
				let tipBranchId = this.$refs['tipcomponent2'].$refs[tipBranchIdRef][0].value;
				let comAgentBranchId = this.$refs['comagentcomponent2'].$refs[comAgentBranchIdRef][0].value;
				let comClientBranchId = this.$refs['comclientcomponent2'].$refs[comClientBranchIdRef][0].value;

				this.profileCost.costBranchIdArray.push(costBranchId);
				this.profileCost.chargeBranchIdArray.push(chargeBranchId);
				this.profileCost.tipBranchIdArray.push(tipBranchId);
				this.profileCost.comAgentBranchIdArray.push(comAgentBranchId);
				this.profileCost.comClientBranchIdArray.push(comClientBranchId);
			});
		},

		getBranchCost() {
			axios.get('/visa/service-manager/branch-cost')
				.then(result => {
					this.branchCosts = result.data.branchCost;
					this.defaultCost = result.data.branchCost;
					this.addServFrm.feeArray = this.defaultCost;
					//console.log(this.branchCosts);
					// $.each( this.branchCosts, function( key, value ) {
					// 	$.each( value, function( key, value ) {
					// 		console.log( key + ": " + value );
					// 	});
					// });
				});
		},

		setBranchCost() {
			this.addServFrm.costArray = [];
			this.addServFrm.chargeArray = [];
			this.addServFrm.tipArray = [];
			this.addServFrm.comAgentArray = [];
			this.addServFrm.comClientArray = [];
			this.addServFrm.costBranchIdArray = [];
			this.addServFrm.chargeBranchIdArray = [];
			this.addServFrm.tipBranchIdArray = [];
			this.addServFrm.comAgentBranchIdArray = [];
			this.addServFrm.comClientBranchIdArray = [];

			this.branchCosts.map((bc, index) => {
				let costRef = 'cost-' + index;
				let chargeRef = 'charge-' + index;
				let tipRef = 'tip-' + index;
				let comagentRef = 'comagent-' + index;
				let comclientRef = 'comclient-' + index;

				let cost = this.$refs['costcomponent'].$refs[costRef][0].value;
				let charge = this.$refs['chargecomponent'].$refs[chargeRef][0].value;
				let tip = this.$refs['tipcomponent'].$refs[tipRef][0].value;
				let comagent = this.$refs['comagentcomponent'].$refs[comagentRef][0].value;
				let comclient = this.$refs['comclientcomponent'].$refs[comclientRef][0].value;

				this.addServFrm.costArray.push(cost);
				this.addServFrm.chargeArray.push(charge);
				this.addServFrm.tipArray.push(tip);
				this.addServFrm.comAgentArray.push(comagent);
				this.addServFrm.comClientArray.push(comclient);

				/////////////////////////////////////////////////

				let costBranchIdRef = 'cost-branch-id-' + index;
				let chargeBranchIdRef = 'charge-branch-id-' + index;
				let tipBranchIdRef = 'tip-branch-id-' + index;
				let comAgentBranchIdRef = 'comagent-branch-id-' + index;
				let comClientBranchIdRef = 'comclient-branch-id-' + index;

				let costBranchId = this.$refs['costcomponent'].$refs[costBranchIdRef][0].value;
				let chargeBranchId = this.$refs['chargecomponent'].$refs[chargeBranchIdRef][0].value;
				let tipBranchId = this.$refs['tipcomponent'].$refs[tipBranchIdRef][0].value;
				let comAgentBranchId = this.$refs['comagentcomponent'].$refs[comAgentBranchIdRef][0].value;
				let comClientBranchId = this.$refs['comclientcomponent'].$refs[comClientBranchIdRef][0].value;

				this.addServFrm.costBranchIdArray.push(costBranchId);
				this.addServFrm.chargeBranchIdArray.push(chargeBranchId);
				this.addServFrm.tipBranchIdArray.push(tipBranchId);
				this.addServFrm.comAgentBranchIdArray.push(comAgentBranchId);
				this.addServFrm.comClientBranchIdArray.push(comClientBranchId);
			});
		},
		
		addOrEdit(d){
			if(d == "add"){
				this.addOrEditModalTitle = ((this.translation) ? this.translation['add-new-service'] : 'Add New Service');
				this.addorEditFunction = 'add';
				this.loadingmodal = false;
				this.commonFunctionReload();

				this.getBranchCost();
				this.addServFrm.errors.errors = {};
				//console.log( this.addServFrm.errors.errors);
				//$('#editService').modal('toggle');
			}else if(d == "edit"){
				this.addServFrm.errors.errors = {};
				//console.log( this.addServFrm.errors.errors);
				var needed =[];
				var optional = [];
				var released = [];
				var released_optional = [];
				this.loading=true;
				this.loadingmodal = false;
				//this.loadingmodal = true;
				
				axios.get('/visa/service-manager/getService/'+ this.activeId)
					.then(result =>{

						//this.profileCosts = result.data.profile_costs;
						//console.log(result.data.select_docs_needed);
						this.addServFrm.id = result.data.id;
					   	this.addServFrm.detail = result.data.detail;
					   	this.addServFrm.detail_cn = result.data.detail_cn;
						this.addServFrm.description = result.data.description;
						this.addServFrm.description_cn = result.data.description_cn;
						this.addServFrm.parent_id = result.data.parent_id;
						this.addServFrm.months_required = result.data.months_required;
						
						this.branchCosts = result.data.branch_costs;
						this.defaultCost = this.branchCosts;
						this.addServFrm.feeArray = this.branchCosts;
						this.loading = false;
						this.addServFrm.profile_id = 0;
					    //this.loadingmodal = false;
						// this.addServFrm.cost = parseFloat(result.data.cost);
						// this.addServFrm.charge = parseFloat(result.data.charge);
						// this.addServFrm.tip = parseFloat(result.data.tip);
						// this.addServFrm.com_agent = parseFloat(result.data.com_agent);
						// this.addServFrm.com_client = parseFloat(result.data.com_client);
						// this.addServFrm.id = result.data.id;

						//console.log(this.branchCosts);
						//this.addServFrm.commissionable_amt = parseFloat(result.data.commissionable_amt);

						this.profileCost.service_id = result.data.id;
						try {
							if(result.data.select_docs_needed){
								result.data.select_docs_needed.map(function(value, key) {
								 needed.push(value[0]);
							 });
							}
						  }
						  catch(err) {}
						  try {
							if(result.data.select_docs_optional){
								result.data.select_docs_optional.map(function(value, key) {
								 optional.push(value[0]);
							 });
							}
						}
						catch(err) {}
						try {
							if(result.data.select_docs_released){
								result.data.select_docs_released.map(function(value, key) {
								 released.push(value[0]);
							 });
							}
						}
						catch(err) {}
						try {
							if(result.data.select_docs_released_optional){
								result.data.select_docs_released_optional.map(function(value, key) {
								 released_optional.push(value[0]);
							 });
							}
						}
						catch(err) {}

					  
					   	
					   	
					   
					    this.addServFrm.docs_needed = needed;
						this.addServFrm.docs_optional = optional;
						this.addServFrm.docs_released = released;
						this.addServFrm.docs_released_optional = released_optional;


					    if(result.data.parent_id == 0){
					   		this.addServFrm.type = 'Parent';
					   		this.showChildOptions = false;
					    }else{
					   		this.addServFrm.type = 'Child';
					   		this.showChildOptions = true;
						}
						
				})

				//this.addOrEditModalTitle = ((this.translation) ? this.translation['edit-service'] : 'Edit Service');
				this.addorEditFunction = 'edit';
				//this.loading = false;
				//$('#editService').modal('toggle');
			}
		},
		changeActiveId(id){
			this.activeId = id;
		},
		editServiceProfile(id, name){
			this.temp_profile_id = id;
			this.profile_name = name;
			$('#editProfile').modal('show');
		},
		addServiceProfile(){
			this.emptyprofileform();
			$('#addProfile').modal('show');
		},
		//reset add/edit service form
		emptyprofileform(){
			this.addProfFrm = new Form({
		       	id 			: '',
		       	name 			: '',
		        is_active		: 1,

	    	},{ baseURL			: '/visa/service-manager'})
		},

		saveProfileForm(){
			if(this.addProfFrm.id == ""){
				this.addProfFrm.submit('post', '/saveProfile')
			    .then(result => {
			        if(result.success){
			        	toastr.success(result.message);
			        	this.getAllProfile();
			    		$('#addProfile').modal('hide');
			        }
			        else{
			        	toastr.error(result.message);
			        }

			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			    });
			    //$('#addProfile').modal('hide');
			    //location.reload();
			}
			else{
				this.addProfFrm.submit('post', '/saveProfile')
			    .then(result => {
			        toastr.success( (this.translation) ? this.translation['profile-updated'] : 'Profile Updated.');
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			    });
				//location.reload();
			}
		},

		saveForm:function (event){
			var flag = true;
			//this.addServFrm.branch_id = data.branch_id;
			var btn = $(this);
				// alert(btn);
			var neededList='';
			var optionalList='';
			var releasedList='';
			var releasedOptionalList='';
			try{
				if(!jQuery.isEmptyObject(this.addServFrm.docs_needed)){
					this.addServFrm.docs_needed.map(function(e) {
						if(neededList == ''){
							neededList = e.id
						}else{
							neededList += "," + e.id;
						}
					});
					this.addServFrm.docs_needed = neededList;
				}
			}catch(err){console.log(err)}
			try{
				if(!jQuery.isEmptyObject(this.addServFrm.docs_optional)){
					this.addServFrm.docs_optional.map(function(e) {
						if(optionalList == ''){
							optionalList = e.id
						}else{
							optionalList += "," + e.id;
						}
					});
					this.addServFrm.docs_optional = optionalList;
				}
			}catch(err){console.log(err)}
			try{
				if(!jQuery.isEmptyObject(this.addServFrm.docs_released)){
					this.addServFrm.docs_released.map(function(e) {
						if(releasedList == ''){
							releasedList = e.id
						}else{
							releasedList += "," + e.id;
						}
					});
					this.addServFrm.docs_released = releasedList;
				}
			}catch(err){console.log(err)}
			try{
				if(!jQuery.isEmptyObject(this.addServFrm.docs_released_optional)){
					this.addServFrm.docs_released_optional.map(function(e) {
						if(releasedOptionalList == ''){
							releasedOptionalList = e.id
						}else{
							releasedOptionalList += "," + e.id;
						}
					});
					this.addServFrm.docs_released_optional = releasedOptionalList;
				}
			}catch(err){console.log(err)}
			

			if(this.addorEditFunction == "add"){
				// check if "parent" is selected
				if(this.addServFrm.type == "Parent"){
					this.addServFrm.parent_id = 0;
					this.addServFrm.cost = 0;
					this.addServFrm.charge = 0;
					this.addServFrm.tip = 0;
					this.addServFrm.com_agent = 0;
					this.addServFrm.com_client = 0;
				}
				this.addServFrm.is_active = 1;
				if(this.addServFrm.type == "Child"){
					$.each( this.addServFrm.feeArray, function( key, value ) {
						$.each( value, function( key, value ) {
							if(key!='branch'){
								if(value===''){
									value=0;
								}else{
									if(!$.isNumeric(value)){
										flag=false;
									}
								}
								
							}

						});
					});
					
				}
				if(!flag){
					toastr.error('Please make sure all value are numeric');
				}else{
					this.isLoading	= true; 
					// save form
					this.addServFrm.submit('post', '/save')
					.then(result => {
						this.commonFunctionReload();
						toastr.success( (this.translation) ? this.translation['new-service-added'] : 'New Service Added.');
						this.loadService();
						this.loadParents();
						this.pageWizard = 1;
						this.showWizard(1,'');
						this.isLoading	= false; 
					})
					.catch(error => {
						this.isLoading	= false; 
						if(this.addServFrm.errors.has('detail')){
							toastr.error( (this.translation) ? this.translation['saving-failed'] : this.addServFrm.errors.get('detail'));
							
						}else{
							toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
						}
			       
			    });

				}
				// Service Branch Cost
				// if(this.addServFrm.parent_id != 0) {
				// 	this.setBranchCost();
				// }
				
				//$('#editService').modal('toggle');
				
			}else if (this.addorEditFunction == "edit"){
				if(this.addServFrm.type == "Parent"){
					this.addServFrm.parent_id = 0;
					this.addServFrm.cost = 0;
					this.addServFrm.charge = 0;
					this.addServFrm.tip = 0;
					this.addServFrm.com_client = 0;
					this.addServFrm.com_agent = 0;
				}else if(this.addServFrm.type == "Child"){
					if(this.addServFrm.parent_id == '0'){
						this.addServFrm.parent_id = '';
					}
					if(this.addServFrm.cost == '0.00'){
						this.addServFrm.cost = '';
					}
					if(this.addServFrm.charge == '0.00'){
						this.addServFrm.charge = '';
					}
					if(this.addServFrm.tip == '0.00'){
						this.addServFrm.tip = '';
					}

					$.each( this.addServFrm.feeArray, function( key, value ) {
						$.each( value, function( key, value ) {
							if(key!='branch'){
								if(value===''){
									value=0;
								}else{
									if(!$.isNumeric(value)){
										flag=false;
									}
								}
								
								
							}

						});
					});
				}

				// Service Branch Cost
				if(this.addServFrm.parent_id != 0) {
					this.setBranchCost();
				}
				if(!flag){
					toastr.error('Please make sure all value are valid');
				}else{
					this.isLoading	= true; 
					this.addServFrm.submit('post', '/edit')
						.then(result => {
								// if(this.profileCost.profile_id > 0) {
								// 	this.setProfileCost();
								// 	this.profileCost.submit('post', '/editProfileCost')
								//     .then(result => {
								//         toastr.success('Profile Price Updated');
								//     })
								//     .catch(error => {
								//         toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
								//     });
								// }
							this.showWizard(1,'');
							this.isLoading	= false;
							this.status = true // or success
								setTimeout(() => { this.status = '' }, 2000);
							toastr.success( (this.translation) ? this.translation['service-updated'] : 'Service Updated.');
							//$('#editService').modal('toggle');
							 this.commonFunctionReload();
							 this.loadService();
							 this.loadParents();
						})
						.catch(error => {
							this.isLoading	= false; 
							if(this.addServFrm.errors.has('detail')){
								toastr.error( (this.translation) ? this.translation['saving-failed'] : this.addServFrm.errors.get('detail'));
							
							}else{
								toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
							}
						});
				}

			
			}

		},


		//show child options on modal
		changeServiceType(){
			var type = this.addServFrm.type;
			if(type == "Parent"){
				this.showChildOptions = false;
			}else if (type == "Child"){
				this.showChildOptions = true;
			}
		},

		//reset add/edit service form
		emptyform(){
			this.activeId = '';
			this.showChildOptions = false;
			this.selectedDocsNeeded=[];
	    	this.selectedDocsOpt=[];
	    	this.selectedDocsReleased=[];
	    	this.selectedDocsReleasedOptional=[];
			this.addServFrm = new Form({
				profile_id: 0,
		       	detail 			: '',
		        detail_cn		: '',
		        description 	: '',
		        description_cn	: '',
		        type   			: 'Parent', //parent or child
		        parent_id		: '',
		        cost 			: '',
		        id	   			: '',
		        is_active		: '',
		        docs_needed		: [],
	        	docs_optional	: [],
	        	docs_released   : [],
	        	docs_released_optional : [],
	        	charge 			: '',
	        	tip 			: '',
	        	com_agent 		: '',
	        	com_client 		: '',
				branch_id:data.branch_id,
				costArray		: [],
				chargeArray 	: [],
				tipArray		: [],
				comAgentArray		: [],
				comClientArray		: [],
				costBranchIdArray: [],
				chargeBranchIdArray: [],
				tipBranchIdArray: [],
				comAgentBranchIdArray: [],
				comClientBranchIdArray: [],
				feeArray:this.branchCosts,
				months_required: 0,
				commissionable_amt: ''
	    	},{ baseURL			: '/visa/service-manager'})


		},

		deleteService(){
			// this.countChildren();
			// if( this.countChild == 0){
				axios.get('/visa/service-manager/delete/' + this.activeId)
		        .then(result => {
		        	if(result.data.success){
			        	toastr.success(result.data.message);
		        		this.commonFunctionReload();
		        		this.loadService();
	       				this.loadParents();
			        }
			        else{
			        	toastr.error(result.data.message);
			        }
				    //toastr.success( (this.translation) ? this.translation['service-deleted'] : 'Service Deleted.');
			    })
			    .catch(error => {
			    	this.commonFunctionReload();
			        toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete Failed.');
			    });
			// }else{
			//	 toastr.error( (this.translation) ? this.translation['delete-failed-delete-child-services-first'] : 'Delete Failed. Delete child services first.');
			// }
		},

		loadService(){
			axios.get('/visa/service-manager/show')
	        .then(result => {
			  this.services = result.data;
			 
			  this.callDataTables();

	    	});
		},

		loadParents(){
			axios.get('/visa/service-manager/showParents')
	        .then(result => {
	          this.serviceParents = result.data;
			  //this.callDataTables();
	    	});
		},

		loadDocs(){
			axios.get('/visa/service-manager/service-documents')
	        .then(result => {
	          this.serviceDocuments = result.data;
	    	});
		},

		commonFunctionReload(){
			this.emptyProfileCost();
			this.emptyform();
	       	// this.loadService();
	       	// this.loadParents();
		},

		reloadDataTable(){
			setTimeout(function(){
				$('#lists').DataTable({
					"aaSorting": [],
					autoWidth: false,
				   columnDefs: [
				      { width: '40%', targets: 0 }, 
				      { width: '15%', targets: 1 }, 
				      { width: '15%', targets: 2 },  
				      { width: '15%', targets: 3 },  
				   ],
		            pageLength: 10,
		            responsive: true,
		            dom: '<"top"lf>rt<"bottom"ip><"clear">'
		        });
			},500);
		},

		countChildren(id){
			axios.get('/visa/service-manager/showChildren/' + id)
	        .then(result => {
	        	this.countChild = result.data.length;
			})
		},

		callDataTables(){
			this.loading = true;
			$('#lists').DataTable().destroy();

			var vm = this;

			setTimeout(function(){
			//var groupColumn = 2;
				$('#lists').DataTable({
					"aaSorting": [],
					autoWidth: false,
					lengthMenu: [ [-1,10, 25, 50, 100], ["All", 10, 25, 50, 100] ],
					"iDisplayLength": -1,
				   columnDefs: [
				      { width: '40%', targets: 0 }, 
				      { width: '15%', targets: 1 }, 
				      { width: '15%', targets: 2 },  
				      //{ width: '15%', targets: 3 },  
				   ],
		            dom: '<"top"lf>r<"clear">t<"bottom"p>',
		         //    "drawCallback": function ( settings ) {
			        //     var api = this.api();
			        //     var rows = api.rows( {page:'current'} ).nodes();
			        //     var last=null;
			 
			        //     api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
			        //         if ( last !== group ) {
			        //             $(rows).eq( i ).before(
			        //                 '<tr class="group"><td colspan="5">'+group+'</td></tr>'
			        //             );
			 
			        //             last = group;
			        //         }
			        //     } );
			        // }
		        });

		    //     $('#lists tbody').on('click', '.toggle-row', function () {
		    //     	var dis = $(this).attr('id');
			   //      var tr = $(this).closest('tr');
			   //      var row = table.row( tr );

			   //      if(row.child.isShown()) {
			   //          row.child.hide();
			   //          tr.removeClass('shown');
			   //      } else {
						// var id = dis.split("_");
			   //      	axios.get('/visa/service-manager/showChildren/' +id[1])
			   //      	.then(result => {
			   //      		var parent = "#parent_"+id[1];
						// 	$(parent).closest("td").closest("tr").remove();
			   //      		$(parent).closest("td").remove();

			   //      		$(parent).remove();
			   //      		$(parent+" tr").remove();
				  //           row.child(vm.formatRow(result.data,id[1])).show();
			   //      	})
			   //      	tr.addClass('shown');
			   //      }
			   //  });

			 },1);
			this.loading = false;
		},

		formatRow(d,parent) {
			if(d.length > 0){
				var ret =  '<table class="table table-bordered table-striped table-hovered" id="parent_'+parent+'">';

				for(var i= 0; i<d.length;i++) {
					if(this.addEditDeleteAuth) {
						var btn ='<center>';
		                btn += '<a style="margin-right:8px;" class="editServ" id="serv_'+d[i].id+'" data-toggle="tooltip" title="Edit Service">';
		                btn += '<i class="fa fa-pencil fa-1x"></i>';
		                btn += '</a>';
		                if(this.authuser == '1' || this.authuser == '1193' || this.authuser == '2352'){
			                btn += '<a data-toggle="tooltip"  class="deleteServ" id="dserv_'+d[i].id+'" title="Delete">';
			                btn += '<i class="fa fa-trash fa-1x"></i>';
			                btn += '</a>';
			            }
		            	btn +='</center>';
					} else {
						var btn ='<center>';
							btn += '<a data-toggle="tooltip" title="View" class="viewServ" id="dserv_'+d[i].id+'">';
			                	btn += 'View';
			                btn += '</a>';
						btn +='</center>';
					}
					
					ret +='<tr>'+
		            '<td><p style="padding-left: 30px; margin-bottom: 0px;">> '+d[i].detail+'</p></td>'+
		            '<td>'+d[i].description+'</td>'+
		            '<td>'+ (parseFloat(d[i].cost) + parseFloat(d[i].charge) + parseFloat(d[i].tip)).toFixed(2) +'</td>'+
		            '<td>'+btn+'</td>'+
		        	'</tr>';
		        }

        		ret+='</table>';

				return ret;
			}
		},
		
	},
	created(){
		
		this.loadService();
		this.loadParents();
		this.getAllProfile();
		this.checkAddEditDeleteAuth();
		this.getBranchCost();
		if(window.Laravel.language == 'cn'){
			this.getTranslation();
		}


        // var x = this.permissions.filter(obj => { return obj.type === 'Service Manager'});

        // var y = x.filter(obj => { return obj.name === 'Add New Service'});

        // if(y.length==0) this.selpermissions.addNewService = 0;
        // else this.selpermissions.addNewService = 1;

        // var y = x.filter(obj => { return obj.name === 'Edit Service'});

        // if(y.length==0) this.selpermissions.editService = 0;
        // else this.selpermissions.editService = 1;

        // var y = x.filter(obj => { return obj.name === 'Delete Service'});

        // if(y.length==0) this.selpermissions.deleteService = 0;
        // else this.selpermissions.deleteService = 1;

		this.loadDocs();
		// this.callDataTables();
	},
	
	updated(){
	
		$(".parent").closest("tr").css({"border":"2px solid #e7e7e7"});
	}
});

// var app2 = new Vue({
// 	el:'#documents',
// 	data:{
// 		translation: null,

// 		_action: null,

// 		documents:[],
// 		act:'',
// 		sel_id:'',

//         setting: data.access_control[0].setting,
//         permissions: data.permissions,
//         selpermissions: [{
//             addNewDocument:0,
//             editDocument:0,
//             deleteDocument:0
//         }],

// 		docsFrm: new Form({
// 	       	id 		: '',
// 		    title 	: '',
// 		   	title_cn: '',
// 	    },{ baseURL	: '/visa/service-manager/service-documents'})
// 	},
// 	methods:{
// 		getTranslation() {
// 			if(window.Laravel.language == 'cn'){
// 	      		axios.get('/visa/service-manager/service-documents/translation')
// 	          		.then(response => {
// 	            		this.translation = response.data.translation;
// 	          		});
//           	}
//     	},
// 		loadDocs(){
// 			axios.get('/visa/service-manager/service-documents')
// 	        .then(result => {
// 	          this.documents = result.data;

// 	          //this.reloadDataTable();
// 	    	});
// 		},
// 		reloadDataTable(){
// 			// $('#lists').DataTable().destroy();

// 			// setTimeout(function(){
// 			// 	$('#lists').DataTable({
// 		 //            responsive: true,
// 		 //        });
// 			// },500);
// 		},
// 		saveDocs(e){
// 		   	if(e == "Add"){
// 				this.docsFrm.submit('post', '/add')
// 			    .then(result => {
// 			        toastr.success( (this.translation) ? this.translation['new-service-document-added'] : 'New Service Document Added' );
// 			        $('#dialogAddDocument').modal('toggle');
// 			        this.reloadAll();
// 			    })
// 			    .catch(error => {
// 			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed' );
// 			    });
// 			}else if (e == "Edit"){
// 				this.docsFrm.submit('post', '/edit')
// 			    .then(result => {
// 			        toastr.success( (this.translation) ? this.translation['service-document-updated'] : 'Service Document Updated' );
// 			        $('#dialogAddDocument').modal('toggle');
// 			        this.reloadAll();
// 			    })
// 			    .catch(error => {
// 			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed' );
// 			    });
// 			}else if (e == "Delete"){
// 				axios.post('/visa/service-manager/service-documents/delete/'+ this.sel_id)
// 			    .then(result => {
// 			        toastr.success( (this.translation) ? this.translation['service-document-deleted'] : 'Service Document Deleted' );
// 			        this.reloadAll();
// 			    })
// 			    .catch(error => {
// 			        toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete Failed' );
// 			    });
// 			}
// 		},

// 		actionDoc(e, id){
// 			if(e == "add"){
// 				this._action = 'Add';
// 				$('#dialogAddDocument').modal('toggle');
// 				this.act = (this.translation) ? this.translation['add'] : 'Add' ;
// 			}else if (e == "edit"){
// 				this._action = 'Edit';
// 				this.act = (this.translation) ? this.translation['edit'] : 'Edit' ;
// 				axios.get('/visa/service-manager/service-documents/info/'+ id)
// 					.then(result =>{
// 					this.docsFrm.id = result.data.id;
// 					this.docsFrm.title = result.data.title;
// 					this.docsFrm.title_cn = result.data.title_cn;
// 				})
// 				$('#dialogAddDocument').modal('toggle');
// 			}else if (e == "delete"){
// 				this._action = 'Delete';
// 				this.act = "Delete";
// 				this.sel_id = id;
// 				$('#dialogRemoveDocument').modal('toggle');
// 			}
// 		},

// 		resetDocFrm(){
// 			this.docsFrm = new Form({
// 				id 		: '',
// 		        title 	: '',
// 		        title_cn: '',
// 		    },{ baseURL	: '/visa/service-manager/service-documents'})
// 		},
// 		reloadAll(){
// 			this.resetDocFrm();
// 			this.loadDocs();
// 		}
// 	},
// 	created(){
// 		this.getTranslation();

//         var x = this.permissions.filter(obj => { return obj.type === 'Service Manager'});

//         var y = x.filter(obj => { return obj.name === 'Add New Document'});

//         if(y.length==0) this.selpermissions.addNewDocument = 0;
//         else this.selpermissions.addNewDocument = 1;

//         var y = x.filter(obj => { return obj.name === 'Edit Document'});

//         if(y.length==0) this.selpermissions.editDocument = 0;
//         else this.selpermissions.editDocument = 1;

//         var y = x.filter(obj => { return obj.name === 'Delete Document'});

//         if(y.length==0) this.selpermissions.deleteDocument = 0;
//         else this.selpermissions.deleteDocument = 1;

// 		this.loadDocs();
// 	},
// });

var app3 = new Vue({
	el:'#service_info',
	data:{
		serv:[]
	},
	methods:{
		loadService(){
			// Controller method doesn't exist; 
			// axios.get('/visa/service-manager/sortedServices2')
	        //	.then(result => {
	        //  	this.serv = result.data;
	    	// });
		},

	},
	created(){
		this.loadService();
		
	},
})
$(document).ready(function() {
	//for editing of child
	$(document).on("click",".editServ",function() {
		var id = $(this).attr('id')
		var id = id.split("_");
        app.changeActiveId(id[1]);
        app.addOrEdit('edit');
    });

	//for deleting of child
    $(document).on("click",".deleteServ",function() {
		var id = $(this).attr('id')
		var id = id.split("_");
        app.changeActiveId(id[1]);
        app.showRemoveServiceDialog();
    });

    // for viewing of child
    $(document).on("click",".viewServ",function() {
		var id = $(this).attr('id')
		var id = id.split("_");
        app.changeActiveId(id[1]);
        app.showServiceDetailModal();
    });


    $(document).on("click",".prof",function() {
    	var pid = parseInt($(this).find('a:first').attr('p-id'));
    	var pname = $(this).find('a:first').text();
    	$('.prof').removeClass('active');
    	$(this).addClass('active');
    	app.selectProfile(pid,pname);
	    // dos tuff
	});
	
});
