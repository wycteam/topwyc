import Multiselect from 'vue-multiselect'

Vue.component('dialog-modal',  require('../components/DialogModal.vue'));
Vue.component('service-panel',  require('../components/services/ServicePanel.vue'));
Vue.component('service-list',  require('../components/services/NewServices.vue'));
Vue.component('docs-list',  require('../components/services/DocumentList.vue'));

let data = window.Laravel.user;

var app = new Vue({
	el:'#services',
	data:{
		translation: null,
		loading	: true,
		loadingmodal	: true,
		addEditDeleteAuth: false,
		selectedService: null,

		authuser : data.id,
		services:[],// list of Existing Services -REMOVE THIS
		serviceParents:[],// list all parents
		servicetype:['Parent', 'Child'],
		addOrEditModalTitle:'',
	   	addorEditFunction: '',
	   	showChildOptions: false,
	   	activeId:'', //selected ID on the table
	   	countChild:'',
	   	serviceDocuments:[], //list of all service documents
	   	selectedDocsNeeded:[],
	    selectedDocsOpt:[],
	    selectedDocsReleased:[],
	    selectedDocsReleasedOptional:[],

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            addNewService:0,
            editService:0,
            deleteService:0
        }],

        branchCosts: null,
        profileCosts: null,
        profiles: [],

		// ADD & EDIT SERVICE FORM
		addServFrm: new Form({
	        detail 			: '',
	        detail_cn		: '',
	        description 	: '',
	        description_cn	: '',
	        type   			: '', //parent or child
	        parent_id		: '',
	        cost 			: '',
	        charge 			: '',
	        tip 			: '',
	        com_client 		: '',
	        com_agent 		: '',
	        id	   			: '',
	        is_active		: '',
	        docs_needed		: [],
	        docs_optional	: [],
	        docs_released   : [],
	        docs_released_optional : [],
			branch_id: data.branch_id,
			costArray		: [],
			chargeArray		: [],
			tipArray		: [],
			comAgentArray		: [],
			comClientArray		: [],
			costBranchIdArray: [],
			chargeBranchIdArray: [],
			tipBranchIdArray: [],
			comAgentBranchIdArray: [],
			comClientBranchIdArray: [],
			months_required: 0,
			commissionable_amt: ''
	    },{ baseURL			: '/visa/service-manager'}),

	    // ADD & EDIT PROFILE FORM
		addProfFrm: new Form({
	        id 			: '',
	        name 		: '',
	        is_active	: 1,
	    },{ baseURL		: '/visa/service-manager'}),


		// ADD & EDIT PROFILE COST
		profileCost: new Form({
	        //id 			: '',
	        name 		: 'Regular Rates',
	        service_id 	: '',
	        profile_id 	: '',
	        // branch_id 	: '',
	        costArray				: [],
			chargeArray				: [],
			tipArray				: [],
			comAgentArray			: [],
			comClientArray			: [],
			costBranchIdArray		: [],
			chargeBranchIdArray		: [],
			tipBranchIdArray		: [],
			comAgentBranchIdArray	: [],
			comClientBranchIdArray	: [],
	    },{ baseURL		: '/visa/service-manager'}),

	},
	components: {
  		Multiselect,
  		'cost-component': require('../components/services/CostComponent.vue'),
  		'charge-component': require('../components/services/ChargeComponent.vue'),
  		'tip-component': require('../components/services/TipComponent.vue'),
  		'comagent-component': require('../components/services/ComAgentComponent.vue'),
  		'comclient-component': require('../components/services/ComClientComponent.vue')
  	},

	mounted() {
	    $('#editService').on('hide.bs.modal', () => {
	    	let tab = "tab-info";
	        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
	  //       this.loading = true;
			// this.emptyform();
	  //       this.emptyProfileCost();
	  //      	this.loadService();
	  //      	this.loadParents();
	    });
	   	$('#dialogRemoveService').on('hide.bs.modal', () => {
	        //this.commonFunctionReload();
	    });
	},

	methods: {
		checkAddEditDeleteAuth() {
			let uId = window.Laravel.user.id;
			
			this.addEditDeleteAuth = (uId == 1193 || uId == 1) ? true : false;
		},

		getTranslation() {
			axios.get('/visa/service-manager/translation')
          		.then(response => {
            		this.translation = response.data.translation;
          		});
		},

		showRemoveServiceDialog() {
			$('#dialogRemoveService').modal('toggle');
		},

		showServiceDetailModal() {
			axios.get('/visa/service-manager/getService/'+ this.activeId)
				.then(response => {
					this.selectedService = response.data;

					$('#serviceDetailModal').modal('show');
				});
		},

		// getProfileCost() {
		// 	axios.get('/visa/service-manager/profile-cost')
		// 		.then(result => {
		// 			this.profileCosts = result.data.profileCost;
		// 		});
		// },

		getAllProfile() {
			axios.get('/visa/service-manager/getAllProfile')
				.then(result => {
					this.profiles = result.data.profiles;
				});
		},

		emptyProfileCost() {
			this.profileCost = new Form({
		        //id 			: '',
		        name 		: 'Regular Rates',
		        service_id 	: '',
		        profile_id 	: '',
		        // branch_id 	: '',
		        costArray				: [],
				chargeArray				: [],
				tipArray				: [],
				comAgentArray			: [],
				comClientArray			: [],
				costBranchIdArray		: [],
				chargeBranchIdArray		: [],
				tipBranchIdArray		: [],
				comAgentBranchIdArray	: [],
				comClientBranchIdArray	: [],
		    },{ baseURL		: '/visa/service-manager'})
		},

		selectProfile(pid, pname) {
			var sid = this.addServFrm.id;

			// save before changing of profile data
			// var last_profile_id = this.profileCost.profile_id;
			// if(last_profile_id > 0){
			// 	this.setProfileCost();
			// 	this.profileCost.submit('post', '/editProfileCost')
			//     .then(result => {
			//         toastr.success('Profile Price Updated');
			//     })
			//     .catch(error => {
			//         toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			//     });
			// }
			
			if(pid > 0){			
				axios.get('/visa/service-manager/service-profile-cost/'+pid+'/'+sid)
					.then(result => {
						this.profileCosts = result.data.profile_costs;
						this.profileCost.profile_id = pid;
						this.profileCost.name = pname;
					});
			}
			else{
				// this.emptyProfileCost();
				this.profileCost.service_id = this.addServFrm.id;
			}
		},

		setProfileCost() {
			this.profileCost.costArray = [];
			this.profileCost.chargeArray = [];
			this.profileCost.tipArray = [];
			this.profileCost.comAgentArray = [];
			this.profileCost.comClientArray = [];
			this.profileCost.costBranchIdArray = [];
			this.profileCost.chargeBranchIdArray = [];
			this.profileCost.tipBranchIdArray = [];
			this.profileCost.comAgentBranchIdArray = [];
			this.profileCost.comClientBranchIdArray = [];

			this.profileCosts.map((bc, index) => {
				let costRef = 'cost-' + index;
				let chargeRef = 'charge-' + index;
				let tipRef = 'tip-' + index;
				let comagentRef = 'comagent-' + index;
				let comclientRef = 'comclient-' + index;

				let cost = this.$refs['costcomponent'].$refs[costRef][0].value;
				let charge = this.$refs['chargecomponent'].$refs[chargeRef][0].value;
				let tip = this.$refs['tipcomponent'].$refs[tipRef][0].value;
				let comagent = this.$refs['comagentcomponent'].$refs[comagentRef][0].value;
				let comclient = this.$refs['comclientcomponent'].$refs[comclientRef][0].value;

				this.profileCost.costArray.push(cost);
				this.profileCost.chargeArray.push(charge);
				this.profileCost.tipArray.push(tip);
				this.profileCost.comAgentArray.push(comagent);
				this.profileCost.comClientArray.push(comclient);

				/////////////////////////////////////////////////

				let costBranchIdRef = 'cost-branch-id-' + index;
				let chargeBranchIdRef = 'charge-branch-id-' + index;
				let tipBranchIdRef = 'tip-branch-id-' + index;
				let comAgentBranchIdRef = 'comagent-branch-id-' + index;
				let comClientBranchIdRef = 'comclient-branch-id-' + index;

				let costBranchId = this.$refs['costcomponent'].$refs[costBranchIdRef][0].value;
				let chargeBranchId = this.$refs['chargecomponent'].$refs[chargeBranchIdRef][0].value;
				let tipBranchId = this.$refs['tipcomponent'].$refs[tipBranchIdRef][0].value;
				let comAgentBranchId = this.$refs['comagentcomponent'].$refs[comAgentBranchIdRef][0].value;
				let comClientBranchId = this.$refs['comclientcomponent'].$refs[comClientBranchIdRef][0].value;

				this.profileCost.costBranchIdArray.push(costBranchId);
				this.profileCost.chargeBranchIdArray.push(chargeBranchId);
				this.profileCost.tipBranchIdArray.push(tipBranchId);
				this.profileCost.comAgentBranchIdArray.push(comAgentBranchId);
				this.profileCost.comClientBranchIdArray.push(comClientBranchId);
			});
		},

		getBranchCost() {
			axios.get('/visa/service-manager/branch-cost')
				.then(result => {
					this.branchCosts = result.data.branchCost;
				});
		},

		setBranchCost() {
			this.addServFrm.costArray = [];
			this.addServFrm.chargeArray = [];
			this.addServFrm.tipArray = [];
			this.addServFrm.comAgentArray = [];
			this.addServFrm.comClientArray = [];
			this.addServFrm.costBranchIdArray = [];
			this.addServFrm.chargeBranchIdArray = [];
			this.addServFrm.tipBranchIdArray = [];
			this.addServFrm.comAgentBranchIdArray = [];
			this.addServFrm.comClientBranchIdArray = [];

			this.branchCosts.map((bc, index) => {
				let costRef = 'cost-' + index;
				let chargeRef = 'charge-' + index;
				let tipRef = 'tip-' + index;
				let comagentRef = 'comagent-' + index;
				let comclientRef = 'comclient-' + index;

				let cost = this.$refs['costcomponent'].$refs[costRef][0].value;
				let charge = this.$refs['chargecomponent'].$refs[chargeRef][0].value;
				let tip = this.$refs['tipcomponent'].$refs[tipRef][0].value;
				let comagent = this.$refs['comagentcomponent'].$refs[comagentRef][0].value;
				let comclient = this.$refs['comclientcomponent'].$refs[comclientRef][0].value;

				this.addServFrm.costArray.push(cost);
				this.addServFrm.chargeArray.push(charge);
				this.addServFrm.tipArray.push(tip);
				this.addServFrm.comAgentArray.push(comagent);
				this.addServFrm.comClientArray.push(comclient);

				/////////////////////////////////////////////////

				let costBranchIdRef = 'cost-branch-id-' + index;
				let chargeBranchIdRef = 'charge-branch-id-' + index;
				let tipBranchIdRef = 'tip-branch-id-' + index;
				let comAgentBranchIdRef = 'comagent-branch-id-' + index;
				let comClientBranchIdRef = 'comclient-branch-id-' + index;

				let costBranchId = this.$refs['costcomponent'].$refs[costBranchIdRef][0].value;
				let chargeBranchId = this.$refs['chargecomponent'].$refs[chargeBranchIdRef][0].value;
				let tipBranchId = this.$refs['tipcomponent'].$refs[tipBranchIdRef][0].value;
				let comAgentBranchId = this.$refs['comagentcomponent'].$refs[comAgentBranchIdRef][0].value;
				let comClientBranchId = this.$refs['comclientcomponent'].$refs[comClientBranchIdRef][0].value;

				this.addServFrm.costBranchIdArray.push(costBranchId);
				this.addServFrm.chargeBranchIdArray.push(chargeBranchId);
				this.addServFrm.tipBranchIdArray.push(tipBranchId);
				this.addServFrm.comAgentBranchIdArray.push(comAgentBranchId);
				this.addServFrm.comClientBranchIdArray.push(comClientBranchId);
			});
		},

		addOrEdit(d){
			if(d == "add"){
				this.addOrEditModalTitle = ((this.translation) ? this.translation['add-new-service'] : 'Add New Service');
				this.addorEditFunction = 'add';
				this.loadingmodal = false;
				this.commonFunctionReload();

				this.getBranchCost();

				$('#editService').modal('toggle');
			}else if(d == "edit"){
				var needed =[];
				var optional = [];
				var released = [];
				var released_optional = [];
				this.loadingmodal = true;
				axios.get('/visa/service-manager/getService/'+ this.activeId)
					.then(result =>{

						this.branchCosts = result.data.branch_costs;
						//this.profileCosts = result.data.profile_costs;
					    //console.log(this.branchCosts);
					   	this.addServFrm.detail = result.data.detail;
					   	this.addServFrm.detail_cn = result.data.detail_cn;
						this.addServFrm.description = result.data.description;
						this.addServFrm.description_cn = result.data.description_cn;
						this.addServFrm.parent_id = result.data.parent_id;
						this.addServFrm.cost = parseFloat(result.data.cost);
						this.addServFrm.charge = parseFloat(result.data.charge);
						this.addServFrm.tip = parseFloat(result.data.tip);
						this.addServFrm.com_agent = parseFloat(result.data.com_agent);
						this.addServFrm.com_client = parseFloat(result.data.com_client);
						this.addServFrm.id = result.data.id;
						//console.log(result.data.id);
						//this.addServFrm.commissionable_amt = parseFloat(result.data.commissionable_amt);
						this.addServFrm.months_required = result.data.months_required;

						this.profileCost.service_id = result.data.id;
						try{
							if(result.data.select_docs_optional){
								result.data.select_docs_optional.map(function(value, key) {
								 optional.push(value[0]);
							 });
							}
						}catch(err){
						}
						try{
							if(result.data.select_docs_released){
								result.data.select_docs_released.map(function(value, key) {
								 released.push(value[0]);
							 });
							}
						}catch(err){
						}
						try{
							if(result.data.select_docs_released_optional){
								result.data.select_docs_released_optional.map(function(value, key) {
								 released_optional.push(value[0]);
							 });
							}
						}catch(err){
						}
					    
					   
					   	
					   	
					    this.addServFrm.docs_needed = needed;
						this.addServFrm.docs_optional = optional;
						this.addServFrm.docs_released = released;
						this.addServFrm.docs_released_optional = released_optional;

					    if(result.data.parent_id == 0){
					   		this.addServFrm.type = 'Parent';
					   		this.showChildOptions = false;
					    }else{
					   		this.addServFrm.type = 'Child';
					   		this.showChildOptions = true;
					    }

					    this.loadingmodal = false;
				})

				this.addOrEditModalTitle = ((this.translation) ? this.translation['edit-service'] : 'Edit Service');
				this.addorEditFunction = 'edit';
				//$('#editService').modal('toggle');
			}
		},
		changeActiveId(id){
			this.activeId = id;
		},

		addServiceProfile(){
			this.emptyprofileform();
			$('#addProfile').modal('show');
		},
		//reset add/edit service form
		emptyprofileform(){
			this.addProfFrm = new Form({
		       	id 			: '',
		       	name 			: '',
		        is_active		: 1,

	    	},{ baseURL			: '/visa/service-manager'})
		},

		saveProfileForm(){
			if(this.addProfFrm.id == ""){
				this.addProfFrm.submit('post', '/saveProfile')
			    .then(result => {
			        if(result.success){
			        	toastr.success(result.message);
			        	this.getAllProfile();
			    		$('#addProfile').modal('hide');
			        }
			        else{
			        	toastr.error(result.message);
			        }

			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			    });
			    //$('#addProfile').modal('hide');
			    //location.reload();
			}
			else{
				this.addProfFrm.submit('post', '/saveProfile')
			    .then(result => {
			        toastr.success( (this.translation) ? this.translation['profile-updated'] : 'Profile Updated.');
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			    });
				//location.reload();
			}
		},

		saveForm:function (event){
			//this.addServFrm.branch_id = data.branch_id;
			var neededList='';
			var optionalList='';
			var releasedList='';
			var releasedOptionalList='';
			try{
				this.addServFrm.docs_needed.map(function(e) {
					if(neededList == ''){
						neededList = e.id
					}else{
						neededList += "," + e.id;
					}
				});
			}catch(err){
			}
			try{
				this.addServFrm.docs_optional.map(function(e) {
					if(optionalList == ''){
						optionalList = e.id
					}else{
						optionalList += "," + e.id;
					}
				});
			}catch(err){
			}
			try{
				this.addServFrm.docs_released.map(function(e) {
					if(releasedList == ''){
						releasedList = e.id
					}else{
						releasedList += "," + e.id;
					}
				});
			}catch(err){
			}
			try{
				this.addServFrm.docs_released_optional.map(function(e) {
					if(releasedOptionalList == ''){
						releasedOptionalList = e.id
					}else{
						releasedOptionalList += "," + e.id;
					}
				});
			}catch(err){
			}
			

			this.addServFrm.docs_needed = neededList;
			this.addServFrm.docs_optional = optionalList;
			this.addServFrm.docs_released = releasedList;
			this.addServFrm.docs_released_optional = releasedOptionalList;
			if(this.addorEditFunction == "add"){
				// check if "parent" is selected
				if(this.addServFrm.type == "Parent"){
					this.addServFrm.parent_id = 0;
					this.addServFrm.cost = 0;
					this.addServFrm.charge = 0;
					this.addServFrm.tip = 0;
					this.addServFrm.com_agent = 0;
					this.addServFrm.com_client = 0;
				}
				this.addServFrm.is_active = 1;

				// Service Branch Cost
				if(this.addServFrm.parent_id != 0) {
					this.setBranchCost();
				}

				// save form
				this.addServFrm.submit('post', '/save')
			    .then(result => {
			    	this.commonFunctionReload();
			        toastr.success( (this.translation) ? this.translation['new-service-added'] : 'New Service Added.');
			        this.loadService();
	       			this.loadParents();
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
			    });
			    $('#editService').modal('toggle');
			}else if (this.addorEditFunction == "edit"){
				if(this.addServFrm.type == "Parent"){
					this.addServFrm.parent_id = 0;
					this.addServFrm.cost = 0;
					this.addServFrm.charge = 0;
					this.addServFrm.tip = 0;
					this.addServFrm.com_client = 0;
					this.addServFrm.com_agent = 0;
				}else if(this.addServFrm.type == "Child"){
					if(this.addServFrm.parent_id == '0'){
						this.addServFrm.parent_id = '';
					}
					if(this.addServFrm.cost == '0.00'){
						this.addServFrm.cost = '';
					}
					if(this.addServFrm.charge == '0.00'){
						this.addServFrm.charge = '';
					}
					if(this.addServFrm.tip == '0.00'){
						this.addServFrm.tip = '';
					}
				}

				// Service Branch Cost
				if(this.addServFrm.parent_id != 0) {
					this.setBranchCost();
				}

				if(this.profileCost.profile_id > 0) {
					this.setProfileCost();
					this.profileCost.submit('post', '/editProfileCost')
				    .then(result => {
				        toastr.success('Profile Price Updated');
				    })
				    .catch(error => {
				        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
				    });
				}
				
				this.addServFrm.submit('post', '/edit')
					.then(result => {
				    	this.commonFunctionReload();
				    	this.loadService();
	       				this.loadParents();
				        toastr.success( (this.translation) ? this.translation['service-updated'] : 'Service Updated.');
				    })
				    .catch(error => {
				        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
				    });
			    $('#editService').modal('toggle');
			}

		},


		//show child options on modal
		changeServiceType(){
			var type = this.addServFrm.type;
			if(type == "Parent"){
				this.showChildOptions = false;
			}else if (type == "Child"){
				this.showChildOptions = true;
			}
		},

		//reset add/edit service form
		emptyform(){
			this.activeId = '';
			this.showChildOptions = false;
			this.selectedDocsNeeded=[];
	    	this.selectedDocsOpt=[];
	    	this.selectedDocsReleased=[];
	    	this.selectedDocsReleasedOptional=[];
			this.addServFrm = new Form({
		       	detail 			: '',
		        detail_cn		: '',
		        description 	: '',
		        description_cn	: '',
		        type   			: '', //parent or child
		        parent_id		: '',
		        cost 			: '',
		        id	   			: '',
		        is_active		: '',
		        docs_needed		: [],
	        	docs_optional	: [],
	        	docs_released   : [],
	        	docs_released_optional : [],
	        	charge 			: '',
	        	tip 			: '',
	        	com_agent 		: '',
	        	com_client 		: '',
				branch_id:data.branch_id,
				costArray		: [],
				chargeArray 	: [],
				tipArray		: [],
				comAgentArray		: [],
				comClientArray		: [],
				costBranchIdArray: [],
				chargeBranchIdArray: [],
				tipBranchIdArray: [],
				comAgentBranchIdArray: [],
				comClientBranchIdArray: [],
				months_required: 0,
				commissionable_amt: ''
	    	},{ baseURL			: '/visa/service-manager'})


		},

		deleteService(){
			// this.countChildren();
			// if( this.countChild == 0){
				axios.get('/visa/service-manager/delete/' + this.activeId)
		        .then(result => {
		        	if(result.data.success){
			        	toastr.success(result.data.message);
		        		this.commonFunctionReload();
		        		this.loadService();
	       				this.loadParents();
			        }
			        else{
			        	toastr.error(result.data.message);
			        }
				    //toastr.success( (this.translation) ? this.translation['service-deleted'] : 'Service Deleted.');
			    })
			    .catch(error => {
			    	this.commonFunctionReload();
			        toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete Failed.');
			    });
			// }else{
			//	 toastr.error( (this.translation) ? this.translation['delete-failed-delete-child-services-first'] : 'Delete Failed. Delete child services first.');
			// }
		},

		loadService(){
			axios.get('/visa/service-manager/show')
	        .then(result => {
	          this.services = result.data;
			  this.callDataTables();

	    	});
		},

		loadParents(){
			axios.get('/visa/service-manager/showParents')
	        .then(result => {
	          this.serviceParents = result.data;
			  //this.callDataTables();
	    	});
		},

		loadDocs(){
			axios.get('/visa/service-manager/service-documents')
	        .then(result => {
	          this.serviceDocuments = result.data;
	    	});
		},

		commonFunctionReload(){
			this.emptyProfileCost();
			this.emptyform();
	       	// this.loadService();
	       	// this.loadParents();
		},

		reloadDataTable(){
			setTimeout(function(){
				$('#lists').DataTable({
					"aaSorting": [],
					autoWidth: false,
				   columnDefs: [
				      { width: '40%', targets: 0 }, 
				      { width: '15%', targets: 1 }, 
				      { width: '15%', targets: 2 },  
				      { width: '15%', targets: 3 },  
				   ],
		            pageLength: 10,
		            responsive: true,
		            dom: '<"top"lf>rt<"bottom"ip><"clear">'
		        });
			},500);
		},

		countChildren(id){
			axios.get('/visa/service-manager/showChildren/' + id)
	        .then(result => {
	        	this.countChild = result.data.length;
			})
		},

		callDataTables(){
			this.loading = true;
			$('#lists').DataTable().destroy();

			var vm = this;

			setTimeout(function(){
			//var groupColumn = 2;
				$('#lists').DataTable({
					"aaSorting": [],
					autoWidth: false,
					lengthMenu: [ [-1,10, 25, 50, 100], ["All", 10, 25, 50, 100] ],
					"iDisplayLength": -1,
				   columnDefs: [
				      { width: '40%', targets: 0 }, 
				      { width: '15%', targets: 1 }, 
				      { width: '15%', targets: 2 },  
				      { width: '15%', targets: 3 },  
				   ],
		            dom: '<"top"lf>r<"clear">t<"bottom"p>',
		         //    "drawCallback": function ( settings ) {
			        //     var api = this.api();
			        //     var rows = api.rows( {page:'current'} ).nodes();
			        //     var last=null;
			 
			        //     api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
			        //         if ( last !== group ) {
			        //             $(rows).eq( i ).before(
			        //                 '<tr class="group"><td colspan="5">'+group+'</td></tr>'
			        //             );
			 
			        //             last = group;
			        //         }
			        //     } );
			        // }
		        });

		    //     $('#lists tbody').on('click', '.toggle-row', function () {
		    //     	var dis = $(this).attr('id');
			   //      var tr = $(this).closest('tr');
			   //      var row = table.row( tr );

			   //      if(row.child.isShown()) {
			   //          row.child.hide();
			   //          tr.removeClass('shown');
			   //      } else {
						// var id = dis.split("_");
			   //      	axios.get('/visa/service-manager/showChildren/' +id[1])
			   //      	.then(result => {
			   //      		var parent = "#parent_"+id[1];
						// 	$(parent).closest("td").closest("tr").remove();
			   //      		$(parent).closest("td").remove();

			   //      		$(parent).remove();
			   //      		$(parent+" tr").remove();
				  //           row.child(vm.formatRow(result.data,id[1])).show();
			   //      	})
			   //      	tr.addClass('shown');
			   //      }
			   //  });

			 },1);
			this.loading = false;
		},

		formatRow(d,parent) {
			if(d.length > 0){
				var ret =  '<table class="table table-bordered table-striped table-hovered" id="parent_'+parent+'">';

				for(var i= 0; i<d.length;i++) {
					if(this.addEditDeleteAuth) {
						var btn ='<center>';
		                btn += '<a style="margin-right:8px;" class="editServ" id="serv_'+d[i].id+'" data-toggle="tooltip" title="Edit Service">';
		                btn += '<i class="fa fa-pencil fa-1x"></i>';
		                btn += '</a>';
		                if(this.authuser == '1' || this.authuser == '1193'){
			                btn += '<a data-toggle="tooltip"  class="deleteServ" id="dserv_'+d[i].id+'" title="Delete">';
			                btn += '<i class="fa fa-trash fa-1x"></i>';
			                btn += '</a>';
			            }
		            	btn +='</center>';
					} else {
						var btn ='<center>';
							btn += '<a data-toggle="tooltip" title="View" class="viewServ" id="dserv_'+d[i].id+'">';
			                	btn += 'View';
			                btn += '</a>';
						btn +='</center>';
					}
					
					ret +='<tr>'+
		            '<td><p style="padding-left: 30px; margin-bottom: 0px;">> '+d[i].detail+'</p></td>'+
		            '<td>'+d[i].description+'</td>'+
		            '<td>'+ (parseFloat(d[i].cost) + parseFloat(d[i].charge) + parseFloat(d[i].tip)).toFixed(2) +'</td>'+
		            '<td>'+btn+'</td>'+
		        	'</tr>';
		        }

        		ret+='</table>';

				return ret;
			}
		}
	},
	created(){
		this.loadService();
		this.loadParents();
		this.getAllProfile();
		this.checkAddEditDeleteAuth();

		if(window.Laravel.language == 'cn'){
			this.getTranslation();
		}


        // var x = this.permissions.filter(obj => { return obj.type === 'Service Manager'});

        // var y = x.filter(obj => { return obj.name === 'Add New Service'});

        // if(y.length==0) this.selpermissions.addNewService = 0;
        // else this.selpermissions.addNewService = 1;

        // var y = x.filter(obj => { return obj.name === 'Edit Service'});

        // if(y.length==0) this.selpermissions.editService = 0;
        // else this.selpermissions.editService = 1;

        // var y = x.filter(obj => { return obj.name === 'Delete Service'});

        // if(y.length==0) this.selpermissions.deleteService = 0;
        // else this.selpermissions.deleteService = 1;

		//this.loadDocs();
		// this.callDataTables();
	},
});

var app2 = new Vue({
	el:'#documents',
	data:{
		translation: null,

		_action: null,

		documents:[],
		act:'',
		sel_id:'',

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            addNewDocument:0,
            editDocument:0,
            deleteDocument:0
        }],

		docsFrm: new Form({
	       	id 		: '',
		    title 	: '',
		   	title_cn: '',
		   	is_unique: 0
	    },{ baseURL	: '/visa/service-manager/service-documents'})
	},
	methods:{
		getTranslation() {
			if(window.Laravel.language == 'cn'){
	      		axios.get('/visa/service-manager/service-documents/translation')
	          		.then(response => {
	            		this.translation = response.data.translation;
	          		});
          	}
    	},
		loadDocs(){
			axios.get('/visa/service-manager/service-documents')
	        .then(result => {
	          this.documents = result.data;

	          //this.reloadDataTable();
	    	});
		},
		reloadDataTable(){
			// $('#lists').DataTable().destroy();

			// setTimeout(function(){
			// 	$('#lists').DataTable({
		 //            responsive: true,
		 //        });
			// },500);
		},
		saveDocs(e){
		   	if(e == "Add"){
				this.docsFrm.submit('post', '/add')
			    .then(result => {
			        toastr.success( (this.translation) ? this.translation['new-service-document-added'] : 'New Service Document Added' );
			        $('#dialogAddDocument').modal('toggle');
			        this.reloadAll();
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed' );
			    });
			}else if (e == "Edit"){
				this.docsFrm.submit('post', '/edit')
			    .then(result => {
			        toastr.success( (this.translation) ? this.translation['service-document-updated'] : 'Service Document Updated' );
			        $('#dialogAddDocument').modal('toggle');
			        this.reloadAll();
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed' );
			    });
			}else if (e == "Delete"){
				axios.post('/visa/service-manager/service-documents/delete/'+ this.sel_id)
			    .then(result => {
			        toastr.success( (this.translation) ? this.translation['service-document-deleted'] : 'Service Document Deleted' );
			        this.reloadAll();
			    })
			    .catch(error => {
			        toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete Failed' );
			    });
			}
		},

		actionDoc(e, id){
			if(e == "add"){
				this._action = 'Add';
				this.resetDocFrm();
				$('#dialogAddDocument').modal('toggle');
				this.act = (this.translation) ? this.translation['add'] : 'Add' ;
			}else if (e == "edit"){
				this._action = 'Edit';
				this.act = (this.translation) ? this.translation['edit'] : 'Edit' ;
				axios.get('/visa/service-manager/service-documents/info/'+ id)
					.then(result =>{
					this.docsFrm.id = result.data.id;
					this.docsFrm.title = result.data.title;
					this.docsFrm.title_cn = result.data.title_cn;
					this.docsFrm.is_unique = result.data.is_unique;
				})
				$('#dialogAddDocument').modal('toggle');
			}else if (e == "delete"){
				this._action = 'Delete';
				this.act = "Delete";
				this.sel_id = id;
				$('#dialogRemoveDocument').modal('toggle');
			}
		},

		resetDocFrm(){
			this.docsFrm = new Form({
				id 		: '',
		        title 	: '',
		        title_cn: '',
		        is_unique: 0
		    },{ baseURL	: '/visa/service-manager/service-documents'})
		},
		reloadAll(){
			this.resetDocFrm();
			this.loadDocs();
		}
	},
	created(){
		this.getTranslation();

        var x = this.permissions.filter(obj => { return obj.type === 'Service Manager'});

        var y = x.filter(obj => { return obj.name === 'Add New Document'});

        if(y.length==0) this.selpermissions.addNewDocument = 0;
        else this.selpermissions.addNewDocument = 1;

        var y = x.filter(obj => { return obj.name === 'Edit Document'});

        if(y.length==0) this.selpermissions.editDocument = 0;
        else this.selpermissions.editDocument = 1;

        var y = x.filter(obj => { return obj.name === 'Delete Document'});

        if(y.length==0) this.selpermissions.deleteDocument = 0;
        else this.selpermissions.deleteDocument = 1;

		this.loadDocs();
	},
});

var app3 = new Vue({
	el:'#service_info',
	data:{
		serv:[]
	},
	methods:{
		loadService(){
			// Controller method doesn't exist; 
			// axios.get('/visa/service-manager/sortedServices2')
	        //	.then(result => {
	        //  	this.serv = result.data;
	    	// });
		},

	},
	created(){
		this.loadService();
	},
})
$(document).ready(function() {
	//for editing of child
	$(document).on("click",".editServ",function() {
		var id = $(this).attr('id')
		var id = id.split("_");
        app.changeActiveId(id[1]);
        app.addOrEdit('edit');
    });

	//for deleting of child
    $(document).on("click",".deleteServ",function() {
		var id = $(this).attr('id')
		var id = id.split("_");
        app.changeActiveId(id[1]);
        app.showRemoveServiceDialog();
    });

    // for viewing of child
    $(document).on("click",".viewServ",function() {
		var id = $(this).attr('id')
		var id = id.split("_");
        app.changeActiveId(id[1]);
        app.showServiceDetailModal();
    });


    $(document).on("click",".prof",function() {
    	var pid = parseInt($(this).find('a:first').attr('p-id'));
    	var pname = $(this).find('a:first').text();
    	$('.prof').removeClass('active');
    	$(this).addClass('active');
    	app.selectProfile(pid,pname);
	    // dos tuff
	});
});
