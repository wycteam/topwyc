
var Branches = new Vue({
	el:'#branches',
	data:{
		translation: null,
		mode:'',
		modalTitle:'',
		frmBranch:new Form({
         	name:''
        }, { baseURL: '/visa/branch'}),
		branches:[],
		selectedID:'',
	},
	created(){
		this.getTranslation();

		 this.showAll();
	},
	methods: {
		getTranslation() {
			axios.get('/visa/branch/translation')
	            .then(response => {
	                this.translation = response.data.translation;
	            });
		},

		changeMode(mode, id){
			if(mode == "Add"){
				this.resetFrm();

				this.mode = "Add";
				this.modalTitle = (this.translation) ? this.translation['add-new-branch'] : "Add New Branch";
				$('#addEditBranch').modal('toggle');
			}else if (mode == "Edit"){
				this.mode = "Edit";
				this.selectedID = id;
				axios.get('/visa/branch/show/'+id)
				.then(result => {
					this.frmBranch = new Form ({
						name : result.data.name,
					},{ baseURL: '/visa/branch'});

				});
				$('#addEditBranch').modal('toggle');
				this.modalTitle = (this.translation) ? this.translation['edit-branch'] : "Edit Branch";
			}else if (mode == "Delete"){
				this.selectedID = id;
				this.mode = "Delete";
				$('#deleteBranchPrompt').modal('toggle');
			}
		},

		saveAddEditFrm(){
			if(this.mode == "Add"){
				this.addBranch();
			}else if (this.mode == "Edit"){
				this.editBranch();
			}
		},

		resetFrm(){
			this.frmBranch = new Form({
	         	name:''
	        }, { baseURL: '/visa/branch'})
		},
		addBranch(){
			this.frmBranch.submit('post', '/save')
	          	.then(result => {
	            this.resetFrm();
	            this.showAll();
	            $('#addEditBranch').modal('toggle');
	            toastr.success( (this.translation) ? this.translation['branch-added'] : 'Branch Added.');
	        })
	        .catch(error => {
	            toastr.error( (this.translation) ? this.translation['saving-failed'] : 'Saving Failed.');
	        });
		},


		editBranch(){
			this.frmBranch.submit('post', '/edit/'+ this.selectedID)
	          	.then(result => {
	            this.showAll();
	            this.resetFrm();
	            $('#addEditBranch').modal('toggle');
	            toastr.success( (this.translation) ? this.translation['branch-successfully-updated'] : 'Branch successfully updated.');
	        })
	        .catch(error => {
	            toastr.error( (this.translation) ? this.translation['update-failed'] : 'Update Failed.');
	        });
		},

		deleteBranch(){
			axios.post('/visa/branch/delete/'+this.selectedID)
			.then(result => {
				this.showAll();
	          	$('#deleteBranchPrompt').modal('toggle');
		          	toastr.success( (this.translation) ? this.translation['branch-successfully-deleted'] : 'Branch successfully deleted.');
		      	})
			.catch(error => {
	            toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete Failed');
	        });
		},
		showAll(){
			axios.get('/visa/branch/list')
	        .then(result => {
	            this.branches = result.data;
		    });

		}

	}
});