Vue.component('news-content', require('../components/News/Projects.vue'));
Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    next();
});
const app = new Vue({
  el: '#newsContents',
  data: {
    translation: null
  },
  methods: {
    getTranslation() {
      axios.get('/visa/news/translation')
          .then(response => {
            this.translation = response.data.translation;
          });
    }
  },
  created() {
    this.getTranslation();
  }
});



$(document).ready(function() {
  $('.summernote').summernote({
    height: 495,
    disableResizeEditor: true,
    toolbar: [
      ['font', ['bold', 'italic', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']],
      ['insert', ['link','picture']],
      ['view', ['codeview']],
    
    ]


  });
  $('.note-statusbar').hide();

  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
              $('#imagePreview').css('background-image', 'url('+e.target.result +')');
              $('#imagePreview').hide();
              $('#imagePreview').fadeIn(650);

          }
          reader.readAsDataURL(input.files[0]);

      }
  }
  $("#imageUpload").change(function() {
      readURL(this);

  });
});
