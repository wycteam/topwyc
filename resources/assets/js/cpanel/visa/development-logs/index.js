import { quillEditor } from 'vue-quill-editor';

var vm = new Vue({
	el:'#devlogs',
	data:{
		mode:'',
		modalTitle:'',
		activeId:'',
		logForm: new Form ({
			title	:'',
			body	:'',
			date	:'',
			version	:'',
		},{ baseURL: '/visa/dev-logs'}),
		latestUpdate:{},
		oldUpdates:{},
		dt:''
	},
	mounted() {
	    $('#editUpdatesModal').on('hide.bs.modal', () => {
	        this.resetAll();
	    });
	    $('#deleteLogPrompt').on('hide.bs.modal', () => {
	        this.resetAll();
	    });
	    $(document).on('focus', 'form#daily-form .input-group.date', function(){
	        $(this).datepicker({
	            format:'yyyy/mm/dd',
	            todayHighlight: true,
	        });
	    });
  	},	
	created(){
		 this.showAll();
	},
	methods:{
	    getDate(){
	    	console.log($('form#daily-form input[name=date]').val());
	    	this.dt = $('form#daily-form input[name=date]').val();
	    },
		deleteLog(){
			axios.post('/visa/dev-logs/delete-log/'+this.activeId)
			.then(result => {
				this.showAll();
	          	$('#deleteLogPrompt').modal('toggle');
		          	toastr.success('Successfully Deleted');
		      	})
			.catch(error => {
	            toastr.error('Delete Failed');
	        });
		},
		addUpdate(){
			this.logForm.date = this.dt;

			this.logForm.submit('post'	, '/add-update')
	          	.then(result => {
	            this.showAll();
	            $('#editUpdatesModal').modal('toggle');
	            this.resetAll();
	            toastr.success('Update Added.');
	        })
	        .catch(error => {
	            toastr.error('Update Failed.');
	        });
		},
		editLog(){
			this.logForm.date = this.dt;
			this.logForm.submit('post', '/edit-log/'+this.activeId)
	          	.then(result => {
	            this.showAll();
	            $('#editUpdatesModal').modal('toggle');
	            this.resetAll();
	            toastr.success('Update Added.');
	        })
	        .catch(error => {
	            toastr.error('Update Failed.');
	        });
		},
		showlogs(){
			axios.get('/visa/dev-logs/old-logs')
	        .then(result => {
	            this.oldUpdates = result.data;
	            this.oldUpdates.length = result.data.length;
		    });
		},
		showLatestUpdate(){
			axios.get('/visa/dev-logs/new-log')
			.then(result =>{
				this.latestUpdate = result.data;
			});
		},
		switchMode(e, id){
			this.resetAll();
			if(e == "Add"){
				this.mode = "Add";
				this.modalTitle = "Add an Update";
				$('#editUpdatesModal').modal('toggle');
			}else if(e == "Edit"){
				this.activeId = id;
				this.mode = "Edit";
				this.modalTitle = "Edit a Log";
				this.getLog();
				$('#editUpdatesModal').modal('toggle');
			}else if(e == "Delete"){
				$('#deleteLogPrompt').modal('toggle');
				this.activeId = id;
			}
		},

		getLog(){
			axios.post('/visa/dev-logs/get-log/'+this.activeId) 
			.then(result => {
	          	this.logForm = new Form ({
					title	:result.data.title,
					body	:result.data.body,
				  	version	:result.data.version,
				  	date 	:result.data.date
				},{ baseURL: '/visa/dev-logs'});
				this.dt = result.data.date;
				$('form#daily-form input[name=date]').val(result.data.date);
	      	});
		},

		doFunction(){
			var e = this.mode;
			if(e == "Add"){
				this.addUpdate();
			}else if(e == "Edit"){
				this.editLog();
			}
		},

		showAll(){
			this.showLatestUpdate();
			this.showlogs();
		},

		resetAll:function(){
			this.activeId = '';
			this.logForm = new Form ({
				title	:'',
				body	:'',
				date	:'',
				version	:'',
			},{ baseURL: '/visa/dev-logs'});
			
		}
	},
	components: {
		quillEditor
		,'dialog-modal' : require('../components/DialogModal.vue')
		,'old-update-list' : require('../../components/DevelopmentLogs/OldLogs.vue')
	},
});

