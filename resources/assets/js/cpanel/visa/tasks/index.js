Vue.component('InfiniteLoading', require('vue-infinite-loading'));
Vue.component('pagination', require('laravel-vue-pagination'));

new Vue({
	el: '#app',

	data: {
		translation: null,

		employees: []
	},

	methods: {
		getTranslation() {
			axios.get('/visa/tasks/translation')
		        .then(response => {
		          	this.translation = response.data.translation;
		        });
		},

		getEmployees() {
			axios.get('/emp')
				.then(response => {
					this.employees = response.data;
				});
		}
	},

	created() {
		this.getTranslation();
		
		this.getEmployees();
	},

	components: {
        'todays-tasks': require('../components/TodaysTasks.vue'),
        'todays-tasks-v2': require('../components/TodaysTasksV2.vue'),
        'edit-who': require('../components/EditWho.vue'),
        'task-history': require('../components/TaskHistory.vue'),
        'reminders': require('../components/Reminders.vue')
	}
});