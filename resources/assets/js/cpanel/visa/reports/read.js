Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

let data = window.Laravel.user;

var readReport = new Vue({
	el:'#read-report',
	data:{
		reports:[],
        services:[],
        trackingno:'',
        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            filter:0
        }]
	},
	methods: {
		submitFilter(){
            params = {
                start: $('form#date-range-form input[name=start]').val(),
                end: $('form#date-range-form input[name=end]').val(),
                client_id: $('form#date-range-form input[name=client_id]').val(),
            };

            axios.get('/visa/report/report-filter', {
                params: params
            })
            .then(response => {
                //this.reports = response.data;
                //this.callDataTables();

                this.reports = response.data;
                this.createDatatableToggle(this.reports);
                // this.createDatatableToggle(this.reports);
            });
		},
        // callDataTables(){
        //     $('#lists').DataTable().destroy();    
        //     setTimeout(function(){
        //         $('#lists').DataTable({
        //             responsive: true,
        //             "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        //             "iDisplayLength": 10,
        //             "order": [[ 2, "desc" ]],
        //             "columns": [
        //                 {
        //                     "width": "35%",
        //                 },
        //                 { 
        //                     "width": "5%",
        //                 },
        //                 {
        //                     "width": "20%",
        //                 },
        //                 {
        //                     "width": "40%",
        //                 }, 

        //             ]
        //         });
        //     },2000);           
        // },

        createDatatableToggle(reports) {

            $('#lists').DataTable().destroy();

            setTimeout(function() {

                var format = function(d) {
                    // console.log(d);
                    var ids = '';
                    var clids = [];
                    for(var i=0; i<(d.client).length; i++) {
                        var found = jQuery.inArray(d.client[i].client_id, clids);
                         // console.log(found);
                        if (found < 0) {
                            // Element was not found, add it.
                            clids.push(d.client[i].client_id);
                            ids += '<a href="/visa/client/'+d.client[i].client_id+'" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="'+d.client[i].fullname +'" data-trigger="hover">'+d.client[i].client_id +'</a>'+",";
                        }
                        // console.log(clids);
                        // if(i != ((d.client).length - 1) ){
                        //     ids = ids+",";
                        // }

                    }
                    // ids = rtrim(ids,',');
                    return '<table cellpadding="5" cellspacing="0" class="table table-striped table-hover dtchildrow" >'+
                                '<tr style="padding-left:50px;">'+
                                    '<td><b>Report : </b>'+d.detail+'</td>'+
                                    // '<td><b>Client IDs : </b> '+ids+'</td>'+
                                '</tr>'+
                            '</table>';
                };
                
                table2 = $('#lists').DataTable( {
                    "data": reports,
                    responsive: true,
                    "columns": [
                        // {
                        //     "width": "5%",
                        //     "className":      'service-control text-center',
                        //     "orderable":      false,
                        //     "data":           null,
                        //     "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
                        // },
                        { 
                            "width": "40%",
                            "className":      'service-control',
                            data: null, render: function(reports, type, row) {
                                if(reports.type == "report"){
                                    return '<b style="color: #1ab394;">' + reports.service + '</b>'
                                }
                                return '<b style="color: black;">' + reports.service + '</b>'
                            }
                        },
                        {
                            "className":      'service-control',
                            "width": "13%",
                            data: null, render: function(reports, type, row) {
                                var sid = '';
                                for(var i=0; i<(reports.client).length; i++) {
                                    sid += reports.client[i].client_id+",";
                                }
                                return '<b>'+reports.log_date+ '</b><span style="display:none;">'+sid+'</span>'
                            }
                        },
                        {
                            "className":      'service-control',
                            "width": "12%",
                            data: null, render: function(reports, type, row, meta) {
                                return '<b>'+reports.processor+ '</b>'
                            }
                        },
                        {
                            "width": "35%",
                            "className":      'service-control wrapok',
                            // "class": "wrapnot",
                            data: null, render: function(reports, type, row, meta) {
                                var ids = '';
                                var clids = [];
                                for(var i=0; i<(reports.client).length; i++) {
                                    var found = jQuery.inArray(reports.client[i].client_id, clids);
                                     // console.log(found);
                                    if (found < 0) {
                                        // Element was not found, add it.
                                        clids.push(reports.client[i].client_id);
                                        ids += '<a href="/visa/client/'+reports.client[i].client_id+'" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="'+reports.client[i].client_id +'" data-trigger="hover">'+reports.client[i].fullname +'</a>'+" , ";
                                    }
                                    // console.log(clids);
                                    // if(i != ((d.client).length - 1) ){
                                    //     ids = ids+",";
                                    // }

                                }
                                return ids;
                            }
                        },
                    ]
                });
                $('body').off('click', 'table#lists tbody td.service-control');
                $('body').on('click', 'table#lists tbody td.service-control', function (e) {
                    var tr2 = $(this).closest('tr');
                    var row = table2.row( tr2 );
                         
                    if(row.child.isShown()) {
                        row.child.hide();
                        tr2.removeClass('shown');
                    } else {
                        row.child( format(row.data()) ).show();
                        tr2.addClass('shown');
                    }
                });
                //EXPAND ALL CHILD ROWS BY DEFAULT
                table2.rows().every(function(){
                    // If row has details collapsed
                    if(!this.child.isShown()){
                        // Open this row
                        this.child(format(this.data())).show();
                        $(this.node()).addClass('shown');
                    }
                });


                // Handle click on "Expand All" button
                $('#btn-show-all-children').on('click', function(){
                    // Enumerate all rows
                    table2.rows().every(function(){
                        // If row has details collapsed
                        if(!this.child.isShown()){
                            // Open this row
                            this.child(format(this.data())).show();
                            $(this.node()).addClass('shown');
                        }
                    });
                });

                // Handle click on "Collapse All" button
                $('#btn-hide-all-children').on('click', function(){
                    // Enumerate all rows
                    table2.rows().every(function(){
                        // If row has details expanded
                        if(this.child.isShown()){
                            // Collapse row details
                            this.child.hide();
                            $(this.node()).removeClass('shown');
                        }
                    });
                });

            }.bind(this), 2000);

        },


        showTracking(id){
            this.trackingno = id;
            axios.get('/visa/report/get-services/'+id)
            .then(response => {
                this.services = response.data;
                $('#tracking').modal('toggle');
            });
        }
	},
	created(){
	    axios.get('/visa/report/get-reports/')
	    .then(response => {
            $('form#date-range-form input[name=start],form#date-range-form input[name=end]').datepicker({format:'yyyy/mm/dd'});
            $('form#date-range-form input[name=start],form#date-range-form input[name=end]').datepicker('setDate', 'today');
	        this.reports = response.data;
            this.createDatatableToggle(this.reports);
	    });

        var x = this.permissions.filter(obj => { return obj.type === 'Reports'});

        var y = x.filter(obj => { return obj.name === 'Filter'});

        if(y.length==0) this.selpermissions.filter = 0;
        else this.selpermissions.filter = 1;

        console.log(this.selpermissions.filter);
	},
    mounted() {
        $(document).on('focus', 'form#date-range-form .input-daterange', function(){
            $(this).datepicker({
                format:'yyyy/mm/dd',
                todayHighlight: true
            });
        });
        $('body').popover({ // Ok
            html:true,
            trigger: 'hover',
            selector: '[data-toggle="popover"]'
        });
    }

});

$( document ).ready(function() {
    $('[data-toggle=popover]').popover();

    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });


});