Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

let data = window.Laravel.user;

var writeReport = new Vue({
	el:'#write-report',
	data:{
        translation: null,
		clients:[],
        services:[],
        selected:[],
        reportForm:new Form({
          id: '',
          message: '',
        }, { baseURL: 'http://'+window.Laravel.cpanel_url }),

        quickReportClientsId: [],
        quickReportClientServicesId: [],
        quickReportTrackings: [],

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            continue:0
        }]
	},
	methods: {
        getTranslation() {
           axios.get('/visa/report/translation')
               .then(response => {
                   this.translation = response.data.translation; // Vue Model
                   window.translation = response.data.translation // Window
               });
        },
		addClient(){
		var clids = $("#selService").attr("cl-ids");
		      if(clids==""){
		        toastr.error( (this.translation) ? this.translation['please-select-client-first'] : 'Please select client first' );
		      }
		      else{
			    var href = "/visa/report/select-services/"+clids;
			    window.location.href = href;
		    }
		},
        showServices(id, e) {
            if(e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#client-' + id).toggleClass('hide');
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },
        selectServices(item){
            if(this.selected.indexOf(item)>-1)
            {
                indx = this.selected.indexOf(item);
                this.selected.splice(indx, 1);

            }else
            {
                this.selected.push(item); 
            }
        },
        getQuickReportClientIdsArray(clientServices) {
            let clientIds = [];
            clientServices.forEach(clientService => {
                if(clientIds.indexOf(clientService.client_id) == -1)
                    clientIds.push(clientService.client_id);
            });

            return clientIds;
        },
        getQuickReportTrackingsArray(clientServices) {
            let trackings = [];
            clientServices.forEach(clientService => {
                if(trackings.indexOf(clientService.tracking) == -1)
                    trackings.push(clientService.tracking);
            });

            return trackings;
        },
        showReport(){
            if(this.selected.length!=0){
                var id = ''
                $.each( this.selected, function( key, value ) {
                  id = value.id + ';' + id;
                });
                this.reportForm.id=id;
                // $('#reports').modal('toggle');

                this.quickReportClientsId = this.getQuickReportClientIdsArray(this.selected);
                this.quickReportClientServicesId = this.selected;
                this.quickReportTrackings = this.getQuickReportTrackingsArray(this.selected);
                // this.$refs.multiplequickreportref.fetchDocs(this.selected);

                setTimeout(() => {
                    $('#add-quick-report-modal').modal('show');

                    // swal({
                    //     title: "Do you want to create a report?",
                    //     type: 'info',
                    //     showCancelButton: true,
                    //     confirmButtonColor: '#3085d6',
                    //     cancelButtonColor: '#d33',
                    //     confirmButtonText: 'YES'
                    // })
                    // .then(function(response) {
                    //     $('#add-quick-report-modal').modal('show');
                    // })
                    // .catch(function(response) {

                    // });

                    $('body').css("padding-right", "0px");
                }, 1000);
            }
            else
                toastr.error( (this.translation) ? this.translation['please-select-service-first'] : 'Please select service first' );
        },
        saveReport(){
            // this.reportForm.submit('post', '/visa/report/save-report')
            // .then(result => {
            //     toastr.success('New Report Added.');
            //     $('#reports').modal('toggle');
            //     this.selected = [];
            //     this.reportForm.id = '';
            //     this.reportForm.message = '';
            //     $('input:checkbox').prop('checked',false);
            // });

        }

	},
	created(){
        this.getTranslation();

	    axios.get('/visa/report/get-clients/'+window.Laravel.data)
	    .then(response => {
	        this.clients = response.data;
	    });

        var x = this.permissions.filter(obj => { return obj.type === 'Reports'});

        var y = x.filter(obj => { return obj.name === 'Continue'});

        if(y.length==0) this.selpermissions.continue = 0;
        else this.selpermissions.continue = 1;
	},
    components: {
        // 'multiple-quick-report': require('../../components/Visa/MultipleQuickReport.vue')
        'multiple-quick-report-v2': require('../../components/Visa/MultipleQuickReportV2.vue')
    }
});

$(document).ready(function() {

    $('#lists').DataTable({
    	"ajax": "/storage/data/ClientsReport.txt",
        responsive: true,
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "iDisplayLength": 10
    });

} );