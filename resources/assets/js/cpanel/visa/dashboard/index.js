import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)
//Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

let data = window.Laravel.user;

var dashboard = new Vue({
	el:'#dashboard',
	data:{
    translation: null,
		summaryReports:[],
		todaysReports:[],
    lastReportedDate:[],
		todaysServices:[],
    deliverySchedules:[],
		service:'',
        setting: data.access_control[0].setting,
        permissions: data.permissions,
        form:new Form({
          id: '',
          reason: '',
        }, { baseURL: 'http://'+window.Laravel.cpanel_url }),
        addSchedule:new Form({
          id:'',
          item:'',
          rider_id:'',
          location:'',
          scheduled_date:'',
          scheduled_time:'',
        }, { baseURL: '/visa/home/'}),
        dt:'',
        selId:'',
        mode:''
	},
	methods: {
        getTranslation() {
            if(window.Laravel.language == 'cn'){
                axios.get('/visa/home/translation')
                   .then(response => {
                       this.translation = response.data.translation;
                   });
            }
        },

				arrangeListInThreecolumn(list){

						var tempChildContainer = [];
						var tempList = [];
						var ctr = 0;

						list.map(function (item, index) {
								if(index % 3 == 0){
									ctr = 0;
										tempChildContainer = [];
										tempChildContainer.push(item)
								}else{
										tempChildContainer.push(item)
								} ctr = ctr + 1;

								if(ctr == 3 || (index + 1) === list.length){
										tempList.push(tempChildContainer)
								}
						});
						return tempList;
				},

				callServicesSummaryReport(){
					axios.get('/visa/home/servicesSummaryReport')
						.then(result => {
							this.summaryReports = result.data;

							this.summaryReports.today_tasks = this.arrangeListInThreecolumn(result.data.today_tasks);
							this.summaryReports.reminders = this.arrangeListInThreecolumn(result.data.reminders);
						});
				},


        // setService(service) {
        // 	let { id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs, docs_needed, docs_optional, detail, parent_id} = service;
				//
        // 	if(service.discount2.length == 0) {
        // 		discount = '';
        // 		reason = '';
        // 	} else {
        // 		discount = service.discount2[0].discount_amount;
        // 		reason = service.discount2[0].reason;
        // 	}
				// 	//console.log(parent_id);
        // 	this.$refs.editservicedashboardref.setService(id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs, detail, parent_id);
        //   this.$refs.editservicedashboardref.fetchDocs(docs_needed, docs_optional);
        // },


	},
	created(){
        this.getTranslation();
				//this.callServicesSummaryReport();
	},
	components: {
			 'service-list': require('../components/Dashboard/ServiceList.vue'),
			 'tasks-list': require('../components/Dashboard/TasksList.vue'),
	},
  mounted() {

  }
});
