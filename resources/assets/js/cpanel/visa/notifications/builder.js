new Vue({

	el: '#notification-builder',

	data: {

		reportTypes: [],
		docs: [],

		selectedReportTypeId: null,

		selectedDocumentId: null,

	},

	methods: {

		loadReportTypes() {
			axios.get('/visa/notification/report-type')
				.then(response => {
					this.reportTypes = response.data;

					this.callDataTable('report-types-table');
				})
				.catch(error => console.log(error));
		},

		loadDocs() {
			axios.get('/visa/notification/doc')
				.then(response => {
					this.docs = response.data;

					this.callDataTable('docs-table');
				})
				.catch(error => console.log(error));
		},

		callDataTable(tableId){
        	$('#' + tableId).DataTable().destroy();

        	setTimeout(() => {
        		$('#' + tableId).DataTable({
	                responsive: true,
	                "lengthMenu": [[10, 25, 50], [10, 25, 50]],
	                "iDisplayLength": 10
	            });
        	}, 500);
        },

        setSelectedReportTypeId(val) {
        	this.selectedReportTypeId = val;
        },

        setSelectedReportType(val) {
        	this.$refs.updatereporttyperef.updateReportTypeForm.title = val.title;
        	this.$refs.updatereporttyperef.updateReportTypeForm.title_cn = val.title_cn;
        	this.$refs.updatereporttyperef.updateReportTypeForm.description = val.description;
        	this.$refs.updatereporttyperef.updateReportTypeForm.services_id = (val.services_id).split(",");
        	this.$refs.updatereporttyperef.updateChosen((val.services_id).split(","));
        	this.$refs.updatereporttyperef.updateReportTypeForm.with_docs = (val.with_docs == 1) ? true : false;
        },

        setSelectedDocumentId(val) {
        	this.selectedDocumentId = val;
        },

        setSelectedDocument(val) {
        	this.$refs.updatedocumentref.updateDocumentForm.title = val.title;
        	this.$refs.updatedocumentref.updateDocumentForm.title_cn = val.title_cn;
        	this.$refs.updatedocumentref.updateDocumentForm.doc_types = val.doc_types;
        }

	},

	created() {
		this.loadReportTypes();

		this.loadDocs();
	},

	components: {
		'add-report-type': require('../../components/Visa/AddReportType.vue'),
		'update-report-type': require('../../components/Visa/UpdateReportType.vue'),
		'delete-report-type': require('../../components/Visa/DeleteReportType.vue'),

		'add-document': require('../../components/Visa/AddDocument.vue'),
		'update-document': require('../../components/Visa/UpdateDocument.vue'),
		'delete-document': require('../../components/Visa/DeleteDocument.vue'),
	}

});