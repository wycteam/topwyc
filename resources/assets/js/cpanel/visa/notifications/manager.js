var manager = new Vue({
	el:'#manager',
	data:{
		clients:[],
        services:[],
        reportType:[],
        docs:[],
        selected:[],
        selectedReportType:[],
        selectedDocs:[],
        reportForm:new Form({
          id: '',
          message: '',
        }, { baseURL: 'http://'+window.Laravel.cpanel_url })
	},
	methods: {
		addClient(){
            var rt = $(".select2").val();
		    var clids = $("#selService").attr("cl-ids");
            if(rt==""){
                toastr.error("Please select report type!");
		    } else if (clids==""){
                toastr.error("Please select client first!");
            } else{
			    var href = "/visa/notification/select-services/"+clids+'/'+rt;
			    window.location.href = href;
		    }
		},
        showServices(id, e) {
            if(e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#client-' + id).toggleClass('hide');
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },
        selectServices(item){
            if(this.selected.indexOf(item)>-1)
            {
                indx = this.selected.indexOf(item);
                this.selected.splice(indx, 1);

            }else
            {
                this.selected.push(item); 
            }
        },
        selectDocs(item){
            if(this.selectedDocs.indexOf(item)>-1)
            {
                indx = this.selectedDocs.indexOf(item);
                this.selectedDocs.splice(indx, 1);

            }else
            {
                this.selectedDocs.push(item); 
            }
        },
        saveReport(){
            if(this.selected.length!=0){

                var id = ''
                $.each( this.selected, function( key, value ) {
                  id = value.id + ';' + id;
                });

                this.reportForm.id=id;

                if(this.selectedReportType.with_docs == '1'){

                    if(this.selectedDocs.length!=0){

                        var title = ''
                        $.each( this.selectedDocs, function( key, value ) {
                          title = value.title + ', ' + title;
                        });

                        var title = title.slice(0, -2);

                        this.reportForm.message = this.selectedReportType.description + ' with the following document/s: ' + title + '.';

                        this.reportForm.submit('post', '/visa/notification/save-report')
                        .then(result => {
                            toastr.success('New Report Added.');
                            this.selected = [];
                            this.selectedDocs = [];
                            $('input:checkbox').prop('checked',false);
                        });
                    }
                    else
                        toastr.error("Please select documents first!");                    

                } else {

                        this.reportForm.message = this.selectedReportType.description;

                        this.reportForm.submit('post', '/visa/notification/save-report')
                        .then(result => {
                            toastr.success('New Report Added.');
                            this.selected = [];
                            this.selectedDocs = [];
                            $('input:checkbox').prop('checked',false);
                        });
                }
            }
            else
                toastr.error("Please select service first!");
        },

        hideService(data){
            if(this.selectedReportType.length != 0){
                var c = 0;
                var a = this.selectedReportType.services_id;
                var s = a.split(',');
                $.each(s, function(i, item) {
                  if(s[i] == data.service_id){
                    c++;
                  }
                });
                return c;
            }
        }

	},
	created(){

        axios.get('/visa/notification/getReportType')
        .then(response => {
            this.reportType = response.data;

            $(".select2").select2({
                placeholder: "Select report type",
                allowClear: true
            });
        });

	    axios.get('/visa/notification/get-clients/'+window.Laravel.data)
	    .then(response => {
	        this.clients = response.data;

            axios.get('/visa/notification/getSelectedReportType/'+window.Laravel.message)
            .then(response => {
                this.selectedReportType = response.data[0];
            });            
	    });

        axios.get('/visa/notification/doc')
        .then(response => {
            this.docs = response.data;
        });
	},
});

$(document).ready(function() {

    $('#lists').DataTable({
    	"ajax": "/storage/data/ClientsReport.txt",
        responsive: true,
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "iDisplayLength": 10
    });

} );