Vue.component('action-logs',  require('../clients/ActionLogs.vue'));
Vue.component('transaction-logs',  require('../clients/GroupTransactionLogs.vue'));
Vue.component('commission-logs',  require('../clients/CommissionLogs.vue'));
Vue.component('view-report',  require('../clients/ViewReport.vue'));

//export const EventBus = new Vue();

let data = window.Laravel.user;

new Vue({

	el: '#app',

	data: {
		page_wizard:1,

		transactionLogsTranslation: null,

		translation:null,
	
		service_editor:false,

		userauth: data.id,
		
		groupId: null, // Ok

		branchId: null,

		details: null, // Ok

		members: null, // Ok
		members2:null,

		serviceCost: [], // Ok

		seLclientId: '',

		assignLeaderForm: new Form({ // Ok
			clientId: '',
			type:''
		}, { baseURL: axiosAPIv1.defaults.baseURL }),

		quickReportClientsId: [],
		quickReportClientServicesId: [],
		quickReportTrackings: [],

		actionLogs: [],
		current_year: null,
		current_month: null,
		current_day: null,
		c_year: null,
		c_month: null,
		c_day: null,
		s_count: 1,

		limit: 0,
		loading: true,

		transactionLogs: [],
		commissionLogs: [],
		current_year2: null,
		current_month2: null,
		current_day2: null,
		c_year2: null,
		c_month2: null,
		c_day2: null,
		s_count2: 1,

		deposits: [],
		payments: [],
		refunds: [],
		discounts: [],
		payDepo:[],
		allTransactions:[],

		gServices: null, 
		gBatch: null, 
 
		docsrcv: null, 

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
			editAddress:0,
			actionLogs:0,
			transactionLogs:0,
			deposits:0,
			payments:0,
			refunds:0,
			discounts:0,
			newMember:0,
			addNewService:0,
			addFunds:0,
			writeReport:0,
			makeLeader:0,
			transfer:0,
			delete:0,
			editService:0
        }],

        report_id : '',
        report_view : '',
		pselected : 0,
		agent_commission: {
			client_id:0,
			full_name:''
		},
		client_commission: {
			client_id:0,
			full_name:''
		},
		
	
        current_cost : 0,
        current_branch : 0,
        payment_status : '',
        service_profiles : [],
		batches : [],
		agents:[],
		clients:[]
	},

	methods: {
		// Ok
		init() {
			
			let pathArray = window.location.pathname.split( '/' );
			this.groupId = pathArray[3];
			//this.fetchGroupData();
			//this.fetchGBatch();
			this.fetchDetails();
			
			//this.fetchAgentCommissionMembers();
			// this.fetchGServices();
		},

		// Ok
		fetchDetails() {
			this.fetchProfiles();
			this.fetchBranch();
			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/summary')
				.then(response => {
					this.details = response.data;
					this.branchId = response.data.group.branch_id;
					this.current_branch = (this.details.group.branch_id);
					this.pselected = (this.details.group.cost_level);
					this.current_cost = (this.details.group.cost_level);
					this.payment_status = (this.details.group.risk);
				})
				.catch(error => console.log(error));
		},

		// Ok
		fetchProfiles() {
			axiosAPIv1.get('/visa/service-profiles')
				.then(response => {
					this.service_profiles = response.data;
				})
				.catch(error => console.log(error));
		},
		fetchGroupData() {
			let groupId = this.groupId;
			axiosAPIv1.get('/visa/group-commission/'+groupId)
				.then(response => {
					this.agent_commission.client_id = (response.data[0]['agent_com_id'] != null) ?  response.data[0]['agent_com_id'] : 0;
					this.client_commission.client_id = (response.data[0]['client_com_id'] != null) ?  response.data[0]['client_com_id'] : 0;
				
					axiosAPIv1.get('/visa/group/' + this.client_commission.client_id + '/'+ this.agent_commission.client_id  +'/client-commission-user')
					.then(response => {
						
						try{
							this.agent_commission.full_name = response.data.agent_com.full_name;
						}catch(err){}
						try{
							this.client_commission.full_name = response.data.client_com.full_name;
						}catch(err){}
						
					
					})
					.catch(error => console.log(error));
					})
		},
		removeCommission(val){
			let groupId = this.groupId;
			var method = (val=='client') ? 1 : 2;
			axiosAPIv1.post('/visa/group/commission-remove',{
				group_id: groupId,
				method_id: method
			})
			.then(response => {
				this.fetchGroupData();
				if(method==1){
					$(".chosen-select-for-clients2").val([]);
					$(".chosen-select-for-clients2").text('');
					$(".clients_select .search-choice").remove();
					$(".clients_select input").attr('placeholder','Select clients commission');
					
				}else{
					$(".chosen-select-for-clients3").val([]);
					$(".chosen-select-for-clients3").text('');
					$(".agents_select .search-choice").remove();
					$(".agents_select input").attr('placeholder','Select agents commission');
				}
			})
		},
				// Ok
		fetchBranch() {
			axiosAPIv1.get('/visa/fetch-branch')
				.then(response => {
					this.batches = response.data;
				})
				.catch(error => console.log(error));
		},

		getTransactionLogsTranslation() {
			if(window.Laravel.language == 'cn'){
				axios.get('/visa/client/translation/transaction-logs')
			        .then(response => {
			          	this.transactionLogsTranslation = response.data.translation;
			        });
			    }
		},
		getTranslation() {
			if(window.Laravel.language == 'cn'){
				axios.get('/visa/group/translation')
			        .then(response => {
			          	this.translation = response.data.translation;
			        });
		    }
		},

		// Ok
	    switchCost(e) {
	    	if(e.target.value != this.current_cost){
	      		this.changeCostLevel(e.target.value);
	    	}
	    },

	    // Ok
	    switchBranch(e) {
	    	if(e.target.value != this.current_branch){
	      		this.changeBranch(e.target.value);
	    	}
	    },

		changeCostLevel(level) {
			//alert(level);
			let groupId = this.groupId;
			var self = this;
			var pselect = $("#profileSelect option:selected").text();
				swal({
                    title: "Are you sure you want to switch service charge to "+pselect+"?",
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'YES'
                })
                .then(function(response) {
                    axiosAPIv1.get('/visa/group/' + groupId + '/switch-cost-level/' + level)
					.then(response => {
						self.init();
						this.pselected = level;
						this.current_cost = level;
					})
					.catch(error => console.log(error));
                })
                .catch(function(response) {

                });
			
		},
		checkEdit(){
			if(this.service_editor){
				this.service_editor = false;
				this.checkboxesStatus();
			
			
			}else{
				this.service_editor = true;
				this.checkboxesStatus();
			
			}
			
		},
		checkboxesStatus(){
			if(this.service_editor){
				$('.not-show').removeClass('not-show').addClass('showing');
				$('.not-show').removeClass('not-show').addClass('showing');
			}else{
				$('.showing').removeClass('showing').addClass('not-show');
				$('.showing').removeClass('showing').addClass('not-show');
				$('[name="selectedServices[]"]:checked').prop('checked', false);  
				this.nxt = false;
			}
		},
		changeBranch(branch_id) {
			//alert(level);
			let groupId = this.groupId;
			var self = this;
			var pselect = $("#branchSelect option:selected").text();
				swal({
                    title: "Are you sure you want to transfer this group to "+pselect+" branch?",
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'YES'
                })
                .then(function(response) {
                    axiosAPIv1.get('/visa/group/' + groupId + '/switch-branch/' + branch_id)
					.then(response => {
						self.init();
						this.branchId = branch_id;
						this.current_branch = branch_id;
					})
					.catch(error => console.log(error));
                })
                .catch(function(response) {

                });
			
		},

		// Ok
		fetchMembers() {
			//if(this.members ==null){
			//this.$refs.groupmembers.isLoading = true;
			this.service_editor = false;
			//this.getGroupMembers();
			let groupId = this.groupId;
			this.page_wizard=1;
			
			axiosAPIv1.get('/visa/group/' + groupId + '/members')
				.then(response => {
					//this.$refs.groupmembers.isLoading = false;
					this.members = response.data;
					//console.log(this.members );
					this.$refs.clientservicesref.createDatatable('membersDatatable');
					this.getTranslation();
					// this.computeServiceCost(this.members);
					
				})
				.catch(error => console.log(error));
			// }else{
			// 	this.page_wizard=1;
			// 	this.service_editor = false;
			// 	this.$refs.groupmembers.$refs.wizards.resetEditServiceForm();
			// }
		},
	
		getGroupMembers() {
			this.groupId = window.location.pathname.split( '/' )[3];
			this.service_editor = false;
			this.page_wizard = 1;
			this.$refs.groupmembers.selectedData = [];
			//if(this.members2 ==null){
				//this.$refs.groupmembers.isLoading = true;
				this.loading=true;
				axios.get('/visa/group/' + this.groupId + '/members')
					.then((response) => {
						this.members2 = response.data;

						$('table.group-members').DataTable().destroy();

						setTimeout(() => {
							//console.log(this.members);
							
							this.$refs.groupmembers.setPrimaryRow(this.members2);
							//this.$refs.groupmembers.isLoading = false;
							this.loading=false;

						}, 1000);
					});
				//}
		},
		changeCommission(val){
			
			var com = (val=='agent') ? this.agent_com : this.client_com;
			let groupId = this.groupId;
			axiosAPIv1.post('/visa/group/' + groupId + '/commission', {
				com:com,
				com_type:val
			})
				.then(response => {
					
				})
				.catch(error => console.log(error));
		},
		setCommision(client_id,method){
			let groupId = this.groupId;
			axiosAPIv1.post('/visa/group/commission-set', {
				group_id:groupId,
				method_id:method,
				client_id: client_id
			})
				.then(response => {
					//alert();
				})
				.catch(error => console.log(error));
		},
		fetchClientsCommissionMembers(id){
			let groupId = this.groupId;
			this.clients = [];
			// $("#client_com").prop("disabled", true);
			// $("#agent_com").prop("disabled", true);
			
			axiosAPIv1.get('/visa/group/' + id + '/'+ groupId +'/client-commission-list')
				.then(response => {
					
					 this.clients = response.data.user;
					// this.clients.sort(function(a, b){
					// 	var a1= a['client']['full_name'], b1= b['client']['full_name'];
					// 	if(a1== b1) return 0;
					// 	return a1> b1? 1: -1;
					// });
					//this.agents = response.data.agent_com;
					// this.agents.sort(function(a, b){
					// 	var a1= a['full_name'], b1= b['full_name'];
					// 	if(a1== b1) return 0;
					// 	return a1> b1? 1: -1;
					// });
					
					// $("#client_com").prop("disabled", false);
					// $("#agent_com").prop("disabled", false);
					setTimeout(() => {
						var my_val = $('.chosen-select-for-clients2').val();
						$(".chosen-select-for-clients2").val(my_val).trigger("chosen:updated");

					}, 1000);
				})
				.catch(error => console.log(error));
		},
		fetchAgentsCommissionMembers(id){
			let groupId = this.groupId;
			this.agents = [];
			// $("#client_com").prop("disabled", true);
			// $("#agent_com").prop("disabled", true);
			
			axiosAPIv1.get('/visa/group/' + id + '/'+ groupId +'/agent-commission-list')
				.then(response => {
					
					 this.agents = response.data.user;
					
					setTimeout(() => {
						var my_val = $('.chosen-select-for-clients3').val();
						//console.log(my_val);
						$(".chosen-select-for-clients3").val(my_val).trigger("chosen:updated");

					}, 1000);
				})
				.catch(error => console.log(error));
		},
		// Ok
		fetchDocs(id) {
			// this.$refs.multiplequickreportref.fetchDocs(id);
		},

		// Ok
		computeServiceCost(members) {
			let groupId = this.groupId;

			this.serviceCost = members.map(member => {
				let cost = 0;
				let charge = 0;
				let tip = 0;

				member.services.map(service => {
					if(service.active == 1 && service.group_id == groupId) {
						cost += parseFloat(service.cost);
						charge += parseFloat(service.charge);
						tip += parseFloat(service.tip);	
					}
				});

				return (cost + charge + tip);
			});
		},

		// Ok
		fetchGServices() {
			this.service_editor = false;
			this.page_wizard = 1;
			this.$refs.groupservices.selectedData=[];
			let groupId = this.groupId;
			if(this.gServices==null){
				//this.$refs.groupservices.isLoading = true;
				this.loading = true;
				
				axiosAPIv1.get('/visa/group/' + groupId + '/services')
					.then(response => {
						//console.log(response.data);
						this.page_wizard==1;
						this.gServices = response.data;
						this.$refs.groupservices.createDatatableToggleByService(this.gServices);
						this.getTranslation();
						//this.$refs.groupservices.isLoading = false;
						this.loading=false;
					})
					.catch(error => console.log(error));
			}else{
				this.page_wizard=1;
				this.service_editor = false;
				this.$refs.groupservices.$refs.wizards.resetEditServiceForm();
			}
		},

		fetchGBatch() {
			let groupId = this.groupId;
			this.service_editor = false;
			this.page_wizard = 1;
			if(this.$refs.batchservices){//avoiding undifined error on first load
				this.$refs.batchservices.selectedData = [];
			}
			
			//if(this.gBatch==null){
				this.loading = true;
				axiosAPIv1.get('/visa/group/' + groupId + '/batch')
					.then(response => {
						//console.log(response.data);
						this.gBatch = response.data;
						this.$refs.batchservices.createDatatableToggleByBatch(this.gBatch);
						this.getTranslation();
						//this.$refs.batchservices.isLoading = false;
						this.loading=false;
					})
					.catch(error => console.log(error));
			// }else{
			// 	this.page_wizard=1;
			// 	this.service_editor = false;
			// 	this.$refs.batchservices.$refs.wizards.resetEditServiceForm();
			// }
		},

		// Ok
		assignLeader(clientId, type) {
			let groupId = this.groupId;

			this.assignLeaderForm.clientId = clientId;
			this.assignLeaderForm.type = type;

			this.assignLeaderForm.submit('patch', '/visa/group/' + groupId + '/assign-leader')
            .then(result => {
            	//console.log(result);
                if(result.success) {

                	this.$refs.groupmembers.$refs.wizards.resetLoadedData();
                	this.getGroupMembers();

                	this.fetchMembers();
                	this.fetchDetails();

					$('#vice-leader-modal').modal('hide');
					toastr.success(result.message);
				}
            })
            .catch(error => console.log(error));

            this.seLclientId = "";
		},

		// Ok
		showMemberRow(id, e) {
			if(e.target.nodeName != 'A') {
				setTimeout(function() {
					$('#member-row-' + id).toggleClass('hide').removeAttr('style');
				}, 500);
			}
		},

		createDatatable(className) {
			$('table.'+className).DataTable().destroy();

			setTimeout(function() {
				$('table.'+className).DataTable({
			        pageLength: 10,
			        responsive: true,
			        "aaSorting": [],
			        dom: '<"top"lf>rt<"bottom"ip><"clear">'
			    });
			}, 500);
		},

		createDatatable2(className) {
			$('table.'+className).DataTable().destroy();

			setTimeout(function() {
				$('table.'+className).DataTable({
			        pageLength: 25,
			        responsive: true,
			        "aaSorting": [],
			        dom: '<"top"lf>rt<"bottom"ip><"clear">'
			    });
			}, 500);
		},

		filterCurrency(value) {
			if(value != null){
			    if(value%1 != 0){
			       return  this.numberWithCommas(parseFloat(value).toFixed(2));
			    } else {
			       return  this.numberWithCommas(parseInt(value));
			    }
			} else{
				return 0;
			}
		},

		numberWithCommas(value) {
			return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},

		createQuickReport(clientId, clientServiceId, tracking) {
			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/client-service/' + clientServiceId)
				.then(response => {
					let clientService = response.data;

					this.quickReportClientsId = [clientId];
					this.quickReportClientServicesId = clientService;
					this.quickReportTrackings = [tracking];
					
					this.fetchDocs(clientService);

					$('#add-quick-report-modal').modal('show');
				})
				.catch(error => console.log(error));
		},

		createDatatableToggleRow(data) {
			var groupId = this.groupId;

			let serviceCost = data.map(member => {
				let cost = 0;
				let charge = 0;
				let tip = 0;

				member.services.map(service => {
					if(service.active == 1 && service.group_id == groupId) {
						cost += parseFloat(service.cost);
						charge += parseFloat(service.charge);
						tip += parseFloat(service.tip);	
					}
				});

				return this.filterCurrency(cost + charge + tip);
			});

			$('table.dt-members').DataTable().destroy();

			var vi = this;

			setTimeout(function() {

				var format = function(d) {
					let cid = null;
					let table = '<table class="table table-bordered table-striped table-hover">'+
								'<thead>'+
		                            '<tr>'+
			                            '<th>Package</th>'+
			                            '<th>Date Recorded</th>'+
			                            '<th>Details</th>'+
			                            '<th>Status</th>'+
			                            // '<th>Cost</th>'+
			                            '<th>Charge</th>'+
			                            // '<th>Additional Fee</th>'+
			                            '<th>Discount</th>'+
			                            '<th class="text-center">Action</th>'+
		                           '</tr>'+
		                        '</thead>'+
		                        '<tbody>';

		                        for(var i=0; i<(d.services).length; i++) {
		                        	cid = d.client.document_receive;
		                        	let clientServiceId = JSON.parse(JSON.stringify(d.services[i])).id;

		                        	var className = (d.services[i].active == 0) ? 'strikethrough' : '';
		                        	var visibility = (d.services[i].group_id == groupId) ? 'table-row' : 'none';
		                        	if(d.services[i].remarks) {
		                        		var remarks = d.services[i].remarks;
		                        		var details = '<a href="#" data-toggle="popover" data-placement="right" data-container="body" data-content="'+remarks+'" data-trigger="hover">'+d.services[i].detail+'</a>';
		                        	} else {
		                        		var details = d.services[i].detail;
		                        	}
		                        	if(d.services[i].discount2.length > 0) {
		                        		if(d.services[i].discount2[0].reason) {
		                        			var reason = d.services[i].discount2[0].reason;
		                        			var discount = '<a href="#" data-toggle="popover" data-placement="left" data-container="body" data-content="'+reason+'" data-trigger="hover">'+d.services[i].discount2[0].amount+'</a>';
		                        		} else {
		                        			var discount = d.services[i].discount2[0].amount;
		                        		}
		                        	} else {
		                        		var discount = '&nbsp;';
		                        	}
		                        	var _action = '<a href="javascript:void(0)" class="edit-service-action" data-toggle="modal" data-target="#edit-service-modal" data-serviceclientid="'+d.services[i].client_id+'" data-serviceserviceid="'+d.services[i].service_id+'" data-serviceid="'+d.services[i].id+'"><i class="fa fa-pencil-square-o" data-serviceclientid="'+d.services[i].client_id+'" data-serviceserviceid="'+d.services[i].service_id+'" data-serviceid="'+d.services[i].id+'"></i></a> &nbsp; <a href="javascript:void(0)" class="write-report-action" data-clientid="'+d.services[i].client_id+'" data-tracking="'+d.services[i].tracking+'" data-clientserviceid="'+clientServiceId+'"><i class="fa fa-list-alt" data-clientid="'+d.services[i].client_id+'" data-tracking="'+d.services[i].tracking+'" data-clientserviceid="'+clientServiceId+'"></i></a> &nbsp; <a href="javascript:void(0)" class="view-service-report-action" data-serviceid="'+d.services[i].id+'"><i class="fa fa-list" data-serviceid="'+d.services[i].id+'"></i></a>';

		                        	table += '<tr class="'+className+'" style="display:'+visibility+'">';
		                        		table += '<td>'+d.services[i].tracking+'</td>';
		                        		table += '<td>'+d.services[i].service_date+'</td>';
		                        		table += '<td>'+details+'</td>';
		                        		table += '<td>'+d.services[i].status+'</td>';
		                        		table += '<td>'+(parseInt(d.services[i].charge)+parseInt(d.services[i].cost)+parseInt(d.services[i].tip))+'</td>';
		                        		// table += '<td>'+d.services[i].charge+'</td>';
		                        		// table += '<td>'+d.services[i].tip+'</td>';
		                        		table += '<td>'+discount+'</td>';
		                        		table += '<td class="text-center">'+_action+'</td>';

		                        	table += '</tr>';

		                        }

		                table += '</tbody></table>';

		            let rcv = cid;
					let docrcv = '<ul class="tag-list" style="padding: 0">';
		            for(var i=0; i<rcv.length; i++) {
						docrcv = docrcv +'<li><a style="cursor:default;"><i class="fa fa-tag"></i>'+rcv[i]+'</a></li>';
					}
                    docrcv = docrcv +'</ul>';

		            return docrcv+table;
				}.bind(this);

		        //if(this.selpermissions.makeLeader == 1 && this.selpermissions.transfer == 1 && this.selpermissions.delete == 1){
					table = $('table.dt-members').DataTable( {
				        "data": data,
				        "columns": [
				            {
				            	"width": "5%",
				                "className":      'details-control text-center',
				                "orderable":      false,
				                "data":           null,
				                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				            },
				            { 
				            	"width": "30%",
				            	data: "client", render: function(data, type, row) {
					                return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
					            }
					        },
					        {
				            	"width": "15%",
					        	data: "client", render: function(data, type, row) {
					        		return data.id
					        	}
					        },
					        {
				            	"width": "15%",
					        	data: null, render: function(data, type, row, meta) {
					        		return serviceCost[meta.row]
					        	}
					        },
					        { 
					        	data: null, render: function(data, type, row) {
					                if(data.leader == 1) {
					                	return '<span><i class="fa fa-star"></i><i class="fa fa-star"></i> Group Leader</span>'
					                }
					                else if(data.leader == 2) {
					           			return '<span><a href="javascript:void(0)" class="vice-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star"></i> Vice Leader</a></span>'
					                }
					                 else {
					                	return '<span><a href="javascript:void(0)" class="make-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star-o"></i> Make Vice----Leader</a></span>'
					                }
					            }, className: 'text-center'
					        },
					        { 
					        	data: null, render: function(data, type, row) {
					        		return '<a href="javascript:void(0)" class="transfer-action" data-toggle="modal" data-target="#transfer-modal" data-id="'+data.client.id+'">Transfer</a>'
					            }, className: 'text-center'
					        },
					        { 
					        	data: null, render: function(data, type, row) {
					        		return '<a href="javascript:void(0)" class="delete-action" data-toggle="modal" data-target="#delete-member-modal" data-id="'+data.client.id+'">Delete</a>'
					            }, className: 'text-center'
					        }
				        ]
				    });
		        //}

		   //      if(this.selpermissions.makeLeader == 0 && this.selpermissions.transfer == 1 && this.selpermissions.delete == 1){
					// table = $('table.dt-members').DataTable( {
				 //        "data": data,
				 //        "columns": [
				 //            {
				 //            	"width": "5%",
				 //                "className":      'details-control text-center',
				 //                "orderable":      false,
				 //                "data":           null,
				 //                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				 //            },
				 //            { 
				 //            	"width": "30%",
				 //            	data: "client", render: function(data, type, row) {
					//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
					//             }
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: "client", render: function(data, type, row) {
					//         		return data.id
					//         	}
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: null, render: function(data, type, row, meta) {
					//         		return serviceCost[meta.row]
					//         	}
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//         		return '<a href="javascript:void(0)" class="transfer-action" data-toggle="modal" data-target="#transfer-modal" data-id="'+data.client.id+'">Transfer</a>'
					//             }, className: 'text-center'
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//         		return '<a href="javascript:void(0)" class="delete-action" data-toggle="modal" data-target="#delete-member-modal" data-id="'+data.client.id+'">Delete</a>'
					//             }, className: 'text-center'
					//         }
				 //        ]
				 //    });
		   //      }

		   //      if(this.selpermissions.makeLeader == 1 && this.selpermissions.transfer == 0 && this.selpermissions.delete == 1){
					// table = $('table.dt-members').DataTable( {
				 //        "data": data,
				 //        "columns": [
				 //            {
				 //            	"width": "5%",
				 //                "className":      'details-control text-center',
				 //                "orderable":      false,
				 //                "data":           null,
				 //                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				 //            },
				 //            { 
				 //            	"width": "30%",
				 //            	data: "client", render: function(data, type, row) {
					//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
					//             }
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: "client", render: function(data, type, row) {
					//         		return data.id
					//         	}
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: null, render: function(data, type, row, meta) {
					//         		return serviceCost[meta.row]
					//         	}
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//                 if(data.leader == 1) {
					//                 	return '<span><i class="fa fa-star"></i><i class="fa fa-star"></i> Group Leader</span>'
					//                 }
					//                 else if(data.leader == 2) {
					//            			return '<span><a href="javascript:void(0)" class="vice-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star"></i> Vice Leader</a></span>'
					//                 }
					//                  else {
					//                 	return '<span><a href="javascript:void(0)" class="make-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star-o"></i> Make Vice----Leader</a></span>'
					//                 }
					//             }, className: 'text-center'
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//         		return '<a href="javascript:void(0)" class="delete-action" data-toggle="modal" data-target="#delete-member-modal" data-id="'+data.client.id+'">Delete</a>'
					//             }, className: 'text-center'
					//         }
				 //        ]
				 //    });
		   //      }

		   //      if(this.selpermissions.makeLeader == 1 && this.selpermissions.transfer == 1 && this.selpermissions.delete == 0){
					// table = $('table.dt-members').DataTable( {
				 //        "data": data,
				 //        "columns": [
				 //            {
				 //            	"width": "5%",
				 //                "className":      'details-control text-center',
				 //                "orderable":      false,
				 //                "data":           null,
				 //                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				 //            },
				 //            { 
				 //            	"width": "30%",
				 //            	data: "client", render: function(data, type, row) {
					//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
					//             }
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: "client", render: function(data, type, row) {
					//         		return data.id
					//         	}
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: null, render: function(data, type, row, meta) {
					//         		return serviceCost[meta.row]
					//         	}
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//                 if(data.leader == 1) {
					//                 	return '<span><i class="fa fa-star"></i><i class="fa fa-star"></i> Group Leader</span>'
					//                 }
					//                 else if(data.leader == 2) {
					//            			return '<span><a href="javascript:void(0)" class="vice-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star"></i> Vice Leader</a></span>'
					//                 }
					//                  else {
					//                 	return '<span><a href="javascript:void(0)" class="make-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star-o"></i> Make Vice----Leader</a></span>'
					//                 }
					//             }, className: 'text-center'
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//         		return '<a href="javascript:void(0)" class="transfer-action" data-toggle="modal" data-target="#transfer-modal" data-id="'+data.client.id+'">Transfer</a>'
					//             }, className: 'text-center'
					//         }
				 //        ]
				 //    });
		   //      }

		   //      if(this.selpermissions.makeLeader == 0 && this.selpermissions.transfer == 0 && this.selpermissions.delete == 1){
					// table = $('table.dt-members').DataTable( {
				 //        "data": data,
				 //        "columns": [
				 //            {
				 //            	"width": "5%",
				 //                "className":      'details-control text-center',
				 //                "orderable":      false,
				 //                "data":           null,
				 //                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				 //            },
				 //            { 
				 //            	"width": "30%",
				 //            	data: "client", render: function(data, type, row) {
					//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
					//             }
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: "client", render: function(data, type, row) {
					//         		return data.id
					//         	}
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: null, render: function(data, type, row, meta) {
					//         		return serviceCost[meta.row]
					//         	}
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//         		return '<a href="javascript:void(0)" class="delete-action" data-toggle="modal" data-target="#delete-member-modal" data-id="'+data.client.id+'">Delete</a>'
					//             }, className: 'text-center'
					//         }
				 //        ]
				 //    });
		   //      }

		   //      if(this.selpermissions.makeLeader == 0 && this.selpermissions.transfer == 1 && this.selpermissions.delete == 0){
					// table = $('table.dt-members').DataTable( {
				 //        "data": data,
				 //        "columns": [
				 //            {
				 //            	"width": "5%",
				 //                "className":      'details-control text-center',
				 //                "orderable":      false,
				 //                "data":           null,
				 //                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				 //            },
				 //            { 
				 //            	"width": "30%",
				 //            	data: "client", render: function(data, type, row) {
					//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
					//             }
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: "client", render: function(data, type, row) {
					//         		return data.id
					//         	}
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: null, render: function(data, type, row, meta) {
					//         		return serviceCost[meta.row]
					//         	}
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//         		return '<a href="javascript:void(0)" class="transfer-action" data-toggle="modal" data-target="#transfer-modal" data-id="'+data.client.id+'">Transfer</a>'
					//             }, className: 'text-center'
					//         }
				 //        ]
				 //    });
		   //      }

		   //      if(this.selpermissions.makeLeader == 1 && this.selpermissions.transfer == 0 && this.selpermissions.delete == 0){
					// table = $('table.dt-members').DataTable( {
				 //        "data": data,
				 //        "columns": [
				 //            {
				 //            	"width": "5%",
				 //                "className":      'details-control text-center',
				 //                "orderable":      false,
				 //                "data":           null,
				 //                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				 //            },
				 //            { 
				 //            	"width": "30%",
				 //            	data: "client", render: function(data, type, row) {
					//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
					//             }
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: "client", render: function(data, type, row) {
					//         		return data.id
					//         	}
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: null, render: function(data, type, row, meta) {
					//         		return serviceCost[meta.row]
					//         	}
					//         },
					//         { 
					//         	data: null, render: function(data, type, row) {
					//                 if(data.leader == 1) {
					//                 	return '<span><i class="fa fa-star"></i><i class="fa fa-star"></i> Group Leader</span>'
					//                 }
					//                 else if(data.leader == 2) {
					//            			return '<span><a href="javascript:void(0)" class="vice-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star"></i> Vice Leader</a></span>'
					//                 }
					//                  else {
					//                 	return '<span><a href="javascript:void(0)" class="make-leader-action" data-id="'+data.client.id+'"><i class="fa fa-star-o"></i> Make Vice----Leader</a></span>'
					//                 }
					//             }, className: 'text-center'
					//         }
				 //        ]
				 //    });
		   //      }

		   //      if(this.selpermissions.makeLeader == 0 && this.selpermissions.transfer == 0 && this.selpermissions.delete == 0){
					// table = $('table.dt-members').DataTable( {
				 //        "data": data,
				 //        "columns": [
				 //            {
				 //            	"width": "5%",
				 //                "className":      'details-control text-center',
				 //                "orderable":      false,
				 //                "data":           null,
				 //                "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
				 //            },
				 //            { 
				 //            	"width": "30%",
				 //            	data: "client", render: function(data, type, row) {
					//                 return '<a href="/visa/client/' + data.id + '" target="_blank">' + data.full_name + '</a>'
					//             }
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: "client", render: function(data, type, row) {
					//         		return data.id
					//         	}
					//         },
					//         {
				 //            	"width": "15%",
					//         	data: null, render: function(data, type, row, meta) {
					//         		return serviceCost[meta.row]
					//         	}
					//         }
				 //        ]
				 //    });
		   //      }		        
				


				$('body').off('click', 'table.dt-members tbody td.details-control');
				setTimeout(function() {
					$('body').on('click', 'table.dt-members tbody td.details-control', function (e) {
						var tr = $(this).closest('tr');
						var row = table.row( tr );
							 
						if(row.child.isShown()) {
							row.child.hide();
							tr.removeClass('shown');
						} else {
							row.child( format(row.data()) ).show();
							tr.addClass('shown');
						}
					});
				}.bind(this), 500);

				//make vice leader
				$('body').on('click', 'table.dt-members tbody a.make-leader-action', function (e) {
					let clientId = e.target.getAttribute('data-id');

					this.assignLeader(clientId, 'vice');
				}.bind(this));

				//vice leader modal
				$('body').on('click', 'table.dt-members tbody a.vice-leader-action', function (e) {
					$('#vice-leader-modal').modal('show');
					let clientId = e.target.getAttribute('data-id');
					this.seLclientId = clientId;
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody a.transfer-action', function (e) {
					let clientId = e.target.getAttribute('data-id');

					this.$refs.transferref.setMemberId(clientId);
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody a.delete-action', function (e) {
					let clientId = e.target.getAttribute('data-id');

					this.$refs.deletememberref.setMemberId(clientId);
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody .edit-service-action', function (e) {
					let serviceClientId = e.target.getAttribute('data-serviceclientid');
					let serviceServiceid = e.target.getAttribute('data-serviceserviceid');
					let serviceId = e.target.getAttribute('data-serviceid');

					this.$refs.editserviceref.init(serviceClientId, serviceServiceid, serviceId)
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody .write-report-action', function (e) {
					let clientId = e.target.getAttribute('data-clientid');
					let clientServiceId = e.target.getAttribute('data-clientserviceid');
					let tracking = e.target.getAttribute('data-tracking');

					this.createQuickReport(clientId, clientServiceId, tracking);
				}.bind(this));

				$('body').on('click', 'table.dt-members tbody .view-service-report-action', function (e) {
					let serviceId = e.target.getAttribute('data-serviceid');

					this.viewReportByService(serviceId);
				}.bind(this));

			}.bind(this), 1000);

		},

		changeMemberType(type){
			var clientId = this.seLclientId;
			this.assignLeader(clientId, type);
		},

		

		fetchDeposits(limit) {
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}

			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/deposits/' + limit)
				.then(response => {
					this.deposits = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.loading = false;
					this.limit = limit;
					this.createDatatable2('dt-deposits');
				})
				.catch(error => console.log(error));
		},

		fetchPayments(limit) {
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}

			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/payments/' + limit)
				.then(response => {
					this.payments = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.loading = false;
					this.limit = limit;
					this.createDatatable2('dt-payments');
				})
				.catch(error => console.log(error));
		},

		fetchDepositsPayments(limit) {
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}

			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/discounts-payments/' + limit)
				.then(response => {
					this.payDepo = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.loading = false;
					this.limit = limit;
					this.createDatatable2('dt-payments');
				})
				.catch(error => console.log(error));
		},


		fetchTransactions(limit) {
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}

			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/transactions/' + limit)
				.then(response => {
					this.allTransactions = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.loading = false;
					this.limit = limit;
					this.createDatatable2('dt-transactions');
				})
				.catch(error => console.log(error));
		},

		fetchRefunds(limit) {
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}

			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/refunds/' + limit)
				.then(response => {
					this.refunds = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.loading = false;
					this.limit = limit;
					this.createDatatable2('dt-refunds');
				})
				.catch(error => console.log(error));
		},

		fetchDiscounts(limit) {
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}

			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/discounts/' + limit)
				.then(response => {
					this.discounts = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.loading = false;
					this.limit = limit;
					this.createDatatable2('dt-discounts');
				})
				.catch(error => console.log(error));
		},

		resetActionLogsComponentData() {
			this.actionLogs = [];
			this.current_year = null;
			this.current_month = null;
			this.current_day = null;
			this.c_year = null;
			this.c_month = null;
			this.c_day = null;
			this.s_count = 1;
		},

		fetchActionLogs(limit) {
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}
			this.resetActionLogsComponentData();

			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/action-logs/' + limit)
				.then(response => {
					this.actionLogs = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.limit = limit;
					this.loading = false;
				})
				.catch(error => console.log(error));
		},

		resetTransactionLogsComponentData() {
			this.transactionLogs = [];
			this.current_year2 = null;
			this.current_month2 = null;
			this.current_day2 = null;
			this.c_year2 = null;
			this.c_month2 = null;
			this.c_day2 = null;
			this.s_count2 = 1;
		},

		fetchTransactionLogs(limit) {
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}
			this.resetTransactionLogsComponentData();
			
			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/transaction-logs/' + limit)
				.then(response => {
					this.transactionLogs = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.limit = limit;
					this.loading = false;
				})
				.catch(error => console.log(error));
		},
		fetchCommissionLogs(limit){
			this.loading = true;
			this.limit = 0;
			if(limit == 0){
				this.limit = limit;
			}
			//this.resetTransactionLogsComponentData();
			
			let groupId = this.groupId;

			axiosAPIv1.get('/visa/group/' + groupId + '/commission-logs/' + limit)
				.then(response => {
					this.commissionLogs = response.data;
					if(response.data.length <20){
						limit = 0;
					}
					this.limit = limit;
					this.loading = false;
					//console.log(this.commissionLogs);
				})
				.catch(error => console.log(error));
		},
		viewReport(report_id) {
			this.report_id = report_id;
			this.report_view = 'group';
			this.$refs.viewreportref.getReports();
		    $('#view-report-modal').modal('show');
		},

		viewReportByService(service_id) {
			this.report_id = service_id;
			this.report_view = 'service';
			this.$refs.viewreportref.getReports();
		    $('#view-report-modal').modal('show');
		},
	

	},
	
	created() {
		this.init(); // Ok
		//console.log(this);
		
		if(window.Laravel.language == 'cn'){
			this.getTransactionLogsTranslation();
		}
		
		  
	},
	
	mounted() {	
		// Popover init
		let vm = this;
		Promise.all([this.fetchGroupData()]);
		var typingTimer;                //timer identifier
		var doneTypingInterval = 1000;  //time in ms, 5 second for example
		var typingTimer2;                //timer identifier
		var doneTypingInterval2= 1000; 
		$('body').popover({ // Ok
		    html:true,
			trigger: 'hover',
		    selector: '[data-toggle="popover"]'
		});

		$('.chosen-select-for-clients2').chosen({
			width: "100%",
			no_results_text: "Searching Client # : ",
			search_contains: true
		});
		$('.chosen-select-for-clients2').on('change', () => {
			let clientIds = $('.chosen-select-for-clients2').val();
			this.client_commission.client_id = 0;
			this.client_commission.full_name = '';
			//console.log(clientIds[0]);
			this.client_commission.client_id = clientIds[0];
			this.setCommision(clientIds[0], 1);
			this.fetchGroupData();
			//this.client_commission.full_name = $.trim($('.chosen-select-for-clients2').text());
		});
		$('.chosen-select-for-clients3').on('change', () => {
			let clientIds = $('.chosen-select-for-clients3').val();
			this.agent_commission.client_id = 0;
			this.agent_commission.full_name = '';
			this.agent_commission.client_id = clientIds[0];
			this.setCommision(clientIds[0], 2);
			this.fetchGroupData();
			//this.agent_commission.full_name = $.trim($('.chosen-select-for-clients3').text());
		});

		$('.chosen-select-for-clients3').chosen({
			width: "100%",
			no_results_text: "Searching Client # : ",
			search_contains: true
		});

		$('.clients_select').on('keyup', '.chosen-container input[type=text]', (e) => {
			
			// setTimeout(
			// 	function(){
			// 		let q = $('.clients_select input[type=text]').val();
			// 		if( q.length >= 4 ){
			// 			//this.fetchClients(q);
			// 			vm.fetchClientsCommissionMembers(q);
			// 		}
			// 	}
			// , 500);
			clearTimeout(typingTimer);
  				typingTimer = setTimeout(
					  function(){
						let q = $('.clients_select input[type=text]').val();
					
							//this.fetchClients(q);
							vm.fetchClientsCommissionMembers(q);
						
					}
					  , doneTypingInterval);
		});
		$('.agents_select').on('keyup', '.chosen-container input[type=text]', (e) => {
				clearTimeout(typingTimer2);
  				typingTimer2 = setTimeout(
					  function(){
						let q = $('.agents_select input[type=text]').val();
						//console.log(q);
						
							//this.fetchClients(q);
							vm.fetchAgentsCommissionMembers(q);
						
					}
					  , doneTypingInterval);
			// setTimeout(
			// 	function(){
			// 		let q = $('.agents_select input[type=text]').val();
			// 		//console.log(q);
			// 		if( q.length >= 4 ){
			// 			//this.fetchClients(q);
			// 			vm.fetchAgentsCommissionMembers(q);
			// 		}
			// 	}
			// , 500);
		});
		// setTimeout(function () { 
		// 	Promise.all([this.fetchClientsCommissionMembers()]);
		//  }.bind(this), 2000)
	},

	components: {
		'edit-address': require('../../components/Visa/EditAddress.vue'),
		'add-new-member': require('../../components/Visa/AddNewMember.vue'),
	    'add-new-service': require('../../components/Visa/AddNewService.vue'),
	    'edit-service': require('../../components/Visa/EditService.vue'),
	    'add-fund': require('../../components/Visa/AddFund.vue'),
	    'transfer': require('../../components/Visa/Transfer.vue'),
	    'member-type': require('../../components/Visa/GroupMemberType.vue'),
	    'delete-member': require('../../components/Visa/DeleteMember.vue'),
	    // 'multiple-quick-report': require('../../components/Visa/MultipleQuickReport.vue'),
	    'multiple-quick-report-v2': require('../../components/Visa/MultipleQuickReportV2.vue'),
	    'client-services': require('../../components/Visa/ClientServices.vue'),
		'group-members': require('../../components/Visa/GroupMembers.vue'),

		'group-service': require('../../components/Visa/ServiceTable.vue'),
		'batch-service': require('../../components/Visa/BatchTable.vue'),
	}

});