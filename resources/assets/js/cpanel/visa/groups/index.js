let data = window.Laravel.user;

var Groups = new Vue({
	el: '#app',

	data: {
		// contacts: [],

		translation: null,
		groupId : null,
		radioValue : 'byservices',
		branches: [],

		allbalance : [],
		allpayment : [],
		total_temp_balance : 0,
		total_temp_col : 0,
		total_distribute : 0,

		total : 0,
		current : 0,
		currentbal : 0,
		tempArray : [],

		loading: false,
		selectedBranch: 0,

		form: new Form({
			groupName: '',
			branch: data.branch_id, // Default: get the current user branch
			leaderId: ''

			// contact: '',
			// address: '',
			// barangay: '',
			// city: '',
			// province: '',
			// zipCode: ''
		}, { baseURL: axiosAPIv1.defaults.baseURL }),

		// addressOptions: 'current address',

		leader: '',
		user_roles: data.roles,
		is_master : false,

        setting: data.access_control[0].setting,
        permissions: data.permissions,
        selpermissions: [{
            addNewGroup:0,
            balance:0,
            collectables:0,
            action:0
        }]
	},

	methods: {
		getTranslation() {
		    axios.get('/visa/group/translation')
		        .then(response => {
		          	this.translation = response.data.translation;
		        });
		},

		getBranches() {
    		axios.get('/visa/get-branches')
         		.then(result => this.branches = result.data);
    	},

    	numberWithCommas(value) {
    		return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    	},

    	loadGroup() {
    		this.loading = true;

    		axios.get('/visa/group/load-group-list/' + this.selectedBranch)
		        .then(result => {
		        	$('#total-balance').text( this.numberWithCommas(result.data[1]) );
		        	$('#total-collectables').text( this.numberWithCommas(result.data[2]) );

		        	this.__datatable__();
		        });
    	},

    	// checkIfSame(idkey, temp_balance, amount){
    	// 	console.log(temp_balance);
    	// 	if(this.current != idkey){
    	// 		this.current = idkey;
    	// 		this.currentbal = parseFloat(temp_balance)+parseFloat(amount);
    	// 		return this.currentbal;
    	// 	}
    	// 	else{
    	// 		this.current = idkey;
    	// 		this.currentbal +=parseFloat(amount);
    	// 		return this.currentbal;
    	// 	}
    	// },

		__datatable__() {
	    //if(this.selpermissions.balance == 1 && this.selpermissions.collectables == 1 && this.selpermissions.action == 1){
				this.callDatatable();
	    //    }

	   //      if(this.selpermissions.balance == 0 && this.selpermissions.collectables == 1 && this.selpermissions.action == 1){
				// this.callDatatable1(1);
	   //      }

	   //      if(this.selpermissions.balance == 1 && this.selpermissions.collectables == 0 && this.selpermissions.action == 1){
				// this.callDatatable1(2);
	   //      }

	   //      if(this.selpermissions.balance == 1 && this.selpermissions.collectables == 1 && this.selpermissions.action == 0){
				// this.callDatatable1(6);
	   //      }

	   //      if(this.selpermissions.balance == 0 && this.selpermissions.collectables == 0 && this.selpermissions.action == 1){
				// this.callDatatable2(1,2);
	   //      }

	   //      if(this.selpermissions.balance == 0 && this.selpermissions.collectables == 1 && this.selpermissions.action == 0){
				// this.callDatatable2(1,6);
	   //      }

	   //      if(this.selpermissions.balance == 1 && this.selpermissions.collectables == 0 && this.selpermissions.action == 0){
				// this.callDatatable2(2,6);
	   //      }

	   //      if(this.selpermissions.balance == 0 && this.selpermissions.collectables == 0 && this.selpermissions.action == 0){
				// this.callDatatable3(1,2,6);
	   //      }
		},

		fetchBalance() {
			
			//if(this.gBatch==null){
				this.loading = true;
				axiosAPIv1.get('/visa/group/all/get-balance')
					.then(response => {
						// console.log(response.data.data);
						this.allbalance = response.data.data;
						this.allpayment = response.data.payments;
						this.total_temp_balance = response.data.total_balance;
						this.total_temp_col = response.data.total_collectable;
						this.total_distribute = response.data.total_distributed;
						//this.$refs.batchservices.createDatatableToggleByBatch(this.gBatch);
						//this.getTranslation();
						//this.$refs.batchservices.isLoading = false;
						this.loading=false;
					})
					.catch(error => console.log(error));
			// }else{
			// 	this.page_wizard=1;
			// 	this.service_editor = false;
			// 	this.$refs.batchservices.$refs.wizards.resetEditServiceForm();
			// }
		},

		callDatatable() {
			$('#lists').DataTable().destroy();

			setTimeout(() => {
				$('#lists').DataTable({
					"ajax": "/storage/data/Group.txt",
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"iDisplayLength": 25,
					columnDefs: [
						{type: 'non-empty-string', targets: [4,5]}
					]
				});

				this.loading = false;
			}, 1000);
		},

		callDatatable1(a) {
			$('#lists').DataTable().destroy();

			setTimeout(() => {
				$('#lists').DataTable({
					"ajax": "/storage/data/Group.txt",
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"iDisplayLength": 25,
					columnDefs: [
						{type: 'non-empty-string', targets: [4,5]},
						{"targets": [ a ],"visible": false,"searchable": false}
					]
				});

				this.loading = false;
			}, 1000);

		},

		callDatatable2(a,b) {
			$('#lists').DataTable().destroy();

			setTimeout(() => {
				$('#lists').DataTable({
					"ajax": "/storage/data/Group.txt",
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"iDisplayLength": 25,
					columnDefs: [
						{type: 'non-empty-string', targets: [4,5]},
						{"targets": [ a ],"visible": false,"searchable": false},
						{"targets": [ b ],"visible": false,"searchable": false}
					]
				});

				this.loading = false;
			}, 1000);
		},

		callDatatable3(a,b,c) {
			$('#lists').DataTable().destroy();

			setTimeout(() => {
				$('#lists').DataTable({
					"ajax": "/storage/data/Group.txt",
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"iDisplayLength": 25,
					columnDefs: [
						{type: 'non-empty-string', targets: [4,5]},
						{"targets": [ a ],"visible": false,"searchable": false},
						{"targets": [ b ],"visible": false,"searchable": false},
						{"targets": [ c ],"visible": false,"searchable": false}
					]
				});

				this.loading = false;
			}, 1000);
		},

		addNewGroup() {
		 	this.form.submit('post', '/visa/group', {
		 		groupName: this.form.groupName,
		 		branch: this.form.branch,
		    leader: this.form.leaderId

		        // contact: this.form.contact,
		        // address: this.form.address,
		        // barangay: this.form.barangay,
		        // city: this.form.city,
		        // province: this.form.province,
		        // zipCode: this.form.zipCode
		 	})
            .then(result => {
                if(result.success) {
                	$('#addNewGroupModal').modal('hide');

			 		toastr.success(result.message);

			 		this.form.groupName = '';
			 		this.form.branch = data.branch_id;
			 		this.clearLeader();

			 		this.loadGroup();
				}
            })
            .catch(error => {
                console.log(error);
            });
		},

		clearLeader() {
			this.form.leaderId = '';

			// this.contacts = [];
			// this.form.contact = '';

			// this.addressOptions = 'current address';
			// 	this.form.address = '';
			// 	this.form.barangay = '';
			// 	this.form.city = '';
			// 	this.form.province = '';
			// 	this.form.zipCode = '';

			this.leader = '';

			$('.leader_typeahead').val('');
		},

		changeStatus(clid,stat){
			axios.get('/visa/group/changeStatus/'+clid+'/'+stat)
			.then(result => {
				$('#cs_'+clid).attr('client-status',stat);
				var fcolor = '#676a6c';
				if(stat == 'High-Risk'){
					fcolor = 'red';
				}
				$('#cl_'+clid).css('color',fcolor);
	          	swal({
                      title: (this.translation) ? this.translation['successful'] : "Successful!",
                      text: ((this.translation) ? (this.translation['status-has-been-changed-to'] + ' ') : "Status has been changed to ") + stat,
                      type: "success"
                  });
	    	});
		},
	},

	created(){
		if(window.Laravel.language == 'cn'){
				this.getTranslation();
			}

		this.getBranches();
		if (window.location.href.indexOf("all-balance") > -1) {
			this.fetchBalance();
	    }

		var master = this.user_roles.filter(obj => { 
	      if(obj.name == 'master' || obj.name == 'vice-master'){
	        return true;
	      }
	    });

	    if(master.length > 0){
	        this.is_master = true;
	    }
	    else{
	    	this.selectedBranch = data.branch_id;
	    	this.loadGroup(); 
	    }

		// var x = this.permissions.filter(obj => { return obj.type === 'Manage Groups'});

	 //    var y = x.filter(obj => { return obj.name === 'Add New Group'});

	 //    if(y.length==0) this.selpermissions.addNewGroup = 0;
	 //    else this.selpermissions.addNewGroup = 1;

	 //    var y = x.filter(obj => { return obj.name === 'Balance'});

	 //    if(y.length==0) this.selpermissions.balance = 0;
	 //    else this.selpermissions.balance = 1;

	 //    var y = x.filter(obj => { return obj.name === 'Collectables'});

	 //    if(y.length==0) this.selpermissions.collectables = 0;
	 //    else this.selpermissions.collectables = 1;

	 //    var y = x.filter(obj => { return obj.name === 'Action'});

	 //    if(y.length==0) this.selpermissions.action = 0;
	 //    else this.selpermissions.action = 1;

		this.__datatable__();
	},

	mounted() {
		//this.__datatable__();

		var vm = this;

		$('.leader_typeahead').typeahead({
            source: function(query, process) {
		      	axiosAPIv1.get('/visa/client/get-visa-clients?q=' + query + '&branch=' + vm.form.branch)
					.then(response => {
						return process(response.data);
					});
		    },
            minLength: 4,
            matcher: function(item) {
                return true;
            },
            updater: function(item) {
            	vm.form.leaderId = item.id;
							//console.log(vm.form.branch);
            	// vm.contacts[0] = item.contact1;
            	// if(item.contact2 != null) { vm.contacts[1] = item.contact2; }
            	// vm.form.contact = vm.contacts[0];

            	// vm.form.address = item.address;
            	// if(item.barangay != null) { vm.form.barangay = item.barangay; }
            	// vm.form.city = item.city;
            	// vm.form.province = item.province;
            	// if(item.zipCode != null) { vm.form.zipCode = item.zipCode; }

            	vm.leader = item.name;
            	return item.name;
            }
        });
		var vm = this;
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        }).on('ifChecked', function(){
		    vm.radioValue = $(this).val();
		    // vm.$refs.exportexcelref.resetExportExcel();
		});

	},
	components: {
		'export-excel': require('../../components/Visa/ExportExcel.vue'),
		'balance-item': require('../../components/Visa/AllBalance.vue'),
		'payment-item': require('../../components/Visa/AllPayment.vue'),
	}

});

$(document).ready(function() {

	jQuery.extend( jQuery.fn.dataTableExt.oSort, {
	    "non-empty-string-asc": function (str1, str2) {
	        if(str1 == '<span style="display:none;">--</span>')
	            return 1;
	        if(str2 == '<span style="display:none;">--</span>')
	            return -1;
	        return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
	    },

	    "non-empty-string-desc": function (str1, str2) {
	        if(str1 == '<span style="display:none;">--</span>')
	            return 1;
	        if(str2 == '<span style="display:none;">--</span>')
	            return -1;
	        return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
	    }
	});

	$(document).on("click",".exportExcel",function() {
		var title = (Groups.translation) ? Groups.translation['export-group-to-excel'] : 'Export Group to Excel?';

		var grid = $(this).attr("group-id");
		Groups.groupId = grid;
		$('.chosen-select-for-date').val('').trigger('chosen:updated');
		Groups.$refs.exportexcelref.resetExportExcel();
		Groups.$refs.exportexcelref.fetchDateList(grid);
		//Groups.$refs.exportexcelref.fetchServiceList(grid);
		// alert(Groups.groupId);
		$('#export-excel-modal').modal('show');
  //     	swal({
		// 	title: title,
  //           type: 'info',
  //           allowOutsideClick: false,
  //           showCloseButton: true,
		// 	html: "" +
	 //            "<br>" +
	 //            '<div class="radio">' +
  // 				'<label><input type="radio" name="optradio" checked value="bymembers">By Members</label>'+
  // 				'<label class="m-l-2"><input type="radio" name="optradio" value="byservices">By Service</label>'+
  // 				'<label class="m-l-2"><input type="radio" name="optradio" value="bybatch">By Batch</label>'+
		// 		'</div>' +
	 //            '<a dl-link="/visa/report/get-group-summary/'+grid+'" class="export-english btn btn-success btn-lg">' + '<img class="dd-selected-image" src="/shopping/img/general/flag/en.jpg"> English' + '</a>' +
	 //            '<a dl-link="/visa/report/get-group-summary-chinese/'+grid+'" class="export-english btn btn-danger btn-lg m-l-xs">' + '<img class="dd-selected-image" src="/shopping/img/general/flag/cn.jpg"> Mandarin' + '</a>' ,
	 //            // '<br><br><label for="startDate">Date :</label>' +
	 //   //          '<br><br>' +
	 //   //          '<div class="ibox float-e-margins">' +
  //   //             '<div class="ibox-title">' +
  //   //             '<h5>Filter Options</h5></div>' +
  //   //             '<div class="ibox-content">' +
	 //   //          '<div class="form-group"><label class="col-lg-2 control-label">Date</label>' +
	 //   //          '<div class="col-lg-10">' +
		// 		// '<input name="startDate" id="startDate" class="date-picker form-control" /></div></div>' + 
		//   //   	'</div>' +
		// 		// '</div>'
		// 		//,
	 //        showCancelButton: false,
	 //        showConfirmButton: false
		// });

    });

	// $(document).on("click", ".export-english", function() {
	//     var type = $("input[name='optradio']:checked").val();
	//     var dlink = $(this).attr('dl-link')+"/"+type;

	//     var preparingMessageHtml = (Groups.translation) 
	//     	? Groups.translation['we-are-preparing-your-report-please-wait'] 
	//     	: 'We are preparing your report, please wait...';

	//     var failMessageHtml = (Groups.translation) 
	//     	? Groups.translation['there-was-a-problem-generating-your-report-please-try-again'] 
	//     	: 'There was a problem generating your report, please try again';

	//     swal.close();
 //        $.fileDownload(dlink, {
 //            preparingMessageHtml: preparingMessageHtml,
 //            failMessageHtml: failMessageHtml
 //        });
 //        return false; //this is critical to stop the click event which will trigger a normal file download!
 //    });

    $(document).on("click",".changeStatus",function() {
		var clid = $(this).attr("client-id");
		var stat = $(this).attr("client-status");
		var ht = '';
		if(stat=='Regular'){
			var label = (Groups.translation) 
	    		? Groups.translation['to-high-risk'] 
	    		: 'to High Risk';

			ht = '<a client-status="High-Risk" client-id="'+clid+'" class="payment-status btn btn-danger btn-lg">' + label + '</a>';
		}
		else{
			var label = (Groups.translation) 
	    		? Groups.translation['to-regular'] 
	    		: 'to Regular';

			ht = '<a client-status="Regular" client-id="'+clid+'" class="payment-status btn btn-success btn-lg">' + label + ' </a>';
		}

		var title = (Groups.translation) 
	    		? Groups.translation['change-group-status'] 
	    		: 'Change Group Status?';

      	swal({
			title: title,
            type: 'info',
            allowOutsideClick: false,
            showCloseButton: true,
			html: "" +"<br>" + ht,
	        showCancelButton: false,
	        showConfirmButton: false
		});
    });

    $(document).on("click", ".payment-status", function() {
	    swal.close();
        var clid = $(this).attr("client-id");
		var stat = $(this).attr("client-status");
		Groups.changeStatus(clid,stat);
    });

});
