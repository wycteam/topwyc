Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

var UserApp = new Vue({
	el:'#access-control',
	data:{
		roles:[],
		permissions:[],
		rolename:'',
		permissiontype:[],
	    form: new Form({
	       roleid:0,
	       selpermissions:[]
	    },{ baseURL: '/visa/access-control'}),
	    roleForm: new Form({
	       id:0,
	       role:''
	    },{ baseURL: '/visa/access-control'})

	},
	methods: {
		showPermissionDialog(role) {
			this.rolename = role.label;
			this.form.roleid = role.id;

			axios.get('/visa/access-control/getSelectedPermission/'+role.id)
	        .then(result => {
	          	this.form.selpermissions = result.data;
	    	});

			axios.get('/visa/access-control/getPermissions')
	        .then(result => {
	          	this.permissions = result.data;
	    	});

	    	axios.get('/visa/access-control/getPermissionType')
	        .then(result => {
	          	this.permissiontype = result.data;
	    	});

			$('#dialogPermissions').modal('toggle');

		},
        toggle(id, e) {
            if(e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },
		selectItem(data,index){
            if(this.check(data)==1) {
				var sp = this.form.selpermissions;
	        	var vi = this;
				$.each(sp, function(i, item) {
				  if(sp[i]!=undefined){
					if(sp[i].permission_id == data.id){
				  		vi.form.selpermissions.splice(i,1);
				  	}
				  }
				});
            } else {
				var d = {"permission_id": data.id, "role_id": this.form.roleid};
                this.form.selpermissions.push(d);
            }
        },
        check(id){
        	var a = 0;
        	var sp = this.form.selpermissions;
			$.each(sp, function(i, item) {
			  if(sp[i].permission_id == id.id){
			  	a = 1;
			  }
			});
			return a;
        },
		saveAccessControl(){
			if(this.form.selpermissions.length != 0) {
				this.form.submit('post', '/updateAccessControl')
		          .then(result => {
		          	$('input:checkbox').prop('checked',false);
		          	this.form.selpermissions = [];
		          	this.reset();
		          	$('#dialogPermissions').modal('toggle');
		          	toastr.success('Successfully saved.');
		          });
	        } else 
	        	toastr.error('Please select at least one permission.');
		},
		reset(){
			$('#form')[0].reset();
		},
		showRoleDialog(){
			$('#dialogRole').modal('toggle');
		},
		addRole(){
			this.roleForm.submit('post', '/addRole')
	          .then(result => {
	          	toastr.success('Successfully saved.');
	          	this.roleForm.role = '';
	          	$('#dialogRole').modal('toggle');
	          	this.getRoles();

	          });
		},
		showEditRoleDialog(role){
			axios.get('/visa/access-control/getRole/'+role.id)
	        .then(result => {
	        	this.roleForm.id = result.data[0].id;
	          	this.roleForm.role = result.data[0].label;
	          	$('#dialogEditRole').modal('toggle');
	    	});

		},
		getRoles(){
			axios.get('/visa/access-control/getRoles')
	        .then(result => {
	          	this.roles = result.data;
	    	});
		},
		saveRole(){
			this.roleForm.submit('post', '/saveRole')
	          .then(result => {
	          	toastr.success('Successfully saved.');
	          	this.roleForm.id = 0;
	          	this.roleForm.role = '';
	          	$('#dialogEditRole').modal('toggle');
	          	this.getRoles();
	          });
		}
	},
	created(){
		this.getRoles();
	},
	mounted(){
		$('#dialogPermissions').on("hidden.bs.modal", this.reset);

        $('body').popover({ // Ok
            html:true,
            trigger: 'hover',
            selector: '[data-toggle="popover"]'
        });
	}
});
