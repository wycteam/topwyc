import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)
Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

let data = window.Laravel.data;

var Accounts = new Vue({
	el:'#editAccount',
	data:{
		groups:[],
		permissions: [],
		selpermissions: data.permissions,
		permissiontype:[],
		getpermissions:[],
		scheduleTypes: [],
		roles: data.roles,
		contribution:[{_sss_es:0,_sss:0,_pag_ibig_es:0,_pag_ibig:0,_phil_health_es:0,_phil_health:0,_day_from:'MONDAY',_day_to:'FRIDAY',_employment_date:''}],
		_day_from:'',
		_day_to:'',
		loan_type:"ONE TIME",
		payment_terms:1,
	    accountForm:new Form({
	      id: data.id,
	      avatar: data.avatar,
	      first_name: data.first_name,
	      middle_name: data.middle_name,
	      last_name: data.last_name,
	      birth_date: data.birth_date,
	      gender: data.gender,
	      civil_status: data.civil_status,
	      height: data.height,
	      weight: data.weight,
	      address: data.address,
	      contact_number: data.contact_number,
	      alternate_contact_number: data.alternate_contact_number,
	      email: data.email,
	      scheduleType: (data.schedule) ? data.schedule.schedule_type.id : '',
	      timeIn: (data.schedule && data.schedule.schedule_type.id == 1) 
	      	? moment(data.schedule.time_in, 'HH:mm:ss').format("h:mm A") : '',
	      timeOut: (data.schedule && data.schedule.schedule_type.id == 1) 
	      	? moment(data.schedule.time_out, 'HH:mm:ss').format("h:mm A") : '',
	      timeInFrom: (data.schedule && data.schedule.schedule_type.id == 2) 
	      	? moment(data.schedule.time_in_from, 'HH:mm:ss').format("h:mm A") : '',
		  timeInTo: (data.schedule && data.schedule.schedule_type.id == 2) 
		  	? moment(data.schedule.time_in_to, 'HH:mm:ss').format("h:mm A") : ''
	    }, { baseURL: 'http://'+window.Laravel.cpanel_url })
	},

	methods: {
		getContribution(){
			axios.get('/visa/payroll/get-contribution',{
				params: {
						id: data.id
				}
			}).then(response => {
					this.contribution = response.data;
					this.contribution[0]._day_from = this.contribution[0]._day_from=='' ? 'MONDAY' : this.contribution[0]._day_from;
					this.contribution[0]._day_to = this.contribution[0]._day_to=='' ? 'FRIDAY' : this.contribution[0]._day_to;
			})
		},
		updateContribution(e,status){
			axios.put('/visa/payroll/control/'+data.id, {
				method:status,
				value:e.target.value,
			}, function (data) {
					   //console.log(data)
			 }).catch(function (error) {
			   console.log(error);
			 }).then(function () {
				 
			 });
		},
		getScheduleTypes() {
			axiosAPIv1.get('/schedule-types')
			  	.then((response) => {
				    this.scheduleTypes = response.data;
				});
		},
		initTimePicker() {
			setTimeout(() => {
				$('.timepicker-input').timepicker();
			}, 500);

			$('.timepicker-input').on('click', function(e) {
				let target = e.target;

				$('#' + target.id).timepicker('showWidget');

				if(target.value == '') {
					$('#' + target.id).timepicker('setTime', '12:00 AM');
				}
			});

			$('.timepicker-input').timepicker().on('changeTime.timepicker', (e) => {
				let id = e.target.id;

				if(id == 'timepicker1') {
					this.accountForm.timeIn = e.time.value;
				} else if(id == 'timepicker2') {
					this.accountForm.timeOut = e.time.value;
				} else if(id == 'timepicker3') {
					this.accountForm.timeInFrom = e.time.value;
				} else if(id == 'timepicker4') {
					this.accountForm.timeInTo = e.time.value;
				}
			});
		},
		showPermissions(){
			axios.get('/visa/access-control/getPermissions')
	        .then(result => {
	          	this.getpermissions = result.data;
	    	});

	    	axios.get('/visa/access-control/getPermissionType')
	        .then(result => {
	          	this.permissiontype = result.data;
	    	});

			$('#dialogPermissions').modal('toggle');			
		},
        toggle(id, e) {
            if(e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },

		selectItem(data){
            if(this.check(data)==1) {
				var sp = this.selpermissions;
	        	var vi = this;
				$.each(sp, function(i, item) {
				  if(sp[i]!=undefined){
					if(sp[i].id == data.id){
				  		vi.selpermissions.splice(i,1);
				  	}
				  }
				});
            } else {
                this.selpermissions.push(data);
            }

        	var p = this.selpermissions;
        	var sp = [];
			$.each(p, function(i, item) {
				sp.push(p[i].id);
			});
			$('#permissions').val(sp).trigger('chosen:updated');

        },
        check(id){
        	var a = 0;
        	var sp = this.selpermissions;
			$.each(sp, function(i, item) {
			  if(sp[i].id == id.id){
			  	a = 1;
			  }
			});
			return a;
        },
        selectRole(role){
        	// console.log(role);
        	if(role.join(',')!=""){
	 			axios.get('/visa/access-control/getSelectedPermissions/'+role.join(','))
		        .then(result => {
		          	
		          	var ids = result.data;
					var uniqueList=ids.split(',').filter(function(allItems,i,a){
					    return i==a.indexOf(allItems);
					}).join(',');
		        	
		        	if(ids!=''){
						axios.get('/visa/access-control/getSelectedPermissions2/'+uniqueList)
					 	  .then(result => {
				            this.permissions = result.data;
				        });
		        	} else {
						this.permissions = [];
		        	}

				setTimeout(() => {
					$('#permissions').chosen({
						width: "100%",
						no_results_text: "No result/s found.",
						search_contains: true
					});

					$("#permissions").trigger("chosen:updated");
				}, 1000);

			    });
        	}
      	
        },
 		initChosen(){

        	var r = data.roles;
        	var sr = [];
			$.each(r, function(i, item) {
				sr.push(r[i].id);
			});

 			axios.get('/visa/access-control/getSelectedPermissions/'+sr.join(','))
	        .then(result => {
	          	
	          	var ids = result.data;
				var uniqueList=ids.split(',').filter(function(allItems,i,a){
				    return i==a.indexOf(allItems);
				}).join(',');
	        	
	        	if(ids!=''){
					axios.get('/visa/access-control/getSelectedPermissions2/'+uniqueList)
				 	  .then(result => {
			            this.permissions = result.data;

			        	var p = data.permissions;
			        	var sp = [];
						$.each(p, function(i, item) {
							sp.push(p[i].id);
						});

						setTimeout(() => {
				        	$('#permissions').chosen('destroy');
				        	$('#permissions').chosen({
								width: "100%",
								no_results_text: "No result/s found.",
								search_contains: true
							});

				        	$('#permissions').val(sp).trigger('chosen:updated');
			        		$('#permissions').trigger('chosen:updated');
			        	}, 1000);

			        });

	        	} else {
					this.permissions = [];

		        	var p = data.permissions;
		        	var sp = [];
					$.each(p, function(i, item) {
						sp.push(p[i].id);
					});

					setTimeout(() => {
			        	$('#permissions').chosen('destroy');
			        	$('#permissions').chosen({
							width: "100%",
							no_results_text: "No result/s found.",
							search_contains: true
						});

			        	$('#permissions').val(sp).trigger('chosen:updated');
		        		$('#permissions').trigger('chosen:updated');
		        	}, 1000);

	        	}


		    });			
		}       
	},

	directives: {
		datepicker: {
		  bind(el, binding, vnode) {
		    $(el).datepicker({
		      format: 'yyyy-mm-dd'
		    }).on('changeDate', (e) => {
		      accountForm.$set(accountForm, 'birth_date', e.format('yyyy-mm-dd'));
		    });
		  }
		}
	},
	created(){

       Event.listen('Imgfileupload.avatarUpload', (img) => {
                this.accountForm.avatar = img;
       });
	   this.getContribution();
       this.initChosen();	

       this.getScheduleTypes();

	},

	mounted() {
		this.initTimePicker();
	}
});

$(document).ready(function() {

    $('.btnUpload').click(function(){
        $('.avatar-uploader input').click();
    })

	$('#first_name, #middle_name, #last_name').keypress(function(event){
		var inputValue = event.charCode;
		//alert(inputValue);
		if(!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123)||(inputValue==32) || (inputValue==0))){
		event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
	 //if the letter is not digit then display error and don't type anything
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        return false;
	    }
	});

	$('#roles').on('change', function() {
	    var role = $(this).val();
	    Accounts.selectRole(role);
	});

} );