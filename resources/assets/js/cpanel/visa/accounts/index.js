Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

var UserApp = new Vue({
	el:'#userlist',
	data:{
		users:[],
		seluser : '',
		permissions: [],
		permissiontype:[],
		getpermissions:[],
		getrolepermissions:[]
	},
	methods: {
		editSchedule(user) {
			this.$refs.editschedule.setSelectedUser(user);

			$('#editSchedule').modal('show');
		},
		getUsers() {
			function getUsers() {
				return axiosAPIv1.get('/users');
			}
	    	axios.all([
				getUsers()
			]).then(axios.spread(
				(
					 users,
				) => {
				this.users = users.data;
			}))
			.catch($.noop);
		},
		parseMoment(time) {
			return moment(time, 'HH:mm:ss').format("h:mm A");
		},
		showRemoveDialog(user) {
			this.seluser = user;

			$('#dialogRemove').modal('toggle');
		},
		downloadQR() {
			axios.get('/visa/access-control/downloadQR')
	        .then(result => {
	          	//console.log(result);
	    	});
		},
		showPermissions(){
			axios.get('/visa/access-control/getPermissions')
	        .then(result => {
	          	this.getpermissions = result.data;
	    	});

	    	axios.get('/visa/access-control/getPermissionType')
	        .then(result => {
	          	this.permissiontype = result.data;
	    	});

			$('#dialogPermissions').modal('toggle');			
		},
        toggle(id, e) {
            if(e.target.nodeName != 'BUTTON' && e.target.nodeName != 'SPAN') {
                $('#fa-' + id).toggleClass('fa-arrow-down');
            }
        },
		selectItem(data){
            if(this.check(data)==1) {
				var sp = this.permissions;
	        	var vi = this;
				$.each(sp, function(i, item) {
				  if(sp[i]!=undefined){
					if(sp[i].permission_id == data.id){
				  		vi.permissions.splice(i,1);
				  	}
				  }
				});
            } else {
            	// var d = {"permission_id": data.id,"permissions":{
            	// 	"type": data.type,
            	// 	"label": data.label
            	// } };
                this.permissions.push(data);
            }
            this.initChosen();
        },
        check(id){
        	var a = 0;
        	var sp = this.permissions;
			$.each(sp, function(i, item) {
			  if(sp[i].permission_id == id.id){
			  	a = 1;
			  }
			});
			return a;
        },
        selectRole(role){
        	if(role.join(',')!=""){
	 			axios.get('/visa/access-control/getSelectedPermissions/'+role.join(','))
		        .then(result => {
		          	
		          	var ids = result.data;
					var uniqueList=ids.split(',').filter(function(allItems,i,a){
					    return i==a.indexOf(allItems);
					}).join(',');
		        	
		        	if(ids!=''){
						axios.get('/visa/access-control/getSelectedPermissions2/'+uniqueList)
					 	  .then(result => {
				            this.permissions = result.data;
				            this.initChosen();
				        });
		        	} else {
						this.permissions = [];
						this.initChosen();
		        	}
		    	});
	    	}  	
        },
		deleteUser() {
			axiosAPIv1.delete('/users/'+this.seluser.id)
				.then(response => {
					toastr.success('User Deleted!');

					var self = this;
					axiosAPIv1.get('/users')
						.then(response => {
							self.users = response.data;
						});
				});
		},
		initChosen(){
			setTimeout(() => {
				$('#permissions').chosen({
					width: "100%",
					no_results_text: "No result/s found.",
					search_contains: true
				});

				$("#permissions").trigger("chosen:updated");
			}, 1000);			
		}
	},
	created(){
		this.getUsers();
	},
	mounted() {
		setTimeout(() => {
			let roles = $('#roles').val();

			this.selectRole(roles);
		}, 1000);
	},
	updated() {
		$('#lists').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"top"lf>rt<"bottom"ip><"clear">'

        });
	},
	components: {
		'edit-schedule': require('../../employee/accounts/components/edit-schedule.vue')
	}
});


$(document).ready(function() {

	$('#first_name, #middle_name, #last_name').keypress(function(event){
		var inputValue = event.charCode;
		//alert(inputValue);
		if(!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123)||(inputValue==32) || (inputValue==0))){
		event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
	 //if the letter is not digit then display error and don't type anything
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        return false;
	    }
	});

	$('#roles').on('change', function() {
	    var role = $(this).val();
	    UserApp.selectRole(role);
	});

} );
