import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)
Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

let data = window.Laravel.user;

var dashboard = new Vue({
	el:'#client-services',
	data:{
    translation: null,
		onprocessServices:[],
		service:'',
        setting: data.access_control[0].setting,
        permissions: data.permissions,
        form:new Form({
          id: '',
          reason: '',
        }, { baseURL: 'http://'+window.Laravel.cpanel_url }),
        addSchedule:new Form({
          id:'',
          item:'',
          rider_id:'',
          location:'',
          scheduled_date:'',
          scheduled_time:'',
        }, { baseURL: '/visa/home/'}),
        scheduled_hour:'',
        scheduled_minute:'',
        companyCouriers:[],
        dt:'',
        selId:'',
        mode:''
	},
	methods: {
        getTranslation() {
            if(window.Laravel.language == 'cn'){
                axios.get('/visa/home/translation')
                   .then(response => {
                       this.translation = response.data.translation;
                   });
            }
        },

        callDataTables2(){
        	$('#onprocessLists').DataTable().destroy();

	      	axios.get('/visa/home/onprocessServices')
	         .then(result => {
	        	 this.onprocessServices = result.data;

	            setTimeout(function(){
	                $('#onprocessLists').DataTable({
	                    responsive: true,
	                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
	                    "iDisplayLength": 10
	                });
				    $(function () {
				      $('[data-toggle="tooltip"]').tooltip()
				    })
	            },2000);
	    	  });
         },


        totalCost(cost,tip) {
            return (parseInt(cost) + parseInt(tip));
        },

        ifToday(date) {
            let today = moment().format('YYYY-MM-DD');
            if(today == date){
                return 'Due Today';
            }
            return 'Due on '+date;
        },


      openProcessService(service){
        	this.service = service;
        	$('#dialogRemove').modal('toggle');
        },
        removeService(){
			  axios.get('/visa/home/removeService/'+this.service.id)
	        .then(result => {
	          this.callDataTables2();
	          toastr.success( (this.translation) ? this.translation['service-successfully-removed'] : 'Service successfully removed.');
	    	});
        },
        openPendingService(service){
        	this.service = service;
        	this.form.id = this.service.id;
        	$('#dialogReason').modal('toggle');
        },
        markAsPending(){
            this.form.submit('post', '/visa/home/markAsPending')
            .then(result => {
            	this.callDataTables1();
            	this.callDataTables2();
                toastr.success( (this.translation) ? this.translation['reason-successfully-saved'] : 'Reason successfully saved.');
                $('#dialogReason').modal('toggle');
                this.service = '';
                this.form.id = '';
                this.form.reason = '';
            });
        },
        setService(service) {
        	let { id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs, docs_needed, docs_optional, detail, parent_id} = service;

        	if(service.discount2.length == 0) {
        		discount = '';
        		reason = '';
        	} else {
        		discount = service.discount2[0].discount_amount;
        		reason = service.discount2[0].reason;
        	}
					//console.log(parent_id);
        	this.$refs.editservicedashboardref.setService(id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs, detail, parent_id);
          this.$refs.editservicedashboardref.fetchDocs(docs_needed, docs_optional);
        },


        showScheduleModal(val, id){
            if(val == "add"){
                $('#addScheduleModal').modal('toggle');
                this.mode = "Add";
            }else if(val == "edit"){
                axios.post('/visa/home/getSched/'+ id)
                .then(result => {
                    this.addSchedule = new Form ({
                        id              :result.data.id,
                        item            :result.data.item,
                        rider_id        :result.data.rider_id,
                        location        :result.data.location,
                        scheduled_date  :result.data.scheduled_date,
                        scheduled_time  :result.data.scheduled_time
                    },{ baseURL: '/visa/home/'});
                    this.dt = result.data.scheduled_date;
                    $('form#frm_deliverysched .input[name=date]').val(result.data.scheduled_date);
                    this.mode = "Edit";
                });
                $('#addScheduleModal').modal('toggle');
            }
        },

        showDeleteSchedPrompt(val){
            this.selId = val;
            $('#deleteSchedPrompt').modal('toggle');
        },

        loadCompanyCouriers(){
            axios.get('/visa/home/showCouriers')
            .then(response => {
                this.companyCouriers = response.data;
            });
        },

        resetSchedFrm(){
            $('form#frm_deliverysched input[name=date]').val();
            this.addSchedule = new Form ({
                item: '',
                rider_id: '',
                location:'',
                scheduled_date:'',
                scheduled_time:''
            },{ baseURL: '/visa/home/'});
        },

        getDate(){
            //console.log($('form#frm_deliverysched .input[name=date]').val());
            this.dt = $('form#frm_deliverysched .input[name=date]').val();
        },

        saveDeliverSchedule(){
            var m = this.mode;
            var combineTime = this.$refs.delivery.alwaysTwoDigit(this.scheduled_hour)+":"+this.$refs.delivery.alwaysTwoDigit(this.scheduled_minute)+":00";
            this.addSchedule.scheduled_time = combineTime;
            this.addSchedule.scheduled_date = this.$refs.delivery.formatDateMDY(this.addSchedule.scheduled_date);

            if(m == "Add"){
                // if(this.dt == ''){
                //     this.dt = $('form#frm_deliverysched .input[name=date]').val();
                // }
                // this.addSchedule.scheduled_date = this.dt;
                this.addSchedule.submit('post'  , '/save-delivery-schedule')
                    .then(result => {
                    $('#addScheduleModal').modal('toggle');
                    toastr.success( (this.translation) ? this.translation['schedule-added'] : 'Schedule Added.');
                    this.resetSchedFrm();
                })
                .catch(error => {
                    toastr.error( (this.translation) ? this.translation['failed-all-fields-are-required'] : 'Failed. All Fields are required.');
                });
                this.resetSchedFrm();
                //sthis.callDataTables6();
            }else if (m == "Edit"){
                // this.addSchedule.scheduled_date = this.dt;
                this.addSchedule.submit('post'  , '/editSched')
                    .then(result => {
                    $('#addScheduleModal').modal('toggle');
                    toastr.success( (this.translation) ? this.translation['schedule-successfully-updated'] : 'Schedule successfully updated.');
                    this.resetSchedFrm();
                })
                .catch(error => {
                    toastr.error( (this.translation) ? this.translation['failed-all-fields-are-required'] : 'Failed. All Fields are required.');
                });
                this.resetSchedFrm();
                //this.callDataTables6();
            }
            this.$refs.delivery.dateFilter = this.addSchedule.scheduled_date;
            this.$refs.delivery.filterSched('form');
        },

        deleteSchedule(id){
            axios.post('/visa/home/delSched/'+id)
            .then(result => {
                $('#deleteSchedPrompt').modal('toggle');
                toastr.success( (this.translation) ? this.translation['successfully-deleted'] : 'Successfully Deleted');
                this.$refs.delivery.filterSched('today');
                })
            .catch(error => {
                $('#deleteSchedPrompt').modal('toggle');
                toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete Failed');
            });

        },

	},
	created(){
        this.getTranslation();
		    this.callDataTables2();

	},
	components: {

	},
    mounted() {
        $('body').popover({ // Ok
            html:true,
            trigger: 'hover',
            selector: '[data-toggle="popover"]'
        });
    }
});

$( document ).ready(function() {

    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });
});
