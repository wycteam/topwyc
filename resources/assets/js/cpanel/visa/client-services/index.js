import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)
Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

let data = window.Laravel.user;

var dashboard = new Vue({
	el:'#today-services',
	data:{
    translation: null,
		onprocessServices:[],
		todaysReports:[],
        lastReportedDate:[],
		todaysServices:[],
        deliverySchedules:[],
		service:'',
        setting: data.access_control[0].setting,
        permissions: data.permissions,
        form:new Form({
          id: '',
          reason: '',
        }, { baseURL: 'http://'+window.Laravel.cpanel_url }),
        addSchedule:new Form({
          id:'',
          item:'',
          rider_id:'',
          location:'',
          scheduled_date:'',
          scheduled_time:'',
        }, { baseURL: '/visa/home/'}),
        scheduled_hour:'',
        scheduled_minute:'',
        companyCouriers:[],
        dt:'',
        selId:'',
        mode:''
	},
	methods: {
        getTranslation() {
            if(window.Laravel.language == 'cn'){
                axios.get('/visa/home/translation')
                   .then(response => {
                       this.translation = response.data.translation;
                   });
            }
        },

        callClientServices(){
        	$('#onprocessLists').DataTable().destroy();

	     	axios.get('/visa/home/onprocessServices')
	        .then(result => {
	        	this.onprocessServices = result.data;

	          //  setTimeout(function(){
	                $('#onprocessLists').DataTable({
	                    responsive: true,
	                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
	                    "iDisplayLength": 10
	                });
				    $(function () {
				      $('[data-toggle="tooltip"]').tooltip()
				    })
	          //  },2000);
	    	});
        },


        callDataTables5(){
            $('#todaysServicesLists').DataTable().destroy();
            var params = {
                date: 0
            };
            axios.get('/visa/client-services/get-service-by-date', {
                params: params
            })
            .then(response => {
                this.todaysServices = response.data;
              //  setTimeout(function(){
                    $('#todaysServicesLists').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                      $('[data-toggle="tooltip"]').tooltip()
                    })
              //  },2000);
            });
        },



        totalCost(cost,tip) {
            return (parseInt(cost) + parseInt(tip));
        },

        ifToday(date) {
            let today = moment().format('YYYY-MM-DD');
            if(today == date){
                return 'Due Today';
            }
            // return moment(this.reminder.extend).fromNow();
            return 'Due on '+date;

        },


        createDatatableToggle(reports,tablename) {

            $(tablename).DataTable().destroy();

            setTimeout(function() {

                var format = function(d) {
                    // console.log(d);
                    var ids = '';
                    var clids = [];
                    for(var i=0; i<(d.client).length; i++) {
                        var found = jQuery.inArray(d.client[i].client_id, clids);
                         // console.log(found);
                        if (found < 0) {
                            // Element was not found, add it.
                            clids.push(d.client[i].client_id);
                            ids += '<a href="/visa/client/'+d.client[i].client_id+'" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="'+d.client[i].fullname +'" data-trigger="hover">'+d.client[i].client_id +'</a>'+",";
                        }
                        // console.log(clids);
                        // if(i != ((d.client).length - 1) ){
                        //     ids = ids+",";
                        // }

                    }
                    // ids = rtrim(ids,',');
                    return '<table cellpadding="5" cellspacing="0" class="table table-striped table-hover dtchildrow" >'+
                                '<tr style="padding-left:50px;">'+
                                    '<td><b>Report : </b>'+d.detail+'</td>'+
                                    // '<td><b>Client IDs : </b> '+ids+'</td>'+
                                '</tr>'+
                            '</table>';
                };

                var table2 = $(tablename).DataTable( {
                    "data": reports,
                    responsive: true,
                    "columns": [
                        // {
                        //     "width": "5%",
                        //     "className":      'service-control text-center',
                        //     "orderable":      false,
                        //     "data":           null,
                        //     "defaultContent": '<i class="fa fa-arrow-right" style="cursor:pointer;"></i>'
                        // },
                        {
                            "width": "40%",
                            "className":      'service-control',
                            data: null, render: function(reports, type, row) {
                                if(reports.type == "report"){
                                    return '<b style="color: #1ab394;">' + reports.service + '</b>'
                                }
                                return '<b style="color: black;">' + reports.service + '</b>'
                            }
                        },
                        {
                            "className":      'service-control',
                            "width": "13%",
                            data: null, render: function(reports, type, row) {
                                var sid = '';
                                for(var i=0; i<(reports.client).length; i++) {
                                    sid += reports.client[i].client_id+",";
                                }
                                return '<b>'+reports.log_date+ '</b><span style="display:none;">'+sid+'</span>'
                            }
                        },
                        {
                            "className":      'service-control',
                            "width": "12%",
                            data: null, render: function(reports, type, row, meta) {
                                return '<b>'+reports.processor+ '</b>'
                            }
                        },
                        {
                            "width": "35%",
                            "className":      'service-control wrapok',
                            // "class": "wrapnot",
                            data: null, render: function(reports, type, row, meta) {
                                var ids = '';
                                var clids = [];
                                for(var i=0; i<(reports.client).length; i++) {
                                    var found = jQuery.inArray(reports.client[i].client_id, clids);
                                     // console.log(found);
                                    if (found < 0) {
                                        // Element was not found, add it.
                                        clids.push(reports.client[i].client_id);
                                        ids += '<a href="/visa/client/'+reports.client[i].client_id+'" target="_blank" data-toggle="popover" data-placement="top" data-container="body" data-content="'+reports.client[i].client_id +'" data-trigger="hover">'+reports.client[i].fullname +'</a>'+" , ";
                                    }
                                    // console.log(clids);
                                    // if(i != ((d.client).length - 1) ){
                                    //     ids = ids+",";
                                    // }

                                }
                                return ids;
                            }
                        },
                    ]
                });
                $('body').off('click', 'table'+tablename+' tbody td.service-control');
                $('body').on('click', 'table'+tablename+' tbody td.service-control', function (e) {
                    var tr2 = $(this).closest('tr');
                    var row = table2.row( tr2 );

                    if(row.child.isShown()) {
                        row.child.hide();
                        tr2.removeClass('shown');
                    } else {
                        row.child( format(row.data()) ).show();
                        tr2.addClass('shown');
                    }
                });
                //EXPAND ALL CHILD ROWS BY DEFAULT
                table2.rows().every(function(){
                    // If row has details collapsed
                    if(!this.child.isShown()){
                        // Open this row
                        this.child(format(this.data())).show();
                        $(this.node()).addClass('shown');
                    }
                });


                // Handle click on "Expand All" button
                $('#btn-show-all-children').on('click', function(){
                    // Enumerate all rows
                    table2.rows().every(function(){
                        // If row has details collapsed
                        if(!this.child.isShown()){
                            // Open this row
                            this.child(format(this.data())).show();
                            $(this.node()).addClass('shown');
                        }
                    });
                });

                // Handle click on "Collapse All" button
                $('#btn-hide-all-children').on('click', function(){
                    // Enumerate all rows
                    table2.rows().every(function(){
                        // If row has details expanded
                        if(this.child.isShown()){
                            // Collapse row details
                            this.child.hide();
                            $(this.node()).removeClass('shown');
                        }
                    });
                });

            }.bind(this), 2000);

        },


        openProcessService(service){
        	this.service = service;
        	$('#dialogRemove').modal('toggle');
        },
        removeService(){
		   	axios.get('/visa/home/removeService/'+this.service.id)
	        .then(result => {
	          this.callClientServices();
	          toastr.success( (this.translation) ? this.translation['service-successfully-removed'] : 'Service successfully removed.');
	    	});
        },
        openPendingService(service){
        	this.service = service;
        	this.form.id = this.service.id;
        	$('#dialogReason').modal('toggle');
        },
        markAsPending(){
            this.form.submit('post', '/visa/home/markAsPending')
            .then(result => {
            	this.callClientServices();
                toastr.success( (this.translation) ? this.translation['reason-successfully-saved'] : 'Reason successfully saved.');
                $('#dialogReason').modal('toggle');
                this.service = '';
                this.form.id = '';
                this.form.reason = '';
            });
        },
        setService(service) {
        	let { id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs, docs_needed, docs_optional, detail, parent_id} = service;

        	if(service.discount2.length == 0) {
        		discount = '';
        		reason = '';
        	} else {
        		discount = service.discount2[0].discount_amount;
        		reason = service.discount2[0].reason;
        	}
					//console.log(parent_id);
        	this.$refs.editservicedashboardref.setService(id, tracking, tip, status, cost, discount, reason, active, extend, rcv_docs, detail, parent_id);
          this.$refs.editservicedashboardref.fetchDocs(docs_needed, docs_optional);
        },
        submitFilter(){
            $('#todaysServicesLists').DataTable().destroy();
            params = {
                date: $('form#date-range-form input[name=start]').val()
            };
            axios.get('/visa/client-services/get-service-by-date', {
                params: params
            })
            .then(response => {
                this.todaysServices = response.data;
              //  setTimeout(function(){
                    $('#todaysServicesLists').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                      $('[data-toggle="tooltip"]').tooltip()
                    })
              //  },2000);

            });

        },

        submitFilter2(val){
            $('#todaysServicesLists').DataTable().destroy();
            if(val == 1){
                $('#yesterday-serv').removeClass('btn-outline');
                $('#today-serv').addClass('btn-outline');
            }
            else{
                $('#today-serv').removeClass('btn-outline');
                $('#yesterday-serv').addClass('btn-outline');
            }
            params = {
                date: val
            };
            axios.get('/visa/client-services/get-service-by-date', {
                params: params
            })
            .then(response => {
                this.todaysServices = response.data;
              //  setTimeout(function(){

                    $('#todaysServicesLists').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                      $('[data-toggle="tooltip"]').tooltip()
                    })

              //  },2000);

            });

        },

        submitFilter3(val){
            $('#deliverySchedule').DataTable().destroy();
            if(val == 1){
                $('#tom-sched').removeClass('btn-outline');
                $('#today-sched').addClass('btn-outline');
            }
            else if(val == 0){
                $('#today-sched').removeClass('btn-outline');
                $('#tom-sched').addClass('btn-outline');
            }
            else{
                val = $('form#date-range-form2 input[name=start2]').val();
            }
            params = {
                date: val
            };
            axios.get('/visa/home/getSchedulesbyDate', {
                params: params
            })
            .then(response => {
                this.deliverySchedules = response.data;
              //  setTimeout(function(){
                    $('#deliverySchedule').DataTable({
                        responsive: true,
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "iDisplayLength": 10
                    });
                    $(function () {
                      $('[data-toggle="tooltip"]').tooltip()
                    })
              //  },2000);

            });
        },

        showScheduleModal(val, id){
            if(val == "add"){
                $('#addScheduleModal').modal('toggle');
                this.mode = "Add";
            }else if(val == "edit"){
                axios.post('/visa/home/getSched/'+ id)
                .then(result => {
                    this.addSchedule = new Form ({
                        id              :result.data.id,
                        item            :result.data.item,
                        rider_id        :result.data.rider_id,
                        location        :result.data.location,
                        scheduled_date  :result.data.scheduled_date,
                        scheduled_time  :result.data.scheduled_time
                    },{ baseURL: '/visa/home/'});
                    this.dt = result.data.scheduled_date;
                    $('form#frm_deliverysched .input[name=date]').val(result.data.scheduled_date);
                    this.mode = "Edit";
                });
                $('#addScheduleModal').modal('toggle');
            }
        },

        showDeleteSchedPrompt(val){
            this.selId = val;
            $('#deleteSchedPrompt').modal('toggle');
        },

        loadCompanyCouriers(){
            axios.get('/visa/home/showCouriers')
            .then(response => {
                this.companyCouriers = response.data;
            });
        },

        resetSchedFrm(){
            $('form#frm_deliverysched input[name=date]').val();
            this.addSchedule = new Form ({
                item: '',
                rider_id: '',
                location:'',
                scheduled_date:'',
                scheduled_time:''
            },{ baseURL: '/visa/home/'});
        },

        getDate(){
            //console.log($('form#frm_deliverysched .input[name=date]').val());
            this.dt = $('form#frm_deliverysched .input[name=date]').val();
        },

        saveDeliverSchedule(){
            var m = this.mode;
            var combineTime = this.$refs.delivery.alwaysTwoDigit(this.scheduled_hour)+":"+this.$refs.delivery.alwaysTwoDigit(this.scheduled_minute)+":00";
            this.addSchedule.scheduled_time = combineTime;
            this.addSchedule.scheduled_date = this.$refs.delivery.formatDateMDY(this.addSchedule.scheduled_date);

            if(m == "Add"){
                // if(this.dt == ''){
                //     this.dt = $('form#frm_deliverysched .input[name=date]').val();
                // }
                // this.addSchedule.scheduled_date = this.dt;
                this.addSchedule.submit('post'  , '/save-delivery-schedule')
                    .then(result => {
                    $('#addScheduleModal').modal('toggle');
                    toastr.success( (this.translation) ? this.translation['schedule-added'] : 'Schedule Added.');
                    this.resetSchedFrm();
                })
                .catch(error => {
                    toastr.error( (this.translation) ? this.translation['failed-all-fields-are-required'] : 'Failed. All Fields are required.');
                });
                this.resetSchedFrm();
                //sthis.callDataTables6();
            }else if (m == "Edit"){
                // this.addSchedule.scheduled_date = this.dt;
                this.addSchedule.submit('post'  , '/editSched')
                    .then(result => {
                    $('#addScheduleModal').modal('toggle');
                    toastr.success( (this.translation) ? this.translation['schedule-successfully-updated'] : 'Schedule successfully updated.');
                    this.resetSchedFrm();
                })
                .catch(error => {
                    toastr.error( (this.translation) ? this.translation['failed-all-fields-are-required'] : 'Failed. All Fields are required.');
                });
                this.resetSchedFrm();
                //this.callDataTables6();
            }
            this.$refs.delivery.dateFilter = this.addSchedule.scheduled_date;
            this.$refs.delivery.filterSched('form');
        },

        deleteSchedule(id){
            axios.post('/visa/home/delSched/'+id)
            .then(result => {
                $('#deleteSchedPrompt').modal('toggle');
                toastr.success( (this.translation) ? this.translation['successfully-deleted'] : 'Successfully Deleted');
                this.$refs.delivery.filterSched('today');
                })
            .catch(error => {
                $('#deleteSchedPrompt').modal('toggle');
                toastr.error( (this.translation) ? this.translation['delete-failed'] : 'Delete Failed');
            });

        },

	},
	created(){
        this.getTranslation();
		    this.callDataTables5();

	},
	components: {
		    'edit-service-dashboard': require('../../components/Visa/EditServiceDashboard.vue'),
        'todays-tasks': require('../components/TodaysTasks.vue'),
        'todays-services': require('../components/TodaysServices.vue'),
        'delivery-schedule': require('../components/DeliverySchedule.vue'),
        'edit-who': require('../components/EditWho.vue'),
	},
    mounted() {
        $('body').popover({ // Ok
            html:true,
            trigger: 'hover',
            selector: '[data-toggle="popover"]'
        });

        $(document).on('focus', 'form#date-range-form .input-daterange', function(){
            $(this).datepicker({
                format:'mm/dd/yyyy',
                todayHighlight: true
            });
        });

        $(document).on('focus', 'form#date-range-form2 .input-daterange', function(){
            $(this).datepicker({
                format:'mm/dd/yyyy',
                todayHighlight: true
            });
        });

        $(document).on('focus', 'form#frm_deliverysched .input-daterange', function(){
            $(this).datepicker({
                format:'mm/dd/yyyy',
                todayHighlight: true,
            });
        });

    }
});

$( document ).ready(function() {
    $('[data-toggle=popover]').popover();

    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });
});
