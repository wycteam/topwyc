window.moment = require('moment');

window.$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': window.Laravel.csrfToken
    }
});

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');
window.VeeValidate = require('vee-validate');
window.VueRouter = require('vue-router').default;

window.Vue.use(VeeValidate);
window.Vue.use(VueRouter);

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + window.Laravel.accessToken;
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.axiosAPIv1 = axios.create({
	baseURL: 'http://' + window.Laravel.base_api_url + '/v1'
});

window.axiosMainSite = axios.create({
    baseURL: 'http://' + window.Laravel.base_url 
});

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from "laravel-echo";

if (window.Laravel.isAuthenticated) {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: window.location.hostname + ':6001',
        auth: {
            headers: {
                Authorization: 'Bearer 1aadb5280dd265f055673ceed2316e07'
            }
        }
    });
}
