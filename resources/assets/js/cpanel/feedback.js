Vue.component('feedback',  require('./components/FeedbackContainer.vue'));

var FeedbackApp = new Vue({
	el:'#feedback-root',
	
	data:{
		pending:[],
		approved:[],
		rejected:[],
		per_page:5,
		pending_page:0,
		approved_page:0,
		rejected_page:0,
		pending_count:0,
		approved_count:0,
		rejected_count:0,
	},

	methods:{
		showPage(approved,skip){
			skip--;
			axiosAPIv1.get('/feedbacks',{
				params:{
					approved:approved,
					skip:skip*this.per_page,
					count:this.per_page
				}
			}).then(response => {
				if(response.status == 200)
				{
					switch(approved)
					{
						case 0: this.pending = response.data; break;
						case 1: this.approved = response.data; break;
						case 2: this.rejected = response.data; break;
					}
				}
			});
		},
	},

	created(){
		
		function getApprovedCount(){
			return axiosAPIv1.get('/feedbacks/count?approved=1');
		}
		function getRejectedCount(){
			return axiosAPIv1.get('/feedbacks/count?approved=2');
		}
		function getPendingCount(){
			return axiosAPIv1.get('/feedbacks/count?approved=0');
		}

		axios.all([
			 getPendingCount()
			,getApprovedCount()
			,getRejectedCount()
		]).then(axios.spread(
			(pending,approved,rejected) => {
			
			this.pending_count = pending.data;
			this.approved_count = approved.data;
			this.rejected_count = rejected.data;
			

			this.pending_page = Math.ceil(pending.data / this.per_page);
			this.approved_page = Math.ceil(approved.data / this.per_page);
			this.rejected_page = Math.ceil(rejected.data / this.per_page);
			


			if(this.pending_page){
				this.showPage(0,0)
			}
			if(this.approved_page){
				this.showPage(1,0)
			}
			if(this.rejected_page){
				this.showPage(2,0)
			}



		}))
		.catch($.noop);

	},


})