import WycNotification from './components/WycNotification.vue'

new Vue({
    el: '#notifications',

    data: {
        notifications: []
    },

    methods: {
        getNotifications() {
            return axiosAPIv1.get('/notifications')
                .then(result => {
                    this.notifications = result.data;
                });
        },

        markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (! notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });

            return axiosAPIv1.post('/notifications/mark-all-as-seen');
        },

        removeNotif(data) {
            var index = this.notifications.indexOf(data);

            this.notifications.splice(index, 1);
        }
    },

    computed: {
        count() {
            let count = 0;

            if (this.notifications) {
                this.notifications.forEach(function (notif) {
                    if (! notif.seen_at) {
                        ++count;
                    }
                });
            }

            return count || '';
        }
    },

    components: {
        WycNotification
    },

    created() {
        this.getNotifications();

        Event.listen('new-feedback', this.getNotifications);
        Event.listen('new-issue', this.getNotifications);
        Event.listen('new-ewallet-transaction', this.getNotifications);
        Event.listen('issue-assignment', this.getNotifications);
        Event.listen('new-document', this.getNotifications);
        Event.listen('new-ewallet-transaction', this.getNotifications);
        Event.listen('notification.remove', this.removeNotif);
    },

    mounted() {
        $(this.$el).find('.panel-body')
            .slimScroll({
                height: '300px'
            });
    }
});
