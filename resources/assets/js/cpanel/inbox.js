import WycInbox from './components/WycInbox.vue'

new Vue({
    el: '#inbox',

    data: {
        notifications: []
    },

    methods: {
        getNotifications() {
            return axiosAPIv1.get('/messages/notifications2')
                .then(result => {
                    this.notifications = result.data;
                });
        },

        markAllAsSeen() {
            this.notifications.forEach(function (notif) {
                if (! notif.seen_at) {
                    notif.seen_at = new Date();
                }
            });

            return axiosAPIv1.post('/messages/mark-all-as-seen2');
        },

        fireNewChatMessage(data) {
            Event.fire('chat-room-' + data.message.chat_room_id, data.message);
        }
    },

    computed: {
        count() {
            let count = 0;

            if (this.notifications) {
                this.notifications.forEach(function (notif) {
                    if (! notif.seen_at && notif.user_id != window.Laravel.user.id) {
                        ++count;
                    }
                });
            }

            return count || '';
        }
    },

    components: {
        WycInbox
    },

    created() {
        //this.getNotifications();

        Event.listen('new-chat-message', data => {
            this.getNotifications();
            this.fireNewChatMessage(data);

            setTimeout(function() {
                var audio = new Audio('/sound/wycnotif.wav');
                audio.play();
            }, 500);
        });

        Event.listen('new-message-notification', () => {
            this.getNotifications();
        });
    },

    mounted() {
        $(this.$el).find('.panel-body')
            .slimScroll({
                height: '300px'
            });
    }
});
