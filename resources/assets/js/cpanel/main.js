$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();

	$.fn.dataTable.ext.errMode = 'none';
	$.extend( true, $.fn.dataTable.defaults, {
	   
	    "language": {
		    "paginate": {
		      "previous": "<",
		      "next": ">",
		    }
		  }
	} );

	$('.modal-content').addClass('bounceInRight');
	$('.modal-content').css({"-webkit-animation-duration": "1s", "animation-duration": "1s", "-webkit-animation-fill-mode": "both", "animation-fill-mode": "both"});
  nav.toggleOnOff()

  $('#top-search.typeahead').typeahead({
    minLength: 2,
    fitToElement: true,
    matcher: function(item) {
      return true;
    },
    updater:function (item) {
      var win = window.open('/visa/client/'+item.id, '_blank');
      if (win) {
        win.focus();
      } else {
        alert('Please allow popups for this website');
      }

      return item;
    },
    }).on('keyup', function(e) {
      if(e.keyCode == 13) {
        let q = $('#top-search.typeahead').val();

        $.get('/visa/client/search/typeahead', { query: q }, function (data) {
          data = $.parseJSON(data);

          $('#top-search.typeahead').data('typeahead').source = data;
          $('#top-search.typeahead').typeahead('lookup').focus();
        });
      } else {
        $('#top-search.typeahead').data('typeahead').source = [];
        $('#top-search.typeahead').typeahead('lookup').focus();
      }
    });

  $('#group-search.typeahead').typeahead({
    minLength: 2,
    fitToElement: true,
    matcher: function(item) {
      return true;
    },
    updater:function (item) {
      var win = window.open('/visa/group/'+item.id, '_blank');
      if (win) {
        win.focus();
      } else {
        alert('Please allow popups for this website');
      }

      return item;
    }
  }).on('keyup', function(e) {
    if(e.keyCode == 13) {
      let q = $('#group-search.typeahead').val();

      $.get('/visa/group/search/typeahead', { query: q }, function (data) {
        data = $.parseJSON(data);

        $('#group-search.typeahead').data('typeahead').source = data;
        $('#group-search.typeahead').typeahead('lookup').focus();
      });
    } else {
      $('#group-search.typeahead').data('typeahead').source = [];
      $('#group-search.typeahead').typeahead('lookup').focus();
    }
  });

  $('#toggle-access-control').change(function() {
    var tog = $(this).prop('checked');
    nav.toggleAccessControl(tog);
  })

});


let data = window.Laravel.user;

var nav = new Vue({
  el: '#navigation',
  data: {
        uauth : data.id,
        setting: data.access_control[0].setting,
        permissions: data.permissions,
        user_roles: data.roles,
        is_master : false,
        selpermissions: [{
            search:0,
            cPanelAccounts:0,
            developmentLogs:0,
            accessControl:0,
            employees:0
        }],

       toggleFrm: new Form({
          setting     : '',
      },{ baseURL     : '/visa/access-control'}),
  },
  methods:{
    toggleAccessControl(t){
      if(t){
        this.toggleFrm.setting = 0;
      }else{
        this.toggleFrm.setting = 1;
      }

      this.toggleFrm.submit('post', '/toggle')
    },

    toggleOnOff(){
      if(this.setting == 1){
        $('#toggle-access-control').bootstrapToggle('off')
      }else if(this.setting == 0){
        $('#toggle-access-control').bootstrapToggle('on')
      }
    }


  },
  created(){
    // to check if user logged in is master account
    var master = this.user_roles.filter(obj => { 
      if(obj.name == 'master' || obj.name == 'vice-master'){
        return true;
      }
    });

    if(master.length > 0){
        this.is_master = true;
    }
    //end

    var x = this.permissions.filter(obj => { return obj.type === 'Tracking'});

    var y = x.filter(obj => { return obj.name === 'Search'});

    if(y.length==0) this.selpermissions.search = 0;
    else this.selpermissions.search = 1;

    var x = this.permissions.filter(obj => { return obj.type === 'CPanel Accounts'});

    var y = x.filter(obj => { return obj.name === 'CPanel Accounts'});

    if(y.length==0) this.selpermissions.cPanelAccounts = 0;
    else this.selpermissions.cPanelAccounts = 1;

    var x = this.permissions.filter(obj => { return obj.type === 'Development Logs'});

    var y = x.filter(obj => { return obj.name === 'Development Logs'});

    if(y.length==0) this.selpermissions.developmentLogs = 0;
    else this.selpermissions.developmentLogs = 1;

    var x = this.permissions.filter(obj => { return obj.type === 'Access Control'});

    var y = x.filter(obj => { return obj.name === 'Access Control'});

    if(y.length==0) this.selpermissions.accessControl = 0;
    else this.selpermissions.accessControl = 1;

    var x = this.permissions.filter(obj => { return obj.type === 'Employees'});

    var y = x.filter(obj => { return obj.name === 'Employees'});

    if(y.length==0) this.selpermissions.employees = 0;
    else this.selpermissions.employees = 1;
  }


});

new Vue({
  el: '#locale',

  components: {
    'locale': require('./components/Locale.vue')
  }
});
