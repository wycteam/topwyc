
new Vue({
	el: '#show-employee',

	data: {
		employee: null,
	},

	methods: {
		getEmployee() {
			let pathArray = window.location.pathname.split( '/' );

			let employeeId = pathArray[2];

			axios.get('/emp/' + employeeId + '/show-detail')
			  	.then((response) => {
					  this.employee = response.data;
			  	});
		},
		
		showLeaveModal(){
			$('#fileLeaveModal').modal('show');
		}
	},

	created() {
		this.getEmployee();
		
	},

	components: {
		'file-leave': require('./components/file-leave.vue'),
		'attendance': require('./components/attendance.vue'),
		'holiday': require('./components/holiday.vue'),
		'loan': require('./components/loan.vue'),
		'penalty': require('./components/penalty.vue'),
	}
});

