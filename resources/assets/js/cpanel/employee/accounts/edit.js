let data = window.Laravel.data;

var Accounts = new Vue({
	el:'#editAccount',
	data:{
		groups:[],
	    accountForm:new Form({
	      id: data.id,
	      avatar: data.avatar,
	      first_name: data.first_name,
	      middle_name: data.middle_name,
	      last_name: data.last_name,
	      birth_date: data.birth_date,
	      gender: data.gender,
	      civil_status: data.civil_status,
	      height: data.height,
	      weight: data.weight,
	      address: data.address,
	      email: data.email
	    }, { baseURL: 'http://'+window.Laravel.cpanel_url })
	},

	directives: {
		datepicker: {
		  bind(el, binding, vnode) {
		    $(el).datepicker({
		      format: 'yyyy-mm-dd'
		    }).on('changeDate', (e) => {
		      accountForm.$set(accountForm, 'birth_date', e.format('yyyy-mm-dd'));
		    });
		  }
		}
	},
	created(){

       Event.listen('Imgfileupload.avatarUpload', (img) => {
                this.accountForm.avatar = img;
       });		

	}
});

$(document).ready(function() {

    $('.btnUpload').click(function(){
        $('.avatar-uploader input').click();
    })

	$('#first_name, #middle_name, #last_name').keypress(function(event){
		var inputValue = event.charCode;
		//alert(inputValue);
		if(!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123)||(inputValue==32) || (inputValue==0))){
		event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
	 //if the letter is not digit then display error and don't type anything
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        return false;
	    }
	});

} );