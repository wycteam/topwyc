Vue.component('dialog-modal',  require('../components/DialogModal.vue'));

var UserApp = new Vue({
	el:'#userlist',
	data:{
		users:[],
		seluser : '',

		selectedUser: null,
	},
	methods: {
		showRemoveDialog(user) {
			this.seluser = user;

			$('#dialogRemove').modal('toggle');
		},
		getUsers() {
			function getUsers() {
				return axiosAPIv1.get('/employees');
			}
	    	axios.all([
				getUsers()
			]).then(axios.spread(
				(
					users,
				) => {
				this.users = users.data;
			}))
			.catch($.noop);
		},
		deleteUser() {
			axiosAPIv1.delete('/users/'+this.seluser.id)
				.then(response => {
					toastr.success('User Deleted!');

					var self = this;
					axiosAPIv1.get('/users')
						.then(response => {
							self.users = response.data;
						});
				});
		},
		editSchedule(user) {
			this.$refs.editschedule.setSelectedUser(user);

			$('#editSchedule').modal('show');
		}
	},
	created(){
		this.getUsers();
	},
	updated() {
		$('#lists').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"top"lf>rt<"bottom"ip><"clear">'

        });
	},
	components: {
		'edit-schedule': require('./components/edit-schedule.vue')
	}
});


$(document).ready(function() {

	$('#first_name, #middle_name, #last_name').keypress(function(event){
		var inputValue = event.charCode;
		//alert(inputValue);
		if(!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123)||(inputValue==32) || (inputValue==0))){
		event.preventDefault();
		}
	});

	$("#contact_number, #alternate_contact_number, #zip_code").keypress(function (e) {
	 //if the letter is not digit then display error and don't type anything
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        return false;
	    }
	});

} );
