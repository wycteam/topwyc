new Vue({
	el:'#attendance_monitoring',

	data:{
		loading: true,

		search: '',

		current: true,

		selectMonths: [
			{value: '1', text: 'January'},
			{value: '2', text: 'February'},
			{value: '3', text: 'March'},
			{value: '4', text: 'April'},
			{value: '5', text: 'May'},
			{value: '6', text: 'June'},
			{value: '7', text: 'July'},
			{value: '8', text: 'August'},
			{value: '9', text: 'September'},
			{value: '10', text: 'October'},
			{value: '11', text: 'November'},
			{value: '12', text: 'December'}
		],

		selectYears: ['2018', '2019', '2020', '2021', '2022'],

		filterMonth: new Date().getMonth() + 1,
		filterYear: new Date().getFullYear(),

		numberOfDays: 0,

		employees:[]
	},

	methods: {
		setNumberOfDays() {
			this.numberOfDays = new Date(this.filterYear, this.filterMonth, 0).getDate();
		},
		setCurrent() {
			if(this.filterMonth == new Date().getMonth() + 1 && this.filterYear == new Date().getFullYear())
				this.current = true;
			else
				this.current = false;
		},
		refreshTableHeadFixer() {
			setTimeout(() => {
		  		$("#fixTable").tableHeadFixer({"head" : false, "left" : 1});
		  	}, 1000);
		},
		getAttendance() {
			this.loading = true;

			axiosAPIv1.get('/attendance', {
				    params: {
				      	filterMonth: this.filterMonth,
				      	filterYear: this.filterYear,
				      	numberOfDays: this.numberOfDays
				    }
				})
		  		.then((response) => {
		  			this.employees = response.data.employees;

		  			setTimeout(() => {
		  				this.loading = false;

		  				$("#fixTable").tableHeadFixer({"head" : false, "left" : 1});

		  				let position = (this.current) ? (200 * (new Date().getDate())) - 200 : 0;
		  				$("#fixTable-wrapper").animate({scrollLeft: position}, 100);
		  			}, 1000);
		  		});
		},
		filter() {
			this.setCurrent();

			this.getAttendance();
		}
	},

	created() {
		this.setNumberOfDays();

		this.getAttendance();
	},

	computed: {
	    filteredEmployees() {
	      	return this.employees.filter(employee => {
	         	return (employee.first_name.toLowerCase().indexOf(this.search.toLowerCase()) > -1) 
	         	|| (employee.last_name.toLowerCase().indexOf(this.search.toLowerCase()) > -1);
	      	});
	    }
	}
});
