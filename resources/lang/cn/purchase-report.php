﻿<?php

return [
	'purchase-report'	=>	'采购报告'	,	//Purchase Report
	'recently-bought'	=>	'最近买了'	,	//Recently Bought
	'tracking-number'	=>	'追踪号码'	,	//Tracking Number
	'product'	=>	'产品'	,	//Product
	'placed-on'	=>	'放在'	,	//Placed On
	'shipping-fee' => '运输费用', //Shipping Fee
	'total'	=>	'总'	,	//Total
	'status'	=>	'状态'	,	//Status
	'see-more'	=>	'查看更多'	,	//See More
	'delivery-info'	=>	'配送信息'	,	//Delivery Information
	'shipped-to'	=>	'运到'	,	//Shipped To
	'address'	=>	'地址'	,	//Address
	'nearest-landmark'	=>	'最近的地标'	,	//Nearest Landmark
	'contact-no'	=>	'联系电话'	,	//Contact Number
	'alt-contact-no'	=>	'备用联系电话'	,	//Alternate Contact Number
	'notes'	=>	'笔记'	,	//Notes
	'by'	=>	'通过'	,	//By
	'qualifications'	=>	'资格'	,	//Qualifications
	'what-in-box'	=>	'盒子里有什么东西'	,	//What's in the box
	'total-summary'	=>	'总摘要'	,	//Total Summary
	'unit-price'	=>	'单价'	,	//Unit Price
	'you-save'	=>	'您保存'	,	//You save
	'now'	=>	'现在'	,	//Now
	'qty'	=>	'数量'	,	///Quantity

	'return-msg-1'	=>	'此订单已于'	,	//This order has been returned on <date> with Case No.  <case number>
	'return-msg-2'	=>	'返回，案号为'	,	//This order has been returned on <date> with Case No.  <case number>

	'reason-return'	=>	'返回原因'	,	//Reason for Returning
	'return-image'	=>	'买家图片'	,	//Images from the Buyer
	'in-transit'	=>	'在途中'	,	//In Transit
	'cancelled'	=>	'取消'	,	//Cancelled
	'pending'	=>	'有待'	,	//Pending
	'received'	=>	'收到'	,	//Received
	'delivered'	=>	'交付'	,	//Delivered
	'returned'	=>	'返回'	,	//Returned
	'brand'	=>	'牌'	,	//Brand
	'weight'	=>	'重量'	,	//Weight
	'return-videos'	=>	'买家视频'	,	//Videos from the Buyer
	'browser-no-html-support'	=>	'您的浏览器不支持HTML5视频。'	,	//Your browser does not support HTML5 video.
	'return-item'	=>	'归还物品'	,	//Return Item
	'reason-return-err'	=>	'请输入返回物品的原因。'	,	//Please enter the reason for returning the item.
	'title-return-err'	=>	'请输入标题以返回该项目。', // Please enter title for returning the item.
	'submit-return-msg-err'	=>	'在上传图片/视频之前首先提交您的理由。'	,	//Submit first your reason before uploading images/videos.
	'submit'	=>	'提交'	,	//Submit
	'close'	=>	'关'	,	//Close
	'no-purchase-msg'	=>	'您还没有购买。'	,	//You don't have purchases yet.
	'mark-received'	=>	'马上收到'	,	//Mark as received
	'mark-return'	=>	'标记为已返回'	,	//Mark as returned

	'in-transit-on-1'	=>	'在'	,	//In Transit on <received date> *use on between dates
	'in-transit-on-2'	=>	'已递送'	,	//In Transit on <received date> *use on between dates

	'drop-files'	=>	'将文件放在这里上传'	,	//Drop files here to upload
	'enter-reason'	=>	'输入原因'	,	//Enter Reason
	'delivered-on'	=>	'送货到达'	,	//Delivered on

	'received-on-1'	=>	'于'	,	//Received on <received date> *use on between dates
	'received-on-2' => '收到',  //Received on <received date> *use on between dates

	'contact-customer-support-1'	=>	'您可以联系我们的'	,	//You may contact our Customer Support regarding this matter.
	'contact-customer-support-2'	=>	'客户支持'	,	//You may contact our Customer Support regarding this matter.
	'contact-customer-support-3'	=>	'关于此事。'	,	//You may contact our Customer Support regarding this matter.

	'no-notes-msg'	=> '没有笔记', //No notes
	'mark-received-msg'	=>	'您已将收到的订单标记为'	,	//You marked your order as received.
	'update-failed-msg'	=>	'更新失败。'	,	//Update Failed.
	'you-sure-msg'	=>	'你确定？'	,	//Are you sure?

	'mark-receive-msg-1'	=>	'将您的订单标记为收到后，您将无法返回该项目。 请参阅'	,	//Once you mark your order as received you will not be able to return the item. Please refer to the
	'mark-receive-msg-2'	=>	'项目退货政策'	,	//Item Return Policy.

	'success' =>	'成功'	,	//Success
	'yes' =>	'是'	, //Yes
	'color' => '颜色', //Color
	'size' => '尺寸', //Size
	'return-product' => '退货', //Return Product
	'not-enough' => '您没有足够的余额来退回此产品。', //You do not have enough balance to return this product.
	'add-balance' => '请添加余额给你的ewallet。', //Please add balance to your ewallet.
	'airway-bill' => '航空运单', //Airway Bill 
	'deliver-to' => '交付给', //DELIVER TO
	'area-code' => '区号', //Area Code
	'collected' => 'COD数量', //Amount to be collected COD
	'awb' => 'AWB号。', //AWB No.
	'dimensions' => '尺寸（厘米）', //Dimensions (cms)
	'order-date' => '订购日期', //Order Date
	'pieces' => '件', //Pieces
	'package-type' => '包装类型', //Package Type
	'sr' => '高级。', //Sr.
	'item-code' => '项目代码', //Item Code
	'item-desc' => '商品描述', //Item Description
	'quantity' => '数量', //Quantity
	'value' => '值', //Value
	'inclusive' => '包含所有税费和运费', //Inclusive of all taxes and shipping charges
	'received-by' => '收到', //Received by
	'printed' => '打印姓名和签名', //Printed Name and Signature
	'relationship' => '关系', //Relationship
	'date-received' => '接收日期', //Date Received
	'attempt' => '尝试原因', //Attempt Reason
	'1st' => '1', //1st
	'2nd' => '第2', //2nd
	'3rd' => '第3', //3rd
	'print' => '打印', //Print
	'shipchar' => '运费和估价费', //Shipping Fee & Valuation Charge
	'vat' => '增值税', //VAT
	'products' => '制品', // Products
	'total-price-including-vat' => '总价格包括增值税', // Total Price including VAT
	'subtotal' => '小计', //Subtotal
	'discount' => '折扣', //Discount


	//new
	'i-receive' => '我收到了这个项目', //I received the item
	'i-return'  => '我想要返回这个项目', //I want to return this item.

	'photo-evidence' => '照片证明', // PHOTO EVIDENCE
	'video-evidence' => '视频证据' // VIDEO EVIDENCE

];
