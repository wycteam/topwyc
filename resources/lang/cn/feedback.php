﻿<?php

return [

	//FOR CHECKING
	'title'		=> '投诉/建议',//'FEEDBACKS/COMMENTS/SUGGESTIONS',
	'liked' => '喜欢!', //Liked
	'unliked' => '已取消!',//Unliked
	'you-and' => '你和', //You and
	'others-liked-this' => '这样的人。',//others liked this.
	'you-liked-this' => '你喜欢这个.', //You liked this
	'add-comment' => '添加评论',//'Add Comment',
	'say-comment' => '说一些关于反馈。',//'Say something about the feedback.',
	'no-more-feedbacks' => '没有更多的反馈',//'No more Feedbacks',
	'success-msg-feedback' => '反馈提交！', //Feedback Submitted
	'try-again'	=> '请再试一次！', 'Please try again!',
	'success-msg-comment' => '评论已提交！',//comment Submitted,
	'view-less-replies' => '查看更少',//view less replies


	
	'send-feedback' => '发送建议', // Send Feedback
	'description1' => '您的意见和建议对我们非常重要，请留下您的宝贵意见。', // Your Feedback is very important to us. Please dont hesitate to post your feedback below :EDIT THIS
	'submit'	=> '提交', //Submit
	'close' => '关闭', // Close
	'description2' => '该帖子将由我们的团队预审通过后才可发布，以避免垃圾信息和恶意发布', // The post will be previewed by our team before posting to avoid spam :EDIT THIS
	'view-more-replies' => '查看更多回复', // View More Replies
	'likes' => '赞', // likes
	'like'	=> '喜欢', //Like

	
];
