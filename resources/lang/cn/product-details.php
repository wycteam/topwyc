﻿<?php

return [
	'no-stock' => '目前，该商品缺货。卖家正在努力补充上述商品。您也可以从他们的商店页面', //Currently, this item is out of stock. The seller is working on to replenish the said item. You may also contact the seller from their
	'store-page' => '联系卖家', //Store Page
	'share' => '分享', //Share
	'add-fav' => '添加到收藏夹', //Add to Favorites
	'remove-fav' => '从收藏夹中删除', //Remove from Favorites
	'report' => '报告', //Report
	'brand'	=> '牌', //Brand
	'quantity' => '数量', //Quantity
	'you-have' => '你有', //You save
	'buy-now' => '立即购买', //Buy Now
	'add-cart' => '添加到购物车', //Add to cart
	'product-details' => '产品详情', //Product Details
	'reviews' => '评论', //Reviews
	'whats-in-box' => '盒子里有什么东西', //What\'s in the box
	'price' => '价钱', //Price
	'weight' => '实际重量', //Actual Weight
	'net-weight' => '净重', //Net Weight
	'size' => '尺寸', //Size
	'minimum-order' => '最小起订量', //Minimum Order Quantity
	'maximum-order' => '最大订单数量', //Maximum Order Quantity
	'warranty-period' => '保修期', //Warranty Period
	'warranty-type' => '保修类型', //Warranty Type
	'review-msg' => '产品评论有助于提高审核产品质量，功能和实用性 - 所以您还在等什么？快写一个吧', //Product reviews are helpful for providing insight into a product\'s quality, function, and usefulness – so how best should you go about writing one?
	'write-review' => '写评论', //Write Review
	'write-review-here' => '在这里写下你的产品评论', //Write your product review here
	'post' => '发布', //Post
	'add-photo' => '添加照片', //Add Photo
	'shared' => '共享', //Shared
	'like' => '赞', //Like
	'replied' => '回复', //replied
	'msg' => '信息', //Message
	'add-friend' => '添加好友', //Add Friend
	'cancel-friend-request' => '取消朋友请求', //Cancel Friend Request
	'restrict' => '限制', //Restrict
	'block' => '拉黑', //Block
	'unfriend' => '不友好', //Unfriend
	'store-details' => '商店详情', //Store Details
	'sold-by' => '已售完成', //Sold & Fulfilled by
	'contact-me' => '联络我', //Contact me
	'before' => '之前', //Before
	'view-store' => '查看商店', //View Store
	'fav' => '收藏', //Favorites
	'cod' => '货到付款', //Cash on Delivery Available
	'buyer-protection' => '100％买家保障', //100% Buyer Protection
	'recently-viewed' => '最近浏览过的', //Recently Viewed
	'modal-brand-header' => '帮助我们了解发生了什么', //Help Us Understand What is Happening
	'whats-going-on' => '这是怎么回事？', //What\'s going on?
	'annoying' => '这很讨厌或者没有趣味。', //It\'s annoying or not interesting.
	'should-not-in-site' => '我认为这不应该在这个网站上。', //I think it shouldn\'t be on this site.
	'rude' => '这是粗鲁，粗俗或使用不好的语言。', //It\'s rude, vulgar or uses bad language.
	'others' => '其他', //Others
	'other-reasons' => '其他原因？', //Other Reasons?
	'why-report' => '您需要告诉我们为什么您要举报此产品。', //You need to tell us why you\'re reporting this product.
	'submit' => '提交', //Submit
	'stock'	=> '股票',//'Stock',
	'product-description' => '产品描述
',//'Product Description',
	'example' => '例', //'Example'
	'tip-content'=>"包的内容是什么？" , // "What are the contents of the package?",
	'tip-weight'=>"这是产品的重量，包括包装的重量（克）" , // "This is the weight of the product including the weight of the packaging. (grams)",
	'tip-stocks'=>"你有多少可用的股票？如果你不输入可用的股票，这个项目将在你的商店中不可用示例：numbers / 10" , // "How many are your available stocks? If you dont enter the available stock, this item will become unavailable in your store.  Example: numbers/10",
	'tip-actual-weight'=>"这是实际产品本身的重量，不包括包装的重量（克）" , // "This is the weight of the actual product itself not including the weight of the packaging. (grams)",
	'tip-size'=>"项目高度，长度和宽度（厘米）" , // "Item height, length and width. (centimeters)",
	'tip-minimum'=>"最少要交货多少物品？" , // "How many items to be ordered in minimum for delivery?",
	'tip-maximum'=>"最多要交付多少物品？" , // "How many items to be ordered in maximum for delivery?",
	'tip-warranty'=>"您的项目的保修期或保质期（通常适用于食品）期间需要多长时间？" , // "How long will be the warranty or expiration(usually applied for food) period for your item?",
	'tip-warranty-type'=>'示例："七天更换"或"一年修理"', //  'Example: "Seven days replacement" or "One year repair"',
	'tip-description'=>"将您的作品描述为简短广告。" , // "Describe your item here as a short advertisement."

	'guide-img1'=>'正确的是您的新产品的预览。', // 'The right is the preview of your new product.',
	'guide-img2'=>'请向下滚动以为您的产品添加更多信息图片。 ', // 'Please scroll down to add more information picture for your product. We have prepared some examples for you to understand better what kind of info picture you\'re going to upload.',我们已经准备了一些例子，让您更好地了解您上传的是什么样的信息图片。',
	'guide-img3'=>'这是你的第一张图片出现在哪里。 请选择并上传您的第一张照片在这里，实时可用在右侧。', // 'Here is where your first picture will appear. Please choose and upload your first picture here and the real time is available at the right side.',
	'guide-img4' => '请按照正确的顺序上传您的信息图片2,3,4,5,6。 预览也将在右侧提供。', // 'Please upload your info picture 2,3,4,5,6 in correct order. Preview will be also available at right side.',
	'guide-img5' => '如果你对你的工作很满意 您现在可以滚动到页面的底部以完成。', // 'If you are satisfied with your work. You may now scroll to the bottom of the page to finish.',


];
