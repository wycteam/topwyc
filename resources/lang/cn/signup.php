<?php 

return [
	'register' => '寄存器', //'Register'
	'first-name' => '名字', //'First Name'
	'last-name' => '姓', //'Last Name'
	'email' => '电子邮件', //'Email'
	'password' => '密码', //'Password'
	'confirm-password' => '确认密码', //'Confirm Password'
	'date-of-birth' => '出生日期（YYYY-MM-DD）', //'Date of Birth (YYYY-MM-DD)'
	'gender' => '性别', //'Gender'
	'email-sent' => '我们刚刚向您发送电子邮件进行验证。',//We just sent you an email for verification.,
	'email-verified' => '您的电子邮件已通过验证',//Your email has been verified!
	'by-agreement' => '通过创建一个帐户，您同意WATAF的',//By creating an account you agree to WATAF's
	'terms-service' => '服务条款',//Terms of Service
	'terms-conditions' => '条款和条件', // Terms & Conditions
	'male' => '男',//Male
	'female' => '女',//female
	'ph-gender' => '选择性别...',//Select gender
];