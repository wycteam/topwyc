﻿<?php

return [

    'categories' => '商品类别', // Categories --commercial product category :EDIT THIS ONE
    'more-categories' => '更多分类', // More Categories
    'popular-stores' => '热门商店', // Popular Stores
    'latest-products' => '最新产品', // Latest Products
    'for-men' => '男士用品', // For Men :EDIT THIS ONE
    'for-women' => '女士用品', // For Women :EDIT THIS ONE
    'cyber-zone' => '数码区', // Cyber Zone
    'shop-now' => '现在去购物', // Shop Now

    //new
    'featured-stores' => '特色商店', //Featured Stores

    //visa
     'tagline-1'         => '我们专注于协助您' //'We Are Here To Help'
    ,'tagline-1-desc'    => '我们为您提供关于移民局和其他政府部门的业务协助办理和商务解决方案' //'We assist and provide services related to immigration and other government department and business solutions.'
    ,'tagline-1-desc2'    => '' //extra code

    ,'tagline-1-btn'     => '点击这里选择服务' //'Start your application today!'

    ,'tagline-2'         => '您宝贵的时间对我们来说很重要。' //'We Value Your Time'
    ,'tagline-2-desc'    => '我们很高兴可以为您介绍我们的服务' //'We are happy to introduce you our services for your convenience.'
    ,'tagline-2-btn'     => '了解更多' //'Learn more about our services.'

    ,'tagline-3'         => '我们公司的目标是提供最好的服务以及保持良好的客户关系。' //'Our company aims to provide the best service and maintain good relationship with our clients.'
    ,'tagline-3-desc'    => '我们以这些基础来实现我们的目标。' //'We implement these values to achieve those goals:'

    ,'tagline-4'         => '联系我们' //'Get in touch with us.'
    ,'tagline-4-desc'    => '我们已经准备好在任何时候协助您' //'We are ready to assist you anytime.'
    ,'tagline-4-desc-2'  => '查询您的服务状态任何时候，任何地方。' //'Track your packages and services anytime, anywhere.'
    ,'tagline-4-desc-3'  => '或者关注我们的公共平台' //'Or follow us on social platform'
    ,'tagline-4-btn'     => '给我们发邮件' //'Send Us Mail'
    
    ,'value-1'           => '透明度' //'TRANSPARENCY'
    ,'value-1-sub'       => '我们推崇' //'We promote'
    ,'value-1-long-desc' => '我们秉承所有的业务专业化，公开化，透明化，对此我们将致力于为客户提供更专业诚信的服务。' //'We undertake to be transparent in all our professional contacts. In practice, this means we will identify our client in proactive external professional contacts and services offered for a client.'
    ,'value-1-long-desc2'=> '有时我们会代表客户，以解决具体问题。 每当我们这样做，我们都承诺透明 无隐藏。'//'Sometimes we establish coalitions on behalf of clients to work on specific issues. Whenever we do so, we undertake to be transparent about the purpose of the coalition and principal funders.'
    
    
    ,'value-2'           => '诚实守信' //'HONESTY'
    ,'value-2-sub'       => '我们重视' //'We value'
    ,'value-2-long-desc' => '我们对此承诺，在工作的过程当中绝不会透露和提供任何客户的信息给他人，除非有客户方授权或政府及其他法律机构授权。所有WYC 的员工将严格按照要求遵守该条例' //'We undertake not to disclose any confidential information provided to us in the course of our work for clients, unless explicity authorized to do so or required by governments or other legal authorities. All WYC employees are contractually obligued to respect this undertaking on all internal matters.'
    ,'value-2-long-desc2'=> '我们存储和保护机密个人及公司信息，以确保只有必要使用这些信息的人才能接触它。' //'We store and handle confidential information so as to ensure that only those who are working with the information have access to it.'
    ,'value-2-long-desc3'=> '我们建议客户坚持所有这些价值观以及我们所提供的建议。' //'We advice our clients to uphold all these values in their own related to the advice we provide them.'

    ,'value-3'           => '保密' //'CONFIDENTIALITY'
    ,'value-3-sub'       => '我们保证' //'We assure'
    ,'value-3-long-desc' => '我们秉承诚信经营的理念。我们承诺绝不散播任何虚假信息欺瞒客户并合理地避免这类情况发生' //'We understand to be honest in all our professional dealings. We undertake never knowingly to spread false or misleading information and to take reasonable care to avoid doing so inadvertently. We also ask our clients to be honest with us and not to request that we compromised our principles or the law. We take every possible step to avoid conflicts of interest.'
    ,'value-3-long-desc2' => '我们也要求客户对我们坦诚布公而不是要求我们违背法律以及破坏原则' //'We also ask our clients to be honest with us and not to request that we compromised our principles or the law. We take every possible step to avoid conflicts of interest.'
    ,'value-3-long-desc3' => '我们甚至尽力避免不必要的利益冲突。' //'We take every possible step to avoid conflicts of interest.'

    ,'value-4'           => '价值观念' //'INTEGRITY'
    ,'value-4-sub'       => '我们甚至尽力避免不必要的利益冲突。' //'We practice'
    ,'value-4-long-desc' => '我们致力于与有自身价值和诚信的客户合作共赢，有时候客户有一些特殊的情况，但是这是我们的职责与客户进行沟通，并且了解情况后有效的协助客户。' //'We will only work for the organizations that are prepared to uphold the values of transparency and honesty in the course of our work for them. Sometimes our clients are involved with contentiuos issues. It is part of our job to provide communications assistance to those clients. We undertake to represent those clients only in ways consistent with the values of honesty and transparency both in what they do and in what we do for them.'

    ,'services'          => '服务' //'Services'
    ,'offered'           => '内容' //'Offered'
    ,'service-1-name'    => '旅游签证延期' //'Tourist Visa Extension'
    ,'service-1-desc'    => '入境可停留时间为30天的外国人可以申请在菲律宾逗留' //'Foreign Nationals who are admitted with an initial stay of thirty (30) days may apply for an additional stay in the Philippines.'

    ,'service-2-name'    => '9G工作签证' //'9G Working Visa'
    ,'service-2-desc'    => '正在菲律宾从事合法职业的外国公民。' //'Foreign Nationals who are proceeding to Philippine to engage in any lawful occupation.'

    ,'service-3-name'    => '移民局清关证明（ECC）' //'Emigration Clearance Certificate (ECC)'
    ,'service-3-desc'    => '外国人在出境前应该获得一份清单证明。' //'A Clearance Certificate that foreign national should secure before leaving the country.'

    ,'service-4-name'    => '外侨登记程序（ARP）' //'Alien Registration Program (ARP)'
    ,'service-4-desc'    => '通过拍照，输入编码信息和指纹，在移民局登记和注册外国人。' //'Register and Re-register foreign national to Bureau of Immigration by caturing, encoding information and finger print.'

    ,'service-view-btn'  => '查看全部服务' //'View All Services'

    ,'tracking'          => '查询' //'Tracking'
    ,'track-1-desc'      => '只需点击一下即可了解您的包裹和服务的状态。' //'Find out the status of your packages and services in just one click.'
    ,'track-btn'         => '查询' //'Track'
    ,'latest-news'       => '近日爆料' //Latest News
    ,'featured-news'     => '高亮显示' //'Featured'


    ,'mobile-app'        => '手机APP' //'Mobile APP'
    ,'apple-store'       => '苹果商店' //'APPLE STORE'
    ,'google-play'       => '谷歌商店' //'GOOGLE PLAY'
    ,'for-ios'           => '用于苹果设备' //'for IOS devices'
    ,'for-android'       => '用于安卓设备' //'for android devices'
    ,'mobile-msg-1'      => '用如下方式可以获得我们的APP' //'Here are the ways how you can get our app:' 



];
