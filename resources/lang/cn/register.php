<?php

return [
	
	 'bday' 				=> '生日' //'Birth Day'
	,'continue'				=> '继续' //Continue
	,'email-opt'			=> '首要邮件地址 (可选)' //'E-mail (Optional)'
	,'female' 				=> '女' //'Female'
	,'fname' 				=> '名' //'First Name'
	,'lname' 				=> '姓' //'Last Name'
	,'male' 				=> '男' //'Male'
	,'mob-num' 				=> '电话号码' //'Mobile Number'
	,'month'				=> '月'//'Month'
	,'ok'					=> '是的'//OK
	,'sign-up' 				=> '注册' //'Sign Up'
	,'resend' 				=> '重新发送验证码' //'Sign Up'
	,'year'					=> '年'//'Year'
    ,'birth-date'        	=> '出生年月'

//Sentences
	,'already-reg'					=> '已经登记了？' //'Already Registered?'
	,'msg-enter-pin'				=> '填写一次性验证码' //'Enter One-Time PIN'
	,'msg-pin-verify'				=> '一次性验证码验证' //'One-Time PIN Verification'
	,'msg-fill-details' 			=> '请填写明细' //'Please fill with your details'
	,'msg-pin-verify-long' 			=> "我们已经发送了一个一次性验证码到你的手机号码，请填写到下方" //"We've sent a One-Time PIN (OTP) to your mobile phone number. Please enter the OTP below."
	,'msg-verify-here'				=> '在这里验证账户'//'Verify Account Here'
	,'msg-who-recommended'			=> '谁推荐了你？'//'Who recommended you?'
	,'msg-four-digit-sent'			=> '一个4位数的代码将被发送到您的手机号码，以确保您可以访问您输入的手机号码。 请输入要继续的代码。'//'A 4-digit code will be sent to your cellphone number to ensure you have access to the cellphone number you entered. Please enter the code to proceed.'			
	,'msg-enter-code-proceed' 		=> '请输入正确的代码以继续。'//'Please enter the correct code to proceed.'
	,'msg-code-sent'				=> '代码已成功发送。 请检查我们发送到您手机号码的代码。'//'Code successfully sent. Please check the code we\'ve sent to your mobile number.'
	,'msg-user-registered'			=> '用户已注册。'//'User already registered.'

	//months
	,'january'		=> '一月'//'January'
	,'february'		=> '二月'//'February'
	,'march'		=> '游行'//'March'
	,'april'		=> '四月'//'April'
	,'may'			=> '可以'//'May'
	,'june'			=> '六月'//'June'
	,'july'			=> '七月'//'July'
	,'august'		=> '八月'//'August'
	,'september'	=> '九月'//'September'
	,'october'		=> '十月'//'October'
	,'november'		=> '十一月'//'November'
	,'december'		=> '十二月'//'December'

];