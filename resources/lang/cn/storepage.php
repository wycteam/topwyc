<?php
return [
	'add-prod' => '添加产品', //Add Product
	'edit-store' => '编辑商店', //Edit Store
	'delete-store' => '删除商店', //Delete Store
	'description' => '说明', //Description
	'address' => '地址', //Address
	'contact' => '联系电话', //Contact Number
	'company' => '公司', //Company
	'company-address' => '公司地址', //Company Address
	'facebook' => 'Facebook的', //Facebook
	'twitter' => 'Twitter的', //Twitter
	'instagram' => 'Instagram的', //Instagram
	'ratings' => '评级', //Ratings
	'products' => '制品', //Products
	'draft' => '草案', //Draft
	'edit' => '编辑', //Edit
	'delete' => '删除', //Delete
];