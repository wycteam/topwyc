﻿<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'=>'这些信息与我们的记录不符。',
     'throttle'=>'短时间尝试登录次数过多，请30秒后再试。'
];
