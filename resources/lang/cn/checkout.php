﻿<?php

return [
	'order-summary' => '订单摘要', //Order Summary
	'order-summary-sub' => '请验证每个产品的数量', //Please verify the quantity per item
	'product-details' => '产品名称及细节', //Product Name & Details
	'no-items-cart' => '购物车是空的！', //No items in cart!
	'shipping-address' => '运送地址', //Shipping Address
	'shipping-address-sub' => '
检查您的运送信息，以确保您可以收到您的订单。', //Check your shipping info to ensure that your item will be received.
	'name-contact' => '姓名与联络方式', //Name & Contact
	'address' => '地址', //Address
	'note' => '备注', //Note
	'standard-delivery' => '标准运送', //Standard Delivery
	'current-bal' => '账户现有余额', //Current Balance
	'total-amt' => '总金额', //Total Amount
	'place-order' => '下订单', //PLACE ORDER
	'return-cart' => '返回购物车', //Return to cart
	'ok' => '好的', //Okay
	'msg-no-items-cart' => '你的购物车是空的！。', //You have no Items in your cart.
	'msg-no-ewallet' => '您的电子钱包余额不足。', //You have insufficient E-Wallet.

];