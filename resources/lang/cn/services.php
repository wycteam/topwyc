<?php  
	 return [
	 	 'tagline-1' 		=> '不必担心，我们是你坚强的后盾' //'Don\'t worry, we\'ve got your back.'
	 	,'tagline-1-desc'	=> '我们明白办理移民局和其他政府部门手续的麻烦之处' //'We understand the hassle of processing immigration & government-related documents.'
	 	,'tagline-1-desc2'	=> '您为什么要花为什么更多的时间和精力来寻求我们的帮助？' //'Why spend more time and energy when you can seek our help? '
	 	,'tagline-1-desc3'	=> '看看我们的服务列表。' //'Check out our list of services we offer.'
	 	,'tagline-1-btn'	=> '仍有其他问题？' //'Still have questions?'
	 ]
?>