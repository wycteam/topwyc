﻿<?php
return [
	'congratulations'=>'恭喜！' // 'CONGRATULATIONS!',
	'sub-msg'=>'你的商店现在已上线。', // 'Your store is now online.',
	'not-yet-verified' => '您的商店还未上线', //Your store is NOT YET online.', 
	'sub-msg-not-verified' => '请提交您的个人或企业身份文件以在个人信息管理页面中验证您的帐户。<br>否则，您商店中的以下功能被锁定：', //Please submit your personal or enterprise identification documents to verify your account in profile management page.<br> Otherwise, the following functions in your store is locked: ', 
	'buy-and-sell' => '1.买卖', //1. Buy and Sell', 
	'online-chat' => '2.在线聊天', //2. Online Chat', 
	'store-rating' => '3.商店评分', //3. Store Rating', 
	'comment-reply' => '4.评论/回复（用户可以在您的产品中发表评论，但您无法回复）', //4. Comment/Reply (People can comment in your product but you cannot reply)', 
	'back-home'=>'返回首页', // 'Back to Home Page',
	'upload-prod'=>'上传产品' // 'Upload Product'

];