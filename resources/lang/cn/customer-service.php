<?php

return [
	'customer-service' => '客户服务', //Customer Service
	'subject' => '学科', //Subject
	'body' => '身体', //Body
	'all-tickets' => '所有门票', // All Tickets
	'open-tickets' => '开门票', // Open Tickets
	'closed-tickets' => '闭门票', // Closed Tickets
	'create-new-ticket' => '创建新机票', // Create New Ticket
	'you-dont-have-any-open-tickets' => '你没有打开门票。', // You don't have any open tickets.
	'issue-number' => '发行数量', // Issue Number
	'date-created' => '创建日期', // Date Created
	'status' => '状态', // Status
	'open' => '打开', // Open
	'closed' => '关闭', // Closed
	'please-subject' => '请指明主题。', //Please indicate subject.
	'give-a-concern' => '给予关注或建议。', //Give a concern or suggestion.
	'message-thread' => '消息主題', // Message Thread
	'reopen-ticket' => '重新開票', // Reopen Ticket
	'this-ticket-is-already-closed' => '這張票已經關閉了。', // This ticket is already closed.
	'if-you-wish-to-reopen-this-issue-a-new-ticket-will-be-created' => '如果您想重新打開此問題，將會創建一個新的票證。' // If you wish to reopen this issue, a new ticket will be created.
];
