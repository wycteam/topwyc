﻿<?php

return [
	'friends' 				=> '朋友', // Friends
	'all' 					=> '所有朋友', // All Friends
	'requests' 				=> '好友请求', // Friend Requests
	'add' 					=> '添加好友', // Add Friend
	'start-adding' 			=> '开始添加好友', // Start Adding Friends
	'unfriend' 				=> '删除好友', // Unfriend
	'unfriend-confirmation' => '你确定你想删除好友吗', // Are you sure you want to unfriend
	'no-request' 			=> '没有好友请求', // No Friend Requests

	//new
	'find-people' 			=> '找人', //find people
	'no-friends' 			=> '你还没有朋友 加个人...', // You don't have friends yet. Add someone ...
	'search' 				=> '搜索', // Search..
	'type-user' 			=> '输入用户的名称', // Type a name of a user
	'result' 				=> '结果', // Results
	'registered' 			=> '注册', // Registered
	'recommended' 			=> '推荐', // Recommended
	'no-recommendations' 	=> '没有朋友推荐..', // There are no friend recommendations..
	'send-message' 			=> '发信息', // Send Message
	'accept'				=> '接受', //accept
	'reject'				=> '拒绝',//Reject
];
