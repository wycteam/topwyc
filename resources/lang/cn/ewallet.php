﻿<?php

return [
	//for checking
	'no-user-found'			=> '哎呀！没有找到用户。请再试一次。', //Oops! No Users found. Please Try again.
	'ph-email'				=> '输入电子邮件', //Type an email
	'ph-email-help'			=> '对于非朋友：提供他/她的电子邮件', //for non-friends: provide his/her email
	'msg-no-account'		=> '没有银行帐户', //No Bank Accounts Yet
	'your-bank-account'		=> '您的银行帐户', // your bank account
	'add'					=> '加',//add
	'edit'					=> '编辑',//edit
	'update'				=> '更新', //update
	'offline-or-online'		=> '线下或线上', //'Offline or Online'
	'from-another'			=> '来往/来自另一个帐户',//'to/from another Account'
	'get-saved-money'		=> '拿到你的钱', //'Get your saved money'
	'reg-account'			=> '注册您的帐户',//'Register your account'
	'upload'				=> '上传', //Upload
	'upload-deposit-slip'	=> '上传存款单', //'Upload Deposit Slip'

	'wait-verification' 	=> '成功！请等待验证', // Success! Please wait for verification
	'success-upload' 		=> '存款上传!', // Deposit Slip Uploaded!
	'try-again' 			=> '请再试一次', // Please Try Again.
	'failed-upload' 		=> '上传不成功', // Upload Unsuccessful!
	'account-added' 		=> '银行账户已添加', // Bank Account Added!
	'account-updated' 		=> '更新银行账户信息', // Bank Account Updated!
	'success-request' 		=> '请求已提交', // Request Submitted!
	'ref-no' 				=> '你的流水号码: ', // Your Reference Number: 
	'deposit' 				=> '存款', // Deposit
	'withdraw' 				=> '提款', // Withdraw
	'give-receiver' 		=> '请将这个代码给接收人', // Please give this code to the receiver/s.
	'ewallet-received' 		=> '已接收', // Ewallet Received!




	

	//CHECKED
	'title'					=> '电子钱包', // E-WALLET
	'transactions'			=> '交易记录', //Transaction Record'
	'available-balance'		=> '可用余额', //Available Balance
	'load-money'			=> '账户储值', //Load Money
	'withdraw-funds'		=> '账户提款', //Withdraw Money
	'transfer-or-receive'	=> '转账/接收', //'Transfer / Receive',
	'bank-account'			=> '银行账户', //Bank Account
	'datetime'				=> '日期时间', //Date & Time'
	'ref-no-short'			=> '流水号', //Ref No.'
	'type'					=> '类型', //Type'
	'amount'				=> '金额', //Amount'
	'description'			=> '描述', //Description'
	'status'				=> '状态', //Status'
	'balance'				=> '余额', //Balance
	'offline'				=> '线下', //Offline'
	'online'				=> '线上', //Online'
	'load-online-soon'		=> '在线储值功能正在建设中！请再忍耐一段时间，即将完工！', //Load Money Online is Under Construction! Please bear with us. It\'s coming soon!
	'close' 				=> '关闭', // Close
	'bank'					=> '银行', //Bank
	'choose-bank'			=> '选择银行...', //Choose a Bank ...
	'submit'				=> '提交', //Submit
	'withdraw-funds'		=> '账户提款', //Withdraw Money
	'account-name'			=> '账户名', //Account Name
	'account-no'			=> '帐号',//'Account Number',
	'bank-account'			=> '银行账户', //Bank Account
    'receive'				=> '接收',//'Receive'
	'transfer'				=> '转账', //Transfer 
	'code' 					=> '代码', //CODE
	'ph-code'				=> '8字符代码', // '8 Character Code',
	'receiver'				=> '接收人',//'Receiver',
	'notes'					=> '备注', // 'Notes',
	'leave-msg' 			=> '留言', //Leave a message 
	'bank'					=> '银行', //Bank
	'status'				=> '状态', //Status'
	'add-bank-account' 		=> '添加您的银行账户', //Add Bank Account
	'choose-bank'			=> '选择银行...', //Choose a Bank ...
	'reenter-account-no'	=> '重新输入帐号', //Re-enter Account Number
	'leave-msg' 			=> '留言', //Leave a message 

	//recently added -xy
	'ewallet-faq-1'			=>	'电子钱包是一个可以安全的存储用户用于交易和支付信息的在线系统。'	,	//A system that securely stores user payment information for numerous electronic transactions on this website.

	'ewallet-faq-2'			=>	'电子钱包系统预计将成为在线支付行业的主流参与者，因为消费者不断的的寻求更好更容易和更快捷的在线购买商品和支付方式。它使得网络购物比以往任何时候都更加安全和快捷，这就是它为什么越来越受欢迎并且得到广泛的应用。'	,	//E-wallets are expected to become a major player in the online payment industry, as consumers constantly continue to look for additional easier and quicker ways to purchase goods and services online. It makes online shopping easier and faster than ever before that is why it is becoming more and more popular and widely used.

	'ewallet-faq-3'			=>	'通过使用这个电子钱包，用户可以轻松快速地在这个购物网站上完成不同产品的购买。 用户也可以使用此系统将钱转移给其他用户。'	,	//By using this e-wallet, users can complete purchases of different products in this shopping website easily and quickly. Users can also transfer money to another users using this system.

	'read-more'				=>	'阅读更多'	,	//Read More
	'depslip-ins-1'			=>	'在您选择的银行存入您的信用金额后，请上传清楚的存款单副本。'	,	//After depositing you rindicated amount on your selected bank, upload a clear copy of your deposit slip.
	'depslip-ins-2'			=>	'要上传存款单，请单击电子钱包交易表上的记录。'	,	//To upload your deposit slip, click the record on E-wallet transaction table.
	'depslip-ins-3'			=>	'我们的客户服务将审查您上传的存款单。'	,	//Our Customer Service will review your uploaded deposit slip.
	'depslip-ins-4'			=>	'一旦验证，您将被通知，通常验证时间为30分钟到1个小时。'	,	//Once it is verified, you will be notified. This usually takes 30 minutes to 1 hour.
	'see-instructions'		=>	'见说明'	,	//See Instructions
	'instructions'			=> 	'说明' , //Instructions
	'invalid-code'			=>  '无效的代码！', //Invalid Code!

	'usable'				=>  '可用',
	'show-details'			=>  '（显示详细资料）',
	'mininum-amount'		=>  '您可以转账的最低金额为100。' //Minimum amount you can transfer is 100.

	 
];
