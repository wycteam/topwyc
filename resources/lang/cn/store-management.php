﻿<?php

return [

	'store-mgnt-title'	=>	'商店管理'	,	//Store Management
	'create-store'	=>	'创建商店'	,	//Create Store
	'store'	=>	'商店'	,	//Stores
	'product'	=>	'商品'	,	//Products
	'upload'	=>	'上传'	,	//Upload
	'up-cover-photo'	=>	'上传封面照片'	,	//Upload Cover Photo
	'store-name'	=>	'商店名称'	,	//Store Name
	'cn'	=>	'（中文）'	,	//(Chinese)
	'en'	=>	'（英文）'	,	//(English)
	'company-name'	=>	'公司名'	,	//Company Name
	'desc'	=>	'说明'	,	//Description
	'company-add'	=>	'公司地址'	,	//Company Address
	'fb'	=>	'Facebook'	,	//Facebook
	'sel-cat'	=>	'选择类别'	,	//Select Category
	'twitter'	=>	'推特'	,	//Twitter
	'category'	=>	'类别'	, // Category
	'add'	=>	'完整地址'	,	//Complete Address
	'ig'	=>	'Instagram'	,	//Instagram
	'city'	=>	'市'	,	//City
	'prov'	=>	'省'	,	//Province
	'land' => '最近的地标', //Nearest Landmark
	'email'	=>	'电子邮件'	,	//Email
	'mobile-no'	=>	'手机号码'	,	//Mobile No.
	'save'	=>	'保存'	,	//Save
	'add-prod'	=>	'添加产品'	,	//Add Product
	'prof-pic'	=>	'个人资料图片'	,	//Profile Picture

	'acct-not-ver-1'	=>	'您的帐户尚未验证。 请注意，您可以创建商店和产品，但以下功能被禁用：'	,	//Your Account is not yet verified. Please be informed that you can create stores and products but the following features are disabled: 
	'acct-not-ver-2'	=>	'*您不能销售您的产品（商店用户可以看到您的产品，但不能添加到他们的购物车）'	,	//* You cannot sell your products (Shop Users can see your product but cannot add to their shopping cart)
	'acct-not-ver-3'	=>	'*您无法回复客户的意见'	,	//* You cannot reply on customer's feedback 
	'acct-not-ver-4'	=>	'*客户无法向您发送信息'	,	//* Customers cannot send a message to you 
	'acct-not-ver-5'	=>	'要验证您的账户，请在这里上传相应的文件。'	,	//To verify your account, please upload documents here.
	'acct-not-ver-6'	=>	'这里。'	,	//To verify your account, please upload documents here.

	'cancel'	=>	'取消'	,	//Cancel
	'sub-cover'	=>	'提交封面'	,	//Submit Cover
	'primary-pic'	=>	'主画面'	,	//Primary Picture
	'choose-file'	=>	'选择一个文件'	,	//Choose a file
	'pick-img'	=>	'选择一个图像'	,	//Pick an Image
	'sub-prof-img'	=>	'提交个人资料图片'	,	//Submit Profile Image
	'prof-pic-set-msg'	=>	'配置文件图片已设置。'	,	//Profile Picture is set.
	'reset-complete' =>	'重置完成。',	//Reset Complete.
	'store-profile-pic'	=>	'商店简介图片'	,	//Store Profile Picture
	'edit-store'	=>	'编辑商店'	,	//Edit Store
	'del-store'	=>	'删除商店'	,	//Delete Store
	'contact-num'	=>	'联系电话'	,	//Contact Number
	'rating'	=>	'评级'	,	//Ratings
	'draft'	=>	'草案'	,	//Draft
	'you-save'	=>	'你节省'	,	//You save<discount>
	'review'	=>	'评测'	,	//Reviews
	'edit'	=>	'编辑'	,	//Edit
	'prod-img'	=> '产品图片',	//Product Image
	'del'	=> '删除',	//Delete
	'store-req' => '商店名称是必需的。', //Store name is required.
	'add-req' => '完整地址是必需的。', //Complete Address is required.
	'city-req' => '城市是必需的。', //City is required.
	'prov-req' => '省是必需的。', //Province is required.
	'land-req' => '需要地标。', //Landmark is required.
	'email-req' => '电子邮件是必需的。', //Email is required.
	'mobile-req' => '需要手机号码。', //Mobile number is required.

	'field-required' => '这是必填栏',	//This field is required
	'province'	=>	'省'		//Province
];
