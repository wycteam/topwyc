﻿<?php

return [
	'delete' => '删除', // Delete
	'ok' => '确认', // Okay
	'cancel' => '取消', // Cancel
	'proceed' => '继续', // Proceed
	'confirm' => '确认', // Confirm
	'close' => '关闭', // Close
	'yes' => '是', // Yes
	'no' => '不是', // No
	'reset' => '重置', // Reset
	'cropped' => '裁剪', // Cropped
	'crop' => '缩放', // Crop
	'store' => '商店', // Store
	'items' => '项目', // Items
	'required' => '必填项', //Required
	'save' => '保存', //Save
	'next' => '下一个', //Next
	'previous' => '上一个', //Previous
	'continue'	=> '继续', // continue
	'view-all' => '查看全部',//View All
	'browse' => '浏览',// 'Browse',
	'remove' => '移除',// 'Remove'
	'submit'	=> '提交', //Submit
	'finish'	=> '完',//'Finish'
	'filter' => '過濾' // Filter
];
