﻿<?php

return [
	'page-title' => '您的收货地址', //Your Shipping Address
	'choose-receiving' => '选择您的接收选项', //Choose your receiving option
	'select-profile' => '选择配置文件', //Select Profile
	'new-address' => '新地址', //New Address
	'new-shipping-address' => '新运送地址', //New Shipping Address
	'ph-new-profile' => '请输入您的新配置文件的名称', //Please enter the name of your new profile
	'full-name' => '全名', //Full Name
	'contact-no' => '联系电话', //Contact Number
	'complete-address' => '完整地址', //Complete Address
	'brgy' => '区', //Barangay
	'province' => '省', //Province
	'select' => '选择', //Select
	'city-or-municipality' => '市/市', //City/Municipality
	'landmark' => '最近的地标', //Nearest Landmark
	'remark-notes' => '备注/备注', //Remark/Notes
	'ph-remarks' => '请将您的首选交货时间以及任何额外的要求或说明告知我们，我们将尽可能地满足您的要求', //Please write your preffered deliver time, any additional request or note to the counter, we will try to assist as much as possible
	'alt-contact' => '备用联系人', //Alternate Contact
	'ph-alt-contact' => '请您留个朋友的姓名和联系方式，以便我们在无法与您的朋友或家人联系时联系您朋友的电话号码，否则该次货物将被退回到运送中心', //Please leave a name and a contact incase we can\'t reach your number, like your friend or family, or else the item will be returned to the pickup point
	'previous' => '上一步', //Previous
	'continue' => '下一步', //Continue
];
