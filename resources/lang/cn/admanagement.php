﻿<?php 
return [
	'ad-mgmt' 		=> '广告管理',//Ad Management
	'instruction1' 	=> '210x240',
	'instruction2' 	=> '精选店铺',//Featured Shop
	'instruction3' 	=> '横幅幻灯片',//on Banner Slide
	'instruction4' 	=> '将在主页的第二张幻灯片上显示',//Will be displayed at homepage on 2nd slide of banner
	'size-info' 	=> '尺寸和信息',//Sizes and Information
	'featured-prod' => '特色产品',//Featured Product
	'size1' 		=> '425x480',
	'desc1' 		=> '将在首页上的主页上显示',//Will be displayed at homepage on the first slide
	'size2' 		=> '425x295',
	'desc2' 		=> '将在主页的第二张幻灯片上显示',//Will be displayed on homepage 2nd slide of the banner
	'size3' 		=> '140x185（左，中，右）',
	'desc3' 		=> '将在主页的第二张幻灯片上显示',//Will be displayed on homepage 2nd slide of the banner
	'featured-shop' => '精选店铺',//Featured Shop
	'size4' 		=> '210x240',
	'desc4' 		=> '将在第二张标签的主页上显示',//Will be displayed at homepage on 2nd tab
	'size5' 		=> '260x185',
	'desc5' 		=> '将在横幅幻灯片下的主页显示',//Will be displayed at homepage under the banner slide
	'call-more-info'=> '请致电我们以获取更多信息',//Call us for more information
];