﻿<?php

return [
	'title' => '收藏目录', //WISHLIST
	'product-name' => '商品名称',//PRODUCT NAME
	'stock-status' => '库存状态', //STOCK STATUS
	'price' => '价钱', // PRICE
	'add-cart' => '添加到购物车', //Add to cart
	'no-item' => '您的愿望清单中没有任何项目', //There are no items in your wishlist
	'are-you-sure' => '您确定要从心愿单中删除此产品吗？', //Are you sure you want to remove this product from wishlist?
	'remove' => '去掉', //Remove
	'removed' => '从心愿单中删除！', //Removed from Wishlist!
	'not-added' => '未添加到购物车。', //Not Added to cart.
	'added' => '加入购物车。' //Added to Cart
];