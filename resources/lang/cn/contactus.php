<?php  
	 return[
	 	 'tagline-1' 		=> '我们都在一起。' //'We are all in this together.'
		,'tagline-1-desc'	=> '对我们的服务有兴趣吗？' //'Interested in our services?'
		,'tagline-1-desc2'	=> '联系我们并且成为一个满意的客户。' //'Contact us and be one of our satisfied customers.'
		,'tagline-1-desc3'	=> '我们不管您在哪里，我们都会倾听您的需要。' //'We will hear you whenever and wherever you are.'
		,'view-services'	=> '查看我们的服务' //'View Our Services'
		,'operating-hrs'	=> '运营时间' //'Operating Hours'
		,'sched'			=> '礼拜一 - 礼拜五 ' //'Monday - Friday (8 am - 5pm)'
	 ]
?>