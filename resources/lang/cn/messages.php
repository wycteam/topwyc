<?php 
return [
	'messages' => '消息', //Messages
	'search-user' => '搜索用户', //Search User
	'enter-to-send' => '按回车键发送消息', //Press enter to send the message
	'write-something' => '写一些东西...', //Write Something...

	'accept'				=> '接受', //accept
	'unfriend' 				=> '删除好友', // Unfriend
	'reject'				=> '拒绝',//Reject
	'add' 					=> '添加朋友', // Add Friends
	'block-msg'				=> '阻止消息', // Block Messages
	'unblock'				=> '解除封锁', // Unblock

	'reminder' => '提醒',//Reminder
	'alert-msg' => '为了安全起见，始终在WATAF.ph内进行交易。',//Always transact inside WATAF.ph for safety and security purposes.
];