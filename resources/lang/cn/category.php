﻿<?php

return [

    'womens-fashion' => '女士时尚', // Women's Fashion :EDIT THIS
        'womens-clothing' => '服装', // Clothing
        'womens-shoes' => '鞋子', // Shoes
        'womens-bags' => '包包', // Bags
        'womens-eyewear' => '眼镜', // Eyewear
        'womens-t-shirt-tops' => 'T恤衫和上衣', // T-Shirt & Tops
        'womens-dresses' => '连衣裙', // Dresses
        'womens-blouses-shirts' => '衬衫和衬衣', // Blouses & Shirts :EDIT THIS ONE
        'womens-jackets-coats' => '夹克和大衣', // Jackets & Coats
        'womens-sneakers' => '球鞋', // Sneakers :EDIT THIS
        'womens-sandals' => '凉鞋', // Sandals
        'womens-flat-shoes' => '平底鞋', // Flat Shoes
        'womens-heels' => '高跟鞋', // Heels
        'womens-shoulder-bags' => '单肩包', // Shoulder Bags
        'womens-tote-bags' => '手包', // Tote Bags
        'womens-top-handle-bags' => '顶级手提包', // Top Handle Bags
        'womens-sunglasses' => '墨镜', // Sunglasses
        'womens-eyeglasses' => '眼镜', // Eyeglasses
        'womens-jewellery' => '首饰', // Jewellery
        'womens-pants-shorts' => '裤子和短裤', // Pants & Shorts
        'womens-skirts' => '裙子', // Skirts
        'womens-jeans' => '牛仔裤', // Jeans
        'womens-lingerie-nightwear' => '内衣和睡衣', // Lingerie & Nightwear
        'womens-muslim-wear' => '穆斯林服装', // Muslim Wear
        'womens-swimwear' => '泳衣', // Swimwear
        'womens-accessories' => '小饰品', // Accessories
        'womens-boots' => '靴子', // Boots
        'womens-flip-flops' => '人字拖', // Flip Flops
        'womens-cross-body-bags' => '十字体袋', // Cross Body Bags
        'womens-satchels' => '手提包', // Satchels
        'womens-backpacks' => '背包', // Backpacks
        'womens-sling-bags' => '吊挎包', // Sling Bags
        'womens-clutches' => '离合器111', // Clutches
        'womens-purse' => '钱包', // Purse
        'womens-bracelets-bangles' => '手饰', // Bracelets & Bangles
        'womens-necklaces-pendants' => '项链和吊坠', // Necklaces & Pendants
        'womens-rings' => '戒指', // Rings
        'womens-earrings' => '耳环', // Earrings
        'womens-jewellery-sets' => '珠宝首饰', // Jewellery Sets
        'womens-watches' => '手表', // Watches
        'womens-fashion-watches' => '时尚', // Fashion
        'womens-business-watches' => '商业', // Business
        'womens-casual-watches' => '休闲', // Casual
        'womens-sports-watches' => '体育', // Sports

    'mens-fashion' => '男士时尚', // Men\'s Fashion :EDIT THIS
        'men-t-shirts' => 'T恤衫', // T-shirts
        'mens-accessories' => '配件', // Accessories
        'mens-backpacks' => '背包', // Backpacks
        'mens-bags' => '袋子', // Bags
        'mens-boots' => '靴子', // Boots
        'mens-bracelets' => '手镯', // Bracelets
        'mens-business-watches' => '商业', // Business
        'mens-casual-watches' => '休闲', // Casual
        'mens-clothing' => '服装', // Clothing
        'mens-earrings' => '耳环', // Earrings
        'mens-eyeglasses' => '眼镜', // Eyeglasses
        'mens-eyewear' => '眼镜', // Eyewear
        'mens-fashion-test' => 'test', // test
        'mens-fashion-watches' => '时尚', // Fashion
        'mens-flat-shoes' => '平底鞋', // Flat Shoes
        'mens-formal-shoes' => '正式鞋', // Formal Shoes
        'mens-jackets-coats' => '夹克和大衣', // Jackets & Coats
        'mens-jeans' => '牛仔裤', // Jeans
        'mens-jewellery' => '首饰', // Jewellery
        'mens-messenger-bags' => '背包', // Messenger Bags
        'mens-necklaces-pendants' => '项链和吊坠', // Necklaces & Pendants
        'mens-pants-shorts' => '裤子和短裤', // Pants & Shorts
        'mens-rings' => '戒指', // Rings
        'mens-running-shoes' => '跑鞋', // Running Shoes
        'mens-sandals' => '凉鞋', // Sandals
        'mens-satchels' => '手提包', // Satchels
        'mens-shirt-accessories' => '衬衫配件', // Shirt Accessories
        'mens-shirts' => '衬衫', // Shirts
        'mens-shoes' => '鞋子', // Shoes
        'mens-shoulder-bags' => '单肩包', // Shoulder Bags
        'mens-sling-bags' => '吊挎包', // Sling Bags
        'mens-sneakers' => '球鞋', // Sneakers :EDIT THIS
        'mens-sports-watches' => '体育', // Sports
        'mens-suits' => '西装', // Suits
        'mens-sunglasses' => '墨镜', // Sunglasses
        'mens-sweaters-cardigans' => '毛衣和开衫', // Sweaters & Cardigans
        'mens-swimwear' => '游泳衣', // Swimwear
        'mens-underwear-socks' => '内衣和袜子', // Underwear & Socks
        'mens-wallets' => '钱包', // Wallets
        'mens-watches' => '手表', // Watches

    'sports-outdoor' => '体育户外', // Sports & Outdoor :EDIT THIS
        'outdoor-gears' => '户外装备', // Outdoor Gears
        'outdoor-kettle' => '户外水壶', // Outdoor Kettle
        'telescope' => '望远镜', // Telescope
        'tent' => '帐篷', // Tent
        'sport-shoes' => '运动鞋', // Sport Shoes
        'badminton-shoes' => '羽毛球', // Badminton
        'tennis-shoes' => '网球', // Tennis
        'sports-safety' => '体育安全', // Sports Safety
        'sports-gears' => '运动齿轮', // Sports Gears
        'sport-skirt' => '运动裙', // Sport Skirt
        'sneakers' => '球鞋', // Sneakers :EDIT THIS
        'swim-gears' => '游泳齿轮', // Swim Gears
        'nemesis-fins' => 'Nemesis 金丝雀', // Nemesis Fins
        'aqua-shoes' => '水族鞋', // Aqua Shoes
        'hydro-belt' => '水带', // Hydro Belt
        'snorkel' => '浮潜', // Snorkel

    'kids-wear' => '儿童服装', // Kids Wear
        'clothes-kids' => '衣服', // Clothes
        'pyjamas-kids' => '睡衣', // Pyjamas
        'kids-sweater' => '毛衣', // Sweater
        'kids-jacket' => '夹克', // Jacket
        'kids-shirt' => '衬衫', // Shirt
        'kids-shoes' => '鞋子', // Shoes
        'kids-sport-shoes' => '运动鞋', // Sport Shoes
        'kids-snow-boots' => '雪地靴', // Snow Boots
        'kids-socks' => '袜子', // Socks
        'kids-tights' => '紧身衣', // Tights

    'baby-products' => '婴儿用品',
        'maternal-products' => '产妇用品', // Maternal Products
        'care-belly-pants' => '连肚裤', // Care Belly Pants :EDIT THIS
        'waist-pillow' => '腰枕', // Waist Pillow
        'stretch-marks' => '妊娠纹去除', // Stretch Marks
        'maternal-leggings' => '绑腿', // Leggings :EDIT THIS
        'maternal-dress' => '连衣裙', // Dress
        'feeding-supplies' => '喂食用品', // Feeding Supplies
        'nursing-bra' => '护理胸罩', // Nursing Bra
        'baby-warmer' => '宝宝保暖', // Warmer
        'breast-pump' => '乳汁提取', // Breast Pump
        'breast-pads' => '乳房垫', // Breast Pads
        'mummy-bag' => '妈咪包', // Mummy Bag
        'baby-supplies' => '婴儿用品', // Baby Supplies
        'strollers' => '婴儿车', // Strollers
        'crib' => '婴儿床', // Crib
        'changing-mat' => '包包护理垫', // Changing Mat
        'fedding-bottles' => '奶瓶', // Feeding Bottles
        'seat-strap' => '座位安全带', // Seat Strap

    'household-products' => '家庭用品',
        'storage' => '存储', // Storage
        'storage-box' => '储物盒', // Storage Box
        'cosmetics' => '化妆品', // Cosmetics
        'compression-bag' => '压缩袋', // Compression Bag
        'tissue-box' => '纸巾盒', // Tissue Box
        'jewelry-storage' => '珠宝存储', // Jewelry Storage
        'hook-shoe-box' => '钩鞋盒', // Hook Shoe Box
        'textile-cloth' => '纺织布', // Textile Cloth
        'textile-denim' => '牛仔布', // Denim
        'sofa-cushion' => '沙发靠垫', // Sofa Cushion
        'carpet' => '地毯', // Carpet
        'textile-stitch' => '缝', // Stitch :EDIT THIS
        'towels' => '毛巾', // Towels
        'curtain-finished' => '窗帘', // Curtain Finished
        'house-cleaning' => '房子打扫', // House Cleaning
        'mop' => '拖把', // Mop
        'trash' => '垃圾类', // Trash
        'toilet' => '厕所', // Toilet
        'wipe-slippers' => '拖鞋', // Wipe Slippers :EDIT THIS
        'cuff' => '袖口', // Cuff
        'rubber-gloves' => '塑胶手套', // Rubber Gloves
        'aprons' => '围裙', // Aprons

    'wedding-supplies' => '婚礼用品',
        'wedding-dress' => '礼服', // Wedding Dress
        'trailing-wedding' => '婚礼', //  Trailing Wedding
        'gown' => '长袍', // Gown
        'bridesmaid-dress' => '伴娘礼服', // Bridesmaid Dress
        'toast-clothing' => '司仪服装', // Toast Clothing
        'suits-groom' => '新郎西装', // Suits Groom
        'bow-tie' => '领结', // Bow Tie
        'wedding-supplies-list' => '婚礼用品', // Wedding Supplies
        'invitations' => '邀请函', // Invitations
        'candy-box' => '糖果盒', // Candy Box
        'candy-bags' => '糖果袋', // Candy Bags
        'kneeling-pads' => '跪垫', // Kneeling Pads
        'wedding-decoration' => '婚礼装饰', // Wedding Decoration
        'ring-pillow' => '戒指枕头', // Ring Pillow
        'clothes-accessories' => '衣服配件', // Clothes & Accessories
        'wedding-viel' => '维尔', // Viel :EDIT THIS
        'wedding-headband' => '头带', // Headband
        'wedding-gloves' => '手套', // Gloves
        'wedding-curd' => '凝乳', // Curd
        'wedding-shawl' => '披肩', // Shawl
        'wedding-belt' => '带', // Belt

    'digital-products' => '数码产品',
        'pc-laptops' => '电脑和笔记本电脑', // PC & Laptops
        'pc-packages' => '电脑包', // PC Packages
        'gaming-laptops' => '游戏笔记本电脑', // Gaming Laptops
        'camera' => '相机', // Camera
        'camera-tripod' => '相机三脚架', // Camera Tripod
        'camera-bag' => '相机包', // Camera Bag
        'micro-camera' => '微型相机', // Micro Camera
        'slr-accessories' => '单反相机配件', // SLR Accessories
        'mobile-phones' => '手机', // Mobile Phones
        'smart-phones' => '智能手机', // Smart Phones
        'basig-phones' => '非智能手机', // Basic Phones :EDIT THIS
        'phone-android' => '安卓', // Android
        'phone-apple' => '苹果', // Apple
        'digital-accessories' => '配件', // Accessories
        'mouse' => '鼠标', // Mouse
        'network-tools' => '网络工具', // Network Tools
        'computer-tables' => '电脑桌', // Computer Tables
        'digital-storage' => '存储', // Storage
        'memory-sd-cards' => '内存/ SD卡', // Memory/SD Cards
        'external-hd' => '外置HD', // External HD

    'home-appliances' => '家用电器',
        'appliances' => '家电', // Appliances
        'flat-screen-tv' => '平板电视', // Flat Screen TV
        'washing-machine' => '洗衣机', // Washing Machine
        'refrigerator' => '冰箱', // Refrigerator
        'gas-stoves' => '燃气灶', // Gas Stoves
        'gas-water-heater' => '燃气热水器', // Gas Water Heater
        'air-conditioning' => '空调', // Air Conditioning
        'living-room' => '客厅', // Living Room
        'audio-cable' => '音频线', // Audio Cable
        'hdmi-cable' => 'HDMI电缆', // HDMI Cable
        'speaker' => '扬声器', // Speaker
        'amplifier' => '放大器', // Amplifier
        'radio' => '无线电', // Radio
        'kitchen' => '厨房', // Kitchen
        'humidifier' => '加湿器', // Humidifier
        'cleaner' => '清洁器', // Cleaner
        'electric-kettle' => '电热水壶', // Electric Kettle
        'cooker' => '炊具', // Cooker
        'purifier' => '净化器', // Purifier
        'telephone' => '电话', // Telephone
        'electric-boxes' => '电箱', // Electric Boxes
        'house-tools' => '工具', // Tools
        'hammer' => '锤子', // Hammer
        'pliers' => '钳', // Pliers
        'wrench' => '扳手', // Wrench

    'beauty-care' => '美容保养',
        'beauty-tools' => '美容工具', // Beauty Tools
        'eyebrow-shaping-device' => '眉毛整形装置', // Eyebrow Shaping Device
        'eyelash-curler' => '睫毛夹', // Eyelash Curler
        'eyelash-brush' => '睫毛刷', // Eyelash Brush
        'false-eyelash' => '假睫毛', // False Eyelash
        'cosmetic-cases' => '化妆品箱', // Cosmetic Cases
        'beaty-appliances' => '美容家电', // Beauty Appliances
        'beautyproducts' => '美容产品', // Beauty Products
        'beauty-sprayer' => '美容喷雾器', // Beauty Sprayer
        'hair-dryer' => '吹风机', // Hair Dryer
        'facelift-yool' => '洁面工具', // Facelift Tool :EDIT THIS
        'barber' => '理发师', // Barber
        'epilator' => '拔毛夹', // Epilator :EDIT THIS
        'nose-hairtrimmer' => '鼻毛修剪器', // Nose Hair Trimmer :EDIT THIS
        'cleansing' => '清洁', // Cleansing
        'shaver' => '剃须刀', // Shaver
        'electric-toothbrush' => '电动牙刷', // Electric Toothbrush
        'scales' => '秤', // Scales
        'footh-bath' => '脚浴', // Footh Bath :EDIT THIS
        'scraping-platters' => '刮盘', // Scraping Platters
        'massage-hammer' => '按摩锤', // Massage Hammer
        'massage-pillow' => '按摩枕头', // Massage Pillow
        'massage-cape' => '按摩靠背', // Massage Cape :EDIT THIS

];
