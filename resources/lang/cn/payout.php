﻿<?php

return [
	'msg-success' => '恭喜！ 6-11天内您将收到您的订单', //Congratulations! You will receive your order in 6-11 days
	'continue-shopping' => '继续购物', //Continue shopping
	'print' => '打印', //Print
	'items-ordered' => '已订购商品', //items ordered.
	'sold-fulfilled' => '销售且履行 ', //Sold and fulfilled by
	'delivered' => '运送', //Delivered by 
	'tax-included' => '总计（含税）', //Total (Tax Included)

];