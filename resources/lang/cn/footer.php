<?php

return [
	'shopping' => '购物', //Shopping
	'all-rights' => '版权所有。', //All Rights Reserved.
	'about' => '关于', //About
	'terms' => '条款和声明', //Terms & Conditions
	'privacy-policy' => '隐私政策', // Privacy Policy
	'faq' => '常见问题', //FAQ
	'site' => '网站地图', //Site Map

	//xy
	'like-fb' => '像我们在Facebook上', //Like us on facebook
	'follow-twit' => '在推特上关注我们', //Follow us on Twitter
];
