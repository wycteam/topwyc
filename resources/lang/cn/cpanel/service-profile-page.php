<?php

return [
	'add-service-profile' => '添加一个新的服务价格订制', // Add Service Profile
	'cancel' => '取消', // Cancel
	'disabled' => '未启用', // Disabled
	'enabled' => '已启用', // Enabled
	'home' => '主页', // Home
	'new-profile-added' => '新的价格订制名以添加', // New Profile Added
	'profile-name' => '服务价格订制名', // Profile Name
	'profile-updated' => '总则以更新', // Profile Updated 
	'regular' => '普通', // Regular
	'save' => '保存', // Save
	'saving-failed' => '保存失败', // Saving Failed
	'service' => '服务', // Service
	'service-name' => '服务名称', // Service Name
	'service-profiles' => '服务列表价格订制版', // Service Profiles
	'status' => '状态', // Status
	'updated-successfully' => '更新成功' // Updated Successfully
];