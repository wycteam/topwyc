<?php

return [
	'action' 				=> '更多选项', // Action 
	'add-new-service'		=> '添加新服务', // Add New Service
	'cancel' 				=> '取消', // Cancel
	'charge' 				=> '收费', // Charge
	'child'					=> '服务细分名称',//'Child',
	'com-amt' 				=> 'Commissionable Amount', //Commissionable Amount
	'cost' 					=> '单据', // Cost
	'description' 			=> '描述', // Description
	'description-chinese' 	=> '中文描述', // Description (Chinese)
	'docs-released'			=> '要发布的文件',//'Documents to be released',
	'edit-service'			=> '编辑服务',//'Edit Service'
	'home' 					=> '主页', // Home
	'list-of-services' 		=> '服务李彪', // List of Services
	'months-required' 		=> '月不能为空', // Months Required
	'new-service-added' 	=> '新的服务以添加', // New Service Added
	'optional-documents' 	=> '可选文件', // Optional Documents
	'optional-docs-released'=> '可选文件将被发布',//'Optional Documents to be released',
	'parent'				=> '服务总称',//'Parent',
	'required-documents' 	=> '必要的文件', // Required Documents
	'save' 					=> '保存', // Save
	'saving-failed' 		=> '保存失败', // Saving Failed
	'service' 				=> '服务', // Service
	'service-charge' 		=> '服务费', // Service Charge
	'service-deleted' 		=> '服务以删除', // Service Deleted
	'service-manager' 		=> '服务管理', // Service Manager
	'service-name' 			=> '服务名称', // Service Name
	'service-name-chinese' 	=> '中文服务名称', // Service Name (Chinese)
	'service-parent' 		=> '服务大类', // Service Parent
	'service-type' 			=> '服务类型', // Service Type
	'service-updated' 		=> '服务已更新', // Service Updated
	'tip' 					=> '其他费用', // Tip
	'yes' 					=> '是', // Yes

	//Sentences
	'are-you-sure-you-want-to-delete-this-service' 	=> '你确定你想要删除这个服务吗?', // Are you sure you want to delete this service? 
	'delete-failed' 								=> '删除失败', // Delete Failed
	'delete-failed-delete-child-services-first' 	=> '删除失败，必须先删除子服务.', // Delete Failed. Delete child services first.
	'delete-service' 								=> '删除服务', // Delete Service
	'no-documents-to-show'							=> '没有文件可显示。',//'No documents to show.',	
	'no-services-to-show' 							=> '没有服务可显示', // No services to show
	'once-deleted-it-will-be-irreversible' 			=> '删除之后将无法恢复', // Once deleted it will be irreversible.
];