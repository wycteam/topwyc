<?php

return [
	'action' 				=> '更多选项', // Action
	'back'					=> '后退',//Back
	'charge' 				=> '收费', // Charge
	'client-number' 		=> '客户编号', // CLIENT NUMBER
	'cost'					=> '单据',//'Cost',
	'date-recorded' 		=> '登记日期', // Date Recorded
	'delete'				=> '删除', //Delete
	'details' 				=> '明细', // Details
	'disable'				=> '未启用',//'Disable',
	'discount' 				=> '折扣', // Discount
	'enable'				=> '激活',//'Enable',
	'extra-cost'			=> '附加费',//'Extra Cost',
	'group-leader'			=> '选择负责人', //Group Leader
	'make-vice-leader'		=> '设定为副团体负责人', //'Make Vice Leader'
	'name' 					=> '名称', // NAME
	'next'					=> '下一个', //next
	'note'					=> '备注',//'Note',
	'package' 				=> '服务包', // Package
	'reason' 				=> '原因', // Reason
	'save'					=> '保存', // Save
	'status' 				=> '状态', // Status
	'total-service-cost' 	=> '总服务费', // TOTAL SERVICE COST
	'transfer'				=> '转移', //Transfer
	'vice-leader'			=> '副组长'//'Vice Leader'
];