<?php

return [
	'action' => '更多选项', // Action
	'add-quick-report' => '添加快捷报告', // Add Quick Report
	'add-reports-to-list-of-clients-below' => '为下列客人添加报告', // Add Reports to List of Clients Below
	'birthdate' => '出生年月', // Birthdate
	'cancel' => '取消', // Cancel
	'charge' => '收费', // Charge
	'client-id' => '客户编号', // Client ID
	'continue' => '继续', // Continue
	'cost' => '单据', // Cost
	'date' => '日期', // Date
	'details' => '明细', // Details
	'discount' => '折扣', // Discount
	'id' => 'ID', // ID
	'name' => '名称', // Name
	'package' => '服务包', // Package
	'please-select-client-first' => '请选择一个客户', // Please select client first
	'please-select-service-first' => '请选择一个服务', // Please select service first
	'remove' => '移除', // Remove
	'save' => '保存', // Save
	'tip' => '其他费用', // Tip
	'write-report' => '编写服务报告' // Write Report
];