<?php

return [
	'amount' 			=> '共计', // Amount
	'balance' 			=> '余额', // Balance
	'current-balance' 	=> '当前余额',//'Current Balance',
	'prev-balance'		=> '前一余额',//'Previous Balance',
	'type' 				=> '类型', // Type
	'current-commission' => '提成当前',
	'prev-commission'    => '上一个提成'
];