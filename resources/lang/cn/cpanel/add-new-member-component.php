<?php

return [
	'add' 		=> '添加', // Add
	'clients' 	=> '客户', // Clients
	'close' 	=> '关闭', // Close

	//Sentences
	'clients-field-is-required' => '客户栏是必填项', // Clients field is required
];