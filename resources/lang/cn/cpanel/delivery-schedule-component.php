<?php

return [
	'action' 		=> '更多选项', // Action
	'add-schedule' 	=> '添加一个计划', // Add Schedule
	'delete' 		=> '删除', // Delete
	'edit' 			=> '编辑', // Edit
	'filter' 		=> '过滤器', // Filter
	'item' 			=> '条目', // Item
	'location' 		=> '位置', // Location
	'rider' 		=> '骑士', // Rider
	'schedule' 		=> '计划', // Schedule
	'today' 		=> '今天', // Today
	'tomorrow' 		=> '明天' // Tomorrow
];