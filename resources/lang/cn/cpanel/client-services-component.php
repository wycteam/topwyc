<?php

return [
	'action' 					=> '更多选项', // ACTION
	'back' 						=> '后退', // Back
	'charge' 					=> '收费', // Charge
	'client-id' 				=> '客户编号', // CLIENT ID
	'continue' 					=> '继续', // Continue
	'cost' 						=> '单据', // Cost
	'date' 						=> '日期', // Date
	'details' 					=> '明细', // Details
	'discount' 					=> '折扣', // Discount
	'name' 						=> '名称', // NAME
	'package' 					=> '服务包', // Package
	'status' 					=> '状态', // Status

	//Sentences
	'please-select-clients' 	=> '请选择客户', // Please select clients
	'please-select-services' 	=> '请选择服务', // Please select services

];