<?php

return [
	'first-expiry' 			=> '入境第一次过期日期', // 1st expiry
	'arrival-date' 			=> '入境日期', // Arrival Date
	'close' 				=> '关闭', // Close
	'complete' 				=> '完成', // Complete
	'cost' 					=> '单据', // Cost
	'disabled' 				=> '未启用', // Disabled
	'discount' 				=> '折扣', // Discount
	'enabled' 				=> '已启用', // Enabled
	'enter'					=> '填写', //'Enter'
	'expiration-date' 		=> '过期日期', // Expiry Date
	'extend-to' 			=> '延期至', // Extend To
	'icard-expiry' 			=> 'ICard过期日期', // ICard expiry
	'icard-issued' 			=> 'ICard签发日期', // ICard Issued
	'note' 					=> '备注', // Note
	'on-process' 			=> '正在办理的包裹', // On Process
	'optional-documents'	=> '可选文件', // Optional Documents
	'pending' 				=> '待办', // Pending
	'reason' 				=> '原因', // Reason
	'required-documents' 	=> '必要的文件', // Required Documents
	'save' 					=> '保存', // Save
	'status' 				=> '状态', // Status
	'tip' 					=> '其他费用', // Tip

	//Sentences
	'please-input-discount-reason' 		=> '请输入打折原因', // Please input discount reason
	'please-select-required-documents'	=> '请选择必须的文件种类', // Please select required documents
	'select-docs'						=> '选择文件',//'Select Documents',
	'service-successfully-updated' 		=> '服务添加成功', // Service successfully updated

];