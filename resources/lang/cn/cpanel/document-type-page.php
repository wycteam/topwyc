<?php

return [
	'action' 					=> '更多选项', // Action
	'add'						=> '添加',//'Add',
	'add-client-document-type' 	=> '添加客户文件种类', // Add Client Document Type
	'cancel' 					=> '取消', // Cancel
	'client-document-type' 		=> '客户文件类型', // Client Document Type
	'client-documents' 			=> '客户文件', // Client Documents
	'client-id' 				=> '客户编号', // Client ID
	'delete'					=> '删除',//'Delete',
	'delete-failed' 			=> '删除失败', // Delete Failed
	'document-type' 			=> '文件种类', // Document Type
	'edit'						=> '编辑',//'Edit',
	'expired-at' 				=> '已过期于', // Expired At
	'home' 						=> '主页', // Home
	'issued-at' 				=> '签发于', // Issued At
	'name' 						=> '名称', // Name
	'save' 						=> '保存', // Save
	'submit' 					=> '提交', // Submit
	

	//Sentences
	'click-to-choose-or-drag-images-to-upload' 	=> '点击选择或者拖拽文件到这里来上传', // Click to choose or drag images to upload
	'images-field-is-required' 					=> '图片栏不能为空', // Images field is required
	'incorrect-filename-format' 				=> '错误的文件名格式', // Incorrect filename format
	'list-of-client-document-types' 			=> '客户文件种类列表', // List of Client Document Types
	'no-client-document-types-to-show' 			=> '没有客户文件种类可显示', // No client document types to show
	'no-documents-to-show' 						=> '没有文件可显示', // No documents to show 
	'service-document-deleted' 					=> '服务文件已删除', // Service Document Deleted
	'you-cant-upload-files-of-this-type' 		=> '你不能上传这种文件格式' // You can't upload files of this type
];