<?php

return [
	'first-expiry'			=> '入境第一次过期日期', // 1st expiry
	'arrival-date' 			=> '入境日期', // Arrival Date
	'charge' 				=> '收费', // Charge
	'complete' 				=> '完成', // Complete
	'close' 				=> '关闭', // Close
	'cost' 					=> '单据', // Cost
	'disabled' 				=> '未启用', // Disabled
	'discount' 				=> '折扣', // Discount
	'docs-released'			=> '要发布的文件',//'Documents to be released',
	'enabled' 				=> '已启用', // Enabled
 	'expiration-date' 		=> '过期日期', // Expiration Date
	'ext-expiry' 			=> '延期后过期日期', // Ext. expiry
	'extend-to' 			=> '延期至', // Extend To
	'extra-cost' 			=> '附加费', // Extra Cost
	'icard-expiry' 			=> 'Icard过期日期', // ICard expiry
	'icard-issued' 			=> 'Icard签发日期', // ICard Issued
	'note' 					=> '备注', // Note
	'on-process' 			=> '正在办理的包裹', // On Process
	'optional-documents' 	=> '可选文件', // Optional Documents
	'optional-docs-released'=> '可选文件将被发布',//'Optional Documents to be released',
	'pending' 				=> '待办', // Pending
	'required-documents' 	=> '必要的文件', // Required Documents
	'reason' 				=> '原因', // Reason
	'save' 					=> '保存', // Save
	'status' 				=> '状态', // Status
	

	//Sentences
	'arrival-date-cannot-be-ahead-from-today' 	=> '到达日期不能是未来日期', // Arrival date cannot be ahead from today
	'no-documents-to-show'						=> '没有文件可显示。',//'No documents to show.',	
	'please-fillup-the-expiration-date' 		=> '请填写过期日期', // Please fillup the expiration date
	'please-input-reason' 						=> '请填写原因', // Please input reason
	'please-select-required-documents' 			=> '请选择必须的文件种类', // Please select required documents
	'successfully-updated' 						=> '成功更新' // Successfully updated

];