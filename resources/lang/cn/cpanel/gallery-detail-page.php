<?php

return [
	'album' 	=> '图册', // Album
	'gallery' 	=> '图库', // Gallery
	'home' 		=> '主页', // Home

	//Sentences
	'drop-files-here-or-click-to-upload' => '拖拽文件到这里或者点击选择文件来上传', // Drop files here or click to upload
];