<?php

return [
	'back' 			=> '后退', // Back
	'charge' 		=> '收费', // Charge
	'client-id' 	=> '客户编号', // Client ID
	'close' 		=> '关闭', // Close
	'complete' 		=> '完成', // Complete
	'continue' 		=> '继续', // Continue
	'cost' 			=> '单据', // Cost
	'date-recorded' => '登记日期', // Date Recorded
	'details' 		=> '明细', // Details
	'disable' 		=> '未启用', // Disable
	'discount' 		=> '折扣', // Discount
	'enable' 		=> '激活', // Enable
	'extra-cost' 	=> '附加费', // Extra Cost
	'name' 			=> '名称', // Name
	'note' 			=> '备注', // Note
	'on-process' 	=> '正在办理的包裹', // On Process
	'package' 		=> '服务包', // Package
	'pending' 		=> '待办', // Pending
	'reason' 		=> '原因', // Reason
	'save' 			=> '保存', // Save
	'status' 		=> '状态', // Status

	//Sentences
	'please-select-at-least-one-client' => 'Please select at least one client', // Please select at least one client
];