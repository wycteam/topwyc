<?php

return [
	'action' 			=> '更多选项', // Action
	'add'				=> '添加',//'Add',
	'add-new-document' 	=> '添加新文件', // Add New Document
	'cancel' 			=> '取消', // Cancel
	'delete'			=> '删除',//'Delete',
	'document-name' 	=> '文件名称', // Document Name
	'documents' 		=> '文件', // Documents
	'edit'				=> '编辑',//'Edit',
	'home' 				=> '主页', // Home
	'list-of-documents' => '文件列表', // List of Documents
	'save' 				=> '保存', // Save
	'saving-failed' 	=> '保存失败', // Saving Failed 
	'service-document'  => '服务文件', // Service Document
	'title' 			=> '标题', // Title
	'title-chinese' 	=> '中文标题', // Title (Chinese)
	'yes' 				=> '是', // Yes

	//Sentences
	'are-you-sure-you-want-to-delete-this-service-document' => '确定要删除这个服务文件吗?', // Are you sure you want to delete this service document?
	'delete-failed' 										=> '删除失败', // Delete Failed 
	'delete-service-document' 								=> '删除服务文件', // Delete Service Document
	'new-service-document-added' 							=> '新的服务文件已添加', // New Service Document Added 
	'no-service-documents-to-show' 							=> '没有服务文件可显示', // No Service Documents to show
	'once-deleted-it-will-be-irreversible' 					=> '删除之后将无法恢复', // Once deleted it will be irreversible
	'service-document-deleted' 								=> '服务文件已删除', // Service Document Deleted
	'service-document-updated' 								=> '服务文件已更新', // Service Document Updated
];