<?php

return [
	'client-id' => '客户编号', // Client ID
	'client-names' => '客户名称', // Client Names
	'collapse-all' => '全部收起', // Collapse All
	'date' => '日期', // Date
	'date-time' => '日期和时间 ', // Date & Time
	'expand-all' => '全部展开', // Expand All
	'filter' => '过滤器', // Filter
	'list-of-reports' => '报告列表', // List of Reports
	'processor' => '受理人', // Processor
	'service' => '服务', // Service
	'service-detail' => '服务明细', // Service Detail
	'status' => '状态', // Status
	'to' => '到', // to
	'total-cost' => '总花费', // Total Cost
	'tracking-no' => '查询编号' // Tracking No
];