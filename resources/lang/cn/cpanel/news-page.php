<?php

return [
	'advanced-settings' => '高级设置', // Advanced Settings
	'allow-comments' => '允许评论', // Allow Comments
	'clear' => '清除', // Clear
	'dont-leave-this-area-empty' => '这个区域不可以空白', // Don\'t leave this area empty
	'featured' => '高亮显示', // Featured
	'featured-image' => '高亮图片', // Featured Image
	'filtered-from' => '过滤于', // filtered from
	'home' => '主页', // Home
	'news' => '新闻', // News
	'news-and-events' => '新闻和实践', // News and Events
	'news-editor' => '新闻编辑器', // News editor
	'next' => '下一个', // Next
	'prev' => '上一个', // Prev
	'private' => '私人的', // Private
	'save' => '保存', // Save
	'title' => '标题', // Title
	'total-entries' => '总计条数', // total entries
	'update' => '更新' // Update
];