<?php

return [
	'gallery' => '图库', // Gallery
	'gallery-title-cannot-be-empty' => 'Gallery title cannot be empty.', // Gallery title cannot be empty.
	'home' => '主页', // 
	'note-gallery-must-have-at-least-5-images-to-be-publish-in-homepage' => 'Note: Gallery must have atleast 5 images to be publish in homepage.', // Note: Gallery must have atleast 5 images to be publish in homepage.
	'publish-to-home' => '发布在主页' // Publish to home
];