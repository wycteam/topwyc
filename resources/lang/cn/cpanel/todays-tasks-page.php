<?php

return [
	'all' => '全部', // All
	'client-id' => '客户编号', // Client ID
	'date' => '日期', // Date
	'estimated-cost' => '预计费用', // Estimated Cost
	'employee' => '员工', // Employee
	'expiration-date' => '过期日期', // Expiration Date
	'filter' => '过滤', // Filter
	'icard-expiration' => 'Icard过期日期', // ICard Expiration
	'load-previous-dates' => '加载前一天', // Load Previous Dates
	'no-more-data' => '没有更多的信息可显示', // No more data
	'no-results' => '没有结果', // No results
	'reminders' => '提醒', // Reminders
	'total-estimated-cost' => '总预计开销', // Total Estimated Cost
	'type' => '类型', // Type
	'tasks' => '任务', // 'Tasks'
	'with-an-estimated-cost-of' => '以及预计可能花费总额' // with an estimated cost of
];