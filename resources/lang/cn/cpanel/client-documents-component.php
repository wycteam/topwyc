<?php

return [
	'add-documents' => '添加文件', // Add Documents
	'choose-file' 	=> '选择文件', // Choose File
	'close' 		=> '关闭', // Close
	'document-type'	=> '文件种类', // Document Type
	'expired-at' 	=> '已过期于', // Expired At
	'issued-at'		=> '签发于', // Issued At
	'remove' 		=> '移除', // Remove

	//Sentences
	'documents-is-required' 				=> '必须选择文件', // Documents is required
	'incorrect-expired-at-field-format' 	=> '错误的过期于格式', // 'Incorrect "Expired At" field format'
	'incorrect-issued-at-field-format'		=> '错误的签发于格式', // 'Incorrect "Issued At" field format'
	'date-expiry-should-ahead-issuance' => '过期时间不能小于签发日期', //Not yet translated Date of expiry should be ahead of issuance date
	'successfully-saved'					=> '原因成功保存', // Successfully saved
	'the-document-type-field-is-required' 	=> '文件种类栏是必填项', // 'The "Document Type" field is required'
	'the-issued-at-field-is-required' 		=> '签发于不能为空', // The "Issued At" field is required
	'you-cant-upload-files-of-this-type' 	=> '你不能上传这种文件格式' // You can\'t upload files of this type
];
