<?php

return [
	'access-control'	=>	'权限控制', // Access Control
	'accts-mngr'		=>	'用户管理', // Accounts Manager
	'advertisements'	=> 	'广告', //'Advertisements'
	'agents'			=>	'代理', // Agents
	'attendance'		=>	'考勤', // Attendance
	'branch-mngr'		=>	'分公司管理员',//'Branch Manager'
	'contacts'			=>	'联系方式', // Contacts
	'cpanel-accts'		=>	'控制面板用户', // CPanel Accounts
	'daily-cost'		=>	'每日开销', // Daily Cost
	'dashboard'			=>	'实时更新墙',  // Dashboard
	'dev-logs'			=>	'开发者文档', // Development Logs
	'docs'				=>	'文件', // Documents
	'employees'			=>	'员工', // Employees
	'financing'			=>	'财务', // Financing
	'gallery'			=>	'图库', // Gallery
	'list-services'		=>	'服务李彪', // List of Services
	'mailbox'			=>	'收件箱', // Mailbox
	'manage-clients'	=>	'个人客户管理', // Manage Clients
	'manage-groups'		=>	'团组客户管理', // Manage Groups
	'news'				=>	'新闻', // News
	'news-events'		=>	'新闻和活动', //'News and Events'
	'profile'			=>	'资料', // Profile
	'read-reports'		=>	'调阅服务报告', // Read Reports
	'reports'			=>	'服务报告', // Reports
	'service-mngr'		=>	'服务管理', // Service Manager
	'service-profile'	=>	'服务列表价格订制版', // Service Profiles
	'shops'				=>	'商店',//'Shops',
	'tracking'			=>	'查询', // Tracking
	'write-reports'		=>	'编写服务报告', // Write Report
	'up-docs'			=>	'已上传文件', // Uploaded Documents
	'client-services' 	=> 	'客户服务',//'Client Services',
	'today-serv'		=> 	"服务数总计",
	'pending-serv'		=> 	'待办业务',
	'on-process-serv'	=> 	'办理中业务',
	'today-task'		=> 	"今天的任务", //"Today's Task"
	'event-calendar' 	=> 	'活动日历',//'Event Calendar'
];
