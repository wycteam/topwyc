<?php

return [
	'action' 			=> '更多选项', // Action
	'add-new-branch' 	=> '添加新的分店', // Add New Branch
	'branch-added' 		=> '分店已添加', // Branch Added
	'branch-manager' 	=> '分店管理', // Branch Manager
	'branch-name' 		=> '分店名称', // Branch Name
	'cancel' 			=> '取消', // Cancel
	'delete-branch' 	=> '删除分店', // Delete Branch
	'delete-failed' 	=> '删除失败', // Delete Failed
	'edit-branch' 		=> '编辑分店', // Edit Branch
	'home' 				=> '主页', // Home
	'id' 				=> 'ID', // ID
	'list-of-branches' 	=> '分店列表', // List of Branches
	'save' 				=> '保存', // Save
	'no-of-clients' 	=> '客户总数', // No. of Clients
	'saving-failed' 	=> '保存失败', // Saving Failed
	'update-failed' 	=> '更新失败', // Update Failed
	'yes' 				=> '是', // Yes

	//Sentences
	'are-you-sure-you-want-to-delete-this-branch'	=> '你确定你想删除这个分店吗?', // Are you sure you want to delete this branch?
	'branch-successfully-deleted' 					=> '分店已成功删除', // Branch successfully deleted
	'branch-successfully-updated' 					=> '分店已成功更新', // Branch successfully updated
];