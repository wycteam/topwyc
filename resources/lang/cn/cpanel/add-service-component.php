<?php

return [
	'charge' 				=> '收费', // Charge
	'click-here' 			=> '点击这里', // click here
	'close' 				=> '关闭', // Close
	'cost' 					=> '单据', // Cost
	'discount' 				=> '折扣', // Discount
	'extra-cost' 			=> '附加费', // Extra Cost
	'note' 					=> '备注', // Note
	'optional-documents' 	=> '可选文件', // Optional Documents
	'reason' 				=> '原因', // Reason
	'required-documents' 	=> '必要的文件', // Required Documents
	'same-day-filing' 		=> '当日提交', // Same Day Filing
	'save' 					=> '保存', // Save
	'service' 				=> '服务', // Service
	
	
	//Sentences
	'check-all-required-documents' 				=> '打钩所有需要的文件种类', // Check all required documents
	'if-you-want-to-view-all-services-at-once' 	=> '如果你想一次性浏览你的所有服务', // If you want to view all services at once
	'please-input-reason' 						=> '请填写原因', // Please input reason
	'please-select-from-required-documents' 	=> '请选择必须的文件种类', // Please select from required documents
	'please-select-service-category' 			=> '请选择服务种类', // Please select service category
];