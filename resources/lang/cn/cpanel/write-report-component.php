<?php

return [
	'action' => '更多选项', // Action
	'add' => '添加', // Add
	'additional-cost-maybe-charged' => '可能产生附加的费用', // Additional cost maybe charged
	'affected-date' => '受影响日期', // Affected Date
	'application-maybe-forfeited-and-has-to-re-apply' => '申请会被认为是无效并且需要重新申请', // Application maybe forfeited and has to re-apply
	'category' => '类别', // Category
	'close' => '关闭', // Close
	'company-courier' => '快递员', // Company Courier
	'consequence-of-non-submission' => '不提交的后果', // Consequence of non submission
	'create-report' => '建立报告', // Create Report
	'date-and-time-delivered' => '日期及时间递送的', // Date and Time Delivered
	'date-and-time-pick-up' => '日期及日期上门取件的', // Date and Time Pick Up
	'date-range' => '日期范围', // Date Range
	'documents' => '文件', // Documents
	'estimated-releasing-date' => '预计办理完成时间', // Estimated Releasing Date
	'estimated-time-of-finishing' => '预计办理完成时间', // Estimated Time of Finishing
	'extended-period-for-processing' => '延长办理时间', // Extended period for processing
	'initial-deposit-maybe-forfeited' => '预付款作废', // Initial deposit maybe forfeited
	'please-fill-up-affected-date' => '请填写受影响日期', // Please fill up affected date
	'please-fill-up-date-and-time-delivered' => '请填写递送日期和时间', // Please fill up date and time delivered
	'please-fill-up-date-and-time-pick-up' => '请填写取件日期和时间', // Please fill up date and time pick up
	'please-fill-up-date-range' => '请填写提起区间', // Please fill up date range
	'please-fill-up-estimated-releasing-date' => '请填写预计办理完成日期', // Please fill up estimated releasing date
	'please-fill-up-estimated-time-of-finishing-date' => '请填写预计完成日期', // Please fill up estimated time of finishing date
	'please-fill-up-extended-period-for-processing' => '请填写延长办理的期限', // Please fill up extended period for processing
	'please-fill-up-submission-date' => '请填写提交日期', // Please fill up submission date
	'please-fill-up-tracking-number' => '请填写单号', // Please fill up tracking number
	'please-select-category' => '请选择种类', // Please select category
	'please-select-company-courier' => '请选择骑士', // Please select company courier
	'please-select-documents' => '请选择文件', // Please select documents
	'scheduled-appointment-date-and-time' => '约见日期和时间', // Scheduled Appointment Date and Time
	'scheduled-hearing-date-and-time' => '面试日期和时间', // Scheduled Hearing Date and Time
	'submission-date' => '提交日期', // Submission Date
	'target-filling-date' => '预计提交日期', // Target Filling Date
	'to' => '到', // to
	'tracking-number' => '单号', // Tracking Number
	'use-custom-category' => '使用定制的类型', // Use Custom Category
	'use-predefined-category' => '使用预置的类型' // Use Predefined Category
];