<?php

return [
	'add' 													=> '添加', // Add
	'amount' 												=> '共计', // Amount
	'balance-transfer' 										=> '转移余额', // Balance Transfer
	'bank' 													=> '银行', // Bank
	'cash' 													=> '现金', // Cash
	'client' 												=> '客户', // Client
	'close' 												=> '关闭', // Close
	'deposit' 												=> '预存款', // Deposit
	'discount' 												=> '折扣', // Discount
	'group' 												=> '团体', // Group
	'option' 												=> '选项', // Option
	'payment' 												=> '已 付款', // Payment
	'reason' 												=> '原因', // Reason
	'refund' 												=> '退款', // Refund
	'storage' 												=> '存储', // Storage
	'transfer-to' 											=> '转移到', // Transfer To

	//Sentences
	'alipay-ref-not-empty'									=> 'Alipay Reference cannot be empty.',//'Alipay Reference cannot be empty.'
	'cannot-transfer-to-same-group-id' 						=> '不能转移到同一个组', // Cannot transfer to same group id
	'please-contact-sir-jason-to-give-discount-to-client' 	=> '请联系Jason来给于折扣', // Please contact sir Jason to give discount to client
	'please-contact-mam-cha-to-give'						=> '请联系Ma\'am Cha来给于',//'Please contact Maam Cha to give',
	'to-client'												=> '到了客户。',//'to client.'
];