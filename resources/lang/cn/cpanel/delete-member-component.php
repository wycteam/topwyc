<?php

return [
	'close'  => '关闭', // Close
	'delete' => '删除', // Delete

	//Sentence
	'are-you-sure-you-want-to-delete-this-member' => '确认要删除这个人吗？', // Are you sure you want to delete this member?
];