<?php

return [
	'back' 					=> '后退', // Back
	'charge' 				=> '收费', // Charge
	'client-id' 			=> '客户编号', // Client ID
	'close' 				=> '关闭', // Close
	'continue' 				=> '继续', // Continue
	'cost' 					=> '单据', // Cost
	'discount' 				=> '折扣', // Discount
	'extra-cost' 			=> '附加费', // Extra Cost
	'generate-new-package' 	=> '生成一个新的服务包', // Generate new package
	'here' 					=> '这里', // here
	'name' 					=> '名称', // Name
	'note' 					=> '备注', // Note
	'optional-documents' 	=> '可选文件', // Optional Documents
	'packages' 				=> 'Packages', // Packages
	'reason' 				=> '原因', // Reason
	'required-documents' 	=> '必要的文件', // Required Documents
	'same-day-filing' 		=> '当日提交', // Same Day Filing
	'save' 					=> '保存', // Save
	'service' 				=> '服务', // Service

	//Sentences
	'check-all-required-documents' 							=> '打钩所有需要的文件种类', // Check all required documents
	'if-you-want-to-view-all-services-at-once-please-click' => '如果你想一次性浏览你的所有服务，请按', // If you want to view all services at once, please click
	'please-select-at-least-one-member' 					=> '请选择至少一个会员', // Please select at least one member
	'please-select-at-least-one-service' 					=> '请选择至少一个服务', // Please select at least one service
	'please-select-from-required-documents' 				=> '不能转移到同一个组', // Please select from required documents
];