<?php

return [
	 'complete'		=> '完成' //'Complete'
	,'empty'		=> '空的' //'Empty'
	,'on-process' 	=> '正在办理的包裹' //'On Process'
	,'package' 		=> '服务包 #' // Package #
	,'pending' 		=> '待办' //'Pending'
];