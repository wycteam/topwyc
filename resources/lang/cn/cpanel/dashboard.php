<?php

// PLEASE INPUT IN ALPHABETICAL ORDER
// PLEASE CHECK 'COMBINED MESSAGES GUIDE' FOR COMPLEX SENTENCES

return [

 'action'						=> '更多选项' //'Action'
,'add-new-delivery-schedule' 	=> '添加新的递送计划' // 'Add New Delivery Schedule'
,'add-sched'					=> '添加一个计划' //'Add schedule'
,'cancel'						=> '取消' // 'Cancel'
,'charge'						=> '收费' //'Charge'
,'client-id'					=> '客户编号' //'Client ID'
,'clients'						=> '客户' //'Client/s'
,'cost'							=> '单据' //'Cost'
,'dashboard'					=> '实时更新墙'//'Dashboard'
,'date'							=> '日期' //'Date'
,'description'					=> '描述' // 'Description'
,'dt'							=> '日期和时间 ' //'Date & Time'
,'delete-failed'				=> '删除失败' // 'Delete Failed'
,'delete-log'					=> '删除记录' // 'Delete Log'
,'del-sched'					=> '递送计划' //'Delivery Schedule'
,'detail'						=> '明细' //'Detail'
,'edit'							=> '编辑' // 'Edit'
,'edit-service'					=> '编辑服务' // 'Edit Service'
,'emp'							=> '员工' //'Employee'
,'est-cost'						=> '计费用' //'Estimated cost'
,'exp-dt'						=> '过期日期' //'Expiry date'
,'filter'						=> '过滤器' //'Filter'
,'grp'							=> '团体' //'Group'
,'icard-exp'					=> 'Icard过期时间' //'Icard Expiration'
,'id'							=> 'ID' //'ID'
,'item'							=> '条目' //'Item'
,'load-prev-date'				=> '加载前一天' //'Load previous dates'
,'loc'							=> '位置' //'Location'
,'name'							=> '名称' //'Name'
,'on-process-serv'				=> '办理中业务' //'On Process Services'
,'pckg'							=> '服务包' //'Package'
,'pending-serv'					=> '待办业务' //'Pending Services'
,'please-put-a-reason'  		=> '请输入原因' // 'Please put a reason'
,'prev-dt-report'				=> '前一天的报告' //'Previous Date report'
,'proceed'						=> '继续' // 'Proceed'
,'reason'						=> '原因' // 'Reason'
,'reminders'					=> '提醒' //'Reminders'
,'rider'						=> '骑士' //'Rider'
,'save'							=> '保存' // 'Save'
,'sched'						=> '计划' //'Schedule'
,'schedule-added'  				=> '计划已添加' // 'Schedule Added'
,'serv'							=> '服务' //'Service'
,'tasks'						=> '任务' //'Tasks'
,'time'							=> '时间' // 'Time'
,'tip'							=> '其他费用' // 'Tip'
,'today'						=> '今天' //'Today'
,'reminders-for-tom'					=> '明日備忘'
,'total-estimated-cost-for-tomorrow'					=> '明天总预计开销'
,'total-estimated-cost-for-today' =>  '今天的总预计开销'
,'today-report'					=> "今天的报告" //"Today's Reports"
,'today-serv'					=> "今天的服务" //"Today's Services"
,'total-today-serv-cost'					=> '今天总服务费'
,'total-yesterday-serv-cost'					=> '昨天总服务费'
,'today-task'					=> "今天的任务" //"Today's Task"
,'tom'							=> '明天' //'Tomorrow'
,'total-client'					=> '全部客户' //'Total Clients'
,'total-est-cost'				=> '总预计开销' //'Total estimated cost'
,'total-serv'					=> '服务数总计' //'Total Services'
,'type'							=> '类型' //'Type'
,'yes'							=> '是' // 'Yes'
,'yesterday'					=> '昨天' //'Yesterday'
,'yesterday-serv'				=> "昨天的服务" //"Yesterday's Services"

//Sentences

,'are-you-sure-you-want-to-delete-this-schedule' 	=> '确认删除这个计划吗?' // 'Are you sure you want to delete this schedule?'
,'failed-all-fields-are-required' 					=> '失败，所有的空白必须都填写' // 'Failed. All Fields are required.'
,'no-data-avail-tbl'								=> '没有内容' //'No data available in table'
,'no-data'											=> '没有更多内容' //'No more data'
,'reason-successfully-saved' 						=> '原因成功保存' // 'Reason successfully saved'
,'remove-this-service-from-the-list' 				=> '是否从列表中移除服务项目?' // 'Remove this service from the list?'
,'schedule-successfully-updated' 					=> '计划已成功更新' // 'Schedule successfully updated'
,'service-successfully-removed' 					=> '服务已成功移除' // 'Service successfully removed'
,'successfully-deleted' 							=> '成功删除' // 'Successfully Deleted'
];
