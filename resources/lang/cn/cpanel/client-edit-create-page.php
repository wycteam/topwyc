<?php

// PLEASE INPUT IN ALPHABETICAL ORDER

return [
	 'select'				=> '选择'//'Select'
	,'valid-until'			=> '有效期至'//'Valid Until'

	 //placeholder
	,'enter-add'	 		=> '输入地址' //'Enter address'
	,'enter-bday'			=> '输入'//'Enter birthday'
	,'enter-cnum'	 		=> '输入电话号码' //'Enter contact number'
	,'enter-email'	 		=> '输入电子邮件地址' //'Enter email address'
	,'enter-fname'	 		=> '输入名' //'Enter first name'
	,'enter-height'	 		=> '输入身高' //'Enter height'
	,'enter-lname'	 		=> '输入姓' //'Enter last name'
	,'enter-mname'	 		=> '输入中间名' //'Enter middle name'
	,'enter-passport'	 	=> '输入护照号码' //'Enter passport'
	,'enter-weight'	 		=> '输入体重' //'Enter weight'

	//gender
	,'male'					=> '男'//'Male'
	,'female'				=> '女'//'Female'

	//civil status
	,'divorced'				=> '离异'//'Divorced'
	,'married'				=> '已婚'//'Married'
	,'separated'			=> '合法分居'//'Separated'
	,'single'				=> '单身'//'Single'
	,'widowed'				=> '寡妇'//'Widowed'

	//Nationality
	,'american'				=>  '美国人'//'American'
	,'british'				=>  '英式'//British
	,'canadian'				=> 	'加拿大人'//'Canadian'
	,'chinese'				=>  '中国人' //'Chinese'
	,'costarricense'		=>  '哥斯达黎加人' //'Costarricense'
	,'cypriot'				=>  '塞浦路斯人' //'Cypriot'
	,'filipino'				=>  '菲律宾人' //'Filipino'
	,'french'				=>	'法文'//'French'
	,'japanese'				=>	'日本人' //'Japanese'
	,'korean'				=>  '韩国人' //'Korean'
	,'latvian'				=>	'拉脱维亚人' //'Latvian'
	,'indian'				=>  '印度人' //'Indian'
	,'indonesian'			=>  '印度尼西亚人' //'Indonesian'
	,'israeli'				=>  '以色列人' //'Israeli
	,'italian'				=> 	'义大利文'//Italian
	,'irish'				=>	'爱尔兰人'//'Irish'
	,'malaysian'			=>  '马来西亚人' //'Malaysian'
	,'salvadoran'			=> 	'萨尔瓦多人'//Salvadoran
	,'swedish'				=>  '瑞典' //'Swedish'
	,'taiwanese'			=>  '台湾人' //'Taiwanese'
	,'thai'				   	=>  '泰人' //'Thai'
	,'ni-vanuatu'			=>	'瓦努阿图人'//'ni-Vanuatu'
	,'vietnamese'			=>  '越南人' //'Vietnamese'


	//Country of Birth
	,'america'				=>  '美国'//'American'
	,'canada'				=> 	'加拿大'//'Canada'
	,'china'				=>  '中国' //'China'
	,'costa-rica'			=>  '哥斯达黎加' //'Costa Rica'
	,'cyprus'				=>  '塞浦路斯' //'Cyprus'
	,'el-salvador'			=>	'萨尔瓦多'//El Salvador	
	,'france'				=> 	'法国'//'France'
	,'india'				=>  '印度' //'India'
	,'indonesia'			=>  '印尼' //'Indonesia'
	,'israel'				=>  '以色列' //'Israeli'
	,'italy'				=> 	'意大利'//Italy
	,'ireland'				=>	'爱尔兰'//'Ireland
	,'japan'				=>	'日本' //'Japan'
	,'korea'				=>  '韩国' //'Korea'
	,'latvia'				=>  '拉脱维亚' //'Latvia'
	,'malaysia'				=>  '马来西亚' //'Malaysia'
	,'philippines'			=>  '菲律宾' //'Philippines'
	,'sweden'				=>  '瑞典' //'Sweden'
	,'thailand'				=>  '泰国' //'Thailand'
	,'taiwan'				=>  '台湾' //'Taiwan'
	,'united-kingdom'		=>  '英国'//United Kingdom
	,'vanuatu'				=>  '瓦努阿图' //'Vanuatu'
	,'vietnam'				=>  '越南' //'Vietnam'

	//sentences
	,'msg-client-create-success'		=>	'成功建立客户。'//'Clients has been created successfully.'
];
