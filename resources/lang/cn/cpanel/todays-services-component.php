<?php

return [
	'action' => '更多选项', // Action
	'charge' => '收费', // Charge
	'cost' => '单据', // Cost
	'date' => '日期', // Date
	'detail' => '明细', // Detail
	'filter' => '过滤器', // Filter
	'id' => 'ID', // ID
	'name' => '名称', // Name
	'package' => '服务包', // Package
	'today' => '今天', // Today
	'yesterday' => '昨天' // Yesterday
];