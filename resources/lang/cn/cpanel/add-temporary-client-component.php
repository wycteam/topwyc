<?php

return [
	'action' 				=> '更多选项', // Action
	'add-contact-number' 	=> '添加联系号码', // Add Contact Number
	'add-details' 			=> '添加明细', // Add Details
	'back' 					=> '后退', // Back
	'binded' 				=> '已绑定', // Binded
	'birthdate' 			=> '出生年月', // Birthdate
	'branch' 				=> '分店', // Branch
	'close' 				=> '关闭', // Close
	'contact-no' 			=> '首要联系号码', // Contact No.
	'contact-number' 		=> '首要联系号码', // Contact Number
	'continue' 				=> '继续', // Continue
	'existing' 				=> '已存在', // Existing
	'female' 				=> '女', // Female
	'first-name' 			=> '名', // First Name
	'gender' 				=> '性别', // Gender
	'group-leader' 			=> '团体负责人', // Group Leader
	'is-invalid' 			=> '是有效的', // is invalid
	'last-name' 			=> '姓', // Last Name
	'male' 					=> '男', // Male
	'no' 					=> '不', // No
	'not-binded' 			=> '未绑定', // Not Binded
	'not-existing' 			=> '未存在', // Not Existing
	'not-group-leader'		=> '不是群组负责人', // Not Group Leader
	'passport' 				=> '护照号码', // Passport
	'proceed-anyway' 		=> '忽略并继续', // Proceed Anyway
	'save' 					=> '保存', // Save
	'select-branch'			=> '选择分公司',//Select Branch	
	'status' 				=> '状态', // Status
	'yes' 					=> '是', // Yes

	//Sentences
	'country-code-is-invalid' 				=> '国家代码无效', // country code is invalid
	'must-be-ten-digit-number' 				=> '必须是10位数字', // must be 10 digit number
	'must-be-eleven-digit-number' 			=> '必须是11位数字'//'must be 11 digit number',
	'the-action-field-is-required' 			=> '行动栏不能为空', // The action field is required
	'the-add-details-field-is-required' 	=> '填写明细栏不能为空', // The add details field is required
	'the-birthdate-field-is-required' 		=> '生日栏不能为空', // The birthdate field is required
	'the-first-name-field-is-required'		=> '名不能为空', // The first name field is required
	'the-gender-field-is-required' 			=> '性别不能为空', // The gender field is required
	'the-last-name-field-is-required' 		=> '姓不能为空', // The last name field is required
	'the-passport-field-is-required' 		=> '护照号码不能为空', // The passport field is required
	'use-existing-profile' 					=> '使用已有的记录', // Use Existing Profile
	'view-existing-profile' 				=> '查看已有记录', // View Existing Profile
];