<?php

return [
	'enter-client-name-cp' 				=> '输入客户名字/手机号码', // Enter Client Name/CP#
	'enter-group-name-leader-name-cp' 	=> '输入群组名称/组长名字/电话', // Enter Group Name/Leader Name/CP#
	'logout' 							=> '登出', // Logout
	'messages' 							=> '信息', // Messages
	'no-new-messages' 					=> '没有新的消息', // No new messages
	'no-new-notifications' 				=> '没有新的通知', // No new notifications
	'notifications' 					=> '通知', // Notifications
	'view-all-messages' 				=> '显示所有信息', // View all messages
	'view-all-notifications' 			=> '查看全部通知', // View all notifications
	'welcome-to-visa-cpanel' 			=> '欢迎您登入签证控制面板' // Welcome to Visa CPanel
];