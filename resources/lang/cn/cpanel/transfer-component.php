<?php

return [
	'back' => '后退', // Back
	'client-to-group' => '个人到团体', // Client to Group
	'complete-transfer' => '完成转移', // Complete Transfer
	'detail' => '明细', // Detail
	'generate-new-package' => '生成一个新的服务包', // Generate new package
	'group-to-client' => '团体到个人', // Group to Client
	'package-no' => '服务包 #', // Package #
	'packages' => '服务包', // Packages
	'please-select-at-least-one-service' => '请选择至少一个服务', // Please select at least one service
	'status' => '状态', // Status
	'tracking' => '查询' // Tracking
];