<?php

return [

	 //Sentences
	 //  'fname-required' 			=>'名不能为空'//'First Name is required.'
	 // ,'lname-required' 			=>'姓不能为空'//'Last Name is required.'
	 // ,'address-required' 		=>'地址不能为空'//'Address is required.'
	 // ,'contact-number-required' =>'首要联系号码不能为空'//'Contact Number is required.'
	 // ,'email-required' 			=>'首要邮件地址不能为空'//'Email is required'
	 // ,'email-email' 			=>'必须是一个有效的E-mail地址。'//'Must be a valid email address.'
	 // ,'email-unique' 			=>'你所提供的邮件地址已经被使用了'//'The email you provided is already used'
	  'agent-required'			=>'请选择一个客户'//'Please select a client.'
	 ,'agent-not-same-midman'	=>'Agent must not be the same as middleman.'//'Agent must not be the same as middleman.'
	 ,'you-added-agent'			=>'You have successfully added new agent.'//'You have successfully added new agent.'
	 ,'client-already-agent'	=>'Client is already an agent.'//'Client is already an agent.'


];