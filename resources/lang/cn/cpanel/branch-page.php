<?php

// PLEASE INPUT IN ALPHABETICAL ORDER
// PLEASE CHECK 'COMBINED MESSAGES GUIDE' FOR COMPLEX SENTENCES

return [

	//LABELS, BUTTONS
	 'add-new-branch'	=>'添加新的分店' //'Add New Branches'
	,'branch-mngr'		=>'分店管理' //'Branch Manager'
	,'del-branch'		=>'删除分店' //'Delete Branch'
	,'list-branch'		=>'分店列表' //'List of Branches'
	,'num-clients'		=>'客户总数' //'No. of Clients'
	,'yes'				=>'是' //'Yes'


	// VALIDATIONS, TOAST & PROMPT MESSAGES
	,'prompt-del-branch'			=>'你确定你想删除这个分店吗?' //'Are you sure you want to delete this branch?'
	,'toast-branch-del-success'		=>'分店已成功删除' //'Branch successfully deleted.'
	,'toast-branch-update-success'	=>'分店已成功更新' //'Branch successfully updated.'
	,'toast-branch-save-fail'		=>'保存失败' //'Saving Failed.'
	,'toast-branch-update-fail'		=>'更新失败' //'Update Failed.'

];