﻿<?php

return [
	'title' => '商品对比', //PRODUCT COMPARISON
	'stock' => '股票', //Stock
	'description' => '描述', //Description
	'add-cart' => '添加到购物车', //Add to cart
	'remove' => '去掉', //Remove
	'no-item' => '在比较中没有任何项目', //There are no items in your comparison
	'are-you-sure' => '您确定要从收藏夹中移除此产品吗？', //Are you sure you want to remove this product?
	'removed' => '从比较中删除！', //Removed from Compare!
	'not-added' => '未添加到购物车。', //Not Added to cart.
	'added' => '加入购物车。' //Added to Cart
];