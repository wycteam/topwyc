﻿<?php

return [

  'header-1'  =>  '生活和工作变得简单。'  , //Life and work made simple.
  'subheader-1' =>  '我们希望通过给您方便的在线购物，与家人度过时光。'  , //We want you to spend time with your family by giving you the convenience of online shopping.
  'about-wataf' =>  '关于WATAF' , //ABOUT WATAF
  'wataf-shop'  =>  'WATAF购物' , //WATAF Shopping
  'desc-1'  =>  '是一家在线商店，所有产品都来自不同的业主。 个别卖家到大公司可以自己开店，提供轻松便捷的购物体验。' , //is an online shop where all products listed coming from different business owners. Individual sellers to big companies can open their own shops to provide effortless and convenient shopping experience.
  'desc-2'  =>  '在这个网站，你会发现无限的选择，满足您的需求和需求。 在一个座位上，您可以完成购物。'  , //In this site, you will find limitless options for your needs and wants. In just one seating you can get your shopping done.
  'social-head' =>  '社会责任感' , //SOCIAL RESPONSIBILITY
  'integ-head'  =>  '廉正' , //INTEGRITY
  'conv-head' =>  '方便'  , //CONVENIENCE
  'social-desc' =>  '作为一家公司，我们希望为每个有抱负的企业家提供机会，从而免费在网站上注册。' , //As a company, we want to give opportunities to each aspiring entrepreneurs,thus, making registration in the website free.
  'integ-desc'  =>  '我们只会为那些准备在工作中坚持透明和诚实的价值观念的组织或个人工作。'  , //We will only work for organizations or individuals that are prepared to uphold the values of transparency and honesty in the course of our work for them.
  'conv-desc' =>  '我们不仅简化了您的购物流程，而且还不断创新，使流程变得快捷方便。'  , //We not only simplify the shopping process for you, but also constantly innovate to make the process quick and easy.
  'header-2'  =>  '为什么选择我们？'  , //WHY CHOOSE US?


];