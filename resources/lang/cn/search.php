﻿<?php
return [
	'per-page' => '每页', //'per page',
	'sort-by' => '排序方式', //'Sort by',
	'title-asc' => '标题A-Z', //'Title A-Z',
	'title-desc' => '标题Z-A', //'Title Z-A',
	'description-asc' => '说明A-Z', //'Description A-Z',
	'description-desc' => '描述Z-A', //'Description Z-A',
	'date-asc' => '最旧最新', //'Date Oldest-Newest',
	'date-desc' => '最新最旧', //'Date Newest-Oldest',
	'ph-title' => '按标题过滤', //'Filter by Title',
	'ph-description' => '按说明过滤', //'Filter by Description',
	'available-items' => '可用项目', //'Available Items'
	'no-results' => '没有找到结果' //No results found

];