﻿<?php

return [
	'favorites' => '收藏', // Favorites
	'cart' => '购物车', // Cart
	'friend-request' => '好友请求', // Friend Request
	'view-all-friend-request' => '查看所有好友请求', // View All Friend Request
	'recent-message' => '最近 (:count)', // Recent (:count)
	'message-requests' => '消息请求', // Message Requests
	'mark-all-read' => '标记全部阅读', // Mark All Read
	'new-message' => '新消息', // New Message
	'view-all-conversation' => '查看所有对话', // View All Conversation
	'notifications' => '通知', // Notifications
	'view-all-notification' => '查看所有通知', // View All Notification
];