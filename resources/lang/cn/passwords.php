﻿<?php
return [
	'password' => '密码必须为六位字符且与密码确认栏位的输入相符。',
	'user'     => '找不到该右键地址锁对应的用户。',
	'token'    => '密码重设码不正确。',
    'reset' => '您的密码已重置！',
    'sent' => '我们已通过电子邮件发送您的密码重设链接！',

    'forgot-password' => '忘记密码', //Forgot Password
    'reset-password' => '重设密码', //Reset Password
    'email-address' => '电子邮件地址', //E-Mail Address
    'send-pw-link' => '发送密码重置链接', //Send Password Reset Link
    'question' => '问题', //Question
    'select-question' => '选择一个问题', //Select a question
    'answer' => '答案', //Answer
    'search-email' => '搜索电子邮件', //Search Email
    'submit' => '提交', //Submit

    'email-error-msg' => '我们找不到具有该电子邮件地址的用户。',// 'We can\'t find a user with that e-mail address.',
    'question-error-msg' => '请选择一个问题。',// 'Please select a question.',
    'answer-error-msg' => '你的答案与选定的问题不符。',// 'You\'re answer does not match the selected question.',
    'alt-email' => '备用电子邮件地址', //Alternate E-Mail Address
    'please-set-alt-email' => '请设置您的备用电子邮件地址。' //Please set your Alternate E-Mail Address.
];