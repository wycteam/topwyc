<?php

return [
	 'successfully-registered' 	=> '成功注册。'//'Successfully Registered.'
	,'you-can-search-wechat' 	=> '请用微信联系我们，你可以点击下面的按钮来复制我们的微信ID然后在微信中搜索我们的账户'//'Please contact us using wechat, after you click the button our wechat ID automatically copied. You can search our ID in wechat app.'
	,'scan-qr-code' 			=> '或者您可以长按下面的二维码来识别以添加我们为好友。'//'or you can use this QR code to view our wechat account.'
];