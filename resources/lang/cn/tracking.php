<?php  
	return [
		//SEARCH PAGE
		 'wyc-tracker' 			=> 'WYC 查询器'//'WYC TRACKER'
		,'enter-tracking'		=> '请输入你的查询编号'//'Enter your tracking No.'
		,'tracking-no'			=> '查询编号'//'Tracking No.'
		,'small-note'			=> '这个功能还处于测试阶段，如果您遇到任何问题，请联系我们。'//'This Page is for beta testing only. Please contact us for feedback and bugs.'
		,'no-result'			=> '没有显示结果。' //'No Results to Show.'

		//RESULT PAGE
		,'name'					=> '名称' //'Name'
		,'client_number'		=> '客户编号' //'Client#'
		,'total_service_cost'	=> '服务费总计' //'Total Service Cost'
		,'date'					=> '日期' //'Date'
		,'packages'				=> '服务包 #' //'Package#'
		,'detail'				=> '明细' //'Detail'
		,'cost'					=> '单据' //'Cost'
		,'charge'				=> '收费' //'Change'
		,'tip'					=> '小费' //'Tip'
		,'discount'				=> '折扣' //'Discount'
		,'group_leader'			=> '团体负责人' //'Group Leader'
		,'reports'				=> '服务报告' //'Reports'
		,'details'				=> '明细' //'Details'
		,'date_time'			=> '日期和时间' //'Date & Time'
		,'close'				=> '合' //'Close'
		,'no_report'			=> '没有报告' //'No Report'
		,'total_package_cost'	=> '总业务办理花费' //'Total Package Cost'
		,'initial_deposit'		=> '总预存款' //'Initial Deposit'
		,'total_payment'		=> '总支付' //'Total Payment'
		,'total_refund'			=> '总找零' //'Total Refund'
		,'total_discount'		=> '总折扣' //'Total Discount'
		,'available_balance'	=> '总可用余额' //'Available Balance'
		,'tracking_no'			=> '查询编号' //'Tracking No.'
		,'service_detail'		=> '服务明细' //'Service Detail'
		,'service_status'		=> '状态' //'Service Status'
		,'date_created'			=> '建立日期' //'Date Created'
		,'service_cost'			=> '总花费' //'Service Cost'
		,'addtl_charge'			=> '其他费用' //'Additional Charge'
		,'service_fee'			=> '服务费' //'Service Fee'
		,'total_cost'			=> '总花费' //'Total Cost'
		,'complete'				=> '完成 ' //'Complete'
		,'empty'				=> '空的文档' //'Empty'
		,'on_process'			=> '办理中' //'On Process'
		,'no_active_service'    => '没有生效的服务。' //'No active service'


	]
?>