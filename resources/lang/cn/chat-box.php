﻿<?php

return [
	'type-a-message' => '输入讯息...', //Type a message...
	'reminder' => '提醒',//Reminder
	'alert-msg' => '为了安全起见，始终在WATAF.ph内进行交易。',//Always transact inside WATAF.ph for safety and security purposes.
	'thank-you' => '谢谢你的聊天 随时给我们留下任何额外的反馈。', //Thank you for the chat. Feel free to leave us any additional feedback.
	'case-solved' => '你的案子解决了吗', //Was your case solved?
	'yes' => '是', //Yes
	'no' => '没有', //No
	'rate-agent' => '你如何评价代理商？', //How would you rate the agent?
	'good' => '好', //Good
	'average' => '平均', //Average
	'bad' => '坏', //Bad
	'please' => '请回答这个问题。', //Please answer this question.
	'comments' => '补充评论', //Additional comments
	'submit' => '提交', //Submit
	'issue' => '此问题已关闭。 感谢您的反馈。', //This issue is closed. Thank you for the feedback.
	'feedback' => '您的反馈非常感谢。' //Your feedback is highly appreciated.
];