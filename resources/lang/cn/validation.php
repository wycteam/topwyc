﻿<?php

return array(

    'alpha_spaces'         => ':attribute 只能包含字母和空格。', //  'The :attribute may only contain letters and spaces.',
    "accepted"         => "必须接受 :attribute。", //  'The :attribute must be accepted.',
    "active_url"       => ":attribute 并非一个有效的网址。", //  'The :attribute is not a valid URL.',
    "after"            => ":attribute 必须要在 :date 之后。", //  'The :attribute must be a date after :date.',
    "alpha"            => ":attribute 只能以字母组成。", //  'The :attribute may only contain letters.',
    "alpha_dash"       => ":attribute 只能以字母、数字及斜线组成。", //  'The :attribute may only contain letters, numbers, and dashes.',
    "alpha_num"        => ":attribute 只能以字母及数字组成。", //  'The :attribute may only contain letters and numbers.',
    'array'            => '的 :attribute 必须是一个数组。', //  'The :attribute must be an array.',
    "before"           => ":attribute 必须要在 :date 之前。", //  'The :attribute must be a date before :date.',
    "between"          => array( 
        "numeric" => ":attribute 必须介于 :min 至 :max 之间。", //  'The :attribute must be between :min and :max.',
        "file"    => ":attribute 必须介于 :min 至 :max kb 之间。 ", //  'The :attribute must be between :min and :max kilobytes.',
        "string"  => ":attribute 必须介于 :min 至 :max 个字符之间。", //  'The :attribute must be between :min and :max characters.',
        'array'   => '的 :attribute 必须有:min和:max项目之间。', //  'The :attribute must have between :min and :max items.',
    ),
    'boolean'          => 'The :attribute field must be true or false.', //  'The :attribute field must be true or false.',
    "confirmed"        => ":attribute 与确认栏位的输入并不相符。", //  'The :attribute confirmation does not match.',
    "date"             => ":attribute 並非一个有效的日期。", //  'The :attribute is not a valid date.',
    "date_format"      => ":attribute 与 :format 格式不相符。", //  'The :attribute does not match the format :format.',
    "different"        => ":attribute 与 :other 必须不同。", //  'The :attribute and :other must be different.',
    "digits"           => ":attribute 必须是 :digits 位数字。", //  'The :attribute must be :digits digits.',
    "digits_between"   => ":attribute 必须介于:min 至 :max 位数字。", //  'The :attribute must be between :min and :max digits.',
    "email"            => ":attribute 的格式无效。", //  'The :attribute must be a valid email address.',
    "exists"           => "所揀選的 :attribute 選項無效。", //  'The selected :attribute is invalid.',
    'filled'           => 'The :attribute field is required.', //  'The :attribute field is required.',
    "image"            => ":attribute 必須是一張圖片。", //  'The :attribute must be an image.',
    "in"               => "所揀選的 :attribute 選項無效。", //  'The selected :attribute is invalid.',
    "integer"          => ":attribute 必須是一個整數。", //  'The :attribute must be an integer.',
    "ip"               => ":attribute 必須是一個有效的 IP 地址。", //  'The :attribute must be a valid IP address.',
    'json'             => '的 :attribute 必须是有效的JSON字符串。', //  'The :attribute must be a valid JSON string.',
    "max"              => array( 
        "numeric" => ":attribute 不能大於 :max。", //  'The :attribute may not be greater than :max.',
        "file"    => ":attribute 不能大於 :max kb。", //  'The :attribute may not be greater than :max kilobytes.',
        "string"  => ":attribute 不能多於 :max 個字符。", //  'The :attribute may not be greater than :max characters.',
        'array'   => ':attribute 可能没有更多:max 项目。', //  'The :attribute may not have more than :max items.',

    ),
    "mimes"            => ":attribute 必須為 :values 的檔案。", //  'The :attribute must be a file of type: :values.',
    "min"              => array( 
        "numeric" => ":attribute 不能小於 :min。", //  'The :attribute must be at least :min.',
        "file"    => ":attribute 不能小於 :min kb。", //  'The :attribute must be at least :min kilobytes.',
        "string"  => ":attribute 不能小於 :min 個字符。", //  'The :attribute must be at least :min characters.',
        'array'   => ':attribute 必须至少有:min项目。', //  'The :attribute must have at least :min items.',
    ),
    "not_in"           => "所揀選的 :attribute 選項無效。", //  'The selected :attribute is invalid.',
    "numeric"          => ":attribute 必須為一個數字。", //  'The :attribute must be a number.',
    "regex"            => ":attribute 的格式錯誤。", //  'The :attribute format is invalid.',
    "required"         => ":attribute 不能留空。", //  'The :attribute field is required.',
    "required_if"      => "當 :other 是 :value 時 :attribute 不能留空。", //  'The :attribute field is required when :other is :value.',
    "required_with"    => "當 :values 出現時 :attribute 不能留空。", //  'The :attribute field is required when :values is present.',
    'required_with_all'    => ':attribute当有以下情况需要输入字段:values。', //  'The :attribute field is required when :values is present.',
    "required_without" => "當 :values 留空時 :attribute field 不能留空。", //  'The :attribute field is required when :values is not present.',
    'required_without_all' => ':attribute当没有：values存在时，需要字段。', //  'The :attribute field is required when none of :values are present.',
    "same"             => ":attribute 與 :other 必須相同。", //  'The :attribute and :other must match.',
    "size"             => array( 
        "numeric" => ":attribute 的大小必須是 :size。", //  'The :attribute must be :size.',
        "file"    => ":attribute 的大小必須是 :size kb。", //  'The :attribute must be :size kilobytes.',
        "string"  => ":attribute 必須是 :size 個字符。", //  'The :attribute must be :size characters.',
        'array'   => 'The :attribute必须包含：size项目。', //  'The :attribute must contain :size items.',
    ),
    'string'           => ':attribute 必须是一个字符串。', //  'The :attribute must be a string.',
    'timezone'         => ':attribute 必须是有效的区域。', //  'The :attribute must be a valid zone.',
    "unique"           => ":attribute 已經用了。", //  'The :attribute has already been taken.',
    "url"              => ":attribute 的格式錯鋘。", //  'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [ 
        'attribute-name' => [ 
            'rule-name' => 'custom-message', //  'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [], 

);