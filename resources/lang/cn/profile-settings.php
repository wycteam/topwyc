﻿<?php

return [
	'store'	=>	'商店'	,	//Stores
	'wishlist'	=>	'收藏'	,	//Wishlist
	'ewallet'	=>	'电子钱包'	,	//E-wallet
	'acct-info'	=>	'账户信息'	,	//Account Information
	'contact-details'	=>	'账户信息'	,	//Contact Details
	'sec-settings'	=>	'安全设定'	,	//Security Settings
	'docs'	=>	'文件上传'	,	//Documents
	'store-mgmt'	=>	'商店管理'	,	//Store Management
	'my-purchase'	=>	'我的购买'	,	//My Purchase
	'my-sales'	=>	'我的销售'	,	//My Sales
	'noti'	=>	'通知'	,	//Notifications
	'upload-dp'	=>	'上传显示图片'	,	//Upload Display Picture
	'cont-cs-name-msg'	=>	'您需要联系我们的客户服务部来更改您的姓名。'	,	//You need to contact our Customer Service to change your name.
	'email'	=>	'电子邮件地址'	,	//E-mail Address
	'sec-email'	=>	'子电子邮'	,	//Secondary E-mail
	'bday'	=>	'生日'	,	//Birthday
	'gender'	=>	'性别'	,	//Gender
	'female'	=>	'女'	,	//Female
	'male'	=>	'男'	,	//Male
	'nickname'	=>	'昵称'	,	//Nickname
	'set-nickname'	=>	'设置为显示名称'	,	//Set as display name
	'save'	=>	'保存'	,	//SAVE
	'com-add'	=>	'完整地址'	,	//Complete Address
	'province'	=>	'省'	,	//Province
	'brgy'	=>	'马兰'	,	//Barangay
	'city'	=>	'市'	,	//City
	'zip-code'	=>	'邮政编码'	,	//Zip Code
	'area-code'	=>	'区号'	,	//Area Code
	'landline'	=>	'座机'	,	//Landline
	'mobile'	=>	'移动'	,	//Mobile
	'landmark'	=>	'最近的地标'	,	//Nearest Landmark
	'sec-lvl'	=>	'安全级别'	,	//Security Level
	'high'	=>	'高'	,	//HIGH
	'med'	=>	'中'	,	//MED
	'low'	=>	'低'	,	//LOW
	'login-pw'	=>	'登录密码'	,	//Login Password
	'strong-pw-msg'	=>	'强密码可以使帐户更安全。 我们建议您定期更改密码，并包含一组数字和字母，以及六个或更多个密码的长度。'	,	//Strong password can make the account more secure. We recommend that you change your password regularly and contains a set of numbers and letters, and the length of more than six or more passwords.
	'change'	=>	'更改'	,	//CHANGE
	'old-pw'	=>	'旧密码'	,	//Old Password
	'new-pw'	=>	'新密码'	,	//New Password
	'conf-pw'	=>	'确认密码'	,	//Confirm Password
	'sec-que'	=>	'安全问题'	,	//Security Questions
	'sec-que-msg'	=>	'恢复密码的一种方法 我们建议您设置一个易于记住的问题，最困难的问题和答案是由其他人更有效地保护您的密码安全。'	,	//One way you recover your password. We recommend that you set up an easy to remember, and the most difficult questions and answers are acquired by others more effectively protect your password security.
	'not-set'	=>	'还没设'	,	//Not Yet Set
	'is-set' => '设置', //Is set
	'set-btn' => '定', //set
	'select-que'	=>	'选择问题'	,	//Select Question
	'ans'	=>	'回答'	,	//Answer
	'ans-on-file'	=>	'[文件回答]'	,	//[Answer on File]
	'see-que'	=>	'查看问题'	,	//SEE QUESTIONS
	'acct-sec-msg'	=>	'恭喜！ 您的帐户安全可靠。'	,	//Congratulations! Your account is well secured.
	'ind-acct'	=>	'个人账户'	,	//INDIVIDUAL ACCOUNT
	'nbi'	=>	'NBI'	,	//NBI
	'upload-nbi-tp'	=>	'上传您的有效NBI'	,	//Upload your valid NBI
	'birth-cert'	=>	'出生证明'	,	//Birth Certificate
	'gov-id'	=>	'政府编号'	,	//Government ID
	'exp-at'	=>	'到期时间为'	,	//Expires at <date>
	'exp-on'	=>	'到期时间为'	,	//Expires at <date>
	'verified'	=>	'验证'	,	//Verified 
	'pending'	=>	'有待'	,	//Pending
	'denied'	=>	'被拒绝'	,	//Denied
	'upload'	=>	'上传'	,	//Upload
	'view'	=>	'视图'	,	//View
	'download'	=>	'下载'	,	//Download
	'ent-acct'	=>	'企业账户'	,	//ENTERPRISE ACCOUNT
	'sec'	=>	'SEC注册'	,	//SEC Registration
	'bir'	=>	'BIR注册'	,	//BIR Registation 
	'upload-bir-tp'	=>	'上传有效的BIR注册'	,	//Upload your valid BIR Registration
	'bp'	=>	'营业执照'	,	//Business Permit
	'upload-govid-tp'	=>	'上传菲律宾政府发行的任何身份证件'	,	//Upload any ID issued by the Philippine Government
	'upload-bc-tp'	=>	'上传有效的出生证明'	,	//Upload your valid Birth Certificate
	'upload-sec-tp'	=>	'上传有效的SEC注册'	,	//Upload your valid SEC Registration
	'upload-bp-tp'	=>	'上传有效的营业执照'	,	//Upload a valid Business Permit
	'rating'	=>	'评级'	,	//Ratings
	'edit-store'	=>	'编辑商店'	,	//Edit Store
	'del-store'	=>	'删除商店'	,	//Delete Store
	'profile'	=>	'轮廓'	,	//Profile
	'ver-user'	=> '已验证用户',	//Verified User
	'not-ver'	=> '尚未验证',	//Not yet Verified
	'menu'	=>  '菜单',	//Menu
	'email-msg'	=> '您不能更改您的电子邮件地址。'	, //You cannot change your Email Address.
	'field-required'	=> '这是必填栏',	//This field is required

	'sec-doc-msg-1'	=> '请'	, //Please answer security questions and upload documents to improve security.
	'sec-doc-msg-2'	=> '回答安全问题'	, //Please answer security questions and upload documents to improve security.
	'sec-doc-msg-3'	=> '和'	, //Please answer security questions and upload documents to improve security.
	'sec-doc-msg-4'	=> '上传文件'	, //Please answer security questions and upload documents to improve security.
	'sec-doc-msg-5'	=> '以提高安全性。'	, //Please answer security questions and upload documents to improve security.

	'no-noti' => '没有通知。', //No Notifications.
	'prof-updated-msg'	=>	'你的个人资料已经更新。'	,	//Your profile has been updated.
	'update-failed'	=>	'更新失败。'	,	//Update failed.
	'pw-changed-msg'	=>	'您的密码已被更改。'	,	//Your password has been changed.
	'saved-msg'	=>	'成功保存'	,	//Successfully saved.
	'upload'	=>	'上传'	,	//Upload
	'succ-upload'	=>	'成功上传！'	,	//Successfully Uploaded!
	'try-again'	=>	'失败。 请再试一次'	,	//Failed. Please try again

	'acct-not-ver-1'	=>	'您的帐户尚未验证。 请注意，您可以创建商店和产品，但以下功能被禁用：'	,	//Your Account is not yet verified. Please be informed that you can create stores and products but the following features are disabled: 
	'acct-not-ver-2'	=>	'*您不能销售您的产品（商店用户可以看到您的产品，但不能添加到他们的购物车）'	,	//* You cannot sell your products (Shop Users can see your product but cannot add to their shopping cart)
	'acct-not-ver-3'	=>	'*您无法回复客户的意见'	,	//* You cannot reply on customer's feedback 
	'acct-not-ver-4'	=>	'*客户无法向您发送信息'	,	//* Customers cannot send a message to you 
	'acct-not-ver-7'	=>  '注意：文件验证过程通常需要1-2个小时。',//Note : Documents verification process usually takes 1-2 hours.
	'acct-not-ver-5'	=>	'要验证您的账户，请在这里上传相应的文件。'	,	//To verify your account, please upload documents here.
	'acct-not-ver-6'	=>	'这里。'	,	//To verify your account, please upload documents here.
	'province' => '省是必需的。', //Province is required.
	'city' => '城市是必需的。' //City is required.
];
