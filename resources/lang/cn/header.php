﻿<?php

return [
	'login' => '登录', //Login
	'email' => '电子邮件', //Email
	'remember' => '记住我', //Remember Me
	'password' => '密码', //Password
	'forgot' => '忘记密码了吗？', //Forgot Your Password?
	'dont-have-account' => '没有帐号？', //Don't have an account?
	'sign-up' => '注册', //Sign Up
	'favorites' => '收藏', //Wishlist
	'compare' => '比较', //Compare
	'friend-req' => '朋友请求', //Friend Requests
	'sent' => '给你一个朋友的请求', //Sent you a friend request
	'see-all' => '查看全部', //See all
	'recent' => '最近的消息', //Recent Messages
	'view-all-msg' => '查看所有消息', //View all messages
	'noti' => '通知', //Notifications
	'view-all-noti' => '查看所有通知', //View all notification
	'profile' => '个人资料', //Profile
	'store-manage' => '商店管理', //Store Management
	'purchase-report' => '采购报告', //Purchase Report
	'sales-report' => '销售报告', //Sales Report
	'e-wallet' => '电子钱包', //E-Wallet
	'usable' => '可用', //Usable
	'feedback' => '投诉建议', //Feedback
	'logout' => '登出', //Logout
	'search' => '搜索', //Search
	'friend-request' => '好友请求', //Friend Request
	'messages' => '消息'	, //Messages
	'notifications' => '通知', //Notifications
	'new-products' => '新产品', //New Products
	'view-all-cat' => '查看所有分类', //View All Categories
	'tickets' => '门票', // Tickets link
	'add-friend' => '添加好友' // Add Friend

	//visa
	,'wyc-group'		=> 'WYC 集团' //'WYC GROUP'
	,'home'				=> '首页' //'Home'
	,'services'			=> '服务项目' //'Services'
	,'tracking'			=> '查询' //'Tracking'
	,'contact-us'		=> '联系我们' //'Contact Us'
	,'english'			=> '英语' //'English'
	,'mandarin'			=> '中文' //'Mandarin'
	,'change-language'	=> '改变语言' //'Change Language'
];