﻿<?php
return [
	'page-title'		=>'创建一个商店', //  'CREATE A STORE',
	'page-title-2' 		=> '编辑您的商店', // 'EDIT YOUR STORE'
	'page-subheader'	=>'所有输入的内容都不应该包含任何冒犯或带诱惑性的词,如“性”或其他敏感词。<br>（我们预设了一个词库来检测敏感词,来决定是否批准你的申请。）', //  'All of the context should not contain any offensive or seductive words like sex or other sensitive words.<br>
	'note'				=>'备注', //  'Note',
	'store-details'		=>'商店详细信息', //  'STORE DETAILS',
	'store-name'		=>'商店名称', //  'Store Name',
	'store-owner'		=>'店主', //  'Store Owner',
	'select-location'	=>'选择位置...', //  'Select Location',
	'guide-provinces'	=>'如果你想一次查看所有省份,请点击', //  'If you want to view all provinces all at once, please click',
	'here'				=>'这里', //  'here',
	'store-address'		=>'商店地址', //  'Store Address',
	'select-cat'		=>'选择类别', //  'Select Category',
	'guide-cat'			=>'请选择最多4个类别。如果您想一次查看所有可用的类别,请点击', //  'Please select maximum of 4 categories. If you want to view all available categories all at once, please click',
	'custom-cat'		=>'自定义类别', //  'Custom Category',
	'contact-info'		=>'联系信息', //  'CONTACT INFORMATION',
	'company-name'		=>'公司名称', //  'Company Name',
	'company-loc'		=>'公司位置', //  'Company Location',
	'mobile-no'			=>'手机号码', //  'Mobile No.',
	'email'				=>'电子邮件', //  'E-mail',
	'store-desc'		=>'商店说明', //  'Store Description',
	'ph-store-desc' 	=> '写一下关于您商店的事情',// 'Tell something about your store',
	'next'				=>'下一个', //  'Next',
	'save'				=>'保存' //  'Save'
];

