﻿<?php

return [
	
	'profile-settings' => '账户管理', // Profile Settings :EDIT THIS
	'store-management' => '商店管理', // Store Management
	'chat-history' => '聊天记录', // Chat History
	'e-wallet' => '电子钱包', // E-Wallet
	'sales-report' => '销售报告', // Sales Report
	'purchase-report' => '购物报告', // Purchase Report
	'customer-support' => '客户服务', // Customer Support
	'feedback' => '反馈', // Feedback
	'logout' => '登出', // Logout

];