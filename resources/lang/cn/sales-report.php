﻿<?php

//PLEASE READ THE COMMENTS
return [
	'sales-report-title'	=>	'销售报告'	,	//Sales Report
	'no-stores-msg'	=>	'你还没有商店。'	,	//You don't have stores yet.
	'pending-order'	=>	'待定订单'	,	//Pending Orders
	'delivered'	=>	'交付'	,	//Delivered
	'cancelled-order'	=>	'已取消订单'	,	//Cancelled Orders
	'in-transit'	=>	'在途中'	,	//In Transit
	'all'	=>	'所有'	,	//All
	'date-time'	=>	'日期和时间'	,	//Date and Time
	'product'	=>	'产品'	,	//Product
	'shipping-fee' => '运输费用', //Shipping Fee
	'total'	=>	'总'	,	//Total
	'status'	=>	'状态'	,	//Status
	'see-more'	=>	'查看更多'	,	//See More
	'no-pending-order-msg'	=>	'恭喜，您没有待处理的订单。'	,	//Congratulations, you have no pending orders.
	'no-cancelled-order-msg'	=>	'您没有取消订单。'	,	//You don't have cancelled orders.
	'no-intransit-msg'	=>	'路上没有订单。'	,	//No orders on its way.
	'no-purchases-msg'	=>	'没有购买。'	,	//No purchases yet.
	'no-delivered-msg'	=>	'您还没有交付订单。'	,	//You don't have delivered orders yet.
	'in-transit'	=>	'在途中'	,	//In Transit
	'cancelled'	=>	'取消'	,	//Cancelled
	'pending'	=>	'有待'	,	//Pending
	'received'	=>	'收到'	,	//Received
	'delivered'	=>	'交付'	,	//Delivered
	'confirm'	=>	'确认'	,	//Confirm
	'received-returned-msg-1'	=>	'你收到退货了吗？'	,	//Did you receive the returned item?
	'saved-msg'	=>	'成功保存。'	,	//Successfully saved.
	'update-failed-msg'	=>	'更新失败。'	,	//Update Failed.
	'mark-pending-msg'	=>	'您确定要将此订单标记为待处理？'	,	//Are you sure you want to mark this order as pending?
	'received-returned-msg-2'	=>	'收到退货项目？'	,	//Received returned item?
	'placed-by'	=>	'放置于'	,	//Placed By
	'placed-on'	=>	'放在'	,	//Placed On
	'shipped-to'	=>	'运到'	,	//Shipped To
	'ship-address'	=>	'邮寄地址'	,	//Shipping Address
	'nearest-landmark'	=>	'最近的地标'	,	//Nearest Landmark
	'contact-no'	=>	'联系电话'	,	//Contact Number
	'alt-contact-no'	=>	'备用联系电话'	,	//Alternate Contact Number
	'buyer-remarks'	=>	'买方备注'	,	//Buyer Remarks
	'owner'	=>	'所有者'	,	//Owner
	'store-name'	=>	'商店名称'	, //Store Name
	'store-address'	=>	'商店地址'	, //Store Address
	'total-summary'	=>	'总摘要'	,	//Total Summary
	'unit-price'	=>	'单价'	,	//Unit Price
	'you-save'	=>	'您保存'	,	//You save
	'now'	=>	'现在'	,	//Now
	'qty'	=>	'数量'	,	//Quantity
	'order-notes'	=>	'订单说明'	,	//Order Notes
	'order-notes-msg'	=>	'您可以按照订单将您的笔记或备注放在这里。'	,	//You can place your notes or remarks here per order.
	'return-videos'	=>	'买家视频'	,	//Videos from the Buyer
	'browser-no-html-support'	=>	'您的浏览器不支持HTML5视频。'	,	//Your browser does not support HTML5 video.
	'return-image'	=>	'买家图片'	,	//Images from the Buyer
	'qualifications'	=>	'资格'	,	//Qualifications
	'what-in-box'	=>	'盒子里有什么东西'	,	//What's in the box
	'brand'	=>	'牌'	,	//Brand
	'weight'	=>	'重量'	,	//Weight
	'tracking-number'	=>	'追踪号码'	,	//Tracking Number
	'stores'	=>	'商店'	,	//Stores
	'order-details'	=>	'订单详细信息'	,	//Order Details
	'returned'	=>	'返回'	,	//Returned
	'delivered-on'	=>	'送货到达'	,	//Delivered on
	'received-on-1'	=>	'于'	,	//Received on <received date> *use on between dates; example: 于7/8/17收到 
	'received-on-2'	=>	'收到'	,	//Received on <received date> *use on between dates; example: 于7/8/17收到 
	'in-transit-on-1'	=>	'在'		,	//In Transit on <received date> *use on between dates; example: 在/8/17过境
	'in-transit-on-2'	=>	'已递送'	,	//In Transit on <received date> *use on between dates; example: 在/8/17过境
	'discount' => '折扣', //Discount
	'subtotal' => '小计', //Subtotal
	'products' => '制品', // Products
	'total-price-including-vat' => '总价格包括增值税', // Total Price including VAT

	'return-msg-1'	=>	'此订单已于'	,	//This order has been returned on <date> with Case No.  <case number>
	'return-msg-2'	=>	'返回，案号为'	,	//This order has been returned on <date> with Case No.  <case number>

	'reason-return'	=>	'回归原因'	,	//Reason for Returning

	'contact-customer-support-1'	=>	'您可以联系我们的'	,	//You may contact our <a> Customer Support </a> regarding this matter.
	'contact-customer-support-2'	=>	'客户支持'	,	//You may contact our <a> Customer Support </a> regarding this matter.
	'contact-customer-support-3'	=>	'关于此事。'	,	//You may contact our <a> Customer Support </a> regarding this matter.

	'ship-your-item'	=>	'运送您的物品'	,	//Ship Your Item
	'shipment-type'	=>	'选择装运类型'	,	//Select Shipment Type
	'drop-off'	=>	'放下'	,	//Drop Off
	'pick-up'	=>	'捡起'	,	//Pick Up
	'required-shipment'	=>	'装运类型是必需的。'	,	//Shipment Type is required.
	'select-courier'	=>	'选择快递'	,	//Select Courier
	'required-courier'	=>	'快递是必需的'	,	//Courier is required
	'required-track-no'	=>	'需要跟踪号码。'	,	//Tracking number is required.
	'submit'	=>	'提交'	,	//Submit
	'cancel-order'	=>	'取消订单'	,	//Cancel Order
	'required-reason'	=>	'原因是必需的。'	,	//Reason is required
	'yes'	=>	'是'	,	//Yes
	'updated-note-msg'	=>	'您的笔记已更新'	,	//Your note has been updated
	'color' => '颜色', //Color
	'size' => '尺寸', //Size
	'shipchar' => '运费和估价费', //Shipping Fee & Valuation Charge
	'vat' => '增值税', //VAT

	'transaction' => '交易', //Transaction
	'pending-orders' => '挂单/ s', //pending order/s

	//airwaybill
	'airway-bill' => '航空运单', //Airway Bill 
	'deliver-to' => '交付给', //DELIVER TO
	'area-code' => '区号', //Area Code
	'collected' => 'COD数量', //Amount to be collected COD
	'awb' => 'AWB号。', //AWB No.
	'dimensions' => '尺寸（厘米）', //Dimensions (cms)
	'order-date' => '订购日期', //Order Date
	'pieces' => '件', //Pieces
	'package-type' => '包装类型', //Package Type
	'sr' => '高级。', //Sr.
	'item-code' => '项目代码', //Item Code
	'item-desc' => '商品描述', //Item Description
	'quantity' => '数量', //Quantity
	'value' => '值', //Value
	'inclusive' => '包含所有税费和运费', //Inclusive of all taxes and shipping charges
	'received-by' => '收到', //Received by
	'printed' => '打印姓名和签名', //Printed Name and Signature
	'relationship' => '关系', //Relationship
	'date-received' => '接收日期', //Date Received
	'attempt' => '尝试原因', //Attempt Reason
	'1st' => '1', //1st
	'2nd' => '第2', //2nd
	'3rd' => '第3', //3rd
	'print' => '打印', //Print
	'shipchar' => '运费和估价费', //Shipping Fee & Valuation Charge
	'close'	=>	'关'	,	//Close

	'photo-evidence' => '照片证明', // PHOTO EVIDENCE
	'video-evidence' => '视频证据' // VIDEO EVIDENCE
];
