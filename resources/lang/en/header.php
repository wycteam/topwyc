﻿<?php

return [
	'login' => 'Login',
	'email' => 'Email',
	'password' => 'Password',
	'remember' => 'Remember Me',
	'forgot' => 'Forgot Your Password?',
	'dont-have-account' => "Don't have an account?",
	'sign-up' => 'Sign Up',
	'favorites' => 'Wishlist',
	'compare' => 'Compare',
	'friend-req' => 'Friend Requests',
	'sent' => 'Sent you a friend request',
	'see-all' => 'See all',
	'recent' => 'Recent Messages',
	'view-all-msg' => 'View all messages',
	'noti' => 'Notifications',
	'view-all-noti' => 'View all notification',
	'profile' => 'Profile',
	'store-manage' => 'Store Management',
	'purchase-report' => 'Purchase Report',
	'sales-report' => 'Sales Report',
	'e-wallet' => 'E-Wallet',
	'usable' => 'Usable',
	'feedback' => 'Feedback',
	'logout' => 'Logout',
	'search' => 'Search',
	'friend-request'	=>	'Friend Requests'	,
	'messages'	=>	'Messages'	,
	'notifications'	=>	'Notifications'	,
	'new-products' => 'New Products',
	'view-all-cat' => 'View All Categories',
	'tickets' => 'Tickets',
	'add-friend' => 'Add Friend'

	//visa
	,'wyc-group'		=> 'WYC GROUP'
	,'home'				=> 'Home'
	,'services'			=> 'Services'
	,'tracking'			=> 'Tracking'
	,'contact-us'		=> 'Contact Us'
	,'english'			=> 'English'
	,'mandarin'			=> 'Mandarin'
	,'change-language'	=> 'Change Language'
];