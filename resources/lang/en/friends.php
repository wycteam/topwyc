<?php

return [
	'friends' 				=> 'Friends', // Friends
	'all' 					=> 'All Friends', // All Friends
	'requests' 				=> 'Friend Requests', // Friend Requests
	'add' 					=> 'Add Friend', // Add Friends
	'start-adding' 			=> 'Start Adding Friends', // Start Adding Friends
	'unfriend' 				=> 'Unfriend', // Unfriend
	'unfriend-confirmation' => 'Are you sure you want to unfriend', // Are you sure you want to unfriend
	'no-request' 			=> 'No Friend Requests', // No Friend Requests
	
	//new
	'find-people' 			=> 'Find People',
	'no-friends' 			=> 'You don\'t have friends yet. Add someone ...',
	'search' 				=> 'Search..',
	'type-user' 			=> 'Type a name of a user',
	'result' 				=> 'Results',
	'registered' 			=> 'Registered',
	'recommended' 			=> 'Recommended',
	'no-recommendations' 	=> 'There are no friend recommendations..',
	'send-message' 			=> 'Send Message', // Send Message
	'accept'				=> 'accept', //accept
	'reject'				=> 'Reject', //Reject
];
