<?php

return [
	'sales-report-title'	=>	'SALES REPORT'	,
	'no-stores-msg'	=>	'You don\'t have stores yet.'	,
	'pending-order'	=>	'Pending Orders'	,
	'delivered'	=>	'Delivered'	,
	'cancelled-order'	=>	'Cancelled Orders'	,
	'in-transit'	=>	'In Transit'	,
	'all'	=>	'All'	,
	'date-time'	=>	'Date and Time'	,
	'product'	=>	'Product'	,
	'shipping-fee' => 'Shipping Fee',
	'total'	=>	'Total'	,
	'status'	=>	'Status'	,
	'see-more'	=>	'See More'	,
	'no-pending-order-msg'	=>	'Congratulations, you have no pending orders.'	,
	'no-cancelled-order-msg'	=>	'You don\'t have cancelled orders.'	,
	'no-intransit-msg'	=>	'No orders on its way.'	,
	'no-purchases-msg'	=>	'No purchases yet.'	,
	'no-delivered-msg'	=>	'You don\'t have delivered orders yet.'	,
	'in-transit'	=>	'In Transit'	,
	'cancelled'	=>	'Cancelled'	,
	'pending'	=>	'Pending'	,
	'received'	=>	'Received'	,
	'delivered'	=>	'Delivered'	,
	'confirm'	=>	'Confirm'	,
	'received-returned-msg-1'	=>	'Did you receive the returned item?'	,
	'saved-msg'	=>	'Successfully saved.'	,
	'update-failed-msg'	=>	'Update Failed.'	,
	'mark-pending-msg'	=>	'Are you sure you want to mark this order as pending?'	,
	'received-returned-msg-2'	=>	'Received returned item?'	,
	'placed-by'	=>	'Placed By'	,
	'placed-on'	=>	'Placed On'	,
	'shipped-to'	=>	'Shipped To'	,
	'ship-address'	=>	'Shipping Address'	,
	'nearest-landmark'	=>	'Nearest Landmark'	,
	'contact-no'	=>	'Contact Number'	,
	'alt-contact-no'	=>	'Alternate Contact Number'	,
	'buyer-remarks'	=>	'Buyer Remarks'	,
	'owner'	=>	'Owner'	,
	'store-name'	=>	'Store Name'	,
	'store-address'	=>	'Store Address'	,
	'total-summary'	=>	'TOTAL SUMMARY'	,
	'unit-price'	=>	'Unit Price'	,
	'you-save'	=>	'You save'	,
	'now'	=>	'Now'	,
	'qty'	=>	'QTY'	,
	'order-notes'	=>	'Order Notes'	,
	'order-notes-msg'	=>	'You can place your notes or remarks here per transaction.'	,
	'return-videos'	=>	'VIDEOS FROM THE BUYER'	,
	'browser-no-html-support'	=>	'Your browser does not support HTML5 video.'	,
	'return-image'	=>	'IMAGES FROM THE BUYER'	,
	'qualifications'	=>	'QUALIFICATIONS'	,
	'what-in-box'	=>	'What\'s in the box'	,
	'brand'	=>	'Brand'	,
	'weight'	=>	'Weight'	,
	'tracking-number'	=>	'Tracking Number'	,
	'stores'	=>	'STORES'	,
	'order-details'	=>	'ORDER DETAILS'	,
	'returned'	=>	'Returned'	,
	'delivered-on'	=>	'Delivered on '	,
	'received-on-1'	=>	'Received on '	,
	'received-on-2'	=>	' '	,
	'in-transit-on-1'	=>	'In Transit on '	,
	'in-transit-on-2'	=>	' '	,
	'discount' => 'Discount',
	'subtotal' => 'Subtotal',
	'products' => 'Products',
	'total-price-including-vat' => 'Total Price including VAT',

	'return-msg-1'	=>	'This order has been returned on'	,	//This order has been returned on <date> with Case No.  <case number>
	'return-msg-2'	=>	'with Case No.'	,	//This order has been returned on <date> with Case No.  <case number>

	'reason-return'	=>	'Reason for Returning'	,	//Reason for Returning

	'contact-customer-support-1'	=>	'You may contact our'	,	//You may contact our <a> Customer Support </a> regarding this matter.
	'contact-customer-support-2'	=>	'Customer Support'	,	//You may contact our <a> Customer Support </a> regarding this matter.
	'contact-customer-support-3'	=>	'regarding this matter.'	,	//You may contact our <a> Customer Support </a> regarding this matter.

	'ship-your-item'	=>	'Ship Your Item'	,	//Ship Your Item
	'shipment-type'	=>	'Select Shipment Type'	,
	'drop-off'	=>	'Drop Off'	,
	'pick-up'	=>	'Pick Up'	,
	'required-shipment'	=>	'Shipment Type is required.'	,
	'select-courier'	=>	'Select Courier'	,
	'required-courier'	=>	'Courier is required'	,
	'required-track-no'	=>	'Tracking number is required.'	,
	'submit'	=>	'Submit'	,
	'cancel-order'	=>	'Cancel Order'	,
	'required-reason'	=>	'Reason is required.'	,
	'yes'	=>	'Yes',
	'updated-note-msg'	=>	'Your note has been updated'	,
	'color' => 'Color',
	'size' => 'Size',
	'shipchar' => 'Shipping Fee & Valuation Charge',
	'vat' => 'VAT',

	'transaction' => 'Transaction',
	'pending-orders' => 'pending order/s',

	//airwaybill
	'airway-bill' => 'Airway Bill',
	'deliver-to' => 'DELIVER TO',
	'area-code' => 'Area Code',
	'collected' => 'Amount to be collected COD',
	'awb' => 'AWB No.',
	'dimensions' => 'Dimensions (cms)',
	'order-date' => 'Order Date',
	'pieces' => 'Pieces',
	'package-type' => 'Package Type',
	'sr' => 'Sr.', 
	'item-code' => 'Item Code',
	'item-desc' => 'Item Description',
	'quantity' => 'Quantity',
	'value' => 'Value',
	'inclusive' => 'Inclusive of all taxes and shipping charges',
	'received-by' => 'Received by',
	'printed' => 'Printed Name and Signature',
	'relationship' => 'Relationship',
	'date-received' => 'Date Received',
	'attempt' => 'Attempt Reason',
	'1st' => '1st',
	'2nd' => '2nd',
	'3rd' => '3rd',
	'print' => 'Print',
	'shipchar' => 'Shipping Fee & Valuation Charge',
	'vat' => 'VAT',
	'close'	=>	'Close'	,

	'photo-evidence' => 'PHOTO EVIDENCE',
	'video-evidence' => 'VIDEO EVIDENCE'
];
