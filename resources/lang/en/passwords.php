<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",

    'forgot-password' => 'Forgot Password',
    'reset-password' => 'Reset Password',
    'email-address' => 'E-Mail Address',
    'send-pw-link' => 'Send Password Reset Link',
    'question' => 'Question',
    'select-question' => 'Select a question',
    'answer' => 'Answer',
    'search-email' => 'Search Email',
    'submit' => 'Submit',

    'email-error-msg' => 'We can\'t find a user with that e-mail address.',
    'question-error-msg' => 'Please select a question.',
    'answer-error-msg' => 'You\'re answer does not match the selected question.',
    'alt-email' => 'Alternate E-Mail Address',
    'please-set-alt-email' => 'Please set your Alternate E-Mail Address.'
];
