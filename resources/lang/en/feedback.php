<?php

return [
	'title'		=> 'FEEDBACKS/COMMENTS/SUGGESTIONS',
	'send-feedback' => 'Send Feedback', // 
	'description1' => 'Your Feedback is very important to us. Please dont hesitate to post your feedback below', //  :EDIT THIS
	'close' => 'Close',
	'submit' => 'Submit',
	'description2' => 'The post will be previewed by our team before posting to avoid spam ', // The post will be previewed by our team before posting to avoid spam :EDIT THIS
	'view-more-replies' => 'View More Replies',
	'likes' => 'Likes', // likes
	'like'	=> 'Like', //Like



	'liked' => 'Liked!', //Liked
	'unliked' => 'Unliked!',//Unliked
	'you-and' => 'You and', //You and
	'others-liked-this' => 'other/s liked this.',//others liked this.
	'you-liked-this' => 'You liked this.', //You liked this
	'add-comment' => 'Add Comment',
	'say-comment' => 'Say something about the feedback.',
	'no-more-feedbacks' => 'No more Feedbacks',
	'success-msg-feedback' => 'Feedback Submitted!',
	'try-again'	=> 'Please try again!',
	'success-msg-comment' => 'Comment Submitted',


];
