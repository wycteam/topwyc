<?php  
	 return[
	 	 'tagline-1' 		=> 'We are all in this together.'
		,'tagline-1-desc'	=> 'Interested in our services?'
		,'tagline-1-desc2'	=> 'Contact us and be one of our satisfied customers.'
		,'tagline-1-desc3'	=> 'We will hear you whenever and wherever you are.'
		,'view-services'	=> 'View Our Services'
		,'operating-hrs'	=> 'OPERATING HOURS'
		,'sched'			=> 'Monday - Friday (8 am - 5pm)'
	 ]
?>