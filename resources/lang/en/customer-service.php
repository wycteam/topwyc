<?php

return [
	'customer-service' => 'Customer Service',
	'subject' => 'Subject',
	'body' => 'Body',
	'all-tickets' => 'All Tickets',
	'open-tickets' => 'Open Tickets',
	'closed-tickets' => 'Closed Tickets',
	'create-new-ticket' => 'Create New Ticket',
	'you-dont-have-any-open-tickets' => 'You don\'t have any open tickets.',
	'issue-number' => 'Issue Number',
	'date-created' => 'Date Created',
	'status' => 'Status',
	'open' => 'Open',
	'closed' => 'Closed',
	'please-subject' => 'Please indicate subject.',
	'give-a-concern' => 'Give a concern or suggestion.',
	'message-thread' => 'Message Thread',
	'reopen-ticket' => 'Reopen Ticket',
	'this-ticket-is-already-closed' => 'This ticket is already closed.',
	'if-you-wish-to-reopen-this-issue-a-new-ticket-will-be-created' => 'If you wish to reopen this issue, a new ticket will be created.'
];