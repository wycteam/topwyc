<?php 
return [
	'messages' => 'Messages',
	'search-user' => 'Search User', //Search User
	'enter-to-send' => 'Press enter to send the message', //Press enter to send the message
	'write-something' => 'Write Something...', //Write Something...

	'accept'				=> 'Accept', //accept
	'unfriend' 				=> 'Unfriend', // Unfriend
	'reject'				=> 'Reject', //Reject
	'add' 					=> 'Add Friends', // Add Friends
	'block-msg'				=> 'Block Messages', // Block Messages
	'unblock'				=> 'Unblock', // Unblock

	'reminder' => 'Reminder',
	'alert-msg' => 'Always transact inside WATAF.ph for safety and security purposes.',

];