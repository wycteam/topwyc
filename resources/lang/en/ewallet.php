<?php

return [
	//for checking
	'no-user-found'			=> 'Oops! No Users found. Please Try again.', //Oops! No Users found. Please Try again.
	'ph-email'				=> 'Type an email', //Type an email
	'ph-email-help'			=> 'for non-friends: provide his/her email', //for non-friends: provide his/her email
	'msg-no-account'		=> 'No Bank Accounts Yet', //No Bank Accounts Yet
	'your-bank-account'		=> 'your Bank Account', // your bank account
	'add'					=> 'add',//add
	'edit'					=> 'edit',//edit
	'update'				=> 'update', //update
	'offline-or-online'		=> 'Offline or Online', //'Offline or Online'
	'from-another'			=> 'to/from another Account',//'to/from another Account'
	'get-saved-money'		=> 'Get your saved money', //'Get your saved money'
	'reg-account'			=> 'Register your account',//'Register your account'
	'upload'				=> 'Upload', //Upload
	'upload-deposit-slip'	=> 'Upload Deposit Slip', //'Upload Deposit Slip'
	'wait-verification' 	=> 'Success! Please wait for verification', // Success! Please wait for verification
	'success-upload' 		=> 'Deposit Slip Uploaded!', // Deposit Slip Uploaded!
	'try-again' 			=> 'Please Try Again.', // Please Try Again.
	'failed-upload' 		=> 'Upload Unsuccessful', // Upload Unsuccessful
	'account-added' 		=> 'Bank Account Added', // Bank Account Added
	'account-updated' 		=> 'Bank Account Updated', // Bank Account Updated
	'success-request' 		=> 'Request Submitted', // Request Submitted
	'ref-no' 				=> 'Your Reference Number: ', // Your Reference Number: 
	'deposit' 				=> 'Deposit', // Deposit
	'withdraw' 				=> 'Withdraw', // Withdraw
	'give-receiver' 		=> 'Please give this code to the receiver/s.', // Please give this code to the receiver/s.
	'ewallet-received' 		=> 'Ewallet Received!', // Ewallet Received!
	



	//DONE CHECKING
	'title' 				=> 'E-WALLET',
	'transactions'			=> 'Transaction Record', //Transaction Record'
	'available-balance'		=> 'Available Balance',
	'load-money'			=> 'Load Money', //Load Money
	'withdraw-funds'		=> 'Withdraw Money', //Withdraw Money
	'transfer-or-receive'	=> 'Transfer or Receive', //'Transfer / Receive',
	'bank-account'			=> 'Bank Account', //Bank Account
	'datetime'				=> 'Time',
	'ref-no-short'			=> 'Ref No.',
	'type'					=> 'Type',
	'amount'				=> 'Amount',
	'description'			=> 'Description',
	'status'				=> 'Status',
	'balance'				=> 'Balance',
	'offline'				=> 'Offline', //Offline'
	'online'				=> 'Online', //Online'
	'load-online-soon'		=> 'Load Money Online is Under Construction! Please bear with us. It\'s coming soon!',
	'close' 				=> 'Close',
	'bank'					=> 'Bank', 
	'choose-bank'			=> 'Choose a Bank...',
	'submit'				=> 'Submit',
	'account-name'			=> 'Account Name',
	'account-no'			=> 'Account Number',
	'bank-account'			=> 'Bank Account',
    'receive'				=> 'Receive',//'Receive'
	'transfer'				=> 'Transfer', //Transfer 
	'code' 					=> 'CODE', //CODE
	'ph-code'				=> '8 Character Code', // '8 Character Code',
	'receiver'				=> 'Receiver',//'Receiver',
	'notes'					=> 'Notes', // 'Notes',
	'leave-msg' 			=> 'Leave a message ', //Leave a message 
	'bank'					=> 'Bank', //Bank
	'status'				=> 'Status', //Status'
	'add-bank-account' 		=> 'Add Bank Account', //Add Bank Account
	'choose-bank'			=> 'Choose a Bank ...', //Choose a Bank ...
	'reenter-account-no'	=> 'Re-enter Account Number', //Re-enter Account Number
	'leave-msg' 			=> 'Leave a message', //Leave a message 

	//recently added -xy
	'ewallet-faq-1'	=>	'A system that securely stores user payment information for numerous electronic transactions on this website.'	,

	'ewallet-faq-2'	=>	'E-wallets are expected to become a major player in the online payment industry, as consumers constantly continue to look for additional easier and quicker ways to purchase goods and services online. It makes online shopping easier and faster than ever before that is why it is becoming more and more popular and widely used.',

	'ewallet-faq-3'	=>	'By using this e-wallet, users can complete purchases of different products in this shopping website easily and quickly. Users can also transfer money to another users using this system.'	,

	'read-more'				=>	'Read More'	,
	'depslip-ins-1'			=>	'After depositing your indicated amount on your selected bank, upload a clear copy of your deposit slip.'	,
	'depslip-ins-2'			=>	'To upload your deposit slip, click the record on E-wallet transaction table.'	,
	'depslip-ins-3'			=>	'Our Customer Service will review your uploaded deposit slip.'	,
	'depslip-ins-4'			=>	'Once it is verified, you will be notified. This usually takes 30 minutes to 1 hour.'	,
	'see-instructions'		=>	'See Instructions'	,
	'instructions'			=> 	'Instructions' ,
	'invalid-code'			=>  'Invalid Code!',

	'usable'				=>  'Usable',
	'show-details'			=>  '(show details)',
	'mininum-amount'		=>  'Minimum amount you can transfer is 100.'

];
