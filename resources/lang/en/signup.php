<?php 

return [
	'register' => 'Register',
	'first-name' => 'First Name',
	'last-name' => 'Last Name',
	'email' => 'Email',
	'password' => 'Password',
	'confirm-password' => 'Confirm Password',
	'date-of-birth' => 'Date of Birth (YYYY-MM-DD)',
	'gender' => 'Gender',
	'email-sent' => 'We just sent you an email for verification.',
	'email-verified' => 'Your email has been verified!',
	'by-agreement' => 'By creating an account you agree to WATAF\'s',
	'terms-service' => 'Terms of Service',
	'terms-conditions' => 'Terms & Conditions',
	'male' => 'Male',
	'female' => 'Female',
	'ph-gender' => 'Select gender...',//Select gender
];