<?php

return [
	 'successfully-registered' 	=> 'Successfully Registered.'
	,'you-can-search-wechat' 	=> 'Please contact us using wechat, after you click the button our wechat ID automatically copied. You can search our ID in wechat app.'
	,'scan-qr-code' 			=> 'or you can use this QR code to view our wechat account.'

];