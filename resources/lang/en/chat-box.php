﻿<?php

return [
	'type-a-message' => 'Type a message...',
	'reminder' => 'Reminder',
	'alert-msg' => 'Always transact inside WATAF.ph for safety and security purposes.',
	'thank-you' => 'Thank you for the chat. Feel free to leave us any additional feedback.',
	'case-solved' => 'Was your case solved?',
	'yes' => 'Yes',
	'no' => 'No',
	'rate-agent' => 'How would you rate the agent?',
	'good' => 'Good',
	'average' => 'Average',
	'bad' => 'Bad',
	'please' => 'Please answer this question.',
	'comments' => 'Additional comments',
	'submit' => 'Submit',
	'issue' => 'This issue is closed. Thank you for the feedback.',
	'feedback' => 'Your feedback is highly appreciated.'
];