﻿<?php

return [
	'delete' => 'Delete',
	'ok' => 'Okay',
	'cancel' => 'Cancel',
	'proceed' => 'Proceed',
	'confirm' => 'Confirm',
	'close' => 'Close',
	'yes' => 'Yes',
	'no' => 'No',
	'reset' => 'Reset',
	'cropped' => 'Cropped',
	'crop' => 'Crop',
	'store' => 'Store',
	'items' => 'Items',
	'required' => 'Required',
	'save' => 'Save',
	'next' => 'Next',
	'previous' => 'Previous',
	'continue'	=> 'continue',
	'view-all' => 'View All',
	'browse' => 'Browse',
	'remove' => 'Remove',
	'submit'	=> 'Submit',
	'finish'	=> 'Finish',
	'filter' => 'Filter'
];
