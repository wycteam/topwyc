<?php  
	 return [
	 	 'tagline-1' 		=> 'Don\'t worry, we\'ve got your back.'
	 	,'tagline-1-desc'	=> 'We understand the hassle of processing immigration & government-related documents.'
	 	,'tagline-1-desc2'	=> 'Why spend more time and energy when you can seek our help?'
	 	,'tagline-1-desc3'	=> 'Check out our list of services we offer.'
	 	,'tagline-1-btn'	=> 'Still have questions?'
	 ]
?>