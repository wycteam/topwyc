<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Invalid email or password.',
    'failed_not_verified' => 'Your account has not been verified.',
    'failed_deactivated' => 'Your account is deactivated. Please contact the administrator.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
