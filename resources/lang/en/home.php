<?php

return [

    'categories' => '商品类别', // Categories --commercial product category :EDIT THIS ONE
    'more-categories' => '更多分类', // More Categories
    'popular-stores' => '热门商店', // Popular Stores
    'latest-products' => '最新产品', // Latest Products
    'for-men' => 'For Men', // For Men :EDIT THIS ONE
    'for-women' => 'For Women', // For Women :EDIT THIS ONE
    'cyber-zone' => 'Cyber Zone', // Cyber Zone
    'shop-now' => 'Shop Now', // Shop Now

    //new
    'featured-stores' => 'Featured Stores', //Featured Stores


    //visa
     'tagline-1'         => 'We Are Here To Help'
    ,'tagline-1-desc'    => 'We assist and provide services related to immigration'
    ,'tagline-1-desc2'    => 'and other government department and business solutions.'
    ,'tagline-1-btn'     => 'Start your application today!'

    ,'tagline-2'         => 'We Value Your Time'
    ,'tagline-2-desc'    => 'We are happy to introduce you our services for your convenience.'
    ,'tagline-2-btn'     => 'Learn more about our services.'

    ,'tagline-3'         => 'Our company aims to provide the best service and maintain good relationship with our clients.'
    ,'tagline-3-desc'    => 'We implement these values to achieve those goals:'

    ,'tagline-4'         => 'Get in touch with us.'
    ,'tagline-4-desc'    => 'We are ready to assist you anytime.'
    ,'tagline-4-desc-2'  => 'Track your packages and services anytime, anywhere.'
    ,'tagline-4-desc-3'  => 'Or follow us on social platform'
    ,'tagline-4-btn'     => 'Send Us Mail'

    ,'value-1'           => 'TRANSPARENCY'
    ,'value-1-sub'       => 'We promote'
    ,'value-1-long-desc' => 'We undertake to be transparent in all our professional contacts. In practice, this means we will identify our client in proactive external professional contacts and services offered for a client.'
    ,'value-1-long-desc2'=> 'Sometimes we establish coalitions on behalf of clients to work on specific issues. Whenever we do so, we undertake to be transparent about the purpose of the coalition and principal funders.'

    ,'value-2'           => 'HONESTY'
    ,'value-2-sub'       => 'We value'
    ,'value-2-long-desc' => 'We undertake not to disclose any confidential information provided to us in the course of our work for clients, unless explicity authorized to do so or required by governments or other legal authorities. All WYC employees are contractually obligued to respect this undertaking on all internal matters.'
    ,'value-2-long-desc2'=> 'We store and handle confidential information so as to ensure that only those who are working with the information have access to it. '
    ,'value-2-long-desc3'=> 'We advice our clients to uphold all these values in their own related to the advice we provide them.'

    ,'value-3'           => 'CONFIDENTIALITY'
    ,'value-3-sub'       => 'We assure'
    ,'value-3-long-desc' => 'We understand to be honest in all our professional dealings. We undertake never knowingly to spread false or misleading information and to take reasonable care to avoid doing so inadvertently.'
    ,'value-3-long-desc2'=> 'We also ask our clients to be honest with us and not to request that we compromised our principles or the law. We take every possible step to avoid conflicts of interest.'
    ,'value-3-long-desc3'=> 'We take every possible step to avoid conflicts of interest.'

    ,'value-4'           => 'INTEGRITY'
    ,'value-4-sub'       => 'We practice'
    ,'value-4-long-desc' => 'We will only work for the organizations that are prepared to uphold the values of transparency and honesty in the course of our work for them. Sometimes our clients are involved with contentiuos issues. It is part of our job to provide communications assistance to those clients. We undertake to represent those clients only in ways consistent with the values of honesty and transparency both in what they do and in what we do for them.'

    ,'services'          => 'Services'
    ,'offered'           => 'Offered'
    ,'service-1-name'    => 'Tourist Visa Extension'
    ,'service-1-desc'    => 'Foreign Nationals who are admitted with an initial stay of thirty (30) days may apply for an additional stay in the Philippines.'

    ,'service-2-name'    => '9G Working Visa'
    ,'service-2-desc'    => 'Foreign Nationals who are proceeding to Philippine to engage in any lawful occupation.'

    ,'service-3-name'    => 'Emigration Clearance Certificate (ECC)'
    ,'service-3-desc'    => 'A Clearance Certificate that foreign national should secure before leaving the country.'

    ,'service-4-name'    => 'Alien Registration Program (ARP)'
    ,'service-4-desc'    => 'Register and Re-register foreign national to Bureau of Immigration by caturing, encoding information and finger print.'

    ,'service-view-btn'  => 'View All Services'

    ,'tracking'          => 'Tracking'
    ,'track-1-desc'      => 'Find out the status of your packages and services in just one click.'
    ,'track-btn'         => 'Track'
    ,'latest-news'       => 'Latest News'
    ,'featured-news'     => 'Featured News'

    ,'mobile-app'        => 'Mobile APP'
    ,'apple-store'       => 'APPLE STORE'
    ,'google-play'       => 'GOOGLE PLAY'
    ,'for-ios'           => 'for IOS devices'
    ,'for-android'       => 'for android devices'
    ,'mobile-msg-1'      => 'Here are the ways how you can get our app:'  


];
