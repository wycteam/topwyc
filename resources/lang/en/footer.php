<?php

return [
	'shopping' => 'Shopping',
	'all-rights' => 'All Rights Reserved.',
	'about' => 'About',
	'terms' => 'Terms & Conditions',
	'privacy-policy' => 'Privacy Policy',
	'faq' => 'FAQ',
	'site' => 'Site Map',

	//xy
	'like-fb' => 'Like us on facebook',
	'follow-twit' => 'Follow us on Twitter',
];
