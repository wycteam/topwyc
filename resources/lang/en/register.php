<?php

return [
	
	 'bday' 				=> 'Birth Day'
	,'continue'				=> 'Continue'
	,'email-opt'			=> 'E-mail (Optional)'
	,'female' 				=> 'Female'
	,'fname' 				=> 'First Name'
	,'lname' 				=> 'Last Name'
	,'male' 				=> 'Male'
	,'mob-num' 				=> 'Mobile Number'
	,'month'				=> 'Month'
	,'ok'					=> 'Okay'
	,'sign-up' 				=> 'Sign Up'
	,'resend' 				=> 'Resend Code'
	,'year'					=> 'Year'
    ,'birth-date'        	=> 'Date of Birth'

	//sentences
	,'already-reg'					=> 'Already Registered?'
	,'msg-enter-pin'				=> 'Enter One-Time PIN'
	,'msg-pin-verify'				=> 'One-Time PIN Verification'	
	,'msg-fill-details' 			=> 'Please fill with your details'
	,'msg-verify-here'				=> 'Verify Account Here'
	,'msg-pin-verify-long' 			=> "We've sent a One-Time PIN (OTP) to your mobile phone number. Please enter the OTP below."
	,'msg-who-recommended'			=> 'Who recommended you?'
	,'msg-four-digit-sent'			=> 'A 4-digit code will be sent to your cellphone number to ensure you have access to the cellphone number you entered. Please enter the code to proceed.'			
	,'msg-enter-code-proceed' 		=> 'Please enter the correct code to proceed.'
	,'msg-code-sent'				=> 'Code successfully sent. Please check the code we\'ve sent to your mobile number.'
	,'msg-user-registered'			=> 'User already registered.'

	//months
	,'january'		=> 'January'
	,'february'		=> 'February'
	,'march'		=> 'March'
	,'april'		=> 'April'
	,'may'			=> 'May'
	,'june'			=> 'June'
	,'july'			=> 'July'
	,'august'		=> 'August'
	,'september'	=> 'September'
	,'october'		=> 'October'
	,'november'		=> 'November'
	,'december'		=> 'December'


];