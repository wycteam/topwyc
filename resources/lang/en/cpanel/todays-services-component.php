<?php

return [
	'action' => 'Action',
	'charge' => 'Charge',
	'cost' => 'Cost',
	'date' => 'Date',
	'detail' => 'Detail',
	'filter' => 'Filter',
	'id' => 'ID',
	'name' => 'Name',
	'package' => 'Package',
	'today' => 'Today',
	'yesterday' => 'Yesterday'
];