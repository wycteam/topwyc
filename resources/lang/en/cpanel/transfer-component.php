<?php

return [
	'back' => 'Back',
	'client-to-group' => 'Client to Group',
	'complete-transfer' => 'Complete Transfer',
	'detail' => 'Detail',
	'generate-new-package' => 'Generate new package',
	'group-to-client' => 'Group to Client',
	'package-no' => 'Package #',
	'packages' => 'Packages',
	'please-select-at-least-one-service' => 'Please select at least one service',
	'status' => 'Status',
	'tracking' => 'Tracking'
];