<?php

return [
	'add' 				=> 'Add',
	'amount' 			=> 'Amount',
	'balance-transfer' 	=> 'Balance Transfer',
	'bank' 				=> 'Bank',
	'cash' 				=> 'Cash',
	'client' 			=> 'Client',
	'close' 			=> 'Close',
	'deposit' 			=> 'Deposit',
	'discount' 			=> 'Discount',
	'group' 			=> 'Group',
	'option' 			=> 'Option',
	'payment' 			=> 'Payment',
	'reason' 			=> 'Reason',
	'refund' 			=> 'Refund',
	'storage' 			=> 'Storage',
	'transfer-to' 		=> 'Transfer To',

	//Sentences
	'alipay-ref-not-empty'									=> 'Alipay Reference cannot be empty.',
	'cannot-transfer-to-same-group-id' 						=> 'Cannot transfer to same group id',
	'please-contact-sir-jason-to-give-discount-to-client' 	=> 'Please contact sir Jason to give discount to client',
	'please-contact-mam-cha-to-give'						=> 'Please contact Maam Cha to give ',
	'to-client'												=> ' to client.',
];