<?php

// PLEASE INPUT IN ALPHABETICAL ORDER
// PLEASE CHECK 'COMBINED MESSAGES GUIDE' FOR COMPLEX SENTENCES

return [

 'action'						=> 'Action'
,'add-new-delivery-schedule' 	=> 'Add New Delivery Schedule'
,'add-sched'					=> 'Add Schedule'
,'cancel'						=> 'Cancel'
,'charge'						=> 'Charge'
,'client-id'					=> 'Client ID'
,'clients'						=> 'Client/s'
,'cost'							=> 'Cost'
,'dashboard'					=> 'Dashboard'
,'date'							=> 'Date'
,'description'					=> 'Description'
,'dt'							=> 'Date & Time'
,'delete-failed'				=> 'Delete Failed'
,'delete-log'					=> 'Delete Log'
,'del-sched'					=> 'Delivery Schedule'
,'detail'						=> 'Detail'
,'edit'							=> 'Edit'
,'edit-service'					=> 'Edit Service'
,'emp'							=> 'Employee'
,'est-cost'						=> 'Estimated Cost'
,'exp-dt'						=> 'Expiry Date'
,'filter'						=> 'Filter'
,'grp'							=> 'Group'
,'icard-exp'					=> 'Icard Expiration'
,'id'							=> 'ID'
,'item'							=> 'Item'
,'load-prev-date'				=> 'Load previous dates'
,'loc'							=> 'Location'
,'name'							=> 'Name'
,'on-process-serv'				=> 'On Process Services'
,'pckg'							=> 'Package'
,'pending-serv'					=> 'Pending Services'
,'please-put-a-reason'  		=> 'Please put a reason'
,'prev-dt-report'				=> 'Previous Date report'
,'proceed'						=> 'Proceed'
,'reason'						=> 'Reason'
,'reminders'					=> 'Reminders'
,'rider'						=> 'Rider'
,'save'							=> 'Save'
,'sched'						=> 'Schedule'
,'schedule-added'  				=> 'Schedule Added'
,'serv'							=> 'Service'
,'tasks'						=> 'Tasks'
,'time'							=> 'Time'
,'tip'							=> 'Tip'
,'today'						=> 'Today'
,'today-report'					=> "Today's Reports"
,'reminders-for-tom'					=> 'Reminders for Tomorrow'
,'total-estimated-cost-for-tomorrow'					=> 'Total estimated budget for tomorrow'
,'total-estimated-cost-for-today'=>'Total estimated budget for today'
,'today-serv'					=> "Today's Services"
,'today-task'					=> "Today's Task"
,'tom'							=> 'Tomorrow'
,'total-client'					=> 'Total Clients'
,'total-est-cost'				=> 'Total estimated cost'
,'total-serv'					=> 'Total Services'
,'total-today-serv-cost'					=> 'Today\'s Total Service Cost'
,'total-yesterday-serv-cost'					=> 'Yesterday\'s Total Service Cost'
,'type'							=> 'Type'
,'yes'							=> 'Yes'
,'yesterday'					=> 'Yesterday'
,'yesterday-serv'				=> "Yesterday's Services"

//Sentences
,'are-you-sure-you-want-to-delete-this-schedule' 	=> 'Are you sure you want to delete this schedule?'
,'failed-all-fields-are-required' 					=> 'Failed. All Fields are required.'
,'no-data-avail-tbl'								=> 'No data available in table'
,'no-data'											=> 'No more data'
,'reason-successfully-saved' 						=> 'Reason successfully saved'
,'remove-this-service-from-the-list' 				=> 'Remove this service from the list?'
,'schedule-successfully-updated' 					=> 'Schedule successfully updated'
,'service-successfully-removed' 					=> 'Service successfully removed'
,'successfully-deleted' 							=> 'Successfully Deleted'



];
