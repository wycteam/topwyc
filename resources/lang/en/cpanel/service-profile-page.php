<?php

return [
	'add-service-profile' => 'Add Service Profile',
	'cancel' => 'Cancel',
	'disabled' => 'Disabled',
	'enabled' => 'Enabled',
	'home' => 'Home',
	'new-profile-added' => 'New Profile Added',
	'profile-name' => 'Profile Name',
	'profile-updated' => 'Profile Updated',
	'regular' => 'Regular',
	'save' => 'Save',
	'saving-failed' => 'Saving Failed',
	'service' => 'Service',
	'service-name' => 'Service Name',
	'service-profiles' => 'Service Profiles',
	'status' => 'Status',
	'updated-successfully' => 'Updated Successfully'
];