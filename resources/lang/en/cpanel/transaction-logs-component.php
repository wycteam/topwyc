<?php

return [
	'amount' 			=> 'Amount',
	'balance' 			=> 'Balance',
	'current-balance' 	=> 'Current Balance',
	'prev-balance'		=> 'Previous Balance',
	'type' 				=> 'Type',
	'current-commission' => 'Current Commission',
	'prev-commission'    => 'Previous Commission',
];