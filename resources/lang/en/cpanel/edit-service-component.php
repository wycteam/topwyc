<?php

return [
	'first-expiry' 			=> '1st expiry',
	'arrival-date' 			=> 'Arrival Date',
	'charge' 				=> 'Charge',
	'complete' 				=> 'Complete',
	'close' 				=> 'Close',
	'cost' 					=> 'Cost',
	'disabled' 				=> 'Disabled',
	'discount' 				=> 'Discount',
	'docs-released'			=> 'Documents to be released',
	'enabled' 				=> 'Enabled',
 	'expiration-date' 		=> 'Expiration Date',
	'ext-expiry' 			=> 'Ext. expiry',
	'extend-to' 			=> 'Extend To',
	'extra-cost' 			=> 'Extra Cost',
	'icard-expiry' 			=> 'ICard expiry',
	'icard-issued' 			=> 'ICard Issued',
	'note' 					=> 'Note',
	'on-process' 			=> 'On Process',
	'optional-documents' 	=> 'Optional Documents',
	'optional-docs-released'=> 'Optional Documents to be released',
	'pending' 				=> 'Pending',
	'required-documents' 	=> 'Required Documents',
	'reason' 				=> 'Reason',
	'save' 					=> 'Save',
	'status' 				=> 'Status',
	
	//Sentences
	'arrival-date-cannot-be-ahead-from-today' 	=> 'Arrival date cannot be ahead from today',
	'no-documents-to-show'						=> 'No documents to show.',	
	'please-fillup-the-expiration-date' 		=> 'Please fillup the expiration date',
	'please-input-reason' 						=> 'Please input reason',
	'please-select-required-documents' 			=> 'Please select required documents',
	'successfully-updated' 						=> 'Successfully updated'
];