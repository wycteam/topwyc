<?php

return [
	'client-id' => 'Client ID',
	'client-names' => 'Client Names',
	'collapse-all' => 'Collapse All',
	'date' => 'Date',
	'date-time' => 'Date & Time',
	'expand-all' => 'Expand All',
	'filter' => 'Filter',
	'list-of-reports' => 'List of Reports',
	'processor' => 'Processor',
	'service' => 'Service',
	'service-detail' => 'Service Detail',
	'status' => 'Status',
	'to' => 'to',
	'total-cost' => 'Total Cost',
	'tracking-no' => 'Tracking No'
];