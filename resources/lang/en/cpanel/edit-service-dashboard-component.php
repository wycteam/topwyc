<?php

return [
	'first-expiry' 			=> '1st expiry',
	'arrival-date' 			=> 'Arrival Date',
	'close' 				=> 'Close',
	'complete' 				=> 'Complete',
	'cost' 					=> 'Cost',
	'disabled' 				=> 'Disabled',
	'discount' 				=> 'Discount',
	'enabled' 				=> 'Enabled',
	'enter'					=> 'Enter ',
	'expiration-date' 		=> 'Expiry Date',
	'extend-to' 			=> 'Extend To',
	'icard-expiry' 			=> 'ICard expiry',
	'icard-issued' 			=> 'ICard Issued',
	'note' 					=> 'Note',
	'on-process' 			=> 'On Process',
	'optional-documents' 	=> 'Optional Documents',
	'pending' 				=> 'Pending',
	'reason' 				=> 'Reason',
	'required-documents' 	=> 'Required Documents',
	'save' 					=> 'Save',
	'status' 				=> 'Status',
	'tip' 					=> 'Tip',

	//Sentences
	'please-input-discount-reason' 		=> 'Please input discount reason',
	'please-select-required-documents' 	=> 'Please select required documents',
	'select-docs'						=> 'Select Documents',
	'service-successfully-updated' 		=> 'Service successfully updated',
];