<?php

return [
	'action' 					=> 'Action',
	'add'						=> 'Add ',
	'add-client-document-type'  => 'Add Client Document Type',
	'cancel' 					=> 'Cancel',
	'client-document-type' 		=> 'Client Document Types',
	'client-documents' 			=> 'Client Documents',
	'client-id' 				=> 'Client ID',
	'delete'					=> 'Delete ',
	'delete-failed' 			=> 'Delete Failed',
	'document-type' 			=> 'Document Type',
	'edit'						=> 'Edit ',
	'expired-at' 				=> 'Expired At',
	'home' 						=> 'Home',
	'issued-at' 				=> 'Issued At',
	'name' 						=> 'Name',
	'save' 						=> 'Save',
	'submit' 					=> 'Submit',
	
	//Sentences
	'click-to-choose-or-drag-images-to-upload' 	=> 'Click to choose or drag images to upload',
	'delete-failed' 							=> 'Delete Failed',
	'images-field-is-required' 					=> 'Images field is required',
	'incorrect-filename-format' 				=> 'Incorrect filename format',
	'list-of-client-document-types' 			=> 'List of Client Document Types',
	'no-client-document-types-to-show' 			=> 'No client document types to show',
	'no-documents-to-show' 						=> 'No documents to show',
	'service-document-deleted' 					=> 'Service Document Deleted',
	'service-document-deleted' 					=> 'Service Document Deleted',
	'you-cant-upload-files-of-this-type' 		=> 'You can\'t upload files of this type'
];