<?php

return [
	'address' 	=> 'Address',
	'close' 	=> 'Close',
	'edit' 		=> 'Edit',
	'name' 		=> 'Name',
	'number' 	=> 'Number'
];