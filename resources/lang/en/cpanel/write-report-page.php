<?php

return [
	'action' => 'Action',
	'add-quick-report' => 'Add Quick Report',
	'add-reports-to-list-of-clients-below' => 'Add Reports to List of Clients Below',
	'birthdate' => 'Birthdate',
	'cancel' => 'Cancel',
	'charge' => 'Charge',
	'client-id' => 'Client ID',
	'continue' => 'Continue',
	'cost' => 'Cost',
	'date' => 'Date',
	'details' => 'Details',
	'discount' => 'Discount',
	'id' => 'ID',
	'name' => 'Name',
	'package' => 'Package',
	'please-select-client-first' => 'Please select client first',
	'please-select-service-first' => 'Please select service first',
	'remove' => 'Remove',
	'save' => 'Save',
	'tip' => 'Tip',
	'write-report' => 'Write Report'
];