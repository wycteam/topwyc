<?php

return [
	 //Sentences

	 // ,'fname-required' 			=>'First Name is required.'
	 // ,'lname-required' 			=>'Last Name is required.'
	 // ,'address-required' 		=>'Address is required.'
	 // ,'contact-number-required' =>'Contact Number is required.'
	 // ,'email-required' 			=>'Email is required'
	 // ,'email-email' 			=>'Must be a valid email address.'
	 // ,'email-unique' 			=>'The email you provided is already used.'
	  'agent-required'			=>'Please select a client.'
	 ,'agent-not-same-midman'	=>'Agent must not be the same as middleman.'
	 ,'you-added-agent'			=>'You have successfully added new agent.'
	 ,'client-already-agent'	=>'Client is already an agent.'
];