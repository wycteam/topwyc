<?php

return [
	'action' 			=> 'Action',
	'add-new-branch' 	=> 'Add New Branch',
	'branch-added' 		=> 'Branch Added',
	'branch-manager' 	=> 'Branch Manager',
	'branch-name' 		=> 'Branch Name',
	'cancel' 			=> 'Cancel',
	'delete-branch' 	=> 'Delete Branch',
	'delete-failed' 	=> 'Delete Failed',
	'edit-branch' 		=> 'Edit Branch',
	'home' 				=> 'Home',
	'id' 				=> 'ID',
	'list-of-branches' 	=> 'List of Branches',
	'save' 				=> 'Save',
	'no-of-clients' 	=> 'No. of Clients',
	'saving-failed' 	=> 'Saving Failed',
	'update-failed' 	=> 'Update Failed',
	'yes' 				=> 'Yes',


	//Sentences
	'are-you-sure-you-want-to-delete-this-branch' 	=> 'Are you sure you want to delete this branch?',
	'branch-successfully-deleted' 					=> 'Branch successfully deleted',
	'branch-successfully-updated' 					=> 'Branch successfully updated',
];