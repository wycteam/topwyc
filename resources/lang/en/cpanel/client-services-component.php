<?php

return [
	'action' 	=> 'ACTION',
	'back' 		=> 'Back',
	'charge' 	=> 'Charge',
	'client-id' => 'CLIENT ID',
	'continue' 	=> 'Continue',
	'cost' 		=> 'Cost',
	'date' 		=> 'Date',
	'details' 	=> 'Details',
	'discount' 	=> 'Discount',
	'name' 		=> 'NAME',
	'package' 	=> 'Package',
	'status' 	=> 'Status',

	//Sentences
	'please-select-clients' => 'Please select clients',
	'please-select-services' => 'Please select services',
];