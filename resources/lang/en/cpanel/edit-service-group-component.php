<?php

return [
	'back' 			=> 'Back',
	'charge' 		=> 'Charge',
	'client-id' 	=> 'Client ID',
	'close' 		=> 'Close',
	'complete' 		=> 'Complete',
	'continue' 		=> 'Continue',
	'cost' 			=> 'Cost',
	'date-recorded'	=> 'Date Recorded',
	'details' 		=> 'Details',
	'disable' 		=> 'Disable',
	'discount' 		=> 'Discount',
	'enable' 		=> 'Enable',
	'extra-cost' 	=> 'Extra Cost',
	'name' 			=> 'Name',
	'note' 			=> 'Note',
	'on-process' 	=> 'On Process',
	'package' 		=> 'Package',
	'pending' 		=> 'Pending',
	'reason' 		=> 'Reason',
	'save' 			=> 'Save',
	'status' 		=> 'Status',

	//Sentences
	'please-select-at-least-one-client' => 'Please select at least one client',

];