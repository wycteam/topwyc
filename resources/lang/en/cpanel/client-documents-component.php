<?php

return [
	'add-documents' => 'Add Documents',
	'choose-file' 	=> 'Choose File',
	'close' 		=> 'Close',
	'document-type'	=> 'Document Type',
	'expired-at' 	=> 'Expired At',
	'issued-at'		=> 'Issued At',
	'remove' 		=> 'Remove',

	//Sentences
	'documents-is-required' 				=> 'Documents is required',
	'incorrect-expired-at-field-format' 	=> 'Incorrect "Expired At" field format',
	'date-expiry-should-ahead-issuance' => 'Date of expiry should be ahead of issuance date',
	'incorrect-issued-at-field-format' 		=> 'Incorrect "Issued At" field format',
	'successfully-saved' 					=> 'Successfully saved',
	'the-document-type-field-is-required' 	=> 'The "Document Type" field is required',
	'the-issued-at-field-is-required' 		=> 'The "Issued At" field is required',
	'you-cant-upload-files-of-this-type' 	=> 'You can\'t upload files of this type'
];
