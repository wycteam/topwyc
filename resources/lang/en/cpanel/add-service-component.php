<?php

return [
	'charge' 				=> 'Charge',
	'click-here' 			=> 'click here',
	'close' 				=> 'Close',
	'cost' 					=> 'Cost',
	'discount' 				=> 'Discount',
	'extra-cost' 			=> 'Extra Cost',
	'note' 					=> 'Note',
	'optional-documents' 	=> 'Optional Documents',
	'reason' 				=> 'Reason',
	'required-documents' 	=> 'Required Documents',
	'same-day-filing' 		=> 'Same Day Filing',
	'save' 					=> 'Save',
	'service' 				=> 'Service',


	//Sentences
	'check-all-required-documents' 				=> 'Check all required documents',
	'if-you-want-to-view-all-services-at-once' 	=> 'If you want to view all services at once',
	'please-input-reason' 						=> 'Please input reason',
	'please-select-from-required-documents' 	=> 'Please select from required documents',
	'please-select-service-category' 			=> 'Please select service category',
];