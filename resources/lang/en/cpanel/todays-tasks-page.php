<?php

return [
	'all' => 'All',
	'client-id' => 'Client ID',
	'date' => 'Date',
	'estimated-cost' => 'Estimated Cost',
	'employee' => 'Employee',
	'expiration-date' => 'Expiration Date',
	'filter' => 'Filter',
	'icard-expiration' => 'ICard Expiration',
	'load-previous-dates' => 'Load Previous Dates',
	'no-more-data' => 'No more data',
	'no-results' => 'No results',
	'reminders' => 'Reminders',
	'total-estimated-cost' => 'Total Estimated Cost',
	'type' => 'Type',
	'tasks' => 'Tasks',
	'with-an-estimated-cost-of' => 'with an estimated cost of'
];