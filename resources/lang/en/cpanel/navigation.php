<?php

// PLEASE INPUT IN ALPHABETICAL ORDER


return [
	'access-control'	=>	'Access Control',
	'accts-mngr'		=>	'Accounts Manager',
	'advertisements'	=> 	'Advertisements',
	'agents'			=>	'Agents',
	'attendance'		=>	'Attendance',
	'branch-mngr'		=>	'Branch Manager',
	'contacts'			=>	'Contacts',
	'cpanel-accts'		=>	'CPanel Accounts',
	'daily-cost'		=>	'Daily Cost',
	'dashboard'			=>	'Dashboard',
	'dev-logs'			=>	'Development Logs',
	'docs'				=>	'Documents',
	'employees'			=>	'Employees',
	'financing'			=>	'Financing',
	'gallery'			=>	'Gallery',
	'list-services'		=>	'List of Services',
	'mailbox'			=>	'Mailbox',
	'manage-clients'	=>	'Manage Clients',
	'manage-groups'		=>	'Manage Groups',
	'news'				=>	'News',
	'news-events'		=>	'News and Events',
	'profile'			=>	'Profile',
	'read-reports'		=>	'Read Reports',
	'reports'			=>	'Reports',
	'service-mngr'		=>	'Service Manager',
	'service-profile'	=>	'Service Profiles',
	'shops'				=>	'Shops',
	'tracking'			=>	'Tracking',
	'write-reports'		=>	'Write Report',
	'up-docs'			=>	'Uploaded Documents',
	'client-services'	=> 	'Client Services',
	'today-serv'		=> 	"Today's Services",
	'today-task'		=> 	"Today's Task",
	'pending-serv'		=> 	'Pending Services',
	'on-process-serv'	=> 	'On Process Services',
	'event-calendar' 	=> 	'Event Calendar'
];
