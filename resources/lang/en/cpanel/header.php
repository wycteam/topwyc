<?php

return [
	'enter-client-name-cp' 				=> 'Enter Client Name/CP#',
	'enter-group-name-leader-name-cp' 	=> 'Enter Group Name/Leader Name/CP#',
	'logout' 							=> 'Logout',
	'messages' 							=> 'Messages',
	'no-new-messages' 					=> 'No new messages',
	'no-new-notifications' 				=> 'No new notifications',
	'notifications' 					=> 'Notifications',
	'view-all-messages' 				=> 'View all messages',
	'view-all-notifications' 			=> 'View all notifications',
	'welcome-to-visa-cpanel' 			=> 'Welcome to Visa CPanel'
];