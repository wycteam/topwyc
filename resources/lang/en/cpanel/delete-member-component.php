<?php

return [
	'close'  => 'Close',
	'delete' => 'Delete',

	//Sentence
	'are-you-sure-you-want-to-delete-this-member' => 'Are you sure you want to delete this member?',
];