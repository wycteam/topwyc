<?php

return [
	 'complete'		=> 'Complete'
	,'empty'		=> 'Empty'
	,'on-process' 	=> 'On Process'
	,'package' 		=> 'Package #'
	,'pending' 		=> 'Pending'
];