<?php

// PLEASE INPUT IN ALPHABETICAL ORDER
// PLEASE CHECK 'COMBINED MESSAGES GUIDE' FOR COMPLEX SENTENCES

return [

	//LABELS, BUTTONS
	 'add-new-branch'				=>'Add New Branches'
	,'branch-mngr'					=>'Branch Manager'
	,'del-branch'					=>'Delete Branch'
	,'list-branch'					=>'List of Branches'
	,'num-clients'					=>'No. of Clients'
	,'yes'							=>'Yes'


	// VALIDATIONS, TOAST & PROMPT MESSAGES
	,'prompt-del-branch'			=>'Are you sure you want to delete this branch?'
	,'toast-branch-del-success'		=>'Branch successfully deleted.'
	,'toast-branch-update-success'	=>'Branch successfully updated.'
	,'toast-branch-save-fail'		=>'Saving Failed.'
	,'toast-branch-update-fail'		=>'Update Failed.'

];