<?php

return [
	'action'		=> 'Action',
	'add-schedule' 	=> 'Add Schedule',
	'delete' 		=> 'Delete',
	'edit' 			=> 'Edit',
	'filter' 		=> 'Filter',
	'item' 			=> 'Item',
	'location' 		=> 'Location',
	'rider' 		=> 'Rider',
	'schedule' 		=> 'Schedule',
	'today' 		=> 'Today',
	'tomorrow' 		=> 'Tomorrow'
];