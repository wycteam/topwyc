<?php

return [
	'album' 	=> 'Album',
	'gallery'	=> 'Gallery',
	'home' 		=> 'Home',

	//Sentences
	'drop-files-here-or-click-to-upload' => 'Drop files here or click to upload',
];