<?php

return [
	'action' 			=> 'Action',
	'add'				=> 'Add ',
	'add-new-document' 	=> 'Add New Document',
	'cancel' 			=> 'Cancel',
	'delete'			=> 'Delete ',
	'document-name' 	=> 'Document Name',
	'documents' 		=> 'Documents',
	'edit'				=> 'Edit ',
	'home' 				=> 'Home',
	'list-of-documents' => 'List of Documents',
	'save' 				=> 'Save',
	'title' 			=> 'Title',
	'title-chinese' 	=> 'Title (Chinese)',
	'yes' 				=> 'Yes',

	//Sentences
	'are-you-sure-you-want-to-delete-this-service-document' => 'Are you sure you want to delete this service document?',
	'delete-failed' 										=> 'Delete Failed',
	'delete-service-document' 								=> 'Delete Service Document',
	'new-service-document-added' 							=> 'New Service Document Added',
	'no-service-documents-to-show' 							=> 'No Service Documents to show',
	'once-deleted-it-will-be-irreversible' 					=> 'Once deleted it will be irreversible',
	'saving-failed' 										=> 'Saving Failed',
	'service-document' 										=> 'Service Document',
	'service-document-deleted' 								=> 'Service Document Deleted',
	'service-document-updated' 								=> 'Service Document Updated',
];
