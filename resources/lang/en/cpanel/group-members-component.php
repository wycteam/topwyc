<?php

return [
	'action' 				=> 'Action',
	'back'					=> 'Back',
	'charge' 				=> 'Charge',
	'client-number' 		=> 'CLIENT NUMBER',
	'cost'					=> 'Cost',
	'date-recorded' 		=> 'Date Recorded',
	'delete'				=> 'Delete',
	'details' 				=> 'Details',
	'disable'				=> 'Disable',
	'discount' 				=> 'Discount',
	'enable'				=> 'Enable',
	'extra-cost'			=> 'Extra Cost',
	'group-leader'			=> 'Group Leader',
	'make-vice-leader'		=> 'Make Vice Leader',
	'name' 					=> 'NAME',
	'next'					=> 'Next',
	'note'					=> 'Note',
	'package' 				=> 'Package',
	'reason' 				=> 'Reason',
	'save'					=> 'Save',
	'status' 				=> 'Status',
	'total-service-cost' 	=> 'TOTAL SERVICE COST',
	'transfer'				=> 'Transfer',
	'vice-leader'			=> 'Vice Leader'

];