<?php

return [
	'add' 		=> 'Add',
	'clients' 	=> 'Clients',
	'close' 	=> 'Close',

	//Sentences
	'clients-field-is-required' => 'Clients field is required',
];