<?php

return [
	'gallery' => 'Gallery',
	'gallery-title-cannot-be-empty' => 'Gallery title cannot be empty.',
	'home' => 'Home',
	'note-gallery-must-have-at-least-5-images-to-be-publish-in-homepage' => 'Note: Gallery must have atleast 5 images to be publish in homepage.',
	'publish-to-home' => 'Publish to home'
];