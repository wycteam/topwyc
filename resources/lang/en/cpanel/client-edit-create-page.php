<?php

// PLEASE INPUT IN ALPHABETICAL ORDER

return [
	 'select'				=> 'Select'
	,'valid-until'			=> 'Valid Until'

	//placeholders
	,'enter-add'	 		=> 'Enter your address'
	,'enter-bday'			=> 'Enter your birthday'
	,'enter-cnum'	 		=> 'Enter your contact number'
	,'enter-email'	 		=> 'Enter your email address'
	,'enter-fname'	 		=> 'Enter your first name'
	,'enter-height'	 		=> 'Enter your height'
	,'enter-lname'	 		=> 'Enter your last name'
	,'enter-mname'	 		=> 'Enter your middle name'
	,'enter-passport'	 	=> 'Enter your passport'
	,'enter-weight'	 		=> 'Enter your weight'

	//gender
	,'male'					=> 'Male'
	,'female'				=> 'Female'

	//civil status
	,'divorced'				=> 'Divorced'
	,'married'				=> 'Married'
	,'separated'			=> 'Separated'
	,'single'				=> 'Single'
	,'widowed'				=> 'Widowed'

	//Nationality
	,'american'				=> 'American'
	,'british'				=> 'British'
	,'canadian'				=> 'Canadian'
	,'chinese'				=> 'Chinese'
	,'costarricense'		=> 'Costarricense'
	,'cypriot'				=> 'Cypriot'
	,'filipino'				=> 'Filipino'
	,'french'				=> 'French'
	,'japanese'				=> 'Japanese'
	,'korean'				=> 'Korean'
	,'latvian'				=> 'Latvian'
	,'indian'				=> 'Indian'
	,'indonesian'			=> 'Indonesian'
	,'israeli'				=> 'Israeli'
	,'italian'				=> 'Italian'
	,'irish'				=> 'Irish'
	,'malaysian'			=> 'Malaysian'
	,'salvadoran'			=> 'Salvadoran'
	,'swedish'				=> 'Swedish'
	,'taiwanese'			=> 'Taiwanese'
	,'thai'				    => 'Thai'
	,'ni-vanuatu'			=> 'Vanuatu'
	,'vietnamese'			=> 'Vietnamese'


	//Country of Birth
	,'america'				=> 'America'
	,'canada'				=> 'Canada'
	,'china'				=> 'China'
	,'costa-rica'			=> 'Costa Rica'
	,'cyprus'				=> 'Cyprus'
	,'el-salvador'			=> 'El Salvador'
	,'france'				=> 'France'	
	,'india'				=> 'India'
	,'indonesia'			=> 'Indonesia'
	,'israel'				=> 'Israel'
	,'italy'				=> 'Italy'
	,'ireland'				=> 'Ireland'
	,'japan'				=> 'Japan'
	,'korea'				=> 'Korea'
	,'latvia'				=> 'Latvia'
	,'malaysia'				=> 'Malaysia'
	,'philippines'			=> 'Philippines'
	,'sweden'				=> 'Sweden'
	,'taiwan'				=> 'Taiwan'
	,'thailand'				=> 'Thailand'
	,'united-kingdom'		=>	'United Kingdom'
	,'vanuatu'				=> 'Vanuatu'
	,'vietnam'				=> 'Vietnam'


	//sentences
	,'msg-client-create-success'		=>	'Clients has been created successfully.'

];

