<?php

return [
	'back' 					=> 'Back',
	'charge' 				=> 'Charge',
	'client-id' 			=> 'Client ID',
	'close' 				=> 'Close',
	'continue' 				=> 'Continue',
	'cost' 					=> 'Cost',
	'discount' 				=> 'Discount',
	'extra-cost' 			=> 'Extra Cost',
	'generate-new-package' 	=> 'Generate new package',
	'here' 					=> 'here',
	'name' 					=> 'Name',
	'note' 					=> 'Note',
	'optional-documents' 	=> 'Optional Documents',
	'packages' 				=> 'Packages',
	'reason' 				=> 'Reason',
	'required-documents' 	=> 'Required Documents',
	'same-day-filing' 		=> 'Same Day Filing',
	'save' 					=> 'Save',
	'service' 				=> 'Service',

	//Sentences
	'check-all-required-documents' 							=> 'Check all required documents',
	'if-you-want-to-view-all-services-at-once-please-click' => 'If you want to view all services at once, please click',
	'please-select-at-least-one-member' 					=> 'Please select at least one member',
	'please-select-at-least-one-service' 					=> 'Please select at least one service',
	'please-select-from-required-documents' 				=> 'Please select from required documents',

];