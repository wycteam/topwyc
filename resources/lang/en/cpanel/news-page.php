<?php

return [
	'advanced-settings' => 'Advanced Settings',
	'allow-comments' => 'Allow Comments',
	'clear' => 'Clear',
	'dont-leave-this-area-empty' => 'Don\'t leave this area empty',
	'featured' => 'Featured',
	'featured-image' => 'Featured Image',
	'filtered-from' => 'filtered from',
	'home' => 'Home',
	'news' => 'News',
	'news-and-events' => 'News and Events',
	'news-editor' => 'News editor',
	'next' => 'Next',
	'prev' => 'Prev',
	'private' => 'Private',
	'save' => 'Save',
	'title' => 'Title',
	'total-entries' => 'total entries',
	'update' => 'Update'
];