﻿<?php

return [
	'title' => 'WISHLIST',
	'product-name' => 'PRODUCT NAME',
	'stock-status' => 'STOCK STATUS',
	'price' => 'PRICE',
	'add-cart' => 'Add to cart',
	'no-item' => 'There are no items in your wishlist',
	'are-you-sure' => 'Are you sure you want to remove this product from wishlist?',
	'remove' => 'Remove',	
	'removed' => 'Removed from Wishlist!',
	'not-added' => 'Not Added to cart.',
	'added' => 'Added to Cart.'
];