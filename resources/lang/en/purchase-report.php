<?php

return [
	'purchase-report'	=>	'PURCHASE REPORT'	,
	'recently-bought'	=>	'Recently Bought'	,
	'tracking-number'	=>	'Tracking No.'	,
	'product'	=>	'Product'	,
	'placed-on'	=>	'Placed On'	,
	'shipping-fee' => 'Shipping Fee',
	'total'	=>	'Total'	,
	'status'	=>	'Status'	,
	'see-more'	=>	'See More'	,
	'delivery-info'	=>	'DELIVERY INFORMATION'	,
	'shipped-to'	=>	'Shipped To'	,
	'address'	=>	'Address'	,
	'nearest-landmark'	=>	'Nearest Landmark'	,
	'contact-no'	=>	'Contact Number'	,
	'alt-contact-no'	=>	'Alternate Contact Number'	,
	'notes'	=>	'Notes'	,
	'by'	=>	'By'	,
	'qualifications'	=>	'QUALIFICATIONS'	,
	'what-in-box'	=>	'What\'s in the box'	,
	'total-summary'	=>	'TOTAL SUMMARY'	,
	'unit-price'	=>	'Unit Price'	,
	'you-save'	=>	'You save'	,
	'now'	=>	'Now'	,
	'qty'	=>	'QTY'	,

	'return-msg-1'	=>	'This order has been returned on'	, //This order has been returned on <date> with Case No.  <case number>
	'return-msg-2'	=>	'with Case No.'	, //This order has been returned on <date> with Case No.  <case number>

	'reason-return'	=>	'Reason for Returning'	,
	'return-image'	=>	'IMAGES FROM THE BUYER'	,
	'in-transit'	=>	'In Transit'	,
	'cancelled'	=>	'Cancelled'	,
	'pending'	=>	'Pending'	,
	'received'	=>	'Received'	,
	'delivered'	=>	'Delivered'	,
	'returned'	=>	'Returned'	,
	'brand'	=>	'Brand'	,
	'weight'	=>	'Weight'	,
	'return-videos'	=>	'VIDEOS FROM THE BUYER'	,
	'browser-no-html-support'	=>	'Your browser does not support HTML5 video.'	,
	'return-item'	=>	'Return Item'	,
	'reason-return-err'	=>	'Please enter the reason for returning the item.'	,
	'title-return-err'	=>	'Please enter title for returning the item.',
	'submit-return-msg-err'	=>	'Submit first your reason before uploading images/videos.'	,
	'submit'	=>	'Submit'	,
	'close'	=>	'Close'	,
	'no-purchase-msg'	=>	'You don\'t have purchases yet.'	,
	'mark-received'	=>	'Mark as received'	,
	'mark-return'	=>	'Mark as returned'	,

	'in-transit-on-1'	=>	'In Transit on'	, //In Transit on <received date> *use on between dates
	'in-transit-on-2'	=>	' '	, //In Transit on <received date> *use on between dates

	'drop-files'	=>	'Drop files here to upload'	,
	'enter-reason'	=>	'Enter Reason'	,
	'delivered-on'	=>	'Delivered on'	,

	'received-on-1'	=>	'Received on'	, //Received on <received date> *use on between dates
	'received-on-2' => 	' ', //Received on <received date> *use on between dates

	'contact-customer-support-1'	=>	'You may contact our'	, //You may contact our Customer Support regarding this matter.
	'contact-customer-support-2'	=>	'Customer Support'	, //You may contact our Customer Support regarding this matter.
	'contact-customer-support-3'	=>	'regarding this matter.'	, //You may contact our Customer Support regarding this matter.

	'no-notes-msg'	=> 'No notes',
	'mark-received-msg'	=>	'You marked your order as received.'	,
	'update-failed-msg'	=>	'Update Failed.'	,
	'you-sure-msg'	=>	'Are you sure?'	,

	'mark-receive-msg-1'	=>	'Once you mark your order as received you will not be able to return the item. Please refer to the'	, //Once you mark your order as received you will not be able to return the item. Please refer to the <a href="/faq" target="_blank">Item Return Policy</a>
	'mark-receive-msg-2'	=>	'Item Return Policy.'	, //Once you mark your order as received you will not be able to return the item. Please refer to the <a href="/faq" target="_blank">Item Return Policy</a>

	'success'	=>	'Success'	,
	'yes' => 'Yes',
	'color' => 'Color',
	'size' => 'Size',
	'return-product' => 'Return Product',
	'not-enough' => 'You do not have enough balance to return this product.',
	'add-balance' => 'Please add balance to your ewallet.',
	'airway-bill' => 'Airway Bill',
	'deliver-to' => 'DELIVER TO',
	'area-code' => 'Area Code',
	'collected' => 'Amount to be collected COD',
	'awb' => 'AWB No.',
	'dimensions' => 'Dimensions (cms)',
	'order-date' => 'Order Date',
	'pieces' => 'Pieces',
	'package-type' => 'Package Type',
	'sr' => 'Sr.', 
	'item-code' => 'Item Code',
	'item-desc' => 'Item Description',
	'quantity' => 'Quantity',
	'value' => 'Value',
	'inclusive' => 'Inclusive of all taxes and shipping charges',
	'received-by' => 'Received by',
	'printed' => 'Printed Name and Signature',
	'relationship' => 'Relationship',
	'date-received' => 'Date Received',
	'attempt' => 'Attempt Reason',
	'1st' => '1st',
	'2nd' => '2nd',
	'3rd' => '3rd',
	'print' => 'Print',
	'shipchar' => 'Shipping Fee & Valuation Charge',
	'vat' => 'VAT',
	'products' => 'Products',
	'total-price-including-vat' => 'Total Price including VAT',
	'subtotal' => 'Subtotal',
	'discount' => 'Discount',
	
	//new
	'i-receive' => 'I received the item',
	'i-return'  => 'I want to return this item',

	'photo-evidence' => 'PHOTO EVIDENCE',
	'video-evidence' => 'VIDEO EVIDENCE'

];
