﻿<?php

return [
	'title' => 'PRODUCT COMPARISON',
	'stock' => 'Stock',
	'description' => 'Description',
	'add-cart' => 'Add to cart',
	'remove' => 'Remove',	
	'no-item' => 'There are no items in your comparison',
	'are-you-sure' => 'Are you sure you want to remove this product?',
	'removed' => 'Removed from Compare!',
	'not-added' => 'Not Added to cart.',
	'added' => 'Added to Cart.'
];