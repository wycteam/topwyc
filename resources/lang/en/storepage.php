<?php
return [
	'add-prod' => 'Add Product',
	'edit-store' => 'Edit Store',
	'delete-store' => 'Delete Store',
	'description' => 'Description',
	'address' => 'Address',
	'contact' => 'Contact Number',
	'company' => 'Company',
	'company-address' => 'Company Address',
	'facebook' => 'Facebook',
	'twitter' => 'Twitter',
	'instagram' => 'Instagram',
	'ratings' => 'Ratings',
	'products' => 'Products',
	'draft' => 'Draft',
	'edit' => 'Edit',
	'delete' => 'Delete',
];