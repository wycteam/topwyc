<?php

return [

	'header-1'	=>	'Life and work made simple.'	,
	'subheader-1'	=>	' We want you to spend time with your family by giving you the convenience of online shopping.'	,
	'about-wataf'	=>	'ABOUT WATAF'	,
	'wataf-shop'	=>	'WATAF Shopping'	,
	'desc-1'	=>	'is an online shop where all products listed coming from different business owners. Individual sellers to big companies can open their own shops to provide effortless and convenient shopping experience.'	,
	'desc-2'	=>	'In this site, you will find limitless options for your needs and wants. In just one seating you can get your shopping done.'	,
	'social-head'	=>	'SOCIAL RESPONSIBILITY'	,
	'integ-head'	=>	'INTEGRITY'	,
	'conv-head'	=>	'CONVENIENCE'	,
	'social-desc'	=>	'As a company, we want to give opportunities to each aspiring entrepreneurs,thus, making registration in the website free.'	,
	'integ-desc'	=>	'We will only work for organizations or individuals that are prepared to uphold the values of transparency and honesty in the course of our work for them.'	,
	'conv-desc'	=>	'We not only simplify the shopping process for you, but also constantly innovate to make the process quick and easy.'	,
	'header-2'  =>  'WHY CHOOSE US?',
	

];