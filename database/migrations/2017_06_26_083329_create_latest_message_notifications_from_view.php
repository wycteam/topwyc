<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLatestMessageNotificationsFromView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW latest_message_notifications_from AS (
                SELECT MAX(id) AS id, from_id
                FROM notifications
                WHERE type = 'App\\\\Notifications\\\\NewChatMessage'
                GROUP BY from_id
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS latest_message_notifications_from');
    }
}
