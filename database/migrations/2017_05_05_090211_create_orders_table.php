<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('orders', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();

            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on(new Expression($dbName . '.users'));

            $table->unsignedInteger('address_id');
            $table->foreign('address_id')
                ->references('id')
                ->on(new Expression($dbName . '.addresses'));

            $table->decimal('total',8,2);
            $table->decimal('subtotal',8,2);
            $table->decimal('tax',8,2);
            $table->decimal('shipping_fee',8,2);
            $table->decimal('charge',8,2);
            $table->string('tracking_number');
            $table->enum('status', ['Complete', 'Incomplete', 'Cancelled']);
            $table->string('name_of_receiver');
            $table->string('contact_number');
            $table->string('alternate_contact_number')->nullable();
            $table->decimal('balance',9,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('orders');
    }
}
