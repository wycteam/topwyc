<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class CreateProductReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        Schema::connection('shopping')->create('product_reports', function (Blueprint $table) {
            $databaseName = DB::connection('mysql')->getDatabaseName();

            $table->unsignedInteger('issue_id');
            $table->foreign('issue_id')
                ->references('id')
                ->on(new Expression($databaseName.'.issues'))
                ->onDelete('cascade');

            $table->unsignedInteger('product_id');
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->timestamps();

            $table->primary(['issue_id', 'product_id']);
            $table->index(['issue_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('product_reports');
    }
}
