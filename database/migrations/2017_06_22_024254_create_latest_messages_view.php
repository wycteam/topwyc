<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLatestMessagesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW latest_messages AS (
                SELECT
                    id,
                    user_id,
                    lmf.chat_room_id,
                    body,
                    seen_at,
                    read_at,
                    lmf.created_at
                FROM latest_messages_from AS lmf
                JOIN messages
                USING (id)
                ORDER BY id DESC
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS latest_messages');
    }
}
