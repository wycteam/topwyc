<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableShopUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('shop_users', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();

            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on(new Expression($dbName . '.users'))
                ->onDelete('cascade');

            $table->string('sn');
            $table->string('avatar')->default('image_default.jpg');
            $table->tinyInteger('online')->default(0);
            $table->enum('status', ['None','Both','Individual','Enterprise'])->default('None');

            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('shop_users');
    }
}
