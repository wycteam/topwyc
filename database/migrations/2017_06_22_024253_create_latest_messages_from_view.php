<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLatestMessagesFromView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW latest_messages_from AS (
                SELECT
                    max(id) AS id,
                    chat_room_id,
                    max(created_at) AS created_at
                FROM
                    messages
                GROUP BY
                    chat_room_id
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS latest_messages_from');
    }
}
