<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('friends', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                  ->references('id')
                  ->on(new Expression($dbName . '.users'))
                  ->onDelete('cascade');

            $table->unsignedInteger('user_id2');
            $table->foreign('user_id2')
                  ->references('id')
                  ->on(new Expression($dbName . '.users'))
                  ->onDelete('cascade');

            $table->enum('status',['Request','Blocked','Restricted','Friend'])->default('Request');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('friends');
    }
}
