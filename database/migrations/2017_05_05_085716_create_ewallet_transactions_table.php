<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class CreateEwalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('ewallet_transactions', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();
            
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on(new Expression($dbName . '.users'))
                ->onDelete('cascade');

            $table->unsignedInteger('processor_id')->references('id')->nullable()
                ->on(new Expression($dbName . '.users'))
                ->onDelete('cascade');
                
            $table->unsignedInteger('rcvr_id')->references('id')->nullable()
                ->on(new Expression($dbName . '.users'))
                ->onDelete('cascade');

            $table->enum('type',['Withdraw','Deposit','Transfer','Received']);
            $table->string('ref_number');
            $table->decimal('amount',9,2);
            $table->longText('description')->nullable();
            
            $table->enum('status',
                ['Pending'
                ,'Completed'
                ,'Processing'
                ,'Cancelled'
                ,'On Hold']
            )->default('Pending');

            $table->decimal('balance',9,2);
            $table->string('deposit_slip')->nullable();
            $table->string('reason')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('ewallet_transactions');
    }
}
