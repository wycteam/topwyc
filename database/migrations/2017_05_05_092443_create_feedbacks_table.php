<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('feedbacks', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();

            $table->increments('id');
            $table->unsignedInteger('parent_id')->default(0);

            $table->unsignedInteger('user_id')->references('id')->nullable()
                ->on(new Expression($dbName . '.users'))
                ->onDelete('cascade');
                
            $table->unsignedInteger('processor_id')->references('id')->nullable()
                ->on(new Expression($dbName . '.users'))
                ->onDelete('cascade');

            $table->text('content');
            $table->boolean('approved')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('feedbacks');
    }
}
