<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('stores', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();

            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                  ->references('id')
                  ->on(new Expression($dbName . '.users'))
                  ->onDelete('cascade');

            $table->string('slug');
            $table->string('name');
            $table->string('cn_name')->nullable();
            $table->text('description')->nullable();
            $table->text('cn_description')->nullable();
            $table->string('email')->nullable();
            $table->string('status')->nullable();
            $table->text('reason')->nullable();
            $table->boolean('is_active')->default(true);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('stores');
    }
}
