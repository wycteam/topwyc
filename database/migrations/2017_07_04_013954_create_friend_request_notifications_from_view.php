<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendRequestNotificationsFromView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW friend_request_notifications_from AS (
                SELECT MAX(id) AS id, from_id
                FROM notifications
                WHERE type = 'App\\\\Notifications\\\\NewFriendRequest'
                GROUP BY from_id
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS friend_request_notifications_from');
    }
}
