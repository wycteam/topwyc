<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('products', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();

            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on(new Expression($dbName . '.users'))
                ->onDelete('cascade');

            $table->unsignedInteger('store_id');
            $table->foreign('store_id')
                ->references('id')
                ->on('stores')
                ->onDelete('cascade');

            $table->string('slug');
            $table->string('name');
            $table->string('cn_name')->nullable();
            $table->longText('description')->nullable();
            $table->longText('cn_description')->nullable();
            $table->longText('boxContent')->nullable();
            $table->string('brand')->nullable();
            $table->string('wPeriod')->nullable();
            $table->enum('package_type', ['Pouch','Box'])->default('Pouch');
            $table->string('weight')->nullable();
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('shipping_fee')->nullable();
            $table->string('charge')->nullable();
            $table->string('vat')->nullable();

            $table->string('shipping_fee_2go')->nullable();
            $table->string('vat_2go')->nullable();
            $table->boolean('fee_included')->default(false);

            $table->unsignedSmallInteger('stock')->default(0);
            $table->unsignedSmallInteger('sold_count')->default(0);

            $table->decimal('price', 9, 2);
            $table->decimal('sale_price', 9, 2)->nullable();
            $table->unsignedSmallInteger('discount')->nullable();

            $table->string('shipment_type')->nullable()->default('Pickup');

            $table->boolean('is_draft')->default(false);
            $table->boolean('is_active')->default(true);
            $table->boolean('is_approved')->default(true);
            $table->boolean('is_searchable')->default(true);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('product_variations');
        Schema::connection('shopping')->dropIfExists('products');
    }
}
