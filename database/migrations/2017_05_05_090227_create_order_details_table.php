<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('order_details', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('order_id');
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');

            $table->unsignedInteger('product_id');
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->string('tracking_number')->nullable();

            $table->decimal('price_per_item',8,2);
            $table->decimal('sale_price',8,2);
            $table->unsignedInteger('discount')->nullable();
            $table->unsignedInteger('quantity');
            $table->boolean('fee_included')->default(false);
            $table->string('disc_fee')->nullable();
            $table->string('shipping_fee')->nullable();
            $table->string('charge')->nullable();
            $table->string('vat')->nullable();
            $table->decimal('subtotal',8,2);
            $table->unsignedInteger('color_id')->default(0);
            $table->string('color')->nullable();
            $table->string('image')->nullable();
            $table->unsignedInteger('size_id')->default(0);
            $table->string('size')->nullable();
            $table->enum('status', ['Pending', 'Pending Request', 'In Transit', 'Delivered', 'Received', 'Returned', 'Cancelled']);
            $table->string('shipment_type')->nullable();
            $table->string('weight')->nullable();
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('courier')->nullable(); // LBC, 2Go, etc.
            $table->longText('remarks')->nullable();
            $table->string('case_number')->nullable(); //case number for return items
            $table->longText('seller_notes')->nullable();
            $table->longText('reason')->nullable();
            $table->string('pickup_location');
            $table->timestamp('shipped_date')->nullable();
            $table->timestamp('received_date')->nullable();
            $table->timestamp('returned_date')->nullable();

            //the date the seller received the returned item
            $table->timestamp('returned_received_date')->nullable();
            
            //when cancelled or returned == balance of the buyer
            //when Delivered or Received == balance of the seller
            $table->decimal('balance',9,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('order_details');
    }
}
