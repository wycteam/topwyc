<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relationships', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('a_user_id');
            $table->foreign('a_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->unsignedInteger('b_user_id');
            $table->foreign('b_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->unsignedInteger('action_user_id');
            $table->string('type')->default('Friend'); // Friend, Family: brother, sister, etc...
            $table->string('status')->default('Pending'); // Pending, Accepted, Declined, Blocked
            $table->timestamps();

            $table->unique(['a_user_id', 'b_user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relationships');
    }
}
