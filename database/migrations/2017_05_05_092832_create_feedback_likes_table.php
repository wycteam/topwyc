<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class CreateFeedbackLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('feedlikes', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on(new Expression($dbName . '.users'))
                ->onDelete('cascade');

            $table->unsignedInteger('feedback_id');
            $table->foreign('feedback_id')
                  ->references('id')
                  ->on('feedbacks')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('feedlikes');
    }
}
