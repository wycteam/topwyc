<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDiscountTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

    {

        // DB::unprepared('CREATE TRIGGER `shopping`.`ProductDiscountTrigger` BEFORE INSERT ON `shopping`.`products` FOR EACH ROW

        //         BEGIN

        //             SET NEW.discount = ROUND(((1 - (NEW.sale_price / NEW.price)) * 100), 0);

        //         END');

        // DB::unprepared('CREATE TRIGGER `shopping`.`ProductDiscountTrigger2` BEFORE UPDATE ON `shopping`.`products` FOR EACH ROW

        //         BEGIN
                
        //             SET NEW.discount = ROUND(((1 - (NEW.sale_price / NEW.price)) * 100), 0);

        //         END');

    }

    public function down()

    {

        // DB::unprepared('DROP TRIGGER `shopping`.`ProductDiscountTrigger`');
        // DB::unprepared('DROP TRIGGER `shopping`.`ProductDiscountTrigger2`');

    }
}
