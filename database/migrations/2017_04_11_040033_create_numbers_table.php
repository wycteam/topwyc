<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numbers', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('numberable_id');
            $table->string('numberable_type');

            $table->string('type')->nullable(); // Home, Work, Shipping, etc...
            $table->string('area_code')->nullable();
            $table->string('number');
            $table->string('number_type')->default('Mobile'); // Mobile Or Landline
            $table->string('country')->default('Philippines');
            $table->text('remarks')->nullable();
            $table->boolean('is_active')->default(false);

            $table->timestamps();

            $table->index(['numberable_id', 'numberable_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numbers');
    }
}
