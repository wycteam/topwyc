<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('addressable_id');
            $table->string('addressable_type');

            $table->string('type')->nullable(); // Home, Work, Shipping, etc..
            $table->string('label')->nullable();
            $table->text('address');
            $table->string('barangay')->nullable();
            $table->string('city')->nullable();
            $table->string('city_id')->nullable();
            $table->string('province')->nullable();
            $table->string('province_id')->nullable();
            $table->string('landmark')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('country')->default('Philippines');
            $table->text('remarks')->nullable();
            $table->string('name_of_receiver')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('alternate_contact_number')->nullable();
            $table->boolean('is_active')->default(false);

            $table->timestamps();

            $table->index(['addressable_id', 'addressable_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
