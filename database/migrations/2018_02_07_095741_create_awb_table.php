<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awbs', function (Blueprint $table) {
            $dbName = DB::connection('mysql')->getDatabaseName();
            $table->increments('id');
            $table->string('code');
            $table->string('order_id');
            $table->string('product_id');
            $table->string('included'); // order details id included
            $table->decimal('total_price',8,2);
            $table->decimal('wataf_cost',8,2);
            $table->decimal('twogo_cost',8,2);
            $table->decimal('shipping_fee',8,2)->nullable();
            $table->timestamps();
        });
        DB::update("ALTER TABLE awbs AUTO_INCREMENT = 205;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awbs');
    }
}
