<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

    {

        DB::unprepared('CREATE TRIGGER `shopping`.`categoryTrigger` BEFORE INSERT ON `shopping`.`categories` FOR EACH ROW

                BEGIN

                SET NEW.slug = LOWER(REPLACE(REPLACE(REPLACE(REPLACE(NEW.name," ","-"),"\'",""),"-",""),"&",""));

                END');

        DB::unprepared('CREATE TRIGGER `shopping`.`categoryTrigger2` BEFORE UPDATE ON `shopping`.`categories` FOR EACH ROW

                BEGIN

                SET NEW.slug = LOWER(REPLACE(REPLACE(REPLACE(REPLACE(NEW.name," ","-"),"\'",""),"-",""),"&",""));

                END');

    }

    public function down()

    {

        DB::unprepared('DROP TRIGGER `shopping`.`categoryTrigger`');
        DB::unprepared('DROP TRIGGER `shopping`.`categoryTrigger2`');

    }
}
