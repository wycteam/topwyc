<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChatRoomView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW user_chat_room AS (
                SELECT chat_room.id,
                    chat_room.name,
                    a.user_id AS a_user_id,
                    b.user_id AS b_user_id
                FROM chat_room_user AS a
                JOIN chat_room_user AS b ON a.chat_room_id = b.chat_room_id AND a.user_id != b.user_id
                JOIN chat_rooms AS chat_room ON b.chat_room_id = chat_room.id
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS user_chat_room');
    }
}
