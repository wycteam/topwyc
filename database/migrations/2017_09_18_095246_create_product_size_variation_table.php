<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSizeVariationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shopping')->create('product_variation_sizes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('product_id');
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
            $table->unsignedInteger('color_id');
            $table->foreign('color_id')
                ->references('id')
                ->on('product_variation_colors')
                ->onDelete('cascade');
            $table->unsignedSmallInteger('count_id');     
            $table->string('label');
            $table->unsignedSmallInteger('stock')->default(0);
            $table->unsignedSmallInteger('sold_count')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shopping')->dropIfExists('product_variation_sizes');
    }
}
