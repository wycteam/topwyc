<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreMonthYearOrderCountSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection('shopping')->unprepared("
            CREATE PROCEDURE sp_store_month_year_order_count(IN store_id INT (10), IN year SMALLINT (4))
            BEGIN
                SELECT
                    DATE_FORMAT(od.created_at, '%m') AS MONTH,
                    DATE_FORMAT(od.created_at, '%Y') AS YEAR,
                    COUNT(*) AS TOTAL_ORDERS
                FROM
                    order_details AS od
                JOIN products AS p ON od.product_id = p.id
                JOIN stores AS s ON p.store_id = s.id
                WHERE
                    s.id = store_id AND DATE_FORMAT(od.created_at, '%Y') = year
                AND (
                    od.status = 'Shipped'
                    OR od.status = 'Delivered'
                )
                GROUP BY
                    DATE_FORMAT(created_at, '%m'),
                    DATE_FORMAT(created_at, '%Y')
                ORDER BY
                    MONTH, YEAR;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection('shopping')->unprepared('DROP PROCEDURE IF EXISTS sp_store_month_year_order_count');
    }
}
