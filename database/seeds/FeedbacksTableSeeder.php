<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Shopping\Feedbacks;

class FeedbacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();

        $num_picks = rand(2,5);
        $faker = Faker\Factory::create();

        for($i=0; $i<$num_picks; $i++)
        {
            $num_comments = rand(2,4);
            
        	$feed = Feedbacks::create([
			        	 'user_id'		=> $faker->randomElement($users->pluck('id')->all())
						,'content'		=> $faker->paragraph(2)
                        ,'parent_id'    => 0
						,'approved'		=> true
						,'processor_id'	=> 1
			        ]);
            
            
        	for($j=0; $j<$num_comments; $j++)
        	{
        		Feedbacks::create([
		        	 'user_id'		=> $faker->randomElement($users->pluck('id')->all())
					,'parent_id'	=> $feed->id
					,'content'		=> $faker->paragraph(1)
					,'approved'		=> true
					,'processor_id'	=> 1
		        ]);
        	}
        }

       
    }
}
