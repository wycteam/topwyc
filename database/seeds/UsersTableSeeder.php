<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'email' => 'jason.tan656@gmail.com',
            'first_name' => 'Jason',
            'last_name' => 'Tan',
            'gender' => 'Male',
        ]);

        factory(User::class)->create([
            'email' => 'chacha.smith656@gmail.com',
            'first_name' => 'Chacha',
            'last_name' => 'Smith',
            'gender' => 'Female',
        ]);

        factory(User::class)->create([
            'email' => 'rochelle.marieseno656@gmail.com',
            'first_name' => 'Rochelle',
            'last_name' => 'Marie',
            'gender' => 'Female',
        ]);

        factory(User::class)->create([
            'email' => 'victoria.flores656@gmail.com',
            'first_name' => 'Rhea',
            'last_name' => 'Flores',
            'gender' => 'Female',
        ]);

        factory(User::class)->create([
            'email' => 'support2@wataf.ph',
            'password' => '123support',
            'first_name' => 'English',
            'last_name' => 'Doe',
            'gender' => 'Female',
        ]);

        factory(User::class)->create([
            'email' => 'support3@wataf.ph',
            'password' => '123support',
            'first_name' => 'English',
            'last_name' => 'John',
            'gender' => 'Male',
        ]);

        factory(User::class)->create([
            'email' => 'xylena.derez656@gmail.com',
            'first_name' => 'Xylena',
            'last_name' => 'Derez',
            'gender' => 'Female',
        ]);

        factory(User::class)->create([
            'email' => 'jeffrey.gatpandan656@gmail.com',
            'first_name' => 'Jeffrey',
            'last_name' => 'Gatpandan',
            'gender' => 'Male',
        ]);

        factory(User::class)->create([
            'email' => 'mario.corpus656@gmail.com',
            'first_name' => 'Mario',
            'last_name' => 'Corpus',
            'gender' => 'Male',
        ]);

        factory(User::class)->create([
            'email' => 'jayar.gabriel656@gmail.com',
            'first_name' => 'Jay-Ar',
            'last_name' => 'Gabriel',
            'gender' => 'Male',
        ]);

        // factory(User::class)->create([
        //     'email' => 'john.doe@gmail.com',
        //     'first_name' => 'John',
        //     'last_name' => 'Doe',
        //     'gender' => 'Male',
        // ]);

        // factory(User::class, 35)->create();
    }
}
