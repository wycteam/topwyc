<?php

use Illuminate\Database\Seeder;
use App\Models\Shopping\Order;
use App\Models\User;
use Faker\Factory as Faker;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = User::whereHas('roles', function ($query) {
        	 $query->whereName('shop-user');
        })->get();

        $faker = Faker::create();
        $rand_num = rand(10, 20);
        $status = ['Complete', 'Incomplete'];

        foreach ($users as $value) {
            for ($i=0; $i<=$rand_num; $i++) {
                $address = $value->addresses()->whereType('home')->first();
                $orderNum = "OR-" . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9);
                $contactNum = rand(0,9) . rand(0,9) .rand(0,9) . "-" . rand(0,9) . rand(0,9) .rand(0,9) .rand(0,9);
                $full_name = $faker->firstName . $faker->lastname;

                $order = order::create([
                     'user_id'                  => $value->id
                    ,'address_id'               => $address->id
                    ,'total'                    => $faker->randomElement([899, 1299, 1500])
                    ,'subtotal'                 => $faker->randomElement([899, 1299, 1500])
                    ,'tax'                      => $faker->randomElement([899, 1299, 1500])
                    ,'shipping_fee'             => $faker->randomElement([899, 1299, 1500])
                    ,'tracking_number'          => $orderNum
                    // ,'status'                   => $faker->randomElement($status)
                    ,'status'                   => 'Complete'
                    ,'name_of_receiver'         => $full_name
                    ,'contact_number'           => $contactNum
                    ,'alternate_contact_number' => $contactNum
                ]);
            }
        }


    }
}
