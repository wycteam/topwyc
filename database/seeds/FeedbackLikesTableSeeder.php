<?php
use App\Models\Shopping\Feedbacks;
use App\Models\Shopping\FeedbackLikes;
use App\Models\User;
use Illuminate\Database\Seeder;

class FeedbackLikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();
        
        $faker = Faker\Factory::create();

        $feedbacks = Feedbacks::get();

        foreach ($feedbacks as $key => $value) {
	        $likes = rand(1,sizeof($users));
        	for($i=2; $i<$likes; $i++)
        	{
		        FeedbackLikes::create([
		        	 'user_id'		=> $users[$i]['id']
					,'feedback_id'	=> $value->id
		        ]);
        	}
        }
    }
}
