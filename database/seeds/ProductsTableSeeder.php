<?php

use App\Models\User;
use Faker\Factory as Faker;
use App\Models\Shopping\Store;
use Illuminate\Database\Seeder;
use App\Models\Shopping\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = Faker::create();

        // $userIds = User::whereHas('roles', function ($query) {
        //     $query->whereName('shop-user');
        // })->get()
        //     ->pluck('id')
        //     ->toArray();

        // $stores = Store::with('categories')->get();

        // $stores->each(function ($store) use ($faker, $userIds) {
        //     factory(Product::class, mt_rand(5, 20))->create([
        //         'user_id' => $store->user_id,
        //         'store_id' => $store->id
        //     ])->each(function ($product) use ($faker, $userIds) {
        //         foreach ($userIds as $userId) {
        //             $product->ratings()->create([
        //                 'user_id' => $userId,
        //                 'rating' => mt_rand(1, 5),
        //                 'remarks' => $faker->paragraph(3),
        //             ]);
        //         }
        //     });
        // });

        Product::create(
            [
                'id'          =>  1 ,
                'user_id'     => '8',
                'store_id'    => '1',
                'slug'        => 'iphone-7-128gb-red',
                'name'        => 'Iphone 7 128gb red',
                'description' => 'Magnam assumenda in laborum quas. Harum tenetur deserunt et reiciendis quo aut hic. Ut atque rerum voluptatum nesciunt sint similique. Nisi voluptate natus eligendi officia aut mollitia in.',
                'boxContent'  => 'It is packed in the standard Apple retail box. Inside the box, there is an iPhone 7 Plus, Apples Lightning EarPods and a 3.5mm adapter. The new Lightning EarPods comes without 3.5mm jack support.',
                'brand'       => 'Apple',
                'wPeriod'     => '2 year unit warranty',
                'package_type' => 'Box',
                'weight'       => '0.2',
                'length'       => '15',
                'width'        => '7.6',
                'height'       => '10',
                'shipping_fee' => '80',
                'charge'       => '9',
                'vat'          => '11',
                'shipping_fee_2go' => '75',
                'vat_2go'          => '9.96',
                'fee_included'     => false,
                'stock'     => '80',
                'sold_count'     => '0',
                'price'     => '899.00',
                'discount'     => '0',
                'shipment_type'     => 'Pickup',

            ]);
    }
}
