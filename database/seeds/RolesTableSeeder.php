<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['label' => 'Master']); // 1
        Role::create(['label' => 'Vice-Master']); // 2
        Role::create(['name' => 'cpanel-admin', 'label' => 'Control Panel Administrator']); // 3
        Role::create(['name' => 'human-resources', 'label' => 'Human Resources']); // 4
        //Role::create(['name' => 'shop-admin', 'label' => 'Shopping Administrator']);
        Role::create(['name' => 'support', 'label' => 'Support']); // 5
        //Role::create(['name' => 'cpanel-user', 'label' => 'Control Panel Website User']);
        Role::create(['name' => 'vendor', 'label' => 'Shopping Website Vendor']); // 6
        Role::create(['name' => 'shop-user', 'label' => 'Shopping Website User']); // 7
        Role::create(['name' => 'employee', 'label' => 'Employee']); // 8
        Role::create(['name' => 'visa-client', 'label' => 'Visa Client']); // 9
        Role::create(['name' => 'authorizer', 'label' => 'Authorizer']); // 10
        Role::create(['name' => 'company-courier', 'label' => 'Company Courier']); // 11


    }
}
