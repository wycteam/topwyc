<?php

use App\Models\User;
use App\Models\Shopping\Store;
use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $vendors = User::whereHas('roles', function ($query) {
        //     $query->whereName('vendor');
        // })->get();

        // $vendors->each(function ($user) {
        //     factory(Store::class, mt_rand(1, 5))->create([
        //         'user_id' => $user->id
        //     ]);
        // });

        Store::create(
            [
                'id'          =>  1 ,
                'user_id'     => '8',
                'slug'        => 'apple-store',
                'name'        => 'Apple Store',
                'description' => 'Apple Store is a chain of retail stores owned and operated by Apple Inc. The stores sell Mac personal computers, iPhone smartphones, iPad tablet computers, iPod portable media players, Apple Watch smartwatches, Apple TV digital media players, software, and select third-party accessories.',
                'email'    => 'jeffrey.gatpandan656@gmail.com',
            ]);
    }
}
