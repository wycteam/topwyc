<?php

use App\Models\User;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionUserPivotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shopPermissionIds = Permission::whereIn('type', ['Store', 'Product', 'Category', 'Order'])->get()
            ->pluck('id')
            ->toArray();

        $users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();

        $users->each(function ($user) use ($shopPermissionIds) {
            $user->givePermission($shopPermissionIds);
        });
    }
}
