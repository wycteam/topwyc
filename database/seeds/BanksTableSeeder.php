<?php

use Illuminate\Database\Seeder;
use App\Models\Shopping\Banks;



class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banks::create([
             'name'             => 'Security Bank'
            ,'shortname'        => 'SB'
            ,'account_number'   => '000013005582'
            ,'active'           => 1
        ]);
        Banks::create([
    		 'name' 		    => 'Banco De Oro'
            ,'shortname'        => 'BDO'
    		,'account_number'	=> '000500401306'
    		,'active'		    => 1
    	]);
    	Banks::create([
    		 'name' 		    => 'Bank of the Philippine Islands'
            ,'shortname'        => 'BPI'
    		,'account_number'	=> ''
    		,'active'		    => 1
    	]);
    	Banks::create([
    		 'name' 		    => 'Metrobank'
            ,'shortname'        => 'MB'
    		,'account_number'	=> ''
    		,'active'		    => 1
    	]);

    }
}
