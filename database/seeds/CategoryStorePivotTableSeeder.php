<?php

use Faker\Factory as Faker;
use App\Models\Shopping\Store;
use Illuminate\Database\Seeder;
use App\Models\Shopping\Category;

class CategoryStorePivotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $categoryIds = Category::where('parent_id', 0)->get()
            ->pluck('id')
            ->toArray();

        $stores = Store::all();

        // $stores->each(function ($store) use ($faker, $categoryIds) {
        //     $store->categories()->attach($faker->randomElements($categoryIds));
        // });

        $stores->each(function ($store) use ($faker, $categoryIds) {
            $store->categories()->attach('68');
        });
    }
}
