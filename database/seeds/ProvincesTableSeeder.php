<?php

use Illuminate\Database\Seeder;
use App\Models\Province;

class ProvincesTableSeeder extends Seeder
{
 
    public function run()
    {
		// Province::create(['id'=>	1	,'name'=> 	'Abra'	, 'region'=>	'CAR - Cordillera Administrative Region'	]);
		// Province::create(['id'=>	2	,'name'=> 	'Agusan del Norte'	, 'region'=>	'13 - Caraga Region'	]);
		// Province::create(['id'=>	3	,'name'=> 	'Agusan del Sur'	, 'region'=>	'13 - Caraga Region'	]);
		// Province::create(['id'=>	4	,'name'=> 	'Aklan'	, 'region'=>	'6 - Western Visayas'	]);
		// Province::create(['id'=>	5	,'name'=> 	'Albay'	, 'region'=>	'5 - Bicol Region'	]);
		// Province::create(['id'=>	6	,'name'=> 	'Antique'	, 'region'=>	'6 - Western Visayas'	]);
		// Province::create(['id'=>	7	,'name'=> 	'Apayao'	, 'region'=>	'CAR - Cordillera Administrative Region'	]);
		// Province::create(['id'=>	8	,'name'=> 	'Aurora'	, 'region'=>	'3 - Central Luzon'	]);
		// Province::create(['id'=>	9	,'name'=> 	'Basilan'	, 'region'=>	'ARMM - Autonomous Region in Muslim Mindanao'	]);
		// Province::create(['id'=>	10	,'name'=> 	'Bataan'	, 'region'=>	'3 - Central Luzon'	]);
		// Province::create(['id'=>	11	,'name'=> 	'Batanes'	, 'region'=>	'2 - Cagayan Valley'	]);
		// Province::create(['id'=>	12	,'name'=> 	'Batangas'	, 'region'=>	'4A - CALABARZON'	]);
		// Province::create(['id'=>	13	,'name'=> 	'Benguet'	, 'region'=>	'CAR - Cordillera Administrative Region'	]);
		// Province::create(['id'=>	14	,'name'=> 	'Biliran'	, 'region'=>	'8 - Eastern Visayas'	]);
		// Province::create(['id'=>	15	,'name'=> 	'Bohol'	, 'region'=>	'7 - Central Visayas'	]);
		// Province::create(['id'=>	16	,'name'=> 	'Bukidnon'	, 'region'=>	'10 - Northern Mindanao'	]);
		// Province::create(['id'=>	17	,'name'=> 	'Bulacan'	, 'region'=>	'3 - Central Luzon'	]);
		// Province::create(['id'=>	18	,'name'=> 	'Cagayan'	, 'region'=>	'2 - Cagayan Valley'	]);
		// Province::create(['id'=>	19	,'name'=> 	'Camarines Norte'	, 'region'=>	'5 - Bicol Region'	]);
		// Province::create(['id'=>	20	,'name'=> 	'Camarines Sur'	, 'region'=>	'5 - Bicol Region'	]);
		// Province::create(['id'=>	21	,'name'=> 	'Camiguin'	, 'region'=>	'10 - Northern Mindanao'	]);
		// Province::create(['id'=>	22	,'name'=> 	'Capiz'	, 'region'=>	'6 - Western Visayas'	]);
		// Province::create(['id'=>	23	,'name'=> 	'Catanduanes'	, 'region'=>	'5 - Bicol Region'	]);
		// Province::create(['id'=>	24	,'name'=> 	'Cavite'	, 'region'=>	'4A - CALABARZON'	]);
		// Province::create(['id'=>	25	,'name'=> 	'Cebu '	, 'region'=>	'7 - Central Visayas'	]);
		// Province::create(['id'=>	26	,'name'=> 	'Compostela Valley'	, 'region'=>	'11 - Davao Region'	]);
		// Province::create(['id'=>	27	,'name'=> 	'Cotabato'	, 'region'=>	'12 - SOCCSKSARGEN'	]);
		// Province::create(['id'=>	28	,'name'=> 	'Davao del Norte'	, 'region'=>	'11 - Davao Region'	]);
		// Province::create(['id'=>	29	,'name'=> 	'Davao del Sur'	, 'region'=>	'11 - Davao Region'	]);
		// Province::create(['id'=>	30	,'name'=> 	'Davao Occidental'	, 'region'=>	'11 - Davao Region'	]);
		// Province::create(['id'=>	31	,'name'=> 	'Davao Oriental'	, 'region'=>	'11 - Davao Region'	]);
		// Province::create(['id'=>	32	,'name'=> 	'Dinagat Islands'	, 'region'=>	'13 - Caraga Region'	]);
		// Province::create(['id'=>	33	,'name'=> 	'Eastern Samar'	, 'region'=>	'8 - Eastern Visayas'	]);
		// Province::create(['id'=>	34	,'name'=> 	'Guimaras'	, 'region'=>	'6 - Western Visayas'	]);
		// Province::create(['id'=>	35	,'name'=> 	'Ifugao'	, 'region'=>	'CAR - Cordillera Administrative Region'	]);
		// Province::create(['id'=>	36	,'name'=> 	'Ilocos Norte'	, 'region'=>	'1 - Ilocos Region'	]);
		// Province::create(['id'=>	37	,'name'=> 	'Ilocos Sur'	, 'region'=>	'1 - Ilocos Region'	]);
		// Province::create(['id'=>	38	,'name'=> 	'Iloilo'	, 'region'=>	'6 - Western Visayas'	]);
		// Province::create(['id'=>	39	,'name'=> 	'Isabela'	, 'region'=>	'2 - Cagayan Valley'	]);
		// Province::create(['id'=>	40	,'name'=> 	'Kalinga'	, 'region'=>	'CAR - Cordillera Administrative Region'	]);
		// Province::create(['id'=>	41	,'name'=> 	'La Union'	, 'region'=>	'1 - Ilocos Region'	]);
		// Province::create(['id'=>	42	,'name'=> 	'Laguna'	, 'region'=>	'4A - CALABARZON'	]);
		// Province::create(['id'=>	43	,'name'=> 	'Lanao del Norte'	, 'region'=>	'10 - Northern Mindanao'	]);
		// Province::create(['id'=>	44	,'name'=> 	'Lanao del Sur'	, 'region'=>	'ARMM - Autonomous Region in Muslim Mindanao'	]);
		// Province::create(['id'=>	45	,'name'=> 	'Leyte'	, 'region'=>	'8 - Eastern Visayas'	]);
		// Province::create(['id'=>	46	,'name'=> 	'Maguindanao'	, 'region'=>	'ARMM - Autonomous Region in Muslim Mindanao'	]);
		// Province::create(['id'=>	47	,'name'=> 	'Marinduque'	, 'region'=>	'4B - MIMAROPA'	]);
		// Province::create(['id'=>	48	,'name'=> 	'Masbate'	, 'region'=>	'5 - Bicol Region'	]);
		Province::create(['id'=>	49	,'name'=> 	'Metro Manila'	, 'region'=>	'NCR - National Capital Region'	]);
		// Province::create(['id'=>	50	,'name'=> 	'Misamis Occidental'	, 'region'=>	'10 - Northern Mindanao'	]);
		// Province::create(['id'=>	51	,'name'=> 	'Misamis Oriental'	, 'region'=>	'10 - Northern Mindanao'	]);
		// Province::create(['id'=>	52	,'name'=> 	'Mountain Province'	, 'region'=>	'CAR - Cordillera Administrative Region'	]);
		// Province::create(['id'=>	53	,'name'=> 	'Negros Occidental'	, 'region'=>	'6 - Western Visayas'	]);
		// Province::create(['id'=>	54	,'name'=> 	'Negros Oriental'	, 'region'=>	'7 - Central Visayas'	]);
		// Province::create(['id'=>	55	,'name'=> 	'Northern Samar'	, 'region'=>	'8 - Eastern Visayas'	]);
		// Province::create(['id'=>	56	,'name'=> 	'Nueva Ecija'	, 'region'=>	'3 - Central Luzon'	]);
		// Province::create(['id'=>	57	,'name'=> 	'Nueva Vizcaya'	, 'region'=>	'2 - Cagayan Valley'	]);
		// Province::create(['id'=>	58	,'name'=> 	'Occidental Mindoro'	, 'region'=>	'4B - MIMAROPA'	]);
		// Province::create(['id'=>	59	,'name'=> 	'Oriental Mindoro'	, 'region'=>	'4B - MIMAROPA'	]);
		// Province::create(['id'=>	60	,'name'=> 	'Palawan'	, 'region'=>	'4B - MIMAROPA'	]);
		// Province::create(['id'=>	61	,'name'=> 	'Pampanga'	, 'region'=>	'3 - Central Luzon'	]);
		// Province::create(['id'=>	62	,'name'=> 	'Pangasinan'	, 'region'=>	'1 - Ilocos Region'	]);
		// Province::create(['id'=>	63	,'name'=> 	'Quezon'	, 'region'=>	'4A - CALABARZON'	]);
		// Province::create(['id'=>	64	,'name'=> 	'Quirino'	, 'region'=>	'2 - Cagayan Valley'	]);
		// Province::create(['id'=>	65	,'name'=> 	'Rizal'	, 'region'=>	'4A - CALABARZON'	]);
		// Province::create(['id'=>	66	,'name'=> 	'Romblon'	, 'region'=>	'4B - MIMAROPA'	]);
		// Province::create(['id'=>	67	,'name'=> 	'Samar'	, 'region'=>	'8 - Eastern Visayas'	]);
		// Province::create(['id'=>	68	,'name'=> 	'Sarangani'	, 'region'=>	'12 - SOCCSKSARGEN'	]);
		// Province::create(['id'=>	69	,'name'=> 	'Siquijor'	, 'region'=>	'7 - Central Visayas'	]);
		// Province::create(['id'=>	70	,'name'=> 	'Sorsogon'	, 'region'=>	'5 - Bicol Region'	]);
		// Province::create(['id'=>	71	,'name'=> 	'South Cotabato'	, 'region'=>	'12 - SOCCSKSARGEN'	]);
		// Province::create(['id'=>	72	,'name'=> 	'Southern Leyte'	, 'region'=>	'8 - Eastern Visayas'	]);
		// Province::create(['id'=>	73	,'name'=> 	'Sultan Kudarat'	, 'region'=>	'12 - SOCCSKSARGEN'	]);
		// Province::create(['id'=>	74	,'name'=> 	'Sulu'	, 'region'=>	'ARMM - Autonomous Region in Muslim Mindanao'	]);
		// Province::create(['id'=>	75	,'name'=> 	'Surigao del Norte'	, 'region'=>	'13 - Caraga Region'	]);
		// Province::create(['id'=>	76	,'name'=> 	'Surigao del Sur'	, 'region'=>	'13 - Caraga Region'	]);
		// Province::create(['id'=>	77	,'name'=> 	'Tarlac'	, 'region'=>	'3 - Central Luzon'	]);
		// Province::create(['id'=>	78	,'name'=> 	'Tawi-Tawi'	, 'region'=>	'ARMM - Autonomous Region in Muslim Mindanao'	]);
		// Province::create(['id'=>	79	,'name'=> 	'Zambales'	, 'region'=>	'3 - Central Luzon'	]);
		// Province::create(['id'=>	80	,'name'=> 	'Zamboanga del Norte'	, 'region'=>	'9 - Zamboanga Peninsula'	]);
		// Province::create(['id'=>	81	,'name'=> 	'Zamboanga del Sur'	, 'region'=>	'9 - Zamboanga Peninsula'	]);
		// Province::create(['id'=>	82	,'name'=> 	'Zamboanga Sibugay'	, 'region'=>	'9 - Zamboanga Peninsula'	]);

    }

}