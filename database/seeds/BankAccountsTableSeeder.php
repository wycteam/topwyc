<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Shopping\Banks;
use App\Models\Shopping\BankAccounts;

class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();

        $banks = Banks::get();

        //sample banks for WATAF
        BankAccounts::create([
             'user_id'          => 1 // MASTER
            ,'bank_id'          => 1
            ,'account_name'     => 'WATAF'
            ,'account_number'   => 9999999
            ,'status'           => 'Approved'
        ]);
         //sample banks for WATAF
        BankAccounts::create([
             'user_id'          => 1 // MASTER
            ,'bank_id'          => 2
            ,'account_name'     => 'WATAF Shopping'
            ,'account_number'   => 9999990
            ,'status'           => 'Approved'
        ]);

        $faker = Faker\Factory::create();
        //ESA = EWALLET SHOPPING ACCOUNT
        foreach ($users as $value) {
	        $bank_id = $faker->randomElement($banks->pluck('id')->all());
	        $status = $faker->randomElement(['Pending','Approved','Approved','Approved','Approved','Approved','Denied']);
        	BankAccounts::create([
        		 'user_id'			=> $value->id
				,'bank_id' 			=> $bank_id
				,'account_name'		=> str_random(10)
				,'account_number'	=> time()
				,'status' 			=> $status
        	]);
        	BankAccounts::create([
        		 'user_id'			=> $value->id
				,'bank_id' 			=> $bank_id
				,'account_name'		=> str_random(10)
				,'account_number'	=> time()
				,'status' 			=> $status
        	]);
        }
    }
}
