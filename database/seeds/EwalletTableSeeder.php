<?php

use App\Models\User;
use App\Models\Shopping\Ewallet;
use App\Models\Shopping\ShopUsers;
use Illuminate\Database\Seeder;

class EwalletTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();

        //ESA = EWALLET SHOPPING ACCOUNT
        foreach ($users as $value) {
        	Ewallet::create([
        		 'user_id' 		=> $value->id
        		,'card_number'	=> createSN('ESA', $value->id)
        		,'amount'		=> rand(8000,10000)
        	]);
        }
    }
}
