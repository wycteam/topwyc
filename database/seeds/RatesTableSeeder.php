<?php

use App\Models\Rates;
use Illuminate\Database\Seeder;

class RatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rates::create(
            [
                'id'                =>  1 ,
                'rate_category'     => 'SAMM',
                'halfkg'    		=> '75',
                'fkg'        		=> '100',
                'excess'        	=> '30',                
                'fkg3'        		=> '190',
                'excess2'        	=> '40',
            ]);

        Rates::create(
            [
                'id'                =>  2 ,
                'rate_category'     => 'SALUZ',
                'halfkg'    		=> '85',
                'fkg'        		=> '110',
                'excess'        	=> '45',                
                'fkg3'        		=> '240',
                'excess2'        	=> '65',
            ]);

        Rates::create(
            [
                'id'                =>  3 ,
                'rate_category'     => 'SAVIS',
                'halfkg'    		=> '95',
                'fkg'        		=> '125',
                'excess'        	=> '55',                
                'fkg3'        		=> '250',
                'excess2'        	=> '70',
            ]);

        Rates::create(
            [
                'id'                =>  4 ,
                'rate_category'     => 'SAMIN',
                'halfkg'    		=> '110',
                'fkg'        		=> '130',
                'excess'        	=> '60',                
                'fkg3'        		=> '250',
                'excess2'        	=> '70',
            ]);

        Rates::create(
            [
                'id'                =>  5 ,
                'rate_category'     => 'OTDLUZ',
                'halfkg'    		=> '140',
                'fkg'        		=> '170',
                'excess'        	=> '60',                
                'fkg3'        		=> '330',
                'excess2'        	=> '110',
            ]);

        Rates::create(
            [
                'id'                =>  6 ,
                'rate_category'     => 'OTDVIS',
                'halfkg'    		=> '160',
                'fkg'        		=> '180',
                'excess'        	=> '70',                
                'fkg3'        		=> '330',
                'excess2'        	=> '110',
            ]);

        Rates::create(
            [
                'id'                =>  7 ,
                'rate_category'     => 'OTDMIN',
                'halfkg'    		=> '170',
                'fkg'        		=> '190',
                'excess'        	=> '90',                
                'fkg3'        		=> '330',
                'excess2'        	=> '110',
            ]);
    }
}
