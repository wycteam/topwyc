<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Shopping\Friends;

class FriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = User::whereHas('roles', function ($query) {
	            $query->whereName('shop-user');
	        })
	        ->get()
	        ->pluck('id')
	        ->toArray();

	    $users2 = $users; 
        $faker = Faker\Factory::create();
        
       

    	foreach ($users as $key => $value) {
		    unset($users2[array_search($value, $users2)]);
    		//relation 1
		    $status = $faker->randomElement([
				'Request','Request', 'Blocked', 'Restricted', 'Friend', 'Friend', 'Friend'
			]);

    		$random_user = $faker->randomElement($users2);
    		$user_id 	= $faker->boolean(rand(1,100))?$value:$random_user;
    		$user_id2 	= $user_id == $value?$random_user:$value;
    		
    		try
			{
				Friends::create([
	    			 'user_id' 	=> $user_id
	    			,'user_id2' => $user_id2
	    			,'status' 	=> $status
	    		]);
			}
			catch (Exception $e)
			{
				// do nothing... php will ignore and continue    
			}
			//relation 2
			$status = $faker->randomElement([
				'Request','Request', 'Blocked', 'Restricted', 'Friend', 'Friend', 'Friend'
			]);
    		$random_user = $faker->randomElement($users2);
    		$user_id 	= $faker->boolean(rand(1,100))?$value:$random_user;
    		$user_id2 	= $user_id == $value?$random_user:$value;
    		try
			{
				Friends::create([
	    			 'user_id' 	=> $user_id
	    			,'user_id2' => $user_id2
	    			,'status' 	=> $status
	    		]);
			}
			catch (Exception $e)
			{
				// do nothing... php will ignore and continue    
			}
			//relation 3
			$status = $faker->randomElement([
				'Request','Request', 'Blocked', 'Restricted', 'Friend', 'Friend', 'Friend'
			]);
    		$random_user = $faker->randomElement($users2);
    		$user_id 	= $faker->boolean(rand(1,100))?$value:$random_user;
    		$user_id2 	= $user_id == $value?$random_user:$value;
    		try
			{
				Friends::create([
	    			 'user_id' 	=> $user_id
	    			,'user_id2' => $user_id2
	    			,'status' 	=> $status
	    		]);
			}
			catch (Exception $e)
			{
				// do nothing... php will ignore and continue    
			}
    		
    	}
    }
}
