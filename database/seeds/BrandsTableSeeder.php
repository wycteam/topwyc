<?php

use Illuminate\Database\Seeder;
use App\Models\Shopping\Brands;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Brands::create(['store_id'=> 1, 'name' => '', 'slug' => '', 'image' => '']);
    }	
}
