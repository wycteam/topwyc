<?php

use Illuminate\Database\Seeder;
use App\Models\Faq;

class FaqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	/* NEW FAQs SHOULD BE ADDED AT THE BOTTOM*/
    	// ACCOUNT
    	Faq::create(['type'=>	'Account'	, 'question' => 	'How can I create an account?'	,'question_cn' =>	'我如何创建一个帐户？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How to verify my account after signing in?'	,'question_cn' =>	'如何在登录后验证我的帐户？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How can I edit my profile?'	,'question_cn' =>	'我如何编辑我的个人资料？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How to upload profile my picture?'	,'question_cn' =>	'如何上传个人资料我的照片？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'What are the appropriate photos for my profile picture?'	,'question_cn' =>	'什么是我的个人资料照片适当的照片？'	, 'answer' =>	'<p><span>Your profile picture is a part of your identity as WATAF user. Remember, it gives the buyers or sellers their first impression of you and it should be truthful representation of you. Here are the guidelines and requirements:</span></p><ul><li><span>Avoid using not a picture of you. Example: Flowers, animals, places, objects.</span></li><li><span>It should be decent and professional-looking.</span></li><li><span>Clear and with proper lighting. Not too dark, not too bright and enough to see the features of your face.</span></li><li><span> Close up and recognizable. Not too close or too far away.</span></li><li><span>The format should be JPG, JPEG, PNG with maximum size of 2MB.</span></li><li><span>The ideal size for profile picture is atleast 300x300 pixels.</span></li><li><span>Do not use a profile picture that contains other people.</span></li><li><span>Do not wear sunglasses and other accessories that covers your face.</span></li><li><span>Do not use blurry or with overlays and not clear enough pictures.</span></li><li><span>Do not use pictures that displays personal contact information.</span></li><li><span> Do not use edited pictures that misleads and obstructs your face.</span></li><li><span>Do not use profile pictures that shows portrayal or description of sexual matters or with vulgar and offensive language or materials.</span></li></ul>'	, 'answer_cn' =>	'<p> <span>您的个人资料图片是WATAF用户身份的一部分。请记住，它给买家或卖家他们的第一印象，它应该是你的真实代表。这里是指导和要求：</ span> </ p> <ul> <li> <span>避免使用不是你的照片。例如：花，动物，地点，物体。</ li> </ li> <li> <span> </ li> </ li> <li>适当的照明。不要太黑，不要太明亮，足以看到你脸上的特征。</ li> <li> <li>紧密和可辨认。 </ span> </ li> <li> <span>格式应为JPG，JPEG，PNG，最大大小为2MB。</ span> </ li> <li> <span>个人资料图片的理想尺寸至少为300x300像素。</ li> </ li> <li> <span>不要使用包含其他人的个人资料图片。</ span> </ li> <li> <span不要戴太阳镜和其他配件，以免遮盖脸部。</ span> </ li> <li> <span>不要使用模糊图案或叠加图片，也不要清晰的图片。</ li> <span>不要使用显示个人联系信息的图片。</ span> </ li> <li> <span>不要使用编辑过的图片来误导和阻碍你的脸部。</ li> > <span>不要使用描绘性表现或性描述的简介图片，或使用粗俗和令人反感的语言或材料。</ li> </ li> </ ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How to edit my account information?'	,'question_cn' =>	'如何编辑我的帐户信息？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How to edit my contact details?'	,'question_cn' =>	'如何编辑我的联系方式？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How to edit my security settings?'	,'question_cn' =>	'如何编辑我的安全设置？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How to change my password?'	,'question_cn' =>	'如何更改我的密码？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'What makes a strong password?'	,'question_cn' =>	'什么使得一个强大的密码？'	, 'answer' =>	'<ul><li><span>At least 8 characters</span></li><li><span>At least one number</span></li><li><span>At least one uppercase letter</span></li><li><span>At least one lowercase letter</span></li><li><span>At least one special character</span></li></ul>'	, 'answer_cn' =>	'<ul><li> <span>至少8个字符</ span> </ li><li> <span>至少有一个号码</ span> </ li><li> <span>至少有一个大写字母</ span> </ li><li> <span>至少一个小写字母</ span> </ li><li> <span>至少有一个特殊字符</ span> </ li></ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'Why do I have to set up security questions?'	,'question_cn' =>	'为什么我必须设置安全问题？'	, 'answer' =>	'<p><span>We use security questions to recover your account. You cannot review your answers after you set up so we advise to use a question that only you can answer but is easy to remember.</span></p>'	, 'answer_cn' =>	'<p> <span> 我们使用安全问题来恢复您的帐户。 您在设置后无法查看您的答案，因此我们建议您使用只有您可以回答但易于记忆的问题。</ span> </ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How to set up security question?'	,'question_cn' =>	'如何设置安全问题？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How to verify my account as legitimate seller?'	,'question_cn' =>	'如何验证我的帐户为合法的卖家？'	, 'answer' =>	'<p><span>You are required to submit the following documents to confirm you identity with us:</span></p><ul><li><span>NBI (Required)</span></li><li><span>Birth Certificate (Optional) &ndash; Scan an original copy of your Birth Certificate issued by NSO. For foreign merchants, submit a certified true copy issued by your government.</span></li><li><span>Government ID (Required) &ndash; any ID issued by the Philippine Government.</span></li></ul>'	, 'answer_cn' =>	'<p> <span>您需要提交以下文件以确认您与我们的身份：</ span> </ p><ul><li> <span> NBI（必填）</ span> </ li><li> <span>出生证明（可选）＆ndash; 扫描NSO签发的出生证明原件。 对于外国商人，请提交由您的政府签发的经认证的副本。</ li> </ li><li> <span>政府ID（必填）＆ndash; 菲律宾政府颁发的任何身份证明。</ li> </ li></ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How can I upload documents?'	,'question_cn' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);
		
		Faq::create(['type'=>	'Account'	, 'question' => 	'Which IDs I can use for the verification process?'	,'question_cn' =>	'我可以使用哪些ID进行验证？'	, 'answer' =>	'我如何上传文件？'	, 'answer' =>	'<ul><li><span>Passport</span></li><li><span>Driver\'s License</span></li><li><span>Social Security System (SSS ID)</span></li><li><span>Government Service Insurance System (GSIS ID)</span></li><li><span>Professional Regulation Commission (PRC ID)</span></li><li><span>Postal ID</span></li><li><span>Voter\'s ID</span></li><li><span>National Bureau of Investigation (NBI) clearance</span></li><li><span>Armed Forces of the Philippines (AFP) ID</span></li><li><span>Certification from the National Council for the Welfare of Disabled Persons (NCWDP)</span></li><li><span>Department of Social Welfare and Development (DSWD) Certification</span></li><li><span>Government Service Insurance System (GSIS) e-Card</span></li><li><span>OFW ID</span></li><li><span>Overseas Workers Welfare Administration (OWWA) ID</span></li><li><span>Police Clearance Certificate</span></li><li><span>Seaman\'s Book</span></li><li><span>Senior Citizen Card</span></li><li><span>Unified Multi-Purpose ID (UMID)</span></li></ul>'	, 'answer_cn' =>	'<ul><li> <跨度>护照</跨度> </li><li> <span>驾驶执照</ span> </ li><li> <span>社会安全系统（SSS ID）</ span> </ li>政府服务保险系统（GSIS ID）</ span> </ li>专业管理委员会（PRC ID）</ span> </ li><li> <span>邮政编号</ span> </ li><li> <span>选民ID </ span> </ li><li> <span>国家调查局（NBI）许可</ span> </ li>菲律宾武装部队（法新社）ID </ span> </ li>全国残疾人福利理事会（NCWDP）认证</ span> </ li><li> <span>社会福利与发展部（DSWD）认证</ span> </ li>政府服务保险系统（GSIS）电子卡片</ span> </ li><li> <span> OFW ID </ span> </ li>海外工人福利管理（OWWA）ID </ span> </ li><li> <span>警方清关证明</ span> </ li><li> <span>海员手册</ span> </ li><li> <span>老年人卡</ span> </ li>统一多用途ID（UMID）</ span> </ li></ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How can I check the status of my uploaded documents?'	,'question_cn' =>	'我如何检查我上传的文件的状态？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'What if I didn’t provide the required documents?'	,'question_cn' =>	'如果我没有提供所需的文件呢？'	, 'answer' =>	'<p><span> As a seller, you can still create stores and upload product but the following features are disabled:</span></p><ul><li><span> You cannot sell your products. Shop users can see your product but cannot add to their shopping cart.</span></li><li><span> You cannot reply on customer\'s feedback or posts on your product.</span></li><li><span> Customers cannot send a message to you.</span></li></ul>'	, 'answer_cn' =>	'<p> <span> 作为卖家，您仍然可以创建商店并上传产品，但以下功能已被禁用：</ span> </ p><ul><li> <span>您不能销售您的产品。 商店用户可以看到您的产品，但不能添加到他们的购物车。</ li> </ li><li> <span>您不能回复客户的反馈或在您的产品上张贴。</ span> </ li><li> <span>客户无法向您发送消息。</ span> </ li></ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Account'	, 'question' => 	'How can I recover my account using Security Questions?'	,'question_cn' =>	'如何使用安全问题恢复我的帐户？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		// STORE

		Faq::create(['type'=>	'Store'	, 'question' => 	'What is the step by step process of opening a store?'	,'question_cn' =>	'开店的步骤是怎样的？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Store'	, 'question' => 	'How can I edit my store?'	,'question_cn' =>	'我如何编辑我的商店？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Store'	, 'question' => 	'How can I delete my store?'	,'question_cn' =>	'我怎样才能删除我的商店？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Store'	, 'question' => 	'How long is the promotion period?'	,'question_cn' =>	'推广期有多久？'	, 'answer' =>	'<p>Wataf.ph is offering a plan for promotion, a minimum of 1 month for both product and store. For further information you can talk to our support team.</p>'	, 'answer_cn' =>	'<p>Wataf.ph提供促销计划，产品和商店至少需要1个月的时间。 有关更多信息，请联系我们的支持团队。</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Store'	, 'question' => 	'What are the plans and fees for promoting my product?'	,'question_cn' =>	'推广我的产品有什么计划和费用？'	, 'answer' =>	'<p>In Customer page support you can talk to our support team through chat to send a request to promote your product or to set a meeting for further information about the terms, conditions and prices about promoting your store.</p>'	, 'answer_cn' =>	'<p>在客户页面支持中，您可以通过聊天与我们的支持小组进行沟通，以发送促销产品的请求或设置会议，以了解关于促销您的商店的条款，条件和价格的更多信息。</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Store'	, 'question' => 	'Can I create multiple stores?'	,'question_cn' =>	'我可以创建多个商店吗？'	, 'answer' =>	'<p>Wataf.ph is a shopping platform that allows you to create multiple stores. </span></span><span>It can be the best way to be organized if you want to open a store for a specific category. For example you have a store exclusive only for:</span></p><ul><li><span><span>Women fashion</span></span></li><li><span>Gadgets</span></li><li><span><span>Kitchen Tools</span></span></li></ul>'	, 'answer_cn' =>	'<p>Wataf.ph是一个购物平台，允许您创建多个商店。 </ span> </ span> <span>如果您想打开特定类别的商店，这可能是组织的最佳方式。 例如，您只有专门店：</ span></p><ul><li> <span> <span>女性时尚</ span> </ span> </ li><li> <跨度>小</跨度></li><li> <span> <span>厨房工具</ span> </ span> </ li></ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Store'	, 'question' => 	'How to manage my stores?'	,'question_cn' =>	'如何管理我的商店？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);


		// PRODUCTS
		Faq::create(['type'=>	'Product'	, 'question' => 	'How to upload a product from Store Management?'	,'question_cn' =>	'如何从商店管理上传产品？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'How to upload a product from Store Page?'	,'question_cn' =>	'如何从商店页面上传产品？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'How can I edit my product listing?'	,'question_cn' =>	'我如何编辑我的产品列表？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'How can I delete my product listing?'	,'question_cn' =>	'我如何删除我的产品列表？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'Can I upload any type of product?'	,'question_cn' =>	'我可以上传任何类型的产品吗？'	, 'answer' =>	'<p>Uploading any Class A or Replica is not prohibited, <strong>GIVEN ONLY THAT IT IS STATED ON THE PRODUCT NAME AND PRODUCT DESCRIPTION</strong>. Any malicious selling of these kinds of products will be subjected for account termination.</p>'	, 'answer_cn' =>	'<p>不允许上传任何A类或副本，<strong>仅在产品名称和产品描述</ strong>上注明。 任何对这些产品的恶意销售都将被终止帐户。</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'Can I upload unbranded products?'	,'question_cn' =>	'我可以上传非品牌产品吗？'	, 'answer' =>	'<p>Yes. Unbranded products are allowed on the website given that an honest and detailed description is provided.</p>'	, 'answer_cn' =>	'<p>是。由于提供了一个诚实和详细的描述，所以没有品牌的产品被允许在网站上。</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'Can I upload pre-loved or 2nd hand products?'	,'question_cn' =>	'我可以上传预先喜爱或第二手产品吗？'	, 'answer' =>	'<p>Yes. 2nd hand products are also allowed on the website given that it is stated on the product name. An honest and detailed description should be provided. </p>'	, 'answer_cn' =>	'<p>是。第二手产品也被允许在网站上，因为它是在产品名称上注明的。 应该提供一个诚实和详细的描述。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'Can I upload food and beverages?'	,'question_cn' =>	'我可以上传食物和饮料吗？'	, 'answer' =>	'<p>Food and beverages are allowed. Just follow these guidelines:</p><ul><li>Correct packaging is followed (canned, bottled, boxed and properly sealed).</li><li>Contents/Ingredients stated in the description and label on the food and beverage should be the same.</li><li>Production and Expiry dates provided in the description and label of the food and beverage should be the same.</li><li>Make sure you comply with the local health department&rsquo;s regulations. For more information please visit the FDA&rsquo;s Food Industry Page.</li></ul>'	, 'answer_cn' =>	'<p>食物和饮料是允许的。只要按照这些指导方针：</p><ul><li>遵循正确的包装（罐装，瓶装，盒装和正确密封）。</li><li>食品和饮料的说明和标签中的内容/成分应该相同。</li><li>食品和饮料的描述和标签中提供的生产日期和到期日期应该相同。</ li><li>确保您遵守当地卫生部门的规定。 欲了解更多信息，请访问FDA的食品工业页面。</li></ul> '	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'Can I upload services?'	,'question_cn' =>	'我可以上传服务吗？'	, 'answer' =>	'<p>Yes. Services are allowed to be sold in the website. Just provide the following informations in the description of the service:</p><ul><li>Highlights and full details of the service</li><li>Complete instructions on how to redeem the service</li><li>Reservation instructions</li><li>Validity period of the service</li><li>Inclusions and exclusions (tax, service charge, discounts)</li><li>Address and contact information of the store branch</li><li>Brief Introduction of the Store and its services</li><li>Terms of use of the services</li></ul>'	, 'answer_cn' =>	'<p>是。 服务可以在网站上销售。 只需在服务描述中提供以下信息：</ p></li> <li>有关如何兑换服务的完整说明</ li><li>预订说明</ li><li>服务的有效期限</ li><li>包含和排除（税收，服务费，折扣）</ li><li>商店分店的地址和联系信息</ li><li>商店及其服务简介</ li><li>服务的使用条款</ li></ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'What are the restricted or prohibited items?'	,'question_cn' =>	'什么是限制或禁止的项目？'	, 'answer' =>	'"<ul><li>Adult Only Materials</li><li>Animals</li><li>Charity and fundraising</li><li>Contracts</li><li>Counterfeit currency and stamps</li><li>Coupons</li><li>Credit cards</li><li>Currency, selling</li><li>Drugs and drug paraphernalia</li><li>Firearms, weapons</li><li>Government documents, IDs, and licenses</li><li>Hazardous, restricted, or regulated materials &ndash; (batteries, fireworks)</li><li>Human remains and body parts</li><li>Intellectual property</li><li>Items encouraging illegal activity</li><li>Lockpicking devices</li><li>Mailing lists and personal information</li><li>Medical Items</li><li>Military items</li><li>Multi-level marketing, pyramid, and matrix programs</li><li>Pesticides</li><li>Police-related items</li><li>Prescription drugs</li><li>Prohibited services</li><li>Real estate</li><li>Recalled items</li><li>Stolen property</li></ul>"'	, 'answer_cn' =>	'<ul><li> </ li> <li>动物</ li> <li>慈善和筹款</ li> <li>合约</ li><li>伪造的货币和邮票</ li><li>优惠券</ li> <li>信用卡</ li> <li>货币，销售</ li> <li>药物和药物用具</ li> li>枪支，武器</ li><li>政府文件，身份证和执照</ li><li>危险的，受限制的或受管制的材料＆ndash; （电池，烟花）</ li> <li>遗体和身体部位</ li><li>知识产权</ li> <li>鼓励非法活动的物品</ li><li> <li>邮件列表和个人信息</ li> <li>医疗项目</ li> <li>军事项目</ li><li>多层次营销，金字塔和矩阵程序</ li> 农药</ li> <li>与警方有关的物品</ li> <li>处方药</ li><li>禁用的服务</ li> <li>不动产</ li> <li> li><li>被盗财产</ li> </ ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'How can I identify “Dangerous Goods”?'	,'question_cn' =>	'我如何识别“危险品”？'	, 'answer' =>	'<p>Any shipment capable of posing a risk to health, safety, property, or the environment which meets the definition of a dangerous good according to the International Air Transport Association (IATA).</p>'	, 'answer_cn' =>	'<p>根据国际航空运输协会（IATA），任何能够危害健康，安全，财产或符合危险货物定义的环境的货物。</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'What are the unacceptable cargoes?'	,'question_cn' =>	'什么是不可接受的货物？'	, 'answer' =>	'<ul><li>Guns & Toy Guys</li><li>Ammunitions and Explosives</li><li>Any acid substance (water battery, irritants)</li><li>Filled or Empty gas tanks/Liquefied Petroleum Gas (LPG)</li><li>Firecrackers  Flammable Items (Matches, Gas Cigarette Lighter, Paint, Ink )</li<li>Illegal drugs</li><li>Motor with fuel and oil (any kind of oil including baby oil)</li><li>Pressurized shipments (aerosol items like perfume)</li><li>Rolling cargoes and heavy equipment</li><li>Deformed bars (measuring 20 ft above)</li><li>Corrosive (e.g. Mercury, Battery Solution, Radiator Flush, Bagoong, Salt/Salty Water) Combustible materials (Fertilizers)</li><li>Toxic Materials (Cyanide, Mercury, Arsenic)</li><li>Industrial Magnets  Dangerous Goods</li><li>Client refuses crating of cargo.</li><li>Corrosive Materials</li><li>Paint  Ink (Printer Liquid Ink)</li><li>Salty Water</li><li>Stove (Gas)</li><li>Amalgam</li><li>Aerosol</li><li>Maintenance Free Batteries</li><li>Matches</li><li>Mercury</li><li>Arsenic</li><li>Cyanide</li><li>Fluorescent</li><li>Light</li><li>Radiator</li><li>Flush</li><li>Gas Cigarette</li><li>Feeder line </li><li>Urine Sample</li><li>Flashlight/stun gun with rechargeable batteries</li><li>Rechargeable Batteries</li></ul>'	, 'answer_cn' =>	'<ul> <li>枪支和玩具专家</ li> <li>弹药和爆炸物</ li> <li>任何酸性物质（水电池，刺激物）</ li> <li>填充或清空气罐/液化石油气/ li> <li>鞭炮易燃物品（火柴，煤气打火机，油漆，油墨）</ li <li>非法药物</ li> <li>含有燃料和油的电机（包括婴儿油在内的任何种类的油）< / li> <li>压缩的货物（香水等气溶胶物品）</ li> <li>滚动的货物和重型设备</ li> <li>变形的条（20英尺以上）腐蚀性例如汞，电池溶液，散热器冲洗液，表面活性剂，盐/咸水）可燃物质（肥料）</ li> <li>有毒物质（氰化物，汞，砷）</ li> li> <li>客户拒绝货物装箱。</ li> <li>腐蚀性材料</ li> <li>油墨（打印机液体油墨）</ li> <li>咸水</ li>气体</ li> <li>汞齐</ li> <li>气溶胶</ li> <li>免维护电池</ li> <li>匹配</ li> <li> <LI>砷</ LI> <LI>氰化物</ li> <li>荧光</ li> <li>光</ li> <li>散热器</ li> <li>清洗</ li> <li>气体卷烟</ li> <li> </ li> <li>尿液样本</ li> <li>带充电电池的手电筒/电击枪</ li> <li>可充电电池</ li> </ ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Product'	, 'question' => 	'How can I report products that are violating policies?'	,'question_cn' =>	'如何报告违反政策的产品？'	, 'answer' =>	'<p>If you see a product that violates one of our policies, report it to our Customer Support and if we determine that a post has violated a policy, we will contact the seller and the buyer to let them know that we\'ve removed the products for the website.</p>'	, 'answer_cn' =>	'<p>如果您发现某件产品违反了我们的任何一项政策，请将其报告给我们的客户支持部门，如果我们确定某个帖子违反了政策，我们会联系卖家和买家，让他们知道我们已经将产品 该网站。</ p>'	, 'upload_type' =>	'text'	]);

		//E-WALLET

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'What is E-Wallet?'	,'question_cn' =>	'什么是电子钱包？'	, 'answer' =>	'<p>An E-wallet refers to an electronic application that allows an individual to make electronic commercial transactions. This includes purchasing items online or in an enterprise with a computer or using a smartphone to purchase a product at an actual store. The platform will apply this service to the website for a more convenient and safer transaction process as smartphones are daily tool carried by everyone at all times.</p><p>The consumer will need to load credits equal to the amount of actual money from the website by paying it through any of our accredited sources or mechanics. The credits are used to purchase any product at any given time in our platform while the remaining unused credits don&rsquo;t have expiration date, which can be consumed at any following time. Another great convenience about this E-wallet is consumers can withdraw their credits from the platform; the credits are going to be converted into real money and be deposited to their chosen bank once request has been processed</p>'	, 'answer_cn' =>	'<p>电子钱包是指允许个人进行电子商务交易的电子应用程序。这包括在线购买物品或在具有电脑的企业购买物品或使用智能手机在实际商店购买产品。平台将这个服务应用到网站上，以便更加方便和安全的交易过程，因为智能手机是每个人随时携带的日常工具。</p><p>消费者需要通过我们认可的任何来源或机制来支付相当于网站实际金额的信用额度。 这些信用用于在我们的平台的任何给定时间购买任何产品，而剩余的未使用信用没有到期日期，其可以在任何以下时间消耗。 这个电子钱包的另一大便利就是消费者可以从平台上提取信用; 信用卡将被转换成真实的金钱，并在请求被处理后存入他们选定的银行</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'How to go on my E-Wallet Page?'	,'question_cn' =>	'如何去我的电子钱包页面？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'How to load money on my E-Wallet Thru Offline?'	,'question_cn' =>	'如何在我的电子钱包上直接加载货币？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'How to withdraw money from my E-Wallet?'	,'question_cn' =>	'如何从我的电子钱包中提款？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'How to transfer money to other Wataf.ph user?'	,'question_cn' =>	'如何将钱转移到其他Wataf.ph用户？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'How to receive money from other Wataf.ph user?'	,'question_cn' =>	'如何从其他Wataf.ph用户收到钱？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'Can I transfer money to multiple Wataf.ph users?'	,'question_cn' =>	'我可以汇款给多个Wataf.ph用户吗？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'How to check my E-Wallet balance?'	,'question_cn' =>	'如何检查我的电子钱包余额？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'How can I register my bank account to E-Wallet?'	,'question_cn' =>	'如何将我的银行账户注册到电子钱包？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'E-wallet'	, 'question' => 	'How can I edit my Bank Account details on E-Wallet?'	,'question_cn' =>	'如何在电子钱包上编辑我的银行账户详细信息？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		// CHAT

		Faq::create(['type'=>	'Chat'	, 'question' => 	'How can I view all of my messages?'	,'question_cn' =>	'我如何查看我的所有消息？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Chat'	, 'question' => 	'How to compose a message?'	,'question_cn' =>	'如何撰写邮件？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Chat'	, 'question' => 	'How to use quick reply?'	,'question_cn' =>	'如何使用快速回复？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Chat'	, 'question' => 	'Can I block a person from sending me a message?'	,'question_cn' =>	'我可以阻止某人向我发送消息吗？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Chat'	, 'question' => 	'How to unblock a person from sending me a message?'	,'question_cn' =>	'如何取消阻止某人发送消息？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		//FEEDBACK

		Faq::create(['type'=>	'Feedback'	, 'question' => 	'How to add Feedback?'	,'question_cn' =>	'如何添加反馈？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Feedback'	, 'question' => 	'How to edit Feedback?'	,'question_cn' =>	'如何编辑反馈？'	, 'answer' =>	'<p>You cannot edit your feedback.</p>'	, 'answer_cn' =>	'<p>您无法编辑您的反馈。</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Feedback'	, 'question' => 	'How to delete Feedback?'	,'question_cn' =>	'如何删除反馈？'	, 'answer' =>	'<p>You cannot delete your feedback.</p>'	, 'answer_cn' =>	'<p>您无法删除您的反馈。</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Feedback'	, 'question' => 	'How to add a comment on a feedback?'	,'question_cn' =>	'如何添加对反馈的评论？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Feedback'	, 'question' => 	'How to like a feedback?'	,'question_cn' =>	'如何喜欢反馈？'	, 'answer' =>	' '	, 'answer_cn' =>	' '	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Feedback'	, 'question' => 	'How to like a comment?'	,'question_cn' =>	'如何评论？'	, 'answer' =>	'<p>You cannot like a comment.</p>'	, 'answer_cn' =>	'<p>你不能评论。</p>'	, 'upload_type' =>	'text'	]);

		//FRIENDS

		Faq::create(['type'=>	'Friends'	, 'question' => 	'How to view requests from other users?'	,'question_cn' =>	'如何查看来自其他用户的请求？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Friends'	, 'question' => 	'How to view my friends list?'	,'question_cn' =>	'如何查看我的朋友列表？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Friends'	, 'question' => 	'How to add friends?'	,'question_cn' =>	'如何添加朋友？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Friends'	, 'question' => 	'How to confirm a friend request?'	,'question_cn' =>	'如何确认好友请求？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Friends'	, 'question' => 	'How to reject a friend request?'	,'question_cn' =>	'如何拒绝朋友请求？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Friends'	, 'question' => 	'How to cancel a friend request?'	,'question_cn' =>	'如何取消朋友请求？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Friends'	, 'question' => 	'How to block a person?'	,'question_cn' =>	'如何阻止一个人？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Friends'	, 'question' => 	'How to unblock a person?'	,'question_cn' =>	'如何疏通一个人？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		// ORDER

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to place an order?'	,'question_cn' =>	'如何下订单？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to search for a specific product?'	,'question_cn' =>	'如何搜索特定产品？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to compare products?'	,'question_cn' =>	'如何比较产品？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to add products to my wish list?'	,'question_cn' =>	'如何添加产品到我的愿望清单？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How long is the order processing time?'	,'question_cn' =>	'订单处理时间有多长？'	, 'answer' =>	'<p>Wataf.ph will provide a cut off time for placed orders to be processed for the next day shipping. All purchased products within the cut off time will be shipped the next day. All purchased products made after the cut off time will be processed by the next day and will be shipped the following day after that.</p>'	, 'answer_cn' =>	'<p>Wataf.ph将为下次发货的订单提供一个截止时间。 所有在截止时间内购买的产品将在第二天发货。 所有购买的产品将在第二天处理，并在第二天发货。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to check my purchases and status?'	,'question_cn' =>	'如何检查我的购买和状态？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'Can I change a product in my existing order?'	,'question_cn' =>	'我可以按现有订单更改产品吗？'	, 'answer' =>	'<p>You cannot cancel your existing order once purchase has been placed.</p>'	, 'answer_cn' =>	'<p>购买后，您不能取消您的现有订单。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'Do the products I buy have warranty?'	,'question_cn' =>	'我购买的产品有保修吗？'	, 'answer' =>	'<p>Wataf.ph is a 3rd party platform between the seller and buyer and doesn’t issue any product warranty. However, we do provide a 7 days payment confirmation period for initial product quality checking and testing. For the real warranty of your product, you may depend on the seller’s post.</p>'	, 'answer_cn' =>	'<p>Wataf.ph是卖方和买方之间的第三方平台，不会发布任何产品保修。 但是，我们确实提供了7天的付款确认期，用于初始产品质量检查和测试。 对于您的产品的真正保修，您可能依赖卖家的职位。</p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'Where can I check order and payment status?'	,'question_cn' =>	'我在哪里可以查询订单和付款状态？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'What will happen if I cancel an order from my store?'	,'question_cn' =>	'如果我从我的商店取消订单会发生什么？'	, 'answer' =>	'<p>Sellers are allowed maximum of 2-3 days to prepare the product orders to be delivered or the order will be cancelled and the product will be suspended. If this happens, please contact Customer Support.</p>'	, 'answer_cn' =>	'<p>卖家最多可以有2-3天的时间来准备交付的产品订单，或者订单将被取消，产品将被暂停。 如果发生这种情况，请联系客户支持。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How can I return an item?'	,'question_cn' =>	'我怎样才能退货？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'Can I cancel an order?'	,'question_cn' =>	'我可以取消订单吗？'	, 'answer' =>	'<p><strong>Buyers</strong>can not cancel their order once it has been placed. However, if the seller cancels the order placed on his/her store, the deducted payment from the buyer will automatically be refunded back to the buyer&rsquo;s E-Wallet.</p><p><strong>Sellers</strong>may cancel an order with a valid reason. However, your product will be temporarily suspended upon doing this, meaning you will not be able to sell this product to any buyers and will not be viewable to other users. You will have to contact our Customer Support to remove the suspension and provide a valid explanation. Just follow these steps:</p><ol><li>From the topmost right of the page, click the dropdown beside your name and select &lsquo;Sales Report&rsquo;.</li><li>Check for the order that needs to be cancelled and change the status to &ldquo;Cancelled&rdquo;></li><li>A pop up will appear to fill up the reason of cancellation</li><li>Click &ldquo;Submit&rdquo;</li><li>The payment made by the buyer will automatically be refunded.</li><li>Contact Customer Support for retrieval of suspended product post due to cancellation of order.</li><li>Proper investigation will be made before denying or approving your request.</li></ol><p>NOTE:<strong><em>Make sure all products posted on your stores are readily available without any damage or issues to prevent cancellation of order resulting to suspension of the specific product post. Wataf.ph makes sure all sellers can give quality products and services for best WATAF shopping experience.</em></strong></p>'	, 'answer_cn' =>	'<p><strong>买家</strong>一旦下订单就不能取消订单。但是，如果卖家取消了在他/她的商店上的订单，则从买家扣除的款项将自动退还给买家的电子钱包。</ p><p> <strong>卖家</ strong>可能有正当理由取消订单。但是，这样做会暂时中止您的产品，这意味着您将无法将此产品销售给任何买家，并且不会被其他用户查看。您将不得不联系我们的客户支持部门取消暂停并提供有效的解释。只需按照以下步骤操作即可：</ p><ol><li>在页面的最右侧，点击名称旁边的下拉菜单，然后选择“销售报告”</ li>。<li>检查需要取消的订单，并将状态更改为“已取消”</ li><li>弹出式窗口会填满取消的原因</ li><li>点击“提交”</ li><li>买方付款将自动退还。</ li><li>由于取消订单，请联系客户支持以检索暂停的产品。</li><li>在拒绝或批准您的请求之前，我们会进行适当的调查。</ li></醇><p>注意：<strong> <em>确保商店上发布的所有商品都可以随时使用，而不会有任何损坏或问题，以防止取消订单，导致暂停特定产品的发布。 Wataf.ph确保所有卖家都能提供优质的产品和服务，以获得最佳的WATAF购物体验。</ em> </ strong> </ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How can I contact the seller?'	,'question_cn' =>	'我如何联系卖家？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'What can I do with a damaged, wrong or incomplete product?'	,'question_cn' =>	'如何处理损坏，错误或不完整的产品？'	, 'answer' =>	'<p>You can ask the seller to return the item. To return an item, see ‘How to return an item?’.</p>'	, 'answer_cn' =>	'<p>您可以要求卖家退货。 要退货，请参阅“如何退货”。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'Can I refund a faulty product?'	,'question_cn' =>	'我可以退还有缺陷的产品吗？'	, 'answer' =>	'<p>Buyers can refund a faulty product by following the Return process properly.</p><ul><li>You can only return the product within the 7 days testing and quality checking period</li><li>Shipping of returned products will be shouldered by the buyer</li><li>The refund process may take a few days because of the shipping. Communication with the seller is a must</li><li><strong>Sellers</strong> can refund the buyer&rsquo;s payment once the returned item is received. Please follow these steps:</li></ul><ol><li>From the topmost right of the page, click the dropdown beside your name and select &lsquo;Sales Report&rsquo;.</li><li>Check for the order with &ldquo;Returned&rdquo; status and confirm it by clicking &ldquo;Return Received&rdquo;</li><li>Once confirmation is made, the money will automatically be refunded to the buyer&rsquo;s E-Wallet</li><li>If you need more help with this process, please contact our Customer Support.</li></ol>'	, 'answer_cn' =>	'<p>买方可以通过正确地遵循退货流程来退还有缺陷的产品。</ p> <ul> <li>您只能在7天的测试和质量检查期内退回产品</ li> <li> 由买方负担</ li> <li>退款过程可能需要几天，因为运输。 与卖家沟通是必须的</ li> <li> <strong>卖家</ strong>可以在退货收到商品后退还买家的付款。 请按照以下步骤操作：</ li> </ ul> <ol> <li>点击页面右上角的下拉菜单，然后选择“销售报告”。 该命令以“返回” 状态并通过点击“收到退货”进行确认</ li> <li>确认后，款项将自动退还给买家的电子钱包</ li> <li>如果您需要更多帮助， 这个过程，请联系我们的客户支持。</ li> </ ol>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How long is the delivery period?'	,'question_cn' =>	'交货期多久？'	, 'answer' =>	'<ul><li>For the areas within metro manila, the delivery period will take 3-5 days using 2GO courier</li><li>For the provincial areas, the delivery period will take 5-10 days using 2GO courier</li><li>For other out of service areas, the delivery period will take 7-12 days using 2G0 Courier</li><li>If your location is not a serviceable area, you may be asked to pick up your purchased product at the nearest 2GO serviceable area branch.</li></ul><p><strong>NOTE: If the product did not arrive at the maximum date of the delivery period, please contact the seller immediately for updates.</strong></p>'	, 'answer_cn' =>	'<ul> <li>马尼拉地铁内的地区，使用2GO快递服务需要3-5天的时间</ li> <li>对于省份地区，使用2GO快递服务的送达时间将需要5-10天 / li> <li>对于其他退出服务区域，使用2G0 Courier的交付周期将需要7-12天</ li> <li>如果您的位置不是可用区域，则可能会要求您提取购买 产品在最近的2GO可用区域分支。</ li> </ ul> <p> <strong>注意：如果产品未在交货期的最长日期到达，请立即联系卖家进行更新。强> </ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'Can I change the delivery address?'	,'question_cn' =>	'我可以更改送货地址吗？'	, 'answer' =>	'<p>You can change your address if you will receive your purchase at a different location if the order is not yet placed.</p><ol><li>For the Shipping Address Profile</span></li><ol><li>Select &ldquo;<strong>Home</strong>&rdquo; if you want the product to be shipped to your saved address in your Contact Details Settings. Your address will automatically fill up the needed informations.</li><li>Select &ldquo;<strong>New Profile</strong>&rdquo; if you want your product to be shipped in a different address and fill up the needed information.</li></ol><li>Review the Purchase Summary of your order if it is correct</li><li>Click &ldquo;Place Order&rdquo; to finish your transaction</li></ol><p>Note:<strong><em>You cannot change the delivery address once order has been placed.</em></strong></p>'	, 'answer_cn' =>	'<p>如果订单尚未放置，您可以在不同的地点收到您的购买地址，您可以更改您的地址。</ p><ol> <li>发货地址配置文件</ span> </ li><ol><li>选择“<strong>首页</ strong>”。 如果您希望将产品发送到“联系人详细信息”设置中保存的地址。 您的地址将自动填写所需的信息。</ li><li>选择“<strong>新建个人资料</ strong>” 如果您希望您的产品以不同的地址发货并填写所需的信息。</ li></醇><li>查看订单的购买摘要（如果正确）</ li><li>按一下[放置订单] 完成您的交易</ li></醇><p>注意：<strong> <em>订单发出后，您无法更改送货地址。</ em> </ strong> </ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How much will be the shipping fee?'	,'question_cn' =>	'运费是多少？'	, 'answer' =>	'<p>The shipping fee depends on your location and to be calculated according to the courier rates.</p>'	, 'answer_cn' =>	'<p>运费取决于您的位置，并根据快递费率进行计算。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'What is the partner courier of Wataf.ph?'	,'question_cn' =>	'什么是Wataf.ph的合作伙伴信使？'	, 'answer' =>	'<p>Wataf.ph is partnered with 2Go as a courier for a fast and reliable transaction.</p>'	, 'answer_cn' =>	'<p>Wataf.ph与2Go合作，作为快递和可靠交易的快递公司。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'What are the areas catered by the courier?'	,'question_cn' =>	'快递员迎合哪些地区？'	, 'answer' =>	'<p>You may check 2GO’s Serviceable Areas. If your location is not a serviceable area, you may be asked to pick up your purchased product at the nearest 2GO serviceable area branch.</p>'	, 'answer_cn' =>	'<p>你可以检查2GO的可用区域。 如果您的位置不是可用区域，则可能会要求您在最近的2GO可用区域分支中提取您购买的产品。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How can I track my order?'	,'question_cn' =>	'我如何追踪我的订单？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'Is there a cut off time for purchasing product?'	,'question_cn' =>	'是否有购买产品的中断时间？'	, 'answer' =>	'<p>There is a daily cut-off time for purchasing products to obtain a next day shipping service. Purchases made after the cut-off time will be considered next day purchase and will be shipped out the day after that. Same day shipping will depend on the seller. Tracking numbers will be provided once your items are shipped out.</p>'	, 'answer_cn' =>	'<p>购买产品每天有一个截止时间来获得第二天的送货服务。 截止时间后的购买将被视为次日购买，并将在第二天发货。 当天发货将取决于卖家。 跟踪号码将提供一旦你的物品运出。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to price my product?'	,'question_cn' =>	'如何定价我的产品？'	, 'answer' =>	'<p>Pricing your products properly is critical if you want to stay in the business. The key is to figure out the cost of your goods, and then add labor, overhead, packaging and shipping costs, as well as re-shipping costs for when things go wrong. Then you mark up the price enough so you can make a profit without scaring away the customers. Giving a discount will help to multiply your sales so you need to balance everything to make an honest profit.</p><ul><li>Input your total price and the discounted price</li><li>The weight and dimensions of the product is important to determine the correct calculation of the shipping cost</li><li>The Shipping cost will be calculated depending on the location of the buyer and will be added to the buyer&rsquo;s total billing payment</li></ul>'	, 'answer_cn' =>	'<p>如果你想留在业务中，正确定价你的产品是至关重要的。 关键是要弄清楚商品的成本，然后增加人工，管理费用，包装和运输成本，以及在出现问题时重新运输成本。 然后，你足够的价格标记，所以你可以赚取利润，而不会吓跑客户。 给予折扣将有助于增加销售额，所以你需要平衡一切，以实现真正的利润。输入您的总价格和折扣价格</ li> <li>产品的重量和尺寸对于确定正确的运输成本计算非常重要</ li> <li> 运输成本将根据买方的地点计算，并将被添加到买方的总计费付款中。</ li> </ ul>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to pack my product?'	,'question_cn' =>	'如何包装我的产品？'	, 'answer' =>	'<p>Please refer to 2go Packaging Policy (downloadable pdf) or (link)</p>'	, 'answer_cn' =>	'<p>请参阅2go包装政策（可下载的pdf）或（链接）</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to ship my product?'	,'question_cn' =>	'如何运送我的产品？'	, 'answer' =>	'<p>For product pickup, you need to schedule it with 2go Courier (website link) once order has been placed in your store (according to the cut off time). Once the pickup has been scheduled, you can print or send a waybill copy to your customer together with the tracking number as you change the order status to “In Transit” in your wataf.ph account’s “Sales Report” page.NOTE: A shipping cost will be held from the sellers in case of product return happens, the exact amount will be credited back once the transaction is successful and the buyer receives the item with no issues.</p>'	, 'answer_cn' =>	'<p>对于产品取件，一旦订单已存入商店（根据截止时间），您需要使用2go Courier（网站链接）进行计划。 一旦计划提货后，您可以在您的wataf.ph账户的“销售报告”页面中将订单状态更改为“正在运输”，从而将打印或将运单发送到客户以及跟踪号码。注意：如果产品退货发生，卖方将承担运费，一旦交易成功，买方收到没有问题的货物，确切数额将被退回。</ p>'	, 'upload_type' =>	'text'	]);

		Faq::create(['type'=>	'Order'	, 'question' => 	'How to check sales transactions of my store/s?'	,'question_cn' =>	'如何检查我店的销售交易？'	, 'answer' =>	''	, 'answer_cn' =>	''	, 'upload_type' =>	'picture'	]);

    }		
}
