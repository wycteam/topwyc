<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Shopping\Ewallet;
use App\Models\Shopping\EwalletTransactions;

class EwalletTransactionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();

        $admin = User::whereHas('roles', function ($query) {
            $query->whereIn('name', ['master', 'cpanel-admin', 'cpanel-user','shop-admin']);
        })->get();

        $faker = Faker\Factory::create();

        //ESA = EWALLET SHOPPING ACCOUNT
        foreach ($users as $value) {
        	$loops = rand(1,3);
        	for($i=0; $i<=$loops; $i++)
        	{
        		$type = $faker->randomElement([
        			'Withdraw', 'Deposit',
        			'Withdraw', 'Deposit',
        			'Withdraw', 'Deposit', 'Transfer'
        		]);

	        	$admins = $faker->randomElement($admin->pluck('id')->all());
	        	$rcvr = $faker->randomElement($users->pluck('id')->all());
	        	EwalletTransactions::create([
	        		 'user_id' 		=> $value->id
					,'type'			=> $type
					,'ref_number'	=> rand(1,time())
					,'amount'		=> rand(500,40000)
					,'description'	=> 'gibberish'
					,'processor_id'	=> $admins
					,'balance'		=> rand(1000,20000)
					,'deposit_slip'	=> ''
					,'rcvr_id'		=> $type=='Transfer'?$rcvr:0
	        	]);
        	}
        }
    }
}
