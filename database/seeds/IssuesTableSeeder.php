<?php

use App\Models\User;
use App\Models\Issue;
use Illuminate\Database\Seeder;

class IssuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supportUsers = User::whereHas('roles', function ($query) {
            $query->whereName('support')
                ->orWhere('name', 'master')
                ->orWhere('name', 'cpanel-admin')
                ->orWhere('name', 'cpanel-user');
        })->get();

        $users = User::whereHas('roles', function ($query) use ($supportUsers) {
            $query->where(function ($query) {
                    $query->whereName('shop-user')
                        ->orWhere('name', 'vendor');
                });
        })
        ->whereNotIn('id', $supportUsers->pluck('id')->toArray())
        ->get();

        $users->each(function ($user) use ($supportUsers) {
            factory(Issue::class)->create([
                'user_id' => $user->id,
            ])->assignSupportToChatRoom($supportUsers->random()->id);
        });
    }
}
