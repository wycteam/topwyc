<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Shopping\Product;
use App\Models\Shopping\Category;
use App\Models\Shopping\ProductCategory;
class ProductCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $faker = Faker::create();

        $categoryIds = Category::where('parent_id', 0)->get()
            ->pluck('id')
            ->toArray();

        $products = Product::all();

        // $products->each(function ($prod) use ($faker, $categoryIds) {
        //     $prod->categories()->attach($faker->randomElements($categoryIds));
        // });

        $products->each(function ($prod) use ($faker, $categoryIds) {
            $prod->categories()->attach('68');
        });
    }
}
