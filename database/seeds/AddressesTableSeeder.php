<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $users = User::all();

        $users->each(function ($user) use ($faker) {
            $user->addresses()->create([
                'type' => 'Home',
                'address' => 'Balagtas Villas',
                //'barangay' => 'Metro Manila',
                'city' => 'PASAY CITY * BARANGAY 5',
                'province' => 'Metro Manila',
                'zip_code' => rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) .rand(0,9),
                'name_of_receiver' => $faker->firstName . $faker->lastname,
                'contact_number' => '09' . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) .rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9),
                'alternate_contact_number' => '09' . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) .rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9),
                'is_active' => true
            ]);
        });
    }
}
