<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Shopping\Product;
use App\Models\Shopping\ProductReviews;

class ProductReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();

        $products = Product::get();

        $num_picks = rand(2,5);
        $faker = Faker\Factory::create();

        foreach($products as $product)
        {
            $num_comments = rand(0,2);
            for($i=0; $i<=$num_picks; $i++)
            {
	        	$review = ProductReviews::create([
				        	 'user_id'		=> $faker->randomElement($users->pluck('id')->all())
							,'content'		=> $faker->paragraph(2)
	                        ,'parent_id'    => 0
                            ,'product_id'   => $product->id
	                        ,'rating_id'    => rand(2, 4000)
				        ]);
	            
	            
	        	for($j=0; $j<$num_comments; $j++)
	        	{
	        		ProductReviews::create([
			        	 'user_id'		=> $faker->randomElement($users->pluck('id')->all())
						,'parent_id'	=> $review->id
						,'content'		=> $faker->paragraph(1)
                        ,'product_id'   => $product->id
			        ]);
	        	}
            }
            
        }

       
    }
}
