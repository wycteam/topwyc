<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Shopping\Product;
use App\Models\Shopping\Order;
use App\Models\Shopping\OrderDetail as Details;
use Faker\Factory as Faker;

class OrderDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereHas('roles', function ($query) {
        	 $query->whereName('shop-user');
        })->get();

        $prod = Product::get();
        $order = Order::get();
        $faker = Faker::create();
        $status = ['Pending', 'In Transit', 'Delivered', 'Received', 'Returned', 'Cancelled'];

        foreach ($order as $value) {
            $pr = $prod->random();
            $orderNum = rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9);

            $OrderDetail = Details::create([
                 'order_id'         => $value->id
                ,'product_id'       => $pr->id
                ,'tracking_number'  => $orderNum
                ,'quantity'         => rand(1,10)
                ,'color'            => $faker->safeColorName
                ,'price_per_item'   => $faker->randomElement([899, 1299, 1500])
                ,'sale_price'       => $faker->randomElement([899, 1299, 1500])
                ,'discount'         => $faker->randomElement([10, 20, 30])
                ,'subtotal'         => $faker->randomElement([899, 1299, 1500])
                ,'remarks'          => $faker->paragraph(2)
                // ,'status'           => $faker->randomElement($status)
                ,'status'           => 'Delivered'
                ,'seller_notes'     => $faker->paragraph(2)    
                ,'shipped_date'     => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
                ,'received_date'    => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
                ,'pickup_location'  => $faker->address
                ,'created_at'       => $faker->dateTimeThisYear($max = 'now', $timezone = date_default_timezone_get())
            ]);
        }
    }
}