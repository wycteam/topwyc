<?php
use App\Models\Shopping\ShopUsers;
use App\Models\User;
use Illuminate\Database\Seeder;

class ShopUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();

        foreach($users as $value)
        {
        	//SU = shopping user
        	ShopUsers::create([
        		 'user_id' 	=> $value->id
				,'sn'		=> createSN('SU', $value->id)
        	]);
        }
    }
}
