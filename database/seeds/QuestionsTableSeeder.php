<?php

use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::create([
            'type' => 'security',
            'question' => 'What is the first name of your best friend in high school?',
        ]);

        Question::create([
            'type' => 'security',
            'question' => 'What time of the day was your first child born?',
        ]);

        Question::create([
            'type' => 'security',
            'question' => 'What was the name of your elementary / primary school?',
        ]);

        Question::create([
            'type' => 'security',
            'question' => 'What time of the day were you born? (hh:mm)',
        ]);

        Question::create([
            'type' => 'security',
            'question' => 'When you were young, what did you want to be when you grew up?',
        ]);

        Question::create([
            'type' => 'security',
            'question' => 'Who was your childhood hero?',
        ]);

        Question::create([
            'type' => 'security',
            'question' => 'In what city or town does your nearest sibling live?',
        ]);

        Question::create([
            'type' => 'security',
            'question' => 'Who is your favorite uncle?',
        ]);
    }
}
