<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createStorePermissions();
        $this->createProductPermissions();
        $this->createCategoryPermissions();
        $this->createOrderPermissions();
    }

    private function createStorePermissions()
    {
        Permission::create(['type' => 'Store', 'name' => 'stores.list', 'label' => 'List', ]);
        Permission::create(['type' => 'Store', 'name' => 'stores.view', 'label' => 'View', ]);
        Permission::create(['type' => 'Store', 'name' => 'stores.create', 'label' => 'Create', ]);
        Permission::create(['type' => 'Store', 'name' => 'stores.update', 'label' => 'Update', ]);
        Permission::create(['type' => 'Store', 'name' => 'stores.delete', 'label' => 'Delete', ]);
    }

    private function createProductPermissions()
    {
        Permission::create(['type' => 'Product', 'name' => 'products.list', 'label' => 'List', ]);
        Permission::create(['type' => 'Product', 'name' => 'products.view', 'label' => 'View', ]);
        Permission::create(['type' => 'Product', 'name' => 'products.create', 'label' => 'Create', ]);
        Permission::create(['type' => 'Product', 'name' => 'products.update', 'label' => 'Update', ]);
        Permission::create(['type' => 'Product', 'name' => 'products.delete', 'label' => 'Delete', ]);
    }

    private function createCategoryPermissions()
    {
        Permission::create(['type' => 'Category', 'name' => 'categories.list', 'label' => 'List', ]);
        Permission::create(['type' => 'Category', 'name' => 'categories.view', 'label' => 'View', ]);
        Permission::create(['type' => 'Category', 'name' => 'categories.create', 'label' => 'Create', ]);
        Permission::create(['type' => 'Category', 'name' => 'categories.update', 'label' => 'Update', ]);
        Permission::create(['type' => 'Category', 'name' => 'categories.delete', 'label' => 'Delete', ]);
    }

    private function createOrderPermissions()
    {
        Permission::create(['type' => 'Order', 'name' => 'orders.list', 'label' => 'List', ]);
        Permission::create(['type' => 'Order', 'name' => 'orders.view', 'label' => 'View', ]);
        Permission::create(['type' => 'Order', 'name' => 'orders.create', 'label' => 'Create', ]);
        Permission::create(['type' => 'Order', 'name' => 'orders.update', 'label' => 'Update', ]);
        Permission::create(['type' => 'Order', 'name' => 'orders.delete', 'label' => 'Delete', ]);
    }
}
