<?php

use Illuminate\Database\Seeder;

use App\Models\ChatRoom;
use App\Notifications\NewChatMessage;
use App\Notifications\NewFriendRequest;
use App\Models\DatabaseNotification as notif;
use App\Models\User;
use App\Models\Shopping\Friends;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = \Faker\Factory::create();

    	$users = User::whereHas('roles', function ($query) {
    		$query->whereName('shop-user');
    	})->get();

   //  	$users->each(function ($user) use ($faker, $users) {
   //  		try
			// {
	  //   		$randomUser = $users->random();

	  //   		$chatRoom = ChatRoom::create();
	  //   		$chatRoom->addMember([$user->id, $randomUser->id]);

	  //   		$message = $randomUser->messages()->create([
	  //   			'chat_group_id' => $chatRoom->id,
	  //   			'body' => $faker->paragraph(1),
			// 	]);

	  //   		$user->notify(new NewChatMessage($randomUser, $message));
			// }
			// catch (Exception $e)
			// {
			// 	// do nothing... php will ignore and continue
			// }
   //  	});

    	$requests = Friends::whereStatus('Request')->get();

    	$requests->each(function ($request) {
    		$user1 = User::find($request->user_id);
    		$user2 = User::find($request->user_id2);

    		$user2->notify(new NewFriendRequest($request, $user1));
    	});


    //     $users = User::whereHas('roles', function ($query) {
	   //          $query->whereName('shop-user');
	   //      })
	   //      ->get()
	   //      ->pluck('id')
	   //      ->toArray();

	   //  $users2 = $users;
    //     $faker = Faker\Factory::create();

    //     for($i=0; $i<sizeof($users); $i++)
    //     {

	   //      $notif_for_user = rand(2,6);
	   //      for($j=0; $j<$notif_for_user; $j++)
	   //      {
	   //      	$data['header'] = $faker->paragraph(1);
		  //       $data['content'] = $faker->paragraph(2);
		  //       $data['data'] = "";
		  //       $type = $faker->randomElement([
				// 	 'App\Models\Shopping\Friends'
				// 	,'App\Models\Shopping\Transactions'
				// 	,'App\Models\Shopping\Ewallet'
				// 	,'App\Models\Shopping\Feedbacks'
				// 	,'App\Models\Shopping\Product'
				// 	,'App\Models\Shopping\Documents'
				// 	,'App\Models\Shopping\Store'
				// 	,'App\Models\Shopping\OrderDetail'
				// 	,'App\Models\Shopping\BankAccounts'
				// 	,'App\Models\User'
				// ]);

		  //       $random_user = $faker->randomElement($users2);
	   //  		$user_id 	= $faker->boolean(rand(1,100))?$value:$random_user;
	   //  		$user_id2 	= $user_id == $value?$random_user:$value;

	   //  		switch ($type) {
	   //  			case 'App\Models\Shopping\Friends':
				// 	case 'App\Models\Shopping\Transactions':
				// 	case 'App\Models\Shopping\Ewallet':
				// 	case 'App\Models\Shopping\Feedbacks':
				// 	case 'App\Models\Shopping\Product':
				// 	case 'App\Models\Shopping\Documents':
				// 	case 'App\Models\Shopping\Store':
				// 	case 'App\Models\Shopping\OrderDetail':
				// 	case 'App\Models\Shopping\BankAccounts':
				// 	case 'App\Models\User':

	   //  			default:
	   //  				# code...
	   //  				break;
	   //  		}

		  //       try
				// {
			 //        notif::create([
			 //        	 'type'				=> $type
			 //        	,'from_id'			=> $user_id2
			 //        	,'notifiable_id'	=> $user_id
			 //        	,'notifiable_type'	=>
			 //        	,'data'				=> $data
			 //        ]);
				// }catch (Exception $e)
				// {
				// 	// do nothing... php will ignore and continue

				// }

	   //      }
    //     }

    }
}
