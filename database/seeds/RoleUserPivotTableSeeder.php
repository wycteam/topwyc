<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class RoleUserPivotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::whereEmail('jason.tan656@gmail.com')->first()
            ->assignRole([1]); // 1 = Master

        User::whereEmail('chacha.smith656@gmail.com')->first()
            ->assignRole([2,3,5,6,7,8]); // 2 = Vice-Master

        User::whereEmail('rochelle.marieseno656@gmail.com')->first()
            ->assignRole([4,6,7]); // 9 = HR

        User::whereEmail('victoria.flores656@gmail.com')->first()
            ->assignRole([3,5,6,7,8]); // 5 = support

        User::whereEmail('support2@wataf.ph')->first()
            ->assignRole([3]);

        User::whereEmail('support3@wataf.ph')->first()
            ->assignRole([3]);

        User::where('email', 'like', '%656@gmail.com')
            ->where('email', '!=', 'jason.tan656@gmail.com')
            ->where('email', '!=', 'chacha.smith656@gmail.com')
            ->where('email', '!=', 'rochelle.marieseno656@gmail.com')
            ->where('email', '!=', 'victoria.flores656@gmail.com')
            ->get()
            ->each(function ($user) {
                $user->assignRole([3, 6, 7, 8]);
            }); // 7 = Vendor, 7 = Shop user, 8 = Employees

        $vendors = User::doesntHave('roles')->limit(5)->get();
        $vendors->each(function ($user) {
            $user->assignRole([6, 7]); 
        });

        $shoppers = User::doesntHave('roles')->get();
        $shoppers->each(function ($user) {
            $user->assignRole([7]);
        });
    }
}
