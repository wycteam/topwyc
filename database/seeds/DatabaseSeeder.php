<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RoleUserPivotTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionUserPivotTableSeeder::class);
        $this->call(PermissionRolePivotTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(AddressesTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(StoresTableSeeder::class);
        $this->call(FaqTableSeeder::class);
        $this->call(CategoryStorePivotTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        // $this->call(ProductReviewsTableSeeder::class);
        $this->call(ProductCategoryTableSeeder::class);
        $this->call(RatesTableSeeder::class);
        // $this->call(BankAccountsTableSeeder::class);
        // $this->call(FeedbacksTableSeeder::class);
        // $this->call(BrandsTableSeeder::class);
        // $this->call(FavoritesTableSeeder::class);
        // $this->call(OrdersTableSeeder::class);
        // $this->call(OrderDetailsTableSeeder::class);
        // $this->call(FriendsTableSeeder::class);
        // $this->call(NotificationsTableSeeder::class);
        // $this->call(IssuesTableSeeder::class);
    }
}
