<?php

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionRolePivotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shopPermissionIds = Permission::whereIn('type', ['Store', 'Product', 'Category', 'Order'])->get()
            ->pluck('id')
            ->toArray();

        $shopUser = Role::whereName('shop-user')->first();

        $shopUser->assignPermission($shopPermissionIds);
    }
}
