<?php

use Illuminate\Database\Seeder;
use App\Models\Shopping\Favorites;
use App\Models\User;
use App\Models\Shopping\Product;

class FavoritesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereHas('roles', function ($query) {
            $query->whereName('shop-user');
        })->get();

        $products = Product::get()->pluck('id')->toArray();

        $faker = Faker\Factory::create();


        foreach ($users as $value) {
            $favorites = rand(2,6);
            for($i=0;$i<$favorites;$i++)
            {
                $product = $faker->randomElement($products);
                
                Favorites::create([
                     'user_id'      => $value->id
                    ,'product_id'  => $product
                ]);
            }
        }
    }
}
