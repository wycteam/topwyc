<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    $gender = $faker->randomElement(['Male', 'Female']);
    $lcGender = lcfirst($gender);
    $show_display_name = $faker->randomElement([true,false]);

    return [
        'name' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('123admin'),
        'first_name' => $faker->firstName($lcGender),
        'last_name' => $faker->lastName,
        'nick_name' => $faker->firstName($lcGender),
        'gender' => $gender,
        'is_active' => true,
        'is_verified' => true,
        'remember_token' => str_random(10),
        'display_name' => $faker->userName,
        'is_display_name' => $show_display_name
    ];
});

// $factory->define(App\Models\Shopping\Store::class, function (Faker\Generator $faker) {
//     return [
//         'user_id' => 0,
//         'name' => ucwords(implode($faker->words($faker->randomElement([2, 3])), ' ')),
//         'description' => $faker->sentence($faker->randomElement(range(6, 12))),
//     ];
// });

$factory->define(App\Models\Shopping\Product::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 0,
        'store_id' => 0,
        'name' => ucwords($faker->sentence($faker->randomElement(range(4, 12)))),
        'description' => $faker->paragraph(3),
        'stock' => mt_rand(50, 100),
        'sold_count' => mt_rand(1, 100),
        'price' => $faker->randomElement([899, 1299, 1500]),
        'sale_price' => null,
        'discount' => null,
        'is_approved' => true,
    ];
});

$factory->define(App\Models\Message::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 0,
        'chat_room_id' => 0,
        'body' => $faker->paragraph(1),
    ];
});

$factory->define(App\Models\Issue::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 0,
        'subject' => $faker->sentence(4),
        'body' => $faker->paragraph(2),
        'status' => $faker->randomElement(['open', 'closed']),
    ];
});
