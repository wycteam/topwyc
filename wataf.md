# WATAF

## Code Snippets

### .env
```
WATAF_DOMAIN=wyc06.dev
WATAF_CPANEL_SUBDOMAIN=cpanel.wyc06.dev

SESSION_DRIVER=redis
SESSION_DOMAIN=.wyc06.dev
```

### config\app.php
```php
'wataf_domain' => env('WATAF_DOMAIN'),
'wataf_cpanel_subdomain' => env('WATAF_CPANEL_SUBDOMAIN'),
```

### app\Acme\Helpers.php
```php
if (! function_exists('append_routes')) {
    function append_routes($type, $site)
    {
        foreach (new DirectoryIterator(base_path() . '\routes\\' . $type) as $dir) {
            if ($dir->isDir() && ! $dir->isDot() && ($dir->getBasename() === $site)) {
                Route::group(['namespace' => ucfirst($site), 'as' => $site . '.'], function () use ($dir) {
                    foreach (File::allFiles($dir->getPathname()) as $file) {
                        require_once $file->getRealPath();
                    }
                });
            }
        }
    }
}
```

### routes\web.php
```php
Auth::routes();

foreach (new DirectoryIterator(__DIR__ . '\web') as $dir) {
    if ($dir->isFile() && ($dir->getFilename() !== '.gitignore')) {
        require_once $dir->getPathname();
    }
}

Route::group(['domain' => config('app.wataf_domain')], function () {
    append_routes('web', 'shopping');
});

Route::group(['domain' => config('app.wataf_cpanel_subdomain')], function () {
    append_routes('web', 'cpanel');
});
```

### routes\api.php
```php
foreach (new DirectoryIterator(__DIR__ . '\api') as $dir) {
    if ($dir->isFile() && ($dir->getFilename() !== '.gitignore')) {
        require_once $dir->getPathname();
    }
}

Route::group(['domain' => config('app.wataf_domain')], function () {
    append_routes('api', 'shopping');
});

Route::group(['domain' => config('app.wataf_cpanel_subdomain')], function () {
    append_routes('api', 'cpanel');
});
```

### Star Rating
```
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
    <path d="M12 6.76l1.379 4.246h4.465l-3.612 2.625 1.379 4.246-3.611-2.625-3.612 2.625 1.379-4.246-3.612-2.625h4.465l1.38-4.246zm0-6.472l-2.833 8.718h-9.167l7.416 5.389-2.833 8.718 7.417-5.388 7.416 5.388-2.833-8.718 7.417-5.389h-9.167l-2.833-8.718z"></path>
</svg>
```

### JavaScript
```js
getProp(obj, prop) {
    var arr = prop.split('.');

    while(arr.length && (obj = obj[arr.shift()]));

    return obj;
}
```

### Messages
```sql
SELECT b.id AS muid, a.uid, a.chat_room_id, b.first_name, b.last_name, b.nick_name, b.display_name
FROM (
    SELECT chat_room_id, user_id AS uid
    FROM chat_room_user
) AS a
JOIN (
    SELECT chat_room_id, u.*
    FROM chat_room_user AS cru
    JOIN users AS u
    ON u.id = cru.user_id
) AS b
ON
    b.chat_room_id = a.chat_room_id
AND
    a.uid = 8
AND
    b.id <> 8
```

### Event Broadcasting
```php
Route::get('test', function () {
    $message = new \App\Models\Message([
        'user_id' => 5,
        'chat_group_id' => 1,
        'body' => 'Lorem ipsum dolor sit amet adipiscing consectitur...',
    ]);

    $message->save();

    // event(new \App\Events\NewChatMessage($message));

    $user = \App\Models\User::find(5);
    $user->notify(new \App\Notifications\NewChatMessage($message));

    return $message;
});
```
