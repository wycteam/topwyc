<?php
Route::group(['namespace' => 'General'], function()
{
    Route::group(['prefix' => '/'], function()
    {
    	//language
    	Route::get('/locale/{locale}', 'GeneralController@setLocale');
        Route::get('translate/{file}', 'GeneralController@getTranslation');

		Route::get('/', 'GeneralController@index')->name('home');
		Route::get('home', 'GeneralController@index');
		Route::get('open-register/{id}', 'GeneralController@openRegistration');
		Route::get('open-register', 'GeneralController@openRegistration');
		Route::get('ios-page', 'GeneralController@iosPage');
		Route::get('android-page', 'GeneralController@androidPage');
		Route::get('about', 'GeneralController@about')->name('about');
		Route::get('terms-and-conditions', 'GeneralController@termsCondition')->name('termsCondition');
		Route::get('privacy-policy', 'GeneralController@privacyPolicy')->name('privacyPolicy');
		Route::get('notifications', 'GeneralController@notifications')->name('notifications');
		Route::get('favorites', 'GeneralController@favorites')->name('favorites');
		Route::get('compare', 'GeneralController@compare')->name('compare');
		Route::get('sitemap', 'GeneralController@sitemap')->name('sitemap');
		Route::group(['prefix' => 'categories'], function()
	    {
			Route::get('/', 'GeneralController@categories')->name('categories');
			Route::get('load', 'GeneralController@loadCategories')->name('load-categories');
		});
	});

	Route::group(['prefix' => 'faq'], function()
    {
		Route::get('/', 'GeneralController@faq')->name('faq');
		Route::get('contents', 'GeneralController@faqContent')->name('faq-content');
	});

	//VISA
	Route::group(['prefix' => 'services'], function()
    {
		Route::get('/', 'GeneralController@services')->name('services');
	});

	Route::get('contact_us', 'GeneralController@contactUs');

	Route::group(['prefix' => 'tracking'], function()
    {
		Route::get('/', 'GeneralController@tracking')->name('tracking');
		Route::get('result/{id}', 'GeneralController@getTracking')->name('get-tracking');
		Route::get('reports/{id}', 'GeneralController@getReports')->name('get-report');
	});

	Route::group(['prefix' => 'quiz'], function()
    {
		Route::get('/', 'GeneralController@quiz')->name('quiz');
		Route::get('submit', 'GeneralController@submitQuiz')->name('submit-quiz');
	});

	Route::group(['prefix' => 'quiz2'], function()
    {
		Route::get('/', 'GeneralController@quiz2')->name('quiz2');
		Route::get('submit', 'GeneralController@submitQuiz2')->name('submit-quiz2');
	});

  Route::group(['prefix' => 'gallery'], function()
  {
      //Route::get('/', 'GeneralController@viewGallery');
      Route::get('/albums', 'GeneralController@getAlbums');
      Route::get('/get-images', 'GeneralController@getImages');
      Route::get('/albums/{id}', 'GeneralController@showAlbum');
  });

   	Route::group(['prefix' => 'open'], function()
	  {
	  	//getTranslation
	  	Route::get('/get-translation', 'GeneralController@getOpenRegTranslation');

	      Route::get('/get-decoded-referral/{code}', 'GeneralController@getDecodedReferral');
	      Route::post('/save-registration', 'GeneralController@saveRegistration')->name('save-registration');
	      Route::post('/save-verification', 'GeneralController@saveVerification')->name('save-verification');
	      Route::post('/resend-code', 'GeneralController@resendCode')->name('resend-code');
	  });





});
Route::group(['namespace' => 'News'], function()
{
    Route::group(['prefix' => 'news'], function(){
      Route::get('/', 'NewsController@index');
      Route::get('/featured', 'NewsController@getFeatured');
      Route::get('/latest', 'NewsController@mostLatest');
      Route::get('/{id}', 'NewsController@showNews');
      Route::get('/comments/{pageId}', 'CommentController@index');
      Route::post('/comments', 'CommentController@store');
      Route::post('/comments/{commentId}/{type}', 'CommentController@update');
    });

});
