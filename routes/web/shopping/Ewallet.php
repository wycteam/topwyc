<?php
Route::group(['namespace' => 'User'], function()
{
	Route::middleware('auth')->group(function () {
		Route::group(['prefix' => 'user'], function()
	    {
			Route::group(['prefix' => 'ewallet'], function(){

				Route::get('/', 'EwalletController@index')->name('ewallet');
	            Route::post('/upload-deposit', 'EwalletController@upload')->name('upload-deposit-slip');
			});

		});
	});
});