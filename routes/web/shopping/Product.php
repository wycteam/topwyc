<?php

// Route::middleware('auth')
// 	->group(function () {
// 	Route::name('product.')
// 		->prefix('product')
// 		->group(function () {

// 	});

// 	Route::resource('product', 'Product\ProductController');
// });


Route::name('')
		->group(function () {

		Route::name('product.')
				->prefix('product')
		        ->group(function () {

		});

        //////////////////////////
        // Authenticated routes //
        //////////////////////////
		Route::middleware('auth')->group(function () {
			Route::resource('product', 'Product\ProductController',['only' => [
                    'store','destroy','edit','update','create'
                ]]);
		});


        ////////////////////////////
        // Unauthenticated routes //
        ///////////////////////////
        Route::resource('product', 'Product\ProductController',['only' => [
            'show','index'
        ]]);

});
