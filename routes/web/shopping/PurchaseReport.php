<?php
Route::group(['namespace' => 'User'], function()
{
	Route::middleware('auth')->group(function () {
		Route::group(['prefix' => 'user'], function()
	    {
			Route::group(['prefix' => 'purchase-report'], function(){
				Route::get('/', 'PurchaseReportController@index')->name('purchases');
				Route::group(['prefix' => 'get-orders'], function(){
					Route::get('/', 'PurchaseReportController@getOrders')->name('get-orders');
					Route::get('/{id}', 'PurchaseReportController@getProduct')->name('get-products');
				});
				Route::get('/recent-products', 'PurchaseReportController@getRecentProducts')->name('recent-products');
				Route::post('/update-receive/{id}', 'PurchaseReportController@updateReceive');
				Route::post('/return-item', 'PurchaseReportController@returnItem');
				Route::post('/{id}/upload-files', 'PurchaseReportController@uploadFiles');
			});
		});
	});
});