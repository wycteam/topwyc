<?php
Route::group(['namespace' => 'User'], function()
{
	Route::middleware('auth')->group(function () {
		Route::group(['prefix' => 'user'], function()
	    {
			Route::group(['prefix' => 'sales-report'], function(){
				Route::get('/', 'SalesReportController@index')->name('sales');
				Route::get('/get-store', 'SalesReportController@getStores')->name('get-store');
				Route::get('/details/{id}', 'SalesReportController@getDetails')->name('get-details');
				Route::get('/detailsByStore/{order_id}/{store_id}', 'SalesReportController@getDetailsByStore')->name('get-details-by-store');
				Route::get('/wholedetailsByStore/{order_id}/{store_id}', 'SalesReportController@getWholeDetailsByStore')->name('get-wholedetails-by-store');
				Route::get('/airway/{id}', 'SalesReportController@getAirway')->name('get-airways');
				Route::get('/updateStatus/{id}/{val}', 'SalesReportController@updateStatus')->name('update-status');
				Route::post('/update-remarks', 'SalesReportController@updateOrderRemarks')->name('update-order-remarks');
				Route::post('/update-to-transit', 'SalesReportController@updateToTransit')->name('update-to-transit');
				Route::post('/cancel-order', 'SalesReportController@cancelOrder')->name('cancel-order');
				Route::get('/checkIfSameStore/{prod_id}/{order_id}', 'SalesReportController@checkIfSameStore')->name('check-if-samestore');
				Route::get('/getProductsFromSameStore/{prod_id}/{order_id}', 'SalesReportController@getProductsFromSameStore')->name('getprod-from-samestore');			
				Route::get('/getProductsFromSameStore2/{order_id}/{store_id}', 'SalesReportController@getProductsFromSameStore2')->name('getprod-from-samestore2');			
				Route::get('/getProductIdsFromSameStore/{prod_id}/{order_id}', 'SalesReportController@getProductIdsFromSameStore')->name('getprodid-from-samestore');

			});

		});
	});
});