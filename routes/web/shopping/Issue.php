<?php

Route::middleware('auth')->group(function () {
    
    Route::resource('issues', 'IssuesController');

});