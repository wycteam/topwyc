<?php

// Route::middleware('auth')
// 	->group(function () {
// 	Route::name('stores.')
// 		->prefix('stores')
// 		->group(function () {

// 	});

// 	Route::resource('stores', 'StoresController');
// 	Route::resource('stores.products', 'StoreProductsController');
// });


Route::name('')
		->group(function () {

		Route::name('stores.')
				->prefix('stores')
		        ->group(function () {

		});

        //////////////////////////
        // Authenticated routes //
        //////////////////////////
		Route::middleware('auth')->group(function () {
			Route::resource('stores', 'StoresController',['only' => [
                    'store','destroy','edit','update','create','index'
                ]]);
		});


        ////////////////////////////
        // Unauthenticated routes //
        ///////////////////////////
		Route::resource('stores', 'StoresController',['only' => [
                    'show'
                ]]);
		Route::resource('stores.products', 'StoreProductsController');


});