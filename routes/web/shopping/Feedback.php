<?php
Route::group(['namespace' => 'General'], function()
{	
	Route::middleware('auth')->group(function () {
    	Route::resource('feedback', 'FeedbackController',  ['only' => ['index', 'post', 'show']]);
	});
});