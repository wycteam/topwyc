<?php
Route::group(['namespace' => 'User'], function()
{
	Route::group(['prefix' => 'cart'], function()
    {
		Route::get('/', 'CartController@index')->name('cart');
		Route::get('/add', 'CartController@add');
		Route::get('/add2', 'CartController@add2');
		Route::get('/view', 'CartController@view');
		Route::get('/update', 'CartController@update');
		Route::get('/updateQty', 'CartController@updateQty');
		Route::get('/delete', 'CartController@delete');
		Route::get('/getType', 'CartController@getType');
		Route::get('/getAddressDetails/{id}', 'CartController@getAddressDetails');
		Route::get('/getActive', 'CartController@getActive');
		Route::post('/checkout', 'CartController@checkout');
		Route::post('/saveProfile', 'CartController@saveProfile');
		Route::post('/deleteProfile', 'CartController@deleteProfile');
	});

	Route::get('getRates', 'RatesController@index');
});