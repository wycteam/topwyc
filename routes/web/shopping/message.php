<?php

Route::middleware('auth')
    ->group(function () {
    Route::name('messages.')
        ->prefix('messages')
        ->group(function () {

    });

    Route::resource('messages', 'MessagesController');
});
