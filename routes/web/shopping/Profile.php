<?php
Route::group(['namespace' => 'User'], function()
{	
	Route::middleware('auth')->group(function () {
		Route::group(['prefix' => 'profile'], function()
	    {	
	    	Route::get('/', 'ProfileController@index')->name('profile');
	    	Route::get('/security-questions', 'ProfileController@getSecurityQuestions')->name('security-questions');
	    	Route::get('/security-answers', 'ProfileController@getSecurityAnswer')->name('security-answers');
	    	Route::get('/provinces', 'ProfileController@getProvinces')->name('get-province');
	    	Route::get('/get-home-address', 'ProfileController@getHomeAddress')->name('get-home-address');
	    	Route::get('/get-home-numbers', 'ProfileController@getHomeNumber')->name('get-home-number');
	    	Route::get('/city/{name}', 'ProfileController@getCities')->name('get-cities');
			Route::get('/settings', 'ProfileController@settings')->name('profileSettings');
			Route::group(['prefix' => 'update'], function()
		    {	
		    	Route::post('/account-info', 'ProfileController@accountUpdate');
		    	Route::post('/address-info', 'ProfileController@addressCreate');
		    	Route::post('/home-address-info', 'ProfileController@homeAddressCreate');
		    	Route::post('/change-password', 'ProfileController@changePassword');
		    	Route::post('/answer-security-questions', 'ProfileController@answerSecurityQuestions');
			});
			Route::post('update-avatar', 'ProfileController@updateAvatar')->name('update-avatar');
			Route::post('upload-docs', 'ProfileController@uploadImage')->name('upload-image-docs');
			Route::post('doc-exist', 'ProfileController@docExist')->name('doc-exist');
			Route::get('notifications', 'ProfileController@getNotif')->name('notifications');
			
		});
	});
	
	Route::resource('users', 'UsersController');

});
