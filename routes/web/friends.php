<?php

Route::middleware('auth')
    ->group(function () {
    Route::name('friends.')
        ->prefix('friends')
        ->group(function () {

    });

    Route::resource('friends', 'FriendsController');
});
