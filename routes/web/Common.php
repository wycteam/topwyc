<?php

Route::name('common.')
    ->prefix('common')
    ->group(function () {
    Route::post('resize-image', 'CommonController@resizeImage')->name('resize-image');
    Route::post('resize-whole-image', 'CommonController@resizeWholeImage')->name('resize-whole-image');
});