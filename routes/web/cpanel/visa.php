<?php

Route::name('visa.')
    ->prefix('visa')
    ->group(function () {

        Route::get('set-locale/{lang}', 'HomeController@setLocale');
        Route::get('get-locale', 'HomeController@getLocale');

        Route::get('get-docs', 'ServiceController@getDocs');

        Route::get('get-branches', 'BranchesController@_getBranches');

        Route::group(['prefix' => 'home'], function(){
            Route::get('translation', 'HomeController@translation');
            Route::get('translation/todays-services', 'HomeController@todaysServicesTranslation');
            Route::get('translation/delivery-schedule', 'HomeController@deliveryScheduleTranslation');
            Route::get('translation/edit-service-dashboard', 'HomeController@editServiceDashboardTranslation');
            Route::get('pendingServices', 'HomeController@pendingServices');
            Route::get('servicesSummaryReport', 'HomeController@servicesSummaryReport');
            Route::get('onprocessServices', 'HomeController@onprocessServices');

            Route::get('removeService/{id}', 'HomeController@removeService');
            Route::post('markAsPending', 'HomeController@markAsPending');
            Route::get('/get-todays-tasks', 'HomeController@getTodaysTasks');
            Route::get('/showCouriers', 'HomeController@loadCompanyCouriers');
            Route::get('/get-reminders', 'HomeController@getReminders');
            Route::post('/save-delivery-schedule', 'HomeController@addDeliverySchedule');
            Route::get('/loadDelScheds', 'HomeController@loadDeliverySchedules');
            Route::post('/delSched/{id}', 'HomeController@deleteSchedule');
            Route::get('/getSchedulesbyDate', 'HomeController@getSchedulesbyDate');
            Route::post('/editSched', 'HomeController@editSchedule');
            Route::post('/getSched/{id}', 'HomeController@getSched');

        });

        Route::group(['prefix' => 'news'], function(){
            Route::get('translation', 'NewsAdminController@getTranslation');
            Route::get('/news-and-events', 'NewsAdminController@getNews')->name('news-and-events');
            Route::resource('/news-list', 'NewsAdminController');
            //Route::post('/news-list/save', 'NewsAdminController@saveNews');
        });


        Route::group(['prefix' => 'tasks'], function() {
            Route::get('/', 'TasksController@index')->name('today-tasks');
            Route::get('translation', 'TasksController@getTranslation');
        });

        Route::group(['prefix' => 'gallery'], function(){
            Route::get('translation/gallery-detail-page', 'GalleryController@getGalleryDetailPageTranslation');
            Route::get('translation/gallery-page', 'GalleryController@getGalleryPageTranslation');
            Route::get('/album-view/{id}', 'GalleryController@getGallery')->name('gallery');
            Route::get('/album-by-id/{id}', 'GalleryController@getSingleAlbum');
            Route::get('/', 'GalleryController@getAlbum')->name('album');
            Route::get('/upload','GalleryController@fileCreate');
            Route::post('/upload/store','GalleryController@fileStore');
            Route::post('/delete','GalleryController@fileDestroy');
            Route::post('/album/add','GalleryController@addAlbum');
            Route::post('/album-list/edit/{id}', 'GalleryController@albumUpdate');
            Route::get('/server-images/{id}','GalleryController@getServerImages');
            Route::resource('/album-list', 'GalleryController');
            Route::get('/newListAlbums','GalleryController@getList');
            Route::get('/featured-image/{id}','GalleryController@featuredImage');

        });

        Route::group(['prefix' => 'ads'], function(){
            Route::get('/', 'AdsController@getAds')->name('ads');
            Route::resource('/ads-list', 'AdsController');
        });

        Route::group(['prefix' => 'payroll'], function(){
            Route::get('/', 'PayrollController@payrollControl')->name('payroll');;
            Route::get('/get-department', 'PayrollController@getDepartment');
            Route::get('/get-employees/{id}', 'PayrollController@getEmployees');
            Route::get('/get-attendance', 'PayrollController@getAttendance');
            Route::get('/get-profile/{id}/{status}/{dt}', 'PayrollController@getProfile');
            Route::get('/show-list/{id}', 'PayrollController@showList');
            Route::get('/get-schedule-type', 'PayrollController@getScheduleType');
            Route::get('/get-contribution', 'PayrollController@getContribution');
            Route::get('/get-holidays', 'PayrollController@getHolidays');
            Route::get('/get-payroll-date', 'PayrollController@getPayrollDate');
            Route::get('/get-holidays-id', 'PayrollController@getHolidaysID');
            Route::get('/get-accounts', 'PayrollController@getAccounts');
            Route::get('/get-installments', 'PayrollController@getInstallments');
            Route::get('/get-payroll-log', 'PayrollController@getPayrollLogs');
            Route::get('/check-saved', 'PayrollController@checkSaved');
            Route::get('/get-payroll-count', 'PayrollController@getPayrollCount');
            Route::post('/post-installments', 'PayrollController@postInstallments');
            Route::post('/recompute', 'PayrollController@reCompute');
            Route::post('/post-holiday', 'PayrollController@postHoliday');
            Route::post('/post-holiday-request-result', 'PayrollController@postHolidayRequestResult');
            Route::resource('/control', 'PayrollController');
        });

        Route::get('tracking', 'TrackingController@index')->name('tracking');


        Route::group(['prefix' => 'report'], function(){
            Route::get('translation', 'ReportsController@translation');
            Route::get('component-translation', 'ReportsController@componentTranslation');
            ROute::get('visa-linker', 'ReportsController@getVisaLinker');
            Route::get('/report-filter', 'ReportsController@reportFilter')->name('report-filter');
            Route::get('/write-report', 'ReportsController@write')->name('write-report');
            Route::get('/select-services/{id}', 'ReportsController@selectServices');
            Route::get('/get-clients/{id}', 'ReportsController@getClients');
            Route::post('/save-report', 'ReportsController@saveReport')->name('save-report');
            Route::get('/read-reports', 'ReportsController@read')->name('read-reports');
            Route::get('/get-reports', 'ReportsController@getReports')->name('get-reports');
            Route::get('/get-reports/{group_id}/{client_id}/{service_id}', 'ReportsController@getClientReports')->name('get-client-reports');
            Route::get('/get-reports-yesterday', 'ReportsController@getReportsYesterday')->name('get-reports-yesterday');
            Route::get('/get-services/{id}', 'ReportsController@getServices')->name('get-services');
            Route::get('/get-group-summary/{id}/{type}/{lang}/{services}/{month}/{year}', 'ReportsController@getGroupSummary')->name('group-summary');
            Route::get('/get-group-summary-chinese/{id}/{type}', 'ReportsController@getGroupSummaryChinese')->name('group-summary-chinese');
            Route::get('/get-group-summary-pdf/{id}', 'ReportsController@getGroupSummaryPDF')->name('group-summary-pdf');
            Route::get('/view-group-summary-pdf/{id}', 'ReportsController@viewGroupSummaryPDF')->name('view-group-summary-pdf');
        });


         Route::group(['prefix' => 'client-services'], function(){
             Route::get('/', 'ClientServicesController@index');
             Route::get('translation', 'ClientServicesController@translation');
             Route::get('/get-today-services/{mode}', 'ClientServicesController@getTodayServices')->name('today-services');
             Route::get('/get-pending-services', 'ClientServicesController@getPendingServices')->name('pending-services');
             Route::get('/get-onprocess-services', 'ClientServicesController@getOnProcessServices')->name('on-process-services');
             Route::get('/get-service-by-date', 'ClientServicesController@getServiceByDate');
         });


         Route::group(['prefix' => 'event-calendar'], function(){
             Route::get('/', 'EventCalendarController@index')->name('events');
             Route::get('translation', 'EventCalendarController@translation');
         });



        Route::group(['prefix' => 'client'], function(){
            Route::get('/', 'ClientsController@index')->name('client');
            Route::get('translation', 'ClientsController@getTranslation');
            Route::get('translation/add-temporary-client', 'ClientsController@addTemporaryClientTranslation');
            Route::get('translation/transaction-logs', 'ClientsController@transactionLogsTranslation');
            Route::get('translation/client-documents', 'ClientsController@clientDocumentsTranslation');
            Route::get('translation/package', 'ClientsController@packageTranslation');
            Route::get('translation/view-report', 'ClientsController@viewReportTranslation');
            Route::get('translation/add-service', 'ClientsController@addServiceTranslation');
            Route::get('translation/edit-service', 'ClientsController@editServiceTranslation');
            Route::get('load-client-list/{branch}', 'ClientsController@load_client_list');
            Route::post('/export-pdf', 'ClientsController@exportPDF');


            Route::get('/validationBeforeSubmit', 'ClientsController@validationBeforeSubmit');
            Route::get('/get-authorizers', 'ClientsController@getAuthorizers');
            Route::post('/saveAuthorizer', 'ClientsController@saveAuthorizer');
            Route::get('/tracking-list', 'ClientsController@trackingList')->name('tracking_list');
            Route::get('/create', 'ClientsController@create')->name('client-create');
            Route::get('/{id}/edit', 'ClientsController@edit')->name('client-edit');
            Route::post('/store', 'ClientsController@store')->name('client-store');
            Route::post('/store2', 'ClientsController@store2')->name('client-store2');
            Route::post('/update/{id}', 'ClientsController@update')->name('client-update');
            Route::post('/updatewithemail/{id}', 'ClientsController@updatewithemail')->name('client-updatewithemail');
            Route::post('/update-avatar/{id}', 'ClientsController@updateAvatar')->name('update-avatar');
            Route::post('/upload-docs/{id}', 'ClientsController@uploadImage')->name('upload-docs');
            Route::post('/verify-docs', 'ClientsController@verify')->name('verify-docs');
            Route::get('/showGroup', 'ClientsController@showGroup');
            Route::get('/showGroup/{branch_id}', 'ClientsController@showGroup');
            Route::get('/showBranch', 'ClientsController@showBranch');
            Route::get('/{client_id}', 'ClientsController@client_profile')->name('client_profile');
            Route::get('/clientServiceCost/{client_id}', 'ClientsController@clientServiceCost');
            Route::get('/clientCompleteServiceCost/{client_id}', 'ClientsController@GetCompleteCost');
            Route::get('/clientCollectables/{client_id}', 'ClientsController@clientCompleteBalance');
            Route::get('/clientDeposit/{client_id}', 'ClientsController@clientDeposit');
            Route::get('/clientPayment/{client_id}', 'ClientsController@clientPayment');
            Route::get('/clientRefund/{client_id}', 'ClientsController@clientRefund');
            Route::get('/clientDiscount/{client_id}', 'ClientsController@clientDiscount');
            Route::get('/clientBalance/{client_id}', 'ClientsController@clientBalance');
            Route::get('/clientCommission/{client_id}', 'ClientsController@clientCommission');
            Route::get('/clientTransfer/{client_id}', 'ClientsController@clientTransfer');
            Route::get('/clientPointsEarned/{client_id}', 'ClientsController@clientPointsEarned');
            Route::get('/clientPackages/{client_id}', 'ClientsController@clientPackages');
            Route::get('/changeStatus/{client_id}/{stat}', 'ClientsController@changeStatus');
            Route::get('/clientServices/{client_id}/{tracking}', 'ClientsController@clientServices');
            Route::get('/clientAddPackage/{client_id}', 'ClientsController@clientAddPackage');
            Route::post('/clientDeletePackage', 'ClientsController@clientDeletePackage');
            Route::post('/clientAddFunds', 'ClientsController@clientAddFunds');
            Route::post('/clientAddService', 'ClientsController@clientAddService');
            Route::post('/clientEditService', 'ClientsController@clientEditService');
            Route::get('/clientGetService/{service_id}', 'ClientsController@clientGetService');
            Route::get('/clientActionLogs/{client_id}', 'ClientsController@clientActionLogs');
            Route::get('/clientTransactionLogs/{client_id}', 'ClientsController@clientTransactionLogs');
            Route::get('/clientCommissionLogs/{client_id}', 'ClientsController@clientCommissionLogs');
            Route::get('/clientDocumentLogs/{client_id}', 'DocumentLogsController@clientDocumentLogs');
            Route::get('clientDocumentLogsDetails/{client_id}', 'DocumentLogsController@clientDocumentLogsDetails');
            Route::post('quick-release', 'DocumentLogsController@quickRelease');

            Route::get('/{id}/services', 'ClientsController@services');
            Route::get('/{id}/packages', 'ClientsController@packages');
            Route::get('/search/typeahead', 'ClientsController@typeahead');
            Route::get('/search/transfer-balance/{branch_id}/{client_id}', 'ClientsController@transferBalance');
            Route::get('/search/group-transfer-balance/{branch_id}/{group_id}', 'ClientsController@groupTransferBalance');
        });

        Route::group(['prefix' => 'group'], function(){
            Route::get('checkCommissionApplication', 'GroupsController@checkCommissionApplication');
            Route::get('translation', 'GroupsController@getTranslation');
            Route::get('translation/edit-address', 'GroupsController@editAddressTranslation');
            Route::get('translation/add-new-member', 'GroupsController@addNewMemberTranslation');
            Route::get('translation/add-new-service', 'GroupsController@addNewServiceTranslation');
            Route::get('translation/edit-service', 'GroupsController@editServiceTranslation');
            Route::get('translation/add-fund', 'GroupsController@addFundTranslation');
            Route::get('translation/transfer', 'GroupsController@transferTranslation');
            Route::get('translation/group-member-type', 'GroupsController@groupMemberTypeTranslation');
            Route::get('translation/delete-member', 'GroupsController@deleteMemberTranslation');
            Route::get('translation/client-services', 'GroupsController@clientServicesTranslation');
            Route::get('translation/group-members', 'GroupsController@groupMembersTranslation');

            Route::get('load-group-list/{branch?}', 'GroupsController@load_group_list');

            Route::get('/', 'GroupsController@index')->name('group');
            Route::get('/all-balance', 'GroupsController@allBalance');
            // Route::get('/export-pdf', 'GroupsController@exportPDF');
            Route::get('/{id}/service-docs', 'GroupsController@serviceDocs');
            Route::get('/service-docs-index', 'GroupsController@serviceDocsIndex');

            Route::get('/{id}', 'GroupsController@show')->name('show-group');
            // Route::get('/{group_id}', 'GroupsController@group_profile')->name('group_profile');

            Route::get('/{id}/packages-services', 'GroupsController@packagesServices');
            Route::get('/{id}/edit-services', 'GroupsController@editServices');
            Route::get('/search/typeahead', 'GroupsController@typeahead');
            Route::get('/changeStatus/{group_id}/{stat}', 'GroupsController@changeStatus');

            Route::get('{id}/members', 'GroupsController@groupMembers');
            Route::get('{groupId}/member/{memberId}/services', 'GroupsController@groupMemberServices');
            Route::get('member/services/{clientServiceId}', 'GroupsController@groupSelectedServices');
      
            Route::post('member/services/update', 'GroupsController@updateServices');
        });


        Route::group(['prefix' => 'notification'], function(){
            Route::get('/builder', 'NotificationBuilderController@index')->name('notification-builder');

            Route::get('/report-type', 'ReportTypesController@index');
            Route::post('/report-type', 'ReportTypesController@store');
            Route::patch('/report-type/{id}', 'ReportTypesController@update');
            Route::delete('/report-type/{id}', 'ReportTypesController@destroy');

            Route::get('/doc', 'DocsController@index');
            Route::post('/doc', 'DocsController@store');
            Route::patch('/doc/{id}', 'DocsController@update');
            Route::delete('/doc/{id}', 'DocsController@destroy');

            Route::get('/services', 'NotificationBuilderController@getServices');

            Route::get('/manager', 'NotificationManagerController@index')->name('notification-manager');
            Route::get('/select-services/{id}/{rt}', 'NotificationManagerController@selectServices');
            Route::get('/get-clients/{id}', 'NotificationManagerController@getClients');
            Route::get('/getReportType', 'NotificationManagerController@getReportType');
            Route::get('/getSelectedReportType/{id}', 'NotificationManagerController@getSelectedReportType');
            Route::post('/save-report', 'NotificationManagerController@saveReport')->name('save-report');
        });

    	Route::group(['prefix' => 'service-manager'], function(){
            Route::get('translation', 'ServiceController@getListOfServicesTranslation');
            Route::get('branch-cost', 'ServiceController@getBranchCost');
            Route::get('service-profile-cost/{profile_id}/{service_id}', 'ServiceController@getProfileCost');
            Route::get('service-profile-cost2/{profile_id}/{service_id}', 'ServiceController@getProfileCost2');
            Route::get('/', 'ServiceController@index')->name('service-manager');
    		Route::post('/save', 'ServiceController@saveService');
    		Route::post('/edit', 'ServiceController@editService');
            Route::post('/editProfileCost', 'ServiceController@editProfileCost');
            Route::post('/updateProfileCost', 'ServiceController@updateProfileCost');
            Route::get('/getAllProfile', 'ServiceController@getAllProfile');
            Route::post('/saveProfile', 'ServiceController@saveProfile');
            Route::post('/updateProfile', 'ServiceController@updateProfile');
            Route::post('/saveProfileCost', 'ServiceController@saveProfileCost');
            Route::get('/sortedServices', 'ServiceController@sortedServices');
            //Route::get('/sortedServices2', 'ServiceController@sortedServices2');
            Route::get('/servProfiles', 'ServiceController@servProfiles');
    		Route::get('/show', 'ServiceController@showService');
            Route::get('/showService/{group_id}', 'ServiceController@showService2');
            Route::get('/showServiceAdded/{group_id}/{month}/{year}', 'ServiceController@showServiceAdded');
            Route::get('/showDateAdded/{group_id}/{date}', 'ServiceController@showDateAdded');
            Route::get('/getServiceProfileCost/{service_id}/{profile_id}/{branch_id}', 'ServiceController@getServiceProfileCost');
    		Route::get('/showParents', 'ServiceController@showAllParentServices');
    		Route::get('/showChildren/{id}', 'ServiceController@showAllChildren');
            Route::get('/getService/{id}', 'ServiceController@getService');
            //Route::get('/getServiceBit/{id}/{val}', 'ServiceController@getServiceBit');
            Route::get('/delete/{id}', 'ServiceController@deleteService');

            Route::group(['prefix' => 'service-information'], function(){
                Route::get('/', 'ServiceController@serviceInfo')->name('service-information');

            });

            Route::group(['prefix' => 'service-documents'], function(){
                Route::get('translation', 'ServiceController@getTranslation');
                Route::get('/', 'ServiceController@getAllServiceDocs');
                Route::get('/list', 'ServiceController@listServiceDocs')->name('service-documents');
                Route::get('/{id}', 'ServiceController@getServiceDocs');
                Route::post('/add', 'ServiceController@addDocuments');
                Route::get('/info/{id}', 'ServiceController@getDocInfo');
                Route::post('/edit', 'ServiceController@editDocuments');
                Route::post('/delete/{id}', 'ServiceController@deleteDocuments');
            });

            Route::group(['prefix' => 'service-profiles'], function(){
                Route::get('translation', 'ServiceController@getServiceProfileTranslation');
                Route::get('/', 'ServiceController@serviceProfiles')->name('service-profiles');
            });

            Route::group(['prefix' => 'client-documents'], function(){
                Route::get('translation', 'ServiceController@getDocumentTypeTranslation');
                Route::post('/{id}/upload-files', 'ServiceController@uploadFiles');
                Route::post('/upload-documents', 'ServiceController@uploadDocuments');
                Route::post('/upload-documents-v2', 'ServiceController@uploadDocumentsV2');
                Route::post('/delete-docs/{id}', 'ServiceController@deleteDocument');
                Route::get('/', 'ServiceController@getAllClientDocType');
                Route::get('/list', 'ServiceController@clientDocuments')->name('client-documents');
                Route::post('add', 'ServiceController@addDocumentType');
                Route::get('info/{id}', 'ServiceController@getDocumentType');
                Route::post('edit', 'ServiceController@editDocumentType');
                Route::get('type/{id}', 'ServiceController@getClientDocuments');
                Route::get('client-id/{id}', 'ServiceController@getClientDocumentsByClientId');
                Route::get('received/{id}', 'ServiceController@getClientDocumentsReceived');
                Route::get('/{id}', 'ServiceController@clientDocumentsById');
            });
    	});

        Route::group(['prefix' => 'service'], function() {
            Route::get('view-all', 'ServiceController@viewAll')->name('services-view-all');
            Route::post('outcome-documents', 'ServiceController@outcomeDocuments');
            Route::get('outcome-documents-v2', 'ServiceController@outcomeDocumentsV2');
            Route::get('outcome-optional-documents-v2', 'ServiceController@outcomeOptionalDocumentsV2');
        });

        Route::group(['prefix' => 'cpanel-accounts'], function() {
            Route::get('/', 'CPanelAccountsController@index')->name('cpanel-accounts');
            Route::get('/create', 'CPanelAccountsController@create')->name('cpanel-accounts-create');
            Route::post('/store', 'CPanelAccountsController@store')->name('cpanel-accounts-store');
            Route::get('/{id}/edit', 'CPanelAccountsController@edit')->name('cpanel-accounts-edit');
            Route::post('/update/{id}', 'CPanelAccountsController@update')->name('cpanel-accounts-update');
            Route::post('/update-avatar/{id}', 'CPanelAccountsController@updateAvatar')->name('cpanel-accounts-update-avatar');
        });

        Route::group(['prefix' => 'dev-logs'], function(){
            Route::get('/', 'DevelopmentLogsController@index')->name('dev-logs');
            Route::get('/new-log', 'DevelopmentLogsController@newLogs')->name('new-log');
            Route::get('/old-logs', 'DevelopmentLogsController@show')->name('old-logs');
            Route::post('/add-update', 'DevelopmentLogsController@create')->name('add-update');
            Route::post('/edit-log/{id}', 'DevelopmentLogsController@edit')->name('edit-log');
            Route::post('/delete-log/{id}', 'DevelopmentLogsController@destroy')->name('delete-log');
            Route::post('/get-log/{id}', 'DevelopmentLogsController@getLog')->name('get-log');


        });

        Route::group(['prefix' => 'access-control'], function() {
            Route::get('/', 'AccessControlController@index')->name('access-control');
            Route::get('/getUser', 'AccessControlController@getUser')->name('get-user');
            Route::get('/getAllUsers', 'AccessControlController@getAllUsers')->name('get-all-user');
            Route::get('/getRoles', 'AccessControlController@getRoles')->name('get-roles');
            Route::get('/getRole/{id}', 'AccessControlController@getRole')->name('get-role');
            Route::get('/getPermissions', 'AccessControlController@getPermissions')->name('get-permissions');
            Route::get('/getPermissionType', 'AccessControlController@getPermissiontype')->name('get-permission-type');
            Route::get('/getSelectedPermission/{id}', 'AccessControlController@getSelectedPermission')->name('get-selected-permission');
            Route::get('/getSelectedPermissions/{id}', 'AccessControlController@getSelectedPermissions')->name('get-selected-permissions');
            Route::get('/getSelectedPermissions2/{id}', 'AccessControlController@getSelectedPermissions2')->name('get-selected-permissions2');

            Route::get('/downloadQR/{client_id}', 'AccessControlController@downloadQR')->name('download-qr');
            // Route::get('downloadQR', function () {
            //     return QrCode::backgroundColor(255, 255, 0)->color(255, 0, 127)
            //                    ->size(500)->generate('http://wyc-group.com');
            // });

            Route::post('/updateAccessControl', 'AccessControlController@updateAccessControl')->name('update-access-control');
            Route::post('/addRole', 'AccessControlController@addRole')->name('add-role');
            Route::post('/saveRole', 'AccessControlController@saveRole')->name('save-role');
            Route::post('/toggle', 'AccessControlController@toggle')->name('toggle');
            Route::get('/getRoleUser/{id}', 'AccessControlController@getRoleUsers')->name('get-role-user');
            Route::get('/permission-tree', 'AccessControlController@permissionTree')->name('permission-tree');
            Route::post('/editPermission', 'AccessControlController@editPermission')->name('edit-permission');
        });

        Route::group(['prefix' => 'financing'], function() {
            Route::get('/', 'FinanceController@index')->name('financing');
        });

        Route::group(['prefix' => 'nfinancing'], function() {
            Route::get('/', 'NewFinancingController@getView')->name('nfinancing');
            Route::get('/delete-row/{id}', 'NewFinancingController@deleteRow');
            Route::get('/get-borrowed', 'NewFinancingController@getBorrowed');
            Route::get('/get-users', 'NewFinancingController@getUsers');
            Route::get('/get-branch', 'NewFinancingController@getBranch');
            Route::post('/update-finance', 'NewFinancingController@updateFinance');
            Route::post('/save-balance', 'NewFinancingController@saveBalance');
            Route::post('/fix-initial', 'NewFinancingController@fixInitial');
            Route::resource('/get-finance', 'NewFinancingController');
            Route::get('/get-daily', 'NewFinancingController@getDaily')->name('get-daily');
            Route::get('/daily-cost', 'NewFinancingController@dataDailyCost')->name('daily-cost');

            // Route::get('/translation', 'NewFinancingController@getTranslation');
            // Route::get('/financing-translation', 'NewFinancingController@getFinancingTranslation');
            // Route::get('/header-translation','NewFinancingController@getFinancingHeadTranslation');
            // Route::get('/modal-translation','NewFinancingController@getFinancingModalTranslation');
        });

        Route::group(['prefix' => 'agents'], function() {
            //translation
            Route::get('/translation', 'AgentsController@getTranslation');

            Route::get('get-agents', 'AgentsController@getAgents');
            Route::get('/', 'AgentsController@index')->name('agents');
            Route::get('get-middlemen', 'AgentsController@getMiddlemen');
            Route::get('get-not-agents', 'AgentsController@getNotAgent');
            Route::get('get-reffered-clients-via-middleman', 'AgentsController@getRefferedClientsViaMiddleman');
            Route::get('/list', 'AgentsController@getAllAgents');
            Route::get('/{id}', 'AgentsController@show');
            Route::post('/', 'AgentsController@store');
            Route::patch('{id}', 'AgentsController@update');

            Route::get('{agentId}/get-agent', 'AgentsController@getAgent');
            Route::get('{agentId}/get-middleman', 'AgentsController@getMiddleman');
            Route::get('{agentId}/get-reffered-clients', 'AgentsController@getRefferedClients');
            //Middleman
            Route::get('/{id}/middleman/{id2}', 'AgentsController@showMidMan');
            Route::get('/getReferrals/{id}', 'AgentsController@getreferrals');
            Route::get('/all-referrals/{id}', 'AgentsController@getAllReferrals');

        });

        Route::group(['prefix' => 'task'], function() {
            Route::get('history/{clientServiceId}', 'TasksController@getHistory');
            Route::patch('{id}/update-who', 'TasksController@updateWho');
        });

        Route::get('actions', 'ActionController@index');
        Route::get('categories-by-action-id', 'CategoryController@getCategoriesByActionId');

        Route::get('users-by-role/{role}', 'UserController@getUsersByRole');

        Route::group(['prefix' => 'branch'], function(){
            Route::get('translation', 'BranchesController@translation');
            Route::get('/', 'BranchesController@index')->name('branch-mngr');
            Route::get('/list', 'BranchesController@getBranches')->name('list-branch');
            Route::post('/save', 'BranchesController@store');
            Route::post('/edit/{id}', 'BranchesController@edit');
            Route::post('/delete/{id}', 'BranchesController@delete');
            Route::get('/show/{id}', 'BranchesController@show');

        });

	});
