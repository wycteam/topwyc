<?php
Route::group(['prefix' => 'documents'], function(){
	Route::get('/', 'DocumentsController@index')->name('documents');
	Route::get('/get-documents', 'DocumentsController@show')->name('get-documents');
	Route::get('/get-pending', 'DocumentsController@showPending')->name('get-pending');
	Route::get('/get-approved', 'DocumentsController@showApproved')->name('get-approved');
	Route::get('/get-declined', 'DocumentsController@showDeclined')->name('get-declined');
	Route::get('/get-count', 'DocumentsController@docCount')->name('get-count');
	Route::get('/get-upload', 'DocumentsController@getUpload')->name('get-upload');
	Route::get('/get-details/{id}', 'DocumentsController@getDetails')->name('get-details');

	Route::group(['prefix' => 'update'], function(){
		Route::get('/{status}/{id}', 'DocumentsController@update');
		Route::post('/verify', 'DocumentsController@verify');
		Route::post('/deny', 'DocumentsController@deny');
	});

});

