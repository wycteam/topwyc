<?php

Route::get('home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('stores', 'StoresController@index')->name('stores');
Route::get('products', 'ProductsController@index')->name('products');

Route::get('feedback', 'FeedbackController@index')->name('feedback');
Route::get('ewallet', 'EwalletController@index')->name('ewallet');
Route::get('ewallet-pool', 'EwalletController@pool')->name('pool');


Route::group(['prefix' => 'faq'], function(){
	Route::get('/', 'FaqController@index')->name('faq');
	Route::post('/create', 'FaqController@create');
	Route::get('/show', 'FaqController@show');
	Route::post('/delete/{id}', 'FaqController@delete');
	Route::post('/getQuestion/{id}', 'FaqController@getQuestion');
	Route::post('/edit/{id}', 'FaqController@edit');
});
Route::get('categories', 'CategoriesController@index')->name('categories');
Route::get('shipments', 'ShipmentsController@index')->name('shipments');

Route::prefix('users')
    ->name('users.')
    ->group(function () {
    Route::post('{user}/roles-permissions', 'UserController@updateRolesAndPermissions')->name('roles-permissions');
});

Route::resource('users', 'UserController');

Route::get('sales', 'SalesController@index')->name('sales');

Route::get('export-daily-orders', 'SalesController@exportDailyOrders');

Route::get('getOrderWithDetails/{id}', 'ShipmentsController@show')->name('getOrderWithDetails');
