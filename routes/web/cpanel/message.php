<?php

Route::name('messages.')
    ->prefix('messages')
    ->group(function () {
    Route::get('/', 'MessagesController@index')->name('index');
});
