<?php

Route::name('emp.')
    ->prefix('emp')
    ->group(function () {

        Route::get('/', 'employee\EmployeeController@getAllEmployees');

        Route::group(['prefix' => 'list'], function(){
            Route::get('/', 'employee\EmployeeController@index')->name('emp-accounts');
        });

        Route::group(['prefix' => 'attendance'], function(){
            Route::get('/', 'employee\EmployeeController@employeeAttendance')->name('emp-attendance');
        });

        Route::get('{id}', 'employee\EmployeeController@show');
        Route::get('{id}/show-detail', 'employee\EmployeeController@showDetail');


	});
