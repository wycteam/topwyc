<?php

Route::name('notifications.')
    ->prefix('notifications')
    ->group(function () {
    Route::get('/', 'NotificationsController@index')->name('index');
});
