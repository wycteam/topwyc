<?php

Route::name('customer-service.')
    ->prefix('customer-service')
    ->group(function () {
    // Route::get('/createconvo/{id}', 'CustomerServiceController@createConvo')->name('createConvo');
});

Route::resource('customer-service', 'CustomerServiceController');
