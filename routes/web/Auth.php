<?php

Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'auth.'], function () {
    Route::get('confirm-email/{token}', 'RegisterController@confirmEmail')->name('confirm-email');
});
