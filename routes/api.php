<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['domain' => config('app.wataf_api_subdomain')], function () {
    foreach (new DirectoryIterator(base_path() . '/routes/api') as $dir) {
        if ($dir->isFile() && ($dir->getFilename() !== '.gitignore')) {
            require_once $dir->getPathname();
        }
    }

    // append_routes('api', 'shopping');
    // append_routes('api', 'cpanel');
});
