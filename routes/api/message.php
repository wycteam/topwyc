<?php

Route::namespace('Api')
    ->name('api.')
    ->group(function () {
    /**
     * Authenticated Routes
     */
    Route::middleware('auth:api')->group(function () {
        /**
         * Version 1 Routes
         */
        Route::name('v1.')
            ->prefix('v1')
            ->group(function () {

            //////////////////
            // Messages API //
            //////////////////

            Route::group(['prefix' => 'messages', 'as' => 'messages.'], function () {
                Route::get('users', 'MessagesController@getUsers')->name('users');
                Route::get('notifications', 'MessagesController@getNotifications')->name('notifications');
                Route::get('notifications2', 'MessagesController@getNotifications2')->name('notifications2');
                Route::post('mark-all-as-seen', 'MessagesController@markAllAsSeen')->name('mark-all-as-seen');
                Route::post('mark-all-as-seen2', 'MessagesController@markAllAsSeen2')->name('mark-all-as-seen2');
                Route::post('{id}/mark-as-read', 'MessagesController@markAsRead')->name('mark-as-read');
                Route::post('mark-all-as-read', 'MessagesController@markAllAsRead')->name('mark-all-as-read');
                Route::get('user/{user}', 'MessagesController@getUserConversation')->name('user-conversation');
                Route::get('chat-room/{chatRoom}', 'MessagesController@getChatRoomMessages')->name('chat-room-messages');
            });

            Route::resource('messages', 'MessagesController');
        });
    });
});
