<?php

Route::namespace('Api')
    ->name('api.')
    ->group(function () {
    /**
     * Authenticated Routes
     */
    Route::middleware('auth:api')->group(function () {
        /**
         * Get the current user
         */
        Route::get('user', function (\Illuminate\Http\Request $request) {
            return $request->user();
        })->name('user');

        Route::post('user/settings', 'UsersController@updateSettings')->name('update-settings');

        /**
         * Version 1 Routes
         */
        Route::name('v1.')
            ->prefix('v1')
            ->group(function () {

            ///////////////
            // Users API //
            ///////////////

            Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
                Route::get('{user}/products', 'UsersController@products')->name('products');
                Route::get('{user}/active-products', 'UsersController@activeProducts')->name('activeProducts');
                Route::get('{user}/stores', 'UsersController@stores')->name('stores');
                Route::get('{user}/active-stores', 'UsersController@activeStores')->name('activeStores');
                Route::get('{user}/documents', 'UsersController@documents')->name('documents');
                Route::get('{user}/doclogs', 'UsersController@document_logs')->name('doclogs');
                Route::get('{user}/orders', 'UsersController@orders')->name('orders');
                Route::get('{user}/complete-orders', 'UsersController@completeOrders')->name('completeOrders');
                Route::get('{user}/roles', 'UsersController@roles')->name('roles');
                Route::get('{user}/permissions', 'UsersController@permissions')->name('permissions');
                Route::get('{user}/sales', 'UsersController@sales')->name('sales');
            });
            Route::get('/getSupports', 'UsersController@getSupports')->name('getSupports');
            Route::get('/getAllAvailableSupports/{chatRoomId}', 'UsersController@getAllAvailableSupports')->name('getAllAvailableSupports');
            Route::get('/getCurrentSupports/{chatRoomId}', 'UsersController@getCurrentSupports')->name('getCurrentSupports');
            Route::resource('users', 'UsersController');
            Route::resource('employees', 'Employee\EmployeeController');

            Route::get('attendance', 'Employee\EmployeeController@attendance');
            Route::get('employee/{id}/attendance', 'Employee\EmployeeController@employeeAttendance');

            Route::get('schedule-types', 'ScheduleTypeController@index');
            Route::patch('schedule/{id}', 'ScheduleController@updateSchedule');

            /////////////////
            // EWallet API //
            /////////////////

            Route::post('ewallet-transaction/refnum', 'EwalletTransactionsController@receiveEwallet');
            Route::get('ewallet-transaction/get-purchased-orders', 'EwalletTransactionsController@getPurchasedOrders');
            Route::get('ewallet-transaction/get-cancelled-orders', 'EwalletTransactionsController@getCancelledOrders');
            Route::get('ewallet-transaction/get-sold-orders', 'EwalletTransactionsController@getSoldOrders');
            Route::get('ewallet-transaction/update-breakdown', 'EwalletTransactionsController@updateBreakdown');
            
            Route::get('ewallet-transaction/get-buyer-returned-orders', 'EwalletTransactionsController@getReturnedOrders');

            Route::post('ewallet-transaction/get-purchased-orders', 'EwalletTransactionsController@getPurchasedOrders');
            Route::post('ewallet-transaction/get-cancelled-orders', 'EwalletTransactionsController@getCancelledOrders');
            Route::post('ewallet-transaction/get-sold-orders', 'EwalletTransactionsController@getSoldOrders');

            Route::post('ewallet-transaction/get-buyer-returned-orders','EwalletTransactionsController@getReturnedOrders');
            
            Route::resource('ewallet-transaction', 'EwalletTransactionsController');
            Route::get('/deposit-transaction', 'EwalletTransactionsController@deposit');
            Route::get('/getUsable', 'EwalletController@getUsable')->name('getUsable');
            Route::resource('user.ewallet', 'EwalletController');

            //////////////
            // Bank API //
            //////////////

            Route::match(array('PUT', 'PATCH'),'/user/{user}/bank-accounts/{bank_account_id}', 'BankAccountsController@update_bank_account')->name('bank-account.update');
            Route::get('/user/{user}/bank-accounts/{bank_account_id}', 'BankAccountsController@show_bank_account')->name('bank-account.show');

            Route::resource('user.bank-accounts', 'BankAccountsController',['except' => ['update','show']]);

            /////////////////
            // Product API //
            /////////////////
            Route::post('saveDrafts', 'ProductController@saveDrafts')->name('saveDrafts');
            Route::post('saveProduct', 'ProductController@saveProduct')->name('saveProduct');
            

            Route::resource('products', 'ProductController');

            /////////////////
            // Order API //
            /////////////////

            // Route::resource('order', 'OrderController');

            /////////////////
            // OrderDetails API //
            /////////////////

            Route::resource('orderdetails', 'OrderDetailsController');
            Route::post('order-details/receive-returned-item', 'OrderDetailsController@receiveReturnedItem');

            /////////////////
            // Store API //
            /////////////////
            //Route::put('/updateStore/{store_id}/{status}', 'StoresController@updateStore')->name('updateStore');
            Route::get('getAllProds', 'StoresController@getAllProds');
            Route::match(array('PUT', 'PATCH'),'updateAllProds/{id}', 'StoresController@updateAllProds');


            Route::get('getUserStores', 'StoresController@getUserStores');
            Route::resource('stores', 'StoresController');


            //////////////////
            // Category API //
            //////////////////

            Route::resource('category', 'CategoryController');

            // //////////////////
            // //// Rate API ////
            // //////////////////

            // Route::resource('rates', 'RatesController');

            ///////////////////
            // Favorites API //
            ///////////////////

            Route::get('favorites', 'FavoritesController@favorites');
            Route::get('addtofavorites/{id}', 'FavoritesController@addtofavorites');
            Route::get('checkfavorites/{id}', 'FavoritesController@checkfavorites');
            Route::get('getfavoritestotal', 'FavoritesController@getfavoritestotal');
            Route::get('deletefavorites/{id}', 'FavoritesController@deletefavorites');

            ///////////////////
            // Compare API //
            ///////////////////

            Route::get('compare', 'CompareController@compare');
            Route::get('addtocompare/{id}', 'CompareController@addtocompare');
            Route::get('getcomparetotal', 'CompareController@getcomparetotal');
            Route::get('deletecompare/{id}', 'CompareController@deletecompare');

        });
    });
});


