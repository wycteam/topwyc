<?php

Route::namespace('Api')
    ->name('api.')
    ->group(function () {
    /**
     * Authenticated Routes
     */
    Route::middleware('auth:api')->group(function () {
        /**
         * Version 1 Routes
         */
        Route::name('v1.')
            ->prefix('v1')
            ->group(function () {

            ///////////////////////
            // Notifications API //
            ///////////////////////

            Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
                Route::get('/', 'NotificationsController@index')->name('index');
                Route::post('/mark-all-as-seen', 'NotificationsController@markAllAsSeen')->name('mark-all-as-seen');
                Route::post('/{id}/mark-as-read', 'NotificationsController@markAsRead')->name('mark-as-read');
            });
        });
    });
});
