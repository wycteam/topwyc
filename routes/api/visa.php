<?php

Route::namespace('Api')
    ->name('api.')
    ->group(function () {
    /**
     * Authenticated Routes
     */
    Route::middleware('auth:api')->group(function () {
        /**
         * Version 1 Routes
         */
        Route::name('v1.')
            ->prefix('v1')
            ->group(function () {

                // VISA
                Route::group(['prefix' => 'visa', 'as' => 'visa.'], function () {

                    // TRACKING
                    Route::group(['prefix' => 'tracking', 'as' => 'tracking.'], function () {
                        Route::get('/service/{id}', 'TrackingController@service')->name('service');
                        Route::get('/group/{id}', 'TrackingController@group')->name('group');
                        Route::get('/report/{id}', 'TrackingController@report')->name('report');
                    });
                    
                    Route::get('/service-profiles', 'GroupController@serviceProfiles');
                    Route::get('/group-commission/{id}', 'GroupController@getCommission');
                    
                    Route::get('/fetch-branch', 'GroupController@fetchBranch');
                    // GROUP
                    Route::group(['prefix' => 'group', 'as' => 'group.'], function () {
                        Route::post('/', 'GroupController@store');

                        Route::get('/{id}', 'GroupController@show');
                        
                        Route::get('/compute/anvo', 'GroupController@anvo');
                        Route::get('/{id}/summary', 'GroupController@summary');
                        Route::get('/{id}/members', 'GroupController@members');
                        Route::get('/{id}/{group_id}/client-commission-list', 'GroupController@clientCommissionList');
                        Route::get('/{id}/{group_id}/agent-commission-list', 'GroupController@agentCommissionList');
                        Route::get('/{client_id}/{agent_id}/client-commission-user', 'GroupController@clientCommissionUser');
                        Route::get('/{id}/switch-cost-level/{level}', 'GroupController@switchCostLevel');
                        Route::get('/{id}/switch-branch/{branch_id}', 'GroupController@switchBranch');
                        Route::get('/{id}/services', 'GroupController@gServices');
                        Route::get('/{id}/batch', 'GroupController@gBatch');
                        Route::get('/all/get-balance', 'GroupController@getAllBalance');
                        Route::post('/{id}/commission', 'GroupController@saveCommission');
                        Route::post('/commission-remove', 'GroupController@removeCommission');
                        Route::post('/commission-set', 'GroupController@setCommission');
                        Route::post('/{id}/service-by-date', 'GroupController@serviceByDate');
                        Route::post('/{id}/service-by-detail', 'GroupController@serviceByDetail');
                        Route::get('/{id}/deposits/{limit}', 'GroupController@deposits');
                        Route::get('/{id}/payments/{limit}', 'GroupController@payments');
                        Route::get('/{id}/refunds/{limit}', 'GroupController@refunds');
                        Route::get('/{id}/discounts/{limit}', 'GroupController@discounts');
                        Route::get('/{id}/discounts-payments/{limit}', 'GroupController@combinedDiscountsPayments');
                        Route::get('/{id}/transactions/{limit}', 'GroupController@getGroupTransactions');

                        Route::get('/{id}/action-logs/{limit}', 'GroupController@actionLogs');
                        Route::get('/{id}/transaction-logs/{limit}', 'GroupController@transactionLogs');
                        Route::get('/{id}/commission-logs/{limit}', 'GroupController@commissionLogs');
                        Route::patch('/{id}/edit-address', 'GroupController@editAddress');
                        Route::patch('/{id}/assign-leader', 'GroupController@assignLeader');
                        Route::post('/{id}/add-members', 'GroupController@addMembers');

                        Route::post('/{id}/add-service', 'GroupController@addService');
                        Route::post('/{id}/add-funds', 'GroupController@addFunds');
                        Route::post('/{id}/delete-member', 'GroupController@deleteMember');
                        Route::patch('/{id}/edit-services', 'GroupController@editServices');

                        Route::get('/{id}/client-service/{clientServiceId}', 'GroupController@getClientService');
                        Route::get('/{id}/client-services', 'GroupController@getClientServices');
                    });

                    // CLIENT
                    Route::group(['prefix' => 'client', 'as' => 'client.'], function () {
                        Route::post('/add-temporary', 'ClientController@addTemporary');
                        Route::post('/add-temporary-validation', 'ClientController@addTemporaryValidation');
                        Route::get('/get-visa-clients', 'ClientController@getVisaClients');
                        Route::get('/get-available-visa-clients', 'ClientController@getAvailableVisaClients');

                        Route::patch('/{id}/transfer', 'ClientController@transfer');
                    });

                    Route::post('quick-report', 'ReportsController@saveQuickReport');

                    Route::post('add-docs', 'ReportsController@addDocs');

                });

        });
    });
});
