<?php

Route::namespace('Api')
    ->name('api.')
    ->group(function () {
    /**
     * Authenticated Routes
     */
    Route::middleware('auth:api')->group(function () {
        /**
         * Version 1 Routes
         */
        Route::name('v1.')
            ->prefix('v1')
            ->group(function () {

            ////////////////
            // Issues API //
            ////////////////

            Route::group(['prefix' => 'issues', 'as' => 'issues.'], function () {
                //
            });

            Route::resource('issues', 'IssuesController');
            Route::get('/getIssueStatus/{cn}', 'IssuesController@getIssueStatus');
            Route::get('/getIssues/{status}', 'IssuesController@getIssues');
            Route::post('/saveIssueFeedback/{cn}', 'IssuesController@saveIssueFeedback');
            Route::resource('product-report', 'ProductReportsController');
        });
    });
});
