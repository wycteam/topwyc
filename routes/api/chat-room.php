<?php

Route::namespace('Api')
    ->name('api.')
    ->group(function () {
    /**
     * Authenticated Routes
     */
    Route::middleware('auth:api')->group(function () {
        /**
         * Version 1 Routes
         */
        Route::name('v1.')
            ->prefix('v1')
            ->group(function () {

            ///////////////////
            // ChatRooms API //
            ///////////////////

            Route::group(['prefix' => 'chat-rooms', 'as' => 'chat-rooms.'], function () {
                //
            });

            Route::resource('chat-rooms', 'ChatRoomsController');
        });
    });
});
