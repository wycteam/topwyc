<?php

Route::namespace('Api')
    ->name('api.')
    ->group(function () {

    //////////////////////
    // Version 1 routes //
    //////////////////////
    Route::name('v1.')
        ->prefix('v1')
        ->group(function () {
        //////////////////////////
        // Authenticated routes //
        //////////////////////////
            Route::middleware('auth:api')->group(function () {
                Route::resource('banks', 'BanksController');
                Route::resource('categories', 'CategoriesController');

                Route::post('feedbacks/{feedback}/approve', 'FeedbacksController@approve')->name('feedbacks.approve');
                Route::post('feedbacks/{feedback}/reject', 'FeedbacksController@reject')->name('feedbacks.reject');
                Route::get('/feedbacks/count', 'FeedbacksController@count');
                Route::resource('feedbacks', 'FeedbacksController');

                Route::resource('feedback-likes', 'FeedbackLikesController', ['only' => [
                    'store', 'destroy'
                ]]);
                Route::resource('product-review-likes', 'ProductReviewLikesController', ['only' => [
                    'store', 'destroy'
                ]]);

                Route::resource('doclog', 'DocumentLogsController', ['only' => [
                    'store', 'index'
                ]]);

                Route::get('friends/notifications', 'FriendsController@getNotifications');
                Route::post('friends/mark-all-as-seen', 'FriendsController@markAllAsSeen');
                Route::post('friends/{id}/confirm', 'FriendsController@confirm');
                Route::post('friends/{id}/delete-request', 'FriendsController@deleteRequest');
                Route::post('friends/block', 'FriendsController@block')->name('block');
                Route::resource('friends', 'FriendsController');
                Route::post('friend/find', 'FriendsController@getFriendId');
            });

        Route::resource('feedback-likes', 'FeedbackLikesController', ['only' => [
            'index'
        ]]);

        Route::post('product/get-related-products', 'ProductController@getRelatedItems')->name('getRelatedItems');
        

        Route::resource('product-review-likes', 'ProductReviewLikesController', ['only' => [
            'index'
        ]]);
        Route::resource('provinces', 'ProvincesController');
        Route::resource('cities', 'CitiesController');
        Route::resource('product-reviews', 'ProductReviewsController');
        Route::post('/search/product', 'ProductsSearchController@search');
        Route::get('/product-review/count', 'ProductReviewsController@count');

        Route::get('featured', 'StoresController@featured')->name('featured');
        Route::get('formen', 'ProductController@formen')->name('formen');
        Route::get('forwomen', 'ProductController@forwomen')->name('forwomen');
        Route::get('getcategories', 'ProductController@getGrandParentCategories')->name('grandparent-categories');
        Route::get('getallcategories', 'ProductController@getAllCategories')->name('allcategories');

        Route::get('forcyber', 'ProductController@forcyber')->name('forcyber');
        Route::get('productcategories/{product_id}', 'CategoryController@productcategories')->name('productcategories');
        Route::get('productstore/{store_id}', 'StoresController@productstore')->name('productstore');
        Route::get('countorder/{store_id}', 'StoresController@countorder')->name('countorder');

        //to get all order details by order_id
        Route::get('getDetails/{order_id}', 'OrderDetailsController@getDetails')->name('orderDetails');
        Route::get('getSummary/{order_id}', 'OrderDetailsController@getSummary')->name('orderSummary');

        Route::get('getOrders', 'OrderDetailsController@orders')->name('order');
        Route::get('getOrderDetails/{id}', 'OrderDetailsController@orderdetails')->name('orderdetails');
        Route::get('intransitorders', 'OrderDetailsController@inTransitOrders')->name('intransitorders');
        Route::get('shipment-filter', 'OrderDetailsController@shipmentFilter');
        Route::resource('order-details', 'OrderDetailsController');

        Route::post('/searchEmail', 'ResetPasswordController@searchEmail')->name('searchEmail');
        Route::post('/searchAltEmail', 'ResetPasswordController@searchAltEmail')->name('searchAltEmail');
        Route::get('/getQuestion/{id}', 'ResetPasswordController@getQuestion')->name('getQuestion');
        Route::get('/answerQuestion/{id}/{userid}', 'ResetPasswordController@answerQuestion')->name('answerQuestion');

        Route::get('featured', 'StoresController@featured')->name('featured');
        Route::get('featuredstores', 'StoresController@featuredstores')->name('featuredstores');

        Route::get('faq', 'FaqController@faq');
        
        // Route::get('translate/{file}', 'GeneralController@show');

        Route::get('/get-details/{caseNumber}', 'OrderDetailsController@getDetailsViaCaseNumber');

        Route::get('/user/sales-report/checkIfSameStore/{prod_id}/{order_id}', 'OrderDetailsController@checkIfSameStore');

        Route::post('awb', 'AwbController@shippingFee');




    });
        //API for mobile APP

        Route::domain(config('app.wataf_domain'))->group(function () {
            Route::name('v1.')
                ->prefix('v1')
                ->group(function () {

                    Route::group(['prefix' => 'app', 'as' => 'app.'], function () {
                        Route::post('/login', 'AppController@mobileLogin');
                        Route::post('/verify-username', 'AppController@verifyUsername');
                        Route::post('/check-client', 'AppController@checkClient');
                        Route::post('/check-passport', 'AppController@checkPassport');
                        Route::post('/check-if-members', 'AppController@checkIfMembers');
                        Route::post('/save-empty-details', 'AppController@saveEmptyDetails');
                        Route::post('/save-password', 'AppController@savePassword');
                        Route::post('/save-new-password', 'AppController@saveNewPassword');
                        Route::get('/get-service-category', 'AppController@getServiceCategory');
                        Route::get('/apk-version', 'AppController@getApkVersion');
                        Route::post('/track-number', 'AppController@trackNumber');
                        Route::get('/get-news', 'AppController@getNews');
                        Route::get('/get-news/{height}/{width}', 'AppController@getNews2');
                        Route::get('/show-news/{id}', 'AppController@showNews');
                        Route::get('/update-balance/{start}/{end}', 'AppController@load_client_list');  
                        Route::get('/update-balance-dec1/{start}/{end}', 'AppController@load_client_list_dec1');  
                        Route::get('/update-group-balance-dec1/{start}/{end}', 'AppController@load_group_list_dec1');  
                        Route::get('/update-group-balance', 'AppController@updateGroupBalance');  
                        Route::get('/get-ads', 'AppController@getAds');                                                                  
                        //test
                        Route::get('/transfer-remarks', 'AppController@transferRemarks');

                        Route::get('/update-tracking-status/{client}', 'AppController@updateTrackingStatus');
                       
                        Route::middleware('auth:api')->group(function () {

                            Route::group(['prefix' => 'batch'], function () {
                                Route::post('write-report', 'AppController@batchWriteReport');
                            });
                              
                            Route::get('/get-personal-info/{client_id}', 'AppController@personalInfo');
                            Route::get('/get-all-packages/{client_id}/{group_id}', 'AppController@allPackages');
                            Route::get('/get-group-info/{client_id}', 'AppController@groupInfo');
                            // Route::get('/v2/get-group-info/{client_id}', 'AppController@groupInfoV2');
                            Route::get('/get-group-members/{group_id}', 'AppController@getGroupMembers');
                            Route::get('/member-package-services/{group_id}/{client_id}', 'AppController@memberPackageServices');
                            Route::get('/transaction-logs/{client_id}/{group_id}', 'AppController@transactionLogs');
                            Route::get('/action-logs/{client_id}/{group_id}', 'AppController@actionLogs');
                            Route::get('/group-transaction-logs/{client_id}', 'AppController@groupTransactionLogs');
                            Route::get('/get-all-user-notif/{client_id}', 'AppController@getAllUserNotif');
                            Route::post('/seen-notif', 'AppController@seenNotif');
                            Route::post('/read-notif', 'AppController@readNotif');
                            Route::post('/read-all-notif', 'AppController@readAllNotif');
                            Route::post('/update-language', 'AppController@updateLanguage');
                            Route::post('/by-service', 'AppController@byService');
                            Route::post('/by-batch', 'AppController@byBatch');
                            Route::get('/group-by-date/{group_id}', 'AppController@groupByDate');
                            Route::get('/group-by-service/{group_id}', 'AppController@groupByService');

                            // Add group members
                            Route::post('group-members', 'AppController@addGroupMembers');

                            // Add service multiple // individual/group
                            Route::post('services-batch', 'AppController@servicesBatchStore');

                            // Temporary Client
                            Route::post('temporary-client/validation', 'AppController@temporaryClientValidation');
                            Route::post('temporary-client', 'AppController@temporaryClientStore');

                            // Add Service To Client
                            Route::get('services', 'AppController@servicesIndex');
                            Route::post('services/get-docs', 'AppController@servicesGetDocs');
                            Route::post('services', 'AppController@servicesStore');
                            Route::post('client-service-edit', 'AppController@clientEditService');

                            // Add Service To Group
                            Route::post('group-services-store', 'AppController@groupServicesStore');

                            // Get all Client Documents by ID 
                            Route::get('client-documents/{client_id}', 'AppController@clientDocuments');
                            // Route::get('v2/client-documents/{client_id}', 'AppController@clientDocumentsV2');
                            Route::get('document-types', 'AppController@documentTypes');
                            Route::post('upload-documents', 'AppController@uploadDocuments');
                            // Route::post('v2/upload-documents', 'AppController@uploadDocumentsV2');

                            // Get all Client Funds by ID 
                            Route::get('client-funds/{client_id}', 'AppController@clientAllFunds');

                            // Add Funds To Client/Group
                            Route::post('add-funds', 'AppController@addFunds');

                            // Add Package
                            Route::post('packages', 'AppController@packagesStore');
                            Route::post('packages-batch', 'AppController@packagesBatchStore');

                            // Delete Package
                            Route::post('delete-package', 'AppController@deletePackage');

                            // Get Latest Service
                            Route::get('latest-service/{client_id}', 'AppController@latestService');

                            // Write Report
                            Route::get('actions', 'AppController@indexActions');
                            Route::get('categories/action/{action_id}', 'AppController@getCategoriesByAction');
                            Route::get('company-couriers', 'AppController@indexCompanyCouriers');
                            Route::get('clients/services', 'AppController@getClientsWithServices');
                            Route::get('clients/packages-services', 'AppController@getClientsWithPackagesServices');
                            Route::post('clients/updated-packages-services', 'AppController@getUpdatedClientsWithPackagesServices');
                            Route::post('client-services', 'AppController@getClientServices');
                            Route::post('services/documents', 'AppController@getServicesDocuments');
                            Route::post('write-report', 'AppController@writeReport');
                            Route::get('service-documents', 'AppController@serviceDocuments');
                            Route::get('action-categories', 'AppController@actionCategories');
                            Route::get('client/{id}/services', 'AppController@getServicesByClient');

                            //admin 
                            Route::get('/get-all-clients', 'AppController@getAllClients');
                            // Route::get('/get-all-clients-full', 'AppController@getAllClientsFull');
                            Route::get('/get-all-groups', 'AppController@getAllGroups');
                            Route::get('/export-group-summary/{group_id}', 'AppController@exportGroupSummary');
                            Route::get('/combine-duplicates', 'AppController@combineDuplicates');
                            Route::get('/filter-batch/{start_group_id}/{end_group_id}', 'AppController@filterBatch');
                            Route::get('/create-packages-services/{group_id}', 'AppController@createPackagesServices');

                            //employee
                            Route::post('/emp-timelog', 'AppController@empTimeLog');
                            Route::post('/get-timelog', 'AppController@getTimeLog');

                            // Group
                                // Add members
                                Route::group(['prefix' => 'group'], function () {
                                    Route::post('/get-available-clients', 'AppController@getAvailableClients');
                                    Route::post('/{id}/add-members', 'AppController@addMembers');
                                });

                            // Synchronization
                            Route::group(['prefix' => 'synchronization'], function () {
                                Route::get('/', 'AppController@synchronizationIndex');

                                // Module::services
                                Route::get('services', 'AppController@synchronizationServices');
                                Route::get('service-profiles', 'AppController@synchronizationServiceProfiles');
                                Route::get('services-with-profiles', 'AppController@synchronizationServicesWithProfiles');

                                // Module::actions
                                Route::get('actions', 'AppController@synchronizationActions');
                                Route::get('categories', 'AppController@synchronizationCategories');
                                Route::get('action-categories', 'AppController@synchronizationActionCategories');

                                // Module::documents
                                Route::get('documents', 'AppController@synchronizationDocuments');
                            });

                        });
                    });

                });


            //Version 2
            Route::name('v2.')
                ->prefix('v2')
                ->group(function () {

                    Route::group(['prefix' => 'app', 'as' => 'app.'], function () {
                                                                
                        // //test
                        // Route::get('/transfer-remarks', 'AppController@transferRemarks');
                       
                        Route::middleware('auth:api')->group(function () {

                            // Route::group(['prefix' => 'batch'], function () {
                            //     Route::post('write-report', 'AppController@batchWriteReport');
                            // });
                            
                            // get Group Info
                            Route::get('get-group-info/{client_id}', 'AppController@groupInfoV2');

                            //get Client Documents
                            Route::get('client-documents/{client_id}', 'AppController@clientDocumentsV2');
                            
                            //upload documents
                            Route::post('upload-documents', 'AppController@uploadDocumentsV2');

                        });
                    });

                });

        });

});
