<?php

Route::namespace('Api')
    ->name('api.')
    ->group(function () {
    /**
     * Authenticated Routes
     */
    Route::middleware('auth:api')->group(function () {
        /**
         * Version 1 Routes
         */
        Route::name('v1.')
            ->prefix('v1')
            ->group(function () {

            ////////////////
            // Customer Service API //
            ////////////////

            Route::resource('customer-service', 'CustomerServiceController');
        });
    });
});