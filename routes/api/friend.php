<?php

/////////////////
// Friends API //
/////////////////

Route::namespace('Api')
    ->name('api.')
    ->group(function () {
    /**
     * Authenticated Routes
     */
    Route::middleware('auth:api')->group(function () {
        /**
         * Version 1 Routes
         */
        Route::name('v1.')
            ->prefix('v1')
            ->group(function () {
            Route::name('friends2.')
                ->prefix('friends2')
                ->group(function () {
                Route::get('/', 'FriendsController@getFriends')->name('index');
            });
        });
    });
});
