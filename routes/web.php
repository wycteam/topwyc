<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

foreach (new DirectoryIterator(base_path() . '/routes/web') as $dir) {
    if ($dir->isFile() && ($dir->getFilename() !== '.gitignore')) {
        require_once $dir->getPathname();
    }
}

Route::group(['domain' => config('app.wataf_domain')], function () {
    append_routes('web', 'shopping');
});

Route::group(['domain' => config('app.wataf_cpanel_subdomain'), 'middleware' => ['auth', 'auth.cpanel-user']], function () {
    append_routes('web', 'cpanel');
});
