const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/cpanel/inspinia.scss', 'css');
mix.sass('resources/assets/sass/cpanel/main.scss', 'css');
mix.sass('resources/assets/sass/cpanel/pages/messages.scss', 'css/pages');
mix.sass('resources/assets/sass/cpanel/pages/profile.scss', 'css/pages');
mix.sass('resources/assets/sass/cpanel/pages/ewallet.scss', 'css');
mix.sass('resources/assets/sass/cpanel/pages/sales.scss', 'css');

mix.sass('resources/assets/sass/cpanel/pages/visa/dashboard/index.scss', 'css/visa/dashboard')
mix.sass('resources/assets/sass/cpanel/pages/visa/tracking/index.scss', 'css/visa/tracking');
mix.sass('resources/assets/sass/cpanel/pages/visa/groups/index.scss', 'css/visa/groups');
mix.sass('resources/assets/sass/cpanel/pages/visa/groups/show.scss', 'css/visa/groups');
mix.sass('resources/assets/sass/cpanel/pages/visa/services/index.scss', 'css/visa/services');
mix.sass('resources/assets/sass/cpanel/pages/visa/clients/index.scss', 'css/visa/clients');
mix.sass('resources/assets/sass/cpanel/pages/visa/clients/profile.scss', 'css/visa/clients');
mix.sass('resources/assets/sass/cpanel/pages/visa/reports/index.scss', 'css/visa/reports');
mix.sass('resources/assets/sass/cpanel/pages/visa/notifications/index.scss', 'css/visa/notifications');
mix.sass('resources/assets/sass/cpanel/pages/visa/reports/write-report.scss', 'css/visa/reports');
mix.sass('resources/assets/sass/cpanel/pages/visa/financing/financing.scss', 'css/visa/financing')
mix.sass('resources/assets/sass/cpanel/pages/visa/agents/index.scss', 'css/visa/agents');

mix
	.js('resources/assets/js/cpanel/inspinia.js', 'js')
	.js('resources/assets/js/cpanel/app.js', 'js')
  .js('resources/assets/js/cpanel/main.js', 'js')
  .js('resources/assets/js/cpanel/notification.js', 'js')
	.js('resources/assets/js/cpanel/inbox.js', 'js')
	// .js('resources/assets/js/cpanel/feedback.js', 'js')
  .js('resources/assets/js/cpanel/pages/user.js', 'js')
  .js('resources/assets/js/cpanel/pages/userprofile.js', 'js')
 //  .js('resources/assets/js/cpanel/pages/stores.js', 'js')
	// .js('resources/assets/js/cpanel/pages/products.js', 'js')
 //  .js('resources/assets/js/cpanel/pages/sales.js', 'js/pages')
  .js('resources/assets/js/cpanel/pages/messages.js', 'js/pages')
  .js('resources/assets/js/cpanel/pages/customer-service/index.js', 'js/pages/customer-service')
  .js('resources/assets/js/cpanel/pages/customer-service/show.js', 'js/pages/customer-service')
	// .js('resources/assets/js/cpanel/pages/ewallet.js', 'js')
  // .js('resources/assets/js/cpanel/pages/documents.js', 'js')
  // .js('resources/assets/js/cpanel/pages/faq.js', 'js')
  // .js('resources/assets/js/cpanel/pages/category.js', 'js')
  // .js('resources/assets/js/cpanel/pages/shipments.js', 'js')

  .js('resources/assets/js/cpanel/visa/dashboard/index.js', 'js/visa/dashboard')
  .js('resources/assets/js/cpanel/visa/clients/index.js', 'js/visa/clients')
  .js('resources/assets/js/cpanel/visa/clients/edit.js', 'js/visa/clients')
  .js('resources/assets/js/cpanel/visa/clients/document.js', 'js/visa/clients')
  .js('resources/assets/js/cpanel/visa/groups/index.js', 'js/visa/groups')
  .js('resources/assets/js/cpanel/visa/groups/show.js', 'js/visa/groups')
  .js('resources/assets/js/cpanel/visa/groups/summary.js', 'js/visa/groups')
  .js('resources/assets/js/cpanel/visa/accounts/index.js', 'js/visa/accounts')
  .js('resources/assets/js/cpanel/visa/accounts/edit.js', 'js/visa/accounts')
  .js('resources/assets/js/cpanel/visa/tracking/index.js', 'js/visa/tracking')
  .js('resources/assets/js/cpanel/visa/services/index.js', 'js/visa/services')
  .js('resources/assets/js/cpanel/visa/services/docs.js', 'js/visa/services')
  .js('resources/assets/js/cpanel/visa/services/profiles.js', 'js/visa/services')
  .js('resources/assets/js/cpanel/visa/reports/index.js', 'js/visa/reports')
  .js('resources/assets/js/cpanel/visa/reports/read.js', 'js/visa/reports')
  .js('resources/assets/js/cpanel/visa/notifications/builder.js', 'js/visa/notifications')
  .js('resources/assets/js/cpanel/visa/notifications/manager.js', 'js/visa/notifications')
  .js('resources/assets/js/cpanel/visa/development-logs/index.js', 'js/visa/development-logs')
  .js('resources/assets/js/cpanel/visa/access-control/index.js', 'js/visa/access-control')
  .js('resources/assets/js/cpanel/visa/financing/index.js', 'js/visa/financing')
  .js('resources/assets/js/cpanel/employee/accounts/index.js', 'js/employee/accounts')
  .js('resources/assets/js/cpanel/employee/accounts/show.js', 'js/employee/accounts')
  .js('resources/assets/js/cpanel/employee/attendance/index.js', 'js/employee/attendance')
  .js('resources/assets/js/cpanel/visa/news/news.js', 'js/visa/news')
  .js('resources/assets/js/cpanel/visa/ads/ads.js', 'js/visa/ads')
	.js('resources/assets/js/cpanel/visa/gallery/gallery.js', 'js/visa/gallery')
	.js('resources/assets/js/cpanel/visa/gallery/album.js', 'js/visa/gallery')
	.js('resources/assets/js/cpanel/visa/nfinancing/financing.js', 'js/visa/nfinancing')
  .js('resources/assets/js/cpanel/visa/tasks/index.js', 'js/visa/tasks')
	.js('resources/assets/js/cpanel/visa/nfinancing/dailycost.js', 'js/visa/nfinancing')
  .js('resources/assets/js/cpanel/visa/branches/index.js', 'js/visa/branches')
  .js('resources/assets/js/cpanel/visa/agents/index.js', 'js/visa/agents')
  .js('resources/assets/js/cpanel/visa/agents/show.js', 'js/visa/agents')
  .js('resources/assets/js/cpanel/visa/payroll/payroll.js', 'js/visa/payroll')
  .js('resources/assets/js/cpanel/visa/payroll/payroll_profile.js', 'js/visa/payroll')
  .js('resources/assets/js/cpanel/visa/payroll/payroll_list.js', 'js/visa/payroll')
	.js('resources/assets/js/cpanel/visa/client-services/index.js', 'js/visa/client-services')
	.js('resources/assets/js/cpanel/visa/client-services/onprocess.js', 'js/visa/client-services')
	.js('resources/assets/js/cpanel/visa/client-services/pending.js', 'js/visa/client-services')
	.js('resources/assets/js/cpanel/visa/event-calendar/index.js', 'js/visa/event-calendar')
  ;

if (mix.config.inProduction) {
    mix.version();
} else {
    mix.sourceMaps();
}

mix.setPublicPath('public/cpanel');

mix.options({
    processCssUrls: false
});
