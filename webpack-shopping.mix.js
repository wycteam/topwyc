const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .sass('resources/assets/sass/shopping/app.scss', 'css', {
        includePaths: ['node_modules/bootstrap-sass/assets/stylesheets']
    })
    .sass('resources/assets/sass/shopping/main.scss', 'css')
    .sass('resources/assets/sass/shopping/modal.scss', 'css')
    .sass('resources/assets/sass/shopping/pages/register.scss', 'css/pages')
    .sass('resources/assets/sass/shopping/pages/reset-password.scss', 'css/pages')
    .sass('resources/assets/sass/shopping/pages/profile.scss', 'css/pages')
    .sass('resources/assets/sass/shopping/pages/home.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/store/index.scss', 'css/pages/store')
    // .sass('resources/assets/sass/shopping/pages/store/form.scss', 'css/pages/store')
    // .sass('resources/assets/sass/shopping/pages/store/show.scss', 'css/pages/store')
    // .sass('resources/assets/sass/shopping/pages/product/details.scss', 'css/pages/product')
    // .sass('resources/assets/sass/shopping/pages/product/product-create.scss', 'css/pages/product')
    // .sass('resources/assets/sass/shopping/pages/ewallet.scss', 'css/pages')
    .sass('resources/assets/sass/shopping/pages/about.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/terms-conditions.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/notifications.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/favorites.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/compare.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/cart.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/faq.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/store.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/feedback.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/sales-report.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/message/index.scss', 'css/pages/message')
    // .sass('resources/assets/sass/shopping/pages/purchase-report.scss', 'css/pages')
    // .sass('resources/assets/sass/shopping/pages/friends.scss', 'css/pages')

    //home temporary file
    .sass('resources/assets/sass/shopping/pages/news.scss', 'css/pages')
    .sass('resources/assets/sass/shopping/pages/comment.scss', 'css/pages')
    .sass('resources/assets/sass/shopping/pages/gallery.scss', 'css/pages')
    // visa

    .sass('resources/assets/sass/shopping/pages/visa/services/index.scss', 'css/pages/visa/services')
    .sass('resources/assets/sass/shopping/pages/visa/contactUs/index.scss', 'css/pages/visa/contactUs')
    .sass('resources/assets/sass/shopping/pages/visa/tracking/index.scss', 'css/pages/visa/tracking')
    ;

mix
    .js('resources/assets/js/shopping/app.js', 'js')
    .js('resources/assets/js/shopping/main.js', 'js')
    .js('resources/assets/js/shopping/login.js', 'js')
    //.js('resources/assets/js/shopping/notification.js', 'js')
    .js('resources/assets/js/shopping/pages/register.js', 'js/pages')
    .js('resources/assets/js/shopping/pages/reset-password.js', 'js/pages')
    .js('resources/assets/js/shopping/pages/home.js', 'js/pages')
    .js('resources/assets/js/shopping/visa/news.js', 'js/visa')
    .js('resources/assets/js/shopping/visa/news2.js', 'js/visa')
    .js('resources/assets/js/shopping/comment.js', 'js/visa')
    .js('resources/assets/js/shopping/visa/gallery.js', 'js/visa')
    .js('resources/assets/js/shopping/visa/open.js', 'js/visa')
    // .js('resources/assets/js/shopping/pages/categories.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/product.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/store/index.js', 'js/pages/store')
    // .js('resources/assets/js/shopping/pages/store/create.js', 'js/pages/store')
    // .js('resources/assets/js/shopping/pages/store/edit.js', 'js/pages/store')
    // .js('resources/assets/js/shopping/pages/store/show.js', 'js/pages/store')
    // .js('resources/assets/js/shopping/pages/product-details.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/product-report.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/product-create.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/product-edit.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/product-list.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/related-products.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/profile.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/profileSettings.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/ewallet.js', 'js/pages')
    //.js('resources/assets/js/shopping/pages/notifications.js', 'js/pages')
    //.js('resources/assets/js/shopping/pages/favorites.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/compare.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/cart.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/sales-report.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/purchase-report.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/store.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/store-page.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/feedback.js', 'js/pages')
    // .js('resources/assets/js/shopping/pages/faq.js', 'js/pages')
    //.js('resources/assets/js/shopping/pages/message/index.js', 'js/pages/message')
    //.js('resources/assets/js/shopping/pages/messenger.js', 'js/pages')
    //profile contents
    // .js('resources/assets/js/shopping/pages/profile/account-info.js', 'js/pages/profile')
    // .js('resources/assets/js/shopping/pages/profile/contact-details.js', 'js/pages/profile')
    // .js('resources/assets/js/shopping/pages/profile/security-settings.js', 'js/pages/profile')
    // .js('resources/assets/js/shopping/pages/friends.js', 'js/pages')
    //.js('resources/assets/js/shopping/customer-support.js', 'js')
    //.js('resources/assets/js/shopping/offcanvas.js', 'js')
    //.js('resources/assets/js/shopping/chat-bar.js', 'js')

    // VISA
    .js('resources/assets/js/shopping/visa/services/index.js', 'js/visa/services')
    .js('resources/assets/js/shopping/visa/tracking/index.js', 'js/visa/tracking')

    //NEWS file
    ;

if (mix.config.inProduction) {
    mix.version();
} else {
    mix.sourceMaps();
}

mix.setPublicPath('public/shopping');
