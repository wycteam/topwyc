<?php

if (! function_exists('append_class_if')) {
    function appendClassIf($class = null, $routeNames)
    {
        $routeName = request()->route()->getName();
        $routeNames = explode('|', $routeNames);

        if (in_array($routeName, $routeNames)) {
            return $class;
        }

        return '';
    }
}

if (! function_exists('append_routes')) {
    function append_routes($type, $site)
    {
        foreach (new DirectoryIterator(base_path() . '/routes/' . $type) as $dir) {
            if ($dir->isDir() && ! $dir->isDot() && ($dir->getBasename() === $site)) {
                Route::group(['namespace' => ucfirst($site), 'as' => $site . '.'], function () use ($dir) {
                    foreach (File::allFiles($dir->getPathname()) as $file) {
                        require_once $file->getRealPath();
                    }
                });
            }
        }
    }
}

if (! function_exists('access_token')) {
    function access_token()
    {
        $token = null;

        if (Auth::check() && ! empty($user = Auth::user())) {
            $token = $user->access_token;
        }

        return $token;
    }
}

if (! function_exists('authorize')) {
    function authorize($action, ...$args)
    {
        if (\Gate::denies($action, ...$args)) {
            abort(403, 'Unauthorized');
        }
    }
}

if (! function_exists('get_policy')) {
    function get_policy($permission)
    {
        $policy = 'App\Policies\\' . $permission->type . 'Policy';
        $nameArray = explode('.', $permission->name);
        $method = end($nameArray);

        return [$policy, $method];
    }
}

if (! function_exists('define_policy')) {
    function define_policy($permission)
    {
        $policy = get_policy($permission);
        $policyMethod = current($policy) . '@' . end($policy);

        Gate::define($permission->name, $policyMethod);
    }
}

if (! function_exists('isActiveRoute')) {
    function isActiveRoute($route, $output = 'active')
    {
        if(is_array($route)) {
            foreach($route as $r) {
                if (Route::currentRouteName() == $r) {
                    return $output;
                } 
            }
        } else {
            if (Route::currentRouteName() == $route) {
                return $output;
            }
        }
    }
}

if (! function_exists('isInRoute')) {
    function isInRoute($route, $output = 'in')
    {
        if(is_array($route)) {
            foreach($route as $r) {
                if (Route::currentRouteName() == $r) {
                    return $output;
                } 
            }
        } else {
            if (Route::currentRouteName() == $route) {
                return $output;
            }
        }
    }
}