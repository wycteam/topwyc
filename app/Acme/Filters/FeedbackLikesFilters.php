<?php

namespace App\Acme\Filters;

class FeedbackLikesFilters extends QueryFilter
{
   
    public function user($user_id)
    {
        return $this->builder->where('user_id','=',$user_id);
    }

    public function feedback($feedback_id)
    {
		return $this->builder->where('feedback_id','=',$feedback_id)->take(1);
    }

    public function product($review_id)
    {
		return $this->builder->where('review_id','=',$review_id)->take(1);
    }

}