<?php

namespace App\Acme\Filters;

class CityFilters extends QueryFilter
{
    public function default()
    {
        return $this->builder->orderBy('name');
    }
}
