<?php

namespace App\Acme\Filters;

class ProductFilters extends QueryFilter
{
    
    public function name($name)
    {
        return $this->builder->where('name', 'like', '%'. $name . '%');
    }


}