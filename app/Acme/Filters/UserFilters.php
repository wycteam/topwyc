<?php

namespace App\Acme\Filters;
use Auth,DB;
use App\Models\Shopping\Friends;
class UserFilters extends QueryFilter
{
    // // Example default filter
    // public function default()
    // {
    //     return $this->builder->where('email', 'like', '%example.net');
    // }

    // Example filter
    public function name($name)
    {
        return $this->builder->where('first_name', 'like', '%'.$name.'%')
            ->orWhere('last_name', 'like', '%'.$name.'%');
    }

    public function searchByEmail($email)
    {
        $user = Auth::user();
        return $this->builder
                ->where('id','<>',$user->id)
                ->where('email',$email);
    }

    public function withEwallet(){
        //IMPORTANT!!!!! accept only wataf personel 
        if(Auth::user()){
            return $this->builder->with('ewallet');
        }
        return false;
    }

    public function search($search)
    
    {
        return $this->builder->where(function ($query) use ($search) {
            $query->where('name', 'like', '%'.$search.'%')
                ->orWhere('first_name', 'like', '%'.$search.'%')
                ->orWhere('last_name', 'like', '%'.$search.'%')
                ->orWhere('email','like', '%'.$search.'%')
                ->orWhere(\DB::raw('concat(first_name," ",last_name)') , 'LIKE' , '%'.$search.'%');
        });

        // return $this->builder->where('name', 'like', '%'.$query.'%')
        //     ->orWhere('first_name', 'like', '%'.$query.'%')
        //     ->orWhere('last_name', 'like', '%'.$query.'%')
        //     ->orWhere('email','like', '%'.$query.'%');
    }

    public function takeNotFriend($take)
    {
        $user = Auth::user();

        $friends = Friends::where('user_id',$user->id)
            ->orWhere('user_id2',$user->id)
            ->select(DB::raw('IF(user_id=\''.$user->id.'\',user_id2,user_id) as id'))
            ->get();

        return $this->builder
            ->whereNotIn('id',$friends->pluck('id')->toArray())
            ->where('id','<>',$user->id)
            ->orderBy('created_at','desc')
            ->take($take);
    }

    public function doctype($type)
    {
        return $this->builder->with(['documents' => function ($query) use ($type) {
            $query->where('type',$type);
            $query->orderBy('created_at','desc');
        }]);
    }
}