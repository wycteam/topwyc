<?php

namespace App\Acme\Filters;

class IssueFilters extends QueryFilter
{
    public function default()
    {
        return $this->builder->with(['user', 'chat_room.users' => function ($query) {
            $query->select('first_name', 'last_name', 'nick_name', 'display_name', 'is_display_name', 'gender');
        }])->latest();
    }

    public function search($search = null)
    {
        if ($search) {
            return $this->builder->where('issue_number', 'like', '%'.$search.'%')
                ->orWhere('subject', 'like', '%'.$search.'%')
                ->orWhere('body', 'like', '%'.$search.'%');
        }
    }
}