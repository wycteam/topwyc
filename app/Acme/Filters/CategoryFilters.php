<?php

namespace App\Acme\Filters;

class CategoryFilters extends QueryFilter
{
    public function default()
    {
        return $this->builder->orderBy('name');
    }
}
