<?php

namespace App\Acme\Filters;
use Auth;
class EwalletTransactionFilters extends QueryFilter
{
    public function user($user_id)
    {
        return $this->builder->whereUserId($user_id);
    }
}