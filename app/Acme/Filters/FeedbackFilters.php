<?php

namespace App\Acme\Filters;
use Auth;
class FeedbackFilters extends QueryFilter
{
    //how many comments/feedback
    public function count($size)
    {
        return $this->builder->take($size);
    }

    public function skip($skip = null)
    {
    	if (! is_null($skip)) {
    		return $this->builder->skip($skip);
    	}
    }

    public function approved($approved=0){
        if($user = Auth::user())
        {
            if($user->hasRole('cpanel-admin') || $user->hasRole('master'))
            {
                return $this->builder->whereApproved($approved);
            }
        }
    }

    public function product($product_id)
    {
        return $this->builder->whereProductId($product_id);
    }
}