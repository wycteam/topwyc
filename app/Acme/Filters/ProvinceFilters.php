<?php

namespace App\Acme\Filters;

class ProvinceFilters extends QueryFilter
{
    public function default()
    {
        return $this->builder->orderBy('name');
    }
}