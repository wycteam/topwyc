<?php

namespace App\Acme\Traits;

trait Documentable
{
    public function documents()
    {
        return $this->morphMany(\App\Models\Document::class, 'documentable');
    }
}