<?php

namespace App\Acme\Traits;

use App\Fragment;

trait TranslatableTrait
{
	public static function bootTranslatableTrait()
	{
		static::created(function($model) {
			$fragment = new Fragment;
			$fragment->key = $model->translateNamespace . '.' . $model->slug;
			$fragment->setTranslation('text', 'en', $model->name);
			$fragment->save();
		});
	}
}