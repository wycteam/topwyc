<?php

namespace App\Acme\Traits;

trait ClientDocumentable
{
    public function clientdocuments()
    {
        return $this->morphMany(\App\Models\Visa\ClientDocuments::class, 'clientdocumentable');
    }
}