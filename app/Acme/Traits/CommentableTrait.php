<?php

namespace App\Acme\Traits;

trait CommentableTrait
{
	public function comments()
	{
	    return $this->morphMany('App\Models\Comment', 'commentable');
	}
}