<?php

namespace App\Acme\Traits;

use App\Acme\Filters\QueryFilter;

trait QueryFilterTrait
{
    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}