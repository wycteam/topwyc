<?php

namespace App\Acme\Traits;

trait Addressable
{
    public function addresses()
    {
        return $this->morphMany('App\Models\Address', 'addressable');
    }
}