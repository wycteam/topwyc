<?php

namespace App\Acme\Traits;

trait Fileable
{
    public function files()
    {
        return $this->morphMany(\App\Models\File::class, 'fileable');
    }

    public function fileImage(){
    	return $this->files()
    			->where('type', 'Image');
    }

    public function fileVideo(){
    	return $this->files()
    			->where('type', 'Video');
    }
}