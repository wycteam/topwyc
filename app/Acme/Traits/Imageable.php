<?php

namespace App\Acme\Traits;

trait Imageable
{
    public function images()
    {
        return $this->morphMany(\App\Models\Image::class, 'imageable');
    }
}