<?php

namespace App\Acme\Traits;

trait BankableTrait
{
    public function banks()
    {
        return $this->morphMany('App\Models\Bank', 'bankable');
    }
}