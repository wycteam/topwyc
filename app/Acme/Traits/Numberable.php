<?php

namespace App\Acme\Traits;

trait Numberable
{
    public function numbers()
    {
        return $this->morphMany('App\Models\Number', 'numberable');
    }
}