<?php

namespace App\Channels;

use RuntimeException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Channels\DatabaseChannel as LaravelDatabaseChannel;

class DatabaseChannel extends LaravelDatabaseChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function send($notifiable, Notification $notification)
    {
        return $notifiable->routeNotificationFor('database')->create([
            'type' => get_class($notification),
            'from_id' => Auth::check() ? Auth::user()->id : null,
            'data' => $this->getData($notifiable, $notification),
            'seen_at' => null,
            'read_at' => null,
        ]);
    }
}
