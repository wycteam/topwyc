<?php

namespace App\Providers;

use Auth;
use Hash;
use Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('match_old_password', function ($attribute, $value, $parameters, $validator) {
            $user = Auth::user();

            return Hash::check($value, $user->password);
        });

        Validator::extend('member', function ($attribute, $value, $parameters, $validator) {
            $validator->setCustomMessages([
                'chat_room_id.member' => 'You are not a member of the chat room.'
            ]);

            $user = Auth::user();

            return $user->isMemberOf($value);
        });

        Validator::extend('facebook_url', function ($attribute, $value, $parameters, $validator) {
            $validator->setCustomMessages([
                $attribute.'.facebook_url' => 'Invalid facebook url.'
            ]);

            return !! preg_match('/(?:https|http)\:\/\/(?:www\.)?facebook\.com/', $value);
        });

        Validator::extend('twitter_url', function ($attribute, $value, $parameters, $validator) {
            $validator->setCustomMessages([
                $attribute.'.twitter_url' => 'Invalid twitter url.'
            ]);

            return !! preg_match('/(?:https|http)\:\/\/(?:www\.)?twitter\.com/', $value);
        });

        Validator::extend('instagram_url', function ($attribute, $value, $parameters, $validator) {
            $validator->setCustomMessages([
                $attribute.'.instagram_url' => 'Invalid instagram url.'
            ]);

            return !! preg_match('/(?:https|http)\:\/\/(?:www\.)?instagram\.com/', $value);
        });

        Schema::defaultStringLength(191);
        Blade::directive('money_format', function ($money) {
            return "<?php echo number_format($money, 2); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/../Classes/Navigation.php';
    }
}
