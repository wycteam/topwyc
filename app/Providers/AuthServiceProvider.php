<?php

namespace App\Providers;

use App\Models\Permission;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\YourModel' => 'App\Policies\YourPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(null, ['middleware' => [ \Barryvdh\Cors\HandleCors::class ]]);

        if (! App::runningInConsole()) {
            $this->registerUserPermissions();
        }
    }

    private function registerUserPermissions()
    {
        Gate::before(function ($user, $ability) {
            if ($user->hasRole('master') || $user->hasRole('cpanel-admin')) {
                return true;
            }
        });

        foreach ($this->getPermissions() as $permission) {
            if (method_exists(...get_policy($permission))) {
                define_policy($permission);
            } else {
                Gate::define($permission->name, function ($user) use ($permission) {
                    return $user->hasRole('master') || $user->hasRole('admin') || $user->hasPermission($permission->name);
                });
            }
        }
    }

    private function getPermissions()
    {
        return Permission::all();
    }
}
