<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewUser extends Notification
{
    use Queueable;

    public $toUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($toUser)
    {
        $this->toUser = $toUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return ['broadcast', DatabaseChannel::class];
        return ['broadcast', DatabaseChannel::class, 'mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Welcome to WATAF!')
            ->markdown('emails.user.welcome', ['toUser' => $this->toUser]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'New Account',
            'body' => 'Welcome to WATAF Shopping! Start shopping and get big discounts! Sell and double your sales. For assistance please send us a message.',
            'image' => '/images/favicon/apple-touch-icon.png'
        ];
    }
}
