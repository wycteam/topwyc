<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\DatabaseChannel;

class NewFriendRequest extends Notification
{
    use Queueable;

    public $friend;
    public $fromUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($friend, $fromUser)
    {
        $this->friend = $friend;
        $this->fromUser = $fromUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', DatabaseChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'New Friend Request',
            'body'  => $this->fromUser->full_name .' sent you a friend request.',
            'image' => $this->fromUser->avatar,
            'user' => [
                'id' => $this->fromUser->id,
                'full_name' => $this->fromUser->full_name,
                'email' => $this->fromUser->email,
            ],
            'friend' => [
                'id' => $this->friend->id,
                'user_id' => $this->friend->user_id,
                'user_id2' => $this->friend->user_id2,
                'status' => $this->friend->status,
            ]
        ];
    }
}
