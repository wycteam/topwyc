<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderStatusUpdate extends Notification
{
    use Queueable;

    public $orderDetail;
    public $fromUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orderDetail, $fromUser, $sameStore = null, $fee = null)
    {
        $this->orderDetail = $orderDetail;
        $this->fromUser = $fromUser;
        $this->sameStore = $sameStore;
        $this->fee = $fee;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // if ($this->orderDetail->status === 'Received') {
        //     return [];
        // }

        return ['broadcast', DatabaseChannel::class, 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $seller = $this->orderDetail->product()->first()->user()->first();

        if ($this->orderDetail->status === 'In Transit') {
            return (new MailMessage)
                ->subject('Your order is being shipped')
                ->markdown('emails.order.shipped', ['orderDetail' => $this->orderDetail , 'sameStore' => $this->sameStore , 'fee' => $this->fee]);
        } elseif ($this->orderDetail->status === 'Delivered') {
            return (new MailMessage)
                ->subject('Your order has been delivered')
                ->markdown('emails.order.autodelivered', ['orderDetail' => $this->orderDetail , 'sameStore' => $this->sameStore , 'fee' => $this->fee]);
        } elseif ($this->orderDetail->status === 'Cancelled') {
            return (new MailMessage)
                ->subject('Your order has been cancelled')
                ->markdown('emails.order.cancelled', ['orderDetail' => $this->orderDetail, 'sameStore' => $this->sameStore , 'fee' => $this->fee]);
        } elseif ($this->orderDetail->status === 'Pending') {
            return (new MailMessage)
                ->subject('Your order has been marked as pending')
                ->markdown('emails.order.pending', ['orderDetail' => $this->orderDetail]);
        } elseif ($this->orderDetail->status === 'Received' && $this->orderDetail->received_date) {
            return (new MailMessage)
                ->subject('The item was successfully received by the buyer.')
                ->markdown('emails.order.received', ['orderDetail' => $this->orderDetail,'fromUser' => $this->fromUser]);
        } elseif ($this->orderDetail->status === 'Returned' && $this->orderDetail->returned_received_date) {
            return (new MailMessage)
                ->subject('The item you returned was received by the seller.')
                ->markdown('emails.order.returned', ['orderDetail' => $this->orderDetail]);
        } elseif ($this->orderDetail->status === 'Returned' && $this->orderDetail->returned_received_date == null) {
            return (new MailMessage)
                ->subject($this->fromUser->full_name.' requested to return your item '.$this->orderDetail->product->name.'. Reason: '.$this->orderDetail->reason)
                ->markdown('emails.order.requestReturned', ['orderDetail' => $this->orderDetail,'fromUser' => $this->fromUser]);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $body = '';

        if (empty($this->orderDetail->tracking_number)) {
            $body = 'Part of your order ';
        } else {
            $body = 'Your order ';
        }

        if ($this->orderDetail->status === 'In Transit') {
            $body = $body.' is being shipped through '.$this->orderDetail->courier.'.';
        } elseif ($this->orderDetail->status === 'Delivered') {
            $body = $body.' has been delivered.';
        } elseif ($this->orderDetail->status === 'Cancelled') {
            $body = $body.' has been cancelled (Reason: '.$this->orderDetail->reason.').';
        } elseif ($this->orderDetail->status === 'Pending') {
            $body = $body.' has been marked pending.';
        } elseif ($this->orderDetail->status === 'Received' && $this->orderDetail->received_date) {
            $body = $this->fromUser->full_name.' successfully received the item '.$this->orderDetail->product->name;
        } elseif ($this->orderDetail->status === 'Returned' && $this->orderDetail->returned_received_date) {
            $body = $body.' has been successfully returned. Your money will go back to your e-wallet.';
        } elseif ($this->orderDetail->status === 'Returned' && $this->orderDetail->returned_received_date == null) {
            $body = $this->fromUser->full_name.' requested to return your item '.$this->orderDetail->product->name.'. Reason: '.$this->orderDetail->reason;
        }

        return [
            'title' => 'Order #'.$this->orderDetail->combined_tracking_number,
            'body' => $body,
            'image' => $this->orderDetail->image,
            'order_detail' => [
                'id' => $this->orderDetail->id,
                'order_id' => $this->orderDetail->order_id,
                'product_id' => $this->orderDetail->product_id,
                'price_per_item' => $this->orderDetail->price_per_item,
                'tracking_number' => $this->orderDetail->combined_tracking_number,
                'discount' => $this->orderDetail->discount,
                'quantity' => $this->orderDetail->quantity,
                'subtotal' => $this->orderDetail->subtotal,
            ],
        ];
    }
}

