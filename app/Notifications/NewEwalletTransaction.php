<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\DatabaseChannel;

class NewEwalletTransaction extends Notification
{
    use Queueable;

    public $transaction;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', DatabaseChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $transaction_type = $this->transaction->type;
        $user = $this->transaction->user;
        $title = 'Ewallet Request';

        switch ($transaction_type) {
            case 'Withdraw':
                $body = $user->full_name .' requested for a '.$transaction_type.' with Transaction No. '. $this->transaction->ref_number;
                break;
            case 'Deposit':
                if(!$this->transaction->deposit_slip){
                    $body = $user->full_name .' deposited money for approval. Please wait for deposit slip.';
                    break;
                }
                else{
                    $body = $user->full_name .' uploaded a deposit slip for Transaction No. '. $this->transaction->ref_number;
                    break;
                }
            case 'Transfer':
                if($this->transaction->status == 'Completed')
                {
                    $title = 'Transfer Complete';
                    $body = $this->transaction->receiver->full_name .' received your '. number_format($this->transaction->amount,2);
                }else if($this->transaction->status == 'Pending')
                {
                    $title = 'Transfer to your Account';
                    $body = $this->transaction->user->full_name .' sent you an Ewallet Amount. Use this code to receive: '.$this->transaction->ref_number;
                }
                break;
        }
        return [
            'title' => $title,
            'body' => $body,
            'image' => $transaction_type=='Transfer'?$this->transaction->receiver->avatar:$user->avatar,
            'transaction' => $this->transaction,
        ];
    }
}
