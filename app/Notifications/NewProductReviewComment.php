<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewProductReviewComment extends Notification
{
    use Queueable;

    public $productReviewComment;
    public $fromUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($productReviewComment, $fromUser)
    {
        $this->productReviewComment = $productReviewComment;
        $this->fromUser = $fromUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', DatabaseChannel::class, 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('New comment on your product review')
            ->line($this->fromUser->full_name.' commented on your review for product '.strtoupper($this->productReviewComment->products->name))
            ->line($this->productReviewComment->content)
            ->action('View Product', route('shopping.product.show', ['product' => $this->productReviewComment->products->slug]));
            // ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'New comment on your product review',
            'body' => $this->fromUser->full_name.' commented on your review for product '.strtoupper($this->productReviewComment->products->name),
            'image' => $this->fromUser->avatar,
            'product' => [
                'id' => $this->productReviewComment->products->id,
                'slug' => $this->productReviewComment->products->slug,
            ]
        ];
    }
}
