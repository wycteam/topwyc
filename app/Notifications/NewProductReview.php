<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewProductReview extends Notification
{
    use Queueable;

    public $productReview;
    public $fromUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($productReview, $fromUser)
    {
        $this->productReview = $productReview;
        $this->fromUser = $fromUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', DatabaseChannel::class, 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('New Product Review')
            ->markdown('emails.product.newProductReview', ['fromUser' => $this->fromUser, 'review' => $this->productReview]);
            // ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'New Product Review',
            'body' => $this->fromUser->full_name.' has reviewed your product '.strtoupper($this->productReview->products->name),
            'image' => $this->fromUser->avatar,
            'product' => [
                'id' => $this->productReview->products->id,
                'slug' => $this->productReview->products->slug,
            ]
        ];
    }
}
