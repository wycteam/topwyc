<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DocumentDenied extends Notification
{
    use Queueable;

    public $document;
    public $fromUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($document, $fromUser)
    {
        $this->document = $document;
        $this->fromUser = $fromUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', DatabaseChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'Document Denied',
            'body' => strtoupper($this->document->type).': '.$this->document->reason,
            'image' => $this->fromUser->avatar,
            'document' => $this->document,
        ];
    }
}
