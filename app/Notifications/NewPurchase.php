<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewPurchase extends Notification
{
    use Queueable;

    public $products;
    public $fromUser;
    public $orderDetails;
    public $total;
    public $discount;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($products, $fromUser, $orderDetails, $total, $discount, $sameStore = null,$fee = null)
    {
        $this->products = $products;
        $this->fromUser = $fromUser;
        $this->orderDetails = $orderDetails;
        $this->total = $total;
        $this->discount = $discount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return ['broadcast', DatabaseChannel::class];
        return ['broadcast', DatabaseChannel::class, 'mail'];
    }

    public function toMail($notifiable)
    {
        $seller = $this->products[0]->user;
        return (new MailMessage)
        ->subject('New Purchase from '.$this->fromUser->full_name)
        ->markdown('emails.order.newPurchase', ['products' => $this->products,'fromUser' => $this->fromUser, 'seller' => $seller, 'details' => $this->orderDetails, 'total' => $this->total, 'discount' => $this->discount]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'New Purchase',
            'body' => $this->fromUser->full_name.' purchased '. count($this->products) . ' product&#8725;s from your store',
            'image' => $this->products[0]->main_image,
            'product' => [
                'id' => $this->products[0]->id,
                'name' => $this->products[0]->name,
                'store_id' => $this->products[0]->store_id
            ]
        ];
    }
}
