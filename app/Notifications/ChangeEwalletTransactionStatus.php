<?php

namespace App\Notifications;

use App\Channels\DatabaseChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ChangeEwalletTransactionStatus extends Notification
{
    use Queueable;

    public $transaction;
    public $fromUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($transaction,$fromUser)
    {
        $this->transaction = $transaction;
        $this->fromUser = $fromUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($this->transaction->status === 'Completed' && $this->transaction->type === 'Withdraw') {
            return ['broadcast', DatabaseChannel::class, 'mail'];
        }
        else if($this->transaction->status === 'Pending'){
            return '';
        }
        return ['broadcast', DatabaseChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        $user = $this->transaction->user()->first();

        if ($this->transaction->status === 'Completed' && $this->transaction->type === 'Withdraw') {
            return (new MailMessage)
                ->subject('Transaction #'.strtoupper($this->transaction->ref_number).' Your withdraw request was approved.')
                ->markdown('emails.ewallet.withdrawApproved', ['transaction' => $this->transaction]);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $title = 'Ewallet Transaction Updated!';
        $body = 'Transaction No. '. strtoupper($this->transaction->ref_number) .'\'s status is now '. $this->transaction->status;

        if ($this->transaction->status === 'Cancelled') {
            $title = 'Ewallet Load Request Denied!';
            $body = 'Transaction No: '.$this->transaction->ref_number.' | Reason: '.$this->transaction->reason;
        }

        if ($this->transaction->status === 'Completed' && $this->transaction->type === 'Withdraw') {
             $body .= ". Expect the transfer within 2-3 banking days excluding holidays.";
        }

        return [
            'title' => $title,
            'body' => $body,
            'image' => $this->fromUser->avatar,
            'transaction' => $this->transaction,
        ];
    }
}
