<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class IssueAssignment extends Notification
{
    use Queueable;

    public $issue;
    public $fromUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($issue, $fromUser)
    {
        $this->issue = $issue;
        $this->fromUser = $fromUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', DatabaseChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'Customer Service',
            'body' => $this->fromUser->full_name.' assigned you the issue: '.$this->issue->subject,
            'image' => $this->fromUser->avatar,
            'issue' => [
                'id' => $this->issue->id,
                'user_id' => $this->issue->user_id,
                'issue_number' => $this->issue->issue_number,
                'subject' => $this->issue->subject,
                'body' => $this->issue->body,
                'status' => $this->issue->status,
            ],
        ];
    }
}
