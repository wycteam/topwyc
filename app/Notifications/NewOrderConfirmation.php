<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewOrderConfirmation extends Notification
{
    use Queueable;

    public $toUser;
    public $products;
    public $order;
    public $orderDetails;
    public $total;
    public $discount;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($toUser, $products, $order, $orderDetails, $total, $discount)
    {
        $this->toUser = $toUser;
        $this->products = $products;
        $this->order = $order;
        $this->orderDetails = $orderDetails;
        $this->total = $total;
        $this->discount = $discount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return ['broadcast', DatabaseChannel::class];
        return ['broadcast', DatabaseChannel::class, 'mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Your order has been confirmed.')
            ->markdown('emails.order.newOrderConfirmation', ['toUser' => $this->toUser, 'products' => $this->products, 'order' => $this->order, 'orderDetails' => $this->orderDetails, 'total' => $this->total, 'discount' => $this->discount]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'New Order',
            'body' => 'Your order ' . $this->order->id . ' has been confirmed.',
            'image' => '/images/favicon/apple-touch-icon.png'
        ];
    }
}
