<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Shopping\Store;
use Illuminate\Auth\Access\HandlesAuthorization;

class StorePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(User $user, Store $store)
    {
        return $store->user_id == $user->id;
    }

    public function delete(User $user, Store $store)
    {
        return $store->user_id == $user->id;
    }
}
