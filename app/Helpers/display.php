<?php
//for views :just aesthetics
function createSN($prefix,$id,$padding=6)
{
	$sn =  str_pad($id, $padding, "0", STR_PAD_LEFT);
	return $prefix.'-'.$sn;
}