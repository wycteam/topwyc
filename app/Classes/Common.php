<?php

namespace App\Classes;

use App\Models\Visa\ActionLogs;
use App\Models\Visa\TransactionLogs;
use App\Models\Visa\PushNotificationsModel as PushNotif;

use DB, DateTime, Carbon\Carbon;
use Auth, Cart;

use App\Models\Visa\Group;
use App\Models\Visa\Deposit;
use App\Models\Visa\Refund;
use App\Models\Visa\Payment;
use App\Models\Visa\Discount;
use App\Models\Visa\ClientService;
use App\Models\Visa\ClientTransactions;
use App\Models\Visa\Package;
use App\Models\Visa\Service;
use App\Models\User;
use App\Models\UserOpen;

use PushNotification;



class Common {

	public static function time_elapsed_string($datetime, $full = false)
    {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

	public static function filterUser($types = [])
	{
		if(Auth::check()) {
			if(in_array(Auth::user()->type, $types)) {
				return true;
			} else {
				Auth::logout();
			}
		}

		return false;
	}

	public static function saveActionLogs($id = 0, $client_id ,$details, $group_id = 0)
	{
        date_default_timezone_set('Asia/Manila');
		if(Auth::check()) {
			//$user = Auth::user();
			$log = new ActionLogs;
			$log->timestamps = false;
			$log->client_id = $client_id;
			$log->details = $details;
			$log->service_id = $id;
			$log->group_id = $group_id;
			$log->user_id = Auth::user()->id;
			$log->log_date = Carbon::now()->format('F j, Y, g:i a');
			$log->save();
		}
	}

	public static function saveTransactionLogs($log_data){
        date_default_timezone_set('Asia/Manila');
        $dates = date('F j, Y, g:i a');
        // \Log::info($log_data);
        // \Log::info(isset($log_data['log_date']));
        // \Log::info(isset($log_data['client_id']));
        if(isset($log_data['log_date']) == ''){
        	$log_data['log_date'] = $dates;
        }
		if(Auth::check()) {
        //Insert new log
		//$user = Auth::user();
        $log_data['user_id'] = Auth::user()->id;		
        TransactionLogs::insert($log_data);
    	}
    }

	//VISA FUNCTIONS for Deposits, Refunds, Discounts, Payments
	public static function get_total($type, $amount ,$group_id = 0 ,$client_id = 0){
		if($group_id>0){
            return $type->where('group_id',$group_id)->sum($amount);
		}
	}

	//Global Function
    public static function formatMoney($number, $fractional=false) {
        if ($fractional) {
            $number = sprintf('%.2f', $number);
        }
        while (true) {
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
            if ($replaced != $number) {
                $number = $replaced;
            } else {
                break;
            }
        }
        return $number;
    }

    public static function toInt($str){
        return preg_replace("/([^0-9\\.])/i", "", $str);
    }

    public static function checkPackageComplete($pack_id){
        $package_complete = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%complete%')
            ->count();

        return $package_complete;
    }

    public static function checkPackagePending($pack_id){
        $package_pending = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%pending%')
            ->count();

        return $package_pending;
    }

    public static function checkPackageOnProcess($pack_id){
        $package_onprogress = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%on process%')
            ->count();

        return $package_onprogress;
    }

    public static function check_package_updates($pack_id){
        $stat = 0; // empty

        if(Common::checkPackageComplete($pack_id)>0){
            $stat = 4; // complete
        }
        if(Common::checkPackageOnProcess($pack_id)>0){
            $stat = 1; // process
        }
        if(Common::checkPackagePending($pack_id)>0){
            $stat = 2; // pending
        }

        $data = array('status' => $stat);

        DB::connection('visa')
            ->table('packages')
            ->where('tracking', $pack_id)
            ->update($data);
    }

    public static function generateIndividualTrackingNumber($length = 7) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateGroupTrackingNumber($length = 7) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = 'G';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateMessage() {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = 'G';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

 //    public static function getCommisions($id, $type = null){
 //    	$ret =[];
	// 	if($type == 'Agent'){
 //            $ref = UserOpen::where('referral_id',$id)->groupBy('client_id')->pluck('client_id');
 //            $cs = ClientService::select(DB::raw('sum(cost+charge+tip) as total_cashflow'))->whereIn('client_id',$ref)->where('status','complete')->orderBy('id','asc')->get();
 //            $ret['total_cashflow'] = $cs[0]->total_cashflow;
 //            $cash = 0;
 //            $com = 0;
 //            // foreach($cs as $s){
 //            // 	$cash += $s->charge + $s->tip + $s->cost;
 //            // 	$service = Service::findorFail($s->service_id);
 //            // 	if($service){
 //            // 		$com += $this->byTotalCashflow($cash,$type);
 //            // 	}	
 //            // }
	// 	}
	// 	return $ret;
	// }

	// public static function byTotalCashflow($cash, $type = null){
	// 	if($cash <= 1000000){
	// 		$
	// 	}
	// }

    public static function savePushNotif($client_id,$title,$body,$type,$type_id,$main,$main_cn = null,$title_cn= null,$body_cn= null,$parent = 0, $parent_type = 0, $clientNames = null, $extra = null, $authlogin = false)
	{
		if(Auth::check() || $authlogin) {
			$auth = User::where('id',Auth::user()->id)
						->where('is_active',1)
						->where('token','!=','')
						->first()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
			$c_at = Carbon::now('Asia/Manila');
			$log = new PushNotif;
			$log->client_id = $client_id;
			$log->title = $title;
			$log->title_cn = $title_cn;
			$log->body = $body;
			$log->body_cn = $body_cn;
			$log->type = $type;
			$log->type_id = $type_id;
			$log->main = $main;
			$log->main_cn = $main_cn;
			$log->parent = $parent;
			$log->parent_type = $parent_type;
			$log->extra = $extra;
			$log->created_at = $c_at;
			$log->updated_at = $c_at;
			$log->save();
			$getService = ClientService::where('id',$parent)->first();

			if(($type == 'Service' || $type == 'Discount' || $type == 'Report') && $parent_type ==2){
				$upd_serv = PushNotif::where('type_id', $parent)
			          ->where('parent_type', 1)->first();
				if($upd_serv){
					    $upd_serv->updated_at = $c_at; 
					    $upd_serv->save(); 			    	
					
					$upd_pack = PushNotif::where('type_id', $upd_serv->parent)
				          ->where('parent_type', 0)->first();
				    if($upd_pack){
				    	$upd_pack->updated_at = $c_at; 
				    	$upd_pack->save(); 
				    }
				}
				else{
					$translated = Service::where('id',$getService->service_id)->first();
	                $cnserv =$getService->detail;
	                if($translated){
	                    $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
	                }
					if($getService){
						$orderdate = explode('/', $getService->service_date);
						$sDate = Carbon::createFromDate($orderdate[2], $orderdate[0], $orderdate[1], 'Asia/Manila');
						//$sDate =  Carbon::createFromFormat('y/m/d h:i:s', $getService->service_date);
						$log = new PushNotif;
						$log->client_id = $client_id;
						$log->title = 'New Service';
						$log->title_cn = '新的服务';
						$log->body = 'New Service '.$getService->detail.' was added to your package #'.$getService->tracking;
						$log->body_cn = "新的服务 ".$cnserv." 已添加到您的编号为#".$getService->tracking."的服务包";
						$log->type = 'Service';
						$log->type_id = $getService->id;
						$log->main = $getService->detail;
						$log->main_cn = $cnserv;
						$log->parent = $getService->tracking;
						$log->parent_type = 1;
						$log->created_at = $sDate;
						$log->updated_at = $c_at;
						$log->read_at = $c_at;
						$log->save();

						$log = new PushNotif;
						$log->client_id = $client_id;
						$log->title = $auth.' created new package';
						$log->title_cn = $auth.' 新的服务包创建';
						$log->body = $auth.' created new package  with reference #'.$getService->tracking;
						$log->body_cn = $auth." 新的服务包创建 关联 #".$getService->tracking;
						$log->type = 'Package';
						$log->type_id = $getService->tracking;
						$log->main = "Package #".$getService->tracking;
						$log->main_cn = "关联 #".$getService->tracking;
						$log->parent = 0;
						$log->parent_type = 0;
						$log->created_at = $sDate;
						$log->updated_at = $c_at;
						$log->read_at = $c_at;
						$log->save();


					}
				}
			}

			if(($type == 'Package' || $type == 'Discount' || $type == 'Service') && $parent_type ==1){				
				$upd_pack = PushNotif::where('type_id', $parent)
			          ->where('parent_type', 0)->first();
			    if($upd_pack){
				    $upd_pack->updated_at = $c_at; 
				    $upd_pack->save(); 
				}
				else{
					$getService = ClientService::where('id',$type_id)->first();
					if($getService){
						$orderdate = explode('/', $getService->service_date);
						$sDate = Carbon::createFromDate($orderdate[2], $orderdate[0], $orderdate[1], 'Asia/Manila');
						$tracking = $getService->tracking;
					}
					else{
						$sDate = $c_at;
						$tracking = $type_id;
					}
					$log = new PushNotif;
						$log->client_id = $client_id;
						$log->title = $auth.' created new package';
						$log->title_cn = $auth.' 新的服务包创建';
						$log->body = $auth.' created new package with reference #'.$tracking;
						$log->body_cn = $auth." 新的服务包创建 关联 #".$tracking;
						$log->type = 'Package';
						$log->type_id = $tracking;
						$log->main = "Package #".$tracking;
						$log->main_cn = "关联 #".$tracking;
						$log->parent = 0;
						$log->parent_type = 0;
						$log->created_at = $sDate;
						$log->updated_at = $c_at;
						$log->read_at = $c_at;
						$log->save();
				}
			}

			if(($type == 'GService') && $parent_type ==1){				
				$upd_pack = PushNotif::where('type_id', $parent)
			          ->where('parent_type', 0)->first();
			    if($upd_pack){
				    $upd_pack->updated_at = $c_at; 
				    $upd_pack->save(); 
				}
				else{
					$gtrack = explode('--', $type_id);
					$group = Group::where('tracking',$gtrack[1])->first();
					//$getService = ClientService::where('service_date',$parent)->('group_id',$group->id)->first();
					$orderdate = explode('/', $gtrack[0]);
					$sDate = Carbon::createFromDate($orderdate[2], $orderdate[0], $orderdate[1], 'Asia/Manila');
					$log = new PushNotif;
						$log->client_id = $client_id;
						$log->title = $clientNames;
						$log->title_cn = $clientNames;
						$log->body = 'Updates for '.$clientNames;
						$log->body_cn = 'Updates for '.$clientNames;
						$log->type = 'Group';
						$log->type_id = $parent;
						$log->main = $clientNames;
						$log->main_cn = $clientNames;
						$log->parent = 0;
						$log->parent_type = 0;
						$log->created_at = $sDate;
						$log->updated_at = $c_at;
						$log->read_at = $c_at;
						$log->save();
				}
			}

			if(($type == 'GReport') && $parent_type ==1){				
				$upd_pack = PushNotif::where('type_id', $parent)
			          ->where('parent_type', 0)->first();
			    if($upd_pack){
				    $upd_pack->updated_at = $c_at; 
				    $upd_pack->save(); 
				}
				else{
					$gtrack = explode('--', $type_id);
					$group = Group::where('tracking',$gtrack[1])->first();
					//$getService = ClientService::where('service_date',$parent)->('group_id',$group->id)->first();
					$orderdate = explode('/', $gtrack[0]);
					$sDate = Carbon::createFromDate($orderdate[2], $orderdate[0], $orderdate[1], 'Asia/Manila');
					$log = new PushNotif;
						$log->client_id = $client_id;
						$log->type = 'Group';
						$log->type_id = $parent;
						if($clientNames != null){
							$log->body = 'Updates for '.$clientNames;
							$log->body_cn = 'Updates for '.$clientNames;
							$log->title = $clientNames;
							$log->title_cn = $clientNames;
							$log->main = $clientNames;
							$log->main_cn = $clientNames;
						}
						$log->parent = 0;
						$log->parent_type = 0;
						$log->created_at = $sDate;
						$log->updated_at = $c_at;
						$log->read_at = $c_at;
						$log->save();
				}
			}

			if(($type == 'GDiscount') && $parent_type ==1){				
				$upd_pack = PushNotif::where('type_id', $parent)
			          ->where('parent_type', 0)->first();
			    if($upd_pack){
				    $upd_pack->updated_at = $c_at; 
				    $upd_pack->save(); 
				}
				else{
					$group = Group::where('tracking',$parent)->first();
					$getService = ClientService::where('group_id',$group->id)->first();
					$orderdate = explode('/', $getService->service_date);
					$sDate = Carbon::createFromDate($orderdate[2], $orderdate[0], $orderdate[1], 'Asia/Manila');
					$log = new PushNotif;
						$log->client_id = $client_id;
						$log->title = 'Group '.$group->name." (".$group->tracking.")";
						$log->title_cn = '新的服务包创建';
						$log->body = 'Updates for Group '.$group->name;
						$log->body_cn = 'Updates for Group '.$group->name;
						$log->type = 'Group';
						$log->type_id = $group->tracking;
						$log->main = 'Group '.$group->name." (".$group->tracking.")";
						$log->main_cn = 'Group '.$group->name." (".$group->tracking.")";
						$log->parent = 0;
						$log->parent_type = 0;
						$log->created_at = $c_at;
						$log->updated_at = $c_at;
						$log->read_at = $c_at;
						$log->save();
				}
			}

			if(($type == 'Timelog' || $type == 'Funds') && $parent_type ==1){				
				$upd_pack = PushNotif::where('type_id', $parent)
			          ->where('parent_type', 0)->first();
			    if($upd_pack){
				    $upd_pack->updated_at = $c_at; 
				    $upd_pack->save(); 
				}
				else{
					$gtrack = explode('--', $type_id);
					$dayname = $gtrack[0];
					$log = new PushNotif;
					$log->client_id = $client_id;
					$log->type = 'Timelog';
					$log->type_id = $parent;
						$log->body = $dayname;
						$log->body_cn = $dayname;
						$log->title = $dayname;
						$log->title_cn = $dayname;
						$log->main = $dayname;
						$log->main_cn = $dayname;
					$log->parent = 0;
					$log->parent_type = 0;
					$log->created_at = $c_at;
					$log->updated_at = $c_at;
					$log->read_at = $c_at;
					$log->save();
				}
			}

			if(($type == 'Reminder') && $parent_type ==1){				
				$upd_pack = PushNotif::where('type_id', $parent)
			          ->where('parent_type', 0)->first();
			    if($upd_pack){
				    $upd_pack->updated_at = $c_at; 
				    $upd_pack->save(); 
				}
				else{
					$gtrack = explode('--', $type_id);
					$dayname = $gtrack[0];
					$log = new PushNotif;
					$log->client_id = $client_id;
					$log->type = 'Timelog';
					$log->type_id = $parent;
						$log->body = $dayname;
						$log->body_cn = $dayname;
						$log->title = $dayname;
						$log->title_cn = $dayname;
						$log->main = $dayname;
						$log->main_cn = $dayname;
					$log->parent = 0;
					$log->parent_type = 0;
					$log->created_at = $c_at;
					$log->updated_at = $c_at;
					$log->read_at = $c_at;
					$log->save();
				}
			}

			// $client = User::where('id',$client_id)
			// 			->where('is_active',1)
			// 			->where('token','!=','')
			// 			->first();

			$client = DB::connection('mysql')
			            ->table('users as a')
			            ->select(DB::raw('a.id, a.first_name, a.last_name, a.is_active, a.token, a.language,a.device_type'))
			            ->where('is_active',1)
						->where('token','!=','')
						->first();				

			if($client){
				// $client->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
				$unread = PushNotif::where('client_id', $client_id)
				            ->where('read_at', null)
				            ->count();
				$msg = ($client->language == 'EN' ? $body : $body_cn);
				if($type == 'Report' || $type == 'GReport'){
					$t = ($client->language == 'EN' ? $title : $title_cn);
					$msg = $t.', '.$msg;
				}
				$message =  PushNotification::Message(str_replace('-','',strip_tags($msg)),array(
					    'badge' => $unread,
					    "content-available" => 1,
					    'custom' => array('wyc-data' => array(
					        'title' => strip_tags($title),'title_cn' => strip_tags($title_cn), 'client' => $client_id, 'type' => $type, 'type_id' => $type_id
					    ))
					));
					$mode = ($client->device_type == "IOS" ? 'wycIos' : 'wycAndroid');
					$deviceToken = $client->token;
	   				PushNotification::app($mode)
	                ->to($deviceToken)
	                ->send($message);
			}
		}
	}

}
