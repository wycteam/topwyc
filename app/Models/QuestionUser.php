<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class QuestionUser extends Model
{
	protected $table = 'question_user';
    protected $fillable = [
        'question_id', 'user_id', 'answer'
    ];


    ///////////////////
    // Relationships //
    ///////////////////

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
