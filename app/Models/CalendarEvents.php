<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarEvents extends Model implements \MaddHatter\LaravelFullcalendar\Event
{
    protected $fillable = [
    	'title', 'status',
    	'start_date','end_date'
    ];

    protected $dates = ['start_date', 'end_date'];

    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId() {
		return $this->id;
	}

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->all_day;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start_date;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end_date;
    }
}
