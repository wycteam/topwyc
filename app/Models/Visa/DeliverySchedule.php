<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon ;

class DeliverySchedule extends Model
{
    protected $connection = 'visa';
	public $table = 'delivery_schedule';
	protected $fillable = ['item', 'rider_id', 'location', 'scheduled_date', 'scheduled_time'];
	public $timestamps = false;

	public function rider(){
    	return $this->belongsTo('App\Models\User','rider_id');
    }

    public function getScheduledDateAttribute($value) {
	  return ($value) ? Carbon::parse($value)->format('m/d/Y') : null;
	}

}
