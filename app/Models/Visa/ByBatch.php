<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class ByBatch extends Model
{
    
    protected $connection = 'visa';
	protected $primaryKey = 'b_id';
	public $table = 'by_batch';
    public $timestamps = false;

    protected $fillable = [
        'date', 'date2', 'group_id',
        'services', 'transactions',
    ];

}
