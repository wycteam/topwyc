<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class FetchService extends Model
{
    protected $connection = 'visa';
	protected $table = 'services';
    protected $fillable = [
		 'id'
		,'parent_id'
		,'detail'
		,'detail_cn'
		,'cost'
		,'cost_lvl3'
		,'cost_lvl2'
		,'who_is_in_charge'
		,'where_to_file'
		,'estimated_cost'
		,'description'
		,'description_cn'
		,'docs_needed'
		,'docs_optional'
        ,'docs_released'
        ,'docs_released_optional'
		,'is_active'
        ,'charge'
        ,'tip'
        ,'branch_id'
        ,'months_required'
        ,'com_agent'
        ,'com_client'
    ];

    public $timestamps = false;
}
