<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $connection = 'visa';
	protected $table = 'reports';
    protected $fillable = [
		 'id'
        ,'client_id'
        ,'group_id'
		,'service_id'
		,'detail'
		,'user_id'
		,'tracking'
		,'log_date'
		,'is_read'
    ];

    public $timestamps = false;

    public function user(){
    	return $this->belongsTo('App\Models\User','user_id');
    }

    public function service(){
    	return $this->belongsTo('App\Models\Visa\ClientService','service_id');
    }

}
