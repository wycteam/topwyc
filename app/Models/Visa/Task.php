<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

  protected $connection = 'visa';

	protected $table = 'tasks';

	public $timestamps = false;

	protected $fillable = ['client_service_id', 'who_is_in_charge', 'where_to_file', 'estimated_cost', 'status', 'date', 'custom_date'];

	public function clientService() {
		return $this->belongsTo('App\Models\Visa\ClientService', 'client_service_id', 'id');
	}

	public function whoIsInChargeRelation() {
		return $this->belongsTo('App\Models\User', 'who_is_in_charge', 'id');
	}

}
