<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Initials extends Model
{
  protected $connection = 'visa';
  protected $table = 'financing_initials';
  protected $primaryKey = 'id';
  protected $fillable = [
    'cash_balance',
    'bank_balance',
    'branch_id',
    'created_at'
  ];
}
