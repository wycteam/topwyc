<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\Controllers\Cpanel\SynchronizationController;
use Carbon\Carbon;

class Discount extends Model
{
    use SoftDeletes;
    // protected $dates = ['deleted_at'];
    protected $connection = 'visa';
	protected $table = 'client_discounts';
    protected $fillable = [
		 'client_id'
		,'discount_amount'
        ,'group_id'
		,'service_id'
        ,'reason'
		,'tracking'
		,'log_date'
        ,'authorizer'
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });

        static::updating(function($model)
        {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });
    }

    public function user(){
    	return $this->belongsTo('App\Models\User','client_id');
    }

    public function group(){
    	return $this->belongsTo('App\Models\Visa\Group','group_id');
    }

    public function service() {
        return $this->belongsTo('App\Models\Visa\ClientService', 'service_id');
    }

}
