<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use App\Models\Visa\Service;
use App\Models\Visa\Branch;
use App\Models\Visa\ServiceProfiles;
use App\Models\Visa\ServiceProfileCost;

class Service extends Model
{
    protected $connection = 'visa';
	protected $table = 'services';
    protected $fillable = [
		 'id'
		,'parent_id'
		,'detail'
		,'detail_cn'
		,'cost'
		,'cost_lvl3'
		,'cost_lvl2'
		,'who_is_in_charge'
		,'where_to_file'
		,'estimated_cost'
		,'description'
		,'description_cn'
		,'docs_needed'
		,'docs_optional'
        ,'docs_released'
        ,'docs_released_optional'
		,'is_active'
        ,'charge'
        ,'tip'
        ,'branch_id'
        ,'months_required'
        ,'com_agent'
        ,'com_client'
    ];

    public $timestamps = false;

	// protected $appends = [
 //        'service_cost',
 //    ];

    public static function boot()
    {
        parent::boot();


    //static::created(function ($service) {
        $srvcs = Service::all();
        $prof = ServiceProfiles::where('is_active',1)->get();
        $branches = Branch::where('id','!=',3)->get();
        foreach($srvcs as $s){
            foreach($prof as $p){
                foreach($branches as $b){
                    //for service profile cost
                    //if($b->id != 3){
                    	$checkCost = ServiceProfileCost::where('branch_id',$b->id)->where('profile_id',$p->id)->where('service_id',$s->id)->first();
                    	if(!$checkCost){
                    		 ServiceProfileCost::create([
        		                // 'service_id' => $s->id,
        		                // 'profile_id' => $p->id,
                                // 'branch_id' => $b->id,
        		                // 'cost' => $s->cost,
                                // 'charge' => $s->charge,
                                // 'tip' => $s->tip,
                                // 'com_agent' => $s->com_agent,
                                // 'com_client' => $s->com_client
                                'service_id' => $s->id,
        		                'profile_id' => $p->id,
                                'branch_id' => $b->id,
        		                'cost' => 0,
                                'charge' => 0,
                                'tip' => 0,
                                'com_agent' => 0,
                                'com_client' => 0,
        		            ]);
                    	}
                    //}
                }
            }
            foreach($branches as $b){
                if($b->id > 1 && $b->id != 3){
                    //for service branch cost
                    $checkBranchCost = ServiceBranchCost::where('branch_id',$b->id)->where('service_id',$s->id)->first();
                    if(!$checkBranchCost){
                         ServiceBranchCost::create([
                            'service_id' => $s->id,
                            'branch_id' => $b->id,
                            'cost' => $s->cost,
                            'charge' => $s->charge,
                            'tip' => $s->tip,
                            'com_agent' => $s->com_agent,
                            'com_client' => $s->com_client
                        ]);
                    }
                }
            }
        }
    //});

    }
    // public function getServiceCostAttribute()
    // {
    //     return $this->servicecost()->get();
    // }


    public function servicecost()
    {
        return $this->belongsToMany(ServiceProfiles::class, 'service_profile_cost', 'service_id', 'profile_id');
    }

    public function serviceBranchCosts() {
        return $this->hasMany('App\Models\Visa\ServiceBranchCost', 'service_id', 'id');
    }

}
