<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class CommentAuthor extends Model
{
  protected $connection = 'visa';
  protected $table = "comment_author";
  protected $fillable = ['name','ip_address','email_address'];
  public $timestamps = false;
}
