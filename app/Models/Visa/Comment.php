<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $connection = 'visa';
    protected $table = 'comments';
    protected $fillable = ['comment','votes','spam','reply_id','page_id','users_id'];

    protected $dates = ['created_at', 'updated_at'];

    public function replies(){

      return $this->hasMany('App\Models\Visa\Comment','id','reply_id');

    }
}
