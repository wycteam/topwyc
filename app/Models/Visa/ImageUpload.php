<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class ImageUpload extends Model
{
  protected $connection = 'visa';
  protected $table = 'image_gallery';
  protected $fillable = ['album_id','filename'];

}
