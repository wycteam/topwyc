<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class ClientDocuments extends Model
{
    protected $connection = 'visa';

    protected $fillable = [
        'user_id',
        'type', 'name',
        'documentable_id', 'documentable_type',
        'file_path', 'file_mime','file_size','multiple',
        'issue_at', 'expires_at'
    ];

    public function clientdocumentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function doctype()
    {
        return $this->belongsTo(ClientDocumentType::class, 'type');
    }
}
