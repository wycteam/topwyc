<?php
namespace App\Models\Visa;

use App\Acme\Traits\ClientDocumentable;
use Illuminate\Database\Eloquent\Model;

class ClientDocumentType extends Model
{
    use ClientDocumentable;
    
    protected $connection = 'visa';

    protected $fillable = [
        'id',
        'name'
    ];

    public function clientdocs()
    {
        return $this->belongsToMany(ClientDocuments::class);
    }
}
